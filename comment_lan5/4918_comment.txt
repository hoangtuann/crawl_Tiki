439713
4918
Luôn bị hấp dẫn với nội dung và lối dẫn dắt câu chuyện của tác giả Kristan Higgins - một trong những tác giả nữ yêu thích nhất của tôi. Nội dung gần gũi, chân thực giống như nó có thể xảy ra ở cái cuộc sống hàng ngày ngoài kia của bất kỳ ai của bất kỳ cô gái nào. Tôi thích cách Kristan xây dựng hình tượng nhân vật, nó không quá tốt đẹp, không quá hoàn hảo giống như những câu truyện ngôn tình Trung Quốc khác nó mang đến cho tôi và những người đọc khác một cái thực tế hơn vào cuộc sống. Và cả những tình huống hài hước cũng được dựng lên bằng ngòi bút khéo léo. Tác phẩm không chỉ đơn giản là một cuốn tiểu thuyết về tình yêu đôi lứa mà trong đó tác giả còn nêu cao tình cảm gia đình, tình cảm giữa bố mẹ con cái và cả tình chị em ruột thịt gắn bó, hy sinh. Hãy cùng thưởng thức, ngấu nghiến từng trang sách và cảm nhận cuộc sống vốn không như là mơ của chính mình.
4
613896
2016-05-31 17:07:49
--------------------------
415600
4918
Tôi lang thang trên mạng và tình cờ thấy được bìa cuốn sách và bị thu hút ngay từ đó. Đặt mua, nhận sách, trả tiền rồi đắm chìm vào những câu chữ.
Phải nói là văn phong của Higgins quá đỗi tuyệt vời nhất là ở khoản gây cười, hóm hỉnh. Vì tác phẩm này ở ngôn kể thứ nhất nên tôi càng có cơ hội cảm nhận được sâu sắc nội tâm nhân vật, chi tiết miêu tả tỉ mỉ và rõ ràng dễ hiểu. Từ những việc dường như chẳng bao giờ tôi nghĩ đến như việc thổi thủy tinh của mẹ Grace hay cả hình ảnh nội cục cằn của cô nữa.
Rồi còn cả Callashan nữa, anh quá đỗi vui tính và ngọt ngào và cả Grace cũng vậy. Khi mà họ nói chuyện với nhau làm tôi không thể tránh khỏi việc phì cười.
Cái kết có hậu vào thoả mãn.
Thật tuyệt vời!

5
856660
2016-04-13 18:57:10
--------------------------
409356
4918
Lần đầu thấy cuốn sách này ở Hội sách mình đã bị bìa của nó thu hút ^^ Lại thêm đọc nội dung tóm tắt ở phía sau lại càng cảm thấy hấp dẫn hơn (đoạn trai đẹp... vừa mới ra tù ý).

Nhân vật Grace là 1 cô giáo dạy lịch sử đã 30 tuổi, luôn tự cho mình là người xấu xí và kém nổi bật nhất trong 3 chị em gái; chồng sắp cưới lại chia tay cô và sắp lấy em gái xinh đẹp mà bản thân cô chẳng tìm được anh chàng nào hoàn hảo hơn... Vì thế cô liên tục bịa ra anh chàng Bác sĩ nhi Watt hoàn hảo không hề tồn tại để lừa mọi người và cũng lừa chính bản thân mình.
Câu chuyện tưởng như vẫn đơn giản nếu như không có sự xuất hiện của anh chàng Callahan hoàn hảo nhưng vừa... ra tù ở nhà bên =)))) Giữa 2 người là tình yêu sét đánh thật sự nhưng học lại cứ chần chừ, đắn đo vì đủ mọi lý do.
Cuối cùng họ vẫn đến với nhau, chấp nhận nhau và yêu thương nhau thật sự trái ngược với tình yêu sét đánh ngộ nhận giữa chồng sắp cưới trước đây của Grace - Andrew và em gái cô - Natalie.

Câu chuyện chỉ có điểm đáng tiếc là hơi ít phân đoạn nói về anh chàng Callahan đã yêu Grace như thế nào và yêu nhiều đến thế nào, nỗ lực thế nào để có thể tiến đến với Grace.

Đây là một câu chuyện lãng mạng, hấp dẫn và không kém phần hài hước đáng để mua và lưu giữ trong thư viện ở nhà ^^
5
198446
2016-04-02 12:59:36
--------------------------
350215
4918
Từ trước đến giờ, mình hầu như chỉ đọc tiểu thuyết lãng mạn. Đọc nhiều quá, hoá nhàm.

Nhưng cuốn này quả thực làm mình vô cùng bất ngờ, thậm chí ngay cả khi chưa mở trang đầu tiên ra mình đã biết tỏng kết cục :))

Lối kể chuyện của tác giả siêu gần gũi, siêu hài hước, khiến cho mình có cảm giác như thấy được chính mình trong hình ảnh nữ chính ấy.

Cách tác giả làm cho câu chuyện thêm phần éo le cũng khá vui. Dù có đôi chút không thực tế lắm về nhân vật nam chính nhưng tác giả đã làm quá tốt việc dẫn dắt người đọc đi cùng nhân vật nữ chính 

Những nhân vật phụ cũng có vai trò quan trọng tác động đến hai nhân vật chính, tính cách của những nhân vật phụ cũng khá tương đồng với những người mẹ, người chị gái, em gái ngoài đời thực.

Đời không như là mơ. Vừa mơ mộng vừa thực tế. Đọc để tìm thấy chút gì của mình trong đó :)

P/S: bìa sách đơn giản mà rất đẹp 
5
1032006
2015-12-10 08:55:13
--------------------------
347574
4918
Truyện được viết theo lối xưng hô ngôi thứ nhất, điều này khiến nhiều người có vẻ không thích lắm vì lối kể này chỉ giúp độc giả nhìn thấy một khía cạnh từ nhân vật “tôi” nhưng với một tác giả thật sự có tài như Kristan Higgins thì tôi không có gì ngạc nhiên khi truyện của cô được đạt giải Rita 2010. Truyện thật sự vô cùng thú vị và hài hước một cách gây nghiện, cô gái Grace Emerson thật sự có một nội tâm khó đỡ, những màn độc thoại, những suy nghĩ hay cách nói chuyện, ứng biến của cô khiến tôi không thể nhịn nổi cười, cô là một cô gái thú vị ra trò, rất yêu gia đình, lâu lâu cũng hơi hâm hâm nhưng khá đáng yêu, thực tế và tốt bụng. Bên cạnh cô còn có tên cựu tù to lớn, hấp dẫn, chưa chi đã bị Grace mang đến những vết tích ấn tượng, lãnh một cú đập của cô từ cây gậy hockey, bị cô phang cho cả cái cào vô mặt và rồi tông móp xe anh, chưa kể con chó quý hóa của Grace luôn xem Callahan như cái gai trong mắt, lúc nào cũng gây rắc rối cho anh. Thế mà từ vai trò là những người hàng xóm khắc kỷ, cả Grace và Callahan đều phát triển thành một tình yêu đẹp với những cử chỉ quan tâm lãng mạn mà họ dành cho nhau.

Với tôi thì truyện này hay và để lại nhiều ấn tượng sâu sắc, từng nhân vật đều được tác giả xây dựng rất rõ nét, không chỉ riêng Grace mà hầu hết các nhân vật còn lại đều mang những điểm hài hước riêng biệt. Truyện mang nhiều ý nghĩa về tình yêu, cả trong tình bạn và tình cảm gia đình, không chỉ là gây cười mà những khắc họa của tác giả về cuộc sống cũng đủ giúp tôi ngẫm thấy những bài học khá đáng giá trong từng lối ứng xử lẫn cách mà khi ta phải đối mặt với tình yêu. Theo tôi đây là một cuốn sách rất đáng đọc.

5
41370
2015-12-04 18:06:28
--------------------------
243495
4918
Với văn phong nhẹ nhàng, tình cảm, hài hước và rất chân thật, Kristan Higgins đã khắc họa chi tiết cuộc sống của những cô nàng hiện đại ngoài 30 vẫn ế chỏng ế chơ.  Đọc câu chuyện ta thấy mình cũng giống nhân vật Grace một phần nào đó: sống cùng chàng bạn trai tưởng tượng suốt thời niên thiếu và chờ đến một ngày tìm thấy tình yêu đích thực của cuộc đời mình. Cuốn sách không chỉ kể về tình yêu bình thường, mà còn kể về khao khát tìm được nửa kia của một nhân vật "đồng tính", xen lẫn trong đấy là niềm yêu nghề và tình yêu thương gia đình. Không có nhiều yếu tố bất ngờ, kịch tính nhưng cũng để người đọc không thể quay lưng.
5
528434
2015-07-27 14:58:46
--------------------------
242826
4918
Nội dung truyện nhẹ nhàng, dễ đọc, cốt truyện khá thú vị. Tuy nhiên, cách viết không tạo ra những bất ngờ và cuốn hút khiến cho cuốn truyện quá dài, nhiều lúc tôi cảm giác hơi phí thời gian để đọc tiếp. 

Truyện phù hợp với những phụ nữ độc thân ngoài 30 tuổi, vì tác giác viết rất chi tiết những cảm xúc, suy nghĩ, nhu cầu của họ và những ức chế mà người xung quanh thường mang lại cho họ. Đây có thể coi là một sự chia sẻ hết sức ý nghĩa. Truyện kết thúc khi nhân vật chính sau nhiều thất bại trong tình yêu cũng đã kết hôn và có con với người mình yêu và yêu mình. Một kết thúc có hậu, có lẽ sẽ giúp những phụ nữ độc thân ngoài 30 tuổi thêm vững tin vào tương lai :-)
3
586824
2015-07-26 23:35:57
--------------------------
229155
4918
Nếu thích cuốn sách có nội dung tình cảm nhẹ nhàng, lãng mạn, hài hước với những tình huống rất chân thật mà có thể chính chúng ta đã từng gặp phải nhưng ngại ngùng thừa nhận.. thì đây là nơi bạn có thể thả hồn, để gật gù thương cảm khi thấy mình trong đó hay thở phào nhẹ nhõm vì tình cảnh của ta đỡ " éo le " hơn quý cô Grace Emerson này. Cốt truyện thú vị, nhân vật dễ gần, tình huống hài hước và kịch tính vừa phải - với tôi thế là đủ cho một tối Chủ nhật bình yên. 
4
496400
2015-07-16 15:18:14
--------------------------
223691
4918
Đời không như mơ là một câu chuyện tình yêu oái ăm xảy ra khi  Grace Emerson- một cô nàng đã ngoài ba mươi ế chỏng ế chơ,bị chồng chưa cưới phải lòng em gái ruột và bỏ rơi laị phải lòng một anh chàng hàng xóm quyến rũ chết người nhưng lại mới ra tù ! Đối măt với gia đình và cuộc sống cùng với một anh chàng bạn trai tưởng tượng, Grace cuối cùng cũng tìm được tình yêu và hạnh phúc đời mình. Đời có thể không như là mơ nhưng giấc mơ cũng có thể đem lại nhiều điều tốt đẹp trong cuộc sống và biết đâu một lúc nào đó đôi khi giấc mơ cũng có thể trở thành sự thực như anh bạn trai bác sĩ tưởng tượng của Grace. Vì vậy, bạn ơi hãy cứ mơ đi!
4
129176
2015-07-07 14:27:48
--------------------------
213227
4918
Mình thường dễ bị ấn tượng bởi bìa sách trước khi quyết định cầm lên lướt thử phần giới thiệu sách lắm :P Và đời không như là mơ cũng nằm trong số đó á, đơn giản mà dễ thương, màu hồng nhạt nhẹ nhàng nữ tính như nội dung câu chuyện vậy. 
Bạn bên dưới bảo nữ chính Grace Emerson không có thê thảm lắm, nhưng mình nghĩ ở cái tuổi đó thì như vậy là quá thảm luôn còn gì ^^! Đọc tới khúc cặp đôi chính chia tay mình khóc luôn, tác giả thiệt là biết đẩy cao trào câu chuyện ghê. May mà lết them mấy trang nữa thì happy ending, không chắc mình ghét tác giả luôn mất.
5
541809
2015-06-23 15:51:25
--------------------------
206074
4918
"Đời không như là mơ" thu hút mình ngay từ phần giới thiệu. Nhìn chung thì nội dung sách không quá mới lạ nhưng lại hấp dẫn người đọc bởi giọng văn thoải mái và hài hước, rất dễ đọc. Câu chuyện rất dễ thương, thực ra nhân vật nữ chính cũng không đến mức "thê thảm" như phần giới thiệu đã nói, những điều tưởng chừng như xui xẻo cùng những tình huống bất ngờ đã giúp Grace và gia đình trưởng thành hơn, để rồi qua đó họ càng hiểu, gắn bó và trân trọng nhau hơn (cô còn tìm được NGƯỜI ĐÓ của mình nữa). Mình thích gần như tất cả các nhân vật, họ đều có những nét đáng yêu của riêng mình, thật mừng vì họ đều có được hạnh phúc.
4
198872
2015-06-08 17:30:20
--------------------------
193938
4918
Ế - một trong những vấn đề "đau đầu" với nhiều cô gái hiện nay. Và cái "ế" trong Đời không như là mơ thật sự là nỗi đồng cảm lớn mà tác giả Kristan Higgins dành cho các nữ độc giả của mình.

Câu chuyện nhẹ nhàng, hài hước nhưng cũng khiến ta không khỏi bất ngờ vì những tình huống chân thật, gần gũi và không kém phần...khó đỡ. Truyên được kể dưới góc nhìn của nữ chính Grace Emerson, vậy nên hẳn sẽ có nhiều bạn có cảm giác đọc quyển tiểu thuyết này như đọc quyển nhật ký của chính bản thân vậy (và gần như là trúng tim đen, đối với mình =))))). Tiếc là đời thật vẫn chưa có chàng Callahan nào xuất hiện cả :v

Không chỉ đơn thuần là chuyện tình yêu, Đời không như là mơ là câu chuyện về gia đình, về tình cảm yêu thương ruột thịt. Dù đôi lúc có khó chịu, có uất ức, có đau buồn vì mâu thuẫn, vì bất đồng hay thậm chí là bị phản bội...từ những người thân, thì suy cho cùng vì họ - những người ta yêu mến - ta sẽ luôn chấp nhận làm những điều ngu ngốc, thậm chí là cả điên rồ.

Còn về tình yêu trong truyện ư? Đó thật sự là một mối tình đẹp, giản dị, lãng mạn và gần với...đời thật nhất mà tôi từng được đọc qua các tiểu thuyết. Motif anh hàng xóm - cô hàng xóm ban đầu cải vã, hục hặc nhưng sau lại yêu nhau là không hề mới mẻ. Nhưng mối tình của một cô nàng ế vụng về và một cựu tù quyến rũ chết người đã được tác giả xây dựng thật nhẹ nhàng và đáng yêu. Điều khiến tôi ấn tượng và yêu nhất trong mối quan hệ của Grace - Callahan chính là sự tác động mà cả hai dành cho nhau. Có một câu trích trong bộ phim The Mean War nói rằng : "Đừng yêu một anh chàng tốt đẹp. Hãy yêu một anh chàng khiến bạn trở nên tốt đẹp hơn." Và Callahan thật sự đã giúp Grace trở nên hoàn thiện hơn, tỏa sáng hơn rất nhiều.

Dựng lên một người bạn trai hoàn hảo để an ủi người thân, và an ủi chính mình Điều ấy là không có gì xấu xa. Nhưng theo thời gian, lời nói dối ấy tuy được dùng với mục đích tốt đẹp, nhưng lâu dần nó sẽ khiến ta tự huyễn hoặc và quên đi cái sự thật rằng : ta đang nói dối những người ta yêu và nói dối với cả chính bản thân ta. 

Khi đọc lời giới thiệu, tôi đã thích và đặt mua ngay không suy nghĩ. Và đọc xong rồi thì lại mê đắm và mãn nguyện với những gì mà Đời không như là mơ mang lại. Văn phong hài hước, dị dỏm, nhẹ nhàng và không hề sến như bao tiểu thuyết lãng mạn khác. Đây thật sự là một quyển sách đáng yêu, mang lại những phút giây thư giãn đầy thoải mái.

Điều duy nhất không hài lòng chính là những phân đoạn tình cảm giữa Grace - Callahan hơi bị ít (theo như cảm nhận của cá nhân). Giá mà tác giả hào phóng hơn, cho tình cảm của họ nhiều đất diễn hơn một chút thì tốt biết bao :3

Cuối cùng, rất mong sẽ có thêm nhiều tác phẩm khác của Kristin được xuất bản tại Việt Nam.
5
164390
2015-05-09 08:25:39
--------------------------
193306
4918
Đây là một cuốn sách mình được chị bạn thân tặng và đã ưng em í ngay từ cái nhìn đầu tiên, ưng từ cái tên tới nội dung cuốn sách.
Khá nhẹ nhàng và hài hước, tác phẩm chỉ xoay quanh việc Grace khổ sở loay hoay thoát khỏi kiếp "hàng tồn kho" và những cái nhìn thương hại về chuyện tình cảm của cô.
Chuyện cũng chẳng có gì nếu Callahan nóng bỏng không xuất hiện và cho cô những khao khát hàng đêm cô mơ về =)) Ờm, thực sự là đôi này đáng yêu chết đi được =))
Tuy có thể nói là gia đình Grace "chèn ép" cô rất nhiều nhưng ko thể phủ nhận được tình cảm họ dành cho Grace và cho nhau là cực kỳ lớn. Mình cực kỳ hâm mộ cái cách mà Margs và Nat thương Grace và ngược lại :x
Tóm lại, đây không phải là một tác phẩm ổn để dành cho những bạn thích phiêu lưu, mạo hiểm hay những pha gay cấn hồi hộp thót tim. Tuy nhiên, những bạn ưa thích nhẹ nhàng hay những bạn ưa phiêu lưu chợt cảm thấy tim có phần ko khỏe thì có thể tìm đến. Đây là một tác phẩm mà bạn có thể thưởng thức trong những ngày nghỉ buồn chán
5
369887
2015-05-07 08:03:04
--------------------------
180668
4918
Grace quả thực là một nhân vật rất thú vị, cuộc đời của nhân vật này quả đúng như cái tên sách “không như là mơ” tí nào :)) từ những ngày học trung học cho đến lúc đi làm, đến khi đã đứng trước bệ thờ mà cô nàng vẫn phải trải qua những tình huống cực kì dở khóc dở cười. Trong những ngày tháng cô đơn nhất của Grace, khi cô nàng tuyệt vọng nghĩ mình sẽ chẳng tìm được ai để cùng xây dựng mái ấm thì lại có một anh chàng cực kỳ đẹp trai, tốt bụng , đáng yêu rớt xuống ngay cạnh nhà, Callahan O'Shea- một anh chàng hoàn hảo, chỉ cần nghe cái tên thôi là đã thấy hấp dẫn rồi :)), duy chỉ có điều là anh ta mới ra tù. Cuối cùng thì câu chuyện bi hài của hai người này sẽ ra sao ? Grace sẽ phải làm gì khi cô nàng đã lỡ yêu anh chàng cựu tù này? Ngòi bút của tác giả sẽ đưa các bạn đến với những điều bất ngờ vô cùng hài hước, và lãng mạn. Cuốn sách rất thú vị và bạn sẽ phải há mồm ngạc nhiên cho đến những dòng chữ cuối cùng.
P/s: điều duy nhất mình ghét ở cuốn sách này là con chó của Grace =))))
4
134401
2015-04-09 21:35:34
--------------------------
