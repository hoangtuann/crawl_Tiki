453224
5209
Mình mê truyện này từ hồi cấp 2, đọc rất buồn cười thỉnh thoảng đọc trên lớp cứ cười hô hố lên ngại ơi là ngại nhưng trách sao được tại truyện hài quá. Đến mẹ mình đọc còn bị lôi cuốn nữa mà :)) Điểm trừ ở cuốn truyện này đó là có một vài tập nội dung hơi bị phản cảm không thích hợp cho trẻ em dưới 16 tuổi chút nào. Mình rất thích nhân vật Nene và Shin, cậu bé xin thì quậy phá thôi rồi đôi khi đọc thôi cũng thấy phát bực với nhân vật này, thực sự là rất khâm phục mẹ Misae ngày ngày phải đối phó những trò nghịch ngợm của cậu bé.
5
1232078
2016-06-21 09:22:47
--------------------------
387128
5209
Về hình thức, bìa nhìn rất bắt mắt, ngộ nghĩnh. Giấy in cũng tốt.
Về nội dung, cuốn truyện tranh này kể về những câu truyện xoay quanh cậu bé Shinnosuke và gia đình của cậu – Nohara. Lúc đầu mình cũng không ấn tượng về mặt nội dung cho lắm, nhân vật chính là cu Shin, một thằng nhóc 5 tuổi thích lột đồ, vẽ hình con voi lên quần lót,... nhưng càng đọc mình càng thấy hấp dẫn, lôi cuốn và ... buồn cười. Khi đọc cuốn truyện này, bạn sẽ phải cười phơ lớ, vì cuốn truyện này có nội dung rất vui nhộn. Mình rất yêu thích bộ truyện tranh này.
5
1142307
2016-02-26 17:26:38
--------------------------
318463
5209
Truyện nói về một ngày của cậu bé Shin dễ thương. 5 tuổi thôi. Nghịch hông nói nổi. Cậu có điệu nhảy "khoe mông" cực chất. Nói chung là gia đình Nohara rất dễ thương. Không chỉ vậy còn có những người hàng xóm rất là dễ thương, chú Omata, ... và các cô giáo ở trường mẫu giáo Action. Lớp hoa hướng dương có Nene, Masao, ... Shin luôn là người chọc ghẹo các bạn trong lớp nhưng ai cũng yêu thương Shin cả. hhihihi . Cả con bé Himawari đáng yêu, lúc nào cũng ghẹo chọc anh Shin kể cả Bạch Tuyết con chó của Shin cũng vậy <3 <3 <3
5
807818
2015-10-06 08:23:23
--------------------------
206504
5209
Truyện tranh là món ăn tinh thần của mình!Từ khi truyện Shin - Cậu Bé Bút Chì ra đời thì mình không bỏ qua tập nào vì truyện xoay quanh cuộc sống đời thường mà vui nhộn, và mình rất thích bé Himawari ,bé làm mình nhớ đến mấy đứa em lúc còn nhỏ của mình, đặc biệt là cu Shin. Trong tập 40 này mình ấn tượng đến cảnh siêu quậy của bé Gimawari vô cùng,đáng yêu lắm,tác giả thật là hiểu về thế giới của con nít.Còn về ngôn ngữ cho đến các tranh minh họa thì sống động , chân thực dù có một số hình hơi người lớn xíu nhưng trẻ nhỏ cũng không mấy quan tâm kỹ cho lắm.Tuyệt vời :)))
5
355857
2015-06-09 21:41:30
--------------------------
