421133
5787
Thật thà là cha giả dối.Quả không sai khi các bạn đọc qua quyển sách này. Tôi đã mua được gần đủ bộ, đọc để bản thân sống tốt hơn và để dành cho bọn trẻ con tôi sau này lớn lên cũng có thể đọc. Mình nghĩ rằng đây là bộ truyện các bậc phụ huynh nên có để cùng đọc và cũng trả lời câu hỏi với con của mình, góp phần hoàn chỉnh tính cách, làm giàu thêm tâm hồn của trẻ con.Giáo dục con trẻ không bao giờ là quá muộn vì "Hiền dữ đâu phải là tính sẵn, Phần nhiều do giáo dục mà nên"
3
505896
2016-04-24 15:18:28
--------------------------
386999
5787
Cuốn “Gieo mầm tính cách – Thật thà” nằm trong bộ 6 cuốn do Nhà xuất bản trẻ phát hành. Cuốn này có các câu chuyện như : Không tham của người, Chuyện con vịt, Chú bé báu dâu tây, Người tiều phu thật thà, Con bò vô chủ, Cái vòng của sự thật, Chọn vua, Thề có mẹ, Những tấm hình dán, Những chuyện nhỏ nhặt, Thành thật, Chú bé nói dối.Sau khi bố mẹ đọc truyện cho các bé nghe, bố mẹ và bé sẽ cùng nhau đọc phần “Cùng suy nghĩ” để trả lời các câu hỏi liên quan đến truyện đó, củng cố lại lần nữa những bài học mà truyện muốn truyền tải.
2
15022
2016-02-26 14:05:01
--------------------------
279812
5787
"Những người tính nết thật thà/Đi đâu cũng được người ta tin dùng" (Ca dao).Thật thà khiến người ta tin tưởng, khi được tin tưởng, người ta sẽ yêu thương, giúp đỡ, hỗ trợ mình. Còn không thật thà, dù chỉ một lần thôi, sau đó nói gì, làm gì cũng không ai tin nữa. Người phương Đông nói chung, người Việt nói riêng, sống trọng về tình cảm, khéo léo quá, nên nhiều khi... không thật, trong khi thật thà lại là một trong những đức tính quyết định thành công đối với mỗi người. Do đó quyển sách này rất hữu ích, từng câu chuyện ngắn gọn, ngôn ngữ trong sáng, dễ hiểu. Đây là sách nên mua cho các bé đọc. 
5
332766
2015-08-27 09:47:27
--------------------------
255541
5787
Để luôn được tin yêu, cần ươm mầm tính cách thật thà, trung thực với bản thân và với người khác trong bất cứ hoàn cảnh nào. Khi gieo niềm tin trong lòng người khác, bạn sẽ xứng đáng nhận được sự tin yêu của mọi người. Dũng cảm, trung thực, thẳng thắn nhận lỗi khi làm điều sai trái, càng được tin tưởng và yêu thương. Quyển sách nhỏ với nhiều hình ảnh minh họa dễ thương, nhiều câu chuyện đơn giản, gần gũi trong cuộc sống dạy cho chúng ta nhiều bài học hay. Rất cám ơn tiki.


5
110777
2015-08-06 09:32:10
--------------------------
166337
5787
Có câu "Honesty is the best policy."
Trung thực là 1 phẩm chất mà ai cũng phải canh cánh để duy trì nó suốt đời. Lầm lạc về nó, con người ta sẽ dối trá mãi mãi. Đi học cũng có thể nói là đi chơi điện tử, lớn lên một chút có thể dối trá mà làm những hành vi mất đạo đức. Bởi thế phải rèn trẻ em từ đầu, giáo dục chúng thế nào là trung thực. Trung thực có thể giúp ta thanh thản, hạnh phúc về tâm hồn như sao. Nhà Xuất Bản Trẻ đã thay ta chuyển tải điều đó tới tâm hồn non nớt của trẻ em qua 2 từ ngữ giản dị và gần gũi hơn: Thật Thà. Hình vẽ của cuốn sách này khá bắt mắt, trong sáng, bởi vậy mà những bài học này khiến cháu mình rất thích thú!
5
114793
2015-03-12 15:02:44
--------------------------
