522413
6114
Bị mất động lực và xuống tinh thần, sau một vài ngày suy nghĩ thì quyết định mua sách về sale để lấy lại những nhiệt huyết trước đây. Và hôm nay tôi nhận được sách! Mở 1 cuốn ra, đọc vài dòng đầu tiên, và tự hỏi tại sao đã không quyết định mua sách sớm hơn. Chỉ vài dòng ngẩu nhiên thôi cũng đã khiến tôi không thể rồi mắt khỏi cuốn sách, Brian Tracy đã làm tôi thay đổi trước tiên là từ nhận thức. Và với những điều thật đơn giản nhưng thấm vào đến tận tim, gấp sách lại tại chương 2 và không làm gì khác hơn là viết liền một bảng mục tiêu to đùng cho năm mới và những điều cần làm để đạt được mục tiêu. Không chỉ là những kỹ thuật hay cách thức bán hàng, tác giả giúp người đọc phát triển toàn diện từ nhân cách và nội lực bên trong cho đến bên ngoài. Đọc đến đâu là ý tưởng cứ ra ầm ầm đến đó, thực sự, sau 2 cuốn Phong Cách Bán Hàng và Hẹn Bạn Trên Đỉnh Thành Công của Zig Ziglar tôi đọc 2 năm trước, đây là cuốn sách hay nhất mà bất cứ người bán hàng nào cũng cần phải có. Hôm nay tôi có thêm 1 thần tượng nữa là Brian Tracy. Cảm ơn hai tác giả rất nhiều vì những từ ngữ như có lửa trong đó đã giúp tôi có thêm động lực và định hướng cho mình. Xin cảm ơn Tiki vì dịch vụ khách hàng chu đáo!
5
1109268
2017-02-09 23:39:02
--------------------------
442817
6114
Mình rất thích và đam mê bán hàng, đọc xong quyển 12 tuyệt kỹ bán hàng mình càng yêu thích bán hàng nhiều hơn nữa. Quyển sách chứa đựng những tuyệt chiêu bán hàng xuất sắc mà tất cả những ai tham gia vào lĩnh vực bán hàng nên đọcvà phải đọc. Những tuyệt kỹ chứa đựng từ những kinh nghiệm đáng quý của Brian Tracy mang đến cho chúng ta. Chúng ta nếu muốn trở thành nhà bán hàng xuất sắc thì hãy đọc để nâng cao kỹ năng, tuyệt kỹ bán hàng để có thể cảm nhận sự cao đẹp của nghề bán hàng!
5
702724
2016-06-05 18:11:41
--------------------------
349299
6114
mình có đọc sơ lược qua bằng bản epub thấy hay quá nên mua cho các bạn bán hàng đọc và dành cho mình 1 cuốn để đọc hiểu. Nhiều "tuyệt kỹ" trong sách mình phải học qua nhiều khóa học bán hàng, các khóa học tâm lý bán hàng, các khóa quản trị , tốn rất nhiều tiền và thời gian.  Vậy mà chỉ trong 12 "tuyệt kỹ" này gom trọn và còn nhiều chương cực hay. 
Mình bắt các bạn bán hàng phải "thuộc lòng" và phản hồi và tiếp đến trao đổi với quản lý và mình để các "tuyệt kỹ" được sử dụng hiểu quả hơn trong ngành của mình.
MỘT CUỐN SÁCH PHẢI ĐỌC ĐỂ BÁN HÀNG!
5
477948
2015-12-08 08:00:02
--------------------------
347415
6114
Ấn tượng về Brian Tracy từ lâu nhưng đây là lần đàu tiên tôi đọc một tác phẩm của ông về bán hàng. Thật sự những gì ông chia sẻ trong quyển sách đáng giá bằng bao năm nỗ lực tích lũy kinh nghiệm của một người bán hàng. Điều đó đã giúp những ai đọc cuốn sách này tiết kiệm được rất nhiều thời gian, đi trên đôi vai người khổng lồ để học hỏi và phát triển bản thân trong sự nghiệp bán hàng. Trước khi làm một việc gì đó nên có một mục tiêu, một kế hoạch cụ thể. Cuốn sách này sẽ rất thiết thực cho một nhân viên bán hàng hình thành kế hoạch đó và thực hiện nó.
5
78338
2015-12-04 13:14:55
--------------------------
300397
6114
Để bán hàng được, đôi khi cần phải những tuyệt chiêu, những kỹ năng, những khéo léo mà một người bán hàng cần phải có. Cuốn sách đã được tác giả mô tả kể lại từ những kinh nghiệm của chính mình để giúp người đọc hiểu thêm và có thêm những kỹ năng cần thiết để bán hàng. Nhờ đó mà giúp cho người đọc cải thiện hơn chính bản thân mình, giúp cho chính bản thân kiếm được nhiều tiền hơn, những khó khăn, những trở ngại sẽ không còn nữa.
Cám ơn tác giả đã chia sẽ những kinh nghiệm quí báu.
5
358906
2015-09-13 22:11:06
--------------------------
163170
6114
Ở Việt Nam, nghề nhân viên bán hàng đa số bị mọi người coi thường. Ít cha mẹ nào muốn con mình khi ra trường lại làm nhân viên bán hàng. Nghề bán hàng bị đối xử bất công như vậy mặc dù bán hàng là một giai đoạn quan trọng nhất trong các công ty và nhân viên bán hàng là một mắt xích quan trọng nhất trong tổ chức. Điều đó càng được khẳng định trong thế giới kinh doanh ngày nay. Tôi đã, đang là sẽ luôn luôn là một người bán hàng nên bản thân rất thích những quyển sách về nghệ thuật bán hàng, đặc biệt là sách của tác giả Brian Tracy. 12 tuyệt kỹ bán hàng là quyển sách đúc kết nhiều kinh nghiệm bán hàng của ông từ khi chập chững bước chân vào nghề. Hãy đọc để nâng cao kỹ năng, bán hàng được nhiều hơn, kiếm được nhiều tiền hơn và quan trọng nhất là cảm thấy ngày càng yêu nghề bán hàng - một nghề tuyệt vời.
5
387632
2015-03-04 12:07:13
--------------------------
