368801
6652
Cá nhân tôi, quyển sách này có kích thước khá nhỏ gọn, dạng như sách bỏ túi, lại hợp giá đối với các bạn HS. Tuy nhiên, vẫn không thể tránh khỏi những nhược điểm, như: việc giải thích các từ của vài vần không rõ nghĩa, vẫn còn chung chung, chưa khái quát, cụ thể, không những vậy, VD minh họa chỉ xuất hiện ở mười mấy từ trong 1 vần, chứ không bao trọn, điều khiến tôi thất vọng ở sản phẩm này có những từ cần VD để dễ hiểu hơn lại không có, giải thích thì qua loa...Nói chung là quyển từ điển này vẫn chưa đạt yêu cầu tôi lắm!
3
1078339
2016-01-14 17:16:22
--------------------------
351110
6652
Cuốn từ điển trình bày khá khoa học và mạch lạc nên sử dụng khá đơn giản và nhanh chóng. Từ ngữ được giải thích một cách dễ hiểu, ngắn gọn và súc tích, các ví dụ minh họa hợp lí, dễ liên tưởng và hợp hoàn cảnh. Nhờ sử dụng sách mà vốn từ của tôi được cải thiện rất nhiều. Chất lượng giấy tốt, nhìn sáng và rất vừa mắt. Kích thước sách cũng nhỏ gọn nên rất tiện để nhét vào túi xách và sử dụng khi cần thiết. Sau vài tháng sử dụng. tôi cảm thấy vô cùng hài lòng. 
4
372517
2015-12-11 18:49:10
--------------------------
241670
6652
Do đặc thù công việc ít khi giao tiếp cũng ít khi viết lách nhiều nên khi đụng đến việc gì phải trình bày ý kiến thì mình gặp ít nhiều khó khăn. Qua một bài báo hướng dẫn để trở thành một người viết tốt, họ khuyến khích mình rất nhiều mẹo hay, một trong số đó là trang bị cho mình một cuốn từ điển tiếng Việt. Thường xuyên tra cứu để hiểu nghĩa và áp dụng hàng ngày thì vốn từ vựng mình cũng sẽ được mở rộng ra, sau này khi cần trình bày thì biết sử dụng câu từ nào phù hợp, ngắn ngọn mà vẫn truyền tải đủ ý. Mình mua về dùng được hơn một tuần này, quyển này giải thích từ rất xúc tích, dễ hiểu. Thật sự rất ưng ý :)
5
150978
2015-07-25 18:54:21
--------------------------
