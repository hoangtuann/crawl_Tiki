466598
5220
Nhận cùng cuốn 86 mà cuốn này bị nếp nhăn ở phần góc. Cũng không đáng kể lắm nhưng mình cũng muốn chia sẽ. Nhưng mình rất thích ở tiki ở những điểm nhiệt tình, kỉ lưỡng, cẩn thận.
Tập này cũng rất là hay, tập nào của conan cũng hay hết. Tính ra mình biết conan đc tròn 10năm, sưu tầm conan hồi còn 7 tuổi không bỏ lỡ tập nào hết.
Mỗi tập là 1 vụ án khác nhau nhưng những bi thương thù hận điều giống nhau cả. Cũng từ xuất phát từ lòng thù hận ganh ghét. Cuối cùng thì chẳng được gì cả.
Tập này xem xong rút ra được bài học đừng nghĩ bạn có tiền bạn muốn gì cũng đc, muốn có tất cả, muốn người khác nghe theo mình. Rất dễ và cũng rất khó sẽ có những người không tham lam như bạn nghĩ
4
1209811
2016-07-02 14:53:19
--------------------------
444099
5220
theo chân Conan cũng 8 năm rồi, từ ngày mình học cấp 2, bây giờ cũng đã 8 năm, đọc qua bao nhiêu tập, kinh qua bao nhiêu vụ án, giết người vì tình, vì bạn cũng nhiều, những hiểu lầm không đáng có cũng không ít. Trong tập này, vụ án để lại cho tôi nhiều cảm xúc nhất là vụ án về tình bạn. Tình bạn đâu thể mua được bằng tiền, có mua được thì tình bạn ấy cũng đâu đáng quý. Ngay từ đầu hung thủ đã sai, sai khi nghĩ có tiền sẽ có bạn.
5
585221
2016-06-07 21:16:00
--------------------------
355696
5220
Vụ hoả hoạn ở Haido được dàn dựng theo vụ án nổi tiếng của Agatha Christie: Vụ án mạng ABC. Thoạt đầu mình không nhận ra cho tới khi Hattori nhắc tới ý các vụ án xảy ra theo thứ tự .. Đó là truyện khá hay của Agatha - nữ hoàng truyện trinh thám. 
Mình rất ấn tượng và do đó biết ngay thủ phạm là ai liền. Sao tác giả người Nhật này thích cho chồng bày mưu giết vợ hoài vậy nhỉ ? Làm như ổng bị bệnh tâm lí và sáng tác theo lối mòn tư duy !
5
535536
2015-12-20 14:59:27
--------------------------
351968
5220
Từ bé đến giờ tôi thích nhất là thể loại truyện trinh thám. Conan là truyện trinh thám mà tôi thích thứ hai ( vì tôi thích tiểu thuyết trinh thám hơn là sách tranh), hồi hộp có, yêu cũng có ( teen mà) , nhiều ý nghĩa nữa. Tập này khá là hay,bìa sách trang trí theo lối cũ nhưng vẫn rất thu hút. Mua liền một lúc mấy tập tại hồi trước mua ở hiệu sách thì bị hết mà giờ vẫn muốn mua thêm nữa. Tiki giao hàng nhanh, bọc cẩn thận, truyện thì vừa hay vừa rẻ. Nói chung là rất hài lòng.
Cảm ơn Tiki ạ.

5
743345
2015-12-13 11:43:38
--------------------------
168006
5220
Hồi hộp và hấp dẫn là những từ dành cho bộ truyện này. Đối với những bạn học sinh, Conan như trở thành một thần tượng, một người bạn với bộ óc cực kì thông minh ( trừ âm nhạc). Trong tập này, tôi thích nhất là vụ án cuối truyện, vừa trọn vẹn lại vừa là oán trách của đứa con với người mẹ. Sau tất cả, vẫn là cái kết hoàn hảo dành cho hai mẹ con, có lẽ, tác giả muốn một phần nào làm nhẹ các tình tiết. Tuy nhiên, không vì vậy mà truyện lại mất đi vẻ hấp dẫn, thay vào đó sẽ là sự tò mò của bạn đọc trong tập tiếp theo vì không biết vụ án nào sẽ xảy ra hay sẽ là cuộc đấu trí với bọn áo đen. Nói chung, Conan sẽ tiếp tục làm bạn đọc say sưa với từng trang sách, lật từng trang mà hồi hộp trong lòng.
4
13723
2015-03-15 20:18:33
--------------------------
