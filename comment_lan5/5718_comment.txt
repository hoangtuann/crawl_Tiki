515949
5718
Vừa được đọc ké cuốn sách nên mình tò mò lên đây xem ý kiến của những người khác thế nào. Bìa sách và nội dung đều khá kinh dị đối với một đứa nhát như mình, tuy truyện còn chưa thật sự logic, liên kết và rành mạch.
Về câu hỏi 3 - 1 = mấy xuyên suốt câu chuyện, mình xin mạn phép đưa ra ý kiến thế này: 3 là chỉ 3 đứa trẻ sinh ba, trừ 1 là ý chỉ đứa bé đã chết từ lâu, nhưng nó lại không phải đã  thật sự chết, mà sau khi được chôn cất có thể nó đã hồi sinh và lớn lên bình thường, ẩn mình trong xã hội người lớn và ngầm giúp 2 đứa kia. 
Khi truyện kết thúc với cái chết của 1 đứa bé chui lại vào bụng mẹ, thì ai đã khâu lại vết mổ trên bụng người mẹ đó, và tại sao có mẩu giấy ghi Kẻ tiếp theo sẽ là ngươi, có phải đó là ý chỉ bi kịch này còn chưa kết thúc, và vẫn còn lại 1 kẻ bí ẩn đang tồn tại. 3 trừ 3 là ý chỉ 3 đứa trẻ đều đã chết nhưng thật ra vẫn còn 1 đứa chưa chết thực sự. Cho dù là 3 trừ 1 hay 3 trừ 3, thì vẫn luôn có 1 kẻ giấu mặt làm thay đổi những đáp án thông thường. Mình suy đoán Phùng Kính chính là kẻ giấu mặt đó.
3
2108065
2017-01-27 00:21:48
--------------------------
497117
5718
Mình từng đọc truyện cánh cửa của Chu Đức Đông và thấy rất thích nên khi thấy cuốn này cũng cùng tác giả nên mình mua luôn , thật sự không cảm thấy ghê rợn j cả , cảm thấy hơi bị lạc đề sao sao nữa í , cảm thấy phí tiền
2
1168796
2016-12-20 20:18:11
--------------------------
455651
5718
Lần đầu tiên mình đọc truyện của tác giả Chu Đức Đông,cách tả nhân vật cậu bé trong truyện khá là ghê rợn,thấm nhất là những đêm vừa ngồi đọc vừa tưởng tượng ra cái mặt của nó,rùng mình chứ chẳng đùa.
Nhưng mà thấy hơi tiếc tiếc khi đọc đến mấy đoạn gần cuối,bắt đầu từ lúc mà ông Trương Cổ ổng chết,tiếc lắm,vì nghĩ ổng sẽ tìm ra được nguyên nhân và giải tỏa nỗi lo lắng cho cả xóm,vậy mà ai ngờ đâu ổng cũng ngủm củ tỏi.
Một kết thúc mở,không biết có nên hy vọng là sẽ có phần tiếp theo không :D
5
1226086
2016-06-22 19:08:35
--------------------------
448170
5718
Truyện hay,nội dung cũng khá đời thường nhưng tạo được sức hút cũng như sợ hãi
Đọc đến phút cuối truyện mình rung người len phát cái,cảm thấy ớn lạnh kinh lên
Nội dung truyện tuy vậy vẫn còn mơ hồ và chưa được giải đáp,như vì sao cả hai đứa trẻ đều không lớn lên?Vậy ai bị thiêu cháy?Ai chui vào bụng người mẹ?Hay vụ tự hỏa thiêu kia là màn kịch?Hay đứa trẻ đã chết trước đó làm nên?
Tuy có nhiều thắc mắc chưa được giải đáp nhưng mình nghĩ đó chính là ý đồ của tác giả,nếu thật là vậy thì quả là vị tác giả đại tài
5
570591
2016-06-15 15:19:43
--------------------------
419631
5718
Mình là fan cuồng của thể loại truyện trinh thám-kinh dị Trung Quốc. Trước giờ mình chỉ đọc Quỷ Cổ Nữ, Lôi Mễ, Sái Tuấn, Hồng Nương Tử . Lần này quyết định đổi qua Chu Đức Đông xem có gì mới lạ hơn. Khúc đầu của truyện khá hấp dẫn, nhưng càng về sau, tình tiết và cách dẫn dắt trở nên lan man, hơi nhàm. Cốt truyện mới lạ, nhưng cách dẫn dắt thì lại khiến mình thất vọng. Nếu đánh giá theo thang điểm 10 mình thấy chỉ xứng đáng 4/10 so với các tác phẩm mình đã đọc. Hy vọng sẽ tìm đọc tác phẩm hay hơn của Chu Đức Đông. 
2
316078
2016-04-21 17:08:45
--------------------------
404233
5718
Truyện ma mà hồi nhỏ đến bây giờ mình sợ nhất chính là ma búp bê , sợ ám ảnh luôn , công nhận lúc đặt hàng là một chuyện mà cầm cuốn truyện trên tay với cái bìa hình ảnh đó là 1 vấn đề khác , trớ trêu thay 
Những đứa trẻ đặc biệt từ 2 tuổi trở xuống nhìn giống búp bê kinh luôn , đặc biệt là trẻ sơ sinh , một đứa trẻ không lớn được như nội dung đã kể trên thì thôi đi , thiệt ra là mình giới thiệu bạn của mình mua , mỗi lần để 1 cuốn truyện ma trong nhà mà ám ảnh ớn lun , bạn mình cũng cho mình đọc 1 vài trang cuối , 12/ 12 là ngày đứa trẻ sinh ra và chết đi trong bụng mẹ , mẹ là mồ chôn của nó và là nơi an toàn nhất , thật....
Mong mấy bạn hãy đọc để ám ảnh , tư duy thử nha
5
1178806
2016-03-24 21:01:40
--------------------------
383592
5718
Cách viết của CĐĐ thực sự rất lôi cuốn. Đây thực sự là một cuốn sách khiến ta thức quá giờ đi ngủ. Đọc xong không dám nhìn lại bìa sách vì thực sự cốt truyện và lối miêu tả của tác giả rất hấp dẫn,kinh dị, bất ngờ. Rất đáng mua. Tuy nhiên mình không hiểu phần kết của nó là như thế nào, và có một vài chi tiết ở phần cuối mình tìm mãi cũng không hiểu phải trả lời nó ra sao nữa. Xuyên suốt cả câu chuyện chúng ta luôn thắc mắc, rốt cuộc thì 3-1= mấy?
3
1172682
2016-02-20 22:40:31
--------------------------
370045
5718
Cốt truyện đầy kịch tính và có phần bí ẩn, thật sự tác giả đã khiến mình phải hồi hộp, xen lận cảm giác sợ sệt khi cố theo dõi truyện. Truyện có yêu tố bí ẩn và giải đáp bí ẩn cũng rất hợp lí. Nhưng phần kết thật sự chưa cảm thấy đã và cũng có đôi chút khó hiểu. Tuy nhiên truyện thật sự rất hay và đây là câu truyện đầu tiên khiến mình phải đọc 1 lèo từ đầu đến đuôi. Rất cảm ơn tác giả đã sáng tác ra 1 câu truyện hay và lôi cuốn như thế
3
798494
2016-01-16 23:20:47
--------------------------
369461
5718
Truyện này không hay như mình nghĩ. Câu hỏi 3-1=? Thực sự không phù hợp với nội dung câu truyện cho lắm. Mình hơi thất vọng. Theo như mình cảm nhận thì hơi lan man, không đúng trọng tâm. Phải nói tới 90% nội dung là nói về đứa bé mà ko phải cốt lõi của sự việc. Nếu đem truyện này ra so sánh với đau thương tới chết hay kỳ án ánh trăng thì chỉ đc 3/10 điểm thôi. Mình hơi khó tính trong chuyện đọc sách nên viết nhận xét gắt gao. Vô cùng xin lỗi tác giả. Thank you so much
1
82802
2016-01-15 20:27:28
--------------------------
365317
5718
Mình thật sự ấn tượng với bìa truyện ngay từ cái nhìn cái đầu tiên và quyết định mua ngay. Chất liệu giấy tốt cùng với bookmark làm mình mê say đắm quyển nay. Nội dung truyện miêu tả không ma quái gê gớm lắm nhưng vẫn tạo cho mình cảm giác rợn người, rùng rợn cho đọc giả. Nhưng thật tiếc là sau khi đọc hết truyện mình vẫn chưa hiểu hết được. 3 -1 có nghĩa như thế nào. Tác giả có đề cập tới rằng nếu khám phá ra bài tình trừ nay thì sẽ hiểu ra bí mật toàn bộ câu chuyện. Nhưng nói thật lòng mình không tài nào hiểu ra được.
3
401249
2016-01-07 20:40:32
--------------------------
344535
5718
Lúc nhận được sách thì cái hộp đã bị móp , giao hàng hơi trễ (hơn 1 tuần) nhưng mình lại rất hài lòng về cuốn sách này cho nên mấy cái lỗi nhỏ nhặt kia cũng không là gì. Theo mình, đây là cuốn sách rất cuốn hút vì  mình đọc chỉ trong 1 ngày. Bìa rất kinh dị và đầy ám ảnh nhưng nó không liên quan đến sách lắm. Mình thấy cái kết rất kỳ lạ mà cũng rất đáng sợ(đứa bé mổ bụng mẹ rồi tự chui vào) nhưng cái câu cuối cùng ghi ngày sinh của nó là 12 tháng 12 thì không hiểu lắm. Còn nhan đề thì mình nghĩ  '3-1=mấy' nghĩa là ba đứa con đã chết 1 đứa thì trên đời thật sự còn mấy đứa tồn tại. 
5
944421
2015-11-28 17:34:29
--------------------------
344162
5718
Tuy truyện có nội dung không có gì đặc sắc, chủ yếu là nói về đứa bé nhưng cách hành văn của Chu đức đông đã khiến người đọc cảm thấy rùng mình.Truyện tuy ngắn nhưng vẫn thấy đáng để đọc, đây là tác phẩm đầu tiên mình đọc của Chu đức đông nên cảm thấy khá ổn. kết thúc hơi khó hiểu, đọc xong mà mình k chắc ai là người đứng sau đứa bé đáng sợ đó. 
THiết kế bìa rất đáng sợ, mua về mình phải bọc vào ngay, chứ vừa đọc vừa ngó cái bìa mà dựng tóc gáy
4
619746
2015-11-27 22:11:41
--------------------------
342432
5718
Mình đã mua một cuốn này về rồi! Chưa kịp đọc thì mấy đứa bạn nó mượn về nhà coi luôn. Nó nói truyện này hay quá. Mày cho tao mượn nào đọc xong tao trả cho. Truyện của Chu Đức Đông đọc rất lôi cuốn và hấp dẫn. Nhìn bìa sách là mình đã có hứng thú để đọc rồi. Bên trong câu truyện rất li kì. Bạn nào muốn trải nghiệm cãm giác giống mình thì. Mình khuyên nên mua cuốn này. Truyện kể về Bé Xoa và 3 người phụ nữ đã đem bé Xoa về nuôi. Tóm lại Mình Khuyên Mấy Bạn Nên Mua Cuốn Này Về Đọc Thử
5
976530
2015-11-24 13:57:27
--------------------------
341656
5718
Mình biết Chu Đức Đông qua tác phẩm Cưới Ma nên khi truyện này được xuất bản mình đã mua nó. So với Cưới Ma thì đây quả là một thất vọng lớn, mặc dù tác phong viết truyện của Chu Đức Đông khá hay song nội dung thì quá nhàm chán, rời rạc, độ logic của truyện thì... quá, cả câu chuyện thì phần tớ thấy logic nhất là phần "vài lời nói thêm" :"D.
Dù sao thì vẫn hóng tác phẩm mới của tác giả ~(`v`~) và mong rằng tác phẩm mới hấp dẫn như Cưới ma, thậm chí còn tốt hơn nữa *hóng-ing* .

1
396060
2015-11-22 17:32:16
--------------------------
332486
5718
Tác phẩm 3-1=mấy của Chu Đức Đông ngay phần mở màng đã rất lôi cuốn. Sự xuất hiện bí hiểm của bé Xoa đã tạo nên không khí rùng rợn, kinh kinh dị dị cho truyện. Và từ ngày bé Xoa được ba hộ gia đình nhận nuôi thì bị kịch đã xuất hiện trong gia đình của họ. Cũng kể từ bi kịch thứ nhất xảy ra là câu chuyện giảm ngay độ hấp dẫn,  không còn không khí bí hiểm,  không còn vẻ ma quái như lúc đầu nữa. Tất cả các tình tiết điều tra về bé Xoa, về bà già thu mua phế liệu của nhân vật dẫn truyện đều cực kỳ trẻ con, ngô nghề,  đọc mà có cảm giác như tác giả đang cố tình đùa giỡn với độc giả. Nói chung, tuy là truyện kinh dị ma quái nhưng cũng phải đảm bảo logic vì nó là nền tảng, còn 3-1 đã không đảm bảo được yếu tố hợp lý,  tất cả chỉ là một tổ hợp rời rạc giữa các nhân vật.
1
180185
2015-11-06 13:50:56
--------------------------
327316
5718
Mình vừa đọc xong quyển này, ý kiến cá nhân thì nó không hay bằng cưới ma, nhưng vẫn là một cuốn sách đáng đọc. 
Câu chuyện về đứa trẻ không bao giờ lớn mang đến cho thị trấn nhỏ Tuyệt Luân Đế biết bao tai họa. Gợi mở ra bài học về tình mẫu tử, đứa trẻ nhẫn tâm gây ra biết bao tội ác nhưng cuối cùng vẫn chọn cách chết trong bụng mẹ, như một cách rời xa thế giới này, sống ở một nơi an toàn tuyệt đối.
Tuy nhiên truyện vẫn nhiều tình tiết không hợp logic, đứa trẻ ấy được tả không bao giờ lớn, nhưng lại có lúc thành người đàn ông trưởng thành kết hôn với Liên Loại. Tựu chung lại vẫn là cuốn sách khá hay, đáng đọc.
4
470842
2015-10-27 16:14:37
--------------------------
322813
5718
Nếu bạn là một độc giả trung thành của dòng truyện kinh dị thì có thể quyển này đáng để trải nghiệm. Tuy không có những tình huống cao trào đáng sợ đến nghẹt thở nhưng cũng tạo sự lôi cuốn nhất định, khiến cho bạn phải đọc tiếp cho đến hết quyển truyện, Chu Đức Đông đã khá là thành công khi xây dựng cốt truyện, nhân vật, các tình huống, hư hư thực thực, pha lẫn tính kinh dị cần có. Đây là quyển truyện đầu tiên mua trên tiki, mình khá là hài lòng, và sẽ gắn kết với tiki trong nhiều sản phẩm sau này.
4
699286
2015-10-17 02:50:39
--------------------------
314827
5718
Cuốn sách 3 trừ 1 bằng mấy . Khi mới bắt đầu người đọc có thể cảm nhận được sự hồi hộp và bí ẩm trong câu chuyện. Trương Cổ một chàng trai hiền lành tốt bụng sống trong một thị trấn nhỏ. Cuộc sống bình thản của anh kết thúc khi anh nhận nuôi bé Xoa. Sau một thời gian mọi chuyện lạ bắt đầu xảy ra . Biết bao nhiêu cái chết, bao nhiêu con người, bao nhiêu vụ án mạng xảy ra nó khiến anh càng ngày càng lấn sâu vào , từ những tình tiết gây cấn mang đến cảm giác hồi hộp, lo sợ. Không nhận thức được mình đến khi nào mới thoát khỏi,  mọi chuyện khi nào mới kết thúc.  
5
285257
2015-09-27 12:44:35
--------------------------
287607
5718
Đọc một mạch 3-1 bằng mấy trên chuyến máy bay công tác, cảm thấy có một chút thất vọng. Chu Đức Đông vẫn rất thành công khi tạo không khí u ám, bí ẩn bằng giọng văn dửng dưng như anh đã làm trong Đám cưới ma. Thế nhưng cái kết hụt hẫng và nút thắt được tháo gở một cách khiên cưỡng khiến tác phẩm này không tạo được ấn tượng như vị tiền nhiệm. Ý nghĩa, thông điệp của truyện được truyền tải mơ hồ. Về phần kinh dị, truyện có những phân đoạn hồi hộp nhưng không làm người đọc sợ hãi. Dù vậy, Chu Đức Đông vẫn lôi cuốn người đọc đến cuối cùng cũng là một thành công. 
2
65308
2015-09-02 21:35:39
--------------------------
270298
5718
Đây lại là một cuốn kinh dị khác của Chu Đức Đông không khỏi khiến cho người đọc lạnh gáy với các tình tiết trong truyện. Khi đọc thì mình rất bất ngờ với tên sát nhân cuối cùng. Nói chung là đang đọc mà cứ phải ngoái đầu lại nhìn ra đằng sau xem có ai không :( 3 trừ 1 thì bằng 2 nhưng với câu chuyện này mình vẫn chưa hiểu lắm dụng ý của tác giả, tên truyện hơi mông lung, mặc dù là nó có thể ám chỉ số đứa trẻ nhưng kết truyện vẫn chưa rõ ràng lắm. Nói chung là với hơn 300 trang và với giá này thì mình thấy cũng ổn rồi.
4
366277
2015-08-18 09:24:55
--------------------------
257397
5718
Mình rất ấn tượng với cuốn Cưới Ma của Chu Đức Đông nên quyết định mua tiếp cuốn này về. Nhìn bìa đã thấy đậm chất kinh dị rồi, hình thức ok nhưng về nội dung thì mình không khoái lắm, cảm giác đọc xong chẳng thấy thoả mãn gì cả mặc dù tác giả có gợi ý để độc giả tự suy ngẫm. Có lẽ tại đoạn kết hụt hẫng + hơi khó hiểu làm mình gấp sách lại và hơi nản. Điểm cộng cho cuốn sách chỉ là tác giả đan xen một số tình tiết khá đột phá. Thôi thì mỗi người một cảm nhận khác nhau vậy :))
3
419258
2015-08-07 17:07:28
--------------------------
254584
5718
Truyện dài kinh dị này của Chu Đức Đông , cũng như Minh Hôn - Cưới Ma, có cách dẫn dắt và miêu tả những yếu tố kinh dị vô cùng tuyệt vời : một câu hỏi tưởng chừng như đơn giản, một đứa bé tưởng chừng như ngây thơ, một thế giới tưởng chừng như yên ả...tất cả đều được Chu Đức Đông dàn dựng rất tuyệt , rồi bỗng chốc, toàn bộ sóng gió được đẩy lên, đưa người đọc vào một chuỗi các sự kiện kinh dị. 
Nhưng, cũng như Cưới Ma, cái kết khá tệ và khá khó hiểu, tuy nhiên, vẫn rất đáng tìm đọc
4
162265
2015-08-05 12:48:45
--------------------------
251250
5718
Truyện khá là khó hiểu, nhất là sau khi Trương Cổ gặp được đứa bé hát rong thì càng khó hiểu hơn nữa. Kết thúc thì coi như tạm hiểu một nửa, rốt cuộc thì Phùng Kình đóng vai trò gì trong truyện ? Chẳng biết rốt cuộc có bao nhiêu đứa bé nữa, là ba đứa hay một đứa ? Chỉ vì một chút tò mò mà Trương Cổ chết một cách thảm thương và... lãng xẹt như vậy :| Trang cuối cùng của truyện Chu Đức Đông đã gợi ý một số chi tiết để độc giả tự suy đoán, riêng mình thì mình đọc xong... chả hiểu gì luôn !!
P/s : mình thích bìa của truyện, Chu Đức Đông khá vui tính khi có cả khúc đặt bản thân vào truyện :))
2
621913
2015-08-02 16:18:36
--------------------------
248074
5718
Quyết định mua sau khi đọc cưới ma của Chu Đức Đông. Thường thì đọc được cái truyện hay là ngồi tìm hết mấy cái truyện của tác giả đó để đọc. Cảm giác sau khi đọc là cái quái gì vậy, mình vẫn không hiểu nổi cái ẩn ý của tên sách là sao. Quá buồn vì suốt thời gian qua đọc đi đọc lại mà vẫn không hiểu được chắc vì IQ thấp : (( So với cưới ma thì thấy không hay bằng. À còn cái bìa thấy rợn toàn phải cất nó ở giữa giữa cho đỡ bị giật mình.
3
130214
2015-07-30 17:12:47
--------------------------
244714
5718
Đầu tiên phải nói đó là thiết kế bìa sách rất có tính hù dọa người khác à, nhìn vào thì thấy ngay là kinh dị rồi. Tên truyện thì khỏi nói, cực kì lạ là gây sự tò mò. Nội dung thì khá hay, mới lạ. Cách dẫn người đọc vào các tình tiết của truyện rất hợp lí và trình tự không bị đứt đoạn, nói chung lối viết văn của Chu Đức Đông là miễn chê được rồi. Truyện không đủ tính kinh dị để gây ám ảnh ngoại trừ cái bìa sách. Cái kết cũng khá hợp lí và có tính giáo dục cao. Đọc hết trang vẫn còn có cảm giác thắc mắc, và hơi khó hiểu, còn hơi lưng lủng. Nhưng tóm lại khá hay. 
4
571330
2015-07-28 13:32:01
--------------------------
220109
5718
Lần đầu tiên minh xem truyện của Chu Đức Đông, bố cục, mạch văn câu chuyện khá mạch lạc, rõ ràng, nộ dung theo mình cũng chưa đến độ gây cho người đọc sự ám ảnh, ghê rợn. Điều mình thich ở cuốn tiểu thuyết này là nhà văn dẫn người đọc đến hết sự tò mò này đến sự tò mò khác về cậu bé đó là ai?  Qua đó, nói lên được tầm quan trọng của việc giáo dục nhân cách con người. Nói chung, 3-1= Mấy? vẫn là một tác phẩm đáng xem cho những bạn đọc yêu thích thể loại kinh dị.
4
467193
2015-07-02 11:42:11
--------------------------
166447
5718
Đây là cuốn mình đọc ké! ^^ hôm nọ rảnh rỗi vào Nhân Văn ngồi lì trong đó đọc cuốn này, cha nội bảo vệ canh me suốt, nhưng kệ quyết tâm đọc hết mới về =)) cuốn này nói chung cho những bạn nào bắt đầu đọc tiểu thuyết kinh dị thôi, chứ mấy mọt hoàn toàn có thể đoán được những tình huống của truyện(mình trong số đó :)) ), cách dẫn truyện của tác giả dựa trên các tình huống tuy phần đầu có phần rời rạc nhưng lại cực logic vào phần sau, nhưng theo cá nhân mình thấy cách bắt đầu câu chuyện bằng cách đặt câu hỏi 3-1 bằng mấy thì có vẻ không hay cho lắm, nghe nó không thật thế nào! 
Trước cũng đọc quyển Địa Ngục Tầng 19 của Sái Tuấn, nhân vật chính cũng hay hỏi Tầng thứ 19 địa ngục, nghe riết nhức đầu @@ lối viết của cuốn này cũng khá giống 3-1 bằng mấy. Nói chung đây vẫn là một cuốn tiểu thuyết hay cho những người nào mới bắt đầu. Trung Quốc thực sự là cái nôi của các tiểu thuyết kinh dị, sau Quỷ Cổ Nữ, Lôi Mễ, Sái Tuấn, thế hệ sau luôn sinh ra những nhà văn kiệt xuất!
4
581182
2015-03-12 19:22:29
--------------------------
160186
5718
Có lẽ Chu Đức Đông là tác giả thứ 2 mình hâm mộ sau Quỷ Cổ Nữ, vì sau khi đọc xong Cưới ma và bây giờ là 3-1=mấy, mình thực sự bị ấn tượng.
Về cuốn sách: 
-Tên rất thu hút, gây tò mò. Khi chưa nhìn tên tác giả mà chỉ nhìn tên truyện mình đã cảm thấy rất muốn mua rồi.
-Về phần bìa : có thể nói là nhìn khá đáng sợ, nhưng mình chỉ thấy cái bìa nó 'đáng sợ' kiểu tầm thường, doạ ma chứ không hề 'có ý nghĩa' lắm.
-Chất lượng giấy tốt, dịch không bị lỗi
-Nội dung : Ám ảnh. Khi mình giở cuốn sách ra, mình đã đọc ngấu nghiến không ngừng nghỉ. Nhưng đấy chỉ là đoạn đầu. Càng về sau truyện càng nhảm và khó hiểu vì tác giả muốn thêm thắt vài chi tiết có lẽ là không liên quan lắm để truyện bí hiểm hơn. Cái chết của nhân vật chính và vài nhân vật khác khá lãng xẹt vì nó quá nhanh, như kiểu viết cho xong ấy. Nhưng đến cuối cùng thì truyện vẫn để lại cho mình một nỗi sợ vô hình nào đấy.
=> nói chung là truyện cũng đáng mua nếu muốn thử cảm giác sợ, chứ nếu xét về tính logic thì truyện còn nhiều lỗi lắm.
4
64861
2015-02-22 22:32:18
--------------------------
155192
5718
Sau khi đọc đến trang cuối cùng, độc giả sẽ tự tìm câu trả lời cho câu hỏi, cũng là tựa sách " 3-1=mấy?". Tùy vào sự cảm nhận riêng và những chi tiết trong truyện mà mỗi người chú ý đến mà những câu trả lời sẽ khác nhau. ( Đó là lý do mà tác giả đã để lại địa chỉ mail cuối sách nhằm mong muốn nhận được nhiều câu trả lời của độc giả). Tuy nhiên, theo mình, cuốn sách đan xen nhiều chi tiết " có vẻ không liên quan lắm" vào để đi đến kết thúc cuối. Điều này có thể gây khó hiểu khi đọc. Chúc các bạn có những trải nghiệm thú vị!
5
550713
2015-01-31 13:34:19
--------------------------
151433
5718
Đọc hết truyện nhưng không hiểu cho lắm. Cũng có một vài tình tiết bất ngờ nhưng nếu xếp vào thể loại kinh dị thì thật sự là không đúng lắm. Mình đọc từ đầu đến cuối nhưng không thấy kinh dị chỗ nào. Có chăng chỉ là hai ba chap cuối nhưng thật sự là mình không hiểu cuối cùng đứa bé ấy là ma hay là người hay chỉ là sản phẩm tưởng tưởng của Trương Cổ. Mình cũng không hiểu chi tiết bà già bán ve chai sinh được ba đứa xong chết mất một thì có liên quan gì đến cái ác. Nhưng truyện này cũng có một vài bài học bình thường liên quan tới những cái xấu của mọi người được đề cập đến giúp chúng ta ý thức được không có bí mật nào là vĩnh viễn. Những lỗi lầm chúng ta mắc phải sẽ được phơi bày ra ánh sáng và chũng ta sẽ phải trả giá vì những lỗi lầm ấy.
Nhưng mình khá kết cái bìa. Nhìn xuôi thì không có gì nhưng khi quay ngược lại nhìn ánh mắt của đứa bé thì có gì đó rợn rợn.
1
80220
2015-01-19 19:44:21
--------------------------
147937
5718
Truyện với tựa đề không có gì kinh dị, còn bìa sách lại mang hơi hướng truyện ma. Nhưng khi đọc vào lại thấy những tình tiết giống như trinh thám pha lẫn chút rờn rợn không thể tả, nhưng nó hoàn toàn không phải là truyện ma. Thật may mắn là mình đã mua truyện này và đọc (vì nghe danh tiếng của tác giả) trước khi  lên Tiki để coi giới thiệu sách. Tiki chơi kỳ quá, tiết lộ luôn cả đoạn cuối cùng hấp dẫn, vậy thì các bạn đọc sẽ không hứng thú nữa.
Nhưng mà các tình tiết tiki chưa nêu thực sự cũng rất hay đấy. Các bạn có thể tự đọc để cảm nhận nhé.
5
174257
2015-01-09 13:27:07
--------------------------
145263
5718
Lúc mua về mình đã thật sự rất cân nhắc vì cái bìa nó làm mình ám ảnh, mình sợ nội dung câu chuyện sẽ gây cho mình nỗi ám ảnh về sau. Với giọng văn của Chu Đức Đông và các tình huống với những vụ án trùng hợp đến khó tin thì lúc mới đọc mình cũng đặt sự nghi vấn vào đứa trẻ mà Phùng Kình mang về.
Cái kết truyện làm mình hơi hụt hẫng và có lẽ đây là những điều Đức Đông muốn gửi gắm đến cái nhìn về sự giáo dục là rất cần thiết với việc hình thành nhân cách con người ngay từ lúc nhỏ.
Cái bìa thật sự làm mình sợ, không dám xem lại mà phải cất thật sâu vào tủ truyện :((
4
124177
2014-12-29 21:04:50
--------------------------
140033
5718
Lần đầu tiên đọc truyện của Chu Đức Đông. Xem giới thiệu thấy rất có hứng thú nên đọc thử. Thực sự là hay, "đứa trẻ không bao giờ lớn", truyện mang nhiều thông điệp mà phải đọc hết thì mới hiểu. Nhân cách con người, sự đối xử của xã hội, sự đố kỵ, những tệ nạn... xuất hiện đan xen giữa mạch truyện. Cái kết hơi đáng tiếc, và buồn. Nhưng mà Tiki ơi lược bớt phần giới thiệu đi thì đoạn kết độc giả mới bất ngờ và cảm nhận được nhiều điều hơn chứ mình biết trước một phần kết thúc rồi nên cũng không có nhiều cảm xúc và bất ngờ lắm.
5
208405
2014-12-09 20:59:35
--------------------------
133980
5718
3-1=Mấy? Không đơn thuần là một câu hỏi đơn giản.
Với một câu chuyện kinh dị đầy ám ảnh này thì câu hỏi của Phùng Kình không hẳn là một câu hỏi dễ trả lời và dễ tìm lời đáp.
Với sự xuất hiện của một đứa bé được nhận nuôi và một loạt những vụ án mạng xảy ra? Mọi sự nghi ngờ dần đổ về đứa bé. Nhưng ai mới là người gây ra tội? Mãi cho đến khi tới những trang sách cuối cùng được lật ra, mình mới nhận ra được hung thủ thực sự của vụ án.
Thích cái cách mà tác giả dẫn dắt người đọc đến với câu chuyện và những tình tiết. Có những gợi ý làm cho người đọc cố gắng phán đoán nhưng vẫn không dễ tìm được câu trả lời.
Thiết kế bìa đầy ám ảnh và cũng không kém phần đáng sợ.
Một câu truyện kinh dị pha lẫn chút trinh thám đầy mê hoặc :)
4
303773
2014-11-07 20:38:03
--------------------------
133593
5718
Mình thik cách hành văn của Chu Đức Đông từ Cưới ma, nên quyết định mua cuốn này về đọc. Cách miêu tả của tác giả thì khỏi phải bàn, các tình tiết trong truyện tuy ko hẳn là ma quái ghê rợn, nhưng vẫn tạo cảm giác gai gai rờn rợn cho độc giả. Nhưng đọc đến cuối truyện, mình vẫn không hiểu bài toán 3-1 hay 3-3 là như thế nào??? Tại sao Phùng Kình lại hỏi Trương Cổ bài toán bí hiểm, sau đó thì Trương Cổ gặp đứa bé???  Ở lời cuối tác giả có viết,  nếu khám phá ra bái tính trừ này sẽ khám phá ra bí mật toàn bộ câu chuyện, nhưng thú thật là mình chưa thể hiểu thế là như thế nào. Có bạn nào đã đọc và hiểu thì giải thích giúp mình với?!?!
4
5276
2014-11-05 23:18:51
--------------------------
