244728
3265
Tôi từ nhỏ đã theo truyền thống gia đình là theo Phật giáo. Nhưng tôi cũng không tha thiết lắm với nó nên cũng không tìm hiểu nhiều điều về Phật giáo. Và tôi cũng không có ý định sẽ đi tu mai này. Nhưng tôi vẫn chọn đọc quyển sách "Tự truyện một người tu" của Thích Hạnh Nguyện. 
Tuy là một quyển tự truyện của một Tỳ Kheo, nhưng từ đó rút ra được rất nhiều bài học từ những triết lý nhà Phật. Phải sống sao cho đúng, cho tâm được nhẹ nhàng, vui vẻ hằng ngày. Cách đối nhân xử thế cũng như đối mặt với những chuyện tiền tài danh vọng trong đời như thế nào cho phải.
5
74132
2015-07-28 13:37:55
--------------------------
