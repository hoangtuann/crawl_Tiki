310803
5364
Là một giáo viên nên mình rất quan tâm đến việc dạy cho tất cả học sinh trong lớp đều tiếp thu được những nội dung học. Mình sẽ rất buồn, nhiều khi rất bực khi học sinh không chịu học hay không nhớ những gì mình dạy. 
Sau khi đọc xong cuốn sách này, bản thân đã ngộ ra được rằng không phải tất cả các học sinh đều tiếp thu kiến thức bằng cách giống nhau. Nhận ra điều đó nên mình cũng cảm thấy thoải mái hơn khi dạy và cũng cố gắng đưa ra các cách học phù hợp với mỗi em. Tất nhiên là bạn phải mất thời gian phân loại tính cách từng học sinh nữa, không phải dễ nhỉ. Nhưng nếu bạn hiểu được vấn đề nằm ở đâu thì việc giải quyết nó sẽ dễ dàng hơn. 
Ngoài ra, bạn cũng sẽ khám phá ra những tính cách của chính mình hay của người khác nếu bạn để ý kĩ.
Tóm lại là mình cảm thấy rất thích thú khi xem xong quyển sách này và quyết tâm cần phải thay đổi điều gì đó cho phù hợp với việc dạy học của mình.
5
472150
2015-09-19 19:56:01
--------------------------
191069
5364
Mình rất thích cuốn sách này.Hẳn ai cũng biết mỗi đứa trẻ đều có một cá tính riêng biệt,vì vậy việc dạy dỗ cũng cân có nhiều cách,cuốn sách này nói về phong cách học tập của bản thân bạn và con bạn,cách dạy phù hợp đối với từng đứa trẻ,hơn hết cuốn sách còn đưa ra được nhiều lời khuyên cho các bậc cha mẹ .

Điều thứ hai mà mình thích là màu sách bìa cuốn sách,màu vàng chủ đạo cuốn hút cùng với hình ảnh những đứa trẻ vui đùa với nhau,đó là điều khiến mình không thể nào bỏ lỡ quyển sách này.
4
412058
2015-04-30 21:47:17
--------------------------
188364
5364
*Hẳn các bạn cũng biết rằng trẻ em như búp trên cành vì trẻ con rất đáng yêu nhưng rồi chúng cũng phải lớn lên. Hành trình đầu tiên của bé khi rời xa gia đình đó là trường học. Ở môi trường đó, không những chỉ bé bỡ ngỡ mà còn khiến phụ huynh và giáo viên hết sức băn khoăn, lo lắng về việc dạy dỗ cũng như hướng dẫn các bé trong việc học.
*Nếu bạn đang là học sinh, sinh viên, phụ huynh hay giáo viên thì đây là cuốn sách dành cho các bạn. Vì sao ư? Tại vì trong cuốn sách này, bạn sẽ có thể thấy được phong cách học tập đa dạng của trẻ cũng như chính bạn để bạn hiểu rằng mỗi người trong chúng ta là một cá nhân mang nét riêng biệt và trẻ cũng thế.
*Đồng thời, sách cũng đưa ra những cách khắc phục nhược điểm của các phong cách học tập để bạn có thể giúp trẻ và giúp chính bản thân cải thiện cách học.
*Thật sự nghiên cứu được trình bày trong sách rất đáng yêu.
*Hãy một lần cầm quyển sách "Mỗi đứa trẻ một cách học" trên tay và đọc để biết được những điều bổ ích mà cuốn sách mang lại bạn nhé.
5
45667
2015-04-25 10:25:14
--------------------------
