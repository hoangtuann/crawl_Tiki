347021
5404
Nội dung chủ yếu của cuốn sách kể về hành trình của 4 người đi lạc trong rừng sau 1 trận quyết chiến. Nhân vật chính là Tùng, lính tình nguyện Việt Nam bị bắt làm tù binh sau khi đại đội của anh bị quân Pol Pot phục kích, Saly (y tá), Ông Lớn và "tên lính áo đen". Đây là một trong những cuốn sách đề cập đến cuộc chiến mà sách vở vẫn còn ít viết tới, học và hiểu được nhiều điều. Tuy nhiên một số đoạn miêu tả sẽ gây khó chịu cho người đọc.
4
597342
2015-12-03 19:41:36
--------------------------
341870
5404
Đọc "Miền hoang", mọi tình huống, cốt truyện cho đến các ý tứ trong truyện đều rất độc đáo. Nổi lên trong tác phẩm là 4 nhân vật vừa là kẻ thù, vừa là đồng minh, là những con người còn sống sót sau trận chiến tàn khốc. Giữa họ, dù là người đến từ xứ văn minh (Tùng, SaLy) hay man rợ (Lục Thum, Rô) thì cũng đều có những dằn xé, mâu thuẫn trong nội tâm rất khốc liệt! Họ đều khát vọng tự do nhưng cũng rất bi kịch, không hẳn là đáng trách nhưng cũng thật đáng thương! Tôi nghĩ cái tài của Sương Nguyệt Minh và cái cuốn hút của tác phẩm này là như thế! Con người sống và tranh đấu để làm gì để rồi rốt cuộc họ không còn nhận ra những giá trị của cuộc sống, và nếu bị "lạc" thì tất yếu họ sẽ bị lãng quên giữa dòng đời, có lối đi nhưng không thể thoát!
5
195935
2015-11-23 10:56:23
--------------------------
