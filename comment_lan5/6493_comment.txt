450062
6493
Thoạt nhìn "Ngày dài 24 giờ" chỉ là một quyển sách hài hước với 10 vạn câu hỏi vì sao của cô người yêu khi xem bóng đá cùng bạn trai. Nhưng ẩn sâu bên đó là những góc nhìn quen thuộc mà lạ lẫm. Không phải là một quyển sách triết lí, giáo điều nhưng tác giả vẫn cho chúng ta thấy được những bài học đáng quý. "Hiện tại có thể thay đổi, tương lai có thể thay đổi, nhưng quá khứ thì không đổi". Vì vậy hãy làm tốt những điều ở hiện tại và tương lai vì sớm muộn gì nó cũng sẽ trở thành quá khứ, mà quá khứ thì không đổi.
4
1157252
2016-06-18 13:39:31
--------------------------
