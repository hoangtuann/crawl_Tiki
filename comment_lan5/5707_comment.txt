463041
5707
Mình rất hay coi phim. Một hôm tình cờ lướt trên internet thấy có bộ phim trò chơi của Ender. Thế là tò mò mình click vào ngay. Xem xong thấy hay quá nên mình và mấy bạn của mình quyết định lên mạng tìm thông tin thì mới biết được là đây là phim chuyển thể. Bắt đầu mình lên tiki tìm kiếm và đặt hàng ngay. Khi nhận được hàng mình mở ra đọc ngay. Truyện rất hay bối cảnh tương lai khi con người có thể sống trên tàu vũ trụ. Mình đọc cảm thấy thú vị mấy bạn nên đọc thử.
5
393298
2016-06-29 01:35:54
--------------------------
415116
5707
Mình đã một thời rất mê bộ phim này, lúc đầu còn không biết nó là chuyển thể cơ, sau này tìm thấy bản tiếng Anh trên fahasa, định mua thì tra trên tiki lại thấy bản tiếng Việt.
Đối với các bạn thích đọc dòng truyện sci-fi hay fantasy thì đây là một cuốn sách không thể nào bỏ qua. Đứng thứ 23 trong danh sách "bự" nhất của goodreads, "Best Books Ever". Về nội dung thì mình sẽ không nhắc lại các bạn đã comment ở trước nữa.
Tuy nhiên, không hiểu sao khi đọc cuốn này mình lại nhớ tới truyện của John Green, nhịp nhanh, gay cấn nhưng lại rất mau quên.
4
767616
2016-04-12 22:54:52
--------------------------
380465
5707
Dưới đây là suy nghĩ mình đã viết lại sau khi đọc cuốn sách này:
" Đơn độc!
Cái cảm giác của tôi sao mà lại giống hoàn cảnh của anh ấy đến thế cơ chứ, bị cô lập, tự băn khoăn và lo lắng và mất phương hướng. Để rồi từ đó tôi tự hiểu ra mình cần phải làm gì cho cuộc sống này. Phải! Tôi đã và đang đi theo con đường mà thượng đế đã sắp đặt, tôi sẽ ngày càng trở nên mạnh mẽ hơn xưa. Bị cô lập là trạng thái cảm xúc khiến con người ta phát huy hết khả năng tiềm ẩn của bản thân. Tôi muốn khóc vì đồng cảm với Ender"
Khỏi cần nói cuốn sách này lay động như thế nào đến trái tim tôi. Nó giúp tôi hiểu thêm về đời mình hơn
Chất lượng giấy 5 sao
5
919665
2016-02-15 09:26:40
--------------------------
379683
5707
Đọc xong quyển này cảm thấy rất tiếc nuối vì 1 quyển sách mà mình cảm thấy rất hay đã kết thúc. Mặc dù cái kết ở chương 14 mình thấy hơi dễ đoán. Cách viết truyện của tác giả đơn giản rất dễ đọc nhưng cuốn hút, dù mình cũng thích lối viết truyện mà phải khiến chúng ta phải suy nghĩ. Trong truyện có nhiều tình tiết hơi bạo lực nên sách viết về trẻ em nhưng thích hợp cho người lớn hơn, truyện cũng có nhiều tình tiết bất ngờ.
Bìa sách đẹp ,văn dịch tốt đọc dễ hiểu dù còn một vài lỗi chính tả trong dịch thuật nên cho 4 sao , nếu không mắc lỗi chính tả thì cho 5 sao. 
4
838344
2016-02-12 09:34:35
--------------------------
361963
5707
Lần đầu tiên mình đọc một quyển sci-fi mà có cảm giác choáng ngợp như đọc "Trò chơi của Ender", bởi nó không khai thác vào những yếu tố khoa học, công nghệ như những quyển sách sci-fi khác mà đi vào những chiều cạnh rất cá nhân. Ở "Trò chơi của Ender" ta có thể tìm thấy một tâm hồn yêu thương  đi kèm với bạo tàn, nơi không có luật lệ nào là giới hạn. Truyện miêu tả tâm lý của Ender rất chi tiết và khéo léo, có lúc ẩn sâu trong những trò chơi của cậu bé, có lúc thể hiện rõ ràng ở những hành động bình thường. Cảm ơn Nhã Nam đã dịch một quyển hay như thế này. 
5
450546
2015-12-31 22:06:10
--------------------------
348885
5707
'Trò chơi của Ender' - một cái tên rất hay, cuốn hút chúng ta từ những lúc nhìn vào cái bìa của cuốn truyện, nhưng sau khi đã gập sách lại rồi, chúng ta ngẩng đầu lên và tự hỏi: Liệu đây có "chỉ" là trò chơi của Ender? Một ngôi trường chiến đấu, nhưng cuộc bắn súng không trọng lực, những người bạn đồng hành dường như chỉ là tấm bình phong của một sự thật lớn hơn nhiều. Đây dường như không còn là trò chơi của Ender nữa, cậu và lũ nhóc còn lại chỉ là những quân tốt thí cho một trò chơi khác thâm độc hơn, tàn bạo hơn, để rồi Ender sau những phút vinh quang ngắn ngủi lại trở thành kẻ tội đồ với tội lỗi không thể nào tha thứ. Một cuốn sách hay, đáng đọc, và đáng để suy ngẫm.
4
502467
2015-12-07 10:07:51
--------------------------
331915
5707
Mình coi phim chuyển thể rồi mới đọc truyện. Phim thì không hay bằng truyện cơ mà được điểm cộng là bạn Asa Butterfield đẹp trai lai láng bừng sáng thôn làng (thụ lòi). Ở truyện quá trình Ender ở trường chiến đấu rồi cho đến khi làm chỉ huy được miêu tả rất cụ thể, chi tiết và rất hay và hay đến nghẹt thở. Những đứa trẻ thông minh nhất của nhân loại được huấn luyện trong môi trường quân sự khắc nghiệt, thiếu tình thương, thiếu sự bảo vệ để có thể trở thành những chiến binh tinh nhuệ nhất. Và đặc biệt Ender - đứa trẻ có trí tuệ siêu việt nhất lại càng bị cô lập sống trong môi trường khắc nghiệt hơn cả. Truyện đáng đọc
5
447833
2015-11-05 12:12:33
--------------------------
296819
5707
Một quyển sách hay, nội dung giàu hàm ý. Câu chuyện nói về Ender, một cậu bé thiên tài. Gánh trên vai trọng trách của toàn nhân loại. Cậu luôn muốn được hưởng tình yêu thương và hạnh phúc mãi  mãi. Nhưng điều đó chẳng thể xảy ra khi hành tinh Mẹ đang đứng trước nguy cơ diệt vong bởi thứ mà con người cho là nguy hiểm. Hơn ai hết, cậu luôn rất cô đơn và nỗi đau là một phần của cuộc sống. Và ENDER-chỉ là một đứa trẻ thiên tài.
5
790028
2015-09-11 12:26:15
--------------------------
286087
5707
-Về nội dung : một tác phẩm xuất sắc, ngoài mong đợi. 
 Chương cuối cuốn sách làm người đọc phải suy nghĩ nhiều thứ. Kẻ thù mà nhân loại  sợ hãi và làm mọi điểu để tiêu diệt lại là những người muốn sống hòa hợp, muốn  làm bạn với con người, và đã rất cố gắng vì điều đó. Đó có lẽ đã có thể là những người bạn.
 bị ấn tượng bởi câu nói của Ender với chị mình " ta phải đi thôi, ở lại đây em thấy gần như là hạnh phúc ấy" .Đứa trẻ luôn muốn được trở về, Anh hùng của nhân loại, đô đốc của phi đội quốc tế, kẻ đi qua vô số hành tinh, tìm kiếm nơi thích hợp  làm nhà cho quả trứng cuối cùng, lại không thể trợ về nhà của chính mình.
5
305651
2015-09-01 13:55:12
--------------------------
283622
5707
Đối với những cuốn sách được dựng thành film thì nguyên tắc của mình là phải đọc truyện trước rồi mới xem film, nhưng Trò chơi của Ender lại là một ngoại lệ, vì xem film xong mình mới biết đến cuốn sách này. Tuy đã xem film trước nhưng khi đọc sách thì mình vẫn cảm thấy đây là một cuốn sách hay vì film chưa thể truyền tải được hết cảm xúc cũng như suy nghĩ của các nhân vật, trong khi chính những điều này cũng góp phần tạo nên sự hấp dẫn của tác phẩm. Hi vọng rằng các phần tiếp theo của bộ sách sớm được xuất bản.
4
307488
2015-08-30 09:01:02
--------------------------
255872
5707
Mình tìm đến quyển sách này sau một lần tình cờ xem bộ phim "Ender's Game" được chiếu trên tivi. Nếu trên phim, mình được mãn nhãn với những hình ảnh, công nghệ hiện đại của thế giới tương lai thì khi đọc sách mình lại cảm nhận được những ý nghĩa nhân văn hết sức sâu sắc và cảm động. "Trận chiến của Ender" kể về những cô cậu bé sinh ra đã đánh đổi tuổi thơ của mình để "được" đào tạo thành những chiến binh cứu lấy Trái Đất, đặc biệt cậu bé Ender với khả năng lãnh đạo thiên tài nhưng phải luôn vật lộn, đấu tranh tư tưởng để tìm thấy bản ngã của chính mình, nỗi cô đơn của một thiên tài luôn bị áp đặt gánh nặng cứu thế giới... 
Về phần hình thức quyển sách, mình rất ưng với bìa sách này, có lẽ vì cảm tình của mình đối với bộ phim,ngoài ra chất lượng giấy và chữ in cũng rõ ràng nữa... Mong là sẽ sớm được đọc những phần tiếp theo ....
5
151116
2015-08-06 14:25:47
--------------------------
233618
5707
Câu chuyện khoa học viễn tưởng của nhà văn Orson Scott Card trở thành một cuốn tiểu thuyết bestseller chẳng phải chuyện ngẫu nhiên. Nó đáng được người đọc trên toàn thế giới dành cho một sự quan tâm đặc biệt. Câu chuyện thiết kế ra một hình hài mới cho Trái đất, cho con người (có lẽ là trong tương lai) . Hình hài ở đây không nói về mặt ngoại hình mà về trí tuệ, tinh thần và cả thế giới quan. Cậu bé Ender Wiggin - một thiên tài chỉ huy quân sự - chưa đầy 10 tuổi đã phải xa rời quê hương, bố mẹ, xa rời cả tuổi thơ để hoàn thành sứ mệnh của một đứa "con thứ ba" là cứu lấy Trái Đất đang đứng trước nguy cơ diệt vong bởi lũ bọ ngoài hành tinh. Tác giả cứ đánh lừa chúng ta là coi Ender như một người trưởng thành - thông minh, giàu lòng trắc ẩn lại tràn đầy dã tính rồi chúng ta chợt nhớ ra nó mới sáu tuổi. Bẵng đi vài trang sách chúng ta lại bị lừa để sau đó lại phải nhớ ra để nhìn nhận sự việc một cách khoan dung hơn. Người ta ép Ender vào những cạm bẫy, những khuôn khổ - bị cô lập để tôi luyện thằng bé thành công cụ để chỉ huy, để chiến đấu và để chiến thắng. Hình như chỉ có độc giả nghe thấy tiếng kêu của Ender "Xin hãy đưa cháu về nhà". Câu chuyện như có chất keo dính Ender lấy độc giả hòa làm một, cảm giác như chúng ta đang ở cùng nó, an ủi nó, lắng nghe nó. Cuốn sách dạy người ta tự lập, dạy người ta cách chiến thắng, dạy cả cách yêu thương và đối mặt. Hơn nữa những dòng suy nghĩ của Ender cũng như hai anh chị còn lại trong nhà Wiggin còn mở ra một hướng đi mới mẻ chưa từng có - hướng đi của trí tuệ minh mẫn. Không, không hẳn là hướng đi mà còn là sự đấu tranh vật lộn để tìm ra bản chất thật của mình... Quả nhiên cái mà câu chuyện cho chúng ta không chỉ là cảm giác chiến thắng, không chỉ là cảm giác cô đơn mà còn là niềm đau đớn của trẻ thơ.
5
84382
2015-07-19 20:02:21
--------------------------
219475
5707
Cứ tưởng truyện phiêu lưu viễn tưởng  thì phải khô khan và khó đọc lắm chứ, không ngờ là lại cuốn hút như thế này. Mình thích mạch truyện, không nhanh quá cũng không chậm quá, tất cả các chi tiết đều có dụng ý của tác giả. Càng đọc càng khâm phục trí thông minh tuyệt vời của cậu bé Ender. Bên cạnh đó, cũng thấy có phần cảm thông cho số phận của cậu, nhưng Ender có vẻ như là một người mạnh mẽ, giàu tình cảm. Khía cạnh đó khiến cho câu chuyện có phần mềm mại và cảm xúc hơn.
4
469377
2015-07-01 15:28:10
--------------------------
214250
5707
Thực sự là một cuốn sách đầy cảm hứng!!!
Câu chuyện lôi cuốn từ khi bắt đầu đến tận những trang cuối cùng khép lại. Không có lấy một chi tiết thừa. Cảm giác khi đọc là khâm phục tài trí tuyệt vời của Ender, ngưỡng mộ cách miêu tả những cảnh chiến đấu hoành tráng trong phòng chiến đấu và cả sững sờ truớc cái kết đau lòng dương như không thể thay đổi nhưng vẫn ấp ủ hy vọng về một tương lai sau trận chiến. Nghe nói vẫn còn phần hai mà mãi vẫn chưa thấy, thiệt mong đợi sẽ được xuất bản sớm!!!
5
419116
2015-06-24 19:05:12
--------------------------
213734
5707
Tôi không phải người yêu thích thể loại viễn tưởng, một lần tình cờ xem bộ phim này trên tivi và khi thấy nó có sách tôi đã quyết định mua.
Phải nói rằng sách hay hơn phim rất nhiều, vì phim đã cắt quá nhiều tình huống quan trọng, lời thoại ý nghĩa cũng bị bớt đi không ít. Càng đọc càng bị lôi cuốn vào câu chuyện, vào tâm trạng và cảm xúc của Ender - một cậu bé thiên tài đầy tình cảm.
Về bìa sách có lẽ nó phù hợp với nội dung nhưng tôi lại không thích, trừ điều đó ra thì mọi thứ còn lại rất ổn.
4
254977
2015-06-24 03:53:40
--------------------------
212881
5707
Mình đã mua và rất rất thích cuốn sách này. Tình tiết hấp dẫn, nhưng cũng khá nhẹ nhàng với người đọc. Mình rất thích cách sử dụng từ ngữ, rất hợp với độ tuổi thiếu niên. Ngoài ra những câu văn của tác giả hay những câu được trích dẫn rất hợp lí và đáng được ghi nhớ Câu chuyện khiến người đọc phải suy ngẫm lại về trẻ em: cách suy nghĩ, bản lĩnh, hành động và thái độ của chúng. Không phải là một câu chuyện thường ngày mà là chuyện viễn tưởng, giúp cho người đọc thỏa thích sáng tạo và tưởng tượng theo cảm nhận riêng.
5
299660
2015-06-22 23:27:34
--------------------------
212875
5707
Trước khi đọc sách, tôi đã xem phim, và nói thật là tôi hối hận vô cùng khi xem phim trước truyện. Bởi bộ phim nhàm chán vô cùng, không tạo được một chút ấn tượng gì, mọi thứ cứ trôi đi tuốt tuồn tuột. Còn truyện lại khác. Tất cả cùng hiển hiện một cách chi tiết và đầy khắc họa. Đặc biệt là với một giọng văn đầy lôi cuốn và hấp dẫn, dẫn dắt đưa người đọc đến nhiều bất ngờ không tưởng. 
Tuy nhiên, có một vài chi tiết hơi kỳ ảo và phóng đại, khiến đôi lúc tôi phải cười xòa vì sự phi logic của nó, nhưng không sao, cũng chẳng ảnh hưởng gì đến sự hấp dẫn của truyện haha.
4
30924
2015-06-22 23:18:40
--------------------------
201134
5707
Tôi đã đọc truyện sau khi xem phim và nhận thấy rằng truyện thực sự hay hơn phim nhiều. Truyện khắc họa từng khía cạnh của nhân vật Ender, đi sâu vào cảm xúc và suy nghĩ của Ender về hoàn cảnh hiện tại hơn. Ender là một cậu bé " khác người" . Ngay cả từ hoàn cảnh bản thân, gia đình: được sinh ra với mục tiêu được đưa ra từ những nhà quản lý . Cũng đặc biệt về lối suy nghĩ chín chắn không khác gì người lớn. Được đào tạo để trở thành anh hùng cứu con người nhưng lại phải đánh đổi tuổi thơ của mình để rồi nhận ra mình chỉ được sử dụng như là một công cụ để chiến đấu cho một cuộc chiến đã trở nên phi nghĩa
4
615549
2015-05-26 18:14:00
--------------------------
184043
5707
Tôi tìm đọc cuốn tiểu thuyết này sau khi xem xong "Ender's Game". Đọc truyện hay hơn xem phim rất nhiều. Truyện được viết đầy cảm xúc cá nhân Ender và cả của những người xung quanh cậu bé thiên tài này. Tiểu thuyết là một câu chuyện khoa học viễn tưởng về công nghệ và sự đe dọa tồn tại của Trái Đất, là cuộc chiến giữa loài người và lũ bọ ngoài hành tinh, nhưng cũng là thông điệp đầy tính nhân văn về nỗi cô đơn của những kẻ hơn người, về nỗi hoang mang và sự vật lộn tìm hiểu bản chất của chính mình. Ender đã được ấn định sinh ra để trở thành một cậu bé thiên tài, và từ bé đến lớn, em luôn được đào tạo theo cái cách biến một đứa trẻ thành công cụ hành động. Em có một người anh khắc nghiệt, nhưng cũng có một người chị Valentine luôn yêu thương và ở bên em. Nhưng chẳng bao giờ là đủ, em luôn phải sống với áp lực là một cậu bé thiên tài. Suốt 12 năm Ender được đào tạo trong trường chiến đấu, cuộc sống của em thật cô đơn và nhàm chán, chỉ có tập luyện, chỉ huy, học cách chiến thắng và chiến thắng. Khát khao của một đứa trẻ bị buộc phải trở thành người hùng cứu thế giới thật bé nhỏ nhưng có bao giờ em có được điều đó như những đứa trẻ bình thường khác?
5
314122
2015-04-17 08:00:49
--------------------------
177232
5707
"Trò chơi của Ender" mang một sức khó tả , một khi đọc không thể dứt ra được , tác phẩm là 1 chuyến phiêu lưu đầy kịch tính và bất ngờ cho tới khúc cuối . Ender một cậu bé buộc phải tham gia mtộ trường đào tạo nhầm sản sinh ra những người chỉ huy tài năng đủ khá năng đánh bại người bọ . Giữa tài năng chiến thuật thiên bẩm cùng với một tâm hồn lương thiện nhưng bị dày vò trong tham vọng của người khác , cuối cùng Ender đã làm được điều không tưởng , đánh bại hành tinh bọ nhưng cũng từ đó cậu khám phá ra đuọc những bí ẩn sau đó . Rất mong chờ phần kế tiếp của serie này
4
219456
2015-04-02 20:09:18
--------------------------
173192
5707
"Trò chơi của Ender" mang đến cảm tưởng như đang được chơi một trò chơi điện tử kiểu hành động nhập vai. Bạn sẽ bị hòa làm một với các nhân vật, bị cuốn vào một cuộc chiến kịch tính và không kém phần nguy hiểm. Ý tưởng của câu chuyện rất hay, thay vì có nhân vật chính là người lớn, tác giả tạo nên những chiến binh trẻ em, mà nhân vật trung tâm là Ender. Chỉ là một đứa trẻ, với ngoại hình nhỏ bé và tâm hồn đầy xáo động, nhưng đã bị đặt lên vai một trọng trách quá lớn là giải cứu trái đất. Cách miêu tả quá chân thật của tác giả khiến mình cùng sợ, cùng phiêu lưu, cùng hành động với Ender, và cảm thấu được những gì mà cậu đã phải trải qua. Dù là một cậu bé thiên tài, nhưng liệu Ender có thực sự muốn giữ một vai trò cao cả như vậy trong cuộc chiến bảo về loài người. Bằng những chi tiết bất ngờ đầy cảm xúc, tác giả đã lột tả được nội tâm phức tạp của một anh hùng đặc biệt.
Tuy nhiên, không thể nói mình đặc biệt thích cuốn sách. Những chi tiết kỳ ảo có lẽ thích hợp hơn với độc giả mới lớn.
3
109067
2015-03-25 19:56:02
--------------------------
159992
5707
Với tư cách là một fan cứng của dòng tiểu thuyết fantasy, tôi đã mua cuốn sách này sau khi nó được giới thiệu rùm beng khắp nơi và dành cả một ngày cắm cúi đọc nó. Về cơ bản, nó không phải là một tác phẩm khó đọc nhưng vẫn giàu ý nghĩa và cuốn hút.

Đây là cuốn sách về những đứa trẻ thông minh. Động lực, sự hiểu biết và một số nét vẫn chỉ ra chúng là trẻ con. Nhưng trí thông minh và sức mạnh tiềm ẩn hoàn toàn bộc lộ ra. Đây là cuốn sách về chiến tranh. Về sự lãnh đạo. Về những phẩm chất khiến con người ta trở thành một con người hùng mạnh và đáng ngưỡng mộ. Bất cứ ai có một tuổi thơ êm đềm có lẽ sẽ khó mà hiểu được nó.

Tuy vậy cuốn sách vẫn thiếu một số điểm. Nhân vật thiếu đặc tính đặc trưng, và tôi khó mà cảm được nhân vật chính. Dù sao thì, tôi vẫn recommend cuốn sách này cho những ai còn trẻ (nó hợp với người trẻ khoảng 14-16 hơn). Những người trả sẽ thấy nó dễ đọc và hay hơn so với người lớn. 

Bên cạnh phần nội dung, tôi xin có lời khen cho phần bìa sách. Nó rất hợp nội dung và đẹp hơn nhiều so với bìa gốc. :)
4
89033
2015-02-21 08:48:46
--------------------------
148619
5707
Tôi vừa đọc xong cuốn sách này, đọc ngấu nghiến... Khi gấp cuốn sách lại, tôi không thể không suy nghĩ về những sự thật được mở ra ở phần "Phát ngôn cho người chết", phần cuối cùng này không phải là phần kết mà nó trở một câu chuyện mới, một sự khởi đầu mới. Cái kết mở đầy mới mẻ và nhân văn.
Thế nào là đúng, thế nào là sai? Tại sao cùng là giết sinh linh nhưng một bên bị coi là sát nhân với đầy đủ nghĩa tởm lợm và kinh khủng của nó, một bên được coi là anh hùng với tất cả vinh quang và danh dự? Đâu là chân lý và đâu là tận cùng của sự thật?
Một tác phẩm cuốn hút với những tảng băng nổi, lay động lòng người và đầy ý nghĩa nhân văn ở tảng băng chìm. Cuốn sách này là một tác phẩm kinh điển, một tuyệt tác của sự tư duy và tưởng tượng.
5
473952
2015-01-11 00:08:15
--------------------------
144234
5707
Cuốn sách này có lẽ là một trong những cuốn sách có tầm ảnh hưởng nhất mình từng đọc. Không chỉ vì những yếu tố kì ảo của sách mà là vì những ý nghĩ của những đứa trẻ trong sách phản ánh ý nghĩ của những đứa trẻ ở ngoài đời. Khi chúng phải chịu sự ép buộc của người lớn và khi tụi nó cố gắng giải quyết được những vấn đề do người lớn đặt ra. Nếu có thể, Tiki up luôn cả phần 2 (phát ngôn cho người chết), phần 3 ( Xenocide) và phần 4 ( Children of the mind) được không? Xin cảm ơn
5
90254
2014-12-25 23:00:38
--------------------------
141380
5707
Nếu những ai thích Harry Potter, The Hunger Games, The Maze Runner,Divigent... hay những sách thuộc thể loại này thì Ender's Game sẽ không làm bạn chán hay thất vọng. Mình chưa có cơ hội được đọc sách này mà xem phim trước. Đối với người chưa đọc sách thì phim hẳn cũng đã rất tuyệt rồi, nên nếu đọc thêm sách nữa thì chắc chắn sẽ cảm thấy hay hơn nữa! Rất thích và nể phục cách suy nghĩ của Ender, thông thường với thể loại này, nhân vật chính luôn có suy nghĩ, cảm xúc khác với người bình thường và luôn khiến ta phải bất ngờ. Tuy biết điều đó nhưng tôi vẫn ngạc nhiên bởi lời nói, hành động của Ender. Và nó khiến tôi phải nghĩ về cuộc sống hậu tận thế, liệu cuộc sống đó ở trong Ender's Game, The Hunger Games,... hay sao? Đến lúc đó liệu chúng ta có bị đem ra làm thí nghiệm, chơi những trò chơi sinh tử và quay cuồng không biết đâu là lối thoát cho một thế giới mới, một trái đất mới hay không?...
5
58935
2014-12-15 12:56:39
--------------------------
138827
5707
Tôi biết đến "trò chơi của Ender" qua bộ phim điện ảnh cùng tên, rất hay, thú vị. Và khi biết phim được dựng lên từ tiểu thuyết cùng tên của Orson Scott Card thì tôi đã tìm mua ngay lập tức. Thật sự giữa phim ảnh và truyện khác nhau đến nỗi tôi tưởng nhưng mình vừa xem một bộ phim khác chứ không phải dựa vào truyện mà dựng thành. Những tình tiết trong phim được cắt giảm đến tối đa đến nỗi tâm lý của Ender đã không được thể hiện trọn vẹn trong phim, một thần đồng đơn độc, một anh hùng bất đắc dĩ khi chỉ mới lên 11, độ tuổi mà trẻ con phải được vui chơi, ăn học, thương yêu. Nhưng Ender lại phải học tập trong môi trường khắc nghiệt đến tàn khốc, tuổi thơ của em và những suy nghĩ không phải của một cậu nhóc lên 11, những suy nghĩ rất người lớn về gia đình, bạn bè, về kẻ thù, về chiến tranh và cái chết. Hãy đọc và cảm nhận, và để hiểu được, hãy khôn lớn !
5
187187
2014-12-04 20:30:05
--------------------------
138404
5707
Dù đã được dựng (chuyển thể) thành một tác phẩm điện ảnh nổi tiếng của Hollywood nhưng vẫn khó có thể truyền đạt toàn bộ nội dung của 1 tác phẩm vào các cảnh phim được.
Một quyển sách ở tương lai viễn tưởng không quá nhiều yếu tố hành động hay những tình huồng nghẹt thở nhưng hấp dẫn ở tính nhân văn và bối cảnh của cốt truyện. Phần nội dung tất nhiên không thể tiết lộ trước được nhưng ở cuối phim (hay sách) sẽ có tình huồng giải quyết và đối diện vấn đề của bản thân nhân vật chính sẽ khiến bạn phải suy nghĩ và có ấn tượng cực kỳ sâu sắc.
5
451219
2014-12-02 19:04:59
--------------------------
134580
5707
Mình là một người thích đọc thể loại sci-fi, và quả thật khi mua cuốn này về, nó đã không làm mình thất vọng. Lúc trước có chiếu phim, mình đã bị cuốn hút bởi cái ý tưởng huấn luyện trẻ em thành giống nòi nhuệ binh của tương lai, cái ý tưởng để những đứa trẻ còn chưa thành niên làm chỉ huy của cả một đội quân.

Càng đọc, Ender's Game càng thu hút mình một cách mạnh mẽ. Mình đọc và  có cảm giác như chính mình là Ender vậy. Chịu sự áp đặt, chịu sự bất công, nếm mùi vị của chiến thắng, lại bị dồn ép và cô lập rồi cảm giác muốn từ bỏ tất cả... Cái chân thật của cảm xúc qua từng câu chữ mình cảm nhận được vô cùng rõ. Ngoài ra, tình chị em của Ender và Valentine là một điểm nhấn của cuốn sách. Mình vui vì ở Ender's Game không hề có kiểu tình yêu nửa vời như ở mấy cuốn tiểu thuyết hiện nay, mà thay vào đó là một mối quan hệ gia đình bền chặt.

Nếu ai đã từng xem phim và cảm thấy hứng thú, mình khuyên là nên đọc cuốn sách này. Phim cắt quá nhiều tình huống quan trọng, lời thoại ý nghĩa cũng bị bớt đi không ít. Để hiểu rõ hơn về Ender, cuốn sách này là tất cả những gì bạn cần. 

Mình chờ tập tiếp theo!
5
62627
2014-11-10 09:37:46
--------------------------
