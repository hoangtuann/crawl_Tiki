397615
5686
Một quyển sách khá hay và hữu ích cho các bà mẹ trong việc nuôi dạy và chăm sóc con. Các câu hỏi thực tế và đi kèm tư vấn của các bác sỹ có thể áp dụng với hoàn cảnh từng người. Các bà mẹ cũng có thể tham khảo thực đơn dinh dưỡng tùy theo giai đoạn, thời kỳ. Còn có bảng chiều cao, cân nặng chuẩn để đối chiếu. Tuy nhiên, biếng ăn của con thật sự là vấn đề khó khăn, nan giải, không dễ tìm ra nguyên nhân vì vậy mình mua sách này để tham khảo và học hỏi một số món ăn cải thiện cho con. Tiki giao hàng lần này nhanh hơn, vẫn bọc rất cẩn thận nhưng có vẻ như sách của mình bị cũ kỹ quá, màu đã hơi ố vàng. Dù sao cũng tạm chấp nhận được. Cảm ơn tiki.  
2
957569
2016-03-15 10:58:51
--------------------------
358012
5686
Nói chung quyển sách này cũng tạm, vì là tập hợp các dạng câu hỏi của các bà mẹ có con biếng ăn, nhẹ cân và lời đáp của chuyên gia. Nhưng sách có thêm phần thực đơn 1 số món ăn dặm khá đơn giản để các mẹ có thể tập nấu cho bé ăn cũng khá hay. Nói chung tiền nào của ấy, quyển sách mỏng nội dung cũng ít với giá tiền như vậy mình thấy hợp lý. Dự là đọc xong sẽ làm quà tặng cho các bà mẹ trẻ bạn của mình. Quyển này thì có vẻ hơi cũ, giấy in cũng cũ nên nhìn không bắt mắt lắm, nhưng lúc mình mua được giảm giá nên thấy cũng tạm.
3
927612
2015-12-24 16:27:30
--------------------------
223241
5686
Mình mua sách ngày 21/2 lúc mình còn chưa được làm mẹ, nhưng cuốn sách rất thiết thực, giá cũng vừa phải giữa hàng ngàn cuốn cẩm nang dành cho bà mẹ khác. Cuốn sách là tập hợp các câu hỏi mà các bà mẹ nuôi con đều thắc mắc, được các chuyên gia dinh dưỡng giải thích rất cẩn thận, tỉ mỉ, rõ ràng. Bên cạnh đó, cuốn sách còn tập hợp các món ăn dặm cùng với nguyên nhân tại sao trẻ lại biếng ăn, phương pháp giúp tre vượt qua, Con mình còn chưa ăn dặm mà mấy chị hàng xóm đã mượn dùng trước và tấm tắc khen sách hay rồi. 
5
465810
2015-07-06 20:26:02
--------------------------
