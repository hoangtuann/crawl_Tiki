249757
7677
chắc hẳn ai cũng đã biết đến trạng Tí, một nhân vật chính trong truyện thần đồng đất việt rồi nhỉ ?Bởi nó đã đi vào tuổi thơ của rất nhiều người khi họ còn nhỏ, trong đó có mình. Và đến nay, truyện vẫn còn đang sáng tác và xuất bản.Trong tập này, Bắc quốc lại yêu cầu cống nộp tượng vàng Liễu Thăng, nhưng khổ nổi, ngân khố không đủ để đúc tượng vàng. Thế là trạng Tí đã nghĩ ra cách để khỏi phải cống nộp tượng vàng một lần nào nữa. Cốt truyện rất hay, hoàn hảo, mạch lạc, có cả những yếu tố bất ngờ, khiến cho người đọc có cảm giác thú vị, hấp dẫn. Ngoài ra, truyện còn được thiết kế rất đẹp, bắt mắt nữa. Nói chung, mình rất thích và hài lòng về quyển truyện này !
5
120141
2015-07-31 22:59:02
--------------------------
