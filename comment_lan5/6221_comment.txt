384494
6221
Mình khá thích bộ sách "Gia đình thân yêu" này. Bộ sách bao gồm các mẩu chuyện nhỏ hằng ngày liên quan đến các nhân vật trong gia đình như Mẹ, Bố, Ông, Bà, ... Chất liệu sách được làm bằng xốp, mềm, gồm 5 trang. Hình dáng của sách được thiết kế theo hình dáng nhân vật, rất độc đáo. Mình chỉ mới mua quyển Mẹ cho con và thấy rất hài lòng. Sắp tới mình sẽ mua thêm các nhân vật Bố, Ông, Bà và Tôi. Cũng cần phải cảnh báo là bé vẫn có thể xé rời các trang sách nhé!
5
438329
2016-02-22 14:40:18
--------------------------
