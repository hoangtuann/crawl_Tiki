379089
5908
Tác phẩm chưa có chạm đến trái tim của người đọc thực chất , chỉ man mác gợi lại nỗi buồn cảm nhận mà ai cũng tán thành , sài gòn một sợi tơ lòng có phải là suy nghĩ của tác giả thường ngày vẫn ngắm nhìn đường phố với quang cảnh khác biệt với ngày trước khi đi vào sự hiện đại phát triển như ngày nay thì đối với tính nóng nực , oi bức thì người với thời cuộc vẫn cứ tiếp nối theo guồng quay nhịp sống được trao nhau tiếng cười nói , huyên náo .
3
402468
2016-02-09 11:43:12
--------------------------
245451
5908
Một tập tản văn bao gồm nhiều mẩu chuyện tap bút về Sài Gòn của tác giả. Quyển sách cho ta cái nhìn về Sài Gòn cũ, khá xưa rồi nên đọc khó mà tưởng tượng được. Nhưng bản thân tôi vẫn thấy thú vị, vì những các xưa ấy quá khác hiện giờ, đôi khi là khó chấp nhận. Có lẽ, quyển sách này sẽ thích hợp cho những cô chú đã từng sống qua thời Sài Gòn xưa cũ ấy hồi tưởng lại những ký ức đẹp đẽ của một thành phố nay đã phát triển nhanh chóng như bây giờ. Đây sẽ là sợi tơ lòng cho đúng những người đã từng trải qua những câu chuyện của Sài Gòn năm ấy.
3
21422
2015-07-28 22:19:57
--------------------------
228517
5908
Tựa sách thật lãng mạn, gợi chút bâng khuâng vương vấn, gợi thiện cảm. Nhưng nội dung của sách chưa thật tương xứng với tiêu đề. Không phủ nhận tác giả có những góc nhìn, phát hiện khá tinh từ sự quan sát tỉ mỉ những gì đặc trưng của SG từ chuyện thời tiết, xe cộ, ngõ hẻm, quán xá... Thế nhưng cách viết của tác giả khiến độc giả như tôi khó tiếp nhận. Bởi vì những câu văn dài hơi, cách sắp xếp từ ngữ trong một câu tùy tiện không theo trật tự ngữ pháp thông thường. Có những đoạn, khi đọc phải cố gắng chắp nối, liên tưởng mới hiểu đoạn văn nói gì. Có những đoạn rời rạc,chêm vào nhiều từ ngữ mà tôi không tường nghĩa. Ít thấy những câu văn giản dị, trong trẻo. Mua và đọc cuốn sách này, tôi thấy nản quá. Tôi buồn vì không cảm nhận được sự trong sáng, uyển chuyển, đẹp đẽ của tiếng Việt trong một cuốn sách có cái tựa đẹp như vậy.
2
295567
2015-07-15 15:45:11
--------------------------
144329
5908
Tôi mua cuốn sách này với tâm trạng háo hức, mong muốn được đọc những tản văn thú vị về Sài Gòn, nơi tôi sinh ra và lớn lên.
Thật đáng tiếc, cuốn sách đã làm tôi khá thất vọng! Nội dung cuốn sách là một tổng hợp nhiều tạp bút về những vấn đề rất gần gũi của Sài Gòn, có những câu chuyện cũng khá thú vị. Nhưng cách viết văn của tác giả là một sự kinh hoàng, làm cản trở mạch cảm xúc về SG. Câu văn lủng củng, viết nhiều câu rất dài, chuyện nọ xọ chuyện kia, không liên kết về logic, dòng suy nghĩ bị gián đoạn nhiều lần. Tác giả sử dụng văn phong bình dân nhưng lại quá bình dân đến mức lộn xộn, khiến tôi cảm giác như tác giả chẳng phân biệt được giữa văn nói và văn viết. Đôi lúc, tôi đọc xong cả một đoạn mà vẫn chẳng thể hiểu được tác giả muốn nói gì với cách viết tuỳ tiện và lộn xộn đó. Tôi đã đọc nhiều sách viết về SG, về miền Tây, cũng chọn cách viết giản dị, bình dân nhưng đọc vào thấy rõ ràng rành mạch và vẫn có sự chỉn chu trong đó.
Nếu bạn đủ kiên nhẫn để đọc một dạng câu văn 5, 6 mệnh đề, dài 7, 8 dòng, viết chuyện này nhảy sang chuyện kia thì có thể chọn lựa cuốn này.
3
6742
2014-12-26 12:31:49
--------------------------
