385805
7763
Đây là một trong rất nhiều cuốn sách thuộc tủ sách thiếu nhi - văn học cổ điển mà mình đã mua tại tiki. Chất lượng sách thì không cần bàn, in ấn nhỏ gọn, thiết kế đẹp mắt. Cốt truyện thu hút, đáng yêu, phù hợp với các em nhỏ. Bên cạnh đó còn có hình minh họa pr một số trang. Câu chuyện kể về cô bé Heidi có tấm lòng nhân ái, đáng yêu, ở đâu cũng được mọi người yêu quý. Câu chuyện giúp mình dạy dỗ em mình một cách nhẹ nhàng, thấm thía qua các bài học trong đó.
5
648254
2016-02-24 16:54:08
--------------------------
285769
7763
Mình mua quyển sách này lâu rồi, hình như cũng ở hội sách đường Lê Thánh Tôn. Biết đó là quyển sách dành cho thiếu nhi nên mình mua đọc để giải trí nhẹ nhàng. Và thật sự thì mình rất, rất, rất thích quyển sách hơn những gì mình tưởng. Không nói nhiều về tình tiết hay câu chữ văn chương gì cả, chỉ đơn giản một điều là mỗi lần mình cảm thấy mệt mỏi vì công việc hoặc buồn bực vì một điều gì đó, chỉ cần cầm quyển sách lên đọc vài trang, mình như có thể hòa mình vào sự trong lành của cuộc sống giản đơn gần gũi, cảm nhận được tình yêu và sự hồn nhiên của Heidi dành cho tất cả mọi người, lúc đó mình cảm thấy nhẹ nhàng và đầu óc được thoải mái đi rất nhiều. Đó là những gì quyển sách đã mang lại cho mình, đây là một sự lựa chọn tốt cho tất cả mọi người, không chỉ riêng các em thiếu nhi.
5
591834
2015-09-01 09:28:04
--------------------------
276110
7763
Tôi thích Heidi từ khi còn bé vì tôi từng xem bộ phim này. Sau này khi một người bạn vô tình nhắc lại Heidi tôi đã tìm và mua cuốn sách này. Có điều giờ tôi đã lớn nên đọc cuốn sách đã không còn thấy hấp dẫn như hồi bé nữa. Nhưng vẫn là một lần để quay lại tuổi thơ. Cuốn sách mang một màu sắc tươi vui, giọng văn của tác giả vô cùng trong sáng và dễ đọc nên chắc chắn sẽ rất thu hút các em nhỏ. Nội dung cuốn sách thì rất có ý nghĩa. Đọc cuốn này khi nào tôi cũng tưởng tượng ra mình đang đứng trên dãy Alp như Heidi, nhìn thiên nhiên vô cùng, cảm giác tự do. Rất tuyệt. Chỉ có điều tất cả mọi thứ đều mang nét quá ngây thơ mà khi là một người lớn sẽ hiểu không thể có những sự ngây thơ như vậy. Nó đã không còn hấp dẫn tôi là vì vậy. 
4
380813
2015-08-23 17:19:37
--------------------------
275181
7763
Heidi là cuốn văn học thiếu nhi Thụy Sĩ đầu tiên tôi đọc, và tôi đã yêu thích ngay từ những chương đầu tiên. Văn phong miêu tả của tác giả đẹp lắm, gợi mở đầy sức hút và tươi sáng, ngọt ngào không thể cưỡng lại được. Cô bé Heidi thật hồn nhiên, đáng yêu, và cũng giàu tình cảm nữa. Dù được sống trong nhung lụa, xa hoa song trái tim Heidi luôn hướng về vùng núi quê nhà thân thương, nơi hoang sơ hẻo lánh nhưng chứa đựng bao vẻ đẹp kỳ vĩ và tình yêu thương. Câu chuyện đề cao tính tự chủ, sự kiên cường và lòng nhân hậu. Đọc rồi bạn sẽ cảm giác trái tim như nở hoa.
5
6502
2015-08-22 18:11:05
--------------------------
237654
7763
Thoạt đầu mình không trông mong nhiều vào tác phẩm này vì nghĩ đó là tác phẩm văn học dành cho thiếu nhi và sẽ không phù hợp với một độc già đã không còn là thiếu nhi như mình. 
Nhưng, khi đã thực sự đọc tác phẩm, thì từng con chữ, từng lời văn, từng cảnh trí trong truyện thực sự làm mình thích thú. truyện Kể về cô bé Heidi nhân hậu, hồn nhiên, dù ở hoàn cảnh nào, với bất kỳ con người nào dù cho có khó khăn hay nhân hậu, thì phẩm chất của cô bé vẫn không hề bị khuất lấp; ngược lại, chính sự hồn nhiên của Heidi đã làm lay động không biết bao nhiêu con người và khiến họ yêu mến cô bé. Sự hồn nhiên của Heidi được tác giả dùng với một liều lượng vừa phải, không quá đà, khiến nhân vật càng trở nên chân thật hơn bao giờ hết. Thiên nhiên hoang đã nhưng căng tràn nhựa sống của đỉnh Alps cũng được miêu tả chân thật, tỉ mĩ bằng ngôn ngữ giàu chất tượng hình.
Heidi quả thật là một tác phẩm văn học thiếu nhi đáng đọc với độc giả dù ở bất kỳ độ tuổi nào.

5
38610
2015-07-22 16:45:59
--------------------------
233832
7763
Heidi là câu chuyện về sức mạnh của tình yêu thương. Với tấm lòng trong sáng và tình yêu thương của mình, cố bé Heidi đã sưởi ấm trái tim băng giá của ông nội, đem đến ánh sáng hy vọng cho bà cụ nhà Peter, đem niềm vui đến gia đình của Clara và ông bác sĩ… Những con người Heidi đã gặp và trao gửi yêu thương đều cảm thấy ấm áp và hạnh phúc hơn. Câu chuyện về cô bé Heidi thực sự rất cảm động với nhiều tình tiết gây xúc động cho độc giả. Heidi chính là một món quà kỳ diệu, em thật trong sáng và đáng yêu. Mình tin rằng, ai đọc tác phẩm này cũng đều phải lòng cô bé Heidi và mong ước được sống cuộc sống của Heidi với thiên nhiên bao la xinh đẹp, không khí trong lành mát mẻ vùng cao nguyên. Nhà văn Johanna Spyri đã thành công khi khắc họa vùng quê Thụy Sĩ giàu chất thơ với câu chuyện tuyệt đẹp về lòng người. Điều mình không thích duy nhất ở cuốn sách này là cái bìa sách, hình bìa đơn giản và không được chăm chút bắt mắt lắm.
4
135597
2015-07-19 22:23:32
--------------------------
188649
7763
Đây là cuốn truyện tôi đọc lúc mới học lớp 5,6 gì đấy nhưng chưa bao giờ tôi quên nội dung cũng như cái cảm giác lúc đọc nó. Cũng bởi cái văn phong trong sáng, ngôn ngữ giản dị và giá trị nội dung của nó. Một câu chuyện dễ thương nhưng rất ấm áp tình người, một câu chuyện rất chân thực và sinh động về bức tranh cũng như con người nơi vùng đồi núi Thụy Sĩ. Hãy cứ tin là khi đọc xong bạn sẽ có 1 khát khao là được đặt chân đến nơi đó, được hít thở bầu ko khí nơi đó, được làm quen vs Heidi, vs mọi người, kể cả ông già kì cục của Heidi ở đó. Họ tốt bụng và hơn hết là họ có sự chân thành- cái mà làm nên hơi thở của cả câu chuyện. Và đến đoạn cuối bạn sẽ cảm thấy chính sự chân thành và tốt bụng ấy cùng với sức mạnh kì diệu của thiên nhiên sẽ là nên một điều đặc biệt với một nhân vật cũng rất đặc biệt. Có thể nói đây là cuốn sách đã góp 1 phần nào đó trong tâm hồn của tôi, một cái gì đó rất trong trẻo và đẹp đẽ cho đến tận ngày hôm nay, và có lẽ là cả sau này nữa. Đừng ngại nếu mua một cuốn sách như vậy và để trên giá sách thân yêu của minh!!!!
5
411693
2015-04-25 20:11:18
--------------------------
137987
7763
heidi là một câu chuyện rất hay, tôi rất thích tác phẩm này nên dù đọc rồi,tôi vẫn tìm mua truyện, cuốn này là bản dịch của đông a, khá hay, là văn học cổ điển bỏ túi, rất tiện khi mang theo mình và đọc mọi lúc mọi nơi, tuy nhiên vì khổ nhỏ, nên dịch cũng chưa thật hay, chưa sát với tác phẩm đầu tôi đọc, nhất là một số đoạn miêu tả cử chỉ, dáng vẻ của nhân vật. Nói chung, cốt truyện hay, làm người ta muốn đọc đi đọc lại mãi, và vô cùng yêu quý cô bé heidi hồn nhiên, thông minh, nhanh nhẹn, trong sáng và đầy vị tha, và tôi thực sự ngưỡng mộ cách em nhìn thế giới, nhìn người lớn, dù có ở tầng lớp, đẳng cấp nào, nhờ em mà tôi thực sự thích cái cuộc sống trên núi cao ấy, rất thú vị.Bạn đọc nên mua quyển truyện này, nếu có nuu cầu có thể mua quyển bản to hơn, không phải văn học bỏ túi, đọc hẳn ràng sẽ hay hơn nhiều,nhưng nếu muốn đọc trong lúc đi đâu đó thì một quyển sách nhỏ có thể bỏ vào túi áo, túi quần như thế này cũng sẽ rất tiện đấy.
4
412050
2014-11-30 09:08:49
--------------------------
