493908
4878
Trước khi mua cuốn sách này tôi đã đọc qua 2 cuốn sách là Con đường Hồi Giáo và Tôi là một con lừa của tác giả Nguyễn Phương Mai, khi đọc 2 cuốn sách này tôi đã biết đến sự thông thái và thịnh vượng của nhà nước Israel và dân tộc Do Thái nhưng chưa được chuyên sâu, nhưng từ khi đọc tiếp cuốn sách này tôi đã biết được rõ hơn sự thăng trầm của lịch sử đất nước và anh hùng của những con người Do Thái và ngày càng khâm phục tính cách cùng sự ngoan cường của họ và là tấm gương noi theo của các dân tộc khác, rất cảm ơn tác giả đã nghiên cứu và viết ra cuốn sách này.
4
699206
2016-12-03 15:52:00
--------------------------
493027
4878
Tôi rất thích khám phá về người Do Thái. Khi đọc qua quyển sách này, nó đã cung cấp cho tôi đầy đủ kiến thức, thông tin, lịch sử, địa lí,... bổ ích. Nó là một cuốn sách rất tuyệt vời của chú Đặng Hoàng Xa. Tôi mong chú ấy có thể viết thêm nhiều quyển sách hay về thế giới này.
5
672274
2016-11-27 21:58:21
--------------------------
480133
4878
Trước đây mình rất mơ hồ về Israel. Sau này lại càng mơ hồ hơn khi thì nghe thấy trên tivi ra rả những tin tức về Israel ném bom nước này, đánh nước kia, nhưng lại thấy mọi người tung hô trí tuệ Do Thái.
Vậy thực sự đất nước này là thế nào. Càng tò mò hơn khi có dịp làm trong 1 doanh nghiệp Israel, mình đã không tiếc thời gian khi đọc quyển sách này. Đây là quyển sách vô cùng đầy đủ về mọi khía cạnh của 1 đất nước lạ lùng này. 
Mình chỉ thấy thiếu sót 1 điểm là tác giả chưa đề cập chi tiết đến từng vị lãnh đạo của Israel ngoài Ben Gurion. Vì theo mình biết thì không phải ai cũng tốt, có những vị như là Netanyahu theo vài người Israel cho mình biết thì cũng thuộc loại bựa nhân.
5
84027
2016-08-15 14:14:32
--------------------------
478915
4878
Cuốn sách viết khá rõ về nguồn gốc của nhà nước Do Thái. Mở rộng nhiều vấn đề như kinh tế, nguồn gốc, đạo giáo, chính trị ... Đặt biệt phần tôn giáo viết rất kỹ không những về tôn giáo của người Do Thái mà còn cho người đọc một số thông tin về Thiên Chúa giáo và Hồi giáo. Sách không đưa ra ý kiến cá nhân, không áp đặc suy nghĩ cho người đọc mà để người đọc tự suy ngẫm. Cho thấy được tính hình thế giới bên ngoài Việt Nam, về một nhà nước xây dựng trên cơ sở là một tôn giáo. Tuy nhiên sách sử dụng ngôn ngữ hàn lâm nên có một số chổ khá khó hiểu. tôi mua cuốn sách vì ngưỡng mộ nền kinh tế sáng tạo và hiện đại của họ nhưng phần kinh tế không thật sự hấp dẫn nên khuyên những người như tôi nên mua thêm cuốn quốc gia khởi nghiệp.
4
1229795
2016-08-05 21:43:49
--------------------------
478209
4878
Tôi được một người bạn giới thiệu cho cuốn sách này khi vô tình nhắc đến "Quốc gia khởi nghiệp". Với tôi, dân tộc Do Thái luôn là một dân tộc kỳ lạ. Từ cách họ chiến đấu kiên cường để giành lấy đất tổ, giữ gìn nền văn minh của một dân tộc "được chúa chọn", đến cách họ dám nghĩ, dám làm và dám phá vỡ mọi nguyên tắc mà những quốc gia khác xem là tôn ti, trật tự. Chỉ có thể gói gọn trong hai từ khi nói về Do Thái và Israel - "Phi thường". Cuốn sách được viết vô cùng chi tiết, và những phần về sau được tác giả Đặng Hoàng Xa phân tích rất logic, từ cách đặt vấn đề, đến sự lý giải cho những thành công hiện tại và những trở ngại trong tương lai. Cuốn sách không chỉ là kho thông tin bổ ích cho những ai muốn tìm hiểu về đất nước, con người Israel hay dân tộc Do Thái nói chung, mà còn như một cuốn cẩm nang cho những bạn trẻ muốn khởi nghiệp, muốn thử và đặt ra những thử thách cho chính bản thân, và lớn lao hơn là sự suy xét, nghĩ về cho nền kinh tế, tiềm lực quốc gia.
5
187067
2016-07-31 22:56:45
--------------------------
473384
4878
Tuy chỉ chiếm tỉ lệ phần trăm rất nhỏ trong tổng số dân số thế giới song người Do Thái khiến cả thế giới kinh ngạc bởi những kì tích mà mình tạo nên. Cuốn sách “Câu chuyện Do Thái – Lịch sử thăng trầm của một dân tộc” của nhà nghiên cứu Đặng Hoàng Xa sẽ giúp mỗi người trong chúng ta tự trả lời được những câu hỏi mà mình còn thắc mắc. Đó là lối sống lành mạnh, coi trọng giáo dục, tinh thần tập thể của họ. Hi vọng người Việt Nam mình sẽ rút ra nhiều kinh nghiệm để đưa đất nước tiến lên.
5
470567
2016-07-10 15:26:10
--------------------------
399788
4878
Chỉ 2 ngày là mình đã đọc xong cuốn sách này.Nó hay, không khô khan như thứ lí thuyết suông-theo mình là thế- như một số cuốn sách lý thuyết viết về các dân tộc. Cuốn sách này cho mình biết tất tần tật về lịch sử 4000 năm của dân tộc Do Thái , về những điều  mà từ trước tới nay họ đều nổi tiếng. Và tất cả đều được viết theo lối dễ đọc dễ hiểu. Nó bổ sung rất nhiều kiến thức về đời sống, xã hội của một đất nước nhỏ bé Israel giữa các nước Ả rập to lớn.Cuốn sách được vận chuyển không tốt lắm vì nó bị quăn mép nhưng không sao vì Tiki đã giúp mình mua được cuốn sách này với giá rất hợp lý nhất.
5
747943
2016-03-18 11:39:35
--------------------------
392895
4878
Bản thân tôi, luôn luôn ngưỡng mộ Nhà nước Do Thái hiện đại, vì cách nó được sinh ra, cách nó tồn tại và phát triển như ngày hôm nay. Chính vì vậy, tôi tìm đến cuốn sách này, như để hiểu thêm về đất nước này.
Với hiểu biết sâu sắc, phong phú về dân tộc Do Thái, những gì mà tác giả Đặng Hoàng Xa cung cấp đã giúp người đọc hiểu thêm phần nào về những thăng trầm, khổ đau cũng như những vinh quang mà dân tộc này đạt được. 
Đây là một cuốn sách thú vị, dù nhiều chỗ trong cuốn sách khá khó đọc do mang nặng tính khoa học, chính trị.
4
997316
2016-03-07 20:51:38
--------------------------
382815
4878
Câu chuyện li kì của cộng đồng do thái. Từ con số 0, người do thái đã xây dựng một đất nước phát triển qua bao năm chiến tranh và lưu lạc. Điều đó Việt Nam ta cần phải học hỏi từ họ rất nhiều.Do thái là một sắc tộc tôn giáo có nguồn gốc từ người Israel, còn gọi là người Hebrew, trong lịch sử vùng Cận Đông cổ đại, sắc tộc này tồn tại từ 1800 trước công nguyên, đó là một lịch sử lâu đời, nhưng trong 2000 họ bị mất nước và lưu lạc khắp nơi. Thật khó khăn khi con người không còn quê hương.
5
828378
2016-02-19 19:17:30
--------------------------
342349
4878
Thích thú với Israel sau khi đọc cuốn Quốc gia khởi nghiệp được tặng bởi Trung Nguyên nên mình đã quyết định mua cuốn sách này. Tuy nhiên, bởi vì khá thích các chủ đề về chiến tranh, đặc biệt là các cuộc chiến của Israel - một đất nước nhỏ bé, mới thành lập trong lòng một thế giới Ả rập rộng lớn xung quanh, và cả các cuốn chiến trên bàn ngoại giao nên mình đã có chút hut hẫng khi đọc chương ba (chương mình nhảy vô đọc đầu tiên). Có lẽ tác giả muốn đề cấp khái quát về nhiều lĩnh vực của Israel nên đã không viết không nhiều về chủ đề này. Dù vậy, cuốn sách cũng đã cung cấp rất nhiều thông tin hữu ích về con người, văn hóa, tôn giáo và chính sách của nhà nước Do Thái. 
4
140014
2015-11-24 11:01:29
--------------------------
332711
4878
Một quyển sách hay, khách quan, khá đầy đủ thông tin về lịch sử hình thành đất nước Do Thái, quá trình phục quốc, cuộc chiến Sinai, về cuộc chiến 6 ngày thần tốc rạng danh tướng Moshe Dayan và cả những thông tin trong các chương Chính sách kinh tế, chương Quốc gia khởi nghiệp sau đó giúp ta học hỏi thêm nhiều điều.

Khâm phục về dân tộc Do Thái, về David Ben Gurion đã lâu, từng được đọc về "Tướng độc nhãn Do Thái", nay muốn tìm và lưu giữ một cuốn sách cho bọn trẻ đọc, giữa nhiều cuốn sách viết về lịch sử dân tộc Do Thái, mình đã chọn cuốn này và đã không chọn lầm. Dù hơi tiếc chút vì sách không có nhiều hình ảnh minh họa, bản đồ cuộc chiến... nhưng vẫn khá hài lòng.

Một quyển sách đáng để có trong tủ sách gia đình.

4
42472
2015-11-06 20:33:49
--------------------------
332022
4878
Mình đã nghe nói về dân tộc hồi giáo khá lâu, lang thang trên tiki thấy cuốn này mình xơi luôn về đọc tìm hiểu sơ về họ, quả là một dân tộc kiên cường, gan dạ và đầy sóng gió để vượt qua, xem thời sự nhiều thấy cứ đánh nhau ở dải gaza này kia liên mà mà cứ tự hỏi sao họ cứ đánh nhau mù mịt mà chả khi nào ngừng nghĩ, cuốn sách này sẽ làm chúng ta khá sáng tỏ phần nào, và hiểu về con người, cuộc sống của dân tộc do thái, thật khâm phục và ngưỡng mộ một dân tộc kiên cường
5
882303
2015-11-05 16:18:43
--------------------------
330525
4878
Mình phải làm một bài thuyết trình về Trí tuệ Do Thái, thế nên mua cuốn này về đọc. 
Phải nói là hay cực luôn.
Cuốn sách gần như bao quát toàn bộ những điểm chính trong lịch sử 4000 năm từ trước công nguyên đến nay của dân tộc Do Thái, lối dẫn dắt như kể chuyện nên đọc không hề ngán xíu nào hết. 
Cuốn sách nhấn mạnh đến những biến cố lớn trong lịch sử của người Do Thái và cách mà họ đã đứng lên để xây dựng một đất nước cho riêng mình ngay chính tại vùng đất của tổ tiên họ hàng nghìn năm về trước, bị bao vây bởi các thế lực thù địch. Không những sống sót một cách thần kì mà họ còn đạt được những thành tựu rất đáng ngưỡng mộ.
5
385695
2015-11-02 20:09:30
--------------------------
292413
4878
Quyển sách hay, chứa đựng thông tin rất đầy đủ và bổ ích về dân tộc Do Thái. Tác giả đã dẫn dắt đọc giả đi xuyên suốt lịch sử Do Thái, giúp đọc giả hiểu được những phẩm chất rất riêng của người Do Thái để có thể phục quốc thành công sau hơn 2000 năm sống lưu vong khắp địa cầu. Đó là một tấm gương tốt cho các dân tộc trên thế giới noi theo, kể cả Việt Nam chúng ta. Tuy nhiên sách viết theo lối diễn đạt của tiếng Anh, cộng với ngôn ngữ hàn lâm nên một số chỗ hơi khó hiểu.
5
733817
2015-09-07 12:03:11
--------------------------
251613
4878
Nhà nước Do thái israel hiện đại ngày nay tuy có lịch sử phát triển không dài nhưng đã trải qua biết bao nhiêu biến cố. Từ vấn đề tôn giáo, lãnh thỗ, dân tộc, kinh tế, văn hóa, chính trị....đến cả vấn đề chiến tranh, lịch sử lập quốc. Tác giả Đặng Hoàng Xa đã đưa người đọc tìm hiểu qua tất cả các vấn đề nói trên 1 cách cô đọng nhưng sâu sắc nhất. Cách thức nhà nước Do thái vuợt qua khó khăn là bài học kinh nghiệm sâu sắc mà Việt Nam chúng ta có thể nghiên cứu và hoc hỏi. Đây là 1 tác phẩm không chỉ có giá trị văn học mà còn có cả giá trị xã hội đáng để suy ngẫm trong đó.
5
105202
2015-08-03 10:18:04
--------------------------
