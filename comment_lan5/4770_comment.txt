400100
4770
Là một fan trung thành của Doraemon từ khi mình mới biết đọc chữ. Doraemon không chỉ gắn liền với tuổi thơ của mình mà với tuổi thơ của rất nhiều bạn. Về hình thức, quyển sách được in màu rất đẹp mắt, sinh động và hấp dẫn người đọc. Về chất lượng, truyện rất hay, xoay quanh câu chuyện thường ngày của Nobita hậu đậu cùng Doraemon và những người bạn. Mình dám khẳng định rằng, một khi đã mê Doraemon rồi thì sẽ mê mãi về sau. Giá của quyển truyện rất hợp lí, vì là truyện màu nên đắt hơn truyện thường một chút. Tóm lại là tiền nào của nấy!
5
701204
2016-03-18 20:18:37
--------------------------
396271
4770
"Doraemon" – là một bộ truyện tranh rất hay của tác giả người Nhật Bản Fujio Fujiko. Đầu tiên phải kể đến điều mình ấn tượng nhất ở cuốn này là sách có in màu làm cho từng trang sách thêm đáng yêu và ngộ nghĩnh, cũng làm cho cuốn sách bắt mắt hơn. Về nội dung truyện thì cũng không cần phải nói nhiều, vì quá nhiều người biết đến và rất quen thuộc rồi. Qua những câu truyện thường ngày của cậu bé Nobita, chúng ta sẽ rút ra được rất nhiều bài học đáng suy ngẫm và nhiều điều như lòng dũng cảm, tình yêu thương và hơn tất thảy, "Doraemon" là hiện thân của một tình bạn cao đẹp. Một bộ truyện tranh bỏi ích!
5
1142307
2016-03-13 09:23:19
--------------------------
194994
4770
Mình là một fan bự của Doraemon từ truyện tranh(manga) cho tới các movie anime. Doraemon là một phần tuổi thơ của mình dù bây giờ mình đã rất lớn rồi nhưng vẫn không bỏ được thói quen tìm đọc những cuốn manga cũng như tìm xem các movie bản anime của Doraemon để được sống với tuổi thơ. Bản in màu này khá đẹp, bắt mắt và sinh động. Mèo Ú Doraemon rất dễ thương, Nobi Nobita hậu đậu, nhút nhát nhưng lại có tâm lòng lương thiện tuy không ít lần gây chuyện khi sử dụng bảo bối nhưng cậu chàng vẫn khiến người khác cảm thấy an tâm khi ở cạnh. Shizuka tốt bụng xinh xắn, Jaine với giọng hát kinh khủng nhưng rất trượng nghĩa, Suneo hay khoe bẽ nhưng cũng là một cậu chàng tốt bụng (khi cần) :)
5
578865
2015-05-12 14:49:57
--------------------------
