446289
5578
Trong Conan đối vs nhân vật nữa thì mình thích cô Eri nhất, tập này mình được chứng kiến 1 màn suy luận vô cùng sắc bén và chính xác từ cô ấy làm mình càng khâm phục hơn vì cô vừa đẹp lại vừa thông minh nữa. Đặc biệt tập này còn có những khoảnh khắc ngọt ngào lãng mạn và đôi khi rất hài hước như sự háo sắc của ông Mori bị Ran và Conan phát giác ngay từ file đầu tiên còn lãng mạn là khi ông Mori nói lới xin lỗi với Eri thì đã bị thu âm và nghe đi nghe lại miết bởi Eri vì cô nghĩ là vẫn chưa tha cho ông Mori được. Vụ án Mori bị tình nghi giết người được Erri phá như đã nói trên lộ diện 1 hung thủ ko ngờ tới- là người đi cùng cô Eri. Tiếp theo là vụ án hỏa hoạn quy mô lớn xen kẽ với vụ án từ 18 năm trước mà hung thủ đã hại chết bố của thanh tra Sato sẽ được đội thám tử nhí cùng nhau truy tìm và vụ này còn kéo theo cả sự sống chết của cả thanh tra Takagi nữa, nhưng cũng giúp chứng tỏ được tình yêu mà chú ấy dành cho cô Sato. Vụ án liên quan đến hai game thủ cũng tiết lộ 1 phần thú vị trong tính cách của giáo viên ngoại ngữ mới chuyển đến trường cấp 3 Teitan- Jolie Santemillion , 1 sự thật rất đáng yêu và bất ngờ. Gay cấn hơn cả là vụ án cuối cùng, khi Haibara và Mitsuhiko gặp nguy hiểm, phải đối mặt với một tên thợ săn giết nhầm người vô tội khi đi săn trong rừng cấm giấu đi tội ác, đi theo nhóm Conan để tìm gặp hai đứa trẻ và bịt đầu mối cho hành vi man rợn của hắn.
5
952213
2016-06-12 00:02:13
--------------------------
423994
5578
Trong bộ truyện Thám tử lừng danh Conan thì tập 27 này  là một trong những tập truyện khá hay và hấp dẫn. Đặc biệt, ở tập này nổi bật lên một nhân vật chính là vợ của bác Mori. Cái cách hai vợ chồng bác thám tử Mori dành tình cảm cho nhau một cách đáng yêu, chân thành dù đã ly thân cũng khiến người ta nghĩ nhiều về hai chữ "nghĩa tình". Và chính vợ của thám tử Mori cũng là người đã minh oan được cho bác ấy trong câu chuyện về phòng kín không tưởng. Bên cạnh đó, gây ấn tượng đáng nhớ nữa là vụ án tên thủ phảm phóng hỏa hàng loạt.

5
531312
2016-04-30 15:50:00
--------------------------
378031
5578
Tình cảm vợ chồng Mori dường như nhích lên không ngừng, và một vụ án lại xảy ra, và dưới sự suy đoán tài tình của Eri và Conan-Shinichi, Mori Kogoro rất cảm động.
Những chuyện kì lạ ngày xưa đã được khép lại, vụ án của cha của Sato, kết thúc bằng những giọt nước mắt của Sato, câu nói ngây ngơ của Trung sĩ Takagi! Thật sự đây là một cặp đội hết sức dễ thương, những hàng động vung về đối với cứ khiến người ta muốn giúp đỡ mà thôi!
Cô giáo Jodie Santemillion, một người vui tính và nghiện game, liệu rằng có phải là ai trong tổ chức áo đen? Thật sự rất lôi cuốn tôi!
5
97553
2016-02-04 07:12:19
--------------------------
374864
5578
Đôié với mình, conan tập 27 là một tập truyện khá hay và hấp dẫn. Truyện vẫn xoay quanh những nhân vật quen thuộc. Trong đó, nhân vật mình ấn tượng nhất ở tập này chính là vợ của bác Mori. Mặc dù 2 vợ chồng đã li thân nhưng vẫn dành tình cảm cho nhau một cách đáng yêu. Với tính cách mạnh mẽ và sự thông minh của mình, cô đã minh oan được cho bác râu kẽm trong vụ án phòng kín không tưởng. Ngoài ra, vụ án tên phóng hỏa hàng loạt cũng rất hấp dẫn. 
Một điểm cộng nho nhỏ là giấy và hình in rất đẹp. Tiki còn giao đúng hẹn nữa chứ  :*
4
302048
2016-01-27 13:41:59
--------------------------
349135
5578
Nhân vật nữ yêu thích nhì là luật sư Eri, vợ ông Mori và Haibara là nhân vật nữ mình yêu thích nhất. Bị teo nhỏ nhưng tính tình vẫn giữ nguyên, không trẻ con như Shinichi. 
Tính cô bé nghiêm nghị, không đùa, ra dáng phụ nữ, lạnh lùng, biết yêu, biết ghen tuông và cũng có lúc dữ tợn. Duy nhất cô ấy yếu đuối một lần là lúc khóc cho người chị đã mất trước mặt Conan. Cô bé luôn có những câu bình luận rất người lớn như "Cứ như vậy thì sớm muộn gì cũng chết".
5
535536
2015-12-07 19:57:06
--------------------------
209960
5578
Ôi, nếu như mọi tập thì người phá án là bác Mori ngủ gật, nhưng mà ở tập này thì người phá án lại là cô Eri, còn bác Mori thì... bị nghi là hung thủ giết người. Thật thú vị, bác Mori được một phen hú vía. Nhưng ấn tượng nhất là tập 27 mình ấn tượng nhất về vụ hoả hoạn. Mình tự hỏi là liệu tên tội phạm ấy có bị làm sao không mà lấy lửa ra phóng hoả bừa bãi? Nhưng mà chú Takasi cũng rất ấn tượng đấy chứ! Nhờ vậy mà thấy rõ được tình cảm mà chú dành cho cô Sato lớn ra sao. Hihi.
5
543425
2015-06-18 14:57:42
--------------------------
143231
5578
Những vụ án ly kì lại tiếp tục xảy ra, chàng thám tử tí hon của chúng ta lại trổ tài một cách tài tình. Tập này, bắt đầu bằng một án mạng trong phòng kín, tuy đây không phải lần đầu tiên mà chúng ta biết về vụ án như thế này nhưng đặc biệt ở đây là có vợ chồng ông Mori. Tôi cảm thấy rằng, phút chốc, tình cảm của họ đã dần nhích lại với nhau hơn một chút. Không những thế, cô thiếu úy Sato của chúng ta cũng có một phần kí ức không thể lãng quên nhưng sau tất cả vẫn là một kết thúc thật đẹp. Mặc dù, Conan đã quá quen thuộc với chúng ta nhưng bằng lối viết đầy sinh động, hấp dẫn. Tác giả đã đưa ra những lập luận, bằng chứng thuyết phục, rõ ràng làm cho người đọc như đi đến một chân trời mới lạ mà không thể phản biện được, chính điều đó, tôi nghĩ, là sức hút cho cả bộ truyện.
3
13723
2014-12-22 10:05:22
--------------------------
