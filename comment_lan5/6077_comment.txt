452280
6077
Trong tập 2 này Ngự Ngã đã thành công thuyết phục độc giả với những trận chiến đầy phấn khích mà cũng vô cùng hài hước ở thế giới ảo the second life. Trong đó mình vô cùng thích một biệt danh của Prince  đó là tinh linh đẫm máu. Ở thế giới thực, quả thật ban đầu mình hơi mất cân bằng vì Phong Làm quả thật rất đời thường. Song cũng không kém phần hấp dẫn bởi những mối quan hệ rắc rối thật thật ảo ảo. Nhưng cốt truyện vẫn logic và không phức tạp làm mình thêm một điểm cộng ở tác giả.
5
1289482
2016-06-20 15:49:58
--------------------------
442251
6077
Tập 2 của bộ LN là sự thay đổi giữa thực và mộng. Phong Lam đã gặp không chỉ 2 đồng đội của cô ngoài đời thực mà còn cả những người quen khác nữa. Đại hỗn chiến nổ ra vô cùng gay cấn và quyết liệt. Liệu đội nào sẽ chiến thắng và được vinh danh? Một tình huống dở khóc dở cười xảy ra khi Prince say rượu. Cậu sẽ phải làm gì để đối mặt với những khó khăn ở vùng đất mới?
Truyện rất hay và chắc chắn bạn sẽ không thể bỏ lỡ từng chi tiết trong tập 2 này!
Chúc các bạn đọc truyện vui vẻ!
5
1269214
2016-06-04 18:57:25
--------------------------
434858
6077
Trong truyện thích nhất anh Gui, đẹp mà dịu dàng, rất mong anh ấy và nhân vật chính thành một cặp,lúc trước coi bản online đã ấn tượng cách miêu tả tính cách của anh, vừa bí ẩn vừa rất đặc biệt, thêm nữa là tuyến tình cảm của nhân vật không quá sâu mà tập trung vào nội dung hành động hơn khiến câu chuyện không bị nhàm chán với những thước tình cảm sến súa. Nói chung cả về nội dung truyện, về hình thức bìa bookmark xuất bản và cả cách giao hàng của Tiki mình đều rất hài lòng.
5
992439
2016-05-24 07:42:12
--------------------------
390053
6077
Sách được gửi về (tuy chậm gần 1 tuần) nhưng mình vẫn rất vui. 1/2 Prince có nội dung hay, ngôn từ gần gũi, tuy có cả cảnh trong game và đời thật nhưng người đọc không hề bị lẫn lộn. Truyện tạo cho người đọc cảm giác vui vẻ, tình cảm, ko sướt mướt, không có nhiều pha ly kỳ nhưng lại tạo ra sức hút riêng của nó.
Đó là nội dung còn về phần sách, tuy được bọc bookcare nhìn ngoài khá ổn nhưng chất lượng quyển sách thì cực tệ. Giấy có chỗ ố vàng, nhăn và bị gập vài chỗ khiến 1 đứa cuồng sách như mình rất khó chịu. Mong tiki chú ý điểm này
5
527374
2016-03-03 10:13:19
--------------------------
378701
6077
Về nội dung,Nối tiếp tập một,sau cả quá trình miêu tả nhưng cuộc chiến cam go,ganh cấy trong game qua cái nhìn của Prince thì ở tập này vẫn tiếp tục như thế song Phong Lam đã có cơ hội khám phá ra nhưng người bạn thân thiết trong game là ai ngoài đời thực.Hóa ra Gui biến thái lại là vị giáo sư có IQ ba con số đang dạy học tại trường cô.Tà Linh-người đã thầm thương Phong Lam suốt tám năm trời lại chính là hàng xóm khi xưa của gai đình cô...Và còn rất nhiều sự thật khác được hế lộ.Cuộc sống đời thực hòa quyện với đời sống ảo song lại chưa từng lấn át nhau
Về hình thức,bìa truyện vô cùng đẹp và hấp dẫn.Đó cũng là một trong những lí do khiến mình muốn mua truyện này,lại còn được tặng kèm poster và bookmark màu.Yêu IPM quá đi mất thôi!!
5
889851
2016-02-06 23:16:32
--------------------------
376944
6077
Truyện tập 1 đã hấp dẫn rồi sang phần hai càng hấp dẫn hơn, cốt truyện lại mang ta tới vùng đất game online lôi cuốn mới mẻ đọc hoài không biết chán.
1/2 Prince cũng lôi cuốn mình chính vì điều đó. Không có tình cảm quá sướt mướt, không có quá nhiều pha li kỳ hấp dẫn nhưng nó thật sự rất hay. Ở tập 2 này, trong thế giới thực tại, Phong Lam đã gặp những đồng dội của mình ngoài đời thực, mặc dù đối phương không biết nhưng cũng tạo cho truyện một sức cuốn hút. Mình rất chờ mong giây phút mọi người biết được thân phận thật của Prince
5
585120
2016-02-01 11:39:29
--------------------------
348024
6077
sản phẩm được nhận dịch vụ bookcare nên được bao bọc rất kĩ ! nhân viên giao hàng rất tốt, mình đặt hàng chủ nhật mà tiki đã đóng gói xong trước dự định 1 ngày ( đóng gói rất nhanh), do giao sớm nên mình không để ý điện thoại luôn bật chế độ im lặng làm shipper gọi rất nhiều lần nhưng shipper rất tận tình, mình gọi lại họ vẫn trả lời rất nhã nhặn không bực bội, phong thái phục vụ khách hàng rất tốt! rất mong sẽ hợp tác với tiki lâu hơn nữa! kì mình mua sách là trong lễ hội giảm giá sách nên được ưu đãi rất nhiều ! 
5
938635
2015-12-05 13:54:49
--------------------------
338209
6077
Đọc tới tập 2, tôi đã bị Gui là cho cú vô cùng bất ngờ khi đọc tới phần ngoại truyện.Gui lúc đầu cứ nghĩ là Gay bẩm sinh ấy chứ :))))) ... ngự ngã khiến tôi cảm thấy vô cùng khâm phục , bộ truyện của ngự ngã cho tôi nhiều cảm xúc đã đọc là không muốn bỏ xuống , và cái nội dung là một thế giới ảo trong game. Một thế giới mà tôi nghĩ không ít người cũng muốn được trải nghiệm sống trong đó TvT Càng đọc càng hấp dẫn , tình tiết đầy bất ngờ, và khi đọc thật sự chẳng có nhân vật nào khiến tôi ghét cả, sự xuất hiện của ai cũng khiến tôi cảm thấy thích và không hề thừa thải cả. 
5
673868
2015-11-15 17:23:44
--------------------------
320523
6077
Thật sự mình cảm thấy Ngự Ngã có một cách suy nghĩ rất khác với hầu hết mọi tác giả khác, chính điều đó cũng làm cho tác phẩm của tác giả khác biệt hơn so với những tác phẩm khác. Và 1/2 Prince cũng lôi cuốn mình chính vì điều đó. Không có tình cảm quá sướt mướt, không có quá nhiều pha li kỳ hấp dẫn nhưng nó thật sự rất hay. Ở tập 2 này, trong thế giới thực tại, Phong Lam đã gặp những đồng dội của mình ngoài đời thực, mặc dù đối phương không biết nhưng cũng tạo cho truyện một sức cuốn hút. Mình rất chờ mong giây phút mọi người biết được thân phận thật của Prince
5
39471
2015-10-11 15:45:22
--------------------------
300750
6077
1/2 Hoàng Tử tập 2 với tên Thực Ảo Đại Hồn Chiến thực sự nghe tên mình đã rất thích rồi. Bìa tập 2 cũng tuyệt vời, thiết kế rất chuẩn, được tặng kèm bookmark 2 mặt rất đẹp nữa. Giấy thì tốt không cần phải nói luôn, các nhân vật trong 1/2 Hoàng Tử lúc nào cũng mang đến cho người đọc cảm giác vui vẻ, khá đồng tình. Ngự Ngã là một tác giả tuyệt vời đã xây dựng cốt truyện của bộ light novel này cực kì xuất sắc đối với người đọc và có thể nói là tuyệt vời trên cả tuyệt vời!
5
558720
2015-09-14 10:15:46
--------------------------
286638
6077
Vẫn luôn ấn tượng nhất trong thiết kế bìa, giấy, bookmark , các hình vẽ trong 1/2 hoàng tử, rất đẹp mắt và cẩn thẩn. Vẫn là nội dung và bối cảnh trong The Second Life nhưng đã rõ ràng và đi sâu hơn. Nếu như ở tập 1 nội dung câu truyện chỉ dừng ở mức độ làm quen và giới thiệu nhân vật, thì ở tập 2 các nhân vật qua trọng cùng đồng hành vơi nhân vật chính Prince hầu như đã xuất hiện đầy đủ, tài năng của nhân vật chính cũng bộc lộ rõ ràng hơn. Các cuộc chiến dấu, khó khăn, diễn biến câu chuyện dần dược hé mở. Câu chuyện phát triển lên tình bạn, tình yêu, đông đội cùng chiến đấu. Tuy nhiên vì lối viết với ngôi thứ nhất nên cảm xúc của nhân vật còn rây thiếu và không đi sâu, hầu hết chỉ có hành động.
5
328945
2015-09-01 21:53:05
--------------------------
283250
6077
Đọc xong tập 1 của quyển sách này rồi thì mình không thể nào ngừng đọc nữa, tập 2 còn hấp dẫn hơn gấp bội. Bối cảnh là một Game online quá hấp dẫn, kích thích người đọc.

Vẫn lối viết đầy sáng tạo, tác giả giúp câu chuyện câu chuyện càng được mở rộng hơn, càng nhiều điều thú vị và bí ẩn hơn, nhân vật chính thì mình rất thích, rất cả tính, có cái tôi, một hình tượng rất đẹp.

Chất lượng sách vẫn không khác mấy so với tập 1. Màu đẹp, bìa lung linh :3
5
621744
2015-08-29 20:43:49
--------------------------
281317
6077
Thật sự sẽ là thiếu sót nếu mua tập 1 mà không mua tập 2 của serie Lightnovel này. Phải nói thật thì càng đọc bạn sẽ càng không thể dứt ra được. Lấy bối cảnh là Gameonline (GO), cuộc sống ảo nhưng các chi tiết được miêu tả cực kì sống động, xoay quanh yêu nhân Prince là một cô gái có cá tính cực kỳ đáng yêu. 
Điểm cộng: Hình thức bìa là 1 điểm cộng cho truyện. Cực kỳ đẹp trai a. Phải nó là GO mà có nhân vật thế này mình cũng phải cắm đầu cắm cổ chơi dù hiện tại GO nào cũng chơi nhưng không phải con nghiện.
Chất giấy ok. Trang màu cuối truyện đẹp lung linh.

5
195704
2015-08-28 12:33:28
--------------------------
275694
6077
Đây là lần đầu tiên tôi đọc thể loại này càng đọc tôi lại càng bị thu hút. Đây là một tác phẩm đầy tính sáng tạo, kể về game online mà người chơi có thể trải nghiệm rất sống động. Nối tiếp phần 1, phần 2 rất hấp dẫn về trận chiến của team Phi Thường và hành trình đến phương Đông của Prince. Tôi rất thích nhân vật này, tuy là con gái nhưng cô lại có sự phóng khoáng, trượng nghĩa của bậc nam nhi cũng như sự tốt bụng, lạc quan khiến mọi người yêu mến. Ngoài ra truyện được trang trí đẹp mắt nhất là hai chiếc bookmark cùng trang màu cuối truyện hết sức dễ thương cũng là một điểm cộng cho truyện.
4
484991
2015-08-23 09:29:24
--------------------------
264783
6077
Mình được biết đến bộ tiểu thuyết "1/2 Hoàng tử" này qua bộ truyện tranh chuyển thề cùng tên. Có thể nói đây là một tác phẩm vô cùng sáng tạo, viết về một loại game thực thể mà người chơi có thể trải nghiệm cuộc sống như các nhân vật mà mình chọn thông qua một thiết bị. Điểm đặc sắc của truyện là nhóm những người bạn vô cùng đặc biệt trong nhóm Phi Thường. Nếu như ở tập 1 là sự làm quen của những thành viên kì quái thì sang đến tập 2, tình huống truyện được đưa đến hồi gay cấn khi họ tham gia vào cuộc bang chiến đầy kịch tính, nhưng cũng vô cùng hài hước. Mình vô cùng thích bìa sách được thiết kế đẹp mắt, tên truyện được phủ bạc tạo nên sự tinh tế cho cuốn truyện. Thật hi vọng những tình huống đặc biệt ở tập 3
5
295410
2015-08-13 14:19:43
--------------------------
263334
6077
Hình thức truyện ổn, kèm 1 trang màu hình Oa Oa rất dễ thương và 1 bookmark hình giống trang bìa. Quyển truyện tiki giao mình cũng rất ổn.
Về nội dung thì vẫn rất hài hước, đầy kịch tính không gây nhàm chán. Ở tập 2 Prince gặp mọi người ở ngoài đời thực và mở ra 1 cuộc phiêu lưu mới của Prince do Prince bị lạc không thể quay về với nhiều tình huống bất ngờ khác. Giờ thì mua tập 3 và 4 chờ đến ngày được đọc thôi, mong rằng truyện vẫn giữ được phong độ.
5
230068
2015-08-12 14:38:27
--------------------------
246546
6077
Đọc tập 1 khiến mình rất kì vọng vào tập 2 .Đương nhiên tập 2 không những hấp dẫn , gay cấn mà còn nhiều tình tiết khiến độc giả không nhịn cười nổi . Hơn nữa , tập 2 mở ra rất nhiều tình tiết hấp dẫn . Không chỉ đề cập đến đại hội mạo hiểm , mà còn đề cập đến nhiền nhân tố mà chính người đọc không ngờ tới được . Có mở có kết rất hợp lý . Mình rất thích cách đính kèm poster và giới thiệu nhân vật bằng hình vẽ ở gần cuối trang . Dễ khiến người đọc tưởng tượng hơn . Mong chờ tập sau 
5
506140
2015-07-29 18:13:02
--------------------------
245266
6077
Tập 2 nối tiếp tập 1 và những trận chiến của team Phi Thường (nghe cái tên là biết trong nhóm không có ai bình thường rồi). Tập này vẫn hay như tập trước, hấp dẫn và gay cấn hơn với những trận chiến dở khóc dở cười. Một vài bí ẩn đã bắt đầu được gỡ nút thắt, thế giới thực và thế giới ảo ngày càng hỗn loạn, đúng với lời giới thiệu "thực ảo đại hỗn chiến". Cuối truyện còn có 1 trang minh họa màu cực lung linh, kèm một bookmark cũng long lanh không kém. 
5
496854
2015-07-28 20:32:31
--------------------------
239630
6077
Nếu như tập đầu tiên là hành trình trở thành nhân yêu của Phong Lam thì tập hai lại chính là khi những gì đáng ra là ảo lại bắt đầu xuất hiện ở thế giới thật. Phong Lam vừa phải cố che giấu thân phận Prince của mình vừa phải cố cư xử như không biết những người mình quen ở trong trò chơi lẫn ở ngoài đời. Tập hai cũng là khởi đầu cho một trong những rắc rối lớn nhất của Prince sau này khi mà đội Phi Thường tham gia vào cuộc thi đấu gay cấn và có phần tàn khốc. Nhiều trận đấu giống như để cho vui thôi hay sao ấy.
5
226020
2015-07-24 00:19:02
--------------------------
221272
6077
Thể loại này lần đầu tôi đọc đến, trước giờ bản thân rất không thích truyện mà lồng nội dung game vào vì cảm thấy chúng rất ảo, không thực lại không thú vị. Nhưng bộ truyện "1/2 hoàng tử" này lại là một sự khác biệt. Trước đây có thấy nhỏ bạn đọc qua nhưng không mấy hứng thú, khi thấy bán trên tiki có rất nhiều người yêu thích thì quyết định mua về đọc thử. Và rồi quả thật là một quyết định đúng đắn. Truyện kể về nội dung ảo nhưng lại rất chân thật, các thông tin trong truyện cũng dễ dàng cho cả người không có kinh nghiệm hay có kinh nghiệm chơi game online tiếp nhận. Các nhân vật được xây dưng rất hợp lí, phối hợp ăn ý với nhau. Văn phong không quá trau truốt nhưng bù lại nội dung lại khiến cho người đọc cảm thấy thích thú muốn tìm hiểu tiếp.
Trong tập hai này nội dung có phần bất ngờ hơn, ngay từ phần giới thiệu đã tạo sự tò mò thắc mắc Hoàng tử sẽ làm gì tiếp theo. Đọc xong tập hai thấy thoả mãn nhưng cũng không cưỡng lại muốn đọc tiếp tập tiếp theo. Bìa sách cả tập một và tập hai đều rất đẹp ( tập hai là hình của gui), cuối tập còn có trang màu giới thiệu nhân vật. Trang đó cả hai mặt đều có hình màu, nhưng một mặt có hình vẽ rất đẹp, mặt kia lại sơ sài, hơi xấu hơn, hình như tập nào cũng như vậy. Mong là nxb sẽ khắc phục chịu khó chọn hình đẹp tí.
5
91482
2015-07-03 17:25:37
--------------------------
221065
6077
Mua được hai tập đầu, đợt ra tập 3 quên mất không cập nhật thông tin thế là giờ muốn mua lại hết hàng. Buồn quá :'( mong là sẽ được in thêm. 
À mà giờ nhận xét trước mấy tập này. 
Đầu tiên phải nói đến cái bìa, tên truyện được mạ kim cộng với hình bìa lung linh đẹp quá trời >.< Trong truyện còn có mấy ảnh minh họa lung linh lung linh >..< nhìn mà sướng.
 Truyện hài vô cùng, dịch rất tốt, đọc mấy lần mà vẫn thấy hay. 
Mong là có thể cầm được tập 3 trước khi mua tập 4, bây giờ nghĩ vẫn thấy tiếc huhuhu
À tóm lại truyện rất đáng mua nha.
5
358661
2015-07-03 12:48:17
--------------------------
217739
6077
Tập 2 tiếp tục là những cuộc đấu gây cấn, nhưng không thể thiếu yếu tố hài hước, gây cười, đó chính là điểm mình yêu thích ở bộ truyện này, bỏ qua cốt truyện, bỏ qua nhân vật, chỉ cần tác giả tạo ra một bộ truyện mà khiến cho người đọc có được thứ mình cần thì quả là hay rồi. Với lại mình không ham mê chơi game lắm, nhưng đọc cũng thấy rất thích thú :))
Về chất lượng sách thì không có gì để nói, bản in rất tốt, nhưng bìa sách có vẻ hơi rắc rối, theo kiểu bìa truyện kiếm hiệp, hơi nhàm chán.

4
37970
2015-06-29 21:14:25
--------------------------
214486
6077
Mua được hai tập đầu, đợt ra tập 3 bận không cập nhật thông tin thế là giờ hết hàng. Buồn ghê gớm, mong là sẽ được in thêm.  Thích truyện này lắm, trước tiên phải nói đến cái bìa, tên truyện được mạ kim cộng với hình bìa lung linh nhìn sướng hết cả mắt. Chưa kể bên trong còn có vài hình minh họa cũng đẹp lung linh luôn. Lâu lâu lôi ra ngắm thấy cực mãn nguyện :)). Truyện hài vô cùng, rất ưng ý lời dịch, đọc mấy lần rồi vẫn thấy buòn cười. Kết nhất giáo sư Gui, yêu Prince không cần biết giới tính, thật dễ thương, hi vọng kết thúc sẽ là cặp đôi này ♥.
Mong là mình sẽ được cầm trong tay tập 3 trước khi phát hành tập 4. Ôi nghĩ mà đau long quá :((
5
391056
2015-06-25 01:06:06
--------------------------
207765
6077
Mình đến với 1/2 prince qua truyện tranh. Đã đọc truyện tranh rồi nhưng mình vẫn muốn sở hữu bộ truyện này bản chữ vì mình biết rằng nguyên bản gốc của nó là bản truyện chữ. Đọc truyện chữ có cảm giác khác hẳn với truyện tranh luôn. Nội dung truyện thì khỏi nói rồi, bìa truyện cũng rất đẹp mắt, lại còn có bìa rời nữa!!!
Trong trang sách cũng trình bày rất đẹp. Và còn có cả bookmark nữa, mình đánh giá cao truyện của IPM vì bìa sách đẹp mắt cuốn hút và có nhiều trang màu trong truyện.
Tuy nhiên trong truyện vẫn còn một vài lỗi nhỏ và mình không thích tên nhân vật được việt hóa hết. Quen đọc tên nhân vật trong truyện tranh rồi nên đọc tên nhân vật trong truyện thấy không quen mấy.
5
526906
2015-06-13 07:41:11
--------------------------
172849
6077
Đọc bình luận thấy các bạn khen nhiều. Riêng quan điểm cá nhân của mình vẫn chưa được hài lòng mấy.
Đúng là truyện có đầy đủ những cá tính riêng, về tình bạn chân thành của đội Phi Thường, những cuộc chiến nảy lửa và những nổ lực không ngừng.
Nhưng dường như truyện bị làm hơi quá lên ở những đoạn nói về sự mê trai, về thầy giáo đẹp trai, về những đoạn hài mà mình nghĩ là nó đang cố gắng tạo cười. Mình lại càng thấy sự tiêu cực trong một cộng đồng sống ảo.
Chắc có lẽ mình không phải một người chơi game giỏi lắm.
Tuy nhiên, sách rất đẹp, về hình thức thì mình rất thích!
4
39173
2015-03-24 23:53:16
--------------------------
164579
6077
Nói thật là hiện tại (lúc tôi nhận xét thì tôi vừa đọc xong 2 cuốn này vào ngày trước và đọc một nửa cuốn 3) thì tôi rất rất là ngóng 4 tập còn lại mau xuất hiện trên Tiki giùm tôi. Nhưng cũng nói thật là ở tập 1, tôi ghét Gui như chưa từng ghét ai vậy nhưng trong tập này thì tôi thích Gui hơn hẳn (không phải vì anh ta là giáo sư Mẫn Cư Văn hay đập chai đâu) vì anh ta có một hoàn cảnh khá giống tôi lúc này - thất tình và tôi hiểu thêm về bản thân. Nhưng quay về với Real life hay Second life của cả đội, tôi thấm thía hơn một chút qua tình bạn của đội Phi Thường - đặc biệt là Prince với những gì không thể buồn cười hơn. Nào thì đánh nhau, nào thì hòa hợp, tính mê trai,.... tất cả đều rất thật trong 1/2 Hoàng tử. Nói chung là tôi thích bộ truyện này: 5 sao không hề hối tiếc. Ai không đọc uổng phí cả đời chơi game online hay sống ở Real life. Chúc các bạn đọc một ngày, một tháng và một năm không thể tốt lành hơn. Cho hỏi có phải truyện này của Trung Hoa không??? giải đáp nhanh nha... Thanks trước vì con bạn đấu khẩu dữ dội.
5
513286
2015-03-08 09:30:45
--------------------------
162669
6077
Sau khi đọc tập 1 của 1/2 Hoàng tử mình đã chờ rất lâu IPM mới phát hành tập 2, theo mình nhớ chính xác là gần 5 tháng hix hix. Tập 2 là nối tiếp những trận đấu gay cấn trên đấu trường của giải đấu giữa các đội trong game để giành phần thưởng là một thành phố riêng. Những trận đấu gay cấn và kết thúc với kết quả bất ngờ khiến bạn cười lăn cười bò. Nếu là một người từng chơi qua thể loại game nhập vai, từng sống trong thế giới ảo thì bạn càng như thấy chính bản thân mình trong đó. Những người bạn quen biết trong game, cùng nhau vày level, cùng nhau đánh quái, công thành và cho đến khi gặp nhau ở thế giới thực.Bạn sẽ thật sự bị hấp dẫn bở quyển truyện này.
5
313795
2015-03-02 23:11:43
--------------------------
146782
6077
Tôi bắt gặp cuốn sách trong một lần tìm kiếm trên Tiki. Và quả là một quyết định đúng

đắn khi rinh cuốn sách về nhà. Truyện kể về người con gái mang tên Phong Lam tham gia 

Second Life – game với độ chân thật tới 99%. Truyện xây dựng trên bối cảnh những trận 

chiến nảy lửa, làm ngưng trái tim người đọc qua những nhiệm vụ mà Hoàng Tử - nhân yêu 

cũng chính là Phong Lam giả thành nam, tác chiến. Hãy đọc truyện để tìm kiếm được cho 

bạn những tình cảm yêu thương kỳ lạ từ những con người quen biết từ thế giới ảo, một 

chặng đường dài của những hạnh phúc kỳ dị.

Ngoài ra, bìa sách còn được trang trí rất bắt mắt. Vừa cứng lại đảm bảo giữ gìn sách mới 

đẹp.
5
296749
2015-01-05 14:53:53
--------------------------
126371
6077
Sau khi đọc tập 1 của 1/2 hoàng tử mình lúc nào cũng ngồi đếm từng giây từng phút không biết chừng nào tập 2 mới xuất bản đây . Sau khi em í ra lò mình chỉ còn thiếu nỗi nâng niu em í như nâng trứng nâng vàng mà thôi . Tiếp nối phần 1 phần 2 hé lộ nhiều tình tiết hồi hộp hơn rất nhiều . Từng bí mật bị bọc sâu trong bóng tối những tưởng sẽ mãi mãi không bao giờ khám phá ra nhưng từng lớp một hiện ra trước mắt mình .Thế giới trong game và thế giói ngoài đời như trộn lẫn vào nhau hoàn toàn không thể phân biệt được cái nào ra cái nào . Hình ảnh minh họa rất đẹp và rõ nét ngoài ra khi nhìn vào có cảm giác rất chân thực và sống động
5
337423
2014-09-18 18:22:27
--------------------------
