329151
2909
Trong tập truyện ngụ ngôn này gồm ba câu chuyện: "Mèo con đi câu", "Chú dế ham chơi", "Voi biến thành heo".
. Mỗi câu chuyện là một bài học nhẹ nhàng, dí dỏm. Từ bài học về tính kiên nhẫn mà mèo con được mẹ dạy, đến cái giá mà chú dế lười biếng phải trả và cuối cùng là bài học dành cho voi con khi chú không chịu lao động mà lại rất lười biềng. Vì là truyện song ngữ nên các bé khi đọc truyện sẽ biết thêm từ mới, như là biết thêm tên tiếng Anh của Mèo, Voi và Dế,..
5
680594
2015-10-31 09:44:43
--------------------------
