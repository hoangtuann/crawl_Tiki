489621
8269
Đây là tác phẩm đầu tiên mình có cơ hội được đọc của Nguyễn Nhật Ánh dù nghe danh ông đã lâu. Và thực sự mình đã hiểu vì sao Nguyễn Nhật Ánh lại là một trong số ít nhà văn được mọi người biết đến nhiều vậy. 'Nữ sinh' mang một giọng văn nhẹ nhàng, không dồn dập, không có quá nhiều tình tiết, chủ yếu là những câu thoại, những cuộc nói chuyện của tuyến nhân vật chính nhưng vẫn có sức hút kì lạ, riêng mình thì không thể rời mắt khỏi từng trang sách. Truyện tính cả nhân vật phụ và chính xuất hiện thì chỉ có 5 người nhưng tạo nên cốt truyện vô cùng lôi cuốn. Nói chung theo cảm nhận của mình thì đây là một cuốn sách hay, nội dung đơn giản, nhẹ nhàng mà rất hấp dẫn, phù hợp với lứa tuổi học trò.
5
1444806
2016-11-08 22:15:48
--------------------------
441078
8269
Đây là một trong những cuốn truyện viết về tuổi học trò, những cô cậu học sinh cấp 3 của chú Ánh. Mỗi câu chuyện mang một màu sắc riêng, một cốt truyện riêng, một sắc thái riêng. "Nữ sinh" cũng mang sắc thái tiêng. Đây là cuốn truyện viết về 3 cô gái tuổi học trò rất nghịch ngợm nhưng mà cũng rất giàu tình cảm, truyện đúng lứa tuổi của nó. Những cô gái cấp 3 với đủ trò nghịch ngợm, quậy phá. Truyện cũng đầy tính hình tượng về một thầy Gia tốt bụng, trẻ trung đầy tâm huyết. Và không thế thiếu trong mỗi truyện của chú Ánh là tính nhân văn sâu sắc về tình bạn bè.
4
470874
2016-06-02 19:54:58
--------------------------
439025
8269
Trong truyện này, bộ ba tinh quái Xuyến, Thục, Cúc Hương vừa ở trong những chương đầu đã tìm thấy đối tượng để trêu đùa, bày bao trò quái quỉ. Đó không ai khác lại chính là anh chàng Gia-khách quen của quán nước ngay đối diện trường của cả bọn. Bộ ba này hành anh đến khổ: đi vắng thì phải "xin phép", nhận lấy "vinh dự được" trả tiền chè cho ba cô nương, không tuân thủ sẽ bị phạt,... Nhưng xung quanh câu chuyện này lại là một nhân vật phụ nhưng góp công đáng kể cho chuyện: Hùng quăn. Những lá thư tình của cậu bạn này gửi cho Cúc Hương không ngờ lại dẫn đến vết sẹo trên đầu Gia. Chưa xong chuyện này, cả ba cô học trò lại ngã ngửa khi biết "anh" Gia lại chính là thầy chủ nhiệm lớp mình. Rồi khi thấy Hùng quăn liên tục trốn tránh thầy để nghỉ học, "anh" Gia tốt bụng đã đến nhà và thấy tình cảnh của Hùng, đồng thời lên phường tìm hiểu... Nhờ Gia, Hùng mới được gặp lại bố và còn ở nhà của thầy mình để chuyên tâm học. Câu chuyện thật ý nghĩa mà cũng thật đậm chất học trò!
5
872188
2016-05-30 19:35:19
--------------------------
423287
8269
Mình rất mê truyện của bác Ánh. Bởi vậy mà Nữ sinh là cuốn truyện không thể không có trong bộ sưu tập của mình. Vẫn xoay quanh câu chuyện về Xuyến, Thục, Cúc Hương. Nhưng người đọc không thấy nhàm chán khi 3 cô đã từng xuất hiện trong "Buổi chiều window" và "Bồ câu không đưa thư". Câu chuyện thật sự rất đậm chất tuổi trẻ, khi 3 cô luôn cảm thấy hào hứng bởi tìm được một người để giao cho "vinh dự được trả tiền chè", khi Hùng quăn rình ở quán chè đánh "anh chàng Gia" vì ghen, vì ngỡ anh ta đang nhăm nhe tán tỉnh Cúc Hương, khi 3 cô được thầy Gia giới thiệu cho làm thêm tại một công ty tin học,...
Xuyên suốt tác phầm là những chi tiết nhẹ nhàng, chân thực, đậm chất thực tế, hài hước, nhưng cũng đậm chất nhân văn khi thầy Gia không những không trách câu học trò nhỏ đã dùng đá chọi mình vì ghen mà còn cho cậu ở nhờ để không bị dở dang việc học....
Với mình, đây thật sự là một tác phẩm rất tuyệt của bác Ánh,
5
208200
2016-04-28 23:01:22
--------------------------
422893
8269
Đã đọc cuốn này cách đây mười mấy năm, đã xem phim nhưng vẫn muốn có một cuốn là của riêng nên mình vẫn quyết mua cuốn này.
Câu chuyện xoay quanh anh chàng Gia lãng tử, đẹp trai và bí ẩn đối với 3 cô gái Xuyến, Thục, Cúc Hương. Các cô tha hồ hành hạ anh đủ điều nhưng không biết tí gì về anh cho tới khi làm...thám tử truy tìm nhân vật mất tích mấy ngày không đến quán cây Sứ quen thuộc. Và bất ngờ hơn khi anh nhận công tác và làm chủ nhiệm lớp các cô. Câu chuyện cứ đưa ba nàng nữ sinh tinh nghịch và cả độc giả đi từ bất ngờ này đến bất ngờ khác.

4
1233753
2016-04-28 12:09:29
--------------------------
421277
8269
Tuổi học trò bao giờ cũng là lứa tuổi đẹp nhất của đời người. Vô tư, hồn nhiên, tinh nghịch, trong sáng. Nguyễn Nhật Ánh có lẽ là nhà văn được lòng nhiều thế hệ độc giả nhất từ người già đến trẻ em. Đọc truyện Nữ sinh, chúng ta bắt gặp đâu đó hình ảnh bản thân mình với những buổi tan trường tại một quán che, quán kem nào đó, cùng nhau ăn uống và tán gẫu chuyện trên trời dưới biển. Rồi tình yêu trong sáng đầu đời cũng bắt đầu chớm nở, thật ngọt ngào và đáng nhớ. Không kém phần quan trọng để những tâm hồn trẻ con ấy định hướng đúng trên đường đời của mình là sự góp phần của thầy giáo làm liêu xiêu trái tim của cô học trò, thầy giáo Gia. 
Đọc và cảm nhận xem, có khi nào bạn bắt gặp bản thân mình trong câu chuyện trên. Mình là ai trong số những nhân vật đó!
4
505896
2016-04-24 19:53:19
--------------------------
421083
8269
Các tác phẩm của Nguyễn Nhật Ánh đều là những tác phẩm hay và sâu lắng, luôn để lại trong lòng người đọc một chút dư vị cảm xúc. Và truyện Nữ sinh cũng vậy. Truyện như một chiếc máy thời gian đưa tôi trở về quá khứ, nơi huy hoàng của tuổi học trò ngây thơ, trong sáng. Truyện mang đến ta những cô nữ sinh xinh đẹp, đáng yêu. Từ tình tiết này đến tình tiết khác, truyện luôn cho ta cảm giác hồi hộp và thú vị. Quả thật, truyện của Nguyễn Nhật Ánh không làm cho tôi thất vọng, vừa giải trí vừa mang đến những bài học ý nghĩa, bổ ích.
5
1224244
2016-04-24 14:03:07
--------------------------
419406
8269
Các tác phẩm của Nguyễn Nhật Ánh hầu hết đều có nội dung nhẹ nhàng, đáng yêu và đầy nhân văn; Nữ sinh cũng không ngoại lệ.

Truyện giúp tôi quay ngược lại thuở học trò với những trò đùa tinh nghịch nhưng đầy hồn nhiên, trong sáng.
Hình ảnh thầy giáo Gia trẻ tuổi là một hình ảnh đẹp về người đứng trên bục giảng. Trẻ nhưng tâm huyết với nghề và có tấm lòng nhẫn nại đầy bao dung đối với học trò. Khiến cho người đọc cảm thấy ấm lòng và vẫn có niềm tin vào những điều tốt đẹp trong cuộc sống bộn bề đầy lo toan và tình người ngày càng mất đi hiện nay.

4
772211
2016-04-21 10:41:34
--------------------------
415648
8269
Bạn sẽ không thể ngờ đến những tình tiết của câu chuyện đâu nhé.
Những cô gái cấp ba xinh đẹp học giỏi nhưng có chút ngỗ ngác nhưng không phải vô duyên. Tự nhiên đi bắt chuyện với người không quen biết... và rồi người ấy lại chính là....
Đọc đi thì biết ;)
Tác phẩm nào của NN Ánh cũng vậy, không những mang đến sự giải trí mà còn có cả những bài học cho độc giả, đặc biệt là ở lứa tuổi mới lớn, đang sống trong vòng tay của thầy cô và bạn bè như chúng ta.
4
1171197
2016-04-13 20:38:44
--------------------------
405324
8269
Mình đã đọc quyển này thời từ hồi in trên báo Mưc Tím  (sau mới xuất bản thành sách). Chuyển nhà vài bận nên mất hết, đến giờ mới có thời gian mua lại được.  Xuất bản cũng là lần 32,33 gì rồi. Tính ra cũng nhiều năm trôi qua...

Sách nay được in với màu sắc chan hòa, dễ chịu.Nội dung cũng không thay đổi gì mấy, cũng Xuyến, Thục, Phong Khê,...Là một trong những tập truyện dài nhưng mỏng nhất của bác Ánh.

Truyện học trò của bác Nguyễn Nhật Ánh lâu lâu cũng toàn kết thúc buồn, mà mình thì hông muốn vậy đâu :((. 
4
1213989
2016-03-26 16:05:18
--------------------------
400847
8269
Như rất nhiều quyển truyện của Nguyễn Nhật Ánh, " Nữ sinh" là một quyển truyện rất hay. Truyện mang một vẻ vô cùng nhẹ nhàng, trong sáng của tuổi học trò mà cũng có tính giáo dục nhẹ nhàng. Truyện đã đưa người đọc đến với những cảm nhận thật sâu sắc, thú vị như tìm thấy chính mình trong câu chuyện. Mình cảm thấy vô cùng thích thú với cuốn sách này, truyện đã tái hiện cả một khoảng trời thật tươi đẹp của tuổi học trò. Mình thực sự rất thích quyển truyện này nói riêng và các truyện khác của bác Nguyễn Nhật Ánh nói chung.
5
1174527
2016-03-19 22:30:40
--------------------------
396351
8269
3 cô nàng này thiệt hết biết ^^ đáng yêu, tinh nghịch , nhí nhảnh, thông minh và có cái duyên chết người :)) mình thích nàng Xuyến nhất, tính tình mạnh mẽ, quyết đoán (không ai có thể bắt nạt được cô nàng) lại hài hước nữa, nàng Cúc Hương thì thôi á, pha trò số 1 luôn, nàng Thục có vẻ lép vế trước 2 cô bạn nghịch ngợm của mình nhưng vẫn có nét cuốn hút riêng, nàng giỏi văn nên nàng có tâm hồn lãng mạn. Các cô nàng quay anh chàng thực tập mệt nghỉ luôn ^^ thế mới có chuyện để đọc :))
5
1173756
2016-03-13 12:14:24
--------------------------
392665
8269
Đây thực sự là tác phẩm dành riêng cho tuổi học trò, đặc biệt là thế hệ 8x. Những ly chè, những buổi xem kịch, những cô nữ sinh tinh nghịch qua nét bút của NNA đã thực sự gợi lại 1 thời đáng để nhớ của con người.
Có 1 số bạn bảo đây không phải tác phẩm thực sự hay của NNA nhưng theo tôi, đây là tác phẩm hoàn hảo cho câu chuyện dài của 3 cô nàng Xuyến, Thục, Cúc Hương qua 3 tác phẩm: Nữ sinh, Bồ câu không đưa thư và Buổi chiều Widows. 
Cúc Hương hài hước, Xuyến thông minh ranh mãnh, Thục nhút nhát rục rè. 3 tạo hình của NNA đã thực sự gây ấn tượng mạnh cho người đọc. Chắc hẳn những ai đã trót đọc Nữ sinh thì phải mau mau tìm đọc 2 tác phẩm còn lại.
4
1151345
2016-03-07 13:37:46
--------------------------
389596
8269
Tớ biết đến cuốn sách này qua bộ phim đã được chuyển thể trước đó một thời gian. Ấn tượng về bộ phim là bộ tứ cô nương lắm chiêu trò những cô học sinh rất dễ thương đáng mến. Nhưng lúc đọc cuốn sách thì lại có một cảm giác hoàn toàn rõ nét hơn về bốn cô nữ sinh này. Câu chuyện cho ta cảm nhận được hết những tâm tình ngây ngô của những cô cậu mới lớn mơ mộng hồn nhiên trong sáng. ta thấy được một phần mình trong đó, một chút dịu dàng của Thục, chút tinh quái, bướng bỉnh, chiêu trò của Xuyến, Cúc, Hương. Tất cả làm nên những kỉ niệm đáng nhớ của tuổi học trò hồn nhiên ấy
4
1161104
2016-03-02 13:36:28
--------------------------
387091
8269
Đời sống học sinh cấp 3 quen thuộc được tái hiện một cách rõ rệt ngay từ những trang đầu tiên của quyển sách này. Đọc nó, có cảm tưởng như mình được quay đến tương lai sau này của mình khi đi học vậy: đi học với những người bạn tinh nghịch, rồi ra về lại ghé quán chè, quán bánh nào đó nghỉ ngơi, rồi một cuộc gặp gỡ chẳng ngờ đến… Một cuốn sách với giọng văn hài hước, cách miêu tả nhẹ nhàng cùng những cảnh quan than thuộc đỗi với lứa tuổi học sinh.   Thật sự mình rất thích những câu chuyện viết về ba cô nữ sinh này, yêu nhất là Xuyến – nhân vật cá tính và dễ thương nhất truyện. Cơ mà dù biết là không thể, đọc đến gần cuối truyện rồi mình vẫn mong Xuyến và thầy Gia được chú Ánh nối tơ duyên cho (mà đúng là không được nhỉ), buồn mỗi chuyện đó chứ còn lại cả tác phẩm đều hoàn hảo.  

5
752893
2016-02-26 16:48:43
--------------------------
386275
8269
Đối với riêng tôi! sách của NGUYỄN NHẬT ÁNH mang lại tất cả kí ức tuổi học trò mà ngở rằng sẽ khó để nhớ!!!Một câu chuyện  học trò quá hay!! 3 cô gái THỤC, XUYẾN, CÚC HƯƠNG mang đến cho người  đọc niềm vui và tuổi học trò ngây thơ hồn nhiên đem đến biết bao nụ cười!!! 
Tôi đã vô tình đọc được cuốn '' bồ câu không đưa thư''- rồi đến'' nữ sinh'' khi biết có quyển ''buổi chiều windows'' có 3 nhân vật trên thì tôi cũng đã được đọC!! mọi người hãy đọc nhé! 1 chuỗi tình cảm học trò!!!!! 
4
1120230
2016-02-25 13:33:52
--------------------------
384720
8269
quả thật bất cứ tác phẩm nào của Nguyễn Nhật Ánh cũng để lại trong lòng người đọc những kỉ niệm đẹp qua nữ sinh ta thấy thấp thoáng đâu đó trong chính bản thân chúng ta cũng có hình ảnh thục một con người rụt rè và cả xuyến với cúc hương qua tác phẩm này một lần nữa nguyễn nhật ánh đã cho ta thấy được sự gắn kết trong con người ông có thể nói sưt gằn kết giữa hiện tại và quá khứ là sự gắn kết thành công nhất của ông hy vọng nguyễn nhật ánh sẽ tiếp tục phát triển sự gắn kết ấy cho chúng ta
4
938389
2016-02-22 21:34:38
--------------------------
384430
8269
Thường thì truyện Nguyễn Nhật Ánh vẫn có giá khá cao nên khi mua mình vẫn rất tiếc :'( nhưng mà truyện Nữ sinh thì khác , giá rẻ hơn và còn được tiki giảm giá nữa nên túi tiền đỡ bị hao hụt . Truyện đọc vào rất hay , mình rất thích sự lém lỉnh của mấy nữ sinh trong truyện và sự dễ thương của anh chàng Gia nữa . Truyện như đem thêm làn gió mới đến cho mọi người để sống hòa hợp vui vẻ và yêu đời hơn . Hy vọng mọi người cũng thích câu truyện này ^^
5
794067
2016-02-22 12:52:57
--------------------------
378384
8269
Thật sự rất hay và rất bất ngờ. Cách dẫn dắt và tình huống của truyện dẫn người đọc đi từ ngạc nhiên này đến ngạc nhiên khác. Ba cô nữ sinh Xuyến, Thục và Cúc Hương vừa mang nét vui tươi hồn nhiên của tuổi trẻ vừa có chút "ranh mãnh" dễ thương. Cái kết không ai ngờ thực sự khiến mình nhớ mãi, mặc dù cái kết rất bình thường nhưng qua ngòi bút của nhà văn Nguyễn Nhật Ánh lại trở nên vô cùng đặc biệt, ban đầu đâu có đoán được mọi thứ lại xảy ra như vậy.
5
1123856
2016-02-05 11:48:26
--------------------------
374241
8269
Câu chuyện nói về bộ ba cô gái nữ sinh Xuyến, Thục, Cúc Hương tình cờ gặp được một người con trai tên là Gia nhưng không biết đó là người sắp sửa trở thành thầy của mình. Cái kết khó có thể lường trước được, nội dung nói về tuổi học trò. 
Đặc biệt bộ ba nữ sinh này được bác NNA viết hẳn ba tập truyện là: nữ sinh, bồ câu không đưa thư, buối chiều windows
P/S ba bộ truyện trên đã được làm thành phim rồi nên nếu các bạn nào lười đọc sách thì có thể coi phim nhá!! Phim tên là nữ sinh
5
307619
2016-01-25 21:36:20
--------------------------
355960
8269
Đầu tiên phải kể đến dịch vụ của Tiki rất tốt, giao hàng nhanh, đóng hộp rất cẩn thận.
"Nữ Sinh" là một truyện dài rất vui của nhà văn Nguyễn Nhật Ánh đã được dựng thành phim "Áo Trắng Sân Trường". Truyện kể về ba cô nữ sinh học giỏi và hay chọc ghẹo cũng như phá phách người khác. Những cô gái tuổi học trò vô tư, tinh nghịch nhưng cũng chất chứa nhiều tình cảm. Tình yêu học trò có nhiều thứ để nhớ, vì sao chỉ những dòng văn bằng nét chữ mà sao Nguyễn Nhật Ánh lại khiến nó chân thực đến lạ! Mời các bạn cùng đọc để xem nạn nhân bị chọc ghẹo của ba cô nữ sinh này là ai. 

5
771830
2015-12-21 00:36:46
--------------------------
350017
8269
Cũng như những câu chuyện về tình cảm ngây ngô của tuổi học trò mà Nguyễn Nhật Ánh đã viết, Nữ sinh cũng là một câu chuyện thú vị, với cốt truyện đơn giản nhưng với lời văn chân thật, thơ ngây, hài hước đã khiến cho câu chuyện trở nên rất cuốn hút. Chắc cô gái nào cũng tìm được chút gì như mình qua ba nữ sinh tinh nghịch Xuyến, Thục, Cúc Hương. Văn Nguyễn Nhật Ánh lúc nào cũng vậy, chân thật như một cuốn nhật kí của mấy cô cậu học trò. Đọc chuyện mà cứ như đọc lưu bút một thời học sinh của chính mình. Ôi, nhớ quá!
3
53055
2015-12-09 18:17:51
--------------------------
346767
8269
Dễ thương quá những cô em gái, những tà áo dài trắng. Cuốn truyện gợi nhớ lại một thời đi học đã qua. Tôi cũng từng là một nữ sinh tinh nghịch, cũng quậy tưng bừng trong lớp học, đủ các trò phá phách với lũ con trai, con gái. Cũng bao lần làm thầy cô đau đầu. Sao mà bao nhiêu kỉ niệm cũ cứ tràn về trong tôi. Cảm giác như mình đang sống trong những năm tháng học trò đó. Nhớ thầy cô giáo quá. Nhớ những lần bị chép phạt tội không thuộc bài. Bị phạt đi tưới cây vì nghịch ngợm trong giờ học, nhiều quá không kể hết được. Mình thích cuốn truyện này quá!!!!
5
754539
2015-12-03 11:48:22
--------------------------
340762
8269
Lúc đầu đọc mình tức thay cho anh Gia khi bị ba cô học trò Cúc Hương, Thục, Xuyến trêu chọc nhưng càng đọc càng thấy các cô đáng yêu, tinh nghịch và cả táo bạo. Truyện gây ấn tượng bởi tạo sự tò mò với sự xuất hiện kì bí của Gia nhưng cởi nút hợp lý khi cả ba cô biết anh Gia là thày giáo chủ nhiệm mới của mình, đặc biệt cô nữ sinh dịu dàng tên Thục lại thầm mến thày. Thời áo trắng bây giờ có được hồn nhiên, trong sáng, chân thành đến thế! Thày cô bây giờ cũng cần lắm sự giản dị, gần gũi, vị tha và bao dung như thời ấy!
5
97460
2015-11-20 14:38:07
--------------------------
329534
8269
Nếu có một ngày bạn thực sự muốn trở về với tuổi thơ, với những dòng ký ức ngọt ngào thật đẹp của tuổi học trò ngây ngô, hồn nhiên thì hãy đến với... "Nữ sinh". Tác phẩm sẽ là con thuyền đưa bạn quay về những năm tháng cấp 3 đẹp nhất trong đời... Nơi chứa đầy tiếng cười, không âu lo, tính toán...nơi một tình bạn chân thành luôn tồn tại. Xuyến, Thục, Hương...cùng những trò đùa nghịch ngợm chỉ có ở thời áo trăng_cấp 3, những màn đấu khẩu với nhau....làm ta quên đi bao mệt mỏi nơi cuộc sống bộn bề âu lo....để mãi đắm chìm trong thế giới đó. Quả thật, bác Ánh chính là linh hồn của tác phẩm thiếu nhi...bạn thấu hiểu cả tâm trạng của tuổi mới lớn. Có lẽ chính vì vậy, truyện như một cuộc sống hiện thực, đọc mà thấy chân thật vô cùng như có thể trải nghiệm cùng nhân vật hay gợi nhắc về những kỉ niệm ta đã quên lãng từ lâu...Ta còn thấy trong đó là những tình cảm lặng lẽ, thầm mến nhau_chỉ biết ngại ngùng bẽn lẽn trước mặt người mình thích...ấp a ấp úng không thốt lên lời...hay những mảnh đời, những hoàn cảnh trái ngược khi sinh ra trong một gia đình tan vỡ, làm ảnh hưởng đến suy nghĩ của những bạn trẻ mới chập chững vào đời...và chính lúc đó tình bạn, những người bạn tốt thực sự, thầy cô giáo sẽ kéo bạn không sa vào cám dỗ là điều cần nhất. Đôi khi sẽ là những tiếng cười vui vẻ vì những trò đùa tinh quái nhưng đến cuối cùng khép lại quyển sách...lại làm ta lắng đọng...nhìn bước đi của các bạn trẻ ấy...bước đi hướng về tương lai....!
3
528960
2015-10-31 23:13:04
--------------------------
320552
8269
Nguyễn Nhật Ánh chính là người nắm giữ tuổi thơ của rất nhiều bạn trẻ! Tôi luôn nhớ đến văn của ông như một cách nhớ về bản thân mình ngày xưa, những hồi ức học trò trong năm tháng xưa cũ thỉnh thoảng lại bị phủ một lớp bụi mờ! Câu chuyện Nữ sinh là một phần "thỉnh thoảng" lại như một làn gió thoảng nhẹ thổi bay lớp bụi ấy và khiến tôi sa vào hoài niệm! Những câu chuyện dễ thương và dịu dàng, tinh nghịch của lớp nữ sinh ngày xưa bây giờ không dễ gì tìm thấy nữa, cách dùng từ đã trở thành hiếm hoi trong thời đại này... Tình yêu học trò có nhiều thứ để nhớ, vì sao chỉ những dòng văn bằng nét chữ mà sao Nguyễn Nhật Ánh lại khiến nó chân thực đến lạ! 
5
547523
2015-10-11 17:34:32
--------------------------
317719
8269
Nữ sinh á! Câu chuyện của nữ sinh thì đương nhiên là rất dễ thương rồi, bên cạnh sự dịu dàng của Thục thì cũng có không ít cô nữ sinh tinh nghịch bướng bỉnh như Cúc Hương và Xuyến. Mỗi người một vẻ góp phần cho câu chuyện đã thú vị lại càng thú vị hơn. Thêm vào đó là tình yêu của tuổi học trò dễ thương nhưng chân thật, giúp cho bạn đọc cảm thấy như tác giả đưa mình vào câu chuyện vậy. Nhờ giọng văn hóm hỉnh, từ ngữ chân thật thật đến từng chi tiết giúp cho người đọc hiểu rõ hơn về nhân vật và thích thú từ trang này đến trang sau. Truyện đã được chuyển thể thành phim. Có một cuốn của chú Ánh là phần tiếp theo của truyện này tên là Buổi chiều windows. Các bạn hãy tự cảm nhận nhé !
4
717149
2015-10-04 07:01:55
--------------------------
299688
8269
Mình đọc truyện của Nguyễn Nhật Ánh từ hồi nhỏ rồi nên giờ tái bản là mình cất công sưu tập lại hết bộ, vì phải nói là cả bộ này cuốn nào cũng hay, cũng dễ thương và gần gũi. 
Cuộc gặp mặt bất ngờ giữa 3 cô gái tinh quái và anh chàng thư sinh bí ẩn mở đầu cho cả loạt những tình huống dở khóc dở cười phía sau, mà chắc hẳn ai đọc cũng phải bật cười khúc khích vì độ láu cá của những Xuyến, Cúc Hương, cũng bồi hồi với  tình cảm của một cô gái hiền lành nết na như Thục. 

4
385695
2015-09-13 14:25:20
--------------------------
298897
8269
Lại được viết nhận xét về sách của bác Nguyễn Nhật Ánh. Cuốn Nữ sinh này mình đọc cảm thấy rất đáng yêu, là những câu chuyện rất thực mà cũng rất hài hước của những nữ sinh tuổi mới lớn.Nhưng có lẽ văn phong của tác giả mình đã khá quen, nên có thể đoán trước được tình tiết, đôi chỗ hơi bị chán. Tuy nhiên, tác phẩm nào của bác Ánh cũng vậy, không những mang đến sự giải trí mà còn có cả những bài học cho độc giả, đặc biệt là ở lứa tuổi mới lớn, đang sống trong vòng tay của thầy cô và bạn bè như chúng ta. Có thể nói là một truyện đáng đọc.
3
593339
2015-09-12 22:08:43
--------------------------
284273
8269
Đây không nằm trong top những tác phẩm xuất sắc nhất của Nguyễn Nhật Ánh, nhưng đọc giải trí cũng tạm ổn. Dù nội dung không có gì đặc biệt, và đôi lúc thấy mấy trò nghịch ngợm của Xuyến và Cúc Hương có hơi đi quá đà, nhưng nhìn chung thì truyện khá nhẹ nhàng, vui vẻ, thể hiện được chất "quái" của các cô cậu học trò. Dù gnay từ đầu truyện đã biết tình cảm của Thục dành cho thầy Gia sẽ không thành, nhưng đọc đến kết, tôi vẫn không khỏi thấy tiếc nuối cho mối tình đơn phương của cô.
4
57459
2015-08-30 20:21:55
--------------------------
282976
8269
Tôi từng vô cùng thích thú với các tập truyện về ba cô nữ sinh Xuyến, Thục và Cúc Hương của nhà văn Nguyễn Nhật Ánh, nhưng có lẽ đọc quen một số tập trước đó rồi nên không mấy ấn tượng với cuốn "Nữ sinh" này, cảm giác lặp lại và hơi nhàm. Ngay những chương đầu tôi đã đoán ra Gia chính là thầy giáo, ngoài ra những phân đoạn trêu chọc của ba nữ sinh đọc cũng ổn bởi có nhiều câu thoại đắt. Nói chung tác phẩm không xuất sắc nhưng để đọc giải trí, nhẹ nhàng cũng được.
3
6502
2015-08-29 16:31:55
--------------------------
274811
8269
Nhìn qua bìa sách cũng như nhan đề của tác phẩm phần nào đã đoán ra được phần nào nội dung của tác giả nhắm đến. Thực sự mình rất thích truyện của bác Nguyễn Nhật Ánh . Bởi trong truyện của bác hồn nhiên có, tinh nghịch có hơn nữa lại hết sức gần gũi, thân thuộc. Câu chuyện xoay quanh các trò nghịch ngợm của Xuyến, Thục và Hương đối với Gia. Câu chuyện như cho ta sống lại một phần kí ức đẹp của tuổi học trò. Thật sự bác Ánh có rất nhiều tác phẩm liên quan đến tuổi học trò nhưng không truyện nào bị trùng lặp thế nên không thể phủ nhận sức sáng tạo dồi dào, sự chỉn chu trong các tác phẩm của bác. Mình chính là yêu những điều đó và cả những mẩu chuyện của bác cũng như nữ sinh vậy.
4
368588
2015-08-22 11:44:40
--------------------------
264480
8269
Mình đã đọc nhiều câu truyện của nhà văn Nguyễn Nhật Ánh, với lối viết văn vui tươi, nhí nhảnh và đáng yêu khiến cho bạn đọc luôn bị hút bởi những cuốn sách nhà văn viết. Lối viết văn gần gũi của bác khiến mình rất thích, mình sẽ đóc đọc nhiều cuốn sách của nhà văn nữa ! Mình đã đọc xong trong 3 ngày đó . Cuốn sách này gần gũi với lứa tuổi học trò của mình, sự tinh nghịch và dí dỏm của ba cô gái ( Thục, Xuyến, Cúc Hương ) Cảm ơn tiki đã mang đến một sản phẩm vừa túi tiền của mình ! Mình sẽ ủng hộ tiki nhiều ! 
4
583708
2015-08-13 10:44:58
--------------------------
259087
8269
Mình đã đọc rất nhiều sách của bác Nguyễn Nhật Ánh. Và luôn như vậy, sách bác viết thật gần gũi và tinh tế. "Nữ sinh" kể về những trò nghịch ngợm của bộ ba Xuyến, Thục, Hương với Gia, người mà sau này lại chính là thầy giáo của họ. Truyện đã được chuyển thể thành phim nhưng dù phim có hay thế nào thì sách vẫn hơn,vì khi đọc sách thì mới cảm nhận được cái hay của cả cuốn sách. Theo mình thì đây là một cuốn sách khá hay và chính xác với lứa tuổi học trò
4
602741
2015-08-09 08:21:38
--------------------------
252575
8269
Mình đã đọc rất nhiều sách của bác Nguyễn Nhật Ánh. Và luôn như vậy, sách bác viết thật gần gũi và tinh tế. "Nữ sinh" kể về những trò nghịch ngợm của bộ ba Xuyến, Thục, Hương với Gia, người mà sau này lại chính là thầy giáo của họ. Thật không thể ngờ được. Đây lại là một trong những tác phẩm hay của bác. Truyện cũng ngắn nên đọc xong cứ thấy tiếc tiếc. Dù sao thì mình vẫn rất thích và sẽ luôn ủng hộ truyện của bác Ánh và cả những truyện khác của Tiki nữa.
5
489580
2015-08-03 19:44:45
--------------------------
242404
8269
So với "Bồ câu không đưa thư" thì "Nữ sinh" không gây cười nhiều. "Bồ câu không đưa thư" khiến người xem phải cười từ đầu đến cuối, song 1 chi tiết buồn làm điểm nhấn ở kết thúc truyện. "Nữ sinh" thì không như vậy. "Nữ sinh" tái hiện chân thật những mộc mạc, hồn nhiên của tuổi học trò giai đoạn thanh thiếu niên cấp 2, cấp 3 ngày xưa, ngày nay nét mộc mạc, hồn nhiên đó rất hiếm.

 Xuyến, Thục, Cúc Hương, Hùng quăn, Sơn sún, những nhân vật này đại diện cho 1 thế hệ học sinh hồn nhiên, tinh nghịch nhưng cũng đôi lúc làm người lớn phiền lòng. Truyện hay, nhưng đọc khá nhanh, mặc dù nhiều tình tiết đan xen vào nhau.
4
622443
2015-07-26 16:41:47
--------------------------
240459
8269
Nguyễn Nhật Ánh là một trong ba tác giả mình thần tượng, cho nên những quyển của chú mình có và đọc rất nhiều. Riêng quyển Nữ sinh này không biết là lần thứ bao nhiêu mình đọc lại, chỉ biết rằng cảm xúc vẫn vậy - hồn nhiên, vui vẻ và trong veo. Khác chăng chỉ là những nhớ nhung, luyến tiếc giờ đây lớn hơn 1 chút bởi lẽ bản thân dường như đã lớn đủ để hiểu câu chuyện của riêng mình sẽ mãi là 1 dấu chấm lửng đầy mơ hồ. Nhưng dù sao thì vẫn rất đẹp trong kí ức của Thục và cả của mình nữa.
4
173799
2015-07-24 16:52:18
--------------------------
238701
8269
Mình đọc buổi chiều Window của nhà văn Nguyễn Nhật Ánh trước rồi mới đọc cuốn Nữ sinh này, mặc dù như thế là bị ngược thời gian. Tuy nhiên mình thích cuốn Buổi chiều Window hơn, đọc mắc cười, dí dỏm và cực kì thú vị. Cuốn nữ sinh này nhẹ nhàng thời đi học quá, không toát lên được nét tinh nghịch của ba cô gái Thục, xuyến và cúc hương. Tuy nhiên đã là tác phẩm của Nguyễn nhật ánh thì các bạn không nên bỏ qua. Đọc mà muốn cười và thương cho anh chàng Gia luôn ấy chứ,à không thầy Gia chứ ^^, muôn màu tình cảm về bạn bè và tuổi học trò, nhí nhố nữa.
4
393545
2015-07-23 13:15:48
--------------------------
201423
8269
Truyện mang màu sắc nhẹ nhàng, giản dị, hồn nhiên của tuổi học trò nhưng vẫn để lại rất nhiều ấn tượng trong lòng người đọc. Truyện có nhiều chi tiết khá hài hước về ba cô bạn Xuyến, Thục, Cúc Hương và anh chàng Hùng quăn. Bất ngờ được mở ra khi Gia chính là giáo viên chủ nhiệm mới của lớp. Thầy Gia là một người tốt bụng, dễ thương, quan tâm đến học sinh và có lòng vị tha. Tình yêu tuổi học trò đáng yêu, hồn nhiên cùng tình thầy trò được khắc họa đậm nét trong truyện này. Nguyễn Nhật Ánh là một nhà văn tài hoa với những câu chuyện về tuổi học trò.
5
641606
2015-05-27 12:36:22
--------------------------
195226
8269
Không gian của câu chuyện chỉ đơn giản là một quán chè nhỏ trước cổng trường, tưởng chừng khiến ta nhàm chán nhưng điều này lại đem đến những bất ngờ thú vị.
Câu chuyện xoay quanh bộ ba quen thuộc của chúng ta: Xuyến, Thục, Cúc Hương, khi gặp anh chàng Gia và ra sức "hành hạ" anh ta mà sau này khi biết một sự thật về anh chàng lại khiến ba cô gái ngỡ ngàng.
Lời văn ngắn gọn, mạch lạc. Bìa sách đẹp và cứng. Giấy vừa đủ độ cứng và chỉ tiết là màu hơi ngà không được trắng cho lắm.
4
621744
2015-05-13 04:50:49
--------------------------
194226
8269
Nội dung không đặc sắc như những cuốn sách khác của Nguyễn Nhật Ánh, mình thấy không có nhiều tình tiết bất ngờ có lẽ vì ở phần giới thiệu sách tiki đã nêu toàn bộ tình tiết chính.
  Mình không thích cách nói chuyện, hành xử lúc đầu của Xuyến và Cúc Hương dù để xây dựng tính cách nghịch ngợm và bạo dạn nhưng có phần hơi quá đáng, nhất là Xuyến. Nhờ nhà trường thay đổi giáo viên chủ nhiệm nên vần đề của Hùng quăn được giải quyết dễ dàng hơn, anh có cơ hội để học tốt như trước và hòa nhập với bộ ba Xuyến, Thục, Cúc Hương sau mọi chuyện. Ngoài ra câu chuyện còn đề cập đến tình cảm của Thục dành cho thầy Gia khiến thầy cũng bối rối khi nhận ra điều đó. Cách kể chuyện mạch lạc, tình tiết đều đặn nhưng mang ý nghĩa nhân văn cao.
3
344536
2015-05-09 20:48:58
--------------------------
183921
8269
Truyện "Nữ sinh" là một truyện rất khiến tôi lạ lẫm và thích thú bởi khung cảnh từ đầu truyện tới gần cuối truyện chỉ xoay quanh quán chè trước cổng trường nhưng không vì thế mà nó nhàm chán mà ngược lại truyện lại vô cùng hấp dẫn bởi những lời trò chuyện dí dỏm của, Thục, Xuyến và Cúc Hương bởi những trò đùa tinh nghịch đôi lần làm điêu đứng anh chàng GIa của chúng ta nhưng có lẽ không gì bất ngờ hơn là phần kết của truyện bởi người mà 3 cô nàng của chúng ta hành hạ trong một thời gian dài lại là thầy chủ nhiệm mới của mình, đọc truyện tôi lại nhớ tới một lời bình dành cho Truyện của Nguyễn Nhật Ánh là bất cứ nơi đâu trong truyện của ông đều trở thành xứ sở thần tiên, xứ sở thần tiên hồn nhiên nhí nhảnh của một thời tuổi trẻ.
4
602070
2015-04-16 20:55:34
--------------------------
180488
8269
Nữ sinh của Nguyễn Nhật Ánh là cuốn duy nhất trong bộ ba cuốn về ba cô gái Xuyến, Thục, Cúc Hương mà đất diễn cho từng nhân vật khá là đều nhau. Nhân vật thầy Gia trong truyện này cũng rất là đáng yêu. Sau này trong cuốn Buổi chiều Windows cũng có được nhắc lại. Thầy Gia mang hình ảnh của một nhà giáo mực thước nhưng cũng khá là hài hước và tâm lý. Thầy còn rất trẻ mà tầm nhìn và đức độ thật khiến người khác phải ngưỡng mộ, hết lòng vì học sinh, và luôn bao dung tha thứ cho lỗi lầm của tuổi mới lớn. Cái cách mà thầy quan tâm học sinh cá biệt Hùng quăn đã nói lên rất nhiều về con người thầy. Ở Thầy có nhiều điểm mà các bậc Thầy Cô ở Việt nam hiện tại cần phải học hỏi. Đây có lẽ là điều mà bác Ánh muốn gửi gắm đến bạn đọc. Tôi nghĩ thế!
4
329218
2015-04-09 15:58:50
--------------------------
174316
8269
Mình đã đọc rất nhiều truyện của Nguyễn Nhật Ánh, trong mỗi cuốn truyện đều mang đến một dư vị nhất định trong lòng người đọc. Nữ sinh cũng là một cuốn truyện mà mình vô cùng yêu thích. Phong cách hồn nhiên, trong sáng, vô cùng phù hợp với lứa tuổi mới lớn.Tính cách những nhân vật chính phóng khoáng, đậm chất học trò. Ba cô nàng Xuyến, Thục, Cúc Hương, với những trò nghịch không kém con trai - đã góp phần không nhỏ làm nên thành công của tác phẩm này. Vẫn với tình tiết hài hước, "chàng trai" mà ba cô gái "bắt nạt" trước cổng trường không ngờ lại là thầy chủ nhiệm của họ - điều này làm nên sự thích thú nơi độc giả.
5
504945
2015-03-27 19:57:20
--------------------------
167601
8269
Đó là những nhận xét của tôi cho ba nhân vật Xuyến Thục và Cúc Hương. Truyện về tuổi học trò luôn là đề tài thu hút tôi bởi lẽ ngòi bút của tác giả quá tinh tế, quá thấu hiểu nên mới có thể viết nên những câu chuyện có trong đời thực như vậy. Tôi đặc biệt yêu mến Thục bởi cô bé nhẹ nhàng sâu lắng quá, nhưng cũng yêu Xuyến-cô lớp trưởng tinh nghịch bày đủ trò làm cho thầy giáo Gia phải lúng túng khi đối diện. Truyện đan xen vào tình cảm học trò làm tôi thêm nhớ thêm yêu tuổi thơ mình quá đỗi. Cái kết không biết nên buồn hay vui nhưng tôi thích nó bởi tôi có thể tự viết ra cho mình một cái kết bản thân.Nữ sinh là cuốn truyện mà đọc nó mỗi người trong chúng ta sẽ quay về tuổi thơ học đường của mình.
4
289148
2015-03-15 07:17:52
--------------------------
167238
8269
Bằng tài năng viết lách của mình, Nguyễn Nhật Ánh đã khiến tôi thật sự tò mò về thầy giáo bí ẩn ngồi ngoài cánh cổng trường kia. Rồi sau đó tôi lại bị thuyết phục bởi sự linh hoạt của thầy khi giải quyết vấn đề với bạn trai tin đồn của cô nữ sinh. Một câu chuyện thật thú vị giữa tình yêu và cũng là tình thầy trò giữa Thục và thầy giáo. Cái kết mở khiến nhiều người vẫn còn băn khoăn: Chuyện tình của họ sẽ đi đến đâu. Lôi cuốn và hấp dẫn, còn gì khiến bạn chần chừ khi mua quyển sách tuyệt vời này?
5
581420
2015-03-14 12:34:41
--------------------------
145599
8269
Mình thấy truyện dài lần này hay nhưng cũng vừa phải, không đau tim bằng Mắt Biếc hoặc 'Còn chút gì để nhớ' nhưng Nữ sinh vẫn có chỗ riêng.

Câu chuyện miêu tả chủ yếu xoay quanh 3 cô gái Xuyến, Thục, Hương, vẫn là vấn đề học đường, tình cảm tuổi mới lớn, và lần này có thêm thầy giáo Gia - người từng bị 3 cô quậy phá đủ thứ - làm thầy giáo chủ nhiệm (mình cứ liên tưởng tới Đan Trường.)  Lần này lấy nhân vật nữ làm chủ đạo, tác giả là nam mà vẫn miêu tả từng suy nghĩ, hành động của 3 cô rất khéo léo. Tính các từng người thì ông để người đọc tự cảm nhận, chỉ nhấn nhá qua câu chữ.
 
Tình cảm học trò của chuyện hết sức đẹp, trong sáng, ghen tuông cũng có nhưng rất vô tư, chóng giận thì chóng quên. Nhân vật Hùng vì tuổi nhỏ, hành sự không thấu đáo, thiếu chín chắn là rắc rối lớn nhất của chuyện, nhưng cuối cùng cũng được cảm hóa bởi lòng bao dung của thầy Gia. Đoạn này là mình thấy hay nhất của quyển sách, chắc những người đọc khác cũng không khỏi hài lòng. Tóm lại, dù không quá đặc sắc nhưng Nữ Sinh vẫn là một tác phẩm hay.
4
418158
2014-12-31 08:13:26
--------------------------
134049
8269
Nữ sinh là một cuốn sách khá hay về tuổi học trò đáng yêu của ba cô gái, Thục hiền dịu, Cúc Hương nhí nhảnh và Xuyến tinh nghịch. Câu chuyện đã viết về sự hồn nhiên của ba cô nữ sinh này qua những cử chỉ hành động và cả lời nói với nhau hay cả tính quậy phá khi tiếp xúc với anh chàng tên Gia lúc chưa biết anh chàng này là thầy chủ nhiệm. Có cả câu chuyện nhỏ giữa Cúc Hương và Hùng quăn như làm đẹp thêm cho câu chuyện "Nữ sinh". Đọc được cuốn sách này tôi mới thực sự hiểu tại sao mọi người lại nói Nguyễn Nhật Ánh là nhà văn của tuổi mới lớn. Ông viết rất hay và thật về lứa tuổi mới lớn. Thật hay "Nữ sinh" ơi!
4
443413
2014-11-07 23:08:27
--------------------------
132099
8269
Tính cách những nhân vật chính phóng khoáng, đậm chất học trò. Ba cô nàng Xuyến, Thục, Cúc Hương, với những trò nghịch không kém con trai - đã góp phần không nhỏ làm nên thành công của tác phẩm này. Vẫn với tình tiết hài hước, "chàng trai" mà ba cô gái "bắt nạt" trước cổng trường không ngờ lại là thầy chủ nhiệm của họ - điều này làm nên sự thích thú nơi độc giả. Ngoài ra, "lòng mến mộ" mà Hùng - một cậu học sinh cá biệt, dành cho Cúc Hương cũng như cách bày tỏ khá vui nhộn và "khác người" của cậu với cô nàng cá tính này càng khiến người đọc phải lần giở hết những trang sách để mau chóng tìm cho ra cái kết của "câu chuyện tình học trò" này.
Nói Nguyễn Nhật Ánh là nhà văn của tuổi mới lớn quả không sai!
4
220900
2014-10-29 19:16:32
--------------------------
122269
8269
những tác phẩm của tuổi thơ luôn theo chân tác giả Nguyễn Nhật Ánh trên con đường văn chương của ông. Tác phẩm này chứa đựng cả một ình cảm học trò nông nổi và tich nghịch. 3 cô gái Thục, Xuyến và Cúc Hương luôn mang lại những tiếng cười cho người đọc khi những lần học nói chuyện với nhau, những lần treo ghẹo Thục, một mô-típ truyện hồn nhiên về lứa tuổi học trò. Câu chuyện cũng phần nào cho mình thấy được lứa tuổi học sinh bây giờ còn khá ít những tình cảm hồn nhiên này.
2
338455
2014-08-24 10:14:51
--------------------------
119524
8269
Nguyễn Nhật Ánh có nhiều tác phẩm dành cho tuổi mới lớn, nhưng mỗi tác phẩm đều có sắc thái riêng. "Nữ sinh" cũng vậy, cũng có một cốt truyện rất riêng. Những cô gái tuổi học trò rất tinh nghịch nhưng cũng rất tình cảm, hồn nhiên và trong sáng. Thầy giáo Gia, một thầy giáo trẻ và tâm huyết được cử về làm giáo viên chủ nhiệm của những cô gái ấy như một sự sắp đặt, một sự sắp đặt hoàn toàn thỏa đáng để "giúp đỡ" cho sự hoàn thiện những tâm hồn ấy. Quả là một câu chuyện hay và đáng để đọc!
4
282264
2014-08-05 19:46:45
--------------------------
117805
8269
Những bộ truyện dài của nhà văn Nguyễn Nhật Ánh được đăng sớm nhất là trên những tờ báo Mực Tím, nhưng mình không biết đến chú Ánh cho đến khi những bộ truyện đó được xuất bản thành sách, những năm 2004 - 2005, thời mình còn là đứa học sinh tiểu học. Mình vẫn nhớ, cuốn đầu tiên mình nhịn ăn mua là cuốn Nữ Sinh với Xuyến, Thục và Cúc Hương. Mình mua cuốn Nữ Sinh đầu tiên là vì nó mỏng và rẻ nhất trong đợt xuất bản đó ^^
Câu chuyện về tình cảm tuổi mới lớn của các cô, cậu học trò đã được nhà văn Nguyễn Nhật Ánh kể lại rất sinh động và chân thật. Hồi đấy "Nữ Sinh" còn được dựng thành phim "Áo Trắng Sân Trường"
Đọc Nữ Sinh cũng thấy bản thân luôn có một cô Thục, 1 cô Cúc Hương, 1 cô xuyến tinh nghịch, phá phách nhưng đáng yêu như thế :D
4
82132
2014-07-23 06:26:58
--------------------------
