489051
3342
- Sách hay, tuy nhiên có nhiều bài khá khó so với tuổi các bé
- Hình thức đẹp, in màu, hình ảnh ngộ nghĩnh
- Giá cả phải chăng
- Tiki giao hàng nhanh, bao bookcare đẹp
5
85763
2016-11-06 11:02:56
--------------------------
340076
3342
Sách có tranh vẽ sinh động, bắt mắt. Các câu hỏi đưa ra tương đối khó để các bé phải tập trung, tư duy logic. 
Tuy nhiên, nhiều câu mình thấy quá khó. Ví dụ như tìm chìa khóa thất lạc trong một mớ dây lộn xộn, mình tìm còn không ra nữa đó. 
Nhưng đa số các câu hỏi là hợp lý, hơi khó hơn để bé phải suy nghĩ. Ba mẹ cũng cần ngồi hướng dẫn để bé có động lực, chứ nhiều khi thấy khó bé cũng nhanh chán. Độ tuổi phù hợp như sách đã ghi, nếu bé nhỏ hơn xem sẽ thấy khó nha.
4
715022
2015-11-19 10:00:07
--------------------------
