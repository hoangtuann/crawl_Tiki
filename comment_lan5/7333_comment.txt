123276
7333
Bị thu hút bởi tựa sách và cách chia đề mục nhỏ cho dễ theo dõi trong phần mục lục tôi đã chọn mua quyển sách này. Tôi khá bất ngờ khi bắt đầu tìm hiểu nội dung sách. Tác giả là "VietBook biên soạn" nhưng trong chương một sách lại để tác giả ở Bắc Kinh, thật sự không biết thế nào. Các dẫn chứng toàn là các nhân vật và các loại sách cổ trong lịch sử Trung Hoa. Thuộc thể loại sách kỹ năng nhưng dẫn chứng dài và chiếm hầu hết nội dung sách trong khi phần đúc kết kỹ năng lại viết sơ sài vài dòng. Nếu sách đặt tên như Thuật xử thế Trung Hoa hay tinh hoa xử thế... có lẽ sẽ rõ ràng hơn khi độc giả lựa chọn một quyển sách kỹ năng. Sách sẽ phù hợp hơn với những độc giả các kiến thức về lịch sử Trung Hoa. Nếu là VietBook biên soạn thì tôi mong có những quyển sách thuần Việt hơn
2
105749
2014-08-31 11:50:11
--------------------------
