491961
1010
Mình dùng OALD lần đầu tiên khi vào đại học, năm 2007; nên cũng có kinh nghiệm với OALD. Và mình vừa nhận được bản này 2 ngày trước.

Ở phần hỏi đáp ở trên có bạn hỏi có Anh - Anh không, quản trị viên trả lời là không, chỉ có Anh - Việt => cái này là hông đúng nha. Toàn bộ nội dung, màu sắc, cách trình bày, theo mình là của bản OALD7; phần dịch nghĩa tiếng Anh cũng được giữ nguyên hông bị bỏ đi, phần dịch nghĩa tiếng Việt theo sau phần tiếng Anh.

Mình phát hiện có 1 trang in bị nhoè - trang 1326. Không biết có bị cái nào khác nữa không. Nhưng tổng quan là chất lượng giấy và mực in cũng như màu rất tốt, gần như không khác gì cuốn OALD đơn ngữ.
5
1096165
2016-11-22 10:41:44
--------------------------
488905
1010
Mới đầu hơi phân vân vì cuốn này khá đắt nhưng mà đc giảm giá vì đúng hội chợ sách nên đã sống chết tậu em ý về. Mình đã tìm hiểu rất kĩ các loại từ điển vì mục đích mình mua để phục vụ học IELTS thế nên yêu cầu về từ rất nhiều. Nói vể điểm mạnh: từ điển Anh- Anh- Việt có lượng từ phong phú, dịch rất hay, nghĩa đa dạng ( khen hơi thừa vì của Oxford cơ mà) ngoài ra từ điển còn cung cấp thêm ipa ( phiên âm quốc tế của từ) cùng các từ đồng nghĩa, trái nghĩa, các dạng động danh tính trạng của từ. Điểm cộng thứ hai là trình bày khoa học dễ tra cứu, từ điển còn có phần hướng dẫn viết bài luận và phần tranh hình minh họa rất phong phú. Về điểm trừ là chữ hơi nhỏ ( nhưng vì số lượng từ lớn nên có thể chấp nhận đc). Điểm trừ thứ hai là trang sách mỏng thế nên lúc lập hơi ghê. Cuối cùng là do bìa mềm nên sách hơi bị õng ẹo vì nó dày quá. Chốt lại nếu bạn có nhu cầu học các loại chứng chỉ quốc tế hay đam mê tiếng anh thì đây là một lựa chọn tuyệt vời. Đây là sản phẩm chất lượng mà VN đã thực hiện khá tốt, rất đáng khen ngợi
5
286491
2016-11-05 13:12:33
--------------------------
478683
1010
Về nội dung: Dựa trên nguyên bản OALD 8 được cấp bản quyền từ đơn vị phát hành bản gốc thì có thể an tâm (tuy chưa có thời gian xem hết)
Về hình thức: nhìn chung là đẹp, giấy hơi mỏng, bìa cũng vậy nên dễ bị cong, nếu có bản bìa cứng nữa thì tuyệt hơn
Về giá cả: mình nhờ bạn đặt mua ở Tiki dịp 7 ngày vàng giá sốc, được giá tốt :)
Tóm gọn lại: đây là quyển từ điển tốt cho những ai cần học tiếng Anh, mình thì kết OALD từ bản 7 rồi.
5
1458761
2016-08-04 08:13:13
--------------------------
476992
1010
Mình ở Bình Dương, mới đặt mua 1 ngày mà đã có sách rồi. Sách vừa đẹp, bắt mắt, lại còn có phần phụ lục rất hay. Thêm váo đó, còn có cả hướng dẫn cách sử dụng các từ ngữ đồng nghĩa nhưng trong trường hợp khác nhau, dòng họ từ, thành ngữ và cụm động từ liên quan.... Một cuốn từ điển hoàn hảo dành cho những ai muốn học chuyên sâu về tiếng Anh. Nhờ cuốn từ điển này mà giờ mình không phải dò qua dò lại giữa từ điển anh - anh và anh - việt nữa rồi ^^ Rất thích sách Tiki
5
1506370
2016-07-24 06:05:00
--------------------------
