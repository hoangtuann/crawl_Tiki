416887
5546
Tiểu thuyết rất hay, là thể loại huyền bí, giả tưởng mà bản thân tôi rất ưa thích. Là 1 con người mê khám phá phiêu lưu như tôi thì bạn cũng sẽ bị hấp dẫn, cuốn hút bởi trăng đêm. 1 cuộc chiến giữa người và yêu ma vương tộc mà linh hồn chàng được đầu thai trong hình dạng nữ giới nắm giữ vận mệnh thế giới, U Minh nổ ra vừa căng thẳng vừa sinh động làm bản thân tôi cũng không ngừng theo dõi diễn biến câu chuyện.
Tuy nhiên, có 1 vài chỗ ít từ ngữ trong sách bị in sai lỗi chính tả. Điều đó làm tôi và các bạn lúc cùng nhau đọc cảm thấy kg phiền mà hài hài. Hihi, mong những năm sau nhà xuất bản sẽ kiểm tra kỹ càng hơn
3
1127737
2016-04-15 21:22:29
--------------------------
377116
5546
Tôi đã có kỳ vọng lớn vào câu chuyện này vì tôi tưởng nó sẽ mới mẻ và thú vị như Liên hoa yêu cốt. Hóa ra chẳng phải vậy. Nội dung khá thú vị nhưng không được khai thác đúng cách. Thật tiếc cho một cốt truyện hay. Nếu viết tốt, nó đã có thể trở thành một tuyệt phẩm. Điểm trừ lớn nhất là quá đông nhân vật. U Minh sứ có 6 người, bên ngự linh sư thêm mấy người, hội Siêu nhiên, ma, quỷ, thần.... Nhiều lúc tôi loạn hết cả lên, còn phải nghĩ và suy đoán vai trò của người này trong cuộc sống người kia nữa. Tôi thích dàn nam, Ảo Nguyệt, Dạ Ly, Hú hay Doãn Kiếm. Nhưng tôi cực kỳ không thích dàn nữ ngoài Toàn Cơ và Diệp Âm. Doãn Điệp hay Điền Giai Dĩnh khá 'bánh bèo'. Cộng thên tính dịu dàng dây dưa với nữ nhân của Dạ Lạc làm tôi khó chịu. Iris, Lương Dĩ Tiên... thì  không mấy ấn tượng. Tôi thấy thà tác giả viết thành đam mỹ còn hơn. Tập 1 này cũng có khá nhiều sạn trong cách xưng hô hay chính tả. Hy vọng tập 2 sẽ khá hơn.
2
926764
2016-02-01 17:51:12
--------------------------
375509
5546
Tập đầu tiên hơi rắc rối bởi tình tiết Dạ Lạc chuyển linh hồn mình vào thân xác của nữ chính nhưng khi phải sống trong một con người mới thì cô gái với người nam nhân vật có lúc cũng có cảm tình với người đó , diễn biến tình cảm phức tạp nên chưa nhìn nhận rõ ràng lắm , chưa nhận ra được ý nghĩa chính của tiểu thuyết nằm ở mối quan hệ nào , cũng có khi gặp phải những tình huống dở khóc dở cười như là phải đón nhận khó xử của người như tiểu trinh .
4
402468
2016-01-28 15:27:23
--------------------------
146938
5546
Về nội dung tác giả đã xây dựng một cốt truyện rất hấp dẫn và trên cả tuyệt vời .Nói thiệt đọc truyện này thử thách khả năng suy luận của độc giả dữ lắm nhưng nó lại gây cho mình hứng thú để liên kết các chi tiết lại với nhau . Giong điệu của tác giả không nhanh không chậm đủ gây cho mình sự tò mò muốn lật sang trang sau đọc tiếp . Cơ bản về nội dung thì nó rất thành công . Điều mình không hài lòng đó là về chất lượng sản phẩm phải nói là rất tệ . Là một người yêu sách không ai có thể chấp nhận một cuốn sách chất lượng giấy in mỏng lét , in thì sai sót nhiều như vậy . Nếu có sai thì sai một vài chỗ thôi chứ đằng này hầu như trang nào cũng sai khiến mình thất vọng ghê gớm .
4
337423
2015-01-06 08:35:27
--------------------------
137855
5546
Tôi dự định xem hết toàn tập Trăng Đêm rồi mới viết cảm nhận nhưng thấy bức xúc quá thành ra viết luôn.
Nói tóm lại tôi chưa coi một cuốn truyện nào mà máu nóng dồn lên não, càng xem càng có ham muốn vô cùng thiết tha là knockout ai hiệu đính quyển sách này. Thử hỏi làm ăn cái kiểu gì hầu như trang nào của quyển sách cũng có lỗi, có khi 1 trang sách lỗi 3 đến 4 chỗ khiến tôi đọc mà thấy cực kỳ khó chịu. 
Một quyển truyện với nội dung hay, ý tưởng độc đáo mà chỉ vì cái khuyết điểm ở khâu edit mà khiến quyển sách giảm giá trị. Một quyển sách lỗi tè le hột me như thế mà bán với giá bìa 129k là không đáng. Làm ăn không có tâm. 
Bên trên là những bức xúc của tôi chủ yếu do lỗi hiệu đính còn về nội dung truyện thì không có gì để phàn nàn.
Đây là một quyển sách hay muốn kinh dị có kinh dị, muốn tình cảm có tình cảm, yếu tố ma mị giăng đầy cả quyển sách, từ Thần, Ma, Yêu, Nhân…đủ cả, tây tàu hỗn tạp: cương thi, tử thần, thi quỷ, thông linh, ngự linh, người có đặc dị công năng, âm dương sư phối hợp nhuần nhuyễn.
Và cái mà tôi tâm đắc nhất cho tới giai đoạn này là tôi không đoán được tình cảm cảm của Dạ Lạc (Phong Linh). Ở những bộ tiểu thuyết khác khi đàn ông xuyên vào đàn bà nếu là ngôn tình thì cỡ nào cuối cùng cái thằng vốn là đàn ông ban đầu đó sẽ bị thuần hóa chấp nhận làm đàn bà và sẽ yêu nam chính. Nhưng ở Trăng Đêm thì bó tay, hết cuốn 1 rồi vẫn không đoán được. Phong Linh xác là nữ nhưng linh hồn là Nam, có thể biến được trở về nguyên hình trước đây là Dạ Lạc (nam). Nói chung, thân xác muốn nam thì nam muốn nữ thì nữ, như vầy là đã khó đoán rồi, lại thêm lúc là Phong Linh (nữ) có tình cảm mập mờ với Doãn Kiếm (nam), khi là Dạ Lạc thì lại dây dưa với Giai Dĩnh (nữ), đối với nam hay nữ gì thì Tà Thần của chúng ta cũng có cảm xúc, bó tay, hỏng đoán được. Đối với Doãn Kiếm và Giai Dĩnh là đã mệt rồi, làm tôi suy không ra, vậy mà các nhân vật phụ đi cùng càng khiến tôi rối thêm nữa, nào là Ảo Nguyệt (nam), Toàn Cơ (nữ), Doãn Điệp (nữ), Hồng Thiếu Gia (nam), Nhan Kỳ (nam)…cũng có tình cảm với Tà Thần. Rối quá đi. Mà càng rối càng khó đoán thì lại càng hay.
Ngoài nội dung, ý tưởng, tình tiết…hay, hấp dẫn, lôi cuốn thì truyện cũng có điểm chưa hoàn hảo. Đó là, có qúa nhiều nhân vật nên dẫn đến khai thác về họ chưa sâu chưa kỹ có khi chỉ là lướt qua, làm tôi có cảm giác như tác giả đào hố rồi để đó không giải quyết. Nếu chăm chút hơn cho các nhân vật thì câu truyện sẽ hoàn mỹ hơn.
Và còn một điều nữa, có nhiều chỗ tác giả viết rất lắp lửng, tối nghĩa, mà những cái chỗ lắp lửng tối nghĩa này chỉ ở những chi tiết nhỏ, đôi khi không quan trọng nên thành ra ai đọc kỹ và nghiễn ngẫm thì mới biết chứ nếu đọc kỹ mà không nghiễn ngẫm thành ra sẽ bỏ qua mà nếu bỏ qua đảm bảo sẽ có người trách lầm tác giả viết có sạn nhưng thực chất là không sạn. Theo tôi tác giả viết như vậy sẽ gây tò mò nghi vấn nhưng với điều kiện là tình tiết sự kiện lớn người đọc mới nhớ còn mấy chi tiết nhỏ vụn vặt mà bỏ lổ ở khúc đầu rồi khúc giữa không đá động khúc chót mới hé lộ mà lại hé lộ theo kiểu sâu sắc chỉ vài câu thoại, mà lại thoại không đầu không cuối thì ai mà nhớ tác giả ơi!!! (các bạn đọc quyển 1 này có nhớ cái câu Ảo Nguyệt nói với Dạ Lạc khi Dạ Lạc đeo sợi dây chuyền có hột đá xanh da trời cho Tiểu Trinh không?: Đai ý của câu nói đó là có tin được Tiểu Trinh không. Dạ Lạc mới Trả lời một câu không ăn nhập, đại ý là ba đã chết, mẹ cũng sắp chết, Tiểu Trinh đau khổ gì đó, giúp đỡ được cho cô ấy tới lúc mẹ Tiểu Trinh chết cô ta sẽ không đau khổ quá nhiều. Tóm lại là sợ dây truyền đeo lên cổ Tiểu Trinh là cố ý tặng cho Doãn Kiếm. Sau đó bỏ qua vấn đề này tới cuối truyện Doãn Kiếm ăn cơm với Tiểu Trinh nói Blab Blab gì đó, chốt lại là Tiểu Trinh làm nhiệm vụ điều tra Phong Linh cho Doãn Kiếm lấy tiền trị bệnh cho mẹ. Từ đó suy ra, khúc đầu câu nói của Ảo Nguyệt với Dạ Lạc về Tiểu Trinh và sự giúp đỡ của Dạ Lạc đó chính là Dạ Lạc sẽ tiết lộ cho Tiểu Trinh 1 chút thân thế của mình để Tiểu Trinh bán cho Doãn Kiếm. Hix. Đọc ngôn tình mà như trinh thám vậy.)
4
180185
2014-11-29 00:06:45
--------------------------
136547
5546
Mình đã mất gần 3 tiếng để ngồi cày cuốn này, và nhiều lúc nó khiến mình phải làm bộ mặt -__-
- Về hình thức: Truyện được in trên giấy tráng ngà, là loại giấy mình rất thích vì nó không bị ố vàng. Nhưng điều đáng buồn là nó không có bọc, thế nên rất dễ bẩn giấy. Trang trí bìa phía sau, chữ khó nhìn, bé bé, nét lại mảnh =.=
Cuốn mình nhận được thì bị lỗi in mới ghét :(((((((((

- Về nội dung: Thật khó để chọn cho nó một thể loại riêng (mình không biết đây là ngôn tính, trinh thám, hay là kiếm hiệp nữa) Nội dung cũng không thể coi là mới. Nhân vật chính là nam, chuyển thế trong thân xác nữ giới. Rất rất rất đẹp trai, đối xử dịu dàng với con gái. Và mình không thích tính cách này chút nào. Chỉ vì đối với nữ giới không dứt khoát nên mới sinh ra đống hiểu nhầm (dù biết phải như thế mới thành truyện). 
Các nhân vật phụ thì có cậu em Dạ Ly rất đáng yêu, luôn tìm cách bảo vệ anh trai, 6 U Minh sứ giả mỗi người một tính cách khá riêng biệt. Những nhân vật khác cũng được, chỉ là mình không ưa nổi tính cách của nhiều nhân vật nữ trong truyện (ngây ngô tới ngu ngốc, ngờ nghệch đến khờ dại)

Tác giả đã lồng ghép quá khứ của nhân vật chính vào từng đoạn, chứ không kể lại thành một chương hoàn chỉnh. Đây cũng có thể coi là sáng tạo vì mình chưa đọc truyện nào có cách kể như này. Mong là sang tập 2 không khiến mình thất vọng
4
259894
2014-11-21 21:36:57
--------------------------
