463483
4980
Không kịch tính, không gay cấn, hồi hộp, nhưng “Chiếc lá cuối cùng” lại luôn để lại trong lòng người đọc những xúc cảm khó phai mờ. Với chất văn nhẹ nhàng, từng câu chữ thấm đẫm tình yêu thương con người, truyện ngắn đã “mang trong nó chức năng sinh thành và tái tạo”. Câu chuyện đã mang lại cho em những bài học rất đáng quý trong cuộc sống. Mỗi người phải biết tin tưởng vào cuộc sống, hãy trân trọng cuộc sống bằng cách sống thật lạc quan, cống hiến cho đất nước. Mỗi người chỉ được sống một lần. hãy sống sao cho sau này, bạn sẽ không bao giờ phải hối hận với cuộc sống đó. 
4
1239263
2016-06-29 12:42:27
--------------------------
454928
4980
Thích vì những tác phẩm của O.Henry rất ý nghĩa. Ban đầu mua sách mình cứ tưởng đây là cuốn full của truyện chiếc lá cuối cùng, nhưng thật ra không phải vậy. Chiếc lá cuối cùng là 1 truyện ngắn trong tập các truyện ngắn của ông. Và đây là tập truyện đó.
Rất không thích cách dịch giả dịch các tên nhân vật trong bài viết, chuyển ngữ sang Việt nghe khó chịu ghê gớm, đang đọc hay mà gặp tên nhân vật làm tụt hết cả cảm xúc. Nếu có tái bản, hi vọng tác giả giữ nguyên các tên riêng như nguyên mẫu @@
3
533454
2016-06-22 11:23:45
--------------------------
440967
4980
Tôi được học  truyện Chiếc lá cuối cùng trong chương trình lớp 8. Tôi vô cùng ấn tượng với tác phẩm này nên đã mua tuyển tập truyện ngắn của Ohenry. Các câu chuyện của ông nhẹ nhàng, ngắn gọn với giọng văn hài hước, dí dỏm nhưng vô cùng ý nghĩa và sắc sảo. Tình tiết truyện hết sức gay cấn kết hợp với kết thúc bất ngờ luôn gây được sự cuốn hút cho người đọc. Ohenry thực sự là 1 bác thấy trong viết truyện ngắn. Tóm lại, đây là 1 cuốn sách tuyệt vời  và đáng tiền mua
5
1258770
2016-06-02 15:57:05
--------------------------
392866
4980
Chiếc lá cuối cùng của O'Henry là một tác phẩm đầy tính nhân văn. Truyện kể về cuộc sống của những người nghệ sĩ nghèo trong đó có cả Sue, Johnsy và cụ Berhman. Mọi thứ bắt đầu kể từ khi Johnsy mắc phải chứng bệnh sưng phổi nặng. Bệnh tật, nghèo khó, tất cả mọi thứ tạo thành sự tuyệt vọng khiến cô mất hết niềm tin vào cuộc sống. Cô nghĩ cuộc sống của cô cũng sẽ giống như những chiếc lá thường xuân ngoài kia, khi chiếc lá cuối cùng rơi xuống thì cuộc sống của cô cũng sẽ chấm hết. Nhưng cụ Berhman đã không để điều đó xảy ra, vào đêm mà chiếc là thường xuân cuối cùng ấy đáng lẽ phải rơi thì ông đã vẽ một chiếc lá khác thay thế vào đó. Cho dù việc làm này phải trả giá bằng chính mạng sống của mình. Cũng chính sự hy sinh đó đã tiếp thêm nghị lực sống cho Johnsy, để cô có thể hoàn cảnh tuyệt vọng của bản thân
4
632796
2016-03-07 20:23:53
--------------------------
387945
4980
Chiếc lá cuối cùng của O. Henry là tuyển tập các truyện ngắn của ông với những câu chuyện nhẹ nhàng, dí dỏm, nhưng ẩn chứa trong đó là những bi kịch nghiệt ngã của cuộc sống. Trong các truyện ngắn của cuốn sách có lẽ mình vẫn thích truyện "Chiếc lá cuối cùng" nhất, đây là truyện mình được học trong sách giáo khoa Văn lớp 8, nói về nghị lực sống của con người, nói về niềm tin về cuộc sống, câu chuyện đem đến cho ta nghị lực sống, phải biết lạc quan, tin tưởng vào cuộc sống.
Bìa sách rất đẹp
5
863683
2016-02-27 21:51:23
--------------------------
352459
4980
Nội dung truyện rất hay ngoại trừ phần dịch thuật các tên địa danh và danh xưng nghe như trong sách giáo khoa lớp 8 ý. Tác phẩm này nên giữ nguyên tên cũ như khi tác giả đặt như thế sẽ chuyên nghiệp hơn mặc dù khó đọc hơn. Nội dung thì khỏi bàn cãi đến sách giáo khoa cũng cho một câu truyện trong sách này để cho bọn học sinh học thì biết nó nổi tiếng thế nào rồi cùng với giá trị nhân văn sâu sắc bộ truyện này rất đáng xem nếu bạn là một người yêu văn học
3
919459
2015-12-14 14:51:45
--------------------------
344866
4980
Phải nói thật tác giả viết chuyện rất cảm động và sâu sắc . Đặc biệt trong cuốn truyện đó có lẽ mọi người nên đọc nhất là câu chuyện chiếc lá cuối cùng .  Khi đọc xong câu chuyện đó 2 từ mà tôi phải thốt lên là xúc động . Ai khi đọc xong truyện này có lẽ sẽ phải rơi nước mắt bởi ý nghĩa nhân văn quá sâu sắc  . Nhưng cái bìa của cuốn sách có vẻ không phù hợp theo tôi nghĩ bìa cuốn sách nên khác hoạ hình chiếc lá một cách rõ nét hơn nhưng mà chất chứa được ý nghĩa sâu sắc trong câu truyện 
5
898367
2015-11-29 11:41:39
--------------------------
322104
4980
Mình mua cuốn sách này vào một ngày tháng mười. Rất háo hức để ôn lại tuổi thơ, một thời học sinh được học truyện ngắn "Chiếc Lá Cuối Cùng" của O.Henry. Nhưng hỡi ôi cách dịch những tên riêng (tên nhân vật, tên thành phố) rất chi là phản cảm và kì cục. Làm mình mất cả hứng để đọc. Mình nghĩ thời đại tên lửa rồi thì những tên riêng này "phải" để nguyên tiếng Anh và không nên dịch ra Tiếng Việt. Ngoài ra mình để ý dưới mỗi truyện đề tên một người dịch khác nhau (đối với mình điều này rất kì lạ và không chuyên nghiệp tí nào). Nói chung mình rất ko vừa lòng với tập sách này. 1 sao
1
712799
2015-10-15 16:28:57
--------------------------
257496
4980
Tôi rất yêu thích các truyện ngắn của O.Henry. Các truyện ngắn mà ông viết có lối miêu tả rất giản dị, dễ hình dung, thường là các câu chuyện đời thường mang những sự kiện trái ngược trong đó, hầu hết đều rất ý nghĩa về lòng nhân hậu, tình yêu, lương tâm, sự hy sinh, phản ánh rất đúng được các mặt đan xen lẫn nhau trong cuộc sống. Nhiều truyện hài hước, nhiều truyện cảm động vô cùng, nhưng truyện nào đọc xong dù thấy vui hay buồn cũng đều khiến ta phải suy ngẫm, liên tưởng đến cuộc sống xung quanh. 
5
740662
2015-08-07 18:24:19
--------------------------
249596
4980
Đã lâu lắm rối mình mới đọc lại tác phẩm Chiếc lá cuối cùng, một tác phẩm đã để lại ấn tượng rất lớn hồi mình còn nhỏ. Bây giờ lớn hơn, khả năng cảm nhận và hiểu ý nghĩa tác phẩm đầy đủ hơn, mình lại càng yêu tác phẩm này hơn.
Ngoài Chiếc lá cuối cùng, các tác phẩm khác của O' Henry cũng rất hay, nội dung từng câu chuyện diễn ra nhẹ nhàng nhưng cũng thật cảm động.
Mỗi con người đều có những số phận khác nhau, nhưng nếu chúng ta có tấm lòng và giúp đỡ người khác một cách chân thành thì cuộc sống sẽ tốt đẹp hơn rất nhiều.
4
707382
2015-07-31 22:44:41
--------------------------
207231
4980
Ô Hen-ry quả thực là bậc thầy về truyện ngắn. Có trong tay cuốn tuyển tập truyện ngắn Chiếc lá cuối cùng của ông là một lựa chọn tuyệt vời. Mình thấy truyện của ông cốt truyện dung dị nhưng dưới ngòi bút tài hoa chúng hiện lên chân thực, đẹp đẽ đến không ngờ và đặc biệt phong cách kết bất ngờ khiến chính mình phải ngạc nhiên, nhiều khi là buồn nhiều hơn vui, nói chung rất thú vị. Nhưng đây là cuốn sách hay, một lăng kính đẹp. Tuy không hướng đến thị hiếu đọc người trẻ nhưng cuốn sách này có rất nhiều truyện ngắn khá hài hước và thú vị, rất đáng đọc!
5
349220
2015-06-11 19:29:49
--------------------------
202306
4980
Một câu chuyện với nội dung sâu sắc và rất cảm động. Câu chuyện thể hiện tình yêu thương đối với nhau của những con người nghèo khổ. Tuy cuộc sống vất vả, khó khăn, nhưng vẫn ấm áp tình người. Mình rất thích nhân vật họa sĩ già và tác phẩm vĩ đại nhất đời ông. Chính tác phẩm ấy đã mang đến niềm hi vọng cho cô họa sĩ trẻ. Cái kết câu chuyện quá buồn, khiến người đọc phải rơi nước mắt: một người họa sĩ trẻ hy sinh cuộc đời mình để mang lại cuộc sống mới cho những người trẻ có nhiệt huyết hơn. Câu chuyện mang đầy tính nhân văn, rất đáng đọc.
5
507583
2015-05-29 10:46:02
--------------------------
192874
4980
Tập truyện ngắn mang nhiều ý nghĩa, nhiều nội dúng với tính nhân văn cao. Có thể nhiều người đã từng tiếp xúc với truyện ngắn "Chiếc lá cuối cùng" trong tập truyện bởi chương trình ngữ văn trung học cơ sơ. Dẫu vậy, tập truyện mang ý nghĩa sâu sắc và đa dạng hơn. Tập truyện ngắn với những cốt truyện bình dị, độc đáo, lời văn của tác giả rất nhẹ nhàng, không quá phô bày bao cảm xúc của nhân vật nhưng lại tế nhị để người đọc thấy được hoàn cảnh diễn ra trong câu chuyện. Tập truyện rất hay, phù hợp với nhiều lứa tuổi, mang tính nhân đạo!
5
419803
2015-05-05 21:41:04
--------------------------
175394
4980
Thật khó để nhận xét cả tập truyện ngắn này vì tôi chỉ được tiếp xúc với truyện chủ đề trong tập sách, Chiếc lá cuối cùng, qua chương trình ngữ văn trong nhà trường. Nhưng chỉ bấy nhiêu thôi cũng để lại trong tôi một ấn tượng mạnh mẽ. Truyện của O. Henry ngắn gọn và dung dị, nhưng sau sự đơn giản ấy, cả một thực tế xã hội được vẽ ra đậm nét. Truyện của ông giàu tính nhân văn, đầy cảm hứng, mang lại tình yêu cuộc sống cho người đọc. Sự hy sinh, lòng vị tha và sự trân trọng từng phút giây cuộc sống là những gì tôi nhận ra từ trang viết của ông.
5
515736
2015-03-30 08:09:35
--------------------------
156821
4980
Mình biết đến tác phẩm này qua một văn bản cùng tên ở chương trình Ngữ văn 8 (hình như thế :v). Nội dung câu truyện khá đơn giản cũng rất thực tế nhưng ý nghĩa nó mang lại rất sâu sắc. Nhân vật cụ Bơ-men là nhân vật mà mình thích nhất. Cụ đã âm thần vẽ lên "chiếc là cuối cùng" - kiệt tác nghệ thuật để đời của cụ, nó đã giúp cụ hoàn thành tâm nguyện lớn nhất đời nghệ sĩ của mình, cũng chính nó đã cứu Giôn-xi - cô họa sĩ trẻ thoát khỏi bàn tay thần chết. 
Tuy vậy, cái kết của truyện lại khiến độc giả không thể kìm được nước mắt. Ngay khi Giôn-xi tìm lại sức sống của mình, cụ Bơ-men lại ra đi bởi chính căn bệnh Giôn-xi từng mắc phải.
5
445522
2015-02-06 15:27:12
--------------------------
156264
4980
Chiếc lá cuối cùng là một tác phẩm hết sức ý nghĩa và mang đầy tính nhân văn. Người họa sĩ già đã không ngại mưa gió để vẽ nên một kiệt tác đó chính là chiếc lá cuối cùng trên cành cây trên bức tường đối diện với căn phòng Giônxi_ cô gái  bị bệnh nặng. Cong người ấy với trái tim đầy lòng thương cảm đã vực dậy, đã làm sống lại một con người tưởng chừng như đang gục ngã dưới bệnh tật. Chính vì lòng thương cảm mà cụ Bơ men đã làm nên một kiệt tác mang đầy tính nhân văn và nó sống mãi với thời gian.
5
519052
2015-02-03 21:11:31
--------------------------
155723
4980
Câu chuyện về những tâm hồn đẹp giữa cuộc sống đầy nghiệt ngã, tình yêu thương, niềm tin giữa con người với con người hay chính là bức thông điệp khẳng định sứ mạng và sức mạnh của nghệ thuật chân chính đó là những gì "Chiếc lá cuối cùng" của O. Henry đem đến. Với "chiếc lá cuối cùng"- kiệt tác duy nhất của đời mình, cụ Bơ-men đã giành lại Giôn-xi từ tay thần chết. Cụ Bơ-men đã vẽ chiếc lá đó bằng cả trái tim, người hoạ sĩ già khốn khổ ấy không có quyền năng tối thượng của Thượng đế, nhưng ông có một trái tim giàu lòng thương cảm. Một câu chuyện đầy ý nghĩa! Người hoạ sĩ già Bơ-men là hiện thân của sự cao thượng, lòng vị tha, đức hy sinh của một con người chân chính đã làm bùng lên sự sống tưởng như đã tắt trong tâm hồn một cô gái trẻ, làm thế nào để cô thôi không bị ám ảnh bởi quy luật lạnh lùng của tạo hoá, để rồi vươn lên giữa cuộc đời bằng chính sức sống tiềm tàng trong tâm hồn cô. Đó là lúc người hoạ sĩ già ấy hiểu thấu sứ mạng vinh quang và cao cả của nghệ thuật: hướng về con người chứ không phải là nhằm tạo chút danh tiếng hão huyền, nghệ thuật chỉ thật sự bắt đầu khi sáng tạo của người nghệ sĩ giúp ích cho đời.
5
172954
2015-02-01 21:44:26
--------------------------
154725
4980
Chiếc lá cuối cùng là một trong nhiều truyện ngắn ý nghĩa của nhà văn O.Henry. Nội dung tác phẩm khá đơn giản nhưng lại mang ý nghĩa sâu sắc. Chiếc lá cuối cùng - kiệt tác của ông họa sĩ già Bơmen, người đã "bốn mươi năm cầm cọ mà vẫn không chạm được đến gấu áo Nữ thần" nhưng chỉ với kiệt tác duy nhất và cuối cùng ấy, ông đã cướp lấy sinh mạng Giônxi từ tay thần chết. Tình người còn lớn hơn nghệ thuật, nó làm cho nghệ thuật trở nên cao cả hơn. Đó mới là tác phẩm sống mãi với thời gian.
5
528434
2015-01-30 10:03:13
--------------------------
154317
4980
"Chiếc lá cuối cùng" là một truyện ngắn đặc sắc của nhà văn O.Henry. Chiếc lá cuối cùng, đó là kiệt tác của một người họa sĩ già, cô độc và "không thành công trong nghệ thuật" dù đã "bốn mươi năm cầm cọ mà vẫn không chạm được đến gấu áo Nữ thần". Nhưng chính kiệt tác duy nhất và cuối cùng của cuộc đời người họa sĩ ấy đó đã cướp lấy một sinh mạng từ tay thần chết trở về.Tác phẩm ấy đã ra đời trong một hoàn cảnh lao động vất vả, nó dũng cảm bất chấp quy luật, vươn lên tất cả để chiến thắng nghèo đói, bệnh tật. Tình người còn lớn hơn cả nghệ thuật, nó làm cho nghệ thuật trở thành sự sống bất tử. Và đó mới là tác phẩm “đáng thờ”, xứng đáng tồn tại với thời gian.
5
546314
2015-01-28 22:05:36
--------------------------
152628
4980
Lần đầu tiên mình biết đến truyện này là qua cuốn sách giáo khoa ngữ văn hồi cấp 2. Nội dung tác phẩm rất đơn giản, thế nhưng bài học của nó thì lại đầy ý nghĩa. Một con người trẻ tuổi đã rất cận kề với cái chết, tuyệt vọng đến nỗi gắn mạng sống của mình vào một chiếc lá mỏng manh. Thế nhưng, ngày qua ngày chiếc lá vẫn còn đó, giống như mạng sống của nhân vật kia vẫn tồn tại. Để rồi, người đọc phát hiện ra sự thật về chiếc lá cuối cùng kia và nhận ra những thông điệp sâu sắc mà O.Henry truyền tải. Có thể nói, đây là một tác phẩm rất hay về những con người trong cuộc sống, về cách suy nghĩ lạc quan, về lòng yêu thương và sự hi sinh cao cả.
5
316903
2015-01-23 18:12:12
--------------------------
