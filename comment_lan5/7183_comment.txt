495443
7183
Quyển sách này nhìn chung có rất nhiều lý thuyết và những công thức khó nhớ. Mình đã mua và cảm thấy không hứng thú lắm vì nó hơi khô khăn và nguyên tắc. Đối với những ai không theo ngành này thì khó có thể hiểu hết được nội dung.
3
1026280
2016-12-12 21:59:36
--------------------------
203559
7183
Mình học chuyên ngành Tài chính, việc mua cuốn sách này chỉ nhằm mục đích tham khảo. Tuy nhiên cách viết của tác giả rất đúng với tên gọi của sách: "tài chính dành cho mọi người". Phải nói rằng thế giới của Tài chính là một thế giới vô cùng phức tạp, với hằng hà sa số các công thức, các chỉ số, các khái niệm... bởi vậy nếu bạn muốn chỉ trong 1 cuốn sách mà có thể hiểu hết về tài chính là điều không thể. Thay vào đó, việc tìm hiểu về lĩnh vực này đòi hỏi thời gian, sự chủ động tìm tòi và cả kinh nghiệm quan sát thực tế. Cuốn sách viết rất dễ hiểu, gọn gàng, trình bày trật tự và nhiều ví dụ, hình ảnh, biểu đồ minh họa, lời viết bình dân, mình cho rằng vậy. Sách này sẽ giúp ích cho nhiều bạn không chuyên ngành nhưng mới bước đầu tìm hiểu về lĩnh vực này.
4
564333
2015-06-01 17:42:36
--------------------------
130333
7183
Tôi không học về ngành tài chính hay kế toán, nhưng cũng đã mua và đọc mục đích để hiểu cơ bản về tài chính trước khi đọc một cuốn sách khác về tài chính - kế toán.
Trong chương II - Lợi suất và rủi ro đưa ra nhiều công thức có thể nói khó hiểu để rồi giải thích một điều cực kì dễ hiểu mà chẳng cần những công thức đó người đọc vẫn dễ dàng hiểu cái kết luận của chương. 
Trong chương III có lẽ chỉ cần đọc 3 trang đầu.

Mục 3.2 của chương VI có lẽ trùng mục 3.2.2 chương I

Trong sách sử dụng quá nhiều thuật ngữ chuyên ngành, kiến thức giống như là chung chung, tổng kết cho những người rất giỏi về tài chính đọc gom lại kiến thức.
2
135756
2014-10-16 22:10:09
--------------------------
115073
7183
Cuốn sách này là quyển sách đầu tay của thầy mình viết, một người thầy mà trong mỗi bài giảng mình cảm nhận được sự tận tâm và nhiệt huyết trong đó, và tất nhiên là sự hài hước, thông minh. 
Về cuốn sách, đầu tiên là nó đã bao quát được những kiến thức "nhập môn" về tài chính, như tên gọi của nó, "Tài chính 101". Lối viết giản dị, bình dân, như mục tiêu của tác giả là dành cho mọi đối tượng người đọc. Các vấn đề đưa ra được bình luận, phân tích hợp lý. Cách tiếp cận vấn đề, các ví dụ minh họa thực tế, chân thật, thậm chí có phần "bình dân". Tuy nhiên, đó cũng là vấn đề lớn của quyển sách, hướng đến "mọi đối tượng người đọc" trở thành "không đối tượng nào cả": Sinh viên thì đọc thấy không đủ, người bình thường đọc chơi chơi thì chả đến đâu, học giả hay giới học thuật thì càng không đến. Đây là điều mà mình cảm thấy tiếc nhất ở quyển sách này. 
Kết lại thì đây là một quyển sách mà những người "quan tâm đến tài chính" ở mức độ trên kiến thức phổ thông nên mua, đọc để hiểu thêm về thế giới tài chính.
4
350154
2014-06-22 16:31:03
--------------------------
