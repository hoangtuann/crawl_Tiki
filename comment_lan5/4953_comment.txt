434805
4953
Bìa sách thật sự rất đẹp ^^
Kết văn phong của tác giả, rất nhẹ, buồn và hẫng. 
Các truyện ngắn thường mang đậm một nét buồn, trong khoảng 10 truyện ngắn thì mình tính sơ sơ cũng phải có đến 6,7 truyện ngắn  là nam chính chết :3 (K hiểu bà này có thù gì với đàn ông).
Nhìn chung, đọc thì đọc chứ mình vẫn không được đọng lại nhiều, có chăng là cảm giác cảm thấy mình tự tin và may mắn hơn người khác chăng? 
Tóm lại, những bạn sống nội tâm, ưa một chút buồn thì nên đọc cuốn này. =)))
4
946189
2016-05-23 22:21:04
--------------------------
262543
4953
Đây là một tuyển tập truyện ngắn hay. Xuyên suốt cả tập truyện có thể thấy phảng phất những dấu vết của "chia tay", "mất mát" và cả "xa cách". Chắc hẳn CaDe đã dành nhiều thời gian cũng như tâm trí để suy nghĩ về vấn đề này lắm. Thế nhưng, không phải vì vậy mà cả quyển sách đều có không khí u buồn. Sau cùng, ta đều thấy một tia sáng của "sự trưởng thành", "sự lột xác", sự giác ngộ trong tâm trí và cả hành động để biết quý trọng những gì mình đang có trong tay, thay vì cứ phải ủ ê trong u buồn.
Nhiều tình huống truyện của CaDe rất độc đáo. Mình nghĩ hầu hết nếu chúng được đầu tư nữa thì có thể trở thành truyện dài. Ở một số truyện ngắn, CaDe "mở nút" quá nhanh, cảm giác rất vội. Một phần cảm xúc vị thế cũng bị mất đi, nhường lại cho một triết lý sau cùng.
Hi vọng sẽ được đọc thêm từ CaDe trong thời gian đến.
5
172444
2015-08-11 22:57:47
--------------------------
247385
4953
Cuốn sách đơn giản là những câu chuyện tình yêu của người trẻ,những trái tim vẫn luôn thổn thức yêu thương,ắt hẳn sẽ bắt gặp đâu đó sự đồng cảm với tác giả, với những cảm xúc mà tác giả gởi gắm, tình cảm là chủ đề xuyên suốt tác phẩm,với những suy nghĩ trăn trở của người trẻ ,cuốn sách nhẹ nhàng với những yêu thương của tuổi trẻ,gần gủi và sống động với những trái tim mang nhiệt huyết,có lúc giữa cuộc sống vội vã người trẻ quên đi những góc sâu thẳm trong trái tim, những rung động và yêu thương chưa được giãi bày. Quên đi những thổn thức của trái tiim.
4
132159
2015-07-30 10:15:26
--------------------------
158177
4953
Cuốn sách này làm tôi nhớ đến câu nói: "Happiness is not something ready made. It comes from your own actions." của Dalai Lama. Hạnh phúc không phải là những thứ sẵn có. Hạnh phúc đến từ chính hành động của chúng ta.
Yêu thương không cần là những điều gì quá xa xôi hay xa xỉ. Năm tháng đôi khi làm phai tàn tình yêu và tuổi trẻ, nhưng chỉ cần chúng ta tự tiếp thêm động lực và mạnh mẽ tiến lên phía trước, bỏ lại phía sau những điều phiền muộn, khi ấy hạnh phúc sẽ tức khắc đến với chúng ta.
Cuốn sách này tập hợp những bài học vô cùng giản dị mà thực sự thấm thía
 Hơn nữa, với ngòi bút mềm mại, mượt mà và gần gũi, tác giả đã chạm đến những điều sâu thẳm trong trái tim tôi, làm khơi dậy những nhận thức vốn đã say ngủ trong lòng người đọc.
5
293316
2015-02-11 19:32:39
--------------------------
157819
4953
"Giữa đôi ngả yêu thương" đã hấp dẫn tôi ngay từ cái tên gọi và bìa sách. Nội dung của các câu chuyện chỉ đơn thuần là những chuyện bình dị, dễ thấy trong đời sống thường ngày. Nhưng dưới ngòi bút nhẹ nhàng, uyển chuyển của tác giả CaDe, nó lại hiện lên thật sinh động, lôi cuốn. Ở đó, có những con người không dám bước về phía trước chỉ vì những bi kịch phải chịu đựng trong quá khứ, có những ước mơ, hoài bão của bao con người trẻ tuổi,... Mỗi câu chuyện đều mang đến cho tôi những bài học đáng suy ngẫm.
4
445522
2015-02-10 13:12:32
--------------------------
157699
4953
Quyển Giữa Đôi Ngả Yêu Thương tạo nên một tuyển tập những cảm xúc rất mới lạ dành cho những người trẻ tuổi với trái tim yêu thương. Ở đó, mình nhìn thấy một số người vái trái tim chôn chặt trong quá khứ mà không dám bước tiếp với hiện tại, thấy được những ước mơ có phần mơ hồ đan xen giữa những chuỗi u ám phía trước làm trái tim họ chùn bước....Những cảm xúc, suy tư của người trẻ về tình yêu và tương lai đã được tác giả CaDe thể hiện một cách thật tinh tế. Dưới ngòi bút của mình, CaDe biến những câu chuyện thường tình ấy thành những câu chuyện hấp dẫn và ẩn chứa nhiều thông điệp trong cuộc sống này!
Không quá mới lạ, không nhiều điểm nhấn hay chiêu trò câu khách, Giữa Đôi Ngả Yêu Thương vẫn tạo được sức hấp dẫn trong lòng mình bằng chính nội dung truyện cô đọng và văn phong trau chuốt, nhẹ nhàng và tinh tế!
4
303773
2015-02-09 22:38:47
--------------------------
155690
4953
Tôi đã từng đọc một tác phẩm của Cade là Chúng ta đã bên nhau bao lâu. Và tôi rất thích văn phong và cách viết cũng như cách dẫn dắt, tạo tình huống cho nhân vật một cách tự nhiên, mềm mại, uyển chuyển của Cade. Khi nghe tin cuốn sách này được xuất bản thì tôi đã rất mong chờ nó và tôi đang đợi tiki cho đặt hàng trước để đặt nó. Khi nhận được cuốn sách trên tay. Tôi thực sự rất hài lòng. Từ hình thức đến nội dung cuốn sách đều được chăm chút rất cẩn thận. Bìa ngoài được làm từ chất liệu tốt, trang trí giản dị mà hấp dẫn. Nội dung mang tính lãng mạng thu hút người đọc từ cái tiêu đề cho đến từng câu chữ.
4
316220
2015-02-01 20:51:17
--------------------------
154094
4953
Yêu Thương luôn là đề tài bất tận diễn hoài không hết được cái ý nghĩa của nó. Nó luôn luôn tồn tại trong mỗi cá thể tâm hồn của con người. Nó được che giấu trong chiếc hộp kỹ lưỡng của tận đáy sâu, để khi cần thiết lại trào lên như cơn sóng mãnh liệt được òa ra thành những trang giấy viết lời tự thú.

"Giữa đôi ngã yêu thương" như một bản tình ca bộc bạch của tác giả CaDe gửi đến người đọc cảm nhận. Nó được tích tụ chưng cất từng cảm nghĩ vào chiếc dấu đóng dập vào mỗi trang truyện đầy súc tích. Và từ đó, bao thầm kín của tuổi trẻ mơ mộng hiểu được, và nhận ra trên con đường dài phía trước còn cả khu rừng mơ hồ u ám mà ta, nhân vật chính đó phải từng bước thoát ra. Và rồi, từng bước chầm chậm nắm lấy những yêu thương đôi ngã của chình mình.
5
527639
2015-01-28 12:14:49
--------------------------
