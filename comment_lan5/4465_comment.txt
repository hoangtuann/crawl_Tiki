472986
4465
Cuốn sách “Chùa Hương Tích – cảnh quan và tín ngưỡng” của tác giả Phạm Đức Hiếu đã giới thiệu cho người đọc những thông tin cơ bản về vùng đất Hà Tây và chùa Hương Tích – một danh lam thắng cảnh tuyệt vời của đất nước ta. Cuốn sách được chia làm 4 phần: giới thiệu sơ lược về Hà Tây và chùa Hương, thắng cảnh chùa Hương, lễ hội chùa Hương, Phật Bà Quan Âm và kinh “cứu khổ cứu nạn”. Ngoài ra, cuốn sách còn có một phần giới thiệu về các đặc sản của chùa Hương. Mình hi vọng cuốn sách trong thời gian tới sẽ được chỉnh lí vì sách xuất bản khá lâu nên nhiều thông tin trong sách đã cũ, không còn phù hợp.
3
470567
2016-07-10 09:46:07
--------------------------
