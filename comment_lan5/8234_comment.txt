512861
8234
Kêu gọi bạn bè, người thân cùng bỏ vốn sáng lập Hay tự thân làm chỉ một mình và gọi vốn bên ngoài?
Có nên thuê, sử dụng nhân viên là bạn bè, người quen hay chỉ tuyển ngòai? Ưu, nhược điểm của từng lựa chọn ra sao? Và trong tình huống nào, hòan cảnh nào thì ta nên lựa chọn phương án này hay khác? 
... 
Hàng loạt các câu hỏi mấu chốt được tác giả đặt ra và giải quýêt dưới nhiều góc nhìn. Công trình 10 năm này quả thực là 1 cuốn sách vô cùng giá trị cho những "founder", nó sẽ giúp bạn cân nhắc giảm thiểu những sai lầm cơ bản trong bước đường đầu tiên tạo lập doanh nghiệp.
Với tôi, đây là một cuốn sách PHẢI CÓ !
5
646036
2017-01-19 09:48:23
--------------------------
473574
8234
Để có thể cầm cuốn sách Thế Lưỡng Nan Của Nhà Sáng Lập trên tay, tác giả đã giành ra hơn 10 năm nghiên cứu hơn 10 ngàn nhà sáng lập, khởi nghiệp. Bản thân mình rất thích những cuốn sách như thế này, bởi nó giúp mình vững niềm tin và hạn chế những sai lầm mà những người đi trước đã mắc phải, từ đó, những dự án của mình phát triển nhanh hơn và bền vững hơn.
Mình khuyên những bạn đang muốn sở hữu 1 cái gì đó của riêng mình, hay những bạn đang hùn hạp làm ăn, phải đọc ngay cuốn sách này. Mình tin khi bạn đọc xong, bạn cũng sẽ khuyên những người khác như thế!
5
65325
2016-07-10 18:38:05
--------------------------
364977
8234
Một doanh nhân khi mới bắt đầu khởi nghiệp thường phải đứng trước những quyết định rất khó khăn và quyển sách này viết về điều đó. Với nội dung sâu rộng và khá là hàn lâm, sách đã nêu lên được những vấn đề rất cụ thể, những nghiên cứu phải nói là sâu sắc bên cạnh đưa ra những bài học quý giá để tham khảo. Với những ai thực sự muốn tìm hiểu sâu thì đây là một quyển sách rất đáng để tham khảo và nghiền ngẫm tới lui từ đó có thể có được lựa chọn chính xác.
4
470739
2016-01-07 09:51:08
--------------------------
277398
8234
Những gì tôi thích nhất về cuốn sách này phân tích phân tích khó khăn của các quyết định quan trọng là đó là một người sáng lập phải đối mặt khi khởi nghiệp. Tôi đọc rất nhiều sách về kinh doanh và không tác giả nào ngoài  Wasserman đề cập đến vấn đề này 1 cách chi tiết cộng thêm những nghiên cứu sâu sắc và những bài học quý giá đúc kết từ những sai lầm của những nhà sáng lập. Đó là một sự bổ sung vô cùng quý giá với những tài liệu về cách làm chủ doanh nghiệp và  bạn sẽ tự tham khảo thường xuyên là cho dù bạn là một người sáng lập, người lao động sớm, nhà đầu tư, người kinh doanh, quan chức chính phủ hay chỉ là người hâm mô những founder tài năng.
5
65620
2015-08-24 21:58:56
--------------------------
