459190
4633
Đây là cuốn thứ 2 mình mua để đọc và ngẫm nghĩ về đề tài chiến tranh trong những tác phẩm của Nguyễn Đình Tú. Lần này, tác giả đưa người đọc tới chiến trường Campuchia khốc liệt, cùng với đó là những vấn đề mà nhân vật chính, Anh gặp phải trên con đường đi tới đó. 
Có những điều Anh đã trải qua, khi mình đọc tới đó, khiến mình nhớ tới những trải nghiệm va chạm trong cuộc sống. Và nhìn vào những điều đó, thấy rằng những gì mình đã trải qua chỉ phần nhỏ tí thôi, chưa phải là gì ghê gớm. 
Chợt nghĩ, nếu có nhiều người trẻ bây giờ, cũng đọc những tác phẩm của những người lính đã kinh qua các cuộc chiến thì tốt biết bao. Họ sẽ hiểu và quý trọng cuộc sống này hơn!!!
4
65325
2016-06-25 21:17:22
--------------------------
412322
4633
Theo đánh giá cá nhân thì Hoang Tâm là tiểu thuyết đọc được của Nguyễn Đình Tú. Luận đề mà nhà văn nói đến trong tác phẩm chính là: con người trong hành trình tìm lại bản thể.  Cách gọi tên nhân vật theo kiểu phiếm định nhà văn muốn chuyển vấn đề cụ thể của tác phẩm thành vấn đề mang tính phổ quát.  Cách viết của tác giả có đan cài yếu tố hiện thực cùng với yếu tố kì ảo như một thủ pháp độc đáo.  Giọng văn kể chuyện say sưa,  nhiều tình tiết cũng là điểm nhấn của tác phẩm 
4
1088790
2016-04-07 15:28:23
--------------------------
384284
4633
Hoang Tâm là câu chuyện khá góc cạnh. Những độc giả thông thường nếu chỉ bị thu hút bởi bìa sách và cái tựa chắc sẽ khá sốc trước sự tàn bạo của cuộc chiến tranh Campuchia, một trong hai bối cảnh chính của truyện được tác giả khắc họa rất chân thực. Yếu tố "hoang tâm" của truyện hơi bị lẫn lộn, khi hai mạch truyện thực và ảo không đan xen được với nhau, tạo cảm giác như tác giả chỉ chép ra suy nghĩ nội tâm của mình rồi cố gắn nó vào một bối cảnh hoang đường thứ hai, coi nhẹ việc dẫn dắt câu chuyện. Nói chung là có cảm giác mình đang đọc một cuốn hồi ký về chiến trường K - được viết rất giản dị và chân thực - chứ không phải một tác phẩm văn học - lý do mình mua sách. Vì vậy nên nếu ai đang có ý định tìm một tác phẩm văn học, mình xin khuyến cáo bỏ qua quyển này,
2
598425
2016-02-22 05:35:07
--------------------------
364375
4633
Hoang tâm không sử dụng hoa từ, mỹ ngữ, bút pháp nghệ thuật thượng thừa hay lối hành văn khuôn khổ chỉnh chu, mà Hoang Tâm hoàn toàn rất gần gũi, mộc mạc, hài hước nhưng phản phất trong cái bình dị, chân phương hay nụ cười của người đọc ấy lại là một triết lý thâm sâu ở đời, khiến người đọc phải suy nghĩ, chiêm nghiệm nhân sinh. Mạch truyện nhẹ nhàng thoải mái, không căng thẳng nên rất dễ đi lòng người đọc.
Thiết kế bìa độc đáo, ấn tượng mạnh bởi người phụ nữ khỏa thân nhưng đầy vết trầy xướt, khiến người đọc liên tưởng tới đời chỉ đẹp khi đã va chạm, va vấp, trày trụa bởi những dằn xé của cái tâm đầy hoang dại.
3
624571
2016-01-06 04:33:54
--------------------------
