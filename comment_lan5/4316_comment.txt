455267
4316
Cuốn này mình đọc xong cũng lâu rồi giờ mới viết nhận xét. 
Về hình thức thì sách dày lắm, bìa đẹp như hình, tạo sự bí ẩn thu hút người đọc, chất giấy mịn nhẹ, màu vàng nhạt cũng hợp với cái bìa lắm.
Nội dung truyện đối với riêng mình thì mạch truyện hơi dông dài, lúc đầu truyện hơi khó hiểu. Nói chung đọc xa xa lắm và phải tập trung lắm cơ. Mình không thích hợp với những truyện như vậy vì cái tính mình thiếu kiên nhẫn, không tập trung lâu vào một việc khi mà nó không hấp dẫn được mình. Truyện này mình cho 3 sao.
3
890619
2016-06-22 14:38:01
--------------------------
453890
4316
Mặc dù bản dịch này kém, hay mắc lỗi ngữ pháp, nhiều câu vô nghĩa gây khó hiểu hoặc không diễn đạt được đủ ý, với cả thỉnh thoảng chấm phẩy cũng vô tội vạ,... Nhưng Incarceron thực sự rất tuyệt. Đọc cuốn hút cực kỳ luôn. Và ý tưởng truyện thì rất hay, tôi dành nhiều lời khen ngợi. Quả thực tác phẩm sáng tạo này đã đem đến cho tôi những giờ phút thoải mái, tôi đọc rất nhanh, vì gần như đoạn nào cũng hấp dẫn. Thế nên tôi chắc chắn sẽ đọc tập 2, và tôi còn hy vọng tác giả Cartherine Fisher sẽ sáng tác thành bộ truyện dài dài, nhưng hình như series này chỉ có ba tập thôi thì phải?
5
386342
2016-06-21 15:31:46
--------------------------
441115
4316
Khi đọc cuốn sách này người đọc sẽ bị cuốn vào một thế giới mới. Nơi mà có hai thế giới tồn tại song song với nhau. Tuy nhiên dù là ở đâu thế giới này hay thế giới kia thì đó vẫn là những ngục tù giam giữ những kẻ ở trang nó theo các cách khác nhau. Qua đó nói lên sự nỗ lực để vượt qua bản thân-ngục tù của  mỗi người. Và sau chót kẻ đâm lao thì phải theo lao cậu bé nhân vật chính rồi cũng sẽ bị cuốn vào một trò chơi mới nơi hoàng cung. Dù gì thì đây cũng là một cuốn sách hay.
5
954214
2016-06-02 20:28:01
--------------------------
429350
4316
Cốt truyện rất thú vị, thế giới hư cấu được xây dựng sống động và độc đáo. Tính cách các nhân vật cũng được khắc họa khá rõ nét. Chắc chắn đây sẽ là một tiểu thuyết hay nếu vào tay một dịch giả tốt.

Nhiều khi đọc như google dịch, rất tối nghĩa. Có chỗ còn sai chính tả và câu cú lộn xộn do dịch như là nghĩa đen word by word vậy. Mà cốt truyện thì phức tạp và nhiều tầng lớp nên câu văn tối nghĩa càng làm khó người đọc trong việc theo mạch truyện.

Khuyên các bạn mua cuốn gốc tiếng Anh. Văn phong khá mượt mà và hay hơn nhiều so với bản dịch.
2
314363
2016-05-13 06:09:09
--------------------------
402906
4316
Tặng bốn sao trong đó hai sao là dành cho bìa. Thực sự bìa quá đẹp, rất lôi cuốn. Cầm trên tay mà vui không tả nổi.nội dung ổn, dễ đọc, dễ hiểu, mạch truyện không quá rườm rà rắc rối. Tôi không mất quá nhiều thời gian để đọc hết cuốn sách này, đọc xong lại nhom nhem muốn mua tiếp phần hai mà tiền không cho phép. Ai mê viễn tưởng thì nên có cuốn sách này trong bộ sưu tập, nó thực sự không làm bạn thất vọng đâu. Cảm ơn tiki đã tài trợ cho cuốn sách này
3
979895
2016-03-22 23:28:03
--------------------------
373405
4316
Incarceron mênh mông thế, ai vẽ nổi
Nhịp cầu cong, vực thẳm, lâu đài
Ai người đời tự do từng trải
Mới định nghĩa đúng Ngục Tù mà thôi.
- Bài ca của Sapphique
Càng đọc càng cảm thấy cuốn hút, thấy như mình đang ở trong Ngục tù vậy. Nhưng đâu mới là lối ra, khi cả Bên Trong lẫn Bên Ngoài đều là một cái tù lớn. Đâu là kẻ xấu, người tốt khi mà ai hành động vì lợi ích của mình, bạn bè mình, gia đình mình. Dù tốt, dù xấu thì tất cả muốn nhìn thấy cái thế giới bên ngoài nơi mọi thanh sắt, mọi luật lệ bị phá bỏ. Ngay cả Incarceron - Ngục Tù cũng vậy.
5
406900
2016-01-23 18:19:16
--------------------------
358104
4316
Nhìn cái bìa sách thôi là muốn hốt em nó về với đội của mình rồi đặc biệt em nó lại thuộc thể loại giả tưởng mà mình thì chưa đọc giả tưởng lần nào hết nên mới mua với hai cuốn thần đá
Nói thật có thể đây là sách do tác giả phương tây nên cách viết hay hơn quyển thần đá nhiều đặc biệt cốt chuyện cuốn hút,mình có cho vài đứa bạn mượn xem ba quyển thì ai cũng nói quyển này hay và không viết dài dòng lê thê như quyển thần đá
Đọc xong hết quyển mới biết hình như nó tới hai quyển mà giờ cháy túi nên không xúc được thêm cuốn nữa tiết quá

5
1014255
2015-12-24 19:57:43
--------------------------
323593
4316
Lần đầu tiên nhìn thấy cuống Incarceron mình bị ấn tượng ngay bởi bìa sách mang màu sách u ám và thần bí, nhìn bìa là biết ngay em, sách thuộc thể loại Fantacy. Muốn giở ra xem thử nội dung thế nào thì phát hiện ẻm bị bao kín bằng nilon, chán vô cùng !! Nhưng đúng là quen mua sách online trên Tiki nên nhìn giá em khá chát những 140k làm mình nản. Mình nhẫm thuộc tên sách và tên tác giả rồi về lên Tiki tìm , đọc thử thấy cũng hay hay nên máu liều mua trọn bộ hai tập luôn. Nội dung cực ổn, cách tác giả miêu tả nhân vật và tình tiết khá cuốn hút dù không khí trong truyện hơi ảm đạm và tịch mịch. Lúc đọc mà cứ bị liên tưởng đến cảnh tối tối trong mấy tập phim Harry Potter.
5
738665
2015-10-19 10:57:39
--------------------------
231297
4316
Mới đọc tôi cảm nhận được sự tình yêu của Hoàng tử và cô gái, nhưng trong tình yêu nói riêng và cuộc sống nói chung đều có cái ác và cái thiện, tôi đã khóc khi Nữ hoàng làm điều như vậy tôi nghỉ thật băt công, tôi xót thương cho số phận của chàng, cô gái đã biết mình lấy 1 người không được đính ước như còn bé, tôi cảm nhận được sự mạnh mẽ của cô gái trước số phận của mình, dám đấu tranh lấy cái mình thích. Qua câu chuyện này tôi rút ra thật nhiều điều sống là phải biết phấn đấu không ỷ lại sống phận hiện tại không biết được tương lai mình ra sao, nên tôi nghỉ sống là sống theo những gì đam mê và thích thú thì cuộc sống nên ý nghĩa hơn. Tôi đang muốn mua phần hai để đọc xem kết cục của hai người họ thế nào.
5
680422
2015-07-17 23:12:25
--------------------------
182266
4316
Cuốn sách đã cuốn hút mình ngay từ cái nhìn đầu tiên . Bìa thiết kế rất cuốn hút với một màu u tối , bí ẩn . Nội dung truyện được giới thiệu khá đặc sắc và lôi cuốn người đọc . 
Cậu chuyện là sự đan xen giữa cổ đại và hiện đại , dù những chức vụ , sự tiến bộ luôn dừng lại ở thế kỉ XVII nhưng lại tồn tại những thiết bị tân tiến của thời hiện đại điều khiển cuộc sống con người . Trong truyện có hai thế giới song song gợi cho độc giả cảm giác huyền bí , tò mò và đầy hứng thú .  
Nhìn chung đây là một cuốn sách cuốn hút và có hình thức đẹp mắt !
4
335856
2015-04-13 19:01:18
--------------------------
