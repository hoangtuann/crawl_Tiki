550425
4657
Mình mua cuốn sách này vì hai lí do
1. Thích đọc những truyện phiêu lưu
2. Năm 1996 đã xem phim về cuộc phiêu lưu này trong một lần đi chúc tết nhà cô giáo. Vì thế rất là nhớ 1 đoạn trong phim và muốn xem lại nhưng lâu rồi không nhớ tên. Tìm mãi thì ra nó là ở cuốn truyện này được chuyển thể
Sách tạm ổn về hình thức, nội dung thì với mình khá hay. 
Tuy nhiên mình thích sách khổ lớn hơn nên không thích lắm khổ sách này dù nó là phổ biến. Cuốn này nều làm bìa cứng thì tốt. Font chữ cũng hơi to, nếu nhỏ lại 1 chút hợp lí hơn. Nhìn trang truyện cảm thấy dài hơn.
4
148734
2017-03-22 11:31:47
--------------------------
510161
4657
Tác phẩm nổi tiếng thế giới, nhiều người khen hay. khi đọc phần giới thiệu tóm tắt mình cũng thấy rất hào hứng: phiêu lưu tới xứ sở tí hon, khổng lồ, rồi xứ sở toàn nhà bác học... Nhưng khi đọc thì k được như vậy, đọc xong đến chuyến phiêu lưu thứ hai, sang chuyến thứ ba thì bắt đầu thấy nhàm chán. Xuyên suốt là 1 giọng văn kể đều đều, k có tình tiết gay cấn, k có đối thoại, k có diễn biến tâm trạng nhân vật. Các cuộc phiêu lưu nghe có vẻ hấp dẫn nhưng tựu chung lại chỉ có 1 mô típ: bị nạn lạc đến 1 chỗ nào đó, rồi bằng cách này hay cách khác được đưa tới gặp vua xứ đó, học tiếng nói ở đó, miêu tả cảnh vật, nhà cửa, con người ở xứ đó; sau 1 vài sự kiện xảy ra thì cuối cùng là rời khỏi xứ đó trở về nhà, rồi lại đi chuyến tiếp theo. Mọi nỗ lực cuối cùng đều nhằm mục đích phê phán xã hội đương thời. Trong suốt truyện, tác giả hay nhắc đi nhắc lại là cố gắng kể ngắn gọn, lược bớt những chi tiết dài dòng k liên quan để tránh làm phiền người đọc nhưng mình vẫn cảm thấy quá đơn điệu, nhàm chán. 
2
1958857
2017-01-13 21:49:46
--------------------------
495170
4657
Bao bìa được thiết kế đẹp, nội dung về Gulliver lạc vào xứ sở tí hon nhiều người được biết qua Doraemon nhưng khi đọc truyện chữ thì có cảm giác thu hút không kém vì người đọc có thể tự tưởng tượng qua từng câu chữ 
5
510988
2016-12-11 04:46:34
--------------------------
454132
4657
Thực ra lúc mua cuốn sách Gulliver Du Ký (Đinh Tị Books) chủ yếu là để đủ phí miễn vận chuyển nên mình cũng không hứng thú lắm, nhưng đến lúc bắt đầu đọc thì không dứt ra được. Mình vốn cũng là một người thích phiêu lưu du ngoạn nên cuốn sách này rất thu hút mình, mặc dù nó là sách thiếu nhi nhưng cách viết và các tình tiết rất hồi hộp hấp dẫn chứ không phải nhàn nhạt dễ đoán như sách cho trẻ em. Chất lượng in ấn của Đinh Tị thì khỏi bàn, sách lại còn trình bày đẹp nữa.
5
182813
2016-06-21 19:37:30
--------------------------
450747
4657
Câu chuyện mang màu sắc viễn tưởng vô cùng độc đáo. Cuộc hành trình của Gulliver ở xứ sở tí hon vô cùng thú vị. Anh ban đầu khiến cho những con người nơi đây sợ hãi, nhưng sau đó, anh đã chứng minh được năng lực, sức mạnh cũng như tài năng không chỉ về thể lực mà còn về suy nghĩ, ước muốn được sống trong hòa bình khi anh chủ động đưa ra ý kiến với nhà vua để thương thuyết với nước bạn đang trong giai đoạn căng thẳng của cuộc chiến tranh. Thực sự rất hay.
5
1420970
2016-06-19 09:11:57
--------------------------
417351
4657
Gulliver Du Ký của tác giả Jonathan Swift đúng là cuốn sách phải nằm trong top tác phẩm kinh điển dành cho thiếu nhi. Cuốn sách nói về cuộc phiêu lưu của Gulliver, anh lạc vào xứ sở người tí hon, rồi lại lạc vào xứ sở của người khổng lồ, anh chia sẻ cuộc sống với những nhân mã (người - ngựa),... cuộc phiêu lưu tràn đầy những điều bất ngờ, những niềm vui và tràn ngập cả sự hy vọng. Ẩn sâu nơi những câu chuyện ngụ ngôn đó là những suy ngẫm về cuộc sống, về con người, về nhận thức cuộc sống. Cuốn sách rất tuyệt.
4
746405
2016-04-16 20:43:37
--------------------------
417047
4657
Khi nghe tên Gulliver, chắc chắn tôi đã cảm thấy rất quen thuộc vì nó đã từng xuất hiện trong truyện tranh đô rê mon. Và khi đọc xong câu truyện đó tôi đã rất thắc mắc là Gulliver trong truyện mà Đo rê mon đã nhắc tới như thế nào, có thật là cao như vậy hay không nên khi tôi lên Tiki và thấy có tác phẩm Gulliver du kí tôi đã quyết định mua nó về và đọc. Khi đọc xong, tôi vẫn tự hỏi mình là phải chăng chính tác gỉa đã đi đến thế giới nhỏ bé ấy hay không. Một câu chuyện không những hay mà còn rất ý nghĩa hơn khi nó còn lên án, phê phán những sai trái trong xã hội của chúng ta 
5
782953
2016-04-16 07:40:21
--------------------------
413429
4657
Tôi biết tới Gulliver Du Ký qua những bộ phim hoạt hình của Gulliver Du Ký mà khi lớn mới có dịp đọc tác phẩm gốc. Riêng tôi thấy rằng tác phẩm này hay hơn phim vì đã lột tả được nhiều góc cạnh của xã hội nước Anh lúc bấy giờ chứ không bo trong tô hồng như phim. Hơn nữa tác phẩm này còn truyền cảm hứng phiêu lưu khám phá những chân trời mới nhằm tìm ra lẽ sống của cuộc đời. Chi tiết Gulliver chiến đấu với đội thuyền tí hon cũng là một hình tượng kinh điển ẩn dụ nhiều tư tưởng. Sách biên tập khá tốt bìa đẹp nhìn rất gợi sự tò mò
5
507362
2016-04-09 18:40:46
--------------------------
400375
4657
Sau khi đọc xong Gulliver Du Ký với Trà Hoa Nữ (cũng là sách kinh điển) tôi nghiệm ra một điều rằng không phải cứ sách kinh điển là hay, là xuất sắc. Nó có thể có giá trị bởi vì phù hợp với thời đại, và một khi đã nổi tiếng thì sẽ có cơ hội lưu truyền qua các thế hệ sau, còn khi mà nhiều cuốn sách có đột phá ra đời, sẽ làm mờ nhạt đi những nội dung bình thường.
Gulliver Du Ký quá dài dòng và rườm rà, tác giả phê phán xã hội Anh thời bấy giờ thông qua các chuyến phiêu lưu và lồng ghép vào mỗi khi dẫn bàn điều gì đó, nó quá thừa thãi và gây ra nhàm chán. Trong bốn cuộc phiêu lưu của Gulliver tôi chỉ ấn tượng với lần nhân vật đến hòn đảo do những con Ngựa làm chủ.
Có một bạn kể với tôi rằng cô ấy đọc một cuốn kinh điển thấy rất bình thường, nhưng vì là sách kinh điển và thấy ai cũng khen nên không dám chê. Tôi nghĩ đó là một vấn đề rất tai hại, việc không dám nêu lên ý kiến, quan điểm cá nhân sẽ khiến con người mắc bệnh phong trào nặng hơn, và dần mai một tư duy lẫn chính kiến của mình.
3
386342
2016-03-19 11:24:55
--------------------------
396154
4657
Gullivr du ký, một tác phẩm viết cho thiếu nhi nhưng dường như nó cũng viết cho tất cả mọi người, những ai thích phiêu lưu , khám phá, những ai thích bộc phá sự sáng tạo. Tác phẩm kể về những cuộc hành trình bí ẩn, kì là và cũng thật huyền ảo đến cái thế giới, đến cái xứ sở của người tí hon, người khổng lồ và bên cạnh đó tác phẩm cũng đã phán ánh xã hội với một thực trạng suy đồi dưới sự cai trị thời bấy giờ. Với lối kể chuyện sinh động, hấp dẫn người đọc, phải chăng ông chính là một trong số những nhân vật đó, đã từng đi qua những thế giới đó để rồi kể lại cho bạn đọc nghe với những điều đã trải qua. Và dù gì đi chăng nữa, thì Gulliver du kí thực sự là một trong những tác phẩm có thể nói là làm khơi nguồn trí tưởng tượng phong phú của trẻ thơ.
4
634723
2016-03-12 23:15:13
--------------------------
355568
4657
Nhìn bìa sách dễ thương và bắt mắt ghê luôn đó. Mình thích những cuốn sách có phần in ấn  và bản đẹp. Nội dung cuốn sách cũng khá thú vị, một cuộc phiêu lưu của tác giả tới hai vùng đất khác nhau. Một nơi thì toàn là người tí hon, một nơi toàn là người khổng lồ mà con chó còn to gấp mấy lần con người bình thường. Mình mua cuốn sách cho nhỏ em gái và nó rất thích thú với cuốn sách này. Hy vọng là Tiki sẽ giới thiệu thêm nhiều cuốn sách hay như thế này đến bạn đọc
5
936274
2015-12-20 11:18:31
--------------------------
354098
4657
Cuốn có bìa hình đẹp , óc hài hước cao , câu truyện đưa đến thế giới mạo hiểm , xa xôi qua những vùng miền chú bé đặt chân tới khám phá ra chính mình , từ đó quý trọng giá trị hiện thực , câu truyện nhiều chi tiết mang sự viễn tưởng cao nên người đọc cũng không bị nhàm chán , giống như xứ sở hùng vĩ hiện ra trước mắt nhằm mở ra tài nguyên vô hạn trên đời này , mãnh liệt , thôi thúc con người có tri thức cho cuộc sống .
4
402468
2015-12-17 12:29:22
--------------------------
351500
4657
Nội dung của sách thì không phải bàn nữa, dù mình chỉ mới đọc đến Gulliver ở xứ lulliput nhưng thấy cách tác giả mô tả mọi thứ rất chi tiết, mặc dù đã bỏ bớt rất nhiều theo lời mở đầu trong sách. Chữ và giấy đều tốt, đọc rất vừa mắt, sách nhẹ nên cầm rất thích. Nhưng điểm mình chưa hài lòng là chất lượng sách, quyển của mình khi vừa khui hộp đã có một vết ố vàng rồi, bên trong sách thì khoảng 30-40 trang đầu có nếp gấp, trang sách không thẳng nhìn rất khó chịu.Chắc do mình xui nên được tiki giao cho cuốn này. Hơi thất vọng.
4
404075
2015-12-12 12:27:01
--------------------------
341992
4657
Cuốn sách này vẫn luôn nằm trong tủ sách kinh điển dành cho thiếu nhi. Nhưng mình dù nay đã đi làm, tìm đọc lại (hồi nhỏ đã được xem qua rồi) vẫn thấy rất là thú vị và tuyệt vời. Cuốn sách đem lại những giây phút thư giãn sau một ngày làm việc mệt nhọc.

Bìa cuốn sách được thiết kế rất ổn. Vừa gây ấn tượng vừa gây tò mò cho người đọc khiến cho họ có cảm tưởng phải nhanh chóng nhảy vào và khám giá chuyến phiêu lưu này. Chất lượng giấy tốt, in ấn, biên tập OK. Nói chung là hoàn hảo !^^
5
712799
2015-11-23 15:58:12
--------------------------
336136
4657
Cuốn sách này đã từng được chuyển thể thành phim, rất nổi tiếng. Một trong những cuốn sách kinh điển của mọi thế hệ độc giả. Câu chuyện đưa người ta vào thế giới kì bí với trí tưởng tượng phong phú.Từ thế giới người tí hon đến thế giới người khổng lồ, rồi hòn đảo của những nhà bác học, ta sẽ được đi từ bất ngờ này đến bất ngờ khác. Thật sự rất khâm phục tác giả có thể có được những câu chuyện thật như thế! Bìa sách cũng rất đẹp, sách được chăm chút, một cuốn sách đáng được sưu tầm.<3 <3 <3 
4
395203
2015-11-11 22:46:06
--------------------------
242660
4657
Tác giả Jonathan Swift rất nổi tiếng với các tác phẩm giả tưởng phiêu lưu. Guilliver du kí mặc dù khi phổ biến với bạn đọc đã từ rất lâu qua sự đồng ý của cả 2 bên tác giả và nhà xuất bản- đã lược bỏ rất nhiều chi tiết rườm rà không cần thiết cho tác phẩm nhưng cá nhân tôi vẫn cảm thấy tác giả rất dài dòng trong công đoạn miêu tả các vùng đất mà nhân vật chính lạc tới. Bản thân tôi thấy nó quá lộ liễu khi dùng các chi tiết về người dân, bộ máy nhà nước, tính cách và nếp sống của các vùng đất để nói lên thực trạng suy đồi của xã hội thật thời bấy giờ.
3
292668
2015-07-26 20:54:34
--------------------------
237795
4657
mình biết đến gulliver qua sách tiếng việt  và truyện doremon. Khi lên tiki tìm thì thấy có bán truyện này nên mình mua về đọc, phải nói truyện này rất là hay. Cả bốn cuộc phiêu lưu của gulliver mình đều rất thích. Tác giả đã vẽ ra câ chuyên rất sinh động về xứ liliput ( xứ xở người tí hon ), xứ brobdingnag ( xứ xở người khồng lồ ), rồi đến laputa và houyhnhnm. Tác giả đã miêu tả  chuyện sinh động đến nỗi khi học xong mình tự hỏi có phải chăng tác giả đã thật đi đến những đất nước này. Không những thế tác phẩm này còn phê phán những cái sai trái của xã hội chúng ta, những tật xấu của con người, chỉ ra cái đúng cái sai. Đây thật dự là một tác phẩm tuyệt vời nó không chỉ là những cuyến phiêu lưu thông thường mà trong cuốn sách đó còn có những nhân xét về cuộc đời và con người nên mọi ngươi hãy mua về đọc đi rất hay đó :)
5
621863
2015-07-22 19:12:54
--------------------------
226541
4657
Mình đã từng xem phim, nghe kể về Gulliver nhưng cho đến khi đọc xong quyển sách này mới có thể cảm nhận trọn vẹn cái hay của câu truyện. Nội dung xoay quanh thuỷ thủ - bác sĩ Gulliver, một con người đam mê phiêu lưu, khám phá và những chuyến đi của anh đến các vùng đất. Từ xử sở của ngừoi tí hon đến vùng đất người khổng lồ, các nhà bác học, các houyh... Tất cả đều được tác giả khắc hoạ chi tiết, chính xác. Cuốn sách còn lên án những thói xấu của con người, châm biến nước Anh và chế độ cai trị thời bấy giờ. Mình thưc sự rất khâm phục trí tưởng tượng tuyệt vời của tác giả. 
4
582086
2015-07-12 13:28:20
--------------------------
217378
4657
Cuốn sách viết cho thiếu nhi nhưng lại dành cho mọi lứa tuổi!

Tinh thần yêu thích việc du ngoạn và phiêu lưu của con người là một nét nổi bật của tác phẩm này, trước cả khi phong trào du lịch và nền công nghiệp du lịch được hình thành, phát triển.

Chàng thủy thủ Gulliver phiêu lưu qua vương quốc của người tí hon rồi lại đến vương quốc của người khổng lồ. Chàng còn ghé thăm xứ sở của những nhà bác học nữa. Chúng ta hãy bước theo chân chàng, đảm bảo các bạn không dừng lại được đâu!

Cuốn sách có bìa được thiết kế ấn tượng. Nhìn đôi chân khổng lồ ở bìa sách là đã muốn khám phá nó ngay rồi. Sách được Đinh Tị in trên giấy creamy đẹp và nhẹ tênh. Mình luôn thích chất lượng sách của Amun – Đinh Tị.
5
598102
2015-06-29 11:07:14
--------------------------
201776
4657
Đây là một cuốn sách hết sức thú vị và vô cùng ý nghĩa. Gulliver Du Ký là cuốn sách của mọi lứa tuổi. Trẻ em tìm thấy ở đó những câu chuyện kỳ lạ, có người tốt, kẻ xấu, những kẻ tàn ác và cả những người nhân ái, ngay thẳng để từ đó phân biệt phẩm giá con người. Người lớn thì rút ra được những bài học về cách xử thế ở đời, làm sao để xử sự, hành xử cho đúng. Trong truyện có những câu chuyện phiêu lưu tưởng tượng kỳ thú khiến  tuổi nhỏ say mê.
4
614265
2015-05-28 11:07:25
--------------------------
185472
4657
Mình biết đến tác phẩm Gulliver du ký qua một tập truyện tranh Doraemon và kể từ đó mình đã xác định là phải sở hữu được tác phẩm này cho bằng được. Nội dung truyện rất thú vị khi kể về chàng bác sĩ - thủy thủ Gulliver phiêu lưu qua vương quốc tí hon rồi lại đến vương quốc của người khổng lồ hay chuyến ghé thăm xứ sở của những nhà bác học. Tất cả đều thể hiện ước mơ phiêu lưu, mong muốn thám hiểm những vùng đất mới của con người. Ngoài ra, truyện còn có ý nghĩa châm biếm xã hội nước Anh thời bấy giờ.
5
316903
2015-04-19 13:35:10
--------------------------
167432
4657
Tôi biết về Gulliver Du kí qua hai nguồn:
Thứ nhất là truyện tranh Đô rê mon với đường hầm thu nhỏ Gulliver.
Thứ hai là sách Đại cương du lịch có phần lịch sử du lịch, các nhà khoa học đề cập Guilliver Du kí để minh chứng tinh thần du ngoạn và phiêu lưu của con người trước thời du lịch phát triển.
Qua hai nguồn này mình cảm thấy rất tò mò và muốn đọc quyển Gulliver Du kí, những cuộc hành trình kì lạ, viễn tưởng nhưng vô cùng tuyệt vời của chàng Gulliver đánh những vùng đất kì bí. Chắc hẳn ai cũng có một thời, hoặc vẫn còn nuôi dưỡng những ước mơ phiêu lưu khám quá. Quyển sách này là một món quà tuyệt vời cho những ai có những giấc mơ đẹp như vậy.
5
515736
2015-03-14 20:21:33
--------------------------
