468152
5457
Cuốn sách thông minh cảm xúc thế kỷ 21 nói đến một vấn đề đang được nhất nhiều sự quan tâm đó là làm sao để xử lý cảm xúc. Bí quyết để cải thiện các mối quan hệ trong công việc và cuộc sống. Những điều này rất cần thiết đối với mỗi người trong công việc và cuộc sống.
Những quyển sách của TGM BOOK luôn có một chất lượng tuyệt vời, không thể chê vào đâu được. Bìa sách thiết kế rất bắt mắt, phù hợp với nội dung.  Giấy dày rất tốt, sờ đã cả tay :)) Bên trong có nhiều hình minh họa bắt mắt.
5
398235
2016-07-04 20:20:41
--------------------------
449511
5457
Mình rất thích TGM Book, có thể nói là một trong những đơn vị sách mà mình thích nhất. Cuốn này cũng của TGM Book nên mình mua về nghiền ngẫm thử, và nó không làm mình thất vọng. Đây không còn là thời đại của IQ, mà phụ thuộc rất lớn vào EQ. EQ điều khiển cảm xúc, tâm trạng của con người, là nguồn động lức để làm việc, biết nhận thức để vươn lên. EQ giúp làm chủ các bản thân và các mối quan hệ, hòa nhập với xã hội và đó là 1 điều rất cần thiết ngày nay.
5
452822
2016-06-17 16:02:05
--------------------------
436233
5457
Tiki giao hàng đóng gói cẩn thận....Nhìn cuốn sách khá là bắt mắt, giấy tốt. Ở ngoài trước khi đọc còn có giấy bóng làm cho cuốn sách trở nên sang trọng hơn. Mình được thày dạy học giới thiệu nên mua cuốn này đọc thử, sách nói về cách quản lý EQ cũng cho tốt...Nắm rõ được cảm xúc ắt hẳn mới đi đến thành công lâu dài được. mình nghĩ nếu các bạn chưa đọc thì hãy nên đọc nó để có thể rèn luyện cũng như được học tập thêm các kỹ năng khác để giúp mình đi đến thành công hơn trong tương lai
4
849887
2016-05-26 13:39:00
--------------------------
397776
5457
Thông thường khi làm bài test đầu vào cho một số công ty, người ta hay check IQ. Hiện nay, rất nhiều công ty bắt đầu có thêm phần trắc nghiệm về EQ. Nếu IQ về tính logic, thì EQ là thiên về cảm xúc. Để làm nhân viên giỏi, IQ góp phần cao, nhưng để làm một người lãnh đạo, EQ là phần rất quan trọng chiếm đa số. Sách giúp ta khái quát được các cách để nâng cao kỹ năng nhận thức, làm chủ cảm xúc và các mối quan hệ xung quanh. Nên có những ví dụ cụ thể hơn, để giúp người đọc hiểu rõ chi tiết mà áp dụng vào đời sống thực tiễn. Nói chung, đánh giá vẫn là 4/5 cho nội dung như thế này.
4
861781
2016-03-15 13:51:00
--------------------------
362567
5457
Từ khi mua cuốn sách này trên tiki tôi đã đọc xong  chỉ trong 1 ngày và nhận thấy rằng trong cuộc sống EQ cũng quan trọng không kém gì IQ, cảm xúc là thứ có thể cho là rất khó kiểm soát nếu như ta không hiểu về việc tại sao nó lại xuất hiện. Qua quyển sách này đã đưa ra các gợi ý để ta suy nghĩ về cảm xúc ảnh hưởng thế nào đến hành động và suy nghĩ mỗi người. Thế nên việc đọc quyển sách này cũng là một cách hướng dẫn ta nên làm gì nếu như những cảm xúc của ta là đúng hay sai. Bìa sách được thiết kế bắt mắt, nội dung rõ ràng nên cũng dễ đọc. Thật sự là một trong những cuốn sách gối đầu của thế kỷ 21.
5
435343
2016-01-02 10:51:32
--------------------------
345976
5457
Mình đi làm được 3 năm. Áo lực công việc và cuộc sống làm mình bất cần, tiêu cực muốn nhảy việc... Khi đọc cuốn sách này mình nhận ra là  chỉ số EQ của mình quá thấp. Cũng đang học cách điều chỉnh cảm xúc như sách dạy, chưa biết có thành công như những người có chỉ số EQ cao như trong sách ví dụ không. Nhưng ít nhất nếu bớt nóng giận tăng được chỉ số EQ cao lên tí thì cũng tốt cho sức khỏe (vì theo đông y tức giận làm tổn hại gan mà ^^)
4
441437
2015-12-01 17:57:43
--------------------------
337610
5457
Khi bạn tiếp xúc với một người có EQ cao tự nhiên bạn sẽ thấy vừa lòng, không khó chịu, rất yêu mến người đó
Đây thực sự là cuốn sách hay, trong thời đại mà IQ không phải là tất cả nữa, khi bạn thu phục được lòng người thì gần như bán sẽ gần hơn 1 bước đến đến với thành công
Tuy nhiên sách này chưa thật sự thu phục hoàn toàn minh nên mình đánh giá 4 sao
 Bạn  biết đấy bạn có thể nâng cao được EQ vì quyển sách ngoài làm rõ vị trí quan trọng của EQ. Theo mình thì IQ+EQ+quyết tâm= tahnfh công
 
4
604569
2015-11-14 12:59:40
--------------------------
321690
5457
Mình thấy một điều ở những tác phẩm thể loại kĩ năng sống là hầu hết những tác phẩm hay được độc giả yêu thích lại là những tác phẩm của tác giả nước ngoài,viết ra và nhận được nhiều phản hồi tích cực. Cuốn sách là những chia sẻ kinh nghiệm, bài học thực tế của tác giả về kĩ năng sống đặc biệt là cảm xúc giúp chúng ta vượt qua thử thách. Cuốn sách thực sự rất hay, rất phù hợp với tư duy và nhận thức người việt nam. Thế hệ trẻ nên học hỏi và tiếp thu những điều bổ ích trong cuốn sách.
5
720797
2015-10-14 17:13:45
--------------------------
311175
5457
Người ta chỉ mới quan tâm về Chỉ số cảm xúc mới đây thôi chứ trước đây chúng ta không bận tâm lắm tới EQ, thứ giúp chúng ta sống hạnh phúc hơn nhiều. 
Bỏ thời gian ra tìm hiểu nó và học cách nâng cao chỉ số EQ là công việc quan trọng trong quá trình xây dựng và cải tạo bản thân. Chưa có trường lớp dạy làm sao thông minh hay làm sao giàu cảm xúc và biết đánh giá cảm xúc người khác để phản ứng cho phù hợp thì phải tự đọc sách vậy. 
Mình thích bộ này. 
5
535536
2015-09-19 21:57:11
--------------------------
308887
5457
Ngoài chỉ số sáng tạo CQ thì chỉ số EQ cũng hết sức quan trọng trong xã hội hiện nay, nó quyết định tới mức độ thành công trong cuộc sống của mỗi người chúng ta.Cuốn sách giúp chúng ta rèn luyệ những kĩ năng quan trọng trong sự nghiệp và cuộc sống.Cuốn sách chứa những phương pháp hiệu quả để chúng ta rèn luyện kỹ năng tự nhận thức bản thân, kỹ năng làm chur bản thân,kỹ năng nhận thức xã hội và kỹ năng làm chủ các mối quan hệ xung quanh.Những kỹ năng này giúp chúng ta tự tin hơn trong cuộc sống và làm cho cuộc sống trở nên thú vị hơn!
5
483530
2015-09-18 21:27:29
--------------------------
298300
5457
Lúc mua sách mình hy vọng khá nhiều, nhưng sau khi đọc lại thấy hơi... buồn ngủ. Sách viết có phần giáo điều và khá lý thuyết. không biết có phải do cách dịch hay không mà mình thấy cách sử dụng từ ngữ hơi xa cách thực tế. 1 cuốn sách về kỹ năng mà lại có quá ít ví dụ thực tiễn, Theo quan điểm của mình thì sách hơi... mỏng cho khối lượng nội dung như vậy. tác giả đặt vấn đề và đưa ra khá nhiều luận điểm nhưng lại viết quá ít ở mỗi vấn đề.
3
623870
2015-09-12 16:37:05
--------------------------
297616
5457
Sách về EQ không còn quá xa lạ với bạn trẻ ngày nay nhưng sách do TGM xuất bản luôn có một chỗ đứng khác biệt về chất lượng so với các nhà phát hành khác. Cuốn sách là nguồn cảm hứng, động viên mỗi khi mình đối mặt với những người có IQ vượt trội so với mình, bạn sẽ bị thuyết phục hoàn toàn với những câu chuyện mà sách truyền đạt và hiểu vì sao EQ có vai trò, lợi ích, tiềm năng,.. hơn nhiều IQ. 
Không có gì thú vị hơn việc trở nên thành công hơn những đứa đầu to học giỏi luôn đứng đầu lớp nhỉ, chỉ đơn giản thực hiện theo những quy tắc làm chủ EQ

5
565623
2015-09-11 23:27:47
--------------------------
296001
5457
Quyển sách là một cẩm nang những lời khuyên có ích tới tất cả chúng ta, những con người dù đang làm bất cứ ngành nghề nào. Chỉ số EQ là chỉ số quan trọng quyết định thành công của bạn dù bạn đang làm trong lĩnh vực gì. ''Thông minh cảm xúc thế kỉ 21''  đã chỉ rõ từng bước đi, cách thực hiện nhằm cải thiện chỉ số EQ của mình. Bìa sách ấn tượng, giấy dày, sáng, chữ rõ ràng. Nói chung đây là quyển sách đáng để đọc. Tôi hài lòng với quyển sách này. Cảm ơn
5
620043
2015-09-10 20:23:17
--------------------------
290797
5457
Cuốn sách sẽ chỉ cho bạn những phương pháp để bạn làm chủ cảm xúc của mình hơn, để từ đó bạn sẽ có được một cuộc sống chất lượng và ý nghĩa hơn rất nhiều. 
Bạn sẽ biết rằng bấy lâu nay bạn hành xử theo cảm xúc của mình (đa số nhiều người đều như vậy, không riêng gì mình bạn).
 Nhưng giờ chính bạn phải "bắt" cảm xúc phục tùng mình và sứ mệnh của nó là giúp mình đạt được những thành công trong cuộc sống.
Một cuốn sách súc tích về EQ và dễ dàng ứng dụng cho bản thân! 
4
649011
2015-09-05 19:49:49
--------------------------
288817
5457
Thông minh cảm xúc thế kỉ 21 được trình bày đẹp, rõ ràng. Sách nêu rõ được các biểu hiện của trí tuệ cảm xúc và các phương pháp làm chủ nó, nhiều ví dụ minh họa cụ thể, thuyết phục. Trong thế giới hiện đại, rõ ràng chỉ số thông minh (IQ) không còn chiếm vị trí độc tôn nữa mà trí tuệ cảm xúc (EQ) đang dần được xem trọng hơn, nó giải thích vì sao những người có IQ không cao mà vẫn thành công. Mình xem nó như kim chỉ nang trong cuộc sống, đặc biệt phù hợp cho những bạn trẻ chưa kiểm soát được cảm xúc của mình.
5
561552
2015-09-03 21:58:48
--------------------------
279721
5457
Khả năng về trí thông minh cảm xúc EQ chiếm đến 80% sự thành bại sau này của con người trong cuộc sống. Không giống như IQ, EQ có thể dễ dàng luyện tập và nâng cao. Tác giả phân rõ từng mục, từng chương với một loạt những kĩ năng cần thiết trong mỗi chương sẽ cho người đọc một tầm nhìn rộng hơn về nhưng gì đơn giản diễn ra hằng ngày mà ta tưởng chừng như đã biết hết rồi. Đương nhiên đọc một cuốn sách xong rồi để đấy và quên đi không thể giúp ta cải thiện được EQ, chừng nào bạn chủ động thực hành những gì được đề cập trong sách thì bạn mới có thể nâng cao được EQ của mình.
5
627565
2015-08-27 06:25:07
--------------------------
268416
5457
Thông Minh Cảm Xúc Thế Kỷ 21 đúng là một cuốn sách đáng để đọc nếu bạn muốn thay đổi tích cực về EQ ( thông minh cảm xúc) của mình! Bạn nâng cao kỹ năng tự nhận thức, làm chủ bản thân, nhận thức xã hội, làm chủ mối quan hệ thông qua các phương pháp mà tác giả Travis Bradberry & Jean Greaves đã giới thiệu trong cuốn sách này. Trong cuốn sách này tôi rất tâm đắc nhất với phương pháp "đối diện với cảm giác khó chịu - Thay vì tránh né cảm xúc, mục tiêu của bạn phải là chủ động tìm đến cảm xúc ấy, đối mặt với nó, và cuối cùng là vượt qua nó" nghe qua thì hơi khó để thực hiện, thế nhưng bạn áp dụng phương pháp này bạn sẽ dập tắt ngay những cảm giác tiêu cực đi! Ngoài ra bìa sách khá đẹp, trình bày rất OK, rõ ràng dễ đọc, chất lượng mực, giấy của TMG Books thì khỏi chê!
4
504087
2015-08-16 13:43:47
--------------------------
262775
5457
Quyển sách đúng là giống như 1 người thầy dạy Yoga vậy. Nó đòi hỏi sự tĩnh tâm, lắng đọng, nhẹ nhàng trong việc hít thở, quan sát và chuyển động từ từ, chầm chậm để đạt tới sự cân bằng bản thân, cân bằng cảm xúc. Đọc thì có vẻ đơn giản nhưng để uốn dẻo và điều khiển được những mạch cảm xúc nhanh-chậm của tâm hồn thật không dễ chút nào, nó đòi hỏi sự kiên trì và luyện tập mỗi ngày. Cuốn sách sẽ là người bạn đồng hành tốt của bạn.
5
281150
2015-08-12 09:17:01
--------------------------
255002
5457
Mình thấy tên sách lạ và bìa cũng lạ lạ nên mua thử về đọc,bình thường mình toàn đọc sách về  trí tuệ cũng như là động lực sách nên khi thấy cuốn sách này nói về chỉ số cảm xúc mình cảm thấy mới lạ,đầu tiên sách của TGM mình rất hài lòng về hình thức,thứ hai nôi dung quá tuyệt vời,sách chỉ cho chúng ta làm chủ được cảm xúc của mình trong mọi hoàn cảnh,sách đọc dễ hiểu và rất thực tế,sách khá mỏng nhưng mà chất lượng với giá như vậy thì hoàn toàn xứng đáng.Tóm lại mình rất thích cuốn sách này.Hiện nay chỉ số EQ còn quan trọng hơn cả IQ và mình tin rằng thành công hay không là do bạn có gặp được người "thầy" tốt hay không.5 sao cho cuốn sách 
5
48148
2015-08-05 17:44:23
--------------------------
247909
5457
Trong cuộc sống trí thông minh là một phần quan trọng nhưng trí tuệ cảm xúc lại góp phần mang đến nhiều thành công cho một người. Bản thân tôi là một người nhạy cảm, đôi khi tôi mệt mỏi vì chẳng thể quản lý được cảm xúc của mình làm mọi chuyện cứ đưa vào bế tắc, vì vậy tôi luôn mong muốn chỉ số EQ của mình được cải thiện vì nó là chìa khóa cho sự thay đổi cuộc đời. Thật sự mà nói khi đọc cuốn sách này tôi vỡ lẽ ra được nhiều thứ, nội dung truyền đạt khá hay, hơn nữa lại đưa ra những phương pháp hữu hiệu. Tôi nghĩ tất cả mọi người nên đọc cuốn sách này để chúng ta có thể hiểu thêm cũng như rèn luyện trí tuệ cảm xúc của mình để nó có thể mang lại được nhiều thành công cho mỗi người!
4
411681
2015-07-30 16:58:12
--------------------------
242057
5457
Trong thời đạt mới, IQ không còn được xem trọng như EQ nữa, cảm xúc là chất xúc tác mạnh mẽ nhất cho toàn bộ hoạt động thể xác và trí não của con người. Người thành công chưa chắc đã có IQ cao nhưng chắc chắn người đó có EQ rất cao. Đó mới là chìa khóa vững chắc cho mọi thành công, giúp con người sống tốt hơn, ứng xử thông minh và biết cách gây thiện cảm với người khác. Một cuốn sách đáng mua!
Cảm ơn Tiki và tác giả đã đem đến một tác phẩm hay cho người đọc.
5
186614
2015-07-26 07:10:46
--------------------------
172678
5457
Mình thấy Thông Minh Cảm Xúc Thế Kỷ 21 là một cuốn sách mà bất kì bạn trẻ nào cũng nên đọc. Sau khi đọc nó, mình đã suy nghĩ mọi chuyện dù là xấu nhất một cách tích cực hơn, loại bỏ nhũng cảm xúc tiêu cực chi phối thái độ và hành động để thêm tự tin và không nản lòng khi gặp khó khăn.Cuốn sách cũng giới thiệu rất nhiều phương pháp khoa học như nâng cao kỹ năng tự nhận thức, rèn luyện kỹ năng làm chủ bản thân để trở thành hiệu ứng đám đông, tăng cường kỹ năng làm chủ mối quan hệ xã hội. Mình thấy đây là một cuốn sách hay, bổ ích và hữu dụng, nên đọc nhiều và suy ngẫm. Về hình thức, bìa sách đẹp, tựa đề vừa nghe đã muốn đọc và chất lượng giấy in dày dặn, cách trình bày các vấn đề cũng rõ ràng, mạch lạc, không quá khó hiểu !
4
485671
2015-03-24 15:19:39
--------------------------
171896
5457
Trong bản 2.0 này (phiên bản mới), tác già nghiên về phương pháp hơn là lý thuyết trong quyển sách Thông minh cảm xúc để hạnh phúc và thành công. 66 phương pháp thực tiễn sẽ giúp chúng ta tự nhận thức, làm chủ bản thân, nhận thức xã hội và làm chủ mối quan hệ - những điều quan trọng trong trí tuệ cảm xúc. Trí tuệ cảm xúc là một kỹ năng đặc biệt quan trọng để thành công trong sự nghiệp và cuộc sống. Tôi không biết chỉ số IQ của mình có cao hay không vì chưa test nhưng thật tuyệt khi biết rằng trong nhiều lĩnh vực EQ còn quan trọn hơn cả IQ và không giới hạn thành công của bản thân nếu IQ trung bình. Đây là quyển sách hay đáng để đọc.
5
387632
2015-03-22 21:29:43
--------------------------
148042
5457
Theo xu hướng bây giờ chỉ số thông minh cảm súc sẽ rất được coi trọng.Và nếu bạn muốn nâng cao trình tư duy cảm súc EQ của mình thì hãy đọc cuốn này tuy phải công nhận là cuốn sách này khá mỏng và hơi đắt nhưng các vấn đề được trình bày trong nó rất súc tích và cô đọng.Sách in thì chất lượng thôi rồi,có hình minh họa rõ ràng và giấy xịn nha.Dịch giả cuốn sách này là anh Khoa và chị Vy-những người khá thành công và dịch rất sát nghĩa ,dễ hiểu.Mình tin là sẽ chẳng còn bao xa đâu các bài kiểm tra EQ sẽ thống lĩnh,làm mờ đi các bài kiểm tra IQ thông thường như hiện nay.Và cuốn sách này thực sự hữu ích với bạn...
5
456366
2015-01-09 17:21:04
--------------------------
144406
5457
Theo bạn một người thành công có những yếu tố gì? Chắc chắn điều đầu tiên bạn nghĩ đến là người đó phải có chỉ số IQ cao. Nhưng thực tế khi đánh giá toàn diện một con người ngoài IQ còn phải dựa vào EQ và tính cách riêng của mỗi người. Một người thành công chưa hẳn chỉ số IQ đã cao. Có rất nhiều người thành công nhưng chỉ số IQ chỉ ở mức trung bình nhưng  chắc chắn rằng họ sẽ có chỉ số EQ rất cao. Không giống với IQ và tính cách sinh ra đã được quyết định, EQ có thể thay đổi và phát triển cũng như ngày càng được nâng cao nếu như có sự học tập và rèn luyện. Như vậy, cách để có được thành công là rèn luyện nâng cao chỉ số EQ của bản thân !!! Đừng lo lắng làm thế nào, bằng cách nào bạn có thể nâng cao được EQ vì quyển sách ngoài làm rõ vị trí quan trọng của EQ trên con đường thành công, còn cho ta những bí quyết rèn luyện để nâng cao trí tuệ cảm xúc (EQ) của chính mình. Cuối cùng, đây là quyển sách rất hay và ý nghĩa, phù hợp với tất cả mọi người. Đồng thời cũng không quên gửi lời cảm ơn TGM Books đã dịch quyển sách rất hay này !!!
5
385726
2014-12-26 18:14:40
--------------------------
139255
5457
Đây thực sự là cuốn sách rất hay, chắc chắn sẽ không làm bạn thất vọng. Trong thời đạt mới, IQ (chỉ số thông minh) không còn được xem trọng như EQ (chỉ số thông minh cảm xúc) nữa, cảm xúc là chất xúc tác mạnh mẽ nhất cho toàn bộ hoạt động thể xác và trí não của con người. Người thành công chưa chắc đã có IQ cao nhưng chắc chắn người đó có EQ rất cao. Ở cuốn sách này, Chỉ với 160 trang ngắn ngủi, tác giả đã chỉ ra 66 phương pháp giúp bạn rèn luyện những kỹ năng trí tuệ cảm xúc như kĩ năng tự nhận thức, kĩ năng làm chủ bản thân, kĩ năng nhận thức xã hội, kĩ năng làm chủ mối quan hệ...., Sẽ là bộ công cụ trợ giúp đắc lực giúp bạn vượt qua khó khăn và đạt nhiều mục tiêu trong cuộc sống.
5
414919
2014-12-06 15:16:28
--------------------------
