527949
6085
Sách quá hay, em mới học cấp 2 nên sau khi đọc xong cuốn sách này em nghĩ rằng chắc cấp 3 mình cũng như vậy chứ! Mong rằng em cũng được học trong một lớp học giống Nguyên. Cách viết vui nhộn của tác giả và cuối cùng là câu : " Ngoài kia hoa nắng đang xôn xao ." Đặc biệt là bìa sách rất đẹp nhưng em vẫn thắc mắc là người ngồi trên võng là Nguyên với Chương hay là với Vũ , em cũng đã mua tác phẩm " Quê Ngoại " của tác giả rồi và nó cũng rất hay , nói chung là quyển truyện này rất tuyệt ! 
5
1444608
2017-02-19 16:30:04
--------------------------
402161
6085
Truyện đúng kiểu những câu chuyện học trò ngốc xít thường đăng trên các tờ báo học trò, dễ thương nhưng chưa thực sự ấn tượng. Đó là câu chuyện về cuộc sống bình dị của những học sinh đang tuổi ẩm ương. Từ ôn thi cấp 3, vào cấp 3, phải học xa nhau, bạn thân cảm nắng ai đó và cảm giác cô đơn khi "nó" bỗng dưng có người quan trọng, có lẽ ai cũng từng trải qua những điều này. Tuy vậy cách xây dựng tình huống và đối thoại trong truyện chưa có điểm nhấn sáng nào, chỉ dừng ở mức khá đáng yêu thôi.
3
968032
2016-03-21 21:01:00
--------------------------
158910
6085
Đây là một câu chuyện rất hay và thú vị. Tôi tìm thấy mình trong đó qua Nguyên. Có nhiều lúc thấy đứa bạn thân của mình nói chuyện vui vẻ với đứa khác, lại buồn và suy nghĩ lung tung. Nhưng sau đó lại thấy mình thật trẻ con, chỉ muốn giành lấy đứa bạn cho riêng mình. Tôi còn học được từ những nhân vật trong truyện về sự tự giác, đoàn kết và giúp đỡ nhau để tình bạn thêm gắn bó. Giọng văn mộc mạc, hóm hỉnh, vui tươi và nội dung trong sáng rất phù hợp cho lứa tuổi mới lớn. Tôi nghĩ người lớn cũng nên tìm đọc để hiểu thêm về tâm lý con mình.
1
439061
2015-02-14 11:25:27
--------------------------
