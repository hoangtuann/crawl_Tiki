296966
6511
Nhiều người trên chia sẻ với mình cuốn sách Em Phải Đến Harvard Học Kinh Tế rất hay. Mình quyết định mua khi Tiki giảm giá. Mình thấy Lưu Vệ Hoa là bà mẹ tuyệt vời, bà hi sinh thời gian quan tâm dạy con rất khoa học. Đây là cuốn sách bổ ích các mẹ nên đọc. Mặc dù không dám mong con mình được như con tác giả nhưng mình sẽ cố gắng dành nhiều quan tâm hơn để dạy con ngoan và giỏi. Cảm ơn tác giả đã chia sẻ kinh nghiệm hay đến độc giả. Thích Tiki giảm giá những cuốn sách hay.
4
437659
2015-09-11 14:17:22
--------------------------
119725
6511
/mình được chị tặng cuốn sách này vào tháng lương đầu của chị ấy, phải nói là chỉ mới đọc mở đầu thôi mình đã thấy rất ấn tượng. Cuốn sách kể về quá trình mẹ của LDĐ nuôi dạy cô, rèn luyện cô theo một đường đi rõ ràng để cuối cùng đạt được mục đích tốt đẹp đã đề ra. Đối vs những bà mẹ có con thơ nếu đọc được cuốn sách này chắc chắn sẽ rất bổ ích, còn vs mình nó lại như một động lực để mình nhìn nhận lại bản thân và cố gắng học tốt cũng như sau này có thể giáo dục một đứa con như thế.
Tuy là cách viết kiểu như viết kí theo từng khoảng thời gian nhưng nó k gây ra sự nhàm chán mà còn tạo cảm giác rất hứng thú khi đọc. Cuối sách còn có cả hướng dẫ để thi IELTS. Đúng là một cuốn sách đáng đọc.
4
166271
2014-08-07 10:14:16
--------------------------
