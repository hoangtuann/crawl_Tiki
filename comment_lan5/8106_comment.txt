295430
8106
“Kể chuyện mẫu giáo: Vì sao vịt con không biết hát?” là một câu chuyện rất thú vị, được kể với ngôn từ trong sáng, dễ hiểu, phù hợp với trẻ thơ, những nhân vật trong truyện cũng rất đáng yêu, chắc chắn bé sẽ rất thích. Đọc truyện này, bé sẽ biết được rằng đôi chân của vịt có màng xòe như mái chèo nên vịt bơi rất giỏi. Ngoài ra mỗi loài vật đều có những lợi thế riêng của mình, sơn ca thì hát rất hay trong khi vịt hát rất dở, nhưng vịt lại bơi rất giỏi, cứu được bạn gà con suýt chết đuối, còn sơn ca lại không biết bơi gì cả. Ai cũng có sở trường riêng, và chỉ cần phát huy sở trường đó để trở nên có ích cho đời là đủ. Với bộ sách này, phụ huynh nên mua trọn bộ về cho bé đọc.
5
332766
2015-09-10 11:06:28
--------------------------
271553
8106
Bộ sách này mình đi siêu thị sách cũng thấy khá lâu rồi, kẹt nổi là hôm đó không có đủ trọn bộ nên mình cũng không mua. Hôm sau thấy trên Tiki bán trọn bộ, lại còn được giảm giá nữa nên đặt mua ngay và luôn. Ưu điểm của cuốn sách này nói riêng và bộ sách này nói chung là hình ảnh rất bắt mắt, trình bày dễ thương, thu hút được sự chú ý của các bé, khi nhìn lên hình là các bé có thể phân biệt được rõ ràng con gà hình dáng ra sao, kêu cục ta cục tác thế nào. Bé cháu nhà mình khi đọc đến truyên này xong, mình bày cho bé kêu cạp cạp như con vịt, và hỏi ngược lại cháu vì sao vịt con không biết hát, bé có thể giải thích được y như truyện nói luôn. 
Nói chung là bộ sách này khá thú vị, nên mua về trọn bộ luôn!
5
104897
2015-08-19 10:46:37
--------------------------
