468970
8373
Tôi đã mua cuốn “Vì Sao Đà Điểu Chạy Nhanh ” vì yếu tố đầu tiên: giá thành hợp lý,  sau đó là hình ảnh đẹp, và chất lượng giấy rất dày, cầm trên tay sản phẩm khá thích. Đó là ưu điểm nổi bật của quyển sách . Khuyết điểm: với tiêu đề như trên, cái tôi trông chờ đầu tiên là sự giải thích vì sao động vật này có khả năng chạy tốc độ cao thế, thay vì “Động vật nào giống người nhất”. Dẫu biết rằng, quyển sách trình bày và giải đáp nhiều câu hỏi lý thú khác nhau, nhưng bố cục sách nên bắt đầu với câu hỏi trang bìa - câu hỏi mà đã khiến độc giả chọn mua quyển sách này thay vì những quyển sách khác. Tóm lại, tôi vẫn hài lòng khi mua sách.
4
966571
2016-07-05 20:50:11
--------------------------
393638
8373
So với những quyển bách khoa thư thiên nhiên dày cộm thì quyển bách khoa thư tí hon này lại dễ thở hơn nhiều đối với lứa tuổi mẫu giáo. Chỉ vỏn vẹn 5 câu chuyện về 5 loài vật khác nhau trong tự nhiên nhưng không vì thế mà quyển sách nhàm chán. Ngược lại, bằng những lời văn súc tích, hình ảnh minh họa chân thật, không cường điệu quá như trong hoạt hình hay truyện tranh, lại giúp các bé có cái nhìn thực tế hơn về hình dáng cũng như giải thích đặc điểm sống trong tự nhiên của các loài thú hoang dã.
5
80783
2016-03-09 03:06:05
--------------------------
