296218
3768
Mình thích mua truyện trên Tiki, cả truyện tranh nữa nhưng mình thấy Dragon Ball và Yaiba mình muốn mua lại không đủ bộ, cứ còn tập này lại thiếu tập kia trong khi truyện mới tái bản một loạt. Mặc dù nội dụng truyện mình đã thuộc nằm lòng nhưng khi NXB Kim Đồng tái bản bộ này nguyên gốc giống bộ bên Nhật thì mình quyết định phải sưu tầm bộ này bằng được. Ở tập 27 này, Frieza (mình cứ quen gọi là Fida) sau khi biến thân 2 lần và hạ gục Vegeta (Cadic) làm mình lo lắng cho tương lai Trái Đất cũng như hành tinh Namex biết bao, Goku phẫn nộ quyết tâm tiêu diệt bằng được Frieza, truyện tranh diễn biến chậm, mình thích coi tranh đánh nhau ì xèo thôi. Truyện hay quá chừng khiến tuần nào mình cũng mong truyện ra lò để sưu tập đủ bộ!
4
68108
2015-09-10 22:27:49
--------------------------
258608
3768
Sau bao chờ đợi của độc giả qua những lời thoại trong câu chuyện,thì ở tập này cuối cùng siêu saya trong truyền thuyết đã xuất hiện. Và siêu saya đó là Songoku thì quá là tuyệt vời luôn :3. Đây là một ngoặc của câu chuyện để mở ra một chương mới để gặp những đối thủ mạnh hơn sau này như Cell, ma bư..xa hơn thần hủy diệt bill. Điều làm mình thích thú khi đọc dragon ball là mạch truyện hấp dẫn lôi cuốn rõ ràng và rất dễ hiểu. Quả không hổ danh là tác phẩm manga hay nhất mọi thời đại :3
5
204594
2015-08-08 17:41:50
--------------------------
226098
3768
Mình thích Dragon Ball từ lúc xem phim hoạt hình hồi nhỏ, đành nhau rất đã. Nhưng tiết là để vuột mất cơ hội sở hữu 1 bộ manga hay. Nên khi Nxb tái bản bộ này với một diện mạo hoàn toàn mới, mình đã phải cám ơn vì mình đã không mua bộ cũ. Chất lượng bản này tuyệt: giấy xốp, bìa rời, không scan hoàn hảo. dịch thuật chỉnh chu (tuy nhiên,có vẻ không so sánh được với One Piece ở 1 số chỗ như: 1 số trang giấy đậm, bị đen, bìa rời nhiều khi không ôm sát làm cho nó bị sục xịch nhô ra). Nhưng có được 1 trong những bộ manga tuyệt đỉnh nhất, chất lượng cao hơn, nếu bỏ qua những thiếu xót nhỏ không đáng có thì mình thấy khá hay.
4
166935
2015-07-11 14:12:14
--------------------------
