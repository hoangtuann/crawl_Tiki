548321
3201
Cuốn sách có các phần, các mục rõ ràng, có các tasks kèm theo lời hướng dẫn bằng Tiếng Việt rất mạch lạc, dễ hiểu, dễ học. Cuốn sách này thích hợp cho cả người bắt đầu tìm hiểu để học và cho những ai muốn thực hành, luyện tập nâng cao band
5
1128154
2017-03-20 13:23:44
--------------------------
446799
3201
Tôi là người đang theo học chứng chỉ Ielts và cảm thấy khá khó khăn về khoản Writing. Tôi cũng đã tìm đọc khá nhiều quyển sách chuyên đề về kỹ năng này và cảm thấy đây là quyển dễ học nhất với mình. Quyển sách được chia theo từng mục tương ứng với các phần có trong 1 bài thi Writing, các hướng dẫn, ví dụ và cấu trúc câu độc đáo trong từng phần đó đều được trình bày mạch lạc, rõ ràng. Ngoài ra, các đề tự ôn cũng có đáp án tham khảo khá hay đằng sau. Tóm lại, nếu bạn gặp khó khăn về kỹ năng viết hoặc mới thực hành kỹ năng này đều có thể sử dụng IELTS Writing For Success - Academic Module như 1 bước đệm cho mình.
5
383229
2016-06-12 21:58:24
--------------------------
