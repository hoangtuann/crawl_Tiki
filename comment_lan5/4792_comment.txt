206232
4792
Khổ to hơn mình nghĩ rất nhiều, còn to hơn cả sách giáo khoa nữa để bé thoả thích tô màu mà không sợ lem ra ngoài . Tổng cộng có 24 trang kể về nàng tiên cá Merliah xinh đẹp thích lướt ván nhưng luôn bị mụ Eris độc ác tìm mọi cách hãm hại. Nhưng đến cuối cùng nàng cũng đánh thắng được mụ Eris độc ác . Phía bên trên là câu chuyện còn phía bên dưới là hình để các bé tô màu. Không chỉ vậy bé còn được tặng kèm 2 trang hình dán nữa, tha hồ mà tô, dán .
5
62182
2015-06-09 09:11:31
--------------------------
165914
4792
Tuổi thơ chắc hẳn ai cũng quen thuộc với ba từ "nàng tiên cá"... nó không chỉ là sự thích thú của mọi trẻ con Việt Nam mà hầu như nó thu hút trẻ con của toàn thế giới. Có lẽ đứa trẻ nào cũng mong ước được thấy "nàng tiên cá" hay kì diệu hơn thế nữa là ước mơ mình trở thành "nàng tiên cá"... Nghe thật ra rất ngớ ngẩn...nhưng nghĩ kĩ thì nó thật có lý đối với "những tâm hồn không có giới hạn"... Với sách tô màu babier - Nàng Tiên Cá, trẻ con sẽ tha hồ đọc truyện và tô màu thỏa thích những "nàng tiên cá" mình từng rất thích! Sách được bố trí rất đẹp mắt và có hình dán cho trẻ tự do sáng tạo.
5
558557
2015-03-11 15:02:44
--------------------------
