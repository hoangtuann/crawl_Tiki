516897
5731
Cách hành văn nhạt nhẽo, những câu chuyện quá tầm thường,rất thất vọng khi đọc từ những trang đầu
1
460197
2017-01-31 16:32:04
--------------------------
328473
5731
Quyển sách này bìa đẹp đấy, màu sắc đẹp đấy nhưng bên trong nội dung, cốt truyện chỉ để đọc chơi những lúc rảnh rỗi thôi, đọc xong không lắng được gì cả. Không sâu sắc mà cũng không hóm hỉnh quá đà, đây giống như là sự pha trộn, một thứ hỗn hợp. Đọc chơi thì được chứ để đọc suy ngẫm thì còn tệ lắm.. Dù sao đối với tác giả trẻ như vậy là ổn rồi. Khuyến khích tác giả viết hay hơn, sâu lắng hơn. Rất ủng hộ và tin tưởng tác giả sẽ có nhiều quyển hay hơn thế này.
4
385726
2015-10-29 21:02:13
--------------------------
288356
5731
Sách bìa khá đẹp và bắt mắt, những tác giả này chắc đa phần còn trẻ nên những câu chuyện cũng đậm chất giới trẻ, thiếu một chút sâu sắc trong từ ngữ, khá thực dụng trong cách viết, có lẽ nó cần được chăm chút ở phần từ ngữ thêm một chút, nhưng dù sao nó cũng là một cuốn sách có những cốt truyện khá sáng tạo và thu hút. Dù rằng đọc có nhiều chỗ bất cập và hơi nhanh cho phần diễn biến. Cuốn sách rất đáng để học hỏi và tìm cảm hứng cho những người mới bắt đâu sáng tác tản văn và truyện ngắn. 
4
713961
2015-09-03 15:27:20
--------------------------
223152
5731
Có rất nhiều cảm xúc khi đọc xong quyển này. Tuy nhiên bản thân mình thấy thì quyển này không hay, chỉ dừng lại ở mức tạm được. Đây là lần đầu tiên mình đọc truyện ngắn của Trung Quốc nên mình thấy không thích lắm. Nó có hơi hướm ngôn tình và không thực tế cho lắm.Điểm cộng của quyển này là bìa đẹp, búp bê cầu mưa. Trong các truyện ngắn thì mình ưng nhất Chuyện kể trong nước mắt. Đó là truyện mình cảm thấy hay nhất trong các truyện. Nói chung thì đây cũng là một quyển không tồi, nhưng mình cũng thấy hơi tiếc tiền khi mua quyển này.
3
278052
2015-07-06 17:51:41
--------------------------
140428
5731
Có rất nhiều xúc cảm khi đọc tập truyện ngắn này với nhiều câu chuyện, tình huống, hoàn cảnh khác nhau. Tuy có vài phần thực tế nhưng cũng có không ít phần hơi hão huyền và mơ mộng xa xôi. Có nhiều câu chuyện hay, ý nghĩa sâu sắc đem lại những bài học thực tế và đáng giá. Nhưng đồng thời có rất nhiều câu chuyện không rõ đang nói về điều gì, cốt truyện là gì và nó cứ lan man làm sao. Vì đây tập truyện ngắn của các tác giả Trung Quốc nên nó có hơi hướm ngôn tình và không thực tế cho lắm, nên những bạn nào muốn tìm kiếm 1 tập truyện ngắn mang nhiều ý nghĩa thì không nên mua truyện này còn với những bạn thích ngôn tình thì đây cũng là 1 lựa chọn không tồi.
3
401466
2014-12-11 12:30:17
--------------------------
140366
5731
Vẫn là chủ tình yêu bất diệt trường tồn qua năm tháng, tuy nhiên tác phẩm không mang đến cho bạn những chuyện tình đầy nước mắt hay những cuộc gặp mặt đầy duyên phận mà tác giả đưa chúng ta đến với tình yêu ở xã hội hiện đại, nơi mà với đa số người xem vật chất quan trọng hơn là tình cảm.
Chuyện tình cảm thì tương lai có thể cho bạn hạnh phúc nhưng với $ bạn có thể có cả tương lai hạnh phúc và sự ấm no, nhiều người bảo $ không mua được hạnh phúc nhưng đa số người ta đều cảm thấy hạnh phúc khi có tiền, và cũng chính $ là sự nguyên nhân của sự phản bội, những mối bất hoà những mâu thuẫn trong cuộc sống, nơi mà những cơn nóng giận của giọt nước tràn ly hay những sự phản bội chan đầy nước mắt đều đưa đẩy cuộc đời con người về bến bờ khác bới một lý do đơn giản... TIỀN.
4
451219
2014-12-10 23:18:06
--------------------------
