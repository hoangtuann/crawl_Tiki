440408
7819
Tony Buzan - tác giả của cuốn sách là một người đạt nhiều thành tựu trong sự nghiệp nghiên cứu trí tuệ nên có thể tin tưởng về tính chuyên môn của cuốn sách. Cuốn sách đề cập đến nhiều phương pháp: Hệ liện tưởng, phương pháp phòng trí nhớ, hệ số chính. Đây là 3 phương pháp mà tôi tâm đắc nhất. Cuốn sách hữu ích là như vậy nhưng điều quan trọng nhất chính là sự tập luyện của bạn, rất khó khăn trong giai đoạn đầu nhưng nếu vượt qua bạn sẽ đạt được thành công xứng đáng hơn những gì bạn bỏ ra.
5
487129
2016-06-01 19:12:14
--------------------------
437889
7819
Sau khi tôi đọc nhiều cuốn của tác giả Tony Buzan, tôi thấy cuốn này khá hay. Sách viết về nhiều cách, phương pháp để ghi nhớ. Tôi thực sự đánh giá cao cuốn sách này. Tôi đang bắt đầu vận dụng, thực hành và đang có ý định mua cuốn luyện đọc nhanh cùng tác giả. Sách đẹp, tiki giao hàng nhanh. Nhưng có 1 điều là dịch giả quá bám vào tiếng Anh, người Việt sẽ khó áp dụng, ví dụ như nhớ tên của người nên dùng tên tiếng Việt. Tôi nghĩ nếu dịch giả làm cuốn này thuần Việt tí thì sách rất hoàn hảo. 
5
696994
2016-05-28 23:55:30
--------------------------
371496
7819
Cuốn sách được trang trí màu khá đẹp, nhưng bên trong lại ít hình minh họa và màu sắc làm nó trở nên kém sinh động. Giấy để in sách rất tốt và mịn nhưng còn tri thức của cuốn sách thì hơi khó hiểu và kém phần linh động trong cách chuyển từ Tiếng Anh sang tiếng Việt ( ví dụ như các Hệ chữ cái, hệ số-vần vẫn lấy các ví dụ nguyên văn Tiếng Anh). Thực sự, mình là một fan hâm mộ của Tony Buzan và mình rất yêu thích những cuốn sách của ông ấy viết! Mong lần tái bản sau, cuốn sách này sẽ hoàn thiện hơn!
4
887420
2016-01-19 18:03:43
--------------------------
353725
7819
Sử dụng trí nhớ của bạn - Use your memory thật sử là một quyển sách giúp cải thiện trí nhớ rất tốt, tác giả của sách lại là người sáng tạo ra sơ đồ tư duy nên cách viết sách của ông rất hữu ích và sáng tạo. Sách giúp ta có trí nhớ tốt hơn từ việc nêu ra hàng loạt cái tên hay hình ảnh và kêu ta phải cố gắng nhớ để  sang trang khác và điền vào. Rất hay tuy nhiên với số trang khoảng 200 mấy trang với chất liệu giấy bình thường mà với giá như  thế thì thật sự là hơi mắc.
4
851304
2015-12-16 19:39:23
--------------------------
342774
7819
Tuy rằng giá cả hơi mắc, nhưng chủ quan mà nói rằng nó hoàn toàn xứng với giá trị nội dung của cuốn sách. Ngay khi có được cuốn sách này, mình đã đọc nó gần như ngay lập tức. Và thực sự bị cuốn hút bởi những nội dung vốn tưởng như là khô khan. Sau 2 ngày đọc, cùng với việc thực hành một số bài tập trong cuốn sách, nhận thức về cách nhớ thực sự đã có hiệu quả. Cuốn sách này thực sự rất có ích. Chỉ cần người đọc chú tâm và bỏ thời gian ra luyện tập theo những bài tập đã được soạn ra một cách cụ thể, bài bản cùng với những ví dụ sinh động trong cuốn sách, thì việc sở hữu một trí nhớ gần như hoàn hảo là hoàn toàn có thể. Bạn chỉ cần thực hành thôi, vì cuốn sách đã chứa hầu như những thông tin cần thiết giúp nâng cao khả năng nhớ của bạn rồi. 
5
979133
2015-11-25 01:50:49
--------------------------
334809
7819
Mình  hay quên trước quên sau lắm, tình cờ biết được tác giả Tony Buzan trên báo thì mình tìm hiểu và mua quển sách này. Thật sự thì trước giờ không thích đọc loại sách này nhưng mà đọc quyển này rất thú vị. Càng đọc càng thích ấy.các phương pháp mà tác giả đưa ra rất hay, không chỉ giúp mình cải thiện trí nhớ mà còn học hỏi về cách trình bày logic nữa. Nhưng mà cái gì cũng cần luyện tập chăm chỉ và thời gian nữa. Mong rằng các bạn cũng sẽ cải thiện trí nhớ tốt hơn qua quyển sách này
5
180068
2015-11-10 02:57:05
--------------------------
327944
7819
Các phương pháp ghi nhớ được trình bày đầy đủ và toàn diện. Việc sử dụng các hệ nhớ rất hay và có thể áp dụng trong thực tế cuộc sống. Sách có kèm một số hình in màu và kết hợp với sơ đồ tư duy rất hay. Phần nhớ hệ số - hình bằng tiếng Anh hơi khó tiếp cận vì thấy nó gượng gạo, nhưng dùng để học tiếng Anh luôn cũng khá hay. Phần nhớ khuôn mặt thì hơi khó một tí, vì mình không có thói quen quan sát đặc điểm khuôn mặt người. Mình nghĩ cần rèn luyện lâu dài, thường xuyên thì việc áp dụng mới đạt hiểu quả cao. Đây là một cuốn sách hay.


4
39590
2015-10-28 19:44:41
--------------------------
313463
7819
Để có được một trí nhớ thật sự tốt thì đòi hỏi phải có một phương pháp luyện tập đúng cách và bài bản. Cuốn sách này đem lại cho tôi một phương pháp rèn luyện về trí nhớ thật hữu ích. Nhưng kết quả đạt được như mong đợi hay không còn nằm trong sự tập trung và sự bền bỉ rèn luyện của mỗi người. Kim chỉ nam trong tay và mọi người hãy sử dụng kim chỉ nam " Sử Dụng Trí Nhớ Của Bạn" bằng tất cả nhiệt huyết, khát vọng, niềm đam mê ... Để đạt được những kết quả mà mình đã đặt ra !
5
519066
2015-09-23 18:34:04
--------------------------
313109
7819
Mình mua cuốn này để học cách nhớ. Đúng là muốn rèn luyện được phải có thời gian dài để tập. Cuốn sách này rất hay, mình đã vận dụng được nó rất nhiều. Sách có nhiều bài tập hình ảnh rất sinh động. Mình rất ấn tượng với những cách nhớ,ví dụ như là tự hình thành trong đầu mình một câu chuyện liên quan đến những vật dụng cần mua chẳng hạn. Rất ưng ý khi mua cuốn sách này. Sau hai năm áp dụng thì thấy rất bổ ích, hiệu quả. Mà tiki bọc sách cũng thích nữa, làm cho cuốn sách giá trị hẳn. 
4
38649
2015-09-22 20:32:27
--------------------------
308107
7819
196k không rẻ tí nào, nhưng mình quan tâm hơn cả là giá trị của sản phẩm. Mình nghĩ mình đã nhận được nhiều thứ bổ ích hơn là con số bù lại cho 196 đó. 
Tony Buzan là một tác giả mà mình vô cùng tin vậy, ông là bậc thầy cho những chủ đề về tự duy. Với những bài tập về trí nhớ có sẵn trong sách, mình đã  có thể luyện tập ngay và nhận ra ưu khuyết điểm của bản thân. Quả thật, trí nhớ mình cải thiện ngay khi áp dụng các của Tony Buzan. 
Đầu tư cho việc học là không bao giờ uổng phí <3
5
806184
2015-09-18 14:50:14
--------------------------
280043
7819
Hình thức: Bìa cứng, giấy dày, trắng, láng, đẹp. Cuốn Tiki giao tới cũng không bị sứt mẻ như những lần khác, mình rất hài lòng. Tuy nhiên, cái giá này thật sự quá đắt, gần 200k cho 1 cuốn sách chỉ 244 trang. Phải nói là khi mua nó mình tiếc tiền dữ lắm nhưng vì muốn có trí nhớ tốt nên đành đầu tư một chút!

Nội dung: Phải nói là mình rất khâm phục ông tác giả đã viết ra cuốn này, đúng nghĩa một thần đồng, giỏi kinh khủng nên ông mới có thể viết ra nhiều cuốn sách hay như thế. Sách khá dày, có nhiều mục khác nhau: về lịch sử, về số điện thoại, về nhớ mặt người… giúp độc giả có thể luyện trí nhớ tăng độ khó lên dần, cải thiện tư duy.

5
285794
2015-08-27 13:31:41
--------------------------
275238
7819
Tiếp nối cuốn " Sử dụng trí tuệ của bạn ", tôi tìm mua cuốn " sử dụng trí nhớ " này và hoàn toàn hài lòng, cuốn sách này nói rõ ràng và dễ ứng dụng hơn cuốn kia, giá tiền như thế này nghiêng nhiều cho công sức của tác giả, mình cũng cảm thấy không hề tiếc. Mình đã nhớ được nhiều hơn , nhất là mấy bài học khó nhằn, nên rất cảm ơn tác giả cuốn sách đã chia sẻ những bí quyết hữu ích này, tuy có một vài chỗ khó ứng dụng nếu bạn không phải người nói tiếng anh ^_^.
5
186614
2015-08-22 18:59:26
--------------------------
274319
7819
Từ khi đọc quyển sách Sử Dụng Trí Nhớ Của Bạn - Use Your Memory của Tony Buzan...
mình đã nhận ra một điều...
là bản thân mình đã có thể nhớ...
rất nhiều những gì mà trước giờ k thể nhớ...! :)
Thế cho nên..
việc lựa chọn mua quyển sách này...
là không sai gì cả..!
Bạn có tin không?!
Không tin?! Hãy mua và đọc thử! :)
Nếu không....bạn sẽ phải tiếc nuối vì đã không khám phá chúng!
5
751878
2015-08-21 21:09:16
--------------------------
268075
7819
Sau "Sử dụng trí tuệ của bạn", "Sử dụng trí nhớ của bạn" là phần bổ sung không thể thiếu về mảng xây dưng và củng cố trí nhớ - trí năng. Có thể bạn chưa từng đọc qua "Sử dụng trí tuệ của bạn" nhưng đọc xong "Sử dụng trí nhớ của bạn", bạn sẽ cảm thấy cần thiết được hệ thống lại, lúc đó bạn sẽ muốn đọc thêm nhiều tác phẩm nữa của Tony Buzan, như tôi vậy. Học sinh, sinh viên trong thời đại này thường thiếu kĩ năng có tính ứng dụng, cuốn sách này sẽ mở đường cho tương lai của bạn, và chắc chắn sẽ xây dựng cho bạn lợi thế khi làm bất kì công việc nào, bất kì công việc nào cần một cái đầu thông minh và linh hoạt 
5
292321
2015-08-16 00:43:30
--------------------------
267700
7819
Sách của Tony Buzan đã giúp tôi cải thiện trí nhớ rất nhiều. Cuốn sách này có những phương pháp sử dụng trí nhớ của mình 1 cách hiệu quả. Và khai thác được hết những tiềm năng của bản thân. Sách chia ra thành 2 phần, lí thuyết và thực hành, rất logic, mạch kạc, sinh động và đầy màu sắc, nó khiến tôi cảm thấy thú vị hơn là những quyển sách đen trắng thông thường. tuy nhiên, giá của quyển sách khá đắt so với số tráng. Bù lại nội dung sách rất tuyệt vời.
4
615456
2015-08-15 20:10:51
--------------------------
266604
7819
Mình chọn mua cuốn sách này là vì cuốn sách viết từ Tony Buzan một tác giả hàng đầu về trí nhớ. Tác giả đã đặt ra các vấn đề khá thực tế và dễ hiểu rồi đưa ra cách giải quyết tốt nhất cho vấn đề bằng các bài thực hành.Lý thuyết này có thể dễ dàng bắt gặp trong cuộc sống nhưng đòi hỏi phải có tính kiên trì cao .Tuy nhiên giá thành của sách khá là cao nên rất khó cho học sinh bọn mình vì sách được in bằng loại giấy bình thường chỉ có vài trang in màu
5
608628
2015-08-14 20:40:30
--------------------------
249817
7819
Tôi đã đọc được kha khá sách được viết bởi Tony Buzan, nhưng chưa có cuốn nào khiến tôi phải trầm trồ về tính thực tiễn như cuốn sách này. Cách trình bày vấn đề của tác giả khá hay, rõ ràng và mạch lạc. Nó không chỉ là lý thuyết suông đâu nhé, mà luôn có ít nhất một đến hai bài thực hành ở mỗi chương. Khi đọc cuốn sách này bạn sẽ có cảm giác mình đang tiến dần từng bậc thang trong việc chinh phục kết quả mà tác giả muốn bạn đạt được – một trí nhớ siêu phàm. Tuy nhiên, nếu bạn không có tính kiên trì thì việc này không dễ chút nào đâu nhé. Về hình thức thì cuốn sách này không khác mấy so với những cuốn sách khác của Tony Buzan, trình bày rõ ràng, sinh động, chất lượng giấy cũng rất là tốt. Cho điểm tuyệt đối cho cuốn sách này!
5
329218
2015-07-31 23:03:58
--------------------------
155897
7819
Thứ nhất tôi xin nhận xét về mức giá của tác phẩm này. Theo tôi mức giá này là hơi cao so với ngoại hình của nó. Loại giấy sử dụng của tác phẩm cũng là loại giấy bình thường, chỉ có vài trang về sơ đồ tư duy là in màu.
Nhưng về nội dung thì không có gì phải bàn cãi. Sách hướng dẫn nhiều phương pháp cải thiện trí nhớ và sự hồi tưởng lại một cách gần như là hoàn hảo. Những phương pháp này hầu hết chúng ta đều không được học ở nhà trường. Quyển sách rất hữu ích với mọi độc giả, đặc biệt là học sinh và sinh viên.
4
387632
2015-02-02 15:29:35
--------------------------
153783
7819
Tony Buzan là bậc thầy về phương pháp học tập. Là người phát minh ra công cụ học sơ đồ tư duy, Tony Buzan đã viết rất nhiều sách và thực hiện nhiều chương trình truyền hình về học tập, trở thành tác giả được yêu thích hàng đầu trong dòng sách này. Quyển sách này, Tony tập trung vào lĩnh vực ghi nhớ, đưa ra những nghiên cứu về trí nhớ người, đồng thời đề xuất những phương pháp rất hay để ghi nhớ sự việc, dữ liệu, thông tin. Phải nói, mỗi quyển sách của Tony Buzan đều là bài học quý giá, mang đến phương pháp hiện đại, học năng động thay vì gò bó vào mớ kiến thức hỗn độn ở nhà trường cổ điển.
5
515736
2015-01-27 14:43:28
--------------------------
