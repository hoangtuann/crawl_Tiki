246200
5681
Câu chuyện kì diệu về Henry Sugar khiến tôi liên tưởng đến Alie lạc vào xứ sở thần tiên. Một cậu bé có khả năng nói chuyện với các loài vật với cây cỏ. Từ đó cuộc phiêu lưu của cậu bé Henry Sugar và ông già có khả năng nhắm mắt vẫn nhìn thấy mọi thứ bắt đầu. Cuộc phiêu lưu đến những vùng đất mới lạ, gặp những điều khác lạ, điều tốt và điều xấu. Bảy câu chuyện của tác giả Roald Dahl sẽ mang đến cho chúng ta vào một thế giới huyền bí đầy mê hoặc.
4
77305
2015-07-29 17:44:05
--------------------------
