378687
7806
Tôi viết nhận xét này khi chưa đọc xong cuốn sách của tác giả Đàm Linh. Lý do là vì tôi thích dành 1 khoảng thời gian dài để nghiền ngẫm một tác giả hơn là đọc một lượt cho hết cả quyển sách. Sau đây là một số thiển ý của tôi.

Về hình thức thì như mong đợi, mọi thứ đều ổn: chất lượng giấy, thiết kế bìa, kiểu chữ... đều rất dễ nhìn, bắt mắt. Về nội dung, tôi đánh giá rất cao nỗ lực của tác giả Đàm Linh. Tôi cho rằng đây là một thành quả đáng ghi nhận của chị và các đồng sự, và hy vọng chị sẽ nỗ lực cống hiến nhiều hơn, trao đổi nhiều hơn với các doanh nhân - những người làm chủ số 1 Việt Nam - để giới trẻ có thể học tập, biết đến họ nhiều hơn.

Tuy tôi đánh giá cao nỗ lực của chị, cũng nhận rõ chị không sa vào lối viết tâng bốc, tung hô thành công của bất kỳ cá nhân nào. Tuy vậy, tính khách quan vẫn chưa thể hiện rõ rệt. Có một số chi tiết tôi cho rằng là chưa đúng sự thật. Chẳng hạn, tôi xin mạn phép được đề cập tin tức gần đây: vụ việc của ô mai Hồng Lam. Khi tôi đọc tin này, tôi vẫn chưa biết về thương hiệu Hồng Lam do tôi không mua ô mai bao giờ, cũng không sống ở Hà Nội. Kỳ diệu là khi tôi giở cuốn sách ra đọc tiếp thì phần tiếp theo chính là về thương hiệu Hồng Lam, lẽ dĩ nhiên là tôi sẽ đọc kỹ hơn so với các phần khác. Tôi đọc có đoạn " công ty định kỳ đưa sản phẩm đến viện vệ sinh kiểm tra" và "có phòng thí nghiệm để kiểm nghiệm sản phẩm". Đến đây tôi bỗng bối rối, nếu vậy thì sao lại xảy ra chuyện không đáng mong muốn về lượng đường hóa học cao gấp 8 lần mức an toàn vậy? Tôi xin không dám tự phán xét, xin được người có kinh nghiệm hơn chỉ điểm. 

Ngoài ra, cuốn sách đề cập rất nhiều về những người làm chủ, cách tạo dựng sự nghiệp, thương hiệu... nhưng chưa chi tiết. Sau tất cả, tôi vẫn rất ưng ý cuốn sách này. Rất cảm ơn nỗ lực của chị Đàm Linh cùng đồng sự, hy vọng tập 2 của "Những người làm chủ số 1 Việt Nam" sẽ phản ánh chính xác hơn cũng như chi tiết hơn về những doanh nhân đang nắm vai trò quan trọng trong nền kinh tế nước nhà.
3
605785
2016-02-06 22:09:30
--------------------------
342808
7806
Đối với mình, đây là một cuốn sách đáng đọc. Không chỉ vì nó đưa đến, dù không nhiều, một góc nhìn về tư duy của người làm chủ, mà còn vì tác giả của nó. Chài ai, người đâu siêu giỏi, còn trẻ như vậy, bận rộn với công việc (chắc chắn luôn), vậy mà vẫn manage được time để phỏng vấn mấy bác. Dù nhiều khi mình muốn bài viết đi sâu hơn tí xíu (tại tò mò :v), nhưng nhìn cuốn sách, nhìn những mẩu chuyện là mình cũng cảm nhận được cuốn sách là thành quả đáng ngưỡng mộ của một con người như thế nào. Anw, Cám ơn Đàm Linh rất nhiều :)
4
297766
2015-11-25 08:38:23
--------------------------
304087
7806
Tựa đề sách khá hấp dẫn '' Người Làm Chủ Số 1 Việt Nam "
Đất nước Việt Nam ta có rất nhiều nhân tài, và tại sao họ có thể vươn lên thành công rực rỡ như vậy?
Chúng ta có thể tìm kiếm những doanh nhân Việt Nam trong nước để học hỏi và ngưỡng mộ, cũng như khích lệ tinh thần của bản than.
Đọc những câu chuyện cuộc đời của những người Việt Nam thành đạt để bạn biết.
Đã có người Việt Nam làm được, vì thế bạn cũng có thể.
Cuốn sách được tác giả người Việt viết nên, tác phẩm dành cho người Việt Nam.
4
119434
2015-09-16 10:29:25
--------------------------
267395
7806
Khi đọc tựa đề của quyển sách, tôi đã nghĩ đến những câu chuyện về lớp trẻ, thế hệ thanh niên đi lên với những thành công hay vấp ngã của họ. Thế nhưng, Đàm Linh lại kể những câu chuyện về những tấm gương thành công điển hình. Có lẽ bản thân tôi cùng nhiều bạn đọc khác đã quen với câu chuyện cuộc đời Đặng Lê Nguyên Vũ, Mai Kiều Liên.... qua những trang báo kinh tế nên dường như không còn cảm thấy mới mẻ nữa.Tuy nhiên, đây chỉ mới là tập 1. Tôi hy vọng tập 2 sẽ mang lại nhiều điều lý thú hơn.
3
64568
2015-08-15 15:00:37
--------------------------
258652
7806
Bạn yêu thích các hoạt động kinh doanh và quan tâm đến nền kinh tế nhưng lại chưa có đủ động lực ? Thế thì hãy tìm ngay cuốn sách này ! Cuốn sách giới thiệu về những người làm chủ, những vị giám đốc, những doanh nhân thành đạt của Việt Nam. Không phải tự dưng mà họ thành công, mà trong chặng đường đó họ đã phải cố gắng rất nhiều và bỏ ra không ít hy sinh. Khi đọc các câu chuyện về họ chúng ta dường như vừa thán phục lại vừa nung nấu được khát khao và sức mạnh của bản thân. Những tấm gương được đề cập trong sách cho thấy nước ta không thiếu nhân tài và không phải là không có thực lực, chỉ là chưa đủ cố gắng mà thôi. Tuy nhiên cuốn sách chỉ mang tính tham khảo là chủ yếu, qua đó độc giả nắm bắt được kha khá thông tin nhưng vẫn chưa giới thiệu chi tiết, chưa bàn luận cụ thể. Dù sao đi nữa thì đây vẫn là 1 cuốn sách khá hay và có tác dụng "truyền lửa" cho những người có đam mê thật sự.
4
75959
2015-08-08 18:35:20
--------------------------
239211
7806
Đọc sách, tôi có cảm giác đang xem một bài báo về các doanh nhân. Lối viết của tác giả dễ hiểu, theo thứ tự thời gian khởi nghiệp của từng doanh nhân. Lý giải hoàn cảnh kinh tế - chính trị bấy giờ. 
Sách là tập hợp nhỏ của những doanh nhân thành đạt, bạn hoàn toàn có thể dùng nó để hiểu sơ lược về họ và các thành tựu gắng liền với họ. Nhưng để đi sâu hơn vào chi tiết thì cuốn sách là chưa đủ.
Nhưng tôi thật sự khâm phục tác giả, Đàm Linh đã tự mình tạo ra một cuốn sách rất hay về doanh nhân Việt. Mà theo tôi, ở Việt Nam chưa ai làm điều này. Tôi mong chờ những tập tiếp theo của sách (nếu có). Bạn sẽ không thất vọng khi đọc sách, sách về doanh nhân nước ngoài thì đầy ra đó. Nhưng ít ai có tâm về viết về doanh nhân Việt như Đàm Linh.
Cảm ơn tác giả đã viết lên một cuốn sách tuyệt như vậy. Mong về sau sẽ có thêm nhiều cuốn sách sâu xa hơn về họ.
3
43494
2015-07-23 19:28:02
--------------------------
236212
7806
Quyển sách mang cái nhìn toàn cảnh về những doanh nhân thành đạt, giàu có , nổi tiếng nhất nhì Việt Nam, những câu chuyện giản dị đời thường về những cái tên lớn, đọc hết quyển sách hẳn bạn cũng sẽ đồng ý với tôi rằng : làm giàu không khó nhưng cũng ... không dễ dàng gì . Tuy nhiên, có một điều chưa được như mong đợi đó là quyển sách đi sâu về những câu chuyện về đời tư, thành tích, thành tựu của những nhân vật này hơn là những chia sẻ, truyền đạt của họ như những người đi trước. Dù sao đây cũng là quyển sách đáng đọc về những tấm gương thành đạt ngay tại chính quê nhà, những người tạo ra những sản phẩm mà có thể chính bạn cũng đang sử dụng một trong số những sản phẩm của họ.
3
24682
2015-07-21 16:50:18
--------------------------
214223
7806
Một người Việt trẻ đã thực hiện những bài viết, bài phỏng vấn về những nhân vật mở đường trong giới kinh doanh việt nam,  họ đã trở thành người làm chủ số 1 Việt Nam như thế nào. 
tôi hiểu được doanh nhân Việt nam đã đi lên như thế nào, họ thậm chí khi khởi nghiệp đã không có đủ điều kiện như chúng ta đây. Những gì họ đạt được sau một quá trình gian nan để vượt khó, không phải rất kì vĩ sao. Qua đó tôi biết được bản thân mình cần cố gắng nhiều và nhiều hơn nữa. Tác giả cuốn sách còn rất trẻ nhưng cách viết lách, cách nhìn nhận, cách tìm tòi vấn đề, đặt câu hỏi với những con người khổng lồ ấy lại không thua kém người có kinh nghiệm tí nào. Đọc cuốn sách này bạn đừng đọc lướt mà hãy vừa đọc vừa suy nghĩ về họ. Sách là tuyển tập những con đường dẫn lối cho ai muốn đi đến thành công và đam mê của mình.
4
54004
2015-06-24 18:04:38
--------------------------
199013
7806
Một cuốn sách hữu ích khi cung cấp những thông tin, những câu chuyện xoay quanh những doanh nhân nổi tiếng của Việt Nam. Những câu chuyện bên lề thú vị mà qua đó đã khơi dậy trong lòng thế hệ trẻ những khát khao ,những đam mê công hiến và làm việc hết mình với sự say sưa để có thể đạt được ước mơ của mình. Một cuốn sách tưởng chừng như rất khô khan, nhưng qua cách viết của Đàm Linh có sức lôi cuốn và khiến mình không khỏi rời mắt. Bên cạnh cung cấp những câu chuyện xoay quanh việc làm giàu của các doanh nhân, cuốn sách còn cung cấp những thông tin hữu ích về Việt Nam bằng một bảng vẽ thông tin chú thích nhưng vẫn đủ khái quát hóa tình hình Việt Nam hiện tại như hiện nay Việt Nam đang ở đâu trên thế giới và chúng ta có gì...Một cuốn sách với khá nhiều thông tin hữu ích ...
4
75031
2015-05-21 11:56:37
--------------------------
152699
7806
Thứ nhất, cuốn sách rất đẹp. Bìa vẽ minh họa gương mặt các doanh nhân một cách hài hước, đậm chất riêng từng người, đặt trên nền chữ S từ Bắc ra Nam. Nền xanh dương cuộn trào sóng nước như mang bao khát vọng sục sôi. Trang sách in rõ nét, giấy nhẹ, chất lượng.
Về nội dung, đó là câu chuyện về những con người "làm chủ số 1 Việt Nam"- những con rồng, con hổ trong nước, sở hữu những tập đoàn lớn, do chính mình làm chủ, chi phối và góp phần phát triển kinh tế Việt Nam. Đọc những chia sẻ của những nhân vật này rất thú vị, bởi họ không chỉ có tài mà còn có tâm, không chỉ lão luyện trong chuyên môn của mình mà còn sẵn sàng bon chen ra thương trường để gây dựng những thành tượu lớn lao. Lớp thế hệ đó thật đáng khâm phục!
Mình đặc biệt ấn tượng với câu chuyện của anh Trương Gia Bình và Đặng Lê Nguyên Vũ, dám dấn thân, dám thử thách, và luôn không ngừng đau đáu về tương lai của dân tộc mình.
Cuối sách còn tập hợp số ít nhưng chất những bài báo sâu sắc của những nhà trí thức viết cho giới trẻ, rất đáng để suy ngẫm. 
Một điểm trừ trong cuốn sách là một số bài viết về nhân vật mang nặng tính thông tin, giới thiệu, không làm nổi bật hình tượng, tính cách doanh nhân (do tác gia không phỏng vấn được).
4
201502
2015-01-23 22:13:28
--------------------------
118668
7806
Một cuốn sách của người Việt, do người Việt viết, viết về người Việt..Những Trương Gia Bình của FPT, Mai Kiều Liên của Vinamilk, hay Đặng Lê Nguyên Vũ của Trung Nguyên, họ đã trở thành người làm chủ số 1 Việt Nam như thế nào. Điều gì khiến người  Việt, những con người vừa mới đây thắng Pháp, thắng Nhật, thắng Mỹ, phải chịu nhục, chịu hèn, điều gì khiến 1 dân tộc anh hùng như Việt Nam khi ra thế giới bị khinh thường tới vậy??? Những thế hệ trẻ chúng ta phải rửa nỗi nhục nghèo hèn cho đất  nước. Mình đã bật cười khi đọc đến đoạn Đặng Lê Nguyên Vũ khi đến năm thứ 4 đại học vì suy nghĩ nhiều để tạo nên loại cafe ngon nhất nên đã bị rụng hết tóc. Nhưng sau đó mình mới nhận ra ước muốn đó đã thấm sâu vào cuộc sống của anh, đam mê lớn nhất đời anh vì vậy anh mới thành công như bây giờ. Hay như chuyện ông chúa đảo Tuần Châu Đào Hồng Tuyển vs tiền ko đếm xủê nhưng bắt đầu vs việc dọn chuồng heo, ngủ ngoài công viên .Để có những thành công của Apple thì Steven Jobs phải bắt đầu từ những ngày khổ cực trong gara ô tô của ba mẹ, vượt qua nỗi đau bị đuổi khỏi công ty do chính mình lập nên...và Đào Hồng Tuyển cũng vậy, ông ko bao giờ quên những ngày mưa ko có chỗ ngủ phải ngủ dưới mái hiên nhà người ta . À mình cũng thấy vui vui khi phát hiện ra Cao Thị Ngọc Dung của PNJ là quê Quảng Ngãi. 10 nhân vật, 10 câu chuyện cuộc đời của các anh chị số 1 Việt Nam, và họ sẽ vẫn đc các thế hệ trẻ nhớ đến dù có thể 1 ngày nào đó họ ko còn là số 1 nữa....
5
340018
2014-07-30 15:24:56
--------------------------
116667
7806
Ấn tượng bởi tự đề. Tôi đã mua cuốn sách này. Đơn giản là có quá nhiều sách kinh doanh nhưng chưa có cuốn sách nào là của người Việt viết về người Việt với một cách nhìn rõ nét và đầy chân thực nhất. 
Đầu tiên khi đọc sách. Tôi bắt đầu nhàm chán vì lối viết đầu của mỗi câu chuyện mang tính lý thuyết và dùng nhiều thuật ngữ kinh tế. Nhưng càng về sau tôi lại càng bị thu hút. Càng thấm được mỗi thành công, mỗi vinh quang không đơn giản đến ngay mà phải qua quá trình lâu dài với bao nhiều nỗ lực và khó khăn. Hiểu được doanh nhân Việt đi lên như thế nào và biết được bản thân mình cần cố gắng nhiều và nhiều hơn nữa. Tác giả cuốn sách cũng làm tôi bất ngờ vì tuổi của tác giả chỉ lớn hơn tôi 1 tuổi nhưng cách viết cách nhìn nhận cũng như sự tiếp cận đến với những con người khổng lồ của Việt Nam.  Đọc cuốn sách này bạn đừng đọc lướt mà hãy vừa đọc vừa ngẫm vừa nghiền nó. Sẽ là bài học hay và là con đường  dẫn lối cho ai muốn đi đến thành công và đam mê của mình. Đáng để bạn tìm và đọc.
3
270245
2014-07-10 16:06:13
--------------------------
111393
7806
Khi mua cuốn Những người làm chủ số 1 VN, tôi kỳ vọng sẽ có nhiều thông tin của những người thành công ở VN hơn những gì báo chí đã viết nhưng chưa được hài lòng lắm. Thông tin còn sơ sài, chưa có nhiều câu trả lời cho các cách mà những nhân vật thành công này giải quyết khó khăn. Ngay như thông tin về Mai Kiều Liên chỉ có những thông tin tổng hợp chung chung về công ty, còn về con người Mai Kiều Liên chưa nhiều. 
Nhưng nói chung đây là cuốn sách đầu tiên ở VN viết về những con người VN thành công. Hy vọng sẽ còn có nhiều cuốn sách như thế viết về con người VN, cách làm thành công của người VN hơn nữa.
2
171789
2014-04-28 16:56:35
--------------------------
104268
7806
Thoát ra khỏi mớ thông tin hỗn độn của giới truyền thông, nơi mà Việt Nam được hình dung là một đất nước nghèo, nơi mà chỉ có những tấm gương, câu nói, chia sẻ của những kẻ ở cách xa hơn nửa vòng trái đất, nơi mà phần lớn giấy bút chỉ được dùng để vẽ lên một bức tranh u ám, thật ra vì những thứ giật gân, chết chóc, hóa chất độc hại mới là thứ mà nhiều người quan tâm, họ mới bán được những sản phẩm truyền thông của mình.

Những Người Làm Chủ Số 1 Việt Nam cho chúng ta một góc nhìn khác, về những "người khổng lồ" tại Việt Nam, những góc nhìn, trăn trở, những con người chưa bao giờ chịu thua trước hoàn cảnh. Bên cạnh những câu chuyện của những người làm chủ này, những câu chuyện mà có lẽ bạn tự đọc, tự cảm nhận sẽ tốt hơn lời tôi kể, còn có một câu chuyện của một cô gái 9x cùng những đồng nghiệp mình, nung nấu một giấc mơ chia sẻ những con người Việt Nam đến cộng đồng, một nỗ lực rất lớn, tôi mua quyển sách này một phần cũng vì sự cố gắng của tác giả đấy.

Tin rằng tuy có đôi ba điểm chưa được chau truốt, nhưng nó là một quyển sách đáng đọc, nhất là cho những bạn trẻ, những người khởi nghiệp - Trích từ lời thầy tôi.

Chúc các bạn đọc sách vui vẻ, mong là nó có ích cho các bạn.
5
190631
2014-01-10 16:51:33
--------------------------
