349255
6774
Nếu là người hiện đại thì quyển sách này đích thị dành cho bạn. Tựa sách xét theo nghĩa đen hay nghĩa bóng thì nó đều đầy sự ám chỉ gợi mời. Tuy nhiên, giọng văn của nữ tác giả lúc thì dịu dàng khi lại đanh ngoan, cố chấp.

  Qua những mẫu chuyện ngắn đúc ra từ chính tác giả hay từ những người mà tác giả đã gặp và nghe câu chuyện của họ. Chúng ta sẽ nhận thấy có lời khuyên, có sự chua chát về cuộc đời hay sự đồng điệu về suy nghĩ với người đọc. Xét ở khía cạnh nào thì nhà văn Phương Thảo cũng gợi ra cuộc sống của những người đã trải nhiều và qua đó ta nhận ra rằng họ vẫn đang sống và làm điều mình yêu.
3
899040
2015-12-07 23:22:09
--------------------------
330872
6774
Mình mua cuốn sách vì thích cái tiêu đề - thích cách chị Phương Thảo thể hiện trên facebook.
Nhưng khi đọc cuốn sách mình hơi chán. Có lẽ khi những stt tập hợp thành một cuốn sách nó đã bị trùng lặp quá nhiều. Chỉ là những ghi chép của chị về những con người, sự vật quanh chị, đôi khi có vài bài viết gần như truyện ngắn ... mình thấy hơi lê thê, lặp đi lặp lại. Nhiều lúc tưởng như có gì như táo bạo, mạnh mẽ, nhưng rồi chẳng đâu vào đâu , như kiểu đang đi bước hụt chân một cái.
Tóm lại là mình chưa đọc hết được quyển sách.
2
671763
2015-11-03 14:36:18
--------------------------
216091
6774
Sài Gòn mùa trứng rụng - chỉ nghe tựa sách đã thấy khiêu khích. Hình bìa cũng làm người ta phải chú ý: một người phụ nữ vô cùng sành điệu mặc áo dài đỏ, tay xách túi Chanel. Đây là một tập tùy bút viết về những tâm tư, suy nghĩ lo âu về chuyện đời, về tình yêu và người yêu của người phụ nữ trong thời hiện đại. Trong đó cũng thấp thoáng hình ảnh của thành phố Sài Gòn thân quen. Cách viết khá tạo bạo nhưng không dung tục. Muốn thấm được ý của tác giả thì không thể đọc nhanh cuốn sách này. Cảm thấy hài lòng vì đã mua cuốn sách này
5
406362
2015-06-27 10:04:42
--------------------------
