514393
5402
Cuốn sách trình bày logic về mẫu câu đàm thoại,ngữ pháp, từ mới, thay thế và mở rộng cùng phần bài tập.ở phần mẫu câu có đầy đủ phiên âm và dịch tiếng việt, còn các phần khác xen kẽ tiếng việt và phiên âm nên có thể tự dịch và đối chiếu tạo sự thích thú khi học. Đĩa cd dùng kèm để tập nghe và phiên âm! Bản in chữ to dễ nhìn sách đẹp dùng kèm sách tự học tiếng trung cho người mới bắt đầu rất tốt!
5
2080165
2017-01-22 16:25:16
--------------------------
433000
5402
So với cuốn giáo trình 301 câu đàm thoại tiếng Hoa, Cuốn giáo trình này trình bày đẹp hơn, trực quan hơn, chất lượng giấy tốt, cập nhật nhiều từ vựng dùng trong cuộc sống hiện nay. Dùng kèm thêm DVD do các diễn viên Trung Quốc diễn bài khóa thì rất tuyệt! Tuy nhiên, cuốn sách không có chú thích cách phát âm (phần thanh mẫu), phần từ mới không có âm Hán Việt, không giới thiệu bộ thủ, cũng không có hướng dẫn tập viết chữ Hán.  
Muốn học cuốn này, tự học rất khó, cần có giáo viên hướng dẫn, dạy kèm!


3
866867
2016-05-20 10:26:57
--------------------------
411655
5402
Chất liệu giấy tốt ( giấy láng, trắng). Chữ rõ ràng. Bìa sách cứng cáp. Sách thích hợp cho các bạn mới học với cách viết dễ hiểu. Tiêu đề được in đậm, in màu rất dễ đọc. Sau 5 bài học có phần ôn tập nữa.Với lại sách này được nhiều trung tâm sử dụng giảng dạy. Nên về nội dung thì khỏi lo. Còn có CD luyện nghe nữa. Nói chung là khi cầm lên làm muốn học hehe. Tiki giao hàng nhanh nữa. Đóng gói cẩn thận. Mình rất thích.Tuyệt vời!!!! Cảm ơn Tiki nhiều nha. 
4
497416
2016-04-06 13:12:55
--------------------------
