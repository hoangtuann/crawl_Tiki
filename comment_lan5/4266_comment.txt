472414
4266
Tôi đọc rất nhiều truyện ngôn tình, tiểu thuyết nhưng hình ảnh Karl trong câu truyện này là nhân vật có sức hút mạnh mẽ nhất với tôi. Không phải là nét đẹp siêu phàm, không phải độ giàu có, hào phóng, si tình... Mà là ấn tượng về con người say mê lao động, yêu thương gắn với lòng vị tha, một người luôn suy nghĩ hơn hai lần khi muốn làm một việc gì đó mà việc đó có thể làm tổn thương người khác. Anh là người đàn ông tốt và chân thành nhất mà tôi từng biết. Câu truyện này nói về quá trình gặp nhau, nhận ra những lỗi lầm, lừa dối rồi sau đó cảm thông, tha thứ, cùng nhau xây dựng ngôi nhà tương lai của hai nhân vật chính. Lời kể giản dị nhưng khắc họa sâu sắc nội tâm nhân vật.
5
1224559
2016-07-09 15:59:06
--------------------------
360655
4266
Bao dung và tha thứ là điểm nổi bật ở cuốn sách này, quả thật ban đầu tôi hình dung câu chuyện sẽ diễn ra một cách khắc nghiệt, chua xót hơn rất nhiều lần nhưng tác giả lại có thể khiến cho cảm xúc của tôi chỉ dừng lại ở một ngưỡng nào đó, bởi truyện của bà ẩn sau những gai góc, xù xì của mâu thuẫn, của dằn vặt lại là một tình yêu vô bờ bến, một sự chung thủy không thể thay thế. 

Đúng là trong lúc mối quan hệ giữa Anna và Karl đã có lúc tưởng chừng như tan vỡ nhưng rất nhanh sau đó, mọi sự đau khổ lại được hóa giải vì sâu trong trái tim hai người họ vẫn yêu nhau rất nhiều và cũng vì các nhân vật phụ cũng được tác giả mô tả tuyệt vời, không kém phần sâu sắc. Có thể ban đầu khi đọc tóm tắt truyện, tôi đã khá ghét Kerstin vì cô khiến Anna & Karl xảy ra chuyện nhưng đến khi đọc rồi thì lại thích nhân vật này, cô ấy xinh đẹp, hoàn hảo và là một phụ nữ rất đảm đang, Karl tất nhiên khó tránh khỏi giây phút ngưỡng mộ, xôn xao trước hình ảnh ấy. Nhưng Kerstin lại chính là người nhắc nhở anh về cảm xúc thật sự của trái tim mình và cũng nhờ cô mà anh nhận ra anh và Anna yêu nhau nhiều đến thế nào.

Đây là một tác phẩm theo tôi rất hay, giọng văn vô cùng mượt mà và sâu sắc, bối cảnh mà tác giả dựng nên về vùng đất Minnesota thời kì mới khai hoang cùng với cách làm việc của con người nơi đây là một điều thú vị nho nhỏ mà nhờ tác giả tôi đã được biết đến. Không sướt mướt, ủy mị, cũng không khô cứng, câu chuyện diễn ra rất tự nhiên và thu hút khiến tôi không thể đặt cuốn sách xuống được. Nhưng có lẽ điều trên hết mà tôi ghi nhớ chính là lòng vị tha và bao dung của karl, thật khó mà chấp nhận một người phụ nữ đã lừa mình chỉ vì muốn tìm nơi nương tựa, sự đấu tranh trong suy nghĩ và nội tâm của Karl khiến tôi rất ngưỡng mộ, cuối cùng anh đã chọn Anna, kiên nhẫn với cô hết lần này đến lần khác, đã yêu cô và cũng dạy cô cách yêu mình, dạy dỗ em trai Anna và xây dựng một gia đình đúng nghĩa. 

Mong rằng sẽ được đọc nhiều hơn nữa những cuốn sách của bà.
5
41370
2015-12-29 16:08:29
--------------------------
