414143
3565
Tập này hay quá, Chân Hoàn trở về và hoàn toàn lột xác, không còn mềm lòng như xưa nữa mà quyết tâm trả thù và giải oan cho cả gia tộc. Trong tập này người đọc thấy rõ được sự thông minh của nàng khi nàng bằng lợi ích và bằng tình cảm mà đã có thêm khá nhiều tỷ muội chốn hậu cung. Dù không thể được như Thẩm My Trang nhưng họ cũng sẽ giúp ích không ít cho Chân Hoàn. Tập này Huyền Thanh xuất hiện ít quá, thật đáng tiếc, nhưng vì có chàng mà Chân Hoàn lại có thêm một người giúp sức chính là Diệp Lan Y. Thêm vào đó, truyện lại rất kịch tính vào mấy trang cuối khi Chân Hoàn bị vu gian dâm với Ôn Thực Sơ. Quả thực không thể nán lại được, phải đọc tập 7 ngay thôi!!!
5
31339
2016-04-11 07:41:05
--------------------------
403671
3565
Chân Hoàn đã trở thành thục phi đứng đầu phi tần nhưng đọc đi đọc lại mình vẫn thấy băn khoăn quá rốt cuộc thì trong lòng Huyền Mặc Chân Hoàn có hơn những người khác không,xem ra làm hoàng đế cũng chả có ai thật lòng yêu thương ngoài mặt thì ai cũng nói lời yêu thương hoàng thượng thế này thế kia nhưng có ai thật lòng đâu,nếu có chắc chỉ có Hoa phi nhưng Hoa phi thì quá thê thảm rồi,mới đây lại có thêm Trinh Qúy Tần nhưng Huyền Mặc đối xử với cô ấy thế nào thì đọc ai cũng biết,người ta đang mang thai mà lại phong tước ngay cho tì nữ ở bên cạnh cũng may là còn giứ lại được đứa con.Hoàng hậu cũng chả sung sướng gì còn giứ được ngôi vị lí do là em của Thuần Nguyên,tính kế đủ đường suốt bao nhiêu năm,bị mỉa mai bởi Xương phi ngay cuối tập 6 về thân phận,nghĩ cũng khổ cho bà ta.
5
526322
2016-03-24 03:50:19
--------------------------
365060
3565
Tập truyện là cái nhìn toàn diện về nàng phi tần của thời đại phong kiến , nàng xinh đẹp và kiên cường chống lại quy tắc ban hành của nhà vua mà như một bản tử đến với ai không nghe , làm trái lệnh vua hoặc có thể bị người trong cung hại chết thì nàng vẫn một mực không oan thán , hiền hòa , thánh thiện không như ai sống bên nhà vua , nàng mang thai mà tâm trạng lo lắng , nghi hoặc về sự sống và cái chết ẩn họa khôn lường .
4
402468
2016-01-07 12:26:42
--------------------------
317219
3565
Chân Hoàn bây giờ đã 26 tuổi rồi.Nàng đã trải qua 10 năm sóng gió của thời thanh xuân, hoa hạnh 15 tuổi đã chết.
Quyển này tập trung vào đoạn đường nàng mang thai và sinh hạ cặp đôi long phụng, từ đó bước lên vị trí Thục Phi đứng đầu tất cả tần thiếp của nhà Chu. Bắt đầu khoảng thời gian "cung đấu" dữ dội của nàng và Hoàng Hậu.
Những tưởng nàng đã từ bỏ trái tim của thiếu nữ Hoàn Hoàn ngày nào bên ngoài Tử Áo Thành, ngờ đâu, nàng vẫn là nàng đó, vẫn mang trái tim nhân hậu và dịu dàng, vẫn đầy tình người nhân nghĩa. Chân Hoàn là một nữ tử đáng được trân trọng.

5
308310
2015-10-02 21:17:16
--------------------------
309104
3565
Chân Hoàn ở tập này đã trở thành người đừng đầu phi tần, quyền uy thanh thế đều vô cùng. Trong cung bây giờ không phải chỉ là thiên hạ của một người nữa, thắng thua còn chưa thể định đoạt.
Hoàng hậu càng ngày càng độc ác. lộ rõ sự âm hiểm, tuy nhiên đã chính thức trở thành kẻ địch trong sáng của Thục Phi ( Chân Hoàn), chỉ bằng vài câu nói đã có thể gây hiềm khích cho mối giao hảo tốt của Kính Phi với Thục Phi, khiến một vị phi tử đang mang thai rồng năm lần bẩy lượt suýt trụy thai mà không cần động thụ,  Trinh Quý Tần dù nhận ân lớn của Chân Hoàn rốt cuộc lại sinh lòng đề phòng,... và còn đủ thủ đoạn khác nữa
Nhưng Chân Hoàn cũng không còn là vị Quý Tần của ngày trước nữa.
Cuộc đối đầu giữa Thục Phi và Hoàng Hậu và phe phái đang ngày càng trở nên gay go hơn. Nhưng giữa những tranh đấu đấy, mình thực sự vẫn cảm thấy thương tiếc mối tình giữa Huyền Lăng và Chân Hoàn. Rõ ràng Chân Hoàn vẫn còn tình cảm với Huyền Lăng, chỉ là chút ít ấy không đủ để xóa đi nỗi hận trong lòng nàng, cũng vì sự xuất hiện của Thanh nữa.
5
496784
2015-09-18 22:16:52
--------------------------
268801
3565
Dù đã đi đến tập 6 nhưng Hậu cung chân hoàn truyện vẫn khiến mình không ít lần nín thở vì lo lắng và hồi hộp cho những cạm bẫy đáng sợ được giăng ra nơi chốn cung đình. Chân Hoàn vượt qua bao gian khổ để quay trở lại hoàng cung, tuy nhận được sự sủng ái hết mực của Huyền Lăng nhưng trong thâm tâm nàng vẫn không nguôi day dứt về một mối tình tưởng chừng như là được ở bên nhau trọn đời với người mình yêu thương nhưng phút chốc giấc mộng đẹp ấy đã vỡ tan tành. Những lúc Chân Hoàn ngẩn ngơ nhìn cảnh nhớ người, tôi quả thật thấy thương cảm cho số phận đầy trắc trở của nàng. Trong tập này, tôi cũng đặc biệt ấn tượng với nhân vật Trinh Quý Tần dù rằng ở phiên bản phim truyền hình nhân vật này đã bị lược bớt. Trinh Quý Tần một lòng ôm mối tình si với Huyền Lăng, thậm chí vì y mà sinh con đẻ cái, suýt mất cả tính mạng, nhưng tình cảm của nàng tiếc thay mãi mãi không được đáp lại. Tử Áo thành bề ngoài thật diễm lệ, rạng rỡ nhưng bên trong lại quá u ám, tối tăm.
Tập này kết thúc với 1 chi tiết vô cùng kịch tính và đầy cao trào, dự đoán cho một đợt sóng ngầm mới lại nổ ra nơi hậu cung hiểm ác.
5
10908
2015-08-16 20:56:53
--------------------------
226159
3565
Tập 6 là sự trở về của Chân Hoàn sau mấy năm trở thành phế phi. Vẫn với giọng văn và cách dẫn dắt rất lôi cuốn, hấp dẫn của Lưu Liễm Tử, Chân Hoàn truyện dù kéo dài đến vậy mà vẫn ko khiến người đọc nhàm chán. Xem phim nhiều lần, đọc truyện mới thấy có nhiều chi tiết không giống phim nhưng cũng không làm mất cái hay, nhân vật trong truyện dữ dằn hơn trong phim nhiều, phần nào thể hiện được cái độc ác lạnh lùng nơi cung cấm nhà Chu. Chân Hoàn trở lại đã là một Chân Hoàn mới, không còn là thiếu nữ ngây thơ như trước, những chuyển biến tâm lí nhân vật được viết rất hợp lý. Nói chung tập 6 không làm cho mình thất vọng.
4
258359
2015-07-11 16:27:54
--------------------------
