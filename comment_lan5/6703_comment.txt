463798
6703
Lại thêm một tập truyện hay của Rich Riordan. Những chi tiết trong truyện, thật không ngờ. Mọi sự cố gắng của nhóm Percy tưởng như thu lại được một kết quả tốt đẹp, là một thành công, nhưng hóa ra lại đã nằm trong sự sắp đặt của người khác, là người ta cố tình cho cậu phải làm thế. Ngư ông đắc lợi. Câu chuyện vẫn như 2 tập trước, táo bạo, nguy hiểm,hồi hộp đến nghẹt thở, và một cái kết của chuyến đi, cái kết đầy bất ngờ ở cuối câu chuyện lại hẹn mở ra một câu chuyện mới thú vị và những cuộc phiêu lưu đầy hấp dẫn của percy ở phía trước.
5
920424
2016-06-29 16:56:29
--------------------------
458889
6703
Mình biết tới bộ sách qua 2 phần phim của tập 1 và 2 ( Kẻ cắp tia chớp và Biển quái vật). Quả là không uổng tiền khi đầu tư mua quyển thứ 3 này. Từ cách dẫn truyện thú vị đến các tình tiết gay cấn đều làm mình cảm thấy rất hồi hộp và không thể đợi để được đọc tập 4. Trước giờ mình rất hiếm khi trải nghiệm thể loại sách thần thoại viễn tưởng, tính đọc giải trí thôi không ngờ lại hút mình đến như thế, sau này chắc cũng khó có bộ truyện nào mình thích đọc đến vậy. 
4
190281
2016-06-25 16:23:53
--------------------------
374477
6703
Mình mua cả bộ 6 cuốn để tặng sinh nhật em trai 15 tuổi của mình. Em mình chỉ thích ngồi máy tính chơi game nên mình nghĩ phải làm gì để kéo nó khỏi màn hình. Bất ngờ là em mình đọc một lèo 6 cuốn trong vòng 1 tuần không nghỉ và tấm tắc khen sách mãi. Em mình còn giới thiệu và cho bạn đọc thử nữa. Thế giới thần thoại, những tình tiết phiêu lưu gay cấn là điểm cuốn hút những độc giả nhỏ tuổi. Ngoài ra, giá trị của tình bạn, sự dũng cảm là điều tuyệt vời khác mà bộ sách đem lại cho tuổi thiếu niên. 
5
169228
2016-01-26 13:30:00
--------------------------
370925
6703
Sau tập 2 không hay được như mong của mình thì với tập 3, tác giả đã trở lại cực kì ấn tượng. Đột nhiện xuất kiện thêm 1 đứa con của Bộ Tam Vĩ Đại, rồi thêm một chuyến phiêu mới của Percy,.. Trong tập này các cảnh chiến đấu cũng nhiều hơn và được miêu tả rất sống động, các mẩu đối thoại giữa Percy và Annabeth ngày càng đáng yêu hơn ><
Điều khiến cho seri này đặc biệt chính là văn phong hài hước đầy thông minh của Rick Riordan, nhất là trong các đặt tựa mỗi chương, nhiều khi đọc tựa xong mình cứ ngờ nhà xuất bản dịch sai, ai ngờ đọc xong mới hiểu ẩn ý của tác giả.

5
859331
2016-01-18 18:10:47
--------------------------
347931
6703
Câu chuyện thứ ba trong bô truyện Percy Jackson và các vị thần trên đỉnh Olympus còn hay hơn những series trước nữa. Percy phải đối mặt với lời nguyền của thần titan, không biết cậu có thể sống nổi đến sinh nhật lần thứ mười sáu của mình không nữa ?Cậu phải bảo vệ hai đứa con của Bộ tam vĩ đại khỏi những thuộc hạ của titan. Titan càng ngày càng mạnh hơn, vì thế Percy phải chiến đấu hết sức mới mong thắng được vị thần hung tợn này.Không những thế, Percy còn phải giải cứu cho những người bạn của mình.
5
621431
2015-12-05 11:06:48
--------------------------
336787
6703
Nếu ai đã đọc hai tập đầu thì không thể bỏ qua cuốn thứ ba này. Những chuyến phiêu lưu cứ nối tiếp nhau, mỗi trang sách đều mở ra một điều thú vị, đưa người đọc đến hết bất ngờ này đến bất ngờ khác. Làm ta một khi đã cầm cuốn sách lên thì không thể nào đặt xuống được. Lời văn, câu từ mạch lạc, rõ ràng cùng lối kể chuyện thú vị, sáng tạo, bác Rick đã làm thu hút độc giả đến những trang cuối cùng. Có điều Bookmark hơi mỏng và dễ gãy, nhưng cũng không quan trọng. 
5
477880
2015-11-12 22:22:54
--------------------------
238791
6703
Đây là một series truyện rất đáng tiếc.
Điều đáng tiếc đầu tiên là film chuyển thể từ truyện quá dở.
Điều đáng tiếc thứ hai là người xem film nhiều hơn đọc sách rất nhiều.
Riêng mình, mình thật sự rất thích series này. Mình chưa từng đọc bất cứ sách hay truyện lịch sử nào, về thần thoại Hy Lạp hay La Mã, vì mình thật lòng không bị rù quyến bởi điều đó. Nhưng Percy Jackson đã thật sự hút mình vào thế giới vừa thật vừa ảo ấy, rồi như thể không còn ranh giới giữa thực và mơ, người và thần, tất cả, tất cả đều được bác Riordan vấn víu vào nhau, với sự sáng tạo không ngờ, và cứ mở một trang sách là thêm một bất ngờ mới.
Đó là những đứa con lai - đứa con của thần và người, chạy đua trong cuộc săn đua với những thử thách nơi trại con lai đặt ra, mà đằng sau đó là những đan xen mờ mờ tỏ tỏ nơi cuộc chiến manh nha của các vị thần và Kronos.
Tất cả đều thú vị và hồi hộp đến không tưởng. Đây là một series truyện rất hay
5
21045
2015-07-23 14:08:53
--------------------------
219508
6703
Không khô cằn như thần thoại Hi Lạp, không quá cứng nhắc như những sử thi xưa của Hi Lạp, Percy Jackson đã cho người đọc tìm hiểu những câu chuyện huyền bí nhưng vẫn đậm chất thần thoại từ những cuốn sách gốc, trong phần 3 này chúng ta sẽ gặp lại nhân vật chính chàng Percy - hậu duệ của thần Poseidon và những người bạn đặc biệt là Thalia đứa con của Zues- đấng tối cao.
Những cuộc phiêu lưu mới qua từng trang sách, nếu ai đam mê các câu chuyện thần thoại chắc sẽ không bỏ qua tập truyện này.
Tập 3 nhưng con quái thú mới , những người bạn mới, nhưng con người mang quyền lực mới sẽ vẽ lên một ập sách lôi cuốn khó có thể cưỡng lại ..
5
592786
2015-07-01 15:56:44
--------------------------
213670
6703
Có thể nói, đây là một quyển sách rất đáng đọc. Đầu tiên là về hình thức, có lẽ vì mình thích màu đen nên thấy bìa đẹp miễn chê, tuy nhiên bookmark hơi mỏng. Nội dung thì không có gì đáng nói vì cách sử dụng ngôn ngữ gần gũi, sắc sảo của bác Rick đã thể hiện ở hai tập trước rồi và tập này cũng không gây thất vọng. Cốt truyện mạch lạc, có nút thắt khiến người đọc thấy cuốn hút ở ngay những trang đầu của quyển sách. Tóm lại, mình rất thích quyển sách này.
4
377097
2015-06-24 00:39:18
--------------------------
210695
6703
Đây là bộ sách mình rất thích. Và đây là quyển mình thích nhất trong bộ sách mình thích. Cốt truyện hay, có cao trào, có nút thắt. Bìa đẹp nhưng bookmark hơi mỏng và dễ gãy. Qua quyển sách này, tính cách của các nhân vật chính ngày càng bộc lộ rõ nét hơn. Và ngay cả những nhân vật chính cũng khiến ta cảm thấy lôi cuốn. Mình đã dán mắt vào quyển sách khi vừa nhận được nó. Phải nói sự lôi cuốn từ ngôn từ của bác Rick thật không thể chối cãi được. Thật tuyệt vời!
4
377097
2015-06-19 18:52:08
--------------------------
148469
6703
Đây là cuốn sách mình thấy hay nhất trong seri Percy Jackson. Rick Rordan đã rất khéo léo trong việc miêu tả vẻ đệp của các vị thần ( nhất là Aphroditer ). Còn đoạn Percy nâng bầu trời cũng thể hiện được tài năng của tác giả, rất thật và cảm xúc.Một cuốn sách quá hay, lời văn, câu từ đều rõ ràng, mạch lạc. Tính cách của các nhân vật đều được phác họa một cách sâu sắc, không quá bộc lộ mà để khán giả từ từ tìm hiểu.Sự xuất hiện của Nico, con trai thần Hades và việc Thalia gia nhập đội Thợ săn khiến cho lời sấm truyền vẫn chưa rõ sẽ nói đên ai. 
Nói chung là rất hay, mình khuyên các bạn nên mua cuốn sách này.
5
418341
2015-01-10 19:24:01
--------------------------
147725
6703
Sau khi khá buồn với tập hai thì mình đã phát cuồng với tập 3. Một cuốn sách quá hay, lời văn, câu từ đều rõ ràng, mạch lạc. Tính cách của các nhân vật đều được phác họa một cách sâu sắc, không quá bộc lộ mà để khán giả từ từ tìm hiểu. Qua cuốn sách, ta đã rất đau lòng với nhiều sự hy sinh, mất mát chỉ để đỉnh Olympus tồn tại. Và đoạn cuối là đoạn chúng ta tưởng chừng như Percy chắc chắn là người anh hùng nhưng bất ngờ thay, một người con của Bộ tam vĩ đại lại xuất hiên, mang thêm nhiều nỗi lo âu cho các nhân vật. Tuy nhiên, nhà xuất bản lại làm mình buồn. Bìa không đẹp, dịch không được sát nghĩa lắm và lâu lau lại gặp lỗi đánh máy. Hầu hết sách Chibooks đều gặp tình cảnh này nên mình mong họ sẽ cẩn thận hơn trong việc in ấn, xuất bản.
5
509107
2015-01-08 17:13:17
--------------------------
146375
6703
Lời nguyền của thần Titan chính thức đánh dấu sự trỗi dậy của các Titan dưới quyền Kronos. Trong cuộc phiêu lưu giải cứu cho Annabeth, cả nhóm đã phải đối mặt với một trong các Titan hùng mạnh nhất, Atlas. Chính nhờ sự dũng cảm của cả nhóm và sự hy sinh cao cả mà cuối cùng âm mưu của Atlas đã thất bại, đội quân của Kronos vẫn bị ngăn cản. Sự xuất hiện của Nico, con trai thần Hades và việc Thalia gia nhập đội Thợ săn khiến cho lời sấm truyền vẫn chưa rõ sẽ nói đên ai. Chi tiết đặc biệt của truyện đó là việc Luke có thể đỡ được bầu trời tiết lộ rằng Luke sau này sẽ trở thành một anh hùng cũng khiến cho người đọc tò mò chờ đợi phần tiếp theo.
5
291067
2015-01-03 16:14:30
--------------------------
