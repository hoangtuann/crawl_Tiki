423604
8176
Cuốn sách Kỹ Thuật Chế Biến Món Sushi này về hình thức rất đẹp, có nhiều hình ảnh minh họa bắt mắt và đặc sắc, chất lượng giấy tốt. Còn về nội dung thì mình thấy hay, bởi vì ngay phần đầu tác giả giới thiệu cho người đọc sơ qua về nguyên liệu của sushi, rồi hướng dân làm các thể loại sushi. Ăn sushi nhiều rồi nhưng cũng cón hiều cái như nguyên liệu chế biến mình chưa được biết đến. Mua cuốn này về chủ yếu là ngắm hình ảnh ^^ Nói chung sách hay, dành cho ai muốn tìm hiểu về văn hóa ẩm thực Nhật Bản
5
854660
2016-04-29 17:29:53
--------------------------
411337
8176
Cuốn sách này có giá khá đắt tuy nhiên rất đáng đồng tiền, sách có minh họa rất nhiều ảnh cụ thể về cách chế biến sushi, cách trang trí, rất đẹp và dễ hiểu. Dành cho những ai mới bắt đầu học làm vì có những chỉ dẫn từ cơ bản nhất cho rất nhiều loại sushi khác nhau, bạn có thể học hỏi được rất nhiều qua cuốn sách này, các bạn có thể có những món sushi ngon đúng cách và còn đẹp nữa.
Mình đã làm được một món sushi ngon cho cả nhà thưởng thức nhờ cuốn sách này.
4
306081
2016-04-05 21:15:15
--------------------------
