445893
5733
Đây là lần thứ 2 mình đọc một cuốn sách liên quan đến lịch sử NB. Cuốn đầu tiên được viết dưới nhãn quan của người Mĩ nên người NB và nước NB không quá được tôn lên cao, ngược lại, đó là sự nhìn nhận mang tính chỉ trích phê bình Thiên hoàng NB (người may mắn thoát án tội phạm chiến tranh 1945), đồng thời đề cao vai trò của tướng Marshall của người Mĩ. (Bí mật triều đại Yamato, Sterling Seagrave & Peggy Seagrave, NXB Trẻ). Cuốn Shogun cuối cùng do tác gia người Nhật viết nên nhãn quan đương nhiên sẽ khác, tôn trọng và say mê nhân vật lịch sử của chính đất nước mình, điều đó ko có gì sai. Những gì mình rút ra được từ cuốn Shogun cuối cùng là cảm xúc lẫn lộn, mang tính xót xa cho dân tộc Việt Nam khá nhiều (Yoshinobu là một tay chính trị gia lão luyện, luôn tính nước lùi cho mình, và luôn sợ bị lịch sử phán xét tội phản nghịch, đó là một kỹ thuật chính trị điêu luyện, khả năng đánh giá tình hình đất nước với bên ngoài chính xác, biết người biết ta...từ đó mới có quyết định táo bạo là xoá bỏ chế độ Mạc phủ (chối bỏ trách nhiệm gánh vác đất nước)...ở một khía cạnh nhất định, con người ta ở trên đỉnh cao danh vọng và đứng đầu gia tộc lãnh đạo thì khó mà tuqf bỏ được vị trí đó, lo sợ chiến tranh vì biết mình ko đủ lực, dám phủ định quyết định tham chiến trước đó do chính mình đưa ra thì Yoshinobu quả thực là dũng cảm (có lẽ ông bị ám ảnh bởi sự đánh giá của hậu thế, nên luôn cân nhắc). Ở nước mình có lẽ chưa có cá nhân nào như vậy trừ trường hợp Hoàng đế Lý Chiêu Hoàng, thời duy nhất mà sự chuyển giao quyền lực ít đổ máu nhất (sự đổ máu là do gia tộc nhà Trần và những người phù Lý gây ra). Có thể lúc đó LCH quá nhỏ dại, nhưng sau này bà cũng ko nuôi dưỡng lòng thù hận để dấy binh đao. Còn lại lịch sử của Việt Nam vì cá nhân chủ nghãi, gia tộc chủ nghĩa nên đất nước luôn vướng vào vòng binh đao khói lửa chém giết nồi da nấu thịt, gần nhất là VNCH và VNCSCN.  Tóm lại, đọc mới thấy vai trò và nhận thức của cá nhân đã làm thay đổi thời cuộc, may mắn cho dtoc NB có những cá nhân dâm gạt bỏ cái sân si của mnhf để phục vụ thời cuộc. Tiếc cho Việt Nam chưa có những cá nhân đó (có thể đã tưngd có như ông Diệm chẳng hạn). Hi vọng và tích cực lên.
4
858392
2016-06-11 08:33:32
--------------------------
398522
5733
Đứng trên vị trí của một người hiếu chiến, có thể thấy nhân vật chính đã không thỏa được ý chí phàm nhân khi nhất mực tránh né quyền lực và chiến tranh để dẹp yên đối phương. 
Tuy nhiên để nhịn được đến cùng, để tránh hậu hoạ và để đất nước phát triển thì quả là phải dẹp bỏ tư duy cá nhân, hay ở đây là cả quyền bính gia tộc. Thật may mắn khi sử sách được chép chân thật và xuất hiện nhà văn có tâm giúp người đời hiểu được Tướng quân Yoshinobu, bởi lẽ trong chính trị, kẻ thua sẽ tan biến mãi mãi. 
Truyện khá phức tạp, dịch thuật tốt nhưng đôi chỗ vẫn chưa chính xác. Mong rằng Nxb sẽ cho hiệu đính kỹ càng trong lần tái bản sau. 
Rất đáng đọc.
3
822265
2016-03-16 13:19:01
--------------------------
270716
5733
Đây là một quyển hơi khó đọc, nhất là phần đầu. Có lẽ phải đọc đi đọc lại vài lần mới thấm được, vì để xuất hiện nhân vật chính tác giả dẫn dắt khá lâu.
Tuy nhiên khi đã nắm được cốt truyện rồi thì sẽ thấy quyển sách này rất hay. Mình thích nhất đoạn Tokugawa Yoshinobu rời Kyoto, ông đã từ bỏ cuộc chiến, tự yêu cầu xóa bỏ chế độ Mạc phủ, trả lại quyền cai trị đất nước cho Thiên hoàng mặc dù ông có thể thắng cuộc chiến đó và tiếp tục trở thành người quyền lực nhất đất nước. 
Và câu chuyện về Mạc phủ cũng là một bằng chứng sống về luật nhân quả: Mạc phủ đã đặt ra cả một hệ thông nhiều gia đình để có người nối dõi, tuy nhiên càng về sau người chết yểu càng nhiều để đến khi buộc phải chọn Yoshinobu, người duy nhất có khả năng lãnh đạo Mạc phủ nhưng lại là người có tư tưởng tiến bộ đến mức sẵn sàng kết thúc chế độ Mạc phủ.
5
501684
2015-08-18 15:38:14
--------------------------
253176
5733
Shiba Ryotaro đã phác họa nên chân dung một con người làm chuyển biến xã hội Nhật Bản thời kì trước và đầu duy tân Minh Trị. Không có quá nhiều hình ảnh samurai với thanh kiếm sáng loáng, không có những tuyệt kĩ đầy hoa mĩ, không có một vị tướng quân với hình ảnh như tưởng tương. Đây là một quyển sách hay nhưng tác giả Shiba Ryotaro viết với tình cảm dành cho nhân vật chính có lẽ hơi nhiều quá khiến cho nhiều đoạn đọc cảm thấy tác giả để xen lẫn tình cảm vào đó. Nhiều chi tiết mang nội dung lặp lại là những lời miêu tả phẩm chất vượt bậc của Yoshinobu, nhưng đây có thể là một quyển sách quý mang tính lịch sử về những biến động của thế kỉ 19 tại Nhật Bản. Trong sách còn vài chỗ viết sai năm và tên người, cách dịch hán âm các tên địa danh, người không đều khiến cho giảm đi 1 phần chất lượng quyển sách.
5
401493
2015-08-04 10:59:17
--------------------------
248609
5733
Chỉ riêng tựa đề của cuốn sách cũng đã gợi lên cảm giác về sự suy tàn của thời Mạc phủ. Những câu chuyện viết ra còn là sự nuối tiếc của tác giả đối với một nền võ đạo lâu đời của đất nước Nhật Bản, người đọc có cảm giác như  tác giả đang nhìn với cặp mắt nuối tiếc của một cậu bé. Tuy nhiên, không dừng lại ở chỗ tiếc nuỗi những giá trị đã dần lùi xa, tác phẩm còn khẳng định những giá trị mới của xã hội Nhật Bản do tư duy đổi mới mang lại. Qua đó, tác phẩm đã đề cao những giá trị duy tân mà những người mới đã mang lại cho xã hội Nhật Bản  và chỉ ra nguyên nhân sự phát triển thần kỳ của đất nước Nhật Bản.
5
24486
2015-07-31 10:07:10
--------------------------
194331
5733
Shogun cuối cùng của tác giả Shiba Ryotaro đã khắc họa nên hình ảnh của một samurai có một cái "đầu lạnh" giữa lúc dầu sôi lửa bỏng của đất nước. Thay thế cho hình ảnh một võ sĩ đạo luôn cầm gươm và tấn công kẻ thù là một samurai có một trí tuệ sáng suốt. Sau khi đọc xong quyển sách nói về Tokugawa Yoshinobu, tôi thiết nghĩ có lẽ do trong thời kỳ đầy biến động của đất nước mà nước Nhật lại may mắn có được những người có đầu óc duy tân nên họ mới phát triển rực rỡ như ngày hôm nay. Quyển sách có rất nhiều điều đáng để ta có thể học hỏi được từ các bậc tiền nhân.
5
387632
2015-05-10 09:56:27
--------------------------
