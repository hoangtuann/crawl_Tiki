206258
3760
Điểm trừ nho nhỏ là về mặt hình thức. Là cuốn sách rất dày nên nhiều trang được in không đẹp, chữ bị mờ khó đọc. Ngoài ra bìa mềm và rất mỏng nên sau mấy ngày sử dụng dễ bị cong.
Điểm cộng lớn của cuốn sách này lại là về nội dung rất tuyệt vời cũng như giá trị mà cuốn sách đem lại cho người đọc. Các đề được đưa ra có ưu điểm: câu hỏi phong phú đa dạng và không bị trùng lặp; sát với đề thi chính thức; bao quát được nhiều phần học: hoá học vô cơ, hoá học hữu cơ, ứng dụng của hoá học, hoá học với môi trường, ... Đáp án là phần mà mình ưng ý nhất: đáp án cho mỗi đề đều chính xác, cụ thể rõ ràng, giải tỉ mỉ từng bài nên khi học có thể biết được cách làm cho bài đó sao cho đúng nhất, nhanh nhất.
Một cuốn sách khá hay và bổ ích.
4
466113
2015-06-09 10:45:23
--------------------------
