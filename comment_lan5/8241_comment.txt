456699
8241
Mình có một cu con mới một tuổi mà hiếu động lắm, không lúc nào yên. Cũng như bao cha mẹ khác, vợ chồng mình mong con  lớn khôn và khỏe mạnh. Quyển sách này mình thấy rất hữu ích, sách viết và mô tả các thời kỳ phát triển của con, những tình huống cần giải quyết như thế nào. Sách khá là dày so với giá tiền, tuy nhiên chất liệu giấy thì không được đẹp. Bù lại, mình dùng dịch vụ bao sách nên nhìn cũng khá ổn. Một quyển sách hay, các ông bố bà mẹ nên xem.
4
615987
2016-06-23 17:24:01
--------------------------
337155
8241
Khi mới tia thấy cuốn này mình đã đặt ngay và luôn. Nội dung sách là các phương pháp để dạy dỗ con trẻ một cách đúng đắn và rất hợp lí , từ dạy con những thứ rất nhỏ nhặt đến những phương pháp uón nắn dạy dỗ trẻ ra sao để chúng hiểu thế nào là đúng, là sai. Rồi cả những tình huống xử lí các mâu thuẫn giữa cha mẹ với trẻ. Đọc cuốn sách này có thể giúp chúng ta tìm ra cách tốt nhất để giáo dục trẻ. Một cuốn sách hay. Tuy nhiên chất lượng giấy không được đẹp lắm, hơi ó vàng. Mong nhà xuất bản có thể khắc phục.
4
714420
2015-11-13 18:40:28
--------------------------
255510
8241
Con trai 3 tuổi của tôi là một anh bạn hiếu động và nhiều lúc tôi cũng lúng túng không biết phải cư xử thế nào với anh ấy cho phải. Những lúc anh ấy làm tôi phát điên thì tôi lại lấy sách này ra đọc, hầu như mọi tình huống tôi gặp phải với anh bạn đó đều có trong sách, tôi biết cách kiểm soát tình huống hơn, biết cách hướng dẫn ảnh đi theo hướng đúng, biết anh bạn nhỏ sẽ có những bước phát triển suy nghĩ gì tiếp theo để điều chỉnh cách dạy bảo em cho đúng. Đây là cuốn sách tôi thấy rất thú vị.
5
575234
2015-08-06 08:52:31
--------------------------
