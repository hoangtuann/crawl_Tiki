450659
5713
Ấn tượng đầu tiên của mình về cuốn sách này là cái tên "Mụ ghẻ" rất ấn tượng làm mình tò mò, và bìa sách cũng khá đẹp. Nhận được sách mình rất thích vì được tặng kèm 4 bookmarks dễ thương. Nội dung tác phẩm là nói về những khó khăn của những con người có liên quan trực tiếp và gián tiếp đến mối quan hệ mẹ ghẻ con chồng và mình rất thích cái cách kết thúc truyện của tác giả, có hậu nhưng không khiên cưỡng. Không tính phần biên tập chưa kĩ còn nhiều lỗi chính tả thì mình cho phần nội dung và hình thức của sách 5 sao.
5
665690
2016-06-19 00:38:06
--------------------------
373007
5713
-Ưu điểm: Nội dung mới lạ. Nói về cuộc đời sống gió của Du. Du đã mất mẹ từ nhỏ nhưng không hề hay biết, khi cô lên mười ba của Du đã đi bước nữa vì nghĩ cô cần sự yêu thương của người mẹ. Nhưng cô đã hok hiểu đc điều đó, cô căm ghét dì Hạnh, người phụ nữ hết mực yêu thương cô. Và Du đã quyết định bỏ đi vì cô nghĩ bố Hà sẽ được hạnh phúc. Khi lớn lên, cô gặp một người tên Viễn và đem lòng yêu anh ta. Vào một buổi tối trên sân thượng, Viễn đã cầu hôn cô, nhưng nếu cô nhận chiếc nhẫn đó đồng nghĩa với việc cô sẽ trở thành mẹ kế của GiGi (Giang). Tuy cô hết mực yêu thương, quan tâm, chăm sóc Giang nhưng Giang đã từ chối tình cảm của cô và xem cô chỉ là "Mụ ghẻ" hay "Người giúp việc" không hơn không kém …
-Nhược điểm: Khi đọc quyển này mình thấy được nhiều lỗi chính tả, có lúc còn nhầm lẫn tên nhân vật.
3
1125289
2016-01-22 19:45:26
--------------------------
366876
5713
Tình mẫu tử luôn là tình cảm thiêng liêng nhất  mà không ai có thể chối bỏ. Câu chuyện dẫn dắt người đọc hết sự việc này đến sự việc khác làm trái tim người đọc không khỏi thổn thức. Một cuộc đời, một số phận được nói đến trong chuyện mà như bao nỗi đau đang còn rỉ máu ngoài kia. Người mẹ luôn chấp nhận sự hi sinh vô điều kiện, bởi họ biết những đứa con của họ thiếu vắng bàn tay chăm sóc và tình thương của mẹ sẽ thiệt thòi đến như thế nào. Câu chuyện có một kết thúc đẹp thoải những mong mỏi của bạn đọc. Vậy là tình thương chân thành cùng sự hi sinh của người mẹ ghẻ đã giúp bà có một mái ấm thật sự. 
Sách có tặng kèm bookmark rất ý nghĩa về tình mẹ, mình rất thích.
5
584199
2016-01-10 22:33:21
--------------------------
315107
5713
Đọc truyện của Lâm Phương Lam, có một sự đảm bảo về nội dung nhất định do tôi đã đọc các tác phẩm khác của cô. Vì thế, Mụ ghẻ cũng không phải là ngoại lệ. Tôi thấy có chút đồng cảm với Du khi cô bị "ngược đãi" bởi con ghẻ. Nhưng trước đó, tôi thấy cô thạt cố chấp với mẹ ghẻ của mình. Nhưng, số phận của cô cũng không hề vui vẻ chút nào khi gặp phải sự cố đầu đời với Cậu Tư, rồi với tình yêu đầu. Mình thông cảm cho cô khi yêu mà vẫn chia tay với người yêu đầu đời. Vì tuổi trẻ thường là thế. 
Thương nhất lúc Du ra đi, tự mình xoay sở cuộc sống với cái thai trong bụng. Nhưng, chồng của cô that đáng trách. Mình không thích người đàn ông mới chỉ nhìn thấy thôi, mà đã vội kết luận. Đành rằng anh ta cuối cùng hiểu ra, nhưng mình vẫn không thích lắm cách Phương Lam xây dựng nhân vật này. 
4
572177
2015-09-28 09:09:25
--------------------------
256941
5713
Đọc "Mụ ghẻ" của Lâm Phương Lam ta thấy thương cho thân phận của những người mẹ kế. Dù tốt dù xấu họ vẫn bị gắn cho mình một ác cảm khó xóa bỏ đặc biệt là ác cảm từ những đứa con riêng của chồng. Truyện với lối hành văn nhẹ nhàng, tinh tế, chỉ chủ yếu xoay quanh những mâu thuẫn, xung đột giữa mẹ ghẻ con chồng, đan xen hiện tại và quá khứ nhưng lại vẫn khiến người đọc khó dứt ra được, cứ muốn cố gắng đọc cho trọn, đọc để đi đến tận cùng cảm xúc với Du, với Giang...
3
627466
2015-08-07 11:40:01
--------------------------
244499
5713
Thấy tên truyện khá lạ tai, bìa truyện lại đẹp, lại đúng phong cách mình thích nên đặt ngay và luôn. Dự định là ngấu nghiến cả tuần không ngờ truyện cuốn hút quá phải đọc hết một lần cho nóng. Lối viết văn của tác giả như một người từng trải, như lôi cuốn trái tim độc giả hòa cùng cảm xúc với nhân vật. Đọc xong truyện cũng đã hơn tuần nay mà mình lưu luyến không thôi, dự định sẽ đọc lại thêm vài lần nữa và còn định chia sẻ cho bạn bè cùng đọc để rồi cùng bàn luận. Bạn nào còn đang phân vân thì mau mua ngay kẻo tiếc nhak!!!!!!!!!!
5
692629
2015-07-28 11:22:17
--------------------------
200320
5713
Đọc xong cuốn sách trong tôi đọng lại những cảm xúc đặc biệt. Những câu chuyện trong sách rất gần gũi và chân thực như thể khi bạn bước ra ngoài đời sẽ bắt gặp những người phụ nữ giống như Du, như dì Hạnh hoặc là các cô bé tuổi tím như Giang. Tôi khá thích giọng văn của Lâm Phương Lam trong "Mụ ghẻ", mạch lạc, truyền cảm và sống động. Tác giả nắm bắt được tâm tư, tình cảm của các nhân vật và viết thành câu chuyện theo một cách rất riêng biệt. Có thể tôi là một "fan cuồng" của tiểu thuyết tình yêu nhưng khi đọc "Mụ ghẻ", cuốn sách đã đem lại cho tôi những trải nghiệm giá trị nhiều ý nghĩa về những thứ tình cảm khác biệt, không chỉ là tình yêu. Nói ngắn gọn, đây là một cuốn sách hay. Chúc cho tác giả sẽ xuất bản nhiều những cuốn sách hay như thế này nữa. ^^
5
27232
2015-05-24 16:35:15
--------------------------
179504
5713
Buồn vui lẫn lộn, đó là cảm xúc của tôi sau khi đọc hết câu chuyện đến gần 3h sáng. Tác giả cho tôi một cái kết thỏa mãn, nhưng vì thỏa mãn, lại khiến tôi khóc, khóc vì Du được hạnh phúc trải qua bao thăng trầm, khóc vì một Du đã phải chịu đựng và trả giá qua cái thời trẻ tuổi nông nổi từ Quảng Ninh vào đến Bạc Liêu, khóc vì một bé GiGi ngỗ ngược và đáng thương, khóc cho tình đầu không thành để rồi Du trở thành Mụ ghẻ. Nói chung, đây là một câu chuyện hay, ấn tượng, người đọc phải suy ngẫm khá nhiều về cuộc đời của từng nhân vật.
Văn phong của chị thuần Việt, dễ hiểu, có chút chua xót và đầy thương cảm, khác với sự dịu dàng, quyến rũ trong tiểu thuyết Say đắm mà tôi từng đọc. Mong chờ tác phẩm mới của chị.
5
566059
2015-04-07 13:57:46
--------------------------
177487
5713
Tác phẩm này theo mình là hay nhất trong số các tác phẩm từng xuất bản của chị. Cốt truyện nghe qua cứ tưởng là cũ, không mới, nhưng thực ra lại có rất nhiều điểm hay và sâu sắc.  Mẹ ghẻ - con chồng thời nay không còn gắn liền  với những định kiến xã hội cũ kĩ nữa. Có thể nói đây là 1 quyển truyện cổ tích thời hiện đại. Văn phong của tác giả cũng giản dị và gần gũi hơn (mình rất dị ứng văn phong trong quyển Say đắm) Mong chờ các tác phẩm tiếp theo của chị!
4
476360
2015-04-03 11:10:47
--------------------------
175534
5713
Những ai từng đánh giá văn phong tác giả Lâm Phương Lam mang hơi hướm phương tây là một sự sai lầm, giọng văn của chị tôi có thể khẳng định rằng rất Việt, rất gợi cảm, rất da diết, và rất riêng; điều ấy thể hiện chín muồi qua tác phẩm “Mụ ghẻ”. Tôi đọc hết cuốn tiểu thuyết chỉ trong vòng một buổi tối, bởi sự lôi cuốn buộc tôi không thể dứt ra được. Dù tác phẩm không hướng tới đề tài gì quá to tát nhưng chính những bình dị, mộc mạc, chân thật và gần gũi càng dễ đi vào lòng người đọc. Cách dẫn truyện thu hút, các tình tiết sinh động, các diễn biến nhiều cảm xúc, nhân vật được khắc họa tràn đầy tâm tư và trải nghiệm, kết hợp cùng cái hồn của truyện đã mang đến một “Mụ ghẻ” thành công hơn mong đợi.

Và qua đây, tôi cũng muốn gửi lời chúc đến tác giả, hy vọng chị sẽ mãi giữ trọn niềm tin và tình yêu thương, để trải lòng cùng độc giả, viết lên nhiều thêm những trang văn tươi đẹp và giá trị soi sáng cuộc đời.
5
6502
2015-03-30 14:29:32
--------------------------
169021
5713
Mụ Ghẻ - một cuốn cổ tích thời hiện đại.
Tác giả đã khéo léo trong việc lồng ghép câu chuyện ở quá khứ với hiện tại, có thể nói như là "quả báo" của Du khi mà những chuyện cô đã gây ra với mẹ ghẻ  ngày xưa đều xảy ra tương tự với cô. Tuy nhiên, truyện lại có một cái kết khá bất ngờ, đầy xúc động. Với lối viết nhẹ nhàng, văn phong giản dị, Lâm Phương Lan đã thể hiện được rõ tâm lí cũng như tính cách của từng nhân vật.
 Bìa sách thì rất đẹp, đơn giản. Điểm trừ duy nhất là sách có nhiều lỗi chính tả. Nhìn chung thì đây vẫn là cuốn sách hay, đáng đọc và suy gẫm.
4
470237
2015-03-17 14:33:33
--------------------------
164965
5713
Quyển này hoàn toàn khác những truyện Việt Nam trước đây mình từng đọc. Không màu mè, không đi theo hướng ngôn tình hay ảo tưởng mà trái lại, nó rất chân thực và xúc động. Motip mẹ ghẻ con chồng được tác giả lồng ghép khéo léo, khiến câu chuyện thu hút thực sự. Những diễn biến tâm lý của Du rất hay, mình rất thích đọc. Duy chỉ có điều không thích là cái bìa không được đẹp và không liên quan lắm đến nội dung truyện. Quả thực đây là một trong những truyện Việt hiếm hoi mà mình muốn giữ lại trên giá sách ở nhà.
4
469377
2015-03-09 11:24:19
--------------------------
160747
5713
Vì đang là mẹ của một cậu bé 2 tuổi không phải con ruột của mình nên tôi ít nhiều bị ấn tượng với tựa sách, màu bìa sách, chậu cây màu hồng và có chút tò mò liệu có mình ở đâu đó trong cuốn sách này...Hơn nữa cũng muốn tìm hiểu thêm tâm lý trẻ từ 10 tuổi đối với người phụ nữ không phải mẹ ruột mình, có khi cũng có thêm chút kinh nghiệm để xóa nhòa ranh giới ấy để "con ghẻ" trở thành con, và "mụ ghẻ" trở thành mẹ, yêu thương nhau như tất cả các cặp mẹ con khác.
3
554704
2015-02-25 13:42:47
--------------------------
160584
5713
Đây là lần đầu mình đọc truyện của tác giả, cuốn sách được tặng trong dịp chơi game online. Phải nói là mình vô cùng ấn tượng, xúc động thực sự trước Mụ Ghẻ.
Tác giả khéo léo.lồng ghép tình yêu trai gái trong mô típ mẹ ghẻ con chồng, vưà là giữ độc giả lớn tuổi, vừa là kéo các bạn trẻ thích truyện mùi mẫn yêu đương.
Ngôn từ tác giả sắc bén, văn phong nhuần nhuyễn rất Việt Nam, đến mức ban đầu mình cứ tưởng tên tác giả là Trung Quốc. Mình biết thêm một vài nét đặc trưng ở các tình thành Quảng Ninh hay Bạc Liêu, những nơi mình chưa có cơ hội đặt chân đến.
Tác phẩm để lại nhiều suy ngẫm cho người đọc.
Nội dung không lạ nhưng cái cách tác giả lồng ghép quá khứ thực tại rất đặc biệt, khớp chi tiết và gợi tò mò.
5
564230
2015-02-24 21:35:11
--------------------------
159861
5713
Có lẽ Mụ ghẻ là quyển truyện hay nhất tôi từng đọc . Ấn tượng đầu tiên của tôi là về bìa sách . Bìa sách đẹp bởi màu nâu nhạt của bìa . Mụ ghẻ là câu chuyện viết về cuộc đời của cô bé Du khi còn nhỏ và lúc trưởng thành . Khi chưa đầy một tháng tuổi Du bị mất mẹ trong một tai nạn ở lò than. Nhiều năm sau ông Hà - cha của Du đã cưới một người vợ lẻ mà không có tình yêu chỉ với mục đích là muốn bù đắp cho đứa con của mình . Nhưng Du nghĩ việc lấy dì Hạnh-dì ghẻ là ông Hà đã quên mẹ ruột của Du - ngươif mà cô ngày đêm mong ngóng . Với suy nghĩ đó cô đã cố gắng tỏ ra xa lạ với dì Hạnh . Khi cô lớn lịch sử ấy cũng đã lâp lại . Để quên đi mối tình đầu Du đã lấy Viễn mặc dầu anh ấy đã có một đời vợ và một đứa con gái 14 tuổi  . Cô con gái này cũng vì lòng đố kị mà xem Du không ra gì vì nghĩ rằng bố cô phải sa sẻ một nửa tình cảm của mình cho Du . Câu chuyện này làm cho tôi rất xúc động về cuộc đời của nhân vật Du
5
140681
2015-02-20 00:10:37
--------------------------
151729
5713
Tủ sách của tôi có khá nhiều truyện, mua ngót nghét cũng 4 năm nay, tuy nhiên, hiếm truyện Việt Nam.
Tôi không thích truyện Việt Nam bởi một số thì motip quá nhàm chán, xây dựng nhân vật thì nhảm nhí, hành văn non nớt. Đặc biệt là ghét cay ghét đắng những tác phẩm đạo văn phong của nước ngoài. Vì thế, truyện VN không được tôi chào đón.
''Mụ ghẻ'' đã thu hút tôi ngay lần đầu tiên bởi cái tên đậm chất VN, ngắn gọn xúc tích mà nhiều hàm ý, sau đó, tôi đã mua về và đúng là nó không làm tôi thất vọng, cốt truyện hay, xây dựng nhân vật và cốt truyện mới lạ, tôi rất thích. Tuy nhiên, tôi không thích cách tác giả đặt tiên nv nữ chính, có vẻ giống tên TQ và một vài chi tiết thì có vẻ không thực tế cho lắm. 
Tóm lại đây vẫn là 1 tác phẩm VN đặc biệt nằm trong tủ sách của tôi!
4
37295
2015-01-20 21:01:41
--------------------------
145698
5713
Đây là một câu chuyện lạ lùng! Theo tôi là như vậy.
Nhân vật chính trải qua từ đau thương này đến bất hạnh khác theo đúng luật nhân quả "gieo nhân nào, gặt quả nấy". Bởi khi Du từng "khơi chiến" với mẹ kế của mình, thì khi trưởng thành, con ghẻ của Du là GiGi lại "chiến tranh tưng bừng" hơn. Tôi dùng "chiến tranh tưng bừng", bởi với cá nhân tôi, những cuộc chiến đó rất trẻ con, rất hài hước - theo đúng độ tuổi 14, bất cần, dậy thì, chỉ có bản thân mới là đúng, mới là nhất.
Tựa đề ngắn gọn, thay vì "dì ghẻ", tác giả gọi là "Mụ ghẻ", ngôn từ trong câu chuyện của tác giả dung dị nhưng sắc bén, tình tiết hợp lí, nút thắt mở nhịp nhàng. 
Một cuốn sách dễ đọc, hay, và đáng suy ngẫm.
Nhưng tôi khá tiếc cho mối tình đầu. Nhưng dù gì, nếu mối tình đầu không vỡ, thì làm sao cô ấy trở thành Mụ ghẻ được.
5
526794
2014-12-31 16:03:07
--------------------------
141335
5713
Cuốn sách này tôi được học sinh tặng dịp 20/11.
Tựa đề rất cuốn hút, gây tò mò. Sách đẹp, bookmark ý nghĩa, giấy không hại mắt…, nhìn chung, tôi rất hài lòng.
Tác giả là cây bút trẻ, điều tôi bất ngờ là số lượng sách được giới thiệu đã xuất bản rất đồ sộ, chỉ có tiểu thuyết và truyện dài. Nên tôi đã không ngần ngại mà đọc tác phẩm luôn.
Cách hành văn độc đáo, từ miêu tả ít nhưng gợi nhiều. Ngôn ngữ sắc bén. Nội dung rất rõ ràng (nhưng rất ám ảnh sau khi kết thúc câu chuyện). Cách xây dựng nhân vật độc đáo, phong phú, từ khi chỉ là 1 đứa trẻ 6 tuổi cho đến lúc trưởng thành, có gia đình, và sinh em bé…, với hai mối tình thời thanh xuân và khi đã chính chắn.
Truyện không có nhiều cảnh nóng, tuy nhiên tôi nghĩ cuốn sách nên dán mác 18+ để giới hạn độ tuổi, vì học sinh của tôi mới lớp 8, mà các em ấy đã biết đến cuốn sách này thì có vẻ không tốt lắm ( Dù cuốn sách cho độc giả 1 cái nhìn mới về suy nghĩ, tâm lí con trẻ).
Ngoài ra, chỉnh sửa bản in còn một số lỗi chính tả, mặc dù không nhiều, nhưng khiến người đọc cảm thấy khá khó chịu.
5
510250
2014-12-15 09:43:17
--------------------------
141135
5713
" Mụ Ghẻ " không phải là câu chuyện cổ tích cổ xưa mà nó là câu chuyện cổ tích hiện đại. Với lối viết nhẹ nhàng, truyện của Lâm Phương Lam đã dễ dàng chạm đến trái tim của người đọc. Như một lẽ tự nhiên của cuộc sống, cuộc sống tuân theo luật nhân quả, có vay ắt sẽ phải trả. Ngày xưa Du đối xử với dì Hạnh thế nào thì ngay bây giờ Du lại bị đứa con riêng của chồng đối xử như vậy, thậm chí còn tệ hơn rất nhiều so với cô trước kia. 
Tôi đặt biệt thích cái cách mà tác giả miêu tả về những thay đổi của Du ở độ tuổi mới lớn, nó không hề cứng nhắc, không hề gượng gạo mà nó rất tự nhiên. Thậm chí tôi còn phải bật cười về những suy nghĩ quá nặng nề của Du.
Tuy nhiên, trường hợp của Du có phần hơi đặt biệt, vì đâu phải ai có cuộc sống bình thường cũng thích chê bai, châm chọc những người trong hoàn cảnh như Du đâu, cũng có người cũng biết thông cảm và thấu hiểu cho Du mà. Nhưng cuộc sống của Du không được gặp những người ấy?
Cuộc sống sẽ thật đẹp nếu ai cũng biết nghĩ cho nhau. Chỉ cần có người buông bỏ những định kiến xưa cũ, đừng quá đặt nặng cái tôi của mình thì sẽ có người vị tha và bao dung cho tất cả những lỗi lầm mà tuổi trẻ ta cứ cố chấp cho là đúng. Với tình yêu của một người mẹ - dù không phải mẹ ruột, cùng với tình yêu sâu đậm dành cho Viễn, những tha thứ và học cách thay đổi quan điểm của mình đã khiến câu chuyện mang hồi kết thực tốt đẹp, khép lại một câu chuyện cổ tích - chuyện cổ tích thời hiện đại.Và tôi tin " Cái gì xuất phát từ trái tim sẽ chạm đến trái tim."
Đọc " Mụ Ghẻ " để cảm thông cho những số phận của người phụ nữ. Và nếu bất kì một ai trong hoàn cảnh này, nếu có suy nghĩ như Du, hãy một lần mở lòng và nhìn lại, cứ mở lòng đi, rồi bạn sẽ được nhận lại những điều xứng đáng.
Và tôi tự hỏi mình, quan điểm " Mấy đời bánh đúc có xương,
                                               Mấy đời mẹ ghẻ biết thương con chồng." trong cổ tích liệu có còn đúng 100% trong xã hội hiện đại?
5
415730
2014-12-13 23:42:19
--------------------------
139563
5713
Đây là lần đầu tiên tôi đọc tác phẩm của LÂM PHƯƠNG LAM. Lối viết nhẹ nhàng, trong sáng mặc dù cốt truyện có những diễn biến éo le.
Truyện kể về cuộc đời của người đã từng là con chồng, giờ thành mẹ ghẻ. Tác phẩm cho cái nhận khác biệt về suy nghĩ mẹ ghẻ con chồng theo quan điểm trước đây, thấu hiểu nỗi lòng của người phụ nữ khi kết hôn với người đàn ông đã từng có vợ và con riêng. Và cuối cùng, cuộc đời sẽ luôn mỉm cười với những tình yêu chân thành, tình cảm vị tha, và một kết cuộc đẹp.
Tác phẩm sẽ hoàn thiện hơn nữa nếu không có những sơ suất nhỏ trong lỗi đánh máy, ghi nhầm tên nhân vật. Nên đọc và cảm nhận.
5
991
2014-12-07 20:09:04
--------------------------
139037
5713
Biết đến chị Lâm Phương Lam qua những câu chuyện được đăng trên chuyên mục Bạn trẻ cuộc sống của báo 24h. Từ những ngày truyện của chị chỉ xuất hiện từng phần vào 7h sáng hàng ngày cho đến hiện tại khi những tác phẩm của chị đã được in thành sách. Chủ đề truyện của chị không mới nhưng ngôn từ, cách dẫn dắt và tình huống truyện thì vẫn có một nét riêng. Tự nhiên, gần gũi mà sâu sắc đôi khi rất sâu cay, độc giả đọc cười đấy rồi lại khóc ngay được... "Mụ ghẻ" hấp dẫn người đọc ngay từ cái tên, từ bìa truyện, từ mùi thơm của giấy. Mụ ghẻ hấp dẫn độc giả bởi những thiệt thòi, mất mát của Du, sự cố gắng, hi sinh của dì Hạnh. Đọc Mụ ghẻ bạn sẽ hiểu hơn về số phận của người phụ nữ, về những mảnh đời, số phận trong cuộc sống này. Truyện của chị Lam luôn gây cho người đọc bất ngờ, thậm chí  đến khi gấp sách lại người ta vẫn bị ám ảnh bởi những bởi câu chuyện mình vừa đọc xong.
Cám ơn chị Lam nhé, mong rằng trong tương lai chị sẽ có nhiều tác phẩm hơn nữa, không chỉ thỏa mãn đam mê của chị mà còn truyền niềm yêu thích  viết lách đến những độc giả như em. Chúc cho bé Mụ và những đứa con tinh thần của chị sẽ được thật nhiều độc giả đón nhận và yêu nhiều hơn!!!
5
501523
2014-12-05 21:30:19
--------------------------
136357
5713
Với cá nhân tôi, rõ ràng MỤ GHẺ là một bước tiến của Lâm Phương Lam. Vẫn ngôn từ rắn rỏi nhưng rất dịu dàng, vẫn những pha hành hạ nhân vật đến đáng thương nhưng rất tình người, vẫn những định kiến dành cho nhau nhưng trong trái tim người ấy lại giữ một vị trí quan trọng, vẫn những hoàn cảnh khắc nghiệt của cuộc đời nhưng sự sống vẫn sinh sôi và bừng sáng.
Tôi cũng thích tên thân mật mà tác giả hay chia sẻ trên trang cá nhân, gọi tác phẩm là "bé Mụ", rõ ràng, tác phẩm là đứa con tâm huyết mà tác giả dành cho, và với cá nhân là người đọc, tôi nhận định, MỤ GHẺ đã chinh phục được một người kĩ tính, khó đọc, chọn lọc tác phẩm (nhất là văn học Việt Nam ở giai đoạn mà người người đều viết như hiện nay).
Kết thúc tác phẩm, không có nghĩa là tôi không suy nghĩ về nó nữa. Cuốn sách cho tôi nhin cuộc đời qua lăng kính không vẹn toàn nhưng hình ảnh đổ lại rất tình người, rất đáng suy ngẫm: ở đó có tuổi thơ, tình yêu đầu tiên trong trẻo, sự bồng bột, vấp ngã, chấp nhận, đứng lên.
Cảm ơn tác giả. Cảm ơn "bé Mụ"
5
393390
2014-11-20 17:10:29
--------------------------
135705
5713
Đây là một quyển sách vô cùng ý nghĩa. Cách kể chuyện xen kẽ giữa cuộc đời của người dì ghẻ và con chồng đã so sánh được những sự việc xảy ra. Hai đời mẹ con đều trải qua làm dì ghẻ, cũng nhận lại được cách đối xử vô lễ của con chồng. Nhưng cuối cùng họ cũng đã thấu hiểu được những gì người khác đã phải gánh chịu và thông cảm cho nhau. Nhờ đó mà họ đã dành tình cảm cho nhau và thay đổi suy nghĩ của nhau về dì ghẻ. Những lời văn của Lâm Phương Lam rất chân thực và sâu sắc đã để lại dư âm lớn trong lòng tôi. Những nhân vật chỉ đóng vai trò phụ nhưng cũng phản ánh được một câu chuyện, cũng đồng thời là một bài học trong cuộc sống thường ngày.
Khi nhận được quyển sách mình rất hài lòng về cách bọc bìa và còn được tặng kèm một bộ bookmark vô cùng ý nghĩa. Nhưng trong nội dung sách thì vẫn có những lỗi chính tả và có chỗ đánh nhầm tên nhân vât, mong Tiki sửa lại nhé (^_^)
5
436081
2014-11-16 19:41:34
--------------------------
134322
5713
Đọc "Mụ Ghẻ" của Lâm Phương Lam.
Phát hiện ra mình không có khả năng review sách cho lắm, muốn viết nhiều ý mà cứ thấy cụt cụt chả liên kết được với nhau. Đành "gạch đầu dòng" vậy:
- Tác phẩm không truyền đạt ý nghĩa gì cụ thể mà người đọc cần tự rút ra ý nghĩa cho riêng mình ở tùy góc độ là người làm cha, là dì ghẻ hay là đứa con riêng.
- Câu chuyện hơi đưa nhân vật vào hoàn cảnh quá đặc biệt khi những người thân quan trọng xung quanh đều ở vào hoàn cảnh "dì ghẻ - con chồng", khiến cho thiếu mất phần tác động rất lớn của những người có hoàn cảnh bình thường khác có thể tác động tới tâm lí của "con chồng".
- Cách tiếp cận khá mới khi đưa miêu tả hiện tại và quá khứ, sự đối lập trong tâm trạng "dì ghẻ - con chồng" của cùng một người.
- Chi tiết hơi khó hiểu: Tại sao Du được học hành tử tế, trở thành một cô gái tốt, chín chắn, biết suy nghĩ lại có thể không về nhà trong suốt khoảng thời gian sau khi trưởng thành? Cũng như việc cô chưa chấp nhận dì ghẻ của mình thì sao có thể khiến con riêng của chồng chấp nhận cô?
Nhìn chung, "Mụ ghẻ" - với cá nhân mình - đánh dấu sự trưởng thành rất lớn trong cách viết của LPL so với tác phẩm đầu mình đọc từ 2 năm trước. Cách viết ngắn gọn và gần gũi hơn, không còn nhiều chi tiết thừa, nhưng vẫn còn các chi tiết thiếu để câu chuyện trở lên hoàn hảo - không phải để có cái kết hoàn hảo. 
Túm lại: Nên đọc để hiểu thêm về những mảnh đời đặc biệt...
4
32823
2014-11-09 07:14:39
--------------------------
132421
5713
Khi nhận sách từ tiki, ấn tượng đầu tiên của tôi là: bìa sách đẹp quá, bookmark đẹp quá, tình cảm quá, ý nghĩa quá, sách được bọc màng co nên cũng rất dễ làm quà tăng.
Đây là lần thứ 2 tôi đọc truyện của Lâm Phương Lam, vẫn là lối viết tình cảm, ám ảnh, nhưng luôn đầy day dứt. Nếu như, tiểu thuyết Say đắm cho tôi cái cảm giác day dứt nhưng quá đổi ngọt ngào, thì MỤ GHẺ lại khiến tôi ám ảnh, suy nghĩ triền miên về số phận của những con người.
Tôi rất thương Du, cô gái từ khi chỉ còn là một đứa nhỏ, thích chơi một mình, hay những người lớn hơn tuổi, thích ngồi vô tư lự ở gần đường ray chở than; cho đến khi cô biết đến cậu bạn trai hàng xóm tên Thành; hay cuộc trốn chạy gây đầy tổn thương do cậu Tư xảy ra; và cuối cùng là cuộc hôn nhân với người đàn ông thành đạt để trở thành mẹ kế của một đứa trẻ quá lắm chiêu trò.
Tôi cũng rất thương dì Hạnh, người đàn bà theo chuẩn mực truyền thống của người phụ nữ Việt Nam, và tôi nghĩ, bà có ảnh hưởng trực tiếp đến tính cách, sự chịu đựng của Du về sau này.
Tôi cũng rất thương dì Châu, dù dì chỉ là một tuyến nhân vật phụ, qua lời kể không quá nhiều, nhưng tôi hiểu ra, hóa ra hôn nhân đôi khi không phải xuất phát từ tình yêu, mà là vật chất, bất chấp lòng tự trọng, tự tôn, địa vị trong xã hội.
Tôi thương cả mợ Hà, người đàn bà chỉ biết sống vì tình yêu, và chết cũng vì tình yêu.
Tôi cảm thấy xót xa cho những mảnh đời người phụ nữ!
5
75153
2014-10-31 15:24:17
--------------------------
132105
5713
Từng làm mẹ mới hiểu hết mà trải lòng cho tình mẫu tử hết lòng dành cho con của mình. Ấy vậy mà nữ chính - Du từng thiếu vắng tình thương của mẹ ruột, đã không chấp nhận người mẹ kế mặc dầu bà yêu thương cô như con ruột thịt mình mang nặng đẻ đau. Không ai muốn mình sinh ra đã đóng vai ác cũng không ai muốn mình phải hy sinh quá nhiều cho điều vô nghĩa. Du trải qua một mối tình, chấp nhận làm mẹ kế của Giang như hồi ký quay lại như khi Du mười tuổi được dì Hạnh chăm nuôi đã khiến cô có suy nghĩ đổi khác và chọn cho mình quyết định mà có lẽ ngần ấy năm là quá bồng bột của cô. Tác phẩm như mang ngôn từ đầy mị hoặc, khắc họa sâu trái tim người con gái và phụ nữ nỗi cô đơn tràn đầy yêu thương có khi lỗi nhịp nhưng chưa bao giờ là nửa vời, từng câu chữ làm rung động một cách mãnh liệt khó định hình nhưng thực sự nó có gì đó rất đặc biệt và chất chứa của sự trưởng thành cùng với chững chạc của một người con. Không quá lãng mạn mà vừa đủ tinh tế, không quá bi lụy nhưng từng trải và hơn hết là ấm áp của một gia đình khuyết. Tưởng như xa lạ, ích kỷ không thể chấp nhận, vậy mà từ lúc nào nhận ra rằng yêu thương chưa bao giờ là muộn màng.
5
26363
2014-10-29 19:41:18
--------------------------
