569589
5661
Tôi sẽ review chi tiết cho bạn sau 20h nữa, lúc mà tôi có thể chơi được cờ vây
5
1208759
2017-04-10 21:41:43
--------------------------
485332
5661
Chỉ trong 64 trang đầu tiên, tác giả đã tóm lược những điểm chính để tự học bất kỳ kỹ năng mới nào. Trình bày gọn gàng, súc tích, có hệ thống và rất khoa học.
Không cần phải mất 10.000 giờ như Malcom Gladwell đã đề xuất, cũng không cần thiết phải trở thành một chuyên gia.
Nếu bạn muốn trau dồi thêm nhiều kỹ năng  nhưng không có nhiều thời gian thì đây chính là cẩm nang dành cho bạn. 
Từ trang 65 trở đi là những ví dụ của cá nhân tác giả trong việc áp dụng những quy luật vào chính trải nghiệm của mình.
5
327781
2016-10-01 18:25:22
--------------------------
395247
5661
Không phải ai sinh ra cũng đã có sẵn những kỹ năng cần thiết. Chúng ta phải học, phải rèn luyện những kỹ năng đó để có thể bổ trợ cho mình trong tương lai sau này. Quyển sách này đến với tôi thật đúng lúc khi tôi đang muốn học một số kỹ năng cần thiết cho công việc của mình. Nó cho tôi biết được, lượng thời gian tối thiểu mà tôi cần bỏ ra để có thể học được một kỹ năng là bao lâu. Nó còn cho tôi biết được từng giai đoạn của việc học kỹ năng. 20 giờ đầu tiên đã giúp tôi học được những kỹ năng một cách hiệu quả hơn. Một điểm tôi chưa thích ở quyển sách này đó chính là cách viết chưa được thu hút cho lắm, chưa đưa ra được những ví dụ thực tiễn và cách áp dụng lý thuyết một cách hiệu quả là như thế nào. 
3
462354
2016-03-11 17:16:13
--------------------------
372216
5661
Là một người ham học hỏi và muốn cải thiện nhiều kĩ năng còn thiếu, tôi chọn mua cuốn sách này sau khi được giới thiệu từ một cuốn sách khác về những bước đầu tiên thiết lập một kĩ năng mới.
Ưu điểm: tác gia dẫn dắt người đọc từng bước khác rõ ràng và phương pháp lặp lại những bài học về một vài kĩ năng như: học yoga, cờ vây, lập trình cơ bản, gõ phím 10 ngón và lướt ván..v..v... để bạn đọc hiểu được 20 giờ đầu tiên theo đúng phương pháp quan trọng như thế nào. Nó giúp cho những bước đầu tiên để học kĩ năng mới bất kì nhanh hơn và có quy trình.
Nhược điểm: vì tôi là người học công nghệ thông tin nên hiểu được một vài điểm trừ trong phương pháp học đó. Nó thiếu thực tế và hơi hoang tưởng trong việc đi đúng hướng như tác giả chỉ trong 20 giờ tập trung. Vì tôi nghĩ cần nhiều hơn vậy.
Cuối cùng, tôi hài lòng về dịch vụ của tiki cho tới thời điểm hiện tại. Cám ơn tác giả, cám ơn tiki.
3
668502
2016-01-21 10:38:30
--------------------------
363707
5661
Bị thu hút bởi đoạn mở đầu đọc được trong chức năng đọc thử, mình rinh liền cuốn này về nhà.
Khi về lật ra xem cái mục lục thì hơi thất vọng vì nó không như mình nghĩ, nó là quyển sách chia sẻ cách học một số kỹ năng cụ thể chứ không phải là sách kể chuyện triết lý như mình lầm tưởng (chỉ tại cái tội mua sách chỉ đọc phần nội dung mà không xem mục lục trước). Nhưng lở mua rồi thôi đọc đại xem sao, với lại lời văn cũng rất cuốn hút. Kết quả là không thất vọng nữa mà lại rất vui, vì qua cuốn sách mình học được thêm vài thứ mà bình thường mình chẳng thèm quan tâm. Cách chia sẻ của tác giả rất thân thiện và cụ thể. Thế là mình đã "bất đắc dĩ" lụm được một quyển sách hay :)
3
848141
2016-01-04 20:46:18
--------------------------
354690
5661
Mình biết tới Josh Kaufman sau khi xem bài nói của ông trên kênh TED. Khi tìm thấy cuốn sách, ngay lập tức mình đã bị ấn tượng bởi cái bìa vàng cùng thiết kế khá là thú vị của nó. Vốn là một người thích đọc sách về kĩ năng sống, xem qua review thấy cũng khá ổn nên mình quyết định mua về đọc xem thế nào. 
Nhưng tới khi bắt đầu đọc thì mình lại cảm thấy có chút thất vọng. Lời văn của tác giả hơi khô khan, không truyền được cảm hứng cho lắm. Phần tác giả nói về cách tự học dù kĩ càng chi tiết nhưng đối với mình thì lại khó áp dụng, có lẽ là do sở thích của mình không trùng với tác giả. 
Có lẽ mình nên dành thời gian để nghiên cứu kĩ hơn những gì Josh Kaufman viết trong sách. Biết đâu trong lúc đó lại có thể tìm được rất nhiều điểm chung với tác giả thì sao?
3
418919
2015-12-18 13:24:37
--------------------------
341374
5661
Cầm cuốn sách trên tay tôi quả thực bị choáng, đầu tiên là vì khổ sách 15x24, tiếp đến là màu vàng chóe nổi bần bật. Tôi bị ấn tượng bởi phần mở đầu dí dỏm của tác giả hơn là phần nội dung, quả thật đọc rồi mới thầm nghĩ "Sao mà giống mình thế?". Về nội dung, tôi chỉ có thể học được phần yoga, gõ 10 ngón và xa hơn 1 chút nữa là cờ vây. Nếu những ai không hề có hứng thú với 3 món tôi vừa kể hay lập trình và lướt ván thì không nên mua cuốn này, thật lòng đấy
4
467203
2015-11-21 22:49:44
--------------------------
335042
5661
Khi thấy tựa đề và miêu tả về sách mình khá ấn tượng và quyết định mua. Tại cùng lúc đang muốn học thêm về vài điều mới nhưng chưa biết bắt đầu như thế nào. Nhận sách từ Tiki, mình háo hức đọc ngay nhưng thật sự thì hơi thất vọng một chút. Sách không nhưng mình hình dung. Tất cả những chia sẻ trong sách đều là những gì chính tác giả đã thực hiện nhưng phần lớn không phải vấn đề mình quan tâm. Mình chỉ có thể góp nhặt được một ít điều từ cuốn sách. Tuy vậy mình cũng không hối hận khi mua sách vì biết đâu được một ngày nào đó mình lại có chung một mối quan tâm với tác giả  thì sao...
3
606122
2015-11-10 13:49:53
--------------------------
333339
5661
Thiệt mình là mình đã cảm thấy vô cùng hài lòng khi cầm quyển sách trên tay. Có thể nói 20 giờ đầu tiên là một quyển sách bổ ích và thú vị cho những ai đam mê học hỏi, tìm tòi cái mới. 20 giờ đầu tiên sẽ tiết lộ cho bạn những nguyên tắc vàng để học và còn là học một kỹ năng nhanh. Còn gì tuyệt vời hơn khi bạn có thể học những điều mà mình mong muốn dù thời gian có hạn. Chỉ cần bỏ ra vài giờ để nghiền kỹ quyển sách này, bạn sẽ tiết kiệm được một kho thời gian quý báu trong việc học một điều gì đó mới mẻ. Còn chờ gì nữa, hãy đặt mua cuốn sách để cảm nhận nó cùng tôi bạn nhé! 
4
295986
2015-11-07 20:21:46
--------------------------
323394
5661
Có rất nhiều việc,nhiều thứ trên đời này mình rất muốn làm,muốn thực hiện nhưng vấn đề lớn nhất của mình đó chính là không biết kiểm soát thời gian nên đã để thời gian trôi qua vô ích,và dĩ nhiên những dự định muốn làm cũng không thể thực hiện được.Khi tìm thấy cuốn sách kĩ năng này mình rất vui vì đã tìm được 1 sự hướng dẫn để kiểm soát được thời gian.Sách rất đẹp,bìa bắt mắt nhưng điều quan trọng nhất vẫn là nội dung bên trong.Sách dành cho những ai vẫn rối rắm trong việc kiểm soát và lập kế hoạch phù họp như mình.
4
440285
2015-10-18 17:44:00
--------------------------
321591
5661
Khi  mò tìm sách để mua về kĩ năng sống , đập vào mắt mình là bìa đẹp , mình đọc thử thì thấy hay hay , nhận xét của mỗi người đều đánh giá tốt. Nhưng khi mua về đọc thì thấy hụt hẫng quá, có lẽ đó không phải là thể loại mình thích , mình chỉ đọc được 10 trang thì bỏ cuộc . Sách thật sự khô khan dù các nguyên tắc chi tiết nhưng yoga lập trình, cờ vây , lướt ván, .. nó không phù hợp với mình nó quá khó để áp dụng vào thực tế , nếu tác giả nói về kĩ năng đối nhân xử thế hay về nguyên tắc học ngoại ngữ thì hay hơn nhiều 
3
715434
2015-10-14 12:33:24
--------------------------
316042
5661
Cuốn sách  " 20 giờ đầu tiên" thật là đáng thất vọng. 
20% của cuốn sách mô tả một số nguyên tắc chung và khá hời hợt cho học tập nhanh chóng. Còn lại 80% cung cấp mô tả của một những chủ đề quan tâm cá nhân. Nếu bạn quan tâm trong Yoga,  lập trình web, lướt sóng, gõ bàn phím..., và hơn nữa muốn biết những gì một người nghiệp dư thừa nhận đã phát hiện ra cho mình về các chủ đề này, sau đó bạn có thể tìm thấy cuốn sách này đáng giá . Nếu không, tôi sợ bạn sẽ thấy nó chỉ là một sự lãng phí thời gian và tiền bạc. Bạn có thể giả sử tôi đang đánh giá một cuốn sách không công bằng, và rằng các kỹ năng cụ thể được thực sự được sử dụng để minh họa cho việc áp dụng các nguyên tắc học tập nhanh chóng. Nhưng đó không phải trong trường hợp này. Có tương đối ít kết nối giữa những gì Josh Kaufman viết về (nói) lịch sử và thực hành Yoga và các nguyên tắc trình bày trong những chương đầu.  "20 giờ đầu tiên" không phải là một cuốn sách tốt cho tôi .
2
474761
2015-09-30 08:59:11
--------------------------
310799
5661
Đã mua từ lâu, đã đọc đi đọc lại nhiều lần, và cuốn sách này vẫn luôn đặt ở trước mặt mình, ngoài việc lĩnh hội các phương pháp học mọi thứ trong 20h, thì cuốn sách cũng là nguồn động lực cho mình học hỏi những thứ mới. Sau khi đọc cuốn sách, mình đã áp dụng cho việc học Yoga giống như Josh, và học thêm 1 kỹ năng mới đó là vẽ đầu tượng, một kỹ năng mà các bạn cho rằng phải đi học ở trường lớp nhưng tin mình đi, bạn có thể áp dụng phương pháp của Josh để học mọi thứ trên đời trong 20 tiếng đồng hồ.
5
6274
2015-09-19 19:54:11
--------------------------
308944
5661
Cuốn sách là tập hợp các kĩ năng để có thể học thứ mới trong khoảng thời gian vỏn vẹn 20 giờ. Qua cuốn sách này, tôi rút ra được bài học rằng cần bỏ công luyện tập, dành thời gian và kiên nhẫn cho tới khi đạt được mục tiêu của mình. 20 giờ đầu tiên là khoảng thời gian quan trọng nhất, dồn toàn tâm vào nó sẽ giúp bạn đạt được kết quả dáng ngạc nhiên trong việc hấp thu kiến thức.

Tuy nhiên điểm trừ mà tôi không thích ở cuốn sách này là các bài tập hơi khó áp dụng vào thực tế, cách trình bày sách cũng chưa thu hút. Tôi chỉ đọc nó để tham khảo thêm một cách học mới. Còn để áp dụng được hoàn toàn tất cả các bài học trong đấy thực sự cần sự quyết tâm lớn của bản thân.
2
508310
2015-09-18 21:40:41
--------------------------
304066
5661
Từng rất ấn tượng với bài thuyết giảng của Josh Kaufman trên TEDtalk về phương pháp học nhanh mọi thứ nên khi thấy cuốn sách này mình đã quyết định mua ngay lập tức. Cái hay của tác giả này là ông không chỉ viết lý thuyết suông mà luôn đưa luôn những ví dụ thực tế vào để người xem có thể học hỏi, ví dụ như cách ông tập lại cách đánh máy bằng 10 ngón, hay cách viết chương trình máy tính trên web,..Cuốn sách này rất thích hợp với những bạn luôn muốn học hỏi những điều mới nhưng lại ngại không có thời gian như mình.
4
135778
2015-09-16 10:16:16
--------------------------
291523
5661
Cuốn sách dạy kỹ năng học nhanh một thứ rất cần cho cuộc sống hiện đại, trong khi thế giới thay đổi từng giây để bắt kịp với xu thế bạn không thể nào áp dụng các cách thức truyền thống nữa mà phải trang bị cho mình những kỹ năng mới. Thì đây chính là sự lựa chọn cho bạn, cách học  này hướng đến kết quả của bạn (độ thông thạo những kỹ năng mới mà bạn muốn đạt được), cách thức tiếp cận mới mẻ ví dụ dễ hiểu và thực tế, chỉ cần bạn có quyết tâm và thực hành chăm chỉ thì việc nắm bắt các kỹ năng mới sẽ không còn là trở ngại lớn  
4
274738
2015-09-06 12:57:01
--------------------------
273795
5661
20 giờ đầu tiên là một cuốn sách hoàn hảo để đạt được tất cả các kỉ năng mong muốn trong thời gian không tưởng mà Josh Kaufman đã mang lại cho mọi người bằng khả năng truyền cảm hứng và dẫn giải chi tiết từng phần giúp học cực nhanh các kỹ năng đem bản thân mình sự tự tin hơn rất nhiều trước những yêu cầu của công việc và học tập để nhanh chóng đạt được những thành công.Quả thật là một cuốn sách vô cùng hữu ích trong cuộc sống.
5
266384
2015-08-21 13:01:51
--------------------------
272774
5661
Josh Kaufman mang đến phương thức thú vị để học hỏi một điều mới mẻ trong 20 giờ. Phương pháp rất quan trọng, lựa chọn đúng cách thức thực hiện, tức là bạn đã thành công được hơn 50% của quá trình. 

Trong thời buổi hiện tại, chúng ta phải không ngừng học thêm những điều mới mẻ. Làm sao với chỉ 24h hạn hẹp, chúng ta có thể học được nó một cách tương đối? Josh sẽ chỉ cho bạn điều đó qua " 20 giờ đầu tiên", và kinh nghiêm của Josh trong việc áp dụng nó để học những thứ mới lạ như Yoga, cờ, lập trình,... tôi tin rất có ích cho bạn
5
482216
2015-08-20 13:35:05
--------------------------
268339
5661
Một cuốn sách có thể thay đổi cuộc sống. 20h đầu tiên trình bày tư duy và các kĩ thuật cần thiết để học bất cứ thứ gì trong 20h. Tác giả cũng đã từng là diễn giả của Ted talk. Mình rất vui vì tìm được cuốn sách này trên Tiki. Nó đã thay đổi suy nghĩ của mình rất nhiều, mình thậm chí đã tự tập để chơi các nhạc cụ khác trong vòng chưa đến 10h. Thử tưởng tượng cứ mỗi tháng ta lại chơi được thêm một nhạc cụ mới mà xem. Rất đáng để thử.
5
59771
2015-08-16 12:12:58
--------------------------
261886
5661
Một cuốn sách với tựa đề rất gây tò mò và ngờ vực khi đặt ra vấn đề học mọi kỹ năng chỉ với trong vòng 20 giờ tập luyện.

Tuy nhiên, cuốn sách khá thực tế và khả thi. Nó đặt ra những vấn đề hợp lý trong việc bạn đặt mục tiêu cho việc học kỹ năng của mình, tìm hiểu, và tạo điều kiện cho chính mình chăm chỉ chuyên cần tập luyện nhất có thể.

Tác giả cũng trình bày việc áp dụng thực tế của chính mình để độc giả có thể có những ví dụ trực quan nhất,

Thêm một điểm cộng nữa, là cuốn sách không chỉ hướng dẫn bạn cách học, mà còn khơi gợi lòng say mê học hỏi của người đọc, khi mà trong thực tế chúng ta lười việc ấy và sa đà vào nhiều thứ khác vô bổ hơn
4
41024
2015-08-11 14:55:45
--------------------------
261802
5661
Mình vẫn rất thích những cuốn sách nói về chủ đề học và làm việc hiệu quả. 20 giờ đầu tiên là 1 trong những cuốn sách khá hay và cơ bản. Bằng cách loại bỏ những thực hành kết quả thấp, và sử dụng các kỹ thuật đã được chứng minh để tăng cường khả năng ghi nhớ thông tin của bộ não, bạn có thể tìm hiểu tất cả các loại kỹ năng chuyên môn mới, nhanh hơn. Vài chương đầu tiên trong cuốn sách này chỉ tập trung vào đó. Mình ấn tượng với 30% nội dung đầu của cuốn sách. Nhưng phần còn lại thì thật sự không hay lắm, trong 70% còn lại của cuốn sách tác giả giải thích về quá trình học của chính mình, rất chi tiết - đôi lúc dông dài và hơi thừa, về việc học tập 6 kỹ năng mới: yoga, lập trình, ukulele, gõ 10 ngón, cờ vây và lướt ván, Các môn học này có thể hữu ích đối với những người thích phiêu lưu và tìm kiếm trải nghiệm mới nhưng không mấy giá trị thực tế đối với mình (trừ gõ 10 ngón), nên đôi lúc có những "khoảng trật" về trải nghiệm khiến mình không thể áp dụng lời khuyên được. Nếu như tác giả có thể chia sẻ về việc học ngoại ngữ hay 1 kỹ năng đặc biệt nào đó trong nghề nghiệp có lẽ sẽ hay hơn.
3
359955
2015-08-11 14:14:00
--------------------------
256988
5661
Lúc đọc giới thiệu của sách trên tiki mình vẫn chưa tin lắm về việc học một kỹ năng chỉ mất 20 giờ, chuyện đó thật không tưởng nên mình quyết định mua sách về để xem sách có thật sự hay như lời giới thiệu hay không. Và thật sự mình thấy mua cuốn sách này về là một quyết định sáng suốt của mình. Nội dung đánh vào đúng vấn đề mà mọi người chúng ta đều quan tâm: Làm thế nào để có thể học được nhiều kỹ năng khác nhau trong khi chúng ta lại có quá ít thời gian?. Cuốn sách này là câu trả lời hoàn hảo nhất, chúng ta không cần đến 10 ngàn giờ để hoàn thành thuần thục một kỹ năng mà chỉ cần 20 giờ để có thể nắm bắt làm chủ được các bước cơ bản của nó, thế là đủ. Mục tiêu của sách là giúp chúng ta trong thời gian ngắn nhất có thể nắm được những điều cốt lõi của những kỹ năng cần thiết nhất. Đây là một cuốn sách hay
5
79617
2015-08-07 12:17:28
--------------------------
233378
5661
Cầm quyển sách trên tay đọc những trang đầu tiên mình cảm thấy tác giả dẫn dắt người đọc rất hay, làm cho trí tò mò của bản thân mình nổi lên, làm cách nào mà tác giả có thể học được những kĩ năng chỉ trong 20 giờ, càng đọc mình càng cảm thấy thích. Muốn trở thành chuyên gia trong lĩnh vực mới bạn cần luyện tập hàng ngàn giờ nhưng chỉ với 20 giờ chúng ta nên học những cái cốt yếu, cơ bản và thường sử dụng nhất. Nếu học được nhiều kĩ năng một cách nhanh chóng thì thật tuyệt vời. Mình thật sự hài lòng về chất lượng sách, đẹp và chất liệu giấy dễ đọc, cảm ơn tiki rất nhiều
5
484263
2015-07-19 16:38:34
--------------------------
216130
5661
Mọi người luôn than phiền luôn "Không có thời gian" để rèn luyện bản thân. Cuốn sách là giải pháp tuyệt vời cho vấn đề muôn thủa đó, bạn không cần phải dùng đến 10K giờ để trở thành chuyên gia của 1 kỹ năng. Chỉ cần 20h, 20h thôi bạn sẽ làm chủ một kỹ năng mới, thổi sáo, đành đàn, thuyết trình, đám phán, mục tiêu của tác giá là trong thời gian ngắn nhất bạn có thể thành thạo những kỹ năng cơ bản để phục vụ cho công việc của mình. Đó là một phương pháp tối ưu.
4
512291
2015-06-27 10:45:26
--------------------------
214577
5661
Quyển sách là một bí kíp cho việc học tập và làm việc hiệu quả. Tác giả trình bày  theo hướng lặp đi lặp lại những thông điệp rằng:
- Phải có kế hoạch
- Phải có chiến lược
- Phải nhìn tổng quát
- Biết trích lọc
- Và tích lũy những điều cốt lõi nhất cho bản thân
Trên cơ sở đó, thực sự là chúng ta không cần dành quá nhiều thời gian, công sức cho việc tiếp cận và tiếp thu một khía cạnh mới nào. Bởi vì, năng suất quan trọng hơn nhiều và đáng đầu tư hơn nhiều.

Đọc quyển sách mà tôi có cảm giác như tác giả sẽ trở thành Bách khoa toàn thư mà không gặp bất kỳ khó khăn gì khi ông có được phương pháp hiệu quả.

Một điếm tôi thấy tiếc là sách chưa chia sẻ được đa dạng phương pháp mà chủ yếu là sự lặp lại và triển khai 1 phương pháp.

Nhưng như thế cũng rất hay vì tính đầy đủ và khả năng thấu đáo, thực hành sẽ thuận tiện, sẽ cao.
3
506526
2015-06-25 10:09:10
--------------------------
212408
5661
Lúc trước mình đã xem video của tác giả Josh Kaufman nói về việc học nhanh bất cứ thứ gì trên Ted.vn. thực sự là khá tò mò về việc học nhanh trong 20 giờ nên mình đã xem và bị thuyết phục ngay sau đó. Sau khi đọc sách thì mình càng thêm có động lực để thực hiện những điều mình thích. Bạn không  nhất thiết phải trở thành  vận đông viên Nguyễn Tiến Minh nhưng bạn có thể thoải mái đánh cầu lông với bạn bè mà không e ngại về khả năng của bản thân, bạn cũng có thể tự học bơi đủ để tự cứu bản thân khi rớt xuống nước...Thực sự là một cuốn sách đáng để đọc
Về chất lượng sách thì rất tốt, bìa trình bày đẹp..ko có gì để chê luôn
5
252288
2015-06-22 13:10:06
--------------------------
211183
5661
Cuộc sống hiện đại có rất nhiều thứ mà chúng ta cần phải học. Cho dù là học vì đam mê sở thích hay học vì công việc đòi hỏi bắt buộc thì cũng cần có những kĩ năng và sự trau dồi không ngừng về kiến thức đó để đạt được điều mong muốn. Tác giả quyển sách là một người đam mê đọc sách và có một kiến thức sâu rộng trong nhiều lĩnh vực khác nhau để từ đó có thể cho ta lời khuyên và cách thức vận dụng thực hành một kĩ năng thành thạo trong thời gian ngắn nhất có thể. Theo mình nghĩ rằng nếu chỉ cần sự tập trung và yêu thích và thực hiện nó trong vòng hai mươi giờ thì chắc tùy vào từng kĩ năng, độ khó dễ của nó nữa nên mình cho rằng nói luyện tập liên tục 20 giờ thì sẽ có một kết quả nhất định thì mình nghĩ rằng (với bản thân mình thì là vậy còn bạn thì có lẽ khác) rất khó khăn. Tuy nhiên, đây cũng là một quyển sách cũng đáng để chúng ta tìm hiểu và học hỏi.
3
274995
2015-06-20 10:08:30
--------------------------
189586
5661
Ý tưởng và nội dung cốt yếu của tác giả Josh Kaufman trong cuốn sách 20 Giờ Đầu Tiên chỉ gói gọn trong việc bạn sẽ hình thành một kỹ năng mới trong ít nhất là 20 giờ! Tác giả đã cho tôi biết thời gian bao lâu để hình thành một kỹ năng và có nhiều bài tập bài tập để chứng minh, cũng như giúp bạn hình thành kỹ năng! Nhưng thật sự theo tôi ý tưởng tốt nhưng rất theo các bài tập của tác giả hơi khó
Bìa sách khá đẹp, khá thu hút! Ý tưởng bạn hoàn toàn có thể sử dụng 20 giờ để hình thành một kỹ năng!(Làm theo các bài tập của tác giả thì hơi khó nhé)
3
504087
2015-04-27 17:39:11
--------------------------
172551
5661
Trước đây mình rất ngại học những thứ mới, không thuộc chuyên ngành hoặc nghe có vẻ tốn thời gian. Nhưng khi xem giới thiệu sách do chính tác giả trình bày trên kênh TEDx, mình được truyền cảm hứng dữ dội. Sau đó nghe tin sách được xuất bản ở Việt Nam, mình mua ngay và không hề thất vọng. 
Quyển sách đã thay đổi tư duy tự học của mình, mình đã bắt đầu tự học thêm tiếng Hàn và Photoshop qua Internet sau khi đọc nó hehe. Cực vui vì cảm thấy như một thế giới mở ra với mình trên Internet mà bấy lâu nay mình chưa khai thác hết được. 
Nhược điểm duy nhất của sách đối với mình là khổ sách, mình thích sách khổ truyền thống hơn.
Một quyển sách đáng đọc trong thời đại số hóa hiện nay!
4
100681
2015-03-24 11:44:33
--------------------------
171006
5661
Josh Kaufman đã cho biết con người mất khoảng 20 giờ thực hành để phá vỡ rào cản thất vọng: để đi từ mức hoàn toàn không biết gì về việc bạn đang cố gắng làm tới mức có thể làm tốt việc đó 1 cách không tưởng. Mặc dù tôi đang thực hành những kỹ năng trong một cuốn sách khác nên chưa thực hành những kỹ năng trong cuốn sách mà tông màu trang bìa của nó là màu vàng chủ đạo này nhưng những kiến thức mà tôi đọc qua đã khiến tôi suy nghĩ rất nhiều về việc mất 20 tiếng để học những điều cơ bản của một kỹ năng. Một quyển sách mang tính cách mạng thông qua những ý tưởng của Josh Kaufman.
5
387632
2015-03-21 01:21:51
--------------------------
167141
5661
dẫn chứng chứng minh cho thấy chúng ta hoàn toàn có thể thành thạo nhiều việc chỉ trong 20 giờ luyện tập (không tính giờ học tập) Ngoài ra, còn rất tốt bụng "giúp đỡ" mọi người luyện tập với mình bằng các bài học Yoga, đánh máy,... phía sau nữa. ^^ Lúc giở ra phần tập Yoga, không biết có phải tác giả vẽ không nhưng mình đã cười rất nhiều vì những hình ảnh minh họa ngộ nghĩnh trong ấy.
Ngoài ra, có thể xem thêm Video TED "Học mọi thứ chỉ với 20 giờ" trên youtube của tác giả này để hiểu thêm, rất đáng xem!
5
382812
2015-03-14 07:23:19
--------------------------
157088
5661
Hì :) mình bị đánh bùa choáng ngay trang mở đầu của cuốn sách vàng chóe này,đọc mà phì cười thấy tác giả sao giống mình thế không biết.Từ khi biết Tiki đến giờ á,đặt hàng sách liên tục(đến mức bị mẹ la quá trời lun) chất đống trên kệ đó nhưng đọc làm sao cho xuể được(cơ bản vì quỹ thời gian có eo hẹp chút vì mình cuối cấp roài) nhưng cứ ham mua mua ,vét đến đồng lẻ cuối cùng cơ.Thôi lan man bản thân xíu thôi,à cuốn này hay lắm nhá.Tác giả có những ý tưởng khá là hay ho cho kiểu làm việc có công suất hiệu quả nhanh chóng,tiết kiệm thời gian nhất có thể.Bạn có đọc và áp dụng xem sao.Đồng thời xem bản thân có giống tác giả hem nhá..Ôi,buồn cười bản thân quá xá nhưng cũng đau vì đúng là mình chưa sử dụng hiệu quả thời gian.Cuốn này không đọc thì hơi phí ^.^
4
456366
2015-02-07 17:59:24
--------------------------
