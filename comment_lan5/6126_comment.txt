483534
6126
Sách chỉ mỏng thôi nhưng các bài tập trong sách thì vô cùng ứng dụng, chủ yếu là những bài đối thoại trong đời sống rất phù hợp cho các bạn học giao tiếp. Các bài tập tương đối ngắn và dễ dàng giúp cho người học không cảm thấy choáng khi mới học. Các bài nghe càng về sau càng khó hơn, càng nhiều từ vựng khác hơn giúp các bạn nâng cao trình độ của mình. Theo tôi đây là một cuốn sách đáng mua, giá cả lại phải chăng vì không kèm theo CD nên phù hợp với các bạn học sinh, sinh viên. Chúc các bạn sớm chinh phục được tiếng Anh.
5
487129
2016-09-15 10:06:04
--------------------------
463866
6126
Đây là một quyển sách hay và bổ ích mà các bạn còn kém phần nghe trong tiếng anh có thể mua về luyện tập. Đầu tiên, mình thấy cuốn này giá khá mềm rất phù hợp với học sinh. Sau là mình thấy cách bố trí các bài học cũng khá là hợp lý dễ học. File nghe của sách thì đã có sẵn trên mạng các bạn có thể down về nghe. Theo mình thấy nếu các bạn kiên nhẫn luyện cuốn này thì có thể cải thiện được phần nào khả năng nghe của mình. Nhưng một điểm hạn chế là, giấy của sách không được đẹp cho mấy, nói chung là ổn.
5
487492
2016-06-29 18:05:40
--------------------------
400501
6126
Đây là cuốn sách tuy mỏng nhưng khá nhiều bài nghe rất hữu ích cho việc học tiếng anh.giá cả rất " hạt dẻ", tiếc rằng sách ko có kèm thêm đĩa CD.
Tôi đã học sách này từ khi ôn thi đại học đến nay tôi đã ra trường làm giáo viên vẫn mua lẠi để dạy cho các em. Các bạn học ngoại ngữ nên trang bị cho mình một cuốn như thế này.
Tiki giao hàng nhanh chóng, đóng gói cẩn thận, mình mua đúng đợt giảm giá và được tặng quà nữa.nên rất vui.cám ơn Ti ki.
3
900617
2016-03-19 13:51:53
--------------------------
383897
6126
Cuốn sách Listen Carefully - Luyện Kỹ Năng Nghe Tiếng Anh ( 2013 ) là một cuốn sách học nghe tiếng anh rất hay và phù hợp với những bạn mới bắt đầu luyện nghe . Nội dung các bài học rất phong phú , hơn nữa mức độ bài cũng không khó . Phần nghe có thể down trên mạng rất tiện lợi mà còn rẻ hơn nữa . Rất cảm ơn tiki vì cuốn sách này và mình sẽ tiếp tục mua thêm nhiều sách để ủng hộ tiki trong tương lai . Sách hay lắm các bạn!
5
1096195
2016-02-21 14:01:01
--------------------------
378892
6126
Quyển sách này mình được giới thiệu mua khi đang ôn nghe tiếng Anh, vì nó được tái bản nên giấy đẹp và in rõ ràng và do mình mua vào đợt giảm giá nên khá là mừng nên cũng không ngần ngại mua ngay. Nội dung quyển sách được chia khá rõ ràng,mạch lạc phân cấp từ dễ đến khó, phần khó thì khó vừa thôi.Tóm lại mình rất ưng ý sản phẩm này,mình dùng làm quyển bài tập luyện nghe cùng phương pháp Effortless English. Nó khá hữu ích cho các bạn mới bắt đầu luyện nghe đó.
4
755333
2016-02-08 11:57:37
--------------------------
319861
6126
Đây là một quyển sách hay, cực kì phù hợp với những bạn mới bắt đầu nghe. Sách có rất nhiều bài hay, được sắp xếp theo thứ tự khó dần giúp bạn cảm thấy dễ dàng trong việc nghe. quyển sách khá mỏng vì thế không gây khó khăn trong việc gây áp lực .Điểm không tốt của sách là in chữ quá nhỏ dễ gây mỏi mắt, đọc không được tốt. Sách không có CD nhưng có thể dễ dàng download trên mạng rất dễ dàng. Giá thành lại rất phải chăng, phải gọi là rẻ, phù hợp với túi tiền sinh viên.
5
491823
2015-10-09 20:55:03
--------------------------
296667
6126
mình đã học quyển này lâu rồi. Khi đó thầy giáo dạy anh văn đã giới thiệu cho mình và mình tập tành nghe trong sách. Những bài nghe rất đơn giản, thích hợp cho những bạn mới luyện kĩ năng nghe. Và lời khuyên của mình dành cho các bạn là: nghe mãi một đoạn hội thoại/bài cho tới khi các bạn nghĩ các bạn đã hiểu được 90% bài thì hãy chuyển sang bài khác. Sau đó lật script ra xem. Các bạn không nghe được thì chỉ có 2 khả năng: phát âm sai hoặc là đó là từ mới. Vậy thì hãy luyện lại phát âm hoặc học ngay từ mới đó nghen.
4
138740
2015-09-11 10:53:27
--------------------------
292136
6126
Sách khá mỏng nhưng cũng bao gồm 15 phần bài nghe theo nhiểu chủ đề đa dạng. Mỗi bài nghe đều có nhiều bài tập nhỏ giúp mình nghe được nhiều bài rất hay và phong phú. Điểm trừ của sách là in chữ quá nhỏ dễ gây mỏi mắt. Điểm trừ tiếp theo là đây là sách nghe nhưng lại không kèm theo CD. Lúc mua về mình phải lên mạng search trên mạng và download về. Tuy vậy giá thành sách rất sinh viên nên mình rất thích. Hi vọng sẽ kiếm được các kĩ năng khác cũng hay và giá sinh viên như thế này.
4
53222
2015-09-07 02:08:29
--------------------------
