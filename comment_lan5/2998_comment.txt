418932
2998
Đây là quyển sách thứ ba mình tìm đọc của tác giả Sơn Nam. Những tác phẩm của Sơn Nam không hiểu sao làm cho mình có cảm giác gần gũi, gắn bó với cái vẻ mộc mạc, giản dị của người dân nơi đây. Qua tác phẩm đình miếu và lễ hội dân gian miền Nam mình hiểu thêm nhiều kiến thức về lễ hội, nghi thức, lễ bái của người Việt Nam thời xa xưa. Đây là một quyển sách hữu ích trong quá trình nghiên cứu về lịch sử văn hóa như mình. Cảm ơn tác giả đã mang đến quyển sách thật tuyệt vời.
5
509425
2016-04-20 11:12:09
--------------------------
277586
2998
Ngày nay, khi cuộc sống người dân ngày càng được nâng cao, không chỉ dừng lại ở những nhu cầu ăn, mặc, ở thông thường nữa, mà dường như đa số đều có thêm nhu cầu là nhu cầu tín ngưỡng vào những vị Phật, vị Thần với niềm tin mang lại cho họ sự bình an và thỏai mái trong cuộc sống thường nhật. Đình, chùa vì thế cũng hình thành ngày càng một tăng. Nhưng có lẽ do người dân Nam Bộ đa số tín ngưỡng đều là Phật Giáo nên đôi khi nhầm lẫn tên gọi giữa đình và chùa, dẫn đến việc những ngôi đình đựơc mang tên là chùa theo tên gọi thân quen nơi cửa miệng của người dân địa phương. Vậy, cách gọi nhầm lẫn giữa chùa và đình như thế thì có đúng không, hay cứ để vậy theo vẻ đẹp đời sống con người miền Nam Bộ.
5
718377
2015-08-25 08:53:37
--------------------------
