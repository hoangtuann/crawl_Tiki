415048
2907
Cái tên Thần Đồng Đất Việt không còn là cái tên xa lạ của các bạn nữa. Nó gắn liền với tuổi thơ mỗi người. Công ty Phan Thị cũng như NXB trẻ đã cho ra mắt những quyển truyện thật là bổ ích. Tuy truyện có bề mằt mỏng nhưng chứa đựng bao nhiêu kiến thức hay ho trong từng trang truyện. Được phiêu lưu với các nhân vật Tí, Sửu, Dần, Mẹo,... rất hấp dẫn. Truyện rất phù hợp cho mọi lứa tuổi nên mình nghĩ các bạn nên mua về đọc để thu thập thêm kinh nghiệm
5
1209601
2016-04-12 21:36:51
--------------------------
310011
2907
Bong bóng là món đồ chơi khá thân thuộc với mọi người, đặc biệt là trẻ nhỏ nhưng vẫn ít nhiều người chưa biết bên trong bong bóng có gì lại bay lên cao được như vậy. Với tập truyện Thần Đồng Đất Việt Khoa Học (Tập 135) - Bong Bóng Lên Trời đã mang lại cho mọi người những bài học thú vị về không khí, cách bơm khí nào vào bong bóng là an toàn nhất cũng như là cùng tìm hiểu các loại khí phổ biến xung quanh mình. Truyện được minh họa khá dễ hiểu, màu sắc tươi sáng, hình vẽ khá dễ thương.
4
408969
2015-09-19 11:30:32
--------------------------
