559037
8250
Những ngôn từ nhẹ nhàng, dung dị. Những tình tiết rất đỗi đời thường. Những biến động tâm lý mà bất cứ ai trong chúng ta cũng từng. Tôi như tìm thấy bản thân mình trong San San. Có đam mê, có ước mơ nhưng tự bản thân mình ngăn cản mình không thực hiện. Có xao xuyến, có phải lòng một ai nhưng lại câm lặng. Từng trang viết như lôi cuốn tôi, đọc quyển sách chỉ trong 1 tiếng đồng hồ mà đến khi gấp lại mà dư âm của nó vẫn còn hòa vào từng suy nghĩ.
5
1539554
2017-03-30 10:50:09
--------------------------
494051
8250
MÌnh vẫn cảm thấy cuốn sách này khá bình thường dù sau đó ba năm. Nhưng đây có thể là cuốn sách mà nhiều người thấy hứng thú, vì nó khá giản dị, hài hước.
Mình thích tác phẩm Dám yêu của Quỳnh Thy hơn, vì nó để lại nhiều thông điệp sâu sắc hơn cho bạn trẻ.
4
759399
2016-12-04 14:43:32
--------------------------
417600
8250
Được bạn mình giới thiệu, nói rất hay. Nhưng đây là tác giả mình chưa bao giờ đọc nên có chút phân vân nhưng quýêt định thử một lần. Quả là không thất vọng. Đây là một quỷên sách theo mình là rất thú vị và hài hước. Lối viết của tác giả khá dễ thương từ cách đặt tên nhân vật cho đến nội dung câu chuyện. Có lẽ mình sẽ tìm đọc thêm sách của tác giả này. Các bạn thử đọc xem, không uổng tiền mua đâu. Tiki đóng gói sách rất kỹ và giao hàng rất nhanh. Yêu tiki
4
231087
2016-04-17 12:31:04
--------------------------
402756
8250
Mình chỉ đọc sơ vài trang thấy thú vị nên quyết định mua về đọc. Truyện này thực sự rất hay, trong truyện các tình tiết diện ra rất chân thực về chuyện tình yêu mà các bạn đọc rồi sẽ nhận thấy mình đâu đó đã từng trải qua hay bắt gặp vài chuyện tương tự. Nhân vật nữ trong chuyện rất cá tính và dễ thương. Truyện có rất nhiều yếu tố hài hước xem lẫn những tính huống éo le rất buồn cười. Tác giả này lần đẩu tiên mình biết đến nhưng rất ấn tượng với cách dẫn dắt câu chuyện nên mới quyết định mua sách đó hihi. 
5
205081
2016-03-22 21:00:35
--------------------------
390384
8250
Đây là cuốn sách thứ 2 của Quỳnh Thy mà tôi đọc.Vẫn là lối văn quen thuộc của tác giả , ta lại bắt gặp hình ảnh những người trẻ dám yêu dám sống.Ta có thể thấy ngoài kia những cuộc tình như trong tác phẩm.San San và Rượu Vang Đỏ , cái tên thật đáng yêu.Tôi như nhìn thấy bản thân mình trong hình ảnh của San San .Có ước mơ nhưng nhút nhát không dám thực hiện.Thật hạnh phúc vì đã có chàng Rượu Vang Đỏ giúp cô có thể tự tin hơn.Tôi thích San San ,thích Rượu Vang Đỏ và yêu Quỳnh Thy.
4
578865
2016-03-03 19:58:26
--------------------------
385202
8250
Mình thích từ câu đề, nếu bạn bỏ qua câu truyện này có lẽ bạn không biết được tình yêu thực sự sẽ trở lại.
Bình thường và bình thường là ở đây. Ngôn ngữ giản dị, câu truyện tình yêu bình thường như mọi người thế nhưng nó lại làm mình rung động vì có lẽ mình lúc nào cũng thích nhẹ nhàng. Một tình yêu một cuộc sống bình bình đạm đạm trôi qua. Không cần sống chết bên nhau chỉ cần yên lặng yêu thương nhau.
Mình không ganh tỵ với tình yêu của họ, nhưng mình thật  nhưỡng mộ.
4
1061830
2016-02-23 18:40:41
--------------------------
382413
8250
 Tôi được một người bạn tặng cuốn sách này trong khoảng thời gian đang rối tinh rối mù với đống tình cảm hỗn độn!

 Khi cầm trong tay cuốn sách ấy, lòng tôi mừng như điên. Bìa sách rất dễ thương, bookmark lại càng dễ thương hơn :3 Tôi không thể rời mắt khỏi được cuốn sách và thế là dành hết thời gian từ 8 giờ đến 1 giờ sáng để đọc~~

 Tôi vẫn ấn tượng nhất với đoạn cuối cùng của cuốn sách..
 - Em biết không, ngày xưa, anh thích em lắm nhé...
- Ngày xưa thôi ư, thế còn bây giờ?
- Anh ... yêu em.
- Em cũng yêu anh.

 Là một câu chuyện giãn dị có thể bắt gặp trong cuộc sống, cũng có thể nhìn thấy chính bản thân bạn trong nhân vật San San.. dù số phận bạn không giống như San San nhưng có lẽ một phần nào đó tâm hồn của bạn sẽ giống...

 Hãy đọc và cảm nhận.. chắc chắn bạn sẽ thích tác phẩm này như tôi.. dù không thích tác phẩm nhưng cũng thích bookmark chứ nhễ!               
                                                      
4
1037501
2016-02-19 08:33:36
--------------------------
336395
8250
Cuốn sách này khá nhẹ nhàng. Từ bìa sách đã tạo thiện cảm đến tên gọi "giống như là tình yêu" đến cả giọng văn của tác giả. Đặc biệt mình thích cách trình bày bìa sách.
Về phần mình, về cơ bản cuốn này đã gợi nhớ ít nhiều cho mình một chút gì đó về tuổi thanh xuân với những lúc bồng bột, hay dễ vui mà cũng dễ buồn vì những chuyện vẩn vơ.
Cái kết có lẽ là lãng mạn với những người tầm tuổi học trò và ưa thích những điều lãng mạn như trong phim Hàn Quốc. 

3
178102
2015-11-12 12:58:34
--------------------------
332675
8250
Giống như giới thiệu, đây là một câu chuyện nhẹ nhàng, giản dị mà Quỳnh Thy mang đến cho người đọc.
Tuổi trẻ bồng bột, lời văn chân thực, tình huống rất đời thường,...tất cả gần gũi đến mức vừa đọc, tôi có thể vừa tưởng tượng rằng mình chính là nhân vật trong câu chuyện ấy.
Quỳnh Thy đối với tôi lúc đầu chính là một cái tên xa lạ, nhưng bây giờ, sau khi đọc câu chuyện này, tôi chính thức trở thành một đọc giả của các tác phẩm của chị.
Tôi thích Quỳnh Thy, thích cách chị viết, thích câu chuyện của chị,... 
Cố lên, tôi sẽ mãi ủng hộ chị!
4
768377
2015-11-06 19:41:12
--------------------------
330563
8250
Quyển sách này đem lại cho mình rất rất nhiều thông điệp ý nghĩa, tiếp thêm động lực cho mình, để có thể sống tiếp với đam mê của bản thân.Thích một ai đó, suy cho cùng cũng chỉ là một trạng thái tình cảm giống như bị mê hoặc, nhiều người thích rồi yêu nhau, nhưng cũng nhiều người thích và phát hiện ra rằng, đó chỉ là cảm xúc lãng đãng nhất thời. Người ta chỉ cần một giây để thích ai đó, nhưng phải mất rất lâu để nhận ra liệu đó có phải là tình yêu. 
“Ngày xưa, anh thích em lắm nhé!”
“Thế còn bây giờ?”
Tôi rất thích câu mà tác giả viết:”Người ta chỉ cần một giây để thích ai đó, nhưng phải mất rất lâu để nhận ra liệu đó có phải là tình yêu”. Kết thúc truyện tôi khá ưng ý, rồi đâu cũng vào đấy. Trái tim tìm về với trái tim.. tìm thấy người mình yêu đã khó, để nói lời yêu càng không dễ dàng!! Đọc để cảm nhận, để biết trân trọng những người đang ở bên cạnh ta nếu không biết nắm giữ, đôi khi lạc nhau là mất nhau mãi mãi chẳng kịp nhận ra ta đã thích hay là yêu..!
5
900373
2015-11-02 21:01:19
--------------------------
314753
8250
Sau khi đọc nhan đề của sách mình quyết định mua luôn vì cứ nghĩ đây là một cuốn truyện nhẹ nhàng có chút buồn. Nhưng khi nhận sách thì nội dung lại khác nhưng cũng không làm mình thất vọng lắm.Cuốn sách này mình đã đọc đi đọc lại tới mấy lần rồi mới đem tặng người ta ý. Sách của mình còn có cả chữ ký của Quỳnh Thy. Hoàn Cảnh lúc đầu của 2 nhân vật chính tương đối trớ trêu, dù chỉ là tình yêu học trò tương đối ko đc thật lắm nhưng cũng là một quyển ko đáng bỏ qua
5
346397
2015-09-27 08:49:32
--------------------------
277672
8250
Chuyện tình yêu trong truyện nhẹ nhàng, dễ thương được viết bởi giọng văn trẻ trung, nữ tính. San San - một cô gái trẻ năng động với nhiều ước mơ và hoài bão. Một Rượu Vang Đỏ ấm áp, chân thành. Hai con người "rất là có duyên" ấy đã gặp nhau và tình yêu giữa cả hai dần dần được nuôi dưỡng. " Anh không muốn em đi một mình. Nếu đi dưới những cơn mưa, anh sẽ đi cùng em." Trái tim đến là tan chảy trước những câu nói quá đỗi chân thành và tình cảm của chàng trai Rượu Vang Đỏ. Lúc nhận sách có chút thất vọng, vì sách mỏng, khổ nhỏ nhưng tận 70k, nhưng phải công nhận một điều là sách trình bày thiệt là đẹp, font chữ nổi bật, nội dung truyện cũng hay.
4
27232
2015-08-25 09:56:49
--------------------------
274831
8250
Đối với phong cách viết truyện của Quỳnh Thy mà nói thì có thể hình dung bằng hai từ nhẹ nhàng và lãng mạng. Tuy nhiên trong cuốn Giống như là tình yêu này, có lẽ tác giả bị ảnh hưởng khá nhiều các chi tiết trong phim Hàn Quốc, đôi chỗ hơi lãng mạn quá đà làm mất đi nét tinh tế vốn có. Cái kết đẹp và hợp lí khiến cho mình hài lòng. Bìa sách của Tamypu đẹp miễn chê, có nét buồn vương vấn và chút cô độc. Giấy in sách rất tốt tuy nhiên sách mỏng mà giá thì khá chát. Sách được Tiki bao bọc rất cẩn thận
4
182813
2015-08-22 12:15:38
--------------------------
272360
8250
Điều ấn tượng đầu tiên của mình về cuốn sách này chính là bìa của cuốn sách,đó là hình ảnh một cô gái ngồi quay lưng lại phía mặt trời sắp vụt tắt cộng thêm tên của cuốn sách cũng đợm một chút gì đó buồn buồn khiến mình liên tưởng tới một câu chuyện tình tan vỡ .Nhưng không khi đọc xong cuốn chuyện thấy lòng mình rất nhẹ nhàng,thoải mái.Một cái kết có đẹp cho San và Đức, Lâm và Mai Chi.Mình luôn thích những cái kết có hậu một chút vì khi đó thấy lòng mình sẽ thanh thản hơn khi nhớ lại thấy cũng vui hơn.Mình rất thích cuốn sách này.Yêu Quỳnh Thy và những tác phẩm của chị.
4
543425
2015-08-19 23:22:25
--------------------------
263961
8250
Một câu chuyện tình cảm ngọt ngào theo kiểu học trò.
Ấn tượng đầu tiên về cái tựa sách là một cảm giác buồn man mác, sao không phải là tình yêu mà lại là "giống như là tình yêu"?
Đọc hết cuốn sách mới hiểu, ra là vậy, cô gái ấy bị lẫn lộn giữa thích và yêu, một sự lầm lẫn của rất nhiều cô gái trẻ.
Một chút Hàn Quốc. Cô gái xuất thân không rõ ràng, cứ ngỡ mình có đầy đủ cha mẹ, rồi lại nhận ra mình chỉ là con nuôi; tìm được người mình yêu và đúng một cái, cô gái bị một chứng bệnh ngặt nghèo. 
Cái kết đẹp, mỗi nhân vật đều tìm cho mình được nửa còn lại.
4
619755
2015-08-12 22:30:53
--------------------------
233236
8250
Ban đầu khi mới đọc tựa Giống như là tình yêu tôi nghĩ đó sẽ là một câu chuyện buồn và đã quyết định mua quyển sách này.Nhưng sau khi đọc xong nó trái ngược lại với những suy nghĩ của tôi lúc đầu.Một cái kết đẹp cho San và Đức, Lâm và Mai Chi. San-một cô sinh viên trẻ có nhiều điều mà tôi nghĩ mình nên học hỏi.Truyện hay có những tình tiết bất ngờ như trong phim vậy nhưng nó quá quen thuộc làm người đọc hơi nhàm chán. Và mặc dù vậy  nhưng tôi vẫn thích nó.Thoáng đâu đó tình yêu đã xuất hiện mà ta chưa nhận ra.
4
475077
2015-07-19 15:11:46
--------------------------
228428
8250
Chất liệu giấy tốt, giọng văn trong sáng, câu từ thì giản dị, cốt truyện nhẹ nhàng nhưng sâu lắng mang đến cho người đọc một cảm giác lạ thường, nhân vật Rượu Vang Đỏ và San San thì có phần nào hài hước. Ngay từ những trang đầu của truyện, khi hai nhân vật gặp nhau thì chúng ta đã biết được phần nào cái kết của câu chuyện nhưng tác giả đã gây bất ngờ khi đã cho xuất hiện nhân vật Lâm, khéo léo đưa họ vào chuyện tình tay ba và đã có rất nhiều chuyện xảy ra nhưng những người yêu nhau thì cũng đã đến được với nhau. Một cái kết thật đẹp, một cái kết thật vẹn toàn. Đây là một cuốn truyện hay! 
5
420670
2015-07-15 14:01:58
--------------------------
203537
8250
Như tựa của cuốn sách: Giống như là tình yêu, đem đến cho người đọc một cảm giác nhẹ nhàng nhưng ẩn chứa điều gì đó man mác, gợi buồn.
Giọng văn trong sáng, không kém phần nữ tính là tớ rất thích, đã đọc được mấy trang đầu thì không thể nào rời mắt khỏi quyển sách mà phải đọc hết cả quyển mới cảm thấy trọn vẹn.
Cuộc gặp gỡ, cái chạm mặt của Rượu Vang Đỏ và San làm tớ thích thú, có chút gì đó dễ thương. Và cả thông điệp mà tác giả gửi gấm vào nó cũng nhẹ nhàng mà sâu sắc.
Đây là một quyển sách hay!
4
621744
2015-06-01 16:36:01
--------------------------
200088
8250
Ngay từ những tình tiết chạm mặt của San và Rượu Vang Đỏ, tôi đã phần nào đoán được diễn biến cũng như kết thúc truyện. Nhưng việc tiếp tục đọc nốt quyển sách này bởi vì cái cách truyền đạt thông điệp của tác giả. Văn phong nhẹ nhàng, nữ tính, trong sáng và chân thực khiến cho câu truyện trở nên gần gũi và đẹp đẽ. Không màu mè, gượng ép hay kịch tính, nút thắt gây cấn vào trong truyện, song, sự tự nhiên qua giọng kể của Quỳnh Thy lại làm người ta thích. Thấy mình đâu đó trong cô gái mạnh mẽ dám theo đuổi tình yêu và mơ ước nhưng từng lững thững giữa các lựa chọn quan trọng của cuộc đời mình. Niềm tin yêu, sự lạc quan và hy vọng cháy sáng đã giúp cô ấy vượt qua tất cả, sống thật với đam mê và nhận ra thứ tình cảm còn chưa xác định rõ trong tim mình. Triết lí của chị dễ nghe, dễ hiểu và không quá dày đặc, giáo điều. 
Nội dung và hình thức của tác phẩm đều mang vẻ đẹp mềm mại. Nhắc nhớ thời sinh viên hay tuổi trẻ nhiệt thành với tình yêu không vướng bận, Giống như là tình yêu đã làm khá tốt. Đẹp và trong đúng như mối tình đầu!
3
385533
2015-05-23 23:12:51
--------------------------
178741
8250
Mới đọc những tranh đầu tiên tôi cảm thấy câu chuyện khá thú vị đây, càng đọc càng nghiền. Câu chuyện kể về một cô gái năng động, vui vẻ nhưng lại không thể theo đuổi ước mơ của mình, cô cũng có 1 tình yêu thầm lặng không thể nói, cứ thế mỗi ngày trôi qua thật nhàm chán và vô vị. Đọc mà sao thấy giống mình quá. Cho đến khi gặp được anh "rượu vang đỏ", cuộc sống bớt cô đơn và tẻ nhạt đi. Có một người bạn tri kỷ luôn là điều tuyệt vời nhất. Nhưng đau khổ liên tục kéo tới, thất tình, người bạn tri kỷ "rượu vang đỏ " mà cô luôn dựa dẫm đột dưng biến mất, gia đình hiện tại không phải là cha mẹ đẻ của cô, người mẹ bỏ rơi cô 20 năm sau quay lại tìm cô để chuộc lỗi, cô bị ung thư não di truyền từ người cha đẻ mà cô vẫn gọi là chú suốt 20 năm. Hãy trân trọng những người luôn bên ta, đừng để mất, rồi mới hối tiếc. Đây thật sự là một cuốn sách hay để ta thư giãn vào dịp cuối tuần.
5
342558
2015-04-05 15:42:17
--------------------------
178740
8250
Mới đọc những tranh đầu tiên tôi cảm thấy câu chuyện khá thú vị đây, càng đọc càng nghiền. Câu chuyện kể về một cô gái năng động, vui vẻ nhưng lại không thể theo đuổi ước mơ của mình, cô cũng có 1 tình yêu thầm lặng không thể nói, cứ thế mỗi ngày trôi qua thật nhàm chán và vô vị. Đọc mà sao thấy giống mình quá. Cho đến khi gặp được anh "rượu vang đỏ", cuộc sống bớt cô đơn và tẻ nhạt đi. Có một người bạn tri kỷ luôn là điều tuyệt vời nhất. Nhưng đau khổ liên tục kéo tới, thất tình, người bạn tri kỷ "rượu vang đỏ " mà cô luôn dựa dẫm đột dưng biến mất, gia đình hiện tại không phải là cha mẹ đẻ của cô, người mẹ bỏ rơi cô 20 năm sau quay lại tìm cô để chuộc lỗi, cô bị ung thư não di truyền từ người cha đẻ mà cô vẫn gọi là chú suốt 20 năm. Hãy trân trọng những người luôn bên ta, đừng để mất, rồi mới hối tiếc. Đây thật sự là một cuốn sách hay để ta thư giãn vào dịp cuối tuần.
5
342558
2015-04-05 15:41:43
--------------------------
175746
8250
Đây là tác phẩm đầu tiên đưa mình đến với tác giả Quỳnh Thy. Ban đầu lựa chọn chuyện vì cái tên khá hay. Chuyện dễ thương .
 Có vẻ cốt truyện không đặc sắc nhưng có lẽ do lời kể chân thật và hấp dẫn của tác già khiến chuyện trở lên cuốn hút người đọc hơn. Bởi đâu đó ngoài kia ta vẫn có thể bắt gặp được những câu chuyện tình đơn phương dễ thương như San thích Lâm . Hay những lần gặp gỡ tình cờ mà cũng không khỏi bất ngờ của San và Rượu Vang Đỏ. 
A chàng Rượu Vang Đỏ quá ấm áp và tốt bụng. Nói chung là tác phẩm này của chị Quỳnh Thy hay, ấm áp, và vô cùng lãng mạn. Các bạn nên thử đọc cuốn này
À quên mất bìa còn siêu đẹp nữa chứ .
4
503956
2015-03-30 21:07:07
--------------------------
174327
8250
Mình thấy tác giả phải nói là một người kể chuyện chứ không phải nhà văn.
Cách kể của tác giả khá là hay, tự nhiên mà dẫn dắt rất có hồn. Đó giống như một năng khiếu bẩm sinh tiềm tàng vốn có của Quỳnh Thy.
Sách viết rất can đảm, San với tâm trạng theo đuổi đam mê văn chương của mình mà bỏ tấm bằng đại học. Các bạn vẫn đang đắn đo giữa gia đình và ước mơ khi đọc sẽ thấy hình bóng mình trong đó.
Cảm xúc vui buồn cũng được đan xen.
Bìa sách gây ấn tượng mạnh, trang nhã và dễ thương. 
Một cuốn sách đáng đọc.
4
39173
2015-03-27 20:12:11
--------------------------
167266
8250
Đây là câu chuyện đầu tiên của Quỳnh Thy mà tôi đọc. Cốt truyện ko có gì đặc sắc, thường có nhiều trong phim ảnh hay những câu chuyện tình cảm khác.Nhưng giọng văn nhẹ nhàng,chân thật
Câu chuyện xoay quanh nhân vật chính San San, một cô gái trẻ với khát khao trở thành nhà văn, dám đối đầu với sóng gió,dù bị gia đình phản đối
Rượu Vang Đỏ là nhân vật tôi ấn tượng nhất, kể cả tên lẫn tính cách :v Chu đáo, ấm áp.....
Nói chung là cũng được, với những ai lãng mạn thì cũng hay, nhẹ nhàng,ấm áp.
Các bạn thử đọc câu chuyện này nha !!!!!
4
565348
2015-03-14 14:11:24
--------------------------
139904
8250
Quyển sách đầu tiên của chị Quỳnh Thy mà mình biết đến là Dám Yêu. Bị ấn tượng bởi lối viết nhẹ nhàng, trong sáng mà chân thật của chị ấy, mình đã tìm đọc thêm những quyển sách của chị.

"Giống Như Là Tình Yêu" là một trong số những quyển sách yêu thích nhất của mình. Tuy mô típ này khá quen thuộc với những bạn đọc giả và với mình, nhưng mình vẫn cảm nhận được cái gì đó rất riêng của chị Quỳnh Thy trong câu chuyện, không lẫn vào đâu được. Hơn hết, mình đã tìm thấy được mình đâu đó trong câu chuyện này. "Giống Như Là Tình Yêu" đưa mình từ cung bậc này đến cung bậc khác của cảm xúc, vui buồn lẫn lộn.

"Tìm thấy người mình yêu đã khó, nói ra lời yêu lại càng không dễ dàng"
4
476006
2014-12-09 16:25:47
--------------------------
120879
8250
Quyển sách là một tiểu thuyết tình yêu với nhiều cung bậc cảm xúc, là câu chuyện về cuộc sống của một cô gái từ ngây thơ, hồn nhiên sau nhiều vấp ngã, tổn thương đã mãnh mẽ hơn rất nhiều.
Giọng văn nhẹ nhàng, cách viết chân thực, tác giả đã khắc họa rõ nội tâm nhân vật, nhất là đối với ước mơ của mình. Có chen nhiều phần bình ý nghĩa. Nội dung truyện cũng có chuyển biến gây kịch tính.
Tuy nhiên, câu chuyện không gây ấn tượng với mình lắm! Đó quả là một chuyện tình cảm động, vượt nhiều sóng gió nhưng thật ra cốt truyện không mới. Chúng ta có thể bắt gặp đâu đó vài (hay nhiều hơn) các bối cảnh trong truyện, cho nên ít nhiều mình cũng đoán trước kết cục. Ngoài ra, tình tiết truyện có vẻ hơi nhanh ở phần cuối.
Nói tóm lại, những bạn thích tiểu thuyết lãng mạn hay kiểu như mấy phim thần tượng chắc sẽ ưng cuốn này. Về phần mình, mình nghĩ nó hơi nhàn nhạt một chút.
3
333932
2014-08-15 18:41:10
--------------------------
115150
8250
Đây là quyển đầu tiên của Quỳnh Thy mà mình đọc và khiến mình thực sự rất ấn tượng. Quyển sách này đem lại cho mình rất rất nhiều thông điệp ý nghĩa, tiếp thêm động lực cho mình, để có thể sống tiếp với đam mê của bản thân. Câu chuyện này rất thực tế nhưng không kém phần lãng mạn, về cô bé San San cá tính mạnh mẽ nhưng tâm hồn lại quá đỗi mong manh được Rượu Vang Đỏ quan tâm, che chở trong mọi tình huống. Mình ngưỡng mộ cách San San đối đầu với sóng gió, tình yêu thuần khiết cao thượng của Rượu Vang Đỏ. Thích nhất cách Quỳnh Thy sắp xếp cho San San và Rượu Vang đụng mặt nhau nhiều lần như thể duyên số sắp đặt.
5
349863
2014-06-23 22:42:40
--------------------------
113364
8250
Lần đầu tiên đọc truyện của Quỳnh Thy. Đây là câu chuyện tình yêu mà bạn có thể bắt gặp ở bất cứ đâu ngoài thế giới rộng lớn ngoài kia. Cô bé San hơi ngốc nghếch và "ngoan quá mức bình thường" khi không dám theo đuổi ước mơ làm nhà văn của chính mình, anh chàng Rượu Vang Đỏ dịu dàng, hài hước đem lòng yêu thầm San San và ngày ngày ở bên cạnh cô, giúp cô vượt qua nỗi đau khi bị thất tình, chàng bác sĩ Lâm ấm áp có nụ cười toả nắng, cô nàng Mai Chi đỏng đảnh, ích kỉ... Tất cả, tất cả đã tạo nên một câu chuyện đời thật nhẹ nhàng và sâu lắng, cùng những bí mật được bật mí ở đoạn kết... Đây là một cuốn sách must-have trên kệ sách của mỗi bookaholic như mình. Hãy đọc và cảm nhận, bạn nhé !
3
115481
2014-05-29 18:14:15
--------------------------
111671
8250
Quyển sách viết về cuộc sống của một cô gái đang tuổi đôi mươi với niềm đam mê bị vùi dập và phải từ bỏ để theo sự kì vọng của cha mẹ. Tình tiết trong truyện khá nhẹ nhàng, ngôn ngữ chị Thy sử dụng cũng nhẹ nhàng lắm. Những cuộc gặp gỡ, những biến cố xảy ra với San San chỉ được tác giả nói qua rồi thông, không đau đớn nhiều và cũng không dằn vặt nhiều, Đọc xong quyển sách mình có cảm giác như đang đọc 1 truyện ngắn mà kéo dài tình tiết ra vậy, chủ yếu là tâm trạng của San San thôi, các đoạn hội thoại và những giải thích cho các biến cố đã được lược bớt khác nhiều khiến câu chuyện hơi nhạt. Điều mình thích nhất ở truyện là cái tên Rượu Vang Đỏ San San dành cho người con trai yêu thương cô, rất tinh nghịch.
3
40835
2014-05-02 13:37:04
--------------------------
109903
8250
Những ngôn từ nhẹ nhàng, dung dị. Những tình tiết rất đỗi đời thường. Những biến động tâm lý mà bất cứ ai trong chúng ta cũng từng. Tôi như tìm thấy bản thân mình trong San San. Có đam mê, có ước mơ nhưng tự bản thân mình ngăn cản mình không thực hiện. Có xao xuyến, có phải lòng một ai nhưng lại câm lặng. Từng trang viết như lôi cuốn tôi, đọc quyển sách chỉ trong 1 tiếng đồng hồ mà đến khi gấp lại mà dư âm của nó vẫn còn hòa vào từng suy nghĩ. 
- Em biết không, ngày xưa, anh thích em lắm nhé...
- Ngày xưa thôi ư, thế còn bây giờ?
- Anh ... yêu em.
- Em cũng yêu anh.
Một cái kết tuyệt đẹp cho những con người can đảm. Tự ngẫm với bản thân: " Càng can đảm càng hạnh phúc. "
3
298852
2014-04-09 12:47:17
--------------------------
108537
8250
Quỳnh Thy lại ra một tác phẩm mới sau khi Cô nàng hoàn hảo mình đã từng đọc, cuốn này lại là cuốn nói về tình yêu của cô gái mà cô ấy cố gắng theo đuổi từ thời thanh xuân của mình.

Đúng như ý của tác giả, tình yêu là thứ gì đó mà ta không thể kiểm soát nổi, có thể ta yêu người này, nhưng người này lại yêu người khác.

Nó là một vòng quanh luẩn quẩn của định mệnh, không ai biết trước được mình có hạnh phúc trong tương lai gần hay không.

Nhưng tôi tin chắc tác phẩm này gợi cho tôi những năm tháng đậm chất thanh xuân của thời tuổi trẻ nông nổi, bồng bột không thể quên của cuộc đời một con người.
3
87044
2014-03-19 17:22:47
--------------------------
