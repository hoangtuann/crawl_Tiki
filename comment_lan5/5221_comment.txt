458748
5221
Với mình thì tập Conan có bố mẹ Conan xuất hiện đều hay cả, xuất hiện một phát là cướp sân khấu của bạn nhỏ luôn :)) Tập này phát hiện thêm sự thật về mẹ Conan, ngầu quá trời quá đất, trong những nhân vật nữ từng xuất hiện trong truyện thì mẹ của Conan chính là người để lại ấn tượng và nổi bật nhất đối với mình, rất mong những tập sau này tác giả sẽ hé lộ về quá khử của bố mẹ Conan nhiều hơn, ví dụ như họ quen nhau thế nào...Tập này cũng có tí hint về tình cảm Haibara dành cho Conan :))
5
24079
2016-06-25 14:01:38
--------------------------
362374
5221
Tập này có sự xuất hiện của khá nhiều nhân vật và những bí ẩn. Liệu bí ẩn nào tiếp tục được bật mí nữa đây? Sự xuất hiện của mẹ Shinichi và những nghi ngờ về tổ chức áo đen rồi sẽ đi đến đâu?
Sự so tài của hai bà mẹ làm cho mọi thứ trở nên thú vị, những trận tranh tài rất thú vị. Ai chẳng muốn mình là người giỏi nhất hết mà.
Vemouth xuất hiện? Mình khá thích nhân vật này, có vẻ gì đó bí ẩn rất cuốn hút. Mình cũng rất thích khả năng cải trang và câu nói nổi tiếng của cô: "The secret make a woman woman!"
4
558702
2016-01-01 21:23:21
--------------------------
357218
5221
Ở đầu trang tác giả nói tập này Akemi, chị của Ai sẽ xuất hiện. Ai ngờ đọc tới cuối chả thấy cô ấy đâu hết. Chỉ là vụ án có nhắc tới chị ấy thôi. Vậy cũng cho là có xuất hiện sao, tưởng tham gia vào một vụ án mạng nào đó chứ. 
Cả nhà Ai đều làm việc cho bọn xã hội đen, là người xấu hay tốt ? Có ranh giới giữa thiện ác không ? Không giết người nhưng giúp người ta giết người thì là đồng phạm. Lạ thật ! Conan không hề ghét Ai tí nào cả dù vì cô ấy mà cậu ta mới bị teo nhỏ. 
5
535536
2015-12-23 10:00:38
--------------------------
300111
5221
Quả là bất ngờ khi trong tập này mẹ của Shinichi và mẹ của Conan lại cùng nhau phá án. Mình rất thích chi tiết hai người bạn thân từ hồi cấp 3 cùng tranh tài trong cuộc thi Miss Teitan và lại có cùng số phiếu bầu chọn. Ai mà ngờ người quyết định danh hiệu Miss Teitan lại chính là ông thám tử râu kẽm chứ. Tập này có vài chi tiết mình vẫn chưa hiểu rõ về Jodie và Vermouth, đã từng nghĩ có khi nào hai người lại là một không? Cái đó phải chờ xem tác giả giải thích sao mới được, hồi hợp ghê!
5
471112
2015-09-13 18:48:41
--------------------------
266326
5221
Tập 41 truyện conan thật ấn tượng.Thật không ngờ Yukiko lại gặp Kisaki, thích thật! Trong tập này, vụ án ám sát không tiếng động trong bóng tối làm mình sợ kiếp luôn. Nhóm thám tử nhí đã chứng kiến 1 vụ giết người không tưởng đồng thời Haibara và Conan lại bị 1 tên lạ mặt theo dõi. Nhưng mình vẫn không hiểu tại sao trong file mục tiêu không thể chạy trốn Vermouth lại cải trang thành cô Jodie nhỉ? Dù sao thì truyện cũng rất hay.
5
650266
2015-08-14 16:03:37
--------------------------
198602
5221
Cho dù đọc đến bao nhiêu tập, bao nhiêu file vụ án thì với mỗi câu chuyện lại là một bất ngờ, một sự hấp dẫn khó cưỡng, một kết thúc không thể đoán được, và cảm thấy bộ óc của Conan quả thật là siêu phàm, đúng chất nhỏ mà có võ, dù là vụ án nào, hóc búa đến đâu cũng tìm ra được lời giải đáp. Tập 41 cũng như vậy, cũng là một chuỗi những bất ngờ nhỏ đến bất ngờ lớn, và bất ngờ và hấp dẫn nhất phải nói là vụ án bị theo dõi của Conan và Haibara, hồi hộp không thể tả được.
4
530790
2015-05-20 12:18:18
--------------------------
168004
5221
Conan đã không còn xa lạ với các bạn nào thích truyện trinh thám. Được lấy ý tưởng từ tiểu thuyết Homes, tác giả Gosho Aoyoma đã sáng tác ra bộ truyện trinh thám mà cả các bạn nhỏ nào khi đọc cũng có thể hiểu được. Qua mỗi năm, truyện cũng được tái bản nhiều lần, một số lượng đáng nể dành cho tác giả Nhật khi phát hành truyện ở Việt Nam. Conan cũng đã trở thành một người bạn của nhiều thế hệ trẻ, đồng hành trên suốt quãng đường dài gần 20 năm. Bằng những suy luận sắc bén, tính logic cực kì cao, không vụ án nào có thể làm khó cậu bé. Không những thế, bộ óc của cậu không phải người tầm thường nào có thể sở hữu, cậu đã trau dồi từ khi còn bé, chính điều đó đã giúp cậu rất nhiều khi đối mặt với những phi vụ khó và cậu luôn thắng, đó là điều chắc chắn.
4
13723
2015-03-15 20:15:40
--------------------------
