257692
7673
Một hôm nọ, Tí mơ thấy đại phu thời tây hán - Dương Hùng, ông chỉ dạy cho trạng Tí cố gắng ghi nhớ bài thơ mà ông dạy, sau này sẽ có ích. Thật đúng như vậy, bài thơ này kết hợp với cuốn sách cổ mà cha Sửu đưa cho Tí kế thừa sẽ cho biết chuyện của 500 năm trước và 500 năm sau . Chính Vì biết trước sự việc nên Tí đã giúp đỡ những ai sắp gặp tai nạn và giúp họ tránh khỏi, bá hộ Mão cũng không ngoại lệ. Nội dung truyện hay, hài hước, thú vị. Truyện cũng được thiết kế rất đẹp, màu sắc tươi tắn, chất lượng giấy in truyện cũng rất tốt, khá dày, độ trắng vừa phải. Nói chung mình rất thích quyển truyện này !
5
120141
2015-08-07 21:46:50
--------------------------
