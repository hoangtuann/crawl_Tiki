448740
5017
Về nội dung, "Vết cắt hành xác" khá thu hút khi để Camille về quê nhà và điều tra vụ án mạng tại quê hương của cô. Người dân ở quanh đây thường khó hợp tác, và bọn trẻ ở đây nó nét gì rất riêng, mỗi đứa được miêu tả bằng một tính cách khác nhau. Truyện không chỉ chú trọng vào việc phá án mà còn nói lên mối liên hệ giữa các nhân vật. Đoạn cuối khá bất ngờ, khi không chỉ mẹ của Camille là thủ phạm của vụ án mà ngay cả Amma cũng có liên quan. Những dấu hiệu tưởng chừng như rất nhỏ nhưng cũng là manh mối cho một vụ giết người.


Về nhược điểm thì mình thấy giấy in không được đẹp, nhiều câu chữ dịch hơi tối nghĩa và khó hiểu, phải đọc lại vài lần mới thấm. Nhưng nói chung đây là một cuốn khá hay.
4
671890
2016-06-16 14:28:26
--------------------------
436816
5017
- Vướng vào 1, à, giờ thì đã thành 2 vụ án mạng mà không rõ hung thủ là ai, và trong khi điều tra thì vô số ám ảnh và những mối quan liên hệ - không muốn nhắc đến - lại tràn ngập ùa về.
- Vướng vào - 1 lần nữa - người mẹ ruột giàu sang cao quý nhất vùng, nhưng cách bà quan tâm và thống trị gia đình thì thật đáng sợ.
- Vướng vào đứa em gái cùng mẹ khác cha mới 13 tuổi mà đã là thủ lĩnh toàn trường, cầm đầu những trò xấu xa, và rượu, và thuốc, và một bộ óc thiên tài.
- Vướng vào quá khứ của chính bản thân, mơ hồ, dơ bẩn, đơn độc, giống như không có gì đáng lưu luyến nhưng thật ra thì cô đã giữ tất cả chúng mãi trên cơ thể mình rồi, Chữ, những vết cắt khắp người.
Quyển truyện này, sàn diễn đều là của các nhân vật nữ, tôi thích như thế. Những tình huống ngỡ bình lặng nhất lại là bước đệm sáng giá cho các biến chuyển Điên Rồ. Có rất nhiều đoạn tôi thường đọc lại nhiều lần. không phải vì khó hiểu, mà vì tôi không thể dừng nghĩ về việc đoạn này khi được dựng thành phim thì HAY HO ĐẾN CỠ NÀO LUÔN!!

Mình đã đọc qua bản tiếng Anh, sau đó mới mua sách tiếng Việt, bản dịch tuy đôc lúc hơi khô cứng, nhưng không mấy ảnh hưởng.

Điểm trừ : 
- Có lẽ đây là cuốn cuối trong kho của Tiki hiện giờ hay sao ấy, (vì có thông báo hết hàng sau khi mình mua) và sách dính vài vết ố, dơ nhẹ.
- Mình không khó tính, nhưng loại giấy in của cuốn này không tốt, bạn có thể nhìn thấy rõ trang chữ bị phản chiếu ở mặt sau.

Nhưng nói thật lòng đây vẫn là quyển sách thú vị, mình hoàn toàn muốn có nó.
4
71770
2016-05-27 11:06:08
--------------------------
422244
5017
Câu chuyện ban đầu mình những tưởng thật sự gay cấn hay sẽ bị cuốn hút vào một vụ trinh thám bí hiểm nào đó. Cuối cùng thì câu chuyện cũng chỉ xoay quanh tâm lí nhân vật chính mà cũng chẳng có nhiều đoạn cao trào lắm. Thấy Gone Girl mang tính logic và nhiều phân cảnh tâm lí đặc sắc cùng diễn biến thú vị hơn. Tuy nhiên thì trong Vết cắt hành xác cũng có đôi chỗ rùng rợn và khiến người đọc có một cách nghĩ hoàn toàn khác về cuộc sống gia đình và những suy nghĩ không thể hiểu nổi diễn ra trong đầu của một cô gái có lẽ là hơi dị thường
1
411359
2016-04-26 20:48:50
--------------------------
355831
5017
Một câu chuyện hay, hấp dẫn và đầy ám ảnh. Cốt truyện  cuốn hút, sống động, cái kết bất ngờ đủ mang lại thỏa mản cho đọc giả. Hơi bùn với cách kết thúc câu chuyện lãng mạn của Camille và Richard. Tôi thích anh chàng cảnh sát này, theo tưởng tượng riêng của mình tôi không hình dung Richard lại sợ hay không thích những vết xẹo của Camille. Rất thích cách hành văn của Flynn, sẽ típ tục mua và đọc tác phẩm của Flynn.
Nhưng bản dịch này của alphabooks thật sự quá tệ quá cẩu thả, không chấp nhận được!!! Nó phá hoại câu chuyện hay của Flynn.
3
554393
2015-12-20 20:43:00
--------------------------
348933
5017
Lần đầu tiên mình thử thể loại trinh thám kinh dị kiểu này. Mình có phần hơi sợ ma, nên cũng đắn đo khi mua. Nhưng mua rồi đọc rồi mới thấy nó k quá đáng sợ, nhưng cực kì ám ảnh. Cách miêu tả nhân vật, miêu tả cái chết quá là ám ảnh.
Nội dung thì được sắp xếp logic đan xen hiện tại và quá khứ giúp người đọc như sống với câu chuyện chuyện của cô phóng viên.
Và phải nói là sau bao nhiêu tình tiết làm người đọc suy đoán ra thủ phạm một cách chắc chắn rồi thì đến cái kết lại quá bất ngờ.
Chỉ là mình chưa hiểu tại sao lại có cái chi tiết "sợi dây thừng có DNA" dưới gối :v
Và cuối cùng phải nói là bản dịch hơi bị tệ. Rất nhiều đoạn đọc cứ như google dịch vậy :|
4
746915
2015-12-07 12:02:59
--------------------------
338916
5017
Mình đã đọc 2 tác phẩm còn lại của Flynn và thấy cực kì hay cho nên mới mua thêm cuốn này để coi, tuy nhiên  cuốn này thì quá tệ. Cách hành văn khó hiểu, lan man, không gây được cảm giác hồi hộp...
Không biết có phải là người dịch dở hay cuốn này tác giả viết dở nữa. 
Ngoài ra sách còn bị lỗi dính lại với nhau nữa nên càng gây thất vọng. Hi vọng nhà xuất bản sẽ để ý khi biên dịch hơn để người đọc không cảm thấy thất vọng như vậy .
1
347895
2015-11-17 08:09:56
--------------------------
306517
5017
Nếu như cô không về nhà đã lẩn tránh được sự di truyền chết tiệt đó, cho tiềm thức của những ám ảnh từ người mẹ cứ mãi nằm sâu trong miền quên lãng của ký ức. Thì nay khi về lại quê nhà chứng kiến và điều tra về những tội ác đang diễn ra thì sự độc ác dần dần trỗi dậy. 

Nhiều người yêu thích vẻ ngây thơ, sang trọng và đẹp đẽ nhưng họ có ý thức được rằng ẩn sau cái bề nổi choáng ngợp đó là những giả tạo, bản ngã nhớp nhúa độc ác ẩn giấu phía trong của những mỹ nhân.

Hãy cẩn thận và đừng quá mê muội để thành nạn nhân tiếp theo của họ nhé!

PS: Dịch đôi chỗ hơi lủng củng, vòng vo nhưng nếu lướt qua một chút thì vẫn có thể hiểu và cảm được mạch truyện.
4
117197
2015-09-17 17:00:04
--------------------------
245490
5017
Cuốn sách này bệnh hoạn 1 cách sâu sắc (xin hãy hiểu rằng đây là 1 lời khen). Chính vì thế, nó sẽ rất phù hợp các những độc giả muốn đọc về các nhân vật phức tạp với quá khứ đen tối không thua gì một câu chuyện kinh dị. Đặc biệt là các nhân vật nữ: họ đều lạnh lùng, tàn nhẫn và độc ác. Gillian Flynn đã xóa bỏ quan điểm "phụ nữ là phái yếu" thường thấy, ngược lại bà tỏ rõ thái độ: phụ nữ cũng có thể trở thành nhân vật phản diện hay không kém đàn ông!
Nội dung tương đối ngắn, nhưng tiết tấu hơi chậm chạp, dài dòng, đôi khi lủng củng, rắc rối. Nhưng đoạn kết thì thật điên rồ, kinh khủng, tuyệt vời! Hơi đáng thương cho Camille, nhưng dù sao thì đây cũng không phải là một câu chuyện cổ tích để mà đòi hỏi kết thúc có hậu. Đây là một cuốn truyện trinh thám u ám tối tăm, với chủ đề chính, không phải là những vụ án mạng, mà là lòng dạ đàn bà.
3
293599
2015-07-28 22:36:35
--------------------------
206094
5017
Truyện này khá hay. Ban đầu đọc thấy chưa hứng thú lắm, vì cách dịch chưa trau chuốt lắm, nhiều chỗ còn lũng cũng! Nhưng dẫu sao có cái hay của nó, vì nếu dịch gần như sát nghĩa mình sẽ thấy được cách viết của người nước ngoài :) Nó khá hay ho so với lối dịch mà văn phong đã được trau chuốt rồi
Nội dung truyện hay, hay thì hay thật, nhưng mình không thấy gây cấn đến nghẹt thở như nhiều người nói! Kết thúc khá bất ngờ, nếu không muốn nói là rất bất ngờ!
4
588740
2015-06-08 18:22:00
--------------------------
184729
5017
Sau thành công của Gone Girl thì Vết cắt hành xác cùng Bóng ma ký ức nhận được sự ngóng chờ khá lớn từ bạn đọc Việt Nam. 
Đối với tôi, một người khá yêu thích những tác phẩm trinh thám, kinh dị nó không thực sự ấn tượng như mong đợi. Nó chưa đủ độ "rùng rợn", "ám ảnh", "gay cấn" hay "hồi hộp đêns thắt tim" như những lời review trên một số báo mạng, diễn đàn
Mạch truyện, câu từ có chút hơi dài dòng, lê thê, câu cú chưa được chau chuốt nhiều về mặt dịch thuật.
Nếu đã từng phát cuồng vì Serie Hannibal của Thomas Harris thì có lẽ bạn sẽ hơi thất vọng khi so sánh với tác phẩm này, đấy có vẻ như là điều đương nhiên
3
263240
2015-04-18 10:52:10
--------------------------
180304
5017
Dù tác phẩm đầu tay và bản dịch không được hay thì vẫn là một câu chuyện rất lôi cuốn, chứa đựng bất ngờ. Gillian có thế mạnh mô tả nội tâm đen tối của nhân vâth một cách dửng dưng nhưng lại rất ấn tượng và sống động. Đáng đọc! Tuy nhiên, tiến triển của chuyện khá chậm trong suốt tác phẩm, rồi đột ngột  để nhân vật chính Camille nhận ra thủ phạm ở đoạn cuối. Các nhân vật cũng không thể hiện suy luận hay quan sát lý thú nào, chủ yếu xoay quanh nội tâm, tính cách.
4
9229
2015-04-09 08:10:03
--------------------------
178866
5017
Mình có đọc bản tiếng Anh của truyện này, tên là "Sharp Objects", khá ấn tượng với lối văn phong hơi bí ẩn, u ám, đúng chất trinh thám của tác giả. Nhưng khi đọc thử vài trang của bản dịch thì thấy khá thất vọng với lối dịch của dịch giả. Cách hành văn của dịch giả không thuần việt cũng không chuyển được trọn nghĩa và văn phong của tác giả, đôi chỗ tác giả xài tiếng lóng nhưng dịch sang vẫn dịch sát nghĩa, thành ra đọc câu nó vô cùng trớt quớt.
Tiếc đang tính mua mà đọc vào thất vọng ghê. Mình khuyên nếu được các bạn nên tìm bản tiếng Anh đọc, khá hấp dẫn.
1
421650
2015-04-05 21:29:10
--------------------------
166547
5017
Câu chuyện diễn biến từ một thị trấn nhỏ với hai vụ giết người. Cũng chính từ đây, một bức tranh gia đình xuất hiện. Đau đớn, ám ảnh, buồn, sợ hãi,... đó là tất cả những cảm xúc tôi trải qua khi thấy mẹ của Camille đối xử với cô và em gái cô như thế nào. Thực ra đây không phải là cuốn truyện trinh thám quá khó đoán thủ phạm, nhưng thật tình tôi đã đoán sai ngay trong cuốn tiểu thuyết dễ dàng như thế. Có lẽ tính bất ngờ đã làm cho câu chuyện thu hút độc giả hơn. Căn bệnh quái đản mà mẹ Camille mắc phải đã hủy hoại cuộc đời ba đứa con của bà. Đây là câu chuyện khá ấn tượng, tôi thích cách trang trí từng trang của cuốn sách này.
4
185178
2015-03-12 21:15:02
--------------------------
158736
5017
Mình không phải là một fan bự của thể loại trinh thám, nhưng những tác phẩm của Gillian Flynn thì mình lại không thể không đọc. Sự độc đáo, mới lạ, sức cuốn hút trong từng câu chữ, và ý nghĩa nhân văn là điều khiến mình tìm đọc hết các tác phẩm của cô ấy. "Vết cắt hành xác" cũng vậy. Sự rùng rợn đã đến ngay từ tên truyện, rồi càng tăng lên qua từng chi tiết của cuốn sách. Một cô phóng viên trở về quê nhà để điều tra những vụ giết người bí ẩn, trong khi chính cô cũng đang vật lộn với những đau đớn, những ký ức không lấy gì làm đẹp đẽ của tuổi thơ. Tâm lý, tình cảm nhân vật trong cuốn này dù phức tạp, nhưng Gillian Flynn vẫn xử lý thật xuất sắc.
Tuy nhiên, nếu so với "Bóng ma ký ức" và "Cô gái mất tích" thì cuốn này chưa hoàn toàn thuyết phục. Mạch chuyện khá chậm, nhiều tình tiết chưa được đẩy lên cao trào, đặc biệt là đoạn nữ chính phát hiện ra ai là kẻ chủ mưu.
Nhưng nhìn chung đây vẫn là một tác phẩm rất đáng đọc. Nó gây cảm giác hồi hộp, ghê sợ, và có cả những cảm xúc vỡ òa. Nỗi đau thương, bi kịch vẫn hiện hữu dù cuốn sách đã khép lại. Khi đọc xong, sẽ thấy cuốn này có khá nhiều điểm tương đồng về mặt tư tưởng với hai cuốn trước của Gillian.
4
109067
2015-02-13 19:56:45
--------------------------
158326
5017
Mình đã đọc hai quyển sách còn lại của Gillian Flynn rồi mới trở lại đọc quyển sách đầu tay này của Gillian Flynn. Có lẽ vì vậy mà mình thấy nó không được hay bằng hai quyển sau về mặt cốt truyện cũng như lối viết, cũng có thể là do bản dịch chưa hay như bình luận của một số độc giả khác. Câu chuyện này cũng có nhiều chi tiết khiến người ta rợn tóc gáy vì trí tưởng tượng khá phong phú mà kinh dị của Gillian Flynn. Nhân vật nữ chính Camille Preaker cũng có tính cách bất cần đời khá giống như Libby Day của Bóng Ma Ký Ức. Cốt truyện diễn tiến khá nhanh và có vẻ như đọc cái vèo là hết truyện nên mình thấy hơi tiếc vì sau hai quyển kia mình đã bắt đầu thích đọc những đoạn miêu tả rất quái gỡ rùng rợn của tác giả.
3
116202
2015-02-12 11:10:28
--------------------------
152173
5017
Trong khi Flynn đưa người đọc đi sâu vào những vùng tăm tối nhất của ham muốn, đau đớn và sự cô độc với cách hành văn bỏ nhỏ các manh mối tinh tế và điêu luyện thì Alpha book đưa người đọc lên đến đỉnh cao của sự cẩu thả về cả biên dịch lẫn biên tập.
Cuốn sách này thật sự là một cuộc đấu tranh không khoan nhượng giữa sự ham muốn theo dõi đến cùng câu chuyện hấp dẫn và sự ngao ngán muốn từ bỏ khi bạn liên tiếp gặp phải những câu văn tối nghĩa và lỗi chính tả.
Tôi còn mua 2 cuốn khác của Flynn là Gone Girl và Bóng Ma Kí Ức, hy vọng là chất lượng không tệ như Vết cắt hành xác, nếu không từ nay chắc phải chia tay với Alpha book quá.
Thật sự là thất vọng.
3
18937
2015-01-22 13:51:13
--------------------------
