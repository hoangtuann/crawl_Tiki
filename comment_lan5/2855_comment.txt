416525
2855
Về hình thức, cuốn sách rất đẹp và bắt mắt cho trẻ em. Được làm bằng chất mút xốp rất nhẹ, không có cạnh nhọn nên cũng an toàn cho bé. Nhưng cần cải thiện về mặt nôi dung. Tuy là sách cho trẻ em, nhưng cũng cần phải có nhiêu thông tin hữu ích. Nôi dung cuốn sách quá đơn giản, lặp đi lặp lại và không có nhiều thông tin.
Ví dụ như : “Xe bus một tầng là xe bus có một tầng để chở hành khách”. Không phải điều đó là quá hiển nhiên sao? Thay vì nói thế này có phải sẽ cung cấp thêm thông tin cho trẻ. “Xe bus một tầng là loại xe bus được sử dụng phổ biến ở nhiều nước trên thế giới”. 

Góp ý cho Tiki, mình thấy tiêu đề trên website ghi “Nó Hoạt Động Như Thế Nào Nhỉ? - Xe Buýt: Bus (Song Ngữ Anh - Việt)”. Mình chọn mua cũng vì nghĩ đây là sách song ngữ. Nhưng cuốn sách hoàn toàn bằng tiếng việt. Cảm thấy có chút thất vọng như bị lừa. Mong Tiki sủa lại nội dung đăng trên website.
3
909375
2016-04-15 11:06:10
--------------------------
