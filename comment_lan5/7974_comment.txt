428040
7974
Đây là một cuốn sách không thể bỏ qua, chúng ta sống ở đời thì phài cần đó là kỹ năng sống - cuống sách này là một trong số các cuốn sách dạy kỹ năng sống hay mà mình đã từng đọc. Trong sách đưa ra các câu truyện từ nhiều tác giả khác nhau rồi hội tụ lại thành một cuốn sách đầy ý nghĩa. Những lời khuyên - góc suy nghĩ khiếng chúng ta thực sự phải suy nghĩ, phải suy nghĩ về trách nhiệm của đời mình. Như Giovanni Boccaccio đã nói: “Trí tuệ là cội nguồn hạnh phúc của con người”. Thật vậy có trí tuệ bạn sẽ được trải nghiệm niềm vui và mang lại hạnh phúc cho những người xung quanh. Sách dạy chúng ta những điều mà ta chưa biết. Mình nghĩ các bạn nên đọc nhất là các bạn teen như mình để thay đổi suy nghĩ, thay đổi cuộc sống ngay hôm nay..!
5
862837
2016-05-10 15:16:10
--------------------------
243967
7974
cuốn sách KĨ năng sống dành cho học sinh- biết trân trọng là cuốn sách rất hữu ích cho chúng ta.Đặc biệt đối với mình là học sinh và đang tuổi lớn mình cần rất nhiều thứ để học, nhiều thứ lạ muốn khám phá.Tuy nhiên cũng vì vậy mà ta quên đi những gì quan trọng đối với ta , xung quanh chúng ta mà ta không hề nhận ra.Cho nên nhờ cuốn sách này mà mình hiểu hơn về cuộc sống , giúp mình biết trân trọng hơn những gì mình đang có và luôn nắm giữ nó để có cuộc sống hạnh phúc hơn
3
416933
2015-07-27 21:38:56
--------------------------
186404
7974
Nếu bạn chưa bao giờ cầm quyển sách này thì bạn nên làm ngay đi! Quyển sách không chỉ dành riêng cho học sinh mà người lớn cũng có thể đọc được! Đấy không chỉ đơn thuần là một quyển sách mà nó còn là những bài học về cuộc sống của ta! Nó giúp ta nhận ra cái đẹp trong cuộc sống như thời gian, bạn bè, ... nhưng đặc biệt nhất là tình thân. Quyển sách này không chỉ lấy mất bao nhiêu là nước mắt của tôi mà nó còn giúp tôi nhận ra dường như tôi đã quên mất cái gia đình khi xưa còn đầm ấm. Tôi khuyên các bạn nên mua quyển sách này để có thể nhận ra những sai lầm mà mình mắc phải và sữa chữa chúng trước khi quá muộn.
5
361292
2015-04-21 08:04:50
--------------------------
118676
7974
Đúng như với cái tiêu đề của sách loài cây đắng nhất nhưng nở hoa thơm nhất . Thật vậy mỗi người chúng ta để có được thành công ước mơ và hoài bão của mình thì phải trải qua biết bao nhiêu là khó khăn trở ngại như câu nói : " MUỐN THẤY ĐƯỢC CẦU VỒNG THÌ PHẢI TRẢI QUA CƠN MƯA " Một ngày tươi đẹp sẽ luôn đến với những người biết nắm bắt cơ hội mà không chờ đội nó .Hạnh phúc cũng như vậy mỗi người chúng ta đều nghĩ hạnh phúc chính là những thứ lớn lao xa hoa mà chúng ta không thể nào có được cứ muốn mãi đi tìm kiếm nhưng họ không hè biết rằng hạnh phúc đang ở ngay bên cạnh bạn những vòng tay, và cã những cái ôm thật ấm áp con tim. Qua tác phẩm người con biết được rất nhiều điều về cách làm người cách mở rộng tái tim và cả sự quý báu mà thời gian đã đêm đến cho mỗi người chúng ta . Các bạn hãy cùng đọc sách và cảm nhận một cách sâu sắc về cuốn sách này nhé !!!!!!!!!!!!!
5
378827
2014-07-30 15:59:58
--------------------------
