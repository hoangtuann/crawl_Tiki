218024
6069
Thị trường chứng khoán đã có ở nước ta hơn 10 năm và ngày càng phổ biến đến đại đa số người dân hơn. Đây là được xem là một kênh đầu tư hấp dẫn trên thế giới. Tuy nhiên, không phải người nào tham gia thị trường này đều hiểu luật. Vì vậy, đề hiểu rõ về cơ cấu vận hành của thị trường chứng khoán thì cần phải hiểu luật chứng khoán đề khi tham gia vào không bị vi phạm luật pháp. Quyển luật này trình bày đầy đủ những nội dung về những vấn đề như: công ty đại chúng, công ty chứng khoán,... Nếu muốn hiểu rõ hơn về một số điều luật thì có thể tìm đọc thêm quyển Luật Doanh nghiệp. Quyển sách này phù hợp với các tổ chức, công ty và cá nhân.
5
356759
2015-06-30 13:34:23
--------------------------
197429
6069
Quyển luật Chứng khoán này được trình bày khá rõ ràng, cụ thể. Các điều luật vô cùng dễ hiểu cho những người chân ướt chân ráo tham gia vào thị trường này. Luật khá ít (so với các bộ luật khác) nên không mất quá nhiều thời gian để đọc qua lần đầu. Nếu bạn cần một cuốn luật CK cập nhật nhất đến thời điểm này thì bạn nên mua nó, để nâng cao trình độ của mình và có hành động phù hợp trong TTCK ở Việt Nam. Điểm trừ của sách là còn mắc lỗi chính tả (dù là Luật) và chất lượng in nhiều trang kém, giấy mỏng, dễ rách.
4
123188
2015-05-17 16:55:15
--------------------------
180028
6069
Với những ai đang tham gia trên thị trường chứng khoán, những ai muốn tham gia vào thị trường này hoặc những ai đang làm trong các công ty tài chính, chứng khoán thi ngoài những kiến thức và kinh nghiệm thì mình nghĩ những đối này cần phải biết thêm luật chứng khoán để có thể biết giao dịch của mình  hay tư vấn cho khách hàng đúng pháp luật hay không, Quyển sách luật chứng khoán 2013 này sẽ cung cấp cho bạn tất cả các thông tin mà bạn cần phải biết khi bạn tham gia thị trường này. Quyển sách này đã được sửa đổi và bổ sung một số điều của luật chứng khoán 2006 vì vậy nó sẽ đầy đủ hơn.
5
588073
2015-04-08 14:55:39
--------------------------
