349504
6195
Tôi mua cuốn sách này trước tiên vì tiêu đề sách, sau khi xem thử nội dung bên trong thì thấy rất ngộ nghĩnh và đáng yêu nên đã quyết định đặt mua và không thất vọng khi nhận được sách. Các bạn đồng nghiệp của tôi khi nhìn thấy cuốn sách và tiêu đề của sách cũng đều thấy rất ấn tượng và ai cũng cầm lên xem thử. Tôi nghĩ các bạn nhỏ ở mọi lứa tuổi nên đọc/được đọc những cuốn sách như thế này để hiểu thêm và yêu thương bố mẹ của mình nhiều hơn.
5
922206
2015-12-08 16:39:11
--------------------------
322704
6195
Cuốn sách nằm trong bộ 3 tuyển tập sách của tác giả người Pháp dành cho thiếu nhi. Vì là sách dành cho thiếu nhi nên nội dung đơn giản, dễ hiểu, trình bày minh hoạ sinh động, màu sắc bắt mắt rất dễ thương, chất liệu giấy màu láng kèm một bìa cứng hình vẽ rời có thể xem như bookmark cho mỗi cuốn. Nội dung thích hợp để giải trí những lúc rảnh rỗi hay chán chường, có thể cho nhóc em ở nhà đọc. Bản thân mình đọc xong thì thấy thương thêm bố mẹ từ những góc nhìn rất nhỏ thôi, là góc nhìn của trẻ nhỏ, từ những ngày còn là con nit với những điều nhỏ nhoi đon giản để lại nhớ về những lý do ba mẹ làm nhiều thứ chung quy lại cũng là vì thương mình mà thôi. 
Nói tóm lại, sách rất đáng yêu, mình hoàn toàn hài lòng
5
55927
2015-10-16 20:35:05
--------------------------
310239
6195
Để hiểu hơn về bố mẹ đây là quyển sách rất hữu ích cho mọi người, nhất là những bạn trẻ tuổi, bố mẹ sẽ có những hành động cấm đoán, la mắng hoặc bắt bạn làm theo ý họ. Nhưng dù là gì thì tất cả những điều họ làm đều muốn tốt nhất cho bạn mà thôi. Hình ảnh sinh động, nét vẽ đẹp và lôi cuốn, rất đơn giản và dễ hiểu, tin rằng quyển sách này sẽ lôi cuốn bạn từ những dòng đầu tiên. Thật sự mình rất thích tác giả này vì ông đã đơn giản hóa những thứ phức tạp thành những câu vô cùng dễ hiểu.
5
399161
2015-09-19 13:34:42
--------------------------
