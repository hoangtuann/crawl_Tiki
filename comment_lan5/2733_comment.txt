489852
2733
Mình đã được học sơ qua môn học Châm cứu, từ đó tìm hiểu thêm 1 số thông tin thấy môn học này thật là hay, nâng cao sức khỏe cũng như giảm thiểu lượng kháng sinh khi bị bệnh. Khi mua quyển sách này, mình nghĩ là tìm hiểu 1 số phương pháp để xoa vuốt cho con thôi. Nhưng khi nhận được sách mình thấy rất bổ ích, các phương pháp xoa vuốt rất đơn giản được viết và minh họa bằng hình ảnh rất dễ hiểu. Bé nhà mình bị ho, cảm khi thay đổi thời tiết, mình cố gắng xoa vuốt theo sách hướng dẫn cũng thấy bệnh của con nhẹ và nhanh khỏi hơn.
4
734643
2016-11-10 11:31:21
--------------------------
