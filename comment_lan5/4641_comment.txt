439334
4641
Cũng như sách đã nói, các bạn 11,12 mua sách này rất là tốt . 
Bìa ngoài của sách khá mỏng, nhưng giấy bên trong thì rất tốt.
Sách vừa có hướng dẫn phân tích thơ, vừa có phân tích văn xuôi, và nghị luận xã hội, và có nhiều đề sát với cấu trúc đề thi THPT hiện nay để cho chúng ta tham khảo. Nếu các bạn thấy chưa đủ thì có thể tìm xem bộ sách " Cẩm nang luyện thi đại học Ngữ Văn" cùng tác giả - ThS. Lê Xuân Soan. Tuy nhiên tôi thích sách có góc nhọn hơn là cong.
4
554626
2016-05-31 08:59:28
--------------------------
382293
4641
Sách được biên soạn rất kĩ càng và chọn lọc đề thi rất hay , phù hợp với cấu trúc đề thi THPT hiện nay . Sách có cả hai phần nghị luận văn học và nghị luận xã hội kèm theo với đó là một số bài văn mẫu được tuyển chọn rất kĩ lưỡng . Nhìn chung quyển này cũng không tệ nhưng cũng không hay cho lắm vì nó chưa phân tích rõ phần đọc hiểu mà chỉ hướng về phần làm văn . Gía như thế thì cũng coi như là ổn và không có vấn đề gì
5
860686
2016-02-18 22:24:18
--------------------------
198399
4641
Sách được chia thành nhiều phần và khá bám sát cấu trúc đề thi quốc gia 2015! Có cả 2 phần nghị luận xã hội và văn học! với các tác phẩm văn học sách khai thác khá nhiều đề, khá đa dạng trong mỗi bài học và có đầy đủ các dàn ý hoặc bài tham khảo hướng dẫn làm bài, kể cả câu tự luận ngắn có thể cho trong phần đọc hiểu! Các bạn có thể mua sách này để tự học vì nó tương đối khá đa dạng và đầy đủ các phần cần thiết theo từng tác phẩm riêng cả văn xuôi và thơ! nhìn chung thì mình thấy mua quyển này cũng rất đáng! đặc biệt là với các bạn thi tự nhiên nhưng muốn vượt qua môn văn 1 cách nhẹ nhàng hơn! chúc các bạn may mắn và thành công nhé!
5
134995
2015-05-19 21:29:10
--------------------------
