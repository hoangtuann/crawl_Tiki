505857
4417
Tôi mua nhiều sách trên tiki nhưng đây là lần đầu tiên viết review về 1 quyển tiểu thuyết, vì nó quá tệ nên phải cảnh báo tới mọi người:

- Văn phong viết kém, đa phần diễn dịch kiểu nội tâm, đọc rất buồn ngủ.
- Các sự kiện và tư liệu ko có gì mới, góc nhìn cũng ko có gì mới khi lý giải các hiện tượng lịch sử.

Dù vậy tôi cũng có gắng đọc hết quyển sách và tự hỏi là tại sao người viết có thể xuất bản 1 quyển sách tệ như thế này? Viết là sở thích nhưng in ra là trách nhiệm, thật chán cho các cây bút VN
1
615608
2017-01-05 21:28:56
--------------------------
392346
4417
Cuốn sách đã gây cho mình khá nhiều bất ngờ, và là những bất ngờ tích cực.
- Lối viết rất hiện đại, mới lạ so với những cuốn sách mình từng được đọc qua. Dù vậy, tác giả không hề làm mất đi dù chỉ một chút tính cổ kính của câu chuyện.
- Mỗi chương mình đọc qua đều đúc kết được nhiều bài học về đối nhân xử thế, đạo làm người, nghĩa vợ chồng.. qua những tình huống và tâm sự của từng nhân vật.
- Đều mình thích nhất ở cuốn sách là nhân vật lịch sử đều được tác giả khắc họa một tính cách rất riêng, tâm tư, giọng điệu của mỗi người mỗi khác, thể hiện khí phách của riêng mình.
 Tuy nhiên, khuyết điểm là bị lỗi chính tả, và mình nghĩ không phù hợp với lứa tuổi dưới 16 để đọc.
 Chung quy lại là rất hay, mình khâm phục tác giả trẻ tuổi này.
4
1130870
2016-03-06 19:29:21
--------------------------
217388
4417
Nhân Huệ vương Trần Khánh Dư là một chiến tướng có nhiều công lao thời Trần, nhưng cũng bị lịch sử sau này “phán xét sai” trong nhiều vấn đề. Nổi bật nhất là quyết định “tiền trảm hậu tấu”, kháng lại lệnh triệu hồi về kinh thành của vua, để ở lại chiến trường, bày mưu tính kế đánh tan đoàn thuyền chở quân lương của giặc Nguyên Mông. Nhờ đó làm thay đổi cục diện cuộc chiến chống quân Nguyên Mông xâm lược.

Tác giả đã trở về quá khứ, viết lại câu chuyện được Nhân Huệ vương kể lại. Cuốn sách có lối tiếp cận lịch sử khá độc đáo như vậy. Hơn nữa tác giả lại là một người còn khá trẻ. Đây là điều đáng quý vì những người còn trẻ thường ít viết về chủ đề lịch sử.

Sách được in đẹp. Tuy nhiên bìa sách dường như chưa thể hiện được cốt truyện bên trong và giá của nó có vẻ hơi đắt!
3
598102
2015-06-29 11:14:47
--------------------------
198713
4417
Toàn bộ nội dung quyển tiểu thuyết tác giả nói rằng mình đang kể lại giấc mơ. Một giấc mơ trở về quá khứ để gặp Nhân Huệ vương " Trần Khánh Dự" và nghe ngài kể lại chiến công đánh thắng quân Nguyên của nhân dân Đại Việt mà Nhân Huệ vương có đóng góp quan trọng qua việc đánh tan đoàn thuyền chở lương của giặc để góp phần quyết định vào thằng lợi chung. Thông qua những dòng lịch sử được ghi lại trong Đại Việt Sử Ký Toàn Thư nói về quan hệ ngoại tình giữa ngài và công chúa Thiên Thụy con gái của vua Trần và con dâu của Quốc công tiết chế Trần Hưng Đạo là chị em với ngài ( một việc hay gặp trong thực tế đã được sử sách ghi lại ở thời Trần) tác giả đã xoáy sâu vào tình tiết có tính thù hận cá nhân đó để nêu lên tinh thần đồng lòng tất cả cho đất nước cả vua, quan, trên dưới thời nhà Trần sẵn sàng bỏ qua những mối hiềm khích, tư thù cá nhân để cùng nhau đánh giặc, giữ vững non sông đất nước. Trong hoàn cảnh sự căm thù dành cho quân giặc lớn hơn tất cả các mối thù cá nhân khác. Tiểu thuyết cũng có những trang mô tả đặc sắc cuộc chiến tranh chống quân Nguyên của cả nước khí đó, mặc dù có một số tình tiết hư cấu khá xa so với lịch sử.
Sách chỉ có 287 trang, khổ nhỏ 13X20, chất lượng giấy bình thường chữ rõ, nhưng có giá theo tôi thấy là hơi cao
3
504882
2015-05-20 16:34:38
--------------------------
