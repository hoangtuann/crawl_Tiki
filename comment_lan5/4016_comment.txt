495231
4016
Sách chia từ vựng theo từng chủ đề giúp các bạn học dễ nhớ, cung cấp được nhiều từ vựng sẽ xuất hiện trong đề. Tuy nhiên chất lượng giấy in hơi mỏng nên chữ ở trang này xuất hiện chữ mờ mờ trang kia. Nhưng giá thành rẻ nên cũng xứng đáng bỏ tiền mua. Chúc các bạn thi tốt nhé! 
4
694276
2016-12-11 14:41:22
--------------------------
345066
4016
Mình là sinh viên và khả năng tiếng anh còn kém,chưa đủ vốn từ để ôn thi toeic,vì thế mình mua sản phẩm này để bổ trợ kiến thức cho vốn từ vựng tiếng anh của mình.Nôi dung sách gồm những từ tiếng anh thông dụng,theo chủ đề,rất dễ nhớ.Ngoài ra mình có thể dựa vao đó để tự làm flashcard nữa.Hình thức thì giấy tương đối tốt.Giá cả cũng phù hợp với túi tiền của sinh viên.Tiki giao hàng thì sản phẩm đã được bao bọc tương đối tốt vì mình sử dụng dịch vụ bookcare.Cảm ơn cuốn sách hữu ích nàynày.
5
950930
2015-11-29 19:01:16
--------------------------
296038
4016
Sách đã chọn lọc những từ vựng phổ biến trong đề TOEIC. Thực sự quyển sách này rất có ích đối với mình. Từ vựng được chia ra học theo chủ đề, rất thuận lợi để học và phân bố nội dung bài học. Với mỗi từ còn có ví dụ kèm theo nhằm giúp việc học từ vựng dễ dàng hơn. Tuy nhiên với một số ví dụ còn chưa được hay cho lắm, mang nặng ngôn ngữ kinh tế vốn đã khó nhớ. Bìa được, giấy hơi mỏng và font chữ theo ý kiến chủ quan thì mình không được thích cho lắm. Giá cả phải chăng. Dù sao cũng không hối hận khi mua quyển sách này.
4
620043
2015-09-10 20:52:36
--------------------------
243995
4016
Vì nhằm mục đích là hướng tới kì thi toeic nên mình cần rất nhiều từ vựng để củng cố thêm kiến thức nên mình đã sở hữu ngay cuốn từ vựng tiếng anh cho người luyện thi toeic này. cuốn này tuy nhỏ và không dày nhưng thật sự rất bổ ích. khối lượng từ vựng tiếng anh trong cuốn này rất tổng hợp, chắt lọc và phân chia theo các chủ điểm rõ ràng để người đọc dễ dàng tổng kết, nắm bắt và bổ sung vốn từ vựng . cuốn sách này được chia thành hai phần , phần I là l từ vựng theo chủ điểm, phần II là luyện tập tổng hợp . cuốn sách sẽ làm bạn hài lòng và bổ sung vốn từ vựng cho bạn trong kì thi toeic.
4
480717
2015-07-27 21:52:47
--------------------------
