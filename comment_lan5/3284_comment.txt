408955
3284
Tình cờ đọc được tập 1 của một người bạn, tôi mê mẩn đến nỗi đi khắp phố Đinh Lễ  để tìm mua nốt những tập còn lại mà không có. Lúc đó Tiki đang hết hàng. May thay Tiki đã lấy thêm sách về & tôi đọc một lèo hết tập 4. Híc, lại phải chờ đợi!
NXB ơi, bao giờ mới có tiếp đây ạ?

Đọc Tam quốc diễn nghĩa của La Quán Trung không thể thấy hết những cái hay, những mặt tốt, những điểm tích cực của Tào Tháo. Bộ truyện này rất đáng đọc cho những ai có lòng mến mộ tài năng của Tào Tháo! Tôi khâm phục nhất là cách dùng người của Ông. Ghét mỗi cái tính đa tình của Ông bao phen gây sóng gió. Ông thật sự là một nhân vật kiệt xuất!
5
1082752
2016-04-01 16:20:03
--------------------------
395058
3284
Tào Tháo là nhà quân sự, chính trị kiệt xuất, cả cuộc đời chỉ mong chinh phục cát cứ, khôi phục lại giang sơn nhà Hán. Là kẻ ôm mộng chí lớn, nhưng ông ta lại giàu tình cảm trước người phụ nữ Đinh thị của mình, Trong tập này thể hiện rõ được hết tầm quân sự, chính trị, dùng người của Tào Tháo, Giận hời, đa nghi, thù hận của ông được xóa bỏ qua cách đãi hiền, chiêu hiền. Ông quả thật kiệt xuất. Qua tác phẩm này để thấy được con người thật, biến trá,dũng cảm, bình tĩnh trước thế sự và thấy được 1 Tào Tháo hiền hòa, độ lượng của ông. Nhân tiện hỏi khi nào có tập 5 vậy mọi người, lâu quá :D
5
1084383
2016-03-11 12:43:30
--------------------------
322475
3284
Đợi tập 5 bộ Tào Tháo - Thánh nhân đê tiện lâu quá.
Hiện mới chỉ xuất bản ra được 4/10 tập.
Hi vọng thời gian xuất bản một mẻ 6 tập cho thỏa đợi mong.

Thời gian này, mới là những tập đầu. Tác giả đã miêu tả rất hay về cậu bé TT với tuổi thơ rất hồn nhiên. Trốn nhà đá gà với bạn thủa thiếu thời. Tác giả cũng khéo léo  thể hiện được mối quan hệ hỗ trợ từ những người bạn này trong con đường lập nghiệp của Tháo. Nào là Sái Mạo (Thái Mạo) dưới trướng của Lưu Biểu, người mà về sau phản chủ quy hàng TT. Hay như ae họ xa của gia tộc họ Tào: các tướng dưới quyền Tào Nhân, Tào Hồng, Hạ Hầu Đôn, Hạ Hầu Nguyên Nhượng ... Chung lại rất ấn tượng giai đoạn về tuổi thơ của TT rất sinh động, rất gần với tuổi thơ.
2
800219
2015-10-16 12:51:10
--------------------------
291140
3284
Nối tiếp ba cuốn sách trên tập một hai ba, tập bốn làm mình tò mò vì dòng chữ cuối cùng của quyển sách có viết " (Còn nữa)" không biết rồi đây Tào Tháo sẽ như thế nào nữa đây một con người trắng đen lẫn lộn với những sóng gió thăng trầm của cuộc đời từ lúc 19 tuổi đã mở ra biết bao nhiêu sự kiện để rồi đây khi các thế hệ sau này đọc lại có cách nhìn khác hơn về ông không thể nói ông tốt mà cũng không thể nói ông không tốt, mình rất hóng tới tập năm
5
686606
2015-09-06 01:14:17
--------------------------
209528
3284
Xem phim về Tào Tháo mình cũng đã xem rồi, mình rất thích những bộ phim cổ trang về lịch sử Trung Quốc như thế, hôm nay dạo quanh Tiki thấy có xuất bản bộ sách viết về nhân vật Tào Tháo khiến mình rất tò mò và muốn sở hữu. Vốn dĩ Tào Tháo là một con người có địa vị lớn ở triều đại xa xưa Trung Quốc, có công dẹp loạn... Nhưng mình xem ở mọt số bản dịch cung cấp thì Tào Tháo rất giảo hoạt, gian trá và nhiều đức tính không tốt khác khiến mình rất tò mò về nhân vật có một không hai này. Đây là một cuốn sách hay, người đọc sẽ hiểu nhiều hơn về Tào Tháo thông qua tác phẩm này.
4
537001
2015-06-17 17:57:38
--------------------------
208142
3284
Tào Tháo là người mình thích nhất trong bộ truyện Tam Quốc Diễn Nghĩa.Tuy đã có ai nói "Tào Tháo là người đã có công lớn trong việc dẹp loạn khăn vàng và Đổng Trác. Tuy nhiên, hình ảnh về ông không được các nhà nho học ưa thích và thường được mang ra làm biểu tượng cho sự dối trá, vô liêm sỉ.",nhưng mình vẫn thích ông vì tài năng của ông trong việc quân.Ông là một người có tính cách vô cũng phức tạp,làm mình suy diễn đến đau cả đầu mà vẫn không hiểu nổi ông.Tuy vậy,mình là người trọng tài năng nên dù có thế nào Tào Tháo vẫn là nhân vật ưa thích của  mình.Cuốn sách này ra mình rất thích.Bìa sách rất hợp với hình tượng của ông!Tên truyện cũng ấn tượng lắm
5
440861
2015-06-14 11:09:47
--------------------------
