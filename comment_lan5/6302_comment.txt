573733
6302
Sách ấn bản rất đẹp, tuy nhiên nội dung còn nhiều lỗi chắc chắn do chuyên môn của người hiệu đính kém. Đề nghị NXB cần chăm chút hơn cho SP
4
290051
2017-04-15 03:37:33
--------------------------
569273
6302
Cuốn sách là một kênh tham khảo hữu ích cho những bạn yêu mến về phong thủy, đặc biệt là phong thủy về hướng nhà ở, hướng bếp, hướng gường ngủ, ... Sách viết đơn giản, dễ hiểu, dễ sử dụng trong việc xem giữa tuổi gia chủ với hướng nhà nào là phù hợp với vận mệnh trong phong thủy theo dân gian ngày xưa truyền lại.
4
600487
2017-04-10 16:01:23
--------------------------
287416
6302
Tôi tìm mua cuốn sách này vì đang có nhu cầu tìm hiểu về thuật Phong Thủy. Trước rừng sách về lĩnh vực này, tôi khá hoang mang khi mình không biết phải chọn cuốn sách gì để bắt đầu tìm hiểu vì trong đầu chưa hề có một khái niệm nào về Phong thủy cả. Cuối cùng sau một hồi lựa chọn trên tiki, tôi đã quyết định đặt mua cuốn "La Bàn Phong Thủy Toàn Thư" này của tác giả Ngô Bạch và NXB Văn hóa thông tin. Tôi khá hài lòng với nội dung cuốn sách mang lại, cùng cách trình bày khoa học, dễ hiểu, tranh ảnh được in màu rất đẹp và rõ nét, bìa cứng dày dặn giúp sách không bị hư hỏng.
4
29716
2015-09-02 18:26:19
--------------------------
