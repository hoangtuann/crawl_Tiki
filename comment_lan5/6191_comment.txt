473231
6191
Sách được thiết kế giống như một cuốn cẩm nang, nhỏ gọn rất thích hợp đã mang đi tra cứu bất cứ đâu. Mình ấn tượng nhất với cái bìa, được trang trí rất phù hợp với một cuốn sách lịch sử, thoạt đầu nhìn cứ tưởng là sách cũ và có hơi bẩn nhưng nhìn kỹ lại mới biết đó là do thiết kế của sách. Nội dung cô đọng liệt kê theo trình tự năm tháng nên rất dễ tra cứu. Sách gần như đã tóm gọn được hết tất cả những sự kiện quan trong của thế chiến II. Mình rất hài lòng vì mua được sản phẩm ưng ý!
5
14119
2016-07-10 13:14:47
--------------------------
461592
6191
Những con số biết nói, những dòng chữ ngắn gọn súc tích nhưng tóm gọn được những trận chiến và tình hình phân chia toàn diện vô cùng hấp dẫn! Khi đọc những số liệu này có thể dễ dàng nhận thấy chiến tranh vô cùng tàn khốc, không nhẹ nhàng khô khan và theo trình tự như những bài học trong sách giáo khoa mà nó cần phải nhìn nhận từ rất nhiều phía, hơn hết nó không chỉ khiến con người ta thống khổ, còn là điều kiện cho kẻ khác làm giàu. Có thể là điều tồi tệ nhưng cũng là điều tốt đẹp cho từng loại người... đều phải đánh đổi từ rất nhiều thứ... Tôi đặc biệt thích phần vũ khí và chiến thuật kỳ quặc, nó chứng minh bất kể một quốc gia hay cá nhân nào dù từng khôn ngoan đến mấy cũng có lúc hành động thật ấu trĩ và liều lĩnh, tất cả chỉ vì chiến thắng... 
5
998926
2016-06-27 21:44:14
--------------------------
437500
6191
Truớc tiên, cuốn sách có kích cỡ khá gọn gàng, như một cuốn sổ tay, trình bày bìa đẹp mắt, tuy nhiên chất luợng giấy chưa tốt lắm. Về nội dung, cuốn sách kể khá nhiều câu chuyện hay, thú vị về tất cả các quốc gia tham gia cuộc chiến, từ Anh, Mỹ, Pháp, Nga cho đến Đức, Ý, Nhật,.... Không chỉ kể chuyện bên lề về những cuộc chiến đẫm máu, những mẩu chuyện đặc sắc như binh lính Anh và Đức cùng mừng giáng sinh trên chiến truờng cho ta thấy tình nguời luôn hiện hữu ngay cả ở những nơi chiến tranh ác liệt nhất. Một cuốn sách đáng đọc nếu muốn tìm hiểu về WW2.
4
1195417
2016-05-28 12:15:59
--------------------------
428345
6191
Mình mua cùng với cuốn ct thứ 1. Cả hai cuốn nhìn đáng yêu lắm, giấy bìa nhìn chất lượng cực kì. Mua sách ở tiki thích nhất là chất lượng sách. Cuốn sách này khá bé cầm đọc rất tiện tay mn ạ. Lượng kiến thức trong này cũg khá nhiều và hay. Mình sắp thi đại học nên hốt e này về để đọc kham khảo., nội dung trình bày chi tiết lắm hi. Bên cạnh còn kèm theo các vấn đề liên quan như phim ảnh, làm sách rất phong phú. Mình rất thích cuốn sách này hehe
4
1292353
2016-05-11 06:25:05
--------------------------
426925
6191
Ban đầu khi đặt hàng tôi cứ nghĩ sách phải to dày lắm, vì hầu hết công trình nghiên cứu về chủ đề này phải vô cùng đô sộ. Khi nhận sách thấy khổ nhỏ gọn gàng tôi khá ngạc nhiên. Tuy thông tin có giản lược nhiều nhưng vẫn bao chứa khá đầy đủ về nhiều khía cạnh của cuộc đại chiến lớn nhất lịch sử nhân loại. Các phần được chia theo dạng "fact" (sự thật) nên rất dễ đọc, không nhàm chán mà trái lại khiến người ta tò mò hơn.

Sách có vài hình họa phá cách khá ngộ nghĩnh, nhưng tôi không thích lắm vì có vẻ hợp với truyện hài hơn đề tài nghiêm túc này (dù cũng phải nói là lối viết/dịch cũng khá hài hước, giúp tiếp thu thông tin dễ dàng hơn). Bìa sách mạ bóng cũng rất đẹp.
4
274545
2016-05-07 22:43:33
--------------------------
368942
6191
Mới nhận được sách tuần trước. Ban đầu ms mua về cứ tưởng khổ to hóa ra lại nhỏ gọn như vậy. Sách tuy nhỏ nhưng rất tiện, chất liệu giấy cũng tốt. Nội dung thì miễn bàn luôn. Cung cấp số liệu chính xác, đầy đủ. Đúng lúc mình cũng đang cần tài liệu về phần này nên mua luôn, rất hữu ích. Cầm đi lại rất tiện lợi. Đây chính xác là một cuốn sổ lưu lại lịch sử thế chiến hay và đáng đọc. Xứng đáng có trong kệ sách của bạn. Có rất nhiều điều hay và bổ ích.
4
613896
2016-01-14 21:10:10
--------------------------
168677
6191
Những Câu Chuyện Lịch Sử Khác Thường - Chiến Tranh Thế Giới Thứ Hai
Cuốn sách nhỏ gọn và hữu ích vô cùng. Ngoài lời giới thiệu của tiki về những sự kiện được liên kết với những con số biết nói thì điều mình đặc biệt bất ngờ là văn phong rất thú vị không hề gây cảm giác nhàm chán với những câu chuyện thực tế của quân đội trong chiến tranh cùng những lời trích dẫn của một số nhân vật chính trị bấy giờ. Tuyệt vời hơn là khi đọc đến cuối sách còn còn phần niên biểu và tra từ theo vần. Mình thấy đây là phần hay nhất bởi lẽ nó liệt kê những mốc thời gian với những sự kiện chính trị khiến cho người đọc xâu chuỗi một cách khái quát tình hình Chiến tranh thế giới thứ hai.
4
503222
2015-03-16 23:52:06
--------------------------
