429629
8252
Cuốn sách "Phân Dạng Và Phương Pháp Giải Toán Hình Học 11 Cơ Bản" này rất có ích cho việc học Hình học 11 của mình. Sách được trình bày hợp lí và khá bắt mắt, giấy in chữ rỏ nét. Mỗi dạng có bài tập ví dụ giải rất chi tiết và dễ hiểu. Các bài tập trong sách được sắp xếp theo thứ tự từ dễ đến khó thuận tiện cho việc tìm kiếm và ôn tập củng cố kiến thức cũng như nâng cao kiến thức cho học sinh. Mình rất hài lòng về cuốn sách "Phân Dạng Và Phương Pháp Giải Toán Hình Học 11 Cơ Bản" này.
4
1190044
2016-05-13 16:42:34
--------------------------
402370
8252
Theo mình nghĩ quyển sách này rất hữu ích với các học dinh lớp 11 như mình. Quyển sách này tổng hợp đầy đủ kiến thức trong chương trình hình học 11 và phân chia theo dạng. Mỗi dạng có bài tập ví dụ giải rất chi tiết và dễ hiểu. Sau đó có các bài tập tổng hợp một chưng có lời giải và cả bài tập tự giải nữa. Quyển sách này đã giúp mình khá nhiều trong việc học hình ( nhất là phần hình học không gian). Nhờ nó mà mình không còn sợ phần hình học nữa. Chỉ có một điểm trừ là giấy hơi mỏng
4
940174
2016-03-22 10:22:03
--------------------------
225957
8252
Trước tiên là về mặt bình thức, sách Phân Dạng Và Phương Pháp Giải Toán Hình Học 11 Cơ Bản (Tái Bản) có cách trang trí bìa đẹp, giấy bên trong khá tốt, màu xám trắng. Cách trình bày bố cục các bài học cũng rất hợp lí, thuận tiện cho việc tìm kiếm. Mỗi bài học đều của Phân Dạng Và Phương Pháp Giải Toán Hình Học 11 Cơ Bản đều có tóm tắt lí thuyết, bài tập khá phong phú, giải chi tiết, khá dễ hiểu. Nói chung là một cuốn sách hình học cần thiết cho học sinh 11.
4
415181
2015-07-11 10:27:41
--------------------------
