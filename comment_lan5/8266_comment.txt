337522
8266
Đây là quyển sách thứ bao nhiêu của bác Thái mà cháu sở hữu nhỉ? Cháu đã mua được rất nhiều, từ " Dấu về gió xóa", rồi" Bốn lối vào nhà cười", " Tiếng thở dài qua rừng kim tước", " Mảnh vỡ của đàn ông", " Hướng nào Hà Nội cũng sông". Quyển nào cũng hay và cháu rất thích cách viết văn của bác Thái. Đọc những quyển sách của bác, cháu có lúc cười, có lúc khóc, nhưng trên hết là cháu thấy nó mang tính hiện thực của xã hội bây giờ. Hi vọng cháu sẽ sưu tập được đủ bộ của bác.
4
293341
2015-11-14 10:29:12
--------------------------
