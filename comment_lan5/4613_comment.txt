183187
4613
Trong số sách luyện thi IELTS mình có thì cuốn này xứng đáng để mua nhất, cũng có thể là do mình kém môn Reading nhưng phải nói là sách rất chất lượng về cả nội dung lẫn hình thức: sách bao gồm 60 bài text được trình bày rất giống chuẩn bài thi thật và mặc dù câu hỏi cho mỗi bài chỉ khoảng từ 7-15 câu nhưng đòi hỏi phải tập trung cao, cuối mỗi text thì có thêm 1 bảng từ vựng cần lưu ý, ngoài ra còn có 3 bài test thi thử luôn. Đúng thứ mình cần.
Tóm lại, với cuốn Winning at IELTS reading này bạn 'tha hồ' luyện tập và đặc biệt là sách được thiết kế cho khung 6-7.5+ nên ai có nhu cầu điểm cao thì nhớ để mắt tới cuốn sách này nhé.
4
190281
2015-04-15 17:18:19
--------------------------
