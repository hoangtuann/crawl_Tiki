444543
4482
MÌnh đã mua cả bộ sách "30 phút cho bé trước giờ đi ngủ" để tặng bé vì nội dung khá đơn giản, dễ hiểu. Hình vẽ tươi tắn sinh động mà không rối rắm, rất phù hợp để tập cho bé nhỏ đọc sách. Nhưng trong Bộ cũng có nhiều quyển mình không ưng ý lắm vì màu sắc và hình vẽ xấu. Ví dụ như "Khỉ con xấu tính" hay "Bản lĩnh của cây nấm"... Quyển "Rùa con đi Siêu thị"  có hình vẽ ko đẹp. Nhìn hình bé ko hình dung được đó là con vật gì. Quyển "Bọ rùa đi mất rồi" cũng có tình trạng tương tự.
3
54291
2016-06-08 16:46:38
--------------------------
