578157
3069
Sách hay, giấy tốt, cải thiện kĩ năng nói đáng kể, mà mình vừa mua xong thì lại giảm giá mới đau chứ :(
5
179908
2017-04-20 07:20:19
--------------------------
417060
3069
Đây là quyển sách giúp chúng tả cải thiện khả năng nói 1 cách toàn diện, bằng những từ mới, cũng như cấu trúc sau mỗi bài học, và thêm cả những mẹo để đạt được điểm cao. Sách ko chỉ giúp ta luyện IELTS mà còn học thêm về các diễn đạt, cũng như giao tiếp trong tiếng Anh. Đọc sách này cũng giúp ta mở rộng ý nhiều hơn, ko khô khan, cụt ý như trước. Mặc dù với giá tiền hơi cao, nhưng rất đáng để sở hữu 1 quyển sách gốc :) Ngữ pháp cũng khá quan trọng khi chúng ta thi speaking và quyển sách cũng biết ta hay gặp lỗi ở đâu, và đưa ra biện pháp sửa đổi dễ dàng nhất 
5
479942
2016-04-16 08:37:13
--------------------------
362378
3069
Sách thiết ké dày dặn, đóng chắc chắn, bìa đẹp, câu văn súc tích không quá dài lê thê, trong sách có những bài mẫu để người học tham khảo và từ đó triển khai được ý của mình tốt hơn, sách tuy có gía thành hơi cao nhưng nếu nhìn chung trên phương diện nội dung thì đây là quyển sách không hề đắt tí nào. Nó giúp người đọc diễn đạt ý mình tốt hơn không chỉ trong kì thi mà ngay cả trong cuộc sống, trong giao tiếp, khắc phục tình trạng bí ý, không diễn đạt được hết ý của bản thân muốn nói.
5
845404
2016-01-01 21:29:00
--------------------------
305076
3069
Tuy là sách khá mắc nhưng chất lượng thì rất xứng đáng. Là một quyển sách hay cho các bạn muốn nâng cao khả năng speaking của mình. Quyển sách cho thông tin chi tiết về cả 3 phần của một bài thi Ielts speaking cũng với đó là những từ ngữ mà người bản địa hay dùng, giúp cho bài nói của mình trở nên tự nhiên hơn, không bị thiếu ý trong quá trình diễn đạt. Sách còn cung cấp những bài trả lời mẫu cho từng dạng câu hỏi giúp các bạn có thể tham khảo một cách dễ dàng, trong đó tác giả sẽ phân tích để trả lời 1 dạng câu hỏi thì chúng ta sẽ bắt đầu từ đâu và những ý gì không thể thiếu trong 1 dạng câu hỏi như thế. Ngữ pháp cũng là một phần khá quan trọng trong bài thi speaking và quyển sách cũng chỉnh sửa những lỗi ngữ pháp mà chúng ta thường gặp phải để cải thiện bài thi nói của mình.
5
476073
2015-09-16 20:29:27
--------------------------
285770
3069
Đây là một quyển sách giúp nâng cao khả năng nói rất tốt. Tôi thây quyển sách không dừng ở phạm vi để luyện IELTS mà còn dùng để nâng cao khả năng diễn đạt bằng tiéng Anh.Trong quyển sách có hướng dẫn 1 số điểm ngữ pháp cơ bản và một số từ vựng thường dùng, Ngoài ra còn hướng dẫn cách dùng các từ nối câu, nhờ đó cách diễn đạt được trôi chảy và mạch lạc hơn. Khi học ngoài cách tư duy làm sao để có thể diễn đạt tốt hơn, tạo ra nhiều ý để nói hơn mà còn giúp củng cố được ngữ pháp, bổ trợ cho các nội dung thi khác trong kỳ thi. Mặc dù giá quyển sách hơi đắt nhưng tính hữu ích mang lại tương đối nhiều.
4
350623
2015-09-01 09:30:06
--------------------------
