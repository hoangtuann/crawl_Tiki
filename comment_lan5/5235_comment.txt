577281
5235
Mình mua bộ hai quyển Sống êm đềm để tặng cho người thân. Mình có nghe qua về nội dung cuốn sách trên mạng, cảm thấy nó rất phù hợp để làm món quà tặng cho người thân mình. Bộ sách có bìa đẹp, chất lượng giấy bên Tiki tốt, mình đã đặt nhiều cuốn trên tiki, và hoàn toàn hài lòng về chất lượng sản phẩm. Sách mình đặt luôn được giao tận nơi, được bọc gói cẩn thận và không méo mó. Nếu bạn muốn tặng quà, thì sách là một món quà ý nghĩa. Cuốn này rất phù hợp để tặng, đặc biệt là cho các mẹ và các bác, những người yêu thích văn phong nhẹ nhàng, lối viết giản dị.
5
778726
2017-04-19 10:00:49
--------------------------
543117
5235
Hình thức đẹp, nội dung hay, tiki gia hàng nhanh, đóng gói cẩn thận
5
521993
2017-03-15 12:09:02
--------------------------
491143
5235
Sách trình bày đẹp nhưng khi ghép 2 cuốn lại chung với nhau từ dòng chữ ở 2 cuốn không ăn khớp, nên mình không thích lắm. còn lại các cái khác đều khá đẹp. Tổng giá trị bộ sách mình mua hết khoảng 160K
Thanks Tiki
5
1541715
2016-11-16 14:12:27
--------------------------
474478
5235
Sách được đựng trong hộp cứng rất đẹp, không sợ sách bị hư hại. Bìa sách và bìa hộp đều rất đẹp, mình cực kì thích bìa của cuốn sách này luôn. Và sống động êm đềm quả là một kiệt tác. Câu chuyện xoay quanh cuộc đời của nhân vật Gregori và Aksinia ở cùng làng quê Cossack ven sông. Chuyện tình của họ làm tôi rất ngưỡng mộ và nể phục, cảm động. Cảm ơn tác giả, nhà xuất bản và dịch giả đã đem đến cho bạn đọc một tác phẩm kiệt xuất như vậy. Thực sự rât thích tác phấm này.
5
412050
2016-07-11 15:09:26
--------------------------
456543
5235
Tuyệt tác Sông đông êm đềm ngày nào vẫn nghe thầy cô nhắc đến giờ cầm trên tay xúc động không thể tả như đến gần hơn với quá khứ con người của thời đại. Ông Mikhail Solokhov thật vĩ đại khi sáng tác tác phẩm này. Sông Đông thật êm đềm hay dữ dội các bạn đọc thử nhé bản thân mình rất thích tác phẩm như thế này, cảm ơn Tiki nhé. Bộ sách nhìn thật lớn hơn hẳn các cuốn khác nhưng cầm rồi đọc và cảm được tác phẩm này rồi các bạn mới thấy hay.
5
686606
2016-06-23 15:17:41
--------------------------
327559
5235
" Sông Đông êm đềm" được biết tới như là bộ tiểu thuyết vĩ đại nhất của nhà văn người Nga Mikhail Sholokhop. Học văn học Nga nghe cô nói là hay lắm nên mua về đọc thử. Cuốn tiểu thuyết này có chủ đề liên quan tới chiến tranh, khắc học 10 năm ( 1912 - 1922 ) cuộc đời của nhân vật chính. Dù chưa đọc nhưng thấy bảo hay hơn cả " Chiến tranh và Hòa bình " cơ, nếu bạn nào có nhiều thời gian thì nên đọc thử. Mình mua hàng trực tuyến rất nhiều, nhưng thích nhất là tiki, giao hàng nhanh, thái độ của nhân viên giao hàng rất dễ thương, dù bị mình cho chạy tới chạy lui hoài vẫn không phàn nàn :)
4
707800
2015-10-28 05:45:05
--------------------------
303506
5235
Tuy tác phẩm có tên gọi là "Sông Đông êm đềm" nhưng cả câu chuyện lại không hề êm đềm tí nào cả. Mà đó là 1 quá trình diễn biến đầy biến động trong hơn mười năm lịch sử của Liên Xô. Phải nói rằng đọc xong tác phẩm mình mới thấm thía được phần nào nỗi mất mát do những cuộc chiến tranh đem lại. Để rồi với nhân vật chính sau khi trải qua toàn bộ mọi chuyện thì những gì anh còn lại chỉ là bản thân với đứa con mà thôi. Điều này làm cho mình cảm thương cho những số phận hẩm hiu cũng chiến tranh và cũng khiến mình căm ghét chiến tranh. Truyện khá dài với nhiều nhân vật nên đọc sẽ khá lâu nhưng theo mình thì đây thật sự là 1 tác phẩm kinh điển
5
39383
2015-09-15 21:19:43
--------------------------
303416
5235
 Tác phẩm chủ yếu nói về cuộc sống con người thời hậu chiến.  Đây xứng đáng là tác phẩm đã đoạt giải nobel Văn học.Mình chỉ mới đọc phần đầu nhưng cảm thấy đây là một tác phẩm hay và khâm phục tài năng tác giả.Ở đoạn đầu tác phẩm viết về chiến tranh thế giới thứ nhất cho ta cảm nhận được sự phi nghĩa tàn bạo của chiến tranh.  Mình thấy thương cảm cho nhân vật Natalia. Ngôn từ của người dịch mình thấy phù hợp thể hiện được tinh thần của tác phẩm. Thiết kế bìa rất đẹp
5
810501
2015-09-15 20:45:19
--------------------------
271847
5235
Những chàng trai Cossack, những cô gái Cossack, những nét văn hoá mang đậm hơi thở Cossack, mang đậm hơi thở sông Đông. Đọc quyển sách này trước hết bạn phải lận lưng cho mình một chút nét văn hoá của người Nga, như thế mới có thể hiểu tác phẩm này một cách toàn vẹn và sâu sắc nhất, và khi đó bạn sẽ không bị bất ngờ khi những nhân vật chính đối thoại một cách thô lỗ với nhau. Đây là một bộ tiểu thuyết sử thi vĩ đại về thế chiến thứ nhất, đọc và cảm nhận những nỗi đau mà chiến tranh đã gây ra cho con người, đó là điều mà tôi ấn tượng nhất với bộ tiểu thuyết Sông Đông êm đềm này!
4
141559
2015-08-19 15:17:44
--------------------------
271348
5235
Mình nghe danh bộ sách này qua lời giới thiệu của thầy giáo dạy Văn hồi lớp 12, nhưng đến gần đây mới có dịp mua. Sau khi đọc vài chương đầu, thực sự mình không thể dứt được. Đây quả thực là một tác phẩm rực rỡ của nền văn học Nga về cuộc sống thời hậu chiến, bao khó khăn của cuộc sống, cùng với những mỗi tình ngang trái đẫm nước mắt của các nhân vật chính. Đây là một tác phẩm vĩ đại của nhân loại, rất xứng đáng để có bộ sách này. Với lại, hộp sách cũng đẹp nữa. ^^
5
77484
2015-08-19 08:02:56
--------------------------
227320
5235
Mình nghe danh tác phẩm này rất lâu. Hôm bữa lướt tiki thì thấy có hộp gồm 2 cuốn rất đẹp nên mua luôn để coi tác phẩm này như thế nào, có xứng đáng là tác phẩm đoạt Nobel văn học năm 1965 hay không?
Kết quả: sau khi đọc 2 chương đầu, mình thấy khá hay, có cảm giác ghiền đọc, nhưng thời gian không cho phép ôm cả bộ đọc đế sáng ^^. Nhưng mình tin là đây là 1 bộ tiểu thuyết sử thi danh bất hư truyền, xứng đáng với 2 chữ "vĩ đại"
4
230629
2015-07-13 23:07:16
--------------------------
225980
5235
Nhắc đến nước Nga, một đất nước giàu truyền thống văn học, nơi sản sinh ra bao nhiêu cây bút đại tài, ta không thể không nhắc đến đại văn hào  Mikhail Solokhov. Có thể nói, chẳng những đây là cây bút của riêng đất nước này, mà còn là một cây bút nổi tiếng khắp năm châu. Hầu hết, trên thế giới, ai cũng đều biết những tác phẩm đồ sộ của ông, trong đó phải kể đến bộ tiểu thuyết rực rỡ, vĩ đại: Sông đông êm đềm!
Tác phẩm là cả một bộ sử kí về chiến tranh, là những trang nhật kí cảm động về cuộc sống con người thời hậu chiến. Chính chiến tranh đã làm cho cuộc sống họ trở nên khó hòa hợp vào cuộc sống bình thường của mọi người!
Có thể thấy, đấy là một tác phẩm lớn của toàn nhân loại, là một cuộc khám phá về chất bên cạnh độ đồ sộ của nó.
5
616731
2015-07-11 10:54:11
--------------------------
200449
5235
Trong số những tác phẩm lớn mà tôi đã từng đọc về đề tài hậu chiến, Sông Đông Êm Đềm là một trong những bộ tiểu thuyết rực rỡ nhất. Nó viết về khát vọng sống và đi tìm chân lý sống của những con người từng bị khói lửa chiến tranh làm lạc lối. Chàng trai Gregori rong ruổi trên hành trình đó mang theo tình yêu, sự đau khổ, mất mát đề nặng trên vai theo thời gian để tìm tới bến bờ bình yên nhất, dù cho sau cùng tất cả những gì còn lại dành cho anh chỉ là một tương lai. Nhưng tương lai đó nhỏ bé, xinh đẹp và đáng để anh nâng niu gìn giữ trong vòng tay và bảo vệ bằng cả mạng sống của mình.

Đó là một câu chuyện mà chưa bao giờ người ta nhận thức được sự rạch ròi giữa cái Đúng và cái Sai trong một con người hơn vậy.
5
455391
2015-05-25 00:30:01
--------------------------
172629
5235
Sông Đông êm đềm là một bộ sách tiểu thuyết hay nói về con người sau chiến tranh thế giới thứ hai.Cuộc thế chiến đã làm cho bao nhiêu số phận đau buồn trong số đó có hai nhân vật trong truyện.Đó là Xô cô lốp và bé Vania,Xô cô lốp là một người lính sau chiến tranh ông đã bị mất vợ và những đứa con,sau nhiều lần đi trở hàng thuê ông đã gặp bé Vania.Hai con người cùng chung một số phận gặp nhau và bé vania đã trở thành con nuôi của ông.Hai cha con ông luôn cuốn quýt bên nhau,bé Vania không chịu được thậm trí là khóc khi ông đi làm mà bé không được đi theo.
4
536745
2015-03-24 13:32:15
--------------------------
162182
5235
Bìa quá đẹp, mềm, nhẹ, bền và dễ dùng. Chất liệu giấy rất tốt và bền nhưng hơi mỏng, cỡ chũ hơi nhỏ ( mình thông cảm điểm này bởi cuốn sách có nội dung khá dài).
Nội dung xoay quanh cuộc đời nhân vật Gregori- một anh chàng nông dân có nhà bên cạnh sông Đông và đi lính theo nghĩa vụ quân sự của thời Nga hoàng.
Những mối tình đằm thắm của Axinhua, Aksinia,  Natalia nhưng vẫn không kém phần cay đắng và đẫm nước mắt. Tuy vậy Gregori đã mất hết ngưởi thân chỉ còn đứa con trai bé bỏng và tiếp tục cuộc đời bên sông Đông êm đềm.
Tái bút: Cảm ơn dịch giả Thụy Ứng với một cái hơi rất là Việt
5
525063
2015-03-01 18:38:17
--------------------------
