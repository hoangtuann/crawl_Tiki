279952
4708
Đây là tác phẩm thứ tư của Hồ Anh Thái mà mình được đọc. Một cây viết đa dạng và nhiều sáng tạo hiện nay. Quyển này kể một câu chuyện có vẻ nhẹ nhàng nhưng ẩn sâu nhiều tầng lớp nghĩa, làm mình hơi liên tưởng một số tác phẩm ngày xưa được học trích đoạn trong sách giáo khoa 11 & 12 như của Nguyễn Minh Châu chẳng hạn. Đọc xong cũng man mác thôi chứ tự luận được ra nhiều về ý nghĩa nữ quyền cho đến khi đọc bài phân tích cuối sách. Nên cho bài phân tích này 10/ 10 điểm haha. Gợi ý thêm cho một số bạn đọc trẻ nên tìm cuốn Dấn thân (Sheryl Sandberg) và phim Mad Max: Fury Road để làm thành combo nữ quyền cho nó nhiều màu sắc và hoành tráng. Tự kết luận về quyển sách này thì đây vẫn là một tác phẩm nên đọc nếu bạn quan tâm đến mảng đề tài này. Đọc để biết thì dễ nhưng để thấu hiểu thì là cả một quá trình.
3
166409
2015-08-27 11:56:32
--------------------------
