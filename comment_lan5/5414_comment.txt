260571
5414
Một ngày nọ chú bé người tham sự cuộc họp của các loài cây và muôn thú trong rừng. Các loài cây vì muốn cho con người thấy tác hại của việc chặt phá cây rừng, muốn cho con người một bài học nên đã không muốn vươn cành, đâm hoa kết trái, không muốn làm sạch mạch nước ngầm, không muốn bám rễ xuống đất nữa. Tất cả muôn thú đều cảm thấy lo sợ vì như thế muôn thú sẽ không còn sống và tồn tại nữa. Chứng kiến sự việc như thế nên chú bé người đã hiến kế sẽ sáng tác bài hát cho con người, để con người không tàn phá thiên nhiên nữa. Nội dung thật ý nghĩa với hình ảnh minh họa đẹp, sống động khiến quyển sách rất thú vị và bổ ích. Cám ơn tiki.
5
110777
2015-08-10 15:19:59
--------------------------
178354
5414
- Về mặt hình thức: Quyển sách được thiết kế khá dễ thương, thu hút người đọc. Các trang giấy được in màu trên giấy láng, cầm và sờ vào giấy rất thích. Màu sắc và hình vẽ rõ nét, dễ xem và dễ hiểu. 
- Về mặt nội dung: Quyển sách này đề cập đến việc con người tàn phá rừng, chặt cây, khai thác gỗ. Nội dung hướng đến dạy trẻ phải biết yêu quý rừng xanh, cỏ cây và động vật. Cuối sách còn có phần trò chơi nhỏ nhằm cho trẻ lược lại kiến thức và ghi nhớ sâu hơn.
4
561797
2015-04-04 21:32:24
--------------------------
