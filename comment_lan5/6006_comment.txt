441258
6006
Tôi hiện đang đọc cuốn này, cuốn tập hợp rất nhiều truyện ngắn. Giọng văn của tác giả rất đa dạng, lúc đọc câu chuyện đầu tiên, tôi thấy hơi "ngộp" vì miêu tả quá nhiều, cứ dựa vào cảnh vật được miêu tả mà đoán ra tâm tình của nhân vật, từ góc nhìn của nhân vật bao quát hết cảnh vật từ xa tới gần, khi nhân vật tiến một bước, gặp một thứ, nói chuyện với một người,... từng thứ từng thứ một đều được tác giả "khoác" lên thật khéo để che đi cái cảm xúc bên dưới.

Các mẫu chuyện sau tôi thấy mang tính trào phúng nhiều hơn, nghe có gì đó vừa hài hài vừa chua xót khi đọc, những con người lạc quan không đúng chỗ, sinh ra nhầm thời hay nhầm đất nước, đây là những điều con người ta không thể chọn lựa được cho mình. Nhưng tác giả cũng rất khéo léo khi che đậy những thứ đó bằng giọng văn "nửa đùa nửa thật" của mình, bằng những cái kết mở khiến người ta phải tự mà suy đoán theo ý mình chứ chẳng còn manh mối nào thêm nữa.
4
563407
2016-06-02 23:31:13
--------------------------
417510
6006
Đây là một tác giả từng đoạt được Nobel Văn học. Mình muốn tìm kiếm, sưu tập lại những cuốn sách mà các tác giả từng đoạt giải Nobel viết từng được xuất bản ở Việt Nam và cuốn sách này nằm trong số đó. Với cụ nhà văn người Đức Heinrich Böll và tác phẩm "Nàng Anna xanh xao", mình đã có vài nhầm lẫn. Mình vì tìm kiếm tác phẩm tác giả đoạt giải Nobel mà quên mất tìm hiểu về các tác phẩm. Đây chính là tập truyện ngắn của ông. Dưới dòng chữ ở bìa ngoài "Nàng Anna xanh xao" là dòng chữ và những truyện ngắn khác, nhỏ đến mức đáng thương. Thật là.
Dù sao thì đây cũng là một tập truyện ngắn hay, nhất là bạn nào hứng thú với văn học Đức, đặc biệt nền văn học nhắm đến sự khôi phục ngôn ngữ Đức sau khi khoa trương trong thời Quốc xã.
4
383240
2016-04-17 08:40:10
--------------------------
405140
6006
Điều đầu tiên muốn nói là cuốn sách này rất khó đọc, mất 2 tuần để đọc xong. Tôi mua cuốn sách này vì tình yêu dành cho nước Đức và con người Đức. Sau chiến tranh, nước Đức hoang tàn, sụp đổ; số phận của con người chênh vênh, chơi vơi với đời. Những mẩu truyện ngắn của nhà văn cho ta thấy được bối cảnh ấy của nước Đức, con người Đức qua cái nhìn của những cựu chiến binh, của những người con tha hương. Giọng văn rất ám ảnh và day dứt.
Sách này do Nhã Nam phát hành nên bìa đẹp, giấy tốt. Phần trình bày cũng rất ấn tượng. Một cuốn sách hay nên có mặt trong tủ sách nhà bạn :)
4
64755
2016-03-26 10:03:05
--------------------------
330928
6006
Quyển sách là tập hợp của nhiều truyện ngắn đặc sắc khác nhau với lối hành văn khá ân  tượng, độc đáo trong câu văn, hình ảnh,... Có thể thấy quyển sách viết về chủ yếu là những hệ lụy, hậu quả khôn lường mà chiến tranh đã để lại cho con người thời hậu chiến, đó là một mất mát to lớn và là một hậu quả đầy tàn nhẫn đối với con người sau khi trở lại cuộc sống thường nhật. Chính điều đó làm cho tôi liên tưởng đến các tác phẩm như Mảnh đất lắm người nhiều ma, Nỗi buồn chiến tranh,... cũng cùng đề tài như vậy. Nói chung đây là một quyển sách khá xúc động.
5
616731
2015-11-03 17:45:41
--------------------------
227827
6006
Sách có bìa đẹp, in ấn tốt, cắt xén cẩn thận, giấy thơm, dàn trang đơn giản.
Nội dung của sách nói về chiến tranh, những tưởng sẽ khô khan, khó hiểu và nặng nề. Nhưng ở đây lại khác. Tác giả xây dựng nhân vật khá chân thật. Thêm nữa là miêu tả bằng giọng văn giản dị, dễ hiểu. Đặc biệt là ông không trực diện lột tả nỗi đau, thay vào đó, dùng những câu chuyện giản đơn, bình thường, khéo léo nói lên nỗi niềm của nhân vật. Vừa giằng xé cũng vừa dễ chịu.
Nói tóm lại, đây là một quyển sách hay, cần đọc chậm, có thể đọc trước khi đi ngủ chẳng hạn.
4
30924
2015-07-14 15:28:17
--------------------------
220398
6006
Mình đã từng đọc qua nhiều nhiều sách viết về chiến tranh đếm không xuể, nhưng chiến tranh dưới ngòi bút của người đoạt giải Nobel văn học 1972, lại rất khác. Ông tiếp cận chiến tranh dưới một con mắt "bình dân", nhìn chiến tranh qua cuộc sống, hành vi, tính cách của từng cá nhân hay gia đình trong xã hội. Cả cuốn sách không hề nhắc gì đến sự khốc liệt dữ dội chết chóc của cuộc chiến, mà chỉ kể vê những con người bị chiến tranh hằn sâu lên cuộc sống, lên những lề thói của mình. Họ tìm kiếm những niềm an ủi cho cuộc sống tẻ nhạt thời hậu chiến, cố gắng thích nghi những nếp sống mới hoàn toàn thay đổi bởi chiến tranh. Từ đó ông khắc hoạ nên những cung bậc cảm xúc khác nhau của tình người, khi nồng ấm khi lạnh nhạt, vả chăng đó chính là hiện thực. Một quyển sách hay và hoàn toàn xứng đáng. 

4
120276
2015-07-02 15:50:38
--------------------------
210947
6006
Trước hết tôi nói về chất lượng quyển sách: rất tốt. Bìa đẹp,ấn tượng,giấy đẹp,không có lỗi chính tả( có thể tôi chưa phát hiên ra). Cuốn sách chỉ dày 235 trang,bao gồm 17 truyện ngắn. Có thể thấy mỗi câu truyện đều không dài lắm,tôi cũng không thực sự nắm bắt và lãnh hội hết được những ý nghĩa của các câu truyện. Cái cảm giác khi tôi mở những trang sách đầu tiên,nó tĩnh lặng,chậm rãi,đơn giản nhưng lại có gì đó rất huyễn hoặc. Nội dung bề nổi thì rất đơn giản,nhưng theo tôi nghĩ cần phải có những kiến thức hiểu biết cá nhân về tình hình nước Đức sau thế chiến 2 để liên hệ và hiểu được những ý nghĩa ẩn sâu trong những câu truyện ngắn đó. Cá nhân tôi có lẽ cần thêm thời gian lâu hơn để hiểu rõ cuốn sách này.
4
462189
2015-06-19 22:33:19
--------------------------
178960
6006
Thật sự mà nói mình không thích cái bìa này lắm, mặc dù chẳng tìm được điểm nào để chê, nhưng không hiểu sao cứ nhìn là mình lại có cảm giác hơi khó chịu, những câu chuyện trong quyển sách này cũng vậy, nó mang một cảm giác u uất và buồn bã thế nào ấy.  Điểm đáng khen là tác giả dùng khá nhiều hình tượng để nói lên nỗi đau của con người chứ không hẳn là đề cập trực tiếp, và nhờ vậy những câu chuyện mang một vẻ gì đó thơ hơn rất nhiều, nhưng mình vẫn không cảm thấy thoải mái lắm khi đọc xong.
3
476360
2015-04-06 09:31:00
--------------------------
162315
6006
Có nhiều truyện, mình đọc đi đọc lại vẫn không nắm bắt được cái sợi dây cốt lõi, nhưng nỗi u buồn, ảm đạm thì bao trùm không lẫn vào đâu được, kiểu như người ta không thấy nó, người ta muốn tránh nó nhưng vẫn cảm nhận được rõ ràng.
Bản chất, tâm tư con người là điều cực kỳ phức tạp, mỗi cá thể là mỗi sự phức tạp riêng, nhưng dường như tác giả đã hiểu hết, và lột tả được hết tâm tư của nhân vật của ông. Đây là điều rất khó, nhưng tác giả đã hết sức thành công.
Một cuốn sách đáng đọc :)
4
66322
2015-03-02 00:02:00
--------------------------
129704
6006
Qua lời giới thiệu thì hẳn mọi người hình dung những truyện ngắn trong cuốn sách này sẽ rất hoang toàn, mệt mỏi, bệ rạc, và toàn đề cập đến chiến tranh, mất mát. Sự thật không hẳn như thế. Heinrich Boll rất khéo léo trong việc chọn các góc nhìn khác nhau để tiếp cận vấn đề, lúc thì ông đề cập trực tiếp đến nỗi buồn của người lính trở về nhà sau mấy năm lang bạt, lúc ông lại vòng vo mượn chuyện của cái tách bị sứt quai mà nói, cũng có lúc ông kể những câu chuyện tưởng chừng chẳng liên quan về hồi tưởng của một vị vua đã rời bỏ vương quốc, một người đàn ông làm nghề vứt bỏ, hay một tiến sĩ thích sưu tầm sự im lặng dưới dạng những đoạn phim nhỏ. Ông kể tất cả những câu chuyện đó bằng một giọng văn sáng rõ, tỉnh ruồi và đặc biệt dí dỏm theo kiểu rất có duyên, có lẽ không đủ khiến bạn bật cười thành tiếng, nhưng cũng đủ tủm tỉm khen ngợi sự thâm thúy của ông trong lúc đọc. Truyện ngắn mình thích nhất là "Đêm thánh vô cùng", "Một câu chuyện lạc quan", "Bên cầu" hoặc là "Đi tìm độc giả" . Quả thật Heinrich Boll đã đánh vỡ định kiến của mình, trước đây hay cho rằng nhà văn Đức ai cũng khô khan, hàn lâm, khó hiểu, nhưng không phải vậy đâu nhé =))
4
38828
2014-10-12 09:11:36
--------------------------
