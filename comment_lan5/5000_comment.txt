467721
5000
Mình đã đặt cuốn sách này từ lâu, sau khi đọc cùng con mình đã mua them tặng cháu nhân ngày 1/6 vì thấy nó quá hay, quá cảm động và ý nghĩa.
ấn tượng đầu tiên là sách nhỏ  gọn, xinh xinh.  Nội dung mỗi câu chuyện rất giản dị, thẫm đẫm tình mẹ. nó không quá dài cũng không quá ngắn nên con mình 3 tuổi rất thích. Thậm chí còn thuộc nhanh nội dung từng câu chuyện về những người mẹ vĩ đại.
có nhiều đoạn rất cảm động, lấy nước mắt của người đọc về sự hy sinh bao la, cao cả của người mẹ giành cho con của mình. Những sự hy sinh thầm lặng ấy luôn hiện thấy trong cuộc sống của chúng  ta,.

5
850880
2016-07-04 10:58:59
--------------------------
181957
5000
Tôi đọc rất nhiều truyện, mỗi câu chuyện đều chứa đựng tình người thấm đẫm yêu thương, nhưng khiến tôi xúc động nhất vẫn là tình cảm mẹ con; nhất là sự hy sinh cao cả của người mẹ dành cho những đứa con của họ.
Truyện cổ tích về mẹ là tập hợp những bộ truyện ý nghĩa kể về những người mẹ vĩ đại, tình mẫu tử thân thương được thể hiện qua sự quan tâm dịu dàng qua những điều nhỏ nhặt, những lời khuyên can nhắc nhở con cái hay sự hy sinh âm thầm lặng lẽ. Càng đọc, tôi càng muốn khóc và muốn được bé lại để trở về vòng tay bao la của mẹ mình. :)
Một quyển sách hay và ý nghĩa, tôi sẽ tặng nó cho những ai cần đến.
5
382812
2015-04-12 21:26:40
--------------------------
