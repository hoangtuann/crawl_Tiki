295086
6520
Qúa hay là một đánh giá không thể bàn cải về vấn đề nội dung của tác phẩm. Cái chất lang bạt, phi thường và hào hùng luôn hiện hữu trong mỗi câu chuyện của từng anh hùng. Nhìn chung khi nhận quyển sách thì mình khá bất ngờ với kích thước đồ sộ và độ dày của tác phẩm. Sách có giấy đẹp tuy nhiên chữ lại nhỏ khó đọc và gây mỏi mắt nên góp ý cho nhà xuất bản. Ngoài ra hình ảnh tượng trưng nhỏ và giấy in hình khó đọc có khi còn đè lên hình.Còn lại thì tuyệt vời, các bạn có hứng thú về kiếm hiệp thì nên bỏ quyển sách này vào tủ sách của mình nhé.
4
254228
2015-09-09 23:03:25
--------------------------
291681
6520
Người dịch:		Á Nam Trần Tuấn Khải
NXB:			Thời Đại
Thể loại:		Tác phẩm kinh điển
Bìa:			Cứng, rất đẹp
Chất lượng in, giấy: Tốt, ảnh màu đẹp
Chất lượng dịch:	Tốt
Nhận xét:		Thủy Hử - Một trong tứ đại danh tác của Trung Quốc. Đây là bản in đặc biệt, có 7 trang màu với hình ảnh minh họa rất đẹp, sống động 108 vị anh hùng Lương Sơn Bạc. Điều này cũng rất tiện cho ai mới đọc Thủy Hử lần đầu có thể nhớ tên nhân vật và có đặc điểm gì về tài năng, nghề nghiệp hay vũ khí. Nếu bạn thích văn học Trung Quốc thì không thể không có tác phẩm này trong tủ sách gia đình.
Khuyên: 		Sưu tầm, nên đọc.

5
129879
2015-09-06 16:31:32
--------------------------
284653
6520
Đã từng đọc qua Thủy Hử, nhưng ấn phẩm lần này rất tốt.
Trình bày rất công phu, thêm nữa đây là bản dịch gốc nên lời dịch rất hay và hấp dẫn. 
Có phần trình bày tóm lược về 108 anh hùng rất thú vị. Vì quá nhiều nhân vật, nên phần tóm lược này rất giá trị.
Tất cả các chương được gom lại thành 1 cuốn nên khi cầm hơi nặng tay, thêm nữa sách đóng không được chắc chắn nên 1 thời gian chắc sẽ bung.
Cần bao lại cân thận để cất giữ cho sau này.
Nhìn chung đây là ấn phẩm rất đáng xem.
4
317294
2015-08-31 09:45:36
--------------------------
265686
6520
Đã từng tiếp xúc qua vài chương hồi trong truyện nhưng đây là lần đầu tiên tôi đọc toàn bộ Thủy Hử. Nội dung của tác phẩm thì không cần phải bàn cãi. Về hình thức thì giấy tốt, trình bày đẹp. Tuy nhiên cái bìa cứng này không làm tôi hài lòng vì cách thiết kế bên ngoài không được đẹp mắt. Một điểm đáng khen đó là tác phẩm có vẽ hình ảnh của 108 anh hùng Lương Sơn Bạc nhưng tiếc rằng vì chất lượng giấy chưa tốt nên chúng cứ dính vào nhau, khi gỡ ra thì hình ảnh bị chồng lên hoặc mất đi. Nếu nhà xuất bản điều chỉnh được các lỗi này thì tác phẩm xứng đáng được 5 sao
4
676173
2015-08-14 08:55:58
--------------------------
262833
6520
Sau bao nhiêu lần xem phim Thủy Hử phiên bản cũ cho đến Tân Thủy Hử tôi vẫn không sao xem được đoạn kết. Tôi quyết định mua quyển sách này để có thể đọc lại trong những lúc rảnh rồi. Khi nhận sách tôi rất ngạc nhiên vì quyển sách rất dày, bìa cứng, giấy tốt và trình bày rất đẹp. Phần đầu quyển sách tác giả đã có phần hình vẽ minh họa cho 108 anh hùng Lương Sơn, và ghi chú một số thông tin cơ bản của nhân vật. Sách hay, trình bày đẹp và giá cũng tương đối mềm nên sở hữu cho riêng mình quyển sách như vậy thì còn gì thi vị bằng.
5
326812
2015-08-12 09:58:13
--------------------------
227480
6520
Thủy Hử gắn liền với tuổi thơ nhiều bạn 8x bấy giờ và tôi cũng không phải ngoại lệ. Tôi đã xem cả Thủy Hử cũ và cả Tân Thủy Hử mới bây giờ. Tuy nhiên các bạn vẫn nên đọc thêm cả sách nữa. Là một trong tứ đại kỳ thư nên khỏi phải bàn nội dung nhé. Tôi xin đánh giá về hình thức : ở tác phẩm này chất luợng in tốt, các chữ không hề bị nhòe. Ngoài ra còn có in màu tượng trưng cho các nhân vật, rất bắt mắt. Cám ơn tiki đã giới thiệu đến đôc giả sản phẩm này.
4
581502
2015-07-14 11:33:11
--------------------------
195569
6520
Có thể nói đây là bản rẻ nhất mà sau nhiều lần cân nhắc mình đã quyết định rước em nó về. Bìa cứng dày và khá đẹp tương tự như các sản phẩm kinh điển khác của Trung Quốc, chất lượng giấy khá tốt, trắng và in rõ thêm vào đó còn có phần minh hoạ màu 108 anh hùng rất đẹp. Khích thước trang sách lớn, nếu đọc xong để trong tủ kính trưng bày như một bộ sưu tập sách cũng rất đẹp. Nói chung với một quyển sách có chất lượng giấy trình tốt, trình bày đẹp, dày đến hơn 1000 trang mà giá như vậy là quá rẻ. 
Nói đến thủy hử thì ít nhiều ai cũng có xem nhưng bộ phim liên quan đến các nhân vật của truyện hoặc xem phim dựa trên cả bộ truyện. Tuy nhiên khi bạn đọc sách thì bạn thậm chí còn thấy hấp dẫn hơn nhiều so với phim ảnh, tâm lý nhân vật, bối cảnh, các trận đánh rất sinh động mà phim ảnh khó có thể thay thế
5
504882
2015-05-14 01:06:47
--------------------------
162751
6520
Tác phẩm kinh điển nên mình k nhận xét về nội dung nha, mình chỉ nhận xét về hình thức để các bạn mua online dễ lựa chọn hơn thôi
Trên tiki có bán nhìu bản Thuỷ Hử nên khi mua mình cũng đã xem qua các nhận xét trước khi quyết định chọn bản này. Có thể nói đây là bản rẻ nhất nhưng được khen là hình thức đẹp nên mình mới chọn. Nhưng khi được giao hàng thì hơi thất vọng với chất lượng của sách. Bìa cứng dày và khá đẹp (theo phong cách cổ điển nha ^.^), chất lượng giấy cũng tốt, trắng và in rõ. Nhưng cách đóng sách thật sự không tốt, ở bìa dán hồ vào k cẩn thận để lem qua các phần khác, mình đã cố gắn nhẹ tay gỡ ra nhưng vẫn bị rách khá nhiều, bực nhất là phần minh hoạ màu 108 anh hùng bị dán đè lên, nhìn k được 1 số người >.
4
112978
2015-03-03 08:43:59
--------------------------
154884
6520
Mua hàng online đôi khi sự ngờ vực về sản phẩm là điều thường xuyên xảy ra, nên cần phải tìm hiểu kỹ càng về sản phẩm: như đọc những lời đánh giá, nhận xét của những người đã sử dụng. 
Tôi cũng là người đã sử dụng sản phẩm này, và có thể nói rằng: các bạn yên tâm về chất lượng về quyển sách này. Bìa, giấy được đóng rất chắc chắn, và bền. Mực in rõ ràng và và sáng. 
  Còn những ai yêu thích những tác phẩm tiểu thuyết kinh điển, những giá trị nhân văn, thì quyển sách này là một bảo vật đấy.
5
273545
2015-01-30 17:47:36
--------------------------
125003
6520
Một cuốn sách hay dù cho bìa không đẹp vẫn sẽ nhận được những lời khen. Cuốn sách này có cả hai yếu tố nội dung cuốn sách thì khôi phải bàn cãi, là một trong tứ đại danh tác sẽ mãi trường tồn với thời gian. Còn về vẻ đẹp bên ngoài rất bắt mắt nếu như ai đó dù chưa biết tác phẩm thủy hử như nào khi nhìn vào cuốn sách cũng sẽ rất thích.
Sản phẩm này theo tôi tiện dụng hơn vì cả tác phẩm được gộp lại trong một tập chứ không phải hai hoặc ba tập như các sản phẩm khác, mặc dù khi cầm khá nặng tay nhưng tôi rất thích cách làm này.
5
357262
2014-09-10 17:23:57
--------------------------
