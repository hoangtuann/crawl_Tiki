440281
5882
So với vụ ở tập 3 thì tập 4 thuyết phục hơn nhiều vì ân oán triều đình kết hợp với âm mưu giang hồ, yếu tố giả thần giả quỷ cũng thuyết phục hơn. Ở đây còn xuất hiện nhân vật mới là Bao Duyên, trở thành couple mới với bàng Dục, 2 thanh niên trẻ tuổi rất dễ thương! Có điều tình cảm trong bộ này quá sức chậm, đọc đến tận tập 4 mà mới chạm môi thôi, hơn nữa có cảm giác Nhĩ Nhã viết truyện kéo dài quá...  Ngoài ra Quỷ Hành Thiên hạ có yếu tố ma quái, nhưng khi lí giải sự thật là hiếm khi nào có yêu ma quỷ quái thật, nên cũng chẳng hiểu rốt cục câu chuyện này theo phương hướng nào... 
3
516261
2016-06-01 14:39:47
--------------------------
381674
5882
Truyện trinh thám của Nhĩ Nhã thì khỏi phải bàn, lôi cuốn, hấp dẫn mà cũng không kém phần ngọt ngào từ cặp đôi chính. Ai đọc nhiều tác phẩm của Nhĩ Nhã sẽ thấy, tình cảm của các cặp đôi tiến triển tương đối chậm, như Thử Miêu ở đây, tận 4 quyển mà vẫn chưa có cái hôn nào chính thức. Nhưng về căn bản, nội dung tình tiết không vì thì thế mà bị kém hấp dẫn, ví dụ điển hình lqf mình vẫn theo Quỷ Hành đến giờ. Về hình thức thì cũng tương đối tốt, bìa và bookmark rất đẹp, chỉ có điều giấy in hơi vàng. Điều khiến mình không hài lòng duy nhất là poster bị gập lại. Còn lại đều tốt.
4
287985
2016-02-17 21:17:38
--------------------------
372663
5882
Rút kinh nghiệm từ 3 tập đầu, thấy nữ quỷ xuất hiện đã biết kiểu gì cũng là người đóng giả :)) Mình luôn cho rằng tình cảm thì phải ào ào mãnh liệt mới đủ đậm sâu, nhưng Nhĩ Nhã đã cho mình một cái nhìn mới. Tập 4 rồi mà cặp thử miêu chỉ mới tạm gọi là thân mật, nhưng mình cảm nhận được tình cảm họ dành cho nhau qua từng cử chỉ vụn vặt, không phải từ lúc này mà ngay từ tập 1 (dù gì cũng có nguyên bộ Du long tùy nguyệt làm đệm rồi!), ít biểu hiện ra nhưng mình thấy tình cảm 2 người đã khắng khít tới mức một cọng lông cũng không thể chen lọt! =]]

Ngoài tình tiết vụ án li kì hấp dẫn, tập 4 còn hấp dẫn bởi sự "lên sàn" của tiểu màn thầu và tiểu tiểu bàn =)) dễ thương muốn ngất~! Giấy tập 3,4 đều vàng như nhau, chắc in chung đợt, mỏng, nhẹ, mực in rõ ràng, ít lỗi, quá tuyệt vời!
5
141394
2016-01-22 00:52:20
--------------------------
355756
5882
  Tập 4 này trong Quỷ hành hệ liệt làm mình vẫn rất hài lòng. Nội dung thì cái tính ly kỳ hấp dẫn của các tình tiết phá án vẫn không hề suy giảm. Truyện bà Nhã viết thì chắc tay khỏi cãi rồi. Cơ mà bả viết ghê thiệt, nhiều lúc đọc ban đêm mà mình thấy sợ cực, cứ như đang đọc truyện kinh dị đêm khuya ấy (nhát lắm (ﾉ◕ヮ◕)). Tập này xuất hiện con trai của bao đại nhân a! Phải nói là cậu Bao Duyên này xuất hiện là để Bàng Dục bớt cô đơn a. Hai người hợp nhau lắm (theo một cách nào đó), mấy đoạn đấu khẩu của hai người phải nói là dễ thương hết biết. Lại nhắc tới cái đôi mèo chuột nào đó, phải nói là hai người này bị thần rùa nhập sao? Sao lại còn lề mề để con dân sốt ruột thế này? May mà tập này có cái cảnh "cổ vịt" vớt vát, thỏa mãn được một tí.
  Về hình thức, giấy, in ấn, cắt xén đều rất ổn. Bìa với bookmark hình Triệu Phổ thì miễn chê, đẹp lung linh rồi. Cơ mà hơi khác tưởng tượng của mình tí, hồi đọc Du long cứ nghĩ anh Cửu phải là vai u bắp thịt hơn tí, mặc đồ cũng là đồ đen chứ  nhìn trong đây anh í có vẻ hơi thư sinh. Nhưng không sao, đặt bookmark hình anh bên cạnh Công Tôn mỹ nhơn thì vẫn hợp nhau như thường ^^
4
110321
2015-12-20 19:15:36
--------------------------
338152
5882
Phải nói là bộ này tập càng về sau càng hay càng thu hút nha. Bìa quyển nào quyển đó đẹp như mơ 
~ với bookmark cứng cứng đẹp đẹp ngắm mãi không chán. Tạp này thì xoay quanh Triệu Trinh – vua – là chủ yếu nhưng mà mấy cảnh ngọt ngào giữa hai bạn Thử Miêu đã tăng lên đáng kể và điều đó làm mình hạnh phúc lâng lâng qua mỗi trang thêm mấy cảnh của Công Tôn và Triệu Phổ và máu mũi mình đã lên láng. Nội dung tập này cũng hay, cũng có nhiều bí ẩn đọng lại tới cuối cùng gở hết mọi nút thắt một lượt nên làm mình không rời tay khỏi cuốn sách được.  Bạn nào đang phân vân có nên mua tiếp không thì mua đi vì mấy tập sắp tới phải nói là ngọt khắp nơi. <3 !

5
540461
2015-11-15 14:33:37
--------------------------
312580
5882
Phải dùng từ say để miêu tả cảm giác của tôi khi đọc Quỷ Hành Thiên Hạ - Tập 4: Quỷ Thôn Ngũ Phần của Tác giả Nhĩ Nhã. Say từ bìa sách, say từ bookmark, say từ bìa hộp đến hình minh họa. Ôi, Triển đại hiệp, một thân trác tuyệt nhưng cũng lãng mạn hào hoa lắm thay. Chuyện tình Ngọc Đường và Triển Chiêu đẹp như một bức tranh làm say đắm lòng người. Phải nói rằng Nhĩ Nhã chưa bao giờ làm thất vọng người đọc từ giọng văn cho đến nội dung, làm người đọc chìm đắm giữa những mưu đồ và cảm xúc tình yêu.
5
144957
2015-09-21 16:45:11
--------------------------
301223
5882
Có thể nói Nhĩ Nhã là tác giả tiêu biểu và điển hình trong thể loại truyện này. Nhĩ Nhã không hề làm thất vọng sự mong đợi của các độc giả của mình.Những tranh đấu trong cung đình,những tội ác mưu đồ giữa người với người. Đan xen đó là những màn lãng mạn không thể tả của cặp đôi Ngọc Đường và Triẻn Chiêu.Từ hình thức bìa vô cùng trau chuốt và đẹp đẽ cho đến nội dung ngôn từ đều hoàn hảo. Rất thích quyển này và đặc biệt là những poster tặng kèm.! Rất hài lòng 
4
520563
2015-09-14 15:40:59
--------------------------
299032
5882
Ôi Triệu Phổ ôi Triệu Phổ ôi Triệu Phổ. 
Quả không hổ danh là Hôi nhãn Tu La, khí chất bức người quá đi mất. 

Tập này thiên về những tranh đấu của cung đình, những tội ác tiền triều. Bạch Ngũ gia Bạch Ngọc Đường và Triển đại nhân Triển Chiêu vẫn cứ lúc nào cũng dính lấy nhau như hình với bóng. Qua tập này mà có thể hiểu thêm về Triệu Trinh, ông vua ta cứ ngỡ ở xa xa lắm, hóa ra cũng là người ở độ tuổi trẻ trung năng động như Triển Chiêu, Triệu Phổ thôi, sự khác biệt duy nhất là Triệu Trinh có quá nhiều gánh nặng!! 

Nói chung là yêu nhà Cú, sớm xuất bản tiếp nhé!!
5
457461
2015-09-12 23:22:36
--------------------------
294660
5882
Truyện của Nhã tỷ chưa bao giờ làm mình thất vọng. Lại là fan cuồng của Thử-Miêu nên chắc chắn không thể bỏ qua truyện này. Mới đó mà đã theo được tới tập 4. Nhớ ngày nào vẫn còn chờ đợi từng bản dịch online, giờ được cầm trên tay quyển truyện, cảm giác thật là tuyệt vời!
Bìa truyện rất đẹp, hộp sách cũng rất chắc chắn, còn được tặng thêm poster. Mình rất hài lòng với chất lượng truyện, người dịch dịch cũng rất ổn. Theo mình đây là một bộ truyện rất đáng mua cho bất kỳ ai là fan của Nhĩ Nhã
4
161984
2015-09-09 15:57:52
--------------------------
293859
5882
Tập này màu hộp rõ hơn tập trước, không còn bị nhạt màu nữa. Vấn đề bị lệch trang cũng hết, giấy tập này dày hơn tập trước, bookmark dày và rất đẹp, 
Về nội dung thì mình không còn gì để nói, chị Nhĩ Nhã luôn là số một trong tim mình. Trong tập này Miêu Miêu và Bạch Bạch đã có nụ hôn đầu tiên, thương thay nụ hôn này lại có hương vị của "cổ vịt cay chết mèo" =]] , nhưng sau cùng thì Mèo ta vẫn chưa chết =]]]. Nói cho cùng thì truyện rất hay, yêu Nhĩ Nhã, yêu Owlbook, yêu Tiki!
5
340377
2015-09-08 19:57:31
--------------------------
292997
5882
Bìa hộp đẹp, book mark đẹp, hình minh họa càng đẹp hơn nữa. Khiến một đứa cuồng truyện này như mình càng thêm cuồng hơn. 
Nội dung truyện thì khỏi phải nói, văn phong của Nhĩ Nhã không bao giờ làm mình thất vọng. Càng đọc càng ghiền, mặc dù đã có bản edit trên mạng nhưng vẫn chờ đợi ra sách để đọc. Cảm giác cầm trên tay cuốn truyện sướng không tả được. Đọc nhiều lần vẫn không thấy chán, Thử Miêu càng ngày càng tình cảm hơn.
Có một điểm không ưng ý lắm ở quyển này là không kiếm ra bookmark của Triệu phổ  
5
579339
2015-09-07 21:23:28
--------------------------
291038
5882
Có thể nói đây là một trong hững tập truyện cuốn hút nhất trong bộ Quỷ hành thiên hạ này. Cứ đọc tới những dòng miêu tả mộ phần là mình lại thót hết cả tim. Và những đoạn gian tình của cặp đôi Thử-Miêu cũng làm tim mình đập một cách bất thường không kém! :D. Vẫn mang đậm  rất nhiều "chất" của Nhĩ Nhã, câu chuyện trong tập này được giải quyết rất gọn gàng, dứt khoát nhưng vẫn rất uyển chuyển để là một phần trong mạch truyện lớn của cả bộ truyện và chắc chắn là không thể thiếu những tình huống hài hước, thú vị đến nổi chính bản thân mình đang đọc sách lại há miệng cười trong vô thức mà không biết!
Hình thức của sách  trong bộ truyện này hầu như không hề thay đổi mà rất đồng đều. Bìa đẹp, không rối mắt, giấy in tốt, lỗi chính tả hầu như không có. Nhưng nếu bìa hộp ngoài dày hơn một chút thì sẽ tốt hơn. Riêng phần poster thì rất rất rất mong Tiki áp dụng cho mọi loại sách kèm poster là hãy cuộn lại và giao trong ống đựng để tránh hỏng. Cảm ơn nhiều! ^^


5
123775
2015-09-05 23:23:34
--------------------------
254940
5882
Cầm quyển sách trên tay, một lần nữa máu mũi lại tuôn trào không cầm lại được =))) . Nội dụng thì khỏi bàn, Nhĩ Nhã chưa từng làm mình thất vọng. Nội dung hay, có cao trào. Nhân vật đáng yêu. Lối dẫn dắt của Nhĩ Nhã rất ổn. Về tập này thì tuy rằng là còn vài chỗ mực vẫn chưa đều nên làm mất chữ nhưng so với 3 tập trước thì tốt hơn rất nhiều, chữ không bị lệch khung nên mình không phải bẻ sách ra để đọc nữa. Nói chung rất ưng quyển này.
5
688030
2015-08-05 16:48:05
--------------------------
237280
5882
Nội dung thì hay khỏi bàn cãi, Nhĩ Nhã viết mà. Mang màu sắc ma quỷ nhưng không rùng rợn mà cực kích thích và hài hước, đến nỗi một đứa mê ngôn chính công như mình lại biết đến một tác giả chuyên dammei và bộ truyện này. :)) Hình thức thì khỏi bàn vì mình rất yên tâm với Owlbooks. Tuy nhiên, khi mua cuốn này mình chỉ thấy có chút đau đầu với mấy cái poster tặng kèm, mình nghĩ Tiki nên hỗ trợ cuộn tròn poster vì nếu gấp vuông, chúng dễ bị gãy và trông không được đẹp. :)
4
152579
2015-07-22 12:55:15
--------------------------
236797
5882
Hình thức: bìa rất đẹp, liếc qua liền biết đây là bạn Triệu Phổ hào khí ngút trời. Trang giấy có hai trang bị gấp nếp và một trang bị nhòe. Xót!!
Ở tập này, không biết tại sao của tôi lại không có bookmark TOT

Nội dung: tập 4 không kỳ quái như mấy tập trước. Hai nhân vật chính Thử Miêu vẫn vô cùng chậm chạp. Tiểu Tứ Tử do có việc để làm nên không chuyên tâm làm ông mai nữa. Nhiều chi tiết gây cười hơn. Một cặp sắp thành đôi và nối gót Phổ Sách, biến thái công x thư ngốc thụ.
Vụ án vẫn còn chừa đất diễn cho tập sau cho nên vẫn còn hai nút thắt chưa giải.
Từ từ mần và cười thôi~~
5
556750
2015-07-22 00:18:58
--------------------------
221019
5882
Mê văn phong của Nhĩ Nhã từ hồi đọc SCI, mình thấy nếu so với SCI thì Quỷ Hành có vẻ đỡ sợ hơn, nhưng mà vẫn hay, tập này so với 3 tập trước thì có vẻ nhẹ nhàng hơn một chút, dù là có "ma quỷ" đấy. Mình càng đọc càng bị bấn điên cuồng với couple Thử - Miêu, đáng yêu ngoài sức tưởng tượng luôn, tập này còn có cả Bàng Dục với Bao Duyên cũng chính thức "lên sàn" nữa. Tình tiết truyện vẫn hấp dẫn, thắt mở hợp lý, hành văn trau chuốt, đọc rất thích, hình thức truyện cũng ổn lắm.
5
393748
2015-07-03 11:44:43
--------------------------
215036
5882
Triệu Phổ lên bìa quá ngầu
HÌnh thức thì không có gì để bàn cải, nhìn là thích rồi. Về nội dung thì tập này có chủ đề về ma quỷ. Những khúc ma quỷ xuất hiện là lạnh người, gặp mình toàn đọc truyện từ 10h khuya tới gần sáng không hà, càng ám ảnh kinh khủng. Những chi tiết miêu tả về bóng ma trong truyện rất thật, làm mình nằm mơ cũng mơ thấy rồi thì một tuần liền bị ám ảnh. Túm lại là, đây là phần đáng sợ nhất trong hệ liệt này. Kết cấu nội dung khá vững chắc mặc dù đôi khi cũng vấp phải vài lỗi thiếu logic hoặc giải thích chưa tới, nhưng với cách hành văn xưa giờ của Nhĩ Nhã thì như này là quá tốt.
Mình vẫn đang đợi các tác phẩm khác của Nhã tỷ đây. Cú ơi ! Cố lên !
5
648841
2015-06-25 19:34:50
--------------------------
214598
5882
Tập này đến lượt Triểu Phổ lên bìa! nhìn rất bá đạo và ngầu!!
Tập này xuất hiện thêm hai nhân vật là "Bàng Dục và Bao Duyên" ôi bạn trẻ này dễ thương chết được hết lần này đến lần khác! còn "làn da" trắng của Bao Duyên nữa chứ cười đau cả bụng, nhưng cũng phải kể đến món ắn "vịt cay chết mèo" tội nghiệp Miêu Nhi của tuôi khi nào hít phải cũng hắt xì. tập này có nhiều tình tiết gây cười hơn mấy tập kia một chút! còn cả vị hoàng đế "Triệu Trinh" nữa chứ! cũng khá thích Triệu Trinh! tập này đa số nói về vấn đề hoàng cung triều đình nên cũng khá phức tạp!
Nhưng Thử Miêu của tuôi vẫn dễ thương quá trời quá đất à "đi điều tra mà cũng chọc ghẹo nhau" đáng yêu chết đi được ♥ <
♥ ♥ Thử Miêu ♥ ♥
5
612250
2015-06-25 10:33:01
--------------------------
214228
5882
Ở một khía cạnh nào đó, Tập 4 không có nhiều tình tiết thần bí như 3 tập trước, đọc thoải mái hơn, không còn phải nửa đêm tự có suy nghĩ rùng mình rồi đắp chăn kín mít nữa. Tuy nói như vậy nhưng các tình tiết của Nhĩ Nhã vẫn đan xen với nhau rất nhiều. Triệu Trinh của Quỷ Hành và Triệu Trinh của SCI làm mình thấy rất thú vị, Tiểu Tứ Tử càng ngày càng dễ thương, còn có Miêu Miêu mỗi lần bị cho ăn giấm vì nợ phong lưu của Bạch Bạch lại làm mình thỏa mãn chết đi được, có lẽ cũng vì bản thân mình thuộc dạng hàng hiếm là sủng công... Mặc dù tình cảm tiến triển dần dần, nhưng mình cảm thấy đoạn cảm động nhất của Thử-Miêu lại là khúc cùng cột tay nhau vào chung một miếng vải rồi bước đi trong khu làng bỏ hoang dưới lòng đất cơ. Ở tập này lại có cảm giác quay về như thuở ban sơ mất tiêu. À, còn có Tiểu Màn Thầu với Cua Nhỏ cũng xuất hiện rồi, cãi nhau chí chóe mà nhiều lúc cũng hợp nhau ghê nơi, cũng lo lắng cho nhau có khác gì Thử Miêu đâu, chỉ là chưa ai nhận ra được gì thôi. Tóm lại đã đọc đến Tập 4 rồi mình phải đặt hàng ngay mua Tập 5 để có thứ mà chuyển ngay lập tức đặng không bị ngắt mạch đấy, haha.
Thích cái bìa ngay từ lần đầu nhìn thấy, tức là khi còn chưa có bản dịch ở Việt Nam đâu, đúng nguyên tác bên Trung rồi, nên càng thỏa mãn...
5
5633
2015-06-24 18:14:57
--------------------------
199437
5882
Có cảm giác như đang đọc về một Phủ Khai Phong ở một thế giới song song nào đó. Khi mà các kẻ xấu đã "quay đầu là bờ". Đọc tập này, nhiều đoạn thật muốn lăn ra cười. "Con trai Bao đại nhân sao lại trắng"?....Rồi còn họa sát thân của Bàng Dục vì "cái cổ vịt"...rất may, hóa ra không phải là do cái cổ vịt thật, ha ha... Mà công nhận, cứ dính vào người của Khai Phong Phủ là y như rằng tự mình chuốc lấy rắc rối, dính phải họa sát thân lúc nào chẳng hay. Tập này còn ghi nhận vị vua cao thâm khó dò Triệu Trinh, thật đáng là vua............Đọc tiếp, đọc tiếp thôi!
4
291719
2015-05-22 11:16:27
--------------------------
196733
5882
Truyện ẩn khuất từ đầu đến cuối , từ nút thắt này đến nút thắt khác lôi kéo mình đến gần cuối bộ truyện rồi mọi việc dần sáng tỏ . Chưa nói đến cách thể hiện tình cảm của Thử Miêu rất hay , mình không biết phải diễn đạt như thế nào , nhưng vừa đọc xong hết Tập 4 là chạy ngay lên đây để viết cảm nhận . Hai người không hề thể hiện tình cảm ra bên ngoài, chỉ đơn giản là những lúc quan tâm nhau và hiểu nhau "quá mức" cũng đủ làm cho người đọc cảm thấy tình yêu đâu nhất thiết phải nói ra mới là yêu
5
372852
2015-05-16 13:22:07
--------------------------
179494
5882
Vụ án thứ tư này không tập trung vào địa phương hay giang hồ nữa mà tập trung vào những bí mật của triều đình. Cuối cùng Bao Duyên và Bàng Dục cũng "lên sàn", khiến phủ Khai Phong càng thêm đông vui. Cặp đôi Cua nhỏ Màn thầu   hứa hẹn là một couple đầy triển vọng. Mình còn ấn tượng cả về Triệu Trinh - vị hoàng đế bề ngoài hiền lành mà bên trong thâm sâu khó lường. Là đam nhưng tác giả đã xây dựng tình cảm Triệu Trinh - Bàng phi rất sâu đậm, không kém gì các cặp đôi nam nam.
5
163552
2015-04-07 13:28:16
--------------------------
174567
5882
Về nội dung: Câu chuyện diễn biến có vẻ dài dòng và chậm nhiệt. Mình có cảm giác 2 nhân vật chính thích hợp làm bạn bè hơn là tình nhân :( Quá nhiều tình tiết và nhân vật mới xuất hiện, mình đọc mà cảm thấy loạn cả lên @@

Về chất lượng sách: Bìa đẹp, poster lung linh ( nhưng gấp lại làm poster có vết hằn, không thích :( ). Bookmark cứng, chất lượng, nhưng mà ở quyển này, lúc lật ra lại không thấy bookmark ở đâu cả, mong rằng đợt sau trước khi giao sáck, Tiki nhớ kiểm tra kĩ Bookmark và Poster tặng kèm trong sản phẩm TwT
3
544806
2015-03-28 12:37:14
--------------------------
160243
5882
Mình rất thích hoàng đế sắc bén như Triệu Trinh, khá cưng vị hoàng đế này. Quyển này vụ án cũng khá hay. Cuối cùng bạn Tiểu Màn Thầu đã xuất hiện. Dễ thương quá! Phụ tử họ Bao đúng thật là làm người ta giật mình. Màu da quá khác biệt.... Tiểu Bàng Giải thật đáng yêu, thật không ngờ anh là con tiểu Bàng giải quậy phá trong Du Long Tùy Nguyệt cơ chứ? Tình tiết hấp dẫn lôi cuốn, đọc rồi là không dứt ra được. Thật sự hâm mộ Nhã tỷ. 
Bìa sách rất đẹp, Triệu Phổ lưu manh lòng mình! Bookmark rất dày, của Owlbooks thì khỏi chê rồi!
5
449762
2015-02-23 11:18:12
--------------------------
158577
5882
Quỷ hành cứ đáng yêu như vậy làm em không thể sống được rồi T^T tập này cũng rất dễ thương. Bạn Tiểu Màn Thầu đã xuất hiện rồi, gì mà trắng thế nhỉ? Trắng tới nổi ai cũng nói :v tội bạn nhỏ quá đi =)) Mà Tiểu Bàng Giải cũng rất đáng yêu nha, con mều Triển Chiêu nữa, không ăn được đồ cay mà cứ đòi ăn cổ vịt :3 nhưng mà mình không thích tập này cho mấy, chẳng biết sao kì thế nhỉ?  Nói chung cũng ổn rồi, Quỷ hành là một trong những bộ hay nhất đấy.
5
122285
2015-02-13 11:07:59
--------------------------
137218
5882
Tập 4 Quỷ Hành Thiên Hạ lại mở ra một vụ án mới về Thôn 5 mộ, phải nói là mỗi tập truyện lại là những vụ án hết sức li kì và bí ẩn. Những chuyện kì quái liên tiếp xảy ra, những tưởng là có một thế lực siêu nhiên nào đó điều khiển nhưng cuối cùng tất cả là do bàn tay con người sắp đặt cả. Tình cảm của bạn Triển Và bạn Bạch tập này vẫn chưa có gì tiến triển cả, nhưng mình nhớ hình như tập 5 hay 6 gì đó, hai bạn chính thức xác lập quan hệ, vậy nên càng hóng các tập tiếp theo hơn :))
Một việc nữa là tập này Cú có tặng 2 poster siêu long lanh và chất giấy thì siêu tốt, cứ tiếp tục như vậy thì mình sẽ ủng hộ dài dài.
5
359192
2014-11-25 10:31:33
--------------------------
134372
5882
Đầu tiên xét về hình thức : Mình rất thích các cuốn đam mỹ do Owlbooks phát hành, giấy lúc nào cũng đẹp, poster long lanh, bookmark thì khỏi chê rồi và truyện lại được dịch bởi một dịch giả mà mình yêu thích nữa, mượt khỏi chê. 
Về nội dung : Phải nói là các vụ án của Nhĩ Nhã tỷ tỷ lúc nào cũng lôi cuốn, trong tập này xuất hiện 2 nhân vật mới là Bao Duyên và Bàng Dục :)) Cái màn thầu và con cua nhỏ này thiệt cưng hết biết, đi đâu cũng dính lấy nhau :)) Còn về couple chính trong truyện này- Thử Miêu thì .... có tiến triển :)) cơ mà hai bạn vẫn chưa có cái hôn này " chính thức" chủ động từ hai phía hết vậy, toàn nhờ xúc tác bên ngoài là sao. Mong chờ tập 5 xem hai bạn có tiến triển gì thêm không.
5
434144
2014-11-09 11:35:29
--------------------------
132185
5882
Cuối cùng cũng rước được em này về nhà òy ~ dzui quá trời luôn!!!!!!! Poster của Công Tôn và Triệu Phổ đẹp long lanh hạt chanh, nội dung truyện hay tuyệt vời ông mặt trời! Nhưng mà đọc đến tập này lại thấy chuyện tình Miêu Miêu với Bạch Bạch có gì đó không ổn. Hai người này giống với hai tương hữu sống chết hoạn nạn có nhau hơn là tình nhân. Coi đến tập này hai người cũng chỉ hun hun một chút. Thật sự là không khơi được chút hứng thú nào của hủ hết. Tiếp nữa, nói cuốn này là phiêu lưu trinh thám cũng chả sai vì tình cảm nhân vật dù được nói đến nhưng rất mờ nhạt. Nếu tiki cùng mấy lời bình không nói đây là đam mỹ chắc ta cứ tưởng này là truyện tranh trinh thám kiếm hiệp phiêu lưu Mao Thử đồng nhân quá.
4
196716
2014-10-30 09:50:50
--------------------------
