471187
5620
Mình đọc đi đọc lại bộ truyện Doraemon cả vài chục lần rồi mà không thấy chán,có lẽ vì Doraemon chiếm được tình cảm của mình và độc giả ngay từ lần đầu.Không hổ danh là một bộ truyện tranh huyền thoại của Nhật Bản.Lời thoại dí dỏm,nét vẽ không mấy cầu kì,nội dung của từng tập truyện rất ngộ nghĩnh và hài hước và đặc biệt hình ảnh chú mèo máy đến từ tương lai siêu đáng yêu là những lí do bộ truyện Doraemon này lại hấp dẫn người được ngay từ lần đầu tiên.Nói chung là truyện rất hay,rất tuyệt vời,cảm ơn Tiki
5
1352835
2016-07-08 11:48:23
--------------------------
395044
5620
"Doraemon" – là một bộ truyện tranh rất hay của tác giả người Nhật Bản Fujio Fujiko mà mình rất thích. Cuốn truyện có nội dung xoay quanh cuộc sống hàng ngày của cậu bé Nobita – một cậu bé lười biếng, thích nằm lười cả ngày và ... ngủ. Nobita có một nhóm bạn rất dễ thương gồm Xuka, Xê-ko, Chaien và chú mèo máy thông minh đến từ tương lai – Doraemon. Nhóm bạn này đã cùng nhau phiêu lưu đến những vùng đất xa lạ. Qua những cuộc phiêu lưu đó, ta rút ra được rất nhiều điều bổ ích như: lòng dũng cảm, tình yêu thương và tình bạn cao đẹp. Một bộ truyện tranh bổ ích!
5
1142307
2016-03-11 12:19:23
--------------------------
337759
5620
Đọc đi đọc lại mà mình vẫn thấy hay, vẫn thấy đặc sắc, vẫn thấy thú vị, và vẫn buồn cười đến đứt cả ruột. Haha. Đây đúng là một kiệt tác của nền truyện tranh Nhật Bản. Nhiều lúc mình đã ước gì có thể gia nhập vào thế giới của Doraemon, ước gì mình cũng có thể trở thành một người bạn thân, bạn tốt với Jaian, Suneo, Shizuka, Nobita và cả Doraemon nữa. Dù biết cái ước mơ đó của mình thật ngốc xít biết mấy nhưng nếu được như vậy thì thật là tốt. Theo mình đây là một cuốn truyện rất hay và phù hợp cho những em nhỏ.
5
932201
2015-11-14 17:48:00
--------------------------
214394
5620
Đây có lẽ là một bộ truyện huyền thoại của lứa tuổi thiếu nhi. Tôi nhớ lần đầu tiên tôi xem là khi tôi còn rất bé, hình như lúc ấy chỉ mới học lớp 2, hay 3 gì đấy. Lứa tuổi ấy thích xem truyện tranh là điều bình thường mà! Tôi vẫn còn nhớ, khi ấy tôi xem chỉ là xem vui, chỉ nhìn hình là chủ yếu chứ không đọc chữ nhiều, cũng như không thể hiểu được nhiều ý nghĩa của truyện. Đến khi đã lớn, tôi mới chợt nhận ra một điều là, Doraemon không đơn giản chỉ là một câu chuyện dành cho thiếu nhi, ẩn sau trong đó là nhiều bài học rất quý giá, về tình bạn, tình cảm giad đình. Trong Doraemon, chúng ta cũng có thể bắt gặp rất nhiều kiến thức hay mà tác giả đã để vào truyện. Nói chung, bộ truyện này thật sự rất tuyệt.
5
13723
2015-06-24 21:52:31
--------------------------
