257130
4313
Mình mua sách này tặng mẹ mình và mẹ chồng, mỗi người một quyển. Mẹ mình đọc xong khen sách hay nên mình mượn lại đọc.
Phải công nhận là sách rất hay!!!
Phần nội dung được tác giả phân chia và trình bày rất dễ hiểu. Nhờ có cuốn sách này mà mình có thêm kiến thức về chăm sóc sức khỏe hàng ngày, đặc biệt là dùng thuốc. Từ trước tới nay khi bị cảm cúm, nhức đầu, hay ho sốt... mình toàn tự ra nhà thuốc mua về uống. 
Nhà xuất bản cũng sử dụng chất liệu giấy tốt để in nên cầm cuốn sách mình thấy rất nhẹ. Một điểm cộng tuyệt đối tăng thêm cảm hứng cho người đọc.
Hơn nữa Tiki bán giá cũng rất phải chăng. Mua được cuốn sách vừa hữu ích vừa rẻ thì còn gì bằng.. Mình cảm ơn Tiki nhé
5
512830
2015-08-07 14:11:30
--------------------------
255140
4313
Ban đầu mình định mua sách này cho mẹ đọc, vì mình chẳng mấy khi động đến sách về sức khỏe và khoa học. Tuy nhiên, cầm lên đọc thử vài trang thì thấy tác giả viết rất dễ hiểu mà vẫn chuẩn xác. Đọc để thấy các thói quen sử dụng thuốc của mình từ lâu nay hình như là có vấn đề hết cả. Tuy nhiên, phần viết về Thực phẩm chức năng thì tác giả không đề cập sâu tới mà phần đó lại là cái mình có hứng thú để tìm hiểu. 
Nói chung sách đáng đọc vì sức khỏe của mình.
4
16522
2015-08-05 19:51:08
--------------------------
250214
4313
Sách này mình có kiếm vài nhà sách gần nhà nhưng không có mà không có thời gian đi xa kiếm nên vô tình thấy có trên tiki nay mua được rẻ ở tiki nên mình rất vui. Mình hay tìm đọc sách về sức khoẻ thấy cuốn sách này rất hay, có ích cho gia đình nhỏ của mình. Tiki giao hàng nhanh đêm trước mình đặt mua thì trưa 2 giờ đã có, tuy trưa hơi nắng nhưng nhân viên rất lịch sự. Có điểm nhỏ là cuốn sách bị rách một tí dù sách rất mới. Cám ơn tiki.
4
728931
2015-08-01 11:40:38
--------------------------
