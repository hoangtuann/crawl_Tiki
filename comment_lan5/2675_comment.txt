570665
2675
Câu truyện vừa nhẹ nhàng, cảm động vừa buồn. Cứ ngỡ hoàng tử sớm muộn cũng sẽ đến với công chúa, vậy mà công chúa lại ở lại một mình, buồn biết bao! Chất lượng sách cũng rất tốt, từ trang giấy đến dịch vụ đều tốt. William Traynor, người ta tưởng a đã tìm thấy mục đích sống của mình, vậy mà a vẫn quyết định chọn cái kết buồn. Có rất nhiều điều tiếc nuối về kết truyện. Mong các nxb sớm dịch phần sau " After You " của Me Before You
5
855698
2017-04-11 21:22:28
--------------------------
519017
2675
Cuốn tiểu thuyết tình cảm thứ 2 đọc một cách liên tục và say mê sau cuốn The Thorn Bird. Không biết viết cảm nhận sao nữa, nhưng thực sự là rất hay và ý nghĩa, ngoại trừ cái kết buồn, nếu mình là Will, có lẽ mình cũng sẽ có ước muốn giống anh ấy :)

"Clack, em là điều duy nhất khiến anh muốn tỉnh dậy vào mỗi sớm mai."
"Anh đưa tiền cho em không phải vì anh muốn em biết ơn, hay nhớ nhung, hay hoài niệm gì hết. Anh đưa cho em vì nó chẳng còn làm anh hạnh phúc được chút nào nữa, nhưng với em thì có."
"Hãy sống."

Người ta chỉ sống một cuộc đời mà thôi, hãy làm cho nó trở nên thật ý nghĩa.
4
282448
2017-02-04 20:40:04
--------------------------
509393
2675
Sách mua tặng nên ko review nội dung. Hình thức đẹp, bìa đẹp giấy đẹp, khổ nhỏ dễ thương. Tiki gói hàng cẩn thận :')))
5
918271
2017-01-12 12:39:02
--------------------------
495351
2675
"Trước khi gặp cô ấy, hai năm qua anh là một người vô dụng, đánh mất cuộc đời xưa cũ sau một tai nạn giao thông, gắn chặt thân xác trên một chiếc xe lăn, đau đớn cả về thể xác lẫn tâm hồn đã dồn anh đến sự lựa chọn tự vẫn. Trước khi gặp anh, cô ấy là cô gái tự ti, mặc cảm với bản thân, lạc trong góc khuất của cuộc đời, gánh trên vai trách nhiệm với gia đình, từ bỏ cơ hội và thu hẹp thế giới của mình lại trong một thị trấn nhỏ mà cả hai người đều gọi là quê hương"
Ban đầu, mình thực sự rất giận anh đã không vì cô ấy mà từ bỏ quyết định của mình, nhưng sau mình mới hiểu ra, vì cô nên anh càng quyết tâm thực hiện quyết định của mình hơn. Vì anh không muốn cô mất đi cơ hội mà người khác có thể cho cô.
Một kết thúc buồn, nhưng thật sự là không hề hụt hẫng.
Thật sự gần như toàn bộ tủ sách của mình đều đặt mua ở Tiki hết, phải gọi là dịch  vụ giao hàng miễn chê luôn, mình mới đặt tối thứ 6 thôi mà sáng thứ 2 đã có sách rồi
5
1130480
2016-12-12 12:55:33
--------------------------
485389
2675
Bản thân mình là một đứa con gái không thích đọc những câu chuyện buồn vì nó khá gây những kí ức sâu về sau . Nhưng câu chuyện này thực sự rất hay , buồn nhưng rất có chiều sâu không mơ mộng viển vông như các câu chuyện tuổi teen mà rất thực tế ! Câu chuyện chứa đựng được nhưng ý nghĩa mà tác giả muốn truyền tải . Đây là một món quà rất tuyệt cho những ngày mưa bên cạnh là một cốc ca cao nóng hổi ! Nếu bạn đã tìm thấy cuốn sách này thì nên mua để đọc 
3
738271
2016-10-02 01:24:44
--------------------------
456018
2675
Vì xem trailer phim nên mình biết đến cuốn tiểu thuyết này, sau đó mình đi tìm ở rất nhiều nơi nhưng không có sách nên đành đọc online vậy.Thế rồi thành ra đọc, ôm cái điện thoại và khóc, khóc rưng rức  vì cái kết sao buồn quá.Nhưng mình hiểu được sự bất lực, bế tắc mơi Will Trailor.Cảm thấy thương Louisa kinh khủng, sau bao nhiêu cố gắng thì quyết định của Will vẫn là đến Thụy Sỹ.Không hiểu sao mình luôn nghĩ bở cái kết buồn nên làm cho cuốn tiểu thuyết mới thật sự ý nghĩa.Đó là 1 câu chuyện lãng mạn về tình yêu, nhưng hơn thế đó là câu chuyện về tình người, tình thân và đặc biệt là cuộc sống.Hãy chọn một cuộc đời ý nghĩa, sống hết mình để không phải hối tiếc cho cuộc đời mình đã sống và hãy nói yêu thương khi còn có  thể.
Thật may là nhà xuất bản trẻ tái bản sách rồi, giấy xốp Phần Lan, bìa sách cũng dễ thương lắm ^^
 
5
1157982
2016-06-23 01:39:06
--------------------------
451180
2675
Sau khi đọc xong cuốn sách này nó khiến tôi phải suy nghĩ rất nhiều, tôi đặt giả thiết rằng nếu tôi là Will tôi có kết thúc cuộc đời mình như vậy không? Chắc chắn sẽ có những người nghĩ rằng anh ấy yếu đuối hoặc biết bao nhiêu người giống anh ấy nhưng vẫn sống bình thường đấy thôi ,sao lại chọn kết cục như vậy. Nhưng nếu như bạn không nằm trong hoàn cảnh như Will sao bạn hiểu được anh ấy phải chịu đựng như thế nào? Từ một ngưởi năng động ,phiêu lưu ,hay tham gia những chuyến du lịch đầy mạo hiểm nhưng chỉ trong một khoảnh khắc ngắn ngủi , anh ấy đã bị tước đi hết quyền lựa chọn và phải ngồi trên chiếc xe lăn suốt đời, bị bệnh tật hành hạ, sống nhờ vào thuốc ,không thể làm việc gì mà không có người giúp, tôi cảm thấy anh ấy lựa chọn rời bỏ cuộc đời này cũng là một quyết định tốt( mặc dù lúc đầu tôi cực kỳ tức về quyết định của anh ấy)  khi cả gia đình của anh ấy nhờ quyết định này cả anh ấy và họ đều như được giải thoát. Còn Lou nhờ có cuộc gặp gỡ định mệnh này Will đã thay đổi Lou biến cô ấy trở nên xinh đẹp ,có gu thẩm mỹ hơn, có ước mơ, có can đảm bước chân ra thế giới bên ngoài chứ không chỉ quanh quẩn nơi thị trấn nhỏ bé cô ở. Cả hai người đã có những khoảnh khắc mà suốt đời họ không thể nào quên được, hơn hết cuốn sách này vừa lãng mạn vừa đủ thực tế để nói lên rằng : cổ tích là không có thật.
5
313953
2016-06-19 14:50:46
--------------------------
448774
2675
Sau khi đọc song tác phẩm,tôi đã hiểu ra rằng tình yêu nếu càng níu kéo càng dễ tan vỡ và làm mất giá trị bản thân .Còn nếu để tự nhiên,thì đó là một tình yêu rất đáng quý để chân trọng . Tác giả có văn phong viết truyện rất cuốn hút và hấp dẫn người đọc,nhiều tình tiết hay và lạ mắt.Cốt chuyện miêu tả chân thực với cuộc sống của những người đang yêu nhau,tìm hiểu nhau,muốn ở bên nhau mãi không rời xa.
'''Trước ngày em đến'''' Anh là người ham chơi,không biết trời đất là gì,xã hội thay đổi như thế nào,ngoài đường có bao nhiêu cô gái đẹp đến tuổi cặp kê.
'''Trước ngày em đến''' Em là một cô gái nhỏ bé,có tính cách con lít,rất khó chiều và dậy dỗ,em ương ngạnh,ghê gớm,bướng bỉnh,đanh đá và còn hay chơi phô nữa.
Và khi gặp em đã làm cho anh bao phen trao đảo trái tim,ngày đêm mất ngủ,nỗi nhớ dâng trào,với những nụ hôn ngọt nào đê mê đầu lưỡi.

5
768145
2016-06-16 14:59:54
--------------------------
445114
2675
Một lời khuyên là bạn hãy đọc truyện trước khi xem phim vì truyện có thể truyền tải những thứ mà phim không thể làm được.
Điển hình là mình bị thất vọng với chị nữ chính trong phim, nhân vật Lou hiện lên như kiểu ngô nghê, ngốc ngếch chứ không như mình cảm nhận trong truyện là cô gái rụt rè nhưng thông minh. Chẳng hiểu do kịch bản hay tại chị ấy diễn nữa. Chắc kì vọng quá nên thành ra vậy.
Dù gì mình vẫn trung thành với nguyên tác. Truyện hay rất hay. Cái kết khiến người ta sẽ nhớ đến cuốn sách mãi.
5
80326
2016-06-09 15:13:31
--------------------------
442447
2675
Có lẽ tình yêu tuyệt vời ở chỗ nó không biến ta thành con người khác mà làm ta tốt đẹp hơn theo từng giây cảm nhận nó. Lou từ cô gái không mục đích sống mà đã tìm ra giá trị cuộc sống. Will từ mặc cảm đau khổ trở nên lạc quan, tràn đầy hạnh phúc. Tuy vậy, họ vẫn là họ. Will vẫn chọn cái chết vì nó là tốt nhất cho tất cả. Yêu là cho nhau quyền lựa chọn. Tôn trọng sự lựa chon của will cũng là cách lou yêu anh. Mình rất hài lòng với kết truyện, hài lòng với cách Will yêu Lou
4
1408269
2016-06-05 00:03:53
--------------------------
439177
2675
Mình chưa được dịp đọc cuốn sách này nhưng đây là bộ phim đầu tiên mà mình coi trailer đến 10 lần, mỗi lần xem là cảm thấy rất xúc động mặc dù nó chỉ là những cảnh chắp vá. Khi đọc các comment của các bạn về cuốn sách này thì mình lại cảm thấy hết sức tò mò, cuốn sách đã chinh phục một trong những nhà khó tính trong văn học, và yêu cầu phải chuẩn bị khăn giấy thật nhiều trước khi đọc. Những điều mà các bạn nói về cô gái Louisa khá giống với mình : tự ti, đôi lúc chấp nhận hiện tại nhưng đôi lúc cũng điên cuồng muốn thay đổi nhưng không biết thay đổi cái gì, hết sức quê, không nghĩ mình có thể làm được gì, chỉ khác một điểm là Louisa từ bỏ tình yêu 7 năm còn mình thì bị tình yêu đấy rời xa. 
Cảm ơn các lời bình thật hay, mình sẽ tìm mua cuốn sách này với hy vọng nó giúp mình nghĩ thoáng hơn như các bạn nói ... Tình yêu không phải là tất cả.
5
582436
2016-05-30 22:14:14
--------------------------
437963
2675
Anh quá lí trí, quá thông minh, có nhiều khát khao và trải nghiệm. Cô giống như món quà tuyệt vời nhất đến với anh, nhưng nó lại không đủ để định hình lại anh. Anh giống như một thế giới mới của cô, anh đem tới cho cô ước mơ và khát khao, giống như cô nói, nếu không có cái xe lăn có lẽ không bao giờ anh tới gần cô. Chinh phục anh, đó có lẽ là thành tựu lớn nhất của cuộc đời cô. 
Mình thích cách bọn họ hướng về nhau, đó là cố gắng đem lại cho đối phương thứ tốt nhất có thể. Anh quan sát cô, tìm ra những điểm mạnh của cô, thôi thúc cô hướng về cuộc sống tốt đẹp mà chính cô chưa bao giờ nghĩ tới. Cô quan tâm tới anh, quan tâm tới từng chi tiết, từng cảm xúc nhỏ của anh, vì anh mà dám can đảm vượt qua giới hạn bản thân biết tới.
Kết thúc là thực tế, không thể thay đổi. Chúng ta được nhìn thấy tình yêu và sự nỗ lực, cách sống... 
Quả thực chúng ta chỉ có một cuộc đời, hay sống xứng đáng với nó!
5
893012
2016-05-29 07:41:40
--------------------------
435417
2675
Sau khi xem xog trailer mình đã tìm và mua ngay cuốn này trên tiki. Chỉ ước ao tác giả có luôn phần 2 rồi.haha. Mình thích đối thoại giữa Will và Lou, hài hước, lắt léo, thích cái cách ra vẻ ông chủ của Will, nét vô tư của Lou và tình cảm tự nhiên của họ. Câu chuyện này chân thực đến kì lạ và đối với mình kết cục của câu chuyện là quá tuyệt vời. Cảm ơn Jojo đã cho tôi một câu chuyện để tôi thỏa sức tưởng tượng nhân vật yêu thích của mình đằng sau câu chuyện đó, tưởng tượng về Lou sau khi Will ra đi :*
5
329000
2016-05-25 03:40:24
--------------------------
427198
2675
Tên tác phẩm là " Me before you" như thật ra phải là " Me after you" mới đúng. Việc Lou bước vào cuộc đời Will, dù chỉ trong vòng 6 tháng nhưng đã làm thay đổi hoàn toàn cuộc đời của cả hai. Will khích lệ Lou khám phá những điều Lou chưa từng nghĩ, đến những nơi chưa từng tới, tìm thấy bản thân mình để có thể sống như cách Will đã sống trước ngày xảy ra tai nạn. Cuộc sống của Will từ khi có Lou đã trở nên tốt đẹp hơn nhưng vẫn không phải là cuộc đời Will mong muốn vì trong đó Will chẳng thể quyết định điều gì, phải phụ thuộc hoàn toàn vào người khác. Cái kết có thể đoán trước được nhưng ta vẫn mong Lou có thể thay đổi quyết định của Will bằng chính tình yêu của mình để níu anh lại với cuộc sống này nhưng rồi chợt nhận ra như vậy vẫn chưa đủ vì cuộc sống của Will sau tai nạn không phải là điều anh muốn và anh cũng không thể khá hơn được nữa. Tác phẩm buồn nhưng không quá bị thương bởi những đối đáp thẳng thắn và hài hước của Will và Lou.  Đây có thể chính là điều làm cho tác phẩm này trở nên đặc biệt hơn.
5
107307
2016-05-08 16:45:00
--------------------------
426269
2675
Đợi lâu quá mà không thấy Tiki về hàng nên mình đã ra nhà sách và mua quyển này luôn, thật sự cảm thấy không hề hối hận chút nào. Tên truyện đã làm mình vẽ ra khá nhiều suy nghĩ và vẫn mong chờ một kết thúc vui vẻ. Tuy nhiên đúng là không thể đơn giản như vậy. Xuyên suốt truyện là tình cảm rất nhẹ nhàng nhưng vô cùng chân thực giữa 2 nhân vật, không phải say mê, không phải đắm chìm trong tình yêu. Họ không chỉ yêu, mà là thông cảm, bao dung, thấu hiểu. 
Biết trước kết thúc truyện còn khiến mình buồn hơn khi đọc tới những lúc 2 người hạnh phúc, vui vẻ bên nhau, tới những chuyến đi chơi, hay những lần Will tỏ vẻ ông chủ. 
Me before you không phải 1 cuốn tiểu thuyết ngôn tình với cái kết hạnh phúc thường lệ dù mà người đọc trông đợi, không quá đắm say, không quá rạo rực, họ bên nhau, thay đổi nhau, yêu và sống hết mình.
4
25350
2016-05-06 16:02:21
--------------------------
424990
2675
Gấp cuốn sách lại vào buổi chiều nóng nực. Chẳng thể nghĩ thêm cái gì khác ngoài sự ấm ức và buồn nản
Anh từ người râu ria xồm xoàm, cố tự tử, chán nản cuộc sống rồi trở thành người đàn ông gọn gàng hơn, muốn sống hơn.
Cô từ người con gái luôn lặp đi lặp lại con đường mình đi, chẳng thể thoát ra khỏi quỹ đạo rồi làm điều mới mẻ vậy mà cô trở thành cô gái biết quan tâm hơn, biết cái gì là dám thử dám làm. 1 tuần cô thành cô gái đã đi 3 nước khác nhau, đã thử nhiều trò mạo hiểm, đã dám đăng ký học đại học, đã dám từ bỏ tình yêu 7 năm của mình
Họ đã thay đổi nhau. đã làm cho nhau một con người mới. Họ xứng đáng để sống êm đềm trong thời gian ngắn ngủi ấy,
Nhưng không phải vậy. Họ chỉ bên cạnh nhau trong cái dằn vặt của bản thân rồi cho đến giây phút cuối cùng cuộc đời anh họ bên nhau chỉ vài phút.
Đó là một tác phầm hay nhưng tôi sẽ không đọc nó lần thứ 2
5
856660
2016-05-03 12:51:17
--------------------------
421104
2675
Với cái cố chấp ngu ngốc của tuổi trẻ, mình đã thực sự mong chờ vào một kết thúc đẹp. Ví như cả hai sẽ sống và sống tốt với nhau, hay đại loại là như thế... Nhưng nếu từ bỏ chút ích kỷ và sự hy vọng đầy áp đặt trong lòng, có lẽ đây là một kết thúc đẹp nhất rồi...
Tình yêu của Lou và Will bắt đầu từ lúc nào, mình cũng không biết rõ, nhưng mình biết nó như bề mặt giữa đại dương, nó đến một nhẹ nhàng và bình lặng, và âm ĩ...
Đó là sự thấu hiểu, cảm thông. Đó là sự dẫn đường, khai sáng. Cả hai thật sự được sống khi ở cạnh nhau.
 Là mỗi lần Lou cáu gắt khi ai đó có thể nói những lời làm tổn thương Will, là những lần Will kiên nhẫn, dịu dàng với Lou. Anh là một kẻ tàn tật, nhưng đầu óc, trí thông minh của anh không chút tàn tật nào. Cô là một cô gái an phận, cô đã luôn nghĩ "Mình không làm được!" trong suốt khoảng thời gian trước-khi-được-gặp-anh. Để rồi cô trở thành thân thể anh và anh là ý chí của cô...
Mỗi nhân vật trong câu chuyện đều đáng quý.
JoJo Moyes đã rất tinh tế trong việc điều khiển mạch phát triển của câu chuyện, để nó thấm đẫm và tâm trạng đến thế.
Hiếm có câu chuyện nào cứ làm mình suy nghĩ như "Trước ngày em đến."
Tuyệt vời! Chân thực!
5
348917
2016-04-24 14:36:51
--------------------------
418413
2675
Đã lâu lắm rồi tôi mới đọc quyển sách khiến mình phải khóc. Khóc vì Will và quyết định của anh. Khóc vì những cảm xúc lẫn lộn trong lòng. Dẫu biết quyết định này là sự giải thoát cho Will, mặc dù chỉ là độc giả theo dõi toàn bộ câu chuyện nhưng cũng khiến ta nghẹn ngào. 
Đọc xong cuốn sách, tôi đã suy nghĩ rất nhiều về câu chuyện cuộc đời William Traynor, quả thật nếu tôi rơi vào hoàn cảnh như anh, bị tai nạn liệt C5/6 như anh thì tôi cũng sẽ quyết định như anh thôi.  
Vậy đó, ta nên trân trọng những gì mình đang có. Còn khỏe mạnh là cuộc sống đã ưu ái cho ta hơn nhiều người khác rồi.
5
43144
2016-04-19 09:57:50
--------------------------
416998
2675
Đóng quyển sách lại mà nước mắt rơi lã chã. Cái kết mình đã biết trước sau gì cũng như vậy nhưng thật tình vẫn làm mình khóc ngon lành... Trong lòng dâng lên rất nhiều suy nghĩ và cảm xúc. Câu chuyện không quá nhấn mạnh về chuyện tình của Lou và Will mà còn là những câu chuyện về hiện thực cuộc sống, về những bài học thay đổi chính mình, những bài học về việc sống sao là đủ, sống như thế nào là trọn vẹn cuộc đời ngắn ngủi này. Mặc dù kết cục khá buồn cho Lou cho Will cũng như cho bất kì đọc giả nào nhưng mình tin nó đủ làm thoã mãn được mọi trái tim chúng ta. Rằng câu truyện này khép lại sẽ là nguồn động lực và cảm hứng để ta học cách viết tiếp câu truyện cuộc đời mình ... JUST LIVE BOLDLY ! 
5
141120
2016-04-15 23:40:12
--------------------------
412177
2675
Chưa đầy 2 tháng nữa là phim "Me before you" chuyển từ truyện này ra rạp. Cuốn truyện cũng như phim kể về số phận của hai con người (một từ thành đạt, giỏi giang trở thành bại liệt; một thì thất nghiệp và chia tay tình yêu) đến với nhau. Họ mang cho nhau những thay đổi, nhưng vẫn bị chia lìa bởi số phận đã định đoạt. Will trước sau gì vẫn phải chết, và anh đã chọn cái chết êm ái trên giường bệnh. Dù một cái kết buồn làm câu chuyện không thể nào bớt u ám, nhưng theo tôi đó vẫn là kết đúng và hay.
5
402389
2016-04-07 11:00:31
--------------------------
403643
2675
Mình đọc cuốn này cũng đã lâu rồi. Hôm nọ đọc được tin sẽ có phim chuyển thể từ cuốn này, xem trailer xong thấy buồn kinh khủng, giống hệt lúc đọc truyện. Nội dung truyện rất cuốn hút, bản dịch tiếng việt cũng khá mượt. Lúc đầu mình đã nghĩ truyện sẽ kết thúc theo một cách khác cơ. Cái kết ám ảnh mình mãi, thật sự là rất buồn mà. Nói chung, đây là một câu chuyện tình đẹp mà buồn, rất nên đọc. Còn nếu bạn nào ngại đọc thì có thể đợi phim rồi xem. Về nội dung thì không chê vào đâu được, nhưng cá nhân mình không thích bìa sách lắm, cũng không hiểu tại sao nữa. 
5
1178040
2016-03-24 00:32:52
--------------------------
393113
2675
Phải nói là đoc nhiều cuốn sách của Jojo Moyes, cảm nhận đầu tiên là rất thích văn phong của cô. 
"Me Before You" tôi đọc cũng lâu lắm rồi, nhưng ấn tượng mà nó để lại trong lòng khó mà quên được. Tôi ko nói nhiều về những gì cuốn sách sẽ mang lại cho bạn, bởi bạn cứ đọc đi rồi sẽ hiểu.
Một cuốn sách ko chỉ về tình yêu mà còn về tình người, về cuộc đời. Vẫn nhớ cái cảm giác khi gấp lại cuốn sách mà tâm trạng cứ chênh vênh, hụt hẫng, buồn nhưng có chút thõa mãn đến kỳ lạ.
Giữ lại lúc nào cũng dễ dàng hơn buông ra, Yêu thế nào đôi khi cũng là một sự lựa chọn khó khăn, bởi trên thế giới này chỉ mỗi tình yêu là ko đủ...

5
170569
2016-03-08 10:39:59
--------------------------
384045
2675
Nói thật chứ ít khi mình rate 5 sao như thế này lắm. Bản thân cũng khá khó tính trong phần văn học nhưng cuốn truyện này xuất sắc quá đi. Bây giờ cũng đã chuyển thể thành phim rồi đó, sắp chiếu rồi. Đọc sách khác với xem phim nhiều lắm. Lúc đọc không biết khóc bao nhiêu lần rồi. Phải, truyện tình buồn mà. Nhưng không đến nỗi là sướt mươts như phim hàn, không phải soái ca trong ngôn tình. Tác giả tả chân thật cuộc sống của mỗi người, ai cũng có 1 góc tối, không ai hoàn hảo hết. Và nếu như mình là Lou, mình cũng sẽ để will ra đi, dù yêu nhau thật lòng nhưng bản thân will thực sự bế tắc và đau đớn về thể xác lẫn tâm hồn. Nếu will không tai nạn, sẽ k gặp đc Lou và có thể cưới cô bạn gái hoàn mĩ đó. Nhưng cuộc sống mà, mình khuyên các bạn cũng nên đọc, 1 truyện tình buồn dạy ta cách từ bỏ trong hạnh phúc, dạy ta cách nhìn nhận lại bản thân. Lần đầu tiên mình thấy thích 1 cuốn truyện như vậy. 
5
254031
2016-02-21 17:49:46
--------------------------
354710
2675
Sau khi đọc xong mọi người sẽ hiểu thế nào là sự thật của tình yêu và cuộc sống. Cuộc sống chắc chắn sẽ trở nên hoàn hảo hơn khi có tình yêu, nhưng nếu chỉ có duy nhất tình yêu thì cuộc sống đó không hề hoàn hảo được. 

Lou, một cô gái sở hữu đủ mọi khó khăn trong cuộc sống, từ gia đình, công việc cho đến chuyện hẹn hò cùng bạn trai. Rồi sau đó Lou gặp Will – một người đàn ông 35 tuổi thành đạt trong sự nghiệp, một người đàn ông ưa thích du lịch và mạo hiểm, nhưng rồi lại bị số phận trêu đùa bởi một tai nạn ngoài ý muốn và phải dính liền với chiếc xe lăn cả đời. Toàn bộ câu chuyện là sự nỗ lực đan xen cùng tình cảm của Lou trong công cuộc làm thế nào để đem đến cuộc sống mới hạnh phúc cho Will trong vòng 3 tháng. Không hẳn bao trùm câu chuyện sẽ là sự đau buồn và tuyệt vọng mà đan xen vào đó còn là những chi tiết thú vị, hài hước của những cuộc nói chuyện hay những tình tiết xảy ra giữa Will và Lou và những sinh hoạt hàng ngày ấm cúng của gia đình. 

Cuộc sống mỗi người không đơn giản chỉ toàn màu hồng hoặc một màu xám xịt u ám. Đôi lúc họ cảm thấy hạnh phúc khi họ được làm chính điều mà họ muốn, điều mà họ mong muốn. Khi bạn cảm thấy bế tắc vào cuộc sống, đó chỉ là bạn đã chọn nhầm đường mà thôi, hãy thay đổi rồi cuộc sống sẽ tốt đẹp hơn. 


P/s: Các bạn sẽ phải chuẩn bị thật nhiều khăn giấy khi đọc quyển này. Thề luôn đó ^^

5
564539
2015-12-18 13:59:35
--------------------------
352491
2675
Câu chuyện này chân thực, và cũng như cổ tích.
Anh ấy không phải chàng hoàng tử hoàn hảo lành lặn, sự thật, anh ấy là một người thất bại, hoàn toàn vô dụng, không có quyền quyết định cuộc đời mình.
Cô ấy, có những nỗi sợ hãi rất-bình-thường, cố gắng chống chọi với bóng ma trong lòng bằng những điều kỳ quặc.
Cuốn sách đáng đọc lắm, nó cho tôi biết rằng có đôi khi tình yêu không là tất cả.
Rằng cuộc sống trước đây sẽ ảnh hưởng đến suy nghĩ của chúng ta rất nhiều, ngay cả khi chúng ta phủ định hay khẳng định. Thật đau lòng, nhưng chúng ta luôn chọn cách giải quyết mà mình cho là tốt nhất.
Cảm xúc nó mang lại thật trọn vẹn, và khó quên.

5
92005
2015-12-14 16:20:52
--------------------------
341094
2675
Khi đọc cuốn sách nay, đâu đó cảm nhận được tình yêu thật vĩ đại.Dù khó khăn, gian khổ đến dường nào, chỉ cần lạc quan, yêu đời và một người biết quan tâm chia sẻ với ta thì dường như mọi thứ thật quá ư đơn giản.Đâu đó thấy được mình ở trong đó, đọc xong cảm nhận rằng phải mạnh mẽ để vượt lên trên mặc cảm tự ti của bản thân để bước lên phía trước.Chỉ cần vững tin vào cuộc sống này, lạc quan vào cuộc sống.Hy vọng tất cả sẽ tốt đẹp với những gì mình đang có.
5
730225
2015-11-21 10:24:32
--------------------------
304850
2675
Đọc cuốn này xong mình khóc rưng rức, chưa bao giờ đọc sách mà bị va đập vào cảm xúc nhiều như thế. Một cảm giác buồn mênh mang, buồn đến da diết, buồn đến không còn lấy một giọt thiết tha với cuộc đời. Hoàn cảnh éo le của một đôi uyên ương trẻ đã  đưa ta đến với  đầy đủ mọi cung bậc cảm xúc đau đớn, mặn chát, đắng cay. "Cho dù quyết định thế nào, đó vẫn là cách em lựa chọn hạnh phúc, miễn là em thấy vui thì anh vẫn còn đủ động lực để sống tiếp quãng đời còn lại cùng cái thân xác tật nguyền xấu xa này". 
4
485082
2015-09-16 17:56:32
--------------------------
289373
2675
Đúng như Tạp chí Heat đã nhận xét, câu truyện hay này sẽ là vị ngon tuyệt vời cho cốc ca cao của bạn. Một vị đắng chân thật với cuộc sống, không có những ảo mộng ngọt ngào về một tương lai tươi sáng cho cả hai. Sau khi đọc xong cuốn truyện này, tôi lại ngồi nghĩ về cuộc sống sau này của nữ chính như trong bộ phim "Titanic". Cô sẽ sống một cuộc sống tốt, lấy một người đàn ông cô yêu nhưng không thể nhiều bằng chàng trai trước kia. Rồi cô sẽ ra đi trong giấc ngủ trên chiếc giường quen thuộc ở tuổi 90. Trong giấc mơ bồng bềnh, cô sẽ nhìn thấy chàng trai của cô, không còn ràng buộc bởi chiếc xe lăn, đứng ở nơi ánh sáng cuối đường hầm, mỉm cười với cô. Và họ sẽ có thêm một cuộc đời hạnh phúc sau đó......
5
795430
2015-09-04 14:00:25
--------------------------
232925
2675
Trước ngày em đến, hai năm qua anh là một người vô dụng, đánh mất cuộc đời xưa cũ sau một tai nạn giao thông, gắn chặt thân xác trên một chiếc xe lăn, đau đớn thể xác và cả tâm hồn đã dồn anh đến bước đường tự vẫn ko thành trong khoảng thời gian mòn mỏi khắc khoải đó!

Trước ngày em đến, em là một cô gái tự ti mặc cảm sau một tai nạn thuở thiếu thời, quanh đi quẩn lại trong góc khuất của cuộc đời, gánh trên vai trách nhiệm với gia đình, tự chối bỏ và thu hẹp thế giới xung quanh với những bộn bề lo toan cho cuộc sống!

Sáu tháng đối mặt, từ giận dữ, mệt mỏi đến thấu hiểu, yêu thương, những gì cố hữu trước ngày em đến có đổi thay cho cả hai ta? Cuộc sống này rồi có tươi đẹp hơn, có đáng sống hơn với cả anh và em? Và tình yêu có đủ để níu giữ bước chân anh hay sẽ là kìm kẹp giữ em lại mãi với buồn đau, tủi hổ?

Một cái kết buồn! Nước mắt rơi cùng nỗi đau của từng nhân vật. Nhưng cảm thấy an ủi phần nào với quyết định của em, hãy bước tiếp em nhé!!!
5
6576
2015-07-19 14:06:58
--------------------------
