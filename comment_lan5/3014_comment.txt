426898
3014
Cuốn hơn cả tin tức, có bìa sách rất bắt mắt, đặc biệt là màu sắc của nó, một màu vàng tươi trẻ thu hút mọi ánh nhìn. Mình thích ngành báo chí, PR quan hệ công chúng nên khi thấy sách có nói tới chủ đề liên quan là mình cũng mua về đọc cho biết. Nội dung được viết một cách đầy thuyết phục và đầy ắp những dẫn chứng gây ấn tượng mạnh làm rõ lý do chuyển từ công thức truyền thống 5W sang công thức 5l của một ngành báo trí trí tuệ: am hiểu, thông minh, có tính diễn giải, sâu sắc và soi sáng. 
5
854660
2016-05-07 22:03:25
--------------------------
293437
3014

Cuốn sách là tập hợp những nghiên cứu thú vị của tác giả về sự phát triển của ngành báo chí, từ ngành báo in cho đến báo điện từ. Có những dẫn chứng, phân tích rất hay và bổ ích để giúp người đọc hiểu hơn về làng báo chí rất sôi động và nhộn nhịp của thời đại báo online bây giờ. Bên cạnh đó, tác giả cũng tiếp tục nghiên cứu và đào sâu hơn về ngành này dựa vào sự hiểu biết về báo chí của những thế kí trước cũng như qua các tác phẩm của tác gia Benjamin Franklink từ thế kỉ 18. 
Vì tôi rất thích đọc về chủ đề này nên tôi thấy đây là 1 cuốn sách rất bổ ích. Thông tin dẫn chứng khá chi tiết và dễ hiểu nhưng không bị quá tải. Người đọc vẫn có thể ghi nhớ được các ý quan trọng chính.

4
52328
2015-09-08 12:40:01
--------------------------
