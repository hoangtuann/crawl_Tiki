335725
2936
Mình mua 4 quyển trong bộ Truyện kể trước khi đi ngủ nhằm giúp trẻ phát triển cả về tình cảm và trí thông minh. Những câu chuyện đều gần gũi, giản dị, dễ dàng giai thích cho trẻ hiểu, hình ảnh trong câc câu chuyện rất sống động tươi vui. Còn có các bài học hình ảnh, tư duy đơn giản cho trẻ nữa. Đợi đợt sau mình sẽ mua thêm vài cuôn trong bộ này cho cháu nhà mình. Tôi rất hài lòng với dịch vụ của Tiki, chúc các bạn sẽ thành công hơn nữa trong tương lai
5
833612
2015-11-11 14:00:33
--------------------------
