379090
5503
Những câu chuyện về lòng tham và sự đố kỵ có nhiều câu truyện hay về giá trị của con người , được tổng quát thành các bức tranh minh họa đặc biệt , gây sự thú vị , tò mò trước khi bước vào độ tuổi đi học , trẻ học qua về các thành công vốn khó mấy cũng không sợ hãi , vơi đi ý chí , yên chí cho trẻ học từng ấy bài học có sự mài công chuẩn bị kỹ lưỡng các hình ảnh đậm chất ngộ nghĩnh , thơ văn , hòa mình với cổ tích .
4
402468
2016-02-09 11:53:54
--------------------------
163332
5503
Tết này mình mua tặng đứa em họ mình (3 tuổi), nó rất thích vì hình ảnh rất đẹp, rất sinh động (chứ chưa biết đọc mà :) ). Hơn nữa giấy rất dày mà lại cứng nữa, nhìn vô tuyệt lắm. Với giá như này nhìn chung là được rồi, chẳng bù cho mấy cuốn "cố tích mới" đã mỏng mà nội dung cũng có được bao nhiêu đâu mà lại bán bằng giá cuốn này. Chưa kể hình ảnh có thật, nhưng không có hấp dẫn được mấy bé lắm. Đó là ý kiến của mình và đứa em 3t của mình.
4
548377
2015-03-04 18:51:22
--------------------------
