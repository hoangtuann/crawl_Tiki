483929
6489
Đọc phần giới thiệu về cuốn sách về một tình cảm mà tưởng chừng không thể nào hình thành được khiến tôi vô cùng tò mò. Và nội dung cuốn sách thật tuyệt vời khi dần dần miêu tả hết những góc khuất trong cuộc sống nội tâm của từng nhân vật. Và đâu đó là hình ảnh những suy tư, trăn trở, những đau buồn của con người cho dù sống trong một xã hội hiện đại như Hàn Quốc. Có thể họ là những tử tù, nhưng cũng có thể điều họ đã làm những lỗi lầm trong quá khứ không thể chuộc lại được, và họ hoàn toàn có quyền được yêu thương như một con người. Và cả những người đã từng mắc lỗi trong quá khứ cũng vậy. 
5
258584
2016-09-18 20:03:38
--------------------------
456212
6489
Khi nhìn bìa sách mình đã cảm thấy bị thu hút vì hình nhìn có vẻ bí ẩn và mơ hồ, nội dung tóm tắt cũng rất mới lạ và thú vị. Sách dày, chất lượng sách rất tốt, bookmark hơi mỏng.
Lần đầu tiên đọc truyện của tác giả Hàn Quốc, cảm thấy văn phong rất mới lạ. Nhẹ nhàng nhưng lại khắc hoạ rất rõ tâm trạng của nữ chính, khiến người đọc cảm nhận được nỗi đau của cô. Những chi tiết của vụ giết người và cuộc đời của Yeon Soo không có cảm giác kinh dị, chỉ cảm thấy xót thương cho một chàng trai vốn tốt đẹp biết mấy. Truyện rất sát thực tế, để lại ấn tượng rất sâu sắc.
4
832968
2016-06-23 11:50:19
--------------------------
383025
6489
Ngay từ đầu mình đã thấy ấn tượng bởi nhan đề của cuốn sách, thêm cả bià sách nhìn rất đỗi mờ hồ. Sau đó biết đó là của tác giả Gong Ji Young, mình quyết định chọn cuốn này. 

Như mong đợi, văn chương của tác giả Gong không hề làm thất vong. Khi đọc "Dù con sống thế nào mẹ cũng luôn ủng hộ" mình thấy thấp thoáng câu chuyện này, cảm giác như là tự truyện của chính tác giả. Lỗi viết văn đầy suy ngẫm, triết lý thấm nhuần. Hơn nưac ở "Yêu người Tử tù" còn miêu tả tâm lý nhân vật vô cùng tuyệt vời.
3
513868
2016-02-20 00:24:12
--------------------------
380310
6489
Vậy nên thay cho câu nói "Tôi muốn chết", ta phải đổi thành "Tôi muốn sống một cách tốt hơn", như thế mới đúng. Bởi vì ý nghĩa sâu xa của từ "sinh mệnh" là một mệnh lệnh yêu cầu chúng ta phải sống và không bao giờ nói đến cái chết.

- Yêu người tử tù

Mình biết đến Gong Ji Young qua movie The Crucible được chuyển thể từ chính tác phẩm của cô. The Crucible đã để lại cho mình ấn tượng vô cùng sâu sắc nên mình dần dà để ý cái tên Gong Ji Young và tìm đọc những tác phẩm của cô. Song không phải ngay từ đầu mình đã có ý định mua Yêu người tử tù, mà là sau khi xem Maundy Thursday - phim chuyển thể từ truyện. Và mình cam đoan rằng Yêu người tử tù không làm mình hối hận, ở cả truyện và phim. Truyện rất hay, giàu ý nghĩa, đáng suy ngẫm, nhẹ nhàng mà thấm đượm tình người thấm đến từng ngóc ngách từng câu chữ như cách mà Gong Ji Young vẫn viết trong từng câu chuyện. Phải nói là nó có tính nhân văn, nhân văn sâu sắc; hiện thực, hiện thực sâu sắc. Từng đấy con người trong câu chuyện hiện lên chân thật đến từng nỗi đau, từng cái run sợ,... khiến người đọc phải ngẫm nghĩ, khiến người ta phải phân vân không biết ai xấu ai tốt trong xã hội này, khiến người ta biết sự sống đáng quý và biết trân trọng mạng sống.

Dạo gần đây mình xem một bộ phim, có một nhân vật đã nói thế này: "Chúng ta, rốt cuộc là người hay quái vật vậy?" Để hiểu được điều này, để hình thành cái chuẩn mực đạo đức vốn chưa từng tồn tại, để phân biệt cái xấu cái ác cái đẹp cái thiện, chúng ta phải mất cả một đời. Nhưng để học cách trân trọng sự sống và học cả cách chết nữa, chúng ta cũng phải mất cả một đời.
4
339564
2016-02-14 15:57:09
--------------------------
348638
6489
Tuy rằng tên gọi và bìa sách hơi cứng nhắc và khiến ta ngần ngại khi mua nhưng nội dung của nó thì thật tuyệt vời. Cuốn sách phơi bày những sự thật trong cuộc sống và hơn cả là nạn bạo lực học đường. Không chỉ vậy, cuốn sách còn dạy chúng ta về lòng vị tha, vị tha cho người khác và vị tha cho chính bản thân mình. Tình yêu thể hiện trong "Yêu người tử tù" không ngọt ngào, lãng mạn như những cuốn tiểu thuyết khác nhưng nó vẫn khiến ta hạnh phúc, mỉm cười và tin vào tình cảm đẹp đẽ ấy một lần nữa. 
5
726322
2015-12-06 14:38:11
--------------------------
341955
6489
Tôi chưa từng đọc văn học Hàn, đây là lần đầu tiên tôi đọc cuốn sách này - chính là tác phẩm do một tác giả người Hàn viết. Không giống những bộ phim Hàn Quốc lãng mạn, nội dung cuốn sách là câu chuyện vừa buồn, vừa vui, vừa hạnh phúc, vừa cay đắng của nhiều thân phận con người trong xã hội. 
Đọc xong cuốn sách, điều khiến tôi còn day dứt là việc liên quan đến án tử hình và sự đúng sai của việc kết tội một con người. Có những người không chủ động làm sai, nhưng khi họ cố gắng làm đúng thì phải trả giá bằng sự sai trái. 
Giá trị nhân văn sâu sắc mà tác phẩm để lại chính là tình yêu thương giữa người với người luôn làm nên điều kì diệu.
5
251812
2015-11-23 14:16:29
--------------------------
324877
6489
Tiểu thuyết của Hàn Quốc cũng khá gống với tiểu thuyết Nhật Bản. Nó luôn để lại cho người đọc những suy ngẫm đáng quý về cuộc đời và có một số tác phẩm cũng để lại những dư âm buồn man mác cho người đọc. Cuốn sách Yêu người tử tù mang lại cho đọc giả cả hai yếu tố nói trên. Lúc mua tác phẳm, mình không thích cái tên này của sách, sau mới biết nó có tên gốc là Our happy time. Cái tên gốc thật rất đúng với cuốn sách so với cái tên bìa bởi mình thấy giữa hai nhân vật trong truyện vẫn chưa đén mức yêu. Họ tìm thấy sự đồng cảm, đồng điệu dành cho nhau, đó là sự kết nối giữa hai trái tim tổn thương khi tìm được người sẻ chia. Quan hệ của họ theo mình nghĩ thì vẫn chưa đến mức yêu, có lẽ nếu có thời gian thì nó sẽ dần phát triển thành tình yêu. Truyện cho chúng ta một cách nhìn khác về những người tử tù, họ cũng chỉ là con người và con người thì hẳn sẽ mắc sai lầm và trong trái tim luôn tồn tại những yêu thương, những hối hận. Lỗi lầm của họ gián tiếp từ xã hội mà hình thành. Và nếu trái tim đang tổn thương được xoa dịu sớm hơn thì những tội lỗi đã không xảy ra và sẽ không có người phải từ bỏ cuộc sống tươi đẹp. Cái kết có lẽ là điều tốt nhất mà tác giả dành cho hai nhân vật của mình, đó là một sự giải thoát cho cả hai. Một cuốn sách đầy tính nhân văn và cũng không kém những suy ngẫm cho người đọc.
5
74657
2015-10-22 11:04:20
--------------------------
305953
6489
Tôi muốn cho 5 sao bởi thật sự tôi rất thích cuốn này. Mỗi lời nói mỗi câu trích dẫn đều ý nghĩa đều quá hay. Người tử tù ấy ...tôi đã khóc và đau lòng rất nhiều chỉ bởi tất cả những oan ức những tủi nhục những gì mà xã hội này đã mang lại cho hắn . CHính cái xã hội này đã biến hắn thành như thế.
Nội dung nói chung không có gì để chê. Giọng văn rất hay.
Hình thức: Sách đẹp, bìa đẹp, giấy đẹp, chữ rõ ràng không có lỗi chính tả. 
Là một cuốn sách đáng đọc
5
514847
2015-09-17 11:32:08
--------------------------
289287
6489
Tôi đã mua cuốn sách này vì tôi rất thích văn học Hàn Quốc!Tiểu thuyết Hàn Quốc rất khác trung quốc!nếu tiểu thuyết trung quốc chỉ viết về tình yêu!thì ở tiểu thuyết Hàn Quốc không chỉ dừng lại ở những câu chuyện tình mà nó như kể lại một cách chân thật của người dân nơi đây!!Một đất nước Hàn Quốc văn minh như ngày hôm nay nhưng vẫn len lói đâu đó là những mãnh đời bất hạnh và đáng thương!Mỗi nhà một cảnh,những người nhà nghèo vì không có tiền để chữa bện cho người thân để rồi phải đi cướp và bị bắt vô tù,những đứa con nhà nghèo thì vì mặc cảm tâm lý mà cứ đòi tự tử!Không chỉ là câu chuyện bình thương mà là một câu chuyện đáng để chúng ta phải suy ngẫm!
5
413637
2015-09-04 12:31:55
--------------------------
268284
6489
Tôi đã đọc manga và coi phim trước khi đọc cuốn tiểu thuyết này. Cá nhân tôi thích tự đề gốc của truyện hơn, vì nó hợp với những gì mà nội dung truyện hướng tới, là cái kết buồn nhưng là sự giải thoát cho cả hai nhân vật chính, mặc dù tựa đề Yêu người tử tù gây chú ý, thu hút người mua nhưng lại làm giảm đi phần hồn của cuốn tiểu thuyết.
Đây không phải là thể loại dễ đọc vì không có tình yêu màu hồng, kết thúc hạnh phúc. Tôi cảm thấy tình cảm nhen nhóm giữa Yoo Jeong và Yoon Soo chưa phải là tình yêu, mà là hai tâm hồn bị thương tổn tìm thấy sự đồng điệu và điều đó kéo họ lại gần hơn. Có lẽ nếu cho họ một thời gian nữa, cảm xúc ấy sẽ trở thành tình yêu. Nhưng nếu tác giả để hai người yêu nhau thì sẽ làm mất đi ý nghĩa nhân văn của câu truyện mà trở thành một cuốn tiểu thuyết tình yêu đơn điệu. Cái kết buồn nhưng lại là sự giải thoát cho linh hồn của cả hai.
5
94667
2015-08-16 10:55:20
--------------------------
222109
6489
Kì thực thì tôi đã xem phim chuyển thể từ lâu rồi, tên phim cũng khác nên khi đọc sách thì tôi mới nhận ra mình đã xem rồi nhưng đọc lại thì vẫn thấy rất hay. Yêu người tử tù là một câu chuyện đẹp ở nơi ngục tù - nơi mà kì tích xảy ra khi tình yêu hình thành giữa một cô gái tình nguyện mang trái tim bị thương tổn và một người tử tù sắp đến ngày thi hành án. Kết thúc hơi buồn và tiếc nuối nhưng tôi nghĩ dù không thể mãi mãi bên nhau thì tình yêu giữa hai người họ dù ngắn ngủi cũng đã đem lại hạnh phúc và chữa lành nỗi đau của mỗi người.
5
129176
2015-07-04 23:07:31
--------------------------
166542
6489
Tôi không thể tin nổi tác giả lại xuất sắc đến độ làm tôi phải rơi lệ. Quả thật là cách kể chuyện rất hay, tôi thích cách phân chia "Nhật kí buồn" và cái tự truyện của nhân vật tôi bởi nó làm tôi hồi hộp hơn. Hồi hộp vì không biết anh chàng tử tù có làm chuyện động địa ấy không, hồi hộp vì không biết tình cảm hai người thế nào nữa. tôi thấy đây là tác  phẩm chạm tới trái tim con người bởi ít tác giả nào khai thác hoàn cảnh diễn biến câu chuyện trong ngục tối. Trong mắt tôi, tôi rất ghét những người tử tù nhưng khi đọc truyện này, tôi thấy con mắt tác giả đầy yêu thương và tôi cảm thấy tác giả quá tuyệt vời khi chọn cho mình đề tài không hề dễ này. 
Bên cạnh nội dung, bìa sách làm tôi muốn đọc cuốn sách này hơn bởi bìa sách khá mờ ảo. Có một người cầm ô đi giữa cơn mưa làm tôi thấy mình như ở trong tranh, cũng đơn độc và mong manh như chính nhân vật trong truyện.
5
185178
2015-03-12 21:08:06
--------------------------
131653
6489
BÂy giờ thì tôi mới đọc tiểu thuyết bộ này, vì trước đó đã xem manga và phim rồi với tựa Our Happy Time (được dịch từ tiếng Nhật và Hàn). 

TRuyện này không phù hợp với tuổi teen hay những ai SỢ đọc sad ending - kết thúc buồn. Chính xác là "sợ" chứ không phải là "ghét" hoặc "chán", "không thích". Bởi rõ ràng, khi xem xong sẽ cứ khiến ta cả ngày, cả ngày nghĩ về nó, và khi tối 1 mình trước khi đi ngủ, đã "lỡ" nghĩ về nó thì lại không ngủ được. 

Truyện tiêu biểu cho thể loại, tình huống: 2 trái tim tổn thương sẽ biết cách chấp vá vết thương của nhau. Khắc họa tâm lí hay, hợp logic, dù có những đoạn xem thấy kì kì nhưng nó chỉ là vài phần trăm nhỏ nhỏ không đáng kể. Có những đạon mệt mỏi vì buồn buồn như suy nghĩ tiêu cực của nhân vật, quá khứ ngày trước gặp tổn thương gì, nhưng có những đoạn khiến cho ta thấy vui vui như những cuộc nói chuyện, những món quà, và đặc biệt là những tấm ảnh. Chính vì niềm vui ít nên càng thấy nó vui hơn rất nhiều, quí hơn, ấm áp hơn cho những gì quá khứ đã tổn thương. 

Mà chính điều này, mà khi xem tôi đã mong là đừng kết thúc buồn. Bởi vì sao ư? Vì 2 nhân vật quá đáng thương, mà hạnh phúc lại chỉ vừa mới đến khiến người xem cũng thấy vui lây nên mong mỏi một kì tích xảy ra. Nhưng nếu kì tích xảy ra thì nó sẽ chẳng làm ta nhớ hoài và phi thực tế. 

Tôi không biết nên gọi đây là "ngược tâm" hay không, nhưng tôi thấy nó thuộc loại "dằn xé". Tuyệt vọng thì chỉ có mong muốn kết thúc bản thân, nhưng khi bắt đầu có hạnh phúc lại sợ cái chết sẽ đến. Ngay cả khi xem tôi cũng cảm nhận được nỗi "chờ trong mòn mỏi, chờ trong sợ hãi" khi lệnh đưa xuống cho nam chính bất cứ lúc nào. Nếu như không xem độ dày của sách còn bao nhiêu, độ dài của phim còn mấy phút, hoặc, còn bao nhiêu chương truyện tranh thì tim chắc cũng nhảy ra ngoài vì buồn. 

Nhưng đến cảnh đó thì tim cũng đập thình thịch rồi... 

Cá nhân tôi thích tựa gốc hơn, và cũng muốn bìa nó trong sáng hơn, không nhất thiết phải âm u và có bóng người trong đó. Vì nó khiến ta chú ý đến mối quan hệ, thời gian hạnh phúc đến chỉ trong ngắn ngủi (vì mỗi tuần, 2 nhân vật chỉ gặp nhau 1 lần, mỗi lần gặp cũng không dài là bao). Tựa gốc tuy không cuốn hút người xem, có lẽ vì đã có tựa "thời gian tươi đẹp" của Đinh Mặc trên thị trường, nên tựa này ấn tượng với người mua hơn vì không thấy quyển tiểu thuyết nào về "Tử tù" hiện nay . (tôi đọc không nhiều nên không thấy, nếu bạn biết xin hãy comt ^^) . Bởi như vậy khi đọc ta thấy ấn tượng mạnh với nội dung câu chuyện, khoản thời gian hiếm hoi của cả 2 nhân vật, chứ không phải chỉ riêng hoàn cảnh hiện tại của nam chính: người tử tù.  

Tôi không thích tiểu thuyết kết thúc buồn, nhưng bộ này đã coi mấy lần nên vẫn quyết định mua để sưu tầm cho để các phiên bản. 

p.s: Nếu bạn muốn nghe nhạc phim, từ khóa "Maundy Thursday eng sub-13/13" trên youtube sẽ làm bạn "nghiện" bài nhạc ấy, cũng như xem bằng mắt được thời gian ít ỏi mà hạnh phúc của 2 nhân vật ^^
5
105014
2014-10-26 17:02:33
--------------------------
125589
6489
Tôi ấn tượng với tác phẩm ngay từ khi đọc tên sách, một điều khá mới mẻ và lạ lẫm đối với tôi, chính sự tò mò này đã thôi thúc tôi mua cuốn sách về đọc và tác giả đã không làm tôi thất vọng khi có thể truyền tải được hết ý nghĩa sâu xa cùng với tính nhân văn cao quý, thấm đượm tình người của tác phẩm. Câu chuyện về một người tử tù khi phải đối diện với cái chết gần kề, niềm khao khát sống của anh cũng như sự ăn năn hối cải tuy muộn màng nhưng vẫn còn giữ lại được ý nghĩa sau cùng của cuộc đời này với ước mong dâng hiến những gì còn sót lại để giúp đỡ những người kém may mắn hơn, để chuộc lại những lỗi lầm mình đã gây ra. Tâm trạng của anh cũng là tâm trạng chung của đa số tử tù khi họ biết mình phải ra đi. Đồng hành cũng những người tử tù trong suốt thời gian chờ thi hành án không ai khác ngoài chính những người làm công việc thiện nguyện, đặc biệt là những người thuộc các tổ chức tôn giáo trong đó Thiên Chúa giáo là phổ biến nhất tại đất nước Hàn Quốc. Sơ Mônica trong truyện là một tu sĩ đã nguyện hy sinh cuộc đời để giúp đỡ những người tử tù tìm về cội nguồn của sự chân lý, tìm lại tình yêu cuộc sống và động viên họ vượt qua nỗi sợ hãi. Những hy sinh âm thầm của Sơ đã làm cho câu chuyện trở nên ấm áp tình người và chứa đầy ánh sáng của niềm hy vọng. 

Yoo Jeong cháu gái Sơ Mônica là một cô gái chịu nhiều tổn thương và thiếu vắng sự quan tâm của gia đình nhưng với việc gặp gỡ Yoon Soo mỗi tuần đã vô tình hình thành một sợi dây liên kết giữa 2 trái tim, bởi tuy xuất thân từ hoàn cảnh trái ngược nhau nhưng những gì mà họ đã trải qua trong cuộc đời lại gần giống như nhau. Một tình yêu không thể nói ra nhưng ta vẫn cảm nhận được những thay đổi tích cực nơi hai trái tim vốn nguội lạnh từ lâu, bởi chỉ có tình yêu mới có khả năng cảm hóa con người và chỉ có tình yêu mới đủ sức vực họ dậy từ đáy sâu của sự tuyệt vọng và đau đớn. Cuốn sách thật sự rất có ý nghĩa, tôi đã đọc và hiểu thêm cũng như cảm thông với số phận của các tử tù, bởi đằng sau những tội ác mà họ gây ra còn biết bao điều mà ít ai biết đến và sâu thẳm trong trái tim, họ vẫn là một con người với lòng hướng thiện và nỗi khao khát được sống một cuộc đời tốt đẹp. Và nếu xã hội có thêm nhiều tấm lòng nhân ái thì có lẽ cuộc sống của các tử tù sẽ được an ủi nhiều hơn, tôi thấy đây là một văn hóa rất hay của người Hàn Quốc.
4
41370
2014-09-14 11:09:18
--------------------------
