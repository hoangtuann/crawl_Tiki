478953
8315
Đây là quyển sách khá hay nói về tâm tư ttuổi dậy thì sự phát triển về cả tâm sinh lý được giải thích chỉ dẫn như mốt lá thư của người mẹ gửi cho Minh Anh một cô bé ngây thơ ở tuổi như mình, mình không đọc luôn tuồng đến hết mà mình chỉ nhìn mục lục rồi tìm những điều thắc mắc rồi nhờ những lá thư đó trả lời. Cảm ơn tác giả cũng như mẹ Minh Anh đã giúp mình. Bạn nào cần tìm hiểu vấn đề này thì hãy tìm đến quyển sách hồng này.
4
725049
2016-08-06 10:02:35
--------------------------
450511
8315
"80 lời mẹ gửi con gái" là một cuốn sách bổ ích cho cacsbanj nữ đang tuổi dậy thì.Bìa cuốn sách được trang trí rất đẹp mắt,màu sắc tươi sáng,tuy nhiên màu sắc của giấy bên trong không được đẹp.Cuốn sách được trình bày dưới dạng những bức thư,thông qua những cuộc trò truyện của 2 mẹ con.Những bức thư đó đều chứa chan tình yêu thương của mẹ với con gái.Ngôn ngữ trong thư sinh động,dễ hiểu.Cuốn sách sẽ giúp các bạn nữ tuổi mới lớn giải đáp những thắc mắc liên quan đến tâm sinh lí tuổi dậy thì.Đây là cuốn sách mà mỗi bạn gái tuổi dậy thì nên có.
4
719524
2016-06-18 21:46:51
--------------------------
354239
8315
Mình mua cuốn này cho chị gái mình, nhưng trước khi đưa cho chị gái thì mình cũng đọc qua và có một số nhận xét sau:
Sách gửi đến cho bạn gái những kiến thức cần có tuổi dậy thì, cũng như cách yêu bản thân và đối xử tốt với mọi người xung quanh
Sách giải đáp mọi điều thắc mắc liên quan tới tuổi dậy thì. Đồng thời giải tỏa những lo lắng không thể nói ra của cha mẹ.
Các bậc làm cha làm mẹ nên đọc để có thể hiểu một phần nào con cái mình vào tuổi dậy thì sẽ ra sao để mà có cách ứng xử phù hợp, tránh làm con tổn thương vì sự thay đổi tính tình của con cái
4
213826
2015-12-17 15:41:45
--------------------------
264870
8315
Bậc làm cha làm mẹ nào cũng vậy, khi con cái bước vào tuổi dậy thì,tâm trạng của cha mẹ là tâm trạng đan xen giữa vui mừng và âu lo. Đó là giai đoạn mẫn cảm, làm sao để cha mẹ hiểu được con cái và ngược lại, làm thế nào để giao tiếp với con và làm thế nào giúp con vượt qua giai đoạn đoạn này 1 cách thuận lợi luôn là điều trăn trở của các bậc làm cha làm mẹ."80 lời mẹ gửi con gái" là những tâm sự của người mẹ gửi đến con gái đề cập những thay đổi của cơ thể thiếu nữ tuổi dậy thì, những kiến thức của tuổi dậy thì như cách chăm sóc bản thân,yêu bản thân và đối xử tốt với mọi người xung quanh, giải đáp mọi thắc mắc liên quan đến tâm sinh lý tuổi dậy thì. Quyển sách như 1 Cẩm nang của tuổi mới lớn.
4
512701
2015-08-13 15:26:59
--------------------------
