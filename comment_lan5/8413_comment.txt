485262
8413
Đây là cuốn sách đầu tiên của Hồ Anh Thái mà tôi đọc, khởi nguồn cho hàng loạt các tác phẩm sau của tác giả mà tôi cố công đọc bằng hết. Nhưng có lẽ ấn tượng ban đầu vẫn ám ảnh hơn cả - ám ảnh về nỗi thống khổ của người phụ nữ, đặc biệt là trong truyện ngắn Tiếng thở dài qua rừng kim tước. Trong câu chuyện, " tiếng sáo" và hành động " chạm mũi" như thể là hai chi tiết đắt giá làm tôi ấn tượng nhất, Từ tiếng sáo bộc lộ nỗi niềm, hân hoan, cho đến day dứt, điên cuồng của người si tình. Tất cả khiến tôi cảm thấy ray rứt, vì lẽ gì mà một người phụ nữ đẹp và đáng được hạnh phúc lại bất hạnh đến vậy? Với tác phẩm này, Hồ Anh Thái đã chạm đến và lay động những cảm xúc rất thật của người đọc.
5
1695525
2016-10-01 08:23:48
--------------------------
427285
8413
Cuốn sách đem lại cho người ta nhiều trải nghiệm và suy ngẫm về đất nước, con người và văn hóa Ấn Độ. Nơi tồn tại một nền văn hóa nhiều dị biệt, mới lạ và đầy bí ẩn trong mắt nhân loại. Đất nước khởi nguyên của nhiều tôn giáo, trong đó có Đạo Phật, vẫn tồn tại nhiều vấn đề nóng bỏng và nhức nhối. Hồ Anh Thái đã nhìn thấy những vấn đề đó bằng con mắt của một người ngoại quốc đầy am hiểu, cảm thông.
Văn phong thâm trầm, phù hợp với không khí của đất nước đầy trầm tích, cuốn con người ta vào thế giới kì lạ của nó.
4
622631
2016-05-08 21:34:45
--------------------------
406361
8413
Không chỉ hiểu sâu về văn hóa, con người Ấn Độ, Hồ Anh Thái còn chỉ ra được "căn bệnh" của xã hội Ấn Độ, vốn thừa đền tháp, thừa thần thánh mà thiếu người sung túc, một cuộc sống quẩn quanh trong một thung lũng như tù ngục mà không ai vượt qua để đi tìm chân trời.
"Tiếng thở dài qua rừng kim tước" thì đặc biệt buồn, gây ám ảnh về thân phận phụ nữ trong một xã hội nặng nề với các hủ tục và định kiến. Nơi mà mỗi người phụ nữ khi sinh ra đã là "một khởi đầu không may mắn". Gấp truyện lại mà tiếng sáo của Raja vẫn còn như văng vẳng, còn len lỏi trong rừng kim tước ẩn dụ, như một tiếng thở dài buồn thương.
4
1225335
2016-03-28 09:51:50
--------------------------
401092
8413
Hôm qua ngồi xem Anandi với mẹ, chợt nhớ đến cuốn sách này. Vẫn nguyên vẹn như vậy, dù đọc lại nhưng ở Hồ Anh Thái vẫn có 1 nét đặc trưng rất riêng và lạ về văn hóa và đời sống của Ấn Độ, về số phận của người phụ nữ Ấn. Đọc cuốn này mà xem Anandi thì ôi thôi nó hay tuyệt. Đây là một cuốn sách hay rất đáng để bỏ tiền ra mua, các bạn trẻ nào quan tâm đến Ấn Độ ( hoặc không cũng đc) thì nên mua cuốn này để có những trải nghiệm lạ và riêng với Hồ Anh Thái
5
80831
2016-03-20 10:04:54
--------------------------
362848
8413
Đây là cuốn sách mình thích nhất của Hồ Anh Thái. Tác giả chưa bao giờ làm mình thất vọng. Tầm của anh Thái, thì chẳng có gì để chê. Vấn đề là những cuốn sách hay như thế này lại ít được giới trẻ biết đến. Chẳng hiểu các bạn trẻ đang đọc những gì, những câu vô nghĩa và vô vị. Sao không tìm những cuốn sách hay ho như thế này để đọc nhỉ? Trong tập, thì “tiếng thở dài qua rừng kim tước” ám ảnh hơn cả. Bạn nào muốn tìm hiểu về văn hóa và đời sống của Ấn Độ thì nên tìm sách của Hồ Anh Thái.
5
247780
2016-01-02 22:47:18
--------------------------
229415
8413
Hồ Anh Thái là tác giả xuất sắc khi viết về Ấn Độ: Văn hóa Ấn Độ, con người Ấn Độ. 
Đọc truyện của Hồ Anh Thái, ta như thấy được trước mắt một Ấn Độ sống động và chân thực như đúng cách dùng từ của Hồ Anh Thái: "một bảo tàng sống của văn hóa" 
Ấn Độ không như những cái nôi văn hóa như ở Trung Quốc, Roma,... đã mai một phần nhiều. Cái văn hóa và cái không khí cổ xưa từ bao đời nay vẫn sống ở nưoi mỗi con ngừoi Ấn Độ, từ trong bản chất, vẫn sống trong từng nếp sống, phong tục hàng ngày.
Nhưng, đi kèm với văn hóa truyền thống, là những cổ tục, lạc hậu, đi kèm với sự hấp dẫn không thể cưỡng lại được của xứ Ấn, là chan chứa bi kịch và nỗi đau
Riêng tập truyện này, Hồ Anh Thái nhấn mạnh, xoáy sâu vào số phận bi kich người Ấn, đặc biệt là ngừoi phụ nữ. Tất cả bi kịch của họ tựu chung lại, đều bắt nguồn từ mặt kia của văn hóa Ấn Độ...

Tập truyện này rất hay, rất ám ảnh, mình thích nhất là truyện Lá quốc thư. Cách viết của Hồ Anh Thái mở ra rất nhiều hướng suy nghĩ tư duy, suy luận của ngừoi đọc về câu chuyện. 
5
323143
2015-07-16 20:19:50
--------------------------
182391
8413
Khi nhắc đến Ấn Độ trong thời gian này, chắc người ta sẽ nghĩ đến những vụ cưỡng bức phụ nữ, nói cách khác là số phận bi thảm của những người phụ nữ. "Tiếng thở dài qua rừng kim tước" là quyển sách được bắt đầu bằng câu chuyện ám ảnh về số phận bi thảm của một người phụ nữ, tên truyện cũng chính là tên của tập truyện ngắn này. 

Người phụ nữ phải có của hồi môn mới lấy được chồng, và phụ nữ sinh ra chẳng khác gì đồ vật hay nô lệ, mạng sống chẳng đáng giá, chỉ cần một can xăng tẩm vào bộ sari và châm lửa, mọi thứ sẽ kết thúc và chẳng ai phải đền tội. Và điều này chỉ vì cô dâu không trả góp đủ món tiền hồi môn cho nhà chồng. 

Tôi biết tác giả Hồ Anh Thái lần đầu tiên qua Cõi người rung chuông tận thế, Mười lẻ một đếm... và đến Đức Phật, Nàng Savitri và Tôi, Người bên này, trời bên ấy thì hoàn toàn trở thành fan của tác giả. 

Đất nước Ấn Độ với những nét văn hóa vừa xa lạ, vừa quen thuộc hiện lên trong những trang viết của nhà văn rất ấn tượng, sinh động bởi vì nhà văn là một người trải nghiệm cuộc sống thực tế ở vùng Nam Á và  Trung Đông trong một thời gian dài. Phải nói là khi đọc những truyện nhà văn viết về vùng đất này, có cảm giác rất thỏa mãn, bởi sự thấu hiểu sâu sắc, kiến thức văn hóa, lịch sử của tác giả. 

Tập truyện này, ngoài phụ nữ Ấn Độ, thứ còn làm tôi nhớ chính là sự phản bội. Hình như truyện nào cũng bắt gặp sự phản bội trong tình yêu, tình bạn, tình người... Điều này là tôi cảm thấy xót xa, vì cảm giác mọi thứ mong manh quá, tăm tối quá, xấu xa quá. Sự thể hiện của nhà văn về điều này nhẹ hẫng, đơn giản, ngắn gọn và điều đó lại càng làm người đọc "thấm".
4
60569
2015-04-13 22:51:49
--------------------------
124037
8413
Cuốn sách này chui vào tủ sách của mình một cách rất tình cờ trong một lần đi dạo hàng sách cũ. Cái tên Hồ Anh Thái từng là một cái tên mơ hồ đối với mình, nhưng sau lần đọc Namaskar - Xin chào Ấn Độ, thì mình đã để ý tới bác ấy. Văn phong của Hồ Anh Thái rất hay, vừa lạ vừa độc đáo. Vốn là một nhà ngoại giao lâu năm nên bác ấy có vốn kiến thức rất sâu rộng về văn hóa nước ngoài, đặc biệt là Ấn Độ. Cuốn sách này lấy bối cảnh xã hội Ấn Độ, bao gồm một tuyển tập những truyện ngắn về văn hóa, tôn giáo và đời sống Ấn. Trong cuốn này thì mình thích nhất là truyện Tiếng thở dài qua rừng kim tước, với truyện Người Ấn (bộ mặt tôn giáo Ấn Độ hiện rất rõ qua truyện này). Bạn nào quan tâm đến Ấn Độ mình recommend quyển này.
4
58432
2014-09-04 22:34:05
--------------------------
