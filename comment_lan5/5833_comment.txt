441347
5833
Tôi không nghĩ đây là một cuốn sách được viết bởi một cựu chiến binh tuổi đã 80. Câu chuyện của bác được viết lại một cách giản dị, dễ gần giống như nghe trực tiếp những câu chuyện thời chiến mà bố mẹ tôi vẫn kể. Qua những trang sách của bác một Hà Nội kiên cường trong gian, khiến tôi cảm nhận được sự ấm áp của tình người.
Điều đáng tiếc là cuốn sách này quá ngắn. Tôi sẽ tìm đọc thêm những tác phẩm của bác Phạm Thắng
Hi vọng Tiki sẽ sớm có cuốn "Đội thiếu niên tình báo bát sắt"
5
869075
2016-06-03 09:11:53
--------------------------
440147
5833
Phạm Thắng vừa là một cán bộ quân nhân ông vừa là một nhà văn! Bằng lối văn giản dị, dễ đọc, dễ nhớ Tháng ngày thương nhớ là tác phẩm phù hợp với mọi lứa tuổi độc giả, nhất là với lứa tuổi thiếu nhi. Đây là một tác phẩm hồi kí của tác giả về tuổi thơ của mình gắn liền với thủ đô Hà Nội cổ kính những năm 40 của thể kỉ trước với con ngõ Đông Xuyên- phố Huế... với kỉ niệm tinh nghịch của tuổi học trò,   cuộc sống gia đình.
  Bên cạnh đó tác phẩm còn gắn liền tuổi thơ tác giả với những mốc Lịch sử quan trọng của Dân tộc Việt Nam. Hình ảnh Hà Nội trước Cách mạng tháng 8, với con người dạy học xưa như ông giáo San, bà đầm Tây... hiện lên hình ảnh một thời đã qua của lịch sử. Rồi đến sự kiện Nhật đảo chính Pháp độc chiếm Đông Dương(9/3/1945), tác giả đã sống cùng năm tháng đất nước khổ đau dưới ách phát xít, nạn đói năm 45 với sự chứng kiến và tái hiện làm xúc động, xót thương độc giả... Rồi tác phẩm làm bừng lên ngọn lửa hào hùng về không khí Tổng khởi nghĩa Cách mạng tháng 8 ở Hà Nội xục xôi mít tinh biểu tình và giành chính quyền,tác giả cùng gia đình hòa mình vào ngọn gió Cách mạng của cả Dân tộc, ngày 2/9 rất đỗi gần gũi thân thương, trong đó hình ảnh đội trống thiếu niên thật đẹp làm sao... Sau cùng là hình ảnh cả Hà Nội đứng lên hưởng ứng lới kêu gọi toàn quốc kháng chiến(19/12/1946) anh dũng chiến đấu chống Pháp và quyết tâm bảo vệ Thủ đô ngàn năm văn hiến,trong đó tuổi thơ tác giả là một phần không thể thiếu-Chú bé liên lạc viên tinh nghịch,dũng cảm...
4
1168716
2016-06-01 11:13:21
--------------------------
337001
5833
Mình mới mua và đã nhận được quyển sách này được 1 ngày. Quyển sách có giá tiền khá mềm chỉ 12 nghìn đồng nhưng nội dung rất hay, phù hợp với lứa tuổi thiếu nhi. Vì thấy bìa sách khá là phù hợp với mấy bé nên mình đã mua vài quyển trong đó có quyển này để tặng em mình. Có vẻ như em nó rất thích. Cảm ơn Tiki đã giúp mình tặng cho người thân được những món quà nho nhỏ và ý nghĩa này. Chắc sẽ ủng hộ Tiki dài dài và đang chờ đợt hội sách tiếp theo lai rinh vài ba em về đọc nữa.
5
521504
2015-11-13 13:13:36
--------------------------
