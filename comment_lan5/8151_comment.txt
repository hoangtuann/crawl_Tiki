484979
8151
Khi đọc những phần truyện của Miko và Tapei thật sự làm mình rất vui. Tình yêu học trò thật ngây thơ, những lúc mà Tapei chọc ghẹo Miko hay những lúc Miko giận hờn, hẳn là ai cũng từng trải qua những cảm xúc này
Sự chênh lệch giữa ngoại hình của 2 nhân vật: Tapei lớn phổng phao, năng động còn Miko thì nhỏ con, rất con nít, còn hay ganh tỵ với em trai nhưng khi 2 nhân vật có tình cảm thì y hệt nhau
Ôi tình yêu của họ thật trong sáng và đẹp làm sao ^^ <3 <3
4
1484875
2016-09-28 15:17:01
--------------------------
462986
8151
Cuốn Love này không có gì mới nhưng cái mà nó làm mình mua vì nó tuyển tập lại hết những câu truyện tình yêu của Miko - Tappei, Yuko - Kenta,... nó làm mình nhớ lại những câu truyện đã đọc. Cuốn Love này màu bìa sách mình không ưng lắm, vì là tựa đề là Love nên sao không để màu hồng chẳng hạn. Mình thích phần mà Mkio trở thành cấp 3. không ngờ miko cao nhanh vậy luôn :v Cuốn Love này mình tìm ở nhà sách có vẻ như đã hết nên mới vội vàng lên Tiki mua về.
5
1286291
2016-06-28 23:44:32
--------------------------
457186
8151
Đây là 10 mẩu truyện về tình yêu mà tác giả cho là hay nhất. Thật ra, mình đã đọc rồi, nhưng đọc lại vẫn thấy vui vui sao í. Trong những truyện tình củm của Miko với Taipei thì mình thích nhất là những truyện Miko được đưa đến tương lai (nhìn 2 người họ ở tương lai cực tình củm lun). Vì là truyện thiếu nhi nên những truyện tình yêu này rất tự nhiên, trong sáng, dễ thương, ngọt ngào. Đọc xong truyện mình cảm thấy rất vui vẻ, tự nhiên. Tác giả rất khôn khéo mang những điều đáng yêu vào truyện. Thật tuyệt khi có một quyển Selection Love như thế này.
5
123186
2016-06-24 07:59:41
--------------------------
432277
8151
Miko được biết đến với sự hài hước đơn giản. Trong tập truỵên Miko selection - top 10 love lần này không chỉ là những câu chuỵên thường ngày những mảnh chuỵên nhỏ trong đời sống như những tập truỵên khác mà đặc  biệt hơn trong tập truỵên này gồm 10 câu chuỵên nhỏ từ những tập truỵên trước được tác giả và mọi người đánh giá là mười câu chuỵên hay nhất trong bộ truỵên miko. Đúng như vậy đây là mười câu chuỵên tôi cảm thấy rất hay. Mặc dù chỉ là viết lại từ nhũng tập tuỵên được phát hành trước, mặc dù là tôi đã đọc chúng từ những quyển trước nhưng đúng là theo bình chọn mười câu chuỵên hay nhất bởi vì khi đọc lại chúng trong quyển miko selection - top 10 love tôi không cảm thấy có chút sự nhàm chán nào mà ngựơc lại chúng rất hay còn có những câu chuỵên thật ý nghĩa
5
1266408
2016-05-18 21:18:54
--------------------------
422862
8151
Trong tập này có chuyện của Yuko-Kenta, Yoshiki-Miho, Tâppei-Miko-Yoshida nhưng lại thiếu mất phần của Mari - tuy Mari không có cặp nhưng Mari vẫn thích đơn phương vẫn phù hợp với chủ đề mà, Mamoru-Yuka cặp này mình mong chờ nhất thế mà không thấy huhu.
Bên cạnh đó những cảnh giữa Tappei-Miko chỉ cần nhìn hình thôi cũng đủ khiến mình có cả giác lân lân rồi. Cả phần trao đổi quà trong tập Noel một mình nữa chứ cứ như là duyên phận sắp đặt từ trước vậy. 
Hy vọng Tiki sớm bán lại những tập còn thiếu nhé. Mình muốn sưu tập trọn bộ Miko
4
61165
2016-04-28 11:21:01
--------------------------
380740
8151
May mắn quá mình vừa mua xong thì tiki hết hàng. Cuốn truyện này là một tấm vé khứ hồi tuổi thơ mình tự tặng bản thân trong khoảng thời gian bận rộn đến ngộp thở thế này. Nhớ ngày xưa còn để dành từng đồng một để đi thuê chứ cũng không dám mua về để đọc, sợ bị bố mẹ mắng. Đã bao năm qua rồi, đọc không biết bao tóp mâng =))) vậy mà cuối cùng nhìn thấy nhóc miko trong lòng bỗng dâng lên cảm xúc khó tả. Mỗi lúc đọc truyện lại thấy mình như đang nằm dài ở nhà cũ, đọc truyện, ăn vặt trong cái nóng mùa hề oi bức, vô ưu, vô lo. Bây giờ chỉ còn biết cố gắng tìm lại cảm xúc thanh bình của quá khứ chứ khó có cơ hội gặp được những khoảnh khắc đó ở hiện tại nữa rồi.
5
944476
2016-02-15 19:18:38
--------------------------
378981
8151
Quyển này tập hợp những chuyện tình gà bông các kiểu của Miko và Tappei, dễ thương muốn chết luôn vậy đó. T-T Không hiểu sao, mặc dù nét vẽ của Yono Eriko tương đối đơn giản, tình tiết cũng nhẹ nhàng yên ả, thế mà mỗi lần đọc “Nhóc Miko” là mình bắt đầu quắn quéo, đọc đi đọc lại vẫn cứ xuýt xoa, có lẽ vì tác giả rất tâm lý, cũng có thể vì sự chăm chút cho từng đoạn đối thoại, mà dù thế nào thì mình cũng đã thành fan mất rồi.
Tác giả quả thật rất tâm lý, những chuyện vui vẻ, hoang mang thời Tiểu học hiện ra sống động qua từng mẩu truyện nho nhỏ, đọc mà thầm ước thời đi học của mình có một cậu chàng vụng về nhưng tốt bụng như Tappei. 
Có lẽ dù đọc được bao nhiêu truyện lãng mạn, cái dễ chạm đến lòng mình nhất chỉ có thể là tình cảm trong sáng, ngây thơ thế này mà thôi. 

5
198930
2016-02-08 20:31:49
--------------------------
376555
8151
Câu chuyện này là tổng hợp nên mình cũng đã đọc cả rồi, nhưng vẫn muốn đọc lại vì thế mình đã mua nó và thêm nữa đó là cuốn mình mua chỉ còn lại một cuốn cuối cùng nên cảm thấy thật may mắn nha. Miko là cô bé nhỏ nhắn năng động, dễ thương, lại trông ngây ngô nên chuyện tình giữa cô bé với lại Tappie cũng thật dễ thương. Tappie quan tâm nhưng lại không nói, ghen thì cái điệu bộ nhìn mà muốn giết cái thằng trước mà mà vẫn ra vẻ không có gì ^^ hé hé. Chúng nó thậg đáng yêu và đang làm người ta ghen tị với tình yêu chúng nó, tôi cũng muốn có một người như Tappie quá đi....
5
1083070
2016-01-31 09:39:02
--------------------------
368993
8151
Hôm nay lục lại tủ sách mới thấy quyển nhóc Miko này. Ngồi đọc lại mà vui quá đi thôi, chả biết mình đã đọc đi đọc lại bao nhiêu lần rồi nữa. Xuyên suốt các tập truyện, mình luôn yêu thích nhất cặp đôi Miko - Tappei. Không hề lãng mạn ngọt ngào như Yuko - Kenta, 2 nhóc tì này chỉ suốt ngày cãi nhau, gặp mặt là lại chí chóe... vậy mà vẫn thu hút người đọc vô cùng. Thật sự mình muốn cảm ơn Ono sensei rất nhiều vì đã tổng hợp chuyện tình cảm của mấy nhóc thành 1 quyển riêng như thế này. Vừa tinh nghịch vừa đáng yêu <3 
5
178929
2016-01-14 22:10:53
--------------------------
357706
8151
Đây là tập tổng hợp chuyện tình ngốc xít của nhóc Miko và Tappie, tập này thật sự rất dễ thương và đáng để đọc đấy các bạn!!! ^^ cám ơn Tiki đã mang bộ truyện này đến với tớ, những lúc cv hay chuyện học hành mệt mỏi là tớ cầm bộ truyện này trên tay là quên mọi phiền não liền, vì vậy tới đã sưu tầm đủ bộ truyện này luôn. Cầm quyển truyện trên tay và ngấu nghiến quên ngày tháng ^^. Thật là không uổng phí khi đầu tư vào bộ truyện tranh dễ thương này!
5
897117
2015-12-24 03:00:36
--------------------------
308991
8151
Quả thật mình tuy lớn rồi nhưng bây giờ tự dưng lại thấy thích đọc lại truyện tranh, cơ mà phải là mấy truyện dễ thương và thư giãn như Miko vầy cơ. Đọc mấy collection thì thấy yêu đời và dễ chịu hơn hẳn ý... chưa kể lại thư giãn cực kỳ luôn.
Mấy mẩu truyện trong sáng, đáng yêu và đúng chuẩn tâm lý của mấy bé học sinh tiểu học. Tình cảm thì nhẹ nhàng mà lúc nào cũng ẩn chứa những bài học cỏn con nhưng rất ý nghĩa trong cuộc sống hằng ngày. Mình thích nhân vật em trai Miko. Cậu bé đúng chín chắn... ^^
4
87926
2015-09-18 21:51:20
--------------------------
308447
8151
Một trong những tập miko selection mình mua ở đây. Miko lúc nào cũng được tác giả Ono đề cập là 1 cô nhóc cực dễ thuơng kì quái, quậy phá và lắm trò. Nhưng ở tập này lại chủ yếu nói đến những chuyện tình cảm mập mờ giữa Miko và Tappei, nhất là 10 chuyện tình cảm mà tác giả cho là đáng yêu nhất ở đây. Với mình cuốn này rất hay, đặc biệt nhất là tập " xin lỗi Tappei " ở đoạn cuối khi Tap nói với Miko là " sáng tui chờ đi học chung " trong rất là ngây thơ, trong sáng và đáng yêu của một tuổi học trò. Mình rất thích những điều tác giả mang đến cho truyện. Mong tác giả vẫn luôn tạo nhiều điều bất ngờ nữa cho người hâm mộ!
5
810206
2015-09-18 18:11:06
--------------------------
299784
8151
Mặc dù những câu truyện có trong "nhóc Miko Top 10 love" này trong những tập khác cũng đã có rồi nhưng mình vẫn rất thích mua về để đọc lại. trong truyện này, mình thích nhất mẩu truyện "mối tình đầu", đọc rồi mà bây giờ vẫn thấy thích thú, dễ thương lắm luôn. Miko luôn là một cô bé ggây rất nhiều ấn tượng với mình bởi đó là một cô bé ngây ngô, tốt bụng và dễ thương. Những truyên còn lại cũng vậy, mình cũng rất thích. đọc xong cuốn này, mình cảm thấy cuộc sống vui vẻ hơn, mình cũng không còn lo phiền như trước nữa.Cứ hồn nhiên như nhóc Miko thì mình khỏi phải lo gì hết.
5
306288
2015-09-13 15:13:35
--------------------------
288276
8151
Đọc Miko mình cực kì thích những câu chuyện tình cảm, đặc biệt là của Miko và Tappei. Khác với các cặp đôi khác, chuyện tình cảm đã rất rõ ràng như Yuko và Kenta, Mamoru và Yuka, chuyện của Miko và Tappei thì lại khác. Hai bạn dường như đã có ý thích nhau nhưng chưa nhận ra.. Ngay cả trong mơ Miko còn thấy Tappei lại là hoàng tử  của mình cơ mà... Tuy nét vẽ của một số truyện đầu không đẹp bằng những tập Miko xuất bản sau này nhưng điều đó không làm mất đi tính vui nhộn, hài hước và dễ thương của Miko. Và mình nghĩ truyện xứng đáng 5 sao.
5
247850
2015-09-03 14:44:56
--------------------------
252046
8151
Mình đã đọc truyện cô nhóc Miko này, và cũng đọc khá rời rạc thôi. Tuy vậy mình nhớ rất rõ cặp đôi nhóc Miko và cậu bạn Teppei.
Chuyện tình củm của 2 bạn thì đáng yêu, hài hước vô cùng. Vì chỉ là 2 đứa nhóc (nhỏ xíu à) nên chuyện tình cảm chẳng đâu đến đâu, dở dở ương ương nhưng cũng làm cho người ta hâm mộ, muốn mình cũng có một tình bạn đẹp, một "mối tình đầu" ngọt ngào như thế.
Chọn mua cuốn này là chuẩn luôn vì sách tập hợp rất khéo những câu chuyện về tình bạn của 2 nhóc. Chất lượng sách cũng rất ổn. Mong sẽ có nhiều tập truyện tổng hợp dễ thơpng như này nữa.
5
469844
2015-08-03 12:54:29
--------------------------
243124
8151
Quả thật Miko cực kỳ dễ thương luôn. Cả em mình và mình đứa nào cũng mê cả. Tập Top 10 love này tuyển chọn 10 truyện tình cảm của cặp Miko và Tappei cực kỳ đáng yêu luôn! Mình thích nhất là tập tặng Sô cô la á, Miko đáng yêu vô cùng luôn nha! Truyện hay mà nét vẽ cũng đẹp nữa, dễ thương ghê. Về phần hình thức thì truyện của Nhà xuất bản Trẻ thì tốt miễn bàn rồi. Giấy xốp siêu nhẹ, nhưng không phải bì rời. Hy vọng sau này Trẻ sẽ cho tái bản Miko với bìa rời!!! 
5
400930
2015-07-27 10:43:29
--------------------------
242144
8151
Mình được bạn tặng cuốn truyện này nhân dịp sinh nhật. Truyện hay, đáng yêu và trong sáng đúng chất tình cảm tuổi học trò, mà lại là tình cảm tuổi tiểu học nên có phần trẻ con. Đọc truyện xong thấy tâm hồn mình như trẻ lại rất nhiều. Càng yêu Miko hơn :)) Truyện phù hợp nhất là cho các em thiếu nhi, tuy nhiên phụ huynh cũng nên đọc để hiểu thêm về con em mình.
Bìa sách đẹp, hình vẽ ngộ nghĩnh, dễ thương. Mình thích cái mặt Miko to bự chảng, nhìn mắc cười ghê luôn ^^  

 
5
636135
2015-07-26 10:07:20
--------------------------
233295
8151
Tập truyện nằm trong bộ collection, bao gồm những mẩu chuyện chủy yếu quay xung quanh cặp đôi miko tappei được chọn lọc từ hơn hai mươi tập truyện đã phát hành. Và đã là fan của bộ truyện thì chắc chắn không thể bỏ qua tập truyện này. Tuy chỉ là những mẩu chuyện cũ đọc rồi nhưng việc tập hợp lại chuyện tình cảm của các nhân vật được khắc họa rõ nét hơn không chỉ có mỗi cặp đôi miko tappei mà còn có cả các tập về yuko, yoshiki, yoshida. Đọc mà cảm thấy ghen tị với những tình cảm hồn nhiên, trong sáng của tuổi học trò trong truyện. Một bộ truyện phù hợp cho mọi người.
5
381629
2015-07-19 15:14:48
--------------------------
232410
8151
Top 10 câu chuyện tình củm cực kì dễ thương, trong sáng và đáng yêu vô cùng! Cặp đôi Yuko - Kenta thì biết thể hiện tình cảm dành cho nhau, luôn quan tâm, chăm sóc lẫn nhau nhưng cũng có đôi lúc giận dỗi, hiểu lầm, mà mọi chuyện cũng qua đi nhanh chóng, đúng là một cặp đôi tuyệt vời! Còn Miko với Tappei thì con nít lắm, như chó với mèo ấy, hay chọc phá lẫn nhau nhưng thực chất luôn quan tâm thế thôi, dễ thương quá chừng! Đọc Miko xong là tâm hồn cứ bay bổng như trẻ con ấy, thoải mái vô cùng. Ngắm cái bìa sách thôi cũng thích rồi, mấy nhóc mặc đồ đầu bếp trông bảnh quá, cái bánh cũng ngon nữa!
5
315293
2015-07-18 19:45:07
--------------------------
219653
8151
Chuyện Miko miêu tả 1 cách dễ thương về tình cảm tuổi tiểu học :)
Nhok Miko lùn tủn đáng yêu , đọc truyện này toàn phá lên cười :* :* :* 
Ai chưa đọc Miko lần nào  thì nên mua đọc thử quyển này đầu tiên . Vì đây là tuyển tập 10 câu chuyện hay dành cho bạn gái cùng với nhiều câu chuyện bên lề thú vị
Nội dung tập này là một tuyển tập đặc biệt ngọt ngào và tình cảm nhất trong sê-ri Nhok Miko. Ngoài tập Tappel với MiKo trong " Lễ hội năm mới "còn có các tập về Yoshiki và Yukko ... Ai từng đọc Miko rồi và đang tò mò không biết cấp 3 Miko sẽ thế nào thì nên tậu ngay em sách này... Vì trong truyện tác giả đã giành riêng 1 chuyện để vẽ " nàng thiếu nữ MiKo cấp 3"
5
633734
2015-07-01 19:46:44
--------------------------
219195
8151
Vẫn theo phong cách siêu dễ thương và hài hước của series Nhóc Miko nhưng cuốn Love Selection tổng hợp lại những câu chuyện tình củm của các nhân vật. Mặc dù có thể mọi người sẽ cho rằng mới cấp 1 mà yêu đương này nọ là không được, nhưng mà trẻ con có tình cảm của trẻ con, có cách thể hiện của riêng chúng. Thực sự là rất ghen tị với tình cảm thuần khiết, vô tư của tụi nhóc. Thích một chút, ghét một chút đều biểu hiện ra mặt, hành động rất vô tư, chỉ là đối phương và ngay cả chính bản thân mình còn chưa hiểu tình cảm đó là như thế nào thôi :)) Với cặp Kenta và Yuko, dù đã là một cặp đôi nhưng cũng chỉ dừng ở ôm nhau một tẹo thôi. Thỉnh thoảng đọc lại những câu chuyện trẻ con như vậy, cảm thấy tâm trạng được thả lỏng và suy nghĩ đơn giản hơn nhiều.
5
93328
2015-07-01 10:58:56
--------------------------
216134
8151
Mình thích truyện Miko ở chỗ những nhân vật rất đa dạng, trẻ con nhưng cũng đầy tình cảm. Mặc dù chuyện tình cảm ở lứa tuổi tiểu học thì hơi sớm (Yuko và Kenta), nhưng tác giả không đi quá lố ở vấn đề đó. Thích cái mặt bự chảng của Miko trong truyện.Truyện này đọc cũng vui, nhưng so với truyện cùng thể loại thì nhóc Maruko vẫn hay hơn. 
4
31242
2015-06-27 10:49:43
--------------------------
206259
8151
Trước đây mình chưa hề đọc Miko, chỉ biết đến cô nhóc này qua lời kể đầy hào hứng của nhỏ bạn :) bây giờ, cầm Miko Selection trên tay, đọc rồi mới thấy sao mà đáng yêu quá đỗi!!! Miko và các bạn ai nấy cũng đều dễ thương, hồn nhiên, trong sáng nhưng cũng giàu tình cảm vô cùng. Tình bạn trong truyện Miko rất ấm áp! Nhìn nhóm Miko chơi đùa cùng nhau làm mình cảm thấy ghen tị ghê đi :3 Cặp đôi Tappei - Miko kawaiii không chịu nỗi :D Câu chuyện trường lớp, bạn bè đời thường nhưng đầy ý nghĩa, đầy tỉnh cảm dễ thương khiến người đọc như trôi về tuổi thơ một lần nữa, cảm thấy ngọt ngào mãi không thôi!
5
247818
2015-06-09 10:45:23
--------------------------
144649
8151
truyện Miko selection top 10 love là một truyện rất hay, truyện đã miêu tả những tình cảm trong sáng, đáng yêu của cái lứa tuổi cấp tiểu học - cấp đầu tiên của con đường học vấn. Truyện đã giúp tôi giải tỏa những căng thẳng, áp lực trong việc học bằng những tiếng cười. Truyện thật sự rất lôi cuốn tôi cũng như các độc giả khác. Một lần nữa tôi có thể khẳng định rằng : Nếu như bạn muốn tìm một cuốn truyện để đọc giải trí thì thật sự Miko selection top 10 love rất đáng để cho bạn đọc. Hi vọng các bạn cũng có thể giải tỏa áp lực bằng việc đọc Miko :)
5
475692
2014-12-27 15:50:24
--------------------------
126863
8151
Miko-Tappei couple siêu dễ thương luôn. Đọc truyện "Nhóc Miko", mình thích nhất những câu chuyện về Miko và Tappei ấy. Mỗi lần mua truyện mới về là mình lại tìm mẫu truyện nào về Miko và Tappei đọc trước rồi đọc những mẫu truyện khác sau. Chẳng hiểu sao mỗi lần đọc những câu chuyện về Miko và Tappei mình lại cười khúc khích một cách sung sướng thế không biết. Thậm chí khi đọc "Nhóc Miko" mình còn có cảm giác mong chờ, hồi hộp, háo hức hơn cả đọc truyện ngôn tình nữa. Thật ngạc nhiên rằng "Nhóc Miko" là truyện tranh dành cho lứa tuổi thiếu nhi nhưng lại có những câu chuyện tình cảm còn tự nhiên, lãng mạn, đáng yêu hơn cả những câu chuyện của người lớn. Thú thật khi đọc "Nhóc Miko", mình cũng có chút ganh tị vói tình cảm của Miko và Tappei đó nha. Hic. Thật tuyệt vời khi có một cuốn truyện tập hợp những câu chuyện và Miko và Tappei thế này!
4
106483
2014-09-21 09:01:31
--------------------------
125545
8151
Đây là tuyển tập những truyện tình cảm của Miko. Vốn rất thích những câu chuyện tình cảm của Miko và Tappei nên tôi đã mong chờ một tuyển tập như thế này từ lâu. Dù đã đọc hầy hết những mẩu truyện này nhưng khi cầm cả quyển chỉ nói riêng về Miko và Tappei, tôi vẫn thấy rất vui và háo hức. Đọc lại nhưng vẫn có cảm giác rất hay, hơn nữa sau mỗi truyện đều có lời bộc bạch của tác giả về ý tưởng truyện. Tôi cực thích câu truyện về giáng sinh, lễ tình nhân và chuyện khi Miko lên cấp 3, dù không còn là thiếu nhi nhưng tôi vẫn ước có một người như Tappei. Miko rất may mắn đấy. Những câu chuyện của Miko làm tôi cảm thấy yêu đời, hơn đứt nhiều câu chuyện tình yêu sến súa thời nay. Những tình cảm trong truyện trong sáng nhưng vẫn thật lãng mạn, đáng yêu kinh khủng và cũng tự nhiên vô cùng. Đó cũng là lí do tôi yêu thích Miko. Cũng mong những tập sau sẽ có nhiều tình huống tình củm hơn nữa :))
5
68745
2014-09-13 23:14:20
--------------------------
