455682
5657
Một nhân vật thám tử học sinh khá cá tính đó là Hattori vùng Osaka đã xuất hiện trong tập này. Hattori da ngăm đen, giỏi suy luận và tính cách khá giống Shinichi, tới Tokyo để tìm Shinichi so tài nhưng cuối cùng bại trận và thừa nhận với Shinichi: "Tớ thua cậu hẳn một bậc". Trong tập này thì cũng chính nhờ Hattori cho Conan uống rượu thuốc mà Conan mới có thể trở lại thành Shinichi. Có thể nói đây là tình tiết mấu chốt cho tất cả các nhân vật khác xuất hiện sau này.
Tiếc rằng lần nào Shinichi muốn nói với Ran thì đều bị teo nhỏ không đúng thời điểm tí nào.
5
23847
2016-06-22 19:51:08
--------------------------
451177
5657
trong tập này chàng thám tử miền Tây Heiji Hattori xuất hiện rồi kìa!! Khi đọc lại tập này mình thấy Heiji lần đầu xuất hiện dễ thương biết mấy! Rồi một sự việc bất ngờ khi thứ rượu của Heiji lại khiến Conan trở về hình dạng thật Shinichi, có thể nói trong tập này hội tụ cả hai anh chàng đẹp trai thế này mà không mua đọc thì cũng uổng. Hơi tiếc một chỗ là mình cứ tưởng Shin sẽ trở về mãi mãi nhưng ai ngờ loại rượu này chỉ có tác dụng lần đó, thật buồn.
5
362041
2016-06-19 14:49:43
--------------------------
386502
5657
Thám tử̀ lừng danh Conan tập 10 (tái bản 2014) quá xuất sắc hấp dẫn lôi cuốn vô cùng, mỗi tập lại là những mẫu chuyện khác nhau với tình tiết hấp dẫn khác nhau nhưng không quá trùng lập không quá khô khan tác giả đã khắc họa được nét tính cách chững chạc của cậu học sinh cấp 3 trong thân xác của cậu bé học sinh tiểu học, sự thông minh, bình tỉnh sử lí tình huống của Conan đã góp phần làm nên sự xuất thần của nhân vật và sự hấp dẫn của truyện.Tập 10 xuất hiện một nhân vật mà tôi rất thích trong bộ truyện, đó là Heiji, cậu đã so tài cùng với Shin và mắc phải sai lầm . Rất cảm ơn tác giả đã dày công để sáng tác ra một tuyệt phẩm như thế này!
5
853243
2016-02-25 19:46:31
--------------------------
378884
5657
mình thấy hay khi tác giả chỉ cho thám tử Conan trở về trong chốc lát chứ nếu trở về luôn thì đâu còn hay nữa vì còn nhiều vụ án li kì vô cùng. cứ nghĩ chỉ có mỗi Shinichi là thám tử học sinh trung học thôi còn có cả hattori Heji cũng tài năng không kém. tội cho Ran Mori lo lắng cho Thám tử Conan mà không hề biết là Shinichi thật. kể cũng lạ sau này Haibara nghiên cứu loại rượu thuốc kia cũng chỉ phát minh ra thuốc chữa tạm thời chứ không triệt để được. thành ra thám tử Conan còn phải sát cánh bên đội thám tử nhí dài dài...
5
568747
2016-02-08 11:01:57
--------------------------
377042
5657
Mình rất thích những tập conan trở lại thành shinichi và tập này là một trong số đó không ngờ chỉ vì uống rượu Bạch cốt nhi mà conan lại trở lại shinichi nhìn oai phong vô cùng khiến cho tất cả mọi người đều ngạc nhiên và cả mình cũng vậy Shinichi tự nhiên xuất hiện với dáng vẻ hơi kiêu nhưng nhìn vô cùng oai và đẹp trai nữa tiếc là chỉ được có một lúc thôi à sau lại trở thành conan và sau đó conan muốn trở thành shinichi nên lén lút uống rượu rồi bị say bí tỉ tội nghiệp nhưng trông lúc đó conan đáng yêu lắm.
5
1102669
2016-02-01 15:14:27
--------------------------
330869
5657
Cách thức giết người và giấu xác rất thú vị : Trên thang máy có cái xác đố ai tìm được ?
Giết người trước mặt mọi người cũng là cao thủ rồi. Nhưng có hơi dại và quá chủ quan khi đi tìm nhân chứng cho mình là thám tử tài ba. Lẽ ra tìm bạn bè là được rồi. 
Hattori xuất hiện đẹp trai, bản lĩnh, nhanh trí. Mình cũng yêu cậu ấy nhiều như Conan vậy. 
Càng về sau thì càng có nhiều nhân vật chứ mỗi Conan thì sẽ chán. 
Mình không thấy câu "Chỉ có một sự thật" hay. 
5
535536
2015-11-03 14:29:13
--------------------------
279363
5657
Đây là lần đầu tiên Conan biến trở lại thành thám tử trung học Shinichi Kudo cũng nhờ thứ rượu thuốc của một anh chàng thám tử từ Osaka. Vậy là truyện lại có thêm nhân vật mới, chàng thám tử Heiji Hatori này chắc chắn sẽ là kì phùng địch thủ với Shinichi đây. Tập 10 này có cả sự xuất hiện của đám nhóc lớp 1B, vụ án cũng rất thú vị. Cứ tưởng đây là lần cuối Conan sát cát bên tụi nhóc chứ đáng tiếc cậu chàng lại bị nhờn thuốc và vẫn là 1 đứa nhóc :)) Vụ án cuối lại kêt thúc ngay khúc Conan đã biết được hung thủ là ai và sắp phá án, thật gây cấn!
4
471112
2015-08-26 18:29:31
--------------------------
241740
5657
"Suy luận không phải để phân thắng thua hay so hpn kém, bởi vì, sự thật luôn chỉ có một", tôi thực sự rất thích câu nói này của Shin trong tập này. Tập 10 xuất hiện một nhân vật mà tôi rất thích trong bộ truyện, đó là Heiji, cậu đã so tài cùng với Shin và mắc phải sai lầm. Tôi thích tập 10 vì tập 10 có sự trở lại của Shinichi, sự xuất hiện của Heiji, viecj so tài giữa hai người, câu nói của Shin, đôi chút tình cảm của Shin Ran và thêm vào đó còn là một yếu tố, một sự việc: việc Shin uống rượu cao lương giúp Shin trở về hình dáng cũ nhưng chỉ dùng được trong 1 lần. Tôi nghĩ đây là một yếu cố quan trọng, vì sau đó nhờ chuyện này mà Ai được cứu sống mà.
5
412050
2015-07-25 20:10:32
--------------------------
219553
5657
Đây là tập có thể nói là rất hay, các nội dung của tập liên quan đến nhau, sự xuất hiện của Shinichi rất ấn tượng. Sự xuất hiện của 1 thám tử miền tây Hattori Heiji đã khiến Shinichi xuất hiện, màn phá án đầy kịch tính của 2 người, rất hồi hộp. 
Conan đã trở lại hình dáng Shinichi của mình nhưng chỉ được có một lúc, đây là 1 sự nuối tiếc đối với những fan chuyện Conan. Còn vụ án ở thư viện cùng đội thám tử nhí đối đầu với nguy hiểm, đầy kịch tính đối với kẻ sát nhân. Tập truyện kết thúc với vụ án đầy bí ẩn, hứa hẹn 1 câu chuyện tiếp theo đầy hấp dẫn.
5
564406
2015-07-01 16:46:16
--------------------------
194107
5657
Những tập đầu của Conan tập trung giới thiệu những nhân vật mới. Tập này giới thiệu cho chúng ta biết về Heiji - thám tử trung học tới từ phía Tây, có thể nói ngang sức ngang tài với Shinichi. Vụ án diễn ra trước mắt 2 người, và cả hai cùng đi tìm sự thật, tìm xem ai là hung thủ thực sự. Cuối cùng Heiji mắc sai lầm dẫn tới tìm nhầm hung thủ, đúng lúc đấy Kudo xuất hiện. Anh chỉ ra những sai lầm trong suy luận của Heiji, và đưa ra những bằng chứng và suy luận đầy thuyết phục của mình. Ấn tượng với câu nói và ánh mắt của Shin: " Suy luận không phải để phản thắng thua vì sự thật chỉ có một mà thôi". Dù bị trở về lại làm thằng nhóc Conan nhưng sự xuất hiện của Shin có lẽ phần nào an ủi được trái tim của ran. Một tập truyện hay.
4
466113
2015-05-09 15:16:38
--------------------------
186563
5657
Thấy Hattori heiji cũng đẹp trai mà mỗi tội da hơi đen.Chân lí của conan: "Suy luận không phải để phân thắng thua hay so hơn kém vì sự thâtj luôn chỉ có mọt mà thôi" đã làm cho Hattori Heiji khá đau đầu cứ nghĩ Shinichi sẽ chở về hình dạng cũ chứ.Híc,Nhưng không sao có thế thì sori Thám tử lừng danh conan này mới tiếp được.Chị Ran mori em xin lỗi nhưng em không muốn Shinichi quay về lắm đâu.Em sợ chuyện kết thúc lắm.Mong rằng Tác giả Gosho Aoyama sẽ cho ra những vụ án hay hơn nữa.
5
457454
2015-04-21 14:02:34
--------------------------
184318
5657
Thám tử lừng danh Conan tập 10 đánh dấu tình bạn chân thành của Shinichi và Hatori chính thức bắt đầu. Tập này khiến mình hơi hụt hẫng vì cuối cùng Conan lại hoàn Conan. Nhưng không sao, cuối cùng Ran lại được gặp Shinichi sau một thời gian dài xa cách.Chỉ tiếc là họ chưa nói được gì nhiều với nhau. Các tình tiết của các vụ án trong tập này nhẹ nhàng, khá hấp dẫn. Mình thật khâm phục tài năng viết truyện và "câu khách" của tác giả Aoyama. Hầu như mỗi lần cầm cuốn truyện Conan trên tay là mình không thể rời mắt khỏi nó.
4
558345
2015-04-17 15:52:29
--------------------------
175734
5657
Vẫn tưởng rằng chai rượu thuốc sẽ giúp mình trở lại hình dáng cũ, ai ngờ làm Conan say bí tỉ. Trong tập này, mình thích nhất là vụ án trong thư viện, coi mà hồi hộp quá trời luôn. Hình như tác giả Gosho Aoyama hơi ưu ái cho bọn thám tử nhí thì phải, thấy xuất hiện hoài luôn nhưng hay lắm và mình rất thích. Một chàng thám tử đại diện cho miền tây với nước da ngâm lộ mặt một cách bí ẩn, thật là nham hiểm quá đi. Nói chung, tập này mang lại cho mình nhiều hào hứng và hấp dẫn vô cùng, mình mong đợi những tập sau sẽ hay hơn nữa.
5
13723
2015-03-30 21:00:43
--------------------------
170451
5657
đọc tập này đúc kết lại là học được câu nói "suy luận không phải để phân thắng thua hay so hơn kém vì sự thật luôn chỉ có một mà thôi" một câu nói thật ý nghĩa,trang truyện lúc đó cũng thật là đẹp,ánh mắt của heiji ấy không hiểu sao từ lúc đó mình cảm giác như hai người đó không còn rào cản giữa hai thám tử nữa mà đó là tình bạn chân thành.heiji rút ra được một bài học quý giá.
cảm ơn ông,aoyama đã cho shin trở về hình hài cũ nhưng mỗi lần như thế lại càng thấy thương vì cậu không thể nói ra điều mà cậu muốn nói với ran để rồi sau đó cả hai đều đau khổ,thấy ran khóc và buồn thật thương quá!
4
586877
2015-03-20 00:15:50
--------------------------
