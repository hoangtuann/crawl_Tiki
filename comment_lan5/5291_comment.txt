238501
5291
Một cuốn nhật ký thú vị để con gái 8 tuổi của mình cùng chia sẻ những trải nghiệm thú vị trong cuộc sống hàng ngày ở nhà và ở trường với cô bé Lê Na.
Những mẫu chuyện ngắn, dễ đọc, rất dễ thương, đôi chỗ hài hước và cả người lớn cũng có thể đọc được.
Những trải nghiệm rất thật, rất gần gũi và nhiều cảm xúc của cô bé Lê Na chắc chắn sẽ nuôi dưỡng những cảm xúc tích cực ở con tôi và nuôi dưỡng thói quen viết nhật ký của bé!
Sách in màu, hình ảnh đẹp, giấy tốt! Một cuốn sách có thể đọc đi đọc lại để thư giãn.
5
292190
2015-07-23 11:07:14
--------------------------
