270959
3892
One piece là bộ truyện nổi tiếng không chỉ ở Nhật Bản mà còn rất nhiều nước khác. Truyện kể về Luffy, một người có ước mơ được trở thành vua hải tặc, luôn ngưỡng mộ Shanks. Lúc nhỏ từng ăn trái ác quỷ nên cơ thể biến thành cao su...
Trong tập trước nhóm của Luffy đã xâm nhập bất hợp pháp vào đảo trên trời nên phải chịu những trừng phạt của 4 vị thần quan. Và trong tập này, họ đã chiến thắng thử thách ngọc, sẵn sàng cho những trận thử thách tiếp theo đầy khó khăn của ba vị thần quan còn lại. Cùng với đó là sự trả thù thần Enel của Shandia... Câu chuyện đang ngày càng trở nên hấp dẫn.
5
462545
2015-08-18 19:06:23
--------------------------
229077
3892
Trái ác quỷ và luffy mũ rơm chưa bao giờ hết hot, bộ truyện tranh đã chinh phục hầu hết các bạn trẻ trên toàn thế giới. Những cuộc đối đầu của băng hải tặc mũ rơm luôn khiến tôi mong chờ. Thú vị, hồi hộp, hài hước và gay cấn là tất cả những gì tác giả đã thể hiện. Ngoài ra, chẳng có cuộc đấu tranh nào là vô nghĩa, ai cũng có mục đích cao cả của mình, bởi vậy nên đôi khi tôi thấy cả sống mũi cay cay. Truyện đề cao tinh thần, ý chí đấu tranh, tình bạn, tình yêu, và đủ thứ tình trên đời. Chắc chắn đọc cuốn truyện này ai cũng cười và đều mong một trái ác quỷ.
5
6502
2015-07-16 14:19:45
--------------------------
218797
3892
Sau tập 26, tập 27 tiếp theo là diễn biết chuyến phưu lưu... trên trời của cả nhóm Mũ Rơm. Vượt qua được thử thách của một thần một cách khó khăn. Cứ tưởng Luffy phải đối đầu với vị thần khác và tốn nhiều thời gian hơn nhưng thật bất ngờ , họ lại tới chỗ của nhóm còn lại một cách khá nhẹ nhàng. Tuy nhiên Chopper phải đối đầu với sự nguy hiểm thật sự. Qua từng trang giấy, ta lại thấy được tinh thần của băng hải tặc Mũ Rơm thật đáng ngưỡng mộ. Cuộc chiến hiện giờ không chỉ còn là của hai thế lực mà là của 3 thế lực. Mỗi trang giấy được lật qua lại càng làm người đọc mong đợi trang sau cho tới khi đặt quyển sách xuống và mong đợi tập tiếp theo.
5
576680
2015-06-30 21:34:40
--------------------------
193362
3892
One piece thực sự là một hành trình tuyệt vời, có vô số phân cảnh cười ra nước mắt nhưng chắc chắn những câu chuyện cảm động mới là thứ mà fan OP không thể nào quên. Từ những câu chuyện thủa ấu thơ đầy thương đau của mỗi thành viên băng Mũ Rơm (đặc biệt là Nami, Robin, Franky, Chopper...), đến cảnh Luffy và Ussop đánh nhau và Going Merry nói lời chào tạm biệt, cảnh Robin hét lên ước mơ được sống của mình và Luffy tuyên chiến với chính quyền thế giới để bảo vệ Robin, cảnh Zoro sẵn sàng chấp nhận cái chết, lấy thân mình chịu đòn của Kuma thay Luffy, cảnh Râu Trắng hy sinh trong danh dự và hồi tưởng về cuộc nói chuyện của ông với vua hải tặc, đặc biệt là cái chết của ACE đã gây cho độc giả không biết bao nhiệu sự xót thương tiếc nuối hay gần đây nhất là chap về tuổi thơ của Law... Tất cả nêu trên đều là những cảnh thường xuyên được mọi người nhắc đến và yêu thích. Riêng tôi lại đặc biệt ấn tượng với arc về đảo trên trời :). Có lẽ cũng không phải vì lý do đặc biệt nào, chỉ là nó đã cảm đến tôi. Một câu chuyện dài về những lời hứa: lời hứa của 2 người bạn, lời hứa của con cháu với tổ tiên, lời hứa của loài vật với chủ nhân của nó, lời hứa giữa những con người vừa mới gặp nhau... :). Đẹp, thật đẹp. Lời hứa dù đã trải qua hàng trăm năm hay chỉ vừa mới ngày hôm qua thì cuối cùng nó cũng đã được thực hiện, đó là điều tuyệt vời nhất...
Cùng chờ đợi những tập sau để cảm nhận những điều ý nghĩa nhé! :)
5
312203
2015-05-07 12:49:07
--------------------------
190776
3892
Sẽ là những pha kịch chiến hài hước vui nhộn với thần quan, cuộc chiến ngầm giữa hay phe đối  đầu từ vài trăm  năm trước nay đã chính mở màn. Trong tập này hé lộ về đảo vàng của Nolan có tồn tại??! Thân phận của  "kị sĩ bầu trời"?, nhóm luffy đang tiếng đến 1 trong những trận chiến cực khủng, đối đầu với những thế lực không tưởng của Skypea. Những khúc dạo đầu trong tap này sẽ là bước chuẩn bị cho những pha cận chiến của tập tiếp theo. Càng đọc One Piece, nguời đọc sẽ càng thấy khả năng tưởng tượng và sáng tạo khá đỉnh của Oda.
5
166935
2015-04-30 11:00:58
--------------------------
189154
3892
Đây là một arc rất hay trong One Piece. Ở arc này, bối cảnh là một nơi khác biệt so với những nơi khác trong hành trình của Going Merry nên diễn biến hứa hẹn nhiều chi tiết thú vị. Luffy và các bạn sẽ đối đầu với một đối thủ rất mạnh và hoang tưởng- Enel. Cũng qua đây, mình đánh giá cao sự hài hước và sáng tạo của Oda Eiichiro, chắc hẳn bạn phải ngạc nhiên lắm với gương mặt của Enel, nhìn y hệt rapper Eminem luôn đó. Những trận chiến hứa hẹn đày kịch tính và hấp dẫn, hồi hộp lắm. Mong tập sau và những trận chiến tuyệt vời khác ♥
5
249905
2015-04-26 17:40:35
--------------------------
