478526
3652
Biết được sách này quá một người bạn, mới đầu thấy không hứng thú với mấy chuyện chuyển thể từ phim lắm, nhưng thấy thằng bạn khen nức nở cũng tò mò, rồi mùa luôn *cười*.
Câu câu chuyện phiêu lưu đầy khắc nghiệt số với đứa trẻ mười bốn tuổi với cái kết đầy ý nghĩa.
Lần đầu tiên mình đọc xong một cuốn sách chỉ trong vòng một ngày mà không bỏ sót từ nào, câu chuyện cực kì lôi cuốn, rất đáng để đọc.
Đã vậy Tiki giáo hàng rất nhanh, gói kĩ càng, shipper nhiệt tình, rất vui vẻ.
5
593076
2016-08-03 00:45:54
--------------------------
453016
3652
Nhìn bìa sách thôi cũng đã hấp dẫn mình trong nháy mắt rồi.Nội dung gay cấn, hay và độc đáo, chất lượng giấy cũng ổn và khá tốt.Không uổng với số tiền mình bỏ ra và công sức mình ngồi đọc :v . Người đọc có thể hình dung được cái kết đầy gay cấn này. Câu chuyện phiêu lưu đầy ý nghĩa, kết thúc đầy tính sâu sắc. Nói chung đây là cuốn sách mà mình nhìn lần đầu tiên đã thấy yêu rồi, cứ không muốn nó có hồi kết luôn ấy hihi :)) Mong tác giả ra nhiều cuốn sách hay như thế này để các bạn độc giả đọc và cảm nhận ~~
5
1324044
2016-06-20 22:49:54
--------------------------
419554
3652
Đã lâu không ngấu nghiến hết quyến sách chỉ trong một ngày, cuộc phiêu lưu thú vị mà mình thấy là khắc nghiệt với đứa trẻ 14 tuổi, bởi mọi chuyện xảy ra như quá sức với lứa tuổi ấy. Ấy vậy mà nhân vật Oskari lại mạnh mẽ vượt qua với ý trí phi thường, không ngại khó khăn và không bỏ rơi người khác chỉ vì mạng sống của mình.
Câu truyện phiêu lưu đầy ý nghĩa, kết thúc đầy tính nhân văn, và làm mình rất tâm đắc với câu "Rừng là vị quan tòa nghiêm khắc. Nó ban cho ta những gì chúng ta đáng nhận được"
5
1073055
2016-04-21 15:19:47
--------------------------
344115
3652
Thiết kế sách đệp mắt với chất lượng giấy tốt.đọc truyện của tác gia này, mình cảm nhận thấy rất thú vị. Tình tiết khá cuốn hút, nhân vật được xây dựng rất hợp với tuổi mới lớn.  Một lễ trưởng thành đặc trưng của một vùng đối với môt nhóc (nhân vật chính) tưởng chừng đơn giản và ảm đạm nhưng không, nó lại trở thành một cuộc trinh thám đầu đời không thể nào quên. Với việc cứu được Tổng thống của USA cậu đã mang lại vinh dự cho cả gia đình và chính bản thân khỏi sự kinh thường của dân làng.Hay hay rất rất hay. càng đọc mà càng thấy hào hứng với nhân vật, truyện được khuyến mại -25% tuy là phí giao hàng cộng vào là bằng giá gốc nhưng mình cũng cảm thấy rấ hài lòng rồi. Thanks tiki nhiều, hy vọng tiki sẽ có thêm nhiều truyện hay hơn nữa.
4
587765
2015-11-27 20:23:05
--------------------------
318055
3652
Oskari từ một người không kéo nổi cây cung, trong lễ trưởng thành còn xảy ra những tình huống dở khóc dở cười làm cho cậu ấy càng tức tối và xấu hổ hơn. Chẳng ai trong làng có thể ngờ được người mà mình đã cưới hôm trước ấy nay lại trở thành một anh hùng cứu Tổng thống thoát khỏi sự truy sát của tên "biến thái" có ý định quay clip cảnh giết ông và lột da nhồi bông... hai người trở thành bạn bất chấp tuổi tác. Oskari quay về với "con mồi" mà mình săn được và làm người bố của mình hãnh diện.
Tuy người đọc có thể đoán được kết quả nhưng nội dung vẫn rất gay cấn. Những lúc tưởng rằng sẽ từ bỏ nhưng lại cố vượt qua, bản năng sinh tồn trỗi dậy mạnh mẽ hơn bao giờ hết khi người ta ở ngưởng cửa cái chết.
Bìa sách đẹp, giấy cũng khá ổn.
4
566337
2015-10-04 22:32:41
--------------------------
230998
3652
Một lễ trưởng thành đặc trưng của một vùng đối với môt nhóc (nhân vật chính) tưởng chừng đơn giản và ảm đạm nhưng không, nó lại trở thành một cuộc trinh thám đầu đời không thể nào quên. Với việc cứu được Tổng thống của USA cậu đã mang lại vinh dự cho cả gia đình và chính bản thân khỏi sự kinh thường của dân làng.
Lần đầu đọc truyện của tác gia này, mình cảm nhận thấy rất ổn. Tình tiết khá cuốn hút, nhân vật được xây dựng rất hợp với tuổi mới lớn - thanh niên.
Thiết kế sách đệp mắt với chất lượng giấy tốt.
5
334680
2015-07-17 20:56:51
--------------------------
