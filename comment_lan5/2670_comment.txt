410250
2670
Một cuốn sách tuyệt vời, nó cuốn hút tôi từ đầu đến cuối. Giống như trong cuốn tản văn trước của chị là Vợ Đông chồng Tây, giọng văn của Kiều Bích Hương vẫn rất hấp dẫn và các câu chuyện kể của chị cũng rất đời, rất chân thật, xen lẫn những chuyện vụn vặt trong cuộc sống ở cả xứ người và Việt Nam. Có những chuyện vui, cũng có những chuyện đem lại cảm giác buồn man mác, và cũng không thiếu những câu chuyện có thể làm cho chúng ta suy ngẫm. Thực sự đón đợi những tác phẩm tiếp theo của chị.
5
17740
2016-04-03 20:13:08
--------------------------
309429
2670
Một người bạn của mình gợi ý mình đọc quyển này. Ban đầu lừng khừng vì vốn hay đọc văn học thuần túy, những cuốn sách gần như mang hơi hướm giọng báo chí của một nhà báo có nghề như chị mình hơi ngại vì nghĩ sẽ khó ngấm. Vậy mà khi đọc mình thấy đồng cảm những câu chuyện chị từng bước trải qua. Ở xứ người với những đứa con mang hai dòng máu. Những nỗi buồn man mác, những nhớ nhung, những niềm vui, niềm hạnh phúc tất cả quyện lại và lắng đọng trong mình rất lâu, rất lâu. 
5
101774
2015-09-18 23:38:48
--------------------------
303070
2670
Vô tình biết đến Kiều Bích Hương qua cuốn sách "Vợ Đông chồng Tây" từ năm trước, đọc say mê cuốn sách của chị, nên khi thấy chị xuất bản sách mới, mình đã cố gắng mua ngay. Đọc những tác phẩm như thế này về cuộc sống của những người phụ nữ làm dâu xứ người, làm vợ, làm mẹ của những đứa con hai dòng máu, mình chợt thấy lòng chùng xuống, buồn man mác mà không hiểu vì sao. Quả thực với giọng văn của một "nhà báo có nghề", chị đã truyền cảm hứng rất nhiều cho người đọc. Có người đọc vì tò mò, hoặc vì sự đồng cảm, có chung cảnh ngộ làm dâu xa xứ... Nhưng dù thế nào đi nữa, mình vẫn cảm thấy trong niềm hạnh phúc, niềm vui làm vợ, làm mẹ giữa trời Âu ấy của chị, vẫn có một tấm lòng của người con gái xa quê, nhớ nhung hoài niệm. Thậm chí còn bắt gặp những mảnh đời khác nhau, những tâm sự khác nhau, thấy được một góc rất Việt trong lòng châu Âu xa xôi ấy. Sự đan xen giữa kí ức và thực tại, giữa những câu chuyện, giữa những số phận, được phác họa tài tình qua ngòi bút tinh tế mà giản dị của chị, đủ làm mình muốn đọc mãi không ngừng, và đôi khi lại bật cười vì những câu chuyện chị kể, nhỏ thôi mà đáng yêu quá!
5
366064
2015-09-15 17:17:04
--------------------------
293423
2670
Đã từng đọc tác phẩm đầu tiên của Kiều Bích Hương viết cho ngày đầu xa xứ Vợ Đông, Chồng Tây và cực thích văn phong của chị cũng như góc nhìn của chị về những cuộc hôn phối của chồng Tây và vợ Việt. Thế nên tôi đã rất hào hứng đón nhận tác phẩm tiếp theo này để biết thêm về đời sống nơi xứ lạ dưới góc nhìn có chiều sâu của nữ nhà báo. Những câu chuyện trong tác phẩm được viết dưới ngòi bút thổn thức đầy cảm xúc của trái tim người phụ nữ. Xuyên suốt tác phẩm ta bắt gặp những mảnh đời và cả cuộc sống nơi xứ người thật gần gũi, thân quen như vẫn thường diễn ra trên đất nước chúng ta. Và tôi cảm nhận rằng con người ở bất cứ đâu trên trái đất này, dù khác màu da, tôn giáo, văn hoá cũng đều có chung tính nhân văn và diễn biến tâm lý cảm xúc như nhau. Cảm ơn Kiều Bích Hương đã mang đến cho độc giả một tác phẩm hay với góc nhìn sâu sắc của một nhà báo nữ.
4
552988
2015-09-08 12:24:22
--------------------------
