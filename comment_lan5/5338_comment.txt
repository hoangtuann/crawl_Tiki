435267
5338
Cuốn sách 500 bài thuốc hay chữa bệnh theo kinh nghiệm dân gian cung cấp cho chúng ta những bài thuốc hay và rất hữu ích về rất nhiều bệnh khác nhau. Chỉ từ những cây cỏ những con vật rất gần gũi trong cuộc sống hàng ngày. Tuy nhiên hình như tác giả là người miền ngoài nên cũng viết tên các loại thuốc theo miền ngoài nên nhiều loài mình cũng không biết nó là cây gì lun. Mình cũng nghỉ là tác giả nên trình bày theo quyển sách theo bệnh thì sẽ dễ tìm kiếm hơn.
3
958015
2016-05-24 20:40:48
--------------------------
350453
5338
Là người Việt Nam, hẳn từ nhỏ ai cũng có đôi lần sử dụng những bài thuốc nam mỗi khi cảm hay sốt nhẹ. Bởi vậy, việc làm giàu thêm tri thức về những cây thuốc quanh ta là rất cần thiết, vừa đơn giản tiện dụng lại không độc hại. Cuốn sách được trình bày đơn giản, trực quan theo từng loại cây thuốc, cách pha chế và tác dụng của nó đối với từng bệnh. Tuy nhiên nên có thêm một phần tra cứu theo tên bệnh và số trang mà bệnh đó xuất hiện thì sẽ hay hơn.
5
1037281
2015-12-10 17:36:23
--------------------------
276281
5338
Là người Việt Nam, hẳn từ nhỏ ai cũng có đôi lần sử dụng những bài thuốc nam mỗi khi cảm hay sốt nhẹ. Bởi vậy, việc làm giàu thêm tri thức về những cây thuốc quanh ta là rất cần thiết, vừa đơn giản tiện dụng lại không độc hại. Cuốn sách được trình bày đơn giản, trực quan theo từng loại cây thuốc, cách pha chế và tác dụng của nó đối với từng bệnh. Tuy nhiên nên có thêm một phần tra cứu theo tên bệnh và số trang mà bệnh đó xuất hiện thì sẽ hay hơn.
4
94449
2015-08-23 20:39:24
--------------------------
