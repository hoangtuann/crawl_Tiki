262333
7822
Những cuốn sách  như hạt giống tâm hồn không còn là quá xa lạ với chúng ta nữa. Tuy nhiên, với mình đây thực sự là một cuốn sách đặc biệt. Qua những trang sách ấy, mình như cảm nhận rõ, nhìn thấu, hiểu sâu hơn chính bản thân mình. Những sự đố kỵ, ích kỷ, hiếu thắng mà hằng ngày mình cố tình phớt lờ đi được nhấn mạnh và  làm rõ qua từng con chữ.
Một cuốn sách mà ai cũng nên đọc để hiểu rõ bản thân mình hơn và để sống tốt đẹp hơn.
5
145385
2015-08-11 20:51:42
--------------------------
221808
7822
Trước khi đọc quyển sách này, tôi cũng đã từng đọc rất nhiều những triết lý hay những bài học khác về sự làm chủ cảm xúc cũng như khiến cuộc sống tâm hồn trở nên tốt đẹp, dễ chịu hơn. Nhưng thật sự thì đâu lại vào đấy, tôi vẫn quay về với những cảm xúc cũ, buồn vì người khác, vui vì người khác, mệt nhọc vì những thứ bên ngoài quá nhiều.
Rồi một việc không mấy dễ chịu đã xảy ra với tôi, ngày nào tôi cũng sống trong cảm xúc buồn chán và bực bội. Cảm thấy mình cần phải làm gì đó để thay đổi tình hình, tôi đã quyết định thử đọc quyển sách này. Thật bất ngờ vì những gì nó mang đến, tôi tự cười chính mình vì những điều ngốc nghếch mình đã mang trong tâm hồn bấy lâu nay. Hiện tại thì tôi có một cuộc sống nội tâm khá vui vẻ và cảm xúc của tôi thì không còn chịu ảnh hưởng từ người khác quá nhiều nữa. Tôi cảm thấy điều đó rất rõ.
Một điều tôi rất lấy làm vui mừng là tôi đã "nghe được tiếng nói lương tâm" của chính mình! (Bạn đừng khó hiểu hay nghĩ rằng tôi từng đánh mất lương tâm, haha không có đâu nhé!). Đó chỉ là một trải nghiệm khi đọc quyển sách này thôi!
Quyển sách rất thú vị, nếu tôi có chỗ nào chưa thông suốt, tôi sẽ không đọc tiếp mà để từ từ nghiệm ra rồi mới tiếp tục đọc. Bạn cũng nên như thế. Và mỗi tháng, hãy dành chút thời gian đọc lại nó nhé!
5
541710
2015-07-04 14:22:57
--------------------------
167245
7822
Đúng như tựa đề của quyển sách “Lăng kính tâm hồn”, nó đã cho chúng ta thấy được tâm hồn của con người, của chính bản thân ta bấy lâu nay cứ chạy theo vật chất, đố kỵ, giận hờn… mà đâu ngờ đó chỉ là một thái độ tiêu cực, chỉ cần thay vào đó bằng một suy nghĩ tích cực mọi chuyện sẽ thay đổi theo chiều hướng tốt đẹp.
Quyển sách do cô Trish Summerfield biên soạn tuy nhỏ gọn nhưng chứa đựng rất nhiều kiến thức, đọc và áp dụng ta sẽ thấy cuộc đời thật ý nghĩa. Tôi luôn nhớ một câu trong quyển sách “khi tặng cho ai món quà, bạn và người nhận ai hạnh phúc hơn”. Cuộc sống là trao đi, đừng nghĩ là mình gom nhóm về mình, cuối cùng sẽ không được gì cả. Đó là quy luật nhân – quả mà trong quyển sách có đề cập tới.
5
204853
2015-03-14 12:49:31
--------------------------
