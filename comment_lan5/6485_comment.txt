432552
6485
Qua lời giới thiệu của bạn bè, mình đã quyết định tìm đến và đọc quyển này.Tìm thấy ở quyển này là một motip khá quen thuộc được chắp nhặt từ nhiều câu chuyện cổ tích, thiếu nhi ngắn nhỏ. Có điều hiện lên ở đây, qua lời văn của Trương Thanh Thuỳ, là một bức tranh thế giới động vật sinh động với cách kể truyện tự nhiên, mạch truyện trôi trảy, logic cũng như để lại những bài học ý nghĩa về cuộc sống. Quyển sách này đã cho tôi thấy được một góc nhỏ của xã hội: sự ác liệt vì miếng cơm manh áo.
3
641971
2016-05-19 12:09:47
--------------------------
137162
6485
Một câu chuyện về thế giới động vật mang lại nhiều bài học và suy ngẫm. Chỉ đơn giản là cái nhìn của những chú chuột về thế giới của chúng và thế giới của chính loài người chúng ta, nhưng đã mang lại nhiều ý nghĩa. Có lẽ tôi chưa bao giờ tự hỏi nếu mình là một con chuột nhỏ bé thì sẽ nhìn loài người ra sao và suy nghĩ gì về những hành động thường ngày của con người. Câu chuyện đã giúp tôi có cái nhìn mới hơn về cuộc sống.  Không những thế, những chi tiết trẻ con đáng yêu cũng đưa tôi phần nào về với tuổi thơ của chính mình.
4
42829
2014-11-24 22:46:49
--------------------------
