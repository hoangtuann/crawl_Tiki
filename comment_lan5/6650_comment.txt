472616
6650
Cám ơn MC Books và Tiki vì cuốn sách này. Tìm sách tiếng Nhật càng lên trình độ cao thì càng ít. Sách của MC Book nói chung chất lượng in rất tốt, CD đính kèm chất lượng âm thanh cũng tuyệt vời, bên trong lại có chứ giải song ngữ Nhật - Việt rõ ràng dễ hiểu nữa. Điều duy nhất mình không vừa lòng là sách hơn ngắn và chỉ có mỗi sách luyện nghe thôi, giá mà có thêm sách luyện các kĩ năng khác thì tốt quá. Trên web MC Books vẫn còn nhiều sách tiếng Nhật khác, hi vọng Tiki nhập về thêm để khách hàng có thêm lựa chọn.
4
190393
2016-07-09 20:45:12
--------------------------
455011
6650
Ngoài tiếng Anh, tiếng Hoa thì tiếng Nhật, tiếng Hàn những năm gần đây càng trở nên được nhiều người quan tâm, học tập hơn vì nhu cầu của các công ty có vốn nước ngoài ngày càng tăng. Mình cũng tìm kiếm các tài liệu để học tiếng Nhật, nhưng tài liệu vẫn còn khá ít, không phong phú lắm. Quyển sách này rất bổ ích vì ngoài ngữ pháp, kanji, đọc hiểu thì khả năng nghe -  nói trong việc học ngoại ngữ cũng rất quan trọng. Quyển ôn luyện này trình bày dễ hiểu, các phần luyện tập giúp mình làm quen từ từ với đề thi thật. Có phần dịch tiếng Việt giải thích nên rất tiện lợi cho những ai tự ôn luyện.
4
165179
2016-06-22 12:16:47
--------------------------
448950
6650
Cuốn sách hơi mỏng nhưng kiến thức thì rất đầy đủ, cập nhập chi tiết, hiện đại. Cuốn sách vô cùng bổ ích cho những ai đang học tiếng Nhật. Vừa giúp học chữ Hiragara, Katakan và Kanji lại vừa được học cách phát âm, các mảng ngữ pháp cơ bản rất dễ hiểu, còn trang bị cả những phần để người mua học giao tiếp những câu cơ bản. Tiếng Nhật công nhận là khó nhưng chỉ cần chăm chỉ, cần cù kết hợp với thầy cô và cuốn sách này thì chẳng có gì ngáng đường chúng ta cả.
5
1420970
2016-06-16 19:42:07
--------------------------
378206
6650
Mình mua quyển này từ năm ngoái, đợt thi JLPT tháng 12 vừa rồi mới lấy ra xem. Sao khi xem và học thuộc các mẫu câu trong đó, mình thấy phần nghe của mình đặc biệt là phần cuối của đề thi nghe N3 (9 câu cuối, mỗi câu 1 điểm) tự tin hẳn lên. Mình không nói khi các bạn học quyển này thì khi vào thi sẽ cho giống hệt, nhưng những dạng trong này, bạn sẽ gặp trong đề thi, khi đó bạn sẽ dùng phương pháp loại suy rất dễ khi đã từng làm quen rồi. Quyển này giúp bạn thống kê lại rất nhiều mẫu câu mà bạn đã học và cho bạn tập làm quen. Nếu kết hợp cả các mẫu câu trong Somatome N3 nữa thì mình nghĩ khi thi bạn sẽ không bị liệt phần nghe đâu. Chúc các bạn thi tốt N3 nhé!

5
36249
2016-02-04 17:44:02
--------------------------
338187
6650
Tài Liệu Luyện Thi Năng Lực Tiếng Nhật N3- Nghe hiểu này thật bổ ích đối với mình. Kĩ năng nghe của mình còn hạn chế nên mình mua cuốn sách này để cải thiện. Chất lượng CD( đặt trong vỉ giấy cuối sách) kèm ở sách rất tốt ,giọng đọc dễ nghe, không bị lẫn tạp âm. Sách lại được biên tập theo cấu trúc của đề thi năng lực . Bìa sách đẹp, tên sách lại được in nổi nhìn bắt mắt.Theo mình nghĩ trước khi học cuốn sách này các bạn nên trang bị phần ngữ pháp , từ vựng , kanji... thật vững.Sách này mình nghĩ các bạn có thể tự rèn luyện ở nhà 
5
776785
2015-11-15 16:07:17
--------------------------
307749
6650
Đầu tiên là về hình thức bìa nhé, bìa dày, chữ nổi nhìn rất thích và giúp sách có vẻ hơi sang chảnh. Đĩa CD được để đàng hoàng trong vỉ giấy. Về nội dung thì rất chi tiết và cụ thể. Đưa ra cách rèn kĩ năng khi nghe, như kĩ năng khái quát vấn đề, kĩ năng nắm bắt các điểm chính,… sau lý thuyết của mỗi kĩ năng là bài tập áp dụng ngay. Thậm chí các phần giải thích đều là song ngữ Nhật – Việt nên rất dễ hiểu. Cuối sách còn có bài thi mẫu mô phỏng phần thi nghe của N3. Sau đó là phần đáp án. Giọng người nói trong CD cũng rất dễ nghe, chứ không như 1 vài sách luyện nghe khác, giọng không rõ ràng, nghe thấy chói tai và khó chịu. Nói chung đây là sách N3 mà mình hài lòng nhất từ trước đến giờ. Tiếc là không có sách luyện các kĩ năng khác như ngữ pháp, Kanji, đọc hiểu, nếu có mình cũng “hốt” cả bộ luôn rồi.
5
205174
2015-09-18 11:38:47
--------------------------
301488
6650
Hiện nay nhu cầu nhân lực biết tiếng Nhật đang tăng cao, các công ty tuyển người có khả năng tiếng nhật rất nhiều, do đó mình đã tập trung đầu tư cho tiếng Nhật ngay bây giờ. Tuy nhiên trên thị trường còn hiếm tài liệu tiếng nhật, được một người bạn giới thiệu cho quyển này nên mình mua ngay. Đĩa CD cho âm thanh rõ, giọng đọc tốt, nghe rất dễ hiểu. Ngoài ra các bài học cũng rất có ích, cấu trúc rõ ràng. Ngoài ra còn có những chỉ dẫn, tham khảo dễ hiểu và súc tích. Đây là một tài liệu hay cho bạn nào muốn nâng cao kỹ năng nghe tiếng Nhật
4
582958
2015-09-14 17:51:25
--------------------------
298304
6650
đối với những bạn học tiếng Nhật đặc biệt là những bạn cảm thấy khả năng nghe chưa ổn bạn nên sử dụng cuốn sách này. Giọng đọc rất tốt, không quá khó nghe. những bài nghe có cấu trúc giống như đề thi năng lực Nhật ngữ, khiến bạn không bỡ ngỡ khi đi thi gặp những bài nghe khác nhau. Ngoài ra sách còn có chú thích đáp án rất rõ ràng, dễ hiểu, phù hợp cho việc tự học hay tự luyện thi ở nhà điểm cộng thêm vào là sách có giấy khá dai và dày, không sợ dễ bị rách như những cuốn sách làm bằng giấy xốp khác 

không thấy có điểm trừ cho cuốn sách này

5
93717
2015-09-12 16:41:07
--------------------------
290350
6650
Cũng giống như tiếng Anh, việc học tiếng Nhật mà không chú tâm luyện nghe thì không có khả năng giao tiếp tốt. Mình cũng từng kiếm sách luyện thi ở nhà sách. Nhưng loại sách luyện thi nghe tiếng Nhật là khá ít thấy. Được biết MC Books có xuất bản và bán trên Tiki, nên mình đã tìm mua. Tài liệu luyện thi tiếng Nhật N3 này là một cuốn sách hay và tốt cho những bạn muốn nâng cao kỹ năng nghe tiếng Nhật, giúp tự tin trong kỳ thi năng lực tiếng Nhật N3. Sách có nội dung chủ đề, chữ viết rõ ràng.  Với CD kèm theo và giọng đọc tốt, cuốn sách rất phù hợp cho việc tự luyện thi ở nhà. 
5
575705
2015-09-05 11:56:51
--------------------------
265248
6650
Bé nhà mình đang học tiếng nhật chuẩn bị cho kì thi năng lực nhật ngữ N3 sắp đến, bé lại yếu phần nghe, mà mình tìm mãi không thấy một quyển sách nào bằng tiếng việt để luyện cả ( bé cũng yếu tiếng anh, dùng sách photo mượn từ giáo viên đôi lúc bất tiện ). Lượn một vòng trên Tiki chợt thấy quyển này, liền thử mua về xem sao. Thật sự mình vô cùng hài lòng, thấy bé bảo những hướng dẫn đều bằng tiếng việt rất dễ hiểu, cặn kẽ, giọng nói trong đĩa dễ nghe và tiếp thu, cực kì thuận tiện cho việc trau dồi kỹ năng nghe của bé nhà mình. Rất đáng đồng tiền bát gạo. Tặng sản phẩm 5 sao luôn!
5
102667
2015-08-13 20:56:54
--------------------------
248862
6650
Hiện tại ở Việt Nam mình nếu nói về sách tiếng Nhật thì còn rất ít, nên đối với người đang học tiếng Nhật như mình nếu tìm được một cuốn sách tiếng Nhật hay hướng dẫn tiếng Việt là khá khó. Mình rất hài lòng cuốn sách luyện thi N3 này bởi bên trong nó có rất đầy đủ hướng dẫn, bài tập, luyện nghe rất hay và bổ ích, giúp những người thi lấy bằng tiếng Nhật tự tin hơn trong thi cử cũng như khả năng nghe của mình
Mình rất trông chờ một cuốn luyện thi khác giống như vậy. Hi vọng Tiki sẽ sớm có những bộ sách liên quan giống như cuốn này
4
313199
2015-07-31 10:23:56
--------------------------
193851
6650
Tìm một quyển sách để học và luyện thi tiếng Nhật tốt trên thị trường thật sự là rất khó vì kho sách về tiếng Nhật còn hạn hẹp. Trước đây mình học toàn là sách photo mượn từ thầy cô và tài liệu do trung tâm phát thôi chứ muốn đi ra ngoài hiệu sách lựa chọn cho mình một quyển vừa ý thì thật khó. Đối với quyển sách này sau khi đọc mình có nhận xét sau: Sách soạn theo cấu trúc đề thi năng lực nhật ngữ đọc cũng dễ hiểu và nó cũng tạo cơ hội cho mình tiếp xúc quen dần với cấu trúc đề thi .
4
324101
2015-05-08 21:41:50
--------------------------
152350
6650
Sách tiếng Nhật trên thị trường thật sự là quá ít, hầu hết khi mình học đều dùng sách photo lại của thầy cô, vừa mờ vừa nhoà, nhiều khi không nhìn ra chữ kanji luôn. Bây giờ MC books xuất bản mấy quyển này thật sự rất hay. Tiếng Nhật ngữ pháp tuy khó nhưng chăm chỉ học, ôn là được còn phần nghe mới gọi là khó nhất, người Nhật nói rất nhanh, lại rất nhiều chữ đồng âm. Càng nhiều sách để luyện nghe càng tốt. Sách soạn theo cấu trúc thi năng lực như quyển này thì càng tốt
5
73558
2015-01-22 20:52:58
--------------------------
