288371
7544
Cùng Mi & Nô Học Lễ Giáo - Mùa Yêu Thương: Để Cháu Giúp Bà
Một câu chuyện hết sức dễ thương khi tình cảm của Mi và Nô dành cho Bà.
Qua câu chuyện chia sẻ với các bé phải biết thương yêu giúp đỡ người già lúc gặp khó khăn, chia sẻ với bà là niềm hạnh phúc của bản thân mình.
Câu chuyện vẽ lên tình tiết rất chân thực, lời thoại lại dễ thương, giúp bé hiểu được tình người trong cuộc sống xô bồ này.
Các mẹ nên tìm mua cho các bé nhé. Câu chuyện rất ý nghĩa đấy ạ.
5
323719
2015-09-03 15:39:44
--------------------------
