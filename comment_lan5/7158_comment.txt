551513
7158
Mình chưa biết nội dung bên trong sách như thế nào, nhưng nhận được quyển sách cũ mèm, lốm đốm vết bẩn trên trang giấy là thấy buồn, thất vọng lắm lắm rồi.
1
1425968
2017-03-23 09:55:27
--------------------------
480565
7158
Lạc chốn phù hoa là một cuốn truyện đậm chất hiện thực, đối với riêng tôi là như thế . Cảm giác bản thân như là nhân vật trong câu chuyện.Rất nhiều cảm xúc lẫn lộn sau khi đọc hết bộ truyện này. tình yêu không còn là điều duy nhất đem lại hạnh phúc nữa.cảm nhận với tôi là như thế đó.Cảm ơn tác giả vì những cuốn sách có ý nghĩa như thế này nhé.
 Có những chuyện ta không thể nào dự đoán được, nó đến rất tự nhiên đến nỗi bản thân đã chìm đắm từ lúc nào không hề biết nữa..
5
389038
2016-08-18 20:13:46
--------------------------
373512
7158
Lúc đầu khi được bạn nè giới thiệu cuốn này vẫn còn khá do dự vì thấy truyện dài quá , khó đọc . Nhưng nhìn bìa sách khá đẹp và tin vào Bất Kính Ngữ nên mình đã mua truyện . Thấy phần đầu truyện kể về nữ chính Tô Mạt , thấy cô sao đáng thương quá , dường như mọi xui xẻo và nhục nhã đều tìm đến mà cô vẫn phải cắn răng chịu đựng -vì cuộc sống của mình và của đứa con thân yêu . Còn nam chính Vương Cư An ban đầu thấy khá đáng ghét nhưng rồi dần , khi hiểu được câu chuyện của anh , ta mới cảm thấy thông cảm cho anh hơn .
4
743273
2016-01-23 22:17:48
--------------------------
307049
7158
Nếu nói câu chuyện thực tế thì tôi không hoàn toàn đồng ý. Đâu có cô gái nào, trong cuộc sống lao động phải bê vác, cũng phải phơi nắng cực như Tô Mạt lại có thể "trắng thế", tất cả mọi người trong một vòng quen biết nhỏ như vậy lại có quá nhiều vấn đề như vậy (một số người phải nói là số phận quá thảm). 
Nhưng đây là một tác phẩm văn học, không phải là một câu chuyệ phóng sự. Tôi vẫn cho tác phẩm này 5 sao vì tôi yêu câu truyện. Nó cho tôi thấy cái kết đẹp của một con người có nghị lực, đồng thời, nó cũng cho tôi thấy thực tế mà tôi nhận ra: nghị lực hay cố gắng thôi chưa đủ, người ta cần sự khéo léo để có thể thành công, cần sự chân thành để có những đối tác tốt. 
Một điều nữa mà tôi rất tâm đắc trong câu chuyện này, đó là, đây không phải câu chuyện cổ tích, nơi mà nhân vật nữ chính sống hạnh phúc trong sự bao bọc của nam chính. Nữ chính Tô Mạt từ một cô gái sống hoàn toàn phụ thuộc vào chồng, mất phương hướng khi mất đi cột trụ đó đã trở nên độc lập, có sự nghiệp và chỗ đứng của riêng mình. Và hạnh phúc thực sự sẽ chỉ đến với người làm chủ cuộ sống của mình.Bên cạnh đó, một cuộc hôn nhân sẽ hạnh phúc và lâu dài hơn nếu hai bên bình đẳng, chia sẻ, thấu hiểu và đồng cảm với nhau.
Tôi thực sự thấy mình có thể học hỏi từ Tô Mạt nhiều điều...
5
460557
2015-09-17 22:26:50
--------------------------
247204
7158
Một điển hình cho truyện của BKN - sâu cay, thực tế, thực tế đến đáng sợ. Hơn 1000 trang giấy, vậy mà đến những chữ cuối cùng của chính văn, 2 nhân vật chính mới chính thức được ở bên nhau. May mà có ngoại truyện, nếu không chắc mình sẽ bức bối chết mất. Vương Cư An, ba mươi tư tuổi, mất con. Suýt chút nữa anh cũng lỡ mất người phụ nữ của đời mình. May mà đến giây phút cô sắp sửa thuộc về người đàn ông khác, anh đã xuất hiện. Đọc đến đây mình mới được thở phài nhẹ nhõm. Đây là một quyển sách hay, nhưng vì nó thực tế, nên sẽ không thực thích hợp cho những bạn mơ mộng một cuộc sống màu hồng. Tuy nhiên, nếu đã bước chân vào Lạc chốn phù hoa, hãy đi đến cuối cùng. Mình tin bạn sẽ không hối tiếc.
5
724380
2015-07-30 09:59:09
--------------------------
199909
7158
Lạc chốn phù hoa là một cuốn truyện đậm chất hiện thực, nó đủ phũ phàng cũng đủ chân thật, lại thêm truyện là do chị GR dịch nên văn phong rất mượt mà. Mình thích nữ chính, ban đầu là thương cho số phận bất hạnh đầy sóng gió của cô, sau đó là khâm phục, ngưỡng mộ sự kiên cường, mạnh mẽ của cô khi đối mặt với nghịch cảnh. Không phải ai cũng đủ khả năng chống chọi không gục ngã cũng không lầm đường lạc lối như Tô Mạt. Vương Cư An cũng là mẫu đàn ông ngoài lạnh trong nóng, tưởng như vô tình nhưng thực chất lại có trái tim ấm áp, là điểm tựa vững chắc cho Tô Mạt giữa dòng đời éo le này.
5
449880
2015-05-23 15:53:41
--------------------------
188002
7158
Lạc chốn phù hoa không hẳn là một quyển tiểu thuyết ngôn tình, vì nó thực, đôi lúc thực đến độ tàn khốc, không có hoàng tử, công chúa hay cô bé lọ lem, cũng không có anh chàng đẹp trai, tài giỏi, một mực si tình, không có cô gái trẻ trung, xinh đẹp. Ở Lạc chốn phù hoa, ta chỉ thấy một người phụ nữ chật vật mưu sinh, một người đàn ông không thể nào gọi là "tốt" nếu đem so sánh với những chàng trai trong tiểu thuyết ngôn tình khác, nhưng những gì tác giả viết về họ, cách tác giả miêu tả cuộc sống và tình cảm của họ, nó lại rất "đời", đây là những người ta có thể dễ dàng bắt gặp trong cuộc sống, ai cũng phải mưu sinh, vì bản thân, vì gia đình. Ở Lac chốn phù hoa, tình yêu không còn là điều duy nhất đem lại hạnh phúc nữa, ở Tô Mạt và Vương Cư An , với mình, họ đến với nhau không chỉ là tình yêu, mà đó còn là tình thân, là sự thấu hiểu đối phương đến tận cùng.
5
24079
2015-04-24 14:37:51
--------------------------
164301
7158
”Lạc chốn phù hoa” là một cuốn sách mang đến cho tôi rất nhiều cảm xúc, và khiến tôi yêu thích bởi tính chân thật của nó. Mình không chỉ thương cảm, đồng cảm cùng Tô Mạt mà còn vô cùng ngưỡng mộ cô ấy. Một người phụ nữ nhỏ bé, yếu đuối, li dị chồng, gánh trên vai nhiều gánh nặng lại bị cả xã hội thối nát dập vùi, thế nhưng cô không gục ngã, trái lại ngày càng mạnh mẽ hơn, sau bao lần vấp ngã cô vẫn kiên cường đứng dậy, dần đạt đến thành công và tìm được hạnh phúc cho bản thân. Vương Cư An mới đầu xuất hiện như một chàng trai lạnh lùng vô cảm, nhưng càng đọc mới càng hiểu anh, càng yêu anh. Chuyện đời cô và anh đều gặp nhiều khó khăn, trắc trở, nhưng có lẽ cũng chính nhờ những trắc trở ấy mà họ sẽ yêu thương nhau hơn, trân trọng hơn niềm hạnh phúc và họ đang nắm trong tay.
5
393748
2015-03-07 13:38:11
--------------------------
121416
7158
Dịch giả Greenrosetq đã đưa tôi đến với tác phẩm này. Từ những tác phẩm trước đó tới "Lạc chốn phù hoa", với văn phong, ngôn từ chau chuốt, dịch giả GR đã không làm tôi thất vọng khi truyền tải hết nội dung của câu chuyện mà tác giả Bất Kính Ngữ đã kể. Tô Mạt, từ một cô gái có phần nhu nhược, vì mưu sinh mà trở nên ghê gớm, sắc sảo. Cô không ngần ngại làm những công việc nặng nhọc, nhịn nhục làm osin, bị những kẻ bẩn thỉu sàm sỡ, thậm chí là bị chiếm đoạt. Cô là đại diện cho những con người nhỏ bé cả về nghĩa đen và nghĩa bóng, đang tồn tại đâu đó trong xã hội này.

Đọc Lạc chốn phù hoa không có hoàng tử, không có công chúa, chỉ có cái hiện thực đen ngòm bày ra trước mắt, bắt người ta phải cố gắng vượt qua hoặc là cúi đầu để bản thân tự chìm xuống đáy giống như cái chết của bạn Tô Mạt, gieo mình từ trên tầng cao xuống.

Mọi thứ thực đến phũ phàng!

Tuy kết thúc tác phẩm là một happy ending, tuy nhiên có khá nhiều trường đoạn tác giả viết khá rối, quanh quẩn không giải quyết một cách dứt khoát gây nhiều tiếc nuối.
4
213970
2014-08-19 16:00:25
--------------------------
112247
7158
Khi "Lạc chốn phù hoa" xuất bản, mình đã không ngần ngại đặt mua sách ngay, bởi mình tin tưởng vào tác giả của "Đừng nhân danh tình yêu". 
Đọc câu chuyện, mình không hình dung được Tô Mạt của "Lạc chốn phù hoa" và Tô Mạt của "Đừng nhân danh tình yêu" lại là một người. Nếu như Tô Mạt của "Đừng nhân danh tình yêu" nhu nhược, ngốc nghếch bao nhiêu thì Tô Mạt của "Lạc chốn phù hoa" lại sâu sắc, giảo hoạt bấy nhiêu!
Dường như tác giả muốn câu chuyện trở nên đặc sắc, căng thẳng nên đã dồn các bi kịch của đời người vào Tô Mạt. 
Vì con gái, vì tương lai, vì kế sinh nhai, Tô Mạt đã phải nhẫn nhịn luồn cúi khi làm osin cho nhà người khác, lại tủi nhục im lặng khi bị hiếp đáp sỉ nhục ở công ty nhưng mà cũng trùng hợp quá, người ấy lại là tổng giám đốc quyền thế ngất trời tại một tập đoàn lớn. 
Vẫn là mô típ chuyện tình hoàng tử - lọ lem những li kì hơn, nhiều tình tiết bất ngờ hơn khiến độc giả phải từ từ tiếp nhận sự thay đổi về tính cách của các nhân vật tuy không khiên cưỡng nhưng vẫn có một chút gì đó gờn gợn, mang chút tiểu thuyết.
Trong câu chuyện, không thiếu những giây phút lay động lòng người, mình đã rớt nước mắt khi đọc tới mất mát lớn của Vương Cư An, xúc động vì tình cảm và hoàn cảnh của Vương Cư An. Ngoài tình yêu, tình cảm gia đình cũng là một khía cạnh được tác giả khai thác sâu sắc, khiến mình đọc rất hứng thú.
Người dịch cuốn này cũng như cuốn "Đừng nhân danh tình yêu" đã hoàn thành khá tốt phần chuyển tải tác phẩm, khiến độc giả thật sự như đang sống cùng câu chuyện.
Rất cảm ơn nhà sách vì những cuốn sách như thế này!!!
4
101005
2014-05-10 21:37:56
--------------------------
