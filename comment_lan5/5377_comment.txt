350210
5377
Mình rất thích những hướng đi , chiến lược của Samsung đưa ra để có thể tồn tại và cạnh tranh với các thương hiệu cao cấp trên thế giới . Sách có nhiều dẫn chứng cụ thể , rõ ràng , chi tiết nên khi đọc mình cảm thấy cực kì tin tưởng . Samsung thật tài giỏi , mình thật sự thật sự cảm thấy ngưỡng mộ những con người ở xứ sở Hàn Quốc này đã đem đến vinh quang không chỉ cho đến thương hiệu của mình mà cả thế giới đều phải bái phục trước ông hoàng công nghệ - Samsung này. Đây là một cuốn sách rất đáng đồng tiền cho những ai muốn học hỏi chiến lược và tầm nhìn xa của người Hàn. The Samsung Way quả thật rất bổ ích
5
781966
2015-12-10 08:44:37
--------------------------
293609
5377
The Samsung Way - Đế Chế Công Nghệ Và Phương Thức Samsung, từ một thương hiệu bình thường được mọi người biết đến ở lĩnh vực hàng gia dụng điện tử, Samsung đã tạo ra một bước nhảy vọt cực kì lớn và không tưởng trong lĩnh vực công nghệ và đang dần hình thành nên một đế chế, tạo một bước ngoặt mới để biến thương hiệu trở thành biểu tượng trên toàn cầu. Cuốn sách cho ta biết được về những "Way" của Samsung trong những việc quản lí, quản trị, một tầm nhìn xa cho một sự khủng hoảng để chuẩn bị một bước đi lớn sẵn sàng đương đầu với các thách thức.
5
136569
2015-09-08 14:48:04
--------------------------
289228
5377
Nếu ai là tín đồ của Samsung hoặc Android, hoặc là người thích nghiên cứu về quản trị kinh doanh, quản trị doanh nghiệp, quản trị chiến lược thì quyển sách The Samsung Way - Đế Chế Công Nghệ Và Phương Thức Samsung của Tác giả JaeYong Song - KyungMook Lee  xứng đáng là 1 tài liệu tham khảo hữu ích. Nếu Apple của Steve Jobs nổi lên chủ yếu nhờ mac và iphone thì samsung xứng đáng được gọi là 1 đế chế khi nghiên cứu và kinh doanh đa ngành như điện tử, điện lạnh, công nghệ, xứng đáng là ngọn đuốc tự hào của người Châu Á.
5
465332
2015-09-04 11:27:21
--------------------------
207750
5377
Samsung quả thực đã hóa rồng kể từ khi chủ tịch Lee Kun Hee lên nắm quyền và đưa Samsung từ một nhà sản xuất hàng nhái giá rẻ vươn mình trở thành một con rồng châu á thực sự trong lĩnh vực điện tử nói chung và điện thoại di động nói riêng. Quyển sách The Samsung way theo cá nhân tôi là một tài liệu khá là đầy đủ về tập đoàn chaebol này. Phương thức Samsung bao gồm các tư duy về quản trị sắc xảo, cách thức phát triển sản phẩm mới cách tân và thời thượng mà còn thay đổi cả hệ thống phân phối sản phẩm bằng cách thay đổi các nhà phân phối để sản phẩm tới tay khách hàng một cách đơn giản nhất. Phương thức Samsung là cả một hệ thống tư duy quản trị và chiến lược cực kỳ xuất sắc mà tất cả chúng ta cần phải học tập.
5
313837
2015-06-13 02:10:04
--------------------------
202713
5377
mình đã có dịp đọc qua cuốn sách này rồi, rất hay và có nhiều điều để học hỏi, mình cũng chẳng phải chuộng hàng ngoại, đọc để có thêm hiểu biết. Samsung thành công nhưng cũng có vài tai tiếng bên mảng đt nên hãng cũng có không ít antifan nhưng có anti hay không cũng phải công nhận 1 điều là Samsung là một tập đoàn tài giỏi. Từ 1 hãng nhỏ bé vươn lên đứng trong hãng top TG, xếp hãng vượt qua hàng loạt các tên tuổi lớn của NB không phải là điều dễ dàng..
5
374352
2015-05-30 12:27:30
--------------------------
159552
5377
Samsung hiện nay là ông vua smartphone trên thế giới điện thoại di động. Để có được vị trí này, Samsung của chủ tịch Lee Kun Hee đã vượt qua hàng loạt các tên tuổi khác trong thị trường smartphone, đặc biệt là Apple của Tim Cook. Không chỉ là ông vua smartphone, đế chế Samsung còn muốn trở thành vua của rất nhiều mặt hàng điện tử khác như tivi và các linh kiện, phụ kiện điện tử khác. Và tập đoàn chaebol này đã mở nhiều nhà máy tại Việt Nam, tạo công ăn việc làm cho rất nhiều công nhân. Để hiểu tại sao Samsung trở nên hùng mạnh như ngày nay, điều mà 20 năm trước chưa ai dám nghĩ đến, cuốn sách này là một tài liệu cần thiết để tìm hiểu.
5
387632
2015-02-17 17:04:16
--------------------------
154433
5377
Chỉ mới đọc vài trang thôi đã thấy có nhiều khía cạnh để nói rồi ! Phải công nhận rất phục cách điều hành cty của Chủ tịch Lee Kun Hee....Dường như "Nhận biết về được khủng hoảng" chính là yếu tố then chốt giúp cho doanh nghiệp thực hiện công việc tái cấu trúc cty trước khủng hoảng kinh tế, và đặc biệt hơn là tốc độ thực hiện của Samsum luôn dẫn đầu - một yếu tố đưa Samsung vươn thành vị trí số 1 thế giới - một chiến lược mà các doanh nghiệp lớn ở Việt Nam nên học hỏi.....
5
540768
2015-01-29 10:08:43
--------------------------
139960
5377
Samsung, cái tên vô cùng quen thuộc với người tiêu dùng Việt Nam. Chúng ta đã biết đến Samsung thông qua các sản phẩm ti-vi hay điện thoại. Cuốn sách này rất bổ ích cho những người muốn tìm hiểu thêm. Cuốn sách quá hay nếu chọn cuốn sách này chắc chắn là lựa chọn tuyệt vời đối với tất cả mọi người yêu thích các sản phẩm của Samsung. Cái này dành cho tất cả mọi người yêu thích sản phẩm này nói chung và doanh nhân, sinh viên,... nói riêng và không dành cho những người không biết giá trị quan trọng tác phẩm này.
5
468545
2014-12-09 18:56:17
--------------------------
