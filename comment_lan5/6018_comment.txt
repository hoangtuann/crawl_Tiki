472055
6018
Cuốn sách này được thiết kế như một cuốn lịch khi đặt hàng thì  mình không nghĩ là quyển sách này được thiết kế như vậy đâu .Còn về chất lượng giấy thì rất tốt không có gì đáng để chê cả chỉ tiết là phần nội dung của nó không thực sự như mình đã kì vọng lúc ban đầu .Nói chung đối với các bạn nào mới tập Yoga thì đây là quyển mà các bạn nên xem còn trên 1 năm thì mình nghĩ trên Tiki có nhiều quyển khác phù hợp với các bạn trên 1 năm mình khuyên nên xem cuốn Yoga toàn tập và các bạn mới cũng đọc được.
3
860687
2016-07-09 09:23:54
--------------------------
260660
6018
Cuốn sách được thiết kế như một cuốn lịch để bàn và rất hợp lý cho việc vừa tập vừa coi động tác, mình rất ưng ý khoản này và nó còn được chia ra làm 2 phần cơ bản và nâng cao nên không cảm thấy áp lực cho một người mới bắt đầu tập yoga như mình. Bên cạnh đó cuốn sách còn nói về lối sống, cách suy nghĩ về yoga cũng như là chế độ ăn sao cho phù hợp, một số thuật ngữ riêng của yoga cũng được giải thích rất cụ thể. Bạn có thể kết hợp xem sách và video trên mạng để tập nhé
4
532427
2015-08-10 16:15:11
--------------------------
243732
6018
Mình đã đặt mua cuốn sách này hồi đầu tháng 6 để tranh thủ thời gian nghỉ hè tự học các bài tập có trong sách. 26 chuỗi động tác từ cơ bản, dễ dàng nhất cho đến những động tác đòi hỏi kĩ thuật phức tạp được trình bày rõ ràng, chi tiết qua từng trang sách. Từ ngữ trong sách cũng khá đơn giản, dễ hiểu hơn nhiều so với những cuốn dạy yoga trước đây mình đã từng đọc qua. Ngoài ra, trong sách còn có phần hướng dẫn cụ thể về cách thở, lối sống và thiền định. Nhìn chung, đây là một cuốn sách thích hợp cho những ai mới làm quen với yoga.
4
134196
2015-07-27 17:36:02
--------------------------
228159
6018
Mình mua sách về để tự tập yoga. Với mình yoga là hoàn toàn mới. Khi xem hình mình không nghĩ sách thiết kế kiểu quyển sổ như vậy. Không biết là ý đồ tác giả thử tính kiên nhẫn của người tập yoga hay chỉ đơn giản là thích thế nhưng mình thấy việc lật từng trang để thực hành khá bất tiện. Các bài tập cũng không quá khó và có hướng dẫn chi tiết. Dù sao với người mới làm quen với yoga mà lại tự luyện tập như mình thì quyển sách là phù hợp rồi. Cảm ơn Tiki!
3
470181
2015-07-15 07:51:50
--------------------------
220441
6018
Sách này được cái là phần thiết kế lạ và đẹp mắt, hình ảnh to rõ rất dễ nhìn nhưng nội dung sách thì không nhiều. Từng trang sách dày không mỏng như kiểu sách A4, chắc bởi vậy giá tiền cao nằm ở hình thức sản phẩm chứ chất lượng thì không có bao nhiêu. Sách thiết kế giúp mình có thể mở ra và gấp lại y như cuốn lịch làm việc để bàn ... Lâu lâu ngồi ngó nhìn thấy tạo cảm giác thư giản cũng hay hay ... Nội dung sách cũng xoay quanh một vài thế tập điển hình căn bản như đứng, ngồi, quỳ, nằm tập, ... Sách hợp với những bạn mới bắt đầu tập Yoga và thích kiểu thiết kế độc đáo, lạ lẫm nhằm để trang trí trên bàn làm việc cũng hay.
3
618799
2015-07-02 16:13:07
--------------------------
