424482
2739
Đây là quyển sách tệ hại và nhàm chán nhất mà tôi biết. 
Tôi không thể nào đọc nổi hết quyển sách, vì nội dung nó quá tệ, thật ngoài sức tưởng tượng của tôi. Ban đầu tôi mua nó vì tôi chỉ nghĩ đơn giản là giúp bản thân tránh được những cám dỗ của những người đàn ông tệ hại. Nhưng khi lướt một loạt qua các trang sách, tôi thấy nó làm cho người ta bi quan, mất hết niềm tin vào cuộc sống; mất niềm tin vào những người đàn ông bên cạnh mình. Bạn thử tưởng tượng xem nếu một cô gái vừa chia tay người yêu hoặc gặp trục trặc tình cảm với bạn trai mà đọc cuốn sách này sẽ thế nào nhỉ ? Chắc không dám quen ai hoặc từ bỏ ý định lập gia đình không chừng !!!
Tôi không biết tác giả cuốn sách này là ai, nhưng tôi nghĩ chắc cô này không có chồng; mà có chắc chồng cô ấy không phải con người bình thường, là "thánh nhân" chẳng hạn !!!
Mua xong cuốn sách, tôi chưa đọc là bán ve hai, mặt dù giá trị cuốn sách cũng không rẻ, nhưng những cuốn sách như vậy phải nhường chỗ cho những cuốn sách tuyệt vời nằm ở vị trí của nó mới hợp lý. 
Nói chung ai không có ý định lấy chồng, chán ghét đàn ông hoặc mê cái vẻ đẹp bên ngoài của nó thì cứ mua mà đọc. Còn ai còn lạc quan, yêu đời, biết đứng dậy sau vấp ngã và tin rằng, phụ nữ sinh là là để được yêu thương, được người đàn ông của mình trân trọng thì không nên mua nó; các bạn nên đọc "A beautiful bad girl" của Thùy Chi đi, rất tuyệt vời, bạn sẽ thấy phụ nữ chúng ta là một viên ngọc quý và đàn ông tốt cho bạn là không thiếu nếu bạn tốt. Chỉ có bản thân mình mới làm mình đau, còn lại không ai được phép làm vậy nếu không có sự đồng ý của mình. Và mình chỉ thật sự rẻ mạt khi cứ suy nghĩ rằng mình sẽ gặp những người đàn ông rẽ mạc !
1
765532
2016-05-01 21:07:28
--------------------------
257580
2739
Đây thật là một quyển sách hữu ích với nội dung được trình bày cụ thể, rõ ràng và dễ hiểu. Tôi nghĩ điểm hay nhất của quyển sách này là các loại "đàn ông tệ hại" được trình bày chi tiết về cách nhận diện từ cách họ tiếp cận "con mồi" đến ngôn từ họ dùng, đến các cử chỉ phi ngôn ngữ. Hơn nữa, tác giả còn giải thích cả nguyên nhân khiến những người đàn ông này trở nên tệ hại. Tác giả cũng cung cấp một số công cụ giúp xác định mối quan hệ hiện tại có lành mạnh hay không.
Các cô gái nên sở hữu quyển sách này!
5
268875
2015-08-07 19:48:45
--------------------------
