448955
2726
Cuốn truyện khổ to, nét, dịch thuật tốt, chữ và hình cũng không bị nhòe hay mờ. Nội dung thì lấy ra từ các mẩu chuyện trong 45 tập truyện ngắn. Trong này mình thích nhất là tập trò chơi xúc xắc thám hiểm vũ trụ. Rất hả dạ vì Jaian và Suneo gieo gió phải gặt bão. Nobita và Shizuka chơi đúng luật thì chiến thắng. Câu chuyện cũng cho thấy sự tốt bụng của Doraemon. Nói thật thì mình rất thích bảo bối này của Doraemon. Gía mà có nó trong tay mình có thể chơi cả ngày cũng được.
5
1420970
2016-06-16 19:50:02
--------------------------
406189
2726
Mua Truyện Tranh Nhi Đồng - Doraemon cũng khá lâu rồi hôm nay mới để lại nhân xét. Tôi mua cho em trai học lớp 2. Hồi còn nhỏ rất hay đọc và thích. Bây giờ có mạng, có máy tính, em tôi chỉ xem trên tivi. Tôi muốn em mình đọc truyện để rèn luyện khả năng viết cũng như chính tả, nghĩ ngay đến bộ truyện Doreamon thần thánh ngày bé.
Truyện Tranh Nhi Đồng - Doraemon được tái bản, in ấn chất liệu khá tốt và bắt mắt. 
Tiki giao hàng nhanh chóng, bao bọc ẩn thận. Tôi khá hài lòng với dịch vụ của Tiki. 
5
110566
2016-03-27 21:20:25
--------------------------
