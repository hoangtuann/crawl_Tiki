272821
5533
Nội dung quyển sách nói về việc xây dựng một ngôi làng xanh sạch đẹp. Thông qua đó truyền thông điệp rằng con người không nên tàn phá thiên nhiên, không chặt phá rừng bừa bãi, không đổ đất đá xuống dòng sông để những cánh rừng, ngọn núi không còn trơ trọi, để những dòng sông không bao giờ cạn. Con người cần phải biết yêu quý và bảo vệ thiên nhiên cũng như bảo vệ chính sự sống của mình.  Quyển sách còn cho chúng ta thấy được hậu quả nặng nề của việc làm trái với quy luật tự nhiên. Hình ảnh đẹp, màu sắc hài hòa, chất lượng giấy tốt. Cám ơn tiki.
5
110777
2015-08-20 14:12:49
--------------------------
178389
5533
Quyển sách được in bên ngoài rất đáng yêu, gần gũi. Các trang sách có chất lượng tốt, giấy dày dặn, bề mặt láng, bóng mờ nên không gây phản chiếu ánh đèn gây chói mắt. Màu sắc hài hòa, đẹp mắt. Hình vẽ dễ thương, thu hút được sự tập trung và tò mò của trẻ em.
Nội dung rất hay, đề cập đến thói xấu của con người chỉ vì ham cái lợi trước mắt mà tàn phá thiên nhiên, không suy tính đến các sinh vật sống khác hay hậu quả nặng nề của nó về sau.
5
561797
2015-04-04 22:11:32
--------------------------
