304667
2875
Là người yêu thích hội họa nhưng vì điều kiện nên tôi không có điều kiện đi học, nên tôi thường tìm mua những cuốn sách hướng dẫn vẽ của nhiều tác giả để tự vẽ. Tôi thực sự thích cuốn Bí quyết vẽ tranh tĩnh vật bởi cách hướng dẫn giải thích rất rễ hiểu. Cách tạo bố cục bức tranh và các bước hoàn thiện đều được mô tả bằng hình vẽ sinh động. Nhờ có sách này tôi đã từng bước vẽ được những bức tranh tĩnh vật về hoa quả trong cuộc sống thường ngày cho riêng mình.
5
469875
2015-09-16 16:01:37
--------------------------
