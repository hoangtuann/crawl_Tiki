550509
8131
Mua sách cũng gần dc một tuần rồi. Công nhận tiki giao hàng nhanh thật, cách gói cũng cẩn thận và đẹp. Nhưng mình buồn là khi nhận quyển sách này thì hơi cũ, bìa nó bị công lại.mình quý sách lắm, mà thấy nó bị gãy bìa thì thật sự thất vọng!!! 😞
4
2223253
2017-03-22 13:17:45
--------------------------
451307
8131
Khi đã ra ngoài xã hội, đã đi làm thì việc giao tiếp giữa người và người sẽ xảy ra thường xuyên. Đặc biệt với công việc kinh doanh, đặc biệt là nhân viên cấp dưới như mình. Mình cần phải hiểu rõ đối phương và mình cần phải chinh phục họ trong thời gian ngắn nhất. 
Việc chinh phục này không chỉ đơn giản giữa khách và người bán hàng mà là giữa lính và cấp trên.
Làm theo những hướng dẫn đơn giản trong cuốn sách này, mình sẵn sàng mở một cuộc trò chuyện với bất cứ ai
Kèm theo đó là những mẹo nhỏ hữu ích và những lời khuyên chuyên nghiệp. Thật sự hữu ích!
4
90460
2016-06-19 16:57:39
--------------------------
433455
8131
mình rất kém trong khoảng giao tiếp nên muốn đặt mua cuốn sách này để cải thiện tình hình. Chất giấy nhẹ, màu vàng giúp đọc không bị chói mắt, cỡ sách bằng 1 quyển vở, không dày lắm. Nội dung trình bày khoa học, có các mẹo rất hay đơn giản và dễ áp dụng. Mỗi trường hợp tác giả đều phân tích rõ ràng dễ hiểu. Đây hẳn là một cuốn sách nên đọc đối với những bạn ngại giao tiếp như mình. Mình đã và đang áp dụng các mẹo trong sách vào cuộc sống và mình thấy khả năng giao tiếp có cải thiện được chút chút, có lẽ do mình chưa quen lắm nên mình sẽ cố gắng hơn nữa.
4
638430
2016-05-21 08:20:08
--------------------------
426448
8131
Tôi mới nhận được quyển sách này hôm nay và mới đọc xong một nửa. Bản thân tôi là một người hướng nội nên rất ít khi giao tiếp, đến khi lớn rồi tôi mới nhận ra giao tiếp là rất quan trọng, bởi đó là con đường duy nhất để tôi có thể chinh phục người đối diện. Cuốn sách này đáp ứng được yêu cầu mà tôi tự đặt ra cho chính bản thân mình khi giao tiếp, ngay chính tên bìa đã toát lên nội dung của cả cuốn và dễ dàng để lại ấn tượng trong lòng bạn đọc. Tôi rất hài lòng về sản phẩm và TIKI.
5
743345
2016-05-06 22:37:31
--------------------------
420070
8131
Sách hữu ích khi tạo ra các tình huống trong giao tiếp mà không mấy ai để ý, nếu không để tâm thì rất dễ lúng túng trong những lúc đó. Cách dẫn dắt thú vị.  Nếu đọc và biết cách vận dụng thông minh thì rất hữu ích còn áp dụng máy móc thì sẽ thành thảm họa. Thât ra đọc để bổ sung kĩ năng mềm chứ không thể nào nhớ hết và áp dụng tất tần tật những điều trong sách được. Mình đọc và áp dụng nhưng không thấy người đối diện bị mình chinh phục cho lắm =.=. Nhiều ví dụ mẫu (mẫu câu để mình áp dụng trong khi giao tiếp mình thấy hơi sến và không chân thành, nên áp dụng linh hoạt chứ bê nguyên câu đó vào người ta sẽ nghĩ ngay mình giả tạo hoặc "lưỡi không xương" ^^.
4
897666
2016-04-22 15:46:12
--------------------------
337990
8131
Nhiều người nói tài năng giao tiếp đến từ bẩm sinh, rằng chỉ có những người trời ban cho sự " dẻo miệng " mới làm quen được với người khác, nhưng sự thật thì không phải như vậy. Quyển sách " 5' chinh phục người đối diện " sẽ nêu ra những kĩ năng, cách thức để tiếp cận một người, một nhóm người hay thậm chí cả một tập thể. Những kiến thức sẽ giúp bạn có thể chủ động mở lời bắt chuyện, duy trì cuộc trò chuyện, và cách kết thúc sao cho phù hợp nhất. Hi vọng các bạn có thể tìm đọc những tác phẩm bổ ích như thế này!
4
539083
2015-11-15 09:33:55
--------------------------
337572
8131
Mình là 1 người không tự tin và không biết cách tạo ấn tượng cũng như bắt chuyện với người lạ.Cuốn sách giúp mình biết cách nói chuyện hay hơn cũng như mạnh mẽ dạn dĩ tự tin nới chuyện với người khác ( đặc biệt là người lạ) giúp mình mở rộng mối quan hệ.Đến nay mình chưa đọc hết sách,vì mình muốn mở rộng mối quan hệ nên phần online mình có bỏ dở.Nhưng thật sự cuốn sách rất hưu ích trong cả cuộc sống và công việc. Mình rất thích Don Gabor và đọc một mạch từ đầu tới cuối
5
604569
2015-11-14 12:12:20
--------------------------
307939
8131
Don Gabor là một tác giả quá nổi tiếng, tôi đã đọc ra nhiều sách của ông, Trong cuốn ' 5 phút chinh phục người đối diện', Don Gabor đưa ra, phân tích cá tính khác nhau và phong cách quan hệ. Cuốn sách này sẽ cho bạn rất nhiều bài học hữu ích, theo tôi cuốn sách rất cần cho những ai  đã, đang gặp rắc rối trong việc giao tiếp với người khác, và sẽ tìm thấy sự giúp đỡ rất nhiều trong cuốn sách này, cuốn giải thích tất cả những điều rất đơn giản làm thế nào để bắt đầu một cuộc hội thoại và tạo các mối quan hệ với bạn bè, làm thế nào để làm chủ nghệ thuật của cuộc trò chuyện... bằng cách này bạn có thể có thêm bạn bè và những người bạn tốt. Tôi nhận thấy rằng tôi nói chuyện tốt hơn sau khi đọc vài chương đầu tiên!. Cuốn sách này là rất dễ dàng để hiểu và tôi đã ngay lập tức đọc hết. .Tôi khuyên những ai hơi nhút nhát nên đọc cuốn sách này hay nếu ai muốn có cuộc đàm thoại tốt hơn,
Cuốn sách này thay đổi một phần cuộc sống của tôi, cải thiện việc giao tiếp, tôi có nhiều bạn bè và tôi cảm thấy tốt hơn. Cảm ơn tác giả. 
4
342197
2015-09-18 13:13:39
--------------------------
290636
8131
Cuốn sách này đơn giản để đọc và hiểu. Các ví dụ của tác giả cũng thực tế giúp người đọc hình dung ra được ngữ cảnh chính xác và có thể đặt bản thân vào các tình huống đó. Tuy nhiên, các ví dự này có phần giống với nhiều cuốn sách khác về giao tiếp. Có lẽ đây là những tình huống kinh điển?
Điều cuốn sách này nhấn mạnh nhất là sự tinh ý trong việc đọc tình huống, nắm bắt cảm xúc của đối phương để chúng ta có thể điều chỉnh hành vi cho phù hợp. Từ những ví dụ trong sách, các bạn có thể áp dụng trong thực tế nhanh chóng.
4
133783
2015-09-05 16:47:47
--------------------------
288751
8131
Trước tiên là bên tư vấn nhiệt tình, có trách nhiệm, giao hàng nhanh chóng đúng hẹn.  Bìa sách và bọc sách tôi thấy khá tốt, nội dung dễ hiểu, có thể tự mình rút ra nhiều kinh nghiệm khi đọc cuốn sách này, tuy chất lượng giấy in chưa khiến tôi thật sự hài lòng, nói chung đây là 1 cuốn sách bổ ích đối với bất kì đối tượng nào muốn trau dồi kỹ năng giao tiếp, và thành công trên con đường giao tiếp. Là một quyển sách đáng để đầu tư . Chúc các bạn thành công . 
5
779034
2015-09-03 21:03:55
--------------------------
277012
8131
"5' chinh phục người đối diện" thực chất là một cuốn sách nói khá nhiều vào việc nâng cao EQ của con người một cách cơ bản nhất và đối với mình, là cực- kì- cần- thiết đối với phần lớn các bạn trẻ việt nam thiếu sót mảng này trong cuộc sống cũng như trong các cuộc phỏng vấn xin việc, giao tiếp với đồng nghiệp, đối tác. Khả năng giao tiếp của mỗi người khi sinh ra không hề như nhau, có người bẩm sinh đã cao, có người lại cực thấp, chính vì vậy cuốn sách này sẽ giúp phần lớn các bạn có thể hòa nhập dễ dàng hơn và lưu loát trong công việc hơn với cuốn sách này. Đối với các bạn đã thiên bẩm tốt trong lĩnh vực này thì mình khuyên các bạn cũng không cần thiết lắm phải mua vì nó không có gì quá cao siêu như trên tựa đề quyển sách.
3
14979
2015-08-24 16:14:52
--------------------------
268464
8131
"5 phút chinh phục người đối diện" của Don Gabor là một quyển sách hay về giao tiếp. Bên trong sách chỉ rất rõ về cách khởi đầu giao tiếp, các lỗi giao tiếp thường gặp và cách khắc phục; chia sẻ lắng nghe sao cho phù hợp, bốn bước tự nhiên để phá vỡ sự im lặng,... ngoài ra còn có làm tăng sự hấp dẫn, tự tin và khôn khéo. Sách đẹp, giấy dày đẹp, có nhiều ví dụ từ thực tế, những câu hỏi thường gặp. Nhược điểm duy nhất là sách hơi dài dòng. Đây là một quyển sách cần thiết cho những ai muốn cải thiện trình độ giao tiếp cá nhân.
4
554150
2015-08-16 14:48:01
--------------------------
265256
8131
Thật ra theo như tôi nghĩ, cuốn này nên đặt là "Những quy tắc giao tiếp cơ bản" thì đúng hơn. Sách nói về nhiều nguyên tắc đơn giản như phá vỡ thế phòng thủ khi giao tiếp của người khác bằng sự cởi mở, hay tìm chủ đề chung để nói chuyện...nhưng những tip như vậy tôi thấy chưa đủ thần kỳ để chinh phục người đối diện trong 5 phút. Tuy nhiên, những nguyên tắc trong sách rất đúng và tôi đã áp dụng thành công.
4
268746
2015-08-13 21:01:37
--------------------------
265106
8131
Đầu tiên khi mới nhìn cuốn sách tôi đã cảm thấy ấn tượng bởi cái bìa được thiết kế rất đẹp, và khi xem tới nội dung bên trong tôi đã bị tác giả "hạ gục" hoàn toàn. Cuốn sách hướng dẫn cách giao tiếp từ khát quát đến chi tiết, từ khởi đầu đến phức tạp nên độc giả có thể nâng cấp dần khả năng của mình. Cuốn sách giúp bạn thưởng thức trọn vẹn điều thú vị trong mọi tình huống mà không sợ ngại ngùng lóng ngóng, chinh phục được những người bạn mới, kết nối tập thể... Tác giả đưa ra rất nhiều ví dụ cụ thể và kèm theo là các mẹo hay, các lời khuyên có ích để bạn không bị khó xử nếu lỡ có gặp phải trường hợp đó. Tác giả cũng chỉ cho bạn đọc làm các câu hỏi trắc nghiệm trong sách để đánh giá đúng hơn về phong cách giao tiếp của bản thân và còn nêu ra các ưu nhược điểm của từng cách nói chuyện. Don Gabor thật thông minh và có hướng tiếp cận độc đáo trong việc chỉ dẫn độc giả trở thành những người giao tiếp giỏi cả khi trực tiếp và trực tuyến, mình thấy sách thật sự hay và hữu ích ! Trình bày trong sách thì rất đẹp, rõ ràng và mạch lạc, bìa gập cứng cáp, keo gáy chắc chắn.
4
75959
2015-08-13 18:45:43
--------------------------
247007
8131
Thực sự đây là một cuốn sách về giao tiếp rất hay và súc tích. Cách tác giả lấy ví dụ bằng các tình huống giao tiếp trong cuộc sống giúp người đọc dễ dàng mường tượng và ghi nhớ hơn. Không những thế tác giả còn tóm gọn những nguyên tắc bằng những từ viết tắt ngắn gọn, dễ nhớ. Mặc dù ngôn ngữ và giao tiếp có sự khác nhau giữa các nền văn hóa, nhưng đọc cuốn sách này sẽ thấy vẫn có thể áp dụng rất nhiều điều vào văn hóa giao tiếp ở Việt Nam. 
5
569146
2015-07-30 09:41:11
--------------------------
217287
8131
Mình đánh giá cao cuốn sách này ở chỗ nó tiếp tục là một cuốn sách viết về hành vi con người. Nếu như đọc hết từng chương, bạn sẽ thấy tác giả đã lấy bối cảnh là một người mới hoàn toàn buớc vào một bữa tiệc xa lạ. Tác giả đã giúp chúng ta từ những buớc cơ bản nhất như nụ cuời, cách nói chuyện, cách ứng xử cũng như cách giao tiếp.

Nhuợc điểm duy nhất của cuốn sách này chính là tác giả đã lấy sẵn một bối cảnh, nên người đọc sẽ cảm thấy hơi hạn hẹp về nội dung. Tuy nhiên, theo mình đây vẫn là một cuốn sách rất đáng đọc.
4
140922
2015-06-29 08:52:06
--------------------------
210839
8131
Lúc mua quyển sách này mình có thấy nó ở danh sách best seller và  xem review của 2 bạn từng mua (cho 4 và 5 sao) thấy các bạn khen nên mua, nhưng mình thấy khá thất vọng.

Ưu điểm: một số ý khá tốt, hình thức thì khỏi chê, sách đẹp từ bìa tới giấy in, sáng sủa, sạch sẽ.

Nhược điểm: mình thực sự thấy nó quá dài dòng, 300 trang mà đọng lại chẳng được bao, ví dụ chưa phù hợp với cách ứng xử, văn hóa của nước ta vì cách nói mà tác giá gợi ý cho người đọc áp dụng nó quá lịch sự, trang trọng, không tạo sự gần gũi giữa 2 người khi đang trò chuyện (mình thấy nói tiếng Anh những câu đó đc, nhưng mình đoán là do người dịch theo nghĩa gốc, nên nó cứng quá) 

Cảm ơn đã đọc nhận xét của mình, ý kiến chỉ là ý kiến cá nhân, việc quyết định mua sách vẫn là ở bạn :D
2
591731
2015-06-19 21:13:17
--------------------------
192627
8131
Vừa đọc xong. Mình rất thích cuốn này, đọc một mạch từ đầu tới cuối. Tác giả không đưa ra những lời tư vấn chung chung mà rất chi tiết, cụ thể, cách giải thích cũng dễ hiểu. Tuy là của tác giả người Mỹ nhưng mình tin cuốn sách này cũng rất thiết thực cho người đọc ở Việt Nam.
Mình đã đọc nhiều sách giao tiếp nhưng vẫn học hỏi được những bí quyết riêng của tác giả qua cuốn sách này.
Về hình thức, cuốn sách này bìa đẹp, trình bày đẹp, rõ ràng, có tóm tắt ý chính trước mỗi đoạn và có tổng kết các ý cần nhớ ở cuối sách.
5
419865
2015-05-05 10:49:20
--------------------------
142850
8131
Cuốn sách viết về những thủ thuật trong giao tiếp. Tuy nhiên nó không giống những cuốn sách thông thường. Ở đây, tác giả đi rất cụ thể vào từng trường hợp, "gãi" đúng chỗ ngứa của người đọc.Những trường hợp sinh động nhằm giúp người đọc dễ hình dung tưởng tượng và không thấy nhàm chán khi chỉ đọc lý thuyết suông thông thường. Ngoài ra tác giả còn chỉ ra các trường hợp khó hay gặp phải trong giao tiếp và chỉ ra cách xử lý gợi ý cho người đọc.

Đây là cuốn sách khởi đầu cần thiết cho những ai muốn trở thành bậc thầy trong giao tiếp!
4
475440
2014-12-20 10:14:55
--------------------------
