230698
3671
Con trai mình 6 tuổi, chưa biết đọc (đang tập) nhưng thấy cuốn này là mê ngay từ cái nhìn đầu tiên. Hình ảnh rất sinh động và hài hước, màu sắc bắt mắt, giấy in dày và mịn. Cuốn sách cung cấp gần như toàn bộ kiến thức về các phương tiện giao thông, lịch sử ra đời, phát triển, đặc điểm của hầu hết các phương tiện di chuyển mà con người đang có. Điểm trừ duy nhất là gáy sách dán keo không được chắc chắn lắm.
4
687089
2015-07-17 17:36:40
--------------------------
