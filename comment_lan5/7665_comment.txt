365321
7665
Mình thấy tập truyện này rất hay khi mở đầu đã cuốn hút với tình tiết Bá hộ Mão bán nước cho dân làng (và dĩ nhiên là ông Bá hộ bán giá cắt cổ rồi), và cậu bé Dậu nghèo khổ là người quay gầu múc nước để bán. Đoạn ấy thương Dậu quá, may mà có Trạng Tí đứng ra phân xử không là Dậu bị Bá hộ Mão quỵt tiền rồi. Từ đó lại dẫn tới việc Tí tìm cách đưa nước về cứu dân làng. Đúng là tập truyện này có nhiều tình tiết và sự việc nên hấp dẫn hơn hẳn. Mình cho truyện 5 sao.
5
386342
2016-01-07 20:47:59
--------------------------
