472130
5086
Đầu tiên, mình tin tưởng ở cuốn sách này vì đã được Đức Giám Mục Imprimatur đàng hoàng khiến mình rất an tâm bởi lúc đặt mua mình cũng hơi lo lắng nhưng khi nhận được sách và nhìn thấy trang Imprimatur của Đức Giám Mục thì mình đã hoàn toàn yên tâm! Các bạn Công Giáo cứ an tâm mà mua cuốn truyện này cho con cháu mình nhé! Mình đã thực sự bị đánh gục bởi những hình minh họa tuyệt đẹp trong sách này! Tất cả đều được in màu! Khổ to và đẹp! Tuyệt vời! Chỉ có điều lần này Tiki đóng sách lỏng lẻo khiến sách mình nhận được không còn hoàn hảo! Tuy nhiên dù sao thì mình cũng vẫn rất thích vì Tiki cũng đầu tư bán sách Công Giáo!
5
633778
2016-07-09 10:30:08
--------------------------
266207
5086
Sách gồm 86 câu chuyện ( 45 truyện cựu ước và 41 truyện tân ước ) được trình bày ngắn gọn nhưng hàm chứa nhiều ý nghĩa, tóm lược những sự kiện nổi bật trong kinh thánh  với nhiều hình ảnh minh hoạ được vẽ rất đẹp, rõ ràng và rất bắt mắt. Một mặt sách truyền cảm hứng cho người xem về tấm lòng nhân ái , cao cả yêu thương con người, sự hy sinh hết mình vì người khác của Đức Jesus. Mặt khác sách cũng nêu lên những phong tục tập quán, lối sống sinh hoạt của người dân thời bấy giờ cho chúng ta hiểu biết thêm. Đây sẽ là món quà có giá trị và nhiều ý nghĩa cho trẻ em trong các gia đình Kitô hữu.
5
728904
2015-08-14 14:45:24
--------------------------
