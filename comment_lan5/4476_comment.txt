463402
4476
Ai học marketing thì đều biết đến Kotler - bậc thầy về tiếp thị. Sách nêu ra các nhận định của bậc thầy về thị trường marketing nói chung và một số cách hiện thực vào thực tế nói riêng. Những ví dụ đều ở nước ngoài nên người đọc coi để tham khảo. Còn áp dụng ở Việt Nam thì cần phải Việt hoá lại theo xu hướng thị trường và vùng miền cho phù hợp. Đọc để tham khảo là lựa chọn tốt cho những ai muốn đi theo hay phát triển trong ngành tiếp thị này. Sách nên đọc.
5
861781
2016-06-29 11:28:18
--------------------------
455287
4476
Đây là một quyển sách kinh điển của bộ môn Marketing. Nhắc đến marketing là nhắc tới Philip Kotler. Mình nhớ hồi còn đi học ngành quản trị ở trường đại học, toàn thấy tên tác giả này trong các định nghĩa, chú thích, quy trình thuộc môn học này. Thật sự nói ông là ông tổ của tiếp thị hiện đại cũng không sai. Đọc sách mình thấy được rằng “phải luôn đổi mới” vì con người, thế giới luôn thay đổi, ngành tiếp thị cũng vậy, phải luôn thay đổi thì mới bắt kịp với xu thế thời đại được.
4
615987
2016-06-22 14:43:45
--------------------------
418065
4476
 'Kotler bàn về tiếp thị' như một bài giảng xuyên suốt đúc kết bức tranh toàn cảnh của tiếp thị từ giai đoạn những năm 2005 cho đến giai đoạn chuyển mình thích nghi với Thời đại mới của tiếp thị điện tử. Sách không chỉ phù hợp cho các nhà quản lý đang phải đối mặt với các vấn đề tiếp thị diễn ra hàng ngày mà còn là sách nhập môn cho các sinh viên, học viên muốn trải mình vào nghề marketing. Đây không phải là một kho lý thuyết sáo rỗng mà là một lượng kiến thức khổng lồ giúp tôi có một các nhìn tổng quan về tiếp thị và cập nhật những tư duy mới bằng các ví dụ thiết thực từ các tập đoàn lớn mà Kotler đã đi qua. Xin chân thành cảm ơn tác giả và dịch giả
4
66692
2016-04-18 15:00:39
--------------------------
406748
4476
Nói về tiếp thị thì phải nói về Kotler, bậc thầy về marketting. Tuy tuổi già nhưng ông đã có những ý tưởng sáng tạo để tạo nên một trong những bước đột phá trong lĩnh vực Tiếp thị, thật phi thường. Quyển sách đã lột tả những kiến thức về marketting, gần gũi nhưng cũng xa lạ với chúng ta, về cách mà chúng ta đã hiểu sai về tiếp thị và nên cần định nghĩa lại tiếp thị trong thời buổi ngày nay. Cách mà ông bàn về tiếp thị thật dễ hiểu, logic và khoa học qua những chương, mục và bảng, biểu đồ. Thật không uổng công khi mua sách về ông tuy rằng không phải là sinh viên về Marketting!
5
474969
2016-03-28 20:46:18
--------------------------
360612
4476
Sách dày nhưng rất nhẹ, cầm rất thích :D. Mấy cuốn của Kotler in bởi Nhà XB Trẻ cầm đều thích cả. Như là cuốn Tiếp thị mở đường tăng trưởng hay cuốn Thấu hiểu tiếp thị từ A đến Z,.. Mình đang dự định để sưu tầm đủ bộ sách Kotler của Nhà XB Trẻ để cho đẹp tủ sách. Mỗi lần cầm lên nâng niu tung lên vài lần rồi mở ra đọc. Sách của Kotler viết thì khỏi nói về tri thức Marketing trong đó rồi. Mình không phải là dân ngành Kinh tế nên nhiều chỗ đọc phải mất thời gian để tìm hiểu thêm kiến thức khác và từ vựng.
5
734210
2015-12-29 14:58:16
--------------------------
330290
4476
Cung cấp những giữ liệu cơ bản quan trọng cho dân làm marketing. Cuốn sách đáng mua vì bạn sẽ được cung cấp rất nhiều thông tin về tiếp thị để có thể hiểu sâu về mảng này. Kotler Bàn Về Tiếp Thị là một quyến sách về những chỉ dẫn cơ bản về maketing mà ông đã đúc kết lại thêm đó là xu hướng mới về tiếp thị rất hữu dụng dành cho mọi người có ý định làm kinh doanh luôn chứ không riêng gì những ai học marketing. Ngoài ra cuốn sách được in ấn rất tốt nên không lo về bảo quản.
4
906355
2015-11-02 12:23:52
--------------------------
297590
4476
Philip Kotler là một cái tên không hề xa lạ gì với những người học về maketing , những quyển sách và giáo trình của ông được xem là kinh thánh của chủ đề tiếp thị.
Kotler Bàn Về Tiếp Thị là một quyến sách vô cùng nổi tiếng của ông viết về những chỉ dẫn cơ bản về maketing mà ông đã đút kết lại dựa trên những bài giảng của ông trên khắp thế giới.
Theo mình thì đây là một quyển sách toàn diện về cả nội dung lẫn hình thức vì sách  được trang trí đơn giản , hơn thế nữa là dù khá dày nhưng trong lượng vô cùng nhẹ .
5
554150
2015-09-11 23:02:35
--------------------------
288517
4476
Mình là học sinh nhưng rất thích đọc sách. Sau khi đọc quyển Tiếp thị từ A đến Z của bác Kotler mình rất thích phong cách viết và những kiến thức, ý tưởng bác đem lại cho mình nên đã quyết định đặt thêm quyển Bàn về tiếp thị. Tuy nhiên quyển sách này lại có những kiến thức khá chuyên ngành đòi hỏi một nền tảng marketing hoặc về kinh tế ( Mình nghĩ thế) nên có nhiều chỗ mình khá không hiểu. Chắc mình phải cất tủ quyển này vài năm nữa mới dám lấy ra đọc lại :)) bạn nào mua đọc thường thức như mình thì nên đọc kĩ vài trang trước khi chọn mua :)
4
227371
2015-09-03 17:08:05
--------------------------
176782
4476
Mình là sinh viên chuyên ngành kinh tế nên rất có hứng thú với những quyển sách thuộc phạm trù kinh tế. Và ở môn Marketing căn bản thầy mình đã giới thiệu tác giả Kotler như là một bậc thầy về Marketing bán hàng. Ông gắn bó với ngành này gần 40 năm nên kiến thức của ông khá bao quát và đa dạng. Mình đã đọc khá nhiều sách của Kotler và chưa bao giờ thất vọng. Sách khá chi tiết, phân tích rất nhiều mặt của tiếp thị như tiếp thị cơ sở dữ liệu, tiếp thị Internet, tiếp thị toàn cầu ... Với mình, đây là một quyển sách đáng mua và đáng đọc, rất hữu ích với các bạn muốn khởi nghiệp hay dấn thân vào ngành Marketing chuyên sâu.
5
63487
2015-04-01 20:52:48
--------------------------
173908
4476
Tiếp thị cơ sở dữ liệu, tiếp thị quan hệ, tiếp thị toàn cầu, và tiếp thị trực tuyến là bốn khái niệm cơ bản mà chúng ta sẽ học được khi đọc xong quyển sách Kotler Bàn Về Tiếp Thị của cây đại thụ trong ngành Marketing, chuyên gia đã có hơn 40 năm trong ngành. Thông qua những thông tin được chia sẻ trong sách, chúng ta sẽ tìm thấy những cách thức làm Marketing mới và lợi nhuận công ty sẽ tăng lên nếu chúng ta áp dụng những kiến thức mới này vào công việc. Sự thay đổi luôn luôn diễn ra và ngành tiếp thị cũng không ngoại lệ và chúng ta cũng không được nằm ngoài dòng chảy đó.
5
387632
2015-03-27 09:12:30
--------------------------
