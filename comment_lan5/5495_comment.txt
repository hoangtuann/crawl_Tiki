377730
5495
Một cuốn từ điển hữu ích cho học sinh tiểu học. Việc so sánh các cặp từ đồng nghĩa trái nghĩa sẽ giúp các em có giàu vốn từ vựng hơn gấp đôi. Các bé có thể đặt câu với hai từ đồng nghĩa hoặc trái nghĩa, từ đó, khả năng phân biệt từ vựng cũng có tiến triển hơn, rèn luyện trí nhớ cũng tốt hơn. Cách học này tuy giúp các em biết cách tra từ điển và so sánh các cặp từ nhưng nó có một trở ngại là các em khó nhớ hơn so với việc học bằng thẻ. Việc học bằng thẻ có hình ảnh minh họa giúp các em nhớ lâu hơn. Nhưng mình nghĩ, cách nào cũng có ưu nhược điểm của nó. Bew nào hợp với cách nào hơn thì chọn cho bé loại hình đó là tốt nhất.
4
63376
2016-02-03 10:10:53
--------------------------
323232
5495
Quyển từ điển này rất thú vị và bổ ích đối với mình. Nhờ có nó mà mình có thể dùng nhiều từ ngữ khác nhau với nghĩa như nhau khi viết văn và không bị cô phê là lặp từ. Quyển từ điển đã giúp mình soạn các bài học liên quan đến từ đồng nghĩa, trái nghĩa rất nhanh và chính xác. Mà quyển từ điển cũng rất gọn, nhẹ, có thể dể dàng mang trong túi hay trong cặp sách để đi đến lớp. Chữ cũng được in rất rõ nét. Cỡ chữ vừa mắt nên đọc rất dễ dàng.
5
711358
2015-10-18 09:38:03
--------------------------
