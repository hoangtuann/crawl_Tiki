458198
6481
Mình đã mua cuốn sách này nhưng chưa có thời gian đọc, mình mua vì đã từng đọc  cuốn Lịch Sử Nội Chiến Ở Việt Nam Từ 1771 Đến 1802 của Tạ Chí Đại Trường và mình cực thích văn phong của ông. Nhận xét tổng quan của mình đó là giá vừa phải ( chấp nhận được ), bìa khá đẹp, chất lượng giấy tốt dù hơi tối màu. Mình xem qua thì thấy có nhiều bản đồ rõ ràng, số liệu dẫn chứng cụ thể. Một quyển sách các bạn nên đọc qua, đặc biệt với những bạn muốn tìm hiểu về người lính bản xứ thời Pháp thuộc.
Xin cảm ơn.
5
470908
2016-06-24 22:52:27
--------------------------
412197
6481
Cuốn sách như một nguồn tư liệu mới cung cấp thêm một góc nhìn mới về những người lính thuộc địa trong giai đoạn đầy đau thương của dân tộc. Từ đầu tới khi gấp cuốn sách lại tôi cố gắng mường tượng một giai đoạn mà ở đó nỗi thống khổ của người dân Việt Nam khắc họa rõ nét qua chính những công cụ trấn áp của nhà cầm quyền Pháp đối với sự phản kháng của dân tộc. Và ở đó tinh thần quật cường của người dân Việt Nam được nêu bật lên với những cuộc bạo loạn đội Cấn...
4
196740
2016-04-07 11:27:23
--------------------------
286681
6481
Tác giả có cách viết lịch sử điêu luyện và chân xác bằng những chuyến điền dã thực tế gặp những cựu lính tập đầu thế kỷ XX. Công trình có cái nhìn bao quát đến số phận những người Việt chịu chế độ trưng binh của thực dân Pháp ở Việt Nam không chỉ riêng Nam Kỳ. Nổi bật hơn hết là sự kháng cự của người Việt với chế độ trưng binh bằng những cuộc khởi nghĩa và phong trào. Công trình sử dụng nhiều nguồn tài liệu thực tế và lưu trữ có giá trị, chưa một công trình nào vượt mặt.
5
390132
2015-09-01 22:16:39
--------------------------
217550
6481
Cuốn sách này có thể nói là cả một công trình nghiên cứu tỉ mì, kỳ công của tác giả. Dù trước đây ông từng đứng ở phía bên kia trong cuộc chiến nhưng cần tách biệt rõ ràng, ông là một nhà nghiên cứu Sử học nghiêm túc, và các tác phẩm của ông đều có giá trị cao, không bị nhiều vấn đề làm ảnh hưởng.
Đọc Người lính thuộc địa Nam Kỳ có thể  nhìn thấy bối cảnh thuộc địa thời Pháp thuộc. Không chỉ những lát cắt về chính trị, quân sự mà còn về xã hội, về mặt con người. Một tác phẩm giá trị.
4
139133
2015-06-29 15:34:22
--------------------------
130179
6481
Sự kỳ công của tác giả trong cuốn sách này là một điều không thể phủ nhận. Từ đầu đến cuối tác phẩm là những sự kiện lịch sử trải dài, với rất nhiều dữ liệu, luận cứ và diễn giải. Điều làm nên sức hút của tác phẩm là lối viết sắc bén, vừa có tính chuẩn xác vừa có tính văn học, khiến cho những trang sách không trở nên khô cứng, mà hấp dẫn và lôi cuốn. Đặt trong bối cảnh thời kỳ Pháp thuộc, tác giả không đào sâu về những trận chiến như những cuốn sách lịch sử thông thường, mà phân tích những lát cắt về xã hội, quân sự và chính trị đương thời. Có rất nhiều đoạn không hề dễ đọc, dễ hiểu, nhưng khi đã quen với giọng điệu của tác giả chắc chắn bạn sẽ cảm thấy thích thú. Lịch sử ở đây không còn là một thứ gì đó xa xôi, mà giống như những câu chuyện kể đầy ý nghĩa, đem đến nhiều kiến thức bổ ích. Đây là một tác phẩm có giá trị cao, mang lại nhiều cảm nhận mới mẻ cho độc giả.
4
109067
2014-10-15 19:51:03
--------------------------
123112
6481
Tất cả các tác phẩm hay nói đúng hơn là các công trình nghiên cứu của bác đều được thực hiện vô cùng nghiêm túc, tỉ mỉ, với khối lượng kiến thức khổng lồ mang đến cho người đọc. Riêng tác phẩm Người lính thuộc địa Nam Kỳ đã vẽ lên một bức tranh sống động của lịch sử Việt  Nam thời kì cận đại hết sức cuốn hút. Là một người cũng xuất than từ khối chuyên Lịch Sử - Địa Lý, tôi vô cùng thần tượng bác Tạ Chí Đại Trường. Chỉ tiếc một điều là tôi tiếp xúc với các tác phẩm của bác quá trễ, nếu không thì con đường học tập nghiên cứu lịch sử của tôi hiện giờ đã khác.
4
10258
2014-08-30 08:39:05
--------------------------
