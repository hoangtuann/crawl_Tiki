546021
2502
Truyện hay cảm ơn Tiki đã đem đến câu truyện  này.
4
1058386
2017-03-17 20:03:16
--------------------------
517021
2502
Cuốn sách nào cũng vậy, nó đều dạy mình một điều gì đó. Bá tước M. C cũng vậy, truyện dạy mình tha thứ, và phải tự bổ sung kiến thức cho mình bất cứ khi nào, bất cứ hoàn cảnh nào dù đang ở trong tù. Nội dung hay từ lúc anh Dantes không biết vì sao mình bị bắt cho đến lúc gặp được người bạn tù già phòng bên cạnh, được dạy cho kiến thức, chỉ đường tới kho báu, vượt ngục và trở nên giàu có, quay về tính nợ tính ơn với những người trong quá khứ. Tui hết sức thỏa mãn khúc này và càng thỏa mãn hơn khi Bá tước dừng lại ở đoạn kết. 
Nói chung thì đây là một quyển sách đọc mà tui không thấy tiếc! 
5
833314
2017-02-01 00:16:03
--------------------------
459240
2502
Một câu chuyện đầy lôi cuốn về cuộc đời bá tước Monte Cristo. Sách dễ đọc, lười đọc như mình vẫn có thể đọc một lèo hết cuốn sách. Do trước cuốn sách này mình có đọc Ruồi Trâu, cũng có tình tiết tương tự về việc một người bị vu oan rồi một thời gian sau trở về với thân phận mới, có lẽ vì Ruồi Trâu quá hay nên khi đọc tác phẩm này mình chỉ thấy cuốn sách khá thú vị, dễ đọc chứ không đọng lại nhiều cảm xúc. Sách khá dày, cầm chắc tay, bìa hơi mỏng với lại mình không thích thiết kế bìa lắm nhưng nói chung đây là cuốn sách đáng đọc, mua không phí tiền đâu.
3
782605
2016-06-25 22:13:35
--------------------------
455392
2502
Cuốn sách này mình mua thật không phí tiền. Khi nhận hàng, nhìn cuốn sách dày cộm to đùng mình khiếp luôn, nghĩ bao giờ mới ăn xong nó đây. Rồi cũng ráng đọc. Đọc vài trang đầu là cuốn luôn vào truyện, cứ chong đèn đọc mãi không muốn gấp sách lại. Sách rất to và dày nên nằm đọc mỏi tay mỏi cổ muốn chết nhưng cứ thích đào hố cơ -.-"  Sách hay mọi người ạ. Rất hay. Xen lẫn cứ vài trang chữ là có chèn vào một bức tranh vẽ nội dung truyện nên đọc thấy hấp dẫn và dễ hiểu hơn. Tranh được chăm chút lắm chứ không phải vẽ cho có đâu. 
5
890619
2016-06-22 15:33:09
--------------------------
402868
2502
Phải nói là rất rất rất hay. Mình đọc đi đọc lại phải 3 lần rồi ấy chứ. Đọc một lần không thể ngấm được sách này đâu, ít nhất các bạn phải đọc lại 2 lần, tin mình đi.
Giá trị nhân văn rất sâu sắc, dạy cho con người ta biết sống theo lẽ phải, biết chờ đợi, biết hy vọng, biết yêu thương. Ngoài ra cũng cảnh tỉnh những kẻ cơ hội, những mầm móng ác nhân hãy coi chừng quả báo, chúng tuy rằng đến muộn nhưng rất đau đớn đấy.
Bìa sách rất đẹp, đơn giản chỉ là hình của Bá tước nhưng lại khiến người ta cảm thấy tò mò nội dung.
5
168630
2016-03-22 22:48:41
--------------------------
381229
2502
Đây là một tác phẩm văn học nước ngoài kinh điển không thể thiếu trong tủ sách của bạn. Dưới ngòi bút của Alexandra Dumas, từng bối cảnh, từng nhân vật  được tái hiện lại thật sống động. Bao yêu thương, hận thù, dằn vặt đeo bám, đè nén lấy nhau. Tác phẩm lột tả chân thật những toan tính, sự hiểm ác, tham lam, sự khôn lường của lòng người...Các bạn hãy bổ sung tác phẩm này vào bộ sưu tập của mình vì đây là một tác phẩm văn học kinh điển rất đáng để đọc và cảm nhận.
5
892878
2016-02-16 20:50:01
--------------------------
379198
2502
Sách nội dung rất hay, hay hơn phim (vì phim sửa lại nhiều quá), đối thoại và tình huống rất kịch tích. Phải công nhận là nếu mình đọc sách này từ trước mấy năm (khi còn rảnh) thì sẽ còn đọc nhiều cuốn văn học cổ điển nữa. Dịch hay, tên nhân vật giữ nguyên, minh họa đẹp :) Nội dung nói về "ác giả ác báo", nói về người ta chỉ có thể trân quý hạnh phúc khi đã trả qua khổ đâu và trí tuệ gói gọn trong 2 từ: hy vọng và chờ đợi. Triết lý và văn hóa thời đó miêu tả trong sách thật sống động. 
5
270707
2016-02-09 22:44:29
--------------------------
378886
2502
Như tiêu đề, truyện dịch dối và lược bỏ một phần ở đoạn cuối chương 16, khi Edmond làm quen với linh mục Faria. Mình tò mò nên tải bản tiếng Anh về đọc thử thì thấy trong bản tiếng Anh linh mục nói "Tôi nói được 5 thứ tiếng Anh, Pháp, Ý, Đức, Tây Ban Nha và tiếng Hi Lạp hiện đại...." và một đoạn dài sau đó ông chia sẻ đã học thế nào và tự chế tạo bút và mực ra sao. Vậy mà trong cuốn sách chỉ có vỏn vẹn "Tôi nói được 5 thứ tiếng và đang tự học tiếng Hy Lạp mới." và một đoạn ngắn nữa kết thúc chương. Mình không hiểu? Dù vì lý do gì đi nữa thì điều này vẫn là không chấp nhận được. Mình đang cảm thấy mất niềm tin vào sách của Đông A, dường như quá chú trọng vào hình thức.


1
924317
2016-02-08 11:13:28
--------------------------
371287
2502
Truyện kể về một anh chàng nghèo, vì bọn quý tộc ác bá chèn ép mà phải đi lưu lạc. Trên một hòn đảo, anh tìm dc kho báu. Nhiều năm sau anh trở về với cái tên Monte Ceristo, nhưng tiếc là cảnh còn người mất, nhiều thứ đã đổi thay. Cô người yêu của anh nay đã làm vợ kẻ đã chèn ép anh. Cha thì đã mất.
Bá tước Monte Ceristo là tác phẩm kinh điển, truyện truyền tải rất nhiều thông điệp, về những phẩm chất tốt đẹp của con người, cũng như lí lẽ chính nghĩa luôn thắng.
5
112576
2016-01-19 10:45:07
--------------------------
364380
2502
Một tuyệt tác văn chương nổi bật bậc nhất trong kho tàng văn học nhân loại, alexander dumas đưa ta trở về thế kỷ XIX để cảm nhận được xã hội Pháp lúc bấy giờ. Một tác phẩm tuyệt vời kể về nghị lực phu thường của một chàng trai có một tương lai hết sức sáng lạn nhưng lại bị hại và phải chịu cảnh ngục tù. Tác phẩm còn cho ta thấy quy luật nhân quả "ác giả ác báo, ở hiền gặp lành". Tâc phẩm này nằm trong bộ sách kinh điển của Đông A bìa rất đẹp.
4
746395
2016-01-06 05:27:33
--------------------------
364212
2502
Ấn tượng đầu tiên của mình về cuốn sách là BÌA QUÁ ĐẸP. Đọc sách lại càng hiểu thêm về bìa sách. Trong sách được vẽ rất nhiều hình minh họa. Mình thấy hình minh họa này đẹp hơn so với hình trong cuốn Trà hoa nữ. Nội dung thì không còn gì phải bàn, quá hay. Cuốn sách quả là xứng với danh hiệu tác phẩm kinh điển. Cốt truyện chặt chẽ, bản dịch mượt, nhưng vẫn còn một hai lỗi chính tả. Tóm lại đây là một cuốn sách đáng đọc và nên đọc. Mình đang dự định mua thêm cuốn Ba chàng lính ngự lâm cũng của tác giả này. Hi vọng cũng sẽ hay!
5
508249
2016-01-05 19:18:02
--------------------------
363501
2502
Bá tước Monte Cristo là một tiểu thuyết của Alexandra Dumas cha mà tôi đã đọc khi học lớp 6 - cùng lúc với cuốn Hoa tuylip đen của ông. Với tôi, "Bá tước Monte Cristo" tuyệt vời hơn hẳn, nó là một câu truyện về tình yêu, lòng thù hận và sự vị tha mà đến nay khi đọc lại tôi vẫn không thể rời xa nó quá nửa ngày nếu chưa đọc xong. Hơn nữa, tôi đánh giá cao bản dịch của dịch giả Mai Thế Sang khi có những ngôn từ và cách trình bày câu cú rất phù hợp với bối cảnh quý tộc Châu Âu thế kỉ XVII - XVIII. Càng đọc kĩ, tôi càng cảm thấy nghị lực và sự thông minh của bá tước cũng như sự khoan dung độ lượng của 'chàng', rồi cũng cảm nhận được trí tuệ uyên bác, tấm lòng nhân hậu của nhà bác học người ý Faria, toàn thể gia đình ông chủ tàu Morrel, cô tiểu thư Valentine và nàng Mercedes xinh đẹp cùng cậu con trai Albert... .Qua tác phẩm, A.D cũng muốn nói: "Trên đời này không có hạnh phúc mà cũng chẳng có bất hạnh, chỉ là sự chuyện biến từ trạng thái này qua trạng thái khác. Chỉ có những người nào đã trải qua cảnh khổ cực mới hưởng thụ được cảnh sung sướng. Chỉ có kẻ nào sắp chết mới biết cuộc sống là thiên đường"
~~~~~~~ rất cám ơn Tiki đã mang đến cho tôi cơ hội được sở hữu cuốn tuyệt tác này một lần nữa, vì tôi đã tìm mòn lốp xe ở các nhà sách gần nơi tôi ở mà vẫn về cùng nỗi thất vọng =))) ~~~~~~~
4
863916
2016-01-04 14:00:30
--------------------------
359290
2502
Hình thức: sách có khổ to và dày, khi đọc cần dùng cả 2 tay để cầm sách, bìa đẹp, câu chữ được in rõ ràng và được biên tập kỹ.
Nội dung: Đây là 1 tác phẩm kinh điển của nền văn học thế giới, tuy tác phẩm được sáng tác vào thế kỉ 19 nhưng giá trị của nó thì đến hôm nay vẫn còn, nó cho chúng ta 1 cái nhìn sâu sắc hơn về cuộc đời. Bản dịch này là 1 bản dịch hay, chau chuốt, dễ hiểu. Ngoài ra sách còn có thêm nhiều hình minh họa để người đọc dễ tưởng tượng. Sách đạt 9/10 điểm cả về nội dung và hình thức.
4
822161
2015-12-26 21:01:52
--------------------------
358412
2502
Mình mua combo này  như một món quà.  Đã nghe danh thuộc bộ truyện kinh điển, không có lí do gì để mình bỏ lỡ. Cầm trên tay là mình muốn đọc ngay. Bìa cứng, giấy đẹp, cuốn truyện rất thu hút bạn đọc.  Mình mua vào đợt sale của tiki, được giá khuyến mại, giao hàng free,  tận tình. Mình rất hài lòng. Rất hào hứng để tặng nó cho người thân và được thưởng thức nó. Hãy đọc và cảm nhận bạn nhé!  Một gợi ý nhỏ là bạn nên mua vào đợt khuyến mại của tiki để được hưởng nhiều ưu đãi
5
778726
2015-12-25 11:15:28
--------------------------
351181
2502
Nếu ai đã từng đọc và yêu mến tác phẩm "Ba người lính ngự lâm" thì có lẽ sẽ khó bỏ qua tác phẩm "Bá tước Monte Cristo" vì cả hai tác phẩm đều cùng tác giả là Alexander Dumas.
Giọng văn quá thu hút, thật khó để đọc mà không bị nó cuốn đi. Ông thật xứng đáng với những gì người ta thường đồn đại.
Nội dung mang bài học nhân đạo sâu sắc. Kết thúc có hậu.
Về hình thức: Có lẽ mình đã quá tin tưởng vào hình thức sách do Đông A đảm nhiệm nên khi biết Đông A có cuốn này, mình đặt mua ngay. Điều làm mình không hài lòng nhất ở đợt xuất bản này là giấy khá mỏng, in không đều mực. 
Tuy nhiên vì bìa đẹp nên vớt vát lại chút đỉnh.
4
654533
2015-12-11 20:47:57
--------------------------
346036
2502
Sách được bảo quản tốt, không nhăn góc, đóng gói cẩn thận. Nội dung sách thì không bàn cãi nữa, vì là một tuyệt tác văn học, đọc và cảm nhận những triết lý về cuộc đời mà tác giả muốn truyền đạt. Về phần trình bày thì giấy hơi mỏng, mực in không đều lắm, đôi lúc có chữ đậm chữ nhạt. Mình rất thích bìa sách, trông rất cổ điển, sang trọng khi cầm trên tay, tuy nhiên nó khá mỏng và có cảm giác thiếu chắc chắn. Tóm lại là tốt, sẽ tuyệt vời hơn nếu chất lượng giấy tốt hơn. 
4
778730
2015-12-01 19:32:59
--------------------------
343420
2502
Mình đọc cuốn sách này bởi mình đã xem qua phim. Bộ phim rất lôi cuốn và làm hài lòng người đọc bởi cái kết đúng với luật nhân quả. Đọc truyện thì luôn hay hơn phim. Những hình ảnh tác giả gợi lên trong tâm thức người đọc rất sống động, đôi khi rực rỡ, xa hoa, đôi khi đen tối, bẩn thỉu, đó chính là những trải nghiệm cuộc đời của nhân vật chính. Tác phẩm đề cao nghị lực của con người trước những bất hạnh của cuộc sống. Nội tâm của các nhân vật rất sống động và bố cục cũng như cốt truyện rất hay. Một tác phẩm nên đọc!
5
831269
2015-11-26 13:11:03
--------------------------
338996
2502
Mình thích tác phẩm này. Không phải chỉ do bìa đẹp, giấy tốt mà do nội dung của nó rất hay, rất nhân văn và sâu sắc. Chỉ hơi tiếc một chút là truyện chưa có cái gọi là cao trào thực sự, có đôi đoạn khá hồi hộp và cảm động, nhưng chưa được tác giả đẩy lên cao hơn nên mình hơi tiếc. Và công cuộc trả thù của Edmond có vẻ diễn ra hơi thuận lợi, dễ dàng, mình thích nó trắc trở hơn 1 chút cơ. Dù sao đó cũng chỉ là 1 vài cảm nhận cá nhân, chứ nhìn chung thì cốt truyện rất hay và đáng đọc.
4
413317
2015-11-17 10:54:47
--------------------------
325472
2502
Nhìn tổng số thể cuốn sách từ thiất kế bìa đến màu sắc đều mang vẻ cổ điển, tuy giấy có hơi mỏng.
Khi mới đọc mở đầu truyện có vẻ như tách giả muốn lướt qua tổng số thể cho tà biết vì sao Edmon Dantes vào tù, tuy có phần nhanh nhưng vẫn cho tà nắm đc cốt truyện khiến người đọc tò mò mò muốn xem diễn tiến tiếp theo của Edmon sẽ gặp ai ,trả thù ra sao.
Càng đọc càng bị cuốn vào truyện. Tuy ít lời dẫn,ít miêu tả chi tiết như những cuốn tiểu thuyết khác nhưng lại cuốn hút theo cốt truyện bằng những đoạn đối thoại,cảnh tring từng chương.thật sự đây là cuốn scáh tuyệt vời. Xứng tầm với từ kinh điển. Kết thúc ép mãn nguyện vọng người đọc và rất đề cao tính nhân văng,tình người.
4
680642
2015-10-23 17:12:13
--------------------------
322160
2502
Đây là một tác phẩm mình được chú bảo vệ trong cônh ty giới thiệu nên mình tìm mua đọc. Nội dung thì nó đúng là một tác phẩm kinh điển. Nhưng điều mình muốn nói ở đây là hình thức quyển sách. Khi mới nhận đơn hàng mình liền lục tìm ngay quyển này nhưng bìa sách (màu) không giống như hình đăng trên tiki, sách mình nhận hơi cũ, có vài chỗ ố vàng tuy không nhiều nhưng nhìn không thích lắm, bìa hơi mềm so với độ dày quyển sách. Điều mình thích nhất là tên nhân vật đc giữ nguyên, không bị Việt hóa như mấy bản dịch khác. Tuy nhiên đây là một tác phẩm tuyệt vời. :)
5
806143
2015-10-15 18:27:49
--------------------------
321674
2502
Một tác phẩm văn học kinh điển có lẽ ai cũng biết, đã được phát hành rất nhiều lần bằng tiếng Việt với nhiều bản dịch khác nhau.
Ở bản 2015 này, Đông A đã làm rất tốt phần hình thức và trình bày nội dung, các tên nhân vật đều được để nguyên dạng (không phiên âm thành tiếng Việt), chữ in sắc nét, rõ ràng, font chữ giống Adobe Caslon (hay chính là Caslon?!) nên đọc rất dễ chịu và nhanh. Giấy in màu vàng nhạt và mỏng, nếu là loại giấy xốp thì hoàn hảo.
Một bản in xứng đáng có trong bộ sưu tập văn học kinh điển.
5
462641
2015-10-14 16:24:16
--------------------------
304347
2502
Trong số những bản đã xuất bản của quyển sách này thì bản của Đông A là bản mình hài lòng nhất tính tới giờ. Nội dung hay khỏi phải bàn cãi, tên nhân vật được giữ nguyên (chắc hẳn rất nhiều bạn quan tâm đến vấn đề này). Nhiều hình minh họa cực kỳ đẹp và bắt mắt, đây cũng là điểm mình thích nhất. Tuy nhiên mình có 1 vài góp ý đến Đông A là sách có cái bìa không được cứng cáp lắm so với độ dày của nó, và mực in của sách không được đậm và đều.
4
126898
2015-09-16 12:48:33
--------------------------
300057
2502
Ngay từ lần đầu tiên Đông A nhá hàng quyển này mình đã bấn loạn lắm rồi, một phần vì đây là tác phẩm kinh điển yêu thích, một phần vì bìa lần tái bản này đẹp lung linh. Sách kinh điển của Đông A quyển nào cũng hay cả, Bá tước Monte Cristo kể về nghị lực của một chàng trai và luật nhân quả "ở hiền hặp lành, ác giả ác báo". Được viết cách đây đã khá lâu nên văn phong của tác giả giống với các tác phẩm cổ điển khác, đó là kể chuyện rất chậm rãi, tuy nhiên không có nghĩa là buồn chán, mà từng chi tiết kết nối với nhau một cách chặt chẽ, tạo nên một cốt truyện lôi cuốn, mình đọc không dứt ra được.
5
582958
2015-09-13 18:05:19
--------------------------
285103
2502
Là quyển tiểu thuyết được viết từ thế kỉ 19, tuy vậy nó không hề trở thành xa lạ đối với thế hệ ngày nay bởi lời văn lôi cuốn cứ bắt người đọc dõi theo mãi từng trang truyện. Quyển truyện như 1 bộ phim chiếu chậm, từng nhân vật trở nên sống động trước mắt mình. Nội dung và bố cục tuyệt hảo, cứ khiến người xem trầm trồ mãi, tiểu thuyết nêu bật lên giá trị nhân đạo mà muôn đời mọi người tôn thờ "gieo nhân nào gặt quả nấy". Cái kết cục khiến người ta hài lòng, người tốt được báo đáp sau bao bất hạnh, kẻ ác cuối cùng rồi phải nhận quả đắng khiến người ta ngẫm nghĩ lại giá trị của mình ở cuộc đời này để mà rèn giũa bản thân, để tin tưởng vào tương lai. 
Là 1 quyển tiểu thuyết xứng đáng về cả giá trị nghệ thuật và giá trị nhân đạo. Xứng đáng tồn tại mãi trong tủ sách văn học thế giới! 
5
196599
2015-08-31 16:11:07
--------------------------
