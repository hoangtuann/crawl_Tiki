419563
6355
Nhận xét đầu tiên chính là vở mỏng, bìa đẹp. Mở vở ra thì thấy chất lượng giấy tốt, biên soạn, trình bày tỉ mỉ, mình thấy rất ưng ý. Về mặt nội dung thì chính là các bài thơ. Dù sao cũng là lớp năm, không còn tập viết từng chữ như lớp một được. Vừa luyện chữ vừa có thể học thêm những bài thơ hay. Trong sách có hai kiểu chữ là chữ nghiêng và chữ thường làm mình thấy rất hài lòng.
Người ta nói nét chữ nét người, theo mình thì luyện chữ không thừa.
4
806293
2016-04-21 15:35:28
--------------------------
309627
6355
Bà chị của mình cứ than sao con viết chữ xấu, nên mình mua 2 quyển "Luyện chữ đẹp yêu thơ văn lớp 5" tập 1 và tập 2 cho cháu luyện chữ luôn. Thấy loại tập viết có cả thơ văn để cháu vừa luyện chữ vừa học thơ văn cũng thú vị, sẽ không dễ gây cảm giác nhàm chán. Việc viết nhiều có thể giúp trẻ nhớ lâu hơn. Cá nhân mình thấy trẻ viết chữ không cần phải quá đẹp quá chuẩn, chỉ cần chữ dễ nhìn một chút và đúng chính tả là ổn rồi. 
5
276944
2015-09-19 03:38:41
--------------------------
