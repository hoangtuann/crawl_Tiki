401947
5899
Đây là tập hợp những truyện cổ từ Nhật rất hay. Những  truyện này mình chưa đọc bao giờ mặc dù mình là người ghiền truyện cổ tích và đọc rất nhiều truyện cổ tích rồi.
Sách trình bày đẹp, giấy in chất lượng tốt, kèm cả hình minh họa.
Truyện vui, dí dỏm và cũng đậm văn hóa Nhật với tình tiết ma quỷ cũng đáng sợ không kém.
Truyện nào nội dung cũng hay cả, chọn tựa đề "Chuột giã bánh giày" vì đó là truyện vui nhất và bài thơ trong truyện đó nghe rất vui tai, kể cho em bé nhỏ nghe bé rất thích bài thơ đó.
5
429557
2016-03-21 15:45:20
--------------------------
335113
5899
Đây là bộ truyện cổ tích nhưng không dành cho trẻ em, ít ra mình nghĩ nó chưa phù hợp với con mình. Với vài cảnh kinh dị như xương cốt chất cao, quỷ ăn thịt người, còn sót lại một mảnh,... nếu bạn không cân nhắc bé sẽ dễ bị ám ảnh lâu dài. Nếu muốn đọc cho bé bạn phải khá chọn lọc. Còn nếu bé tự đọc được thì tốt nhất nên đợi bé lớn hơn một chút, tầm trên 10 tuổi cái đã. Thế giới sách bao la là, việc gì phải vội.

Tuy nhiên phải thừa nhận bộ truyện khá hấp dẫn, cô đọng, ý nghĩa, và rất Nhật Bản. 
4
197061
2015-11-10 15:55:03
--------------------------
314952
5899
Tập hợp các câu chuyện cổ tích của Nhật Bản, những câu chuyện nổi tiếng, những bài học cuộc sống, những triết lý ẩn chứa, thông qua các câu chuyện ngắn, càng đọc càng thâm thía một phân văn hóa Nhật Bản, văn hóa được thế giới ngưỡng mộ, mỗi người yêu Văn hóa Nhật Bản nên có cho mình một quyển sách như thế này, để hiểu thêm về con người về các câu chuyện về các đất nước. Hình thức: trang trí bìa đẹp, giấy rõ ràng, đọc không bị mỏi mắt, trình bày các mục cụ thể, bắt mắt. 
5
558701
2015-09-27 20:22:40
--------------------------
308270
5899
Những câu chuyện trong cuốn sách này rất giản dị, rất ngắn, mang đậm chất dân gian Nhật Bản. Ngoài các nhân vật người thì yêu ma quỷ quái, các loài động vật xuất hiện rất nhiều và thường xuyên, mang lại cho câu chuyện một chút gì đó hơi đáng sợ và rùng rợn nhưng cũng rất hài hước. Câu chuyện được lấy làm tiêu đề cuốn sách "Chuột giã bánh giầy" cũng là câu chuyện mình thích nhất vì nó rất vui nhộn và ngộ nghĩnh. Sách cũng có nhiều hình minh họa dễ thương và phù hợp với nội dung với các câu chuyện này.
5
31303
2015-09-18 16:21:25
--------------------------
294803
5899
Mình rất thích văn hoá & đất nước Nhật Bản nên mính đã đọc rất nhiều sách của các tác giả Nhật Bản viết về đất nước, con người Nhật Bản. Nhưng truyện cổ tích Nhật Bản thì "Chuột giã bánh giày" là truyện cổ tích đầu tiên mình đọc.Mình thấy truyện rất thú vị và  vô cùng hài hước. Truyện gồm 58 câu chuyện, là 58 bài học đạo lý thấm đẫm nhân sinh quan và thấm đẫm cái nhìn của người Nhật về tự nhiên, vì họ gắn bó mật thiết với tự nhiên.. Trong truyện còn có các hình minh họa vô cùng dễ thương. đọc truyện giúp mình hiểu rõ hơn về phong tục, tập quán, văn hóa của người Nhật

4
512701
2015-09-09 18:37:53
--------------------------
294148
5899
Nhật Bản vốn là 1 nước có nền văn hoá rất thú vị và các luật lệ của họ rất nghiêm khắc, rèn con người rất tốt. Các câu chuyện trong sách rất gần gũi và dễ hiểu, không bị siêu thực và quá xa vời. Hầu hết đều mang rất nhiều ý nghĩa đối với việc rèn nết, rèn người, phân biệt tốt xấu, tính giáo dục rất cao.
Bìa sách đẹp, dễ thương, trình bày tốt và rất được trau chuốt. Vô cùng thích. Sách không chỉ phù hợp cho trẻ con mà cả người lớn cũng nên tìm mua.

4
52328
2015-09-09 00:52:15
--------------------------
278820
5899
Mình rất yêu thích Nhật Bản vì vậy khi tình cờ biết tiki có bán quyển này mình đã mua.Những câu truyện trong sách quả thật rất hay và mang một màu sắc dân gian khác hẳn so với các câu truyện dân gian của các nước khác.Tuy cũng có những cái xấu cái ác và hướng người đọc đến Chân Thiện Mỹ nhưng ở đây các truyện không thần thánh hóa mà rất gần gũi và thực tế.Truyện hay cùng với hình thức bìa đẹp mắt và chất lượng giấy rất tốt.Đây quả thực là quyển sách xứng đáng 5 sao.
5
368991
2015-08-26 10:53:18
--------------------------
274916
5899
Mình rất thích truyện cổ tích và rất thích Nhật Bản vì vậy không thể bỏ qua cuốn sách này. Cuốn sách mang đến cho mình nhiều thêm những hiểu biết về nước Nhật cũng như văn hóa của họ, các câu truyện rất hài hước, các câu truyện đa dạng và có nhiều yếu tố khá bất ngờ, mình có kể cho các bé nghe vài câu truyện thì thấy các bé rất thích, tuy nhiên theo mình thì có một số truyện không nên kể cho trẻ nhỏ nghe. 
Trong sách còn các tranh minh họa dễ thương, nét vẽ đơn giản nhưng rất phù hợp với từng câu truyện, mình rất hài lòng với cuốn sách này.
5
145252
2015-08-22 13:32:11
--------------------------
262518
5899
Truyện cổ tích Nhật Bản giàu hình ảnh và tinh tế. Nó mang nhiều giá trị tinh thần đạo đức đặc trưng của người Nhật mà mình ngưỡng mộ. Sau có con mình cũng sẽ cho đọc truyện cổ tích Nhật Bản. Truyện cổ tích Việt Nam, dù rất yêu nước, nhưng mình nghĩ hiện tại chúng không được đầu tư, và nhiều cái cũng không còn giá trị thực tiễn. Mong một ngày nào đó, nhà xuất bản nào đó có thể đầu tư sưu tầm, trau chuốt ngôn từ hình ảnh cho một cuốn sách cổ tích Việt Nam đẹp đẽ và đáng đọc như cuốn cổ tích Nhật Bản này. Khen một lần nữa, một quyển sách cực kì đáng mua. 
5
739106
2015-08-11 22:33:46
--------------------------
246327
5899
Sách kể về các câu chuyện loài vật, người xấu người tốt.... để bé rút ra được bài học. Có những đoạn lặp đi lặp lại trong cùng 1 câu chuyện để bé dễ nhớ. Có hình vẽ minh họa nên cũng thu hút trẻ nhỏ, tuy nhiên sẽ phù hợp hơn nếu bé trên 5 tuổi, vì bé nhỏ tuổi thích những quyển sách có nhiều tranh vẽ, quyển sách này nội dung chuyện kể phong phú nhưng tranh vẽ ít.
Bé nhà mình cũng rất hứng thú mỗi khi mẹ kể chuyện cho nghe, nội dung tương đối dễ hiểu.
3
304896
2015-07-29 17:54:38
--------------------------
242173
5899
Mình mê đất nước Nhật Bản lắm! Thấy bạn bè giới thiệu quyển này nên mua về đọc thử. Quả thật rất hay! Truyện cổ tích nhưng không theo một mô típ chán ngấy như ta thường lấy. Truyện có phần hài hước, rùng rợn và đơn giản. Đọc truyện giúp mình tìm hiểu thêm được nhiều phong tục, văn hóa của người Nhật. Bên cạnh những câu chuyện luôn có những bài học, giáo dục đáng suy ngẫm. Đây là một quyển sách không chỉ dành cho trẻ em mà người lớn cũng nên đọc để hiểu biết nhiều hơn.
5
303016
2015-07-26 10:41:20
--------------------------
234025
5899
Chuột giã bánh giày, Những câu chuyện cổ tích Nhật Bản, rất nhiều người ca ngợi, và vô tình được chương trình tivi giới thiệu, nên mình đã đặt mua, và đọc với những câu chuyên vui nhộn, Và đậm đà bản sắc của Nhật Bản giúp mình tìm hiểu biết thêm nhiều về nền văn hoá của Nhật. Những câu chuyện cổ tích rất hay, đáng để đọc lắm.
Giá sách trên tiki lại rất rẻ và còn hàng, chứ bên ngoài hỏi mua không có. Mua sách trên tiki là lựa chọn hàng đầu của mình.

5
323719
2015-07-20 07:59:31
--------------------------
229993
5899
Chuyện chuột giã bánh giầy là tuyển tập các câu truyện cổ tích Nhật bản, khác với các chuyện cổ tích việt nam, các câu chuyện cổ tích Nhật bản khá lạ và thường là gắn liền với các yếu tố nhân vật trong thiên nhiên.
Nói chung mình lớn rùi lên cũng không còn thích các câu chuyện cổ tích nữa vì phần lớn nó đêu có nội dung đơn giản và dễ đoán nhưng bạn nào muốn tìm hiểu thêm về văn hóa nhật bản thì cũng nên đọc thử nhé mình thấy cũng có những truyện hay hay và ngẫm ra thì cũng có những bài học đáng suy nghĩ
4
153008
2015-07-17 10:27:32
--------------------------
215168
5899
Thường khi nghe đến truyện cổ tích dân gian sẽ nghĩ đến chữ chán ngay, nhưng quyển sách này thì hoàn toàn ngược lại, thú vị, hấp dẫn và hài hước vô cùng. Những câu chuyện được kể đều hết sức giản dị và đầy tính chiêm nghiệm, thể hiện một phần nào đó khía cạnh tính cách con người và văn hóa của đất nước Mặt Trời Mọc, họ gắn bó với thiên niên, có niềm tin lớn lao vào cuộc sống, luôn lạc quan và yêu đời.
Đây là quyển sách bổ ích với cả người lớn và trẻ em.
4
30924
2015-06-25 23:26:42
--------------------------
175601
5899
Vốn rất yêu thích văn hóa Nhật Bản và khâm phục con người của xứ sở hoa anh đào này nên mình quyết định mua cuốn này về đọc để tìm hiểu sâu hơn về văn hóa Nhật Bản.
Khác với cổ tích Việt Nam, cổ tích Nhật Bản rất hài hước. Hơn thế nữa nó còn thẫm đẫm nhân sinh quan cũng như cái nhìn của trẻ em, và thấm đẫm cái nhìn của người Nhật về tự nhiên vì họ gắn bó mật thiết với tự nhiên. Có thể nói nó chứa đựng sâu sắc những trí tuệ căn bản của đời người và được truyền qua nhiều thế hệ.
Đây là cuốn sách rất đáng đọc, không riêng gì đối với trẻ em mà còn cả người lớn.
5
445522
2015-03-30 17:25:45
--------------------------
171241
5899
"Chuột giã bánh giày" là cuốn sách tập hợp khá nhiều những câu truyện cổ tích Nhật Bản nổi tiếng, chứa đựng ý nghĩa nhân văn sâu sắc. Năm mươi tám câu truyện là mười tám bài học được kể một cách dung dị, chân chất mà lại vô cùng hài hước, thú vị như con người nơi đây. Cách xây dựng cốt truyện cũng như cái kết truyện thì vô cùng bất ngờ, không giống các truyện cố tích Việt Nam cho lắm. Các bài học đạo lí mà năm mươi tám câu truyện mang đến cũng rất rõ ràng, tự nhiên. Một cuốn truyện không chỉ dành cho trẻ em mà còn dành cho người lớn chúng ta nữa
5
417795
2015-03-21 15:07:30
--------------------------
166879
5899
Ngay từ đầu, cái tiêu đề "Chuột Giã Bánh Giầy" khá hài hước. Vì thế mình mới quyết định mua cuốn này.

Nội dung là các câu chuyện cổ tích của Nhật. Câu chuyện khá hài. Ngộ nhất là cái khúc chuột đem tiền ra phơi cho khô rồi đem vô. 
Rồi tới chuyện "chuột đấu vật" cũng rất bựa ... (đọc đi rồi biết)

Về hình thức, sách khá đẹp. Bìa cứng, bóng. Ruột sách giấy dày, khó rách. Giá bán sách thì theo mình nó cũng rẻ.
Mình khuyên các bạn nên mua cuốn này nếu bạn yêu văn hóa Nhật, hoặc đơn giản là chỉ thích chuột và tò mò muốn biết chuột giã bánh giày như thế nào.
5
449008
2015-03-13 19:01:32
--------------------------
148060
5899
Không lạ gì truyện Nhật, nhưng khi nghĩ đến truyện dân gian thì mình hơi rợn, tại vì Nhật nổi tiếng với truyện ma mà. Thế mà hôm nay đọc quyển này, phải nói là mình thay đổi cách nghĩ hoàn toàn. Người Nhật hài hước và giản dị hơn nhiều cái cách họ hay thể hiện. Truyện cổ của họ cũng thú vị lắm, chứ không phải toàn truyện kể về yêu ma quỉ quái. Like quyển này, và thêm điểm cộng cho Nhật Bản - vốn đã rất tuyệt vời với nhiều truyền thống văn hóa đặc sắc.
5
532207
2015-01-09 18:15:49
--------------------------
133837
5899
Vì mình rất thích văn hóa và đất nước Nhật bản nên đã quyết định mua quyển này về xem thử truyện cổ tích Nhật Bản với các chuyện cổ tích mình đã đọc có những đặc điểm gì nổi bật hay khác nhau không. Và thật sự là mình vô cùng bất ngờ khi đọc quyển này. Từng truyện trong sách hài hước không thể tả. Lúc nào đọc hết một chuyện là mình không thể không cười sằng sặc với cách hành văn và cốt chuyện không thể nào đoán được sẽ kết thúc như vậy. Nói chung, sau khi đọc quyển này, mình có một cái nhìn hoàn toàn mới về người Nhật và văn hóa của họ. Thật sự không giống bất kì quốc gia nào về độ "Bựa".
5
119078
2014-11-07 08:27:11
--------------------------
