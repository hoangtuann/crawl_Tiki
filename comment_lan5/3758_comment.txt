492235
3758
Mình đã lầm tưởng về cuốn sách này! Mình cứ tưởng nó là sách dạy cách phản biện ai ngờ sách khó hiểu! Ở trường mình ghét mác gặp sách viết như mác ấy, lỡ mua rồi thì phải đọc ngiềm ngẫm thôi chứ sao bây giờ hic! Đọc được vài trang nó chán thôi rồi luôn
2
938635
2016-11-23 21:36:57
--------------------------
484853
3758
Sách là một cuốn cẩm nang theo mình khá phù hợp với bạn trẻ đang đi học hay đi làm. Cẩm nang tư duy phản biện là lời khuyên, thông qua vấn đặt ra để tạo thêm khả năng tư duy vấn đề và cách phản biện lại vấn đề được đặt ra.mình biết đếm quyển sách này nhờ thầy giới thiệu, trong lớp sử dụng sách nầy làm tài liệu học tập. Nhờ đó, lớp học có nhiều tài liệu nghiên cứu trong giờ học môn Art, tài liệu khá chuyên sâu, nhìn tổng quát đến chi tiết các lập luận, tư duy, và phản biện

3
1080756
2016-09-27 17:16:27
--------------------------
463464
3758
Việt Nam với nền giáo dục thiếu tính phản biện, chỉ theo lối giáo dục 1 chiều nên học sinh đa phần thụ động. Đây là cuốn cẩm nang theo tôi thì phù hợp với đa số bạn trẻ, đang là học sinh hoặc sinh viên, hay đã đi làm rồi. Sách bao gồm những lời khuyên giúp bạn hình thành thói quen tư duy phản biện, lập luận để đào sâu khai thác vấn đề, từ đó dẫn tới những cách giải quyết vấn đề triệt để, hoặc mở lối, khai sáng cho những ý tưởng sáng tạo mới phát sinh.
4
861781
2016-06-29 12:24:26
--------------------------
423822
3758
Là một hình thức tư duy phân tích đào sâu vào bản chất, cái cốt lõi, thứ phản ảnh chính sự vật và hiện tượng. Cuốn sách này bạn có thể thất vọng khi nhìn vào vẻ ngoài -- rất mỏng ( đó là cảm giác của tôi khi cầm ) nhưng nội dung thì hoàn toàn không thể chê được. Với critical thinking, bạn sẽ không những đào sâu được bản chất của vấn đề, mà thông qua đó, còn có khả năng sáng suốt trước những thứ làm bạn mê hoặc. Một khi bạn áp dụng các chuẩn mực trí tuệ một cách chặt chẽ và nghiêm khắc, sẽ không ai có thể lừa dối được bạn. Trong cuốn sách này có những mục tiêu chuẩn tư duy khác nhau cho từng đối tượng ( nghiên cứu, bài báo, kiểm kê lập luận)... Nếu thấm nhuần và thực hành nó mỗi ngày, chúng ta chắc chắn sẽ tiến sâu hơn trong các lĩnh vực như khoa học, kinh tế học, quản trị học,,,, 
4
876303
2016-04-30 01:06:55
--------------------------
400599
3758
Sách mỏng và giá không cao nhưng thực sự rất hữu ích, và nội dung chứa đựng thì rất thâm sâu. Nếu bạn nào đang theo học hoặc hứng thú với các đề tài triết học thì nên mua quyển này về nghiên cứu. Vì như ta đã biết tư duy biện chứng, các mặt mâu thuẫn đối lập luôn vì đấu tranh vừa hài hòa với nhau. Điều này tồn tại trong mọi vật và trong mọi vấn đề vì thế để hiểu được bạn cần đi tới tận cùng của chân lý nghĩa là tự mình thấy được nguồn gốc đó.
5
465332
2016-03-19 16:32:53
--------------------------
293795
3758
Tư duy phản biện là một trong những Tư duy cần có của mỗi con người. Như một nhà xã hội học đã nói: " Một xã hội mà không có phản biện là một xã mù." Do đó, con người cần trang bị cho mình tư duy phản biện khi bước chân ra ngoài xã hội.
Phản biện được hiểu nôm na là "nói ngược", tức ra đưa ra những quan điểm, lập luận để phân tích ngược lại vấn đề mà quan điểm của một ai đó đưa ra, Tư duy phản biện nó khác với tư duy tranh luận. Nhìn qua có vẻ giống nhưng phản biện là một mảng riêng biệt nhưng được bao hàm trong tranh luận. Muốn phản biện được tốt, cần có sự tư duy và dây dựng được kỹ năng tư duy phản biện. Và cuốn sách này sẽ đem đến cho các bạn một cái nhìn rõ ràng về tư duy phản biện, hãy đọc để chiêm nghiệm và hiểu được thực sự tư duy phản biện là gì? và cách xây dựng nó như thế sao cho phù hợp với cuộc sống đầy rẫy vấn đề tranh luận
4
596522
2015-09-08 18:42:57
--------------------------
288314
3758
Khái niệm tư duy phản biện ở Việt nam thường ít được đề cập. Do không có kiến thức nền, nên những cuộc phản biện thường đi đến sự công kích cá nhân, thay vì tập trung vào giải quyết vấn đề. Cuốn sách dành cho những bạn muốn tìm hiểu, và áp dụng để tránh những bẫy trong phản biện. Sách mỏng nhưng là một kĩ năng cần thiết và quan trọng, nhất là những sinh viên, thường xuyên phải phản biện với hội đồng khoa học. Cuốn sách hay, đáng đọc và ứng dụng trong cuộc sống hàng ngày.
4
37796
2015-09-03 15:08:19
--------------------------
243221
3758
Theo mình nghĩ thì cuốn sách này phù hợp với những người vốn dĩ đã có tư duy tốt một tí, cỡ học sinh cấp 3 chưa chắc đã tự đọc và vận dụng được. Tuy nhiên, vì cuốn sách chủ yếu là nêu lên những khái niệm và vấn đề cơ bản nhất của tư duy phản biện nên bản thân mỗi người có thể tự do hiểu, suy luận và mở rộng theo nhiều cách khác nhau có định hướng. Theo mình nghĩ, những người có trình độ khác nhau sẽ có mức độ tiếp thu nhiều ít khác nhau. Điều mình thích nhất là cuốn sách cung cấp những gợi ý phân tích, tiêu chí đánh giá lập luận, nghiên cứu. Nó khiến cuốn sách tuy rất bao quát, súc tích nhưng có tính định hướng rõ ràng. Mình nghĩ cách tốt nhất để lĩnh hội cuốn sách là đọc kĩ các khái niệm cơ bản nhiều lần và thực hành liên tục với các hướng dẫn.

Nói sang bọc sách một tí, tiki bọc cho mình cuốn này chán quá trời, nhăn nhúm hết sách tuiiii.
4
431364
2015-07-27 11:59:31
--------------------------
240481
3758
Vì đây là cuốn cẩm nang nên cuốn sách gần như chỉ cô đọng  những ý chính của tác giả và hầu như không diễn giải chi tiết, mở rộng... nhưng cẩm nang sẽ giúp bạn nhìn ra những thiếu sót, mơ hồ trong tư duy của bản thân và chỉ ra những phương pháp cụ thể tư duy có hệ thống, tư duy phản biện vô cùng cần thiết trong học tập, nghiên cứu đặc biệt đối với sinh viên. 
Tuy nhiên, đây là sách học nên chỉ có thực hành nhiều mới thực sự hiểu được trọn vẹn.
4
421938
2015-07-24 17:06:23
--------------------------
213365
3758
Tư duy phản biện còn rất mới lạ trong giáo dục Việt Nam nhưng ở nước ngoài thì đây là một môn học vô cùng thú vị , nó giúp ta tư duy một cách khoa học và cặn kẻ .Vì ai cũng có tư duy , tuy nhiên nếu chúng ta bỏ mặc nó thì nó sẽ bị bóp méo và rời rạc.
Quyển cẩm nang sẽ cho bạn hiểu thêm về những yếu đó hình thành nên tư tưởng , ba cấp độ tư tưởng ,các chuẩn trí tuệ phổ quát và những cấp độ của tư duy vô cùng thú vị .
Mình đã mua trọn bộ cẩm nang này và vô cùng vừa ý . Xin cảm ơn .
4
554150
2015-06-23 18:29:25
--------------------------
