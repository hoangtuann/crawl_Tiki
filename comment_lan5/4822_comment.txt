552293
4822
Mình đặt mua sách từ 04/2016 vì lúc đó tiki có đợt khuyến mãi, mãi đến gần cuối năm mới có dịp về nc. Thật sự rất thích thú với khả năng sáng tạo và cách vẽ của tác giả. Nhưng mà... chất lượng của sách ko hề tốt chút nào, gáy sách đã bung chỉ ra, nhiều trang rời rạc, cảm giác ko xứng đáng với số tiền bỏ ra, thật sự là vậy... nên mình mong sau này sách sản xuất ở vn - đặc biệt là artbook cần được lưu tâm hơn ko những về nội dung bên trong mà cả chất lượng bên ngoài.
3
295655
2017-03-23 23:15:51
--------------------------
486512
4822
Sách không có phần đọc thử nên khi nhận sách không đúng ý của mình uổng tiền .... tranh vẽ tốt nhưng mình cần là một bức ảnh thật như xưa chứ không phải là ảnh khắc họa theo kiểu cho những kiến trúc sư hay sinh viên đồ họa . . 
1
626781
2016-10-14 10:18:59
--------------------------
287751
4822
Đây là cuốn sách art book đầu tiên mà mình mua ở Việt Nam. Mình đã mua nhiều cuốn art book nước ngoài, thì cảm thấy chất lượng của cuốn artbook này là khá ổn. Rất phù hợp với người yêu sài gòn cũng như du khách nước ngoài. Giá của cuốn sách tương xứng với chất lượng sách. Sách có hộp sách in ánh kim, các hình vẽ theo lối artline, có đệm màu nước. Màu sắc in lên rất thật và rất đẹp. Rất tốt. Mình sẽ luôn ủng hộ các artist Việt Nam phát hành sách artbook. 
5
174257
2015-09-02 23:30:13
--------------------------
246119
4822
Đây là một cuốn sách rất hay và được minh họa bằng màu nước của một sinh viên có cái nhìn về Sài Gòn xưa bằng những cung bậc cảm xúc qua những nét cọ màu nước , hoặc những đường nét minh họa bằng Artline rất nghệ thuật , cái màu trắng đen của bìa sách nó gợi cho ta những cảm giác xa xưa củ Sài Gòn , những ký ức về kiến trúc công trình của Sài Gòn tráng lệ lúc bấy giờ , thể hiện một chốn phồn hoa lệ của Sài Gòn , lịch sử nơi ấy . Cuốn sách cũng là sự khơi gợi cho những người con xa xôi không ở Sài Gòn , nhưng họ vẫn có thể có cảm giác như đang ở trong lòng quê hương , vùng đất phía nam này ....
5
721282
2015-07-29 17:37:10
--------------------------
242882
4822
Thật sự không hối tiếc gì khi mua cuốn sách về. Một quyển sách đẹp từ ngoài vào trong. Quyển sách đẹp từ hình vẽ, thiết kế, font chữ và cả nội dung. Mọi thứ rất hoàn hảo, cho thấy sự đầu tư rất kì công của tác giả. Quyển sách đã tái hiện lại một Sài Gòn với những công trình rất quen với những màu sắc rất thú vị và những công trình xưa và thay đổi nhiều nhưng với một phong cách có một chút gì đó hoài niệm, chút gì đó hiện đại, có xưa nhưng cũng rất mới. Nó khiến ta yêu lại những cái gì đó xưa cũ thân thuộc của Sài Gòn nhưng ta lỡ 1 lần quên. Một quyển sách tuyệt vời cho ai muốn khám phá Sài Gòn xưa cũ. 
5
590518
2015-07-27 01:43:17
--------------------------
233374
4822
Một cuốn sách nặng về chất! Mình thực sự rất vui khi cầm nó trong tay, một cuốn sách đẹp từ ngoài vào trong, khiến mình nâng niu nó, gìn giữ nó như nâng niu và gìn giữ Sài Gòn. Nét vẽ của Trọng Lee là sự sáng tạo về những công trình đã và đang tồn tại trong lòng thành phố, khơi gợi trí tưởng tượng và lòng yêu mến của người đọc, đặc biệt là trong thời đại ngày nay, khi thành phố đang đổi mới và các bạn trẻ dường như "quên không yêu" những gì đã quá thân thuộc quanh mình!
5
590048
2015-07-19 16:35:09
--------------------------
214716
4822
Thật ra để "Đừng đánh giá cuốn sách qua cái bìa" cũng hơi khó, vì cái bìa và thẩm mỹ của nó là yếu tố đầu tiên ghi điểm trong mắt người đọc, trọng lee rất sáng tạo trong việc thiết kế hộp sách, bìa sách, bố cục của nội dung, font chữ và hình vẽ rất hài hòa, rõ ràng, dễ tưởng tượng cho cả những người chưa từng bước chân đến đất Sài Gòn có một cái nhìn tổng quát về lịch sử các công trình, văn hóa, tín ngưỡng nơi đây với những nét vẽ sinh động và có hồn, màu sắc nhẹ nhàng, ko quá rập khuôn
5
488985
2015-06-25 13:44:06
--------------------------
204987
4822
Sài Gòn xưa và nay là một bộ sách kết hợp với tranh rất tuyệt vời và thú vị. Những hình ảnh thể hiện một Sài Gòn rất mới, rất khác, mang chút gì đó xa xưa nhưng cũng đậm chất phương Tây. Chợ Bến Thành, Nhà thờ Đức Bà,... được thể hiện lại qua nét vẽ đầy tinh tế, một chút xưa một chút mới. Mình rất thích quyển sách này, nó rất tuyệt vời.
Không chỉ nội dung bên trong đẹp và tinh tế mà bìa sách cũng rất ấn tượng, mình rất thích.
Quả thật mua quyển này thật xứng đáng!
5
621744
2015-06-05 14:25:43
--------------------------
184924
4822
Bộ sách tranh "Sài Gòn xưa - Màu hoài niệm" làm mình ấn tượng ngay cái nhìn đầu tiên. Hình vẽ khá đẹp, nhưng cũng có những nét phá cách độc đáo. Bộ sách tranh như tái hiện lại một Sài Gòn mờ ảo , diễm lệ , mang đầy hơi thở cuộc sống ; một Sài Gòn được mệnh danh là hòn ngọc Viễn Đông với những dấu ấn lịch sử như nhà thờ Đức Bà, chợ Bến Thành, nhà hát Thành phố.... Đây quả là một món quà độc đáo cho những người dân xa xứ hay những người bạn ngoại quốc
4
293528
2015-04-18 16:34:23
--------------------------
