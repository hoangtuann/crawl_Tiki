472084
4671
Mình vẫn luôn thích những quyển sách của báo hoa học trò xuất bản, có lẽ vì hầu hết những câu chuyện trong đó là nói về tình cảm của tuổi học trò- một thứ tình cảm trong sáng, mãnh liệt, khó có thể quên được. Quyển sách " Hãy nói yêu thôi, đừng nói yêu mãi mãi - lắng nghe lời thì thần của trái tim" này cũng vậy. Những câu chuyện nhẹ nhàng mà sâu lắng, mang theo nhiều cung bậc cảm xúc cho người đọc.
Ngoài ra quyển sách này còn có một điều đặc biệt là xuất hiện thể loại truyện đôi nữa.
5
398235
2016-07-09 09:44:40
--------------------------
380479
4671
Trái tim tuổi teen vốn bướng bỉnh, ngốc nghếch, nhạy cảm và cần lắm những sẻ chia. Bởi thế, Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi ra đời, là để trao cho teen một cái nắm tay, một cái ôm ấm áp, để những tâm hồn đồng điệu có thể kết nối với nhau qua trang sách.
Với Lắng nghe lời thì thầm của trái tim, độc giả sẽ được tiếp cận với “truyện đôi” - một hình thức truyện ngắn rất mới và thú vị. Ba tác giả Hoàng Anh Tú, Ploy và Phan Hồn Nhiên sẽ làm độc giả bất ngờ với 3 cặp “Truyện đôi” sâu sắc và ý nghĩa tạo thành một dòng chảy cảm xúc rất đỗi ngọt ngào!
5
720428
2016-02-15 10:07:09
--------------------------
375816
4671
"Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi - Lắng Nghe Lời Thì Thầm Của Trái Tim" đến với mình một cách tính cờ qua lời giới thiệu của một người bạn, và cái tình cờ bỡ ngỡ ấy trong dịp cuối năm này khiến ta càng thêm mong chờ và khát khao hơn nữa. Mỗi câu chuyện ngắn đều mang trong mình những màu sắc rất riêng, chúng như những ngọn gió lạ đến từ những con người trẻ khát khao tìm được hạnh phúc cho mình, những ngọn gió ấy sẽ sưởi ấm lan tỏa trong trái tim cô đơn của mỗi người. Chúng ta đều còn trẻ, đều không tránh khỏi những sai lầm trong tình cảm, chỉ mong những sai lầm ấy không làm tổn thương quá nhiều với người ta đã từng yêu thương
5
634785
2016-01-29 11:34:23
--------------------------
332894
4671
Mình rấr rất là không hài lòng với chất lượng bản in của ba bản "Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi":
Lắng Nghe Lời Thì Thầm Của Trái Tim
Trái Tim Tỉnh Thức
Như Chờ Tình Đến Rồi Hãy Yêu

Cả ba cuốn mình mua đều bị lỗi bìa sách bị hụt một khúc so với khổ giấy chung gây ra những page đầu bị sờn lên do không được bảo vệ tốt. Mình đã liên hệ với bên "chăm sóc khách hàng" nhưng được trả lời là nguyên lô đó cuốn nào cũng bị nên không đổi cho mình.

Mỗi lần cầm cuốn sách lên là mình lại tụt hết cả cảm xúc, chẳng thưởng thức được gì hết. Rất là bực mình :(
1
712799
2015-11-07 08:53:14
--------------------------
306126
4671
Đây là 1 tác phẩm lãng mạn, nhẹ nhàng dành cho giới trẻ. Với 20 câu chuyện ngắn của nhiều tác giả trẻ Việt Nam truyện đã đưa mình đến với vùng đất của tình yêu để trải nghiệm đầy đủ những cảm xúc, những rung động của thứ tình cảm thiêng liêng này. Tuy 20 câu chuyện đều viết về tình yêu nhưng mình đọc lại không thấy chán. Ngoài ra mình còn như được đắm chìm vào cảm xúc của các bạn trẻ trong đây. Tại đây mình cảm nhận được chút nhẹ nhàng, chút say đắm, chút yêu thương của các nhân vật và điều này đã khiến mình không thể rời bỏ cuốn sách được. Rất mong chờ các tập tiếp theo
4
39383
2015-09-17 13:08:28
--------------------------
303041
4671
"Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi - Lắng Nghe Lời Thì Thầm Của Trái Tim", quyển sách với 20 câu truyện ngắn, 20 câu truyện khác nhau của những tác giả trẻ. Quyển sách làm rung động trái tim bạn đọc, và đặc biệt mình rất thích lối viết truyện đôi, truyện trong truyện của quyển sách này. Dư vị tình yêu, ngọt ngào , cay đắng, buồn tủi đều đọng lại trên từng trang sách.Mình bị ấn tượng với những truyện ngắn của Minh Nhật, rất Minh Nhật! Sách có hình thức đẹp, tiki bọc cẩn thận và giao hàng rất nhanh. 
5
685939
2015-09-15 16:53:42
--------------------------
252552
4671
Lần đầu tiên mình đọc 1 cuốn trong serri 3 tập của" Hãy nói yêu thôi, đừng nói yêu mãi mãi" lúc đầu tựa đề khiến mình không thích lắm bởi mình là tuýp người chug thuỷ. Nên mình không mua nhưng được tiki tặng vào hoá đơn trên 500 ngàn nên mình đọc thử. Nó khiến mình lớn hơn trong suy nghĩ: không có gì bất biến đặc biệt đối với con người, người nào nói yêu bạn, hãy tin, còn ai nói bạn yêu bạn mãi mãi, là không chân thật. Đọc xong cuốn thứ 1, mình đã đặt mua trọn bộ của truyện.
Những tản văn muôn màu, kết thúc bất ngờ. Giờ nguyên bộ nằm trong kệ sách mình rồi, lâu lâu lấy ra đọc lại.
Cám ơn tiki
3
684106
2015-08-03 19:15:04
--------------------------
252034
4671
Bìa sách rất đẹp và ấn tượng. Ngay từ khi mới nhìn thấy, tôi đã liền muốn mua ngay để về nhà nghiền ngẫm. Nội dung sách cũng rất tuyệt, đôi khi có những truyện đọc thấy thật sâu lắng và nó cứ lắng đọng mãi trong tâm trí, trong tim. Đọc nó giúp ta hiểu hơn về chính ta trong sâu thẳm tâm hồn, hiểu hơn về mọi thứ xung quanh và nhận ra những gì giản đơn nhất. Sách khổ dài cho cảm giác dễ đọc, dễ cầm, giấy tốt và tôi đặc biệt ấn tượng với những trang vẽ kèm theo trong cái mẫu truyện.
4
531757
2015-08-03 12:39:47
--------------------------
232604
4671
Lắng nghe lời thì thầm của trái tim - quyển sách đã từng tạo nên một hiện tượng trên thị trường nay đã trở lại với một diện mạo hoàn toàn mới vô cùng đẹp mắt. Không chỉ là 16 truyện ngắn về tình yêu với đầy đủ những cung bậc đến từ các tác giả quen thuộc như Hoàng Anh Tú, Ploy, Phan Hồn Nhiên, Phan Ý Yên, Dương Thụy...mà còn có 10 cảm thức chứa đựng nhiều giá trị sâu sắc về cuộc sống. Chất lượng giấy tốt, minh họa đẹp mắt lại còn tặng kèm bookmark nữa nên quyển sách khiến mình rất hài lòng.
4
330563
2015-07-18 21:48:28
--------------------------
232577
4671
Mình nhớ không lầm thi "Hãy nói yêu thôi 2 - Lắng nghe lời thì thầm của trái tim" chính là quyển sách khiến mình tìm mua cho đủ bộ "Hãy nói yêu thôi" của 2!. Cuốn sách quả thật khiến mỗi độc giả phải xúc động, mình yêu cuốn sách cũng vì truyện đôi của Ploy và Hoàng Anh Tú. Mỗi câu chuyện đều mang đầy nét trẻ, có những truyện ngắn, lời kể ngắn gọn súc tích nhưng để lại nỗi day dứt khôn nguôi cho độc giả, cũng có những truyện viết thật nhẹ nhàng,... chính những phong cách rất khác nhau của từn tác giả đã tạo nên ấn tượng khó phai, khiến mình phải sưu tập các cuốn sách tiếp sau, mặc dù bộ ba Hãy nói yêu thôi mình không có dịp mua.
Lần tái bản này mình thật sự vui mừng và mua ngay. Tuy nhiên về hình thức sách thật đáng thất vọng. Bìa sách được vẽ lại đẹp, mỗi tập là một màu chủ đạo riêng, nét vẽ đặc trưng của Kim Duẩn khinế bìa sách rất bắt mắt, phần ruột cũng được giữ nguyên, trừ phần phụ trương của đợt đầu xuất bản là không có; nhưng phần giấy in thì là một thảm hoạ: giấy quá mỏng,, in thấu trang, đọc không dám lật mạnh tay vì có cảm giác giấy dễ rách. Và sách in gáy chưa đều, giấy mỏng và gáy sách mọng, mỏng hơn đợt đầu sách xuất bản.
Mong Phương Nam lưu ý đến điểm này cho lần tái bản những tập sách tiếp sau của bộ này để những độc giả yêu mến nó sẽ thật sự có trải nghiệm tốt khi muốn sưu tập lại.
3
38610
2015-07-18 21:33:21
--------------------------
214448
4671
Cuốn sách thực sự đã làm tan chảy trái tim người đọc. Mỗi tác giả, mỗi câu chuyện, mỗi phong cách viết đã đang và sẽ ấp ủ những "trái tim đang lớn". Cầm cuốn sách trên tay, và bạn sẽ được nếm trải những cung bậc cảm xúc mà các tác giả mang lại. Cuốn sách có 16 truyện ngắn, mà Ploy, Hoàng Anh Tú, Phan Hồn Nhiên, Phạm Lữ Ân,...đã gửi gắm những câu chuyện tình yêu. Riêng mình thích nhất là truyện Môi anh ngọt vị anh đào của Ploy và Ký ức hôn kể về chuyện tình của Chris và Bảo Lan. Mình rất mong Mong chờ Cuốn sách tiếp theo trong bộ sách của những cảm xúc "hãy nói yêu thôi đừng nói yêu mãi mãi" của báo Hoa Học Trò
5
670417
2015-06-24 23:02:17
--------------------------
211241
4671
Từ bìa sách cho đến cách trang trí sách bên trong với gam màu rất dịu và ưng mắt. Bởi lẽ nhiều tác giả nên nhiều những chiêm nghiệm tuổi trẻ được mang đến với độc giả. Có những trang truyện mong manh, tinh tế; những trang truyện đượm buồn lại có những trang đầy ma lực cuốn ta vào con chữ. Mỗi tác giả 1 tuyên ngôn làm "những lời thì thầm " không hề bị lặp lại mà rất thấm. Tôi thấy 1 phần giá trị cuộc sống mà trước giờ tôi không để ý và chưa hề quan tâm. Người ta có biết, hiểu thì mới có trưởng thành và lớn lên. Và năm tháng học đường của tôi là sự lớn lên với Phan Hồn Nhiên, Ploy, Đoàn Công Lê Huy và Hoàng Anh Tú,...
4
635432
2015-06-20 11:22:38
--------------------------
196477
4671
"Hãy nói yêu thôi, đừng nói yêu mãi mãi. Lắng nghe lời thì thầm của trái tim". mới nghe tên sách, nhìn cái bìa sách đẹp ơi là đẹp kia là đã muốn đọc lắm rồi. Cuốn sách là tổn hợp những truyện teen hay. Bạn có thể gặp mô tuýp truyện khiểu này rồi nhưng cuốn sách mang lại cho người đọc nhưng câu chuyện không hề nhàm chán. Ngược lại, sau khi đọc hết một trang bạn sẽ rất nóng lòng muốn biết tiếp diễn câu chuyện như thế nào, muốn khám phá câu chuyện bằng trí tò mò không ngừng nghỉ, sẽ là những câu chuyện thật đáng yêu, nhẹ nhàng, cũng có thể là một câu chuyện hơi buồn khiến đôi lúc tim bạn se lại, xúc động, thổn thức. Nói chung là sách hay lắm. Đọc biết liền.
5
595431
2015-05-15 22:12:05
--------------------------
181624
4671
Bộ sách rất hay, tôi rất đánh giá cao về bộ sách này và nếu ai thích đọc truyện ngắn thì hãy đọc bộ truyện này một lần 
Mỗi truyện ngắn hay mỗi bài cảm thức, tản văn như một cơn gió vừa mới lạ vừa thân thuộc, khiến trái tim teen tan vỡ bởi những chuyện tình yêu ngọt ngào và ẩm ương, những ấm áp của tình thân, tình bạn, những suy tư về cuộc sống rất cần thiết cho hành trang teen khi lên đường chinh phục đại dương đời sống bao la, đầy cạm bẫy… Văn chương trong Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi luôn phong phú và đầy ắp sáng tạo, với một Phan Hồn Nhiên tinh tế, sắc bén, một Phạm Công Luận sâu lắng, nhẹ nhàng, một Phan Ý Yên dữ dội… Mỗi phong cách tác giả như một mảng màu mà bất cứ ai khi đắm mình vào, cũng sẽ tìm thấy màu của mình, đường nét mình, tâm hồn mình trong đó.
Tôi rất thích những câu nói này
4
607454
2015-04-12 09:59:26
--------------------------
180057
4671
Đã chờ đợi lâu rồi đến nay bộ sách này mới được tái bản lại và tất nhiên hình thức thì tuyệt hơn trước đây nhiều. Truyện được viết theo một lối viết rất Việt Nam, bao gồm nhiều mẩu truyện ngắn lãng mạn, ngọt ngào của nhiều tác giả. Có người viết rất thâm thúy, cũng có người viết theo dạng kể chuyện nhưng cũng rất thú vị và lôi cuốn, mỗi một tác giả là một phong cách viết khác nhau vì thế càng khiến tập sách này trở nên hấp dẫn hơn. Thật sự đây là bộ sách rất ý nghĩa mà HHT đã dành tặng cho những người trẻ Việt, như là món ăn tinh thần không thể thiếu hiện nay.
4
41370
2015-04-08 15:57:33
--------------------------
174545
4671
Mình không phải là một người thích đọc truyện ngắn, thậm chí nhiều lúc còn thấy hơi nhàm chán vì mô típ lặp đi lặp lại của nó, trừ truyện của chị Fuyu, Phan Hồn Nhiêm và Phan Ý Yên. Cuốn truyện hao hao tản mạn của một thời tuồi trẻ. Đúng là nên chỉ nói yêu, vì yêu mãi mãi là điều rất khó, tình yêu còn dang dở mới là tình yêu đẹp. Cuốn truyện được trình bày rất đẹp và  bắt mắt, đem lại cảm giác ấm áp, thực sự rất giống một cái ôm cho teen chúng mình.
4
401487
2015-03-28 11:56:51
--------------------------
171190
4671
Bạn nào thích những truyện ngắn trên hoa học trò thì nên mua quyển này , truyện hay vô cùng . Mình đọc truyện này từ lâu lắm rồi, hồi đấy ngày nào cũng hóng xem hôm nào có quyển tiếp,có rồi thì sướng không điên người. Truyện được viết bởi nhiều tác giả khác nhau nên nội dung rất phong phú ,mỗi người có cách viết riêng không hề nhàm chán chút nào. Hãy nói yêu thôi được viết bởi những cây bút xuất sắc nhất HHT nên đảm bảo truyện cực kỳ hay ,mình thích nhất truyện của chị Phan Ý Yên nhẹ nhàng sâu sắc. 
 So với bản cũ thì bản này đẹp hơn rất nhiều và điểm mình thích là giấy truyện khác hẳn lúc trước.
5
526401
2015-03-21 13:40:50
--------------------------
