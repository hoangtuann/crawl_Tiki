411403
5374
Đọc hết tập 1 nhưng không có diễn biến gì quan trọng, tất cả chỉ là nền cho tập 2 này. 
Khi bắt đầu đọc, tôi vẫn mong chờ về 1 câu chuyện cảm động như lời giới thiệu về tác phẩm.
Phần này tác giả mới chính thức đưa người đọc vào trong câu chuyện chính, không quanh co lung tung như tập 1.
Rất thực tế về những chuỗi ngày đi xoay tiền để thay thận cho Tiểu Băng của Đàm Duy,những khó khăn gặp phải của cặp đôi này. Sự động viên giúp đỡ của cô bạn Tạ Di Hồng và cô giáo Lam. Và cái kết hạnh phúc cho cặp đôi này sau chuỗi ngày vất vả, sự hy sinh âm thầm của họ dành cho nhau.
Có thể nói nội dung đẹp, cảm động là thế nhưng tôi vẫn cứ cảm thấy tác giả đã phá hình ảnh Đàm Duy tôi nhìn thấy ở tập 1 ở những chương cuối truyện, và cả sự hy sinh thầm lặng của Tạ Di Hồng. Nó quá thực tế phũ phàng đối với tôi, và tôi cảm thấy đoạn chương về Đàm Duy và Tạ Di Hồng là thừa thãi,và nó làm câu chuyện rắc rối, đọc đến đó đã không muốn đọc tiếp nữa.
3
393052
2016-04-05 22:58:16
--------------------------
305494
5374
Một câu chuyện thực tế về đời sống vợ chồng, không hào nhoáng cũng không quá ảo như những ngôn tình khác. Các nhân vật đều có cá tính riêng, cảm nhận ban đầu của mình là hơi không thích Tiểu Băng và Di Hồng nhưng cuối cùng mới hiểu hết nội tâm nhân vật. Riêng Đàm Duy mình thấy hơi khó chịu vì nhân vật này "hơi ngốc" so với danh hiệu tiến sĩ của mình nhưng được cái là anh ấy rất chung thủy và yêu vợ, là mẫu đàn ông thật sự tốt và có lẽ rất hiếm gặp, trong truyện mình thấy tiếc cho Di Hồng nhiều nhất vì cố ấy hy sinh quá nhiều, cái kết tốt đẹp với Đàm Duy tuy ngắn ngủi nhưng cũng là điều hạnh phúc với cô ấy,nhưng cuối cùng tác giả cũng đã cho cô ấy được một cái kết thật có hậu.Truyện có kết thúc mở, hay và đáng để đọc
4
288837
2015-09-16 23:21:30
--------------------------
233560
5374
Có thể nói tôi không thấy được sự liên kết giữa tập 1 và tập 2. Tuy nhiên tôi lại đánh giá tập 2 cao hơn tập 1. Nó hấp dẫn, lôi cuốn và lời văn cũng đưa độc giả đến nhiều căn bậc cảm xúc và tôi thấy cái tên của tác phẩm cũng nói lên được câu chuyện này có đôi nét buồn khi đọc. Nhưng tôi lại đánh giá cao điều đó vì đối với tôi những tác giả có thể nối tiếp cảm xúc nhân vật đến những đọc giả thì quả thật tác phẩm đó vô cùng thành công. Và riêng tôi, tôi thấy ảnh bìa rất đẹp, có sự hài hòa về màu sắc.
5
517104
2015-07-19 19:10:57
--------------------------
148235
5374
Một câu chuyện mới bắt đầu, có thể nói là không liên quan gì đến tập 1 nhưng cũng phải nhờ tập 1 mới hiểu hết được tính cách mà tác giả xây dựng lên cho từng nhân vật.
Tiểu Băng - nhân vật nữ chính bệnh nặng và phải tốn rất nhiều tiền, tưởng chừng như không có cách nào giải quyết được nhưng cuối cùng cô cũng vượt qua được khó khăn. Một thử thách lớn khác lại đến với hai vợ chồng cô làm cho họ phải đấu tranh mới có thể vượt qua. Một tình yêu lớn đã làm cho mọi khó khăn tan biến.
Khác với những cuốn tiểu thuyết khác là đều nói đến tình yêu lọ lem - hoàng tử, cuốn truyện này nói đến tình yêu vợ chồng dù khó khăn nhưng luôn biết cách vượt qua và sống hạnh phúc. Lời văn hay, luôn khiến cho người đọc cuốn hút.
5
89607
2015-01-10 10:50:44
--------------------------
145478
5374
Ai sẽ ôm em khi thấy em buồn ?... đây là điều đầu tiên mà tôi bị cuốn sách này cuốn hút ...Cách đặt câu hỏi này rất hay ... Nó tưởng chừng chỉ là một câu hỏi đơn giản nhưng lại chứa đựng đằng sau bao nhiêu sự cô đơn, bi thương của một cô gái trẻ ... Tôi rất thích cách tác giả xây dựng các nhân vật trong tác phẩm .... Tính cách dễ thương, cá tính của 2 nhân vật thật sự cuốn hút tôi ... Và tôi thích cả những lúc tác giả tạo nên những tình huống làm Ngải Mễ của tôi đau khổ ... nó làm cho tôi bật khóc ... đây là lần đầu tôi khóc khi đọc 1 tiểu thuyết  .... Hay tác phẩm này thật sự rất hay
4
475692
2014-12-30 17:45:30
--------------------------
143530
5374
Ngải Mễ đặc tên cho cuốn sách này rất hay và bùi tai . Ai sẽ ôm em khi thấy em buồn ? Một câu hỏi tu từ mà sau dấu chấm hỏi đó ẩn chứa biết bao nhiêu sự cô đơn và bi thương ước mong hạnh phúc . Tôi thích cách kể của Ngải Mễ cô không vội dẫn dắt người đọc vào câu chuyện mà để cho họ thấy một khía cạnh khác để chuẩn bị tâm lý trước khi bắt đầu . Trong tác phẩm lần này của Ngãi Mễ về nhân vật tôi rất hài lòng tính cách của nữ chính và nam chính khá dễ thương và cá tính , Điểm không hài lòng duy nhất của tôi là có nhiều đoạn cô làm cho nhân vật tôi yêu thích đau khổ và dằn vặt làm tôi xót chết đi được . Bìa thiết kế nhìn không đẹp mắt tí nào
3
337423
2014-12-23 16:03:20
--------------------------
