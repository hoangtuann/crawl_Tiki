504589
4786
Tập truyện phù hợp với mọi người, đặc biệt là những người lâu lắm rồi không chậm lại để nhìn nhận cuộc đời.
Nguyễn Ngọc Tư viết những điều đời thường nhất, chân thật nhất, mà đôi khi trong cả cuộc đời này tôi cũng không tìm ra được.
Những câu chuyện nhẹ nhàng, dễ đọc dễ hiểu mà thấm lắm. Thấm như thể chính tôi là người từng trải.
*sách rất hay nhưng chất lượng giấy chưa tốt, phù hợp với giá tiền.
3
1009394
2017-01-03 21:45:20
--------------------------
461154
4786
Ngày mai của những ngày mai cái tên thôi sao đã thấy dài lê thê và không đích đến, buồn vô hạn, lại là một tác phẩm nằm trong bộ sưu tập của mình về Nguyễn Ngọc Tư. Lời văn vẫn buồn như thế, vẫn lơ đễnh và mênh mông như vậy. Có một chút ánh sáng ở cuối câu chuyện thôi cũng thấy vui rồi. "Người ta ngây ngất trước sự hào nhoáng, mê mẩn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng" - Một trích dẫn không thể hay hơn mà mình từng đọc được. Vẫn là chị, đọc lại như mới, không thể lẫn được!
4
309333
2016-06-27 16:26:59
--------------------------
449324
4786
Cuốn đầu tiên đọc của chị là "Đong tấm lòng", cách đây khoảng 1 tuần, thấy thích quá nên mua tiếp cuốn này :)
Đọc Nguyễn Ngọc Tư, chưa bao giờ không phải ngừng lại ngẫm nghĩ, nhưng phần lớn là ngừng lại để buông một tiếng thở dài. Truyện chị buồn, những nỗi buồn rất thường nhật, buồn trước cây trước cỏ, trước những thân phận người. Đọc sách của chị, chẳng thể đọc nhanh cho xong...đọc để thấy chúng ta còn phải yêu thương nhiều hơn nữa những người, những vật quanh mình...đọc để dừng lại đôi chút giữa cuộc sống hối hả đôi khi làm người ta vô tình...Sẽ mua thêm những cuốn sách khác nữa của chị.
5
167724
2016-06-17 12:28:21
--------------------------
448948
4786
"Ngày mai của những ngày mai" là một tác phẩm mang màu sắc vừa xưa mà lại vừa nay của Nguyễn Ngọc Tư. Đọc tác phẩm, chúng ta có lúc thấy mình như đi lạc về thời quá khứ, có lúc lại như biết trước được ngày mai của mình như thế nào. Bản thân được sinh ra ở nơi quê biên giới, tôi như cảm nhận được hết những tình cảm bình dị, đơn sơ mà tác giả đã mang đến trong tác phẩm. Người làng quê chân chất, tình cảm của người ở quê mộc mạc, đơn sơ. Vậy đó, nhưng theo dòng thời gian, tình cảm ấy, những con người ấy có giữ được nguyên sơ sự dung dị của con người mình?
3
505896
2016-06-16 19:38:49
--------------------------
443110
4786
Nguyễn Ngọc Tư, cô nhà văn này với giọng mộc mạc hết sức. Đọc mà thấy một tâm hồn bình dị, dễ thương. Lời văn khắc khoải, buồn man mác ẩn chức bên trong. Đặc biệt, mình cực kỳ ấn tượng với trang bìa, được thiết kế cực kỳ thích hợp. Đọc đi rồi sẽ thấy chính bản thân mình bên trong ấy, quá thân thương. Tuy nhiên, một chút không thích là cô Tư chưa có chút phá cách trong các tác phẩm, vẫn đang '1 màu' kiểu ấy. Tuy nhiên, vẫn nhận đjnh đây là 1 cuốn sách hay, nên đọc khi rảnh rỗi. 
5
544482
2016-06-06 11:13:25
--------------------------
437820
4786
Đây là tác phâm mang phing cách mới lạ phải nói là rất káhc lạ đối với nguyễn ngọc tư. Không còn mang vẻ u uất trong tình cảm mà chỉ tập trung nói về nhưng khoảng lặng trong cuộc sống. Tình thương dành cho tất cả mọi người. Những rung cảm nhe nhàng nhưng phải sâu sắc lắm người mới nhận ra . Nếu ai đã quen với vẻ buồn bả của tác giả mà đọc tác phẩm này sẽ có chút bất ngờ, nghi ngờ. Không biết có đó phải nhà văn quen thuộc của chúng ta hay không
3
604014
2016-05-28 21:59:02
--------------------------
434869
4786
Cuộc sống muôn màu muôn vẻ, có những góc sáng nhưng cũng có những góc tối tăm khó dò, ai trong chúng ta cũng đã từng có những ước mơ, những hoài bão, những thứ mình muốn theo đuổi, nhưng mấy ai có thể kiên định đi đến cuối cùng? Ngày mai của những ngày mai chính là quyển sách bộc bạch những tâm tình đó, đôi khi con người ta không phải cố tình hờ hững lãng quên, chỉ đơn giản là bị cuốn vào những xô bồ tấp nập của cuộc sống, đã quá vất vả với những bon chen đời thường cùng cơm áo gạo tiền, nên cuối cùng nếu có tâm nhìn lại chỉ có thể là điều tiếc nuối.
4
992439
2016-05-24 08:31:29
--------------------------
431945
4786
Lần đầu mua chỉ ấn tượng với tên sách và cách trình bày bìa sách nhưng sau khi đọc hết chương đầu tiên thì mới cảm nhận được cách viết đơn sơ, mộc mạc nhưng ngọt ngào của Nguyễn Ngọc Tư. Quyển sách có nội dung rất hay, giấy dày nên cảm giác đọc rất thoải mái. Nguyễn Ngọc Tư đã để lại trong tâm trí mình một câu nói: Người ta ngây ngất trước sự hào nhoáng, mê mẩn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng..Câu nói này thật đáng để suy ngẫm sâu sắc hơn..
5
1221130
2016-05-18 12:09:35
--------------------------
420478
4786
Mình thích đọc sách của Nguyễn Ngọc Tư vì thường sách có cái gì đó buồn man mác nhưng lại rất sâu sắc và mộc mạc. Đọc sách để thấy những câu chuyện về miền tây sông nước nơi có những con người tần tảo lam lũ cho đến khi họ bị thị thành hóa, con người dần dần xa cách nhau. Đọc sách để bắt gặp chính tuổi thơ của mình ở đâu đó trong cuốn sách để rồi khi gấp sách lại cảm thấy đau đáu cho thân phận con người. Đây là một quyển sách thích hợp đọc trong một buổi chiều yên ả và người đọc thích một chút suy tư trầm lắng.
3
196347
2016-04-23 11:12:47
--------------------------
413508
4786
"…Người ta ngây ngất trước sự hào nhoáng, mê mẫn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng..."
Đây là lời đề phía sau sách, và tôi cảm thấy rất tâm đắt. Cũng chính vì vậy mà văn của chị tư mang đậm tính chân thật. Và vì nó thật nên đôi khi có những sự thật đằng sau khiến người đọc phải xót xa. Đọc quyển sách này rất nhiều lúc tôi phải buông xuống và ngẫm. Thật ra nó không nặng nề, chị viết nhẹ nhàng lắm, vì chị viết chuyện thật, và vì nó thật nên nó đau...! 
5
448288
2016-04-09 20:43:37
--------------------------
407580
4786
Vì nhìn thấy bìa sách rất ấn tượng nên mình đã quyết định mua quyển sách này, mặc dù là tạp văn nhưng với lỗi viết chân chất đơn giản mà mộc mạc, giọng văn đầy cuốn hút, thì Nguyễn Ngọc Tư một làn nữa lại cho mình thấy được những góc khuất của cuộc sống mà thế giới hiện đại đang che mờ dần. Câu văn bình dị nhưng vẫn được tác giả trau chuốt đem lại một tác phẩm tuyệt vời. Sách in đẹp, cầm cũng nhẹ tay nên cho dù có nằm đọc cũng cảm thấy thoải mái.
4
110169
2016-03-30 12:56:58
--------------------------
404236
4786
Ấn tượng đầu tiên về cuốn sách là bìa khá đẹp. Lần đầu tiên đọc 1 cuốn sách của Nguyễn Ngọc Tư, cảm thấy khá là hài lòng. 54 bài tạp văn ngắn sẽ đưa chúng ta đến những vùng quê nơi con người sống có 1 tâm hồn rất tinh khiết. Những bài tạp văn rất là nhẹ nhàng, chân thực và sâu sắc. Quả thực, những điều tươi đẹp trong cuộc sống đang dần bị con người ta hắt hủi bởi lối sống thực dụng và do ảnh hưởng của công nghệ phát triển. Chúng ta bây giờ đang quá vô cảm với cuộc sống, cuộc đời, với con người với nhau. 
4
1148627
2016-03-24 21:02:49
--------------------------
402846
4786
Tôi đã nghe qua cái tên Nguyễn Ngọc Tư rất nhiều, nhưng đây là lần đầu tiên tôi được đọc quyển sách của chị. Quyển sách được thiết kế với tấm bìa rất đơn giản, có máy đánh chữ nhìn rất cổ xưa. Từng trang chuyện của chị tôi đọc cảm giác buồn man mác. Lời văn chị rất hay, mang theo lối viết cổ xưa mà lôi cuốn tôi trong từng trang sách. Quyển sách "Ngày mai của những ngày mai" này tuy không phải là quyển tiểu thuyết dài, nhưng nó chứa đựng những tản văn hay, ý nghĩ, sâu sắc lòng người.
5
1209601
2016-03-22 22:30:14
--------------------------
396001
4786
Nhìn đời bằng con mắt của một người thành phố tôi dường như cảm thấy nữ nhà văn như đang nói thực lòng mình, nói cho tôi biết những thứ chân quê, quen thuộc mà tôi không gặp bấy lâu nay đang dần hiện ra trên từng trang viết của một người chân quê. Không bởi vì thành phố này quá lớn mà tôi cảm thấy thiếu thốn một mối quan tâm dành riêng cho người hàng xóm láng giềng, hay một người quen nhưng xa lạ , bởi dành cho họ nhiều quá nhưng sự chân thành của họ đối với nhau, đối với tôi không là bao. Đọc văn chị, tôi cảm thấy những tình cảm này thực sự nảy nở giữa những người quen chưa từng biết một cách chân thành và cũng nhận lại từ họ sự chân thành không kém. Đôi khi, một góc khuất nào đó khiến ngòi bút của chị thiên về sự phê phán, đó là một sự phê phán nhân văn, có sự điều chỉnh của cảm xúc, bọc lộ những tâm trạng không chì của riêng chị mà trong đó  còn có cả tôi, cả những đọc giả gần xa.
4
634723
2016-03-12 20:11:26
--------------------------
385792
4786
Ngày Mai Của Những Ngày Mai là món quà sinh nhật được một người bạn thân tặng. Vừa đọc vào những trang đầu tiên đã như thấy đây là thứ văn chương mình tìm kiếm bao lâu nay, chân thực, giản dị hết sức đời thường nhưng vô cùng sâu sắc. Rồi sẽ thấy mình đâu đó giữa những mẫu "tạp văn". Một tác phẩm sẽ không thích hợp cho người vội vàng. Sau mỗi câu chữ là sự thật trần trụi cuộc đời mà mình phải suy ngẫm.
Sau khi đọc xong, đã quyết định lên tiki mua một cuốn làm quà tặng. Và bắt đầu yêu cái văn chị Tư :)
5
482200
2016-02-24 16:24:34
--------------------------
385592
4786
Đến với sách qua một lần lang thang trên tiki chỉ đề tìm một cuốn sách đặt cho đủ tiền để không tốn tiền ship. Mình thật sự ấn tượng với bìa sách, rất đẹp và sáng tạo, và khi nhìn thấy tác giả là chị Nguyễn Ngọc Tư thì mình không còn chần chừ gì nữa mà đặt ngay bởi mình đã là fan của chị từ lâu. Những truyện ngắn trong sách như những khoảng lặng sâu lắng và dung dị đời thường. Mình thích nhất một câu: "Người ta ngây ngất trước sự hào nhoáng, mê mẩn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng..." thật thấm thía.
5
132597
2016-02-24 11:18:12
--------------------------
359771
4786
Tôi đã đọc một vài tác phẩm của Nguyễn Ngọc Tư nhưng đây là lần đầu tiên tôi đọc tản văn của chị. Hầu như tác phẩm nào cũng khá khó đọc, khó hiểu. Nhưng '' Ngày mai của những ngày mai'' lại hoàn toàn khác. Những câu chuyện của chính cuộc đời của tác giả viết ra nên ta thấy rõ ngay sự chân thật, có những điều hệt như ta đã trải qua, có nhưng cảm xúc ta cũng đã từng cảm nhận. Nhưng hơn hết chính là cái man mác buồn, một cái buồn rất riêng đúng chất Nguyễn Ngọc Tư.
4
827272
2015-12-27 22:57:14
--------------------------
351864
4786
Lại một lần nữa tôi cầm trên tay quyển sách của nhà văn Nguyễn Ngọc Tư. Tư vẫn vậy, chưa bao giờ làm tôi thất vọng, dù những câu chuyện tôi đã được đọc trên các trang mạng, dù đọc rồi tôi vẫn muốn đọc đi đọc lại rất nhiều lần những câu chuyện của Tư. Ngài Mai Của Những Ngày Mai làm tôi thật hoang mang tự hỏi: Ngày mai rồi lại ngày mai, ngày mai rồi sẽ ra sao, ngày rồi lại có những ngày mai vậy đâu là điểm dừng?
Là vì "Tôi không là tôi cũ, người không còn là người cũ, gặp nhau cũng như không... Tôi đã có lần gặp bạn mừng chưa hết, thì nói câu gì đó đau lòng rồi cuộc hẹn tan mau." làm tôi chạnh lòng nhưng Tư thắp lên niềm tin "Dù ngày mai, mốt đó là của những ngày mai, mốt khác. Nó xa, nhưng mà gần, cứ kêu lên, ngày mai, chẳng phải qua đêm nay là tới đó sau? Những hẹn hò có khi mãi mãi là hẹn hò, nhưng nhờ nó, người ta mới chịu khó ngồi nuôi dưỡng niềm tin."
Nhưng mà cách in quyển này làm tôi phiền lòng chút đỉnh. Nội dung thì hay rồi mà đọc lâu lâu lại thấy lỗi trình bày làm mình nả lòng hết sức. Một quyển sách hay mà vài trang thấy hai từ thiếu dấu cách, có chỗ lại dư. Càng về cuối lỗi càng nhiều, cãi lỗi sai chính tả ở trang 126 nữa, chữ "một" lại bị đánh là "mộit", cái chữ "Quá hay" trang 248 lại thiếu mất dấu chấm đằng sau. Hi vọng lần xuất bản sau quyển sách sẽ làm hài lòng đọc giả hơn nữa về sách trình bày.
"Mai mốt gặp, dạ, mai mốt gặp".
4
151536
2015-12-13 01:16:53
--------------------------
336333
4786
Lần đầu tiên mình quyết định chọn đọc tác phẩm của NNT. Thật sự cuốn sách này gây ấn tượng sâu sắc đối với mình. Giọng văn chân thật,giản dị nhưng thật sâu sắc,thấm từng câu từng chữ. Những câu văn miêu tả giàu hình ảnh,âm thanh,khi đọc những truyện ngắn trong tuyển tập đầu mình cứ tự vẽ ra những hình ảnh tuyệt đẹp về cuộc sống của nhân vật trong tác phẩm của tác giả. Thật tình là kể những câu chuyện đơn giản,rất đời thật nhưng lại đi sâu vào lòng người,để lại những ấn tượng không bao giờ quên.
5
341053
2015-11-12 11:43:56
--------------------------
335555
4786
Tác phẩm này là tác phẩm thứ 3 mình mua cùa Nguyễn Ngọc Tư sau Cánh đồng bất tận, Ngọn đèn không tắt. Vẫn là Nguyễn Ngọc Tư như vậy, chẳng khác gì, đọc xong truyện của chị mà thấy buồn quá chừng, buồn vu vơ, nhưng mà vẫn muốn đọc.:( Đọc xong một cuốn của Nguyễn Ngọc Tư là phải ngừng để đọc quyển khác của tác giả khác, không chịu nổi đọc 2,3 quyển liền, buồn kinh khủng, cũng không biết tại mình nhạy cảm quá không nữa. Nhưng dù sao thì cũng đã đặt mua thêm mấy cuốn nữa rồi.
5
833612
2015-11-11 10:26:52
--------------------------
333080
4786
Biết đến chị đã lâu nhưng đây là lần đầu tiên mình được đọc tác phẩm của chị, một ấn tượng khó phai, một Nguyễn Ngọc Tư chân thực, một tác phẩm chạm vào góc khuất con người. Ngày mai của những ngày mai, là một giấc mơ tươi đẹp hay hạnh phúc đau buồn, dằn vặt hay mỉm cười, dằn vặt vì chính bản thân lại hờ hững trước hoàn cảnh của người khác, mỉm cười chính là tự giễu bản thân mình. Truyện  của Nguyễn Ngọc Tư là thế, nghe ra bình dị nhưng lại trần trụi tàn nhẫn, tất cả đã được trau chuốt tỉ mỉ qua tác phẩm Ngày Mai Của Những Ngày Mai, đó không chỉ là một cuốn sách mà là cả cuộc đời thực tế vô cùng và cũng là một thực tế ngày nay chúng ta phải đối mặt. Bạn lạnh lùng trước người khác, bạn sẽ lại được sự đáp trả tàn nhẫn hơn, đó mới chính là cuộc sống, mong là mỗi người trong chúng ta hãy sống chậm, chậm để cảm nhận những vui buồn trong cuộc sống cho đến khi nhận ra sư thật "…Người ta ngây ngất trước sự hào nhoáng, mê mẫn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng..."
5
634785
2015-11-07 13:01:33
--------------------------
296398
4786
Tác giả Nguyễn Ngọc Tư vẫn luôn như vậy, chưa bao giờ làm mình thất vọng. Cái phong cách trong chữ nghĩa của chị có gì đó rất riêng mà rất ấn tượng. Từ những câu chuyện của những con người bình thường và bình dị, qua ngôn từ bình dân lại thể hiện được cái bản sắc chân chất mà phóng khoáng của người con miền Nam. Có lẽ nhờ cái sự mộc mạc mà gần gũi trong cách kể của chị, mỗi người con miền Nam đọc qua sẽ cảm thấy mình ở trong đó. Và mình cũng rất tâm đắc với câu nói đầu sách: "Người ta ngây ngất trước sự hào nhoáng, mê mẫn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng".
5
82495
2015-09-11 05:38:53
--------------------------
287633
4786
Một cách hành văn mộc mạc trong thế kỷ 21 như Nguyễn Ngọc Tư trải bao sự phản ánh của được luận tưởng chừng khó có thể tồn tại trên văn đàn. Thế nhưng nối gót Cánh đồng bất tận, chị đã khẳng định được cái "chất" của một con người miền nam phóng khoáng mà nhạy cảm qua tác phẩm Ngày mai của những ngày mai. Những câu chuyện bình thường với những ngôn từ bình thường kể về những con người bình thường: người bà, người hàng xóm... trong cuộc sống hàng ngày của nhân vật "tôi". Nhưng chính những điều bình thường ấy đã tôn vinh sự vô thường trong tâm hồn của Nguyễn Ngọc Tư: cảm đời rồi mới thấy yêu đời.
4
502553
2015-09-02 21:57:05
--------------------------
287213
4786
Lần đầu tiên mình mua sách của Nguyễn Ngọc Tư. Quả thật phong cách viết của chị Tư gây ấn tượng với mình rất nhiều. Đó là những trải nghiệm, suy tư của một người trăn trở với những vấn đề nhỏ nhặt, thường ngày trong cuộc sống và cả những điều mà người bình thường ít ai nghĩ đến. Lời văn của chị khá trau chuốt nhưng lại bình dị đến lạ.
…Người ta ngây ngất trước sự hào nhoáng, mê mẩn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng…
Mình sẽ tìm đọc khác và cùng suy ngẫm với tác giả Nguyễn Ngọc Tư.

5
376152
2015-09-02 14:16:33
--------------------------
279061
4786
"Người ta ngây ngất trước sự hào nhoáng, mê mẫn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng", câu nói được in ở bìa quyển truyện làm mình rất ấn tượng, câu nói như xuyên qua lòng người đọc . Nguyễn Ngọc Tư là một nhà văn giản dị, đôi lúc thấy những văn từ của bà rất gần gũi, rất thân mật đối với con người miền Nam. Và quyển này cũng vậy, sự mộc mạc làm người ta thấy đồng cảm, và sự chân thành khiến những mẫu chuyện ngắn như khắc sâu vào lòng người. Mình thích văn ở Nguyễn Ngọc Tư là vậy, đủ sâu sắc, đủ thấu hiểu, đủ sự bóng loáng mà vẫn hài hòa nét mộc mạc miền Nam
5
634444
2015-08-26 14:12:35
--------------------------
266750
4786
Với "Ngày mai của những ngày mai", tác giả Nguyễn Ngọc Tư đã không làm mình thất vọng. Tác phẩm đã lấy đi không ít nước mắt cùa tôi vì qua từng câu truyện, tôi như thấy thấp thoáng đâu đó hình ảnh của chính mình, cuộc đời của chính mình và rồi cảm xúc cứ thế tuôn trào. Ngòi bút của "Chị Tư" rất mộc, rất đời, không văn hoa, không hào nhoáng nhưng sao từng chữ, từng câu của chị cứ nhẹ nhàng in đậm vào tâm trí để rồi tôi phải tự hỏi "Chẳng lẽ chị đang viết về mình???"
5
193942
2015-08-14 22:33:16
--------------------------
266245
4786
Chị Tư bao giờ cũng vậy, bao giờ cũng pha thêm mấy cái bùi ngùi, buồn bã, rồi lại nhớ nhung, bồi hồi ở cái kết của câu chuyện thế vậy? Nhưng qua đó tôi có thể nhìn lại cuộc đời mình, xem lại được một lần nữa những thứ tôi tưởng chừng như đã mất đi trong thế giới thực tại này.Thực sự, dù lời văn có mộc mạc đến mấy, có giản dị đến mấy, có ngắn gọn đến mấy thì luôn thể hiện những cảm xúc nhất định, những cái cảm giác tuy ngột ngạt nhưng giàu tình cảm, buồn miên man đến khó tả.Thật thích khi được nghe chị Tư kể chuyện,thật vui khi được ngồi đọc truyện của chị Tư,cái cảm giác ấy...Bao giờ cũng thế,từng dòng chữ cứ hiện lên lần lượt trong tâm trí tôi,từng lời văn cứ mãi vang lên trong đầu,từng con chữ cứ mãi rượt đuổi nhau trong trí óc của một đứa trẻ chỉ mới lên 13.Ôi,tôi yêu chị Tư mất rồi...
5
620916
2015-08-14 15:17:35
--------------------------
265818
4786
Truyện của cô Tư thì hoặc là kết thúc lấp lửng, hoặc là kết thúc buồn nhưng hầu như cốt truyện nào cũng làm người ta phải suy ngẫm. Ngày mai của những ngày mai cũng là một tác phẩm như thế, mỗi truyện ngắn trong tác phẩm đều để lại cho người đọc một nỗi xót xa khó tả cho thân phận của những con người trong câu chuyện ấy. Phải nói cô Tư có cái nhìn rất tinh tế, thấu đáo và hiểu lòng người, từng chữ cô Tư viết cứ như nói lên giùm nỗi lòng của người đọc, tuy mộc mạc nhưng lại khiến người ta có thể rơi nước mắt.
5
464538
2015-08-14 10:24:21
--------------------------
262999
4786
Đọc truyện cô tư mình luôn hỏi:"không biết chừng nào cô tư viết truyện bớt buồn nữa?" Nhưng mà bớt buồn thì đâu ra truyện cô tư nữa. Truyện cô viết truyện nào cũng buồn thúi ruột. Ngày Mai Của Những Ngày Mai cũng vậy, mỗi truyện đều để lại trong người đọc nỗi xót xa, nhung nhớ, nhớ lại cái thời mà chúng ta đã quên mất và xếp chúng vào ngăn kéo kí ức. Cô tư rất hiểu lòng người, cô nhắc chũng ta nhớ những gì mộc mạc nhất trong kí ức mỗi con người. Ngày Mai Của Những Ngày Mai là quyển truyện sâu sắc và mang tính nhân văn cao.
4
84364
2015-08-12 11:48:52
--------------------------
261349
4786
Tập này chị Tư, theo cảm nhận của mình viết theo lối tản văn, như những kinh nghiêm sống của chị, bạn chị hay những sự kiện chị đã thấy, nghe và trải qua chứ không phải các truyện ngắn. Đây giống như một tập tự sự về đời, cho thấy cái nhìn của chị. Khi đọc giống như ta đang đọc một cuốn nhật kí, trong đó lồng vào cảm xúc của người viết cho mỗi sự kiện vậy. Vẫn mang đặc trưng buồn và nhiều sâu lắng, ai đang buồn đời mà đọc cuốn này thì thấm vào tận chân răng, càng buồn hơn. 
5
81533
2015-08-11 01:10:24
--------------------------
260869
4786
Biết đến Nguyễn Ngọc Tư qua quyển "Đảo" nên cũng không khó để mình tìm đến một tác phẩm khác của cô, và quyển "Ngày mai của những ngày mai" là một trong số những quyển sách mà mình tìm đến sau khi "thấm" quyển "Đảo".
"Ngày mai của những ngày mai" là quyển sách mà theo mình rất giản dị, và rất nhân văn. Đọc những tác phảm của tác giả Nguyễn Ngọc Tư, mình như trưởng thành hơn, biết suy nghĩ sâu sắc hơn và đặc biệt là mình suy nghĩ về đời, về người nhiều hơn. Rất nhiều cảm xúc trong mình khi đọc và ngẫm từng con chữ. Thấm thía! Sâu sắc! Bình dị! Và...giản đơn...!
Đọc chậm, ngẫm sâu là những gì mà mình làm khi đến với quyển sách này - "Ngày mai của những ngày mai"
4
308302
2015-08-10 18:44:21
--------------------------
258997
4786
Trước kia vô tình coi bộ phim được chuyển thể từ quyển sách " Cánh đồng bất tận " của tác giả Nguyễn Ngọc Tư, thấy phim rất hay, rất có ý nghĩa nhân văn và làm người xem thấm thuần được những sự khó khăn, cơ cực mà tác giả muốn truyền đạt. Đồng thời cũng chợt nhận ra và hiểu rõ hơn về tính nhân văn của tác phẩm khi kết phim hiện lên những dòng suy nghĩ của cô bé nhân vật. Và thế là sau bộ phim ấy, liền tìm ngay đến những tác phẩm khác của chị Nguyễn Ngọc Tư đó là quyển " Ngày mai của những ngày mai " để lại được đắm mình trong những con chữ mà chị viết, sâu lắng mà cũng mênh mang, giản dị mà rất nhân văn. Mua sách của chị rồi về nghiền ngẫm về cuộc đời, cuộc sống xung quanh và rồi cho đến khi gấp sách lại vẫn thấy thấm thía, bâng lân. Mọt sách thì nên tậu ngay 1 cuốn :D
5
531757
2015-08-08 23:34:49
--------------------------
258634
4786
Tản văn thật đúng là không phù hợp với mình rồi. Những câu chuyện ko đầu không cuối, lửng lửng lơ lơ. Bạn nào thích đọc sách có nội dung như một câu chuyện, có đầu có cuối thì đừng mua cuốn này. Theo cá nhân mình thì trước NNT viết tản văn đọc cũng được lắm mà sau này mình đọc không cảm được nữa. Cuốn nào của Tư xuất bản mình cũng mua vì mình thần tượng chị. Mua để dành lâu lâu đọc. Nhưng gần đây nhất là Sông và Ngày mai của những ngày mai mình đọc thấy buồn chán quá. 
3
201207
2015-08-08 18:13:29
--------------------------
253841
4786
Cái bìa nó không có khớp với cái tựa sách. Nhưng phải nói là sách của Nguyễn Ngọc  Tư cuốn nào cũng đi thẳng vào lòng người. Con người ta vốn luôn mê mẩn trước sự hào nhoáng, bóng bẩy mà quên đi mất những thứ nhỏ nhặt xung quanh để đến khi nhận ra mới cảm thấy những điều đó thật ý nghĩa. Có những câu văn ngắn gọn sâu sắc và đầy tinh tế đọc đi đọc lại mới hiểu hết được ý nghĩa của tác giả muốn truyền đạt. Hãy cảm nhận cuộc sống từ những điều đơn giản, bình dị xung quanh ta, đừng nên sống vội rồi phải nuối tiếc những gì đã qua.
5
393794
2015-08-04 20:09:43
--------------------------
250250
4786
Nguyễn Ngọc Tư với lối viết văn nhẹ nhàng, đơn giản, mộc mạc nhưng cũng rất sâu sắc. Chính cái sự mộc mạc, chân thật ấy tưởng chừng như dễ dàng để cảm nhận được hết nhưng thật sự không phải như vậy. Người đọc phải thật sự sâu sắc mới có thể cảm nhận một cách trọn vẹn ý nghĩa trong từng câu chuyện. Mặc dù như vậy nhưng có rất nhiều người tìm đến văn của Nguyễn Ngọc Tư để tự mình khám phá và tìm ra ý nghĩa ẩn sâu bên trong từng câu chữ. Và với cuốn sách "Ngày mai của những ngày mai" đó là cuộn phim về cuộc sống và con người với những cảm nhận hết sức tinh tế và giàu cảm xúc. Cuốn sách giúp ta đón nhận cuộc sống một cách tự nhiên và vui vẻ... :D
4
223102
2015-08-02 10:07:57
--------------------------
250122
4786
Biết đến tác giả Nguyễn Ngọc Tư qua bộ phim Cánh đồng bất tận. Xem phim xong mới đọc truyện và khá thích phong cách của tác giả này. Từ đó bắt đầu tìm đọc những quyển sách của Nguyễn Ngọc Tư và đã không bỏ qua tác phẩm Ngày mai của những ngày mai. Thật sự là mình không thích bìa sách lắm, thấy bình thường quá, nhưng nội dung thì đã không làm mình thất vọng. "…Người ta ngây ngất trước sự hào nhoáng, mê mẫn trước sự bóng bẩy, nhưng chỉ rơi nước mắt trước sự giản dị tự đáy lòng..." Là những mẫu chuyện rất đời thường, rất gần gũi và rất đáng suy gẫm.
4
680206
2015-08-01 10:23:01
--------------------------
239036
4786
Đây là lần đầu tiên mình mua sách của Nguyễn Ngọc Tư dù đã được biết đến tên của tác giả này từ khá lâu. Hầu hết tên của những quyển sách được tác của đặt rất hay, cô đọng và đầy gợi mở. Sách là những mảnh ghép vui buồn của cuộc sống bằng lối viết mộc mạc và chân chất nhưng thể hiện một cái nhìn sâu sắc của tác giả về những vấn đề tưởng chừng như rất giản đơn. Hãy đọc "Ngày mai của những ngày mai" để thêm yêu những điều nhỏ nhặt nhưng đầy ý nghĩa của cuộc sống.
4
330563
2015-07-23 16:43:10
--------------------------
237353
4786
Đọc xong, cảm giác đầu tiên là nhẹ nhàng, sau đó là quá gần gũi với cuộc sống.
Đôi khi mình chỉ cần có 1 góc nhỏ, 1 quyển sách mà mình yêu thích là đủ để thấy đời đáng sống rồi. Ngoài ra, đọc sách của tác giả Nguyễn Ngọc Tư cũng là lần thứ n rồi và lần nào cũng thấy đọng lại những ý nghĩa nhân văn hết sức sâu sắc.Mình thích nhất là trong mỗi truyện ngắn đều có những thông điệp dài và chất chứa trong đó là những tình cảm chân thật của tác giả. Mình thấy có bản thân trong đó. Đồng cảm nhận và chia sẻ tình yêu thương con người, ý nghĩa nhân văn chất chứa trong đó khiến mình không còn muốn sống vội, sống vàng, cái cuộc sống vốn dĩ đang khiến người ta hiện đại hơn, thông minh hơn nhưng...thiếu tình người hơn. Cảm ơn Tiki đã cho mình có cơ hội tiếp cận sách nhanh nhất.
Thanks
5
689867
2015-07-22 13:47:45
--------------------------
232166
4786
Khi đọc tạp văn của Nguyễn Ngọc Tư cũng là dịp để lòng mình lắng lại, yên tĩnh để suy ngẫm thêm về cuộc sống. Những tạp văn của Nguyễn Ngọc Tư trong "Ngày mai của những ngày mai" làm người đọc có cảm giác rưng rưng một nỗi buồn về những điều tốt đẹp đã và đang ngày một biến mất trong cuộc sống. Sau khi đọc xong quyển này, thấy hiểu và trân trọng hơn những người phụ nữ. Họ sinh ra để làm làn mưa mát lành, dạy cho những người đàn ông biết yêu quý những điều rất nhỏ, những điều làm nên ý nghĩa của cuộc sống hàng ngày.

5
471112
2015-07-18 17:14:37
--------------------------
231045
4786
Chỉ cần là văn cô Tư thì đã là một bảo chứng tuyệt vời rồi. Dù chưa đọc review, dù tác phẩm không xuất sắc như những tác phẩm khác cùng tác giả thì có mọt điều chắc chắn là chỉ cần là sách của Nguyễn Ngọc Tư thì đã vượt mức sàn chung của văn học trong nước hiện nay rồi, trong một thị trường bát nháo quá nhiều rác phẩm như này. 
Và phải nói là mình thích sách ngay từ cái tiêu đề, trong các tác giả hiện nay thì cô Tư có cái hay là đặt tên sách hay nhất, hấp dẫn độc giả từ cái nhỉn đầu tiên. Mình luôn cảm thấy tên sách cũng đóng phần khá quan trọng trong việc quyết định nội dung. Và cuốn sách quả đúng như mong đợi, về một Nam bộ trong ký ức của mình.
3
419116
2015-07-17 21:11:16
--------------------------
229513
4786
Là một người con Nam Bộ, mình hạnh phúc khi đọc được những trang viết của chị . Hơi bất ngờ khi biết rằng những trang viết cũng có thể mộc mạc đến vậy. Mộc mạc trong từng chữ, trong từng câu khi miêu tả về cảnh vật về con người. Nhưng sâu sắc, sâu sắc trong từng cái nhìn, từng cách nghĩ...
Thỉnh thoảng lại thấy bất ngờ bởi một câu nghe thân thuộc nào đó, bật cười khi nhớ rằng, ngày xưa, lúc ở quê mình cũng hay nói những câu ấy. Những câu nói đi vào trang sách, thân thương.
Cách nghĩ đôi lúc có hơi khác, quan niệm sống đôi lúc cũng hơi khác. Nhưng thấy trân trọng những con người như chị. Cảm ơn vì quyển sách tuyệt vời...
5
644890
2015-07-16 21:14:09
--------------------------
228145
4786
Nguyễn Ngọc Tư thật sự là một nhà văn có thể biến những điều giản dị thành một sự thích thú xen lẫn ngạc nhiên đối với người đọc. Chuyện chẳng có gì viết ra cũng thành chuyện, mà chuyện hay chứ chẳng đùa. Tản văn chị viết đọc mà cứ thong thả, chầm chậm vì biết thế nào cũng phải đọc cho xong, chứ dừng lại chịu không được. "Ngày mai của những ngày mai" cũng vậy, đọc xong, có khi chả nhớ cái cốt là gì, nhân vật là ai, mà chỉ man mác nhớ về cái cảm xúc đồng điệu đó, nó cứ bám theo, khiến con người ta thênh thang và cảm kích, vì nếu Tư không viết ra, mình cũng tưởng mình đã lâu không nhìn thấy những điều đẹp tự nhiên đến vậy...
4
642428
2015-07-15 02:15:51
--------------------------
227300
4786
Với từng mẫu chuyện, bạn sẽ cảm nhận được một bức tranh muôn màu đậm chất miền quê sông nước. Nơi có xóm giềng với mái nhà tranh, có bầu trời cao đầy gió, có cánh đồng xanh thăm thẳm, có dòng sông vẫn chảy dài không mỏi mệt... Yêu con đường làng, yêu con người chân chất, đầy tình yêu thương giữa người với người. Đâu đấy bạn sẽ biết rằng cuộc đời này đáng sống biết bao...
5
530797
2015-07-13 22:23:10
--------------------------
224315
4786
Chị Tư viết tản văn rất hay và thấm. Cuốn sách này cũng không ngoại lệ. Sách tập hợp những bài viết đậm chất Nguyễn Ngọc Tư - buồn man mác trộn lẫn sự tỉnh thức nhìn nhận cuộc đời. Cuốn sách chất lượng từ hình thức đến nội dung. Bìa đẹp, hình ảnh cổ điển. Chất giấy và in ấn tốt. Mỗi bài viết là một lát cắt cuộc sống. Có những điều đi qua đời mình hết sức tự nhiên và quen thuộc, nhưng không phải ai cũng nhìn nhận được sự việc một cách sâu sắc như chị. Từng câu từng chữ đã được chị thể hiện bằng cả tấm lòng. Chính vì vậy, càng đọc các tác phẩm của chị, ta càng thấy thấm thía hơn cái sự đời, sự tình cũng như sự chân thành đối với văn chương.
5
433873
2015-07-08 14:50:20
--------------------------
223071
4786
Ngày mai của những ngày mai là cuốn sách thứ hai của Nguyễn Ngọc Tư mà tôi được đọc, sau cuốn Cánh đồng bất tận. Với cuốn sách này, tôi nhận thấy mình sao mà quá vô tâm với mọi thứ. Mỗi ngày tôi sống, làm việc, học tập và vui chơi nhưng chưa bao giờ để ý đến những con đường tôi đi, những cô lao công trên, những người bán hàng quán ven đường. Dường như đối với tôi, những thứ đó đều nằm ngoài thế giới tôi đang sống. Nhưng, Nguyễn Ngọc Tư đã cho tôi nhận ra mỗi người, mỗi vật đều có những câu chuyện riêng đáng để ta suy ngẫm và học hỏi, tích lũy kinh nghiệm sống. Những câu chuyện vụn vặt trong cuộc sống thường nhật dưới ngòi bút của và trở nên thật sinh động, sâu sắc.

5
67900
2015-07-06 15:47:52
--------------------------
212814
4786
Mình mua sách từ khi mới phát hành, trong đợt Event Tuyên Chiến Sách Giả này thấy tác phẩm giảm giá 50% thật quá hấp dẫn :(
Về nội dung, tác phẩm vẫn là những tạp văn sâu sắc với lối viết chân chất, ngôn từ giản dị nhưng đọc từng truyện từng truyện qua cái nhìn của tác giả mình như được khơi lại những cảm xúc, suy nghĩ đã bị cuộc sống vội vã này làm quên bẫng đi. Cuốn tản văn nào của chị, mỗi lần mình chỉ đọc 2,3 truyện là những suy nghĩ trong đó đã chiếm chọn tâm trí mình rồi, có khi chỉ vấn đề trong một câu truyện cũng khiến mình phải dừng lại, gấp cuốn sách và suy ngẫm rất lâu.
Còn về hình thức sách thì từ bìa sách đã rất ấn tượng rồi. sách cầm chắc chắn, chất lượng.
4
404829
2015-06-22 22:05:50
--------------------------
206720
4786
lúc đầu mua về chỉ vì ấn tượng bởi bìa sách sách đẹp tưởng sẽ được đọc trong này là những câu chuyện sướt mướt nhưng không phải vậy trong đầy đậm chất cuộc sống bây giờ đôi lúc loang lổ màu buồn lời văn có vẻ khắc khoải giản dị đến vô cùng có lẽ như nhà văn đã nói người ta chỉ rơi nước mắt trước những sự giản dị tự đáy lòng tôi đã khóc khi đọc thấy mình trong đó ở những con người vô cảm lướt qua những cuộc đời rất hờ hững mà không có một giây nào ngồi đó và nghĩ lại chỉ khi đọc quyển sách này tôi mới cảm nhận hết màu sắc của cuộc đời này .mong rằng sẽ có một ngày nào đó được gặp tác giả và cảm nhân cuộc sống của cô...
5
536424
2015-06-10 14:09:20
--------------------------
204260
4786
Mặc dù mình đã đọc gần như hầu hết những bài viết của Tư trong tác phẩm này qua blog của chị nhung mình vẫn chọn mua cuốn sách này vì rất yêu thích văn phong của chị. Vẫn lối viết chân thành, giản dị, chậm rãi không ồn ào và vội vã đó, Nguyễn Ngọc Tư đã đưa những nhân vặt, những suy nghĩ những góc nhìn về cuộc sống của mình đến với độc giả một cách thật tự nhiên và gần gũi. Đọc Nguyễn Ngọc Tư để thấy yêu thêm những điều giản dị xung quanh mình, để biết quan tâm hơn đến những điều tưởng chừng vụn vặt nhưng nhiều khi có ý nghĩa to lơn đối với kỷ niệm của mỗi người. Đọc để tìm lại mình, để kịp thời ngăn mình, không để lòng mình chai lì, dần trở thành vô cảm với cuộc đời này.  Văn của Tư là văn của yêu thương...
5
75031
2015-06-03 14:48:49
--------------------------
193892
4786
Tư hiền, Tư gần gũi và Tư viết hay. "Ngày mai của những ngày mai" tập hợp nhiều điều vụn vặt mà chị góp nhặt từ những gì diễn ra xung quanh, không còn gò bó nhiều về người nông dân nữa, chị nói còn nhiều thứ hơn thế. Tản văn của Tư đôi lúc là Tư hiện đại, sành điệu, đôi khi là Tư chất phác nhưng vẫn nhận ra 1 Tư chân thật trong từng con chữ mình viết. Quyển sách không quá dày nhưng tôi lại không dám đọc nhiều, mỗi ngày dăm ba tản văn là đủ, sợ hết, đôi lúc đọc mà cứ rưng rưng, đọc mà muốn vỗ đùi thỏa chí 1 cái vì sao Tư lại viết đúng với những gì mình suy nghĩ quá. Tư tuyệt vời! "Ngày mai" làm gì có trong lịch, sao mình không sống cho trọn vẹn vào ngày hôm nay, nhỉ?!
5
46905
2015-05-08 23:28:57
--------------------------
177726
4786
Yêu chị Nguyễn Ngọc Tư từ khi đọc "Cánh đồng bất tận" Người miền xa kia không phải ai cũng tốt, những tác giả khát đều miêu tả những cái đẹp nhưng đối với chị Tư, chị đã dám khai quật các khía cạnh khác, xấu xí của nơi này. 
Và một lần nữa "Ngày mai của những ngày mai" lại đưa chúng ta đến với góc khuất của cuộc sống. Ai cũng có thể nhận ra, nhưng có ai dám nói ra như chị? Một xã hội hững hờ, vô tình. Con người đã quá đi sâu vào vòng xoay cuộc sống mà quên đi những điều giản đơn. Tình yêu giữa người với người, những điều nhỏ nhặt nhất có thể khiến thay đổi ít nhiều xã hội vô cảm này
4
520065
2015-04-03 18:45:56
--------------------------
176745
4786
Đây là 1 trong những quyển sách hiếm hoi của chị Tư mà tôi chọn đọc. "Ngày mai của những ngày mai" vẫn là những bài tạp văn dung dị, đời thường nhưng không kém phần tinh tế. Tôi còn đặc biệt ấn tượng với hình vẽ trang bìa, một chút hoài cổ xen lẫn hiện đại. Quyển sách là những góc khuất của cuộc sống mà con người do quá vướng bận vào vòng quay của cuộc sống nên đã vô tình hờ hững, lãng quên. Thế nên quyển sách là người bạn đồng hành giúp chúng ta có những giây phút lắng đọng, khoảng lặng sâu thẩm trong tâm hồn để nghĩ và ngẫm. Đây là tác phẩm tôi khá hài lòng của chị Tư.
4
131327
2015-04-01 19:28:33
--------------------------
175440
4786
Đọc sách của Nguyễn Ngọc Tư lúc nào cũng vậy, vẫn cái cảm xúc như khi đọc Cánh đồng bất tận hay Đảo.
Nhưng ở Ngày mai của những ngày mai có lẽ tác giả viết khá là mới mẻ hơn, tự nhiên hơn. Cách sử dụng từ ngữ rất đơn giản nhưng vẫn gây sự tự nhiên, cuốn hút. Như những nét chấm phá không tên, tác giả mang đến những cảm xúc nhất định cho người đọc.
Ở Ngày mai của những ngày mai, có thể thấy tác giả đang trẻ hóa văn phong. Tuy nhiên có đoạn hơi nhạt, kiểu như không có cảm xúc ý.
Chờ đợi tác phẩm tiếp theo.
4
39173
2015-03-30 10:53:48
--------------------------
174371
4786
Đây là cuốn sách có bìa ngoài khá khác lạ so với những cuốn khác của tác giả Nguyễn Ngọc Tư. Bìa sách trông khá trẻ trung nhưng cũng có gì đó mang hơi hướng cổ xưa. 
Nội dung sách chỉ đơn giản là những câu chuyện đời thường bình dị mà tác giả đã từng trải qua và bây giờ kể lại. Tuy chỉ là những câu chuyện bình thường, quen thuộc, nhưng chúng lại mang đến cho ta những bài học sâu sắc về vẻ đẹp tâm hồn của con người và tình yêu thương giữa con người với nhau.
Ngôn ngữ mà tác giả sử dụng rất tự nhiên, giản dị, từ đó, người đọc có thể dễ dàng hiểu được ý nghĩa của những câu chuyện.
5
445522
2015-03-27 21:14:58
--------------------------
171390
4786
Những chia sẻ về cuộc đời của Nguyễn Ngọc Tư luôn để lại cho mình những nỗi khắc khoải khó diễn đạt thành lời. Qua góc nhìn của Nguyễn Ngọc Tư, độc giả luôn có cơ hội nhìn và cảm nhận những khía cạnh của cuộc sống, cả những mặt sáng lẫn tối.
Cuốn "Ngày mai của những ngày mai" có thiết kế bìa khá trẻ trung, trông khác hẳn với thiết kế của những tác phẩm trước của Nguyễn Ngọc Tư. Cá nhân mình thấy bìa sách lần này khá thu hút sự chú ý của mình. Nó góp phần kéo độc giả lại gần hơn với sáng tác của Nguyễn Ngọc Tư.
Một cuốn sách hay, thấm đượm mọi trải nghiệm gai góc của Nguyễn Ngọc Tư
4
48662
2015-03-21 20:49:09
--------------------------
169438
4786
Chỉ là những câu chuyện ngắn tưởng chừng không liên quan mà tác giả là người trải nghiệm và thuật lại. Tuy nhiên đọc sách rồi sẽ thấy, sẽ cảm và sẽ nhận ra " Ồ! Sao mình bỏ qua lâu rồi nhỉ?... "
Những góc khuất lần lượt được tác giả khám phá để mang đến cho độc giả những thước truyện hết sức chân thật, những từ ngữ địa phương khiến ta bật cười, rồi lại hoai hoải xúc cảm quen thuộc từ quá lâu ta đã bỏ quên...
Điểm cộng : Sách trang trí đơn giản và đẹp, nội dung có hay và có chưa hay.
Khuyến khích : tặng Bookmark cho độc giả để đánh dấu câu chuyện mình đã đọc qua.
4
542114
2015-03-18 11:26:59
--------------------------
169216
4786
Đây là lần đầu tiên tôi được đọc văn chị Nguyễn Ngọc Tư. Đối với tôi, tôi cảm thấy rất hợp với cuốn sách này. Nó chứa đựng một nội dung vô cùng sâu sắc và mang tính hiện thực mà đa phần bọn họ đều không hề nhận ra giữa cuộc sống bộn bề ngày hôm nay: đó chính là vẻ đẹp tâm hồn và tình yêu thương giữa con người với nhau. Khung cảnh cơ cực, con người bần hàn, một bức tranh u tối với những cảnh đời bất hạnh đang khẩn thiết cầu cứu một bàn tay chìa ra giúp đỡ họ, kéo họ ra khỏi cái vũng lầy tăm tối mang tên nghèo khổ đó. Họ khổ, rồi dần dần họ sinh ra vô cảm, mất đi tình yêu thương, tình người trong chính bản thân mình. Quả là bi kịch biết bao nhiêu!
Bên cạnh đó, lối viết văn của chị có chút gì điềm đạm, không ồn ào, bay bướm và đầy hoa mỹ như đa phần giới trẻ hiện nay yêu thích, nhưng vì thế mà phần "hồn", phần nội dung của cuốn sách khiến người đọc dễ thấm hơn.
5
293316
2015-03-17 20:44:49
--------------------------
166557
4786
đây là một cuốn sách không phải theo phong cách những câu chuyện ngôn tình thời này. đơn thuần nó chỉ là một cuốn sách thuật lại những trải nghiệm thực tiễn của tác giả qua những chuyến đi, những gì bà được chứng kiến, nhìn thấy. Có cảm giác như ta đang hiện diện trong chính những cuộc sống đó đói khổ, cơ cực, khó khăn, đầy dẫy những mảnh đời bất hạnh đang cần sự cứu giúp của con người. bằng giọng văn trầm, nhẹ nhàng, sâu lắng tác giả đã cho ta nhìn thấy phần nào thế giới hiện tại, con người bị cầm tù trong đói khổ, nhưng sống bên cạnh họ lại vô vàn những con người vô cảm đang mất dần đi tình thương, tình người, tình dân tộc.
3
465834
2015-03-12 21:33:55
--------------------------
