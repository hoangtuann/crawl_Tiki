555241
6865
Sách hay. Tôi cảm thấy cảm ơn Tiki rất nhiều. Chúc Tiki sinh nhật vui vẻ
5
2048381
2017-03-26 22:40:50
--------------------------
555149
6865
Một câu chuyện rất hay, gần gũi và dễ thuộc. Tranh vẽ minh họa rất đẹp. Mình xem tranh mà cũng muốn đến ngôi làng nơi bé Mi đang sống
5
1771154
2017-03-26 21:15:50
--------------------------
545665
6865
Mình mua truyện này cho bé 21 tháng. Vì chưa biết nói nhiều nên con chỉ nhớ được một vài từ mỗi trang. Tranh vẽ đẹp, rõ nét nhưng câu viết khá dài và khó hiểu nên mình chỉ chọn đọc cho con mà thôi. Dù vẫn chưa hiểu hết nội dung nhưng bé có thể nhìn từng trang để nói theo mình. Ví dụ khi mình hỏi "mẹ bảo bé mi đi làm gì nào?" thì con nói "đi mua sữa cho em bé", rồi bé gặp ai, bị làm sao ^^ Đoạn cuối hơi khó để diễn đạt cho con. Nhưng con mình vẫn rất thích quyển này và đòi đọc lại suốt. Đợi con lớn hơn chút sẽ hiểu được ý nghĩa của truyện là gì.
4
48839
2017-03-17 11:35:39
--------------------------
473617
6865
Câu chuyện đáng yêu về cô bé nhỏ tuổi nhưng đã biết giúp mẹ mua sữa cho em bé. Bé cháu mình 18 tháng tuổi rất yêu thích câu chuyện này, mỗi tối ăn cơm xong đều nói "Mi ơi, Mi ơi" là bắt mẹ hoặc dì đọc truyện cho cháu nghe, mỗi lần nhấn nhá giọng các nhân vật là bé rất thích. Hi vọng sau này bé lớn lên cũng biết tự lập như bé Mi. Mình đặc biệt ấn tượng và thích thú với những chi tiết vẽ trong truyện về khu phố ở nước Nhật, về các hàng hóa bán trong tiệm tạp hóa. Chỉ vào những hình vẽ và đố bé đó là gì cũng là một trò chơi hay sau khi đọc xong truyện. 
5
775864
2016-07-10 19:23:12
--------------------------
463680
6865
Câu chuyện như 1 phần nhỏ của chương trình "Con đã lớn khôn" mà Đài truyền hình mua bản quyền từ Nhật. Ngôn từ giản dị, hình vẽ dễ thương nhưng đầy chi tiết, giấy không dày lắm nhưng độ dày vậy là phù hợp, vì truyện này thích hợp cho trẻ 5-10 tuổi, lứa tuổi này đã khá lớn và có thể nhận thức được là truyện để đọc chứ không phải để xé.

Ehon Nhật Bản thật sự là một tủ sách rất bổ ích cho tinh thần trẻ em VN. Tuy nhiên, không chắc có nhiều phụ huynh VN dám cho con em mình lâp nên chiến công như bé Mi vì nhiều điều kiện không cho phép.
4
63996
2016-06-29 15:29:22
--------------------------
462892
6865
Cũng như nhiều quyển ehon khác, ehon "Chiến công đầu tiên của bé Mi" có nội dung trong trẻo, dễ thương. Chỉ đơn giản là đi mua sữa giúp mẹ, nhưng với bé Mi, đó là cả một chiến công. Chuyện miêu tả sinh động cảm giác háo hức, tự hào, nhưng không kém phần hồi hộp của bé Mi khi lần đầu tiên cầm tiền đi mua hàng một mình.
Trong truyện có một chi tiết mình không thích là có người đàn ông đeo kính đen đến cửa hàng mua thuốc lá. Mình sửa lại (lấy bút sửa vào sách để khi Ông, Bà đọc cho bé cũng sửa theo) là mua 5 quả trứng.
Bé nhà mình 16 tháng, rất thích truyện này đến mức thuộc và kể lại được luôn (dù chỉ mới là nhắc chữ cuối theo Mẹ).
5
8736
2016-06-28 22:37:29
--------------------------
456408
6865
Cuốn ehon “Chiến công đầu tiên của bé Mi” kể về hành trình giúp mẹ đi siêu thị mua sữa cho em của bé Mi. Trên đường đi bé đã tự mình vượt qua những chướng ngại và bé rất hạnh phúc khi hoàn thành được nhiệm vụ của mình. Cuốn sách được trình bày rất logic, hình ảnh được in màu rất đẹp; khổ giấy cứng – dày - cầm vừa tay, nội dung thì tuyệt vời. Bạn nhỏ nhà mình rất thích được đọc ehon trước khi đi ngủ. Cảm ơn Quảng Văn Books đã lựa chọn dịch và đem về cho các bạn nhỏ Việt Nam những món ăn cho tâm hồn tuyệt vời! 
5
301718
2016-06-23 14:05:56
--------------------------
448447
6865
Mình mua cuốn ehon này cho con gái 4 tuổi. Truyện có tình huống khá gần gũi và mang tính giáo dục kỹ năng cho trẻ trong cuộc sống đời thường (bé Mi đi mua sữa cho mẹ và những tình huống Mi gặp phải như gặp người đi xe đạp phóng thật nhanh ngoài đường, gặp người đàn ông dữ dằn ở tiệm tạp hoá, bà chủ cửa hàng không nghe thấy tiếng bé Mi và cả chuyện bé Mi quên lấy tiền thừa), nhưng có vẻ con gái mình lại không thích lắm. Đọc được 1-2 lần rồi không có hứng thú những lần tiếp theo :((
4
1358241
2016-06-15 23:14:38
--------------------------
447195
6865
Quyển ehon này rất hay và có tính giáo dục đối với các cháu nhỏ. Mình mua cho bé 3 tuổi nhà mình, bé thích lắm, cứ cầm đưa cho mẹ bảo mẹ đọc cho con. 

Truyện có độ dài vừa phải, tranh vẽ đẹp, màu sắc bắt mắt nên bé nhà mình rất thích. Truyện kể về 1 câu chuyện thường ngày (mẹ nhờ Mi đi mua sữa cho em bé vì mẹ đang bận), và kể về "hành trình" đi mua sữa của bé Mi (5 tuổi), bé đã vượt qua nỗi sợ của mình như thế nào. Mình cũng rất thích cuốn sách này
5
1046240
2016-06-13 17:06:07
--------------------------
440902
6865
Bé nhà mình khá là nhút nhát. Làm gì cũng phải có người lớn bên cạnh. Đọc cho bé nghe Chiến công đầu tiên của bé Mi thấy bé im lặng như ngẫm nghĩ cái gì đấy. Sau đó mỗi lúc muốn bé tự làm điều gì đó sẽ nói "bạn bé Mi đâu có sợ, bạn còn dám đi đến cửa hang tạp hóa cơ mà..." thế là bé nhà mình cứ như lên tinh thần, đi làm liền. Tôi cũng dần dần có thể dạy bé cách tự lập. Câu chuyện nhỏ nhưng đem lại hiệu quả lớn. 
Cám ơn Tiki đã mang cuốn sách này.
4
606122
2016-06-02 14:26:27
--------------------------
436761
6865
Khi có được cuốn sách bé nhà mình rất thích. Đây là Một cuốn sách hay và bổ ích. Bé nhà mình rất hứng thú với cuốn sách luôn bắt mình đọc đi đọc lại rất nhiều lần và luôn muốn có chiến công như của bé My. Rất đáng yêu. Cuốn sách khiến Bé rất thích được làm việc giúp đỡ gia đình được thể hiện mình đã lớn. Cuốn sách rất đẹp, bé nhà mình rất thích những hình ảnh trong sách. Bé luôn để đầu giường để trước khi đi ngủ bé đều muốn xem và nghe đọc.
4
963495
2016-05-27 09:48:37
--------------------------
433128
6865
Chuyện kể về bé mi 5 tuổi đã biết giúp mẹ đi mua sữa cho em. Đây là 1 trong số những cuốn ehon nổi tiếng ở việt nam. Mặc dù truyện dành cho trẻ từ 3 - 6 tuổi nhưng bé nhà mình 2 tuổi cũng rất thích. Mặc dù bé chưa biết chữ nhưng mình đọc cho bé xem tranh bé rất nhớ hình và thích nghe đọc. Các bạn có con nhỏ nên mua cuốn này vì đọc xong bé sẽ học theo bạn mi, can đảm đi 1 mình và biết giúp đỡ mẹ khi mẹ bận không thể đi mua đồ được.
5
1355433
2016-05-20 13:45:19
--------------------------
426242
6865
Mình thực sự rất khâm phục cách cha mẹ Nhật dạy con, những em bé 3, 4 tuổi đã có thể tự đi một mình giúp đỡ bố mẹ những việc nhỏ. Mình cũng có một bé 4 tuổi, nên mình hay mua sách ehon của Nhật để đọc cho bé nghe, bé rất thích thường nhờ mẹ đọc lại nhiều lần cho bé. Đây là một trong những cuốn sách mà bé nhà mình yêu thích nhất, bé cũng học theo bạn để giúp đỡ bố mẹ những viêc nhỏ như chơi với em, tự cất dọn đồ chơi, tự xách ba lô đi học... Những việc làm tuy nhỏ nhưng cũng khiến bố mẹ rất vui. Sách ehon của Nhật thực sự bổ ích cho các bé
5
1066198
2016-05-06 14:51:42
--------------------------
412477
6865
Sách đẹp, giấy bóng, hình ảnh và chữ viết to đẹp. Nội dung hay, em bé dũng cảm đi mua đồ giúp mẹ, trải qua nhiều khó khăn và nổi sợ ở lứa tuổi em để đến được tiệm tap̣ hóa gần nhà. Truyện không chỉ giúp các bé hoc̣ tính tự lập, lễ phép mà còn giúp cha mẹ hiêủ con hơn. Ngoài ra mình cũng thích tình tiết bà bán tạp hóa xin lỗi bé Mi khi không để ý đến vị khách nhỏ đứng đợi nãy giờ. Tình tiết này khá hay vì theo mình nhiều người lớn không có thói quen xin lỗi trẻ nhỏ khi làm điều gì sai. Nhất là cha mẹ khi làm sai cũng nên xin lỗi con đề trẻ nhỏ noi theo. Tuy nhiên ở VN mình chắc không dám cho con đi một mình qúa, tình hình bắt cóc trẻ em rồi giết người thật mất nhân tính và đáng sợ hic.
4
429650
2016-04-07 20:49:44
--------------------------
403593
6865
Sách là câu chuyện về cô bé mi mi lần đầu tiên giúp mẹ đi mua sữa 1 mình trước sự ngạc nhiên của bản thân và của cậu bạn tomo. Con mình rất thích truyện này vì nó giống như 1 tấm gương để con mình học hỏi theo, giống như được công nhận là mình đã lớn khi giúp mẹ 1 điều gì đó. Nét vẽ rất đẹp, giấy đẹp và cứng. Một đặc điểm của ehon là mỗi truyện bìa mang 1 màu sắc khác nhau nên giúp trẻ dễ nhớ đượccaau chuyện nào thì bìa sẽ là màu gì (có thể đó là 1 cách nhớ riêng của con mình) :)
5
169714
2016-03-23 23:02:52
--------------------------
394040
6865
Hôm nay mình nhận được cuốn sách Chiến công đầu tiên của bé Mi. cu cậu nhà mình rất thích, vì được làm quen với ehon từ hè năm ngoái. cảm nhận của mình đầu tiên phải nói về tranh vẽ của truyện, tranh vẽ rất sinh động, chân thực từng chi tiết, màu sắc gây sự thích thú cho mình và em bé. còn nội dung cốt truyện thì gần gũi với cuộc sống sinh hoạt hàng ngày nên rất dễ nhớ. mình rất thích cuốn ehon này. hy vọng sẽ mua được nhiều cuốn ehon hay và hấp dẫn như vậy tại Tiki trong thời gian tới.
5
659816
2016-03-09 20:45:55
--------------------------
385733
6865
Cuốn “Chiến công đầu tiên của bé Mi” là một trong những cuốn Ehon đầu tiên mình mua về cho con. Theo mình biết cuốn sách này phiên bản tiếng Nhật khá nổi tiếng. Sách có tổng cộng 31 trang, hình vẽ đẹp, màu sắc hài hòa. Truyện có khá là nhiều chữ nên mình nghĩ thích hợp cho các bé từ 2 tuổi trở lên. Đối với các bé, giúp đỡ được bố mẹ dù là một việc rất nhỏ nhưng khi được bố mẹ khen ngợi thì các bé đều rất vui. Mẹ của bé Mi nhờ Mi đi mua sữa cho em và Mi đã rất vui khi được mẹ tin tưởng giao tiền và giao nhiệm vụ này. Mi có hoàn thành tốt nhiệm vụ mẹ giao hay không? Các bé hãy cùng đọc cho đến cuối truyện nhé.
3
15022
2016-02-24 13:49:29
--------------------------
380628
6865
Bộ truyện tranh ehon nội dung hay, tranh đẹp, có tính giáo dục cao. Trong bộ truyện ehon thì quyển "chiến công đầu tiên của bé mi" là quyển yêu thích nhất của bé con 19 tháng nhà mình. Đoạn bé mi bị ngã cậu ấy xuýt xoa cầm sách lên thơm vào chân bé mi yêu lắm. Tối nào cũng cầm sách ra bảo mẹ đọc. Mình đã mua khá nhiều truyện cho trẻ em như: bách khoa thư larousse, chuột tip, chuột hin, chim cánh cụt bino ... bộ nào cũng hay nhưng tầm tuổi bé nhà mình thì bộ ehon là phù hợp hơn cả. Các bố mẹ nên thường xuyên đọc sách cho con trước giờ đi ngủ để tạo cho con niềm yêu sách nhé
5
371453
2016-02-15 14:18:56
--------------------------
380485
6865
Trong các quyển Ehon Nhât bản , mình thích nhất quyển " chiến công đầu tiên của bé Mi" và " Trường mẫu giáo của chú voi Grumba" vì hình vẽ sinh động, đẹp mắt và nội dung thú vị.  
Mình thường đọc cho bé nghe mỗi tối từ lúc 8 tháng tuổi. Bé rất thích thú và chăm chú lắng nghe, sau này biết mở sách thì mở từng trang cho mẹ đọc. Dù đã đọc rất nhiều lần nhưng bé vẫn rất thích thú. Có thể nói quyển sách này là tiền đề cho việc yêu thích đọc và xem sách của bé nhà mình. giờ đây bé được 16 tháng rất thích đọc và xem sách.
Hy vọng nhà xuất bản có thể phát hành thêm nhiều quyển sách hay như thế này nữa.
5
708274
2016-02-15 10:13:58
--------------------------
347422
6865
Cuốn nhỏ nhưng lại thích hợp cho trẻ nhớ được nhiều nội dung câu chuyện . Giá thành rẻ bổ ích cho trẻ sinh sống với câu chuyện đời thường . Mang lại nhiều điều bổ ích cho suy nghĩ của trẻ tích cực hơn về sinh hoạt , lao động cho mọi người . Cách tư duy tươi sáng giúp giáo dục đạt nhiều thành công cho trẻ . Cách suy nghĩ của người Nhật dạy trẻ mau tiến bộ hơn trong tương lai xa , sự chuẩn bị bao giờ cũng cần thiết khi đến tuổi nhận thức .
4
402468
2015-12-04 13:22:39
--------------------------
346369
6865
Về hình thức sách thiết kế rất đẹp và nhân vật lột tả được nội tâm qua nét mặt .Nội dung hay và ý nghĩa, khơi dậy cho các bé sự thích thú và tự hào khi được tự mình làm được 1 việc "lớn". Tôi ước giá như ở phần cuối có thêm một vài chi tiết nhỏ thể hiện niềm vui của người mẹ khi thấy bé Mi về, chẳng hạn ôm bé Mi xoa đầu hoặc cúi xuống nhìn 2 chân trầy xước của Mi đê câu chuyện kết thúc nhẹ nhàng và không tạo cảm giác hụt hẫng cho người đọc. Nói chung cuốn sách này vẫn rất ý nghĩa và các bé nên tìm đọc
4
495660
2015-12-02 14:44:38
--------------------------
344392
6865
Dù bé nhà mình mới hơn 1 tháng tuổi nhưng những cuốn truyện Ehon quá dễ thương làm mình không cầm lòng được mà mua về cho bé. Quyển đầu tiên mình chọn đọc cho con là quyển này "Chiến công đầu tiên của bé Mi' vì nét vẽ dễ thương, màu sắc hài hòa, dễ chịu. Hai mẹ con nằm trên giường, mình vừa giơ truyện lên vừa đọc cho con nghe, một trang đọc cả tuần để con "thẩm thấu" ^^ Dần dần mình sẽ mua trọn bộ Ehon mà Tiki có, cho bé bi nhà mình lẫn cho bà mẹ mê truyện tranh nữa.
5
82987
2015-11-28 11:46:45
--------------------------
330731
6865
Về nội dung sách này mình rất thích, cả về màu sắc, rất bắt mắt và thu hút trẻ con, nội dung nhẹ nhàng nhưng mang tính giáo dục cao. Con gái mình rất thích sách này, bé thường cầm sách bảo người lớn đọc cho bé nghe. Ehon Nhật Bản rất hay, mong rằng sẽ ngày càng có nhiều ehon để nuôi dưỡng tâm hồn các bé, nếu có ehon kiểu Việt Nam sẽ hay hơn nữa, vì nhiều tên các nhân vật trong Ehon là tên tiếng Nhật, không quen thuộc với các bé. Có một điểm trừ là dù sách bóng đẹp như mình thấy giấy mềm và khá dễ rách. Các bé chưa ý thức được việc giữ gìn sách nên rất dễ hư sách.
3
319050
2015-11-03 09:43:59
--------------------------
323770
6865
Câu chuyện kể về bé Mi mới 5 tuổi. Một hôm mẹ bận, nhờ Mi đi mua sữa cho em, mẹ dặn bé chú ý xe cô và nhớ lấy tiền trả lại. Lần đầu tiên ra ngoài, bé gặp những trải nghiệm thật lạ lẫm và thú vị khi lần đầu tiên đi một mình. Bé vừa đi vừa hát, rồi gặp chiếc xe đạp đi qua, gặp bạn, rồi cô bé chạy, bị ngã trầy xước chân tay và rơi tiền. Khi đến quán, không thấy ai, cô rón rén gọi, nhưng không ai nghe thấy. Rồi cuối cùng bé Mi cũng mua được sữa cho em. Ba mẹ hãy dạy cho con mình tự lập từ nhỏ, để bé có được những trải nghiệm thú vị, từ đó học hỏi được nhiều điều.
5
677227
2015-10-19 17:01:29
--------------------------
318913
6865
Câu chuyện này thu hút mình vì con gái nhà mình còn nhát. Bé Mi trong truyện lần đầu tiên tự mình đi đến cửa hàng và nêu lên món đồ cần mua giúp mẹ. Qua câu chuyện, không chỉ giáo dục con giúp mẹ từ những việc nhỏ mà còn hình thành cho bé tính cách tự tin, tự lập.
Hình ảnh trong truyện đơn giản nhưng đẹp, mô tả rất chân thực những góc quen thuộc của con. Con đường từ nhà đến tiệm tạp hóa, trên đường đi sẽ gặp gì, như là gặp xe to nè, gặp, chó nè...
Còn dạy con cách giao tiếp cơ bản để mua hàng cho mẹ cần nữa.
Cám ơn tiki nhiều vì nhờ có tiki mà mình mới rinh quyển này về cho con gái.
5
197056
2015-10-07 11:49:36
--------------------------
315080
6865
Đây là câu chuyện đầu tiên mình đọc cho bé nhà mình hồi 18 tháng tuổi và bé thích nhất trong số các truyện ehon mình mua về. Tối nào mình cũng đọc cho bé nghe nên đến giờ bé thuộc lòng cả câu truyện rồi, còn nhắc mình mỗi khi mình bỏ qua chi tiết nào đó. Cốt truyện rất đơn giản, kể về "chiến công đầu tiên" của một cô bé 5 tuổi đó là một mình đi đến cửa hàng tạp hóa mua sữa giúp mẹ. Truyện rèn cho trẻ tinh thần tự lập ngay từ khi còn nhỏ, sự dũng cảm và có thể cải thiện cả tính nhút nhát của bé nữa đấy. Bé nhà mình chưa thật hiểu hết ý nghĩa của câu chuyện nhưng dàn dần rồi bé sẽ hiểu khi bé lớn.
5
795661
2015-09-28 07:54:52
--------------------------
314517
6865
Theo mình được biết thì đây là một trong những truyện ehon Nhật Bản đầu tiên có mặt ở Việt Nam. Truyện kể về “chiến tích” của Mi trong lần đầu tiên giúp mẹ đi mua sữa cho em và trải qua một số trở ngại nho nhỏ trên đường từ nhà đến cửa hàng tạp hóa. 
Vừa đọc truyện cho con nghe, mình vừa giúp bé hiểu thêm về tinh thần tự lập, lòng dũng cảm...Để rồi, vào các ngày sau đó, mình cũng dần dần tập cho bé 1 số việc đơn giản có thể tự làm. Như giúp mẹ đem quà sang biếu hàng xóm, đi mượn đồ dùm mẹ...
Truyện thích hợp cho ba mẹ nào muốn rèn luyện cho con tinh thần tự lập ngay từ khi còn nhỏ tuổi bằng những việc đơn giản, vừa sức với bé.
5
194362
2015-09-26 10:41:32
--------------------------
311676
6865
Đã mua cuốn này và cuốn Bàn tay kỳ diệu của Sachi, cả 2 truyện này đều ẩn chứa thông điệp đơn giản và ý nghĩa phù hợp cho bé khoảng 3 tuổi. Tuy nhiên mình thích cuốn này hơn ở điểm tất cả các trang đều là trang màu. Câu chuyện xoay quanh việc bé Mi lần đầu tiên được mẹ nhờ đi mua sữa. Công việc tưởng chừng như đơn giản nhưng với một cô bé nhỏ vài tuổi thì đó là cả một hành trình. Nào là chạy nhanh quá bị té, nào là lần đầu mua nhút nhát quá không dám gọi to bác bán sữa,...các bé hãy thử đọc để thấy được sự nhút nhát mà dễ thương của bé Mi!!!
5
476132
2015-09-20 00:14:45
--------------------------
302646
6865
Ấn tượng đầu tiên khi đọc truyện mình rất thích cách vẽ và phối màu của truyện, rất nhẹ nhàng, tình tế phù hợp với nội dung câu chuyện cũng rất đơn giản nhưng truyền đạt được nhiều điều hay ho, thú vị. Truyện có nội dung là bé Mi lần đầu đi mua đồ, bé lúng túng, ngây ngo nhưng cuối cùng Mi rất vui vì đã làm được. Tuy chỉ là việc đi mua đồ của bé Mi nhưng cũng như đối với các bé, lần đầu làm gì cũng không khỏi gặp khó khăn, nhưng cha mẹ hãy để bé làm, để con trưởng thành và có trách nhiệm hơn.
5
138619
2015-09-15 13:07:35
--------------------------
299188
6865
Chuyện kể về bé Mi lần đầu tiên đi mua hàng lúc 5 tuổi.
Bé vụng về, thiếu kinh nghiệm và nhút nhát. Đi có một đoạn phố cỡ 100m và 1 chỗ rẽ mà đối với Mi như một chuyến phiêu lưu lớn vậy, mình đọc và nhớ về tuổi thơ của mình nên cũng rất hồi hộp cùng cô bé.
Nhưng mi rất dũng cảm và độc lập, cô bé không hề bỏ cuộc dù bị té đau chảy máu, hay bị ngó lơ khi cố gắng mua hàng, và cũng đã biết cố gắng không khóc lúc tủi thân, dễ thương ghê!
5
614101
2015-09-13 04:03:42
--------------------------
296843
6865
Truyện "Chiến công đầu tiên của bé Mi" là câu chuyện nhẹ nhàng, đơn giản, sát với thực tế. Với người lớn chúng ta, công việc đi ra ngoài mua đồ là hết sức bình thường, tuy nhiên với những đứa trẻ lên 5 thì với chúng là điều hết sức phi thường, chứng minh được mình đã lớn, có thể giúp đỡ được mẹ mình. Truyện mang tính giáo dục cao, có tác động tích cực đến trẻ nhỏ, là món quà tinh thần hữu ích cho trẻ. Hình ảnh minh họa đẹp, sống động, lôi cuốn người xem đặc biệt là các bé nhỏ.
5
775965
2015-09-11 12:36:11
--------------------------
290959
6865
Con trai mình thích quyển "Chiến công đầu tiên của bé Mi" lắm. Cu cậu cứ đọc đi đọc lại hoài. Hồ hởi nói với mẹ "mai mốt mẹ kêu con đi mua đồ ở xa xa như bạn Mi nha. Chứ kêu con đi xuống tầng dưới mua đồ (nhà mình ở chung cư) gần xịt à, chán lắm" . Hôm sau cậu hăng hái lập "chiến công" với mẹ bằng việc đem bịch rác xuống tầng dưới, rồi xuống đem đồ gửi giùm mẹ. Câu chuyện trong sách tuy đơn giản nhưng lại có tác động tích cực với con trẻ. Thật đáng vui mừng. Mình hy vọng các bé con cũng sẽ tích cực hơn khi được tiếp xúc với những món ăn tinh thần bổ dưỡng như thế này. 
5
663152
2015-09-05 22:16:06
--------------------------
287906
6865
Quyển truyện này thích hợp cho các bé từ 3 tuổi trở nên do lời truyện dài, dễ khiến các bé nhỏ hơn mất tập trung. Tuy  nhiên mình vẫn mua cho bé nhà mình (23 tháng), nhưng mình không đọc hết lời truyện mà lại kể ngắn gọn theo cách của mình, giúp bé dễ nhớ hơn. Bé đặc biệt thích truyện bởi hình ảnh của truyện rất thực tế và sinh động, màu sắc đẹp (bản thân mình còn rất thích), qua câu chuyện này mình và bé nhà mình còn tập đóng vai mẹ đưa tiền nhờ con đi mua đồ vật giúp nữa. Rất thú vị và đáng mua bạn nhé. :)
5
385515
2015-09-03 09:18:17
--------------------------
285838
6865
Từ hôm mua mấy quyển Ehon mới con mình rất thích đọc. Truyện " Chiến công đầu tiên của bé Mi" rất hay và mang ý nghĩa giáo dục thực tế. Câu chuyện kể về việc bé lần đầu tiên được mẹ nhờ đi mua sữa cho em mà đi có 1 mình, kiểu cốt truyện giống như chương trình " con đã lớn khôn" ấy. Khi đọc cho con mình vừa đọc vừa giải thích việc bạn ấy đi 1 mình và phải chú ý những điều gì để có thể đến nơi an toàn và trở về an toàn mà thực hiện được đúng nhiệm vụ được giao. Việc cho bé đi 1 mình ở Việt Nam thì vẫn hơi sợ, nhưng câu chuyện là thêm 1 phần vào kĩ năng sống cho các bé.
5
729076
2015-09-01 10:53:51
--------------------------
277051
6865
Có những điều là bình thường đối với người lớn, nhưng đối với trẻ thì lại rất vĩ đại. Chiến công đầu tiên của bé Mi là một câu chuyện như thế. Bé Mi được mẹ nhờ đi mua sữa cho em, và "dù chỉ là hành trình ngắn ngủi nhưng qua con mắt của em lại là một chuyến phiêu lưu vừa kỳ thú vừa gian nan". Cuộc sống và văn hóa Nhật Bản ít nhiều phảng phất trong các câu chuyện của bộ Ehon này. Văn phong của cậu chuyện dễ thương, cuốn hút, hình minh họa đẹp. Các bà mẹ có con nhỏ nên mua cả bộ về đọc cho con nghe hàng đêm :) 
4
332766
2015-08-24 16:35:13
--------------------------
271323
6865
Bé nhà mình hơn 3 tuổi  rất thích sách này. Cu cậu bắt mình đọc đến cả hai tuần liên tiếp, tối nào trước khi đi ngủ cũng đọc, mỗi lần đọc đến năm bảy lượt mà không biết chán, cứ đến chỗ ưa thích là bé lại cười vang nhà, mình đọc đi đọc lại đến mỏi miệng. Mỗi trang được diễn đạt rất súc tích, nội dung manh tính giáo dục cao, cộng với hình vẽ sống động, phù hợp với lứa tuổi, có lẽ điều ấy đã thu hút các bé ngay khi nhìn thấy sách. Mình đang tiếp tục sưu tầm truyện Ehon khác...
5
331353
2015-08-19 05:31:00
--------------------------
270464
6865
Cuốn sách được thiết kế theo khổ ngang lạ mắt, chất liệu giấy trơn bóng, hình minh họa màu đẹp. Nội dung kể về một bé gái 5 tuổi lần đầu giúp mẹ đi mua sữa. Công việc tuy có vẻ đơn giản nhưng đối với bé Mi đó lại là một chuyến phiêu lưu đáng nhớ.:Vì em còn quá nhỏ nên em gọi cô bán hàng không nghe thấy, phải ôm hộp sữa vừa to vừa nặng mà leo lên con dốc dẫn về nhà,... Qua đó cho ta thấy được cách xử lý tình huống rất đáng yêu và của bé. Cách viết nhẹ nhàng rất phù hợp để đọc cho các bé hoặc cho các em bé mới vào lớp một tập đọc. 
4
95034
2015-08-18 11:40:36
--------------------------
269158
6865
Ehon Nhật Bản: Chiến Công Đầu Tiên Của Bé Mi của Công ty phát hành Quảng Văn do Nhà xuất bản Văn Học phát hay thật hay, sách được vẽ bằng những hình ảnh minh họa rất đẹp mắt, giấy dày trơn lán. Một câu chuyện rất hay về cô bé Mi được mẹ nhờ mua sữa cho em, cô bé chưa bao giờ bước ra đường một mình nên run và sợ lắm...trải qua nhiều tình tiết thú vị cuối cùng cô bé cũng mang được sữa về cho em. Các mẹ nên mua sách này về để dạy cho con trẻ có tính tự lập nhé!
5
722015
2015-08-17 09:41:41
--------------------------
266180
6865
Cuốn sách rất hay cả về nội dung và hình thức, từng chi tiết trong cuốn sách đều mang tính giáo dục cao, là tấm gương về sự cố gắng, nỗ lực không ngừng cho trẻ, giúp trẻ tự tin và trưởng thành. Đồng thời cuốn sách cũng là một tài liệu tham khảo tốt cho cha mẹ về những phản ứng của trẻ nhỏ trong quá trình trưởng thành.
5
415825
2015-08-14 14:23:11
--------------------------
261942
6865
Bé nhà mình rất thích truyện này. Câu chuyện kể về chuyến đi xa giúp mẹ của cô bé Mi 5  tuổi. Mặc dù gặp khó khăn trên đường đi nhưng bé vẫn rất dũng cảm và can đảm để hoàn thành nhiệm vụ mẹ giao. Câu chuyện cổ vũ ý chí cho các bé, dạy các bé cần phải mạnh mẽ hơn và tự tin hơn khi không có ba mẹ ở bên. Cả chúng ta những người làm cha mẹ ông bà cũng học thêm những bài học bổ ích, rằng không phải lúc nào cứ ở cạnh con đã tốt, cần phải cho con cơ hội để thử thách để chứng tỏ mình và để ba mẹ thấy rằng " con đã lớn khôn"
5
478786
2015-08-11 15:31:07
--------------------------
260168
6865
Đây là quyển sách tranh Ehon đầu tiên trong kệ sách gia đình mình, và cũng là quyển mà bé thích nhất. Sách không chỉ là bài học dành cho con trẻ mà còn là một lời nhắn nhủ dành cho các bậc cha mẹ: hãy đặt niềm tin nơi con.
Một câu chuyện đơn giản nhưng rất cảm động về hành trình đi mua sữa cho mẹ của bé Mi, với bao thử thách dọc đường. Có lúc vấp ngã trầy chân, tưởng chừng muốn bỏ cuộc, nhưng bé Mi vẫn quyết tâm đi tiếp để hoàn thành nhiệm vụ.
5
462641
2015-08-10 10:07:31
--------------------------
255948
6865
Mình luôn có trong tay tất cả các ehon được bán trên Tiki, nên may mắn dc đọc qua nội dung tất cả các quyển. "Chiến Công Đầu Tiên Của Bé Mi " dạy cho bé về sự dũng cảm, tính tự tập và biết giúp đỡ mẹ..Truyện kể về cô bé Mi 5 tuổi được mẹ nhờ đi siêu thị mua sữa để cho em bé uống, mặc dù lần đầu phải đi ra ngoài một mình nhưng Mi vẫn rất sung sướng dũng cảm nhận lời lời đề nghị của mẹ. Để đến được siêu thị và trở về nhà bé cũng gặp không ít khó khăn nhưng rồi cũng vượt qua được. Truyện hay nhưng nội dung dài và hơi nhiều chữ.
4
454819
2015-08-06 15:02:46
--------------------------
255887
6865
Đây là 1 trong những cuốn ehon mà bé mình thích nhất, và mình cũng vậy, vì nội dung rất hấp dẫn. Truyện kể về 1 lần bé Mi đi mua sữa cho mẹ, có kịch tính, thắt mở nên bé theo dõi rất chăm chú. Nhờ chuyện này mà mình thường nhắc bé đi đường phải cẩn thận nép vào lề đường như bạn Mi, và nhắc nhở bé yêu thương em, giúp đỡ mẹ.
Tranh vẽ sinh động, mình có thể kết hợp cùng bé mô tả cảnh vật ở trong tranh, khuyến khích khả năng quan sát
5
499847
2015-08-06 14:36:12
--------------------------
255422
6865
Câu chuyện giản dị thôi nhưng tranh thì đẹp vô cùng, đến mình cũng thích mê luôn. Những cảm xúc của cô bé Mi trong truyện được thể hiện rất tinh tế qua từng bức tranh, lúc phụng phịu vì tủi thân không được bác bán hàng để ý tới, hay lúc sợ hãi vì sợ rơi mất tiền. Mình nghĩ là rất hợp với các bé 3, 4 tuổi, giúp các bé ý thức giúp mẹ làm những việc nhỏ, không sợ hãi bước ra ngoài cuộc sống. Xứng đáng vote 5 sao cho một tác phẩm giáo dục hay.
5
585544
2015-08-06 00:49:40
--------------------------
254433
6865
Câu truyện nhỏ nhưng đáng khích lệ. 
Đây là truyện bé nhà mình thích nhất trong những cuốn Ehon đã mua. Có thể do nội dụng thật gần gũi với trải nghiệm thực sự của các bé. Bé Mi tự mình làm việc mà hàng ngày có sự trợ giúp từ người lớn. Bé phải tự suy nghĩ, tự nói với chính mình và vượt qua vài thử thách nhỏ trên đường lên dốc đi mua sữa hộ mẹ. Các truyện Ehon khác cũng là những câu truyện chân thực nhưng có tính rất khích lệ các bé. Vote cho Ehon và sẽ ủng hộ.
5
232089
2015-08-05 11:02:38
--------------------------
253433
6865
Với các bé mẫu giáo, một việc bình thường được làm lần đầu tiên cũng có thể gọi là một chiến công. Tôi thích thú với ý tưởng của tác giả, và chắc một ngày nào đó tôi sẽ để con trai 3 tuổi của tôi lập một chiến công đầu tiên cho riêng nó. Truyện là một tình huống nhỏ, nhưng có đặt mình vào vị trí đứa bé 5 tuổi mới thấy đó là thách thức lớn.  Tôi thích cách người mẹ động viên bé tự làm, chỉ một việc đơn giản, nhưng khi bé vượt qua được, bé sẽ có lòng tự tin lớn hơn, và một cảm giác phấn chấn rất tuyệt khi thành công. Một chiến công nhỏ và một sự khích lệ lớn dành cho bé.
5
82774
2015-08-04 14:34:06
--------------------------
246575
6865
Bé nhà mình rất thích câu chuyện "Chiến công đầu tiên của bé Mi". Mình cũng đã mua nhiều truyện viết cho trẻ con để khuyến khích con đọc nhằm giúp con yêu đọc sách nhưng con có vẻ không hào hứng. Rồi một hôm, nghe giới thiệu của 1 chị bạn, mình đã đặt mua "Chiến công đầu tiên của bé Mi" và thật tuyệt khi nhìn thấy quyển truyện mẹ mua, con đã tự lật ra và đọc to cho mẹ nghe. Nhận xét của mình là truyện có tranh vẽ khá đẹp và nội dung rất đơn giản mà lại gần gũi.
5
572305
2015-07-29 18:15:20
--------------------------
243520
6865
Vừa viết review cho cuốn "bàn tay kỳ diệu của Sachi" và sẽ thật thiếu sót nếu không viết review cho "chiến công đầu tiên của bé Mi". Đây là 1 trong 2 cuốn mà bé nhà mình thích nhất trong bộ ehon 4 cuốn. Một câu chuyện rất hay, rất thực, cổ vũ bé tự lập, chỉ bé cách dùng tiền, cách bắt đầu làm một việc mình chưa từng làm, cách vượt qua những khó khăn để hoàn thành việc mẹ giao. Tranh rất đẹp, cứ lật tới trang nào là bé nhà mình thuộc luôn nội dung trang đó hihi
5
454812
2015-07-27 15:11:04
--------------------------
235583
6865
Một câu chuyện rất đậm nét Nhật Bản, qua nội dung câu chuyện của Bé Mi, đã truyền đạt cho bé nhà mình tính tự lập, cẩn thận và không nhút nhát nữa. 
Mình rất thích tranh vẽ trong chuyện, Đó là hình ảnh của cuộc sống Nhật đã được thu nhỏ qua câu chuyện này, lời thoại lại rất trẻ thơ nên thu hút bé nhà mình rất nhiều.
Những câu chuyện Ehon luôn thu hút các bạn nhỏ, vì cách thể hiện trong chuyện, truyện được vẽ tranh trãi dài trên cả 2 trang giấy, nên sẽ giúp các bé thầy rõ các tình tiết.
Rất thích mua những chuyện Ehon của Nhật.
5
323719
2015-07-21 10:22:55
--------------------------
210000
6865
Đây là câu chuyện mình tâm đắt nhất trong bộ truyện của ehon mà mình đã được đọc. vì tình tiết câu chuỵện là bé Mi bị ngã chảy máu chân nhưng vẫn tự mình đứng dậy đi mua sữa cho mẹ. khi đọc câu chuyện là đúng lúc chiều hôm đó con mình đi chơi bị vấp đá ngã chảy máu đầu gối, hai mẹ con nhớ lại mà nước mắt mẹ ứa ra vì thương cho bé Mi và thương cho con gái. ngôn ngữ phong phú, xúc tích: " tiếng leng kheng, chiếc xe lướt qua như một cơn gió, ngã sõng soài, tim đập thình thịch..." con mình đã học được những cụm từ này bổ sung vào vốn từ của con. hình ảnh đẹp và chi tiết nhất là bước tranh mà Mi gặp Tomo, có ăngteen, có chú chó ngồi trước nhà, có người đang ngồi học bài ở xa xa, rất hay đối với mình. cám ơn sách mang lại cảm xúc rất thực cho tôi.
5
667019
2015-06-18 16:38:34
--------------------------
204856
6865
Một cuốn sách trong bộ ehon dành cho bé mà tôi vô cùng ấn tượng. Tranh vẽ đẹp, bối cảnh rất Nhật, từ cây cối, con đường lên dốc cho đến sinh hoạt của con người. Hơn hết, nội dung câu chuyện khiến tội vô cùng thich thú.
Cuốn sách kể về sự tự lập của bé Mi, dũng cảm, hết mình, ngoan ngoãn. 5 tuổi, nếu ở Việt Nam, liệu đứa trẻ có thể đi xa hay ba mẹ yên tâm một mình nếu bé ra ngoài mua sữa cho bé.
Bé gặp cậu bạn Tomo trên đường đi nhưng không mải chơi, gặp chú đi xe đạp vội vàng nép mình vào bức tường chắn, làm rơi đồng xu 200 yên, vội vàng kiếm tìm dù chân tay trầy xước đau buốt...
Một cuốn sách một bài học tuyệt vời.
5
566059
2015-06-05 09:00:17
--------------------------
195769
6865
Tình tiết truyện rất chi tiết, tỉ mỉ, đưa ra các tình huống phù hợp với độ tuổi của bé, chẳng hạn như khi bé đến cửa hàng nhưng không biết làm cách nào để gọi người bán hàng, tình huống 1: bé gọi thật to nhưng vô tình tiếng xe ô tô đi qua át cả tiếng bé, tình huống 2: bé định gọi to nhưng khi phát ra thì chỉ bé thôi, … có khi bé định gọi thì lại có bác kính đen đi vào, rồi bác to béo vào mua hàng, … Nói chung các tình huống rất gần gũi và thực tế với bé khi một mình đi mua hàng giúp mẹ.
5
578967
2015-05-14 16:34:12
--------------------------
194588
6865
Chiến công đầu tiên của bé Mi khá là dễ thương như sự bắt đầu cho hành trình khám phá thế giới bên ngoài của trẻ thơ. Lần đầu tiên ra ngoài 1 mình để mua sữa cho em bé, Bé Mi phải đối mặt với khá nhiều rắc rối mà chính bé phải tự tìm cách giải quyết bằng chính những suy nghĩ non nớt của trẻ thơ. Chỉ là 1 bài học nhỏ nhưng sẽ là 1 khởi đầu cho tư duy của trẻ muốn khám phá thế giới xung quanh, giúp trẻ đối mặt với nhiều trở ngại mà mình cũng có thể làm được như người lớn. Với nét vẻ dễ thương, nội dung cô đọng, chắc chắn sẽ là 1 gợi ý khá hay cho phụ huynh.
4
166935
2015-05-11 09:14:05
--------------------------
192251
6865
Con gái 3 tuổi của mình rất thích nghe mẹ đọc truyện. Mình đã mua 6 quyển ehon: Chiến Công Đầu Tiên Của Bé Mi, Trường Mẫu Giáo Của Chú Voi Grumpa, Bàn Tay Kỳ Diệu Của Sachi (Tặng Kèm Tranh Ehon Tô Màu), Cá Voi Con Ơi - Lớn Nhanh Nào, Cùng Lau Cho Sạch Nào, Chào Mặt Trăng. Và con gái mình thích nhất quyển Chiến công đầu tiên của bé Mi và Bàn tay kỳ diệu của Sachi. Tối nào trước khi đi ngủ cô bé cũng đòi mình đọc cho nghe. Mình thấy truyện dạng này mang tính giáo dục cao, mình có thể vừa đọc cho con nghe và vừa dạy cho bé những điều hay.
Mình mong sẽ có thêm nhiều tập mới nữa để mua tiếp cho con gái của mình.
5
210330
2015-05-04 09:01:55
--------------------------
184127
6865
Đọc câu truyện trong sách tôi như thấy lại tuổi thơ của mình trong đó. Và cũng giống bé Mi, với lần đầu tiên được mẹ nhờ đi mua đồ tôi đã rất căng thẳng dù trước đó đã đi cùng bố, mẹ nhiều lần. Cũng là cái ngã đau điếng mà vẫn mạnh mẽ đứng dậy đi tiếp không khóc, không kêu đau. Cách viết nhẹ nhàng, gần gũi, câu từ đơn giản, dễ nhớ hy vọng sẽ phù hợp với các em bé. Dù sao thì giá của những cuốn truyện tranh ehon này vẫn rất là mắc.
4
470181
2015-04-17 10:58:27
--------------------------
179803
6865
Trước mình thiếu 20k cho đủ đơn hàng nên đã quyết định mua cuốn truyện có bìa rất dễ thương này . 
Sau khi mang về mình đã mở ra đọc thử và kết luận : Truyện được vẽ rất dễ thương sinh động . Sách 100% là trang màu, bìa đẹp , chất liệu giấy rất tốt. Sau đó mình đọc cho đứa cháu mình và thì nó cảm thấy rất thích thú . Sách ý nghĩa , dễ hiểu giúp cho trẻ em dễ dàng cảm nhận mà không gây khó hiểu . Sách còn được tặng kèm thêm hai bức tranh để tô màu nên cháu mình càng thích hơn ♥ .
5
335856
2015-04-07 23:50:14
--------------------------
176695
6865
Vừa nhận được truyện sáng nay, chiều nay mới mở ra xem truyện cá voi con ơi lớn nhanh nào thì mình hơi bị bối rối, vì câu chữ trong truyện đã thay đổi phần nào, mình yêu truyện ehon so với những đầu truyện khác ở Việt Nam là do ngôn từ đơn giản, dễ hiểu, gần gũi với cuộc sống hàng ngày, bé nhà mình vừa nghe mẹ đọc 3 - 4 lần là thuộc ngay và đọc theo ko sót 1 chữ, nhưng lần mua này truyện đã tái bản và ngôn từ có vẻ hoa mỹ và cao siêu hơn, giống như nhìn tranh vẽ và kể truyện vậy, thật sự rất thất vọng. Ví dụ như đoạn cá voi con ở trong bụng mẹ, truyện cũ là ông trăng ơi chờ cháu nhé thì truyện mới lại là ông trăng ơi chờ em nhé, còn truyện chú voi Grumpa ở truyện cũ khi chú voi than thở thì thốt lên rằng buồn quá, ôi buồn quá thì truyện mới lại là Chao ôi buồn quá. Mình thật sự tha thiết mong rằng nhà xuất bản hãy dịch những ngôn từ sát với cuộc sống hàng ngày để cho các bé khi nghe truyện cảm thấy như chính mình đang kể lại chuyện của mình chứ không phải là cuốn văn học sáo rỗng mang vẻ ngoài là truyện thiếu nhi
1
380921
2015-04-01 17:50:41
--------------------------
175592
6865
Khi thấy cuốn truyện này trên Tiki, mình quyết định mua cả ba tặng sinh nhật em gái.
 Vừa mới nhận hàng của Tiki, mình phải mở ngay ra xem. Khi đọc thử, thấy tranh vẽ giản lược, cũng rất ngộ nghĩnh, màu sắc sinh động và bắt mắt. Nội dung rất đơn giản, chỉ là những câu chuyện ngây thơ đầu đời của con trẻ. Đó là bé Mi lần đầu đi mua hàng, là chú voi Grumpa với trường mẫu giáo đáng yêu, hay là lời tâm tình của cá voi mẹ.Truyện vừa mang tính giải trí, vừa có thể giáo dục trẻ em về nhiều phương diện. Ngoài ra truyện còn tặng một số tranh tô màu, món quà nhỏ rất thích hợp với trẻ em.
Còn một điều là mình khá tiếc là QV xuất bản ngay vài cuốn mới sau khi mình mua. Nếu có nhiều thì mua cả bộ thích hơn nhiều
5
401487
2015-03-30 17:05:19
--------------------------
160985
6865
Mình đã mua một bộ 3 cuốn trên tiki. Trong 3 cuốn Chiến công đầu tiên của bé Mi, Cá voi con ơi lớn nhanh nào và Trường mẫu giáo của chú voi Grumpa, con mình thích nhất là  Chiến công đầu tiên của bé Mi vì hình vẽ rất đẹp, sinh động và nhiều màu sắc phù hợp với thiếu nhi. Nội dung truyện cũng rất dễ thương và có tính giáo dục con trẻ biết giúp đỡ cha mẹ những chuyện vừa sức mình. Sau khi đọc truyện này mỗi khi nhờ con mình hay nói con có giỏi như bạn bé Mi không để khích lệ con làm.
5
284620
2015-02-26 11:24:00
--------------------------
150733
6865
Tôi mua bộ 3 cuốn ehon: Chiến công đầu tiên của bé Mi, Cá voi con ơi lớn nhanh nào và Trường mẫu giáo của chú voi Grumpa để đọc cho con trai mình lúc con hơn 2 tháng tuổi. Bé rất thích thú và chăm chú nhìn tranh mỗi khi mẹ đọc cho nghe (đặc biệt những đoạn mẹ lên giọng, xuống giọng). Và tôi đang chờ đợi cuốn ehon thứ 4 “Bàn tay của Shachi” đang trên đường giao hàng về nhà.
Tôi rất thích những cuốn truyện tranh này vì cốt truyện rất ngắn gọn và gần gũi, dễ hiểu phù hợp với trẻ em từ 0 tuổi, có hình vẽ minh họa đẹp, giấy đẹp. Nhưng rất tiếc là dịch vụ Bookcare chưa hỗ trợ bọc sách đối với những cuốn truyện “ngoại cỡ” này. Hy vọng trong thời gian tới Tiki có thể hỗ trợ cải thiện thêm ở điểm này.
5
490746
2015-01-17 16:02:21
--------------------------
132577
6865
Truyện rất đáng yêu, ngôn từ viết đơn giản dễ hiểu, tranh vẽ dễ thương. Tuy là một câu chuyện ngắn nhưng đối với các em lại là một cuộc phiêu lưu với nhiều thử thách khi bé Mi bị ngã, đánh rơi tiền, rồi khi cô bán hàng không nhìn thấy bé Mi nhỏ bé làm bé Mi sợ quá, tim đập thình thịch.
Con gái nhỏ của mình cũng là bé Mi nên mình mới mua quyển sách này và mình cảm thấy nó rất thú vị, nhẹ nhàng nhưng lại cuốn hút.
Mình cũng sẽ mua tiếp 2 quyển còn lại trong bộ 3 quyển này.
5
198939
2014-11-01 13:30:28
--------------------------
