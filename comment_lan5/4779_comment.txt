473301
4779
Lại là câu truyện về doreamon và 6 người bạn đến từ mọi miền trên thế giới rồi đây ! Vì có tình cảm thật sự đặc biệt với những nhân vật này cho nên mình đã đặt ngay cuốn " Đội quân doraemon ( tập 5 )" trên tiki,cảm nhận đầu tiên khi cầm quyển truyện trên tay chính là dày ( cầm thích vô cùng ),thứ hai là màu sắc bắt mắt bonus thêm nửa cái bánh gato rất là xinh nữa haha.Mình cảm thấy nội dung các câu truyện trong tập này cũng khá ổn và dễ thương.
5
1419914
2016-07-10 14:04:23
--------------------------
444919
4779
Nói sao nhỉ, tự nhiên mình cảm thấy cuốn này nét vẽ "mất phong độ" so với các tập trước, phần nội dung thì cũng không có chuyến phiêu lưu đủ để nguy hiểm, hấp dẫn, hồi hộp, đau tim, đánh nhau điên cuồng đến mức để lại ấn tượng sâu sắc trong lòng mình. Mình cũng không thẻ mua tập này vì đang tích tiền mua Doraemon truyện ngắn cho đủ bộ để về sưu tầm và có lẽ cũng là vì mình đã đọc cuốn này trên mạng rồi, dù công nhận là cầm truyện trên tay vẫn sướng hơn.
4
1420970
2016-06-09 10:43:47
--------------------------
201155
4779
Dường như chú mèo máy Doraemon đã gắn liền với tuổi thơ của biết bao nhiêu bạn nhỏ Việt Nam. Mình cũng thích đọc Doraemon đặc biệt là các tập Doraemon thêm. Vẫn là những tình tiết vui nhộn, vẫn là những nét buồn cười và hóm hỉnh rất đặc trưng, truyện mang đến cho ta không chỉ những giấy phút vô tư, thoải mái và còn cho ta nhiều điều sâu sắc. Đó là tình bạn cao đẹp, bền chặt của nhóm mèo máy Doảemon hay những tình cảm đẹp đẽ, đầy nhân văn trong cuộc sống. Tập truyện này chúng ta sẽ cùng đội quân của bạn mèo ú dễ thương đi thám hiểm ở vương quốc kẹo ngọt đầy thú vị và gặp một người bạn là đầu bếp tài ba của nhóm mèo máy đấy!
5
466113
2015-05-26 18:37:17
--------------------------
