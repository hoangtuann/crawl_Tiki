433703
4787
Thật sự không biết có phải mình là người lớn hay không mà mình thực sự không thể thích tác phẩm này. Truyện đúng là có những tình tiết trẻ con, những cử chỉ và lời nói với những câu chuyện ngây ngô của trẻ con khiến cho chúng ta phải bật cười. Điều đó chẳng có gì đáng chê trách cả. Nhưng thật sự không thể hiểu nổi sách là cho thiếu nhi mà lại có những tình tiết người lớn như buổi tối cha mẹ shin muốn gần gũi nhưng không được vì shin phá. Nên không muốn mua sách này cho em nữa
1
827272
2016-05-21 17:01:00
--------------------------
409131
4787
Không hổ danh truyện tranh đến từ Nhật Bản, truyện  về cậu bé shin rất nghịch ngợm hài hước và đáng yêu. Truyện không chỉ dành cho trẻ em mà nó còn thích hợp cho mọi người đọc để giải trí và xã stress. Về chất lượng của giấy thì khỏi phải chê, giấy in trắng và dày , chữ và hình ảnh được in rõ ràng, phần đầu của truyện còn được in màu trong rất thích và đẹp. Tuy nhiên chắc do tập đầu nên hình vẽ chưa được đẹp cho lắm nhưng dù sao vẫn ủng hộ và sẽ tiếp tục đọc truyện này nữa.
4
958015
2016-04-01 22:14:15
--------------------------
396268
4787
"Shin cậu bé bút chì" – là một bộ truyện tranh của tác giả người Nhật Bản Yoshito Usui. Có lẽ vì đây là tập đầu tiên của manga series này nên tác giả vẽ không được đẹp cho lắm. Thoạt đầu mình cũng không có mấy thiện cảm với bộ truyện này bởi lẽ cũng vì nhân vật chính là cậu bé Shinnosuke – mọi người thường gọi cậu là "cu Shin". Cậu bé này mới có 5 tuổi nhưng chỉ thích quậy phá, nghịch ngợm khắp nơi, vẽ hình con voi lên quần lót, thích lột đồ,... nhưng khi đọc bộ truyện này, các bạn sẽ thấy những trò đùa nghịch đó rất dễ thương, nghộ nghĩnh và .... hài hước khiến các bạn không thể rời mắt vì quá hấp dẫn. Một bộ truyện tranh tuyệt vời!
5
1142307
2016-03-13 09:15:11
--------------------------
384229
4787
Có lẽ hình ảnh cậu nhóc lém lỉnh thông minh và rất hài hước vui tính đã không còn xa lạ với những bạn đọc gà xa nữa rồi! Shin - Cậu bé bút chì tập 1 - Tái bản 2015 là quyển truyện cực kì thú vị bạn sẽ chết cười thật đấy! Khi bạn lên giường ngủ bạn nhớ về hình ảnh Shin bạn sẽ cười không ngớt nổi, truyện hay mà lại rất chân thật, gần gủi với cuộc sống thường nhật của mỗi gia đình, nhưng bằng tài năng và khả năng dán tạo tuyệt vời của rác giả đã cho ra đời bộ truyện này!
5
853243
2016-02-21 22:53:39
--------------------------
363131
4787
Shin - Cậu Bé Bút Chì (Tập 1) - Tái Bản 2014 là một quyển truyện đầu tiên được viết từ tác giả "Yoshito Usui" . Cậu bé này đến từ Nhật Bản , một trong những quốc gia mà tôi rất thích! Cuốn truyện nói về những sự việc xung quanh của một cậu bé tên là "Shinnosuke" , mọi người thường gọi cậu là "cu Shin" . Hình vẽ nhân vật và những cảnh vật... vẽ không đẹp cho lắm! Vì đây là tập đầu tiên nên truyện đó là đương nhiên. Chất lượng giấy rất tốt , màu in cũng vậy, những giấy truyện còn quá mỏng và dễ rách!
4
603156
2016-01-03 16:02:17
--------------------------
166860
4787
Lần đầu đọc truyện tôi dã phải hạ nó xuống vì hình vẽ khá loằng ngoằng, rắc rối mà hơn nữa chữ nhỏ quá. Sau đó lại cầm lên khi nghe điệu cuwoif điên dại của con em. Ừ thì thôi đọc một lần vậy. Ấy thế mà tôi đọc một mạch hết veo quyển truyện gần 200 trang trong tiếng rưỡi. Quả thực đúng là chết cười thật.  Tự hỏi sao có một thằng bé lóc nhóc, ngỗ nghịch, ngang buwowbgs, đểu gải, thích lột đồ, thích ngắm gái đẹp như thằng Shin nhỉ. Nó mới có năm tuổi thôi mà.Nhưng Shin cũng vô cùng đáng yêu đấy.
4
186455
2015-03-13 17:58:54
--------------------------
