402720
3339
Sách hay, nhiều nội dung hấp dẫn giúp nâng cao trí thông minh cho trẻ. Màu sắc của sách rất tươi sáng, chắc chắn bé nào cũng thích. Mua cuốn sách cho cháu mình, nhưng thỉnh thoảng mình cũng lôi ra ngắm nghía 1 chút, nếu bé nhà mình thích thì chắc chắn mình sẽ mua trọn bộ cho bé luôn. Giá sách cũng vừa túi tiền nên mình luôn ủng hộ. Trên tiki có nhiều loại sách dành cho trẻ nên mình rất thích, sách nào cũng hay, dù bé còn nhỏ nhưng mình luôn muốn tạo điều kiện cho bé được đọc nhiều sách để phát triển cả IQ và EQ. Tiki giao hàng nhanh và cẩn thận nên sẽ ủng hộ tiki dài dài.
5
337031
2016-03-22 20:27:48
--------------------------
328137
3339
- Về phần dịch vụ của Tiki: Giao hàng nhanh, đóng gói cẩn thận, sản phẩm mới, đẹp
- Về phần nội dung sách: 
 1. Sách hay, nội dung sinh động, gần gũi rất phù hợp với độ tuổi của trẻ.
2. Hình ảnh đẹp, rõ nét, màu sắc bắt mắt
 3. Có rất nhiều bài tư duy cho trẻ suy nghĩ tốt.
 4. Thiếu phần chấm điểm. Nên có chấm điểm bằng hình dán như sách bé tập học toán vì mình thấy: bé nhà mình rất thích phần này, luôn cố gắng học tốt để được các hình dán gấu con màu xanh dương, hay voi màu nâu,...
 5. Giá tốt, nói chung là cũng khá hài lòng với bộ sách mua được. Bé đã học xong quyển này và cứ đòi học lại mãi thôi :).
 Cám ơn Tiki.
5
183878
2015-10-29 09:16:45
--------------------------
310199
3339
Trung thu này mình mua tặng mỗi cháu một cuốn sách. Khi cháu mình nhận được cuốn Tủ Sách Khơi Nguồn Trí Tuệ - Phát Triển Tư Duy Cho Trẻ 3 - 4 Tuổi cháu rất thích. Cuốn sách có nhiều hình ảnh đẹp, dễ thương nên cháu rất háo hức lật từng trang xem đi xem lại. Trong sách hướng dẫn cách chơi các trò chơi trí tuệ và hình ngộ nghĩnh nên các bé vui vẻ hợp tác cùng ba mẹ.Thích cách phục vụ các bạn Tiki rất chuyên nghiệp, giá cả lại phải chăng thường xuyên có khuyến mãi.
4
437659
2015-09-19 13:12:24
--------------------------
277721
3339
Đây là cuốn sách khá là bổ ích cho các bạc lứa tuổi mầm non với rất nhiều các hoạt động thú vị, khám phá các hoạt động tư duy cho các em. Từ những kĩ năng rất dễ như quan sát, tìm các đặc điểm đến các kĩ năng khó như tổng hợp, phân tích, so sánh... Nếu như trẻ tự học tại nhà thì đây có lẽ là 1 cuốn sách gợi ý các hoạt động dạy trẻ cho bố mẹ khá tốt. Mình rất hài lòng với dịch vụ đóng gói và giao hàng của Tiki: giao hàng khá là nhanh và đóng gói trong hộp cac tong rất đẹp và chuyên nghiệp. Cảm ơn Tiki.
4
724204
2015-08-25 10:46:44
--------------------------
