467504
3540
Đây là quyển thứ hai của tác giả này mà mình đọc được, quyển trước là Kẻ tầm xương... Phải nói cách tác giả miêu tả kỹ lưỡng, chi tiết về các kỹ thuật liên quan để có thể tìm ra hung thủ rất độc đáo. Nếu không đọc các tác phẩm này, mình thật không biết là các kỹ thuật liên quan có thể giúp ích và được quan tâm nhiều như vậy trong công tác phá án hình sự ở Mỹ. Đối với ngôn ngữ cơ thể thì mình có tìm hiểu nên biết, nhưng về giám định tài liệu thì chưa bao giờ nghĩ tới. Nhưng mà vẫn còn chi tiết trong truyện mình thắc mắc đó là các đoạn miêu tả ...click.. về nhân vật Digger... Vì trong truyện nói nhân vật này bị chấn thương nhưng lại có các miêu tả như là hung thủ đã gài máy móc, hay con chíp để điều khiển tên này?!? Ngoài ra, mình cũng thấy truyện in trên giấy tốt, dịch tốt và mình không thấy có lỗi in ấn hay chính tả nào..
4
1028782
2016-07-03 22:32:56
--------------------------
432876
3540
Giọt Lệ Qủy là một trong những quyển sách hay và mới lạ của Jeferry Deaver. Nếu là fan của dòng truyện trinh thám chắc có lẽ chúng ta đã quá quen thuộc với những nhân vật như nhà hình sự học Lincoln Rhyme, chuyên gia ngôn ngữ cơ thể Kathryn Dance, v.v... Thì ở tác phẩm này tác giả đã đưa một chuyên gia giám định tài liệu vào tiểu thuyết này. Qua mỗi một tiểu thuyết của Jeferry Deaver tôi luôn học hỏi được một điều gì đó ẩn sau mỗi câu chuyện. Các tác phẩm của ông xoay quanh những thứ công nghệ, có thể nói là trinh thám kiểu hiện đại làm cho tôi rất thích thú.
5
509425
2016-05-19 22:57:49
--------------------------
421344
3540
Cuốn sách này của Jeffrey Deaver giới thiệu cho chúng ta một nhân vật trung tâm hoàn toàn mới. Nếu trước đây chúng ta có nhà hình sự học Lincoln Rhyme, chuyên gia ngôn ngữ cơ thể Kathryn Dance của CBI thì trong cuốn sách này là sự xuất hiện của Parker Kincaid, cựu đặc vụ FBI, chuyên gia giỏi nhất nước Mỹ về giám định tài liệu. Điều này mang đến cho cuốn sách một sự mới lạ, có thể gọi là phá án dựa trên những nét chữ. Như mọi khi, Deaver lại mang đến cho độc giả một cuốn sách đầy hấp dẫn, với những màn chạy đua cùng thời gian để giành giật lấy sự sống, và một kết thúc không thể bất ngờ hơn chi khi bạn đọc đến trang cuối cùng.
4
122042
2016-04-24 22:09:54
--------------------------
401380
3540
Với những fan của Jeffery Deaver, vốn quen với tiết tấu nhanh và dồn dập qua những tác phẩm của ông về Seri nhà hình sự học Lincoln Rhyme thì có thể sẽ hơi ngán do các tình tiết hơi chậm, thiên nhiều về suy luận và giám định tài liệu, đặc biệt là chữ viết. Nhưng không vì thế mà "Giọt lệ quỷ" mất đi chất riêng của mình, tác phẩm vẫn là một cuốn sách đáng đọc dành cho những bạn yêu thích trinh thám, đặc biệt là muốn tìm hiểu về giám định chữ viết, một thể loại điều tra tương đối mới mẻ và lạ lẫm.

4
182482
2016-03-20 16:09:40
--------------------------
350415
3540
Phong cách truyện “Giọt lệ quỷ” tương tự như phim hành động Mỹ hay một số tác phẩm trinh thám - hình sự Mỹ khác. Mở đầu đã khá cuốn hút với nhịp truyện nhanh và gay cấn khiến mình đọc liền một mạch không muốn buông rời. Đề tài giám định tài liệu cho người đọc cảm giác háo hức khám phá những uẩn khúc đằng sau con chữ. Có lẽ điểm đắt giá nhất của truyện là ở yếu tố bất ngờ, đọc đến tận những trang cuối rồi, khi mà tưởng mọi thứ đã kết thúc thì vẫn xảy ra điều bất ngờ. Dù có một số tình tiết mình thấy hơi phi lý và cường điệu (như đoạn cuối về đứa trẻ) nhưng nhìn chung thì tác giả viết khá chặt chẽ, đều có diễn giải và giải thích hợp lý nên mình hài lòng.
Sau khi đọc xong cuốn trinh thám khó đoán này mình lại bắt đầu nghiền thể loại này, hy vọng sẽ tìm đọc được những tác phẩm hay và hấp dẫn hơn nữa.
4
386342
2015-12-10 15:48:41
--------------------------
326688
3540
Đây là tác phẩm viết về một tuyến nhân vật khác của Jeffery, nhân vật giám định chữ viết. Phải nói là mình hơi bất ngờ trước tuyến nhân vật mới này mà tác giả xây dựng. Ai trong chúng ta cũng biết chữ viết tay  của một người cho ta biết một số điều về người đó như: tính cách, thói quen, tay thuận,... Và qua tác phẩm này chúng ta sẽ được khám phá thêm rất nhiều khía cạnh của nét chữ. Tiết tấu chuyện ban đầu hơi chậm rãi nhưng càng về sau càng được đẩy nhanh đến nghẹt thở. Những chi tiết nhỏ đôi khi lại nói lên rất nhiều điều. Có thể khi đọc những chương đầu bạn sẽ thấy không có gì hấp dẫn nhưng những chương sau tác phẩm sẽ khiến bạn không thể ngừng lại được. Mình đã thức đến gần 2h sáng để đọc cho hết vì không ngừng lại được.
5
738980
2015-10-26 13:23:41
--------------------------
287905
3540
Mình đã quen với việc theo dấu chân nhà hình sự học lừng danh Lincoln Rhyme cùng nữ cảnh sát xinh đẹp Amelia Sachs qua các cuộc khám nghiệm hiện trường. Nên khi chuyển qua lĩnh vực mới là giám định chữ viết thì thấy hơi bị bất ngờ. cũng vì việc giám định chữ viết để phán đoán hung thủ vẫn là lĩnh vực mới lạ nên cũng chưa hấp dẫn cho lắm. Cốt truyện lần này cũng hơi lan man, dài dòng nên có vẻ nhàm chán. Tác giả lại miêu tả nhiều về tâm lý của Kincaid và Lukas, diễn biến mối quan hệ giữa 2 người nên mình không thích cho lắm. Dù sao đây cũng là tiểu thuyết trinh thám mà. :) Cái kết bất ngờ cũng đã vớt vát lại được một ít hấp dẫn cho quyển sách. Nhưng tựu chung lại thì mình vẫn bị thất vọng bởi "giọt  lệ quỷ."
3
126889
2015-09-03 09:17:29
--------------------------
242271
3540
Theo mình đánh giá, đây vẫn là 1 trong những tác phẩm thành công của Jeffery Deaver. Với việc khai thác 1 đề tài mới lạ: giám định chữ viết, lại 1 lần nữa Deaver khiến cho chúng ta ngạc nhiên trước kho kiến thức phong phú, đa dạng và đồ sộ của mình. Cốt truyện chặt chẽ với những tình tiết gay cấn, hấp dẫn, kết thúc gây bất ngờ cho độc giả, xây dựng tâm lý nhân vật khá xuất sắc, mỗi nhân vật mang 1 nét tính cách riêng biệt. Riêng mình thấy Kincaid có phần hơi yếu đuối quá vì luôn lo nghĩ cho 2 đứa con. Và với những ai đọc Deaver nhiều thì có lẽ sẽ thấy hơi thất vọng 1 chút về phần cuối truyện. 
4
38833
2015-07-26 13:41:57
--------------------------
