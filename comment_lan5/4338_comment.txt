485876
4338
Ban đầu mua quyển sách này chỉ nghĩ đọc cho vui, không ngờ chỉ với những bức tranh và lời dẫn ngắn gọn, tôi có thể suy nghĩ đến nhiều khía cạnh của cuộc sống hơn, dường như cuộc đời của "hòn đá xanh" này chất chứa rất nhiều thông điệp và ý nghĩa. 
Jimmy Liao đã trở thành một tác giả ừa thích của tôi.Chắc chắn tôi sẽ tiếp tục đọc những cuốn sách tiếp theo của ông. 
Cảm ơn Tiki đã đem đến những cuốn sách hay và bổ ích.
Dịch vụ của Tiki rất tốt và đảm bảo.

5
1399543
2016-10-06 21:34:35
--------------------------
419320
4338
Mình đã mua hết 3 cuốn của Jimmy Liao (Nàng rẽ trái... và âm thanh của sắc màu). Và nói thật mình thích nhất cuốn này trong bộ 3, tranh thì đẹp miễn bàn rồi, từng câu thơ cũng rất thấm thía, có tí trầm buồn, lạc lõng nhưng vẫn đầy hi vọng. Cảm giác có phần liên hệ với chính bản thân mình nữa, tâm trạng chơi vơi, vụn vỡ... Cuộc đời đời người cũng vậy, mỗi người đều có cội nguồn của riêng mình, trải qua bao khó khăn thăng trầm, kí ức, tình cảm, tâm hồn mỗi thứ đều vơi bớt môt chút ở khắp những nơi mình từng đi qua cho đến khi gặp được chốn yên bình nơi sau chót, khiến mình cảm động rất nhiều. Hi vọng sẽ có thêm nhiều tác phẩm của Jimmy Liao được xuất bản trong thời gian tới! <3
5
55
2016-04-21 01:43:31
--------------------------
406066
4338
Mình thật sự không hiểu tại sao cuốn sách này lại hấp dẫn mình đến như vậy.
Ngay từ khi thấy bìa sách, mình như bị mê hoặc và lập tức vào coi nội dung. Cả nội dung lẫn bìa đều mang một vẻ mê hoặc khó từ chối.
Cả cuốn sách đều là những trang giấy đầy màu sắc tuyệt vời và ý nghĩa. Tuy nội dung chỉ được thế hiện qua vài dòng chữ mỗi trang nhưng lại cho thấy sự đơn giản, súc tích và sâu sắc đằng sau mỗi bức tranh.
Về nội dung và trình bày cuốn sách nói riêng, mình có thể nói đây là một cuốn sách đầy hấp dẫn. Nhưng về chất lượng, mình rất không vui khi sách tiki giao đến lại có chất lượng cực kỳ kém, sách của mình bị trầy xước hư hại bìa đủ đường. Tuy thật may là phần giấy bên trong không sao nhưng nó lại càng làm mình tiếc nuối hơn vì đây là một cuốn sách hay và mình mong nó phải hoàn hảo. Mong tiki bảo quản sách tốt hơn.
5
744492
2016-03-27 17:44:52
--------------------------
400852
4338
Tôi đã giới thiệu quyển này với vài người bạn có con nhỏ xung quanh mình. Vì đây là quyển sách con tôi cực kỳ thích, cứ đòi đọc và kể mãi. Một quyển sách với hình vẽ lạ, màu sắc tuyệt đẹp, lời văn ngọt ngào, đượm một chút buồn thương, cuốn hút. Trẻ con thích hình vẽ đẹp, lạ mắt, nhưng trên hết là câu chuyện chạm vào trái tim con. Cuộc sống cứ trôi đi, ai cũng nhớ về nơi mình đã sinh ra... Và nhiều điều sâu xa hơn thế... Như là sự đổi dời của cuộc sống, của nhân gian... Con nhỏ sẽ cảm nhận theo kiểu khác, lớn dần con sẽ hiểu câu chuyện sâu hơn. 
4
138991
2016-03-19 22:34:59
--------------------------
353469
4338
Lại 1 tác phẩm tuyệt vời của Jimmy Liao. Ông dùng những bức tranh đầy màu sắc để nhân cách hóa, để diễn tả hành trình tìm về nhà của hòn đá xanh. Sách khắc họa niềm khao khát được quay về tổ ấm, quê hương của hòn đá xanh, từ lúc được mang đi là 1 hòn đá lớn, vẫn không nguôi hi vọng khắc khoải về quê hương; cho đến khi tan vỡ thành những mảnh đá nhỏ, những hạt bụi vẫn cố gắng để được trở về nhà, về khu rừng thân yêu. Nó cũng giống như khát khao của người xa quê hương lúc gần đất xa trời muốn được trở về. Khâm phuc tài năng của Jimmy Liao, với cách dùng hình ảnh, màu sắc để kể chuyện
5
62433
2015-12-16 10:49:55
--------------------------
321451
4338
lần đầu tiên đọc cuốn sách này trong nhà sách, mình bị cuốn hút từ trang đầu tiên và cứ thế đọc đến trang cuối cùng. mình rất thích tác phẩm này, cứ như thấy được một phần nào bản thân mình trong đó, có gì đó cô đơn, lạnh lẽo và cứ mãi bị cuộc sống cuốn theo nhưng trái tim mình thì cứ ngóng về quê nhà nơi mà ở đó mình tìm thấy sự thân thuộc và bình an, nơi mà mình có cảm giác mình được trở về. cảm ơn Jimmy Liao vì tác phẩm này. hy vọng sẽ có nhiều hơn nữa tác phẩm của Jimmy Liao 
5
636237
2015-10-13 22:49:28
--------------------------
253224
4338
Mình đặt nguyên bộ ba Hòn Đá Xanh ; Nàng rẽ trái , chàng rẽ phải và Âm thanh của sắc màu. Và cuốn này làm mình hơi thất vọng. Thực sự thì chất lượng giấy cũng như hai quyển kia khá tốt. Màu sắc , hình vẽ tuyệt vời. Nhưng không hiểu sao nó làm mình cảm thấy nhạt. Chắc tại mình không hiểu rõ nội dung của nó. Chắc mình phải đọc lại và để hiểu hơn. nhưng hiện tại là mình không hề thích quyển này tẹo nào ... Nội dung lặp lại quá nhàm chán . 
3
632918
2015-08-04 11:32:38
--------------------------
247702
4338
Hòn Đá Xanh là một câu chuyện tuyệt vời với biết bao ý nghĩa khác của tác giả Jimmy Liao. Câu chuyện về hòn đá bị con người chia cắt với quê hương, luôn nhớ nhung nửa kia cuả mình, và cuối cùng dù nát thành tro bụi vẫn tìm đường trở về thật khiến người ta cảm động. Đây là câu chuyện vĩ đại về tình thân, tình yêu nhưng cũng là lời kêu than của tự nhiên đang từng ngày từng giờ bị con người huỷ hoại. Dù là vật tưởng như vô tri cũng có tình cảm thiêng liêng nên xin đừng làm tổn thương nó.
5
167283
2015-07-30 16:39:36
--------------------------
197036
4338
Cảm giác dạo một vòng nhà sách rồi theo cảm tính lật dở tùy ý từng quyển bắt mắt. 
Mình rất thích bìa quyển sách, hơn hết lại là một quyển sách với những trang vẽ màu  tuyệt vời đến vậy. Sau khi đọc thử vài trang đầu, lại đọc đọc rồi đọc cho đến hết. Một hành trình dài, phải nói như thế. "Một ngàn năm đã trôi qua. Một trăm năm đã trôi qua ..." Mình rất thích câu này, câu nói được mở đầu và kết thúc với ý nghĩa khác nhau.
Mỗi người sẽ có một cảm nhận khác nhau sau khi đọc xong tác phẩm này, với mình, mình cảm thấy hòn đá này thật cố chấp, tuy nhiên những gì đã qua thì không thể trở lại như cũ, dù đã cách rất xa quê hương, nơi thuộc về mình nhưng hòn đá ấy vẫn luôn mong mỏi nhớ về nó, cho đến khi hóa thành những hạt bụi nhỏ bé được gió cuốn đi thì hòn đá vẫn ngóng trông về một nơi xưa cũ. Ngọn lửa ấy mãi không bao giờ tắt.
Mặc dù đã đọc qua rất nhanh ở nhà sách, nhưng mình nghĩ sẽ mua tác phẩm này về, cùng với hai tác phẩm đã được xuất bản ở Việt Nam. Những bức vẽ vô cùng đẹp.
5
292653
2015-05-16 20:40:36
--------------------------
