457149
2849
Vũ Trọng Phụng sáng tác thiên về đời sống của những người dân thi thành thay vì sáng tác về người nông dân như những tác giả cùng thời. Nhưng ông đã rất thành công khi đã khắc họa được bức tranh thị thành với những thói hư tật xấu, với những con người muốn được làm tây một cách nửa vời làm cho xã hội đốn mạc và mất đi những vẻ đẹp văn hóa truyền thống quý giá của người Việt. Ông viết với giọng văn đầy trào phúng nên khi đọc ta sẽ rất dễ bật cười nhưng sau đó lại phải ngẫm nghĩ về cái cười đoa
5
827272
2016-06-24 06:40:42
--------------------------
421486
2849
Vũ Trọng Phụng là một trong những nhà văn tôi rất hâm mộ từ khi học bài "Hạnh phúc của một tang gia" trích trong tác phẩm Số đỏ. Cảm nhận đầu tiên của mình khi nhận sách này là bộ sách rất dày, có đầy đủ mọi tác phẩm trong cuộc đời viết văn của ông. Những truyện ngắn của ông không hiểu sao làm cho tôi mỗi khi đọc xong đều cảm thấy ấn tượng và khó quên. Một quyển sách đầy tính nhân văn, truyện của Vũ Trọng Phụng luôn có tính chất châm biếm về thói hư tật xấu của xã hội đương thời. Cảm ơn tác giả đã mang đến một quyển sách xuất sắc.
5
509425
2016-04-25 11:07:32
--------------------------
412095
2849
Dưới ngòi bút trào phúng của nhà văn Vũ Trọng Phụng đã cho người đọc cái nhìn mới lạ về xã hội Việt Nam lúc bấy giờ Tây ta lẫn lộn. Hồi còn đi học cấp 3 có đọc qua tác phẩm Số đỏ thì cảm thấy rất thích cách hành văn của ông, đem lại tiếng cười đồng thời cũng phê phán xã hội tân thời lúc đó. Quyển này tuy k trọn vẹn tất cả các tác phẩm của ông nhưng cũng khá đầy đủ và chi tiết, thiếu mỗi tác phẩm Làm đĩ thì phải. Nói chung đây cũng là một tác phẩm đáng để đọc
5
195428
2016-04-07 07:27:10
--------------------------
402216
2849
Lớp 11 học Hạnh phúc của một tang gia của Vũ Trọng Phụng bị ấn tượng bởi lối hành văn, cách dùng ngôn từ và cách kể chuyện trào phúng của ông. Thế nên về lên tiki tìm ngay. Sách hội tụ đầy đủ các tác phẩm của Vũ Trọng Phụng, từ tiểu thuyết, truyện ngắn đến kịch,...  cho ta một cái nhìn đầy đủ về một xã hội "chó đểu" trước cách mạng tháng Tám. Giấy in rất nhẹ thế nên sách có dày thì khi đọc cầm cũng không bị mỏi tay. Giấy lại còn màu vàng nữa, không bị chói mắt, đỡ cận thị. 
4
744628
2016-03-21 22:24:27
--------------------------
388170
2849
Tôi đã rất háo hức khi đặt mua sản phẩm này vì Vũ Trọng Phụng là một trong những tác giả yêu thích của tôi trong chương trình học phổ thông. Tiki đóng gói rất cẩn thận, sản phẩm có bìa thiết kế tinh tế, phù hợp với giai đoạn lịch sử mà các tác phẩm trong sách phản ánh, giấy tốt, dù sách dày nhưng không hề nặng và còn được tặng kèm bookmark. Về nội dung, cốn sách cũng tập hợp tương đối đầy đủ những tác phẩm nổi tiếng của Vũ Trọng Phung, thậm chí có cả tiểu thuyết "Số đỏ" được nhiều đọc giả yêu thích. Một sản phẩm đáng mua.
4
741642
2016-02-28 12:32:31
--------------------------
341176
2849
Hồi còn học PT, biết tới Vũ Trọng Phụng qua đoạn trích Hạnh phúc của 1 tang gia (của tác phẩm Số đỏ). Nhớ mãi câu nói của cụ cố Hồng: biết rồi, khổ lắm, nói mãi. Câu nói mà thời nay đã trở nên phổ biến. Chắc cụ Phụng cũng chẳng ngờ đâu. 
Có đọc tác phẩm của ông - nhất là phóng sự mới thấy vì sao lại có danh xưng "ông vua phóng sự đất Bắc". Cả 1 thời kỳ nhiễu nhương, điên đảo, đầy rẫy bất công với biết bao số phận người được coi là "dưới đáy của xã hội", với những hạng người "rửng mỡ" , "lai căng"... được ông tái hiện, miêu tả chân thực, sống động với ngôn ngữ giản dị, không trau chuốt mà thấm đẫm tư tưởng nhân văn với tinh thần phê phán, châm biếm đến xót xa. Vậy đấy. Có phê phán, có châm biếm nhưng lại có cả xót xa. Những con chữ ấy, tư tưởng ấy ắt hẳn sẽ vẫn còn khiến nhiều thế hệ độc giả phải thán phục...
5
982126
2015-11-21 14:12:11
--------------------------
305072
2849
Mình biết đến Vũ Trọng Phụng khi học phổ thông với tác phẩm Số đỏ. Mình đã rất thích và vô cùng ấn tượng với giọng văn trào phúng và châm biếm một cách sâu sắc. Qua tìm hiểu biết rằng Vũ Trọng Phụng không những viết tiểu thuyết hiện thực rất đặc sắc mà còn là ông vua phóng sự. Quyết định mua cuốn sách này để tìm hiểu thêm về những tác phẩm khác của ông! Cuốn sách rất dày, với hình ảnh trên bìa khá ấn tượng. Giấy vàng đọc không bị chói, mỏi mắt, cũng rất thích. Sách này xứng đáng 5 sao!
5
724772
2015-09-16 20:26:26
--------------------------
278655
2849
Vũ Trọng Phung được biết đến là "Ông vua phóng sự đất Bắc". Ngay từ thời học phổ thông, tôi đã được tiếp xúc với tác phẩm Số đỏ của ông, từ đó, rất thích phong cách viết của ông. Lâu này, có nhiều nguồn tác phẩm của ông đã được xuất bản, tuy nhiên phần lớn còn mang tính rải rác nên tôi chưa có dịp tìm đọc của ông được. Thật may mắn khi tôi tìm được quyển sách này, khi nó tuyển tập hầu như đầy đủ tác phẩm của ông. Đó là những tác phẩm phản ánh cuộc sống sinh hoạt, từng góc nhìn của tác giả trong những năm thế kỷ 20, nó vừa châm biến sâu sắc, nhưng cũng không kém phần nhân văn. Cá nhân tôi đánh giá đây là 1 quyển sách rất hay dành cho những người trẻ muốn tìm hiểu lại cuộc sống nước ta trước, từ đó có thể rèn luyện bản thân tốt hơn.
5
315066
2015-08-26 00:57:17
--------------------------
246214
2849
Sách là tuyển tập hầu như đầy đủ các tác phẩm của Vũ Trọng Phụng, ví dụ như toàn bộ tác phẩm Số Đỏ, Giông Tố đều được in đủ, duy có các vở kịch thì chỉ đăng trích đoạn (khá đáng tiếc, vì mình cũng rất thích đọc kịch).Tuy vậy vẫn thiếu 1 số tác phẩm nổi tiếng khác của ông, ví dụ như Làm Đĩ. Sách in cực đẹp, chữ rõ ràng trên giấy Nhật vàng nhẹ nên đọc không hề bị chói mắt, bìa sách gợi nhắc chút thơ mộng của Hà Thành. Mình mua gói bọc sách nên còn được bọc nữa. 

5
162924
2015-07-29 17:45:16
--------------------------
