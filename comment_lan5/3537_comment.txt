333114
3537
Pháp tái chiếm Đông Dương là cuốn sách rất có giá trị về mặt tri thức viết về thời kỳ chống Pháp lần 2 của quân và dân ta. Không chỉ đơn thuần liệt kê những trận đánh mà các tác giả còn muốn chứng minh rằng nguyên nhân của sự tái chiếm chính là do bọn phản động thuộc địa của Pháp gây ra mà tâm điểm là sự xỏa trá của D'argenlieu khi đã thông báo sai lệch tình hình ở Đông Dương về Pháp trong khi Pháp đang muốn tìm một biện pháp hòa bình ở Đông Dương để tập trung giải quyết những vấn đề trong nước.
Không chỉ vậy, cuốn sách còn chỉ rõ tác động của "chiến tranh lạnh" đã từng bước ảnh hưởng tới cuộc chiến từ việc tái chiếm chuyển dần sang cuộc chiến tranh ngăn chặn làn sóng cộng sản tràn vào Đông Nam Á do Mỹ là kẻ đứng sau thao túng, viện trợ cho Pháp và từng bước buộc Pháp lệ thuộc vào Mỹ.
5
405107
2015-11-07 13:44:21
--------------------------
