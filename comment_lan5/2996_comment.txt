216412
2996
Nếu các bạn thích nghiên cứu, tìm hiểu về lịch sử đặc biệt là lịch sử nước nhà thì quyển sách này sẽ rất cần thiết có trong tủ sách của bạn. Sở dĩ như vậy là do cuốn sách này cũng cấp một cái nhìn gần gũi, chân thực, với nguồn dữ liệu phong phú và cách viết giản dị, văn phong mộc mạc và không hề mang tính hàn lâm, mặc dù nội dung chứa đựng lại có vẻ hàn lâm. Thế đấy. Vì ít nhiều lý do, giai đoạn Duy Tân này ít thể hiện trong chính sử, hay như sách giáo khoa, nhưng nếu như có cuốn sách này thì giai đoạn đó gần như được lấp đầy. Sách in đẹp, bìa bền, màu giấy vàng dễ đọc, trình bày đẹp.
5
564333
2015-06-27 18:08:15
--------------------------
