457128
5855
Đây là tập thứ ba của series ăn khách Suzumiya Haruhi. Và đáng ngạc nhiên rằng, tuy là tập 3 nhưng lại kể về rất nhiều câu truyện bên lề chưa từng được kể trong suốt nửa năm hoạt động của đoàn SOS trước khi lễ hội văn hóa toàn trường diễn ra. Bạn có thể sẽ thắc mắc liệu mình có xếp sai tập của cuốn sách không, nhưng không đâu, chính sự bất thường ẩn trong nhịp sống bình thường này mới làm nên Haruhi, Kyon và nhóm SOS bất thường gồm đủ loại thành phần từ siêu năng lực gia tới người du hành thời gian như vậy đấy.
5
1021968
2016-06-24 01:03:16
--------------------------
457066
5855
Mình chỉ muốn khóc hết nước mắt vì số tiền mình đã bỏ ra khi mua nó, biết vậy để tiền lại mua những quyển khác mà hiện giờ mình muốn mua còn tốt hơn, mình thích light novel nhật nhưng không có nghĩ quyển light novel này là hay. Có vẻ như các light novel chuyển thể từ anime đều không nắm bắt được cách kể cho các nhân vật, nó cứ có một cái logic là những màn đối thoại, những suy nghĩ  của main nam mà không thể hiểu rõ hơn về các nhân vật khác. Không có yếu tố hấp dẫn.
3
936775
2016-06-23 23:22:10
--------------------------
444745
5855
Tập này là tập hợp gồm 4 câu chuyện rất vui,ngôn ngữ dể thương làm bản thân nhiều khi bật cười,như bao tập khác tập này nói lên nét riêng cá tính của haruhi.Câu chuyện thi đấu bóng chày như bao lần đều được yuki-người ngoài hành tinh sửa đổi dữ liệu quả bóng cây gậy để dẩn đến chiến thắng làm nguôi ngoai sự buồn chán của haruhi.Câu chuyện thứ 2 về miruku một cô gái đến từ tương lai mà hỏi cái gì củng là thông tin mật,lối viết của tác giả lại miêu ta rất hay làm cho bản thân cảm nhận được sự vui vẻ.Câu chuyện thứ 3 thì về một biểu tượng của đoàn SOS,một việc đơn giản nhưng xây dựng lên một cách cuốn hút.Còn câu chuyện thứ 4 theo cảm nhận của riêng em thì nó đơn giản thiếu đi sự vui nhộn.
4
826663
2016-06-08 22:09:10
--------------------------
437983
5855
Tập này xảy ra một vụ án khiến câu chuyện trở nên lôi cuốn hẳn. Tác giả thật sự đã làm cho người đọc bị xuôi theo câu chuyện và không ngừng đọc được. Diễn biễn cũng khá bất ngờ khiến không ai ngờ nổi, điều này làm nổi bật hẳn lên. 
Tập này còn có bookmark nên mình cũng vui hơn hẳn, dù sao có bookmark vẫn thích hơn 
Dù tập này nói về sự chán chường nhưng nó không hề chán chường chút nào, xem anime chỉ vỏn vẹn 2-3 tập nhưng đọc light novel thì nói rõ ràng hơn
Tóm lại hay.
5
612250
2016-05-29 08:19:47
--------------------------
371110
5855
tập 3 này kiểu như là ngoại truyện ý. Nội dung hấp dẫn. Tuy nhiên nếu không chịu đọc kĩ sẽ thấy hơi rối, các khái niệm về thời gian lúc nào cũng làm mình mất rất nhiều thời gian để suy nghĩ. Không suy nghĩ kĩ thì sẽ chả hiểu tại sao lại thế luôn. Đọc hết tập bắt đầu hiểu ra vì sao mà Kyon được Haruhi chọn vào đoàn SOS. Hóa ra chính cậu đã quay về quá khứ và truyền cảm hứng cho cô nàng quái chiêu này. Bộ này càng đọc càng hay, thật tuyệt vì mình đã quyết định mua nó  
5
905465
2016-01-18 23:21:27
--------------------------
335960
5855
    Mình mua cuốn này cùng tập 2,3. Mình chỉ đọc tập 1 của bạn và rất thích truyện này! Mình mua trong đợt khuyến mại nên giá cũng phải chăng. Tuy nhiên đợt đó mình đã bị ''viêm màng túi'' nặng đó nha. Nhưng dù thế nào cũng phải mua đủ các em về thì sợ việc phải đọc ngắt quãng. Truyện thật sự rất hay và không lãng phí số tiền bạn đã bỏ ra đâu! Cứ mua nếu bạn đã để ý đến nó! Truyện thật sự rất hay.
   Mong Tiki sẽ có nhiều đợt khuyến mại như thế này nữa. 
4
768112
2015-11-11 19:21:42
--------------------------
324129
5855
Khi đọc mình nhận ra rằng, tác giả viêt rất hay và lôi cuốn khiến cho người đoc không thể ngừng xem được, nội dung luôn được chuyển biến một cách bất ngờ làm người đọc không thể đoán trước truyện gì sẽ xảy ra tiếp theo mà chỉ có thể đọc tiếp kèm theo đó là các tình huống gây cười và kì bí ko thể thiếu. Quả là một cuốn lightnovel hay, giá mà mình biết tới nó sớm hơn thì hay biết mấy. So với hai tập trước thì tập này có bookmark nên cũng đỡ buồn, truyện này hay mà sao mình thấy hơi bị hiếm và ko nổi so với truyện khác chắc do xuất bản sớm hơn mấy truyện trước                                                                                                                                                      
5
815806
2015-10-20 15:21:33
--------------------------
318662
5855
Mình thấy series Light Novel này hay mà ít người mua quá. Xuất bản lâu rồi mà cũng không bằng các quyển Light Novel mới xuất bản gần đây. Chắc tại nó nhỏ hơn các Light Novel bình thường, với lại mình mua nó không có post card và book mark. Mình yêu thích series cũng chỉ vì nó là Light Novel lại còn đc chuyển thể thành anime nữa. Câu chuyện đời thường thú vị xoay quanh Suzumiya Haruhi. Hy vọng IPM ko bỏ dở bộ này vì mình muốn sưu tập đủ. Các bạn cũng xem anime trước nếu các bạn muốn tìm hiểu rõ bộ Light Novel này.
4
789882
2015-10-06 17:11:31
--------------------------
310420
5855
tập này có vụ án đầy bất ngờ có phần ghê rợn lúc đầu nhưng lúc sau lại có một cái kết đầy bất ngờ,mình đã từng xem anime của tập phim rồi nói đúng hơn haruhi suzumiya là anime thứ hai mà mình coi lúc đầu xem k hiểu thiệt nhưng dần dần truyện càng lúc càng hay khiến mình không thể rời mắt được,tác giả đã biết rất nhiều học thuyết về vũ trụ(dù mình không biết gì cả)rất chi tiết,mạch truyện rất hay không có lỗi sai nào cả,bìa truyện thiết kế rất đẹp,có bookmark kèm theo rất xinh
5
273002
2015-09-19 15:49:03
--------------------------
308457
5855
An phận thủ thường đôi chút vì tập này có bookmark =.=". Nghe cái tựa tưởng chán lắm nhưng hoàn toàn ngược lại hẳn. Tập này có nhiều bất ngờ ập tới. Vụ án mạng dựng lên hoành tráng dữ lắm, nhưng té ra thì... chỉ là để mua vui cho Haruhi mà thôi. Điểm trừ duy nhất là... không đánh số tập nên mình chẳng biết thứ tự để đọc, thành thử ra đọc lưng chừng hơi bị bí. Cách hành văn của Nagaru thiệt là chẳng chê vào đâu được. Hiện mình đã đủ trọn 4 tập rồi, vẫn đang đợi các tập tiếp theo sớm ra mắt để được biết tiếp câu chuyện.
3
534452
2015-09-18 18:22:19
--------------------------
305510
5855
Trong tập 3 này, một vụ án mạng được tạo nên chỉ để mua vui, câu chuyện khá bất ngờ. Đúng là serie về Haruhi có khác.
SỰ CHÁN CHƯỜNG nhưng không hề chán chút nào! Ban đầu đọc có lẽ sẽ hơi khó hiểu một chút nhưng nếu đã từng đọc NỖI BUỒN và TIẾNG THỞ DÀI rồi thì bạn sẽ hiểu thôi. Và lúc đó bạn sẽ nhận ra câu chuyện thật dễ hiểu.
Tanigawa Nagaru thực sự đã rất nhọc công nghiên cứu kỹ về khoa học, quy luật thời gian và không gian cho đến những chi tiết linh tinh nhỏ nhặt nhất. 
Biên tập theo kiểu rất Nhật Bản từ phong cách dàn trang đến kiểu chữ. Chất lượng giấy in thì tuyệt vời, mực in nét khỏi chê vào đâu được.
Chính thức trở thành fan của Tanigawa Nagaru.

3
39173
2015-09-16 23:30:42
--------------------------
291743
5855
Thế giới của Haruhi: Với Kyon đó là thực tuy nhiên với Haruhi đó là cái gì đó mờ ảo mà hấp dẫn kể từ lúc Kyon... xuất hiện^^
Mình đã xem anime trước đó, tuy nhiên theo hiểu biết anime chỉ gói gon khoảng 2-3 tập truyện thôi. Thêm 1 phần movie có thể là 4. Mình chắc chắn là những ai tìm đọc Haruhi tại VN đều đã từng xem Anime của haruhi rồi. Hiện tại light novel của nó đã đến tập 9 nhưng ở VN không hiểu IPM gặp vấn đề gì mà xuất bản nhỏ giọt vậy.
Truyện hay không cần nói! chỉ  một vài nhân vật với những kỹ năng được haruhi ấn định (nhưng không biết) mà tác giả đã khai thác triệt để, từ thời gian không gian đến khái niệm khoa học, tuy khó hiểu mà được mô dê hiểu, tuần tự , logic... rất xứng đáng được giải 
Hi vọng sớm có tập mới ...
5
324739
2015-09-06 17:36:00
--------------------------
287477
5855
Từ Nỗi buồn đến Tiếng thở dài và tập này là Sự chán chường. Có lẽ bị ảnh hưởng bởi hai tập trước nên ở tập này mình lại mong đợi một "kỳ tích" xuất hiện  từ trí tưởng tượng "bay cao bay xa" của Haruhi. Và bùm một vụ án mạng trong phòng kín xảy ra. Vụ án khá giống trong các tiểu thuyết trinh thám : ở căn biệt thự trên một hòn đảo biệt lập, một người bị đâm, một người mất tích..., chỉ khác ở chỗ người bị đâm không chết, án mạng thực ra là một vở kịch được dàn dựng để... mua vui cho Haruhi.   Điểm trừ duy nhất là Asahina ít xuất hiện trong tập này (vì ngất xỉu).
4
503185
2015-09-02 19:46:21
--------------------------
281341
5855
Thật sự thì lần đầu nghe cái tên nhiều bạn sẽ bị nhầm tưởng nếu chưa đọc tập 1 hoặc 2. Sự Chán Chường Của Suzumiya Haruhi. Ủa người ta chán chường thì càng đọc càng chán chứ sao. Tại sao lại để cái sự chán chường ấy lan tỏa trong cộng đồng mọt. Nhưng không. Serie này nói về Haruhi và binh đoàn SOS mà thật sự thì nó không hề chán chút nào. Trái lại nó vừa hài vừa bựa lại thú vị lắm.
Điểm cộng: Chất giấy tốt, bìa trắng làm nổi bật hình minh họa. Font chữ được design rất Nhật. Được tặng 1 PC rất đẹp mà hình như là fanart.
Điểm trừ: AAAA. Không có đâu.


5
195704
2015-08-28 12:46:24
--------------------------
281007
5855
Sau một chút thất vọng ở tập 2 thì tập 3 này đối với mình đã phần nào mang trở lại được sự bí ẩn, cuốn hút và hài hước của tập 1. Đầu tiên về hình thức thì IPM luôn làm rất tốt và mình rất hài lòng với chất lượng bản in. Về nội dung thì tập này bao gồm 4 mẩu truyện ngắn xoay quanh đoàn SOS với những chuyện kì lạ xảy ra quanh họ. Trong 4 câu truyện thì mình cực thích "vè Lá trúc" (kể về chuyến du hành thời gian của Kyon và Mikuru), đứng thứ 2 có lẽ là "hội chúng đảo biệt lập" với một cái kết cực kì bất ngờ. Tóm lại thì "Sự Chán Chường Của Suzumiya Haruhi" theo mình là tương đối tốt, tuy vẫn còn vài hạt sạn nhỏ nhưng nếu bạn không quá khó tính thì truyện thực sự rất thú vị và đáng để đọc.
4
148945
2015-08-28 03:23:15
--------------------------
268118
5855
Dạo này mình khá tin tưởng IPM trong các tác phẩm từ Nhật vì chất lượng giấy, chất lượng dịch của IPM khá tốt. Nên rất hào hứng tiếp tục mua quyển thứ ba trong serie về Haruhi này.
Thấy nhiều bạn nhận xét không tốt về cuốn này. Nhưng với mình nó vẫn vô cùng tuyệt. Bất cứ truyện gì muốn hay đều phải có những đoạn hơi bình thường để khi xảy ra một sự kiện chấn động gì đấy ta mới cảm thấy bất ngờ. Với mình thấy mấy trò nghịch ngợm bí ẩn của Haruhi khá vui mà. Đầu óc nàng ta suy nghĩ những thứ linh tinh nhanh thật.
4
139133
2015-08-16 02:15:16
--------------------------
258475
5855
Sự Chán Chường Của Suzumiya Haruhi vẫn là cốt truyện ấy, vẫn là nhân vật ấy. Có thể "Sự Chán Chường Của Suzumiya Haruhi" mang sự nhàm chán nhưng vẫn lôi kéo người đọc, quả là một thành công lớn.
Khổ 10.5 x 14.8 cm, độ dày 384 trang khá hợp lí, thuận tiện cho việc bảo quản, cất giữ.
Mong tác giả Tanigawa Nagaru cũng như IPM hợp tác thành công với nhau để tiếp tục series light novel này! Thực sự thì bạn đọc rất rất rất ngóng chờ những loạt truyện tiếp theo được ra mắt đấy ~

3
608340
2015-08-08 16:04:29
--------------------------
248282
5855
Haruhi đã rất thành công ở tập 1 và tập 2, nhưng đọc tập 3 lại có phần khiến mình cảm thấy... không hứng khỏi cho lắm. Vẫn những tình tiết hài hước quen thuộc, vẫn là Haruhi ngang tàng bướng bình ( nhưng phần này có phần dè chừng e sợ), vẫn là Kyon với những đoạn suy nghĩ riêng biệt độc đáo. Nhưng cách tác giả dẫn vào là 1 chuyện, và kết thúc có phần không"xứng" cho lắm.|(minh sẽ không nói chi tiết kẻo bạn bị spoil) Tuy nhiên, đó vẫn là 1 quyển sáng tốt, và phần này có thể không hấp dẫn bằng phần sau. 
3
620273
2015-07-31 09:44:07
--------------------------
236677
5855
Công nhận một điều series light novel Suzumiya Haruhi vẫn rất tuyệt, bìa đẹp từ bìa ngoài đến trong, nội dung của Sự Chán Chường Của Suzumiya Haruhi cũng rất hay. Ở tập này lại được tặng bookmark nữa, mình thấy có chút "bựa" ở series Suzumiya Haruhi nên mình cũng rất thích đọc. Haruhi khá "bá đạo" và mong rằng các tập sau của series Suzumiya Haruhi sẽ vẫn mang chút "bựa" và tuyệt vời như tập này và những tập trước để giúp cho người đọc có thể cảm nhận được nội dung của light novel hơn. Tuy hơi khó hiểu nhưng chỉ cần vừa đọc vừa suy nghĩ thì có thể hiểu nhanh hơn!
5
558720
2015-07-21 22:28:29
--------------------------
