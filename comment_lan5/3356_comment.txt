393170
3356
Đầu tiên, cuốn sách được in màu rất đẹp, giấy tốt hẳn hoi và nội dung thì đa dạng, thích hợp cho những bạn muốn ôn luyện Toeic.
Tôi đã mua cuốn này dành tặng cho nhỏ bạn và nhỏ thích mê. Nội dung phù hợp với hiện thực giúp người luyện Toeic dễ dàng ghi nhớ và đọc dễ hiểu hơn. Sách không bị lỗi về chữ hay giấy in. Màu sắc tranh ảnh sinh động không gây nhàm chán, kích thích cho cả hai phần não hoạt động. Bài tập khá ổn về mặt nội dung và sự đa dạng. Đĩa nghe cấu thành ổn và nghe được tốt, file không bị lỗi.
4
95468
2016-03-08 12:35:16
--------------------------
306913
3356
Mình đang ôn thi Toeic lên tiki thấy sách này được giảm giá nên đặt ngay và luôn. Vừa làm hết 4 test của sách này :3. Về nội dung thì sách bám đề thi Toeic hiện nay, các bài test phù hợp cho mình làm quen với đề thi và cấu trúc bài thi Toeic, sách có phần đáp án cụ thể giải thích rõ ràng. Có một điều là sách in giấy ko được tốt, chữ nhỏ và khá mờ. Chất lượng đĩa CD ổn nhưng nên để 4 bài nghe chung 1 đĩa sẽ tiện lợi và đỡ tốn kém hơn. 
4
27628
2015-09-17 21:18:40
--------------------------
281487
3356
Nhân dịp tiki giảm giá 60% đầu sách luyện thi Toeic của Longman, mình đã mua trọn bộ 4 quyển luyện Toeic của Longman. Sau khi luyện xong 3 quyển trước, thì bạn có thể sử dụng quyển More Practice Tests để luyện trước khi vào kỳ thi chính thức. Mức độ của 4 bài này là từ khó đến dễ. Tuy nhiên, hình ảnh trong sách không được rõ, chữ nhỏ và hơi mờ nên việc nhìn là hơi khó khăn khi tập trung lâu. Chất lượng đĩa CD thì tốt, âm thanh nghe rõ. Phần trả lời thì có giải thích rõ ràng. Đây là bộ sách nên mua khi luyện toeic. 
5
356759
2015-08-28 14:10:48
--------------------------
260798
3356
Vừa nhận được quyển này từ tuần trước,khá hài lòng về nội dung  sách.Sách này khá tốt với những bạn có trình độ tiếng anh khá trở lên muốn tự luyện TOEIC,vì quyển sách hoàn toàn bằng tiếng anh.Nội dung gồm 4 bài test hoàn chỉnh 200 câu (100 câu nghe và 100 câu đọc hiểu) ,phần audio script,phần đáp án có giải thích bằng tiếng anh.Sách kèm tận 4 CDs để bạn luyện nghe.Nói chung quyển sách khá ổn ,về phần chất lượng sách có lẽ do đây là bản bìa mềm nên giấy không được cứng cho lắm.Một quyển sách đáng mua cho những ai đang luyện thi TOEIC!
4
310336
2015-08-10 17:46:48
--------------------------
