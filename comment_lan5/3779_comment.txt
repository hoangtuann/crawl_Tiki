255943
3779
Bộ Đề Kiểm Tra 15 Phút, 45 Phút, Học Kì Địa Lí Lớp 9 là cuốn sách tham khảo tốt. 
Sách hay, bìa đẹp, chắc chắn, giá cả hợp lí. Thích hợp với cả việc học của học sinh và việc dạy của giáo viên. 
Đề kiểm tra đa dạng, từ loại hình trắc nghiệm đến tự luận, đặc biệt là có cả các bài kiểm tra kĩ năng Địa lí. 
Tuy nhiên, các câu hỏi tự luận chưa hay lắm, nhiều câu hỏi chỉ xoay quanh hình thức tái hiện kiến thức (biết, hiểu), ít câu hỏi tư duy bậc cao (phân tích, tổng hợp).
4
205314
2015-08-06 15:02:00
--------------------------
