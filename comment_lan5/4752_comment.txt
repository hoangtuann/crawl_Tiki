492467
4752
Cuốn sách rất hay. Nó là cuốn sách đầu tiên làm tôi tức tối, rầu rĩ, suy nghĩ và mỉm cười nhiều đến vậy. Nó như một món quà từ thiên đường 53 năm về trước. 53 năm nhưng tình cảm và chân lí trong nó trường tồn mãi đến bây giờ. Nên mua! And enjoy it!
5
1703937
2016-11-24 21:34:38
--------------------------
489261
4752
Đây là một quyển sách hay. Câu truyện Trà Hoa Nữ là 1 sự chia buồn cho nhân vật chính. Xã hội đã đẩy cô vào tình thế làm 2 gái bán hoa, nhưng tâm hồn cô đâu đó vẫn còn len lỏi sự trong sáng, ngây thơ và yêu đời. 
Về hình thức, mình rất thích cách trang trí bìa, vừa đẹp lại vừa tinh tế, đậm chất cổ điển. Giấy màu trắng ngà, chữ in tốt, rất đáng đồng tiền. Sách lại được bọc bookcare nên mình cũng yên tâm. Đánh giá cho sản phẩm này 4 sao   
4
576561
2016-11-07 10:46:14
--------------------------
488721
4752
Quyển sách khiến mình không thể rời mắt, ngấu nghiến từng trang giấy mới cảm thấy nỗi đau của Marguerite, người phụ nữ đáng thương. Cái kết khi Armand được nhẹ nhõm khiến mình càng day dứt hơn cho nỗi đau Marguerite đã chịu đựng vì anh.
5
586926
2016-11-04 14:00:34
--------------------------
483385
4752
- Đọc cuốn sách này, tôi cảm thấy rất thương cảm với tình yêu của nàng Marguerite. Số phận nàng đầy rẫy gian truân nguy hiểm, như thể nàng bị vòng vây của cuộc sống hào nhoáng mà tối tắm của Paris nuốt chửng. Còn Armand, dường như là người duy nhất còn nghĩ đến nàng khi chết, à không, người duy nhất nghĩ đến nàng kể cả khi nàng sống, người duy nhất coi nàng như một thiếu nữ 20 sắp giã từ cuộc đời.
- Tác phẩm này là một trong những tác phẩm văn học mà tôi yêu thích nhất. Giữa cuộc sống bộn bề vẫn có tình yêu chân thành như vậy.
4
1079953
2016-09-13 20:31:18
--------------------------
476283
4752
Xin mượn những dòng thơ của Pautopxki để giãi bày nỗi lòng của bản thân tôi:
             "Những trang sách suốt đời vẫn nhớ
              Như những đám mây ngũ sắc trên đầu"
Và có lẽ, những đám mây ngũ sắc ấy - những dư vị những cảm xúc những trải nghiệm những luyến tiếc nhớ thương mà Trà hoa nữ đem đến cho tôi ắt hẳn cũng sẽ khiến những ai đã đọc phải thừa nhận rằng đây là một tuyệt tác. Victo Huygo viết "Những người khốn khổ" để phơi trần bộ mặt trái của xã hội, để cất lên những tiếng nói ai oán than cho số phận khốn khổ của nhân vật chính. Còn với tác giả Alexander Dumas lại là tiếng khóc thầm dành cho những cô gái giang hồ, mà ở đây là nàng Marguerite Gautier. Tôi yêu và quý nhân vật chính, tôi thích cách cô hành xử, tôi ghét và giận tên đàn ông ấy, gã tồi chỉ biết ghen tuông và chà đạp lên tình yêu của nàng. Có thể, tình yêu của kĩ nữ với nhiều người mong manh, nhưng trong tác phẩm này, tôi cảm nhận nó mãnh liệt từ trái tim của người phụ nữ ấy, nàng không hề chịu sự nhơ nhớp, bẩn, mà nàng là trong sáng nhất!
5
1520508
2016-07-19 17:06:32
--------------------------
461526
4752
Nối gót Alexandre Dumas cha, ở "Trà hoa nữ", bạn đọc cũng phần nào thấy một chút phong vị của người cha nhưng vẫn rất riêng, rất đặc sắc của phong cách Alexandre Dumas con. Tác phẩm dù viết rất nhiều những mặt tiêu cực của xã hội về người phụ nữ nhưng rốt cuộc sau cùng vẫn thể hiện niềm trân trọng, yêu quý người phụ nữ. Nhân vật người con gái trong tiểu thuyết đẹp như những đóa trà hoa. Điều ấn tượng nhất của tôi đối với phiên bản này là bìa quá đẹp. Chất lượng giấy và chữ in cũng rất tốt.


5
60352
2016-06-27 21:05:50
--------------------------
452801
4752
Khi đọc những chương đầu thì đã biết kết quả nữ chính đã chết, dù đã biết trước kết quả nhưng khi đọc khúc lá thư nhật ký của nàng kĩ nữ Marguerite gửi Armand thật đau bùn làm sao,  có cảm giác như nàng đã quá nhìn rõ bộ mặt của thế gian nên lúc chết nàng đã buôn xuôi tất cả chỉ mong muốn duy nhất là gặp người yêu của mình. Thật xót xa làm sao, mình giá như và hy vọng Armand về kịp thì hay biết mấy. Thật buồn cho số phận người phụ nữ khi sinh ra đã không thể lựa chon thân phận cho chính mình .........:((((((((((((
4
1254819
2016-06-20 20:41:23
--------------------------
438788
4752
Tôi đã đã rơi lệ khi thấy Marguerite hy sinh cho tình yêu của mình. Liệu có đáng cho một người phụ nữ chỉ vì nghề nghiệp của mình mà bị khinh rẻ? Đáng thương thay cho cho một trái tim, yêu bằng chút thuần khiết còn sót lại trong tâm hồn thì lại bị xã hội nhơ nhớp, trụy lạc này nhấn chìm. Than ôi cho nàng Marguerite khi tìm thấy tình yêu và lẽ đời thì nhận ra những điều đó thật phù du. 
Thiên Đàng chắc sẽ không vui khi chỉ đón tiếp những con người ngoan đạo. Thiên Chúa sẽ vui mừng gấp bội khi chào đón những con người lầm lạc trở về!
5
716186
2016-05-30 12:15:06
--------------------------
416267
4752
1 tác phẩm kinh điển khá hay và được dịch giả chọn từ ngữ rất tốt, vẫn giữ nguyên được tên những địa danh hay tên nhân vật chính là một điều đáng trân trọng. Rất thích cách dùng từ của dịch giả vẫn giữ được nét khá cổ điển của tác phẩm. Mặc dù không phải là 1 loại truyện thuộc dòng ưa thích nhưng đã là tuyệt tác thì không hoài phí đọc đi đọc lại vì có thể hình dung chính xác những gì đang diễn ra mỗi khi đọc đến từng đoạn khác nhau. cần lắm những tác phẩm và dịch giả hay như thế này để có thể chuyển tải tốt từ ngữ và lời văn của nguyên tác !
5
1256689
2016-04-14 20:07:26
--------------------------
412340
4752
Nó là một thời quá khứ... là một điểm nhấn giữa hàng trăm cuốn sách tôi từng đọc...
Vẫn là chuyện tình buồn, vẫn là những ngăn cách không cách nào vượt qua, cuối cùng là cái chết chấm dứt hết thảy....
Nhưng vẫn có điểm khác.... nàng đầy kiêu hãnh, chỉ yếu đuối khi bên cạnh chàng, rồi lại mạnh mẽ từ bỏ tất cả...
Trà Hoa Nữ, tựa như Moulin Rouge, cũng phận đời bất hạnh như Xin lỗi, em chỉ là con đ*... 
Gói gọn nhưng đầy xúc tích, cái tình được chắt lọc không thừa không thiếu, để ta nếm trải được hạnh phúc của nàng, rồi lại cùng khóc khi chứng kiến nàng rơi vào bể khổ....

5
60725
2016-04-07 16:05:30
--------------------------
409162
4752
Mình mua cuốn sách này là để làm quà tặng nhân dịp sinh nhật cho một người bạn rất thân. Mình hơi lưỡng lự trước việc mua bản này hay mua bản của Nhã Nam và cuối cùng thì mĩnh cũng đã đưa ra lựa chọn mua bản này vì bìa rất đẹp. Sách rất hay và sâu lắng. Nội dung có trầm có bổng rất cuốn hút mình. Và mình có thể cảm nhận được cái đẹp từ một con người như nhân vật chính trong tác phẩm. Tuy mình không hứng thú lắm với văn học nước ngoài nhưng đây có thể chính là một cuốn sách nước ngoài chinh phục được mình. 
4
1087912
2016-04-01 23:00:31
--------------------------
401401
4752
Xuyên suốt "Trà hoa nữ" là câu chuyện tình buồn và lãng mạn của nàng Marguerite Gautier, một cô kỹ nữ nhan sắc tuyệt trần. Hồng nhan bạc mệnh, chính vì sắc đẹp khó ai so bì đó đã khiến cô phải mang trong mình số mệnh éo le và bạc bẽo. Khoảnh khắc đẹp nhất trong đời cô có lẽ là tình yêu đẹp mà ngắn ngủi với chàng Armand mà thôi. 
Trên thị trường có nhiều bản "Trà hoa nữ" khác nhau, nhưng mình quyết định chọn bản của Đông A, thứ nhất vì đây là bản dịch của Nguyễn Bích Như, cô đọng, đơn giản mà chất chứa nhiều tình cảm, thứ hai về hình thức tác phẩm, bìa rời, giấy mịn, chữ in rõ ràng và có nhiều hình minh họa phù hợp khiến người đọc dễ theo dõi tác phẩm hơn.
5
182482
2016-03-20 16:59:24
--------------------------
399748
4752
Đã từng nghe nhiều người nói về câu chuyện này nhưng không mấy nhiều cảm xúc, cho đến tận ngày được sờ rờ chạm vào cuốn sách! tôi bị lôi cuốn bởi bìa sách được thiết kế ấn tượng và bởi vì một câu chuyện tình buồn mà đẹp vô cùng. Một cô kỹ nữ yêu một loài hoa thanh tao, và rằng thì cô cũng thanh tao như chính loài hoa ấy. Cô với cả một thân phận như vậy đã tìm được tình yêu đích thực của lòng mình...dù câu chuyện đã không kết thúc đẹp. Nhưng ở đó có thể thấy được sự hy sinh cao cả của cô cho người yêu cô! Thật xúc động cho một chuyện tình :3
5
337574
2016-03-18 10:54:43
--------------------------
364061
4752
Mình mua sách vì hấp dẫn bởi bìa quá đẹp và sở thích đọc sách kinh điển. Truyện nhẹ nhàng, dễ đọc, về cuối thấy hấp dẫn hơn. Đọc truyện xong thấy trân trọng tình cảm của nhân vật chính dành cho cô gái nhưng cốt truyện không quá hấp dẫn, ít nhất là so với kỳ vọng của bản thân mình ^^.
Toàn câu chuyện quanh quẩn việc cô gái không thể thoát khỏi thói quen ăn chơi trác táng, tiêu pha lãng phí và xem đây là một trong những nguyên nhân rào cản tình yêu. Điều này khiến mình không thỏa đáng lắm, vì chẳng nhẽ cô không thể hi sinh thói xấu này cho tình yêu được mô tả là chân chính, cao thương, chân thành được hay sao?
3
665201
2016-01-05 13:45:04
--------------------------
359104
4752
Một cuốn sách rất xúc động, càng đọc càng thấy hay. Đọc đoạn đầu mình rất bực mình, vì cô nàng Marguerite rốt cuộc cũng chỉ là cô gái bao sống nhờ vào nhan sắc, quen thói sống xa hoa nhung lụa nhưng lười lao động chấp nhận đổi tình lấy tiền. Nhưng càng đọc càng thấy số phận cay đắng của nàng, trót sa chân vào vũng lầy nên càng không tin vào tình yêu chân thật. Chỉ đến khi nàng nhận ra tình yêu, nàng đã vì tình yêu đó mà hy sinh, hy sinh vì người mình yêu, hy sinh vì danh tiếng gia đình người yêu và vì cả người con gái mà nàng chưa từng gặp mặt, em gái của người yêu. 
4
665229
2015-12-26 15:13:04
--------------------------
339995
4752
Marguerite có thể không phải là một cô gái với thể xác trong sạch. Đúng! Nàng ta - một kỹ nữ giang hồ,nhưng tâm hồn và trái tim nàng tuyệt đối thuần khiết và trong sáng hơn bất cứ tiểu thư hay quý tộc nào. Một con người đã dám yêu hết mình, chạy đến tận cùng của cảm xúc tình yêu ,thậm chí là cái chết chỉ để một lần trong đời yêu ai đó và được ai đó yêu bằng một cách chân thật, không vụ lợi nhất. Cái tình yêu cao thượng và dị thường của một "gái điếm" ấy lẽ ra phải được tôn vinh và ngợi ca thì nó lại phải chịu cảnh giày vò, lăng ô trước định kiến của xã hội đến ngay cả khi chết đi, tình yêu ấy vẫn sống mà chứng minh rằng :" Một nàng kĩ nữ cũng biết yêu thương và đã cao thượng đến mức hy sinh cả hạnh phúc của mình khi có người đã nhắc tới hạnh phúc của một cô gái khác "
5
730038
2015-11-19 00:16:22
--------------------------
320739
4752
Mình chỉ có muốn nói là sách rất rất đẹp và nghệ thuật. Khi nhận được sách cảm thấy như đang cảm một tác phẩm nghệ thuật chính hiệu. Mình rất thích lần xuất bản này của Đông A, sách có kích cỡ rất vừa tay, không quá mỏi khi cầm đọc. Trà Hoa Nữ là một tác phẩm nghệ thuật văn hoa kinh điển rồi, một câu chuyện buồn nhưng hết sức nhân văn và tinh tế. Riêng về nội dung, mình đánh giá bộ truyện này 5 sao, hết sức xứng đáng để cho những ai chưa đọc qua tác phẩm này đọc một lần. 
5
628835
2015-10-12 10:58:12
--------------------------
298438
4752
Lúc mới đặt hàng mình không nghĩ nó tuyệt đến vậy. Cầm sách trên tay mà cảm giác như đang cầm một bức họa quá mãn nhãn. Sách rất vừa vặn, minh họa đẹp, chất lượng giấy ổn, trình bày cân đối. Nói chung phần hình thức mình không bàn nhiều nữa. Về nội dung thì ai cũng biết rồi, mình cũng tham khảo nhiều bản dịch khác nhưng cuối cùng quyết định mua bản dịch của Nguyễn Bích Như, bởi mình đã đọc thử và tin tưởng vào cách làm việc của Đông A từ trước đến nay. Luôn đầu tư và chọn những bản dịch tốt nhất cho tác phẩm. Đã yêu mến văn học cổ điển thì không nên bỏ qua tác phẩm này. Thật tuyệt!
5
474670
2015-09-12 18:37:30
--------------------------
296598
4752
Một tác phẩm kinh điển, hay, đáng để đọc và thưởng thức. Cuộc đời không ai có thể đánh giá ai qua vẻ bề ngoài. cuộc đời đã đưa đẩy con người ta vào vòng nhơ bẩn nhưng cô gái ấy vẫn có một tâm hồn trong trắng, một trái tim lương thiện. cô gái ấy thanh cao hơn hẳn những con người tự cho mình thanh cao, để có quyền dè bỉu, khinh miệt. Nàng như đóa hoa trà thơm ngát hương hoa, cố vùng vẫy để sống mà vẫn không sống được. Kết cục tuy bi thảm nhưng là cái kết hay, cũng không thể còn cách nào khác cho một đóa trà hoa!
5
651235
2015-09-11 10:25:32
--------------------------
288428
4752
Đầu tiên là bị cuốn hút bởi tựa đề và hình vẽ của bìa sách, với mình bìa vẽ rất đẹp và nghệ thuật. Quyển sách này được nhiều nhà xuất bản nhưng mình thấy bản này là đẹp nhất. Về nội dung có lẽ đọc sẽ cảm nhận nhiều hơn - cuộc đời của một người con gái, một kỹ nữ, đáng thương hơn đáng trách. Kết cục khá đau buồn cho cô gái, đọng lại trong lòng mình nhiều cảm xúc khó tả, cũng có lẽ mình vẫn thích các truyện về thân phận của những người phụ nữ. Họ thật mong manh, thật đáng thương, và ở nhân vật chính trong câu chuyện là cả một tâm hồn thanh cao, tinh khiết và rộng lượng.
4
449561
2015-09-03 16:08:16
--------------------------
275905
4752
Câu chuyện về nàng kĩ nữ tài hoa với muôn vàn sự hâm mộ, tâm điểm của những phù hoa nhất đời là đề tài quen thuộc để các nhà văn thể hiện nhân sinh quan về sự vô thường và phù phiếm của cuộc sống từ xưa tới nay. Nhưng Alexander Dumas con có cách của riêng mình để khiến câu chuyện hấp dẫn, "Trà Hoa Nữ" rất thích hợp để rèn luyện cho đôi mắt thấu suốt và một tâm hồn nhạy cảm với nghệ thuật phù hoa, nhằm nhận ra triết lý cho cuộc đời mình. Là một fan trung thành của văn học cổ điển, mình đánh giá cuốn truyện tiêu biểu cho nền văn học lãng mạn Pháp này 5 sao!
3
292321
2015-08-23 13:44:47
--------------------------
273531
4752
Nàng, cho đến lúc chết, vẫn mãi mãi là một đóa hoa trà thanh tao, tinh khiết. Vì nàng, bất kể cuộc sống đã qua thế nào, luôn luôn là một cô gái ko rộng lượng, chưa từng oán trách ai trong suốt cuộc đời mình. Bao quanh nàng là những kẻ ích kỷ, cố tình đẩy nàng vào cuộc sống hoang đàng chỉ để kiếm lợi từ thân xác nàng. Đàn ông kiếm danh tiếng, sự tự mãn qua sắc đẹp của nàng, đàn bà kiếm vật chất, tiền bạc qua những cuộc tình phong lưu của nàng. Còn người đời, dưới con mắt định kiến, lại nhìn nàng như một ả đàn bà tham lam, ích kỷ, hoang đàng. Có ai biết được rằng, nàng luôn sòng phẳng với tất cả những kẻ đó, thậm chí có phần rộng rãi khi cho họ nhiều hơn những gì họ xứng đáng được nhận. Những tưởng nàng sẽ thoát khỏi cuộc sống bị xâu xé bởi những kẻ tham lam đó, bởi cuối cùng cũng có một người yêu nàng bởi chính con nàng, trân trọng nàng như cách người ta phải trân trọng một người con gái trong sáng nhất. Thế nhưng, một lần nữa, nàng bị đẩy xuống vũng bùn bởi sự ích kỷ của một người đàn ông được cho là rộng lượng, bởi sự rộng lượng ngự trị nơi trái tim của người con gái vốn được xem là tham lam. Vì tình yêu, nàng sẵn sàng hy sinh hạnh phúc, thứ vốn dĩ được xem là ảo tưởng với những người con gái như nàng, để đem lại hạnh phúc cho một gia đình, cho một người con gái khác mà nàng chưa từng gặp. 
Trà Hoa Nữ, Trà Hoa Nữ, tôi ko nghĩ mình sẽ đọc một câu chuyện được xem như là kinh điển của những tác phẩm về thân phận của các cô kỹ nữ. Bởi đã có quá nhiều bộ phim về đề tài này, thậm chí còn có một tác phẩm thuần Việt được xem là viên ngọc quý của VHVN mà ai cũng biết đó là truyện Kiều. Tôi đã nghĩ sẽ là những câu chuyện lê thê về những thăng trầm của cô gái. Nhưng tôi đã lầm, bởi những thăng trầm đó được tác giả kể lại rất ngắn gọn để người đọc có thể hiểu hết những hạnh phúc, khổ đau trong mối tình cuối cùng và cũng là duy nhất của cô. 
4
288628
2015-08-21 08:31:30
--------------------------
272383
4752
 Kết cuộc đau buồn của nàng Marguerite đã được thể hiện ngay từ những trang đầu tiên, tuy vậy vẫn thu hút người ta muốn biết về cuộc đời của cô kỹ nữ này. Dù chỉ là một cô gái giang hồ nhưng qua từng lời dẫn chuyện, ta từng chút nhìn ra được ẩn sau tấm thân nhơ nhuốc đó, lối sống phóng túng đó là một tâm hồn cao thượng, thanh tao như chính loài hoa trà mà cô yêu thích vậy. Tình yêu với Armand đã làm cô thay đổi  và rồi sẵn sàng vì tình yêu ấy mà hy sinh. Một chuyện tình buồn và thấm đẫm nước mắt. Trong thời đại đó, những định kiến của xã hội là chẳng dễ gì xóa bỏ. Và cũng như tác giả nói, "nếu đó là một chuyện phổ biến, ta đã chẳng nhọc công viết lại làm gì"
5
623765
2015-08-19 23:46:55
--------------------------
252401
4752
Trà Hoa Nữ là 1 trong những tác phẩm hay nhất của Dumas, lần đầu tiên tôi biết đến nó là qua vài dòng miêu tả trong series của Lucky Luke mà tôi xem hồi bé, nên tôi rất tò mò và mua quyển sách về, thực sự không thể thất vọng, rất hay! Truyện vẫn đang khai thác đề tài tình yêu và xã hội, song, vẫn chưa thể có lối thoát, như có lẽ chính Dumas, tác giả của bộ truyện cũng ko thể thoát khỏi cái định kiến, định luật xã hội bây giờ để tìm về một cõi hạnh phúc thần tiên mà các fan có thể mong chờ, nhưng có lẽ, đấy là tất yếu, dù yêu cách mấy, si tình cách mấy, vẫn bị vùi dập cho đến cuối đời, cho đến khi thân xác kiệt quệ và khô héo, dù cho tình yêu đó vẫn trường tồn, vĩnh hằng, nhưng nếu mọi cố gắng chỉ để đổi lại là chua chát, thì có khi, tình yêu này không thể cứu rỗi được điều gì ngoài hành hạ...Nhưng có thể như con người ta có câu nói, thà 1 phút huy hoàng mà chợt tắt, còn hơn le lói suốt trăm năm... 
Còn về tiết tấu câu chuyện thì không thể sai được, đúng là lối văn tỉ mỉ, cách dùng từ câu nệ và diễn biến tương đối đều của cốt văn cổ điển, nếu bạn đọc là người mang tư tưởng thoáng đãng, rất dễ bị xa cách với tác phẩm, bởi nó không thể tách ly được với tư tưởng con người bé nhỏ không thể thắng được xã hội cay nghiệt bấy giờ, nhưng còn đối với người mê say nét đẹp người phụ nữ trong văn học cổ điển, đây luôn là báu vật đáng để sưu tầm.
4
9289
2015-08-03 17:06:38
--------------------------
234118
4752
Mình đã khóc khi đọc hết quyển truyện này vì mình quá đau thương cho cuộc đời người con gái đó.Dù biết cuộc đời lúc thịnh lúc suy nhưng sao với cuộc đời nàng Marguerite mình không thể nào nhận thấy được một chút ánh sáng nào để thoát khỏi quãng đời đen tối và cô độc.Đã bao nhiêu người đàn ông vùi dập thân xác nàng,bao đêm truy hoan làm nàng đau đớn nhưng với nàng tình yêu chỉ có một,dẫu cho cuộc tình đó là cuộc tình đớn đau thì đó cũng được coi là chút an ủi cho cuộc đời nàng.Nhưng  tình yêu mãi không thoát rời định kiến,và cái định kiến ấy cũng mãi chính là sợi dây oan nghiệt thắt chặt nàng.Một câu truyện thật sự rất rất hay,khiến cho mình chìm trong bao xúc cảm.Đọc xong truyện nhưng vẫn tồn lại trong mình những nỗi ngỗn ngang. Truyện hay hình thức bên ngoài của truyện cũng đẹp.Đây quả là một câu truyện đáng đọc.
5
368991
2015-07-20 09:27:09
--------------------------
231218
4752
Trà hoa nữ: Câu chuyện về người kĩ nữ yêu hoa trà - Marguerite - từ một thiếu nữ khuê các trở thành một kĩ nữ nổi tiếng nhất lúc bấy giờ. Bao nhiêu người đàn ông đã đi qua cuộc đời nàng, những kẻ giàu có, ham mê nhan sắc nhưng không thể làm nàng xiêu lòng. Tình yêu của đời nàng là Duval - chàng thanh niên yêu nàng bằng cả trái tim. Tuy nhiên, đến khúc cuối cùng, Marguerite lại chấp nhận từ bỏ, chấp nhận hi sinh vì điều tiếng của xã hội đối với người kĩ nữ đã ngăn cản một tình yêu đẹp, chân thành và để lại nhiều tiếc nuối.
Về hình thức: Bìa sách đẹp, mình thích nhất là ở mỗi chương đều có một vài hình ảnh minh họa, đậm chất cổ điển. Tuy nhiên, cũng như những quyển sách khác, truyện vẫn còn một vài lỗi chính tả không đáng có. Mong là Đông A có thể khắc phục tình trạng này.
5
35746
2015-07-17 22:31:43
--------------------------
231163
4752
cái không hay khi đọc một tác phẩm kinh điển là trước khi cầm cuốn sách lên nội dung đã bị spoil hết trơn hết trọi rồi. Dù chưa đọc nhưng đã biết trước về số phận bi thảm của nàng Trà Hoa nữ, về mối tình tuyệt vọng của nàng, thậm chí tại sao nàng từ bỏ tình yêu của đồi mình. Nên nói chung cuốn sách không hấp dẫn về nội dung gay cấn, tuy nhiên đây vẫn là một câu chuyện tình quá ư thơ mộng và kinh điển, trong truyện dù biết hết về tương lai phía trước nếu ở bên nhau, một kết cục quá hiển nhiên cho sự kết hợp giữa một cô gái phóng túng và một chàng trai nghèo không thể đáp ứng nổi nhu cầu vật chất của cô, nhưng họ vẫn tha thiết bên nhau. Cuối cùng dù hiện thực khắc nghiệt phũ phàng đã chiến thắng khi chia rẽ họ nhưng chỉ riêng can đảm dám yêu của nàng thôi cũng mãi bất hủ rồi.
3
419116
2015-07-17 22:08:49
--------------------------
226525
4752
Cuốn sách này đã thu hút tôi bởi cái tên của nó. Trà hoa nữ, cái tên đầy vẻ nữ tính và trong trắng. Khi đọc giới thiệu ở Tiki và các nhận xét, tôi thực sự vô cùng ngạc nhiên về nội dung của nó và không hề hối hận khi mua quyển sách này. Sách về chuyện tình buồn của cô kĩ nữ Marguerite và anh chàng Duval. Đã có quá nhiều từ ngữ hoa mỹ miêu tả sự tinh tế và ý nghĩa của nó, tôi sẽ không nhắc lại nữa. Điều khiến tôi tâm đắc nhất là tình cảm trân trọng của tác giả dành cho phụ nữ, thể hiện qua từng câu chữ. Ở cái thời mà tác phẩm ra đời thì tình cảm đó thật sự vô cùng xúc động và đáng quý. Quay trở lại với cuốn sách, phải nói rằng lần xuất bản này của Đông A khá thành công. Bìa sách rất đẹp, màu sắc gợi vẻ hoài cổ hợp với nội dung  sách. Sách cũng hầu như không có lỗi chính tả. Tuy rằng một số chỗ dịch còn khá khó hiểu nhưng tôi vẫn rất thích trà hoa nữ.
4
477402
2015-07-12 12:59:13
--------------------------
220599
4752
Mình ấn tượng với tác giả Dumas con đó là " Tôi không dám thoạt nhìn đã khinh khi người đàn bà nào nữa" bởi vì ông tôn trọng phụ nữ, điều mà không phải bất cứ đàn ông xã hội nào cũng có được. Để ý các tác phẩm kinh điển thường rất buồn, những tình yêu thường bị dày xéo đến mức chỉ có cái chết mới thoát khỏi đau đớn ấy. Nhưng không vì thế mà người ta không đón nhận, ngược lại người đọc cảm thông, xót xa cho thân phận, phê phán xã hội đương thời. Những ai đã đọc và yêu thích số phận người phụ nữ trong Tiếng chim hót trong bụi mận gai, Thằng gù nhà thờ Đức Bà hay Những người khốn khổ... thì đừng bỏ lỡ tác phẩm này.
5
328024
2015-07-02 19:32:13
--------------------------
214275
4752
Điều tôi không hài lòng ở quyển sách này chính là lời nói đầu của Nguyễn Kim Hoa. Trước hết phần lời nói đầu đã nói trước hết nội dung cuốn sách. Điều này có thể chấp nhận được,xem như là một bản tóm tắt truyện.Nhưng điều mà tôi không chấp nhận được là ở đoạn cuối "Ta vẫn thấy tác giả lẽ ra phải bước lên một bước quyết định. Trong khi chủ trương nghệ thuật phải phục vụ đời sống con người. Alexandre Dumas con vẫn đứng trên lập trường đạo đức luân lí tiểu thị dân như vậy" trước hết tôi nhấn mạnh đây là một tiểu thuyết dựa trên nguyên mẫu có thực. bản thân tôi thấy tác giả đã hoàn thành xuất sắc nhiệm vụ tái hiện lại cuộc đời của cô gái bao-nhân vật chính của câu truyện. Đây là 1 tác phẩm chừa đựng tính hiện thực và lãng mạn. sau khi đọc xong tôi thấy quyết định của cô gái là 1 sự hy sinh cao cả và không có gì đáng bàn cãi hay chê trách như trong lời nói đầu. Và hơn nữa,việc so sánh truyện kiều và trà hoa nữ là 1 phép so sánh không thõa đáng,bởi đúng là 2 câu truyện về số phận nghiệt ngã của 2 cô gái,nhưng bản thân tôi thấy ngoài việc họ đều là phận bán thân cho đời ra thì chẳng còn nét gì chung. Chứ không như theo cách nói trong lời nói đầu "nhưng do ý nghĩa xã hội và tầm tư tưởng hạn chế như đã nói khiến tôi không có ý định so sánh 2 nhân vật của 2 tác phẩm này"
4
462189
2015-06-24 19:40:47
--------------------------
208414
4752
Ngay từ lúc mở đầu quyển tiểu thuyết này mình đã có một chút linh cảm về số phận của cô kỹ nữ và cảm thấy rất tò mò, muốn tìm hiểu về nhân vật và càng về sau thì hình ảnh về cô càng được phác họa rõ nét và mang theo đó là một số phận nghiệt ngã đáng cho người đời phải nhớ đến. Cô đã để người đọc phải thổn thức và theo dõi câu chuyện về cuộc đời mình một mạch cho đến hết quyển sách mà không thể bỏ xuống giữa chừng được. Vì tình yêu, cô chấp nhận thay đổi bản thân mình, chấp nhận từ bỏ những thứ xa hoa vốn thuộc về mình để sống cạnh Đuyvan nhưng cuộc đời nào có êm ả và luôn thử thách cô. Cô phải chết trong sự cô quạnh và buồn tủi, khiến cho người đọc không khỏi ngậm ngùi. Một tác phẩm kinh điển rất hay.
5
274995
2015-06-15 08:40:51
--------------------------
207202
4752
Chuyện tình buồn của nàng kỹ nữ nổi tiếng và cũng nhiều tai tiếng Marguerite Gautier đã kết thúc theo cách không ai mong muốn. Nhưng vì thế, tình yêu của họ lại đẹp đẽ và không ngừng làm thổn thức người đọc từ bao thế hệ nay.

Những lời hoa mỹ, ca ngợi tác phẩm này đã quá nhiều, có lẽ tôi không cần nói thêm. Tôi chỉ muốn lưu ý các bạn một chút, đó là cuốn sách này của Đông A có một vài lỗi chính tả nhỏ, một vài câu văn hơi khô khan. Nhưng đây là cảm nhận chủ quan của mình thôi.

Sách được Đông A thiết kế bìa ấn tượng, dường như mau màu sắc hoài cổ. Giấy nhẹ và bìa có hai lớp. Những cuốn sách trong tủ sách kinh điển của Đông A thường thu hút người đọc ngay từ cái nhìn đầu tiên như vậy.
4
598102
2015-06-11 17:48:59
--------------------------
205374
4752
Tôi đọc tác phẩm, càng lúc càng bị thu hút. Quả thật, càng về sau, nó càng lôi cuốn người đọc đến những trang tiếp, kế tiếp nữa. Nó khiến cho độc giả không ngừng thổn thức, mong chờ một cái kết tốt đẹp hơn là Marguerite Gautier phải chết trong cô độc và nhớ nhung đến Đuyvan.
Nàng kĩ nữ xinh đẹp, ăn chơi có tiếng này đã bị tấm lòng chân thành và si tình một cách điên cuồng cảm hóa. Nàng dần muốn lui về vùng nông thôn sống một cuộc đời yên bình với chàng. Nhưng cuối cùng, nàng chấp nhận hi sinh để tương lai của Đuyvan sáng sủa hơn. Nàng dã khóc bao nhiêu lần, bao nhiêu giọt nước mắt, nàng đau khổ nhường nào, chúng ta chỉ mường tượng, chứ không thể biết rõ. Vì quá nhiều! 
Câu chuyện tình buồn khép lại bằng cái chết thảm khốc đó, vơi sswj thương tiếc khôn nguôi của nhân vật "tôi" và chàng Đuyvan, khiến người đọc không khỏi chạnh lòng, xót xa.
Tác phẩm đồng thời còn bóc trần bộ mặt của những con người tham lam, ích kỉ, sống lợi dụng vào người khác. Đó là người bạn thân cạnh của sổ nhà Marguerite. Đồng thời, tác phẩm nói về thói quen sống xa hoa của tầng lớp thượng lưu trong xã hội Paris bấy giờ. 
Những gì còn sót lại sau khi đọc đến dòng cuối cùng vẫn là thương tiếc cho mối tình đẹp kia.
4
616731
2015-06-06 16:06:08
--------------------------
197668
4752
Tôi không bao giờ nghĩ mình sẽ thích một cuốn sách với một chủ đề như thế cho tới Trà Hoa Nữ của Alexander Dumas. Thật là một tình yêu đẹp nhưng không kém phần đau lòng. Một chuyện tình buồn khiến ta say mê. Tôi khá thích văn phong của Alexander Dumas nhưng khá buồn là bản dịch chưa chuyển tải hết vẻ đẹp của nó. Cách cốt chuyện được xây dựng, khi những tình tiết được hồi tưởng lại, khiến tôi vô cùng tò mò và muốn được khám phá thêm. Đây quả thật là một trong những cuốn sách nên đọc, tuy nhiên tôi hơi thất vọng về bản dịch.
4
114793
2015-05-18 10:44:47
--------------------------
195762
4752
Vì đã có quá nhiều lời bình và nhận xét hoa mỹ dành cho câu chuyện về nàng kỹ nữ nổi tiếng này nên mình cũng không muốn lặp đi lặp lại nội dung đó nữa. Thay vào đó mình xin nói một chút về bản sách do Đông A ấn hành. Khi ĐA xuất bản quyển này mình đã kỳ vọng tương đối nhiều vì trước giờ ĐA đã có kinh nghiệm dịch thuật và xuất bản các tác phẩm kinh điển tốt, hầu như không có lỗi. Với Trà Hoa Nữ lần này, phải nói là mình thích hình thức của sách, từ bìa cho đến font chữ và chất liệu giấy. Mình thích cảm giác vintage và hoài niệm khi cầm quyển sách trên tay. Tuy nhiên nội dung khiến mình có hơi thất vọng. Trước hết là bài review ở đầu sách (lẽ ra nên được đặt ở cuối sách) không hay, vừa dài dòng kể lể vừa khuôn sáo, lại huỵch toẹt ra hết nội dung câu chuyện thành thử nếu bạn nào chưa từng biết quyển sách này nói về cái gì thì đừng nên đọc. Thứ hai là lỗi chính tả. Mình chưa đọc kĩ hết cả quyển sách nhưng việc bắt được 1 lỗi chính tả với mình cũng khá nhức mắt, vì trước đây mình chưa thấy lỗi chính tả nào trong sách ĐA mà mình từng mua cả. Thứ ba là cách dịch hơi khô khan, thiếu mất sự uyển chuyển mềm mại của câu từ và nó khiến mạch cảm xúc của mình bị gián đoạn, do phần xưng hô của các nhân vât khá lung tung, do cách chọn từ hình như không chính xác, dẫn đến vài đoạn tương đối khó hiểu mà mình phải đọc đi đọc lại vài ba lần mới nắm bắt hết được. Mình vẫn sẽ đọc hết quyển sách nhưng nếu có thể mình hy vọng sẽ tìm được bản dịch tốt hơn cho tác phẩm này.
3
5412
2015-05-14 16:29:04
--------------------------
193637
4752
Chuyện tình yêu giữa 1 kĩ nữ nổi tiếng Paris thời đó và 1 anh chàng dòng dõi quý tộc, có phải chăng chỉ là 1 bên tham danh phú quý, còn 1 bên mê mẫn nhan sắc. Nếu bạn nghĩ vậy thì bạn nên đọc tác phẩm,nó sẽ lấy đi nước mắt của bạn, cũng như đã lấy đi nước mắt của tôi vậy .
Tôi khâm phục tình yêu Marguerite dành cho Duval. Một tình yêu chân thành, sẵn sàng chấp nhận sự dằn vặt, khi nói dối để Duval rời xa mình,tất cả là vì anh .
Tôi ngưỡng mộ sự si tình của Duval với Marguerute , dù cô ấy là 1 kĩ nữ nhưng cô ấy thật hạnh phúc vì được yêu như thế .Anh không chỉ trích, chê bai quá khứ của cô, mà còn chấp nhận đấu tranh với gia đình để được bên cô .
Tôi thích cách viết của Dumas , xứng đáng là 1 tác phẩm kinh điển.
Về cả hình thức bên ngoài, lẫn nội dung bên trong, tác phẩm rất đáng để bạn có 1 cuốn.
5
616860
2015-05-08 11:19:55
--------------------------
187577
4752
Ta có Marguerite Gautier, nàng kĩ nữ danh giá nhất chốn Paris phồn hoa đô hội, nơi những chàng, những anh, những lão bá tước, công tước... hết thảy đều muốn vây quanh nàng, sở hữu nàng và chiếm đoạt nàng. Thoạt trông cuộc sống gái làng chơi của nàng đầy vinh hoa và sung sướng, với những cỗ xe ngựa, áo choàng, khăn lông, túi xách đắt tiền, với những buổi hội hè, nhảy nhót, khiêu vũ, xem hát thâu đêm... Nhưng mà, khi nàng đã một lần trong đời thật sự rơi vào lưới tình của một chàng trai trẻ, cùng với duyên phận ngắn ngủi và số kiếp long đong dường như đã được định sẵn cho nàng từ khi nàng chọn lấy con đường buôn phấn bán hương này, tình yêu đã không mỉm cười với nàng, mãi mãi cho đến khi nàng nhắm mắt xuôi tay...

Ta có Arman Duval, một chàng trai con nhà danh giá, với một tình yêu chân thành và trong trắng, đã dám và sẵn lòng đánh cược với gia đình để sánh bước cùng cô kĩ nữ kiêu kì kia, tạo nên một mối nghiệt duyên với những ghen tuông hờn dỗi thuở ban đầu, những khoảnh khắc hạnh phúc ngắn ngủi về sau, và phải hối hận khôn cùng mãi cho đến tận cuối đời...

Xã hội tư sản đã đẩy số phận con người vào những hố bi kịch, với danh dự cá nhân và phẩm giá dòng tộc chễm chệ đứng hàng đầu, đẩy tình yêu không cùng giai cấp xuống hàng thứ yếu. Ở xã hội ấy, tình yêu của một người con gái không hề được coi trọng chỉ vì thành kiến với cái nghề mà cô ấy đeo mang, cũng như can đảm nắm lấy số phận của chàng trai không qua nổi hai chữ danh dự của một gia đình. Rồi từ đó, bi kịch lớn nhất có thể xảy ra chính là tính mạng và cả cuộc đời khốn khổ sau này của hai người...

Về hình thức, lần này Đông A đã làm rất tốt khâu thiết kế bìa, có cả bìa trong và bìa áo, với hình ảnh những đóa hoa trà rất rực rỡ, khổ sách dễ cầm, dịch mượt. Tuy nhiên, khâu biên tập còn vài thiếu sót, nhưng không đáng kể. Trà hoa nữ nằm trong tủ sách Văn học cổ điển, thế nhưng mình thích font chữ dùng trong hai cuốn ra mắt cùng thời điểm là Hội chợ phù hoa và Ivanhoe hơn, font chữ trong hai quyển này rõ ràng và cân đối hơn. Mong Đông A tiếp tục cho ra mắt những tác phẩm kinh điển còn lại với chất lượng ngày càng tốt hơn, để những người yêu thích dòng văn học kinh điển như mình có thể thưởng thức trọn vẹn.
5
81526
2015-04-23 15:55:12
--------------------------
163305
4752
Cho mình nói về bìa trước đã: Bìa này mình thấy là đẹp nhất trong tất cả ấn bản đã phát hành, mềm, nhẹ cầm thì thấy dễ chịu và thoải mái. Chất liệu giấy thì tốt, giấy nhẹ, bền, sáng sủa, cầm không bị mỏi tay và đọc thì không bị mỏi mắt.
Nội dung nói về cuộc đời người kỹ nữ Marguerite Gautier thích mùi hương và vẻ đẹp của bông hoa trà, tình cờ gặp được chàng luật sư Duval phóng khoáng. Không lâu sau đó họ nở một mối tình đẹp nhưng do địa vị trong xã hội quá khác xa nhau lên họ phải chia tay nhau trong nước mắt và nổi buồn. Câu chuyện còn đề cao phẩm chất còn người cho dù họ là tầng lớp thấp kém nhất của xã hội thời bấy giờ.
5
525063
2015-03-04 17:55:37
--------------------------
160338
4752
Nếu bạn đọc sơ nội dung thì có lẽ bạn sẽ muốn quẳng cuốn sách đi ngay lập tức. Thật thì tôi cũng đã suýt làm thế. Đơn giản tôi nghĩ rằng ai lại đi quan tâm về cái thuở phụ nữ vẫn còn mặc cooc-xê bó đến ngạt thở, đi vòng quanh làm trò tiêu khiển cho đàn ông. Thà xem Jane Austen hay "Cuốn theo chiều gió" thì hơn. Ở đó hình ảnh của những cô gái hiện ra mạnh mẽ hơn, đĩnh đạc và có học thức hơn. Nhưng...tôi đánh liều và ngẫm nghĩ mình đã suýt ngốc như thế nào. 
Giọng văn chính trong cuốn sách chính là giọng kể của tác giả, đóng vai một người thứ ba nghe kể lại câu chuyện về cô gái giang hồ có đời sống thật phức tạp và tình yêu của người đàn ông kia dành cho anh ta. Nó gần như là đề tài xuyên suốt cuốn sách. Và thái độ chân thành đã làm cho tôi, một độc giả tin tưởng hoàn toàn rằng câu chuyện kia là có thật. Và nó cũng làm tôi suy ngẫm. Mọi việc có đơn giản kể về nửa đời ngắn ngủi của cô gái giang hồ mang bệnh nan y cùng tình yêu đến cái chết cũng không giết chết được. Hay nó còn trên cả thế. Danh dự, tình ái, con người, bản chất thối nát của xã hội cuồng quay. Đau lòng và đáng đọc để suy ngẫm có lẽ chính là cụm từ tôi dành cho cuốn sách này.
Hơn ghét tý là những cuộc đối thoại khá nhiều nên phải đọc và nuốt từng từ để ngâm xem nhân vật đang có ý gì, 
Bìa hơn hướng hoài cổ rất hợp với câu chuyện.
4
285978
2015-02-23 21:19:38
--------------------------
160093
4752
Marguerite Gautier là gái làng chơi. Cả cuộc đời của cô chỉ có những đêm bên giường, bên những chàng trai. Rồi khi cô rời làng trở về vùng nông thôn, câu chuyện của cô mới thực sự bắt đầu. Tình yêu, há phải chăng là những phút giây đồng cảm, để rồi khi nhìn vào đó, ta thấy bóng dáng mình phản phất. Marguerite Gautier là kĩ nữ nhưng trên hết, cô cũng là người. Câu chuyện kết thúc trong bi thương nhưng cũng từ đó, ta mới thấy tình yêu thật cao cả. Câu chuyện mang đến cho người đọc những cung bậc cảm xúc rất khác nhau. Không chỉ đề cập đến chuyện giường chiếu, ở Trà Hoa Nữ còn có rất nhiều những khoảng lặng để ta cảm thông cho số kiếp của cô kĩ nữ thích Hoa Trà.
Tôi đọc Trà Hoa Nữ khoảng năm 2010 vì vậy mà thiết kế bìa sách này làm tôi thấy thích thú bởi nó đẹp hơn và không còn mắc phải những lỗi đáng tiếc nữa. Đây là cuốn sách khá hay và ấn tượng, số trang không quá dày, bạn đọc có thể cầm theo mà không cảm thấy bất tiện.
4
185178
2015-02-21 22:21:52
--------------------------
