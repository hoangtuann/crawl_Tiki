465603
5570
Một phương pháp học vẽ rât đơn giản và đễ dàng bằng cách phân chia quá trình vẽ một bức tranh thành 4 giai đoạn, phối hợp với các hình cơ bản ví dụ như hình tròn, hình tam giác... từ đó phác thảo những đường nét hình con mèo, sau đó xóa đi những đường nét của hình khối ban đầu. Nhìn chung, mục đích đạt được là biết vẽ một con vật ở mức căn bản nhất, còn thì vẽ theo cách này sẽ cho ra một chú mèo ít dễ thương và đường nét hơi cứng, thiếu đi sự duyên dáng đáng yêu vốn là đặc điểm riêng của loài mèo mới có.
4
357674
2016-07-01 10:44:08
--------------------------
457744
5570
Theo tôi, sách "Học vẽ phương pháp mới - Em học vẽ mèo" này rất hữu ích không chỉ đối với các bé mà còn dành cho cả những phụ huynh muốn học hỏi để chơi cùng con nữa. Trong sách có khoảng 10 kiểu mèo khác nhau nhìn rất dễ thương, vui nhộn. Mỗi kiểu mèo thì hướng dẫn rất chi tiết cách vẽ, những đường nét kỹ thuật để tạo thành một chú mèo hoàn chỉnh. Tôi cảm thấy phương pháp này rất tuyệt vời, giúp cho bé phát triển tư duy, logic, cách nhận dạng hình khối và cách phác thảo để có được một bức tranh hoàn chỉnh hơn. Cảm ơn tiki đã giao hàng rất nhanh và đóng gói cẩn thận
4
506959
2016-06-24 15:26:22
--------------------------
