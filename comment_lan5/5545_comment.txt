524069
5545
Công tử vô sỉ là cuốn đầu tiên mua hàng ở Tiki. Sách đẹp, mùi thơm, nhất là nội dung rất hay. Hài hước thư giãn, mỗi nhân vật có một tính cách và câu chuyện riêng. Đọc lại bao nhiêu lần vẫn thấy hay. Hâm mộ Duy Hòa Tống Tử 😊😊😊
5
2163043
2017-02-12 21:55:19
--------------------------
492152
5545
Hãy đọc để giải trí với những phút giây cười muốn bể bụng. Hội sách năm ngoái sale giá 20k một quyển tiếc đứt ruột ko mua được
4
540495
2016-11-23 13:29:57
--------------------------
478900
5545
Quá tuyệt vời!!!! Ban đầu mình nghĩ mua truyện về phải đọc từ từ, mỗi ngày chỉ vài chương thôi, nếu cứ đọc một lèo xong luôn như đi thuê, sau đó vứt xó thì phí hoài quá. Thế nhưng khi đọc, mình bị câu chuyện cuốn hút, chương 1, chương 2, rồi cứ thế đọc hết cả tập 1 chỉ trong một buổi tối. Đọc xong đã khuya lắm rồi mà còn chưa thỏa mãn, mình lại lôi tập 2, 3 ra xem các đề mục chương rồi tưởng tượng những tình tiết tiếp theo. Thật sự là một bộ sách rất cuốn hút, tình tiết nhẹ nhàng nhưng tình cảm lại rất cháy bỏng. Tất cả các nhân vật đều vô cùng đáng yêu, từ chính đến phụ, kể cả những nhân vật vô danh cũng rất thú vị. Mình khá thích văn phong của tác giả, không nhiều ngôn từ hoa mỹ, gần gũi dễ hiểu, lại hài hước nữa.
5
1126260
2016-08-05 20:10:58
--------------------------
442161
5545
Mình là con gái nên luôn thích những cau chuyện nhọt ngào, nhẹ nhàng. Hơn nữa thiên tính của mình là song ngư, một co bé hay mơ mộng về những cau chuyện cổ tích, dễ xúc động khi nhìn mưa,... Nhưng cau chuyện thật sự là vô cùng ngọt ngào. Chêm vào những tình tiết hài hước, cả tính cách của các nhân vật nữa. Tất cả như hoà quyện lại và tạo nên "Công tử vô sỉ"
Đây là quyển sách do bạn mình đề cập đến trong vô tình và khuyến khích mình nên mua ẻm nên lúc đầu thì mình cũng chẳng mặn mà gì cho cam. Nhưng khi thật sự đọc, mình bị cuốn hút hoàn toàn, đến nỗi đi đâu cũng mang theo bên người. Hay, thật sự rất hay!
5
622737
2016-06-04 14:35:38
--------------------------
440724
5545
Những tình tiết nhẹ nhàng, pha lẫn chút hồi hộp. Nam chính rất nhẹ nhàng, có cái gì đó đúng là ... vô sỉ khiến người ta phải nghẹn giọng. Nữ chính dù tham tiền nhưng vẫn rất vô tư, luôn coi tính mạng con người là trên hết, cho dù người ta có ý đồ xấu với chính bản thân mình. Nội dung hấp dẫn, lôi cuốn. Mình đang đặt mua 2 quyển còn lại và chờ giao hàng. Hồi hộp, ngóng trông tình tiết tiếp theo là gì...
Ao ước cũng có được 1 công tử luôn cần là xuất hiện như thế...
4
975121
2016-06-02 10:03:29
--------------------------
440263
5545
Không hiểu sao có nhiều bạn nói truyện nhàm , không hay bla bla ,.. chắc tại mỗi người có cách nhìn khác nhau . Riêng tôi thấy truyện rất hay và hài . Nhiều khi nằm đọc cười như điên . Lại còn cực dễ hương nữa . Tính cách nam chính nữ chính tôi đều rất thích . Nam chính kiểu sói đội lốt cừu . Thích giả ngây giả ngu nhưng lúc cần thiết thì lại biến thành con sói thông minh xảo quyệt . Nữ chính thì ..ừm .. đúng kiểu vô lại như trong truyện nói . Cực hèn nhát . Nhưng đó là điểm đáng yêu của nữ chính . Ayyy nói chung là hay hết chỗ nói. 
4
1155208
2016-06-01 14:23:29
--------------------------
416700
5545
Về hình thức mình chỉ nói được 1 từ: xấu. Về nội dung thì hài hước, ổn.   Trước hết là nói về nữ chính Thẩm Tri Ly. Theo như tác giả thì tỷ đây chính là con quỷ hám tiền. Vì tiền nên chị mới phải dây dưa với Tô Trầm Triệt công tử vô sỉ của chúng ta. Nhà anh không có gì ngoài điều kiện, khắp cả nước đều là sản nghiệp của gia tộc, đã vậy thân thế còn hiển hách, võ công vanh danh thiên hạ cộng thêm gương mặt tuấn mỹ rất biết giả vờ. Tập 1 vô cùng hài hước nhưng các tập sau thì nhạt dần. Đọc giải trí thì ổn. 
4
47916
2016-04-15 15:56:31
--------------------------
413435
5545
Phần đầu là nam 9 bị thương rơi xuống núi, được nữ 9 chữa trị. Nam 9 lại mất trí nhớ, xem nữ 9 là nương tử của mình vào lúc tỉnh dậy. Cần sự biểu lộ tâm tình nữ 9 hơn bởi vì trong mọi hoàn cảnh nam 9 cứu nữ 9 đều là nữ 9 suy nghĩ tới ngân lượng. Cảm xúc vẫn còn mong manh, trong cuộc đại hội võ thuật nam 9 bậc nhất luôn tự tìm "dấm chua" mà đánh bại mọi đối thủ. Tác giả có sự sáng tạo trong việc làm đọc giả hình dung ra sự ham tiền của nữ 9, vẻ đẹp và sự thanh thuần của nàng, sự lãnh khốc vô tình từ nam 9 và nỗi sợ mất đi nữ 9 của chàng.
4
1100535
2016-04-09 18:58:49
--------------------------
385622
5545
Một câu chuyện hài hước và vô cùng vui nhộn!
Mình đã được một vài người bạn giới thiệu và mua cuốn sách này! Nó thật sự ko làm mình thất vọng, đây đúng loại nam chính mình thích nhất! Vô sỉ, giả ngây nhưng lại vô cùng thông minh. Hơn nữa đây còn là thể loại truyện sủng mà mình rất thích. Nữ chính mạnh mẽ đúng lúc, yếu đuối đúng lúc, cho ta đọc và hiểu rõ hơn tâm lý người đọc! Câu truyện lại mang đến cho ta rất nhiều điều hài hước, nhưng ko nhàm chán! Không hối hận vì đã mua cuốn sách này
4
552048
2016-02-24 11:59:45
--------------------------
381993
5545
Truyện trình bày tạm ổn, ít sai chính tả. Tác phẩm này nếu nói là hay thì cũng không hẳn. Lúc đầu đọc truyện thấy hài hài sau đó không hiểu sao lại thấy hơi ghét nữ chính. Nam chính mất trí nhớ. Tỉnh dậy yêu luôn nữ chính nên điều này hơi vô lí. Tác phẩm này cũng không ngược tâm đến mức chết đi sống lại mà nó bình bình thế nào á. Truyện này đọc để giải trí thì đc. Có thể nói mình khá thích câu chuyện này. Ghét mỗi cái bìa trông xấu kinh. Hi vọng tập 2 sẽ hay hơn.
3
877398
2016-02-18 13:32:05
--------------------------
376687
5545
Đây là lần thứ hai tôi đọc truyện của Duy Hoà Tống Tử nhưng ấn tượng không hề tệ đi mà còn tốt lên. Thẩm Tri Ly là một người y thuật cao minh, lại còn rất hám tiền. Độ hám tiền của nàng thật sự khiến người khác cười ra nước mắt. Tô Trầm Triệt thì mặt dày vô sỉ. Nhưng sự dịu dàng của hắn thật sự dễ dàng làm người khác rung động. Thực ra thì Thẩm Tri Ly suy nghĩ rất đúng. Kẻ không quen biết khi tỉnh lại một mực nói yêu nàng và muốn bên nàng cả đời, thiệt là khó tin tưởng. Tôi thích tính cách thực tế của nàng, có chút mạnh mẽ, có chút yếu đuối, chỉ thanh tú xinh đẹp song khá hấp dẫn. Chắc hẳn có bí mật ẩn sau tất cả, và tôi thực sự không biết thật tâm Tô Trầm Triệt như thế nào nữa. Nóng lòng muốn đọc tiếp quá đi mất.
5
926764
2016-01-31 15:27:34
--------------------------
365932
5545
đọc tóm tắt truyện thì mình bị liên tưởng đến Thất dạ tuyết của Thương Nguyệt, cùng là nàng cốc chủ tham tiền và chàng thiên hạ top 10 võ lâm, nên không tránh khỏi so sánh giữa 2 tác phẩm, thực tế thì không hề giống nhau. Nếu Thất dạ tuyết là câu chuyện buồn thê lương đầy nuối tiếc thì Công tử vô sỉ là câu truyện cười ra nước mắt. Cười vì cách suy nghĩ và hành động quái quái của các nhân vật, rơi nước mắt vì những tình cảm sâu sắc, những điều tốt đẹp mà các nhân vật làm vì tình yêu. Cách hành văn của Duy Hòa Tống Tử thì mình không thấy hấp dẫn lắm vì hơi cụt, không được mượt mà và hội thoại là chính. Nhiều đoạn mình bị vấp khiến mình thấy diễn biến truyện không trôi chảy gây ra khó hiểu, phải đọc đi đọc lại mới ngẫm ra ý đồ tác giả.
2
414111
2016-01-08 23:46:09
--------------------------
364154
5545
Mình thích đọc truyện cổ trang mà hài hài lắm. Đọc công tử vô sỉ là mê liền luôn k dứt được. Cả nữ chính và nam chính đều được xây dựng rất tuyệt. Vừa mạnh mẽ, thông minh vừa tài giỏi nữa. Đặc biệt truyện có những tình huống hài không đỡ được. Đảm bảo đọc một lần mê luôn. Mình đọc một  tập xog là phải mua liền luôn hai tập tiếp về đọc. Có ba tập nhưng mình thấy không lan man mà nó theo một dòng chảy cứ thế xuyên xuốt không bị rời rạc. Nói chung nên đọc thử truyện này các bạn sẽ cảm nhận được rõ hơn :D
5
615141
2016-01-05 17:14:31
--------------------------
354337
5545
Mình rất thích truyện này, lần đầu được độc câu truyện hài hước như vậy!! Thích tính hi sinh tất cả vì tình yêu của Thập Nhị Dạ công tử -Tô Trầm Triệt, thích tính ham vàng, y đức của Cốc chủ-Thẩm Tri Ly, nhưng hơn cả là tình yêu siêu đẹp, siêu lãng mạng, siêu hài hước, cũng vô cùng éo le của 2 người. Đọc đoạn đầu mà chỉ muốn đọc một mạch đến cuối luôn. Không biết đối với mọi người như thế nào nhưng đây là truyện hay nhất mình từng đọc. Khi mới nhận được truyện, mở ngay nếp gấp bìa trang cuối là thấy ngay lời đối thoại của nhân vật nam chính :" Tri Ly, từ giây phút đầu tiên tỉnh lại nhìn thấy nàng, dường như trong tiềm thức có người mách bảo ta... đây chính là người ta muốn gần gũi, người ta muốn ở bên suốt đời. Cho nên ta phải giữ chặt nàng, đời này kiếp này không bao giờ buông tay" . Đọc xong lúc đó là muốn đọc truyện luôn không chần chừ gì nữa.Giá truyện cũng phải chăng, bìa đẹp, nhìn cả hình họa cũng rất đẹp nữa, bên trong mỗi trang sách được trang trí khá tỉ mỉ, chất giấy cũng tốt có chút sai chính tả. Nhưng rất hài lòng
4
1035404
2015-12-17 18:47:30
--------------------------
346849
5545
Bìa đẹp, tên truyện gây kích thích ghê. Tập 1 rất đáng để đọc, các nhân vật đều vô sỉ, mặt dày nhưng cũng rất dễ thương. Truyện mang yếu tố hài hước, dí dỏm. Tuy nhiên tiếc là 2 tập còn lại không được như mong đợi. Còn tập này rất đáng để mua. Có lẽ đây là một trong số ít truyện miêu tả nam chính mặt dày vô sỉ ngay từ đầu, còn nữ chính gây ấn tượng là một con người có thể vì tiền mà bất chấp tất cả luôn. Nói chung xét về khía cạnh bựa thì đúng là hai con người này sinh ra là dành cho nhau luôn.
4
187006
2015-12-03 14:35:27
--------------------------
299232
5545
Điều đầu tiên thu hút mình mua cuốn sách này là do cái văn án của nó quá hài, bìa truyện cũng đẹp thế là tò mò mua thôi. Truyện có khả năng gây cười cao bởi các nhân vật được xây dựng, một nam chính vô cùng vô sỉ làm cho mình mở rộng hiểu biết chỉ có vô sỉ hơn không có vô sỉ nhất, còn nữ chính thì là một thần y nhìn sao cũng thấy thanh lệ thoát tục nhưng cái tính ham tiền của chị làm người ta té ngửa. Mỗi nhân vật đều có khuyết điểm không có sự hoàn hảo như các câu truyện tiểu thuyết khác nhưng lại làm nó trở nên sinh động chân thật hơn. Mình rất hài lòng mình sẽ trở thành fan trung thành của Duy Hòa Tống Tử, mong ra nhiều truyện của tác giả hơn.
4
338898
2015-09-13 08:10:47
--------------------------
278923
5545
Tác phẩm đầu tiên của Duy Hòa Tống Tử mà tôi đọc là "Em đừng mong chúng ta là người dưng", nên khi đọc tác phẩm này tôi khá bất ngờ! lối hành văn dí dỏm dễ thương vô cùng. Tôi mua truyện do khá ấn tượng với tựa truyện "Công tử vô sỉ" nên thật muốn biết nam chính vô sỉ đến độ nào! Đúng là không thất vọng! :) "Vô Sỉ" đáng yêu chết đi mất! Truyện hài vô cùng, đọc mà cứ mỉm cười một mình suốt! Nữ chính là thần y tham tiền, lại gặp phải một anh sư huynh đáng sợ. Truyện hài hước nên đọc những đoạn gây cấn nguy hiểm thì vẫn làm tôi bật cười! Dàn nam nữ phụ thì "đủ màu đủ sắc" phát ngôn câu nào là cứ làm tôi phì cười câu đó! Truyện giải trí tốt cho những ngày mưa gió! :)
5
544616
2015-08-26 12:12:16
--------------------------
276786
5545
Mình cực ấn tượng với cái tên truyện, văn án thì đã toát lên tính vô sỉ của nam chính rồi. Dưới ngòi bút của tác giả từng nhân vật hiện ra một cách rất hài hước. Vì vậy mà mình đọc liền một mạch cuốn sáh luôn mới cam tâm buông xuống được. Đọc truyện mà cười lăng ra khi miêu tả "thuộc hạ của công tử nào đó". Nhưng ấn tượng nhất, và thích nhất là nữ chính Thẩm Tri Ly, nhất là tính hám tiền của chị dường như thành thương hiệu rồi, bên cạnh đó cũng hâm mộ y thuật của chị lắm luôn. Nam chính thì mặt dày vô sỉ khòi nói rồi. Nói tóm lại là hài, giải trí tốt.
4
472538
2015-08-24 13:00:26
--------------------------
259408
5545
Thật đúng là nội dung y như tên truyện, hài hước không thể tả. Nếu như Tô Trầm Triệt là một công tử mặt dày, phong lưu áp dụng triệt để phương châm "đẹp trai không bằng chai mặt" thì Thẩm Tri Ly lại là một thần y cốc chủ tham tiền như mạng, vắt cổ chày ra nước. Hai con người không hề có một điểm chung nhưng cuối cùng duyên số lại đưa đẩy họ gặp nhau để cuối cùng xảy ra biết bao câu chuyện dở khóc dở cười. Nói chung đây là một câu chuyện hài hước, nhưng liệu hai nhân vật chính có thực sự vô tư như vậy không, mình vẫn đang đợi tập 2 để tìm câu trả lời.
4
359192
2015-08-09 14:02:51
--------------------------
248144
5545
Mình khá ấn tượng với tên truyện nhưng không thích bìa truyện một chút nào, vì nam chính (?) ở trên bìa sách trông có vẻ rất... ẻo lả. Về nội dung, mấy chương đầu truyện khá hài hước, cảm giác nhân vật nữ chính rất giống với nhân vật Kim Kiền trong truyện Đến Khai Phong phủ làm nhân viên công vụ - không từ bất cứ thủ đoạn nào để kiếm tiền. Nam chính mặt dày đúng như tên truyện, giả ngu để theo đuổi nữ chính. Truyện đáng yêu, đọc giải trí rất tốt, tác giả biết cách xây dựng tình huống truyện để lôi kéo người đọc. 
4
10907
2015-07-30 17:58:16
--------------------------
247451
5545
Là tác phẩm với lối viết hài hước, nhiều cuộc đối thoại cười chảy nước mắt. Nội dung khá ổn tạo tiếng cười là chủ yếu. Hai con người cùng chung tính cách giống nhau là vô sỉ nhưng so ra Tô trầm triệt mặt dày là siêu cấp. Thẩm Tri Ly ham tiền luôn bắt chẹt người bệnh nhưng vẫn dành tình thương cho những đứa trẻ mồ côi. Tô Trầm Triệt mất trí nhớ từ công tử nho nhã thành tên siêu dính mặt dày, luôn giữ bộ mặt ngây thơ đáng thương để lấy lòng Thẩm Tri Ly. Hành trình chạy trốn của họ cùng nhiều tình tiết buồn cười cũng giúp họ dần gắn bó với nhau.
4
454196
2015-07-30 10:21:19
--------------------------
240455
5545
"Công tử vô sỉ" đúng như tên truyện là anh nam chính quá đỗi vô sỉ,quá đỗi mặt dày,sau khi được chị nữ chính cứu liền yêu chị,bám chị suốt ngày và còn hay bày ra vẻ mặt "vô tội". Chị nữ chính thì tham tiền,yêu tiền,có tiền mới tính đến việc khác,tính cách chị cũng hơi vô sỉ nhưng chị cũng rất tốt bụng. Hai Anh chị này hay có những tình huống dở khóc dở cười khiến mình cười sặc sụa,phải nói là họ rất hợp với nhau luôn! Truyện viết ổn,điểm trừ là do bìa sách không thu hút cho lắm.
4
127341
2015-07-24 16:49:45
--------------------------
213438
5545
Câu chuyện được viết rất dí dỏm hài hước với những tình huống khiến mình không nhịn được cười. Nếu chỉ để đọc giải trí thì bạn không nên bỏ qua quyển truyện này.
Nữ chính Tri Ly nữ cường đúng kiểu mình thích, có khí chất riêng, mỗi tội mê tiền, cái này thì giống mình :))) nhưng vẫn tôt bụng, quan tâm đến người khác.
Nam chinh vô sỉ, mặt dày đeo đuổi nữ chính khắp mọi nơi, cực kì chung tình, nhưng r chàng lại nhớ lại tất cả :3 làm mình cuống cuồng mua tập 2 để xem tiếp.
4
255987
2015-06-23 19:56:49
--------------------------
193060
5545
Đọc truyện này phải nói là cười đau cả bụng, chống chỉ định nếu đang ăn cơm, uống nước. Nhiều tình huống oái oăm khiến mình cười ngặt nghẽo. Hình tượng nhân vật khá là dễ thương, cả nam chính lẫn nữ chính. Nam chính, đúng như tên truyện, anh này khá là "vô sỉ". Anh Tô mặt dày, thích giả ngây thơ để đeo bám nữ chính nhưng sự chung tình của anh ấy khiến mình rất có cảm tình. Nữ chính Tri Ly cá tính, mê tiền nhưng không bình hoa, không thánh nữ, rất thích cá tính chị ấy. Truyện đáng yêu, đọc xả stress rất tốt.
4
393748
2015-05-06 13:33:14
--------------------------
187053
5545
Về cuốn sách này nội dung thì không có gì để chê nhưng mình lại không thích hình thức một chút nào , bìa màu quá tối nhìn cứ y như sách cũ vậy . Chính vì vậy khi mua mình mới phân vân như vậy . 
Nội dung truyện khá hài hước , nam chính rất mặt dày, rất vô sĩ cứ bám riết lấy nữ chính . Nữ chính vô cùng tham tiền nhưng cũng rất tốt bụng . Hai người - một người có tiền - một người ham tiền chẳng phải quá hợp nhau sau ! Chuyện tình của họ vô cùng hài hước nhưng không kém phần tình cảm chút nào hết ♥ Nhìn chung đây là một bộ truyện hay , bởi tôi rất thích nam chính mặt dày vô sỉ .
5
335856
2015-04-22 12:41:54
--------------------------
180104
5545
Mình tìm đọc truyện thứ nhất vì tên tác giả: Duy Hòa Tống Từ. Ngày xưa đã từng đọc Em đừng mong chúng ta là người dưng, thấy cũng được nên khi Công tử vo sỉ xb mình cũng đã tìm đọc. Cứ ngỡ truyện sẽ ngược lắm cơ ai ngờ cũng khá dễ thương. Thấy hầu như mọi người thích nam 9 còn riêng mình thì rất thích chị nữ 9, hám tiền dễ thương tốt bụng nhưng không Thánh nữ. Truyện có rất nhiều éo le từ thế hệ trước nhưng qua tay nam nữ chính ( đặc biệt là nam 9 ) mọi chuyện dù phức tạp đến đâu cũng được hóa giải. Nói chung truyện cũng tạm ổn nhưng mình nghĩ 3 tập thì quá dài, 2 tập thì tốt hơn
4
555204
2015-04-08 17:49:28
--------------------------
160342
5545
Công tử vô sỉ làm người ta yêu thích vì sự ngây thơ giả tạo nhưng lại hết sức thâm tình của Tô Trầm Triệt, lạnh lùng, hám tiền nhưng là để che dấu con người mềm yếu, đã từng bị tổn thương của Thẩm Tri Ly. Tập 1 của truyện khắc họa một tình yêu nhất kiến chung tình của Tô Trầm Triệt dành cho Tri Ly, dù hai người có tính cách đối lập, ở hai thế giới khác nhau. Dù không biết rõ tình cảm đó là thật hay giả dối, ta vẫn bị cuốn vào sự ngọt ngào đó, rồi cũng lo lắng nếu tình cảm đó tan biến như bọt xà phòng khi chàng lấy lại trí nhớ. Thật sự rất muốn đọc tiếp những tập còn lại để biết kết thúc của chuyện tình này.
5
217940
2015-02-23 21:49:41
--------------------------
151516
5545
Yêu chết đi được cái hình bìa tập 1 nhé, không biết có ai giống với mình không chứ thích cái hình anh lắm, không rời mắt được luôn.

Về nội dung trong tập một thì chủ yếu là hài hước, mỗi khi đọc đến mấy câu trả lời của Tô Trầm Triệt, phải nói là cười ngóac miệng đến mang tai luôn, càng đọc càng thấy anh xảo quyệt, đặc biệt rõ ràng cái đoạn anh đánh nhau trên võ đài ấy, nhìn như vô tình mà thực ra là cố tình đánh cho người ta đau điếng cả người nhưng vẫn luôn miệng nói anh là người tốt. Cho nên mình thấy rằng đỉnh cao của bậc thầy diễn viên cũng chưa chắc bằng anh đâu nhé!

Đọc, đọc mấy đoạn anh giả vờ chật vật với Thẩm Tri Ly ấy, cái bộ dáng nhỏ tội nghiệp, chu môi, giận dỗi, thật sự là cứ muốn đập bàn bồm bộp và cười ra tiếng ấy.

Giới thiệu ngắn gọn xúc tích, ai tò mò thì xin mời rinh em nó về.... thích lắm, yêu lắm
5
378156
2015-01-20 00:43:07
--------------------------
140516
5545
Lần đầu tiên tôi đọc một tác phẩm làm bản thân tôi cười nhiều mà khóc cũng nhiều . Truyện cực sủng là một món ăn ngon cho các fan thích thể loại nào . Đọc truyện ấn tượng nhất là câu nói của anh nam chính “Tri Ly, từ giây phút đầu tiên ta tỉnh lại nhìn thấy nàng, dường như  trong tiềm thức có người mách bảo ta...… đây chính là người ta muốn gần gũi, là người mà ta muốn ở bên suốt đời. Cho nên ta phải giữ chặt nàng, kiếp này không bao giờ buông tay”.Tình yêu của nam chính đối với nữ chính là bất ly bất dịch . Có thể hy sinh tất cả cho người mình yêu quả thật là một tình yêu khiến cho tôi ngưỡng mộ.Văn phong của tác giả cũng khá hài hước, trang trước đang trong dòng tư tưởng trầm tư, trang sau đã làm phụt cười hú hét
5
337423
2014-12-11 15:36:52
--------------------------
137470
5545
Truyện cực sủng a~~~ 
Chị main không phải dạng bánh bèo vô dụng mà là nữ cường, lại còn là phú bà nữa (yêu tiền thế cơ mà :v), tính cách mạnh mẽ nhưng tình cảm lại mềm yếu, ôm mối tình si với sư phụ từ nhỏ, một nỗi đau dai dẳng mãi không nguôi. Mình rất cảm động đoạn chị dần cảm động với tình cảm của anh main nhưng lại luôn hoài nghi trong lòng, chỉ 1 câu nói: "Sư phụ, con phải làm sao đây? Những thứ như thế này trước giờ không ai dạy cho con..." làm mình vừa tương vừa tội cho chị...
Anh main thuộc dạng phúc hắc với ng ngoài, nhưng với chị main thì lại vô cùng dịu dàng, anh trên giang hồ rõ là quân tử nhưng khi làm nũng với chị thì cứ rõ 1 bộ dạng thỏ bạch đáng yêu bị chị ức hiếp, mấy cái vẻ mặt oan ức như "tiểu tức phụ bị chồng ruồng bỏ" hay đôi mắt to tròn chớp chớp vô tội (thật ra là muốn đánh trống lảng khi chị main truy cứu tội).... đáng yêu quá đi.
Còn 1 nhân vật nữa mình cũng khá thích là anh sư huynh của chị main, dù chỉ mới đọc tập 1 thôi nhưng cũng lờ mờ nhận thấy thâm tình của anh với chị từ nhỏ nhưng khi lớn lại bị thù hận vùi lấp khiến anh càng đẩy chị ra xa :(
Những nhân vật phụ khác cũng rất dễ thương, nhất là vị cung chủ nuôi cả 1 dàn hậu cung toàn nam sủng cực phẩm (oaoaoao... ước mơ của bao nhiêu fan girl XD), cuộc sống hoang lạc nhưng ng mình yêu cả đời lại không yêu mình, đến cuối cùng vẫn không nhận ra dc tấm chân tình của ng ngay bên cạnh....
Văn phong của tác giả cũng khá hài hước, trang trước đang trong dòng tư tưởng trầm tư, trang sau đã làm phụt cười hú hét :v có đoạn còn: "anh main giả nai đó... giả nai đó đừng tin..."
Tuy nhiên những điểm trừ nhỏ, tuy là truyện về giang hồ cổ đại nhưng những cảnh miêu tả chiêu thức, đánh nhau, mưu kế..v...v... tác giả miêu tả chung chung, không đặc sắc lắm. Nhừng đoạn tác giả tạo nút thắt, nhưng đến tầm nửa đường là có thể đoán ra dc đoạn sau rồi, không có cảm giác hồi hộp "hack não" lắm. Truyện tập trung nhiều vào phần t/c của anh-chị main hơn nên những tình tiết khác miêu tả khá hời hợt, hơi thiếu cao trào, kết thúc khá chóng vánh (đấy là suy nghĩ của mình)
Cuối cùng là, dù sao cũng rất rấtttt chờ mong tập 2 XD
4
30718
2014-11-26 20:21:59
--------------------------
