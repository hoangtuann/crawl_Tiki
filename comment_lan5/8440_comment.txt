351122
8440
Một câu chuyện cổ đại mới mẻ, một nữ tử lỡ vào phủ Quận Vương là trù nương , tình cờ được thế tử để mắt ,nhưng nàng không an phận mà muốn có danh phận ,muốn làm chính thất tôi rất thích tính này của nàng ,biết vươn lên không ngại khó khăn không sợ mọi người nghĩ nàng không an phận thử hỏi ở cổ đại mấy ai nghĩ được như nàng. cuộc sống là phải như vậy .
Câu văn mượt mà trôi chảy, hài hước, không gây nhàm chán, ,hình bìa khá đẹp nhưng mà nhìn khá buồn.
4
313953
2015-12-11 19:08:32
--------------------------
291606
8440
Dù sao thì từ tập một mình đã thích hai anh chị này rồi. Vệ Minh đã dành trọn tình yêu của mình cho Thanh Thu, một câu truyện tình yêu nhẹ nhàng và ngọt ngào. Dù có nhiều người không thích truyện này cho lắm nhưng mình lại thấy đây là một cuốn truyện khá thú vị. Vệ Minh yêu Thanh Thu vì tài nghệ nấu ăn, tiếng đàn kinh tâm động phách, còn Thanh Thu lại yêu chàng vì đã sẵn sàng từ bỏ tất cả vì nàng. Không phải là Thanh Thu tham vinh hoa phú quý hay sĩ diện nhưng một con người tài hoa như thế chẳng lẽ lại phải làm thiếp, hy sinh tình yêu của bản thân sao. Truyện khép lại thật nhẹ nhàng với một kết thúc viên mãn. Dù còn nhiều thiếu xót về chi tiết trong câu truyện nhưng hy vọng trong câu truyện tiếp theo của minh tác giả sẽ lên tay và cho ra đời nhiều tác phẩm hay hơn nữa.
4
280592
2015-09-06 14:55:44
--------------------------
204497
8440
Về hình thức cá nhân tôi không đánh giá cao bìa này nhìn chả thu hút ánh nhìn gì cả . Về chất lượng có lẽ là điểm cộng duy nhất cho cuốn sách này giấy in rất tốt , thơm tho , không bị cong mép , hầu như không bị sai lỗi chính tả . Về nội dung từ tập 1 đã thấy vô lý rồi sang tập 2 còn thấy vô lý hơn . Chưa thấy nữ chính nào mà quá quắt như cô này đã là phận gái lỡ tuổi lấy chồng , xuất thân chả lấy gì làm tự hào , còn yêu cầu phải làm chính thấp , không cho phu quân nạp thiếp . Được nam chính để ý là quá vinh hạnh rồi còn bày đặt làm kiêu thấy ớn
1
635463
2015-06-04 09:58:14
--------------------------
178843
8440
Càng đọc càng thấy phản cảm với tư tưởng của nữ chính. Một cô gái quá lứa lỡ thì thời cổ đại mà lại một lòng muốn làm chính thất, không muốn chung chồng, làm thiếp không thèm, trong khi đó lại than vãn sao mình không có ai rước (?!). Sau có đoạn chị ấy nghĩ mình làm thế có quá phận không, nhưng rồi đâu lại vào đấy. Ban đầu còn đỡ, càng về sau càng lằng nhằng, dông dài. Anh nam chính yêu và sủng chị lên mây, mà mình thì không hiểu chị có cái gì để anh yêu và ghen ghê gớm thế. Nội dung của tập 2 tính ra chẳng có gì mà kéo ghê quá.
2
57459
2015-04-05 20:19:55
--------------------------
157045
8440
Một mối tương tư là một câu chuyện có tình tiết nhẹ nhàng. nàng là một cô gái lỡ thì cũng là một đầu bếp có tài vì vị hôn phu chết trận mà đi làm đầu bếp trong phủ quận vương. Tại đây nàng gặp và có mối tình với vị thế tử. Khi tình cảm nảy sinh thì nàng gặp lại vị hôn phu tưởng như là đã chết rồi với thân phận khác là thù địch của thế tử. Truyện nhẹ nhàng không có nhiều tình tiết đặc sắc lắm . Đọc giải trí được nhưng không để lại nhiều dư âm cho người đọc
4
221130
2015-02-07 13:09:44
--------------------------
116698
8440
Thêm một câu chuyện về gái ế. Gái ế ở hiện đại còn có khả năng lấy chồng. Gái ế của ngày xưa là khổ nhất, chịu bao điều tiếng. Nữ chính Thanh Thu là một người như thế. May mắn thay nàng gặp được thế tử Vệ Minh. Xét về tâm lý nhân vật, nam nữ chính được thể hiện khá ổn. Thanh Thu biết thân phận mình, tránh né Vệ Minh, Vệ Minh vì tình yêu càng muốn ở bên Thanh Thu. Tâm lý nhân vật phụ như mẫu thân, Tuyết Chỉ hay Ninh Tư Bình.... cũng rất ổn. Họ có lý do chính đáng: tình yêu. Tuy nhiên, điểm trừ của câu chuyện là dài dòng. Nhiều đoạn miêu tả lê thê đọc phát mệt. Nếu cắt ngắn vá sửa lại phần hành văn thì sẽ tốt hơn rất nhiều.
3
111181
2014-07-10 23:12:19
--------------------------
