449568
4400
Tựa đề của cuốn sách nói lên tất cả. Sách vừa giúp học hỏi được nhiều thứ. Từ lịch sử cổ đại của phương tây , châu âu cho đến thế kỉ công nghê ngày nay. Sau khi đọc vài chương đầu thì mới biết người ta nói " Thương trường như chiến trường "  rất đúng. Cách nhưng " thuyền trưởng " chèo lái 1 con thuyền của họ vượt qua những ngọn sóng lớn và khó khăn đối mặt. Cũng như đang dẫn dắt 1 đội quân của mình phải dành chiến thắng. Rất đáng để đọc nhưng có điều là cuốn sách hơi dài nên làm gọn lại và tạo bề dày để hấp dẫn người đọc :))
5
875584
2016-06-17 17:46:26
--------------------------
400587
4400
Ngày xưa các tướng quân dùng gươm, đao, vũ khí, chiến xa để ra trận thì ngày nay các CEO dùng kế hoạch, biểu đồ, ... để xâm chiếm thị trường. Cả hai đều có điểm chung giống nhau là đều chứa đựng mưu toan, mưu lược, tính toán đường đi nước bước thế nào để chiếm thế thượng phong, hoặc đánh lừa đối thủ. Bởi vì vậy mà người ta nói thương trường như chiến trường, và trong cuộc chiến đó mỗi công ty cũng cần một vị tướng quân tài ba thao lược lèo lái công ty hằng ngày. 
4
465332
2016-03-19 16:20:36
--------------------------
393045
4400
Khi nhận được sách từ Tiki, mình rất hài lòng về cách bảo quản (sách có bọc ni-lông), hình thức sách (giấy đẹp, chữ in thẳng hàng lối, màu mực đều). Giá trị nội dung mới thật dự quan trọng, mình rất hài lòng về cuốn sách và đang nghiền ngẫm nó mỗi ngày. Sách chỉ ra sự tương đương giữa những tướng quân cổ đại với những CEO thời hiện đại, nhưng không đơn thuần chỉ là sự so sánh, qua đó là cả một bầu trời tri thức  về lịch sử, kinh tế, xã hội trong bối cảnh lịch sử trước đây rất lâu và hiện tại. Đối với mình, cứ mỗi dòng chữ lại có những vấn đề cần khám phá và tìm hiểu. Một cuốn sách đáng đọc, những bạn thích tìm hiểu về lịch sử cũng như kinh tế chọn cuốn sách này thì đó là sự lựa chọn thật sự không tồi.
5
927111
2016-03-08 06:05:31
--------------------------
314995
4400
Mới giới thiệu mà đã thấy hay.CEO hay nhà lãnh đạo vào Tướng Quân họ luôn có những tố chất,tài năng mà bản thân họ sinh ra đã có hoặc phải trải qua khố luyện trong một thời gian dài để trở thành người dấn dắt,chỉ đạo tuyệt vời.Có người sinh ra đã thông minh hơn người,tài năng vượt trội.Nhưng để trở thành người đứng đầu của 1 quân đội,tổ chức thì yêu cầu họ phải học hỏi,nỗ lực rất nhiều có cả máu và nước mắt...Đọc tiểu sử của bạn sẽ thấy đa số trong số họ là tự lập từ nhỏ một yếu tố then chốt để thành công.
5
251005
2015-09-27 21:48:33
--------------------------
296737
4400
Cuốn sách thật sự hấp dẫn và bổ ích. Vừa giúp người đọc tìm hiểu thêm về lịch sử các triều đại và nền văn minh thời cổ đại vừa giúp nhận ra được các nét tương đồng giữa các phẩm chất, tính cách, những thành công và thất bại của cái nhà Lãnh đạo, tướng quân thời cổ đại với các CEO điều hành doanh nghiệp hiện đại ngày nay. Từ đó rút ra được nhiều bài học kinh nghiệm bổ ích cho chính bản thân mình trong việc điều hành doanh nghiệp trong thời buổi hiện nay. Đặc biệt trong các vụ M&A, mua bán và sát nhập, bành trướng của 1 số doanh nghiệp lớn đang vào Việt Nam trong thời kỳ hội nhập 
Tuy nhiên cần sửa 1 số lỗi đánh số trang sai nhằm thuận tiện cho người đọc theo dõi sách.
5
786715
2015-09-11 11:26:02
--------------------------
271679
4400
Trước tiên, mình xin nhận xét về mặt hình thức của quyển sách. Mình rất thích loại giấy của NXB Trẻ, là loại giấy vàng, đọc không mỏi mắt như giấy trắng thông thường, ngoài ra nó còn rất nhẹ, quyển sách hơn 300 trang nhưng vẫn có thể cẩm đọc khi nằm. Về phần nội dung, đây là một sự so sánh thú vị giữa những người tướng quân thời cổ đai và CEO thời nay, qua đó ta thấy được các đức tính cần thiết cho việc quản trị, lãnh đạo con người. Đây là cuốn sách rất bổ ích cho những bạn theo con đường kinh tế, những người quản lý, quản trị viên. Thêm vào đó ta còn có được lượng tri thức lịch sử khổng lồ về các đế quốc thời cổ đại.
4
582958
2015-08-19 12:45:56
--------------------------
268639
4400
Trở thành một doanh nhân lèo lái con thuyền doanh nghiệp của mình vượt qua mọi khó khăn đến thành công! Đó là điều mà bất cứ ai trong chúng ta cũng muốn đạt được nhưng mấy ai có thể vượt qua mọi trở ngại khó khăn trọng việc lãnh đạo, quản trị doanh nghiệp để gặt hái được thành công. Tướng Quân Và CEO là cuốn sách có thể giúp cho tất cả chúng ta, những người đã và đang theo đuổi sự nghiệp thành công cho chính bản thân mình. Thông qua cuốn sách bạn sẽ thấy những điểm tương đồng giữa những tướng quân cổ đại và ceo ngày nay. Đây là cuốn sách đáng đọc đối với tôi.Quyển sách này cực kỳ hay và hữu ích  đối với tất cả mọi người mong muốn nâng cao khả năng thuyết phục người khác của mình.
4
562396
2015-08-16 17:42:33
--------------------------
229552
4400
Về chất lượng: trang giấy màu vàng đọc lâu không hại mắt - TỐT, bao bìa mẫu mã cũng khá đẹp, kiểu chữ, trình bày rõ ràng...Về nội dung thì tác giả đã liên kết từ những mưu kế đánh trận của các vị hoàng đế, hay tướng quân nổi tiếng thế giới như Caesar, Hannibal, Alexander, Xenophon,...với những quyết định điều hành của các nhà quản trị của các công ty lớn hiện nay. Ngoài những kiến thức về lĩnh vực kinh tế mà bạn còn mở mang kiến thức về lĩnh vực lịch sử, địa lí thế giới.
4
656164
2015-07-16 21:35:10
--------------------------
203637
4400
Tướng quân và CEO của tổng biên tập tạp chí danh tiếng Forbes và nhà khảo cổ học John Prevas đã tái hiện một cách sống động quá trình thực hiện chức năng lãnh đạo của người xưa và so sánh với các tổng giám đốc, các nhà quản lý của các công ty, tập đoàn lớn. Ngoài việc phân biệt sự khác nhau cũng như giống nhau giữa họ, hai tác giả còn trực tiếp cho chúng ta biết các trường hợp đội lốt nhà quản trị như người chinh phạt, thương nhân,...và cả những kẻ tham lam hèn nhát. Tướng quân và CEO của Steve Forbes & John Prevas có thể mang lại cho chúng ta những khái niệm mới, cách nhìn mới về nhà lãnh đạo và quan trọng hơn cả là chứng minh rằng thế nào mới là một nhà lãnh đạo thực thụ.
5
313837
2015-06-01 21:32:57
--------------------------
203553
4400
1 tướng quân và 1 vị lãnh đạo CEO hiện nay lại có sự giống nhau đến bất ngờ từ con đường thực hiện tham vọng và duy trì tham vọng cũng như thành quả và phát huy tối đa thành quả của mình.Steve Forbes và John Prevas tác giả của cuốn sách đưa ra sự so sánh giữa 6 người tướng giỏi nhất và 6 vị CEO giỏi nhất đều thấy chung ở họ là đức tính khiêm nhường nhẫn nại tột cùng , và họ luôn quan sát 1 cách tỉ mỹ chi tiết nhỏ nhất. và họ luôn cảnh giác trước các vấn đề xảy ra xung quanh. 1 tác phẩm hay đúc kết được rất nhiều kinh nghiệm cho các nhà quản lý , các nhà quản trị nhân lực mà cả các cá nhân để đưa mỗi bản thân đến với thành công và duy trì nó , phát triển nó hơn nữa. tôi yêu cuốn sách này.
5
642439
2015-06-01 17:26:01
--------------------------
177118
4400
Lãnh đạo là một việc không nhiều người làm được hay nói đúng hơn là một công việc rất khó. Người lãnh đạo phải hi sinh rất nhiều, rất rất nhiều lợi ích của bản thân để hết lòng cống hiến vì lợi ích của tập thể. Ngày nay trên thị trường có rất nhiều sách nói về công việc lãnh đạo nhưng với quan điểm của cá nhân tôi thì quyển sách Tướng quân và CEO này là nói về lãnh đạo một cách rõ ràng nhất. Bằng việc so sánh giữa cách lãnh đạo quân đội và đất nước của các vị vua, tướng quân của các đế chế cổ đại Hy Lạp, Ba Tư và La Mã với cách điều hành công ty của các CEO của thời đại ngày nay, Steve Forbes, John Prevas đã mang lại cho chúng ta những di sản lãnh đạo mà trong đó chúng ta có thể rút ra được những bài học lãnh đạo cho riêng mình. Khi đọc tác phẩm này, đầu óc tôi có thể tưởng tượng  được các cuộc hành quân của đế quốc Ba Tư, cuộc đấu tranh tới hơi thở cuối cùng của 300 chiến binh Sparta,... và cả các kho báu đồ sộ của các vị vua ngày xưa. Tất cả là nhờ sự kết hợp tài tình của Steve Forbes - John Prevas. Một tổng biên tập nổi tiếng và một giáo sư lịch sử.
5
387632
2015-04-02 16:57:20
--------------------------
