531953
4110
Sách rất hay và có ích. Chất lượng giấy khỏi chê. Quyển sách đáng đồng tiền. 
5
1373219
2017-02-26 15:21:11
--------------------------
462632
4110
Đúng như tựa đề quyển sách, nó giải thích các thì, các cấu trúc tiếng anh một cách rõ ràng, rành mạch nhưng cũng rất dễ hiểu giúp người đọc dễ dàng nắm được các cấu trúc ngữ pháp này. Quyển sách còn đưa ra những ví dụ minh họa phía sau còn có đáp án. Về phần hình thức thì càng làm người đọc hài lòng hơn quyển sách khổ lớn, được in màu chất lượng cực kì tốt. Đây thật sự là một quyển sách ngữ pháp bổ ích cho những mong muốn cải thiện kiến thức ngữ pháp của mình.
4
958015
2016-06-28 19:30:01
--------------------------
452502
4110
Ngày xưa mình không thích học tiếng Anh, vì cảm thấy ngán với mớ từ vựng, ngữ pháp nên cứ học đâu quên đó. Nhưng xét lại thời điểm hiện nay thì tiếng Anh vô cùng quan trọng trong cuộc sống nên cũng dần tập làm quen với nó. Mình cũng sưu tầm nhiều cuốn ngữ pháp, sách có, file có nhưng thực sự chưa làm mình thỏa mãn cho đến khi bắt gặp cuốn này thì nó đã chinh phục mình từ những trang đầu tiên. Tác giả viết và giải thích rất chi tiết những trọng điểm ngữ pháp, lối viết dễ hiểu và mình cảm nhận như là đọc đâu nhớ đó. Sau phần lí thuyết còn có bài tập để củng cố (có đáp án). Thật sự rất hài lòng.
Sách rất dày và in màu. Cách trình bày cũng rất đẹp.
5
527557
2016-06-20 17:37:00
--------------------------
411398
4110
Đợt hội sách vừa qua vì sắm quá nhiều nên mình còn đắn đo chưa mang em này về nhưng mà sau đó được mấy cô bạn rủ rỉ hãy mang em ấy về ngay, thế là đặt liền luôn hehe.. Sách vừa đến tay cái là đã thấy thích thú rồi, sách dày mà giấy rất chất lượng luôn nhé, cực thích ạ. Nội dung thì đầy đủ, rõ ràng mà cách trình bày cũng dễ hiểu nữa. Chắc chắn phải vote cho e ấy 5 sao luôn.
Với lại tiki giao hàng cũng nhanh nữa. Cám ơn tiki!!!!
5
107742
2016-04-05 22:54:17
--------------------------
396815
4110
Mình mua sách này chủ yếu là vì hình thức rất đẹp (bìa đẹp, dày, giấy của sách tốt, ...). Nhưng mình thật sự rất vui khi chất lượng của sản phẩm cũng tương tự như thế. Tất cả các dạng ngữ pháp tiếng Anh từ cơ bản đến nâng cao về các thì, các mẫu câu, dạng động từ, ... đều được tóm tắt, ghi chép đầy đủ trong sách. Sau mỗi dạng là bài tập ví dụ, nâng cao để rèn luyện thêm. Vì giấy và bìa tốt nên cầm quyển sách rất có hứng học và làm bài, chứ không như những sách bài tập anh khác. Tuy nhiên khi giao hàng đã làm gập một góc bìa khiến mình khá bực. Mong tiki sẽ khắc phục điều này để không có lần sau như thế nữa
4
782711
2016-03-14 08:28:09
--------------------------
359889
4110
Quyển sách này hầu như giải thích rất chi tiết những điểm căn bản của ngữ pháp trong tiếng Anh. Đọc những nội dung hướng dẫn cùng với ví dụ đi kèm, mình hiểu rằng tác giả đã rất tâm huyết để làm sao cho người học tiếng Anh hiểu và tiếp thu một cách nhanh nhất, dễ hiểu nhất. Bên cạnh đó, quyển sách còn trình bày trên loại giấy tốt, khuôn lớn với hai màu đặc trưng là xanh biển và đen, bôi đen và gạch chân những điểm cần chú ý nữa. Tất cả những điều này đã tạo nên một hứng thú rất lớn cho người học.Mình xin cảm ơn tác giả quyển sách, cảm ơn Windy.
5
274995
2015-12-28 10:29:47
--------------------------
344278
4110
Ban đầu nhìn cuốn sách dày cộp mình khá là ngại nhưng khi giở ra mình bị hình thức đẹp của cuốn sách hút hồn. Sách trình bày cẩn thận, rõ ràng, màu xanh rất dễ chịu, không gây nhàm chán kiểu giấy trắng mực đen thẳng tuột từ trên xuống dưới. Về nội dung cũng ổn. Sách chia các mục nhỏ, riêng rẽ, dễ hiểu, giải thích cặn kẽ từng vấn đề một, lại có cả bài tập kèm theo để luyện tập luôn. Vừa học ngữ pháp vừa làm bài tập luôn sẽ nhớ lâu, hiểu sâu hơn 
4
744628
2015-11-28 01:12:49
--------------------------
306519
4110
Lại một quyển do McBook phát hành làm con tim mình rung động, sách quá dày, quá bắt mắt, ngữ pháp tiếng anh khá đa dạng, đầy đủ, giải thích quá rõ ràng. Mình mua cho cả 3 chị em nhà mình cùng học. Mua nó vì thấy mọi người chia sẻ cảm nhận quá hấp dẫn và mình đã không hề hối hận về quyết định của mình. Sách rất chất lượng nên giá như thế là vô cùng hợp lí nên mọi người đừng ngần ngại khi mua nhé........Yêu sách quá quá nhiều, nó sẽ trở thành cánh tay đắc lực giúp mình học tốt tiếng anh
5
676151
2015-09-17 17:00:38
--------------------------
294042
4110
Về hình thức thì sách đẹp, màu xanh rất dễ chịu, hk gây khó chịu. Chưa kể không làm người học ngán ngẩm. Chứ nhìn vào mấy quyển photo chữ đen giấy trắng thì ngán cực kì. Hết hứng học. Đề cập đến rất nhiều phần ngữ pháp quan trọng. Giống Mai Lan Hương ấy. Phrasal verb của sách cũng cho những từ thường hay gặp nên tranh thủ tích lũy được kha khá từ mới.Bài tập thì nhiều nên mình vưa học vừa luyện tập để hiểu rõ hơn. Có đáp án dò nữa. Mà chỉ có  mỗi đáp án thì cũng hơi bất tiện. Nhiều khi làm sai với đáp án mà cũng không biết tại sao sai. Không có cái để giải thích. Phải đi hỏi bạn. Nhưng tóm lại là quyển sách này rất hay đó mn nên mua nha. Mấy bạn mất kiến thức hay thi thì nên đọc
5
656157
2015-09-08 22:52:38
--------------------------
273874
4110
Sách này mình mua cho e trai mình học anh văn, thật ra lúc đầu rất ngại mua những quyển tiếng anh mang tính ngữ pháp như thế này vì lo sẽ khó hiểu khô khang, cũng may nhận hàng đọc sơ thì dễ hiểu, cũng có thể dành cho mấy bạn mất căn bản ngữ pháp anh văn mà ngại đi trường học, sách có màu phân chia dễ đọc lắm còn có bài tập để tự kiểm tra bài vừa học đáp án rõ ràng và sách cũng rõ to khủng bố hehe!! Rất hữu ích!
4
728931
2015-08-21 14:19:37
--------------------------
257414
4110
Tôi đánh giá cao cuốn sách này, không chỉ về hình thức mà còn cả về nội dung. Bản thân cuốn sách hội tụ khá đầy đủ kiến thức ngữ pháp tiếng anh, kèm theo các hướng dẫn, ví dụ minh họa gắn gọn, dễ hiểu. Cuốn sách cũng chỉ rất rõ cách dùng các "Thì" sao cho đúng trong từng trường hợp và thích nhất là có bài tập áp dụng cho mỗi chương mỗi chủ đề cụ thể. Cuối cùng mình xin chân thành cảm ơn tiki và nhà xuất bản về sản phẩm này! Cảm ơn rất nhiều
5
635908
2015-08-07 17:18:44
--------------------------
232648
4110
mình đã mua và đã đọc cuốn sách này, nó rất hay , nó có toàn bộ ngữ pháp , bên cạnh đó còn có nhiều ví dụ cho mình rất dễ hiểu... bìa sách lại rất đẹp , với nội dung trong trang giấy trắng và chữ xanh dương có thêm các hình minh họa sẽ tạo thêm cho bạn hứng thú khi học... sách có phần bài tập để mình thực hành và có đáp án cho bạn so sánh, sách trang trí rất đẹp , lại hướng dẫn rất chi tiết ....và dễ hiểu, không phí cho một sách hay như thế này, rất đáng để bạn cho vào tủ sách học tốt tiếng anh của bạn !!!
4
480717
2015-07-18 22:11:14
--------------------------
227284
4110
- Cuốn sách rất hay, nội dung cuốn sách rất rõ ràng và đầy đủ, từng chương tác giả giải thích rất rõ thế nào là Đại từ, Danh từ, Tính từ.v.v..... vả cách dùng chúng như thế nào cho hợp trong một câu nói. Ngoài ra cuốn sách cũng chỉ rất rõ cách dùng các "Thì" sao cho đúng trong từng trường hợp và thích nhất là có bài tập áp dụng cho mỗi chương mỗi chủ đề cụ thể.
- Cuốn sách tập hợp đầy đủ kiến thức ngữ pháp tiếng anh có ví dụ minh họa giúp ngưởi học dễ hiểu dễ nắm bắt và đặc biệt dễ áp dụng cho đúng ngữ pháp.
- Cuốn sách dày tới 615 trang, màu sắc trong cuốn sách đa số màu xanh chữ đen nên nhìn lâu không mỏi mắt, sách do MCBook phát hành đẹp rồi, nhưng cuốn của mình thì có một chút không hài lòng nhưng nội dung cuốn sách quá tuyệt nên không thể không cho 5* được. Thanks tác giả rất nhiều.
5
657350
2015-07-13 21:56:24
--------------------------
212000
4110
Tôi đánh giá cao cuốn sách này, không chỉ về hình thức mà còn cả về nội  dung.
Bản thân cuốn sách hội tụ khá đầy đủ kiến thức ngữ pháp tiếng anh, kèm theo các hướng dẫn, ví dụ minh họa gắn gọn, dễ hiểu. 
Sau mỗi phần thì luôn có bài tập tự luyện nhằm nâng cao hiệu quả và chất lượng học của mỗi cá nhân. 
Bố cục sách rõ ràng, giúp người đọc có cảm hứng cao hơn khi mở cuốn sách ra :)
Tóm lại, với tôi, đây là một cuốn sách tuyệt vời. Giờ đây tôi không còn sợ ngữ pháp tiếng anh nữa rồi. Rất cảm ơn tác giả :)
5
657968
2015-06-21 14:38:24
--------------------------
200520
4110
Trước giờ trên thị trường sách cũng có khá nhiều sách về ngữ pháp tiếng Anh nhưng hình như đều đi theo mẫu hình chung của sách ngữ pháp Mai Lan Hương.Nhưng với quyển sách này thật sự mang 1 phong cách khác,từ nội dung cho đến hình thức đều mang đậm 1 phong cách riêng.
Với hình thức bài tập phong phú, người học dễ nắm được và nắm vững các cấu trúc ngữ pháp của tiếng anh,rất phù hợp cho những ai đang trong quá trình luyện thi các kì thi Tiếng Anh
Còn về chất lượng giấy và hình thức mình không phải bàn vì hầu hết các sách của MC Books đều tốt.
Đây là 1 quyển sách rất hữu ích
5
368991
2015-05-25 11:19:01
--------------------------
