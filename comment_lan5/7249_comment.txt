345070
7249
Quyển truyện Shin - Cậu Bé Bút Chì - Tập 43 ( 2014 ) có 1 tấm bìa thật là dễ thương , đẹp , xinh xắn , ... Mình cứ mãi nhìn tờ bìa mà không không biết chán là gì ! Tấm bìa có 3 người : cu Shin , Himawari , và Bạch Tuyết đang chơi nhạc , nhìn trông rất là hào hứng thú , vui vẻ . Như hình trên tờ bìa , cốt truyện , nội dung câu chuyện đều nói về cu Shin và Himawari , câu chuyện nào cũng hay hết cả , mình đều thích hết và mình thích nhất nhưng câu chuyện nói về Hima !
5
603156
2015-11-29 19:07:40
--------------------------
230798
7249
Mình rất thích đọc shin-cậu bé bút chì, dường như tập nài cũng khá thú vị. Cách trang trí bìa của tập 43 khá vui nhộn, mình rất thích. Có lẽ, trong tập này vui nhất là khi dì Musae dọn về nhà xin ở. Mình cũng khá là thích phần ngoại truyện, mình nghĩ phần ngoại truyện ở tập nào cũng vui cả. Mình còn học được một bài học về tình bạn trong tập này. Himawari cũng quậy như người anh trai của mình, vậy mà tập này khiến mình cảm thấy ngạc nhiên khi cô bá cảm thấy buồn vì mất bạn, hành động đó làm cho mình cảm thấy cô bé khác xa với thường ngày. Tập truyện này đã mang lại cho mình một bài học bổ ích, Shin-cậu bé bút chì mình biết là để giải trí những cũng có lúc mang cho chúng ta những bài học tốt đẹp.
4
77881
2015-07-17 19:05:00
--------------------------
