472992
8441
Thế giới của các vị thần, của huyền thoại, của truyền thuyết luôn hấp dẫn con người chúng ta. Bởi vậy, mình đã quyết định chọn mua cuốn sách “Giải mã huyền thoại – Huyền thoại và truyền thuyết La Mã” của tác giả Jilly Hunt. Cuốn sách mang đến cho mình thêm nhiều kiến thức về những bí mật ẩn chứa trong các huyền thoại và truyền thuyết của người La Mã. Các hình ảnh minh hoạ trong sách khá đẹp mắt, hấp dẫn. Thêm vào đó, cuốn sách khá mỏng nên chúng ta sẽ không mất nhiều thời gian để đọc.
4
470567
2016-07-10 09:55:15
--------------------------
376243
8441
Mỗi nền văn minh lớn đều có những nét văn hoá đặc trưng. Nhưng những nét văn hoá đặc trưng đó từ đâu ra. Bộ sách giả mã huyền thoại về La Mã đã giúp ích rất nhiều cho mình trong việc hiểu thêm về những phong tục và nguồn gốc của các nghi lễ và nhất là hệ thống các vị thần cổ xưa. La Mã tiếp thu rất nhiều từ nền văn hoá Hy Lạp qua các lần dẫn quân chinh phạt. Do đó họ đã tiếp thu và phát triển lên rất nhiều để hình thành nên bản sắc riêng. Đó là lý do vì sao Italia ngày nay lại tập trung giữ được nhiều tác phẩm nghệ thuật cũng như kiến trúc hoành tráng, vĩ đại đến như vậy.
5
93234
2016-01-30 13:36:11
--------------------------
247772
8441
Mình rất thích những câu chuyện huyền thoại và truyền thuyết! lúc đầu mua cuốn sách này mục đích tìm hiểu thêm thôi, chứ cũng không hi vọng gì nhiều mấy! nhưng khi nắm cuốn sách trên tay mình rất là ngạc nhiên, vì: sách đẹp, hình ảnh rõ ràng, giấy dày đẹp! không chỉ hình thức đẹp mà nội dung cũng rất ok: ngắn gọn, chi tiết, dễ  hiểu; những từ ngữ khó hiểu được tác giả giải thích chi tiết ở nhưng trang cuối của cuốn sách, điều này rất tiện lợi cho người đọc! nói chung cuốn sách rất được, chỉ có 1 điểm trừ duy nhất là: bìa sách hơi mỏng nên rất dễ cong lên thôi!
5
291602
2015-07-30 16:45:54
--------------------------
