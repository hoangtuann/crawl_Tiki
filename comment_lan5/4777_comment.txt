473277
4777
Bên cạnh những câu truyện truyền thống về Doreamon và Nobita,chaien,xeko,xuka,dorami....thì mình thật sự yêu thích những câu truyện về 7 người bạn trong đội quân doreamon ! ( Cảm giác nó thích thú thế nào ấy ).Đặt cuốn " Đội quân doraemon ( tập 1)" trên tiki lâu rồi nhưng bây giờ mới ngồi nhận xét được nè.Trước cứ ngỡ không bán quyển này nữa,hóa ra vẫn còn tái bản thế là phải đặt ngay kẻo lỡ ý,ưu điểm đầu tiên là truyện dày cầm rất là thích tay,cách in đọc từ phải sang trái cũng giống kiểu đọc bên Nhật.Nội dung thì không cần bình luận luôn !
5
1419914
2016-07-10 13:47:46
--------------------------
469781
4777
Tôi thấy tập 1 đội quân Doraemon là tập hay nhất. Một tình bạn đẹp đẽ đã được gầy dựng và sức mạnh của tình bạn giữa bảy con mèo máy thật là mãnh liệt.
Bảy con mèo máy là bảy tính cách khác nhau, trầm lặng có, nổi loạn có, không sót một tính cách nào.
Mỗi câu chuyện là mỗi bài học sâu sắc để dạy lại cho thế hệ trẻ thơ của cả thế giới.
Trong truyện, thích nhất Doramed bởi vì chú mèo máy này rất đáng yêu và vô cùng hài hước. Những cử chỉ, hành động thẳng thắn và hồn nhiên khiến tôi bật cười không ngớt.
5
975584
2016-07-06 20:09:30
--------------------------
341447
4777
Đội quân Doraemon lại là những cuộc phiêu lưu mới của chú mèo máy nhưng không phải đồng hành cùng Nobita, Shizuka, Suneo, Jaian mà lần này Doraemon sẽ cùng phiêu lưu với những người bạn mèo máy giống như mình. Điểm hấp dẫn là mỗi thành viên đều không có những sở trường và sở đoản khác nhau để rồi từ đó các thành viên trong nhóm sẽ hỗ trợ, giúp đỡ lẫn nhau. Tập 1 này là sự khởi đầu của nhóm mèo máy "một người vì mọi người, mọi người vì một người". Dù chỉ là truyện tranh dành cho thiếu nhi nhưng bộ truyện này lại có ý nghĩa giáo dục rất cao, rất đáng đọc. 
5
471112
2015-11-22 08:51:32
--------------------------
202692
4777
Với mình thì đọc truyện tranh Doraemon vào những lúc thấy căng thẳng hay mệt mỏi đã trở thành thói quen, thành niềm đam mê. Mình thích đọc các tập Doraemon thêm hơn. Có lẽ bởi ở mỗi tập truyện không tập trung giới thiệu một món bảo bối nào đó của mèo ú dễ thương, mà lại tập trung vào việc đề cao những nét đẹp tuyệt vời của tình bạn chân thành của nhóm mèo máy. Qua tập 1 với những tình huống khôi hài, những phút lo sợ thấp thỏm hay những tràng cười không ngớt, tác giả đã làm toát lên những nét rất riêng trong tính cách của hội mèo béo. Trong đó mình thích Dorawan nhất - điểm tĩnh, đậm phong cách Á đông (vì sống ở Trung quốc) và đặc biệt là thông minh và giỏi võ.
Thật thích.
4
466113
2015-05-30 11:30:58
--------------------------
