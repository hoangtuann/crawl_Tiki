577601
7966
khá hay. Đây thật sự là một cuốn sách " phải đọc " trong nghệ thuật thương thuyết, không những giúp ích mọi người trong việc nâng cao kỹ năng thương lượng
4
5209150
2017-04-19 15:25:44
--------------------------
409390
7966
Ngày nay khi công nghệ thông tin phát triển vũ bão, thì con người dường như bị bao vây bởi các thiết bị công nghệ, các phần mềm máy tính như email, Facebook, twitter, instagram, ... Những tiện ích này ban đầu có vẻ như hữu ích nhưng thực sự càng ngày chúng càng làm mất đi sự tương tác giữa người với người, do đó ta mất đi sự nhạy bén để nắm bắt thâm ý khi thương lượng. Vì vậy cuốn sách này sẽ giúp ta có những kỹ năng để rèn giũa những kỹ năng đó. rất hữu ích.
4
465332
2016-04-02 14:17:20
--------------------------
338408
7966
Đây thật sự là một cuốn sách " phải đọc " trong nghệ thuật thương thuyết, không những giúp ích mọi người trong việc nâng cao kỹ năng thương lượng , từ lúc khai cuộc cho tới kết thúc, mà còn có những tình huống cụ thể, giúp người đọc hình dung rõ hơn, trong những tình huống đó còn có đề cập đến Việt Nam, thật sự khiến cuốn sách trở nên rất dễ đọc và lôi cuốn. Xin cảm ơn NXB Lao Động - Xã hội và Tiki đã giới thiệu cho bạn đọc cuốn sách rất bổ ích này.
5
504522
2015-11-16 07:51:35
--------------------------
203788
7966
Đây đúng là quyển sách kỹ năng đang tìm để đáp ứng nhu cầu công việc hiện tại.
Các chương trong sách đều rất bổ ích đặt biệt cho những người đang cần thông tin như thế này để hỗ trợ rất nhiều trong công việc và trong đời sống.
Mình đã giới thiệu bạn bè , đồng nghiệp trong công ty mình rất nhiều vì cuốn sách thật sự cần thiết .
Mình cảm thấy tự tin hơn rất nhiều sau khi đọc hết trang cuối cùng của sách và thay đổi nhiều trong cách nghĩ cách thương lượng với đối tác.
Cảm ơn tác giả và tiki rất nhiều.
5
425559
2015-06-02 12:24:36
--------------------------
172502
7966
Giống như chơi một ván cờ tướng hay cờ vua, quá trình thương lượng cũng có những nước cờ riêng của nó, bất kể chúng ta đang thương lượng về vấn đề gì, kinh doanh, mua bán nhà đất hay cả khi xảy ra xích mích. Trong tác phẩm này, Roger Dawson đã chỉ ra 64 nước cờ giúp chúng ta chiến thắng trong thương lượng nhưng ở vị thế mà cả 2 cùng thắng, tức là người kia không cảm thấy mình thua. Thương lượng về giá nhưng không làm mất lòng khách hàng, thương lượng về mức lương nhưng không làm mất lòng nhà tuyển dụng hoặc sếp cũ,... là những điều mà chúng ta có thể làm được nếu đọc và thực hành nội dung trong quyển sách tuyệt vời này.
5
387632
2015-03-24 08:53:58
--------------------------
