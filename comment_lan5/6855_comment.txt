478074
6855
Cuốn này cùng bộ với cuốn Xanh da trời và xanh lá mạ nhưng thật lòng mà nói không được bằng cuốn Xanh da trời và xanh lá mạ. Không nhiều truyện hay và đúng là có nhiều truyện rất quen, bạn có thể đọc ở trong trường phổ thông như Lặng lẽ Sapa, Đứa con nuôi. Một vài truyện ngắn của nước ngoài cũng không thật sự đắc sắc lắm, thậm chí còn hơi khó hiểu, gây buồn ngủ. Tôi khá thất vọng về cuốn này, nhưng đã lỡ mua rồi nên cũng cố gắng đọc hết cuốn. Truyện ngắn mà tôi thích nhất trong cuốn này cũng là tên cuốn sách luôn.
3
1116390
2016-07-30 18:24:54
--------------------------
166950
6855
Cuốn Bông mẫu đơn màu trắng này hẳn sẽ làm bạn thất vọng !
Tựa sách mặc dù có vẻ lạ nhưng thật ra bên trong lại là tuyển tập các truyện ngắn của nhiều tác giả, Bông mẫu đơn màu trắng chỉ là một trong số đó. Và đa số bạn đã được học 50% truyện trong cuốn sách này ở cấp Hai hoặc các sách liên quan.
Về mặt hình thức sách, bìa sách rất xấu và dễ bám bụi, dấu vân tay,... giấy xốp màu kem đen và tối, giấy sần sùi khá xấu và mực in đậm khó nhìn. Một cuốn sách không mấy hay cho lắm ?
3
298441
2015-03-13 20:17:44
--------------------------
