470546
8263
Tôi mua quyển này vì ấn tượng với cái bìa sách do Tamypu vẽ. Nhìn rất cuốn hút nên đã quyết định mua quyển này mặc dù chẳng biết tác giả Thủy Anna là ai cả. Bookmark đẹp khổ sách nhỏ nhỏ xinh xinh cầm rất vừa tay. Nhưng đó chỉ là hình thức bên ngoài còn nội dung bên trong thì thật sự chẳng làm vừa lòng tôi tí nào cả. Nội dung thì cứ nhàn nhạt không mang nhiều ý nghĩa đặc sắc các truyện ngắn không có gì ấn tượng chẳng để lại cho tôi cảm xúc gì khi gấp quyển sách lại. Và cảm thấy hơi tiếc tiền khi mua quyển này
2
278052
2016-07-07 17:26:34
--------------------------
311332
8263
Nếu như chưa từng gặp anh thì điều gì sẽ xảy ra. Cuộc sống của em vốn rất yên bình nhưng tại sao anh lại xuất hiện để làm rối nó lên cơ chứ. Phải chi ngày đó ta không biết nhau thì có tốt hơn không. Nhiều lúc em ước mình được trở lại những ngày mà ta chưa từng quen biết để em thôi vấng vương. Nhưng người ta vẫn thường hay "nếu như" cho những điều đã xảy ra rồi. Làm sao có thể quay lại được như ngày đầu phải không anh! Và em sẽ xem đó là duyên, duyên chúng ta chỉ là một đoạn ngắn trên đoạn đường dài kia. Được biết anh cũng đủ là em hạnh phúc. Em sẽ xem đó là hồi ức đẹp, một món quà ta dành cho nhau. "Nếu như chưa từng gặp anh" chỉ là tựa đề nhưng nó đã rất lôi cuốn. Câu truyện là tiếng lòng của người con gái trong mối tình đơn phương tự dằn vặt. Thấy đâu đó có chút đồng cảm với chính mình về những điều mình đã trãi qua. Sách còn bé quá so với giá nên làm mình hơi thất vọng tí nhưng dù sao vẫn thích "nếu như".
"Em vẫn gọi anh là chàng trai
Bởi không có ngày hôm qua và ngày hôm sau để gọi tên..."
3
475077
2015-09-19 22:40:59
--------------------------
245247
8263
'' Nếu như chưa từng gặp anh'' cuốn truyện bé bé xinh xinh, bìa đẹp và cầm vừa tay. Cuốn sách chắt lọc và ghi lại một cách nhẹ nhàng hết thảy những kỉ niểm đáng yêu, những vui buồn lẫn lộn hay cả nhưng suy tư, những cảm xúc trong trẻo của người con gái. Mình thích nhất sự bộc bạch kể chuyện hết sức nhẹ nhàng mà lôi cuốn của Thủy Anna. '' Lúc giận nhau, một ngày dài đến thế. Nỗi buồn đặc quánh trong suy nghĩ của em. Tình yêu dù ở thiên đường, hay mặt đất. Nhất định vẫn cần đến sự chân thành. Nhất định chỉ nên có hai người, là đủ.'' Cảm ơn Thủy Anna đã mang đến cho mình những giây phút sống thực sự chậm lại và lắng sâu !!!
3
172743
2015-07-28 20:21:58
--------------------------
198211
8263
Gọi đây là một cuốn tiểu thuyết thì không đúng, bởi đây là một cuốn sách mỏng vỏn vẹn 123 trang kể về câu chuyện tình yêu đơn phương của một cô gái trẻ với một bác sĩ-người cũng đang có những đau khổ trắc trở  trong tình yêu. Cuốn sách không có quá nhiều lời đối thoại giữa 2 nhân vật chính, phần lớn là những lời tự sự , những bộc bạch về tình cảm, những nhớ mong không nguôi, những băn khoăn trăn trở về tình yêu của cô gái trẻ vốn đã vuột mất mối tình đầu của mình, giờ không biết sẽ đi về đâu với mối tình đơn phương kế tiếp. "Nếu như chưa từng gặp anh" được kể bằng giọng văn nhẹ nhàng, bay bổng và lãng mạn , hẳn sẽ là cuốn sách thú vị cho những ai cũng đã từng có thời gian yêu đơn phương một ai đó. Một cuốn sách lãng mạn cho những ngày mưa buồn...
4
75031
2015-05-19 14:17:08
--------------------------
117834
8263
Bìa sách thể hiện rất đúng với tên sách cũng như nội dung trong cuốn sách.  Nó như một mê cung tình yêu khơi gợi cho người đọc sự tò mò, sự rối rắm,một vòng luẩn quẩn như một mê cung.Có thể nói "Nếu như chưa từng gặp anh" là sự rung động khá nhẹ nhàng của người con gái khi mới chớm yêu. Câu chuyện khiến người đọc như đóng vai nhân vật Băng Di trong câu chuyện, bởi những cảm xúc của cô rất thật, rất tinh tế. Phần kết câu chuyện khá hụt hẫng và khó hiểu, chắc cũng phần nào khơi gợi người đọc sự tưởng tượng về cái kết, cách trình bày cốt truyện này rất hấp dẫn người đọc...
4
352490
2014-07-23 14:01:09
--------------------------
115566
8263
Như một cuốn nhật kí của tác giả viết về thời con gái với những rung đồng đầu đời rất đáng yêu. Có chút ngang bướng, có chút thương nhớ, có chút trẻ con, tinh nghịch, ngốc nghếch. Một mối tình đơn phương rất ngọt ngào, nhưng cũng rất cay đắng. Những hình ảnh mà tác giả Thuỷ Anna sử dụng rất đẹp, rất thơ. Cả nỗi buồn cũng rất đẹp. Nỗi buồn đó, nhẹ như mây. Một cuốn sách rất bình yên, chậm rãi. Cả M, P và "tôi" đều rất xứng đáng để có được hạnh phúc. Một cái kết mở. Cho cả ba.
5
182813
2014-06-29 20:54:05
--------------------------
112817
8263
Đọc "nếu như chưa từng gặp anh" của Thủy Anna mình có cảm giác giống như đọc một quyển nhật kí của một người con gái viết về nhưng rung động, thương nhớ của mình dành cho người yêu, những tủi hờn, đau khổ khi không được đến bên người mình yêu thương. Nhẹ nhàng len vào dòng cảm xúc là nỗi nhớ thương chua xót dành cho người đã mất. Tuy nhiên cách viết có phần hơi mông lung không có đích đến. Mình hơi hụt hẫng vì tác giả để một kết thúc mở. không biết rồi M và Băng Di có đến với nhau không hay như câu nói "chỉ cần P là đủ"???
3
187187
2014-05-21 08:06:09
--------------------------
