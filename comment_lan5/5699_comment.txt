452367
5699
"Cái mũi đen" là câu chuyện nhẹ nhàng, cảm động về hai mẹ con chú gấu trắng Bắc cực Koda khi đối mặt với gã thợ săn Vova. Tuy câu chuyện ngắn, ngôn từ sử dụng đều trong sáng, dể hiểu, phù hợp với trẻ nhỏ nhưng ý nghĩa câu chuyện đem lại vô cùng sâu sắc. Nó đã khơi gợi lên tình cảm mẹ con trong tâm hồn non nớt của trẻ một cách hồn nhiên, trong sáng nhất. Hình minh họa rất phù hợp với nội dung, màu sắc nhẹ nhàng, bản in rất đẹp! Truyện này rất phù hợp để mẹ kể cho bé trước khi ngủ.
5
1433819
2016-06-20 16:31:10
--------------------------
385728
5699
Truyện “Cái mũi đen” được dịch từ tiếng Hàn, truyện có tổng cộng 37 trang. Cũng như cuốn truyện “Làn hơi ấm”, truyện “Cái mũi đen” cũng có sự xuất hiện của gấu mẹ, gấu con Koda và gã thợ săn Vova. Giữa một ngôi làng gấu Bắc Cực tuyết phủ trắng xóa, ắt hẳn rất khó tìm được chú gấu Bắc cực với bộ lông trắng như tuyết, nhưng cái mũi của gấu Bắc Cực rất to mà lại đen sì – đó chính là điểm giúp gã thợ săn Vova tìm thấy hai mẹ con gấu.Nhưng rồi, sau đó Vova lại chẳng nhìn thấy hai cái mũi đen đâu nữa, vậy cái mũi đen đâu rồi? Làm sao hai cái mũi đen lần lượt biến mất trước mũi sống của gã thợ săn? Bố mẹ hãy cùng con khám phá nhé.
3
15022
2016-02-24 13:33:46
--------------------------
306201
5699
Cuốn sách đẹp một vẻ đẹp dễ thương. Cốt truyện cũng nhẹ nhàng, lời văn giản dị, rất phù hợp cho các bé được nghe mẹ đọc cho cũng như các bé mới tập đọc. Nội dung trong trẻo, chứa đựng những bài học theo kiểu để các bé tự ngấm chứ không phải theo kiểu hô khẩu hiệu. “Cái mũi đen” là tình yêu, là bản năng hy sinh quên mình của người mẹ. Đó là tình yêu, sự tha thiết với mẹ của đứa con. Và đất trời che chở, bảo vệ cho tình yêu ấy khỏi nguy hiểm.
5
135645
2015-09-17 13:42:42
--------------------------
296540
5699
Lúc mở sách ra đã rất ngạc nhiên, sao mỏng và to thế, nhưng nhìn baby nhà mình tự đọc, tự lật từng trang một thì đã hiểu lý do rồi.
Tranh vẽ rất dễ thương, vừa chân thực, vừa gợi được nhiều liên tưởng cho trẻ nhỏ. Nội dung mang đến nhiều giá trị sâu sắc. Cùng đọc với trẻ nhỏ, người lớn không chỉ đọc một câu chuyện ngắn gọn, mà còn có thể tự sáng tạo thêm những chi tiết, giảng giải, chỉ dạy thêm, đặt ra những câu hỏi để các bé hiểu cặn kẽ và tự tìm ra được nhiều điều mới mẻ nữa.
Cả hai quyển đều rất dễ thương, mua tặng làm quà cho các gia đình trẻ là hợp cực luôn đấy.
5
141514
2015-09-11 09:50:03
--------------------------
277722
5699
Câu chuyện nằm trong bộ 2 cuốn truyện kể về hai mẹ con gấu Bắc Cực và gã thợ săn Vova. Ở truyện này tác giả giới thiệu với các em nhỏ về gấu Bắc Cực trắng như tuyết nhưng có hai cái mũi đen rất to, và kể cho các em về tình yêu giữa hai mẹ con nhà gấu khi gấu mẹ ôm gấu con, còn gấu con thì che cái mũi của gấu mẹ để gã thợ săn không trông thấy. Hình vẽ minh họa đẹp, sách rất ít chữ (nên nhiều trang), các em nhỏ hẳn sẽ rất thích
4
499847
2015-08-25 10:47:47
--------------------------
264842
5699
Quả đúng là tất cả tình yêu trên thế gian đều bắt nguồn từ mẹ. Tình yêu của mẹ vô cùng cao cả, ngọt ngào và thầm lặng biết bao. Nội dung quyển sách rất đơn giản, ít chữ nhưng thể hiện tình yêu, sự hy sinh vô bờ bến của mẹ dành cho con mình một cách rất sâu sắc. Từng trang của quyển sách được vẽ thật đẹp, màu sắc tươi tắn, sinh động rất bắt mắt và rất dễ thương. Hai mẹ con gấu Koda đáng yêu vô cùng, rất yêu gấu bắc cực. Cám ơn tiki.
5
110777
2015-08-13 15:02:27
--------------------------
261888
5699
Mình rất thích quyển truyện này khi tình cờ được người bạn chia sẻ tiêu đề những quyển sách nên mua cho bé. Mình quả rất bất ngờ với sự nhẹ nhàng của câu chuyện. Thật sự thích thú khi ôm con trong lòng và Khi đọc đến đoạn gấu mẹ ôm con để che chở và cầu nguyện cho con, gấu con của mình cũng quay ra che mũi cho mẹ. Cuộc sống thật bình yên và hạnh phúc. Khi mình ôm con, con vòng tay che mũi cho mẹ. Khi mình làm động tác đi như gấu mẹ con cũng cong cong người đi theo. Cảm ơn tác giả và tiki rất nhiều. 
5
728382
2015-08-11 14:56:07
--------------------------
257860
5699
Mình rất thích quyển sách này từ hồi mới thấy tựa sách, truyện viết về tình cảm mẹ con ấm áp của 2 chú gấu từ đó hướng đến việc giáo dục tâm hồn. Truyện rất thích hợp cho trẻ em đọc. Khổ sách khá to, nhưng lúc mua  về mình khá bất ngờ với độ mỏng của nó, quá mỏng so với mình nghĩ. Phần kim bấm giữa sách cũng chưa được đẹp, mình nghĩ với giá tiền 48.000đ đó thì bên nxb Nhã Nam nên đầu tư kỹ hơn về vấn đề này. Nhưng giấy in màu nên trong rất đẹp rất sắc nét.
4
19933
2015-08-08 01:12:32
--------------------------
253017
5699
Các câu chuyện về tình cảm mẹ con luôn là sự quan tâm hàng đầu của tôi. Đặc biệt khi bạn đã làm mẹ thì việc thể hiện tình cảm với con và dạy con về tình cảm này là điều rất quan trọng. Tôi đã chọn mua cuốn này cho con  và cùng đọc với con. Truyện viết rất nhẹ nhàng trong sáng, hình vẽ giản dị nhưng dễ thương, sâu sắc. Truyện kể về 2 chiếc mũi đen của hai mẹ con gấu Koda đang bị gã thợ săn Vova theo dõi. Và 2 chiếc mũi đen đã làm thế nào để thoát khỏi tầm ngắm của gã thợ săn, tất cả là nhờ tình yêu thương của hai mẹ con dành cho nhau, và nhờ chiếc ôm nồng ấm
4
478786
2015-08-04 08:37:27
--------------------------
224919
5699
MÌnh đặc biệt thích bộ đôi cuốn sách về mẹ con gấu trắng Koda này. Thật tình cảm và đáng yêu. Truyện ít chữ, tranh to và đơn giản, đến cả gam màu cũng tối giản, nhưng chịn sự tối giản ấy làm nên nghệ thuật. Mỗi trang truyện là một bức tranh, đẹp đến mức mình muốn treo lên. Nội dung thể hiện cả trong phần lời và phần tranh, dung dị, có những điều không nói bằng lời mà bạn vẫn có thể cảm thấy nó. Như tình yêu của mẹ, lặng thầm, sâu kín, nhưng yêu thương vô bờ bến.
5
82774
2015-07-09 14:41:51
--------------------------
224319
5699
Do mình là người rất thích các tác phẩm văn học thiếu nhi nên khi vừa thấy cuốn truyện này là chỉ muốn mua ngay.  Hình ảnh đẹp, màu sắc sinh động rõ ràng và đầy nghệ thuật là điều đầu tiên làm mình thích thú. Nội dung tuy đơn giản nhưng đã thể hiện được tình yêu thương vô bờ bến của mẹ dành cho con, là động vật nhưng chúng biết yêu thương bảo vệ gia đình mình, còn con người thì cứ nhẫn tâm sát hại các loài động vật khác chỉ vì lòng tham... Câu truyện thiếu nhi ấm áp, bổ ích, cùng giáo dục tất cả chúng ta không chỉ tiên gì trẻ nhỏ biết quan tâm muôn loài, không tham lam ích kỷ, không để lợi ích bản thân đặt lên trên những tình cảm thiêng liêng cao đẹp.
5
55223
2015-07-08 14:53:12
--------------------------
