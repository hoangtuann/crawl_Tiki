556225
4344
Những mem nào mới bắt đầu làm quen hoặc chưa hiểu nhiều về luật hình sự thì không nên mua. Các tình huống thì rất hay, rất thực tế, nhưng vấn đề ở chỗ là chỉ giải thích câu hỏi bằng lí thuyết chưa chỉ ra được vấn đề cần trả lời của câu hỏi trong tình huống, nếu chưa có một nền tảng vững vàng thì các bạn sẽ không thể hiểu được cân trả lời. Thêm một điểm trừ nữa là có một số tình huống hỏi vấn đề này nhưng trả lời vấn đề khác. Đọc xong mình có một số nhận xét như vậy, mong là tác giả xem xét bổ sung.
3
1419884
2017-03-28 10:33:43
--------------------------
208075
4344
Sách có tổng cộng là 41 bài tập và đây là những tình huống rất thực tế  và một số tình huống được tác giả lấy từ những vụ án có thật trong thực tế nên tính ứng dụng của quyển sách rất cao. Mỗi tình huống được trình bày rất bài bản và tình tự rất hợp lý: nêu lên tình huống, những câu hỏi cần phải trả lời, gợi ý trả lời, nguồn quy phạm pháp luật,... Theo đánh giá của mình thì đây là một quyển sách thực sự cần thiết cho tất cả các bạn đang theo học ngành luật hoặc những ai muốn tìm hiểu về bộ môn luật hình sự.
4
588073
2015-06-14 06:38:36
--------------------------
