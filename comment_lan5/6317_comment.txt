317209
6317
Mình mua quyển này hơn 1 tuần trước, sách tốt, đẹp dịch những câu thông dũng, dễ hiểu, dễ tìm nhưng chắc do lúc di chuyển nên lưng sách hơi không chắc cho lắm nhưng cũng không sao, có 1 vài câu dịch hơi huề vốn nhưng mấy câu đó dễ có thể tự hiểu được, sách rất tiện dụng hữu ích các bạn nên mua quyển này hiện thì chỉ còn 2 quyển và mình sẽ rủ bạn mua hết luôn nhá...Dịch vụ giao hàng cũng rất tiện lợi, tiki cũng rất nhiệt tình giúp đỡ, hỗ trợ và tư vấn...trang đáng tin cậy nên các bạn nên mua...
5
834908
2015-10-02 21:06:33
--------------------------
246942
6317
Từ bé ta vẫn nghe những người lớn tuổi nói chuyện tục ngữ, họ vẫn xen vào lời nói chuyện những câu thành ngữ ca dao khi răn dạy con cháu mình. Và không chỉ nghe thành ngữ tục ngữ, tôi còn muốn tìm hiểu về nó nữa. Đến với cuốn sách này như vậy, thật không thất vọng. Trước hết sách trình bày 2 khái niệm về 2 loại này. Sách bìa cứng khá dày dặn có sợi dây để đánh dấu trang. Về nội dung sách liệt kê một loạt thành ngữ tục ngữ và có đánh dấu từng thể loại cụ thể. Tuy nhiên nếu như phân loại thành ngữ tục ngữ theo từng mục cụ thể (thiên nhiên, tình cảm, con người...đại loại như thế) thì sẽ dễ theo dõi hơn.
4
299614
2015-07-30 09:35:33
--------------------------
