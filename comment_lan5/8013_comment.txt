501989
8013
Nhận xét của mình theo hình thức là bìa đẹp, chất lượng tốt nhưng còn về nội dung thì không rõ là do dịch giả hay do biên tập mà lúc đọc, mình thấy câu cú rất lủng củng, như thể chỉ cần thêm vài chữ vào trong câu thì đọc sẽ rất xuôi. Mình xin trích một đoạn trong cuốn này, những chỗ như "chuyến tàu suốt", "anh ta đang cuộc hành trình"... là gì? các bạn góp ý giúp mình với nhé.

"Thời gian trôi đi, bình thường con người ta không nhận thấy. Con người sống là sống trong một sự hoà hoãn tạm thời. Một khi đến được trạm nghỉ, bấy giờ ta mới biết ra lúc nào thì đè nặng trên ta ngọn gió đông bao giờ cũng tiến bước. Chúng ta giống như những hành khách kia trên chuyến tàu suốt, trong đêm tối ầm ĩ bánh xe lăn, chốc chốc loé lên một đốm sáng, anh ta ngồi bên này cửa kính đoán bao nhiêu thôn làng ruộng nương, bao nhiêu vùng hạnh phúc đã chảy tuôn qua một cách phung phí, mà anh ta không tài nào níu giữ cái gì được, vì anh ta đang cuộc hành trình. Chúng tôi cũng vậy, bừng bừng một cơn sốt nhẹ, hai tai còn ầm ì tiếng động cuộc bay, chúng tôi cũng cảm giác là mình đang cuộc hành trình, bất chấp cái yên tĩnh của trạm nghỉ. Chúng tôi cũng vậy, được đưa tới một tương lai nào không biết, qua sự nghĩ suy của các luồng gió, bằng những nhịp đập của trái tim mình."
2
193272
2016-12-29 13:18:19
--------------------------
366827
8013
Mình ưng cuốn này là vì thoạt đầu ấn tượng bìa sách kiểu khoảng rộng chân trời galaxy và cái tên đơn giản, ngắn gọn mà rất gây tò mò, cảm hứng. Sau đó, vào xem kĩ hơn mới biết của nhà văn Exupéry,thực lòng mà nói mình hâm mộ tác giả đã từ rất lâu rồi, khi mua cuốn "Hoàng tử bé" dịp kỉ niệm 70 năm. Mặc dù chưa đọc hết sách nhưng qua nội dung thì đúng là giọng văn của tác giả thật nhưng câu cú từ nhà biên soạn rất là mâu thuẫn người đọc, trông nhiều khi cứ như dịch google. Mong rằng khi mình góp ý như thế này sẽ nhà xuất bản sau khi tái in lần nữa sẽ chỉnh sửa thật tốt hơn.
5
724750
2016-01-10 20:31:02
--------------------------
306353
8013
Lối hành văn và cách sử dụng ngôn từ của Antoine de Saint-Exupéry không phải dễ hiểu hết được trong lần đọc đầu tiên, nên cuốn sách đòi hỏi đọc giả phải thật sự chú tâm khi thưởng thức nó mới hiểu hết các giá trị và thông điệp mà tác giả gửi gắm trên hành trình chiêm nghiệm và trải nghiệm cuộc đời của chính mình.

" Xứ con người" mang đến một câu chuyện nhẹ nhàng về sự hòa hợp của thiên nhiên với nắng gió, sao trời và con người- trung tâm của vũ trụ. Giá sách phù hợp với túi tiên, nhưng giá trị của nó thì vượt trội như các tác phẩm khác của chính tác giả. Tôi rất hài lòng với cuốn sách trên.
5
258935
2015-09-17 15:34:54
--------------------------
299506
8013
Sau khi đọc tác phẩm "Hoàng tử bé", mình đã rất thích nhà văn Antoine de Saint Exupery. Vậy nên mình đã lựa chọn mua tiếp cả "Bay đêm" và "Xứ con người". Bìa sách được thiết kế khá đẹp, bầu trời đêm với những vì sao, trên nền tím tím thơ mộng là một chiếc máy bay kiểu cũ. Cũng như tác phẩm "Bay đêm". "Xứ con người" không quá dài, vì vậy giá thành rất rẻ. Nhưng không vì thế mà đây là một tác phẩm không hấp dẫn. Trái lại, đây là câu chuyện với nhiều màu sắc. Gió, cát, sa mạc, sao, và cả con người nữa. Văn của Antoine de Saint Exupery luôn khiến người ta phải đọc đi đọc lại, nghiền ngẫm thật kỹ thì mới hiểu trọn vẹn được những gì ông muốn truyền tải. 
5
340213
2015-09-13 12:09:17
--------------------------
297718
8013
truyện khắc họa cuộc sống rất chân thực , khiến cho mình cảm thấy nhà văn có lối suy nghĩ về con người rất rõ ràng , tác giả hiểu con người như từng trải qua những điều trăn trở của con người nên thấu hiểu được họ và nếu có khó khăn thì tác giả cũng không mấy suy nghĩ về nó theo chiều hướng khó khăn gian khổ mà chỉ mong cuộc sống tốt đẹp hơn theo sự tiến triển của nó mà thôi . mình thích câu chuyện ông mang đến người đọc một sự sâu sắc 
4
402468
2015-09-12 03:07:01
--------------------------
276706
8013
Vốn rất thích tác giả Antoine de Saint-Exupéry qua tác phẩm "Le Petit Prince" vì thế mình đã tìm mua các tác phẩm của ông ấy được xuất bản ở Việt Nam. Và "Xứ con người là quyển thứ 3 (Thứ nhất là "Hoàng tử bé", Thứ 2 là "Bay đêm"). Nhưng tác phẩm nào của ông cũng thế, đọc 1 lần chưa đủ thấm để hiểu, và đọc lướt càng khó nắm bắt ý nghĩa từng từ, từng câu. Chính vì thế, mỗi lần đọc tác phẩm này hay những tác phẩm khác của ông, ta phải đọc từng từ, từng chữ và phải đọc đi đọc lại nhiều lần mới thấm và hiểu điều ông muốn truyền tải. Cơ mà đừng lo bởi bạn sẽ khogn6 bao giờ ngán khi phải đọc đi đoc lại nhiều lần tác phẩm này đâu^^
5
393159
2015-08-24 11:57:22
--------------------------
275895
8013
Qua ngòi bút tinh tế của Antoine De Saint-Exupéry, vẻ đẹp của sa mạc cũng hóa người. Bay muôn ngàn ngả, rung rinh rung rinh  nhưng không bao giờ từ bỏ mục đích, mãi trung thành như đụn cát sa mạc. Là một nhà văn kiêm phi cong chuyên nghiệp, Antoine De Saint-Exupéry có những chất liệu tuyệt vời để tạo nên áng văn chương bất hủ, nóng bỏng như cát trên Sahara nhưng dịu dàng không kém ánh sao đêm. Cuốn sách nhỏ này được trình bày và in ấn rất tinh tế, rất đáng đọc.
5
292321
2015-08-23 13:33:19
--------------------------
259632
8013
Xứ con người là tác phẩm thứ ba, được tôi mua của nhà văn người Pháp Antoine de Saint-Exupéry do Nhã Nam xuất bản. Đây quả là một cuốn sách tuyệt vời cả về nội dung lẫn hình thức. Nó không dài (nên giá rất rẻ) nhưng lại mang vô cùng nhiều ý nghĩa. Chưa kể nội dung thì cuốn sách tuyệt đẹp về phần nhìn. Cái bìa đầy thơ mộng với ngàn ánh sao, cát sa mạc, máy bay, phi công và có lẽ là gió. Cái bìa đã thể hiện được tinh thần của truyện, một bản anh hùng ca của sao, gió, cát và con người.
5
167283
2015-08-09 17:58:40
--------------------------
233719
8013
Thật sự mà nói, để đọc và hiểu được tác phẩm này (dù không nhiều), mình đã đọc đi đọc lại từng câu, từng chữ rất nhiều lần. Dù đã đọc đi đọc lại như thế nhưng mình vẫn thấy rất khó để nắm bắt được hết những gì tác giả muốn gửi gắm.Đây thực sự là một tác phẩm khó đọc, không bay bổng nhẹ nhàng như "Hoàng tử bé", nhưng việc cố gắng để hiểu một tác phẩm như vậy thật sự rất thú vị. Và còn một điều này nữa, cảm xúc mà "Xứ con người" và "Bay đêm" mang lại cho mình thật sự rất giống nhau, phải chăng là do cả hai đều khó đọc cả ư?
5
247339
2015-07-19 21:11:14
--------------------------
224370
8013
Tôi mua và đọc cuốn này vì quá mê cuốn "Hoàng tử bé", và thật là thất vọng vì dịch giả đã làm cho cuốn sách trở nên tối nghĩa và dùng một số từ khó hiểu. Cuốn sách như lời tự tình của tác giả về cuộc đời, và về ngành hàng không. Tưởng chừng đó chỉ là một ngành đơn giản như bao ngành nghề khác, nhưng không phải vậy nó đầy nguy hiểm nhưng cũng không kém phần nên thơ, cùng với bao kỷ niệm về đồng nghiệp về những lần gặp hiểm nguy và thoát chết. Vẫn là chất văn đầy ý nghĩa như cuốn "Hoàng tử bé", nên ai đã thích nét đặc trưng đó, thì cũng sẽ say mê cuốn này y như vậy. Hy vọng nếu lần sau tái bản, thì sẽ hay hơn vì phần dịch quá kém ảnh hưởng đến nội dung.
4
279639
2015-07-08 15:59:20
--------------------------
215763
8013
Hoàng tử bé là một trong những quyển sách yêu thích nhất của mình, và tác phẩm này của nhà văn Antoine De Saint-Exupéry đã không hề làm mình thất vọng khi quyết định đặt mua. Tác giả đã vẽ ra cả một vùng trời hoang sơ bí ẩn, nét đẹp phi thường của vũ trụ và tâm hồn con người trong cuốn sách nhỏ tuyệt đẹp này. Theo những dòng tâm sự miên man của người phi công đọc giả có thể cảm thấy sững sờ khâm phục trước tự nhiên, và trên cả những cái đó, lòng can đảm của con người khi tiến hành công cuộc chinh phục tự nhiên, chinh phục vẻ đẹp đó. Bìa sách cũng đẹp tuyệt vời luôn, thực sự là bầu trời đêm trong tưởng tượng của tôi với những vì sao lấp lánh. Bất cứ ai đã từng ngước mắt lên ngắm bầu trời và ngưỡng mộ vẻ đẹp của xanh thẳm mênh mông, của trăng đêm và những vì tinh tú nhất định sẽ thích quyển sách này.
4
419116
2015-06-26 19:50:00
--------------------------
214291
8013
Mình vừa bắt đầu quyển Xứ Con Người vì rất hâm mộ bác Antoine, nhưng đọc được vài trang không hiểu có phải mình đọc tiếng Việt không nữa :|
- "...anh đã cho tôi thấy: qua cái vỏ ngoài cục mịch, vị thiên thần thắng được rồng tinh." ----> có ai liên tưởng được đây là câu văn nói về một phi công lái máy bay dân sự vừa bay qua cơn bão, vào thập niên 20 ở Pháp ko?
- "cái thứ nhựa trắng ấy đối với tôi trở nên biên giới giữa thực và hư..." ----> diễn đạt từ ngữ, câu cú ngữ pháp kiểu gì đây?
- "Đến lượt tôi, tôi sắp sửa, từ lúc rạng đông, đảm nhiệm một chuyến chở nhiều hành khách, đảm nhiệm một chuyến thư từ đi châu Phi."
Không biết đây là dịch giả Thành Long dịch word-by-word hay là biến hóa quá ảo diệu lời văn vậy? Thiết nghĩ biên dịch cái tiên quyết là hành văn đúng ngữ pháp, không quan trọng là dịch từ tiếng nào nhưng khi về tiếng mẹ đẻ phải trôi chảy, sau là giữ được thần thái của nhà văn, của văn hóa đất nước nhà văn, chứ không phải cho ra sản phẩm như được dịch từ Google Translator vậy, rồi còn chêm vào mấy từ rất lạc tông như vậy.
Nản quá hết muốn đọc, tác phẩm kinh điển vậy mà...
3
391626
2015-06-24 20:07:33
--------------------------
173437
8013
Đây là cuốn sách thứ hai tôi đọc của Saint-Euxpery, sau cuốn "Hoàng tử bé". Tôi ngưỡng mộ văn phong triết lí sâu sắc ẩn chứa dưới những ngôn từ rất dễ thương của ông, đặc biệt là vô số những bức tranh sống động mà ông vẽ lên cho tôi, nét vẽ vô cùng đáng yêu, và tôi đã lục lọi để tìm đọc thêm các tác phẩm của ông để được học hỏi thêm về điều ấy.
"Xứ con người", một câu chuyện dưới lăng kính của những con người trưởng thành, có thể gọi là cuốn tự truyện kinh điển của ngành hàng không, của những người phi công từ những năm ngành hàng không còn mới bắt đầu xuất hiện, phục vụ trước hết cho thông tin liên lạc. Sự hi sinh, can đảm, thậm chí là đánh đổi bằng cả mạng sống của bản thân cho niềm tin, và sứ mệnh của mình, những người chiến sĩ thực thụ. Tôi tự hỏi rằng, trong cả mấy thập kỉ đến nay, những người phi công còn có thể giữ được những phẩm chất cao quý như thế không, hay chỉ có thể có được trong tâm hồn những người phi công ở thời đại đó.
Tôi nghĩ rằng Saint-Exupery là một nghệ sĩ, khi mà trí tưởng tượng của ông có thể tràn đầy màu sắc như vậy. Ông viết như là đang vẽ, ông đã từng vẽ những bức minh họa ngộ nghĩnh trong "Hoàng tử bé", và tôi tin rằng khi ông viết những áng văn này, ông đã tự vẽ chúng ở trong tâm trí mình, chỉ còn mượn phương tiện để có thể thể hiện được thôi.
Sa mạc có lẽ đã gắn liền với cuộc đời ông, hoặc ảnh hưởng tới ông một cách nào đó mạnh mẽ, hoặc thật sự là đã có những câu chuyện kì diệu đã xảy ra, hoặc đã có những con người mà ông từng phải suy nghĩ sâu sắc và khắc khoải như những người nô lệ da đen trong tác phẩm này.
5
249602
2015-03-26 10:24:42
--------------------------
129266
8013
"Xứ con người" có thể xem như một tự truyện của Saint-Exupéry về công việc can trường của ông và những người đồng đội: không bưu - vận chuyển thư và vật phẩm bằng đường hàng không. Quyển sách hơn 200 trang này sẽ đưa ta bay qua những sa mạc rộng lớn, vùng biển bao la và những ngọn núi phủ đầy tuyết trắng. Ta sẽ có cảm tưởng rằng thiên nhiên trong "Xứ con người" là một tử thần đích thực với lớp áo ngoài kiêu sa đẹp đẽ, sẵn sàng cướp đi sinh mạng của những chàng không bưu dũng cảm một khi họ mất cảnh giác. Tự truyện đề cao vai trò con người, đặt người không bưu và công việc ý nghĩa của họ lên trên cả sự vô hạn của tự nhiên và hữu hạn của đời người. Bên cạnh đó, đọc tác phẩm, ta còn bắt gặp những câu văn trau chuốt mang ý nghĩa nhân sinh sâu sắc được đúc kết từ những trải nghiệm về cuộc đời của Exupéry. Đây thật sự là tác phẩm rất đáng đọc.
5
398179
2014-10-08 13:57:53
--------------------------
103748
8013
Xứ Con Người là một quyển tự truyện rất hay. Nó mở ra cho ta rất nhiều suy nghĩ sâu sắc về con người và lý tưởng, ước mơ, những triết lí nhân sinh sâu sắc về tình yêu, tình bạn, tình đồng chí. Khép lại cuốn sách, ta có thể cảm nhận được ngay cả trong gian khổ, thiếu thốn vẫn có những nét đẹp, nét lãng mạn rất riêng, làm ta say mê qua từng câu chữ. Câu chuyện hi sinh trong tác phẩm không mang đậm sắc thái u ám hay buồn tủi, trái lại, nó cho ta cái nhìn rất khác, về những con người thầm lặng nhưng vĩ đại, ngày qua ngày chở những chuyến thư đi. Văn phong trong tác phẩm quả thật rất phóng khoáng, mở ra trong mắt người đọc nhiều chân trời, mới có, lạ có, cuốn hút người xem.
4
195117
2014-01-03 12:44:24
--------------------------
