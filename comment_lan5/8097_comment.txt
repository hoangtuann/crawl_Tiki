258391
8097
“Những Câu Chuyện Về Tính Lương Thiện”, nằm trong bộ sách thiếu nhi “Những Câu Chuyện Về Những Giá Trị Cao Đẹp”.
Về nội dung, những câu chuyện trong sách đơn giản nhưng chứa nhiều giá trị, có thể giúp ta dạy trẻ nhiều điều hay lẽ phải, giúp trẻ hình thành nhân cách tốt đẹp.
Về hình thức, cuốn sách nhỏ nhắn, bìa màu bắt mắt, giấy in dày dặn, màu trắng hơi ngà giúp cho mắt đỡ bị mỏi nếu thời gian đọc lâu, trình bày sách thoáng đãng dễ nhìn, có hình minh họa.
So sánh một chút, cá nhân mình thấy “500 Câu chuyện đạo đức” hay hơn.

3
501251
2015-08-08 15:03:00
--------------------------
