469255
6688
Chú chiếu bóng , nhà ảo thuật và tụi con nít xóm nhỏ sài gòn năm ấy là một cuốn sách có tựa đề khá dài dòng của ông Lê Văn Nghĩa . Điều làm mình chú ý đến cuốn sách này cũng chính là cái tựa đề dài dòng ấy của cuốn sách . 
Sách của ông là sách dành cho thiếu nhi , nhưng người lớn đọc cũng hổng sao , mà người già đọc càng khoái . Bìa sách nhìn rất vui nhộn và bắt mắt . Nội dung trong sách cũng rất dễ thương và dí dỏm . Nhiều chỗ khiến mình phải bật cười khi đọc . Mình khá thích cuốn này . Cảm ơn Tiki nhiều .
5
593209
2016-07-06 10:44:23
--------------------------
463021
6688
Bản thân là một đứa sinh ra và lớn lên ở khu Chợ Lớn, quận 5, mình cực kì phấn khích khi đọc quyển sách này của tác giả Lê Văn Nghĩa. Cả một quyển sách là một lát cắt về đời sống cư dân vùng Chợ lớn những năm 60 -70, với đủ mọi thành phần trong xã hội như chính cái tên ngộ nghĩnh của nó. Đọc “Chú Chiếu Bóng, Nhà Ảo Thuật, Tay Đánh Bài, Và Tụi Con Nít Xóm Nhỏ Sài Gòn Năm Ấy’’ mình như thấy cả tuổi thơ, không phải là của mình mà là của ba má mình, lớp thị dân Sài Gòn – Chợ Lớn ba, bốn chục năm về trước. Lịch sử được thể hiện sống động qua từng trang sách, từ những đứa trẻ nhỏ xóm nghèo, những người lái ba gác chở thuê, những tay giang hồ tứ chiếng với lối ngôn ngữ ‘’rặt’’ chất dân Sài Gòn thời đó, cho đến những địa điểm vẫn còn hằn dấu thời gian cho tới nay như chợ Bình Tây, xóm Bãi Sậy, rạp Tân Lạc nay là Trung tâm văn hóa Hậu Giang, trường trung học Bình Tây, Ty cảnh sát nay là UBND quận, hay những thứ chỉ còn là kí ức một thời quá vãng như các cây cầu Palikao, cầu Bình Tiên, cầu Chợ Lớn, bắc qua các con rạch như rạch Bãi Sậy, rạch Hàng Bàng nay đã bị lấp, đứng trên khu đất này bây giờ khó có thể tưởng tượng được cái cây cầu ngày xưa ra sao. Đôi lúc khi đang đọc bạn có thể sẽ thốt lên: “ê chỗ này gần chỗ mình ở nè”, “cái Tửu lầu Á Đông này bây giờ vẫn còn nè”, “người ngày xưa nói chuyện ngộ quá hén”. Biết bao điều thú vị và lôi cuốn. Tốm lại, rất đáng đồng tiền bát gạo!
5
282485
2016-06-29 01:01:55
--------------------------
443182
6688
Chú Chiếu Bóng, Nhà Ảo Thuật, Tay Đánh Bài, Và Tụi Con Nít Xóm Nhỏ Sài Gòn Năm Ấy không chỉ là một câu chuyện về tuổi thơ của một xóm nhỏ nghèo ở Sài Gòn trong những năm 60 mà còn là một quyển sách về văn hóa, đời sống của người Sài Gòn trước đây được tác giả lồng ghép một cách khéo léo. Có những từ rất đặc trưng mà chỉ người Sài Gòn mới biết đều được tìm thấy trong tác phẩm này, một chiếc vé về với tuổi thơ đầy màu sắc, đầy hoài niệm. 
5
464110
2016-06-06 14:05:40
--------------------------
433898
6688
Không giống những chuyện nói về trẻ con cho người lớn đọc như kiểu Nguyễn Nhật Ánh. Truyện của Lê Văn Nghĩa có nhiều điểm mới lạ và có nhiều thứ mình thích hơn. Câu chuyện về bọn trẻ con nghèo đói ở Sài Gòn ngày xưa, nghèo tới nỗi cơm nấu cho có cháy, rồi đánh cái cháy đó ra cuộn lại chấm với mắm kho quẹt để làm thức ăn, cơm mà thức ăn cũng là cơm, ám ảnh cực kỳ luôn. Mua thịt cho thằng bạn bằng phiếu đổi bánh mì ở nhà thờ, đẩy xe hàng qua cầu để kiếm tiền giúp bạn, rồi làm đủ các thứ. Nói chung là đọc sẽ biết. 
Truyện có rất nhiều ngôn ngữ kiểu như: dầu hèn cũng thể, bể cái lu còn cái khạp, mút chỉ cà tha,… toàn phương ngữ được dùng ngày xưa, và có cả tiếng Tàu bồi nữa. Truyện hay, cảm động và đầy tình người.

5
1252869
2016-05-21 23:11:25
--------------------------
408337
6688
Đây là lần đầu tôi đọc một tác phẩm của nhà văn Lê Văn Nghĩa và cảm giác là "thích hết chỗ nói". Giọng văn của chú Nghĩa rất gần gũi, bình dân, mang hơi hướng của bối cảnh trong tác phẩm của chú. Các mối quan hệ bạn bè, thầy trò, người lớn - trẻ con rất bình dị nhưng toát lên tính nhân văn qua những biến cố, cách hành xử và suy nghĩ của những con người có liên quan trong đó. Lâu lắm rồi tôi mới lại được cảm nhận cái gốc của "sự học" qua một tác phẩm, khi đề cao nhân cách đáng trọng của con người lên trên hết. Câu chuyện rất ý nghĩa, chứa đựng những bài học nhẹ nhàng mà thấm thía. Thú vị hơn nữa là qua tác phẩm này, những tiếng lóng, khung cảnh Sài Gòn, những triết lý của con người trong giai đoạn ấy được tái hiện thật sinh động, hấp dẫn và đầy cảm xúc. 
5
113977
2016-03-31 15:39:11
--------------------------
396841
6688
Phải nói nhà văn Lê Văn Nghĩa rất giỏi đặt tên sách. Từ những cái tên như Chuyện chán phèo, Tào lao xịt bộp, Nếu Adam không có xương sườn đến quyển sách này. Ông dắt người đọc vào một thế giới tuổi thơ hồn nhiên bằng giọng văn cũng hồn nhiên. 
Ngoài các nhân vật người lớn gắn với câu chuyện của tụi con nít và được xuất hiện chiễm chệ trên bìa sách như chú chiếu bóng, nhà ảo thuật, tay đánh bài , tôi còn rất ấn tượng với nhân vật cô giáo Lan Sinh. Một người thầy luôn chú trọng việc xây dụng giá trị đạo đức cho học trò của mình, cô cảm thây mình là người có trách nhiệm khi tụi nhỏ tìm cách kiếm tiền sớm.
Câu kết của truyện làm tôi phá lên cười. Khi đọc đến đoạn "tôi chính là" thì trong đầu tôi nghĩ chắc là thằng nhóc xuyên suốt câu chuyện như thằng Minh chứ không thể là 2 nhân vật chính còn lại như thằng Ti hoặc thằng Chim. Vậy mà tác giả nỡ lòng nào phán cho một câu xanh rờn  nêu đích danh một nhân vật ít "đất diễn" hơn hẳn.
5
653141
2016-03-14 09:31:28
--------------------------
389993
6688
Lê Văn Nghĩa chưa bao giờ làm mình thất vọng. Khoái giọng văn của chú từ thời Tuổi Trẻ Cười, mấy năm nay không đọc TTC nữa thì tìm sách của chú. "Mùa hè năm Petrus", rồi "Chú chiếu bóng, nhà ảo thuật, tay đánh bài và tụi con nít xóm nhỏ Sài Gòn năm ấy", đều là những trang sách đẹp. Đẹp vì văn hay, đẹp vì mang lại hồi ức xưa cũ cho những người từng sống qua thời đó (cho dù có thể không ở cùng một nơi), đẹp vì gợi nhớ lại hình ảnh người dân bình dị, thanh tao, không nhiều mưu mô hay kiểu sống mackeno bây giờ. Những đoạn nói về cách sống, cách làm người rất đẹp, không chút giáo điều mà cứ thấm vào lòng, không sao quên được. Cám ơn chú đã mang lại chút bình yên cho cuộc sống này!
5
227100
2016-03-03 05:38:15
--------------------------
385007
6688
Từ cái tên sách, cái bìa sách tới cái nội dung truyện hoàn toàn không có gì để chê bai cả.
Tụi con nít trong truyện lại lớn lên trong cái khu người hoa ở, nên lâu lâu lại thêm vài câu như đúng rồi, kể cả người lớn, với riêng mình lại càng quý hơn, vì mình là một đứa gốc Hoa nhưng lại không thể đằm mình trong cái thế giới quen thuộc như tụi con nít ấy.
  Nhân vật nào cũng đáng yêu dù chúng nó chỉ là những đứa trẻ nghèo dễ bị lây nhiễm thói hư tật xấu,nhưng sau cùng, vẫn chân chất và ngoan ngoãn. Cái kết mở, có những nút thắt vẫn không được tháo gỡ hẳn, nhưng không sao, vì nó lại khiến câu chuyện trở nên thật như đời thêm, thật đến mức những trang cuối rồi chú Nghĩa còn viết vài dòng, nhắn nhủ mình chính là con Hồng - một đứa con gái trong tụi con nít ở xóm nhỏ Ba-ra-dô năm ấy làm mình suýt tưởng ...là thật.  Lâu lâu đọc lại phải tấm tắc như trẻ nhỏ vừa được người lớn trong sách dạy cho mấy điều, ví như khi nhà ảo thuật gia bảo với thằng Ti đời người sống phải có cái tình , cùng nghề không được phá nồi cơm của nhau; rồi chú Hai Ngon yêu cái nghề chiếu bóng bảo ban thằng Minh phải học cho giỏi để sau này mới làm đạo diễn được, lúc nào cũng quen miệng nói "dẫu hèn cũng thể".

 Sài Gòn của những thập niên 60 hồi đó, ngay cả những đứa trẻ cũng được dạy cách yêu nước một cách rất gần gũi, như cách mẹ thằng Ti đánh anh em nó vì phát hiện ăn 'su-cu-la' của Mỹ, rồi hùng hồn bảo thà "nghèo thì cạp đất mà ăn nghe không tụi bây". Tụi nhỏ giận nhau, tị nạnh nhau, ghen ghét nhau, rồi sau đó lại đến gần nhau như chưa có khoảng cách nào. Sài Gòn của những thập niên 60 hồi đó mà khiến một đứa 9x như mình, dẫu có nhiều điều không được trải nghiệm qua nhưng vẫn thân thương quá đỗi. 

 May quá khi giữa hàng loạt sách của một tác giả quen thuộc là Nguyễn Nhật Ánh, vẫn có vài người (như trước đó là Nguyễn Ngọc Thuần, nhưng sau lại đi về mảng truyện ngắn/truyện dài về thế giới người lớn mất rồi, giờ đọc được quyển sách này của chú Nghĩa ) chịu viết truyện "dành cho thiếu nhi, nhưng người lớn đọc cũng hổng sao, mà người già đọc càng khoái", thương quá!
5
403401
2016-02-23 13:30:50
--------------------------
373080
6688
Nhà văn Lê Văn Nghĩa đã thật sự gây ấn tượng cho người đọc với tác phẩm có một cái tựa đề dài và độc đáo "Chú Chiếu Bóng, Nhà Ảo Thuật, Tay Đánh Bài, Và Tụi Con Nít Xóm Nhỏ Sài Gòn Năm Ấy"
Đó là câu chuyện gần gũi, thân quen, hài hước nhưng ẩn sâu lại là những điều tuyệt vời về tuổi thơ tuy có có khăn, nghèo khổi nhưng luôn tràn đầy tiếng cười, niềm vui, có cả nước mắt nhưng luôn thấm đẫm những triết lí sâu xa và tình người cao thượng của những con người Sài Gòn trước năm 1975.
Giọng văn hài hước với những từ ngữ địa phương, dân dã đã góp phần tạo nên nét nghệ thuật và độc đáo cho tác phẩm.
Quả đây đúng là một cuốn sách hay co cả thiếu nhi và người lớn cùng đọc.
5
748918
2016-01-22 21:40:45
--------------------------
370149
6688
Tiki giao hàng nhanh, có bookmark. Tên truyện làm mình ấn tượng bởi nó rất dài. Lê Văn Nghĩa là người đầu tiên tái hiện một cách đậm đặc và đầy chi tiết sống thực trong văn xuôi, với tư cách người trong cuộc. Tác giả không có ý giáo huấn ai, nhưng cuốn truyện của ông đặc buêtj khiến người đọc phải suy nghĩ về cách sống ở đơi, cách ứng xử trong tình bạn, tình thầy trò, tình người phố thị,... Cách mà bọn trẻ nghèo ở xóm nhỏ Ba-ra-dô khiến người đọc rưng rưng cảm động.
5
938958
2016-01-17 10:01:14
--------------------------
364682
6688
Giọng văn đầy khoa trương mà thật thực chất về một thế hệ vốn sinh sống dưới chế độ mới còn nhiều thiếu thốn , vật chất còn chưa nhiều nhưng với những trò chơi dân gian lại còn phải luôn nghĩ ra cách để bảo tồn nó hiện hành trên toàn thể sân chơi , ai cũng đưa mắt nhìn vào thói quen đó bỏ lại đằng sau sự thay đổi vĩnh viễn của con người trong thời đại lương tâm còn ngổn ngang với tính toán nửa vời giờ đã trôi qua khoảng cách thân thương của đời .
5
402468
2016-01-06 17:57:00
--------------------------
338328
6688
Những câu chuyện của thằng Minh với chú 2 Ngon, Thằng Ti với nhà ảo thuật, Thằng Chim đi theo tay cờ bạc đan xen với nhau. Những chi tiết đời thường ấn tượng như món "Sơn Hào Hải Vị" rau với mắm kho quẹt ngon tổ sư bồ đề của thằng con nhà giàu Long Mập, Cách tụi con nít xóm nghèo che chở cho nhau, tình cảm giữa chú 2 ngon với thằng Minh, tình thầy trò của nhà ảo thuật với thằng Ti, những bài học tuy giản dị nhưng thực tế. Và thích lắm sự thông minh, manh lanh của thằng Minh nữa.... thật sự là hay 1 cây xanh rờn luôn!
5
612834
2015-11-15 22:35:39
--------------------------
331880
6688
Mình mua cuốn sách này là cho ba mình nhớ lại tuổi thơ, vì Sài Gòn và tụi con nít trong cuốn sách này chính là những câu chuyện hồi nhỏ ba mình hay kể. Mình cũng mua cuốn sách này để tìm sự khác biệt của trẻ con ngày đó với bây giờ. Đúng là khác nhau nhiều, hồi đó đơn giản nhưng mọi người chơi chung với nhau vui hơn là bây giờ hiện đại, nhà ai nấy về, cắm đầu vào công nghệ :) Nếu được chọn, mình muốn là trẻ con hồi đó, dù không có tiền ăn quà, không được bữa cơm đàng hoàng, nhưng vui và đầy kỉ niệm!! 
Cuốn sách này rất nên mua, những câu chuyện nhẹ nhàng cho ta thấy được Sài Gòn vqf con người có rất nhiều thay đổi.
5
833314
2015-11-05 11:14:10
--------------------------
311205
6688
Đã từng biết chú Nghĩa từ "Mùa hè năm Petrus" nên nghe cái tựa dài dài như "Chú Chiếu Bóng, Nhà Ảo Thuật, Tay Đánh Bài, Và Tụi Con Nít Xóm Nhỏ Sài Gòn Năm Ấy" thì mình lên đặt sách về liền. Sau hai ngày là mình có sách.Kì này sách được bọc bao nilong lại,tránh lúc vận chuyển bị quăn.Nhìn bìa sách đã hấp dẫn,đọc lại càng thấy vui. Vẫn là cách viết đặc sệt giọng Nam Bộ,dí dỏm,cuốn hút của chú Nghĩa,nhưng với đám nhóc trong truyện này,mình có thể có góc nhìn rõ nét hơn về một Sài Gòn gần 40 năm về trước,những ngày trẻ trung mộc mạc đầu tiên của nó.Càng đọc mình càng mến văn chú Nghĩa và mến mảnh đất thân yêu nơi mình đang sống này.
4
178672
2015-09-19 22:06:41
--------------------------
310505
6688
Mình đặt mua quyển truyện là vì ấn tượng với tiêu đề. Một tiêu đề dài và gần như đã liệt kê hết những nhân vật trong đó. Hơn nữa, mình cũng rất thích đọc truyện về đề tài tuổi thơ này.  Đọc xong quyển sách, mình  vừa thấy một phần của mình trong đó và cảm thấy yêu lại những giây phút đã qua đó. Mình không phải người Sài Gòn, nhưng tác giả đã tái hiện cho mình một Sài Gòn xưa thật đẹp, với Minh, Ti, Chim hay những đứa trẻ trong xóm Ba-ra-dô. Đây là một cuốn sách dành cho mọi lứa tuổi, đúng như dòng chữ đầu sách "dành cho thiếu nhi, nhưng người lớn đọc cũng hổng sao, mà người già đọc càng khoái".
5
162797
2015-09-19 16:38:46
--------------------------
301988
6688
Mình vốn dĩ đã đọc rất nhiều truyện về khoảng thời gian này, nhưng rất ít hay nói đúng hơn là chưa ai viết về tuổi thơ của "những đứa trẻ" thời đó. Đây là một mảng rất ít được khai thác vì lúc nó người ta chỉ tập trung vào cuộc đấu tranh của dân tộc, về một miền bắc kiên cường, một miền nam anh dũng. Trong truyện, ta bắt gặp tuổi thơ của các chú, các bác, khi mà họ chưa được cách mạng giác ngộ và vẫn sống cuộc sống thường nhật của mình. Khi không nhắc đến tiếng bom, tiếng súng, cuộc sống của họ cũng bình lặng thôi, mặc dù cai nghèo vây quanh nhưng họ có tình người sâu sắc. Những cô cậu bé trong truyện chỉ mới học đến lớp năm tiểu học thôi nhưng mà có những suy nghĩ rất người lớn, có cách hành động độc lập và quyết đoán và có tình người. Phải chăng, chính thời đại đã làm ra con người hay con người đã làm ra thời đại. Đọc truyện, ta thấy váng vất là sự nhớ mong tuổi thơ của tác giả, một tuổi thơ đáng trân quý như mật ngọt. Truyện thật sự rất hay, đôi lúc dí dỏm và đôi lúc khắc khoải nhưng sáng nhất vẫn là tình người của nhứng đứa trẻ nghèo dành cho nhau.
5
184168
2015-09-14 22:12:05
--------------------------
298070
6688
Tình cờ biết đến cuốn này, nghe cái tên đã thích chết đi được xong nhìn cái bìa lại càng mê. Một phong cách rất Việt Nam mà gợi lên cảm giác rất gần gũi thân quen. Chính thế mà không ngần ngại mua về ngay.
Cũng là những câu chuyện về Việt Nam xưa, chính xác cụ thể hơn là Sài Gòn cũ. Cuộc sống khổ có, nghèo có, không no đủ nhưng tình người lúc nào cũng đầy ắp, con người luôn thương mến nhau. Trẻ nhỏ thì luôn giữ được vẻ hồn nhiên vô tư. Vừa đọc mà vừa gật gù thấy quá hay.
Cực kì "recommend" cho mọi người !!!
5
52328
2015-09-12 13:31:10
--------------------------
291341
6688
Mình xem video giới thiệu sách Chú Chiếu Bóng, Nhà Ảo Thuật, Tay Đánh Bài, Và Tụi Con Nít Xóm Nhỏ Sài Gòn Năm Ấy trên web của nhà xuất bản Trẻ lâu rồi, giờ mới có dịp cầm cuốn sách trên tay. Truyện đúng theo lời dẫn "dành cho thiếu nhi, nhưng người lớn đọc cũng hổng sao, mà người già đọc càng khoái". Tác giả mở ra trước mắt người đọc một hình ảnh khác của Sài Gòn những năm 60, với những người nghèo lam lũ nhưng lúc nào cũng giàu lòng thương người, với những triết lý sống "dẫu hèn cũng thể, nát vỏ vẫn còn bờ tre", với những đứa trẻ hồn nhiên, vô tư và dũng cảm. Dù cho cuộc sống có như thế nào, thì tình người vẫn vượt lên tất cả. Một quyển sách đáng để đọc và suy ngẫm!
5
323757
2015-09-06 10:45:36
--------------------------
290479
6688
Mình biết tác phẩm này từ lâu rồi, qua trang giới thiệu của báo Tuổi Trẻ Cười, cũng đã đọc một số truyện ngắn của tác giả Lê Văn Nghĩa, thích giọng văn nhẹ nhàng nhưng pha chút hóm hỉnh. Mọi nhân vật đều được nêu tên trên tên của tác phẩm này, một bức tranh Sài Gòn xưa được tái hiện một cách giản dị và chân thật. Đôi khi bạn tìm thấy chính mình trong những bức tranh ấy. Một Sài Gòn xưa thân thuộc đối với những người con đất Sài Gòn. Sách không chỉ dành cho trẻ em mà đúng là người lớn đọc sẽ càng thích.
5
153585
2015-09-05 13:54:26
--------------------------
289380
6688
Mình đón nhận tác phẩm bằng tâm thế sẽ đón nhận 1 điều thú vị từ lời nói mở đầu của nhà văn Nguyễn Nhật Ánh và báo VN Express. Cái hay, cái đẹp của truyện là sự tái hiện chân thực , sinh động cuộc sống Sài Gòn và trẻ em đô thị thời chiến. Đúng như lời nhận xét của VN Express: “ Thế giới ấy có thể làm độc giả trẻ bật cười và độc giả lớn tuổi bồi hồi…”  Đọc “ Chú chiếu bóng, nhà ảo thuật, tay đánh bài và tụi con nít xóm nhỏ Sài Gòn năm ấy” , cho ta thấy 1 bức tranh cuộc sống rất khác, tràn đầy yêu thương và ăm ắp tình người, mặc cho thiếu thốn, mặc cho những khó khăn cuộc sống vẫn luôn hiển hiện.
5
411440
2015-09-04 14:09:17
--------------------------
280964
6688
Mình sinh ra và lớn lên ở Sài Gòn, tuy không cùng thời với tác giả nhưng khi đọc mình có cảm giác như được sống lại với thời thơ ấu của chính mình. Đây không chỉ đơn thuần là tác phẩm văn học dành cho thiếu nhi mà mọi lứa tuổi đều có thể đọc được và nên đọc. Sài Gòn xưa như hiện lên qua từng trang sách dưới câu chuyện của một đám con nít rất đỗi ngây thơ, hồn nhiên. Đọc tác phẩm này để thấy nuối tiếc và trân trọng hơn quãng thời gian thơ ấu đã đi qua của cuộc đời. 
4
17909
2015-08-28 01:11:10
--------------------------
276667
6688
Ban đầu, tôi mua tác phẩm này chỉ vì nhà văn Nguyễn Nhật Ánh( nhà văn mà tôi hâm mộ)  đã viết lời dẫn cho cuốn sách. Nhưng sau khi đọc xong, tôi thật sự bị cuốn hút và rất tâm đắc với tác phẩm, chỉ muốn cầm lên và đọc đi đọc lại đến khi cảm thấy chán thì thôi. Chỉ với 324 trang thôi, nhưng ta hình dung được hình ảnh con người và  cảnh vật của Sài Gòn năm ấy, cái thời mà còn chiến tranh loạn lạc, còn cái đói cái nghèo. Đặc biệt với giọng văn đặc sệt miền nam, hóm hỉnh, dân dã và chân thật của tác giả nhưng đã dẫn dắt ta đi qua biết bao trạng thái cảm xúc, buồn có, vui có, kể cả nước mắt. Nổi bật hơn hết chính là giá trị nhân văn sâu sắc, nhựng bài học làm người giản đơn mà tác giả gửi gắm qua tác phẩm. Thật là một quyển sách  đáng để đọc và nghiền ngẫm!!!
5
89054
2015-08-24 11:12:42
--------------------------
276107
6688
Đầu tiên phải nói bìa sách rất đẹp, cứng, màu tươi và nổi, tựa sách lại dài ngoằng một cách dễ thương. Nội dung truyện bao gồm nhiều câu chuyện ngắn về lũ trẻ con Sài Gòn những năm về trước, giản dị nghịch ngợm, tinh quái nhưng nhiều lúc vẫn thể hiện những mặt tốt đẹp và dũng cảm. Truyện làm những người lớn như mình gợi nhở rất nhiều những năm tháng ấu thơ, nguyên sơ nhưng lúc nào cũng không thiếu trò để mong đợi mỗi khi ngày mới đến. 
Ai vốn có một tuổi thơ đáng nhớ đều muốn viết nó ra. Và tác giả cuốn sách đã làm được điều ấy.
5
201502
2015-08-23 17:15:59
--------------------------
266094
6688
Ấn tượng đầu tiên của mình về quyển sách này là tựa sách dài một cách thú vị,có bao nhiêu nhân vật đều được nêu lên tựa sách cả, trước đây về dòng sách thiếu nhi mình chỉ quen đọc của Nguyễn Nhật Ánh và một vài sách nước ngoài, đây là lần đầu tiên mình đọc tác phẩm của tác giả Lê Văn Nghĩa, vậy mà mình đã trở thành fan của tác giả hồi nào không biết. Giọng văn chân thực, tình cảm, lối dẫn dắt truyện thì cuốn hút, thú vị khiến cho những trang sách luôn tươi mới, tuy vẫn là những chuyện cũ của sài gòn nhưng qua lời kể của tác giả lại thành những ký ức quý báu không bao giờ cũ. Đây là một quyển sách đáng đọc cho bất kì độ tuổi nào 
5
24682
2015-08-14 13:25:48
--------------------------
252479
6688
Mình đặt mua tác phẩm vì cảm thấy thích thú với tiêu đề. Qủa thật cái tiêu đề đã nêu tên lên hết các nhân vật có trong đó. mình là người Quảng nhưng khi lật những trang sách của tác giả cũng phải gật gù khi tưởng tượng: thì ra Sài Gòn ngày xưa là vậy, thì ra làm xiếc rong cũng có chuẩn mực đạo đức ngề ngiệp của mình, thì ra coi phim rạp ngoài đường phố là vậy..... Tuy đây là một câu chuyện buồn hài hước nhưng mình thấy thỏa mãn với kết thúc của câu chuyện. Mình ngĩ tác phẩm phù hợp vs hầu hết mọi lứa tuổi, nhưng vẫn tuyệt nhất khi bạn là người Sài Gòn vào thời đại ấy đọc tác phẩm và lim dim ngĩ về những ngày tháng xưa cũ.
4
196834
2015-08-03 18:20:25
--------------------------
250021
6688
Tôi hình dung Sài Gòn xưa qua từng trang giấy, vừa đọc vừa gật gù, à té ra trường học này trước kia là nhà biệt thự sao, rồi các nghệ sĩ một thời ngày đó. Câu chuyện không chỉ dừng lại ở chỗ tái hiện được Sài Gòn xưa mà đó là là câu chuyện của đám con nít ngày xưa đó. Vừa đọc tôi vừa nghĩ, con nít ngày xưa ngây thơ thật đó, hồn nhiên quá và rồi lại có cảm giác thật buồn khi đọc đến hết truyện. Cảm giác tiếc nuối, ngỡ ngàng khi đọc đến hết truyện.
4
130360
2015-08-01 09:11:26
--------------------------
238084
6688
Đọc xong quyển sách này, tôi chợt cảm thấy một nỗi buồn nhẹ nhàng. Hóa ra trong cuộc đời ta, có những người bạn thân thiết với ta một cách tự nhiên vô vụ lợi, thế rồi một ngày kia họ biến mất khỏi đời ta, lắng đọng vào một góc khuất trong tâm trí ta, rồi như một lẽ thường của thời gian, ta quên họ đi. TÔi cũng có những người bạn như vậy, có những ký ức mơ hồ, cũng có những điều rõ ràng khi chợt nhớ về họ. Cuốn sách này đã mang tôi về với tuổi thơ không chỉ của những nhân vật trong truyện mà cả tuổi thơ của tôi, là một dòng suy tưởng song song. Khép lại quyển sách cũng là lúc tôi nhớ lại những người bạn của mình, những người đã đi qua cuộc đời mình, để lại những kỷ niệm đẹp, vui nhưng khi nhớ lại thì dâng trào một nổi tiếc nuối lâng lâng.
5
77484
2015-07-22 22:39:40
--------------------------
232554
6688
Tôi đã từng đọc mùa hè năm Petrus ký và bây giờ là chú chiếu bóng, nhà ảo thuật, tay đánh bài và tụi con nít xóm nhỏ Sài Gòn... 1 cuốn sách với tựa đề thật dài. 
Ai cũng bảo tôi đọc truyện con nít, nhưng hãy đọc thử mà xem... Tôi thấy hiện ra nơi ấy là Sài Gòn tuy nghèo khó, nhưng niềm vui con nít ngây ngô luôn hiển hiện trong từng ngõ hẻm, những ước mơ trẻ con thay đổi & lớn lên theo năm tháng... khiến cho người lớn nào cũng mãi nhớ về tuổi thơ của mình.
Sài Gòn đó, mãi còn đây trong trái tim mỗi người chúng ta.
5
89522
2015-07-18 21:21:47
--------------------------
231379
6688
mình rất thích lối hành văn của bác Lê Văn Nghĩa, phóng khoáng nhưng trau chuốt và rất "sài gòn". "Mùa hè năm Petrus" đã đưa mình đến với cuốn sách này. vì là tác phẩm thứ 2 mình đọc của bác, nên mình đã nghĩ có lẽ sẽ nhanh chán thôi, cũng vẫn là giọng văn đó. không có sự biến hóa. thế nhưng cuốn sách đã làm mình bất ngờ. là câu chuyện về những người sống trong xóm nghèo. thế nhưng không hề sầu thương bi lụy. vẫn là chút ngô nghê nghịch, phá, lém lỉnh của những đứa trẻ, là sự tốt bụng chân thành của người sài gòn, nổi bật hơn cả là sự gắn bó keo sơn của những người bạn.
5
9323
2015-07-18 00:11:47
--------------------------
226335
6688
Câu chuyện về tụi con nít xóm nghèo Ba-ra-dô gần Chợ Lớn được thuật lại rất thật và sống động qua tác phẩm truyện dài "dành cho thiếu nhi, nhưng người lớn đọc cũng hổng sao, mà người già đọc càng khoái" của cây bút gạo cội Lê Văn Nghĩa. Như một nhà phê bình nhận xét, ông không chỉ truyền đạt sự việc mà còn tái hiện lại hoàn hảo không khí thời cuộc của đô thị Sài Gòn những năm 60-70 qua ngôn ngữ đối thoại, nét văn hóa pha tạp Ta-Tàu-Tây, qua những cái tên, cái địa danh mà bây giờ cái còn cái mất. Một tác phẩm chắc chắn mình còn muốn đọc lại và viết nhiều hơn về nó.
5
680759
2015-07-11 22:48:10
--------------------------
222663
6688
Tuy không phải là một người Sài Gòn nhưng khi đọc quyển sách này tôi đã trót yêu Sài Gòn mất rồi. Một tuổi thơ vô tư bên cạnh của một cuộc sống khó khăn nơi Sài Gòn cũ. Những đứa trẻ ấy trong sáng, không cần những thứ quá xa xỉ vẫn bên nhau chia sẻ khó khăn. Không biết bây giờ giữa lòng thành phố phồn hoa nhất đất nước liệu có còn tìm lại được những đứa trẻ như Minh, Ti, Chim...hay không. Thật cám ơn tác giả đã để những thế hệ đi sau chúng tôi được yêu một Sài Gòn xưa cũ.
5
572503
2015-07-05 23:41:20
--------------------------
219616
6688
Thật ra mình mua cuốn sách này phần vì tò mò, phần vì bị lôi cuốn bởi câu văn chào đầu hài hước "dành cho thiếu nhi nhưng người lớn đọc cũng hổng sao mà người già đọc càng khoái", đúng là như vậy , tuổi thơ của một đám trẻ con xóm nghèo bỗng chốc trở nên thật sống động với những nhân vật vừa trong sáng tuổi mới lớn, vừa tinh nghịch như Minh,Ti,Chim và những đứa trẻ trong xóm Ba-ra-dô nhờ vào ngòi bút hài hước, dí dỏm của tác giả . Đây là một cuốn sách hay dành cho tất cả mọi lứa tuổi vì đối với trẻ em thì dí dỏm, còn với người lớn thì lại gợi nhắc về những kỉ niệm đẹp trong đời .
5
17907
2015-07-01 18:35:09
--------------------------
212789
6688
Là một người trẻ nên khi nghe qua tựa sách đầy mới lạ và cuốn hút này, tôi cảm thấy khá thích thú và muốn khám phá thêm về hình ảnh Sài Gòn xưa cho nên tôi đã quyết định đặt mua cuốn sách này.Cuốn sách đã vẽ nên trong tâm trí tôi một hình ảnh Sài Gòn xưa mang hơi thở và màu sắc đầy chân chất, mộc mạc, không quá trau chuốt tỉ mỉ. Thế nhưng chính cái mộc mạc, gần gũi ấy đã đem đến một cái nhìn hoàn toàn khác về một Sài Gòn không hoa lệ, rực rỡ.
Đây thực sự là một cuốn truyện hay mọi lứa tuổi đều có thể đọc được.
5
535232
2015-06-22 21:32:33
--------------------------
212700
6688
Điều mà tôi ấn tượng nhất ở quyển sách “Chú chiếu bóng, nhà ảo thuật, tay đánh bài và tụi con nít xóm nhỏ Sài Gòn năm ấy” của nhà văn Lê Văn Nghĩa chính là tựa đề của tác phẩm. Đây có lẽ là quyển sách có tựa đề dài nhất Việt Nam tới thời điểm hiện tại. Quyển sách đã đưa tôi quay ngược thời gian trở về Sài Gòn hơn nửa thế kỷ trước - cái thời mà tụi con nít thích chạy ra đường ngóng trông chú chiếu bóng để được xem chiếu bóng thùng. Bao nhiêu kí ức về Sài Gòn cứ dâng trào trong tôi, đưa tôi về với Sài Gòn với những ngày còn là cậu học trò lớp nhất (lớp 5 ngày nay) với “tụi con nít xóm nhỏ Sài Gòi năm ấy” gồm hơn chục đứa dân quận 6 như thằng Minh, Chim, Long mập, Ti, con Hồng, Út Đẹt… và những người lao động cần cù, bươn chải để kiếm sống như “chú chiếu bóng” Hai Ngon, “nhà ảo thuật” Khổng Có, “tay đánh bài tên” Mùi… Mặc dù lối kể của nhà văn Lê Văn Nghĩa kiểu như nhớ gì kế đấy, không theo thứ tự nhưng qua hơn ba trăm trang sách, tác phẩm đã khắc họa được một cách chân thực và sinh động con người Sài Gòn những năm 1960 – 1970 từ cách ăn mặc, lời ăn tiếng nói hằng ngày đến những hoạt động sinh hoạt cùng nhau. Đồng thời, qua từng trang sách,  tác phẩm còn giúp người đọc biết được, hơn mấy chục năm trước, người Sài Gòn ăn uống như thế nào, ăn mặc ra sao, xem phim gì, chơi những trò chơi nào, ảo thuật đường phố ra sao, dân Sài Gòn thực sự sẽ nói năng như thế nào,… Đọc “Chú chiếu bóng, nhà ảo thuật, tay đánh bài và tụi con nít xóm nhỏ Sài Gòn năm ấy” như là xem lại một cuốn phim đầy chân thực và cảm xúc về thời thơ ấu của mình với những cảnh quay đặc sắc về con người Sài Gòn xưa khiến người đọc không thể bỏ lỡ phút giây nào của cuốn phim và cứ mãi dõi theo cuộc sống tuy có cực khổ vì phải mưu sinh kiếm sống nhưng lại chất chứa đầy niềm vui của con người Sài Gòn.
5
408969
2015-06-22 19:31:51
--------------------------
204077
6688
Đọc để nhớ về tuổi thơ, nhớ nhất là cảnh uống sữa tiệt trùng và bị rượt, cảnh tụi con nít hay đọc mấy bài thơ chế hay chế tên của các nghệ sĩ .
Đọc để được xem lại văn hoá, nếp sống Sài Gòn những năm 60, có những cái còn tới giờ. 
Đọc để thấy sự giúp đỡ đùm bọc của tụi con nít xóm nghèo Ba-ra-dô, thấy sự láu cá và dám nghĩ dám làm của tụi nó. 

Đúng là người lớn đọc cũng chả sao vì trong truyện vẫn có "những người lớn không láu cá" - những người mà tụi nhỏ hâm mộ - dạy bảo tụi nó. Đọc tới phần cuối kể về cuộc sống có hậu của những người lớn khiến mình rưng rưng.

Thích nhất câu "Dầu nghèo cũng thể" của chú Hai Ngon :) Thôi không phá truyện nữa, ai muốn quay về tuổi thơ và xem lại văn hoá Sài Gòn thì truyện này rất đáng đọc nhé!

Đọc sách viết cho con nít thường vui và hay, thấy nhớ. Người lớn đã đọc thì thể nào cũng thấy giá trị nhân văn được gửi vào. Bác Nguyễn Nhật Ánh cũng viết lời giới thiệu cho sách nữa. Nói chung là đọc xong thấy hài lòng, khiến mình lên danh sách cần mua cho tháng sau sẽ phải là Mùa Hè Năm Petrus :D
5
6912
2015-06-03 01:05:53
--------------------------
180182
6688
Tôi không biết những địa danh trong cuốn sách này, nhưng tôi vẫn cảm nhận được nét xưa, giống hệt như những bức ảnh Sài Gòn trắng đen tôi thấy qua báo, ảnh. Tôi không thể có cảm giác quen thuộc, gần gũi, chẳng hạn như Nguyễn Nhật Ánh đã có. 
Giọng văn châm biếm, hài hước, vô cùng ý tứ, thật sự là tôi bị lôi kéo vào quyển sách này, chỉ trong một tối và chiều hôm sau đã đọc xong. 
Nhân vật của ông là những người tốt bụng thật thà, khi đọc tôi đã lo sợ cho họ,  ở trong cảnh nghèo người ta thường khó mà bình đẳng và tôn trọng nhau, tuy nhiên những đứa trẻ luôn được dạy điều hay ý đẹp, thậm chí còn có rất nhiều câu thâm thúy, và các tích trong Cổ học tinh hoa. Tôi chưa từng biết về Cổ học tinh hoa, nhưng sau đọc xong quyển này, tôi hứa hẹn sẽ tìm một quyển CHTH về đọc. Và cũng hứa hẹn tìm đọc hết sách của Lê Văn Nghĩa. 
Có quyển sách Indonesia tựa VN là Chiến binh cầu vồng, 2 quyển này khá là giống nhau về tính cách của mấy đứa nhỏ, và hoàn cảnh cơ cực mà chúng chịu phải. Rất tuyệt vời là cả hai đều rất hay và không khỏi khiến tôi khâm phục, yêu mến, vui vẻ. Tôi chưa bao giờ nghĩ tội nghiệp hoặc thương hại người nghèo cả, kể cả các nhân vật, dù họ có nghèo đến mấy.
5
176413
2015-04-08 20:46:32
--------------------------
164110
6688
Tôi mua cuốn sách này vì đọc được ở đâu đó rằng: nếu muốn được quay lại sống trong không khí của  SG những năm 60 thì đây là cuốn sách giúp bạn làm được điều đó, một phần cũng vì tò mò xem, SG xưa thì "trông như thế nào", khác gì so với SG nay. Và quả thực, tôi đã không nhịn nổi đọc từ từ mà ngốn, ngốn hết cuốn sách chỉ trong vòng 2 ngày. Không phải người SG, nhưng khi đọc sách tôi thấy mình yêu từng ngóc ngách, con người của SG thuở đó... Cười, rồi tưởng tượng ra những trò láu cá của tụi nhỏ trong xóm Ba-ra-dô, rồi ngẫm... Thực sự, cuốn sách đã đọng lại trong tôi rất nhiều điều. Một cuốn sách rất hay, "dành cho thiếu nhi, nhưng người lớn đọc cũng hổng sao, mà người già đọc càng khoái"
5
360715
2015-03-06 20:47:55
--------------------------
160274
6688
Với lối văn dí dỏm, hài hước, tác phẩm đã thu hút người đọc từ những trang đầu tiên. Cuộc sống sinh hoạt của người dân thời xưa được tái hiện một cách sinh động, chân thực nhờ những nhân vật chính là tụi con nít xóm Ba-ra-dô, chú Hai Ngon, con Bông,....những trang sách là những bức tranh muôn màu về đời sống của cảnh Sài Gòn xưa cũ. Dù tác phẩm có một chút vấn đề nhạy cảm mà có thể một số người đọc không hiểu nhưng phải nói thật rằng đây đúng là "truyện dài dành cho thiếu nhi, nhưng người lớn đọc cũng hổng sao, mà người già đọc càng khoái" .....
5
188750
2015-02-23 14:04:42
--------------------------
134979
6688
Một tác phẩm quá tốt, quá hay.
Hay từ đâu? Hay từ từ từng câu, từng chữ, từng ý mà tác giả gửi đến trong tác phẩm.
Tác giả dùng những ngôn ngữ rất bình dân, người đọc có thể cảm nhận được rõ cái thân mật của tác phẩm, cứ như bác Lê Văn Nghĩa đang trò chuyện với bạn.
Một câu chuyện nhiều lúc khiến tôi phá lên cười như điên, nhưng có lúc lại làm lòng chùng xuống vì cách tụi nhỏ đối xử với nhau, việc chú Hai Ngon từ giã thằng Minh vì phải trốn chánh phủ về Bạc Liêu, chuyện tụi con nít xóm Ba-ra-dô giúp đánh cái đơn chuộc thằng Chim khỏi bót cảnh sát,... đã làm tôi rơi nước mắt.

Trừ một vài chi tiết khá nhạy cảm ở đầu truyện (mà tôi tạm gọi là loáng thoáng đâu đây chút 16+ !) thì tác phẩm khá hay, khá hoàn chỉnh. Một Sài Gòn năm xưa đã hiện ra trước mắt tôi.

Một trong những cuốn sách hay nhất về SG mà mọi người nên đọc, từ già đến trẻ, dù là con nít mới học tiểu học.
5
448654
2014-11-12 13:24:44
--------------------------
127759
6688
Tôi mua cuốn sách này ban đầu vì cái bìa đẹp lẫn cái tên hay hay và tò mò không biết cái Sài Gòn xưa cũ nó sẽ hiện ra trong trang sách này như thế nào. Và ngay khi mở quyển sách ra đọc thì đã bị cuốn hút bởi cái lối kể chuyện nhẹ nhàng, hài không chịu nổi và những mẫu chuyện sống động, các nhân vật đầy màu sắc, những câu thoại rất "quỷ kiến sầu" và trên hết là những bài học giản dị nhưng vô cùng thấm thía, cảm động mà người lớn dạy trẻ con cũng như ta học được từ lũ trẻ. Trong đó mình khoái nhất là câu nói "dầu hèn cũng thể mà mậy" của chú Hai Ngon, không hiểu sao nghe câu này dần dần cho đến trang cuối cùng thì xúc động quá chừng chừng. Một Sài Gòn tuy mình không từng sống qua nhưng hiện lên trong sách rất sinh động khiến mình cảm thấy như đang sống ở trỏng vậy. Tóm lại đây là quyển sách hay cho trẻ con, người lớn đọc hổng sao mà người già đọc càng khoái.
5
18366
2014-09-26 16:40:44
--------------------------
122823
6688
Mình thực sự ấn tượng từ tựa đề quyển sách: "Chú Chiếu Bóng, Nhà Ảo Thuật, Tay Đánh Bài, Và Tụi Con Nít Xóm Nhỏ Sài Gòn Năm Ấy", đến khi đọc rồi thì không thể dứt ra khỏi. Biết chú Lê Văn Nghĩa từ ngày còn nhỏ đọc những mẩu truyện ngắn về điệp viên Không Không Thấy trên báo Tuổi Trẻ Cười và ngưỡng mộ giọng văn hài hước nhưng đầy tính châm biếm của chú đến tận khi lớn lên.

Quyển sách kể về bọn trẻ xóm Ba-ra-dô ở Chợ Lớn với 1 tuổi thơ hồn nhiên và tinh thần tương thân tương ái. Những giáo lý của chú Hai Ngon, những bài học làm người của nhà ảo thuật Khổng Có truyền đạt lại cho bọn trẻ hết sức tự nhiên, chân thành và thấm đẫm tính nhân văn. Mình rất thích câu :"Dầu hèn cũng thể" của chú Hai Ngon, dầu mình nghèo, mình có thể bị người ta ghét nhưng không được để người ta khinh. Từng câu văn, từng thông điệp chuyển tải nhẹ nhàng làm người đọc rất thấm. Mình cũng thực sự xúc động trước tình bạn mà bọn trẻ dành cho nhau, đó là 1 tình bạn hồn nhiên, không vụ lợi, 1 tuổi thơ mà bất cứ ai trong chúng ta cũng đã trải qua và luôn hoài vọng khi trưởng thành.

Một quyển sách xứng đáng có mặt trên kệ sách nhà bạn và là một quyển sách hay mà người lớn, con nít đều đọc được.
5
33861
2014-08-28 13:13:01
--------------------------
