463246
5120
Về nội dung & giá trị của cuốn sách quý của dân tộc này, rất nhiều bạn khác đã review kỹ rồi nên mình chỉ xin nói thêm 1 câu: đây là một trong những cuốn sách thuộc hàng tinh hoa của dân tộc Việt Nam mà mỗi con người Việt Nam đều nên đọc, ít nhất 1 lần. 

Vì thích tra cứu & đối chiếu giữa các học giả khác nhau nên mình mua cuốn này cùng cuốn Việt Nam văn hóa sử cương - Đào Duy Anh, Đất lề quê thói và Việt Nam văn học sử lược - Dương Quảng Hàm. 

Sách in giấy rất đẹp, chữ rõ. Khuyết điểm duy nhất là chữ in hơi to & khổ sách quá to. Bù lại bìa thiết kế rất đẹp, rất dân-gian. Nếu được dịch sang tiếng Anh thật tốt thì đây sẽ là cuốn sách mà mình sẽ rất tự hào được giới thiệu với bạn bè năm châu.

5
369572
2016-06-29 09:47:19
--------------------------
460145
5120
Mình đã từng đọc "Đất lề quê thói" của tác giả Nhất Thanh nên hôm nay mình quyết định mua thêm quyển "Việt Nam phong tục" của Phan Kế Bính để so sánh. Nhìn chung thì cả hai quyển đều đầy đủ, được tác giả biên soạn công phu nhưng mình vẫn thích tác phẩm của Phan Kế Bính hơn vì "Việt Nam phong tục" là quyển sách đầu tiên viết về phong tục Việt Nam và cái tên Phan Kế Bính chắc không còn xa lạ gì với mọi người. Cảm ơn tác giả đã mang đến một quyển sách cực kỳ hữu ích.
5
509425
2016-06-26 20:32:05
--------------------------
448862
5120
  Phan Kế Bính là một trong những học giả, văn thân nổi tiếng ở thời buổi cuối thế kỉ 19 đầu thế kỉ 20. Với sự tiếp thu tri thức từ Nho học, Dân tộc và văn minh tri thức của phương Tây(Pháp), lối sống hòa đồng giữa văn hóa dân tộc và văn hóa Âu châu đã cho ông một cách nhìn nhận hài hòa và vốn kiến thức phong phú uyên bác, thâm sâu...
  "Việt Nam phong tục" được trình bày theo lối văn xuôi dễ đọc, khoa học và có tính khái quát cao, phù hợp cho cả học sinh, trí thức, nhà nghiên cứu và cả các tầng lớp bình dân... Người đọc dễ nhớ dễ tiếp cận! Ngoài ra người đọc còn tìm lại được cả về xã hội xưa của cha ông với những thuần Việt cùng với sự giao lưu tiếp thu từ bên ngoài cấu tạo thành phong tục độc đáo của người Việt ta!
    Bên cạnh đó còn có những lời bình, đánh giá mang tính chất khách quan rất cao của tác giả về các phần tư liệu đưa ra. Tác giả không chỉ ngợi ca các nét văn hóa đẹp cần phát huy của dân tộc mà còn phê phán các phong tục lạc hậu, cổ hủ của cha ông không văn minh,khoa học... Đặc biệt ông còn đề cao tinh thần dân tộc trong toàn bộ tác phẩm giúp cho cuốn sách mang nhiều giá trị cho mọi thời đại và là niềm tự hào của cả dân tộc Việt Nam.
   Có thể nói "Việt Nam phong tục" là cuốn sác gối đầu giường cho mọi nhà về văn hóa của dân tộc ta!

5
1168716
2016-06-16 16:32:40
--------------------------
340147
5120
Việt Nam phong tục là một bộ biên khảo khá đầy đủ về phong tục cũ của đất nước ta được Phán Kế Bính viết cách đây một thế kỷ và xuất bản vào năm 1918. Tác phẩm được chia làm ba phần: phong tục liên quan đến gia tộc, phong tục gắn liền với làng xóm và phong tục tiêu biểu cho xã hội. Tác phẩm là nguồn tài liệu phong phú cung cấp cho độc giả, nhất là giới trẻ, nhiều kiến thức quý báu để tìm hiểu về văn hóa dân tộc, từ tục lệ trong gia đình tới thói quen ngoài xã hội, kể cả thuần phong mỹ tục lẫn hủ tục.Phan Kế Bính đã mang đến cho chúng ta một tác phẩm thuần Việt, những phong tục truyền thống ý nghĩa cùng những bài học quý báu thời xưa. Một cuốn sách khắc ghi những giá trị xưa thương mến!
5
634785
2015-11-19 12:23:59
--------------------------
307124
5120
"Việt Nam phong tục" có thể coi là cuốn sách vô giá với mọi thời đại cũng như với mọi lứa tuổi công dân cộng đồng Việt, trong cũng như ngoài nước. Cuốn sách cho độc giả rất nhiều hiểu biết về lối sống, phong tục, ngôn ngữ,.. thay đổi qua từng thời và từng thế hệ một cách rất rõ ràng và sâu sắc. Nói trên phương diện chủ quan, cuốn sách rất hợp với ông bà, cha mẹ mình, cũng như những cụ lớn tuổi khác, nhưng về phương diện khách quan, với giới trẻ ngày nay, cội nguồn chắc chắn là những gì không thể bỏ qua. Cuốn sách tuy có hơi khô khan, nhưng với những trang bìa màu in  lối sống người xưa, cũng như những kiến thức rất bổ ích thì trở nên đáng quý hơn bao giờ hết.
4
537285
2015-09-17 23:00:00
--------------------------
303780
5120
Tôi tiếp xúc với tác phẩm tuyệt vời này một cách rất tình cờ. Bởi lẽ ban đầu chỉ là giúp chú của tôi tìm mua quyển sách này cho công trình nghiên cứu văn hoá phong tục Việt Nam của chú. Thế nhưng khi tò mò được những trang đầu tôi đã bị cuốn hút. Những định nghĩa xen những cảm nhận hết sức tinh tế và thú vị về các phong tục nước ta từ trong gia đình, làng xóm đến ngoài xã hội. Đọc xong sách mà thấy yêu dân Việt, nước Việt đến lạ. Ở đâu, người Việt mình cũng giản dị chân thành giàu tình cảm và ý chí. Tôi yêu nước tôi. 
4
530827
2015-09-15 23:28:24
--------------------------
302263
5120
Một cuốn sách ra đời đã gần 100 năm nhưng những giá trị của nó vẫn trường tồn với thời gian. Đất nước bước sang thời đại mới, phát triển nhanh và hội nhập càng sâu rộng thì chúng ta nên dừng lại và đọc nó. Những phọng tục từ ngàn đời của dân tộc đã được cụ Phan Kế Bính tập hợp lại theo các thể loại một cách tỉ mỉ khá hoàn chỉnh, sau mỗi nội dung là những lời bình sâu sắc của cụ về những cái tốt cái xấu. Hình thức cuốn sách của Nhã Nam thì không còn gì phàn nàn, sách khổ lớn, bìa mềm nhưng khá cứng cáp và đẹp, giấy đẹp, chất lượng in tốt.
5
252046
2015-09-15 08:48:30
--------------------------
297835
5120
Mình đã đọc cuốn "Phong tục Việt Nam" của cụ Phan Kế Bình lâu lắm rồi. Nay Nhã Nam phát hành lại bản mới, đẹp nên mua ngay. Sách có bìa rời, khổ lớn, vài trang hình được in màu, giấy và chữ in rất đẹp. Mình mua tặng ông ngoại và ông rất thích.
Các phong tục trong sách tất nhiên chưa đầy đủ hết sự đa dạng về tập quán ở Việt Nam, nhưng những giá trị, truyền thống được tác giả viết thì đã thể hiện được linh hồn và văn hóa của dân tộc.
Cuốn sách này như một ly trà chiều sau khi đọc các tác phẩm văn học mới hiện đại. Thấm đượm và nhẹ nhàng.
5
551513
2015-09-12 10:31:16
--------------------------
273026
5120
Sách viết rất tỉ mỉ về từng phong tục của cha ông ta ngay cả nguồn gốc tại sao có phong tục ấy cũng được đề cập trong sách, sau mỗi chương, tác giả còn đưa hẳn những điểm tốt, xấu, phong tục nào nên giữ phong tục nào nên bỏ hoặc thay đổi để phù hợp với lối sống hiện đại, thời kỳ hội nhập của Việt Nam.
Sách in rất tốt, không có lỗi đánh áy nào, giấy cũng tốt, ngoài ra còn có mấy trang màu về cách ăn mặc của ông thầy, cưới hỏi, tập quán của người Việt mang tới người đọc một cảm giác gì đấy "rất Việt Nam".
5
550844
2015-08-20 16:49:50
--------------------------
262390
5120
Cuốn sách viết rất chi tiết về phong tục của người Việt Nam cách đây khoảng 100 năm nhưng vẫn còn nhiều giá trị cho lớp trẻ hiện nay. 

Ngày nay, nhiều phong tục đã bị đơn giản hóa hoặc bỏ đi trong suốt thời kỳ đất nước khó khăn đang được thực hiện lại. Cuốn sách có thể coi là lời chỉ dẫn giúp lớp trẻ biết cách thực hiện các phong tục đó theo đúng văn hóa Việt Nam, không thực hiện một cách lung tung và lai tạp.

Cuốn sách cũng giúp tôi hiểu rõ hơn về văn hóa Việt Nam và lối suy nghĩ của người Việt Nam.  
5
586824
2015-08-11 21:27:13
--------------------------
252424
5120
Phong tục, hủ tục của Việt Nam là một đề tài khai thác vô cùng tận mà chỉ có thể tìm về trong sách, đơn giản, thành lũy bề dày bấy lâu của Việt Nam, chỉ có sách vở truyền lại, dân gian kể lại, còn phim ảnh thì trái ngang chưa bao giờ làm được. Quyển sách này hay là ở chỗ dám thẳng thắn nhận xét, phê bình những cái xấu vốn dĩ ăn rễ vào người dân Việt Nam bấy lâu bướng bỉnh, không chịu tiếp thu cái thời đại,  tuyệt nhất là nó được viết từ năm 1915, cái thời đại mà xã hội chưa phóng khoáng như bây giờ, mà tác giả đã có những tư tưởng bức phá và hiện đại như thế, tuy nhiên, cũng vì đó là thập niên xưa nên có những "phong tục" lúc bấy giờ đến thời nay đã trở nên cũ kỹ và có thể quy về thành hủ tục rồi, nhưng có lẽ, chủ yếu là sự xem xét của người đọc dành cho mỗi tư tưởng mà quyển sách đề cập. 
5
9289
2015-08-03 17:22:23
--------------------------
235218
5120
Biết Phan Kế Bính cũng khá lâu rồi, chủ yếu là từ Tam Quốc Diễn Nghĩa với vai trò là một dịch giả.
Cuốn Việt Nam phong tục được biên soạn khá đầy đủ chi tiết, cách xưng hô, các tục cổ, giai cấp,... từ phong tục của gia tộc cho đến phong tục tiêu biểu của xã hội; thậm chí còn so sánh phong tục của hai miền Nam-Bắc. Phan Kế Bính có tư tưởng của một nhà nho hiện đại, thẳng thắn đưa ra nhận xét, có khi là chỉ trích những hủ tục mà đã ăn sâu vào nếp sống của người Việt. Có thể thấy, văn hóa phương Tây phần nào ảnh hưởng đến cách suy nghĩ của một học giả, tiến bộ và văn minh hơn. Cuốn sách không chỉ là ghi chép phong tục Việt mà còn truyền bá quan điểm từ cá nhân tác giả : nên rũ bỏ cái cũ, xấu đồng thời tiếp thu những cái mới có ích hơn . Nói thì có vẻ dễ, nhưng điều này quả thực rất khó với một đất nước có bề dày lịch sử như vậy.

5
401487
2015-07-20 22:26:33
--------------------------
230313
5120
Khi còn đi học mình đã được nghe nhắc đến Việt Nam phong tục, và vì là người hâm mộ bản dịch Tam Quốc Diễn Nghĩa của Phan Kế Bình, mình đã mua quyển này ngay. Đây là một bộ biên khảo khá đầy đủ về phong tục cũ của nước ta được ông biên soạn và viết cách đây một thế kỷ và xuất bản vào năm 1918. Tác phẩm được chia làm 3 phần: phong tục liên quan đến gia tộc (17 mục), phong tục gắn liền với làng xóm (hương đảng) (34 mục) và phong tục tiêu biểu cho xã hội (47 mục). Thầy mình nói đây cũng là tác phẩm biên khảo đầu tiên về loại này do một nhà Nho viết ra, đặc biệt trong hoàn cảnh hết sức thiếu thốn về tài liệu và bản thân tác giả chưa có tinh thần phân tích rạch ròi nên trong phần phong tục xã hội, đặc biệt là tôn giáo khó tránh khỏi điểm chủ quan, nhận xét phiến diện và bị chỉ trích là sai lầm. Tuy nhiên điều này không thể phủ nhận được giá trị của tác phẩm với tư cách là nguồn tài liệu phong phú cho độc giả, cung cấp nhiều kiến thức về văn hóa dân tộc, từ tục lệ trong gia đình tới thói quen ngoài xã hội, kể cả thuần phong mỹ tục lẫn hủ tục. Qua đó ta vẫn thấy được tài năng và một số tư tưởng tiến bộ vượt thời đại của Phan Kế Bính, một học giả đa tài đồng thời có thể xem là "nhà báo" lớn trong thời đại của ông.
5
5412
2015-07-17 13:37:21
--------------------------
171576
5120
Đây là một quyển sách hay, có giá trị cao, dù dc viết cách đây cả trăm năm nhưng nhiều vấn đề trong nội dung vẫn còn nguyên giá trị đến hiện tại.
Tác giả đã phân tích và so sánh nhiều phong tục của hai miền Nam- Bắc và cả của vn- phương tây. Sau những chú giải chi tiết về phong tục, thì theo m thấy, ý kiến của tác giả về việc giữ hay bỏ hoặc đánh giá tốt xấu của phong tục còn hay hơn, bổ ích hơn (^__^)
Quyển sách mang tính chất lịch sử qua giọng văn, cách viết và cả nội dung nữa. M nghĩ ai là người vn cũng nên đọc qua quyển sách này
5
75271
2015-03-22 08:56:41
--------------------------
169649
5120
Việt Nam Phong Tục đích xác là một quyển tư liệu hay và bổ ích, có giá trị lưu giữ và tham khảo lâu đời. Bên cạnh những điều nghiên, phân tích và giới thiệu về những phong tục tập quán của nước ta từ xưa, Cụ Phan Kế Bính còn thẳng thắn và khéo léo đưa ra những nhận xét khách quan, những góp ý chân thành cùng những trăn trở rất thời đại.

Đọc, để hiểu và trải nghiệm một bản sắc rất riêng về văn hóa và phong tục của nước Việt Nam ta từ xưa đến nay, chắc chắn vô cùng lý thú và bổ ích.
5
81526
2015-03-18 18:34:42
--------------------------
156405
5120
Điều giá trị nhất của cuộc sách nằm ở tính thời đại của nó. Sách được cụ Phan Kế Bính cho xuất bản từ năm 1915, đến nay đã trải qua hơn 100 năm mà vẫn thấy như sách mới xuất bản hôm qua.
Những phong tục được giới thiệu trong sách, đa phần còn được lưu giữ đến tận ngày hôm nay. Những bình luận, đánh giá của tác giả tuy là viết về XH VN thế kỷ 19 đầu 20, ta lại thấy nó vẫn đúng trong bối cảnh XH thế kỷ. Những hủ tục vẫn còn sót lại, những bài học kinh nghiệm từ các nước phương Tây vẫn còn nguyên giá trị.
Đã là người VN, tôi nghĩ ai cũng nên đọc cuốn này.
5
6742
2015-02-04 18:00:44
--------------------------
152014
5120
Đây là một cuốn sách hay và bổ ích, nó tái hiện lại xã hội Việt Nam chủ yếu vào thế kỉ 19, đã bắt đầu được thổi vào nền văn minh phương tây, những tập quán ngàn đời bắt đầu thay đổi. Phan Kế Bính viết ngắn gọn, súc tích, dễ hiểu. Ông có tư tưởng rất hiện đại chứ không giống như các nhà nho cũ. Ông thẳng thắn lên án các hủ tục, bảo vệ phụ nữ, ca ngợi những cải cách tiến bộ, tôn vinh những truyền thống tốt đẹp cuả nhân dân ta. Tác phẩm này không chỉ giúp chúng ta có thêm hiểu biết về lịch sử mà còn nhận thấy tình yêu nước cuả tác giả trong thời kì bị ngoại xâm
5
403246
2015-01-21 19:28:18
--------------------------
151571
5120
"Việt Nam phong tục" thật là một quyển sách hay và chứa nhiều điều bổ ích. Những phong tục, nét truyền thống xưa cũ được tái hiện, kể lại một cách đầy sinh động và chi tiết. Quyển sách như những điều mới mẻ đối với thế hệ trẻ hôm nay như mình, những con người không còn được thấy nữa những phong tục truyền thống của dân tộc vì sự phai tàn dần theo thời gian. Tác giả không chỉ cho ta cái nhìn đầy cụ thể, chi tiết mà còn truyền tải những bài học về lịch sử đất nước, nhắc nhở chúng ta ghi nhớ cội nguồn của mình. Quyển sách thực sự rất cần thiết cho các bạn trẻ để hiểu hơn những giá trị truyền thống!
4
442739
2015-01-20 11:33:52
--------------------------
