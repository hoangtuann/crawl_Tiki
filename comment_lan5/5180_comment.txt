276839
5180
Câu chuyện về các nàng công chúa của Disney từ lâu đã rất nổi tiếng với các em nhỏ và cả người lớn. Đọc cuốn sách này ta thấy một vùng trời tuổi thơ ùa về. Cuốn sách kể về câu chuyện người đẹp và quái thú - một câu chuyện đã rất nổi tiếng nhưng được viết lại dưới hình thức khác. Câu chuyện trong cuốn sách giúp các bé và cả người lớn nuôi dưỡng tình yêu giữa con người và con người, giữa con người và động vật. Rất cần thiết để giúp nuôi dưỡng tâm hồn trong sách của trẻ thơ.
5
300425
2015-08-24 13:41:21
--------------------------
174374
5180
Phải nói đây là câu chuyện tình yêu kinh điển của hãng phim hoạt hình Disney. Chưa cần phải coi phim, người xem cũng bị ấn tượng bởi hình vẽ rất đẹp và sống động đặc biệt là chàng quái vật. Câu chuyện là hành trình đi tìm tình yêu đích thực của chàng quái vật, trải qua bao khó khăn và thử thách, chàng đã tìm được người con gái mà mình yêu và trở thành người. Câu chuyện phải nói là rất cảm động và ý nghĩa với những người yêu nhau thật sự, dù là không gian hay thời gian họ đều sẵn sàng vượt qua, bất kể người đó xấu hay không phải là người. Bối cảnh hợp lí và cốt truyện hay đã tạo nên thành công cho tác phẩm. Đỉnh của đỉnh!
5
13723
2015-03-27 21:23:12
--------------------------
