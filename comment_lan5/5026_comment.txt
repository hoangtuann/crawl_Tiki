201842
5026
Hiện nay, với xu thế phát triển của toàn cầu, dường như chúng ta ai cũng bận rộn với công việc, hội họp hoặc có thể vì một lí do nào đó mà chúng ta mất đi sự quan tâm đến những người thân của mình, đặc biệt là mẹ của chúng ta. Cuốn này làm tôi nhớ đến mẹ của mình biết bao, tôi chợt nhớ rằng mình chưa làm gì được cho mẹ cả. Xin cảm ơn tác giả rất nhiều, có lẽ không chỉ riêng mà tất cả những ai nếu có cơ hội đọc được cuốn sách quý giả này cũng sẽ không khỏi xúc động và cũng sẽ không ngừng cảm ơn tác giả. Sự yêu thương của mẹ, sự hi sinh của mẹ, sự bao dung của mẹ cả đời này chúng ta không sao đền đáp được. Mong rằng những ai đang còn mẹ trên đời hãy yêu thương, trân quý và quan tâm đến cha mẹ của mình nhiều hơn.
5
455308
2015-05-28 13:04:49
--------------------------
