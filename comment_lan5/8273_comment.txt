462499
8273
Thấy trên tiki giảm 40% nên mình cũng tò mò vào phần đọc thử xem thế nào. Có những điểm cần nói về sản phẩm sau đây:
- Sách chia làm 16 chủ đề quen thuộc trong IELTS. 
- Trong mỗi chủ đề lại chia nhỏ làm 3 phần: Vocabulary, Speaking và Listening
- Phần Vocabulary rất hay và quen thuộc ở Việt Nam, có cả nghĩa tiếng Việt, ví dụ như tourist trap là nơi du lịch chặt chém, hoặc calligraphy là thư pháp,v..v...
- Sau phần Vocab có phần practice và tiếp là phần Speaking để thực hành những từ vựng trên.
- Sản phẩm được đóng gói cẩn thận, chu đáo.
Nói chung rất ưng sản phẩm này. Cám ơn Tiki.

5
492684
2016-06-28 17:05:04
--------------------------
