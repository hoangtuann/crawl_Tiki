493242
4796
Mình đặt hàng quyển này vào ngày 25/11 và nhận được vào 28/11, nhưng khi nhận được sản phẩm thì rất thất vọng khi sách xước và nhìn bìa sách nham nhở, nhìn rất mất cảm tình, cũng chẳng muốn đổi trả vì rất mất thời gian
1
1850290
2016-11-29 08:46:52
--------------------------
492628
4796
Mình mua cuốn sách này từ hơn 1 năm trước, khi đó còn là sinh viên năm 2. Thầy Tuấn viết rất cụ thể, chi tiết nên mình dù chưa biết gì nhưng cũng có thể follow theo từng hướng dẫn của Thầy. Sau này mình biết Thầy còn làm cả album clip hướng dẫn trên youtube nữa nên càng trực quan. Kinh nghiệm của mình khi làm việc với R là phải thật bình tĩnh, khi gõ lệnh mà không chạy được thì cứ bình tĩnh lại tra lại sách xem sai chỗ nào. Thường thì mình tự mày mò cũng ra. Thỉnh thoảng mới phải đi hỏi ý kiến chuyên gia.
5
641609
2016-11-25 17:29:31
--------------------------
467329
4796
Được biết phần mềm R thông qua các bài giảng của thầy qua internet. Thực sự rất vui khi được cầm cuốn sách này trên tay. Nó thực sự bổ ích không chỉ cho các bạn chuyên nghiên cứu, mà còn đặc biệt hữu ích cho các bạn không chuyên về nghiên cứu vẫn có thể áp dụng thực hành để làm luận văn, luận án vì Thầy hướng dẫn rất tỉ mỉ, dễ hiểu. Phần mềm này được download sử dụng miễn phí không như một số phần mềm khác như eview. Đặc biệt nó hỗ trợ vẽ biểu đồ rât đẹp. Hơn hẳn SPSS, Stata. Giá sản phẩm bên Tiki rẻ hơn bên nhà xuất bản tphcm trên đường Minh Khai, bữa mình định mua online bên đó mà so với Tiki thì mắc hơn nên quyết định order bên Tiki. Một điểm cộng nữa cho Tiki là gói hàng cẩn thận, giao hàng nhanh.
5
1243686
2016-07-03 17:38:57
--------------------------
413856
4796
Các giáo trình về hướng dẫn sử dụng các phần mềm trong phương pháp nghiên cứu khoa học định lượng hiện nay đã có khá nhiều tại Việt Nam. Tuy nhiên, sách viết chuyên sâu về hướng dẫn sử dụng phầm mềm R trong công tác nghiên cứu khoa học bằng tiếng Việt thì đây là tác phẩm duy nhất.Phần mềm R có ưu điểm là rất dễ sử dụng, miễn phí nên khâu cài đặt rất đơn giản. Phần mềm R có thể thực hiện nhiều phương pháp phân tích hữu ích như: hồi quy, phân tích chuỗi, phân tích sống còn, ... Tuy không mua Phân Tích Dữ Liệu Với R trên tiki do lúc mua thì trên tiki đang tạm thời hết hàng nhưng hi vọng những nhận xét của tôi sẽ giúp ích cho những bạn có nhu cầu mua do trên tiki sách được chiết khấu cao hơn nên sẽ tiết kiệm được phần nào.
4
984574
2016-04-10 14:20:56
--------------------------
343896
4796
R là một ngôn ngữ phổ biến trong giới data analyst, do ưu điểm vượt trội của nó so với các phần mềm khác là sự miễn phí. Tuy nhiên, không phải của rẻ là của ôi, trái lại R còn rất hữu dụng và mạnh mẽ không thua kém các phần mềm đắt tiền như SAS hay SPSS. R hiện đang được sử dụng rộng rãi trong các trường học, viện nghiên cứu hoặc những công ty startup, nơi mà chi phí bản quyền thường rất eo hẹp. Hiện tại, quyển sách này là tài liệu tiếng Việt duy nhất viết về R. Do đó, với những ai quan tâm tới R thì đây là một quyển sách đáng tiền tới từng xu lẻ.
5
270700
2015-11-27 13:36:32
--------------------------
312030
4796
Đối với một sinh viên như mình bỏ ra gần 200 nghìn mua sách có phần hơi quá sức. Mình đã đắn đo rất lâu mới quyết định mua cuốn này. Sau khi nhận sách, mình thấy mua nó là quyết định đúng. Cuốn sách khá to, chất lượng giấy và chất lượng in đều rất tốt. Còn về nội dung, đây là sách của thầy Tuấn mà nên nội dung khỏi chê. Sách viết rất đầy đủ và dễ hiểu, hơn hẳn nhiều cuốn về thống kê và phân tích khác, rất phù hợp cho những người tự học. Mình đánh giá sản phẩm 10 điểm.
5
274499
2015-09-20 16:48:03
--------------------------
170889
4796
Tôi được biết phần mềm R từ GS. Nguyễn Văn Tuấn thông qua cuốn sách "Phân tích số liệu và tạo biểu đồ bằng R", đây là cuốn sách khá cơ bản và dễ hiểu đối với tôi khi tiếp cận công cụ R trong việc học tập và nghiên cứu.
Đợt này, tôi cảm ơn tiki giới thiệu và tạo điều kiện để tôi có thể mua cuốn sách mới nhất của GS. (hic, giá bìa khá cao so với mặt bằng chung các sách hiện nay?!)
Và hi vọng, tôi sẽ học hỏi thêm những điều bổ ích từ cuốn sách này.
4
17456
2015-03-20 22:02:12
--------------------------
