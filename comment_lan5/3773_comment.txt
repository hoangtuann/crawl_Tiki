462508
3773
Cuốn sách Real lives Real Listening này mình đã thích từ lâu nhưng mà chưa có dịp mua hết cả 3 cuốn. Mới mua thêm cuốn này nên mình rất thích, mà được giảm giá nhiều nữa. Nội dung sách đều xoay quanh 15 chủ đề quen thuộc, gần gũi trong cuộc sống. 15 chủ đề này sẽ đi xuyên suốt trong 2 cuốn level b1 và c1 nên việc học sẽ được hệ thống hơn. Bài nghe khá đơn giản, phù hợp với trình độ A2.
Sách được đóng gói cẩn thận, chu đáo. Giấy sách đẹp, nhìn thích mắt.
Mong Tiki có nhiều cuốn sách phù hợp như vậy, và đặc biệt có thêm nhiều cuốn sách ở trình độ A1.

5
492684
2016-06-28 17:16:26
--------------------------
308635
3773
Mình đang rất cần tìm những quyển sách hay của các nhà xuất bản nổi tiếng như Cambridge, Oxford, Collins... Và thật hay là tìm được cuốn này trên Tiki, vừa đỡ lo không phải sách lậu, vừa rẻ hơn giá thành trên thị trường. Tuy nhiên thì với nhà xuất bản, mình muốn sách giữ nguyên bản quyền của Collins thay vì in vào đó là các nhà xuất bản ở Việt Nam. Về nội dung thì thật tuyệt vời. Các bài nghe, kĩ năng rất xứng đáng với từ Real lives, Real listening. Bài nghe rất thực với đời sống. Vì vẫn đang ở trình độ non của A2 nên mình thấy có vẻ như bài nghe này hơi khó thì phải. Qua sách này, mình đã cải thiện nhiều về kĩ năng nghe cũng như về vốn từ vựng. Hi vọng, Tiki có thể ra thêm nhiều loại sách mới của những nhà xuất bản nổi tiếng khác.
5
588562
2015-09-18 20:03:24
--------------------------
