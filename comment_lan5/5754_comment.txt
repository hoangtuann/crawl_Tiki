330174
5754
Người xưa có câu: “đồng tiền to như bánh xe bò”, đó là câu nói dành cho những người xem trọng đồng tiền. Qua câu chuyện này, đồng tiền không phải là tất cả mà những gì quý giá nhất chính là những điều đơn giản xung quanh chúng ta. Câu chuyện hay và thật ý nghĩa, thật tiếc cho người chồng, vì quá muốn giữ của của mình mà đuổi vợ đi, giữ tiền làm gì khi bên cạnh mình không còn một ai. Mong rằng nxb sẽ cho xuất bản những cuốn sách hay như thế này để các bạn trẻ hiểu về giá trị đồng tiền.
5
13723
2015-11-02 08:46:00
--------------------------
