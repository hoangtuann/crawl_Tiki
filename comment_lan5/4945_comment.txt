409046
4945
Khi tôi bị stress là lúc tôi mua cuốn sách này. Ngay tựa đề " Không gục ngã" đã thu hút ngay từ cái nhìn đầu tiên với tôi. Cuốn sách nhỏ nhắn, xinh xắn để bỏ trong túi mọi lúc mọi nơi. Nội dung trình bầy ngắn gọn, có những vần thơ ru êm trái tim mệt mỏi và những câu chuyện, những lời khuyên thấm thía từ tác giả khiến chúng ta nhận ra nhiều sự thật về cuộc sống. Để rồi từ nơi ta gục ngã, ta lại đứng lên một lần nữa. Giống như miếng sắt rèn trong ngọn lửa, càng rèn càng nhẵn nhụi, " chắc chắn và bền hơn".
5
956786
2016-04-01 20:13:30
--------------------------
403175
4945
Tôi ấn tượng bởi cái tên sách nên đã mua.
Khi cầm quyển sách, tôi thấy nó hơi nhỏ so với những gì mình nghĩ, bên cạnh đó, tiki bọc quyển sách này của mình không đẹp, bị cong ( do bọc bé hơn sách ) làm mình không hài lòng. Nội dung bên trong cũng gọi là tạm được. Từng mẩu chuyện nhỏ góp thành ý nghĩa lớn, sức mạnh lớn. Sau mỗi câu chuyện lại có một câu danh ngôn, tôi rất thích điều này.
Tuy hơi buồn vì bọc sách không như ý nhưng tôi đánh giá cao nội dung sách.
3
349844
2016-03-23 12:51:02
--------------------------
391826
4945
Nơi con người gục ngã sẽ là nhưng ngã ba của cuộc đời, học là đứng lại và làm nên cuộc đời huy hoàng hoặc cuối mình làm người tầm thường. Cuốn sách như một người thầy với những khuyết danh và bài học dạy bảo ta mỗi ngày. Những dòng chữ ngắn gọn nhưng sắc xảo. Nỗ lực của con người là vô hạn, vì thế hãy tận dụng tiềm năng của chúng ta, tiềm năng nay có thể khiến ta mở ra con đường thành công, con đường của sự sung túc. Đừng gục ngã, hãy đạp khó khăn mà đi lên.
4
828378
2016-03-05 21:07:22
--------------------------
360242
4945
     Cuộc sống là những trải nghiệm, là con đường không bằng phẳng mà mỗi con người phải đi qua. Sẽ có lúc ta vấp vào đá mà ngã, cũng có khi giẫm phải gai, hay là đi vào vũng bùn lầy lội. Nhưng dù đường có khó đi như thế nào, cũng đừng có gục ngã. "Không gục ngã"-ai nói bạn không được ngã xuống(mà dù bạn không muốn thì vẫn sẽ phải ngã), gục ngã ở đây lại khác, đó chính là sự gục ngã của ý chí. Dù bạn có thất bại bao nhiêu lần nhưng nếu không từ bỏ, thì bạn đã không ngã.
     Không có con đường trải đầy hoa hồng nào dẫn đến thành công cả. Bạn phải ý thức được điều đó và lựa chọn cho mình những bài học đúng đắn.
     "Không gục ngã"-cuốn sách bỏ túi được thiết kế nhỏ gọn, là tập hợp của những câu nói, câu danh ngôn hay và ý nghĩa. Hãy mang nó bên mình, để khi bạn mệt mỏi trước những thách thức trong cuộc sống thì còn có nó-người bạn, cũng như người thầy sẽ dạy cho bạn cách lấy thêm dũng khí để đi tiếp trên con đường mang tên "cuộc sống".
4
857178
2015-12-28 21:27:53
--------------------------
335315
4945
Đúng như tựa đề, nội dung bên trong của nó là những câu chuyện về những con người đứng vững trong giông tố của cuộc đời. Tập sách khổ nhỏ chỉ vỏn vẹn có hơn 70 trang, rất mỏng và nhẹ, nhưng nội dung ý nghĩa của nó thì không hề "nhẹ". Không chỉ là "không gục ngã" mà nó còn cho mình những bài học khác về cách thưởng thức hương vị của cuộc sống và đặc biệt tâm đắc với câu: "luôn có một con gấu đuổi theo sau lưng và một con gấu cản lối đi phía trước bạn; rồi thỉnh thoảng bạn cũng gặp những chú chuột đáng ghét cứ muốn phá bĩnh sợi dây an toàn mà bạn đang níu giữ. Nhưng bất kể tất cả, những trái dâu ngọt lành nhất vãn sẽ chờ bạn trên suốt hành trình, chỉ cần bạn biết tin và thật sự sống với niềm tin của mình."
5
919635
2015-11-10 21:20:59
--------------------------
327895
4945
Mình sở hữu cuốn sách này một cách hơi dị. Một lần đi lên trường cấp 3 hoạt động phong trào gì đó thì lúc về nhìn thấy cuốn sách trong giỏ xe đạp. Khá bất ngờ và không tìm ra chủ nhân nên mình mang về và đọc thử. Sách rất giàu chất thơ, dù đã dịch ra tiếng Việt. Mình nghĩ dịch giả đã dịch rất tốt. Tuy ngắn gọn nhưng giàu cảm xúc và cả nghị lực. Nền sách được trang trí đẹp, có viền và hoa, nhìn rất thích. Cuốn sách tình cờ này làm mình nhớ tới mỗi lần gặp khó khăn và tự nhủ rằng mình không được gục ngã. Hãy dành tặng cuốn sách này cho những người bạn yêu thương.
4
39590
2015-10-28 17:38:55
--------------------------
325941
4945
Mình rất thích đọc hạt giống tâm hồn,tình cờ thấy cuốn sách với tựa đề 'không gục ngã' nên mình đã đặt mua .Và mình thấy rất hữu ích và không uổng phí khi mua cuốn sách này.Bìa sách phía ngoài như thể hiện giông tố của cuộc đời vậy,rất tinh tế.Thể hiện bên trong cuốn sách là những câu chuyện ngắn mà sâu sắc cùng những câu danh ngôn ,ngạn ngữ đầy tinh tế gắn liền với những câu chuyện .Khi đọc  chúng ta có thể nghiền ngẫm và tự liên hệ với chính mình.Trong cuộc sống chúng ta không ít lần đối diện với thất bại,đau khổ ,khó khăn .Cuộc sống đâu phải toàn màu hồng ,chúng ta cần phải đối diện với những khó khăn thất bại để trưởng thành hơn,và qua đó chúng ta cũng học hỏi thêm nhiều điều hay.Nghệ thuật sống không phải thể hiện ở việc bạn tìm cách chối bỏ khó khăn mà là ở việc bạn học cách trưởng thành từ những khó khăn đó.
5
883074
2015-10-24 16:29:09
--------------------------
314963
4945
Cảm nhận đầu tiên là thiết kế đẹp, trang nền hoa văn. Nội dung mang nhiều ý nghĩa sâu sắc từ những bài học cuộc sống, giúp cho con người vượt qua mọi thử thách, cuộc sống vốn dĩ đã không màu hồng thì con đường nào bạn đi cũng đầy chông gai và nhiều thử thách nên để vượt qua thì bạn nên đón nhận và tiếp bước,  "khi một cánh cửa khép lại sẽ có một cách cửa khác mở ra. Nhưng chúng ta thường tiếc nuối ngoái nhìn lại cánh cửa đã đóng mà không nhận ra rằng cánh cửa đang mở kia là để chào đón mình "
5
773863
2015-09-27 20:43:15
--------------------------
248439
4945
Mình đặt mua sách này khi tâm trạng mình cực kỳ tồi tệ và không ổn định, lúc đó mình gặp stress, áp lực đủ phía từ công việc và cuộc sống nên mình tưởng chừng như gục ngã vậy. Rồi đột nhiên mình nghĩ " Tại sao không tìm mua một cuốn sách để vực dậy tinh thần của mình nhỉ?" Vậy là mình quyết định mua sách " Không Gục Ngã" và kết quả là rất hài lòng với những lợi ích quý báu mà sách mang lại cho mình. Sau khi đọc và nghiền ngẫm nội dung của sách, mình cảm thấy tinh thần ổn hơn rất nhiều. Theo mình thì sách này có nội dung ngắn gọn nhưng cũng thật ý nghĩa. 
5
538881
2015-07-31 09:58:11
--------------------------
