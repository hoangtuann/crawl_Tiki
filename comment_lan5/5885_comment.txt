452024
5885
Đây là tác phẩm thứ 3 của nhà văn Nguyễn Huy Tưởng mà mình từng đọc. Vẫn còn nhiều tác phẩm của ông mà mình tìm được, dù ông viết phần nhiều cho thiếu nhi. Về nội dung, có lẽ ai cũng biết đến Huyền Trân Công chúa nhưng ít ai biết được nhà Trần còn có một vị Công chúa nữa cũng nổi tiếng không kém - đó là An Tư công chúa. Cả 2 vị công chúa nổi tiếng nhất lịch sử nhà Trần vì các cuộc hôn nhân mang tính trọng đại. Về hình thức, cuốn sách khá mỏng, in rất đẹp, giấy màu vàng ngà dễ đọc; tiki giao hàng khá nhanh và gói cẩn thận. Còn về văn phong của Nguyễn Huy Tưởng thì khỏi phải chê. Tuyệt vời!!!
5
634855
2016-06-20 12:58:09
--------------------------
308708
5885
Đây là câu chuyện về một nàng công chúa vô cùng dũng cảm và cũng vô cùng đáng thương của triều đại nhà Trần. Xuyên suốt câu chuyện là số phận đầy đau khổ chứa đầy nước mắt của nàng công chúa xinh đẹp nhưng bất hạnh. Nàng đã vì triều đại nhà Trần, vì con dân Đại Việt mà từ bỏ tình yêu của mình để dấn thân kết hôn với giặc ngoại xâm. Những ngày tháng đau buồn khi sống bên tướng giặc Thoát Hoan chẳng khác nào chết nhưng nàng vẫn gắng gượng sống vì lời hứa với người mình yêu. Nhưng cuối cùng khi chiến thắng cũng là lúc người nàng yêu ra đi cuối cùng nàng công chúa quá đau buồn đã gieo mình vào những cơn sóng biển để giữ trọn lời thề với chàng. Phải cảm ơn nhà văn Nguyễn Huy Tưởng rất nhiều vì đã viết ra một tác phẩm rất hay về số phận một nàng công chúa dũng cảm trong lịch sử Việt Nam!
5
143101
2015-09-18 20:31:39
--------------------------
188357
5885
Nguyễn Huy Tưởng là nhà văn chuyên viết truyện cho thiếu nhi. Câu chữ trong các sách của ông giản dị, gần gũi với cả người lớn và trẻ em. An Tư là một công chúa đời Trần, thông quan câu truyên của công chúa An Tư nhà văn Nguyễn Huy Tưởng đã diễn tả rất sống động, chân thực cuộc chiến tranh bảo vệ tổ quốc của quân dân nhà Trần mà đằng sau đó là những hi sinh của những người mẹ, người vợ cho các chiến sĩ ra trận. Công chúa An Tư là một điển hình như thế.
5
504882
2015-04-25 10:06:41
--------------------------
