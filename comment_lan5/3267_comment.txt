253970
3267
Mình đã được ba mẹ kể rất nhiều về những ngày  ăn bo bo, gạo hòa lan (cơm trộn khoai lang) ăn độn, khó khăn. Quyết định mua quả thật không sai. Bìa sách đẹp, bên trong có những hình ảnh về những con người đã chạy đôn đáo vì hạt gạo của người dân thành phố một thời. Rất cảm ơn nhà báo Hoài Bắc đã viết ra cuốn sách này. Cốt truyện là những ngày tác giả đi theo người trong công ty làm việc, những lời kể thật thà, cả những khó khăn như làm việc giúp dân cho nhà nước mà lại bị cảnh sát bắt vì lúc đó các tỉnh chưa liên kết chặt chẽ với nhau để mang hạt gạo về cho dân, và nhiều, nhiều chuyện khác nữa. May mắn thay công ty đã làm ăn rất phát đạt và hiện giờ tuy cô Ba Thi đã ra đi nhưng tinh thần của bà vẫn còn trong tất cả những người làm lương thực hôm nay.
5
533241
2015-08-04 21:47:46
--------------------------
