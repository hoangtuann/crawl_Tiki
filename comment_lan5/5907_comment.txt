375542
5907
Truyện ngắn được viết chưa sâu sắc , cảm nhận còn thiếu sức sống làm cho mọi người phải suy nghĩ lâu xa , viết còn nhiều lạc điệu và dài dòng nên không khiến câu truyện có sức lắng đọng còn tồn tại trong mỗi bước đi của diễn biến , nội dung không có miêu tả được phần lớn những coi trọng trong bản tính chưa thấy có đủ những sức gợi cảm trong vần chữ , hơi gượng gạo giữa hoàn cảnh và thực tế , nên chỉ thể làm văn ngắn hạn , trích dẫn hay .
3
402468
2016-01-28 16:54:26
--------------------------
344571
5907
Về hình thức.của truyện : bìa truyện khá đẹp,đựơc thiết kế tỉ mỉ,đựơc bọc plastic vừa khít với cuốn truyện.Nói chung là về hình thức cuốn truyện đựơc đầu tư khá là kĩ càng,tạo ấn tượng tốt cho người đọc.
Về nội dung của truyện:  những câu chuyện chân thật,những cảm xúc,sự tốt đẹp và mặt trái ngựơc của tình yêu đựơc thể hiện khá là rõ ràng. Những câu nói khiến ta ngỡ ngàng nhận ra những thứ trong tình yêu.Có thể sẽ bị phản bội,bị tổn thương khiến chúng ta không dám giành hết con tim mình để yêu.Nhưng hãy bỏ qua mọi sự ích kỉ,tự lừa dối mình để bắt đầu một câu chuyện mới
3
917899
2015-11-28 18:53:48
--------------------------
226522
5907
Tuy không phải là người thường xuyên đọc ngôn tình nhưng mình vẫn thấy tác phẩm khá hay, nội dung ổn, cốt truyện tự nhiên nhưng đôi khi hơi lan man. Những câu chuyện đa số nói về những tình yêu không thành hay còn dang dở. Yêu một nửa tim thôi như nói đến chứng bệnh " sợ yêu " khi bị tổn thương, nói chung là, sau khi mua tác phẩm, mình đánh giá tác phẩm ở mức bình thường hơn là đem lại nhiều cảm xúc, hi vọng rằng các tác phẩm sau sẽ tuyệt vời hơn!
4
539083
2015-07-12 12:58:33
--------------------------
131287
5907
Ấn tượng về cuốn sách này do là những câu trích dẫn hay, bài thơ và bìa của nó, còn về nội dung thì lại hơi nhàm chán. Tất cả những truyện trong tác phẩm nếu không ngắn quá mức cần thiết, cốt truyện sơ sài, miêu tả nội tâm nhân vật kém sâu sắc, tình tiết diễn ra quá nhanh thì miêu tả nội tâm quá nhiều đến mức dài dòng, lan man, tình tiết kéo dài lê thê nhàm chán, cốt truyện bình thường không nổi bật, sâu sắc hay có tính sáng tạo. Về mặt văn phong thì một số tác giả có văn phong khá tốt và cốt truyện tương đối hay thì một vài tác giả còn lại cách hành văn còn khá non, lập từ nhiều và cốt truyện dài dòng. Tóm lại ngoài cái tựa đề hay ra thì 14 câu truyện ngắn trong tác phẩm này không thực sự nổi bật, hay ho và đáng để đọc.Thật uổng phí khi mua tác phẩm này.
3
401466
2014-10-23 21:04:14
--------------------------
