458473
8311
Mình đã mua cuốn sách này. Mua về được mấy hôm đã thấy con trai khoe là "con đọc hết cuốn sách rồi mẹ ạ". Sách viết về những câu chuyện thường ngày, mỗi một câu chuyện lại có một bài học rút ra cho các bé học tập. Mình thấy sách rất có ý nghĩa trong việc rèn luyện tính cách của các bé, nhất là khi các bé đang ở độ tuổi thích bắt chước. Bé thứ hai nhà mình tối nào cũng đòi mẹ đọc cho một vài câu chuyện mới chịu đi ngủ. Mình thấy thật tốt khi có cuốn sách này.
4
1409868
2016-06-25 10:08:52
--------------------------
210861
8311
Mình đã mua cuốn sách này. Là cuốn sách rất có ích với những câu chuyện mang ý nghĩa về những đức tính tốt dành cho bé nghe trước khi ngủ, mỗi câu chuyện là một đức tính mà bé học được. Bé nhà mình sau khi nghe kể rất thích vì có những hình ảnh đáng yêu và nhân vật ngộ nghĩnh, thêm vào đó, những câu chuyện đó còn giúp bé nhà mình biết yêu thương hơn, không chê bai người khác, bé đã biết giúp đỡ mẹ …Với những câu chuyện ngắn còn giúp bé dễ hiểu với chuyện mà cuốn sách mang lại. Mình rất hài lòng!
5
469469
2015-06-19 21:29:59
--------------------------
