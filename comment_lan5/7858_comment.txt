292281
7858
Với những nhân vật như chú sâu, muỗi, bọ rùa... tưởng như rất xấu xí đáng sợ, nhưng qua những nét vẽ ngộ nghĩnh, đáng yêu cùng một câu chuyện tuy đơn giản nhưng mang một ý nghĩa nhân văn sâu sắc chắc chắn sẽ khiến bé yêu của bạn yêu thích cuốn sách này và bắt bạn kể đi kể lại câu chuyện đến thuộc lòng thì thôi. Chính điều đấy sẽ là cơ hội rất tốt để bé có tiền đề để xây dựng nhận thức đúng đắn, biết yêu cuộc sống, biết làm những điều tốt cho cuộc đời.
3
29716
2015-09-07 10:11:25
--------------------------
