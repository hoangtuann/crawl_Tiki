160527
5806
Cuốn sách "Mẹ giỏi con cũng giỏi" khá hữu ích đối với các bậc cha mẹ. Nội dung sách đưa ra các ví dụ  và cách giải quyết cụ thể đối với từng trường hợp  mà các bậc cha mẹ hay gặp phải với con cái mình.  
Tuy nhiên, theo tôi thấy lời văn và cách diễn giải còn dài dòng  làm cho người đọc cảm thấy còn bối rối và khó có thể nhớ để áp dụng  được.  Cuốn sách  có khá nhiều nội dung hữu ích nhưng phải ai kiên trì lắm thì mới có thể đọc hết được. 
Nhưng đây cũng là cuốn sách mà các bậc cha mẹ nên có để có thể nuôi dạy con cái mình tốt hơn
3
506959
2015-02-24 17:20:44
--------------------------
