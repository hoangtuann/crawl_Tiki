554813
6621
Nội dung cuốn sách thì rất hay
Nhưng tiki nhớ kiểm tra kỹ sách nhé
Mình khá buồn vì một số trag sách vẫn còn bị díh vào nhau và chưa đc cắt kĩ ấy
4
2201772
2017-03-26 11:56:48
--------------------------
433345
6621
Cuốn sách hãy sống cuộc đời như bạn muốn giống như tựa sách của nó, nội dung nói về cách muốn thay đổi bản thân mình,và những kĩ năng cần thiết cho cuộc sống mỗi ngày và về kể về nhưng con người đã sống cuộc đời như mình muốn đã vượt qua nhiều khó khăn, và chúng ta cần tạo sự khác biệt để thành công và cố gắng vượt qua nhiều thử thách để sống cuộc đời như bạn muốn, nhưng sách cũng có nhiều phần khó hiểu cần phải đọc kĩ.
Mình rất ấn tượng về bìa,sách thiết kế đẹp và tinh kế, chất liệu giấy được.
3
506806
2016-05-20 21:40:42
--------------------------
377105
6621
Mình đang ngồi trên ghế nhà trường , đôi khi mình rất nhút nhát , e dè , lo sợ nhiều chuyện , nhưng khi đọc xong cuốn này rồi mình cảm thấy có suy nghĩ tin vào bản thân , hết lo âu , bớt tự ti và mạnh dạn hơn  , tác giả đưa ra một lời khuyên là đừng bao giờ từ bỏ những gì mình muốn , và đừng lắng nghe ai hết , cứ đơn giản là sống cuộc đời mà bản thân bạn hằng mong muốn . Nếu ao ước có được thứ gì đó , bạn cứ làm đi vì bạn còn trẻ mà , làm để sau này không phải hối hận chứ ! Một điều quan trọng là bạn phải xây dựng cuộc đời bạn sao cho khác biệt , sống cho ước mơ của mình , đừng phí thời gian suy nghĩ tới lời nói của người khác . Hơn hết , hiện giờ ta đang trong quá trình hội nhập , tác giả mong ta có thể can đảm chọn đúng hướng đi cho mình !!!!!!!!!1
5
781966
2016-02-01 17:35:08
--------------------------
337616
6621
Mình quyết định mua cuốn sách này vì được bạn bè giới thiệu. "Hãy sống cuộc đời nhưu bạn muốn" là một cuốn sách bổ ích. Ấn tượng ban đầu về bìa sách đẹp, màu sắc hài hoà, thiết kế lạ mắt. Chất liệu giấy cũng rất tốt, chữ rõ ràng. Chúng ta thường nghĩ về tương lai tươi sáng nhưng lại chưa thật sự cố gắng cho cuộc sống chúng ta hằng mơ ước đó hoặc sự phấn đấu đó chưa đủ. Chúng ta vẫn chưa bỏ được các thói quen không tốt mà chúng ta vẫn làm hằng ngày. Vì vậy dám ước mơ thì cũng phải dám biến ước mơ đó thành sự thật với những nỗ lực vươn lên.
5
717149
2015-11-14 13:04:35
--------------------------
290299
6621
Nghe tựa đề thôi cũng đã thích rồi!Một cuốn sách về kỹ năng sống khá gây cảm giác buồn ngủ(đọc cuốn kỹ năng sống nào mà không có cảm giác này!!hihihi!!)Nhưng thật sự thì khi đọc xong bạn sẽ cảm thấy mình được mở mang kiến thức và trở thành một con người trưởng thành hơn!Cuốn sách đề cập nhiều về vấn đề mình muốn làm gì thì hãy làm điều ấy(chỉ là những việc tốt cho xã hội thôi nhé)!Nếu bạn thích tham gia những tổ chức phi chính phủ hay từ thiện mà không dám thực hiện vì không có sự đồng ý của ba mẹ thì cuốn sách này sẽ cho bạn lòng can đảm để làm điều đó!!!
4
413637
2015-09-05 11:13:41
--------------------------
168550
6621
Tác giả đề cập đến những điều quan trọng cần được nuôi dưỡng trong mỗi con người như: sự dũng cảm, sự giúp đỡ, lòng tốt, sự cam kết, sáng tạo, hạnh phúc... Ngoài câu chuyện về nhân vật có thật dám từ bỏ vật chất, cuộc sống nhàm chán, đơn điệu để sống một cuộc sống mà họ mong muốn như một tấm gương cho người đọc thì cuối mỗi chương là những bài tập luyện nho nhỏ để người đọc có thể thực hành nuôi dưỡng tâm hồn, chạm được vào ước muốn thực sự của bản thân và trên hết là dám sống theo lựa chọn của mình.
4
473130
2015-03-16 21:00:09
--------------------------
139206
6621
"Hãy Sống Cuộc Đời Như Bạn Muốn" là một cuốn sách khá hay. Cuốn sách có nhiều ưu điểm khá hay mà mình đánh giá cao. Bìa ngoài đẹp, sáng tạo,.. giấy in rất mịn, màu đẹp... Nội dung cuốn sách giúp định hướng bạn đạt đến những mục tiêu, thành công một cách nhanh chóng bằng việc nghĩ lớn, làm lớn, thay đổi lối tư duy, rèn luyện những kĩ năng cần có như: kĩ năng giao tiếp, kĩ năng hiểu ngôn ngữ cơ thể, kĩ năng ứng xử, tác phong làm việc,.. Tác giả cũng đưa ra những kinh nghiệm được đúc kết từ rất nhiều ví dụ thực tế..... Nội dung cuốn sách thiết thực, hấp dẫn mọi lứa tuổi. Nhược điểm: lỗi dịch thuật ở phần đầu: biên dịch khá chán nên đọc không hiểu lắm, từ chương 2 trở đi mới thấy dễ chịu hơn và nội dung cuốn sách mà đem áp dụng với người việt nam thì cần lưu ý.
3
414919
2014-12-06 14:43:06
--------------------------
