462914
8434
Cuốn sách này viết về những câu chuyện theo quy luật nhân quả của đạo Phật, có tính giáo dục tốt, đặc biệt là dạy con người biết đối nhân xử thế, sống có đạo đức theo nhà Phật. Cuốn sách giúp trẻ em có tư duy logic đồng thời giúp quý phụ huynh có định hướng giáo dục con cái đúng đắn, hình thành tính cách tốt trong đời sống xã hội. Quyển sách này từng gây sốt ở Việt Nam với những tư tưởng giáo dục của Phật Giáo được đưa vào cuộc sống thực tế hàng ngày một cách nhẹ nhàng mà sâu sắc, đóng góp trong việc hình thành nhân cách của con người: ví dụ câu chuyện Con bò và ông phú nông khuyên mọi người dùng những lời lẽ cũng như tình cảm yêu thương, chân thành để  với đối xử với nhau thì cuộc sống sẽ tốt đẹp hơn, hay câu chuyện tranh cãi và tranh luận...tôi tin đây sẽ là một tác phẩm hữu ích cho mọi người
5
1406208
2016-06-28 22:47:58
--------------------------
405661
8434
Cao Huy Thuần đến với vân học, theo tôi - đó là cái duyên. Một cái duyên của sự nhận tỉnh và mơ mộng. Lời văn xúc tích, truyền cảm và mang đến cho người đọc những khám phá, trải nghiệm mới.
Thông qua ngôn từ, chúng ta dễ dàng nhận thấy biết bao điều mà Cao Huy Thuần muốn nói tới bằng những câu chuyện đời, chuyện người, từ đó cho ta thấy những công cụ truyền tải qua văn chương, một kiểu văn không chút hà khắc, thơm thảo và đầy ấn tượng. Mọi người nên chọn tác phẩm này để đọc.
3
429966
2016-03-26 23:48:37
--------------------------
317550
8434
"Nhật ký sen trắng" của Giáo sư Nguyễn Huy Thuần không phải là một câu chuyện giản đơn với những tình tiết trong trẻo, nhẹ nhàng. Lồng ghép trong đó là những triết lý, những thông điệp, khiến người đọc phải suy ngẫm rất nhiều. Mặc dù tác giả bộc bạch là cuốn sách này hướng đến các bạn tuổi mới lớn, nhưng sau khi đọc xong mình lại nhận thấy cuốn sách cần cho cả những người trẻ tuổi nữa. Vì những điều hay về đạo đức, về tình cảm, về cuộc sống đều đáng phải học hỏi, cho dù là đã bước qua tuổi teen đi chăng nữa.
5
531312
2015-10-03 19:12:23
--------------------------
301351
8434
Đây tác tác phẩm đầu tiên mình đọc của bác Cao Huy Thuần. Truyện hay thật, cứ trong vắt, tinh khiết như tựa chính tựa đề của tác phẩm, tinh khôi như một bông sen trắng . Một tác phẩm xứng đáng để đọc và suy ngẫm rất nhiều. Đôi khi triết lý không nằm ở những phạm trù lớn lao, nó nhỏ bé và đầy ý nghĩa như những câu chuyện trong tác phẩm này. Một tác phẩm nên có trong kệ sách mọi nhà. Để thỉnh thoảng đọc những câu chuyện mà và chiêm nghiệm về cuộc đời
5
77191
2015-09-14 16:37:40
--------------------------
288120
8434
Mình đã từng đọc sách của tác giả Cao Huy Thuần và rất thích các dẫn chuyện, lập luận của tác giả. Với cuốn sách này không phải là ngoại lệ. Cuốn sách dành cho lứa tuổi teen nhưng mình đọc vô cùng hào hứng: những câu chuyện nhỏ, những ví dụ giản dị nhưng sâu lắng dạy người ta những điều cơ bản của đạo đức con người. Cách viết dung dị giúp các em teen sẽ dễ dàng đọc. Mình sẽ dùng cuốn sách này tặng cho các cháu của mình đang ở lứa tuổi này. Chắc chắn rất hữu ích.
5
327308
2015-09-03 12:42:07
--------------------------
234711
8434
Quyển sách gồm những câu chuyện mang tính giáo dục thiếu niên nhưng người lớn đọc vẫn thấy rất ý nghĩa. Tác giả thật tinh ý khi dựng lên nhân vật Sen Trắng với tính tình hồn nhiên, ngây thơ đúng với lứa tuổi dậy thì. Những câu chuyện mà sen trắng kể với các bạn cũng là những vấn đề mà nhiều bạn trẻ gặp phải hiện nay như phải đối diện với những lời nói xấu và tin đồn như thế nào? Có nên trả thù người đã hại mình không? Lòng biết ơn, tính khiêm tốn,... Tất cả những vấn đề được tác giả gợi mở hết sức tự nhiên và đưa ra giải pháp qua từng câu chuyện kể, giọng văn như đang trò chuyện, tâm tình nên người đọc không hề thấy chán hay buồn ngủ như học những bài học đạo đức giảng lý thuyết suông, khô khan, mà ngược lại còn thấy lòng nhẹ nhàng, thanh thản vì những nỗi băn khoăn, lo lắng của mình nay đã có người cảm thông và chia sẻ. 
5
277587
2015-07-20 15:26:50
--------------------------
232065
8434
Xu hướng viết sách của GS Cao Huy Thuần thường mang giọng triết họ, trong cuốn sách này cũng vậy.Tác giả viết về những bài học đạo đức nhưng không phải là những bài học khô khan mà dưới những câu chuyện , những buổi sinh hoạt của Sen Trắng với ngôn từ rõ ràng , khúc chiết nhưng cũng rất dễ hiểu. Những câu chuyện là những giá trị đạo đức căn bản của con người, cách ứng xử trong cuộc sống( ví dụ như bài trả thù).Tôi 18 tuổi nhưng khi đọc sách vẫn ngộ ra rất nhiều điều để hoàn thiện nhân cách.Tôi nghĩ cuốn sách sẽ là một món quà tinh thần ý nghĩa và bổ ích cho các bé
5
373417
2015-07-18 16:01:50
--------------------------
138196
8434
Đọc "Nhật ký Sen Trắng" của bác Cao Huy Thuần lắng đọng và sâu sắc lắm luôn. Truyện được viết cho đối tượng ở tuổi 15 hoặc viết cho các bậc phụ huynh. Khi cuộc sống ngày càng hào nhoáng, rỗng mục thì lúc cầm trên tay cuốn sách dung dị, nhẹ nhàng thế này khiến tâm cũng như tim mình không tránh khỏi cảm giác hạnh phúc, an nhiên. Những tác giả cổ, những câu chuyện xưa, những ngụ ngôn hay những đời thường, tất cả lần lượt xuất hiện qua giọng kể của Sen Trắng, Sen Hồng, Sen Búp, Sen Nụ ... Những thỏ thẻ chuyện trò ấy giống như những mảnh ghép ráp vào bức tranh dang dở của bản thân mình, bức tranh mang tên "lăng kính, cách nhìn, đồng cảm cuộc sống".
5
358379
2014-12-01 10:31:38
--------------------------
116656
8434
Giọng văn bác Cao Huy Thuần vốn gần gũi, với cuốn NKST này còn gần gũi hơn. Một người lớn như tôi đọc xong còn thấy mình phải học nhiều điều. Tôi nghĩ nếu đưa quyển sách này vào giờ học "Đạo đức" ở cấp 2, có lẽ xã hội mai sau không còn những chuyện đau lòng như ta thấy đầy rẫy hiện nay. Từng trang sách là những bài học sâu sắc nhưng với lối kể chuyện gần gũi thật phù hợp để các em nhỏ có thể hiểu mà không cảm thấy quá lý thuyết.
Nếu bạn cần 1 cuốn sách để dành tặng cho những đứa em, đứa cháu mình yêu thương thì NKST là một món quà không thể tuyệt vời hơn. 
Cám ơn tác giả.
5
124590
2014-07-10 14:31:51
--------------------------
109438
8434
Tớ được một người bạn tặng cho quyển sách này. Mục đích chính của bạn là tặng cho con trai mình đang học lớp 5 nhưng khi mình mở sách ra đọc mấy trang đầu thấy cũng rất thú vị vì thế mà đã đọc...đọc một cách chậm rãi để mong ngấm cho hết những bài học tưởng bình thường, tưởng chùng như tặc lưỡi vì nghĩ mình đã có sẵn bên trong rồi :)). Quyển sách tưởng như không chỉ dành cho trẻ nhỏ...nó dạy lại cả cho ngườí lớn chúng ta rất nhiều điều...rằng tranh luận khác với tranh cãi..qúa nhiều người lớn đang lạm dụng ngôn từ cuả mình để đẩy người khác vào tình cảnh đối nghịch với họ...họ đã làm mất đi bản chất lắng nghe, học hỏi và tự điều chỉnh mình trong tranh luận...ta được thêm 1 lời nhắc về yêu thương sự sống xuất phát từ tâm mình, không chỉ đánh gía bởi hành động hình thức bề ngoài...Rằng mình đã được nhận rất nhiều thứ từ trời đất, đến từ mọi người xung quanh. Vậy nếu ta không biết cho mà chỉ nhận thì ai mới đích thị là người buồn? Buồn vì không được cho hay rồi lại chính là nỗi buồn không được nhận? Thế nên, con trai, con gái của mẹ ơi...con hãy trao những lời hòa ái, hãy sử dụng công lý để phân giải những óan ân trong cuộc đời, không thù ghét con người mà chỉ trừng trị hành động ác độc...Con cũng phải biết khi nào cần lên tiếng về những vấn đề mà con hiểu, để bảo vệ quan điểm, chính kiến của mình. Nhưng cũng đừng qúa đà thành ra cãi nhau..vì "cãi nhau mà không biết dừng lại là tự mình khuấy nước cặn mời mình"..
Đọc Nhật ký sen trắng để mình hiểu rõ hơn về "tâm hồn cao thượng", để con mình sẽ hiểu "cao thượng" khác với "rộng lượng"....Rằng thật không dễ để hy sinh quyền lợi của mình cho người khác...huống chi là việc nghĩ đến khó khăn của người khác trước khi nghĩ đến khó khăn của mình....và người nào hành động được điều này thì chắc hẳn là 1 hành động "cao thượng - 1 tâm hồn cao thượng"...
Rồi còn rất rất nhiều bài học nằm trong quyển sách tưởng chừng như nhỏ gọn này...dành cho độc giả mọi lứa tuổi. Mình rất mong nó sẽ góp phần vào cuộc sống của mỗi gia đình như 1 lời dạy con trẻ biết ơn bắt đầu đơn giản chỉ từ 2 từ "Cám ơn". Có thể với bé con nhà mình đọc 1 lần sẽ chưa ngấm hết... nhưng với 1 vị trí trân trọng trong giá sách sẽ có ngày con trai, rồi con gái mình sẽ lại cần đến sách để tìm được 1 lời khuyên....
3
72543
2014-04-01 20:19:37
--------------------------
