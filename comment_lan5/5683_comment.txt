375291
5683
Các tác phẩm của nhà văn Roald Dahl luôn đem lại một sức hút đặc biệt đối với mình. Với những tình tiết kỳ lạ, thậm chí đến mức kỳ quái, mỗi câu chuyện của Roald Dahl – không truyện nào giống truyện nào nhưng đều có sức hấp dẫn riêng. "James và quả đào khổng lồ" cũng là một tác phẩm như thế, tác giả đã sáng tạo ra những nhân vật vô cùng thú vị như cô nhện, chú rết, chú giun,… những con vật được nhân cách hóa một cách rất sinh động kề vai sát cánh với nhân vật chính. Một câu chuyện lạ lùng nhưng lại rất lôi cuốn, khiến người đọc có thể thích thú theo dõi từng trang. Đây thực sự là một cuốn sách hay trong bộ sưu tập truyện Roald Dahl của mình.
5
135597
2016-01-28 09:33:40
--------------------------
275719
5683
Khi bạn phải sống với hai bà cô một cao nhòng, gầy đét và một lùn tịt, mập như cái lu, hai bà cô suốt ngày kiếm cớ bắt nạt bạn, giam giữ không cho bạn ra ngoài khỏi khuôn viên ngôi nhà, lúc đó bạn sẽ ước có một phép lạ giúp bạn thoát khỏi những đau khố này, Và phép lạ đã đến nhưng vì sự bất cẩn của bản thân, James đã tạo ra một phép lạ khác, những người bạn mới xuất hiện, và chuyến phiêu lưu trên trái đaào khổng lồ của James và những người bạn đã bắt đầu...
5
13278
2015-08-23 10:03:12
--------------------------
148068
5683
"James và quả đào khổng lồ" tuy là một cuốn sách dành cho thiếu nhi nhưng lại có những chi tiết mà mình nghĩ ngay cả người lớn cũng sẽ phải bất ngờ về độ "kỳ" của nó :)))))))). 
Tác giả Roald Dahl có một trí tưởng tượng quá "đỉnh" với khi xây dựng được một cốt truyện với những tình tiết tưởng chừng phi lý nhưng lại hợp lý một cách lạ kỳ. Mình nghĩ ai đọc cuốn sách này hẳn đều sẽ cảm thấy thích thú trước các nhân vật là những chú bọ khổng lồ nhưng dễ thương và có tính cách rất riêng như Rết xấu tính, Giun đất hiền lành:)))))
Tuy nhiên mình lại thấy có một vài tình tiết khá sợ không phù hợp với thiếu nhi lắm như là cảnh bố mẹ james bị con vật sổng từ chuồng thú ăn thịt hay là khi hai bà cô bị quả đào cán dẹp lép, rồi khi Nhện kể chuyện bà mình bị giết. Nhưng nói chung đây vẫn là một câu chuyện hay và không thể bỏ lỡ.
5
431082
2015-01-09 18:32:52
--------------------------
