363489
4697
Truyện Khám Phá Thế Giới - Lời Chào Mừng Ban Mai được dịch thành thơ, đọc nghe rất vần nên bé nhà mình rất thích nghe và rất dễ nhớ truyện. Thỉnh thoảng bé lại ê a đọc "mặt trời ơi mặt trời, mau thức giấc đi thôi" và rất thích thú với câu đó. Truyện vẽ rất đẹp và dễ thương nên bé nhà mình rất thích. Tuy nhiên tên truyện được dịch ko đúng lắm vì mình thấy tên tiếng Anh của truyện là "Wake up" tuy nhiên dịch là lời chào mừng ban mai cũng có ý nghĩa nên cũng ko sao.
5
592123
2016-01-04 13:09:28
--------------------------
332476
4697
“Lời chào mừng ban mai” là một trong 12 cuốn sách trong bộ sách Khám phá thế giới do Nhà xuất bản Kim Đồng phát hành. Mình rất vui khi đã mua được gần trọn bộ. Cuốn “ Lời chào mừng ban mai” gồm 21 trang, hình vẽ đẹp, màu sắc hài hòa, bắt mắt. Truyện kể về ông mặt trời vì còn buồn ngủ nên mãi mà không chịu dậy soi sáng mọi vật, mở đầu cho một ngày mới. Thông qua truyện, ba mẹ có thể dạy bé về ông mặt trời, các vì sao, đám mây, khi gà trống gáy là lúc mặt trời ló dạng v.v... Mình rất thích bộ truyện này.
3
15022
2015-11-06 13:35:43
--------------------------
300742
4697
Trong các câu truyện thuộc bộ Khám Phá Thế Giới của nhà xuất bản Kim Đồng thì có thể nói mình thích quyển Lời Chào Mừng Ban Mai này nhất. Truyện có cách kể có vần điệu như một bài thơ với hình minh họa dễ thương. Truyện kể về bạn mặt trời ngủ nướng mà sao giống các bé thế từ việc ngủ cố, lăn lộn, đến càu nhàu vì bị đánh thức ^^ có lẽ vì thế mà mình rất thích câu truyện này. Mỗi lần kể mà mình cứ muốn cười lăn vừa kể vừa trêu bé rằng bạn mặt trời giống con quá xấu xấu nhỉ
5
153008
2015-09-14 10:09:08
--------------------------
