355497
8300
Nói về nhiều màu sắc đẹp của con người và cảnh đẹp của vùng quê thời bấy giờ , nếu có một trong số những điều chưa biết thì qua đây có thể nhận ra tập quán đặc tính của người vùng đồng bằng sông cửu long chạy dài dọc miền quê xa xôi , tận cuối của mảnh đất màu mỡ , phù du , nhiều suy nghĩ có tính cổ động người ta theo những tán cây , ngọn cỏ mà giữ vững nét độc đáo của vùng sông nước buồn tẻ nhạt , tiếc thay cho những gì đã lấy đi .
4
402468
2015-12-20 00:56:51
--------------------------
337458
8300
Sở hữu một bộ sách gồm nhiều truyện ngắn vẫn là niềm thích thú của tôi. Mặc dù, phương tiện kỹ thuật ngày nay có thể hỗ trợ ta nghe đọc truyện trực tuyến hoặc nghe qua audio. Truyện của nhà văn Sơn Nam vẫn phảng phất những dư âm của đời sống vùng Nam Bộ. Nhà nghiên cứu Bourdiex nhận định, từ những biên khảo và những sáng tác của Sơn Nam có thể cho ta hình dung về những động thái văn hóa của cư dân vùng đồng bằng trước những đổi thay của đời sống. Còn gì hơn, nếu chúng ta gặp những điều ông bà mình kể trong những truyện ngắn này.
5
390132
2015-11-14 02:00:20
--------------------------
321805
8300
"Hương Quê, Tây Đầu Đỏ Và Một Số Truyện Ngắn Khác" là một trong số những cuốn sách của Nhà văn Sơn Nam mà gần đây nhà xuất bản Trẻ ấn hành lại. Riêng cuốn này thì thật sự thích vì các câu chuyện đều cực kỳ hay và đậm màu sắc Nam Bộ. Các câu chuyện diễn ra ở một khoảng thời gian rất xa với bối cảnh hiện đại này. Văn phong của Nhà văn Sơn Nam thì vẫn luôn dung dị và tràn đầy tình cảm. Cuốn này đọc nhấm nháp mỗi ngày như thể uống một tách cà phê ngon vậy. Không cần vội vàng, Vừa đọc vừa chiêm nghiệm, vừa suy ngẫm. Rõ ràng tác phẩm ra đời đã lâu mà vẫn nguyên vẹn giá trị và sức hút. "Hương Quê, Tây Đầu Đỏ Và Một Số Truyện Ngắn Khác" là cuốn sách rất đáng đọc.
5
531312
2015-10-14 21:56:54
--------------------------
