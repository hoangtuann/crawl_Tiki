403347
5441
Richard Templar tác giả nổi tiếng của những quyển sách về quy tắc. Mình đọc đầu tiên là cuốn những quy tắc trong cuộc sống và quyết định sẽ sưu tầm các đầu sách còn lại của Ông. Phong cách viết nhẹ nhàng, dễ hiểu, gần gũi và tự tin đã tạo ấn tượng cho mình, lôi cuốn mình đọc hết cuốn sách và mua thêm những cuốn sách còn lại của Ông. Những mẫu truyện ngắn dễ đọc, dễ áp dụng và điều đó tạo nên những quyển best seller của Ông. Những quy tắc và những quy tắc hay, hay và hay!
5
702724
2016-03-23 17:16:10
--------------------------
398697
5441
Nội dung sách cho thấy tác giả không thuộc mẫu cây bút thích diễn giải loanh quanh và dong dài. Với mỗi quy tắc là tác giả đưa đến ngay cho độc giả một chủ đề thực tế, sau đó là đi ngay vào phần hướng dẫn và đánh giá rất ngắn gọn và đúc rút ra ngay kết quả. Mỗi quy tắc được xem như là môt lời khuyên, hướng dẫn đến chúng ta trong hầu như mọi lĩnh vực công việc. 
Điểm trừ của sách có thể đó là có quá nhiều quy tắc, đôi lúc chính tôi cũng choáng với độ dài của những quy tắc này: " quá nhiều". Nếu có thể rút ra được những quy tắc quan rọng nhất có thể xem là sách đã mang lại hiệu quả cho người đọc. Không thể nhất thiết phải rập khuôn mọi nguyên tắc.
3
62072
2016-03-16 16:45:52
--------------------------
353605
5441
Mình đã mua và đã đọc cuốn sách này, mình có những nhận xét như sau:
- Sách trình bày đẹp, nội dung, cách diễn đạt dễ hiểu, dễ nhớ.
- Thiết thực, có những chỉ dẫn và ví dụ rất cụ thể. Giúp người đọc dễ hình dung được.
=> Với bản thân mình là sinh viên mới ra trường và vừa bắt đầu công việc ở một công ty lớn. Nên mình nghĩ cuốn sách này giúp ích cho mình rất nhiều. Giúp mình biết nên làm gì và không nên làm gì, định hướng từ suy nghĩ đến hành động.
4
978500
2015-12-16 14:53:02
--------------------------
309454
5441
Richard Templar đã đem đến cho chúng ta một cuốn sách không thể thiếu cho những ai sẽ, sắp và đang đi làm. Tác giả đã đưa ra những quy tắc rất hay trong công việc để chúng ta có thể làm tốt công việc của mình và giữ gìn hình ảnh, xây dựng thương hiệu bản thân ở nơi làm việc cũng như tạo cho chúng ta cơ hội để thăng tiến.
+ Sách trình bày đẹp mắt, dễ hiểu và dễ ghi nhớ.
+ Nội dung hay, thiết thực, tác giả đưa ra những chỉ dẫn và ví dụ cụ thể.
+ Giấy còn hơi mỏng.
Với những sinh viên mới ra trường, những người đang làm việc thì nên tìm đọc cuốn sách này để trang bị cho mình những kỹ năng cần thiết khi đi làm.
4
341353
2015-09-18 23:46:44
--------------------------
199567
5441
Mình đã đọc cuốn sách Những quy tắc trong công việc và có những lời nhận xét như sau:
Thứ nhất về hình thức cuốn sách được thiết kế cũng rất bắt mắt, chất liệu giấy in trắng và độ bền khá tốt. 
Thứ 2 về nội dung cuốn sách khá hay, Nội dung cá bài viết trong cuốn sách khá hay và có nhiều nội dung thực tế đa số mọi người đều gặp phải và cuốn sách có phần công phu và tâm huyết của tác giả đã bò ra. Nhưng có một số nội dung trong cuốn sách tác giả chưa được đầu tư kỹ lưỡng nội dung còn rất sơ sài. 
Thứ 3 Cuốn sách này rất có ích cho mọi người có thể học theo và xử lý công việc một cách tốt hơn.
Thứ 4 Cuốn sách này nói chung cơ bản đã đáp ứng được yêu cầu mong mỏi của quý độc giả. Nhưng với một số độc giả khó tính thì vẫn còn rất nhiều vấn đề cần phải hoàn thiện để cuốn sách được tốt hơn.
Một lần nữa cảm ơn tác giả đã có một tác phẩm tốt.
5
484466
2015-05-22 15:10:11
--------------------------
176237
5441
Richard Templar là một trong những tác giả viết sách yêu thích của tôi . Sách của ông luôn hấp dẫn từ những dòng đầu tiên và duy trì được đến dòng cuối cùng . Cuốn sách đề cập vô cùng chính xác những vấn đề gặp phải tại nơi làm việc . Sự phân tích , sự giải quyết vấn đề của ông vô cùng tinh tế  Đôi chỗ khiến người đọc phì cười . Dù nhiều vấn đề tôi cũng đã biết cách xử lý nhưng khi đọc phân tích của ông tôi càng thấy thấm thía hơn . 
Những cuốn Quy tắc Richard Templar có nội dung vô cùng đắt giá nhưng bìa sách lại đơn điệu quá ( không rõ có phải do yêu cầu của tác giả không ) . Và cũng khó để mua được cả bộ sách của ông vì chúng luôn hết hàng .
Tôi hy vọng các cuốn sách Quy tắc được xuất bản thêm để có thể bổ sung vào list sách yêu thích của mình.
5
331157
2015-03-31 20:53:58
--------------------------
163838
5441
Tác giả Richard Templar đã cung cấp cho chúng ta một thứ cực kỳ cần thiết để được thăng tiến, được tôn trọng và đạt tới đỉnh cao thành công của công việc: 10 quy tắc vàng để thành công trong công việc. Quyển sách chứa đựng lời khuyên của tác giả rằng khi những người khác, những đồng nghiệp của bạn chỉ chăm chăm tập trung vào những gì họ làm thì trong lúc đó bạn hãy đọc và vận dụng những quy tắc vàng này vào công việc và chúng sẽ giúp cho bạn gặt hái nhiều thành công hơn. Hãy sử dụng những chỉ dẫn quyết định cho sự thành công của cá nhân bạn và xa hơn là công ty của bạn, tổ chức của bạn.
5
387632
2015-03-05 23:27:37
--------------------------
138909
5441
Nghe tên cuốn sách có vẻ giáo điều nhưng đọc sẽ thấy không hề mang nặng lý thuyết giáo điều mà vô cùng có ích.
Khi đọc cuốn sách này có một số quy tắc mình đã từng làm nhưng không thật sự tuân thủ và cũng có một số quy tắc mình chưa từng để ý nhưng rất đáng để học hỏi nếu  muốn thành công và thăng tiến trong công việc.
Cuốn sách không nêu những ý kiến đao to búa lớn về chuyên môn nghiệp vụ trong công việc mà chủ yếu nói đến những hành vi, thái độ và cách ứng xử đối với công việc, đồng nghiệp, sếp... trong môi trường công sở. Chính những điều này lại góp phần quan trọng trong công việc của bạn.
Theo ý kiến của mình thì nên đọc, rồi đọc lại để ngẫm lại lần nữa :)
5
56624
2014-12-05 11:46:38
--------------------------
