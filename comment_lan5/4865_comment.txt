389050
4865
Tôi vẫn chưa thể nào quen với cách viết truyện của Phương Tây, nó hơi khác với ngôn tình Trung Quốc, nhất là cách nói chuyện, cư xử.
Đối với Chàng Công Tước Bí Ẩn này, cốt truyện cũng khá hay, các nhân vật đều có cá tính riêng, đặc biệt là Victoria cô ấy có biệt tài may vá, sáng tạo ra nhiều mẫu váy với từ những chiếc váy cũ, sau đó cô ấy bắt đầu sáng tạo với những mẫu áo lót, nhằm tạo cho những người phụ nữ cảm thấy thoải mái,không gò bó lại gơi cảm nhưng cô ấy lại có một nỗi sợ hãi khi mỗi lần đến gần cửa ra vào. Vì thế sự xuất hiên của Jonathan đã thay đổi toàn bộ cuộc đời cô, anh như ánh mặt trời khiến cô dám dũng cảm đối diện với nỗi sợ của mình.
3
313953
2016-03-01 10:30:23
--------------------------
359775
4865
Tôi khá thích cuốn này của tác giả, so với “Bí mật tội lỗi” thì truyện này giọng văn không đến nỗi ảm đạm bằng, nói chung là sáng sủa hơn, vui tươi hơn. Mặc dù câu chuyện về nỗi sợ hãi khi phải bước ra khỏi nhà của nhà của Victoria có hơi phi lý nhưng cách mà tác giả thông qua câu chuyện đó để xây dựng nên một tình yêu tuyệt vời của Victoria và Jonathan cũng khá hay và thuyết phục. Nếu như tình yêu của Jonathan có thể giúp Victoria chiến thắng nỗi sợ hãi của mình thì ngược lại, cô cũng đã dùng tình cảm của bản thân để sưởi ấm trái tim vốn nguội lạnh của Jonathan. 

Truyện ấn tượng ngay từ khi bắt đầu và đến khi kết thúc cũng khá là hoàn hảo, tuy nhiên so với những tác giả quen thuộc về dòng văn học lãng mạn lịch sử khác thì có lẽ tôi không thể đánh giá cao Michelle Willingham, so với họ bà vẫn còn thiếu một chút gì đó gọi là sự say đắm, tình huống li kì cùng với một kết cấu chặt chẽ. Thêm nữa là bìa truyện nhìn không mấy ấn tượng, cũng không phù hợp với một tiểu thuyết phương Tây, bìa sách lần này của BV làm tôi có liên tưởng đến một quyển truyện tranh Manga nào đó.

4
41370
2015-12-27 23:06:25
--------------------------
317614
4865
Bề ngoài truyện "Chàng công tước bí ẩn" của tác giả  Michelle Willingham dược đóng kết khá là tốt. Bìa sách đẹp, chất lượng in và giấy tốt, câu dịch cũng rõ nghĩa linh động chứ không máy móc. Về nội dung câu chuyện cungc khá thú vị. Tôi đã đọc nhiều văn học nước ngoài và vẫn không thể nào phân biệt hết được tước hiệu Công tước, Bá tước, Hầu tước, Tử tước…. Mô tip truyện cũng khong có gì lạ lẫm cho lắm nhưng với tài viết của nhà văn, những diễn biến tâm trạng và hành động được thực hiện rõ nét hơn. Tuy nhiên vẫn có gì đó không được rõ ràng. Tóm lại, “Chàng công tước bí ẩn” là một câu chuyện đáng đọc.
4
722069
2015-10-03 21:03:51
--------------------------
305724
4865
Ấn tượng đầu tiên của mình với cuốn sách là bìa sách, bìa sách rất bắt mắt, ưa nhìn, dễ gây thiện cảm. Ấn tượng tiếp đến là nội dung, cuốn sách có nội dung khá mới mẻ khi tác giả đã tạo nên nhân vật nữ chính với tính cách khá rụt rè khi cô không dám bước ra khỏi nhà sau một biến cố. Sự xuất hiện bất ngờ của Jonathan đã làm thay đổi Victoria, làm hay đổi cuộc sống của cô. Jonathan đã làm cho cuộc sống của Victoria trở nên có màu sắc, có ý nghĩa hơn. Anh đã giúp cô khắc phục được nỗi sợ hãi của mình, vượt lên trên chính bản thân. Và tác giả đã làm cho độc giả bất ngờ hơn nữa với thân thế thật sự của Jonathan- một công tước, cũng chính tình tiết này đã đưa câu chuyện sang  một ngã rẽ mới. Với mình, câu chuyện không chỉ mang ý nghĩa giải trí hay vì sở thích  mà trong đó còn chứa đựng một ý nghĩa rất sâu sắc: Đừng bao giờ cảm thấy sợ hãi mà bỏ cuộc, hãy vượt lên trên tất cả bằng chính bản thân mình để có thể đạt đến thành công. Mong bạn đọc cũng sẽ sớm sở hữa cuốn sách này như mình :)
5
284930
2015-09-17 09:09:50
--------------------------
252813
4865
Mô típ truyện khác lạ. Nữ chính - Victoria - là một người khá là nhút nhát và rụt rè vì một tai nạn hồi còn nhỏ khiến cô ko dám ra khỏi căn nhà của mình ; Jonathan - một chàng công tước khá là điển trai nhưng bị thương và phải tá túc nhà nữ chính nhờ mấy tuần. Bùm, hai người bắt đầu mở rộng lòng mình hơn và từ từ mở rộng lòng mình . Jonathan tha thiết muốn cưới Victoria nhưng cô sợ, ko dám bước ra khỏi nhà. Bằng cách nào đó Jonathan đã giúp Victoria vượt qua khỏi nỗi sợ đó và cô đồng ý cưới anh. Cuộc đời của cô như mở ra một trang sách hoàn toàn mới khi cô phải cố gắng hoà nhập với mọi người bên ngoài. Đến khúc cuối cô có một người con trai xinh xắn ( suýt nữa bị xẩy thai :(   ) Ngoài ra tác giả còn chú ý đến những mối quan hệ của những người em gái Victoria và mẹ của cô ấy.
4
258651
2015-08-03 22:25:53
--------------------------
220342
4865
Victoria- cô gái xinh đẹp nhưng luôn tự ti và nhút nhát. Tất nhiên là có lý do khiến nàng trở nên như thế. Gia đình yêu mến cô, nhưng mọi người đều không biết làm thế nào để có thể giúp cô thoát khỏi sự ám ảnh về việc phải bước chân ra khỏi ngôi nhà để khám phá thế giới bên ngoài, đối với cô, đó là cả một vấn đề mà có lẽ chính bản thân cô cũng cho rằng cả đời này cô sẽ gắn bó với ngôi nhà cô đang sống mà không có cách nào thoát ra được. 
Jonathan – chàng công tước giàu có, lạnh lùng nhưng bên trong là trái tim nồng ấm và đầy những vết thương. Chàng thông minh, mạnh mẽ và kiên nhẫn giúp Victoria thoát khỏi sự ám ảnh mà trở nên mạnh mẽ hơn với tình yêu nồng  nàn dành cho nàng. 
Câu chuyện hấp dẫn người đọc bởi lối viết truyện đơn giản, dễ hiểu, tình yêu mãnh liệt có thể giúp ta làm được những chuyện không thể ngờ đến.
4
412826
2015-07-02 15:14:07
--------------------------
219328
4865
Một tác phẩm văn học cổ đại đã chễm chệ nằm trên giá sách của tôi. Không gì tuyệt vời hơn với lối dẫn chuyện vô cùng cuốn hút của Michelle Willingham. Nội dung không quá sướt mướt, đủ để khiến tôi như mê hoặc với từng lời thoại, từng hành động của nhân vật. Không quá rườm rà câu nệ, đủ để khiến bạn hình dung ra Toria hay Jony đang làm gì trước mắt bạn. Nó sinh động.
Khi đọc những trang đầu tiên của tác phẩm này, tôi đã vô cùng thích thú vì tôi biết nó không làm tôi thất vọng. 
Tại sao không thể sở hữu cho mình một câu chuyện quá đỗi tuyệt vời như vậy. Không! Tôi thật may mắn vì tôi đã sở hữu nó.
5
479602
2015-07-01 13:18:29
--------------------------
217776
4865
Đây là câu chuyện tình yêu của một cô nàng sợ phải ra ngoài. Victoria luôn nhốt mình trong nhà với nỗi ám ảnh bị lãng quên và bỏ rơi. Vậy cho nên, trừ khi là có anh chàng nào đó rớt vào người cô chứ nếu không chẳng thể mong chờ vào việc cô sẽ kiếm được một tấm chồng. Và Jonathan, không rớt - mà được khiêng vào, đã đến với Victoria theo cái cách mà hẳn là chẳng vị Công tước nào muốn: bị một thằng nhóc bắn và suýt nữa chết vùi trong giá lạnh.
Chàng Công Tước Bí Ẩn quả thực xứng đáng với một vài lời khen vì câu chuyện và vì cách kể của tác giả: Chậm rãi để các nhân vật khám phá ra nét đẹp của nhau, cẩn trọng để các nhân vật suy nghĩ về hành động của mình và tín hiệu của đối phương, đắm chìm với những rung động từ nhẹ nhàng đến mãnh liệt họ dành cho nhau. 
Nếu đã quen với tiết tấu nhanh như gió trong các cuốn truyện Tây trước đây thì đến Chàng Công Tước Bí Ẩn, tình yêu hiện hữu với nhịp điệu chậm hơn, nhưng cũng chắc chắn hơn (và đương nhiên, chẳng thiếu phần nóng bỏng) mà nhiều người sẽ cho là nó dài dòng.
Mình đánh giá nội dung cuốn này ổn, nhưng trừ sao vì cái bìa (hổng hiểu sao không cảm nổi) và cái gáy sách (đọc xong gáy hằn nếp từ lúc nào không biết).
3
17877
2015-06-29 22:23:39
--------------------------
206023
4865
Mình thích tác giả Michelle Willingham (MW) vì  tình tiết truyện của MW  nhanh, hợp lý và đôi khi trong nghịch cảnh tác giả vẫn làm cho ta cười được chỉ bằng một lời thoại hay một suy nghĩ của nhân vật. Đó là điều mà mình luôn mong chờ để đón đọc tác phẩm của MW. 
Tình yêu trong tác phẩm này là một  tình yêu cổ tích thật sự, tuy nhiên, hai nhân vật chính đã phải đấu tranh với bao nỗi sợ hãi dày vò tâm hồn suốt một thời gian dài trước khi định mệnh mang họ đến với nhau. 
Hy vọng sẽ sớm được thưởng thức tiếp chuyện tình của hai nhân vật còn lại là Margaret và Amelia.
4
181887
2015-06-08 14:06:15
--------------------------
194358
4865
Tôi chỉ vừa đọc xong quyển sách này cách đây ít phút và quyết định chia sẻ những cảm xúc tuôn trào đang đọng lại trong tôi.
Ngay từ khi đọc sơ lược nội dung sau cuốn sách, tôi đã nghĩ ngay đến mô tuýp truyện "lọ lem và hoàng tử", nhưng lòng hiếu kỳ vẫn thôi thúc tôi đặt mua xem thử như thế nào và sau khi xem xong, tôi thấy mình thật may mắn khi quyết định đúng đắn.
Tình yêu của Ngài Công Tước và Nàng Victoria thật cảm động, từ những rung động ngọt ngào đến những suy nghĩ sâu lắng trong nội tâm của cả hai, với những câu văn hài hước nhưng nhẹ nhàng để người đọc dễ dàng hoà mình vào cảm xúc của nhân vật. Giữa tình cảm và lý trí đan xen, Chàng đưa Nàng ra khỏi nỗi sợ hãi từng chút một mà vẫn chưa nhận ra là mình đã thật sự yêu người con gái nhút nhát nhưng có một nội tâm kiên cường đến vậy. Còn Nàng, bằng chính tình yêu, sự dịu dàng và chân thành đôi khi mãnh liệt của Chàng đã khiến Nàng có thể tự tin hơn vào chính bản thân mình, hơn hết lại có thể dìu Chàng ra khỏi "bóng đêm của sự sợ hãi, mặc cảm" trong chính bản thân Nàng như Nàng đã từng phải đối mặt.
Hay và cảm động lắm, và nhiều tình tiết rất hài hước, nhẹ nhàng khiến Tôi bật cười, ví như là câu nói Nàng nói với Chàng rằng ko phải làm gì cho Nàng cả sau khi cả hai thật sự nhận ra mình ko thể sống thiếu đối phương nữa.." Chàng đừng bắt Em đánh cờ với Chàng cho đến khi Em chết được ko?!" Đọc đến câu này Tôi cũng mỉm cười huống chi là Ngài Công Tước trước cô vợ nũng nịu này..
Một lời khuyên danh cho bạn nào đang lưỡng lự xem có nên mua quyển sách này hay ko là "Hãy đặt mua ngay đi nếu không sẽ hết hàng đó"..
Chúc mọi người cũng cảm thấy vui vẻ và ấm áp như Tôi sau khi đọc quyển sách này nhé..
Thân..!!
5
563417
2015-05-10 11:23:35
--------------------------
175436
4865
Sách được viết theo kiểu ngôn ngữ văn học châu Âu theo cung cách quý tộc cho nên nếu đọc lần đầu chắc chắn sẽ cảm thấy rất khó hiểu, cần phải chú tâm và đọc thật kỹ.
Nhưng phải nói, khi quyết định mua cuốn sách này, là vì mình tin tưởng vào Bách Việt - nổi tiếng là công ty sách với những dịch giả giỏi hàng đầu. Mình thực sự phục dịch giả với ngôn từ không kém một nhà văn.
Ý chính, về quyển sách này. Thực sự thì mình cảm thấy rất phục đôi tình nhân trong truyện này. Thông minh, sắc sảo là những từ có thể nói về quyển sách. Cảm xúc xuyên suốt là tình yêu thương và sự chân tình truyền hơi ấm tình yêu sưởi ấm con tim khô cằn, giá lạnh. Nhẹ nhàng và quyến rũ, mạch truyện khá đơn giản nhưng tác giả rất thông minh khi chọn đề tài này.
Thế nhưng có một điều là khi đọc xong rất dễ quên những chi tiết trong truyện.
4
39173
2015-03-30 10:42:56
--------------------------
167232
4865
Phải nói đây là một trong những cuốn sách có bìa 'bắt mắt' nhất mình từng đọc. "Chàng công tước bí ẩn" cùng series với "Bí mật tội lỗi" của Michelle Willingham.  Đây là cuốn #1, "bí mật tội lỗi" là cuốn #2. Và minh chẳng hiểu tại sao Bách Việt lại xuất bản cuốn #2 trước #1, khiến mình đọc có cảm giác lệch lạc rất khó chịu (vì trót đọc cuốn #2 trước mất rồi.) 
Về phần nội dung mình đánh giá cao "chàng công tước bí ẩn" hơn cuốn trước. Tâm lý của Jonathan và Victoria cũng không mấy phức tạp, nhưng cái bệnh sợ ra ngoài của Victoria thì mình thấy không được hợp lý cho lắm. 
Nói chung văn phong của Michelle Willingham mình cảm thấy không được thông minh như Julia quinn, ngọt ngào nóng bỏng như Lisa Kleypas hay giàu tính nhân văn như Mary Balogh hoặc nhiều trắc ẩn như Julie Garwood. 
Nói chung sau khi đọc hết tác phẩm thì mình không đọng lại gì nhiều cả.
3
125574
2015-03-14 12:24:43
--------------------------
166875
4865
Ngay khi nhìn thấy bìa ngoài, mình cảm thấy một sự tò mò pha chút bí ẩn đúng như tên gọi vậy. Sau khi đọc xong cuốn sách, chắc chắn các bạn sẽ rất ngưỡng mộ về đôi tình nhân. Họ yêu nhau từ trong chính cảm xúc con tim mình. Và bạn sẽ càng thêm yêu quyển sách hơn nữa về cô gái trong truyện đã cảm hoá trái tim chàng trai có trái tim giá lạnh. Hai người đến với nhau như một phép mầu và chúng ta cùng mong cho họ sẽ sống với nhau trọn đời!
4
93425
2015-03-13 18:55:16
--------------------------
