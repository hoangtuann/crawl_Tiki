531805
3809
Lúc đọc tựa đề thấy tên Bùi Kiến Thành tôi nhận ra trước đây xem tivi hay có phỏng vấn chuyên gia kinh tế Bùi Kiến Thành, tôi nhớ đến ông vì cái tên của ông khá hay và lạ. Đến khi đọc giới thiệu sách lại ngạc nhiên hơn vì ông là người cố vấn cho cả 2 chế độ.

Đã lâu rồi đọc được quyển sách về tiểu sử hay đến vậy, khá bất ngờ vì ông cũng là người Quảng Nam, biết thêm được Quảng Nam mình lại có những con người tài năng đến vây ông và cha ông (Bác Sỹ Tín).

Đọc những câu chuyện về tết về quê hương Quảng Nam làm tôi lại bồi hồi nhớ về những kỉ niệm tuổi thơ của tôi. Mặc dù ông cỡ tuổi ông ngoại của tôi nhưng những kỉ niệm về tuổi thơ của ông như  bánh nổ, bánh tổ, bánh đậu xanh khuôn, kẹo cau đó cũng là những kí ức thời thơ ấu của tôi.

Đặc biệt là qua cuốn sách tôi có thêm kiến thức về lịch sử một giai đoạn biến cố đầy thăng trầm của đất nước, một góc nhìn khác về chế độ cũ về vua Bảo Đại về cuộc chiến thống nhất đất nước cũng như giai đoạn xây dựng lại đất nước sau này.

Cảm ơn tác giả đã biên soạn cuốn sách này đã giúp tôi biết thêm về một con người tài năng Bùi Kiến Thành, cũng như biết thêm về một giai đoạn thăng trầm của dân tôc.
5
830063
2017-02-26 09:50:56
--------------------------
287627
3809
Bác Bùi Kiến Thành là một cái tên quá lớn trong giới kinh tế, tài chính của Việt Nam rồi. Có đợt, xem thời sự hay những chương trình kinh tế sáng chủ nhật, thấy bác luôn luôn, và những ý kiến của bác thường rất đáng để chú ý. Nhưng khi đọc cuốn sách này, tác giả sẽ cho chúng ta thấy một Bùi Kiến Thành rất khác, mà không chỉ một cuộc đời có những nét về cả một dòng họ. Và qua cuốn sách, người đọc không chỉ thấy được đất nước thăng trầm qua một thời gian dài dưới một góc nhìn của người trẻ (người viết khá trẻ) nhưng tư liệu lại từ câu chuyện kể của một người đã đi qua bao biến động của cuộc sống với một cái nhìn nhẹ nhàng bao dung. Rất đáng đọc
5
625854
2015-09-02 21:50:25
--------------------------
286675
3809
Mình tình cờ nghe bạn giới thiệu đến cuốn sách này. Thực sự ban đầu mình thấy hơi lạ với tên của bác Bùi Kiến Thành nên trước khi quyết định mua, mình đã tìm hiểu sơ qua về lý lịch của bác. Tuy vậy, phải đến khi được đọc chính quyển sách này, mình mới hiểu thêm về cuộc đời của một chuyên gia kinh tế tài chính đã từng cố vấn cho ba đời thủ tướng nước ta. Không chỉ có vinh quang mà còn cả cay đắng trong đó. Đặc biệt hơn là những thông tin về bác mà không phải ai cũng biết. Lối viết khá lôi cuốn người đọc. Hi vọng các bạn cũng sẽ tìm thấy sự hứng thú khi đọc nó.
4
315772
2015-09-01 22:11:46
--------------------------
213643
3809
Kiến Thành: Kiến tánh thành Phật.
Đọc xong quyển sách tôi thật không tin là mình vừa nhìn lại một giai đoạn đầy biến động trong lịch sử dân tộc, đây có thể không phải là một quyển sách lịch sử. Nhưng nó lại chứa đựng quá nhiều những câu chuyện lịch sử thú vị từ cái nhìn của người trong cuộc, cuộc đời của bác Bùi Kiến Thành.
Người đọc sẽ lần lượt đi qua những năm tháng đầy biến động của đất nước, chúng tà cùng bác Thành nhìn lại quãng đường từ thời Pháp, đến thời Ngô Đình Diệm, rồi bác qua Pháp làm bất động sản rồi lại về nước giúp CS để mở cửa phát triển đất nước. Trong từng giai đoạn đã qua bạn sẽ không bao giờ thấy được sự thù hằn, hay tức giận của bác Thành (dù gia đình bác đã gần như mất sạch tài sản trong những năm tháng chiến tranh). Chính sự nhẹ nhàng mà sâu lắng, đơn giản mà lại rất thâm thúy đã làm cho cuộc đời của bác Thành trở nên đặc biệt lạ thường. 
Đây là cuốn sách đáng đọc vì nó sẽ cũng cấp cho chúng ta những góc nhìn rất khác về một giai đoạn thú vị của lịch sử VN
4
361233
2015-06-23 23:30:06
--------------------------
