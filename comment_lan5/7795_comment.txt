486122
7795
Lời đầu tiên mình xin chân thành gửi lời cảm tới nhà sách Alphabook và những đóng góp của dịch giả Tiểu Khê.

Mình sắp thi vào samsung nên mình muốn đọc nhiều tài liệu về SS nhưng thực sự đây là một sự đầu tư chưa khôn ngoan lắm cho cuốn sách. Rất ấn tượng bởi bìa sách nhưng lời văn trong cuốn sach khiến mình cảm thấy: dịch giả hoặc tác giả chưa đi sâu lắm về những chiến lược hoặc mới chỉ nắm sơ bộ phần nào các chiến lược. 

Lời văn viết khá vội và không có sự đầu tư kĩ lượng cho ngôn từ. Đọc qua mình cũng có thể đoán: dịch giả hoặc tác giả chỉ đang cố gắng cho ra 1 cuốn sách thị trường. Hoặc đôi khi thấy buồn cho nhà sách Alphabook khi lịch duyệt qua quát trước khi ấn hành. 

Đây cũng là bài học quý giá cho tôi khi tin tưởng vào hào quang của bìa sách. Kính mong các đọc giả khác hãy thông cảm và dành thời gian cho những cuốn sách tương xứng với giá trị của lời quảng cáo ạ.

Trân trọng
1
1766849
2016-10-09 21:29:47
--------------------------
473627
7795
Có thể do mình đọc khi có những trông đợi quá nhiều vào bài học từ một tấm gương của tinh thần và sự quyết liệt, cũng có thể do mình không quen lắm với dạng sách kinh tế... dù thế nào đi nữa thì với mình, đây thật sự là cuốn sách gây thất vọng khiến mình rất tiếc số tiền bỏ ra. Tuy trên tựa đề có "lựa chọn chiến lược" và "kỳ tích", lời giới thiệu cũng có chi tiết câu nói nổi tiếng của Lee Kun Hee khá ấn tượng nhưng nội dung sách lại là những lời thuật đều đều, chung chung và buồn chán mà thôi.
2
80143
2016-07-10 19:34:33
--------------------------
349188
7795
Điều mình thích nhất ở cuốn sách này và quyết định mua nó là thông điệp nhắn gửi đến tương lai và là tầm nhìn xa  của Lee Kun Hee tài ba  : nếu ta không thay đổi Hàn quốc về mặt công nghệ thì ta sẽ trở nên tụt hậu. Mình thích cách mà cuốn sách được  bổ sung những biểu đồ , tỉ lệ phần trăm ,... những thứ ấy không chỉ cho mình thấy được gã khổng lồ công nghệ xứ Kim chi - Samsung không chỉ giỏi và tài năng , Mình còn có thể thấy được mỗi lần Samsung có chiến lược gì đều câu được khách hàng một cách thông minh . Ngoài ra , cuốn sách này làm thích thú với lĩnh vực mà xưa nay mình nghĩ vốn khô khan -  công nghệ  thông tin. Theo mình ông là một tấm gương cho bao người dân Hàn quốc học hỏi . O6ng đã thay đổi được cái nhìn của thế giới đối với không chỉ đối với Samsung mà còn với nước Hàn.
5
781966
2015-12-07 21:41:43
--------------------------
320021
7795
"Bất ngờ" đó chính là hai chữ tôi muốn nói lên khi đọc xong cuốn sách. Tôi "bất ngờ" vì tốc độ tăng trưởng cao của tập đoàn Samsung. "Bất ngờ" về ông- Lee Kun Hee-một con người tài ba đã dìu dắt được một tập đoàn hạng 2 trở thành tập đoàn hàng đầu thế giới, đánh bại bao đối thủ tầm cỡ như sony, panasonic,apple...
"Bất ngờ" về những chiến lược, lựa chọn kinh doanh mạo hiểm- mà trước đó ai cũng nghi ngờ rằng Lee Kun Hee  sẽ thất bại... nhưng không, ông đã làm được thành công đã mỉm cười với ông-một con người có ý chí tiến thủ không ngừng.
 Bằng một giọng văn truyền cảm, tôi nghĩ tác giả và cuốn sách đã thực sự thành công khi đã đốt cháy trong tôi một niềm tin mãnh liệt vào ước mơ và hai từ "hoài bão". Tôi cũng có ước mơ trở thành một doanh nhân vì lẽ đó cuốn sách đã phần nào truyền và dạy cho tôi nhiều điều về kinh doanh-về những điều mà một doanh nhân cần có. 
5
702741
2015-10-10 10:57:34
--------------------------
247667
7795
Rất cảm ơn người đã viết ra cuốn sách hay như thế này .Nó giúp cho không chỉ riêng tôi mà tất cả những người đang đọc quyển sách này thêm hành trang mới để bước vào tương lai mới đang chờ đón chúng tôi,và cuốn sách còn là công cụ hữu ích đối với những người đang mất phương hướng trong cuộc sống hiện tại và chưa có mục đích rõ ràng trong tương lai .Thật là khâm phục người đứng đầu samsung từ một thương hiệu bình dân trở thành một trong những doanh nghiệp hàng đầu thế giới hiện nay 
5
324187
2015-07-30 12:14:15
--------------------------
160771
7795
Samsung hiện nắm trong tay một số lượng lớn người dùng smartphone và được mệnh danh là "ông vua smartphone", vượt qua hàng loạt các nhà sản xuất, các thương hiệu smart phone khác như Sony, Lenovo, HTC, ... và đặc biệt là Apple của Steve Jobs huyền thoại. Tất cả các thành tựu đó in đậm dấu ấn của người đứng đầu tập đoàn chaebol này, ngài Lee Kun Hee.
Tầm nhìn xa trông rộng, ý chí mạnh mẽ của một doanh nhân và cách mà ông thay đổi một Samsung trì trệ và làm cho nó hóa rồng đã được mô tả cụ thể trong tác phẩm này. Tôi đặc biệt thích tác phẩm ở chỗ ông đã thay đổi toàn bộ Samsung bằng cách rất độc đáo. Quyển sách giúp cho các nhà quản trị, lãnh đạo tự tin hơn khi ra quyết định của mình.
5
387632
2015-02-25 15:15:21
--------------------------
131523
7795
Giọng văn phiến diện, chỉ tập trung khai thác những thành tựu và qua đó ca ngợi ( một cách quá đáng ) vị chủ tịch tập đoàn Samsung. Đọc khắp các trang chỉ thấy "một vị anh hùng, cứu tinh, xuất sắc, tài năng"...đem lại cảm giác rất nhàm chán. 

Không phủ nhận những thành tựu và tài năng mà vị chủ tịch này mang đến cho tập đoàn Samsung, đưa thương hiệu vươn ra tầm thế giới, nhưng thật đáng buồn là lối hành văn một chiều như thế này đã làm giảm tính hấp dẫn của cuốn sách.
1
134717
2014-10-25 17:57:40
--------------------------
122858
7795
Cuốn sách chia sẻ khá chung chung về Lee Kun Hee và những gì ông làm cho Samsung, có cảm giác đang đọc một cuốn sách về một anh hùng một tay xoay chuyển cả tập đoàn, nhưng lại không nói rõ chi tiết, cũng không biết tại sao, vì cái gì mà ông phải làm những việc như vậy. 

Giọng văn dẫn dắt cũng cứ đều đều, đôi lúc tâng bốc ông Lee Kun Hee (dù tác giả nói là không) nhưng lại không chia sẻ được là tại sao quyết định đó thật sự sáng suốt. 

Đọc được hơn 1/2 cuốn và không muốn tiếp tục đọc nữa.
2
173079
2014-08-28 15:01:27
--------------------------
111744
7795
Cuốn sách miêu tả nhiều về Lee Kun Hee và những quyết định được cho là sáng suốt, có tính " chiến lược" dẫn tới sự thành công của Sam Sung ngày hôm nay.
Nhưng không đề cập nhiều vì sao ông ta có thể ra những quyết định đó, có những thành công đó. Lee Kun Hee có được vị trí lãnh đạo Samsung có thể vì cha ông ta không còn sự lựa chọn nào khác chứ không phải bản thân ông ta xứng đáng với vị trí đó. 
Giọng văn cũng không hấp dẫn và lôi cuốn, chẳng cảm nhận được sự nhiệt huyết hay đam mê gì. Thấy hơi tiếc tiền vì mua cuốn này, chắc mình đọc một lần rồi thôi
2
286764
2014-05-03 09:17:41
--------------------------
108636
7795
Là một quyển sách khá chung chung về những hoạt động của Lee Kun Hee, và không làm hài lòng bản thân tôi. Cuốn sách làm chúng ta liên tưởng đến một siêu sao, thay vì một người bình thường, và các mối quan hệ của ông - đó là thứ dở nhất của cuốn sách này.

Cuốn sách nêu ra những bước ngoặc, những lựa chọn chiến lược mà SamSung thực hiện sau khi Lee Kun Hee thừa kế cỗ máy lỗi thời SamSung. Dường như đang kể về một ông vua đang trị vì đất nước của mình, chứ không nhiều là hành động của những nhà quản trị. Hơn nữa ji Pyeong Gil không đưa ra những phân tích SWOT nào cụ thể cho những tình huống cũng như cho chính Lee Kun Hee. Việc này làm giảm giá trị của cuốn sách.

Cuốn sách không mang lại nhiều giá trị cho bản thân như tôi kỳ vọng, ngoài một nguồn cảm hứng cho thế giới màu hồng ra, nó là thứ không nên lưu giử bên mình. Vì nó là một thái cực, chứ không phải toàn cục của sự việc.
3
287948
2014-03-20 23:13:02
--------------------------
