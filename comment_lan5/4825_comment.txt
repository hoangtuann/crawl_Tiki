221646
4825
Đây là cuốn cuối cùng trong bộ 4 cuốn 345 Câu khẩu ngữ tiếng Hán. Trong tập cuối này có thêm khoảng 98 câu nói tiếng Trung thông dụng được giới thiệu qua 16 bài học từ bài 49 đến bài 64. Sách có mục luyện tập cho mỗi bài học, có danh sách hệ thống lại từ vựng đã dùng trong sách rất tiện cho việc tra cứu. Một số bài tập được phỏng theo kỳ thi HSK để giúp bạn làm quen dần với kỳ thi nếu bạn có ý định thi. Giống như ở 3 tập đầu, phiên âm pinyin vẫn được dùng dễ dàng cho việc đọc các câu tiếng Trung.
4
470937
2015-07-04 09:51:47
--------------------------
