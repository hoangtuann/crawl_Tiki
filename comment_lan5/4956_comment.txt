285062
4956
Đây là sách tranh dành cho thiếu nhi. Các bức ảnh rất đẹp và có hồn. Nỗi mất mát thể hiện qua phong cảnh và ánh mắt của các con vật rất sống động. Cái chết là điều không ai muốn nhưng nếu nó xảy đến, chúng ta cần chấp nhận. Trẻ thơ cũng cần hiểu điều này. Bố mẹ có thể mua về để đọc cùng con cái mình và trò chuyện với bé, để bé tiếp cận với vấn đề nhạy cảm này một cách trong sáng và găn gũi nhất, như một cách giúp trẻ mạnh mẽ hơn.
4
273261
2015-08-31 15:43:36
--------------------------
247446
4956
Tất cả chúng ta từ khi sinh ra đều đã từng chứng kiến cái chết của vật nuôi hoặc người thân, bạn bè. Trải qua những việc như vậy từ trẻ em đến người lớn cũng đau buồn và cần sự an ủi. 

Tôi rất nhớ cảm giác trống rỗng, buồn bã, day dứt, giày vò, ân hận của mình vào những năm đầu học cấp 2 khi người giúp việc nhà tôi bị đột quỵ giữa đêm, dù bố tôi đã cố gắng cõng ông thật nhanh sang chạy bệnh viện gần nhà nhưng ông vẫn không qua khỏi. Khoảng thời gian sau đó, rất nhiều người hàng xóm của gia đình tôi cứ gặp tôi là hỏi han, tò mò... tôi trả lời nhát gừng, cáu kỉnh và tôi rất ghét những câu hỏi đó. Tôi thu mình lại, chỉ muốn ở 1 mình và đừng ai hỏi gì hết để tôi nghĩ về khoảng thời gian đã qua, tôi nghĩ về những ngày tháng chính ông đã về quê đón chị em tôi lên Hà Nội khi cha mẹ tôi quá bận ... Tôi cần 1 ai đó quan tâm, nói chuyện một cách tế nhị về cái chết, về sự chia ly, về cuộc sống vẫn tiếp diễn với những người còn đang sống. Và hình như khi đó quanh tôi chẳng ai có ý nghĩ chia sẻ với tôi điều đó hoặc chẳng ai hiểu và tìm cách nói chuyện với chị em chúng tôi. Hoặc họ muốn mà chưa có khả năng diễn đạt đồng cảm về những mất mát đau thương ấy..

Ba mẹ tôi luôn mong muốn cho con cái chúng tôi lớn lên trong đầy đủ, có nhiều niềm vui và hạnh phúc nhưng trong những ngày tháng đó chắc ba mẹ tôi còn bận rộn lo lắng nhiều việc khác và thực sự chắc họ cũng chưa quan tâm lắm đến cảm xúc của chị em tôi. Họ còn có quá nhiều thứ việc để lo và có lẽ những thứ đó với họ nó quan trọng hơn là ngồi để nói chuyện cùng chúng tôi, giúp chúng tôi đủ mạnh mẽ hơn, đối diện với nỗi buồn, nguôi ngoai bớt phần nào ... Trẻ con chúng tôi cũng hiểu và có cảm xúc trước những biến cố ấy ....

Tôi thích cuốn truyện này, nó không hấp dẫn hồi hộp hay gay cấn, dễ thu hút trẻ em nhưng lời lẽ nó mượt mà, nhờ nó tôi có cơ hội chia sẻ với các con mình về nỗi đau, về mất mát, về sự may mắn khi mình còn được sống trong cuộc đời này. Ngoài các tác phẩm văn học, các bộ truyện tranh hay, các cuốn nói về khoa học..  tôi cũng rất thích các cuốn sách dạy kỹ năng sống nhẹ nhàng (ít lý thuyết) như cuốn sách này. Nó đồng hành cùng tôi trong công cuộc dạy các con mình can đảm, mạnh mẽ, sẵn sàng đối diện, vượt qua các nỗi đau trên bước đường trưởng thành.

Và cuối cùng khi đọc xong cuốn sách tôi luôn nói với các con mình: Hãy sống, làm việc, đối xử với mọi người để khi nằm xuống mình không còn phải day dứt rằng "Vẫn còn bao điều muốn nói..."


4
438149
2015-07-30 10:20:51
--------------------------
