433193
6735
tác phẩm này được viết khá tốt, dịch cũng tốt và bìa đẹp. Từ trước đến nay có rất nhiều người phê phán nhà Nguyễn một cách khá khắc khe, nhưng khi đọc cuốn sách này, có lẽ người ta sẽ nhận ra được những "thách thức" vô cùng to lớn của triều đại này trong lịch sử nước nhà lúc bấy giờ, từ đó có cái nhìn toàn diện hơn về nhà Nguyễn. Đây là một tác phẩm nghiên cứu hay và đáng đọc nếu bạn muốn nghiên cứu về lịch sử Việt Nam thời Nguyễn và các mối bang giao của VN thời này.
4
52004
2016-05-20 15:40:29
--------------------------
388209
6735
Cuốn sách này không đơn thuần chỉ nói về vấn đề sử học khô khan nhưng tác giả người Nhật đã cung cấp đọc giả 1 hình ảnh về thời nhà Nguyễn . Khi người dân sống trong cơ cực và lo sợ, những vấn đề , biến cố gây rối ren dưới thời các vua triều Nguyễn . Họ còn phải đối mặt với 1 đất nước hoàn toàn xa lạ về địa lí , màu da , văn hóa và ngôn ngữ .Đi sâu vào sách , Tsuboi một mặt đưa ra lí luận chặt chẽ các mối quan hệ và tình hình an ninh-chính trị bị phá vỡ , hết các cuộc nổi loạn về tôn giáo ở Đàng Trong thì lại đến những bọn thổ phỉ tàn quân của Thái bình thiên quốc quấy phá , cướp bóc ở các tỉnh phía Bắc .Rồi đến các tàu chiến vạm vỡ bên Pháp tấn công các cửa biển hòng đòi được một hiệp ước ...
Vì sách thuộc thể loại nghiên cứu và là 1 luận án của tiến sĩ người Nhật nên đa phần sách viết rất khó hiểu , theo kiểu Hàn lâm và chuyên ngành . Nên những ai có ý định đọc cho qua thì rất khó nắm được các sự kiện hay biến cố mà sách cung cấp
4
1090368
2016-02-28 13:23:39
--------------------------
354420
6735
Tôi rất thích đọc lịch sử Việt Nam do người nước ngoài viết,  với tôi đó là sự công bằng . Những tư liệu được sử dụng trong tác phẩm đều được khảo cứu kĩ lưỡng,  đa chiều. Đọc sách ta có thể cảm nhận được suy nghĩ của từng nhân vật lịch sử, ta sẽ cái nhìn chính xác hơn,  sâu sắc hơn với từng vấn đề,  sự kiện đã từng xảy ra. Nguyên nhân,diễn biến, kết quả vốn dĩ không đơn giản như trước đây ta từng biết. Nằm trong đó là cả một chuỗi những câu chuyện hay thú vị. Để biết thêm chi tiết xin mời đọc tác phẩm :-)
5
520489
2015-12-17 20:59:01
--------------------------
308680
6735
Sách là luận văn tiến sĩ của một người Nhật tìm hiểu về một gian đoạn lịch sử Việt Nam rất quan trọng. Dẫn chứng sách chủ yếu được trích lục từ các kho lưu trữ của Pháp nên có thể thấy được góc nhìn của tác giả thể hiện cách nhìn của nước khai thác vùng Viễn Đông. Chính vì lẽ đó nó trở thành nguồn tài liệu quan trọng giúp người đọc hiểu hơn về đời sống của cả dân chúng và giới chính trị. Ngoài ra việc tìm hiểu liên kết các sự kiện tại Trung Quốc trong giai đoạn lịch sử này cũng phần nào làm rõ việc Pháp cụ thể hóa sự hiện diện của mình tại An Nam.

Điều quan trọng nhất là tác giả hạn chế bình luận mà cung cấp nhiều sự kiện biến cố dành cho độc giả sau khi đọc có thêm say mê để tìm đọc thêm các góc nhìn khác.
4
664053
2015-09-18 20:20:27
--------------------------
299724
6735
Mình rất thích lịch sử, đặc biệt là một khoảng sử dường như bỏ trống của nước mình, quảng đầu thế kỷ XIX đến khi Pháp hoàn thành xâm lược Bắc Kỳ, vậy nên mình đã nhanh chóng quyết định mua quyển này. Trước tiên cần phải nhấn mạnh, dù gì thì đây cũng là một Luận án tiến sỹ, cho nên cách viết hết sức hàn lâm, dùng nhiều từ ngữ chuyên ngành, có thể sẽ gây chán cho bạn đọc. Tuy nhiên, nếu bỏ qua vấn đề đó thì một rừng kiến thức mới lạ, với góc nhìn của một người ngoài cuộc (tác giả người Nhật mà), ắt hẳn sẽ khiến chúng ta có thêm nhiều vấn đề để suy ngẫm. Bìa sách in đẹp, bắt mắt, gợi suy nghĩ, giấy in chất lượng khá tốt, chữ in đẹp, ít lỗi. Quyển sách rất đáng giá.
4
564333
2015-09-13 14:46:02
--------------------------
286732
6735
Cùng với Người Pháp và người Annam: Bạn hay thù, đây là công trình thứ 2 mô tả tình cảnh Việt Nam vào nữa thế kỷ đầu tiên từ khi thực dân Pháp xuất hiện 1847-85. Công trình có giá trị cho những nhà nghiên cứu về hậu-thực dân (post-colonialism) quán sát về những biến đổi xã hội, văn hóa từ khi chịu sự ảnh hưởng của thực dân. Công trình còn cho thấy ứng xử của các quốc gia trên trường quốc tế trong giải quyết các vấn đề xung đột. Sách hàm chứa nội dung lịch sử và giá trị khoa học cao.
5
390132
2015-09-01 22:52:56
--------------------------
253614
6735
Những trang đầu cuốn sách không thực sự lôi cuốn được tôi nhưng tôi đã cố gắng đọc và tự nhủ, đọc một luận án tiến sĩ không thể giống như một tiểu thuyết được, hãy kiên nhẫn và đọc kỹ từng chữ, từng trang. Quả nhiên, cuốn sách đã thu hút tôi bằng cách nhìn nhận, đánh giá vấn đề rất mới, khác hẳn cách mà các sử gia Việt Nam vẫn viết xưa nay, giúp tôi có cái nhìn sâu hơn, đa diện hơn về một giai đoạn đẫm nước mắt của dân tộc. Tôi thích đoạn kết, đó là những bài học  lịch sử đắt giá, có giá trị cho chúng ta đến hôm nay và mai sau.
5
401570
2015-08-04 16:13:06
--------------------------
246897
6735
Đây là một trong những cuốn sách gối đầu giường trước khi đi ngủ cua tôi. Cuốn sách cung cấp cho người đọc một cái nhìn khái quát và mới mẻ về một giai đoạn đầy rối ren và đau thương của lịch sử dân tộc. Tác giả là người Nhật nên nhìn nhận vấn đề không theo lối mòn của các sách sử Việt Nam về cuối triều Nguyễn thướng thấy. Tuy nhiền cũng có một điểm cần lưu ý là sách được viết từ luận án tiến sĩ của tác giả nên lối hành văn và tiếp cận vấn đề giống như một bài viết khoa học nên sẽ gây một số khó khăn cho người đọc ở những phần đầu, nhưng càng vào sâu các chi tiết càng hấp dẫn và co giá trị. Đây là một cuốn sách không thể bỏ qua cho những người quan tâm và yêu thích lịch sử dân tộc
5
142370
2015-07-30 09:31:34
--------------------------
235270
6735
"Nước Đại Nam Đối Diện Với Pháp Và Trung Hoa" là cuốn sách về lịch sử Việt Nam nhưng được viết bởi một sử gia người Nhật - vì thế nên tránh được các hạn chế bởi cái nhìn một chiều thường được nhận thấy ở các tác phẩm lịch sử trong nước.  Cuốn sách chuyển tải những nội dung rất mới về một giai đoạn trong lịch sử - giai đoạn giao thoa giữa thời kỳ Pháp thuộc và những ảnh hưởng của Trung Hoa. Những kiến thức này không được giảng dạy trong trường học. Đây là cuộc đối diện khắc nghiệt bởi trong sự giao thoa Hoa - Pháp, nước Đại Nam phải tự vận động để giữ lấy bản sắc của riêng đất nước này. Một vấn đề lịch sử được nhìn dưới cái nhìn của một sử gia của một đăt nước thành công trong việc tiếp cận văn hóa phương Tây, do đó cuốn sách cũng mang lại những sự nhìn nhận rất khác biệt.
Tuy nhiên, cuốn sách mang tính học thuật cao, do đó không dễ tiếp  thu các vấn đề nếu chỉ tiếp cận cuốn sách ở bề nổi. 
5
24486
2015-07-20 23:08:51
--------------------------
220405
6735
Tuy là một cuốn sử viết ra bởi một người Nhật nhưng nội dung đề cập đến thời kỳ Tự Đức ở nước ta trước những biến động xã hội và sự xâm thực của văn hoá ngoại lai - điều mà triều đình Huế đã thất bại trong khi ở Nhật Thiên Hoàng lại tiến hành duy tân thành công. Đọc để suy ngẫm, để nhìn lại lịch sử dưới một lăng kính mới mẻ, nhìn về một thời kỳ của nước nhà ta mà trường học ít khi đề cập đến, và đọc để góp phần vào nền văn hoá Việt Nam hồn hậu. 

5
120276
2015-07-02 15:52:23
--------------------------
162283
6735
Đây là một công trình nghiên cứu công phu và rất chi tiết đúng theo phong cách của một công trình nghiên cứu khoa học. Sách đã cung cấp nhiều tư liệu và quan điểm về một thời kỳ lịch sử quan trọng của dân tộc, thời kỳ mà cùng lúc đó Nhật Bản đã duy tân thành công và vươn lên thành một quốc gia hùng mạnh. Trong khi đó, Việt Nam có những điều kiện và hoàn cảnh tương đồng nhưng đã không làm được như vậy.
Tuy nhiên, cá nhân tôi thấy sách khá khó đọc, phù hợp hơn với những người có nhu cầu nghiên cứu chuyên sâu về chủ đề này.
4
196707
2015-03-01 21:57:45
--------------------------
157926
6735
Có lẽ ít người ở độ tuổi teen như mình lại thích đọc truyện lịch sử... Mình cũng thế! Nhưng sau khi đọc quyển này mình đã có những suy nghĩ khác. Mình thấy đọc truyện lịch sử như thế này rất có ích, giúp ta hiểu biết được ngày xưa đã xảy ra những gì và như thế nào... Đặc biệt ở câu chuyện này là tác giả của truyện này không phải là người Việt mà chính là một người nước ngoài Yoshiharu Tsuboi. 
Nhưng điểm chưa được của truyện lại ở chỗ khác. Nó hơi khó hiểu, nó bắt ta phải suy ngẫm từng chút từng chút nội dung của nó
4
356391
2015-02-10 19:33:39
--------------------------
126754
6735
Lần xuất bản này được Nhã Nam trình bày rất đẹp, dễ đọc. Tác giả nhìn nhận lịch sử nước ta dưới con mắt một người nước ngoài và là một học giả nên tính khách quan và biện chứng rất cao. Người đọc dễ dàng hình dung được những bộ phận cấu thành nên xã hội Việt Nam lúc bấy giờ dẫn đến hiểu được những mâu thuẫn và cách vận động của triều đình Đại Nam thời Tự Đức. Đơn cử như tầng lớp "sĩ phu và văn thân" đóng vai trò quan trọng thậm chí là lũng đoạn nền chính trị được phân tích rõ ràng từ xuất thân lều chõng hay quý tộc của họ, trong khi chính người Việt Nam ta học sử nhiều khi không nhận ra quyền lực mềm của giới văn sĩ này. Mọi kết luận đều có dẫn giải kĩ lưỡng từ cội rễ văn hóa và truyền thống của dân tộc ta nên rất thuyết phục.
Tuy nhiên cuốn sách vốn là luận án tiến sĩ nên chất hàn lâm và học thuật rất cao, không thể đọc liền một mạch từ đầu đến cuối được, đặc biệt là không dành cho ai thường hay mở phần cuối truyện xem kết thúc trước. Khuyến khích các bạn mong muốn tìm hiểu lịch sử một cách đa chiều, sâu sắc và rút ra những bài học về một thất bại lớn của nước nhà trước sự biến chuyển của thế giới một thế kỷ trước để chuẩn bị đối mặt với những biến đổi còn lớn hơn mà thế hệ chúng ta sắp phải đương đầu.
3
114275
2014-09-20 11:19:17
--------------------------
