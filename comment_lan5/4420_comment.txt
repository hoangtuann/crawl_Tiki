325391
4420
Phàm là những cuốn viết về Hà Nội thì mình đều mong muốn mua và đọc hết. Mặc dù sinh ra và lớn lên ở đây và ngót nghét 20 năm sống ở thủ đô nhưng càng lớn mình càng cảm giác không biết gì về Hà Nội, có đôi lúc mình muốn cảm nhận rõ hơn về Hà Nội, hiểu hơn về Hà Nội nên thật sự rất trân trọng và thích những cuốn sách viết về Hà Nội. 
Sách của NXB TRẺ nên được trình bày cẩn thận, bìa đẹp cứng cáp. Cho dù là có thật nhiều cuốn về HÀ Nội nhưng được viết bởi nhiều người khác nhau, dưới nhiều góc nhìn và sự cảm nhận khác nhau nên đọc không bao giờ chán.
Đây cũng là 1 cuốn sách hay để các bạn biết thêm những điều thú vị về Hà Nội.
4
52328
2015-10-23 13:11:30
--------------------------
309541
4420
Mình đã từng đọc rất nhiều về Hà Nội. Lần này đọc Đỗ Phấn, mình lại có thêm một cách nhìn khác. Đỗ Phấn viết về Hà Nội dưới góc nhìn của một người đã gắn bó với nó rất rất lâu và thực sự hiểu Hà Nội. Những chuyện như tán phét lê la từ chuyện quá khứ rồi hiện tại ,và truyền thống , lại dân số, cả những sinh hoạt… Những suy tư Đỗ Phấn gợi ra làm mình gần cái hồn của Hà Nội, một cái hồn khác những gì mình đã từng được chia sẻ.
5
101774
2015-09-19 00:28:38
--------------------------
