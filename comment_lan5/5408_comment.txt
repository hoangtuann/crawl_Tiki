366867
5408
Như cái tên của quyển sách, đó là hành trình tìm kiếm và khám phá mơ ước bản thân của những đứa trẻ trong quãng thời gian đến trường. Sau mỗi mẩu chuyện của hai đứa trẻ nhỏ xung quanh trường học là những bài học được tác giả rút ra thành những bí kíp để giúp những người chua nhận ra được mơ ước tiềm ẩn trong bản thân họ. Quyển sách còn khá đơn giản về nội dung , hình vẽ minh họa khá bắt mắt, chất giấy in màu dày dặn, chất lượng. Rất phù hợp với những bé từ lớp 5 đến lớp 8 .
3
584199
2016-01-10 22:17:34
--------------------------
245193
5408
Câu chuyện ý nghĩa này nói về một cặp bạn thân tên là Rhuda và Mai Geum Mi.Rhuda là nhạc sĩ nổi tiếng,Mai Geum Mi là nghệ sĩ thực thụ,Dan Ho là ca sĩ trứ danh,còn cô nhóc Yoo Ni là một tay trống cừ khôi.Tương lai sẽ như vậy đó.Nhưng,để thực hiện được giấc mơ tuyệt vời đó,bốn bạn nhỏ đã phải từng bước xác định mục tiêu,xây dựng định hướng bản thân cũng như nỗ lực học tập không ngừng.Và rồi ban nhạc Băng Dán Lấp Lánh đã ra đời,nhận được sự ủng hộ của tất cả mọi người.Dù còn bao khó khăn chờ đợi ở phía trước,nhưng nhờ có định hướng -ngôi sao lấp lánh dẫn đường,Băng Dán Lấp Lánh hứa hẹn sẽ có tương lai tươi sáng. Sách rất hay!
5
374754
2015-07-28 19:48:33
--------------------------
215386
5408
Đây là cuốn sách hay, hấp dẫn và cần thiết cho các bạn trẻ. 
Hình vẽ dễ thương ngộ nghĩnh, bìa vàng bắt mắt sáng tạo, giấy đẹp.
Nội dung hay, phù hợp với lứa tuổi từ 7 đến 15, trình bày khoa học và có hướng dẫn cụ thể. Mỗi phần lại có hình minh họa sinh động và note. 
Tuy nhiên, với giá bìa 74.000 thì quyển sách hơi mỏng và nội dung vẫn chưa thực sự cụ thể với các em bé khoảng 7-8 tuổi.
    Mặc dù vậy, đây là một quyển sách hay, ý nghĩa và thiết thực cho những bạn đang bỡ ngỡ về cuộc đời mình.
3
563925
2015-06-26 11:24:34
--------------------------
177360
5408
Ước mơ của tôi là gì nhỉ?
Từ khi tôi 15 tuổi, tôi luôn tự hỏi bản thân mình rằng ước mơ của tôi thật sự là gì.
Câu hỏi đó theo tôi suốt những năm tháng của cái tuổi mới lớn, tôi mải miết đi tìm ước mơ của mình. 
Tìm hoài, tìm hoài,...
Vào sinh nhật tôi 18, bạn thân của tôi đã tặng tôi 1 món quà thật ý nghĩa, đó là cuốn sách này. 
Tôi biết được mình đã sai.
Và đọc cuốn sách tôi nhận ra cách định hướng mọi thứ trong tương lai theo kế hoạch.
Nó giúp tôi học cách tin tưởng bản thân mình hơn để theo đuổi sở thích, đam mê của bản thân (tôi đã nhận ra ước mơ thật sự luôn bên trong mình) 
Tôi thật sự cảm ơn tác giả, cảm ơn người bạn của tôi đã khiến cho tôi nhận ra điều tuyệt vời này. Cảm ơn tất cả.
Tôi yêu bạn, yêu cuốn sách này, tôi yêu ước mơ của tôi.
5
584396
2015-04-02 23:01:26
--------------------------
139211
5408
"Mơ Ước Ở Nơi Đâu" là một cuốn sách rất hay, cung cấp kĩ năng mềm cần thiết cho bạn định hướng tương lai. Cuốn sách có bìa ngoài màu vàng nghệ, khá nổi. Chất lượng giấy tốt, mịn đẹp,... Nội dung mới mẻ, hấp dẫn, bố cục rõ ràng, khoa học,.. phù hợp với nhiều lứa tuổi đặc biệt là các bạn trẻ. Cuốn sách cung cấp kĩ năng cơ bản và cần thiết giúp bạn định hướng được con đường tương lai mình sẽ đi như thế nào,... Qua đó, giúp bạn cảm thấy tự tin hơn vào bản thân, vững bước trên con đường đã chọn và về đích một cách nhanh nhất.
5
414919
2014-12-06 14:46:15
--------------------------
