472494
8303
Đây có thể xem là một trong những tiểu thuyết giá trị nhất của ông già Nam bộ Sơn Nam. Tiểu thuyết có câu chuyện lôi cuốn, hấp dẫn. Bối cảnh tái hiện lại Nam kỳ trước khi Pháp đến, từ cảnh quang thiên nhiên đến lối sinh hoạt, các đại gia giàu có thống trị xứ biển Tây một thời… Thậm chí, đến tín ngưỡng dân gian Nam bộ buổi đầu, cũng được nhà văn tài ba chú ý  khai thác, ông Đạo Đất là một trong những nhân vật xuyên suốt tác phẩm, làm nổi bật đời sống tinh thần của cư dân Nam kỳ trên vùng đất mới khai khẩn ngày xưa. Những điều đó, ngoài tạo nên giá trị văn học, còn mang giá trị văn hóa sâu sắc, tái hiện lại một không gian - thời gian văn hóa là xứ Nam kỳ thời Nguyễn.
5
78994
2016-07-09 17:22:59
--------------------------
337455
8303
Vẫn với ngòi bút đặc sắc viết về Nam Bộ, Bà Chúa Hòn tái hiện lại bối cảnh của không gian miệt vườn vùng đồng bằng sông Cửu Long. Nhưng nếp nghĩ, sinh hoạt của cư dân vùng châu thổ. hình ảnh chân thực đã giúp Bà chú hòn được lựa chọn cho một bộ phim lôi cuốn. Tập sách được in với minh họa đẹp, chất lượng cao đảm bảo việc trân quý những tác phẩm của "ông già Nam Bộ". Tôi lấy làm tiếc một điều nhà xuất bản trẻ không tái bản bìa cứng để phục vụ cho giới yêu sách có được một bộ sách đẹp của nhà văn Sơn Nam.
5
390132
2015-11-14 01:51:27
--------------------------
