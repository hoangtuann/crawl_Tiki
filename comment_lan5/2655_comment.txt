459563
2655
Bước Thịnh Suy Của Các Triều Đại Phong Kiến Trung Quốc là bộ sách rất đáng đọc, cung cấp những sử liệu chi tiết cũng như một số kinh nghiệm và trí tuệ lịch sử dựa trên nền tảng nghiên cứu và phân tích bộ Tư Trị Thông Giám “Gương Chung Cho Người Cai Trị”. Qua bộ sách, chúng ta không những có kiến thức về các sự kiện lịch sử mà còn có cái nhìn tổng quan về các khía cạnh khác như: kinh tế, chính trị, xã hội, ngoại giao, những bài học trị loạn và thịnh suy, ... kết hợp với ngòi bút bình luận, phân tích sâu sắc của nhóm tác giả do Cát Kiếm Hùng làm chủ biên đã cho chúng ta những suy ngẫm khách quan của người đời sau về các nguyên nhân và bài học rút ra được từ quá trình hưng thịnh rồi suy vong của chế độ phong kiến Trung Quốc kéo dài hàng ngàn năm lịch sử.
Sách được đóng bìa cứng, in chữ sắc nét trên nền giấy tốt. Tuy vẫn còn có một số lỗi chính tả nhưng nhìn chung, đây là một bộ sách quý giá cho những bạn đọc đam mê lịch sử.
5
1375079
2016-06-26 11:07:58
--------------------------
