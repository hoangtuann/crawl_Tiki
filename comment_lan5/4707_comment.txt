483065
4707
Bìa sách đẹp, lạ lạ, giấy tốt, cuốn sách nho nhỏ trông dễ thương lắm. Tuy nhiên nội dung lại gây cho mình một sự thất vọng lớn, các tình tiết hơi nhạt nhoà, đôi khi tình tiết truyện qua quá nhanh làm rất mất hứng đọc. Mình có thể đoán được nội dung của sách nên càng cảm thấy hơi chán ấy, vài ý lại có vẻ không thuyết phục lắm. Nhưng khi đọc, câu văn cũng làm mình cảm thấy cái đen tối, sợ hãi của nhân vật. Được biết Miyuki Lê là một tác giả trẻ, chúc bạn hoàn thành tốt hơn ở những tác phẩm sau nha.
3
532372
2016-09-11 00:32:52
--------------------------
436641
4707
So với cuốn bóng đen trong học viện thì tác giả này viết khá hơn, tạm ổn, không trẻ trâu.
Nhưng nội dung thì chả có gì, ngay từ chương 2 đã ngờ ngợ việc đa nhân cách, cô chị không có thật, nhưng so với tác giả kia thì mình cho rằng đọc chơi cho vui cũng được.
Mình biết bạn Nhật kia không phải con gái ngay từ đầu rồi, tác giả cuối cùng cũng làm cho suy nghĩ mình đúng nên câu chuyện trở nên tầm thường hơn bao giờ hết.
Không li kì, không kịch tính, chỉ dành cho teen teen đọc được, chứ một đứa fan trinh thám, tác phẩm này không đáng được cho sao.
Đây là lần cuối đọc truyện của tác giả vn, bạn này chưa đến mức được gọi là nhà văn, văn phong nhàn nhạt không sức hút, hối hận vì tin tưởng tác phẩm của vn, không sâu sắc.
Vn chắc được mỗi Nguyễn Nhật Ánh với Hamlet Trương là giỏi. 
1
936775
2016-05-26 23:35:46
--------------------------
428215
4707
Mình nghĩ đây là lần đầu tiên mình được cầm trên tay một cuốn sách mang yếu tố kinh dị của một tác giả việt nam. Mình luôn nghĩ chỉ có những tác phẩm nước ngoài mới có.thể đem lại cho mình những cảm giác sợ hãi những mình đã nghĩ lại khi đọc xong cuốn sách này. Mặc dù nó cũng khá mới lạ nhưng văn phong có trau chuốt đã đem lại cho mình một ấn tượng nhất định. À, thì ra nỗi sợ hãi lại có hình thù nhất định như vậy, nỗi sợ hãi vô hình khiến tâm hồn chông chênh không định hình được như vậy à? Tóm lại mình khá hài lòng về cuốn sách này
3
1158644
2016-05-10 20:40:29
--------------------------
427342
4707
Đọc nhan đề quyển sách, chắc chắn ai cũng không khỏi tò mò. Đọc quyển sách này, tôi được trải nghiệm cảm giác sợ hãi khá rõ nét. Truyện mang yếu tố kinh dị, cũng là chủ đề đa nhân cách khá mới lạ. Tuy nhiên, tôi cảm thấy văn phong tác giả chỉ được trau chuốt ở phần đầu, khá lôi cuốn tôi vào câu chuyện, nhưng rồi đến những đoạn gần cuối, diễn biến câu chuyện diễn ra quá nhanh, như viết cho xong, tôi không còn cảm thấy bị cuốn hút nữa. bên cạnh đó lời thoại nhân vật không dứt khoát, khiến tôi không thích cuộc đàm thoại giữa các nhân vật. Tôi nghĩ nếu khắc phục, tác giả sẽ có được những tác phẩm vượt trội hơn.
3
1344843
2016-05-09 00:05:24
--------------------------
253980
4707
Thực sự đây là quyển truyện ngắn mang hơi hướng kinh dị với đề tài song nhân cách khá mới mẻ tại Việt Nam . Ban đầu mình cũng không nghĩ nội dung lại gây cấn đến vậy , nhiều lúc làm mình thật sự " bị lừa " một cách bất ngờ . Tuy nhiên , bối cảnh và lời nói nhân vật có hơi hướng " Nhật hóa " không tạo cảm giác gần gũi , không gây thuyết phục người đọc . Dù vậy với một tác giả trẻ với tác phẩm đầu tay như thế này thực sự là có tiềm năng . Mình mong chờ các tác phẩm tiếp theo .
3
438372
2015-08-04 21:53:34
--------------------------
