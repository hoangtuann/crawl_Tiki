459761
8210
Sách mỏng, in màu, giống kiểu của các tạp chí. Sách cung cấp kiến thức cơ bản về da, các biện pháp chăm sóc da (không chỉ có da mặt mà còn hướng dẫn chăm sóc da toàn thân), các loại bệnh về da hay gặp và các biện pháp thẩm mỹ cho da. Sách cũng trình bày thêm một số món ăn, thức uống tốt cho da cũng như đề cập đến các thói quen xấu gây hại cho da. Lần tái bản sau tác giả nên cập nhật thêm kiến thức mới về chăm sóc da thì cuốn sách sẽ hoàn thiện hơn.
3
63149
2016-06-26 14:02:33
--------------------------
301010
8210
Cuốn sách không hề dày hay khó đọc, chỉ khoảng 100 trang, trình bày trên giấy in màu, phân chia thành các chương từ hiểu biết ban đầu đến chuyên sâu hơn, giấy màu trình bày khá đẹp, nhìn cứ như là tạp chí ấy. Tuy nhiên kiến thức của cuốn sách này đưa ra cũng khá là lâu rồi, giờ có nhiều phương pháp làm đẹp hơn, thì không thấy đề cập tới, nếu sau này có tái bản thì cập nhật thêm là tốt nhất. Dù sao cũng nên sở hữu một cuốn để thỉnh thoảng ngồi xem lại nhắc nhở mình chăm sóc da thường xuyên.
3
104897
2015-09-14 13:31:24
--------------------------
295731
8210
tôi có sở thích về skincare nên mua quyển này về đọc thêm, sách khá chi tiết về phân tích cấu tạo làn da cũng như dấu hiệu nhận biết tình trạng da, cách chữa trị khi bị 1 số tình trạng về da. Nói chung nội dung khá hài lòng nhưng sách này được in khá lâu rồi nên tôi nghĩ cần tái bản lại để tập trung nhiều vấn đề về da mà hiện nay người ta đang hướng đến. Nếu bạn nào đã có kiến thức ổn định về skincare thì có lẽ 1 số bài viết phía sau không mới mẻ lắm đâu.
4
612426
2015-09-10 16:34:16
--------------------------
248683
8210
Sách mỏng, giấy đẹp nhưng nội dung khá sâu sắc và đầy đủ.
Nhưng bản thân mình mong muốn có một quyển sách chăm sóc da hiệu quả thật sự hơn, cụ thể hơn, chi tiết hơn vì thế cuốn sách bày mình cho 4 sao thôi.^^
Mình hy vọng sách sẽ tái bản và bổ sung những xu hướng làm đẹp mới nhất hiện nay và tư vấn kèm theo thương hiệu các sản phẩm mỹ phẩm từ bình dân đến cao cấp cho các làn da để bạn đọc dễ tìm kiếm và hỗ trợ chăm sóc làn da( cái này sách chưa có)
Với bạn nào muốn có kiến thức về làn da thì cuốn sách này khá bổ ích nhe.
4
400247
2015-07-31 10:10:33
--------------------------
