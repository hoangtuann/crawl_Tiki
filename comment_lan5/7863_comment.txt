300858
7863
Lễ phép là một đức tính vô cùng quan trọng và cần thiết đối với trẻ nhỏ. Nhất là trong giai đoạn đầu đời, bắt đầu biết quan sát sự việc, hiện tượng xung quanh thì việc dạy dỗ cho trẻ về những đức tính này là điều thiết yếu hàng đầu cho mỗi bậc phụ huynh. Cuốn sách "Chuyện kể cho bé trước giờ đi ngủ - Lợn con học lễ phép" này thực sự là một cuốn sách hay, bổ ích đối với mỗi gia đình đang có trẻ nhỏ ở giai đoạn hình thành nhân cách này.
3
29716
2015-09-14 11:41:02
--------------------------
264629
7863
Mình vừa nhận được hàng, tiki làm việc rất nhanh chóng, mình đặt hôm thứ 3 nhưng đến thứ 5 là có hàng rồi, tốc độ giao hàng vẫn rất nhanh. Về chất lượng sản phẩm, sản phẩm rất mới, hình thức đẹp, màu sắc rõ nét. Truyện kể có chuyện chim non vui vẻ và chuyện lợn con học lễ phép với nội dung đơn giản, dễ nhớ, hợp với bé nhà mình. Mình mua ngay đợt khuyến mãi nên giá cả rẻ hơn thị trường nhiều lại còn được miễn phí giao hàng. Mong tiki vẫn giữ chất lượng phục vụ như thế này. Mong tiki sắp tới xem xét về việc giảm chi phí giao hàng để mình ủng hộ thêm tiki nhiều lần nữa
4
300425
2015-08-13 12:07:33
--------------------------
