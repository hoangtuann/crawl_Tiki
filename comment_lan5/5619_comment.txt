440072
5619
Vụ án tên trộm gà trống ít dữ kiện vậy mà cũng đoán ra sự thật được, thật khó tin. Kid lại xuất hiện, lần này ít nhàm chán nhờ tứ đại vật thiêng :thanh long, bạch hổ, phượng hoàng, huyền vũ và kỳ lân. Nhưng dùng cái lưới thật to để bắt con cá nhỏ thật không xứng đáng lắm. Chỉ có chuyện Conan đá cái áo khoác vào Kid làm dính cái mạc vào lưng áo khiến cảnh sát luôn nhận ra hắn dù hắn đã cải trang rất giống là thông minh và nhanh trí. Thêm cái trò ngụy trang thành con nít cũng được. 
5
535536
2016-06-01 09:53:43
--------------------------
400976
5619
"Thám Tử Lừng Danh Conan Tập 68 " là một tập truyện rất hay, không thể bỏ qua. Trong tập này có sự xuất hiện của siêu trộm Kid, đội thám tử nhí đã phải đối mặt với tên trộm đào hoa này. Không những thế, chúng ta còn được thưởng thức câu truyện tình lãng mạn giữa thanh tra Shiratori và cô giáo Kobayashi và vụ án về ngày sinh nhật tồi tệ của Kisaki Eri vô cùng hấp dẫn. Có lẽ bất cứ người đọc nào cũng không thể rời mắt khỏi trang giấy một khi đã đọc.
5
1174527
2016-03-20 00:17:55
--------------------------
217729
5619
Trong Conan thích nhất là những vụ án Conan đối đầu với siêu trộm Kid, và trong tập này Conan không chỉ chiến đấu 1 mình mà còn có nhóm thám tử nhí ở cùng 1 chiến tuyến để bảo vệ chiếc sừng kỳ lần khỏi tay siêu trộm Kid, rất ly kỳ và hấp dẫn. Kid tài nhất vẫn là khả năng hoá trang siêu phàm suýt nữa đánh lừa được mọi người nhưng lần nào cũng vậy, không bao giờ siêu trộm này thắng được Conan. Rất ấn tượng với những vụ án có sự xuất hiện của Kid .
4
471112
2015-06-29 20:48:55
--------------------------
193587
5619
Tập này có 2 vụ ấn tượng nhất. Vụ thứ nhất là vụ án Nhóm thám tử nhí đối đầu với Kid nhằm bảo vệ chiếc sừng kỳ lân, đọc đoạn này mà biết thêm kiến thức về 4 thần thú bảo vệ 4 phương Thanh Long, Bạch Hổ, Huyền Vũ, Chu Tước. Một vụ nữa cũng ấn tượng không kém là vụ án cuối cùng, khi Ran quyết tâm giữ hình tượng thục nữ để nắm giữ trái tim người cô yêu, thích nhất đoạn sau cùng, đoạn ghi lời bói trên quẻ của Ran ấy. Cái chỗ cô tung người lộn vòng rồi sút cho tên tội phạm bất tỉnh đọc đi đọc lại vẫn thấy mắc cười.
5
393748
2015-05-08 00:58:19
--------------------------
186605
5619
Mình thích nhất ở truyện này là đoạn về vụ án của Kid :)) Kid của lòng mình đã chứng minh cho người khác thấy không có gì là không thể đối với anh, ngay cả khi mọi người dám chắc anh không thể hóa thành trẻ con, nhưng anh đã làm được và đánh lừa tất cả mọi người, kể cả conan. Truyện còn có một File về Ran và Sonoko đi bói quẻ tình yêu cũng rất hay, hay nhất là đoạn Ran tung chưởng Karate về tên tội phạm đang cô giết cô, thực sự đọc đi đọc lại vẫn thấy rất là buồn cười. Ủng hộ tập 68.
4
530790
2015-04-21 15:27:01
--------------------------
184796
5619
Ở tập truyện này Kid hóa thân thành ngài thanh tra Nakamori thật sự gây bất ngờ rất lớn, dường như không một người nào mà nhà ảo thuật gia không giả dạng được họ kể cả trẻ em. Thật khâm phục tài năng cũng như khả năng suy luận vạch ra từng kế hoạch chi tiết để đánh cắp chiếc sừng kỳ lân. Một cô gái mạnh mẽ như Ran khi xem quẻ bói tình yêu bảo phải dịu dàng thì cô ấy bỗng chốc thành một cô gái khác, có lẽ cô chỉ muốn gây ấn tượng với người mình yêu, nhưng cô nhanh chóng là chính mình khi biết rằng quẻ bói mà Sonoko đọc không phải là của mình, tung chiêu karate mới học cho tên hung thủ một bài học nhớ đời, quả là một cô gái mạnh mẽ!
5
412541
2015-04-18 12:31:37
--------------------------
183049
5619
Conan tập 68 vẫn giữ được chất lượng tốt như các tập trước. Rất đáng tiền. Tập này cũng có nhiều vụ án hấp dẫn lôi cuốn người đọc, thích nhất là vụ có Kid, li kì hấp dẫn vô cùng, khiến người đọc không thể rời mắt khỏi trang sách. Kết cục đầy bất ngờ mả mình không ngờ tới, đến nỗi bật cười làm mọi người cứ tưởng mình bị khùng, hì.Bên cạnh đó, trong tập này còn xen lẫn chuyện tình cảm lãng mạn giữa thanh tra Shiratori và cô Kobayashi, và có một chuyện rắc rối xảy ra, Kobayashi hiểu lầm Shiratori và Sato nên giận anh. Chuyện này đang hấp dẫn thì kết thúc mất tiêu. Hóng tập 69 .
4
558345
2015-04-15 13:01:04
--------------------------
