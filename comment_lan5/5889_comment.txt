307962
5889
Mình học khối ngành kinh tế nên mua cuốn sách này phục vụ cho môn học Quản trị tài chính doanh nghiệp. Trong quá trình tham khảo thì mình nhận thấy sách trình bày khá đầy đủ những lý thuyết, nội dung cơ bản, có rất nhiều ví dụ cụ thể cho sinh viên có thể hình dung nên mình rất hài lòng và cuốn sách khá hữu ích với mình.Tuy nhiên cuốn sách chỉ mới dùng lại ở phần cơ bản mà chưa có những kiến thức mang tính chuyên sâu hơn. Mong rằng sản phẩm sẽ ngày càng hoàn thiện.
4
440026
2015-09-18 13:33:06
--------------------------
270898
5889
Mình mua cuốn sách này để phục vụ cho việc học môn Tài chính doanh nghiệp. Nội dung sách là những lý thuyết căn bản về tài chính doanh nghiệp như: cổ phiếu, chứng khoán, đầu tư..... Mình đánh giá nó 3 sao thôi vì cuốn sách chỉ đơn thuần là một cuốn giáo trình bình thường chứ không có gì đặc biệt cả.
Bìa sách khá dễ bị bám bẩn, trang giấy cũng mỏng hơn mình nghĩ nhưng được cái màu chữ in đậm nét. 
3
280007
2015-08-18 18:18:36
--------------------------
