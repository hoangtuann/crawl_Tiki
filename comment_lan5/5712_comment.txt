473295
5712
Đưa tâm đây ta an cho,...
Với vai trò là một người bác sĩ, nhưng thâm thúy về Phật học, tác giả đã cho người đọc cái nhìn bao quát hơn về Kinh Kim Cang, về tinh thần phá chấp khai ngộ của đạo Phật.
Không phải là cái gì quá mới, quá khó khăn bí ẩn về một trí tuệ, không cầu kỳ xa hoa như cái tên của nó là “Kim Cang”. Mà đó là một phong cách sống, trí tuệ sống rất chân thật, gần gũi và rất bình dị trong cuộc sống hằng ngày: “đói ăn mệt nghĩ”

4
890847
2016-07-10 13:58:07
--------------------------
140497
5712
Mình nghĩ các tác phẩm của tác giả Đỗ Hồng Ngọc đều rất hay, rất thấm đẫm tính nhân văn, nhưng cách thể hiện có phần "phá cách" mang tính tiêu cực. Thay vì thể hiện trên cuốn sách hình chữ nhật, tác giả chọn cho mình một style hình vuông đầy khác biệt, điều này gây nên không ít bất tiện trong thẩm mĩ cũng như cách bài trí sách. Với nội dung hấp dẫn, đậm triết lý sâu sắc, tính nhân văn cao đẹp, "Gươm báu trao tay" như những lời tâm sự hết sức chân thực của một lương y như tử mẫu, đó là sự khó nhọc, vất vả, và cả hy sinh để đem tài năng và tâm huyết của mình vào việc cứu đời, cứu người. Cuốn sách có tính nhân văn sâu sắc, phù hợp với nhiều lứa tuổi.
4
414919
2014-12-11 15:22:53
--------------------------
