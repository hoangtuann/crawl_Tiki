477007
6055
Thiết nghĩ văn bản luật thì cần phải phổ biến hơn nữa trong xã hội. Luật tuy khô khan nhưng không nhàm chán, gắn liền với thực tế nhất. Phải hiểu miếng luật để không phải "đứng hình" khi bị "hỏi thăm". Về cơ bản thì mình nghĩ mọi người nên đọc và nắm luật giao thông đường bộ và luật xử lý vi phạm hành chính vì 2 thứ luật này rất gần gũi với đời sống hằng ngày (ra đường là gặp). Về phần sách thì gọn nhẹ, mực in vừa phải, dễ nhìn. Điểm trừ duy nhất là bìa sách quá mỏng, cầm đọc 1 lát là bị vểnh ngược lên.
4
1501984
2016-07-24 09:31:25
--------------------------
