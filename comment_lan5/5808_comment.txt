456186
5808
Một bức tranh về Ấn Độ với những sự nghèo đói,bất công,khốn khổ.. đã được thể hiện rõ trong Cân bằng mong manh,đọc mà thấy bi đát thay cho số phận của những nhân vật trong truyện,nó còn cho ta thấy một điều ta không hiểu gì về người khác thì ta không có quyền phán xét về hành động của họ,ta có ở vị trí của họ đâu.Một cuốn sách gây ám ảnh với những cuộc đời đớn đau đầy tiếng than khóc nhưng cuộc đời là thế,nó khốn khổ đấy nhưng ở đâu đó vẫn lóe lên những tia hi vọng và tình người chỉ cần ta sống đúng với bản chất.Sách rất dày nhưng đọc mau hết,đáng để đọc và sưu tầm.
4
526322
2016-06-23 11:30:55
--------------------------
453898
5808
Xuyên suốt tác phẩm, các nhân vật hết lần này đến lần khác bị tước đoạt cơ hội sống tử tế, và bị đẩy vào tình thế ngày càng bi đát. Nhân vật người thợ trẻ Omprakash khiến ta đồng cảm về khát vọng vươn lên trong cuộc sống. Thế nhưng bị cuộc sống chế độ vùi dập anh cũng dần thỏa thuận với những bất bình của mình và vui sống. Nhân vật chàng sinh viên Maneck là người có số phận may mắn nhất trong câu chuyện, nhưng không thích ứng được với đổi thay nên đã chọn cách từ bỏ. Thái độ đa nghi của cô Dina trong truyện cũng phản ánh cuộc sống thiếu niềm tin của xã hội, đặc biệt ở thành thị, và vẫn đúng cho bây giờ. Dù ta có mong cô ấy sớm xử xự tốt hơn với 2 bác cháu dưới quê lên thì ta vẫn có thể nhìn thấy bản thân mình trong cô ấy, giằng xé giữa lòng tốt và sự cẩn trọng. Nhìn chung, truyện khiến ta cảm thấy mình may mắn biết bao và có thể thấy được phía sau của cân bằng mong manh là sự thích ứng với thay đổi. Mình cho 4 sao vì mặc dù truyện lột tả chân thực những hoàn cảnh đẩy con người ta tới số phận của họ, nhưng hơi nghiêng về phía bi kịch nhiều hơn.
4
713748
2016-06-21 15:36:08
--------------------------
292491
5808
Nhìn cuốn sách đồ sộ nên cũng hơi băn khoăn khi bắt đầu đọc :)
Đọc thì thấy cực kỳ cuốn hút. Sách vẽ ra một xã hội Ấn Độ đa dạng đến vô tận và chất chứa những đầy điểm đối lập : vô cùng truyền thống nhưng cũng vô cùng hiện đại, nhân văn và tàn ác, bao la và nhỏ bé, khoan dung và ti tiện, sự phân chia đẳng cấp một cách sâu sắc, bất bình đẳng với nữ giới.... Cuốn sách làm mình liên tưởng đến những bộ phim ấn độ đã từng xem. Mình vẫn luôn tự hỏi, với một tiềm năng con người, tài nguyên bao la như vậy, bao giờ Ấn độ mới vươn lên xứng đáng với vị trí dành cho mình ??
4
327308
2015-09-07 13:39:08
--------------------------
284085
5808
Số phận con người Ấn Độ trong thời kì thập niên 70 đầy biến động chính trị đã hiện lên rõ nét dưới ngòi bút của nhà văn Rohinton Mistry, và tất nhiên nhờ bản dịch tuyệt với của dịch giả Nguyễn Kim Ngọc. Đây không phải là một cuốn tiểu thuyết li kì hay vụ án, nhưng chắc chắn nó sẽ khiến bạn không thể rời mắt và muốn lật tiếp những trang tiếp theo để xem số phận trêu đùa các nhân vật như thế nào. Đó là cô Dina, một người phụ nữ góa chồng phải sống phụ thuộc vào anh trai. Đó là Maneck, cậu sinh viên lên thành phố học tập và dần nhận ra những sự thật trần trụi về cuộc sống mà trước đây cậu chưa hề biết tới. Đó là hai bác cháu Ishvar và Om, những nạn nhân của sự phân biệt đẳng cấp trong xã hội, đã bị số phận đưa đẩy và phải chịu những sự bất công và đau khổ vì mất phẩm giá. Đó còn là cuộc sống của những người ăn xin, người diễn xiếc, những đứa trẻ,... Tất cả đã tạo nên một bức tranh về đất nước Ấn Độ với bất công, loạn lạc, nghèo đói, tệ nạn,... Cân bằng mong manh quả thật là một cuốn sách tuyệt vời và chắc chắn sẽ không làm bạn thất vọng. 
5
307488
2015-08-30 16:40:52
--------------------------
247824
5808
Đã lâu lắm rồi tôi mới được đọc một tác phẩm văn học của châu Á mà gây nhiều ám ảnh như vậy. Ngay cái tên tiểu thuyết Cân Bằng Mong Manh đã mang lại những cảm giác chông chênh day dứt và đầy hiếu kỳ cho độc giả. Cuốn sách không phải là dễ đọc nhưng những gì nó chứa đựng thì quả là phi thường. Khi bạn đi đến trang cuối cùng của câu chuyện dài này, bạn sẽ nhận thấy từ sâu thẳm trái tim bạn đang thay đổi. Con người là sinh vật kỳ diệu nhất, dù trong hoàn cảnh thế nào, họ cũng luôn vươn lên, kiên cường, để sống và hy vọng.
5
167283
2015-07-30 16:50:44
--------------------------
224915
5808
CÂN BẰNG MONG MANH

Khi tôi đọc đến hơn ½ quyển sách này, tôi vẫn nói với cô bạn đã đưa nó cho tôi rằng tôi chưa hiểu vì sao quyển sách lại có tựa đề Cân Bằng Mong Manh và hơn nữa tôi vẫn chưa biết nên “liệt” quyển sách này vào loại gì: Hay, chán, rất hay, rất chán”. Và khi đọc đến dòng cuối cùng của quyển sách, cô bạn kia lại hỏi tôi câu hỏi tôi đã hiểu được ý nghĩa cái tựa chưa. Rằng cô ấy thật sự có nên đọc cái quyển sách mà để hiểu cái tựa đề người ta cần phải đọc đến trang cuối cùng, sau đó mất thêm 3 ngày ở trạng thái lơ lửng mới hiểu nổi?

Ám ảnh, bạo liệt, đau đớn, dữ dội là những tính từ để tôi có thể miêu tả về cảm xúc của tôi dành cho Cân Bằng Mong Manh. Quyển sách này thật sự không dành cho tôi, cho những người vốn dĩ đã là nạn nhân hay chứng kiến quá nhiều bi kịch về bất công, về sự tàn nhẫn, về sự độc ác hay từng treo ngược chính mình khi bị cuộc đời dồn ép và quấn quanh cổ mình sợi dây trách nhiệm, tình & lý, đúng & sai.

Tuy nhiên, tôi vẫn nghĩ mình nên viết giới thiệu cho quyển sách này. Bởi nó xứng đáng được nhiều người đọc, bởi nó cũng đúng với thực trạng xã hội chính trị thối nát, kịch cỡm, tham nhũng, dối trá, bởi hơn hết nó giúp bạn luôn biết dừng lại 1 giây trước khi phán xét bất cứ ai, dù thậm chí đó là một gã chuyên cho vay nặng lãi hay ăn mày, bởi bạn biết gì về cuộc đời người ta mà cao giọng chê bai trách cứ. 

Bởi Cân Bằng Mong Manh được Ronhinton Mistry viết rất tự nhiên, rất đời thường, rất tỉ mỉ, rất hài hước nhưng lại khiến bạn không thể khóc cũng chẳng thể cười dù  ông miêu tả cái ác, cái thiện, cái hợm hĩnh và cả cái tận cùng của khổ đau. Tôi nghĩ tác gỉa Rohinton Mistryvà dịch gỉa Nguyễn Kim Ngọc đã vô cùng tài tình vì bằng tài năng của mình với con chữ, họ như thôi miên chúng ta, giúp người đọc đạt trạng thái “ Cân Bằng Mong Manh” trong cuộc phiêu lưu từ trang 1 đến trang 1053.
3 Lý Do bạn nên đọc Cân Bằng Mong Manh
1.	Cân Bằng Mong Manh  giúp bạn thấy những đau khổ, cay đắng mà bạn đã & đang trải qua thật ra chỉ là một mảnh vải ghép trong một tấm chăn ghép được đặt tên bạn. Nên niềm vui niềm hạnh phúc cũng vậy, chúng sẽ là những mảnh vải ghép tạo nên sự thú vị của tấm chăn. Nếu chúng ta cắt hết những mảnh xấu đi, tỉa hết những đêm đáng sợ và chỉ may những mảnh đẹp lại với nhau thì kéo sẽ cùn, tấm chăn chỉ còn lại tí vải. Và chúng ta sẽ chỉ có một cuộc đời quá ngắn ngủi.
Nói theo cách của Maneck thì : “ Cuộc đời có nhiều thứ phức tạp lắm, khó mà lấy kéo chia được. Tốt xấu đan xen thế này. Như những ngọn núi của tớ chẳng  hạn. Chúng đẹp lắm, nhưng cũng có thể gây lở tuyết”.

Thế nên quy tắc cần ghi nhớ về cuộc sống qua triết lý tấm chăn vải ghép đó là  cả tấm chăn quan trọng hơn rất nhiều so với bất kì ô vải  đơn lẻ buồn tẻ xám xịt nào.
2.	Cân Bằng Mong Manh giúp bạn mở lòng mình ra, ngồi xuống bên cạnh một người nào đó đủ kiên nhẫn, đủ thời gian, đủ tình yêu để nghe bạn kể lại thật chi tiết, thật rõ ràng, thật trung thực về cuộc đời của chính bạn. Điều này theo Valmik- người sửa bản in- là cực kỳ quan trọng đối với sự tồn tại của mỗi con người. Vì khi kể lại tường tận cuộc đời bạn cho một ai đó chịu lắng nghe, bạn sẽ tự nhắc mình nhớ bạn là ai để bạn không tự đánh mất chính mình, bạn giữ được màu sắc tươi sáng trong tâm hồn bạn dù  đối diện với bất cứ nghịch cảnh nào .
Luôn luôn hi vọng- hi vọng để đối trọng lại nỗi tuyệt vọng của chúng ta. Nếu không, chúng ta sẽ lạc lối. Cuộc đời của chúng ta chỉ là một chuỗi những tai nạn- một sợi xích leng keng kết từ những sự kiện tình cờ. Dù vô tình hay hữu ý, chúng nối dài mãi thành một tai hoạ lớn mà chúng ta gọi tên là cuộc đời.

3.	Cân Bằng Mong Manh giúp bạn điều tiết lại những suy nghĩ hợm hĩnh đề cao bản thân bạn , những ý nghĩ ban ơn dành cho người thấp hơn bạn hay những câu nói chia sẻ cực kỳ sáo rỗng dành để chia buồn với ai đó.
Vì có những nỗi buồn, sự đau đớn mà người khác chỉ cần sự im lặng của bạn, giọt nước mắt lặng lẽ của bạn , sự có mặt của bạn bên cạnh họ là đủ. Vì mọi lời chia sẻ đều sẽ là sáo rỗng, là thổi bùng ngọn lửa thiêu rịu chính họ khi họ đang cố gắng vượt qua nó. Như nỗi đau của Om, của Ishvar. 

Vì có những người chỉ cần bạn cùng chia đôi miếng bánh đang ăn dở của bạn, chia đôi cốc nước cùng uống với họ là đủ để họ biết ơn bạn. Đủ để họ hiểu bạn yêu quý họ, bạn tin tưởng họ và bạn xem họ như một gia đình. Người ta không cần nói quá nhiều lời yêu thương khi mà hành động lại vô cùng xa cách, dè chừng, nghi ngại.

Vì chúng ta, tất cả chúng ta đều là quái thai- chúng ta là thế- tất cả lũ chúng ta. – Theo cách nghĩ của nhân vật Ông Trùm. Nên đừng bao giờ để “quái thai” bên trong bạn suy nghĩ và hành động, đừng đặt mình cao hơn người khác, đừng cho rằng mình xứng đáng được ăn trong một cái chén sạch không xứt mẻ, còn người khác thì không.








5
2671
2015-07-09 14:28:12
--------------------------
223863
5808
Cân bằng mong manh là một cuốn sách dày và đồ sộ vì chỉ có thế tác giả mới vừa miêu tả được toàn cảnh của xã hội Ấn Độ, nơi con người bị ngăn cấm, đe dọa và trừng phạt trong khi cố thoát ra sự kìm hãm trong tầng lớp giai cấp mình, nơi những khu ổ chuột cũng dần biến mất nhưng không phải vì thoát nghèo mà là : “cải tạo đô thị” ,…vừa có thể tập trung vào cuộc đời của những nhân vật cụ thể. Bốn con người xa lạ với những hoàn cảnh khác nhau luôn cố gắng tìm thấy những điều tôt đẹp trong cuộc sống  bị dòng đời xô đẩy sống cùng một mái nhà. Nơi đây, họ đã trải qua những ngày tháng tươi đẹp nhưng cuối cùng họ đều bị  cuộc đời xô ngã, mang nỗi đau cả thân thể và trong tim . Giữa hi vọng và tuyệt vọng ,muốn tồn tại, con người chỉ có thể giữ một trạng thái “cân bằng mong manh” để không chìm vào cái hố sâu của tuyệt vọng và kết thúc cuộc sống của chính mình.
5
129176
2015-07-07 18:33:34
--------------------------
187279
5808
Nhìn quyển sách dày cộp mà tôi cũng thấy hơi ngán, không hiểu sao nó dày thế dù bản tiếng Anh cũng không đến nỗi nào. Nhưng vào đọc rồi thì những điều đó không quan trọng nữa vì tôi như bị cuốn hút vào cuộc đời của từng nhân vật. Tôi rất thích cách Mistry kể chuyện, bắt đầu từ những thành tựu hôm nay của ai đó, ông kể ngược lại để ta dần dần nghiệm ra họ đã trải qua những khổ nghiệm xương máu ra sao. Cuộc đời ở đâu cũng thế, những con người chỉ từ những đau khổ khó khăn mới gắn kết nhau hơn. Dù là một câu chuyện bi đát, nhưng giọng kể không khiến tôi cảm thấy bi quan và tuyệt vọng vì những sai trái bất công dù đó là trong  xã hội nào, mà nó là góc nhìn lạc quan của những con người dám công nhận sự hiện diện của những bất công ấy để họ đấu tranh và thay đổi, dù mỗi ngày chỉ nhích đi được 1 phân thì dần cũng sẽ đến đích thôi.
5
116202
2015-04-22 20:15:51
--------------------------
158731
5808
Sự đồ sộ của tác phẩm này không phải là vấn đề, vì ngay từ những chương đầu mình đã bị thu hút hoàn toàn vào câu chuyện tuyệt vời này của Rohinton Mistry, và đọc xong chỉ trong vài ngày. Đây thực sự là một kiệt tác, khắc họa sâu sắc một tấn bị kịch của đời người, bi kịch của cả một đất nước Ấn Độ rộng lớn. Nó là một cuốn tiểu thuyết, nhưng những chi tiết được kể ra lại quá chân thực đến mức xóa nhòa khoảng cách của đời sống và hư cấu, biến nó trở thành một kiểu phim tài liệu, phóng sự bắt trọn mọi hình ảnh. Ở đó có những người lao động nghèo hèn phải vật lộn với công cuộc mưu sinh, có những người ăn mày lang thang trên đường phố để xin của bố thí sống qua ngày, có những khu ổ chuột xập xệ với cảnh sống bẩn thỉu, khốn cùng. Ở đó cũng có hình ảnh về một vùng đất đa dạng về văn hóa nhưng quá sức hỗn loạn, đôi khi là tàn nhẫn, có sự quan liêu, giả dối của một chính phủ lúc nào cũng kêu gào dân chủ và hòa bình. Nhưng sau những mảng tối u ám ấy, tình người, tình yêu thương nảy sinh từ những điều bình dị nhất vẫn hiện hữu, vẫn tỏa sáng như một ánh lửa nhỏ nhoi mà ấm áp. Từ bốn nhân vật chính trong một căn nhà nhỏ, tác giả đã phóng chiếu tầm mắt của mình ra toàn thể cái xã hội ấy, để vẽ nên một bức tranh đa chiều về cuộc sống, về đời người. Một câu chuyện đau lòng, ngay cả cái kết của nó cũng quá sức bi đát, nhưng đọng lại trong lòng người đọc không chỉ là những tiếng gào thét, than khóc đau đớn, mà còn là một khoảng tĩnh lặng bình yên đến lạ lùng, để ở đó hy vọng và những giá trị nhân bản tốt đẹp vẫn luôn còn mãi.
Ngôn ngữ kể chuyện cuốn hút, nghệ thuật khắc họa nhân vật xuất sắc, những triết lý được đan cài một cách nhẹ nhàng mà thấm thía. Nó đem đến quá nhiều cảm xúc: buồn bã, cảm động, xót thương cho nhân vật, đôi khi là nỗi bực tức, ấm ức, sự căm ghét với những bất công. "Cân bằng mong manh" thực sự gây choáng ngợp.
Cuốn này làm mình liên tưởng rất nhiều đến "Triệu phú khu ổ chuột" của Vikas Swarup
5
109067
2015-02-13 19:38:52
--------------------------
