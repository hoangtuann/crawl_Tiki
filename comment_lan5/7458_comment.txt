495358
7458
Một trong vài tựa sách mình thấy rất hợp với không khí Giáng sinh. Những mẩu truyện ngắn ấm áp, nhẹ nhàng và đầy tính nhân văn rất đậm chất Giáng sinh. Bìa đẹp (quá hợp để chụp ảnh flat lay với phụ kiện Giáng sinh :P), duy có điểm trừ là mình không thích chất giấy, mỏng và hơi bị trắng quá.
Mình có viết giới thiệu cuốn sách này và 5 tựa sách khác khá hợp với Giáng sinh năm nay, nếu bạn nào thích đem không khí Giáng sinh về nhà thì ngó qua thử luôn nhé: https://waterlemon.xyz/6-tua-sach-hay-cho-nguoi-lon-dip-giang-sinh/
4
68408
2016-12-12 13:27:51
--------------------------
387361
7458
Bìa sách thiết kế đẹp, hợp và tràn đầy không khí giáng sinh.
Hình thức và cách trình bày nội dung cũng rất ấn tượng làm tôi liên tưởng tới màu của truyện "Cô tiên xanh".
Nội dung thì không có chỗ nào để phê bình. Mỗi truyện ngắn đều mang đến cho độc giả một xúc cảm khác nhau. Điểm chung là câu chuyện nào cũng giàu ý nghĩa và ngập tràn ấm áp. Mỗi lần đọc hết một truyện cứ như xua tan được cái lạnh lẽo của mùa đông đêm giáng sinh.
Tuy là mình mua trong đợt giảm giá nhưng chất lượng sách tốt làm mình rất hài lòng.
5
1157252
2016-02-26 22:23:24
--------------------------
358250
7458
Đêm Giáng sinh mà cầm cuốn sách này trên tay thì thật là tuyệt vời. Mình đã mua nó từ một năm trước nhưng năm nay và nhiều năm sau nữa chắc cũng vẫn đọc đi đọc lại. Những câu chuyện nhẹ nhàng và ý nghĩa sẽ mang đến cho chúng ta cảm giác bình yên và đầy phép màu. Tuyển tập truyện ngắn mang nhiều nội dung khác nhau xoay quanh dịp lễ Noel cũng như những mong ước trẻ con của nhiều độc giả, đều được thể hiện hết sức chân thật và cảm động. Bìa sách đẹp lung linh, trang sách ấm áp đi vào lòng người, tất cả đã là quá đủ cho một buổi tối Giáng sinh của mình.
5
433873
2015-12-25 00:27:59
--------------------------
272822
7458
Giáng sinh nhiệm màu là tuyển tập các truyện ngắn về giáng sinh. Mình không biết đây có phải là những truyện ngắn hay nhất hay không, nhưng mình thấy nó là những câu chuyện thật ấm áp, đậm chất giáng sinh. Đậm chất giáng sinh là ấm áp, thấm đẫm tình người. Mỗi câu chuyện đều để lại một câu chuyện ý nghĩa nào đó, mà chỉ có trong dịp giáng sinh mới có được. Giáng sinh-ngày Tết của người phương tây, dịp mà những lo toan, tính toán sẽ bỏ đi hết, chỉ còn lại tình yêu, tình thương còn lại
Đọc những câu chuyện này vào dịp giáng sinh thì mới thấy giáng sinh nhiệm màu thế nào, thấy thêm yêu những mùa giáng sinh hơn
5
486044
2015-08-20 14:13:52
--------------------------
199076
7458
Là một người Việt Nam, tôi chưa bao giờ có dịp trải nghiệm cái không khí trong những đêm giáng sinh đầy tuyết và tôi quyết định mua cuốn sách, phần vì tò mò, một phần lại bị thu hút bởi vẻ ngoài bắt mắt của nó. Đọc cuốn sách' giáng sinh nhiệm màu' này, tôi phần nào cảm nhận được sự thiêng liêng trong giờ khắc ấy, tôi hiểu rằng hơi ấm tình người có thể xua đi cái rét giá buốt của mùa đông. Cuốn sách chứa đầy giá trị nhân văn sâu sắc qua những câu chuyện kể chân thực. Một cuốn sách đáng để sở hữu!
4
635412
2015-05-21 14:26:06
--------------------------
152274
7458
Khi vừa trông thấy bìa sách mình đã bị hớp hồn ngay, bìa phông nền giáng sinh luôn luôn khiến con người ta cảm thấy náo nức, rộn ràng, hứng khởi trong lòng. Còn gì tuyệt vời hơn khi bạn là người được tặng cuốn sách này đúng ngay dịp giáng sinh ^^ Đây chính là món quà giáng sinh năm nay mà chị mình đã tặng cho mình ♥ 
Cuốn sách là tập hợp của rất nhiều truyện ngắn hay trên thế giới, bạn dường như cảm giác được không khí giáng sinh từ nhiều nơi vậy. Các câu chuyện đều rất hay, ý nghĩa, nhẹ nhàng, ấm áp. Độc giả chắc chắn sẽ cảm thấy ấm lòng trước những mẫu chuyện đong đầy tình cảm này. Dù cho không khí mùa đông có lạnh lẽo thế nào thì vẫn còn nhiều con người có tấm lòng ấm áp sẽ khiến bạn thêm yêu cuộc sống và chiêm nghiệm được nhiều hơn, biết cách cư xử với người khác hơn, biết trân trọng người thân hơn, cũng như biết thương xót cho những số phận bất hạnh ngoài kia...
Tóm lại đây là 1 cuốn sách hay dành cho mọi người !
5
75959
2015-01-22 18:25:43
--------------------------
147713
7458
Mình mua cuốn sách này trong buổi offline tiki tháng 12 vừa rồi. Mãi đến bây giờ mới có thời gian để đọc và suy ngẫm nó. 1 cuốn sách thật sự không làm mình thất vọng, từ hình thức đến nội dung. Mình rất thích cách trang trí bìa sách, rất thu hút. Tuy Giáng Sinh đã qua, nhưng khi lật giở từng trang sách mình vẫn cảm nhận được cái không khí se lạnh nhưng lại thật sự rất ấm áp. Đúng là "Giáng Sinh nhiệm màu"!
5
131327
2015-01-08 17:06:21
--------------------------
