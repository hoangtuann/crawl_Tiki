566961
6950
Phải nói cuốn sách này là một bể năng lượng và một trời lạc quan.
Tôi mua cuốn này trên tiki lúc giảm 50% vì cái tên nghe khá gây tò mò.Cuốn sách không hẳn là truyện dài, truyện ngắn hay tự truyện của cô nàng, tôi cũng không biết phải gọi thế nào nữa, nhưng những câu chuyện được kể trong sách thật quá đỗi gần gũi mà lại rất thú vị, mới mẻ. Lý do là gì? Là cô nàng đã đeo cho mình những cặp lens đủ màu sắc khi biết những câu chuyện đó. Mọi vấn đề xui xẻo trong cuộc sống tưởng chừng sẽ làm ta gục ngã nhưng nó lại trở thành yếu tố giúp ta nếm trải được đủ mọi niềm vui của cuộc sống nếu ta có tình yêu, sự tin tưởng.
Bạn bè tôi đọc ai cũng kêu hay.
Huyền Nhím- cô ấy là một cô nàng quá yêu đời và lạc quan.
5
1698285
2017-04-07 20:45:55
--------------------------
532765
6950
Tôi đã đọc tác phẩm và thấy rất hay cuốn sách nên mua ạ . Đọc và thấm lắm
4
133327
2017-02-28 12:08:15
--------------------------
320624
6950
Đeo len nhìn đời có thiết kế rất đẹp, bắt mắt và tên sách ấn tượng. Nhưng tôi ấn tượng nhiều hơn về nội dung. Phải nói rằng nhân vật chính yêu đời và sống hết mình vì nó, cách viết của tác giả là nhìn người khác qua con mắt của mình, bằng những góc nhìn khách quan đa chiều, sự sâu sắc đã giúp người đọc hiểu hơn về cuộc sống những người xung quanh và cả chính ta. Sống lạc quan, biết yêu, biết ghét và chân thành là "tôi" luôn hướng đến, dù là cuộc sống có thế nào thì chỉ cần đeo một chiếc len để thấy nó vẫn tươi vui và đáng sống.
5
475171
2015-10-11 21:29:19
--------------------------
296759
6950
Huyền Nhím là một tác giả rất cá tính, cá tính cả trong văn chương của cô. Tôi mua "Đeo Lens nhìn đời" khi tình cờ đọc được những câu trích dẫn trên mạng. "Đừng chán đời nhiều đến mức để đời phải chán mình". Đọc sách cứ như thể đọc được nỗi lòng của mình, tâm trạng cà lơ phất phơ hay mông lung đủ thứ. Đọc tới nhiều đoạn liền phì cười, cảm thấy thật thú vị. Cuộc sống này đã quá nhiều tổn thương và đau khổ rồi, thật cần những cuốn sách như thế này giống như những miếng urgo để băng vết thương.
4
27232
2015-09-11 11:42:17
--------------------------
250989
6950
Bản thân vốn dĩ không ưa cái thể loại ngôn tình của những tác giả trẻ. Nhưng tình cờ được cô bạn thân chia sẻ cuốn ' Chạy trốn yêu thương" - Huyền Nhím mình lại có cách nhìn khác đối với các sáng tác về tình yêu và đặc biệt hơn là ấn tượng về Huyền Nhím. Thế là bắt đầu tìm Facebook của chị để kết bạn và tiếp tục săn đón cuốn sách thứ hai của chị " Đeo lens nhìn đời ". Qủa thực rất hạnh phúc khi số tiền mà mình bỏ ra để mua sách rất xứng đáng. Những điều thường ngày vốn dĩ ta tưởng là rắc rối ấy đều trở nên đơn giản đến lạ lùng dưới con mắt của cô nàng tác giả " rất đời" ấy. Ngay đến cả chuyện chồng có bồ cũng thành điều có cai hay riêng của nó. giọng văn tưngg tửng khiến đọc giả không khỏi phát cười từng câu chữ.
Cám ơn Huyền Nhím đã cho mình một cách nhìn lạc quan hơn về cuộc đời này.
5
358967
2015-08-02 11:39:25
--------------------------
172784
6950
Ban đầu, tôi không có cảm tình với bìa sách cho lắm, nhưng tò mò về cái tên nên quyết định đọc thử. Cuối cùng thì đã không phải thất vọng. Truyện xoay quanh những câu chuyện hàng ngày của Nhím – một cô gái trẻ trung, vô tư, lạc quan có thừa. Những rắc rối, những mối quan hệ, tình bạn, tình yêu,…qua cái nhìn của cô đều như được đơn giản hóa, nhẹ nhàng hơn, đem lại một cách nhìn hết sức mới mẻ, thú vị. Giọng văn trong cuốn sách đa dạng, có nét trẻ con, cũng có sự chín chắn của người trưởng thành, đôi lúc lại pha thêm chút tưng tửng rất đáng yêu.
4
449880
2015-03-24 20:57:47
--------------------------
165268
6950
Mặc dù đã đọc kha khá sách để lôi kéo bản thân mình ra khỏi cái vũng bùn của tình yêu, nhưng chỉ đến khi chị Nhím "đeo len" cho...thì tôi mới lại nhìn đời tươi sáng hơn bội phần. Giọng văn tưng tửng của chị làm mọi tảng đá trong lòng tan dần ra, nhìn sự việc theo một hướng khác làm mình ngộ ra con đường mới. Sao trước đây, mình cứ nhắm mắt đi theo lối mòn...theo vết xe đổ của chính bản thân. 
cuốn sách mỏng dính, nên tôi luôn mang theo trong giỏ...khi lạc lối lại mượn tạm len của chị Nhím mà đeo!
4
144332
2015-03-10 11:15:51
--------------------------
158621
6950
Tôi biết đến Huyền Nhím đầu tiên chính là nhờ cuốn này, vì cái tên gợi cho tôi chút gì đó phóng khoáng, có phần buông thả nhưng lại có chất thực. Đeo lens nhìn đời có một giọng văn mang tính chất hài hước, hóm hỉnh và yêu đời, tôi gọi giọng văn này là giọng văn "tăng động". 
Ngoài ra, phần bìa sách cũng khá đẹp, tuy nhiên hình ảnh cô gái lại không được cân đối lắm gây nên một chút mất cân đối cho hình thức. Chất lượng giấy cũng không quá tệ. Nhìn chung thì tôi khá thích cuốn sách này của Huyền Nhím.
3
435897
2015-02-13 13:01:04
--------------------------
156557
6950
Ban đầu, thấy bìa sách, tôi nghĩ đây là cuốn sách tuổi teen. Nhưng vì đã đọc "Chạy Trốn yêu Thương của Huyền Nhím", tôi rất thích, vì thể tôi tiếp tục mua "Đeo lens nhìn đời".
Quả thực tôi đã không thất vọng, chất văn của Huyền Nhím quá hay. Rất chân thực, gần gũi, rất đời mà lại không hề  khiến người ta thấy khó chịu.  Tôi tự hỏi, dù bằng tuổi nhau, nhưng những gì Huyền Nhím suy nghĩ và cảm nhận cứ như thể hơn tôi cả chục tuổi. Suy nghĩ chín chắn và già dặn, nhưng cô ấy lại chọn cho mình cách sống trẻ trung, vui tươi, yêu đời.

Rất thích Huyền Nhím và 2 cuốn sách của nhà văn này.
5
554818
2015-02-05 11:14:35
--------------------------
127860
6950
Đây là lần đầu tiên tôi đọc một cuốn sách của Huyền Nhím, thật sự tôi khá ngạc nhiên với cô gái này.. từ giọng văn cho đến cách sống, cách bộc lộ cảm xúc và còn cả "xoay sở" cảm xúc :)
Tác giả nói với tôi như một người bạn tâm tình, kể về mọi thứ xoay quanh cuộc đời cô ấy một cách thành thật, vô tư và ngang nhiên, như "bơ" đi tất cả mọi buồn bã, chật vật mà thế giới này tạo nên để cười, để sống, để gạt phăng đi mọi u uất. Bên cạnh đó là tình bạn, tình yêu, tình đồng nghiệp,... được "đơn giản hóa" đi bội phần. Đúng! Huyền Nhím khá quái đảng, dư thừa sự vô tư, lạc quan và còn không ngại thể hiện cái ngông của mình với bạn đọc
Gấp "Đeo lens nhìn đời" mà cảm thấy niềm tin đan len lỏi đâu đó trong tâm hồn mình... hãy tìm đọc cuốn sách này của Huyền Nhím nhé, tôi cá chắc rằng bạn sẽ phải mỉm cười lạc quan thôi :)
4
248457
2014-09-27 11:23:03
--------------------------
119271
6950
Lúc đầu mình đặt mua vì thấy cái tên và bìa sách rất ấn tượng. Đến khi hàng về thì quả thực đã không làm mình thất vọng. Rất hay :D
Nội dung truyện xoay quanh cuộc sống về cô gái tên Nhím. Nhưng khi đọc lên thì cũng giống như đang nói về cuộc sống của chính mình vậy. Giọng văn gần gũi, bộc trực, có chút gì đó tưng tửng, điên điên. Rất thú vị và cá tính :D
Nhân vật "tôi" trong truyện rất thẳng tính và mình thấy cũng hơi hơi giống mình ;)) *cười xòa*
5
364272
2014-08-03 22:01:17
--------------------------
