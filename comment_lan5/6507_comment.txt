291265
6507
Lúc đầu nhìn thấy cuốn sách này mình cũng khá thích thú, vì mình đã theo dõi hết vua đầu bếp Việt Nam mùa đầu tiên. Cuốn sách như một câu chuyện thành công của vua đầu bếp Thanh Hòa.Thật sự rất ngưỡng mộ ý chí của anh cũng như tinh thần làm việc và thi đấu của anh trong suốt quá trình tham gia Master Chef. Cuốn sách phù hợp cho những ai thích nấu món ăn. Tuy vậy cách làm của anh Thanh Hòa lại rất đơn giản và giản dị.Không phô trương màu mè,sách trình bày đầy đủ, súc tích, màu sắc của hình minh họa rất đẹp.
4
100658
2015-09-06 09:38:42
--------------------------
269605
6507
Tôi rất hào hứng khi biết anh Thanh Hoà đã cho ra cuốn sách đầu tiên sau khi đoạt giải quán quân chương trình Master Chef. Đúng như những gì tôi mong đợi ở sản phẩm này, trước tiên là trình bày rất đẹp, khoa học và mạch lạc. Phần hình ảnh minh hoạ rất sống động và công phu. Sau đó là phần nội dung rất hay và đầy tâm sự của tác giả. Quyển sách thật xứng đáng với chi phí bỏ ra.
5
183104
2015-08-17 16:24:04
--------------------------
257141
6507
Tôi biết anh Thanh Hòa qua chương trình Vua đầu bếp Việt Nam. Anh không chỉ nấu ăn ngon mà còn có tính cách rất dễ thương. Cuốn sách này có bố cục rõ ràng, màu sắc hài hòa, in rất đẹp và chất lượng giấy tốt. Những món ăn của anh là sự pha trộn giữa phong cách Á và Âu làm những món ăn đơn giản lại trở nên lạ và đặc biệt hơn. Hướng dẫn của anh thật ngắn gọn, súc tích, dễ hiểu kèm những hình ảnh món ăn thật hấp dẫn làm người đọc muốn thực hiện ngay.
4
85228
2015-08-07 14:17:29
--------------------------
224028
6507
Lúc đầu nhìn thấy cuốn sách này mình cũng khá thích thú, vì mình đã theo dõi hết vua đầu bếp Việt Nam mùa đầu tiên. Nhưng đọc cuốn sách rồi thì mình thấy hơi thất vọng. Cuốn sách to và dày, nhưng lượng công thức lại hơi ít, bên cạnh đó tranh ảnh tuy đẹp nhưng có rất nhiều bức bị lặp lại. Những chia sẻ của tác giả về hành trình của mình tạo cảm hứng cho người đọc, nhưng cuốn cookbook này có lẽ phù hợp với các đầu bếp chuyên nghiệp hơn, các công thức vẫn được viết quá đơn giản, ngắn gọn. Nói chung, quyển sách này vẫn chưa chinh phục được mình hoàn toàn.
3
295088
2015-07-07 23:37:55
--------------------------
222864
6507
Quyển này mình mua tặng chồng mình, Thanh Hòa đã thuyết phục được anh ấy bởi cách vừa cầu kì trong món ăn nhưng cũng giản dị trong cách tiếp cận với người đọc qua cách viết và trình bày lại món ăn. Không phô trương màu mè như 1 số tác giả về sách ẩm thực khác, sách trình bày đầy đủ, súc tích, màu sắc của hình minh họa rất đẹp. Tặng anh mà thấy anh vui mình cũng vui lây mặc dù đam mê nấu ăn của mình ít hơn chồng mình nhiều nhưng đọc quyển sách này mình cũng thấy rất thích và hữu ích đối với mình lắm vì nó chỉ  cho mình rất cụ thể và cho mình thêm những bí quyết nấu ăn ngon nữa! 
Khi nhận được sách, sách vẫn còn bao kiếng mỏng ở ngoài và được giảm giá khá nhiều từ Tiki nên khi nhận được sản phẩm mình rất hài lòng nhé!
5
351847
2015-07-06 11:55:28
--------------------------
217317
6507
Mình không thích xem Master Chef của VN nên cũng không biết Ngô Thanh Hòa là ai, mình thích xem của US nên mới biết người chiến thắng được tài trợ xuất bản cuốn sách nấu ăn của chính mình. Tuy nhiên đọc xong sách mình có cảm giác háo hức muốn tìm hiểu anh Ngô Thanh Hòa là ai, quá trình anh ấy tìm hiểu đến nấu ăn như thế nào.... Nói chung một phần là do cuốn sách hay và hấp dẫn, hình ảnh bắt mắt đậm chất Việt Nam. Hướng dẫn món ăn chi tiết 8/10, có nhiều ghi chú nên nấu như thế nào rất hữu ích. Toàn là những món thông dụng và đôi khi có nhiều món cũng cao cấp mà nhìn ngay vào bạn đủ tự tin để chế biến, mức độ khó thì tăng dần. Tuy nhiên mình cũng muốn hỏi a Ngô Thanh Hòa là bột ớt chuông kiếm ở đâu. Có dịp mình lên Đà Lạt sứ sở ớt chuông nhưng hỏi bột ớt chuông thì chả đâu có làm thấy cũng hơi hụt hẫn.
4
125159
2015-06-29 09:53:22
--------------------------
