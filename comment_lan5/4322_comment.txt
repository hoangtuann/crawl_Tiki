528513
4322
Tài năng của Jeffery Deaver so với các tác giả truyện trinh thám khác mà tôi từng đọc đó là đi vào chi tiết những bằng chứng hơn là suy đoán, ông tin vào bằng chứng hơn là lời khai của nhân chứng sự như 1 nhà điều tra thực thụ. Chưa từng có tác giả nào lại tập trung đến bằng chứng hiện trường hơn ông, ông luôn khiến người đọc phải khó chịu hồi hộp trước những trường đoạn kịch tính của câu chuyện, và cuối cùng là sự bất ngờ về hung thủ thật sự, thật ko bút nào lột tả hết sự hấp dẫn gay cấn của cuốn truyện này :) 
5
849096
2017-02-20 14:24:06
--------------------------
473901
4322
Quả thật tác phẩm của  Jeffery Deaver chưa bao giờ khiến người đọc phải thất vọng. Đầu tiên phải nói đến hình thức của cuốn sách, với gam màu tối cùng  hình ảnh phù hợp với tựa đề của quyển sách tạo nên một sự ma mị, bí ần. Chất liệu giấy in rất tốt, chữ viết rõ ràng, Các tình tiết trong câu chuyện luôn gây sự tò mò, hấp dẫn. Quá trình phá án được xử lí một cách logic, dẫn người đọc từ bất ngờ này sang bất ngờ khác. Là một fan của thể loại trinh thám thì bạn không nên bỏ qua tác phẩm này!!!
5
426871
2016-07-10 22:32:42
--------------------------
446139
4322
một trong những quyển sách tôi rất thích. những bạn fan của trinh thám nên mua quyển này, không hối hận chút nào đâu ạ. Một cuộc đối đầu vô cùng lôi cuốn và bạn sẽ cực kì sock khi đọc đến cuối truyện, lúc tấm màn được hé lộ và bạn biết hung thủ thực sự là ai. Điều tôi thích ở Jeffery Deaver là những khía cạnh mà nhà văn đề cập đến, những con người với những số phận riêng trong xã hội, những bản chất ( sự kinh hoàng, chán ghét, thèm khát, sợ sệt..) của mỗi con người. Tình yêu Lincoln Rhyme và Amelia chắc là một điểm sáng giữa màu sắc dị của cuốn truyện này. Mong mọi người hãy đọc nó
5
979895
2016-06-11 16:54:50
--------------------------
426996
4322
The coffin dancer là tác phẩm thứ hai của nhà văn Jeffery Deaver và là cuốn sách thứ năm mình đọc về bộ truyện trinh thám của ông. Ban đầu mình chọn Búp bê đang ngủ, cây thập tự ven đường vì mình thích phá án dựa vào ngôn ngữ cơ thể của Kathryn Dance hơn. Nhưng sau đó mình lại cảm thấy hối tiếc vì đã không tìm mua tác phẩm này trước tiên. Mình cực kỳ thích phá án dựa trên chứng cứ khoa học. Qua tác phẩm này mình đã học được rất nhiều thứ như các chất hóa học, súng, lựu đạn, v.v... Một quyển sách đáng đồng tiền bát gạo.
5
509425
2016-05-08 04:37:57
--------------------------
342247
4322
Nếu như Sherlock Homles là tác phẩm kinh điển về thể loại trinh thám cổ điển thì tác phẩm này, theo cá nhân mình đánh giá, là thuộc vào dạng "must read" ở thể loại trinh thám hiện đại. Khi các tác phẩm của Conan Doyle hay Agathar Christie cho các thám tử của mình tới điều tra/hoặc có mặt ở hiện trường vụ án thì Jeffery Deaver lại khai thác tác phẩm này (tác phẩm khác mình chưa đọc nên không dám khẳng định) ở một khía cạnh khác - vị thám tử của ông không thể cử động. Yếu tố này góp phần tạo nên một cuộc đấu trí kịch tính giữa một bên là người đi săn - không thể di chuyển và một bên là con mồi - sát thủ bóng ma chưa bao giờ thất bại.
Về hình thức thì bìa của phiên bản tiếng Việt này được thiết kế khá đẹp và cuốn hút, giấy tốt, tuy nhiên cũng làm cho cuốn sách khá nặng khi cầm đọc nên nếu có thể thì những lần tái bản sau nhà xuất bản nên sử dụng loại giấy nhẹ tối màu như nhà xuất bản Trẻ in những tác phẩm của Agatha Christie dù dày nhưng cầm vẫn rất thoải mái.
Về nội dung thì truyện có khá nhiều điểm hấp dẫn người đọc như cách dẫn truyện hấp dẫn và bất ngờ, khối lượng kiến thức khá rộng về hình sự như: nhận diện nhân dạng, phương pháp điều tra hiện trường sử dụng các thiết bị hiện đại, về súng, chất nổ, bom,... Tuy nhiên, đáng chú ý nhất vẫn là hai điểm sau. Thứ nhất là khả năng vận dụng các kế sách như đánh lạc hướng (dương đông kích tây), tương kế tựu kế (kế nằm trong kế), ve sầu thoát xác hay những cái bẫy tinh vi mà hai bên đặt ra nhằm hạ gục đối phương sẽ đưa độc giả từ bất ngờ này đến bất ngờ khác. Thứ hai là ở khía cạnh tâm lý học tội phạm với phương thức nhập tâm vào hung thủ để hiểu được hắn đang nghĩ gì và sẽ làm gì tiếp theo làm mình nhớ đến nhân vật Phương Mộc trong các tác phẩm của Lôi Mễ.
Ngoại trừ một đoạn miêu tả dài dòng không hấp dẫn lắm và đoạn cuối mình chưa thấy thỏa mãn lắm nhưng nhìn chung đây là một tác phẩm hay và đáng có trên giá sách trinh thám của mình.
4
182663
2015-11-24 00:48:39
--------------------------
342211
4322
Đây là quyển đầu tiên mình đọc của Jeffery Deaver. Thật sự rất thú vị và lôi cuốn. Diễn biến câu chuyện mạch lạc, ngắn gọn nhưng không kém phần logic và hấp dẫn. Các nhân vật được tác giả xây dựng rất chân thực. Các chi tiết tình cảm được đan xen khá hợp lý giúp mạch truyện đỡ căng thẳng. Đây là một lựa chọ sáng suốt cho những bạn thích thể loại trinh thám pha chút kinh dị đấy nhé!! Thật sự rất hài lòng và sẽ tìm mua thêm nhiều tác phẩm khác của Jeffery Deaver.
5
914655
2015-11-23 23:08:37
--------------------------
340883
4322
Đây là cuốn sách đầu tiên của Jeffery Deaver mà mình đọc. Lúc đầu mình định mua cuốn "Kẻ Tầm Xương" nhưng những đánh giá của độc giả trên Tiki dành cho "Vũ Điệu Của Thần Chết" đã làm mình đổi ý. Nội dung sách khá li kỳ, hồi hộp với nhiều tình tiết bất ngờ, đặc trưng của thể loại trinh thám. Về hình thức sách mình không thích lắm vì khổ sách hơi to, giấy lại nặng nên rất khó cầm tay để đọc mà chỉ thích hợp để ngồi đọc trên bàn nên hơi bất tiện. Bìa minh hoạ cũng không được ấn tượng.
4
470937
2015-11-20 18:57:59
--------------------------
312083
4322
Tôi say mê truyện Conan từ nhỏ. Lớn lên tôi hay đọc truyện trinh thám và những truyện có tính chất trinh thám. Khi đọc cuốn này tôi rất hài lòng, cách viết văn hay, cách diễn đạt tuyệt vời. Nó đưa ta vào câu chuyện một cách cuốn hút, hồi hộp trong từng diễn biến và khó có thể đoán được diễn biến tiếp theo. Kịch tính, căng thẳng, làm người đọc như nghẹt thở khi đọc và hòa mình vào nhân vật đang đọc. Nó cũng cho thấy sự thông minh, nhanh nhẹn, sắc bén và khả năng phán đoán tuyệt vời của con người mà ta luôn hi vọng. Một quyển sách hay và lôi cuốn.
5
61245
2015-09-20 19:12:31
--------------------------
305588
4322
Tác phẩm của Jeffery Deaver là những tác phẩm mà tôi đánh giá cao về chất trinh thám hồi hộp và kịch tính đan xen với những bất ngờ.Ông Jeffery Deaver đã xây dựng nhân vật Lincoln Rhyme toát lên vẻ thông minh ,chuyen nghiệp ,sắc bén và không kém đi sự nhanh nhẹn của một nhà hình sự nổi tiếng.Ông đã khắc họa kẻ giết người rất sắc xảo , ông biết tạo tình huống kịch tính và gây cấn làm cho người đọc không khỏi nghẹt thở qua từng câu văn mà ông miêu tả  mà kẻ giết người đã tạo ra.
5
563698
2015-09-17 00:23:32
--------------------------
298707
4322
Vẫn với giọng văn quen thuộc của Jeffery Deaver, nhân vật Lincoln Rhyme hiện lên với sự tài ba của một nhà hình sự học nổi tiếng. Trong The Coffin Dancer tác giả lại đưa chúng ta vào một loạt những vụ giết người bí hiểm đòi hỏi sự thông minh, chuyên nghiệp, nhanh nhẹn và sắc bén trong cả suy nghĩ và hành động của Linc cùng các cộng sự ( đặc biệt là Amelia ). Đối phó với một kẻ giết người thông minh, hắn tính toán đến từng chi tiết trong mỗi vụ giết người. Qua từng vụ án các cộng sự cùng nhà hình sự học lại phải nỗ lực chạy đua với thời gian. Từng chi tiết được tác giả chọn lựa vô cùng hợp lí, cộng thêm các yếu tố hành động vô cùng gay cấn đã tạo nên một tác phẩm vô cùng tuyệt vời của JD. Kết thúc của truyện lại càng gây bất ngờ cho người đọc, một kết thúc mà ta không thể ngờ tới là những suy nghĩ của mỗi người khi đọc xong. Một câu chuyện, một cái kết sánh ngang với những tác phẩm của Agatha Christie!
4
620158
2015-09-12 20:37:27
--------------------------
297556
4322
Mình đã muốn mua quyển sách này từ lâu rồi nhưng vì sách xuất bản từ lâu đã hết hàng nên phải đến lần tái bản này mình mới có cơ hội mua nó. Cũng như những quyển tác phẩm khác của Jeffery Deaver, truyện này xoay quanh cuộc đấu trí của nhà thám tử tài ba Lincoln Rhyme với 1 tên sát thủ nguy hiểm "Vũ công tử thần". Nếu so với Kẻ tầm xương thì quyển này diễn biến được đẩy lên cao trào và kịch tính hơn khiến cho người đọc không thể rời cuốn sách được và chỉ mong muốn sớm biết được bộ mặt của hung thủ. Và với cái kết khá bất ngờ thì mình nghĩ rằng Jeffery Deaver đã thành công với tác phẩm này
5
39383
2015-09-11 22:33:12
--------------------------
292768
4322
Ấn tượng với Jeffery Deaver từ Kẻ tầm xương, ở Vũ điệu của thần chết, văn phong của tác giả vẫn vô cùng hấp dẫn. Mạch truyện nhanh và nhiều tình tiết bất ngờ.  Tác giả làm người đọc đi từ bất ngờ này đến bất ngờ khác. Tên tội phạm thật sự chỉ được hé lộ vào những giây phút cuối cùng. Tuy nhiên, yếu tố tình cảm ở quyển này không ấn tượng như trong Kẻ tầm xương (ý kiến riêng của bản thân mình thôi). Truyện miêu tả nhiều góc tối trong tâm hồn con người, sự phức tạp của cuộc sống này, sự không hoàn hảo và chính cái không hoàn hảo lại tạo nên cái hoàn hảo của riêng mình. Lyncoln Rhyme, Amelia Sachs, tình yêu của họ giữa những cái không hoàn hảo của cuộc sống làm cho tôi thấy thật đáng yêu và cảm động. Chúc họ luôn hạnh phúc!
5
183284
2015-09-07 16:49:40
--------------------------
290270
4322
Thật ra mình là một người mê dòng truyện Trinh thám pha lẫn một chút gì đó yếu tố kinh dị . Và mình cũng rất là kén chọn và khó tính cho các tác phẩm mình đã từng đọc qua . Được một thời gian tìm tòi thì không có tác phẩm nào có thể làm mình vừa ý , vô tình được đứa bạn giới thiệu cho nhà văn Jeffery Deaver . Mình cũng bắt đầu tình hiểu các tác phẩm của ông và phải nói mức độ hài lòng của mình rất đạt ngưỡng cao luôn .

Ban đầu thì mình tính mua The Sleeping Doll vì tình tiết sơ lược mình khá hài lòng . Thế nhưng do tiki không còn hàng nên mình mới mua thử Vũ điệu Tử Thần . Lần đầu hàng về , trước mình phải nhận xét qua về " ngoại hình " của bé . Quyển sách có thiết kế bìa không mấy nổi trội lắm , hình ảnh chủ đề thì cũng khá là nhàm chán . Phía sau gáy sách còn dính một chút keo khô khá là mất thẩm mỹ ( mong tiki nên kiểm tra kho hàng của mình trước khi giao ) . Tiếp đó là về nội dung , nội dung không có từ nào để diễn tả ngoài 3 chữ " Khá hoàn hảo " . Tác giả biết bắt nhịp truyện đi từ chậm rãi đến kịch tính hồi hộp . Logic lập luận cũng rất là chặt chẽ , phân tích và đánh sâu vào tâm lí con người . Giữa một tên tội phạm " Rất mưu mô " và có thể bày ra nhiều hình thức gây án khác nhau . Cùng với một điều tra viên tài ba mang tên Lincoln Rhyme . 2 bộ óc thiên tài nẩy ra những tình tiết tranh đấu đến cao trào và kịch tính khiến mình khó có thể sẽ bị ám ảnh trong một vài ngày . 

Nói chung thì mình không đánh giá cao lắm tác phẩm này , nhưng nói chung đây là một tác phẩm đáng đồng tiền để bạn đọc có thể được hài lòng .
4
569089
2015-09-05 10:51:48
--------------------------
287893
4322
Có lẽ đây là một trong những tác phẩm xuất sắc nhất của Jeffery Deaver. Câu chuyện kể về một tên vũ công bí hiểm, lạnh lùng, thông minh, man rợ với những hợp đồng đẫm máu và luôn hoàn thành tốt các hợp đồng đó. Vậy mà cuối cùng hắn lại bị thất bại trước Lincoln Rhyme. Kết thúc câu chuyện làm hài lòng độc giả nhưng hóa ra vẫn chưa kết thúc thật sự. Người đọc chỉ muốn đọc thật nhanh, lật giở thật mau những trang sách để đến được tận cùng của câu chuyện. 
mình đọc xong rồi mà cứ tiếc ngẩn ngơ vì sách không dày thêm 1 tí nữa. :)
5
126889
2015-09-03 09:03:47
--------------------------
268486
4322
Hoàn hảo là từ tôi muốn dành cho tác phẩm này. Ngay từ đầu, tôi bị cuốn vào câu chuyện mà Jeffery Deaver sáng tạo nên và không thể dứt ra được cho đến khi đi đến trang cuối cùng. Các tình tiết truyện li kì, đóng- mở xâu chuỗi 1 cách nhịp nhàng. Những kĩ năng nghề nghiệp, những kiến thức khoa học trong tác phẩm cũng thực sự rất rộng lớn cho thấy tài năng uyên bác của tác giả. Qua đó tôi cũng bổ sung cho bản thân mình được rất nhiều. Làm cho tác phẩm trở nên trọn vẹn không thể thiếu cuộc đấu trí giữa những bộ óc siêu phàm: Lincoln, Stephen và vũ công quan tài thực sự. Những con người ấy đã dùng chính suy nghĩ của họ để mở ra từng cánh cửa sự thật và cuối cùng là tấm màn bí mật không thể không khiến người đọc bất ngờ. Nói tóm lại, không uổng phí cho tôi và các bạn để đọc "Vũ điệu của thần chết"
5
724101
2015-08-16 15:21:47
--------------------------
250209
4322
Mạch truyện ban đầu có vẻ chậm rãi nhưng về sau càng hồi hộp kịch tính. Với khả năng đánh lạc hướng bậc thầy, tác giả đã dẫn dắt người xem qua nhiều bất ngờ thú vị, có quá nhiều bí mật khiến ta không thể bỏ sách xuống. Cuối cùng, mọi sự thật lại khác hẳn với những điều ban đầu ta đã biết. 
Về hình thức, sách có bìa đẹp, chất liệu giấy khá tốt, dịch thuật ổn có chú thích rõ ràng mặc dù có một vài lỗi chính tả. 
Rất mong có thể xem thêm nhiều tác phẩm của tác giả Jeffery Deaver.  
5
692199
2015-08-01 11:33:15
--------------------------
246118
4322
Mở đầu câu chuyện là cái chết đầy thương tâm của Edward Carney, phi công của hãng hàng không Hudson Air Charters cùng với sự mất tích của viên đặc vụ FBI Tony Panelli. Mọi nghi ngờ đều đổ dồn về Phillip Hansen – một “doanh nhân” làm giàu từ việc buôn bán vũ khí trái pháp luật. Và một “tổ chuyên án” đã được thiết lập mà người chỉ huy không ai khác là nhà hình sự học – Lincoln Rhyme. Cả đội nhanh chóng phát hiện ra hung thủ chính là một tên khét tiếng trên “thị trường giết thuê” vẫn được gọi với biệt danh Vũ công quan tài. Từ đó bắt đầu nổ ra cuộc đấu trí ngang tài ngang sức giữa Lincoln và tên vũ công, hết lần này tới lần khác âm mưu của hắn đều bị Lincoln chặn đứng, càng về sau thì cao trào càng được đẩy lên tới tột đỉnh!
4
293599
2015-07-29 17:37:05
--------------------------
237607
4322
Đọc cuốn sách này có cảm giác như đang xem một bộ phim hành động vậy. Tác giả đã đem đến một hình tượng nhân vật phản diện với đầy đủ những kĩ năng giết người chuyên nghiệp, lực lượng cảnh sát và những nhà điều tra hình sự đầy kinh nghiệm, những đoạn truy lùng, săn tìm thủ phạm,... tất cả chúng sẽ khiến bạn không thể rời mắt khỏi cuốn sách. Một điểm mình rất thích ở tác giả này đó là cách ông viết về những yếu tố rất thật của con người, những đoạn phân tích tâm lý, tranh cãi giữa các nhân vật đem lại cảm giác rất gần gũi với thực tế. Một cuốn trinh thám không thể bỏ qua!
5
637976
2015-07-22 16:18:42
--------------------------
193427
4322
Đỉnh cao của trinh thám hiện đại. Với ngòi bút của tác giả, tâm lý học của tội phạm là không tưởng. Thiên tài thám tử vs thiên tài tội phạm. Khi đọc về cuối làm mình nổi da gà vì bất ngờ và tuyệt vời. Vì vậy Jeffery Deaver luôn là tác giả có trong bộ sưu tập sách của mình. Sự thông minh và tạo hình dù qua ngòi bút cũng làm cho người đọc như thấy trước mắt. Những cuộc rượt đuổi và những hình thức giết người, đánh lạc hướng làm mình không thể dời mắt đến trang cuối cùng
5
625492
2015-05-07 15:43:16
--------------------------
182772
4322
Một cảm giác rùng rợn lạnh sống lưng, cộng thêm một chút kinh tởm, đó là những gì có thể nói về cuốn sách này và tên hung thủ. Những kế hoạch gây án chi tiết, cách ra tay dứt khoát cộng với thái độ máu lạnh của tên giết người được gọi là "Vũ công quan tài" khiến cho người đọc trở nên hồi hộp, đồng thời luôn thắc mắc rằng mọi việc sẽ được làm rõ như thế nào khi mà thám tử Lincoln Rhyme đã gặp phải một đối thủ quá tinh ranh. Có thể nói mình đã không hối hận khi đọc tác phẩm này. Hơn nữa, bìa sách sau khi được tái bản vẫn giữ được vẻ ám ảnh vốn có, đặc biệt phông chữ có phần đẹp hơn bản cũ.
5
316903
2015-04-14 21:38:45
--------------------------
