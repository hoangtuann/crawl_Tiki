457847
4611
Đọc khúc dẫn chuyện thực sự rất hi vọng vào quyển sách này vậy mà càng đọc càng thấy chán, nói rời rạc ko theo một câu chuyện hay đưa đến một kết luận nào cả mà còn lý thuyết nữa chứ. Tác giả diễn giải theo từng chữ cái của chữ MY JOB FUTURE, từng chữ cái nói về 1 vấn đề bắt đầu bằng chữ đó nhưng rất mông lung, rời rạc, ví dụ cũng rất nông, không có chiều sâu. Trong khi phần mở đại loại là tại sao chúng ta phải dậy sớm hằng ngày, tất tả chuẩn bị mọi thứ rồi bon chen nhích từng cm đường để đến được chỗ làm, vùi đầu vào công việc 1 ngày đến tối mịt lại bon chen về, cứ thế cứ thế trong khi không hề yêu thích, vậy thì mục đích của việc làm việc của chúng ta là gì. Đấy, nêu ra được như thế thì thấy hấp dẫn rồi đấy vậy mà không trả lời được gì hết. Sách to và rất dày, đọc rất mất thời gian mà không thấy thay đổi được gì. Lần thứ 2 đọc sách hướng nghiệp của tác giả Hàn Quốc, lại thất vọng, niềm tin chỉ còn lại 1 quyển nữa thôi. Nếu lần thứ 3 cũng không thấy được gì hay thì sẽ không mua sách của tác giả Hàn Quốc nữa đâu
2
605606
2016-06-24 16:36:06
--------------------------
380858
4611
Với tiêu đề của sách, cử nghĩ sẽ là tổng hợp những cách để chọn nghề nghiệp phù hợp cho bản thân. Nhưng sách này đặc biệt ở chỗ là tổng hợp về những ngành nghề đặc biệt ở các nước trên thế giới. Những mẩu truyện này không phải 'dạy đời' ta, mà giúp ta được truyền cảm hứng. Hơn thế nữa, chúng ta được biết thêm nhiều kiến thức về các ông lớn như 'Google', 'L'oreal',.... Những bài học bổ ích ta học được từ các nước bạn, từ thái độ làm việc, cách ứng xử hằng ngày. 
Cuốn sách hay để gặm nhấm khi không biết nên đọc gì.
4
285764
2016-02-15 23:23:46
--------------------------
341570
4611
Xin khẳng đinh, đây không phải là cuốn sách DẠY ĐỜI.

Với tôi những dòng chữ trong đây là kiến thức, là kinh nghiệm của những người đi trước được tác giả Kim Rando tổng hợp và đúc kết.

Ưu điểm: Những câu chuyện khởi nghiệp, kinh doanh, những thất bại của người khác sẽ là kinh nghiệm sống đáng quý cho bạn. Từng dòng, từng chữ, từng trang kiến thức trong đây cực kì có giá trị với mỗi người, không chỉ nói riêng với những người trẻ đang lênh đênh trên dòng đời không biết mình đi về đâu ,mà nó dành cho tất cả mọi người, mọi lứa tuổi. Những kiến thức của tác giả rất rộng lớn và đáng học hỏi. Đọc xong mỗi chương cùa quyển sách tôi lại có thêm 1 ý tưởng cho cuộc đời mình. Tôi cũng dần dần xác định được tương lai tôi phải làm gì mặc dù trước đây tương lai tôi mịt mù bóng tối.
Hi vọng đọc xong cuốn sách này bạn sẽ tìm được ánh sáng - dù là le lói cho tương lai của mình.

Khuyết điểm: Sách vẫn còn một số chỗ viết sai chính tả. Những phần đầu mỗi chương nhà xuất bản sử dụng giấy xám-chữ trắng nhỏ nên hơi khó nhìn, dễ mỏi mắt.

Thật sự cảm thấy đáng giá với số tiền mình bỏ ra để mang em nó về.
4
289237
2015-11-22 13:20:02
--------------------------
263884
4611
Nghề nghiệp là điều gắn bó cả một đời người vì thế không thể chọn lựa một cách qua loa, sơ sài, đặc biệt là với một học sinh năm cuối cấp như tôi, chính vì thế tôi đã mua quyển sách này. Quyển sách phân tích khá chi tiết xu hướng nghề nghiệp, đồng thời đưa ra những ngành nghề mới lạ và có khả năng sẽ cần nguồn nhân lực trong tương lai như nghề quản gia, nghề mộc, nghề kéo xe... Có lẽ có người sẽ nghĩ những ngành nghề mà sách đưa ra như đang giễu cợt trình độ của họ nhưng theo tôi, nghề nào cũng là nghề, miễn là có thể kiếm sống và không thẹn với lương tâm mình là được,nếu yêu thích cứ làm hết mình, nhất định sẽ đạt kết quả tốt.
5
44065
2015-08-12 21:39:40
--------------------------
234117
4611
Các thông tin cung cấp trong quyển sách thì rất hay nhưng tính ứng dụng của quyển sách chưa cao. Quyển sách trình bày các thông tin về các quan điểm nghề nghiệp, các loại hình nghề nghiệp mới trên thế giới, nội dung diễn đạt khá hay, thông tin đưa ra đầy đủ, tuy nhiên đó là cho từng chương sách, còn ý tưởng chủ đạo mà tác giả muốn truyền đạt xuyên suốt quyển sách thì nói thật tôi đã đọc hết hơn một nữa quyển sách nhưng tôi vẫn chưa cảm nhận được ý tưởng chủ đạo đó. Và tính ứng dụng cũng không cao, Ví dụ như tác giả khen ngợi quan điểm làm ít giờ nhưng hiệu quả cao của nước Pháp, tuy nhiện nếu người đọc là người Hàn, người Nhật Bản - những nơi đề cao việc làm nhiều giờ và người đọc làm theo ý kiến đưa ra (làm ít giờ) thì sẽ khó được thăng tiến trong quốc gia của họ. Nói chung quyển sách này nếu có nhu cầu mở mang kiến thức về các nghề nghiệp trên thế giới thì nên đọc còn nếu người đọc cần tìm nhưng lời tư vấn hướng nghiệp hữu ích, thực tế thì quyển sách này không đáp ứng được.
4
42985
2015-07-20 09:27:00
--------------------------
217556
4611
Điểm cộng cho quyển sách này là nội dung tổng hợp nhiều kiến thức tổng quát về các nơi trên thế giới về mặt lao động. Nhưng nội dung cũng không sâu.
Cầm quyển sách dày trên tay mà thất vọng vô cùng với nội dung kể lại thật khô khan. Các câu chuyện qua chi tiết khi kể về con đường việc làm của một người đến mức khó rút ra kinh nghiệm riêng cho chính bản thân. Đâu đó tác giả như cố rút ra kết luận để kéo chút cảm giác của người đọc lên. Sự thể hiện kiến thức của tác giả qua cuộc nghiên cứu được kể lại gây cảm giác nhàm chán.
Càng ngày càng thấy nội dung đều đều ko nổi bật.
Mình thật sự rất thất vọng. Mình đọc chỉ đến nửa cuốn sách là thấy nản. Nhưng sẽ cố gắng đọc hết quyển để ko phí tâm huyết của tác giả.
1
673262
2015-06-29 15:48:55
--------------------------
206732
4611
Sau khi đọc được những trang sách quý giá này đã giúp tôi có thêm động lực và niềm vui để cống hiến hết mình cho nghề nghiệp cao quý của mình, vì là nghề cao quý nên niềm vui tinh thần là chính các bạn ạ, lương bổng thì chẳng bao nhiêu nên để làm giàu từ nghề cao quý này thì lại càng không thể. Thế nhưng bù lai tôi được nhiều đồng nghiệp yêu quý, nhiều học sinh tin yêu và rất nhiều học sinh gặp khó khăn về hoàn cảnh gia đình, bạn bè, nhiều học sinh không ngoan... sau khi được tôi trò chuyện, giúp đỡ các em đã thay đổi và trở thành những học sinh yêu quý của tôi. Có lẽ đó là niềm vui lớn nhất của tôi. Nhiều lúc, tôi cũng rất tâm tư lẽ ra nghề giáo phải được xã hội trân trọng và được hưởng mức lương xứng đáng nhưng thực tế các nhà giáo khó mà có thể sống bằng lương của mình như hiện nay. Thế nhưng, sau khi đọc được cuốn sách này tôi thấy rằng có lẽ như vậy là hạnh phúc rồi vì tôi đang theo đuổi một nghề hết sức cao quý và có vai trò rất quan trọng như hiện nay, xin cảm ơn tác giả rất nhiều vì đã cho ra đời tác phẩm quý giá này. 
5
455308
2015-06-10 14:47:44
--------------------------
194766
4611
Tình cờ lang thang trên mạng và bắt gặp quyển sách này, tôi thấy mình thật may mắn. Phải nói thế nào nhỉ? Tôi là một người trẻ và là một người trẻ luôn trăn trở về ước mơ và công việc của mình: Liệu rằng hai thứ đấy có mãi đi cùng nhau trên 2 đường thẳng song song hay có thể gặp nhau ở một điểm nào đấy? Vậy nên không chần chừ để quyết định sở hữu Tương lai nghề nghiệp của tôi! Trong suốt quá trình đọc tôi đã không ít lần rơi nước mắt. À thì ra, trên khắp thế giới này có rất nhiều người trăn trở với ước mơ của bản thân; có rất nhiều người dũng cảm đi theo tiếng gọi con tim mình - nỗ lực để tạo ra cuộc sống tốt đẹp cho chính bản thân mình và góp phần làm thế giới trở nên tốt đẹp hơn. Những khái niệm, những công việc mới xuất hiện liên tục trong từng trang sách. Cách mà tác giả đưa ra thông điệp khiến người đọc có nhiều động lực để đi trên con đường của chính mình - con đường mang lại tiếng cười niềm hạnh phúc cho mỗi người, mọi người! Mong có nhiều người sở hữu cho mình quyển sách tuyệt vời này! Thân!
5
55561
2015-05-11 19:31:36
--------------------------
