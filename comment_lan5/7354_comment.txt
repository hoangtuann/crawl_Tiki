121201
7354
Không như những tác phẩm ngôn tình tràn ngập màu hồng và sướt mướt quá mức, tình yêu trong "Anh chàng mộ bên" được kể lại một cách chân thực và hồn nhiên. Tôi thích cách tác giả lần lượt chuyển từ góc nhìn của nhân vật nam sang nhân vật nữ. Nó cho thấy đàn ông suy nghĩ khác với phụ nữ tới mức nào, và ai cũng có cái lý của mình. Tình yêu thật phức tạp, vì ngoài những khía cạnh sinh lý, bản năng, nó còn là sự kết hợp của hai tâm hồn khác biệt. Một cuốn sách nên đọc đối với những ai muốn tìm hiểu tình yêu một cách nghiêm túc.
5
402410
2014-08-18 09:18:33
--------------------------
113251
7354
Truyện chỉ xoay quanh hai nhân vật chính là Benny và Desiree. Lời văn nhẹ nhàng, tình cảm, mỗi chương được viết xen kẽ tâm sự của từng nhân vật. Mình nghĩ truyện này chỉ dành cho các bạn đang yêu nhau nhưng phải trưởng thành rồi, vì có quá nhiều tình tiết sex . Thật sự nội dung chỉ giới hạn ở hai địa điểm: nội thành và ngoại thành. Không quá cao trào, không quá hấp dẫn, chỉ là một tình yêu nhẹ nhàng không hơn không kém. Tác giả cũng chưa lột tả hết tâm trạng của nhân vật khi vui buồn. Mình đã bỏ qua vài chương không cần thiết. Mình hơi thất vọng khi đã theo hết câu chuyện mà chỉ biết cái kết mở, còn phần tiếp theo. Mình nghĩ tác giả nên chăm chút cho lời văn của mình thêm dồi dào, tạo sức hút với người đọc chúng mình.
2
340113
2014-05-28 11:04:37
--------------------------
