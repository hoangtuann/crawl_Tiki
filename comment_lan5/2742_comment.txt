452933
2742
One piece chắc hẳn không còn xa lạ với fan anime, manga nữa.. Một bộ truyện có tuổi thọ tới tận bây giờ.  Giờ đã được nhà xuất bản Kim Đồng tái bản lại. Bìa thì quá tuyệt rồi. Giấy thì thơm quá chừng.Với giá tiền thế này mà được quyển truyện dày như thế thì quá tốt. Lại một câu chuyện cảm động về tình bạn mà Oda đã gởi tới độc giả. Không tình bạn nào qua tay Oda mà không cảm động, không hay được hết như câu chuyện của Norland với Calgara trong tập 31 này đây
5
640477
2016-06-20 21:56:00
--------------------------
401486
2742
Hiện tại tiki đang có chương trình khuyến mại nên mua one pice là hợp li nhất đối với những tín đồ mê truyện manga nhật bản đặc biệt là one pice.Tập này vừa cảm động vừa vui nha càng ngày cho thấy sự trưởng  thành của lufy và các đồng đội.
Càng ngày càng mong chờ thêm nhiều trận đánh, khám phá những vùng biển mới và nhanh chóng chinh phục tân thế giới trở thành vua của các loại vua hải tặc tiền truy nã càng cao thì hải quân càng vất vả truy bắt cả đội


3
1228983
2016-03-20 20:49:03
--------------------------
389741
2742
Lại thêm một câu chuyện tuyệt vời về tình bạn. Chân thành và thiêng liêng là điều bất kì tình bạn đẹp nào cũng nên có. Đúng là đến từ đâu không quan trọng mà quan trọng đó là ai. Cám ơn Oda đã giúp giới trẻ ngày nay hiểu hơn về tình bạn.
Về phần tiki thì mình thấy lúc dịch truyện thì những xưng hô giữa các nhân vật có cái gì đó hơi ngượng ép vì cuộc sống trong thế giới One Piece là cuộc sống phưu lưu tự do tự tại nên ngôn ngữ có thể mang tính dân gian. Chắc sợ lớp trẻ học theo nên mới xưng hô lễ phép như vậy. Cám ơn tiki vì có bộ truyện này
5
587182
2016-03-02 19:23:27
--------------------------
320480
2742
Tập ni khá buồn và rất hay nói về tình bạn thiên liên cao cả giữa Calgara và Norland làm tôi rất xúc động Norland không hề nói dối chỉ là hòn đảo cậu tìm đã bị đánh bật lên trời càng ngày tôi càng bị hút bởi truyện dù 400 năm nhưng tôi nghĩ không gì có thể ngăn được sức mạnh tình bạn của họ và tiếng chuông mỗi lần ngân lên thì tình bạn của 2 người càng vững bền chúc cho ad mong up các tập cũ và cập nhật các tâp mới thật nhanh đúng là 1 kết thúc hoàn hảo 
5
830152
2015-10-11 12:36:51
--------------------------
299636
2742
Cậu chuyện của Calgara và Norland khiến cho tôi rất cảm động tôi nghĩ đây là một tình bạn rất thiêng liêng nhưng cũng đầy tiếc nuối. Sau khi hiểu lầm rồi đến hóa giải hiểu lầm chờ đợi và chiến đấu nhưng cả hai cũng không được gặp nhau, tôi đã mất cả tiếng đồng hồ để suy ngẫm về tình bạn của họ quá đẹp quá tuyệt vời. Một người trước khi chết vẫn lo an toàn của người kia. Một người khi bị mất quê hương chiến đấu giành lại chỉ với một mong ước là có thể rung lên chiếc chuông để báo người bạn của mình biết mình đang ở nơi đây. Tôi ước gì đến cuối đời thì họ sẽ được gặp lại nhau nhưng tôi biết cái kết thúc như vậy mới là kết thúc hoàn hào nhất
5
604301
2015-09-13 13:41:56
--------------------------
275801
2742
Tình bạn của Calgara và Norland là một trong những tình bạn đẹp nhất thế giới One Piece, họ đến từ hai nơi khác nhau, lúc đầu tiên niềm tin tín ngưỡng cũng khác nhau,, nhưng trải qua biết bao thăng trầm, hiểu lầm, họ cuối cùng cũng trở thành bạn thân, tốt nhất của nhau, dù cho họ chia tay trong nuối tiếc. Thánh Oda đúng là thánh, khiến độc giả vừa khóc vừa cười, và ngày càng không thể dứt ra được ! 
5
186614
2015-08-23 11:16:17
--------------------------
257120
2742
cuối cùng đã đến câu chuyện về Calgara và Norland,một câu chuyện hay về tình bạn,niềm tin.trước khi rời đảo,Calgara đã hiểu lầm Norland nhưng Norland không hề oán trách,không hề giận Calgara.Vì Norland hiểu mình đã chặt đi một loài cây kí sinh quan trọng đối với ngôi làng của Calgara.dù ông hiểu nhưng ông vẫn không nói mà lặng lẽ rời khỏi đảo.Khi Calgara hiểu rằng mình đã hiểu lầm Norland,ông lập tức chạy ra nơi con thuyền rời đi,ông hét lên nhưng đâu ai nghe.Để rồi ông phải đánh lên tiếng chuông vàng của đảo ông cùng với lời nhắn..:lần sau lại tới nhé Norland
5
561013
2015-08-07 14:07:41
--------------------------
227450
2742
Cuối cùng mọi thứ đã được làm sáng tỏ trong tập này. Về huyền thoại của thành phố vàng trong quá khứ và cuộc gặp gỡ định mệnh của 2 con người khác nhau hoàn toàn về thân phận, về xuất thân, về niềm tin, nhưng điều đã gắn kết 2 con người của huyền thoại chính là "ý chí" mạnh mẽ. Mặc dù, thật sự theo cả nghĩa đen lẫn nghĩa bóng là họ cách nhau 9 tầng mây nhưng lời hứa đợi chờ vẫn còn đó. Tập này làm mình thật sự cảm động về quá nhiều chi tiết, Oda ss rất biết mua những tiếng cười và cả những giọt nước mắt của nguời đọc.Skypiea đang đi đến hồi kết. Thật hồi hộp mong chờ tập tiếp theo!
5
166935
2015-07-14 11:01:13
--------------------------
221073
2742
One Piece là bộ truyện tranh duy nhất khiến mình vừa có thể khóc, vừa có thể cười. Ở tập truyện này tác giả đã cho thấy tình bạn giữa hai người đàn ông, dù họ có ở đâu, có đang trong tình trạng nguy hiểm thế nào, họ vẫn không phủ nhận sự tồn tại của nhau, không quên đi lời hứa " Hãy thắp lên ngọn lửa "hay " tôi sẽ cho anh quay trở lại"... 400 năm qua đi những lời hứa vẫn còn đó, con cháu của hai người tiếp tục kế thừa ý chí đó của họ. Hãy rung lên đi, chuông vàng!!!
5
655896
2015-07-03 13:05:42
--------------------------
