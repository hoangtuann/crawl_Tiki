357575
5636
cuốn sách vẫn thật hấp dẫn, ngộ nghĩnh và vô cùng đáng yêu. Khi đọc xong cuốn truyện này tôi chỉ ước có thể lên cỗ máy thời gian của Doraemon để có thể quay trở về  cái tuổi thơ đầy những mộng mơ và khát vọng ấy. Được sánh bước cùng với những người bạn thân thiết, cùng nhau vượt qua nhiều những khó khăn, vất vả, gian nan và muôn vàn những thử thách. Để chúng ta có thể hiểu được giá trị của tình bạn chân thật, những người bạn tưởng chừng sẽ không bao giờ có thể rời xa được nhau, thật hạnh phúc, vui sướng biết mấy. Chất liệu cũng rất tốt, chữ in rất rõ và dễ đọc
5
932201
2015-12-23 20:31:02
--------------------------
301530
5636
Tuổi thơ của tất cả chúng ta đều có chú mèo máy thông minh Doraemon và nhóm bạn Nobita, Shizuka, Suneo, Jaian, Dekisugi. Là những câu chuyện vô cùng thú vị, cuốn hút. Tác phẩm cho ta sống trong những ngày thơ ấu vui nhộn, ngập tràn tiếng cười và những điều kì diệu. Đến bây giờ, khi đã trưởng thành,tìm đọc lại Doreamon, tôi vấn thấy nó cuốn hút như ngày thơ ấu. Chất lượng sách tốt, bìa sách dày dặn, hình in rõ nét. Giá thành sách cũng rất hợp lí. Nếu muốn tìm quà tặng cho trẻ em thì không nên bỏ qua Doreamon.
5
151980
2015-09-14 18:14:34
--------------------------
