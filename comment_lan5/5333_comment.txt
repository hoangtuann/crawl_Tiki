463953
5333
Lần đầu tiên mình được xem phim Frozen là mình đã rất thích rồi, thích tạo hình nhân vật: Elsa xinh đẹp, luôn cố tỏ ra mình là một người lạnh lùng, thương em gái, Anna tinh nghịch, hồn nhiên nên mới để cho gã Hans lừa gạt, Olaf ngố,thích mùa hè mặc dù biết rằng mình sẽ bị tan chảy vào mùa hè. Nội dung truyện rất hay và ý nghĩa, là một câu chuyện nói về tình chị em, xoay quanh những hiểu lầm, trên hết là tình yêu cao cả của Elsa đối với Anna và ngược lại. Tuy nhiên mình thấy rằng cuốn truyện được dịch không hay cho lắm, vì vậy nên mình chỉ cho 4* thôi.
4
1050004
2016-06-29 20:20:52
--------------------------
430422
5333
Bìa sách vẽ hình chị em nhà Elsa nên các bé rất thích
Bên trong có trang màu nêu rõ từng nhân vật và tính cách của từng tính cách của nhân vật. Truyện thì được tóm tắt cả câu chuyện trên phim hoạt hình, hơn nữa trình bày như vậy sẽ khiến trẻ chịu khó đọc sách hơn. Từ bộ phim nổi tiếng, nên khi thấy cuốn sách truyện này mình đã nhấn đặt mua ngay lập tức vì độ hấp dẫn của nó. Em của mình cũng vì hâm mộ bộ phim hoạt hình này mà chịu khó cầm sách lên đọc. Sách được bọc book care nên càng mới và đẹp. Cảm ơn Tiki
5
1185978
2016-05-15 09:24:54
--------------------------
400079
5333
Mình đã xem rất nhiều phim hoạt hình của hãng Disney và đặc biệt ấn tượng với phim "Nữ hoàng băng giá", thật là bất ngờ vì "Frozen"- một bộ phim mình rất thích lại có cả phiên bản truyện tranh. Nhìn thấy quyển truyện này mình rất thích, rất bắt mắt, bìa có hình 2 chị em Esla rất đẹp. Đây là một câu chuyện cảm động về tình chị em, không chỉ thu hút thiếu nhi mà bất cứ ai cũng cảm động vì câu chuyện này. Đây quả là một quyển chuyện tuyệt vời. Mình rất thích quyển chuyện này.
5
1174527
2016-03-18 19:50:23
--------------------------
373119
5333
Còn gì tuyệt hơn một cuốn sách được trao đi trong dịp Tết. Em của mình đã rất thích thú khi nhận được cuốn sách này. Nó cầm lấy và chạy lên phòng rồi đọc một lèo hết luôn cả cuốn. Nó còn tấm tắc khen: "Hình ảnh trong sách vẽ đẹp lắm chị ạ! Màu của mấy con chữ cũng đẹp mắt nữa." Đúng là bố cục của cuốn sách trong bắt mắt thật, tôi thử đọc thì thấy nội dung câu chuyện được thuật lại rất cụ thể, sinh động, tạo cảm giác như chính tôi đang xem bộ phim này vậy.
4
534860
2016-01-22 22:29:48
--------------------------
371871
5333
Mình đã nhận được sách rồi, thực sự rất hài lòng khi nhìn thấy cuốn sách. Đúng như mình nghĩ, sách làm bằng loại giấy nhẹ và đẹp như các quyển sách khác trong kho truyện của Dineys. Ngoài hình thức rất đẹp thì nội dung truyện rất lôi cuốn. Nội chung cuốn truyện rất hay và phù hợp với trẻ em. Khi đọc cuốn sách cho con nghe mình cũng thực sự bị hấp dẫn. Con mình cũng rất thích câu chuyện này như các truyện khác mình đã mua trên Tiki. Nói chung, cuốn sách rất đáng được các bậc cha mẹ mua cho con em mình.
5
88785
2016-01-20 16:23:09
--------------------------
370289
5333
Truyện Disney xây dựng hình tượng và cá tính nhân vật thật sự rất trau chuốt và kỹ lưỡng. Ngoài ra, ý nghĩa câu chuyện cần thể hiện rất sâu sắc và tuyệt vời : Elsa có tài năng đặc biệt, có thể tạo ra tuyết như ý muốn, thế nhưng vì chưa học được cách kềm chế sức mạnh đó mà Elsa đành phải chọn cuộc sống cô đơn vì sợ rằng mình có thể làm tổn thương người khác. Bù lại, cô em gái Anna lại có trái tim vô cùng ấm áp và yêu thương chị mình, chính tình cảm chị em , sự tin tưởng và hy sinh đã mang hai chị em lại gần nhau. Tuy nhiên, đây là truyện chữ nên dành cho các em đã biết đọc hoặc phụ huynh đọc cho các em nghe. có thể mua đĩa phim minh họa thì tốt hơn. 
4
595952
2016-01-17 14:25:12
--------------------------
359445
5333
Disney- Nữ hoàng băng giá, bộ phim hoạt hình tôi yêu thích từ nhỏ và xem nó rất rất nhiều. Sau khi phân vân không biết có nên mua hay không, tôi đã quyết định mua nó bởi sự tò mò không biết nó có giống như nội dung phim mình từng coi hay không? Và đúng như sự mong đợi của tôi, nó rất hay. Bìa sách trang trí đẹp mắt, chất lượng sách cũng được. Ngôn từ mượt mà, sâu sắc. Tình yêu thương, hạnh phúc được thể hiện rõ nét qua câu chuyện trên thông qua nàng công chúa Elsa. Với một sự PR vô cùng rầm rộ thì tất nhiên Disney- Nữ hoàng băng giá được mọi người ủng hộ rất nhiều.
5
914758
2015-12-27 08:33:02
--------------------------
295718
5333
Cuốn sách có bìa đẹp và khá là nhẹ, bên trong còn có mấy trang ảnh mầu từ phim rất ấn tượng. Mình mua cuốn sách này để tặng cho đứa cháu gái vì nó rất thích xem bộ phim này như mình vậy. Sách được viết lại từ phim nên khá là dễ đọc dễ hiểu phù hợp với trẻ em nhỏ. Nó đã rất thích khi được mình tặng cuốn sách này và mình đã làm nó chăm chỉ đọc sách hơn nên khá là vui. Đây là một quà tặng tuyệt vời giành cho trẻ em nhất là khi sắp đến trung thu rồi.
5
766204
2015-09-10 16:24:05
--------------------------
287152
5333
Tình chị em của Elsa và Anna thật tuyệt vời. cảm động. Cả hai người luôn thương yêu nhau, luôn nghĩ đến nhau nhưng cách thể hiện tình cảm đó ra lại khác nhau! Nhận được cuốn sách từ tiki mình rất vui, bìa sách rất đẹp, dễ thương, chất lượng giấy rất tốt, sờ vào mềm mại đã lắm. Mình vui ghê vì mình đã có ẻm rồi, mình đã xem đi xem lại Fozen rất nhiều lần rồi và khi biết có sách về chị em Anna mình lập tức mua ngay. (Khoe tí đã ^^) Như mình đã viết ở tiêu đề băng giá không hề có vì ở trong đấy chỉ có tình yêu thương thôi chẳng qua băng giá chỉ tạm thời phủ lên thôi và đúng thế kết thúc băng giá đã biến mất thay thế bằng mùa xuân ấm áp, mùa của sự yêu thương!!
5
508336
2015-09-02 12:43:30
--------------------------
262805
5333
Mình lần đầu cũng chỉ mua cuốn sách này để tặng cho em gái mình thôi nhưng khi mua về thì đến mình cũng nghiện cuốn sách này luôn. Thứ nhất, cuốn sách  có bìa rất đẹp, bìa khá dày, giấy xốp, nhẹ.Nội dung truyện thì khỏi phải nói rồi, rất hay và rất phù hợp với trẻ con. Mình và em mình còn thích hơn nữa đó là cuốn sách có mấy trang đầu được in tranh màu, trông rất đẹp và rất thú vị. Sau khi đọc cuốn truyện này xong thì mình với em mình xem luôn phim. Thích lắm luôn!
5
528028
2015-08-12 09:40:45
--------------------------
251198
5333
Là siêu phẩm của Disney năm 2014 mình xem phim và hứng thú hơn khi đọc chuyện bởi khgi xem phim tuy hiệu ứng hình ảnh và âm thanh tốt nhưng khi đọc chuyện mình có thể đọc bất cứ lúc nào.Câu chuyện về nữ hoàng băng giá Elsa,công chúa Anna và một vài nhân vật khác nữa câu từ đơn giản dễ hiểu thích hợp những đứa trẻ.Ngay cả bía sách cũng rất đẹp em mình nhìn một cái là mê liền.Ấn tượng là tình cị em trong fro zen.rất cảm động.Mong rằng hai chị em nhà mình cũng có thể yêu quý nhau như thế.
5
457454
2015-08-02 15:26:28
--------------------------
242506
5333
Truyện chữ của bộ phim hoạt hình nổi tiếng Frozen của hãng Disney đã khiến tôi phải tò mò mua về cho mẹ con cùng đọc xem có khác gì trong phim hay không?
Truyện tường thật đầy đủ những gì đã được xem trong phim nên theo tôi xem phim với hình ảnh, âm thanh, hiệu ứng tuyệt vời, nền nhạc xuất sắc của các bài hát trong phim: Let it go, For the first time in forever, Do you want to build a snowman?, In summer ... vẫn thú vị và hào hứng hơn.
Tuy vậy, cuốn sách cũng giúp tôi rèn luyện thói quen đọc sách chữ và bồi dưỡng khả năng viết văn cho con. Những điều đó không có được khi chỉ xem phim.
Sách bìa cứng, bóng, đẹp, có 4 tờ in màu đầu cuốn sách chụp lại những phân cảnh trong phim.
4
292190
2015-07-26 18:59:52
--------------------------
241151
5333
Thật ấn tượng vì những phim hoạt hình được yêu thích trên Disney đều được chuyển thể thành tiểu thuyết. Nữ hoàng băng giá là bộ phim mà được nhiều bạn nhỏ yêu thích và ngay cả tôi cũng vậy. Và tôi nghĩ cuốn tiểu thuyết này sẽ được nhiều bạn trẻ đón đọc. Trong Nữ hoàng băng giá tôi lại rất yêu thích Elsa, cô có một tấm lòng nhân hậu và tình thương cao cả cho đứa em Anna. Vì không muốn làm hại đến mọi người cho nên cô đã tự giam cầm mình trong căn phòng tối tăm suốt mười mấy năm. Còn cô em Anna không sợ bị đóng băng bởi ma thuật của chị, cô vẫn cố găng đi tìm người chị. Cũng chính vì vậy mà Elsa đã điều khiển được ma thuật của mình mang mùa xuân lại cho xứ sở Arendelle.
5
477877
2015-07-25 11:13:17
--------------------------
218686
5333
Là một fan của Disney tôi đã rất háo hức khi biết được Frozen còn có truyện và sẽ được xuất bản. Nhưng thật sự là hơi thất vọng vì mình nghĩ rằng nó sẽ giống như những bộ phim chuyển thể khác có những tình tiết, câu thoại và những suy nghĩ trong đầu của nhân vật không được đưa vào màn ảnh, nhưng những gì được viết trong tác phẩm này hoàn toàn khác với suy nghĩ trong đầu tôi trước đó. Nó dường như hay có thể nói là nó hoàn toàn chỉ miêu tả những hành động trong phim, những câu thoại trong phim, hay nói cách ngắn gọn hơn là kể lại một cách triệt để những gì đã có trong phim, đọc thì hơi chán cho những bạn là người lớn đã xem phim. Tuy nhiên nó rất tuyệt vời cho trẻ con vì mình cũng đã tặng lại cho em gái 6 tuổi của mình nó rất thích và khen hay suốt. và thật tiếc khi nó chỉ có mấy trang đầu là truyện tranh nhưng mình cũng mong nhà sách nếu lần sau có xuất bản thì nên bổ sung thêm phần truyện tranh như vậy sẽ tăng được trí tưởng tượng của các em dễ dàng hơn và đỡ chán hơn cho những em lần đầu hoặc không thích lắm trong việc đọc truyện dài.
4
289349
2015-06-30 19:54:43
--------------------------
200491
5333
Mua quyển truyện này với mục đích tập cho con gái bảy tuổi thói quen đọc truyện dài, vì con đã xem phim này rồi. Tuy truyện đã được Disney dựng thành phim nhưng mình cũng phải “duyệt” lại trước khi cho con gái đọc. Thật sự thích thú khi đọc từng trang trong cuốn sách này và truyện cũng có happy ending nhưng rất bất ngờ không phải theo kiểu “Hoàng Tử và Công chúa happily ever after” thông thường. Thích nhất “triết lý” – tình yêu đích thực: quyết định hy sinh bản thân để cứu chị chính là nghĩa cử của tình yêu đích thực. Sẽ có rất nhiều điều để trao đổi với con gái thông qua quyển truyện này.
4
183312
2015-05-25 09:47:28
--------------------------
193630
5333
Tôi xem phim và bị ấn tượng mạnh bởi những kĩ xảo quá đặc biệt của Frozen. Từ bài hát cho đến hình ảnh lâu đài băng sừng sững giữa núi.
nhưng sẽ thật thiếu đi ý nghĩa nếu không có sự diễn tả cảm xúc của ngôn từ.
hai chị em Anna Elsa là hai tính cách trái ngược nhau, người thì ương bướng tinh nghịch, người thì trầm lắng yếu đuối. những tưởng cả hai sẽ không có điểm chung nhưng, chính tình yêu thương mà họ dành cho nhau đã vượt lên tất cả.
phần bìa và giấy tốt, một cuốn sách đáng để đọc.
5
220265
2015-05-08 11:06:51
--------------------------
192565
5333
Câu chuyện mang nhiều tình tiết kịch tính, cao trào. Thích cái cách mà Elsa và Anna bày tỏ tình cảm dành cho nhau. Không quá cầu kì, chỉ đơn điệu nhưng quý giá. Dù hai chị em ngăn cách nhau bởi phép thuật, bởi nỗi sợ hãi của Elsa, dù cô đơn bủa vây lạnh lẽ, Anna vẫn yêu quý chị của mình và Elsa vẫn quan tâm em. Đặc biệt hơn cả là tình tiết Anna bị người minh yêu lầm tưởng, vượt qua bão tuyết bảo vệ cho chị của mình. Nó không chỉ chứng minh tình yêu của Anna với Elsa mà còn chứng minh sự bất lực của Elsa với chính phép thuật của mình. Câu chuyện nhanh chóng vào hồi kết tốt đẹp, gây ấn tượng mạnh với độc giả.
5
419803
2015-05-04 23:24:44
--------------------------
191206
5333
Đã từng coi qua phim hoạt hình trên Disney và cũng nghiện bài hát trong phim. Truyện rất hay và ý nghĩa. Mặc dù tình cảm chị em không còn như xưa, nhưng cô em gái vẫn mong ước mình và chị gái sẽ trở về như lúc thơ ấu. Câu chuyện cho ta thấy một sự hy sinh dành cho nhau của 2 chị em. Hành trình đi tìm lại người chị. Tình cảm chị em, tình yêu, sự dối trá của anh chàng hoàng tử út thực sự rất thú vị.
ĐIểm cộng cho sách: Giá cả rất phải chăng, bìa đẹp, phù hợp với mọi lứa tuổi, đọc truyện truyền đạt được nhiều cái mà trong phim không có.
5
255414
2015-05-01 10:31:19
--------------------------
180919
5333
Câu truyện cho thấy 1 tình chị em đầy cảm động. Người chị với quyền năng có thể biến bất cứ thứ gì thành tuyết. Với lỗi lầm lúc nhỏ đã gây ra cho người em nên lúc nào cũng rất nghiêm khắc. Còn người em gái thì không hiểu vì sao người chị của mình lại xa lánh mình. Tất cả những khuất mắt chỉ được giải quyết sau khi 2 chị em cùng vượt qua biết bao sóng gió, cuối cùng tình cảm của 2 chị em mới được tốt đẹp lại như xưa. Qua câu truyện, ta thấy người chị bên ngoài thì lạnh lùng nhưng lúc nào cũng thương người em, còn người em gái tính tình bướng bỉnh những cũng rất yêu quý chị mình.
5
588073
2015-04-10 14:23:02
--------------------------
177809
5333
Frozen là câu truyện cũng như bộ phim hay nhất mà mình từng đọc(xem). Nữ hoàng kiêu sa Elsa lạnh lùng có phép thuật nhiệm màu đó là tạo ra băng tuyết. Công chúa cá tính Anna không quản khó khăn gian lao đã đến lâu đài của Elsa để mong chị có thể hóa giải phéo thuật. Người tuyết Olaf nhí nhảnh đáng yêu và rất tốt bụng. Gia đình là mái ấm yêu thương, có sức mạnh diệu kì hóa giải tất cả mọi thứ. Tình cảm gia đình cũng là thứ tình cảm vô cùng thiêng liêng và đẹp đẽ. Vì vậy cúng ta càn phải chân trọng nó. Đó chính là bài học mà Frozen muốn gửi đến chúng ta.
5
588266
2015-04-03 20:55:38
--------------------------
174390
5333
Một câu chuyện rất hay, đầy ý nghĩa nhân văn nhưng cũng phơi bày cả những mặt trái của cuộc sống. Lúc xem phim, mình cực kỳ thích vẻ đẹp kiêu sa, lạnh lùng của Elsa, thích người tuyết đáng yêu mà Elsa tạo ra, cũng rất thích những khung cảnh đẹp tuyệt trong phim còn khi đọc sách thì lại bị cuốn hút bởi cách miêu tả tâm lý nhân vật. 
Cuốn truyện đã khiến mình thêm trân trọng tình cảm gia đình và giữa người thân với nhau cũng như thêm hiểu hơn về mặt trái xã hội và những nghịch lí trong cuộc sống.
5
445522
2015-03-27 21:47:58
--------------------------
169860
5333
Có ai lớn như mình mà còn coi và đọc truyện disney không? MÌnh đặc biệt rất nghiền bộ phim này về mọi thứ,nội dung rồi hình ảnh,xây dựng nhân vật,tuyệt vời,mô típ hay không khiến người khác nhàm chán. Hay quá đi,đọc truyện còn hay hơn cơ.Nếu xem phim khiến mình mê mẩn vẻ đẹp của elsa,anna và đồ họa thì đọc truyện khiến mình chú trọng vào tâm lí nhân vật hơn.quả thật là hay và ý nghĩa.khi mọi thứ mất đi thì tình thương yêu sẽ ở lại.đoạn anna bị đóng băng ai cũng sẽ nghĩ  Kristoff sẽ chạy đến bên cô và làm tan chảy băng giá nhưng cuối cùng lại thật bất ngờ.truyện đem đến một khía cạnh mới về tình yêu-tình yêu thương trong gia đình.ba mẹ elsa và anna cũng rất yêu quý hai chị em đấy thôi.còn nhiều bất ngờ.hoàng tử Hans khi mới xuất hiện ai cũng nghĩ sẽ là một câu chuyện tình yêu hoàng tử-công chúa nhưng ai ngờ hắn ta lại là kẻ xấu.kết thúc có hậu,đáng đời hans,mong kristoff và anna là một đôi!
5
586877
2015-03-18 22:50:22
--------------------------
163832
5333
Mình chưa xem phim hoạt hình nữ hoàng băng giá nhưng khi đọc sách thì thấy rất hấp dẫn. Mình thích đọc hơn là xem phim có lẽ là tùy người nhưng thật sự câu chuyện rất hấp dẫn có phần gay cấn. Tuy chỉ tưởng tượng qua những bức tranh trong quyển truyện nhưng cũng đã thấy rất sinh đông. Câu chuyện mang rất nhiều thông điệp về cuộc sống về tình yêu thật sự và một điều quan trọng nữa là dù bất cứ việc gì chỉ cần chúng ta có tấm long và thành tâm muốn làm thì điều đó sẽ thành.
5
529949
2015-03-05 23:01:38
--------------------------
160042
5333
Từ khi mới ra phim, tớ rất muốn có một cuốn sách thật tiện lợi để đọc mọi lúc mọi nơi. Một lần ghé qua nhà sách thì  thấy cuốn frozen nên nhảy vào mua liền. Cuốn sách rất hay luôn! Chẳng có phù thủy, ác thú hay thậm chí là quái vật, chỉ có bóng tối của riêng ta, từ đó câu chuyện đã gắn kết sự chia cắt của hai chị em Anna và Elsa. Đầy ý nghĩa và dạy cho ta nhiều bài học hơn nữa. Chính tình thương đã làm tan chảy mùa đông vĩnh cửu ở Arendelle
3
375271
2015-02-21 14:54:39
--------------------------
158666
5333
Lần trước mình đợi mãi đến lúc tiki ra hàng, mà sao sách lại ra trễ hơn dự kiến trên tiki làm mình không đặt được. Cứ đợi đến cuộc thi Vẽ disney nhận quà mê ly để được tặng quyển này mà hỡi ơi lại hết Frozen mất rồi, buồn hết ngày trời. Nhưng may sao con bạn có mua cho mình quyển Frozen, mừng 5 phút nhưng thoáng buồn hết ngày trời nữa......không có postcard.....Truyện rất hay, mặc dù mình xem phim đến thuộc thoại hết rồi nhưng khi cầm quyển này lên đọc thì cứ như thuở ban đầu mới xem Frozen zậy, miễn chê....
5
457316
2015-02-13 16:24:29
--------------------------
150857
5333
Frozen-nữ hoàng băng giá là một trong những câu chuyện cổ tích được chuyển thể thành công nhất của Disney. Từ một trong những câu chuyện có kết thúc không có hậu, bà chúa tuyết đã trở thành câu chuyện về tình chị em sâu sắc giữa Elsa và Anna. Có được sức mạnh to lớn từ nhỏ nhưng không có khả năng kiểm soát, Elsa buộc phải tự cô lập mình để tránh làm hại đến người khác. Nhưng số phận đưa đẩy khiến cho bố mẹ Elsa và Anna mất đi, gánh nặng cả vương quốc đều đặt trên vai của Elsa. Vào ngày lên ngôi của mình, cô bị bộc phát sức mạnh và gây ra mùa đông lạnh giá trên khắp vương quốc. Anna buộc phải lên đường tìm kiếm chị mình và chấm dứt mùa đông trên khắp vương quốc. Trên đường đi, cô gặp Kristof và Sven, người tuyết tốt bụng Olaf. Anna bị Elsa làm bị thương, chỉ có tình yêu đích thực mới hóa giải được. Vào giây phút cuối cùng, Anna đã từ chối cơ hội được cứu sống của mình để cứu Elsa, hóa giải mùa đông cho cả vương quốc. Đặc biệt trong câu chuyện này, chàng hoàng tử Hans hóa ra lại là một kẻ xấu xa gian trá tìm cách chiếm đoạt vương quốc. Câu chuyện làm nổi bật lên những tình cảm tốt đẹp giữa gia đình và cho thấy sự băng giá trong trái tim con người chỉ có thể bị tan chảy bởi yêu thương.
5
291067
2015-01-17 21:48:45
--------------------------
150241
5333
Hồi trước, lúc xem Frozen, em gái mình đã rất thích thú và luôn mong muốn sẽ có truyện tranh để đọc bất cứ lúc nào nó thích. Vậy nên lúc mua cuốn này về, mình mừng lắm ^^. Sách rất đẹp, postcard xinh xắn dễ thương, bìa cũng ấn tượng. Chất lượng giấy cũng như chất lượng in ấn rất tuyệt. Câu chuyện của Elsa, Anna cùng với những người bạn đáng yêu: Kristoff, Even,… từ màn ảnh bước ra trang giấy cũng hết sức ấn tượng và giàu giá trị nhân văn. Thật sự rất hài lòng với truyện này.
5
393748
2015-01-16 01:41:34
--------------------------
150229
5333
Với câu chuyện về các nàng công chúa của Dísney thì mình đặc biệt ấn tượng với câu chuyện về Frozen hay Nữ Hoàng Băng GIá. 
Câu chuyện có cốt truyện đầy lôi cuốn: nàng công chúa Elsa với pháp thuật bẩm sinh từ nhỏ, đã cố che giấu qua biết bao nhiêu năm, phải sống trong mặc cảm và chịu rời xa người em gái thân yêu nhất của mình.
Để rồi một ngày ma lực của cô bị phát hiện, cô phải chạy trốn khỏi vương quốc của mình. Nhưng lúc này ý chí trong cô lại trỗi dậy, cô thành lập vương quốc cho riêng mình và trở thành nữ hoàng trong chốn đó!
Hành trình tìm lại người chị của Anna thật sự làm mình cảm động! Tình chị em đã giúp cô vượt qua biết bao nhiêu khó khăn thử thách. Cũng nhờ cuộc hành trình này mà cô nhận ra được tình yêu đích thực này!
Một câu chuyện đẹp từ nội dung, ngôn từ cho đến cách xây dựng nhân vật và những ý nghĩa sâu sắc mà nó đặt ra :)
Tác phẩm chuyển thể hoạt hình cũng rất hay. Đọc - xem và cảm nhận để thấy hết được cái hay về nàng công chúa này!
5
303773
2015-01-15 23:15:00
--------------------------
149691
5333
Phải nói rằng nhà xuất bản rất biết cách chớp thời cơ. Cùng với sự lan tỏa và nổi tiếng của bộ phim hoạt hình nữ hoàng băng giá, tác phẩm đã ngay lập tức chuyển thể thành sách, phù hợp để đọc cho các em bé trước khi đi ngủ. Đặc biệt là bìa sách thiết kế rất ấn tượng. Mình nghĩ đối với những ai chưa có điều kiện xem phim hay những bạn muốn lưu giữ câu chuyện cảm động của Disney như một món quà kỷ niệm thì nên mua sách, chúng ta có thể giở ra đọc bất cứ lúc nào, hơn nữa cũng có thể để dành tặng cho những đứa trẻ mà chúng ta yêu quý. Postcard tặng kèm rất đẹp.
5
146361
2015-01-14 14:28:43
--------------------------
