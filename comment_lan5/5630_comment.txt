160949
5630
Không ai là không biết đến bộ truyện Doraemon và các nhân vật trong đó. Không biết từ lúc nào Doraemon là một phần không thể thiếu của cuộc sống, luôn hiện diện trong mỗi gia đình như là một người bạn tri giao của mỗi đứa trẻ. Doraemon như đưa tôi đến một thế giới mới, thế giới chỉ dành riêng cho những trẻ thơ, hồn nhiên và trong sáng. Cầm quyển truyện Doraemon trên tay, tôi nghĩ rằng, ít có bạn nhỏ nào là không đọc hết liền một lúc, truyện cuốn hút, hấp dẫn và hài hước trên từng trang sách. Không kể trẻ con, người lớn cũng tìm đến Doraemon như để hiểu nhiều hơn về con của mình, suy nghĩ cũng như cảm xúc khiến họ không khỏi bâng khuâng.
5
13723
2015-02-26 09:37:37
--------------------------
