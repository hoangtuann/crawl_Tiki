309480
6988
Cháu mình cũng thuộc dạng nhút nhát nên khi đọc truyện này thì rất thích hợp với bé. Nhân vật trong truyện là một cô bé nhút nhát. Em không có chút tự tin nào trước đám đông, ngay cả khi đứng trước cô giáo quen thuộc của mình. May mắn thay cho em (và cả cho cháu mình khi đọc) em đã học được câu thần chú diệu kỳ để chữa khỏi bệnh nhút nhát của mình. Đó là câu “Mình làm được”. Nghe thì đơn giản thôi nhưng để đặt hết niềm tin vào và thay đổi trở nên tự tin thì cũng không dễ dàng gì. Để có thể tự tin các em nhỏ cần phải nỗ lực nhiều tuy nhiên câu thần chú này cũng sẽ giúp nhiều cho các bé nhút nhát rồi sẽ tự tin và mạnh dạn hơn. 
5
43129
2015-09-18 23:54:57
--------------------------
