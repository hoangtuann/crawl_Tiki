568981
5454
ngòi bút sắc sảo châm biếm sâu cay. ông đúng là ông vua phóng sự đất bắc
5
5176966
2017-04-10 11:14:08
--------------------------
465694
5454
Quả nhiên là "ông vua phóng sự" của đất Bắc kỳ, Vũ Trọng Phụng khiến người đọc phải cười nhưng suy ngẫm lại về những cái gọi là giá trị đạo đức. Ở một cái xã hội mà đồng tiền có quyền lực vạn năng, bản chất lương thiện vốn có của con người thật khó lòng mà giữ được. Tình người cũng vì thế mà tan biến đi, chỉ còn lại những tính toán, lọc lừa nhằm đạt được lợi ích riêng. "Cái giá trị làm người nghĩa là có kho không bằng súc vật. Có khi con chó mỗi tháng khiến chủ tốn kém hơn tôi tớ trong nhà." Dưới xã hội như vầy thì bảo sao mà nghề "buôn người" không phổ biến cho được. Văn phong trào phúng của Vũ Trọng Phụng đúng là bậc thầy!!!
5
95034
2016-07-01 13:00:10
--------------------------
223496
5454
Vũ Trọng Phụng, một cây bút tài năng của nền văn chương Việt, được mệnh danh là ông vua phóng sự của đất Bắc Kì, cùng với lối văn trào phúng, châm biếm đả kích sâu cay cuộc đời. Đọc tác phẩm, người đọc như cười ra nước mắt khi chứng kiến những cảnh trái khoáy, những sự bất lương, những thủ đoạn đẩy người khác vào đường cùng... Cũng như nhiều tác phẩm khác, hai tác phẩm trên cũng cùng bóc trần bộ mặt thật của những con người bịp bợm, lọc lõi, đã đưa đến cái nhìn toàn cảnh, dẫu có hơi bi quan về thực tại xã hội bấy giờ! Nhưng chung qui lại, nó khiến cho người đọc hết sức thán phục trước cách viết quá ư là đặc sắc :Ngỡ vậy mà không phải vậy!
4
616731
2015-07-07 10:22:05
--------------------------
167512
5454
Biết đến Vũ TRọng Phụng bởi tác phẩm " Số đỏ " mình tiếp tục tìm mua những cuốn tiểu thuyết của ông . Quả thật câu truyện này cũng đầy trào phúng và sâu cay như các tác phẩm khác của ông . Với tài năng của mình, Vũ trọng Phụng đã lật tẩy cái thỏi bạc bịp , vô tâm , khốn nạn của cái xã hội thời bấy giờ.Danh hiệu “ông vua phóng sự” quả là xứng đáng với tài năng và cống hiến to lớn của Vũ Trọng Phụng trong việc phát triển, hoàn thiện thể phóng sự, góp phần khẳng định tên tuổi một nhà văn hiện thực lớn xuất sắc của nền văn học Việt Nam hiện đại.Là học sinh thích môn văn nên mình đã mua quyển này và thấy nó rất có ích cho việc học tập :)
5
455823
2015-03-14 22:13:18
--------------------------
148929
5454
Vũ Trọng Phụng không hề "phụ bạc" những kì vọng của tôi. Mỗi phóng sự, mỗi tiểu thuyết ông viết đều để lại trong tâm trí tôi những ấn tượng sâu sắc. Đọc phóng sự của ông khiến tôi đặt ra một câu hỏi: làm thế nào ông có thể thâm nhập vào từng cái xã hội thu nhỏ của chủ đề mà ông hướng tới, như là làng bạc bịp, với những mánh khóe tài tình hay ho. Từng cảnh đời được khắc họa lại chân thật. Không đọc phóng sự này, có lẽ tôi không biết được trên đời này còn từng ấy loại mánh khóe. Dù sao, không nói đến đạo đức hay luân thường đạo lý, thì những con người có nhiều thành tựu trong lĩnh vực mình làm, ấy là một loại tài năng.
5
1050
2015-01-11 23:46:00
--------------------------
