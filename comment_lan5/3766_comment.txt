347467
3766
Lời dẫn giải chưa được thuyết phục lắm về cách làm việc hiệu quả lâu dài . Nếu nhiều người sử dụng nhưng chưa đủ kinh nghiệm công việc thực tế thì chưa thể hoàn thành hết . Lý do ở con người chưa thể nhìn nhận theo lối tư duy chủ quan mà đi về lối mòn trong đánh giá hành vi tổng thể . Vẫn chưa có nhiều thực tế nói lên sự thay đổi của nhiều người vẫn chưa thực làm cho rõ ràng hơn . Có thể tham khảo lúc chưa tìm ra giải pháp tạm thời nào .
3
402468
2015-12-04 14:24:16
--------------------------
322754
3766
Mình mua cuốn sách này với mục đích tìm kiếm những kinh nghiệm về làm việc khi mình vừa mới tốt nghiệp và bắt đầu đi làm. Cuốn sách có phạm vi nội dung lớn và bao trùm, có thể tìm thấy phần lớn các khía cạnh của làm việc trong cuốn sách này. Cũng bởi vì phạm vi rộng nên có một số phần của cuốn sách đề cập một cách chung chung, tổng quát, chưa nêu được những "tips" cho người mới đi làm. Tuy nhiên cuốn sách cũng đã phần nào đưa mục tiêu chính là cung cấp cái nhìn toàn cảnh cho người đi làm và những vấn đề họ có thể gặp phải trong quá trình làm việc. 
3
697945
2015-10-16 22:18:59
--------------------------
318022
3766
Sách này mình mua tham khảo lấy ý tưởng khái quát thôi chứ không phải vì đi làm thật, quả thật thấy ngôn từ hay nhưng không thuyết phục lắm. Một số chương viết chặt chẽ, rất ổn, một số khác lại gây ra tâm lí phản bác vì không áp dụng được cho thực tế từng người. Bản thân đọc chỉ để tham khảo nên không đặt nặng lắm hay hay dở nhưng thấy nó không vừa ý mình lắm cũng hơi buồn. Ngoài vấn đề đó thì những cái khác đều ổn, bản thân mình thấy dùng cho sinh viên là tốt nhất vì nó giúp các bạn có cái nhìn tàn cảnh những gì nên hay không nên làm, cách hành xử để chuẩn bị tốt nhất cho tương lai của mình.
3
284845
2015-10-04 21:24:50
--------------------------
212554
3766
Theo cảm nhận của một ngườ mới đầu đi làm như mình thì Cuốn Sách Số 1 Về Làm Việc rất quan trọng! rất bao hàm và tổng quát.Ttuy nhiên còn khá sơ sài, đưa ra khá nhiều vấn đề mà có ít cách giải quyết. mình thấy nó không phải là phù hợp với tất cả mọi người đâu vì mình phải làm thế này làm thế kia trong khi mình nghĩ là bản chất con người mình là cái quan trọng nhất! tính cách con người mình có thể không phù hợp với nơi này thì sẽ phù hợp với nơi khác! quan trọng là mình được thoải mái! tuy nhiên cũng phải có nhiều lưa ý trong cuốn sách rất hay.
3
12722
2015-06-22 16:36:42
--------------------------
