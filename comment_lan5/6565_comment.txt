382500
6565
Quyển sách rất hay và hữu dụng cho ngày nay.trong sách có những cách dùng kế rất hay và những kế đó được tác giả giải thích nguồn gốc và cách dùng của nó một cách lôi cuốn và chi tiết. Đọc quyển sách này chúng ta không phai là người suy tính hay mưu mô. Chúng ta đọc để biết những cách họ hãm hại va biết cách suy đoán và đề phòng. Đọc và hiểu hơn về mưu kế.....những mưu kế trong sách được nói đến khá hay và chúng ta có thể áp dụng một số kế nhất định
4
359881
2016-02-19 11:48:16
--------------------------
322385
6565
Nhiều lúc có nhiều chuyện chúng ta không biết làm sao để giải quyết vấn đề khi chúng ta bị vào tình thế nguy hiểm. Quan trọng chúng ta không biết tại sao mình lại bị nguy hiểm... Thời Xuân Thu chiến quốc nước Việt có đại phu tên là Văn Chủng cùng với Phạm Lãi đã giúp Việt Vương Câu Tiễn đánh bại nước Ngô và đưa nước Việt trở thành một nước hùng mạnh lúc bấy giờ. Tuy nhiên, người biết sách lược, biết thức thời là kẻ chiến thắng trong mọi tình huống. Phạm Lãi biết được đạo lí " điểu tận cung tàn nên tự biết thói lui để tránh họa sát thân. Còn đại phu Văn Chủng thì không tránh khỏi họa sát thân. Trong quyển 36 kế và xử thế này thì việc thoái lui hay thức thời của Phạm Lãi là nằm trong phần thối chiến kế của quyển sách.
5
583002
2015-10-16 09:49:12
--------------------------
220781
6565
36 Mưu Kế Và Xử Thế" là một cuốn sách hay về xử thế, ngày nay người ta thường nói thương trường là chiến trường, dù là thương trường hay chiến trường thì việc dùng mưu mẹo đóng một vai trò vô cùng quan trọng , quyết định sự thành bại của một thương vụ. Cuốn sách có thiết kế bìa ngoài đẹp, bắt mắt giúp dễ dàng liên tưởng đến 36 mưu kế binh pháp nổi tiếng của Tôn Tử. Nội dung trình bày rõ ràng, logic giúp cho người đọc dễ nắm bắt. Là một cuốn sách hay nên đọc và ứng dụng vào cuộc sống ( theo quan điểm cá nhân của mình)
5
572238
2015-07-02 23:05:03
--------------------------
140256
6565
"36 Mưu Kế Và Xử Thế" là cuốn sách rất hay, rất thiết thực trong đời sống. Nhìn qua có thể thấy, cuốn sách có bìa ngoài đẹp, ấn tượng, hấp dẫn người đọc,... Nội dung không mới nhưng hấp dẫn người đọc bằng phép lập luận chặt chẽ, rõ ràng, lô-gic, dẫn dắt thú vị, ngôn từ dễ hiểu,... Cuốn sách là tập hợp nhưng kinh nghiệm quý giá qua đời sống thực tế và qua các nghiên cứu về tâm lý học, kinh tế học, mang đến cho người đọc 36 mưu kế hay và hiệu quả nhất để áp dụng trong xử thế trong cuộc sống và công việc. Cuốn sách có tính thực tế cao, giá trị nhân văn sâu sắc, phù hợp với nhiều lứa tuổi.
5
414919
2014-12-10 17:08:39
--------------------------
