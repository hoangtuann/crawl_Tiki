468997
8352
Tôi đã mua cuốn “Loài Hoa Nào To Nhất Thế Giới” vì giá thành sản phẩm hợp lý, hình ảnh sặc sỡ, chữ in rõ nét, và giấy dày. Sản phẩm này tôi không thấy có khuyết điểm nào, trừ bố cục sách nên trình bày câu hỏi trang bìa trước nhất, thay vì những câu hỏi khác. Tuy nhiên, tôi thấy khá thú vị, khi các cháu bé nhà tôi đang tìm câu trả lời cho câu hỏi “Loài Hoa Nào To Nhất Thế Giới”, thì các bé lại được biết thêm “Loài Hoa Nào Nhỏ Nhất Thế Giới”. Nhìn chung, đây là một quyển sách tuy nhỏ, nhưng chứa nhiều thông tin kiến thức hay.
4
966571
2016-07-05 21:17:11
--------------------------
