451165
5652
Đây là một trong những tập truyện Conan mà mình cảm thấy rất đáng sợ... nhất là vụ Sonata ánh trăng, những vụ án giết người cứ liên tục xảy ra mà ngay cà Conan cũng không ngăn cản được làm cậu cảm thấy bất lực, còn người mình thì tim đập tình thịch lo sợ sẽ càng có nhiều người chết hơn. Bi thảm hơn là hung thủ lại chính là "anh" bác sĩ, người đã cải trang thành nữ, vô cùng thân thiện và tốt bụng, khi nghe tin cô ấy chết mình đã rất buồn nhưng không ngờ cái chết đó là giả và chính cô là hung thủ...
4
362041
2016-06-19 14:36:45
--------------------------
386496
5652
Thám tử̀ lừng danh Conan tập 7 (tái bản 2014) quá xuất sắc hấp dẫn lôi cuốn vô cùng, mỗi tập lại là những mẫu chuyện khác nhau với tình tiết hấp dẫn khác nhau nhưng không quá trùng lập không quá khô khan tác giả đã khắc họa được nét tính cách chững chạc của cậu học sinh cấp 3 trong thân xác của cậu bé học sinh tiểu học, sự thông minh, bình tỉnh sử lí tình huống của Conan đã góp phần làm nên sự xuất thần của nhân vật và sự hấp dẫn của truyện. Tập này Ran ghen kinh khủng quá, tội cho Shinichi đã phải phá án tìm người rồi còn bị Ran "truy sát" như vậy, anh chàng này nhát quá . Rất cảm ơn tác giả đã dày công để sáng tác ra một tuyệt phẩm như thế này!
5
853243
2016-02-25 19:38:41
--------------------------
352947
5652
Vụ án chiếc dương cầm ở đảo Tsukikage quá ám ảnh nhỉ, hung thủ lại là người có cảm giác hiền lành, đáng tin, lại là người có bằng chứng ngoại phạm chắc chắn nhất. anh ấy cải trang quá tài tình đi, bao nhiêu năm mà mọi người vẫn không nhận ra, để có thể sống dưới cái vỏ bọc đó lâu như vậy, hẳn là mối thù đó sâu sắc lắm, nhưng cuối cùng lại là người đáng thương nhất. Conan không chỉ phá án xuất sắc mà còn có thể làm dịu đi lòng thù hận. Tập này Ran ghen kinh khủng quá, tội cho Shinichi đã phải phá án tìm người rồi còn bị Ran "truy sát" như vậy, anh chàng này nhát quá
5
580575
2015-12-15 12:06:00
--------------------------
325724
5652
Cô ấy làm mình nghĩ ngay tới nàng cung Thiên Yết, yêu hết mình nhưng ghen cực kỳ. 
Vụ án ở đảo khá hay. Nhưng tinh ý ngay từ đầu biết ngay hung thủ rồi. Bởi vì người ấy giới thiệu ngay rằng đến từ Tokyo. 
Dễ nhận ra rằng hung thủ là người mời Mori đến đảo nhằm chứng kiến mình giết người chứ nếu điều tra án thì không cần phải giấu mặt. Mà mời tới vậy thì phải biết Mori nổi tiếng. Người ấy ở Tokyo dĩ nhiên biết. 
Đọc tiếp sẽ thấy rằng dân ở đảo không biết Mori là thám tử, ai cũng ngạc nhiên hoặc tưởng ông là phi công. 
Chỉ duy nhất người ấy không ngạc nhiên còn chủ động bám riết họ một cách kỳ cục. 
Tác giả vẽ nhiều chân rít cố làm rối bố cục nhưng lại sơ ý ngay từ đầu rồi. 
5
535536
2015-10-24 02:39:21
--------------------------
259357
5652
Không thể tin được hung thủ của vụ án trên đảo lại là cô bác sic ấy à không phải là anh bác sĩ ấy.Chẳng hiểu sao đọc conan quen rồi mà cứ thấy rợn người khi nhìn thấy xác chết ý.Trong tập 16 Conan có nói với hattori rằng "Thám tử điều tra ra hung thủ rồi mà lại để hắn chết ngay trước mắt chẳng khác gì đồng phạm.Nhưng trong trường hợp này conan đã cố gắng lắm rồi mà.Thậm hí Narumi còn để lại thông điệp cảm ơn với Conan mà.cũng hơi bực khi thấy có người mạo nhận làm người quen của Shinichi nhưng chắc là người ta có uẩn khúc.
5
457454
2015-08-09 13:09:42
--------------------------
250616
5652
Vụ án chiếc dương cầm ở đảo Tsukikage phải nói là xuất sắc, có ai ngờ thủ phạm lại là người được loại ra khỏi những người tình nghi ngay từ đầu. Nhưng những vụ án mạng cứ liên tiếp xảy ra khiến Conan nghi ngờ và suy luận nhạy bén đã giúp cậu khám phá ra một bí mật không ai ngờ được. Và kết thúc cũng thật bất ngờ, cứ tưởng sau khi chân tướng bị lộ thủ phạm sẽ giơ tay nhận tội nhưng không, Conan đã phải bất lực khi nhìn người đó chết mà không thể làm gì khác được.Rất ấn tượng!
4
471112
2015-08-02 10:37:56
--------------------------
176405
5652
Trong tập này, đa phần là những vụ án xảy ra ở đảo Tsukikage cùng với các bản nhạc Sonata ánh trăng của Bethoven. Những vụ án này thật đáng sợ và dã man nhưng lại có một kết thúc rất bất ngờ. Có lẽ, Conan cảm thấy khó chịu lắm, nhìn thấy tội phạm chết ngay trước mắt mình mà mình lại không thể làm gì được. Tuy nhiên, tập này cũng lấy đi không ít sự tiếc của người đọc, mình cũng đã lắng lòng khi nhìn thấy sự bất lực của Conan. Cuối cùng, trong 10 tập đầu, có lẽ, đây là tập đáng nhớ nhất.
4
13723
2015-04-01 09:26:00
--------------------------
