453789
3938
Mình mua cuốn Bành Tổ Dưỡng Sinh Kinh này vì tò mò đã từng nghe truyện về Bành Tổ này sống 800 trăm tuổi dĩ nhiên là có thật hay không cũng chưa ai kiểm chứng như là truyền thuyết, và xem bí quyết của Bành Tổ cách dưỡng sinh và bí kíp thức ra không gì ghê gớm lắm đâu chủ yếu là sống sạch, thanh tịnh, kết hợp ăn uống theo ngũ hành âm dương điều hòa và thiền thực ra khó áp dụng đời sống hiện đại đủ thứ chi phối trừ phi sống trên núi thôi, đặc biệt môi trường sống nữa các bạn ạ, và theo truyền thuyết chỉ là truyền thuyết thôi nhé Bành Tổ mất vì nhà trời phát hiện ông là người không có trong sổ sinh tử và phát hiện ra ông thế là bắt ông đi... vậy có thể không bắt ông còn sống già hơn nữa và có lẽ là bất tử hihi... dù sao đây cũng là cuốn sách thú vị để tìm hiểu cách sống người xưa...
3
730322
2016-06-21 14:45:16
--------------------------
306432
3938
Qủa thật mình mua cuốn sách này vì cái tên rất ư là hấp dẫn! Ai chẳng biết Bành Tổ là người sống thọ nhất từ trước tới giờ, từ trước đến giờ biết bao nhiêu thế hệ đều muốn khám phá ra bí quyết trường thọ của ông và mình cũng khộng ngoại lệ! Đây là một cuốn sách dưỡng sinh tương đối khó đọc vì nó đòi hỏi người đọc phải có chút kiến thức về âm dương ngũ hành vốn là 1 trong những nhân tố chính của cuốn sách. Tuy nhiên khi bạn dày công chiêm nghiệm ra rồi thì sẽ thấy nó cũng chỉ là những cách đơn giản dễ thực hiện trong cuộc sống này! Nếu bạn yêu thích tìm tòi những bí ẩn về người xưa thì hãy mua nó vì nó sẽ bổ sung cho bạn một số kiến thức mới lạ!
4
143101
2015-09-17 16:20:39
--------------------------
