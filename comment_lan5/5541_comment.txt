543489
5541
Rất hài lòng vì sản phẩm của Tiki, tuy nhiên phí giao hàng hơi cao, mong rằng Tiki sẽ tiếp tục phát triển mô hình này, cực kỳ tiện lợi!
5
2232281
2017-03-15 15:42:28
--------------------------
467511
5541
Truyện này hay cực kỳ. "Bỉ vỏ" là một câu chuyện buồn để lại nhiều suy ngẫm, rất đáng đọc. Nhưng mà có một số chỗ miêu tả thực quá khiến mình cảm thấy không phù hợp với trẻ em lắm. Bỉ Vỏ khắc họa xã hội Việt Nam trong những năm 1930 một cách chân thực quá đến nỗi bản thân người đọc cũng như cảm nhận được từng cơn khổ nhục đau đớn của nhân vật chính trong câu chuyện. Truyện kể về những thăng trầm trong cuộc sống của Bính, tuy là người lương thiện nhưng do dòng đời xô đẩy mà phải làm những việc trái vào lương tâm, đạo đức.
5
808428
2016-07-03 22:41:14
--------------------------
459705
5541
Hồi mình học một đoạn trích trong Những Ngày Thơ Ấu của tác giả Nguyên Hồng, cô dạy văn mình đã giới thiệu cho lớp mình quyển này . 
Ngay từ nhan đề đã để lại ấn tượng rất sâu sắc cho độc giả, lần đầu tiên nghe từ "Bỉ Vỏ" mình đã không hiểu nó là gì, còn tự dịch bừa, sau đó mình đọc giới thiệu tác phẩm mới biết đây là từ lóng để chỉ những người đàn bà ăn cắp.
Truyện là những thăng trầm trong cuộc sống của Bính, tuy là người lương thiện nhưng do dòng đời xô đẩy mà phải làm những việc trái vào lương tâm, đạo đức. Truyện được Nguyên Hồng thể hiện rất xuất sắc những khung bậc cảm xúc của các nhân vật. 


5
505164
2016-06-26 13:07:23
--------------------------
409345
5541
Cuốn sách ấn tượng ngay từ tên gọi Bỉ vỏ, nghe rõ lạ bởi đó là tiếng địa phương, Cuốn sách cho ta hiểu thêm về thế giới "anh chị " thời đó khốc liệt thế nào, tuy nhiên nó vẫn thấm đẫm tính nhân văn, tình người, tình thương giữa những con người lầm lỡ ấy thật đáng nể phục. Thế nhưng mình không thích kết việc người đàn bà đó lại xa lầy vào trộm cắp, mình mong tình thương của họ có thể làm họ muốn tránh xa con đường tội lỗi này. Ngoài ra mình rất ấn tượng về cách dùng từ của tác giả, mà đa số là khẩu ngữ dân anh chị, nó là câu chuyện chân thực hơn
4
402348
2016-04-02 12:46:42
--------------------------
402520
5541
Câu chuyện về một cô gái nơi thôn quê, vì lầm lỡ nên đã mang thai, vì luật làng quá khắt khe nên cô buộc phải bán đứa con mình mang nặng đẻ đau cho một gia đình nhà giàu khác. Cô phải đi tha hương cầu thực ở nơi đất khách quê người, rồi cô lại một lần nữa bị kẻ gian hại, khiến cho cô bị bệnh. Nhờ vậy nên cô đã gặp được Năm, người đã chuộc cô ra khỏi nơi nhà chứa. Sau khi trải qua bao sóng gió với Năm, hai người quyết định làm một vụ cuối để dồn tiền chuộc đứa con năm xưa của mình, nhưng không ngờ, Năm lại cướp chiếc vòng của đứa bé năm xưa của Bính, khiến cho đứa trẻ chết. Một cái kết đầy đau khổ.
5
791798
2016-03-22 14:10:10
--------------------------
393435
5541
Nguyên Hồng là một trong những tác giả mà mình rất thích sau khi đọc tác phẩm những ngày thơ ấu. Tác phẩm Bỉ vỏ- một cái tên mà chắc chắn rất ít người biết nó có ý nghĩa là gì. Chuyện kể về một cô gái nghèo tên Bính, vì trót lỡ lầm trong tình yêu mà có thai sau đó lại bị người yêu ruồng bỏ. Trải qua những tháng ngày bấp bênh, không biết trải qua bao nhiêu lần cay đắng tủi nhục, Bính đã biến thành một bỉ vỏ- người đàn bà trộm cắp. Cô đã có cơ hội hoàn lương khi lấy một mật thám, nhưng số phận và may mắn không mỉm cười với cô. Cuối cùng, tất cả kết thúc trong trại giam khi cô bị chính người chồng mật thám kia bắt.
4
632796
2016-03-08 19:33:08
--------------------------
363183
5541
Mình rất thích đọc các tác phẩm của Nguyên Hồng. Ngay từ hồi học cấp 2, mình được học và đọc truyện ngắn của ông và mình thật sự bị cuốn hút vào dòng tự sự chan chứa tình cảm của nhà văn. Cô giáo dạy văn hồi đó của mình có kể về tác phẩm Bỉ vỏ, mình rất thích truyện này qua lời kể của cô và mong sớm được đọc đầy đủ tác phẩm này nhưng mãi bây giờ mới có dịp tìm đọc và cầm cuốn sách say sưa. Mình thật sự xúc động và xót thương cho cuộc đời của người phụ nữ và những co người ở đáy xã hội vào thế kỉ trước. Một bức tranh đầy mảng sáng tối, phản chiếu cuộc sống đầy cơ cực, đau khổ và tủi nhục của những con người trôi nổi trong cuộc đời của chính họ và trong cả cái xã hội đầy bất công thời đó. Một cuốn sách đáng đọc! 
4
574805
2016-01-03 18:25:28
--------------------------
351583
5541
"Bỉ vỏ"- một tác phẩm không mới của nhà văn Nguyên Hồng.
Càng đọc và ngẫm nghĩ tác phẩm này, ta càng thương cảm cho số phận người phụ nữ trong xã hội xưa. Người phụ nữ từ chân chính- đoan trang đã bị cái xã hội nơi mà nhân phẩm người phụ nữ không hề được coi trọng- làm cho bị tha hóa, không còn giữ được vẻ đẹp bản chất vốn có. Đọc "Bỉ vỏ" ta có thêm hiểu biết về xã hội xưa và thân phận hẩm hiu của người phụ nữ trong xã hội đó.
Tôi không có gì ngạc nhiên với sự hẩm hiu ấy của họ, bởi qua nhiều tác phẩm đương đại khác tôi phần nào hiểu được, nhưng "Bỉ vỏ" lại mang đến cảm giác khác khi khai thác vấn đề ở một khía cạnh mới.
3
513404
2015-12-12 15:00:34
--------------------------
344968
5541
Bỉ vỏ là tác phẩm phản ánh rõ hiện thực của xã hội phong kiến thối nát . Tôi nghĩ cuốn truyện phù hợp với lứa tuổi từ lớp 8 trở lên . Bìa sách thiết kế quá chuẩn và phù hợp với nội dung của câu truyện . Tác giả đã rất thành công khi khắc hoạ nhân vật bính cũng như cuộc sống xã hội . Nhưng cái kết cho câu truyện rất đắng lòng và rất cảm thương cho bính đó là đứa con của bính bị giết chết dưới tay của năm sài gòn . Và tác phẩm rất hữu ích cho những ai đang nghiên cứu về văn học trong thời kì văn học trước cách mạng tháng 8

5
898367
2015-11-29 15:03:38
--------------------------
319381
5541
Mình rất thích mua và đọc những tác phẩm của thời xưa, những tác phẩm được học trong sách giáo khoa. Được cô chủ nhiêm giới thiệu cho mình quyển sách này nên đã mua về đọc.
Đằng sau cái sự tha hóa biến chất của cô gái nông thôn còn có tình yêu, có sự che chở giữa những kẻ mà xã hội không coi họ ra gì! mình đã khóc trước số phận đưa đẩy vùi dập của Bính. tác giả đã vẽ nên một bức tranh chân thực, trần trụi, phơi bày cái xã hội mục nát lúc bấy giờ, thân phận của người phụ nữ khi sống trong thời đại trọng nam khinh nữ. Đây cũng có thể lấy làm dẫn chứng cho bài nghị luận về thân phận người phụ nữ xưa và nay mà mình chuẩn bị viết. Rất hài lòng khi đã chọn mua quyển sách này!  
5
782953
2015-10-08 16:17:14
--------------------------
279288
5541
Đọc "Tôi học Đại học" mình rất thích những phát biểu của nhà văn Nguyên Hồng được thầy Nguyễn Ngọc Ký chia sẻ. Lần đổi gió với những tác phẩm văn học Việt Nam, mình đã hào hứng chọn mua những tác phẩm của các tác giả: Thạch Lam, Tô Hoài, Nam Cao...và "Bí vỏ-Nguyên Hồng".  Tác phẩm này dễ đọc, giọng văn gần gũi, cốt truyện gắn với hiện thực xã hội lúc bấy giờ. Đọc tác phẩm rồi mình thật biết ơn vì được sinh ra trong xã hội mới-phụ nữ được nhận nhiều ưu ái và quyền lợi. Không như số phận của Bính và chị Minh, số phận người phụ nữ gắn với lề thói khắc nghiệt, đáng sợ. "Bỉ vỏ" là một câu chuyện buồn để lại nhiều suy ngẫm, rất đáng đọc.
5
247455
2015-08-26 17:21:38
--------------------------
245682
5541
Chân thực và sâu sắc. Tôi đã khóc rất nhiều khi đọc qua những nỗi khổ tâm của Tám Bính. 1 số phận bị đẩy đưa, vùi dập, dù luôn cố gắng giữ cho lòng mình trong sạch, nhưng cuối cùng vẫn không được. Đòn thù cuối cùng của cuộc đời giáng lên Tám Bính đủ để cho bất cứ độc giả nào cũng phải nghẹn ngào.
Nguyên Hồng thực sự rất xuất sắc khi đã thật thà nói lên được những đau thương trong cuộc sống của những con người dưới đáy xã hội, đặc biệt là những người phụ nữ.
4
293599
2015-07-28 23:48:17
--------------------------
224179
5541
Một phụ nữ đầy lòng yêu thương nhưng do đời đưa đẩy, cô ấy đã bị tha hóa. Nhưng trong sự tha hóa ấy, cô ấy lại tỏa rạng một nhân cách đẹp. Tám Bính ngoan đạo, yêu thương chồng, thương con mình,... Một người phụ nữ bị tha hóa nhưng lại có những đức tính ấy thì thật là đáng khâm phục. Đọc "Bỉ vỏ" mới thấy xã hội thời ấy thật tàn nhẫn và đầy dục vọng. Cám ơn Nguyên Hồng đã cho chúng ta thấy điều đó!
5
502448
2015-07-08 10:56:12
--------------------------
219932
5541
Có lẽ mình là một người hoài cổ, vẫn thích mua và đọc những tác phẩm văn học ngày xưa, cái thời kì có phần đen tối của xã hội xưa! cùng với bước đường cùng thì bỉ vỏ là một tác phẩm mình ấn tượng sâu sắc! ấn tượng từ cái tên, chắc chẳn nhiều người thắc mắc không hiểu bỉ là gì, sao lại là bỉ vỏ... Đằng sau cái sự tha hóa biến chất của cô gái nông thôn còn có tình yêu, có sự che chở giữa những kẻ mà xã hội không coi họ ra gì! Nếu năm không yêu thương Bính, liệu năm có cướp đứa trẻ về cho Bính không... Không thể trách họ, chỉ có thể nói xã hội thời đó khó cho họ con đường sống! câu chuyện ngày xưa và bây giờ, nếu là bây giờ bỉ vỏ ở đất cảng sẽ khác, nhưng tình người sẽ ra sao,...lan man một chút thôi! Đọc cũng đọc rồi! mua về để đó cho ai đó đọc và ngẫm nghĩ hiểu chuyện ngày xưa theo cách của mình
4
479090
2015-07-02 08:37:18
--------------------------
218004
5541
Tôi ấn tượng với Nguyên Hồng từ thời cấp 2, lúc đọc tác phẩm Những ngày thơ ấu được trích trong sách giáo khoa. Vì vậy tôi tìm đọc các tác phẩm khác của ông. Bỉ vỏ không phụ sự kì vọng của tôi. Với giọng văn độc đáo, Nguyên Hồng đã vẽ nên một bức tranh chân thực, trần trụi, phơi bày cái xã hội mục nát lúc bấy giờ, vẽ nên bức tranh thực tế đến đau lòng của người phụ nữ trong giai đoạn chiến tranh, ở cái xã hội cũ ấy. Một tác phẩm rất đáng đọc.
4
193839
2015-06-30 13:27:45
--------------------------
160925
5541
Như bao cô gái nông dân hiền lành chân chất, Bính cũng ao ước có được cuộc sống hạnh phúc, tuy nhiên, sự lừa lọc, những thói xấu của xã hội đã xô đẩy cô  xuống đáy bùn đen.
Bính là đại diện cho người nông dân lam lũ, cho tầng lớp lao động trong xã hội phong kiến, muốn hướng đến cái đẹp, cái thiện nhưng bị chính sự bất công, thối nát của xã hội đẩy xuống tận cùng. Đọc "Bỉ vỏ", ta liên tưởng đến những nhân vật cùng số phận như chị Dậu của Ngô Tất Tố, lão Hạc, Chí Phèo của nhà văn Nam Cao. Tương lai của họ là sự tăm tối, không lối thoát. Câu chuyện đã kết thúc nhưng khiến người đọc ngậm ngùi thương cảm cho số phận của con người.
4
473130
2015-02-26 08:36:33
--------------------------
160273
5541
Mình viết những dòng nhận xét này trong hoài niệm. Vì mình đọc cuốn này đã rất lâu rồi. Cũng giống như Chí Phèo của Nam Cao, truyện là câu chuyện về quá trình tha hóa của một người phụ nữ, từ một cô gái xinh đẹp ngây thớ trở thành một người đàn bà lẳng lơ, lọc lõi ngón đời. Bỉ vỏ hấp dẫn người đọc ngay từ cái tên- người đàn bà ăn cắp, tiếng lóng của phường trộm cướp miền Nam. Nguyên Hồng là một nhà văn tài tình trong việc xây dựng nhân vật, nội tâm, tính cách. Về nội dung câu chuyện thì đã có phần giới thiệu ở trên rồi, có lẽ không cần trích lại nữa. Truyện mang nhiều tính nhân văn, cũng giống như Chí Phèo của Nam Cao, nhân vật trong đây luôn day dứt tìm lương thiện nhưng dòng đời xô đẩy phũ phàng, con người đã gục ngã trước ngưỡng cửa của sự lương thiện!!
5
123938
2015-02-23 14:02:53
--------------------------
157311
5541
Đọc "Bỉ Vỏ" của NGuyên Hồng, không ai không khỏi bùi ngùi và xúc động trước hoàn cảnh chrua Bính, một cô gái quá ngây thơ và trong sáng, chỉ mong một tình yêu đẹp dành cho mình. Đó không phải chỉ là một mình ước nguyện của cô, mà hơn bao giờ hest, nó là ước mơ, khát khao của của bao người phụ nữ. Bính là một đại diện. Với tầng lớp bình dân, vì tình yêu Bính đã sai lầm dẫn đến phải có con. Với một tác giả bình dân như Nguyên Hồng, ông hiểu và thấu được nỗi đau của người phụ nữ. Văn phong bình dị, mộc mạc, đồng thời cũng lên án một xã hội phong kiến gay gắt, khiến cuộc đời cô Bính lầm than. Hạnh phúc của người phụ nữ chỉ khát khao có mái ấm, có gia đình, nhưng ôi, Bính ơi. một bước lầm lỡ khiến cả cuộc đời tiêu tan.
5
33116
2015-02-08 12:25:43
--------------------------
147944
5541
Tiêu đề của tôi có thể khiến các bạn nghĩ đây là một câu chuyện kinh dị, nhưng thật ra không phải vậy. Bỉ Vỏ khắc họa xã hội Việt Nam triong những năm 1930 một cách chân thực quá đến nỗi bản thân người đọc cũng như cảm nhận được từng cơn khổ nhục đau đớn của nhân vật chính trong câu chuyện. Cái xã hội xưa thật đáng sợ. Chương trình ngữ văn THCS hay THPT tuy có đưa vào các tác phẩm cùng thời kì này nhưng chỉ là những đoạn trích ngắn, học sinh không thể hiểu được một cách chân thực nhất thực trạng xã hội lúc bây giờ. Với Bỉ Vỏ, lần đầu tiên tôi có cái nhìn toàn diện về cả bộ mặt xã hội ngày ấy, cái xã hội đã đày đọa, dẵm đạp lên người con gái ấy. Tàn tạ và xơ xác, nhưng BÍnh vẫn chưa từng từ bỏ tia hy vọng nhỏ nhoi vào một cuộc sống khác, ấy thế mà tình huồng cuối chuyện thật bất ngờ, thật đau xót biết nhường nào...
Trong truyện có một số đoạn miêu tả được cho là hơi thực quá, thậm chí thô. Nhưng bản thân tôi không cho là vậy, điều đó chỉ càng thể hiện Nguyên Hồng đã không giấu giếm sự thật, không ngần ngại phơi bày ra ánh sáng cái hiện thực xã hội tàn khốc lúc bấy giờ.
5
496784
2015-01-09 13:37:48
--------------------------
138808
5541
Bỉ vỏ diễn tả một xã hội hồi xưa rất đáng sợ. Mình cũng không nghĩ là những năm 1930 nó lại ghê đến vậy. Mọi người toàn hành động theo luật rừng, giang hồ, xã hội đen, cướp bóc... lộng hành đầy ngoài đường, khiến cho cô nhân vật chính chịu khổ từ trang đầu tới trang cuối của quyển sách.

Cách thức miêu tả của tác giả hay, có đầy chất văn và nên thơ, từ vựng dồi dào và cách dùng từ của ông rất đa dạng, linh hoạt. Ông viết Bỉ Vỏ khi đang ở đúng độ tuổi của mình hiện tại (hình như 21 tuổi) nên mình biết lượng từ vựng này phải do đọc sách rất nhiều mới có được. 

Lối hành văn tả thực, nghiêm túc và không có trào phúng, mỉa mai nhưng đôi khi tả thực quá, vài chỗ dung tục mà mình đọc không trôi - hầu hết những đoạn tả thân thể Bính hay những cảnh hiếp dâm mình thấy đều hơi quá đáng, không biết các bạn nữ khi đọc sẽ cảm thấy thế nào.
3
418158
2014-12-04 18:28:33
--------------------------
