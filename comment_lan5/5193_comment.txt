501017
5193
Cuốn sách này là mình mua giúp cho cậu mình. Cậu mình rất thích quyển sách này. Cuốn sách này được in bìa rất đẹp. Chất liệu giấy tốt. Nội dung rất rõ ràng và nói rất chính xác về cung Kim Ngưu. Tiki giao hàng rất là nhanh. Cảm ơn Tiki nhé
😀😁
5
1294315
2016-12-27 17:50:38
--------------------------
422625
5193
Cuốn sách này mình cảm thấy được. Thứ nhất về hình thức thì cuốn sách trông rất dễ thương, ngộ nghĩnh có kèm theo đồ đánh dấu trang rất đẹp. Sách nhỏ gọn dễ cầm theo bất cứ lúc nào. Về nội dung thì mình nghĩ nó phù hợp cho lứa tuổi mới lớn. Tuy nhiên thì mình vẫn thấy nó khá ngắn và chưa đề cập chuyên sâu. Nếu góp ý thì mình cũng góp ý thêm cho sách cần tăng độ dày với nhiều nội dung phong phú hơn. Như vậy sẽ giúp sách trở nên hấp dẫn hơn. 
4
1169207
2016-04-27 18:37:23
--------------------------
374688
5193
Về hình thức: quyển sách nhỏ nhỏ, xinh xinh, bìa sách đậm chất Kim Ngưu, book mark kèm theo khá là dễ thương. 
Về nội dung: những bài trắc nghiệm thú vị, các thông tin vô cùng bổ ích, hình minh họa ngộ nghĩnh giúp mình hiểu hơn về cung hoàng đạo của bản thân. Đặc biệt, những thông tin về Kim Ngưu được trình bày trong sách có nhiều nét tương đòng với mình, từ tính cách đến sở thích, từ sở trường đến sở đoản.
Tóm lại: Đây chính là một quyển sách cần thiết cho những ai muốn hiểu thêm về cung hoang đạo của bản thân cũng như bạn bè, người thân. Đây cũng là món quà ngọt ngào dành cho bạn bè, người thân và cả "gà bông" và "cạ cứng".
5
1080233
2016-01-26 21:13:47
--------------------------
333286
5193
Cuốn sách giống như một cẩm nang nhỏ bỏ túi vô cùng tiện lợi và hữu ích.
Với những ngôn từ hài hước, những mẩu chuyện thú vị, giúp mình hiểu hơn về chòm sao đại diện của mình, về những đứa bạn xung quanh, về gà bông rồi vô số chuyện vụng vặt khác =))) 
Sách tuy nhỏ nhưng tiện ích vô cùng, còn được tặng kèm kẹp sách vô cùng xinh xắn đáng yêu, rất đáng để sở hữu :)
Mua về, đọc nó rồi cùng cảm nhận xem nó có đúng với bản chất thật của mình không, xem xem là tốt hay xấu để phấn đấu hoàn thiện :***
5
768377
2015-11-07 19:11:44
--------------------------
