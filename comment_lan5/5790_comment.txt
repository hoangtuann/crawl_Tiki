491070
5790
Quyển sách này rất ý nghĩa và hữu ích trong việc giáo dục trẻ, hình thành nhân cách. Nó cũng tốt cho học sinh phổ thông và thanh thiếu niên. Đọc quyển sách này làm cho mọi người trẻ có động lực để không từ bỏ ước mơ và lý tưởng của mình.
5
1064118
2016-11-16 07:15:56
--------------------------
438155
5790
Đối với một học sinh cấp hai như mình, những bài văn luận rất nhức đầu và nhàm chán, thiếu rất nhiều ý tưởng nhưng từ khi đặt hàng cuốn sách này của Tiki, mình không còn sợ viết nghị luận nữa vì quyển sách tuy nhỏ nhắn như vậy, nhìn cứ như cho mẫu giáo đọc thôi nhưng thật ra bên trong chứa rất nhiều câu chuyện hay, đặc biệt hơn hết là rất rất nhiều câu danh ngôn nổi tiếng mà mình không biết. Không dừng lại ở đó, nhiều lúc nó giúp mình suy ngẫm lại đời sống bộn bề ngày nay.. liệu mình sống đã tốt chưa?? Rất cảm ơn Tiki
4
541588
2016-05-29 12:12:17
--------------------------
387003
5790
Cuốn “Gieo mầm tính cách – Kiên trì” nằm trong bộ 6 cuốn do Nhà xuất bản trẻ phát hành. Cuốn này có các câu chuyện như : Chuyện của những người thành công, Cậu bé và hũ đậu phộng, Con kiến và chiếc lá, Ai cũng có thể thành đạt, Con quạ và bình nước, Có chí thì nên, Vua Bruce và con nhện, Sức mạnh ý chí, Hãy bước lên, Đôi chân kỳ diệu, Ngu Công dời núi, Học cách đứng lên. Kiên trì là một đức tính tốt mà các bé nên được rèn luyện từ nhỏ, thông qua các câu chuyện này, bố mẹ có thể giúp bé hiểu được đức tính kiên trì sẽ giúp ích cho các bé như thế nào ở mọi mặt của cuộc sống.
3
15022
2016-02-26 14:10:22
--------------------------
322599
5790
Rất thích bộ sách Gieo mầm tính cách của Nhà xuất bản Trẻ. Trước hết là bộ sách này có kích thước vừa phải, xinh xắn, mà mình nghĩ là phù hợp bàn tay cầm và tiện cho các em học sinh mang theo. Thứ hai là nội dung sách có tính giáo dục cao mà không khô cứng, không hình thức. Như tập sách chủ đề Kiên Trì này, viết rất giản dị mà gửi gắm được bao bài học đạo đức sâu sắc. Sách dành cho học trò như thế này thì quá hữu ích và đáng đọc lắm.
3
531312
2015-10-16 17:21:56
--------------------------
166343
5790
Edison cũng từng nói: “Thiên tài là một phần trăm cảm hứng và 99 phần trăm đổ mồ hôi”
Dân gian ta nói rõ hơn: "Cần cù bù thông minh"
Kiên trì, kiên nhẫn hay siêng năng là 1 phẩm chất cần rèn luyện từ thưở ban đầu của trẻ em. Bởi vì sau này chúng không chỉ kiên trì trong học tập mà còn phải kiên trì trong kinh doanh, trong các mối quan hệ xã hội ,... Nếu không kiên trì thì sự học không thể thành công, dễ mà đốt cháy giai đoạn. Vì thế " Gieo Mầm Tính Cách - Kiên Trì " là 1 cuốn sách đắc lực trong việc rèn luyện trẻ em hình thành riêng cho mình đức tính kiên trì. Chất lượng giấy của NXB Trẻ khá tốt và chất lượng.
5
114793
2015-03-12 15:11:28
--------------------------
