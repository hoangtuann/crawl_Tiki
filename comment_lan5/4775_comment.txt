394822
4775
"Doraemon" – là một bộ truyện tranh rất hay của tác giả người Nhật Bản Fujio Fujiko gắn liền với tuổi thơ của mình. Bộ truyện tranh có nội dung xoay quanh cuộc sống hàng ngày của cậu bé Nobita – một cậu bé lười biếng, học kém, yếu thể thao và những cuộc phiêu lưu của cậu và nhóm bạn gồm: Xuka, Xê-ko, Chaien và chú mèo thông minh đến từ tương lai – Doraemon. Qua những câu truyện hàng ngày đời thường, những cuộc phiêu lưu ly kỳ và hấp dẫn, ta sẽ rút ra được rất nhiều bài học ý nghĩa và sâu sắc. Mình rất thích bộ truyện này.
5
1142307
2016-03-10 23:05:02
--------------------------
