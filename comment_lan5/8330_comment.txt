122930
8330
Chuyến phiêu lưu đến thành phố dây cót lần này không chỉ mang lại những trải nghiệm thú vị cho nhóm bạn Nobita mà còn cho cả bản thân mình nữa. Tại thành phố toàn những con thú nhồi bông, những đồ chơi hiện đại trở nên có sức sống nhờ được vặn dây cót, nhóm bạn được thỏa sức vui chơi, sáng tạo, xây dựng những công trình đường sá cầu cống.... Đặc biệt làm mình bất ngờ là sự xuất hiện của những con thú biết nói và vô cùng thông minh như ngựa nhồi bông Leonardo de Vanci, heo bông Pibu.... Những con thú này rất có ý thức bảo vệ môi trường luôn xanh-sạch-đẹp bằng những hành động vô cùng thiết thực như sử dụng pin năng lượng mặt trời, xây dựng nhà cửa bằng vật liệu thân thiện với môi trường và thậm chí là phạt Jaian và Suneo vì hành động làm ô nhiễm môi trường.
 Tuy chỉ là 1 tập truyện nhỏ chủ yếu dành cho thiếu nhi nhưng tác giả đã rất tinh tế khi khéo léo lồng ghép những thông điệp bảo vệ môi trường, kêu gọi ý thức của con người trong việc giữ gìn môi trường xung quanh mình.
5
10908
2014-08-28 21:05:51
--------------------------
