402683
5927
Cuốn 180 Đề Tài Nói Tiếng Anh có rất nhiều đa dạng về chủ đề để luyện nói. Tôi thấy rất hay. Mỗi chủ đề tác giả cho một bài nói bằng tiếng anh và sau đó là các câu hỏi để chúng ta có thể xây dựng khung nói cho bản thân mình. Tôi thấy cuốn này rất phù hợp với các bạn học tiếng anh trình độ từ sơ trung cấp trở lên. Khi đọc xong bài khóa và trả lời các câu hỏi thì từ đó mình cũng có thể nghĩ ra nhiều câu nói liên quan khác, đó gọi là sự gởi mở. Đề tài nói thì dồi dào phong phú. Hay !
4
854660
2016-03-22 19:12:49
--------------------------
172539
5927
*Mặc dù sách xuất bản năm 2011 (hơi xưa) và thiết kế bìa khá đơn giản nhưng nội dung của sách rất rõ ràng: 3 cấp độ A, B, C (60 chủ đề/ cấp độ). Vì thế, các bạn có thể luyện tập dần dần để nâng cao mà không cảm thấy quá tải.
1)	Luyện đọc được là vì dưới các bài văn là những câu hỏi liên quan đến nội dung bài để bạn khai thác và ghi nhớ bài văn đó.
2)	Luyện nói: giúp bạn có thêm kiến thức về các đề tài xung quanh cuộc sống mà áp dụng (có cả một số chủ đề về hội thoại)
3)	Luyện viết: vì trình bày theo bài văn nên dễ để bạn biết bố cục, câu cú, cách dùng ngữ pháp và từ vựng như thế nào cho hợp lý 
*Với giá như vầy thì thật sự đây là cuốn sách rất hữu ích cho mọi đối tượng độc giả muốn nâng cao trình độ tiếng Anh của mình.
5
45667
2015-03-24 10:56:42
--------------------------
