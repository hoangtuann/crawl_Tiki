426467
6372
Nhờ có tiki mà mình đã sưu tầm được bộ truyện cổ Đông Nam Á, cuốn Truyện Cổ Mianma này mỏng và ít nội dung hơn so với các cuốn khác nhưng nhìn chung những mẩu truyện mà Tác giả PGS. TS Ngô Văn Doanh biên soạn ở chủ đề nước Mianma vẫn giữ được đặc sắc riêng, chứ không bị trộn lẫn. Sách hay dành cho ai muốn tìm hiểu về truyện cổ, một nét văn hóa đặc trưng của từng nước và kể truyện cho con nghe. Sách có bìa gập nhưng giấy ố vàng không ổn lắm. Tiki bán sách rẻ ^^
4
1295734
2016-05-06 23:01:28
--------------------------
