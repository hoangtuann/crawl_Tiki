475787
3702
Lúc nhận được hàng, tôi rất hài lòng về chất lượng giấy và bookcare bên ngoài của sách, phải nói tiki giao hàng và bảo quản sách rất tốt.
Khi đọc quyển truyện này, lúc đầu tôi cũng sợ truyện không diễn tả được tinh thần của nguyên bản như hamlet, thế nhưng càng đọc tôi càng bị cuốn hút vào tính cách, số phận cậu bé olivơ tuýt đáng thương qua cách diễn tả tranh truyện độc đáo. Phải nói là, dịch giả đã nắm được cái hồn của tác phầm và diễn tả nó một cách hài hước, đầy lạc quan và trong sáng như chính trái tim nhân vật olivo vậy. 
Càng đọc tôi càng cảm thấy thương cảm cho olivo. Cuộc đời Olivo là một chuỗi bất hạnh, mẹ mất khi sinh ra em, sống trong trại tế bần với những con người vô nhân tính, điển hình là Coocnay bớt xén từng hạt gạo nấu cháo cho lũ trẻ mồ côi vì lòng ích kỉ. Sau đó, Olivo lại còn bị lừa làm một tay cướp giật, bị vu oan giá họa vì điều xấu mà em không làm, bị người cậu cướp đoạt lấy tài sản lẽ ra thuộc về em...Hàng loạt biến cố ập đến đôi vai của cậu bé đáng thương, có lúc tưởng như em đã bị xã hội thối nát ấy dồn đến đường cùng! Thật may là "ở hiền gặp lành", cậu bé đáng yêu với đôi mắt to trong sáng ấy đã tìm thấy mái ấm cho riêng mình, được hưởng hạnh phúc em xứng đáng được có.
Câu chuyện tuyệt vời, cách dẫn dắt bằng ngôn ngữ tranh cũng đáng hoan nghênh, thế nên tôi cho sách này 5 sao.
5
48150
2016-07-16 13:56:48
--------------------------
460657
3702
Con gái mình đã hiểu được cuộc phiêu lưu và tuổi thơ đầy nước mắt của Oliver Twist sau khi đọc xong cuốn truyện này. Dù bị đối xử tàn nhẫn nhưng Oliver Twist vẫn gìn giữ được sự lương thiện và đặc biệt là ở cậu bé dũng cảm vẫn cháy bỏng khát khao hạnh phúc.
Oliver Twist là tiểu thuyết nổi tiếng của Charles Dickens. Và việc chuyển tải tiểu thuyết dưới dạng truyện tranh, Oliver Twist đã có một diện mạo hoàn toàn mới, thực sự thu hút lứa tuổi thiếu nhi. Tôi sẽ mua các cuốn truyện tương tự trên Tiki.vn cho con gái đọc.
4
1424738
2016-06-27 10:37:03
--------------------------
459233
3702
Danh tác thế giới – Olive Twist (phiên bản truyện tranh) mang phong cách hài hước, đem lại cho mình nhiều rất nhiều cảm xúc: cảm động vỡ òa trong hạnh phúc  (bí mật được tiết lộ) – hồi hộp (khi Olive bị bắt, Nancy bị theo dõi) – xúc động nghẹn ngào (khi biết Olive là cháu của dì Rodo)
Phiên bản tuy truyện tranh ngắn gọn nhưng vẫn truyền tải được trọn vẹn nội dung của phiên bản dài
Ưu điểm: ngắn gọn, gây cười (chỉ cần đọc phiên tên nhân vật thôi cũng đã thấy buồn cười rồi)
Nhược điểm: phiên âm tiếng Việt gây khó khăn trong tra cứu
4
301718
2016-06-25 22:03:43
--------------------------
260479
3702
Đọc truyện Olivo tuýt ta mới thấy được những mặt khác ở xã hội giữa sự giàu có nhưng độc ác và nghèo khó với lương thiện. Bộ danh nhân thế giới đã kết hợp vô cùng hay giữa truyện chữ và tranh giúp cho người đọc dễ dàng tiếp nhận được câu truyện. Hình ảnh được sắp xếp hợp lí có những tình tiết gây cười giúp cả trẻ con và người lớn đều có thể đọc. Truyện lại bìa đẹp bắt mắt người đọc và quan trọng là giá cả hợp lí phù hợp với tất cả mọi người  
5
59512
2015-08-10 14:22:17
--------------------------
206562
3702
Danh tác thế giới có rất nhiều và thường rất dài, nhưng nhờ có cuốn sách này mà mình không mất nhiều thời gian để nắm bắt khá đầy đủ về nội dung, giá trị của tác phẩm. Nhờ lối dẫn chuyện sinh động, các hình ảnh minh hoạ trong sáng, dễ hiểu và có đan xen các tình huống hài hước, sách diễn tả nhẹ nhàng cuộc chiến giữa thiện và ác, đồng thời đề cao giá trị con người, khát khao hạnh phúc lương thiện cháy bỏng trong họ. Sách có sự sắp xếp hợp lí các hình ảnh, nhưng cũng cần hạn chế các tình huống gây cười không đáng có, làm mất mạch cảm xúc, tính cách của mỗi nhân vật của phiên bản gốc.
4
561261
2015-06-10 08:33:35
--------------------------
