461426
7124
Nói đến văn học Việt Nam, cụ thể là văn học trước Cách Mạng, mỗi độc giả ta đều phải kể đến nhà văn Nguyễn Công Hoan, một nhà văn tiêu biểu của chủ nghĩa hiện thực phê phán, bậc thầy của truyện ngắn châm biếm. Cuốn sách này tóm lược rất nhiều ý kiến, nhiều lời nhận xét phong phú, chân thực mà sắc sảo, tinh tế của các nhà văn, nhà thơ, nhà phê bình nổi tiếng, phù hợp và cần thiết cho những độc giả có hứng thú và đam mê dành cho bộ môn Văn học. 
3
1239263
2016-06-27 20:10:12
--------------------------
139484
7124
Nguyễn Công Hoan là nhà văn tiêu biểu của chủ nghĩa hiện thực phê phán. Cuốn sách này có 2 chương, ngòi bút của nhà văn lực lưỡng, dũng khí, lạ lùng. Ông là người đã đặt những viên gạch đầu tiên đắp nền móng cho nền văn xuôi hiện thực phê phán. Đọc Bước đường cùng của ông sẽ giúp cho thế hệ sau cách mạng hiểu được những cảnh lố lăng, ngang trái của xã hội cũ và còn nhiều  tập tư liệu có giá trị khác nữa. Đây là tài liệu tham khảo hữu ích đối với những đọc giả quan tâm, yêu mến và trân trọng tài năng của Nguyễn Công Hoan.
4
476952
2014-12-07 11:03:57
--------------------------
