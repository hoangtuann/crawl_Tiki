234601
3662
Đọc cuốn sách này như đang đọc một tập truyện tranh đầy thú vị. Các hình vẽ sinh động, màu sắc hấp dẫn và thu hút đem đến cho các bạn nhỏ những kiến thức bổ ích về vật lý. Đó là những bài học cơ bản đầu tiên về lực, ánh sáng, âm thanh, những ứng dụng vật lý trong cuộc sống hàng ngày và cả những trò "ảo thuật" vật lý hấp dẫn. Tất cả những kiến thức ấy được trình bày thông qua những tình huống hài hước của các nhân vật vui nhộn trong tập sách. Cuốn sách này đã mê hoặc tất cả các cậu trai bé nhỏ của khu phố nhà mình đấy!
5
687089
2015-07-20 14:36:52
--------------------------
