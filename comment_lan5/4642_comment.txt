478066
4642
Trong quyển sách này có rất nhiều kỹ năng mà các bạn trẻ đều cần biết, đặc biệt là các bạn nữ. Tuy nhiên mình thấy có nhiều kỹ năng mà các bạn có thể đã được dạy từ rất bé, hoặc có thể tự rút lấy cho mình, vd như xỏ kim, vặn vít hay đập trứng. Với những bạn nữ khá vụng về thì sẽ tìm thấy được nhiều điều hay ho ở trong cuốn sách này đấy. Các bạn hãy đọc để trở thành nữ công gia chánh chính hiệu nhé.  Giấy in và bìa rất tuyệt
4
618204
2016-07-30 17:21:20
--------------------------
464551
4642
Mình mua tương đối nhiều sách trên Tiki, lúc mua thi mình cũng đã lên kênh vtc9 xem họ nhân xét về cuốn này và cuối cùng mình quyết định đặt mua, mặc dù mẹ mình cũng khuyên là cuốn này không cần thiết, và thực sự nó quá phí tiền chứ không phải vừa. Mấy trò dạy trong đấy thấy rất bình thường, và quá cơ bản, ví dụ như cách dạy luộc trứng lòng đào, luộc theo đúng thời gian như trong sách hướng dẫn thì trứng chín chuyển thành màu đen lâu rồi, hay như cách bọc quà sao cho khéo, mình thấy  không những còn không khéo mà còn rất cổ và cũ ý. Nói chung là mình không có ý dìm, nói xấu đâu nhưng thực sự mình không thấy bổ ích. 
1
394626
2016-06-30 11:10:08
--------------------------
442737
4642
Thật sự là trước lúc mua cuốn sách này, mình cũng muốn trở thành một "bạn giá khéo tay" nên mới quyết định đặt mua. Tuy nhiên, mua về và đọc nó thì cảm thấy thật sự thất vọng. Một cuốn sách giá bìa 64k mà tương đối mỏng, có lẽ là do màu. Nhưng đó không phải là tất cả mà là trong cuốn sách này chỉ dẫn con gái những điều quá là hiển nhiên trong đời sống mà lẽ ra ai cũng phải biết, cứ như chỉ dẫn một đứa con nít mới lớn ấy! Lần sau có lẽ mình mua gì cũng phải nên cân nhắc kĩ lưỡng. Điều mình hài lòng là Tiki giao hàng rất nhanh.
3
1079381
2016-06-05 15:48:02
--------------------------
427589
4642
Sách này thi mình được tặng dịp sinh nhật... Mình là nam...
Vâng! Đúng là nam nhưng mà thấy những kĩ năng trong sách thì ai cũng cần. Mình cũng đã áp dụng mấy cái kĩ năng trong sách vào đời sống và thấy rất bổ ích. 79 kĩ năng trong sách được sắp xếp theo mục và rất khoa học. Có những phần ghi chú và chú ý nữa...
Thế mà lại đi mắng thằng bạn tặng mình cuốn sách nữa chớ.
Thì nói chung rất bổ ích. "Không chỉ bạn gái cần khéo tay..." Không có gì khó cả, chỉ cần chú ý thôi.
5
1331176
2016-05-09 18:05:10
--------------------------
427197
4642
Cuốn sách này rất bổ ích , chúng chỉ cho ta những cách sơ cứu , cách làm bánh , cách may vá và cả ....xỏ chỉ nữa :)) nhưng mình thấy cuốn này không hợp với mình cho lắm vì những điều này mình biết làm hết rồi , chỉ phù hợp với những em nhỏ từ 8-10 tuổi thôi. Lúc đó thì mới bắt đầu làm việc nhà mà phải không? Mình nghĩ sách nén đưa thêm vào những cách sơ cứu khi bị... Nhiều hơn để mình còn biết cách mà chữa trị nữa. Tiki giao hàng rất nhanh
4
1043353
2016-05-08 16:44:27
--------------------------
423963
4642
cuốn sách rất hay và là sẽ là cuốn sách cho những ai đang muốn trở thành một người con gái đảm đang tuyệt vời.Cuốn sách được chia làm nhiều mục với những siêu kĩ năng thích hợp cho từng mục. Bạn sẽ dễ dàng tìm thấy những kĩ năng rất nhỏ như xỏ kim đến những kĩ năng xử lí việc nhà hay kĩ năng của các đầu bếp , còn có những kĩ năng rất con gái như xịt nước hoa hay nặn mụn nữa.Nói chung là nó rất cần thiết đối với các bạn gái.Cảm ơn tiki !


5
1078430
2016-04-30 13:34:21
--------------------------
423724
4642
Mình không mua sách ở tiki nên mình muốn nhận xét về nội dung sách chứ không phải chất lượng hay tiến độ giao hàng. Sách có chỉ dẫn rõ từng bước làm việc, đưa ra phương pháp làm nhanh gọn mà không phải ai cũng biết. Mình cứ nghĩ là mình đã biết làm nhiều việc rồi nhưng đến lúc đọc sách này, mình mới biết cách viết thư như thế nào (một kỹ năng rất quan trọng nhưng không còn phổ biến nên ít ai được biết), cách thắt cà vạt hay cách thắt dây giày (trước khi đọc sách mình cứ để nguyên dây giày rồi chà vì mình sợ tháo ra rồi không thắt lại được). Sách rất hữu ích, mình rất thích 
5
1193702
2016-04-29 21:08:06
--------------------------
418207
4642
Ban đầu khi đọc tựa đề cuốn sách này mình rất thích vì nghĩ rằng sẽ bố sung cho mình thêm nhiều mẹo vặt trong gia đình. Với bố cục và cách giới thiệu các mục nội dung rồi ràng logic, mình trông đợi học hỏi nhiều điều thú vị hơn từ sách.  Thế nhưng nội dung hầu hết các chương đều là những kiến thức quá cơ bản, cách thức quá đỗi bình thường.  Bạn chỉ cần quân sát hay nghe người lớn làm thử 1 lần là biết ngay không hơi đâu mà phải ngồi đọc các hướng dẫn dài dòng để có thể đập 1 qua trứng,  xỏ kim... 
Thật ra cuốn sách sẽ hữu ích nếu các bé gái là các tiểu thư ít khi được quân sát học hỏi từ thế giới trực quan, cái gì cũng phải mở sách,  đọc cách hướng dẫn một cách lý thuyết.  Có lẽ sách xuất bản đã lâu chưa kịp cập nhật tổng hợp những mẹo vặt hay và thiết thực.
1
1296770
2016-04-18 20:31:13
--------------------------
404398
4642
Ban đầu khi mới nhận được cuốn sách tôi đã nghĩ rằng: có lẽ cuốn sách quá trẻ con với mình rồi, chỉ dành cho các bé gái thôi vì có những kĩ năng hết sức nhỏ nhặt như cách phơi chăn màn, quần áo, sơ cứu khi bị bỏng hay lau bụi, gập quần áo. Tuy nhiên, đọc kĩ thì vẫn có những kĩ năng mà người lớn có khi còn qua loa và không biết tới: thoát khỏi đám cháy, xử lý khi bị chảy máu mũi,.... Tự hoàn thiện bản thân mình không chỉ là việc cần làm đối với trẻ nhỏ mà là quá trình miệt mài cả đời của tất cả phụ nữ chúng ta!
5
905408
2016-03-25 08:25:15
--------------------------
368887
4642
Quyển này nội dung có đôi phần giống với quyển " Bạn trai tháo vác", nhưng hình ảnh minh họa rất tỉ mĩ, dễ hiểu, lại có phần" nữ tính" hơn. Quyển sách này rất bổ ích, tuy có vài kỉ năng mình cảm thấy hơi thừa ( như xỏ kim chẳng hạn,...), cũng có thể là mình đã làm được những điều đó từ nhỏ nên thấy dễ dàng, nhưng đối với vài bạn thì rất cần thiết. Qua sách, mình đã có thêm rất nhiều kiến thức bổ ích. Đây là quyển sách mà mỉnh nghĩ rằng " Đã là con gái thì nhất thiết phải đọc qua" và con gái ngày nay ít nhất là phải có được những kĩ năng như trong quyển sách này.
5
647133
2016-01-14 20:06:19
--------------------------
366826
4642
Nè thì tại sao tiki à quá là kì tại sao mà tạo ra một sản phẩm tốtttt vậy nèkk tiki ơi sau này nhớ ra nhiều sách nữa nhaa
5
1033320
2016-01-10 20:23:24
--------------------------
349321
4642
Mình được anh bạn giới thiệu cuốn sách "Bạn gài khéo tay" nên mua tặng con gái.
Mình đã lên TIki đọc thử và đặt mua ngay. Quả là một cuốn sách rất bổ ích dành cho các bạn gái tuổi mới lớn. Một số quyết làm đẹp nho nhỏ, vài kỹ năng để vào bếp làm bánh, nấu một vài món ăn đơn giản đãi cả nhà...và cả cách xử lý vết bỏng, kỹ năng thoát hiểm khi gặp đám cháy.. Những kỹ năng này đôi khi một người làm mẹ như mình cũng bỏ quên...
Cuốn sách được in hình màu cùng ảnh minh họa cũng bằng màu nên không gây sự nhàm chán khi đọc.
Con gái mình đã đọc hết quyển sách một cách hào hứng và còn nói "Sách rất hay mẹ à, con sẽ cho các bạn trong lớp mượn đọc nữa"
Đây đúng là một cuốn sách gối đầu giường dành cho tuổi mới lớn!


5
939764
2015-12-08 09:22:46
--------------------------
343685
4642
Tôi mua quyển sách "Bạn gái khéo tay" này cùng với quyển "Bạn trai tháo vát". Tôi là con gái nhưng vẫn mua cả hai cuốn vì tôi nghĩ con gái cũng nên trang bị nhưng kiến thức của con trai để khi cần dùng trong cuộc sống hàng ngày. Và tôi đã không sai khi chọn mua cả hai cuốn đó. Nội dung sách rất hữu ích, đều là nhưng kĩ năng hằng ngày. Hướng dẫn dể hiểu, rõ ràng ... Nhưng mình vẫn còn thấy bìa sách hơi đơn giản, tranh minh họa trong sách mình nghĩ nên in màu và vẽ chi tiết từng bước một rõ ràng hơn nữa. Nhưng nhìn chung thì đây là một cuốn sách kỹ năng sống nên có trong tủ sách teen của nhà mình !!!
5
763023
2015-11-26 22:47:47
--------------------------
343587
4642
Sau khi đọc các nhận xét trên tiki, mình quyết định mua quyển sách này và mình đã không thất vọng. Quyển sách hướng dẫn các mẹo vặt tưởng chừng như rất đơn giản nhưng đó lại là những kĩ năng sống rất cần thiết trong cuộc sống hiện đại của chúng ta. Sách gồm 79 kĩ năng, thích hợp với các em gái tuổi từ tuổi thiếu niên nhi đồng đến tuổi mới lớn, để các em tự tin hơn khi được trang bị những kĩ năng cần thiết hằng ngày. Không xa xôi, vĩ đại, sách rất gần gũi và bổ ích.
4
947030
2015-11-26 20:20:59
--------------------------
340244
4642
Mình rất thích quyển sách này! Những kĩ năng mà quyển sách đề xập đến dều rất cần thiệt cho các bạn gái chúng mình. Nhờ có quyển sách này, từ một cô nàng vụng về, mình đã tgay đổi được hình tượng trong mắt mọi người: gọn gàng hơn, ngăn nắp hơn, khéo léo hơn,... Mình sẽ giới thiệu quyển sách này cho các bạn gái đồng trang lứa để các bạn cũng sẽ có những kĩ năng cần thiết cho cuộc sống trườn thành mai sau. Mình mong tác giả quyển sách này sẽ cho ra thêm nhiều quyển sách bổ ích hơn nữa và được nhiều bạn đọc đón nhận❤
5
941465
2015-11-19 15:32:05
--------------------------
296890
4642
Mình biết đến cuốn sách thông qua giới thiệu trên báo Hoa Học Trò, thấy thú vị nên quyết định đặt mua, chờ đợi mãi cuối cùng cũng được sở hữu em nó. Sách đẹp, bìa ấn tượng, sách được đóng cẩn thận, nội dung bên trong thì cực hữu ích. Sách cung cấp cho các bạn gái những mẹo vặt trong cuộc sống hằng ngày, từ việc luộc mỳ, làm bánh đến lau dọn nhà cửa, những kỹ năng thoát khỏi đám cháy, kỹ năng xử lý vết bỏng, chuột rút, còn cả bí quyết lặn mụn đúng cách nữa chứ. Quả là một cuốn sách rất cần thiết cho tụi con gái chúng minh.
5
474900
2015-09-11 13:10:33
--------------------------
289313
4642
Được giới thiệu cuốn sách này qua báo Hoa học trò lâu rồi nhưng giờ tôi mới thật sự sở hữu nó. Nó đúng là 1 cuốn cẩm nang bổ ích với biết bao mẹo vặt cần thiết cho cuộc sống hằng ngày của phái nữ. Sách rất hữu ích để các bạn gái trao dồi các kĩ năng cơ bản trong cuộc sống để sau này có thể ra đời và làm chủ được cuộc đời mình. Cuốn sách chính là nơi tích luỹ những kĩ năng nhỏ thôi nhưng rất cần cho bạn gái. Tôi rất thích cuốn sách này, nó như 1 "bí kíp" nhỏ mà tôi có thể làm hành trang mai sau. Cảm ơn tiki và tác giả đã đem đến cho phái nữ cuốn sách này.
5
774087
2015-09-04 13:01:31
--------------------------
275654
4642
Trong một thời đại hiện đại hóa và phụ nữ cũng tham gia nhiều vào các hoạt động xã hội nhiều như hiện nay thì cuốn sách này vẫn là một cẩm nang hữu ích giúp các bạn gái hoàn thiện các kĩ năng cơ bản cần thiết cho cuộc sống hàng ngày để sau này dễ dàng làm chủ cuộc đời mình. Sách có chất lượng khá tốt, trình bày và minh họa đẹp nên mình rất thích khi có thể trang bị cho mình những kiến thức tuy nhỏ mà hữu ích cho bản thân trong cuộc sống hàng ngày. Một cuốn sách đáng mua!!! Cảm ơn tác giả và Tiki đã đem đến những cuốn sách hay cho người đọc!
4
186614
2015-08-23 06:04:55
--------------------------
274500
4642
Cuốn sách gồm rất nhiều những mẹo ứng biến cho các bạn gái trong cuộc sống hằng ngày mà có lẽ sẽ có nhiều bạn chư biết. Những tips hay ho trong nữ công gia chánh, cách phòng thân khi ra đường, cách ứng phó khi gặp sự cố bất ngờ,...những điều mà mọi cô gái đều cần phải biết :) Sách có thể làm một cẩm nang hoàn hảo - người bạn thân thiết của bất cứ cô gái nào :)) Bên cạnh đó còn có những công thức nấu ăn ngon lành chiêu đãi cả nhà. Sách minh họa đẹp, giấy tốt, in ấn khá chất lượng, mình hài lòng!
5
247818
2015-08-21 23:06:03
--------------------------
255282
4642
Mình rất yêu thích cuốn sách này! Cuốn sách đã mang lại nhiều điều bổ ích và lí thú cho cuộc sống trước mắt của mình để đề phòng trong những trường hợp bất trắc hay những trường hợp khó xử lí. Sách đã hướng dẫn rất cụ thể và chi tiết những điều mà mình nên làm để có thể trở thành một cô gái hiểu biết và duyên dáng! Một cuốn sách cần thiết cho các bạn gái mới lớn để có thể tự tin vững bước trên đường đời sau này! Xin chân thành cảm ơn Tiki!
4
350240
2015-08-05 21:45:40
--------------------------
254017
4642
Đây là cuốn sách tôi chọn để tặng em gái vào sinh nhật. Tôi thấy mỗi bạn gái nên trang bị cho mình cuốn sách này như là một cuốn cẩm nang để giúp hoàn thiện bản thân mình. Cuốn sách đã chia sẻ những kĩ năng cần thiết, luôn xuất hiện trong cuộc sống của chúng ta. Cuốn sách còn cung cấp những cảnh báo, những mẹo giúp ta biết nên hay không nên làm gì cho phù hợp. Trong cuộc sống hiện đại như bây giờ thì sự tháo vát, khéo léo trong mọi việc luôn là yêu cầu tối ưu nhất.
4
606112
2015-08-04 22:34:55
--------------------------
249031
4642
Sách có nhiều hình ảnh minh họa nên đọc cũng bắt mắt, ngoài hình thức đẹp thì chất lượng giấy in cũng rất tốt. Mình mua sách này cho con gái mình vì thấy các hướng dẫn ở đây cũng hữu ích, các tình huống cũng thường có trong thực tiễn. Những gợi ý xử lý có thể là quen , có thể là lạ với mỗi người, nhưng điều quan trọng hơn tất cả là mọi thứ đều đã được hệ thống trong cẩm nang này, để mình có thể hướng dẫn lại cho cháu, giúp cháu tự tin và khéo léo hơn mọi tình huống của cuộc sống thường ngày.
5
544151
2015-07-31 12:10:40
--------------------------
248863
4642
Quyển sách cung cấp những kĩ năng cơ bản mà các bạn gái cần có.Mình vốn là người phụ nữ không lấy gì khéo tay cho lắm, nên mình xem quyển sách như cẩm nang sống cho bản thân. Đây là quyển sách hội tụ những kĩ năng bổ ích trong cuộc sống như kĩ năng sơ cứu , kĩ năng dùng trong cuộc sống thường nhật, ... đến các kĩ năng rất con gái  như chống rối mái tóc , cách xịt nước hoa.....Tất cả các kĩ năng đều rất hay và hữu ích . Mình sẽ mua thêm 1 quyển để tặng em gái .
5
512701
2015-07-31 10:24:35
--------------------------
248545
4642
Nếu là con gái thì mình nghĩ mối bạn nên có một cuốn. Từ ngày có cuốn sách, mình đã thay đổi được bản thân, trở nên khéo léo hơn, đõ vụng về hon trước. Sách cho ta những kĩ năng cơ bản nhất, thiết yếu trong cuộc sống. Những kỹ năng được vẽ kỹ, cách làm khá hiệu quả, ngôn ngữ cũng hài hước. Có cuốn kim chỉ nan này trong tay thì mình tin là ta sẽ được học thêm nhiều kĩ năng trong cuộc sống thường nhật. Chất lượng giấy cũng đảm bảo. Mình đã mua một cuốn cho mình và nhỏ em, dù nó chỉ 10 tuổi nhưng cũng rất thích và luôn học theo những gì sách viết. Nhìn nó vậy, bố mẹ và mình cũng yên tâm
5
139917
2015-07-31 10:04:10
--------------------------
248308
4642
Sách khá hay và bổ ích, có nhiều kỹ năng cần thiết mà các bạn nữ phải có. Tuy nhiên, mình hơi thất vọng vì sách không đề cập đến những kỹ năng về vệ sinh cá nhân. Bìa sách đẹp và khá dày, giấy màu trắng sáng, chữ rõ đẹp. Hình ảnh minh họa thân thiện, dễ hiểu. Qua quyển sách này, mình đã học được khá nhiều kỹ năng như làm mì spaghetti mà trước đây mình chưa biết. Mình khuyên các bạn gái mới lớn như mình nên có một quyển sách như vậy để cho vào tủ sách.
4
602995
2015-07-31 09:46:29
--------------------------
239929
4642
Sách rất hay và hữu ích. Có nhiều hình minh họa đẹp, dễ hiểu. Tuy có một số mẹo ai cũng biết nhưng cũng có 1 số mẹo rất hay mà các bạn nữ nên biết. Mình lúc trước khi có cuốn sách này khá vụng về nhưng nhờ nó, bây giờ mình đã đỡ vụng về hơn :'>. Các phụ huynh nên mua cho con mình càng sớm càng tốt để các em nhỏ được rèn luyện những kỹ năng sớm và lớn lên sẽ khéo tay hơn! Những kỹ năng này rất bổ ích đấy. . . .
5
662221
2015-07-24 10:18:50
--------------------------
234144
4642
Sách rất hay, có nhiều mẹo hay cực.Sách có thiết kế đẹp, có điểm nhấn giúp người đọc dễ đọc, dễ hiểu và dễ nhớ. Nhược điểm: Đầu và cuối sách có vài trang bị trống, mình không biết là do tác giả cố ý hay do nhà in nhưng cá nhân mình thì không thích sách có trang trống như vậy.Theo mình thì các bậc phụ huynh nên cho con mình đọc cuốn sách này càng sớm càng tốt! Giấy rất tốt, các trang sách đều có hình ảnh minh họa dễ hiểu, bên cạnh đó còn có những lưu ý và mách nhỏ đều các bé có thể thực hiện kĩ năng tốt hơn! Có một số kĩ năng đơn giản bé có thể tự thực hiện một cách dễ dàng, nhưng cũng có một số kĩ năng cần sự giúp đỡ của người lớn và bạn bè! Càng lớn thì sẽ càng cảm thấy một số kĩ năng trong sách rất đơn giản, ko cần đọc cũng biết làm! Nên lời khuyên của mình ở đây là nên cho các bé tuổi còn nhỏ đọc quyển sách này! Vì theo thời gian các bé sẽ rèn luyện các kĩ năng một cách thành thạo và trở thành một cô gái khéo tay
5
711237
2015-07-20 09:44:07
--------------------------
230618
4642
Nhược điểm:
- có phần lê thê dài dòng
- hơi bị mắc chút đỉnh
- đa số các kỹ năng không có gì mới lạ, có vài kỹ năng nhiều công đoạn trong khi có nhiều mẹo nhỏ lại hay hơn và ngắn gọn hơn, hiệu quả hơn
- 1 số chỉ dẫn của kỹ năng chưa đúng lắm (như khi say xe thì đi xe hơi phải ngồi cạnh tài xế mà mình thử rồi có thấy đỡ hơn đâu)
Ưu điểm:
- dùng từ ngữ có tính teen & hài hước
- hình minh họa dễ hiểu và rõ ràng, chất lượng sách tốt
- hướng dẫn, giải thích của 1 số kỹ năng dễ hiểu và nắm bắt được theo hướng khoa học
3
534452
2015-07-17 16:20:22
--------------------------
219751
4642
Theo mình thì các bậc phụ huynh nên cho con mình đọc cuốn sách này càng sớm càng tốt! Giấy rất tốt, các trang sách đều có hình ảnh minh họa dễ hiểu, bên cạnh đó còn có những lưu ý và mách nhỏ đều các bé có thể thực hiện kĩ năng tốt hơn! Có một số kĩ năng đơn giản bé có thể tự thực hiện một cách dễ dàng, nhưng cũng có một số kĩ năng cần sự giúp đỡ của người lớn và bạn bè! Càng lớn thì sẽ càng cảm thấy một số kĩ năng trong sách rất đơn giản, ko cần đọc cũng biết làm! Nên lời khuyên của mình ở đây là nên cho các bé tuổi còn nhỏ đọc quyển sách này! Vì theo thời gian các bé sẽ rèn luyện các kĩ năng một cách thành thạo và trở thành một cô gái khéo tay!
5
594849
2015-07-01 21:48:37
--------------------------
219013
4642
Cuốn sách đề cập đến những vấn đề thường gặp nhất của các bạn gái, từ các kỹ năng sơ cứu, đến các kỹ năng trong sinh hoạt hàng ngày, ... đến các mẹo vặt mà các bạn gái chắc sẽ thích mê. Các kỹ năng/mẹo này được trình bày rất dễ hiểu và thường có hình minh họa kèm theo giúp cuốn sách sinh động hơn.
Đây thực sự là cuốn sách mà các bạn gái nên đọc. Mình thấy cuốn sách này chẳng khác nào một cuốn cẩm nang sống cho các bạn gái cả.
Ưu điểm: kiến thức trong sách rất cần thiết, thực tế và thường gặp. Sách có thiết kế đẹp, có điểm nhấn giúp người đọc dễ đọc, dễ hiểu và dễ nhớ.
Nhược điểm: Đầu và cuối sách có vài trang bị trống, mình không biết là do tác giả cố ý hay do nhà in nhưng cá nhân mình thì không thích sách có trang trống như vậy.
5
96853
2015-07-01 07:28:10
--------------------------
211838
4642
Đây là một quyển sách rất cần thiết cho các bạn gái teen ngày nay. Sách có tất tần tật những kỹ năng bổ ích trong cuộc sống như những kĩ năng sơ cấp cứu, những kĩ năng dùng trong cuộc sống thường nhật hay những kỹ năng xử lí việc nhà ( sau khi đọc xong mình đã bắt tay vào thực hiện ngay ),hơn nữa người viết sách cũng rất sáng tạo khi minh họa bằng hình ảnh dễ hiểu. Có được những kĩ năng hữu ích này sẽ giúp các bạn gái tự chăm sóc bản thân mình và giúp đỡ mọi người xung quanh đấy.
4
467014
2015-06-21 09:42:47
--------------------------
206181
4642
Vừa thấy quyển sách là đã nhấn mua ngay vì tôi đang có tư tưởng sống tự lập 1 mình. Thiết kế bìa rất bắt mắt, giấy cứng, tốt, màu tiêu đề và hình minh họa là màu tím đúng màu yêu thích của tôi. Sách đề cập đầy đủ các kỹ năng thiết yếu của 1 người phụ nữ độc lập, vừa trông xinh đẹp, tự tin bên ngoài vừa giỏi việc nhà, bếp núc. Thích nhất ở phần Sơ Cứu và phần Siêu Kỹ Năng "Rất con gái", tác giả chỉ ra những mẹo hay giúp chúng ta khỏi cơn khủng hoảng, bĩnh tĩnh xử lý tình huống khi có chuyện không tốt xảy ra.
5
303045
2015-06-08 23:12:16
--------------------------
202191
4642
điểm mạnh: giấy rất trắng, ngôn ngữ gần gũi, mẹo vặt thiết thực, phân loại rõ ràng.
nhận xét khác: cá nhân mình thấy cuốn sách này phù hợp cho các bạn gái tuổi teen hơi là đứa cũng có tí tuổi như mình.Với mình thì khoảng hơn 50% nội dung không thật sự cần thiết ví dụ như cách đập trứng, cách bóc hành không cay, xỏ kim...tuy nhiên cũng có  nhiều thứ hay ho như kỹ năng an toàn trong một đám cháy...

thành ra thấy mấy cuốn mẹo vặt gia đình của các tác giả Việt Nam phù hợp hơn. quan điểm cá nhân, cảm nhận cá nhân ạ :). Mình nghĩ sẽ để dành cuốn sách này cho con gái.
3
128360
2015-05-28 23:19:56
--------------------------
186021
4642
Đó là một cuốn sách hay dành cho bạn gái ở cuốn sách đề cập đến những kĩ năng dù đó là việc nhỏ nhặt nhất như kĩ năng sơ cấp cứu đó là những cách sơ cứu sao cho đúng,hay là việc xử lí những vết dằn vết đứt tay,hay là cách đi sang đường sao cho đúng.Cách thắt cà vạt,xỏ buộc giày hay là cách xỏ kim,đó là những công việc thường nhật nhất.Một cuốn sách hội tụ những điều thật ý nghĩa đối với tôi và bao bạn gái khác.Ngoài ra nó còn có các hình ảnh minh họa rất là sống động và thực tế...
5
536745
2015-04-20 13:32:34
--------------------------
179371
4642
Đã qua lâu rồi cái thời phụ nữ suốt ngày lo chuyện nội trợ, rất nhiều người đều hiểu và tạo điều kiện cho con gái phát triển bình đẳng, học tập và vui chơi, nhưng vì vậy mà vô tình tạo ra những cô gái "không biết một tí gì" giống như mình đây. Những kỹ năng nhỏ nhỏ nhưng lại rất ngại hỏi, sợ bị chê cười, sợ bị la mắng.. nhưng quyển sách đã cho mình một hướng giải quyết tuyệt vời. Quyển sách cung cấp đầy đủ những kỹ năng cơ bản mà con gái nên có để có thể hỗ trợ cho chính bản thân mình trong cuộc sống, với những lời hướng dẫn và hình ảnh minh họa rất cụ thể, chi tiết và dễ tiếp thu.
5
561797
2015-04-06 23:32:52
--------------------------
176785
4642
Gia đình tôi thuộc loại khá giả. Tôi là con út nên rất ít khi làm việc nhà. Có thể nhiều người sẽ nói rằng tôi sướng thế còn gì. Nhưng tôi lại cảm thấy nó không hay tí nào. Tôi cần biết cách sắp xếp thời gian, làm những việc mà những người cùng tuổi vẫn làm. Cha mẹ anh chị tôi cũng không có thời gian chỉ bảo tôi. Vì vậy mà lúc thấy cuốn sách này, tôi đã vui mừng. Vui chứ, tôi phải cố gắng để học thật tốt những kinh nghiệm trong cuốn sách này. Tôi thấy nó thực sự rất bổ ích cho lứa tuổi mới lớn như tôi. Nó giúp tôi có nhiều kinh nghiệm trong cuộc sống để giải quyết những tình huống bất ngờ, chẳng hạn như: cách sơ cứu vết thương, đứt tay, nấu một số món ăn đơn giản, cách chăm sóc cho bản thân.... 
Rồi tôi có thể sẽ giúp cho mẹ tôi được nhiều việc hơn, giảm bớt khó khăn cho gia đình. Chắc hẳn cha mẹ anh chị tôi sẽ phải ngạc nhiên thôi
5
599601
2015-04-01 20:58:53
--------------------------
175387
4642
Một quyển sách khá hay và bổ ích về các kinh nghiệm cuộc sống xung quanh dành cho các bạn gái. Những tình huống gần gũi, bất ngờ và hay xảy ra như: đứt tay, chảy máu mũi, ong chích,... hay những kinh nghiệm làm đẹp cho tóc, da, quần áo, giày dép,... ; đến những kỹ năng vào bếp nấu nướng và làm việc nhà,... đều được tác giả viết khá chi tiết là liệt kê đầy đủ các bước.
Nói chung để áp dụng được cuốn sách này hoàn chỉnh, bạn phải đọc đi đọc lại nhiều lần, luôn bình tĩnh và suy nghĩ kỹ càng để hành động, áp dụng cho mọi tình huống thật tốt và hợp lý. :)
4
382812
2015-03-30 06:56:42
--------------------------
173090
4642
Mình vốn là con một nên được ba mẹ rất cưng chiều. Thường thì mọi việc trong nhà ít khi phải làm lắm. Nhưng rồi đi học xa, gặp không ít khó khăn trong sinh hoạt hằng ngày. Sự vụn về khiến mình gặp không ít rất rối. 
Rồi một hôm đi nhà sách mình tình cờ gặp được "bạn ấy,  mở ra xem phần giới thiệu và cảm thấy rất thích thú, nghĩ ngay rằng nó viết ra là để giúp mình. Mình mua ngay và quả không sai, "bạn ấy" hệt như bí kiếp giúp mình vượt qua những khó khăn, vụn về trước đây. 
Sau một thời gian về nhà, mẹ rất ngạc nhiên về sự thay đổi của mình - "bạn gái khéo tay"
4
48819
2015-03-25 16:21:26
--------------------------
