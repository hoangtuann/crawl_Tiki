466941
5871
Cuốn sách nhỏ gọn như 1  cuốn sổ tay vậy. Mình cảm thấy nó rất hữu ích. Mỗi một bài lại có các mẫu câu, từ mới, bài khóa. Từ mới cũng không quá nhiều, chủ yếu là các từ thường dùng cần thiết, rất thích hợp cho những ai mới bắt đầu tìm hiểu tiếng Trung như mình. Không nhất thiết là dành cho người đi du lịch, những ai thích muốn tự học tiếng trung cũng dễ dàng và hữu dụng. Do có thiết kế nhỏ gọn nên dễ dàng mang theo, khi rảnh rỗi lại đem ra học. 很好
4
1186455
2016-07-03 06:19:37
--------------------------
453448
5871
Tiki giao hàng khá nhanh, sách nhận được có kích thước nhỏ gọn. Mình mua tặng bé em do nó đang muốn tự học tiếng Trung, em mình rất thích, nói sách hay và nhỏ gọn cũng tiện mang ra ngoài, rảnh rỗi thì lại lấy ra học. Nói chung cảm thấy khá hài lòng. Mình nghĩ không cần phải đi du lịch mới cần sử dụng mà có thể học để biết thêm do sách có rất nhiều chủ đề như nhà hàng,  khách sạn.  Cách sắp xếp các phần trong sách theo thứ tự: hội thoại, mẫu câu, từ vựng  cũng dễ theo dõi. Cám ơn Tiki nhe. 
5
266941
2016-06-21 11:15:42
--------------------------
440896
5871
Cuốn này nhỏ gọn như cuốn sổ tay vậy, cầm đi rất là tiện. Sách được dịch theo hai thứ tiếng nên cũng rất thích hợp cho các bạn muốn học theo kiểu song ngữ anh-hoa. Cuốn này dành chủ yếu cho các bạn mới học hoặc không biết tiếng trung vì theo mình thấy lượng từ cuốn sách này cung cấp cũng không nhiều. Thích hợp để học giao tiếp các tình huống hàng ngày.
CD nhỏ nhắn, dễ thương nhưng mình chưa mở ra nghe bao giờ nên cũng không biết chất lượng đọc ra sao.
Tuy nhiên, cũng rất hài lòng với số tiền mà mình bỏ ra.
4
463357
2016-06-02 14:18:21
--------------------------
434764
5871
tớ sẽ cho 5 sao ở phần biên tập sách và lượng kiến thức được trình bày có hệ thống tuy vẫn hy vọng sẽ học được nhiều hơn như vậy 1 chút, còn phần nghe thì, có thể nói đây là phần nghe chán nhất trong những cuốn sách tiếng trung có CD của tớ. phần cho nam đọc có một vài chỗ không đúng hoặc có thể tớ nghe không được rõ, phần nữ đọc cứng và không cảm xúc, tớ chỉ có thể cho 2sao. Hy vọng tiki ngày càng có thêm nhiều sách bỏ túi tiện ích cho mọi người!
3
725829
2016-05-23 21:05:59
--------------------------
422921
5871
Mình đang có nhu cầu tự học tiếng Trung cho nhiều mục đích khác nhau, chính vì vậy khi cầm quyển sách này trên tay hầu như mình có thể giở ra xem mỗi khi có thời gian rảnh rỗi, do thiết kế sách nhỏ, gọn, vừa tay, kèm theo CD đọc theo sách giúp mình phát âm chuẩn hơn, chỉ bất tiện là CD thì không phải lúc nào cũng có máy để nghe được, mà loại đĩa nhỏ dành riêng cho loại sách bỏ túi này thì chỉ có ổ đĩa của máy tính xách tay mới đọc được thôi.
5
992439
2016-04-28 13:13:41
--------------------------
415366
5871
Mình học chuyên ngành tiếng Trung, hiện lại dùng tiếng Trung đào tạo cho các bạn mới học. Nội dung chủ yếu về cuộc sống hằng ngày. Với cuốn sách này mình thấy phiên âm chính xác, ba ngôn ngữ Anh Việt Trung, phiên âm chính xác, nội dung tương đối đa dạng, mẫu câu dễ sử dụng, phù hợp với người mới bắt đầu. Nhưng để sử dụng cuốn sách này, bạn nên học trước cách đọc phiên âm. Cách sắp xếp các phần trong sách theo thứ tự: hội thoại, mẫu câu, từ vựng có lẽ một số bạn sẽ không quen. Mình nghĩ thường nên là từ vựng, mẫu câu, hội thoại.
4
137541
2016-04-13 11:39:55
--------------------------
411299
5871
Quê hương mình là thành phố du lịch, du khách Trung Quốc rất nhiều, các học sinh, sinh viên có nhu cầu học tiếng Hoa cũng nhiều. Lúc đầu mình không để ý kích cỡ giáo trình, khi mua hàng về mới thấy nhỏ xíu :)) Nhưng mà nội dung rất phong phú, nói là tự học tiếng Hoa cho người đi du lịch nhưng giáo trình này cũng bổ ích cho các bạn đang làm ngành du lịch. Từ đổi tiền, nhà hàng, khách sạn đều trình bày rõ ràng, song ngữ Hoa - Anh, các bạn ngoài học tiếng Hoa cũng có thể luyện cấu trúc ngữ pháp tiếng Anh. 
4
1091973
2016-04-05 20:29:16
--------------------------
271121
5871
Khi cầm cuốn sách trên tay tôi mới thấy dường như mình đang cầm một cuốn sổ tay vậy. Bởi nó có thiết kế nhỏ gọn, rất phù hợp để bỏ túi và mang đi nhiều nơi. Trong cuốn sách chứa đựng nhiều câu nói được chia theo từng chủ đề như: Đặt phòng, Gọi điện thoại, Đồ ăn nhanh,... giúp tôi dễ tra cứu. Mỗi câu, từ đều có nghĩa trong tiếng Việt, tiếng Trung và cả tiếng Anh, do đó tôi có thể học một lúc 2 ngôn ngữ Trung và Anh. Ngoài ra còn có CD giúp tôi phát âm chuẩn hơn. Thật là cuốn sách bổ ích. Ai định đi du lịch ở Trung Quốc thì nên mua quyển này.
5
603877
2015-08-18 21:11:44
--------------------------
