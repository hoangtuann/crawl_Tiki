468165
4817
Sách có chất lượng tốt, hơn cả mình dự đoán. Sách bao gồm các bài luận mẫu hỗ trợ cả thi IELTS general training và academic. Trình bày dễ hiểu, chia ra thành nhiều unit với nhiều topic, rất hữu ích và tiện dụng bởi có thể học từ vựng theo chủ đề cùng lúc. Cuối mỗi bài luận là phần key vocabulary với giải nghĩa tiếng việt và ví dụ đi kèm. Ngoài ra còn có các phần ghi chú thêm về cách diễn đạt hay, sao cho đạt điểm cao. Về phần task 1 thì có đầy đủ dạng biểu đồ, cách viết... Chất lượng giấy tốt là một điểm cộng nữa của sách.
5
106566
2016-07-04 20:36:40
--------------------------
268488
4817
Một cuốn sách khá hay, bạn nào còn yếu writing hoặc chưa tự tin có thể tìm tham khảo. Sách đưa ra rất nhiều tips, kinh nghiệm của các thầy, những câu hỏi từ các bài kiểm tra đã thi. Mình đã học được rất nhiều từ cuốn sách này. Nhiều bài test và cách viết esay các bạn đang luyện IELTS rất cần phải biết. Một cuốn sách các bạn nên có :). Mình mua cuốn sách đã lâu, nghiền ngẫm kỹ, hy vọng nhờ nó mình sẽ đạt điểm số cao writing trong kỳ thi sắp tới.
5
469258
2015-08-16 15:23:42
--------------------------
