481852
5540
Đối với hiến và Luật, chúng ta cần phải chú trọng và tìm hiểu để thực hiện đúng theo các Điều khoảng đã được ban hành. Là người Việt, chúng ta cần phải nắm rõ sử và luật, đó là hai điều căn bản để có thể trở thành một công dân tốt, có nghĩa vụ và trách nhiệm, cùng với các kiến thức cơ bản đối với bản thân.
Hiến pháp của nước CHXHCNVN đã được ban hành vào năm 1946, sau nhiều lần sửa đổi và bổ sung, bản hiến pháp này càng hoàn thiện hơn về tính chất ban hành thực hiện, áp dụng tích cực theo chiều hướng gia tăng vào cuộc sống, khá dễ dàng để nắm rõ nó
5
1106939
2016-08-31 06:41:48
--------------------------
381823
5540
Cuốn sách Hiến pháp nước CHXHCN Việt Nam này tổng hợp toàn bộ 5 bản hiến pháp của nước ta từ 1945 đến nay (1945, 1959, 1980, 1992 và 2013 )
Đặc biệt trong đó là bộ hiến pháp năm 2013, nó quy định những điều cơ bản về chính thể Nhà nước ta, quyền và nghĩa vụ của công dân trong giai đoạn hiện nay. Đây là những căn cứ xác thực nhất cho mỗi công dân dựa vào đó để bảo vệ những quyền lợi hợp pháp của mình. 
Về phần hình thức, cuốn sách có mặt bìa rất dễ nhìn, chất liệu giấy in tốt, chữ viết rõ ràng, dễ nhìn tuy phần keo dính bìa hơi nổi lên nhưng nó không phải là vấn đề lớn.
5
1081376
2016-02-18 09:35:06
--------------------------
311229
5540
Là người Việt phải biết sử Việt, với luật pháp cũng vậy, nếu không biết luật Việt thì làm sao có thể biết mình được hưởng những quyền lợi gì, nghĩa vụ ra sao, sống và làm việc thế nào để không phạm pháp... Hiến pháp là bộ khung sườn cho mọi luật pháp khác, vì vậy tìm hiểu hiến pháp sẽ giúp chúng ta hiểu được cốt lõi luật pháp của nước Việt Nam.

Sách tổng hợp hiến pháp từ năm 1946, qua nhiều lần bổ sung, sửa đổi có hiến pháp mới năm 2013. Nhìn chung hiến pháp từ những năm 1959 tới nay đều bám sát bộ khung từ hiến pháp năm 1946.
5
447930
2015-09-19 22:13:00
--------------------------
