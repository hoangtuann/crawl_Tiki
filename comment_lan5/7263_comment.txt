484539
7263
Đây thực sự là một cuốn sách rất hay và ý nghĩa. 
Nếu chỉ nhìn bìa sách, mình cảm thấy không ấn tượng nhiều. Nhưng khi mở sách ra thì lại thấy rất ấn tượng bởi giấy đẹp, có in hình chìm rất đẹp. Cách thiết kế này khiến mình thích thú và cảm thấy muốn nâng niu trân trọng cuốn sách này thật cẩn thận. 
Về nội dung, những câu nói dù đa số không có nguồn gốc là từ người nổi tiếng nhưng vẫn hay, ý nghĩa và triết lí cô cùng. Đọc xong lại dừng lại nhâm nhi suy ngẫm thật là tuyệt.
 Mỗi trang sách là một hoặc nhiều câu nói hay có cả tiếng anh và tiếng việt và được trình bày bằng kiểu chữ rất đẹp. 
Cuốn sách như một liều thuốc tinh thần làm cho người đọc thêm lạc quan vui vẻ tin yêu cuộc sống và biết suy nghĩ sâu sắc tích cực hơn về mọi câu chuyện đời thường.
4
1092520
2016-09-24 13:39:35
--------------------------
442344
7263
Với một thông điệp * Mỉm cười và bước tới * . Cụôc sống có đôi khi không như chúng ta mong đợi, chúng ta phải mỉm cười dù có bao nhiêu khó khăn và thử thách. Nụ cười sẽ giúp cho chúng ta vượt qua hay bất cứ điều gì mà ta phải trải qua. 
Cuốn sách với 2 thứ tiếng còn giúp cho chúng ta trao dồi ngôn ngữ Tiếng Anh nhiều hơn. Với thiết kế đẹp mắt, giấy đẹp cũng nói lên được tâm huyết của tác giả. Cuộc sống muôn màu hãy sống thật có ý nghiã..
5
929170
2016-06-04 21:30:04
--------------------------
425823
7263
Con người không phải lật đật, có vấp ngã, có đau thương mới có trưởng thành. Cách con người lớn lên là học gì từ sau thất bại, bạn định dành bao lâu để tưởng nhớ đến thất bại đó? Cuốn sách chính là thông điệp mà các tác giả muốn gởi gắm đến người đọc, hãy coi mỗi thất bại, mỗi vấp ngã là một bài học của cuộc đời, đừng quá đau buồn, hãy cứ mỉm cười và bước tiếp con đường bạn đã chọn, hãy cứ vững tin vào chính bản thân bạn. Đừng lo lắng, hãy tự tin. Cuốn sách được thiết kế song ngữ, rất tốt cho việc trau dồi vốn ngôn từ tiếng anh. 
5
816965
2016-05-05 15:43:24
--------------------------
342592
7263
Một quyển sách hay và ý nghĩa - theo tôi là vậy. Bởi vì khi đọc nó bạn hoàn toàn có thể trau dồi vốn anh ngữ của bản thân, giúp bạn yêu thêm môn tiếng anh vốn được xem là khó khăn đối với nhiều bạn trẻ, không những thế, mỗi câu thơ, câu nói trong cuốn sách đều là những đạo lí, triết lí sống, bài học làm người đáng trân trọng và noi theo. Ngoài ra, bố cục, cách trang trí, màu sắc của cuốn sách rất tuyệt, bắt mắt, hấp dẫn người đọc. Tôi rất thích cuốn sách này
4
726338
2015-11-24 19:46:14
--------------------------
280731
7263
Những vấp ngã trong cuộc sống là những điều không thể tránh khỏi. Nhưng liêu có bao nhiêu người có thể đứng dậy sau khi vấp ngã và mỉm cười chấp nhận để bước tiếp. Cuộc sống khó khăn và cuốn sách này chính là một công cụ để ta gỡ bớt những nút thắt trong cuộc sống với những lời khuyên, những mẫu chuyện hay và ý nghĩa, ta sẽ thấy cuộc sống này còn nhiều điều đẹp biết bao. Đây đúng là quyển sách đáng để đọc. Nó chứa đựng nhiều tầng y nghĩa nhân văn sâu sắc.
5
479150
2015-08-27 21:40:07
--------------------------
199860
7263
Vẫn với phong cách thiết kế vốn thu hút bạn đọc ngay từ vẻ bề ngoài và cách trình bày song ngữ Anh - Việt, tủ sách thông điệp yêu thương lại đưa đến bạn đọc một món ăn tinh thần thật có ý nghĩa!
Mỗi trang sách là mỗi bài học trong cuộc sống, những trải nghiệm được đúc kết một cách sâu sắc nhất! 
Chúng ta không có quyền bắt cuộc sống phải cho ta như thế này hay như thế nọ... nhưng chúng ta có quyền lựa chọn thái độ đối với nó...vậy tại sao bạn lại không chọn "Luôn mỉm cười và bước tới". Như vậy cuộc sống chẳng phải là đẹp hơn sao?!
5
558557
2015-05-23 13:25:14
--------------------------
