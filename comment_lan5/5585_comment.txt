464992
5585
Ấn tượng với cuốn sách vì chất lượng tốt (mình không ngờ là Tiki lại bao bìa kiếng cho sách), mực in đều màu và không bị lem, sách cầm vào thấy rất nhẹ chứ không giống như mấy bản in trước (đó là điểm mình thích nhất ở bản tái bản này). Còn về nội dung thì không có gì đề chê, mình thích Conan từ khi còn nhỏ, tác giả Aoyama rất tài tình khi có thể nghĩ ra những tình tiết hay và cách giải quyết vụ án tài tình đến thế.Trong tập này mình thích nhất là vụ về "Người vàng".
5
1158794
2016-06-30 18:38:49
--------------------------
461840
5585
Thám tử lừng danh Conan là truyện trinh thám, nên có những tập truyện nối tiếp nhau, và các nhân vật có liên quan đến nhau xen kẽ xuất hiện vậy nên không theo dõi từ đầu có khi cũng hơi khó cho các bạn nắm bắt. Tuy nhiên, đây là bộ truyện nổi tiếng, phổ biến có hơn 16 năm đồng hành cùng với tuổi thơ, sự trưởng thành của biết bao thế hệ bạn đọc nên chắc là những ai đã đọc cũng đọc từ khi bắt đầu truyện. Mình không thích đội thám tử nhí lớp 1B và những vụ án liên quan đến các nhóc vì thường nó sẽ nhỏ và đơn giản. Tập này có sự xuất hiện của một nhân vật bí ẩn, một anh chàng đẹp trai, tri thức, lịch lãm tên Okiya Subaru. Vì những cảm nhận của Haibara nên người xem cũng sẽ đặt câu hỏi về thân phận thật sự của Subaru, là bạn hay thù, có phải là người thuộc tổ chức áo đen không?
4
23188
2016-06-28 05:10:52
--------------------------
426146
5585
Mấy vụ án nhỏ lẽ không đang bận tâm. Đáng chú ý là xuất hiện một nhân vật bí ẩn, điển trai, trí thức, thanh lịch nhưng có thể là thành viên xã hội đen. Người này thông minh, giỏi thám tử như Conan vậy. Thế mới nguy hiểm. Sắp xếp hiện trường vụ án ở vụ cà phê rất độc đáo, các chi tiết của hiện trường ngày càng được tác giả bận tâm nhiều, hợp lý và giải thich, phân tích cũng cặn kẽ. Tập này chỉ được mỗi vụ án cà phê này thôi, các vụ khác hơi nhàm chán. 
4
535536
2016-05-06 11:26:09
--------------------------
422458
5585
Tập 60 của bộ truyện Thám tử lừng danh Conan vẫn là một tập hay, tuy nhiên không có những cao trào hay là những tình tiết quá mức giựt gân khiến người ta phải bị ám ảnh, phải bị ấn tượng hoài hoài. Lúc đọc thì vẫn hồi hộp thích thú nhưng đọc xong thì không nhớ lâu. Điểm đặc biệt của tập này là hung khí mà hung thủ sử dụng, có thể khiến cho người ta vô cùng bất ngờ. Vẫn rất nể biệt tài viết truyện trinh thám của tác giả Aoyama Gosho. Nhìn chung thì fan của Conan cũng không thể thiếu tập 60. 
5
531312
2016-04-27 11:11:57
--------------------------
378144
5585
Thám Tử Lừng Danh Conan Tập 60 (Tái Bản 2014) rất hấp dẫn và li kì bởi những tình tiết phá án,gây án.Thám Tử Lừng Danh Conan Tập 60 là một trong những tập của bộ truyện tranh trinh thámThám Tử Lừng Danh Conan nổi tiếng của Nhật Bản do tác giả  Aoyama Gosho sáng tác.Nhân vật chính của truyện là một thám tử học sinh trung học có tên là Kudo Shinichi - thám tử học đường xuất sắc - một lần bị bọn tội phạm ép uống thuốc độc và bị teo nhỏ thành học sinh tiểu học lấy tên là Conan Edogawa-thám tử nhí cùng với vụ án mạng  "Karaoke box", "Hammer man" và "Vụ giết người không tưởng"
5
917899
2016-02-04 14:20:06
--------------------------
377934
5585
Với trí tưởng tượng của một đứa trẻ, Conan đã vạch mặt "Người Vàng"-hung thủ đốt căn nhà và tấn công cha cậu bé-, dựa vào những chiếc xe ô tô trong hiện trường vụ án.
Môt anh chàng tên Okiya Subaru có những hành động và lời nói kì lạ khiến Haibara nghi ngờ. Liệu đây có phải là một thành viên trong tổ chức áo đen ?
Câu chuyên tình của hung thủ trong "li cà phê chết người" và kết thúc cay đắng....Truyện mang nhiếu cảm xúc trong từng file, với những tình tiết hấp dẫn, thật sự đây là một cuốn truyện thú vị dưới ngòi bút tinh tế của tác giả
5
97553
2016-02-03 20:26:16
--------------------------
377582
5585
Tập 60 Thám tử lừng danh Conan tiếp tục vụ án trong quán cà phê mà trong đó Eisuke là một trong những tình nghi. Phần tiếp theo là vụ do đội thám tử nhí ra tay cùng sự thận trọng của cô bé Haibara trước nhân vật Subaru đầy bí ẩn. Liệu anh ta tốt hay xấu? Phần cuối truyện là các cuộc giết người hàng loạt của "người búa" và sự thật sau ly cà phê cay đắng. Nhìn chung cả tập vẫn xoay quanh các vụ án đầy phức tạp nhưng được giải quyết nhanh chóng dưới suy luận sắc sảo của Conan.
4
449710
2016-02-02 22:25:27
--------------------------
362408
5585
một anh chàng tuyệt vời nữa lại xuất hiện trong tập này, anh là Subaru. Haibara có cảm giác không ổn với người này? Liệu anh có là thành viên tổ chức áo đen? vẫn chưa biết điều gì nhưng sau khi giải quyết vụ án, Conan đã đưa ra một lý do hết sức vô lý nhưng có vẻ mang niềm tin chắc chắn khi một người  mê Holmes không thể nào là người xấu được. Tất nhiên là Ai sẽ không thể yên tâm với lý do ấy được. Qua tập này, tôi biết thêm về nhièu cách tưởng tượng con người và biết cả xe caenh sát Nhật màu đen trắng cơ đấy!
4
362041
2016-01-01 22:12:11
--------------------------
353773
5585
Truyện tranh Conan tập 60 mua trên Tiki có chất lượng tốt, dày và đẹp hơn khi mua ở ngoài, với cùng một giá tiền, nên mình rất mừng khi khui gói hàng ra. Nội dung thì tất nhiên là không có chỗ chê. Dù đã đạt mốc 60 tập nhưng các vụ án vẫn khiến mình đau đầu nhức óc không đoán được. Trong tập này có vụ trắng-đỏ-vàng rất hay, mật mã kiểu trẻ con nhưng mình vẫn phải ngạc nhiên kiểu "dễ vậy mà sao không nghĩ ra chời". Ngoài ra, nhân vật Hondo Eisuke bí ẩn cuối cùng cũng lộ ra thân phận rồi đi mất, thay vào đó là một nhân vật bí ẩn mới xuất hiện. Bác Aoyama làm Conan FC phải tiếp tục hồi hộp mong ngóng nữa rồi đây!
5
131208
2015-12-16 21:03:12
--------------------------
277119
5585
conan tập 60 này sẽ tiếp tục vụ án tại quán karaoke mà nhân chứng là Esuke. Hung khí là một thứ không tưởng. Tiếp đến,đội thám tử lớp 1B bắt tay vào xử lí một vụ cháy nhà có liên quan đến cậu bé bạn học. Qua cách nhìn nhận sự việc một cách đơn giản và đầy ngô nghê, Conan cùng những người bạn cuả mình đã chỉ ra được hung thủ. Tập này còn có vụ tấn công người hàng loạt của một nhân vật với biệt danh ghê sợ. Truyện kết thúc với vụ án đẫm nước mắt của hung thủ... đây là một tập truyện rất hay và hấp dẫn.
4
302048
2015-08-24 17:48:17
--------------------------
256741
5585
Đây là một tác phẩm trinh thám tuyệt vời, bìa sách rất bắt mắt và chất lượng giấy cũng tốt. Hình ảnh sinh động,các nhân vật được phát thảo rõ nét từ ngoại hình đến tính cách. Truyện có những tình tiết gay cấn trong quá trình suy luận để tìm ra hung thủ cũng như sự thật được giấu sau một tấm rèm che. Truyện còn nói về tình yêu đẹp giữa ran và shinichi-conan, sự chờ đợi của Ran như để chứng tỏ một tình yêu vĩnh hằng của mình đối với anh chàng thám tử lừng danh của chúng ta
5
705378
2015-08-07 09:21:45
--------------------------
251789
5585
Những vụ án trong tập này đều có nhừng tình tiết hay khác nhau nhưng nếu so với những tập khác thì không nổi bật bằng. Có lẽ khá nhất là vụ của nhóm thám tử nhí, sự xuất hiện của nhân vật Subaru vẫn còn là điều bí ẩn, không biết anh ta là thù hay bạn. Tập 60 chỉ là những vụ án bình thường được phá chứ không đụng độ gì tới bọn Áo Đen nên cũng giảm bớt phần nào hấp dẫn. Tuy nhiên nếu là fan của Conan thì chắc chắn là không thể bỏ qua bất kì tập truyện nào rồi.
4
471112
2015-08-03 10:33:15
--------------------------
183038
5585
Cuốn sách này có chất lượng khá tốt, mực in đều màu, không bị lem, giá cả cũng phải chăng nữa. Mình thật sự rất thích kiều trang trí bìa của loạt sách mới này, ở phía sau còn một thám tử nổi tiếng trong các truyện trinh thám khác được tác giả Aoyama chọn lọc bằng cách kể hóm hỉnh. đầy tự nhiên. Mình rất hài lòng với bộ tái bản này của truyện Conan. Là tín đồ của Conan, chắc chắn mình sẽ ẵm trọn nguyên bộ này về nhà. Nhưng nó chỉ có một khuyết điểm nhỏ là nếu không sử dụng khéo, gáy sách sẽ  bị tróc ra do khô keo làm cho phần ruột trơ trọi cả ra.
5
558345
2015-04-15 12:52:25
--------------------------
179761
5585
ấn tượng với tập 60 vì từ hồi học cấp 1 đã mò bên nhà hàng xóm đọc rồi mặc dù không hiểu cái gì :) thích nhất vụ án liên quan đến những chiếc xe ô tô.qua lời kể ngây ngô trong nhật kí của cậu bé con trai chủ nhà,conan đã quá thông minh và không khó để cậu tìm ra thủ phạm.con đường suy luận ra thủ phạm ở vụ án này được aoyama viết ra rất hấp dẫn mà không quá khó hiểu.đọc bị cuốn hút.
Ai cảm thấy sợ hãi khi nhìn thấy ba nghi phạm.về sau khi biết Subaru là akai thì thấy vui vui.mong sẽ có ngày anh trở lại với cuộc chiến!
4
586877
2015-04-07 22:27:10
--------------------------
