469006
8356
Tôi đã mua cuốn “Những Chiếc Gân Trên Lá Có Tác Dụng Gì” từ TIKI. Sau một thời gian chờ đợi, tôi cũng nhận sản phẩm. Khuyết: không thấy. Ưu: giá không thể rẻ hơn, chữ đậm và rõ, hình vẽ đẹp, và dễ thương. Một yếu tố khác mà tôi chọn mua series sách này, khi so sánh các sản phẩm khác là sự hình thành thói quen tập quan sát thế giới quan xung quanh trẻ, nhằm kích thích óc sáng tạo, tò mò và học hỏi nơi trẻ nhỏ. Ví dụ, “gân trên lá”, quá quen thuộc đến đỗi bình thường mà chúng ta, những người lớn đôi khi không thèm để ý, và có suy nghĩ vì sao. Những câu hỏi như thế này, như một sự nhắc nhở nhẹ nhàng hãy quan tâm nhiều hơn đến những việc xung quanh. Đây là một quyển sách tuy nhỏ, nhưng ý nghĩa.
5
966571
2016-07-05 21:26:32
--------------------------
465559
8356
Quyển sách nhỏ chỉ gói gọn trong 8 trang nhưng nội dung chứa  lượng kiến thức không nhỏ chút nào. Bằng cách lần lượt đặt ra các câu hỏi:
1. Những chiếc gân lá có tác dụng gì?
2. Tại sao cây leo có thể leo được tường cao?
3. Tại sao lá cây có thể dùng làm thức ăn gia súc?
4.Tại sao mùa thu lá cây thường bị rụng?
5.Có phải trả xanh và hồng trà là lá của hai loại cây khác nhau?
và sau đó lần lượt trả lời một cách ngắn gọn, dễ hiểu, dễ tiếp thu so với lứa tuổi trẻ em. 
Chất lượng giấy tốt, hình vẽ dễ thương. Đây là một quyển sách đạt chất lượng về cả nội dung lẫn hình thức.
5
357674
2016-07-01 10:03:55
--------------------------
