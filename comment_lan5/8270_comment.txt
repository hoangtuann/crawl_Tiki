539589
8270
Một trong những cuốn sách tôi đọc liền tù tì từ đầu đến cuối và không muốn bỏ lỡ một phút nào để đọc nó.Một cuốn sách giá trị gấp nhiều lần so với mấy con số bìa sau góc phải bên dưới của sách.
Thực sự rất hay, rất cảm động, thương Lũi và bé Út Đậu kinh khủng.Mình tin chắc rằng 2 anh em sẽ sớm được về quê đoàn tụ với má thôi.Chắc chắn rồi.
5
1959767
2017-03-11 14:29:01
--------------------------
471784
8270
Những ô cửa sáng đèn của Kim Hài đã khiến mình ngồi khóc khi đọc, bởi vì thương cho hoàn cảnh nghèo khó của hai anh em Lũi và Út Đậu. Một câu chuyện cảm động, phải dừng lại nhiều lần để cảm xúc chùng xuống bởi vì mình rất dễ khóc khi thấy những đứa trẻ mà ở tuổi của chúng lẽ ra phải được ăn học và có cuộc sống tốt đẹp hơn là phải làm thuê bươn chải kiếm tiền. Những dòng văn nhẹ nhàng đi vào lòng người, bìa sách in rất đẹp, khổ sách nhỏ gọn, một quyển sách đáng đọc.
5
460351
2016-07-08 22:14:23
--------------------------
393657
8270
Tôi đã đọc tác phẩm của Kim Hài trước đây hơn 10 năm, và khi thấy tác phẩm này của bà tôi không ngần ngại rinh về và đọc ngay. Tác phẩm nói về cuộc sống trong xóm lao động nghèo của hai anh em Lũi và Út Đậu - những đứa trẻ phải ra đời quá sớm so với tuổi ăn tuổi học của mình. Sài Gòn lộng lẫy nhưng bên cạnh đó vẫn có những con người tần tảo mưu sinh, tác giả miêu tả cái cuộc sống khốn khổ của những con người đó rất tài tình, không phải ai cũng xấu xa, bên trong những gương mặt mệt mỏi đầy cáu gắt đó là tâm hồn đẹp đẽ, họ cũng có mơ ước, khát khao và cái khao khát cháy bỏng, khiến họ là việc quần quật kiếm từng đồng một là được trở về nhà, về quê hương, về cái nơi mà cho dù họ có tồi tệ đến đâu cũng dang tay ôm lấy họ. Cám ơn tác giả đã cho tôi hiểu thêm được một khía cạnh khác của cuộc đời.
5
52288
2016-03-09 08:13:06
--------------------------
320830
8270
thứ nhất bìa đẹp đọc thử thì hay tò mò về nội dung nên mình quyết định mua về đọc , 4 quyển mình mua thì nó là quyển đầu tiên mình đọc , Qủa thật , nếu không đọc thì mình không nghĩ có nhiều bất hạnh từ nhỏ phải lao động , câu chuyện cảm động xoay quanh những nhân vật nghèo khó phải bán mì hoành thánh và nhưng xung đột nhỏ của tuổi trẻ, mình thích người anh trai luôn chăm lo khi quyết định đi tàu về thăm mẹ ,kết thúc hơi phần lạc lõng quá,, :'(
5
715434
2015-10-12 14:11:58
--------------------------
305370
8270
Thật sự là một cuốn sách rất hay và cảm động. Mình thất thật tiếc khi cuốn sách chưa có ai viết nhận xét. Sâu sắc, nhẹ nhàng, cảm động tình cảm gia đình, tình anh em dù chỉ là đứa trẻ nhỏ, tình thương giữa những người lao động nghèo khó không có máu mủ gì với nhau. Từ đầu tới cuối truyện mình rớm nước mắt rất nhiều lần. Mình đã thực sự bị cuốn vào những mảnh đời trong truyện. Sài Gòn trong truyện hiện lên chân thật qua nhiều góc nhìn. 
 Rất khuyên mọi người nên mua đọc. !!!

5
531514
2015-09-16 22:26:58
--------------------------
