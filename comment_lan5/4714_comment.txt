262785
4714
Tôi không phải là người Sài Gòn chính gốc, chỉ là một người con xa quê đến học tập tại Sài Gòn và rời đi nơi khác để lập nghiệp nhưng đối với tôi Sài Gòn luôn là một nơi thú vị để nhớ về. Tủ sách của tôi có rất nhiều tác phẩm về Sài Gòn, khi đọc các tác phẩm ấy tôi lại cảm thấy như Sài Gòn gần gũi thân thương như máu thịt, từ tiếng rao của gánh hàng rong đến cái bình trà đá miễn phí đặt ven đường.

Với tác phẩm Sài Gòn Chợ Lớn rong chơi, lại một lần nữa tôi lại được nhìn Sài Gòn qua lăng kính của những con người khác nhau và họ đã khắc họa nên một nét rất riêng, đủ để nhận diện Sài Gòn qua từng cái tiêu đề. 
4
71445
2015-08-12 09:24:49
--------------------------
231037
4714
Nếu tự mình cố gắng làm việc phạm sai lầm và sửa sai thì sẽ rất khó khăn và tôn nhiều thời gian để có thể có được thành công. Chính vì vậy, để nhanh chóng học hỏi thêm được kiến thức và kinh nghiệm thì ai cũng cần phải có những người cố vấn. và "ai che lưng cho bạn" đã giúp độc giả biết cách duy trì mối quan hệ với những người cố vấn, tạo được sự thân thiện khi nói chuyện với mọi người để từ đó mọi người có thể chia sẻ kinh nghiệm, góp ý cho mình để mình có những quyết định đúng đắn hơn.
5
274812
2015-07-17 21:08:00
--------------------------
203341
4714
Tôi sinh ra và lớn lên trên mãnh đất miền Tây thân yêu,nhưng từ nhỏ tôi đã nghe đến hai từ Sài Gòn,nghe sao mà thân thuộc quá,và giờ tôi cũng đã lên Thành Phố này học tập,Sài Gòn khác với các chỗ khác đó là sự ồn ào sự năng động của riêng nó,hầu như Sài Gòn hội tụ đủ các người tại các tỉnh về đây. Từ đó làm cho cái thành phố năng động này lại càng thêm phong phú: vốn sống,văn hóa,giọng nói....Nếu bạn sống Sài Gòn mà bạn không đến khu chợ lớn thì đó là một đáng tiếc vô cùng...cám ơn tác giả đã gợi Sài Gòn và chợ Lớn trong tác phẩm này...
3
385020
2015-06-01 01:11:51
--------------------------
