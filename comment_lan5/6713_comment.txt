154717
6713
Mình mua cuốn truyện này cho bé trai 3 tuổi nhà mình với lý do bé đã được xem trên youtube vide về 3 chú heo con, nên mình muốn mua truyện để đọc cho bé có thể hiểu kỹ hơn câu truyện. Truyện có hình ảnh 3D sống động, đẹp, thậm chí hình ảnh chú sói với mình là người lớn còn thấy khá đáng sợ. Nội dung cũng dễ hiểu, tuy nhiên hơi nhiều chữ nên bé 3 tuổi nhà mình không thích lắm, thỉnh thoảng cũng đọc nhưng chưa bao giờ đọc được hết truyện. Mình nghĩ truyện này sẽ phù hợp hơn với những bé tầm 5 tuổi!
4
336914
2015-01-30 08:43:47
--------------------------
