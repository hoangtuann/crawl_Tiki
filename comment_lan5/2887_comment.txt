311447
2887
Khi đọc tập truyện nay, mình chỉ thích phần truyện Thiên tài khoa học. Về nội dung, truyện giới thiệu đến độc giả anh em nhà Wright - người đã khai sinh kỷ nguyên hàng không. Câu chuyện này đã giúp mình học hỏi được rất nhiều điều hay và bổ ích, đồng thời rèn luyện được nhiều đức tính tốt từ các nhà khoa học tài giỏi với những phát minh tuyệt vời. Còn phần truyện chính, do bị lỗi nên nhiều trang bị nhăn làm mình rất khó đọc. Mình hy vọng những tập truyện sau không bị lỗi như thế này nữa.
3
408969
2015-09-19 23:14:38
--------------------------
