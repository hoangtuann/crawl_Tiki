433378
7907
Sách là cuốn tiếp cận tuyệt vời về đại dương cho học sinh. Hình vẽ sinh động, sắc nét, thể hiện rõ ý tưởng. Nội dung không quá cao siêu, là những thứ cơ bản xung quanh chúng ta mỗi ngày như: mưa, thủy triều, vòng tuần hoàn của nước,...hay các hiện tượng thiên tai: lốc, vòi rồng,... Phần sau sách nói về sinh vật biển từ thuở khai sinh trái đất cho đến bây giờ. Sách phù hợp cho khoảng lớp 5 trở lên đến cấp 2 và chỉ đáp ứng kiến thức phổ thông thôi. Với mức giá như vậy so chất lượng sách là okie 
4
194394
2016-05-20 23:05:20
--------------------------
345311
7907
Mình đánh giá quyển sách Khám phá Biển này 7/10 điểm - khá ổn cho loại sách dành cho gia đình, không thuộc loại xuất sắc lắm, nhưng bạn có thể đọc đi đọc lại với khá nhiều thông tin trong đó. Sách trình bày và minh họa cũng hơi người lớn, hoặc thích hợp bé lớn 8-10 tuổi trở lên. Thông tin bên trong cũng thuộc dạng tổng quát cũng là một lý do, vì thường với bé nhỏ sẽ thích con vật cụ thể, miêu tả cụ thể hơn, "zoom" cận cảnh hơn. Đó là kinh nghiệm thực tế với hai bé nhà mình, tuy nhiên không sao cả, mình sẽ giữ gìn sách đợi đến một ngày bé hứng thú hơn. 
4
197061
2015-11-30 11:09:49
--------------------------
291711
7907
Cuốn sách là một kho kiến thức về thế giới đại dương, hấp dẫn cho những bé muốn tìm hiểu về mẹ biển, muốn cuốn bách khoa thư thu nhỏ thú vị. Với hơn một trăm trang sách thôi, tuy không phải là những nghiên cứu to lớn nhưng nó có thể cung cấp cho người đọc những câu hỏi cơ bản của biển như là về thủy triều, lý giải vì sao nước biển mặn,các loài cá mập. Cuốn sách được biên tập dưới dạng chủ đề và các câu hỏi. Có những hình minh họa sinh động vô cùng thú vị. Bìa sách được thiết kế khá đẹp, đại dương được các tác giả thể hiện rất phong phú. Chắc chắn các bé sẽ rất thích khi được khám phá đại dương qua những trang sách nhỏ hấp dẫn. Chất lượng giấy rất tốt, hình minh họa màu và chụp thực tế rất sinh động.
4
246068
2015-09-06 16:56:26
--------------------------
