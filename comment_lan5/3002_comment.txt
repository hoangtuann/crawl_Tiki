449342
3002
Các tác phẩm của Sơn Nam thì không cần phải bàn cãi, không phải tự nhiên mà người ta lại ưu ái đặt cho ông biệt danh Ông già Nam bộ. Trong quyển Gốc Cây, Cục Đá & Ngôi Sao - Danh Thắng Miền Nam này, tác giả ghi chép, mô tả vô cùng tỉ mi cách chơi kiểng, lý do tại sao người ta lại chơi và các danh thắng trải dài dải đất phía Nam của Tổ quốc này. Mua sách hay, lại còn được giảm giá và giao hàng cẩn thận. Cám ơn Tiki rất nhiều nhé. 
5
913302
2016-06-17 12:45:55
--------------------------
277618
3002
Bạn đã từng phiêu du miền Nam đến những vùng biển thơ mộng Hà Tiên , Kiên Giang , ghé thăm hòn phụ tử mà nay hòn phụ đã đổ sập không còn , những con người Sài Gòn - Gia Định từ những vùng đất mà có thể bạn chưa bao giờ đến . Hãy đọc ngay cuốn sách " Gốc Cây - Cục Đá & Ngôi Sao - Danh Thắng Miền Nam " để biết thêm nhiều thông tin bổ ích mà nhà văn Sơn Nam muốn nhắn gửi cho chúng ta trong từng câu , từng chữ . 
5
718377
2015-08-25 09:21:04
--------------------------
