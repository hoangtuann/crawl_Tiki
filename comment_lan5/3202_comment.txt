277424
3202
Con tôi đã đến tuổi học đọc, học viết, và hiển nhiên bé rất tò mò, thích thú với các con chữ. Ngoài việc cho trẻ theo học ở trường, bản thân tôi ở nhà cũng rất muốn hỗ trợ thêm cho bé. Nghe bạn thân giới thiệu bộ sách song ngữ Anh - Việt này, tôi vô cùng phấn khởi. Vì nhận ra sự bổ ích mà nó mang lại. Không những giúp trẻ tăng kỹ năng đọc, nhận dạng chữ ở cả hai ngôn ngữ, hơn hết cuối mỗi sách còn có các trò chơi giải trí, gia đình tôi đã có những giờ học bổ ích. Tri thức vì thế mà được nâng lên giá trị cao hơn..
Bé và tôi rất thích các hình ảnh minh họa, đáng yêu, rõ nét. Mùi giấy mới thơm. Khổ sách vừa tay. Thật sự rất thích !
3
478050
2015-08-24 22:22:54
--------------------------
