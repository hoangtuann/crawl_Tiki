499641
3446
Hình ảnh bị vỡ kể cả trang bìa.
Gáy trang dễ bong rách khi đọc nhiều.
Tên nhân vật dịch sang tiếng Việt => đọc không trôi => nên để tên gốc nhân vật.
1
1100656
2016-12-25 10:43:59
--------------------------
485778
3446
Không cần bàn thêm về nội dung. Dịch giả dịch khá tốt. Tuy nhiên mình muốn góp ý với dịch giả đôi lời: Mình không hiểu sao đa số dịch giả tại Việt Nam khi dịch sách đều dịch luôn tên của nhân vật? Mình định mua cuốn này nhưng qua đọc thử thấy không thể nào nuốt trôi phần tên được dịch kiểu: Uranôx,.. Vừa không đúng tên nhân vật, vừa chẳng phải tiếng Việt, thật là kỳ cục. Mỗi khi một cái tên như vậy lướt qua mắt mình thấy rất khó chịu. Mục đích của mình là mua cho con trai 12 tuổi đọc. Nếu bé đọc quen những cái tên như vậy thì sẽ gây nên sự lệch lạc hiểu biết về tên thật sự của nhân vật. Vì vậy mình đã không chọn mua.
3
1755652
2016-10-06 10:38:04
--------------------------
477841
3446
Đã đọc quyển sách này. Lời văn dịch tương đối ổn, nội dung hấp dẫn. Ý kiến cá nhân tên các vị thần hay địa danh nên giữ nguyên bản góc dịch phiên âm ra thấy không đúng lắm : như thần Poseidon dịch là Pô-xê-đôn lúc đọc còn không biết là thần biển, đỉnh Olumpos thì dịch là O6limpox. Cây gia phả khá rối rắm vì không  hợp với phong tục châu á. Các bố mẹ nên chọn lọc chuyện kể cho trẻ nghe không nên để trẻ tự xem. Về hình thức thì sách bìa dày rất đẹp, với lại tốc độ giao hàng của tiki khá nhanh đấy. Vài lời nhận xét.
3
763640
2016-07-29 09:47:49
--------------------------
453111
3446
Cuốn sách tập hợp khá đầy đủ những câu chuyện về thần thoại Hy Lạp, đỉnh Olympus về những con người trong thời đại Hy Lạp cổ. Là tập hợp những lý giải của con người trước sức mạnh của thiên nhiên khi mà khoa học chưa xuất hiện. Tính khí thất thường cùng với sức mạnh toàn năng của các vị thần, ý chí vươn lên và sự nhanh trí của con người trước hoàn cảnh khó khăn...những câu chuyện như vậy luôn khiến mình thích thú. Bản dịch chưa được chau chuốt cho lắm, sách còn có cả những hình minh họa. Rất đáng để bỏ chút thời gian rảnh rỗi để thưởng thức.
3
596518
2016-06-21 01:03:32
--------------------------
424296
3446
Cuốn sách này theo mình thì nội dung tạm ổn. Cách kể chuyện cũng khá hay và hấp dẫn, lôi cuốn người đọc. Bìa sách đẹp, khá ấn tượng đối với mình. Các tranh ảnh minh họa cũng rất đẹp. Tuy nhiên mình không thích cho lắm ở phần tên các vị thần. Tên các vị thần nên viết nguyên âm và tiếng nước ngoài thì tác phẩm sẽ có giá trị hơn. Ngoài ra, sách còn viết sai một số lỗi chính tả. Tuy k nhiều nhưng nó làm cho quyển sách mất đi giá trị và người đọc chưa cảm thấy hài lòng.
5
1218232
2016-05-01 12:33:41
--------------------------
404623
3446
Cũng giống như những quyển sách khác về "Thần thoại Hy Lạp", bản sách này cung cấp cho bạn đọc những thông tin thú vị, hấp dẫn về thế giới của những vị thần Hy Lạp. 

Điểm cộng: 
 + Sách có độ dày tương đối.
 + Bìa sách chắc.
 + Giấy màu dễ đọc, may mắn là tôi chưa phát hiện lỗi nhòe chữ. 
 + Đặc biệt: bên cạnh tên của các vị thần, địa danh đã được Việt hóa thì sách có chú thích thêm tên gốc (chưa Việt hóa bên dưới sách) giúp bạn đọc dễ theo dõi. 
Điểm trừ: 
 + Phần hình minh họa nhìn không hấp dẫn. 


 
4
1199770
2016-03-25 14:34:55
--------------------------
392285
3446
Đây là cuốn sách khá hay. Có vô số mẫu chuyện kể về thế giới thần linh và các vị anh hùng trong thời Hy Lạp cổ đại. Từng hình ảnh được khắc họa rõ nét. Các vị thần trong thần thoại Hi Lạp được miêu tả khá hiện thực, tao cũng biết buồn, vui, giận dữ, ganh tị. Các vị anh hùng được miêu tả một cách hùng dũng qua các chiến công của họ
Mình đã thích Thần Thoại Hi Lạp từ khi còn bé mà bây giờ mới có cơ hội để tìm hiểu nhiều hơn. Thế giới thần thoại thật đẹp.
5
899370
2016-03-06 17:33:38
--------------------------
378614
3446
Sách được đóng bìa cứng rất chắc chắn. Tuy được trang trí khá màu mè nhưng tôi chưa thấy được sự trau chuốt của khâu thiết kế và trình bày. Ngay từ trang bìa phía trong đã thấy các họa tiết trang trí bị vỡ một cách thô kệch. Lật vào bên trong các hình ảnh minh họa đều đúng nghĩa của từ tệ, rất mờ và khó nhìn. Thêm vào đó, các tên riêng đều phiên âm sang tiếng Việt, tôi không hề thích điều này. Về nội dung thì đây và một cuốn sách đáng đọc, các câu chuyện khá là lôi cuốn.
3
383320
2016-02-06 12:38:00
--------------------------
376839
3446
Mới hốt ẻm này về, nhận sách mà lòng vui phơi phới, đọc nội dung cực lôi cuốn thích cuốn này từ lâu lắm rồi,bìa sách đẹp và chắc chắn nữa, tuy nhiên tên nhân vật thì hơi khó nhớ một chút và bố cục hơi sơ sài, chữ cũng hơi nhoè một chút. Vốn là người ít đọc sách nhưng nhìn chung đây là cuốn sách hay mà ai cũng nên đọc một lần trong đời. Về phục vụ của tiki thì rất thích lần đầu đặt mua sách ở đây vô cùng hài lòng, thời gian nhanh chóng
5
113483
2016-01-31 23:52:40
--------------------------
324203
3446
Về nội dung của bộ truyện này thì các bạn không có gì để bàn  cãi nữa. Một tác phẩm kinh điển của thế giới đang ở trước mắt bạn. Phần in chữ của sách rất tốt, giấy khá cứng, dày, bìa sách cứng đẹp còn có ảnh minh họa của các vị thần bằng những bức tranh nổi tiếng (có điều hơi mờ tí). Theo mình thì các bạn cũng nên mua cho mình một cuốn về đọc, giá thành của nó cũng khá là rẻ.câu chuyện vô cùng lôi cuốn người đọc Nó giống như một xã hội thu nhỏ vậy, câu chuyện kể về mười hai vị thần trên đỉnh olympus đấu vs các titan rất hấp dẫn. Với lại tốc độ giao hàng của tiki khá nhanh đấy ^^
5
813501
2015-10-20 18:25:40
--------------------------
320298
3446
Về nội dung thì có lẽ mọi người cũng đã biết rồi vì quá nổi tiếng; sách, truyện, phim,... đều có đủ. Thế giới của các vị thần cũng lắm chuyện và phức tạp giống như xã hội loài người thu nhỏ, mối quan hệ của các vị cơ mà phải viết thành cây phả hệ xem mới nắm hết được. Về hình thức sách có nhiều hình ảnh nhỏ minh họa đẹp (đa số hình mấy chị nữ lộ nguyên vòng 1 không hà, cái này mấy anh nam khoài nè, hihi), tên các vị thần được việt hóa nhưng không có dấu gạch nối nên cũng không đến mức khó chịu, mục lục tên các vị thần rõ ràng giúp mình dễ hiểu, giấy dày, ngà ngà đẹp lắm luôn, sách cầm trên tay rất chắc chắn. Tuy nhiên chất lượng in không tốt, mực trang đậm trang nhạt nhìn khó chịu, chữ to nhưng độ giãn giữa các chữ quá gần không hợp lý, nhìn vào toàn chữ với chữ, đọc khoảng 2 trang là muốn bỏ xuống rồi vì quá mỏi mắt. Thiết kế bìa không đẹp, tối tăm sao á, chỉ được cái tên sách in vàng nổi là coi được. Tiki giao sách cho mình từ Hà Nội về tróc nguyên cạnh sách phía dưới, thất vọng lắm luôn. Cuốn này tính ra cũng mắc tiền nên các bạn cân nhắc kỹ trước khi mua nha.
3
719449
2015-10-10 22:03:32
--------------------------
310844
3446
Còn phải nói, đây là áng văn vô tiền khoán hậu trong lịch sử văn học thế giới. Nếu một ngày, Thần thoại Hi Lạp biến mất thì không biết còn có áng văn nào để sức mạnh và giá trị để thay thế nó? Một phần, Thần thoại Hy Lạp phàn ánh tư duy, tình cảm của nhân dân Hy Lạp, một mặt phản ánh trí tưởng tượng phong phú và sáng tạo của họ. Từng câu chuyện nhỏ mang một điển tích dạy hậu nhân rất nhiều điều, từ gót chân Achilles cho đến ngựa gỗ thành Troa.. Rất phong phú và đa dạng. Cuốn sách rất đẹp, đặc biệt mình rất thích phần phụ lục tên các vị thần và anh hùng
4
366416
2015-09-19 20:09:18
--------------------------
301791
3446
Ở mỗi khu vực trên thế giới điều có những thần thoại khác nhau để giải thích về nguồn gốc của tự nhiên, vũ tụ, con người. Góp phần vào kho tàng văn hóa chung của nhân loại - một tuyệt tác Thần Thoại Hy Lạp - đã đem lại cho chúng ta một cái nhìn khác về thế giới tâm linh, con người, tự nhiên và vũ trụ ở thời Hy Lạp cổ đại. Nội dung quyển sách đáng để chúng ta tìm hiểu về một nền văn minh rực gỡ của của nhân loại dưới cách giải thích của người Hy Lạp xưa. Bên cạnh đó chúng ta còn được trải nghiệm vè phiêu lưu cùng với những anh hùng, những huyền loại làm nên thế giới huy hoàng.
4
755291
2015-09-14 20:51:18
--------------------------
299316
3446
Trước giờ, tôi chỉ biết một vài vị thần hy lạp qua những bộ phim được trình chiếu, và với tôi đó là những bộ phim rất hay, rất hấp dẫn và gây ấn tượng. Qua cuốn sách này, tôi mới biết rõ hơn về nguồn gốc của thần thánh, mối liên hệ giữa các vị thần cũng như cuộc sống của họ. Thế giới các vị thần cũng như thế giới của người trần, họ thần thánh, có quyền năng riêng của mình nhưng cũng vẫn là loài có yêu có ghét có thương có hận.... Ở đây, nó còn phản ánh sự tin tưởng thần thánh gần như tuyệt đối của người xưa. Phong tục, tập quán và những tục lệ mà đến nay ở nhiều nơi còn giữ. Sách là những câu chuyện lôi cuốn, hấp dẫn và mang màu sắc thần linh. 
4
6502
2015-09-13 09:41:27
--------------------------
291687
3446
Biên dịch:		Trường Tân
NXB:			Hồng Đức
Thể loại:		Thần Thoại
Bìa:			Cứng, rất đẹp
Chất lượng in, giấy: Tốt
Chất lượng dịch:	Tốt
Nhận xét:		Trong thế giới truyện thần thoại, cổ tích, truyền thuyết, Thần Thoại Hy Lạp là một tác phẩm không thể thiếu trong tủ sách nhà bạn, đặc biệt nếu nhà bạn có trẻ nhỏ. Cuốn sách này được in với bìa cứng, hình ảnh minh họa rất đẹp, bên trong, còn có nhiều hình ảnh minh họa đen trắng khác về các vị thần hoặc các sự kiện. Điều khiếm khuyết, các tên riêng của nhân vật và địa danh được Việt hóa rất khó chịu.
Khuyên: 		Sưu tầm, nên đọc.

5
129879
2015-09-06 16:35:47
--------------------------
206159
3446
Tuy bìa không được đẹp,nhưng mình cũng nhờ bạn mua giùm cuốn sách này.Nó gần như gắn với tuổi thơ của mình vì hồi nhỏ anh chị mình hay kể cho mình những mẩu chuyện nhỏ trong thần thoại Hy Lạp.Thần thoại Hy Lạp cổ quả thật hay,ly kỳ và rất...thân thoại.Thế giới của các vị thần cũng phức tạp y hệt thế giới con người vậy.Truyện phản ánh một xã hội Hy Lạp cổ đại rất sùng bái thần linh và có lòng tôn kính đến các lớp người trước.Điều này có điểm khá gần với các câu chuyện truyền miệng trong truyền thuyết Việt Nam.Thần thoại Hy Lạp xứng đáng là câu chuyện kể cho trẻ em.
5
440861
2015-06-08 21:52:59
--------------------------
205397
3446
dù là thần thoại, "thần thoại Hy Lạp" như là một cuốn tiểu thuyết lôi cuốn người đọc. Từng câu chuyện liên kết với nhau một cách chặt chẽ như thể là thật và nó là thần thoại nổi tiếng nhất thế giới thì chúng ta không còn gì phải móc mẽ hay chê bai - quá tuyệt vời. Không phải từ một tác giả nhưng nó như một câu chuyện do một người sáng tác, đây có thể coi là kiệt tác văn học cổ, một câu chuyện tuyệt vời. Bản dịch này thì không biết sẽ như thế nào, nhìn qua bìa sách thấy bất ổn về bố cục và hơi sơ sài, đây là một điểm trừ của quyển sách này.
4
277478
2015-06-06 17:21:16
--------------------------
