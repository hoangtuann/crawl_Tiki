478070
6892
Tôi thích cuốn sách này vì có nhiều truyện ngắn hay. Mặc dù bìa cuốn sách có ghi là truyện cho lứa tuổi chớm yêu nhưng tôi thấy phù hợp với nhiều người đã qua lứa tuổi này chứ không chỉ dành riêng cho tuổi ô mai. Tôi đặc biệt thích truyên ngắn "Chiếc ruy băng màu mạ non". Khi đọc truyện này tôi thấy chính mình như là cô bé nhân vật chính vì tôi đã từng có trải nghiệm giống cô bé này, vừa đọc mà vừa cười cái cử chỉ, hành động của cô bé khi nói chuyện với chàng trai mà cô thích.
Tuy nhiên có một vài truyện ngắn của nước ngoài, tôi không thích lắm, có lẽ phải dịch lại nên thiếu sự truyền cảm chăng, đôi chỗ đọc hơi khó hiểu. Dù sao cũng rất đáng mua để đọc, nhất là đã giảm giá 25% như vậy rồi.
4
1116390
2016-07-30 18:17:46
--------------------------
412665
6892
Tôi có vẻ hơi khó, tôi không thích đọc thể loại truyện ngắn từng tập như thế này. Lại còn xen lẫn giữa  truyện Việt, nước ngoài với nhau. Mới đầu cứ tưởng là một quyển sách về 1 câu chuyện tình yêu, nhưng không, chỉ là vài mẩu truyện ngắn không rõ mở đầu và kết thúc được đóng lại thành một tập. Tuy khá thất vọng về nội dung sách nhưng tôi cho 3 sao vì sự phục vụ khách hành của Tiki rất nhanh chóng và gọn lẹ. Tôi hi vọng mình có thể đặt và mua hàng ở Tiki tiếp.
Tôi thích cách làm việc của Tiki.
2
1273083
2016-04-08 10:05:09
--------------------------
291759
6892
Cuốn sách là những câu truyện tình cảm hết sức ngọt ngào đầy yêu thương. Những tình cảm đầu đời, những mối tình đầu có cả cay, đắng, mặn, ngot nhưng vẫn khiến người ta xao xuyến. Những tình cảm đầu đời đó khiến người ta không ngừng bồi hồi xao xuyến khi nhớ lại, có lúc cảm thấy tiếc nuối, khi lại ngọt ngào khó quên. Không phải ai cũng có thể giữ được tình cảm đẹp đẽ đó nhưng với tác phẩm này tác giả khiến cho tâm hồn người đọc như được vỗ về, êm dịu. Một cuốn truyện có tính nhân văn và giáo dục cao giành cho lứa tuổi thiếu niên.
4
280592
2015-09-06 17:58:36
--------------------------
