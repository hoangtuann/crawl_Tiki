574721
5750
Tiki giao hàng rất nhanh chóng, sách bìa cứng rất đẹp, tuy nhiên vẫn còn nhiều lỗi chính tả trong sách, nhưng về nội dung thì tuyệt vời
4
1556667
2017-04-16 14:37:54
--------------------------
546085
5750
Đầu tiên, về bao bì sản phẩm là bì cứng dày nên nhìn thấy rất đẹp mắt cộng với việc gói và vận chuyển hàng quá good của Tiki nên mình vote 5* ihhihi.

Tựa đề hấp dẫn ,lôi cuốn người mua nhất là đối với những người đang có một cuộc sống buồn chán, tẻ nhạt và đang cần sự thay đổi phá cách ,mới mẻ.Hãy thay đổi ngay hôm nay, những hành động nhỏ mỗi ngày, tích cực sẽ giúp cho cuộc sống bạn thay đổi đến bất ngờ và tốt đẹp hơn. Quyển sách tốt là quyển sách hướng chúng ta đến một chúng ta tốt đẹp hơn đúng đắn hơn, vậy còn chờ gì nữa mà không mua lẹ đi hỉ ? :))))
Chúc các bạn ngày vui, thông minh và sáng suốt!!!!!
5
273540
2017-03-17 20:24:56
--------------------------
442253
5750
Mình đọc sách này hồi trung học, cũng cố gắng làm theo những chỉ dẫn trong sách và thấy rất hiệu quả. Nhưng thực tế là mình hiểu rất ít hoặc không hiểu những nguyên lý đằng sau những nguyên tắc đơn giản ấy.
Bây giờ đã trưởng thành, đọc lại cuốn sách một lần nữa đem đến cho mình những cái nhìn sâu sắc hơn, qua đó mình có thể giải thích được những xui xẻo, khó khăn đến với mình và biết cách chuẩn bị về mặt tinh thần để vượt qua.
Mình cũng học đựơc cách lạc quan, cách đối nhân xử thế sao cho nhẹ nhàng.
Cuốn sách này là cuốn sách cuộc đời của mình.
5
920910
2016-06-04 19:03:16
--------------------------
327693
5750
Cuốn sách khá lạ và độc đáo, nội dung ấn tượng cuốn hút tôi nên tôi dành cho 2 sao, còn lại 2 sao tôi dành cho những bức tranh ngộ nghĩnh còn lại.
Cuốn sách rất đặc sắc, đọc xong rồi mà tôi vẫn muốn đọc lại. Tiêu đề cũng chính là triết lý xuyên suốt cuốn sách: "Cuộc Sống Thay Đổi Khi Chúng Ta Thay Đổi".
Và nó đã thực sự làm thay đổi tôi, từ cách nghĩ cho đến những hành động
Và 1 sao còn lại thì tôi dành cho giá tiền- vì nó rất hợp lý. 
Cảm ơn tiki đã giới thiệu cuốn sách đến tôi.
5
702741
2015-10-28 11:54:02
--------------------------
322322
5750
Tôi đang đọc được nửa quyển sách, sách viết rất hay tuy nhiên lỗi chính tả ít nhưng lập đi lập lại nhiều lần, làm cảm xúc đọc sách của tôi có một chút không thoải mái. 
Nội dung quyển sách rất hay, tôi luôn nghĩ mình nghĩ mình làm được thì sẽ làm được và đọc quyển sách này thì củng cố thêm suy nghĩ cho tôi với hướng suy nghĩ tích cực hơn trong cuộc sống, khiến tôi đối mặt với khó khăn dễ dàng hơn, mỉm cười vui vẻ hơn.
Riêng cá nhân tôi thấy mọi người nên có một quyển cho cá nhân mình, đôi khi giúp bạn gỡ được khúc mắc cuộc sống
Trân thành cảm ơn dịch vụ của tiki đã đưa tôi đến một quyển sách hay
5
682130
2015-10-15 23:48:45
--------------------------
306767
5750
Mới đầu là tính chọn những cuốn " đời thay đổi khi chúng ta thay đổi" nhưng thấy cuốn này là tổng hợp của những cuốn đó lên mua luôn 1 lần cho tiện ^^! Hình thức bên ngoài thì bìa cứng sách mà mình nhận được hơi bị móp 1 xíu xíu ở hai đầu @@ không biết là va chạm ở đâu?( hơi xót tí)! Còn về những trang sách ở trong cũng khá tốt nè :) không quá mỏng cũng không quá dày, chữ in to rõ, hình vẽ cũng vậy khá là sinh động. Đối với mình một cuốn sách bìa cứng, hơn 500 trang, giấy không quá tốt, nội dung tuyệt vời như vậy mà giá 68k(tại Tiki) thì quá là HỢP LÝ luôn. Cám ơn tác giả, nhà sản xuất và Tiki nhiều nha.
5
675686
2015-09-17 19:48:23
--------------------------
249969
5750
Mình xin phép chỉ nhận xét về hình thức chứ không về nội dung vì theo như mình thấy, các cuốn còn lại của Andrew đã nói lên tất cả rồi. 4 sao mình chọn trong đó có 2,5 sao là về nội dung, còn hình thức chỉ tạm được 1,5 sao thôi. Những cuốn bìa cứng mình mua về thì khá đẹp còn cuốn này dường như giấy không được chất lượng cho lắm, thỉnh thoảng đọc còn bị rít giấy. Chữ đôi lúc bị nhòe, chữ to chữ nhỏ?  Nói tóm lại là mình mua sách vì nội dung của chúng chứ không phải vẻ bề ngoài thế nhưng chất lượng cũng đánh dấu một phần quyết định trong cảm tình của người đọc đối với tác phẩm.
4
445175
2015-08-01 01:02:07
--------------------------
227902
5750
Về hình thức: bìa cứng, trình bày bìa khá sinh động. Tuy nhiên chất lượng trang giấy không được tốt lắm nên trình bày chữ cũng không được tốt. Cho nên, nhìn vào là thấy mất hết cảm tình.
Về nội dung: có thể nhận thấy đây là tổng hợp nội dung của 5 quyển Đời thay đổi khi chúng ta thay đổi nên không cần gì để bàn luận lại nội dung này nữa. Nếu cần thiết, mỗi người sẽ tự trải nghiệm thực tế cho bản thân mình vì tùy mỗi người nhận thức khác nhau mà đánh giá được nội dung có phù hợp với bản thân hay không
4
502287
2015-07-14 17:02:13
--------------------------
164508
5750
Mình mua quyển này cũng với 5 quyển khác của Andrew. Tuy nhiên mình thấy chưa thực sự hài lòng lắm. Bìa cứng khá đẹp, nhưng giấy bên trong thì mỏng, chữ dày và nhoè. Mình không thích hình thức lắm. Nhưng xét về nội dung thì thật tuyệt vời. Cũng với 5 quyển Đời thay đổi khi chúng ta thay đổi, cuốn sách này cho chúng ta những kĩ năng mềm cần thiết cho cuộc sống hàng ngày. Cách viết của tác giả chia phần rõ ràng, đọc rất dễ nhớ để có thể vẫn dụng trong cuộc sống. Nói chung mình thấy rất vui khi có được quyển sách này.
4
466113
2015-03-07 23:27:13
--------------------------
