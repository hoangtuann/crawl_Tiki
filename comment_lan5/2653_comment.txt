404014
2653
Tập 4 này phải nói là cực kì tuyệt vời. Bìa được thiết kế đẹp hơn với màu xanh lơ. Các nhân vật rất dễ thương, cốt truyện thì vẫn vui, vẫn bựa như các tập trước. Trong tập này thì đã khắc phục được lỗi phông chữ nhỏ, các nét vẽ cứng cáp hơn, không thô như trước nữa nên cho 5 sao. Trong tập này tập trung vào Cự Giải là nhiều, thích nhất chap "Những lời khó nói của Cự Giải" vì chap này giúp chúng ta có thể hiểu được những "Tâm tự trong lòng" thật lãng xẹt của Cự Giải =)))))). 
5
988511
2016-03-24 15:40:49
--------------------------
395200
2653
Tập 4 của Học viện mật ngữ 12 chòm sao này chất lượng khác hẳn rõ rệt so với các tập khác. Font chữ dễ nhìn hơn so với các tập trước. Thật sự rất mong chờ tập 4 này về đến tay mình, và đã ko làm mình thất vọng, nội dung vẫn siêu bựa, hài hước như các tập trước đó. Đặc biệt truyện tiki giao tới vẫn còn mới in, bìa siêu đẹp, mịn, ko bị gãy nếp, nhăn góc :) Truyện rất thích hợp để giải trí, xả stress những lúc rãnh rỗi. Vẫn tiếp tục ủng hộ. Lót dép hóng tập 5 nào các bn ơi ^^ 
5
1044360
2016-03-11 16:20:35
--------------------------
334780
2653
Tập 4 đã về đến nhà. Mình cảm thấy thật hạnh phúc biết bao vì mình đã luôn mong chờ nó từng ngày. Tập 4 đã khắc phục được nhiều nhược điểm của 3 tập đầu như chữ đã in to ra, giấy và chất lượng in tốt lên, nét vẽ đẹp. Nội dung vẫn hay như vậy có khi còn hay hơn hẳn nữa. Các nhân vật thì miễn bàn, ngộ nghĩnh, dễ thương và trông ngố ngố. Tập nay cười vỡ bụng lun nhất là lúc sau khi cự giải kể về việc mình  khổ tâm và lúc thiên bình thời đồ đá.
5
771049
2015-11-10 00:15:07
--------------------------
303818
2653
Nối tiếp các tập truyện trước, Học Viện Mật Ngữ 12 Chòm Sao (Tập 4) đã phát hành. Lần này, bìa truyện được làm đẹp hơn mấy tập trước: bìa truyện nổi bật với màu xanh nhẹ nhàng, dịu mắt và hình ảnh dễ thương, ngộ nghĩnh của 12 chòm sao. Còn về nội dung thì truyện vẫn giữ nguyên phong cách hài hước, hóm hỉnh và cực kỳ vui nhộn như mấy tập truyện trước. Mình thích nhất là câu chuyện "Nỗi buồn khó nói của Cự Giải" vì các chòm sao trong câu chuyện này có phần "tưng tưng", không giống như người bình thường nhưng quá vui nhộn và quậy hết mình.
5
408969
2015-09-15 23:46:41
--------------------------
286908
2653
Bìa sách rất ưng, đẹp, mịn, bắt mắt. Nội dung bên trong hài hước, cười không nhặt được mồm. Đem vào lớp đọc mấy đứa bạn còn tưởng mình điên. Các nhân vật ngộ nghĩnh, hài hước, đặt biệt là có phần "tưng tửng" một dàn. Thích nhất nhân vật Bạch Dương, Sư Tử và Nhân Mã trong mỗi tập, bá đạo. Truyện rất hấp dẫn, hài, thích bộ truyện này. Luôn hóng bao giờ ra tập mới để còn mua nên bây giờ có đủ 5 tập truyện rồi ^^! Mau mau ra tập 6 để em còn mua. :))
5
313062
2015-09-02 07:45:24
--------------------------
279430
2653
Mình vẫn rất hài lòng khi đọc tiếp tập 4, sau 1 tràng nội dung hài và bựa không còn gì để nói nữa =)))))) vẫn biết là không thể lột tả hết được 12 cung hoàng đạo một cách công bằng nhưng mình cũng buồn một tẹo *rưng rưng* đọc xong 4 tập mình thấy các nét vẽ có hơi không rõ ràng, mình nghĩ tác giả or somebody nên để đậm nhạt rõ ràng hơn, phân bố các nhân vật và cảnh sao đó cho nó nổi bật tí, thực thì nhìn hơi rối và đôi khi không xác định được nhân vật chính trong khung hình nó như thế nào nữa. Tóm lại thì mình vẫn rất thích truyện :)) bởi mới mua 5 cuốn luôn đó :v
5
698682
2015-08-26 20:08:39
--------------------------
267716
2653
Rất ưng ý bìa tập 4, màu xanh  mát mắt dễ chịu, giấy nhẹ, nhưng trang ít quá, đọc chưa đã là hết mất rồi. Các nhân vật vẫn "tưng tửng" như hồi mới ra mắt :)) , mình rất thích hội Những cậu bé cởi truồng ngộ nghĩnh và hội Những người tặng quà quyến rũ, cười không nhặt được mồm. Điều phải nói là lần đầu tiên thấy Xử Nữ mở mắt nói chuyện, thầm cảm ơn tác giả đã cho thêm 1 đứa bị "mắt nhỏ" giống Song Ngư (chỉ có 2 chấm, trong khi mắt mấy đứa kia long lanh, to đùng).
4
593346
2015-08-15 20:26:40
--------------------------
266174
2653
Vẫn là nét vẽ nv ngộ nghĩnh và dễ thương, nội dung tr hài hước, tác giả đã đem tới cho mình những giây phút thật vui vẻ khi đọc cuốn này :)))) Đặc biệt, tập 4 này còn có phần phụ lục đầy ngộ nghĩnh k kém j các tình huống trong tr . Đọc mà cười k ra tiếng! :))) Ngoài ra, trong tập 4 còn gây cười nhiều nhất ở phần trở về thời tiền sử!! Haha!!! Đọc đến đoạn đấy thì chỉ có ôm đất mà cười thôi!!;)))) Mong những có thêm nhiều tình huống hay và thú vị hơn nữa!! :)))
5
419973
2015-08-14 14:16:25
--------------------------
257034
2653
Mình đã mua quyển truyện này , ấn tượng đầu tiên của mình là bìa truyện đẹp , nhưng nét vẽ thì khá là rối mắt , đánh bóng nhân vật không đều . Nhưng về nội dung thì khá tốt ,hài hước , hấp dẫn ,có thể đọc để giải trí , cách khắc họa tính cách của từng cung rất ổn . Thêm đó mình khá thích cái phần dành cho tác giả , rất dễ thương . Mình vẫn sẽ mua tập 5 và mong chờ có sự tiến bộ về cách vẽ của tác giả .
3
688834
2015-08-07 13:01:25
--------------------------
241736
2653
Quyển truyện này có hình vẽ rất dễ thương và phong cách rất Việt, mang lại niềm vui cho người đọc. Về Horoscope cũng có mang vào một số tính cách tổng quát của các cung hoàng đạo, kiểu như các cung Lửa thì dễ nóng tính, thích thể hiện, đại diện bởi hình vẽ cung Sư Tử thường hay ra tay cầm đầu ^^
Serie truyện này rất thú vị, đáng để đọc cho vui vào những lúc rảnh rỗi.
Mình ủng hộ sự ra đời và tiếp tục mong ngóng cho ra thêm những tập sau nhanh nhanh !
4
18618
2015-07-25 20:09:14
--------------------------
239910
2653
Quyển này rất tuyệt vời :>, thích nhất phần phụ lục. Nhân vật rất nhí nhảnh, cốt truyện cực vui, có nhiều đoạn mắc cười. Thích nhất mấy cái phần bên lề của tác giả, cực mắc cười =)). Còn các phần truyện chính trong tập 4 thì thích nhất là nỗi buồn của cự giải, cuối cùng thì lí do buồn cực mắc cười, các bạn nên mua để biết lí do đó. Mong rằng tập 5 sẽ hay hơn nhiều. Hóng tập 5 ra nhanh. Chúc các bạn khác coi vui vẻ giống mình nhen, hi hi hi
5
662221
2015-07-24 10:09:37
--------------------------
223591
2653
Quyển 4 này thực sự rất tuyệt vời luôn :"> phải nói là các nhân vật càng ngày càng dễ thương và cốt truyện ngày càng vui, hấp dẫn. Quyển này thích nhất là chương lạc về thời tiền sử, đọc buồn cười không chịu được, nhất là đoạn Thiên Bình thời tiền sử xuất hiện. Tập 4 lần này đã khắc phục được lỗi chữ bị nhỏ mà các tập khác vẫn còn, thêm 1 điểm cộng. Phải nói thật là lâu rồi mình mới để dành tiền mỗi tháng để chờ mua một bộ truyện tranh như thế.  Mong chờ tập 5.
5
591834
2015-07-07 12:20:04
--------------------------
