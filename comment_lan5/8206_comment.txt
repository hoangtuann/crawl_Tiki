433102
8206
Tác giả Lê Văn Sự có viết rất nhiều sách tiếng Anh, mình thấy sách của thầy ấy, khá hợp với từng năng lực, không đánh đố, mang tính tổng quát để ôn tập lại kiến thức. Cuốn sách 60 Đề Thi Nghe - Nói - Đọc - Viết: Luyện Thi Chứng Chỉ A Tiếng Anh năm trong bộ A, B, C có tặng kèm cả địa CD để luyện nghe. Có điểm minh không hài lòng ở cuốn sách này là chất lượng giấy mỏng và dễ rách, cũng không được trắng đẹp nữa. Sách đầy đủ đề thi của các kỹ năng và đáp án.
4
854660
2016-05-20 12:57:37
--------------------------
311294
8206
Nằm trong bộ sách 3 quyển luyện thi chứng chỉ Tiếng Anh ABC.
+ Ưu điểm: - gồm cả 4 kỹ năng Nghe - Nói - Đọc - Viết trong một quyển
                   - lời nói đầu được dịch song ngữ Anh - Việt
                   - có kèm CD để tự ôn luyện
                   - có cả lịch dạy đề nghị trong 4 tuần để mình sắp xếp thời gian hợp lý
                   -  giấy in mỏng vừa và có màu trắng sáng.
+ Khuyết điểm: nhà xuất bản in sách không được chuẩn lắm, nhiều trang sách chưa được tách rời, người học phải tự dùng kéo cắt.
4
276944
2015-09-19 22:32:43
--------------------------
