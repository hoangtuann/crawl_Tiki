546081
5447
Với tư cách một dân marketing và đang hứng thú với lĩnh vực sale, quyển sách này không thể không có trong tủ sách của mình. Vừa thấy nó là mình quăng vô giỏ hàng liền. Tuy chưa đọc nhưng mình sẽ xem sơ qua và mình tin rằng nó sẽ vô cùng hữu ích cho mình. GOOD CHOICE!!!!!

Thêm nữa, Tiki giao và gói hàng tốt quá nên yêu uuuuuuuuuuuuu!!!
5
273540
2017-03-17 20:19:29
--------------------------
224131
5447
Mình mua cuốn sách này cũng khá lâu rồi và rất ấn tượng với cách tiếp cận vấn đề của tác giả.
Cuốn sách này không chỉ giúp mình hiểu về hành vi của khách hàng, mà còn giúp mình hiểu về hành vi tiêu dùng của chính mình. Đôi lúc mình hay suy ngẫm về 1 số tình huống phát sinh trong ngày, trong công việc và liên tưởng đến các nội dung trong sách.
Rất thú vị.
4
302479
2015-07-08 10:06:14
--------------------------
216565
5447
Tôi mua cuốn sách này vì muốn tìm hiểu và hạn chế việc mua sắm hơi "vô tội vạ" của bản thân. Việc hiểu về những thủ thuật, biện pháp của các công ty khiến người tiêu dùng không ngại ngần bỏ tiền ra mua hàng của họ rất hữu ích cho những người có tật hay mua sắm nhiều.
Đây cũng là một cuốn sách rất hay về marketing, nhất là về mảng hành vi người tiêu dùng. Tôi đã học môn này trên trường đại học nhưng chỉ được học phần ngọn, tức những hành vi, những gì biểu hiện ra bên ngoài. Còn phần gốc, tức nguyên nhân của những hành vi đó lại không được đề cập đến. Cuốn sách này đã giải quyết tốt phần gốc mà tôi vẫn thắc mắc bấy lâu nay. Công trình nguyên cứu của tác giả có giá trị lớn, được tài trợ bởi những tập đoàn đa quốc gia để họ áp dụng trong kinh doanh nên tôi tin vào tính xác thực của nó.
4
57860
2015-06-27 22:22:36
--------------------------
173954
5447
Cuốn sách này, mình mới mua nhưng đã đọc xong. Có thể nói, một cuốn sách chứa nhiều thông tin thú vị và bổ ích cho ngành quảng cáo nói chung, đặc biệt là dành cho ngành quảng bá thương hiệu nói riêng (một khái niệm tương đối còn ít nhiều lạ đối với Việt Nam). Những người làm nghề kinh doanh/bán hàng, thì cuốn sách sẽ cung cấp khá nhiều thông tin để giúp bản thân nắm bắt được tâm lý khách hàng nói chung, cũng như chuẩn bị gì để khiến khách hàng móc hầu bao. :d Đối với những người làm marketing thì đây là một cuốn sách rất hay giúp ích nhiều cho sự sáng tạo, mở mang thêm kiến thức chuyên ngành. Đọc xong cuốn sách, có thể thấy được, quyết định mua hàng phần lớn không nằm ở ý thức mà nằm ở tiềm thức, khi mà quyết định mua hàng của chúng ta đa phần chỉ được quyết định chóng vánh trong vòng 5 giây.
5
204398
2015-03-27 11:09:14
--------------------------
163831
5447
Tôi là một nhân viên bán hàng. Và sau khi thực hiện rất nhiều giao dịch với khách hàng, tôi nhận ra một điều: Đại đa số khách hàng mua hàng bằng cảm xúc chứ hoàn toàn không có chỗ cho lý trí trong quyết định mua hàng của họ. Khách hàng không bao giờ mua những gì họ cần, họ chỉ mua những thứ họ muốn mà thôi. Rất nhiều người ảo tưởng rằng bản thân mình là người tiêu dùng thông minh nhưng thực ra không phải vậy. Và câu hỏi cũng chính là tên của quyển sách này: Điều Gì Khiến Khách Hàng Chi Tiền? Trong quyển sách này, tác giả Martin Lindstrom đã cho chúng ta biết câu trả lời cho câu hỏi luôn làm đau đầu rất nhiều Marketer: Tại sao và làm thế nào để khách hàng chi tiền và trung thành với thương hiệu?
5
387632
2015-03-05 22:52:56
--------------------------
160123
5447
Đôi khi bạn sẽ có những quyết định khi mua một loại hàng hóa mà chính bản thân mình cũng không hiểu vì sao.. Một quyết định mua sắm được đưa ra rất nhanh - chỉ trong vòng 2 giây, vậy nó là do cảm xúc hay lý trí?
Quyển sách là một công trình nghiên cứu về tâm lý mua hàng. Bạn sẽ hiểu được các thương hiệu đang dùng chiêu gì để dụ dỗ bạn, và tiềm thức của bạn mách bảo bạn ra quyết đình. Âm thanh, hình ảnh, màu sắc cũng ảnh hưởng rất nghiêm trọng đến bạn.
Quyển sách này phù hợp cho những ai đang làm trong lĩnh vực PR hay marketing. Nó cũng đặc biệt thích hợp cho phụ nữ thích mua sắm thả ga mà những thứ mua được đôi khi về vứt xó.
Chúc các bạn đọc sách hiệu quả
4
341512
2015-02-22 11:24:12
--------------------------
