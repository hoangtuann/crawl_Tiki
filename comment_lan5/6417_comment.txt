461457
6417
Mình muốn tự học tiếng anh ở nhà nên mua cuốn này tham khảo.Cuốn này khá là hay,nội dung hữu ích.Có nhiều chủ đề khác nhau với in hình minh họa nữa,cuối mỗi bài đều có ghi từ mới,chú thích và cách phát âm rõ ràng.Lấy từ những bài báo nên vừa học tiếng anh mà vừa biết thêm thông tin có ích nữa.Chỉ có một điều mình không thích là chữ in quá nhỏ nên mình nhìn không rõ lắm với có những đoạn văn có phông chữ hơn khó nhìn nên mình nhìn lâu thấy không thoải mái lắm
4
1060192
2016-06-27 20:27:50
--------------------------
407260
6417
Trong đợt hội sách online vừa rồi của tiki mình có chọn mua cuốn sách này vì giá được giảm khá hợp lý. Mình đã làm hai cuốn tập 1 và tập 2 của bộ sách này. Sang tập 3 thì cuốn sách vẫn giữ nguyên biên soạn kết hợp 4 kỹ năng nghe nói đọc viết cùng phần ngữ pháp, từ vựng, phát âm. Với mỗi kỹ năng sách đều có phần bài tập đa dạng, nhiều từ mới được giải thích cặn kẽ. Cuối sách có phần đáp án để đối chiếu. Đây là cuốn sách rất thích hợp cho việc tự luyện tiếng anh tại nhà.
5
954169
2016-03-29 19:44:06
--------------------------
121479
6417
Head way là cuốn sách tiếng anh hay mình thấy cũng được về đọc hiểu.Sách biên soạn gồm nhiều bài báo nước ngoài được viết hoàn toàn bằng tiếng anh cho ta cái nhìn khách quan về mọi ngữ pháp, văn phạm trong cách viết.Tác giả biên soạn kĩ càng có nhiều từ vựng và chú thích ở mọi nơi trong sách.Bìa đẹp, nhưng nếu nó được in màu bên trong thì mình sẽ thấy hấp dẫn và lôi cuốn hơn.Nhưng nhìn chung thì sách cũng tạm ổn, nếu trình độ của bạn đã vượt qua lớp 11 rồi thì không cần làm chi.
4
338141
2014-08-19 21:02:10
--------------------------
