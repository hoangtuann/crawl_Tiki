403686
4077
Phải nói là mình rất khâm phục nhà sách alphabook vì dự án sách Góc nhìn sử Việt. Cho đến thời điểm hiện tại dự án đã ra mắt được 40 cuốn, toàn là những tài liệu giá trị, quyển sách này chắc chắn cũng không phải là một ngoại lệ. Nội dung cuốn sách xoay quanh nhân vật Trịnh Thức (con của Trịnh Bồng-vị Chúa Trịnh cuối cùng), qua ngòi bút tài hoa của mình, tác giả đã phần nào lột tả được bối cảnh lịch sử thời bấy giờ, cũng như khắc họa một cách chân thực, xúc động mối quan hệ Vua-tôi, cha-con. Truyện hấp dẫn bởi lối kể tự nhiên cùng với những đoạn tả cảnh thiên nhiên khiến cho nội dung lịch sử không bị khô khan nhàm chán. Cá nhân mình rất thích cuốn sách này, chỉ hơi tiếc một chút là quyển sách hơi ngắn.
4
1177538
2016-03-24 07:17:49
--------------------------
326357
4077
Giai-đoạn Lê Trịnh có lẽ là giai-đoạn ít sử-liệu nhứt trong lịch-sử nước nhà. Phần vì nội-loạn, bảy lần đánh nhau của Đàng Trong - Đàng Ngoài, phần vì Tây-sơn nổi dậy, Thanh-triều xâm lăng, sử sách ít nhiều bị thiếu sót. Tiểu-thuyết này thuật lại một phần nhỏ trong bối-cảnh đó, khi Tây-sơn tiến ra Bắc-hà, lúc Chiêu-thống sang Tàu cầu-viện. Chỉ đơn-thuần nói về cha con Yến-đô vương, từ lưu-lạc trong dân-gian đến sum họp nơi miền sơn-thổ, lại kể về chuyện tình đẹp như mộng của Trịnh-Thức và Nhược-Lan. Tác-giả đứng trên lập-trường của kẻ bại-trận trong thanh-sử mà viết, thực là chuyện xưa nay hiếm. Ngẫm lẽ, trong cái bạo-tàn của chiến-sự cũng nồng chảy lửa ái-tình, và dầu là kẻ chiến-bại, vẫn mang nghĩa-khí của bậc thức-giả Nho-gia thời bấy giờ. Hẳn độc-giả có xem qua, cũng vương-vấn đôi chút về mối tình cầm-sắt của họ, cũng thấu-cảm cho cha con họ Trịnh khi thiên-mệnh chẳng còn, và cũng suy-tưởng về thế-sự quốc-gia lúc cảnh loạn-ly chiến-cuộc. 
5
34195
2015-10-25 15:11:02
--------------------------
324549
4077
Mình rất thích bộ Góc nhìn sử Việt này, phần vì nó cung cấp nhiều tư liệu hay và thú vị mà đó giờ mình chưa tiếp cận, đặc biệt là ở giai đoạn Lê - Trịnh này, phần nữa là vì cách viết của các tác giả hết sức mộc mạc và giản dị, dễ đọc dễ hiểu. Đối với tác phẩm Giọt máu sau cùng này cũng vậy, đó là câu chuyện kể về Trịnh Bồng cùng con, kể về sự suy tàn của một gia tộc từng làm mưa làm gió khắp Bắc Hà suốt hơn 200 năm, ắt hẳn câu chuyện đó không kém phần bi thương mà khiến bạn đọc chau mày. Với lối viết nhẹ nhàng nhưng không hề kém sức hấp dẫn, tác giả sẽ dẫn bạn đi suốt tấn bi kịch đó, tấn bi kịch song hành với một thời đại hào hùng tiếp sau đó, tấn bi kịch của nhân thế nói chung khi cực thịnh thì ắt sẽ tận.
4
564333
2015-10-21 14:14:11
--------------------------
207188
4077
Góc nhìn sử Việt là bộ sách "đáng kinh ngạc" của Alphabooks, vì những giá trị và nội dung phong phú ở nhiều cuốn sách trong bộ sách này mang lại. Tôi đã mua cuốn Giọt máu sau cùng này vì tò mò. Bởi tiểu thuyết lịch sử viết về giai đoạn lịch sử này khá hiếm.

Câu chuyện bắt đầu từ việc Trịnh Bồng nổi lên ép vua Lê Chiêu Thống phong làm Yến Đô Vương để kế nghiệp vương nghiệp của các Chúa Trịnh. Những âm mưu thoán đoạt, mưu hại, những mối quan hệ chồng chéo, ràng buộc nhau trong chốn cung vua phủ chúa được tác phẩm miêu tả khá sinh động. Người đọc dễ hình dung bối cảnh loạn lạc thời bấy giờ.

Theo tôi nghĩ, Giọt máu sau cùng là một cuốn sách nên có trên tủ sách của những người trẻ. Học lịch sử qua trang tiểu thuyết thật ra thú vị hơn nhiều người lầm tưởng!
4
598102
2015-06-11 17:23:00
--------------------------
