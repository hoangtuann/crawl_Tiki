205676
3633
Quyển sách nên đọc với các bạn thích đi và trải nghiệm (hay có thể dùng một từ đang hot là Phượt) - Nhật kí hành trình xuyên Châu Mỹ La tinh bằng xe gắn máy của chàng trai Ernesto Che Guevara, 23 tuổi, tài hoa, lãng tử, đôi chút ngang tàng và thích phiêu lưu mạo hiểm - một người sinh ra ở Argentina nhưng đã nghĩ mình thuộc về thế giới này và đất nước Argentina chỉ là nơi sinh ra anh. Đây không phải là câu chuyện về một Che Guevara huyền thoại, một lãnh tụ cách mạng đã trở nên bất tử khi bỏ lại sau lưng những vinh quang sau thành công của cách mạng Cuba để bắt đầu con đường cách mạng ở một đất nước Châu Mỹ La tinh khác và hy sinh ở tuổi 39. 

Đọc Quyển Nhật ký khiến tôi tự hỏi có phải cứ đi được nhiều nơi có phải là hữu ích, liệu thời gian chỉ vài ba ngày của những chuyến đi có đủ để khám phá một vùng đất, con người, thiên nhiên và những gì riêng nhất hay chỉ chạm được đến những cái bên ngoài, bề nổi nhất của vùng đất ấy mà thôi?

Hãy đi để từng bước trưởng thành, đi để khám phá cuộc sống, đừng đi chỉ vì muốn làm đầy danh sách địa điểm đã ghé (nhưng không đọng lại được điều gì ngoài những thứ được sự hỗ trợ của công nghệ).
5
24152
2015-06-07 11:56:03
--------------------------
