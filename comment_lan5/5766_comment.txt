256220
5766
Tôi đã đọc nhiều bài nghiên cứu của GS.Trần Quốc Vượng, ông là một trong Tứ trụ nghiên cứu về nền văn hóa nước nhà. Và quyển sách này là tổng hợp các bài viết, tiểu luận nho nhỏ của ông về các lĩnh vực bao quát một nền văn hóa. Ông là người đi rất nhiều nên văn phong và cách suy nghĩ vô cùng sâu sắc và mang sức gợi mở rất lớn. Tôi hi vọng sẽ có nhiều tuyển tập và tái bản các công trình của Trần Quốc Vượng. Về hình thức quyển sách thì Nhã Nam là một nhà xuất bản có uy tín nên không có sai sót về lỗi chính tả.
5
418013
2015-08-06 17:57:39
--------------------------
216044
5766
Quyển sách viết về văn hóa Việt Nam với những truyền thống, phong tục tập quán, như một sự khẳng định nền văn hóa người Việt với bè bạn năm châu, cũng như góp phần khơi dậy lòng tự hào, tự tôn dân tộc của cộng đồng người Việt trong nước cũng như đang định cư tại nước ngoài.
Tất cả những truyền thống văn hóa theo chiều dài lịch sử như trống đồng, về những công trình, những vị anh hùng, các loại bánh trái của người Việt từ ngàn xưa đều gom tụ vào quyển sách này. 
Đọc nó, tôi không khỏi tự hào về người dân quê mình, đồng thời tưởng nhớ đến những người đã khuất, đã dựng nước, giữ nước!
Tôi sẽ mang theo bên mình khi có dịp gặp gỡ người nước ngoài. Tôi sẽ tự hào giới thiệu cho họ về những truyền thống văn hóa dân tộc!
5
616731
2015-06-27 08:28:02
--------------------------
