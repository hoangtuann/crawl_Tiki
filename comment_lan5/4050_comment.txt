431566
4050
Xem hết 4 cuốn và thật sự rất ngạc nhiên khi phát hiện có nhiều truyện không hề xuất hiện trong cả 4 cuốn. Tôi cứ nghĩ đây đã là trọn bộ rồi chứ, sao lại có truyện không xuất hiện được nhỉ?
Về hình thức truyện thì rất ok, giấy tốt, màu cũng đẹp, cầm lên cảm giác rất thích, nếu chất lượng dịch có thể hay hơn, phong phú cảm xúc hơn thì đã có thể cho full 5 sao rồi. Đáng tiếc! @@
Không biết xui hay sao mà cuốn tôi mua bìa cán không được chuẩn lắm, để lại mấy vết bong bóng nhìn khá là chướng mắt. :((
4
104893
2016-05-17 16:57:32
--------------------------
399028
4050
Mình đã đặt mua tập 1 trên Tiki, rất hay và thú vị. Cách viết lời thoại của tác giả rất hóm hỉnh, phù hợp với các lứa tuổi. Hình vẽ cũng dễ thương, ngộ nghĩnh. Vì vậy mình quyết định đặt mua trọn bộ Giải mã nhóm máu tập 2, 3, 4. Đọc mấy cuốn truyện này cùng bạn bè rất thú vị, vừa đọc vừa suy diễn đặc điểm của các nhóm máu và bạn bè của mình ngoài đời, thấy rất đúng ^^ Tiki giao hàng cũng rất nhanh, vì đặt sách nên mình háo hức nhận sách để đọc luôn, cảm ơn Tiki nhé!!!
4
533238
2016-03-17 08:28:41
--------------------------
387538
4050
Giải Mã Nhóm Máu (Tập 4) của nhà xuất bản Kim đồng có giá 45k, giá tiki bán ra là 36k nên mình mới quyết định mua, chứu 45k thì đắt thật. ^^ Truyện rất hay, kể về 4 nhóm máu A, B, O, AB. Mỗi nhóm máu có một tính cách đặc trưng không trộn lẫn. Mình là người nhóm máu O nên khi đọc truyện mình thấy có những cái nói khá đúng về những người nhóm máu O, nhưng cũng có lúc không đúng. Nhưng nhìn chung truyện hay và thú vị, màu sắc đa dạng. Hài Lòng !
4
854660
2016-02-27 09:34:30
--------------------------
193565
4050
"Giải mã nhóm máu" là một trong những quyển truyện mà mình đã mong chờ từ rất lâu, khi mà vnsharing còn đăng tải từng chap hàng tuần. Những mẩu chuyện hài hước xoay quanh 4 nhóm máu không chỉ mang lại tiếng cười cho mình mà còn giúp mình có những cái nhìn mới về tính cách từng nhóm máu. 4 nhóm máu, 4 tính cách khác nhau, lém lỉnh nhưng không kém phần thân thuộc và rất thật. Sách truyện đặt trên Tiki trước giờ đã ok rồi, giá truyện rẻ và chất lượng khá tốt. Nói chung mình rất hài lòng. Mong những quyển sách sắp đạt tiếp theo của mình cũng vậy ^^
5
321692
2015-05-07 23:43:13
--------------------------
