399619
3830
Là một trong số các tác phẩm thuộc series KGI của Maya Banks, tôi thực sự đánh giá cao về nội dung và cả hình thức cuốn sách do BV xuất bản lần này. Từ đầu đến cuối truyện là một loạt các tình tiết gay cấn đến nghẹt thở, đan xen không ít những giây phút ngọt ngào lãng mãn của hai nhân vật chính Shea và nathan, những cảnh đầy không khí đầm ấm của gia đình. Tôi thực sự mong BV sẽ xuất bản đủ series KGI này thay vì cứ xuất bản kiểu nhảy cóc vài cuốn giữa series thế này, vì các nhân vật trong bộ truyện đều có mối liên hệ rất chặt chẽ với nhau.
5
954169
2016-03-17 23:27:32
--------------------------
325463
3830
Truyện mang một chút yếu tố kì ảo nhưng nội dung thì vẫn có nét hấp dẫn và lôi cuốn. Shea-Nathan, cặp đôi có khả năng thần giao cách cảm kì lạ và chính yếu tố ấy đã đưa họ đến với nhau, tuy nhiên khi đọc xong truyện này thật sự không để lại cho tôi ấn tượng gì nhiều, có lẽ tôi thích hợp với những thể loại sách mang tính thực tế hơn là kì ảo.

Series về KGI là một bộ truyện điển hình cho dòng văn học hiện đại của M.Banks, thế nhưng tôi lại có hứng thú với thể loại lãng mạn lịch sử của bà hơn, ở thể loại này bà viết vô cùng chắc tay và nội dung cũng cực kì hấp dẫn.

4
41370
2015-10-23 16:34:50
--------------------------
285330
3830
Trong KGI series về nhà Kelly, mình mong chờ ở Việt Nam sẽ xuất bản thêm các cuốn khác về anh em Ethan, Sam hay Donovan... (đọc trailer tiếng Anh thấy rất thú vị), đặc biệt về Ethan, vì từ cuốn "Chạy trốn và yêu thương" đến "Lời thì thầm trong đêm", các anh em nhà Kelly thường nhắc về tấm gương của ng chị dâu Rachel - vợ của Ethan nên mình rất tò mò về câu chuyện của 2 người này.
Đối với tình tiết của "Lời thì thầm trong đêm", tình yêu của Nat và Shea có vẻ hơi nhanh quá, biết là Shea cứu Nat, nhưng để đạt mức yêu hy sinh bản thân thì chưa thấy đến mức đó, chắc do tác giả muốn truyền tải tình yêu của họ xuất phát từ sự hòa hợp tâm linh, một mối duyên tâm linh giữa hàng triệu tâm linh, cô lại liên hệ và cứu giúp anh....
Tác giả chưa đi sâu lắm vào tổ chức thí nghiệm chị em Shea-Grace, việc giải cứu Shea cũng không li kì như mong đợi của mình (chắc do mình xem phim hành động nhiều quá).
Sách đẹp, chất lượng in tốt, không có lỗi chính tả, nội dung không bị dài dòng và thừa thãi câu chữ. Mình đã mua các cuốn đã xuất bản của Maya Banks, hy vọng sẽ được đọc thêm những quyển khác nữa vì search trên Google thì bà í viết rất nhiều series khác nhau, mà toàn được xuất bản bên nước ngoài hết rồi!
4
170711
2015-08-31 18:57:29
--------------------------
248147
3830
Mình khá thich phong cách của Maya Banks. Mình thích Shea với sự dũng cảm và sẵn sàng hy sinh của cô. Mặc dù vậy, tuy chủ đề chính trong tác phẩm là sự thông kết nối về mặt tâm hồn của 2 người mà những người khác không hề có được, nhưng theo mình thì khía cạnh sự thấu hiểu đến tâm hồn của nhau thật sự chưa được làm rõ cho lắm, vì mình vẫn chưa cảm nhận được tâm hồn của cả 2 nhân vật chính. Có vẻ như dòng văn học lãng mạn hiện đại là dòng chủ lực của Maya Banks, nhưng thật sự thì mình thích dòng văn học lãng mạn lịch sử của bà hơn.
4
258176
2015-07-30 18:00:02
--------------------------
214638
3830
Có vẻ "dễ thương" không thích hợp lắm cho câu chuyện tình cảm lãng mạn pha chút hành động này, nhưng mà đọc xong mình chỉ có thể thốt lên câu "Dễ thương quá đi à!" :D
Mình đánh giá cuốn này cao hơn Trốn Chạy Và Yêu Thương (mặc dù về cái bìa thì vẫn khó đỡ ngang ngửa như ngày nào). Truyện có yếu tố siêu nhiên, nhưng cái năng lực siêu nhiên ấy lại là bi kịch cho Shea và Grace, không biến họ thành anh hùng, cũng chẳng làm họ sống tốt hơn mà đẩy họ vào đường cùng, bị săn đuổi và phải trốn chạy. Tuy vậy, ở một nơi nào đó, với vài người nào đó, Shea và Grace lại là những thiên thần giúp họ vượt qua khốn cảnh, giành lại sự sống và tự do.
Mình thích cuốn truyện này bởi những góc độ tâm lý mà tác giả đặt Nathan vào - điều vốn không quá nhiều trong dòng tiểu thuyết phương Tây. Nó không dài, không nhiều nhưng đủ để mình hình dung được suy nghĩ, ám ảnh và khó khắn mà anh gặp phải sau khi trải qua một biến cố lớn - nó chẳng liên quan đến việc anh là một quân nhân, nó đơn giản là trạng thái tâm lý mà thôi.
Cuốn truyện này có lẽ không đến mức khuyên đọc, vì dù sao thì nó cũng là một cuốn sách giải trí thôi, nhưng có lẽ là một trong số ít truyện Tây gây được ấn tượng với mình.
4
17877
2015-06-25 11:34:52
--------------------------
205105
3830
Trong phần tiếp theo này của bộ truyện mình đã cảm nhận được tình yêu giữa hai nhân vật chính sâu sắc hơn.
Vì Nathan mà Shea tự đưa mình vào nguy hiểm. 
Vì Shea mà Nathan vẫn như ở trong tù ngục dù đã thoát khỏi ngục tù và đang sống trong sự bao bọc yêu thương của gia đình. 
Tác giả đã đưa ra một lý do mà Shea và Nathan phải thuộc về nhau (Các bạn đọc và khám phá nhé, đã có lúc mình phải rơi nước mắt). 
Và một điều mà xuyên suốt câu chuyện mình luôn mong đợi đó chính là tin tức từ anh Rio, người mà đã đùng đùng bảo là sẽ tìm chị gái của Shea. Tuy nhiên đến hết truyện anh này vẫn biệt vô âm tích. Mình rất rò mò về chị gái của Shea và hy vọng là sẽ sớm xuất bản phần tiếp theo để giải mã bí ẩn này.
4
181887
2015-06-05 20:12:23
--------------------------
201426
3830
Dù có đôi phần ảo diệu nhưng theo nhận xét cá nhân của bản thân thì mình đánh giá khá cao tác phẩm này của Maya Banks (theo mình thì hay hơn cuốn trước cùng series là "trốn chạy và yêu thương", truyện có kết cấu hợp lý, tình tiết đan xen cũng khiến người đọc khá "nghẹt thở" nhất là năm lần bảy lượt hai nhân vật chính chịu tra tấn khổ ải, tuy không đi vào quá nhiều tình tiết như mấy truyện sát nhân trinh thám vì dù sao "Lời thì thầm trong đêm" cũng là một tác phẩm lãng mạn, nhưng Maya Banks vẫn lột tả được hết những khó khăn gian khổ mà Nathan và Shea phải chịu đựng. 
Truyện trong series KGI nên dù ít dù nhiều mỗi tác phẩm tuy riêng biệt nhưng vẫn có liên quan mật thiết đến nhau, việc vẫn chưa tìm thấy Grace - chị gái Shea khiến mình vẫn bị bứt rứt không yên. Hy vọng BV có thể chuyển ngữ cuốn tiếp theo trong series này của Maya Banks.
Phần dịch thuật và in ấn mình khá hài lòng, đặc biệt tác phẩm có phần dịch rất trau chuốt và cẩn thận. 
Mình kiểu người khá phù phiếm ưa hình thức nữa nên cũng có đôi lời muốn cảm ơn đến dịch vụ của tiki, sản phẩm giao đến tay mình còn đẹp và cẩn thận hơn mình tự ra chọn ở nhà sách nữa. Cảm ơn Tiki nha.
4
125574
2015-05-27 12:38:57
--------------------------
