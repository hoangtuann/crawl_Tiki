300303
4477
Bảy năm ở Paris là câu chuyện kể của một cô gái Việt sống ở Paris từ năm 27 tuổi. Tuy nhiên, xuyên suốt cuốn sách không chỉ là "Bảy năm ở Paris", mà đan xen vào đó còn là những tháng năm Hà Nội thời bao cấp hay không gian của Sài Gòn mộc mạc. Cảm nhận về Paris, về con người, cuộc sống ở Paris được lồng trong cái nhìn tinh tế và đằm thắm, Paris không hiện lên một cách hoa lệ, mà gần gũi bởi sự cảm nhận của chính tác giả. Không phải là cuốn sách viết về những chuyến đi, Bảy năm ở Paris là những mẩu chuyện về cuộc sống, là cảm nhận về một vùng đất mà tác giả có sự gắn bó, đã trở thành một phần cuộc sống của chị. Cũng chính vì xuất phát từ cảm nhận chân thành nên câu văn, lỗi dẫn dắt rất nhẹ nhàng và mạch lạc.
5
287238
2015-09-13 21:10:06
--------------------------
187071
4477
Trước mình đã đọc Hồi 1 Bỏ nhà đi Paris ra năm ngoái, nay đọc tiếp phần còn lại thấy mảnh ghép Hồi 2 Sống và yêu đã dựng nên ngôi nhà vững chãi Bảy năm ở Paris. Nhẹ nhàng mà cảm động, chuyện Paris xen lẫn với những ký ức thời bao cấp ở Hà nội, nơi tác giả lớn lên. Những suy tư, trăn trở rất thơ mà cũng rất thực. Mình đã sống ở Paris mấy tháng và thấy cách nhìn của chị Camille Thắm Trần về Paris tinh tế, không khoa trương hoa mỹ mà chân thành, đằm thắm. Phần chị viết về việc "kiếm chồng" làm mình khá choáng... đọc xong gấp cuốn sách lại, thấy âm hưởng còn ngọt trong cổ họng như vừa uống một chén trà ngon.
5
585073
2015-04-22 13:13:03
--------------------------
186189
4477
Rất thích đọc sách về du lịch, các vùng đất trên thế giới, tôi đã đọc cuốn Bảy năm ở Paris của chị Camille Tham Tran. Chuyện được bố trí như một vở kịch, gồm hai hồi chính: Hồi 1 Bỏ nhà đi Paris và Hồi 2 Sống và yêu. Câu chuyện không chỉ kể về Paris mà còn đầy kinh nghiệm sống. Một cô gái Việt đã học hỏi, khám phá và thể hiện bản lĩnh của mình thế nào ở trới Tây, hay nói rộng ra là trong môi trường quốc tế... Tôi bắt đầu đọc sách và đã muốn đọc một mạch đến cuối vì hấp dẫn, tôi thích giọng văn nhẹ nhàng và hài hước của chị Camille Thắm Trần.
4
615489
2015-04-20 19:59:55
--------------------------
