473093
4830
Viết lại câu tiếng anh rất đa dạng, vì thế mình quyết định lựa chọn quyển sách này. Sách có tổng hợp các dạng viết lại câu, từ dễ cho đến khó, phù hợp với mỗi trình độ tiếng anh của mỗi người. Mỗi câu bài tập là một dạng viết lại khác nhau, giúp hiểu sâu được phương pháp viết lại dễ dàng và chính xác nhất, đặc biệt còn ôn tập được vốn từ ngữ Tiếng Anh hạn hẹp của mình. Và hơn hết, mình tin tưởng sách Windy, sách bao giờ cũng đầu tư chất lượng, giấy in dày, chữ được in rõ ràng dễ đọc dễ hiểu.
5
810918
2016-07-10 11:19:46
--------------------------
400129
4830
Đúng là viết lại câu tiếng Anh không hề đơn giản, nhất là với những bạn học yếu, thiếu vốn từ. Cuốn bài tập sẽ giúp bổ trợ phương pháp học tốt hơn, nâng cao và rèn luyện khả năng viết tiếng Anh. Nói chung sách hay và bổ ích, trình bày đẹp, chất lượng mà nếu so với các sách giáo dục khác cũng là rất tốt, giá cả hợp lý. Bây giờ mình còn thích mua sách giáo khoa trên Tiki hơn là mua ngoài hiệu, bởi vì Tiki phục vụ khách hàng chu đáo và đem đến cho mình niềm tin rất lớn.
4
386342
2016-03-18 20:46:21
--------------------------
397171
4830
Nội dung sách rất hay! Phù hợp với những ai sắp sửa thi THPT Quốc Gia như mình. Nhất là phần luận, chỉ có 0,5 điểm thôi nhưng nó gây khó dễ cho rất nhiều người! Mình nghĩ các bạn nên mua cuốn sách này, nó củng cố kiến thức về cấu trúc câu, giúp bạn tự tin bước vào phòng thi. Mình mua cuốn này với cuốn Những từ dễ nhầm lẫn trong tiếng anh cùng tác giả, hai cuốn này rất có ích! Hy vọng là MC Books sẽ cho ra mắt nhiều sách tự học tiếng anh như thế này để những ai gặp khó khăn với tiếng anh sẽ không cảm thấy khó! Còn giá thì khỏi bàn rồi, giá ở Tiki rất ổn,mình đã khảo sát ở nhiều trang bán sách online và mình thấy giá Tiki rất rất ổn.
5
541939
2016-03-14 15:58:49
--------------------------
373396
4830
Cuốn sách bài tập này thực sự rất cần thiết cho những bạn học sinh đang gặp khó khăn vè khoản viết lại câu tiếng anh.
Vì bài kiểm tra tiếng Anh bị điểm kém mà thầy đã khuyên mình mua nó. Mới đầu mình không quan tâm lắm, nhìn qua thấy toàn là chữ, không thấy hình ảnh hay tranh vẽ nào khiến một đứa lười học như mình nhìn vào mà hai mắt cứ díp lại. Thế nhưng dưới sự thúc giục của thầy giáo "mỗi ngày phải đem nộp bài cho tôi kiểm tra, nếu không mời cậu gọi phụ huynh đến", mình mới cố kiết làm cho xong.
Nghe nói vậy chứ thực ra trong quá trình mình làm bài tập cũng cảm thấy bị thu hút. Cuốn sách bao gồm nhiều bài tập với các dạng đầy đủ và cần thiết, có cả đáp án của mỗi phần, khi mình làm xong thường đối chiếu, dần dần mấy chữ ấy cũng tự nhiên nằm ở trong đầu. Kết quả là mình đã vượt qua kì kiểm tra một cách mĩ mãn. Nghĩ đi nghĩ lại, chắc phải mua thêm mấy quyển bài tập tiếng anh nữa về làm thôi! Hì hì...
4
857178
2016-01-23 17:58:01
--------------------------
368383
4830
Quyển sách thực sự hữu ích. Theo mình nghĩ dạng bài của sách được xếp từ dễ đến khó để người làm dễ củng cố. Quyển sách thích hợp cho những bạn còn yếu phần writing, những bạn ôn luyện học sinh giỏi hay THPT cấp quốc gia. Trong sách còn có những hình vẽ rất đáng yêu nữa, giấy thuộc loại tốt. Cầm vào làm mình muốn làm ngay. Mình là fan của hãng The Windy lâu rồi nhưng mới chỉ mua được một vài cuốn sách thôi. Các bạn nên mua sách này nha, rất hữu ích đó.
5
676200
2016-01-13 21:46:05
--------------------------
339347
4830
Sau một hồi lựa chọn kĩ càng, mình đã quyết định mua cuốn sách này để luyện tiếng anh trong lúc rãnh rỗi. Và đây là một sự lựa chọn khá tốt.
Bài tập viết lại câu rất phong phú về dạng ngữ pháp lẫn nội dung. Mỗi câu đều chừa dòng để người làm trực tiếp làm lên sách rồi chỉnh sai đúng được. Nhưng có một khuyết điểm là sách không giải thích đáp án, mà chỉ đưa ra mỗi đáp án thôi, nên người làm phải tự tìm hiểu giải thích lỗi sai.
Sách khá dày, chất lượng giấy tốt.
Nhìn chung, đây là cuốn sách chất lượng tốt, lại giá thành khá rẻ.
4
115290
2015-11-17 21:42:21
--------------------------
338760
4830
Nói như vậy có nghĩa là mặt hạn chế của cuốn sách này chính là chỉ có giải chứ không giải thích rõ ràng là tại sao trong trường hợp như vậy lại viết lại câu như thế. Đơn giản là chỉ có đề và sau sách là có đáp án. Thực ra cuốn sách này có lẽ thích hợp cho những bạn luyện thi học sinh giỏi hơn thay vì những bạn sinh viên muốn luyện bằng tiếng anh như mình.Dù sao đi chăng nữa thì mình đã nhờ người bạn mua hộ mình cuốn sách này chắc từ hồi mình còn là sinh viên năm nhất.giờ thì chuẩn bị đi kiến tập rồi nên có lẽ cái gì chưa biết chắc phải hỏi thăm thầy cô cấp 3 quá.
3
571682
2015-11-16 21:10:20
--------------------------
338307
4830
Cuốn sách gồm nhiều bài tập với các dạng đầy đủ và cần thiết. Khi cầm cuốn sách trên tay mình rất hài lòng về nội dung của nó cũng như cách phục vụ giao hàng của tiki. Chất lượng giấy tốt, chữ in vừa phải, rất dễ nhìn. Nội dung phong phú và độ khó dễ khác nhau. Không những giúp mình củng cố lại kiến thức mà còn luyện cả chính tả và từ vựng. Rất cần thiết cho những bạn đang muốn ôn luyện ngữ pháp. Cuốn sách là một sự lựa chọn hoàn hảo dành cho bạn. 
5
797620
2015-11-15 21:29:22
--------------------------
334618
4830
Muốn học tốt tiếng anh thì kĩ năng viết là không thể thiếu, quyển sách này có nhiều dạng bài tập giúp ta rèn luyện kĩ năng, tuy nhiên nhược điểm của nó là không cho cấu trúc, thêm vào đó cách sắp xếp chưa có khoa học lắm, câu khó câu dễ lẫn lộn với nhau. Mình nghĩ để làm tốt bài tập tiếng anh ở đây, bạn nên học hỏi phần lý thuyết, cấu trúc ở những quyển sách khác để vận dụng làm các dạng bài tập trong quyển sách này nhằm khắc sâu thêm kiến thức
4
726338
2015-11-09 20:26:53
--------------------------
324069
4830
Quyển sách này có rất nhiều dạng bài tập về viết lại câu. Mỗi dạng lại được chia sang một phần khác nhau. Tuy nhiên, nó lại không có phần lý thuyết, phần cấu trúc ngữ pháp để chúng ta dựa vào làm. Phần đáp án cũng không có giải thích gì thêm. Vì vậy, quyển sách này sẽ thực sự hữu ích nếu như bạn đã nắm vững kiến thức về các cách viết lại câu. Cách trình bày của sách cũng sẽ khiến ta nhanh chóng chán nản khi không biết làm. Cần phải siêng năng để có thể làm tốt sách này.
3
837337
2015-10-20 12:48:15
--------------------------
310935
4830
Cuốn sách gồm nhiều bài tập với các dạng đầy đủ và cần thiết. Khi cầm cuốn sách trên tay mình rất hài lòng về nội dung của nó cũng như cách phục vụ giao hàng của tiki. Chất lượng giấy tốt, chữ in vừa phải, rất dễ nhìn. Nội dung phong phú và độ khó dễ khác nhau. Không những giúp mình củng cố lại kiến thức mà còn luyện cả chính tả và từ vựng. Rất cần thiết cho những bạn đang muốn ôn luyện ngữ pháp. Cuốn sách là một sự lựa chọn hoàn hảo dành cho bạn. 

5
357244
2015-09-19 20:40:22
--------------------------
236956
4830
Được bạn giới thiệu về sách anh văn của The Windy và mua về ôn luyện thử, mình cảm thấy rất hài lòng về sản phẩm. Các dạng bài tập viết lại câu trong cuốn sách khá là phong phú và đa dạng, phần luyện tập lại nhiều và xen kẽ trong từng mục bài tập. Cuốn sách đã giúp ích rất nhiều cho mình trong những kì thi đòi hỏi kĩ năng viết lại câu tiếng anh trong trường phổ thông, vì các dạng bài tập khá sát với chương trình học. Còn về chất lượng bản in, sách được in rõ chữ, mặc dù không phải loại giấy dày nhưng giấy vẫn sáng, chừa dòng vừa mắt, rất dễ trình bày bài làm và dễ đọc, rà soát lại đáp án sau khi làm bài xong.
5
195117
2015-07-22 08:59:54
--------------------------
234102
4830
Bài Tập Viết Lại Câu Tiếng Anh thực sự là một quyển sách khá bổ ích đối với mình!  Cuốn sách cung cấp cho mình 2 dạng bài bài tập viết lại câu hay thi nhất trong các kì thi quan trọng. Mình làm thì mình thấy ở mỗi dạng bài  lại có những kiểu câu khác nhau rất đa dạng và phong phú về các kiểu câu hay gặp, mức độ khó dễ khác nhau! Cách trình bày của quyển sách còn khá hợp lí và dễ đọc, có cả những hình ảnh rất sinh động để quyển sách bớt khô khan hơn! Tuy nhiên,  mình chưa hài lòng ở cách sắp xếp các bài tập này cho lắm! Trong quyển sách này có đưa ra rất nhiều bài tập khó dễ khác nhau, có những câu rất khó nhưng lại có những câu lại cực dễ. Tuy nhiên các câu khó dễ ấy lại được sắp xếp rất lộn xộn. Nếu như The windy sắp xếp các câu này theo trình tự từ dễ đến khó thì chắc chắn sẽ rất dễ cho mình để học hiệu quả. Hơn nữa, nếu quyển sách có bổ sung thêm cho mình phần kiến thức về cấu trúc câu hay gặp thì việc làm bài tập của mình cũng sẽ bớt khó khăn hơn rất nhiều
4
306288
2015-07-20 09:16:04
--------------------------
233068
4830
Viết lại câu, nghe đã không đơn giản chút nào. Từ những ngày đầu tiếp xúc với tiếng anh, mình đã rất vất vả để chăm chút cho bài tập viết lại câu. Mình mua cuốn này về cùng với rất nhiều cuốn sách khác, nhưng do tính thiên vị khó chữa mà luôn muốn làm những cuốn sách bìa đẹp và trình độ dễ trước, để cuốn này ở lại sau cùng. Nhưng sai lầm cũng đến với mình từ đó, khi mà giở trang đầu tiên của cuốn sách, như có gì đó thôi thúc mình và mình đã lao vào làm luôn liền tù tì mấy bài. Vốn kiến thức không có gì khó nhưng rất sâu, tổng hợp lại rất tốt, làm mình thấy rất hối hận khi không ưu tiên làm quyển sách này trước. Cho đến giờ, mình vẫn thấy đây là một cuốn sách đáng để học, đáng để mua.
5
676659
2015-07-19 14:31:03
--------------------------
215901
4830
Mình rất tin tưởng vào chất lượng các đầu sách của The Windy. Cá nhân mình thấy cuốn viết lại câu này của The Wind khá bổ ích đối với mình. Các dạng bài tập của The Windy đưa ra trước nay rất đa dạng và phong phú, không chỉ rèn kỹ năng làm bài mà còn cung cấp một lượng kiến thức rất phong phú, bổ ích, chất lượng mà hiệu quả. zkhông chỉ cung cấp các dạng bài tập về viết lại câu, cuốn sách còn cung câp rất nhiều kiến thức bổ ích về cả từ vựng và ngữ pháp. Một cuốn sách rất đáng mua 
5
417795
2015-06-26 22:52:34
--------------------------
195344
4830
Mình đã mượn cuốn sách này xem thử, mình thấy cuốn sách này rất hay. Sách phù hợp cho mọi người, học sinh cho tới những người đã đi làm có nhu cầu học tiếng anh đều rất hữu dụng. Sách cung cấp cho ta kiến thức về cả từ vựng và ngữ pháp, các cấu trúc câu và bài tập thực hành. Đặc biệt, cuốn sách do nhóm tác giả The WIndy rất uy tín viết nên mình rất yên tâm về chất lượng, mình đã mua nhiều sách của The Windy rồi. Đây là quyển sách giúp chúng ta học và ôn thi rất hiệu quả.
5
506920
2015-05-13 15:54:52
--------------------------
