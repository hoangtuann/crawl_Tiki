399257
3384
Đây là cuốn truyện Việt Nam mình khá thích. Nó không phải là một cuốn truyện thông thường, mà nó là một bài học sâu sắc về tham vọng, về ''gieo gió gặp bão''.Câu chuyện kể về nhân vật Nhiên một người có tài năng về viết lách và Quân một người dám giết bạn, người yêu của mình chỉ vì tham vọng. Tuy nhiên,mình không hiểu lắm, thấy hơi vô lý ở đoạn truyện đầu tiên khi nói về Lưu Dương. Mình nghĩ Lưu Dương là bạn của Nhiên chứ không nghĩ là Nhiên.Đọc các bạn sẽ thấy. Tuy nhiên bìa và chất lượng sách rất đẹp, tuy khổ hơi nhỏ và mỏng. Mình rất thích các chính sách hỗ trợ của Tiki. Cảm ơn Tiki
4
747943
2016-03-17 14:35:30
--------------------------
383449
3384
Tác phẩm Bẫy của Phạm Anh Tuấn có những điểm mạnh và cũng có điểm yếu. Điểm mạnh đó là văn phong tác giả cực chất, tôi có lời khen cho một tác giả trẻ mà câu văn sắc sảo, mạnh mẽ, phong phú và linh hoạt như vậy. Nói chung về điểm này tôi không có gì để chê cả. Cái hay thứ hai ở truyện của Phạm Anh Tuấn đó là anh đã khắc họa được một thế giới văn chương quá hiện thực, với những góc khuất cùng sự phức tạp mà có lẽ chỉ những người trong cuộc mới hiểu, đúng là chẳng thua gì showbiz.
Còn về nội dung, tôi nghĩ nó chưa đủ yếu tố cấu thành thể loại trinh thám nên tôi nhận xét trên phương diện một tiểu thuyết. Phần mở đầu tiểu thuyết khá tốt, có sự gợi mở và cuốn hút nên tôi đọc với cảm giác háo hức, nhưng sau dần tình tiết cứ đều đều, nó vẫn nhấn nhá, đi sâu vào các khía cạnh của sự khắc họa và miêu tả chứ chưa đạt được cao trào cần thiết. Tôi nghĩ đó là điểm yếu nhất của tác phẩm này. Còn các chi tiết kinh dị tôi thấy ổn. Dù sao tôi vẫn đánh giá sách 4 sao ủng hộ tác giả. Hy vọng những ấn phẩm sau của anh sẽ hoàn thiện hơn.
4
386342
2016-02-20 20:03:52
--------------------------
382084
3384
Tôi là người yêu thích văn chương, nên khi đọc tác phẩm này, nhận ra hiện thực đầy khốc liệt trong giới văn chương không khỏi cảm thấy có chút shock, chút bất an. Thế giới văn chương trong mắt tôi vốn từng rất đẹp, rất hào nhoáng, nhưng giờ ngẫm ra, đúng là thứ gì cũng có giá của nó. Có tài, nhưng không có thủ đoạn, không có nền tảng, chỗ dựa thì không thể đứng vững. Bẫy xoay quanh 5 nhân vật: Nhiên- chàng trai trong sáng, đầy tài năng, bị chính người mình yêu giết chết vì cái tài đó, Dương-một cô gái mang nỗi hận, mong muốn trả thù cho Nhiên; Hải- người yêu tha thiết Nhiên; Luân-người yêu Dương và Quân-một nhà văn và cũng là kẻ đã cướp đi mọi thứ của Nhiên. Những miêu tả tâm lý kịch tích, cộng với yếu tố ma mị liên quan đến bùa ngải khiến cho câu chuyện càng trở nên hư hư thực thực.
Tôi thấy buồn cho cái chết của Hải, anh yêu Nhiên, anh không chấp nhận được Lưu Dương, nhưng rốt cuộc vẫn vì cả hai mà chết. Hải chết, Nhiên cũng thực sự chết.
Nếu chưa xem phim Scandal: Hào quang trở lại, thì đây là một tác phẩm đủ độ gây cấn, đủ mùi tình cảm để đọc. 

4
291719
2016-02-18 15:51:52
--------------------------
333366
3384
"Bẫy" đối với tôi không còn đơn thuần chỉ là một quyển sách hay là một câu chuyện nữa, nó đã giúp tôi có cái nhìn khác về thế giới văn chương, về cuộc sống và ánh hào quang. "Bẫy" khiến tôi ám ảnh, không vì những cảnh ghê rợn như bao tiểu thuyết khác mà là vì sự thật tàn khốc của cuộc sống. Cuộc sống đã trở thành một cuộc chơi và cái ta đặt cược chính là mạng sống của mình.
Tôi thấy Quân rất đáng thương. Quân đã giết Nhiên nhưng tất cả đều vì sự khốc liệt của thế giới văn đàn, vì quy luật đào thải của cuộc sống. Quân đã trả giá cho tất cả những gì mà hắn làm nên - sự ám ảnh và cái chết đầy đau đớn.
Hải lại càng đáng thương hơn nữa. Tình yêu của anh dành cho Nhiên đã không được đáp lại. Anh sống trong nỗi cô đơn, dằn vặt. Cuối cùng, anh lại ra đi mà không hề biết được sự thật, rằng Lưu Dương - người con gái luôn ở cùng anh - là Nhiên - người mà anh yêu nhất.
Cái kết của tác phẩm, đối với tôi, không hẳn là có hậu. Quân đã trả giá, sự thật về tác phẩm "Chính em" và thế giới văn đàn được phơi bày. Nhưng niềm đam mê văn chương của một người như Dương đã hoàn toàn biến mất. Kết thúc này khiến tôi suy ngẫm rất nhiều. 
Tình yêu, thù hận, hào quang, tiền bạc. Tất cả khiến cho tài năng cả con người bị vùi dập. 
5
909317
2015-11-07 20:48:05
--------------------------
332930
3384
Tôi luôn đánh giá cao văn phong của Phạm Anh Tuấn, Tuấn có khả năng viết vượt trội hơn hẳn so với những tác giả khác cùng độ tuổi. Nội dung tác phẩm được Tuấn xây dựng rất tốt và chặt chẽ. Qua Bẫy, tôi nhận ra thế giới của văn chương - một nơi mà tôi cũng đang đứng, vẫn còn rất nhiều điều mà tôi chưa thể biết hết. Tôi không thích đọc tiểu thuyết Việt Nam, và Bẫy là một trong số ít những truyện tiểu thuyết Việt Nam mà tôi đánh giá cao về mặt văn phong lẫn nội dung.
Tuy nhiên, đối với tôi mà nói, những tình tiết trong truyện vẫn chưa đủ để khiến tôi bất ngờ. Hay nói cách khác, tôi có thể đoán được Lưu Dương chính là Nhiên, bùa ngải chỉ là giả và cả những tình tiết khác ngay từ những chương đầu. Cái lý do cho sự xuất hiện của tiểu thuyết "Bẫy" trong truyện khiến tôi không mấy hài lòng. Bởi, cái tôi muốn là sự trả thù, một sự trả thù có tính toán, một sự trả thù độc địa. Nhưng hóa ra, "Bẫy" xuất hiện, chỉ là để kéo Quân ra khỏi vũng lầy có thể tỏa ánh hào quang của thế giới văn chương. Điều đó khiến một kẻ hẹp hòi với kẻ thù như tôi cảm thấy hụt hẫng. Đương nhiên, đó chỉ là sở thích của riêng tôi mà thôi.
Bẫy không chỉ nói về một thế giới đầy mưu mô toan tính, trong đó còn có biết bao thứ triết lý về nhân sinh quan. Điều đó khiến một người lười đọc như tôi cảm thấy mệt. Khi mà cứ đọc được vài đoạn là lại thấy nhân vật cảm khái về sự đời, con người, về thế giới văn chương v v... Bởi tôi thích cách viết ngắn gọn, đơn giản, nhưng đủ ý hơn. Tôi muốn các tình tiết trong truyện có nhiều thêm một chút, bớt triết lý đi một chút, như vậy sẽ khiến người đọc duy trì được sự căng thẳng và hồi hộp, đồng thời giúp mạch truyện không bị dài dòng lê thê một cách không đáng có. 
Dẫu sao, Bẫy cũng là một bộ tiểu thuyết rất đáng đọc, đáng để mua, và đáng để ngẫm. Và tôi nghĩ rằng nó sẽ khiến cho nhiều người hài lòng, thậm chí là với cả những người khó tính đối với văn học Việt như tôi.
4
452718
2015-11-07 10:23:44
--------------------------
309910
3384
Mình vốn dĩ chẳng thích thể loại trinh thám, và hầu như trong tủ sách của mình, chỉ trừ sách của Tuấn, hầu như không còn bất kì quyển nào thuộc thể loại trinh thám cả. 
So với cuốn trước, tôi lại càng cảm thấy "Bẫy" hay hơn, cuốn hút hơn thậm chí là ám ảnh hơn. 
Tôi thích cách Tuấn viết về cái nghề viết đầy thị phi trong cuộc sống này, tuy tàn nhẫn, khắc nghiệt nhưng rất thực tế. 
Một cuốn sách khi bỏ tiền ra mua, chắc chắn sẽ không bao giờ cảm thấy hối tiếc. 

4
472775
2015-09-19 10:49:39
--------------------------
242280
3384
nội dung lôi cuốn, các nút thắt, tình tiết của câu chuyện cũng được tác giả xử lý tốt, kết thúc vẹn cả đôi đường nhưng hơi vội. Nét trinh thám của truyện rất rõ: một cuộc đấu mèo vờn chuột giữa hai nhân vật, và đương nhiên cũng có xuất hiện vụ án. Truyện có kha khá cảnh yêu đương đồng tính, các mối quan hệ của các nhân vật tương đối phức tạp, toàn tay ba tay tư, các đoạn tả cái chết hay đánh đấm thì không được tỉ mỉ như văn Mỹ, Nhưng ít ra mình cũng đọc được đến hết chứ không bỏ ngang như "Luật ngầm" hay "Biến mất" 
4
38833
2015-07-26 13:49:23
--------------------------
225077
3384
Mình rất ấn tượng với "Bẫy" nó không chỉ đơn giản là một câu chuyện, mà còn là một lời cảnh báo cho tất cả những ai đang có tham vọng trong giới văn chương. Tôi thích cái cách bạn tả nhân vật, nó chân chật và có phần lột được cái ác của một con người. 

Qua "Đánh đổi", đến với "Bẫy" tôi thật sự đã liệt kê Tuấn vào một trong các tác giả mua truyện không cần đọc giới thiệu. Vì tôi tin tưởng, nếu có ra nhiều tác phẩm khác, bạn cũng vẫn sẽ khiến cho tôi mua mà không cần suy nghĩ.

Tuy nhiên, có những điểm tôi đã đọc rất kĩ nhưng vẫn không thể nào hiểu được. Ví như, bạn tạo một nhân vật Lưu Dương ban đầu là bạn thân của Nhiên, và Hải yêu Nhiên nhưng lại không yêu Dương. Cách bạn diễn tả cho thấy hai nhân vật này song song với nhau trong một khoảng thời gian, trước khi chết, Nhiên còn yêu cầu Hải phải bên cạnh Dương cho đến khi tìm hết sự thật.

Nhưng cuối cùng, nhân vật Dương này lại là Nhiên chuyển giới thành. Nó hơi phi lí một chút, vì theo miêu tả, Hải ắt phải biết cả Dương và Nhiên trong cùng một thời điểm. Lúc trước, Dương là một cô gái không có tâm hồn độc ác, thù hận, nhưng sau cái chết của Nhiên, cô lại trở nên cực đoan, tìm đủ mọi cách để trả thù. 

Hải đã quá yêu Nhiên, để rồi khi Dương bên cạnh anh lại không thể nào yêu cô. Cái chết của Hải khiến mình khá bùi ngùi, chí ít, trước khi chết, anh biết được người con trai mà mình luôn thương nhớ chưa bao giờ rời xa. 

Còn Luân, anh đến với Dương bởi bản chất của cô làm anh như nhìn thẳng vào chính mình. Anh yêu cô, yêu người con gái với ánh mắt lạnh lẽo đầy sương mù ấy.

Đối với tôi, Quân quá tham vọng, và kết quả của sự tham vọng đó chính là nhân quả báo ứng. Con người, ai cũng thế, đều có mặt trái mà mặt phải. Phía sau cái vỏ ngoài rạng rỡ đó lại chính là tâm hồn của một con ác quỷ đội lớp người.

 Không hối hận vì đã quyết định ma quyển sách này. Cám ơn bạn.
5
144489
2015-07-09 20:00:28
--------------------------
215512
3384
Mình không ngờ tác giả còn trẻ mà viết được hay đến như vậy, vì thể loại truyện vừa trinh thám, vừa hành động lại vừa có tình cảm, tình thân mà viết được như thế thì không phải dạng vừa đâu. Đáng khen cho tác giả trẻ mà dám bước trên con đường riêng, tạo được dấu ấn riêng và rất cứng tay thế này. Tôi tin chắc rằng nếu Tuấn ra cuốn nữa, mình vẫn sẽ mua và ủng hộ vì quá tin tưởng "tay nghề" cậu ấy. Bìa sách cũng rất ấn tượng, không quá u tối, nhưng không xì tin quá đà. Tuyệt lắm, đáng tiền.
Cảm giác giống ý như đang xem 1 bộ phim thật ấy. Không biết đạo diễn Victor Vũ có nên "hỏi thăm" ý tưởng này không nhỉ?!
5
4397
2015-06-26 13:57:31
--------------------------
215434
3384
Tôi đã đọc cuốn tiểu thuyết đầu tiên của tác giả mang tên Đánh đổi. Xét về Bẫy, câu chuyện logic hơn, gay cấn và khốc liệt hơn. Tôi thích tình yêu trong đó. Nó đầy ám ảnh,hi sinh. giống như tác giả từng khẳng định, tuy mang lốt vỏ trinh thám, nhưng vẫn là câu chuyên tình. Mối tình tay ba trong đó rất đẹp và cũng rất đau đớn. Thế giới văn chương trong câu chuyện không còn đẹp như truyện ngôn tình mà tôi từng đọc. Câu chuyện hơi khô nhưng rất thấm.

Tuy nhiên, đoạn cuối có phần hơi khó tin nhưng vẫn có thể chấp nhận được. Hy vọng một câu chuyện khác từ bạn.
4
596812
2015-06-26 12:46:23
--------------------------
213533
3384
Tôi nghe lời giới thiệu về cuốn sách và quyết định mua cuốn sách này khi vừa ra mắt. Nó làm tôi rất ngạc nhiên và sốc khi không ngờ cuộc sống của những tác giả trẻ bây giờ. Tuy nhiên, điều tôi ấn tượng hơn là tình yêu và những tâm lý của người trong cuộc. Mối tình tay ba giữa Lưu Dương - Hải- Luân rất hay, ám ảnh, mãnh liệt. Nó khốc liệt đến mức tôi bắt đầu có cái nhìn khác về tình yêu hiện đại.

Nếu muốn tìm một cuốn sách về gay cấn, vừa hiện thực, vừa day dứt ám ảnh về một tình yêu, đây là cuốn sách dành cho bạn.
5
673924
2015-06-23 21:23:11
--------------------------
