501365
7260
Đây là 1 cuốn sách rất hữu ích và tội rất thích nó 
4
2019400
2016-12-28 11:57:32
--------------------------
373747
7260
Sách viết có thể gọi là khá tốt, chất lượng ổn, tuy vậy vẫn có một số bài văn hơi lạc đề. Ví như bài văn thứ hai lại ở trong cuộc thi UPU khiến mình có chút hơi hoang mang? Mình tự hỏi có phải là lạc đề tài không? Một số bài văn khác rất hay, giọng văn mượt và trau chuốt, ý tứ rõ ràng cụ thể, câu cú mạch lạc, rất phù hợp cho các bạn học sinh lớp 6 đang yêu thích học môn Văn.  Và đặc chị họ mình rất thích, thế nên mình sẽ cho 4 sao. Cảm ơn nhiều :)
4
1103537
2016-01-24 16:00:11
--------------------------
211108
7260
Mình đã rất thích quyển sách này ngay từ lúc đầu. Nó là tổng hợp của những bài viết đoạt giải cao. Những bài đoạt giải thì không có vấn đề gì thậmc hí có thể nói là đưa vào khá đầy đủ. Nhưng mình thật sự thất vọng ngay ở bài thứ hai. Chủ đề của cuộc thi Viết thư UPU lần 39 là “Hãy viết thư cho một vận động viên hoặc một nhân vật thể thao mà em ngưỡng mộ để nói Thế Vận Hội Olympic Games có ý nghĩa gì đối với mình” sao? Việc in ấn đề tai hoàn toàn sai khiến mình vô cùng thất vọng. Buồn cười hơn bài viết đoạt giải của chủ đề trên lại gửi Đạo diễn Trương Nghệ Mưu?! Điều đó trong đầu mình một câu hỏi người thực hiện quyển sách này thực sự có trách nhiệm với những gì mình viết hay không? Mình hy vọng quyển sách sẽ được in ấn xuất bản lại và thêm nhiều bài đoạt giải của Quốc tế hơn nữa!
2
502851
2015-06-20 08:55:43
--------------------------
