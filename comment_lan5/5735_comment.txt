356724
5735
Tôi tự nhận mình là đứa ngốc nhất trên đời khi đặt nhầm phần 1 của series sách này thành phần 2. Tôi cứ nghĩ mình sẽ bị đứt đoạn, giống như đang nhìn một toa đầu ko có đầu máy. Thế nhưng khi bắt đầu đọc, tôi hoàn toàn bị đắm chìm vào nó. Tác phẩm này khơi gợi tình yêu đối với ai cập cổ đại trong tôi. Từ trước đến giờ vẫn ngại đặt sách qua mạng thế này. Nhưng..gửi tới những bạn đọc tiki, đây là một cuốn sách đáng đọc với những ai thích ai cập nói riêng và tất cả những bạn còn e dè chưa dám đặt sách nói chung
4
979895
2015-12-22 12:06:06
--------------------------
250345
5735
Series Biên Niên Sử nhà Kane cho ta thấy rõ văn phong của Rick Riordan. Ông đã hoàn toàn xuất xắc khi kết hợp yếu tố thần thoại lịch sử và hiện đại.  Điều đó đã làm nên thành công cho bộ truyện. Series ghi chép về cuộc giải cứu thế giới của 2 anh em nhà Kane nhưng đồng thời cũng viết về chuyện tình yêu dễ thương của 2 người họ. Mình rất thích cách mà 2 anh em thay phiên nhau kể chuyện. Tác giả phải xuất sắc lắm mới làm rõ được tính cách các nhân vật . 
Mình thấy bìa sách không hẳn là quá đẹp nhưng mình tôn trọng công sức của nhà thiết kế bìa. 
5
323288
2015-08-02 10:15:00
--------------------------
