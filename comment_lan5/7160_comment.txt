572801
7160
Khá hài lòng với sách của tiki ,nhưng có quyển chắc là bọc lâu ngày nên giấy hơi ngả vàng :'( hơn nữa thái độ của shipper ... không biết mọi người thấy như nào chứ tôi thì thấy tệ vô cùng 😳 ngta mua hàng mà giọng điệu cứ như là ngta nhờ vả gì mình vậy 😠😠
3
5182770
2017-04-14 09:45:46
--------------------------
368799
7160
Là thành quả của sự nhận thức tư duy có thể là tự do góp phần tạo nên sức lôi cuốn con người thu nhận những đức tính hoa mỹ của vô vàn những con người như cánh đàn ông còn độc thân mà cũng có ích khi chỉ ta thấy ưu điểm và nhược điểm cần biết trước khi giao tiếp với khác giới như họ được , bởi thế nếu mỗi người có cách hiểu khác nhau thì cần phải thay đổi và hợp tác với họ mới mong có thể thành công nhanh chóng và hợp lý .
4
402468
2016-01-14 17:09:31
--------------------------
361850
7160
Đọc truyện của Lam Bạch Sắc có thể thấy giọng văn của cô rất hài hước gây nhiều tình huống buồn cười cho người đọc . Tình yêu giữa nam chính và nữ chính thật sự rất thú vị . Nhưng về việc giải quyết tình huống của hai người tôi thấy hình như tác giả không chú trọng lắm và giải quyết khá chóng vánh khiến tôi không hiểu rõ . Về nội dung truyện này đọc chủ yếu chỉ để cười chứ không có ấn tượng sâu sắc như kiểu đọc một lần là nhớ mãi mãi được.
3
337423
2015-12-31 19:23:24
--------------------------
256089
7160
Lúc đầu mới đọc tên mình thấy sao sao ak, những người trong công ty mình khi thấy quyển sách này ai cũng thắc mắc và tò mò về nó, chính mình cũng không nghĩ rằng nó là 1 câu chuyện. Mình đọc cảm thấy thú vị lắm, cũng rút ra được vài điều cho bản thân, nhưng sách này chỉ để giải trí và xả stress thôi. Nội dung chưa xâu sắc lắm.Có những đoạn rất hài hước, ngồi đọc rồi cười một mình. Cả mẹ của mình cũng thích lắm. Hi vọng sẽ có thêm nhiều sách như vậy trên trang Tiki.
4
366563
2015-08-06 16:23:40
--------------------------
216845
7160
Nghe tựa truyện hài hài, lại có bìa cứng nên vốn trông đợi một tác phẩm xuất sắc lắm (thường hay hay mới có bìa cứng mà). Nhưng mua về và đọc hết 2 quyển cũng không hẳn là thất vọng. Nhưng chưa "đã". Cái cảm giác đã ở đây là đã về nội dung, về tình huống, về cách mà Lộ Tấn và Thắng Nam giải quyết rắc rối của mình. Hài hước đan xen lần nào cũng là những tình huống tréo ngoeo mà cười ra nước mắt. Nhưng không phải quá tệ đến nỗi vứt truyện, vẫn đáng để đọc hết quyển 2 mà nói tính chất giải trí, xả stress của truyện khá, nếu ai muốn tìm đọc.
3
163693
2015-06-28 12:26:19
--------------------------
208019
7160
Nếu nhìn qua, có thể sẽ cho cho rằng đây là một cuốn cẩm nang cho bạn gái, nhưng thực chất cũng chẳng phải. Mình thấy truyện này cũng tạm được, nhiều tình tiết dí dỏm hài hước nhằm mục đích gây cười rất hiệu quả. Lộ Tấn là một anh chàng gần như hoàn hảo chỉ mắc mỗi khuyết điểm là quá khắt khe, còn Cố Thắng Nam là một cô nàng gần như chẳng có gì đặc biệt ngoài khả năng nấu ăn hơn người, hai người vốn chẳng ưa nhau nhưng lại bất đắc dĩ phải "đối mặt" x lần, mà lần nào cũng làm mình cười té ghế. Tuy nhiên, truyện hài thì hài cơ mà đặc sắc thì chưa đến, đọc để xả stress là chính thôi.
3
393748
2015-06-13 22:10:49
--------------------------
