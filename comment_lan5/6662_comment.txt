480782
6662
Thật sự rất khó để dùng một từ miêu tả tập truyện này. Có niềm vui, có sự hài huớc, có tục tằn, bi thương và cả kinh hoàng. Nhưng có lẽ sự buồn man mác buồn đến ám ảnh là thứ sương mờ bao phủ toàn bởi cuốn sách. Bạn sẽ thấy, ngay cả câu cười tục tíu cũng anh lên sự buồn bã hư hư thực thực. 
"Tôi và cậu ấy đã chăn ngựa trên núi Brokeback một mùa hè" 
Một câu nói ấy thôi đã tràn ngập sự đau thương lỡ làng và mất mát. 
Hãy đọc cuốn sách này và cảm nhận từng luồng truyện theo bản thân bạn.
5
513868
2016-08-20 14:47:18
--------------------------
478749
6662
Quyển sách này có nhiều truyện khác nhau lấy chung đề tài về đất và con người miền Tây, nước Mỹ, khắc nghiệt có, dịu dàng có, tình có, và Chuyện tình núi Brokeback để lại trong tôi nhiều ám ảnh nhất, Jack và Ennis, chuyện tình đẹp mà cả hai còn không dám nhận đó là tình yêu vì những định kiến xã hội, gia đình,..  thế rồi cả hai quẩn quanh trong mê cung mà không tìm được lối thoát đến khi cái chết đã chia lìa họ. Jack chết, chỉ còn lại một mình Ennis sống trong những kỉ niệm, những giấc mơ không bao giờ xóa nhòa nước, "ban đêm  khóc ướt đầm đìa gối có khi ướt cả giường"
5
285647
2016-08-04 20:02:44
--------------------------
476259
6662
+ Điều đầu tiên tôi muốn nói đó là sự lầm tưởng của mình. Khi tôi mua cuốn sách này về, tôi cứ nghĩ cả quyển sách nói về cuộc tình giữa Enis và Jack như trong thước film mà tôi đã coi, nhưng chuyện tình của hai người chỉ là một câu chuyện ngắn.
+ Cách viết của nhà văn khá hay nhưng không biết có phải do lỗi dịch thuật không mà tác phẩm dùng quá nhiều từ địa phương, nhất là câu chuyện " Núi Brokeback " - thật ra tôi mua về cũng chỉ đọc mỗi câu chuyện núi Brokeback thôi - làm người đọc rất khó trong việc thưởng thức tác phẩm, mong Nhã Nam chỉnh sửa lại trong những lần tái bản sau.
+ Nói về câu chuyện Brokeback, đó là một câu chuyện đầy nụ cười, đầy nhục giục nhưng cũng đầy đau khổ của hai người, sống trong xã hội mà tình yêu hai người không được chấp nhận quả là đau đớn. Người đọc khi đọc tác phẩm sẽ hiểu được hoàn cảnh của hai người, nhất là gia đình của Jack vs một ông bố quá tàn nhẫn.
+ Nói chung, câu chuyện của Enis và Jack từ truyện đến phim đều tuyệt vời, nó đã cho mọi người thấy được sự đau khổ và dằn vặt mà mỗi nhân vật trong câu chuyện phải chịu đựng.
5
448310
2016-07-19 14:52:58
--------------------------
473638
6662
Sau khi xem xong, mình quyết định mua sách này trên tiki không cần đắn đo. Khi cầm cuốn sách TIKI giao hàng rất tốt và đây là lần đầu mình hàng trên TIKI. Chỉ lướt được vài chương, tuy văn phong rất khó đọc, mình cũng chưa đọc kỹ chương cuối vì muốn dành thời gian cho những chương khác, nhưng tác giả cho thấy 2 anh chàng cao bồi trong hoàn cảnh đó họ đã đến với nhau trong một tình yêu thật sự. Cái kết tuy bất ngờ nhưng tình yêu đó, ngọn lửa cháy bỏng vẫn sẽ luôn mãnh liệt trên ngọn núi băng giá Brokeback Mountain ngày ấy
5
1025290
2016-07-10 19:53:57
--------------------------
455644
6662
Tôi đọc nó trong vòng năm ngày, mỗi ngày một ít để thấm từng câu chữ của cuốn sách. Có lẽ tôi chưa quen lắm với giọng văn của người phương Tây, có lẽ nó "thô nhưng rất thật". Đó là những câu chuyện về miền đất Wyoming, đó là những miền đất cát với những bụi gai, những ngọn núi, những trang trại đậm chất viễn Tây, đó là những con người thô kệch, bạo lực nhưng cô đơn với những chuyện tình ngang trái, đó những cái chết đến bất ngờ, nhanh và thực... Tôi ấn tượng nhất là hai truyện : Bùn lầy và núi Brokeback. Đặc biệt là "núi Brokeback", một tình yêu nhiều nuối tiếc, nhiều trăn trở và cuối cùng là một người ra đi, một người ở lại, hình ảnh hai chiếc áo lồng vào nhau khiến tôi ám ảnh, chỉ còn những kỉ niệm nơi ngọn núi xa xôi, "khi họ làm chủ thế giới và dường như không có điều gì sai trái". 
Và tôi mong đến một ngày nào đó, tất cả những gì chúng ta cần chỉ là tình yêu, không phải là dị tính hay đồng tính. Tất cả xuất phát từ trái tim đến trái tim mang theo những mơ ước, những nổi niềm để san sẻ, để  hạnh phúc. 
"cà phê cũ đang sôi nhưng hắn kịp trước khi nó trào, rót vào cái tách cáu bẩn và thổi chất nước đen, để một mảng giấc mơ tràn đến.... Gió quất toa moóc như trút đống đất từ xe đổ đất xuống, dịu đi, ngưng hẳn, để lại sự im lặng tạm thời", "Jack, tôi thề mà..."
5
867856
2016-06-22 19:04:14
--------------------------
440550
6662
Một cuốn sách rất hay viết về những tình yêu. Những con người yêu nhau ở một nơi heo hút - núi brokeback - cùng nhau làm việc để sống qua ngày. Nhưng rồi đến khi họ nhận ra được tình cảm của mình thì đã quá muộn, chôn vùi cả tuổi xuân của mình vào công việc mà quên mất cả tình yêu. Đặc biệt ấn tượng với truyện ngắn cuối cùng, câu chuyện giữa Jack và Ennis, họ yêu nhau như vào thời đó thì chuyện đó hoàn toàn là sai trái nhưng tình yêu họ vẫn mãnh liệt. Nói chung những câu chuyện trong cuốn sách này đều khá buồn, giống như cảnh núi brokeback vây - ảm đạm, hiu quạnh, không một chút sức sống của thiên nhiên.
5
1352739
2016-06-01 22:44:11
--------------------------
381007
6662
Mình đã xem phim này trước đó, do Lý An làm đạo diễn, từ đó đọc cuốn sách này hấp dẫn và lôi cuốn hơn.  Đây là một câu chuyện khiêm tốn, chặt chẽ tập trung chủ yếu vào Wyoming Ennis và Jack khi họ dành một mùa hè làm việc với nhau trên núi Brokeback. Họ bị buộc phải dung hòa tình yêu của họ dành cho nhau với cuộc sống thông thường hàng ngày của họ. Câu chuyện để lại cho bạn đọc nhiều cung bậc cảm xúc, sự chân thành, say đắm nồng nhiệt,...

Chắc chắn mình sẽ giới thiệu cuốn sách cho một vài người bạn.
4
621875
2016-02-16 13:09:01
--------------------------
376651
6662
Sách bao gồm nhiều truyện ngắn về cuộc đời của những con người khác nhau. Những câu chuyện vui có, buồn có, hài hước có nhưng tất cả đều xoay quanh cuộc sống và làm việc của họ ở chốn vùng quê hẻo lánh, những hình ảnh của những con người tuy cô đơn nhưng không chịu khuất phục số phận có thể ám ảnh người đọc, khiến họ lưu lại nỗi buồn man mác sau khi kết thúc cuốn sách. Tuy nhiên, tác phẩm lại sử dụng nhiều ngôn từ khá bạo lực nên sẽ gây khó chịu cho những ai đòi hỏi những câu từ hoa mĩ, nhưng chính những từ ngữ đó mới bộc lộ hết tính cách của nhân vật thong tác phẩm.
5
584052
2016-01-31 13:52:34
--------------------------
371532
6662
Tôi đã mua cuốn truyện này khoảng 2 tháng trước và đã nghĩ mình không đọc nữa dù chỉ mới đọc trang đầu vào ngày nhận được hàng. Không phải khi đó tôi đánh giá nó không hay mà là nó không hợp với tôi. Từ trước tới giờ tôi chỉ đọc đam mỹ ( thể loại văn có yếu tố namxnam từ TQ chắc hẳn các bạn ai cũng biết rồi nhỉ =]),nó nhẹ nhàng, dễ ngấm và dù có ngược cũng không quá khó chịu. Thế nhưng quyển truyện này thật sự khác quá. Và trong 1 lúc rãnh rỗi gần đây tôi đã đọc nó .Từ ngôn từ đến văn phong, rắn rỏi dứt khoát nó toát lên cái hiện thực phũ phàng của tình yêu đồng giới và một cái kết mà đối với tôi không thể là một cái kết đẹp (dù với nhiều bạn khác nó là như vậy).Dù sao một sống một chết,nhưng con tim vẫn hướng về nhau đã là rất tuyệt rồi. Quyển truyện đáng đọc!
5
562820
2016-01-19 19:46:09
--------------------------
360356
6662
Thực sự mà nói mình mua quyển sách này vì với mình, Brokeback Mountain để lại cho mình một kỉ niệm rất khó quên. Cầm quyển sách ở hội chợ mình đã mua ngay mà không đọc lại, phần vì bìa nó quá đẹp (Nhã Nam làm bìa bao giờ cũng đẹp), phần vì chính cái kỉ niệm đó. Kết quả khi về nhà mình đọc - thực sự mà nói dịch thuật của quyển này có vấn đề. Có một số câu ngữ pháp đảo loạn lên và không hề giống với cách nói của người Việt, đặc biệt là cách dịch word-by-word khá khó chịu (đơn cử các câu chửi bậy kiểu sh*t các kiểu, hoàn toàn có thể dịch là "chết tiệt" hay gì đó chứ không nên dịch là "cứt thật" gì đó). Mình không được tiếp xúc với văn bản tiếng Anh gốc nên mình không biết liệu có lỗi sai nghĩa, sai câu gì không, nhưng thực sự bản dịch đọc lên rất trúc trắc, mình phải đọc nhiều lần mới lần ra ý của câu. Thực sự nếu bạn mua quyển sách này vì bị bìa của nó thu hút thì mình xin khuyến cáo các bạn nên đọc thử nội dung trước khi mua, xem mình có tiếp thu được cái văn phong của dịch giả trong đó không rồi hẵng quyết định rút ví.
2
1082353
2015-12-29 02:13:34
--------------------------
342045
6662
Mình tìm đến cuốn sách cũng chỉ vì Brokeback Moutain. 1 câu chuyện tình cảm rất chân thật, cảm động.
Ở đây giữa Ennis và Jack tôi thấy đó không chỉ là tình yêu đơn thuần nữa mà còn là điều gì đó hơn vậy rất nhiều. 2 người đàn ông đều đã vợ con đuề huề nhưng vẫn luôn trong mình niềm khát khao được bên nhau, trao cho nhau những cảm xúc mãnh liệt dâng trào.
Suốt 20 năm trôi qua cảm xúc của cả 2 dành cho nhau vẫn luôn nguyên sơ như những đêm chăn cừu lạnh buốt mà đầy hơi ấm của nhau trên ngọn Brokeback đó. Ennis và Jack chưa khi nào thừa nhận nhau bằng một tiếng YÊU, giữa họ như là một sự mặc hiểu cảm xúc của nhau không cần phải thông qua ngôn từ.
Mình đã khóc khi đọc đến đến đoạn miêu tả sự đau đớn không thành lời của Ennis khi tìm thấy cái áo của mình được lồng ở trong, tay áo cẩn thận luồng trong tay áo của Jack. Không những câu chữ miêu tả sự đau đớn tột cùng nhưng mình như cảm nhận được rõ rãng nỗi đau ấy cứ nghẹn lại.
Jack đã gìn giữ chiếc áo như gìn giữ những hồi ức đẹp của 2 người một cách cẩn thận, nâng niu suốt 20 năm. Anh đã ngầm chuẩn bị gần như mọi thứ để đợi đến ngày cả Anh và Ennis sẽ được êm ấm bên nhau cho phần đời còn lại sau khoảng thời gian dài trong mòn mỏi đợi nhau. Chỉ là không ngờ số phận nghiệt ngã đến cuối chỉ còn lại một mình Ennis ngưởi lại phải nhận lấy cô đơn suốt đoạn đời còn lại với những kỷ niệm không nguôi, êm đềm, đẹp đẽ mà nhói lòng.
P/s : hình thức sách thì không gì nói đến, nhưng điều mình cảm thấy không thoải mái khi đọc là mạch truyện rất khó nắm bắt, không biết rằng vì đây là văn phong của tác giả hay dịch thuật chưa mượt. nên mình đã phải đọc thật chậm rãi, để nắm rõ từng chi tiết một, không dám đọc lướt, bỏ qua 1 chữ nào.
4
117702
2015-11-23 17:27:40
--------------------------
311418
6662
Không như những tác giả khác , cái đẹp là chủ thể để làm nổi bật những cái khác. Ở "chuyện tình núi BB", không chỉ không có những hình ảnh đẹp mà ngược lại, tác giả gần như phơi bày hết tất cả những sự thật về cuộc sống một cách chân thực, rõ nét. Không phải vì vậy mà không có cái đẹp trong tác phẩm. Không, không phải vậy, xuyên suốt 11 truyện ngắn, cái đẹp vẫn đan xen trong sự buuồn bã, u sầu của sự chết chóc, những cuộc chia ly,... và núi Brokeback là truyện hay nhất theo cảm nhận của tôi. Nó thấm đẫm nỗi buồn của Jack và Ennis, gợi ng đọc sự xót xa, thương cảm. và TRUYỆN KHÔNG DÀNH CHO NHỮNG TÂM HỒN TRONG SÁNG. cho 4 sao.
4
576561
2015-09-19 23:06:59
--------------------------
307478
6662
Tôi biết đến quyển sách này khi xem Brokeback Mountain, có hơi chút thất vọng khi đây là tập truyện ngắn (bởi có lẽ tôi thích truyện dài hơn). Bìa sách đẹp, giấy tốt nhưng dịch khô, không giữ được chất cao bồi của nguyên tác
Trong các tập thì tôi thích Chuyện tình Brokeback nhất: Ngọn núi hẻo lánh với 2 con người cô độc và 1 tình cảm sai trái ... hay là đúng đắn?
Wyoming đơn giản là môt thị trấn nhàm chán với những con người cố tìm lấy thú vui cho riêng mình. Nó đơn giản, nhàm chán nhưng khi gấp lại quyển sách, nó để lại hết những hoang mang, cay đắng, thú vị rồi bỏ đi hờ hững như bản chất của nó...
4
214856
2015-09-18 09:04:22
--------------------------
305541
6662
Về hình thức: Sách được in rất đẹp, bìa sách cứng, chất lượng giấy tốt, mực in rõ ràng

Về nội dung: chuyện tình núi brokeback là một tác phẩm tuyệt vời. Truyện hấp dẫn bởi tính chân thật trong từng tình tiết truyện. Tác phẩm này thực sự rất nổi tiếng và đã được dùng làm kịch bản cho một bộ phim cùng tên và cũng rất nổi tiếng. Nhưng đến khi đọc truyện bạn mới thực sự cảm nhận được cái hay tuyệt vời mà phim không thể diễn tả được. Những tình tiết khó hiểu trong phim bạn sẽ được giải đáp tại quyển sách này.

Hãy mua và đọc nhé, chắc chắn bạn sẽ không hối hận đâu
4
409134
2015-09-16 23:48:10
--------------------------
253216
6662
Tôi biết đến Chuyện Tình Núi Brokeback của tác giả Annie Proulx qua bộ phim điện ảnh cực nổi tiếng trước rồi mới tìm đến truyện. Hoá ra đây chỉ là một tuyển tập truyện ngắn mà trong đó  Chuyện Tình Núi Brokeback là nổi tiếng nhất. Xuyên suốt tập truyện là nỗi buồn: nỗi buồn của sự cô đơn, của dang dở, của nuối tiếc và cũng là của tình yêu. Tôi đặc biệt ấn tượng với cách tác giả xây dựng nhân vật trong các truyện ngắn của mình, tiếc là bản dịch vẫn chưa ổn lắm thì phải nên làm tác phẩm kém hay đi nhiều.
4
167283
2015-08-04 11:28:24
--------------------------
249400
6662
Văn học vẫn còn khá ít tác phẩm về thế giới LGBT nhưng đã có những tác phẩm để lại trong lòng người đọc, dù là giới nào, vẫn đầy ắp yêu thương, tính nhân văn sâu sắc, sự cởi mở về giới tính của con người. Trên hết mọi thứ, con người không đi tìm cho mình một giới tính mà đi tìm hạnh phúc và yêu thương - "Brokeback Moutain" là một tác phẩm của sự yêu thương. Tác phẩm điện ảnh của "Brokeback Moutain" cũng khá sát với tác phẩm văn học. Tình yêu không thể bị che đậy dù nó đang được bọc trong một gia đình hạnh phúc, một người vợ đẹp và hai con. Tình yêu khi không được trọn vẹn lại càng mảnh liệt và cồn cào. Hình ảnh người đàn ông đến nhận chiếc áo khoát, là kỷ vật duy nhất của người yêu mình, là hình ảnh một tình yêu thực sự, bất chấp người ấy là ai và đang yêu ai...
4
378725
2015-07-31 16:06:35
--------------------------
246280
6662
Có đôi khi chúng ta lạc nhau trong chốc lát và mất nhau cả đời. Câu chuyện này tôi từng xem bản movie, có đôi chỗ khó hiểu và đã được giải đáp khi đọc câu chuyện. Câu chuyện tình buồn, tôi đọc vào một tối trời mưa, cảnh tượng về dãy núi về những chàng cao bồi cứ lướt qua, về chiếc áo thấm đẫm mùi người người yêu hay sự đau đớn của những người trong cuộc. Có những sự chia ly là mãi mãi và chỉ có thể chờ đến kiếp sau mới quay lại tìm nhau được.
4
107384
2015-07-29 17:50:46
--------------------------
236596
6662
Thấy mọi người đều xem phim và đọc sách nhưng tôi mới chỉ đọc sách, còn phim thì chưa có cơ hội. Đầu tiên về bản dịch, tôi thấy có vài chỗ từ ngữ hơi thô, đọc hơi cảm thấy khó chịu. Bản in đẹp, rõ ràng. Nội dung nói chung khá gây nhiều ấn tượng và chứa đựng nhiều cảm xúc, nhiều người đã nhận xét kĩ càng về phần này rồi nên tôi ko nói thêm. 11 câu chuyện khác nhau nhưng dường như cùng chảy chung một mạch. Những cảm xúc luyến tiếc, bi ai và ám ảnh xuyên suốt bằng những câu chuyện với cái kết dang dở tạo nên một bức tranh đầy đủ về sự sống khắc nghiệt của con người xứ Wyoming. Câu chữ của nhà văn dùng trôi chảy, mạch lạc
4
267843
2015-07-21 21:28:44
--------------------------
234236
6662
Những câu chuyện ngắn về cuộc sống của con người xứ Wyoming với thiên nhiên hùng vĩ nhưng cũng không kém phần khắc nghiệt. Ở đó, con người như thật nhỏ bé và lọt thỏm giữa vùng rừng núi ấy. Họ gắn bó với nhau bằng nghề nghiệp, bằng sự cảm thông với những nỗi vất vả mà ngày ngày họ đang trải qua. Trong đó, là nỗi khát khao về một tương lai tươi sáng, về một tình yêu không có khoảng cách. Đặc biệt, mối tình giữa 2 nhân vật chính Jack và Ennis là ám ảnh nhất trong toàn bộ câu chuyện. 2 con người yêu nhau nhưng do những rào cản xã hội lại không thể đến được với nhau. 2 chiếc áo lồng vào nhau là sự khát khao và mong muốn và cả những luyến tiếc khi 2 ng phải chia xa. 
4
107360
2015-07-20 10:44:20
--------------------------
220857
6662
Mình đã từng xem phim này rồi, thấy nó hay và thực tế nên tìm đến truyện để đọc. Cá nhân mình thích đọc truyện hơn, vì dưới ngòi bút tài tình của tác giả, mình thấm thía hơn về tính cách cũng như nội tâm nhân vật. Đây là một câu chuyện thực tế vô cùng, không phải ai cũng có đủ can đảm để đạp lên dư luận mà sống. Mình không kỳ thị người đồng tính, nhưng mình hy vọng trên thế giới này ít người đồng tính thôi, vì họ quá khổ, khổ từ tâm hồn đến thể xác, thế mà còn bị dư luận lên án nữa, đáng buồn thật!
4
31339
2015-07-03 08:17:06
--------------------------
219248
6662
Ai mua quyển này ắt cũng do coi phim. 11 câu truyện ngắn và 11 cuộc đời khác nhau nhưng tất cả đều diễn ra tại vùng núi Wyoming hùng vĩ nhưng cũng đầy khắc nghiệt. Con người ở đó phải mạnh mẽ, kiên cường để sinh sống từ đó bao nhiêu con người, số phận khác nhau. Câu truyện thứ 11 cũng là câu truyện nhiều ngưởi mong đợi nhất, nó cho thấy khát vọng được nhiêu, được hạnh phúc và mong ước được thừa nhận của người đồng tính lúc bấy giờ, do là truyện ngắn nên phim phần nào đã thể hiện trọn vẹn nội dung của cậu truyện, nhưng mình vẫn thích đọc hơn. Ngoài ra, các câu truyện khác cũng rất hấp dẫn
5
308313
2015-07-01 11:56:09
--------------------------
209861
6662
Mua cuốn sách này để tặng sinh nhật người yêu.
Cũng có xem phim này rồi, thực sự rất hay và cảm động.
Mua sách rồi mới biết nó có rất nhiều chương và chuyện tình núi Brokeback chỉ là 1 trong số các chương của cuốn sách này.
Nhưng có thể nói đây là phần hay nhất của cuốn sách, nói về mối tình giữa 2 nhân vật chính, về một chuyện tình xuất phát từ ngọn núi Broke back hùng vĩ.
Mấy phần khác cũng hay, văn phong của tác giả thì rất thực, hấp dẫn và lôi cuốn.
5
100385
2015-06-18 12:07:36
--------------------------
204500
6662
Có thể nói đây là một truyện tình đẹp của hai nhân vật nam chính Jack và Ennis. Trải qua bao nhiêu yêu thương mãnh liệt, quấn quýt, rời xa rồi trở lại, nhưng cho tới tận cùng, đó có thực sự hạnh phúc. Có thể trong những năm tháng ấy, quãng thời gian hai người cùng nhau, đã từng là hạnh phúc. Nhưng cái tôi trong Ennis quá lớn, và không thể dũng cảm cùng Jack đi tới cuối con đường. Có lẽ cuộc sống thực là thế. Yêu nhưng phải đối mặt với người đời. Bao nhiêu người có dũng khí để nắm chặt tay đi đến cuối cùng. Ấn tượng với mình ảnh hai chiếc áo lồng vào nhau ở bìa truyện, mộc mạc, giản dị nhưng đủ để nói rằng hai người yêu nhau.
4
26110
2015-06-04 10:08:30
--------------------------
201231
6662
ngôn từ giản dị Annie Proulx đã miêu tả một cách chân thật về một miền quê Wyoming tràn những nỗi buồn thông những câu chuyện qua ngòi bút của bà mỗi câu chuyện như một câu chuyện là những bức chân dung đơn sắc chi chít nỗi cô đơn khép lại quyển sách là một chuyện tình đẹp giữa Ennis và Jack dù Jack qua  đời ở cuối câu chuyện và kỉ vật Ennis có thể nhớ về Jack là hai  chiếc  áo quyện chặt lấy nhau mang một thông điệp khiến người đọc suy ngẫm  Từ lúc đó Jack cứ xuất hiện trong giấc mơ của Ennis  gã xuất hiện như thuở ban đầu vẫn là ánh mắt nụ cười này hắn bàn về chuyện làm lại cuộc đời  sau những giấc mộng dài cuối cùng họ cũng tìm thấy nhau gửi trao nhau những thứ tình cảm qua bao nhiêu cách trở
5
618090
2015-05-26 22:01:13
--------------------------
197135
6662
Mình biết đến "Chuyện tình núi Brokeback" vì đã từng xem phim. Lúc đầu mình những tưởng "Chuyện tình núi Brokeback" là một cuốn tiểu thuyết, đến lúc biết được thì có phần hơi hụt hẩn. Cuộc sống đơn độc hai gã chăn cừu giữa núi rừng hoang sơ, bát ngát. Jack là một người đồng tính, còn Ennis thì không. Dường như cuộc sống thiếu thốn vật chất, lẫn tinh thần hay là cả những nhu cầu "sinh lý" nơi đó đã xô hai người họ lại với nhau. Nhưng dù bắt đầu như thế nào thì tình cảm của họ đã lớn dần theo năm tháng ở núi Brokeback. Cuối cùng thì những khắc nghiệt của xã hội cũng tạo ra một bi kịch cho cả hai. Người thì chết , người thì sống một cuộc đời đau đớn và đầy hoài niệm.
4
386431
2015-05-17 01:25:26
--------------------------
196681
6662
Tôi tìm đến cuốn sách sau khi đã quá ấn tượng với phim của đạo diễn Lý An, tôi nghĩ câu chuyện trong Chuyện tình núi Brokeback sẽ rất tuyệt vời, nhưng khi đọc lần đầu tôi không thể cảm nhận được gì ngoài sự thất vọng cho một cuốn sách mà nhiều người yêu thích đến thế. Nhưng hai và tháng sau khi đọc lại tôi lại có cảm nhận hoàn toàn khác, tôi thấy trong đó là cả một miền tây hùng vỹ, những con người không bao giờ khuất phục và cả chuyện tình đam mê của hai chàng trai trên núi Brokeback. Tôi nhận ra có lẽ đây là cuốn sách cần thời gian, trải nghiệm để cảm nhận chứ không phải là cách đọc vồ vập, hấp tấp, và khi đã cảm nhận được bạn sẽ không phải hối tiếc vì có nó trong tay.
Tuy nhiên có lẽ do muốn giữ nguyên tinh thần của truyện mà dịch giả đã sử dụng nhiều từ khá thô, nhiều lúc đọc tôi thấy không thích lắm.
4
465492
2015-05-16 11:59:24
--------------------------
196411
6662
Ennis và Jack, vì sao họ mãi không thể đến được với nhau? Dù biết rõ câu trả lời rằng Ennis không thể vượt qua tâm lý bản thân, dù biết câu trả lởi rằng xã hội băng hoại đạo đức cổ hủ chà đạp tình yêu giữa họ, tôi vẫn không thể nén được nước mắt. Mở đầu bằng một Ennis chán nản, luôn mơ về Jack, kết thúc bằng một Ennis đau đớn, và cũng đang mơ về Jack. Những năm tháng làm chồng của một phụ nữ, Ennis sống như rô bốt, nhưng cho đến khi gặp lại Jack, chính anh cũng không kìm lòng được mà ôm hôn Jack thắm thiết. Tình cảm dồn nén bao lâu bùng cháy, chi tiết đó khiến tôi xúc động mạnh, có lẽ vì từ ngữ miêu tả quá mạnh mẽ. 
Về cảnh cuối, tôi đã khóc khi Ennis vùi mặt vào áo của Jack; cố ngửi lấy cái mùi của chàng cao bồi bảnh trai lẫn mùi hương kỷ niệm. Những ngày tháng họ bên nhau trên núi Brokeback, nơi họ làm chủ thế giới và chẳng có ai cản ngăn họ. Ennis có bao giờ hối hận vì không giữ chặt Jack không? Có bao giờ hối hận vì không thể bảo vệ anh không? Thật đau lòng.
Nếu bạn yêu ai đó, xin đừng buông tay họ. Xin đừng.
5
526861
2015-05-15 20:22:39
--------------------------
191865
6662
Vì chuyện tình núi brokeback nên mới quyết định mua cuốn sách này, nhưng sau khi đọc xong từng mẫu truyện ngắn lại cảm giác như một sự liên kết mong manh xoay quanh cuộc sống nơi chứa đựng nhiều nỗi tuyệt vọng, những niềm vui tạm bợ và hơn hết là sự cô đơn giữa vùng núi hoang dã Wyoming. Wyoming quá rông lớn quá hoang vu càng làm nỗi cô đơn thêm khắc khoải trong lòng người. Những nhân vật dường như mang cho người đọc cảm xúc như chạm vào nỗi tận cùng của cô độc, cảm xúc day dứt không thôi. Đặc biệt chuyện tình núi brokeback lại không mang lại nhiều cảm xúc như khi xem phim có lẽ do kì vọng quá nhiều.
4
283000
2015-05-03 01:19:49
--------------------------
170247
6662
Riêng mẫu chuyện nhỏ cuối cùng trong Núi Brokeback cũng đủ khiến tôi tặng quyển sách này 5 sao. Lúc đầu đọc tôi hơi khó chịu với cách dịch thô tục, nhưng ngẫm lại, đó mới chính là phong cách cowboy Mỹ: rất chân thật và mộc mạc. Và cũng chính vì xuyên suốt là những hình ảnh và từ ngữ thô, nên khi xuất hiện hình ảnh lãng mạn như cảnh Ennis ôm Jack từ đằng sau mà ko gợn chút tình dục, hay khung cảnh thi vị của một Brokeback u buồn, thơ mộng lại khiến tôi ấn tượng sâu đậm. Ít truyện nào làm tôi nhớ lâu đến vậy dù chỉ đọc qua 1 lần. 
Tôi thích nhân vật Jack hơn, anh ấy như đại diện cho phái nữ trong mối quan hệ với Ennis. Anh thích con ngựa màu hồng, ngoại hình được miêu tả là "bảnh bao", đôi khi còn khá dễ thương với những lời nói hờn dỗi. Jack yêu Ennis, bền vững và khao khát, anh muốn sống với Ennis một cách trọn vẹn, nhưng cho dù Ennis có vượt qua mâu thuẫn của bản thân, thì sự kì thị của xã hội lúc bấy giờ cũng đã tước mất Jack khỏi tay anh. Là một người thích kiểu u ám, bi kịch, nhưng Lần đầu tiên khi đọc một câu chuyện, tôi mong ước cái kết có thể tươi đẹp hơn một chút, và Jack của tôi xứng đáng được hưởng thứ hạnh phúc anh ấy luôn mơ ước. 

Khi đọc đến đoạn Ennis lồng 2 chiếc áo vào nhau, nhưng lần này là lồng chiếc áo của anh ở ngoài áo của Jack, như thể Ennis muốn bảo vệ Jack, muốn che chở cho Jack khỏi những kẻ cổ hũ và băng hoại đạo đức của xã hội, tôi đã rơi nước mắt. Ennis khóc và thề, một lời thề ko trọn vẹn nhưng hẳn ai cũng sẽ có đáp án của lời thề dang dở ấy cho riêng mình. Theo tôi, tôi nghĩ Ennis sẽ thề rằng anh đã muốn sống một hạnh phúc trọn vẹn với Jack biết bao...

Một câu chuyện hay vô cùng. Tuyệt đỉnh cảm động.
5
526861
2015-03-19 18:29:22
--------------------------
147783
6662
Cuốn sách là là một trong những cuốn mình rất ưng ý của nhà xuất bản Nhã Nam. Cuốn sách được thiết kế bìa rất hợp với truyện. Hình ảnh hai chiếc áo đan vào nhau treo trên chiếc giá - như tình yêu của hai chàng cao bồi, mãi chẳng rời xa.
Dịch thuật thì khá tốt và rất ít lỗi. Các ngôn từ được dịch rất chính xác, tuy có hơi thô so với thuần phong mĩ tục Việt Nam nhưng mình thấy nó rất hợp lý bởi nó thể hiện được sự phóng túng của những anh chàng cao bồi miền Tây.
4
316220
2015-01-08 20:42:35
--------------------------
126268
6662
Theo mình nghĩ Jack là người đồng tính (một số chi tiết cho thấy Jack có quan hệ với nhũng người đàn ông khác ) còn Ennis chỉ là một người đàn ông bình thường do sự cô đơn, mối liên kết đặt biệt giữa hai con người họ đã tới với nhau mà jack là người chủ động. Truyện có tình tiết, màu sác nhẹ nhàng, mang cảm xúc mạch truyện tác động mạnh tới đọc giả...nó như tiểu sử cuộc đời của một cá nhân mang đầy bi kịch tình cảm. Một câu truyện tinh cảm kinh điển sâu sắc
4
323456
2014-09-18 01:07:17
--------------------------
120513
6662
Mình không đọc bản gốc bằng tiếng anh, nên không dám chắc có phải cho bản dịch không rõ gây khó hiểu hay tại nguyên tác nó vậy, cốt truyện rất hay, nhưng mà phải đọc lần thứ 2 mình mới hiểu (không bao gồm gồm Brokeback mountain vì cái này thì đã lên phim và quá nổi tiếng rồi). Nhưng những câu chuyện về sự cô đơn ở chốn miền quê đồng không mông quạnh, con người như nhỏ bé và chỉ sống làm việc cho qua ngày, cô đơn đến nổi nói chuyện với 1 con bò, cô đơn giữa cảnh thiên nhiên hoang dã, cô đơn đến nỗi làm mình ám ảnh. Nhận xét chung là tuyệt, nên đọc sách vào buổi tối, khi đang ở một mình thì càng thấm hơn.
5
180949
2014-08-12 23:11:21
--------------------------
120117
6662
Mình xem phim rồi mới đọc truyện nên mình rất ấn tượng với trang bìa của sách- ý nghĩa và rất ý nghĩa.Một câu chuyện mang lại cho mình nhiều cảm xúc-vui buồn ,hài hước và không thể nói thành lời.Hai chiếc áo lồng vào nhau thể hiện tình cảm chưa bao giờ xa cách giữa Ennis và Jack.Một chi tiết đắt của câu chuyện.Tuy nhiên mình có một chút phiền lòng vì cách dịch .Dịch giả dùng các từ như:hổng,ổng..trong văn nói dịch trong văn viết làm mình có chút hụt hẫng ,mất cảm xúc hẳn khi gặp mấy từ đó.Không biết mấy bạn cảm nhận như thế nào nhưng đối với mình thì thật là khó chịu.Ngoài ra có một số lỗi chính tả như Wyoming thì viết là Wyomin...Dù vậy đây vẫn là một cuốn sách hay-đem lại cho mình cảm xúc mới lạ về cuộc sống.
4
192593
2014-08-09 12:33:38
--------------------------
117451
6662
Tôi lỡ hẹn 1 lần với cuốn sách này. Không phụ lại sự chờ đợi đó chuyện tình núi brokeback đem đến cho tôi vô vàn cảm xúc, chuyện tình giữa những đấng mày râu, những câu chuyện cứ xoay vần mà bạn không thể dứt nó ra được! Đọc 1 lần chưa hiểu thì đọc 2 lần nhưng chắc chắn là sẽ đọng lại cho bạn một ấn tượng về một cuốn sách nhiều mảng màu cuộc sống. Với cá nhân tôi thích sự cô đơn trong hình hài của các nhân vật tác giả dựng nên. Một cuốn sách thật sự rất đáng đọc!
5
118818
2014-07-18 17:43:51
--------------------------
