411389
5373
Được giới thiệu là câu chuyện cảm động về cặp đôi Đàm Duy và Tiểu Băng, đọc hết tập 1 cũng không khiến tôi có tí cảm xúc gì, không hay như tôi nghĩ.
Nhân vật Đàm Duy khiến tôi ấn tượng và đáng quý nhất, là nhân vật được tác giả xây dựng có cốt cách từ đầu đến cuối truyện.
3 nhân vật chính còn lại Tiểu băng,Thường Thắng, Tạ Di Hồng khiến tôi rất khó chịu trong cách thể hiện của tác giả, phải nói là rối như tơ vò, tính cách đa nghi hay thắc mắc với Đàm Duy của Tiểu Băng, tính cách thẳng đuột đầy mập mờ về tình cảm của Tạ Di Hồng với Đàm Duy và kẻ đáng thương Thường Thắng.
Tình tiết tập này không gây ấn tượng gì, không hay, nhiều thứ khiến tôi cảm thấy bị thừa thải.
Tuy vậy vẫn có những tình huống trong câu chuyện khiến người đọc phải suy ngẫm, khá thực tế
Chẳng hiểu sao nữa, có lẽ mình không thích hợp với lối viết của Ngãi Mễ.
3
393052
2016-04-05 22:38:09
--------------------------
233551
5373
Tôi thấy đây là một tác phẩm hay, có nhiều bài học về hôn nhân, về cuộc sống vợ chồng cho mọi người tham khảo cũng như hiểu rằng không phải có tình yêu là có hạnh phúc, mà tình yêu chỉ là một sự khởi đầu. Mà chính sự hiểu biết lẫn nhau, tha thứ cho nhau mới là chìa khóa thật sự của hạnh phúc. Tuy nhiên nó chưa thật sự hấp hẫn, cuốn hút người đọc. Có lẽ vì đã đem vào quá nhiều bài học vào quyển sách nên khi đọc tôi thấy có chút nhàm chán.
4
517104
2015-07-19 18:56:26
--------------------------
215148
5373
Ngãi Mễ đã để lại những cung bậc cảm xúc rất riêng biệt trong những lời văn. "Ai sẽ ôm em khi thấy em buồn" là một chuỗi những câu chuyện về cuộc sống vợ chồng, hạnh phúc không phải là cứ yêu thương ngọt ngào mà đó còn là những lo lắng giận hờn để hiểu nhau hơn.
Vợ chồng phải trải qua cùng nhau nhiều sóng gió, chỉ có tình yêu và sự tin tưởng mới giúp 2 con người vốn xa lạ trở nên gần gũi, hòa hợp. Hơn thế nữa đó còn là những mối quan hệ khác, với bố mẹ 2 bên, với bạn bè, sống làm sao để dung hòa được mọi điều để mọi người đều có thể hạnh phúc khi nhìn vào cuộc hôn nhân của mình.
4
49743
2015-06-25 22:57:36
--------------------------
147630
5373
Đọc tựa đề sách thì thấy rất tò mò, AI SẼ ÔM EM KHI THẤY EM BUỒN - tác phẩm của một tiểu thuyết gia nổi tiếng.
Đúng như mọi người nói về cô, Ngải Mễ là một cây bút rất xuất sắc, cô luôn dẫn dắt người đọc vào từng chi tiết trong câu chuyện của mình.
Nội dung truyện rất lôi cuốn, xây dựng hình ảnh cho từng nhân vật cũng thật ấn tượng. Câu chuyện xoay quanh cuộc sống của 2 vợ chồng Đàm Duy và Tiểu Băng, họ luôn phải trải qua nhiều chuyện phức tạp như: "cuộc chiến" chống HIV, nhận con rơi, "cuộc tình tay ba" với bạn thân,....và nhiều tình huống bất ngờ khác luôn xảy ra.
Truyện chứa nhiều tình tiết người lớn 18+, nên xem xét trước khi đọc.
4
89607
2015-01-08 15:08:48
--------------------------
139365
5373
Ngải Mễ đặc tên cho cuốn sách này rất hay và bùi tai . Ai sẽ ôm em khi thấy em buồn ? Một câu hỏi tu từ mà sau dấu chấm hỏi đó ẩn chứa biết bao nhiêu sự cô đơn và bi thương ước mong hạnh phúc . Tôi thích cách kể của Ngải Mễ cô không vội dẫn dắt người đọc vào câu chuyện mà để cho họ thấy một khía cạnh khác để chuẩn bị tâm lý trước khi bắt đầu . Trong tác phẩm lần này của Ngãi Mễ về nhân vật tôi rất hài lòng tính cách của nữ chính và nam chính khá dễ thương và cá tính , Điểm không hài lòng duy nhất của tôi là có nhiều đoạn cô làm cho nhân vật tôi yêu thích đau khổ và dằn vặt làm tôi xót chết đi được . Bìa thiết kế nhìn không đẹp mắt tí nào
3
337423
2014-12-06 19:22:36
--------------------------
