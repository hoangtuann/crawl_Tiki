394367
3299
Em mình học lớp 6, nên nhân ngày 8/3 mình tặng cuốn này cho nó với hi vọng thái độ học tập của nó sẽ tích cực hơn! 
Cuốn này có những kiến thức trọng tâm và bài tập liên quan, nếu như phụ huynh có bận rộn khi mà khôn thể quan tâm con cái, thường xuyên kiểm tra bài vở như ba mẹ mình vậy, thì tại sao không mua sản phẩm này về để cuối tuần kiểm tra năng lực học tập của con mình đi đến đâu?
Tặng sách rất có ích, và hữu dụng hơn khi mua cuốn sách này!
4
672530
2016-03-10 11:30:36
--------------------------
