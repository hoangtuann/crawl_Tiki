252449
7698
Trong chuyến đi thị sát các huyện vùng biên ải, tới một huyện nọ, đang đi trên xe sát hạch một vòng thì trạng Tí và các bạn ( trong đó có cả mùi mập nữa) gặp một người câm cầm tờ giấy trắng tinh thưa kiện. Không biết làm như thế nào, trạng Tí đành dùng mưu hỏi các người dân trong huyện. Khi đó biết được sự tình uẩn khúc, nên trạng Tí phân xử ngay, đòi lại được công bằng cho người câm đó. Sau khi lấy lại được tiền cho chú câm ấy, vì quá vui mừng nên   đập cả đều vào cột, và chú ấy đã nói lại được,  kể rõ sự tình cho trạng Tí nghe. Và thêm một bất ngờ nữa là, chú ấy lại là cha của Mùi mập. Nội dung truyện hay, cảm động, xúc tích. không những vậy, truyện còn được thiết kế rất đẹp, màu sắc hài hòa, in hình khái quát truyện. Nói chung mình rất hài lòng về quyển truyện này!
5
120141
2015-08-03 17:43:26
--------------------------
