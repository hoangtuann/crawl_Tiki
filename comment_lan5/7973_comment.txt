424276
7973
Bìa sách: đẹp, mình rất thích
Chất lượng giấy: tốt, font chữ dễ nhìn, họa tiết nhỏ xinh khá thú vị
Cuốn sách có 3 mục chính: Lựa chọn của con người, Ranh giới để lùi lại, Hãy cất cao tiếng nói của bạn trong cuộc sống.
Trong mỗi mục là mười mấy câu chuyện nhỏ, chiếm từ 2 -3 mặt giấy. Sau mỗi câu chuyện là có góc suy ngẫm, đúc kết lại ý nghĩa, bài học...đưa ra lời khuyên cho người đọc.

"Con đường đi tới thành công của mỗi chúng ta không phải lúc nào cũng trải đầy hoa hồng mà còn có cả gai góc. Nhưng dù thế nào đi nữa, bạn cũng đừng vội buông xuôi mà hãy nhớ rằng muốn nhìn thấy cầu vồng, bạn phải đi qua cơn mưa. Hãy tin rằng ngày mai nắng sẽ lên và cuộc đời sẽ ươm hồng những ước mơ của bạn"...Và đó cũng chính là thông điệp mà cuốn sách gửi gắm đến cho bạn!
5
400969
2016-05-01 11:18:26
--------------------------
248977
7973
hi, Mình vui khi cầm trong tay quyển sách này , nó cứ như thiên thần giúp mình biết lựa chọn cả cách sống là hành động Mình thích nhất câu nói : đời người có biết bao con đường , bên mỗi con đường là bao hoa thơm trái ngọt , Nếu bạn không thể tiếp tục đi trên con đường này thì đừng vội bỏ cuộc , hãy bước sang một con đường khác , biết đâu bạn sẽ nhận ra con đường đố đẹp đẽ hơn không gian thoáng đãng hơn , cảnh đẹp mê hồn hơn.
5
715434
2015-07-31 11:38:42
--------------------------
