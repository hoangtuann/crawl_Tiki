393504
3504
Sách nấu ăn thị trường hiện nay bán khá nhiều, trên tiki cũng vậy. Nhưng khi mình muốn mua đa số mình xem mấy bạn nhận xét trước, thấy được mới đặt. Khi đặt quyển này thì thấy khá rẻ mà những 168 món, thật ra ban đầu cũng thấy lo lo vì rẻ quá mà nhiều món như thế, trang thì ít, nhưng khi nhận được hàng và đến nay làm kha khá số món đều thành công thì thấy cuốn này khá tốt, trình bày rõ ràng, cách bày làm dễ hiểu, thêm nữa là những nguyên liệu đều dễ kiếm, phù hợp với mọi đối tượng sử dụng
3
50038
2016-03-08 20:59:02
--------------------------
214590
3504
Sách nấu ăn hiện nay khá nhiều nhưng không biết nên chọn quyển nào vì tôi ít có kiến thức về các đầu bếp nổi tiếng. Quyển sách không dày lắm nhưng tới 168 món khiến lúc đầu tôi khá e dè vì số món quá nhiều. Tuy nhiên sách giấy đẹp và trình bày khá rõ ràng. Một điểm trừ bự là không có hình minh hoạ cho món ăn nên bị ít hấp dẫn một phần. Mọi thứ còn lại khá ổn. Giá cả rất phù hợp, chỉ hơn 20 nghìn. Cá nhân tôi khá hài lòng về giá cả cũng như chất lượng của quyển sách này.
4
89533
2015-06-25 10:26:08
--------------------------
