315427
5326
 Mình biết đến cuốn sách sau khi xem phim nhưng quả thật, nó không hấp dẫn như bạn xem phim. Giọng văn như kiểu dịch thuật, chỉ thích hợp với những bạn chưa bao giờ xem phim và muốn biết nội dung. Nhưng với mình, một câu chuyện tình yêu hay như thế thì người dịch nên đặt nhiều tình cảm của mình vào đó hơn. Còn về nội dung truyện thì không có gì để bàn cãi, một câu chuyện tình yêu cảm động, đẹp đẽ và đầy sức lan tỏa. Dù sao nếu bạn chưa xem phim thì bạn cũng nên mua cuốn sách này để biết về một câu chuyện hay! 
5
839181
2015-09-28 20:45:58
--------------------------
292892
5326
Mình mua cuốn sách vì rất thích phim hoạt hình Wall E, nhưng lời văn không được mượt mà lắm khiến cuốn sách không truyền tải được câu chuyện như mong đợi, ít nhất là vớu người lớn. Đối với tụi nhỏ thì đây vẫn là một cuốn sách tốt để tập thói quen và tình yêu với sách do nội dung ngắn, câu chuyện nhiều bài học và đầy tính nhân văn. Sách hợp với trẻ khoảng 6-7t, dành để ba mẹ ngồi kể trước giờ ngủ và dạy trẻ cách sống bảo vệ môi trường lẫn tình yêu cao cả của hai người máy đáng yêu
3
450198
2015-09-07 19:25:14
--------------------------
244357
5326
Mình biết đến cuốn sách sau khi xem phim nhưng quả thật, nó không hấp dẫn như bạn xem phim. Giọng văn như kiểu dịch thuật, chỉ thích hợp với những bạn chưa bao giờ xem phim và muốn biết nội dung. Nhưng với mình, một câu chuyện tình yêu hay như thế thì người dịch nên đặt nhiều tình cảm của mình vào đó hơn. Còn về nội dung truyện thì không có gì để bàn cãi, một câu chuyện tình yêu cảm động, đẹp đẽ và đầy sức lan tỏa. Dù sao nếu bạn chưa xem phim thì bạn cũng nên mua cuốn sách này để biết về một câu chuyện hay!
3
85767
2015-07-28 10:01:51
--------------------------
182133
5326
Bìa sách đẹp giá cả rất phải chăng. Nội dung phù hợp với mọi lứa tuổi. Sách kể về một người máy sống bằng năng lượng mặt trời, suốt ngày chỉ biết dọn dẹp rác thải, nhưng đến một ngày định mệnh cậu đã gặp được Eve- một người máy thăm dò trái đất để tìm sự sống của thực vật. Và Wall-E đã đem lòng yêu cô gái người máy thăm dò đó. Mặc dù, nhiều biến cố xảy ra, Wall-E đã xém chết, mất trí nhớ, nhưng cuối cùng Wall-E và Eve cũng đến được với nhau. Không chỉ là một câu chuyện để nói về tình yêu, sách còn gởi gắm cả những thông điệp đến với con người đang sống trong thế giới này-Hãy bảo vệ  Trái Đất.
4
255414
2015-04-13 12:44:28
--------------------------
161800
5326
Đây là một câu chuyện mà với mình, nó mang tính nhân văn rất cao. Wall E và Eve mang lại rất nhiều cảm xúc. Đôi khi với con người thì những cái nắm tay đôi lúc là những điều đơn thuần thôi, nhưng với hai bạn người máy, thì nó không hề đơn thuần. Mượn hình ảnh của hai cỗ máy để làm cho cái nắm tay trở nên thiêng liêng. Hoặc nói nhờ cái nắm tay đơn thuần mà tình cảm giữa hai cỗ máy thật đáng trân trọng. Cách nói nào mình cũng thích cả, bởi vì tình yêu là một phép màu nhiệm, mà ở đây từng chi tiết đều làm mình run lên vì sự đáng yêu của anh chàng Wall E.
5
43136
2015-02-28 22:32:52
--------------------------
151488
5326
Wall. E thực sự là một câu chuyện đầy cảm động và lấy đi nước mắt của nhiều người!
Câu chuyện kể về chứ robot Wall-E một mình cô độc trên Trái Đất. Rồi ngày định mệnh đến khi chú gặp được Eve - cô robot thăm dò ngoài Trái Đất. Tình yêu dần xuất hiện giữa họ. Nhưng biến cố lại xảy ra với Wall-E và những con robot khác. Và để bảo vệ Trái Đất. Wall-E đã hy sinh thân mình để chống lại robot Auto.
Đọc đến đoạn Wall-E bị mất đi trí nhớ của mình làm mình cảm động đến nỗi rưng rưng nước mắt. Và thật may là kết thúc có hậu diễn ra - tình yêu chân thành đã chiến thắng tất cả!
Mình yêu thích tác phẩm Wall. E này :) Thực sự có hồn và nhiều cảm xúc!
5
303773
2015-01-19 22:38:43
--------------------------
150741
5326
Wall-e từng là một bộ phim tuyệt vời và khi ra sách nó vẫn giữ được những điều đó. Wall-e là một chú robot nhặt rác sống trên trái đất vào thời kỳ mà con người tất cả đều đã chuyển ra ngoài vũ trụ ở. Wall-e sống cô độc một mình với những vật dụng hư cũ, một con gián là người bạn duy nhất của cậu. Nhưng rồi cuộc đời Wall-e thay đổi khi Eve xuất hiện.Eve là một robot thăm dò ngoài trái đất. Ban đầu, cô rất lạnh lùng với Wall-e, nhưng rồi sau đó một tình cảm đặc biệt đã nảy sinh giữa họ. Wall-e theo Eve tới phi thuyền ngoài trái đất, gặp gỡ nhiều robot khác và cả con người. Wall-e gây ra nhiều rắc rối nhưng cũng giúp đỡ rất nhiều người. Cuối cùng, để bảo vệ mầm cây xanh trước robot xấu xa Auto, Wall-e đã bị hỏng hóc nặng. Dù sau đó đã được sửa chữa, nhưng dường như trí nhớ của Wall-e đã biến mất. Chính vào giây phút ta cảm thấy tiếc nuối đó, thì bài hát vang lên về tình yêu đã giúp Wall-e nhớ lại, tạo nên một kết thúc có hậu cùng với việc con người quay về Trái đất. Câu chuyện là minh chứng rõ ràng cho câu nói: Tình yêu là không hề có biên giới. Nó có thể đến từ bất cứ đâu, tác động vào tất cả và làm nên những điều kỳ diệu nhất.
5
291067
2015-01-17 16:45:00
--------------------------
