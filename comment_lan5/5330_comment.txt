374363
5330
Cuốn sách được in màu, dày dặn minh họa đẹp, dễ thương, dễ hiểu. Cuốn sách là cuốn cẩm nang giúp cho bạn gái trở thành nữ hoàng giao tiếp. Cuốn sách này cùng một bộ sách cẩm nang về giao tiếp hiệu quả, duyên dáng với 3 cuốn sách khác của nhà sách Minh Long: Nói nhiều không bằng nói đúng, Khéo ăn nói sẽ có được thiên hạ, nói thế nào để được chào đón-làm thế nào  để được ghi nhận. Ưu điểm của cuốn sách là ngắn gón, xúc tích, hiệu quả, không giông dài, dài dòng khiến cho người đọc có cảm giác nhàm chán. Cuốn sách thu hút ở giọng văn dí dỏm, hình ảnh minh họa hài hước khiến người đọc tiếp thu và ngấm nhanh những thông tin tác giả muốn truyền đạt
5
905408
2016-01-26 08:39:31
--------------------------
351587
5330
Cuốn này khá bắt mắt với chất liệu giấy tốt, nội dung nhiều màu sắc, hình ảnh minh họa sinh động.
Về nội dung thì cuốn này khá hữu ích, mình rất thích nó, cảm thấy nó rất phù hợp vì mình học hỏi được nhiều thứ hữu ích, hay ho. Từ cách dùng từ sao cho hợp lý trong giao tiếp, các thái độ, cử chỉ, nết mặt như thế nào sao cho hợp hoàn cảnh, đến cách lựa chọn trang phục phù hợp.
Cuốn này có thể dùng làm quà cho các em gái mới lớn, hoặc cho những người cần giao tiếp nhiều, hoặc cho những người phụ nữ mà ta yêu thương.

4
513404
2015-12-12 15:05:15
--------------------------
299579
5330
Minh đã biết đến quyển sách này khi mình lang thang ở nhà sách . Mình rất ấn tượng với quyển sách này , cả về hình thức lần nội dung . Về hình thức , sách có trang bìa rất bắt mắt , còn bên trong thì chất liệu giấy rất tốt . Về nội dung , sách giới thiệu nhiều kĩ năng để tạo lập và phát triển các mối quan hệ trong cuộc sống dành cho phái nữ , ngoài ra còn có rất nhiều câu chuyện rất hài hước . Sách rất thích hợp cho các bạn học sinh , sinh viên như mình . Giá ngoài nhà sách khá cao , nhưng nếu mua ở tiki thì mức giá rất mềm . Cảm ơn Tiki vì quyển sách này . Mình sẽ tiếp tục ủng hộ tiki . 

5
650543
2015-09-13 13:08:01
--------------------------
270958
5330
Sức Hút Của Kỹ Năng Nói Chuyện được trình bày khá nhiều khía cạnh của vấn đề từ ngôn ngữ hình thể đến kỹ năng nói chuyện. Sách được trình bày theo lối trực quan sinh động và được in màu khá đẹp trên giấy tốt. Bên cạnh những ưu điểm đó thì mình thấy nội dung cuốn sách trình bày chưa tới, đề cập đến nhiều khía cạnh vấn đề kỹ năng nói chuyện nhưng mỗi vấn đề đều quá qua loa đại khái. Tuy nhiên cuốn sách này vấn đáng để tham khảo và nên đọc cùng những cuốn sách về kỹ năng nói khác để được chuyên nghiệp hơn.  
4
74132
2015-08-18 19:06:09
--------------------------
224401
5330
Cuốn sách này đem lại những bí quyết rất hay mà các cô gái nói riêng và tất cả chúng ta nói chung có thể học hỏi để trở thành một người biết ăn nói, biết giao tiếp giỏi. Khổ sách to, giấy tốt, bên trong lại in màu rất độc đáo (lên màu rất đẹp luôn đó), chỉ cần nhìn thôi đã thấy hứng thú rồi, việc đọc do đó cũng trở nên vui vẻ và dễ dàng hơn ! Cuốn "cẩm nang" hữu hiệu này đưa ra một số trường hợp giao tiếp, những câu chuyện ví dụ mà bạn có thể bắt gặp trong đời sống, qua đó biết áp dụng cách cư xử, cách ăn nói sao cho thuận tình hợp lý. Tác giả không viết vòng vo hay triết lý mà đi trực tiếp vào vấn đề nên dễ đọc, dễ hiểu hơn, các chủ đề rất rõ ràng và bố cục mạch lạc. Tuy nhiên có một số câu chuyện hài chỉ đưa ra để nói ví dụ mà chưa đưa được cách khắc phục. Cuốn sách chưa phải là hoàn hảo nhưng xét tổng thể cũng được coi là hay, đẹp từ cách trình bày tới nội dung !
5
75959
2015-07-08 16:43:21
--------------------------
206217
5330
Sách được biên tập, thiết kế đẹp, kỹ lưỡng với những hình ảnh ví dụ minh họa vui. Thích ở chỗ cách tác giả đề cập thẳng vào vấn đề, đi từng bí quyết một, giải thích rõ hơn cụ thể nên làm gì và tại sao phải làm như vậy. Không như một số sách khác, đưa ra ví dụ những người giao tiếp giỏi và câu chuyện xung quanh cuộc đời họ mà theo tôi là hơi dài dòng không cần thiết. Vui nhất là các bí quyết nói chuyện hài hước, các ví dụ truyện hài trong sách rất hay.
4
303045
2015-06-09 08:09:21
--------------------------
194501
5330
Điểm cộng là chất liệu giấy tốt, màu sắc rực rỡ, bắt mắt, khá hấp dẫn và dễ hiểu. Điểm trừ là nhiều phần còn đặt vấn đề khá chung chung, không có định hướng rõ ràng. Phần cuối đọc như chuyện cười, khá là nhạt nhẽo, đưa ra nhiều ví dụ nhưng không nêu phương pháp cụ thể. Đọc trích đoạn khá ấn tượng nhưng cả sách thì hơi thất vọng. Bạn nào mới đọc sách về kĩ năng sống và giao tiếp thì có thể đọc cuốn này vì nó khá nhẹ nhàng, vào đề trực tiếp, dễ đọc vì không có tí triết lý nào cả,
3
375775
2015-05-10 20:10:12
--------------------------
191792
5330
Lúc cầm quyển sách này trên tay, tôi rất ấn tượng vì giấy tốt và đẹp. Quyển sách viết về cách giao tiếp trong cuộc sống thông qua các mục được chia rất rõ ràng và các hình vẽ rất sinh động, tốt cho những bạn muốn đọc thể loại sách như thế này nhưng ngại đọc nhiều chữ. Các tình huống cho từng trường hợp cũng rất rõ nét, giúp người đoc liên hệ với bản thân và rút ra các kinh nghiệm giao tiếp cho bản thân. Sách bìa cứng, đẹp, nhiều màu sắc bắt mắt, nhìn rất có cảm tình, rất xứng với giá tiền bỏ ra.
5
453577
2015-05-02 19:12:19
--------------------------
166606
5330
Khi tôi còn nhỏ, tôi rất thích đọc cuốn sách này vì theo tôi cuốn sách này sẽ giúp tôi trở thành nữ hoàng trong giao tiếp, tôi đã giàm giụm số tiền tiết kiệm của tôi để mua cuốn sách này và giờ tôi vô cùng vui mừng khi nhận được cuốn sách, tôi đã đọc hết từ A đến z và thấy có rất nhiều điều hay mà trong cuộc sống, vô tình mọi người không chú ý phải, tôi cảm thấy phấn khích vô cùng khi sắp sửa trở thành một nữ hoàng giao tiếp tài ba, sau này tôi cũng mong muốn trở thành nhà ngoại giao tài giỏi
5
556089
2015-03-12 23:07:56
--------------------------
