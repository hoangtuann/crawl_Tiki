483759
7793
Sau khi đọc một số bài trong sách mình có tí nhận xét. 
* Ưu điểm: 
- Sách khá dày, giấy cũng không quá mỏng.
- Sách tập hợp rất nhiều bài luận mẫu với nhiều chủ đề khác nhau.
- Sách phù hợp cho mọi trình độ, thích hợp cho những bạn ôn thi B1, B2 (CEFR), tham khảo cách viết luận và viết thư.
* Khuyết điểm:
- Phần từ vựng sau mỗi bài thấy chưa hợp lý lắm, từ dễ thì giải nghĩa còn một số từ khó thì không giải nghĩa. 
- Các bài luận không sắp xếp theo thứ tự từ trình độ từ thấp đến cao. 
4
675686
2016-09-17 01:36:49
--------------------------
430461
7793
Về cuốn sách Tuyển Tập 556 Bài Luận Mới Sơ Cấp, Trung Cấp Và Nâng Cao thì mình có nhận xét như sau : Sách dày như cuốn từ điển, Sách có rất nhiều đề văn phong phú, tới tần hơn 550 đề. Rất tốt cho việc tham khảo. Mỗi đề văn là đều có bài văn tiếng Anh, có kèm dịch tiếng Việt, có kèm cả một list từ vựng giúp người học không phải mất thời gian tra cứu. Điểm trừ là mới chỉ là các bài văn liệt kê nội dung thôi chứ chưa có những phân tích bố cục, cách thức làm bài, nên những bạn nào mới học thì sẽ bối rối không biết bài văn viết theo dạng gì. Và điểm trừ là sách in mực chưa đều, trang mờ  trang đậm, như sách lậu vậy @@ nhưng dù sao cũng cho 4 sao.
4
1348984
2016-05-15 10:22:44
--------------------------
399622
7793
ĐẶC ĐIỂM: 
Quyển sách gồm nhiều bài luận, đa dạng chủ đề, đầu mỗi văn bản đều có mục Plan ghi các luận điểm của mỗi đoạn, giúp dễ hình dung cấu trúc của bài. Một số bài đầu có giải nghĩa từ mới và dịch luôn bài luận nhưng về sau thì không thấy nữa.

Mình -1 sao vì quyển sách mình nhận không được đẹp. Mép không thẳng, gáy sách nhăn và bong tróc (không nhiều nhưng rất mất thẩm mĩ). Còn về nội dung thì rất vừa ý vì mình chỉ mua để luyện đọc hiểu tiếng Anh. Nếu bạn muốn tìm tư liệu làm quen cách hành văn hoặc mục đích giống mình thì quyển sách này rất thích hợp.
4
855934
2016-03-17 23:33:00
--------------------------
337076
7793
Hồi sinh viên có lần chui vô thư viện kiếm ý viết bài thì thấy cuốn này. Thế là đứng đọc miết tới hết giờ phải về. Những bài viết trong này có những ý tưởng rất là hay để bạn có thể học theo và tạo ra những bài viết độc đáo khác của riêng bạn. Mình qua thời cần dùng sách này rồi nhưng mình vẫn mua nó để dành cho con em sau này đọc. Ngày xưa cầm cuốn sách cũ, giấy vàng, đôi chỗ bị rách long cả tờ giấy ra, nhưng nay thì đã có sách mới tinh và giấy trắng sạch sẽ. Một cách để nâng cao kỹ năng viết tiếng Anh là đọc càng nhiều tiếng Anh càng tốt. Bạn sẽ không bao giờ thấy  cuốn sách này thừa đâu.
5
143507
2015-11-13 15:37:12
--------------------------
282936
7793
Đây là một cuốn sách có giá thành khá rẻ so với độ dày hơn 500 trang của nó. Do mình đặt hàng qua mạng nên không có điều kiện xem kỹ sản phẩm, khi về mới thấy cuốn sách có các ưu nhược điểm sau:
Ưu điểm: giá rẻ, nhìn chung các bài đều đúng từ vựng ngữ pháp.
Nhược điểm:
1. cuốn sách chưa có bố cục rõ ràng, không phân chia thành chủ đề cụ thể, các bài luận mức độ khó dễ bị sắp xếp lộn xộn.
2. chỉ dịch nghĩa ở một vài bài đầu, còn sau thì không có dịch tiếng việt nữa. Thế là mình mất công đi tra từ.
3
652942
2015-08-29 16:02:02
--------------------------
280688
7793
Sách dày và có rất nhiều các chủ đề, đề bài luận gần gũi và đa dạng. bên dưới có bổ sung lượng từ vựng mới, các ý chính, gợi ý để người làm dễ tiếp thu. Các bài đầu có dịch nghĩa tiếng Việt chi tiết, nhưng đến mấy bài sau thì không dịch nữa, nên thấy cũng không rõ ý cho lắm. Sách trình bày tương đối rõ ràng, cụ thể, có thể tham khảo tốt cho các bài luận đơn giản đến nâng cao. Hy vọng nó sẽ giúp ích đáng kể cho tôi.
4
754890
2015-08-27 21:13:59
--------------------------
267644
7793
Mình thấy quyển sách này tạm được. Chủ đề viết luận trong sách thì rất nhiều, phong phú, những 556 bài, xem hoài không hết luôn. Nhiều chủ đề hay, giúp luyện sự sáng tạo rất tốt như nhìn tranh cho sẵn rồi viết bài về những hình đó, sử dụng từ cho sẵn rồi viết thành câu chuyện tuỳ ý, viết bài dựa theo cốt truyện và bản đồ có sẵn, nhiều bài dạy viết thư... Một số bài có dịch ra Tiếng Việt cả bài, ghi chú thích từ mới, rất dễ hiểu. Tuy nhiên sách chỉ có những bài mẫu thôi chứ chưa hướng dẫn cụ thể cách viết luận, chưa có dàn bài. Mình thấy sách chỉ phù hợp với trình độ viết luận nâng cao, còn với những bạn mới tập tành viết luận thì có thể hơi khó hiểu.
3
118307
2015-08-15 18:54:33
--------------------------
238241
7793
Sách dày, khá đầy đủ về những bài văn writing. Song vì đầy là sách tuyển tập những bài luận văn nên không hướng dẫn cụ thể về cách viết trình bày cho lắm, có giải thích từ vụng nhưng không đầy đủ. Mỗi bài luận chỉ viết ra những không giải thích bố cục, cách tìm hiểu đề tài...Bạn nào muốn tham khảo thì được còn hướng dẫn viết cụ thể hơn thì không đủ. Hơn nũa mình thấy sách này dùng để tham khảo dành cho các bạn năng lực tiếng anh sơ cấp hơn vì chưa nâng cao cho lắm.
3
328945
2015-07-23 07:19:16
--------------------------
225773
7793
Sách giới thiệu tuyển tập Essay, nội dung các essay khá đa dạng, phong phú, có thể cho một vài ý tham khảo được. Song những essay trong sách chưa thực sự sát chủ đề và tuân thủ theo các quy tắc viết luận, giới thiệu bài mẫu nhưng không đưa ra outline định hướng làm bài. Sách cung cấo nhiều chủ đề về mặt xã hội và bài báo sưu tầm, nhưng thiếu phần các loại văn bản hành chính, đơn từ tiếng Anh. Có thể nói quyển sách này giống như quyển tổng hợp các bài văn mẫu vậy. 
3
618122
2015-07-10 21:56:28
--------------------------
132893
7793
Mình là học sinh học tiếng anh và đang yếu phần writing,nên khi thấy sách trên tiki mình đã nhanh chóng tậu nó về để học.quyển sách rất dày,556 bài luận cơ mà ^^Sách trình bày kỹ các kỹ thuật mới và kỹ năng ứng dụng ngôn ngữ trong viết luận cùng các hướng dẫn phân tích cụ thể, rõ ràng cho từng loại Essay kèm các ví dụ sinh động. Phần 2 trình bày 556 bài Essay và Writing mới được tổng hợp từ các kỳ thi gần đây. Các bài luận Anh văn gồm luận hình ảnh, miêu tả, tường thuật, phân tích, bình luận tập trung vào các chủ đề quan trọng, thực tế trong cuộc sống hàng ngày, mình thấy có tính thực tiễn rất cao, phù hợp với tình hình phát triển xã hội hiện nay. Ngoài ra sách còn có phần chú giải từ khó, Essay plan và Essay comment để hiểu được tác dụng của các phần khi viết luận.Mình thấy kĩ năng writing của mình đã khá hơn.cảm ơn tiki nhiều!
5
168271
2014-11-02 20:19:25
--------------------------
