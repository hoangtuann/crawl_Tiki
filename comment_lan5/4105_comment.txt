452079
4105
Từ lâu minh vốn không ưa lịch sử, nhưng tất nhiên là từ năm lớp 7, bởi vì lịch sử lớp 6 nói thật là ngắn, dễ hiểu, ít thời gian, chủ yếu toàn là nội dung sự kiện do khoảng thời gian học trong sách là từ thời xa xưa nên cột mốc thời gian khó xác định. Thêm nữa là  hồi xưa mình học một cô giáo rất vui tính nên lịch sử bớt khô khan hẳn đi. Mặc dù vẫn có nhiều câu hỏi và bài tập trong sách giáo khoa cũng như cô giáo giao về nhà hơi khó. Đó là lúc mình cần cuốn sách này.
5
1420970
2016-06-20 13:56:37
--------------------------
303932
4105
Lịch sử từ lâu đã được xem như là một môn học "khó nuốt", câu chữ dài dòng và khó nhớ đối với các bạn học sinh. Chính điều này đã gây nên tình trạng việc học Lịch sử trở thành một nỗi sợ hãi ngay trong nhà trường, không chỉ đối với các em học sinh mà còn là tình trạng chung của nhiều thầy cô giáo giảng dạy và các bậc phụ huynh. Quyển sách này ra đời chính là cứu cánh thích hợp dành cho những bạn học sinh cảm thấy môn Lịch sử khiến ta lúng túng bởi cách hướng dẫn của người viết rất rõ ràng và khoa học, bám sát nội dung ở sách giáo khoa theo chương trình khung của Bộ Giáo dục và Đào tạo dành cho cấp học Trung học Phổ thông, đồng thời hướng dẫn các bạn trả lời các câu hỏi trong bài. Thông qua việc học tập một cách có hệ thống như trên, môn Lịch sử sẽ trở nên dần thú vị và hấp dẫn. Điều này sẽ tác động không nhỏ đến tâm lý người học, giúp đạt được thành tích và kết quả cao, góp phần nâng cao chất lượng giảng dạy và học tập môn Lịch sử trong nhà trường. Đây chắc chắn sẽ là một người bạn thân thiết của các bạn học sinh lớp 6.
4
765212
2015-09-16 07:54:06
--------------------------
