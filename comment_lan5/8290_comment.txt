430096
8290
Tôi rất thích đọc truyện ngắn của Trang Lax & Hi Trần nên đã không ngần ngại đặt mua cuốn sách này. Nhưng có lẽ đây là những tác phẩm trong thời gian đầu sáng tác của 2 tác giả nên nhiều chỗ tôi vẫn thấy sự vụng về và hơi gượng ép. Mỗi câu truyện lại có những thông điệp riêng nhưng không được thể hiện khéo léo và đôi khi không rõ ràng. Bởi vậy tôi hơi buồn một chút. Tôi thích những tác phẩm sau này của 2 tác giả hơn :3
Bìa sách rất bắt mắt, bên trong bố trí các phần cũng rất dễ nhìn. Tôi thấy hài lòng về hình thức hơn nội dung.
3
548594
2016-05-14 14:54:11
--------------------------
403299
8290
Mình rất thích cuốn sách này. Cuốn sách là kết hợp của hai tác giả mình vô cùng yêu thích nên mình càng cưng nó hơn nữa. Những tranh giấy, mở ra những câu chuyện, những cuộc đời. Đâu đó là niềm vui, lạc quan, là niềm yêu cuộc sống, đâu đó là vài vấp ngã, là bồng bột, là tuổi trẻ. Mình thích truyện của anh Tiến lắm, đọc hoài đọc hoài không chán, mà đổi lại chỉ thấy nhẹ lòng. Chị Lax viết cũng rất hay.
Tuy nhiên cuối mỗi truyện không ghi tên tác giả. Vì mình đọc nhiều văn của chị Lax và anh Hi nên mình đoán được. Yêu anh tác giả lắm.
5
433772
2016-03-23 16:06:25
--------------------------
351566
8290
Những mẩu chuyện ngắn nhưng rất dễ thương! Dường như tôi thấy hình ảnh của chính mình trong đó. Những điều tưởng như nhỏ nhặt và ít ai quan tâm đã được Trang Lax và Hi Trần dùng ngòi bút khắc họa sống động! Mỗi câu chuyện đều mang một thông điệp rất đáng yêu! Đọc xong vẫn muốn đọc lại thêm nhiều lần nữa! Như lá lộc vừng xoay trong gió đã thật sự "xoay tít" trong lòng tôi! Nhắc nhớ thật nhiều về một thời ngây ngô với những tình cảm đầu đời khó lí giải nhưng là tình cảm đẹp nhất, trong sáng nhất!!!!
4
572233
2015-12-12 14:23:03
--------------------------
334983
8290
Mình mua sách vì tựa đề =)) phần cũng vì ấn tượng với tác giả Hi Trần qua các truyện ngắn đăng trên báo HHT và Trà sữa cho tâm hồn. 
Bìa sách khá đẹp, trình bày bắt mắt.
Hầu hết các truyện ngắn trong sách đều viết về tuổi học trò hồn nhiên, ngây thơ,vài mối tình nho nhỏ vừa chớm nở, rất đáng yêu. Nhưng sách vẫn chưa thật sự ấn tượng, chưa để lại được dư âm sâu sắc cho người đọc. 
Dù sao vẫn luôn ủng hộ hai tác giả,mong thấy được sự đột phá trong những đầu sách sau. 
2
847088
2015-11-10 12:41:35
--------------------------
269284
8290
Như lá lộc vừng xoay trong gió ấn tượng với tôi bởi cái tên rất lãng mạn và bìa rất xinh xắn. Trong tất cả 18 truyện ngắn của hai tác giả Trang Lax và Hi Trần thì tôi thích nhất là Trái tim có nhiều ngăn hơn ta tưởng của Trang Lax và La Tomatina của Hi Trần. Với Trái tim có nhiều ngăn hơn ta tưởng, tôi đồng cảm với Quỳnh trước nỗi sợ sẽ phải chia xavì đã học những năm cuối cấp. Còn với La Tomatina tôi thấy thích thú trước cô bạn đũa không cùng đôi ấy, với cá tính rất riêng và rất yêu. Mà cũng có lẽ vì tôi có phần giống cô ấy nữa. Chờ đợi cuốn tiếp theo của hai tác giả
5
182813
2015-08-17 11:16:01
--------------------------
262835
8290
Tuổi thanh xuân,ai cũng có cho riêng mình một phần kỉ niệm.Đó có thể là kỉ niệm về tuổi thơ,về gia đình,và đáng nhớ hơn cả là những kỉ niệm về một thời áo trắng hồn nhiên,trong sáng và đầy mơ mộng."Như lá lộc vừng xoay trong gió" là một cuốn sách như vậy.Hai tác giả Trang Lax và Hi Trần đã đem đến cho chúng ta những câu chuyện sinh động,hấp dẫn và đầy cảm xúc,đưa ta trở về với những kí ức tươi đẹp.Hãy tìm đọc và cảm nhận nó bạn nhé!
5
717650
2015-08-12 09:59:17
--------------------------
246926
8290
Lúc đặt mua cũng chỉ đọc mỗi phần giới thiệu do ấn tượng về bìa, về đồng tác già của quyển này và hơn nữa là ấn tượng về lá lộc vừng. Chẳng biết người khác thế nào nhưng với mình lá lộc vừng cứ như nhắc đến khoảng thời gian cắp sách đến trường với bao nhiêu nặng động tinh nghịch và hiện tại là nhớ nhung. Và gần đúng với tưởng tượng là những câu chuyện trong đây như làm cho mình nhìn thấy lại khoảng kí ức hồn nhiên ấy và có những câu chuyện có kết thúc chưa hoàn hảo như mình mong muốn nhưng đó đều là những kinh nghiệm quý báu những cảm xúc thật nhất. 
4
538292
2015-07-30 09:34:10
--------------------------
243784
8290
Cuộc đời với những vòng xoay hối hả có những lúc ta lãng quên những một phần ký ức nhưng có những ký ức sẽ không bao giờ ta có thể lãng quên. Đó là những ký ức về một thời áo trắng, vụng dại, ngây ngô mà cũng mơ mộng. Bức tranh thời thơ ấu sẽ mãi là hành trang cho ta trên bước đường đời chông gai phía trước. Đây là tuyển tập truyện ngắn của hai tác giả mà tôi thích nhất, đặc biệt với truyện Trái tim có nhiều ngăn hơn ta tưởng, nhẹ nhàng mà sâu sắc!
5
479601
2015-07-27 18:32:34
--------------------------
236477
8290
Luôn trong sáng, linh động và “hời mà sâu” là những gì Lax viết. Kể gây “nghiện”, vẽ nỗi buồn tuyệt đẹp và “già trong trẻ” là chất văn của Hi Trần. Tôi thấy sự kết hợp này rất hay. Tư duy của hai tác giả không “y chang”, cũng không chống nhau mà dung hòa thú vị. Tôi có thể cảm thấy cá tính của mỗi người trong từng tác phẩm không viết tên tác giả. Khi thì cực kì dễ thương, ngọt ngào, dí dỏm mang thông điệp nhân văn tích cực mà không hề đá động đến đạo lí dài ngoằng hay bất kì thứ triết lí nào của Lax. Lúc thì cuốn hút bởi lối dẫn truyện thật thà, thông minh của Hi. Tôi đã đọc vèo cả quyển sách mà chẳng bị tụt cảm xúc hay vấp phải sự khó chịu nào. 
Phần nhìn, tôi rất thích màu đỏ của bìa sách. Trình bày mục lục, minh họa cũng khá dễ thương. Tuy nhiên, font chữ sử dụng nhiều kiểu nhưng thiếu hài hòa, không đẹp mắt lắm. Thêm nữa, việc không chia phần các tác phẩm cân bằng, rõ ràng hoặc đề tên tác giả ở mỗi truyện hơi làm khó cho độc giả mới làm quen với hai cây bút ấy.
Như thời cắp sách đáng nhớ được ghi lại dưới ngòi bút của hai tác giả, tôi tin cuộc sống sẽ cứ xoay, mầm cây vẫn lớn và tim mình cứ nhảy múa trong lồng ngực. Ấm và đỏ.
Cảm ơn. 
4
385533
2015-07-21 20:22:50
--------------------------
175612
8290
Là những câu chuyện đời thường nhưng đầy triết lý.
Cuốn sách này đã mang đến cho tôi những hoài niệm, những cảm xúc hỗn độn và nỗi buồn mang mác khi nhớ tới những người bạn cũ, những kỷ niệm thời học trò không thể nào xóa nhòa. “Như lá lộc vừng xoay trong gió” là bức tranh kỷ niệm được vẽ bằng ghế đá, hàng cây, bằng những con đường nhỏ, bằng tiếng cười giòn giã của bạn bè, bằng những bịch bánh tráng me chia nhau nho nhỏ... Đó là những ký ức của quá khứ, hiện tại, tương lai, mà tất thảy chúng ta đều đã và đang có, khi khoác trên người tấm áo học sinh trắng ngần.
Lối viết giản dị, sâu lắng của Trang Lax và Hi Trần đã đem đến cho người đọc những cảm xúc nhẹ nhàng, sâu lắng. Điểm trừ duy nhất của cuốn sách là có nhiều truyện gần gần giống nhau khiến mình rất bực. Tuy vậy thì đây vẫn là một cuốn sách hay và đáng đọc.
4
445522
2015-03-30 17:40:02
--------------------------
169173
8290
Mình biết đến Trang Lax và Hi Trần qua những truyện ngắn trên báo Trà sữa. Và cũng đã biết đến "Như lá lộc vừng xoay trong gió" khá lâu rồi. Đây là cuốn truyện tuy đồng tác giả nhưng nhìn vào trong đó vẫn có thể dễ dàng nhận ra những nét riêng của từng cây bút trẻ. Những câu chuyện lãng mạn và gần gũi với giới trẻ khiến hai tác giả ngày càng có một vị trí nhất định trong lòng người đọc. Chúc cho hai bạn sẽ sớm có nhiều phá cách hơn nữa trong những trang viết của mình trong tương lai. Cảm ơn tiki đã giúp mình sở hữu được cuốn sách này trong thời gian ngắn nhất, ít rắc rối nhất, và thân thiện nhất!
3
208200
2015-03-17 20:02:37
--------------------------
167769
8290
Mình biết Lax và Hi Trần qua HHT, mua quyển sách này vì điều đó. Bìa vẽ đẹp. Nhưng nội dung làm mình hơi thất vọng, không phải tác giả viết không hay, mà nhiều truyện mình thấy cứ gần gần giống nhau. Hơn nữa, một tập in chung 2 tác giả mình tưởng mỗi tác giả phải 50/50 chứ không nghĩ lại là 30/70 như vậy, mà truyện Trang lax áp đảo Hi, vô tình làm cụt hứng khi đọc vì quá nhiều cái quá quen như: nhân vật XOAY ĐI XOAY LẠI trong 1 vài truyện, ngôi kể chủ yếu ngôi 3. Hơi ngán. Mong rằng, Trang lax & Hi Trần ra hai tập sách riêng.
3
190083
2015-03-15 13:23:03
--------------------------
157175
8290
Cuốn sách Như Lá Lộc Vừng Xoay Trong Gió là hay hay nhất đó.Bởi vì đối với mình hai tác giả Trang Lax và Hi Trần đều là những tác giả thực lực và cuốn sách trên đã thể hiện điều đó. Xuyên suốt mỗi câu chuyện là giọng văn chau chuốt rất mượt không hề khô khan và đặc trưng bởi một chút giọng điệu bất cần đời và ngôn ngữ teen quá chất. Bìa sách cũng rất ấn tượng đấy vì họa sĩ vẽ hoa lộc vừng đẹp quá. Mong là sắp tới Trang Lax và Hi Trần sẽ ra nhiều sản phẩm hơn nữa
5
349220
2015-02-07 21:11:53
--------------------------
155149
8290
Đọc những nhận xét ở dưới mình cảm thấy nó đúng đó, đại diện cho phần đông bạn đọc. Mà tiếc quá, mình là cái người nằm ngoài phần đông.
Mình rất rất ( cực ) thích cách viết của Hi Trần và Trang Lax. Thậm chí có thể coi là nghiện. Bệnh nghiện nỗi buồn. 
Đa phần câu chữ trong chuyện rất lắng ( kiểu mang chiều sâu ). Người ôm nhiều nỗi buồn trong người như mình dễ bị cảm nắng những câu chữ ấy. Càng đọc càng yêu.
Nói chung, truyện phù hợp với kiểu người bình lặng nhàn nhạt như mình chứ không dành cho típ người muốn khám phá mới mẻ hay tìm kiếm gì đó độc đáo. Bạn nào giống mình thì nên mua. Không thì mua về, sách cũng mốc ở trong nhà. Điều đó thật không xứng đáng với một cuốn sách hay.
4
230052
2015-01-31 12:09:37
--------------------------
153816
8290
Cá nhân mình rất thích các truyện ngắn Lax viết, một phần vì tác giả đã trưởng thành nên truyện viết dù đậm chất học trò vẫn có nhiều chiêm nghiệm sâu sắc, lối viết dung dị nhưng dí dỏm, đáng yêu. Mình đã rất mong chờ tác giả sẽ ra mắt một tập sách chọn lọc dày dặn hơn quyển này. Mình cảm thấy kết hợp với Hi Trần làm tổng quan các truyện trong sách nhẹ nhàng đi rất nhiều và không có điểm nhấn. Các truyện hầu hết đã đăng trên Trà Sữa và HHT rồi, điều này thì hầu như mấy tác giả trẻ đều thế nên không thể nói gì được.
Một điều nữa là minh họa trong sách không được đầu tư, khá cẩu thả. Mình nghĩ tác giả chỉ in mình truyện chữ cũng hay mà.
3
201502
2015-01-27 16:17:25
--------------------------
149425
8290
Điều đầu tiên tôi cảm thấy không hài lòng ở quyển sách này là hai tác giả này kết hợp lại có lẽ không thích hợp cho lắm, và không ghi rõ truyện ngắn nào của ai, mặc dù đọc thì có thể đoán ra được.
Điều thứ hai tôi cảm thấy không được vui là có kha khá các tác phẩm truyện ngắn trong quyển sách này đã được báo Hoa học trò và Trà sữa cho tâm hồn in ra rồi, nên khi bắt gặp những điều mình đã biết trong một quyển sách mới toanh thì khó chịu kinh khủng. Tôi muốn được đọc nhiều thứ mới mẻ hơn!
Bìa sách đẹp nhưng sách hơi mỏng, chất lượng sách tốt! Những truyện ngắn nhẹ nhàng và truyền tải thông điệp cũng nhẹ nhàng không kém, có khi nhẹ quá nên vẫn chưa cảm nhận được sâu sắc lắm, truyện viết theo kiểu teen story bình yên, đôi lúc hơi nhạt nhẽo nhưng cũng có phần sáng tạo. Hy vọng sau này có thể đón đọc những quyển sách mới mẻ hơn từ hai tác giả có thể nói là một trong những gương mặt tác giả trẻ hiện nay!
3
252073
2015-01-13 18:05:26
--------------------------
122115
8290
Tôi chọn mua cuốn sách này bởi cái tựa đề nghe hay hay, bìa sách cũng khá là bắt mắt.
Nhìn chung thì cuốn sách không đến nỗi tệ nhưng chưa đủ sức để gây ấn tượng và dư âm cho người đọc. Những mẩu chuyện mà 2 tác giả đưa vào rất nhẹ nhàng và có chút mới mẻ, tạo được những cảm giác lắng đọng của tuổi trẻ, của tình bạn, của thời gian, của quãng đời học sinh nhiều màu sắc :)
Có lẽ mỗi người một sở thích khác nhau, nhưng tôi không phù hợp với văn phong của Trang Lax & Hi Trần, tôi cần thêm những thông điệp, những nhắn nhủ rõ ràng hơn bộc pháp lên từ cốt truyện
Mong là với những tác phẩm sau, 2 tác giả sẽ thành công hơn và truyền cảm hứng tốt hơn đến người đọc :)
3
248457
2014-08-23 12:09:20
--------------------------
