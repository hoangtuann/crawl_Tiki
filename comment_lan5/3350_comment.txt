564397
3350
Khi mua cuốn sách này, tôi thực sự hi vọng nó sẽ mang lại một bước ngoặt để bứt phá về môn lý. Cho đến bây giờ, cuốn sách này chưa làm tôi thất vọng vì sự đa dạng của đề thi trong phần bài tập và lý thuyết. Hi vọng cuốn sách này sẽ đem lại sự hài lòng cho nhiều bạn đọc khác.
5
322229
2017-04-04 21:45:20
--------------------------
443969
3350
Sách có thiết kế bao bì đẹp, chất lượng giấy tốt đây là điểm mạnh để thu hút người mua từ cái nhìn đầu tiên. Khi mở vào những trang đầu của sách là những lời nói lợi ích đặt ra mục tiêu giúp cho học sinh định hướng trước cho tương lai và có động lực vượt qua những khó khăn những bài tập khó mà môn vật lí mang lại. Ngoài ra, sách không những giải chi tiết từng bài tập mà sách còn có những công thức mẹo rất hay. Qua mỗi đề có một câu chuyện về những thành tựu của vật lí đem lại cho đời sống chúng ta hiện nay.
5
1377125
2016-06-07 18:13:02
--------------------------
411819
3350
Cảm nhận đầu tiên là trọn bộ có bao bì đẹp, cảm quan tốt. Em mình đang học lớp 11, cậu ấy học yếu. Lúc đầu mình đặt cứ tưởng là chuyên đề luyện thi, ai dè là bộ đề , hic, với trình độ cậu ấy thì sách chưa phù hợp. Nhung mình sẽ tham khảo dần. Mình đặt cả bộ hoá học nữa. Hi vọng em mình sẽ học tiến bộ hơn và đậu đại học nhờ sự hỗ trợ của bộ sách này. Giá cả tiki không đắt, đóng gói cẩn thận , bộ này giao cũng nhanh lắm.
4
970689
2016-04-06 18:01:29
--------------------------
397311
3350
Đây là một cuốn sách chi tiết, cặn kẽ mà mình cảm thấy thực sự thích luôn.Vật lý là một môn học khá khó để dành được điểm tuyệt đối, điểm giỏi.Tuy nhiên cuốn sách này lại tích hợp các loại bài tập với những lời giải chi tiết, tỉ mỉ, nội dung được cập nhật phù hợp với kì thi trung học phổ thông quốc gia.Hình thức cuốn sách rất đẹp, vì nó được đặt trong bookset mà. Mình thích lắm. bày ra giá cực kì đẹp, chất lượng giấy tốt, trình bày trong cuốn sách cực kì đẹp, thu hút mình lắm luôn.Hơn hết  mình rất có cảm tình với việc giao hàng với đóng gói của Tiki luôn. Cám ơn Tiki.
5
747943
2016-03-14 19:58:59
--------------------------
394076
3350
Bìa sách đẹp, bên trong in khá rõ ràng, nội dung thì khá là đầy đủ, bài tập rất hay, cách giải dễ hiểu, cặn kẽ. Phần mở đầu của sách khá ấn tượng, mấy bạn mua rồi có thể đọc. Ở mỗii đề trong sách có phần lời giải rất chi tiết, dễ hiểu, mấy bạn có thể xem đáp án. Đối với mấy bạn mua sách này rồi thì biết, có nhiều công thức, nhiều nhiều dạng bài tập và nhiều phương pháp giải mới rất hay. Tuy nhiên sách có nhiều chỗ không cần thiết. Nhưng nói chung thì mình rất hài lòng.
4
1075780
2016-03-09 21:23:49
--------------------------
383166
3350
Đây là một trong những cuốn sách luyện giải đề thi đại học mình hài lòng nhất. Mình đã mua trọn bộ sách này bao gồm toán, lý, hóa, sinh, anh văn. Cuốn nào cũng hay hết. Sách có ưu điểm mà những cuốn sách khác không có đó là phần đầu giúp học sinh định hướng ước mơ và mục tiêu, hướng dẫn cách thực hiện mục tiêu hiệu quả, tạo động lực và niềm tin thực hiện mục tiêu đã đề ra. Một cuốn gồm 10 đề, có đáp án và giải thích rất chi tiết, khiến việc ôn luyện trở nên dễ dàng hơn bao giờ hết. Xứng đáng 10 sao. Mà tiki chỉ có 5 sao là cao nhát nên mình cho 5 sao vậy. Hihi
5
393766
2016-02-20 11:41:58
--------------------------
339670
3350
Về hình thức, sách khá đẹp, giấy dày, trắng sáng, không mỏi mắt nếu nhìn lâu, trình bày hợp lý, đẹp. Về nội dung, sách rất bổ ích cho các bạn học sinh đang ôn thi kì thi THPT Quốc gia. Mỗi đề đều có lời giải chi tiết, giảng giải cụ thể, giúp chúng ta hiểu được bản chất mỗi bài toán, các dạng bài tập tiêu biểu, cuối mỗi bài toán đều có chia sẻ những kinh nghiệm để chúng ta học tập. Ngoài ra cuối mỗi đề đề có những câu chuyện thú vị về các nhà khoa học nổi tiếng. Cuốn sách khá tuyệt vời.
5
539865
2015-11-18 14:43:03
--------------------------
327437
3350
Sau Hóa Học có lẽ Vật Lý là một môn thi khá khó để đạt được điểm tuyệt đối cũng như mức điểm giỏi, nhất là ở kì thi THPT Quốc Gia năm 2015 và 2016 tới đây. Mình khá yêu thích môn Vật lý song cũng chưa thực sự làm tốt các dạng bài tập của bộ môn này nên quyết định mua Tuyệt đỉnh luyện đề thi THPT Quốc Gia môn Vật Lý để về tham khảo các dạng bài tập cũng như tiếp cận gần hơn tới đề thi THPT quốc gia bằng cách giải các đề trong sách. Mình khá ưng vì sách được trình bày đẹp mắt, về phần này mình rất hài lòng với MC Books từ những cuốn sách ôn tiếng anh của mình trước đây. Sau mỗi đề thi sẽ là cách giải chi tiết giúp mình đối chiếu được kết quả một cách chính xác cũng như hiệu quả. Nói chung mình khá hài lòng với nó
4
578865
2015-10-27 21:13:08
--------------------------
304401
3350
Sách là combo 2 quyển tập 1 và tập 2 gồm hơn 20 đề thi thử đại học được biên soạn theo cấu trúc đề thi thpt quốc gia năm 2015 (và có lẽ còn của cả vài năm tiếp theo) với lời giải chi tiết, rõ ràng không khác gì lời thầy cô giảng. Qua mỗi đề còn có phần những chú ý được rút ra từ bài tập trong đề (do tác giả lưu ý giúp và cả phần để mình tự viết những điều mình cảm thấy cần chú ý). Hình thức sách cũng rất bắt mắt, giúp học sinh cảm thấy hứng thú hơn. Tặng sách 3 sao!
3
794642
2015-09-16 13:15:34
--------------------------
283548
3350
Bộ sách luyện đề thi THPT Quốc Gia môn Vật Lý làm mình khá hài lòng. Các kiến thức trong sách được trình bày khá chi tiết, khoa học và dễ hiểu. Bộ sách gồm hai cuốn với thiết kế khá đẹp, chất lượng từ hình thức (giấy in màu đẹp mắt), chất liệu giấy, trình bày đẹp mắt tới những kiến thức phong phú, hữu ích cùng tổng hợp các đề, cách giải chi tiết xuyên suốt cả bộ sách. Đây chắc chắn là một cuốn sách thiết yếu cho những bạn học sinh sắp bước vào kì thi THPT Quốc Gia như mình.
4
417795
2015-08-30 06:45:29
--------------------------
282999
3350
Quyển sách trọn bộ 2 cuốn Tuyệt đỉnh ôn thi kỳ thi trung học phổ thông quốc gia :Sách với chất liệu tốt, giấy dày, in rõ đẹp. Đầy đủ nội dung để luyện thi kỳ thi Trung học phổ thông  quốc gia. Có lời giải dễ dàng xem, dễ hiểu,với giá thành phải chăng, phù hợp với học sinh. Với trọn bộ 2 quyển sách này, các bạn có thể sẽ đủ kiến thức để thi trong kỳ thi trung học phổ thông quốc gia. Cá bạn nên mua và nên chia sẽ với mọi người để cùng xem
4
774108
2015-08-29 16:52:47
--------------------------
274870
3350
Phải nói bộ sách Tuyệt Đỉnh Luyện Đề Thi THPT Quốc Gia môn Vật lí trọn bộ 2 tập là một bộ sách hữu ích với mỗi bạn học sinh đang trong thời gian ôn thi cho kì thi THPT QG sắp tới. có thể nói đây là bộ sách tổng hợp các đề thi và cách hướng dẫn giải chi tiết, đọc sách dễ hiểu. Với giá thành phải chăng, đây là một bộ sách thích hợp với những bạn có phương hướng dự thi môn Vật lý trong kì thi tới. 
4
63711
2015-08-22 12:48:50
--------------------------
274181
3350
Đã đặt mua trọn bộ 2 quyển sách này. Cảm nhận đầu tiên là sách trình bày rất đẹp, chất lượng in ấn rất tốt. Về nội dung: tác giả cập nhật tốt các dạng bày tập trong cấu trúc đề thi năm 2015. Có nhiều dạng bài tập và lời giải chi tiết, phù hợp cho việc ôn luyện của học sinh. Sách cũng đáp ứng được nhu cầu tham khảo của giáo viên trong công tác ôn thi THPT cũng như ôn thi HSG.
5
753175
2015-08-21 19:02:00
--------------------------
267072
3350
Lúc nhận được sách mình rất bất ngờ bởi hộp đựng hai quyển sách, cứ tưởng Tiki sẽ để hai quyển sách ra riêng nào ngờ lại đóng 2 quyển sách vào chung  một hộp. Mình rất thích hộp sách đó. Về nội dung 2 quyển sách, mỗi quyển là một tập riêng, ở mỗi tập đã trình bày và giải 10 đề theo mức độ tăng dần, từ cơ bản đến nâng cao, tổng cộng "Tuyệt Đỉnh Luyện Đề Thi THPT Quốc Gia Môn Vật Lí (Trọn Bộ 2 Tập)" đã giải 20 đề, rất cụ thể, chi tiết các bạn nhé. Nếu luyện hết 2 tập này chắc cũng được 9 điểm khi thi môn Vật Lý ấy chứ. Chất liệu giấy rất tốt, mềm và mịn, sờ vào rất thích. Giá 2 cuốn chỉ cỡ 150k, tính ra cũng không đắt lắm
5
637158
2015-08-15 10:28:21
--------------------------
249579
3350
Mình mua combo 2 quyển này để luyện đề ôn thi thpt quốc gia. Mình thấy khá thích. Sách trình bày rất đẹp, bắt mắt, cả bìa lẫn nội dung bên trong, rõ ràng, dễ hiểu. Lời giải khá đầy đủ, chi tiết. Khá ổn. Sách được xuất bản năm 2015 nên cập nhật được xu hướng mới, không lỗi thời. Phần đầu sách tập 1 có lời khuyên thành công, cách xác định mục tiêu, đầu sách tập 2 là một mẩu chuyện khoa học. Mình thấy sách của MCBooks đều trình bày đẹp, gây thiện cảm cho người đọc. Mình nghĩ combo này sẽ là một gợi ý hay cho những bạn ôn thi thpt quốc gia môn vật lý.
5
528600
2015-07-31 22:43:11
--------------------------
245101
3350
Mình chuẩn bị bước vào lớp 12 và đối mặt với kì thi thpt quốc gia nên muốn tìm mua sacha tham khảo hay để luyện dần nên mình quyết định mua combo sách này. Sách có chất lượng rất tốt, bìa cứng và được trang trí đẹp, giấy bên trong có độ dày vừa phải chứ không mỏng như các cuốn tham khảo khác mà mình từng mua, cách trình bày đẹp và rõ ràng, cứ hết mỗi đề thì lại có một trang truyện về các phát minh, kèm theo đó là những bài học về động lực và cách sống để thành công.
4
415181
2015-07-28 18:37:40
--------------------------
231872
3350
Mình xin nhận  xét về quyển Tuyệt đỉnh vật lý này 
 Về nội dung , sách có hướng dẫn khá chi tiết , tỉ mỉ cho các câu hỏi trắc nghiệm, các bộ đề khá sát với đề thi , các dạng bài tập khá đa dạng, ngoài ra sách còn có phần định hướng ,xác định mục tiêu học tập khá bổ ích. 
Về hình thức , bìa đẹp , cứng , giấy in tốt ,  chữ in rõ ràng  , đặc biệt  sách còn có hộp đựng sách rất tiên lợi.
Tóm lại mình rất hài lòng về cuốn sách này , nó sẽ hỗ trợ tốt cho kì thi THPT quốc gia sắp tới  cho mình
5
629883
2015-07-18 13:47:52
--------------------------
216446
3350
Mình rất hài lòng với bộ sách Luyện thi Tuyệt đỉnh này của MCBooks, đặc biệt là với bộ môn Vật Lí này. Các bộ đề trong đây bám rất sát với đề thi THPT quốc gia, ngoài ra kèm theo đó còn có một vài trang viết về kỹ năng sống, hướng dẫn xác định mục tiêu học tập giúp cho học sinh có định hướng rõ ràng. Bên cạnh các dạng bài tập đa dạng và bộ đề phong phú là những lời giải rất chi tiết và tỉ mỉ, hướng dẫn giải những câu trắc nghiệm một cách nhanh chóng tiết kiệm thời gian để có kết quả chính xác. Do đó, sách này giúp mình có thể hoàn toàn tự học ở nhà một cách dễ dàng và hiệu quả nên mình rất hài lòng.
5
639452
2015-06-27 19:44:33
--------------------------
206354
3350
Điểm cộng:
- Hình thức: bìa đẹp mà chất lượng giấy cũng rất tuyệt vời: giấy dày, mát tay, chữ được in rất rõ ràng, không bị mờ
- Nội dung: các đề thi có ưu điểm: bám sát với đề thi chính thức, các câu hỏi đưa ra phong phú và đa dạng, không có sự trùng lặp, phạm vi kiến thức chủ yếu là lớp 12 nhưng cũng có nhiều câu hỏi nằm trong phần chương trình học của lớp 10 và 11 nên rất hợp lí. Phần giải: mỗi bài tập đều có giải đúng và rất chính xác. Giải không chỉ đưa ra các đáp án đúng cho câu hỏi mà còn nêu ra cách làm để sao cho tiết kiệm thời gian mà lại có kết quả chính xác nhất.
Điểm trừ: theo ý kiến chủ quan của mình thì trong sách này có một vài chỗ không cần thiết là những trang có dòng kẻ ghi: "bạn thấy bài học thế nào" hay "bạn đã học được gì từ để thi này", giống với phong cách của học sinh tiểu học hơn là các anh chị đang học cấp 3 như mình.
4
466113
2015-06-09 14:47:28
--------------------------
