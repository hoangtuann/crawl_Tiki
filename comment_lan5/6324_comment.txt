333700
6324
Tuy là sách luyện nghe nhưng lại không có CD, nên chúng ta phải download trên mạng. Sách trình bày nhìn rất chán. Giấy không được tốt lắm. Chữ thì mờ, nhìn đau mắt. Nhưng nếu bỏ qua những lỗi hình thức thì quyển sách này lại rất bổ ích. Nội dung nghe theo từng chủ đề liên quan gần gũi với đời sống để chúng ta có thể luyện nghe mỗi ngày. Học xong cuốn sách này chắc rằng kỹ năng nghe của bạn sẽ tăng lên một bậc. Những người có trình độ Basic nên mua cuốn này.
2
837337
2015-11-08 10:44:54
--------------------------
158943
6324
Sách là sách được tác giả nước ngoài viết từ lâu nhưng nội dung vẫn còn rất tốt vẫn có khả năng ứng dụng cao trong các kì thi.
Sách là một bộ lọc về những nội dung có thể khi nghe chúng ta hay bị gài. Không trình bày theo dạng tests nhưng chia topic với những chủ đề riêng, dễ tiếp cận hơn so với các đề tests.
Tuy nhiên sách có trình bày bìa không bắt mắt nhìn rất chán. Nội dung in bên trong thì không có độ sắc nét cao nhìn như một cuốn sách photo không hơn không kém. Điều quan trọng hơn đây là một cuốn sách về Listening nhưng lại không tặng kèm CD tuy nhiên có thể download phần audio trên google
3
201433
2015-02-14 12:28:15
--------------------------
