230113
4687
Một tập truyện nhỏ nhắn, bao gồm các câu chuyện cổ tích đặc trưng cho văn hóa Việt Nam, đọc cho con nghe mà mình như sống lại một thời tuổi thơ. Các nhà giáo dục đều khuyên nên đọc truyện cổ tích cho trẻ nghe để cho trẻ phát triển trí tưởng tượng và hình thành nhân cách, biết phân biệt cái tốt cái xấu, những gì nên làm và những gì không nên.
NXB Kim Đồng đã thực hiện quyển sách rất công phu, mỗi truyện đều được minh họa màu rất đẹp, nét vẽ đơn sơ giản dị, toát lên được tâm hồn người Việt. Chất lượng in rất tốt, trên giấy ngà rất đẹp.
5
462641
2015-07-17 11:25:43
--------------------------
