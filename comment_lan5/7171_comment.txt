348736
7171
Tập này chúng ta lại được gặp một nhân vật mới trong số những Danh Nhân thế giới. Ông là Alexander Graham Bell, Cha đẻ của những chiếc điện thoại. 
Thời đại của chúng ta hẳn ai cũng có ít nhất 1 chiếc phone trên tay, vậy có bao giờ bạn tự đặt cho mình câu hỏi ai là người đầu tiên phát minh ra điện thoại chưa? Chắc là cũng có chứ. Cuốn sách này viết về thời niên thiếu, và sự nghiệp phát minh ra chiếc điện thoại của Alexander Graham Bell. Hình ảnh sách minh hoạ rất dễ thương, các bạn nên tìm đọc. 
5
754539
2015-12-06 18:15:00
--------------------------
269211
7171
Cảm giác đầu tiên khi mình mở cuốn sách này ra là mình rất thích cái mục lục. Từng mục nhỏ với tựa bằng cả tiếng Việt lẫn tiếng Anh, in hai mà, nổi bật và rõ ràng, dễ hiểu. Lướt qua từng mục, mình thấy tóm tắt được đầy đủ các thành tựu lướn của Graham Bell . Nội dung trong sách khá ổn, mặc dù không được chi tiết như mình mong đợi. Mỗi mục chỉ 1-2 trang và giới thiệu sơ qua thôi. Tuy thế mình nghĩ cũng cung cấp cho các em học sinh hiểu biết sơ qua về mỗi danh nhân, nếu có hứng thú các em có thể tìm hiểu sâu hơn từ các nguồn khác.
4
82774
2015-08-17 10:14:58
--------------------------
