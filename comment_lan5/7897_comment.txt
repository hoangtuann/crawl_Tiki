421584
7897
Cuốn sách rất hữu ích. Cung cấp đầy đủ thông tin về sự phát triển của trẻ qua từng giai đoạn một cách rõ ràng. Nội dung cuốn sách giúp phụ huynh hiểu tâm lý của trẻ ở thời điểm hiện tại. Từ đó giúp chúng ta xử lý các tình huống một cách hợp lý nhất. Cuốn sách thực sự rất cần thiết cho những ai lần đầu làm cha, làm mẹ như mình. Cuốn sách mình mua từ năm ngoái khi bé được hơn 1 tuổi, mình đọc đi đọc lại mấy lần mà vẫn nghiền. Đặc biệt giờ bé của mình đang ở lứa tuổi "ẩm ương" 2 tuổi thì cuốn sách như bảo bối giúp mình bình tĩnh hơn khi xử lý các tình huống với trẻ.
4
715502
2016-04-25 13:59:43
--------------------------
