554149
3077
Lúc trước tình cờ biết được sách này qua trang chủ của Nhã Nam. Những câu chuyện rời rạc, manh múng, tưởng chừng như không hề ăn nhập lại với nhau, nhưng khi đã đọc xong rồi thì mọi thứ đều được xâu chuỗi lại thành một hiện thực hoàn chỉnh và tàn nhẫn.
Đây là một quyển sách thật sự rất đáng đọc.
5
645139
2017-03-25 17:18:31
--------------------------
447122
3077
Toàn bộ những từ chỉ số lượng trong cuốn sách này đều được sử dụng bằng số 4 như: 4h, 44 ngày, 444 phút hoặc là 4 ga tàu..... Cách viết lạ, dường như các tình tiết được đan xen nhau khiến mình hơi khó liên kết lại để hiểu. Bị ấn tượng bởi cái tên của nó và vì còn vài ngày nữa là hết tháng 4 nên mình đã mua nó để đọc xem (nếu đúng như khoảng thời gian như trong truyện thì sẽ có tâm trạng thế nào?). Và mình cảm thấy có lẽ thể loại này mình không hiểu được và hiện tại mình đã ngưng đọc nó khi chưa được một nửa có lẽ vì tâm trạng không hợp nên mình sẽ để nó đọc vào dịp khác. tuy nhiên mình vẫn thích cách tác giả diễn tả nhưng diễn biến tâm trạng của nhân vật, việc sử dụng những con số 4 tự nhiên nó không khiến câu chuyện trở nên khô cứng
3
703243
2016-06-13 15:33:37
--------------------------
412623
3077
Đây là một tiểu thuyết với dung lượng không dài nhưng thực sự tác giả gieo vào suy nghĩ người đọc một chuỗi những sự quẩn quanh, mệt mỏi.  Có thể nhận ra sự cách tân về hình thức lời thoại của Thuận, rất nhiều số 4, rất nhiều câu, đoạn lặp đi lặp lại. Các chi tiết cũng vụn vặt, sự xâu chuỗi không rõ rệt.  Nhưng qua tác phẩm vẫn ám ảnh chúng ta về những điều mà tác giả kể,  trong đó rõ nhất chính là sự ám ảnh về quá trình con người đi tìm và khẳng định mình.  Tác phẩm không lấy sự trau chuốt,  không mang tính trữ tình đậm đà. Toàn bộ như phơi trải lên tất cả sự xô bồ của thế giới người 
4
1088790
2016-04-08 07:11:15
--------------------------
323518
3077
Văn phong của tác gỉa thu hút mình ngay từ trang đầu tiên của tác phẩm. Một câu chuyện tràn đấy số 4 ở mọi thời điểm khiến người đọc ấn tượng sâu sắc và bị cuốn theo mạch truyện lúc nào không hay. Lối viết lạ, cách dẫn truyện cũng thú vị và khá thông minh. Đan xen yếu tố tình dục nhưng không hề thô mà để cho người đọc sự cảm nhận mơ hồ về những điều chứa chan bên trong mỗi con người. 
Mình thực sự thích tác phẩm này và mong chờ ấn phẩm mới của Thuận. 
5
374081
2015-10-19 03:04:46
--------------------------
286468
3077
Mình chọn mua cuốn này vì lời giới thiệu quá hấp dẫn về lối viết của câu chuyện và chuyện số 4 sẽ xuất hiện liên tục, đến mức "độc giả phát ngán" như lời nhân vật có nói ở chương cuối. Sách dày chưa đến 200 trang nhưng quả khó đọc vì lối viết đan xen. Và số 4 đúng là xuất hiện nhiều đến ngán thật. Nội dung thì tuyệt vời vì mình rất thích đọc những chuyện về thời hậu 75 như vậy để hiểu thêm về thời cuộc lúc đó. Nhưng mình thực chưa hiểu hết câu chuyện nên có lẽ sẽ phải đọc lại. có lẽ là vào dịp 30.4 năm sau chăng :)
5
16313
2015-09-01 19:47:51
--------------------------
257127
3077
Tôi hiếm khi cho một tác phẩm năm sao. Không phải quá tuyệt vời, quá dữ dội nhưng vẫn xứng đáng với một truyện ngắn năm sao khi tôi đọc liên tục không rời mới nổi trong gần ba giờ đồng hồ.
Thoạt tiên tôi hơi khó chịu khi quá nhiều số 4, lại có phần hơi khó hiểu giữa các nhân vật, Ân, nàng, cô hắn,… nhưng chỉ qua 2 chương đầu, các khó hiểu được dần dần tháo gỡ và quen hơn với những số 4. Những cảnh tình dục được đề cập đến dường như càng lên cao trào khi những nỗi đau càng về cuối càng được tiết lộ. Để đến cuối cùng, khi nỗi đau được bộc lộ rõ ràng và sắc nét nhất, các chi tiết thắt nút được mở ra thì cũng là lần đầu tiên cặp tình nhân đó "nói chuyện" với nhau bằng lời nói, chứ không phải bằng xác thịt. 
Nỗi đau một thời của những nhân vật mà họ là nạn nhân cứ thế mà đau âm ỉ, theo mãi và làm người đọc phải phân vân, suy nghĩ không thôi. 
5
119223
2015-08-07 14:10:14
--------------------------
240893
3077
Tình cờ mua quyển sách này qua sự giới thiệu của nhỏ bạn. Thực sự thì lúc đầu đọc mà ù ù cạc cạc cả tai lẫn mắt, khó chiu lắm, cũng đã nhiều lần gấp sách lại và quẳng nó đi. Cơ mà không dứt được. Cái hay nằm ở chỗ đó. Lạ ở chỗ, đọc lướt nhanh thì có vẻ chẳng liên quan, chẳng mang ý nghĩa gì nhưng trái ngược hoàn toàn. cuốn sách tuy không dày nhưng lại mang nhiều ý nghĩa qua hai mặt nóng hổi của tập thể: chính trị và tình dục. Đương nhiên nội dung cuốn sách như vậy thì không thể nào diễn tả trực tiếp được, chỉ có thể nấp bóng sau những ngôn từ của nhà văn. Và Thuận đã thành công.
4
120193
2015-07-24 23:50:40
--------------------------
220288
3077
Bạn sẽ bị ấn tượng ngay bởi cách trình bày mạch truyện, khá khó hiểu và kén chọn độc giả nhưng một khi đã bị dẫn dắt theo tình tiết thì khó mà thoát khỏi nó. Với độ dài quyển sách không nhiều nhưng đủ để chiêm nghiệm về những nỗi đau, bất hạnh và cả mảng đời người trôi nổi. Thuận là một tác giả không đi theo xu hướng viết cho người ta hiểu ngay, quên ngay mà đây là cây bút khiến người đọc dõi theo từng câu chữ để rồi day dứt không yên. 
Hãy đọc quyển sách này thật lâu và thật chậm, bạn sẽ có thể cảm nhận một khía cạnh khác của cuộc sống với ngôn ngữ bình dị và cái nhìn chân thật từ phía tác giả.
4
364576
2015-07-02 14:18:53
--------------------------
219252
3077
Cá nhân mình thích chủ đề của cuốn sách này, tác giả đã rất khéo léo kết hợp hai chủ đề không hề liên quan đến nhau là chính trị và nhục dục bằng những câu chuyện giản đơn hằng ngày.

Cách viết của tác giả khá lạ, văn phong viết và cách dùng từ khác với đa số các sách hiện tại trên thị trường. Mới đầu đọc chắc chắn sẽ có cảm giác bị 'khớp' vì lối viết lạ quá, cảm giác hơi khó chịu một chút, nhưng nếu bạn chịu khó đọc tiếp, dành thời gian ngẫm nghĩ thì sẽ thấy cuốn sách chứa đựng nhiều bài học hay.

Một cuốn sách đáng đọc, nhưng trước khi mua các bạn hãy đọc thử xem có hợp văn phong không đã nhé.
4
661329
2015-07-01 11:57:47
--------------------------
216048
3077
Thuận là một trong những tác giả nổi tiếng của văn học Việt Nam đương đại với những tác phẩm khó đọc như Thang máy Sài Gòn, Chinatown. Có thể thấy, những tác phẩm của bà được độc giả đón nhận khá nồng nhiệt. 
Riêng ở quyển sách này, một quyển sách tiếp tục gây ấn tượng với bạn đọc. Mốt lối viết với hình thức mới lạ. Một sự kết hợp, hòa quyện giữa những mặt ngỡ như tách rời nhau, không thể ào kết hợp được, là chính trị và nhục dục của con người. Quả thực, câu chuyện về đôi vợ chồng Việt kiều này có quá nhiều thứ để chia sẻ, để nói đến.
Bạn hãy thử đọc, khám phá và chia sẽ nhé ^^
4
616731
2015-06-27 08:40:53
--------------------------
