554857
4218
Sách nói về những hoàn cảnh của nhiêù người nổi tiếng và rút cho ta những điều lên làm hằng ngày. Nói chung tạm được. Nhân viên giao hàng lịch sự
3
2240059
2017-03-26 13:13:36
--------------------------
408221
4218
Trong cuộc đời mỗi người chắc đôi lần ai cũng sẽ gặp những vấp ngã, thất bại. Và qua mỗi lần như vậy chúng ta đều có cách nhìn nhận riêng và cách thức riêng để vượt qua vấp ngã, thất bại đó. Tuy nhiên, vẫn còn đó những người thiếu cái nhìn lạc quan và hoài nghi về bản thân. Nếu bạn vẫn còn thấy chán nản về thất bại, hãy đọc cuốn sách này để có thêm ý chí và nghi lực vượt qua tất cả.
Mỗi câu chuyện, những chia sẻ của tác giả đều là những trải nghiệm thực tế được đúc kết từ kinh nghiệm của bản thân và đây sẽ là những bài học quý báu cho mỗi người trong kinh doanh cũng như trong cuộc sống.
Vấp ngã không phải là thất bại,mà chỉ là một bước giúp ta tới gần với thành công hơn.
Hãy đọc và cảm nhận bạn nhé!
4
614561
2016-03-31 12:43:02
--------------------------
386049
4218
Thật tình cờ khi tôi lên Ti ki tìm mua truyện cho cô bạn bạn gái thân ở nhà bên bên cạnh  đã nhìn thấy và bị thu hút ngay ánh mắt vào cuốn sách này. Tôi đã đặt mua và nhận được ngay sau đó mấy ngày. Ti ki giao hàng khá nhanh và đúng như đã hẹn, tôi bắt tay ngay vào việc đọc. Quyển sách là một cẩm nang hi vọng và chứa đựng niềm tin cho mỗi con người chúng ta. Nếu chưa tìm được cho mình cuốn sách hay thì tôi khuyên bạn nên chọn "Học từ vấp ngã để thành công"
4
922909
2016-02-24 22:56:39
--------------------------
266107
4218
Khi bạn đang đối mặt với nỗi sợ thất bại, khi bạn không dám đứng lên đi tiếp sau những vấp ngã vì nỗi sợ quá lớn, tôi khuyên bạn nên đọc quyển sách này. Thất bại và vấp ngã trong cuộc sống có thể nói vừa là thử thách để chúng ta có thêm bản lĩnh và vừa là bài học để chúng ta hoàn thiện bản thân. Không có khó khăn, thành công vô nghĩa. Quyển sách gửi đến người đọc một thông điệp ý nghĩa: "Hãy dám đối mặt với thất bại và đứng lên sau những vấp ngã, bạn chắc chắn sẽ thành công"
5
138751
2015-08-14 13:35:57
--------------------------
249536
4218
Trước đây tôi rất sợ thất bại, sợ cái cảm giác thua cuộc,nó làm tôi chán nãn cảm giác như rớt xuống vực vậy, nhưng nhờ cuốn sách này tôi đã có thêm niềm tin để dám thất bại, nó dạy tôi một triết lí sống đúng đắn để dần tiến tới thành công,, nó còn cho tôi cả động lực để theo đuổi ước mơ của mình dù có gặp phải khó khăn hay thất bại, qua đó tôi luôn tâm niệm rằng : " Tôi phải đứng dậy từ nơi tôi vấp ngả" và " người khác đứng dậy sau vấp ngã và họ thành công, học làm được thì tôi cũng làm được"
5
668162
2015-07-31 22:39:21
--------------------------
247236
4218
"Sự khác biệt giữa vĩ nhân và kẻ tầm thường là cách mỗi người nhìm nhận sai lầm". Các bạn có thấy câu nói này tuyệt vời không ạ? Bản thân tôi và tất cả các anh chị ngồi ở đây chắc hẳn ai cũng sợ thất bại phải ko ạ? Tại sao chúng ta lại sợ? Bởi vì nó là điều rất kinh khủng nhưng quan trọng hơn hết là chúng ta yếu đuối, không đủ can đảm và nghị lực để đối mặt và vượt qua khi nó đến. Nếu không gặp khó khăn thử thách thì mỗi người trong chúng ta sẽ ko phát triển được nghị lực, trí tuệ cũng như sự trưởng thành và em rất thích 1 câu nói của một cuốn sách tôi đã từng đọc có viết là: “Nếu con đường dẫn đến sự thành công mà được trải toàn hoa hồng thì thành công ấy chẳng có ý nghĩa gì cả". Vậy để biết chúng ta có thể học được gì từ thất bại thì các bạn hãy sở hữu ngay cuốn sách để có thể biết được bí quyết từ John C.Maxwell nhé :D.
5
690615
2015-07-30 10:02:19
--------------------------
231025
4218
Nếu tự mình cố gắng làm việc phạm sai lầm và sửa sai thì sẽ rất khó khăn và tôn nhiều thời gian để có thể có được thành công. Chính vì vậy, để nhanh chóng học hỏi thêm được kiến thức và kinh nghiệm thì ai cũng cần phải có những người cố vấn. và "ai che lưng cho bạn" đã giúp độc giả biết cách duy trì mối quan hệ với những người cố vấn, tạo được sự thân thiện khi nói chuyện với mọi người để từ đó mọi người có thể chia sẻ kinh nghiệm, góp ý cho mình để mình có những quyết định đúng đắn hơn.
5
274812
2015-07-17 21:05:31
--------------------------
195622
4218
Thất bại là điều mà bất cứ ai cũng sợ, cũng lảng tránh và không dám đối mặt. Nhưng chúng ta không hề biết cái gì cũng có mặt phải của nó. Trong quyển sách Học từ vấp ngã để từng bước thành công, John Maxwell đã cho chúng ta biết thất bại chẳng qua chỉ là một thử thách để kiểm chứng sức mạnh của chúng ta. Thất bại luôn nhiều hơn thành công và chỉ có những ai dám đương đầu mới hi vọng có thể vượt qua được thất bại và giành lấy vinh quang trong mọi mặt của cuộc sống. Quyển sách là một cẩm nang chứa đựng hi vọng và mang tới niềm tin cho mỗi chúng ta.
5
387632
2015-05-14 09:47:59
--------------------------
191690
4218
Nhiều người đứng dậy sau vấp ngã, họ thành công. Nhiều người không thể đứng dậy, họ trượt dài trong sầu khổ, tự hạ thấp bản thân mình và dần quay lưng với cuộc sống. Thực sự cuốn sách này rất ý nghĩa, nó dạy tôi một triết lí sống đúng đắn : Phải dũng cảm đối mặt, đứng dậy từ chính nơi mình đã ngã xuống thì mới có thể thành công. Triết lí đơn giản ấy không phải ai cũng có thể làm được bởi cuộc sống khó khăn, đôi khi chẳng như ta mong muốn được. Cảm ơn NXB Dân trí đã xuất bản một cuốn sách hay và ý nghĩa như vậy, rất đáng đọc.
5
606127
2015-05-02 13:54:15
--------------------------
