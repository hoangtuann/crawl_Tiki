453553
5539
  Nội dung thì tôi thấy cho 10 sao cũng được chứ đừng nói là 5 sao nhưng tôi cảm thấy quá ức chế bởi bìa của cuốn sách: một tác phẩm nổi tiếng đã làm nên tên tuổi của Nam Cao, hai nhân vật mà cái tên đã trở thành tên gọi cho một loại người mà họa sĩ lại có thể vẽ châm biếm như vậy. Về nội dung thì các bạn chú yên tâm khi mà tác phẩm đã được in vào trong sách giáo khoa THPT, truyện có những câu thoại đã trở thành kinh điển, lên án tố cáo thời kì đen tối của đất nước đã đẩy những người nông dân đến sự tha hóa, buộc phải thay đổi bản chất của mình. Hai nhân vật chính Thị Nở cùng Chí Phèo cùng kết chuyện đã gây nỗi ám ảnh sâu sắc trong lòng người đọc.
4
857649
2016-06-21 12:12:13
--------------------------
438811
5539
" Chí Phèo " của nhà văn Nam Cao là một câu chuyện vô cùng hay và ý nghĩa . Cuốn truyện này gồm có nhiều truyện của tác giả Nam Cao và trong đó có  chuyện Chí Phèo. Mình rất thích câu chuyện đó. Truyện đã tái hiện lại một cách sâu sắc và chân thực một thời gian khổ đã qua của đất nước ta, một thời xã hội bế tắc, xấu xa đã đẩy con người ta đến bước đường cùng như Chí Phèo thật là đáng thương. Truyện đã giúp chúng ta biết trân trọng những giây phút thanh bình, ấm no này.
5
1351512
2016-05-30 13:04:26
--------------------------
435465
5539
Chí Phèo gần như là tác phẩm đầu tiên và hay nhất của Nam Cao .Mặc dù Nam Cao viết khá nhiều truyện ngắn nhưng ít truyện ngắn nào vượt qua được đỉnh cao của Chí Phèo .Nếu có thì chỉ có Lão Hạc .Hai câu chuyện khác nhau nhưng cùng ghi lại dấu ấn trong lòng độc giả bởi cái thiện ở cuối tác phẩm.Chí Phèo chàng thanh niên giàu ước sau khi ra tù bị tha hóa thành kẻ bợm rượu ,y chỉ thức tỉnh khi được sưởi ấm bằng tình yêu của Thị Nở ,cái thiện trong y thức tỉnh và y vùng giết lý trưởng.Khác với cái thiện chuyển hóa trong Chí Phèo ,cái thiện trong Lão Hạc xuyên suốt từ đầu tới cuối chứng minh cho sự khát khao hướng thiện của những người nghèo khổ bấy giờ .
5
1136040
2016-05-25 09:18:33
--------------------------
432768
5539
Chí Phèo là một cuốn sách khá đặc biệt của tôi.Nó mang lại cho tôi rất nhiều giá trị về văn hóa, về xã hội và trong đó chứa đựng rất nhiều bài học sâu xa và ý nghĩa.Từ ngay truyện ngắn đầu tiên (Chí Phèo) đã thể hiện được ngòi bút tinh tế của Nam Cao.Rồi đến " Lão Hạc ", " Dì Hảo ",...Tất cả đều phản ánh tình trạng xã hội thực tại những năm tháng chiến tranh và những cái bất công của người nông dân không thể dãi bày cùng ai.Truyện của Nam Cao có cái gì đó đặc biệt lắm, vừa nhẹ nhàng, vừa sâu sắc, lời văn thì sắc sảo nhưng lại đượm chút gì đó đắng cay, buồn cho những số phận, buồn cho những con người vô tội.Không hổ danh!
5
646700
2016-05-19 19:17:44
--------------------------
422886
5539
Mẫu sách trình bày đẹp. Về phần nội dung thì có phần giới thiệu sơ lượt về tác giả, điểm sơ qua những tác phẩm để đời của Nam Cao- nhà văn nổi tiếng với lối hành văn hiện đại, qua đó bộc lộ hết được những hình ảnh đời thường của người dân trong xã hội cũ. Quyển này không chỉ viết về tác phẩm Chí Phèo không mà còn viết những câu truyện ngắn để đời của ông. Cho mấy đứa bạn mượn đọc tụi nó thấy hay quá nên mỗi đứa mua về đọc kaka --> giống như tui là người giới thiệu sản phẩm á :v Thiệt tình thì 31.5k thì quả là không mắc mà là quá rẻ ấy :*
4
717457
2016-04-28 11:55:34
--------------------------
422286
5539
Tên đề là "Chí Phèo" nhưng bên trong còn có 17 truyện ngắn khác của tác giả Nam Cao. Mình có nghe nhắc đến cái tên này nhiều lần trong tiết ngữ văn nhưng không để ý lắm. Đến khi đọc các truyện ngắn của Nam Cao thì thấy thật sự rất hay, văn phong không lẫn vào đâu được. Truyện ngắn của Nam Cao xoay quanh những câu chuyện ở làng Vũ Đại. Xã hội phong kiến khắc nghiệt khiến cả những chuyện không thể trở nên có thể, từ một chàng trai hiền lành thành 1 kẻ du côn.
5
755854
2016-04-26 22:17:16
--------------------------
400917
5539
"Chí Phèo " của nhà văn Nam Cao quả là một tập truyện rất hay. Trong quyển này gồm nhiều những câu chuyện nổi bật, đặc sắc của nhà văn Nam Cao. Những câu chuyện này đã tái hiện cả một thời kì đầy gian khổ của đất nước ta, truyện có ý nghĩa nhân văn vô cùng sâu sắc. Gập trang sách lại mà hình ảnh những nhân vật như còn ám ảnh mãi trong lòng người đọc. Quyển truyện được thiết kế nhỏ gọn bằng giấy siêu nhẹ rất dễ đọc. Mình thực sự rất thích quyển truyện này.
5
1174527
2016-03-19 23:16:27
--------------------------
391582
5539
Hối tiếc cho cuộc đời của Chí Phèo bởi vì Chí có muốn cuộc đời của mình như thế đâu. Là một thanh niên trai tráng, Chí vẫn luôn ao ước có một cuộc đời bình thường như bao người, nhưng chính bởi cái xã hội thối nát đương thời đã xô đẩy Chí xa ngã. Chí biến thành một con quỷ của làng Vũ Đại, và định mệnh đã khiến cho Chí gặp được Thị Nở - người đàn bà khiến Chí muốn một lần nữa làm người, muốn một lần nữa thấp hy vọng cho cuộc đời mình. Nhưng tất cả phút chốc tan thành mây khói bởi lời từ chối của Nở, thế là Chí đã trả thù, trả thù những người tạo nên Chí Phèo ngày hôm nay.
4
105743
2016-03-05 16:44:17
--------------------------
374811
5539
Văn phong của Nam Cao rất lạ nhưng cũng rất quen
Đủ để tạo nên sự khác biệt giữa ông với những nhà văn khác
Mộc mạc,giản dị,gần gũi,dân giã
Có lẽ cũng chịu nhiều tác động của yếu tố hoàn cảnh,văn của ông con người luôn đói luôn khổ
Ấn tượng nhất là truyện 'Trẻ em không được ăn thịt chó'
Đến khổ làm sao
Đọc văn của Nam cao ta như tìm về được cái bình dị của con người,của làng quê,của sự chân chất
Và như thấy rõ một phần nào của bộ mặt xã hội lúc bấy giờ!
5
515302
2016-01-27 11:00:08
--------------------------
364566
5539
  Tôi luôn ấn tượng các tác phẩm của Nam Cao, ấn tượng bởi cách xây dựng nhân vật của ông. Các tác phẩm khác cũng có người nông dân cùng khổ, có trí thức , có cường hào ác bá, nhưng riêng " Chí Phèo" ông lại xây dựng những hình mẫu nhân vật hoàn toàn khác: Một Thị Nở vừa xấu vừa dở hơi sống với bà con nhưng không phải không có tình người, một Chí Phèo hung hăng, nát rượu nhưng chẳng phải bản chất hắn thế, là đời buộc hắn thế, rồi hình ảnh cái lò gạch xuyên suốt toàn tác phẩm gây ra cho bạn đọc cảm giác cái vòng tròn cùng cực, cái số phận cay đắng đã xiết chặt Chí Phèo rồi cũng sẽ xiết chặt những cảnh đời bất hạnh khác. Đọc truyện sợ Chí Phèo một, thì thương hắn hai, xót cho số phận con người cùng cực trong xã hội cũ, đau đớn cho cảnh bế tắc của thân phận con người. Tất cả đều lột tả qua giọng văn chân thực mà không kém phần cảm thương của Nam Cao. Quả thực, " Chí Phèo" là một tác phẩm vô cùng ấn tượng luôn để lại dấu ấn sâu đậm trong mỗi người đọc.
5
893157
2016-01-06 13:34:21
--------------------------
344979
5539
Truyện phản ánh rõ xã hội xưa . Phải cộng nhận ngôn ngữ cũng như cách viết của Nam Cao rất bình dị và gần gũi phù hợp với tất cả mọi người . Cái thứ hai câu truyện có nội dung rất hay nhất là chi tiết cuối cùng . Mình đọc đoạn đầu và đoạn cuối phải công nhận Nam Cao xây dựng cốt truyện rất hay . Nhưng cái bìa sách mình thấy không phù hợp với nội dung truyện bởi cái kết và nội dung truyện đều rất buồn . Nhìn cái bìa mình thấy khắc hoạ rõ nét được hai nhân vật nhưng nhìn có vẻ rất giống tranh biếm hoạ
4
898367
2015-11-29 15:19:38
--------------------------
342348
5539
          Chí Phèo là cuốn sách hay, nhân văn và sâu sắc!! Nhắc đến Chí phèo chắc hẳn ai cũng biết nhưng ít người đã đọc và trải nghiệm tập truyện ý nghĩa như thế này. Đây là sách gồm các truyện ngắn vào thời phong kiến chiến tranh của Nam Cao.  Từng câu chuyện khắc họa lên hình ảnh nhân vật nổi bật, đặc biệt là truyện ngắn Chí Phèo đã xây dựng nên một hình tượng nhân vật mà ai cũng biết.                                                      
           Có một chút không hài lòng lắm. Thứ nhất là giấy mỏng và trắng hơi chói mắt nên cũng hơi khó đọc chút. Mình đã mua cuốn này ở nhà sách và thấy Tiki bán với giá mềm hơn nên cũng hơi tiếc. Từ ngày biết Tiki mình chả bao giờ đi nhà sách để mua sach cả. :))).                                                                                                                                                                                                                                                                                                
4
830966
2015-11-24 11:00:13
--------------------------
284207
5539
Mình đã mua cuốn sách này. Đây là tuyển tập các truyện ngắn của nhà văn Nam Cao. Mình thích nhất là truyện ngắn Lão Hạc. Qua tuyển tập mình tìm hiểu được nhiều hơn về tác giả Nam Cao về lối hành văn của ông. Mình khuyên các bạn nên mua cuốn sách này. Nó thật sự rất bổ ích. Nếu các bạn định thi chuyên văn vào lớp 10 như mình thì nên mua cuốn sách này Nó sẽ giúp ích các bạn rất nhiều!!! Khổ sách không quá to giúp bạn dễ dàng trong việc đọc và tìm hiểu nó.
5
643226
2015-08-30 19:07:03
--------------------------
253388
5539
Không chỉ có Chí Phèo mà quyển sách này còn bao gồm rất nhiều tác phẩm tiêu biểu của Nam Cao. Như "hình ảnh" trong cuốn sách này như một thước phim quay chậm lấy bối cảnh hiện thực trước Cách Mạng tháng Tám của người dân lao động lâm vào cảnh bần cùng hóa: Một Chí Phèo bị khước từ quyền làm người (Chí Phèo), Một Lão Hạc oàn mình trong cái chết đau đớn và dữ dội (Lão Hạc), hay ở ngay chính trong câu nói của Hộ:" Một tác phẩm thật giá trị, phải vượt lên trên tất cả các bờ cõi và giới hạn, phải là một tác phẩm chung cho cả loài người" (Đời thừa), thậm chí có trong Giăng sáng: "Nghệ thuật không là ánh trăng lừa dối, không nên là ánh trăng lừa dối! Nghệ thuật có thể chỉ là tiếng đau khổ kia, thoát ra từ những kiếp người lầm than". Tất cả đều cho ta thấy một Nam Cao hiện thân của một nhà văn hiện thực và nhân đạo chân chính.
5
368588
2015-08-04 13:38:55
--------------------------
253285
5539
Chí phèo thuộc loại tác phẩm kinh điển của Việt Nam sau năm 1945. Vì mình chuyên Văn nên muốn tìm hiểu nhiều hơn chỉ hai ba tác phẩm của nhà văn hiện thực Nam Cao. Mình quyết định chọn mua cuốn sách này. Tới khi cầm nó trên tay mình cảm thấy quyết định mua sách ban đầu quả rất đúng đắn. Sách nhẹ lắm, nhưng giấy lại rất dày màu vàng đúng màu chuẩn làm cho mắt không mỏi, không sợ hư mắt. Và với nó, tôi đã bổ sung được một lượng kiến thức về Nam Cao!
5
366662
2015-08-04 12:19:00
--------------------------
230276
5539
Tác phẩm Chí Phèo của Nam Cao là mọt tác phẩm đặc sắc và có giá trị bền bỉ, cho dù có được đưa vào chương trình dạy học cấp ba hay không thì vẫn sẽ có đông đảo các bạn trẻ đón đọc. Vì tác phẩm đã đề cao giá trị con người, sống không phải chỉ là sống mà phải sống cho đúng nghĩa, sống để yêu thương và san sẻ, sống để làm người lương thiện, Mặc dù không có cái kết có hậu như những tác phẩm sau năm 1945 nhưng Chí Phèo đã để lại cho chúng ta nhiều bài học quý báu, nhiều giá trị cần nắm giữ và phát huy.
4
691304
2015-07-17 13:10:29
--------------------------
207246
5539
Chí Phèo đã cho mình nhiều cảm xúc từ đầu đến cuối truyện. Lúc đầu là thấy gét cài thằng Chí phèo "dở hơi" suốt ngày rạch mặt ăn vạ, với cái thân hình và gương mặt gớm giếc và tính cách mà ai cũng sợ.. nhưng sau đó lại cảm thấy nhân vât này thật đáng thương: khi hắn cảm thấy được tình yêu từ Thị Nở và ước mơ về cuộc sống thanh bình mai sau, lúc mà hắn muốn bỏ cuộc sống rạch mặt ăn vạ của mình thì hắn cũng bị định kiến xã hội làm cho mất đi hi vọng, cuối cùng phải chọn cái chết. Câu chuyện chính là ước mơ của con người về một tương lai tốt đẹp và cũng là phê phán xã hội phong kiến đã phá  hoại ước mơ, tương lai của con người.
5
530921
2015-06-11 20:53:08
--------------------------
193689
5539
Khi đọc tác phẩm “Chí Phèo” tôi cảm thấy trong mình có rất nhiều cảm xúc đối với nhân vật Chí Phèo. Đầu tiên là đồng cảm với con người hiền lành này khi mà cuộc đời mới bước vào tuổi xuân xanh thì lại bị tống vào tù khi mà hắn ta chẳng làm gì nên tội cả. Sau đó, tôi lại cảm thấy không thích nhân vật này khi mà hắn đi tới đâu là ăn vạ tới đó. nhưng sau cùng tôi lại cảm thấy cảm thông với con người cùng khổ này, hắn ta yêu Thị Nở, một cô gái không ai thèm yêu, còn hắn, muốn cũng không có để rồi kết thúc của con người đau khổ ấy là cái chết. Tác giả đã vẽ nên cho chúng ta một câu chuyện riêng của làng Vũ Đại nhưng cũng là câu chuyện chung của xã hội thời ấy. Bìa sách với hình ảnh ngộ nghĩnh và rất lạ khiến cho chúng ta cảm thấy tức cười khi nhìn vào bìa sách nhưng lại cảm thấy buồn thương khi nhìn lại những con người ở trang bìa ấy. Những con người mà suốt cuộc đời phải sống trong bóng tối, sống dưới quyền cai trị của người khác.
3
13723
2015-05-08 14:01:15
--------------------------
191135
5539
Hồi cấp 2 mình được học Tắt đèn và Lão Hạc, lên đến cấp 3 thì được học Chí Phèo, từ đó mình rất thích đọc các tác phẩm nói về cuộc sống bần cùng của người nông dân trước Cách mạng tháng 8, đặc biệt là các tác phẩm của nhà văn Nam Cao. Mình có cảm giác Chí Phèo được viết bằng một giọng văn lạnh lùng nhưng ẩn sau đó lại là trái tim thương xót của Nam Cao. Nhà văn chưa thấy được lối thoát cho Chí Phèo như Kim Lân, nên cuộc sống của người nông dân vẫn cứ là một vòng luẩn quẩn. Đọc xong Chí Phèo, ắt hẳn ai cũng phải bị ám ảnh
Về bìa sách, mình thực sự không thích cho lắm vì nhìn khá giống một cuốn truyện tranh biếm họa, không thu hút.
5
606127
2015-05-01 08:23:21
--------------------------
187256
5539
Mình đã được học tác phẩm "Chí Phèo" năm lớp 11, nay tìm mua lại quyển sách tuyển tập truyện ngắn của nhà văn Nam Cao để tìm hiểu nhiều hơn. Quả đúng vậy, ông là một trong những ngòi bút hiện thực và nhân văn sâu sắc nhất thời bấy giờ. Không chỉ riêng Chí Phèo, các truyện ngắn của Nam Cao luôn giống như vết cắt cứa vào tâm can người đọc, luôn khiến người ta phải căm phẫn, xót xa cho 1 xã hội quá nhiều khắc nghiệt ấy. Để rồi lại trào dâng niềm cảm thương tột cùng cho những con người trong xã hội cũ, như anh Chí, họ không có quyền được sống, họ sinh ra là người nhưng không được đối xử như con người, bởi vì sự bất công luôn tồn tại. Đọc tác phẩm để suy ngẫm và thông cảm nhiều hơn.
4
131327
2015-04-22 19:22:24
--------------------------
185539
5539
Tác phẩm văn học Chí Phèo được đưa vào giàng dạy ở chương tình phổ thông và đây là tác phẩm được nhắc đến rất nhiều. Tác phẩm xoay qua nhân vật Chí Phèo bị mọi người trong làng xa lánh, không ai muốn quan hệ với nhân vật này làm cho nhân vật này luôn cảm thấy bản thân mình rất khó hòa nhập với cuộc sống. Nội dung truyện đề cao tính nhân văn đó là hình ảnh bát cháo hành giữa nhân vật Chí Phèo và Thị Nở cho thấy tình người. Tuy nhiên, theo quan điểm của mình thì nhân vật Chí Phèo khi nói lên câu "AI cho tôi lương thiện" thì theo mình Chí Phèo lúc rơi vào tình cảnh như vậy, bản thân mình có thể thay đổi bằng cách đầu quân tham gia kháng chiến bảo vệ đất nước thì chắc chắn nhân vật này sẽ thay đổi cách nhìn của người khác về mình. Tuy nhiên, đây cũng là một tác phẩm hay.
3
588073
2015-04-19 15:52:30
--------------------------
181240
5539
Tôi gặp Lão Hạc trong những ngày tháng cấp 2 và đến Chí Phèo trong sách giáo khoa cấp 3, từ đó đã cố tìm đọc thêm các tác phẩm khác của nhà văn Nam Cao. 
Nếu Lão Hạc bán con Vàng nhưng vẫn giữ được tâm hồn của người cha, Hộ bán đi những bài văn không còn nhiều giá trị nhưng vẫn có Từ là gia đình, thì Chí Phèo - bán đi linh hồn và thể xác cho quỷ dữ hắn còn có gì? Bát cháo hành của Thị Nở và buổi sáng bình thường của những người xung quanh đã thức tỉnh sức khao khát được là mình, mong muốn được yêu thương, được sống yên bình trong hắn, nhưng... đã muộn rồi. Kết truyện ám ảnh và đầy chân thực của Nam Cao đã tố lên tội ác cái xã hội phong kiến bất công và bạo ngược ấy, giải phóng cho Chí nhưng đồng thời cũng mở ra một cái vòng luẩn quẩn... Cái lò gạch cũ, một thế hệ Chí con ra đời.
5
382812
2015-04-11 13:22:19
--------------------------
178923
5539
Lần đầu tiên tôi đọc " Chí Phèo" là do cốt truyện độc đáo. rất cuốn hút, lần thứ hai tôi đọc là khi học lớp 11 học tác gia Nam Cao và Chí Phèo... Lần thứ 3, thứ 4, thứ 5, thứ 6 tôi đọc lại nó chỉ đơn thuần là muốn đọc lại. Bây giờ tôi đọc " Chí Phèo" còn để tìm những câu hay, thâm thúy của Nam Cao. Chí Phèo là một trong rất nhiều truyện của Nam Cao viết về người nông dân nghèo trước cách mạng tháng tám.Những người nông dân đó bị biến chất về nhân cách và đạo đức như Chí Phèo. Dưới đôi tay tài hoa của tác giả xã hội nửa thực dân nửa phong kiến hiện ra sinh động, chân thực vô cùng, Bìa sách in đẹp và rất dễ mang theo..
5
186455
2015-04-06 06:08:50
--------------------------
168973
5539
Chí Phèo là 1 trong những câu chuyện mà khi đọc xong rồi bạn vẫn không tài nào rứt ra được.Không phải vì nó hấp dẫn với các nhân vật và tình tiết như ngôn tình hay hồi hộp như truyện trinh thám,kinh dị.Chí Phèo hấp dẫn vì những giá trị thực tế của nó.Truyện ngắn đã khắc họa số phận khốn cùng,bi thảm của người nông dân nghèo trong xã hội cũ và thể hiện niềm cảm thương,trân trọng của Nam Cao đối với họ,đồng thời tố cáo tội ác của xã hội thuộc địa phong kiến ở Việt nam trước cách mạng.Không trau chuốt,không tinh tế lãng mạn.Nhưng truyện khiến người đọc phải suy nghỉ về bi kịch bị ép làm người xấu,rồi lại vùng vẫy mà không thể nào thoát ra khỏi.
4
383783
2015-03-17 13:46:59
--------------------------
155613
5539
Chí Phèo là một tác phẩm hay phản ánh rõ nét nhất về con người trong xã hội phong kiến.Chí là một con người hiền lành lương thiện,nhưng đã bị xã hội phong kiến tha hóa về đạo đức và con người và trở thành kẻ chuyên đi đâm thuê chém mướn.Nhưng từ khi gặp được Thị cuộc đời của hắn dường như đã thay đổi,hắn thấy sợ rượu chưa bao giờ hắn sợ như lúc này,hắn cảm thấy vui khi được thị cho bát cháo vì hắn chưa bao giờ được người ta cho cái gì mà hắn toàn đi ăn chộm.hắn thay đổi từ lúc đó.Phải chăng tình người đã làm người ta thức tỉnh.Tác giả cho người ta thấy cái tình đã đánh thức mộ người như Chí và làm hắn thay đổi.
3
536745
2015-02-01 15:12:21
--------------------------
148205
5539
Về nội dung tác phẩm thì tớ không cần phải bàn nhiều nữa bởi vì chúng ta đều được học tác phẩn Chí Phèo ở trường phổ thông, đó thực sự là một Kiệt Tác của một Kiệt Tác được vun đắp, nuôi nấng bởi mảnh đất Hà Nam- tác gia Nam Cao.
Trong chương trình phổ thông chỉ giới thiệu đoạn trích cho chúng ta, để phân tích cặn kẽ tác phẩm thì ta nên tìm đọc toàn bộ tác phẩm để thấy được cái tài và cái tình mà Nam Cao đã thể hiện trong truyện ngắn. Việc xuất bản ra cuốn sách tác phẩm kinh điển như thế này là việc cần thiết, tuy nhiên cần có tính đột phá trong việc chọn lựa cách phổ biến tác phẩm bằng sách. 
Không khó để tìm đọc toàn bộ các tác phẩm truyện ngắn trên internet nên độc giả không cần tốn tiền để mua những cuốn sách như thế này (trừ một số người thích sưu tầm sách, ví dụ như tớ :D). Có lẽ cần hơn những cuốn sách độc đáo hơn hoặc như Số Đỏ, đôi khi việc dịch tác phẩm kinh điển sang ngôn ngữ khác, nhất là tiếng Anh thì vừa quảng bá được văn học nước nhà, vừa giúp học sinh trau dồi thêm cả về ngữ văn lẫn Anh văn. Tất nhiên việc chuyển một tác phẩm kinh điển sang ngôn ngữ khác mà không làm sai lệch đi về nội dung và nghệ thuật là chuyện không phải nói là làm được ngay mà cần sự nỗ lực hợp tác của cá nhân và tập thể.
Hi vọng trong thời gian tới sẽ có những ý tưởng độc đáo của ngành xuất bản Việt Nam trong vấn đề này :)
3
36378
2015-01-10 01:38:31
--------------------------
140126
5539
Nam Cao là một nhà văn hiện thực của các nhà văn hiện thực. Đây là một truyện ngắn đã làm nên tên tuổi một nhà văn hiện thực xuất sắc như ông. Tác phẩm đã lột trần bản chất một xã hội cũ bóc lột dồn đẩy những người nông dân lương thiện vào con đường bần cùng hóa không lối thoát. Văn học là đây, luôn bắt nguồn từ hiện thực cuộc sống nhưng vẫn cho chúng ta thấy giá trị nhân văn sâu sắc với tình người ấm áp. Cũng vì thế, những nhân vật như Chí phèo, Thị Nở, Bá Kiến như bước ra cuộc sông và sống mãi trong lòng mọi thế hệ độc giả
5
97460
2014-12-10 08:48:52
--------------------------
