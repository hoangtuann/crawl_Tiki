470496
3818
Mua quyển sách này cũng lâu rồi bây giờ mình mới có dịp để nhận xét. Đợt mua quyển sách này là đặt giùm cho người bạn, mình cũng không quan tâm quyển sách lắm, tại mình lười đọc. Nhưng khi mình mượn quyển sách đọc thử vài trang thì mình bị câu chuyện của nó cuốn hút vào. Hết tình tiết này đến tình tiết khác, nội dưng câu chuyện làm mình say mê và cứ theo dõi tới. Quyển sách khá dày, giấy cũng không tốt lắm, chữ in đều màu rõ ràng dễ đọc, bìa sách đẹp. Tốt
5
570226
2016-07-07 16:16:53
--------------------------
435976
3818
Trong số các tuyển tập truyện Sherlock Holmes thì có lẽ bản này là bản ổn nhất. Mình mua tập 1 tại nhà sách Phương Nam và order tập 2, 3 từ Tiki. Chất liệu giấy khá tốt, nhẹ, màu giấy tự nhiên không lóa, in ấn ổn, một số trang in hơi bị xô nghiêng trang giấy nhưng không nhiều và không ảnh hưởng mấy đến nội dung nên vẫn ok. Sách có một số câu từ dịch hơi khiên cưỡng nhưng vẫn có thể hiểu được do cách dùng từ của Conan Doyle từ ngôn ngữ gốc (tiếng Anh) vốn cũng không dễ dịch.
3
478889
2016-05-26 00:17:59
--------------------------
427765
3818
Không nhắc tới những vụ án li kỳ, hiểm hóc mà Holmes trải qua, vì nó đã vô cùng tuyệt vời rồi, ấy  thế mà vị thám tử hư cấu mới nổi tiếng đến thế chứ! Càng đọc đến gần cuối thì mình càng ngưỡng mộ tình bạn đẹp đẽ, chân thành của Holmes và Doctor.Watson, cứ có cảm giác là giữa họ có một tình yêu trong sáng nảy nở vậy. Mình đã thực sự nghĩ rằng Holmes là có thật, và mình yêu nhân vật ấy vô cùng, vì những đức tính đẹp đã, nếu không nói là vĩ đại của vị thám tử tài ba!
5
505121
2016-05-10 01:05:16
--------------------------
422328
3818
Trước giờ tôi rất hứng thú với các tiểu thuyết đề tài phá án, hình sự. Nên tôi đã tìm mua và đọc hai quyển sách trước của Sherlock Holmes. Đây là quyển sách thứ ba và cũng là quyển sách cuối cùng của bộ truyện này. Các tình tiết trong mỗi vụ án luôn hấp dẫn tôi từ những chương đầu tiên của truyện. Một quyển sách không thể thiếu cho những người mê sử dụng khoa học vào những vụ án hóc búa nhưng đã được Holmes giải quyết rất xuất sắc. Cảm ơn tác giả đã mang đến một quyển tiểu thuyết thật tuyệt vời.
5
509425
2016-04-26 23:36:25
--------------------------
417290
3818
Đây là quyển sách của Conan Doyle thứ 3 mà mình có và là quyển sách của Conan Doyle thứ 2 mà mình mua tại Tiki. Những cuộc trinh thám và phiêu lưu tìm lời giải đáp cho những bí ẩn mới trong cuốn sách này càng ngày càng thú vị và hồi hộp, gay cấn. Tuy vậy, không giống như hai quyển trước, quyển sách Sherlock Holmes toàn tập (Tập 3) này có rất nhiều lỗi dịch khá buồn cười và lỗi chính tả bị lặp đi lặp lại ngay từ vụ án đầu tiên "Thung lũng khủng khiếp".
5
711358
2016-04-16 18:14:18
--------------------------
410716
3818
Về hình thức:
+Sách dày, giấy xốp, nhẹ. Bìa đẹp.
+Lần mua này mình đăng kí sử dụng bookcare. nghe mấy bạn nhận xét là Tiki bọc những sách dày thường không chắc, dễ bị rơi nhưng mình thấy rất tốt. Bìa đẹp, bọc rất chắc.
Về nội dung:
+Như mình đã nói ở hai quyển trước, những câu chuyện luôn kịch tính, hấp dẫn và làm mình tò mò.
Cuối cùng thì mình đã có đủ bộ ba quyển, đang cảm thấy rất vui. Cứ lôi ra đọc đi đọc lại hoài à! Căm ơn Tiki rất nhiều ạ!
5
857178
2016-04-04 19:19:54
--------------------------
407189
3818
Sherlock Holmes quả là không làm mình thất vọng. Hình thức bên ngoài lẫn bên trong đều trình bày rất rõ ràng và đẹp mắt, chữ in to và rõ ràng, tuy vậy trang giấy có phần hơi ngả vàng. Nội dung thì không thể chê vào đâu được, nhất là về mạch logic của truyện. Thật sự, truyện vẫn giữ được phong độ qua ba tập liền. Đây quả là một cuốn tiểu thuyết không thể nào thiếu trong bộ sưu tập những người yêu truyện trinh thám như mình.
Mình cũng rất hai lòng về dịch vụ tiki,  rất nhanh và tiện lợi.
5
923880
2016-03-29 16:47:28
--------------------------
386200
3818
Về hình thức, bìa rất đẹp và rất tuyệt, vẽ về nhân vật chính của bộ truyện – Sherlock Holmes. 
Về nội dung thì bằng trí tuệ trời phú, siêu việt, Sherlock Holmes có những lập luận rất logic và chặt chẽ để tóm được thủ phạm. Phụ tá của anh là bác sĩ Watson – người ghi chép lại những vụ án anh đã phá với ngôi kể thứ nhất. Nhưng cũng có vài vụ án mà chính Sherlock Holmes kể như vụ: "người lính bị vảy nến" chẳng hạn. Truyện có rất nhiều tình tiết hấp dẫn, ly kỳ, cuốn hút bạn đọc.
Trong tập 3 này, các bạn sẽ được "thưởng thức" cách phá án của Sherlock Holmes trong các vụ án như: "Vòng tròn đỏ, ma cà rồng ở Sussex, thung lũng khủng khiếp, người lính bị vảy nến,..." 
Đây là một quyển sách trinh thám của nhà văn Athur Conan Doyle mình rất thích. Đây sẽ là cuốn sách trinh thám gối đầu giường cho các bạn mê truyện trinh thám. Mình nghĩ vậy =)))))
4
1142307
2016-02-25 10:51:05
--------------------------
349888
3818
không biết nói gì hơn ngoài 2 từ: tuyệt vời, đến với phần 3 này, các vụ phá án ngày càng li kì và hấp dẫn hơn, Sherlock Holmes với tài năng phá án của mình lại một lần nữa khiến chúng ta vô cùng kinh ngạc và sửng sốt, lối phá án kín đáo, hiệu quả, cộng thêm đức tính khiêm tốn tuyệt vời. holmes đã trở thành biểu tượng lớn trong thế giới truyện trinh thám , trở thành thần tượng trong lòng các độc giả, đặc biệt là các fan ghiền trinh thám như tôi. cảm  ơn tiki rất nhiều
5
1009180
2015-12-09 12:09:10
--------------------------
348877
3818
Ưu điểm: Sách rất nhẹ, cầm lâu không thấy mỏi tay. Giấy xốp, chữ in dễ đọc. Bìa sách đẹp, bắt mắt. Truyện được xây dựng rất logic. Nhân vật cũng được khắc họa rõ nét.Tác giả cực kì thành công khi vẽ nên Sherlock Holmes là một con người kì lạ, có một đầu óc tư duy nhạy bén, và bác sĩ Waston là người bạn tốt của Sherlock Holmes. Các vụ án trong Sherlock Holmes ngắn gọn, nhanh chóng, không dài dòng, không dây dưa. Tôi vô cùng hài lòng với quyển sách này.
Nhược điểm: Không có lỗi chính tả nhưng lỗi dịch thì rất nhiều. Nhiều câu được dịch rất buồn cười
4
514889
2015-12-07 09:23:10
--------------------------
343563
3818
Chất lượng sách tốt. Truyện dày 600 trang nhưng cầm nhẹ tay. Phông chữ đẹp, rõ nét. Nhưng chất lượng dịch thì cần xem xét lại. Nhiều chỗ dịch cẩu thả, dùng từ ngữ không phù hợp. Chẳng hạn, trong cùng lời kể của một nhân vật về hung thủ, câu trước dùng y, câu sau dùng nó.. rất lộn xộn. Hoặc câu trước dùng "nó" để nói về một người, câu tiếp theo lại dùng từ "nó" để nói về một vật. Hoặc dùng từ "nội vụ" để chỉ về một sự việc, vụ việc xảy ra... Thật thất vọng khi mua bản này.  Một cuốn sách với nội dung hay nhưng vấp phải bản dịch dở khiến tôi mất cả cảm hứng đọc nó.
3
775065
2015-11-26 19:35:16
--------------------------
342102
3818
Là fan thể loại trinh thám thì đừng bỏ qua cuốn sách này nhé. Đây là cuốn sách kinh điển cho thể loại văn học này. Truyện được xây dựng rất logic. Nhân vật cũng được khắc họa rõ nét. Tác giả đã rất thành công khi vẽ nên Sherlock holmes là một con người kì lạ, co một đầu óc tư duy nhạy bén, và Waston bác sĩ và cũng là người bạn của Sherlock holmes. Mình nghĩ nên thay đổi bìa sách để hấp dẫn người đọc hơn vì bìa sách hiện tại nhìn còn bình thường quá.
Mình được giao sách hơi lâu, tưởng chỉ có 3- 4 ngày thôi ai dè cả tuần lận.


4
323288
2015-11-23 19:30:52
--------------------------
328551
3818
Nội dung thì chắc mọi người đều biết, nói về những vụ án hóc búa được giải bởi bộ óc thiên tài Sherlock Holmes và người cộng sự John Watson.
Chất lượng của cuốn tái bản lần này khá ổn, giấy nhẹ, xốp, màu vàng ngà nên dù đọc lâu cũng không thấy đau mắt. Mình khá bất ngờ là cuốn truyện hơn 600 trang nhưng cuốn sách vẫn nhẹ, cầm lâu không hề mỏi tay.
Nhưng chất lượng dịch thì cần xem xét lại. Không có lỗi chính tả nhưng lỗi dịch thì rất nhiều. Có nhiều câu dịch rất buồn cười, giống như không xem xét ngữ cảng mà chỉ dịch từng chữ trong tiếng Anh vậy. Hơn nữa nhiều chỗ đặt dấu câu bị sai làm mình bị lẫn lộn giữa lời dẫn và lời thoại.
4
448411
2015-10-29 22:50:25
--------------------------
319122
3818
cũng như 2 cuốn trước, chất lượng giấy và lời văn đều làm hài lòng mình.
chỉ là ở cuốn 3 này, lâu lâu vẫn có lỗi chính tả ( không hề có trong 2 tập trước), và không hiểu sao trong cuốn này, từ "nội vụ" rất hay được dùng.
Ví dụ " Tôi không muốn dính líu quá sâu vào nội vụ", đọc đến đây cảm giác hơi sạn sạn.
Với ở bìa sách, tên được in bằng nhũ bạc, dễ trầy lắm luôn
Nhưng ngoài mấy lỗi xíu xiu đó ra thì chẳng có gì đáng chê. Sách nhẹ, giấy xốp, chữ in dễ đọc, hành văn lưu loát. Đây là lần đầu tiên mình mua sách của NXB này, và mình khá là hài lòng.
4
265156
2015-10-07 22:12:32
--------------------------
317439
3818
Sherlock Holmes là tác phẩm nổi tiếng của nhà văn Conan Doyle. Tác phẩm của ông được coi là một sáng kiến lớn trong lĩnh vực tiểu thuyết trinh thám. Sherlock Holmes là nhân vật thám tử hư cấu do Arthur Conan Doyle sáng tạo ra. Sherlock Holmes nổi tiếng nhờ trí thông minh, khả năng suy diễn logic và quan sát tinh tường trong khi phá những vụ án mà cảnh sát phải bó tay. Và đến với tập 3 tác giả không để độc giả phải thất vọng. Câu chuyện đã kết thúc nhưung với tôi nó vẫn mãi tồn tại!!! Yêu......
5
698653
2015-10-03 13:16:24
--------------------------
314477
3818
Tất cả vụ án trong Sherlock Holmes tôi đều rất không, ngắn gọn, nhanh chóng, ko dài dòng, dây dưa! Tôi đặc biệt thích vụ án "Ba người cùng mang họ Garideb", thực ra vụ đó so với những vụ tầm cỡ như Bản vẽ tàu ngầm hay Cung đàn sau cuối đều ko có gì nổi bật, nổi bật nhất là sự lo lắng sợ hãi của Holmes khi Watson trúng đạn, ngàn vạn lần chẳng dễ gì thấy cảnh tượng đó, cảm động biết bao! Tôi đặc biệt thích cách làm việc của Sherlock Holmes, tùy có nguyên tắc nhưng lại tùy hứng bất ngờ!! Những vụ án mà anh tự ý thả thủ phạm đi chỉ vì anh cho rằng đó là đúng, anh thích sự thật, thích vạch trần nhưng không hề bị gò ép bởi pháp luật hay cái gọi là nhân danh công lý! Tôi thích nhất Sherlock Holmes ở sự phóng khoáng đó!!!
5
516261
2015-09-26 09:37:42
--------------------------
313749
3818
Tập ba này chính là tập mà tôi thích nhất trong cả ba tập bởi vì trang bìa rất đẹp. Màu xanh không quá đậm mà cũng không quá nhạt, nhìn rất thích mắt. Ngoài ra, phần nội dung cũng rất hay. Bắt đầu là câu chuyện "Thung lũng khủng khiếp", chỉ cần nghe cái tựa thôi cũng đã thấy nó thật "khủng khiếp". Mặc dù mình không phải là một người can đảm, nói thật đôi lúc mình cũng không dám xem truyện trinh thám vì sợ nhưng mình đã xem Sherlock Holmes, bởi vì nó rất hấp dẫn. Mặc dù đôi lúc cũng rất sợ nhưng chính vì sự hấp dẫn đó đã thúc đẩy mình phải xem hết bộ Sherlock Holmes này.
4
13723
2015-09-24 16:01:32
--------------------------
292957
3818
Những ai đã đọc Conan thì không thể không biết nhân vật thám tử là thần tượng của Shinichi chính là Sherlock Holmes. Với bộ óc thông minh, linh hoạt, tài tình, Holmes đã thể hiện mình là một thám tử xuất sắc qua từng vụ án khác nhau, đặc biệt là vụ án "Thung lũng khủng khiếp". Còn về ưu điểm thì gần như hoàn hảo về mọi mặt. Đây là một cuốn truyện tuyệt vời, bạn sẽ không thể rời mắt khỏi nó cho đến khi đọc hết. Cảm ơn nhà văn Conan Doyle đã mang đến một nhân vật thám tử thông minh như vậy.
5
432473
2015-09-07 20:45:09
--------------------------
282917
3818
Nếu nói về Holmes thì có lẽ không ai trong số các fan của thể loại trinh thám không biết đến vị thám tử ở số 221B phố Baker này. Holmes là một trong những đỉnh cao của truyện trinh thám, là một tượng đài không thể phủ nhận cùng với những suy luận hợp lí và đầy logic đã khiến cho mọi độc giả phải say mê. Cuốn thứ ba trong số ba cuốn tuyển tập về các vụ án của Holmes là một cuốn truyện hấp dẫn, được mở đầu bằng vụ án Thung lũng khủng khiếp, cũng là một trong 4 tiểu thuyết hấp dẫn mà Conan Doyle viết về ông. Một trong những cuốn truyện mình không thể rời mắt.
5
703335
2015-08-29 15:50:02
--------------------------
260748
3818
Tập 3 của bộ Sherlock Holmes kể về những mẩu chuyện ngắn được sáng tác sau này, mẩu truyện cuối cùng được viết vào năm 1914. Đây là lúc mà tác giả Conan Doyle "giết chết" đứa con đẻ của mình khi để Holmes rơi xuống vực đá có xoáy nước. Tuy nhiên sau cùng tác giả lại để Holmes sống lại thể theo nguyện vọng của toàn bộ fan hâm mộ nước anh. "Sự trở về của Sherlock Holmes", "Cung đàn sau cuối" là những tình tiết mình thích nhất ở bộ này. Bộ sách xứng đáng nhận 10 sao!
5
655125
2015-08-10 17:13:10
--------------------------
241145
3818
Nối tiếp tập 2, tập 3 cũng là một chuỗi các vụ án ly kỳ,hấp dẫn không kém. Tác giả Conan Doyle một lần nữa bộc lộ tài viết lách của mình và chính điều đó càng làm mình yêu thích nó hơn. Đọc sách này mình rất ngưỡng mộ Sherlock Holmes bởi những suy luận tài tình của ông qua đó mình cũng học hỏi được rất nhiều: óc quan sát, khả năng phân tích tình huống, cùng với kiến thức hết sức sâu rộng về một số lĩnh vực trong xã hội. Mình rất thích cuốn sách này. Còn bạn thì sao?
5
709167
2015-07-25 11:03:45
--------------------------
229307
3818
Ưu điểm: Tôi không muốn nói nhiều về nó nữa, vì có lẽ đã quá đủ, tên tuổi của nó. Tôi biết đến Sherlock Holmes qua manga trinh thám - Conan từ lâu rồi. Nhưng bây giờ mới có cơ hội để khám phá một kiệt tác như vậy. Đó sẽ là những của quý trong kho tàng trinh thám cổ điển thế giới mà mãi về sau, rất lâu sau này nó vẫn là một bộ truyện đáng đọc. Sẽ được xuất hiện trong nhà in mỗi năm mà bất cứ độc giả nào có thể phủ nhận giá trị của nó dù có phải là fan trinh thám hay không?
Khuyết điểm nho nhỏ: Do có hơi nhiều dich giả trong một tác phẩm nên khiến người đọc dễ bị hoang mang và có một ít lỗi chính tả, nhưng không sao, bạn hãy tìm đọc và giá trị cốt lõi của nó không bao giờ khiến bạn thất vọng.
4
516742
2015-07-16 18:41:02
--------------------------
205294
3818
tập ba trong bộ này khiến mình thấy gần như trọn vẹn cả quá trình viết tác phẩm, dẫu biết là chưa thật sự đầy đủ các vụ án của Conan Doyle nhưng mình thích vì nó được viết rất bài bản có hệ thống, tập này những vụ án chủ yếu là rải rác nhiều giai đoạn của Holmes và Watson. vụ người đàn bà che mạng trên mặt làm mình thích nhất vì nó nói lên tính hai mặt của  con người...  hay Cung đàn sau cuối của cuộc đời holmes.. đọc mê tơi và chất lượng sách cả về giấy và bìa đều tuyệt...
5
568747
2015-06-06 12:34:44
--------------------------
