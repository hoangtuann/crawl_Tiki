324593
4916
Mình đọc kiếm hiệp từ nhỏ nhưng chưa có bộ sách nào có cốt truyện và tình tiết lạ,hấp dẫn như bộ này,có thể vì chủ yếu nói về phong thủy ! Sau 2 tập đầu thì mình lại càng sốt ruột đọc tập 3,ở tập này tác giả khai thác nhiều về tâm tư,cảm xúc của chàng trai thiếu niên An Long Nhi,cũng vì thế mà thấy hơi buồn khi An Long Nhi bắt đầu có những mâu thuẫn về mặt cảm xúc đối với Kiều Kiều !Buồn nhất là đoạn Kiều đuổi chồng đi rồi trảm song long,còn lại tập này chủ yếu xoay quanh đội quân Thái Bình đang trên đường tạo phản ! nhìn chung tập 3 không kịch tính như 2 tập trước nhưng xoáy sâu vào cảm xúc,suy nghĩ của các nhân vật,đọc cứ như ngôn tình hehe
4
594091
2015-10-21 15:47:30
--------------------------
269689
4916
Tập này mình không thích nên cho 4 sao thôi. Sau 3 năm học nghề, Long Nhi tái xuất giang hồ đi tìm lại trảm long quyết. Thanh Nguyên dù không được truyền cách thức trảm long nhưng dựa vào sự thông minh của mình y cũng tìm ra cách trảm long thành công gây ra đại nạn cho dân chúng. Lúc này quân phản Thanh của Hồng Tú Toàn giao tranh dữ dội với quân triều đình. Kiều Kiều cùng Jack bán vũ khí và tham gia đánh trận cùng quân HTT . Hiểu lầm hành động của KK đối với TN, Long Nhi lại lặng lẽ đi cứu con của Jack cùng bạn bè và không tái hợp với KK. Vì tính mạng của Jack, KK để cho Jack ra đi tìm con còn mình đơn độc ở lại nghĩa quân chiến đấu. Phần này thiên nhiều về đánh trận mình và có sự hiểu lầm giữa KK và LN nên mình không thích bằng 2 tập kia.
4
43129
2015-08-17 17:55:01
--------------------------
247661
4916
Trảm Long là 1 trong những cuốn truyện khiến người xem phải chờ đợi ở mỗi lần xuất bản. Có lẽ Nhã Nam đã biết cách khiến người khác phải thật sự cảm thụ hơn là ''tấn công ồ ạt''. Mỗi tập của Trảm long đều mang những âm hưởng khác nhau. Với tập này, thật sự cuốn hút với sự ttrưởng thành của các thành viên trong nhóm. Sự nghi ngờ của Long Nhi , cách xử lý của Kiều Kiều, liều ăn nhiều của Jack. Trong tập này mình học được cách bấm đốt lóng tay...mặc dù chưa hiểu ý nghĩa :d

5
365692
2015-07-30 12:11:24
--------------------------
242378
4916
Ở tập ba này, đội quân tóc dài Thái Bình Thiên Quốc được khắc họa rõ nét. Nguyện vọng, mong mỏi của quân nổi loạn, của từng ví trí lãnh đạo. Tuy nhiên, trong thâm tâm mình quân Thái Bình chỉ là quân tạo phản 100%, điêu toa, mưu cầu lợi ích cá nhân, không vì dân vì nước. Hình ảnh Lục Kiều Kiều được đẩy đến vị trí cao nhất khi cô quyết tâm vì quân Thái Bình mà trảm Bạch Long, Xích Long, hy sinh lợi ích sinh con của riêng mình với Jack để đạt đến đỉnh cao của võ học, quyết tâm trảm long mạch nhà Hán để giúp quân Thái Bình. Còn sự hy sinh nào cao hơn nữa khi phải hy sinh hạnh phúc cá nhân, còn đau đơn nào cao hơn nữa khi phải xua đuổi chồng đi chỗ khác để tránh họa sát phu, còn tình nghĩa nào cao hơn nữa khi âm thầm cứu giúp con riêng của chồng... Hay, nên đọc. Háo hức chờ phần 4.
4
129879
2015-07-26 16:12:19
--------------------------
216791
4916
Bởi thích sách viết về phong thủy nên ngay từ khi đọc lời giới thiệu về sách đã thích vô cùng. Đã đi đến tập 3 rồi, có thể nói trong 3 tập thì tập 1 tạo ấn tượng mạnh nhất về kiến thức phong thủy. Tập 2 là cuộc chiến đấu giữa hai thế lực triều đình đang muốn hủy diệt tất cả long mạch trong thiên hạ để bảo vệ ngôi báo và những thầy phong thủy, huyền học dân gian không muốn sinh linh đồ thán vì khi trảm long mạch sẽ gây ra những thảm họa thiên nhiên tàn khốc chắc chắn ảnh hưởng đến sự yên ổn của dân lành. Tập 3 lại nói nhiều về tổ chức chống triều đình của Hồng Tú Toàn.

Có thể nói đây là bộ sách về phong thủy khá hay mà mình đã đọc.
5
790
2015-06-28 11:25:50
--------------------------
