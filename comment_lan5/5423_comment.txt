264437
5423
Mua cuốn Sách 3D: Những Loài Thú Lớn Họ Mèo thật là không vô ích. Trọn bộ gồm rất nhiều cuốn.
Hầu hết thì cách trình bày giống nhau, giấy in khổ lớn (A4 thì phải) bìa cứng, dày. Toàn bộ đều in màu và màu sắc rất sống động, nhìn rất chân thật, có những câu chuyện, mô tả và chú thích rõ ràng, dễ hiểu. Một số trang có in 3D, dạng 3D phải dùng kính mới có thể xem được. 
Kính 3D tặng kèm được thiết kế bằng bìa cứng làm khung kính nên phải cẩn thận nếu không dễ rách, không được thấm nước. Đã kiểm tra kính, ngoại trừ việc kính hơi tối, khó nhìn và mau mỏi mắt, thì đã test xem hình và các phim 3D đều xem được ( để độ sáng cao nhất).
3
683464
2015-08-13 10:23:19
--------------------------
