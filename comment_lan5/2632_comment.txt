374608
2632
Với trẻ nhỏ, việc học không phải là một sớm một chiều mà phải từ từ rèn luyện, vừa học vừa chơi, thì bé mới thích thú, hứng khởi, và kiến thức sẽ nhẹ nhàng ngấm  dần vào trí não bé. Với cuốn sách "Tập Tô Và Làm Quen Với Chữ Số", bé sẽ được làm quen và học dần với các con số, cách đếm số, cách viết số. Các hình ảnh minh họa cụ thể, sinh động, cùng với nhiều bài tập đa dạng theo hình thức trò chơi, các em sẽ hứng thú hơn trong học tập. 

3
402531
2016-01-26 17:23:40
--------------------------
