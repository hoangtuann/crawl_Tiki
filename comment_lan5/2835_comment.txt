329198
2835
Tập truyện có ba câu chuyện: "Sói xám và ba chú heo con", "Khỉ cứu nguy", "Chú rùa kiêu ngạo". Những câu chuyện ngộ nghĩnh, nhẹ nhàng nhưng chất chứa nhiều bài học bổ ích. 
Sói gian ác phải trả giá bằng cái chết chìm. Chú khỉ con nhờ vào hành động nhỏ của mình đã cứu sống được mẹ của thỏ con. Còn chú rùa kiêu ngạo lại phải sống với phương Bắc lạnh lẽo trong khi ý định khi được ngỗng đưa rùa đi là sang phương Nam ấm áp. Tính cách quyết định hoàn cảnh và ý thức của từng con vật có trong câu chuyện.
Hình ảnh sinh động, nhìn vào từng bức tranh mà có thể thấy được từng trạng thái cảm xúc mà các con vật thể hiện trong câu chuyện.
4
680594
2015-10-31 11:32:07
--------------------------
