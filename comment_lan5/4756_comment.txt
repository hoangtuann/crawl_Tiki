472461
4756
Tôi từng đọc tác phẩm " Gió Mùa Hè " của tác giả này. Nói chung tác phẩm tạm ổn từ ngôn ngữ cho đến nội dung. Nhưng tác phẩm này gây cho tôi nhiều thất vọng. Mạch truyện dài dòng, tù túng, cảm giác tác giả không thoát ra được chính bản thân mình, suốt gần ba mươi trang sách nhưng điểm lại nội dung truyền tải không được bao nhiêu. Đôi lúc tác giả làm mình rất bực. Những nhân vật so với tác phẩm trước không có gì mới lạ. Đây là quyển sách tôi đọc lâu nhất từ trước đến giờ. Vì đọc mấy trang lại thấy chán không muốn đọc tiếp.
2
1224559
2016-07-09 16:44:39
--------------------------
339557
4756
Trong các tác giả văn học lãng mạn thì có lẽ C.Anderson là một trong những tác giả viết truyện hướng về giá trị con người nhiều nhất. Với tác phẩm của bà, những con người mang khiếm khuyết trên cơ thể được nhắc đến như một giá trị tiềm ẩn đằng sau những khuyết tật mà họ mắc phải. Annie được xem như là một cô gái có trí tuệ thấp kém, bị điếc và ngây ngô như đứa trẻ nhưng sự thật cô là người như thế nào thì phải đến khi Alex đón cô về và đối xử với cô bằng cả tình yêu thương của mình thì lúc ấy những phẩm chất của Annie mới dần được bộc lộ. Annie không điên, không ngu ngốc, cô chỉ bị điếc khi còn rất nhỏ nhưng vì sự thiếu quan tâm từ cha mẹ mà Annie đã phải đánh đổi cả quãng đời trưởng thành của mình để sống trong sự cô độc và xa lánh từ người khác.

Đối với Alex, anh thật là một chàng trai vô cùng tuyệt vời, đức tính kiên nhẫn của Alex là điều mà tôi ngưỡng mộ nhất. Thật khó để mà giao tiếp, chăm sóc, đưa một người khiếm khuyết trở về cuộc sống đời thường đồng thời yêu thương họ bằng cả trái tim và cũng khiến họ yêu lại mình, nhưng chính Alex lại làm được điều không tưởng đó. Một quá trình đầy khó khăn với Alex nhưng thành quả mang lại cho anh chính là một Annie bình thường, Annie biết yêu, Annie đem đến cho anh đứa con mà anh khao khát, là niềm hạnh phúc không thể thay thế của anh. 

Câu chuyện của C.Anderson luôn thấm đẫm tình người và giá trị nhân văn đồng thời đề cao những phẩm chất cùng nghị lực của những người mang khuyết tật trên cơ thể. Có thể thấy với Annie, cuộc đời cô chính thức thay đổi kể từ khi đến sống với Alex, bởi những con người trong ngôi nhà ấy đều dành cho cô tình yêu thương và sự tôn trọng, điều mà Annie không có khi sống với cha mẹ mình. Cha mẹ cô có quan tâm đến con dù rất ít nhưng điều quan trọng là họ lại không biết cách yêu con và để cho danh vọng, tiền tài che mờ cảm xúc của mình. Tôi thật sự rất thích những cuốn sách của C.Anderson với sự nhẹ nhàng, ngọt ngào, đầy lãng mạn và tràn ngập tình yêu. Truyện của bà luôn mang lại cho tôi sự ấm áp trong tim với nhiều cung bậc cảm xúc thú vị.

4
41370
2015-11-18 11:14:32
--------------------------
253911
4756
Đây là lần đầu tiên mình đọc 1 tác phẩm của Catherin Anderson. Vì vậy ngay từ đầu mình đã rất đắn đo có nên mua hay không vì quả thật tác phẩm khá dày. Rồi mình quyết định mua. Bây giờ mình rất vui vì quyết định đó. Mình đã đọc khá nhiều tiểu thuyết lãng mạn tình cảm về chủ đề tình yêu với bối cảnh Châu Âu thế kỉ XVIII - XIX, và đây là một trong ít cuốn sách mình cho là tuyệt. Văn phong mềm mại, lãng mạn, trữ tình mang đến cho người đọc cách nhìn mới mẻ và tinh tế. Tạo hóa không tạo ra con người hoàn thiện nhưng cũng không lấy đi tất cả, những người khiếm khuyết về thể chất thì càng có nét đẹp tinh thần ngời sáng.
Annie biết tất cả những bất công, sự khinh miệt của người đời và chính cả cha mẹ dành cho mình mà lẽ ra mình không đáng bị đối xử như vậy nhưng không vì thế cô chìm đắm trong buồn đau, thù hận. Trái lại, cô vẫn sống vui vẻ, tự tạo dựng cho mình một thế giới riêng đẹp và ấm áp, không hề thù hận hay ghét bỏ cha mẹ, cô vẫn yêu quý họ vì họ. Và  Alex thật may mắn đã nhẫn nại để khám phá ra và dám bước vào thế giới riêng đầy màu sắc đó của Annie.

5
275160
2015-08-04 20:53:11
--------------------------
192082
4756
Đây là tác phẩm thứ hai của tác giả Catherine Anderson mà mình đọc sau Điệu Waltz dưới ánh trăng. Thật mừng là tác giả vẫn giữ lối văn phong lãng mạn, hài hước, trau chuốt từng câu chữ. Mình đã bật cười trước nỗi sợ bé xíu của Alex và cái cách anh vượt qua nỗi sợ đó chỉ vì Annie. Dù cách viết hay, nhưng nội dung tác giả xây dựng vẫn còn rất nhạt. So với Điệu Waltz dưới ánh trăng, tác giả vẫn yêu thích mô típ, nữ chính xinh đẹp, khả ái nhưng bị khiếm khuyết 1 bộ phận cơ thể, rồi may mắn gặp được nam chính tuyệt vời, yêu thương cô hết mực. Nói thật, mình cảm thấy cuốn sách khá dày, nhưng nội dung lại có chút lan man, chuyển biến tâm lý của Alex từ thương hại sang yêu  Annie có chút không rõ ràng. Hơn nữa, mình đã mong chờ có 1 khúc cao trào ở cuối truyện, như là sự trả thù của em trai Alex chẳng hạn, thế nhưng tác giả lại kết thúc truyện 1 cách êm đềm và quá dễ dàng, khiến mình có chút hụt hẫng. Dù sao thì, mình cũng rất cảm kích mối tình tuyệt đẹp của Alex và Annie để học được cách nhìn nhận và đánh giá con người không phải bằng mắt mà bằng trái tim kiên nhẫn và tràn đầy yêu thương.
3
51157
2015-05-03 16:24:37
--------------------------
180081
4756
Annie cô bé bị điếc sau một trận sốt hồi từ 6 tuổi, khiến cô đối xử tàn tệ bởi chính cha mẹ mình, nhưng Annie vẫn là cô bé đáng yêu với một tâm hồn đẹp. 

Cái cách Alex mang âm thanh, tình yêu và hy vọng đến với cuộc sống vô thanh của  Annie thật ngọt ngào và đầy tính nhân văn. Cũng như cách Annie mang hạnh phúc vào cuộc đời không màu sắc của Alex. Họ biến cuộc hôn nhân tiện lợi thành hạnh phúc mãi mãi về sau.

Mình thích cách  hành văn của Catherine Anderson dí dõm, đáng yêu, nhưng cũng rất cô đặc. Đôi khi chỉ cần một tấm lòng, một chút yêu thương, một chút nhẫn nại, bạn hoàn toàn có thể thay đổi cuộc sống của một ai đó
5
313623
2015-04-08 17:15:02
--------------------------
