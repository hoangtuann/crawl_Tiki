422766
2648
Mình rất thích những quyển sách có nội dung đơn giản, hình vẽ sống động, dễ thương, tập trung vào nội dung muốn truyền tải như quyển này, rất phù hợp với những em bé nhỏ dưới 2 tuổi như con mình (hình như sách này của Nhật, mình thấy hình vẽ đều có liên quan đến cuộc sống ở Nhật). Sách in giấy bìa cứng nên không sợ bé làm rách. Tóm lại là một quyển sách rất đáng mua. Mua trên tiki lúc nào cũng được giảm giá so với giá bìa nên mình thường đặt một lúc nhiều cuốn cho bé.    
5
624686
2016-04-27 23:53:28
--------------------------
