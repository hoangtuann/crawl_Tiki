137428
6588
"Câu chuyện nhỏ - bài học lớn" là cuốn sách rất hay và thú vị, rất cần thiết với mỗi chúng ta. Cuốn sách có bìa ngoài đẹp đẽ và hấp dẫn. Ẩn chứa bên trong nội dung cuốn sách là những câu chuyện hết sức bình dị trong đời sống của những bậc trí thức, các bậc thiền sư- luôn mang trong mình tâm hồn thanh tịnh, các bậc doanh nhân,... hay là những người rất đỗi bình thường trong cuộc sống,.. Qua những câu chuyện của họ, người đọc tự ý thức được những chân lý, những kinh nghiệm hết sức đơn giản mà vô tình ta lãng quên trong cuộc sống. Qua những câu chuyện như thế ta tự thấy hình bóng của mình trong một vài tuyết nhân vật. Có thể là nhân vật ác, hoặc cũng có thể là nhân vật thiện,... Nhưng chỉ cần ý thức và thay đổi, cuộc sống của ta sẽ hạnh phúc và tươi vui hơn rất nhiều. Cuốn sách phù hợp cho mọi lứa tuổi. Trẻ nhỏ đọc để học hỏi cái hay, cái tốt, người lớn đọc để nhận ra cái đúng và sai của mình
5
414919
2014-11-26 16:32:11
--------------------------
