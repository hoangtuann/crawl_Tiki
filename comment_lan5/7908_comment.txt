457891
7908
- Hình thức: sách đẹp từ bìa tới giấy in, tới ảnh, chỉ thỉnh thoảng các trang chữ nền đen in không nét. Nhưng giá bìa 249k là quá đắt để sở hữu quyển sách này 
- Nội dung: Các trang viết kèm với các trang hình ảnh về Siêu mẫu Hà Anh. Phần đầu là những người thân viết cho chị, phần sau là chị ấy tự viết, những đối tác khen ngợi về con người chị.
 Thấy nội dung lan man, giọng văn không đặc sắc nên thực sự không mê, đọc xong không nhận được thông điệp gì cả. Có lẽ sách chỉ hợp với những người hâm mộ chị.
2
438736
2016-06-24 17:14:19
--------------------------
413214
7908
Thích và ngưỡng mộ Hà Anh cùng sự nghiệp của cô ấy đã lâu, đọc cuốn sách này xong có phần yêu thích cô ấy hơn nữa. 
Một cuốn sách được trau chuốt đến mức gần như hoàn hảo trong phần hình ảnh, in ấn. Và cách Hà Anh viết vẫn chưa bao giờ khiến tôi thất vọng. Đây là những trải lòng rất thật của một người phụ nữ thành đạt, một cô siêu mẫu có cái đầu thông minh, một trái tim mạnh mẽ và một cá tính không thể bị phủ nhận. Hà Anh rất khéo mà cũng rất thẳng, khéo là bằng câu chữ mà cho người ta thấy cô ấy là người như thế nào, khiến người ta ngưỡng mộ vô cùng; thằng là vì cô ấy dám nói ra những điều khuất tất của một cuộc thi, những sự lừa dối giả tạo của giới showbiz ngay trong cuốn sách của mình. 
Có lẽ trong showbiz Việt hiện nay, nhất là giới người mẫu, hiếm có chân dài nào được như Hà Anh. Vừa có một cái đầu trí tuệ, vừa có một thân hình tuyệt đẹp! 
"Là tôi, Hà Anh!" như một khẩu hiệu của chính cô ấy, không lẫn lộn với ai khác, lại như một nguồn cảm hứng cho những người con gái khác hãy phấn đấu mỗi ngày để trở thành một người phụ nữ tỏa sáng dù trong vai trò gì đi nữa.
5
81138
2016-04-09 10:13:02
--------------------------
371649
7908
Mình mua cuốn này ở hội sách vì được giảm giá khá tốt, cuốn này cũng được pr rất nhiều về tài năng viết lách của Hà Anh, tiết lộ bí mật showbiz nhưng đọc thì mình rất thất vọng. Cuốn sách không có gì đặc sắc hơn là một tập hợp các bài viết từ blog hay facebook và mang nặng tính quảng cáo, như thể chúng ta chưa biết Hà Anh nổi tiếng và tài năng như thế nào. Tất cả quyển sách chỉ nói về việc cô ấy thông minh xinh đẹp giỏi giang ra sao, nhiều nguời ngưỡng mộ, theo đuổi cô như thế nào 
Không nên đọc nếu không phải là fan.
Ưu điểm là giấy đẹp
2
306081
2016-01-20 08:48:24
--------------------------
367042
7908
Nhiều bài viết trong sách tôi đã từng đọc đi đọc lại nhiều lần trên blog và fanpage của Hà Anh. Tôi thích cách chị nhìn đời và tận hưởng cuộc sống lắm lắm. Bởi vậy tôi quyết định đặt mua tác phẩm, dù tâm niệm rằng chắc nó cũng chỉ tập hợp lại những bài viết cũ đã từng công bố. Tôi muốn thể hiện sự ủng hộ và biết ơn của tôi dành cho "người chị ngôi sao" này. Và trời ơi, khi nhận được cuốn sách, tôi thấy mình thật... Hời :D. Không chỉ là một cuốn sách, đây rõ ràng là một tác phẩm nghệ thuật! Bìa cứng cáp, từng trang giấy bóng bẩy, sắc nét, đẹp đẽ. Và một lần nữa, tôi lại thêm khâm phục sự thông thái và chuyên nghiệp của Hà Anh! Tôi bỏ tiền ra mua, và rõ ràng tôi nhận được những thứ hay ho hơn nhiều việc đọc miễn phí trên mạng. Các cuốn sách thật - "bằng giấy bằng mực" có lợi hơn ebook là có thể ngắm, có thể sờ, có thể ngửi... dùng để gối đầu, đập ruồi hay đập vô mặt đứa nào mình ghét...và cuốn "Là tôi, Hà Anh" hoàn thành xuất sắc bất kể nhiệm vụ nào ở trên :p. Thêm nữa, nội dung trong sách cũng có nhiều bài viết chưa hề xuất hiện trên mạng. Nói chung là tôi thấy mình rõ ràng đã được một món hời rồi, cảm giác hài lòng với bản thân trong vai trò người tiêu dùng thông thái quá! Tôi sẽ tiếp tục ủng hộ Hà Anh ^^ 
4
1084675
2016-01-11 12:35:01
--------------------------
328597
7908
Kì thực tôi chưa đọc hết, nhưng chỉ khoảng nửa quyển sách thôi thì tôi đủ cơ sở để khẳng định: Tôi đã sai khi không đặt mua nó sớm hơn. Nói đến chân dài viết sách, chắc nhiều người vẫn nhớ "Sợi xích" của Lê Kiều Như. Qủa thật tôi cũng chưa có "vinh dự" được đọc cuốn sách ấy, nhưng tôi tin chắc rằng cuốn sách này hoàn toàn khác những gì bạn đã nghĩ cũng như đã thấy về việc chân dài viết sách. Nó đơn giản chỉ là những câu chuyện phím, chuyện đời tư, chuyện cuộc sống và chuyện kể về hành trình nỗ lực của một cô chân dài sinh năm 1982. Ở hiện tại, chưa thể gọi Hà Anh là một người thành công mỹ mãn nhưng vẫn gọi là có chút thành tựu. Ở cái tuổi đó, chưa thể coi Hà Anh là một người gạo cội và lão làng. Tất cả những gì có trong quyển sách đơn giản chỉ là sự chia sẻ và những lời động viên từ một người đi trước dành cho những người trẻ hơn đi sau.
Nếu bạn là một người muốn tìm thấy những góc nhìn chân thực của cuộc sống người nổi tiếng, những điều mới mẻ cho cuộc sống, hay những thứ vượt qua giới hạn bình thường của xã hội, hãy đọc "Là tôi, Hà Anh". Đọc tự truyện để thấy một bé Ly cũng là một đứa trẻ rất đỗi bình thường như tất cả những đứa trẻ khác: cũng hái hoa bắt bướm, nghịch ngợm, ham chơi, lém lỉnh "thó" đồ của hàng xóm hay nói dối ba mẹ. Hoặc để thấy một Hà Anh tuổi 16, cũng nhẹ nhàng, đáng yêu, nhiều suy tư và e dè trước "đám đông công lý" trước khi trở thành một phụ nữ thẳng thắng, ngoan cường và tự tin để khác biệt. Vâng, thứ làm tôi luôn yêu chị chính là cách chị động viên những người trẻ như tôi dám tự tin để khác biệt. Chân lý luôn thuộc về số đông, khác biệt nghĩa là đi ngược lại điều đúng. Cả thế giới cởi truồng thì đứa duy nhất mặc quần áo chính là kẻ khiêu dâm - tôi không có ý mỉa mai, nhưng thực sự đó là chân lý. Làm điều đi ngược lại với số đông trước tiên là bị kẻ khác trao cho ánh nhìn e dè và nghi ngại. Tiếp đến là những lời dè bỉu, chê bai, thậm chí là nguyền rủa. Tệ nhất là khi bạn bị mọi người trù dập và ném đá vì đi ngược lại nguyên tắc của số đông. Nhưng hãy nhớ, xã hội không phát triển vì số đông mà nó nhích lên vì có những người dám đi tiên phong và tạo ra điều khác biệt. Dĩ nhiên, kẻ dám khác biệt sẽ chịu nhiều áp lực cả về thể chất lẫn tinh thần. Họ phải cố gắng nhiều hơn kẻ khác và thường tự mình an ủi mình trong những lúc khó khăn. Cuốn sách như một lời động viên từ một người đã thành công vì dám khác biệt, dám "ngẩn cao đầu giữa đám đông thay vì cúi thấp xuống cho bằng họ".
Tôi, một cậu nhóc dị biệt nên phải luôn khác biệt, dù đôi lúc thực sự mệt mỏi. May mắn thay, tôi biết về chị và cầm trên tay quyển sách của chị để những lúc mệt mỏi nhất tôi lấy nó làm lời động viên. "Cố lên cậu nhóc! Em đang làm đúng và thành công chỉ cách em vài bước nữa thôi!" - cuốn sách nói thế. Xin nhấn mạnh, không ai đúng - ai sai, cơ bản chỉ là quan điểm khác biệt nên xin đừng ném đá hay phản pháo tôi. Nếu bạn là một người sống theo kiểu an nhàn, hòa vào đám đông mà đi thì cuốn sách và Hà Anh với bạn là những thứ "chướng tai - gai mắt". Còn nếu bạn là một người muốn sống vì những điều mới mẻ và khác biệt, cuốn sách và chị Hà Anh xứng đáng để bạn tìm hiểu.
Xã hội mà ai cũng chọn riêng cho mình một hướng riêng, ai cũng muốn mình phải khác biệt thì sẽ đại loạn. Nó không khác gì mỗi con ngựa kéo một cỗ xe về một hướng. Kết quả là cỗ xe vỡ thành nhiều mảnh và chẳng đi được đến đâu. Ngược lại, nếu cả xã hội là một chủ thể đồng nhất, cuộc sống sẽ nhàm chán và phát triển chậm chạp. Đó chỉ là tính hai mặt của vấn đề.
5
887161
2015-10-30 00:17:53
--------------------------
103720
7908
Mình có follow trang fanpage của Hà Anh & rất ấn tượng với cá tính cùng suy nghĩ của chị ấy. Không giống những trang của người nổi tiếng khác, Hà Anh rất chịu khó viết: về cuộc sống, về con người, hay về ước mơ, sự nghiệp... Mỗi ngày trôi qua là một trải lòng của "chân dài" sexy, tự tin & sâu sắc này. Mình cũng có đọc thử một vài đoạn trích dẫn rồi mới quyết định bỏ tiền mua sách. Điểm mình thích nhất là giọng văn của Hà Anh không miên man, lòng vòng như những tác giả trẻ ăn khách hiện giờ. Nó thẳng thắn, mộc mà vẫn rất con gái, rất lãng mạn. Cuốn sách mang tựa đề khá kiêu - "Là tôi, Hà Anh!" - nhưng không nhuốm màu sắc huênh hoang, tự sướng về bản thân. Bạn có thể bắt gặp chính mình đâu đó trong những dòng sẻ chia rất gần gụi của Hà Anh. Và nếu có duyên, sẽ tìm thấy lời khuyên chân thành cho sự nghiệp, tình yêu & cuộc sống từ cô gái từng trải ấy ;)
4
34073
2014-01-03 01:02:02
--------------------------
101251
7908
Khi mới nghe đến tên Hà Anh Vũ, mình đã không nghĩ rằng những gì mình được đọc lại giản dị, chân thành và đáng yêu đến như vậy.
Sách rất hấp dẫn, những câu chuyện ấu thơ, những câu chuyện showbiz, cuộc sống, tình yêu, đặc biệt là những đoạn Hà Anh chia sẻ về bí quyết để "cưa đổ" những chàng trai. Mình đã đi từ ngạc nhiên này đến ngạc nhiên khác. Vì thấy rằng, một "ngôi sao" hóa ra cũng có thể gần gũi, chân thành và rất "người". úuốn sách đã cho thấy một Hà Anh ngoài showbiz rất khác, và mình mong cuốn sách mở đầu này sẽ tạo động lực để Hà Anh viết nhiều hơn.
Sách rất đẹp. Có lẽ ở Việt Nam, mọi ng` thường đánh giá sách đắt hay rẻ dựa vào giá tiền của nó. Nhưng rõ ràng, một cuốn sách khổ lớn, bìa cứng, in màu toàn bộ và hấp dẫn, xứng đáng với mức giá như vậy.
5
54041
2013-12-03 10:40:31
--------------------------
100115
7908
Mình khá hài lòng về hình thức của cuốn sách: bìa cứng, dày dặn, hình ảnh in trên giấy couche, đẹp và sắc nét. Tuy có vài đoạn nền đen chữ trắng (có nhẽ là chị cố tình làm thế vì nền đen là bắt nguồn giai đoạn chị vào showbiz) đọc lâu hơi mỏi mắt nhưng chung quy quyển sách này đạt chất lượng tốt. Mở đầu cuốn sách là những bài thơ của ông chị Hà Anh gửi đến cô cháu gái. Ở đây tập hợp rất nhiều hình ảnh của chị Hà Anh từ lúc bé đến khi trưởng thành. Văn phong gần gũi và giản dị. Nó như quyển nhật kí, kể chuyện, trải lòng của chị. Nhiều ảnh sexy (khoe dáng chuẩn đây mà).
3
87298
2013-11-21 18:20:49
--------------------------
