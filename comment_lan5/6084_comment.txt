402836
6084
Tác phẩm này của Nguyễn Trí Công vừa khắc họa hình ảnh một gia đình tan vỡ với hai người con bất hạnh là Oanh và Hiếu đành ngậm ngùi nhìn bố mẹ chúng chia lìa, đồng thời vẽ nên một câu chuyện tình yêu tuổi mới lớn hết sức ngọt ngào với những cảm xúc yêu thương vừa chớm nở, những cuộc giận hờn vu vơ nhưng cũng không kém phần đau đớn trước cái chết của người mình yêu của Cúc Phượng và chú Cường. Cốt truyện hay, miêu tả và biểu cảm đặc sắc chính là yếu tố hấp dẫn người đọc của tác phẩm này. Khi đọc xong quyển sách này, em đã buồn cả một ngày trước cái kết không có hậu của câu chuyện. Sách có bìa đẹp, giấy chất lượng tốt, hình ảnh minh họa đẹp. Cảm ơn Tiki!
3
1136124
2016-03-22 22:18:51
--------------------------
305078
6084
Văn phong đơn giản, nhẹ nhàng, kết cấu truyện khá an toàn. Một câu chuyện về gia đình, bạn bè, tình yêu trong thời loạn khá cảm động và mang đậm tính thực tế. Tuy nhiên rất cần ở tác giả việc xây dựng được cao trào mạnh mẽ hơn, gay cấn hơn. 
 Xúc động nhất trong cuốn sách là ở phần kết, sự ra đi cao quý của chú Cường, một cái chết đường đột, bất ngờ, để lại đằng sau là nỗi đau đớn tột cùng của người mẹ già, anh trai, 2 đứa cháu nhỏ và đáng thương nhất là cô người yêu bé bỏng Cúc Phượng - một bông hoa tình yêu vừa chưa kịp nở đã vội héo úa.
3
485082
2015-09-16 20:29:33
--------------------------
