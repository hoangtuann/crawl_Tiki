468323
4469
Tiki giao hàng nhanh, đúng hẹn, gói hàng cẩn thận. Quyển tài liệu luyện thi năng lực hsk này in đẹp, sách rõ chữ, không bị nhòe mực. Nội dung sách không quá khó, trình bày logic, rỗ ràng. Mỗi bài tập đều có đáp án ở phía sau giúp mình tự đánh giá được mức độ của bản thân. Đĩa CD kèm sách chất lượng tốt, không bị rè, phát âm rõ ràng. Sách hữu ích cho người tự ôn luyện. Khi mua mình còn được tiki tặng kèm một coupon Vaseline. Rất hài lòng về chất lượng phục vụ của tiki
5
902898
2016-07-04 22:11:53
--------------------------
430231
4469
Mình đã đặt mua và được giao hàng rất sớm. Sách được bọc trong hộp giấy cứng nên không bị có nếp hay bị rách chỗ nào cả. Sách bản đẹp và có cả phần đáp án sau sách để sau khi làm xong mình có thể kiểm tra lại. Được tặng kèm đĩa CD. Mình thấy sách rất hữu dụng với những bạn muốn thi HSK. Quyển một thì phần bài tập vẫn còn khá dễ. Hi vọng quyển hai sẽ có nhiều phần bài tập nâng cao hơn. Mình còn được tặng bookmark. Mình chuẩn bị đặt mua thêm quyển hai.
4
1353641
2016-05-14 20:31:50
--------------------------
372180
4469
Mình đang học tiếng trung để thi chứng chỉ HSK. Vì mới bắt đầu học sang quyển 3 nên giáo viên cũng chưa cho tài liệu luyện thi. Lên Tiki tham khảo và đọc nhận xét của mọi người nên quyết định mua một lúc ba quyển 1,2 và 3 luôn. Mình rất hài lòng về giáo trình này. Đây là tài liệu tự ôn luyện khá hay. Ngoài ra, giấy của sách đẹp, chữ và hình rõ nét và CD nghe cũng rất rõ. Sách giao cũng rất nhanh. Mong Tiki sẽ cập nhật nhiều sách Hán ngữ bổ ích hơn nữa.
5
66022
2016-01-21 09:33:26
--------------------------
307322
4469
Mình đã theo học Hán ngữ được gần một năm nay. Lúc mới vào học những ngày học đầu tiên đã được giáo viên hướng dẫn mua cuốn sách này. Sách tổng hợp các cấu trúc đề và các điểm ngữ pháp cơ bản của kì thi HSK. Sách được viết bằng 100% tiếng Trung nên chắc sẽ khó cho một số bạn ví dụ như mình nếu không có sự hướng dẫn của thầy hoặc cô. Sách được in đẹp và rõ nét, chữ không quá nhỏ, nhưng giấy trắng nên có thể bị mỏi mắt khi nhìn đề bài lâu.
5
53222
2015-09-18 01:11:20
--------------------------
281190
4469
Ngành mình học yêu cầu phải có chứng chỉ Hán ngữ HSK vào lúc ra trường nhưng thú thực mình không có thời gian để đi học ở trung tâm được.Lên mạng dò hỏi thì mình biết được cuốn sách này rất phù hợp cho những ai muốn tự ôn thi như mình. Cầm trên tay cuốn sách rồi là mình ưng ngay, sách có CD kèm theo rất tiện lợi.Ngày nào mình cũng tranh thủ làm từ 1 đến 2 bài.Sách có bìa cứng và bóng, bài test có kèm câu trả lời ở cuối sách.Mình canh giờ của một bài kiểm tra rồi ngồi làm, sau đó tự chấm điểm.Bây giờ mình đang đặt hàng để mua cuốn thứ 2.Rất hài lòng với cuốn sách này :]].
5
124154
2015-08-28 10:51:26
--------------------------
