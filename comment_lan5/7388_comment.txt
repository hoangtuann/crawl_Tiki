310066
7388
Trong tất cả các nhân vật thì Nobita quả đúng là nhân vật thú vị nhất. Nhiều nét tính cách, hậu đậu, lười biếng, nhút nhát, học hành thì lúc nào cũng kém cỏi. Thật khó kiếm được người thứ hai xui xẻo như thế. Chính vì vậy nên mới cần có người bạn như Doraemon bên cạnh, giúp đỡ cho cuộc sống gian nan ấy, để thu dọn tàn cuộc khi Nobita gây chuyện. Nhiều lúc ước gì có một người bạn tốt như thế ở bên cạnh. Đến cuối cùng, Nobita lại chính là người may mắn nhất.
5
698658
2015-09-19 11:59:12
--------------------------
240764
7388
Điểm 0 và Bỏ nhà ra đi đúng là một tập truyện riêng dành cho cậu bé siêu lười Nobita, ngày xưa khi đi học bị điểm kém mình đã ước có những bảo bối để trốn dưới lòng đất hay giấu bài kiểm tra như những bảo bối Doraemon cho Nobita mượn, nhất là bánh mì giúp trí nhớ để học bài thật nhanh thuộc và cây chung cư để trốn sau những giờ học thêm căng thẳng. Truyện đúng nghĩa đã vẽ nên một bức tranh của trẻ thơ, giúp phát huy tối đa trí tưởng tượng của nhi đồng một cách cao và xa nhất.
5
120276
2015-07-24 21:58:53
--------------------------
