487968
7166
Sách trình bày rất ổn, có ví dụ rồi hình minh họa rõ ràng nữa. Ai đang muốn bắt đầu học tiếng Trung thì nên học theo bộ sách này luôn. Bản thân mình thấy học sách này thật sự có hiệu lực quả. Chất liệu sách cũng tốt, sờ đã tay nữa.
5
408145
2016-10-29 00:51:29
--------------------------
449933
7166
Sách rất vừa vặn và in rất đẹp. Bìa nhìn vào rất bắt mắt nhìn là muốn mua liền trang giấy thì rất tốt khi luyện viết ghi rất đẹp. Nội dung trình bày rất logic và dễ hỉu. Bạn nên mua quyển này có CD nó sẽ chỉ cho ta học cách phát âm khi mới học tiếng trung phát âm rất quan trọng nếu bây giờ sai thì sau này sẽ khó sửa . CD thật tuyệt nó đọc rất dễ nghe không nhanh. Cảm ơn MCbook mình rất yên tâm khi mua sách của MCbook và rất hài lòng về tiki
5
671746
2016-06-18 10:49:08
--------------------------
447336
7166
ối với người vừa nhập môn , quyển sách này khá đầy đủ và rõ ràng , khiến ta nhanh chóng nắm bắt được quy luật của tiếng Hán . chất lượng giấy rất tôt , chữ in rõ ràng .Mình mua cùng bạn bộ quyển thượng và quyển hạ . có kèm cả thêm phần tiếng anh . Chữ in đẹp , rõ ràng , mực in rất tốt , mình học dùng tẩy nhiều cũng không mờ chữ .Từ vựng , đoạn hội thoại , dạy cách đọc , phát âm từng từ một , rất đầy đủ và chi tiết , những bạn nào có ý định học tiếng Trung thì nên sắm cho mình bộ giáo trình này ( gồm 6 quyển ) . Rất hữu dụng  
5
683638
2016-06-13 23:17:35
--------------------------
445820
7166
Trước tiên là phải nói là Tiki quá tuyệt luôn, giao hàng siêu nhanh, gói hàng cẩn thận, giá sản phẩm và giá ship còn rẻ nữa. 
Mình có la cà xem mấy clip tiếng Trung cho người tự học, thấy thầy bảo cần có quyển tập viết Hán ngữ và giáo trình Hán ngữ. Giờ mình đủ bộ rồi, bao giờ học xong quyển 1 này sẽ mua tiếp ở Tiki. Sách đẹp lắm, giấy dày, trình bày khoa học, có cả phiên âm, phần giải thích bằng tiếng Anh và tiếng Việt nên rất thuận tiện cho bạn nào muốn học kết hợp tiếng Anh với tiếng Trung. Sách có 15 bài, mỗi bài đều hướng dẫn kĩ, có phần bài tập và phần luyện viết để người đọc có thể ôn lại bài nữa. Mình chưa thử CD, nhưng thấy các bạn với thầy đánh giá cũng rất tốt
4
589740
2016-06-10 22:51:48
--------------------------
411370
7166
Tôi từng nhiều lần đặt hàng trên tiki và cũng khá an lòng về chất lượng nhưng lần này tôi rất không vui vì quyển sách này. Về nội dung quyển sách khá tốt, dễ hiểu, cần thiết cho ai tự học tiếng hán. Nhưng về hình thức thì bìa sách của quyển này trầy trụa rất nhiều. Thiết nghĩ các bạn đóng gói phải đứng trên lập trường người mua chứ. Chúng tôi cũng bỏ tiền ra mua mà. Nếu như tiki giảm giá vì lý do quyển sách này có vấn đề gì đó thì nên nói rõ để người mua chấp nhận hay không thì cả 2 đều vui vẻ. Nếu như tôi ở Sài Gòn thì tôi đã đổi trả quyển này rồi. Đằng này tôi đang ở quê, không tiện lấy hàng phải nhờ người khác nên việc đổi trả rất bất tiện. Tôi nghĩ tôi sẽ còn đặt hàng ở tiki nhiều lần nữa, mong rằng tiki sẽ tiếp thu ý kiến này của tôi. Kinh doanh phải dựa trên uy tín, lòng tin và chất lượng. Hy vọng tiki sẽ không làm mất lòng tin của tôi lần nào nữa.
1
728125
2016-04-05 22:03:14
--------------------------
394039
7166
Mình vô cùng hài lòng với cuốn một vỡ lòng của bộ giáo trình hán ngữ này của mc book vì nhiều lý do.
Thứ nhất, chữ in rất rõ, đậm nét nên rất dễ học cho những người mới học tiếng Hán. Thứ nhì, lượng kiến thức thì rất căn bản, chắc, nhẹ nhàng, không dồn dập nặng nề, giải thích ngữ pháp, giải nghĩa từ vựng cũng rất chi ly, để cho caen bản của mình rất vững. Thứ ba, sách còn có CD nữa, thu âm rất kỹ lưỡng, không tạp âm nên mình nghe rất dễ dàng.
4
624571
2016-03-09 20:45:41
--------------------------
381951
7166
Giáo trình rất hay, phù hợp với người mới bắt đầu học như mình. Hình vẽ minh họa cụ thể, đẹp và dễ hiểu. Bìa sách đẹp, bắt mắt, giấy cũng đẹp luôn. Phiên âm, Chữ Hán, nghĩa Anh, Việt đều có. Rất bổ ích, dễ học. Ngoài ra sách còn có CD đi kèm theo từng bài học, nghe rồi tập phát âm theo cũng rất tiện lợi mà dễ học. 
Giá cả cũng hợp lý, mình mua đúng dịp Tiki đang khuyến mãi nên giá được giảm đôi chút. Nói chung là rất ưng sách <3 Tính mua các tập tiếp trên Tiki để học.
5
741041
2016-02-18 12:53:59
--------------------------
349774
7166
Giáo trình bản mới này biên soạn khá đầy đủ. Từ các bài học cơ bản lên dần cho người bắt đầu học tiếng Hán.
Nó cũng giống như sách giáo khoa tiếng Việt lớp 1 vậy.
Có điều, tự học thì còn khó lắm. Phần ngữ âm ở 5 bài đầu, ghi kí hiệu phát âm nhưng lại không ghi phát âm trong tiếng Việt >< nên chẳng khác gì nhìn thấy nhưng không hiểu.
Túm chung muốn tự học thì phải mua thêm các cuốn dạy nghe nói, đọc viết nữa nhé.
Tốt hơn hết nên có giáo viên dạy kèm.
4
514579
2015-12-09 04:21:46
--------------------------
340206
7166
Đây là một quyển sách mà những bn mới bắt đầu học tiếng Trung nên mua. Bởi vì nó vừa dễ học có thể tự học mà k phải đến trung tâm nào, song có cả đĩa nghe nhưng giá cả tuong đối rẻ, sách in rõ ràng các điểm ngữ pháp giải thích khá rõ, những bức hình minh hoạ ở mỗi bài khá đẹp và dễ hiểu khi đưa vào ngữ cảnh. Ngoài nội dung của sách, thì phần thân sách được thiết kế đẹp và khá chắc chắn nên các bn k sợ bị rách hay sứt bìa . tóm lại đây là một cuốn sách rất tuyệt các bn ak!
5
860762
2015-11-19 13:41:19
--------------------------
308543
7166
Trình bày bìa sách thu hút, khiến cho người học có hứng thú. Sách được viết rất rõ ràng, chi tiết. Bày cách phát âm rõ kĩ lưỡng cho những ngữ âm, ví dụ như cách uốn lưỡi, cong môi, mở miệng. Bố cục trình bày rõ ràng, giúp cho người học dễ dàng tiếp thu và ôn tập. Ngôn ngữ gần gũi với người Việt, có luôn cả tiếng Anh giúp các bạn nào muốn tôi luyện thêm tiếng Anh. Các chủ đề trong sách gần gũi với đời sống hàng ngày, rất tiện cho những bạn nào chỉ muốn học tiếng Trung để giao tiếp. Theo mình, đây là sự khởi đầu tốt cho người tự học tiếng Trung ở nhà.
4
112336
2015-09-18 19:23:12
--------------------------
302824
7166
Giáo trình Hán ngữ tập 1(quyển thượng 1)
Đây là tập 1 cuốn 下 (hạ) của bộ giáo trình mới gồm 6 cuốn - 76 bài của tác giả Dương Ký Châu chủ biêu do NXB Đại học Ngôn ngữ Bắc Kinh phát hành. Bộ này dc tác giả bổ sung và đổi mới hơn so với bộ giáo trình của tác giả Trần Thị Thanh Liêm chủ biên 100 bài trước đây! Cuốn này gồm từ bài 1 đến bài 20, tổng cộng 157 trang. Sách được thiết kế theo format: bài khóa - từ vựng - ngữ pháp - bài tập và được trình bay bằng 3 thứ tiếng: Hoa - Anh - Việt. Khoảng 15 bài đầu chủ yếu xoáy vào cách đọc chữ thông qua phiên âm Pinyin và giúp người đọc quen dần với mặt chữ tiếng Hoa. Các bài khóa có thêm pinyin giúp người học đọc và hiểu dễ hơn. Từ mới được giới thiệu trong cuốn này hơn 500 từ, ở mức độ cơ bản, đơn giản và thông dụng nhất! 
5
595286
2015-09-15 14:48:05
--------------------------
297142
7166
cuốn sách này rất phù hợp với những ai bắt đầu học tiếng trung,có kèm đĩa CD để bạn để dàng tự học ở nhà trước khi lên trường.Bên trong nội dung còn có phần writing giúp người học dễ dàng luyện viết.Nội dung có cả nghĩa tiếng anh,tiếng việt phù hợp với mọi người.nói tóm lại sách này rất hữu ích cho những ai bắt đầu học tiếng hán.về phần in ấn thì mình nghĩ không cần phải nói gì thêm quá tuyệt vời,chữ rõ ràng và được in bằng giấy chất lượng không gây tổn hại đến mắt
5
803125
2015-09-11 16:40:48
--------------------------
266610
7166
Cuốn sách này khá rẻ rất phù hợp với lứa tuổi học sinh như mình sách kèm đĩa CD nên giá cả thế này khá ổn .Hình vẽ minh họa cho mỗi bài học trong tranh cũng khá đẹp và dễ hiểu .Đọc hết hai tập thì có thể biết sơ bộ về Hán ngữ rồi đó.Về nội dung thì mỗi từ mới có đủ Bình âm, Hán việt, tiếng Anh và cả giải nghĩa nữa. Phần ngữ pháp được giải thích một cách khá rõ nên giúp ta hiểu sâu về bài học hơn.Mình nghĩ những bạn mới học tiếng Trung thì nên mua quyển này có quyết tâm thì kết quả sẽ rất tốt
5
608628
2015-08-14 20:47:02
--------------------------
264760
7166
Quyển sách này rất bổ ích, lại rất phù hợp với người bắt đầu học chữ Hán. Mình chưa từng học qua các lớp dạy tiếng Hán mà mình tự học trên các sách mà Tiki có, thật sự không thể tưởng tượng, mình đã có thể biết hơn rất nhiều khi mua sách này về học. Rất dễ học, chữ in rõ ràng, dễ hiểu, bìa bóng chắc chắn và đẹp, giá cả hợp lí, các bạn mới tập học tiếng hán hãy cùng mua quyển này về, mình không hối hận vì đã bỏ tiền mua nó,rất rất bổ ích. 
4
621499
2015-08-13 13:50:12
--------------------------
