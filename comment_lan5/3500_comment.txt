212971
3500
Tôi đọc những lời tự sự về cuộc đời, về quê hương, về tình yêu của Thanh Hoa trước mỗi phần thơ, mà tim thắt lại. Có gì đó nghèn nghẹn, đồng cảm, day dứt và luyến tiếc, rồi lại lạc quan, vui tươi nhen nhóm trong tôi.
Tập thơ này khiến tôi cảm thấy 1 ngày dài mỏi mệt của mình trở nên dễ thở hơn, an yên hơn vô cùng. Tôi ấn tượng rất sâu đậm với bài thơ về cha của Hoa. Bài thơ khiến tôi tưởng tượng ngày bé mình được cha cõng trên vai, đuổi theo trăng đêm. Tôi cũng rất thích bài thơ về facebook và tình yêu. Ôi chao ơi là đúng, là thật.
Mặc dù có vài chỗ thơ hơi hẫng, như cách Hoa nói là "còn non", nhưng tôi vẫn cảm thấy tập thơ này bình dị và bình yên vô cùng. Cảm ơn Hoa, cảm ơn vì những nghị lực sống và nhãn quang nhìn đời của em.
5
4397
2015-06-23 09:13:18
--------------------------
