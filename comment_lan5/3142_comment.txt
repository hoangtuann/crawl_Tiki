469902
3142
Những câu chuyện với nội dung đơn giản, ngắn gọn, dễ hiểu. Mỗi câu chuyện truyền tải một thông điệp tế nhị, nhẹ nhàng, các câu chuyện có ý nghĩa. Truyện kể với nhân vật là các bạn động vật quen thuộc và rất đáng yêu như con chó, ếch, sư tử, cáo, cò, rùa, dê .... để chuyển tải những thông điệp, những lời nhắn gửi một cách nhẹ nhàng, gần gũi với trẻ nhỏ.
Cuôn truyện ngụ ngôn Ê Dốp này có hình minh họa trong sách đẹp, nội dung hay, xứng đáng được đánh giá bốn sao!

4
1124107
2016-07-06 22:26:57
--------------------------
464777
3142
Hồi bé lúc học tiểu học, thỉnh thoảng trong sách có mấy bài đọc thêm là những câu chuyện ngụ ngôn Ê - dốp. Lúc đó thấy thú vị với những mẩu chuyện đó lắm. Giờ tìm đc cả cuốn tổng hợp nhiều câu chuyện, tôi mừng lắm. Quyển sách có kích thước vừa tay, nên cầm gọn lắm. Sách in màu đẹp, chữ to rõ và ko sai chính tả. Còn có hình minh họa dễ thương nữa. Giấy cứng và thơm. ^^ khách quan mà nói với chất lượng thế này thì giá có đắt hơn tôi cũng muốn mua. Cảm ơn nhé!
5
430974
2016-06-30 14:42:02
--------------------------
362427
3142
Mình cực kỳ ưng cuốn này, với minh họa màu, in ấn tuyệt đẹp, những câu chuyện giản dị mà sâu sắc vô cùng, thích hợp làm những bài học đạo đức, hành xử đầu đời cho các bé. Cuốn sách này sẽ khiến các bé yêu thích việc đọc sách honwe, dễ dàng tiếp cận với nội dung. Hôm mình nhận hàng, mấy người đòi mua lại sau khi mình đọc xong nhưng nhất định không bán, giữ lại cho con để góp thêm vào gia tài. Giá bìa cuốn này thế là quá rẻ so với chất lượng
4
23764
2016-01-01 22:41:31
--------------------------
343020
3142
Con trai mình đang học lớp 2. Vừa nhận được sách là cu cậu đọc ngay 1 lúc phải hết nửa cuốn. Hình minh họa trong sách đẹp, nội dung hay. Con mình thích chuyện người da đen và người da trắng, khen chuyện đó hay, cứ bắt mình phải đọc chuyện đó. Hình đọc cho con vừa lòng, nhưng mình thấy chuyện người da đen và người da trắng không hay bằng chuyện con cáo và con sếu. Mình đọc cho bé 3 tuổi nghe, mặc dù bé chưa hiểu nội dung hàm ý của các câu chuyện nhưng nhân vật chính của chuyện là các bạn động vật đáng yêu nên bé rất thích và chăm chú lắng  nghe.
4
834230
2015-11-25 16:47:10
--------------------------
338964
3142
Sách đẹp, tranh minh hoạ đẹp nhưng nhiều chữ hơn tranh nên phù hợp với bé từ 3 tuổi trở lên. Các câu chuyện dài vừa phải nên bé nhà mình có thể nghe được hết câu chuyện. Hôm qua mình vừa nhận được hàng của Tiki, đọc ngay cho bé 1 truyện và bé rất thích. Đọc truyện ngụ ngôn rất thú vị, vừa rút ra bài học vừa thấy cách xử lý tình huống của nhân vật rất thông minh. Mình nghĩ các mẹ có thể mua quyển này về đọc cho con, vừa hay vừa dạy con cách hành xử thông minh. 
4
703221
2015-11-17 10:14:49
--------------------------
