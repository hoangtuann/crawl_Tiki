387482
3218
Mình mua cuốn sách vì có mấy nhận xét là phù hợp với trẻ em nhưng đến khi mua về thì mình hơi thất vọng. Hình thức cuốn sách to đẹp, giấy dày minh hoạ  bên trong đều là minh hoạ màu, nhìn rất rõ nét. Tuy nhiên nội dung thì mình nghĩ nhà xuất bản nên xem xét và chỉnh sửa lại. Nội dung các câu chuyện bị cắt xén không thương tiếc, mình đọc và thấy câu chuyện cụt ngủn chứ không như nguyên bản mình được đọc trước kia. Trong cuốn này có chuyện "bé gái nhỏ bán diêm" ngay đến cả nhan đề của chuyện mình nghe cũng không thấy thích rồi, nội dung thì chỉ vỏn vẹn mấy chục dòng là hết. Cuốn sách này chỉ đáng 2 sao thôi ạ.
2
373267
2016-02-27 06:34:02
--------------------------
328751
3218
Cách kể chuyện được lược một cách đơn giản, kèm thoe hình ảnh minh hoạ đầy màu sắc, vô cùng phù hợp với trẻ khoảng mẫu giáo lên cấp một. Cháu mình còn nhỏ nhưng thích nhìn màu sắc, mình có thể đọc cho nó nghe và chỉ cho nó hình này nọ, rất thú vị.
Mình là người lớn nhìn vào còn thấy thích nữa là. Tậu ngay cho cháu một cuốn thì đây thật sự là lựa chọn hàng đầu luôn.
Tuy nhiên, số lượng tuyện hơi ít, nên phải tìm kiếm thêm một số truyện khác nữa. Nếu có một cuống đủ bộ luôn thì thích quá!
4
38943
2015-10-30 15:11:46
--------------------------
325883
3218
lúc mới nhìn cuốn sách này rất màu mè nên mình nghĩ phù hợp cho các bé đang trong quá trình tập đọc và viết,đánh vần câu chữ.ưu điểm của cuốn này là màu 100%. Do là sách của nhà xuất bản mỹ thuật nên được đầu tư các khoản minh họa cho từng trang truyện.Tuy nhiên hơi đáng tiếc là số lượng bài thì lại quá ít nên khi nhận được thì đọc rất nhanh dẫn đến nhanh chán.khổ sách cũng quá to nên cầm khá vướng tay.Về các bài trong truyện cổ thì được chọn lựa từ các câu chuyện cổ tích cổ điển và khá quen thuộc.
3
557814
2015-10-24 14:43:52
--------------------------
