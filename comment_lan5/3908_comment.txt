477038
3908
Mình biết đến cuốn sách này qua bộ phim hoạt hình cùng tên của Ghibli Studio. Vì quá thích thú với thế giới tí hon của Arriety mà mình đã tìm mua cuốn sách này và quả thực không phí một chút nào cả. Với lối dẫn chuyện lôi cuốn và đầy bí ẩn, Mary Norton đã đưa người đọc đến gần hơn với thế giới nhỏ bé mà đầy màu sắc đó cũng như đã thuyết phục được cả trẻ em lẫn người lớn rằng 'người vay mượn' là có thật. Cùng với Arriety, bà đã chinh phục, lấp đầy, thỏa mãn bao tâm hồn đọc giả; chứng minh được rằng trí tưởng tượng của con người là vô hạn và đã thể hiện sức sống mạnh mẽ trong suốt hơn nửa thế kỉ qua.
4
256164
2016-07-24 13:04:05
--------------------------
476537
3908
Đây là một cuốn sách đầy phi thường và bất ngờ. Nó cho ta thấy rằng trí tưởng tượng của con người là không có giới hạn. Khác với bộ phim cùng tên của Ghibli, cuốn chuyện không cuốn ta một lèo theo những diễn biến nhanh mà đưa ta từ từ trang này qua trang khác giúp ta hiểu sâu hơn về cuộc sống của những người tí hon, về lịch sử gia đình, về họ hàng, về cách họ làm việc và kiếm sống. Câu chuyện đem đến cho ta một góc nhìn hoàn toàn mới, cho ta tưởng tượng, được nhìn ra những đồ vật dụng cụ những món ăn của họ thường ngày. Song về bản dịch, có đôi chỗ khiến người đọc cảm thấy không hài lòng, mong rằng những lần tái bản tiếp sẽ đem đến cảm tình nhiều hơn cho độc giả!
4
706780
2016-07-21 11:02:48
--------------------------
472174
3908
Tôi biết đến cuốn sách này qua lời giới thiệu của một người chị và đã tìm đọc thử. Và tôi đã hết sức yêu thích tác phẩm này, yêu thích từng nhân vật, từng câu chuyện. Tác giả đã tạo nên một thế giới hết sức xinh đẹp và sống động khiến cho câu chuyện về người tí hon vốn ở trong giấc mơ của mọi đứa trẻ nay trở nên chân thật và gần gũi hơn bao giờ hết. "Những người vay mượn tí hon" đã đem đến cho tôi những hoài niệm, những mơ mộng của thời thơ ấu, những cảm xúc trẻ con mà tôi không nghĩ sẽ được một lần cảm nhận lại. Ngoài ra, phiên bản anime cũng rất hay, nhạc phim rất tuyệt vời. Những bạn có khả năng/sở thích nghe nhạc khi đọc sách giống tôi có thể nghe những bài nhạc phim khi đọc "Những người vay mượn tí hon", cảm giác rất tuyệt.
  Một cuốn sách đáng đọc!^^
5
699407
2016-07-09 11:22:33
--------------------------
468327
3908
Em coi anime của Ghibli trước khi mua cuốn này về, và khi cuốn này được giao về thì bị tróc mất 1 góc sách nên hơi buồn một chút. Em rất ấn tượng vì anime rất hay, nhưng khi em đọc cuốn này thì em lại không có hứng thú bằng xem anime, 1 phần là phong cách dịch hơi thô, không lột tả được cái chất huyền bí trong câu chuyện, và 1 phần là do anime làm em áp đặt suy nghĩ rằng cuốn sách hao hao như anime mà không phải nên khiến em hơi mất hứng, nhưng dù sao thì anime cũng dựa lên từ tác phẩm này nên sẽ cố mở rộng suy nghĩ hơn để dễ đón nhận tác phẩm.
3
84723
2016-07-04 22:15:19
--------------------------
461227
3908
Ngày bé mình đã từng đọc sách về thế giới tí hon. Họ ở ngay bên cạnh chúng ta, trong vòm cây, trên mái nhà, dưới gốc cây,... họ không muốn gặp chúng ta, mà chỉ yên lặng theo dõi chúng ta sống ra sao. Đọc cuốn sách này, cảm giác rất thú vị vì được hiểu rõ hơn họ đang nghĩ gì, đang là gì và sống ra sao. Hơi buồn cười một tẹo nhưng sau khi đọc cuốn sách này xong, đôi khi có vài món đồ tìm không thấy, mình sẽ nghĩ ngay đến việc người tí hon đã mượn tạm của mình và và hôm sau họ sẽ trả lại mình ^^
4
455656
2016-06-27 17:20:21
--------------------------
459619
3908
Một cuốn sách mà mình khá đắn đo khi mua.Khi nhận hàng rồi mình rất vui vì sách được bọc rất cẩn thận và nhất là thiết kế bìa rất đẹp mắt,mình thực sự hài lòng với hình thức.Tuy nhiên thì mình hơi thất vọng chút xíu ở nội dung.Mình có biết là cuốn sách được chuyển thể thành phim hoạt hình rồi nhưng mình định đọc sau mới xem.Cuốn sách có nội dung rất hay,một thế giới về người vay mượn thật sống động và lạ lẫm.Tuy nhiên cách hành văn thì hơi chán.Chắc tại lỗi dịch.Phần đầu miêu tả quá là rườm rà,mình đọc mãi mới hình dung ra một chút.Tiếp theo thì cũng khá ổn nhưng nói chung chưa thực sự chưa làm mình cảm thấy thích thú.và phần kết thì khó hiểu và không rõ ràng.Chuyển sang xem phim thì hay hơn.Dù gì thì cuốn sách cũng không đến nỗi.Nếu bạn xem phim trước chắc sẽ thích thú khi đọc sách hơn.
3
395480
2016-06-26 12:01:06
--------------------------
458662
3908
Hình như các tác phẩm chuyển thể từ phim dài hay ngược lại (đặc biệt là của Nhật) thì phim luôn để lại ấn tượng sâu sắc hơn trong lòng người đọc thì phải.
Thứ 1, mình thấy chất lượng giấy ổn, bìa thì rất đẹp (khỏi chê luôn) nhưng cau cú có phần gì đó thiếu tính tự nhiên thì phải
Thứ 2, hình tượng nhân vật không đậm nét, hình ảnh cô bé tý hon Arriety thì khá ổn (lanh lợi, hoạt bát, có phần thích khám phá,vv...) nhưng hình tượng Sho có phần gượng gạo từ đó không bộc lộ hết nét đẹp của cậu bé này
Thứ 3, Tuy vậy hình tượng khu vườn trong sách lại rất đẹp mang tính cổ kính phương Tây...
nói chung là một cuốn sách khá là hay đặc biệt cho thiếu nhi
4
678752
2016-06-25 13:06:22
--------------------------
456400
3908
Đầu tiên phải khen về hình thức của cuốn sách, bìa sách được trình bày rất bắt mắt, gây ấn tượng ngay khi nhìn, rất phù hợp với trẻ con. 
Câu chuyện này đã được chuyển thành phim hoạt hình rất hay, nên khi đọc cuốn sách này có vẻ mình thấy không được hấp dẫn bằng, kết thúc hơi hụt hẫng, chưa miêu tả được kĩ tâm lí của nhân vật!
nhưng nói chung vẫn khá phù hợp với tâm hồn của trẻ con, điểm trừ là giá khá cao với một cuốn sách nhỏ và mỏng như thế này!
4
917875
2016-06-23 14:00:46
--------------------------
455848
3908
Ai cũng đã từng có một tuổi thơ với những mơ ước, những suy nghĩ về những hình ảnh, những cảnh tượng đẹp như mơ về người tí hon, về những cô nàng phù thủy, về bà tiên,.... Ai mà nghĩ rằng những người tí hon sẽ sống ở một nơi nào đó cạnh chúng ta và làm cuộc sống thêm muôn màu muôn vẽ. Sách thích hợp dành cho lứa tuổi thiếu nhi, hình ảnh đẹp bắt mắt, nội dung câu chuyện vui nhộn, dễ thương, đầy màu sắc, có đôi lúc khiến ta xúc động khi đọc, khi ngẫm nghĩ lại thì nó mang nhiều ý nghĩa khác nhau. 
4
416217
2016-06-22 21:48:46
--------------------------
452941
3908
Cuốn truyện "Những người vay mượn tí hon " này vô cùng hay, đáng yêu. Quyển truyện có bìa rất là đẹp, bắt mắt. Mình rất là ấn tượng với quyển truyện này. Nó viết về một gia đình người tí hon rất là đáng yêu, những câu chuyện xoay quanh gia đình họ và việc đi mượn. Cô bé Arietty tí hon rất là đáng yêu, mình rất thích nhân vật này. Câu chuyện không chỉ đơn giản là những con chữ mà nó còn đòi hỏi ở người đọc một trí tưởng tượng, nó gợi mở trong tâm trí người đọc một thế giới vô cùng lạ kỳ, hấp dẫn. Đây đúng là một tác phẩm kinh điển, đáng để đọc.
5
1351512
2016-06-20 22:00:35
--------------------------
452713
3908
Cuốn sách này mình mua tặng bạn nên không nhận xét về nội dung được, nhưng theo bạn mình thì cuốn sách này khá hay, cốt truyện lành mạch, bìa rất đẹp, mình đặt hàng được 3 hôm là đã nhận được cuốn sách, tiki giao hàng rất nhanh, Giao tới tận nhà mình dù không hề gọi hỏi đường đi như thế nào luôn, đang ngồi ở nhà nhân viên tới trước cổng luôn nên rất hài lòng với dịch vụ này, mua hàng nhiều trang trên mạng đơn hàng bị chia lẻ ra khi giao... Giao lâu mà vẫn tốn từ 5-10k tiền ship mà không giao được tận nhà nữa, mình sẽ là khách hàng lâu năm của tiki.
4
729595
2016-06-20 19:53:48
--------------------------
449893
3908
Cuốn truyện đã được chuyển thể thành phim điện ảnh và phim hoạt hình nhưng hiển nhiên là mình chọn xem bản hoạt hình của Nhật Bản do Ghibli sản xuất. Phải nói là sau khi xem hoạt hình xong rồi đọc truyện có rất nhiều cảm xúc, tuy đã biết trước các tình tiết nhưng văn phong của tác giả vẫn rất cuốn hút mình. Hình ảnh cô bé Arrietty thực sự rất đáng yêu, lại dũng cảm, gan dạ. Hình ảnh những con người tí hon không còn chốn dung thân cũng khiến chúng ta phả ngẫm nghĩ nhiều điều.
5
1420970
2016-06-18 09:52:12
--------------------------
435119
3908
Mình là fan ruột của Ghibli, phim nào cũng xem hết rồi. Thế giới của Arriety làm mình nhớ đến lúc còn nhỏ hay tưởng tượng ra người tí hon với đồ vật dễ cưng :)) 
Vô tình thấy quyển này khi đang dạo trên tiki, lúc đó còn ngờ ngợ không biết phải truyện mà Ghibli đã làm phim hay không. Đọc tóm tắt thì thôi, ôi xong, bấm mua luôn. Chính là nó! :))
Truyện lúc nào cũng dễ thương, bìa thiết kế đẹp quá đẹp, nhìn thấy thoải mái hẳn, chất lượng tốt, nhỏ gọn nhưng chất lượng (y)
5
418019
2016-05-24 14:20:19
--------------------------
434522
3908
Hình thức cuốn truyện rất đẹp. Lần đầu nhìn thấy bìa sách là mình đã muốn đọc vì có khung cảnh thiên nhiên, cây cối bao quanh các nhân vật ở chính giữa. Bìa sách còn gợi cảm giác như 1 câu chuyện cổ tích vậy.

Nội dung thì không được như mong đợi lắm. Có lẽ mình kì vọng nhiều chi tiết mô tả sự tương tác và gắn bó giữa Arriety và cậu bé hơn, giống như trong hoạt hình của Ghibli. Đọc khá hụt hẫng vì chuyện ngắn quá.

Dù sao cũng là một cuốn sách đáng đọc!
4
869250
2016-05-23 12:03:13
--------------------------
432624
3908
Bìa sách của Những Người Vay Mượn Tí Hon rất đẹp và tươi trẻ, phần nội dung của sách thì khiến mình rất hứng thú nhưng sau khi đọc mình cảm thấy không ấn tượng cho lắm. Tình tiết trong truyện khá ít, đơn giản có lẽ vì độ dài của sách khá ngắn, truyện không có các câu chuyện phiêu lưu thú vị và cái kết chưa được trọn vẹn cho lắm. Tóm lại mình cảm thấy khá bất ngờ trước tiếng tăm mà cuốn sách này có được vì thế chỉ đánh giá sách ở mức độ trung bình.
3
934783
2016-05-19 14:13:23
--------------------------
431799
3908
Ban đầu, mình xem phim hoạt hình của Ghibli, đó là một trong những bộ phim mình yêu thích nhất của studio huyền thoại này. Sau đó, mình bắt gặp trên tiki và quyết định mua thử xem sao. Quả nhiên không phụ lòng mong đợi. Những thứ tí hon, dễ thương khiến con gái thích mê ấy, giờ đã sống thật sự trong những trang văn của Mary Norton. Cây kim trở thành vũ khí, chiếc cặp tóc chính là chiếc kẹp phơi quần áo. Cơ mà, thú thật mình thấy phim hoạt hình của Ghibli hay hơn, sống động hơn và diễn tả nhân vật cũng như tình bạn giữa họ cũng đầy đủ hơn bản dịch nữa. 
4
385242
2016-05-18 01:18:28
--------------------------
427512
3908
Mình đặc biệt thích đọc sách viết cho thiếu nhi, cuốn sách cũng có thể được xếp vào một trong số những cuốn sách viết cho thiếu nhi mà mình yêu thích nhất !
Mình từng đọc nhiều cuốn nhưng với cuốn sách này, thú thật lúc đầu đọc hơi chán nhưng càng đọc càng thấy dễ thương về việc những người tí học đã bị " nhìn thấy", trong sách có nhiều đoạn mô tả rất dễ thương về những đồ dùng trong nhà ông Pod như một thế giới trong mơ. Mình thấy bất cứ ai cũng sẽ đem lòng yêu mến 1 câu chuyện dễ thương nhưng ngộ nghĩnh như thế này !
4
90063
2016-05-09 14:19:37
--------------------------
418694
3908
mình đã xem phim hoạt hình Người vay mượn của Ghibli và vô cùng thích bộ phim này, nên đã tìm kiếm và tậu quyên truyện này về. so với phim thì truyện chưa toát hết được cái thần của phim, mang nhiều ý của người dịch hơn. hơi thất vọng vì mình rất trông chờ vào quyển truyện. có lẽ do ảnh hưởng của bộ phim quá lớn khiến cho bản dịch truyện chưa làm vừa lòng người đọc :)
Dù vậy nhưng cũng rất đáng mua để làm kỉ niệm hi.
bìa đẹp, giấy nhẹ tênh, thơm nữa ^^
4
181651
2016-04-19 19:33:00
--------------------------
414953
3908
Mình mua cuốn sách này vì thấy tựa rất hay và mình cũng thích những câu chuyện có một chút tưởng tượng. Tuy nhiên, khi đọc vào mới thấy nó không có chút gì là thần tiên hay bất cứ thứ gì tương tự như thế. Đó là một câu chuyện rất thực tế về một gia đình tí hon sống trong một xã hội toàn những con người to lớn. Nó cho thấy sự khó khăn vất vả của họ cũng như niềm vui nỗi buồn. Tác giả khắc họa rất sâu sắc sự tò mò của cô bé tí hon và những gì cô đã gây ra. Truyện có kết thúc mở nên mình không thích cho lắm nên chỉ được 4 sao. Bìa truyện thì khỏi phải chê. Mình khuyên các bạn nên đọc câu chuyện này vì nó có rất nhiều bài học bổ ích. Chúc các bạn đọc truyện vui vẻ.
4
1236771
2016-04-12 19:56:09
--------------------------
414694
3908
Mình mua quyển này từ hồi sau Tết, Tiki giao hàng rất cẩn thận rồi nhưng hơi tiếc là truyện có bị xước ở bìa ngoài một chút :( Còn về truyện thì đọc khá hay. Câu chuyện về thế giới của người vay mượn, khiến mình đọc thích thú dã man và đôi lúc còn thấy nó vô cùng vi diệu :v Arriety bé nhỏ cùng với ông Pod và bà Homily trong 1 gia đình cùng 1 cậu bé đã tạo nên câu chuyện :)) Đôi lúc cũng khá li kỳ hồi hộp. Nói chung là một cuốn truyện rất đáng đọc. Truyện này cũng được làm thành phim nữa cũng hay lắm đó :))
5
1161604
2016-04-12 12:18:15
--------------------------
407124
3908
Truyện kể về cuộc sống của gia đình người tí hon.
Cái đặc sắc ở đây là thường người tí hon thì mọi thứ từ áo quần, đồ vật,nhà cửa ..cũng giống như của thế giới chúng ta nhưng thu nhỏ lại; nhưng ở truyện này thì họ "vay mượn" của thế giới người "khổng lồ" chúng ta để tận dụng nó cho cuộc sống của họ. 
Nên khi đọc truyện kích thích trí tưởng tượng rất nhiều : họ "mượn" củ khoai tây và chỉ cắt một lát để nấu cho một bữa, họ "mượn" 1 nửa kéo cắt mòng tay để dùng làm dao, họ "mượn" hộp diêm để làm phòng ngủ,...Phải nói là tác giả kể về cách người tí hon tận dụng mọi thứ rât tuyệt vời và linh hoạt
Truyện khá cuốn hút, có hồi hộp, gay cấn, nhưng văn phong hơi dài dòng nên phải đọc chậm rãi thì mới hiểu hết ý tác giả truyền tải.
3
429557
2016-03-29 15:03:39
--------------------------
404760
3908
"Arrietty", một tuyệt tác do bàn tay chuyên nghiệp của Studio Ghibli uốn nắn nên, chính là dựa trên câu chuyện kinh điển này! Mary Norton đã giúp độc giả phiêu du trong một thế giới rộng lớn & khác lạ, dưới cái nhìn & lối sống của những "người vay mượn tí hon". Tất cả được miêu tả một cách vừa ma mị, vừa sống động và tinh tế, mới mẻ. Cuộc đời của những kẻ nhỏ bé, sống bằng những đồ vay mượn từ con người cứ diễn ra trong niềm lo sợ, hồi hộp. Ông Pod, được coi là người vay mượn giỏi nhất & là trụ cột duy nhất giúp cho gia đình ông duy trì được qua ngày. Bà Homily, một người vợ & người mẹ kì lạ, rối rắm, khó hiểu như chính mái tóc của mình. Cho đến khi một sự thay đổi ập đến, từ chính quyết định táo bạo & liều lĩnh của cô con gái Arrietty tinh nghịch, lém lỉnh, sau khi bị "nhìn thấy" trong lần đầu đi vay mượn đầu tiên: cô đã nói chuyện với một cậu bé người. Rồi sau đó, những con người khác sinh sống trong ngôi nhà ấy cũng dần tiến gần vào cuộc sống của gia đình tí hon. Những diễn biến tiếp theo, hứa hẹn vô cùng li kì & đáng theo dõi, đang chờ đợi những cá nhân yêu thích sự KINH ĐIỂN! Hãy chọn quyển sách này :)
5
440475
2016-03-25 18:11:27
--------------------------
401283
3908
Mình biết tác phẩm này qua bộ phim THẾ GIỚI BÍ ẨN CỦA ARRIETTY của Studio Ghibli. Một Arrietty dúng cảm , đúng mẫu người mình thích. 
Bìa sách thiết kế rất đẹp , màu sắc hài hòa , chất lượng giấy cũng khá tốt 
Về phần dịch thì không được tốt cho lắm , một số câu lủng củng , nó khiến cho mình hơi nản , không muốn đọc tiếp.
Nhưng theo mình thì có lẽ bản hoạt hình hay hơn , vì ở phim , Arrietty và Sho có tình cảm với nhau , rất dễ thương , và phần hình ảnh ở phim cũng rất đẹp.
5
791798
2016-03-20 14:01:03
--------------------------
384912
3908
Thực ra mình đã xem phim này từ hồi còn học cấp 1 hay cấp 2 gì đấy, và đã vô cùng thích nó.

Hôm bữa có xem cái list sách của nhỏ bạn và thấy cuốn này. Ngẫm ra mới biết là cuốn này chuyển thể từ một cuốn truyện nổi tiếng và quyết định mua.

Nội dung thực sự bất ngờ. Quả đúng là sách viết cho những tâm hồn trẻ nhỏ. Ngay từ đầu cái thực và cái không thực đã xen lẫn vào nhau. Ẩn sâu dưới sự náo nhiệt của nơi chúng ta đang ở ấy lại hóa ra có một cuộc sống khác. Khi đọc, mặc dù đây là tác phẩm của tưởng tượng nhưng sự chân thực lại hiện rõ. Cũng có sự thắt mở câu chuyện ở nhiều đoạn nhưng thật sự rất hay và lôi cuốn...

Hãy thử tưởng tượng vào một ngày uể oải và thất vọng, ta lại thấy một thế giớ đang ẩn kế bên mình.

Tuyệt. Tuyệt lắm !!!!
5
119567
2016-02-23 10:34:18
--------------------------
378997
3908
 biết cuốn sách này qua một lần lang thang trên hiệu sách ở thành phố-nơi cách xa nhà tớ hàng cây số. Phim hoạt hình thì đã xem qua rồi, không những xem mà còn xem đi xem lại nhiều lần nữa, rất thích các nhân vật trong bộ phim. Vốn nghĩ chỉ có phim hoạt hình thôi, không ngờ lại tình cờ bắt gặp ngay được sách, tớ đã rất ngạc nhiên và thích thú Bìa sách rất đẹp, màu xanh mát tươi sáng tạo cảm giác rất vui vẻ khi nhìn, có những cái bóng nho nhỏ ẩn dưới tán cỏ cây tạo cảm giác rất tò mò, thú vị. Sách cầm hơi nhẹ tay, kích thước còn bé nữa, rất đáng yêu, tiếc là tớ chưa có cơ hội được xem qua bên trong nên cũng chẳng biết văn phong thế nào, nghe bảo rất thú vị nhỉ. Sẽ sớm thôi, tớ mang nó về và tận hưởng lại khởi đầu câu chuyện cổ tích về những người tí hon đáng yêu này ngay ấy mà.
2
879184
2016-02-08 21:23:30
--------------------------
352001
3908
Mới nghiền hết cuốn truyện tối qua nên giờ cảm xúc trào dâng phải viết ngay cảm nhận. =))))) Gia đình tí hon đó thật đáng yêu. Mọi thứ trong gia đình đó giống như trong cổ tích vậy. Và cậu chàng đáng yêu giúp đỡ Arrietty và gia đình cô cũng vậy, một cậu bé dễ thương tràn đầy tình cảm. Còn gia đình tí hon. Ông bố điềm đạn, cẩn thận, đáng kính,.. Bà mẹ hay nói nhưng ẩn sâu làm tâm hồn nhạy cảm yêu thương gia đinh. Arrietty và mọi thứ nói cô, nơi câu chuyên và nhưng Người Vay Mượn đã cuốn hút tôi theo từng câu chữ. Nhất định phải mua nó lần nữa để tặng cho con bạn thân mới được
5
746405
2015-12-13 13:30:34
--------------------------
341347
3908
Từ xưa đến nay, những câu chuyện về người tí hon luôn hấp dẫn mỗi đứa trẻ bởi nó khơi gợi trí tưởng tượng, nỗi tò mò về những con người khác với chúng ta. Họ bé nhỏ, yếu ớt nhưng vô cùng đáng yêu và kiên cường. Truyện "Những người vay mượn tí hon" cũng vậy, câu chuyện xây dựng một gia đình của những con người nhỏ bé, họ sống bằng cách luôn vay mượn mọi thứ từ con người nhưng lại có lòng tự trọng rất cao. Giữa họ luôn có những tình cảm thiêng liêng vô cùng đáng quý đã làm tôi cảm động. Khép cuốn truyện lại mà tôi vô cùng luyến tiếc vì tôi vẫn còn muốn tham quan, tìm hiểu nhiều hơn nữa về cuộc sống của họ! :))
5
641117
2015-11-21 21:46:55
--------------------------
334752
3908
Theo mình thì nôi dung sách hay, cốt truyện độc đáo, thú vị nhưng phải đồng ý với các bạn nhận xét trước là truyện chưa gây được ấn tượng bằng bộ phim của Ghibli. Bản thân cũng là một fan của Ghibli nên mình đã tìm mua quyển này, dù cho diễn biến trong câu chuyện biến động rất nhịp nhàng với nhiều sự miêu tả gần gũi, nhưng so sánh với phiên bản của phim thì vẫn có nhiều điểm không thể bằng. Nhưng dù sao mình cũng rất thích ý tưởng câu chuyện của tác giả và lối hành văn của bà, những nhân vật bà khắc họa, đáng yêu vô cùng. 
(mua từ lâu mà cho bọn bạn mượn giờ mới tới lượt mình đọc, đứa nào cũng cứ giữ lâu lâu để vừa đọc vừa mở phim so sánh^3^)
4
662480
2015-11-09 23:27:57
--------------------------
330842
3908
Truyện đọc bình thường. Nửa đầu cuốn không hấp dẫn. Hình như sách thiếu nhi Nhã Nam xuất bản gần đây ít cuốn đặc sắc, mặc dù "Những người vay mượn tí hon" được liệt vào danh mục sách kinh điển. Đề tài về người tí hon thì nhiều mà tác phẩm này chưa tạo được điểm nhấn thú vị, chưa có những tình tiết gay cấn. Kể từ đoạn Những Người Vay Mượn gặp cậu bé cũng có thu hút hơn song nhìn chung truyện chỉ ở mức trung bình. Hơn nữa văn dịch sử dụng quá nhiều chấm phẩy nên đọc vừa rối vừa không ưa nhìn.
3
386342
2015-11-03 13:29:42
--------------------------
321344
3908
Mình mua cuốn sách này là do đã xem phim hoạt hình của Studio Ghibli. Do vậy, mình muốn thử phiên bản gốc thì sẽ như thế nào.
Tuy nhiên, có lẽ một phần do bản dịch không được tốt lắm hoặc là vì mình đã quá ấn tượng khi xem phim hoạt hình rồi nên khi đọc có hơi hụt hẫng chăng? 
Tuy nhiên, về nội dung thì đây cũng là một cuốn sách nên đọc vì nó giúp chúng ta khơi gợi trí tưởng tượng. Về hình thức thì cuốn sách có bìa minh họa đẹp, chất lượng giấy ổn.
3
723262
2015-10-13 18:31:40
--------------------------
316746
3908
Tôi rất thích tí hon. Tuổi thơ của tôi gắn liền với những mảng tưởng tượng đầy màu sắc về thế giới tí hon, những ngôi nhà bằng nấm, những chiếc dù bằng cánh hoa, chiếc tách trà có thể trở thành bồn tắm,... Đó là một thế giới yên bình, ảo diệu và khiến tim reo leng keng sung sướng. Đọc "Những người vay mượn tí hon" của Mary Norton, thế giới đó sống lại, và sinh động hơn muôn phần. Đó là lý do tôi cảm tình với cuốn sách.

Một điểm ưng ý nữa là kiểu nữ chính lanh lẹ, khôn ngoan, ham thích khám phá như Arriety là mẫu tôi khá thích.

Bìa sách cũng đẹp, hình minh họa đặc chất Tây cổ.

Nhưng, lý do mình cho 3 sao là vì phần văn phong. Xếp bên hàng truyện thiếu nhi yêu thích, cuốn sách này có phần xuề xòa trong cách viết. Câu cú hơi lủng củng. Cũng có thể đó là lỗi của biên tập/ người dịch chăng? Quả thật là đọc không mượt và nhiều khi phải nhăn trán.

Một phần nữa là khi mang truyện ra đối chiếu với bộ phim hoạt hình phóng tác cùng tên (chuyển sang bối cảnh Nhật Bản) thì thú thật tôi thích phim hơn nhiều. Rất khó để có thể yêu thích phim hơn truyện chữ, vì thông thường thế giới truyện chữ giải phóng trí tưởng tượng cao hơn, nhiều màu sắc hơn. Nhưng với tác phẩm này thì phim hoạt hình cực kỳ ấn tượng, cũng mang cái kết tạo ấn tượng nhân văn và tươi sáng hơn.

3
606417
2015-10-01 17:22:01
--------------------------
313403
3908
Đây là một câu chuyện về người tí hon khá thú vị. Nó khiến mình thực sự tò mò không biết những người tí hon có tồn tại thực sự hay không. Cách họ ăn, uống, sinh sống, "vay mượn" những thứ be bé xinh xinh từ thế giới con người khiến mình rất thích thú. Tuy nhiên, cách kể chuyện chưa thực sự hấp dẫn, mạch câu chuyện hơi dài dòng, lê thê, và một số chi tiết về cô bé Egletina, bác Luppy khiến câu chuyện thật đau buồn. Đặc biệt, mình mong muốn một cái kết có hậu, rõ ràng hơn về số phận nhà cô bé Arrietty, thế mới là một câu chuyện thiếu nhi hay.
3
198939
2015-09-23 15:55:12
--------------------------
297857
3908
Mình quyết định mua Những người vay mượn tí hon của tác giả Mary Norton vì muốn biết tác phẩm tuyệt đến thế nào mà nhờ nó đạo diễn Hiromasa Yonebayashi của Ghibli studio dựng nên bộ phim hoạt hình The Secret World of Arrietty hay đến vậy. Mọi người đã đọc truyện nên tìm ngay phim để xem. Vậy mới thấy Thế giới vẫn luôn ẩn chứa những bí ẩn tuyệt vời. Biết đâu những người tí hon sống ngay gần bên mà ta không hề biết.
Mình thích minh họa bìa này hơn cái bìa cũ. Cuốn sách mỏng thôi nhưng chứa rất nhiều điều tuyệt vời.
4
139133
2015-09-12 10:49:55
--------------------------
292150
3908
Có thể nói đây là câu chuyện kể lại của một người bà cho đứa cháu về một gia đình có thể nói là còn lại duy nhất của những người tí hon. Họ là những người vay mượn và được đặt tên theo nơi mà họ đang trú ẩn. Một gia đình với ông bố đã bắt đầu mệt mõi với nghề vay mượn, một bà mẹ luôn ham những đồ có giá trị và cô con gái nhỏ thích khám phá thế giới bên trên sàn nhà. Câu chuyện cũng không được hấp dẫn lắm, chỉ tập trung chủ yếu vào việc miêu tả gia đình tí hon, đến lúc gây cấn nhất thì cũng là là được kể hời hợt nhất. Tuy nhiên kết thúc truyện có đôi chút bất ngờ và cảm thấy không biết mình đọc xong cuốn truyện đâu là thực và đâu là ảo.
3
53222
2015-09-07 03:08:01
--------------------------
273754
3908
Từ hồi còn bé, mình đã rất thích những ngôi nhà dành cho búp bê bé bé xinh xinh và thường hay tưởng tượng có những người tí hon cũng trốn trong những ngôi nhà này mà mình không biết. Thế rồi trong một dịp tình cờ, mình được xem bộ phim hoạt hình của studio Ghibli và sau đó mình cũng tìm đọc luôn quyển sách "Những người vay mượn tí hon" này... Tuy nội dung hơi khác chút ít, nhưng đều xoay quanh về chuyến "phiêu lưu" khám phá thế giới bên ngoài của cô bé tí hon Arrietty và tình bạn trong sáng , dễ thương giữa cô với cậu bé loài người. Câu chuyện như thoả mãn "cơn khát" về thế giới những người tí hon của mình, khiến mình cứ ngấu nghiến đọc một lèo từ đầu chí cuối quyển sách. Thêm vào đó là bìa tái bản lần này của Nhã Nam cũng khá đẹp, màu xanh của khu rừng cây rậm  rạp với trung tâm là bóng của người tí hon rất phù hợp với nội dung câu chuyện, đồng thời gợi trí tò mò cho người đọc...
4
151116
2015-08-21 12:15:23
--------------------------
266537
3908
Mở đầu câu chuyện là cô bé Kate bị mất chiếc que đan khiến bà May liên tưởng đến những Người Vay Mượn - những sinh vật tý hon tồn tại trong câu chuyện của cậu em trai của bà. Trong ký ức hạnh phúc, cậu bé gặp gỡ gia đình Arrietty và biết thêm nhiều về cuộc sống của những người vay mượn. Biến cố xảy đến khi cậu cố giúp họ có cuộc sống ấm no hơn. Câu chuyện thiếu nhi lồng với ý nghĩa cuộc sống và một chút châm biếm sự hiếu chiến của con người. Cái kết hơi bất ngờ, dù không rõ những người vay mượn có tồn tại hay chỉ là sự tưởng tượng của cậu bé nhưng chắc chắn khi cầm quyển sách này, ta sẽ như cô bé Kate nóng lòng muốn nghe hết câu chuyện. 
4
326002
2015-08-14 19:41:52
--------------------------
261581
3908
Bìa cuốn sách khiến mình muốn mua ngay vì dễ thương quá. Còn khi mở cuốn sách ra bạn sẽ lập tức bước vào thế giới người tí hon sinh động, đáng yêu, một tình bạn đẹp không bị ngăn cách bởi giống loài. Mình nghe nói sách này đã được chuyển thể thành phim hoạt hình nhưng chưa được xem. Có lẽ sẽ rất dễ thương. Đọc sách thì được quyền tha hồ tưởng tượng, xem phim thì sẽ như đến với thế giới mà bạn đã tưởng tượng ra. Cuốn sách này hợp với trẻ con nhưng người lớn cũng nên đọc để đến gần hơn với thế giới mà mình đã trải qua.
5
266218
2015-08-11 10:56:24
--------------------------
254000
3908
Chỉ riêng cái tên thôi cũng đã gây tò mò cho mỗi chúng ta. Cuốn sách kể về thế giới của những người ti hon mà tiêu biểu là gia đình của Arrietty. Cuộc gặp gỡ giữa cô và cậu bé Sho khi cậu đến dưỡng bệnh tại nhà cũ của mình trong rừng đã gây ra vài rắc rối nhỏ đáng yêu. Nhưng từ đây, một tình bạn đẹp đã nảy sinh giữa hai người, vượt qua rào cản về giống loài. Cuốn sách cũng đã được chuyển thể thành phim hoạt hình. Nếu xem phim, bạn sẽ vô cùng ngạc nhiên về ngôi nhà nhỏ của Arrietty. Tuy nhiên tôi khuyên bạn nên đọc sách trước khi xem phim, sẽ hay hơn rất nhiều đấy.
4
606112
2015-08-04 22:16:51
--------------------------
243871
3908
Mình cảm thấy khá hổ thẹn khi thú nhận là mình mua quyển này vì bìa tái bản quá đẹp :( Nhưng mua về đọc hết rồi mới thấy không uổng tiền chút nào cả. Cốt truyện thì quá hay, dịch vài chương đầu hơi chán nhưng càng về sau càng hấp dẫn, cách cô Norton miêu tả cảnh và chuyển động của sự sống thì phải nói là quá hoàn hảo, tuy cái kết của quyển 1 làm mình cảm thấy khá bối rối nhưng lại khiến mình hồi hộp khi nghĩ đến cảm giác được cầm các quyển còn lại trong series trên tay. Nói chung, đây là một tác phẩm kinh điển dành cho thiếu nhi tuyệt vời! Rất đáng để "nhấm nháp" trong những ngày mưa.
4
67580
2015-07-27 20:22:37
--------------------------
242941
3908
Cuốn sách không chỉ dành cho trẻ con mà còn dành cho tất cả những tâm hồn cô đơn khao khát một tình bạn đẹp. Đây vốn không phải là cuốn sách đầu tiên kể về thế giới của những người tí hon, trước đó đã có "Gulliver du kí", nhưng điều làm cuốn sách này "đẹp" hơn hẳn là vì Mary Norton đã lồng ghép vào đó một tình bạn tuyệt đẹp, một tình bạn nảy sinh giữa 2 tâm hồn cô đơn, một tình bạn vượt trên những khoảng cách về kích thước, giống loài, vượt trên sự ngăn cấm của mụ bếp ích kỉ. Con người ta không bao giờ cô đơn trong thế giới của riêng mình, như Arrietty đã trở thành một phần của Sho.
5
528434
2015-07-27 08:29:21
--------------------------
241218
3908
Mình biết đến "những người vay mượn tí hon" qua bộ phim hoạt hình "Arrietty - cô bé vay mượn" của hãng phim hoạt hình Ghibi. Sau đó, thấy nhà xuất bản Nhã Nam có phát hành quyển này nên mình đã mua ngay về đọc. Quyển sách là câu chuyện kể lại cuộc sống của những người "vay mượn" tí hon, sống ở những ngôi nhà vắng vẻ hoặc ít người. Họ có thể sống ở dưới sàn, trong hốc tường hoặc ở cả trong hang của con lững trong vườn. Họ "vay mượn" những đồ dùng của con người để phục vụ cho sinh hoạt của họ. Công việc "vay mượn" rất nguy hiểm vì có thể bị "nhìn thấy" nhưng lại là một trải nghiệm thú vị đối với cô bé Arrietty, con gái của một gia đình người "vay mượn" sống ở dưới sàn căn bếp của một toà lâu đài cổ. Câu chuyện có thể có thật hoặc không, nhưng đã mang đến một màu sắc mới, một thế giới khác, một thế giới mà từ đứa trẻ con đến người lớn đều mong muốn được tiếp xúc. Tuy sách và phim hoạt hình có nội dung khá khác nhau nhưng mình rất thích.
4
40835
2015-07-25 12:02:32
--------------------------
225361
3908
Dù đã đọc và xem không ít những tác phẩm kể về người tí hon nhưng Những Người Vay Mượn Tí Hon của Mary Norton  vẫn mang đến cho tôi những cảm xúc trong trẻo tuyệt vời. Đọc một câu chuyện tuyệt vời như vậy khi mà tuổi đời đã chẳng còn trẻ trung thơ bé gì, tôi cảm thấy như mình đang quay ngược thời gian, trở về thơ ấu, cái thuở vẫn còn dòm xuống gầm giường gầm chạn hay tìm tòi nơi hốc cây bụi lá, bởi biết đâu nơi ấy sẽ có một gia đình tí hon sinh sống đầy bí mật. 
Cuốn sách mỏng thôi nhưng từ nội dung tới hình thức ddeuf thật tuyệt vời. Tôi sẽ giữ gìn nó thật cẩn thận để cho những thế hệ sau của gia đình tôi tiếp tục được thưởng thức kiệt tác văn học thiếu nhi này.
4
167283
2015-07-10 11:32:00
--------------------------
217521
3908
Bìa tái bản của Nhã Nam rất đẹp (Tiki không cập nhật bìa, đây là bìa cũ rồi), mình bị lôi cuốn bởi cái thế giới tí hon dễ thương hiện lên trên màu xanh huyền bí của rừng cây nên dù chưa xem phim chuyển thể và chưa rõ nội dung mình đã muốn rinh ngay bạn ấy về thế giới sách của mình. 
Mình vẫn chưa dám đọc truyện vì nghe bạn mình nói sách không chỉ có một tập, mong Nhã Nam sẽ sớm phát hành tập tiếp theo để mình còn rinh đủ chị đủ em ^^.
4
49203
2015-06-29 14:49:19
--------------------------
213114
3908
Tôi đã xem phim trước khi đọc quyển sách này. Mặc dù cả hai có khác nhau về nội dung chút ít nhưng cả hai đều rất hay, rất đáng xem, và tôi cũng rất thích chúng.
Tôi không nghĩ "Những người vay mượn tí hon" chỉ gói gọn trong phạm vi thể loại sách thiếu nhi mà nó có thể phù hợp với mọi lứa tuổi. Một quyển sách dễ dàng chiếm trọn sự yêu thương từ độc giả, mỗi khi ai có dịp được tiếp xúc với nó. 
Còn về bìa sách, Tiki đã nhầm giữa bìa cũ và bìa mới tái bản. Cuốn sách tái bản 2015 có bìa màu xanh lá cây rất đẹp và bắt mắt, chứ không phải bìa mà tiki hiện đang để trên web.
5
122381
2015-06-23 12:57:17
--------------------------
208248
3908
Mình đọc cuốn sách này vì đã xem phim nên khá tò mò muốn biết truyện sẽ như thế nào. Cảm giác cả truyện và phim đều rất tuyệt :> Nếu mình vẫn còn bé có lẽ mình sẽ nhìn quanh xem xung quanh mình có người tí hon nào đang sống hay không ^^ Cốt truyện hay và cảm động, nhiều chỗ hóm hỉnh và đầy hồn nhiên ♥ Một cuốn sách văn học thiếu nhi tuyệt vời rất đáng để đọc thử mỗi khi mệt mỏi. Sách không quá dày, không quá căng thẳng như những tác phẩm kinh điển. Đọc rồi cảm giác rất nhẹ nhàng. Mình được bố tặng cuốn này vì vòi quà sinh nhật, mua ở 1 nhà sách nọ mà bị họ chém 45k, mình rất bực, định sang ngay nhà sách kế bên tìm mua nhưng bố nói ko đáng mấy đồng nên thôi, mua luôn. Mua ở tiki mỗi tội mất ship chứ thực ra có 38k các bạn à, mong tiki giảm nữa để các bạn yêu sách thiếu nhi có thể dễ dàng mua tác phẩm này hơn
5
114793
2015-06-14 17:02:19
--------------------------
207993
3908
Minh xem phim  Arrietty của Ghibli rồi mới đọc truyện này. Nói thật là xem xong phim dễ dàng hơn để tưởng tượng ra truyện: nhân vật hay khung cảnh, tuy cũng vì vậy mà mình hơi bị bó buộc một chút khi đọc. Minh thực sự thích tình bạn của Arriety và Sho, rất hồn nhiên, nhưng mình thấy một thứ gì đấy nhen nhóm, sự thật là mình đã mong đợi hơn vào cái kết truyện. Mình mong gì đó hơn là việc gia đình họ chung sống hạnh phúc, có thể là khởi đầu cho tình yêu của một cậu bé loài người và cô bé tí hon chăng ?
4
20175
2015-06-13 21:15:49
--------------------------
192480
3908
Hoàn toàn hài lòng với hình thức lẫn nội dung của quyển sách này ^^ Sách hay, thú vị về cuộc sống những người tí hon bên dưới ngôi nhà của con người. To lớn và tí hon – tưởng như trong họ luôn có những suy nghĩ khác nhau, nhưng giữa họ lại xuất hiện 1 thứ tình cảm thiêng liêng lạ kì, truyền từ đời này sang đời khác. Câu chuyện làm mình thấy rất vui, và cũng rất xúc động. Thật tiếc khi đọc hết, mình còn muốn khám phá cuộc sống của họ nhiều hơn nữa ^^
4
472572
2015-05-04 17:18:31
--------------------------
191176
3908
Quyển này tái bản bìa rất đẹp mà hình như Tiki để nhầm hình bìa cũ rồi. nội dung mình đã đọc nhưng mình vẫn mua vì quá thích cái bìa mới, nó đẹp và ấn tượng hơn hẳn cái bìa cũ. Đây là 1 quyển sách thú vị về cuộc sống của những người tí hon, về những suy nghĩ, những trăn trở và tình cảm của họ, đó còn là tình bạn thật đẹp giữa Arriety và Sho – cậu bé con người mà lẽ ra Arriety luôn phải tránh xa. Truyện thật dễ thương và đáng yêu biết mấy.
4
418163
2015-05-01 09:45:43
--------------------------
190107
3908
Mình biết đến câu chuyện này cũng do xem phim hoạt hình của Ghibli. Quả thật mới xem nhưng đã bị mê hoặc bởi thế giới tí hon kì diệu bí ẩn nên quyết định đọc cuốn sách này. Và hơn cả kì vọng đây là một cuốn sách khá hay và hấp dẫn. Cuộc sống của những người vay mượn tí hon hấp dẫn và bí ẩn được mở ra qua từng trang sách. Họ sống cũng giống như con người bình thường tuy nhiên luôn phải lẩn trốn con người và sinh sống bằng cách vay mượn. Mỗi người trong thế giới mang một màu sắc riêng, một tính cách riêng nhưng mình đặc biệt thích cô bé Arriety tính cách và sự tò mò đưa cô bé đến khám phá thế giới con người cũng như mang người đọc khám phá thế giới của mình trong con mắt của những người tí hon.
5
94962
2015-04-28 20:24:26
--------------------------
