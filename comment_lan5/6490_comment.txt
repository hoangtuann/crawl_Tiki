415714
6490
Từ nhỏ đã rất mê các thể loại về vũ trụ về thiên hà và về UFO, nhưng thật sự đi nhà sách tìm rất hiếm khi có thể tìm được sách về thể loại này. Ngay khi nhìn thấy cuốn này mình đã mua ngay không suy nghĩ, và đây thật sự là một cuốn sách thú vị, cũng có thể xem là quý giá bởi những tài liệu được cung cấp trong đó. Đọc cuốn sách này những hình dung về UFO của tôi được rõ ràng hơn bao giờ hết, và càng thêm khẳng định cho niềm tin UFO là - có - thật! Rất thích.
5
1139174
2016-04-13 21:49:30
--------------------------
411118
6490
Đối với nhiều người,UFO là một đề tài hoang đường,rằng UFO là không có thật.Nhưng mình hoàn toàn tin rằng vẫn có UFO,vẫn có sự tồn tại của người ngoài hành tinh.Chẳng qua nó bị che dấu quá lâu bởi một số lý do.Mình vẫn chưa hiểu tại sao họ lại muốn giấu nó.Mục đích đằng sau sự che giấu này là gì.Vậy sự thật là gì,liệu có UFO và người ngoài hành tinh không?Để giải đáp thắc mắc này,mình đã đặt mua cuốn bí mật UFO này.Cám ơn tiki đã tạo điều kiện để mình có thể mua được cuốn sách thú vị này.
Gửi trả lời
5
543671
2016-04-05 16:06:50
--------------------------
377941
6490
Mình luôn thích tìm hiểu về UFO nên quyết định mua quyển sách này. Và sau khi đọc xong thì mình hoàn toàn hài lòng về lựa chọn đúng đắn của bản thân. Cuốn sách đã cung cấp cho mình rất nhiều thông tin bổ ích xoay quanh UFO, với cách trình bày rất khoa học, dễ hiểu. UFO, đề tài tưởng chừng như khô khan nhưng qua cuốn sách này, nó trở nên thú vị hơn bao giờ hết. Mình nghĩ những ai có đam mê về thiên văn và đặc biệt là UFO nên mua quyển sách này. Rất đáng đồng tiền bát gạo.
5
724380
2016-02-03 20:56:19
--------------------------
265212
6490
Không giống những bài báo thường có trên mạng hay những lời bình vô căn cứ về hiện tượng UfO . Tại cuốn sách này , tác giả giải thích hiện tượng một cách rõ ràng rằng từ UFO không hẳn là định nghĩa của sản phẩm thuộc ngoài hành tinh . mà là những vật thể bay không có lời giải thích đáng , đọc cuốn sách này , ta có thể cảm nhận được sự nghiêm túc , chân thật trong từng chứng cứ , dữ liệu tác giả đưa ra . Cầm quyển sách từ những trang đầu tiên , nó thật sự cuốn hút bạn đến tận trang cuối cùng . Đây quả là cuốn sách tuyệt nhất cho những ai luôn muốn hiểu biết về UFO
5
740173
2015-08-13 20:31:06
--------------------------
256599
6490
Ngay từ những trang đầu tiên, tôi đã bị hấp dẫn bởi hiện tượng các vật thể lạ bay trên bầu trời tại Bỉ. Tôi thực sự đã không thể ngừng đọc cho đến khi cuốn sách chỉ còn vài trang, lúc đó là 1 cảm giác ngỡ ngàng xen lẫn tiếc nuối vì đã "nỡ" đọc hết cuốn sách hay như vậy. Cuốn sách cung cấp những tài liệu rất chính xác và cụ thể về UFO, cách viết khoa học, dễ hiểu và vô cùng thuyết phục. Nếu bạn không biết nhiều về UFO, bạn phải đọc cuốn sách này. Nếu bạn nghĩ rằng UFO là đề tài vô nghĩa, cuốn sách này sẽ giúp bạn tỉnh ngộ!
Nhưng mình có thất vọng chút xíu về bìa sách, nó mỏng quá!
4
677404
2015-08-06 23:32:05
--------------------------
159053
6490
UFO luôn là đề tài bí ẩn cho các nhà khoa học cũng như cho những người yêu thích vũ trụ. Những bằng chứng về UFO đã có mặt trên khắp thế giới nhưng vẫn không ít người không tin vào điều đó. Nếu bạn là một trong số những người kể trên thì hãy đọc cuốn sách này, nó sẽ làm thay đổi suy nghĩ của bạn cách hoàn toàn.
Với những số liệu chính xác, dẫn chứng thực tế và thuyết phục cùng cách trình bày sách cực kỳ khoa học sẽ giúp bạn dễ dàng hiểu rõ và hoàn toàn tin tưởng vào sự tồn tại của UFO. Cuốn sách này không chỉ giúp bạn mở mang kiến thức mà còn là một món quà đầy tri thức thành cho những người bạn đam mê khoa học của bạn. Hãy thử đọc nó và khám phá một trong những đề tài hấp dẫn nhất của vũ trụ học nhé!
5
225054
2015-02-14 19:31:58
--------------------------
