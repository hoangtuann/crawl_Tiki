399259
7112
Sách được biên tập và in ấn rất đẹp mắt, mới cầm lật nhanh nhanh xem sơ qua đã thích rồi.
Minh biết đến cuốn sách này lúc em bé được 7 tuần tuổi. Sách hay và hấp dẫn mình đọc nhanh nhanh đến tuần 8 thì mỗi ngày đọc 1 trang.
Sách do tác giả VN biên tập nên rất dễ hiểu, không như kiểu sách dịch, khô khan và khó hiểu, lại không phù hợp thực tế cuộc sống ở một số vùng xa thành phố.
Sách khá hữu ích nhưng một số nội dung tùy theo tính cách và môi trường sống của mẹ sẽ hơi khó áp dụng, mình sống kiểu nông thôn quen rồi nên một số vấn đề có vẻ "sang chảnh", "sến sến" là mình cho qua luôn
5
508177
2016-03-17 14:37:51
--------------------------
381946
7112
 Vừa nhận sách xong, lật sách đọc vài trang cảm giác thực sự thích thú. Sách rất hay, hướng dẫn chi tiết những công việc cần làm theo từng ngày. Mình mua sách khi đã được 17 tuần rồi, hơi tiếc 1 chút vì đã không biết đến quyển này sớm hơn. Giao diện trình bày rất đẹp, màu sắc dễ chịu, hình ảnh sinh động. Cuốn sách này thực sự hữu ích cho bà mẹ mang thai lần đầu như mình. Sách còn có tặng kèm đĩa nhạc, cực kì hài lòng với sản phẩm này. Hàng hóa được tiki đóng gói cẩn thận, nhân viên giao hàng thân thiện.
5
68032
2016-02-18 12:44:59
--------------------------
357215
7112
Phần lớn sách cho bà mẹ, phụ nữ mang thai hiện nay trên thị trường là sách dịch nên có nhiều điều chưa phù hợp với môi trường sinh hoạt, thói quen, lối sống ở Việt Nam nên việc thực hiện theo khá khó khăn.
Quyển sách này có tác giả là người Việt nên cảm thấy thực tế hơn, dễ áp dụng hơn và nó cũng phù hợp với người Việt hơn. 
Sẽ giới thiệu với bạn bè và những người sắp làm mẹ quyển sách này. Đây là quyển sách rất đáng mua. Ngoài ra còn có đĩa CD tặng kèm. nó được chắc lọc, lựa chọn nên cũng yên tâm hơn các tài liệu trên mạng.
Mong con sẽ ra đời thật thông minh, khỏe mạnh. 
5
752472
2015-12-23 09:58:22
--------------------------
329160
7112
Nhận được sách mình rất là thích, Tiki đóng góp cẩn thận, giao hàng nhanh, sách mới, mình rất hài lòng khi nhận sách này.
sách in màu đẹp và rất nhiều thông tin rất hữu ích, mình biết được bé lớn như thế nào trong bụng mẹ, hàng ngày bé làm gì trong đó, những điều mình cần chú ý để tốt cho bé.
đọc từng trang sách giúp gắn kết mình với bé nhiều hơn, mình có thể thư giãn sau ngày làm việc mệt mỏi
cuốn sách này thực sự rất có ích cho mình và những người phụ nữ mang thai khác.
5
702333
2015-10-31 10:05:02
--------------------------
206130
7112
Mặc dù mang tựa đề "Thai giáo theo chuyên gia" nhưng với tôi cuốn sách giống lời khuyên của người thân, không cao siêu mà đơn giản, gần gũi. Mỗi ngày chỉ 1 trang, cả quá trình mang thai kì diệu chỉ gói gọn trong hơn 200 trang sách nhưng lại vô cùng bổ ích, thiết thực. Nhờ đó mà tôi biết được thai giáo không chỉ bằng cách dạy học cho con mà còn bằng dinh dưỡng, cảm xúc, v.v; muốn dạy con trước hết người mẹ, người cha phải thật thoải mái và dành hết tình yêu cho con. Cuốn sách là người bạn đồng hành của tôi suốt chặng đường mang thai này.
5
623530
2015-06-08 20:08:15
--------------------------
205193
7112
Mình đã hoàn toàn không biết thai giáo là gì cho đến khi đọc cuốn sách nay. Thai giáo- một khái niệm nghe thật lạ lẫm, em bé của mình đã vào tháng thứ 5 mình mới biết đến cuốn sách này, mình đọc ngấu đến đúng tháng thứ 5 thì dừng lại để mỗi ngày đọc 1 trang thôi. Sách rất bổ ích với mình, nhờ nó mình mới biết thai giáo đơn giản như thế nào, nhưng cũng hiệu quả như thế nào, nhờ nó, mình cũng biết thêm trong thời gian mang thai, các bà mẹ nên nói chuyện với con thật nhiều, lúc làm việc, lúc nghỉ ngơi, rồi cùng con vẽ tranh, lại cả tập yoga…. Em bé sẽ lớn dần, sẽ nghe hết đó! Mua sách lại còn được tặng kèm đĩa nữa chứ!
5
290680
2015-06-06 00:55:36
--------------------------
