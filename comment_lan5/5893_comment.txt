460072
5893
Em mình năm nay lên lớp 8, lúc đi học thầy giáo luôn bắt đem theo từ điển nên mình tặng em cuốn từ điển này. phải nói là nó rất thích vì từ điển có hẳn hộp đựng riêng nè, giải thích từ bằng tiếng anh đúng chuẩn, sử dụng phiên âm quốc tế, còn có thêm từ điển tranh theo chủ đề nữa có điều em mình thấy từ điển nên được thiết kế to hơn để bớt dày vì trông nặng lắm, hơi khó tra từ, mà tiền nào thì của đấy nên mình thấy cũng khá hài lòng.
5
1299219
2016-06-26 19:33:16
--------------------------
420959
5893
Mình mua sản phảm này từ năm lớp 8. Nói chung là cuốn sách này rất tốt, nó đã giúp đỡ mình trong việc học tiếng Anh rất nhiều. Về nội dung, phần đầu cuốn sách có những hình ảnh minh họa theo từng chủ đề rất dễ nhớ. Ở mỗi tứ có phần giải nghĩa bằng tiếng Anh, kèm theo những ví dụ và cấu trúc, thành ngữ rất chi tiết.Về hình thức, sách có bìa rất đẹp, kèm theo một chiếc hộp cứng bọc giúp cuốn tù điển được bền hơn. Đây là một cuốn từ điển rất hữu ích, các bạn nên mua nhé.
5
1125409
2016-04-24 09:55:21
--------------------------
322851
5893
Với tôi cuốn: Từ Điển Anh - Việt là cuốn sách thiết thực ứng dụng nhất mà tôi từng đọc. Cuốn sách đã bổ trợ cho tôi vô vàn những từ vựng mới, những từ chuyên dùng khi giao tiếp. Thực sự nhiều lúc tôi cũng hay bị quên từ nếu những lúc ấy mà không có cuốn từ điển này thì tôi chẳng biết phải làm sao.
Hiện nay, mặc dù từ điển điện tử ngày càng được ứng dụng nhiều và tra cứu cũng nhanh chóng hơn nhưng sách từ điển vẫn luôn có những giá trị riêng của nó mà không gì có thể thay thế được như cách giải thích khá cặn kẽ...
Tuy nhiên giá thành cuốn từ điển này tương đối đắt- đó cũng chính là rào cản về kinh tế, nhưng dù sao với tôi, đây cũng là cuốn sách tuyệt vời.
5
702741
2015-10-17 08:55:01
--------------------------
