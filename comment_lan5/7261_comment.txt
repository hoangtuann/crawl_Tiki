310758
7261
Mình rất thích đọc tuyển tập văn học tuổi 20 của NXB Trẻ, càng đọc càng thấy thấm. Những Đêm Không Ngủ Ở Toronto cũng vậy, tuy mang sắc thái gần giống như Oxford thương yêu của Dương Thụy, nhưng lại có chút gì đó thực tế, gần gũi hơn. Đúng như tựa truyện, cuốn sách mở ra những chuỗi ngày trăn trở về gia đình, tình yêu, tình bạn, việc làm, chính trị.... Nhân vật chính trong truyện luôn bị giằng xé giữa những lựa chọn, và câu hỏi lớn nhất là "ở hay về?", chính câu hỏi đó mà dẫn đến bao nhiêu chuyện buồn, nhưng cuối cùng cô cũng có sự lựa chọn cho riêng mình.
5
323757
2015-09-19 19:34:48
--------------------------
305495
7261
Sau quyển Oxford thương yêu của Dương Thụy, đây là quyển thứ 2 tôi đọc về văn học tình yêu, cuộc sống của tuổi trê. Nếu đối với Dương Thụy của sống của Thiên Kim may mắn vì gặp được chàng trai TBN, người yêu thương cô một cách rất quái đảng, Thì đối với Thu Hoài Cuộc sống ở Toronto là "Những đêm không ngủ". Khi đọc sách của Dương Thụy ai cũng vẽ lên bức tranh tuyệt đẹp của du học sinh, thì đối với Thu Hoài đã làm cho ta thấy rõ hơn cuộc sống du học sinh không chỉ có màu hồng, Bằng ngòi bút văn chương của mình tác giả đã cho ta thấy được những sự trả nghiệm vui, buồn từ cách giao tiếp, mưu sinh và cả những rối bời suy nghĩ về tình yêu cũng như "Ở hay Về? ". Ngay khi đọc xong quyển sách tôi cũng thấy được một phần nào của bản thân trong tác phẩm. Xin cám ơn!
4
776871
2015-09-16 23:22:01
--------------------------
229372
7261
Vốn là một người thích đi du lịch nhưng...không đủ tiền, mình tìm đến sách du kí và du học như là một niềm vui mỗi khi rảnh rỗi. sách du kí giúp mình vừa đọc sách vừa đc đi du lịch tưởng tượng mà ko cần phải đến tận nơi, biết nhiều hơn về các địa danh trên thế giới. Hễ có sách du kí là mình là mua ngay để đọc. Lang thang trên Tiki thấy cuốn này như bắt đc vàng nên mua ngay. Là một người thích đi du học Canada nên khi nghe tựa đề nhắc về thành phố Toronto xinh đẹp, mình lại càng vui hơn nữa vì lâu nay Canada vốn bị lãng quên trong sách du kí mà thay vào đó là châu Âu, châu Á và nước Mỹ có phần nhiều hơn.Ttừ lâu mình đã rất thích được đi Canada, được ngắm tuyết rơi, được đi dạo theo những con đường đỏ sắc lá phong. Đọc cuốn sách này mình đã hiểu biết hơn nhiều về những vấn đề khi đi du học, những vấn đề thủ tục khi đi du học, những rắc rối và phức tạp trong đời sống ở xứ người, những toan tính, cũng như những âu lo mệt mỏi khi phải đối mặt với nhiều vấn đề về học hành cũng như làm việc. Bên cạnh việc thể hiện rõ những mặt thật của cuộc sống du học, truyện còn xen vào câu chuyện tình cảm của tác giả, nhẹ nhàng có, buồn vui có,.... Tất cả đều mang đến một cảm xúc thú vị cho người đọc và khi gấp cuốn sách lại, dường như mình đã học hỏi và biết thêm được rất nhiều điều hay ho mới lạ về đất nước Canada xa lạ nhưng rất đỗi thân thương ấy. Mình không hề cảm thấy tiếc nuối khi mua cuốn sách rất hay này.
5
393384
2015-07-16 19:45:50
--------------------------
192801
7261
Nếu  Anh Quốc hiện lên với nét cổ kính mà hoa lệ trong " Oxford thương yêu", hay nước Bỉ đẹp đẽ mà chân sơ ở " Cung đường vàng nắng" đã từng và đang tiếp tục khiến bao người say mê thì chắc chắn các bạn sẽ thích thú với quyển sách này. " Những đêm không ngủ ở Toronto", nghe tựa đề chắc rằng cũng đã phần nào đoán được Canada chính là điểm dừng chân của cuộc " hành trình". Nhắc tới đất nước nằm bờ trên của nơi nổi tiếng với tượng Nữ thần Tự do thì dường như ai cũng chỉ nghĩ tới một từ : LẠNH. Tuy nhiên, theo ngòi bút của tác giả, Canada không đơn thuần chỉ có lạnh, vẫn còn những cảnh đẹp, những con người và những mảnh kí ức khiến tim mình ấm áp; xứ lá phong hiện lên thật đẹp và tráng lệ, khiến người ta quên đi cái lạnh vốn có của nó. Bên cạnh đó, tác giả đã lột trần những vấn đề nhức nhối về vấn đề du học mà hầu như ai cũng biết nhưng luôn chối bỏ bằng những lý do xuất phát từ lòng ích kỷ : việc chọn ngành học chỉ để dễ định cư, cha mẹ bắt ép con mình làm dâu xứ người để được tờ giấy PR ( thẻ xanh), du học sinh bị bóc lột tàn nhẫn từ chính đồng bào của mình,.. Những lần mỏi mệt vì làm việc nhiều giờ, những lúc tê tái khi phải đi bộ giữa trời tuyết, những khi thất vọng vì chẳng được công ty nào gọi phỏng vấn,  những lần khắc khoải khi phải chịu áp lực có được thẻ xanh từ phía gia đình, và cả những đêm không thể ngủ vì bị vây kín bởi những vấn đề không lối thoát... tất cả những mặt trái đau đớn của du học sinh đều được bộc lộ rõ nét một cách trần trụi, nó chân thực tới mức khiến người ta phải đau lòng.Tuy nhiên, những mệt mỏi, chán chường, khổ đau đều được xóa tan nhờ tình yêu và lòng chân thành, nhờ sự thấu hiểu và lòng cảm thông ; giữa cái rét buốt cắt da cắt thịt ở Canada, con người ta vẫn luôn ấm áp, lạc quan, và vẫn luôn đủ nghị lực vượt qua mọi khổ ải và gian nan.  " Những đêm không ngủ ở Toronto" là một lựa chọn không tồi cho những ai luôn khát khao được đặt chân đến nơi xứ người.
5
561582
2015-05-05 18:25:02
--------------------------
120533
7261
Cuốn truyện quả thực rất hay. Tác giả đã viết về một đề tài muôn thuở mà gần như không bao giờ có lời giải đáp: Về hay ở sau khi du học? Ở truyện này, tác giả đã cho thấy được nhiều khía cạnh khác nhau của việc du học ở nước ngoài. Từ đầu đến cuối truyện, tác giả vẫn không có ý kiến ủng hộ quan điểm nào, chỉ liệt kê ra những mặt ưu điểm và khuyết điểm của việc về hay ở để rồi có một kết thúc mở cho người đọc tự ngẫm nghĩ, trong sự tiếc nuối của mối tình Duy-Linh mà thôi

Gấp trang sách cuối cùng lại, câu nói của tác giả cứ làm mình suy nghĩ mãi: cuộc đởi không bao giờ là một đường thẳng mà là một mê cung để rồi khi đứng trước một ngả rẽ không thể biết trước nó sẽ dẫn ta tới đâu. Đúng vậy, quan trọng nhất là mỗi người phải có bản lĩnh để đi theo ngã rẽ đó và có thể tự chịu trách nhiệm với những gì đã chọn.

Mình đọc xong đã cho một đứa bạn mượn xem và nó cũng rất thích. Tuy nhiên, chúng mình đều có cảm giác rằng có một số chương dường như rời rạc, không được liên kết với nhau lắm. 

Đây chắc chắn là một tác phẩm đáng đọc cho những ai đang và sẽ du học
5
383339
2014-08-13 09:27:31
--------------------------
