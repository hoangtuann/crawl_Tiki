432587
6672
Lại một tuyển tập truyện ngắn từ cuộc thi viết được xuất bản thành sách - điều nên được khuyến khích. Tập truyện đa dạng về nội dung, từ những mẩu truyện theo cái nhìn của người trong cuộc - đồng tính nữ (nội dung khá hiếm thấy), đồng tính nam - và theo cái nhìn của những ai có người thân, bạn bè là thành viên cộng đồng LGBT. Các mẩu truyện rất ngắn nhưng vẫn có chiều sâu và những cái kết có hậu. Tập truyện cho ta thấy được một tương lai tươi sáng và có phần khá khẩm hơn cho cộng đồng LGBT.
4
748432
2016-05-19 13:10:09
--------------------------
414929
6672
Quả thực lúc mình mua sách không hề để ý đây là quyển sách về đề tài đồng tính. Mua vì bìa ấn tượng và mình thì thích đọc truyện ngắn nên mua. Sau khi đọc sách (mình vẫn đọc chưa xong) mình chỉ có 1 nhận xét rằng những câu truyện được viết trong này ngắn, quá ngắn và (mượn hai chữ mình đã nghe ở đâu đó) "rất đời". Thế nên, nếu quan tâm đến đề tài này thì hãy xem như một quyển sách để hiểu thêm về cuộc sống của những con người trong thế giới này nhé.
4
1162275
2016-04-12 19:26:26
--------------------------
388481
6672
Quyển sách này, mình được tặng khi mua đơn hàng trên 300k ở hội sách 21/4.
Ấn tượng đầu tiên là bìa và tên sách đều lạ, sau đó mình mới biết đây là sách về cộng đồng LGBT.
Không phải là truyện ngắn, mà là những câu chuyện siêu ngắn về tình yêu đồng giới, về gia đình, định kiến xã hội.... 
Gần như, khi đọc xong quyển sách, thì đó là tất cả những gì mình biết về cộng đồng này.Đến bây giờ, khi ngồi viết nhận xét này, thì kiến thức về cộng đồng LGBT cũng nhiều hơn và với mình, đó là thông cảm... và mình muốn nói:Họ có quyền được yêu và sống hạnh phúc.
4
1157982
2016-02-29 00:55:10
--------------------------
348354
6672
Mua sách vì bìa sách đẹp và cái tên lạ, nội dung về cộng đồng LBGT nên mình đã hi vọng sẽ đọc được nhiều câu chuyện hay, sâu sắc và nhân văn hơn thế. Một phần có thể cũng vì không quen đọc truyện mini siêu ngắn thế này, nhưng thật sự rất thất vọng về chất lượng truyện. Chỉ có một vài truyện là hay nổi bật, và khi đọc mình thực sự cảm thấy truyện có ý nghĩa và có tính nhân văn. Ngoài ra các truyện còn lại làm mình có cảm giác họ chỉ viết về cộng đồng LGBT như một chủ đề chứ không bỏ thời gian tìm hiểu về hoàn cảnh, tâm lý của họ.
2
557111
2015-12-05 23:19:30
--------------------------
251493
6672
Sau Hành trình về thị trấn buồn tênh, đây là tập truyện ngắn thứ hai mình đọc về đề tài đồng tính. Mình rất quan tâm về đề tài này, trong mình luôn hi vọng rằng các tác phẩm về đề tài đồng tính sẽ lột tả hết được cái nhìn chân thực về đề tài này cũng như là cầu nối cho sự đồng cảm của nhiều người cho thế giới thứ ba.
Và quyển sách này không làm mình thất vọng.
Dù là truyện ngắn (hoặc nói theo cảm tính là rất ngắn), nhưng các câu truyện đều lột tả đầy đủ và chân thực những mảnh đời của người đồng tính trong xã hội Việt Nam, từ thành thị tới nông thôn, ở mọi ngõ ngách, mọi số phận. Dù rất ngắn, nhưng truyện đã cô đọng được những cảm xúc lúc nhẹ nhàng, lúc dữ dội của những người đồng tính. Mình rất thích thú và trân trọng điều đó. Cái hay là vì truyện rất ngắn nên đọc sẽ không thể bị nhàm chán được, lại gây kích thích càng muốn đọc!
Một cuốn sách đáng để đọc trong những lúc rảnh rỗi ngắn ngủi, hay những lúc bình lặng của ngày.
Sách đẹp, bìa giản dị nhưng gợi được sự tò mò, trình bày truyện rất hay!
Hi vọng truyện sẽ bán tốt hơn nữa, trân trọng!
5
80550
2015-08-02 21:17:12
--------------------------
224805
6672
Gọi là truyện ngắn chứ theo mình thấy thì là truyện siêu siêu ngắn luôn, có mấy truyện chỉ độ 1, 2 trang thôi à. Mà không hài lòng với cách bố trí trang giấy lắm, lề ngoài trống quá nhiều, cảm giác cứ như nxb cố tình để kéo thêm được vài trang giấy nữa ý. Cũng có lẽ vì truyện quá ngắn mà nhiều lúc thấy hụt hẫng, chưa cảm được nội dung tác giả muốn truyền đạt thì truyện đã hết từ đời nào rồi. Vì là truyện tự sáng tác của những cây bút không chuyên, nên không thể đòi hỏi nhiều hơn về nội dung. Nhưng về phần cảm xúc thì quyển sách này vẫn chưa làm mình hài lòng. Dù sao cũng rất ủng hộ những truyện vì giúp mình hiểu thêm về cộng đồng LGBT và thông cảm với họ.
3
371734
2015-07-09 11:28:45
--------------------------
180346
6672
Đây là lần đầu tiên đọc một cuốn sách với những truyện ngắn mà lại cô đọng và chạm tận đáy cảm xúc như vậy. Mỗi truyện chỉ vỏn vẹn gần 2 trang nhưng những gì nó mang lại gần như bằng một cuốn sách 200 trang. Từng chi tiết về suy nghĩ, lời thoại, tình huống của các nhân vật đều truyền tải những ý nghĩa cực kì hay. Bạn không thể đọc lướt hay bỏ dỡ được cuốn sách này. Mỗi trang bạn đọc qua sẽ khiến bạn bần thần suy ngẫm hồi lâu và đôi khi man lại đôi chút ám ảnh...
4
140964
2015-04-09 10:46:36
--------------------------
156461
6672
Mình đặt mua cuốn này vì một người bạn của mình có truyện được in trong đó. Nhưng quả thật đây là một cuốn sách hay và đáng đọc.
Bìa sách đẹp, đợt mình mua còn được tiki khuyến mại bọc nữa nên nhìn càng đẹp hơn.
"Xoạc cẳng đợi mùa xuân" gồm nhiều truyện ngắn mini cực kì cô đọng, nhưng không vì thế mà không lấy đi nước mắt của mình. Những mẩu truyện về bé gái có hai người mẹ, về người chồng sẵn sàng đưa người yêu của vợ về nhà dưới danh nghĩa vợ lẽ để hai người được hạnh phúc, về hai anh chàng trọ chung một nhà rồi yêu nhau,... đều khiến mình cảm động. Đúng như lời bình của giáo sư Chu Văn Sơn ở đầu sách, có lẽ dung lượng nhỏ và ngắn nên cảm xúc mới dồn nén trong những lời văn, người đọc đọc và suy ngẫm nhiều hơn, thấm nhiều hơn.
5
397533
2015-02-04 20:58:37
--------------------------
