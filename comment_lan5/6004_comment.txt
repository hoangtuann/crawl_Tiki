521221
6004
Lúc đầu tôi tưởng mình sẽ gặp một Chung NGuyên Mộc Đồng oan gia thứ 2 nhưng đọc xong hơi thất vọng một chút. Những chương gần cuối có hơi rối một trước có lẽ tác giả một thức thách đôi trẻ đây mà... Nhưng vẫn là HE thôi! :)
3
1817024
2017-02-08 12:16:48
--------------------------
495656
6004
Trang bìa sách nhìn là ấn tượng, đọc thêm lời giới thiệu là tôi đã mua ngay.
   Truyện có những khúc nhẹ nhàng sâu lắng, khác với những câu chuyện ngôn tình khác , truyện diễn biến chậm nhưng lời văn thu hút ng đọc. Nữ chính thì đanh đá, tính tình giống nam nhưng bên trong lại yếu lòng.
   Nam chính thì tính tình thất thường làm cho nữ chính thấy phiền phức và đau đầu, nhưng chính anh đã kéo cho ra khỏi đau khổ. hai người lum phủ định tình yêu của mình dành cho đối phương. Cuối cùng anh và cô đã có đc hạnh phúc sau những biến cố xảy ra,
   Nói chung là cái kết có hậu
     Tôi thích cuốn này, nên mua, và trải nghiệm
4
838261
2016-12-13 21:22:15
--------------------------
476154
6004
Đừng vội nói lời yêu là cuốn tiểu thuyết có môtip quen thuộc, một công tử nhà giàu đẹp trai, tính cách bá đạo và nàng lọ lem có hoàn cảnh khó khăn nhưng tính cách lạc quan, thẳng thắn đã trở thành định mệnh của nhau. Nhưng có một điểm khác là nó rất thực tế, không mang chất ngôn tình lãng mạn sướt mướt hay đi sâu quá nhiều về nội tâm nhân vật,  nhưng vẫn rất cuốn hút người đọc vì tính chân thực của nó. Riêng tôi cực thích tính cách bá đạo của Tiêu Kiến Thành, mặc dù anh tính cách thất thường, bá đạo nhưng a rất yêu Tô Lạc, do tính thẳng thắn và bất chấp để có được người mình yêu nên anh vụng về trong cách thể hiện tình cảm và làm cho Tô Lạc ghét anh... Nhưng cuối cùng vẫn là HE, lọ lem thì vẫn phải dành cho hoàng tử thôi...☺☺
4
1133817
2016-07-18 22:01:33
--------------------------
447887
6004
Đây là lần đầu tiên mình đọc truyện của Tự Do Hành Tẩu. Nói hay cũng không phải hay nhưng nói dở lại càng không phải dở chỉ có thể nói là tạm ổn. Cũng giống như những cốt truyện ngôn tình khác, tính cách nhân vật cũng vậy: anh giàu_ đẹp trai_ giàu có; còn cô thì nghèo_ đáng yêu_ dí dỏm. Mối tình ấy lại nảy nở sau nhiều đanh đấu tranh với nhau. Lại thêm một câu truyện hài hước đáng yêu. Về phần chất lượng giấy và bìa của sách thì cũng coi khá tạm được.
3
1193836
2016-06-15 00:00:28
--------------------------
438209
6004
Đừng Vội Nói Lời Yêu là cuốn sách đầu tiên tôi đọc của Tự Do Hành Tẩu. Nội dung truyện khá hay, điều tôi thích là nó rất thực tế, không như 1 số truyện ngôn tình khác. Tôi rất thích tính cách của nhân vật nữ chính, mạnh mẽ, thông minh và quyết đoán nhưng ngược lại tôi không thích nhân vật nam chính 1 chút nào. Anh ta quá tự cao và trẻ con, hay ghen tuông quá mức. Còn tình tiết truyện thì chưa được đầu tư nhiều, không kịch tính và cuốn hút như mong đợi. 
3
1258770
2016-05-29 14:04:13
--------------------------
423524
6004
Thứ nhất mình nói về khoản ngoại hình thì cuốn này không có gì đáng chê trách cả đẹp,sinh động,mang một cái gì đó man mác buồn,bookmark cũng rất ưng.Nội dung chuyện mình thấy cứ nhàng nhàng theo lối văn chương của các tác giả khác nhưng với mình thì cũng ổn,rất hấp dẫn người đọc.Tuy nhiên diễn biến tình cảm tâm lí của nhân vật cứ bấp bênh thế nào í, Kết thúc chuyện hơi nhanh so với suy nghĩ của mình tuy nhiên là cái kết có hậu nên mình rất hài lòng!Tiki gói hàng cũng rất cẩn thận,cảm ơn tiki!
3
819443
2016-04-29 15:14:38
--------------------------
418771
6004
Thật ra nội dung cuốn sách này không có gì mới lạ bởi vẫn chỉ là câu chuyện tình giữa một chàng công tử và một cô nàng nghèo khó bị khinh thường bởi cuộc đời. 
Nhưng điểm thú vị tác giả mang đến trong cuốn sách này là xây dựng hình tượng nữ chính khác hoàn toàn so với những cuốn ngôn tình khác. Nữ chính không hề yếu đuối, ngu ngu ngơ ngơ mà vô cùng thông minh, tỉnh táo, năng động và lạc quan trước cuộc đời nhiều sóng gió. Mình nghĩ chính vì tính cách lạc quan tích cực này của nữ chính đã thu hút nam chính. Thật ra hình tượng của nam chính trong cuốn này cũng rất thú vị. Khi thì phong lưu rất đáng ghét. Khi lại hài hước theo kiểu rất đáng yêu. 
Đây là một cuốn ngôn tình đáng đọc để giải trí nhiều hơn là suy ngẫm. Mình thích những cuốn sách nội dung vui vẻ không "hành não" người đọc nhiều như cuốn sách này.
3
281738
2016-04-19 22:18:20
--------------------------
400533
6004
Cô nàng này thật sự mạnh mẽ. Dù đó là sóng gió gì dù đó là chông gai gì thì cô nàng Tô Lạc này luôn vượt qua xuất sắc. Đó là điều tôi thích ở cô. Còn chàng trai Tiêu Kiến Thành khi mới bắt đầu chỉ xem cô như một niềm hứng thú mới của anh. Nhưng đó là sai lầm. Vì sau đó anh đã yêu cô mất rồi. Sẵn sàng bỏ ra thời gian tiền bạc vì cô. Và ngày càng lún sâu, ngày càng yêu cô hơn. Hai ngươi đến với nhau nhẹ nhàng như một cơn gió.
5
959784
2016-03-19 14:36:43
--------------------------
358680
6004
Ngay từ lúc đọc nội dung và trích dẫn truyện mình đã quyết định nhảy hố luôn . Thêm nữa là Tự Do Hành Tẩu cũng là một tác giả mà mình khá thích . Tô Lạc - nữ chính của truyện là một cô gái rất kiên cường , sống trong một ngôi nhà không được tôn trọng như vậy mà vẫn luôn không ngừng phấn đấu , có lẽ đây cũng chính là điểm đã lôi cuốn anh nam chính . Còn nam chính Tô Kiến Thành lại là một công tử phong lưu đúng nghĩa , thích TL mà luôn muốn chọc tức chị , tạo nên một một mối tình oan gia đáng yêu . Bạn hãy đọc thử truyện để cảm nhận thêm nhé !
4
743273
2015-12-25 18:34:44
--------------------------
353757
6004
Khách quan mà đánh giá thì cuốn truyện này của Tự Do Hành Tẩu chỉ ở mức tàm tạm thôi, chắc tầm 3 sao. Bìa truyện nhìn oải quá, lần sau nên chăm chút cho đep hơn. Nội dung thường thường không hay không dở, motip vẫn là nam giàu nữ nghèo không môn đăng hộ đối, nhân vật không quá nổi, Tô Lạc - nữ chính cũng như nam chính Tiêu Kiến Thành đều thế, còn tình cảm thì phải nói là biến chuyển với tốc độ quá nhanh, giải quyết  mâu thuẫn cũng chưa thấu đáo, nhưng văn phong của Tự Do Hành Tẩu thì khá hay
3
630600
2015-12-16 20:25:43
--------------------------
346025
6004
Mình khá thích tính cách của nữ chính Tô Lạc , mạnh mẽ . Còn về ông nam chính Tô Kiến Thành thì thôi khỏi nói rồi .. Sáng nắng chiều mưa , tính cách quái đản , mình cảm thấy truyện này có cái gì đó chưa đúng cho lắm . Mình không cảm nhận được tình cảm của Tô Lạc dành cho Tô Kiến Thành lắm , tình tiết truyện không hấp dẫn như mình tưởng . Thiết kế bìa khá bắt mắt , rất ưng ý , chất lượng giấy in tốt . Sách được bọc kĩ lưỡng , thích ngay từ cái nhìn đầu tiên 
3
824035
2015-12-01 19:12:52
--------------------------
280529
6004
Ngay từ lúc đọc các trích dẫn trong sách ở fanpage của Đinh Tỵ là mình đã rất kết cuốn sách này, chờ nó ra mắt là mua ngay. Bìa sách Kim Duẩn thiết kế đẹp hết sức, lại còn rất có hồn nữa. Giấy Phần Lan nhẹ nhưng hơi khó bảo quản. Nội dung gay cấn, những đoạn Tô Lạc đối đầu với Tiêu Kiến Thành có lúc khiến mình bật cười trước kiểu châm chọc đả kích của Tô Lạc khi nói Tiêu Kiến Thành là heo, hay những lúc Tiêu Kiến Thành hù dọa cô. Mình rất thích nhân vật Tô Lạc, hơi nóng nảy, nhưng lại rất ngây thơ và dễ tin người, không tính toán. Tiêu Kiến Thành chu đáo và bá đạo. Hai người này đều rất cứng đầu. Nhiều tình tiết thực tế, không ảo như trong tiểu thuyết. Rất thích cuốn này
5
182813
2015-08-27 19:30:33
--------------------------
266290
6004
Mình đọc trước vài chương trên mạng rồi mới quyết định mua truyện này. Phải nói là không hề thất vọng tý nào. Tuy là ngôn tình tất nhiên sẽ không tránh khỏi 1 vài tình tiết hơi phóng đại nhưng truyện rất hài và thực tế. Mình thậm chí còn hiểu thêm 1 vài lĩnh vực sau khi đọc truyện nữa. Tính cách của nữ chính rất thẳng thắn, thậm chí là hơi quá dữ dằn, nhưng điều này đúng với thực tế mà, đâu phải lúc nào nam nữ chính cũng phải đẹp - giàu - hiền hậu dễ thương và biết điều. Nói chung rất đáng đọc nếu bạn đã quá chán thể loại tình đẹp như mơ.
4
433563
2015-08-14 15:45:39
--------------------------
255743
6004
Đây là lần đầu tiên mình đọc truyện của tác giả Tự Do Hành Tẩu, nói thật thì mình thấy cũng ổn, không quá hay cũng không quá tệ, ít nhất là hay hơn khá nhiều quyển. Nhân vật nam chính mình thấy cũng khá bình thường, cách xây dựng nhân vật không mới nhưng mình đánh giá cao nữ chính, tính cách khá rõ ràng. Cách hành văn của tác giả khá thực tế là một điểm cộng lớn, nhưng bù lại có vài chi tiết mà mình nghĩ tác giả nên đi sâu vào thêm nữa đó là nội tâm nam chính và bối cảnh gia đình nam chính. Bìa truyện cũng không quá đẹp, nhưng mình thích màu bìa, giấy tốt.
3
19933
2015-08-06 12:31:32
--------------------------
209685
6004
Thật sự mình không hiểu sao quyển này bị đánh giá thấp thế,khi mình đọc đến hết truyện thì thấy truyện không hề dở tí nào,khá thực tế không mơ mộng gì nhiều dù là mô típ oan gia cô gái nghèo với tổng tài nhưng nữ chính trong truyện lại không thuộc phải thuộc dạng dễ dãi,cũng có suy nghĩ riêng mình cũng có coi trước coi sao,chẳng phải do anh nhà giàu đẹp trai mà đi thích,cô có người yêu thầm trước đó,nhưng điểm yếu nhiều lúc lại quá cả tin người khác.Mình đọc thấy trong truyện cũng có nhiều câu nói khá hay,đa số toàn của nam chính,phải công nhận một điều nam chính trong truyện này khá tưng tửng,nhưng đã câu nào nói thì chẳng thể cãi lại được,cái cách anh tán tỉnh và thể hiện tình cảm cũng rất bá đạo,nhiều lúc phải lắc đầu với mấy trò ghen trẻ con của anh,khá tiếc khi tác giả không nói sâu về gia đình hay thân phận của nam chính,nói chung truyện cũng khá nhanh,tập trung vào nữ chính là chủ yếu.Một cuốn truyện hay theo ý kiến của mình.
5
560863
2015-06-18 00:08:18
--------------------------
206200
6004
Tuy truyện theo mô típ cũ nhưng mà cách tác giả hành văn vô cùng thực. Mình không nói đến diễn biến tình cảm của hai nhân vật làm gì, cứ nhìn tình cảm diễn biến mà không nhìn khía cạnh thực tế trong truyện thì cũng khó rút kinh nghiệm ở bản thân. Tô Lạc sinh ra trong gia đình với hoàn cảnh bố mẹ ly hôn, bố rượu chè, bồ nhí bỏ vợ và hai con,... bla bla, đó là hiện thực đầy ra xã hội. Công việc của cô là công ty chuyên tổ chức buổi đấu giá gây quỹ từ thiện, thì sẽ gặp những nhân vật lớn cũng là chuyện thường thấy và gặp Tiêu Kiến Thành cũng không có gì là lạ. Những chuyện xảy ra trong công việc bị đối tác hay khách hàng trở mặt, lật lọng và còn phải hầu rượu, đôi khi tức đến trào máu nhưng cũng phải ngọt nhạt, khúm núm, hạ mình,... đó là mặt thực của xã hội, ra đời đi làm thì sẽ có cảnh như vậy. Còn về Tiêu Kiến Thành đối với đàn ông như vậy thì gặp mới mẻ như Tô Lạc cũng là chuyện thường, phụ nữ ăn nói sắc sảo, thông minh, biết tiến, biết lùi thì đàn ông càng thích và hứng thú. Cho nên mới thấy ngoài xã hội nếu chỉ là bình hoa di động thì sẽ chẳng bao giờ được đàn ông có địa vị như Tiêu Kiến Thành để mắt và theo đuổi, đàn ông có tiền thời buổi bây giờ kiêu ngạo bỏ ra, gặp dịp thì chơi. Anh là người làm ăn nên nhìn cách nói chuyện là hiểu ngoài đời nếu mà dây vào loại tiểu nhân như anh ta thì có nước chết không toàn thây. Đàn ông không thích phụ nữ thông minh nhưng lại thích phụ nữ biết cách ăn nói thông minh, hài hước và biết điều.

P/s: Nên nhìn theo khía cạnh thực tế của truyện rồi các bạn sẽ thấy ngoài đời cũng y như vậy.
5
138118
2015-06-09 01:52:25
--------------------------
198322
6004
Mình không thích cái bìa này tí nào nhưng vì đây là truyện chị GR dịch nên cũng ráng hốt em nó về đọc thử, ai dè nội dung cũng chẳng hay. Điểm duy nhất mình đánh giá cao ở truyện này là tính chân thật, đề cập khá sâu đến vấn đề tiền bạc. Tuy nhiên motip truyện cũ, tình tiết không nổi trội dẫn đến nhàm chán, nữ chính Tô Lạc tính cách cứ kỳ kỳ, giống như một cô gái không nói lý lẽ vậy. Tình cảm giữa Tô Lạc và Kiến Thành cũng biến chuyển nhanh quá, thành ra không hiểu tại sao họ từ oan gia đã quay sang yêu nhau nhanh vậy luôn.
3
393748
2015-05-19 18:41:44
--------------------------
178493
6004
Trước đây rất thích Tình yêu thứ ba nên đối với truyện này mình có kỳ vọng khá cao, tuy nhiên phải nói là thất vọng hơi nhiều. Phần đầu truyện tác giả viêt rất ổn nhưng đến cuối thì tình tiết khá nhanh và đuối, giống như tác giả thiếu ý tưởng nên cho kết thúc sớm ấy. Những đoạn đối đáp của Tô Lạc và Tiêu Kiến Thành khá hay, nếu phần cuối tác giả viết dài hơn, lí giải Tiêu Kiến Thành yêu Tô Lạc từ lúc nào thì truyện sẽ hay hơn nhiều, truyện này kết thúc viên mãn nhưng người đọc như mình vẫn thấy gượng, khác hẳn với Tình yêu thứ ba, tuy là kết thúc buồn nhưng mình rất hài lòng với cái kết ấy.
Mà nhân vật Tiêu Kiến Thành giống như là có bệnh không kiểm soát được hành vi hay sao ấy, nhiều lúc thấy anh này cường điểu hóa vấn đề quá :))
3
24079
2015-04-05 06:02:12
--------------------------
174816
6004
Theo mình nghĩ thỳ đây là câu truyện hết sức thật tế....Nó gần như phản ánh cuộc sống của chúng ta!
Nó là một câu truyện nhàm chán đối với mình...Hầu như trong truyện tác giả chỉ nói đến cuộc sống ,công việc , tiền bạc ,...của nam + nữ chính chứ không hề có sự lãng mạng , hay thứ tình cảm nào trong ấy! Đọc hết truyện rồi nhưng mình vẫn không cảm thấy một chút gì gọi là tình yêu của nhân vật chính đối với nhau....
Quyển đầu tiên đọc của '' Tự Do Hành Tẩu '' - thật sự thất vọng , chán !
2
445182
2015-03-28 22:21:38
--------------------------
172384
6004
Đây là một câu truyện khá buồn, một truyện tình cảm đi khá sâu về vấn đề tiền bạc cuộc sống. Tô Lạc là một nhân viên quỹ từ thiện bình thường, chăm chỉ làm việc, tích cực giúp đỡ các trẻ em nghèo. Từ khi gặp Tiêu Kiến Thành thì cuộc sống của cô  càng ngày rơi vào bế tắc. Đó là cái vòng luẩn quẩn của cuộc sống mưu sinh, công việc trắc trở, tình cảm gập ghềnh, gia đình lục đục. Các nhân vật như tưởng tốt đẹp  thì ra đều mang bộ mặt giả dối, đều xuất phát từ mong cầu lợi ích cho mình. Mình thấy rằng với truyện này, tác giả xây dựng hình tượng các nhân vật có đôi chút tiêu cực, sự miêu tả khắc họa về một cuộc sống suy đồi của đồng tiền, mọi chuyện đều luôn giải quyết bằng đồng tiền, cuộc sống này luôn bất công, mọi thứ chỉ đúng với người có quyền, có tiền mà thôi. Chính vì thế quyển này khiến mình đọc khá chán, nội dung có phần u buồn.
3
454196
2015-03-23 22:01:46
--------------------------
163076
6004
So với Tình yêu thứ ba thì Đừng vội nói lời yêu của TDHT đã xuống sức một cách ngạc nhiên đối với tôi. Dường như những điểm mà tôi thích ở Tình yêu thứ ba trong cuốn này đều không có. Cốt truyện cũ, nếu không muốn nói là quá bình thường, khiến cho tôi có cảm giác nhàm chán và phải cổ gắng để đọc hết truyện. Bên cạnh đó, việc thiết kế bìa kiểu này thực sự không đẹp mắt. Mình không hiểu ý nghĩa của bìa muốn nói gì. Nhìn chung, đây là một tác phẩm khiến mình rất thất vọng của một tác giả mà mình rất thích.
2
63376
2015-03-04 07:50:19
--------------------------
152147
6004
Thứ nhất là về bìa sách : không được đẹp mắt, không lôi kéo được sự chú ý của khách hàng, màu sắc thì không bắt mắt
Thứ hai: về nội dung cuốn sách, mình thì không mua sách, nhưng mình đã được đọc ké của bạn,  sự thật là mình đã quyết định không đọc hết nó. Cốt truyện này đã quá cũ được  hầu hết những cuốn tiểu thuyết bình thường đều sử dụng, mình còn có thể đoán trước được tình huống truyện, vì thế khi đọc có cảm giác rất nhàm chán . Ngoài ra, từ đầu truyện đến chỗ mình đọc và dừng lại ( mình đã nói là mình không đọc hết mà ) thì mọi tình tiết cứ bình bình như thế, cứ một đường thẳng tắp mà đi . Tóm lại ,với một người ăn rồi đọc tiểu thuyết như mình thì đây là một cuốn sách khá nhạt , không để lại ấn tượng trong lòng độc giả....có hơi hụt hẫng
2
539501
2015-01-22 12:15:38
--------------------------
148741
6004
Mình rất thích Tình Yêu Thứ Ba của Tự Do Hành Tẩu nên rất ngóng bộ này, ấy vậy mà khi đọc thì thấy nó hẫng không tả nỗi. Món ăn cũ và cũ mèn phát ngán. Mở đầu là mối oan gia của hai người, chuyện đấu giá, chuyện thi uống rượu, đấu võ mồm... sao mà nó cứ nhàn nhạt thế nào ấy! Cảm giác như đang ngủ gà ngủ gật rồi bị ai đó dập một phát bảo: hai đứa đã yêu nhau!

Thật tình mình chẳng mấy ấn tượng với hai nhân vật chính, cốt truyện đã vậy, hai anh chị cũng vậy theo. Chưa kể cái kết như bị kéo hẫng xuống vậy. Chán là chán!
3
382812
2015-01-11 13:14:55
--------------------------
147762
6004
Mình chọn cuốn này là bởi tên truyện nghe khá hấp dẫn dù bìa không được đẹp lắm, nhưng khi đọc truyện thì mình thấy truyện không được hay lắm, nó cứ nhan nhạt sao đó và diễn biến tình cảm giữa hai nhân vật chính cứ có chút gì khiên cưỡng, tính cách nhân vật cũng không có gì đặc sắc, đã thế có một số tình tiết còn khá vô lý và mang nặng tính sắp xếp chứ không được tự nhiên, cũng không có tình tiết cao trào. Tuy nhiên truyện cũng không đến mức quá dở vì dù sao mình cũng có thể đọc hết truyện mà không bỏ dở, nếu đọc để giết thời gian thì cũng ok.
2
17740
2015-01-08 19:59:07
--------------------------
140046
6004
Đọc một vài đoạn review của Đừng vội nói lời yêu thấy khá hay, Nhưng khi đọc cả truyện thì mình không thấy như vậy, truyện xoay quanh những cuộc cãi vã tranh luận của hai nhân vật chính là chủ yếu, cốt truyện khá rời rạc khiến mình chỉ muốn bỏ giữa chừng. Tự Do Hành Tẩu không miêu tả được hết nội tâm của hai nhân vật chính, thậm chí mình còn chả nhận ra hai người họ yêu nhau từ khi nào. Kết thúc lại càng không thích, đùng một cái Tiêu Kiến Thành bỏ hôn lễ bay đến chỗ Tô Lạc rồi hai người đoàn tụ trên trực thăng, diễn biến quá nhanh, đọc không kịp thấm. Truyện nên có ngoại truyện thì sẽ hợp lí hơn.
4
465856
2014-12-09 21:40:40
--------------------------
134784
6004
Tôi mua sách trước hết là do tên tuổi của tác giả Tự Do Hành Tẩu với tác phẩm "Tình yêu thứ ba" làm mưa làm gió một thời gian, tới nay lại còn được dựng thành phim. Tuy nhiên truyện mới của tác giả Đừng Vội Nói Lời Yêu lại khiến tôi không thích vì cốt truyện đơn giản, chủ yếu xoay quanh những cuộc tranh cãi của 2 nhân vật chính, nội tâm nhân vật chưa được bộc lộ rõ ràng nhất là đoạn Tô Lạc nhận ra tình yêu của mình với Tiêu Kiến Thành, đọc đến đây tôi mới ngỡ ra một điều "Thế cô ấy yêu từ lúc nào vậy?"...Kết thúc truyện theo tôi nhanh và có phần gượng gạo, chưa đi đến đâu. Việc Tiêu Kiến Thành bỏ dỡ hôn lễ của mình để bay một lèo tới cứu Tô Lạc cứ như là 1 trò chơi và thiếu hợp lý, rồi cả đoạn kết của 2 nhân vật phụ Dương Nhuệ và Thẩm Doanh lại là 1 dấu hỏi lớn, họ yêu nhau nhiều như vậy nhưng cuối cùng lại chia cắt nhau và không có một giải đáp rõ ràng cho số phận của 2 nhân vật này. Theo tôi truyện còn nhiều những uẩn khúc, những đoạn rời rạc làm cho tôi đôi khi cảm thấy nhàm chán, đúng như một số bạn nhận xét nếu như có thêm vài chương hoặc ngoại truyện nữa thì tác phẩm này sẽ thuyết phục độc giả hơn,
3
41370
2014-11-11 12:42:28
--------------------------
130912
6004
Quyết định mua Đừng vội nói lời yêu là do khá ấn tượng khi đọc những chương đầu của truyện. 
Truyện đọc được, khá nhẹ nhàng, không có tình tiết cao trào. Truyện viết theo ngôi thứ ba nhưng lại mang hơi hướng của ngôi thứ nhất. Toàn bộ câu chuyện là xoay quanh nhân vật Tô Lạc, những vấn đề trong cuộc sống của cô về giá đình, công việc và tình yêu. Vì vậy chưa lột tả được nội tâm của những nhân vật khác, đặc biệt là nam chính Tiêu Kiến Thành.
Kết thúc hơi vội, nếu có thêm 1, 2 chương hoặc thêm ngoại truyện nữa thì hay hơn. Nhưng có một chi tiết trong truyện mà mình rất thích, đó là lúc Tiêu Kiến Thành và Tô Lạc nói chuyện với nhau. Anh đã nói với cô đại khái là những người thân mà lâu nay cô luôn tin tưởng cũng có những chuyện lừa gạt, giấu diếm cô. Chi tiết này rất hay và ý nghĩa.
3
416570
2014-10-20 23:47:22
--------------------------
130592
6004
Đầu tiên nói về Tiêu Kiến Thành - anh trẻ, có tiền và có quyền, mọi thứ đối với anh đều đạt được quá dễ dàng nên khi gặp phải Tô Lạc thì anh bị cá tính quật cường của cô thu hút và dù anh có bao nhiêu thủ đoạn, cô cũng không chịu thua nên anh càng muốn chinh phục -> 1 motip quá cũ
Còn về Tô Lạc, thật sự mình không thích, công việc của cô như ban đầu được nói là người bán nụ cười để xin tài trợ, quyên góp cho Quỹ từ thiện nghĩa là khả năng giao tiếp và ứng xử của cô phải giỏi. Thế mà sau khi Tiêu Kiến Thành giúp cô nâng giá 1 món đồ cổ lên xong xin số điện thoại của cô thì bi từ chối thẳng thừng lí do : tôi ko hứng thú với anh. Sau đó mọi chuyện xảy ra, cô chửi bới, cô không giữ mồm miệng, không có trường hợp nào cô thể hiện được kĩ năng giao tiếp khéo léo mà bản chất công việc cô phải có
Đúng như Tiêu Kiến Thành đã nói, anh có lúc muốn buông tha nhưng cô cứ lững lờ rồi như gợi ý cho anh cơ hội .. Cô muốn anh giúp thì thẳng thắn ngay từ đầu 1 lần, cứ cái kiểu "băng thanh ngọc khiết" muốn người ta bỏ tiền ra giúp xong quay đầu bỏ đi 1 cách tự tin. Tiền mà cho vào quỹ từ thiện xây trường thì 1 đi không trở lại, anh là 1 thương nhân, không lí do gì anh phải bỏ 1 số tiền lớn để làm mà ko có lợi ích như anh đã nói bao nhiêu lần. Thế mà cô cứ bám theo anh như anh nợ cô, là mình thì mình cũng nổi điên chứ nói gì đến Tiêu Kiến Thành, anh trừng phạt 1 người không biết điều như cô chẳng có gì là sai
2
373747
2014-10-18 20:06:28
--------------------------
129837
6004
Sau khi đọc xong đừng vội nói lời yêu tôi có thể kết luận truyện không hay mà cũng không dở . Nó giống như một món ăn mà ngày nào chúng ta cũng ăn cần them một chút gia vị hay cái gì đó để làm nó đột phá rực rỡ . Tô Lạc và Tiêu Kiến Thành là một đôi oan gia đúng nghĩa . Tuy không phải đến mức gặp nhau là đánh nhau nhưng hai người sẵn sàng bày tỏ công khai thái độ thù địch đối với đối phương . Chuyện sẽ chả ra sao nếu như có một ngày Tô Lạc phải nhờ vả kẻ thù truyền kiếp của mình . Tôi rất hiểu cái cảm giác phải nhẫn nhịn cầu xin ấy của Tô Lạc . Truyện thiếu cái thứ gọi là các chi tiết cao trào . Mạch truyện diễn ra đều đều khá chậm khó tránh khỏi làm người đọc buồn chán .
3
337423
2014-10-13 08:14:30
--------------------------
129643
6004
Mình cực kỳ thích tác phẩm của Tự Do Hành Tẩu, với cách viết thực tế, rất gần gũi với cuộc sống nên luôn đem lại sự đồng cảm cho người đọc. 
Đặc biệt nhân vật nữ trong tác phẩm của cô luôn là những con người mạnh mẽ, kiên cường, luôn nghĩ cho người khác.
Tô Lạc cách nói chuyện không biết sợ trời sợ đất, luôn thấy bất bình ra tay giúp đỡ người khác, chuyện của người khác cũng có thể coi như chuyện của mình mà tận tình ra mặt giúp. Quả thật nhân vật này cực kỳ thích hợp làm ở hội từ thiện. Có thể bởi chính tính cách của cô, luôn bênh vực đứng về kẻ yếu, biết cảm thông cho họ, mà không so đo tính toán, đã làm lay động nam chính Tiêu Kiến Thành. Anh là một con người kỳ lạ, tìm mọi cách tiếp cận cô, yêu cô nhưng luôn chọc tức cô, lại là con người thủ đoạn hết lần này đến lần khác trêu ghẹo cô, ép cô đến đường cùng phải cầu xin anh, chấp nhận ở bên cạnh anh. Cách anh quan tâm cô cũng cực kỳ khác thường, con người luôn ngang tàng như thế, không ngờ lại yêu cô đến ngay ngốc, khi bỏ rơi cô còn không nỡ, mong muốn cô tìm một người đàn ông khác, để mình mới có thể yên tâm mà quên được cô. Câu chuyện của họ thật đẹp và kết thúc có hậu khiến mình rất hài lòng.
Ngoài ra, bookmark & poster của Amun rất đẹp rất chất lượng, kẹp giấy cũng rất dễ thương.
5
38403
2014-10-11 20:09:53
--------------------------
