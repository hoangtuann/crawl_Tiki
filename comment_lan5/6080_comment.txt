456165
6080
Đúng là tuy cốt truyện đơn giản nhưng đọc xong tập 1 là cứ muốn đọc tiếp tập 2,sang tập 2 các tình tiết đã trở nên hấp dẫn hơn,nhiều nhân vật với những tính cách khác nhau xuất hiện giúp cho truyện trở nên thú vị hơn.Truyện này không thể đọc nhanh được mà phải chậm rãi thưởng thức mà công nhận mình khâm phục tác giả thật từng câu chữ từng lời trong truyện đều chứa đầy ý nghĩa,các đoạn đối thoại giữa các nhân vật rất hay.Dịch giả cũng làm rất tốt khi có thể truyền tải hết được những điều tác giả muốn nói qua tiếng việt,đọc mà thấy mê luôn,một cuốn sách hay và đáng để sưu tầm.
4
526322
2016-06-23 10:49:54
--------------------------
407784
6080
Lời văn của Thomas Mann thật sự rất thú vị, và phải nói rất rất dí dỏm, hoàn hảo về ngữ pháp, ngôn từ, không có gì là thừa thãi cả! Thật sự khâm phục tài năng viết văn của tác giả, không hổ là 1 nhà văn lớn của thế giới, và cũng phải cám ơn dịch giả nữa! thật hoàn hảo cho 1 tác phẩm như thế, cho từng câu văn dài mà đẹp như thế! Nếu bạn đọc mỗi ngày và thật sự suy nghĩ về những ý tưởng của tác giả, nó thật sự thú vị và muốn đọc thêm nữa! Dù 3 tuần đầu của Hans Castorp ở sơn trang thật sự dài nhưng đều có mục đích và giá trị về thời gian với cả người đọc. Và tất nhiên cần 1 chút kiên nhẫn đối với những ai không có tính kiên nhẫn lắm, sau đó mỗi trang sách thật sự là những điều mới mẻ và tuyệt vời! :) mình muốn mua ngay tập 2 này! mua ngoài đắt hơn nhiều mà k biết trên tiki có chất lượng k nữa!
5
1274156
2016-03-30 18:30:42
--------------------------
389162
6080
So với tập 1, tập 2 Núi thần hay hơn, nhiều diễn biến kịch tính hơn, tác giả có phần hài hước hơn và tạo được nhiều xúc cảm hơn. Tập 2 xuất hiện các nhân vật mới có tính cách độc đáo hơn do đó tạo nên sự đa dạng và mang lại nhiều màu sắc hơn cho phong cảnh 'quanh năm tuyết phủ'. Chính sự đa dạng hơn ở tập 2 này nên lại càng yêu hơn nhân vật chính của Núi thần, do có nhiều nhân vật mới xuất hiện tạo nên nhiều tình huống mới nên càng làm tính cách của Hans nổi bật, vẫn ngay thẳng, hào hiệp và có phần hơi hồn nhiên nhưng vẫn là nhân vật được tác giả ưu ái hơn cả

5
554079
2016-03-01 14:37:19
--------------------------
241914
6080
Đây là phần tiếp theo của Núi thần tập 1, cho nên phải đọc tập 1 thì mới hiểu diễn tiến của tập 2 này. Vẫn tiếp tục là câu chuyện về chàng Hans Castorp, với các nhân vật khác trên Sơn trang lạnh lẽo, buốt giá quanh năm, một nơi mà khái niệm thời gian được tính khác lạ so với ở mặt đất. Kết thúc truyện khiến mình hơi chút buồn và có phần gì đó ám ảnh khi đọc xong truyện này. Những triết lí sâu sắc được Thomas Mann ẩn giấu dưới ngòi bút tài tình, điêu luyện của ông sẽ khiến mình suy ngẫm thêm nữa về quyển sách này trong một thời gian.
Chất lượng giấy in tốt, trang có vẻ mỏng hơn so với tập 1 nhưng vẫn thật sự rất dày. Sách chỉ thích hợp ngồi để đọc chứ nằm đọc thì hơi khó.
4
125876
2015-07-25 22:09:54
--------------------------
160631
6080
Phải nói rằng bộ sách này thật sự quá hay dù cốt truyện cũng chẳng có gì mới mẻ đáng để nói đến. Thế nhưng khi đọc nó mình chỉ có thể đọc một vài chương mỗi ngày, và dành tí thời gianđể suy nghĩ chút về những triết lý của Thomas Mann. Ông đã đưa ra những lập luận rất khác người về tình yêu, cuộc sống, âm nhạc, v.v. nghe tưởng chừng như vô lý nhưng người đọc không cách nào phản bác ngay được. Mình thích tất cả những đoạn hội thoại qua lại cũng như những đoạn miêu tả cuộc sống ở Sơn Trang hay những đoạn phân tích tâm lý của Hans Castorp. Giọng văn của truyện cũng rất hài hước không hề nhàm chán đối với một tác phẩm được xuất bản gần cả trăm năm về trước. Đó cũng là nhờ phần lớn dịch giả Nguyễn Hồng Vân đã rất xuất sắc trong việc biên dịch tác phẩm này.
5
116202
2015-02-25 08:47:22
--------------------------
