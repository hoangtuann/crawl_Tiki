300867
7865
Bé nhà mình rất yêu thích cuốn sách "Chuyện kể cho bé trước giờ đi ngủ - Một cộng hai bằng mấy?". Kể từ lúc đặt hàng bé lúc nào cũng háo hức chờ sách mới và liên tục hỏi bao giờ thì sách về. Đặt hàng qua Tiki quả là tiện lợi, không cần mất công nắng mưa ra hiệu sách rồi lục tung đống sách thiếu nhi để tìm mà vẫn mua được cuốn sách ưng ý, lại yên tâm sách chất lượng tốt, không phải sách giả, giá cả lại rất dễ chịu vì luôn được giảm giá. Giao hàng cũng rất nhanh, không cần chờ đợi lâu.
4
29716
2015-09-14 11:47:59
--------------------------
