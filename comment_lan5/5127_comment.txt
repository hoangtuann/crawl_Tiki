471396
5127
Sách có chất lượng giấy tốt, hình ảnh đẹp, nội dung dễ hiểu. Lúc đầu bé tè dầm, nhưng đổ lỗi cho các bạn của bé, nhưng ba mẹ không la mà chỉ dặn dò các bạn của bé, cho đến khi bé tự nhận ra là do mình làm ướt giường, ba mẹ cũng không la mà chỉ dặn dò bé đi toilet trước khi đi ngủ và bé không làm ướt giường nữa. 
Bé nhà mình rất thích truyện này, cứ đọc đến “mẹ không giận bạn à” hay “ba không giận bạn à”, là bé trả lời “Không đâu”.  Sách giúp bé nhận thức về việc đi toilet trước khi đi ngủ và dũng cảm nhận lỗi của mình, và quan trọng là ba mẹ không la bé, mà chỉ dặn dò, nhắc nhở bé giúp bé mạnh dạn nhận lỗi. 

5
234400
2016-07-08 14:56:23
--------------------------
431845
5127
Cuốn sách kể về một cậu bé hay tè dầm và luôn tìm cách đổ lỗi cho các bạn khác như: voi, mèo, cá heo...Sau một thời gian bé tự hiểu ra và đẫ biết nhận lỗi với mẹ, từ đó cũng không tè dầm nữa.Sách có tính giáo dục cao, lối viết nhẹ nhàng, dễ hiểu, phù hợp cho các bé.Sách cũng dạy cho bố mẹ biết cách ứng xử và giải quyết khi con yêu mắc lỗi. Hình vẽ minh họa sinh động.Chất liệu giấy dày, đẹp.Rất đáng để mua quyển này cho con giai 2 tuổi rưỡi của tôi.
4
897832
2016-05-18 08:25:36
--------------------------
428010
5127
Lúc trước khi bé còn ngồi chưa vững, mình đã mua quyển này về cho bé " gặm" rồi. Bé cực thích quyển này ạ, hình ảnh đáng yêu, lại có cả mèo, cả voi, lại có chuyện tè dầm trên giường giống y bé. Khi đọc sách cho bé mình hay minh họa hoặc thay đổi điệu bộ giọng điệu theo y như hình ảnh cậu bé nhỏ trong truyện khi đổ lỗi cho các bạn thú bông tội tè dầm. Bé thích thú lắng nghe quyển sách đầu tiên mẹ mua cho, và mình cũng có ấn tượng sâu sắc đến quyển sách này. 5 sao.
5
533458
2016-05-10 14:18:36
--------------------------
256735
5127
Hình ảnh đẹp, bắt mắt cùng nhiều câu thoại ngắn kể về một em bé, cứ mỗi sáng thức dậy thì giường của bé lại bị ướt. Hết lần này đến lần khác, em bé đỗ lỗi cho bạn gấu, cho cá voi, cho thỏ, cho rùa và cho cả mèo nữa. Mỗi lần như thế bé đều nhận được những lời dịu dàng và ân cần từ bố mẹ. Đến ngày thứ 7, không còn đỗ lỗi cho ai nữa mà bé đã ngượng ngùng nhận lỗi về mình. Bé thật dũng cảm và đáng yêu làm sao. Quyển sách rất bổ ích. Cám ơn tiki.
5
110777
2015-08-07 09:12:45
--------------------------
253077
5127
Truyện kể về em bé tè dầm và nghĩ ra mọi cách để đổ lỗi cho người khác. Nhưng cuối cùng đã kịp hiểu ra và nhận lỗi của mình. Sách vẽ hình đáng yêu và màu sắc tươi sáng. Tôi đã sử dụng câu chuyện này làm bài học giáo dục con trai độ tuổi lên 3 vẫn còn tè dầm của tôi. Sau này, bé học theo em bé trong truyện biết tự nhận lỗi " mẹ ơi, sáng nay con thức dậy thấy quần áo và giường mình ướt sũng mẹ ạ, chắc là tối qua con đã tè dầm rồi" Đó là giá trị mà cuốn sách đã mang lại cho hai mẹ con tôi.
5
478786
2015-08-04 09:36:06
--------------------------
249017
5127
Sách có tính giáo dục cao, lối viết nhẹ nhàng, dễ hiểu, phù hợp cho các bé. Hình vẽ minh họa sinh động, thu hút. Chất liệu giấy dày, không bị nhàu nát nên các bé có nghịch chút chút thì sách vẫn đẹp. Mình hay đọc cho bé cuốn này và bé rất thích, học thuộc lòng một số đoạn nữa. Một điểm cộng cho Nhã Nam là phân chia sách theo đô tuổi phù hợp nên các phụ huynh không mất quá nhiều thời gian để tìm hiểu nội dung sách trước khi mua. Cảm ơn Nhã Nam và Tiki
4
351793
2015-07-31 12:01:22
--------------------------
215665
5127
Truyện  Ai làm giường ướt? đã làm tốt được mục tiêu đề ra là giúp các em học được cách học cách nhận lỗi do mình gây ra. Hình vẽ dễ thương, màu sắc cũng rất dễ nhìn, không bị chói mắt. Tác giả cũng hiểu được tâm lý của trẻ em, đồng thời cách cư xử của bố mẹ cũng rất đáng để mình học hỏi. Mình rất thích bộ truyện này. Mình sẽ mua đủ bộ cho con khi bé đến tuổi mẫu giáo. hiện nay con mình chỉ được nghe cho vui chứ chưa hiểu được nội dung câu chuyện.
4
284620
2015-06-26 16:57:26
--------------------------
154064
5127
Mình mua cuốn truyện này cho bé nhà mình 3 tuổi và bé rất thích. Hình ảnh truyện đẹp, nội dung đơn giản, dễ hiểu. Truyện kể về một bạn bé tè dầm các ngày trong tuần, vì sợ bố mẹ mắng nên từng ngày bạn đều đổ lỗi cho các con thú bông mà bạn có. Lần đầu tiên mình đọc truyện cho bé nhà mình, bé cũng tưởng các bạn thú làm ướt giường bạn bé, sau đó mình hỏi bạn thú bông bé xíu có tắm được trên giường không, có uống nước được không, có đùa nhau té nước được không... thì bé mới hiểu ai là người làm ướt giường, khá thú vị! Cuốn truyện còn cho thấy bố mẹ bạn bé rất nhẹ nhàng, bao dung từ từ hướng dẫn để bạn bé tự biết nhận lỗi, không còn nói dối nữa, cũng rất đáng để cho các bậc cha mẹ như chúng ta học tập!
5
336914
2015-01-28 11:24:11
--------------------------
151888
5127
Cuốn sách kể về một bạn nhỏ tè dầm nhưng vì sợ bố mẹ la nên đã nghĩ ra nhiều nguyên nhân lý giải tại sao giường ướt. Cuốn sách dạy bé biết nhận lỗi và sửa sai, chứ không nên đổ lỗi cho người khác. Từ ngữ của sách dễ hiểu, hình vẽ dễ thương, màu sắc hài hòa. Mình nghĩ bố mẹ nên mua sách này về để vừa kể chuyện cho bé, vừa giúp bé học được nhiều bài học bổ ích để trở thành bé ngoan. Bé nhà mình rất thích cuốn sách này và đã nhớ ngay một số từ đơn như “ bé, bố,mẹ” khi mình đọc sách này cho bé.
4
15022
2015-01-21 13:43:46
--------------------------
