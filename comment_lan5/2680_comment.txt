389275
2680
Trong thời đại ngày nay, khi sản phẩm hàng hóa quá nhiều và công nghệ cũng gần như tương đương nhau, ít khi có những sản phẩm đột phá như iphone thì lợi thế cạnh tranh để các công ty cạnh tranh với nhau trên thị trường là gì? chỉ có thể là dịch vụ. Và dịch vụ lại ngày càng trở nên quan trọng, đôi khi là yếu tố quyết định để khách hàng chọn lựa sản phẩm. Ví dụ như khách hàng sẽ quyết định mua hệ thống wifi khi có cả một hệ thống bảo trì, bảo hành đằng sau.
4
465332
2016-03-01 19:22:27
--------------------------
246420
2680
Với cuốn sách bán chạy nhất mới của mình "Nâng tâm dịch vụ," Ron Kaufman đã đưa những bài học thiết thực, phù hợp và hành động của mình đến một cấp độ mới tuyệt vời. Từ cách tiếp cận học tập được thực hiện bởi chuyên gia chăm sóc khách hàng , Kaufman viết lên bài học của mình thông qua cả kinh nghiệm và ngụ ngôn, thực hành tốt nhất và trường hợp xấu nhất, thực tế và cũng dễ hiểu, kịch bản đáng nhớ mà làm cho bài học trở nên sống động. Và không phải là điểm chính của bài học được ghi nhớ để họ có thể bị tác động? Ông là một bậc thầy kể chuyện,giúp độc giả thấu hiểu không chỉ là ý nghĩa mà còn hậu quả của việc hành động và không hành động.
5
65620
2015-07-29 18:02:32
--------------------------
