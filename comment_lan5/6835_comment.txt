519825
6835
Mình ít đọc văn nước ngoài nên khá dũng cảm khi mua quấn này. Cứ cầu trời khấn phật cho nó hay và may sao, nó hay thật. Nhẹ nhàng nhưng thấm rất nhanh. 40k là quá rẻ cho quyển này
5
704324
2017-02-06 15:25:18
--------------------------
473719
6835
Thật sự là như vậy đấy! Tác phẩm tuy dành cho thiếu nhi, nhưng thông điệp nó mang lại thì rất tình người, nhân văn, và nó khiến suy nghĩ, tâm hồn mình cảm giác hạnh phúc và yêu đời hơn. 
Dù là mình đã coi phim này trên HBO, nội dung thì quá chất rồi. Rất hay! Nhưng mình vẫn muốn đọc tác phẩm này, để cảm nhận đầy đủ những giá trị mà tác giả truyền tải. Quả thật là đọc truyện này, cho mình cảm giác khác hẳn so với coi phim. Li kì, thú vị và cách thức cảm nhận giá trị! 
1 tác phẩm cần có trên kệ sách của mình!
5
65325
2016-07-10 20:53:58
--------------------------
464460
6835
Đây là câu chuyện nhẹ nhàng nhưng sâu sắc nói về tình bạn của một chú lợn con và một cô nhện thông minh. Quyển sách này không chỉ thích hợp với các bé từ tiểu học trở lên mà với người lớn như mình cũng rất mê nữa. Một câu chuyện hư cấu về tình bạn của các loài vật quanh ta, cho chúng ta thấy không chỉ con người mới có tình cảm mà ngay cả các loài vật bé nhỏ cũng có tình cảm vậy. Con gái mình rất thích cuốn sách này và sau khi đọc truyện bé thường đòi mẹ mua cho 1 chú lợn con để chăm sóc giống như trong sách vậy.
4
1066198
2016-06-30 10:15:38
--------------------------
452849
6835
Mình mua quyển sách này lúc đầu chỉ là nghe giới thiệu qua loa, rồi trông thấy bìa ấn tượng rồi thêm vào giỏ hàng chứ trước lúc mua không hề có kỳ vọng gì đặc biệt. Ấy thế mà, đây lại là một quyển sách hết sức ý nghĩa. Một quyển sách hết sức xúc động về một chú lợn xuân tên là Wilbur và có ai lại tin được rằng chú đã được một chị nhện mang tên Charlotte cứu sống. Những tình bạn tuyệt vời như giữa Fern với Wilbur, Wilbur với Charlotte. Ai cũng nên đọc quyển sách này, ít nhất là một lần trong đời.
5
502455
2016-06-20 21:08:46
--------------------------
428409
6835
Câu chuyện xoay quanh tình bạn rất đẹp giữa một chú lợn Wilbur và một cô nhện Chattlote. Chú lợn Wilbur đáng yêu  thoát chết một cách kỳ diệu nhờ sự giúp đỡ cực kỳ tận tâm của nhện Chattlote. Truyện rất thích hợp đọc cho các bé, kể cả các bé còn ở  trong bụng mẹ vì mình có thói quen mua rất nhiều truyện để đọc cho con. Mình đã đọc ebook trước rồi và thấy rất hay nên  mua luôn sách để dành đọc con nghe. Tuy nhiên phiên bản giấy này lời xưng hô giữa 2 con vật là "chị- tôi" không hay bằng " cậu- tớ" trong phiên bản ebook trên mạng.
4
1177409
2016-05-11 10:12:22
--------------------------
382003
6835
Đây là một trong những quyển truyện ba mẹ con tôi thích nhất. Câu chuyện giữa các con vật ở nông trại thật thú vị, và tác giả với lối kể chuyện hấp dẫn của mình đã dụ dỗ các bé ngay từ những trang đầu tiên. Các bé có thể cảm nhận qua câu chuyện tình cảm giữa người và người, giữa người và vật nuôi, giữa những con vật. Truyện có những chi tiết gây cười cũng nhưng những chi tiết cảm động đến rơi nước mắt. Tôi rất hài lòng vì đã mua sách này cho mình và các con.
5
339092
2016-02-18 13:54:09
--------------------------
366023
6835
Điều khiến mình ấn tượng đầu tiên đối với quyển sách nhỏ này là minh họa khá đẹp, bookcare của tiki rất tốt. Đây là câu chuyện kể về tình bạn cảm động giữa chú lợn xuân Wilbur và chị nhện xám Charlotte trong trang trại nhà Zuckerman. Lời kể của E.B.White hóm hỉnh, sinh động và hài hước. Qủa thực cuốn sách này rất xứng đáng   là một trong những tác phẩm kinh điển của thiếu nhi Mỹ. Điểm trừ khiến mình chưa hài lòng lắm là giấy không trắng và đẹp như những cuốn sách khác của Nhã Nam
4
320340
2016-01-09 08:48:15
--------------------------
361883
6835
Đây là một trong những cuốn sách tôi đánh giá cao nhất trong số sách thiếu nhi đã đọc về tính nhân văn. Tác giả thể hiện tình yêu với động vật qua tác phẩm này, cũng như trong Chiếc Kèn của Thiên Nga, Nhắt Stuart. Nó gây xúc động vì tình thương mà wibur nhận được từ cô bé Fern, cô nhện Charlotte hay chú chuột. Yêu thương chỉ vì yêu thương mà không cần được đáp lại hay phải cùng loài giống. Nó xứng đáng là món quà tuyệt vời dành tặng cho thiếu nhi về bài học về tình người. Bìa đẹp, được bọc một lớp bìa khác ra ngoài và minh họa rất kĩ càng. Sách hoàn hảo
5
23764
2015-12-31 20:22:44
--------------------------
352086
6835
1. Tên sách: Phù hợp với nội dung của truyện. 
Đánh giá: 5 đ
2. Thiết kế: Bìa sách đẹp, màu xanh dễ thương, màu sắc tươi sáng. Truyện có nhiều ảnh minh họa đẹp.
Đánh giá: 5 đ
3. In ấn: Giấy tốt, mịn. In ấn ổn, phông chữ to, dễ đọc.
Đánh giá: 5 đ
4. Nội dung: Truyện kể về cuộc sống của chú lợn xuân Wilbur ở trang trại nhà Zuckerman. Chú có cuộc sống thanh bình, vui vẻ, náo nhiệt. Mọi chuyện vẫn tốt đẹp cho đến khi Wilbur biết được mình sẽ bị giết lấy thịt khi mùa đông tới. Và rồi Wilbur được cứu sống nhờ trí thông minh của cô nhện Charlotte. Một câu chuyện dễ thương, vui tươi, đầy sức sống thông qua cách miêu tả cuộc sống của các con vật ở trang trại nhà Zuckerman. Truyện đề cao sự chân thành, hy sinh trong tình bạn.
Đánh giá: 5 đ
5. Nhân vật: 
Wilbur: Ngây thơ, chân thật, dễ thương.
Charlotte: Thông minh, nhân ái, chân thành, tận tụy.
Templeton: Xảo quyệt, ích kỷ.
Fern: Yêu động vật và có thể nghe chúng nói chuyện.
Đánh giá: 5 đ
Tổng điểm: 25/5 = 5 đ

5
934783
2015-12-13 17:01:17
--------------------------
345446
6835
Mình là một fan cuồng các tác phẩm văn học cổ điển và văn học thiếu nhi, mới nhìn qua bìa Charlotte và Wilbur là mình chấm "ẻm" liền rồi, bìa nhìn rất dễ thương, và đọc bình luận của các bạn bên dưới thì mình không đắn đo gì nữa và đặt luôn :))
Về hình thức thì Nhã Nam luôn khiến mình hài lòng về chất lượng sách, giấy mịn, đẹp, chữ in rõ ràng... Duy có một điều này thôi là mình ước sách làm bìa bình thường chứ không làm bìa rời. Mình không dùng bookcare nên bìa rồi đọc sách nhiều lúc cứ xê dịch rất khó chịu. Ngoài điểm này ra thì mình hài lòng với tất cả :D
5
393341
2015-11-30 15:18:26
--------------------------
338925
6835
Charlotte Và Wilbur là một câu chuyện thuần khiết dành cho trẻ con. Nó có tất cả những gì cần thiết cho một câu chuyện thiếu nhi: thế giới tưởng tượng hồn nhiên, sự tò mò và hài hước, tình bạn và những cuộc phiêu lưu. Nhưng sâu đậm và ý nghĩa nhất là bài học về sự mất mát và trưởng thành.

Wilbur đã từng là một chú lợn ngây thơ và giản dị, cuộc sống không gì nhiều hơn chuyện ăn và trò chuyện cùng bạn bè. Khi lên tới "vinh quang" lớn nhất của cuộc đời, chú cũng không hề tỏ ra ngạo mạn và hợm hĩnh với mọi người. Nhưng chú đã dũng cảm đối mặt với hiện thực, đã cố gắng hết mình để mang những quả trứng của cô bạn Charlotte về nhà, chăm sóc chúng và coi chúng như con của mình.

Hình ảnh những chú nhện con tung những sợi tơ để bay lên bầu trời sau khi nở ra là một hình ảnh chân thực và cảm động. Cô bé Fenn đã không còn tới ngắm nhìn những con vật mỗi ngày, như bao người lớn khác. Tất cả rồi đều phải lớn lên, đi tìm con đường của cuộc đời mình. Nhưng rất nhiều thứ vẫn còn ở lại, đó là tình bạn và sự quan tâm lẫn nhau. Và chúng làm ta hạnh phúc.

Mình đặt sách rất nhiều lần trên Tiki, nhưng không hiểu sao trong đơn hàng lần này có 2 cuốn (Charlotte Và Wilbur, Chuyện dài bất tận)  có chất lượng tệ so với sách mới. Giấy có biểu hiện cũ, có vài nếp gấp và vết bẩn, giống như sách trưng bày và bảo quản không tốt. Hi vọng sự việc như thế không xảy ra một lần nữa.
4
532620
2015-11-17 08:31:26
--------------------------
324431
6835
Mình mua sách này vì nó là truyện thiếu nhi (mình khoái đọc truyện thiếu nhi dù đã qua 20 tuổi) và cái bìa gây tò mò đối với mình.
Khi mới đọc thì mình thấy không hấp dẫn lắm, nhưng càng đọc càng không dứt ra được, nhất là khi chị nhện Charlotte xuất hiện và những lần chị quyết định giăng chữ lên mạng nhện, những con chữ lần lượt xuất hiện làm mình khá bất ngờ, bất ngờ hơn khi Wilbur cố tỏ ra cho xứng đáng với ý nghĩa con chữ đó. Thật thú vị!
"Charlotte và Wilbur" cho mình nhiều hơn một câu chuyện, nó ý nghĩa và sâu sắc hơn mình nghĩ và nó khiến mình đã phải đọc đi đọc lại tới 3 lần từ lúc nhận sách tới giờ.
Cảm ơn Tiki nhé!
5
545308
2015-10-21 09:48:54
--------------------------
311837
6835
Cuốn sách tôi được đọc từ rất lâu và vì quá hay nên phải tự thưởng một cuốn sách trong nhà. Đây là một tác phẩm xứng đáng được bình chọn là một trong những tác phẩm thiếu nhi kinh điển của mọi thời đại. Lời văn trong trẻo, súc tích và vui tươi làm cho người đọc luôn được mải mê trong trạng thái vui vẻ và thú vị. Cốt truyện đơn giản nhưng được biến hóa một cách tài tình. 
Không chỉ trẻ con mà cả người lớn cũng có thể hòa mình vào từng trang sách hài hước và ý nghĩa này. 
5
190232
2015-09-20 11:11:32
--------------------------
306297
6835
Mình rất thích quyển này. Là một câu chuyện thiếu nhi hài hước, dễ thương và tình cảm. Nói về tình cảm giữa con người mà động vật dành cho nhau. Dù chúng không cùng loài với chúng ta nhưng khoảng cách của cả hai rất gần gũi và thân thương. Giọng văn mềm mãi, xém một tí trẻ con nhưng đơn giản, ngây thơ. Giọng điệu nhẹ nhàng, ngọt ngào tan chảy trái tim của người đọc. Wilbur là một chú lợn đáng yêu, vượt qua nhiều khó khăn. Bìa sách dễ thương, tinh tế, đáng để bạn mua đọc ;)
5
504577
2015-09-17 14:53:17
--------------------------
304602
6835
Bị hớp hồn ngay từ khi nhìn thầy bìa của cuốn sách này. Khi lấy hàng mở ra, sách có bìa ở ngoài có màu, khi tháo lớp bìa này ra, thì bìa trong màu trắng, thiết kế đơn giản mà đẹp mắt. Quả thực đây là một cuốn sách hay, và nội dung không thể chê đi đâu được, cốt truyện cảm động và mang tính nhân văn sâu sắc. Sở dĩ mình mua nó vì mình vốn rất thích đọc các tác phân văn học nước ngoài viết cho thiếu nhi, chúng đều có một nét gì đó ngây thơ, giúp thoải mái tâm hồn nhưng có tính giáo dục cao. Sách lấy thế giới của các con vật ra làm đề tài như bao cuốn sách biết cho thiếu nhi khác, nhưng nội dung cuốn hút vô cùng, xoay quanh tình bạn đẹp giữa nhên Charlotte và lợn Wilbur. Hãy đọc và thưởng thức nó nhé!
5
205891
2015-09-16 15:24:13
--------------------------
303190
6835
Phải nói đây là quyển văn học thiếu nhi về nhân cách hóa động/thực vật mà mình thích nhất trong tất cả các quyển mà mình đã đọc qua. E.B.White phải thật sự nhập tâm vào từng nhân vật, biến hóa thành từng tính cách, không những của người lớn mà của cả con trẻ để có thể dựng lên một câu chuyện tuyệt vời như thế. Giọng văn không khoa trương, không thậm xưng mà đơn giản, ngây thơ và trong veo, các tình huống được sắp xếp thật tài tình, cứ thế các bài học về tình bạn, tình yêu thương tuôn chảy nhẹ nhàng. Một chút cao trào làm cho câu chuyện thêm phần thú vị. Ngoài ra bìa sách Nhã Nam in rất đẹp, giấy trắng, dày tạo nên giá trị không nhỏ cho tác phẩm.
5
380464
2015-09-15 18:27:03
--------------------------
300779
6835
Câu truyện thiếu nhi rất hay ho mà mình được đọc. Truyện kể về chú lợn Wilbur cực kì. Đáng yêu, luôn lạc quan và biết vượt lên mọi khó khăn. Câu truyện làm mình thấy rất yêu cô bé "Người", yêu động vật, chăm chút cho chú lợn. Mọi trẻ em sinh ra đều nhân hậu vì vậy mà người lớn cần nuôi dưỡng những tâm hồn nhân hậu ấy. Hãy đọc để biết yêu thương trẻ em, động vật và thấu hiểu tâm lý trẻ thơ để mở lòng hơn với chúng. Cô nhện Charllote như một người chị cả trong cái chuồng heo đó. Đã hi sinh bản thân mình vì chú lợn Wilbur làm mình liên tưởng đến những người chị cả trong gia đình. Luôn chăm chỉ làm việc, nhịn ăn nhịn mặc để dành cho những đứa em. Câu truyện làm mình thấy thêm yêu cuộc sống này!
4
329267
2015-09-14 10:40:26
--------------------------
300766
6835
Một câu chuyện trong veo và giản dị, nhưng đẹp đến ngỡ ngàng. Từng trang sách lật mở, tôi đã bất ngờ, đã cười và đã đã khóc. Ở trong tác phẩm tưởng như nhỏ bé đó, có sự hi sinh, sự thủy chung và những bài học vô cùng giá trị. Tôi khâm phục chị nhện Charlotte với một trái tim chan chứa yêu thương, tôi thích lợn Wilbur bởi sự ngây thơ và chân thật. Và tôi cám ơn tác giả E.B.White đã mang đến cho nền văn học thế giới một tình bạn đẹp tuyệt vời đến thế. 
5
77191
2015-09-14 10:35:44
--------------------------
299176
6835
Wilbur - sinh ra thiệt thòi nhưng lớn lên trong tình yêu thương. Cậu ta là một chú lợn kẹ và sự sống gần như đã được định đoạt khi vừa mới lọt lòng mẹ. Charlotte - sinh ra là sát thủ nhưng lại mang trái tim của một thiên thần. Một cô nhện luôn phải giăng bẫy bắt mồi vì đó là nghề gia truyền và cũng là công việc nuôi sống cô hàng ngày. Trái ngược với ngoại hình và công việc, cô luôn hết mình giúp đỡ bạn bè cho đến hơi thở cuối cùng mà không mong muốn nhận lại gì. Tên truyện chỉ nhắc đến hai con vật, nhưng trong tuyện còn một con vật nữa góp phần không hề nhỏ cho thành công của truyện. Đó chính là Templeton - một con "chuột ra chuột". Tác giả đã lột tả khá thành công hình ảnh của Templeton đầy tính toán, nhưng cũng chẳng kém phần ngờ nghệt và dễ bị lừa. Charlotte và Wilbur là một tác phẩm khá hay, một chuyện nhẹ nhàng nhưng cũng không kém phần hài hước và lôi cuốn. Quả thật đúng như những gì The Times Literary Supplement đã đề cập đến "Một cuốn sách xuất chúng dành cho thiếu nhi".
5
53222
2015-09-13 03:27:47
--------------------------
298956
6835
Charlottle và Wilbur là cuốn sách được thiết kế đẹp với áo bìa rời, chữ viết to, rõ và nhiều hình minh họa đi kèm. Các nhân vật trong truyện đều được mô tả sinh động , cách viết nhẹ nhàng đơn giản. Đọc phần giới thiệu của tiki mình đã rất tò mò không biết làm sao một con nhện có thể cứu sống chú lợn bạn mình. Và theo dõi qua từng trang sách mình đã dần dần thấy được, cảm thấy thú vị giữa tình cảm của Fern và em lợn kẹ, về tình bạn gắn kết, sự giữ lời hứa giữa Wilbur và chị nhện Charlotte. Đặc biệt và gây bất ngờ cho mình hơn cả là tình bạn tiếp nối, không thay đổi của lợn Wilbur với những đứa con, cháu, chắt của Charlotte sau này… Một câu chuyện nhỏ nhưng mang nhiều ý nghĩa, rất phù hợp với các em thiếu nhi.
3
648692
2015-09-12 22:40:52
--------------------------
297273
6835
Tôi đã đọc nhiều những quyển sách thiếu nhi viết về động vật nhưng khi đọc đến quyển Charlotte và Wilbur này thì đây có thể nói là một trong những quyển sách về động vật mà tôi yêu thích nhất cũng như làm tôi xúc động nhất. Ngay từ đầu câu chuyện đã cực kì lôi cuốn bởi nội dung dễ thương dung dị qua giọng kể chuyện trong veo, đầy tình cảm của tác giả. Phải thực sự là một người yêu động vật và chú tâm đến cuộc sống tươi đẹp xung quanh lắm mới có thể viết ra một tác phẩm hay thế này. Cuối truyện còn khiến tôi suýt bật khóc !
5
24682
2015-09-11 18:39:42
--------------------------
296507
6835
Mình mua sách trong hội sách của Tiki. Trước tiên vì thấy bìa đẹp, dễ thương, sau vì câu chuyện tóm tắt trên bìa sau của sách nghe có vẻ hấp dẫn.
Điểm cộng rất lớn là hình minh họa trong sách cực dễ thương, chú heo Wilbur thì mập ú nu ú nần, dễ thương kinh khủng.
Câu chuyện thì nhẹ nhàng, nghe có chút "viễn tưởng" nhưng rất thú vị. Đoạn kết hơi buồn, nhưng lại le lói hy vọng vào tương lai.
Sách thích hợp để tặng cho bạn bè, vì truyện ca ngợi tình bạn vô cùng đẹp của Charlotte và Wilbur.
4
325420
2015-09-11 09:25:47
--------------------------
296062
6835
Mình đã tìm mua quyển sách này khi vô tình đọc trên một trang web giới thiệu những quyển sách hay. "Charlotte và Wilbur" là cuốn sách kinh điển dành cho thiếu nhi của văn học Mỹ. Có được tình bạn đã là một điều đáng quý nhưng tình bạn giữa những giống loài không giống nhau lại càng đáng quý hơn. Tình bạn giữa cô bé Fern và chú lợn Wilbur hay giữa Wilbur và chị nhện Charlotte. Sự tinh nghịch - hóm hỉnh và điềm đạm, thông minh đã hòa hợp lại với nhau và tạo nên một tình bạn rất đẹp, giúp đỡ nhau trong những lúc khó khăn...
5
649807
2015-09-10 21:07:00
--------------------------
288792
6835
Mình biết câu chuyện này đầu tiên qua bộ phim thiếu nhi về chú lợn Wilbur và chị nhện Charlotte này từ nhiều năm trước, nên mình cũng muốn đọc thử bản sách xem thế nào. Vì câu chuyện này dành cho thiếu nhi nên nó cũng đậm chất dễ thương, và cả sự phi thực tế nữa. Nhưng thông qua các nhân vật là các nhân vật đáng yêu, lại có những tính cách đặc trưng, chúng ta thấy được một tình bạn đẹp giữa chú lợn con Wilbur ngây thơ, ngại ngùng và một cô nhện thông minh, điềm đạm, chín chắn. Qua đó gửi gắm đến ta những giá trị đáng quý về tình bạn, lòng hy sinh và sự chân thành.
4
3243
2015-09-03 21:41:44
--------------------------
288503
6835
Tác phẩm "Charlotte Và Wilbur " của nhà văn E.B.White này có thể coi là một trong những tác phẩm thiếu nhi kinh điển. Nội dung sách xoay quanh tình bạn của chú lơn Wilbur và cô nhện Charlotte. Mình sẽ không nhận xét nhiều về nội dung vì đây là một tác phẩm kinh điển rùi vì vậy các bạn nên tự đọc và cảm nhận không lãng phí đâu, điều mình muốn nhận xét nhiều ở đây là về ngoại hình của sách được in rất đẹp, có nhiều hình minh họa dễ thương, chữ khá to và rõ ràng- một quyển sách gia đình nên có
4
153008
2015-09-03 16:59:37
--------------------------
282022
6835
Dơi , mình nghĩ cuốn sách này cực kì tuyệt vời tuyệt vời luôn án và mình tự hỏi khi mình còn nhỏ tức là khi mình là cô bé 8 -9 tuổi như cô bé Fern chẳng hạn thì mình thực sự có thể hiểu được ngôn ngũ của các động vật không và thật sự câu chuyện này quá quá là đáng yêu đến nỗi mà mình nghĩ rằng mình có thể đọc đọc mãi mãi về cuộc phiêu lưu giữa chú lợn ủn ỉn Wilbur và chị nhện Charlotte bởi vì nó còn đem lại rất rất nhiều bài học mà chúng ta có thể học hỏi từ một thế giới tuyệt đẹp nữa ~~ Đây là một câu chuyện thiếu nhi thực sự quá quá là tuyệt vời ~~
5
36336
2015-08-28 21:08:36
--------------------------
279648
6835
Tình bạn được tạo thành từ những con người khác nhau, với tính cách khác nhau, sẽ rất thú vị và bền chặt. Bạn hãy cứ tin vào điều đó nếu nhìn vào chú lợn Wilbur, chị nhện Charlotte. Cuộc hành trình bắt đầu vô tình, trải qua những biến cố, và kết thúc nhẹ nhàng nhưng đầy thú vị cho một tình bạn mãi về sau. Không đơn giản chỉ là câu chuyện về chú lợn và chị nhện. Mà đằng sau là bài học đắt giá, sâu sắc cho những ai đã từng có bạn, đang có bạn và sẽ tiến vào thế giới diệu kì của tình bạn. Câu chuyện cho những đứa trẻ mang hình hài người lớn mà bạn nên đọc...
5
475837
2015-08-26 23:41:46
--------------------------
272963
6835
Chú lợn Wilbur thật dễ thương, tính cách ngây ngô trẻ con mà lại quá nhạy cảm và dễ bị tủi thân, Trái ngược với chú, chị nhện Charlotte điềm đạm, thông minh, khéo léo. Họ đã tạo nên một tình bạn vừa kỳ cục vừa oái oăm nhưng cũng lại rất đáng yêu và thú vị. Charlotte luôn hết lòng vì bạn, thậm chí vắt kiệt sức khỏe của mình cho tới chết để cứu Wilbur. Một câu chuyện viết về tình bạn, chân thành, sâu sắc, cảm động.
4
6502
2015-08-20 15:59:16
--------------------------
254769
6835
Hơi hư cấu một chút nhưng quyển sách đáng để chúng ta phải suy ngẫm. Không chỉ đơn thuần là một câu chuyện về một chú lợn được sự giúp đỡ của một chú chuột và một cô nhện đã thoát khỏi cái chết nhiều lần, mà ẩn chứa trong đó là tình yêu thương bè bạn, sự thủy chung son sắt. Một chú chuột ham ăn giúp tìm chữ cái về cho cô nhện tốt bụng, để cô nhện dệt chữ lên tơ của mình, cứu một chú lớn thoát chết hết lần này đến lần khác, để rồi cuối cùng, cô nhện sau khi đẻ trứng đã chết đi, để lại trong lòng bạn nhiều điều nuối tiếc về một tình bạn trong sáng, đẹp đẽ đầy hy sinh và lòng biết ơn sâu sắc. Quyển sách thật cảm động. Cám ơn tiki
5
110777
2015-08-05 14:50:51
--------------------------
251191
6835
Lại một câu chuyện - quyển sách dành cho con nít nhưng người lớn nên đọc. Không chỉ đơn thuần là câu chuyện mà còn là một lời nhắc nhở, nhắc nhở cho người ta phải suy ngẫm rất nhiều. Tôi đọc đi đọc lại quyển sách này thường xuyên, có đôi lúc tôi thấy được thế giới tuổi thơ của mình hiện ra ngay trước mắt khi cầm quyển sách này lên, tôi nghĩ rất nhiều người cũng giống như tôi mê sách này ở chỗ rất tin vào con vật- chúng cũng có ngôn ngữ, có suy nghĩ, có tình cảm...
4
57408
2015-08-02 15:18:12
--------------------------
250455
6835
Tôi đọc và cảm nhận được vẻ đẹp của Charlotte và Wilbur, của các con vật trong chuồng trại, của thời gian, và của tình bạn.
Cuộc sống ngày thường của Wilbur được phủ bởi yêu thương và chăm bẵm. Đến khi sang đông thì một câu chuyện cảm động của tình bạn bắt đầu... Bắt đầu đơn giản, và kết thúc cũng nhẹ nhàng.
Tôi buồn khi thời gian sống Charlotte quá ngắn ngủi, nhưng rồi lại ánh lên hi vọng về những đứa con của chị, về đường đời của các con, các cháu của chị. Trên tất cả, Wilbur vẫn nhớ về chị và đó là thứ đẹp đẽ nhất mà tôi đọc và học được từ cuốn sách.
5
220265
2015-08-02 10:24:14
--------------------------
234410
6835
Nói một cách ngắn gọn  thì đây là một cuốn sách dành cho thiếu nhi nhưng nội dung và ý nghĩa của nó mang lại không dừng ở việc phục vụ cho lứa tuổi đó. "Charlotte và Wilbur" có thể ví như một ngọn gió mát khiến tâm hồn ta trong phút chốc trở nên dịu mát và dễ chịu hơn bao giờ hết. Câu chuyện về tình bạn giữa một chú lợn và chị nhện, nghe thì có vẻ kì quái nhưng khi đọc mình lại thấy rất dễ thương, Wilbur ban đầu có vẻ khá ích kỉ, lúc nào cũng muốn lợi cho bản thân mà không nghĩ đến suy nghĩ của người khác, nhưng không phải vì thế mà Charlotte bỏ rơi cậu. Cô bạn nhện nhỏ nhắn và vô cùng thông minh luôn vì bạn mà bất chấp mệt mỏi và nguy hiểm. Đến tận những giây phút ngắn ngủi cuối cùng của cuộc đời, cô cũng không ngừng nghĩ cách để giúp bạn mình thoát chết. Không chỉ có tình bạn đẹp, những câu nói của Charlotte trong tác phẩm cũng rất hay( mình sẽ không trích ra vì để khi bạn đọc sẽ tự cảm nhận :3 ). Bia được làm rất đẹp ; cách trình bày từng trang sách cũng vậy; các bạn sẽ không phải tiếc tiền cho một cuốn sách như thế này đâu.

4
134346
2015-07-20 12:46:11
--------------------------
221921
6835
Mình đã mua cuốn này lâu rồi, rất hay, thỉnh thoảng lấy ra đọc lại vẫn thấy rất ý nghĩa. Charlotte và Wilbur là một cuốn sách trong trẻo, nhẹ nhàng về tình bạn. Dường như có một thế giới tuổi thơ ngọt êm và dịu nhẹ lắng đọng trong đó. Từ chuyện lợn Wilbur quả quyết mình sẽ giăng được tơ, chuột Templeton giữ khư khư quả trứng ngỗng ung trong tổ như một món đồ quý đến việc chị nhện thông thái Charlotte quả quyết sẽ tìm cách cứu mạng sống cho bạn mình… cuốn sách tạo nên một thế giới động vật sinh động đến ngỡ ngàng. Các nhân vật trong tác phẩm không nhiều. Nhện Charlotte hiểu biết và chăm chỉ, lợn Wilbur thực thà và khờ khạo, chuột Templeton thực dụng và ham ăn… mỗi con vật đều đại diện cho một tính cách, và trong nhiều tình huống khác nhau, những tính cách ấy được soi tỏ rõ ràng. Vì tình bạn, chị nhện sẵn sàng đón nhận kết cục buồn thảm nhất cho mình. Vì tình bạn, lợn Wilbur đã nâng niu bọc trứng của bạn từ hội chợ về khu chuồng ấm áp và canh giữ chúng trong suốt mùa đông giá rét. Cũng có vẻ tốt tính, có vẻ được việc - nhưng chuột Templeton làm cái gì cũng tính toán tới lợi ích của mình trước tiên. Cuốn sách mang góc nhìn trẻ thơ nhưng ẩn khéo léo bên trong là những tính giáo dục cao với triết lý sâu xa về cuộc sống, tình bạn và sự tồn tại của tình bạn theo thời gian. 
Một tác phẩm tuyệt vời dành cho trẻ con và những ai đã từng là trẻ con :)
5
42833
2015-07-04 17:31:51
--------------------------
216837
6835
Một tác phẩm đơn giản, dễ thương dành cho trẻ em và cũng dành cho những đứa như mình, những đứa đã một thời tin rằng  ngoại trừ con người thì tất cả các loài vật đều có suy nghĩ riêng của chúng, có thể nói chuyện với nhau và làm những chuyện bí ẩn mà con người không thể nào biết được.
Thật vậy, ai có thể tin được một chú lợn béo ú như Wilbur lại sợ sệt đến nỗi phải nhờ vả một cô nhện nhỏ bé như Charlotte cơ chứ. Và dĩ nhiên cũng chả mấy ai tin cô nhện ấy không những giải cứu thành công chú lợn này mà còn giúp chú ta những điều khác nữa cho đến khi đọc câu truyện này. Một tác  phẩm tuyệt vời  cho trẻ con.
4
139133
2015-06-28 12:16:11
--------------------------
215860
6835
Được giới thiệu như một tác phẩm xuất chúng dành cho thiếu nhi, nên mình đã sưu tầm nó vào bộ sưu tập dành cho cô em gái nhỏ của mình. Và tất nhiên là cũng dành cho chính bà chị mê tít truyện thiếu nhi này nữa :)
Câu chuyện với một cốt truyện rất li kì về một cô nhện thông minh đã cứu sống cậu bạn lợn của mình khỏi nguy cơ bị làm thịt, không những thế còn đem về giải thưởng danh giá cho cậu. Câu chuyện đầy màu sắc thần kì của tuổi thơ cổ tích, nhưng ẩn trong đó là những giá trị to lớn và sâu sắc.
Câu chuyện cho ta thấy được giá trị của tình bạn, rằng đôi khi người ta có thể hi sinh rất nhiều để đứng phía sau giúp đỡ bạn bè. Và dù chúng ta có thể không có một điểm chung nào, chỉ cần yêu thương nhau, và ta có thể thành những người bạn thân thiết nhất!
Truyện cho ta trải nghiệm khung cảnh đồng quê yên bình và nhẹ nhàng, cho các em thêm tình yêu thương với các loài động vật và niềm vui khi hòa mình với thiên nhiên.
Và như vậy là đủ những điều ta cần trong một cuốn sách cho thiếu nhi.
4
71367
2015-06-26 21:49:45
--------------------------
173430
6835
Tôi không mong đợi gì nhiều hơn ở một tác phẩm như thế.
Tôi đã được sống lại tuổi thơ, với những suy nghĩ dễ thương, trong sáng, ngây thơ. Tôi đã cảm nhận được tâm hồn mình được thanh lọc lại, để cảm nhận sự ngây ngô đáng yêu của chú Wilbur, sự hi sinh mạnh mẽ khó tin của nhện Charlotte. Tôi cảm nhận rũ bỏ được những mệt mỏi hằng ngày của một người trưởng thành phải hết lòng và cố gắng vì công việc.
Tuổi thơ của tôi ko được đọc những cuốn sách như thế này, tuổi thơ của tôi chỉ gắn liền với cuốn sách giáo khoa và sục sạo những bài văn trong đó.
Tôi nghĩ là cần nuôi dưỡng tâm hồn trẻ thơ bởi những cuốn sách như thế này. Và tôi sẽ để lại cuốn sách đẹp đẽ này trên giá sách, dành tặng cho những đứa con của mình cơ hội được đọc nó.
5
249602
2015-03-26 10:06:38
--------------------------
173138
6835
Mình mua cuốn sách này vào đầu 2015, không phải nói quá chứ truyện đã cuốn hút mình từ khi đọc những trang đầu. Charlotte và Wilbur là một đôi bạn ngộ nghĩnh và đáng yêu vô cùng, đáng yêu không kém là cô bé Fern có tình thương vô bờ dành cho động vật, ngạc nhiên hơn là có vẻ cô nghe được loài vật nói. Và đoạn mình thích nhất cũng là đoạn cảm động đối với mình trong cuốn sách này là 500 quả trứng của Charlotte nở, rồi lần lượt bay đi chỉ còn duy nhất ba con nhện nhỏ giăng tơ trước chuồng Wilbur, chúng làm cho Wilbur hết sức vui mừng xúc động vì nó đã tưởng sẽ không còn ai làm bầu bạn với nó bởi vì Charlotte đã mãi mãi ra đi còn Fern đã chẳng lui tới trang trại như trước nữa.
Một cuốn sách đáng đọc và ngẫm nghĩ về tình bạn!
5
542401
2015-03-25 18:44:44
--------------------------
166595
6835
Mình nghĩ đây là một câu chuyện ý nghĩa, nhẹ nhàng và mang tính giáo dục cao. Nhưng đối với mình nó không thực sự hấp dẫn, tình tiết còn hơi dễ đoán và không gay cấn. Có lẽ nó phù hợp với lứa tuổi nhỏ hơn mình tìm đọc.

Nhưng mình khá thích cách tác giả xây dựng tính cách nhân vật - đặc biệt là Charlotte, Wilbur và Fern cùng các nhân vật phụ khác. Chỉ đáng tiếc là câu chuyện xảy ra giữa họ tuy nhiều ý nghĩa nhưng không phải lúc nào cũng có thể cuốn hút được người đọc.
3
557111
2015-03-12 22:42:05
--------------------------
165632
6835
Mình đọc quyển sách này vì mình nhớ hình như trước đây mình đã từng xem một bộ phim có nội dung thế này, và khi đọc xong thì mình phát hiện hóa ra cuốn sách này đã được chuyển thể thành phim điện ảnh. Câu chuyện kể về tình bạn đầy ý nghĩa và cảm động về những người bạn ở 1 nông trang với những cố gắng  giữ lấy sự sống cho chú heo Wilbur. Mạch chuyện khá tự nhiên và đôi lúc khá hài hước, thú vị. Mình rất thích cuốn sách này vì giấy khá được và minh họa khá hợp với mình.
5
114793
2015-03-10 20:06:19
--------------------------
160195
6835
Câu chuyện về chú heo Wilbur được cứu nhờ cô nhện Charlotte dù đã ra đời hơn 60 năm nhưng giá trị của nó vẫn không hề thay đổi.
Một quyển truyện vỏn vẹn 227 trang nhưng chứa vô vàn những điều kỳ thú:
- Về một cô nhện thông thái, tốt bụng nhưng luôn ẩn mình đằng sau đúng với 2 chữ KHIÊM NHƯỜNG
- Về một chú heo thật thà, khá là nhiều chuyện và hay xỉu nhưng luôn tốt với bạn bè.
- Về con chuột già tham lam, gian mãnh nhưng lại là nhân vật mình thích nhất truyện vì đôi khi mình khá là khó hiểu về tính cách nhân vật này khi a 3 lần 7 lượt dường-như-vô-tình mà giúp đỡ Wilbur.
-Về cô nàng Fern, mình nghĩ đây chính là tác giả của toàn bộ câu chuyện trên, cô là đại diện cho thời thơ ấu của tất cả chúng ta: hồn nhiên, vui tươi và đầy ắp những ước mơ và sự sáng tạo.
Và cùng những con vật còn lại như bầy ngỗng, bầy cừu, đám nhện con,... đã tạo nên câu chuyện không chỉ đơn giản là dành cho trẻ con mà còn dành cho tất cả chúng ta-những ai đã từng là trẻ con, ngồi xuống và ngẫm lại về khoảng thời gian vô ưu nhất trong cuộc đời.
5
172193
2015-02-23 06:12:51
--------------------------
156999
6835
Đây quả thực là một tác phẩm đáng đọc cho không chỉ trẻ con mà còn cho người lớn. Đọc để cảm nhận tính nhân văn sâu sắc mà tác giả đã truyền tải, cảm nhận  tình bạn, sự thủy chung,... là những điều thiêng liêng mà không chỉ con người mà ngay cả đối với những con vật cũng có nữa. 
Một cuốn sách rất đáng để gối đầu giường con trẻ. Tôi nghĩ những ai đã và đang làm cha mẹ, nên tặng cho con cái mình những quyển sách thiếu nhi nhẹ nhàng mà đầy tính nhân văn giáo dục thế này thì quả là rất có ích và ý nghĩa.
5
23656
2015-02-07 04:42:49
--------------------------
151295
6835
Một cô nhện nhỏ bé thì có thể làm gì được chứ? Ấy vậy mà trong câu truyện này, bằng tình yêu thương, lòng nhân hậu, cô nhện Charlotte cùng với chú lợn Wilbur ngộ nghĩnh đáng yêu đã dựng nên một tình bạn rất cao cả. 
Ngày qua ngày, tình bạn đó càng được thể hiện một cách sâu sắc hơn khi Charlotte đã nhiều lần cứu sống Wilbur, thậm chí phải hi sinh cả tính mạng của mình. Đáp lại tình cảm đó, Wilbur đã từng ngày chăm sóc và bảo vệ cho bọc trứng nhỏ của Charlotte. Kết thúc câu chuyện mở ra một tình bạn mới, đẹp đẽ, trong sáng giữa Wilbur và những đứa con của Charlotte. Xuyên suốt câu chuyện là tình cảm bạn bè thật nhẹ nhàng, ấm áp.
4
473130
2015-01-19 13:00:25
--------------------------
147246
6835
Mình biết đến cuốn sách này khi đọc Nhật ký chú bé nhút nhát. Quả không hổ danh tác phẩm kinh điển, đây là một cuốn sách vô cùng ý nghĩa không chỉ dành riêng cho các em thiếu nhi mà còn phù hợp cho cả người lớn. Những bài học không bao giờ cũ về tình bạn được lồng ghép rất khéo léo trong từng câu văn. Câu chuyện về tình bạn giữa Wilbur, một chú lợn và Charlotte, một cô nhện đã gần như lật tung cả trang trại lên để cứu bạn mình khỏi nguy cơ bị giết thịt. Mình rất thích cái kết của cuốn sách này, như là một cấu chuyện không chỉ về tình bạn mà còn về lòng tin và sự thủy chung.
5
248911
2015-01-07 12:07:02
--------------------------
142130
6835
Lúc mới nhìn qua bìa sách, tôi cứ ngỡ đây đơn thuần chỉ là một câu chuyện dành cho trẻ con, nhưng đọc rồi mới biết nó chứa đựng nhiều ý nghĩa như thế nào. 
Mạch truyện như ấm lên bởi tình bạn ấm áp của các loài vật, mà nổi bật ở đây là tình bạn giữa chú lợn WIlbur và cô nhện Charlotte. Ngòi bút của tác giả đã khiến tôi không nhìn chúng như nhìn một con lợn và một con nhện bình thường mà thấy được những điều hết sức đẹp đẽ và dễ thương ở chúng. Thậm chí, tôi còn có nhiều phần kính trọng Charlotte - một cô nhện đã hy sinh thân mình để giúp cho WIlbur, tạo nên một tình bạn vĩ đại. Ở Charlotte, tôi cũng thấu hiểu thêm được phần nào ý nghĩa của cuộc sống. 
Không có gì khó hiểu khi CVW được trao giải thưởng văn học Newbery. Đây là một tác phẩm rất đáng đọc. Tuy nhiên, CVW còn một điểm khiến tôi không hài lòng đó chính là cách dịch của dịch giả. Trong truyện tồn tại một số đoạn dịch rất cứng và máy móc.
4
123326
2014-12-17 18:32:26
--------------------------
137678
6835
Charlotte Và Wilbur là một tác phẩm viết cho thiếu nhi với cái nhìn rất nhân văn và chứa đựng rất nhiều điều đáng quý và rất đáng trân trọng!
Câu chuyện là những cung bậc cảm xúc về tình bạn giữa những con người với nhau và cũng giữa con người với những con vật...Và trong tình bạn ấy, mình nhận thấy được sự chân thành, sự yêu thương hết mực và cả những sự hy sinh dành cho nhau!
Với lối kể hài hước và dí dỏm, mạch truyện nhẹ nhàng và cảm xúc, chứa đựng nhiều thông điệp ý nghĩa và nhân văn. Tác phẩm này quả thực là một tác phẩm rất đáng để tìm đọc và xứng đáng nằm trong "kho" sách của độc giả :)
5
303773
2014-11-27 21:07:04
--------------------------
131895
6835
Lâu rồi mình mới đọc được cuốn sách hay như thế này! Quả thực rất lôi cuốn!
Cách dẫn chuyện dí dỏm và sinh động của tác giả khiến mình không thể rời mắt cho đến khi kết thúc tác phẩm.
Một câu chuyện hay và cảm động về tình bạn của cô bé Fern với Wilbur, của Wilbur với các con vật khác trong trang trại nhà Zuckerman, đặc biệt là tình bạn cao thượng, không tiếc bản thân của Charllote dành cho chú lợn xuân Wilbur.
Charllote và Wilbur là một cuốn sách hay dạy ta cách đối xử với loài vật, dạy cho ta biết trân trọng tình bạn bằng câu chuyện hết sức đáng yêu mà không cần rao giảng đạo lý.
Và đặc biệt, E.B.White đã chứng tỏ cho ta thấy rằng "cuộc sống này nảy sinh từ cái chết" như thế nào.
5
164722
2014-10-28 10:30:38
--------------------------
126751
6835
Đây là một tác phẩm truyện hay nhất mà tôi từng đọc, nó dạy ta cách sống và đối xử với động vật. Ngay từ những trang đầu ta cũng cảm nhận được điều đó. một tác phẩm giàu tính nhân văn và tình người. Hãy đọc để cảm nhận! Ta sẽ thấy hiểu hơn về những con vật gần gũi với ta, thấy được không chỉ ở người, động vật cũng có tình cảm và cách bày tỏ của chúng hoàn toàn khác chúng ta. Có khi thứ tình cảm ấy còn mãnh liệt và đẹp đẽ hơn con người. Hãy đọc để biết yêu thương hơn! Để trau dồi cảm xúc, để mở rộng trái tim, để biết sẻ chia và để biết đánh thức lòng trắc ẩn... Quả thật là một tập truyện xuất sắc.
5
311391
2014-09-20 09:45:30
--------------------------
121695
6835
“Charlotte và Wilbur” là câu chuyện thú vị về.tình bạn giữa những con vật trong trang trại nhà Zuckermn, đặc biệt hơn cả là tình cảm chân thành, cao cả của cô nhện Charlotte dành cho cậu lợn Wilbur. Chưa bao giờ mình đọc 1 câu chuyện về tình bạn loài vật lại thấy xúc động như vậy! Charlotte chỉ là một cô nhện bé tí nhưng lại rất thông minh và giàu tình cảm, cô đã nghĩ ra “trăm phương ngàn kế”, huy động cả trang trại để cứu cậu bạn của mình. Tâm tư tình cảm của loài vật được tác giả miêu tả rất dễ thương và đáng yêu! Mình mua cuốn truyện này một cách rất tình cờ mà không biết đây là một tác phẩm kinh điển. Khi đọc xong thì mình đã hiểu tại sao nó lại nổi tiếng và được nhiều người yêu thích như vậy. Hấp dẫn, cảm động và giàu tính nhân văn, đó là những điều bạn sẽ cảm nhận được khi đọc cuốn truyện này!
5
135597
2014-08-20 22:49:33
--------------------------
121590
6835
Đây không chỉ là "Một cuốn sách xuất chúng dành cho thiếu nhi" mà còn là một cuốn sách xuất chúng dành cho người lớn. Charlotte và Wilbur kể cho chúng ta nghe câu chuyện về tình bạn, lòng yêu thương và sự chân thành. Về một cô nhện Charlotte vô cùng thông minh đã cứu chú heo Wilbur khỏi những lần bị bán bởi những mạng nhện thần kỳ được dệt bằng lòng yêu thương, sự chia sẻ chân tình. Có lẽ, không một ngôn từ nào có thể diễn tả hết sự tuyệt vời của cuốn sách này dành cho mọi lứa tuổi, cuốn sách dạy cho những bài học về sự đồng cảm, về tình yêu thương giữa những người bạn và về mối dây thân tình thắm thiết được nối dài theo thời gian.
5
19052
2014-08-20 13:29:17
--------------------------
