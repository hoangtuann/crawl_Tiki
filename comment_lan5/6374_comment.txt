397974
6374
cuốn Truyện Cổ Campuchia do nhà xuất bản văn hóa thông tin phát hành cũng được lâu rồi nên chất lượng giấy cũng có phần cũ, nhưng được cái chất lượng in tốt và sách cầm nhẹ. Sách khá dày, gần 300 trang, có tới gần 50 câu truyện cổ của đất nước Campuchia. Hiện nay hầu hết là truyện cổ tích andercen, truyện cổ grim, ít được biết đến truyện cổ của các nước khác. Đây là một trong bảy cuốn truyện cổ Đông Nam Á. Tôi đã đặt mua về vừa để đọc vừa cho trẻ con trong nhà đọc cùng.
4
854660
2016-03-15 17:41:39
--------------------------
