355009
6671
Có nhiều diễn biến trong câu chuyện ,nếu không có một nhân vật phải chết thì bá tước đã thay đổi toàn bộ người điều hành cho mình mà không xem xét điều ảnh hưởng đi chung với nó , nếu gặp rối ren trong cải cách mà bà không làm để mọi chuyện suôn sẻ thì ngược lại , có kẻ muốn ám hại bà mà không dễ gì biết trước được nó đến vào lúc nào , đồng minh bị phá vỡ quy tắc , bà lại có được những kẻ thù ở cạnh mình , sẵn sàng ra tay tàn ác .
4
402468
2015-12-19 00:03:12
--------------------------
295785
6671
Đây là bộ tiểu thuyết hùng tráng của nhà văn Mỹ George R.R. Martin. Tập thứ tư và thứ năm liên tục đứng đầu các bảng xếp hạng của Mỹ và nhiều nước trên thế giới. truyện được dịch ra nhiều thứ tiếng.Truyện kể về sự trang giành ngai vàng của nhà vua, các cuộc chiến cũng như các âm mưu nhằm thống trị đất nước. Sách được in bằng giấy tốt, trắng, bìa hấp dẫn. Cách dịch cũng phù hợp với nội dung của tác phẩm. Giá hơi cao nhưng nội dung vẫn xứng đáng với giá tiền đã bỏ ra.
5
68764
2015-09-10 17:12:25
--------------------------
292744
6671
Trò Chơi Vương Quyền - Tập 4B: Lời Tiên Tri là tập cuối của phần 4. Trong phần này tính cách của hoàng hậu Cercei được tập trung khắc họa rất chi tiết. Những tình tiết xoay quanh nhân vật này rất kịch tính, thể hiện sự cô độc, lạnh lùng, tàn nhẫn trong các quyết định, qua đó ta thấy được sự dối trá, tàn bạo của thế giới quyền lực và chính trị mà cái đích cuối cùng là ngai vàng, là cái tôi của mỗi người. Phải nói nhân vật Cercei là một thành công của tác giả George R.R. Martin.
5
465332
2015-09-07 16:32:53
--------------------------
253346
6671
Tôi đã phải chờ đợi rất lâu mới đặt mua được cuốn Trò Chơi Vương Quyền - Tập 4B: Lời Tiên Tri của tác giả George R.R. Martin trên Tiki. Sách cứ nhập về là hết mà tôi lại là kẻ luôn chậm chân. Qua đó cũng thấy được bộ truyện này nóng tới mức nào, gây say cho độc giả ra sao. Tập này tiếp tục cuộc chiến giành ngai Sắt với chủ đề chính xoay quanh Lời tiên tri. Không hiểu sao tôi tin rằng lời tiên tri đang nói đến Daenerys Tagaryen. Không liên quan mấy nhưng tôi rất mong nhà phát hành nên suy nghĩ tới việc phát hành sách bìa cứng.
5
167283
2015-08-04 13:10:45
--------------------------
216693
6671
Mỗi tập của Trò chơi vương quyền là một sự lôi cuốn khác nhau. Tình tiết truyện đan xen giữa bạo lực, tình dục và những bí ẩn ám ảnh liên tục xuất hiện khiến Trò chơi vương quyền trở nên hấp dẫn không thể dứt bỏ. Mình thích một hình tượng anh hùng mạnh mẽ và thực tế hơn hẳn so với các cuốn sách viễn tưởng khác, không quá thần thánh hóa nhân vật. Đây cũng là cuốn sách hiếm hoi mà các nhân vật phụ được đầu tư tính cách và chi tiết nhiều hơn, không chỉ quá chú trọng và tập trung vào một mình nhân vật chính, vậy nên truyện chưa bao giờ nhàm chán cả
4
13083
2015-06-28 09:04:36
--------------------------
196814
6671
Trong truyện này nhân vật nữ nào cũng mạnh mẽ,nhân vật mà tôi thích nhất là Cersei.Tuy là nhân vật phản diện nhưng suy cho cùng ngay từ đầu bà ta cũng chỉ là con cờ của gia tộc Lannister cưới phải Robert Baratheon một người không hề yêu bà ta để rồi bây giờ cha mất chồng mất con mất xung quanh bà ta không còn ai có thể tin tưởng.Tôi cũng rất thích Arya và Brienne,Arya là một cô gái độc lập kiên quyết còn Brienne thì rất mạnh mẽ dù cho cô ấy phải chịu một số phận đau xót
5
526322
2015-05-16 15:20:48
--------------------------
180907
6671
Phải rất kiên nhẫn và bền bỉ mới đọc tới chừng này của bộ truyện. Nhiều nhân vật cũ đã ra đi, tập này đa phần là các nhân vật mới. Thích cái cách tác giả xây dựng nhân vật, nhất quán và ấn tượng. Tuy nhiên, mình rất khó chịu với cách dịch thuật: các nhân vật khác đều gọi bằng cô hoặc anh, riêng nhân vật Cersei lại gọi bằng ả. Mặc dù mình chẳng thích Cersei, nhưng ý đồ của tác giả là kể một cách khác quan với góc nhìn của từng nhân vật, người dịch y như rằng chẳng hiểu gì về câu chuyện.
Dù vậy, đây là tác phẩm rất đáng đọc, mình mong chờ tập tiếp theo
5
220006
2015-04-10 13:50:30
--------------------------
129354
6671
Ôi Cersei, sự cô độc đã biến bà thành kẻ thất bại mất rồi, sẽ chẳng có Ngai sắt nào cho bà trừ vương miệng ảo tưởng. 

Trò Chơi Vương Quyền là một bộ tiểu thuyết quá đồ sộ và có quá nhiều nhân vật, việc sách xuất bản theo từng tập với khoảng thời gian dài như vậy dễ ngắt mạch đọc và dẫn tới dễ quên tên nhân vật. Nhưng tôi hiểu để dịch một tác phẩm như thế không phải đơn giản, cầu có thời gian. Chính tác giả George R. R. Martin cũng mất nhiều năm để viết xong một tập.
Ở tập này 4b này chuyển biến mạnh mẽ nhất là âm mưu  Cersei nhắm tới Margaery đã thành công, cá nhân tôi nhận thấy việc Cersei làm là đi quá xa, từ khi Tywin mất,  Cersei lập tức thay thế tất cả bộ máy của mình bằng người mới. Người mà chính bà cũng không thể tin được, trong khi chính gia tộc Lannister đang gặp rắc rối lớn, thì bà lại muốn triệt hạ đi đồng minh lớn nhất của mình. Chung quanh bà chỉ toàn là kẻ thù mà bà không thể nhận ra.
Ở tập này Brienne cũng đã có bước chuyển lớn của mình, Brienne cũng là một trong những nhân vật tôi thích nhât. Tôi đã thoáng đau lòng khi đọc tới đoạn này:

“Một số người được ban cho con trai, một số người được ban cho con gái. Nhưng chẳng có ai bị nguyền rủa tới mức sinh ra một kẻ như cô.”

Tội lỗi duy nhất mà Brienne mắc phải là khác người, cũng như Tyrion, nhưng chẳng lẽ khác biệt lại là tội lỗi hay sao?
4
43494
2014-10-09 10:58:52
--------------------------
127452
6671
Nếu bạn là fan của LOTR và những bộ truyện lịch sử, fantasy của phương Tây thì Trò Chơi Vương Quyền là một sự lựa chọn hoàn hảo.
Điều đặc biệt ở bộ truyện này là nó mang đậm tính lịch sử và chỉ viết về thời cổ xưa, với những vị vua và các lãnh chúa. Đọc GoT và bạn sẽ thấy như mình thực sự đang ở vùng đất Westeros huyền thoại. Truyện có nhiều POV characters khác nhau, tạo ra nhiều điểm nhìn nhưng đều gắn kết mật thiết. George R. R. Martin đã thực sự tạo nên một thế giới lịch sử đầy cuốn hút, với những âm mưu về quyền lực và tình dục. Một điểm khác ở Trò Chơi Vương Quyền là bạn không thể nói ra được đâu là nhân vật chính diện, bởi mỗi nhân vật đều có quá khứ và sai lầm. Một câu chuyện phản ánh đúng thực tế, và cực kì cuốn hút. Một khi đã cầm sách lên, bạn sẽ không thể bỏ xuống được.
5
431455
2014-09-24 12:56:53
--------------------------
