355197
5112
Nếu có một số điều dễ nhận thấy trong văn phong của những người sống gần với thời đại phong kiến sẽ nhìn ra ngay , truyền thống cổ xưa kín đáo mà nhẹ nhàng thấm đẫm qua từng câu văn , nhiều lối cách diễn đạt cởi mở , phóng khoáng , chân trời ngay tại nơi đây là sự kiện lớn đối với nhiều người rồi , kiến thức không cần tìm hiểu nhiều mà vẫn có phong thái toát lên nhiều thi vị , giản dị không ngờ , rõ ràng và lập luận sắc bén .
4
402468
2015-12-19 14:02:44
--------------------------
334363
5112
Mình mua tác phẩm điêu tàn để nhằm hiểu thêm về thơ ca của Chế Lan Viên, một trong những viên ngọc sáng của thơ mới và cách mạng. Thơ ông hùng hồn mang theo nét rùng rợn khiến người đọc sởn gai ốc nhưng lại không kém phần cuốn hút, đọc mà chẳng thế nào dứt ra được. Thiết kế bìa cũng rất đẹp, toát ra cái khí chất âm u của tập thơ. Điều mình không ưng ý ở sản phẩm này là khổ sách khá lớn trong khi cả quyển thì lại quá mỏng, không phù hợp tẹo nào. Nhưng được cái là Nhã Nam xuất bản quyển nào là quyển ý đều tốt cả.
5
902137
2015-11-09 12:50:00
--------------------------
259305
5112
Tìm hiểu về Chế Lan Viên và biết rằng đây là một tập thơ khá nổi tiếng của ông. Nhưng có lẽ đọc những lời đau thương chết chóc trong tập thơ lại khiến mình có cái gì đó rợn ngược, sợ hãi. Vì vốn dĩ con người vốn sợ cái chết chóc ấy lắm. Điểm cộng mình thích đó là bìa sách đã bao trùm về nội dung mà Chế Lan Viên đã thể hiện ở toàn nội dung thơ, như ông đã viết: "Trong thơ ta dân Chàm luôn sống mãi/ Trong thơ ta xương máu khóc không thôi". Mặt khác sách in rất tốt, chất lượng giấy rất tuyệt!
3
299614
2015-08-09 12:19:49
--------------------------
192364
5112
Chắc là do mình không đắn đo và xem xét kĩ trước khi mua nên không thích tập thơ này lắm.. Chủ yếu nói về tâm trạng, cảm xúc và miêu tả về cái "điêu tàn" của một nền văn minh Champa.. Mình là người ưa tiểu thuyết hoặc thơ tình lãng mạng nên mặc dù Chế Lan Viên đã thực sự dồn rất nhiều cảm xúc, trau chuốt từ ngữ vào từng bài thơ nhưng mình chỉ đọc được mấy bài là dừng, bởi cảm thấy không thích hợp với mình. Tóm lại là bạn nào có tính cách như mình thì không nên mua nhé.. Đọc hơi rợn người thật đấy.. Ôi chết chóc, đầu lâu, xương xẩu.. Chế Lan Viên version đầu tiên là đây!!!
3
457189
2015-05-04 13:21:02
--------------------------
177763
5112
Đây là khoảng thời gian mà tác giả tự nhốt mình vào chiếc hộp u kín của chế độ mà tự day dứt, ảo nảo, để rồi thốt ra những vần thơ mang sắc thái đầy kinh hoàng, với những tiếng gào thê lương, những bóng ma ám ảnh tràn trề thi nhau đội lên trên từng lớp nghĩa như lớp sóng đổ xô vào bờ xúc cảm. Biết Chế Lan Viên từ hồi lớp 11, và rất có ấn tượng về tác giả này. Viết thơ tuyên truyền có vẻ không hợp với ông, nhưng những nội dung trừu tượng siêu hình trong thơ ông luôn rất giàu hình tượng, bể tưởng tượng phong phú không bao giờ vơi cạn.
Sách được in giấy bóng tốt, trang trí hợp với nội dung. Rất thích cách in giấy, nhìn cứ như tấm gỗ cũ mục, mang nét cổ kính như bức tượng Chăm u huyền.
5
209069
2015-04-03 20:07:59
--------------------------
166581
5112
Nhã Nam rất biết cách hấp dẫn người đọc bởi chính sự hiểu biết người đọc của mình, bộ sách Việt Nam danh tác là một trong những dẫn chứng, như ở Điêu Tàn chẳng hạn, chưa nói đến  thơ Chế Lan Viên cuốn hút, cách trình bày cuốn sách, với những trang thơ vàng thơm mùi thời gian, những bức mình dán tay mềm mại đã là một điểm cộng quá lớn rồi
Trước tới giờ, vốn chỉ biết tới người lính trẻ Chế Lan Viên qua những trang thơ ngọt ngào chạm tận sâu trái tim, và lần đầu tiếp xúc với tác phẩm này, tôi đã được chứng kiến một Chế Lan Viên khác, một CLV chìn sâu trong u tối, một CLV của Chiêm Thành xưa cũ...
4
310296
2015-03-12 22:20:18
--------------------------
157874
5112
Mình học văn và khá hâm mộ nhà thơ Chế Lan Viên. Chế Lan Viên khá giống với nhà thơ điên Hàn mặc tử. Thơ ông cũng mang những nét quái dị có thể nói khó hiểu vô cùng nhưng lại pha chút gần gữi quen thuọc với người Việt. Điêu tàn là tập thơ mang đậm phong cách thơ Chế Lan Viên trước cách mạng. Đọc điêu tàn tôi cảm nhận một vùng không gian thơ khá u tối không có tương lai, cùng với những hình ảnh thơ trừu tượng dẫn lối người đọc vào những ngõ cụt. Cá nhân tôi không phải người cảm thơ giỏi nên tôi chỉ nhìn được cái vỏ ngoài của thơ CLV mà không thấy được cái hay nhất tinh túy nhất của thơ ấy. Đối với giáo viên và những người yêu thơ thì đây là một lựa chọn không tồi.
4
471002
2015-02-10 17:15:40
--------------------------
154560
5112
Viết "Điêu tàn" khi chỉ mới 15, 16 tuổi, sự xuất hiện của Chế Lan Viên được xem như một niềm kinh dị giữa làng thơ Việt. Nhưng cái kinh dị ở đây không chỉ ở vấn đề tuổi tác, mà còn bởi một giọng thơ buồn ảo não pha sắc màu huyền bí kì lạ.Trong "Điêu tàn", chúng ta rùng rợn ở cõi âm với xương sọ đầu lâu, với mồ  không huyệt lạnh,....Cảm ơn cuốn sách đã cho tôi rùng rợn trước thế giới của "Điêu tàn", cảm nhận được nỗi đau trí tuệ sâu sắc của nhà thơ. Từ bìa sách đến nội dung, tất cả đều rất điêu tàn, rất Chế Lan Viên.
5
546314
2015-01-29 18:35:46
--------------------------
148926
5112
Mới nhìn bìa sách thôi mà mình đã thấy một cảm giác rất hoang sơ, điêu tàn, rợn gáy, điên loạn. Rất Chế Lan Viên. Mở từng trang giấy, tôi như bước từng bước vào thế giới của ông, cái thế giới của cõi chết, của yêu ma, quỷ quái, sọ người. Nhà thơ muốn tránh xa tất cả loài người. "Hãy đem tôi xa lánh cõi trần gian!" Tránh đến mức thậm chí ông còn không nhớ nổi chính mình là ai. Sống trong một xã hội tù túng, bó buộc, nhà thơ đã chọn thoát ly thực tại. Có lẽ đó cũng là lựa chọn tốt nhất.
5
1050
2015-01-11 23:33:55
--------------------------
148771
5112
Chế Lan Viên là một tác giả mình rất yêu thích, đặc biệt là tập thơ này của ông.
Về mặt nội dưng mình nghĩ không cần bàn đền nhiều, vì ai hay đọc và yêu thích cũng như tìm hiểu về thơ Chế Lan Viện thì không ai không biết đến tập thơ này.Ở tập thơ này ông đã sáng tạo rất nhiều về cả mặt nội dung và nghệ thuật, qua đó khẳng định ngòi bút điêu luyện và bút lực mãnh mẽ của mình
Tuy nhiên, cá nhân tôi không thích bìa của tập thơ này cho lắm, tuy nó mang đậm màu sắc nghệ thuật của Chế Lan Viên nhưng trông cứ hơi cũ kĩ thế nào
4
496784
2015-01-11 14:23:44
--------------------------
