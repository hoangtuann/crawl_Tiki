270732
6718
Chuyện Bạch Tuyết Và Bảy Chú Lùn ở Việt Nam hiện nay có rất nhiều bản sao khác nhau, ở nhà mình cũng phải có vài quyển rồi. Tuy nhiên mình chưa ưng ý quyển nào.
 Nhưng sau khi đọc thử ở ngoài nhà sách thì mình quyết định mua thêm quyển này trên Tiki vì đây chính là cốt truyện mà tuổi thơ mình ghi nhớ. Với việc mụ phù thủy năm lần bẩy lượt tìm cách hãm hại Bạch Tuyết và cuối cùng mụ bị trừng phạt bằng cách phải đi đôi giầy nóng và nhảy múa mãi mãi
Quyển sách này rất đẹp hình ảnh 3D nên nhìn rất sinh động. Ngoài ra còn có phần trò chơi tương tác, giúp các bạn nhỏ có thể nhớ lâu, nghĩ nhanh.
5
501684
2015-08-18 16:00:27
--------------------------
