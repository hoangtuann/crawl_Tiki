415775
6373
Cuốn Truyện Cổ Thái Lan là một trong bảy cuốn truyển cổ Đông Nam Á do Tác giả PGS. TS Ngô Văn Doanh biên soạn. Mình đã sưu tầm đủ bộ luôn rồi. Cuốn sách tuy dày nhưng cầm rất nhẹ. Có 22 mẩu truyện, tuy gọi là mẩu nhưng nó cũng khá dài. Có điểm không hài lòng là sách hơi bị ngả màu quá, nếu như chất lượng giấy tốt hơn nữa và có in nhiều hình minh họa thì tốt hơn. Như thế sẽ gây hứng thú cho các em bé tuổi hơn. Chứ sách toàn chữ thế này thì chỉ có người lớn mới chịu ngồi đọc thôi. 
4
854660
2016-04-13 23:08:47
--------------------------
