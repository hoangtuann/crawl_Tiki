462806
6002
Chưa đầy hai trắm trang giấy mà chứa đựng bao nhiều cảm xúc, phần lớn có lẽ là sự chua xót cay đắng của tác giả đối với số phận của gia đình mình, ở đó hình ảnh của người mẹ, người vợ, người chị dâu, những người con gái thôn quê hiện lên buồn u uất. Mình thích lối hành văn của tác giả, cô đọng không rườm rà nhưng vẫn đầy cảm xúc, nét đẹp toát lên từ sự đơn giản và mộc mạc. Chắc có lẽ sẽ phải đọc lại nhiều lần để thấm nhuần tâm sự mà tác giả đã gửi gắm qua từng con chữ.
5
132597
2016-06-28 21:50:49
--------------------------
438818
6002
Chân trời cũ này là một tác phẩm vô cùng hay , sâu lắng. Nó mang một màu sắc gì đó hơi hơi buồn như chính cái tên của nó gợi ra. Sau khi đọc , nó đã gợi lên cho người đọc thật nhiều suy nghĩ về một thời đói khổ đã qua của nước nhà. Nó gợi lên những tình cảm thật thiêng liêng, sâu sắc trong gia đình hay nhiều mối quan hệ khác. Nó mang đậm một nét rất riêng có thể là hơi thở Trung Hoa phảng phất bên trong bởi đó là cội nguồn của tác giả...
5
1351512
2016-05-30 13:12:15
--------------------------
415859
6002
Đầu tiên là xét tới bề ngoài sách. Phải gọi là cực ưng luôn, bìa sách mềm min, lúc sờ rất sướng. Hình ảnh bìa sách hài hoà, nhẹ nhàng.
Về nội dung sách: Dường như nhân vật trong truyện là 1 cậu bé thơ bé, hồn nhiên, vô tư, nghịch ngợm mà cũng đượm buồn vì cậu sinh ra và lớn lên khi nghiệp nhà đã tàn tạ.Người cha đã từng có lúc là trụ cột của gia đình, nhưng rồi sa vào nghiện ngập và chết sớm để lại cả một gánh gia đình cho người mẹ trông coi. 2 chị em dòng máu Hoa Kiều... Chung chung mà nói, mình khá thích tác phẩm này. :)
5
446851
2016-04-14 05:59:01
--------------------------
409042
6002
Thật bất ngờ khi một quyển sách từ một thế kỷ trước lại gây cho tôi nhiều xúc động đến thế. Nội dung chính của quyển sách nói về chuyện cá nhân tác giả nhưng tôi có cảm tưởng như tác giả đang nói hộ lòng mình. Những hoài niệm của riêng tác giả lại khiến tôi suy tư và cay xè đôi mắt khi nó làm tôi liên tưởng đến gia đình tôi, bạn bè tôi, hay đơn giản là những người đã đi qua cuộc đời tôi.

Khi đọc đến phần Lòng mẹ thì một cảm xúc xúc động mãnh liệt và dào dạt trong tâm khảm khiến tôi phải rung lên từng tiếng nấc nghẹn ngào. 

Chắc chắn rằng bạn cũng sẽ tìm thấy được từ tác phẩm này đâu đấy những gương mặt thân quen mà một thời đi qua cuộc đời bạn để rồi để lại nhiều hoài niệm, luyến nhớ, những dĩ vãng nhạt nhòa có hạnh phúc và có cả xót xa.
5
582986
2016-04-01 19:59:39
--------------------------
318677
6002
Chân trời cũ cho người đọc thêm yêu quê hương đất nước Việt Nam mình, không cần thời sự mà rất đỗi thân quen... Những dòng độc thoại thể hiện cái tôi nội tâm phức tạp càng in khắc sâu phong cách của Hồ Dzếnh. Câu chuyện nhẹ nhàng, lắng đọng, dễ đi vào lòng người, khiến chúng ta không khỏi miên mãn suy nghĩ về những kiếp người còn nhiều khó khăn mà đầy chân chất, mộc mạc, hiền từ như thế .... 
Tiki bọc sách này bằng Bookcare khá đẹp, bìa cũng đẹp nữa, mình rất ưng ý. :)

5
533454
2015-10-06 17:51:46
--------------------------
315336
6002
Ở tuổi vẫn ham mê truyện tranh , nhận được một cuốn truyện như vậy chả vui vẻ gì . Nhưng chính là khi vẫn còn vô lo vô ưu tôi lại thấy mình thật già dặn sau khi đọc Chân trời cũ . Phải kiên nhẫn lắm mới đọc hết cuốn sách . Thu hút kì lạ ... Tôi thấy người mẹ tần tảo ấy mà tôi nghĩ đến bà đến mẹ ! Tôi mường tượng nếu như cha tôi nghiêm khắc thì đáng sợ đến nhường nào ... Tôi mơ đến những người bạn cũ , có chị Sáu có hàng mơ rầu rầu ... Tuổi thơ tôi là thế , màu xanh bịn rịn của trang sách Hồ Dzếnh in bóng vào tâm can tôi ... Đượm buồn trong kí ức ... Nhưng tôi vẫn yêu mãi chân trời tà năm ấy! Chân trời cũ
5
843825
2015-09-28 19:30:46
--------------------------
263675
6002
Hồ Dzếnh được mình biết đến như một nhà thơ nhiều hơn là một nhà văn nên khi Nhã Nam cho xuất bản cuốn sách này, kì thực rất tò mò xem văn phong của Hồ Dzếnh ra sao và mình cũng tương đối hài lòng. Trong Chân Trời Cũ, ta vẫn thấy được phong cách chung của tác giả đó là viết về quê hương đất nước Việt Nam ta. Tuy văn phong không được lai láng như Thạch Lam hay vốn từ uyên bác giống Nguyễn Tuân mà mộc mạc, chân chất, dễ hiểu, dễ cảm. Một cuốn sách đẹp cả về nội dung và hình thức.
5
112948
2015-08-12 19:06:36
--------------------------
247216
6002
Hồ Dzếnh- lúc nghe tên mình cứ thấy vừa quen vừa lạ, mình thấy tò mò bởi cái tên ấy nên đã tìm đọc tác phẩm. Câu chuyện bắt đầu. Mình thấy chất làng quê đong đầy và thân quen, rồi xuất hiện những kiếp đời, những số phận khổ đau, những suy ngẫm của con người,.. Văn chương mộc mạc, từ ngữ cũng không hẳn tài tình nhưng làm đủ làm người đọc nhớ về phong cách Hồ Dzuếnh. Đọc xong sách, thấy đầu nhẹ bâng, nhưng trong lòng cứ dợn lên cảm xúc gì đó, là niềm đồng cảm với những con người xa lạ, hay sự hoài niệm về những thứ đã qua,.. Lắng đọng !
5
256191
2015-07-30 10:00:17
--------------------------
232203
6002
Hồ Dzếnh - nếu ai một lần nghe qua tên ông có thể cảm thấy xa lạ với cái nên văn học Việt Nam cũ, không có trong sách giáo khoa. Rồi cũng sẽ bỏ qua ông với tác phẩm của ông.  Nhưng các bạn hãy đọc, vì mình tin Nhã Nam đã chọn lọc những tác gia với những tác phẩm hay của họ - đáng ghi nhận, xứng đáng để cho vào bộ Việt Nam Danh tác.
Văn chương của ông không thể nói đến mức đại tài hoa - cái từ mình nghĩ Việt Nam mình hiếm có nhà văn được nhận từ đó (mình nghĩ có cụ Nguyễn Du đầu tiên) hay gần tới được (thế thì nhiều), nhưng văn của ông cũng rất chân thật và từ ngữ rất mộc, rất chân thành.
Với tác phẩm này sẽ tìm lại những ký ức cũ và cả những kỹ niệm đẹp, những số phận người rất phải để suy nghĩ - khóc, suy tư, thẫn thờ, cười, bình thản... 
4
472508
2015-07-18 17:39:53
--------------------------
229371
6002
Ở tuổi 17 mình hiếm khi đọc các tác phẩm nổi tiếng Việt Nam. Trong lần lang thang facebook ''Nhã Nam'' mình đã thấy hình ảnh của bộ ' Việt Nam danh tác'' của các bạn trẻ đã mua trên page đăng lên. Điều gì ở những tác phẩm này cuốn hút các bạn trẻ như mình.Mình thực sự tò mò. Mần trên tiki và đọc được nhận xét của các bạn cộng với đoạn đầu đọc thử của truyện khá hấp dẫn khiến mình không băn khoăn mà đặt mua ngay. Lúc nhận được sách thì mình rất ngạc nhiên vì sách của nhã nam bìa rất đẹp, hình ảnh đơn giản mà ý nghĩa, màu sắc hài hòa bìa mướt mườn mượt chỉ muốn dụi mặt vào và ôm đi ngủ thôi. Rất cưng. Mình đọc xong thì càng cưng nó hơn.Truyện kể về cuộc sống cách nhìn đời của một cậu bé với hai dòng máu Hoa- Việt rất đặc biệt- Hồ Dzếnh . Những suy nghĩ những tình cảm đơn thuần từ khi còn là một đứa trẻ đến khi trưởng thành của ông thực sự làm mình thấm thía. Đời người như người ta vẫn nói là một hành trình chứ không phải điểm đến. Hành trình tìm về một cuộc sống hạnh phúc tìm về cội nguồn tìm về với chân lí với mục đich sống của từng nhân vật trong truyện sao mà khó đến thế. Mỗi người mỗi số phận. Đau thương đến lạ kì. Đọc xong mà phải thốt lên rằng '' vì sao họ khổ thế. vì sao ai cũng khổ. thật lạ''. Hồ Dzếnh cho người trẻ như mình biết rằng nỗi buồn đau khổ và mọi khó khăn đời người ai cũng từng kinh trải chỉ còn tình thân con người ở lại chỉ còn tình yêu quê hương sống mãi. Mọi thứ sẽ mãi là những kí ức đẹp đẽ những chiêm nghiệm tuyệt vời mà không phải ai cũng có được.Truyện của Hồ Dzếnh nhẹ nhàng dễ đọc. Chính '' Chân trời cũ'' của Hồ Dzếnh đã là động lực để mình quyết tâm mua cho trọn bộ '' Việt Nam danh tác'' tuyệt vời( dù đến giờ thì mình vẫn chưa thực hiện được). Nhưng mình tin chắc trong tương lai gần mình sẽ rinh chúng từ tiki về. Mình tin tưởng rằng những cuốn sách tiếp theo sẽ hứa hẹn đem lại rât nhiều điều tuyệt vời cho mình.
5
96879
2015-07-16 19:44:42
--------------------------
218924
6002
Mình tìm hiểu và được biết Hồ Dzếnh là một nhà thơ nổi tiếng của Việt Nam. Ông được biết nhiều nhất qua tập thơ Quê ngoại với một giọng thơ nhẹ nhàng, siêu thoát. Ngoài ra Hồ Dzếnh còn là một nhà văn với nhiều tác phẩm, tiêu biểu là tập truyện ngắn Chân trời cũ. Hồ Dzếnh chỉ kể chuyện về người cha mình, các anh, chị, em mình, con ngựa của cha mình mà làm cho người đọc rung động tận đáy lòng. Mỗi câu chuyện là mỗi số phận của con người được ông miêu tả với giọng văn mộc mạc chân chất nhưng đọng lại trong lòng người đọc một nỗi buồn phảng phất và rất lâu sau đó mới dứt ra được.
4
460604
2015-06-30 23:02:33
--------------------------
217301
6002
Hồ Dzếnh là một người mình rất yêu mến và kính trọng. Dường như người miền Nam ngày trước biết đến ông nhiều hơn. Qua tác phẩm này, ngòi bút của tác giả đã đưa độc giả tái khám phá một bức tranh “bàng bạc” nỗi buồn của kiếp nhân sinh – những nỗi buồn tựa như “chân trời ngày cũ”.

Lời văn trong cuốn sách mộc mạc, có phần chân chất, không hoa mỹ, không bay bướm lãng mạn. Nhưng vì thế mà dường như nó “đọng” lại trong lòng độc giả lâu hơn. Nhất là những độc giả trung tuổi. Với những người trẻ như mình, tác phẩm này có sự bổ ích riêng của nó, nhất là trong việc “điềm đạm hóa” những tâm hồn bồng bột!

Sách được Nhã Nam in đẹp, giá trắng mịn như thường thấy.
4
598102
2015-06-29 09:36:12
--------------------------
207950
6002
Việt Nam Danh Tác - Chân Trời Cũ sẽ không bao giờ hối hận khi bạn mua quyển sách này. Những tác phẩm của Hồ Dzếch luôn khiến những đau khổ cũ và thêm sắc màu trong những trang văn. Về hình thức thì cuốn sách khá đẹp, giấy lán min,chữ in rõ ràng, dễ thấy. Sẽ là một điều tuyệt vời nhất, nếu đem tặng quyển sách trân quý này cho những bậc cao niên mà bạn quý mến. Để qua đó, là một thông điệp mà tác giả như muốn nhắn gửi: "Nói về dĩ vãng là một điều khó khăn, khi những điều ta ngẫm nghĩ chưa có đủ thì lắng xuống và thêm bền chặt".
5
302082
2015-06-13 19:21:13
--------------------------
157960
6002
Mở đầu cuốn danh tác là câu chuyện gặp gỡ của Người mẹ - một cô gái miền quê Trung Bộ Việt Nam với Người cha là khách lãng du người Trung Quốc để rồi từ đó, những câu chuyện ngắn khác được mở ra, với những nhân vật: "Chị dâu", anh cả, anh hai, người chú, em Dìn, thằng cháu đích tôn, chị Yến.... Mỗi câu chuyện là một kiếp người, một số phận mà xuyên suốt trong đó là một màu ảm đạm, thê lương......
"Cứ gì phải chung sống dưới một mái nhà, chia uống một ngụm nước, cùng ngắm một dòng sông, người ta mới yêu nhau được?"
5
291719
2015-02-10 21:27:21
--------------------------
137663
6002
Chân Trời Cũ của Hồ Dzếnh là tác phẩm đầu tiên mà mình đọc được của tác giả này.
Quyển sách với những lời văn mộc mạc ẩn chứa đầy hoài niệm về cuộc sống và tính yêu của những con người ở " cái dải đất cần lao, cái dải đất thoát được ra ngoài sự lọc lừa, phản trắc, cái dải đất chỉ bị bạc đãi mà không bạc đãi ai bao giờ". Chỉ bấy nhiêu chất văn đó thôi cũng đã làm mình yêu văn chương của Hồ Dzếnh đến mức nào. Yêu cái chất mộc mạc của con người, yêu cái sự dằn vặt khi nhớ về người cữ, con người cũ....
Chân Trời Cũ xứng đáng là một tác phẩm hay, một trong những danh tác Việt Nam tạo được sức sống cho văn chương!
4
303773
2014-11-27 20:27:51
--------------------------
132067
6002
"Viết văn, xét đến cùng, lại là việc "nhìn vào trong hồn" nhiều hơn. Vì thế, cảm giác dằn vặt, ân hận là cảm giác không gột rửa được trong văn Hồ Dzếnh. Đó là nỗi day dứt vì chưa hiểu, chưa thấy được, chưa thông cảm hết được với những nỗi khổ của kiếp người, nhất là khi nỗi khổ ấy lại là thứ không nói lên được, đặc biệt ở các nhân vật nữ trong tập truyện." Đọc Chân trời cũ mà mường tượng đến những tháng ngày xa xôi, những cột mốc thời gian trong cuộc đời của một con người,  những sự đã xảy ra và ghi dấu kiếp nhân sinh - như những linh hồn lưu lạc - đẹp đẽ, giản đơn nhưng mang nhiều nỗi niềm...
5
404741
2014-10-29 15:17:11
--------------------------
