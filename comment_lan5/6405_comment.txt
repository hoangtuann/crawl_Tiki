423244
6405
Mình mua quyển này vì con mình học tiểu học, cứ hay nhờ mình giải nghĩa các từ trong sách Tiếng Việt. Mình muốn giải nghĩa chuẩn hơn, và tập cho con thói quen tra cứu, tự tìm câu trả lời nên mua tặng con quyển này.
Ưu điểm: 
- tương đối nhỏ gọn, nhẹ, có thể mang trong cặp nếu cần
- tra thử 1 số từ thì thấy giải nghĩa dễ hiểu và đúng

Nhược điểm:
- font chữ khó nhìn, khó phân biệt dấu ở các từ có cả dấu mũ và dấu thanh
- loại từ được viết tắt (ví dụ "dt", "đgt") nhưng lại không có ghi chú giải thích các từ viết tắt đó
2
96831
2016-04-28 22:18:25
--------------------------
281825
6405
Để học tốt được ngoại ngữ khác thì trước hết chúng ta phải hiểu rõ ngôn ngữ tiếng Việt của chúng ta. Vì vậy, cuốn từ điển tiếng Việt là một cuốn từ điển cần thiết đối với tôi để có thể hiểu ý nghĩa từ vựng và tìm ra được điểm khác nhau so với ngoại ngữ mà tôi đang học. Đây cũng là cách để tôi nhớ được từ mới nhiều hơn.. Và tôi thấy cuốn “Từ điển Tiếng Việt” của Viện Ngôn Ngữ này trình bày rất rõ ràng, có chú giải về ngữ pháp, có ví dụ minh họa, chất lượng giấy cũng khá tốt..
3
654655
2015-08-28 18:44:28
--------------------------
