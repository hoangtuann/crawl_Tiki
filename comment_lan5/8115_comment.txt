365631
8115
Có nhiều sự hấp dẫn tại công sở làm việc của cô gái trẻ Duyệt Duyệt luôn có cơ cấu khiến người ta thích gần lại câu truyện rằng mối quan hệ tranh chấp có những diễn biến rất thường xuyên diễn ra , để lại nhiều sự lo ngại về thực tại giữa các công ty bây giờ , nhiều khâu chuẩn bị cũng như bị chèn ép làm cho bất ngờ sinh sự với các đồng nghiệp khác , chỉ có điều mất đi sự bình yên nên có bởi đấu tranh cũng không công bằng có hại hơn được điều mình muốn .
4
402468
2016-01-08 13:51:14
--------------------------
261836
8115
Khi đọc tên tiểu thuyết, mình hy vọng nó chứa đựng nhiều những mưu mô, thủ đoạn mà người ta sử dụng để chơi xấu nhau chốn công sở. Đáng tiếc, truyện tập trung nhiều hơn vào tình cảm của nữ chính Duyệt Duyệt. Hết lần này đến lần khác cô vướng vào âm mưu nhằm đẩy cô ra khỏi công ty cũng như vì ghen ghét cô nhận được sự ưu ái của Tống Dật Tuấn, đáng nhẽ cô phải tự mình giải quyết vấn đề đó, thể hiện sự trưởng thành sau khi lăn lộn trong môi trường ấy. Nhưng không, tất cả vấn đề của cô đều được Doanh Thiếu Kiệt hay Tông Dật Tuấn giúp đỡ, xử lý giùm cô, đến ngay cả chuyện tình cảm cũng phải thức tỉnh giùm thì cô mới nhận ra. Nói chung là mình không thích nữ chính cho lắm, còn lại thì khá ổn.
Thêm một điều nữa, không thể tin được là sách của mình bị đóng nhầm số trang, lặp lại 2 lần từ trang 97- 128, trong khi đó từ trang 128- 161 thì biến mất ??!! 

3
42306
2015-08-11 14:27:56
--------------------------
258300
8115
Tôi bị cuốn hút vì tiêu đề: "Âm mưu nơi công sở" vì đúng là chốn văn phòng có nhiều điều khó cũng không khó mà dễ cũng không dễ. Nhưng vì là ngôn tình, nên thiết nghĩ nó cũng có chút gay cấn hoặc cuốn hút khi theo dấu truyện, có điều hơi nhạt 1 tí. Dẫn dắt của truyện khá mờ nhạt, chỉ như là kể lại 1 câu chuyện, tình tiết cũng hoàn toàn không quá đặc sắc để lôi kéo bạn vào câu chuyện tình lãng mạn hay là một chút gì đó gọi là âm mưu bắt đầu hiển hiện. Tôi chỉ thấy hơi tẻ nhạt khi phải theo đuổi mạch truyện, quá chậm... Dù đã tiến tới trang thứ 100 nhưng vẫn chưa thể xác định và vạch ra ranh giới giữa nữ chính và nam chính, cách khắc họa nhân vật cũng khá tầm thường. Có lẽ tôi đã quen với việc nhân vật ngôn tình đều theo 1 mô tuýp nhưng có điều ít ra cũng nên có chút gì đó nổi bật hoặc cao trào hoặc là đừng để người đọc có cảm giác lịm đi khi vẫn còn muốn theo dõi...
3
557371
2015-08-08 13:26:24
--------------------------
176079
8115
Đọc truyện, mình cảm thấy hơi tiếc cho Dật Tuấn, anh yêu Duyệt Duyệt có lẽ vì ở cô có những điều xưa nay vốn thiếu trong cuộc đời anh: sự trong sáng, thánh thiện, một thế giới tốt đẹp, còn anh đã trót vấy bẩn để trèo lên đỉnh núi danh vọng. Dù biết anh sẽ không thể thành với Duyệt Duyệt nhưng mình vẫn đau lòng khi anh chọn cái chết, trong khi đáng lẽ ra tác giả hoàn toàn có thể để anh làm lại cuộc đời.
Còn Thiệu Kiệt có lẽ là một đối tượng phù hợp để làm người yêu, làm chồng. Điều thú vị nhất có lẽ là tật nói lắp của anh, lần đầu mình thấy có một nam chính mắc tật này đấy. Nhưng chính điều đó làm anh đáng yêu hơn.
Tuy có tên Âm mưu nơi công sở nhưng truyện này thực ra không có nhiều âm mưu ghê gớm gì mấy. Nếu trông đợi sẽ có những màn đấu đá tóe lửa có lẽ bạn sẽ phải thất vọng, nhưng bỏ qua điều đó thì đây cũng là một truyện hay.
4
57459
2015-03-31 16:47:06
--------------------------
153189
8115
Đây là quyển tiểu thuyết đầu tiên mình đọc nói về tình yêu chốn văn phòng cũng những âm mưu trên thương trường kinh doanh
Nữ chính Tô Duyệt Duyệt tuy ngoại hình và gương mặt không xinh đẹp mỹ miều  hay nổi trội nhưng ở cô là tính cách mạnh mẽ, quyết đoán và luôn nghĩ cho người khác.
Tống Dật Tuấn và Doanh Thiệu Kiệt là 2 người đàn ông trong cuộc đời cô, 1 con người là sếp của cô, quá hoàn hảo, Dật Tuấn tài giỏi, đẹp trai, giàu có, thân thiện, 1 mẫu người mà nhân viên nào cũng dễ dàng yêu mến, vậy mà anh đã phải lòng cô, một cô gái không có gì nổi trội ở công ty, anh yêu cô bởi chính con người cô, trong sáng, thánh thiện, bên cạnh cô khiến anh thoải mái, không cảm thấy áp lực, căng thẳng bởi  những âm mưu chốn công sở.
Doanh Thiệu Kiệt, anh chàng chân thành, hay cà lăm mỗi khi nói chuyện với cô. Định mệnh có lẽ đã gắn kết 2 người kể từ khi cô tình cờ thuê được nhà của anh, lại thuê xe của anh để tiện đường đi làm. Thế là ngày ngày 2 người đều ở bên nhau, yêu nhau lúc nào không hay. Anh luôn âm thầm quan tâm, lo lắng cho cô từ xa, còn cô thích anh, yêu anh nhưng cứ luôn phân vân với tình cảm của mình khi đứng giữa 2 người đàn ông.
Cái kết là sự ra đi của Dật Tuấn làm mình hơi hụt hẫng,  anh ta đã quyết định làm lại cuộc đời trong sạch hơn, cớ sao tác giả lại để cho anh ấy ra đi như thế. Duyệt Duyệt vì cái chết của anh mà không muốn gặp lại Thiệu Kiệt, cô quyết định ra đi. Sau bao nhiêu năm tháng tìm tòi không mệt mỏi, định mệnh một lần nữa đã gắn kết họ lại với nhau, để Thiệu Kiệt có thể tìm thấy Duyệt Duyệt và ở bên cô.
5
38403
2015-01-25 13:51:16
--------------------------
143162
8115
Đây là cuốn tiểu thuyết tôi khá ấn tượng. Đầu tiên là thiết kế bìa khá bắt mắt với gam màu sáng. Sau đó khi đọc truyện rồi mới thấy cốt truyện không hề nhàm chán. Tô Duyệt Duyệt là nhân vật nữ chính, tôi không thích tính cách của cô lắm, nhưng tôi lại đánh giá cao năng lực làm việc của cô trong truyện. 1 cô gái đời thường như bao cô gái văn phòng thế kỉ 21, cũng mơ ước được làm trong công ty liên doanh, có thu nhập cao. Chính mô típ nhân vật đời thường đã khiến cho người đọc cảm thấy sự chân thật trong tác phẩm. Doanh Thiệu Kiệt tôi lại khá ấn tượng, 1 anh chàng thông minh tài năng nhưng mắc bệnh nói lắp khi gặp chuyện quan trọng. Đó là 1 điểm rất đáng yêu của nhân vật. Nội tâm phong phú và sự kiên định với tình cảm khiến tôi thích nhân vật này. Tống Dật Tuấn lại là nhân vật nam thứ trong chuyện tình yêu. Anh yêu Duyệt Duyệt giống như tìm kiếm 1 tình yêu bù đắp. Yêu cô vì cô có những cái mà không bao giờ anh có được: Sự trong sáng lương thiện. Nhân vật này phản ánh 1 sự thật chốn văn phòng là cách đi cửa sau để có quyền chức. Nhưng anh cũng rất đáng thương khi lựa chọn cái chết để yêu Duyêt Duyệt. 
Tôi nghĩ rằng cuốn sách phản ánh được phần nào sự thực của cuộc sống văn phòng trong thời đại mới.
4
375799
2014-12-21 22:12:13
--------------------------
111678
8115
Cuốn truyện xoay quanh các mối quan hệ của Tô Duyệt Duyệt nơi công sở xen lẫn với những chi tiết hài nhẹ nhàng. Nhưng tôi vẫn mong được đọc về nhiều về âm mưu nơi công sở nhiều hơn, vì trong truyện tác giả có vẻ hướng tới truyện tình cảm là chủ yếu. Văn phong ổn, tuy nhiên cốt truyện cũng chưa có gì đặc sắc, dừng lại ở mức an toàn.
Tôi thấy nhân vật thú vị nhất trong truyện là Doanh Thiệu Kiệt, hay nói lắp, có điều anh vất rất dễ thương, lại vẫn giữ được vẻ mạnh mẽ tin cậy lúc cần, không xây dựng theo khuôn mẫu quen thuộc của các hoàng tử trong ngôn tình hiện đại.
Điều mà tôi không thích trong truyện đó chính là tình cảm của các nhân vật có phần đột ngột, gượng ép, dường như tác giả làm thế để ép cho họ đi đúng theo mạch truyện, khiến truyện mất tự nhiên.
Cuốn truyện này nếu đọc để giải trí thì cũng được
3
123326
2014-05-02 14:52:04
--------------------------
110837
8115
Thực sự đây là một cuộc sách rất hay, rất logic, hài hước nhẹ nhàng. Tô Duyệt Duyệt là một cô gái thật thà, ngây thơ, nhưng không phải loại nữ chính ngốc nghếch yếu đuối chờ nam chính cứu giúp như một số ngôn tình khác. Không cần phải nhan sắc đẹp rực rỡ, hơn thế nữa cô còn bị cận (rất ít tác phẩm nào để nữ chính 4 mắt), nhưng Duyệt Duyệt đã từng bước vô tình chinh phục 2 anh em đẹp trai của tập đoàn. Không phải kiểu tình yêu sét đánh hay là kiểu tình yêu một bên có ấn tượng từ trước, mà là tình yêu trong sáng, hình thành dần dần trong mỗi người. Mỗi người trong truyện đều có mỗi tâm sự của riêng mình, về tình cảm, ai cũng đáng thương cả. Thật sự mình không ghét Tống Dật Tuấn cho nên cái kết anh chết thực sự khá hụt hẫng, đoạn anh trân trối với Duyệt Duyệt rất đáng thương..Tuy cái kết là 2 main chính bên nhau cơ mà nếu có thêm ngoại truyện về c.s 2 người sau này thì cuốn truyện sẽ trọn vẹn hơn nhiều ^^. 
Tóm lại, đây là một cuốn sách rất đáng đọc!
5
187302
2014-04-21 17:06:10
--------------------------
110040
8115
Cuốn sách có hơi hướng lãng mạn và thi vị hóa hiện thực nơi công sở nhiều hơn so với “âm mưu” mà đề tựa của quyển sách hướng đến. Tôi không nghĩ đây là tác phẩm của một người từng có kinh nghiệm làm nhân viên cấp cao trong doanh nghiệp nước ngoài. Hoặc giả như sau những thực tế cạnh tranh khắc nghiệt của môi trường doanh nghiệp nhưng tác giả vẫn có thể hình dung những mảng hồng lung linh sống động ở chốn công sở thì đó thật sự là một điều rất đáng quý. 

Có lẽ nhiều cô gái sẽ yêu thích tình cảm mãnh liệt mà Tống Dật Tuấn dành cho Tô Duyệt Duyệt, lại càng yêu thích hơn nữa những quan tâm chân thành mà thầm lặng của Doanh Thiệu Kiệt dành cho cô gái họ Tô. Nhưng hiện thực cuộc sống có lẽ sẽ khác rất nhiều so với những gì mà Ni Mạt Tiểu Ngư vẽ nên. Sự ngây thơ của Tô Duyệt Duyệt sẽ được xem như một món ăn lạ miệng mà bàn tiệc nơi văn phòng hiếm khi có được, vì vậy nó có thể kích thích sự hiếu kỳ và khẩu vị nhất thời của người thưởng thức, nhưng khó có thể có được một vị trí thường trực trong đầu người dùng. 

Tôi cho rằng nội dung nhẹ nhàng của cuốn sách thích hợp với những bạn trẻ mới ra trường vốn vẫn còn giữ được nhân sinh quan màu hồng tươi đẹp.
3
37868
2014-04-11 10:15:03
--------------------------
108064
8115
Lúc mới đầu đọc cuốn sách này mình thấy hơi chán vì chỉ nhắc đi nhắc lại công việc của Tô Duyệt Duyệt .
Nhưng càng đọc thì càng không thể rời mắt khỏi cuốn sách luôn. Những âm mưu, đấu đá nhau nơi công sở thật sự rất thực tế và sinh động. Ngoài ra, mình cũng cực thích những lúc mà hai "oan gia" Doanh Thiệu Kiệt và Tô Duyệt Duyệt đụng độ nhau, rất đáng yêu và dễ thương.
Đoạn cuối truyện làm mình muốn khóc vì sự ra đi của Tống Duật Tuấn, thực ra lúc đầu mình rất ghét nhân vật này vì đã ngăn cản Tình yêu của Tô Duyệt Duyệt nhưng lúc anh chết thì mới thấy thật đáng thương. Nói chung đây là một cuốn sách đáng để đọc.
5
273450
2014-03-14 16:39:05
--------------------------
102579
8115
Mình mua Âm Mưu Nơi Công Sở ở Tiki nhưng trong lòng cũng khá đắn đo vì thấy chưa có bạn nào nhận xét về quyển sách. Tuy nhiên khi mua rồi mình đã không hối hận vì nội dung truyện rất hay, những âm mưu, toan tính đấu đá nhau trong công ty nơi Duyệt Duyệt làm việc rất thực tế, nhân vật cũng trần trụi hơn. Có người vì địa vị mà sẵn sàng bám váy phụ nữ như Tô Dật Tuấn, có người vì muốn được hưởng lợi mà không từ bất cứ thủ đoạn nào như Nhan An Tâm.

Mình rất thích tình yêu giữa Duyệt Duyệt và anh chàng nói lắp thật thà, đáng yêu Doanh Thiệu Kiệt. 2 người cũng trải qua nhiều sóng gió mới đến được với nhau. Đoạn cuối gay cấn và đẩy truyện lên đến cao trào. Nói chung đây cũng là quyển sách khá hay về ngôn tình dành cho ai yêu thích thể loại này.
4
33861
2013-12-21 08:01:43
--------------------------
