532377
6344
Mình mua cho bạn và thấy bạn mình khen rất hay, hấp dẫn.
4
916398
2017-02-27 15:38:17
--------------------------
490150
6344
Một tác phẩm tuyệt vời khỏi phải bàn cãi, nó đã được chứng thực qua thời gian. Định mệnh đã đưa tôi đến với em... Tôi đã yêu thích tác phẩm này ngay lần đầu tiên xem bộ phim Thiên Thần Và Ác Quỷ và quyết định tậu cho mình một quyển, ngay khi được cầm quyển tiểu thuyết trên tay thì lại càng yêu hơn. Tác phẩm đưa ta đến với câu chuyện đầy li kỳ và kịch tính mang đậm tính phiêu lưu và tôn giáo.  Một tác phẩm hay mà khi bạn đã đọc rồi thì không thể dừng lại đựơc nên hãy xác định thật kĩ trước khi mở trang đầu tiên nhé.^^
5
1860168
2016-11-11 12:07:34
--------------------------
472963
6344
Khi mình nhìn thấy cuốn sách này trên trang web Tiki, mình thật sự ấn tượng với hình thức của bìa sách: một nửa thiên thần và một nửa ác quỷ tượng trưng cho một phần lương thiện và một phần ác độc trong con người, đó là lí do mình mua cuốn sách này. Cuốn sách thật sự lôi cuốn người đọc khiến người đọc khó lòng mà rời quyển sách khi đang đến hồi cao trào. Nhưng quyển sách này khó lòng tránh khỏi những sai sót - thì mình nhận thấy lỗi chính tả cũng khá nhiều nên góp ý để lần tái bản sau có chỉnh sửa kịp thời.
4
971913
2016-07-10 09:09:47
--------------------------
468357
6344
Thông qua hai bộ phim điện ảnh đỉnh cao là Thiên thần và ác quỷ và Mật mã Da Vinci, mình mới tìm đọc và thấy tác phẩm trên tiki nên đã nhanh chóng mua về để thưởng thức. Về phần bao bìa thì rất chất lượng, giấy bao đẹp, giấy viết và mực sáng, rõ, gây ấn tượng mạnh với người đọc qua bìa ngoài vẽ rất công phu.
Nội dung thì đậm chất phong cách của Dan Brown, cuốn hút, kịch tính và giả tưởng xen lẫn hiện thực được lồng ghép vào nhau tạo nên một tác phẩm ăn khách nhất của ông. 
4
1294681
2016-07-04 22:48:02
--------------------------
436100
6344
Không có gì phải bàn cãi về nội dung của những cuốn sách do Dan Brown viết, tuy là viết nhiều truyện tiểu thuyết nhưng rõ ràng không có sự trùng lặp về nội dung giữa các câu chuyện. Nói thật mình rất may mắn khi sở hữu tác phẩm kinh điển này, do lúc nhìn lên thì chỉ còn 2 cuốn nên mình nhấn nút mua ngay. Và mình đã không có gì phải hối hận khi đặt mua cuốn sách.
Về hình thức thì mình có một số miêu tả như sau: Bìa sách cực đẹp, hơn nữa do làm bìa cứng nữa nên nhìn rất là thích, sách khổ lớn nên giống như là bạn đang cầm cuốn từ điển hay bách khoa toàn thư hơn là một cuốn tiểu thuyết, màu của giấy là vàng nhạt, mình rất thích màu này vì nó chống mỏi mắt. Quả thực ai là fan của Dan Brown thì không thể bỏ qua tác phẩm này.
4
1176595
2016-05-26 09:56:37
--------------------------
417925
6344
Tôi biết đến Dan Brown từ những ngày đầu của đại học qua một người bạn. Tác phẩm của ông thế hiện một tri thức vô cùng uyên bác về các vấn đề vốn bản chất đã lôi cuốn con người. Đọc sách chúng ta sẽ được phiêu lưu cùng ông qua các trang giấy. Đọc "Thiên thần và Ác quỷ" hay hơn hẳn phim truyền hình vì trí tưởng tượng của chúng ta ngày càng phát triển. Đây là quyển đầu tiên trong chuyến hành trình của Giáo sư Đại học Havard Langdon. Các bạn sẽ mê tác phẩm ngay từ những trang viết đầu tiên!
5
54458
2016-04-18 10:08:55
--------------------------
404915
6344
Đây là lần thứ 2 mình mua cuốn Thiên Thần Và Ác Quỷ của Dan Brown, vì bản mua cách đây hơn 7 năm đã bị mất. Phải khẳng định đây là một tác phẩm rất hay, nhưng phải đọc cuốn sách do Nhà xuất bản Văn hóa - thông tin phát hành này quả thật là kinh khủng, nó đầy lỗi chính tả, kéo dài xuyên suốt từ đầu tới cuối, kể cả trên bìa sau của sách. Những lỗi ngô nghê, bỏ dấu sai, địa danh tiếng nước ngoài thì viết sai, chữ 't' bị viết thành chữ 'l'....
Cầm trên tay một tác phẩm bán chạy hàng đầu, nhưng lại mang đến trải nghiệm đọc vô cùng khó chịu. Các bạn nên suy nghĩ trước khi mua. 
3
5066
2016-03-25 21:17:56
--------------------------
393990
6344
Tuy mình chưa mua nhưng mượn đọc của thằng bạn cũng mua ở tiki. Và điều khó khăn nhất là trả lại cuốn sách cho nó :)))) Phải nói từ khi nhập tâm vào cốt truyện thì mình ko thể bỏ nó xuốn được và mất ăn mất ngũ với những tình tiết hấp dẫn và những kiến thức khoa học lý thú và Dan Brown mạng lại.
Điều mình học được từ truyện là đừng vội phán xét người khác cho dù đã vận dụng hết lý lẽ, vốn sống hay điều thói luật lệ của xã hội,... đâu đó vẫn còn một cái sai của bản thân đối với việc chấp nhận sự thật (ở đây, trong truyện là "tội lỗi" của Đức Thánh Cha, và phát minh làm thay đổi thế giới, và nhiều nhiều nữa...). Mọi chuyện có thể đã khác đi nếu mỗi người đều có thể thấu hiểu cho nhau trước đó. Giờ thì mình lại đang dành thời gian tìm hiểu về Hội Illuminati và rất nhiều kiến thức bổ ích khác mà tác giả đưa vào truyện đây.
Mình cũng đang để dành tiền để tậu cuốn này về ^^
5
1096825
2016-03-09 19:17:19
--------------------------
381002
6344
Đây là cuốn sách thứ 3 mà mình mua sau mật mã davinci và biểu tượng thất truyền. Mình thì vẫn mong sau tái bản bìa sẽ đẹp hơn như bản gốc, tuy nhiên khi cầm lên thì cũng khá đẹp, sách dùng giấy tốt nên rất nhẹ nhưng lại không quá mỏng manh. 
Nếu bạn đã từng xem flim thì sẽ không quá thất vọng khi đọc cuốn sách này, có nhiều nội dung của sách bị lược bỏ trong flim nên khi xem có thể bạn sẽ không nắm được hoàn toàn tác phẩm, nhất là những cái chết của các vị hồng y.
3
584052
2016-02-16 12:55:58
--------------------------
367298
6344
Đây là một trong số cuốn sách dày hiếm hoi mà mình bị cuốn đến tận trang cuối cùng. Thật sự cứ mở ra là không đóng vào nổi vì cảm xúc nó cứ tăng dần đều từ đầu đến cuối nên nếu gập sách lại mình có cảm giác sẽ lỡ mất một điều gì đó.
Còn nội dung thì không phải bàn, hoàn toàn choáng ngợp và nó thuyết phục đến nỗi mình cứ tâm niệm rằng những gì tác giả viết là sự thật. 
Truyện còn cung cấp một lượng kiến thức lớn về tôn giáo, về nghệ thuật và biểu tượng một cách rất thú vị
Nói chung là tuyệt vời, mình hoàn toàn recommend, đặc biệt đối với những ai thích truyện trinh thám.
Nhìn sách dày vậy chứ đọc hết lúc nào không hay
5
1106079
2016-01-11 22:13:24
--------------------------
344943
6344
Dạo này Tiki hay cháy hàng quá nên cũng canh chừng lắm mới đặt mau đươc cuốn này . Mình củng chưa đoc xong nên cũng không nhận xét nhiều ve nội dung , nhưng phải nói cuốn sách là cả 1 kho tàng kiến thức đồ sộ , tình tiết rất la hấp dẫn , đan xen hồi hộp gay cấn . 
  Mình thích nhất là bìa truyện , rất nổi bật , bìa rời . Nhưng mình hơi tiếc là bìa của mình nó hơi bị móp một chút nhưng khong sao
Cuốn sách này rất đáng để mua
4
2180
2015-11-29 14:07:54
--------------------------
330709
6344
Thật sự đây là tác phẩm mình thích nhất trong bộ 3 tác phẩm : Biểu tượng thất truyền, Thiên thần và ác quỷ, và Hỏa Ngục của nhà văn Dan Brown.
Tình tiết rất lôi cuốn người đọc cộng với kho kiến thức về kiến trúc văn hóa lịch sử đồ sộ của tác giả, ta dễ dàng lạc vào mê cung những bí mật về giáo hội Vatican.
Truyện có tiết tấu nhanh nhất trong 3 bộ truyện, và có 1 cái kết rất hợp lý với người đọc chứ không phải là những cái kết cụt ngủn như 2 bộ truyện kia.
Bạn sẽ bất ngờ với trùm cuối của quyển sách này ( riêng mình thì không, hehe ).
Sách in rất đẹp, giấy rất tốt, đây là 1 trong những quyển sách mà mình rất hài lòng về chất lượng in.
5
605896
2015-11-03 09:14:12
--------------------------
312267
6344
Đây lần đầu mình cố gắng đọc một cuốn sách dày như thế, và viết về thể loại khoa học viễn tưởng. Lúc đầu với những người thiếu kiên nhẫn như mình thì rất khó để nhớ được các tên nước ngoài trong truyện. Nhưng khi đọc được 1 khoảng nội dung thì mình như bị cuốn theo nó và hồi hộp xem tình huống tiếp theo như thế nào. Dù đọc nhiều lần rồi nhưng mình vẫn thấy hồi hộp như lần đầu vậy. Tác giả viết truyện thật là 1 thiên tài dùng chữ. Nhưng so với bản cũ, cái bìa phiên bản này không đẹp bằng.
4
367570
2015-09-20 23:36:53
--------------------------
310706
6344
Bìa cứng chắc chắn và đẹp. Ruột giấy vàng đọc không hề mỏi mắt. Tuy nhiên mình cảm thấy khó chịu khi phần đánh máy mắc một số lỗi chính tả không đáng có. Hy vọng lần tái bản sau sẽ hoàn hảo hơn. Về phần nội dung thì đúng phong cách của Dan Brown. Mạch truyện dồn dập, gây cấn. Tác giả đã lồng vào đó rất nhiều kiến thức thuộc nhiều lĩnh vực khác nhau như tôn giáo, vật lý, kiến trúc, lịch sử mà không hề gây cảm giác nhàm chán cho người đọc. Nếu ai thích Dan Brown thì chắc chắn phải thích Thiên thần và ác quỷ.
4
641297
2015-09-19 18:57:23
--------------------------
308873
6344
Mình mua tác phẩm này vì Tiki giới thiệu và đọc được những lời khen của các độc giả Tiki. Mình vẫn chưa đọc mà nhận xét không biết có được đăng không nhỉ? Nói về hình thức cuốn sách thì rất đẹp.Bìa cứng, giấy vàng, sách khổ lớn nhìn rất giá trị. Độ nổi tiếng của Dan Brown không cần phải bàn nên chắc chắn nó sẽ rất hứa hẹn. Mình chưa đọc vì dạo này không có nhiều thời gian. Mình muốn dành lúc rảnh rỗi mới đọc nó bởi mình sợ nếu bắt đầu rồi không buông được sách, lại phải thức đêm thì khổ lắm! Dù gì vẫn cảm ơn Tiki và các bạn đã mang đến cho mình cuốn sách giá trị này!
5
724772
2015-09-18 21:23:57
--------------------------
308401
6344
Thích cuốn này nhất trong các tác phẩm của Dan Brown, nên ngày khi vừa biết Thiên thần và ác quỉ được tái bản, mình đã đặt mua trên Tiki, vừa được bao sách vừa được mua giảm giá. Bìa sách thật sự rất đẹp, cứng cáp, phông chữ rất ấn tượng. Nhưng đáng tiếc là cuốn của mình bị lệch bìa một chút. Còn về nội dung thì khỏi bàn. Truyện của Dan Brown luôn cuốn hút mình ngay từ những tình tiết đầu tiên, hấp dẫn và nghẹt thở cho đến phút cuối cùng. Đây cũng là cuốn dày nhất trong các tác phẩm của ông.
4
172460
2015-09-18 17:35:04
--------------------------
304459
6344
Đây là tiểu thuyết đầu tiên mình đọc của tác giả Dan Brown. Quả thực như những gì mà mọi người đã nhận xét về nó. Quá lôi cuốn, nghẹt thở và đem lại nhiều kiến thức sâu rộng. Mình sẽ tiếp tục đọc và sưu tầm những tiểu thuyết còn lại của tác giả như mật mã davinci, biểu tượng thất truyền, hỏa ngục, pháo đài số... Sách bìa cứng rất chắc chắn và hình ảnh được thiết kế đẹp mắt. Bên trong chữ in to rõ ràng trên nền giấy hơi rám ngã vàng. Mình rất thích sách có ruột giấy loại này, vì có mùi giấy và đọc ít mỏi mắt.
5
701139
2015-09-16 13:52:16
--------------------------
302711
6344
Đúng như tiêu đề nhận xét, đây là tác phẩm mình thích nhất của Dan Brown. Tuy mình không có hứng thú với cả tôn giáo và khoa học nhưng sự kết hợp hai yếu tố này của Dan Brown lại hấp dẫn mình. Với một lượng kiến thức khổng lồ, cộng với những tình huống hồi hôp đến nghẹt thở, còn xen cả một số chi tiết hài hước nhẹ nhàng, Thiên thần và ác quỷ lôi cuốn mình ngay từ lúc mới đọc những trang đầu. Robert Langdon cũng là nhân vật mình rất thích, am hiểu nhiều và có tính cách thú vị. Bản bìa cứng lần này mình mua nhìn dày dặn nhưng vẫn nhẹ, bìa đẹp, được đóng gói cẩn thận và giao hàng nhanh chóng. Mình rất hài lòng.
5
234502
2015-09-15 13:39:54
--------------------------
294182
6344
Lại một tác phẩm khác của Dan Brown mà mình vừa mua bổ sung cho bộ sưu tập "Dan Brown" của mình. Mặc dù không quan tâm nhiều lắm đến các tôn giáo và sự hình thành của nó nhưng khi đọc "Thiên thần và ác quỷ" tôi vẫn không thấy nhàn chán mà lại thấy rất hấp dẫn, kịch tính và lôi cuốn nhờ lối hành văn và cách đan xen các tình tiết rất tài tình của Dan Brown. Để viết được một tác phẩm như vậy ắt hẳn Dan Brown phải rất ham hiểu đến khoa học, vật lý cũng như tôn giáo.
5
625458
2015-09-09 07:40:45
--------------------------
290377
6344
Đây là tác phẩm thứ hai mình đọc sau cuốn Mật mã Davinci của tác giả Dan Brown. Từng xem phim Thiên Thần Và Ác Quỷ rồi, nhưng vẫn muốn tìm đọc sách để biết thêm chi tiết hơn về tác phẩm. Khi nhận được sách, mình khá bất ngờ về kích cỡ to và dày của sách. Bìa cứng rất đẹp và chắc chắn, giấy vàng xốp nên khá nhẹ khi cầm trên tay. Nội dung tác phẩm xoay quanh một câu chuyện đầy li kỳ và kịch tính mang đậm tri thức giữa khoa học và tôn giáo, giữa Công giáo và Hội kín. Tác phẩm đưa người đọc đến với quốc gia Vatican với nhiều kiến trúc cổ đẹp, hiểu biết thêm nhiều kiến thức bổ ích. Tác giả phải rất am hiểu về các lĩnh vực khoa học và tôn giáo nên mới có tác phẩm sâu sắc và hay đến vậy. Một tác phẩm hay và đáng để bỏ thời gian đọc. 
5
575705
2015-09-05 12:18:21
--------------------------
281078
6344
Là một Fan của Dan Brown từ khi đọc xong "Mật mã Dan Vinci", mình đã bắt đầu sưu tầm toạn bộ tất cả các tác phẩm trong bộ sách của Dan Brown mà không hề ngần ngại về chất lượng, giá cả. Trong đó, Thiên thần và Ác quỷ là cuốn thứ 2 mình ưu tiên mua trước nhất, cũng như tác phẩm trước đó, cuốn sách này là một kho tàng kiến thức về khoa học, mật mã, tâm linh, tôn giáo, kiến trúc, lịch sử,..v..v.. nhưng lại không hề nhàm chán, các kiến thức này được xem kẽ một cách vô cùng khoa học trong các tình tiết câu chuyện để tạo nên sức hấp dẫn lạ kỳ, vô cùng gay cấn. Một cuốn sách đáng đọc.
5
441722
2015-08-28 08:46:38
--------------------------
277904
6344
Mình biết đến tác giả Dan Brow do một người bạn giới thiệu. Lên mạng tìm kiếm thì thấy cuốn sách "Thiên Thần Và Ác Quỷ" được đánh giá rất cao tôi bèn lên tiki đặt mua về. Khi đọc tôi không thể ngờ được cuốn sách lại lôi cuốn đến thế, rất nhiều kiến thức về khoa hoc công nghê, tôn giáo đã làm tôi phải tò mò. Khi đọc song cuốn sách này tôi đã tự nhủ mình sẽ là fan ruột của tác giả Dan Brow mất. Có điều mình không hài lòng ở cuốn sách đó là chất lượng giấy chưa được tốt, có vài trang bị mờ chữ làm mình có cảm tưởng như đang đọc sách lậu. Hơi buồn cho một cuốn sách hay.
4
207333
2015-08-25 14:04:33
--------------------------
275522
6344
Mình rất thích các tác phẩm của Dan Brown nên đã mua về đọc và sưu tầm luôn. Cũng như các tác phẩm thành công khác, cuốn sách mang đến cho người đọc sự lôi cuốn trong từng chương, từng câu thoại. Mình đã rất phấn khởi đọc từ chương đầu tiên và một mạch đến chương cuối cùng. Quả thật cái tội ác mà hung thủ gây ra thật đáng sợ, nhưng cũng gây không ít tò mò. Mặc dù đang ở thời điểm sau cuốn sách được viết khá lâu nhưng mình vẫn thấy rất bất ngờ trước những tiến bộ vượt bậc của khoa học. Mình thích cách viết thực trong ảo và ảo xen thực ấy, luôn cung cấp những tri thức và ý tưởng mới cho người đọc, thật đáng ngưỡng mộ :D
5
66202
2015-08-22 22:41:13
--------------------------
274278
6344
Những ai là fan của tiểu thuyết tinh thám hay thậm chí là kinh dị thì không nên bỏ qua tác phẩm này. Nghẹt thở trong từng trang sách. Mình đã đọc qua tất cả những tiểu thuyết của Dan Brown, nhưng đây là cuốn mà mình đánh giá cao nhất khả năng xây dựng cốt truyện và nhân vật của tác giả.Thực sự là Thiên thần và Ác quỷ đã vượt qua giới hạn chỉ là một quyển tiểu thuyết thông thường, nó chứa đựng những tri thức tôn giáo, mật mã và cả những hiểu biết về xã hội đáng kinh ngạc. Rất nên đọc!
5
193218
2015-08-21 20:32:08
--------------------------
268769
6344
Ngày nhận được cuốn sách tôi như nhảy cẫng lên vì sung sướng. Sung sướng hơn khi xem lại thì biết tác phẩm này đã gần như cháy hàng. Tôi đã xem phim" Thiên Thần và Ác quỷ " nhưng tôi vẫn chọn mua sách để đọc. Vì tôi tin sách khiến cho trí tưởng tượng thêm phong phú hơn và cái hồn của Dan Brow hơn. Thiên thần và Ác quỷ là một cuộc chiến âm thầm nhưng đầy nghẹt thở bên trong tòa thánh Vatican. Đừng hỏi tôi về chi tiết cụ thể mà hãy cảm nhận khi dã đọc xong tác phẩm. ^^ 
Trong tiếng nhạc du dương của bản nhạc giao hưởng, cùng tách trà nóng, tay mân mê từng trang tiểu thuyết và nghiềm ngẫm từng câu từng chữ...ôi quá là tuyệ vời.
Tin tôi đí. Rất là tuyệt vời :)))
5
596522
2015-08-16 20:33:07
--------------------------
258280
6344
Theo bản thân mình, về mặt thương mại thì Thiên thần và ác quỷ vẫn thua Mật mã Davinci về doanh số ấn bản được bán ra, bởi lẽ Mật mã davinci mang quá nhiều yếu tố gây tranh cãi về nguồn gốc tôn giáo. Còn về phần nội dung, theo mình cảm nhận, Thiên thần và ác quỷ là tiểu thuyết hay nhất của Dan Brown. Nó xen kẽ kiến thức tôn giáo và tình tiết câu chuyện lẫn nội tâm, đặc tả tính cách nhân vật rất hài hòa. Dan Brown lại không làm được như thế với các tác phẩm sau về cuộc phiêu lưu của giáo sư Robert, về bìa thì đúng là không thể đẹp hơn được rồi. Mình rất ưng ý!
5
292668
2015-08-08 13:12:16
--------------------------
257985
6344

Người dịch:		Văn Thị Thanh Bình
NXB:			Văn Hóa Thông Tin
Thể loại:		Trinh thám 
Bìa:			Đẹp – Ấn Tượng
Chất lượng in, giấy: Tốt
Chất lượng dịch:	Tốt
Nhận xét:		Đã đọc cuốn này hai lần và xem phim nhiều lần. Mỗi lần đều tìm thấy một nét hay riêng của tác giả cũng như của nhà làm phim. Và quang trọng hơn cả, thông điệp ấn phẩm đem lại: Ta là ai: Thiên Thần hay Ác Quỷ? Không dám tự xưng là thiên thần nhưng cũng không muốn là ác quỷ, thế nhưng đôi khi ta cũng ác như quỷ và thi thoảng ta cũng tốt hơn cả thiên thần. Sách của Dan Brown lúc nào tình tiết cũng nhanh đến nghẹt thở và đầy trí tuệ.
Khuyên: 		Quá hay, phải đọc.

5
129879
2015-08-08 09:41:28
--------------------------
251135
6344
Với sự quảng bá rộng rãi cùng với danh tiếng của tác giả Dan Brown thì mình nghĩ rằng không có gì để nghi ngờ độ hot cũng như chất lượng của cuốn tiểu thuyết như thế này. Quả thực là một quyển tiểu thuyết này là kì tích luôn ấy. Trong câu chuyện lần này, Dan Brown lại một lần nữa chứng minh tài năng của mình qua diễn biến, cốt truyện và song song là sự lôi cuốn, các tình tiết logic một cách hoàn hảo. Thiên thần và ác quỷ thực sự mà tác giả nhắm đến là ai? Đó là câu hỏi mà mình luôn tự hỏi trong quá trình theo dõi câu chuyện này.
5
120193
2015-08-02 14:23:40
--------------------------
246626
6344
Truyện Thiên thần và ác quỷ của Dan Brown chứa đầy những thủ đoạn ác độc, từng trang sách chập chờn bóng dáng của quỷ dữ từ những đường hầm bóng tối của hội kín. Hận thù và tham vọng  đã khiến cho cuộc chiến giữa tôn giáo và khoa học dai dẳng và ngày càng khốc liệt hơn. Với kiến thức về tôn giáo, nghệ thuật và cả những lĩnh vực khoa học công nghệ cao Dan Brown đã thu hút người đọc qua từng mê cung bí ẩn, căng thẳng và khiếp sợ của một cốt truyện gây cấn, với những phân tích tâm lý sâu sắc và tư duy phán đoán tài tình của nhân vật Robert Langdon. Trong mỗi con người ranh giới giữa thiện và ác thật sự quá mong manh, là thiên thần hay ác quỷ là tuỳ vào tham vọng và ước nguyện của bản thân. Đây là tiểu thuyết trinh thám hình sự đầy màu sắc bí ẩn nơi thánh đường , hãy xem phim và truyện để cảm nhận tác phẩm này kĩ càng nhé các bạn.
5
521993
2015-07-29 18:19:28
--------------------------
244849
6344
Truyện của Dan Brown thật sự cuốn nào cũng hay và rất hồi hộp. Thiên thần và ác quỷ là cuốn sách mà mình phải thức đêm để đọc cho hết. Truyện có tình tiết hồi hộp, có cao trào, thắt nút và cái kết thật sự làm người đọc bất ngờ. 

Nội dung truyền tải của truyện không những mang tính trinh thám hồi hộp mà còn đầy tính nhân văn. Niềm tin vững chắc để nuôi sống tâm hồn của con người được thể hiện ở nhiều phương diện. Truyện cho người đọc thấy trong tâm hồn mỗi người đều có cả thiên thần và ác quỷ, khoảng cách mong manh vô cùng, để nhắc nhở con người cần sáng suốt trong mọi hành động và quyết định.
5
199346
2015-07-28 15:19:50
--------------------------
242637
6344
Cuốn Thiên thần và ác quỷ của Dan Brown lôi đọc giả vào thế giới của những điều bí ẩn trong hành trình của giáo sư Robert Langdon. Một cuộc hành trình lôi cuốn đến nghẹt thở, được nhận xét là nếu đọc sẽ không thể bỏ xuống. Cầm trong tay cuốn sách mà đọc tới rạng sáng, một cảm giác rất tuyệt vời. Lần này, Robert phải đối mặt với tôn giáo lớn nhất thế giới, với những con người được vô hạn tôn kính. Mỗi con người đều có một cuộc sống riêng, một cách nghĩ riêng biệt. Ranh giới giữa thiên thần và ác quỷ, giữa tốt và xấu, ......đều rất mỏng manh. Không ai là đúng hoàn toàn, cũng không hẳn là sai hết tất cả. Dù là giáo hoàng được mọi người tôn sùng cũng có những bí mật riêng........Một cuộc đấu thật hấp dẫn, đúng chất của Dan. 
5
722037
2015-07-26 20:40:37
--------------------------
241706
6344
Thiên Thần Và Ác Quỷ là một cuốn sách rất hay mang nội dung trinh thám, hình sự. có thể nói Thiên Thần Và Ác Quỷ là tác phẩm hay nhất của Dan Brown. Khi đọc nó mình có cảm giác rất lôi cuốn, những sự kiện được liên kết một cách chặt chẽ. Nếu ai chưa đọc Thiên Thần Và Ác Quỷ thì nên mua một cuốn để đọc đảm bảo sẽ bị nó mê hoặc. Nói chung là nội dung sách rất hay nhưng còn 1 số thiếu sót như còn có lỗi chính tả, một vài trang đầu tiên bị dính với nhau. nhưng với Thiên Thần Và Ác Quỷ thì đó không còn là vấn đề. Mình cho 5 *
5
673828
2015-07-25 19:33:23
--------------------------
239801
6344
Thiên Thần và Ác Quỷ là cuốn sách khởi đầu cho loạt sách về vị giáo sư Robert Langdon. Nội dung của cuốn sách quá hay và đầy lôi cuốn, kịch tính và li trì trong các tình tính cao trào. Ngoài ra, một lượng lớn kiến thức về tôn giáo, khoa học,... được đề cập khiến tôi đôi lúc choáng ngợp, nhưng nó vẫn mang tính chất tham khảo khá tốt, mở rộng tri thức. Quả thật, ranh giới giữa thiện và ác, thiên thần và ác quỷ quả thật rất mong manh, chúng dường như đan xen và luôn xuất hiện mâu thuẫn nhưng thống nhất trong bản chất con người. Tuy nhiên, bên cạnh bìa được thiết kế khá đẹp, điều khiến tôi khó chịu là một số lỗi chính tả, lỗi in ấn từ nhà xuất bản. Với nữa, sách thật nhẹ?
4
276840
2015-07-24 09:02:34
--------------------------
233883
6344
mình đọc sách này từ hồi lớp 6 nhưng hồi ấy là đọc trực tuyến. bây giờ mới mua sách. mua rồi thì thấy thích lắm. mỗi tội giấy sách hơi chán nhưng phông chữ với cỡ chữ thì tuyệt. nội dung thì khỏi bàn. trong các cuốn của Dan Brown thì mình thích cuốn này nhất, có thể là do mình đọc tác phẩm này đầu tiên. nội dung sách hay, gay gấn cũng gắn với nhiều vấn đề tôn giáo, cuộc đấu tranh giữa đức tin tôn giáo và khoa học. hãy mua nó đi rồi bạn sẽ biết.
3
144795
2015-07-19 23:21:50
--------------------------
221166
6344
Khi đọc cuốn này của Dan, ông đã cho người đọc đến một chân trời mới. Các bí mật được giải mã thật ly kỳ và hấp dẫn, độc đáo. Lần này một lần nữa Dan lại làm người đọc si mê từ đầu đến cuối truyện. Tình tiết đan xen đầy bất ngờ, khiến bạn không thể bỏ ngang xương quyển sách dù là bất cứ chỗ nào! Kết thúc rất hay làm thay đổi hoàn toàn dự đoán của người đọc, gây ấn tương cực kỳ. Bên cạnh đó, bìa sách rất đẹp, bìa cứng dể bảo quản và chi tiết unique :)
5
508055
2015-07-03 15:38:54
--------------------------
215104
6344
Thấy thương 4 vị hồng y giáo chủ quá nhiều luôn. Khi đọc đến đoạn sát thủ ra tay với vị hồng y giáo chủ cuối cùng, mình đã cầu mong ông được sống, thật sự là quá tội. Đọc truyện thấy mở mang đầu óc được nhiều, kiến thức về nghệ thuật Tây phương thời xưa chi tiết mà hấp dẫn dễ sợ, tình tiết truyện thì gay cấn khỏi nói, túm lại là quá ưng luôn, ban đầu là mình đọc truyện mượn của nhỏ bạn, ghiền quá nên mua luôn 1 quyển về để dành đọc lại, sau truyện này mình thành fan của Dan Brown luôn.
5
153209
2015-06-25 21:44:30
--------------------------
212075
6344
Đây là một tuyệt phẩm cho những người đam mê tiểu thuyết trinh thám. 
Theo em thì cuốn sách này còn hay hơn cả cuốn "Mật mã davinci".
Quyển sách đã cho ta thấy hành trình phiêu lưu của ông Langdon để chống lại những người đã tự xưng là thành viên của hội kín  Illuminati đã diệt vong sau những càn quét của giáo hội.
Những người đọc cuốn này có thể thấy nó có môtíp khá giống với quyển  "Mật mã davinci". Đó là đều đi với 1 cô gái và kẻ xấu chính là người gần gũi với chúng ta nên đoạn đầu có vẻ hơi chán nếu người nào đã đọc trước quyển davinci.
Cuối cùng thì em thấy quyển sách tuy to nhưng rất nhẹ, cầm rất thoải mái và bìa sách rất đẹp. Đối với em thì cuốn sách được 4 *
4
509506
2015-06-21 17:57:53
--------------------------
210438
6344
Nếu là người đọc trinh thám thì không thể bỏ qua cuốn sách này!.
Đây là một trong những cuốn sách hay nhất của Dan Brown sau Mật mã Davinci.
Người đọc Dan Brown chắc không còn xa lạ gì với nhân vật Robert Langdon và những chuyến phiêu lưu của ông. Ở câu chuyện này, là một cuộc phiêu lưu tại Vatican.
Có điều, tôi không thích NXB văn hóa thông tin lắm. Giấy không được đẹp cho lắm, cả cuốn Pháo đài số và Thiên thần và ác quỷ đều của NXB Văn hóa thông tin. Hơi buồn vì giấy hơi vàng đen.
5
492239
2015-06-19 14:44:00
--------------------------
200710
6344
Đây là một câu chuyện với đầy đủ những yếu tố giật gân hấp dẫn nhất mà người đọc có thể biết tới - cuộc phiêu lưu đầu tiên của giáo sư Robert Langdon tại Vatican. Dan Drown luôn biết cách làm người đọc thỏa mãn với những tình tiết mạo hiểm ly kì xuyên suốt toàn bộ câu chuyện thông qua những âm mưu chồng chất và cách hé lộ chúng giữa những khoảnh khắc lóe sáng trên từng mặt của viên kim cương Illuminati.

Thiên Thần và Ác Quỷ có thể nói là một trong những cuốn sách hay nhất của Dan Brown với một khối lượng kiến thức đồ sộ và đầy tính logic minh họa cho vấn đề gây tranh cãi giữa khoa học và tôn giáo.

Về mặt hình thức, tôi phải thừa nhận là sách nhẹ, trình bày bìa đẹp. Tuy nhiên có một vài lỗi trong khâu đóng bìa và các lỗi chính tả khi đọc và phần in hình khá cẩu thả so với đòi hỏi của một tác phẩm ăn khách dù đã trải qua 1 lần tái bản. Chất giấy quá dễ bị lão hóa, dễ thấm nước và có khả năng bị mối mọt quá cao. Tuy nhiên với giá tiền 130 nghìn chúng ta không thể đòi hỏi quá cao được.
4
455391
2015-05-25 19:46:28
--------------------------
190783
6344
Theo đánh giá cá nhân của mình thì De Vinci Code và Angel&Demon là hay nhất trong series của Dan Brown. Khi đọc về Illuminati, mình đã phát cuồng về hội kín này cũng như những kí hiệu đặt biệt, một tuyệt tác về đối xứng và kí tự, đặt biêtk là viên kim cương Illuminati, một sự hoàn mĩ tuyệt đối. THeo chân Langdon, nguời đọc được vào trong những thư viện tuyệt mật nhất trên thế giới, được sống lại những kí ức về The Dark Ages, là những  giả dối được che đậy bởi nhưng âm mưu kinh khủng. Qua đó, nguời đọc sẽ nhận ra được một lượng kiến thức uyên thâm của bậc thầy về văn hóa, lịch sử, kí ttự Dan Brown. TUY SÁCH khá dày nhưng cầm khá nhẹ do chất liệu giấy xốp, tuy vẫn còn nhiều điểm trừ về bìa và lỗi chính tả nhưng đây là 1 trong những cuốn xứng đáng nằm trên kêh sách nhà bạn.
5
166935
2015-04-30 11:20:00
--------------------------
184565
6344
Nội dung thì không cần bàn tới nữa, truyện của Dan Brown phải nói là quá hay & lôi cuốn người đọc. Thông tin dàn trải trên nhiều lĩnh vực, từ biểu tượng, tôn giáo đến khoa học. Mỗi cuốn truyện của Dan Brown là 1 cuộc phiêu lưu ly kỳ, hồi hộp, căng thẳng đến nghẹt thở. Tuy nhiên truyện lần này mình mua có những lỗi không đáng có. Thứ nhất là quá nhiều lỗi chính tả, thứ hai là lỗi in ấn( truyện mình mua bị mất các trang từ trang số 177 cho đến trang 192, còn các trang từ 193 cho đến trang 208 thì bị lặp lại 2 lần). Hơi buồn vì mua phải truyện bị lỗi (mình mua tại tiki ngày 7/4/2015)
3
604224
2015-04-17 23:25:43
--------------------------
183161
6344
Lại là một hành trình dài của Robert Langdon, nhưng đặc biệt hơn, lại là hành trình đầu tiên nhất mà độc giả đến với vị giáo sư quyến rũ này. Vẫn là nhịp văn nhanh, lôi cuốn, hài hước nhưng cũng đầy bất ngờ. Chất văn rất riêng Dan Brown. Là sự kết hợp tuyệt vời giữa trí tuệ và thời gian, tình yêu và lòng hận thù, đức tin và sự phản bội. Tất cả đan xen trong tình huống tưởng chừng như bế tắc. Người đọc bị cuốn trôi vào lằn ranh giữa sự sống và cái chết, cái thiện và cái ác. Ranh giới mỏng manh tưởng chừng như rạn vỡ. Nhưng, là một nhà văn tầm cỡ, Dan Brown biết cách để tháo gỡ trong tình thế nguy hiểm nhất, nút thắt được mở bung, khiến người đọc như thở phào. Nhưng cái dư vị của cuộc chiến sinh tử, cái giá của sự thật và cái thiện không phải ai cũng dễ quên. Thiên thần hay ác quỷ, rồi sẽ tự tìm thấy kết quả cho bản thân, cuộc chiến đó, chưa từng một khắc kết thúc.
5
475837
2015-04-15 16:24:51
--------------------------
165865
6344
Quyển sách rất đặc biệt, tình huống gay cấn, bí ẩn, hấp dẫn cuốn hút bất ngờ, ngôn ngữ mạch lạc, giàu nhịp điệu.
Những tình huống, nhân vật, bối cảnh được xây dựng theo lối huyền bí, cùng ngòi bút tài năng, tác giả đã vẽ nên một câu chuyện về thế giới mà ranh giới giữa cái thiện và cái ác dường như không hề tồn tại. Những tình tiết gay cấn tưởng như đã đi vào thế bí, thế nhưng tác giả đã xây dựng những tình huống mới giải quyết một cách hoàn toàn thuyết phục.
Một quyển sách đáng để người ta đọc đi đọc lại mà không bao giờ chán.
4
540359
2015-03-11 12:37:48
--------------------------
143155
6344
nội dung của cuốn sách thì k cần phải bàn đến. quá hay quá lôi cuốn, đã cầm lên là không muốn đặt xuống nữa. Tuy nhiên, bên cạnh những tuyệt vời của tác giả thì xuất hiện k ít lỗi từ nhà xuất bản. Quá nhiều lỗi chính tả trong cuốn sách này. Mới chỉ đọc đến chương 42 thôi nhưng số lỗi chính tả phát hiện được đã k còn đếm được bằng cả số ngón chân lẫn ngón tay rồi. Những lỗi đơn giản thì có thể nhận ra được nhưng những lỗi mà làm sai hoàn toàn ý văn của tác giả thì đúng là một thảm họa. Hy vọng với những lần xuất bản tới nhà xuất bản nên đọc kỹ sản phẩm của mình hơn nữa để tác phẩm của tác giả đến một cách toàn diện nhất với người đọc
3
285607
2014-12-21 21:47:01
--------------------------
136849
6344
Khi nhìn bìa sách tái bản mới này tôi thật sự không thích vì cá nhân tôi thấy bìa cũ đẹp hơn nhưng khi tận tay cầm được quyển sách này thì nó lại rất đẹp. Hình ảnh bên ngoài đẹp hơn trên web. Với cách viết quen thuộc giống những cuốn sách khác của Dan Brown đan xen giữa trinh thám, các vấn đề tôn giáo và tình tiết giải mã đầy lôi cuốn. Trong hành trình này của Robert Langdon là quá trình rượt đuổi kẻ giết người, chạy đua với thời gian và đặc biệt là nhận diện ác quỷ dưới cái mặt nạ của thiên thần. Cái ranh giới giữa thiện và ác quả thật rất mong manh. Người tưởng chừng như một vị thiên thần cứu thế hóa ra lại là một ác quỷ ẩn mình. Lôi cuốn từ đầu đến cuối và cao trào ở đoạn cuối cùng. Truyện của Dan Brown luôn mang đến nhiều kiến thức về tôn giáo, kiến trúc, lịch sử, kí tượng học v.v..
5
392352
2014-11-23 18:24:33
--------------------------
131059
6344
Về hình thức thì cuốn sách không khá khẩm hơn những cuốn cùng tác giả được tái bản của nhà phát hành. Bìa sách tuy đã có sự thể hiện tên tác phẩm nhưng không sắc nét. Các địa danh, tổ chức, ngôn ngữ được chú thích đầy đủ và rõ ràng. Có một số lỗi chính tả làm ảnh hưởng đến nội dung của cuốn truyện. Ví dụ như 5000 và 500 nanogram trong hộp phản vật chất, 55 phút và 50 phút trong vụ ám sát vị hồng y đầu tiên. Đối với những tình tiết ảnh hưởng đến sự sống còn của nhân vật như thế này thì đòi hỏi phải chính xác tuyệt đối. Tuy nhiên tôi vẫn đánh giá 5 sao vì phần nội dung của tác phẩm.
So với tác phẩm đầu tay thì cuốn sách này của Dan Brown thực sự đột phá về nội dung. Tác phẩm thể hiện lượng kiến thức sâu rộng của tác giả về các công trình nghệ thuật nổi tiếng trên thế giới. Sự kết nối các tình tiết thật tài tình và đầy kinh ngạc. Mọi thứ tưởng như đã đi vào bế tắc, tưởng như đã kết thúc mà hóa ra không phải vậy. Phải đi đến tận cùng của tác phẩm bạn mới thấy rõ tài năng xoay chuyển tình thế bậc thầy mang đầy yếu tố bất ngờ của Dan Brown. Những con người ngỡ là ác quỷ thực ra lại là thiên thần, những bóng dáng tưởng là thiên thần lại mang trong mình những kế hoạch ác quỷ. Hai phạm trù này không thực sự tách bạch mà đan xen trong mỗi vấn đề. Về tuyến nhân vật, Vittoria tuy là người đồng hành nhưng không có quá nhiều ảnh hưởng tới quá trình giải mã những hình ảnh ám thị, mà chủ yếu dựa và sự phán đoán và suy luận của Langdon. Tác phẩm có đan xen một vài tình tiết tình cảm nhưng ngôn từ chưa được linh hoạt và mượt mà (có lẽ do đây không phải là lĩnh vực thế mạnh của tác giả).
5
90029
2014-10-22 09:27:31
--------------------------
