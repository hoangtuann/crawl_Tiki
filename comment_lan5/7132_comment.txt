485299
7132
Đây là một cuốn sách hay và ý nghĩa với rất nhiều câu chuyện mang nội dung sâu sắc. Nó mang đến cho chúng ta những bài học hay rèn luyện cho ta các kĩ năng sống. Mỗi câu chuyện là một bài học ý nghĩa. Nó lôi cuốn người đọc còn bởi các câu chuyện thú vị qua các hình ảnh ngộ nghĩnh. Bìa sách được thiết kế rất tinh xảo với chất liệu giấy tốt. Giấy nhẹ nhưng đẹp và dễ đọc. Đây là cuốn sách mà mỗi chúng ta nên có để làm hành trang vững vàng bước vào cuộc sống.
3
1218232
2016-10-01 12:44:02
--------------------------
472907
7132
Tủ Sách Sống Đẹp - Cửa Sổ Tâm Hồn, 1 tác phẩm tuyệt vời chứa đựng nhiều bài học nhân văn sâu sắc. Thấm nhuần tư tưởng nhân văn của mỗi con người. Khiến chúng ta phải suy nghĩ về bản thân mình. Liệu mình đã tốt chưa, hay chúng ta còn bao nhiêu điểm mạnh, yếu mà chúng ta chưa biết. Tuy tất cả mọi người đều không hoàn hảo nhưng các bạn đừng đứng đó chờ sợ chợ giúp mà hãy vượt qua nỗi mặc cảm ấy và tiến về phía trước. Chính chúng ta mới là người có thể lấp đầy những thiếu sót của bản thân mình. Đó là những điều mà tôi được qua tác phẩm này.
5
1486626
2016-07-10 07:40:24
--------------------------
424184
7132
Ngày còn nhỏ, tôi có một quyển sách mang tên Quà tặng cuộc sống, bìa màu hồng, như ng sau này tôi bị mất cuốn sách đó năm tôi học lớp 12, cách đây 4 năm. Nhưng thật may mắn, như một sự tình cờ, cuốn sách này chứa đựng tất cả các mẫu chuyện như trong cuốn sách cũ của tôi. Không những vậy, nó còn chứa đựng nhiều mẫu chuyện khác mà sách cũ của tôi không có.
Tôi nghĩ với áp lực công việc trong cuộc sống như ngày nay, mỗi chúng ta rất cần những cuốn sách như thế này, để mỗi chúng ta tĩnh lặng tâm hồn và nhìn lại mình. Đọc cuốn sách này, kết hợp với những nốt nhạc không lời dương thì còn gì bằng. Hãy đọc sách và cảm nhận rằng chúng ta hạnh phúc và may mắn hơn rất nhiều người. Để rồi bản thân mình sẽ học được cách yêu thương nhau hơn, vượt qua mọi khó khăn trong cuộc sống !
5
765532
2016-04-30 23:16:23
--------------------------
402319
7132
Có những cuốn sách đọc khi bạn quá mệt mỏi, đọc để xoa dịu một chút tâm hồn thif đây là một cuốn như vậy.
Không mang quá nhiều thông tin hay thông điệp nặng nề, mỗi câu chuyện ngắn, nhưng súc tích này mang một lát cắt nhỏ của tâm hồn. Có hỉ, nộ, ái, ố nhưng chung quy lại đều là nhứng tâm hồn đẹp, hướng người đọc tới cái đẹp.
Đôi khi để không làm trái lòng mình chỉ cần những hành động nhỏ thôi nhưng ý ngha của nó lớn biết chứng nào.
Đây là một cuốn sách hay, đáng đọc, tuy nhiên như đã nói ở trên, sách có thể đọc ỏ đâu cũng được, trên xe buýt, khi rảnh rỗi... nhưng khổ sách khá lớn và dày lại thành điểm trừ. Nếu được in ấn với chất giấy nhẹ hơn, khổ gọn lại một chút để tiện mang theo bên mình sẽ tốt hơn.
Bìa minh họa  khá đơn điệu, không có độ tinh tế cho một cuốn sách có nội dung như thế này!
4
416211
2016-03-22 07:12:46
--------------------------
266359
7132
Cuốn sách này mình mua là nhờ cô giáo dạy văn giới thiệu, cô bảo mua để học được nhiều điều từ cách hành văn của các tác giả, các bài học,... Hiện tại thì sách chưa về nên mình rất hóng, mới đọc qua nhưng đã thấy thích rồi. Trong Tủ sách sống đẹp thì có cuốn này với cuốn Quà tặng cuộc sống là 2 cuốn hấp dẫn mình nhất, từ tựa đề, bìa sách cho đến mỗi câu chữ bên trong. Mỗi một câu truyện trong đó là những bài học dạy cho ta, vừa đọc vừa ngẫm. Nếu mọi người có ý định mua sách kỹ năng sống mà chưa biết mua quyển gì thì mình nghĩ đây cũng là một cuốn sách đáng để mua đó. Chờ sách về là ngồi nghiền thôi.
3
114829
2015-08-14 16:29:57
--------------------------
230535
7132
Khi còn đi học, mình được thầy giáo yêu cầu sinh viên về tìm đọc cuốn sách "Cửa sổ tâm hồn". Sau khi nhận được quyển sách này từ Tiki, mình đã rất háo hức, hồi hộp, không biết cuốn sách này có hay không? Hay là nó giống các cuốn "Hạt giống tâm hồn" khác? 
Câu chuyện "Một cuộc đua tài" khiến mình rất quan tâm. Dựa trên một câu chuyện có thật, về một cậu bé không may bị mất cánh tay trái, nhưng nhờ có cô y tá mà cậu bé đã vượt lên mặc cảm, khó khăn và tự tin đối mặt với cuộc sống. Qua đó, giúp mình nhận ra khi dạy một đứa trẻ thì cần nhất là: 
"Biết mà học, không bằng thích mà học. 
Thích mà học, không bằng say mê mà học. 
(Biến việc trẻ phải học thành việc trẻ thích học) thì mới có thể thành công. Hay theo Khổng Tử: "Dĩ thân vi giáo" - Giáo dục bằng chính cuộc sống gương mẫu của bản thân. 
Truyện "Cô gái có một bông hồng" có thể trong cuộc sống chúng ta không nhận ra. Cô gái khiến mình rất khâm phục về sự thông minh và sáng suốt khi thử lòng người yêu. Nếu như chàng trai chỉ vì vẻ đẹp bề ngoài thì sẽ không thể có được tinh yêu thật sự. 
Ba vị thần ước - nội dung nói về các vấn đề thời sự trong cuộc sống hiện nay. Truyện muốn con người nhận ra rằng: "Mang lại hạnh phúc cho người khác là điều đáng quý, song liệu hạnh phúc mà họ mang đến thật sự có ích cho người nhận hay không lại là một chuyện khác. Chính điều đó mới quan trọng đối với cuộc sống con người." Các câu chuyện khác như: Một câu chuyện cảm động; Tại sao phụ nữ khóc? Buổi kiểm tra; Giá trị một giờ đua; Đường đua; Bạn có nghèo không? Bài học từ loài ngỗng; Nước mắt người cha;... cũng rất hay. Chúng thuộc vào loại không chỉ để đọc một lần. 

Chính những câu chuyện từ Cửa số tâm hồn đã mang đến cho tâm hồn mình những khoảng lặng bình yên, để ta nhận ra sức mạnh của nụ cười, của ánh mắt, của bàn tay, của bờ vai chính mình ! Cuốn sách như một nốt nhạc điểm xuyết cho cuộc sống chúng ta thêm tươi đẹp hơn. Mà hình như lúc nào đọc cũng cần: khi vui để biết chia sẻ, khi buồn để thôi bi quan, khi thành công để biết nhìn lại, khi thất bại để biết vượt lên,.. "Cửa sổ tâm hồn" là một trong những cuốn sách cần thiết gối đầu giường của tất cả mọi người, đặc biệt không phải chỉ dành đọc một mình. 

5
408948
2015-07-17 15:31:38
--------------------------
