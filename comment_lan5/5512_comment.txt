442813
5512
Truyện không quá đỗi ly kỳ và rắc rối như những tiểu thuyết trinh thám khác, lời văn dễ hiểu, mạch truyện liền nhau khiến người đọc muốn đọc liền từ đầu tới cuối cuốn truyện.

Bắt đầu từ việc Kathy Culver mất tích bí ẩn, bố của Kathy- ông Adam bị giết chết. Cùng với việc điều tra của Myron và Win, hàng loạt các nhân chứng, kẻ tình nghi liên quan bị đưa ra ánh sáng. Tình tiết truyện không quá đỗi bất ngờ, thủ phạm gây án cũng không quá cao siêu gì nhưng lời văn hấp dẫn khiến truyện hay hơn rất nhiều. Nhìn chung là một tác phẩm trinh thám đáng đọc
4
671890
2016-06-05 17:59:33
--------------------------
382797
5512
Cuốn sách sẽ không làm những độc giả hâm mộ chàng cựu danh thủ bóng rổ-thám tử Myron thất vọng. Đây là quyển đầu tiên trong sê ri về Myron, rất hay đúng với phong cách Harlan Coben. Không giống với những truyện trinh thám khác ,chàng thám tử trong truyện rất đời thường và đa cảm, tốt bụng.Tác phẩm lôi cuốn với nhiều nút thắt bất ngờ,pha chút chất trào phúng và những suy ngẫm sâu sắc của nhà văn về cuộc sống. Rất mong những tác phẩm khác của nhà văn sẽ được dịch và xuất bản ở VN.
5
511049
2016-02-19 18:35:28
--------------------------
351939
5512
Theo mình thì cuốn trinh thám này nếu nói hay thì cũng không phải là quá hay về mặt nội dung,các tình tiết trong truyện không thuộc loại cao siêu gì,dễ hiểu nhưng nó có một sức hút đã đọc là không thể nào bỏ ngay xuống được.Truyện có nhiều nghi phạm tìm được người này lại suy ra người kia và đều có mối liên hệ với nhau,ngoài ra lời văn của truyện cũng rất hài hước,tuy là truyện trinh thám nhưng không hề có kiểu căng não.Cái mình không thích duy nhất ở truyện này là cái kết,mình thấy nó hơi nhanh,nguyên cả truyện điều tra đủ thứ thế nhưng khi phát hiện ra thủ phạm mình lại thấy hơi hụt hẫng,không có gì bất ngờ cho lắm.
4
526322
2015-12-13 10:06:11
--------------------------
314395
5512
Mình hâm mộ tác giả Harlan Coben qua hai cuốn tiểu thuyết trinh thám "Đừng nói một ai" và "Người vô tội". Nhưng khi đọc đến "Nhân chứng đã chết" thị mình thấy hơi hụt hẫng bởi bố cục truyện hơi bị "đầu voi đuôi chuột". Đoạn đầu tác giả cho xây dựng môt loạt các đối tượng tình nghi với các tình tiết lắt léo đan xen nhau. Nhưng đoạn cuối thì như bị hụt hơi bởi động cơ gây án quá lỏng lẻo, không đủ sức thuyết phục. Tóm lại với mình đây là một cuốn trinh thám bj viết hơi vội.
4
93234
2015-09-25 22:49:47
--------------------------
309229
5512
Nhân Chứng Đã Chết là một cuốn truyện trinh thám khá đặc sắc. Truyện hay, hấp dẫn qua từng trang sách làm mình chỉ muốn đọc không ngừng. Ngoài ra, bìa truyện được thiết kế nhìn khá lôi cuốn, khiến cho ta có cảm giác tò mò muốn khám phá xem bên trong sách viết những cái gì. Cuốn sách khá nhiều chi tiết mới mẻ, đọc cũng thấy hơi sợ sợ nhưng rất thu hút, cuốn hút mình. Truyện được đẩy lên cao trào khi việc vợ của Kathy bị lộ ảnh khỏa thân trên một tạp chí khiêu dâm. 
5
451462
2015-09-18 22:50:47
--------------------------
232174
5512
Lại 1 tác phẩm nữa của Harlan Coben làm mình mất ngủ mấy đêm vì mải đọc và cũng sợ nữa. Không hẳn là một cốt truyện mới mẻ nhưng những dòng văn của Coben làm người đọc mê mải lần theo từng câu chữ, từng chi tiết trong câu truyện. Mình thấy đấy chính là thứ tạo nên sự cuốn hút cho tác phần này.
Vợ chưa cưới Kathy của nhân vật mất tích rồi khi sự việc tưởng như lắng xuống thì bức ảnh khỏa thân của cô lại xuất hiện trên một tạp chí khiêu dâm, tác giả đã làm mạch truyện trở nên hồi hộp gay cấn hơn bao giờ hết. Khi đọc mình đã bị cuốn vào vòng xoáy của những nhân vật, hỗn loạn nhưng cuối cùng lại kết lại rất sắc bén.
Đáng đọc.
5
459083
2015-07-18 17:23:25
--------------------------
209588
5512
Cuốn sách này là tất cả những gì tôi muốn trong một cuốn sách tuyệt vời! Sự hài hước nhẹ nhàng ẩn giấu những cái bẫy ngọt ngào, Harlan Coben gợi ý sự thật chứa đằng sau câu chuyện mà không phải mất công giải thích từng sự việc theo lối dài dòng lê thê, chỉ cần đọc đến cuối và mọi sự trở nên thật rõ ràng, thật thông thái.
   Và thậm chí đây là một cuốn sách độc lập hay là một seri tiểu thuyết về Myron Bolita thì nó vẫn quá tuyệt khi đã lột tả hoàn toàn tính cách nhà đại diện thể thao kiêm thám tử này.
5
529844
2015-06-17 20:38:25
--------------------------
161961
5512
Không biết những ai đã từng đọc cuốn này rồi nghĩ sao nhưng mình thấy cuốn trinh thám này khá bình thường, không quá xuất sắc. Truyện dễ đọc, dễ hiểu, không có chỗ nào khó đọc hay chứa quá nhiều kiến thức chuyên sâu khiến độc giả phải đau đầu nên bạn có thể đọc một mạch từ đầu tới cuối. 
Tuy nhiên, mình thấy khá hụt hẫng khi quá trình suy luận, phá án của thám tử Myron kéo dài tới gần hết cuốn sách, trong khi động cơ gây án được tác giả viết trong có 3, 4 dòng. Chưa kể, tác giả còn để cho thám tử Myron quá giỏi, hầu như anh này ít gặp khó khăn, bế tắc gì, cứ tằng tằng mà ra manh mối. Cái kế để hung thủ lộ diện cũng không quá cao siêu (Nhân tiện nhắc tới hung thủ, kẻ này độc giả cũng không quá khó để đoán đâu, nếu bạn đã từng xem nhiều phim, truyện phương Tây). 
Nói chung về cuốn này, ai từng đọc nhiều trinh thám rồi sẽ thấy nó chỉ dừng ở mức độ bình thường, không quá cao siêu. Trừ một vài lỗi chính tả, thì mạch văn, lời người dịch khá hấp dẫn, dù bạn có thể đoán được hung thủ từ trước nhưng vẫn có thể đọc một mạch tới hết cuốn sách. ^^
4
92868
2015-03-01 11:07:51
--------------------------
