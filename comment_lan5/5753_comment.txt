472450
5753
Đây là cuốn sách rất hay và đặc biệt mà tôi biết của dòng văn học sau 1945. Lối viết rất chất phác, giản dị nhưng suy tư, sâu lắng. đọc tác phẩm đôi khi tôi không thể hoàn toàn hiểu được hoàn cảnh và những câu chuyện của tác phẩm nhưng tôi cảm nhận được tinh thần của người viết và nhân vật được nêu lên. Những trăn trở những suy tư về thời cuộc rất nhẹ nhàng, bởi vậy không dành cho ai đọc một cách vội vàng nhưng phải từ từ mà cảm nhận. Cái hay mà tôi biết ở tác giả là ngôn từ rất giản đơn, những dấu ấn của tác giả trong tác phẩm được nhà xuất bản rất chú ý về câu chữ trình bày khi xuất bản đó cũng là ngụ ý, là nét khác biệt không lẫn vào ai được mà chỉ là Trần Dần
5
816829
2016-07-09 16:31:35
--------------------------
383390
5753
Xét về mặt trinh thám, thì chắc chắn tôi không thể cho nó chọn vẹn 5 sao như trên kia, nhưng về mặt tâm lí, thì tôi không hề ngần ngại. Một điều khiến tôi sửng sốt, đó là trong thời điểm ấy, lại có một nhà văn với ngòi bút hiện đại đến thế, một nhà văn có bước đi táo bạo đến thế. Có thể cách sắp xếp đoạn văn câu văn vô trật tự, đảo trật tự quy tắc dấu và chữ cái khiến một vài độc giả ngại ngần hay khó chịu. Nhưng cứ tin tôi, đi hết quyển sách, bạn sẽ nhận được những cảm xúc lớn hơn. Mà như tôi, là tự hào.
5
278389
2016-02-20 17:40:44
--------------------------
331193
5753
Đọc xong Những ngã tư và những cột đèn, tìm hiểu thêm về tác giả Trần Dần mới thấy bất ngờ quá. Không ngờ một tác giả không còn trẻ và trông có vẻ cũ kỹ lại có những áng văn trẻ trung, hiện đại như vậy. Mình đặc biệt thích nhân vật chị vợ Trinh - vô cùng nữ tính và dễ thương như chính cái nick name Cốm do chồng đặt. Đọc tác phẩm thấy toát lên một Hà Nội thật nhẹ nhàng, người Hà Nội thật dung dị. Một câu chuyện thật giản đơn nhưng cũng thật sâu lắng. Nói chung đọc xong cuốn sách này mình có cảm giác cực kì nhẹ nhõm và thoải mái.
5
388069
2015-11-04 08:32:02
--------------------------
281592
5753
thực sự đọc những trang đầu của cuốn sách đầu tiên mình cảm thấy sửng sốt vì không nghĩ ông lại có một lối văn hiện đại, phá cách đến thế; câu chữ và ngôn từ, cấu trúc ngữ pháp, cách đặt dấu phẩy của ông rất khác thường; sau đó có phần bỡ ngỡ, mơ hồ ấy là do lối kể chuyện phi tuyến tính không theo thời gian, không được kể theo kiểu trình tự, mà lại là kiểu bay nhảy từ khoảng thời gian này qua khoảng thời gian khác, từ nhân vật này sang nhân vật khác, qua các hồi tưởng và những mảnh kí ức rời rạc. Nhưng những cái đặc biệt khác thường ấy lại dần khiến mình thích thú, mình thích kiểu sửa hết i dài thành i ngắn và 'đề nghị nxb đừng sửa gì của tôi'  của ông rồi cách ông quan niệm đứng trong với lại đứng ngoài thời gian. Vô cùng thú vị. Trần Dần là người tiên phong trong việc cách tân thơ văn, tác phẩm này của ông là một trong những minh chứng cho điều ấy.
4
400204
2015-08-28 15:20:01
--------------------------
225853
5753
"Những ngã tư và những cột đèn" và cả Trần Dần đều là cái tên còn được biết đến chưa nhiều trong làng văn Việt Nam, cũng bởi một thời gian Trần Dần bị "gạt" ra khỏi đời sống văn chương. Đây được xem như sự trở lại của Trần Dần khi tuổi của ông đã không còn trẻ. Thực tế cuốn tiểu thuyết được Trần Dần viết từ những năm 60 khi ông chọn cho mình lỗi rẽ là tiểu thuyết mà về sau không ai có thể tiếp bước. Tuy là một cuốn tiểu thuyết, nhưng lại mang âm hưởng và ngôn ngữ thi ca. Truyện nhẹ nhàng, lại được viết đúng vào thời điểm mà tác giả đang sống trong khung cảnh thật, nên khiến người đọc có cảm giác mình cũng đang sống về những ngày đó.
5
24486
2015-07-11 00:25:59
--------------------------
195676
5753
Đi quan ngã tư cuối cùng, nhìn rõ mặt "tên nhon-cằm" hiện ra dưới quầng sáng của đèn đường, mình chợt có cảm giác, mình chẳng biết gì về cuộc đời này.

Cuộc đời là một chuỗi những ngã tư, những cột đèn, nơi mà ta luôn hối hận mỗi khi bước qua một ngã tư, vì tội lỗi là, ta đã chọn lựa.

Lúc trước, khi chưa đọc cuốn này, mình đã hơi bị "đánh lạc hướng" về nội dung của nó. Mình đã nghĩ nó mang hơi hướm của "Đêm giữa ban ngày", hay "Ba người khác" hay một số truyện khác nữa mà mình đã từng đọc qua... Nhưng viết nên cuốn sách này, mình nghĩ Trần Dần đã cực kì "gương mẫu". Gương mẫu về chính trị, gương mẫu trong từng câu từ. Trong từng mảnh nhỏ của sự thật cuối cùng mà ông góp nhặt lại. 

Mình thích cuốn sách này, cũng như say mê đoạn văn xanh - tím. 

Bên này của sổ tôi tím, bản thảo lem nhem mực tím, cuốn nhật kí và bản sao nhật kí tím. Bên này cửa sổ tôi xanh, sáu cây bàng xanh lá và vài chiếc xe cam nhông xanh.
5
102543
2015-05-14 12:07:20
--------------------------
