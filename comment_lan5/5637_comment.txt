409873
5637
Tuổi thơ tôi gắn liền với những câu chuyện Đôremon. Những câu chuyện về những bửu bối thần kì đem lai cho tôi những kí ức đẹp về một tươi lai tươi sáng màu nhiệm. Những mẫu chuyện Đôremon bạn có thể đọc đi đọc lại đọc bất cứ lúc nào. Bạn không cần đọc theo thứ tự tập mà cứ thế có cuốn nào là ôm đọc mãi không chán. Tôi đã mua cả bộ truyện Đôremon này để cho em tôi đọc. Rất hay. Tiki giao hàng và bảo quản hàng rất tốt. Cảm ơn Tiki rất nhiều.
3
958480
2016-04-03 10:29:20
--------------------------
262121
5637
Cuốn truyện tranh doremon này quá hay.đặc biệt là " Vĩnh biệt bé cây".Câu truyện khá là cảm động,dạy trẻ phải biết yêu thiên nhiên,yêu cây cỏ hoa lá. Với những hình vẽ cực kỳ ngộ nghĩnh,đáng yêu,những biểu cảm của doremon và nobita,giúp mỗi chúng ta cảm thấy rất thân thiết.Cuốn truyện in bìa rất đẹp,bóng loáng,hình vẽ bìa cũng dễ thương nữa.Giá của cuốn truyện khá là rẻ,chỉ 16000đ,tuy thế niềm vui mà doremon mang lại là không ít.Mình rất cảm ơn nhà sách tiki đã mang cho mình cuốn truyện này.Mong nhà sách sẽ nhập thêm để mình có thể mua nữa chứ doremon hết hàng mất rồi.Cảm ơn tiki với cuốn truyện tranh gắn liền với thời ấu thơ này
5
395184
2015-08-11 17:40:15
--------------------------
