558197
3674
Đây là một trong những cuốn sách học tiếng anh mà mình tâm đắc nhất. Sách do chính người Việt biên soạn, lúc trước cũng như nhiều người với tâm lí học người bản xứ sẽ giúp cải thiện khả năng học ngoại ngữ tốt hơn, nên bản thân mình chỉ chọn sách do người nước ngoài biên soạn. Sau này, mình nhận ra rằng cùng là người Việt sẽ hiểu và dễ dàng chỉ ra những lỗi sai cơ bản trong cách sử dụng tiếng Anh hoặc các ngôn ngữ khác. Vì vậy, với cách trình bày rõ ràng và dễ hiểu, cuốn sách này giúp người đọc phân biệt và sử dụng từ ngữ trong tiếng Anh một cách chính xác nhất. Chân thành gửi lời cảm ơn đến tác giả đã bỏ công sức và thời gian để biên soạn cuốn sách này và đem đến nguồn tri thức tuyệt vời về ngôn ngữ học cho người Việt Nam.
5
149872
2017-03-29 19:36:18
--------------------------
555493
3674
Nói chung là mn khen nhiều rồi nên mình nghĩ cho 5 sao là đủ rồi, k cần nói gì thêm.
4
1520348
2017-03-27 10:42:41
--------------------------
490133
3674
Ôi! Mới nhận được sách hôm nay và chỉ mới đọc thử vài trang nhưng đã thấy cuốn sách là cả một kho tàng kiến thức hay. Đọc rồi mới thấy mình chỉ mới hiểu về tiếng anh rất ít và chỉ vỏn vẹn vài trăm trang của sách mà thôi. Sách không chỉ cho biết về ngữ pháp mà còn giúp ta phân biệt các từ trái nghĩa , đồng nghĩa vơi nhau nữa. Đây là cuốn sách giúp ít cho các bạn rất nhiều và giúp chữa lại những lỗi mà ta hay gặp phải nữa chứ. Cuốn sách rất đáng giá tiền cho những ai đang suy nghĩ không biết nên mua hay không và mình khuyên các bạn nên mua sách đi, không uổng tiền đâu
5
858191
2016-11-11 11:04:17
--------------------------
458481
3674
Đây là một cuốn sách cực hữu dụng với những người muốn ôn luyện từ mới như mình. Sách trình bày rất chi tiết, rõ ràng phân biệt sự khác nhau và ngữ cảnh sử dụng các từ dễ gây nhầm lẫn cho chúng ta. Bìa sách dày, cứng cáp, giấy in chất lượng tốt, chữ to rõ nhìn. Vì số lượng từ quá nhiều và có một số từ cũng không phổ biến lắm nên thành ra mình cũng lười lâu lâu mới lại lấy ra coi một vài từ. Hi vọng cuốn sách sẽ giúp từ mới của mình được cải thiện hơn.
5
747189
2016-06-25 10:16:51
--------------------------
449642
3674
Wow! Phải nói là quyển từ điển này cứ bị hết hàng mãi luôn ấy, bởi vậy nên lúc có hàng một cái là mình đặt ngay và luôn. Lúc nhận được tuy lâu hơn so với thời gian dự kiến nhưng mở ra cầm vào thì thấy đã lắm, sách đẹp, bìa cứng, giấy tốt, chữ to nên là ngoài việc cần mới tra từ ra thì mình đọc hàng ngày luôn. Nội dung thì rất dễ hiểu, các ví dụ minh họa cũng rõ ràng. Giá tiền bỏ ra thực sự rất đáng để nhận được một cuốn từ điển đẹp và hữu ích như vậy.
5
298930
2016-06-17 20:38:18
--------------------------
449079
3674
Phải nói đây là một cuốn từ điển khác biệt so với những cuốn từ điển khác. Thay vì chỉ là dịch nghĩa từ tiếng Anh sang tiếng Việt thì quyển này tác giả còn hướng dẫn cho cách sử dụng từ khi ở trong một ngữ cành nhất định. Với cách trình bày chi tiết, dễ hiểu, cuốn sách rất phù hợp cho những người mới học ngoại ngữ và cũng dành cho những người đã học lâu năm có thể nghiên cứu sâu hơn. Điểm đáng nói ở đây, tác giả lại là người Việt nên văn phong sẽ rất gần gũi.
Sách được in rõ nét, chất lượng giấy thì tốt miễn bàn, được dùng giấy bóng nên hơi nặng.
5
527557
2016-06-16 22:42:06
--------------------------
440578
3674
Tôi tình cờ thấy cuốn sách này trên tiki ko qua sự giới thiệu nào, nhìn giá bìa tôi nghĩ ngay đến câu tiền nào của nấy và khi nhận được hàng thì là thế thật. Đây có lẽ là cuốn từ điểm to và dày nhất của tôi cho đến giờ, bìa cứng gáy đóng chắc chắn, giấy in tốt. Đặc biệt bố cục rõ ràng ko làm người khác nhức mắt hay buồn ngủ. Mỗi  từ vựng đều có ví dụ rất dễ hiểu, tuy nhiên không có phiên âm. Cầm cuốn sách trên tay tôi cảm nhận được tâm huyết của người biên soạn ra nó. Đây là cuốn sách đã giúp ích rất nhiều cho tôi. 
5
812036
2016-06-01 23:19:28
--------------------------
427473
3674
Bề ngoài quyển sách cỡ to, đẹp, chữ nhìn đơn giản nhưng lôi cuốn tầm nhìn. Cá nhân tôi thích sách to, rõ vì nhỏ quá khó xem.

Sách in giấy trắng tinh, chữ rõ, mực vừa, thêm cái bookcare nên nhìn rất đẹp, sạch sẽ, ngăn nắp.

Nội dung thì khỏi phải bàn cãi, quá tuyệt cú mèo! Rất có ích cho việc học tiếng anh.

Nếu như bình thường học tiếng anh mất 3-5 năm mới có thể thành thạo thì cuốn sách sẽ là phép màu rút gọn thời gian lại chỉ cần 1-2 năm. Tất cả nội dung cần thiết cho cách sử dụng từ và nghĩa gói gọn trong lòng bàn tay. 

Còn chờ gì nữa mà chưa đặt ngay cho mình một cuốn đi hả các bạn? Anh Văn giờ đây không còn là khó mà trở nên TRONG TẦM TAY.
5
363769
2016-05-09 12:52:50
--------------------------
423611
3674
Lần trước định mua Từ Điển Cách Dùng Tiếng Anh này mà thấy hết hàng nhanh quá, đợt này thấy có hàng lại là mình đã mua ngay kẻo hết. Mới đầu thấy đánh giá cuốn này rất cao, tận 5 sao. Khi mua về mình mới thấy cuốn này xứng đáng cho 5 sao thật. Vì nó thật sự rất công phu, chi tiết về nội dung ngữ pháp. Sách nêu rất nhiều những ngữ pháp giống nhau khó phân biệt trong tiếng Anh. Tôi thấy cuốn này là đầy đủ nhất từ trc tới nay so với các cuốn khác. Sách cũng to, giấy đẹp, bìa cứng. Nói chung rất hài lòng. Rất đáng để mua dành cho những ai muốn học tiếng anh nghiêm túc 
5
854660
2016-04-29 17:37:06
--------------------------
412573
3674
Tôi biết cuốn sách qua một cuốn sách và những người bạn của tôi, tôi đã quyết định mua ngay sau vài lần được cho mượn.
 Sự khác biệt lớn nhất so với cuốn từ điển khác theo tôi là yếu tố văn hóa bản thân tác giả cũng nằm trong ban đối ngoại nên chúng ta có thể tin tưởng nhiều, cuốn sách giải thích rất rõ ràng trong từng ngữ cảnh, phân biệt anh mỹ và anh anh rõ ràng.
 Chất lượng cuốn sách rất tốt hình thức đẹp( đâm ra thích đọc hơn là tra).
  Riêng tôi vẫn thấy tìm từ hơi lâu so với từ điển khác có lẽ phần đề từ chưa thực sự tách biệt ra nên tìm chưa quen.
Các bạn nên mua
4
988955
2016-04-07 22:50:38
--------------------------
403677
3674
Đây là cuốn từ điển đặc biệt nhất mà mình từng mua. Và mình cũng chưa bao giờ thấy cuốn từ điển nào hữu ích tương tự như vậy. Ngôn ngữ phong phú, đa dạng. Phần giải thích cách dùng cực kì dễ hiểu nhờ sử dụng những từ ngữ đơn giản, gần gũi. Đây là một quyển sách được nghiên cứu trong vòng 10 năm nên có thể nói là không thể chê vào đâu được. Mình rất thích  và hài lòng với cuốn sách này. Mình nghĩ những bạn đã và sẽ mua nó cũng cảm thấy như mình vậy.
5
746262
2016-03-24 06:05:41
--------------------------
393639
3674
Sau khi đọc lướt qua quyển Tự Điển tại nhà sách Green & Brown tôi thấy thú vị quá vì nó dẫn giãi từng Chữ và các dùng.Tôi đã đặt mua liên tiếp 2 bộ và bây giờ đặt bộ thứ 3 để tặng cho đứa cháu từ Boston về Việt Nam .Những gì thầy Lê Đình Bì diễn giải làm cho người học cảm nhận được cách dùng như thế nào để hoàn thành câu đúng ý nghĩa,dùng từ thật chính xác .Theo tôi thật thú vị khi có quyển Tự Điển nầy đồng thời ao ước thầy viết dài hơn để đầy đủ những gì người học còn cảm thấy khiếm khuyết .Ngôn ngữ nào cũng có những quy luật riêng không phải ai cũng hiểu hết, nhất là ngôn ngữ không phải là ngôn ngữ bản xứ vì thế thật nhiêu khê để nắm vững quy luật của nó.Tôi hy vọng nhiều bạn học sinh không bỏ qua quyển Từ Điển này và mong Tiki có thể chọn nhiều đầu sách giá trị trong nhiều lĩnh vực để đáp ứng lòng mong đợi của những khách hàng trên cả nước 
5
595908
2016-03-09 03:56:55
--------------------------
389212
3674
Nhìn chung, quyển sách này có khá nhiều ưu điểm cho người sử dụng, đặc biệt là cho học sinh, sinh viên học chuyên ngành tiếng Anh. Quyển sách phân biệt nghĩa và cách sử dụng trong các trường hợp cụ thể của những từ đồng nghĩa không hoàn toàn, từ đó giúp các bạn có thể sử dụng đúng từ nào vào mục đích, ngữ cảnh nào cho thật phù hợp và chuẩn xác nhất. Về hình thức, chất lượng giấy trắng và khá dày nên khó rách khi sử dụng. Thêm vào đó, sách này là bìa cứng và tổng quan cuốn sách cũng khá dày và lớn. Tuy nhiên, cuốn sách chưa cung cấp đủ các từ đồng nghĩa khác cũng cần thiết phải phân biệt rạch ròi hoàn cảnh sử dụng, nên ch]a thật toàn vẹn cho 1 cuốn sách hay.
4
601966
2016-03-01 17:00:21
--------------------------
373815
3674
Mua rồi và sử dụng cả năm nay mà chẳng chê vào đâu được. Cuốn sách giúp mình phân biệt những từ dù có nghĩa giống nhau nhưng dùng khác nhau trong từng trường hợp cụ thể, nhờ vậy mới thấy học từ vựng đâu thể chỉ học cái nghĩa cơ bản là đủ. Bên cạnh phân biệt từ tác giả còn giúp chúng ta hiểu rõ hơn về cách dùng của một số cấu trúc mà sẽ dễ gây nhầm lẫn. Bìa sách dày bảo vệ sách rất tốt, giấy trơn láng, chữ to rõ, trình bày rõ ràng. Mình rất thích cuốn sách này
4
361076
2016-01-24 19:12:04
--------------------------
351050
3674
Mình đã mua cuốn sách này cách đây không lâu và đã xem qua. Nhìn chung thì cuốn sách khá là tốt. Sách cứng lắm nha. To nữa. Nên chữ in rất rõ ràng dễ nhìn. Cuốn từ điển này khá là hay, nội dung tư vựng phong phú, cách trình bày mới mẻ, sinh động, dễ hiểu. Có phần ghi chú cho mình bằng tiếng việt để giúp người đọc dễ hiểu hơn về từ mới. Có cả ví dụ tham khảo về cách sử dụng từ ngữ đó trong các hoàn cảnh, tình huống khác nhau. Bên cạnh đó cuốn sách cho ta thêm kiến thức về những từ có nghĩa giống nhau hoặc gần giống nhau. Rồi chỉ ra cách dùng thích hợp và ví dụ minh hoạ giúp t hiểu rõ và phân biệt tránh sai sót. Cuốn từ điển này không chỉ giúp ta tìm thấy nghĩa của từ một cách nhanh chóng mà còn ghi nhớ được lâu vì sự chi tiết của cuốn sách mang đến. Mình nghĩ nếu ai muốn học từ vựng hay muốn trau dồi vốn từ nhiều thì nên mua cuốn sách này. 
4
717149
2015-12-11 16:39:57
--------------------------
344578
3674
"Từ điển về cách dùng Tiếng Anh" không phải là mới và đã có nhiều bản Tiếng Anh do người bản ngữ viết nhưng cuốn sách này hoàn toàn dành cho người Việt nên yên tâm là đọc rất dễ hiểu.

Tôi đã đọc và thấy nó rất hay, rất thú vị, thỏa trí tò mò, khám phá ngoại ngữ của tôi. Tôi cảm thấy tự tin hơn sau mỗi trang sách về vốn sử dụng Tiếng Anh.

Là người học Tiếng Anh lâu nhưng khi mở cuốn sách này đọc, tôi thấy mình vẫn còn rất nhiều điều chưa được biết về ngoại ngữ này. Tôi nghĩ đây là một trong những cuốn sách để học tốt nhất mà tôi từng đọc. Nó thật sự hữu ích cho bất kì đối tượng người Việt nào đã, đang hoặc sẽ học Tiếng Anh.

Cám ơn tác giả đã dày công viết nên cuốn sách hữu ích và đầy đủ này. Thank you so much.
5
457880
2015-11-28 19:16:30
--------------------------
331136
3674
Từ Điển Cách Dùng Tiếng Anh (Tái Bản 2015) của Tác giả Lê Đình Bì. M.A. Lúc mình cần mua thì phải đợi rất lâu mới có hàng, sách chất lượng lắm to, dày khủng bố, mà với giá này mình thấy là rẻ đó chứ, đúng như tên Từ Điển Cách Dùng Tiếng Anh mình chưa thấy sách này ghi về ý nghĩa cách dùng tiếng anh tốt như cuốn sách này, quá là may mắn khi mua được nó, rất thích luôn. Mong có nhiều sách chất lượng như thế và nhiều tác giả có tâm nghiên cứu như Tác giả Lê Đình Bì. M.A.
5
730322
2015-11-03 23:54:44
--------------------------
312282
3674
Dạo qua hiệu sách, mình bắt gặp cuốn này và về nhà lập tức lùng bằng được trên TIKI để mua. Cảm quan chung đầu tiên khi đọc phần giới thiệu và lật sơ là: sửng sốt, vì hóa ra bao năm học tập của mình đều có thể đúc kết chỉ trong vài trăm trang. Hai là: ghen tị ghê gứm. Nếu ngày xưa khi mình bắt đầu biết tới tiếng Anh mà có một cuốn sách gối đầu giường như thế này có phải đã không phải chật vật lên chật vật xuống như thế không chứ! Có rất nhiều điều để hối tiếc trong đời, nhưng hối tiếc nhất của mình trong việc học là khi ngồi trên ghế nhà trường không được học Anh văn ra đầu ra đũa. Mong các bạn đã, đang và sẽ cầm cuốn sách này trên tay rút ra cho mình nhiều bài học bổ ích từ nó. 
5
292321
2015-09-20 23:57:15
--------------------------
291011
3674
Quyển sách này chỉ cho bạn cách dùng những từ ngữ dễ nhầm lẫn , nêu ví dụ minh hoạ cách sử dụng của các từ đó nữa . Có được em này mình sẽ đỡ tốn thời gian trong việc tìm hiểu rất nhiều , đơn giản là vì tác giả đã giúp ta hệ thống lại . Thông thường , mình sẽ gặp các từ đó trong bài tập lựa từ vựng và dù tra từ điển dịch sang tiếng Việt vẫn cho ra nghĩa giống nhau . Quyển sách là một người thầy thực sự giúp bạn tự ôn luyện , làm chủ ngữ pháp tiếng anh . Cảm ơn tác giả đã cho ra đời một tài liệu nghiên cứu có giá trị to lớn .
5
124682
2015-09-05 22:56:39
--------------------------
284966
3674
Đây là một trong những quyển từ điển mình cực kì thích. Nó giúp mình rất nhiều trong cái bài luận tiếng Anh. Nhờ nó mình không sử dụng từ vựng loạn xì ngầu như trước nữa. Trong tiếng Anh, có nhiều từ nghĩa giống nhau, có từ có thể dùng trong nhiều trường hợp nhưng có những từ chỉ dùng trong những trường hợp nhất định. Tiếng Việt mình cũng vậy. Chẳng hạn như bảo : " Quân địch đã hi sinh mạng sống của mình trước sự đánh đuổi và tinh thần đoàn kết của quân dân ta". Như này thì có tát nước vào mặt người đọc. :) Qua quyển từ điển này mình thật sự ngưỡng mộ bác Lê Đình Bì về khả năng uyên thâm, hiểu biết sâu rộng về vốn từ của Bác. Cảm ơn Bác, cảm ơn First New đã mang đến cho độc giả nước mình một tác phẩm tuyệt vời về cả nội dung lẫn hình thức như vậy. Và nhờ có Tiki mình mới được biết tới và sở hữu một cuốn sách tuyệt vời như vây. Cảm ơn Tiki! <3
5
30388
2015-08-31 14:23:54
--------------------------
266248
3674
Mình mê cuốn từ điển này ngay từ khi mới đọc vài dòng giới thiệu. Đã từ lâu mình luôn mong có được một cuốn từ điển hữu dụng và độc đáo như thế này. Sách trình bày khá rõ ràng, dễ hiểu, có kèm theo ví dụ về cách dùng. Không phải mình đang quảng cáo sách đâu nhưng nếu các bạn muốn mở rộng vốn tiếng anh và nắm chắc cách dùng từ cho chính xác thì nên sắm ngay một cuốn cho bản thân.
5
10992
2015-08-14 15:19:28
--------------------------
261413
3674
Khi cầm trên tay quyển sách này và lật xem một vài trang, bạn sẽ nhận ra ngay rằng chính sự đồ sộ của quyển sách cộng với cách bố cục, trình bày và giải thích cặn kẽ này đã cho thấy tâm huyết của tác giả là rất lớn. Quyển sách này đã giúp cho độc giả nói chung và những người học tiếng Anh trở nên dễ dàng hơn rất nhiều ví dụ như cùng là to lớn nhưng ta không biết khi nào dùng big, khi nào dùng large hay lúc nào dùng great thì quyển sách này sẽ giải thích cặn kẽ cho chúng ta để từ đó chúng ta có thể vận dụng đúng ngữ cảnh và không bị bối rối trong việc sử dụng. Bên cạnh đó quyển sách còn liệt kê những từ gần giống nhau mà chúng ta hay bị nhầm lẫn trong cách sử dụng để chúng ta hiểu hơn về nó và vận dụng không bị sai nữa. Quyển sách này còn liệt kê một số điểm ngữ pháp quan trọng cũng như những điểm khác nhau giữa tiếng Anh và tiếng Mỹ nữa. Nói chung, quyển sách rất đáng để những người học tiếng Anh sở hữu.
5
274995
2015-08-11 08:35:41
--------------------------
255611
3674
Vô tình lang thang Internet thì đọc được bài giới thiệu về cuốn sách này, sẵn dịp đang có ưu đãi nên mình mua thử về coi, quả nhiên, sách hay thiệt luôn, đúng như những gì mình đang mong đợi ở 1 cuốn sách dạy tiếng Anh cho người tự học, chất lượng giấy tốt, bìa cứng, đẹp. Có được cuốn từ điển này mình thấy tự tin hơn hẳn trong việc tự học tiếng Anh, chắc chắn nó sẽ được cải thiện một cách đáng kể. Rất cảm ơn tác giả Lê Đình Bì vì những tâm huyết ông đã bỏ ra khi biên soạn cuốn sách này.
5
46432
2015-08-06 10:38:37
--------------------------
251097
3674
Tôi đã may mắn đặt mua cuốn sách này trong đợt khuyến mãi nên chỉ phải chi 168 ngàn - một cái giá quá lời cho cả một kho tàng kiến thức hữu ích. Cuốn sách cần thiết cho tất cả những ai đã đang và sẽ học Tiếng Anh, đặc biệt là những người đang luyện thi TOEIC, IELTS, TOEFL,... Chất lượng giấy tốt, bìa cứng và đẹp, nội dung thì miễn bàn. Cách sử dụng từ sao cho đúng ngữ cảnh, bởi có những từ dù đơn giản nhưng cũng rất dễ phạm sai lầm. Cảm ơn tác giả Lê Đình Bì vì những tâm huyết mà ông đã bỏ ra.
5
187067
2015-08-02 13:40:16
--------------------------
242333
3674
Mình may mắn mua ngay đợt khuyến mãi nên được giảm giá chỉ còn 168k, cảm giác cầm cuốn từ điển trên tay thật tuyệt. Bìa cứng, chất lượng giấy tốt, nội dung rõ ràng, dễ tra từ. Sách gần như thống kê đầy đủ các từ dễ gây nhầm lẫn trong tiếng Anh, bên cạnh đó, nó còn chỉ rõ cách phân biệt giữ các từ thường dùng ở Anh và ở Mỹ cho thấy mức độ am hiểu, quá trình nghiên cứu sâu sắc của tác giả Lê Đình Bì. Sở hữu quyển từ điển này, mình tự tin hơn trong phần kiểm tra phân biệt từ loại của giáo viên, tiếng Anh của mình cũng được cải thiện đáng kể.
5
635412
2015-07-26 14:53:46
--------------------------
240994
3674
Tôi lang thang trên mạng để tìm những quyển sách học tiếng Anh hay và vô tình thấy quyển này. Tôi đã đặt mua ngay, cảm giác vui sướng vì thật sự ưng ý: nội dung rất hay, hữu ích trong cách dùng từ- những từ dễ gây nhầm lẫn khi nghĩa na ná như nhau nhưng cách dùng lại khác nhau. Giấy in chất lượng tốt, tuy có một số rất ít trang chữ bị mất nét nhưng nhìn chung là rất tốt. Đây là một trong số ít quyển sách tiếng Anh mà tôi ưng ý nhất, rất cảm ơn tác giả.
5
257290
2015-07-25 07:56:02
--------------------------
229766
3674
Cuốn từ điển này rất hữu ích, đặc biệt đối với những ai đang gặp khó khăn khi học từ vựng tiếng Anh. Tác giả liệt kê từ ngữ và cách dùng từ rất chi tiết, trong tiếng Anh có nhiều từ có nghĩa tương tự nhau, tùy trường hợp mà chúng ta lựa chọn từ ngữ phù hợp, ví dụ như road, street và way đều mang ý nghĩa là con đường, đường đi, nhưng không phải lúc nào chúng cũng thay thế cho nhau được, phải tùy theo ngữ cảnh mà lựa chọn từ ngữ phù hợp. Cách trình bày của tác giả cũng rất chi tiết, rõ ràng, giá thế này cũng khá mềm cho một quyển từ điển bìa cứng.
5
24079
2015-07-17 03:30:30
--------------------------
228442
3674
Mình được người quen giới thiệu mua quyển sách này nên trước khi mua nó trên tiki mình cũng đã đọc qua.Quả thực sách này hay về mọi mặt.Sách giúp cho người học phân biệt được các từ mà thường có sự nhầm lẫn trong tiếng Anh.Có thể thấy tác giả đã khá là dày công,khi tác giả đã đi đến chi tiết từng từ một và kèm theo đó là các ví dụ cụ thể để phân biệt cách dùng các từ.Giúp cho người học dễ dàng nắm bắt hơn.Bên cạnh đó sách có chất lượng giấy phải nói là rất rất tốt,chữ in thì rất rõ ràng.Đây quả thật là một quyển sách rất đáng đồng tiền.
5
368991
2015-07-15 14:16:35
--------------------------
227436
3674
Từ điển các dùng tiếng Anh này là cuốn sách cực kì hay và bổ ích. Nội dung thì được tác giả nghiên cứu rất kĩ. Cuốn sách này có thể dùng cho mọi cấp độ khi học tiếng Anh. trong cuốn này mình thích nhất phần viết về những từ hay nhầm lẫn. Các từ được biên soạn rất phong phú đa dạng. hơn nữa dưới mỗi các cụm từ đều có những ví dụ cụ thể kèm với những lời giải thích rất dễ hiểu. nói chung sách này có toàn ưu điểm và không có một nhược điểm nào cả
5
587976
2015-07-14 10:41:31
--------------------------
208040
3674
Cuốn sách này có một phần mà mình rất thích, đấy là phần nói về các từ dễ nhầm, đó là các từ đồng âm khác nghĩa hoặc những từ có nghĩa na na giống nhau nhưng cách dùng thì lại không giống nhau, phải dùng tùy trường hợp và dùng với từng cấu trúc câu; nêu ra một số lỗi sai mà người học tiếng Anh thường hay mắc phải để tiện sửa chữa. Cách trình bày trong sách khá khoa học, dễ thấy, dễ hiểu, chi tiết. Có lẽ tác giả đã phải nghiên cứu rất lâu và rất tâm huyết mới có thể đúc rút được cuốn sách này, tuy nhiên chất lượng giấy chưa được tốt lắm.
5
393748
2015-06-13 22:53:13
--------------------------
200367
3674
Có thể nói đây là một cuốn từ điển kiểu mới dành cho những ai học tiếng anh. Các từ trong sách được đưa ra khá phong phú, kèm theo đó là phần ghi nghĩa tiếng Việt và những ví dụ tham khảo để người đọc hiểu cách sử dụng từ đó trong những văn cảnh và trường hợp khác nhau. Đặc biệt cuốn sách này còn tập trung khao thác mối quan hệ giữa các từ có nghĩ tương đương hay gần giống nhau hay có cách phát âm giống nhau. Các từ có nghĩa gần giống nhau được chỉ ra cách dùng phù hợp và có ví dụ minh họa để có thể phân biệt được chúng. Khi học tiếng anh mà sử dụng cuốn sách này không chỉ hiểu nghĩa mà còn ghi nhớ được những từ đó. Mình nghĩ đây là cuốn sách hay và cần thiết cho học sinh.
5
466113
2015-05-24 18:33:04
--------------------------
