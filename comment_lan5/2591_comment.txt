507213
2591
Tuổi thơ như ùa về qua từng trang sách, những kỉ niệm khó quên, tình cảm non dại, qua lời văn bình dị, mộc mạc. Sách được chia làm 2 phần tản văn và truyện ngắn, mỗi phần với mỗi cảm xúc khác nhau. Những câu chuyện với cái kết dở dang để lại buâng khuâng, suy tư khó tả. Đây là cuốn sách mình thích nhất cho đến bây giờ. Thật sự rất đáng để đọc qua.
5
1870485
2017-01-07 23:59:43
--------------------------
471551
2591
Nếu một ngày nào đó không thấy tôi giữa đô thị bon chen hãy tìm tôi giữa cánh đồng...
 cánh đồng của thời gian của những ngày xưa cũ của những gì đã qua nhưng vẫn còn vương lại trong tâm hồn tôi như một nỗi niềm day dứt....
Nếu ai muốn quên đi quá khứ thì chắc chắn rằng họ sẽ không bao giờ trưởng thành được nếu ai muốn từ bỏ những điều được coi là tồi tệ phía sau thì họ sẽ không lớn nổi giữa thế giới vốn đang ngày càng bộn bề và co ro này
5
720108
2016-07-08 17:58:19
--------------------------
449678
2591
"Hãy tìm tôi giữa cánh đồng" là cuốn sách - người bạn đã an ủi tôi rất nhiều, có lẽ ai đó sẽ nói tôi mượn lời có cánh để nói về "nó", cuốn sách khi t cho rằng nó  mang tuổi thơ tôi đến giây phút hiện tại, cả không gian và thời gian...
Ngoài ra còn có những mẩu chuyện tình yêu, tuy tan vỡ nhưng không day dứt, tuy xa nhưng gần. 
Cả những điều tinh tế nhỏ bé nhất trong cuộc sống được diễn tả lại với giọng văn nhẹ nhàng...
Khi không thấy tôi ở nơi phố xa đông đúc này nữa hãy tìm tôi giữa cánh đồng.
4
725698
2016-06-17 21:21:31
--------------------------
436165
2591
Mình biết đến Đặng Nguyễn Đông Vy (ĐNĐV) từ những bài viết hồi còn ở báo hoa học trò, rồi thực sự mến mộ cô khi đọc "Nếu biết trăm năm là hữu hạn". Một cách viết không lẫn vào đâu được, đọc từng dòng của cô đó không chỉ là sự nhẹ nhàng mà người đọc còn cảm nhận sự thật lòng, sâu sắc của một tâm hồn đầy trải đời. Mọi thứ ĐNĐV chia sẻ trong cuốn sách này vẫn luôn biểu trưng cho phong cách của cô, luôn đề cập đến những điều nhỏ bé nhưng ý nghĩa, những điều mà bất cứ ai ngay cả các tác giả khác chưa bao giờ đề cập đến. Không ăn theo xu hướng của các nhà văn trẻ hiện đại khác, sách của cô không bàn bạc hay dạy dỗ người đọc phải sống thế này, thế nọ nhưng quyển sách này vẫn cho mình những hoài niệm về tuổi thơ, gợi những tình cảm ruột thịt, và cả những suy tư về cuộc sống.... Trong quyển sách này, mình đặc biệt ấn tượng và nhớ mãi, nghĩ mãi về "Lá cuối năm" và "Mẹ chẳng có gì để ngưỡng mộ", lời văn không hoa mĩ, không quá trau chuốt, lại không phải kiểu cố gắng xúc động mạnh như một số tác giả khác nhưng mọi điều đều có sức ám ảnh vô cùng lớn, chậm rãi cứ như đợi chờ người đọc đi cùng, hiểu cùng....
4
555104
2016-05-26 11:26:30
--------------------------
427522
2591
Đã từng đọc sách này, đã từng mua tặng bạn và cuối cùng cũng tự thưởng cho bản thân bằng cách mua cho mình quyển sách.
Biết tác giả từ hồi đọc báo sinh viên. Các câu chuyện dù là tản văn hay truyện ngắn đều rất giàu cảm xúc và đầy ám ảnh:
"Tình yêu là gì? Đó là cảm giác thắt lại khi ta nhìn thấy người ấy, nó vỡ oà một cảm giác yêu thương trong trái tim, lan toả ra từng tế bào, đến tận đầu những ngón tay, nhưng đôi khi nó làm người ta đau đớn. Tình yêu, nó sâu thẳm, nhọn hoắt, và dễ làm tổn thương. Tôi luôn thấy tình thương thì an toàn hơn tình yêu. Tình thương như một mảnh lụa, êm dịu, mượt mà, dễ lan tỏa, và dai dẳng nữa, giống như đêm. Tôi vẫn muốn yêu như là thương, và muốn được yêu như là thương" (Trích trong Tạm biệt Rainy).

5
39475
2016-05-09 14:45:04
--------------------------
408990
2591
Yêu thích sách của Đặng Nguyễn Đông Vy từ hồi đọc " Nếu biết trăm năm là hữu hạn" rồi. GIờ đọc thêm quyển này, càng thấy yêu quý chị.

Mỗi câu chuyện là một bài học quý giá. Đọc truyện nào cũng thấy cái triết lý trong đó, cũng cảm động mà rưng rưng. 
Nhưng thích nhất chắc là câu chuyện "Những chiếc hộp" với đoạn: "Tôi buồn bã khi nghĩ rằng, đáng lý ra tôi phải cho em tôi những chiếc xe đồ chơi khi điều đó còn làm cho chúng vui sướng, hơn là cất giữ hàng năm trời, cho tới khi chúng bắt đầu mơ về những chiếc xe hơi thật."

4
1213989
2016-04-01 17:02:28
--------------------------
375012
2591
Những tác phẩm về quê hương đất nước luôn đem lại những ấn tượng nhẹ nhàng mà sâu sắc trong lòng người đọc.
Với người đã và đang sinh sống nơi làng quê sông nước hay đồng lúa, vườn cây... tác phẩm sẽ đem lại sự đồng cảm, hay gợi nhớ nhiều kỷ niệm thân thương. Từ hình ảnh, mùi vị, không gian, thời gian đều đem lại những xúc cảm khó tả.
Với người sống ở thành phố, chưa từng trải nghiệm cảnh miền quê, hay đã từng một lần trải qua, truyện cũng đem lại nhiều hoài cảm đẹp, an yên...
4
402531
2016-01-27 16:42:36
--------------------------
373480
2591
cuốn sách " Hãy tìm tôi giữa cánh đồng" của tác giả Đặng Nguyễn Đông Vy bao gồm 2 phần: tản văn và truyện ngắn. Đến với phần tản văn các bạn độc giả sẽ được thưởng thức những kỉ niệm thân thương mang đầy hình bóng của chúng ta trong tuổi thơ của tác giả, trở về với những ngày thơ ấu nơi có những bữa ăn đạm bạc nơi có những trận đòn kèm theo bài học đáng nhớ của bố hay những lời yêu thương đầy ngọt ngào của mẹ. đến với phần truyện ngắn các bạn sẽ được trải lòng mình qua những cảm xúc rất thật của con người thổn thức với vô vàn những mẩu truyện nhỏ nhưng mang đầy ý nghĩa. Một cuốn sách hay rất đáng để đọc khi các bạn cần ôn lại những kỉ niệm thời thơ ấu.
5
1117828
2016-01-23 21:33:17
--------------------------
371339
2591
Thành thật mà nói cuốn sách của chị  Đông Vy làm tôi có cảm giác chị là con người cực kỳ tinh tế và thấu hiểu, nhẹ nhàng và cảm thông. Chị làm tôi nhìn lại những xúc cảm, cảm nhận ngày xưa về những cánh cửa, những cây cỏ quanh nhà đầy kỷ niệm với những gắn bó qua thời gian, khi quay về luôn cố tìm những điều xưa cũ, những giá trị xưa cũ mà mọi người có lẽ đã lãng quên. Thật tình rất muốn được thấy tận mắt chị chia sẻ qua lời nói và lời kể của chị.
4
713961
2016-01-19 13:02:08
--------------------------
366823
2591
Vẫn với phong cách nhẹ nhàng, sâu lắng, Đặng Nguyễn Đông Vy dẫn dắt người đọc qua những câu chuyện không hẳn là buồn thương cũng không hẳn vui thích, có gì đó man mác, khi đọc xong mỗi mẫu truyện lại thấy lắng đọng một chút. Tôi thích phong cách ấy của tác giả ngay từ quyển Nếu biết trăm năm là hữu hạn( Bút danh Phạm Lữ Ân là dùng chung cho hai tác giả đông Vy và Phạm Công luận). Nếu bạn nào yêu thích lối viết này nên mua quyển Nếu biết trăm năm là hữu hạn, chắc chắn sẽ không thất vọng vì quyển kia sâu lắng hơn, không chỉ là những câu chuyện mà đó là những quan điểm rất hay. Hy vọng tác giả sẽ viết nhiều quyển nữa. Vẫn thấy quá ít sách của Đông Vy 
4
42014
2016-01-10 20:09:37
--------------------------
345064
2591
Những kỷ niệm thời ấu thơ bao đỗi bình dị và sâu sắc làm sao . Món ếch xào xả được tác giả miêu tả khi nấu cùng bà làm mình cảm thấy hoài niệm về nhiều điều hồi còn nhỏ , mình không biết trân trọng thức ăn giờ lớn rồi ngẫm lại thấy tiếc nuối quá . Hoàn cảnh của tác giả hồi ấy ở dưới quê cũng khá bình thường nhưng tuổi thơ của bà thật trọn vẹn và ý nghĩa . Một đứa con gái sống ở thành phố như mình cảm thấy ghen tị làm sao ấy . Nhớ lúc nhỏ khổ sở bao nhiêu , ba mẹ có căn nhà tệ thế nào nhưng giờ đây trưởng thành rồi và có sự nghiệp , xây nhà lên là đoạn mình cảm thấy xúc động nhất . Cuốn sách này được viết với lối văn rất giản dị , đằm thắm yêu thương cho nên khi đọc mình cảm thấy rất ấm áp , thoải mái.
5
781966
2015-11-29 18:59:27
--------------------------
340214
2591
Không biết nói gì nữa.. bìa sách in đẹp nội dung cũng rất tuyệt vời. Những câu chuyện tưởng chừng như viết về chính tôi. Ngay những câu chuyện đầu tiên tôi cảm thấy như đag thấy mình trong quá khứ. Ôi! Ngôn từ mộc mạc, lời lẽ tự nhiên một cách lạ lùng. Có thể 1 số ng đọc gốc thàh phố sẽ k có cảm giác như chúg tôi, xuất thân từ nôg thôn k bỏ qua bất kì kỉ niệm nào trong đó. Nên mới có cảm giác này. Tôi cứ muốn được đọc tiếp, đôi khi trong cuộc sống áp lực việc học và cuộc sống làm tôi mệt mỏi, lục tìm lại cuốn sách tôi lại vui cười với nhữg hìh ảnh gần gũi trong những trang sách này. Là cuốn sách các bạn nên có ở trong tủ sách của mình..
5
850715
2015-11-19 13:57:10
--------------------------
329648
2591
Tuổi thơ vốn là điều gì đó mà khi nghĩ về ta thấy trong lòng một niềm hoài niệm xa xăm về một vùng trời bình yên của kí ức. Dẫu biết rằng chẳng ai có thể níu giữ được tuổi thơ, nhưng sao khi nghĩ về nó ta cứ băn khoăn mãi và ao ước có một ngày được sống lại những giây phút ấy. Dù cho tuổi thơ của tôi có dữ dội thật đấy, nhưng vẫn đong đầy những kỉ niệm chẳng thể nào quên và tôi luôn muốn tìm lại... Tuổi Thơ Tôi !!! Cảm ơn tác giả vì quyển sách !!!
5
836957
2015-11-01 08:59:10
--------------------------
329232
2591
Tôi đã tìm mua và đọc quyển này thông qua sự giới thiệu của một người bạn. Chúng tôi "bị kích thích" bởi chính tựa đề truyện - "Hãy tìm tôi giữa cánh đồng" - nghe sao yên bình quá. Rồi tôi đọc truyện, tôi như bị cuốn vào từng câu, từng mẩu truyện của tác giả. Hẳn phải trải qua một tuổi thơ với những vấp ngã và sự bình dị từ mớ thân quen nơi quê nhà, mớ bình yên nơi đồng lúa với chú mục đồng thì lời văn mới chân thật và chứa trọn vẹn cảm xúc đến thế. 
4
764601
2015-10-31 12:54:22
--------------------------
322214
2591
cuốn sách đưa ta trở về với tuổi thơ, với những kí ức mà đã lâu lắm rồi mới khơi dậy lại trong lòng. hãy đọc và cảm nhận, và nhớ về tuổi thơ, nhớ về quê hương, nhớ đến nơi ta sinh ra, sống hạnh phúc như thế nào! cuốn sách ngôn từ dễ hiểu, cảm xúc, bình yên, dịu nhẹ...và  bìa rất đẹp, cùng với đó là những mẩu chuyện nhỏ in đậm trong tâm trí và đôi khi gặp ngay bản thân mình trong cuốn sách. phải thật sự cảm ơn tác giả... cảm ơn vì đã tạo ra một cuốn sách như vậy.Tiki giao hàng nhanh chóng nhưng mình có ý kiến là nên bao gói cẩn thận hơn vì sách của mình bị cong 1 xíu vì đường vận chuyển dài.
5
733261
2015-10-15 20:19:50
--------------------------
320414
2591
Cứ như đang bước chân vào một cánh đồng bất tận có hơi mùi của quê hương đồng lúa, cỏ thơm lẫn cả hoa, rồi chợt quẩn quanh hít thở rồi hoài niệm vậy. Tác giả hình như chỉ dẫn truyện và cảm thán về những chuyện mình đã trải qua nhưng lại vô hình chung dẫn dắt người đọc bước theo những cảm xúc đó và vùi đầu vào chúng, để hoài thương, nhớ nhung và đồng cảm! Văn phong mượt mà, viết rất tốt, bình yên và giản dị, như cuốn theo làn gió mát lành bay bổng về quá khứ lẫn hiện thực...
5
516261
2015-10-11 09:56:23
--------------------------
317120
2591
Hôm nay tôi vừa cầm trên tay quyển sách này. Chưa cần đọc hết nhưng tôi rất tâm đắc với nó. Thật sự cám ơn tác giả. Nhờ chị mà tôi đã nhớ lại ngày bé tôi cũng đã yêu những cánh đồng lộng gió như thế nào.. tôi đã bình yên như thế nào trong những buổi chiều đó... cũng nhờ chị, tôi được gợi nhớ về ông nội cũng thật đặc biệt của tôi trong những lời kể của ba..đây là một quyển sách thật sự tuyệt vời, nó giúp người ta tìm về với những kí ức trong trẻo nhất, bình yên nhất, để sau những xô bồ, bon chen của cuộc sống, tâm hồn ta có một nơi chốn an nhiên tìm về....
4
733970
2015-10-02 15:43:39
--------------------------
288596
2591
Lúc đầu đọc thử đã ấn tượng với trang đầu tiên về thứ '' keo dán'' ăn được rồi sau đó tác giả đã đưa ta vào một thế giới hồn nhiên với những khúc đồng dao làng quê, với những lần giận hờn,....Tôi yêu cái cách tác giả viết nên một câu chuyện thâm sâu mà tinh tế ấy, nhẹ nhàng, sâu lắng và khiến bạn muốn đọc thêm nữa. Tóm lại tôi khuyên bạn nên mua quyển sách vì nó sẽ đưa bạn từ bất ngờ này sang bất ngờ khác, từ câu chuyện này sang câu chuyện khác rất hay và hấp dẫn :)
5
769797
2015-09-03 18:12:31
--------------------------
287045
2591
Những lúc thấy lòng chênh vênh tôi lại đọc sách và lần này tôi may mắn vì đã chọn "Hãy tìm tôi giữa cánh đồng", đọc cuốn sách, tôi như tìm về tuổi thơ, tìm về cái suy nghĩ trong veo thời còn bé, chạm đến những gì đẹp đẽ nhất tôi từng trải qua. Bất giác tôi nhớ nhà, khao khát được một lần quay trở lại thời thơ ấu, Đây là cuốn sách đầu tiên tôi đọc do Đặng Nguyễn Đông Vy viết, thực sự tôi yêu tác giả mất rồi, yêu cái ngòi bút thâm sâu mà tinh tế ấy, mạch văn nhẹ nhàng, sâu lắng nhưng khiến người đọc phải suy ngẫm nhiều, một cuốn sách đáng đọc khi lòng bạn cảm thấy vô định.
4
605323
2015-09-02 10:46:00
--------------------------
269757
2591
Như tìm thấy mình ở đâu đó trong cuốn sách này. Cảm giác mình thấy thật bình yên, một cuốn sách đáng để đọc. Không biết mình đã đọc đi đọc lại bao nhiêu lần. Chỉ biết mỗi lần gấp cuốn sách lại, lòng của mình trở nên thanh thản lạ thường. Cảm giác lòng mình được an ủi.
Ngôn từ đơn giản, mộc mạc, sâu lắng, nhẹ nhè len vào cảm xúc. <3
5
413372
2015-08-17 19:33:32
--------------------------
264745
2591
Lúc đọc phần đầu tiên của sách, tự nhiên thấy quen quen, hóa ra là đã đọc được mẩu chuyện lá cuối năm và bữa sáng bằng hồ dán ... trên một mẩu bảo nào đó. Chợt thấy lòng chênh vênh, lành lành mướt mát như mùa xuân, ấm áp anh vui như ngày Tết đoàn viên, ... lại có chút gì man mác khó mà nói lên lời. Giống như bạn đi rất xa rất xa, ngày hôm nay bỗng đột ngột trở về, trong cảm giác thân quen có đôi phần xa lạ, mà bạn vốn là người quen nay trở thành khách lạ. Cuốn sách này khiến cho người ta không thể kìm lòng mà cuốn đi, đến những tháng ngày mà đời có xô bồ cũng chịu an nhiên
4
100688
2015-08-13 13:38:02
--------------------------
