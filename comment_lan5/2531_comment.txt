334270
2531
Quyển sách này của Damian Ryan  nhìn chung sẽ khá hữu ích cho những người đã và đang làm trong ngành digital marketing. Tuy nhiên với những người đang tìm hiểu thì không nên đọc. Những mẹo và những bài viết trong quyển sách này nghiên về kinh nghiệm và chia sẻ nhiều hơn là hướng dẫn cách làm. Dù nó nói bao quát về các công cụ của Digtal Marketing từ SEO, SEM, lập Web và các công cụ phân tích.... nhưng nhìn chung mỗi bài viết chỉ mang tính chất trình bày chứ chưa sâu. Đơn cử như chương về SEO thì nó nói và chỉ ra ưu, nhược những cách làm SEO hiệu quả nhưng không nêu được làm cách nào để đạt được hiệu quả đó hay đưa ra 1 ví dụ về chạy SEO là như thế nào. Nên theo mình, nếu ai có kinh nghiệm trong ngành marketing đặc biệt là digital marketing thì quyển sách này mới thực sự hữu ích. 
4
422154
2015-11-09 10:18:25
--------------------------
307063
2531
Theo mình, sách marketing có 2 dạng: TƯ DUY và CÔNG CỤ. Cuốn sách của Damian Ryan nói đa số về công cụ,tips,những điều cơ bản trong digital marketing. Thế mà trong cuốn sách này sử dụng rất nhiều những thuật ngữ cực kỳ chuyên sâu, case study thuộc công ty lớn khá khó áp dụng cho doanh nghiệp vừa và nhỏ. Nếu chưa có bất kỳ kiến thức về digital marketing, tôi khuyên bạn đừng đọc vì bạn sẽ mất thời gian để google những khái niệm chưa được giải thích tận tình. 

Nhìn góc độ tích cực, 13 chương trong sách sẽ giúp bạn hình dung tổng quan về Digital marketing, từ SEO đến truyền thông xã hội, email marketing..Đọc hãy chọn lọc tips có thể ứng dụng vào doanh nghiệp của mình. Bạn sẽ thấy rất hiệu quả :) 


3
508749
2015-09-17 22:33:04
--------------------------
303206
2531
mình mua quyển sách này củng với quyển sách Facebook từ A đến Z đọc 1 tháng nay mà chưa hết luôn. nói chung là rất nhiều kiến thức từ tiếp thị đến quảng cáo và truyền thông, ai hứng thú về các lĩnh vực này thì nên mua đọc, rất hữu ích. sách dễ đọc và dễ hiểu, từ lựa chọn đúng kênh cho đến đi đúng cách, từ truyền thông xã hội, tiếp thị điện thoại, email cho đến Google. những chiến lược tiếp thị trong kỷ nguyên số được khai thác rất nhiều, cách viết khá rõ ràng, gần gũi và chân thực.
5
678916
2015-09-15 18:35:02
--------------------------
279831
2531
Nếu bạn đang quan tâm đến thương mại điện tử thì quyển sách "Tiếp thị số từ A đến Z" là dành cho bạn. Hiện nay trên thị trường có rất nhiều sách về thương mại điện tử nhưng quyển sách trình bày nhiều nội dung một cách đầy đủ nhất. Sách sẽ hướng dẫn bạn cách sử dụng và lựa chọn các công cụ tiếp thị như: email, SMS,... một cách hiệu quả nhất trong việc thu hút khách hàng. Ngoài ra bạn có thể tìm đọc thêm quyển sách Facebook từ A đến Z và quyển sách SEO Master để bổ sung thêm nhiều kiến thức về lĩnh vực này.
5
356759
2015-08-27 10:01:19
--------------------------
278434
2531
"TIẾP THỊ SỐ TỪ A ĐẾN Z" là một cuốn sách hữu ích cho các bạn học chuyên ngành Marketing nói riêng và Quản trị kinh doanh nói chung. Cuốn sách được viết bởi Damian Ryan, một chuyên gia kỳ cựu về tiếp thị và truyền thông số, là tập hợp những chiến lược tiếp thị trong kỷ nguyên số được khai thác, đúc rút như tiếp thị từ truyền thông, mạng xã hội, tiếp thị qua điện thoại, tiếp thị liên kết, tiếp thị qua e-mail và nhiều chiến dịch tiếp thị số khác nữa. Sách khổ lớn và dày 552 trang đòi hỏi người đọc phải chú tâm để có thể tiếp thu được tốt những kiến thức hữu ích này

5
60677
2015-08-25 21:29:18
--------------------------
248810
2531
Đây có thể nói là cuốn sách gối đầu giường cho tôi khi mới bắt đầu tham gia vào lĩnh vực Marketing Online. Damian Ryan đã mang đến cho tôi một con đường rõ ràng để dẫn đến thành công. Dễ đọc và dễ hiểu, từ lựa chọn đúng kênh cho đến đi đúng cách, từ truyền thông xã hội, tiếp thị điện thoại, email cho đến Google. Những kiến thức mà Damian Ryan đưa ra thật sự rất xứng đáng với kinh nghiệm hơn 30 năm của một chuyên gia về tiếp thị và truyền thông số. Văn phong của ông rõ ràng và dễ hiểu, làm cho một lĩnh vực mênh mông như đại dương trở nên gần gũi và chân thật hơn bao giờ hết. Thật sự là một quyển sách đáng phải đọc cho dân tiếp thị số!
5
9827
2015-07-31 10:16:25
--------------------------
