233770
4125
Mình đặt mua quyển Truyện Kể Về Những Tấm Gương Đạo Đức, tặng cho con trai của mình để mong tính cách của bé dịu lại. Tuổi thơ thời hiện đại khác hẳn với tuổi thơ của những thế kỷ trước. Công nghệ thông tin đã xâm nhập vào từng ngõ ngách cuộc sống đã ảnh hưởng không ít đến tâm hồn và tư duy của trẻ nhỏ. Quyển sách bao gồm nhiều câu truyện nhỏ, sau mỗi câu truyện là phần kết đầy ý nghĩa. Ví dụ sau mẩu truyện 'Hai bát mì bò" phần kết là: Lòng nhân ái đã được gieo nảy mầm tươi xanh..Ước chi mãi mãi là những chuyện thật như thế, chung quanh mỗi chúng ta trong cuộc đời bon chen, giẫm đạp này.
5
460604
2015-07-19 21:38:50
--------------------------
233266
4125
Mình đặt mua quyển Truyện Kể Về Những Tấm Gương Đạo Đức, tặng cho con trai của mình để mong tính cách của bé dịu lại. Tuổi thơ thời hiện đại khác hẳn với tuổi thơ của những thế kỷ trước. Công nghệ thông tin đã xâm nhập vào từng ngõ ngách cuộc sống đã ảnh hưởng không ít đến tâm hồn và tư duy của trẻ nhỏ. Quyển sách bao gồm nhiều câu truyện nhỏ, sau mỗi câu truyện là phần kết đầy ý nghĩa. Ví dụ sau mẩu truyện 'Hai bát mì bò" phần kết là: Lòng nhân ái đã được gieo nảy mầm tươi xanh..Ướ chi mãi mãi là những chuyện thật như thế, chung quanh mỗi chúng ta trong cuộc đời bon chen, giẫm đạp này.
' 
5
460604
2015-07-19 15:13:30
--------------------------
