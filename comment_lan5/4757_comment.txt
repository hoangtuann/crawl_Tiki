487144
4757
Kinh điển, buồn, bi thương nhưng khiến người đọc phải suy nghĩ nhiều. Chuyện về thời cũ nhưng có những thứ vẫn không hề thay đổi dù đang ở thời mới, cá nhân mình thấy không hợp lắm vì tính mình thích những thứ vui vẻ, nhưng vì bạn bè khen hay và kinh điển của văn học việt nam nên vẫn quyết định đọc cho biết. Vẫn sâu sắc, sâu lắng, nhưng không hơpi với mình thôi, mà vẫn nên đọc, nên có, vì bạn sẽ nhận ra văn học việt nam mình cũng không hề thua nước nào cả đâu, vừa đọc vừa thấy tự hào :D
4
1643449
2016-10-20 02:09:09
--------------------------
470238
4757
Mấy tác phẩm nổi tiếng thế này nay đâu còn ai đủ trình độ để mà sáng tác. Mình yêu văn học Việt Nam lắm chứ, nhưng mà chỉ yêu những tác phẩm của các tác giả thời kỳ đầu thôi, chứ sau này viết không còn hay nữa. Một thời, cuốn này từng bị cấm vì nội dung không phù hợp với xã hội thời bấy giờ. Nhưng mà có đọc thì mới biết, tác phẩm này đã phản ánh rõ nét cái hiện thực thời bấy giờ ở nước ta, nhất là thân phận người phụ nữ. Cái kết thấy mà buồn man mác. Đọc xong mà thương người phụ nữ lắm. Cám ơn tiki.
5
634855
2016-07-07 12:02:34
--------------------------
437155
4757
Toàn bộ cuốn sách kể về cuộc đời u ám và gian truân của một người phụ nữ ở xã hội Việt Nam xã hội cũ
Đúng là phận đàn bà 12 bến nước long đong lầy đầy khổ sở đủ thứ lo toan,lại gặp phải người chồng ác phu thất đức thì cuộc sống chẳng khác nào dưới âm ty địa ngục
Bỉ vỏ ngoài hay ở cốt truyện còn hay ở ngôn từ. Đa số sử dụng từ ngữ địa phương rặc tiếng miền Nam, mình người Nam đọc từ mới nghe rất hay, từ cũ nghe rất quen thuộc, ấm vào lòng
5
215431
2016-05-27 20:01:21
--------------------------
410859
4757
Vừa mới đọc xong tác phẩm và cảm thấy chạnh lòng cho những số phận hẩm hiu bị dòng đời xô đẩy. Số phận một người đàn bà quê mùa thành ra một dân móc túi, một dân "anh chị" trong một cơn gia biến khiến người đọc không khỏi ngậm ngùi cay đắng cho số kiếp con người.

Những thăng trầm mà Tám Bính đã trải qua cũng như những sự trả giá cho một đời lầm lỗi của nàng được tác giả xây dựng với trí tưởng tượng phong phú. Và mình chắc rằng Nguyên Hồng phải có những kinh nghiệm sống rất rộng lớn mới có thể khắc họa được giới giang hồ một cách chân thực và sống động đến như thế.

Những gút mở được tác giả xây dựng đêm đến cho người đọc đi từ bất ngờ này đến bất ngờ khác. Dù biết rằng ác giả ác báo nhưng vẫn rất khó hình dung và đoán biết được cấu tứ của truyện.

Tóm lại đây là một tác phẩm đáng đọc dù đã ra đời gần một trăm năm nhưng thiên phóng sự vẫn thở mạnh mẽ hơi thở của thời đại.
5
582986
2016-04-04 23:08:16
--------------------------
394843
4757
Nguyên Hồng quả vô cùng xuất sắc kể câu chuyện về cuộc đời Tám Bính, một người phụ nữ bé nhỏ yếu đuối nhưng đã sớm phải vật vã, xoay sở với đời. 
Đọc sách nhiều khi thấy giận sôi người vì những miệng đời trắng trợn và trơ trẽn hết mực, lại có lúc thấy thương thay số phận người con gái giữa chợ rối ren. Cũng nhiều lúc thấy rùng mình run sợ, bởi bút lực tài hoa của Nguyên Hồng, người đã chắt lọc và miêu tả từng chi tiết hết sức đắt giá. 
Đọc từ năm lớp Sáu khi được thầy dạy Văn giới thiệu và yêu thầy vô cùng sau đó ! 
Rất đáng đọc !
4
75144
2016-03-10 23:33:59
--------------------------
384533
4757
Nội dung truyện xoay quanh cuộc đời của cô gái quê xinh đẹp, chân chất, thật thà Bính. Từ phút yếu lòng, dại dột, trót lầm lỡ tin lời đường mật của một người đàn ông mà cuộc đời nàng rơi vào tối tăm, bẩn thỉu và đầy bi kịch. Đọc truyện tôi vừa giận, vừa xót xa cho nàng. Tám Bính dấn thân vào con đường tội lỗi lúc nào không hay. Nàng có đau đớn, có dằn vặt, có ân hận, có khát vọng quay về con đường lương thiện. Nhưng thật khó, nàng bị chênh vênh giữa 2 phía, 1 bên là con đường trong sáng thanh thản, 1 bên tuy bẩn thỉu, nhưng lại níu kéo bằng 1 cái tình rất lạ của 1 tay anh chị. Đọc truyện, tôi thực sự không định nghĩa được cái gì gắn kết họ với nhau, có 1 tình yêu giữa chốn bùn nhơ vậy ư?
4
198939
2016-02-22 15:54:32
--------------------------
378256
4757
Mình rất thích đọc các tác phẩm văn học Việt Nam hiện đại, đặc biệt là của Nguyên Hồng. Cô giáo dạy văn gợi ý và tóm tắt cho mình tác phẩm Bỉ vỏ, mình thấy thú vị nên tìm đọc thử. Trước đó mình có đọc Những ngày thơ ấu của Nguyên Hồng rồi và mình rất thích. Tác phẩm này cũng vậy. Nguyên Hồng đã phản ánh trong tác phẩm của ông một sự thật, một cuộc đời, một số phận người phụ nữ Việt Nam đau khổ, truân chuyên. Đọc tác phẩm này, mình thấy thương thay cho Tám Bính, thương thay cho Năm Sài Gòn, thương thay cho đứa con của họ...
5
528434
2016-02-04 21:12:25
--------------------------
368912
4757
mình đã từng nghe đến Bỉ Vỏ nhiều lắm nhưng chưa được đọc, thấy tiki có bán và lại được giám giá nên đã mua về đọc thử, thú thật mà nói nhiều tác phẩm của nhiều tác giả lớn tuy được đánh giá là xuất sắc nhưng đọc vẫn không cảm nhận được sâu sắc như khi đọc Bỉ Vỏ, có nhiều tác phẩm cũng ra đời cách đây rất lâu nên mình đọc chỉ thấm một phần nào nhưng đọc tác phẩm này của Nguyên Hồng thực sự rất tuyệt và vô cùng khâm phục lối viết của ông, thật sự đọc từng câu từng chữ rất thấm thía và mình nghĩ đã yêu văn chương mọi người nên đọc tác phẩm này, chính vì giá trị to lớn và ý nghĩa sâu sắc mà có lẽ Bỉ Vỏ luôn được đánh giá cao là vì vậy.
4
581360
2016-01-14 20:38:33
--------------------------
366206
4757
Ôi cái cuộc đời, khổ quá, đến Cha mẹ còn chưa thương lấy con mình, ấy vậy mà một thằng "chạy vỏ" khét tiếng lại yêu thương nàng, số phận Tám Bính từ cái khổ này đẩy đưa đến cái nghiệt ngã khác, đọc mà đau đáy, buồn, may mà có Việt Nam Danh Tác mới tìm lại được mấy quyển sách cứ tưởng không mua được, Bỉ vỏ - người đàn bà ăn cắp, hyhy. hay lắm, mua không hối hận gì đâu. có điều tiếng lóng nhiều quá tuy tra rồi nhưng vẫn có nhiều từ không hiểu.

5
555023
2016-01-09 14:11:41
--------------------------
365020
4757
cái khốn cùng của một người con gái, của một phận người trong xã hội cũ từ khi bị lừa với đứa con trong bụng  rồi phải ra đi, vào thành phố rồi trở thành người buôn phấn bán hương bất đắc dĩ, chịu nhiều khổ cực, bỉ ổi của xã hội đương thời. cả một đời Tám Bính tưởng như sẽ bình an khi kết thân với một tên tướng cướp như Năm saigon, nhưng cuối cùng cuộc đời lại chẳng đi về đâu, đứa con mà cô sinh ra cuối cùng lại chết trước mặt mẹ nó một cách tức tưởi... cảnh cuối khi cảnh sát ập vào cũng là lúc chấm hết cho mọi chuyện.
4
568747
2016-01-07 11:25:41
--------------------------
359658
4757
Cuốn sách đã tái hiện lại cuộc sống của nhân dân Việt Nam trong những năm tháng đầu cuộc CM Tháng Tám. Cảm thấy thương cho những số phận bất hạnh như Tám Bính. Cuốn sách thật sự chinh phục mình bằng một giọng văn chân thật, thật đến từng chi tiết. Nó như cuốn người đọc vảo cái thế giới đầy âm u của xã hội VN cũ đã đẩy con người vào những bước đường cùng. Thế mới thấy nhân dân ta đã phải sống khổ cực đến nhường nào. Cảm ơn Nguyên Hồng vì đã luôn lên tiếng bênh vực những người phụ nữ đáng thương. Cảm ơn vì đã viết ra một cuốn sách hay đến thế!
3
1021905
2015-12-27 18:21:27
--------------------------
354950
4757
Đây là cuốn sách có tuổi đời rất cao ở trong làng văn học việt nam thời kháng chiến chống lại những cái nghèo đói , cùng cực của chế độ xã hội cũ , biết sống vì miếng cơm , manh áo mà không có nhiều cách vượt qua , chỉ có cách làm bằng nhiều việc kiếm sống cho qua ngày , nếu phải sống khổ như thế nhân vật chính chỉ có những thị phi luôn đeo bám chị cả đời , bởi xã hội vốn không công bằng với ai cả , dòng văn miêu tả là những điều còn nhỏ mà tác giả đã thấy .
4
402468
2015-12-18 22:15:14
--------------------------
353388
4757
Trong lần đi hội sách vừa rồi ở Hà Nội tôi đã tình cờ mua được cuốn bỉ vỏ của tác giả nguyên hồng. Một trong những cuốn sách tôi đã muốn mua từ lâu, bìa ngoài được nhã nam trình bày khá đẹp, theo ý kiến của tôi là đẹp hơn các nhà xuất bản khác.Một khi đã đọc cuốn sách thì bạn khó có thể rời mắt ra được. Mạch truyện vô cùng lôi cuốn, kể về xã hội nước ta ngày xưa cuộc sống của dân anh chị, những kẻ móc túi trộm cướp... Nhân vật chính của tác phẩm là một "bỉ vỏ" đọc xong cuốn sách bạn có thể hiểu được bỉ vỏ có nghĩa là gì. Tuy vậy cuốn sách sử dựng hơi nhiều tiếng lóng khá khó khăn lúc mới đầu đọc để có hiểu ý nghĩa của những từ đó. Một cuốn sách rất đáng đọc!
5
139598
2015-12-16 00:33:44
--------------------------
341166
4757
Đọc tác phẩm này tôi cứ ngỡ như đang trong cái xã hội đương thời ấy. Đọc. Suy ngẫm. Và bất chợt lại trào nước mắt. Tám Bính - một cô gái có tấm lòng thiện lương vì số phận đưa đẩy mà phải đi chạy vỏ rồi gặp Năm Sài Gòn - một tay anh chị. Số phận của họ đáng thương vì thiếu tình yêu thương gia đình nhưng cũng có phần đáng trách vì đi theo con đường "chạy vỏ" sai trái. Dù vậy, vẫn căm ghét hơn cả là một xã hội mục nát, đầy những tên quan tham xảo trá, mất nhân tính...
    Một cuốn sách đáng đọc!
5
681750
2015-11-21 13:48:04
--------------------------
304485
4757
Cái tên sách lạ, khiến mình tò mò và sau khi đọc thì đã hiểu được ý nghĩa. Tác giả viết cuốn sách này khi tuổi đời chưa đầy 20. Cuộc đời của tác giả cũng trải qua bao thăng trầm, biến cố nên giọng văn có một cái gì đó rất từng trải, như thể tác giả hiểu hết tình cảnh của những nhân vật trong truyện vậy. Bức tranh hiện thực của xã hội những năm 1930. Buồn nhưng chân thật, cay đắng, trần trụi mà cũng thật đáng thương. Sách đẹp, chất lượng tốt nhưng mình thấy chú thích ý nghĩa từ lóng ngay trong trang sẽ tiện hơn so với làm bảng ở cuối truyện, hơi bất tiện và làm mình tuột cảm xúc.
5
569970
2015-09-16 14:08:15
--------------------------
292409
4757
Khi nhận cuốn sách này đọc cái tên tôi rất tò mò vì thật sự không hiểu tựa sách muốn nói đến gì. Khi đọc vô tôi mới biết à thì ra "Bỉ vỏ" là tiếng lòng để chỉ người đàn bà an cắp. Càng ngạc nhiên hơn khi tôi thấy nhà văn Nguyên Hồng viết tác phẩm này khi mới 20 tuổi. Nội dung thì quá hay, tác giả đã khắc họa lên cuộc sống phong kiến những năm 1930. Đọc mà tối thấy nhân vật Bính bị đẩy đến cuộc sống "chạy vỏ" đều toàn do cái phong kiến ấy đưa tới. Sách do Nhã Nam phát hành khá đẹp, chất liệu giấy tốt tuy nhiên có đôi chỗ trong sách dùng từ lóng nhưng phía dưới hoặc ở cuối sách không có diễn giải nghĩa cho những từ lóng ấy làm việc trải nghiệm tác phẩm của tôi chưa được trọn vẹn lắm. Mong nxb chú ý chỗ này. 
5
387183
2015-09-07 12:00:33
--------------------------
265771
4757
Bỉ vỏ là một trong số những cuốn nằm trong bộ Việt Nam danh tác do Nhã Nam xuất bản, và nó là một trong những cuốn rất đáng đọc nếu muốn biết về xã hội Việt Nam thời bấy giờ. Nguyên Hồng không có giọng văn đả kích và trào phúng như Vũ Trọng Phụng nhưng nó cũng đủ sắc bén để chọc ngoáy và làm cho trái tim độc giả tan nát. Thật đấy =)). Câu chuyện rất buồn, lột tả trần trụi thực trạng cuộc sống bấy giờ và nhất là phận đàn bà, lúc nào cũng phải chịu khổ vì cái xã hội phong kiến thối nát. Đọc mới biết xã hội Việt Nam bây giờ đã thay đổi rất nhiều, phụ nữ có không ít ưu ái và quyền lợi. Và nên đọc để biết quý trọng hơn những gì mình đang có.
5
464538
2015-08-14 09:58:49
--------------------------
259034
4757
Phải nói là bộ sách Việt Nam danh tác của Nhã Nam thực sự mang nhiều ý nghĩa, Nó giúp cho những độc giả trẻ như tôi được có cơ hội tiếp xúc dễ dàng hơn với những tác phẩm kinh điển trong các giai đoạn văn học trước, để không phải đắm chìm trong văn học nước ngoài mà không nhận ra văn học Việt Nam tuyệt vời đến thế, ý nghĩa như thế. Tác phẩm này của Nguyên Hồng mình đã đọc qua một lần nhưng vẫn mua ở Nhã Nam đọc lại, thực trạng xã hội trong giai đoạn cũ được tác giả lột tả trần trụi, là một lời lên án tố cáo gay gắt. Đọc cuốn truyện tôi đặt mình vào trong giai đoạn ấy mới thấy thật kinh khủng, thân phận bèo bọt của đàn bà, xã hội thối nát của bọn nhà giàu, mới thấy được sống trong môi trường tốt như ngày nay thật sung sướng ,may mắn biết bao. Cám ơn Nhã Nam vì bộ sách này!
5
52762
2015-08-09 01:36:24
--------------------------
254559
4757
Một tác phẩm kinh điển của nền văn học nước nhà! Sau khi đọc nhiều tác phẩm nước ngoài rồi, mình muốn thử đổi gió với tác phẩm nước nhà xem sao, và Bỉ vỏ là tác phẩm đầu tiên mình đọc. Cuốn sách dùng từ ngữ "cổ", từ hồi thế kỉ trước, nhưng vẫn cực kì hấp dẫn. Cuộc sống của những con người nghèo khổ, sóng xô cuộc đời, sự thối nát, đê tiện của bộ nhà giàu vô lại, cái kết cho phận người con gái... Lúc đầu mình không rõ cái tên cuốn truyện, cứ tưởng là "Rỉ vỏ" - do cái bìa là hình chiếc xe lửa. Nhưng thật ra "Bỉ vỏ" là cách gọi cái "nghề" của nhân vật nữ chính, và chiếc xe lửa chính là nơi cô hành nghề.
5
513433
2015-08-05 12:24:11
--------------------------
235858
4757
Đọc truyện này làm mình nhớ đến bộ phim điện ảnh Hương Ga, tuy nó chỉ giống một phần và hình ảnh nhỏ của cô Hương Ga và cô Bính.
Cái nhan đề khi mình mới thấy nó trên Tiki, Nhã Nam thì cứ đọc là Rì Vỏ, Rỉ Vỏ rồi để ý kỹ thì là BỈ Vỏ. Rồi đọc suy nghĩ cái nhan đề đó có nghĩa gì, đọc tác phẩm, những từ chú thích mới có nghĩa là "người đàn bà ăn cắp, ăn trộm".
Một cuộc đời đau khổ của "nàng" Bính, những giọt nước mắt cơ cực, cô phải lăn lộn, đấu tranh, chật vật để kiếm miếng ăn cho mình và cho gia đình. Cô kiên cường từ Bắc vào Sài Gòn.
Đọc truyện thì có lẽ đây là tác phẩm đầu tay của chàng trai mới mười sáu, mười bảy tuổi đầu Nguyên Hồng thì câu từ vẫn còn khá non, không được trau chuốt, nhưng qua đó ta vẫn thấy được cái nhìn lớn tuổi của một đứa trẻ thành niên chuyển sang thanh niên. Ta có thể cảm nhận được tầm nhìn và sự quan trọng của cách nhìn nhận sự việc của những người trẻ tuổi, vẫn có thể hiểu và làm được như một cậu bé Nguyên Hồng xây dựng nhân vật nàng Bính trong Bỉ Vỏ.
4
472508
2015-07-21 13:30:33
--------------------------
222458
4757
Có thể nói, đây là một trong những tác phâm xuất sắc của Nguyên Hồng. Đây là tác phẩm đầu tay của một nhà văn trẻ chưa đầy hai mươi tuổi. Tuổi nghề qua trẻ, nhưng  có lẽ chính điều đó đã làm nên một giọng văn đôi lúc ngây ngô và đặc sắc.
Tác phẩm cuốn hút bạn đọc trước hết ở lối văn rất gần gũi, giản dị, tiếng lóng... cùng với cách gọi nhân vật rất đặc biệt: nàng!
Tác phẩm mang một cốt truyện đặc sắc, độc đáo, kết thúc buồn, bất ngờ. Qua đó, ta thấy rõ ràng được cuộc đời đầy những khổ đau, đầy nước mắt của  Bính, một cô gái quê nhưng vì miếng cơm mà dần dần trở thành một cô gái giang hồ.
Bản in của Nhã Nam bìa đẹp, chữ rõ ràng.
Mình có góp ý là thông tin quyển sinh có ghi số trang là 405, nhưng thực tế là khoảng 300 trang hơn!
5
616731
2015-07-05 16:41:04
--------------------------
219870
4757
Mình được giới thiệu cuốn sách này rất lâu rồi, nay mới tìm được nó nên đọc có rất nhiều cảm xúc. Cuốn sách được viết bằng giọng văn hiện thực, miêu tả rõ cái thối nát của xã hội xưa bẳng ngôn ngữ dân dã giống như các tác phẩm của thời kì trước. Khi đọc bạn sẽ bị cái hiện thực của cuốn sách làm cho giận dữ, ghê tởm, sợ hãi,.. Đó chính là điều mình tâm đắc ở cuốn sách này.
Bìa sách chính là thứ thúc đẩy mình mua sách này, thiết kế rất đẹp, chất lượng giấy tốt. Bên trong sách cũng được canh lề, chừa trang, trình bày sang trọng, xứng đáng là một quyển sách cao cấp.
5
63566
2015-07-02 00:38:01
--------------------------
209318
4757
“Bỉ vỏ” đã phản ánh chân thực xã hội thối nát đương thời. Bính từ một cô gái xinh đẹp, hiền lành, ngây thơ và lương thiện đã bị dòng đời xô đẩy, trở thành một người đàn bà giang hồ. Ngay cả đến Năm Sài Gòn, một tên cướp khét tiếng, cũng là người giàu tình cảm, luôn quan tâm và yêu thương Bính. Trải qua biết bao nhiêu biến cố, khổ đau, cuối cùng họ lại phải chịu đựng cuộc sống trong ngục tù. Xã hội thối nát, đầy rẫy bất công đương thời đã làm cho họ tha hóa, cướp mất quyền sống của họ. “Bỉ vỏ” là một thành công khi đã tái hiện lại thời kì ấy và thể hiện tiếng nói cảm thương những con người lương thiện bị chà đạp. Chúng ta không nên bỏ qua tác phẩm này.
4
458943
2015-06-17 12:52:05
--------------------------
179671
4757
Bìa sách là một trong những tiêu chí mình chọn sách.Vì dù sao nó cũng là bộ mặt của cuốn sách mình chọn.Bìa sách của Nhã Nam lúc nào cũng làm mình hài lòng vì đẹp và hài hòa.Hồi nhỏ mình cũng đã đọc Bỉ vỏ nhưng lúc đó chua thấu hiểu lắm.Bây giờ chọn mua lại để cảm nhận lại một tác phẩm mình đã đọc để hiểu hơn nữa về tác phẩm và cả tác giả nữa.Văn học Việt Nam luôn là lựa chọn số 1 của mình vì họ là người trực tiếp viết ra những con chữ bằng tâm hồn mình.
5
440861
2015-04-07 20:32:38
--------------------------
166214
4757
Mình cứ nghĩ sẽ rất khó để có thể tìm được một bản Bỉ Vỏ hoàn chỉnh nhất, bởi mình tìm mãi trên thị trường mà không có. May quá, nhờ có dự án Việt Nam danh tác của Nhã Nam mà mình cuối cùng cũng có cơ hội cầm trong tay ấn bản hoàn hảo nhất của Bỉ Vỏ - một trong những tác phẩm làm nên tên tuổi của Nguyên Hồng!
Có lẽ sẽ không cần phải bàn nhiều về nội dung của Bỉ Vỏ nữa, bởi nó đã được công nhận bởi biết bao thế hệ độc giả. Ấn bản mới này của Nhã Nam chính là sự hồi sinh cho tác phẩm xưa của Việt Nam. Thiết kế bìa đơn giản mà khái quát được nhiều nội dung và không khí của chuyện. Chất lượng giấy cũng rất tuyệt vời.
Dù ngôn ngữ trong truyện còn đôi chỗ khó hiểu dù đã có chú thích do tôn trọng nguyên tác xưa nhưng cá nhân mình thấy đây là sự cố gắng rất đáng khuyến khích của Nhã Nam trong việc hồi sinh các danh tác xưa.
5
48662
2015-03-12 09:23:34
--------------------------
163707
4757
Mình rất thích bìa của các cuốn trong bộ Việt Nam danh tác của Nhã Nam, và bìa cuốn Bỉ vỏ này cũng không ngoại lệ, nó phảng phất nét gì đó xưa cũ, tiêu đề truyện cũng được để rất sáng tạo, nói chung là ưng. Còn nội dung thì Bỉ vỏ của Nguyên Hồng là một tác phẩm đã quá nổi tiếng rồi. Đây quả là một bức tranh sinh động khắc họa rõ nét về những phận người dưới đáy xã hội, những kẻ bị đẩy vào con đường tội lỗi, là những tên lưu manh, kẻ cắp như Tám Bính, Năm Sài gòn...Tác phẩm không những mang giá trị hiện thực, giá trị nhân đạo mà còn có giá trị nghệ thuật không nhỏ, khiến ta vừa ghê sợ vừa xót xa cho những kẻ mang đầy tội lỗi đó.
5
17740
2015-03-05 19:00:01
--------------------------
163623
4757
Mình đã đọc Bỉ Vỏ cách đây rất lâu rồi, nhưng khi cầm và đọc lại tác phẩm này cảm giác vẫn còn đượm buồn và đau đáu như xưa. 

Mối nhân duyên truân chuyên của Tám Bính - một số phận khốn khổ ngay từ lúc cô bắt đầu mang trong mình đứa con của người đã bạc tình cạn nghĩa, dẫn đến thật nhiều những bi kịch liên tiếp đẩy cô vào những bước đường cùng bất khả vãn hồi, và Năm Saigon - một tên "chạy vỏ" khét tiếng yêu thương Bính hết mực, rồi cũng vì kế sinh nhai mà đã phụ tấm lòng Bính hết mực - được Nguyên Hồng ném vào một cuộc đời đầy rẫy những khổ đau và tàn nhẫn, nơi tình thương của con người chỉ nảy nở rất nhỏ nhoi và tạo hóa luôn sẵn sàng trêu ngươi từng số mệnh. 

Cuộc đời Bính khổ, khổ vì sự phụ bạc của tình cũ, khổ vì kiếp gái điếm nhà thổ, khổ vì mất đứa con rứt ruột, khổ vì một tình thương chân thật ẩn sau sự cứu vớt của Năm, khổ vì thương Năm mà thỏa hiệp cả với kiếp chạy vỏ của Năm, khổ vì đôi lúc Bính nhận ra mình không hề khổ, khi từ một người con gái không biết cả mùi đời trở thành một ả đàn bà cao tay trong nghề trộm cắp, để rồi, Bính khổ vì đứa con thất lạc bao năm lại là nạn nhân của chồng mình, đã chết bởi chính cái nghiệp mà mình đang đeo mang...

Bỉ Vỏ là một trong nhiều những quyển trong bộ Việt Nam Danh Tác mà mình đọc lại nhiều lần, và mình sẽ còn đọc lại nhiều lần nữa, lý do đơn giản là mình  rất thích văn chương của các cụ ngày xưa. Riêng về cuốn này thì Nhã Nam làm chưa được trọn vẹn lắm, vì trong tác phẩm có rất nhiều tiếng lóng, mặc dầu đã có bảng tra cứu để đối chiếu ở cuối sách, nhưng vẫn thiếu một số từ quan trọng. Mình cho 5 sao vì nội dung tác phẩm kinh điển này thôi.
5
81526
2015-03-05 15:10:18
--------------------------
