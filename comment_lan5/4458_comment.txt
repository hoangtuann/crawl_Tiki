394940
4458
Mình mua sách này để luyện trí thông minh cho bé nhà mình. Sách có rất nhiều bài bổ ích để luyện cho bé. Nhưng có lẽ mình mua sớm quá nên bé chưa dùng được. Mình nghĩ những bài luyện tập trong sách này sẽ hợp với những bé đã biết một chút. Tầm tuổi có thể dùng và hợp lý mình nghĩ phải dành cho bé trên 3 tuổi. Tuy nhiên sách rất hay và bổ ích dành cho các bé để các bé có kỹ năng mềm trong cuộc sống. Rất cần thiết cho các mẹ muốn luyện tập cho con.
3
919917
2016-03-11 09:25:11
--------------------------
