317711
3102
Tomato Toeic Compact part 1,2 là một quyển sách dành cho những bạn mới tiếp cận Toeic , với mục tiêu từ 500 điểm trở xuống. Quyển sách được chia làm hai phần chính.
+ Phần đầu gồm 10 bài giảng nói ngắn gọn về các từ vựng liên quan đến những chủ đề thông dụng nhất và những đặc điểm lưu ý khi làm bài ở phần part 1, 2. Những kiến thức được cung cấp ở phần lý thuyết sẽ giúp cho người học nắm được những nội dung cơ bản để có thể dễ dàng tiếp cận, thực hành tốt hơn với part 1, 2 của đề thi Toeic thực tế
+Phần 2 gồm 15 bài test cụ thể, giúp người học vận dụng những kiến thức đã được giới thiệu ở phần lý thuyết vào thực hành. Đặc biệt, sau mỗi bài test sẽ có những tip nho nhỏ để bạn hoàn thiện kỹ năng một cách tốt hơn. 
Với cách bố trí nội dung cuốn sách một cách khoa học như vậy, mình nghĩ đây là một cuốn sách khá hay và thực tế dành cho những bạn tư ôn luyện. 


4
43591
2015-10-04 01:42:35
--------------------------
