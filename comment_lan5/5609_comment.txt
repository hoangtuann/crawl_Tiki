468195
5609
Mình đã đọc rất nhiều truyện conan và không ngoài mong đợi cuốn này rất hay. Có rất nhiều điều thú vị trong cuốn sách này. Thứ nhất là về phần tiếp nối của tập trước. Nhờ đó mà mình lại biết được thêm một điều nữa là hiện tượng cột sáng Mặt Trời. Và cũng không ngờ rằng đoạn này kết thúc bằng một kiss. Điều thứ hai là không tin được là cũng có vụ án mà ba của conan bỏ giữa chừng. Điều thứ ba mình thấy được trong tập này là anh subaru chắc chắn có quan hệ gì đó với chị của Ai chan chắc là bạn trai cũng nên. Điều cuối cùng là sự thay đổi của haibara cô bé cởi mở hơn với bạn bè và dẫn chứng rõ nhất là hy sinh bí mật của mình để cứu bọn trẻ. Tập này thật sự rất hay.
5
1236771
2016-07-04 20:53:03
--------------------------
440130
5609
Takagi gặp nạn, được toàn đội cảnh sát cứu giúp và Sato, người yêu của anh đã ngoạn mục bắn mấy phát đạn làm đứt sợi dây trói, kịp thời cứu anh khỏi bị bom nổ cho banh xác. Tác giả viết truyện tình cảm hài hước chắc cũng hay lắm đây. Những tình tiết hay của tập này là muối tạo bọt, ám chỉ bằng bàn tay dưới sự trợ giúp của bóng, màn biểu diễn võ của Sera, Al uống thuốc giải độc trở thành Sherry để cứu bọn trẻ và vô tình để lộ sự hiện diện của mình. 
5
535536
2016-06-01 10:50:06
--------------------------
400988
5609
Thám Tử Lừng Danh Conan Tập 77  là tập truyện thật hay, không thể bỏ qua. Trong tập này, chúng ta sẽ được thưởng thức một vụ án là chấn động sở cảnh sát là trung sĩ Takagi bị bắt cóc.Không những thế, tập truyện này còn có sự góp mặt của Amuro, Subaru và Sera. Chúng ta sẽ được theo dõi phần kết bất ngờ của vụ án món quà của trung sĩ Takagi và vụ án chưa có lời giải của ông Yusaku Kudo. Đây quả là tập truyện tuyệt vời. Mình rất thích nó và không thể rời mắt khỏi trang giấy.
5
1174527
2016-03-20 00:35:40
--------------------------
