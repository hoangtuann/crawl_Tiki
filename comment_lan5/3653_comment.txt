279906
3653
Đây là câu chuyện mà cậu con trai 3 tuổi của tôi bắt đọc đi đọc lại nhiều lần, thậm chí cậu còn tưởng tượng rằng mình là một trong ba chú dê đực :) Câu chuyện đề cao sự nhanh trí (của hai chú dê em) và lòng dũng cảm (của chú dê anh cả) trong việc chiến đấu với gã khổng lồ độc ác. Sách giá rẻ (6.000 đồng),gọn gàng, lời văn rõ ràng, dễ hiểu, hình minh họa dễ thương. Trong giai đoạn đầu đời của các bé, khi lựa sách để đọc cho bé nghe giúp bé làm quen với việc đọc sách hàng ngày, những quyển sách như thế này tôi nghĩ là sự lựa chọn không tồi :)
4
332766
2015-08-27 11:06:10
--------------------------
