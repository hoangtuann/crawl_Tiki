307996
4673
Hồi nhỏ, tôi có rất nhiều sách hay, nhiều cuốn trong số chúng được bạn bè mượn mà không bao giờ trả lại. Chỉ tiếc là sau này tìm để mua mà không thấy, "Sống giữa bầy voi" là một cuốn như vậy. Đọc TP này của Vũ Hùng, ta sẽ hình dung được tình cảm của loài voi, chúng trung thành, biết yêu thương và căm giận. Để rồi từ đó, ta biết yêu thương thiên nhiên, động vật, nếu bảo vệ chúng, thì chúng cũng sẽ bảo vệ con người.
Cảm ơn Tiki, hàng tháng tôi đều mua cho con trai nhỏ bé của mình một cuốn sách, rất nhiều trong số đó đến từ Tiki, hy vọng lớn lên một chút, con trai tôi cũng sẽ yêu sách như tuổi thơ của bố. 
4
540941
2015-09-18 13:54:14
--------------------------
274793
4673
Vũ Hùng vốn là thần tượng văn chương của mình từ lâu. Tác giả vốn nổi tiếng với những quyển sách viết về thiên nhiên miền rừng núi dành cho thiếu nhi. Từng đọc giữ lấy bầu mật, sao sao,... Nên mình rất vui khi cầm trên tay quyển Sống giữa bầy voi này. Truyện kể về những chú voi nơi rừng núi hoang dã với những "luật rừng" rất lạ nhưng theo nhà văn thì rất nhân bản. Động vật có những đức tính tốt mà con người cần có như: không bao giờ hại hay ăn thịt đồng loại, luôn đoàn kết chống lại kẻ thù chung,... Bìa sách màu khá đẹp, giấy dày, màu rõ nhưng hơi cũ rồi, mong tiki khắc phục.
5
614800
2015-08-22 11:33:43
--------------------------
