438142
8265
Mình mua sách của anh Phương cũng một phần vì là đồng hương ( cùng quê Thái Nguyên) Là con rể một nhà văn hóa lớn ( Giáo sư Hoàng Ngọc Hiến), lại có lối sống trầm lặng, ít va chạm, nhiều người không thể nghĩ là anh ấy lại là Tổng biên tập Tạp chí văn nghệ quân đội , mang hàm Thượng tá.Thật sự cũng trong 1 số sự kiện tìm anh để xin chữ ký mà chưa lần nào gặp được.
Văn anh Phương phải đọc nhiều lân, đọc chậm mới thật sự hiểu được sâu xa cái nghĩa của nó. Là người đi qua chiến tranh, trưởng thành qua nhiều gian khó thì kinh nghiệm sống đủ cho anh viết từ nay đến hết cuộc đời này. Các bạn khi đọc phải thật thoải mái, thanh thản thì mới nên đọc, nếu đã thích thì sẽ bị nghiện đấy.
5
213133
2016-05-29 11:52:49
--------------------------
303456
8265
Cầm trên tay cuốn sách mỏng như Thoạt kì thủy, ngay từ đầu không bao giờ nghĩ rằng sức nặng của cuốn sách có thể lớn đến vậy. Đây có thể được xem là tác phẩm khó đọc vì nó mang nỗi ám ảnh về những kiếp người sống và cảm nhận cuộc đời bằng bản năng chứ không phải ý chí. Đàn ông, đàn bà, thanh niên... tất cả như đang mộng mộng ảo ảo trong chính giấc mơ lớn của đời mình. Tâm hồn họ như lúc nào cũng có bão, một cơn bão lớn mà ngay chính họ cũng không thể giải thích nguyên nhân hay thoát khỏi nó được. Điều này cho thấy ngòi bút đầy tính người và giàu chiều sâu của tác giả Nguyễn Bình Phương.
5
403666
2015-09-15 21:03:29
--------------------------
209730
8265
Thoạt kỳ thủy - câu chuyện của một thế giới người tha hóa bị nhân cách. Tính - một đứa trẻ sinh ra trong bao lực và suốt đời sống trong bao lực, hắn sợ "mắt chó - ánh trăng" như là sự mặc cảm bản nguyên của loài người. Câu chuyện của Tính là câu chuyện về vấn đề giáo dục của một thời đại. Mỗi người sinh ra không phải là mầm mống của cái xấu, nhưng thế giới xung quanh và chính môi trường tha hóa đã khiến cho bào thai đó càng ngày càng tha hóa. Thoạt kỳ thủy là một thế giới hỗn mang thực sự của làng Linh Sơn kia, như là sự khởi đầu hoang sơ, dã man của loài người nếu như không có giáo dục.
4
337674
2015-06-18 08:11:38
--------------------------
167680
8265
Chả hiểu sao Tiki xếp sách này vào thể loại văn học lãng mạn, chả có chút gì lãng mạn ở đây, chỉ có điên. Mình đọc mà thấy như 1 thế giới toàn người điên, đến nỗi người bình thường cũng khô héo chết rũ trong cái thế giới ấy. Từ đầu đến cuối là một mạch văn tự sự, dưới góc độ nhiều nhân vật, mà đa phần tâm trí họ không được bình thường, nên cảm giác của mình khi đọc sách là quá hỗn loạn, và khó có thể hiểu điều tác giả muốn truyền tải qua cuốn sách này là gì. Khi có 1 người hỏi mình em đang đọc gì thế, mình trả lời em đọc toàn người điên không, sau đó bật cười vì câu trả lời của chính mình, nhưng cơ bản mình chẳng nặn ra được một ý nghĩa, hay hình dung về 1 cốt truyện rõ ràng rành mạch để trả lời người ta. Sách bé bé, xinh xinh cơ mà đọc nặng đầu lắm đấy, bạn nào có ý định đọc thử lối văn tự sự của Nguyễn Bình Phương bằng một cốt truyện ngắn và nhẹ nhàng hơn thì mình khuyên là hãy bắt đầu với Trí Nhớ Suy Tàn chứ ko phải cuốn này
3
26136
2015-03-15 10:54:40
--------------------------
166480
8265
Tôi đọc cuốn sách này vào tối qua. Không biết là vì cà phê hay vì sự ám ảnh của cuốn sách này mà tôi cứ lăn lóc mãi. Không thể nào ngủ được.
Thoạt Kỳ Thủy là cuốn sách đầu tiên của N. B. P. mà tôi tiếp xúc. Sách này quả thật rất ám ảnh, ám ảnh kinh khủng. Tôi lại rất nhạy cảm với chất văn như thế này nên không thể nào cất nổi nó khỏi đầu. Con người trong T K T hiện ra với phần con quá lớn, đầy bản năng, kì quái. Đọc cuốn sách này khiến tôi hơi nhọc đầu vì phải loanh quanh giữa cái mơ và cái điên của nhân vật. Phải nói rằng đây là 1 tác phẩm hay, nhưng tôi vẫn chưa có đủ khả năng để hiểu hết ý nghĩa của nó.
4
114793
2015-03-12 20:30:20
--------------------------
