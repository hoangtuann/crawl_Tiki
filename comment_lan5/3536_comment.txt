218329
3536
Truyện cổ Grim là cuốn truyện cổ được anh em nhà Grim tập hợp. Những câu chuyện hoàng tử, công chúa, mụ phù thủy, những chuyến phiêu lưu kỳ thú,...  đầy màu sắc cổ tích theo phong cách Phương Tây với những tòa lâu đài nguy nga đồ sộ, những mụ phù thủy đội cái mũ nhọn hoắt, như bày ra trước mắt qua những lời văn trau chuốt của tác giả. Mình đọc tác phẩm này từ khi còn bé tí, cái thời không có tiền mua sách toàn phải vào thư viện mượn sách về xem, đọc liền tù tì mấy ngày liền hết
5
572238
2015-06-30 15:28:46
--------------------------
