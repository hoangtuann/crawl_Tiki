574273
4962
Truyện hay,  cảm động..............................
4
2195077
2017-04-15 20:27:57
--------------------------
349683
4962
Qua nét chữ và văn chương của tác giả  Nhậm Đại Tinh, có thể cảm nhận được tình cah con sâu sắc. Chính những biến cố xã hội, cái sự tham lam , tranh giành, giả dối, giả tạo của xã hội ngày nay. Xã hội đã cuốn đi cái bản chất tốt đẹp của người cha,khiến người con nghĩ xấu đến người cha. Nhưng qua đó cũng thông hiểu được tình cảm, tình phụ tử cha con giữa 2 cha con họ. Người cha cũng biết tự khắc phục mình sau đó.
Truyện khá hay và sâu sắc. Đáng để mua và đọc.
5
828055
2015-12-08 21:28:44
--------------------------
319098
4962
Người cha này có thể ta dễ dàng tìm thấy trong cuộc sống thường ngày. Người mà vốn là một người cha tốt, nhưng vì hoàn cảnh đưa đẩy mà dẫn đến những tệ nạn xã hội. Công ti phá sản, ông chạy trốn, trở về lái taxi, rồi lại đi đóng phim. Tuy nhiên bộ phim mà ông đóng lại là một bộ phim đồi trụy. Mặc dù vậy, cuối cùng ông cũng hoàn lương và xin lỗi người con trai yêu quý của mình.
Bìa sách tôi chưa hài lòng lắm về hình minh họa. Truyện cũng không có bookmark.
4
614288
2015-10-07 21:23:36
--------------------------
180960
4962
Nhìn vào cuốn sách làm cho tôi khá hài lòng về nội dung cũng như mặt hình thức.Cuốn sách đề cập đến tình cha con của Uông Lập Quần và Uông Đại 
Cương.Mở đầu chuyện cậu bé Uông Đại Cương đã khẳng định cha mình là một người cha tồi.Từ việc cha cậu làm trong công ty của ông ngoại cậu sau đó công ty đó bị phá sản,bố cậu từ lúc đó lại đi cặp bồ với Phi Phi  một thời gian mới thấy trở về nhà.Sau đó ông lại lái taxi,rồi lại làm người tốt khi nhặt được túi đồ và trả lại.Nhờ việc này ông làm diễn viên và đi đóng phim,khiến mẹ tôi rất vui mừng.Nhưng sau này khi tôi đi xem thì mới biết cha đóng phim toàn cảnh nóng và vi phạm pháp luật nên công ty đó phá sản.Cha cậu vì vụ này lại bị mất tích một lần nữa.
5
536745
2015-04-10 17:39:11
--------------------------
