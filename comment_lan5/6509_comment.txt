394205
6509
Mình đã từng xem bộ phim về cô gái tài năng này  nên khi đọc sách cũng càng khâm phục hơn về ý chí , sau này thấy tiki có nên mình order ,,   Với giọng văn đầy thực tế ,kể lại một cách đầy sống động về cuộc sống của bethany với đầy những mảng màu khác nhau .mình thật thấy khâm phục . Qua mỗi trang , mỗi chương cô gái này bày tỏ sâu sắc niềm tin tuyệt đối  vào Thượng Đế trong những thời kì khó khăn , , cuốn sách còn có thể làm quà tặng cho những người bạn , mình thấy giá cả khá ổn  cộng với dịch vụ bookcare nữa thì quá ổn
5
1051528
2016-03-09 23:30:20
--------------------------
135577
6509
Cuốn sách rất hay. Bìa ngoài đẹp, ấn tượng , phù hợp với nội dung cuốn sách. Chất lượng giấy in và dịch thuật cũng rất tốt. Mình rất ấn tượng và khâm phục hình ảnh nhà vô địch lướt ván Bethany Hamilton. Cô bị mất cánh tay trái trong một lần cá mập tấn công khi đang lướt sóng khi chỉ mới 13 tuổi. Sau đó, bằng niềm tin mạnh mẽ và sự đam mê cháy bỏng, cô đã vượt qua khó khăn, quyết tâm theo đuổi sự nghiệp lướt ván và trở thành nhà vô địch thế giới. Cuốn sách có ý nghĩa nhân văn sâu sắc, nuôi dưỡng tâm hồn ngày một tươi mới, cho dù có lúc thăng lúc trầm nhưng không bao giờ ngừng nghỉ- như những cơn sóng ngoài biển khơi. Mình đánh giá rất cao cuốn này.
5
414919
2014-11-16 10:05:50
--------------------------
131638
6509
Câu chuyện của Bethany Hamilton là một bài học quý giá cho những ai đang lùi bước trước những khó khăn thử thách trong quá trình đi đến mục tiêu. Cô đã chứng minh rằng khiếm khuyết cơ thể không là gì nếu quyết tâm của bạn đủ mạnh mẽ. Câu chuyện cũng đã được dựng thành phim nhưng mình thích đọc sách hơn cả. Vì chỉ khi đọc sách (qua lời tự sự của chính cô) mình mới có thể cảm nhận ý nghĩa của những khó khăn mà cô gái trẻ này đã trải qua. Nếu bạn đã biết câu chuyện của "Nick" Vujicic, bạn cũng nên biết thêm câu chuyện của Bethany. Đây sẽ là nguồn động lực tuyệt vời cho bạn.
5
398179
2014-10-26 14:36:35
--------------------------
