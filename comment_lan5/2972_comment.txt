251929
2972
Cầm trên tay 4 cuốn trong serri truyện , mình rất thích chất lượng lẫn nội dung Những câu chuyện đạo đức hoàn toang mới mẻ và rất có ý nghĩa, không những dạy cho các bé những bài học hay mà cả thúc đẩy vai trò của bậc phụ huynh trong việc dạy dỗ con cháu, đặc biệt là các mẹ bĩm sửa trẻ Cám ơn tiki đã cung cấp bộ truyện rất hay này, mình không rõ bộ này có những tập khác nữa không ạ, nếu có mình sẽ tiếp tục mua ủng hộ tiki, cám ơn
5
684106
2015-08-03 11:04:23
--------------------------
233100
2972
Quyển Tủ Sách Câu Chuyện Đạo Đức - Con Quạ Ngốc Nghếch (Song Ngữ Anh - Việt), có nội dung mang tính giáo dục cao. Từ câu truyện về chú quạ ngốc nghếch Mẹ sẽ dạy cho con bài học không được đễ dàng tin người lạ. Bé nhà mình rất thích vì qua quyển sách này bé luyện thêm những câu tiếng anh mới, áp dụng trong những tình huống thực tế của đời thường. Sách khá hay các mẹ có con nhỏ nên mua cho bé, vừa mang tính chất giáo dục vừa để luyện học Tiếng anh rất hữu ích.
5
460604
2015-07-19 14:35:43
--------------------------
