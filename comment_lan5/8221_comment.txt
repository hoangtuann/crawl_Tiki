464469
8221
Ở trường mẫu giáo các bé nhà mình được các cô tổ chức lễ hội halloween nên bé rất mê các nhân vật phù thủy và thần tiên, vì vậy mình đã chọn và mua quyển sách này cho bé. Nói chung nội dung quyển sách rất buồn cười và cũng có khá nhiều gợi ý cho bé hóa trang cho lễ hội halloween. Màu sắc trong sách tươi sáng, giấy bóng đẹp tuy nhiên đóng gáy không chắc lắm nên rất dễ rách, bạn nào mà mua cuốn này thì chú ý hoặc bọc sách hoặc dán băng dính lại để giữ sách được lâu hơn nhé.
3
1066198
2016-06-30 10:22:47
--------------------------
430592
8221
Cũng giống như những cuốn sách khách trong cùng một bộ Thế Giới Hình Ảnh Của Bé, cuốn sách này cũng là một cuốn sách có nhiều hình vẽ đẹp và dễ thương cùng những lời miêu tả dễ hiểu và gần gũi của tác giả Fleurus. Đây không phải là cuốn sách về khoa học mà nó giống như cuốn từ điển nhỏ giải thích cho các bạn nhỏ biết thế giới của phù thủy và tiên là như thế nào. Cuốn sách cũng có những hướng dẫn vui vui và dễ làm cho các bạn nhỏ thử tay làm những bộ trang phục xinh xinh cho Halloween. 
4
287808
2016-05-15 14:27:10
--------------------------
310034
8221
Đây là cuốn thứ ba trong bộ Thế Giới Hình Ảnh Của Bé mà tôi mua cho con. Tuy nhiên, tôi không thích quyển này lắm. Thứ nhất, bé chỉ xem cho vui, thông tin không nhiều và không hay như hai cuốn trước. Thứ hai, cuốn này chủ yếu vẽ về phù thủy, Halloween và những gì liên quan, một phần nhỏ khác là vẽ các bà tiên. Halloween không xa lạ với các bé ngày nay, nhưng nội dung sách tôi cho rằng thích hợp với trẻ em phương Tây hơn với nhiều hình ảnh ma quỷ, đầu lâu ghê rợn. Điều này cũng dễ hiểu vì sách do Fleurus xuất bản, Đông A chỉ là đơn vị mua bản quyền. Tuy vậy, có một
3
479122
2015-09-19 11:42:47
--------------------------
277983
8221
Bìa cuốn sách nhìn rất hấp dẫn trẻ em, hình ảnh và màu sắc thật sinh động đa dạng. Việc miêu tả tính cách từng phù thủy là sự lồng ghép nét đặc trưng của mỗi quốc gia trên thế giới khiến trẻ có sự hiểu biết rộng hơn ngoài đất nước chúng đang sinh sống. Cuốn sách là nối tiếp các câu chuyện nhỏ làm cho trẻ có cảm giác như đang được sống trong một thế giới cổ tích huyền bí mà xung quanh chúng là cả thế giới động vật và đồ vật vô cùng gần gũi và đáng yêu. Các bé trai dễ dàng bị mê hoặc bằng phòng thí nghiệm của phù thủy với đầy đủ các loại chai lọ, hóa chất là những côn trùng, động vật gần gũi xung quanh chúng. Còn các bé gái thì vô cùng yêu thích các bộ trang phục cũng như phụ kiện của các bà phù thủy. Sách còn có phần hướng dẫn làm các trang phục trong lễ hội hóa trang, với vật liệu đơn giản dễ kiếm dễ tìm, các bé có dễ dàng tự làm cho mình những chiếc áo chiếc mũ ngộ nghĩnh đáng yêu.
5
518902
2015-08-25 15:00:28
--------------------------
223085
8221
Những cuốn sách của Fleurus thật là đẹp ý, mình ngắm nghía hoài mà thích quá. Thật tuyệt khi nghĩ bọn trẻ con bây giờ có bao nhiêu sách đẹp thế này để làm tăng niềm say mê học hỏi và khám phá. Mỗi cuốn sách là bao điều mới, những hình ảnh đẹp luôn hấp dẫn, thu hút và khiến bé nhớ lâu. Cuốn sách này - Phù thủy và thần tiên - là những hình ảnh tuyệt đẹp về thế giới cổ tích diệu kì, là một tập hợp những điều thú vị về thần tiên, phù thủy. Với những em bé đã say mê nghe truyện cổ tích, hẳn là sẽ rất thích thú với cuốn sách này.
5
82774
2015-07-06 16:08:48
--------------------------
