419904
4936
Tập này tuy là phần tiếp theo của Tâp 1, nhưng lại nói nhiều về những vụ mà Tổ Gia đã thực hiện khi còn trẻ. Cũng như cho biết rõ hơn về nguồn gốc của Tứ Gia và vợ của ông Hoàng Pháp Dung, người đã được đề cập cuối tập 1. Tứ gia là một người am hiểu khoa học kĩ thuật phương Tây, giúp tạo ra những công cụ giúp cho Tổ gia thực hiện được thuật Trát phi của mình, giúp mình biết thêm một số mánh khóe của người Trung Hoa xưa. 
Nhưng mình thích nhất là màn chất vấn của Tổ gia với các tông phái kì bí khác như Mai Hoa Dịch số, Kỳ môn Độn giáp, Phong thủy địa lý, thật xem tướng và Bốc phệ. Đoạn này là hay nhất, giúp hiểu thêm Khoa học phương Đông xưa, mà không bị mê tín dị đoan.
5
125876
2016-04-22 10:41:56
--------------------------
248026
4936
Mình đọc xong bộ "Tôi là thầy tướng số" và rất yêu thích nội dung tác phẩm. Sách là tổng hợp giữa những dữ kiện khoa học và điều thần bí. Có những chi tiết rất đắt về nhân văn, về nghệ thuật sử dụng từ ngữ, xây dựng tính cách nhân vật và sẽ giải thích cho bạn một số điều bí ẩn như "Cương thi", "Mèo đen chạy qua xác chết". Đồng thời, sách cũng mang những điều khiến bản thân mình phải suy ngẫm về cuộc sống, cách nhìn nhận vấn đề. Kết thúc tập 2 hơi lửng lơ, không biết sẽ có tập 3 không. Nếu có, mình chắc chắn sẽ mua. 
5
169228
2015-07-30 17:08:36
--------------------------
243937
4936
Một bộ truyện mà mình thật sự không biết nên xếp nó vào thể loại nào, có 1 chút kinh dị, 1 chút phiêu lưu, 1 chút huyền ảo, nhưng cũng lại rất trần trụi và thực tế. Tưởng đâu, cuộc đời Tổ Gia - bậc đại tài tướng số, ông trùm lừa đảo đã kết thúc sau tập 1, nhưng sang tập 2, cuộc đời ông lại được phơi bày thêm nhiều góc khuất khác, li kì hơn, hồi hộp hơn và cũng day dứt hơn. Cuối cuốn 2, những tưởng mọi chuyện đã kết thúc trong quá khứ, tuy nhiên hàng loạt nhân vật tưởng như không còn bỗng nhiên quay về, và hiện hữu giữa thực tại. Thật sự, mình rất mong chờ các tập tiếp theo càng nhanh càng tốt.
Đây thật sự là 1 bộ truyện hay và sẽ không hối hận khi đọc nó, tuy nhiên bộ sách vốn đã rất chỉnh chu từ bìa, in ấn, chính tả, lại bị 1 lỗi nhỏ khác màu nơi gáy sách khiến 2 tập không được đồng bộ khi xếp cạnh nhau, rất là đáng tiếc
5
41744
2015-07-27 21:07:04
--------------------------
210440
4936
Mình không thích cái trò không rõ ràng liên quan tới tướng số và đây là quyển sách liên quan tới tướng số đầu tiên mình mua, không phải vì muốn tìm hiểu mà một phần do tò mò. Quả thực quyển sách còn hơn những gì mình mong đợi.
Mình đã đọc và có nhận xét ở phần 1 của quyển sách về việc đọc truyện này chẳng khác gì truyện kinh dị, thực mà kinh dị ở cách các A Bảo đối xử với những người mê tín. bóc trần tất cả các chiêu trò của phái Giang Tướng. 
Tuy nhiên, khi đọc xong toàn bộ 2 tập, mình không khỏi cảm giác khó tả về các vấn đề chưa rõ trong quyển sách, rõ ràng các trò A Bảo dựng lên đều là thực, các âm mưu đều là thực nhưng nhuộm trong đó vẫn có sự huyền bí liên quan tới nhân quả, sự trả giá.
Nói chung đây là một bộ rất đáng để đọc, 5 sao cho toàn bộ quyển sách ^^ tuy nhiên mình không hiểu tại sao tập 1 và 2 ra cùng một lúc mà lại không có một Set nguyên bộ mà phải mua rời rạc
5
324739
2015-06-19 14:45:53
--------------------------
174424
4936
Hết cuốn một rồi đến cuốn hai, tôi bị cuốn hút vào một thế giới hoàn toàn mới lạ và đầy những thủ đoạn. Thế giới ấy tồn tại song song cùng chúng ta nhưng sao tôi chưa từng được biết đến. hàng loạt những biến động của môt giai đoạn lịch sử Trung Quốc được trình bày nhưng lại không hề gây chán hay rối cho người đọc. Hoàng Pháp Dung sẽ ra sao, Tổ Gia thật sự còn sống hay không, và người phụ nữ lạ mặt trò chuyện với Thiên Lượng là ai... hàng loạt câu hỏi vẫn chờ được giải đáp. Mong rằng những tập tiếp theo sẽ nhanh chóng được xuất bản
5
513549
2015-03-27 22:53:13
--------------------------
172335
4936
Lâu lắm rồi mới được đọc một tác phẩm kinh dị, ly kỳ và triết lý đến thế. Câu chuyện xoay quanh cuộc đời nhận vật “Tổ Gia”, chưởng môn phái Giang Tướng, một bậc anh tài tuấn kiệt.
Tác phẩm đề cập, giải thích nhiều học thuyết của Trung Quốc từ nho giáo, lão giáo, phong thủy, dịch số, thần số, đẩu số, chu dịch, kinh dịch…
Trong truyện có rất nhiều nhân vật sừng sỏ được khắc họa rõ nét, mưu cao, kế thâm, thủ đoạn khó lường.
Các chi tiết thực thực, ảo ảo, ma quái đan xen. Những thủ đoạn bói toán, đoán mệnh, cải vận, nhập đồng, gọi xác… bị bóc mẽ một cách thú vị, bất ngờ.
Nhưng sợi chỉ vàng xuyên suốt tác phẩm là tư tưởng nhân quả báo ứng của nhà Phật.
Chỉ mới đọc qua 2 tập đầu của tác phẩm tôi vẫn chưa biết Tổ Gia có thật sự đã chết chưa? Liệu ông đã luyện thành bậc chân sư có thể đoán được vận mình hay vẫn cứ là “tên lừa đảo siêu hạng”? Tổ Gia đã gài bẫy thế nào để đưa Hoàng Pháp Dung vào chỗ chết? Người đinh ninh đã chết lâu rồi lại bỗng nhiên xuất đầu lộ diện, ả đã dùng trí thông mình tuyệt đỉnh của mình để thoát chết như thế nào? Vẫn đang chờ tập 3, 4.
5
381838
2015-03-23 20:40:47
--------------------------
163930
4936
Về bìa thì giống cuốn một, bìa đẹp, nhẹ, mềm và khá là bền. Chất liệu giấy tốt, không có mùi hăng khó chịu mà lại có mùi khá là thơm ( theo mình là mùi giấy da mới), giấy nhẹ, cầm đọc ít bị mỏi, màu giấy sáng dễ đọc. Cỡ chữ vừa nên dễ nhìn dễ đọc. Cuốn sách này cũng rất dễ bảo quản.
Cuốn hai thì thầy tướng số Tổ Gia bắt đầu đi khắp chốn giang hồ ngao du, kết bạn, học hỏi cách bói toán, vâng vâng. Nhưng điều quan trọng nhất là thấy muốn gặp được tài nhân trả lời cho mình câu hỏi: Tại sao tôi không thể bói cho chính mình?. Chính vì thế mà cuộc đời của Tổ Gia vô cùng đau khổ và thê lương.
5
525063
2015-03-06 10:21:20
--------------------------
