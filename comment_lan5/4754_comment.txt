454655
4754
Lịch Sử Tự Nhiên là cuốn sách phải có của những người yêu thích tìm tòi thế giới thiên nhiên, quả là món quà quý có thể mang tặng mọi người, mình rất thích sách giấy màu, cuốn sách to, dày nặng lắm nhưng vì chứa nhiều kiến thức hữu ích, rất vui vì sở hữu nó và chia sẽ cho người thân ai cũng thích đọc rất hấp dẫn, làm kệ sách nhà mình thêm nổi bật. Mong có nhiều quyển sách kiến thức như vậy được dịch và mình sẵn sàng mua . Cám ơn nhà xuất bản.
5
728931
2016-06-22 03:54:21
--------------------------
413013
4754
Trong một lần vào nhà sách mình vô tình tìm được quyển lịch sử tự nhiên này. Cảm nhận đầu tiên của mình là sách khá dày và nặng nhưng khối lượng kiến thức bên trong quyển sách thật đồ sộ và khổng lồ. Đây là cuốn sách đắc tiền nhất nhất mình mua trong tất cả các quyển sách. Nhưng quyển sách này đã không làm mình thất vọng. Mình rất yêu thích các sách của Đông A vì sách làm rất đẹp cả về hình thức lẫn nôi dung. Bên trong sách chứa đựng các thông tin về khoáng sản, động vật và thực vật rất phong phú. Tất cả đều in màu làm cho sách thêm sinh động. Mình nghĩ rằng mỗi người trong chúng ta nên trang bị cho mình 1 cuốn để biết thế giới tự nhiên rộng lớn như thế nào.
5
509425
2016-04-08 20:42:45
--------------------------
396846
4754
Tôi vừa mua được cuốn sách Lịch sử tự nhiên này vào đợt giảm giá mạnh của Tiki, thật sự rất hài lòng về giá cả và nội dung của cuốn sách. Sách có nội dung phong phú, đặc biệt nhiều hình ảnh sinh động và chi tiết về các loài thực vật và động vật trên trái đất, con tôi còn nhỏ mà say sưa xem hết trang này đến trang khác cùng bố mẹ. Tôi nghĩ đối với các cháu đang đi học thì đây sẽ là cuốn sách tham khảo tuyệt vời, bổ sung thêm kiến thức cho bộ môn sinh học trên lớp.
5
922206
2016-03-14 09:41:04
--------------------------
365349
4754
Mình mới mua cuốn sách này đọc cực kì hay và bổ ích phù hợp với mọi lứa tuổi mặc dù để dành tiền khá lâu nhưng mua không cảm thấy uổn phí
Thông tin gì cũng có vạn vật trên trái đất bổ sung thêm rất nhiều kiến thức cho mình mắc dù mình chưa đọc hết nhưng sẽ cố gắng đọc xong trong thời gian ngắn nhất vì cuốn sách quá ư là tuyệt vời
Mua cuốn này là lựa chọn rất tuỵet cho những bạn còn đag phân vân vì giá ca của nó nhé
Còn đang giảm giá nữa nên bạn nên mua đi ko thấy uổng đâu

5
474044
2016-01-07 21:30:13
--------------------------
277766
4754
Thấy bìa cuốn sách là lịch sử tự nhiên nên mình rất háo hức mua về để xem tự nhiên phát triển lịch sử của nó như thế nào. Và xem sơ qua mình thấy các loại hình khoáng động thực vật được để cập phong phú, sách trình bày bố cục đâu ra đấy, hình ảnh đẹp thiếu mỗi cái chưa được sắc nét như vật thật nên vẫn chỉ giống ghi chép lại màu sắc và hình dáng thôi, không gây cho mình cảm giác trầm trồ ngưỡng mộ và giấy in vẫn hơi bóng nên khó nhìn. Tuy nhiên sau khi cầm nó trên tay để ngâm qua cứu lại thì, mình cảm thấy hơi thất vọng về nó. Vì ngoài vài trang phác thảo sơ về sự sống, trái đất, khí quyển, sinh cảnh, tiến hóa, động thực vật (mỗi mục 2 trang) thì những thông tin về khoáng động thực vật trong sách chỉ nhử một bản mô tả về vài nét phác họa của nó chứ không giúp người đọc cảm nhận được LỊCH SỬ của nó qua các thời kì biến đổi của trái đất. Mình chỉ cảm thấy nó như một cuốn bách khoa cơ bản về khoáng động thực vật thì đúng hơn là một cuốn lịch sử.
Các thông tin hấp dẫn hơn cần nên được bổ sung để cuốn sách trở nên có giá trị hơn. Chẳng hạn như:
Mục khoáng vật, đá nên thêm thông tin về nơi thường xuất hiện, thời gian hình thành và thời gian xuất hiện trên trái đất, giải thích về thuật ngữ được đề cập trong sách,...
Thực vật: thời gian sinh sản hoặc thời gian nở rộ, bổ sung đầy đủ khu vực phân bố,...
Rất mong trong tương lai cuốn sách này sẽ được hoàn thiện tốt nhất để trả về cho nó giá trị mà cái tên nó mang theo.
3
609164
2015-08-25 11:37:49
--------------------------
254781
4754
Có thể xem đây là cuốn bách khoa tuyệt nhất của Đông A. Một cuốn sách đồ sộ, cuốn hút mọi lứa tuổi với những hình ảnh, thông tin được trau chuốt kỹ lưỡng. Cách sắp xếp nội dung dễ theo dõi và ghi nhớ. Một cuốn bách khoa tuyệt vời cho người đọc lạc vào Trái Đất bao la, đa dạng, phong phú với vô vàn động thực vật, các loại đất đá, khoáng sản. Cuốn sách mang giá trị vĩnh hằng về kiến thức khoa học và xứng đáng để được đặt trên kệ sách của mỗi người.
5
568029
2015-08-05 14:59:36
--------------------------
250552
4754
Mình thật sự rất hài lòng khi mua quyển sách này. Là một quyển bách khoa đồ sộ, hình ảnh và nội dung được chọn lọc tỉ mỉ, khá dễ hiểu và có tính hệ thống cao.
Bìa đẹp, giấy in đẹp, màu sắc và nội dung phong phú
Giá thành không được tính là cao với một quyển bách khoa đồ sộ như thế này. Đọc sách, mình thu nhặt được nhiều kiến thức bổ ích, cũng như thêm trân trọng và yêu thương Trái Đất xanh của mình hơn.
Một quyển sách xứng đáng có mặt ở bất kỳ kệ sách gia đình nào
5
75271
2015-08-02 10:32:17
--------------------------
222159
4754
Cuốn sách là một công trình đồ sộ về kiến thức, rất trau chuốt về nội dung, và hình ảnh. Cuốn sách thật sự thể hiện được tâm huyết của những người mong muốn mang một cuốn sách tổng hợp về khoa học tự nhiên giá trị đến rộng rãi mọi lứa tuổi người đọc ở Việt Nam; con nít vài tháng còn có thể xem được (theo ý kiến chủ quan của mình thì rất thiếu những quyển sách khoa học cho mọi lứa tuổi kiểu này). Cực kỳ hứng thú mỗi lần giở sách với các kiến thức thu được.Theo đánh giá của mình, đây là cuốn sách có giá trị..."long-life", hy vọng giấy sách của Đông A chịu được...50 năm. Về giá sách, nhiều bạn có thể còn đắn đo vì giá sách sao có vẻ..."trên trời". Nếu bạn vẫn có mối bận tâm như vậy, cứ lấy giá của sách chia theo kiểu "long-life", thì chi phí bạn bỏ ra mỗi ngày là quá ít cho những kiến thức giá trị bạn thu được. Cuốn sách là một món ăn tinh thần rất ngon, rất bổ...
5
39519
2015-07-05 04:33:56
--------------------------
205893
4754
Ngay khi Đông A cho phát hành quyển sách này, mình đã nhanh tay mua 1 quyển. Đông A có thế mạnh trong việc phát hành các sách về bách khoa bằng hình. Do đó mình rất yên tâm về chất lượng cuốn sách này.

Hàng ngàn bức ảnh được minh họa trong sách làm cho nó trở nên vô cùng sinh động, dễ hiểu, dễ tiếp cận. Nội dung sách cực kỳ phong phú. Từ khoáng vật, đá, hóa thạch đến các loài sinh vật, động thực vật... trên trái đất.

Ai đó từng nói, chúng ta hay nói chuyện "vũ trụ trên trời" mà quên mất rằng mặt đất của chúng ta vẫn còn nhiều điều chưa được biết tới và khám phá. Cuốn sách này giúp người đọc có cái nhìn tổng quát về thế giới tự nhiên nói chung. Đồng thời, những kiến thức mà nó mang lại rất thiết thực, nhất là cho các em học sinh. Mình cho rằng, các thư viện nhà trường nên có những sách quý như thế này cho các em tham khảo!
5
598102
2015-06-07 22:56:48
--------------------------
189763
4754
Tôi không thể tưởng tượng nổi tủ sách của mình có thể thiếu một quyển bách khoa toàn thư như thế này nên đã luôn rất chờ mong đến ngày nó được xuất bản. Tôi rất có niềm tin vào đội ngũ biên tập và dịch thuật của Đông A vì những quyển bách khoa được xuất bản trước đó đã làm tôi vô cùng hài lòng. Lần này cũng không ngoại lệ. LSTN là một quyển bách khoa tuyệt vời, hoàn hảo về chất lượng và hình ảnh, đa dạng phong phú về nội dung, lại được sắp xếp rõ ràng, rành mạch. Đây quả thật là một công trình nghiên cứu và dịch thuật công phu và tôi đảm bảo là bất kì ai một khi đã sở hữu quyển sách đều phải lao vào đọc ngay! Tôi may mắn có dịp ghé thăm Bảo tàng Lịch Sử Tự Nhiên Quốc Gia Hoa Kỳ trong một chuyến du lịch nên càng hiểu rõ hơn hết tầm quan trọng và đồ sộ của quyển sách này và những kiến thức nó mang lại. Vì vậy lời khuyên của tôi là các bạn đừng bỏ qua cơ hội sở hữu quyển LSTN có một không hai này, sẽ không cảm thấy nuối tiếc đâu! :3
5
5412
2015-04-27 22:34:03
--------------------------
180963
4754
Mình vừa được tặng quyển sách này tuần trước.Phải nói đây là 1 món quà tuyệt vời.Có quyển sách này trong tay mình mới biết được trên trái đất có bao điều lí thú với vạn vật muôn màu muôn vè.Với những hình minh họa đa phần là hình ảnh thực tế đã đem đến 1 thế giới vô cùng sống động về các loại khoáng sản,thực vật,động vật...phong phú trên trái đất.Có rất nhiều thứ mình mới biết lần đầu thông qua quyển sách này,rất tuyệt.Những ai là fan của DONG A hay đam mê sách tự nhiên như mình thì nên có trong bộ sưu tập của mình.
5
606483
2015-04-10 17:43:39
--------------------------
180439
4754
Bách Khoa Toàn thư về hình ảnh giới tự nhiên! cuốn sách to đẹp,dày dặn,được thiết kế và dịch thuật rất công phu.Cả cuốn sách là 1 bộ sưu tập các hình ảnh từ khoáng sản,quặng đá đến các loài chim,thú!Đông A luôn luôn làm hài lòng các độc giả khó tính! cuốn sách này người lớn đọc thì rất thú vị vì nhiều thông tin bổ ích,còn  các em thiếu nhi thì cũng phải mãn nhãn vì hình ảnh quá tuyệt vời! Mặc dù giá thành khá cao,nhưng mới mức giảm giá 20% hấp dẫn cùng với nội dung quá độc đáo,thì với số tiền như vậy thì vẫn hoàn toàn đáng đồng tiền bát gạo!
5
16159
2015-04-09 14:22:05
--------------------------
