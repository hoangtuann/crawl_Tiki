567737
2467
Phải công nhận tới thời điểm này tiki vẫn là trang web mua hàng online mà mình tin tưởng nhất, vừa đặt cuốn Fantastic Beasts And Where To Find Them hôm nay, tiki hẹn khoảng 3 hay 4 ngày nữa sẽ nhận được hàng nhưng mình nhận được ngay vào ngày hôm sau luôn, sách vẫn ở trong trạng thái cực kỳ tốt, không bị rách hay trầy phần bìa, tóm lại là cực kỳ hài lòng luôn! Good job!
5
1146591
2017-04-08 20:57:23
--------------------------
562697
2467
Cuốn sách đẹp và khá ấn tượng. Tuy nhiên, bọc bookcare làm hỏng bìa sách của tôi
4
2166330
2017-04-03 10:52:33
--------------------------
561917
2467
Sách đẹp và hay. Giá giảm đúng lúc cần mua để đọc sau khi xem phim.
5
733166
2017-04-02 10:08:22
--------------------------
556473
2467
Là 1 PotterHeads thì không thể thiếu rồi nên mình không có ý kiến :)))
PotterHeads nào chưa có là không xong với thầy Dum đâu nha :))) SGK bắt buộc đó :))
5
1380237
2017-03-28 13:30:17
--------------------------
553665
2467
Mình đặt sách vào đầu tuần và lịch trình dự định được giao sách là từ thứ 7 đến thứ 2, tuy nhiên mới thứ 5 đã được nhận sách. Mình rất hài lòng với toàn bộ thủ tục thanh toán và giao hàng của Tiki ^^
Một điểm trừ nhỏ là khi mở sách ra mình thấy các trang sách đều bị "uốn lượn", dạng như khi giấy bị ẩm sẽ cong và "uốn lượn" ấy? Làm mình hơi buồn 1 tẹo :(
Nhìn chung, mình tặng 5 sao cho Tiki :)
5
5101988
2017-03-25 09:22:01
--------------------------
553301
2467
Mọi điều về cuốn sách này rất tốt, và sẽ tốt hơn nữa nếu Tiki nhập thêm 2 cuốn còn lại trong bộ Library book!
5
794337
2017-03-24 21:32:02
--------------------------
552713
2467
Fantastic Beasts textbook phiên bản mới lần này thật sự đã gây sốt trong công đồng những người yêu thích bộ truyện Harry Potter. Đây là 1 trong 3 cuốn sách trong bộ Hogwarts Library, được chính tác giả J.K Rowling viết để làm từ thiện cho Comic Relief và Lumos.

Lần tái bản này đã bổ sung kha khá những điều thú vị, và cũng làm tiền đề cho 4 phần phim Fantastic Beasts sắp tới. Văn phong của cô Jo vẫn luôn như thế, vẫn có chút hài hước, và gợi mở rất nhiều điều. Đủ hé lộ một số tình tiết gây tò mò cho những phần phim sau, cũng đủ rạo rực và phấn khích khi tìm hiểu những sinh vật huyền bí (magical creatures) trong thế giới phù thủy.

Tuy đây chỉ là một cuốn sách giáo khoa được các phù thủy sinh Hogwarts học bắt đầu từ năm 1, nhưng nó không hề vô vị tẹo nào. Bạn sẽ phải bật cười trước sự ngộ nghĩnh của các loài sinh vật huyền bí, và một số loài thì lại vô dụng đến mức bạn sẽ ngờ ngờ vì sao chúng lại xuất hiện. 

Với văn phong gần gũi và cách viết khá dễ hiểu, Fantastic Beasts như đưa người đọc quay lại thế giới thân thuộc trong Harry Potter, mà vẫn giữ được nét đặc sắc của chính nó
5
19559
2017-03-24 10:32:19
--------------------------
551241
2467
Ngay lúc tiki giảm giá 50% mình đã đặt hàng ngay và luôn, 14/3 ra mà qua ngày hôm sau tiki đã giao rồi, thích cái giao hàng nhanh của tiki, sách thì đẹp tuyệt khỏi phải nói, cho tiki 5 sao luôn
5
2226714
2017-03-23 00:18:07
--------------------------
551185
2467
Chất lượng giấy in ở Anh quá tốt, giao hàng đúng hẹn, nhanh chóng.
Bổ sung các sinh vật mới so với lần trước.
Bìa cứng, giấy tài chế nhưng rất nhẹ và bền.
5
143276
2017-03-22 22:55:40
--------------------------
549273
2467
Ôi hôm nọ hốt được deal 50%, em sách về đẹp long lanh mê tơi, giá mà mua được đủ bộ Hogwarts Library :D
5
164757
2017-03-21 09:57:31
--------------------------
547969
2467
Mình nhận được cuốn sách trong vòng đúng 1 ngày kể từ khi xuất bản ở Việt Nam :)
Phiên bản đặc biệt này phát hành cùng lúc với thế giới nên mình rất hào hứng.
Về hình thức, sách đẹp, cứng cáp, 1 bìa giấy và 1 bìa cứng, bỏ bìa giấy ra trông như một cuốn sổ màu xanh lá cây rất đẹp mắt với gáy mạ chữ vàng.
Về nội dung thì chắc mình khỏi bàn, đây là cuốn sách giáo khoa của HP lúc còn học ở trường nên nội dung khá thuần túy học thuật, bao gồm các nghiên cứu và ghi chép của Newt Scanmader về các sinh vật huyền bí. Loài nào trông như thế nào, ăn gì, ở đâu đều sẽ được ghi chép đầy đủ.
Sách bằng tiếng anh, sẽ hơi khó khăn cho các bạn AV không tốt hoặc thậm chí là tốt nhưng chưa đọc qua bản gốc HP, vì cô JK xài khá nhiều từ ngữ chuyên biệt :D

Tiếc 1 điểm là bản này là "Phiên bản dành cho Muggle" - như đã được chú thích, vì vậy sẽ không có ghi chép của bộ ba.
Dù vậy bản này có bổ sung các Beast mới cũng như hình ảnh minh họa khá đpẹ mắt :D

Trừ bớt 1 sao vì không có chữ viết tay và chú thích của Harry thôi, còn lại oke miễn chê nha.
4
11664
2017-03-19 23:04:45
--------------------------
545801
2467
Thực sự em khá may mắn khi pre-order :)))) Trước kia thì giảm 50% và với mã giảm giá em được giảm thêm 10% nữa :))) so với giảm 30% bây giờ là một trời một vực :)))) sách đẹp, hình minh họa, nội dung hay, mùi sách đúng chuẩn bloombury :))))) Cơ mà cầm quyển sách thấy không dày lắm nên hơi hụt hẫng :((
5
1963926
2017-03-17 14:22:20
--------------------------
545209
2467
Là 1 potterhead thì quyển sách giáo khoa này với mình là ko thể thiếu.
5
404415
2017-03-16 20:33:01
--------------------------
543037
2467
Hình thức cuốn sách không thể chê vào đâu được. Sách bìa cứng in nổi tựa sách với hình vẽ trước sau bìa sách đẹp cực kì. Thiết kế nội dung trang sách với hình minh hoạ trắng đen nhìn rất đẹp mắt. Nhưng mà nội dung sách chỉ giới thiệu quanh các con beast trong phim với vài lời của tác giả thôi. Nên bạn nào đã biết về cốt truyện mà vẫn yêu thích phim cần tìm hiểu thêm thì hẵng mua. 
4
903295
2017-03-15 11:20:05
--------------------------
