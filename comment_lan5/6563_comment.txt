458179
6563
Không phải ngẫu nhiên mà cuốn sách này lại nổi tiếng trên thế giới như vậy. Tôi yêu mê Shelock Holmes, say mê những câu chuyện của ông, ngưỡng mộ tư duy của ông, và dành khá nhiều thời gian để đọc những vụ án của ông. Đọc trinh thám, không chỉ để giải trí, mà còn giúp tôi rèn luyện được trí tư duy, học hỏi được một chút từ óc quan sát của ông, khiến trí tưởng tượng của tôi trở nên phong phú hơn. Cuốn sách như một người bạn của tôi, và tôi học được từ nó khá nhiều điều
5
920424
2016-06-24 22:36:36
--------------------------
382269
6563
Những ai là fan của truyện trinh thám và đặc biệt là fan của bộ truyện tranh Thám tử lừng danh Conan thì sẽ không thể không biết đến Sherlock Holmes với tài phá án không ai sánh bằng. Đầu óc suy luận cực kì nhanh nhạy mà bất kì ai cũng phải trầm trồ thán phục. Tuy nhiên theo tôi thì truyện có vẻ hơi khó hiểu :))) đương nhiên là bởi vì Sherlock Holmes là một đầu õ thiên tài thì đầu óc bình thường như tôi làm sao mà hiểu được :v . Truyện rất hấp dẫn và kịch tính
4
1101166
2016-02-18 21:54:49
--------------------------
379640
6563
Với mỗi người có một nhận xét,một cảm nhận riêng nhưng với tôi-một con bé nghiền truyện trinh thám thì Sherlock Holmes chính là một kiệt tác mà Conan Doyle đã tạo ra. Đọc truyện...những trang đầu tiên tôi bị ấn tượng bởi sự quan sát,bởi sự thông minh,bởi trí óc tài tình của ngài Holmes.
Biết đến Holmes là lúc tôi đọc Conan,là lúc tôi luôn nhớ đến câu nói của Shinichi-một chàng thám tử được mệnh danh là Sherlock Holmes của Nhật Bản:"Loại trừ những cái có thể còn những cái cuối cùng dù khó tin đến đâu cũng chính là sự thật" hay" sự thật luôn luôn chỉ có một mà thôi "
Về hình thức của trang bìa...lúc đầu tôi không thích nó lắm vì tôi nghĩ thường khi chọn một cuốn sách người ta sẽ chọn khi bị ấn tượng bởi trang bìa nhưng tôi lại không bị nó thu hút chút nào...theo tôi tên của tác giả chỉ nên ghi là CONAN DOYLE chứ không nhất thiết phải thêm chữ ARTHUR.riêng dòng chữ trọn bộ 3 tập ấy cho sau chỗ dán tem ghi số tiền là được mà...đó chỉ là góp ý nhỏ của tôi thôi.
Rất mong các bạn tìm mua cuốn sách trinh thám hấp dẫn này

5
1151996
2016-02-11 21:34:54
--------------------------
361694
6563
nếu nói Conan là truyện tranh nổi tiếng về trinh thám mà bạn đã từng đọc, vậy sao bạn không thử tìm đọc truyện về Sherlock Holmes. hồi nhỏ tôi đã hâm mộ Sherlock holmes từ một bộ phim hoạt hình và cố tìm những cuốn sách về sherlock holmes, phải nói đây là một tác phẩm kinh điển, các chi tiết được viết rất cụ thể và tinh tế, đọc mà như bạn cũng tham gia vào vụ phá án, được thay, suy xét về các tình huống và đôi khi bạn sẽ quên mất rằng Holmes là 1 nhân vật hư cấu. thật sự bạn sẽ phải thán phục cách mà Holmes giải quyết mọi vấn đề, và bạn sẽ hiểu tại sao khi tác giả cho Holmes chết thì nhiều độc giả lại đòi kiện cáo,.....
5
492693
2015-12-31 14:32:31
--------------------------
296750
6563
Chỉ có thể dùng hai từ : " SIÊU PHẨM ". 
Quá khâm phục trước khả năng diễn giải, suy diễn logic cùng vốn kiến thức thâm sâu của mình về mọi lĩnh vực : từ y học cho đến hóa học, ngôn ngữ.....
Mở đầu từng vụ án, Conan Doyle dẫn dắt người đọc vào những chi tiết, manh mối vô cùng phức tạp, rối ren nhưng đầy huyền bí.Nhưng kết thúc, với sự tháo " tháo nút " từng chi tiết, rất cặn kẽ tác giả đã thuyết phục toàn bộ người đọc về " đáp số" cuối cùng của vụ án.
Một tác phẩm , xứng đáng là phải đọc trong thể loại trinh thám, hình sự
5
171183
2015-09-11 11:35:38
--------------------------
284426
6563
Đây là một cuốn truyện thuộc thể loại trinh thám rất hay, rất đặc sắc. Cá nhân mình là một đứa cuồng sách trinh thám thì đây quả thực là một lựa chọn đúng đắn nhất! Cuốn sách viết về những vụ án ly kì, nhiều tình tiếng khó đoán, khó lường trước. Nhân vật thần thám Sherlock Holmes có những lập luận rất sắc bén, logic. Đó là chiếc khoá để mở ra hết bất ngờ này đến bất ngờ khác cho người đọc. Cách viết của tác giả đặc biệt rất lôi cuốn, khiến người đọc khó lòng mà rời khỏi cuốn sách!
4
392399
2015-08-30 22:19:26
--------------------------
205835
6563
Một cuốn sách tuyệt vời trên cả tuyệt vời. Tôi được biết Holmes là thần tượng của Kudo Shinichi đứa con tinh thần của các bạn trẻ như chúng tôi cho nên nó đã khơi trí tò mò của tôi đến với Sherlock Holmes. Nó khác với những vì mà tôi biết về Kudo, nó như nâng cao thêm về một bậc thử thách trong từng vụ án. Khác với truyện Conan, Holmes thì phân tích qua từng chi tiết nó nhất và không có bất kì một nghi phạm như Conan, nó như một bậc thầy cho cái nghề tham tử. Tôi đã có quyển 1 rồi và đang cố dành tiền mua quyển 2 và 3. Chao ôi thích quá đi!
5
595429
2015-06-07 20:01:28
--------------------------
143766
6563
Những bạn đọc yêu thích truyện trinh thám chắc chắn không thể bỏ qua cuốn sách " Sherlock Holmes ". Riêng đối với tôi, nhân vật Sherlock Holmes để lại trong tôi nhiều ấn tượng sâu sắc cả về tính cách lẫn sự thông minh trời cho của nhân vật này . Tôi như bị cuốn vào từng trang văn. Những tình tiết li kì, hấp dẫn , kịch tính đến ngạt thở khiến cho bạn đọc khó lòng có thể rời mắt khỏi cuốn sách. Trước đây , tôi không hề thích loại truyện trinh thám bí ẩn , cho đến khi cầm trên tay cuốn sách này thì tôi mới thực sự hiểu được tại sao trên thế giới lại có nhiều người thích đọc Sherlock Holmes và yêu thích nhân vật này tới như vậy.
4
450785
2014-12-24 13:07:59
--------------------------
