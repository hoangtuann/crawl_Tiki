463168
6738
Phần 3 này điểm không ưng ý nhất chính là thuộc về bộ phận biên tập sách. Có quá nhiều lỗi, thậm chí mình phải nghĩ là nó được Auto Replace nên văn phong rất kỳ cục. Chất liệu in ấn cũng khá tệ, mực nhoè không rõ chữ. Hy vọng NXB chú ý hơn trong các sản phẩm khác, vì nội dung của sách đúng là quan trọng, nhưng chất lượng trình bày cũng ảnh hưởng không kém tới tâm trạng người đọc.

Phần này nối tiếp kịch tính của 2 phần trước, tuy nhiên người đọc vẫn chưa hiểu rõ dụng ý của tác giả với những tình tiết đó. Có vẻ mọi thứ còn hơi rối rắm. Kết thúc (tạm thời?) cũng mơ hồ, chưa làm người đọc thoả mãn. 

2
401462
2016-06-29 08:50:18
--------------------------
449014
6738
Một chút về bìa sách: bìa rất đẹp, vẫn giữ được chất huyền bí, thần thoại và đẹp ơi là đẹp như các bản khác của nước ngoài. Chủ đề của bộ truyện này cũng ko có gì mới mẻ: siêu nhiên, phù thủy, pháp sư, người sói, gọi hồn, yêu tinh NHƯNG dưới một cách tiếp cận khác có thêm khoa học, các nhà thí nghiệm và nhưng phòng lab, nơi cai quản lạnh lẽo các nhân vật chính trong truyện khiến cho bộ truyện này thêm sức hấp dẫn. Đọc hết tập cuối sức mạnh hắc ám đền tội thật ra mình có chút thất vọng. Tuy là tập cuối và cái kết khác mãn nguyện cho 4 bạn trẻ nhân vật chính tring truyện nhưng vẫn còn để lại một vài tình tiết chưa được giải đáp đủ thỏa mãn.
4
31396
2016-06-16 21:00:25
--------------------------
447921
6738
Mặc dù kết như vậy là quá hay nhưng mình thấy còn hơi thiếu thiếu một cái gì đó. Ước hì tác giả làm thêm phần ngoại truyện nữa thì tốt quá quá tốt.
Series Fantasy Sức Mạnh Hắc Ám thật tuyệt vời. Một cảm xúc mới sau bộ Soul Screamers.
Tiki đóng gói hàng kĩ lưỡng, sách đc bọc bookcare do tiki tặng. Giao hàng nhanh chóng, nhân viên giao hang nhiệt tình dù kiếm nhà mình hơi lâu. Nhưng vẫn vui vẻ tươi cười. Mình thấy rất vui vì chất lượng sản phẩm. Phục vụ khách hang chu đáo
5
1209811
2016-06-15 03:43:03
--------------------------
427751
6738
Là tập cuối trong series nên tác giả đã làm cho nhân vật chính buộc phải giải quyết khá nhiều tình huống cùng với sự giúp đỡ của nhóm bạn. Để thoát ra khỏi nhà Lyle là ý tưởng mạo hiểm nhưng là cách duy nhất để họ được tồn tại, đựơc sống, nên họ gần như liều mạng để đạt đựơc mucn đích cuối cùng ấy. Và tất nhiên là mình theo dõi từng câu chữ như thể mình là người trong cuộc và hân hoan mừng rõ khi họ được giải phóng, cũn như còn cả tương lai tươi sáng đang chờ đón phiá trước!
4
505121
2016-05-10 00:19:40
--------------------------
406452
6738
Đây là một trong những truyện fantasy mà mình yêu thích nhất. 
Yêu nhất là nhân vật Derek Souza, Derek có một tuổi thơ bất hạnh, sống trong khu thí nghiệm, từ nhỏ đến lớn luôn cô độc, bị mọi người xung quanh dè chừng, sợ hãi. Chỉ có mỗi cha con Simon đối xử tốt với Derek. Tính tình cậu cũng trở nên khó chịu, cộc cằn. Thật may là bây giờ cậu đã gặp được Chloe. Ở bên Chloe mới thấy được những mặt vui vẻ, diu dàng của Derek. 
Tác giả đã tạo nên một câu chuyện thú vị và đầy bất ngờ. Đưa ta đi từ ngạc nhiên này đến ngạc nhiên khác. Chloe ban đầu chỉ nghĩ mình bị bệnh nhưng sau đó phát hiện mình có năng lực siêu nhiên, một người gọi hồn đầy quyền năng, rồi phát hiện nhà mở Lyle không phải là một nhà mở bình thường. 
Ngạc nhiên hơn nữa là những mối quan hệ, ban đầu Chloe thân với Rae, nhưng sau đó mới biết Rae phản bội và Tori thì không hề xấu như mình nghĩ. Tình cảm Chloe ban đầu nghiên về phía Simon, nhưng sau đó mới nhận ra Derek mới là người mình yêu thương đích thực. Và Tori với Simon thực chất là anh em. 
Mình chỉ buồn 1 điều là kết thúc của truyện là kết thúc mở, giá như tác giả viết tiếp câu chuyện thì hay biết mấy, vì còn rất nhiều khúc mắt vẫn chưa được giải và cuộc chiến với hội Edison vẫn chưa kết thúc. Tuy vậy mình vẫn rất yêu thích bộ truyện này!
5
1144349
2016-03-28 12:37:54
--------------------------
343193
6738
Mình đã chờ cuốn sách này được sản xuất rất lâu rồi, sau khi đọc 2 phần trước thì mình không thẻ nào bỏ lở phần 3 này được. Và đúng như những gì đã mong đợi, cuốn sách đúng là tác phẩm tuyệt vời, với một kết thúc bất ngờ, không dễ đoán như những tác phẩm tiểu thuyết bình thường. Cuốn truyện này sẽ lôi cuốn các bạn, khiến các bạn từ bất ngờ này đến bất ngờ khác, và tôi cam đoan rằng bạn sẽ không hối hận khi xem bộ ba sức mạnh hắc ám này.
Nếu là 1 fan của tiểu thuyết giả tưởng sức mạnh, thì không thể nào bạn có thể bỏ qua bộ truyện này. Mình ước gì có thêm một phần 4 nữa, kết thúc sớm quá. Khó mà có thể kiếm thêm được một bộ hay như vậy
5
931354
2015-11-25 22:25:37
--------------------------
303217
6738
"Series Sức Mạnh Hắc Ám" là một tác phẩm hay nhưng ở phần 3 - Đền Tội được cho là phần cuối này, tôi thấy nó chưa đủ để kết thúc câu chuyện khi mà còn rất nhiều thứ lỡ dỡ. Tác giả xây dựng nên rất nhiều điều bí ẩn, từ hội Edison, về cha của Simon,  Derek và nhất là chi tiết về việc mẹ Chloe chỉ là một người bình thường nhưng tại sao lại có được sợi dây chuyền làm giảm sức mạnh tà ác?,... và thậm chí ngay cả cái chết của những nhân vật phụ, nó khiến tôi khó hiểu khi tình tiết truyện diễn biến quá nhanh và có phần đuối khi các vấn đề không được giải thích cho người đọc hiểu một cách thấu đáo. Thêm một điểm trừ nữa là người dịch sách thật sự dịch rất tệ, từ ngôn ngữ tới cách xưng hô lộn xộn cả lên, có lúc thấy rất vô duyên và vẫn chưa sửa được lỗi chính tả. Ngoài ra thì bìa sách trong series truyện này Chibooks làm rất đẹp mắt còn về phần nội dung, truyện rất hay nhưng nếu tác giả kéo dài thêm một tập nữa để làm rõ các vấn đề thì sẽ hoàn hảo vô cùng.
3
44933
2015-09-15 18:39:35
--------------------------
280410
6738
Đây là cuốn cuối cùng trong bộ, và mình có cảm giác nó chưa đáp ứng được kỳ vọng của mình. Nhưng cũng không thể gọi là thất vọng được vì truyện quá dễ thương và khá lôi cuốn. Từng nhân vật được tác giả miêu tả một cách đầy đủ và khá chuẩn xác, nhất là về Tori và Chloe, cặp đôi kỳ lạ nhưng cũng cực kỳ đáng yêu. Về phần nội dung mình thấy nhiều chi tiết chưa hợp lý và chưa được giải thích một cách rõ ràng cho người đọc hiểu. Sách trang trí đẹp nhưng vẫn mắc lỗi như cuốn Thức Tỉnh đó chính là lỗi chính tả, tuy có giảm hơn so với cuốn trước nhưng vẫn còn khá nhiều.
4
609835
2015-08-27 17:20:27
--------------------------
225028
6738
Series Sức mạnh hắc ám là một trong những series kì ảo mình thích nhất. Cuốn "Triệu hồi" thực sự làm mình ấn tượng. Đến cuốn "Thức tỉnh", mọi thứ có vẻ đuối đi một chút và mình đã mong chờ một sự bùng nổ ở cuốn thứ ba này. Nhưng sự thực là mình có đôi chút thất vọng với "Đền tội."

Mọi thứ dường như vẫn chẳng đâu vào với đâu. Vấn đề mà mình mong muốn được giải quyết nhất - một con đường mới, một cuộc sống mới cho những người có năng lực siêu nhiên - lại chưa hề được giải quyết. Mọi thứ mới chỉ dừng lại ở chỗ Hội Edison bị phá huỷ. Cha Simon thực ra là ai, ông đã làm những gì trong quá khứ, ông có vai trò như thế nào; hay mối quan hệ giữa ông và mẹ Tori ra sao; thậm chí là việc mẹ Chloe từ đâu mà có được sợi dây chuyền cho cô bé, tất cả đều chưa được giải đáp. Cái kết lửng lơ này khiến mình thực sự hụt hẫng.

Việc xây dựng nhân vật cũng có vấn đề. Tính cách của Chloe ngày càng nhạt nhoà. Mình không còn thấy bóng dáng cô bé đam mê điện ảnh và có phần liều lĩnh ở tập 1 nữa. Chloe ở tập này thậm chí còn không biết rõ tình cảm của bản thân, để rồi lấp lửng với Simon và Derek. Ngoài Chloe ra, ba nhân vật còn lại đều có cá tính riêng, và đó lại chính là sai lầm vì điều đó làm cho nhân vật chính trở nên mờ nhạt. Mình rất thích Derek, tác giả xây dựng anh đúng kiểu một người sói mạnh mẽ, nóng nảy, ít nói nhưng đầy tình cảm. Có lẽ, Derek chính là nguyên nhân khiến mình yêu thích series Sức mạnh hắc ám này.

Với rất nhiều vấn đề chưa được giải quyết kể trên, có lẽ series này nên có thêm một tập nữa thì tốt hơn. Dù sao, mình cũng rất muốn biết liệu số phận những con người ấy sẽ đi về đâu, và họ sẽ sử dụng năng lực siêu nhiên của mình như thế nào cho có ích, chứ không phải cứ phá tan cái Hội Edison kia là tất cả đều xong xuôi.
3
24661
2015-07-09 18:39:29
--------------------------
224417
6738
Ngay từ khi đọc tập 1 tôi đã bị cuốn hút vào series Sức Mạnh Hắc Ám này, nhưng dường như càng về các tập sau phong độ của tác giả càng giảm, dù sao đi nữa thì nhìn chung đây vẫn là 1 bộ truyện hay và hấp dẫn, chỉ là còn thiếu 1 chút gì đó gọi là "đỉnh điểm". Chính vì tác giả đặt ra quá nhiều bí ẩn ở 2 tập đầu mà đến tập cuối này vẫn chưa giải thích thoả đáng nên mới khiến độc gỉa không hài lòng. Sợi dây chuyền Chloe đeo để giúp cô làm giảm sức mạnh của tà ma ấy, tại sao mẹ Chloe có được sợi dây đó để mà trao cho con gái mình, chẳng phải người mẹ chỉ là người bình thường thôi sao? Nếu tác giả nói nhiều hơn về quá khứ của người mẹ này thì tốt rồi... Trong tác phẩm lại đề cập tới rất nhiều thế lực, nào là hội của những người sói, nào là hội Edison, nào là nhóm những người tách ra từ Edison, nhưng cuối cùng thông tin về các hội ấy cũng chỉ có chút ít khiến độc giả tò mò hơn. Điều làm tôi bất ngờ nhất có lẽ là việc Tori và Simon là anh em cùng cha khác mẹ. Nhưng nhắc tới đó lại khiến tôi thắc mắc hơn về cha của Simon và Derek, ông ấy rốt cuộc đã làm những việc gì trong suốt ngần ấy năm ẩn giấu ? Cái chết của các nhân vật phụ cũng khiến tôi hơi hoang mang vì chưa hiểu rõ đầu đuôi, dường như họ chết lẹ quá thì phải ???? Tori là nhân vật mà tôi cảm thương nhất, cô ấy phải chứng kiến mẹ ruột mình - cũng là kẻ thù của mình chết ngay trước mắt, người bố mà trước đây cô tưởng yêu thương mình hoá ra không phải bố ruột và ông cũng không phải quá quan tâm gì tới cô, người mà trước kia cô từng có tình cảm hoá ra là anh trai mình, cô bạn thân Liz thì đã bị giết hại từ lâu... Và tôi rất ưng ý vì tác giả đã để Chloe và Derek đến với nhau vui vẻ và hạnh phúc, nụ hôn cuối truyện của họ thật mãnh liệt và ngọt ngào ! Còn về phần dịch, phải nói đây là dịch giả tệ nhất tôi từng biết, cách xưng hô loạn xì ngầu cả lên, gặp ai cũng dịch là "nó", thành ra tôi đọc mà nhiều khi không hiểu nhân vật nào đang được nói tới, hơn nữa cách xưng hô như vậy cũng rất thô lỗ, nhiều chỗ dịch còn lủng củng, lỗi đánh máy lỗi chính tả thì quá nhiều, mực in đôi chỗ bị nhoè. Điểm cộng duy nhất là bìa sách của cả bộ đều rất đẹp và huyền ảo !
4
75959
2015-07-08 17:07:00
--------------------------
157616
6738
Mình nghĩ phần này tác giả sẽ giải đáp hết mọi thắc mắc, nhưng mình cảm thấy tác giả chỉ muốn cho xong cuồn truyện này.

Nhiều tình tiết chưa hợp lí. Thứ nhất là về sợi dây chuyền của Chloe,bà Margaret giải thích được một ít rồi lại thôi, vì sao sợi dây chuyền của Chloe chuyển màu tới cuối truyện vẫn không được giải thích. 

Thứ hai, là mẹ của Chloe, lúc ở nhà của Mr.Bank, Chloe liên lạc được với một người mà cô nghĩ là mẹ của mình, tất cả chỉ dừng lại ở đó, chỉ là lời cảnh báo của 1 người có thể là mẹ cô, tác giả cũng không xoáy sâu vào vấn đề này khiến mình thắc mắc mẹ Chloe là người như thế nào.

Thứ ba, là sức mạnh của Chloe, Dereck, Simon và Tori. Mính mong chờ 1 kết thúc hoành tráng như Chloe sẽ gọi 1 đội quân âm binh và điều khiển còn Tori và Simon sẽ trổ tài pháp thuật của mình, nhưng thực sự thì mọi thứ không như mình mong đợi, không có sự bùng nỏ, vẫn chỉ là những gì mình thấy ở phần 1 và phần 2. Chỉ có sự biến hình của nhân vật Dereck làm mình hài lòng.

Mình nghe nói truyện này chỉ có 3 phần thôi. Tuy kết thúc không làm hài lòng nhưng nói chung vẫn được, lối kể chuyện vẫn cuốn hút người đọc. Đánh giá mình cho 3 sao.
3
516346
2015-02-09 17:13:54
--------------------------
128654
6738
Phải nói đây là cuốn sách nhiều lỗi chính tả nhất mà mình từng gặp. Nhiều chỗ thì thiếu từ, làm câu văn trở nên tối nghĩa, nhiều chỗ lại lặp 2 từ liên tiếp. Nhưng lỗi sai đáng nói nhất - mà mình không thể không nghĩ là do người biên tập quá cẩu thả, là rất nhiều chỗ cho nhân vật nói về mình thì thay vì chữ "mình" lại đồng loạt bị thế chỗ bằng chữ "tôi" (cứ như là lúc biên tập dùng lệnh Find and Replace và cho Replace toàn bộ "mình" thành "tôi" vậy), khi đọc lên nghe rất chướng, và lỗi này cứ tái lặp liên tục trong suốt diễn biến cuốn sách.
Mình vẫn cho 3 sao vì cái hay của truyện là không thể phủ nhận.
Về phần nội dung, phải thừa nhận rằng diễn biến truyện đã làm mình thót tim không biết bao nhiêu lần, cả trong 2 tập trước và cho đến tập này. Nếu không gặp những lỗi chính tả tái lặp như vậy thì hẳn việc đọc truyện còn hứng thú hơn nhiều
3
30603
2014-10-03 21:44:37
--------------------------
123395
6738
Mình khá mong chờ vào tập cuối này nhưng mình hơi bị thất vọng. Tập 1 và 2 có nói đến chiếc vòng cổ nhưng chưa giải thích kĩ làm mình rất tò mò không biết nó là cái gì nhưng đến tập 3 chỉ giải thích khá mơ hồ về chiếc vòng, vả lại mình củng rất tò mò về mẹ Chloe nhưng khi mình đọc hết quyển sách thì Chole hầu như không thắc mắc gì nhiều ngoại trừ lúc gọi hồn ông Dr. Đó là về cốt truyện 
Còn phần dịch thì mình khá bực mình khi Chloe gọi Tori là " nó " nghe cứ như là Chloe không ưa Tori vậy. Lúc Chole gọi ông Andrew củng vậy đáng lý ra dù có bị phản bội hay sao đi nữa củng đừng xưng là 'ông ta " 
 Cách đánh chữ củng vậy 1/3 trang mình thấy chữ hơi đậm khoảng cách giữa 2 dòng khá hẹp dẫn đến khó đọc nhưng về sau chữ nhạt bớt và khoảng cách được nới rộng.
3
88562
2014-09-01 09:31:13
--------------------------
123343
6738
Phải công nhận là đọc phần 3 mình cảm nhận khác hẳn 2 phần trước. Cốt truyện rất li kì với những tình tiết không thể nào đoán định trước được. Cách diễn đạt cũng tự nhiên và lôi cuốn hơn cộng thêm tác giả miêu tả tâm lí của một cô gái mới yêu lần đầu rất thực làm cho tập truyện trở nên cuốn hút hơn. có một chi tiết nhỏ mình chưa ưng ý là các chi tiết liên kết các phần với nhau có phần thiếu thuyết phục. Dù sao thì mình rất mong đón đọc tập 4 của bộ truyện này.
4
284845
2014-08-31 21:14:00
--------------------------
119177
6738
Giống như những bạn bên dưới nhận xét, mình hoàn toàn đồng ý với điều đó. Nếu như tác giả có thể cho ra phần tiếp theo để giải thích toàn bộ bí ẩn có vẻ hay hơn. Mình vẫn chưa hiểu được diễn cảnh sẽ khủng khiếp ra sao nếu như Chloe nhất quyết triệu hồi linh hồn của mẹ cô (lý do cá nhân chẳng hạn). Còn kẻ mạnh mà Chloe gặp được trên đường thoát khỏi hội Edison thật sự là ai? Dù là kết thúc mở nhưng tác giả cũng nên để người đọc hiểu rõ những phần "quan trọng cần lý giải" trước khi đóng câu truyện chớ... TT^TT
4
50318
2014-08-02 21:58:53
--------------------------
118450
6738
Đã rất háo hức ngóng chờ tập 3 - phần cuối của series Sức mạnh hắc ám nhưng có lẽ nó chưa thể làm thỏa mãn bạn đọc cũng như ngang tầm với 2 tập trước. Thứ nhất là nội dung cũng như cách lý giải chưa hợp lý, để lại rất nhiều thắc mắc cho người đọc khi theo dõi ( điều này có lẽ đa số bạn đọc đã nhận ra khi đọc xong). Thứ hai, chất lượng giấy và mực in thực sự là kém xa so với 2 quyển trước. Mình rất quý sách nên hay để ý tiểu tiết này. Từ cách trình bày, đánh chữ đều có vẻ qua loa không được đầu tư. Mực đa phần bị nhòe gây nhức mắt khi đọc. Thứ ba, lỗi dịch thuật. Vẫn là dịch giả của tập 2 nhưng lỗi dịch thuật, nhất là khi dùng đại từ nhân xưng và tính từ sở hữu còn sai gây cảm giác bực mình khi đọc.
Tóm lại, có lẽ vì đã trót mua 2 quyển trước nên vẫn phải mua tập 3. Nhưng thực sự là không hài lòng
3
116057
2014-07-28 23:58:35
--------------------------
118072
6738
Đây quả thực là bộ truyện rất hay.Đầu tiên là nội dung trước.
Nói sao nhỉ? Hay. Lôi cuốn. Tuy nhiên, đọc xong mình vẫn cảm thấy có một số điều vẫn chưa được giải đáp và một số nhân vật chưa có cái kết. Thứ nhất, về phẩn 'ả bán yêu' Diriel có thỏa thuận giúp Chloe trốn thoát, thế mà Chloe phóng thích xong 'ả' đi luôn, do là Chloe mạnh tới mức có thể gọi hồn 'ả' lúc nào cũng được!? Thứ hai, mối quan hệ giữa Jacinda (mẹ Rae) với tiến sĩ Davidoff ở phần 2-Thức Tỉnh vẫn chưa được lý giải. Còn một số điều nhỏ nữa mình không nhớ. À, còn cái ông bảo vệ đã chết ở trường Chloe học lúc trước đâu rồi ý nhỉ?
Bìa sách đẹp, hầu như không có lỗi chính tả đáng kể. Khác với 2 phần trước là lần này, trong một số lời nói của nhân vật có chèn thêm điệu bộ, biểu cảm vào dấu ngoặc đơn
5
227848
2014-07-25 23:14:07
--------------------------
117020
6738
Đây quả thực là một bộ truyện rất hay, nếu cuốn thứ ba này được viết lại... Mình không rõ đây có phải cuốn cuối của bộ truyện này hay không nhưng nếu đây đúng là kết thúc của bộ truyện thì thành thật mà nói, đây là cái kết quá thất vọng. Ngay cả khi bộ truyện vẫn tiếp diễn, thí cuốn thứ ba này quả là một hạt sạn lớn làm cho người đọc chưng hửng. Hai cuốn đầu của bộ truyện này phải nói là cực kì hay. Mình đã kì vọng rất nhiều khi được nghe lới giới thiệu rắng "Phần ba là phần hấp dẫn nhất của cả bộ truyện." Nhưng quả thật mình khá thất vọng sau khi đọc. 
Đáng lý đây phải là một phần có nhiều tình tiết quan trọng. Ví dụ như bật mí những bí mật của hội Edison, hay cho ta thấy những khả năng thật sự của Chloe (Bởi những phần trước ta vẫn chưa hiểu được khả năng mạnh nhất mà năng lực gọi hồn của Chloe có thể đạt được.)  Về mẹ của Chloe. Hay cái hồn ma bán yêu trong trung tâm Edison Chloe đã gặp là ai thật sự... Nhựng nhân vật trên đều được nhắc qua, nhưng lại miêu tả khá qua loa và đôi chỗ hơi rối rắm. Ngoài ra, mình cũng không thích cách tác giả sắp xếp cho tình cảm của Chloe và Derek phát triển. Mình xin lỗi khi không biết miêu tả ra sao nhưng nói nôm na thì cặp đôi này giống như có "History" nhưng lại không có "Chemistry" với nhau. Bên cạnh đó, việc bỏ ngang nhiều nhân vật xuất hiện ở phần trước cũng để lại nhiều thắc mắc. 
Một điều nữa mình không thích trong phần này là cách giải quyết ác "nhân vật phản diện". Đành rắng nhân vật phản diện thì kiểu gì cuối cùng cũng chết, nhưng sao mình có cảm giác cách tác giả "giết" các nhân vật đó cứ như "khôn biết phải giải quyết sao nên cho chết đại cho lẹ"...
Đó là những ý kiến riêng của mình. Xét cho cùng, mình vẫn mong đợi truyện sẽ ra đến phần sau, nếu thật sự có phần sau. Cái kết lưng chừng chưa xong trước mà cũng chả xong sau của tập này làm cho người đọc, hoặc ít nhất là mình cảm thậ khó mà hài lòng. Thánh thật mà nói, mình tự hỏi không biết có phải phần này, tác giả đưa đại cho một ai đó khác viết không...? Xét riêng thì đây vẫn là một cuốn sách hay. Nhưng khi đặt chung bộ với hai cuốn đầu của bộ truyện, có lẽ mình sẽ nói, đây chính là cuốn dở nhất trong cả bộ...
3
148922
2014-07-14 16:59:28
--------------------------
116750
6738
Đầu tiên là nội dung trước. 
Nói sao nhỉ? Hay. Lôi cuốn. Tuy nhiên, đọc xong mình vẫn cảm thấy có một số điều vẫn chưa được giải đáp và một số nhân vật chưa có cái kết. Thứ nhất, về phẩn 'ả bán yêu' Diriel có thỏa thuận giúp Chloe trốn thoát, thế mà Chloe phóng thích xong 'ả' đi luôn, do là Chloe mạnh tới mức có thể gọi hồn 'ả' lúc nào cũng được!? Thứ hai, mối quan hệ giữa Jacinda (mẹ Rae) với tiến sĩ Davidoff ở phần 2-Thức Tỉnh vẫn chưa được lý giải. Còn một số điều nhỏ nữa mình không nhớ. À, còn cái ông bảo vệ đã chết ở trường Chloe học lúc trước đâu rồi ý nhỉ?
Bìa sách đẹp, hầu như không có lỗi chính tả đáng kể. Khác với 2 phần trước là lần này, trong một số lời nói của nhân vật có chèn thêm điệu bộ, biểu cảm vào dấu ngoặc đơn :) :) :)
4
65000
2014-07-11 17:50:59
--------------------------
116049
6738
Tiếp tục chuyến phiêu lưu - đúng hơn là sự trốn chạy khỏi nhà mở Lyle lần trước, "Đền tội" là quyển sách cuối cùng, đưa câu chuyện đi đến hồi kết với những lời giải đáp, những sự thật được hé lộ sau tấm màn bí mật đã bao phủ xuống cuộc đời của bộ ba Chloe, Simon và Derek. Chloe cuối cùng cũng sẽ phát hiện ra sự thật về thân thế của mình, tại sao cô có khả năng trời phú như vậy. Nhưng quan trọng hơn, cô sẽ phải quyết định rằng tình cảm của mình sẽ đặt về phía ai: Simon - một cậu chàng bảnh bao hay Derek - một người dị thuồng với dáng vẻ thô kệch nhưng ẩn chứa những sức mạnh tiềm ẩn ghê gớm? Và chính Simon và Derek cũng sẽ phát hiện ra sự thật kinh hoàng về bản thân, về nguồn gốc của họ, về tung tích của cha họ và lí do tại sao họ được mang đến nhà mở Lyle. 
Nhà mở ấy được lập ra với mục đích gì? Rae còn sống hay đã chết? Chuyện gì thật sự đã diễn ra dưới ngôi nhà ấy? Và ai sẽ phải đối mặt với những tội lỗi mà họ gây ra? Tất cả đều sẽ được giải đáp trong phần cuối cùng của series truyện "Sức Mạnh Hắc Ám" - "Đền tội".
5
6495
2014-07-04 17:36:16
--------------------------
