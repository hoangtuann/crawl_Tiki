462492
4220
Quyển sách này có lối viết khá dễ hiểu và dễ đọc. Nhưng các thông tin trong quyển
sách sẽ có ích với những độc giả mới bước đầu bước chân vào vấn đề dinh dưỡng. Còn đối với những người có hiểu biết căn bản thì quyển sách này có vẻ hơi "hời hợt" quá. Có những chủ đề nêu ra rất hay và thú vị nhưng lại quá ngắn, chưa kịp hiểu thêm nội dung thì đã sang mục khác. Kiến thức dinh dưỡng luôn thay đổi hằng ngày nên đôi lúc những thông tin trong sách sẽ không còn đúng nữa.
4
1128470
2016-06-28 17:00:28
--------------------------
424560
4220
Đây là quyển thứ 2 trong bộ này mà mình mua. 
Quyển này giới thiệu về dầu cá, hoa quả chín ép, thực phẩm nhập khẩu, dầu ô liu, lạc, các loại trà, gan ngỗng, sữa dê, đậu hũ lụa, đậu hũ thối... Sách giúp mình nhìn nhận vấn đề một cách khách quan hơn, để ta có thể biết thực phẩm mà ta lựa chọn sử dụng thế nào là tốt, thế nào không.
Nhược điểm: Giấy có màu và mùi của sách cũ, nội dung ăn dầu cá hay ăn cá tốt hơn copy y nguyên trong tập 1 của bộ này.
2
769150
2016-05-02 08:33:15
--------------------------
355841
4220
1. Tên sách: Tên hay và phù hợp với nội dung của sách.
Đánh giá: 5 đ
2. Thiết kế: Bìa sách đẹp, màu hơi tối.
Đánh giá: 4 đ
3. In ấn: Mình đọc bản ebook nên không đánh giá tiêu chí này.
4. Nội dung: Sách cung cấp những kiến thức bổ ích về ẩm thực về dầu cá, hoa quả chín ép, thực phẩm nhập khẩu, dầu oliu… Sách có nội dung bổ ích nhưng khá nhàm chán vì có nhiều từ chuyên môn và không nói được sự thú vị của ẩm thực.
Đánh giá: 4 đ
5. Nhân vật:
Không đánh giá.
Tổng điểm: 13/3 = 4,3 đ

4
934783
2015-12-20 20:49:52
--------------------------
