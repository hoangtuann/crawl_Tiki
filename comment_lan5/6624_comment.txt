492518
6624
Cuốn sách này mình không mua trên Tiki, mua ở hiệu sách với giá rất đắt, và mình hoàn toàn hài lòng với đồng tiền đã bỏ ra cho cuốn sách.
Đọc xong mình cảm thấy mình có thể giỏi hơn cả  bác sĩ chuyên khoa! 
Cuốn sách nói về cái nhìn khác của bệnh ung thư, cái nhìn thông minh, mà mình cũng đã từng tiếp cận ở rất nhiều phương tiện khác những thông tin về bệnh ung thư.
Đôi khi mình nghĩ nếu ai cũng có cuốn sách này thì bệnh ung thư sẽ ko còn là nỗi lo của bất cứ ai nữa.
5
747025
2016-11-24 23:28:10
--------------------------
492518
6624
Cuốn sách này mình không mua trên Tiki, mua ở hiệu sách với giá rất đắt, và mình hoàn toàn hài lòng với đồng tiền đã bỏ ra cho cuốn sách.
Đọc xong mình cảm thấy mình có thể giỏi hơn cả  bác sĩ chuyên khoa! 
Cuốn sách nói về cái nhìn khác của bệnh ung thư, cái nhìn thông minh, mà mình cũng đã từng tiếp cận ở rất nhiều phương tiện khác những thông tin về bệnh ung thư.
Đôi khi mình nghĩ nếu ai cũng có cuốn sách này thì bệnh ung thư sẽ ko còn là nỗi lo của bất cứ ai nữa.
5
747025
2016-11-24 23:28:10
--------------------------
288404
6624
Tuy mình vẫn chưa đọc hết cuốn sách này, nhưng mà mình vẫn muốn chia sẻ suy nghĩ của mình về nó. 
Đây không phải là cuốn sách viết về những người bị mắc bệnh ung thư cùng những câu chuyện đầy nghị lực của họ, cùng với những thông điệp như “Hãy cố gắng lên” chẳng hạn. Nếu bạn muốn tìm hiểu riêng biệt về từng loại bệnh ung thư thì cuốn sách này cũng không đáp ứng điều đó. Nhưng, cuốn sách cung cấp những hiểu biết cơ bản về ung thư, cùng rất nhiều những phương pháp trị liệu ung thư mà có thể chúng ta chưa từng biết hay đã biết, cũng như tác dụng hay tác hại của từng phương pháp. Nếu bạn thắc mắc tại sao những phương pháp tốt như vậy trong cuốn sách này đề cập lại không được phổ biến rộng rãi, cuốn sách này cũng sẽ giải đáp cho bạn. Bên cạnh việc chữa trị thì cuốn sách cũng cung cấp một số chế độ ăn uống hỗ trợ việc điều trị, các xét nghiệm cần thiết để phát hiện bệnh và phương pháp phòng ngừa. Nếu như người đọc cần biết thêm thông tin về những nội dung mà trong sách nói đến, có thể tra cứu trực tuyến những địa chỉ mà trong trong sách ghi.
Vì đây là sách chuyển ngữ nên có thể nhiều người sẽ thắc mắc không biết tìm mua thuốc ở đâu trong nước, người dịch cũng biết thế nên đã cung cấp thông tin ở cuối sách rồi, mặc dù là không nhiều đâu, nhưng mình nghĩ sẽ hữu ích cho những ai cần nó.
5
255445
2015-09-03 15:51:23
--------------------------
180410
6624
Mình nghĩ ai đều mang trong mình những nỗi sợ như là có người sợ máu, có người sợ đau, người sợ khổ, sợ cực,... Nhưng chung quy lại thì chúng ta đều có cùng một cái sợ lớn nhất là sợ chết, nhất là những người đang mắc những căn bệnh hiểm nghèo thì cái chết ám ảnh họ hơn bất cứ thứ gì hết. Họ sợ chết vì sợ sẽ không được nhìn cái thế giới tươi đẹp này nữa hay là không còn nhìn thấy người thân, bạn bè của họ nữa. Và các bệnh nhân ung thư là không ngoại lệ. Họ sẽ rất tuyệt vọng khi biết mình mắc bệnh. Nhưng nếu như ai có 1 lần đọc qua câu chuyện Thoát khỏi ung thư thì sẽ hoàn toàn khác. Bởi truyện như tiếp thêm nghị lực sống cho họ, như một liều thuốc tinh thần. Ý nghĩa nhân văn của cuốn truyện này rất lớn, nó giúp mình nhìn rõ được những gì xung quanh đều tươi đẹp cùng niềm tin vào cuộc sống.
5
433071
2015-04-09 12:53:42
--------------------------
