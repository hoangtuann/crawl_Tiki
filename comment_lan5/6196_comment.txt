408220
6196
Nhìn cái bìa thôi là cũng thấy thu hút rồi, vừa đẹp vừa lãng mạn. Thật sự khi đọc hết thì cảm thấy rất tiếc vì muốn truyện dài dài dài và dài hơn nữa để đọc thỏa thích. Tác giả xây dựng tình huống truyện rất khéo léo và hấp dẫn, lôi cuốn người đọc từ những trang giấy đầu tiên. Hồi hộp, lãng mạn, cảm động nhưng cũng không kém chi tiết hài hước, tất cả tạo nên một cuốn tiểu thuyết hay tuyệt vời. Nhân vật mình ấn tượng nhất có lẽ là Royce Westmoreland - rất ấn tượng. Đây là tiểu thuyết lãng mạn đầu tiên mình đọc và mình chắc chắn là nếu như bạn có cuốn tiểu thuyết này trong tay, nó sẽ không làm bạn thất vọng đâu.
4
1165894
2016-03-31 12:37:23
--------------------------
396317
6196
Vương quốc của những giấc mơ là một trong những tác phẩm rất thành công của tác giả Judith McNaught cùng với Whitney, My love đã đưa tên tuổi của bà vụt trở thành ngôi sao sáng của dòng tiểu thuyết văn học lãng mạn. Vương quốc của những giấc mơ là câu chuyện tình yêu lãng mạn nhưng đan xen trong đó cũng những thử thách, yêu và hận. Jennifer Merrick là con gái của một lãnh chúa Scotland, nhưng vì tính tình quá mức bốc đồng, vô tâm thậm chí là liều mạng của mình mà cha cô phải gửi cô vào tu viện để kiềm chế cô lại. Royce Westmoreland người được mệnh danh là Sói đen, vị tướng tài ba của quân Anh, đang trên đường tiến chiếm lãnh địa của lãnh chúa Merrick. Tệ hơn, anh lại là kẻ thù mà cô rất căm hận. Hai con người, hai thế giới song song có thể hòa làm một?
5
632796
2016-03-13 11:11:00
--------------------------
392277
6196
Đây là câu chuyện hay nhất trong bộ ba series này. Cuốn đầu tiên tôi đọc là Whitney yêu dấu, có lẽ do cuốn Vương Quốc Của Những Giấc Mơ viết sau nên độ chín của tác giả cũng tới hơn và đằm hơn. Tác giả cũng đã biết cách tiết chế và xử lý chi tiết và diễn biến truyện hợp lý. Không bị thừa hay tham tình tiết như cuốn đầu tiên. Điều đó, khiến cho tôi cảm thấy rất thích thú vì câu chuyện đi theo hướng hợp lý và vừa phải, không bị cố tình tạo thêm tình tiết để kéo dài câu chuyện. Nhìn chung, đây là một cuốn tiểu thuyết hay, thích nhất đoạn nữ chính quỳ phục dưới nam chính. Tôi đọc mà cảm xúc dâng trào.
4
454536
2016-03-06 17:16:53
--------------------------
391638
6196
Đây là một cuốn sách rất đáng giá để đọc và nên có trong tủ sách của bạn.Câu chuyện xoay quanh tình yêu ngang trái giữ Jennifer-nàng con gái của lãnh chúa nhà Merrick trên đất nước Scotland và Sói-kẻ được mệnh danh là kẻ hắc ám,giết người không gớm tay của nước Anh.Nhưng giữa họ đã xảy ra tình cảm trong khi Jennifer bị em trai của Sói là Stephan bắt cóc về trại.
Bằng giọng văn điêu luyện của Judith Mcnaught,câu chuyện đã trở nên sống động hơn,những sự vật hay tình huống trong truyện được bà khắc hoạ và giải quyết một cách vô cùng khéo léo.Và đặc biệt là tình huống dẫn đến cái kết thúc vô cùng hấp dẫn và có hậu=)))))
5
846507
2016-03-05 17:48:03
--------------------------
388772
6196
Cuốn sách này khiến mình tin không gì có thể đánh bại tình yêu, dù đối phương có là kẻ thù.
Jenny, cô gái mạnh mẽ, kiên cường, nhưng bên trong lại là một trái tim bé bỏng, yếu ớt, khao khát được yêu thương.
Tình huống truyện được xây dựng khá hợp lý, logic, nhiều lúc đọc mình phải bật cười vì sự sến súa khó tả của Royce.
Theo mình thấy thì motip truyện cũng giống truyện Jamie cô dâu của anh, nên mình đọc xong cuốn đó, đọc cuốn này thì thấy hơi nhạt một chút, và, điểm chung là có nhiều cảnh...
Mình thích bìa của cuốn truyện này, có gì đó mộng mơ, kì ảo, rất đẹp.
5
602517
2016-02-29 17:30:51
--------------------------
379244
6196
Motip quen thuộc khi trận chiến của người Scot và người Anh kéo dài hàng thập kỉ chấp dứt nhờ 1 cuộc hôn nhân hòa giải. Tuy nhiên cuộc hôn nhân này diễn ra trong không khí thù địch, muốn ăn tươi nuốt sống nhau của 2 bên gja đình. Cô đã yêu chàng nhưng vì tình cảm với gia tộc phải gạt bỏ trái tim đi theo người cha nhãn tâm. Chàng - sói đen, đã làm tất cả để bảo vệ nàng, nhưng đã bị nàng lừa hết lần này đến lần khác chỉ vì niềm tin mù quáng dành cho cha và gia tộc. Cao trào là khi chàng chấp nhận cái chết thì cô mới hiểu ra và đến bên chàng. 1 câu chuyện lấy đi quá nhiều nước mắt của người đọc và quá hành hạ Sói Đen. Nhưng 1 cái kết viên mãn sẽ khiến các bạn hài lòng.
5
138737
2016-02-10 10:28:18
--------------------------
311197
6196
Truyện viết về tình yêu lãng mạn giữa tiểu thư Jennifer và chiến binh dũng cảm Royce biệt danh là Sói Đen. Tuy là tù binh của Royce nhưng Jennifer không khuất phục. Nàng luôn tìm đủ mọi cách để trốn thoát và gây nhiều khó khăn cho quân lính của Royce. Dù rất tức giận  nhưng Royce cũng không thể ngừng được tình cảm của mình. Mình thích sự dũng cảm và khá mưu trí của Jennifer tuy có lúc nàng cũng hơi ngây thơ. Tuy nhiên mình không thích phần cuối khi Jennifer vì tin cha mình đã ra mặt ủng hộ đội nhà trong cuộc thi đấu giữa các đội quân. Điều đó đã làm tổn thương Royce và những người của anh. Truyện khá là lôi cuốn và thú vị.
4
43129
2015-09-19 22:04:20
--------------------------
288292
6196
Mình không am hiểu nhiều về văn chương, chỉ là sở thích được lật mở từng trang sách, được bay bổng theo từng trang sách được hồi hộp chờ đợi cái kết. VQCNGM giúp mình có niềm tin nhiều hơn về một tình yêu chân thành và một tương lai thật sáng cho những ai dám yêu, dám hận và dám chiến đấu cho tình yêu của mình. Mình thích cá tính của Jenny, không nhất thiết con gái phải dịu dàng, thùy mị, e ấp, yếu ớt thì mới được một người đàn ông mạnh mẽ che chở. Những cô gái cá tính có những nét hay của cô ấy, nhưng hơn hết họ cũng cần được một người đàn ông che chở, và Royce xuất hiện như một giấc mơ hoàn hảo của Jenny.
Cảm ơn Judith Mcnaught và Tiki đã mang đến cho tôi những câu chuyện thật hay!
5
413361
2015-09-03 15:00:08
--------------------------
261287
6196
mới đầu mình rất ấn tượng với bìa sách rất đẹp, chất liệu giấy cũng khá ổn, khi bắt đầu đọc truyện không như những cốt truyện khác chưa đọc đã có thể đoán được kết quả, nhưng câu  truyện này hấp dẫn, lôi cuốn người đọc ngay từ lúc đầu và  chỉ muốn đọc một mạch cho đến hết, dù đọc lại nhiều làm nhưng mình vẫn muốn đoc thêm lần nữa. Truyện cho ta thấy một  sự quyết tâm tột cùng vượt qua mọi khó khăn, gian khổ để đạt được mục đích của các tuyến nhân vật . Để rồi đích đến cuối cùng là một tình yêu trong sáng không mưu toan danh lợi
5
634351
2015-08-10 23:40:55
--------------------------
260648
6196
Chẳng là chờ đợi mỏi mòn mới rước được chàng Sói của " Vương quốc của những giấc mơ " trên mạng online về nhà ôm ấp. Nhưng phải lần 2 Tiki gửi lại mới thực sự ôm được, lần 1 sách hỏng. Chàng Sói mình đã đọc muốn mòn mắt trên mạng, đọc đến mức ngày ra sách là canh chừng tiki có là đặt ngay về không do dự. Mà bìa đẹp, tòa lâu đài đẹp, màu sắc đẹp, in ấn đẹp, cái gì cũng đẹp hết. Có sách ôm luôn đi ngủ cùng với 1 quyển Until you được Alphabook phát hành và mơ khi nào được ôm em " Whitney, my love ". Nên chờ thôi. chờ sẽ có trọn series luôn. Còn câu chuyện, có cần phải nói không nếu không hay thì không thể ôm ấp được. Một câu chuyện long lanh như cổ tích cho các cô gái mơ mộng. Cám ơn Thái Uyên. 
5
131519
2015-08-10 16:06:11
--------------------------
253290
6196
Theo cá nhân mình đây là một cuốn sách có phong cách rất lạ theo kiểu phương Tây nhưng chính nó làm lôi cuốn người đọc ngay từ những trang đầu tiên. Bạn mạnh mẽ như Royce, dũng cảm gan lì như Jennifer không còn quan trọng, hai con người hai số phận gặp nhau và tất cả chỉ vỏn vẹn trong hai chữ tình yêu. Họ từ định kiến đến tha thứ,ghét đến thương, từ chia ly đến hội ngộ như chỉ làm sâu sắc thêm tình yêu vĩ đại ấy. Một vương quốc của những giấc mơ cho những trái tim đã, đang và sẽ yêu cho những ai còn ngập ngừng vì sự khác biệt trong mỗi con người, những ai còn những giằng xé cố chấp trong suy nghĩ về tình yêu. Hãy đọc và cảm nhận để sẵn sàng yêu và được yêu

P/s Điểm cộng cho sách nữa là bìa rất hút người xem và chất lượng giấy khá tốt

5
137771
2015-08-04 12:32:27
--------------------------
239650
6196
Đối với một người hâm mộ tiểu thuyết phương Tây thì đây là một tác phẩm không thể bỏ qua. Sách có bìa rất đẹp, gây ấn tượng nên mình quyết định mua. Juditch đã xây hình tượng nhân vật nữ rất mạnh mẽ và kiên cường khiến mình rất thích nhân vật nữ chính. Còn độ nhạy cảm thì đa phần tiểu thuyết của Juditch cuốn nào cũng có nên vấn đề này miễn bàn rồi. Cảm ơn Tiki đã đem đến cho mình một cuốn tiểu thuyết rất lãng mạng. Tiki giao hàng rất nhanh chóng và goí hàng cẩn thận. 
4
184169
2015-07-24 00:34:32
--------------------------
233224
6196
Mua sách vì thấy bìa rất đẹp và tựa rất hay. Đọc xong rồi nhưng vẫn luyến tiếc phải đọc lại vài lần nữa.
Điểm cộng: cuốn sách này khác biệt ở chỗ nam chính không phải là 1 người được ngưỡng mộ, được coi trọng như bình thường. Mà đây là 1 tên cướp bị mọi người ghê sợ, căm ghét. Ngay lần gặp nhau của nam nữ chính đã bất thường rồi: 1 vụ bắt cóc nên sau đó quá trình tìm hiểu nhau, chinh phục nhau của họ lãng mạn hơn bình thường, gay cấn hơn bình thường, cũng khó khăn hơn bình thường. Cách viết của tác giả cũng rất hay, lời văn đẹp, trau chuốt, tình tiết càng về cuối càng nhanh và kịch tính. Chính nhờ tác phẩm này mà mình mới biết và yêu thích Judith Mcnaught, cũng như tìm đọc thêm những tác phẩm khác của bà.
Điểm trừ: mình đọc rất nhiều tiểu thuyết tình cảm phương Tây rồi nhất là bối cảnh xưa cũ thời bá tước, công tước thế này. Nữ chính bị rập khuôn. Cũng vẫn là 1 cô gái xinh đẹp, thông minh, hiểu biết, nhiều ước mơ nhưng bị coi như bình hoa di động, bị tư tưởng xưa cũ trói giữ, kìm kẹp. Và dĩ nhiên là cô yêu người thấy được những khác biệt nơi cô. Thêm 1 điểm trừ hơi "nhạy cảm" nữa là các cảnh "nóng" tác giả tả chi tiết quá. Đọc một mình không mà còn thấy ngượng đỏ mặt nữa. Nên toàn phải "lướt lướt" qua những chỗ đó.
5
22444
2015-07-19 15:11:00
--------------------------
232153
6196
Ban đầu đọc sách sẽ cảm thấy nó có vẻ dài dòng vì mình chưa quen cách viết văn của tác giả vì trước giờ toàn đọc truyện ngôn tình. Nhưng càng đọc càng thấy cách viết chân thực đầy đam mê và khát vọng của tác giả thật sự thu hút mình. Vương quốc của những giấc mơ như một món quà của tác giả cho mọi cô gái. Con gái, ai cũng có những giấc mơ hạnh phúc, những khát vọng yêu thương nhưng vẫn còn nhiều người vẫn chưa dám đấu tranh giành lấy những yêu thương đấy. Judith Mcnaught đã để nhân vật của mình làm hộ điều đó, đã đấu tranh, đã kiên cường, đã mạnh mẽ. Từ đó truyền cảm hứng cho mọi cô gái, mọi đọc giả của mình.
4
379131
2015-07-18 17:04:35
--------------------------
217731
6196
Phải rất lâu rồi mới có truyện của Judith Mcnaught xuất bản tại Việt Nam, truyện của bà thật sự là hàng hiếm. Khi “Vương quốc những giấc mơ” xuất hiện, tôi đã rất ấn tượng bởi cái bìa tuyệt đẹp và cũng rất mộng mơ, còn nội dung thì không lẫn đi đâu được trong kiểu viết của Judith, giằng xé giữa trái tim và lý trí, thêm nữa là hận thù cũng lên tới đỉnh điểm và xót xa không kém. Đây có lẽ là truyện về Scotland đầu tiên mà tôi đọc của bà và tôi thấy nó cũng không thua kém gì so với những thể loại khác. Cuộc gặp gỡ giữa Jennifer và Sói quả là rất thú vị và hài hước, một cô gái bé nhỏ không hề sợ hay chùn bước trước tên lãnh chúa to lớn với tiếng tăm lẫy lừng, mà ngược lại ương bướng và gây ra bao phiền toái cho hắn, thế mà dần dà tình yêu lại nảy nở giữa hai cá tính nổi trội này. Chàng sói thật sự đã khiến tôi rất ấn tượng trước tính nhẫn nại và tình yêu sâu đậm của hắn, thứ mà hoàn toàn trái ngược với bản chất “Sói” mỗi lần hắn ra tay trước kẻ thù và Judith, chính là người đã khiến trái tim băng giá của hắn tan chảy đồng thời để lộ một phần con người rất đáng yêu, tốt lành của hắn cho độc giả được nhìn thấy. 

Tôi thích cả hai nhân vật này, thích cách mà tác giả xây dựng cốt truyện, từng tuyến nhân vật đều có cá tính, có nội tâm rõ ràng. Tình yêu cũng là điểm sáng trong toàn bộ truyện của Judith, cách mà Sói thể hiện tình yêu với người mà mình yêu thương hay cách mà hắn hy sinh bản thân mình để chuộc lỗi với cô thật đáng khâm phục và cảm động. Họ đã yêu nhau, đã giận nhau nhưng vì yêu đã tha thứ cho nhau để có thể sống hạnh phúc trọn vẹn với tình yêu của mình. Một câu chuyện thật tuyệt.
5
41370
2015-06-29 20:51:51
--------------------------
156602
6196
Đây không phải là lần đầu tiên mình đọc một cuốn tiểu thuyết tình cảm  phương Tây, nhưng nó không khô khan như những cuốn mình đã từng đọc, và mình đã kết nó ngay từ cái nhìn đầu tiên.

Mình rất thích học tiếng Anh, và điều tốt hơn là mình giỏi bộ môn này, tuy nhiên mình thấy các từ ngữ trong tiếng Anh tương đối nghèo nàn, khô khan và rõ ràng, nhưng khi đọc truyện của Judith, mình đã thực sự khâm phục bà về lối viết và cách dùng từ. Câu chuyện thật trôi chảy và những từ ngữ thì diễn đạt một cách vô cùng chân thực tình cảm mãnh liệt, cháy bỏng của Royce và Jennifer, bà cho ta thấy những thử thách, thử thách thực sự mà một tình yêu đích thực phải trải qua, sự thù hằn và những quan điểm cổ hủ thâm căn cố đế, khoảng cách và sự khác biệt giữa các dân tộc, sự trái ngược đầy cuốn hút giữa hai nửa của tình yêu...

Cho đến tận bây giờ mình mới nhận ra rằng những câu chuyện tình yêu không phải chỉ khi được diễn tả bằng những từ ngữ ẩn dụ và những phép so sánh sâu xa thì mới trở nên ý nghĩa. Theo một cách nào đó, như cái cách mà Judith đã làm, bà đã viết một câu chuyện tuyệt vời bằng cách miêu tả nó thật chân thực - như chính bản thân nó phải thế, và lôi kéo mọi người, bất kì ai đã đọc câu chuyện này vào trong đó cho dù họ có muốn hay không.

Thật là một câu chuyện tuyệt hay~~~~!
5
137364
2015-02-05 14:08:07
--------------------------
150078
6196
mình rất thích truyện về cung điện,hoàng tộc phương tây nên đã mua thử để đọc.Nhưng có vẻ truyện không phải gu của mình.
Chuyện tình của nam nữ chính thì khá nhàm chán.Theo mô típ quen thuộc gập là yêu.Tính các nam nữ chính cũng rất rập khuôn.Nam lạnh lùng nữ ngây thơ nhưng mạnh mẽ
Cơ mà những tình tiết trong truyện thì khá hay.Mới đầu truyện hơi rườm rà nhưng diễn biến ngày càng nhanh.lôi cuốn,những cuộc đấu tranh tâm lí khá sâu sắc...Mình thích Royce.Anh trung thành và tình cảm của anh với Jenny rất đáng yêu và nó cũng khiến mình cảm động.Mình thích Sói nữa.Từ đầu đến cuối truyện chỉ mong anh được hạnh phúc.Jenny thật may mắn khi có sói ở bên cô.
Một số người nói thích văn phong của tác giả nhưng mình không thích lắm.Tác giả miêu tả cung điện xưa khá mờ nhạt,không có điểm nổi bật.Mình khá là thất vọng.
Đây là một cuốn tiểu thuyết nhẹ nhàng lãng mạng phù hợp để đọc giải trí.Giấy truyện tốt nhưng mình không ưng bìa lắm.Xấu và nhạt.Tòa lâu đài trông ảo lắm
3
379843
2015-01-15 17:28:33
--------------------------
136351
6196
Nếu bạn là người thích hơi hướng của Anh quốc thì đây là cuốn sách bạn nên đọc. Bạn sẽ khao khát ngày nào đó có một tình yêu như thế này, tưởng như nó sẽ là mơ nhưng xét theo khía cạnh hiện tại vẫn có thể xảy ra, chỉ là vật dụng được trang hoàng bằng bạc thì không thể rồi ^_^. Nhân vật ấn tượng sẽ làm cho bạn nhớ hoài khi đọc đến gần cuối cuốn sách, Arik lạnh lùng kiệm lời, to xác lại sợ rắn, dù là người ít nói nhưng anh ta là một chiến binh trung thành, đôi khi ngớ ngẩn, sẵn sàng đốn ngã cả cây thông vì con nhện trên cây cắn mình ^_^. Nhân vật nữ trong truyện của Judith MC thì lúc nào cũng  đẹp khỏi chê. Ngôn từ rất dễ đển hình dung và cảm nhận bạn đang sống trong một thế giới quý tộc cổ xưa, băng qua những cánh rừng, cảm động vì hình ảnh trung thành của chú ngựa Thor. Dấng để đọc
5
156553
2014-11-20 15:58:30
--------------------------
130364
6196
Một cuốn tiểu thuyết khiến người ta muốn mua ngay khi nhìn thấy cái tên, trong cuộc sống hiện nay thì giấc mơ là những điều cần thiết, nó khiến ta nhẹ nhõm hơn, và tôi mong chờ ở nội dung bên trong một câu chuyện nhẹ nhàng, nhưng trái với cái tiêu đề. Nội dung của câu chuyện lại là một câu chuyện đầy kịch tính với những trái tim yêu phải đấu tranh giữa trái tim và lí trí, những trận chiến khi cuộc sông của những người lại phụ thuộc vào quyết định của các vị vua,... Khi mới đọc giọng văn dẫn dắt chưa thực sự cuốn hút nhưng càng sau đó ta càng bị cuốn vào mạch chuyện, có niềm vui nhưng cũng không kém đau thương, nhưng cuối cùng một cái kết ấm áp sẽ xoa dịu trái tim ta.
4
191232
2014-10-17 08:31:13
--------------------------
129789
6196
Bằng sự tàn nhẫn của Sói, Royce Westmoreland đã hạ gục rất nhiều kẻ thù, băng qua những thành trì đỏ rực lửa chiến tranh để trở thành cơn ác mộng ghê gớm nhất của những người dân xứ Scotland. Thế nhưng, cũng chính con người ấy, bằng tình yêu của Sói, đã tặng cho Jennifer Merrick – con gái của kẻ thù, một vương quốc, vương quốc của những giấc mơ và trở thành người bảo vệ mạnh mẽ nhất của nàng.
Sự lãng mạn đã xuất hiện từ ngay từ tên của tác phẩm và từ phần bìa sách lung linh sắc trắng xanh của mây trời. “Vương quốc của những giấc mơ” là câu chuyện kể về chàng chiến binh tên Sói - Royce Westmoreland và cô thiếu nữ mạnh mẽ Jennifer Merrick. Cách hành văn của Judith McNaught đủ lãng mạn và cũng đủ ly kỳ, đặc biệt sắc sảo trong các đối thoại của nhân vật. Mình từng đọc nhiều tiểu thuyết lãng mạn lịch sử, nhưng rõ ràng những tác phẩm của bà là một thế giới rất khác, với sự tinh tế trong ngôn từ và những tình yêu luôn bắt đầu trong khoảnh khắc khiến người đọc ngơ ngẩn. Tình tiết truyện khá đặc sắc và tình huống không ngừng xoay chuyển, có cảm giác rất sống động. Tâm lý nhân vật được miêu tả tốt và có diễn biến hợp lý.
Về các nhân vật, mình rất ấn tượng với sự giằng xé giữa lòng trung thành và tình yêu của Jennifer, giữa thù hận truyền kiếp và lòng tin. Cái tàn khốc của chiến tranh và sự ê chề khi bị mọi người dè bỉu không thể hạ gục Jenny, nhưng mình lại thấy nàng rất đáng thương khi bị người cha với trái tim đầy thù hận sử dụng như một công cụ trả thù. Jenny là cô gái rất đáng mến nhưng mạnh mẽ, và một khi gặp phải người như Royce thì đương nhiên là giữa họ sẽ nảy sinh ra những tình huống không biết đỡ kiểu gì. Chẳng hạn như cái lần Jenny cắt nát mấy cái chăn, khiến Royce nổi điên và lật cô nàng lại để đánh đòn như đánh con nít, hay như khi Royce tán tỉnh Jenny bằng những câu đại loại như: “Mắt nàng giống vải len ướt, còn da nàng khiến ta liên tưởng đến vỏ trứng gà.”,... Mình đã chết cười khi đọc những đoạn đấy. 
Và vẫn còn đó sự đối đầu giữa hai vùng đất. Jenny là thiếu nữ ngoan cường bị chính người thân của mình lợi dụng, còn Royce lại chiến đấu vì lòng trung thành với đất nước và đức vua. Sau bao nhiêu biến cố, mâu thuẫn sâu sắc và những hiểu lầm, mình tự hỏi, liệu Jenny có thể tìm thấy vương quốc của riêng mình và liệu Sói có thể ở bên người phụ nữ mà anh yêu thương?
“Liệu có ích gì không, nếu ta nói rằng ta yêu nàng?"
Mình rất xúc động khi đọc tới phân đoạn trong đấu trường, Royce đã giữ lời hứa rằng sẽ không động đến người thân của Jenny, và mình thật sự sụt sịt khi biết rằng Royce sẵn sàng đứng đó, chết dần chết mòn vì cô, như một cách anh xin lỗi khi đã vô tình giết chết anh trai của Jenny. Đó là một tình yêu đủ mạnh để vượt qua hai bờ chiến tuyến và vượt qua ranh giới của lỗi lầm hay thứ tha.
Không thể đổ lỗi cho chiến tranh, vì đôi khi những chúng chỉ bắt nguồn từ sự mù quáng của con người. Nhưng giữa tất cả những cuộc chiến không khoan nhượng ấy, mình rất mãn nguyện khi biết rằng Jennifer rồi sẽ bình yên, vì nàng đã được tình yêu của Sói bảo vệ.
5
198930
2014-10-12 19:19:26
--------------------------
124743
6196
Quyển sách là một cuộc thực hiện những trải nghiệm bản thân cũng như thực hiện khát khao của Jenifer và câu bạn Sói Đen. Mạch truyện với nhiều tính tiết được chuyển tấu một cách nhanh chóng nên làm cho người đọc cực ký hứng thú và cuốn hút. Văn phong cũng tự nhiên, không bóng bẩy, nhiều hình ảnh mơ mộng, nhẹ nhàng pha lẫn chút lãng mạng.
Là một bài học cho tình yêu thương giữa những con người với nhau, là sự vượt lên khó khăn để sống và thực hiện ước mơ của mình.
Một quyển truyện sống động và đầy màu sắc. Một trí tưởng tưởng thật tuyệt vời!
5
303773
2014-09-09 07:43:20
--------------------------
124711
6196
Một câu chuyện khá ổn trong số những tiểu thuyết tình cảm lãng mạn. Cốt truyện cũng tương đối ổn, lời văn cũng ổn. Đây sẽ là một cuốn tiểu thuyết tuyệt với cho những ai thích thể loại truyện này. Tuy nhiên, hầu như tất cả các tiểu thuyết tình cảm lãng mạn, tác giả chỉ xây dựng có một kiểu nhân vật (nam thì mạnh mẽ, lạnh lùng, từng trải...; nữ thì bướng bỉnh, mạnh mẽ, ngây thơ...), và chỉ có một kiểu tình huống: gặp là yêu. Nói tóm lại là nó cũng không mới mẻ gì cho lắm, và đặc biệt không phù hợp với những ai muốn tìm làn gió mới trong văn học.
3
220006
2014-09-08 22:23:41
--------------------------
124084
6196
Nếu bạn đang buồn chán và rất cần một điều gì tươi mới thì Vương quốc của những giấc mơ là một trong những lựa chọn sáng suốt nhất . Hoàn cảnh của Jennifer trong truyện làm tôi rất thương cảm cô là công chúa luôn được hưởng mọi điều tốt nhất bỗng một ngày lại bị đi đày bị đối xử không khác gì thường dân .Tôi rất ghét nhân vật cha của Jennifer đây là kiểu người có rất nhiều tham vọng sẵn sàng hy sinh tất cả để có được điều mình muốn . người mẹ kế thì mưu mô ác độc là hình ảnh ta thường thấy trong tính cách của các nhân vật phản diện . Sói đen là một chàng trai tốt bụng luôn tỏ ra mình là người giỏi nhất .Jennifer và Sói đen đã cùng chung tay gây dựng nên một vương quốc tự do nơi mọi người đều được hạnh phúc . Mạch truyện khá nhanh hết yếu tố này đến yếu tố khác ngoài ra còn được pha trộn một ít sự tưởng tượng nữa càng làm cho truyện thêm hay hơn
5
337423
2014-09-05 11:00:02
--------------------------
