387787
4884
Bộ sách Mười Vạn Câu Hỏi Vì Sao chính là chìa khóa để đáp ứng cho nhu cầu tìm tòi, khám phá của trẻ cũng như bổ sung kiến thức cho các bậc làm cha làm mẹ. Sách có hình vẽ ngộ nghĩnh, minh họa cụ thể dễ dàng. Dễ đọc dễ hiểu nhờ những kiến thức rất thực tế về những câu hỏi gần gũi với thế giới bên ngoài, về nhưng điều ta thường hay thắc mắc, những điều tưởng chừng như đơn giản hay thậm chí những điều ta không hề nghĩ tới. Là bộ sách bổ ích rất nên đọc theo cảm nhận của mình.
5
1108222
2016-02-27 17:15:56
--------------------------
