373975
6580
Mình từng xem Prison Break season 1 và khá yêu thích những tình tiết thông minh và kịch tính trong phim. Tuy nhiên, bộ phim đó không là gì nếu đem so sánh với tác phẩm này. Không thể tưởng tượng nổi một người thanh niên có khát vọng sống vĩ đại đến nhường ấy, cộng với trí thông minh, lòng bác ái và tài năng văn chương thiên phú của tác giả - mình cho rằng đây là tác phẩm tự truyện kịch tính và tuyệt vời nhất mình từng đọc. Là phụ nữ, có những tình tiết trong truyện mình cảm thấy hơi buồn cho số phận của những người đàn bà trong tác phẩm - tuy nhiên nếu nhìn tổng quan, họ chính là một trong những nhân tố làm nên nét hùng tráng của tác phẩm. 
5
169228
2016-01-25 10:04:37
--------------------------
365633
6580
Đây là một cuốn tự truyện được kể lại chân thực về một người đàn ông bị kết tội giết người cho một nạn nhân mà anh chưa từng gây ra. Với lòng khát cháy tự do, không chịu nổi sự tù hãm, anh liên tục tìm cách vượt ngục. Những người tù khác có thể suy sụp và gục ngã, nhưng Bướm, người đàn ông đó vẫn nuôi ý chí và giữ vững tinh thần để đợi một ngày có cơ hội. Dù bị nhốt trong nhà biệt giam, anh vẫn tập bước đều cho quen để giữ thể trạng tốt. Anh vượt ngục bằng cách cho những quả dừa vào bao tải, buộc chặt cùng với mình để sóng đẩy mình trôi lênh đênh trên biển và tự giải thoát cho chính mình. Sau mỗi lần bị bắt lại, anh lại lên ngay kế hoạch tẩu thoát. Giờ thì anh đã là công dân tự do của nước khác, đã có vợ con. Cuốn sách tuykhông xuất sắc về mặt văn học nhưng nghẹt thở vì sự chân thực
Sách được in bìa cứng đẹp, bên trong giấy khá mỏng, tên nhân vật phiên âm tiếng việt nhưng cũng chấp nhận được. Cuốn sách rất hay
5
23764
2016-01-08 13:53:56
--------------------------
