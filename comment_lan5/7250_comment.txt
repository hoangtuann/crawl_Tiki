453541
7250
Vì tò mò câu chuỵên của cô Matsuzaka và chàng bác sĩ mê khủng long Tokuro nên mình tìm mua 47 ngay sau khi đọc xong 46 . Những tưởng họ đã hạnh nhưng  " ai ngờ kết cục lại buồn như vậy  " , mình hơi bất ngờ khi Shin vốn là một bộ  truỵên hài hước mà lại có tình huống khiến người đọc có thể rơi nước mắt như vậy,  điều này càng làm truỵên thêm phần ý nghiã  .
Ngòai ra thì ở tập này bé Hima cũng đi mẫu giáo rồi,  quậy tưng bừng không kém gì anh Shin  ^^ .
Tiki giao hàng nhanh nữa ,  sẽ ủng hộ dài dài : 3
5
617834
2016-06-21 12:02:37
--------------------------
385581
7250
Lần đầu tiên mua sách tại tiki, tiki gói hàng rất đẹp, cuốn truyện thì mới toanh, sạch bóng không bị nhăn nheo như ở các nhà sách nội dung truyện rất hay - rất hấp dẫn và mắc cười với những hành động nghịch ngợm của cậu bé Shin, phù hợp với lứa tuổi 9 đến 16, lại còn được tặng quà cover sách xuân nữa chứ quá đã nhưng tiki giao hàng khá lâu khoảng 7-8 mong tiki xem xét lại điều này. Nói tóm lại đây là một cuốn truyện hay - có tính giải trí cao sau nhưng ngày học tập căng thẳng, mệt mỏi.
5
765121
2016-02-24 10:52:14
--------------------------
375648
7250
Tập truyện này đối với mình thật bất ngờ, cuối cùng cũng thấy Himawari đi học nhà trẻ mẫu giáo, cô bé nghịch ngợm không khác gì cậu bé Shin cả. Còn phần Quỷ dữ hồi sinh thì phải đến tập sau mình mới biết kết thúc, tại mình chưa đọc tập trước nên cũng không biết diễn biến từ đầu thế nào cả, những câu chuyện diễn biến vẫn rất gay cấn và hồi hộp, cậu bé Shin trông rất tinh nghịch bên cạnh Quỷ vương. Còn phần truyện của cô giáo Mastuzaka nữa, thật làm cảm động lòng bạn đọc!
4
564406
2016-01-28 21:31:48
--------------------------
325634
7250
Cuốn truyện Shin cậu bé bút chì tập 47 ( 2014 ) này mình công nhận nó rất là hay và hài !! Lúc trước mình mua trên Tiki Shin cậu bé bút chì tập 46 , tính mua Shin cậu bé bút chì tập 47 mà lúc đó đã hết hàng nên mình chỉ mua tập 46 , nên vì thế hôm nay mình mới mua cuốn này cho hợp thứ tự. Cái bìa trước truyện rất dễ thương , và đẹp . Cuốn truyện này như bao cuốn truyện khác , chất lượng , tình huống của cậu bé Shin rất hay , dễ thương , tốt . Đối với mình sản phẩm này rất là hay và được!!
5
603156
2015-10-23 22:09:29
--------------------------
232574
7250
"Shin cậu bé bút chì" là tổng hợp những câu chuyện thú vị và đầy "oái ăm" của gia đình cậu bé Shin. Siêu quậy Shinosuke đã biết bao lần khiến mẹ phải xấu mặt vì những hành động "vô tư" của mình. Chính cậu bé cũng gây nên biết bao rắc rối, bao tình huống dở khóc cười cho những người xung quanh. Tuy thế, cậu bé cũng rất tốt bụng, dù đôi khi "thái quá". Một cậu nhóc mà khi xuất hiện sẽ mang đến những giây phút thư giãn đầy tiếng cười cho người đọc. Dù nét vẽ rất sơ sài, ít trau chuốt bóng bẩy như những tác phẩm manga khác, nhưng tác phẩm này vẫn được ưa chuộng nhờ nội dung thú vị mang tính giải trí cao. Một tác phẩm phù hợp cho cả trẻ em lẫn người lớn!
4
77881
2015-07-18 21:31:34
--------------------------
