368582
4606
Tựa văn nghe rất hay. Nhưng nói lúc nào cũng dễ hơn làm. 4 năm qua đi rồi, mình chẳng thể làm vậy được. Dẫu biết cố níu kéo thì người đau khổ mãi vẫn chỉ là mình. Người ta vô tâm, người ta hững hờ, mỗi ánh mắt như muốn đâm vào tận tâm cam mình.Nhưng cuốn sách lại là 1 chia sẻ rất thú vị của tác giả. Biết là đau đó nhưng chúng ta phải thử làm 1 lần trong đời. Ai rồi cũng phải buông tay một ai đó, chẳng thứ nào trên đời mãi mãi thuộc về mình!
5
1110546
2016-01-14 11:42:03
--------------------------
353902
4606
Truyện dài lê thê , chưa có nội dung xuất sắc phù hợp với nhiều người đã có mối tình nào đó thật sâu sắc rồi , thế nhưng người nào có nhiều điểm tương đồng với nhân vật thì đều không thể hiểu được những gì tác giả nói ở trong đó , một trong số những điều quan trọng trong kết cấu về suy nghĩ , cách hành động đều chưa đồng nhất , lời văn rời rạc và lâu nên nhàm chán mà không có cảm giác thư giãn với câu truyện như này , chỉ có bìa truyện nhìn vẽ đẹp .
4
402468
2015-12-17 05:05:25
--------------------------
342451
4606
Cuốn sách Tay buông tay và tim thôi nhớ đã gây ấn tượng rất mạnh với mình ngay từ những từ ngữ của cái tựa sách. Tay buông tay và tim thôi nhớ nghe có vẻ rất buồn và bi đát,nhưng ai rồi cũng phải trải qua một lần đau trong cuộc đời. Đọc cuốn sách này để cảm nhận trước được những cung bậc cảm xúc, những cánh cửa tình yêu. Để những ai đã trải qua cảm giác đau trong tình yêu có thể thấy rằng mình đau khổ nhưng vẫn có người đau khổ hơn hoặc những ai chưa từng trải có thể rút kinh nghiệm cho bản thân mình. Mình cũng thích cách trang trí bìa sách, bố cục và thần thái tốt. Mình thích cuốn sách này.
5
695640
2015-11-24 14:38:36
--------------------------
324294
4606
Cuốn sách với những bài tản văn ngắn gọn, súc tích nhưng là cả kinh nghiệm của người viết sách. Đó là những gì tác giả đã từng trải. Đọc tác phẩm tôi thấy tôi của những ngày trước, đã từng như vậy, đã yêu như vậy, đã đau khổ và trải nghiệm rất nhiều cảm xúc như vậy
Bây giờ nhìn lại. Nhìn cuộc tình mình đã trải qua, có nước mắt, có nụ cười. Và quan trọng là mình đã trưởng thành hơn rất nhiều. Và đã tha thứ cho ai đó. Đã bao dung và chấp nhận cho một ai khác bước vào để bắt đầu 1 chuyện tình mới
4
507725
2015-10-20 21:39:30
--------------------------
311924
4606
Mình mới đọc xong cuốn sách này tuần trước, và mình khá ấn tượng với cuốn sách. Những mẩu truyện nhỏ mang tính chất của những bài học và những lời khuyên về tình yêu, đọc thấy hay và thấm thía. Tác giả cũng đem lại những góc nhìn khác nhau về tình yêu, đọc thấy hay và ý nghĩa. Có thể liên hệ với những kinh nghiệm trong quá khứ của người đọc. Lối viết nhẹ nhàng của tác giả cũng đặc biệt vì cô cũng bộc lộ suy nghĩ của bản thân cô, cảm giác như tác giả đang nói chuyện với người đọc. 
4
41201
2015-09-20 13:18:36
--------------------------
223462
4606
Tôi mua quyển sách này vì tựa sách TAY BUÔNG TAY VÀ TIM THÔI NHỚ nghe rất đồng cảm.Tôi muốn tìm cho mình cảm giác quên được.Tìm cách gì đó để quên,tìm thứ gì đó để không còn nhớ!Nghĩ rằng tay buông thôi thì sẽ quên được nhưng nào ngờ khó thế!Nhưng thực sự khi đọc xong cũng nhẹ người được phần nào ,nghĩ thoáng một chút.Có thể bị phụ tình rồi cũng quên mau!Đến lúc nào đó ta  sẽ phải cảm ơn vì người trước đã ra đi để gặp người tốt hơn.!Hay  chỉ đơn giản vì người đó không xứng đáng để ta trân trọng.Lối văn nhẹ nhàng nhưng cũng nhạt 1 chút lúc lại đậm sâu,có vẻ như những gì tác giả có tình cảm đặc biệt với chương nào thì sẽ viết hay và sâu sắc hơn.....
4
301811
2015-07-07 09:31:45
--------------------------
196349
4606
Mình thích Trương Tiểu Nhàn từ lần đầu đọc "Tuyển tập tản văn hay " của chị nên lần này thấy "Tay buông tay và tim thôi nhớ" đã đặt mua ngay. Tuy nhiên mình cũng có chút thất vọng vì nội dung không như mình mong đợi , ít ra là phải lôi cuốn được như tiêu đề của cuốn sách mang lại . Vẫn là tập hợp những tản văn ngắn nhưng nhiều bài viết hết sức rời rạc. Phần dịch thuật cũng dường như không tốt lắm khi chuyển ý quá cứng nhắc khiến mình đọc nhiều chỗ phải suy nghĩ mãi. Mà thôi, đã mua rồi thì không tiếc. Không lại tiếc :)
3
75031
2015-05-15 18:40:22
--------------------------
186984
4606
So với tác phẩm "Tuyển tập tản văn hay của Trương Hiểu Nhàn" thì đây không phải là quyển xuất sắc nhất. Dù vậy, vẫn như thế, văn phong ngắn gọn, dứt khoát mà câu chữ vẫn rất hàm xúc, ý của từng câu, từng đoạn được gói gọn chứ không lan man dài dòng lê thê. Cuối tản văn có những đúc kết suy ngẫm của tác giả nên có cảm giác đọc rất logic.
Những tản văn này không phải dễ đọc và dễ hiểu nếu là người không hợp thể loại. Tuy nhiên, nội dung tản văn sâu sắc và ý nghĩa, lấy những điều thực tế nhìn thấy để khái quát và nêu lên những suy nghĩ sống, lời khuyên rất chất. Nếu có dịp, tôi nghĩ mình sẽ đọc đi đọc lại vài lần vì mỗi lần đọc tôi lại cảm những điều khác nhau. 
Có buông bỏ thì mới nắm lấy được những điều tốt đẹp hơn.
5
137606
2015-04-22 10:39:49
--------------------------
182505
4606
Sau khi đọc những dòng trích dẫn ở bìa sau cũng như những lời giới thiệu "có cánh" về bộ 3 tác phẩm của Trương Tiểu Nhàn được xuất bản tại Việt Nam, mình đã rất hy vọng đây sẽ là 1 trong những quyển sách khiến mình phải đọc đi đọc lại. Nhưng rốt cuộc sau khi cầm trên tay, mình buộc phải cảm thấy tiếc vì đã bỏ tiền ra mua nó. Nội dung hời hợt, sơ sài, đôi khi lại lan man đến mức khó chịu. Văn phong thì thật sự là mình ko "tiêu hoá" được, đôi khi mình cảm giác đang đọc những dòng lảm nhảm của một đứa trẻ chưa lớn. Thêm 1 điểm trừ cực lớn ở khâu in ấn và biên tập, sách bị in thiếu mất từ trang 145 đến 160. Hy vọng nhà xuất bản sẽ chú trọng hơn trong những lần tái bản sau (mặc dù với chất lượng sách như thế này mình cũng ko hy vọng sách sẽ được tái bản).
1
391626
2015-04-14 11:30:56
--------------------------
182384
4606
Sau khi đọc những dòng trích dẫn ở bìa sau cũng như những lời giới thiệu "có cánh" về bộ 3 tác phẩm của Trương Tiểu Nhàn được xuất bản tại Việt Nam, mình đã rất hy vọng đây sẽ là 1 trong những quyển sách khiến mình phải đọc đi đọc lại. Nhưng rốt cuộc sau khi cầm trên tay, mình buộc phải cảm thấy tiếc vì đã bỏ tiền ra mua nó. Nội dung hời hợt, sơ sài, đôi khi lại lan man đến mức khó chịu. Văn phong thì thật sự là mình ko "tiêu hoá" được, đôi khi mình cảm giác đang đọc những dòng lảm nhảm của một đứa trẻ chưa lớn. Thêm 1 điểm trừ cực lớn ở khâu in ấn và biên tập, sách bị in thiếu mất từ trang 145 đến 160. Hy vọng nhà xuất bản sẽ chú trọng hơn trong những lần tái bản sau (mặc dù với chất lượng sách như thế này mình cũng ko hy vọng sách sẽ được tái bản).
1
391626
2015-04-13 22:32:51
--------------------------
182324
4606
" Tay buông tay và tim thôi nhớ " - tình yêu khi đến một lúc nào nào vỡ vụn trong hư vô  , chính khi ấy chúng ta phải buông tay nhau và thôi nhớ nhung về đối phương . 
Dù tình yêu ấy là có mang theo sự thù hận hay tình yêu đong đầy thì đều là một sự đau đơn khó có thể hình dung . Liệu rằng khi đã yêu , đã hận khi chia li có thể thoải mái ? Chỉ khi ta buông được tất cả xuống thì khi chia ly nỗi đau ấy mới với bớt đi ít nhiêu . 
Mỗi lần nhìn lại cuộc chia li ấy chúng ta có thể thấy được rằng : Đúng vậy , đối phương chẳng đáng cho chúng ta phải bỏ ra yêu thương , vậy nên khi đã hiểu , khi chia ly hãy buông tay dứt khoát . Không nên hận thù hay thương nhớ mà hãy tiến về một tình cảm chân thật khác ở tương lai .
4
335856
2015-04-13 20:30:04
--------------------------
170012
4606
Cuốn sách Tay buông tay và tim thôi nhớ đã gây ấn tượng rất mạnh với mình ngay từ những từ ngữ của cái tựa sách. Tay buông tay và tim thôi nhớ nghe có vẻ rất buồn và bi đát,nhưng ai rồi cũng phải trải qua một lần đau trong cuộc đời. Đọc cuốn sách này để cảm nhận trước được những cung bậc cảm xúc, những cánh cửa tình yêu. Để những ai đã trải qua cảm giác đau trong tình yêu có thể thấy rằng mình đau khổ nhưng vẫn có người đau khổ hơn hoặc những ai chưa từng trải có thể rút kinh nghiệm cho bản thân mình. Mình cũng thích cách trang trí bìa sách, bố cục và thần thái tốt. Mình thích cuốn sách này.
4
468790
2015-03-19 12:18:25
--------------------------
