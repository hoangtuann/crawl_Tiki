472374
5197
Cuốn sách “Sử ta - Chuyện xưa kể lại tập 1” nằm trong bộ sách bốn tập của các tác giả Nguyễn Huy Thắng, Nguyễn Như Mai và Nguyễn Quốc Tín. Cuốn sách đã đem tới những rung động đầy cảm xúc nơi người đọc: lòng tự hào truyền thống vinh quang của tổ tiên, sự ngưỡng mộ những bậc vĩ nhân tài hoa mà giản dị, nỗi đau xót trước những mất mát của dân tộc… Cảm ơn các tác giả và Nhà xuất bản Kim Đồng đã đem đến cho bạn đọc một tác phẩm thực sự đáng quý.

5
470567
2016-07-09 15:30:05
--------------------------
423558
5197
Đây là quyển sách thứ 2 tôi mua trong loạt bài: Sử ta - chuyện xưa kể lại. Tiếc là Tiki chưa phục vụ được tập 3, tập 4. Hy vọng sẽ được mua 2 tập này trong thời gian ngắn nhất. Tập 1 này trình bày giai đoạn lịch sử từ đầu đến thời Đinh. Những câu chuyện lịch sử được kể với giọng kể giản dị, ngắn gọn, súc tích. Đủ để người đọc hiểu và cảm nhận qua từng giai đoạn lịch sử hào hùng của dân tộc. Giải thích rõ ràng những di tích, câu nói, địa điểm, niên hiệu của nước ta ứng theo từng thời kì. Tóm lại, đây là quyển sách đáng đọc.
4
404105
2016-04-29 16:17:37
--------------------------
400042
5197
Bộ sử được các tác giả của nhà xuất bản Kim Đồng biên soạn lại một cách rất cô đọng, súc tích những nhân vật, sự kiện có tầm ảnh hưởng tới toàn dân tộc và góp phần làm nên lịch sử vẻ vang của nước nhà. Tập một khái quát lịch sử nước Việt Nam từ thủa hồng hoang với những truyền thuyết quen thuộc như Trăm trứng đẻ ra trăm con, Trầu Cau, Mai An Tiêm, Thánh Gióng, Chử Đồng Tử tới những buổi đầu dựng nước, bảo vệ nước  với cách sắp xếp theo thời gian trước sau rất dễ hiểu. Bộ sách không chỉ thích hợp với các em nhỏ mà với bất kỳ ai muốn hiểu thêm về nguồn gốc nước nhà.
5
93234
2016-03-18 18:34:58
--------------------------
