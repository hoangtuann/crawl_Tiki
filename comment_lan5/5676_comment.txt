491902
5676
Mình mới nhận được cuốn sách này lúc chiều, thật sự khá thất vọng khi nhận được sách. mình đặt mua nguyên bộ 3 cuốn thần thoại Bắc Âu vì mình thích thần thoại, nhưng khi sách được giao hàng cứ như sách cũ mua lại, 2 cuốn tập 1, tập 2 còn đỡ, chứ cuốn 3 này vừa cũ, giấy xấu, bìa nhăn, đề nghị Tiki xem lại vấn đề này. Còn về nội dung thì không có gì để chê trách, đây là cuốn tiểu thuyết thần thoại hay, lời văn lôi cuốn và hấp dẫn, nếu ai yêu thần thoại thì không thể thiếu bộ truyện này trên giá sách được.
3
1328007
2016-11-21 19:20:25
--------------------------
422136
5676
Quả thật không còn kết truyện nào hợp lý hơn, mặc dù mình vẫn rất thích và thương cảm cho nhân vật Loki. Diễn biến tâm lý nhân vật cũng phần nào khiến người đọc cảm thấy thỏa mãn. Ít ra đâu đó vẫn còn hình bóng của Loki. Câu truyện đem đến nhiều cung bậc cảm xúc. Đây là thần thoại Bắc Âu đầu tiên mình đọc, thật sự rất ấn tượng. Câu truyện tình yêu đẹp. Không giống kiểu ngôn tình soái ca mà theo một hướng rất lạ. Về hình thức truyện đẹp, dày, giấy tốt, giảm giá sâu. Rất đáng để mua. 
4
288800
2016-04-26 15:57:16
--------------------------
421134
5676
Lời chúc phúc của odin tập 3 khép lại một quá trình huyền bí , giả tưởng những quá khứ còn văng vẳng bên tai người đang cố xoay chuyển tình thế gian nan , khó khăn ngàn lần , câu nói rất dễ thích nghi , dễ gần và đem lại những giá trị tinh thần rất lớn , đi theo sự cảnh giác cao độ những con đường cạm bẫy mà hỗn loạn bao vây từ việc xuất hiện của các thế lực cao lớn hấp dẫn làm đối phương phải thay đổi bước tiến chống lại bóng đêm , tác phẩm khá hay và nên chọn xem
4
402468
2016-04-24 15:18:52
--------------------------
366310
5676
Đây là bộ đồng nhân đầu tiên tôi đọc hoàn chỉnh. Vì là đồng nhân của thần thoại Bắc Âu, thần thoại tôi yêu thích nhất trong số các thần thoại cổ đại (có Hy Lạp, Bắc Âu, Ai Cập…) nên tôi không lưỡng lự chọn mua ngay từ hôi đầu nó mới ra tập 1. Truyện có một nữ chính mang hai ký ức, và hai nam chính, nên thái độ của tôi rất mông lung. Tôi chẳng biết hy vọng nữ chính về với nam một hay nam hai nữa. Cảm giác bối rối, băn khoăn này kéo dài từ đầu truyện xuyên qua từng trang sách. Vốn là truyện đồng nhân nên đương nhiên bản gốc bị thay đôi kha khá. Ngày trước tôi có đọc truyện tranh thần thoại Bắc Âu, những hình ảnh minh họa trong đó bị áp đặt vào nhân vật trong truyện này khá nhiều, song, cũng dần vơi bớt. Hỏa thần Loki với mái tóc đỏ trở thành em của Frigg và yêu nàng điên cuồng. Odin vẫn là chủ các vị thần, có mối tình sâu sắc với Frigg. Còn Frigg thì yêu Odin vô cùng. Tôi sẽ rất hài lòng nếu Frigg và Odin sẽ giữ nguyên như thế, nếu không xảy ra cuộc chiến Hoàng hôn. Các vị chủ thần đều chết một lần, ở trong thân xác người phàm, chờ đợi ngày thức tỉnh. Frigg cũng thế. Trong thời gian đó, nàng là Ena, nữ thần kim tượng của Vanir, đem lòng mến mộ Lando. Lando thức tỉnh thành Loki và họ ở bên nhau. Dù đọc tập 1 từ hơn một năm trước, tôi vẫn nhớ như in hình ảnh Loki dịu dàng ân cần bên Ena, và hình ảnh hắn gào thét điên cuồng khi Ena giũ bỏ ký ức trở lại là Frigg. Tôi đã cảm thấy rất đau lòng. Tôi còn nghĩ Loki nên là nam chính. Tôi từng nghĩ, Odin xuất hiện dưới danh nghĩa Shujin một cách mờ nhạt như vậy, thì Frigg đến với Loki cũng tốt. Hóa ra mọi việc không hề đơn giản như vậy. Tập 2 là kể về quá khứ khi Frigg còn chưa thành thần Tình yêu. Tôi mới nhận ra, Odin xuất sắc như thế nào, và họ xứng đáng với nhau ra sao. Loki là một đứa trẻ bé nhỏ, được Frigg coi như em trai. Nàng sẽ không yêu hắn theo kiểu tình yêu nam nữ. Tình cảm của nàng với Loki khác hoàn toàn với Odin, mặc dù cả hai đều rất quan trọng với nàng. Hình ảnh Loki cô đơn lặng lẽ nhìn tình cảm mình lớn dần trong tim khiến tôi đau nhói. Hắn là kẻ cố chấp, ích kỷ, ép buộc Frigg ở bên mình. Odin thì khác. Chàng thông minh, mạnh mẽ, ấm áp, và cho nàng lựa chọn. Loki thể hiện tình cảm ở cả hành động và lời nói. Còn Odin chủ yếu là hành động. Giống như Sif từng miêu tả, Loki giống đứa trẻ ranh, xốc nổi, ngang ngạnh. Odin thì trầm tính, chín chắn hơn. Ai cũng đều dành cho Frigg tình cảm mãnh liệt. Điều quan trọng là trái tim nàng đặt nơi đâu. Khi đọc sang tập 3, tôi rất hỗn loạn. Ena là Ena, Frigg là Frigg. Nhưng lý trí và tình cảm của Ena vẫn ở trong thân thể của Frigg. Lý trí và tình cảm của Frigg đôi lúc vẫn dao động vì một “ta” khác đó. Ước gì, Ena chưa bao giờ từng tồn tại, như vậy Frigg sẽ không có cảm xúc đó với Loki. Nhưng như vậy, thì Loki sẽ tổn thương sâu sắc lắm. Ena là hồi ức ấm áp nhất mà hắn từng có. Nàng cho hắn tình yêu thương, sự dịu dàng và Fasier. Còn Frigg thì chỉ nghĩ đến Odin, đến bộ tộc Aesir, đến những mặt xấu xa của Loki. Tôi rất thương Loki, vì nhiều đoạn Frigg khiến hắn đau đớn. Mối tình đầu của hắn kéo dài qua ba nghìn năm, dài bằng đoạn tình duyên của nàng và Odin. Chỉ khác là, nàng coi hắn như đứa trẻ, và với tính cách hắn, hắn hủy hoại Asgard, hủy hoại Frigg. Loki luôn bộc lộ cảm xúc nên tôi có thiên vị cho Loki hơn chút. Tuy nhiên, tôi không phủ nhận rằng, đọc những đoạn có Odin, tôi cảm động và thừa nhận Frigg sẽ mãi mãi thuộc về Odin. Odin giấu tất cả mọi thứ trong trái tim. Bao nhiêu điều chàng làm vì nàng đều bị che dấu. Đến cuối cùng, khi mọi việc bại lộ, tôi mới ngỡ ngàng, hóa ra chàng đã hy sinh nhiều như thế. Chàng đã chết nhưng hình bóng của chàng có thể kéo dài một nghìn ngày, chứng tỏ tình yêu của chàng cho Frigg vô cùng, vô cùng sâu đậm. Tôi nghĩ Loki cũng nhìn ra. Odin đánh đổi sự bất tử của mình lấy sự chuyển thế của Frigg. Vì thế Loki đã lựa chọn cứu rỗi bản thân, giúp Frigg hạnh phúc. Loki biến mât, còn Odin trở lại. Có lẽ Loki là cái cây mọc lên sau điện Sương Mù. Loki muốn ở bên nàng, ngắm nhìn nàng và con của hắn mõi ngày. Loki đánh đổi bản thân để lấy lại thần vị cho Odin. Loki không chết, nhưng hắn không còn bản thể của một vị thần. Hắn đã thay đổi theo hướng tốt đẹp, vì hắn biết, nàng sẽ mãi mãi không quay trở về bên hắn nữa. Tôi có hơi hẫng hụt với cái kết này, nhưng quả thực nó rất hợp lý. 

Tyr và Sif vẫn đến với nhau. Thor có một hạnh phúc thực sự bên người khác. Balder quá đáng ghét, bị thù hận che mờ tâm trí. Còn chuyện của Hoder với Freyr luôn tỏ vẻ mập mờ nhưng lại dễ dàng nhìn thấu, thật khó chịu mà.

Bỏ qua những lỗi chính tả không đáng có, nội dung gốc của thần thoại, thì đây là một câu chuyện đáng đọc. Tuy hơi rối vì ba ký ức (trước buổi Hoàng hôn của các vị thần, trước khi chủ thần thức tỉnh, sau khi chủ thần thức tỉnh), chi tiết chính và cao trào đẩy lên rất khá. Văn phong mạch lạc. Nhân vật xây dựng tốt. Nhiều đoạn khá là ngược, đọc mà đau lòng vô cùng. Nếu là fan thần thoại Bắc Âu, chắc chắn không nên bỏ qua quyển này.
4
926764
2016-01-09 17:59:48
--------------------------
358477
5676
Tôi đã từng yêu nhân vật Loki..mãi cho đến khi tôi biết hắn đã làm những gì. Tình yêu hắn dành cho frigg quá biến chất. Nó phá hoại tình yêu chung thủy của odin và frigg. Tôi hơi ghét frigg vì không có cách giải quyết ổn thoải để hai nam chính của truyện luôn phải chịu đau khổ. Tôi thương odin muốn khóc, sự chung thủy của chàng...lần cuối cùng khi frigg tự cho phép mình được yêu loki như ena, khi frigg giả vờ yêu loki vì odin muốn cứu nàng tại cuộc đàm phán sau chiến tranh, sự biến mất hay đúng hơn là hi sinh của loki vì tình yêu của odin và frigg. Tất cả đều làm tôi cảm động muốn khóc.
5
979895
2015-12-25 12:41:30
--------------------------
293187
5676
Đọc trọn tập 3, có thể tổng kết lại là mình không hài lòng về truyện này chút nào, về cả bìa truyện, chất lượng giấy hay quan trọng nhất là nội dung. Khách quan mà nói, cốt truyện "Lời chúc phúc của Odin" không dở lại khá lạ nhưng nó không hợp với sở thích của mình. Cách hành văn của tác giả cũng mượt, hệ thống nhân vật đa dạng nhưng tệ một cái là nữ chính được xây dựng với tính cách quá vô vị. Dù rằng không nên đánh giá mức độ hay dở của một bộ truyện nếu chỉ xét trên tính cách nhân vật, nhưng đây lại là nữ chính, trung tâm của toàn bộ câu chuyện, đất diễn nhiều nhất. Mặt khác, một bộ truyện có thu hút được độc giả hay không lại phụ thuộc rất nhiều vào nam nữ chính. Chính vì thế, cần cân nhắc trước khi mua quyển này.
3
323629
2015-09-08 07:52:23
--------------------------
266552
5676
Chất liệu giấy đã tốt hơn so với cuốn 1 & 2...
Bộ này ngược tâm quá ...

Khi đọc xong mình cảm thấy tiếc quá, Frigg & Odin lại về với nhau sau bao ngược tâm đến mức đau tim.... Nhưng còn Loki & Ena thì sao? Cảm thấy hụt hẫng quá. Khi đọc cuốn 1 thì mình thấy Loki dần dần yêu Ena chứ không phải là tái sinh của Frigg, nhưng khi đọc đến cuốn này thì không rõ người Loki yêu là ai. Và khi Loki mất tích... là chàng đã chết?  Ena yêu Loki đến thế, thật tội nghiệp cho nàng. 
Còn về Sif, tuy nàng không yêu ai đàng hoàng, quen hết người này đến người khác, nhưng nếu là bạn thì nàng thật sự là người tốt, tuy Frigg đã không còn chung tộc với nàng, và còn là tộc thù địch, nhưng nàng vẫn giúp đỡ và bảo vệ cho người chị em của mình. 

Đọc bộ này thì thích là thích cách viết của tác giả, những đoạn miêu tả phong cảnh hay sự kiện cảm thấy rất hùng vĩ, rất hay .... chỉ là không thích kết truyện mà thôi. Có vẻ còn thiếu gì đó.....
4
504684
2015-08-14 19:58:17
--------------------------
259177
5676
Lời Chúc Phúc Của Odin - Tập 3 tựa đề Nỗi nhớ nhung của Odin. Mình mua cả 3 tập và cảm thấy truyện rất hay.Tập 1 thì nói về tình yêu Lando và Ena ngay từ khoảnh khắc đầu gặp nhau Lando đã trao trọn tình yêu của mình cho Ena.Tập 2 thì nói về tình yêu của Odin và Frigg. Và cuối cùng tập 3 kết thúc với 1 cái kết mình rất thích. Một câu truyện về thần thoại mang nhiều ý nghĩa nhưng lối viết văn rất sâu sắc lôi cuốn người đọc. Mình sẽ ủng hộ tiki nhiều hơn
4
483857
2015-08-09 10:13:13
--------------------------
207008
5676
Truyện ngôn tình rất ít nói về thể loại Thần thoại Bắc Âu, đây là 1 trong những tác phẩm hiếm hoi viết về thể loại này khiến mình cảm thấy rất hứng thú khi đọc truyện. về cách miêu tả nhân vật thì khỏi phải nói, tác giả miêu tả 1 cách hoàn mỹ và quá đẹp làm mình vừa đọc vừa liên tưởng đến không gian thần thoại huyền ảo đẹp đẽ, trong đó có những nhân vật là thần nhưng lại có tính cách, cách sống thậm chí shopping như con người nên rất gần gũi ^^. Tình tiết truyện trải dài qua nhiều kiếp. Quyển 1 hoàn toàn nói về tình yêu của Frigg và Loki làm mình cũng nhầm tưởng Loki mới là nam chính của truyện, sang đến quyển 2 là chuyện tình tay 3 của Frigg - Loki - Odin. Quyển 2 nói về buổi đầu gặp và yêu nhau của Odin và Frigg, chỉ tiếc đến cuối truyện Odin lại có chút hiểu lầm với Frigg. Còn Loki tuy là nguyên nhân gây nên chiến tranh và chia rẽ tình cảm của Odin và Frigg nhưng mình cũng cảm thấy không ghét Loki lắm (Có lẽ Quyển 1 tác giả miêu tả tình cảm của Loki dành cho Frigg chân thành nên ko ghét được ^^). Giờ mình mới chuẩn bị đọc Q3 nhưng nghe tựa "Nỗi nhung nhớ của Odin" đã thấy sự chung tình của chàng. Bù lại 2 quyển trước nói về 2 người không nhiều lắm, chưa đã.
Truyện hay mỗi tội khi nhận được truyện mình thấy bìa sách bẩn quá hic. Không biết truyện đã cũ hay do bên đóng gói đây :(
Dù sao vẫn cho 5 sao vì nội dung truyện :)
5
270010
2015-06-11 11:00:20
--------------------------
133773
5676
Tác giả đã cho mình mở rộng tầm mắt khi chứng minh cho mình thấy một tình yêu cao đẹp là một tình yêu không chỉ sẵn sàng hy sinh tất cả vì người mình yêu mà còn dám đem tính mạng của mình ra mà đặt cược . Lối hành văn hấp dẫn tràn đầy sống động cảm xúc tác giả đã vẽ ra một thế giới chứa đầy những điều mầu nhiệm mà ta có thể hình dung được qua những câu từ miêu tả chi tiết của tác giả . Nội dung thì hấp dẫn không còn gì để nói . Các yếu tố hấp dẫn nối tiếp nhau khiến cho mình càng đọc càng thấy hay không thể ngừng lại được . Một câu chuyện trên cả tuyệt vời , rất cám ơn tác giả đã đem đến cho mình một câu chuyện hay và hấp dẫn đến như vậy
5
337423
2014-11-06 20:21:22
--------------------------
