455303
7133
Cuốn sách là tập hợp những mẩu chuyện giản dị mà vô cùng sâu sắc. Nó giúp tôi thay đổi cách sống chưa tốt trước đây của mình. Cảm ơn tất cả những người đã viết cuốn sách này, vì họ giúp tôi sống đẹp, sống tốt hơn: giúp tôi trân trọng hơn những gì mình đang có tuy mọi thứ không phải lúc nào cũng hoàn hảo bởi khi mất đi ta mới biết chúng vô cùng quan trọng, giúp tôi biết tự đứng lên sau mỗi lần vấp ngã bởi chông gai thử thách sẽ tôi rèn ta, giúp tôi yêu thương mọi người xung quanh bởi "sống là cho đâu chỉ nhận riêng mình". Cuốn sách chính là món quà vô giá đối với tôi.
5
385681
2016-06-22 14:50:30
--------------------------
206208
7133
Người đọc có thể nhận thấy họ trong chính những câu chuyện trong cuốn sách kỳ diệu này. Cuộc sống tuy muôn màu muôn vẻ nhưng nếu biết tận huởng, trân trọng những gì bạn đang có thì đó chắc chắn là một thiên đường rực rỡ, hấp dẫn vô cùng đặc biệt. Với hơn 50 mẩu chuyện lớn nhỏ, cuốn sách giúp mình nhận ra những giá trị đích thực, sâu sắc mà bình dị của cuộc sống này. Đó như một thông điệp đầy tính nhân văn, triết lý cao đẹp về lẽ sống của mỗi con người. Mình hy vọng cuốn sách cũng sẽ giúp ích cho nhiều bạn như đối với mình.
4
561261
2015-06-09 07:42:57
--------------------------
194772
7133
Cuốn sách là tập hợp những mẩu chuyện giản dị mà vô cùng sâu sắc. Đọc để hiểu được rằng cuộc sống không phải lúc nào cũng như mình mong đợi. Sẽ có những lúc khó khăn vấp ngã hay đôi khi đó chính là số phận mà mình phải chấp nhận để vượt qua. Điều quan trọng nhất sau tất cả chính là phải biết đứng dậy và trân trọng hiện tại của mình. Cuốn sách đã tuyển chọn thành công các câu chuyện rất hay và mang đến một thông điệp đầy ý nghĩa: Hãy biến cuộc sống thành quà tặng cho chính mình.
5
433873
2015-05-11 19:39:56
--------------------------
162123
7133
Cuộc sống là 1 bức tranh muôn màu muôn vẻ, muôn hình vạn trạng với những gam màu khác nhau. Đọc "Quà tặng cuộc sống" trong bộ Tủ sách sống đẹp, tôi cảm thấy trân trọng hơn những gì mình đang có tuy mọi thứ không phải lúc nào cũng hoàn hảo như mong muốn. Thật vậy, cuộc sống là món quà vô giá mà Thượng đế ban tặng cho mỗi người, món quà ấy không ai giống nhau cả nhưng có lẽ đều đáng để chúng ta nâng niu và gìn giữ. Và quyển sách này là món quà tinh thần rất ý nghĩa và thấm đượm giá trị nhân văn cho tất cả mọi người.
5
131327
2015-03-01 16:01:24
--------------------------
