166631
5089
Tôi tình cờ đọc lướt một vài trang của "Mắc duyên bút mực" trong Fahasa và quá thích, rất rất thích. 

Đây là quyển sách viết về những tác giả, những nhà văn, nhà thơ, hội họa trong làng nghệ thuật của Việt Nam. Tôi thích nhất chương viết về nhà thơ Trúc Thông và họa sĩ Đoàn Văn Nguyên, tôi ấn tượng cách tác giả trò chuyện, tìm hiểu về từng nhân vật một cách thấu đáo, chi tiết. 

Ngoài ra thì tác giả Nông Hồng Diệu - một cây bút xuất sắc của báo Tiền Phong còn gom góp những câu thơ, những nhận xét khác của những nghệ sỹ khác dành cho nhân vật mình đang viết về. Hơn nữa là trong sách còn có những bức họa về chân dung của những nghệ sỹ được viết đến trong sách. 

Sách được in ấn rõ đẹp, còn được tặng một bookmark rất dễ thương nữa ^^! Khi mua sách rồi về nhà lên Tiki tôi mới thấy giá Tiki rẻ hơn, rút kinh nghiệm lần sau cứ mua trên Tiki cho chắc ^^!
5
50283
2015-03-13 00:03:38
--------------------------
