390699
5410
Mình đã đọc cuốn sách này. Nói chung nặng về lý thuyết, ít ví dụ và thực hành. 
Cuốn này nếu ai không kiên nhẫn thì sẽ không đọc hết. Nội dung không hay lắm, không nên mua nếu có ý định muốn học hỏi, nên mua nếu muốn lấy dữ liệu làm luận văn...
Nếu muốn ủng hộ tác giả Việt thì có rất nhiều sách hay để các bạn lựa chọn. Giá 69.000 đồng cũng không hợp lý lắm với một cuốn sách kém hấp dẫn. Bạn nào đang định mua thì hãy xem xét và lựa chọn thêm.
1
625363
2016-03-04 12:06:49
--------------------------
164759
5410
Mình chưa đọc cuốn sách này, nhưng đã coi qua clip giới thiệu của tác giả. Một con người đầy nhiệt huyết và năng nổ. Hi vọng rằng nội dung cuốn sách cũng thể hiện điều này.
Một phần mình đặt mua cuốn sách này cũng vì muốn ủng hộ các tác giả Việt. Hi vọng các tác giả có thể dành nhiều tâm huyết hơn cho các tác phẩm của mình, để một ngày nào đó, mình đi mua sách không còn chăm chăm vào các cuốn Best Seller của nước ngoài nữa.
Chủ đề tái cấu trúc cũng đang rất nóng ở Việt Nam. Hi vọng nội dung cuốn sách sẽ giúp mình cập nhật thêm thông tin bổ ích
3
558456
2015-03-08 18:28:05
--------------------------
