162259
7455
Sách viết về Lỗ Tấn và những bạn bè gắn với tuổi thơ của ông. Đọc sách này, ta biết một phần tuổi thơ của Lỗ Tấn, hiểu được cách giáo dục từ nhỏ và hoàn cảnh sống của ông, từ đó thêm quý trọng con người có sự nghiệp văn chương vĩ đại sau này của trung quốc. Cơ mà, nếu chưa biết mà đọc sách này thì thấy thú vị, chứ biết rồi thì thấy nhàm, tại vì trong này gồm 2 câu chuyện, mà gần như chỉ là chuyển từ ngôi thứ nhất các tác phẩm văn học tự trần thuật lại của lỗ tấn sang ngôi thứ 3 mà thôi. Mà một trong 2 truyện đó thì có trong sách giáo khoa rồi, cho nên mình chỉ thấy nó tàm tạm, mua về đọc một lần rồi thôi. Thú thật đọc tác phẩm ở ngôi thứ nhất vẫn hay hơn.
2
403246
2015-03-01 20:46:07
--------------------------
