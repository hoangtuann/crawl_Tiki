197949
6152
Thật sự thì trước khi mua ở Tiki thì mình đã đọc qua tập 1 ở bên ngoài,đọc xong thấy hay nên tiếp tục đọc tập 2, lướt Tiki thì thấy giá khá hợp lý nên đã đặt mua. Tào tháo là nhân vật gây tranh cải nhất trong lịch sử Trung Quốc, một con người vừa có dã tâm,vừa có hùng tâm, là một con người mang nhiều mặt nạ,nhưng chỉ có như vậy thì ông mới có thể đặt nền móng cho đế nghiệp thống nhất Trung Quốc của nhà Tân sau này. Một người mưu trí,có tầm nhìn và khả năng dùng người của ông thật đáng khâm phục. Qua 2 tập thì tác giả đã khắc hoạ hơn rõ nét con người Tào Tháo,nhưng bước thăng trầm trong cuộc đời đầy biến động của ông. Quyển sách rất hay và có những chi tiết mà trong tác phẩm Tam quốc Chí chưa đề cập ra. Cảm ơn Tiki.
5
71903
2015-05-18 20:37:31
--------------------------
168809
6152
"Thà ta phụ người trong thiên hạ chứ không để người trong thiên hạ phụ ta" là câu nói nổi tiếng của Tào Tháo mà đến nay vẫn còn gây rất nhiều tranh cãi về ý nghĩa của nó. Và chủ nhâm của nó, thiên tài quân sự Tào Mạnh Đức thì bị các học giả ngày xưa chê bai, khinh khi và coi ông là biểu tượng của sự vô liêm sỉ và bỉ ổi. Tôi đã đọc tập 1 của bộ sách này và có thể kết luận rằng: điều đó đúng nhưng chỉ đúng một phần. Một nửa con nghười còn lại của ông là một con người tràn đầy lòng yêu thương cho người dân thiên hạ. Tôi thích hình ảnh 2 mặt của Tào Tháo trên bìa của cuốn sách. Hình ảnh đó đã diễn tả 1 cách rõ nét nhất con người của nhân vật đầy tranh cãi này.
5
387632
2015-03-17 10:38:54
--------------------------
159640
6152
"Trị thế năng thần, loạn thế gian hùng." Đây là câu nhận xét về Tào Tháo của nhiều nhân sỹ Tam Quốc đương thời và cũng nói lên vận mệnh của Tào Tháo. Nếu là vào thời thịnh trị, Tào Tháo sẽ là bậc hiền thần trung nghĩa, là người cùng với vua cáng đáng mọi việc, xây dựng một triều đình vững mạnh. Nhưng tiếc thay, Tào Tháo lại sinh ra vào thời loạn, nên ta chỉ thấy được cái gian hùng của Tào Tháo. Mượn danh vua để bức chư hầu, tuy mang tiếng là phản nghịch, nhưng vẫn là danh chính ngôn thuận, cũng nhờ đó mà thiên hạ không bị chia năm sẻ bảy, chiến sự liên miên không dứt. Nếu không có Tào Tháo, thì có biết bao nhiêu kẻ xưng vương xưng đế, đại cục thiên hạ ắt hẳn sẽ không còn chỉ là Tam Quốc, mà chia cắt không chỉ 100 năm. Vậy mới nói, ở con người Tào Tháo, công tội luôn đi đôi với nhau, trong công có tội, trong tội có công.
5
291067
2015-02-18 08:19:27
--------------------------
