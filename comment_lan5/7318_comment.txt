568421
7318
Theo mình đây là truyện có dàn nam phụ tuyệt vời nhất, đã khóc hết nước mắt khi Thiên Chi Tà ra đi và hồn phách bị tiêu tán, có những tình yêu mà con người ta không nhận ra mình đã yêu tự bao giờ, chỉ nguyện một lòng, nguyện đem những gì tốt nhất lại cho người kia. Mặc dù đây là người muốn nàng nhập Ma, vì nàng nhập Ma mà làm mọi chuyện, vì nàng mà phản bội lời thề với Ma thần, để rồi có một kết cục như vậy. Cõ lẽ đêm được nàng rúc vào trong lòng, bản thân cuối cùng cũng đã nhận ra mình đối với nàng là tình hay sứ mệnh của một trung thần một lòng vì Thiếu quân mà thốt ra hai chữ xin lỗi, để lại bộ y phục trắng tinh tựa tuyết như khi còn ở Nam Hoa sơn, một Mộ Ngọc thủ toạ đệ thử, không nhiễm một tí ma khí hay dơ bẩn nào nơi Ma cung.

Lúc thành ma rồi, Trọng Tử vẫn có một người vì nàng không tiếc thanh danh, liên tục dùng địa vị của mình mà năm lần bảy lượt bảo vệ nàng. Chính nàng khi gặp lại hắn cũng hồi tưởng về chàng trai kiêu ngạo, phong lưu, thoáng đãng khi xưa của Thanh Hoa cung mà tự ca thán chính mình đã thay đổi. "Ta không thể bảo vệ nàng nhưng chỉ cần ta còn sống trên đời này, ta tuyệt đối không bao giờ để bọn họ động vào nàng" "nàng có biến thành bộ dạng nào đi chăng nữa thì ta cũng mặc kệ..." "Tiểu nương tử...". Chàng thiếu niên ấy đã chấp nhận buông bỏ mọi thứ, vì người chàng không nghĩ tới, càng lại là người nằm trong tim của nàng.

Lúc thành ma rồi, nàng vẫn còn có một Tần Kha nguyện hi sinh bản thân mình vì nàng, vì nàng mà không tiếc mạng của mình, vì nàng làm những việc thầm lặng, chỉ tiếc là... dường như mọi cố gắng đó vẫn không xoay chuyển được số mệnh. "Tiên sơn, con cá khổng lồ, mặt biển rộng, tên tiểu công tử cao ngạo trong bộ y phục màu tím kiêu sa đã từng nghiêm mặt mắng nàng là "nha đầu xấu xí" ", "nàng và hắn đáng lẽ phải rất gần gũi, nhưng không hiểu sao, càng ngày khoảng cách ấy càng xa", "hắn đã từng khuyên nhủ nàng...hắn đã từng bất chấp thương thế, nắm chặt bàn tay nàng, khuyên nàng nhẫn nhịn chờ đợi hắn, hắn nhất định sẽ cứu nàng ra..." "Nhưng nàng đã không đợi được đến ngày đó". Ngay cả kiếp sau của Tần Kha, vẫn mang tính cách đẹp như vậy từ kiếp trước, dù cho có đầu thai bao nhiêu lần đi nữa, tựa như sẽ không bao giờ thay đổi.

Chuyện tình của Âm Thuỷ Tiên và Tuyết Lăng tiên sinh như một hồi chuông làm thức tỉnh Trọng Tử, trong câu chuyện đó, người nữ không tiếc tu vi của mình mà kéo dài sự sống cho một người phàm trần, mà khi chuyển kiếp đầu thai, hắn ta từ lâu đã quên nàng là ai, quên nàng là vì ai mà nhập ma. "Kiếp trước nàng vì ta mà nhập ma, Kiếp này ta vì nàng mà thành tiên" thế sự xoay vần, có lẽ khi còn là một con người, hắn vẫn luôn muốn tu tiên, mà nàng không đồng ý, vì nàng đã nhập ma rồi, một khi hắn tu thành tiên, thân phận giữa họ lại càng khó xử. Nhưng nàng không biết rằng, hắn vẫn giữ một chấp niệm: khi ta thành tiên rồi, sẽ nhớ được chuyện của kiếp trước, sẽ nhớ nàng là ai...

Những uẩn khúc của Ma thần Vạn Kiếp, ai cũng tưởng hắn phản bội sư môn, vì tham mà cướp Nghịch Luân kiếm, mà giết 3000 đệ tử của Tiên môn. Nhưng cũng không ai nghĩ rằng, mọi sự hắn làm, đều là vì sư môn, không màng bách tính, là vì tiên mà nhập ma, để rồi khi nhận ra, đã không thể quay đầu lại. "Hỏi ai còn nhớ không, xưa kia có một vị thủ toạ đệ tử của Trường Sinh cung, y phục trắng tựa tuyết, mái tóc dài đen nhánh, vang danh thiên hạ, lẫy lừng một cõi giang sơn...."
5
458455
2017-04-09 18:41:17
--------------------------
526533
7318
Tôi là fan HTC nhưng khi đọc TT tử vẫn cảm thấy TT là một tác phẩm rất xuất sắc. Nếu HTC đề cao cách hành văn, nội dung thì cảm xúc ở TT đc nâng lên đến mức cao trào. Tôi rất ấn tượng với tác phẩm TT, mong các fan HTC có thể đón nhận TT và cảm nhận đc sự khác biệt giữa hai tác phẩm 
5
1177127
2017-02-16 23:40:52
--------------------------
517141
7318
Lần đầu đọc truyện Thục Khách, bản thân trước nay là 1 con mê sư đồ luyến tiên hiệp. Trước kia rất thích Hoa Thiên Cốt, nhưng đọc xong Trọng Tử mới thấy mình thích Trọng Tử hơn 100 lần HTC, Tình yêu của Trọng Tử đối với Lạc Âm Phàm có lẽ trải qua thêm mấy kiếp nữa cũng khó đổi, như tình yêu của Tần Kha và Trác Hạo đối với nàng, rất khó phai. Ấn tượng cũng là dàn nam phụ, thích cũng là dàn nam phụ. Đầu tiên là Tần Kha, sau là Trác Hạo, thích nhất 2 nam phụ này (có lẽ vì mik thuộc kiểu thích loại quen nhau khi còn nhỏ, tình yêu trẻ thơ) ấn tượng vs Thiên Chi Tà và Vong Nguyệt, khâm phục và làm cho mình đau lòng nhất là Sở Bất Phục, thần tiên ca ca. Cảm thấy Lạc Âm Phàm quá nhiều gánh nặng trên vai, không thể một lòng với Trọng Tử (y như cha Bạch Tủ Lạnh -_-)
5
1414385
2017-02-01 14:22:31
--------------------------
487885
7318
Tôi là fan cuồng của truyện & phim Hoa Thiên Cốt, sau này nhân dịp Tiki giảm giá sốc bộ Trọng Tử nên mua về xem thử vì biết 2 truyện này có mạch truyện gần giống nhau. Thực sự không thể so sánh HTC & TT truyện nào hay hơn vì cách hành văn của 2 tác giả & tình tiết trong truyện hoàn toàn khác nhau. Nếu ai đã từng yêu mến HTC cũng nên đón nhận Trọng Tử vì TT thật sự là một tiểu thuyết xuất sắc.

Đi xuyên suốt 3 tập truyện dày của Trọng Tử, tôi thấy như sống thật trong thế giới thần tiên đẹp đẽ & lung linh. Các nhân vật được xây dựng rất tinh tế, ngoài nam chính Lạc Âm Phàm, tôi thật sự rất ấn tượng với Mộ sư thúc - Thiên Chi Tà. Chỉ tiếc là đất diễn của Thiên Chi Tà quá ít ỏi, lúc đầu là Mộ Ngọc, về sau là Thiên Chi Tà, cái chết cũng quá nhanh chóng nhiều tiếc nuối. Nếu TT không đến được với LAP thì Mộ sư thúc cũng là một lựa chọn ấm áp trong thế giới nhiều người luôn muốn ruồng rẫy ràng, đẩy nàng đi thật xa.

Những trường đoạn phân tích nội tâm của TT & LAP rất hay, rất ám ảnh dưới ngòi bút của Thục Khách. Phải dùng đến 2 chữ "tan nát" khi ở chương "tiên ma gia đấu", LAP đã dùng kiếm đánh bại TT để cả 2 đồng quy vu tận, Thục Khách đã mô tả "đó là trái tim đã chết", mình hiểu được nỗi đau sâu sắc của LAP. 

TT rất hay, lối hành văn quyến rũ, những khung cảnh thần tiên tuyệt trần, cảnh giao đấu, cảnh tuyết trắng vô cùng thương tâm, tạo nhiều cảm xúc của độc giả.

Mối tình Tuyết Lăng - Âm Thuỷ Tiên cũng là mối tình sư đồ dễ thương trong sáng. TT xứng đáng trở thành bộ tiểu thuyết sư đồ luyến 5 sao cho những ai yêu mến câu chuyện thần tiên huyền huyễn.
5
528917
2016-10-28 06:22:11
--------------------------
463941
7318
Đến cuối truyện, Trọng Tử và Lạc Âm Phàm đã đến với nhau xem như là một kết cục trọn vẹn nhất, nhưng tôi vẫn cảm thấy nhớ thương cho những số phận đã đi qua cuộc đời họ. Rất nhiều người đã bỏ lỡ hạnh phúc của mình, mất đi và tan biến, thậm chí đầu thai không còn bất kỳ ký ức nào, hoặc tiếp tục bước đi trên con đường của mình vĩnh viễn không cần quay đầu lại.... Tần Kha, Trác Hạo... Vãn Kiếp, Yên Chân Châu, Thiên Chi Tà.... đời họ mất đi, rốt cục họ đã làm những gì? Có những điều họ hối tiếc, cuối cùng ai sẽ thay họ hoàn thành tâm nguyện cuối cùng? 
4
998926
2016-06-29 20:06:11
--------------------------
375751
7318
Có khá nhiều người từng so sánh cho tôi nghe Mệnh phượng hoàng, Hoa thiên cốt và bộ Trọng tử này với nhau. Hai quyển kia quá xuất sắc, vì vậy tôi không chút chần chừ mua ngay bộ này, và quả là Trọng tử không hề làm tôi thất vọng, nếu không muốn nói là hay hơn hẳn cả 2 bộ kia. Cùng môtip về thầy trò tu tiên giống Hoa thiên cốt, nhưng cốt truyện của Trọng tử đào sâu hơn, chi tiết hơn và cũng ám ảnh người đọc hơn. Tác giả viết rất logic, không bị sự gượng ép thường thấy của loại văn sư đồ luyến. Cốt truyện thì cực kỳ hấp dẫn, lôi cuốn.
4
105666
2016-01-29 07:22:39
--------------------------
368611
7318
Với một dàn nam khủng hoảng như thế nhưng những nhân vật nữ phụ thì ôi thôi,rất chán, và rất rất ngán, tầm thường hơn cả bình thường. Một Văn Linh Chi đanhđá nhỏ nhen; một Mẫn Tố Thu cuồng loạn ghen tuông như Hoạn Thư; ngay cả mộtTrác Vân Cơ tiên tử yêu đơn phương Lạc Âm Phàm cũng không có ấn tượng sâu sắc,lúc chết đi cũng chẳng gây đau xót tí nào. Đến nỗi Cung Khả Nhiên, người yêu thanh mai trúc mã của Sở Bất Phục cũng khá nhàm, chả hiểu chị ấy có gì đặc sắc mà anh ý phải hy sinh như vậy mà chị Cung cũng chả hiểu gì anh ý, bực cả mình,không yêu để Mọt yêu cho. Chỉ có chuyện Âm Thủy Tiên nửa tiên nửa ma còn có chút màu sắc. Biết sao được, con người lúc nào cũng có điểm mạnh điểm yếu mà.Thục Khách đã tạo được những nhân vật nam phụ phi thường như vậy thì chắc không có nhân vật nữ nào đua kịp. 
5
1108819
2016-01-14 12:25:24
--------------------------
365568
7318
Phần cuối cùng là cái kết đầy thương cảm , hối tiếc về một sự riêng biệt với kiểu chia rẽ hai bên người cùng khổ tạo nên sự hòa hợp rất ăn ý , bởi ai cũng phải trải qua lâm ly , khổ nạn mới về sống chung được với nhau nữa , vậy mà chàng trai phải mất đi người con gái mà chàng qua nhiều kiếp vẫn không thể thoát được số phận bi ai trần gian , quá nhiều sự bất chấp đến khi đòi lại được cũng không có làm ta cảm thấy hạnh phúc .
4
402468
2016-01-08 11:36:25
--------------------------
333538
7318
Muốn cho 5* lắm,khổ nỗi mình không có hứng với tình sư đồ luyến...
Nhiều người thích Hoa Thiên Cốt hơn là Trọng Tử,bởi Hoa Thiên Cốt hoa lệ hơn,đặc sắc hơn,phọng phú hơn.Không thể phủ nhận điều đó nhưng mình lại thấy,đọc xong Trọng Tử ,mình lại thấu hiểu cuộc sống hơn,cảm nhận những giá trị vĩnh hằng ở một tầng nghĩa sâu hơn,khác hơn.
Bỏi mình hiểu tâm trang của Trọng Tử,của Lạc Âm Phàm.Một trong những tình huống mình hả hê nhất truyện là khi Lạc Âm Phàm đọa ma.Hay như tâm trạng của Trọng Tử sau ba kiếp làm thầy trò với vị tiên cao quý bậc nhất Tiên giới này...
Điểm cộng cho truyện là phần ngoại truyện sủng vô cùng,mình rất thích.Truyện không có khuyết điểm nào đáng nói,ngoài việc hơi dài,lết được đến cuối cũng là cả một kì công
P/S:mình thấy Trọng Tử cuối truyện sao cư phải về bên nam chính nhỉ?Về vớiThiên Chi Tà cũng đươc,đó là một vị Thần bao dung với nàng.tác giả cũng thật...
4
487743
2015-11-07 23:44:56
--------------------------
331819
7318
Thục Khách nói đời người không như ý, nên cô gửi vào trang sách những cái kết viên mãn. Nhưng đời người cái gì là viên mãn. Cuối truyện, cô cho nhân vật chính được ở bên nhau, nhân vật phụ có kiếp sau của họ, ai cũng có đôi có cặp, nhưng như thế là xóa sạch được những đau khổ trước đó hay sao?

Mình không thích nhân vật chính trong truyện này. Lạc Âm Phàm thì quá cứng nhắc, những đoạn miêu tả tình cảm với nữ chính thì mình có cảm giác Thục Khách viết chưa sâu, mình không cảm nhận được cái gọi là "khắc cốt ghi tâm" như tác giả nói. Nữ chính thì chưa đủ quyết đoán, vẫn có nhiều lúc cư xử ngu ngốc, làm hại nhiều người khác, đặc biệt là dàn nam phụ.

Mình thích nhất Tần Kha. Yêu một người là như thế, âm thầm bảo vệ người ta đến tận cùng. Nhưng cuối cùng thì sao, đau khổ anh nhận hết về mình, vẫn phải chịu cái kết bi thảm như những nam phụ khác
2
403246
2015-11-05 09:37:02
--------------------------
320384
7318
Dù chết cũng không buông tha nàng, thực sự Lạc Âm Phàm khiến tôi rất ngỡ ngàng, trước dùng Tỏa Hồn Ti tự kìm hãm mình để cùng nàng “đồng vu quy tận”, sau đó lấy thân tuẫn kiếm, dùng Kính tâm thuật tịnh hóa cả 2, đến cả cơ hội sống cùng chẳng cho nàng. Quá sức… shock.. phải nói là shock… Ko phải trước nay chưa có cặp đôi nào sống chết cùng nhau, nhưng chắc chỉ có mỗi Lạc Âm Phàm mới thà kéo người yêu chết chung chứ ko cho nàng cơ hội bỏ đi. Cả đời vô tư, được có 1 cơ hội ích kỉ thì ích kỉ khủng khiếp. Sau này bên nhau rồi hễ nàng hờn dọa có tí xíu, cũng phán câu xanh rờn là nàng mà đi ta tự phá tan hồn xiêu phách tán, mà trước đó cũng phải cùng nàng hồn xiêu phách tán. Nghe xong đờ luôn… Ngày xưa vì Lục giới, chuyện giết nàng bẻ xương nàng làm không chút lưu tình, giờ ai dám xử tệ với nàng, chắc 
Cuối truyện xem như đoạn kết viên mãn, Tần Kha hay Mẫn Tố Thu gì đó cũng là tái sinh, sống cuộc đời mới, dù vậy tôi vẫn lưu luyến đại thúc Vãn Kiếp, Yên Chân Châu, Thiên Chi Tà nữa, rất buồn, họ chẳng còn có cơ hội để gặp lại nữa rồi… 
Vong Nguyệt Cửu U rất tốt, chẳng hiếm hoi gì có vị thần rảnh rỗi xem náo nhiệt như Vong Nguyệt, thích xoay chuyển kết cục 1 phen, cũng tốt, nếu ko có Vong Nguyệt, tiên giới không đại loạn, ai tốt ai xấu biết chừng nào thấy được!?
Phút cuối nhìn đám ngườu Mẫn Vân Trung, Ngu Độ, Hành Huyền biết mình làm sai, trách lầm người tốt, Trọng Tử cười tự giễu, tôi cũng thấy vậy, hại người ta cả đời rồi, đến cuối mới biết đó là sai, thực chẳng biết nên làm vẻ mặt gì với họ. tình cảm của nàng, con người nàng sống lẽ nào phải nhờ họ cho phép mới được quyền sống hay sao!? 
Tôi rất vui vì thấy Tuyết Lăng và Âm Thủy Tiên đã tái ngộ, không những thế Tuyết Lăng rất “tích cực” giữ đồ đệ, yêu thương nàng, chiếm giữ nàng, cuối cùng những người yêu nhau đã trở về với nhau!
5
516261
2015-10-11 07:46:40
--------------------------
153871
7318
Tình cờ biết được truyện gốc của Hoa Thiên Cốt chính là truyện này, tôi tò mò đọc thử. Dù cốt truyện cơ bản giống nhau nhưng  nội dung đa số đều có khác biệt rất lớn. Tình yêu  trong thiên truyện này, theo cảm quan cá nhân tôi mà nói là 4 chữ "khắc cốt ghi tâm", muốn nắm tay người đi đến cuối đời, nhìn nhân gian hỉ, nộ, ái, ố, bất kể miệng lưỡi thế nhân. Ước vọng đời người chỉ giản đơn như thế, nhưng cái hay của Thục Khách, là lại khiến nó khó khăn nhường ấy. Chỉ có điều, trong cả "Trọng Tử" và "Hoa Thiên Cốt", tôi đều có chút cảm giác nam chính không nổi bật bằng dàn nam phụ, đây có lẽ là một trong những điểm tôi thấy tiếc nhất. Chính vì hình tượng quá hoàn mĩ, trách nhiệm quá nặng nề, nên các anh không thể tự do thể hiện hay ghi điểm trong lòng người đọc nhiều bằng các nam phụ. :))  Nhưng cuối cùng vẫn thằng cuộc trong tình yêu. Khi tôi đọc hết chương cuối là một ngày nắng, có lẽ đâu đó có một Trong Tử, một Lạc Âm Phàm đang mỉm cười, đang hạnh phúc, đang tựa vào vai nhau ngắm nhìn thế gian... :)))
5
152579
2015-01-27 19:52:43
--------------------------
116372
7318
Sau gần 4 ngày, đã đọc xong 3 tập của"Trọng Tử". Nhưng cảm giác buồn bã, vương vấn vẫn còn tồn tại. Đôi lúc thầm nghĩ, hận và yêu, liệu có thể song hành không. Để rồi ngày kia được đoàn tụ, nắm tay nhau đi đến cuối con đường, không còn hận, chỉ yêu thôi là đủ. Trải qua hai kiếp, dù mang sát khí trời sinh, tình yêu ấy vẫn sâu đậm, biết thế gian khó chấp nhận mối quan hệ yêu đương loạn luân của sư đồ, nàng âm thầm chịu đựng, nhẫn nhục. Nhưng tình yêu nào ai biết, chỉ cần nhìn thấy chàng thôi, nàng đã rất hạnh phúc rồi. Nhận lấy mọi đau đớn, mọi nhục nhã, kinh bỉ; song, yêu - nàng vẫn yêu, duy chỉ có thêm hận càng khắc sâu, nhập ma nhưng bản tính vẫn thiện lương. Đối đầu với chàng chỉ thêm nhói đau. Về Lạc Âm Phàm - thân là tiên sư thủ độ cũng khó tránh khỏi hai chữ "ái tình", dù biết rằng chàng phải bảo vệ sinh linh bách tính, nhưng cũng vì vậy mà chàng đã ra tay giết nàng hai lần, để chợt nhìn lại quả ra chính chàng mới là ma chứ không phải nàng. Muốn bảo vệ bách tính, nhưng lại càng muốn bảo vệ nàng hơn. Chàng biết mình đã đem lại cho nàng bao cay đắng, đớn đau, vô dụng, ngay cả bảo vệ nàng, chàng cũng không làm được. Trái tim Trọng Tử như cùng chung nhịp đập với chàng, cùng đớn đau, cùng hận, cùng yêu, nhưng cũng thật đáng thương. 

Tôi nghĩ ngoại truyện là không cần thiết. Bởi khi Trọng Tử Cùng Lạc Âm Phàm biến mất, như thế đã đủ lắm rồi, không cần dài dòng, bởi, Lạc Âm Phàm quả không xứng đáng với tình yêu mà Trọng Tử dành cho chàng, chàng ta quá nhu nhược. Tôi chỉ thấy cảm động với Tần Kha, Trác Hạo. Yêu một người, mà người đó lại không yêu mình, hy sinh vì người đó. Qủa thật, tình yêu ấy không gì sánh bằng được. Ngay khi sắp chết, Tần Kha một lòng bái sư như vẫn muốn kiếp sau, chàng và Trọng Tử không gia nhập Tiên môn nữa, để cùng hạnh phúc, không phiền muộn điều gì nữa.

Đọc "Trọng Tử", tâm cũng nhận ra, là tiên hay ma, cũng không quan trọng. Quan trọng chính là con người phải biết hướng thiện. Trong tâm có ma, ắt là ma; trong tâm có tiên, ắt là tiên. Nhưng "Con người có tiên tính và ma tính, thì mới là con người (Thục Khách)", chúng ta cũng vậy, trong lòng nảy sinh ý xấu, nhưng tại bị lương tâm kìm hãm, đó mới là con người!
5
42987
2014-07-07 19:35:30
--------------------------
107693
7318
Trọng Tử bất hạnh. Nàng không được quyền từ chối nhận năng lực của cha mình là Nghịch Luân Ma Vương để lại sát khí trời sinh trên người mình nên cuộc đời nàng không được bình thường như mọi người mà lúc nào nàng cũng chịu bất hạnh nhiều hơn cả. Ngay cả người nàng tin tưởng nhất là sư phụ của mình Lạc Âm Phàm cũng phụ nàng. Bản thân Trọng Tử là một người tốt nhưng sự bất công của những người xung quanh trong Tiên môn như Mẫn Vân Trung, Ngu Độ làm cho nàng càng chịu thêm ấm ức. Lạc Âm Phàm lúc nào cũng nghĩ mình nghĩ cho nàng sao nàng không chịu hiểu nhưng thật ra lại làm trái ngược lại những điều đó. 
Tôi đã bật khóc khi Tần Kha quyết định hi sinh vì Trọng Tử. Giá như Tần Kha bên cạnh nàng nhiều hơn thì có thể nàng đã không yêu Lạc Âm Phàm. Tần Kha yêu không thể hiện đúng cách đến khi nói ra thì đã muộn. Trác Hạo chẳng qua vì mù quáng chứ tình yêu dành cho Trọng Tử không chân thật và hắn cũng chẳng dám hi sinh như Tần Kha. 
Bên cạnh đó tôi còn ấn tượng những nhân vật phụ như Thiên Chi Tà. Lúc nào hắn cũng nhẫn nhịn Trọng Tử, bên cạnh nàng, lúc hắn là Mộ Ngọc thì ân cần, quan tâm chia sẻ. Nếu Thiên Chi Tà không là nhân vật phản diện mà là nhân vật nam chính thì tôi nghĩ sẽ ấn tượng hơn cả Lạc Âm Phàm.
Còn lại dàn nhân vật nữ thì tôi không ấn tượng lắm. Mỗi nhân vật đều có khuyết điểm và nhược điểm riêng nhưng sau khi đọc xong tôi rút ra được bài học cho mình là: Đen chưa chắc đã đen. Cũng giống như hình bát quái vậy. Trong âm có dương, trong dương có âm. Dẫu có trong tối nhưng tuỳ vào bản thân chúng ta thì tối cũng sẽ thành sáng. Nghĩ là tốt chưa chắc đã tốt, xấu cũng chưa chắc không có tốt.
5
46568
2014-03-10 16:13:37
--------------------------
106110
7318
Tôi vừa hoàn thành tập cuối Trọng Tử, và hình ảnh Trọng Hoa vì nàng mà lấy thân tuẫn kiếm, vứt bỏ ngàn năm tu tiên, vứt bỏ tất cả danh vọng, quyền lực, vì nàng mà cam tâm nhập ma- thực sự khiến tôi ko cách nào quên được. Hình ảnh ấy, phân đoạn Áy sâu sắc đên nỗi nó xoá tan tất cả những hận thù, dằn vặt, những đau thương mà Trọng Tử vì chàng gánh chịu suốt ba kiếp. Không cần nàng tha thứ, chỉ cần nàng ở bên. Trọng Hoa ôm linh hồn Trọng Tử chui vào thân kiếm, nguyện mãi mãi vì nàng sống trong thân kiếm, chỉ cần có nàng ở bên.

Một kết thúc đẹp. Một kết thúc khiến mọi người thoả mãn. hàng trăm năm, mỗi đêm Trọng Hoa đều ôm Trọng Tử ngắm sao, và hỏi duy nhất một câu, tha thứ cho sư phụ, được ko. Và sau hàng trăm năm, cuối cùng linh hồn ấy cũng kỉm cười nhìn chàng và nói, Được. 

Trọng Hoa- Trọng Tử, một mối tình đẹp, đẹp đến mức vượt ngưỡng tiên, ma, nhân. Một bộ truyện sâu sắc và tràn đầy tính nhân văn, về nhân quả, về cuộc sống.
5
170765
2014-02-14 10:33:43
--------------------------
