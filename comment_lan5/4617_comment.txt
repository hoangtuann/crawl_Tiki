298040
4617
Lý Nhuệ là 1 tác giả có rất nhiều cuốn tôi thích, thích từ cái tên cho đến nội dung như Đất dày, Ngân thành cố sự, Ngàn dăm không mây. Ông chủ yếu viết về chính trị, lịch sử, xã hội nhưng cách viết không hề bị nặng về kiến thức, tư tưởng và góc nhìn rất khách quan. Thông qua câu chuyện của người này, người kia để tái hiện lại xã hội Trung Quốc cũ. 
Tôi rất tò mò về thời kì này nên đã không chần chừ mua ngay cuốn Mao Trạch Đông để tìm hiểu được thêm về các nhà lãnh đạo thời trước của Trung Quốc, quả là rất đáng học tập. Sách còn dày dặn, bìa cứng nữa. Đẹp lắm.
4
52328
2015-09-12 13:16:47
--------------------------
