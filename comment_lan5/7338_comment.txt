530257
7338
Mình nhớ là đã đọc cuốn này lần đầu từ tầm 7-8 năm trước, khi mình còn là sinh viên. Cuốn sách tâm huyết chứa đựng cả quãng thanh xuân của chị Dương Thụy ấy đã từ từ nhen lên trong mình tình yêu với châu Âu - vùng đất dịu dàng nhiều nét văn hóa xa lạ mà thật nhiều thương yêu. 
Đến khi được thực sự có những trải nghiệm của bản thân mình ở châu Âu, mình mới nhận ra là mình hạnh phúc nhường nào khi đi theo những chỉ dẫn và lần theo những tình cảm dạt dào của chị dành cho mảnh đất ấy. 
Lần này mình mua cuốn sách để tặng cho 1 người bạn qua bên đó du học, hy vọng bạn của mình sẽ có những kỷ niệm đẹp, dưới bầu trời châu Âu.
Rồi một lần nữa, cuốn sách lại gieo vào lòng mình những ước mơ và dự định mới, với châu Âu, vào một ngày không xa.
5
20161
2017-02-23 09:10:52
--------------------------
478041
7338
Mình đến với Dương Thụy từ quyển "Cung đường vàng nắng" và nghiện từ đó. Khi đọc tên đề "Venise và những cuộc tình gondola, mình đã nghĩ đây cũng là truyện dài giống "Cung đường vàng nắng" hay "Oxford thương yêu" và đặt mua. Tuy nhiên nghe các bạn nói cuốn này không hay mà mình thì đã đặt nên cũng hơi nản. Nào ngờ về đến nhà mở ra và đọc những trang đầu tiên mình mới biết tưởng không hay mà hay không tưởng, hay từ lời nói đầu của cuốn sách. Châu Âu qua giọng văn nhẹ nhàng của Dương Thụy hiện lên không lộng lẫy mà lại mang sắc đẹp hiền hòa, đầm ấm. Chìm vào những dòng chữ, mình cảm giác như chính bản thân cũng đang ở Châu Âu, ngắm nhìn Paris hoa lệ, Lìege bình yên, một Ý lãng mạn. Từ nhỏ đến giờ mình luôn muốn được đi xa nhưng không có điều kiện, vì vậy có lẽ những cuốn sách của Dương Thụy chính là lựa chọn đầu tiên khi mình muốn du lịch qua những trang sách. Thật sự cuốn sách quá tuyệt vời!!!
5
385471
2016-07-30 14:14:52
--------------------------
477779
7338
Lối viết truyện của Dương Thụy rất nhẹ nhàng và có gì đó rất bình yên, không phải là những cuộc phiêu lưu khó khăn, trắc trở, Dương Thụy viết về những chuyến viếng thăm, đi học, đi làm ở các nước Châu Âu, về kinh nghiệm du lịch, con người và văn hóa ở mỗi vùng đất mà chị đặt chân đến. Lời văn của chị đôi khi cũng rất hài hước và dí dỏm. Mình cảm thấy rất thích quyển sách, chỉ muốn được du lịch đến những nơi đó liền thôi. Vừa đọc vừa nhâm nhi tách cà phê thì vô cùng lý thú.
4
799707
2016-07-28 20:45:22
--------------------------
475765
7338
Về trình bày sách, mình thấy sách được đầu tư tốt. Hình thức sách đẹp, gọn gàng, chữ in rõ ràng. Giấy nhẹ.
Về nội dung, vốn mình ko cảm được văn Dương Thụy nhưng vì lí do công việc và học tập cần tiếp xúc và phân tích lý luận sách truyện nên mình buộc phải tiếp nhận. Truyện của Dương Thụy hầu như không có chiều sâu, cả cuốn này cũng vậy. Đọc trang này có thể biết nội dug trang sau. Motif đi theo và giới thiệu một nơi nào đó trên thế giới, tình yêu giữa người việt và người ngoại quốc vẫn không đổi. Hầu hết các tựa sách của Thụy đều gắn với 1 cái tên nước ngoài. Tôi đã đọc khá nhiều thể loại văn học truyện dài có viết về cuộc sống, văn hóa nước ngoài và có một số cây bút viết khá hay như Linh Lê. Riêng cuốn sách này và những cuốn khác, có lẽ phù hợp cho người muốn đọc chơi chơi, đọc hiểu tại chỗ ko phải suy ngẫm nhiều.
3
867619
2016-07-16 11:59:18
--------------------------
475128
7338
Mình đọc Trả lại nụ hôn trước khi biết đến Venise và những cuộc tình gondola. Cả 2 tác phẩm đều là về những chuyến du lịch của Dương Thuỵ, chủ yếu là Châu Âu qua cái nhìn vô cùng lãng mạn, xinh đẹp và hóm hỉnh. Tác phẩm mang lại cho mình rất nhiều cung bậc cảm xúc, vì là người không đi du lịch nhiều nên khám phá được nhiều địa danh trên thế giới một cách đẹp hết sức này thực sự rất có ích khi biết về lịch sử, người dân và những nét đặc biệt từ các vùng đất khác nhau. Đọc cuốn sách và cùng thưởng thức 1 ly trà hay ly cafe vào tiết trời nhẹ là điều tuyệt vời nhất đấy, tin mình đi!
4
743489
2016-07-13 01:45:32
--------------------------
469051
7338
Thích chị Dương Thụy từ lúc biết đến tác phẩm Oxford yêu thương. Nên gom hết tác phẩm của chị đọc thử. Trong Venise và những cuộc tình Gondola, chị  viết rất bình thường nhưng rất thú vị.Những chia sẻ, cảm nhận về cuộc sống nơi xa quê hương làm câu chuyên của chị trở nên thật gần gũi . Mỗi một vùng đất mà chị đặt chân tới, chị đều làm nó nổi bật lên một điều gì đó đặc biệt khiến người đọc phải nhớ mãi, chẳng hạn như  Montreux đẹp như một giấc mơ. Tiki giao hàng nhanh chóng, chuyên nghiệp, đóng gói cẩn thận, rất tin tưởng khi đặt hàng ở Tiki
4
294963
2016-07-05 22:11:03
--------------------------
449679
7338
Đây là tác phẩm đầu tiên tôi biết đến của Dương Thụy, lúc đầu tôi chưa biết Dương Thụy là ai, một nhà văn trẻ tuổi hay là đã lớn tuổi, cách viết văn phong của cô như thế nào?, có hợp với sở thích mình không?, nghĩ chắc đây là cuốn tiểu thuyết tình yêu lãng mạn thôi. Ban đầu tôi ấn tượng là cái chữ Venise một thành phố xinh đẹp, lãng mạn trên biển mà tôi rất yêu thích, luôn ao ước một lần được đặt chân đến đó để thỏa mãn cho cái tâm hồn thơ mộng của mình.Sau đó tìm hiểu thêm thì mới biết đây không phải là cuốn tiểu thuyết tình yêu mà là cuốn sách tập hợp những câu chuyện được ghi lại những điều Dương thụy đã trải qua ở nhiều nơi nhiều thành phố tại các nước Châu Âu trong thời gian còn là du học sinh của cô ấy. Khoảng thời gian đó của cô gái này thật sự rất thú vị, tuổi trẻ thật tuyệt nếu bạn được đi nhiều, trải nghiệm nhiều, học hỏi nhiều và ghi chúng lại để cất giữ và trở thành bài học đáng quý cho tuổi trẻ tươi đẹp trong cuộc đời. Qua tác phẩm Dương Thụy đã dùng ngồi bút tinh tế để khắc họa những nơi xinh đẹp trên các nước Châu Âu để đọc giả hiểu thêm nhiều cuộc sống nơi đó và sẽ là kinh nghiệm giúp ít nếu sau này bạn được đặt chân đến đó.
5
179975
2016-06-17 21:21:34
--------------------------
447344
7338
Lần đầu tiên mình biết đến Dương Thụy qua lời giới thiệu của bạn bè khi đang mải mê tìm sách thể loại du ký. Mình cũng tò mò và tìm mua về xem thử. Quả nhiên quyển sách không làm mình thất vọng khi ngồi nhăm nhi tách cà phê hòa vào bức tranh châu Âu hoàn hảo. Tác giả đưa mình vào những chân trời mới như Pháp, Thụy Sĩ, Hà Lan, Ý, v.v... Sau khi đọc xong mình thích mê tới nỗi muốn đi châu Âu ngay lập tức. Sách còn cung cấp cho mình nhiều thông tin thú vị để bổ sung vào hành trang du lịch của mình.
5
509425
2016-06-13 23:37:26
--------------------------
446427
7338
Ban đầu mình hơi thất vọng đôi chút vì mình đã rất kì vọng vào một mối tình thật lãng mạn nào đó như các truyện khác của Dương Thụy. Thì ra đây là những kí ức rất thật về Châu Âu. Cuốn sách này có lẽ sẽ là một cuốn cẩm nang rất thú vị cho những ai sắp ghé qua Châu Âu hoặc là nguồn cảm hứng cho một chuyến đi Châu Âu trong tương lai  không xa. Mình vẫn còn chưa đọc hết cuốn này, vẫn đang khám phá xứ "văn minh" một cách rất thật theo lăng kính của Dương Thụy.
2
1391093
2016-06-12 10:40:30
--------------------------
426138
7338
Mình được tặng cuốn Venise và những chuyện tình Gondola trong một đơn hàng của Tiki. Ấn tượng đầu tiên về cuốn sách là bìa nó đẹp dã man, Tiki còn gói rất cẩn thận nữa. Nghe tên của cuốn sách nình cứ nghĩ là nói về truyện tình nào đó giống Beloved Oxford, nhưng mở ra thì hoàn toàn bất ngờ, vì truyện không phải thể loại tình cảm mà là du kí, bên trong còn có ảnh nên chuyến đi Châu Âu hiện lên rõ ràng và đẹp dã man. Có ba nước mình rất thích là Ý Pháp Hà Lan nữa. Thực sự rất cảm ơn Tiki đã khiến mình biết đến cuốn sách này
4
1230012
2016-05-06 11:14:53
--------------------------
408000
7338
Cuốn sách được viết từ chính những trải nghiệm của Dương Thụy về Châu Âu. Thể hiện được cái nhìn thông thái nhưng rất đỗi tinh tế, lãng mạn nhưng không thiếu tính độc lập, mơ mộng mà vô cùng mạnh mẽ. Lối viết ấy như thể hiện chính con người của Dương Thụy- một con người trải nghiệm, thông thái, nhìn sự vật bằng đôi mắt của một cô gái độc lập, có vốn hiểu biết đó đây.

Đọc cuốn sách xong, trong lòng tôi chợt dâng lên một niềm khao khát muốn tới Châu Âu một chuyến. Một cuốn sách tuyệt vời, là một cuốn sách có thể khơi gợi trong lòng người đọc một cái gì đó. Dương Thụy và tác phẩm của chị đã làm được điều đó.
3
616425
2016-03-30 23:05:04
--------------------------
391503
7338
Mình rất thích Châu âu sau khi đọc quyển sách này. Với cái nhìn rất riêng của chị Dương Thụy Châu âu huyền ảo và quyến rủ that sự với những mối tình dành cho Châu âu. đọc tác phẩm bạn được hòa mình vào chuyến du ngoạn châu âu thú vị không quá hào nhoáng nhưng rất thú vị. Giọng văn nhẹ nhàng lôi cuốn của chị làm ta không thể nào bỏ sách xuống khi đã đọc. Nếu bạn nào thích và đam mê châu âu như mình thì hay đọc tác phâm. Bạn sẽ có một chuyến phiêu lưu thú vị đấy
4
581993
2016-03-05 15:11:36
--------------------------
389920
7338
Mình mua Venise và những cuộc tình Gondola là vì thích chị Dương Thuỵ  từ Oxford thương yêu. Khi nhận sách mình hơi thất vọng khi thấy giấy hơi xốp. Tuy nhiên, bìa lại khá đẹp. Đây không phải những câu chuyện tình yêu đẹp và lãng mạn mà là những trải nghiệm nơi trời Âu - một giấc mơ không của riêng ai. Nội dung khá mới và lạ lẫm với mình. Có thể gu của mình không hợp với thể loại du lí thế này nên thấy không được cuốn hút. Nếu ai chỉ thích đọc truyện có mạch truyện và nội dung rõ ràng thì nên cân nhắc.
3
973897
2016-03-02 23:23:02
--------------------------
380722
7338
Cuốn Venise Và Những Cuộc Tình Gondola như là một cẩm nang hành trình du lịch vòng quanh châu Âu. Đặc biệt với lối viết của tác giả Dương Thụy thì mình càng thêm háo hức, đón đọc cuốn sách này hơn. Mình say đắm mọi thứ mà chị kể, chị miêu tả, sống động và nhẹ nhàng lôi cuốn. Qua cuốn sách, mình lại thêm phải lòng nước Ý, một chút lãng mạn và rất đỗi say mê. Với mình, đây có thể là một trong những vật không thể thiếu trong túi khi du lịch ở châu Âu ^^ 
5
181063
2016-02-15 18:42:50
--------------------------
376717
7338
Mình rất thích đọc sách thể loại du kí của Dương Thụy, sách tràn ngập thông tin về nơi mà cô đến ngoài ra những cảm xúc của Dương Thụy qua những chuyến đi cũng hết sức ngọt ngào pha chút thơ ngây và niềm vui thuần khiết của việc được đi lại khám phá những đất nước mới, những thành phố mới.
Trong chúng ta sẽ có những người không có cơ hội được đi đây đi đó nhiều thì đây cũng vẫn là một cuốn sách đáng đọc để chúng ta biết thêm về nhiều nơi trên thế giới và đặc biệt là châu Âu lãng mạn qua mắt nhìn mơ mộng của Dương Thụy
4
306081
2016-01-31 17:15:57
--------------------------
375286
7338
Tôi thích các tác phẩm của Dương Thụy, hầu hết những tác phẩm của chị tôi đều đọc rồi. Ấn tượng nhất là tác phẩm Oxford Thương Yêu và Nhắm Mắt Thấy Paris. Sau đấy tôi đọc đến tác phầm Venise và những cuộc tình Gondola này thì càng thích các tác phẩm của Dương Thụy hơn nữa. Là một người hâm mộ thể loại du ký nên sức hút của quyển sách này với tôi là rất lớn, nó mang lại cho tôi nhiều kiến thức từ nhiều nước tác giả đã đi qua, mang lại niềm hứng khởi muốn độc hành đi du lịch bụi, muốn trải nghiệm đến những miền xa xôi đấy. Giọng văn của Dương Thụy nhẹ nhàng, dí dỏm, mộc mạc, cuốn hút và dễ hiểu. Sách khá dày, lượng kiến thức vừa đủ và mới lạ làm tôi càng yêu tác phẩm này hơn. Hy vọng sẽ có nhiều tác phẩm như thế này được xuất bản.
4
537001
2016-01-28 09:25:43
--------------------------
359805
7338
Mình vốn thích văn Dương Thụy và thích sách về du ký nên đã đặt rất nhiều kì vọng vào quyển sách này, mà có vẻ kì vọng lớn thì dễ thất vọng. Thấy cuốn sách này Dương Thụy viết cứ hời hợt sao ấy, Châu Âu trong đây như được tô bóng bẩy lên mà chả có cảm xúc gì trong đấy cả. Tác giả miêu tả những nơi cô đi qua, những thông tin về mảnh đất đó nhưng chỉ tô vẽ được cái nền mà thiếu đi tinh thần quốc gia. Dương Thụy không làm cho mình có cảm giác như chính mình cũng đang được thăm thú khắp nơi như cô, điều cơ bản mà một quyển sách du ký cần làm được.
3
450546
2015-12-28 01:50:34
--------------------------
359505
7338
Mình đã nghe nói nhiều về Dương Thụy qua cuốn Nhắm Mắt Thấy Paris nên mới quyết định thử đọc quyển này. Dương Thụy chu du qua hầu hết mọi nơi của Châu Âu với vai trò của một cô du học sinh là chủ yếu. Từ Paris rực rỡ tới Provence hay Venice với những con kênh đào. Nội dung quyển sách thực sự khiến mình muốn đi du lịch tới Châu Âu ngay lập tức. Tác giả không chỉ miêu tả những đặc trưng về mỗi nơi cô đến mà còn đem vào sách cả tình cảm của cô nữa. Ở tác phẩm này, bạn sẽ không thấy những câu chuyện tình yêu như những tác phẩm trước mà là một hành trình dài với những cảm xúc có phần lãng mạn. Một quyển sách hay cho những ai đam mê xê dịch!
4
490713
2015-12-27 11:09:51
--------------------------
324681
7338
Đọc sách của chị Dương Thụy mình rất thích vì y như mình đang được đi du lịch cùng với nhân vật chính vậy đó. Bản thân cũng là một người rất thích đi du lịch nên khi đọc sách của chị cảm thấy rất lý thú. 
Sách của chị thường có những câu chuyện hóm hỉnh, dễ thương, rất dễ lấy lòng các độc giả nữ. Tuy nhiên, bên cạnh đó nó cũng khơi gợi độc giả "xách ba lô lên và đi" để tận mắt ngắm nhìn tất cả những địa điểm được miêu tả trong sách.
Nếu các bạn muốn biết thêm về vùng đất Châu Âu, có ý định khám phá thêm nơi này thì đây là cuốn sách rất đáng để đọc!
4
723262
2015-10-21 18:57:51
--------------------------
304498
7338
Mình là người vốn rất thích đi du lịch đây đó. Khi đọc "Vanise và những cuộc tình Gondola" tôi cảm thấy rất thích thú, quyển sách là giúp tôi có thêm nhiều trải nghiệm đầy hứng thú, những chuyến đi "trong tưởng tượng" nhưng rất thực tế về Châu Âu. Tôi chắc chắn rằng mình sẽ có thêm nhiều kinh nghiệm quý giá nếu có dự định đi "phượt" như Dương Thụy. Nếu các bạn muốn biết thêm về vùng đất Châu Âu, có ý định khám phá thêm thì đây là cuốn sách rất đáng để đọc đấy !
4
453523
2015-09-16 14:14:09
--------------------------
301626
7338
Quyển sách viết về nơi chốn mà Dương Thụy có dịp đi qua trong thời gian du học của mình cùng những cảm nhận rất riêng về con người và cảnh vật nơi đó. Giọng văn nhẹ nhàng, chân thành và cuốn hút đã khiến mình không rời sách cho đến trang cuối cùng. Đây cũng là khởi nguồn cho niềm đam mê với thể loại sách du ký của mình. Với những bạn thích đi du lịch,đây sẽ là gợi ý quý giá về những địa điểm nên đến khi có dịp. Với những bạn không có điều kiện để đến tận nơi, đọc sách là cơ hội để khám phá mà không tốn kém lắm (chỉ tốn tiền mua sách mà thôi).  
4
63149
2015-09-14 19:34:57
--------------------------
284082
7338
Mình không vẫn thường hay đọc các thể loại sách về du kí, nhưng với Venise và Những cuộc tình Gondola của Dương Thụy, mình đã có những trải nghiệm tại nhà rất thú vị. Chỉ với số tiền như vậy, mà mình có được những kiến thức bổ ích về du lịch trong tầm tay thì quả là tuyệt vời. Dương Thụy vẫn vậy, giàu cảm xúc nhưng vẫn rất tinh tể, hiện đại. Chị viết với cái nhìn sâu sắc nhưng cũng không kém phần nhiệt huyết của một người trẻ tuổi. Cảm ơn Dương Thụy đã cho mình cơ hội có một chuyến trải nghiệm vùng đất châu Âu tuyệt vời chỉ qua từng trang sách.
5
131327
2015-08-30 16:38:22
--------------------------
270079
7338
Venise Và Những Cuộc Tình Gondola là ghi chép về những chuyến đi, những gặp gỡ với con người thật của chính tác giả nên đôi chỗ chưa hẳn nhận được đồng cảm hoàn toàn của người đọc. Nhưng cái chất bụi bặm, hóm hỉnh, khát vọng vươn xa của sinh viên trong “Venise và những cuộc tình Gondola” tạo nên nét cuốn hút rất riêng. Các độc giả trẻ có thể đồng cảm với cảm xúc của tác giả trong hành trình của mình. Hành trình của một cô gái trẻ khao khát đi, dấn thân và cảm nhận. Đặc biệt là với những bạn đã, đang đi xa hay những bạn chỉ đơn thuần ôm ấp niềm mơ ước được đi, được khám phá những vùng đất mới.
2
710934
2015-08-17 22:34:57
--------------------------
260745
7338
mình mới nhận được cuốn sách chiều nay. tiki giao hàng rất nhanh nhẹn và lịch sự. cuốn sách trông dày hơn mình nghĩ. từ lâu đã thích sách du kí nên thấy cuốn du kí nào cũng tìm mua bằng được. đã biết chị dương thụy qua "beloved oxford", "cung đường vàng nắng" nhưng cuốn sách này vẫn là tuyệt vời nhất. cuốn sách cho mình được trải nghiệm, được "đi du lịch trong tưởng tượng", từng hình ảnh cứ hiện ra trước mắt, những hình ảnh về một châu Âu xa xôi, lạnh giá, rất cổ điển nhưng cũng rất hiện đại. từng câu từng chữ trong sách đều cuốn hút một đứa mê du lịch nhưng....không có tiền như mình. một cuốn sách mà bất cứ ai mê du lịch đều nên đọc!!
5
393384
2015-08-10 17:10:46
--------------------------
254971
7338
Mình vốn không phải người đam mê du lịch. Nhưng thú thật, sau khi đọc quyển sách này của chị Dương Thụy là mình chỉ muốn được sống lăn xả mọi miền để cảm nhận cái hay cái đẹp của bạn bè bốn phương (dù điều kiện ít ỏi chỉ tậu được chuyến đi qua những trang sách). Mình có thể cảm nhận được từng cảnh vật, mỗi hương vị món ăn cũng như hơi thở xứ người một cách chân thật nhất qua câu chữ của chị. Chị đã viết cuốn sách bằng tất cả tình cảm dịu ngọt nhất, với cả trái tim nhiệt huyết sôi nổi và dĩ nhiên còn bằng mọi trải nghiệm trong cuộc sống. Với mình đây là một tác phẩm tuyệt vời. Nếu chỉ cần chi 63000 cho một cuộc chu du thế giới trời Âu thì quả là đáng giá vô cùng.
5
433873
2015-08-05 17:14:28
--------------------------
228175
7338
Vốn yêu thích châu Âu từ những hồi còn nhỏ với những nào là vẽ lãng mạn, cổ điển, vintage với một nền thơ mộng, hào nhoáng và khơi gợi tới vẻ đẹp quý tộc...nhưng khi đọc tác phẩm này của Dương Thụy lại là một góc nhìn rất riêng, có lãng mạng nhưng nhiều nét thực tế hơn, ở góc nhìn của một con người Việt Nam thăm thú và may mắn được làm việc ở châu Âu. Cái nhìn thực tế của tác giả, những tâm sự về rất chân thật về đời sống ko được hào nhoáng, dư dả tiền bạc khi ở châu Âu không làm mất đi vẻ lãng mạn nơi đây mà ngược lại tạo một cảm giác thú vị, lãng mạn dưới một góc mới gây thích thú cho người đọc. Nếu ai có ý định đi du học trời tây hay thậm chí du lịch thôi thì đây là một tác phẩm rất đáng nên đọc trước khi vác ba lô lên đi. 
4
630580
2015-07-15 08:11:35
--------------------------
220896
7338
Tựa sách "Venise và những cuộc tình ca gondola" chính là điều hấp dẫn mình đầu tiên, và mình "hốt" liền mà không cần phải đọc review. Thoạt đầu mình nghĩ đây sẽ là một câu chuyện tình ở thành phố của lãng mạn này, nhưng rồi thật sự bất ngờ và thú vị khi đây lại là một bức tranh phác họa gần như toàn cảnh châu Âu với góc nhìn đầy thi vị. Hồi bé rất thích đọc thần thoại Bắc Âu, rồi lớn lên lại thèm ngắm tháp Paris, chu du Venise trên những chiếc gondola nghe những chàng lái đò hát, hay đơn giản chỉ là một lần đi thử metro, mặc váy như bà đầm hái nho,...Rồi cái ước mơ ấy như được thỏa mãn khi đắm mình chìm vào những dòng chữ. một chữ thôi. Tuyệt!
4
227352
2015-07-03 09:10:43
--------------------------
168482
7338
Mình đã giới thiệu cuốn này cho nhiều bè bạn, nhất là những bạn có kế hoạch đi châu Âu học hoặc du hí. Cảnh thật, người thật, cảm nhận và trải nghiệm của tác giả cũng rất thật và... cái cách chị Dương Thụy tả về món ăn thức uống ở châu Âu khiến mình và bạn mình chảy nước miếng (chỉ muốn đặt ngay vé máy bay đi liền, ngay và luôn!). Giá tiền tương đối hợp lý cho một quyển sách gối đầu cho các bạn thích xê dịch, khám phá và có góc nhìn lãng mạn (thiết nghĩ nếu in thêm nhiều hình ảnh thì giá bìa sẽ tăng lên nhiều lắm, chậc).
4
461845
2015-03-16 19:34:43
--------------------------
147318
7338
Tôi yêu thích đất nước Pháp từ khi còn rất nhỏ. Nhìn những tòa lâu đài cổ kính, những cánh đồng hoa oải hương rộng lớn, những dòng sông hiền hòa trên những bộ phim tôi xem qua, tôi chỉ mơ ước được đến đó một lần. Và Dương Thụy đã giúp tôi thực hiện giấc mơ ấy. Những ghi chép sinh động về khung trời Provence thơ mộng đầy màu sắc và mùi vị làm tôi cứ ngỡ mình đang đắm chìm trong nước Pháp hoa lệ mất rồi. Và không dừng lại ở đó, "Venice và những cuộc tình Gondolas" còn biến tôi trở thành một kẻ du hành lãng mạn qua đường phố cổ châu Âu, nơi cảnh vật biến con người ta thành nghệ sĩ. Đôi khi du lịch là chẳng đi đâu cả, chỉ cần trên tay có một quyển sách, một tách cappuccino và vừa đọc vừa tưởng tượng.
5
86026
2015-01-07 16:11:48
--------------------------
136843
7338
Tôi yêu cách viết sách của Dương Thụy, chị viết những tiểu thuyết với bối cảnh ở trời Tây rất hay và lần này với "Venise Và Những Cuộc Tình Gondola" đã không còn những nhân vật hư cấu, những tình tiết sáng tạo nữa mà đó là những chi tiết thật, con người thật. Trời Tây trong quyển sách này được Dương Thụy kể và miêu tả có cả mặt xấu và điểm tốt nhưng nhìn chung đó là những không gian đẹp đến choáng ngợp qua ngòi bút của chị. Hơn 10 quốc gia châu Âu mà mỗi nơi có những nét đẹp riêng, nét văn hóa và tính cách con người khác nhau. Cuốn sách này cũng đồng thời cung cấp cho tôi thêm nhiều hiểu biết mới mẻ. Văn phong giản dị, nhẹ nhàng và cách dùng từ rất bình dân nhưng chính cách viết ấy đã tả nên một bức tranh trời Tây đẹp vô cùng.
4
392352
2014-11-23 17:55:07
--------------------------
123090
7338
Tôi là một người thích đi chu du đó đây, tôi thích chủ nghĩa "xê dịch", có lẽ vì thế mà tôi cũng yêu luôn những tác phẩm của Dương Thụy. Chị đi nhiều, trải nghiệm nhiều nên những gì chị viết ra tự nhiên, chân thực lại vô cùng sống động. Một khung trời châu Âu hiện ra lung linh dưới ngòi bút của chị. Mỗi nơi đi qua đều lưu lại những ấn tượng khó quên - từ Paris tráng lệ, tràn ngập ánh sáng, với những khu vườn thơ mộng làm rung bao tâm hồn thi sĩ, từ nước Bỉ vui nhộn, Hà Lan rực rỡ sắc cam...đến Áo với âm nhạc của Mozart hay Anh với Oxford cùng những tòa tháp đẹp như mơ. Không chỉ là đi, là đến mà còn là để thấu hiểu, để cảm nhận được vẻ đẹp của những vùng đất ta đặt chân qua, để thỏa mãn khát khao đi xa và khám phá. Gấp trang sách lại, ta vẫn nghe thoảng đâu đó mùi hoa oải hương, vẫn cảm nhận được sắc vàng lấp lánh của hoa hướng dương dưới ánh nắng trời, vẫn nghe râm ran tiếng ve sầu dưới những vòm lá...
Mặc dù chưa một lần đặt chân đến nhưng trong lòng tôi lại có một tình yêu thật lạ với những vùng đất ấy. Văn Dương Thụy nhẹ nhàng và truyền cảm, có đôi khi hơi gấp gáp nhưng đầy sôi nổi và rạo rực tâm hồn của một người trẻ thích khám phá. "Venise và những cuộc tình Gondola" là một cuốn cẩm nang đặc biệt cho những ai muốn đi và tìm hiểu về châu Âu, muốn thỏa mãn khát khao "xách ba lô lên và đi" của mình. Một cuốn sách nên đọc!!!
4
93006
2014-08-29 21:56:06
--------------------------
119985
7338
DT là một nhà văn mà tôi rất yêu thích bởi chị có một vốn kiến thức sâu rộng, đi nhiều nơi, biết nhiều thứ. Các tác phẩm mà DT viết ra đều có chất lượng rất tôt, chân thực và có tính ứng dụng thực tiễn cao. Vì là một du học sinh nước ngoài nên các truyện của cô đa số viết về nc ngoài và đời sống người Việt ở nước ngoài cũng như cách người khác đánh giá về người Việt Nam.
“Venise và những cuộc tình gondola” này lúc đầu tôi mua nó cứ ngỡ đây sẽ là một câu chuyện tình dài tuyệt vời nhưng khi đọc thì thật sự không phải, đây là cuốn sách kể về những nơi mà tác giả đã đi qua và bây giờ tác giả tổng hợp lại về những nơi đó, miêu tả lại những con người và vẻ đẹp của nó.
Đối với tôi cuốn sách này rất hay và bổ ích nhưng có lẽ đối với mọt số khác nó sẽ là nhàm chán vì đây không phải truyện nhưng thật sự nếu bạn muốn hiểu nhiều hơn về châu Âu thì nên mua nó.
4
286005
2014-08-08 15:32:46
--------------------------
119087
7338
Đây không phải là cuốn sách của câu chuyện tình yêu của hai con người hay kể về một mối tình châu âu lãng mạng nào đó mà đây là cuốn sách về những trải nghiệm về những vùng đất mà Dương Thụy đã đi qua. Càng đọc sách mình càng thấy được mở rộng tầm mắt ngắm nhìn thế giói rộng lớn này, mà cụ thể là châu âu.
Mình thấy một góc rất thực tế của châu âu chứ không hoàn toàn là quá kiều diễm xinh đẹp như mọi người vẫn đồn đến. Mình được biết thêm về cả thành phố Barcelona mà mình yêu thích, quả thực rất thú vị.
4
166271
2014-08-02 12:42:23
--------------------------
118358
7338
Cuốn sách này hoàn toàn không có những câu chuyện tình lãng mạn tuy vậy vẫn đậm chất Dương Thuỵ và khiến mình không thể rời mắt. Mình xem đây giống như là một cẩm nang du lịch hơn vì chính những thông tin, những cái nhìn mới lạ, những địa điểm kì thú mang đầy dáng dấp Châu Âu trong những trang văn. Càng đọc mình càng thấy háo hức và bị một Châu Âu hơi-khác-hơn so với những gì người ta thường tả, khác ở đây là những thành phố lạ lẫm hơn, ít được nhắc tới hơn, ít tráng lệ hơn so với những Paris hoa lệ hay London sương mù nổi tiếng, những nơi khác trên khắp lục địa này lọt vào mắt xanh của Dương Thuỵ. Đây chắc chắn là cuốn sách mà những bạn trẻ, giống như mình, đang ấp ủ ước mơ đặt chân đến Châu Âu không thể bỏ lỡ.
4
284845
2014-07-28 10:45:49
--------------------------
114979
7338
Phải nói đây là một trong những quyển sách du kí mà mình thích nhất. Cách viết của Dương Thụy nhẹ nhàng, dí dỏm, hơn hết nữa là những gì chỉ trải nghiệm được ghi nhận qua lăng kí thực tế và bình dị. Cảm giác rất thoải mái khi đọc và cảm nhận những chuyến hành trình của chị. Không có quá nhiều những đoạn hội thoại, chỉ là những dòng suy tư rất thật từ chị. Trải nghiệm từ một cô sinh viên không có nhiều tiền nhưng đam mê trải nghiệm và khám phá. Từng trang sách chị viết đều vô cùng đáng giá, không chỉ để biết them về những vùng đất mới mà còn là những kinh nghiệm vô cùng quý giá nếu bạn cũng là người đam mê “phượt bụi” như Dương Thụy.
5
360416
2014-06-21 02:38:56
--------------------------
