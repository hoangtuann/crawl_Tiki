241160
4229
Đến tập này, ta thấy dường như Sunako đã dần dần trở thành một cô gái xinh đẹp rồi. Bản thân Sunako đã đẹp, chỉ vì cô ấy thích truyện kinh dị và sống u ám thôi. Đọc đến đây, cười muốn rụng răng luôn. Sunako đến Harajuku chỉ vì nghe nói khu này có bán những trang phục kinh dị, trong khi 4 người kia thì ảo tưởng là cô ấy muốn mua sắm như bao cô gái bình thường khác. Bản thân mình rất thích Sunako, cô ấy sống vì chính cô ấy thôi, không hề vì nhân xét của người khác mà thay đổi cách sống. Mình khâm phục cô ấy. Thật là mong chờ các tập tiếp theo!
4
143495
2015-07-25 11:21:10
--------------------------
180634
4229
Lúc đầu khi nhìn nét vẽ và đọc qua nội dung về một cô gái sống cùng bốn chàng đẹp trai thì tôi không thích lắm , nghĩ nó nhạt nhẽo nhưng khi lên mạng đọc qua một hai chap tôi thấy khá có hứng thú và quyết định mua bộ truyện này . Mỗi nhân vật trong truyện đều có một cá tính riêng bao gồm cả Sunako - tính cách vô cùng đặc biệt . Không giống những người khác , cô thích ru rú trong nhà , thích những thứ kinh dị và sợ ánh mặt trời . Dần dần cô đã bớt hơn nhưng tính cách thì ăn máu rồi . 
Thực sự bộ truyện này đọc rất buồn cười , đủ mọi thể loại trong bộ shoujo này luôn . Nhìn chung đây là một bộ shoujo khá nổi trội trong số shoujo Kim từng xuất bản , tuy nhiên nét vẽ không được đẹp lắm .
4
335856
2015-04-09 20:39:10
--------------------------
180543
4229
Bộ shoujo đầu tiên mình cho là không hề sến súa. Truyện hài hước  với nhiều tình huống cười ra nước mắt luôn. 4 anh chàng siêu đẹp trai và một cô gái... motif có vẻ xưa cũ nhỉ, nhưng nếu cho rằng nó giống các bộ như Con nhà giàu thì nhầm to nhé, phải nói bộ này vượt trội hơn hẳn về sự phá cách nội dung, chị nhân vật chính quả rất bá...Tuy là shoujo nhưng có vẻ sự tiến triển trong tình cảm nhân vật cực chậm chạp, không "mì ăn liền" xíu nào cả. Lúc đầu nhìn bìa truyện, cách vẽ không làm mình ưa nổi nhưng may mà ở trong vẽ không tệ, đừng đánh giá bộ vội qua bìa truyện, không thì lỡ bộ hay mất ♥
5
249905
2015-04-09 17:56:16
--------------------------
