288476
5173
Quyển "Cẩm Nang Sinh Hoạt Bằng Tranh Cho Bé - Kĩ Năng Trong Sinh Hoạt Thường Ngày" của nhà xuất bản Kim Đồng này cung cấp và dạy cho các bé các kỹ năng cần thiết trong sinh hoạt hàng ngày thông qua các hình vẽ minh họa dễ thương và dễ hình dung, qua sách các bé có thể học được các kỹ năng rất cơ bản như gấp chăn màn, gấp quần áo, đánh răng, rửa mặt, dọn dẹp đồ chơi sau khi chơi xong, tự mặc quần áo, đi ngủ đúng giờ v.v... Sách được in màu đẹp và được biên dịch xử lý kỹ. Nói chung mình thấy đây là một quyển sách hay nếu có điều kiện thì nên mua cho các bé tham khảo
4
153008
2015-09-03 16:43:41
--------------------------
258386
5173
Đây là một cuốn sách mà bố mẹ nên dùng để hướng dẫn cho trẻ thực hành các kỹ năng trong sinh hoạt hàng ngày. Các hình ảnh của sách rất dễ hiểu, trực quan làm cho việc hướng dẫn làm các công việc hàng ngày của bố mẹ trở lên dễ dàng. Có những kỹ năng mà bản thân một bà mẹ như mình cũng đang làm sai từ cách sử dụng đũa, cách đặt con cá lên đĩa như thế nào thì thuận tiện cho việc ăn tới cả việc ngồi xem tivi như thế nào cho không hai mắt- điều mà mình nói với bé rất khó. 

Trẻ con từ 2 tuổi là có thể tiếp cận dần để tạo thói quen tốt trong sinh hoạt. Chúc các bố mẹ luôn hướng dẫn bé thành công
5
342603
2015-08-08 14:55:05
--------------------------
