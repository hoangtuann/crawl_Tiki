484790
8383
Truyện này không hấp dẫn và rùng rợn bằng kiếp nạn trời định, huyết án liên hoàn. Trong truyện này cổ tiểu yên lơ ngơ quá không nhạy bén. Tội phạm giả làm La Thiên mà Cổ Tiểu Yên cũng không nhận ra, dù đeo mặt nạ giống hệt la thiên nhưng thói quen cách nói truyện đâu thể nào y hệt được. Không biết  tác giả có viết phần tiếp theo không, chứ để kết thúc la thiên bị hủy dung chia tay với cổ tiểu yên thì chán lắm. 
Mà nói chung tôi không thích nhân vật cổ tiểu yêu vì cái tính bướng bỉnh cứng đầu mà hại la thiên lâm vào nguy hiểm xém mất mạng
2
555706
2016-09-26 22:15:42
--------------------------
482245
8383
Không bít mấy bạn thấy sao chớ riêng mình thì khá là thích truyện của Thượng Quan Ngọ Dạ đấy, vừa mua cuốn này xong là bay lên giường đọc liền 1 mạch đến hết lun lôi cuốn trong từng trang sách khiến mình không bỏ xuống được. BỌ CẠP RỪNG SÂU khá là hay nhưng theo mình thấy thì 2 quyển trước có vẻ hay và kịch tính hơn nhưng dù sao thì đây là 1 tác phẩm đáng để bạn tìm đọc. 
Ây da~~~ Tui cần viết đủ 100 từ thì mới gửi được nhận xét này, biết ghi thêm gì bây giờ??? Thích anh La Thiên quá, cảm ơn tác giả vì đã tạo ra 1 nhân vật hết sức đẹp trai và tài giỏi này mãi iu, gửi ngàn nụ hôn đến tác giả và nếu có phần tiếp theo thì mong tác giả viết 1 kết thúc có tâm tí xíu 
p/s: không biết cái nhận xét này của mình có được đăng không nữa ahuhu 
5
740752
2016-09-04 20:45:49
--------------------------
372458
8383
Tác giả cho biết nhiều ý tưởng độc lập , quan sát tự do làm hoàn thành nên nhiều cách miêu tả chưa có mấy bất ngờ làm tăng sự ấn tượng với sản phẩm làm thành nên những mâu thuẫn chưa có làm rõ , giảm đi chỉ trích về số phận người nhân vật , chỉ có thể làm mạnh lên , gia tăng lên ý nghĩa của quan điểm vốn khai thác đã khó rồi còn chưa có thể nào nghĩ về điều kiện nó đem theo chưa nhiều , phải chăng nó đạt được âm mưu liên tưởng nào thôi .
4
402468
2016-01-21 17:48:36
--------------------------
305623
8383
Đây là series đầu tiên mà mình đọc của Thượng quan Ngọ Dạ. Quyển đầu tiên rất hay và kinh dị, sang quyển thứ 2 thì thấy nhàm và chủ yếu là phá án; quyển thứ 3 " Bọ cạp rừng sâu" cũng nhàm như vậy. Nếu ai không đọc cả 3 quyển thì chắc k hiểu và cũng không thấy hay đâu. Truyện không hồi hộp và sợ hãi như 2 phần trước, mình chỉ mua để biết cuối cùng sự thật nó là thế nào thôi. Chủ yếu là phân tích tâm lý nhân vật, đọc truyện này na ná như phim Hunger game ý. 
4
619746
2015-09-17 01:01:13
--------------------------
288546
8383
Truyện của Thượng Quan Ngọ Dạ lúc nào cũng hấp dẫn tôi và quyển Bọ cạp rừng sâu cũng không phải là một ngoại lệ. Ở đây, tác giả không tập trung vào những yếu tố rùng rợn những tập trước mà cô khoáy sâu vào phần miêu tả diễn biến tâm trạng nhân vật hơn. Đây chính là một chuyển biến rõ rệt trong văn phong của tác giả và cũng là một điểm tôi yêu thích nhất trong truyện. Điều này khiến cho câu chuyện không những không nhàm chán mà còn vô cùng kịch tính. Bạn nên mua và đọc Bọ cạp rừng sâu vì tôi tin bạn sẽ phải kinh ngạc đó.
5
383229
2015-09-03 17:26:00
--------------------------
219259
8383
Nếu đây là tác phẩm đầu tiên của Thượng Quan Ngọ Dạ (TQND) mà bạn đọc thì bạn sẽ thấy khá hài lòng với diễn biến của tác phẩm. Nhưng do trước đó mình đã đọc Kiếp nạn trời định và Huyết án liên hoàn nên mình không thích tác phẩm này bằng. Nếu như 2 cuốn trước mình đọc mà không muốn rời mắt cho dù rất sợ nếu bạn đọc vào ban đêm thì Bọ cạp rừng sâu rất bình thường, thậm chí mình không hề thấy hồi hộp và sợ hãi. Dù sao phải nói là tuy tác phẩm không như kì vọng nhưng bút pháp của TQND vẫn mượt như thế làm cho người đọc cảm thấy rất hảo cảm. Là một fan của TQND mình rất mong chờ vào các tác phẩm tiếp theo của chị!
4
676040
2015-07-01 12:08:09
--------------------------
169290
8383
Tôi đã đọc qua tất cả tiểu thuyết của Thượng Quan và đây là truyện khiến tôi "lộn ruột" nhất. Đương nhiên nếu bạn yêu thích thể loại tiểu thuyết kinh dị trinh thám tôi tin tác phẩm này là vừa đủ để thoả mãn niềm yêu thích của bạn. 

"Lộn ruột" là vì cái kết thúc sau khi hai nhân vật chính trải qua mọi sự đày đoạ, sợ hãi và khám phá ra sự thật thì rốt cuộc cũng chỉ là con rối trong tay kẻ sát nhân! Nói thẳng ra là cái tốt đã thua cái xấu, thua thảm hại. Rồi thì một ngày nắng đẹp kẻ sát nhân chợt thấy thương cho mấy ngài cảnh sát chắc cả đời cũng không mò nổi ra hắn nên ra đầu thú! Vì sao?? Vì hắn ta thích vậy! 

"Vừa đủ" là vì Thượng Quan đã tạo đc một cốt truyện kịch tính phải nói là nghẹt thở. Tôi đảm bảo là nếu bạn cầm vô nó rồi thì khó mà bỏ xuống được. Đương nhiên như tôi đã nói tôi ghét cay cái kết thúc nhưng khách quan mà nói thì đây là một cái kết khá độc, lạ và không thể nào đoán ra trừ khi bạn mở trang cuối ra xem trước :) 

Dù sao đây cũng là một truyện đáng đọc!
4
520315
2015-03-17 22:09:54
--------------------------
108880
8383
Tác phẩm BỌ CẠP RỪNG SÂU của Thượng Quan Ngọ Dạ lần này cũng chỉ có thể nói là tạm được, coi giết thời gian cũng ok, chứ nói hay thì không đúng lắm. Nhưng túm lại, nội dung tuy không có gì mới, motip giống các phim điện ảnh của Mỹ thì kết thúc khá bất ngờ nằm ngoài suy đoán của tôi, mà tôi cũng dám chắc nếu ai đọc tác phẩm này cũng không thể nào đoán được hung thủ, đảm bảo luôn. Ngoài kết thúc bất ngờ thì tình tiết trong truyện cũng khá gây cấn, kích thích người đọc, mỗi chương sách luôn tạo ra cao trào, có điểm nhấn để nhớ.
Bên cạnh những cái hay đó thì truyện vẫn còn khá nhiều điểm thiếu sót và phi logic. Vị dụ như:
_lLm sao mà đưa cả đám vô trong một khu rừng mà không để lại dấu vết gì, đưa kiểu gì mà siêu thế?
_Cái thứ hai là các nhân vật có liên quan đến vợ chồng Tô Tuyết thì đều bị đưa vô rừng và giết chết. Nhưng nếu đưa vô như vậy thì hung thủ phải ra tay giết cho sạch hết à nhưng đằng này Đoàn Lôi thì sao, sự đưa vô không mục đích, tác giả chỉ nhằm làm rối loạn phán đoán của người đọc, đây là chi tiết thừa tôi thấy không hay.
_Rồi điểm vô lý nữa, là vì sao Chu Vũ lúc nào cũng gây khó dễ, ác cảm với Tiểu Yên. Nếu tác giải muốn sử dụng cái sự ác cảm này để khắc họa lên bản tính ích kỷ, chỉ biết nghĩ đến bản thân mình của Chu Vũ thì phải cài đặt cái lý do gì đó để Chu Vũ nghi ngờ không ưa Tiểu Yên thì như vậy mới hợp lý.
Nhưng thôi, nhìn chung ngoài những điểm nói trên thì cũng phải tuyên dương tác giả đã khắc họa được tính cách nhân vật khá rõ nét, xem xong rồi hồi tưởng lại vẫn thấy được điểm nhấn chứ không phải mù mờ tính cách như những tác phẩm khác.
3
180185
2014-03-24 11:17:10
--------------------------
107417
8383
Cá nhân tôi cho rằng, đây là 1 tác phẩm ko có gì đặc biệt của TQND so với 2 cuốn đã xb trước đây là Kiếp nạn trời định & Huyết án liên hoàn. Cốt truyện cũ, cách xây dựng cũ, may mắn là chi tiết bất ngờ cuối truyện của tác giả cũng làm tôi ngạc nhiên đôi chút thật :) Nhưng có lẽ vì quá tập trung vào 1 chi tiết bất ngờ ấy mà tác giả có phần qua loa với các nhân vật khác, thậm chí có nhân vật chỉ biết là liên quan chứ chẳng hiểu lquan tới mức nào mà phải chết :)) Tác giả còn xào lại nhiều chi tiết, ví dụ việc chìa khóa để mở mặt nạ nằm trong bụng một người, muốn lấy phải mổ bụng người đó (tôi đã đc nghe kể về chi tiết này trong film Saw - Lưỡi cưa).

Đây có thể coi như phần 2 của Huyết án liên hoàn, khi Bồ Bằng giữ lời, quyết tâm ra 1 câu đố cho La Thiên. Bồ Bằng là một nhân vật thú vị, một sát nhân kiểu mẫu ta vẫn hay bắt gặp trong film hay những cuốn truyện trinh thám - cực kì thông minh, tàn nhẫn nhưng cũng rất biết điểm dừng. Kiểu sát nhân ko lấy giết người làm niềm vui, mà giết người vì tự cho mình có sứ mệnh trừng phạt tội lỗi của kẻ khác. Trong truyện, Bồ Bằng đã đưa nhóm người đến rừng Bọ Cạp, tượng trưng cho nơi con người trở về với bản ngã của mình, nơi mà nhóm người đc đưa tới thể hiện rõ bản chất "con" hay bản chất "người" của mình. Có lẽ TQND ko tập trung vào cái ác theo hướng "tội phạm", vì cô đã xây dựng nên 1 nv Bồ Bằng cực kì xảo quyệt và ko ai có thể thắng nổi. 

1 lần nữa, tôi thêm thất vọng về nhân vật nữ chính, ko hiểu nhân vật này đc xây dựng nhằm mục đích gì, có ích lợi gì trong quyển truyện, ngoài việc cứ lao vào tròng rồi lại đc cứu thoát thần kì. Đến linh cảm đặc biệt cũng ko có, cũng chẳng suy luận đc gì; nếu ko đc nói rõ từ đầu đến cuối thì nữ chính rõ ràng chẳng biết gì. Trong tập này vai trò của La Thiên cũng ko đc chú trọng, khi anh chẳng có cơ hội nào để suy luận phá án gì cả. Ngay từ đầu anh đã thua Bồ Bằng. A chỉ có đủ sức mạnh ý chí để chống chọi qua thử thách của BB, thế là hết.

Tôi từng cho rằng có lẽ mình đã có thêm 1 tác giả truyện trinh thám kinh dị mới để yêu thích, nhưng ko hiểu sao càng đọc thêm của TQND tôi càng thấy bình thường hơn, ko đc như người ta ca tụng, hoặc có thể do cảm nhận của tôi khác :)
3
16123
2014-03-06 10:01:27
--------------------------
107355
8383
Tác phẩm này so với quyển Kiếp nạn trời định thì không kinh dị bằng, tiểu thuyết trinh thám, ly kỳ mới đúng. Không biết còn tập tiếp theo hay không nhưng tôi vẫn còn nhiều thắc mắc và nhiều câu hỏi chưa được giải đáp.
Vì sao Bồ Bằng lại biết được những chuyện không ai nói ra? Vì sao lại trả thù cho Tô Tuyết, vì Tô Tuyết giống 1 người quen, người quen đó là ai? Vì sao chỉ nói cho Tiểu Yên nghe? Vì sao không là La Thiên? Chẳng phải trò chơi này là dành cho La Thiên sao? Đúng rồi phải đòi gặp La Thiên mới đúng chứ. Cái cách tự sát của Tô Tuyết quá thông minh và độc đáo, tôi không nghĩ chính Tô Tuyết là người nghĩ ra cách đó, nếu cô ấy có đủ khả năng như vậy thì đã không phải chịu oan ức rồi tức tưởi mà chết. Vì sao chìa khóa lại được để trong người Tư Khải Điển mà không phải là Chu Vũ 1 kẻ ích kỷ, Cố Phong 1 người tàn nhẫn, Vương Hải Thành nguyên nhân chính dẫn đến trò chơi tàn ác này? Tuy đến cuối truyện đã có câu trả lời nhưng vẫn không làm tôi cảm thấy thỏa mãn. Đây là cảm nhận của riêng tôi, có thể không cùng quan điểm với những người khác.
Thực tình mà nói nếu chưa từng bị đẩy vào hoàn cảnh đau thương đó thì bản thân sẽ không hiểu rõ con người mình là như thế nào. Tuy tàn nhẫn nhưng cũng không thể trách được, vì con người luôn luôn tồn tại phần CON chỉ những ai có ý chí và sức mạnh tinh thần mới có thể giành lại phần NGƯỜI trong bản thân.
4
22758
2014-03-05 15:16:38
--------------------------
