457263
5631
Bà cô Jodie dạy anh văn tại trường của Ran thật đáng nghi, có lẽ nào bà ta thuộc băng của bọn áo đen? Tập này Hattori đã hỗ trợ Conan nhằm xác định chân tướng cô Jodie nhưng không thành công. Nhân vật Hattori quả thật quá thông minh và dí dỏm, ngày càng thích anh ta hơn cả Conan. Kết thúc tập này Ran rơi vào cơn sốt và trong khi thiếp đi Ran nhớ lại lần du lịch Mỹ cùng Shinichi và tình cờ phá vụ án Quả Táo Vàng ly kỳ phỏng theo thần thoại Hy Lạp.
3
23847
2016-06-24 09:13:35
--------------------------
436707
5631
Cô bé Haibara này lúc đầu rất lạnh lùng nhưng sau khi cùng hòa nhập với thế giới của bọn trẻ lớp 1B và cảm nhận được sự quan tâm của Conan thì cô bé đã hòa đồng hơn. Mình thích nhất là cá tính lạnh lùng và mạnh mẽ của cô nàng.Còn nhân vật cô Jodie nữa, bí ẩn bí ẩn~~ làm mọi người không khỏi tò mò. Tập này hay quá, đến nỗi ngồi coi trộm trong lớp nên bị thu luôn rồi T_T. Tập này rất hay, có lẽ là cuốn sách không thể thiếu trên kệ sách của các fan Conan rồi.
5
1113445
2016-05-27 07:56:53
--------------------------
432921
5631
Nửa đêm ngồi đọc tập 34 của bộ Thám tử lừng danh Conan không phải là ý hay xíu nào, vì truyện rất hấp dẫn, lôi cuốn và gay cấn. Càng xem càng thấy yêu mến nhân vật Haibara. Haibara có một cá tính đặc biệt vô cùng, vừa có chiều sâu nội tâm vừa rất mạnh mẽ, bản lĩnh. Còn nhân vật cô Jodie thì mang trong mình nhiều bí ẩnlạ lùng. Các vụ án có cách dẫn dắt mới mẻ, đọc không hề nhàm chán mà thu hút từ đầu tới trang cuối cùng luôn. Phải chấm 5 sao thôi.
5
531312
2016-05-20 01:00:47
--------------------------
379130
5631
Cô giáo Jodie đúng là một người bí ẩn mà. Connan cùng vs Hattori đi tìm chứng cứ ngay tại nhà cô ấy mà cũng không tìm ra. Mình bắt đầu lo lắng cho Ran khi Connan đang phá án mà cô nàng lại ngất xỉu, ngay lúc đó thì ký ức lại chợt hiện về trong tâm trí cô nàng. Nhờ vào lần hồi tưởng này mình phần nào cũng biết thêm về người đàn bà trẻ mãi không già đó, về sau mới biết bà ấy là... Mà sao mẹ Shinichi lại thân với bà ấy thế nhỉ? Nói chung tập này hay quá, hồi hộp chờ đợi tập sau để xem chuyện gì sẽ xảy ra nữa.
5
13723
2016-02-09 16:38:57
--------------------------
361284
5631
vì mình là fan trung thành của Haibara và cô Jodie nên tập này đối với mình là cô cùng hay. cô Jodie xinh đẹp, quyến rũ và vô cùng thông minh khiến cho người đọc không khỏi tò mò về nhân vật này. hơn nữa mà nhờ cô Jodie, vốn tếng anh của mình đã thêm được xíu :). 
đoạn mình thích nhất trong tập này chính là lúc conan quan tâm đến Haibara. dễ thương vô cùng luôn á :))
à mà tập này còn có người mẹ xinh đẹp của Shin nữa chứ. một tập truyện với toàn những nhân vật hay.
nói chung, conan tập 34 này là một sản phẩm rất đáng để mua và thưởng thức.
5
302048
2015-12-30 19:19:07
--------------------------
354884
5631
Thành viên bọn áo đen, cô giá dạy tiếng Anh, thông minh, cô Jodie này tưởng qua mặt được hai thám tử thiếu niên nổi tiếng của miền Đông và miền Tây nước Nhật. Jodie ranh ma nhưng Hattori còn cáo già hơn nhiều. Cậu ấy lừa được cô uống say và mượn được máy ảnh để lấy lại phim đã chụp. Nhưng biết rồi lại không làm gì mà bỏ qua thì quá nguy hiểm đi. Sao tác giả không viết tiếp và xử lý cô này luôn đi nhỉ !
Vì cô ấy cũng rõ thân phận của họ rồi. Nếu báo cho tổ chức chẳng phải họ sẽ bị giết hết sao !
5
535536
2015-12-18 20:48:14
--------------------------
285647
5631
Càng xem càng thấy yêu Haibara Ai quá! Cô bé tuy lúc đầu rất lạnh lùng nhưng càng về sau khi cùng hoà mình vào thế giới ngây ngô của bọn trẻ lớp 1B và cảm nhận được sự quan tâm của Conan, Haibara đã mở lòng hơn. Thích nhất đoạn trong tập này khi Conan gọi lớn tên Haibara khi nghĩ cô bé sắp làm chuyện dại dột. Conan quan tâm Haibara là thế nhưng thực ra cô nàng mạnh mẽ lắm, và mình thích cá tính đó của Haibara. Còn nhân vật cô Jodie nữa chứ, bí ẩn về nhân vật này ắt hẳn cuối cùng cũng sẽ được Conan làm sáng tỏ thôi.
4
471112
2015-09-01 01:21:27
--------------------------
264411
5631
Ở tập này mọi việc đều được đẩy lên cao trào. Về thân phận thật của Haibara- cô bé này có thân phận như thế nào. Rồi thêm sự xuất hiện đầy bí ấn của cô Jodie nữa. Và thêm nhiều vụ án xảy ra, biết rõ là một vụ án mạng nhưng vẫn không thể phá giải được bức màng bí mật đó. Chỉ có thể là Conan mới khiến bí mật hé mở. Tuy nhiên tập này tác giả dùng rất nhiều từ tiếng Anh khiến một đứa ngu Anh như mình đọc truyện lại hơi chậm so với những tập khác.
5
310891
2015-08-13 10:08:31
--------------------------
202848
5631
Tập này khiến cho mọi người không khỏi tò mò! Bắt đầu những tập truyện có tiếng anh của tác giả, tập này cũng vậy, có nhiều từ tiếng anh lắm! Về thân phận của Haibara nó khiến mình không khỏi bí ẩn với những hành động của cô bé ở sân ga! Tập này cũng nói một chút về cô Jodie, cô ấy là ai cũng như cô ấy có phải là người của tổ chức không nữa! Rất tò mò. Tập này còn quay ngược thời gian, trở về câu chuyện phá án của Ran và Shinichi, nó xảy ra ở New York và đầy tiếng anh, chỉ là phần mở đầu nhưng không thể không khiến người ta không mong đến tập tiếp theo được!
4
564406
2015-05-30 18:26:34
--------------------------
149024
5631
Tập 34 này tò mò và bất ngờ nhất ở cô giáo Jodie, thực sự rất là bí ẩn. Về Haibara, mặc dù còn chưa thích ứng được với cuộc sống mới nhưng thật vui vì Ai đã nghĩ lại, bắt đầu tập làm quen với mọi thứ.Ban đầu mình cũng khá bực bội khi Ai cứ nghĩ quẩn như vậy, trong khi bác Agasa và Conan thì chẳng bao giờ có ý định ruồng bỏ cô nhóc. Mình nghĩ Ai quyết định ở lại 1 phần là do Conan. Vụ ở nhà hàng Trung Quốc chuyển sang Golden Apple thực sự rất thú vị. Một cách dẫn dắt lạ đối với mình, khiến mình cảm thấy thích thú. Thêm nữa ở tập này là Shinichi phá án chứ không phải Conan, đồng thời được chiêm ngưỡng dung nhan minh tinh màn bạc - mẹ của Shin. Đặc biệt là hành tung bí ẩn của người đồng nghiệp với mẹ Shin. Conan vẫn đem lại nhiều bất ngờ, sự hồi hộp và niềm vui cho mình. Tuyệt!
5
123769
2015-01-12 12:30:09
--------------------------
