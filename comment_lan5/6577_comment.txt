445854
6577
Với tôi mà nói, tôi khong có quá nhiều thiện cảm dành cho nhân vật chính Thân Minh, hay chính xác hơn là tôi không ưa Tư Vọng. Dẫu biết trước đó cậu bị hãm hại, bị đối xử tệ bạc đến cùng cực, nhưng cách cậu trả thù, tôi không thích. Khiến cho những con người liên quan sự việc năm xưa phải chịu bao đớn đau, khổ sở, lại còn giết hại họ, có cần thiết phải như vậy không. Dù gì, để trời báo chẳng phải hay hơn sao, cứ vòng mãi như vậy, chả biết bao giờ thế giới này mới hết hỗn độn. Chỉ cần dọa cho họ chết khiếp là được rồi, không cần dày vò nhau như thế
3
920424
2016-06-11 01:52:19
--------------------------
372427
6577
Cuốn có nhiều thay đổi về mặt học tập lẫn nhiều mấu chốt trở thành những con người có sức hút lớn chống trả được sự thật đen tối , lấm bùn còn đang được thực thi biến câu truyện trở nên mất đi tính chân thật mà nó đem lại cho mỗi người đang cố ý trở thành vật truyền thuyết , trinh thám kinh dị pha với sự huyền bí của thể loại đi tìm sự mâu thuẫn trong chính mình lẫn của thời đại có sự ngang hàng với lối thuật lại diễn biến còn tẻ nhạt .
4
402468
2016-01-21 16:47:39
--------------------------
350816
6577
Mình rất tích thể loại trinh thám - kinh dị, mình đọc khá nhiều truyện thể loại này như Kỳ án ánh trăng, Truyền thuyết Lâu Lan, Đau Thương đến chết...
Hồ Sinh Tử, phải nói là ý tưởng nội dung rất hay, nhưng tình tiết mình thấy hơi nhanh.
Nửa đầu truyện đọc hấp dẫn, nhưng từ sau khi Cốc Thu Sa chết, rồi cảnh sát Hải chết, mọi chuyện trở nên nhạt hơn.
Mình cảm thấy khá tiếc nhân vật Cốc Thu Sa, tác giả không miêu tả được nhiều về nhân vật này. Đến lúc cô ta chết mình cũng ko biết nên thương hay nên ghét.
Nói chung truyện đọc tạm được, thư giãn.
3
593742
2015-12-11 10:37:41
--------------------------
300868
6577
Tôi đã từng đọc 2 bộ tiểu thuyết kinh dị của sái tuấn là hồ sinh tử và đề thi đẫm máu. Nhưng để lại trong suy ngĩ của tôi là những câu chữ lòng vòng. Tuy tác giả có miêu tả chi tiết từng sự việc,vấn đề nhưng lại k cho đọc giả cái cảm giác say mê mà lại có cảm giác mau chán và chẳng còn mún tiếp tục theo dõi diễn biến và kết thúc câu chuyện ra sao. Có lẽ đây sẽ là tác phẩm cuối cùng của Sái Tuấn trong tủ sách của tôi. 
2
685231
2015-09-14 11:48:09
--------------------------
246042
6577
Đã đọc hầu hết truyện của Sái Tuấn nên quyết định thử luôn quyển này. Nội dung ¾ đầu hơi dài dòng lê thê, tiết tấu chậm rãi đọc hơi buồn ngủ, phải đến gần cuối mới có gay cấn, hồi hộp. Các mốc thời gian trong truyện hơi lung tung nên đòi hỏi phải đọc 1 lèo là tốt nhất, nhân vật chính có vẻ yếu đuối, tham sân si cố đấm ăn xôi quá nên mình cũng không thích.
Tổng kết lại thì quyển này không xuất sắc lắm nhưng cũng kg đến nỗi tệ. Nên đọc các quyển khác của Sái Tuấn trước khi đọc quyển này.

2
474064
2015-07-29 17:30:39
--------------------------
219446
6577
Sau khi đọc Mắt Mèo, thấy tác giả này cũng được nên đánh liều mua đọc thêm cuốn này. Nội dung truyện phát triển bình thường, tạo được không khí kỳ bí, ngột ngạt, cũng có máu me, giết chóc nhưng không đáng sợ lắm. Nhiều tình tiết còn thiếu thuyết phục, mạch truyện thong thả, tiết tấu tăng lên từ từ và đặc biệt càng về sau càng hay. Nhiều nút thắt được tháo gỡ hay, bất ngờ. Tình thù luẩn quẩn, vay trả luân hồi. Tuy rằng kết thúc có hậu nhưng đầy đau đớn, cay đắng. Lòng người đố kỵ, lợi dụng lẫn nhau, mưu hại nhau, tranh quyền đoạt lợi... hay chỉ vì mục tiêu đơn giản để che đậy một sai lầm nho nhỏ... nhưng rồi đều đi từ sai lầm này đến sai lầm khác, hại người, hại mình, hại luôn cả người thân của mình. Dư vị của cuốn sách này quá nhiều, buộc người đọc suy ngẫm quá nhiều... mình đã từng là ai trong số những nhân vật buồn đau của truyện. HAY.
4
129879
2015-07-01 14:59:46
--------------------------
160477
6577
Mình là 1 fan chính cống của thể loại truyện kinh dị - trinh thám, và đã từng đọc hết tất cả các tác phẩm của Sái Tuấn đã từng xuất bản ở Việt Nam. Mình đặt biệt rất thích truyện này. Mô-típ trọng sinh, đầu thai chuyển kiếp như thế này bây giờ có vẻ như ko còn quá lạ lẫm. Nhưng truyện có kết cấu hợp lý, bố cục chặt chẽ, giọng văn hấp dẫn ly kỳ khiến người xem một khi đã đọc thì ko thể nào dứt mắt ra được. Đây có thể nói là tác phẩm mình thích nhất của Sái Tuấn ^^.

Nhưng có một điều khiến mình rất thắc mắc khi gấp cuốn sách lại, đó là cái kết cuối cùng của Tư Vọng là gì? Cậu từng nói rằng bản thân sẽ ko sống nổi qua 18 tuổi, cuối truyện lại cầm chuỗi hạt của Tiểu Chi rồi đi đến Khu ma nữ, có phải Tư Vọng định tự sát để chết theo Tiểu Chi ko? Bởi vì bây giờ cậu cũng đã trả xong thù, những người thân yêu nhất cũng ko còn nữa, thế gian này ko còn gì lưu luyến cậu.

Suốt cuộc đời của Tư Vọng (Thân Minh) có rất nhiều phụ nữ vây quanh, rốt cuộc người cậu yêu nhất trên đời này vẫn là Tiểu Chi.

P/S: Cái tên Âu Dương Tiểu Chi ko biết có phải là tên vợ hay tên người yêu của tác giả ko, mà trong bất cứ tác phẩm nào cũng xuất hiện 1 nhân vật nữ mang cái tên này ^^
5
61839
2015-02-24 14:42:36
--------------------------
119149
6577
Tôi vừa đọc xong cuốn Hồ Sinh Tử của Sái Tuấn nên còn tràn ngập cảm xúc nên quyết định viết cảm nhận cho nó.  
Đầu tiên tôi sẽ đánh giá về ý tưởng, đây là một ý tưởng hay đối với trinh thám kinh dị nhưng không mới lạ với mấy ai đọc ngôn tình rồi. Vì sự việc Thân Minh chết nhưng nhổ hết canh Mạnh bà ra nên anh đầu thai vẫn giữ được trí nhớ kiếp trước rất giống trọng sinh trong ngôn tình.
Còn về nội dung thì cũng bình thường, tuy bình thường nhưng tác giả rất biết cách câu kéo người đọc làm cho họ không thể rời mắt khỏi quyển truyện.
 Tuy nội dung ổn nhưng cách thể hiện rất có hơi dài dòng nhiều khi tôi thấy khá lang man khiến tôi hơi bực, đôi khi có nhiều tình tiết thừa kiểu giống như cố ý kéo dài trang giấy.
Mang danh là truyện kinh dị, trinh thám nhưng yếu tố đó tôi không đánh giá cao, vì truyện này về mặt kinh dị vẫn chửa đủ còn về mặt trinh thám thì cũng không thấy gì vì hầu như tôi thấy mấy tình tiết phá án rất mờ nhạt. Truyện chỉ tập trung nói về cảm xúc của Thân Minh và những yêu hận của anh ta.
Cuối cùng một điều khiến tôi ghét ở quyển sách này là Sái Tuấn hay đặt tên nhân vât nữ trong truyện là Âu Dương Tiểu Chi. Lần sau mà tác phẩm của anh ta còn đặt cái tên này nữa là tôi tẩy chay sách của anh ta luôn.
4
180185
2014-08-02 18:08:34
--------------------------
