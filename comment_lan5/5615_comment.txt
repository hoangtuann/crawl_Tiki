458806
5615
Đây là một torng số những tập mình khá thích trong series Conan, nhất là vụ án trên tàu hỏa. Cặp đôi Hattori - Kazuha ở phần đầu rất dễ thương, đã thích mà cứ ngại :)) Vụ án tàu lửa gây cấn vì quý bà bí ẩn hóa ra lại là mẹ Conan, hơi tiếc vì bố Conan xuất hiện hơi ít, mình rất thích những đoạn của hai người này, trai tài gái sắc, lại cộng thêm bạn con Conan thông minh, liệu có gia đình nào vượt qua nhà Kudo được không ;)) Vụ án thứ ba thì mình thấy không gay cấn lắm, điểm cộng là chàng trai định mệnh của Sonoko cuối cùng cũng đã xuất hiện
5
24079
2016-06-25 14:54:30
--------------------------
385895
5615
Trong cuốn conan tập 22 này có 3 vụ án mà mình thấy cả 3 đều hấp dẫn và kịch tính.Ở vụ án thứ nhất chúng ta lại được chứng kiến sự phối hợp phá án giữa anh chàng thám tử miền tây heiji và conan trong một lần heiji và kazuha lên tokyo.Không những gặp lại heiji và kazuha chúng ta còn được gặp lại ba mẹ của conan trong vụ án thứ 2 trên chuyến tàu tốc hành có giường nằm đến hokkaido mang tên "Hokutosei" và cuối cùng chúng ta còn được gặp anh chàng đã cứu sonoko thoát khỏi cái chết trong gang tấc nữa chứ.Mình chỉ muốn nói tập này tyệt vời.
5
1135753
2016-02-24 18:56:57
--------------------------
378234
5615
Kết thúc vụ án giết ông quản gia, Hattori và Kazuka với những hành động vụng về thân mật ấy, cặp đôi này đã trở nên cuốn hút hơn rồi!!
Hiếm khi ba mẹ của Shinichi xuất hiện! Nên cuốn tuyện đã hấp dẫn hơn với bộ ba xuất sắc này, câu chuyện thời xưa của tiểu tuyết gia Kudo Yusaku đã kết thúc, hé lộ khuôn mặt hung thủ của vụ án bây giờ!
Đặc biệt, ngoài đạo tặc Kid, Makoto-một anh chàng da ngăm, vô cùng giỏi võ- đã xuất hiện, bắt đầu câu chuyện tình của cặp mới này (Chắc chắn sẽ không thua kém gì cặp đôi Shinichi-Ran, Takagi-Sato)=>=>
5
97553
2016-02-04 19:47:56
--------------------------
344329
5615
Chàng xuất hiện giỏi võ karate, sao ai cũng chọn Karate nhỉ ? 
Đẹp trai nhất bộ truyện lại cực kỳ phong độ, da ngăm đen, cao và có những cú đá đẹp mắt xứng danh hoàng tử. 
Chờ cậu ấy xuất hiện lâu rồi. 
Vụ án trên xe lửa rất hoàn mĩ nếu không nói tới chuyện quần áo hoá trang chưa được phi tang. Nhưng tại sao lúc cảnh sát xét phòng không tìm ra, vô lý quá ! Đốt trên tàu không được. Quăng thì bị lộ. Chắc dùng áo sợi sinh học, tự tiêu huỷ hay dùng hoá chất tiêu huỷ thì thoát tội. Hay tới nhà ga đi vệ sinh vứt luôn trong đó rồi mới đi tìm mẹ Kudo giết hại. 
Có gì đó vướng víu mà tác giả không xứ lý được. 
5
535536
2015-11-28 09:57:52
--------------------------
212725
5615
mình đến cười ngất với cặp đôi Hattori - Kazuha vì họ y chang như cặp đôi Shin- Ran vậy, dĩ nhiên Hattorri may mắn hơn Shin vì vẫn là anh chàng 16 tuổi bình thường để còn vui vầy với Kazuha .  vụ án với Sônoko thì buồn cười vì anh chàng Makoto vốn chẳng phải kiểu ưa thích của bà chằn nảy lửa này... nhất là khi hung thủ lộ diện mình cũng hết hồn và lo cho Sonoko vì nhẹ dạ mà suýt chút nữa thành nạn nhân của tên sát nhân rồi rồi cú song phi giữa công tử của cú đá Makoto và tuyệt đỉnh Karate Ran Mỏi quả không chê vào đâu được dành cho tên hung thủ... tuyệt vời
5
568747
2015-06-22 19:58:38
--------------------------
207988
5615
Đa phần là conan tập nào cũng hay hết á! Dĩ nhiên tập 22 cũng vậy rồi. Ấn tượng nhất với tớ là tập về chuyến tàu thần bí ấy, hay và đầy kịch tính. Conan rất dễ thương nhỉ! Mà bác Aoyama vẽ tranh giỏi thật ấy! Không chỉ phần truyện ấy tuyệt vời đâu, trong truyện Heiji với Kazuha cũng có mặt luôn, một cặp trời sinh! Tóm lại là ở tập 22 này làm mình rất hài lòng tuyệt đối và rất thú vị nữa! Mình học được nhiều điều hay lắm, cảm ơn bác Aoyama nhé! :)
5
543425
2015-06-13 21:08:41
--------------------------
196899
5615
Yêu tập này lắm cơ, không những xuất hiện cặp đôi Heiji và Kazuha đáng yêu của lòng mình, mà còn là những cái đầu tiên của câu chuyện tình lâm li bi đát sau này của Sonoko nữa kia chứ, cái cảnh Makoto cứu Sonoko thật sự là lãng mạn quá đi thôi, chắc lúc đó tim Sonoko ngừng đập vì vừa bị ám sát vừa được trai đẹp cứu quá mất. Vụ án có dính tới cha Shinichi cũng rất rất là hay nữa, ai mà ngờ được những tình tiết ấy là ăn cắp từ cuốn sách của cha Shinichi cơ chứ, càng ngày càng nhiều bất ngờ.
5
530790
2015-05-16 16:56:53
--------------------------
179764
5615
tập 22 này có 3 vụ án và thích nhất vụ án cuối cùng.chuyến du lịch mà sonoko ao ước mãnh liệt tìm thấy một nửa của mình,cuối cùng cô nàng tiểu thư ấy đã tìm thấy rồi nhé.lại còn là người hoàn hảo nữa ! makoto xuất hiện trong nhà trọ nơi mà sonoko và ran ở.anh chàng da ngăm luôn theo dõi sonoko và chính nhờ anh ta mà sonoko thoát khỏi nhát dao chí mạng của kẻ sát nhân.khiếp võ gì mà đá ghê quá! một cảnh lãng mạn trong rừng và một tình yêu mới bắt đầu.vụ án thứ 2 cũng rất hay nhưng mình thấy hơi khó hiểu cái đoạn dàn dựng trong phòng :P
4
586877
2015-04-07 22:32:11
--------------------------
177190
5615
Không biết các bạn như thế nào chứ mình thích cặp Hattori và Kazuha lắm, thấy cặp này cứ như là chó với mèo, vờn nhau suốt. Trên chuyến tàu, tên tội phạm vận dụng những tình tiết trong cuốn tiểu thuyết đã gây nên tội lỗi. Suýt nữa thì mẹ của Shinichi cũng bị hại, may mà có ông nhà văn Kudo Yusaku kịp thời can thiệp. Anh chàng được mệnh danh là "công tử của những cú đá" ghê thật, nhìn mà đã con mắt. Hình như tác giả muốn mở ra một mối tình mới thì phải? Sướng chị Sonoko rồi nhé! Cuối cùng, cuốn truyện hay về nội dung và có nhiều tình tiết mới lạ, gây cấn và hấp dẫn, đó là điểm thu hút của cuốn truyện.
4
13723
2015-04-02 18:21:37
--------------------------
