304516
7315
Một tuyệt tác giàu kịch tích và đầy tính nhân văn, rất đáng trân trọng. Quyển sách làm thay đổi suy nghĩ người đọc. Nhiều tình tiết ly kỳ, hấp dẫn về một chú chó mặc dù bị đối xử tàn nhẫn  nhưng cuối cùng chú lại là người giúp đỡ cho chính người đã đối xử tệ với mình. Lắng đọng, giàu cảm xúc với trí tưởng tượng phong phú và hư cấu nhưng rất đời thực, rất ý nghĩa. Quyển sách thật tuyệt vời với thông điệp gieo nhân  nào, gặp quả ấy và thông điệp yêu thương, lòng nhân hậu. Cám ơn tiki.
5
110777
2015-09-16 14:20:31
--------------------------
232852
7315
Yêu thương chân thành và tấm lòng nhân ái của Trison thực sự khiến  người xem cảm động. Là giống chó Labrador siêu đẳng, Trison đã được huấn luyện bài bản để làm tốt vai trò " cán bộ đường lối" cho những người khiếm thị. Nhưng số phận đẩy đưa Trison làm nạn nhân chính cho âm mưu thay thế nội tạng cho Grin, chó cưng của đại gia bạo chúa Mikhailovich. May mắn là vợ ông ta, bà Olga đã bí mật cứu Trison và nó phải sống dưới thân phận của Grin từ đó. Khi bị phát hiện, Trison một lần nữa thoát chết nhờ người làm nhân hậu. Rồi cuộc sống lại thử thách trái tim của chú chó khi thành bạn dẫn đường cho ông chủ ác tâm lúc ông ấy bị mù. Cuối truyện, sự ăn năn của ông Mikhailovich là cái kết cảm động và có hậu, có ý nghĩa nhân văn cao: " Tha lỗi cho tôi nhé, bạn chó yêu qúy, hãy tha lỗi cho tôi..." Dĩ nhiên là Trison đồng ý. Vì bản giao ước bất thành văn giữa người và chó từ buổi bình minh của văn minh nhân loại là luôn giúp đỡ nhau, bất kể trong hoàn cảnh nào,  tình huống nào.
5
521993
2015-07-19 13:40:13
--------------------------
175730
7315
Đây là một trong 1 loạt các series truyện về chú chó dẫn đường. Mua sách rồi và đọc rồi mình mới biết điều này. Đọc rồi mình cũng mới biết tác giả của cuốn sách vẫn ở tuổi vị thành niên, vì vậy dường như cuốn sách hướng tới các độc giả nhỏ tuổi. Độ dài và nội dung của cuốn sách phù hợp với các bạn nhỏ, để học hỏi và xây dựng lòng trắc ẩn và nhân ái từ khi tuổi còn nhỏ. Các bố mẹ có thể mua cho con mình. Cuốn sách cũng mang lại những thông tin rất bổ ích về các giống chó thuần chủng, mình nghĩ cũng rất phù hợp để khơi gợ các bạn nhỏ ham học hỏi hơn về động vật và khoa học.
3
176277
2015-03-30 20:53:55
--------------------------
156433
7315
Lần đầu được cảm nhận tác phẩm viết về chó, mình thấy rất thú vị. Cũng không phí tiền khi mua sách cuả First new.  Điểm xuất phát cũng là điểm ban đầu, Trison - chú chó dẫn đường được tác giả nhập vai, đã trải qua không ít tình huống nguy hiểm từ Baọ chúa Alexander Mikhailovich, nhờ ơn Chúa ban phát may mắn nên Trison đã thoát được nhờ những người tốt bụng có lòng thương động vật.  Trison hoà nhập, làm bạn với loài mèo rất thân không như khái niệm xung khắc giữa chó và mèo xưa nay người ta hay nói. Trải qua cuộc sống từ khó nghèo rồi giàu sang bị đồng tiền làm mù mắt, sau tai nạn máy bay trực thăng, ông Bạo chúa Alexander Mikhailovich đã mù và không sống được bao lâu nữa do di chứng tai nạn để lại. Đó cũng là lúc con ngươì ta mới nhận ra giá trị cuộc đời đem tới. Tập truyện tuy ngắn nhưng đâỳ bài học ý nghĩa này sẽ dạy chúng ta nhiều điều sâu xa trong cuộc sống xung quanh mà có khi ta đã lãng quên mà không hề hay biết.
5
534452
2015-02-04 19:59:32
--------------------------
119405
7315
Một câu chuyện nhỏ nhưng đầy cảm xúc. Đọc truyện, mình trải qua giây phút vui tươi khi chú chó Trison đối thoại cùng hai chú mèo, hấp dẫn ngay từ đầu truyện khi Trison bị bắt, hồi hộp lo lắng khi Trison bị phát hiện giả danh chú chó khác và đặc biệt là cảm động với những tấm lòng nhân hậu. Đó là bà vợ qua mặt ông chồng để cứu chú chó. Đó là gia đình anh nhân viên bất chấp có thể bị sa thải, cố tình giả cái chết của Trison khi nhận được lệnh bắn chú chó từ ông đại gia bạo chúa.

Quả báo đã giáng xuống ông đại gia khi ông mất đi đôi mắt cũng như cận kề cái chết. Nhưng bất ngờ hơn cả là ở cuối truyện khi ông thú nhận rằng đã cố lờ đi để Trison được sống cũng như có những lời sám hối với những tội lỗi thời xưa. Và có lẽ cũng chính vì thế ông đã nhận lại được quãng thời gian cuối đời có Trison làm bạn tâm giao. 

Chó Dẫn Đường - Đại Gia Và Nhân Quả là một cuốn sách đầy ý nghĩa, là lời cảnh tỉnh và khơi dậy lòng nhân hậu trong mỗi con người. Đừng vì hạnh phúc bản thân mà chà đạp lên mọi người xung quanh.
5
194291
2014-08-05 01:00:47
--------------------------
110792
7315
Tác giả nhìn thế giới với góc nhìn của một chú chó trong sáng, giọng văn dễ thương vô cùng luôn, có những pha hành động rất nhộn, một con mèo không thèm biểu lộ cảm xúc trước chú chó ở lần đầu tiên gặp mặt, không phải vì nó "sang chảnh" mà vì nó lười biểu cảm. Đọc cuốn sách như được phiêu lưu cùng chú chó. Đầu cuốn sách, chú chó ở trong trường huấn luyện, và đó cũng là nơi chú trở về ở những dòng cuối cùng của truyện. Một đại gia bạo chúa ở giữa những người tốt bụng thật tình, và vài người tốt bụng nhờ... "tiền đưa đường dẫn lối".

Đọc xong Chó Dẫn Đường - Đại Gia Và Nhân Quả, tôi lại tự hỏi mình, có bao giờ mình đang cố giữ cái gì đó khư khư, ra sức bảo vệ khi nó sắp mất đi mà lại vô tình làm tổn hại những sinh vật xung quanh không? Trong khoảnh khắc nào đó, có khi nào mình là đại gia bạo chúa như trong truyện mà không hề hay biết? 

Tôi nghĩ Chó Dẫn Đường - Đại Gia Và Nhân Quả là một quyển sách hay, đáng chia sẻ cho bất cứ ai quanh mình, đặc biệt là cho những ai sẽ đang và đã là đại gia  :)
5
163702
2014-04-20 23:44:46
--------------------------
