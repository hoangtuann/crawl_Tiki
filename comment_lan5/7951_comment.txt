393637
7951
Một quyển tập tô chữ bổ ích cho các bé chuẩn bị vào lớp một. Đúng như tên gọi, tập hướng dẫn các bé cách đi nét khi viết các chữ cái hoa đầu câu trong hệ thống chữ cái Việt Nam. Thứ tự các nét đều được đánh số theo thứ tự trước sau, giúp các bé không viết ngược ngạo. Bên cạnh mỗi chữ còn có các từ và hình ảnh sinh động minh họa cho chữ cái mà bé đang học. Viết chữ hoa đúng và đẹp là một thách thức với trẻ nhỏ, đòi hỏi sự luyện tập nhiều. Tuy nhiên, phần luyện tập cho mỗi chữ trong tập lại không nhiều.
5
80783
2016-03-09 02:57:19
--------------------------
