440857
5788
Việc dạy và hướng dẫn để các bé hình thành những đức tính tốt là việc không hề dễ. Phần đa các bé đều rất thích nghe kể chuyện. Thông qua những câu chuyện có thể dần dần giúp các bé hình thành tính cách, nhân cách. Những câu chuyện ngắn gọn, dễ hiểu, hình minh họa trong sách cũng rất dễ thương giúp các bé dễ hiểu, dễ tiếp thu và hứng thú hơn với việc đọc sách. Mình đã mua trọn bộ Gieo mầm tính cách cho bé nhà mình. Để ý thấy từ khi đọc cho bé nghe những câu chuyện trong sách bé tiến bộ hơn rất nhiều. Biết lễ phép, quan tâm...người khác. Mình rất vui khi những tính cách tuyệt vời ấy sẽ cùng bé mỗi ngày lớn lên.
4
606122
2016-06-02 13:35:24
--------------------------
421281
5788
Đã có khi nào bạn cảm nhận được rằng mình là người không tử tế chưa? Vậy sống thế nào để trở thành người tử tế, từ những việc làm nhỏ nhất? Hãy gieo cho tâm hồn của bạn, của những cô cậu bé mới lớn những câu chuyện thật đẹp về cách đối xử với nhau giữa người với người. Bạn sẽ hạnh phúc biết bao nếu trong lúc khó khăn có người dang tay giúp đỡ bạn, Và còn hạnh phúc nào hơn khi bạn giúp đỡ một ai đó và nhận được lời cảm ơn chân thành. Hãy sống thật tử tế, chính là thông đẹp mà cuốn sách muốn gửi đến chúng ta!
3
505896
2016-04-24 20:00:24
--------------------------
386485
5788
Cuốn “Gieo mầm tính cách – Tử tế” nằm trong bộ 6 cuốn do Nhà xuất bản trẻ phát hành. Cuốn này có các câu chuyện như : Gói quà của người không quen biết, Cùng chơi, Chợt hiểu, Bạn tốt, Chuột nhắt và sư tử, Một cử chỉ đẹp, Đổi chỗ, Tô mì nóng của người lạ, Chiến thắng, Kỉ niệm ở rạp xiếc, Tính lầm, Ly sữa nóng. Trước mỗi câu chuyện đều có những câu danh ngôn rất hay, các câu chuyện có hình minh họa, và kết thúc truyện có phần “Cùng suy ngẫm” để giúp các bé nắm vững hơn bài học mà truyện muốn truyền tải.
3
15022
2016-02-25 19:23:58
--------------------------
255493
5788
Quyển sách nhỏ gọn, gồm nhiều câu chuyện nhỏ, mỗi câu chuyện đều có hình minh họa đẹp mắt, dễ thương. Nội dung ngắn gọn, dễ hiểu, gần gũi trong cuộc sống hàng ngày nhưng tất cả đều lồng ghép các bài học nhỏ trong đó. Cần phải đối xử tử tế với tất cả mọi người, từ người trẻ đến người già, từ người cơ nhở, khó khăn đến người neo đơn khuyết tật. Đem lại niềm vui cho người khác, bạn sẽ thấy cuộc sống này tốt đẹp biết chừng nào. Quyển sách thật dễ thương. Cám ơn tiki.
4
110777
2015-08-06 08:30:57
--------------------------
