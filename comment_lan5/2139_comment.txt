575577
2139
Mình mới vừa nhận được box Percy Jackson vào 13h30 là mình tháo seal ra, unbox, mở xem từng quyển và sau đó mở máy tính để bình luận trên Tiki ngay luôn. Điều đầu tiên mà mình cực kì ưng ý về box đó là 5 quyển trong hộp bóc ra rồi bỏ vào rất dễ dàng, vì bìa sách bóng trơn và hộp bên trong được lót thêm bìa trơn nữa, chỉ cần để úp cái hộp cho phần gáy sách chúi xuống là sách tự động trượt ra. Box đựng 5 quyển nhưng cầm vừa y trong bàn tay luôn, thậm chí là còn nhỏ hơn box Chuyện xứ Lang Biang của bác Ánh nữa. Nhưng mà cho dù vậy thì chữ bên trong lại không quá nhỏ khó mà đọc như tập 5 box Harry Potter Children Edition mà chữ vẫn ở dạng vừa. Giấy không trắng sáng làm lóa mắt người đọc. Giống như trên tiêu đề nhận xét của mình thì đây là box rất đáng có trong bộ sưu tập của những fan hâm mộ Percy Jackson như mình :)
p/s: mình có đính kèm thêm 3 hình chi tiết về 5 quyển sách bên trong box review cho mọi người xem ạ :D
4
1099532
2017-04-17 14:34:13
--------------------------
