491027
2575
Một tác phẩm không thể nào bạn quên. 
Câu chuyện mà bạn sẽ muốn đọc đi đọc lại nhiều lần
5
344992
2016-11-15 21:32:11
--------------------------
417144
2575
Phải thừa nhận một điều rằng bìa sách của TU cực kì ấn tượng và tôi cũng thích bộ sách này một phần nhờ cái bìa. Hiện tại sau khi đã đọc xong 1 tập của Sói và bồ câu thì tôi cảm thấy truyện không hay bằng "Tro tàn trong gió" nhưng lại rất thích cá tính của các nhân vật. Và không thể phủ nhận một điều rằng lời văn của K.Woodiwiss luôn cực kì sâu sắc, văn chương của bà rất hay và người dịch cũng rất tuyệt vời khi có thể truyền tải được hết nội dung qua ngôn ngữ tiếng Việt, nhờ đó mà tôi cũng thấy rằng tiếng Việt của mình thật sự quá phong phú. Thật sự thì với những tác phẩm của K.Woodiwiss, chắc chắn tôi không thể bỏ qua được.
5
41370
2016-04-16 12:29:52
--------------------------
402147
2575
Tôi thực sự bất ngờ vì Sói và Bồ câu không được như mong đợi. Sau Tro tàn trong gió tôi mong đợi ở 2 tập sách của Sói và Bồ câu có hàng trăm chữ một nội dung sâu sắc hơn nhiều 1 cuốn "Sói cà Bồ câu lên giường với nhau thế nào" tôi đã đọc. Và cũng bất ngờ vì đã có nhiều review tốt tới vậy mà tôi lại ko đọc hết nổi 1 tập. Diễn biến tâm lý của nhân vật kỳ quặc và ngớ ngẩn. 2 nhân vật chính đều được khắc họa là cá tính, cứng cỏi, kiên cường nhưng lại dễ thay đổi quan điểm chỉ vì...tình dục. Mà những đoạn miêu tả về tình dục lại vô cùng hời hợt. Có lẽ cái gì nhiều đến thừa thãi thì sẽ nhạt nhẽo. Có lẽ tôi chấm 2 sao chủ yếu thất vọng vì chất lượng sách sau khi kỳ vọng quá nhiều ở 1 tác phẩm của Kathreen. Chân tình khuyên bạn ko nên đọc.
1
696174
2016-03-21 20:48:48
--------------------------
360720
2575
Cái bìa sách quá đẹp luôn, nhìn rất phù hợp với nội dung truyện. Truyện này hay, viết về tình cảm nhưng mà không giống một số ngôn tình nhảm nhảm, rẻ tiền. Nhưng mà mình cảm thấy nếu mà nhân vật nữ không đẹp chắc truyện này diễn biến được có vài trang và chấm hết bằng cái chết của nữ chính. Chứ dạng bướng mà còn xấu thì đúng là thảm họa. Mình rất thích tính cách của nữ chính và nam chính, thích luôn tính của bà mẹ của nữ chính mặc dù bà này nhiều lúc suy nghĩ cực đoan kinh khủng.
4
897666
2015-12-29 18:09:19
--------------------------
357005
2575
Mình mua quyển sách này từ gợi ý của một người bạn. Vì truyện được kể về hiệp sĩ và tình yêu mang hướng quí tộc nên dù không thích cho lắm mình vẫn muốn mua và đọc thử. Đầu tiên phải nói về bìa, nó là một trong những yếu tố làm thu hút mình, không quá cầu kì mà đơn giản nhưng toát lên được chất riêng của quyển sách. Nội dung thì hay tuyệt khỏi phải nói. Tiếp đến là nội dung chủ yếu xoay quay tình yêu đầy mang tính thù hận của Aislinn và Wulfgar. Dù Wulfgar là kẻ thù của mình, là chủ nhân của mình thì nàng Aislinn dù căm hận chàng thì vẫn luôn còn lí trí, tỉnh táo và thông minh biết được những tính tốt ở chàng, nghĩ cho người dân nước Anh mà không màng bản thân mình. Những cuộc đối thoại mang tính đối đầu hay những cảnh nóng dưới ngòi bút của tác giả Kathleen do dịch giả dịch lại thì nghe như những câu thơ bay bổng, nhẹ nhàng, trau chuốt đến từng con chữ, hơn nữa là không hề có một lỗi chính tả nào dù là nhỏ nhất trong quyển sách. Mình thấy thú vị với cả cái cách Aislinn coi Wulfgar như một con sói hoang thì Wulfgar lại cứ luôn miệng gọi nàng"con cáo" nhưng với một cách trìu mến và thân thương. Tình huống truyện hấp dẫn với những nút thắt mở đẩy truyện lên cao trào. Đọc xong tập 1 thì mình may mắn được tặng tập 2 nên càng đọc càng thích và rất mãn nguyện với kết thúc có hậu, xứng đáng với tình yêu thực sự mà hai người dành cho nhau. Đây là dòng sách lãng mạn lịch sử đầu tiên mình đọc lại với một tác giả mình chưa từng nghe qua nhưng sau khi đọc xong thì mình không thể phủ nhận rằng mua quyển sách là một trong những sự sáng suốt của mình.
5
391713
2015-12-22 20:01:27
--------------------------
336762
2575
Ấn tượng với tác giả từ sau khi đọc Tro tàn trong gió, tôi đã không do dự mà chọn đọc tác phẩm tiếp theo - Sói và Bồ câu. Cuốn sách cuốn hút từng câu chữ khiến tôi đọc mà không dừng lại được. Hai nhân vật chính như hai phía đối lập, Nàng kiêu hãnh, xinh đẹp và sắc sảo, Chàng uy dũng và hoang dã. Hai người họ gặp nhau khi nàng là chiến lợi phẩm của một tay hiệp sĩ thắng trận, thân phận khác biệt, sự gặp gỡ khác biệt khiến họ luôn đối chọi nhau để rồi tình yêu như hạt giống gieo vào trong học lúc nào không biết và hạt giống ấy chỉ chờ một thời điểm thích hợp để nảy mầm, phát triển và đơm hoa. Hãy đọc để thấy được một câu chuyện tình ngỡ như hoang đường nhưng không kém phần ngọt ngào giữa hai con người vốn là kẻ thù không đội trời chung.
5
50326
2015-11-12 21:49:19
--------------------------
334549
2575
Quyết định mua sách vì bìa sách thiết kế đẹp theo kiểu cổ điển, vì những nhận xét của người đọc trước và vì phần giới thiệu khá hấp dẫn.
Câu chuyện tình yêu trong bối cảnh lịch sử đầy màu sắc được kể lại vô cùng lôi cuốn. Ngòi bút tài hoa của tác giả làm người đọc như nhìn thấy trước mắt tòa lâu đài cổ kính, những cuộc chè chén hoan hỉ, những cuộc hành quân rầm rập vó ngựa, nàng rất đẹp và chàng thật gai góc,...
Niềm kiêu hãnh, nỗi hận thù, sự chai lỳ, những dã tâm, ... xoay vòng quanh mỗi con người, mỗi số phận,... chỉ có tình yêu chân chính và tâm hồn trong trẻo là bến bờ hạnh phúc trong bình yên.
Một tác phẩm hay để đọc!
4
463360
2015-11-09 18:31:26
--------------------------
318555
2575
Câu chuyện miêu tả nhân vật thực và độc đáo. Tình tiết hấp dẫn người đọc. Nội dung được. Tình yêu của hai nhân vật chính rất thú vị. Tập 1 thì hơi nhiều cảnh tình cảm nhưng nó thật sự lôi cuốn. Cả các nhân vật phụ cũng được làm rõ tính cách và góp phần không nhỏ trong việc xây dựng một câu truyện hay. Mình đọc mà không dứt được. Tuy vậy thì mình cũng không biết truyện này có ý nghĩa gì sâu xa không, hẳn phải tìm hiểu thêm. Bìa sách thiết kế đẹp, giấy dày, tốt, bên trong còn có bookmark rất tiện lợi. 

4
847202
2015-10-06 12:33:41
--------------------------
316159
2575
Lúc nhận được sách thật sự rất ưng ý, bìa đẹp, màu đẹp. Nội dung cũng rất lôi cuốn và diễn biến tâm lý nhân vật rất thật. Sự dằn xé của Aislinn trước tình cảm nảy sinh đối với Wufgar rất đáng thương và cách diễn đạt làm cho người đọc có thể thấu hiểu được.
Lúc mình đọc thì sách bị rơi trang do keo dính không chặt. Mình đã liên hệ với Tiki Care thì được hỗ trợ hơn mức mong đợi. Tiki Care sau đó đã cử người đến lấy lại sách lỗi và chuẩn bị giao sách mới cho mình. Quá ấn tượng với cách làm việc của Tiki Care!
5
820352
2015-09-30 12:01:59
--------------------------
283316
2575
Khi nhận được bộ truyện này mình thật sự yêu ngay từ cái nhìn đầu tiên bởi sách quá đẹp, bìa dày màu nhẹ nhàng êm dịu. Đọc đến nội dung thì lôi cuốn từ những trang đầu đến cuối! Chỉ có điều tập một tập trung quá nhiều chi tiết cảnh nhân vật chính âu yếm nhau quá nhìêu, nhưng đến tập hai thực sự lôi cuốn và khi đọc hết cốt truyện vẫn,còn đọng lại trong tâm trí. Rất đáng  bỏ tg ra đọc và đáng đồng tỉèn! Tác giả biết khai thác cốt truyênn và bát tâm lý nhân vật rất logic tuy cái kết có phần đơn giản. Hy vọng tiki giới thiêun thêm nhiều sách hay nữa
5
680642
2015-08-29 21:32:24
--------------------------
279281
2575
Lần đầu mình mua sách của Kathleen sau khi đọc xong nhiều nhận xét vì mình vốn đã rất thích thể loại này và mình thật không hề thất vọng. Cảm nhận đầu tiên khi đọc sách là sách rất lôi cuốn từ cách xây dựng hình tượng nhân vật tiểu thư Aislinn vẻ ngoài yếu đuối nhưng nội tâm mạnh mẽ cùng chàng bá tước hung bạo nhưng cuốn hút. Tác phẩm rất cuốn hút bởi mạch truyện đầy bất ngờ, những câu chữ đối thoại đầy tình cảm, những cảm xúc đối lập của Aislinn vừa căm hận nhưng đầy yêu thương dành cho chàng bá tước Wolfgar.Tất cả đã làm nên một tác phẩm đậm chất lãng mạn.
5
17907
2015-08-26 17:15:44
--------------------------
242803
2575
Tôi vừa nhận được 2 quyển Sói và Bồ câu, dù chưa đọc nhưng với tôi tất cả những quyển sách của Thái Uyên đều được biên tập rất hay và kĩ lưỡng về nội dung. Nên tôi rất yên tâm chất lượng của quyển sách mình mua. Còn riêng với Kathleen thì tôi không bao giờ nghi ngờ về tác phẩm của bà. Như 2 tập Tro tàn trong gió trước của bà tôi đã mua, những tác phẩm của bà luôn trau chuốt kĩ lưỡng và câu chuyện bà viết thì ngọt ngào, ấm áp và mãnh liệt. Tôi nghĩ Sói và Bồ câu cũng sẽ không nằm ngoài dự đoán của bản thân khi tôi đặt mua. Cám ơn Thái Uyên. 
5
131519
2015-07-26 22:49:33
--------------------------
234325
2575
Tiki vừa giao hàng hôm trước, hôm qua tôi đã đọc ngấu nghiến quên ăn quên ngủ và đã xong cuốn 1, tiếp tục nghiền ngâm cuốn 2 vì không thể ngừng được. Cốt truyện không có gì mới lạ, vẫn theo motip kẻ thù không đội trời chung yêu nhau nhưng sâu sắc và cuốn hút, với nhiều chi tiết gợi cảm và lãng mạn, câu chuyện là hành trình thuần hóa chàng Sói Wolfgar của nàng bồ câu Aislinn, Wolfgar là kẻ thù đã chiếm vùng đất, lâu đài nơi nàng sinh sống, xuyên suốt mạch truyện là những dòng cảm xúc vương vấn giữa yêu và không yêu vì những lý do riêng của cả hai nhân vật, tuy vậy, cái kết cuối cùng vô cùng viên mãn với 1 cậu nhóc kháu khỉnh được sinh ra làm minh chứng cho t.y của cả hai chắc chắn sẽ làm hài lòng bạn đọc!
5
144207
2015-07-20 11:43:01
--------------------------
227923
2575
Tôi đã ngấu nghiến tập 1 này trong vòng chưa đầy 24h đồng hồ, duy chỉ một lý do là vì thời gian tôi không có nhiều và vì tôi không bỏ quyển truyện xuống được. Tôi phải kết thúc nó để còn tập trung làm những việc khác.
Nếu bạn đã đọc tập 1 thì tôi thề, khi bạn kết thúc tập một bạn sẽ phải cầm ngày tập 2 lên đọc...
Về phần văn phong thì khỏi phải nói, tôi cảm kích tác giả lẫn dịch giả vô cùng. Nếu như trong truyện "Tro tàn trong gió" bạn được dịp đọc những đoạn tả cảnh mượt như nhung thì trong "Sói Và Bồ Câu" bạn sẽ được đọc những đoạn thoại vần như thơ. Tôi không biết các bạn thế nào nhưng tôi không hề phát hiện ra bất cứ lỗi chính tả hay câu chữ thiếu sót nào. Và điều này quan trọng vì những cuốn truyện khác là vô số kể. 
Về phần nội dung, tôi đã đã đọc khá nhiều tác phẩm có những tuyến nhân vật làm ta phát điên vì tức giận, nhưng tôi chưa bao giờ thấy nhức cả đầu khi mỗi lần đọc đoạn nào có cái cô Gwyneth. May là truyện, chứ phim mà nghe cô này chanh chua chẳng thua gì kịch nói đâu. Đọc mà tôi cứ ong cả tai lên...(đó là cảm giác của tôi...hihi). Tuy nhiên, tất cả chỉ là chất xác tác cho tình yêu của hai nhân vật chính thêm phần đậm đà mà thôi. Tập hai hứa hẹn những mưu mô lớn, tôi đã dừng lại ngay đoạn hạnh phúc nhất mà mình trong đợi, đoạn này nằm ở trang đầu của tập hai. Tôi đã khóc vì mừng cho cái cô Aislinn.
Sau cùng thì cũng như mọi lần, nếu yêu thể loại sách này thì bạn phải có nó. ^^


5
181887
2015-07-14 18:07:50
--------------------------
