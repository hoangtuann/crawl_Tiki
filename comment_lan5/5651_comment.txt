458010
5651
Dường như tác giả rất thích xoay độc giả quay mòng mòng thì phải :)) Cứ ngỡ Conan lại gặp phải bọn áo đen và sắp rơi vào đường cùng, cuối cùng hóa ra lại chẳng phải ai xa lạ. Tình tiết truyện quả thật rất gay cấn, mang đến hết bất ngờ này lại đến bất ngờ khác. Nhất là trong vụ án tiếp theo, cái xác tự dưng biến mất và đội thám tử nhí bị nghi ngờ là báo tin giả. Nhưng cuối cùng cả bọn đã giải được vụ án hóc búa này nhờ vào sự thông minh của Conan, và cả sự gan dạ của những cô, cậu bé đáng yêu nữa. Thế là đội thám tử nhí được nổi tiếng rồi nhé!
5
776787
2016-06-24 19:32:55
--------------------------
385405
5651
Thám tử̀ lừng danh Conan tập 4 (tái bản 2014) quá xuất sắc hấp dẫn lôi cuốn vô cùng, mỗi tập lại là những mẫu chuyện khác nhau với tình tiết hấp dẫn khác nhau nhưng không quá trùng lập không quá khô khan tác giả đã khắc họa được nét tính cách chững chạc của cậu học sinh cấp 3 trong thân xác của cậu bé học sinh tiểu học, sự thông minh, bình tỉnh sử lí tình huống của Conan đã góp phần làm nên sự xuất thần của nhân vật và sự hấp dẫn của truyện. Lần đầu thành lập với cái tên Thám Tử Nhí Lớp 1B. Vụ án ban đầu những tưởng chỉ là tìm mèo lạc hóa ra lại trở thành vụ giết người . Rất cảm ơn tác giả đã dày công để sáng tác ra một tuyệt phẩm như thế này!
5
853243
2016-02-23 22:40:07
--------------------------
382073
5651
Truyện cũng hay, tưởng thám tử toàn căng thẳng, ai dè giờ có thêm đội thám tử nhí, siêu dễ thương luôn. Thêm tình tiết vậy cho trẻ hóa bộ truyện ra, Conan có bạn, ha ha. Mà đội thám tử nhí đấy không những không sợ khi thấy hiện trường vụ án đầy một màu đỏ, mà còn khá là bình tĩnh để giúp Conan nữa chứ, rất thú vị. Gặp mình chắc về ôm mẹ ám ảnh lâu rồi. 
Tuy là truyện mua online nhưng tiki giao hàng có tâm, truyện ko nhăn ko rớt giấy ko rách.
4
838470
2016-02-18 15:39:09
--------------------------
325320
5651
Vụ "Chiếc tủ biết nói" hóc búa. Nó làm mình nhớ tới truyện "Thảm kịch ở Styles" của Agatha Christie, hung thủ tự đổ tội cho mình rồi có chứng cứ ngoại phạm mà thoát tội. 
Nhưng không phải vậy, bởi vì nếu ông ấy cố gán cho mình là hung thủ tại sao ông ấy ngạc nhiên khi Conan hỏi ổng có chơi môn rút kiếm không ..
Thực sự ổng không nghĩ làm cho căn phòng kinh hoàng với những vết chém chằng chịt để đổ tội cho bản thân, chỉ là che giấu chỉ điểm hung thủ thôi của nạn nhân thôi, sau đó làm xong thấy căn phòng ổng nghĩ ra luôn : hay là đổ tội cho mình ?

Mình thấy ổng ngốc lắm. Ai lại đi làm căb phòng trở nên như thế, vì đánh nhau tới mức đó chắc có âm thanh nhưng có ai nghe tiếng động gì đâu, ai hiểu trinh thám thấy căn phòng vậy đoán ngay người ta muốn che giấu điều gì rồi. 

Bao Công thời niên thiếu phần 1 có tình tiết tương tự ở vụ án trên chùa, cũng ghi lại tên hung thủ và tráo đổi các hộc tủ. Đó là bộ phim giống truyện Conan nhất. 
4
535536
2015-10-23 11:26:29
--------------------------
250612
5651
Lần này thì nhóm thám tử nhí lại tiếp tục xuất hiện và còn chính thức thành lập với cái tên Thám Tử Nhí Lớp 1B. Vụ án ban đầu những tưởng chỉ là tìm mèo lạc hóa ra lại trở thành vụ giết người. Tuy với thủ đoạn tinh vi nên thủ phạm qua mặt được cảnh sát và làm nhóm thám tử nhí bị nghi oan là báo tin giả nhưng với tài suy luận của Conan cùng với sự gan dạ (cứng đầu) của các bạn mà cuối cùng thủ phạm cũng phải lộ mặt và các bạn trẻ đã được phục hồi danh dự. Hay tuyêt! Còn nhớ lần đầu tiên đọc Conan chính là tập 6, bởi vậy những chi tiết trong tập 6 như đóng dấu vào trí nhớ luôn, không quên được, nay được đọc lại thấy nhớ tuổi thơ ghê.
4
471112
2015-08-02 10:37:33
--------------------------
196848
5651
Đội thám tử nhí lần đầu ra quân :)) và chiến thắng hoàn toàn, công lớn chủ yếu là nhờ vào Conan, tất nhiên rồi :)) Một vụ án máu me kinh hoàng trong nhà tắm nhưng khi báo cảnh sát thì mọi thứ đã biến mất, làm Conan phải một phen vắt óc suy nghĩ để minh oan và tìm ra hung thủ. Nhưng hài nhất của vụ án này phải kể đến đoạn đầu, hóa ra ba cái người mà Conan cứ ngỡ là áo đen lại là ông Kusado và vợ ổng - ba mẹ của Conan - và cả bác tiến sĩ Ausaga hùa nhau đùa Conan. Mặt Conan lúc đó cười muốn chết luôn :))
4
530790
2015-05-16 16:04:39
--------------------------
176398
5651
Những vụ án lần này thật là ghê rợn, mình khi xem mà còn thấy ớn nữa là. Tuy nhiên, không hiểu sao mình lại rất thích, đặc biệt là vụ án trong nhà tắm. Bọn thám tử nhí bị một phen phát khiếp khi phát hiện cái xác trong lần phá án đầu tiên nhưng công nhận trẻ con nhìn thấy như thế mà không sợ, gan thật. Phải nói Conan thật bản lĩnh, dám đối mặt với bọn tội phạm và vạch mặt họ nữa, chắc vì máu thám tử nổi lên nên mới liều như vậy. Cuối cùng, tập này để lại cho mình nhiều ấn tượng nào là sự dũng cảm của bọn thám tử nhí và tình bạn thân thiết, hay thật là hay.
4
13723
2015-04-01 09:18:37
--------------------------
