288259
7543
Cùng Mi & Nô Học Lễ Giáo: Bạn Đã Chào Chưa?
Bé còn nhỏ, nên chưa biết được phép lễ phép với người lớn.
Một câu chuyện rất phù hợp với các bé còn nhỏ, thông qua câu chuyện Mi và Nô, sẽ giúp bé học được tính lễ phép với người lớn, và biết chào hỏi khi gặp người lớn.
Hình ảnh các nhân vật thì đáng yêu vô cùng, nào là anh Gà trống Choai, cô Gà mái Mơ, bác Bò Vàng... rất gần gũi với cuộc sống của bé nên rất dễ cho bé cảm nhận được ý nghĩa của câu chuyện.
5
323719
2015-09-03 14:34:43
--------------------------
