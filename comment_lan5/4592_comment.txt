499877
4592
Đọc đến kết thúc mở luôn mà vẫn tiếc hùi hụi, sao nhanh hết vây ̣, đọc vẫn chưa đã. Không biết chừng nào mới ra tập tiếp không đây nữa.
5
1976161
2016-12-25 19:13:35
--------------------------
465925
4592
NXB:			Văn Học
Thể loại:	             Kinh dị giả tưởng
Bìa:			Đẹp
Chất lượng in:  		Tốt
Chất lượng giấy:	Tốt
Chất lượng dịch:	Tốt
Nhận xét:		Nhập môn bói toán với thẻ xương. Lý giải các hiện tượng ma trơi, bùa yểm, trấn yểm, cách giải. Hai nhân vật chính của truyện này giông giống hai nhân vật chính trong series truyện Ma Thổi Đèn. Lối kể chuyện đơn giải, các vấn đề được giải quyết một cách khoa học. Thích nhất là đoạn kết bạn với sói thảo nguyên, tuy nhiên chi tiết sói đầu đàn là sói cái thì cần phải xem lại.
Khuyên:		Hay, nên đọc.

4
129879
2016-07-01 16:01:26
--------------------------
448648
4592
Sách thiên cơ đúng là một tác phẩm hấp dẫn mà tôi được đọc,tác giả bieest chọn tiêu đề cho câu chuyện,câu chuyện đưa người đọc vào một thế giới rất huyền bí nhưng pha lẫn nhiều nguy hiểm,cuối cùng kì thiên hạ và bố khỉ đều vượt qua tất cả.Để chứng minh cho mọi người thấy năng lực tiềm ẩn của bản thân.Ngoài ra câu chuyện còn  miêu tả rất chân thực  về tính cách của kỳ thiên hạ và bố khỉ rất là hấp dẫn,nếu như tác phẩm này không có hai nhân vật trên thì phải nói rằng tác phẩm không còn giá trị nữa,Sách thiên cơ đúng là tác phẩm của mọi tác phẩm về thể loại thần bí giả tưởng.Nhưng mà ông Túng Mã Càn Khôn này chẳng biết là ông nào,để bút danh gì mà kì lạ vậy,đúng là người trung quốc có khác,toàn tên hiệu lạ.
5
768145
2016-06-16 12:12:31
--------------------------
348799
4592
Câu truyện xoay quanh công cuộc truy tìm sự kỳ bí trong lịch sử với sức lan rộng lớn làm cho những người bạn thân quyết định đi tìm công lý cho cái thiện bị xâm chiếm tạo ra sự đấu tranh mạnh mẽ nổ ra , có nhiều thứ miêu tả huyền ảo , mờ đi dẫn nhân vật đi đến các thể dạng để con người luôn thấy tác nhân thật mới mẻ , chẳng thể nhầm lẫn với các tác giả hiện đại xứ trung hoa ngày nay , còn hấp dẫn hơn hơn các tác phẩm trước .
4
402468
2015-12-06 20:18:34
--------------------------
322792
4592
Bỏ qua phần bìa đẹp và chất lượng in tốt, cùng vs nhóm tác giả dịch rất mượt thì nội dung truyện mở ra một cốt truyện mới mẻ, về thuật chiêm bốc, như bói toán cùng đầy rẫy hiểm nguy trog cuộc hành trình của hai người bạn thân. Nhiều bạn nhận xét cốt truyện giống ma thổi đèn hay đạo mộ bút kí, nhưng mình lại thấy hoàn toàn k phải thế. Nhân vật tôi cùng người bạn thân Bố Khỉ được đặt vào hoàn cảnh gần như trái ngược nhân vật của Thiên Hạ Bá Xướng và Nam Phái Tam Thúc. Nội dug truyện cũng k hề thiên về trộm mộ mà là 1 cuộc hành trình của nhân vật "tôi" xoay quanh cuốn sách. Mình chỉ hụt hẫng về phần kết, nó ko cho mình cảm giác như chân tướng mọi chuyện đã rõ ràng, mà thay vào đó tg như gợi ra thêm nhiều chuyện để độc giả tự suy diễn. Nói chug mình cũng chỉ đánh giá sách ở mức độ khá. Mở đầu tuy ko có gì đặc sắc nhưng càng đọc sẽ càng thấy hấp dẫn hơn. Tuy nhiên ai k muốn 1 kết thúc mở thì có thể cân nhắc trc khi mua sách ^^
4
874262
2015-10-16 23:42:44
--------------------------
285605
4592
Lúc đầu đọc nhận xét thì mình khá mong chờ nhưng khi đọc thì mình lại thấy hơi thất vọng. Chất lượng giấy và mực in thì khá đẹp. Còn về nội dung thì nói thật ngoại trừ chuyện về cuốn sách ra, mình thất nhiều chỗ khá tương đồng với truyện Ma thổi đèn. Vì đã đọc qua Đạo mộ bút kí và nhiều tác phẩm đạo mộ và thám hiểm khác nên mình khoing thật sự ấn tương lắm về nội dung cuốn sách. Các yếu tố kinh dị được sử dụng nhiều nhưng chưa thật sự gây ấn tượng với mình . Cuốn này chỉ tạm ổn thôi 
3
111393
2015-08-31 23:26:01
--------------------------
180282
4592
Sách thiên cơ là cuốn sách tiếp theo thu hút tôi dau những bộ trinh thám trộm mộ như ma thổi đèn hay dạo mộ bút ký, tác giả viết rất hay, đọc không thể dứt ra được, chỉ có điểm trừ là nhân vật hơi giống trong ma thổi đèn, cùng là 2 thằng bạn chơi thân từ nhỏ. Sách XB rất đẹp, nhẹ, giấy xốp màu nhà mà mình rất thích, người dich rất chắc tay, ộ này đã đem lại cảm giác phiêu lưu và mạo hiểm đến với mình. Mong tiki cập nhật thêm những cuốn như Mắt Thiên Phật hay Bùa Chú ở phần giới thiệu cuối sách này ý nha :">
5
361752
2015-04-09 00:33:03
--------------------------
171657
4592
Đây là một tác phẩm hay, hấp dẫn và không kém phần thú vị, đặc biệt với những ai yêu thích sự huyền bí và muốn khám phá những điều huyền bí đó. Trong cuốn sach đã gợi ra vô vàn bí ẩn và lôi cuốn ngườii đọc vào những cuộc phiêu lưu để tìm ra bí ẩn đằng sau bức màn. Tuy có nhiều phần giả tưởng nhưng cũng không thiếu kiến thức lịch sử Trung Quốc hấp dẫn. Trong tác phẩm này, chúng ta vừa có thể kích thích trí tưởng tượng và khả năng suy đóan. Đó là một truyện hấp dẫn.
Bìa và hình đẹp, tạo nên sự huyền bí.
5
525123
2015-03-22 12:07:26
--------------------------
