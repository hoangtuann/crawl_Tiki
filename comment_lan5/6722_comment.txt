517245
6722
Đọc chưa xong nhưng mọi thứ tới lúc này đều ổn với tác phẩm này. Hy vọng được tiếp tục câu chuyện này với những ấn phẩm tiếp theo trong series được dịch ra việt ngữ.
4
1928278
2017-02-01 16:38:53
--------------------------
416730
6722
Thực ra mà nói điểm hài lòng duy nhất của minh với tác phẩm này chỉ là hình thức bề ngoài. Nội dung của tác phẩm này không hề tương xứng với những lời có cánh bên ngoài bìa in, nói chung là không đáp ứng được kỳ vọng của mình. Tất nhiên cảm nghĩ của mỗi người mỗi khác nhưng thực sự kết cục của truyện rất lủng củng, đôi chỗ mang tính khiên cưỡng và có nhiều tình tiết bản thân mình thấy na ná "Mật mã Da Vinci" của tác giả Dan Brown. Hình thức thì rất được, đúng chất của Nhã Nam.
3
230629
2016-04-15 16:43:25
--------------------------
344058
6722
Xét về tổng thể nội dung thì khá hay, nhưng chưa so sánh được với bất kì tác phẩm nào của Dan Brown, vì vách tổ chức truyện quá giống, cách lồng ghép các chi tiết lịch sử vào trong truyện, nhưng kém tinh tế hơn vì Dan Brown dung kí tượng học và đưa vào cả những cập nhật khoa học, những điều huyền bí mà ai cũng mong khám phá.
Kết thúc truyện chính là điểm trừ lớn, vì không thể cứ khơi khơi mà hòa cả làng được,  và cũng không đưa được ý tưởng của tác giả.
Mình mất 1 tuần mới đọc hết,  nếu so với 2 ngày của Hỏa Ngục. Đánh giá tổng thể 6/10 điểm
3
114065
2015-11-27 18:34:36
--------------------------
237406
6722
"Nhật ký bí mật của chúa" cuốn hút ban đầu từ tên tác phẩm. Những tác phẩm kết hợp đề tài trinh thám và tôn giáo luôn mang đến màu sắc huyền bí và lôi cuốn thú vị. Nếu đã từng đọc Mật mã Devinci hay Thiên thần và ác quỷ chắc chắn sẽ càng tò mò hơn với chủ đề này.

Hệ thống sâu sắc, chặt chẽ và đồ sộ làm cho chúng ta phải tập trung cao vào từng chi tiết để có thể nắm bắt được tình tiết câu chuyện, vẫn rất thích những bí mật khía cạnh tọn giáo được lồng ghép trong tác phẩm, mang đến những mảng văn hóa lịch sử quý giá, tác giả đã đầu tư khá kỳ công trong mảng chủ đề này.

Câu chuyện mở đầu lôi cuốn, tình tiết dẫn dắt cũng khá sinh động, tuy vài chỗ nho nhỏ cùng cái kết còn chưa thỏa lòng mong mỏi so với những cao trào tác giả đã viết nên nhưng đây vẫn là 1 cuốn sách hay nếu xét về tổng thể và tuyệt vời với lượng kiến thức thu được.
4
44912
2015-07-22 14:18:22
--------------------------
224696
6722
Tác phẩm với chủ đề trinh thám tôn giáo này đã không vượt qua được cái bóng quá lớn của các tác phẩm viết bởi ngòi bút của Dan Brown. "Nhật ký bí mật của Chúa" gây tò mò bởi tựa đề và cách câu chuyện bắt đầu. Tuy vậy, những chi tiết thừa thãi trong truyện là không thể chối cãi. Điểm sáng của tác phẩm là nhịp độ nhanh, câu văn trau chuốt và bài học ẩn ý qua cách câu chuyện kết thúc, dù cho nó sẽ khiến nhiều độc giả khác cảm thấy hụt hẫng. Tổng kết, "Nhật ký bí mật của Chúa" là một tác phẩm đáng nghiền ngẫm, nếu như tránh so sánh nó với những tác phẩm đồng loại đột phá hơn.
3
598425
2015-07-09 07:43:07
--------------------------
175283
6722
So với cái dòng tâng bốc ngoài bìa so sánh quyển này với Mật mã Da Vinci thì thật sự Nhật kí bí mật của Chúa gây cho mình thất vọng lớn. Cốt truyện thiếu kịch tính, tình huống mở đầu truyện đã có vấn đề rất lớn làm mình có cảm giác cả câu chuyện là một sự vô lý. Diễn biến quá dễ đoán. Hành động của các nhân vật, đặc biệt là nhân viên FBI Reily làm mình không hiểu nổi, và cũng không ưa nổi. Chưa kể nhiều đoạn có cảm giác "ăn theo" Mật mã Da Vinci. Cái kết thiếu sự bất ngờ. Và đặc biệt là ông tác giả còn lố hơn Dan Brown ở cái khoản nhồi nhét hàng tá kiến thức linh tinh ngoài lề vào, biến chúng thành những chi tiết thừa thải lê thê... Câu chuyện có vẻ nhìn nhận vấn đề tôn giáo một cách khách quan ở khía cạnh lịch sử, nhưng thật ra nó mang nặng tư tưởng cá nhân của tác giả về Thiên Chúa giáo.
Nhưng xét tổng thể thì đây cũng không phải là một cuốn sách quá tệ. Nó chỉ quá tệ nếu đem nó ra so sánh với những cuốn best seller chất lượng khác mà thôi. Vẫn đáng để đọc thử cho những ai thích khám phá.
2
494249
2015-03-29 20:38:01
--------------------------
174215
6722
với những ai yêu thích thể loại đan xen tôn giáo với thực tại kiểu như Da Vinci's code hay Angels and Demons thì đây là cuốn đáng xem.
cốt truyện theo mô típ thường thấy mà không thường thấy. có nhiều bất ngờ mà không bất ngờ, đặc biệt là càng về cuối truyện. khá hấp dẫn. đặc biệt là cái nhìn về tôn giáo và đấu tranh tâm lí của các nhân vật trong truyện
kết thúc rất mở, nhiều nút thắt chưa dc gỡ. 
vẫn còn 3 cuốn nữa trong series chưa có mặt ở VN, mong tác giả và nxb sớm dịch
4
105191
2015-03-27 16:43:25
--------------------------
163561
6722
"Nhật ký bí mật của chúa" là một tác phẩm đồ sộ và khó đọc hơn mình nghĩ lúc đầu rất nhiều. Mình đã phải cố gắng tập trung vào các chi tiết nhỏ nhặt của câu chuyện mới có thể hiểu được cái diễn biễn lắt léo, phức tạp. Tess và Sean, hai nhân vật chính, hai con người đầy tài năng với đầu óc phán đoán xuất sắc và cả niêm đam mê với khảo cổ học, đã dấn bước vào một hành trình truy tìm những kẻ tội phạm, và cũng là hành trình tìm kiếm những sự thật bị giấu kín. Tác giả phân tích khá cặn kẽ những khía cạnh lịch sử, tôn giáo thông qua suy nghĩ của hai nhân vật, tạo nên sự kết nối, tung hứng bất ngờ cho câu chuyện. Tuy nhiên cũng rất khó để nói tác phẩm này hấp dẫn, vì nó chứa nhiều kiến thức khó nhằn không phải ai cũng quen thuộc.
3
109067
2015-03-05 11:32:55
--------------------------
142415
6722
Điều đầu tiên phải nói đến là  mình thích bản dịch này, rất trơn tru, từ ngữ chọn lọc
Cốt truyện có vẻ như đi theo xu hướng thường thấy : vẫn là một cặp đôi đột nhiên vướng vào nhau qua một loạt biến cố, và rồi họ cùng sát cánh đi đến tận cùng để tìm ra sự thật cho những rắc rối đó
Mình thích cách tác giả đan xen quá khứ và hiện tại làm một, cách kể tạo ra cảm giác thần bí và bi tráng. Nhưng về phần kết, cách tác giả kết truyện khiến mình hơi hụt hẫng, ý kiến của mình là : các nhân vật đã theo đuổi một thứ chẳng đi đến đâu cả. Có nhiều hướng khác mà tác giả có thể kết thúc câu truyện, ví dụ như những gì họ tìm thấy có thể biến mất trước mất trước mắt họ, nhưng bằng cách nào đó nó vẫn được lưu giữ.
Nói chung, phần kết làm mình hối tiếc về cả nghĩa đen lẫn nghĩa bóng
3
97129
2014-12-18 16:53:56
--------------------------
