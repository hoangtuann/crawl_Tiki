470138
5873
Biết đến tác giả Trần Thu Trang qua quyển Phải lấy người như anh. Sau đó lần lượt đọc hết các tác phẩm của chị. Đặc biệt thích nội dung, cốt truyện trong các tác phẩm của Trần Thu Trang. Đã định đặt trên Tiki cuốn này nhưng tình cờ đi hội chợ sách lại mua được cuốn sách này. Đúng là không phụ lòng mong đợi. Tuyển tập là những câu chuyện tình cảm nhẹ nhàng, khiến tâm hồn có những rung động nho nhỏ xao xuyến. Tự nhiên cảm thấy yêu đời, lại có chút mơ mộng, chờ đợi, đúng là: Tình yêu còn ở đây khắp thế gian!
5
1240948
2016-07-07 10:39:12
--------------------------
358246
5873
Nhà văn nói lên sự thực của những con người cô đơn thời công nghệ , thiếu hòa nhập khi những người trẻ hiện nay chỉ biết ngồi nhìn thời gian qua đi mà không trông chờ về phía trước , chỉ nghĩ cho bản thân , mang đến hơi thở của nhịp sống nhanh , gấp gáp thì sự trầm lắng đối với một số góc phố lại thay đổi cách nhìn nhận , phóng khoáng và có phần hơi khó tính , cần sự tỉ mỉ với tâm khí , nhìn chung ở xã hội văn minh .
4
402468
2015-12-25 00:11:43
--------------------------
190232
5873
Tôi vẫn còn nhớ mình đọc "Độc thân, cần yêu" là trong một quán cafe sách trong lúc khóc lóc giận dỗi với bạn. Và tôi đã ngấu nghiến nó trong 2 tiếng đồng hồ. Những truyện ngắn của Trần Thu Trang hấp dẫn, lôi cuốn, đậm chất Hà Nội. Đôi khi, chỉ là những sự kiện rất nhỏ trong cuộc sống, nhưng được chị đúc kết rất chân thực, sâu sắc, và tôi cảm thấy "thèm yêu" sau khi đọc xong tác phẩm của chị. Và khi nhấm nháp xong, tôi rời quán cà phê và thấy cuộc đời này đẹp hơn rất nhiều. Luôn ủng hộ những tác phẩm của chị!
4
512436
2015-04-29 00:52:34
--------------------------
129341
5873
Đọc độc thân cần yêu phải nói là tôi không thể hiểu nổi và có nhiều lúc trách tác giả lại đẩy những nhân vật trong truyện vào những tình huống éo le đến như vậy . Tuy họ cũng từng phạm lỗi nhưng với tính cách đó của họ phải chịu sự trừng phạt lớn như vậy thì tôi thấy quá thực rất bất công cho họ . Nhịp điệu của truyện khá chậm các chi tiết như một đoạn băng quay chậm hiện lên trước mặt người đọc . Truyện không có nhiều tình tiết lôi cuốn nhưng lời tâm tình thổ lộ của các nhân vật rất chân thành và đặc biệt . Tôi thấy có lẽ qua tác phẩm này tác giả muốn truyền tải thông điệp quý giá giành cho những cô nàng độc thân hãy cứ yêu đi yêu hết mình để khi nhìn lại sẽ không hối tiếc vì những gì mình đã làm
4
337423
2014-10-09 08:10:09
--------------------------
