410428
4696
Mình đã mua quyển sách này được mấy hôm. Đây là một cuốn trong bộ Khám phá thế giới mà mình mới mua trên Tiki. Đọc cho bé nhà mình rất thích. Nhất là đoạn cuối cùng vì bé rất thích chơi trò ú òa mà. Cứ đến đoạn cuối là cười lăn lộn luôn. Hình ảnh trong sách cũng rất sinh động. Sách ít chữ mà chủ yếu là hình ảnh nên bé cũng thích hơn. Suốt ngày ngồi giở giở xem xem không thấy chán. Bé cũng biết thêm nhiều con vật hơn như con kiến, con sâu, con cua,... 
4
989906
2016-04-04 09:29:09
--------------------------
374023
4696
Đây là một trong những quyển sách yêu thích của con mình. Sách nhỏ, không quá nhiều trang nhưng hình vẽ minh họa rõ ràng, mang tính thẩm mĩ cao. Lời văn không nhiều nên rất phù hợp với những bé còn nhỏ tuổi. Sách viết về những cái lỗ nhỏ và những con vật chui ra từ những cái lỗ nhỏ ấy. Đó là anh sâu béo chui ra từ quả táo, đàn kiến chui lên từ lỗ cát, chú chuột chui ra ăn vụng lúa, cái tổ của anh sóc... Đặc biệt, trang cuối sách là cái lỗ được tạo nên từ đứa bé nhỏ trong chăn, muốn trùm chăn cùng với mẹ. Đọc đến trang cuối này, cùng với trò chơi ú òa trong chăn của đứa bé, con mình cảm thấy thú vị, cười khanh khách.
4
955736
2016-01-25 12:44:52
--------------------------
332485
4696
Cuốn “ Niềm vui khi thức giấc” là một trong số 12 cuốn trong bộ sách Khám phá thế giới do Nhà xuất bản Kim Đồng phát hành. Cuốn “Niềm vui khi thức giấc” có tổng cộng 21 trang, được dịch từ tiếng Hoa sang tiếng Việt. Cá nhân mình rất thích bộ truyện này và thấy chất lượng bộ truyện không hề thua kém các cuốn Ehon Nhật Bản. Lời văn được dịch ra ngắn gọn, có vần điệu với nhau, đọc lên rất thích, dễ thuộc. Mình nghĩ ba mẹ nên tìm và mua cho các bé trọn bộ truyện này để đọc cho các bé nghe trước khi đi ngủ.
3
15022
2015-11-06 13:48:19
--------------------------
