405574
7276
Không biết từ bao giờ One Piece thành một phần không thể thiếu trong cuộc sống của mình. Và 1 năm trở lại đây mình quyết định sẽ tích cóp từng chút một để có một bộ truyện đầy đủ. Cám ơn tiki đã giao hàng rất đúng hẹn và chưa một lần sai sót.
Ở tập 3 này sau tên hề Buggy đã bị đánh bại vì những việc làm không thể tha thứ của hắn. Tuy bị thương nặng nhưng Zoro vẫn thể hiện được đẳng cấp của mình và hơn hết đó là việc tin tưởng hiểu ý đồng đội. Zoro là thuyền viên đầu tiên và chắc chắn sẽ là người hiểu Luffy nhất (điều này phải theo dõi truyện mới hiểu được). Trong tập này thánh Ussop cũng xuất hiện rồi. Sẽ lại một trận chiến nữa nhưng chào mừng Ussop đến với băng SH
5
587182
2016-03-26 22:19:25
--------------------------
284169
7276
Ở tập ba này, Luffy cùng Zoro đi tìm kiếm thêm đồng đội để có thể tiếp tục tiến vào Grand Line. Và cậu đã gặp Usopp. Một người cũng có tham vọng làm cướp biển, nhưng anh ta có một tật xấu là luôn ba hoa và "chém gió" mọi chuyện. Rắc rối cũng từ đó xảy ra khi Luffy gặp Usopp. Nhưng trải qua hoạn nạn mới hiểu rõ được bản chất của nhau. Usopp thực ra là con người có tấm lòng nhân hậu và quả cảm. Luffy đã đồng ý cho Usopp tham gia vào đội hải tặc của mình, từ đó cùng nhau tiến vào Grand Line
5
77305
2015-08-30 18:19:00
--------------------------
253958
7276
Cuộc phiêu lưu của Luffy vẫn tiếp tục, cậu vẫn đang trên đường tìm kiếm thêm đồng đội  cho mình. Dừng chân trên một hòn đảo nhỏ, Luffy đã gặp được người bạn mới - thuyền trưởng Usopp. Nhìn Usopp mình liên tưởng ngay đến Punochio với chiếc mũi dài khi nói dối. Usopp cũng có một tài "chém gió" cực đỉnh, mặc dù mũi không dài được thêm nhưng bình thường mũi của Usopp đã dài sẵn rồi. Tên quản gia của gia đình Kaya cũng thật nguy hiểm, hắn định chiếm đoạt tài sản của gia đình giàu nhất làng này. Lại có một cuộc chiến hay xảy ra rồi!
5
730702
2015-08-04 21:37:24
--------------------------
197217
7276
Sau khi giải quyết được băng hải tặc Buggy thì Luffy lại tiếp tục cuộc hành trình của mình. Dừng chân tại ngôi làng nhỏ, Luffy đã gặp một người bạn mới. Phải nói là khả năng “chém gió” của anh chàng này cựa  kì ghê, gì cũng nói được hết. Tên quản gia thật là đáng sợ, hắn âm mưu giết tiểu thư Kaya để chiếm đoạt tài sản. tuy nhiên thuyền trưởng Usopp đâu thể đứng nhìn được và nhất định Luffy cũng sẽ ra tay giúp đỡ. Đến tập này mình vẫn có cảm giác hào hứng nhưng hình vẽ vẫn chưa đẹp, mình nghĩ, nếu bạn nào xem phim thì sẽ thấy đầy đủ tình tiết hơn.
5
13723
2015-05-17 10:19:24
--------------------------
