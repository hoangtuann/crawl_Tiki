516297
5942
Cuốn sách rất bổ ích cho những người thường xuyên tiếp xúc với các vết thương cần băng bó kịp thời . Các Note ghi rõ ràng , tránh cái gì , làm cái gì cho đúng , phải. nói tóm lại , đây là 1 cẩm nang cần đọc qua vì trong cuộc sống không ai lường trước được điểu gì,
4
1109671
2017-01-28 23:58:02
--------------------------
455870
5942
Cuốn sách thật sự rất là bổ ích cho đời sống. Không ai có thể đoán trước điều gù sẽ xảy ra, tai nạn gì sẽ ập xuống với mình và những người xung quanh. Biết cách sơ cứu và thêm chút kiến thức y khoa sẽ giúp ta rất nhiều. 
Sách to. Trình bày rõ ràng, rành mạch, có rất nhỉegu hình minh họa giúp ta dễ dàng tiếp cận nội dung. Tủ thuốc gia đình không thể thiếu trong nhà, thì cuốn sách này cũng là cẩm nang vô cùng cần thiết cho mỗi chúng ta. Rồi một ngày bạn sẽ cảm ơn  quyển sách.
5
1235004
2016-06-22 21:58:34
--------------------------
393114
5942
Tôi rất thích cuốn sách này. Quả thật rất bổ ích. Từ những vết thương nhẹ như xước da, ngất cho đến những chấn thương, tai nạn nguy hiểm, những bệnh tật đột xuất mà chúng ta đều có thể gặp trong gia đình, thậm chí ngoài xã hội. Chúng có thể đến bất chợt, như 1 cơn gió vậy, nhưng sẽ để lại hậu quả khó lường. Sách hướng dẫn chi tiết, cả về nguyên nhân lẫn cách chữa trị. Tôi nghĩ đây là cuốn sach tối cần thiết cho mỗi gia đình, để chúng ta có thể ứng phó kịp với các tai nạn không báo trước. Thậm chí tôi nghĩ trong những trường hợp khẩn cấp thì  1 hành động giúp đỡ theo chỉ dẫn sẽ cứu sống 1 con người. Đó là điểm quan trọng mà tôi luôn nhớ khi đọc quyển sách này.
5
774087
2016-03-08 10:46:09
--------------------------
341333
5942
     Tôi từng xem một bộ phim về cứu hộ, trong đó có tập kể về một cặp tình nhân gặp tai nạn xem máy. Cô gái bị thương nặng không thể cử động được, anh bạn trai sa khi gọi cứu hộ vì muốn bạn gái cảm thấy thoải mái, đã cởi mũ bảo hiểm của cô. Khi nhân viên cứu hộ tới nơi, sau khi kiểm tra, xem xét tình hình, một anh cứu hộ đã quá nóng nảy hỏi:" Vì sao anh cởi mũ của cô ấy? Anh đã làm gãy cổ cô ấy rồi ". Sau khi đưa đến bệnh viện, được chữa trị nhưng cổ cô ấy bị gãy, và cô ấy đã không thể đi lại được. Thay vì ở bên an ủi, động viên thì anh bạn trai không thể chấp nhận việc mình cũng là nguyên nhân gây nên bi kịch cho cô gái( dù không cố ý, chỉ thiếu kiến thức thôi), đã đổ lỗi cho đội cứu hộ và nhiều lần gọi báo động giả cho đội cứu hộ. Một tai nạn khác xảy ra, một cô gái bị xe ô tô đâm, vì nghĩ là báo động giả nên đội đến nơi chậm hơn so với bình thường và cô gái ấy đã chết vì không kịp đến bệnh viện cấp cứu.
      Điều mình muốn nói ở đây là chúng ta không thể lường được hết mọi việc, có câu ở đời không ai biết được chữ Ngờ mà. Chúng ta chỉ có thể tự hoàn thiện bản thân, tự trang bị kiến thức để khi xảy ra chuyện thì vẫn có thể bình tĩnh, làm hết sức mình để bản thân không phải hối hận, để sau này khi nghĩ lại không phải thốt lên:" Giá như, lúc đó...".
      Sách mắc một lỗi nhỏ là ở trang bìa chữ biến chứng in thành chữ:" biẹin chứng ", nhưng sách rất hay, chỉ dẫn cụ thể, có cả hình minh họa. Lại được Nữ hoàng Anh Elizabeth viết lời giới thiệu, nên nội dung và chất lượng đã được bảo chứng nhé.


5
837471
2015-11-21 20:54:10
--------------------------
311278
5942
Cuốn sách rất hữu ích, trang bị kiến thức sơ cấp cứu đơn giản cho người đọc trong nhiều tình huống khẩn cấp, dùng để tự cứu mình, người thân hay bất kỳ gặp tai nạn. Cuốn sách thực sự là cẩm nang mà những bạn trẻ mới lập gia đình nên tìm đọc, vì trẻ em thường dễ bị các tai nạn đối với các vật dụng trong nhà....

Sách có nội dung dễ hiểu, có hình minh họa cụ thể, tuy nhiên trình bày hơi rối mắt.
Bỏ qua phần trình bày thì cuốn sách thực sự rất hoàn hảo.
4
447930
2015-09-19 22:27:19
--------------------------
308484
5942
Cũng như tủ thuốc nên có trong mỗi gia đình thì quyển Cẩm Nang sơ cấp cứu trẻ em & người lớn cũng cần thiết có mặt trong mỗi gia đình như vậy. Quyển sách là những hướng dẫn cụ thể, rõ ràng nhiều tình huống nguy cấp có thể xảy đến cho mỗi người trong gia đình chúng ta. Nhờ vậy mà giúp ta có thể bình tĩnh hơn khi xử lý các tình huống cấp bách. Quyển sách này thật sự rất hữu ích cho cuộc sống chúng ta. Tuy nhiên, sách nên trình bày rõ ràng hơn, có lẽ muốn tiết kiệm trang sách để không quá dày mà bố cục hơi rối đọc hơi mệt. Hy vọng các lần tái bản sau sẽ được khắc phục điểm này. Nhìn chung quyển sách hữu ích. 
5
663152
2015-09-18 18:39:52
--------------------------
278025
5942
Quấn sách rất hay, nhiều thông tin bổ ích giúp ích mà mỗi người đều nên trang bị.
Những kiến thức cơ bản về sơ cứu, chỉ cần bỏ chút thời gian nghiên cứu nhưng  trong những trường hợp khẩn cấp có thể cứu sống tính mạng  chúng ta hoặc giúp đỡ những người xung quanh.

Tuy nhiên phải trừ 1 sao vì bố cục trang sách hơn rối mắt, dồn quá nhiều thông tin trong một trang giấy làm người đọc nhanh mệt mỏi
Sách rất đáng để mua và đọc.
 
4
288825
2015-08-25 15:44:46
--------------------------
249694
5942
Sách đẹp và rất hữu ích!
Mình đảm bảo các bạn sẽ không thấy hối hận khi mua sách này, có những kiến thức sơ cấp cứu người không trong ngành y tế sẽ không được học hoặc tìm hiểu. Vì thế cuốn sách này sẽ mang lại những kiến thức quý giá đó cho chính bạn và gia đình bạn. Trong cuộc sống có những tình huống nếu ta không chuẩn bị sẵn kiến thức bổ ích này từ trước chúng ta sẽ không biết xử lý sao cho tốt và có thể gây ra hối tiếc không đáng.
Cuốn sách hay!
5
400247
2015-07-31 22:53:25
--------------------------
220470
5942
Cuốn sách Cẩm Nang Sơ Cấp Cứu Trẻ Em và Người Lớn của tác giả Nguyễn Đính Lân cung cấp những cách sơ cấp cứu căn bản, thường thấy, thường gặp trong những tai nạn thường xảy ra phổ biến trong cuộc sống hằng ngày, là một cuốn sách bổ ích cho mọi thanh niên, thiếu niên và gia đình nên đọc để áp dụng vào cuộc sống. Thiết kế bìa minh họa trực quan sinh động, giấy tốt . Về nội dung trình bày đễ hiểu , có hình minh họa nên dễ thực hiện, tuy nhiên cần thực hành nhiều lần cho thành thục chứ nhìn dễ vậy chứ khi thực hành cũng lóng ngóng tay chân lắm
5
572238
2015-07-02 16:38:21
--------------------------
194003
5942
Mình thực sự rất vui và thích thú khi có được quyển sách này. Đây là một quyển sách rất hay, lôgic và hữu ích với những ai đã và đang nghiên cứu về y học sơ cứu và cả những ai thích và thấy nó hữu ích với mình trong cuộc sống thường ngày. Trong cuộc sống thường ngày chắc hẳn ai cũng gặp những tình huống cần sơ cứu dù là khẩn cấp hay thứ cấp thì cũng nên có trong mình những kiến thức cần thiết.nếu bạn đọc là muốn chuẩn bị hành trang cho mình bước vào ngưỡng cửa ngành y thì đây là một khởi đầu phù hợp nhất.
cảm ỏn tiki đã có những tựa sách hay và bổ ích như vậy.!!!!
5
576989
2015-05-09 11:33:58
--------------------------
