257212
6138
Mặc dù được xuất bản trước "Thỏa Hiệp Với Dịu Dàng" nhưng hình như "Nhiệt Đới Buồn" được viết sau và vì vậy phong cách viết của Phương trong hai quyển này cũng có chút gì đó không giống nhau. Tôi có biết chị thích Hội An, cũng xem vài tấm hình chụp, tựu chung là giữa chị và Hội An có cái gì đó rất nhẹ nhàng thân thiết, nên thật không lạ khi hình ảnh Hội An hiện lên rất đẹp và giàu cảm xúc. Ai không hay đọc văn của Phương hay những chia sẻ vụn vặt của chị, cũng như tiếp xúc với chị thì sẽ thấy câu chuyện trôi đi rất chậm, nhiều khi mơ màng lãng đãng, không có cao trào nhiều hay hội thoại, lại nhiều miêu tả. Nhưng với tôi đây chính là điểm mạnh của Phương. Văn chị rất gọn gàng và nữ tính. Thực ra mà nói không mấy ai có thể nhìn nhận mọi việc một cách bình thản, khiêm tốn và gần gũi như Phương, và điều này làm mọi thứ chị viết ra trở nên thật đơn giản, như chính người ta thường ao ước cuộc sống của mình như thế.

5
5412
2015-08-07 14:59:37
--------------------------
157710
6138
Nhiều bạn bè mình bảo rằng quyển Nhiệt Đới Buồn này "nhạt:. Nhưng riêng mình cảm nhận rằng nó không hề nhạt chút nào cả. Có lẽ nhiều người nhận định nó nhạt bởi cốt truyện đơn giản chăng? Quả thực mà nói thì cốt truyện này có phần đơn giản, ít điểm nhấn và thậm chí không mang nhiều cao trào nào cả? NHƯNG...Vâng! Nhưng mình thấy chiều sâu cảm xúc đã che hết tất cả những khuyết điểm đó. Nội tâm nhân vật đã được thể hiện một cách rất tinh tế và đầy sâu sắc.
Thêm vào đó là chị Phương Rong đã phát họa nên một vẻ đẹp rất cổ kính của Hội An. Mộc mạc, đơn giản mà đầy màu sắc, hoài cổ mà đẹp một cách lạ lùng.
Văn phong của chị làm mình muốn tan vào trong tác phẩm vậy! Thực sự rất cuốn hút và có hồn!
Nhiệt Đới Buồn để lại nhiều ý nghĩa sâu lắng và đầy cảm xúc!
4
303773
2015-02-09 23:21:32
--------------------------
150188
6138
Lần đầu đọc, mình cũng không thích quyển này lắm. Cảm thấy buồn ngủ nữa. Vì văn tả cảnh thì nhiều mà thoại lại quá ít. Mở đầu cũng chẳng rõ ràng. Đọc một hồi mới lờ mờ hình dung ra câu chuyện đã bắt đầu như thế nào, nhưng không có gay cấn hấp dẫn gì hết, bảo là đều đều nhàm nhàm cũng đúng đó. Mình bỏ ngang ở trang sáu mươi mấy thì phải.

Một thời gian sau, trong một lần dọn lại tủ sách, định đem thanh lý bớt vài quyển không hợp, trong đó có quyển này. Nhưng mình tiếc cái bìa quá (hì, bìa đẹp thật mà) nên mới cố ngồi lật hú họa thêm vài trang. Dạo đó tâm trạng mình cũng không ổn cho lắm, nói chung là rất chán chường và mệt mỏi.

Lần đọc thứ hai này, mình rút kinh nghiệm, không đọc từ đầu nữa mà giở ra trang mục lục phía sau xem thử có tựa chương nào thu hút được mình không. Sau đó mình đã chọn đọc phần III, Người phụ nữ biết tất cả. Đọc được vài trang thì ồ lên, à, ra đây là phần viết dưới góc nhìn của người mẹ. Một trang, hai trang, lại ba trang,... Không hiểu sao cứ vậy mà đọc cho đến hết phần III, sang phần IV rồi hết quyển luôn. Sau đó thì lật lại, đọc từ đầu, thiệt chậm. Đến lúc này, mình mới bắt đầu cảm nhận được cái hay của truyện.

Đọc xong, bảo cảm nhận thì hơi quá, chỉ là thấy cảm giác chán chường và mệt mỏi của bản thân mình phần nào vơi bớt đi. Có lẽ, kiểu người như nhân vật nữ trong câu chuyện này giờ chẳng có ngoài đời đâu. Nhưng mình thích sự lý trí trong tình cảm của cô ấy, cũng như nhận thức gia đình và tình cảm nhân thân mới là điều quan trọng nhất trong cuộc sống của cô ấy. 

Vì mình cũng là một người...muốn được ở bên mẹ mãi mãi :)
4
537702
2015-01-15 21:30:19
--------------------------
149823
6138
Tôi đi qua dãy truyện Việt Nam, thấy màu xanh mát mắt bèn xem mấy trang đầu và khi biết nó không về những câu chuyện ẩm ương vụn vặt như status facebook thì mua về.
 Nhưng quá trình đọc thật thất vọng. Truyện mỏng teo nhưng đọc mãi gần 2 tuần mới xong. Nội dung không có gì. Và cố gắng níu kéo bằng giọng văn cố giữ cho mình dịu dàng, thành ra lại giả tạo. Tôi cảm giác tác giả đã viết với một cái vỏ do mình tạo ra, muốn thể hiện một tâm hồn mộng mơ và tìm đồng cảm ở người đọc. Nhưng cảm giác chung toát lên rất là giả tạo và gượng gạo.
1
536972
2015-01-14 21:11:02
--------------------------
132175
6138
Sau khi đọc tác phẩm Nhiệt đới buồn của Phương Rong tôi bỗng nhiên muốn đi đến Hội An ghê gớm . Muốn đi thăm những cảnh đẹp đượ miêu tả đặc sắc trong truyện . Muốn được nếm những món ăn đặc sắc mà tác giả đã kể trong truyện . Cách viết của Phương khá nhẹ nhàng và êm đềm . Nó như một nước vậy mặc dù không có gì đặc biệt nhưng là yếu tố quan trọng không thể thiếu trong câu chuyện . Ngôn từ giản dị đơn giản không cầu kì phức tạp . Hay nhưng không đơn điệu . Cuốn sách khắc họa một nét đẹp riêng cổ xưa tưởng chừng đã biến mất trong tâm hồn của con người ngày nay . Hóa ra thế giới này vẫn còn rất nhiều người tốt
4
337423
2014-10-30 09:01:04
--------------------------
124769
6138
Điều đầu tiên phải nói là bìa mới của sách đẹp hơn hẳn bìa cũ, về cả tranh minh họa cũng như cách thiết kế. Bìa có hai lớp, áo ngoài như trong hình phía trên, còn áo trong chỉ một màu trắng tin, in dạ màu xanh lam tên tác phẩm và tác giả. Giản đơn mà đẹp đẽ vô cùng. Phong cách bìa của bộ 9 tác phẩm đoạt giải Văn học 20 lần V này quả thật có thể khiến độc giả khó tính nhất hài lòng về ngoại hình của sách.

Nói về nội dung của "Nhiệt đới buồn", chi bằng hẫy nói về những cảm xúc dịu dàng mà tôi cảm nhận được trong quá trình đọc. Câu chuyện chỉ đơn giản kể về một cô gái trẻ một ngày nọ bỏ việc ở thành phố và về nhà thăm mẹ. Sau đó cô đến Hội An theo một lời hẹn ước cùng người yêu cũ. Tại đó, họ gặp nhau, ôn lại chuyện cũ, rồi chia tay một cách nhẹ nhàng. Và cô gái lại lên đường trở về nhà, sống tiếp quãng đời còn lại bên cạnh gia đình, mãi mãi.

Thật sự mà nói, đây có thể là quyển có nội dung đơn giản nhất trong 9 tác phẩm đoạt giải năm nay. Nhưng chính cách viết của tác giả đã khiến cho câu chuyện trở nên truyền cảm và đáng đọc hơn rất nhiều. Văn của Phương giống như nước, mềm mại và có khoảng lặng.  Nhiều đoạn trầm sâu đến mức gây nhức nhối dù vẫn hết sức dịu dàng. Tôi nghĩ "Nhiệt đới buồn" là câu chuyện đọc để "cảm", để hòa nhịp chậm rãi vào dòng suy tưởng miên man của nhân vật, cùng cô nhìn lại tuổi trẻ của mình cũng như những tình yêu đã qua, cùng cô nhận thức ra đâu mới là điều quan trọng nhất trong đời sống của một con người. Văn của Phương có tính hướng thiện, đây là điều tôi lờ mờ cảm thấy được. Tức là điều chị mang đến cho người đọc không phải cảm giác mãn nguyện, thỏa chí hay thích thú chi chi đó, mà chính là cái cảm giác "tôi muốn sống tốt hơn." Nhắc nhớ những điều giản đơn nhưng ý nghĩa trong cuộc sống, những hạnh phúc trong tầm tay, ngay bên cạnh nhưng không phải ai cũng có thể nhận ra.

Truyện có 4 phần tất cả, với những tựa đề riêng có tính bao quát. Cá nhân tôi thích nhất phần "Mộng pháo hoa", kể về tuổi trẻ và tình yêu. Nhưng đồng thời tôi cũng xúc động rất nhiều khi đọc phần viết về mẹ, "Người phụ nữ biết tất cả." Cảm giác chân thật và rất thuyết phục. Tôi cũng muốn về sau này có thể sống thật gần bên mẹ như thế.

Hội An trong câu chuyện này thật sự rất đẹp. Tôi chỉ có thể nói như thế. Thật vui vì các tác giải trẻ ngày nay ngày càng chú ý hơn đến các địa danh của quê hương mình. Nếu có dịp, tôi muốn mang theo "Nhiệt đới buồn" của Phương đến Hội An, vừa đi lang thang khắp phố cổ vừa nhâm nhi từng chút một câu chuyện tháng Tư này. 

T/s: đã đọc văn chị P từ lâu lắm rồi, nay hay tin chị ra sách và còn đạt được giải trong cuộc thi Văn học 20, tôi thật sự cảm thấy vui lây với chị. Mong sẽ được gặp chị ở các tác phẩm sau!
5
10596
2014-09-09 11:24:27
--------------------------
