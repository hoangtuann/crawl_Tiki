447182
5656
Tập 14 của bộ truyện Thám tử lừng danh Conan đọc rất hồi hộp. Conan đã che giấu thân phận và tham gia vào đội thám tử nhí...Với tài năng của cậu, mọi vụ án đều được giải quyết rất nhanh chóng...Nhưng điều làm cho tập này rất kịch tích là khi Ran đang bắt đầu nghi ngờ về thân phận Conan chính là Kudo Shinichi. Tập này lần nữa khẳng định Tác giả Gosho Aoyama là người có biệt tài về truyện trinh thám với các ý tưởng sáng tạo độc đáo, khó đoán. Càng đọc truyện càng bị say mê. 
4
531312
2016-06-13 16:44:24
--------------------------
386726
5656
Thám tử̀ lừng danh Conan tập 11 (tái bản 2014) quá xuất sắc hấp dẫn lôi cuốn vô cùng, mỗi tập lại là những mẫu chuyện khác nhau với tình tiết hấp dẫn khác nhau nhưng không quá trùng lập không quá khô khan tác giả đã khắc họa được nét tính cách chững chạc của cậu học sinh cấp 3 trong thân xác của cậu bé học sinh tiểu học, sự thông minh, bình tỉnh sử lí tình huống của Conan đã góp phần làm nên sự xuất thần của nhân vật và sự hấp dẫn của truyện.vụ án hoa hải đường, ngày ấy, cái lúc thấy thi thể bà dì bị treo ở giếng nước, hai mắt trợn trừng, mình sợ muốn chết, tận lúc này vẫn không quên được cảm giác đấy :((. Vụ án này cũng hấp dẫn mình bởi có sự xuất hiện của bố mẹ Shinichi. Ở vụ án đầu tiên tập truyện, trong vụ án cái chết của ảo thuật gia, Shin xém thì lộ thân phận . Rất cảm ơn tác giả đã dày công để sáng tác ra một tuyệt phẩm như thế này!
5
853243
2016-02-26 01:27:35
--------------------------
386371
5656
Thám Tử Lừng Danh Conan là một bộ truyện tranh trinh thám Nhật Bản và vô cùng có sức hút đối với các bạn mê truyện tranh trinh thám ở Việt Nam.Thám Tử Lừng Danh Conan Tập 14 là cánh của mở ra các vụ án li kì tiếp theo.Tất cả các vụ án đều được khám phá và cho ra ngoài ánh sáng bởi chàng thám tử trẻ tuổi  Kudo Shinichi ,bị teo nhỏ thành học sinh tiểu học lấy tên là Conan Edogawa.Nhưng trong tập này Ran đang nghi ngờ về thân phận thật sự của cậu,vùa phải phá án vùa lo dè chừng Ran,thấy tội cho Conan thật
5
917899
2016-02-25 16:14:18
--------------------------
377705
5656
Conan là một trong những bộ truyện hay nhất mà mình đang theo dõi. Lúc nhỏ mình đọc tập này, mình thích lắm, vì bìa đẹp và còn lãng mạn nữa. Tác giả Aoyama Gosho luôn có một ý tưởng tuyệt vời và logic về cách giết người táo bạo, bìa sách được lồng ghép vào những cảnh đẹp rất tuyệt vời và mãn nhãn. Trong tập này, Conan bị Ran Mori nghi ngờ về thân phận của nhóc. Cảnh Ran gỡ tung đôi mắt kính của Conan ra đẹp lung linh luôn, ước gì trang đó được in thành trang màu thì thích biết mấy. Tuy nhiên may mắn đã đã được mẹ của Shinichi là cô Yukiko giúp đỡ.Nhưng nụ cười cuối truyện của Ran, ý nghĩa là gì? Có lẻ cô nàng vẫn còn nghi ngờ Conan.
Cảnh Ran vứt tung đôi kính ra đẹp lung linh luôn, đến giờ mà mình vẫn nhớ mãi...
5
308255
2016-02-03 09:48:42
--------------------------
360823
5656
lúc bị Ran Mori dí sát vụ giống Shinichi mình cứ tưởng mọi chuyện sẽ bị bại lộ rồi cơ chứ nhưng sau đó lại thấy vậy mới hay và giờ đây tập truyện đã kéo dài đến 85 rồi... :). vụ nhà ảo thuật gia có cái hay là giúp Ran Mori tìm ra tấm ảnh cũ về cô và Shinichi để rồi mối nghi ngờ tăng lên đến bất ngờ. từ đây về sau cũng nhiều lần Ran Mori nghi ngờ nhưng Conan đều hóa giải được hết có lẽ nhờ kinh nghiệm từ lần đầu tiên này. Mẹ Yukiko xuất hiện mình cũng ngạc nhiên nhưng hiểu bây giờ chưa có nhiều nhân vật để giúp Conan che giấu thân phận thật cho mình.
5
568747
2015-12-29 21:51:04
--------------------------
335403
5656
Bố mẹ Shinichi xuất hiện. Họ cãi nhau, giận hờn rồi tham gia vụ án. Quả là rất thú vị. 
Có một điều mình thắc mắc là nạn nhân để lại lời nhắn tố cáo hung thủ sao tự tin quá ! Chắc gì cảnh sát hiểu được thông điệp ấy mà phá án. Đã vậy còn dùng nhiều ký hiệu thuộc các lĩnh vực khác nhau và không phải chuyên môn của mình ! 
Ghi tên hung thủ là được rồi. Có lẽ sẽ bị hung thủ phát hiện rồi xoá nhưng một mật mã khó giải cũng không khác gì không để lại lời nhắn gì. 
Tập này bình thường thôi. Lại giết nhau vì tiền !! 
5
535536
2015-11-10 23:54:05
--------------------------
300030
5656
Đọc tập này mà thót tim mấy lần vì thân phận thật sự của Conan suýt nữa là bị Ran phát hiện. Đọc mà thấy tội Conan ghê, vừa phải điều tra tìm tung tích bọn áo đen mà còn vừa phải che giấu thân phận Shinichi của mình nữa, thêm cô bạn gái đa nghi như Ran thì không mệt mới lạ. Mà bất ngờ nhất khi đọc tập này là người cứu nguy cho Conan lại chính là mẹ của cậu ấy luôn, công nhận tác giả tạo hình cho nhân vật Yukiko đẹp thật, vừa có nét nhí nhảnh nghịch ngợm nhưng cũng rất ra dáng một người phụ nữ quyến rũ, mình rất thích nhân vật này mặc dù tần số xuất hiện của Yukiko trong truyện không nhiều như những vật khác. Đọc tập này thấy hai mẹ con nhà thám tử cùng nhau phá án thích ghê!
5
471112
2015-09-13 17:44:53
--------------------------
257196
5656
Tác giả Gosho Aoyama luôn có một ý tưởng tuyệt vời,sáng tạo về nội dung lẫn bìa sách. Kì này Conan đã phá được vụ án là cái chết của ảo thuật gia nổi tiếng cũng là bố của bạn học cạnh lớp 1B. Những suy luận tài tình của Conan đã tiếp tục phá được vụ án của những người bạn thuở nhỏ mẹ mình. Nhưng vẫn còn những thiếu sót, bất ngờ bố shinichi xuất hiện và giải đáp thắc mắc cho mọi người. Tiếp tục lại có thêm vụ án mạng ở khu biệt thự. Chúc các bạn luôn vui vẻ cùng Conan! 
4
614800
2015-08-07 14:51:14
--------------------------
241750
5656
Đây là một tập truyện hấp dẫn, ở tập này, Conan bị Ran nghi ngờ làm Shin cũng như bạn đọc thót tim, tuy đã được mẹ là Yukiko giúp đỡ nhưng nụ cười cuối của Ran chứng tỏ cô vẫn còn nghi ngờ Shin đấy. Tập này khá thú vị ở vụ án hoa hải đường cùng với sự xuất hiện của Kudo Yusaku, bố của Shinichi khiến tình tiết vụ án càng trở nên hấp dẫn, tôi thực sự ngưỡng mộ tài năng phá án, khả năng suy luận siêu phàm của nhân vật này. Nhìn chung taaoj 14 là một taapjcos phần gay cấn nhưng cũng khá nhẹ nhàng, tình cảm.
5
412050
2015-07-25 20:15:49
--------------------------
209038
5656
Đây là tập có sự góp mặt của đội thám tử nhí lớp 1B, các thành viên trong đội mặc dù mới vào nghề mà ai cũng tỏ ra nhanh nhẹn, hoạt bát và mang về chiến công rực rỡ khi bắt được hung thủ. Ấn tượng với mình là vụ án hoa trà. Mình không ngờ là lại có sự góp mặt của cả gia đình Shinichi cơ đấy. Mà lúc Shin sắp bị bại lộ thân phận trước mặt Ran thì may mà có mama Shin cứu chứ không thì... Cảnh Ran vứt tung đôi kính ra đẹp lung linh luôn, đến giờ mà mình vẫn nhớ mãi...
5
543425
2015-06-16 18:25:42
--------------------------
189243
5656
Đây là một trong những tập Conan đầu tiên mình đọc, đến bây giờ vẫn thấy ấn tượng với vụ án hoa hải đường, ngày ấy, cái lúc thấy thi thể bà dì bị treo ở giếng nước, hai mắt trợn trừng, mình sợ muốn chết, tận lúc này vẫn không quên được cảm giác đấy :((. Vụ án này cũng hấp dẫn mình bởi có sự xuất hiện của bố mẹ Shinichi. Ở vụ án đầu tiên tập truyện, trong vụ án cái chết của ảo thuật gia, Shin xém thì lộ thân phận. Mình cực thích cái ảnh Ran ném kính đi, xung quanh lá bay phấp phới, đẹp cực.
5
393748
2015-04-26 21:03:01
--------------------------
178712
5656
Tập này hay và đặc biệt ấn tượng bìa truyện,đẹp quá.tập này thích nhất cảnh lúc ran dẫn conan về nhà shin.ran nghi ngờ conan là shin vì khuôn mặt của conan lúc bỏ kính ra không khác nào shin hồi bè khi chụp ảnh cùng bác ảo thuật gia.sau vụ án đó ran nói dối là đi mua thức ăn khuya nhưng dẫn conan đến nhà shin và nói đó là nhà của cậu.conan tái mét luôn :v trang truyện mà ran vứt bỏ cái kính của conan thật là đẹp,hình ảnh đan xen nhau thật tuyệt vời.vụ án liên quan hoa hải đường cũng hay mà ghê !
5
586877
2015-04-05 14:42:24
--------------------------
174502
5656
Đầu tiên là bìa sách, thiết kế quá đẹp luôn, nhìn là mê liền, thích ơi là thích. Nội dung thì khỏi chê vì lần này có sự xuất hiện của mẹ Shinichi cơ mà. Điều ấn tượng nhất của mình là mẹ Shinichi trẻ ghê cơ, mà còn xiteen nữa. Phải công nhận là tác giả Gosho Aoyama vẽ đẹp cực kì, hình vẽ sống động đến nỗi mỗi lần nhìn thấy xác chết là giật mình. Không hiểu trước khi ra đời Conan, tác giả đã nghiên cứu như thế nào để lập luận logic như thế, chắc bộ óc của ông cũng ghê gớm lắm. Không những thế mà còn có hình vẽ nữa, vậy mới biết để xuất bản một quyển truyện là vất vả lắm.
4
13723
2015-03-28 10:13:23
--------------------------
