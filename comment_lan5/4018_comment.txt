415909
4018
Truyện cổ Grim bản này có nhiều truyện mình chưa đọc bao giờ, nhưng cũng có nhiều chuyện đã rất nổi tiếng, sách đóng chắc chắn, màu sắc bắt mắt, Còn nội dung thì miễn bàn rồi, anh em nhà Grim quá nổi tiếng với những câu chuyện của mình. Con mình rất thích đọc nhưng do còn bé nên các bé muốn nhìn nữa. Quyển sách này đáp ứng đầy đủ hai tiêu chuẩn "nhìn và đọc" của các bé. So với giá đã giảm thì nên mua vì quá mềm so với quyển sách đóng gáy chắc chắn, bắt mắt như vậy.
4
553752
2016-04-14 09:40:45
--------------------------
387481
4018
Cuốn truyện to đẹp với bìa cứng được làm rất công phu và chắc chắn. Truyện có nhiều hình minh hoạ rõ nét, thích hợp cho trẻ em. Lời dịch truyện tương đối hay, mang tính văn học cao, không giống như một số truyện mình đọc, lời văn rất thô và cụt ngủn. Trong cuốn truyện này có 28 câu truyện ngắn, tuy nhiên chỉ có 1, 2 câu chuyện nổi tiếng, như Hoàng tử ếch hay Sói và bảy chú dê con, những chuyện còn lại đều không hay lắm. Cuốn sách tạm được thôi nên mình đánh giá 3 sao thôi. Mặc dù nhìn cuốn truyện rất đẹp và sang.
4
373267
2016-02-27 06:18:38
--------------------------
