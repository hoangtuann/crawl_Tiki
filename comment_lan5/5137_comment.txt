570749
5137
Với mình thì quyển sách khá cuốn hút về cô gái Faith, muốn đọc đến hết để xem kết thúc câu chuyện. Tuy nhiên quyển sách này chỉ phù hợp với những người trên 18 tuổi mà thôi
4
673656
2017-04-11 22:38:04
--------------------------
427852
5137
Không biết vì mình là người chưa có nhiều kinh nghiệm tình yêu hay sao nữa mà mình nghĩ cuốn sách này chỉ mang tính giải trí không hơn không kém . 
Câu chuyện tình yêu giữa Faith và Gray liệu rằng chỉ là một tình yêu thể xác đơn thuần , một tình yêu trần tục không hơn không kém , có lẽ với mình thì Faith là một cô gái dễ dãi và dễ bị cuốn hút , Gray rõ ràng với mình là anh ấy rất hấp dẫn , nhưng với những tổn thương và bi kịch anh ấy gây ra cho tuỗi thơ và gia đình của Faith thì mình nghĩ Faith phải cứng rắn và mạnh mẽ hơn nữa.
Tuy nhiên thật ra truyện có vài diểm nhấn như cái chết của cha Gray , một vài hiểu lầm và quá trình tìm ra hung thủ cũng khá hay tuy nhiên vẫn chưa đạt tới sự hồi hộp mình mong chờ .
Về chuỗi truyện thì mình nghĩ tác giả viết khá mạch lạc và dễ đi theo mạch truyện.
Bản dịch khá ổn .
3
308892
2016-05-10 09:55:18
--------------------------
240917
5137
Trước giờ toàn đọc ngôn tình Trung quốc, đây là cuốn tiểu thuyết tình yêu phương Tây mình đọc đó. Gây ấn tượng đầu tiên chắc là do nữ chính rồi, đúng kiểu mình thích, mạnh mẽ, không hề yếu đuối, kiêu hãnh, dám quay về nơi từng là địa ngục của cô để đối mặt và dám yêu lần nữa. Nam chính để tóc dài, kiểu lãng tử rồi, mình không thích lắm. Truyện rất hay, cao trào, nhiều cảm xúc nhất là những khúc mô tả ánh mắt của các nhân vật trong truyện, những con mắt đầy cảm xúc, tình yêu thù hận lẫn lộn, khát khao và dè chừng. Kết thúc có hậu, bìa thiết kế tím mộng mơ lãng mạn ghê, giấy đẹp.
5
393545
2015-07-25 00:21:55
--------------------------
240013
5137
Đây là lần đầu tiên mình đọc một tác phẩm của tác giả  Linda Howard nên không lường trước được nó lại nóng bỏng như vậy. Cốt truyện khá ấn tượng nhưng tính cách nhân vật lại không gây được ấn tượng cho lắm. Đôi khi thấy họ cứ giằng xé giữa tâm trạng có - không. Cũng có thể do hoàn cảnh xuất thân của mỗi người, do cái " đêm định mệnh " ấy mà ra. Nhưng nếu hai nhân vật chính có thể quyết đoán hơn một chút, có thể tin tưởng nhau hơn một chút, có thể biểu lộ tình cảm của mình hơn một chút thì có lẽ tác phẩm đã khác. Cả tác phẩm mình thấy cứ như thể hai con người ấy bị chi phối bởi xúc cảm của cơ thể hơn là một chút lí trí , một chút yêu thương mà họ đáng ra nên đối diện. Nói chung, đây cũng là một tác phẩm đáng đọc.
3
107373
2015-07-24 10:58:49
--------------------------
224595
5137
Có lẽ đây là tác phẩm mà Linda Howard "ngược đãi" các nhân vật trong sách nhất, vì ngay từ cốt truyện, đã khơi lên vị thế mất cân bằng giữa Gray và Faith, một kẻ đẹp trai và ngạo mạn muốn cưa đổ cho bằng được người phụ nữ mà anh ta coi khinh nhất thị trấn. 

Faith, một cô gái đẹp nhưng lại mang trong mình cái vết nhơ của gia đình, khiến cô vừa chịu sự tủi nhục, vừa bị đả kích bởi chính người cô yêu thương và ngưỡng mộ, làm cho tôi cảm thấy uất ức thay cho cô, bởi thế, tôi mới nói, Linda đã hạ cô xuống một bậc còn dưới cả Drea - cô nhân tình của tên trùm ma túy trong "Thiên sứ tử thần" nữa.

Trái lại, hành động của Gray tôi chẳng biết cho là khôn ngoan hay dở hơi nữa, anh ta một mặt căm ghét Faith, mặt khác lại không cưỡng lại được sự cuốn hút mà muốn chinh phục cô bằng được, tình yêu của họ cứ như chơi trò rượt bắt và tranh đấu tư tưởng, giữa quá khứ kia và hiện tại, làm cho tôi thấy mệt cho cả hai, thế nhưng cuối cùng cũng có một cái kết trọn vẹn cho họ.

Điểm cộng cho quyển sách là chất lượng sách tốt, ít sai chính tả và yếu tố trinh thám luôn là những điểm gay cấn và hồi hộp nhất trong tác phẩm của Linda Howard.
Chúc các bạn đọc sách vui :)
3
34656
2015-07-08 22:27:46
--------------------------
200921
5137
Lại thêm một cuốn sách nữa của Linda Howard được xuất bản nhưng theo nhìn nhận của tôi thì tôi không đánh giá cao cuốn này bằng những cuốn trước của bà, cơ bản vì tôi không thích nhân vật nam chính của truyện. Gray đẹp trai, nhà giàu, quan hệ với nhiều phụ nữ, bản tính cũng kiêu ngạo không kém nhưng thái độ mà anh ta xử sự với Faith thật chẳng đáng nể chút nào. Vụ việc 12 năm trước thì không nói làm gì nhưng 12 năm sau gặp lại, ban đầu anh ta vẫn coi Faith như một cô gái rẻ tiền, đáng khinh trong mắt, thế mà về sau lại theo đuổi cô một cách nhiệt liệt, một mặt muốn đẩy cô đi khỏi thị trấn, mặt khác lại muốn Faith ở lại, tôi có cảm giác như từ đầu đến cuối truyện Gray chỉ muốn Faith với mục đích duy nhất là thỏa mãn nhục dục của bản thân, mọi hành động của Gray đều xuất phát từ nhu cầu của mình, chẳng thấy tình yêu ở điểm nào từ anh ta, mãi cho đến khi lời yêu phát ra từ miệng Gray tôi cũng thấy chẳng có cảm xúc gì. 

Tôi thật sự rất không thích thứ tình yêu trong truyện này, ngoài cái tình yêu gượng ép ấy ra thì phần còn lại tác giả viết khá tốt, tôi thích nhất là đoạn xảy ra 12 năm trước, thời thơ ấu của Faith. Thời điểm được miêu tả rất hay và sống động về cuộc sống khó khăn, nhục nhã của Faith hay cách mà cô chăm sóc em trai bệnh tật thật vô cùng cảm động. Theo tôi phần đầu truyện chính là phần hoàn hảo nhất, còn vụ án về sau thì cũng tạm chấp nhận được vì theo tôi vẫn còn sót lại ít gượng gạo trong việc hung thủ xuất đầu lộ diện. Nói tóm lại thì với tôi cuốn sách này chỉ xếp hàng trung trong những quyển sách mà tôi đã đọc của L.Howard.
3
41370
2015-05-26 10:42:10
--------------------------
186582
5137
Nhìn thấy tên tác giả là Linda Howard thì mình thấy đây chắc chắn là một cuốn sách nên đọc . Linda Howard dẫn dắt tình huống rất lôi cuốn và không ngừng gây bất ngờ . Đây giống như một câu truyện cổ tích với nhân vật nam chính Gray - một chàng trai hội tụ đầy đủ các yếu tố để trở thành một chàng hoàng tử trong mơ của các cô gái : đẹp trai , tài giỏi và giàu có  . Nhân vật nữ chính là Faith - một cô gái tuyệt vời nhưng không được lựa chọn gia đình của chính mình , Faith sinh ra trong một gia đình được xem là cặn bã của thị trấn , bị mọi người khinh bỉ nhưng vẫn giữ được sự hồn nhiên và có một niềm tin bất diệt vào tương lại mọi người sẽ phải nhìn cô bằng con mắt khác - một phụ nữ thành đạt , đáng được tôn trọng , Faith là một nàng Lọ Lem đúng nghĩa . Nhưng có vẻ hơi mang hướng '' ngược '' một chút . Điểm trừ duy nhất là mình không thích cách Gray đối xử với Faith : mang hướng bản năng và ép buộc . Và cả Faith nữa , cách ứng xử xưa một Faith mạnh mẽ , độc lập trên đường đời nhưng lại nhu nhược , yếu đuối và cả cam chịu , không chút phản kháng khi bên cạnh Gray dù biết Gray xem mình chỉ là loại rác rưởi . Mình đã đọc 'Quý ông hoàn hảo của Linda và thấy cuốn sách này không được cuốn hút bằng . Nhưng có một điểm cộng là Linda đã xem cả yếu tố trinh thám vào câu truyện giúp nó hấp dẫn hơn chứ không đơn thuần là một chuyện tình đơn lẻ . 
Nhưng nói chung đây là muốn cuốn sách rất hay và những người yêu thích văn học lãng mạn nên đọc .
Thêm một điểm cộng nữa là bìa sách rất đẹp và giấy tốt , dịch không có lỗi và văn phong dịch rất hay .
4
616140
2015-04-21 14:45:57
--------------------------
161489
5137
Mình luôn thích sự cuồng nhiệt nóng bỏng trong văn chương của Linda Howard nhưng mình không thích hai nhân vật này. Nữ chính Faith thì đáng yêu nhưng thỉnh thoảng lại hành động hơi ngốc nghếch làm mình phát bực. Nam chính Gray thì dạng công tử nhà giàu, ban đầu hay chửi Faith là rác rưởi gái gú này nọ. Có lẽ mình không thích mấy truyện "ngược" nên mình có cảm giác họ không phải là dành cho nhau hoặc mối quan hệ của họ quá gượng ép. Lồng thêm vào chuyện tình cảm là vụ bí ẩn về sự mất tích của cha Gray, nên câu chuyện có phần hấp dẫn hơn và giúp mình đọc đến được hết truyện. Nhưng tóm lại đây không phải là tác phẩm hay nhất của Howard.
3
116202
2015-02-28 04:48:18
--------------------------
157841
5137
Đây không phải một trong những tác phẩm của Linda Howard mình yêu thích nhất mà phải nói đây là tác phẩm của Linda Howard mà mình YÊU THÍCH NHẤT. Văn phong của Linda Howard thì vẫn thế, luôn mãnh liệt, đầy kịch tính, nóng bỏng và thu hút. Nhưng điều tạo nên sự khác biệt trong Đêm Định Mệnh chính là cách LH xây dựng tính cách cho từng nhân vật. 
Faith từ một cô gái lanh lợi, dễ thương chẳng làm gì nên tội bị cả thị trấn dè bỉu, bởi đơn giản cô mang họ Devlin. Người mẹ xinh đẹp nhưng lăng loàn và người cha nghiện ngập bất tài của Faith, anh và chị gái Faith đi theo vết xe đổ của cha và mẹ, những người dân trong thị trấn với thói tọc mạch không ngừng. Và với chàng trai trẻ Gray xưa kia luôn giận dữ nông nổi, Gray ngày nay đầy quyến rũ chín chắn và thành đạt, người mẹ lạnh lùng và cô em gái Gray chưa bao giờ thôi giận dữ với người cha đã mất tích của mình... Tất cả tạo nên một câu chuyện vừa hồi hộp kịch tính, vừa quyến rũ.
Một câu chuyện với nhiều tuyến nhân vật nhưng không hề rối. Và điểm nhấn của truyện chính là bí mật sau bao nhiêu năm dần được hé lộ, rằng có phải thật sự cha Gray đã bỏ đi với mẹ Faith hay còn bí mật khủng khiếp nào phía sau. Phải đến cuối tác phẩm người đọc mới có thể tìm ra bí mật đó. Đây chính là điểm cuốn hút khiến người đọc không thể rời mắt khỏi Đêm định mệnh cho đến phút cuối cùng. 
Thật sự đối với mình, đây là tác phẩm thành công nhất của Linda Howard.
5
125574
2015-02-10 15:34:51
--------------------------
