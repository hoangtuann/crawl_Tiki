487627
5895
Khi tôi mua về,sách dễ đọc không gây rối mắt, tôi ấn tượng về tên sách, tôi cứ nghĩ nội dung sẽ xuất hiện có một ông bụt hay một phép màu không có thật. Nhưng khi tôi đọc xong tôi thấy rất hay, có ý nghĩa đối với tôi rất nhiều.Không hề có một phép màu kỳ lạ nào hết, mà đó là phép màu của sự quyết tâm trong suy nghĩ và hành động. Để tôi biết mình cũng đang phung phí như thế nào, để tôi tiết kiệm lại để có thể làm được nhiều việc có ích hơn trong cuộc sống.
5
1227913
2016-10-25 08:32:24
--------------------------
355812
5895
1. Tên sách: Tên hay và phù hợp với nội dung của sách.
Đánh giá: 5 đ
2. Thiết kế: Bìa sách đẹp, hợp với nội dung, màu sắc tươi sáng.
Đánh giá: 5 đ
3. In ấn: Mình đọc bản ebook nên không đánh giá tiêu chí này.
4. Nội dung: Sách là câu chuyện về cậu bé Young Uh vốn rất lãng phí đã học cách tiết kiệm và kiếm tiền và cùng với những người bạn giúp đỡ người khác và trở thành người tốt. Sách khá hay, nội dung đơn giản, dễ hiểu.
Đánh giá: 5 đ
5. Nhân vật:
Young Uh: Ích kỷ, lãng phí nhưng thay đổi thành ngườ tiết kiệm, tốt bụng.
Ya Na: Giỏi giang, nhân hậu.
Đánh giá: 5 đ
Tổng điểm: 20/4 = 5 đ

5
934783
2015-12-20 20:28:31
--------------------------
238025
5895
Em đã đọc cuốn sách này và thấy rất hay.Câu truyện là từ một cậu bé lớp 4 không hề  biết tiết kiệm là gì,rất hay chi tiêu lãng phí.Trong khi đó,một tuần cậu bé chỉ được 5000 won.Vì chi tiêu quá lãng phí nên cậu còn không có tiền mua quà cho bạn nữa.Chính vì thế mà cậu bé đã ăn cắp chiếc ví hình mèo của chị mình.Và thế là cậu bé đã phải trả một cái giá'' rất đắt'' cho việc mình làm. Nhưng những trang sau,cậu bé này hoàn toàn biến thành một cậu bé khác.Cậu bé này biết nhặt vỏ chai để tái chế,biết tiết kiệm nguồn tài nguyên đang cạn kiệt dần trên Trái Đất,và cũng biết làm từ thiện.Chính vì thế mà cậu bé cũng sẵn sàng cho đi số tiền khá lớn chỉ dể lo tiền viện phí cho bà.Thực sự cuốn sách này rất,rất hay.
5
374754
2015-07-22 22:06:30
--------------------------
