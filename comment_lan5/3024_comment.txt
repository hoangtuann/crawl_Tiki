512581
3024
Mình vừa nhận được sách, sách được đóng gói cẩn thận, rất đẹp. Cuối sách có phần đáp án để khi nghe xong có thể kiểm tra lại. Nếu ai có ta định luyện nghe để thi Ielts thì nên chọn quyển này dù giá thành cao hơn so với mấy quyển kia, nhưng tiền nào thì của nấy thôi.
3
1043498
2017-01-18 17:22:11
--------------------------
427157
3024
Mình mua quyển này lúc mình bắt đầu luyện thi thôi. Mặc dù tầm của mình không gọi là cao nhưng thấy sách này nghe khá ổn, không có khó như mấy quyển của Cam, nhưng nội dung khá hay lại còn đa dạng. Đĩa nghe thì khỏi chê, trong veo luôn, mình hay copy qua điện thoại để tập nghe. Với lại sách IELTS có khác, bìa đẹp, giấy cũng đẹp, có tội luyện nghe viết note lên nhiều thì hơi tiếc. Nói chung, quyển sách này hay và phù hợp cho mấy bạn mới luyện. Chứ còn band cao hơn thì học cái này hơi chán.
2
1029132
2016-05-08 15:16:40
--------------------------
307682
3024
Về hình thức, sách khá dày, giấy tốt, mực in rõ ràng. Về nội dụng, sác được chia theo 4 levels và cứ mỗi level thì lại được chia nhỏ theo các topics khác nhau nữa. Nhìn chung, mình không cảm thấy thích quyển sách này lắm vì bài tập bị chia nhỏ lẻ, vụn ra, độ dài bài nghe lại khá ngắn. Sách gồm có 2 phần. Phần trước để nghe/làm bài và phần sau là Workbook để các bạn có thể luyện tập thêm. Mình vẫn chưa sử dụng hết quyển sách nhưng theo mình thì sách khá dễ, phù hợp cho các bạn mới bắt đầu học Ielts.
3
138740
2015-09-18 11:08:42
--------------------------
