471455
5593
Chắc hẳn khi nhắc tới Doraemon thì các bạn sẽ nghĩ ngay đến chú mèo máy đến từ tương lai cực đáng yêu phải không nào!!!,mình cũng như thế đấy ,mình biết tới chú mèo máy đáng yêu này từ lúc học mẫu giáo ,lúc đó mình chỉ xem phim chứ ít khi đọc truyện.Doraemon là một chú mèo máy rất thông minh,được Sewashi gửi về để chăm sóc và giúp đỡ cậu bé Nobita lười biếng,hậu đậu.Sản phẩm truyện tranh Doraemon-Chú mèo máy đến từ tương lai của Tiki bạn có mức giá tốt ,xuất bản lần mới nhất ,lại còn được khuyến mãi thêm quà tặng .Cảm ơn Tiki về sản phẩm truyện tranh hay và thú vị này
5
1352835
2016-07-08 15:49:34
--------------------------
388793
5593
"Doraemon" là một bộ truyện tranh của tác giả người Nhật Bản Fujio Fujiko mà mình rất yêu thích. Tác giả có nét vẽ rất sinh động, gần gũi với chúng ta: Doraemon tác giả vẽ rất đáng yêu, ngộ nghĩnh với chiếc túi thần kì không gian 4 chiều có rất nhiều bảo bối. Bộ truyện tranh này có tính giáo dục và nhân rất cao. Qua những cuộc phiêu lưu của nhóm bạn Nobita, ta rút ra được nhiều bài học ý nghĩa và sâu sắc. Hơn thế nữa, "Doraemon" là một hiện thân của tình bạn cao đẹp, lòng dũng cảm. Mình rất thích bộ truyện này, mộc mạc, đơn sơ nhưng cũng rất thần tiên, dạy cho ta nhiều điều thú vị và bổ ích.
4
1142307
2016-02-29 18:11:56
--------------------------
216402
5593
Bộ truyện được ra đời với nội dung rất lạ mà chưa có bộ truyện nào trước đây nói về nội dung này cả. Nội dung đơn giản, chỉ đặc biệt ở chỗ xuất hiện chú mèo máy thông minh mà thôi, còn lại tất cả đều rất gần gũi với chúng ta. Các bạn nhỏ, dù là ai đi chăng nữa, tôi tin rằng đều đã được đọc bộ truyện này. Tuy nhiên, dù là rất hay như vậy nhưng vẫn còn nhiểu điểu tôi nghĩ rằng không hợp lí. Tôi thắc mắc không biết rằng tại sao khi Doraemon xuất hiện trên đường phố mà mọi người lại không giật mình. Nếu là tôi, khi thấy một chú mèo như thế ra đường có lẽ sẽ rất hoảng hốt, vì nếu ở thời ấy thì mọi người vẫn chưa biết Doraemon là gì mà! Bằng chứng là khi ba mẹ Nobita mới gặp Doraemon lần đầu tiên đã phải ngất xỉu. Có lẽ tác giả muốn câu chuyện đơn giản hơn một chút vì đây là truyện của thiếu nhi, không nên rắc rối. Nhưng dù sao, câu chuyện này vẫn rất hay, bằng chứng là nó đã tồn tại rất lâu và đến tận bây giờ, các bạn nhỏ vẫn rất yêu thích chú mẻo máy đáng yêu này!
5
13723
2015-06-27 17:51:35
--------------------------
