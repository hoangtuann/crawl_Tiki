419374
7772
Bé nhà mình đặc biệt rất thích truyện Bu Bu này. Câu truyện ngộ nghĩnh về giấc mơ Bu Bu phiêu lưu cùng những giọt nước. Nó khác hẳn những truyện trước đây về Bu Bu, mới lạ và rất thu hút. Bé còn khám phá được nhiều điều thú vị về hiện tượng thiên nhiên, đó là quá trình hình thành mây và tạo mưa, cũng như nước chảy ra sông ngòi. Bé còn yêu cả bài thơ dân gian: Dạy trời mưa xuống/ Lấy nước tôi uống...
Qua truyện này, bé sẽ thích quan sát và học hỏi hơn nữa những điều hay trong cuộc sống.
5
325110
2016-04-21 09:51:56
--------------------------
