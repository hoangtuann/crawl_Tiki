558405
7222
Mình rất hài lòng khi nhận được sản phẩm, cả tiền sách lẫn tiền ship chưa tới 50 ngàn mà vẫn được đóng gói cẩn thận, giao hàng nhanh hơn dự kiến, sách trình bày rất rõ ràng và dễ viết, cảm ơn tiki nhiều ^^
5
5101033
2017-03-29 21:03:49
--------------------------
532165
7222
Mình cảm thấy cảm thấy quyển này cũg khá ổn. Tuy nhiên có từ bị viết sai thứ tự. Các bạn để ý nhé
4
1432617
2017-02-27 00:45:32
--------------------------
523721
7222
Sách đẹp , to , nhưng ô để tập viết quá ít, in đầy đủ thông tin
5
633327
2017-02-12 09:08:11
--------------------------
501789
7222
Mặc dù sách in rất rõ ràng, số lượng từ đủ để thi HSK 3 tuy nhiên cần có thêm trang để luyện viết. Số lượng ô trong sách hiện tại quá ít.
2
21972
2016-12-29 01:32:52
--------------------------
490336
7222
Cuốn tập viết này khá mỏng, nhưng đếm ra cũng phải được 600 chữ. Chất giấy mỏng, màu xanh nhìn khá bắt mắt, tuy nhiên vì có hướng dẫn viết từng nét nên một số chữ sẽ bị chiếm mất nhiều ô tự viết. Đây là một bất tiện lớn (nên mình đã photo ra thêm mẫu ô vuông để luyện thêm.)
Vấn đề tự học tiếng Trung khá phổ biến, vì idol, vì một số sở thích liên quan cần tiếng Trung. Tự học thì không thể tốt và nhanh như đi trung tâm hay trên lớp có giáo viên được, nên sẽ vì khó mà nhanh chán nhanh nản, cái đầu tiên nhanh nản là phát âm và viết chữ. Phát âm thì có nhiều âm trùng và phân biệt kiểu thêm s, kéo dài, uốn lưỡi, bật hơi... Còn chữ viết là chữ tượng hình, khác hoàn toàn chữ latinh chúng ta đã quen từ bé, nó cũng không dễ như chữ Nhật hay Hàn vì chữ to và thoáng, chữ Trung dày nét và nhiều nét kết hợp, một chữ be bé có thể có đến 17 nét. Nhìn rất phức tạp và khó nhớ. Cách duy nhất là đọc nhiều và viết nhiều. Thật sự là khó tự học. ????????????
3
530517
2016-11-12 08:47:35
--------------------------
484972
7222
Lúc cầm được sách trên tay thích lắm. Sách to, giấy đẹp. Nhưng khi viết được mấy trang thì phát hiện keo ở gáy sách không chắc lắm. Xài nhiều rất dễ bị bung. 
Hướng dẫn viết theo trình tự rất rõ ràng. Mong là sách sau này có thể sắp xếp theo dễ trước khó sau a. Với một người mới học như mình có một số chữ phần đầu hơi khó nhớ và khó viết theo thành thạo. Sách có chỉ cách đọc và phiên âm với dịch rõ ràng nên rất thích. Mặc dù phần tự viết khá ít, chỉ có 2 dòng nhưng mình có thể tự viết thêm ở ngoài vì có rất nhiều từ sách không thể in hết được.
4
907672
2016-09-28 14:14:53
--------------------------
477093
7222
Mới học tiếng trung thôi, mà tự học, nên quyết định mua 1 quyển tập viết này vì nghe mà không nhớ mặt chữ nên thấy hơi khó học.mua quyển này cảm thấy rất tốt. Dễ tập viết. Hi hi hi. Font chữ to rõ ràng.nét đậm nhạt rất tốt. Rất thích. Giấy cũng okey nữa. Không dày không mỏng. Bìa dày dài đẹp.Cầm trên tay thấy sang sang thế nào đây =)))) nói chung cực thích. Cũng không biết viết nhận xét gì thêm ngoài good good good. mới order quyển nhập môn đợi nhận hàng lại viết nhận xét tiếp cho tiki ha ha
5
843181
2016-07-24 21:02:14
--------------------------
470823
7222
Sách khổ to, giấy đẹp, in to rõ ràng thuận tiện cho việc tập viết chữ. Sách có phần giải thích nghĩa từ, hướng dẫn các nét cơ bản trong tiếng trung thuận tiện cho người mới làm quen với ngôn ngữ này. Nhưng có điều số lượng trang hạn chế, số lượng từ cũng hạn chế nên chỉ phù hợp cho những ai mới bắt đầu tìm tòi học hỏi về ngôn ngữ này. Đối với những bạn đã có vốn kiến thức, từ vựng về ngôn ngữ này thì không cần thiết phải sử dụng cuốn sách này.
4
949022
2016-07-07 23:06:55
--------------------------
444352
7222
trước tiên là mình là người chưa biết gì về tiếng Trung, được bạn bè giới thiệu mua quyển này.
 Quyển này có ưu điểm là trình bày khoa học, chữ in đậm, sắc nét, chất lượng tốt, phân chia làm các bộ dễ dàng cho người học, có giải thích bằng tiếng Việt nữa.
 Vừa hôm 6.6 đặt hàng mà hôm nay Tiki đã giao đến nơi rồi, giá trị không lớn nhưng được Tiki gói cẩn thận lắm. Sẽ tiếp tục ủng hộ Tiki. Chắc là sẽ mua thêm quyển giáo trình Hán ngữ để học cùng quyển này luôn
Cho Tiki 4 sao nhé
4
589740
2016-06-08 10:50:45
--------------------------
436846
7222
Gần đây nổi hứng đi học tiếng hoa, vào lớp thấy chị học chung có quyển sách này cũng "đua đòi" mua theo :)). Cả lớp có 3,4 người order cùng, cũng may ngay thời điểm mới có hàng, giao nhận khá nhanh. Nhận được sách rồi cảm thấy rất tốt. Sách nghe mùi mới toanh :))), chất lượng giấy khá tốt, từng chữ đều có cách đọc và dịch tiếng việt bên dưới khá dễ học. Do chị bạn có dùng dịch vụ BookCare nên được bao bì nữa, chung là rất thích. Chỉ có cái, có một số từ có quá nhiều nét, nên thành ra mấy ô luyện viết ít lại. Mình thấy sách nên bổ sung thêm một số hàng nữa để luyện viết hoặc cho thêm mấy trang ô trống để tha hồ luyện viết. Tóm lại là rất hài lòng. :D
4
31437
2016-05-27 11:51:14
--------------------------
435023
7222
Theo mình thì cuốn sách hay rõ ràng bố cục rõ thích hợp cho những bạn mới bắt đầu học tiếng trung như mình . Mình rất hài lòng về sản phẩm này vì có thể giúp mình rèn luyện được nhiều hơn với cách hướng dẫn trong sách có thể giúp mình nắm rõ hơn những kiến thức cơ bản của tiếng trung cần những nét dấu nào trước đều hướng dẫn rất rõ ràng và tỉ mỉ . Khi mình viết hết cuốn này mình sẽ mua thêm để rèn luyện thêm về kỹ năng viết các nét trong tiếng trung
5
1370706
2016-05-24 12:25:41
--------------------------
433263
7222
Theo mình, học chữ Hán khó nhất là phần viết. Học trường lớp đa phần chỉ học những chữ có sẵn rập khuôn mà không bắt đầu từ cơ bản. Chính vì vậy, mình đã chọn cuốn sách này để tham khảo và bổ sung phần cơ bản thêm vững chắc. Sách được in màu rõ và đẹp, đặc biệt phù hợp với những người mới học viết. Không chỉ hướng dẫn chi tiết đến từng nét chữ, sách còn cung cấp nhiều kiến thức quan trọng để luyện viết. Hơn nữa, đây cũng là nhà xuất bản uy tín nên mọi người đều có thể tin dùng.Một điều đáng tiết của quyển này là hàng để viết hơi ít nếu thêm nhìu hàng để viết thì thật tuyệt
5
671746
2016-05-20 17:47:04
--------------------------
432846
7222
Mình rất thích cuốn sách này, sách to, giấy khá là dày. Ban đầu thấy được bookcare cứ tưởng sách nhỏ , nhân viên giao hàng còn hỏi là to hay nhỏ , mình nói nhỏ làm người ta tìm không ra ai ngờ sách to mà cũng được bọc book care rất đẹp và chắc chắn. Giá sách rất hợp lý, tiki đóng gói và giao hàng khá nhanh. Chất lượng sách thì tốt rồi, nội dung đúng như tên sách là luyện viết tuy nhiên ô để tập viết hơi ít vì có nhiều chữ khá nhiều nét đã chiếm gần hết ô nên bắt buộc phải viết ra giấy nháp. Nhưng bù lại sách có phiên âm và dịch nghĩa, nói chung rất thích...
3
401067
2016-05-19 21:56:44
--------------------------
430448
7222
Sách viết rất chi tiết và dễ hiểu, giá cũng rất ok. Mình vừa học xong quyển này và đang tìm thêm mấy quyển nữa để học thêm. Cũng rất hài lòng về tiki từ cách gói gém sp đến cách giao hàng đều rấy nhiệt tình. Mình mua 2 quyển sách chưa tới 50k nhưng 2 cuốn nằm ở 2 kho khác nhau nên nhân viên phải ship 2 lần vậy mà chỉ tính tiền 1 lần ship. Thái độ nhân viên ship rất nhiệt tình... nói chung là sẽ ủng hộ tiki dài dài không chỉ về lĩnh vực sách mà còn về mỹ phẩm,  đồ gia dụng......
5
793238
2016-05-15 10:11:10
--------------------------
429009
7222
Mình rất hài lòng về cuốn sách này. Cuốn sách Tập viết chữ Hán này được trình bày khá bắt mắt, nói chung là đẹp. Giấy dày, chữ in rõ, khổ sách to, có phần giải thích cách viết, giải thích nghĩa của từ khá chi tiết và dễ hiểu. Nhưng có điều là phần ô trống để tự luyện viết hơi ít. Cuốn sách này khá phù hợp cho các bạn tự học Tiếng Trung và cũng tiện để bổ sung và ôn tập lại kiến thức, giúp hiểu hơn về cấu trúc cũng như cách viết của chữ Hán. 
4
1190044
2016-05-12 11:42:08
--------------------------
419688
7222
Là một sinh viên ngành việt nam học, việc nghiên cứu các văn bản chữ hán trở nên cần thiết và nó được gói gọn trong môn hán văn mà chúng tôi được dạy. Với tôi, môn này là một thử thách lớn thật sự, đây cũng là đông lực để tôi tìm kiếm một tài liệu hướng dẫn.
Cuốn sách mà tôi tìm được này là một tập hướng dẫn viết chữ hán căn bản, nó giúp tôi hình dung được chi tiết cách cấu thành chữ hán. Sách có cả phần hướng dẫn viết nét và phần ô trống cho ta luyện chữ nên ai có nhu cầu nên sử dụng nó làm tiền đề cho việc học chữ Hán.
4
1081376
2016-04-21 19:42:54
--------------------------
418149
7222
Mình mua quyển này cùng với bộ giáo trình Hán ngữ, nói chung là rất tốt, bìa ổn, và kích thước thì to hơn mình nghĩ @@ Mình hoàn toàn hài lòng về điều này <3 chất lượng giấy bên trong thì rất tốt, giấy dày, in rõ ràng, và mở đầu cuốn sách còn có các bộ, ngoài ra còn có phần kết cấu chữ Hán, hướng dẫn viết rất hay. Bên cạnh từng chữ còn có phiên âm nữa. Có 1 điểm trừ là cách gói hàng của Tiki, sách trầy trượt hết luôn @@ nhưng cũng không to tát lắm. Giá còn hạt rẻ nữa
3
1230012
2016-04-18 18:27:25
--------------------------
409045
7222
sách tuyệt vời cho người mới học tiếng trung quốc, bìa sách cứng cáp, chất liệu giấy chỉ là khá tốt mà thôi, vì chỉ cần viết bút lông kim là dễ bị lem mực ra mặt sau, nên chỉ viết tốt nhất trên bút chỉ. Số lượng chữ để tập viết hơi ít, chỉ có hai hàng nên viết không đã tay lắm. Sách có hướng dẫn cách viết rõ ràng, trình bày đầy đủ các nét trong chữ hán, sau sách còn có từ vựng sắp xếp chữ piyin rất hay để học thêm từ vựng. Tiki giao hàng nhanh, đóng gói tốt.
5
652998
2016-04-01 20:09:03
--------------------------
403950
7222
Sách hay, chữ to nhìn rất thích.hướng dẫn chi tiết các nét chữ, cách đọc nét chữ. Giải thích chi tiết rõ ràng, có cả phiên âm cả dịch nghĩa. Có các nét cơ bản đến các nét khó, các từ cơ bản đến các từ khó. Cách trình bày gọn gàng bắt mắt. Khổ giấy to cầm vào rất thích, giấy cx đẹp nữa.Cần một cái bút ngòi to để viết...^^. Trừ hơi ít giấy thôi. Nói chung là sách hay hữu ích cho những bạn mới tập viết mới học hay là những bạn thích tìm hiểu về tiếng trung.
4
1233762
2016-03-24 14:21:27
--------------------------
401555
7222
Học tiếng Hán ngoài luyện nghe và nói thì tập viết cũng vô cùng quan trọng, tình cờ lúc đang tìm kiếm sách luyện viết, tôi ghé vào trang của Tiki và đặt mua thử. Nhận được sách thấy khá hài lòng. Sách trình bày rõ, giấy dễ viết, ngoài phần luyện viết chữ theo các quy tắc viết chữ Hán còn có thêm 500 chữ Hán cơ bản ở cuối sách được giải rõ : Hình, âm, nghĩa, tả pháp, đặc biệt có chữ Hán phồn thể và giản thể để phù hợp với từng đối tượng học. Cuốn sách thích hợp với những ai muốn học nhanh và học chắc chữ Hán. Tuy nhiên phần luyện viết hơi ít.
4
1217179
2016-03-20 22:12:42
--------------------------
394219
7222
Vô tình tìm thấy đựơc trên tiki. Chất giấy dày khá ổn , kẻ ô ly rõ ràng, mực in cũng tốt. Rất phù hợp cho người mới bắt đầu học tiếng Trung.  Có rất nhiều chữ hán, nét chữ ổn và chuẩn xác. Rất bổ ích cho vịêc tập luyện viết chữ Trung Quốc. Bìa sách in màu khá đẹp nên tôi rất thích, còn có bảng chữ cái chú thích rõ ràng xác nghĩa, sách vừa tốt vừa túi tiền, có thể mua liền hai tập để củng cố luyện viết. Sách có kẻ ô ly giúp dễ dàng trong bịch canh nét chữ.
3
914250
2016-03-09 23:57:56
--------------------------
386390
7222
vừa rồi mình có học phiên âm tiếng trung nhưng thế vẫn chưa đủ còn phải viết chữ hán nhiều hơn vì mình rất sợ khi viết chữ hán ,không chỉ mình tôi và rất nhiều các bạn khác cũng như vậy. Và sau một thời gian mình lên mạng thì thấy các sản phẩm của tiki được bày bán và tìm sản phẩm cho mình tốt nhất đó là quyển tập viết chữ hán. Quyển tập viết chữ hán không chỉ là đơn thuần nhưng trong đó sách mở mang tầm hiểu biết của chúng ta ,sách có cả phiên âm và dịch nghĩa ,có các nét, chữ được viết mẫu để mình viết thạo tay hơn. Các nét chữ rõ và đều đặn. Trước khi viết thì mình viết bút chì còn viết thạo tay thì có thể viết bằng bút bi. Vì quyển sách dễ nhòe nhoẹt khi viết bằng bút mực. Có quyển viết chữ hán mình thật hài lòng, tự tin từng trang sách, mình không còn sợ viết chữ hán nữa. Kết hợp đọc và viết thì mình cố gắng hơn rất nhiều. Mong các bạn mua hàng và được giảm giá ưu đãi từ tiki nhé. Cuối cùng, chúc các bạn một ngày tốt lành.
4
1171187
2016-02-25 16:55:57
--------------------------
360169
7222
Chất lượng sách khá tốt, giấy viết tương đối đẹp. Nội dung dạy các quy tắc viết chữ khá đầy đủ, phần cuối sách còn có thêm 500 chữ Hán cơ bản cộng thêm phần phiên âm và dịch nghĩa. Cuốn sách biên soạn theo giáo trình hán ngữ nên nội dung khá phù hợp với người mới học lựa chon bộ giáo trình này. Tuy nhiên vẫn còn một số chỗ hơi khó hiểu. Trong phần tập viết chữ vẫn còn bị lỗi. Nhìn chung đây  là một cuốn sách hữu ích cho việc tập viết, giá cả lại hợp lí. 
4
531436
2015-12-28 20:25:58
--------------------------
353613
7222
Ấn tượng đầu tiên là sách khổ to, bên trong có khá nhiều thông tin, có đủ các bộ trong chữ Hán, giấy để luyện viết hơi ít, để làm mẫu là chính, bạn nào mua rồi vẫn nên kiếm một quyển vở khác để luyện cho thuộc hơn. Thích hợp với những người mới bắt đầu, như mình chẳng hạn. Sách cứng và chắc, như kiểu giáo án vậy. Ngoài ra còn có phiên âm, chỉ dẫn cách viết từng bước một.  
Một sản phẩm tốt, giá cả hợp lý. Tiki bọc sách cẩn thận, về đến nơi không bị quăn chỗ nào.

4
791184
2015-12-16 15:28:45
--------------------------
352366
7222
Mình nhận được cuốn Tập viết chữ Hán - MC books phát hành sau ba ngày đặt mua trên Tiki. Phải nói là tốc độ chuyển sách của Tiki rất nhanh luôn. Điều đầu tiên mình ấn tượng là sách được bọc cẩn thận, mở ra sách rất mới, giấy giày dặn, sáng. Quyển sách trình bày khoa học, bao gồm những từ cần thiết nhất, thích hợp cho những người mới học tiếng Trung như mình. Tuy nhiên phần tập viết lại chỉ có hai dòng, nên mình phải viết ra ngoài để tập luyện nhiều hơn.

Hi vọng cuốn sách là người bạn đồng hành trong quá trình học tiếng Trung của mình ^_^
5
1030676
2015-12-14 13:31:18
--------------------------
346415
7222
Mình đang học chuyên nghành tiếng Trung vàữ được cô giới thiệu nên đã mua cuốn tập viết chữ hán này tại tiki.Và thật sự rất bất ngờ về hiệu quả của nó.Cuốn tập viết bám sát giáo trình Hán ngữ nên dùng song song rất tốt.Tập viết in chữ to,từng nét rõ ràng,chừa chỗ tập viết cũng nhiều.Giấy đẹp,bìa cũng đẹp,đặc biệt là có đầy đủ 214 bộ trong tiếng hán.Tuy nhiên vẫn thiếu một số từ trong giáo trình Hán ngữ.Nhưng lựa chon cuốn này để viết nét bút thuận cho những người mới học thât sự là quyết định đúng đắn.
4
448401
2015-12-02 16:36:58
--------------------------
337057
7222
Mình chỉ mới bắt đầu tìm hiểu chưa bao lâu, vì chưa có điều kiện đến trung tâm nên tự học ở nhà. Đến nhà sách tìm nhưng không có mấy cuốn tập viết như này nên lên mạng tìm thì thấy ở đây có nên quyết định mua luôn. 
Rất hài lòng, rất thích hợp cho người mới bắt đầu. Sách to, chắc là cỡ A4. Chữ rõ ràng có giải thích, phiên âm luôn. Rất nhiều trang tập viết như sách của mấy bé mầm non vậy. Thích lắm. Luyện được rất nhiều chữ luôn.
Phần sau có thêm nhiều chữ..... nếu muốn thì có thể mua tập, kẻ thêm ô để viết thêm giống như là rèn luyện thêm bên ngoài vậy.
Chữ to, dễ nhìn còn có thứ tự viết từng nét nữa.
5
948454
2015-11-13 15:00:27
--------------------------
331964
7222
Trên thị trường có không ít quyển dạy viết chữ Hán, nhưng theo thiển ý của mình thì quyển này khá tiện dụng. Thứ nhất có lượng lớn chữ được giới thiệu, cũng như có phần giới thiệu cho người đọc nắm rõ được các quy tắc, các tinh thần của chữ Hán,về phần viết thì chữ đa dạng, có hướng dẫn cụ thể và kèm theo chỗ để viết thử, viết theo thứ tự từ dễ đến khó. Giấy in khá tốt,đẹp, dễ nhìn, viết êm. Giá thành thì mình thấy khá rẻ so với nhiều cuốn cùng chủ đề khác, và điều đặc biệt ở quyển này là bạn có thể kết hợp linh hoạt hoặc học với giáo viên hướng dẫn, hoặc cũng có thể tự học tại nhà khi rảnh rỗi. Nhưng tiếng Hán không dễ học, cho nên dù được hướng dẫn kỹ, mình nghĩ sự kiên trì là một yếu tố không thể thiếu.
4
564333
2015-11-05 14:00:08
--------------------------
328448
7222
Mình mua được gần 2 tháng rồi, nhưng vẫn chưa học viết hết cuốn này. Cuốn này cực kì sướng luôn. Có đầy đủ cách viết, cách đọc, nghĩa của các bộ, từ vựng; cách viết các từ rất cụ thể. Tổng cộng có 618 từ vựng. Không những thế, bìa sách khá cứng, trang sách thì dày như những cuốn sách giáo khoa xuất bản mới nhất, to thì bằng sách tiếng anh của cấp 1 hiện nay, có 120 trang. Nói chung mua cuốn này về thực sự không hề hối hận. Mình hâm mộ TFBOYS nên muốn học tiếng Trung, cuốn này đáp ứng tất cả những nhu cầu mình muốn. Vì mình chưa từng học tiếng Trung nên không biết cần những kĩ năng gì nên thấy cuốn này khá đầy đủ. 
5
473585
2015-10-29 20:33:21
--------------------------
324259
7222
Lần đầu tiên mua hàng của tiki. Cuốn sách tuy giá trị không lớn nhưng được tiki gói rất cẩn thận. Cuốn sách khá dày, trình bày rõ, giấy tốt, có phân tích rõ cách viết các nét, các bộ. Phần sau có dạy viết từng chữ cơ bản thường dùng. Nói chung mình rất hài lòng. Bạn nào học tiếng Hoa muốn viết chữ tốt thì đây là cuốn sách đáng tham khảo. Giá lại rẻ hơn giá thị trường. Kinh nghiệm của mình khi tập viết tiếng hoa là các bạn nên học theo bộ để sau này ghép chữ đỡ bị rối nhé. Ngoài ra nên sử dụng bút mực kim để nét chữ đẹp hơn.
5
830368
2015-10-20 20:33:43
--------------------------
319562
7222
Tôi mới học tiếng trung và nhận thấy rằng Hán tự( hanzi) là phần khó nhất trong tiếng trung nên chúng ta cần phải đầu tư kĩ lưỡng.
Quyển sách trình bày đẹp, giấy đẹp ,khổ lớn, chữ to rõ ràng, rẻ hơn nhà sách, đó là điều rất tuyệt vời.
Các bạn nào học tiếng trung theo giáo trình hán ngữ thì nên mua quyển này vì nó đi theo một hệ thống với quyển giáo trình hán ngữ.
Về loại bút ở trong sách không đề cập đến thì mình khuyên các bạn nên dùng bút dạ kim made in Germany hoặc bút bi nước.
Chúc các bạn học tốt
5
789535
2015-10-08 23:32:15
--------------------------
316825
7222
Đang trong giai đoạn bắt đầu học tiếng trung nên mình quýêt định mua quỷên tập viết này về tập viết. Được cái mua ở tiki giá rẻ hơn ở nhà sách, lại được cộng xu, được giảm giá nhiều lần mà chất lượng sản phẩm như nhau mình rất thích. 
Ưu điểm của sản phẩm: giấy đẹp, khổ giấy lớn, trình bày rõ ràng, dễ xem, dễ tập viết theo, rất phù hợp với việc mới học như mình. 
Nhược điểm của sản phẩm: chừa quá ít để tập viết theo. 
Đây là lần đầu tiên mình mua sp của tiki, mình rất hài lòng.
4
797627
2015-10-01 20:46:27
--------------------------
315574
7222
Bây giờ học tiếng Trung rất nhiều, mình cũng không ngoại lệ. Mình mới học và tự học nên mới mua Giáo Trình Hán Ngữ Tập 1 (Quyển Thượng 1 - Kèm CD) kèm quyển tập viết chữ Hán này để luyện chữ.
Sách khá đầy đủ, chu đáo, chi tiết sát nội dung trong quyển giáo trình.
Sách to, giấy cũng khá dày, dễ nhìn, chỉ có điều mình chưa được ưng lắm là trong sách chỉ cho kiểu nét chữ thời xưa viết bằng bút lông (Hay kiểu thư pháp) nên mình rất khó viết vì mình viết bút chì, đồ đi đồ lại thì xấu lắm. Nhưng mà những trang sau cùng lại có cách viết hiện đại nên mình cũng biết cách viết, không quá khó!

5
694162
2015-09-28 23:26:01
--------------------------
295822
7222
quyển to, giấy tốt. Có điểm tôi không thích là sách chưa nêu cách viết rõ lắm. vì thời bây giờ giới trẻ học viết tiếng Trung rất nhiều, nhưng ko phải ai cũng học chuyên sâu. Bây giờ học người ta nên viết bằng bút đi học bình thường (bút bi) hơn là bút lông như hồi xưa. Mà trong lại lại chỉ dạy sách viết hồi xưa, kiểu nét thanh nét đậm viết bằng bút lông. nên khi viết bằng bút thường thì ko thể giống bút lông được nên sẽ khác. Có số nét thì bút bi không cần nhưng bút lông thì cần. bạn nào chưa viết tiếng trung bao giờ sẽ không phân biệt được. nên có ô phân tích chữ viết bút lông, viết bút bi, chữ in trên máy tính... Điều nữa là mỗi chữ chỉ có 2 dòng ô để tập viết, mà chữ nhiều nét thì có khi nó lầm mẫu mất một dòng rồi, chỉ còn 1 dòng để tập viết, trong khi đó chữ viết dễ thì chỉ mất 3-4 ô làm mẫu, được 1 dòng rưỡi tập viết. Như vậy những chữ nhiều nét lại không được tập viết nhiều.
Đươc cái ở đây là mình được bọc, mà mình lại không đăng kí bọc nên lúc được sách rất vui(miễn phí mà) ^^
3
703839
2015-09-10 17:41:58
--------------------------
289989
7222
Mình tự học tiếng Hoa ở nhà nên mua quyển này về luyện viết chữ. Sách trình bày đẹp, hướng dẫn cụ thể, đầy đủ từ nét cơ bản đến thứ tự viết nét và các bộ chữ. Ngoài ra, còn có cả phần phụ lục 500 chữ tiếng Hoa. Mình dùng nó như một cuốn từ điển để tra từ. Đa số các chữ là dạng giản thể. Mình khá thích cuốn này nhưng nếu có thể bổ sung cách viết phồn thể thì sẽ tốt hơn. Mình trừ 1 sao vì sách của mình có vài trang bị in nhòe chữ, rất khó nhìn.
4
401010
2015-09-04 23:55:01
--------------------------
281711
7222
Mình mới học tiếng Trung, nên muốn tập viết thêm chữ Trung. Cuốn sách này mình thấy soạn rất chi tiết, hướng dẫn cụ thể  từng nét viết sao cho đúng, ngoài ra có thêm  phần 500 chữ Hán cơ bản  thích hợp cho những người mới học tiếng Trung. Loại Giấy của cuốn tập viết cũng khá tốt, dày, có thể viết trực tiếp trên đó luôn,  điểm trừ cho sách là có vài trang giấy đầu bị dính lại với nhau (không biết những cuốn khác có bị như mình không)  và phần tập viết mỗi chữ hơi ít chỗ . Tiki giao hàng nhanh chóng.
4
5134
2015-08-28 16:55:29
--------------------------
279301
7222
Theo mình thì cuốn tập viết này khá đầy đủ, chu đá. Nhưng đến phần cuối thì không được kỹ lắm, tự nhiên bỏ qua vài nét, nhiều chỗ không được chu đáo, chắc vì căn bản phần đầu đã dạy thứ tự rồi. Còn phần 500 chữ hán thường dùng mình không thích sự sắp xếp theo phiên âm, mình thích sắp xếp theo alphabet tiếng việt hơn, như vậy tìm từ cũng dễ hơn.
Giấy viết rất tốt, giày, mịn, viết lên cũng rất dễ chịu thoải mái :)
4
468807
2015-08-26 17:27:23
--------------------------
270361
7222
Mình đang học tiếng trung làm ngôn ngữ hai nên tất yếu có một quyển để tập viết chữ hán trau dồi kiến thức là rất tốt. May là lúc mình mua thì quyển sách này vẫn còn  hàng. Sách phân bố cục rất rõ ràng, giấy bên trong khá dày, sờ mịn tay, viết cũng êm bút, màu sắc khá nhã nhặn, khổ sách cũng vừa phải, giá lại rẻ, kiến thức trọng tâm. Vừa có thể học theo giáo trình của thầy cô vừ có thể tự học ở nhà. Mình khá hăm hở khi nhận được cuốn này từ tiki. Tặng bốn sao nhé.
5
471002
2015-08-18 10:23:32
--------------------------
269604
7222
Sách hướng dẫn rõ ràng về các nét cơ bản trong khi luyện viết, mô tả từng nét viết khi mình viết từng chữ, các chữ được liệt kê từ dễ đến khó giúp mình luyện dần dần. Cuối trang còn liệt kê 500 chữ cơ bản thường dùng trong tiếng Trung, rất có lợi cho mình khi ôn tập và học thêm từ mới. Điểm trừ là mỗi chữ chỉ có một dòng để tập viết, quá ít để có thể ghi nhớ và học thuộc, nhưng theo mình lượng chữ Trung Quốc rất nhiều nên đây cũng không chắc là một điểm trừ.
5
474822
2015-08-17 16:23:04
--------------------------
258298
7222
Mình bắt đầu học tiếng Trung và khi nhận được cuốn sách này, lật giở từng trang để tìm hiểu về cấu trúc của nó thì cảm nhận đầu tiên chính là: cảm thấy hăm hở để bắt đầu. Bởi vì, cuốn sách được in khá đẹp, giải nghĩa khá dễ hiểu và chi tiết; vừa có kiến thức chung, lại vừa có thể thực hành, tự học và tự tìm hiểu - khá thú vị.

Cảm nhận tiếp theo là phần dòng trống để luyện chữ có vẻ hơi ít... Nhưng ngẫm ra thì có lẽ tác giả có sự tính toàn rồi, để làm quen thì mình nghĩ 1 dòng là chấp nhận được, chăm chỉ hơn thì chúng ta sẽ kiếm cuốn tập vậy :)  

Bắt đầu học và tập viết thôi!
4
547377
2015-08-08 13:24:25
--------------------------
229622
7222
Mình rất vui vì đã chọn được 1 cuốn sách rất bổ ích để phục vụ cho việc học tiếng trung của mình. Trong sách có phần giải thích nghĩa từ, nghĩa của các bộ, hướng dẫn các nét cơ bản trong tiếng Trung, Phông chữ in đẹp, kích cỡ vừa phải, phù hợp cho các bạn mới bắt đầu học nhưng điều mà mình thất vọng nhất chính là hơi ít dòng và ô trống để luyện tập.
4
619661
2015-07-16 22:14:01
--------------------------
227297
7222
Tập Viết Chữ Hán có đầy đủ quy tắc từng nét chữ hán, giới thiệu sơ lược về bộ thủ cho các bạn mới học. Phần tập viết chữ hán của mỗi chữ hơi ít chỗ để viết nhưng bù lại cuốn sách có phần cuốn như một cuốn từ điển hán ngữ thu nhỏ gồm 5000 chữ hán cơ bản để giúp người mới học dễ dàng tập viết. Cuốn sách sẽ giúp bạn nhiều trong việc học tiếng trung nhất là với bạn không có điều kiện học trung tâm mà tự mày mò học tại nhà.
4
170935
2015-07-13 22:19:12
--------------------------
218627
7222
Mình hiện đang học tiếng Nhật nhưng vẫn mua cuốn này về để luyện viết vì mình cũng thích tiếng Trung, ngoài ra trong tiếng Nhật còn có bộ chữ Kanji vay mượn nhiều từ tiếng Trung. Thế là tiện một công 3 việc (thêm việc được học nghĩa Hán Việt của từ nữa hehe)
Điểm cộng:
- Có phần giải thích nghĩa từ, nghĩa của các bộ, hướng dẫn các nét cơ bản trong tiếng Trung.
- Phông chữ in đẹp, kích cỡ vừa phải, phù hợp cho các bạn mới bắt đầu học.
- Giấy dày, màu hài hòa, không gây nhức mắt.
Điểm trừ nho nhỏ: 
- Hơi ít dòng, ô trống để luyện tập.
5
58603
2015-06-30 18:47:42
--------------------------
215970
7222
Học tiếng Trung phần viết cực kì khó, Hán tự từ học nghĩa lẫn học viết đều khiến mình  nhức đầu. Thầy có dạy viết ở trường nhưng không đủ, nếu không luyện thường xuyên thì mau quên chữ lắm. Mình cũng học theo quyển Giáo trình Hán ngữ nên mua cuốn này viết theo rất tốt. Có điều giấy hơi mỏng nên các bạn đừng dùng viết mực, cũng đừng dùng chì bấm vì nét nhạt lắm. Nên dùng chì chuốt hoặc chì bấm ruột lớn viết mới lên nét.     
Có một nhược điểm là phần viết hơi nhiều ô ly. Mình thích phần viết chỉ là một ô vuông trắng để mình tự chia tỉ lệ để viết chữ cho quen thì tốt hơn.
4
139133
2015-06-27 01:23:54
--------------------------
209041
7222
Theo tìm hiểu thì mình mua quyển này để bắt đầu học tiếng Hán và sau một thời gian sử dụng thì thấy rất phù hợp cho những người mới bắt đầu học hoặc tự học. Quyển này trình bày các chữ trong phần luyện tập dựa trên Quyển giáo trình Hán ngữ nên khi mua thì mua kèm sẽ tăng hiệu quả hơn. 

Chất lượng giấy tuy dày hơn so với một số quyển khác nhưng chỉ nên viết chì còn viết mực thì hơi lem chút. Cách trình bày cũng hợp lý như phần đầu có giới thiệu sơ qua kết cấu chữ Hán rồi các yếu tố cấu thành. Nhìn chung sẽ không bị ngán lắm.
4
22952
2015-06-16 18:31:40
--------------------------
185385
7222
Theo mình, học chữ Hán khó nhất là phần viết. Học trường lớp đa phần chỉ học những chữ có sẵn rập khuôn mà không bắt đầu từ cơ bản. Chính vì vậy, mình đã chọn cuốn sách này để tham khảo và bổ sung phần cơ bản thêm vững chắc. Sách được in màu rõ và đẹp, đặc biệt phù hợp với những người mới học viết. Không chỉ hướng dẫn chi tiết đến từng nét chữ, sách còn cung cấp nhiều kiến thức quan trọng để luyện viết. Hơn nữa, đây cũng là nhà xuất bản uy tín nên mọi người đều có thể tin dùng.
5
433873
2015-04-19 11:40:15
--------------------------
174442
7222
Trong số các quyển luyện viết chữ Hán thì mình thấy quyển này ổn nhất. Các nét được giới thiệu đầy đủ, cộng thêm các bộ, cách viết, phiên âm và nghĩa của từ, hoàn toàn đủ để học hết cơ bản. Các ô kẻ của sách giúp chỉ dẫn cách đặt nét rất rõ ràng. Thường thì mình luyện viết bằng bút chì rồi tẩy đi viết lại, sau vài lần đã thấy nét chữ lên tay hẳn. Quyển này đi kèm với bộ giáo trình, nhưng mình chỉ tự học với app từ điển trên di động + xem phụ đề tiếng Trung trên phim chứ chưa mua sách, đến giờ vẫn thấy khá ổn, định khi nào có vốn từ kha khá sẽ bắt đầu học ngữ pháp.
5
513440
2015-03-27 23:37:00
--------------------------
161112
7222
Mình vừa mua cuốn này để ôn lại căn bản tiếng Hoa, đây là quyển sách rất tốt dành cho những bạn nhập môn. Quyển sách được phân chia theo từng phần, giải thích bố cục rõ ràng, vừa giản thể vừa phồn thể lại còn dịch sát nghĩa tiếng Việt. Và còn hướng dẫn rất chi tiết về cách viết các nét nữa nhé.
Tuy nhiên mình cảm thấy sẽ tốt hơn nếu có bảng chữ cái nguyên âm và cách phát âm những chữ đó. 
Tóm lại, mình không thể không hài lòng về sản phẩm này. Cảm ơn Tiki.vn đã cung cấp cho mình một quyển sách bổ ích nhé.
4
113812
2015-02-26 18:29:20
--------------------------
151209
7222
mình đã mua cuốn này ở nhà sách trước khi thấy giới thiệu trên tiki, nội dung đi theo bộ giáo trình hán ngữ nên hoàn toàn phù hợp cho người mới học viết Hán tự, giúp bạn dễ dàng đếm số nét và quen với cách viết bút thuận, cá nhân mình thấy nó nhiều ô ly quá thì nhìn ko hay, với lại mỗi chữ nên để viết khoảng 3 dòng thì học được nhiều hơn chỉ viết 2 dòng ^^ giấy hơi mỏng, hầu hết mn đều tập viết bằng bút chì là chủ yếu, chì bấm thì nét viết lên nhìn ko rõ nhưng do ngòi nhỏ nên dễ viết (vì khuyết điểm đã nói là nhiều ô ly quá), chì gỗ thì phải viết đồ nhiều nét mới thấy được, viết bằng mực nước đen thì nó sẽ nhem ra trang sau do giấy khá mỏng
3
28519
2015-01-18 22:15:07
--------------------------
145907
7222
Mình mới bắt đầu học tiếng Trung, Mình tính tự học ở nhà trước rồi mới đến trung tâm vì chưa có thời gian. Lượm trên mạng xin kinh nghiệm thì người ta khuyên mua cuốn này, thấy giá bán cũng rẻ nên mua luôn. Nó gắn liền với bộ giáo trình Hán ngữ, các từ mới được lọc ra từ giáo trình  Hán ngữ (6 quyển) hình như Tiki có bán nguyên bộ luôn ấy. 
+Nội dung: luyện viết theo trình tự từ chữ, gắn liền với nội dung học của giáo trình từ dễ đến khó. Hướng dẫn khá chi tiết cách viết.
+Hình thức: Giấy đẹp dễ tẩy xóa khi viết sai. Bìa đẹp, chữ viết nhiều máu sắc, rõ ràng, thu hút.
Nói chung sách đẹp giá rẻ, còn chất lượng nội dung thì không bàn nhiều vì chưa có biết  nhiều nhìn sơ thì thấy giúp người học dễ hình dung.
4
72950
2015-01-01 18:41:27
--------------------------
