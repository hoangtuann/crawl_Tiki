442032
10031
trí nhớ của mình không tốt, mục đích mua để cảm nhận trí nhớ mình đang kém ở mức nào và cách cải thiện. Cuốn Sách mỏng, với thông tin vừa đủ, lời văn đơn giản, ngắn gọn có thể ứng dụng nếu chịu khó thực hành. Nội dung có vài phần giống như cuốn sách TÔI TÀI GIỎI BẠN CŨNG THẾ  của ADAM KHOO. kiến thức cung cấp nhiều so với cái giá bỏ ra mua khi TIKI  giảm như vậy, sách tốt cho trí não và cả động lực rèn luyện sức khỏe vì có phân tích lợi ích của thể dục, ngổn thức ăn và thể thao.
4
760925
2016-06-04 10:42:41
--------------------------
415787
10031
Cuốn Rèn Luyện Trí Nhớ: 101 Mẹo Gợi Nhớ Tức Thì mỏng và nhẹ, giống như một cuốn sổ tay có thể đem bên mình một cách dễ dàng đi bất cứ nơi đâu. Sách hay nêu cho người đọc nhiều kiến thức có ích, giúp cho trí nhớ được tốt lên. Có rất nhiều phương pháp cũng như các bí kíp rèn luyện trí nhớ được nêu ra trong sách ở mọi phương diện trong cuộc sống ví dụ như nhớ tên, nhớ sự kiện, cách viết tiếng anh, nhớ những số dài hay học ngoại ngữ. Tôi nghĩ chúng ta nên tham khảo để chọn cho mình những cách rèn luyện trí nhớ tốt cho bản thân. Sách này nằm trong bộ 101 mẹo của nhà xuất bản trẻ gồm 5 cuốn, nếu mua cả bộ để làm quà tặng cho các em thanh thiếu niên  thì rất có ý nghĩa.
4
854660
2016-04-13 23:21:17
--------------------------
370322
10031
Một trí nhớ tốt có thể hỗ trợ chúng ta rất nhiều trong cuộc sống cũng như trong công việc hàng ngày. Một số người có trí nhớ rất tốt, ngược lại một số người lại thường quên rất nhanh. Cuốn sách “Rèn luyện trí nhớ: 101 mẹo gợi nhớ tức thì” của Micheal Tipper đã phân tích các nguyên nhân của chứng quên từ đó đưa ra các biện pháp rèn luyện giúp tăng cường trí nhớ từ những việc đơn giản hàng ngày. Những cách thức mà Micheal Tipper đưa ra trong cuốn sách này có thể không hoàn toàn có tác dụng đối với tất cả mọi người. Nhưng từ những phân tích khá logic của ông về chứng lãng quên, chúng ta cũng nên rèn luyện một cách kiên trì, giúp bộ não hoạt động thường xuyên. Chắc chắn sẽ ít nhiều giúp ta cải thiện trí nhớ.
4
29827
2016-01-17 15:30:05
--------------------------
307362
10031
một cuốn sách vô cùng chuẩn mực được tác giả viết một cách có trình tự, trình bày, cấu trúc đoạn văn rất logic nhưng lại bay bổng
nhưng điều k hay ở đây
là quyển sách chỉ áp dụng được cho những người thật sự thích, thật sự phù hợp
và những người đó thường thuộc vào số ít
nhưng nếu bạn quyết tâm
thì mình nghĩ những câu chỉ dẫn, những bài luyện tập trong sách sẽ giúp bạn tiến bộ vượt bậc
một cách k thể tưởng tượng được
nếu như bạn không lười nhác, chỉ biết đọc mà k chịu thực hành!
Nhưng bạn phải đúc kết nó thành kinh nghiệm bản thân của chính bạn
biến đó thành 1 thói quen
nếu không bạn sẽ lại quên
lại có 1 trí nhớ k tốt
....
5
233155
2015-09-18 03:35:05
--------------------------
282158
10031
Mình đang cần tìm phương pháp ghi nhớ tốt để có thể ghi nhớ những việc nhỏ nhặt nhất kể cả trong cuộc sống. Có nhiều phương pháp mà tác giả giới thiệu tuy có hơi khó áp dụng đối với cách ghi nhớ thông thường của bản thân, nhưng mình sẽ cố gắng thay đổi để có thể áp dụng được càng nhiều phương pháp càng tốt. Tác giả Michael Tipper đã nêu ra các nguyên nhân và giải pháp để có thể ghi nhớ tốt, và mình sẽ cần phải luyện tập để có thể ghi nhớ tốt như ông. Cảm ơn tác giả đã chia sẽ những bí quyết của mình trong quyển sách này. Ông đúng là bậc thầy trong việc ghi nhớ.
4
156324
2015-08-28 22:40:45
--------------------------
104778
10031
Trước hết cuốn sách này cũng có rất nhiều thông tin  cho chúng ta biết thêm về cách rèn luyện cũng như bảo vệ trí nhớ, nhưng cách nói của tác giả rất chung chung, cần chi tiết hơn ,còn có nhiều chỗ khó hiểu. Các phương pháp ghi nhớ  rất rời rạc, thiếu thực tế , em đã cố gắng thử nhưng không thành công, nhất là đối với cách viết Tiếng Anh, khi ta cần nhớ một từ này mà lại phụ thuộc vào một từ khác thì chẳng khác nào cần nhớ 2 từ, đối với những người vốn Tiếng Anh còn hạn hẹp như em thì không thể áp dụng. Cũng trong đó, phần đề mục Là cách ghi nhớ sự kiện mà lại hướng dẫn phương pháp nhớ công thức toán ,hóa thì em thấy không liên quan đến nhau. Nói chung, cuốn sách không thực sự hữu ích.
1
52762
2014-01-18 09:35:21
--------------------------
68175
10031
Mình đã từng nghe nói đến Michael Tipper về tài ghi nhớ đáng khâm phục của ông . mình rất hâm mộ ông về điều đó . mình đã thử nhiều cách để có trí nhớ tốt nhưng không được . không những thế mình còn cảm thấy nhức đầu , chóng mặt nói chung là không có chút khả quan . may mà bạn mình giới thiệu cho quyển sách Rèn Luyện Trí Nhớ: 101 Mẹo Gợi Nhớ Tức Thì của ông mà mình đã từ từ rèn luyện trí nhớ rất tốt . tuy mình vẫn còn trong quá trình rèn luyện nhưng mình thấy kết quả rất khả quan . mình rất thích quyển sách đó .
5
101449
2013-04-10 18:37:58
--------------------------
