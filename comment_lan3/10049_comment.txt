456752
10049
Đây là tác phẩm thứ 2 mình đọc của tác giả Nguyễn Đông Thúc, trước đó là tác phẩm "Trăm sông về biển". Sau đó mình có đọc thêm "Ngôi sao cô đơn" nhưng đây là quyển thứ 2 mình đọc của tác giả :D Phải nói là ngôn từ của tác giả rất được. Như một bạn đã bình luận trước đó là "tác giả viết rất chắc tay". Quả thật là vậy. Tác giả giống như một đầu bếp tài ba, thêm các gia vị rất vừa phải, không bị lạt hay mặn quá. Mình mua quyển này ban đầu vì cái tên của nó nghe rất "cô đơn", khá giống với cái tâm trạng lúc ấy của mình, khi về nhà thì lại đâm ra ghiền cái bìa, nhìn đẹp lắm, không như bìa của quyển "Ngôi sao cô đơn". Nội dung sách có nhiều bạn bình luận rồi, mình không nói lại nữa nhưng phải nói là sách của Nguyễn Đông Thúc rất có chiều sâu, đọc hay! Hy vọng sau này sẽ được đọc nhiều sách của bác hơn.
5
34607
2016-06-23 18:14:16
--------------------------
434030
10049
Ba nhân vật Thương, Phong, Tiềm là bạn học của nhau nhưng biến cố năm 1975 đã thay đổi số phận của họ, thay đổi tình bạn của họ và thay đổi chính bản thân họ. Cuốn sách tuy chỉ có  ba nhân vật chính nhưng mỗi người một cuộc đời chìm nổi giữa thời cuộc biến động. 
Tác giả miêu tả rất tinh tế tâm lý của 3 nhân vật chính, cũng như mối quan hệ giữa 3 người bạn thân từng thề nguyền rằng không có gì và không một ai” có thể thay đổi tình bạn của họ. Cách dẫn dắt câu chuyện rất lôi cuốn, làm người đọc tự đặt câu hỏi liệu 3 nhân vật chính sẽ sống như thế nào trước những biến cố cuộc đời? Đó cũng chính là điểm hấp dẫn của cuốn sách khiến độc giả theo đến những trang cuối cùng. Đây là một tác phẩm hay, đáng đọc và suy ngẫm.

4
471112
2016-05-22 10:17:06
--------------------------
346565
10049
Đây là tác phẩm thứ 2 của tác giả mà mình đọc, đầu tiên là Ngọc trong đá, so với Ngọc trong đá thì truyện này mang một cường độ nhẹ hơn, tác phẩm cũng lồng vào đây khá nhiều lời bài hát, nếu như người đọc đã từng nghe chúng thì sẽ dễ cảm hơn. Yếu tố chính trị được đưa vào rất nhiều, rất thích hợp cho thế hệ trẻ hình dung cụ thể hơn về một giai đoạn có nhiều biến chuyển của đất nước khách quan hơn. Rất thích cách tác giả đi sâu vào miêu tả tâm lý 3 nhân vật chính, đa dạng nhiều hình thức, rất lôi cuốn. Sẽ tìm đọc các tác phẩm còn lại của tác giả.
4
112537
2015-12-02 22:54:19
--------------------------
189170
10049
Tác giả viết rất chắc tay... Tất cả đều rất vừa phải và tế nhị...
Thoáng qua cứ tưởng đây là một tiểu thuyết tình yêu tay 3 ủy mỵ và phức tạp giữa 3 người bạn... Nhưng thực tế đây là một trang đau thương của lịch sử với ba số phận của ba người bạn thân sau ngày đất nước thống nhất...
Lồng ghép vào mối quan hệ tình cảm của 3 người bạn tâm giao chí cốt (được mô tả rất tinh tế) là những thực tế của lịch sử... không ồn ào và làm quá nhưng cũng đủ cho một người trẻ 26 tuổi như tôi cảm nhận được những gian nan và khó khăn xuyên suốt một thời đau thương hậu chiến tranh của đất nước.
Hơn nữa, tác phẩm còn trao cho người đọc những quan điểm tốt đẹp về cuộc sống này... Rằng "không có gì và không một ai" có thể đánh đổ hết những điều bình dị và nhân văn trong cõi đời này... 
Hy vọng mọi người sẽ đọc và cảm nhận nhiều hơn ^^
5
620263
2015-04-26 18:14:21
--------------------------
