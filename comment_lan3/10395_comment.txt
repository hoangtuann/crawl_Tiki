191309
10395
Giọng văn nhẹ nhàng đầy cảm xúc lôi cuốn người đọc khiến bạn cảm thấy tò mò về tình tiết , diễn biến câu chuyện đôi khi kịch tính cao trào.
Tính cách cùng tình cảm của các nhân vật đều có sự trưởng thành không quá gò ép .Tất cả  đều khá tự nhiên.
Tình cảm con người quả  thật là khó đoán, trái tim của người mình yêu lại càng khó đoán hơn đôi khi lại khiến bản thân thực sự chẳng hiểu gì.
Tính cách Nhâm Nhiễm khá dứt khoát, rõ ràng không dấu diếm khiến tôi thích. Đôi khi hạnh phúc cho bản thân đơn giản là vậy. Thích là thích và theo đuổi không thích là không thích. Cô ấy quả thật đã hi sinh khá nhiều khi chọn ở bên cạnh Gia Thông.
4
393052
2015-05-01 13:27:19
--------------------------
117110
10395
Nội dung tuy cũ mà mới, cũ là ở chỗ, hai chàng trai tính cách trái ngược cùng tranh giành một cô gái, mới là ở chỗ câu chuyện không bị bó hẹp ở một địa điểm cụ thể, nó khiến người đọc có cảm giác như được phiêu lưu vậy! Chỉ có điều mình không thích phần bìa cho lắm, dường như người chọn bìa chỉ lấy tạm một ảnh đầy rẫy trên mạng và đem đi in sách vậy, có lẽ không phù hợp với nội dung cho lắm. Tại sao không chịu khó bỏ chút thời gian để vẽ bìa minh họa cho phù hợp. Tuy có tốn kém hơn nhưng chắc chắn sẽ khiến bạn đọc hài lòng hơn.
3
352231
2014-07-15 11:59:11
--------------------------
91125
10395
     Một câu chuyện viết về tình yêu thế nhưng mình nghĩ đó là viết về quá trình trưởng thành của cô gái Nhâm Nhiễm. Từ môt con người con gái ngây thơ qua những biến cố cuộc đời đã trở thành một phụ nữ trưởng thành và chín chắn. Đọc câu chuyện, không chỉ có tình yêu mà nó còn làm ta phải suy nghĩ về nhiều thứ. Câu chuyện viết rất thực tế và mình cũng rất thích nhân vật Nhâm Nhiễm này. Lúc yêu Kỳ Gia Thông và nhất quyết đi theo anh, cô hết mình vì tình yêu chăm sóc anh dù biết được đối với Gia Thông cô không phải là tất cả, cô luôn ý thức được hành động ngông cuồng nhưng không phải là nông nổi này của mình khi đi theo Gia Thông nhưng cũng không hề hối hận với sự lựa chọn của mình. Vài năm sau, cô gái ngang bướng ấy đã trở thành một người phụ nữ trầm lặng đầy lí trí, tuy còn rất yêu Gia Thông nhưng vẫn giữ được thái độ hờ hững của mình sau tất cả nhưng gì Gia Thông đã làm, chứ không yếu lòng như những cô gái trong truyện khác. Có lẽ đây chính là nhân vật nữ mình yêu thích nhất trong những truyện đã từng đọc.
4
104038
2013-08-16 07:54:57
--------------------------
87266
10395
Một tiểu thuyết tình yêu nhưng lại có chiều sâu cùng với nhiều tầng lớp ý nghĩa, những giá trị chân thực.
Những biến cố thăng trầm của cuộc đời Nhâm Nhiễm cũng như diễn biến nội tâm của cô được tác giả khắc họa rất chân thực và sống động. Từ một cô gái ngây thơ trong sáng, trải qua bao nhiêu khó khăn vất vả của cuộc đời, cô đã thay đổi, trở thành người có tính cách lạnh lùng và trầm lặng.
Còn nhân vật Kỳ Gia Thông, một nhân vật rất đáng ngưỡng mộ. Dù bị Nhâm  Nhiễm thờ ơ lạnh lùng, anh vẫn cứ kiên trì bám víu tình yêu mà mình theo đuổi không buông, tấm chân tình của anh đã làm cảm động Nhâm Nhiễm, cảm hóa trái tim cô.
Thật may mắn là sau bao nhiêu gian nan thử thách, họ lại được ở bên nhau.
5
130823
2013-07-16 19:24:38
--------------------------
80323
10395
Sự đỗ vỡ của gia đình, hành động bồng bột trong tuổi trẻ, những biến cố trong cuộc đời đã khiến cho Nhâm Nhiễm từ một cô gái trong sáng, ương bướng, vui vẻ trở thành một cô gái trầm lặng, thậm chí bị mắc bệnh trầm cảm. Tình cảm của cô với Kỳ Gia Tuấn chỉ đơn thuần là bạn bè thân thiết nhất. Còn tình yêu của cô với Kỳ Gia Thông hay Trần Hoa tuy bắt đầu ở độ tuổi mười tám nhưng nó đã khắc cốt ghi tâm, đi theo cô mãi trong suốt thời gian còn lại cuộc đời. Mặc dù cô đã cố gắng loại trừ hình bóng người ấy ra khỏi cuộc sống của mình, nhưng Trần Hoa lại làm điều ngược lại. Anh dùng đủ mọi cách để có thể xuất hiện trong cuộc đời cô. Anh biết mình đã trả giá cho hành động sai lầm, thiếu suy xét ngày trước. Do vậy, dù cho bị cô thẳng thừng từ chối bao nhiêu lần, bị cô ruồng bỏ bao nhiêu lần, anh như miếng keo dính gỡ hoài không ra. Sự thấu hiểu giữa cô và anh đã đạt đến cảnh giới không cần nói cũng hiểu được suy nghĩ của người kia. Vậy thì ai có thể thay thế được người kia trong trái tim người này đây. Thử thách cuối cùng đã khiến cho họ trở lại bên nhau, trở lại như những ngày đầu. Hạnh phúc có được khi tưởng chừng đã mất đi khiến cho họ càng quý trọng từng giây từng phút bên nhau hơn.
5
63376
2013-06-12 12:13:07
--------------------------
19016
10395
Truyện của Thanh Sam Lạc Thác luôn kén người đọc bởi nó sâu sắc và cần sự kiên nhẫn để cảm toàn bộ câu chuyện. Nhâm Nhiễm là một cô gái dịu dàng nhưng kiên cường, mạnh mẽ. Tuổi thanh xuân của cô đã trải qua biết bao biến cố nhưng cô vẫn dũng cảm vượt qua tất cả. Kỳ Gia Thông - một chàng trai lịch lãm, trưởng thành và tài giỏi đã luôn yêu Nhâm Nhiễm đến cùng. Hai người yêu nhau nhưng tình yêu của họ vẫn vấp phải những thăng trầm của cuộc sống, những biến cố không ai có thể lường trước được. Khép lại câu chuyện, ta sẽ có rất nhiều điều để suy ngẫm, về tuổi trẻ, về những sự lựa chọn, về tình yêu, về cuộc sống nhưng suy cho cùng tất cả chỉ còn là quá khứ và ta vẫn phải tiếp tục hướng về phía trước. Nhìn về nơi đã từng là những ánh đèn xa hoa rực rỡ nhưng liệu có còn rạng ngời như xưa ...
4
4911
2012-02-10 19:07:11
--------------------------
