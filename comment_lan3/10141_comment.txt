80310
10141
"Những người tình cô đơn kỳ lạ của tôi" là một tác phẩm đặc biệt. Đặc biệt bởi nội dung truyện, bởi phong cách và lối dẫn dắt của tác giả, và bởi cả những cảm xúc mà mỗi trang sách đem đến cho người đọc. Tác giả đào sâu những diễn biến tâm lý phức tạp, khắc họa tinh tế hình tượng nhân vật, đồng thời sử dụng những câu văn vừa giàu tính triết lý vừa gần gũi, nhẹ nhàng, làm câu chuyện này trở nên tươi mới, hấp dẫn, lôi cuốn một cách kỳ lạ. Xuyên suốt toàn bộ tác phẩm, người đọc như chìm đắm trong những suy tư, những trăn trở về cuộc sống, về tình yêu và con người. Cách viết của tác giả rất chân thực, sống động, nhưng cũng có gì đó lãng đãng, mơ hồ khó nắm bắt. Chính những điều này đã để lại những dư âm khó quên cho độc giả sau khi đọc xong tác phẩm này.
4
109067
2013-06-12 11:36:35
--------------------------
76405
10141
Ấn tượng của mình là quyển sách này khá mỏng so với 76.000 ( giá bìa ), nhưng mình cũng đọc xem sao. Điểm cộng là trang bìa được thiết kế rất đẹp và tinh tế, lời văn của tác giả rất lôi cuốn, hấp dẫn và trau truốt, tình tiết cũng khá hợp lý, mạch truyện cũng khá ổn. Khi mình đọc phần giới thiệu, dường như  Taurus và nhân vật '' tôi'' đã yêu nhau từ cái nhìn đầu tiên, nhưng theo mình cảm nhận thì không phải thế, họ đã có những lần nuôi dưỡng tình cảm nên họ mới yêu nhau, không đúng như phần giới thiệu,   truyện còn một vài chỗ khó hiểu khiến mình phải đọc đi đọc lại nữa. Tuy vậy thì đây cũng là một truyện cũng đáng xem.
3
57869
2013-05-22 18:45:32
--------------------------
40352
10141
Thực sự, tôi đã bị ấn tượng mạnh - nếu như không muốn nói là bị ám ảnh bởi căn phòng ngập đầy thằn lằn và gián trong đây. Nếu tôi là nhân vật chính, có lẽ khi tỉnh dậy tôi sẽ ngay lập tức ngất đi thêm lần nữa, haha.

Một cốt truyện đặc biệt thú vị, thu hút độc giả từ những chương truyện đầu tiên và khiến những ai đang đọc dang dở không hề muốn lìa xa một chút nào. Một cuốn truyện như thế không phải tác giả nào cũng viết lên được. An Hạ không phải là một ngòi bút quá xuất sắc, nhưng cô luôn biết cách dẫn dắt câu chuyện của mình theo một hướng vô cùng đặc biệt và thu hút đến lạ.

"Những Người Tình Cô Đơn Kỳ Lạ Của Tôi" đáng được gọi là cuốn sách gối đầu giường cho ai đang muốn tìm cảm giác mạnh, haha ~

5
34863
2012-09-20 13:16:08
--------------------------
34686
10141
Mới được một người bạn tặng cho cuốn sách này, tôi đã hoan hỷ vì bạn tôi giới thiệu rằng cuốn sách có nội dung phá cách rất táo bạo, miêu tả nội tâm sâu sắc và truyền tải những triết lý sâu sắc. Thế nhưng, ngoài văn phong của tác giả ra, tôi không có gì để yêu thích ở cuốn sách này. Nhiều khi thấy thật đáng buồn vì mình sống trong một thời đại héo hon đến thế, nơi người ta chối bỏ không chịu nhìn nhận bản chất thức sự của tình yêu là những gắn bó lâu dài, sự thấu hiểu, đồng cảm được vun đắp dần qua những trải nghiệm chung. Tiếc thay, tác giả chỉ dùng giọng văn lôi cuốn của mình để thêm mắm dặm muối cho một câu chuyện tình yêu sét đánh phi lý, và những tưởng tượng rất trẻ con về một thứ viễn tưởng như " bản thể tinh thần" hay những tâm hồn có sự tương giao. Việc đưa vào những hình ảnh ghê rợn như thằn lằn rắn rết thực chất chỉ là 1 chiêu câu khách. Đặc biệt điều tôi thấy phản cảm nhất chính là mối quan hệ đồng tính nữ được miêu tả lộ liễu trong câu chuyện. Xã hội đã thay đổi, nó tha hóa nhiều và dường như người ta có xu hướng thích những thứ bất thường, bệnh hoạn để giải trí, còn hơn nhìn nhận những giá trị đích thực, cũ kĩ nhưng luôn đúng về tình yêu một nam một nữ.
2
48540
2012-07-24 15:53:50
--------------------------
34632
10141
Cuốn sách mang đến cho tôi những hình dung kì ảo về một thế giới siêu thực. Có những hình ảnh, những tính cách đẹp đến đáng say mê, như chàng trai chải chuốt mang bên mình bầu không khí quyến rũ u buồn, hay người đàn bà bí ẩn trong ngôi biệt thự trắng, tiếng nói như chuông ngân và những xúc chạm có phần phi giới tính. Cách tác giả dùng giọng văn trầm ngâm, nhấn nhá để miêu tả những mối quan hệ kì lạ, tình cảm không thể gọi tên, và những gắn kết tinh thần khiến khiến những con người trong câu chuyện như vừa đấu tranh với nhau vừa hòa lẫn vào nhau là điều ấn tượng nhất, dù rằng vẫn còn thiếu 1 chút để đạt được cảm giác tròn đầy. Với tôi, rất khó để hiểu được hết ý tứ của câu chuyện, nhưng lại luôn cảm nhận được cái không khí đặc quánh, vừa đau đớn vứa say mê. Có lẽ đây không phải 1 phát súng rền vang trong bối cảnh nền văn học nói chung, nhưng vẫn là 1 cuốn sách đáng mua, đáng đọc, và đáng suy nghĩ !
4
48472
2012-07-24 09:23:01
--------------------------
33552
10141
Với mình thì 286 trang này chưa thể lột tả được cái cốt lõi mà tác giả nhắm tới. Nó vẫn làm mình thấy thiếu thiếu gì đó. Không khí bao quanh cuốn truyện là một âm hưởng trầm lắng pha chút dễ sợ, chút ấm áp nồng nàn. Nhưng như vậy vẫn chưa đủ chưa đủ để làm người đọc chìm đắm trong câu chuyện, trong các nhân vật, dành thời gian suy nghĩ về nó và ghi lại dấu ấn sâu sắc trong tâm hồn mỗi người. Tuy nhiên trong đó vẫn có những yếu tố lạ phá cách khá cuốn hút. Nếu tác giả dành nhiều tâm huyết hơn mình nghĩ nó có thể định hơn nữa. Dù gì đây cũng là một tác phẩm dị dòng hay. Có thể bởi mình đã đọc nhiều tác phẩm cùng loại nên yêu cầu hơi gắt gao một chút. Nếu muốn thử cảm giác lơ lửng, nửa đắm nửa mê, thoát khỏi hiện thực nhàm chán , khó khăn thì mình nghĩ cuốn sach này cũng không phải là một lựa chọn tồi.
3
9270
2012-07-14 15:15:32
--------------------------
28761
10141
Cô đơn, kỳ lạ và ám ảnh là những dư vị đọng lại trong tôi khi đọc xong cuốn sách này. Tôi như bắt gặp mình và bạn bè mình trong những nỗi cô đơn nao lòng như thế. Nhiều người thắc mắc tại sao cuộc sống đủ đầy mà con người vẫn cô đơn. Cô đơn và ám ảnh, nhân vật tôi, Tee, Taurus... đã tạo nên một câu chuyện kỳ lạ, hấp dẫn đến từng câu chữ. Đặc biệt, điều làm tôi ấn tượng nhất có lẽ là cách viết của An Hạ, nhẹ nhàng, nhưng đi vào lòng người và cực kỳ lôi cuốn. Một cách viết, một cuốn sách đáng đọc trong những nỗi cô đơn kỳ lạ của chính mình.
5
41383
2012-05-31 10:04:17
--------------------------
26885
10141
Đó là cảm nhận của mình sau khi đọc xong cuốn sách này. Có cảm giác như một phần bên trong mình bị rơi mất và trôi tuột xuống một cái hố sâu thẳm nào đó... Có lẽ xã hội càng phát triển, con người càng cô đơn... Thích nhất những câu văn viết về cảm giác của nhân vật "tôi" với Taurus và Madame J, rất đẹp, rất thật, rất đau... và không lối thoát. 
Một cuốn sách đáng đọc!

5
36066
2012-05-14 11:05:31
--------------------------
