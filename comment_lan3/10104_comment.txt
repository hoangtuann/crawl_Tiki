446007
10104
Tôi quyết định mua cuốn sách này vì nghĩ nó sẽ là tài liệu quý báu trong việc học của tôi và đúng là nó không làm tôi thất vọng. Ông cha ta ngày trước rất đáng cho chúng ta học tập. Cuốn sách này đã gửi gắm rất nhiều tình cảm lòng ngưỡng mộ cũng như sự tự hào của dân tộc Việt Nam qua bao đời nay. Ngoài ra sách được trình bày khá đơn giản chủ yếu chắc lọc những ý chính. Quả thật là một tài liệu quý báu. Rất đáng để mua và đọc thử.
4
1242353
2016-06-11 12:46:32
--------------------------
417426
10104
Cảm nhận đầu tiên của mình khi mua quyển sách này là thiết kế bắt mắc nhìn rất ấn tượng khi đập vào mắc mình là những hình ảnh của Nguyễn Du, Nguyễn Trãi và Lý Thái Tổ cùng tựa đề danh nhân Việt Nam. Những con người làm rạng danh cho cả dân tộc với các cường quốc năm châu trên thế giới. Trong sách là hai mươi bảy con người đã vì nước quên thân giành lại độc lập cho đất nước. Nhờ có quyền sách này mình hiểu thêm về cuộc đời của các bậc hiền nhân.
4
509425
2016-04-16 23:08:26
--------------------------
303283
10104
Sách có bìa đẹp, chữ to dễ đọc, trình bày theo chuỗi thời gian lịch sử, không những giúp bạn biết nhân vật mà còn biết nhân vật ở thời nhà gì, năm nào. Nội dung được tối giản giúp người đọc có thể dễ ghi nhớ những sự kiện lịch sử có liên quan đến nhân vật. 
Chỉ có mỗi 1 điều là sách được đóng quá khít, có 1 số chữ bị mất khi lật trang, làm việc đọc không được thoải mái, không thể nằm đọc được. Nhưng dù sao đây cũng là một cuốn sách đáng để đọc, để học, 
3
751673
2015-09-15 19:37:00
--------------------------
234196
10104
Mình mua cuốn này cho Ba vì thấy Ba rất thích tìm hiểu về các Danh Nhân, tuy nhiên khi nhận về thì không có gì ấn tượng, cuốn sách khổ nhỏ, toàn là chữ, ko có hình minh họa. Trình bày tối giản, quá đơn sơ, khi mở các trang sách thì rất khó vì lối đóng sách xưa củ, nếu muốn nằm đọc thì cuốn này ko làm được, vì giữa trang các trang sách rất khít, nhất thiết phải ngồi ngay ngắn và phải đè + giữ từng trang sách thật chặt mới đọc hết chữ bên trong. Thật thất vọng quá.
1
345191
2015-07-20 10:15:46
--------------------------
