411361
10610
Fan cuồng của dòng tiểu thuyết khoa học - viễn tưởng. Mình thật sự may mắn khi tình cờ biết được tới tác phẩm Vampirate này. Đây thật sự là quyển tiểu thuyết về lĩnh vực viễn tưởng hay nhất mọi thời đại mà mình từng đọc, từng tình tiết truyện khéo léo, cách xây dựng cốt truyện và diễn đạt bằng nhiều chất liệu ngôn từ khác nhau đã giúp cho bộ tiểu thuyết có chỗ đứng trong lòng bạn đọc. 
Nếu bạn nào là fan dòng tiểu thuyết khoa học viễn tưởng giống  mình thì hãy tìm đọc bộ truyện này nhé, rất hay!
5
513335
2016-04-05 21:45:20
--------------------------
278673
10610
Hải tặc ma cà rồng là một cuốn sách thuộc thể loại viễn tưởng và phiêu lưu. Sách là một sự kết hợp hoàn hảo và đầy thú vị giữa những kẻ phiêu bạc ngoài khơi với một chủng loài khét tiếng man rợ.Giữa hai thái cực đó có một sự kết nối sâu sắc,  liên quan mật thiết với nhau và được dần dần hé mở qua từng trang truyện. Điều mình thích nhất ở bộ truyện này là sự hành văn của tác giả: kiểu hành văn đơn giản, đọc là hiểu, không ẩn chứa ngụ ý sâu xa. Vì thế dù tập 3 có dày hơn 2 tập trước nhưng mình chỉ đọc một tối là hết truyện. Nhưng cái cảm giác hồi hộp, sống động như chính bản thân được trải nghiệm vẫn không bị mất đi như khi đọc các cuốn sách phiêu lưu khác! Rất đáng đọc 
5
569990
2015-08-26 01:32:28
--------------------------
257783
10610
Cornor và Grace Tempest khiến mình như đắm chìm vào câu chuyện của họ với các tình tiết có thể nói là ngoạn mục và cực kì hấp dẫn. Câu chuyện Hải Tặc Ma Ca Rồng này cũng không thiếu tiếng cười và đặc biệt là tình cảm, tình cảm chị em. Một câu chuyện đầy tính nhân văn. Đúng thật như mình được giới thiệu đó là nó kết hợp giữa hiện thực và tưởng tượng. Nghe có vẻ hư cấu nhưng đọc vào mấy thấy nó mang lại thật nhiều giá trị tinh thần cho người đọc. Không liên quan nhưng mình thấy nhân vật Cheng Li được mô tả rất rõ, mình đọc xong mà cứ ám ảnh Cheng Li luôn @@
5
331912
2015-08-07 23:05:07
--------------------------
209337
10610
Đây là một trong những serie thể ma cà rồng khiến mình thích thú nhất. Một cuốn sách chứa đựng nhiều điều thú vị. Mình luôn phải ồ lên vì sự sáng tạo cũng như cách hành văn trôi chảy cuốn hút vào hai nhân vật chính. Càng ngày càng nhiều chi tiết bất ngờ được hé lộ
Tuy nhiên, công bằng mà nói, mình cảm thấy phần đầu của cuốn sách không được hấp dẫn bằng phần sau của cuốn sách. Tác giả nên làm cho đều hơn. Và không biết phải do mình không, nhưng cách dịch lần này không hay lắm. Có đôi chỗ đọc vẫn rất hài hước.
Một cuốn tiểu thuyết trinh thám phiêu lưu mạo hiểm đáng để đọc. Hy vọng một ngày gần nhất sẽ được xem phim hải tặc ma cà rồng.
5
386757
2015-06-17 13:13:24
--------------------------
129985
10610
Bộ truyện Hải Tặc Ma Cà Rồng này theo mình thấy là một bộ truyện khá thành công và có nhiều sức hút đối với đọc giả.
Truyện có nhiều hình ảnh và tình tiết. Từ những câu chuyện đơn giản và hài hước về chị em Grace và Cornor cho đến những trận đối đầu đầy kịch tính. Tất cả vẽ nên một khung tranh sống động và gay cấn.
Điểm mình thích nhất ở bộ truyện này là tình chị em của Grace và Conor - tuy 2 chị em có tính cách trái ngược nhau nhưng lại yêu thương nhau đến kì lạ.
Ngoài tình chị em, bộ truyện còn đề cập đến tình bạn bè - với những triết lý và hình ảnh giàu màu sắc và đầy nhân văn!
4
303773
2014-10-14 08:35:10
--------------------------
55560
10610
Hải tặc ma cà rồng là bộ truyện mình tăm đắc nhất trong kệ sách ở nhà, một phần là vì mình rất thích những truyện thần bí về ma cà rồng nhưng cái chính là mình rất thích sự sáng tạo hải tặc và ma cà rồng để tạo nên HẢI TẶC MA CÀ RỒNG.
Ngay từ tập 1, mình đã rất ấn tượng bởi lối viết cũng như nội dung câu chuyện. Hình ảnh hai chị em Grace và Connor làm mình liên tưởng đến bộ Nicholas Flamel- Sophie và Josh- chị em với tính cách khác nhau.
Theo mình nghĩ, HẢI TẶC MA CÀ RỒNG là một câu chuyện phiêu lưu kì thú với những tiếng cười nhưng cũng như nước mắt,, về tình chị em, bạn bè. Một tác phẩm mang đấy tính nhân văn, kết hợp giữa tưởng tượng và hiện thực.
Rất mong chờ tập 4 cũa bộ truyện thú vị, mình rất háo hức để theo chân Gace và Connor tiếp tục cuộc hành trình của họ.

5
16069
2013-01-14 21:51:45
--------------------------
31820
10610
Mình đã đọc xong cả 3 tập của bộ truyện Hải tặc ma cà rồng và những cảm xúc đọng lại trong mình thật khó phai. Theo quan điểm của mình thì bộ truyện  do Justin Somper tạo ra đã mang lại sức hút khó tránh khỏi cho người đọc.Thế giới trong truyện là cuộc phiêu lưu của các cướp biển, khó khăn luôn ở trước mắt và phải biết vượt qua.Và truyện cũng làm cho ta hiểu thêm rằng ma cà rồng không ác như mọi người tưởng.Hihi.
Từng tình huống trong truyện tạo cho người đọc cảm giác lôi cuốn khó dứt tay ra khỏi cuốn sách được. Xen lẫn vào trong đó là những bất ngờ, những cá tính riêng của Connor và Grace Tempest và của các thủy thủ đoàn nữa. Phần 3 này có kết thúc mở, và mình rất mong chờ phần 4 được phát hành sớm
Và nếu ai thích được đọc những cuốn sách về thế giới cướp biển thì đây là một quyển sách không thể không đọc.
5
32388
2012-06-29 16:16:53
--------------------------
21857
10610
Nếu bạn là một người đam mê phiêu lưu, mạo hiểm hay những điều kì bì hoặc thậm chí là một người thích những tiểu thuyết lãng mạn thì chắc chắn đây là một quyển sách mà bạn không thể bỏ qua. Từ tập một đến hết tập 3, câu chuyện về 2 chị em Grace và Connor đã đưa người đọc chạm đến mọi cung bậc cảm xúc. Từ những tình tiết như khi Grace và Connor lạc nhau sau trận bão, khi Lorcan quan tâm Grace hay khi Connor quyết tâm tìm đến với Cheng Li, tác giả thật sự là một cây bút tài ba mới có thể viết nên được một câu chuyện hấp dẫn như thế. Tất cả nhân vật từ Grace, Connor, Lorcan, thuyền trưởng tàu hải tặc ma cà rồng, Cheng Li đến những nhân vật phản diện như Sidorio, Jez... đều được miêu tả một cách toàn diện. 
Tập 3 thậm chí còn hay hơn cả tập 1 và 2 khi tất cả những chi tiết về tình cảm của Grace và Lorcan, quyết định của Connor, hành động của Sidorio khi hắn lôi kéo ma cà rồng khác theo con đường    "tà đạo" hay hình ảnh của Thuyền trưởng tàu hải tặc ma cà rồng đang từ từ được hé mở. 
Chỉ tiếc một chút là bộ truyện này kéo dài lâu quá. Ở bên mình mới chỉ có tập 3 trong khi ở nước ngoài đã có đến tập 6 rồi. Hi vọng NXB cố gắng rút ngắn thời gian để độc giả có thể thưởng thức bộ truyện vô cùng hấp dẫn này. 
5
25350
2012-03-17 21:26:54
--------------------------
16200
10610
Mình là 1 fan của truyện giả tưởng như thế này, và mình cũng rất thích cách dịch truyện của các Đặng Phi Bằng, cho nên mình không thể bỏ qua bộ truyện Hải Tặc Ma Cà Rồng. Và cuốn thứ 3 này đã không làm mình thất vọng.

Một điều rất rõ ràng là cuốn 3 này chứa nhiều hành động và cảm xúc hơn 2 cuốn đầu. Có những sự kiện dữ dội hơn rất nhiều, gây ra cho người đọc sự chờ đợi và cảm xúc sâu.

Nếu như ở 2 cuốn trước, người đọc thường đọc nhanh phần Cornor để qua phần của Grace (vì phần Grace thú vị hơn hẳn), thì ở cuốn này, vai trò Cornor đã được tăng đáng kể (với nhiều sự kiện hơn, tất nhiên). "Câu chuyện" giữa Grace và Lorcan cũng đã được hé mở một tí trên vài phương diện (mình RẤT THÍCH cặp này trong cuốn 3!!)

Tuy nhiên, công bằng mà nói, mình cảm thấy phần đầu của cuốn sách không được hấp dẫn bằng phần sau của cuốn sách. Tác giả nên làm cho đều hơn. Và không biết phải do mình không, nhưng cách dịch lần này không hay lắm. Có đôi chỗ đọc vẫn rất hài hước,  nhưng nhìn toàn thể, cách dịch không như mình mong đợi.

Đọc xong cuốn 3, có lẽ bạn sẽ hơi bị hẫng, không phải vì truyện không hay mà là bạn muốn biết thêm. Cá nhân mình bị hẫng vì chưa biết thêm chuyện giữa Lorcan và Grace :(

Nói tóm lại, mình thích cuốn sách này. Rất mong là sẽ sớm ra cuốn 4 :)
5
5610
2011-12-17 22:41:33
--------------------------
13112
10610
       HẢI TẶC MA CÀ RỒNG - Thuyền trưởng máu là tập 3 trong bộ truyện cùng tên. Đây là bộ truyện về ma cà rồng đầu tiên mà tôi mua, nó đã không làm tôi thất vọng. Đọc mỗi trang truyện, tôi như bị cuốn vào chính hành trình nguy hiểm, mà không kém phần thú vị này. Hai chị em với 2 tính cách khác nhau đã làm cho câu chuyện càng thêm sống động. Mổi người với một suy nghĩ khác nhau đã xây dựng một HẢI TẶC MA CÀ RỒNG lôi cuốn đến ngạt thở. 
      Với mỗi trang truyện là một trãi nghiệm, một chuyến phiêu lưu thú vị với đầy nguy hiểm để từ đó ta thấy được ý chí và quyết tâm của con người, " con người có thể bị hủy diệt chứ không thể bị đánh bại".
      HẢI TẶC MA CÀ RỒNG là series về ma cà rồng rất đáng để dựng thành phim như những người anh em của nó ( The Twilight Saga, Darren Shan,..), đều là những tác phẩm về ma cà rồng đã quá nổi tiếng. Tôi rất hi vọng một ngày gần đây không chỉ được thưởng thức truyện mà còn được xem phim HẢI TẶC MA CÀ RỒNG. 
5
6328
2011-10-19 13:59:47
--------------------------
