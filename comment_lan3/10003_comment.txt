285863
10003
Bé nhà mình 2 tuổi và cực kì thích thú mỗi khi chuẩn bị lên giường đi ngủ được mẹ cầm quyển sách này đọc truyện cho nghe với giọng nói ghét không thể tả được: "Mẹ cầm sách đọc truyện cho em nhỏ nghe nữa, mẹ hi".
Mình mua gần trọn 2 phần của bộ sách này rồi. Đây thực sự là bộ sách tuyệt vời để rèn luyện chỉ số EQ cho bé. Đồng thời nhờ bộ sách này mà bé nhà mình đã biết yêu việc đọc sách. Thình thoảng bé lật từng trang sách và kể lại một số câu chuyện làm cả nhà nhìn nhau cười vui trong lòng.
Các mẹ hãy mua bộ sách này về cho con yêu của mình nhé!
5
496573
2015-09-01 11:08:43
--------------------------
