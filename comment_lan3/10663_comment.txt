480578
10663
Đọc xong cuốn tiểu thuyết này, đọng lại trong tôi một dư vị rất lạ và thích thú, không giống như bất cứ tiểu thuyết nào trước đây. Rất nhẹ nhàng tính cách của các nhân vật được miêu tả chi tiết, khiến tôi thỉnh thoáng như được thấy mình trong đó qua các tình huống, nếu như mình rơi vào tình huống như vậy mình sẽ sử lý như thế nào, liệu có giống như nhân vật đó không. Tình cảm gia đình thiêng liêng không thể thiếu trong mỗi con người, trong mọi tình huống đều có cách để giải quyết, hãy suy nghĩ thật sáng suốt!
4
357262
2016-08-18 22:00:05
--------------------------
405313
10663
Ấn tượng đầu tiên của mình là tên sách, nghe khá lôi cuốn "Xin Hãy Quên Em".
Vì thế thấy giảm giá 60% còn 36k, mình đã quyết định chọn mua đọc thử ngay. 
Nói chung không hối hận, đây là cuốn tiểu thuyết kỳ diệu, lối viết khá lạ đối với bản thân mình, cách viết vừa lãng mạn vừa gây cấn, hấp dẫn, cứ đọc trang này là lật tiếp trang kế, không thể dừng, không thể bỏ lỡ.
Mình nghĩ đây là cuốn sách đáng mua, mình rất thích. 
Đó giờ đọc ngôn tình TQ khá nhiều, giờ đọc sang tiểu thuyết của Isabel Wolff, thấy rất hay, chắc chuyển sang đọc thể loại này luôn quá. 
4
1167338
2016-03-26 15:33:29
--------------------------
380829
10663
Đây là cuốn tiểu thuyết huyền diệu , đầy màu sắc màu nhiệm vượt qua cả thời gian tồn tại lâu dài , đem đến trách nhiệm cao lớn đối với người mềm yếu , xin hãy quên em còn làm cho người xem thấy con người dễ thay đổi , mỏng manh và nhìn nhận các mặt trái còn có lý do đưa đẩy nghịch lý ấy thành bài học cho mình trước nguy cơ phải xóa sổ khỏi danh sách phiền muộn , kéo theo xuống ngục tối cảnh giác như các tác giả trước đây , chiều sâu vốn có .
3
402468
2016-02-15 22:24:31
--------------------------
296534
10663
từ đầu đến cuối câu chuyện được kết nối với nhau bằng những tình huống hiện thực đời sống thông thường. văn phong có vẻ nhẹ nhàng nhưng vẫn đọng lại những điểm nhấn và nét riêng của tác giả. văn phong nhẹ nhàng không có nghĩa là tình huống truyện chầm chậm trôi, lúc nhanh lúc chậm và có lúc cao trào ( theo mình là lúc mà cô phát hiện ra bí mật gia đình mình ). Isabel Wolff đã thành công trong việc chọn lối chick lit - dòng văn viết về những cô gái độc thân ngoài 20, dễ dàng tiếp cận với bạn đọc. cung bậc cảm xúc của Anna cũng giống như những cô gái đôi mươi khác, có vui có buồn, có lo âu và phiền muộn của những cô gái trẻ độc thân. 
không quá lạ khi tác phẩm trở thành international best seller
4
93717
2015-09-11 09:43:47
--------------------------
160240
10663
Xét về lối viết trong Forget me not thì Isabel Wolff không thực sự nổi trội; bằng chứng là những khoảng không rất sa đà. Nhưng nếu như quyển sách trở thành "The International Bestseller" thì bạn hãy tự mường tượng hết mức những gì nội dung của nó có thể mang lại. Thực tế thì những mâu thuẫn gia đình nhẹ nhàng, những tình yêu nhẹ nhàng, những quyết định "nhẹ nhàng" như là sinh một đứa con và lựa chọn công việc của mình - thứ mà với một người phụ nữ là cả cuộc đời trưởng thành của cô ta; lại trở nên hấp dẫn tuyệt đối. Hãy tin rằng bạn sẽ muốn trở thành mẹ của cô bé Milly - chủ thể của tình yêu mẫu mực nhất của một "single mom" như Anna. Và rồi thì một lựa chọn nhẹ nhàng, như Jamie chẳng hạn - sẽ là tất cả những gì tốt đẹp nhất để có thể đem lại hạnh phúc
Điều tuyệt vời mà hai nhà thiết kế vườn có thể đem lại cho cuộc sống của nhau và của một đứa trẻ có phải là sự hợp lý và phát triển, đúng không?
3
541549
2015-02-23 11:07:37
--------------------------
61710
10663
Mình không thích tác giả Isabel Wolff bằng Susan Elizabeth Philips hay Emily Giffin, nhưng vẫn phải công nhận cô là một nữ tác giả tài năng trong dòng tiểu thuyết lãng mạn. "Xin hãy quên em" cũng là một trong những tiểu thuyết thành công của cô. Thể loại chicklit thực sự đã phát huy tối đa tác dụng của nó, trong việc tạo dựng nên những câu chuyện có diễn tiến nhanh, những tình huống hấp dẫn và bất ngờ. Tác giả vừa thành công trong việc kể chuyện, vừa tài tình trong cách đưa đẩy những mối quan hệ, khắc họa tâm lý nhân vật. Chừng đó thôi đã đủ để cuốn này trở thành một tác phẩm mà ai cũng nên tìm đọc.
4
20073
2013-03-03 16:17:25
--------------------------
