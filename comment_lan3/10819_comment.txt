8043
10819
Nhờ những vũ khí nổi tiếng, đó là sự may mắn, trí thông minh và lòng can đảm, Lucky Luke đã dẫn đường cho đoàn lữ hành từ phía đông nước Mỹ tới California một cách an toàn. Mặc dù trong chuyến đi cũng gặp nhiều gian nan như đoàn xe phải đi vào sa mạc hoang vu, ghê rợn, chỉ toàn đá và cát, cùng cái nắng gay gắt đè nặng lên người và thú. Sau đó là thiếu nước uống rồi phải băng qua sông và phải tìm cách vượt qua người da đỏ Sioux...nhưng đoàn xe vẫn tiếp tục lăn bánh, họ không nản chí và trong suốt chuyến đi Lucky Luke luôn nhắc họ phải dũng cảm, tiến về phía trước. Tôi đã học được lòng can đảm, không nản chí trước khó khăn thử thách nhờ vào Luky Luke. Một câu chuyện đáng để đọc và suy ngẫm.
5
5514
2011-07-12 00:59:31
--------------------------
5337
10819
Từ lúc còn bé tí xíu tôi đã từng được biết đến một chàng cao bồi tốt bụng nơi miền Tây hoang dã của nước Mỹ, một con người thật sự biết quan tâm đến người khác, luôn đồng hành và reo ruổi trên khắp các nẻo đường, cuốn theo không biết bao nhiêu là cuộc hành trình đầy vui nhộn và những câu chuyện dở khóc dở cười của các băng nhóm tội phạm khét tiếng và ghê gớm nhất. Nhìn lại trang truyện nay đã được tái bản với hình thức vô cùng hấp dẫn, tôi thiết nghĩ đây là cơ hội để những bậc làm ba làm mẹ nên dành thời gian để mua về và trang bị cho tủ sách gia đình thật đầy đủ hơn. Tính giáo dục luôn nằm sau những cuộc phiêu lưu kì thú của chàng cao bồi Lucky Luke sẽ giúp các em nhỏ học được các bài học bổ ích cho cuộc sống của mình. Đừng chần chừ nữa mà hãy có ngay một quyết định cho chính mình nhé !
5
5647
2011-05-30 21:51:17
--------------------------
3826
10819
Mặc dù đặt trong bối cảnh miền Tây hoang dã của nước Mỹ trong những ngày đầu khai phá, Lucky Luke không hề mang yếu tố bạo lực.Ngược lại, đó là 1 bộ truyện tranh hài hước,hấp dẫn, dành cho mọi lứa tuổi. Những cuộc phiêu lưu của Lucky Luke phản ánh công cuộc khai phá miền Viễn Tây của những con người tiên phong dũng cảm.Và chuyến phiêu lưu này là 1 ví dụ điển hình. Lucky Luke sắm vai người dẫn đường cho 1 đoàn lữ hành đến miền Tây - vùng đất hứa. Cuộc hành trình đầy khó khăn, gian khổ với những mối đe doạ từ người da đỏ,những hiểm nguy từ những sa mạc khô nóng,đầy cát và gió,rồi những âm mưu phản bội trong đoàn...Tất cả như ngăn không cho đoàn lữ hành tiến lên phía trước... Những khó khăn đó đã được hai tác giả tài hoa Morris và Goscinny  khéo léo đưa vào nét hài hước không lẫn vào đâu được. Bạn sẽ không thể quên anh đánh xe Ugly Barrow mỗi lần phát ngôn toàn "sấm sét,đầu lâu xương sọ",nhà phát minh kỳ quái Martins chuyên tạo ra những cỗ máy "hủy diệt",ông nhà đòn hát tình ca rất "ngọt"v..v.. Tóm lại, đây là một tập truyện tranh không thể thiếu trong nhà bạn ^^
5
4058
2011-05-05 08:38:45
--------------------------
