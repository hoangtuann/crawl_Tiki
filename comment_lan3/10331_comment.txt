417785
10331
Khi nhận được sách mình rất thích, bìa sách đẹp trang nhã, sách vừa không dày. Khi đọc cuốn sách này bạn sẽ tìm được nhiều câu trả lời ý nghĩa cho hạnh phúc của cuộc đời mình. Đọc các câu chuyện trong đó bạn sẽ rút ra được nhiều điều thú vị từ cuộc sống trong đó có nhiều câu mình thích, ví như “Có những người giống như loài mèo, chỉ muốn được vuốt ve và gãi nhẹ, bởi vậy người đó cảm nhận tình yêu bằng cách chạm vào nhau, có thể là một cái vỗ nhẹ vào lưng, xoa đầu, một vòng tay ôm hay một nụ hôn.

Có những người lại giống như loài chim hoàng yến, sẽ hạnh phúc nhất khi bạn ngồi đó và thưởng thức tiếng thánh thót của nó. Vì vậy, người đó cảm nhận được tình yêu thương bằng cách dành thời gian bên nhau. Việc âu yếm, làm nhiều điều hay cho họ liên tục những lời yêu thương chẳng là gì cả, chỉ có thời gian dành bên nhau mới thực sự có ý nghĩa với họ".
5
619956
2016-04-17 20:34:44
--------------------------
62633
10331
Bạn có biết ngọn hải đăng có ý nghĩa như thế nào? Một ngọn đèn biển soi đường cho những con tàu ngoài biển đêm mênh mông nguy hiểm. Hồi tôi còn nhỏ, tôi vẫn hay nghe về một ngọn đèn sáng rực rớ giữa biển đêm. Đối với tôi lúc đó nó đẹp lắm. Và giờ đây thì tôi đã có ngọn hải đăng của mình. Bạn đã có ngọn hải đăng của mình chưa? Ngay khi tôi trông thấy một quyển sách với bìa là hình ảnh của ngọn hải đăng bên biển, tôi đã biết nó viết về điều gì và tôi biết tôi cần đọc nó. Từ một cuộc đời như một đại dương vô tận không có ánh sáng mặt trời, sau đêm gặp ngọn hải đăng của mình, Andy thấy ánh sáng giữa đại dương đó. "Chúng ta có thể mất đi ngôi nhà, nhưng đừng bao giờ để mất đi mái ấm!". Tôi biết! Tôi biết điều đó. Cuốn sách với ngọn hải đăng này đã cho tôi điều đó. Hãy để ngọn hải đăng sáng lên rực rỡ và soi tỏ đường cho con thuyền đời của tôi không chìm vào biển đêm vô tận kia." Tôi tự nhủ với chính mình. Với 207 trang sách, giản dị mà đầy triết lí cuộc đời, ngọn hải đăng của tôi đã sáng hơn. Và tôi sẽ nói với những người thân tôi là tôi yêu họ nhiều lắm. Ngọn hải đăng hãy giúp tôi có đủ nghị lực để làm điều đó nhé!
5
84996
2013-03-10 15:15:13
--------------------------
28504
10331
Ngay từ tên sách đã gợi lên cho người đọc rất nhiều suy nghĩ. Khi tôi nhìn thấy tên sách, bất giác hình ảnh ánh đèn lấp loáng trong ngày giông bão. Không mất nhiều thời gian, tôi nhanh chóng chọn mua cho mình cuốn sách này.
Đúng như cái tên, cuốn sách là một loạt những kỉ niệm, những khoảnh khắc đáng nhớ, những kỉ niệm khắc sâu trong trái tim con người. Ông Jones- nhân vậy xuyên suốt cuốn sách, là một người vừa bí ẩn, vừa kì lạ nhưng trên hết, ông luôn mang đến cho mọi người những bài học đáng nhớ, những phút giây thoải mái được trải lòng mình.
Chỉ có cát, biển và bầu trời nhưng mỗi con người lại có những cách nhìn nhận khác nhau. Lạc quan hay bi quan, tươi sáng hay buồn thảm, chính mỗi cách nhìn đem lại những kết quả khác nhau. Hẳn người viết đã biết ơn Jones lắm, vì nhờ có những cuốn sách, nhờ có cách nhìn bầu trời, cát và biển cảm nhận được từ Jones, một cuộc sống mới đã đến.
Bóng dáng Jones thoắt ẩn thoắt hiện, thoáng thấy rồi lại thoáng v ụt mất chỉ trong giây lát. Con người kì lạ ấy luôn biết xuất hiện đúng lúc, biến mất khi những trải nghiệm và lời nguyên đã đến được người cần. Có những người chỉ gặp được Jones một lần trong đời, cũng có người may mắn được gặp ông và chia sẻ cùng ông nhiều lần. Dù thế, gặp được Jones, có lẽ là kỉ niệm cũng như trải nghiệm không thể quên của mỗi con người.
Jones biến mất, để lại chỉ là chiếc cặp và những hạt giống.  Nhưng hình bóng của con người đặc biệt đó sẽ mãi sống, mãi đồng hành cùng tất cả. Mỗi hạt giống được gieo là một niềm tin được nhân lên, mỗi lần cây vươn mình là một lần ánh sáng hy vọng lại lan tỏa. Cuộc đời này có lẽ có rất nhiều Jones như trong câu chuyện, ẩn hiện xung quanh ta, cho ta những lời khuyên để ta vững bước trên đường đời ...
5
40294
2012-05-28 14:01:24
--------------------------
