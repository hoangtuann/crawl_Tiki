481842
10874
Mua lâu rồi nhưng giờ mới review, cuốn này lúc giao tới mình hồi hộp lắm vì ko biết màu có đẹp ko.Lúc mở ra thì mừng lắm , bìa được vì màu nổi nhưng nét hơi mờ, cốt truyện ở trong khá hay,nét đẹp,trang minh họa rất đẹp và sống động, có thể nói là lung linh đến tim đập chân rung (ko biết có phải vì mình hiếm đặt truyện màu ko nữa) .Có thể nói,nhân vật nữ được xây dựng hình ảnh hoàn toàn phù hợp: xinh đẹp,xuất hiện tựa tiên nữ giáng trần,tài hoa lẫn võ công đều vượt trội.Nam chính cũng không thua kém.Phần kết mở gợi theo hướng tốt và tương lai tươi đẹp của hai nhân vật.
4
657270
2016-08-31 00:47:32
--------------------------
476435
10874
Ở Đại Mạc Hoang Nhan tập trung khai thác chủ yếu vào các biến cố cuộc đời, có thể là của công tử Thư Dạ, có thể là Tinh thánh nữ Sa Mạn Hoa, cũng có thể là Đỉnh Kiếm Hầu Mặc Hương, hay từ công tử Liên Thành đến nàng hầu Lục Cơ.
Cái hay của truyện là nằm ở mạch chuyện, những mảnh đời nhỏ nhoi lại có những đường dây kết nối,nối lên một áng văn "sống". Truyện được kể tĩnh nhưng trong truyện lại có động. Thế nhưng cái tĩnh đã bao trùm lên cái động, cái động thì vẫn xuyên suốt, làm rạng lên vẻ đẹp của truyện
5
584588
2016-07-20 13:56:11
--------------------------
476085
10874
Thật ra thì mình không có ấn tượng mấy với tác phẩm Đại Mạc Hoang Nhan này,đọc xong không đọng lại ấn tượng là mấy,nội dung cũng không được sâu sắc,nhưng cách kể chuyện và lời lẽ trong truyện thì tạm ổn! 
Điểm cộng cho việc vẽ tranh minh họa khá đẹp.
Mạch truyện cũng tạm ổn.
Nói chung đối với người thích đọc truyện dài hơi như mình thì truyệ này chưa đáp ứng được sự tò mò,thích thú khám phá của mình,và chưa cuốn hút mình theo từng trang sách như những tác phẩm khác. Có lẽ mình khá khó tính :D
4
1226086
2016-07-18 15:14:56
--------------------------
452742
10874
Truyện Thương Nguyệt luôn gắn với những cái buồn.
Đại Mạc Hoang Nhan là cuốn đầu tiên của Đỉnh Kiếm Các hệ liệt. Truyện về tình huynh đệ và tình yêu của Công tử Thư Dạ trấn giữ Đôn Hoàng thành. Tình huynh đệ thể hiện khá rõ nhưng tình yêu còn khá mờ nhạt. Những hiểu lầm trong quá khứ cùng những toan  tính đã chia cắt tình yêu đó.
Thương Nguyệt đã để kết thúc mở, lôi cuốn độc giả và tiếp nối câu chuyện tình yêu bằng cuốn thứ hai của hệ liệt : Đế Đô Phủ, xoay quanh toan tính quyền lực còn ẩn chứa tình yêu của Thư Dạ và Sa Mạn Hoa.
5
1140423
2016-06-20 20:07:36
--------------------------
415663
10874
Nói chung là câu chuyện tóm gọn về tình huynh đệ giữa thư dạ và huỳnh hằng . Tình yêu giữa sa mạn hoa và thư dạ . Cốt truyện hay,cảm xúc sâu sắc và còn có thêm hình ảnh cho dễ hình dung nữa . Nhưng theo mình thấy với giá 29.000đ thì một tác phẩm như thế này thì đã quá tuyệt, nếu bạn không thích đọc truyện thì có thể xem hình ảnh cũng rất đẹp đó chứ . Còn về phần cốt truyện thì mình ấn tượng về chuyện thư dạ say nhưng mà tỉnh . Dù kiếm đến thuốc phiện và rượu thì suy cho cùng cũng là về muốn gợi lại cảm giác bên sa mạn hoa khi đã từng . Nhưng kết thúc quá mơ hồ chẳng đâu ra đâu cả nên mình đánh giá truyện này hay về cốt truyện nhưng diễn tả quá sơ sài nên mình đánh giá 4/5 vì có phần hình ảnh đẹp!
4
1095528
2016-04-13 20:54:25
--------------------------
405411
10874
Truyện thiên về chất kiếm hiệp - phiêu lưu, thể hiện tình huynh đệ rất chân thành,gắn kết giữa Thư Dạ và Mặc Hương, thật là có chút đam mĩ trá hình hehe =))))))))))))))
Nội dung ngắn gọn nhưng cô đọng, đầy đủ, khung cảnh hào hùng,...
Về tình yêu của Sa Mạn Hoa và Thư Dạ thì quá mờ nhạt, nói chung nguyên cuốn truyện mà hai người chưa gặp nhau được lần nào, kết thúc quá mơ màn cho một cuộc tình
Cách xây dựng nhân vật đa dạng cảm xúc, tác giả Thương Nguyệt thành công khi viết tác phầm Đại Mạc Hoạng Nhan...
4
800462
2016-03-26 17:56:22
--------------------------
366266
10874
Tôi biết tác giả Thương Nguyệt qua Thất Dạ Tuyết và thực sự rất ấn tượng với văn phong của chị vì thế đã mua ngay tác phẩm này. Nhưng đọc tác phẩm này lại không đủ độ gay cấn, cũng không để lại ấn tượng sâu sắc nào cho người đọc. Bù lại bút pháp trong truyện vẫn đậm chất kiếm hiệp dù không cần miêu tả cảnh chém giết nhiều. Truyện khá to bản so với những quyến sách khác, có nhiều hình minh họa, rất khác biệt với những truyện khác. Tóm lại, truyện để giải trí thì ok nhưng khộng đọng lại nhiều cảm xúc.
3
919917
2016-01-09 16:22:33
--------------------------
360679
10874
Đây là lần đầu tôi đọc tác phẩm kiếm hiệp. Không thường xuyên đọc nên cũng không biết truyện kiếm hiệp phải ra sao mới gọi là hay nhưng tôi cho rằng tuy cuốn truyện này không quá dài nhưng đã truyền tải hết những thứ tình cảm thiêng liêng nhất của con người. Tình bạn qua bao năm vẫn bền vững và ngày càng thấu hiểu nhau của Thư Dạ và Mặc Hương, mỗi con người 1 lí tưởng nhưng vẫn luôn lo nghĩ cho nhau. Tình huynh đệ của Thư Dạ và Liên Thành rất đáng trân trọng khi người mẹ của Liên Thành đã gây bao nỗi đau cho Thư Dạ nhưng cuối cùng thì ra Thư Dạ luôn làm điều tốt nhất cho em mình. Tôi nghĩ chi tiết đặc sắc nhất của truyện là khi Lục Cơ, người hầu của mẹ Liên Thành, sau nhiều năm hận Thư Dạ, khi nhìn thấy những bức thư cho thấy sự yêu thương mà Thư Dạ dành cho em trai đã hối hận. Tôi cũng không ngờ, chiếc tủ tưởng rằng cất giấu điều gì quý giá lại chứa những bức thư đó. Tình yêu của Thư Dạ và Sa Mạn Hoa khá buồn và chỉ có cái kết mở nhưng người đọc có quyền hy vọng cho họ. Các cảnh đánh nhau thì không đặc sắc mấy. Hình minh họa nhìn được nhưng không có màu, hơi thất vọng vì điều đó nhưng giấy đẹp và mịn. Cuốn truyện dành cho ai muốn thử thể loại kiếm hiệp nhưng không muốn đọc quá dài như tôi
4
68290
2015-12-29 16:39:49
--------------------------
360178
10874
Trước khi bạn đọc bạn phải chuẩn bị một bộ óc tưởng tượng phong phú mới được, ngay chương đầu tác giả đã miêu tả những hình ảnh sinh động, cầu kỳ khiến bạn phải vận dụng đầu óc tưởng tượng ngay nhưng sau đó tất cả những chương sau đều có hình ảnh minh họa làm cho bạn dễ dàng mường tượng ra, phải nói đây là một câu chuyện cực kỳ cảm động vế tình anh em, tình bằng hữu kể cả tình yêu, những nhân vật trong truyện khiến cho người đọc cảm thấy thương cảm ,khâm phục họ.
5
313953
2015-12-28 20:39:58
--------------------------
347651
10874
Lần đầu tiên mình đọc truyện của Thương Nguyệt là cuốn Đại Mạc Hoang Nhan này, cũng mua tại tiki luôn. Lần đầu cầm trong tay cuốn sách mình đã ấn tượng rồi. Giấy in truyện rất đẹp, khi sờ có cảm giác thích tay. 
Truyện đọc càng về sau càng thấy hấp dẫn, mặc dù đây chỉ là một tác phẩm trong bộ Đỉnh Kiếm Các hệ liệt của Thương Nguyệt nhưng nội dung trong truyện là một câu chuyện riêng biệt. Đọc truyện nhiều đoạn mình đã rơi nước mắt vì cảm động tình cảm của các nhân vật dành cho nhau vì nhau. Tình yêu đôi lứa , tình huynh đệ thắm thiết sẵn sàng vì nhau dù hy sinh tính mạng. Thư Dạ vì Sa Mạn Hoa mà thủ thành nhiều năm, mong chờ một ngày nàng sẽ đến. Trao thân đệ Liên Thành cho vị huynh đệ vào sinh ra tử Đỉnh Kiếm Hầu dạy bảo. Cùng với bao hiểu lầm của quá khứ khiến chàng phải lao vào vòng sinh tử dù phải chiến đấu cùng nàng Sa Mạn Hoa hay đệ đệ Liên Thành.
Cuộc chiến cuối cùng trên đỉnh Kỳ Liên, Mặc Hương đã thay chàng đỡ một mũi tên của Sa Mạn Hoa, và một lần nữa Thư Dạ lại để nàng ra đi... chàng sẽ đi tìm nàng. 
Một cái kết mở hơi buồn chút khi mà tình yêu chưa tìm thấy hạnh phúc trọn vẹn.


5
70692
2015-12-04 20:26:29
--------------------------
310172
10874
Mình chỉ hay đọc ngôn tình chứ kiếm hiệp thì chỉ toàn xem phim thôi. Nhân dịp Tiki giảm giá mạnh mình quyết định mua cuốn này đọc thử. Khi nhận sách thì khá bất ngờ. Sách tuy mỏng nhưng khổ lớn, chất lượng tốt, có nhiều hình vẽ minh họa rát đẹp. 
Về nội dung, lúc đầu cứ sợ sách mỏng thế này thì nội dung bị cắt xén nhiều, nhưng khi đọc rồi thì lại không hề thất vọng. Nổi bật nhất có lẽ chính là tình huynh đệ của hai đệ nhất sát thủ Tu La Trường – Thư Dạ và Mặc Hương – một điều tưởng chừng như không thể. Và cuối cùng Công tử Thư Dạ có tìm được Sa Mạn Hoa không? Đó là một câu hỏi vẫn còn bỏ ngỏ.
4
84133
2015-09-19 12:55:43
--------------------------
309775
10874
Truyện của Thương Nguyệt bi thương và đau lòng lắm, cái kết nào cũng ám ảnh mình cả nên mình đã phải đắn đo rất lâu mới quyết định mua quyển này. “ Đại mạc hoang nhan” là quyển mở đầu cho hệ liệt “Đỉnh Kiếm Các” và đã không làm mình thất vọng. Nội dung chặt chẽ, logic, đặc biệt mình rất thích cái tình Thương Nguyệt miêu tả trong này, sự dằn xé khi phải lựa chọn giữa yêu - hận - bá nghiệp của các nhân vật trong truyện rất hay, sâu sắc. Mối tình giữa Thư Dạ và Sa Mạn Hoa cũng đẹp nhưng mình thấy hơi thiếu, chắc là do phân đoạn của họ không nhiều lắm.
4
39798
2015-09-19 09:40:36
--------------------------
307673
10874
Đại mạc hoang nhan có một cái mở đầu khá hấp dẫn và quen thuộc. Tình tiết câu chuyện xoay quanh mỗi nhân vật, khắc sâu mỗi khía cạnh của nhân vật ấy.Mỗi người một tính cách, một số phận, một chí hướng nhưng tất cả họ không tránh khỏi vòng xoáy của cuộc tranh giành quyền lực, những yêu, ghét, hận, thù trong nhân thường thế thái. Đặc biệt, với lối kể chuyện tài tình, Thương Nguyệt đã tạo nên những màn võ thuật vô cùng đẹp mắt xen lẫn những bức tranh nội tâm đẫm lệ. Đọc Đại Mạc Hoang Nhan, cái nhức nhối, day dứt cứ hòa lẫn vào từng con chữ, vào từng hình ảnh và lan tỏa sang mỗi người đọc không thôi…
3
722069
2015-09-18 11:03:18
--------------------------
297095
10874
Mình mua sách vì giá sản phẩm khá rẻ, nhưng khi nhận được sách thì mình rất hài lòng. Bìa sách rất đẹp, chất lượng dịch khá tốt, ngoài ra truyện còn có kèm theo hình minh họa. Tuy truyện không dài như các tiểu thuyết trung quốc thường thấy nhưng mình thấy cốt truyện khá hấp dẫn, đặc biệt mình rất thích cách xây dựng nhân vật của Thương Nguyệt. Có thể nói đây là một tác phẩm khá ổn, truyện xúc tích rõ ràng, không mất quá nhiều thời gian để đọc. Mong là Thương Nguyệt sẽ cho ra đời những tác phẩm như thế này trong tương lai.
4
115397
2015-09-11 16:13:55
--------------------------
293761
10874
Cá nhân mình không thích truyện kể về sa mạc (nếu là sa mạc tuyết thì được :]] ) bởi vì tâm lý ghét nóng, ghét cực khổ dưới trời nóng nhưng thấy mọi người phản ứng tích cực với Đại mạc Hoang nhan quá nên bấm bụng mua thử. Không tệ, có nét độc đáo, khá hay. Mình cũng bắt đầu thích phong cảnh rộng lớn, tự do ở sa mạc. Xuyên suốt sách có hình ảnh minh hoạ, good. :v lúc đầu mình nghĩ quyển này nhỏ lắm ai ngờ mỏng nhưng khổ cực to :)) 
Thương Nguyệt xâydựng mối tình của Sa Mạn Hoa với Thư Dạ hay, tình bạn của y với Mặc Hương cũng có nhiều điểm thú vị nên mình cũng thích thích.
4 sao. <3 
4
619695
2015-09-08 17:12:35
--------------------------
280228
10874
Thích Thương Nguyệt từ hồi đọc Thất dạ tuyết. Cách viết và câu cú của tác giả khá mượt mà, không dài dòng và hoa lá. Quyển này trước kia không đọc vì nó quá ngắn và chả hiểu một truyện ngắn như thế thì truyền tải được những gì cho bạn đọc... Nhưng đọc rồi thì đúng là phải suy nghĩ lại. Cốt truyện tuy đơn giản nhưng lại cô đọng, súc tích, cảm động. Mối tình giữa Sa Mạc Hoa và Công tử Thư Dạ có chút bi ai, đau thương nhưng cũng rất đẹp. Nói chung có yếu tố ma giáo  là mình rất thích. Mỗi tội truyện hơi nhiều hình vẽ, cũng không đẹp, đọc cứ như đọc truyện tranh, lại thêm cái kết thúc mở...
5
625177
2015-08-27 15:11:40
--------------------------
278849
10874
Mình có hơi kì vọng khi đọc được dòng "một trong năm đại diện xuất sắc nhất của thế hệ tác giả tiểu thuyết tân kiếm hiệp Trung Quốc", dù không đến mức thất vọng nhưng cũng có phần không thoả mãn. Tình tiết khá nhanh, những chi tiết quan trọng (theo mình) chỉ được điểm sơ qua, tình cảm của nhân vật có phần gấp gáp và trẻ con, thủ đoạn không có gì nổi bật, cách giải quyết cũng quá bình thường. Thật ra mình chấm 3.5 sao, định trừ xuống còn 3 thôi, nhưng chất lượng giấy tốt, câu văn của tác giả cũng ổn, thêm nữa là mình thích tình bạn giữa Mặc Hương và Thư Dạ nên quyết định cho 4 sao, chấm hơi rẻ một chút cũng được.
4
198872
2015-08-26 11:17:26
--------------------------
272976
10874
168 trang sách kèm minh họa rất đẹp (hình minh họa đen trắng mình thấy đẹp hơn so với bìa màu), rất ngắn so với đa phần tiểu thuyết Trung Quốc hiện nay nhưng đây thực sự là một bản hùng ca, được Thương Nguyệt xây dựng trên nền gió cát sa mạc của vùng đất Đôn Hoàng. Cuốn sách mở ra cuộc đời đầy thăng trầm của những Công tử Thư Dạ, Công tử Liên Thành, Tinh thánh nữ Sa Mạn Hoa, Đỉnh Kiếm Hầu Mặc Hương, nàng hầu Lục Cơ. Phong cách viết của Thương Nguyệt rất uyển chuyển, mô tả chân thực sống động những màn tranh đấu võ thuật nhưng cũng không thiếu đi chất tình, giằng xé nội tâm nhân vật. Dù vậy cái tình ở đây cũng chỉ là thoáng qua, không che lấp đi sự bi tráng của tiểu thuyết kiếm hiệp.
5
60677
2015-08-20 16:09:38
--------------------------
258701
10874
Mình mua cuốn sách này là nhờ bìa đẹp chứ không phải vì nội dung, mình cũng không phải là fan của thể loại truyện này. Tuy nhiên, sau khi đọc xong mình quyết định là sẽ kiếm thêm vài cuốn thuộc thể loại này đọc tiếp. Đọc những trang đầu tiên, mình bị nhầm lẫn, không biết ai là nhân vật chính của truyện, cái nhân vật lúc đầu mình nghĩ là "nhân vật tà ác" đó, hóa ra lại là một chàng trai vô cùng tốt, tình yêu của chàng dành cho nàng thật sâu đậm và cũng thật đau lòng, xót xa. Mình càng thêm bội phục chàng về tấm lòng rộng lượng và cao thượng đành cho em trai cùng cha khác mẹ của mình. Truyện kết thúc mở làm mình phải ngẩn ngơ một buổi nghĩ về kết thúc đẹp cho câu chuyện. Nói tóm lại là mình thích cuốn truyện này! Cảm ơn Tiki đã giảm giá 70% 
4
477940
2015-08-08 19:48:23
--------------------------
241857
10874
Cuốn này mình mua trong đợt giảm giá, thấy rẻ rẻ mà mọi người khen nhiều nên mua đọc thử xem sao, với lại mình cũng thích truyện kiếm hiệp. Khi chưa nhận sách, mình tưởng nó là truyện tranh, đến khi nhận sách thì thấy không phải, giá bìa vẫn thật là rẻ, bìa đẹp, sách khổ lớn, dễ đọc. Tuy câu chuyện không dài, nhưng cốt truyện khá hay và bất ngờ, tuy bạn đọc đến gần cuối đã có thể đoán được kết cục nhưng khi đọc bạn hoàn toàn vẫn có thể khóc. Trong truyện có tình yêu đôi lứa, tình cảm gia đình, có quốc gia, có thù hận. Mỗi thứ, mỗi thứ hiện lên trong cảnh sa mạc cát và gió... Một người anh trai, có thể vì em mình mà làm tất cả, dù cho mẹ của người kia hại mình rất nhiều... Tình cảm gia đình chính là thứ tình cảm thiêng liêng nhất, đặt trên tất cả, vượt qua mọi nỗi hận thù...
4
359546
2015-07-25 21:38:34
--------------------------
238327
10874
Mình đã vô cùng thích tác giả Thương Nguyệt từ khi đọc bộ này. Những tác phẩm của Thương Nguyệt dù ngắn dù dài gì cũng luôn có những khúc mắc khiến con người ta phải suy ngẫm. Tình anh em, tình yêu, tình người trong Thương Nguyệt có một cái gì đó...bất đắc dĩ! Chắc từ đó là hình dung chính xác nhất! Tôi thích Thương Nguyệt, cũng như Đồng Hoa, Thương Nguyệt là một trong rất ít những tác giả mà bất kỳ tác phẩm nào tôi cũng đi tìm đọc.... Đã từng khóc rất nhiều với Thất dạ tuyết, bi thương với Bỉ ngạn hoa, đau lòng với Ảo thế, và Đại mạc hoang nhan thì cho tôi chìm vào trong màu vàng của cát, của sa mạc, để với không tới được tình yêu của hai nhân vật chính....
5
133269
2015-07-23 09:12:58
--------------------------
234977
10874
Đây là cuốn truyện thuộc thể loại kiếm hiệp đầu tiên mà mình đọc. Nội dung rất hay, hình tượng của các nhân vật cũng rất tuyệt, tương ứng với tính cách của mỗi người.

Nội dung hấp dẫn, tuy không mấy mới lạ nhưng cũng đủ làm mình hồi hộp mỗi lần đọc từng trang truyện. Hình minh họa có trong truyện rất đẹp, mê tít mấy ảnh ấy luôn rồi :)))).

Chất lượng truyện cũng rất tốt, bìa minh họa lung lung luôn, không bị quăn góc với hư gáy sách.

Nếu thích thể loại này thì bạn nên mua quyển này!
5
621744
2015-07-20 18:30:49
--------------------------
228700
10874
Thương Nguyệt đã xây dựng hình ảnh các nhân vật rất sinh động, đặc biệt là nhân vật nam chính - Cao Thư Dạ - một hình tượng điển hình của thể loại kiếm hiệp, vừa anh hùng lại vừa quyết đoán, mạnh mẽ, với đầy đủ khí chất của một đấng nam nhi. Không kém cạnh, các nhân vật như Mặc Hương và Liên Thành mỗi người đều có một nét đẹp riêng, mạnh mẽ và vững chãi. Cốt truyện lôi cuốn, không dừng lại ở kiếm hiệp mà còn lồng ghép tình cảm qua mối tình của Thư Dạ và Mạn Hoa. Truyện có nhiều tranh vẽ rất đẹp.
4
354933
2015-07-15 20:23:08
--------------------------
215944
10874
Từ sau khi đọc “Đại mạc dao” của Đồng Hoa, mình bỗng thấy rất thích các tác phẩm liên quan đến đại mạc, vì nó gợi trong lòng mình cảm giác hùng vĩ tráng lệ dù có gì đó hơi đau thương. Đại Mạc Hoang Nhan là một tác phẩm xuất sắc của Thương Nguyệt. Đọc truyện này, điều khiến mình khâm phục và cảm động nhất không phải tình cảm nữ nhi thường tình mà là tình huynh đệ sinh tử có nhau của Thư Dạ và Mặc Hương, thấy họ kề vai sát cánh chiến đấu bảo vệ thành mà cảm phục vô cùng. Truyện kết mở, nhưng dù thế nào thì cũng mong Thư Dạ sẽ tìm được Mạn Sa Hoa.
5
393748
2015-06-26 23:57:25
--------------------------
214397
10874
Trước đây mình chưa bao giờ đọc thể loại kiếm hiệp nên không mua dạng này bao giờ. Hôm trước thấy Tiki giảm giá nên mua đọc thử xem sao. Thật sự là rất hay.
Nội dung của Đại Mạc Hoang Nhan nói về Cao Thư Dạ , một người lạnh lùng, vô cùng quyết đoán, sẵn sàng ra tay tàn độc khi cần nhưng cũng không tiếc ra tay chính nghĩa. Nhưng, vì sự độc ác và nham hiểm của Nhị nương mà cậu - một người thiếu niên chỉ mới mười ba tuổi phải bước tới một nơi khắc nghiệt đầy nguy hiểm - Tu La trường. Tuy Nhị nương đã khiến cậu bị như vậy nhưng Thư Dạ không vì vậy mà oán hận Cao Liên Thành - con trai của Nhị nương, cũng là em trai cậu. Thư Dạ đã nhờ Mặc Hương huấn luyện cho Liên Thành. Thư Dạ trở thành sát thủ của Tu La trường và ở nơi đây, cậu gặp Mặc Hương và đem lòng yêu Sa Mạn Hoa. Thư Dạ và Mặc Hương đã kề vai sát cánh bên nhau vượt qua khó khăn nơi nguy hiểm. Sau khi trốn thoát, Thư Dạ đã trở thành 1 người căm hận Minh Giáo, ban lệnh cấm Minh Giáo. Truyện có kết thúc mở, không rõ ràng cho mối tình đẹp giữa Thư Dạ và Sa Mạn Hoa.
5
461324
2015-06-24 21:56:20
--------------------------
206660
10874
Đọc đỉnh kiếm các hệ liệt đúng là một nét rất riêng. Cũng có nói về tình cảm nam nữ nhưng mang đậm chất kiếm hiệp. Trong Đại Mạc Hoàng Nhan, Cao Thư Dạ là một người mạnh mẽ, quyết đoán, cần tàn độc có tàn độc, cần chính nghĩa có chính nghĩa. Chỉ vì sự độc ác của Nhị nương đã khiến 01 thiếu niên 13 tuổi bị lâm vào hoàn cảnh khắc nghiệt trở thành sát thủ của Tu La trường nhưng không vì thế mà hận Liên Thành (con của Nhị nương), ngược lại còn nhờ Mặc Hương đào tạo Liên Thành. Sau khi trốn thoát trở về trở thành 1 người căm hận Minh Giáo. Truyện đề cao tình huynh đệ vào sinh ra tử giữa Thư Dạ và Mặc Hương, tình anh em giữa Thư Dạ và Liên Thành, mối tình chớm nở sớm tàn giữa Thư Dạ và Sa Mạn Hoa. Mặc dù kết thúc truyện là kết thúc mở, không rõ ràng cho mối tình của Thư Dạ và Sa Mạn Hoa. Nhưng sang Đế Đô Phủ đã cho thấy một kết thúc đẹp của mối tình này.
Nếu bạn đã yêu thích Thất Dạ Tuyết và Đại Mạc Hoàng Nhan thì nên đọc hết tất cả các truyện của Đỉnh Kiếm Các hệ liệt, truyện hay và đầy ý nghĩa.
4
176382
2015-06-10 11:23:25
--------------------------
201757
10874
Mình vốn không thích truyện Kiếm hiệp nên trước đây không mua. Sau đó Tiki mở event mới thấy cuốn này. Bìa rất đẹp, giấy tốt, khổ to mà giá lại rẻ so với những quyển khác nên mình mua về đọc thử. Thật sự rất là hay. Lời văn của tác giả khỏi chê, cốt truyện trội hơn hắn những truyện kiếm hiệp khác, bên cạnh đó còn có hình minh họa đẹp mê hồn nữa. 
Truyện còn mang chất ngôn tình, không nhấn mạnh cảnh đao thương mà chăm chút cho tính cách, hoàn cảnh, tình cảm của từng nhân vật, miêu tả quang cảnh sắc nét. tác phẩm mang tính nghệ thuật cao. Tác phẩm rất đáng đọc, đọc và suy ngẫm, đây không đơn giản chỉ là một cuốn tiểu thuyết.
5
638382
2015-05-28 10:52:18
--------------------------
197952
10874
Đây không phải lần đầu tôi đọc tác phẩm của Thương Nguyệt, sau Kim Dung và Cổ Long hầu như không có ai có thể đạt đăng phong tạo cực trong thế giới kiếm hiệp, nhưng với Thương Nguyệt, truyện kiếm hiệp của cô lại đậm màu sắc ngôn tình. Bút pháp lãng mạn rõ nét, sự tinh tế hòa vào từng câu chữ, những bối cảnh tuyệt đẹp, những khung cảnh kiếm hiệp hoành tráng mà không kém phần nhẹ nhàng như lưu quang phi vũ. Ngòi bút của cô như thổi hòn vào những cảnh đao thương kiếm ảnh chỉ chốc là huyết vũ loạn thiên. Nhân vật mỗi người một tính, mỗi người một đặc điểm, nhưng tất cả đều không thể thoát số mệnh, tất cả xoay quanh hai chữ quyền mưu. Tôi thích đọc thất dạ tuyết hơn bộ Đại mạc hoang nhan này, nhưng với Đại mạc hoang nhan thì văn phong trau chuốt và tiến bộ hơn rất nhiều. Kết cấu rât chặt chẽ và logic đưa người đọc khám phá đến từng câu từng chữ. Tác phẩm rất đáng đọc, đọc và suy ngẫm, đây không đơn giản chỉ là một cuốn tiểu thuyết.
4
471002
2015-05-18 20:39:38
--------------------------
197951
10874
Đây không phải lần đầu tôi đọc tác phẩm của Thương Nguyệt, sau Kim Dung và Cổ Long hầu như không có ai có thể đạt đăng phong tạo cực trong thế giới kiếm hiệp, nhưng với Thương Nguyệt, truyện kiếm hiệp của cô lại đậm màu sắc ngôn tình. Bút pháp lãng mạn rõ nét, sự tinh tế hòa vào từng câu chữ, những bối cảnh tuyệt đẹp, những khung cảnh kiếm hiệp hoành tráng mà không kém phần nhẹ nhàng như lưu quang phi vũ. Ngòi bút của cô như thổi hòn vào những cảnh đao thương kiếm ảnh chỉ chốc là huyết vũ loạn thiên. Nhân vật mỗi người một tính, mỗi người một đặc điểm, nhưng tất cả đều không thể thoát số mệnh, tất cả xoay quanh hai chữ quyền mưu. Tôi thích đọc thất dạ tuyết hơn bộ Đại mạc hoang nhan này, nhưng với Đại mạc hoang nhan thì văn phong trau chuốt và tiến bộ hơn rất nhiều. Kết cấu rât chặt chẽ và logic đưa người đọc khám phá đến từng câu từng chữ. Tác phẩm rất đáng đọc, đọc và suy ngẫm, đây không đơn giản chỉ là một cuốn tiểu thuyết.
4
471002
2015-05-18 20:39:34
--------------------------
128301
10874
Một câu chuyện với nội dung không mới, không lạ. Nhưng dưới cách viết của tác giả, tác phẩm có một sự cuốn hút của riêng nó. 
Tôi cảm phục tình bằng hữu giữa Mặc Hương và Thư Dạ, dù xa cách nghìn trùng, với thân phận, địa vị đó, nhưng vẫn tin tưởng nhau tuyệt đối, sẵn sàng chết vì nhau. Đúng với câu: "có phúc cùng hưởng, có họa cùng chịu"
Tôi cảm phục tình huynh đệ của Thư Dạ dành cho Liên Thành. "Đời này làm huynh đệ, đời sau vẫn vậy". Vượt qua mối thân thù đại hận, rốt cuộc, huynh đệ lại một nhà.
Trong tác phẩm, dù tình yêu của Thư Dạ và Mạn Sa Hoa là sự dây nối cho diễn biến câu chuyện, nhưng nó lại không phải là nhân tố chính.
Cuối cùng, Thư Dạ vẫn đặt thành Đôn Hoàng lên trước, đặt quê hương của mình lên trên tình cảm nam nữ thường tình. Ai bảo, anh hùng khó qua ải mỹ nhân, trong thời khắc đó, Thư Dạ đã vượt qua ải rồi.
Câu chuyện không dài, kết thúc nửa kín, nửa mở, nhưng nó làm nên cái hay cho truyện.
Trang bìa tuy "màu mè" nhưng theo tôi lại không đẹp. 
Đây là 1 cuốn light novel đáng đọc của Trung Quốc!
5
291719
2014-09-30 20:24:57
--------------------------
122780
10874
Truyện quá hay, trên cả tuyệt vời, hào hùng, bi tráng, lẫm liệt, tịch mịch, buồn bã. Có những tình yêu đơn phương nhưng mãnh liệt, cháy bỏng, thủy chung đợi chờ. Và cũng có những tình bạn cao cả, thiêng liêng, trong vắt như pha lê, ở đó con người ta có thể chiến đấu, hi sinh tất cả vì bằng hữu, huynh đệ tri kỷ của mình mà không hối tiếc điều gì, dù phải đánh đổi mạng sống.
Và ta cũng thấy được tình yêu lứa đôi thật quá nhỏ bé, ích kỉ so với tình yêu quê hương, tình bằng hữu. Suy cho cùng, tình yêu lứa đôi cũng chỉ là một mảnh ghép nhỏ trong tấm gương thế thái nhân tình, ta có thể si mê, chìm đắm trong nó, nhưng chỉ trong một khoảng thời gian, ta không nên hi sinh lý tưởng cả đời mình vì nó. Truyện bị gián đoạn 1 vài chỗ vì có hình ảnh thêm vào, hình ảnh thì cũng đẹp đấy, đúng chất truyện cổ trang nhưng mình thì thích mạch truyện liên tục, không gián đoạn hơn.
5
360787
2014-08-27 23:58:08
--------------------------
113192
10874
Mới đầu mình khá phân vân vì không biết tiểu thuyết kiếm hiệp có thích hợp với mình không, vì mình không thích những truyện chỉ toàn chém chém giết giết. Thế nên khi đọc mình đã rất bất ngờ.
Trước hết nói về hình thức của sách, sách khổ to, mỏng và có hình ảnh trang trí (mới đầu mình không thích lắm), font chữ vừa phải dễ đọc, giấy cũng khá đẹp.
Còn về nội dung thì như đã nói, mình rất bất ngờ vì Thương Nguyệt còn khai thác thêm những khía cạnh rất đẹp về tình bạn, tình anh em trong gia đình. Cho dù là người ngồi trên ngôi cao chót vót, cách xa ngàn trùng nhưng Thư Dạ và Mặc Hương vẫn luôn là tri kỷ, luôn sát cánh và tin tưởng lẫn nhau. Hay như Thư Dạ vốn mang mối thâm thù với mẹ của Liên Thành, nhưng vẫn luôn dốc toàn tâm ý hy vọng e trai của mình trưởng thành vững chãi.
4
55006
2014-05-27 17:39:27
--------------------------
93932
10874
Có thể nói đại mạc hoang nhan là một trong những tác phẩm xuất sắc nhất của Thương Nguyệt. Điểm đặc biệt của tác phẩm này đó là nó không chỉ dừng lại ở chuyện tình yêu, chuyện nhi nữ thường tình, những mối tình đẹp đẽ diễm lệ như những "anh chị em" khác, đại mạc hoang nhan mang một không khí lẫm liệt, hào hùng, nóng cháy của nắng cát, của chí khí nam nhân. 

Những ai đã đọc đại mạc hoang nhan có lẽ sẽ không quên được hình ảnh Mặc Hương và Thư Dạ kề vai sát cánh chống trả quân thù, bảo vệ Đôn Hoàng thành. Những người đàn ông trong đại mạc hoang nhan cũng biết yêu, biết sầu nhưng họ không vì tình mà bi lụy. Thư Dạ từ bỏ việc đuổi theo Sa Mạn Hoa để cứu người bạn thân Mặc Hương; Mặc Hương không ngần ngại uống cực lạc đan để đủ sức cùng Thư Dạ chiến đấu; Liên Thành có thể từ bỏ hận thù với người anh trai cùng cha khác mẹ vì bảo vệ Đôn Hoàng thành.

Như Thương Nguyệt đã viết, trong cả đời người, có những thứ khác ngoài mộng tưởng và bá đồ, đó chính là tình huynh đệ và cố thổ.

Khi đọc truyện, tôi có thể thấy được bóng dáng của bậc anh hùng trong những người nam nhân như Mặc Hương, như Thư Dạ ấy.

Nói về tình cảm giữa Thư Dạ và Sa Mạn Hoa, cá nhân tôi thấy nó khá mờ nhạt so với tình bạn của Thư Dạ và Mặc Hương. Cái lý do Thư Dạ yêu Sa Mạn Hoa chưa đủ để thuyết phục tôi về việc anh ta có thể si mê cô như vậy.

Còn một điều nữa đáng để nói tới ở đại mạc hoang nhan, đó chính là cái không gian tĩnh lặng vương nét buồn u uẩn bao trùm lên cả tác phẩm. Cái lặng lẽ ấy không ào đến một cách bất ngờ, nó ẩn mình trong từng câu chữ, cứ chậm rãi nhấm nuốt, thâm nhập vào trái tim người đọc.

Bản dịch đại mạc hoang nhan có chất lượng tốt, tôi đặc biệt thích cách dịch về đại mạc và Đôn Hoàng thành của dịch giả, chỉ một câu: "Tái ngoại mênh mang, ba vạn dặm ngập tràn gió cát." đã truyền tải được hết không khí của tác phẩm. Tuy nhiên hơi đáng buồn là Đông A đã cắt đi một số đoạn khá hay.
5
57447
2013-09-08 11:38:26
--------------------------
86721
10874
Là cuốn mở đầu của hệ liệt “Đỉnh kiếm các” nên có lẽ Thương Nguyệt không muốn tác phẩm của mình trở nên quá bi thương. Cuối cùng Công tử Thư Dạ có tìm được Sa Mạn Hoa không? Đó là một câu hỏi vẫn còn bỏ ngỏ, nhưng điều đó cũng không còn thật sự quan trọng. Theo ý kiến cá nhân của mình, nhân vật Sa Mạn Hoa xuất hiện khá mờ nhạt và dường như không gây được nhiều sự chú ý, tình cảm của người đọc. Nổi bật nhất có lẽ chính là tình huynh đệ của hai đệ nhất sát thủ Tu La Trường – Thư Dạ và Mặc Hương – một điều tưởng chừng như không thể. Một điểm cộng lớn cho truyện là trình bày, bìa truyện rất đẹp, giống như cuốn Bỉ ngạn hoa. 
4
63254
2013-07-12 12:51:56
--------------------------
77797
10874
Truyện thật sự rất hay, lối viết của Thương Nguyệt quả thật không chỗ nào chê, từng câu văn, lời nói của nhân vật đều được trau truốt kĩ lưỡng, truyện đã để lại cho mình nhiều ấn tượng sâu sắc. 
Mỗi nhân vật trong truyện đều được đầu tư khá kĩ lưỡng, tác giả đã khắc họa rõ nét về con người họ, cả tính cách lẫn ngoại hình, thật sống động làm ta dễ liên tưởng và ghi nhớ. Truyện tuy có nhiều nhân vật nhưng các nhân vật đều được phân biệt rất rõ.
Truyện được đầu tư nhiều, về tinh tiết rất thú vị và lôi cuốn hấp dẫn, ngôn từ trau truốt, những tâm tư tình cảm của nhân vật, hấp dẫn ta đến trang cuối cùng. 
Bìa truyện cũng rất đẹp và bắt mắt, giá cả lại hợp với túi tiền, một sự lựa chọn hợp lý. 

4
57869
2013-05-29 17:57:45
--------------------------
60604
10874
Mình là một fan của Thương Nguyệt. Tác phẩm hay nhưng đáng tiếc đã bị cắt bỏ nhiều đoạn hay để chèn vào hình ảnh. Truyện vừa bi tình vừa hào hùng đúng chất TN, đậm chất nữ nhi mềm mại . Công tử Thư Dạ tưởng chừng là một người sắt đá nhưng hóa ra lại ẩn sâu bên trong một trái tim mềm yếu, một tình yêu bất diệt đói với Sa Mạn Hoa - tam thánh nữ của Quang Minh đỉnh, một người con hiếu thảo thà để cho người em cùng cha khác mẹ hận mình đến xương tủy cũng không kể những việc xấu xa mẹ mình đã làm, một người chủ thành đầy trách nhiêm. Cảm động trước tình bằng hữu của Thư Dạ và Liên thành, thích cái cách anh hùng không bi lụy vì mỹ nhân nhưng lại dám hy sinh cả tính mạng vì bằng hữu "Liên Thành mấp máy môi, nhưng vẫn không sao nói nên lời, rồi bỗng nhiên lại lao đến như một mũi tên, cất tiếng gọi khẽ, “Đại huynh”. Ấn tượng nhất là đoạn Mặc Hương lên núi Kỳ Liên thay cho Thư Dạ để rồi chịu một mũi tiễn của Sa Mạn Hoa. 
5
6178
2013-02-24 10:07:53
--------------------------
30377
10874
Thương Nguyệt là một trong số ít tác giả nữ viết truyện kiếm hiệp rất thú vị. Dù là tranh đấu giang hồ nhưng truyện của cô gái này còn chút hơi hướm nữ tính, nhờ vậy truyện của cô có một cái đẹp nao nao mà không kém phần mềm mại, tạo cho truyện một âm hưởng rất riêng. Đại mạc hoang nhan là câu chuyện hoà trộn tuyệt vời giữa chất hào khí và cảm giác bi tráng. Mình khá ấn tượng với nhân vật Thư Dạ và tình huynh đệ của anh ta. Anh ta yêu Sa Mạn Hoa tưởng như có thể vì nàng bỏ ra tất cả, kể cả bản thân mình, nhưng vào giờ phút quyết định đã chọn từ bỏ nàng vì huynh đệ. Đấy là một nghĩa cử đẹp mà chỉ có bậc trượng phu mới làm được. Đáng tiếc là không biết vì sao bản dịch đã cắt bỏ khá nhiều doạn, mà còn là những đoạn đắt giá của truyện nữa chứ >”<
4
5548
2012-06-14 14:54:46
--------------------------
29811
10874
Câu chuyện của Thư Dạ được tác giả thêu dệt vừa mang màu sắc cổ trang hào hùng, vừa thấm đấm chất bi thương sâu sắc.

Đây là quyển truyện khiến tôi không hề muốn đọc lướt hay bỏ qua một đoạn nào ( thường thì đối với đa số truyện tôi hay đọc lướt một vài đoạn mà tôi thấy không gây hứng thú cho bản thân). 

Từng câu từng chữ khiến bạn say mê, không thể nào rời mắt, cảnh vật, con người xung quanh được miêu tả hết sức sống động và gợi hình.

Quyển sách tuy không dài nhưng lại hội tụ tất cả các yếu tố hỉ, nộ, ái, ố.

Các lời nhận xét chi tiết về tác phẩm đã được các bạn trên nói rõ, chỉ có đọc tác phẩm bạn mới có thể cảm hết được những gì tác giả muốn mang đến cho người đọc.

5
19881
2012-06-08 14:28:06
--------------------------
29757
10874
Đôn Hoàng thành chủ công tử Thư  Dạ. Từ nhỏ bị mẹ kế bán cho Hồi  Hột, rồi trở thành sát thủ trong Tu La Trường của  Quang Minh Đỉnh. Trải qua trăm trận chém giết, và biết bao đau đớn ở nơi địa ngục trần gian đó, ngỡ như công tử Thư Dạ đã trở thành một người chai sạn. Nhưng chàng không như thế. Vẻ ngoài sắt đá là để che giấu một trái tim mềm yếu, một tình yêu bất diệt.

Thư Dạ công tử yêu Sa Mạn  Hoa, tam thánh nữ của Quang Minh đỉnh, thánh nữ  Đãi  Nguyệt của Bái  Nguyệt  Giáo. Chàng vì nàng mà xây Cực Lạc cung, chỉ mong trong thời khắc mê muội mà gặp lại dung nhan cũ. Chàng vì nàng mà muốn tìm đến cái chết trong tay người mình yêu thương. Tình yêu của  Thư  Dạ dành cho  Sa Mạn  Hoa mãnh liệt đến mức nàng trở thành giấc mộng của chàng, là điều suốt đời ao ước.

Đây chắc là tác phẩm có được cái hào khí lẫm liệt nhất trong số tác phẩm của  Thương Nguyệt. Hình ảnh Thư  Dạ từ bỏ đi mộng tưởng Sa Mạn Hoa để quay trở về Đôn Hoàng, hình ảnh Mặc Hương uống cả vốc  Cực  Lạc đan (một loại thuốc phiện) cưỡi ngựa tiếp chiến dù đã cận kề cái chết đều là những cảnh tượng đận chất anh hùng ca.

Chính sự hào hùng này mới là thành công vượt bậc của  Đại  Mạc  Hoang Nhan trong lòng người đọc. Rất hiếm khi đọc một tác phẩm vừa bi tình mà lại vừa hào hùng như vậy. Mộng tưởng của Thư Dạ là Sa Mạn Hoa, còn của Mặc Hương là quyền lực, nhưng 2 người có thể sẵn sàng từ bỏ mộng tưởng của bản thân để cứu lấy người huynh đệ đã vào sinh ra tử cùng mình.
5
5230
2012-06-08 09:29:56
--------------------------
27852
10874
Với lối văn phong đậm chất nữ nhi, câu chữ mềm mại và quyến luyến người đọc, nếu ai đã từng yêu thích Thất Dạ Tuyết thì nhất quyết ko nên bỏ qua Đại Mạc Hoang Nhan.

Cũng giống như Thất Dạ Tuyết, tác phẩm này cũng đầy cái không khí dư tình diễm lệ, khiến lòng người rung động mạnh mẽ. Thư Dạ yêu Sa Mạn Hoa, tinh thánh nữ của Quang Minh đỉnh. Chàng ta vì nàng mà bất chấp tìm đến cái chết, dù là chết dưới tay của người mình yêu thương cũng đã mãn nguyện. Bởi vì tình yêu của  Thư  Dạ dành cho  Sa Mạn  Hoa mãnh liệt đến mức nàng trở thành bá mộng của chàng, là điều suốt đời ao ước.

Thêm vào đó, tác phẩm còn khắc họa tình tri kỉ bằng hữu của 2 người từng là đệ nhất sát thủ Tu la trường - vốn là nơi địa ngục khắc nghiệt ko có tình người. Tình tri kỉ giữa Thư Dạ và Mặc Hương vượt qua cả mộng tưởng, hơn cả bá đồ, dùng chính sinh mạng của mình để tin tưởng lẫn nhau, dù là mười năm, là hai mươi năm, hay đến cả đời đối với nhau vẫn là sự tận tình gắn bó mãnh liệt như thời thơ dại.


Điều cuối cùng thích quyển sách này là vì hình vẽ minh họa - quá đẹp và rất phù hợp. Hình vẽ minh họa ko làm hạn chế cái trí tượng tượng của người đọc mà càng khiến cho ko khí truyện càng trầm buồn.  

4
21199
2012-05-22 12:32:24
--------------------------
17635
10874
Đọc truyện mà như đang xem một bộ phim điện ảnh.Thương nguyệt đã sử dụng một ngôn ngữ chau chuốt t giàu chất gợi tả làm cho bạn như đang sống trong bối cảnh của câu chuyện.Với việc tái hiện lại con đường tơ lụa nhộn nhịp,hoang mạc bao la rộng lớn với cát gió, thành trì nguy nga, tráng lệ.
Từ hình ảnh bìa sách đến các ảnh minh họa đều đẹp và mỹ lệ.Các nhân vật trong truyện mỗi người một tính cách,một vẻ đẹp tâm hồn, một suy nghĩ, một hoài bão.Truyện đề cập đến tình yêu ít nhưng đề cao tình bạn cao thượng giữa Thư Dạ và Mặc Hương thì nhiều .Có thể vì người bạn của minh mà hi sinh(đôi lúc cách nói chuyện của hai người đầy gai góc mưu mô, lợi dụng nhau, nhưng ẩn sau đó là một tình bạn rất đẹp, lo lắng cho nhau, lặng lẽ giúp nhau).
Điểm đáng tiếc ở truyện là hơi ngắn, đọc xong rồi mà có cảm giác chưa đã.Bởi thế, mới giống một bộ phim điện ảnh.!

4
21119
2012-01-13 12:20:10
--------------------------
8263
10874
 Tôi tiếp cận với các tác giả Thương Nguyệt, trở nên thích tác phẩm của cô ấy là từ Đại Mạc Hoàng Nhan. 
 Đọc xong Đại Mạc Hoàng Nhan, để lại ấn tượng trong tôi sâu sắc nhất là tình bạn của Thư Dạ và Mặc Hương. Nếu để tìm một cuốn sách để tặng người bạn thân nhất của mình, tôi sẽ chọn cuốn Đại mạc hoàng nhan. Tình bạn của Thư Dạ và Mặc Hương khiến tôi rất cảm động. Khi đọc đoạn cuối truyện, tôi đã khóc vì hành động của Mặc Hương khi thay bạn nhận lời khiêu chiến của Sa Mạn Hoa mặc dù biết trước trận chiến đó là cái chết đang chờ mình ngay trước mặt. Tôi cảm động vì hành động của Thư Dạ không tiếc mạng sống, gạt bỏ sự tự tôn của mình mà van xin người em hãy mở cửa cho Mặc Hương vào thành. 
 Truyện hay còn bởi các nhân vật phụ trong chuyện được xây dựng khá công phu, tỉ mỉ, gây ấn tượng mạnh cho người đọc. Tôi không thể quên một Lục Cơ trung thành một cách mê muội, một Liên Thành non nớt, ngây thơ....Các tình tiết trong chuyện được sắp xếp hợp lý, nhiều cho tiết để lại dấu ấn mạnh trong tôi. Tên các nhân vật trong truyện, tên các chiêu thức võ học cũng như các địa danh rất lạ và cũng rất hay. Đặc biệt, cách kể truyện và miêu tả cảnh, nội tâm nhân vật của tác giả rất hay, đọc truyện ta có thể tưởng tượng ra được mọi thứ như đang diễn ra ngay trước mắt.
 Đại mạc hoàng nhan - một cuốn sách không thể bỏ qua của Thương Nguyệt.
5
3780
2011-07-14 23:42:12
--------------------------
4683
10874
Những ai đã và đang mê mẩn " Thất Dạ Tuyết" của Thương Nguyệt thì sẽ không thể nào bỏ qua tác phẩm thứ 2 của cô:" Đại Mạc Hoang Nhan" . Cốt truyện từ những văn từ đầu tiên xuyên suốt đến những thanh âm cuối cùng của 1 bài hát được truyền bá trong mây gió của công tử Thư Dạ đều nhuốm 1 gam màu ảm đạm, 1 nhịp điệu trầm mặc. Mặc dù những ngôn từ mà nữ văn sĩ tài ba ấy đã dùng để dẫn dắt mọi người xuyên suốt 1 kí ức thăng trầm, 1 tình yêu khốn khổ,1 sự hi sinh cao cả rất nhẹ nhàng, rất thướt tha, êm đềm,nhưng sau khi gấp quyển sách vào bạn lại cảm thấy 1 thứ cảm giác lạ lùng dấy lên trong mình.Mỗi nhân vật đề có tính cách,hình tượng khác nhau, tâm tư sâu sắc và ăn sâu vào trái tim người đọc. Và bạn sẽ không thể nào quên được hình ảnh chàng hoàng đế trẻ tuổi tài ba năm nào đã nhường lại ngôi vị cho người em trai yêu quý và 1 mình 1 ngựa rong ruổi khắp sa mạc tìm lại người con gái khi xưa đã đánh cắp trái tim của y
Người đã ra đi, chỉ còn lại tiếng thiết mã vang trong gió thu, thư kiếm lẻ loi cho ánh mắt đau buồn.
   " Ngày xưa lầu cao vang tiếng ca sớm, tiếng đàn đêm, trên đó có tà áo vũ nữ khuynh quốc khuynh thành.
    Những thiếu niên du hiệp đất Yên Triệu, dọc ngang uống cạn rượu trong chén vàng.
    Rượu trong chén vàng, vứt bỏ cả nỗi sầu !
    Vứt bỏ hẳn nỗi sầu, khúc ca mới xin đừng hát tiếng biệt ly.
    Cô gái nhà ai ngày xưa ấy, ngoảnh đầu mong lại được tương phùng ? Tâm ý thiết tha trong lòng nàng, lầu son cách mấy muôn trùng.
    Mười bước giết một người, khảng khái tại cung Tần. Ngần ngừ không muốn buông cung, phất phới bóng chim hồng kinh sợ.
    Chẳng nề núi sông nghiêng ngửa, tri kỷ chênh vênh giữa sống và chết.
    Ôm bảo đao, ca khóc giấc mộng ngắn ngủi, tung hoành mây gió phút chốc tay không.
    Dựa lan can không nói, ngang ngửa thổi khúc Tam lộng, hỏi anh hùng, đâu là anh hùng ?) "
5
5191
2011-05-20 22:11:15
--------------------------
