234177
10797
Là quyển thứ hai trong bộ Năm vị tổ sư, truyện là cuộc hành trình theo chân cậu bé Hầu trong chuyến phiêu lưu đi kiếm tìm và giải đáp những bí ẩn về thân thế của chính mình và của chùa Ẩn Chân. Khác với Hổ bộc trực và thẳng tính, Hầu tinh ranh và có nhiều mưu kế hơn - giống hệt một chú khỉ, khiến truyện có nhiều chi tiết hài hước và dí dỏm hơn. Bên cạnh đó, thêm nhiều bí ẩn xung quanh các nhân vật được hé lộ giúp mạch truyện càng hồi hộp và hấp dẫn, tạo sự tò mò cho người đọc. Mình sẽ tiếp tục theo dõi bộ truyện này. 
5
387532
2015-07-20 10:05:07
--------------------------
