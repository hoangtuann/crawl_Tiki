575333
10149
Ấn tượng đầu tiên của mình về cuốn sách là bìa sách rất đẹp. Tiki giao hàng nhanh chóng. Lúc mới nhận hàng về mình chọn ngay cuốn này để đọc. Và giờ mình đang đọc lại lần 2. Quả thực đây là một cuốn sách rất đáng đọc ít nhất 1 lần trong đời cho bạn nào thích đọc sách. Cảm ơn Tiki một lần nữa vì cách phục vụ tận tình và giao hàng nhanh chóng :) :) :)
5
2191148
2017-04-17 10:36:13
--------------------------
553509
10149
Bìa sách đẹp, hình thức trình bày, màu sắc bắt mắt, chất liệu giấy tốt, Tiki giao hàng nhanh, không chê vào đâu được. Tuy nhiên về mặt nội dung, nếu các bạn ít hoặc chưa từng đọc các sách khác về giao tiếp có thể sẽ thấy sách này hay. Với quan điểm cá nhân của mình, sách này cũng chỉ đưa ra được 1 số ít vấn đề trong giao tiếp, hay nhưng chưa đủ, các bạn vẫn cần tìm đọc thêm nhiều sách khác mới có thể hoàn thiện kĩ năng nói chuyện của mình. Dù vậy, ở những vấn đề mà sách nêu ra, vẫn rất thiết thực, bổ ích và nên đọc.
4
2196126
2017-03-25 00:03:21
--------------------------
520577
10149
Sách khá mỏng, có in bìa khá đẹp, viết về những kì năng rất cần thiết khi giao tiếp. Tuy hơi khó ghi nhớ và  ứng dụng tất cả vào thực tế nhưng sau khi đọc xong mình đã giao tiếp hiệu quả hơn rất nhiều
4
331649
2017-02-07 15:35:53
--------------------------
482307
10149
Sách được in dưới dạng khổ to, khác lạ với một số sách khác. Sách đề cập đến những kỹ năng giao tiếp cần thiết trong cuộc sống thường ngày bằng cách sử dụng hình minh họa, rất thu hút, và tạo cảm giác thú vị khi đọc. Sách tuy không nhiều trang, khá mỏng, đọc rất nhanh nhưng có nhiều điều cần học hỏi. Đây là cuốn sách phù hợp cho nhiều lứa tuổi, học sinh hay người đi làm đều có thể đọc vì sách cung cấp nhiều thông tin bổ ích về những kỹ năng mềm mà ai cũng cần có.
4
637448
2016-09-05 11:51:44
--------------------------
458121
10149
Cuốn sách này khá mỏng, bìa trang trí đẹp. Và khi đọc cuốn này, mình cũng học hỏi thêm 1 vài ý tưởng, hướng suy nghĩ ở các khía cạnh của giao tiếp. Với 1 người mới, đang muốn cải thiện mình, thì cuốn này cung cấp chưa đủ toàn diện các kỹ năng nền tảng về giao tiếp. 
Theo mình, nó chỉ cung cấp các kỹ thuật và được sử dụng trong 1 số trường hợp trong giao tiếp thôi. Với người hay giao tiếp với khách hàng như mình, thì cuốn sách này, giúp mình tham khảo thêm 1 vài trường hợp giao tiếp.
Bạn nên đọc cuốn Nghệ Thuật Giao Tiếp Để Thành Công - leil lowndes, để xây dựng nền tảng cơ bản giao tiếp.
3
65325
2016-06-24 21:41:23
--------------------------
444095
10149
Sách này mình mua trong đợt giảm giá, vừa nhìn thấy là thích vì trang giấy trang trí dễ thương, bắt mắt, chất liệu giấy và màu in tốt. Về nội dung cũng tạm ổn, những kiến thức đa phần là mình đã biết rồi, coi như xem lại củng cố thôi (chắc có lẽ mình đã đọc được từ nhiều nguồn khác rồi). Mình để dành cho mấy đứa em nhỏ nhỏ đọc vì sách này không kén độ tuổi lắm. Có nhiều hình vẽ minh họa sống động. Với mức giá khuyến mãi thì mình thấy hài lòng về quyển sách này.
3
182814
2016-06-07 21:10:36
--------------------------
434489
10149
Sách có nội dung và bố cục rõ ràng, rất nhiều điều cần biết về giao tiếp được đưa ra cụ thể và dễ hình dung cho người đọc. Mặc dù thuộc về thể loại sách kỹ năng nhưng giọng văn không nhàm chán, dài dòng. Hơn nữa  những bí quyết mà sách đưa ra đều thực tế, có thể áp dụng vào đời thực. Sách lớn, giấy in màu đẹp. Đáng lẽ sách xứng đáng 5 sao nhưng quyển mà mình nhận được lại có bìa cong và nhăn nheo do nhân viên chuyển hàng sắp xếp sách không cẩn thận nên nhìn cuốn sách xấu hẳn. Mình mong Tiki nên khắc phục việc này.
2
1224595
2016-05-23 11:10:20
--------------------------
429847
10149
Khi bạn nói chuyện với một ai đó, cần phải nói đúng vào trọng tâm. Không phải cứ nói luyên thuyên ra vẻ mình là người hiểu biết. Nói nhiều vẫn đúng nhưng người đối diện cảm thấy bạn nói quá nhiều, phải ngồi nghe bạn nói,  bạn đang dạy đời họ. Nói ít nhưng mà đúng, họ sẽ cảm thấy bạn thật giỏi, khéo nói, giao tiếp giỏi, tiếp thu vấn đề nhanh, tầm thật nhìn bao quát..v... v..  
Cuốn sách dạy bạn kỹ năng trong giao tiếp, cách hành xử, chứ không phải muốn nói gì thì nói. 
Cũng có 1 cuốn sách rất hay Khéo Ăn Khéo Nói Sẽ Có Được Thiên Hạ.
4
1209811
2016-05-14 00:09:46
--------------------------
413997
10149
Sách rất hay, nội dung tốt, có giải thích dễ hiểu, rõ ràng, tường tận. Chất lượng giấy ổn, in màu đẹp, bắt mắt. Từng phân mục được trình bày rõ trong những ô khác màu và tiêu đổ nổi bật. Nhưxng nhân vật trong từng tình huống làm rõ được vấn đề đang nhắc đến. Đôi khi mình thấy được bản thân mình giống như người đang mắc lỗi trong những mẩu truyện nhỏ này, từ đó giúp mình lưu ý hơn.  
  Tuy là đọc xong mình không nhớ được hết và không thực hành được ngay nhưng chỉ một vài đieefu mình học được thôi đã rất có ích cho mình r. Các bạn nên mua nhanh nhanh lên nhé!!!
5
1193702
2016-04-10 20:05:21
--------------------------
411194
10149
Cuốn sách trình bày khá thú vị, bản lớn, có nhiều hình và ví dụ minh họa có thể áp dụng trong nhiều trường hợp : giao tiếp với bạn bè, trong công việc, khi nói chuyện với người lạ, lúc nói chuyện với người thân,... Những bí quyết được trình bày dưới dạng những lời khuyên, dẫn dắt từ từ và hợp lý. Đây là cuốn sách rất hữu ích cho những bạn trẻ, trong đó có mình để có thể giao tiếp tự tin, hiệu quả hơn trong công việc, học tập cũng như trong cuộc sống thường ngày.
4
522164
2016-04-05 18:04:41
--------------------------
410310
10149
Xét về hình thức, cuốn sách này hơi to một chút, nên khi dùng cũng không được tiện cho lắm. Các trang sách in mầu và có cả hình ảnh minh họa rất bắt mắt, hấp dẫn. Về nội dung: cuốn sách đề cập đến 36 cách để chiếm được tình cảm của người khác. 36 cách này được trình bày rất khoa học, rõ ràng, bên cạnh đó có hình ảnh minh họa luôn nen rất dễ hiểu. Mình đã mua cuốn sách này cách đây gần 1 tháng rồi. Khi nhận sách cũng rất hài lòng về nội dung. Có điều duy nhất thấy khó chịu là Plastic bị vỡ. Rất mong Tiki điều chỉnh lại việc bọc sách sao cho vừa vặn, không bị ngắn quá so với sách, hay kiểm tra lại xem plastic có bị bong hay rách chỗ nào không. Lần đầu mua sách ở tiki rất hài lòng với dịch vụ nên đã đánh giá thang điểm 10/10. Vậy mà lần sau mua thấy hơi thất vọng một chút về dịch vụ bookcare. 
3
1144123
2016-04-03 21:49:55
--------------------------
375015
10149
Trong cuộc sống ta đều đã từng gặp những người rất nhiệt tình và sôi nổi đưa ra ý kiến của mình, thể hiện quan điểm của mình một cách rất nhanh chóng và nói rất to. Tự nghĩ trong đầu những người này ba hoa bốc phét và thích thể hiện mình. Nhưng thực chất có thể họ chỉ muốn trình bày quan điểm của mình thôi chứ không có dụng ý khoe mẽ nào khác. Cuốn sách cho chúng ta nhận ra những cách giao tiếp "quá lố", biết tiết chế bản thân, biết nên nói gì, điều gì chưa phải lúc thông qua hình ảnh minh họa đẹp và những tình huống dí dỏm
5
905408
2016-01-27 16:48:33
--------------------------
318172
10149
Hình thức trình bày sách khá lạ, khổ to hơn bình thường, in màu và chất lượng giấy rất tốt. Cuốn sách làm m khá thích vì không cần nâng niu nhẹ nhàng như các quyển sách khác bởi nó rất chắc chắn.
Về nội dung, cuốn sách phân ra làm 3 phần: Nghệ thuật Im lặng - Quan sát - Lắng nghe. Trong mỗi phần lại là những bí quyết rất hữu dụng đủ để mọi người áp dụng, phần tranh - ảnh cũng tốt nhưng nhiều tình huống trong văn phòng - công ty quá! 
Tóm lại, đây là cuốn sách khá bổ ích cho những ai muốn nâng cao kỹ năng giao tiếp. M thấy nhiều sách khá lan man, dài dòng kiểu giáo huấn, còn quyển này không như vậy. Rất cảm ơn tủ sách "Bạn tốt". 
4
339241
2015-10-05 11:01:28
--------------------------
270457
10149
Nói Nhiều Không Bằng Nói Đúng (Tác giả 2.1/2 Bạn Tốt) là một người bạn, người anh hướng dẫn bạn giao tiếp một cách tốt hơn và hiệu quả hơn thông qua 3 cách rất rất đơn giản "IM LẶNG, QUAN SÁT, LẮNG NGHE" để bạn có thể lấy được tình cảm của mọi người xung quanh. Nhưng cần rất nhiều ở mọi người sự rằng luyện, thực hành để trở nên thành thục. Cách trình bày với rất nhiều hình ảnh minh học và màu sắc rất thu hút cho bạn đọc. Một cuốn sách hữu ích bạn không cần nói nhiều hãy nói đúng.
4
504087
2015-08-18 11:35:51
--------------------------
258397
10149
36 bí quyết xuyên suốt từ im lặng đến quan sát và cuối cùng là lắng nghe. Cuốn sách tuy mỏng nhưng tóm gọn và khá nhiều trường hợp cụ thể được đưa ra ví dụ mà ta có thể áp dụng cho thực tiễn. Ngoài ra cuốn sách được in màu với những nhân vật vui mắt nên đọc khá thú vị. Mình mua gần hết sách về kỹ năng nói chuyện của nhà sách Minh Long, ngoài cuốn sức hút của kỹ năng nói chuyện dành cho phái đẹp ra thì cuốn này trình bày sinh động giúp mình dễ nắm bắt vấn đề hơn, lâu lâu đổi khẩu vị đọc sách một chút sẽ khiến mình thoải mái hơn. 
4
74132
2015-08-08 15:10:09
--------------------------
251375
10149
Đã mua. Sách bản to. Giấy đẹp, in màu sinh động. Sách nêu ra những bí quyết, kỹ năng được lồng ghép kết hợp với những câu chuyện nho nhỏ qua các nhân vật, nên khiến người đọc hứng thú hơn và không có cảm giác khô khăn hay nhàm chán như cách trình bày sách toàn chữ thông thường (thích hợp cho những bạn dễ chán hay lười đọc sách nhiều chữ). Đây là một quyển sách rất hữu ích cho giới trẻ, đặc biệt là những bạn sắp ra đời, sắp phải tiếp xúc với các mối quan hệ xã hội nhiều hơn. Vì vậy biết vận dụng thuần thục các kỹ năng mềm là một lợi thế trong cuộc sống, và NNKBND có thể giúp bạn điều này.
5
82495
2015-08-02 19:09:46
--------------------------
