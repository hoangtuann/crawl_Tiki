333077
10035
Sách in màu, giấy láng đẹp mắt, hình ảnh minh họa sinh động. Tựa đề cũng rất thu hút "Vì sao bạn chưa giàu",nhìn là muốn mua đọc thử ngay. Nội dung trong sách bám sát với tựa đề, phân chia các mục muốn trình bày rõ ràng. Tác giả lần lượt phân tích các nguyên nhân từ những ý nhỏ nhặt đến những việc lớn hơn khiến bản thân con người chưa giàu lên được. Cách viết và kể như lời nói chuyện của một người bạn đang chia sẻ kinh nghiệm, hướng dẫn và chỉ ra những điểm mà bạn chưa làm đúng - chưa làm tốt; không hề có phê phán hay chỉ trích gay gắt. Là quyển sách đọc vừa để giải trí, vừa có thể ghi nhận những lời khuyên & bài học hữu ích cho việc làm giàu của các bạn trẻ trong giai đoạn khởi nghiệp - lập nghiệp..    
4
289786
2015-11-07 12:59:03
--------------------------
331107
10035
Cuốn sách đưa ra những ví dụ thiết thực trong đời sống công sở nhưng có vẻ thiên về nghệ thuật quản lý tiền bạc hơn làm giàu. Hình ảnh trẻ con có thể không hợp với những người nghiêm túc, người thích học bằng hình ảnh sẽ thấy thích. Lý thuyết hay nhưng nhiều và khó nhớ để hệ thống, nhiều điểm không ứng dụng được ngay. Phù hợp với người đang làm công muốn tiết kiệm nhiều tiền, còn người muốn lập nghiệp không thích hợp để đọc cuốn sách này. Ngoài ra, chất lượng giấy và màu sắc rất tốt, rõ ràng  và dễ đọc.
3
59986
2015-11-03 23:05:12
--------------------------
305516
10035
Sách này thật sự rất bổ ích. Có thể nhận ra rất nhiều thứ từ nó. Sau khi đọc tôi cảm thấy sự thay đổi rõ rệt về mọi thứ trong cuộc sống. Bạn nên mua để đọc và cảm nhận. Sách có rất nhiều ví dụ minh họa rõ nét, chân thực và thực tế. Bạn có thể dễ dàng nhận ra mình trong 1 số ví dụ minh họa của sách. Sách có màu sách bắt mắt, gần gũi và tạo ra sự ham muốn không cảm thấy nhàm chán khi đọc. Từ đó có thể có được rất nhiều điều bổ ích.
4
750025
2015-09-16 23:35:09
--------------------------
280976
10035
Sách viết rất hay và hữu ích! Đọc sách xong mình rút ra được một số điều giúp mình chi tiêu hợp lý và tiết kiệm được nhiều tiền hơn. Giúp mình hiểu rõ hơn về sự khác biệt trong tư duy của người giàu và người nghèo. Hy vọng những điều trong sách viết sẽ góp một phần cho những thành công về tài chính của mình sau này.
Lối hành văn đơn giản, dễ hiểu. Bìa sách trang trí đẹp. 
Tiki làm việc rất chuyên nghiệp. Minh mới đặt sách ngày hôm trước là hôm sau nhận được rồi!

5
772683
2015-08-28 01:28:09
--------------------------
211037
10035
Một cuốn sách được nhóm tác giả khá chăm chút từ nội dung cho tới cách diễn đạt, hình ảnh và câu từ khá trẻ trung. Phù hợp với những bạn trẻ hơn là những người lớn tuổi hay đã đi làm. Đa số cuốn sách là những câu chuyện đời thường của một nhóm nhân viên văn phòng về cách tiêu tiền và cũng như đầu tư. Bạn có thể sẽ thấy một chút gì đó của mình trong đó. Ngoài ra cuốn sách còn đề cập đến một số cách làm tăng thu nhập thụ động cho đọc giả. Tuy nhiên giá cuốn sách còn hơi cao so với mặt bằng chung. Lý do có thể là do sự đầu tư chăm chút hình ảnh nội dung màu sắc cho cuốn sách.
3
32372
2015-06-20 00:53:07
--------------------------
185842
10035
Tôi biết cuốn sách này do thằng bạn giới thiệu từ khi đọc xong Tối ưu hóa trí thông minh. Cùng một công ty phát hành là Đông Á, chất lượng của những trang sách trong quyển sách này rất tuyệt vời. Hiếm khi chúng ta thấy được những quyển sách được là bằng loại giấy cực tốt như thế này. Nội dung và tranh vẽ được in màu làm cho chúng ta thích thú hơn khi đọc và dễ tư duy hơn. Với khả năng trình bày tốt, dễ hiểu và ngôn từ gần gũi với đời sống con người, quyển sách cho chúng ta những kỹ năng và tư duy để có thể giàu có hơn trong cuộc sống. Với những nội dung trong 128 trang sách, quyển sách đã lí giải cho chúng ta biết "Vì sao bạn chưa giàu ?"
5
387632
2015-04-20 02:15:08
--------------------------
169307
10035
Riêng mình thấy khi đọc quyển sách này, hình ảnh, nhân vật, các câu chuyện trong này đã thể hiện lên cho mình nhiều bài học mà không thấy chán khi đọc tiếp. Để biết được nhiều hơn về các vấn đề về kinh tế, cách giải quyết, đọc quyển này vừa dễ đọc khi có nhiều hình ảnh màu sắc và còn dễ hiểu khi những hình ảnh, câu chuyện ấy nói lên những ý nghĩa rất thực tế, gần gũi.Các bạn trẻ cũng có thể dễ dàng đcọ quyển này với cách trang sách đặc sắc này mà không chán, có thể tiếp xúc với xã hội sớm hơn qua quyển này!
5
540461
2015-03-17 22:38:03
--------------------------
131405
10035
Quyển sách này không chỉ thú vị, chất lượng giấy tốt, minh họa đẹp mắt mà nó còn ẩn chứa nhiều triết lý sống giàu có và thanh đạm, dẫn tới hạnh phúc.

Sách còn có các nhân vật hư cấu, mỗi người tượng trưng cho các tính cách khác nhau phản ánh ý chí của người đọc (bốn người được nêu, 2 người thành đạt, 1 người ất ơ, 1 người còn lại là thất bại).

Để trả lời cho câu hỏi "vì sao bạn chưa giàu", các tác giả đã đưa ra rất nhiều tình huống cụ thể, từ cách suy nghĩ, lối ăn nói, cách chi tiêu của người nghèo, giúp chỉ ra tại sao bạn chăm chỉ mà vẫn cứ nghèo, các sai trái giúp người đọc kiểm soát túi tiền của mình. 

Rất rất nhiều điều trong số này, dù vô ý hay cố ý, chúng ta đều đã gặp phải. Chính vì nói ra nhiều điều động chạm đến lối sống vật chất và hưởng thụ của một số người  nên quyển sách này không được nhiều người ưa thích. Riêng mình cho rằng, quyển sách này cực tốt cho bất kỳ ai muốn làm giàu chân chính.
5
418158
2014-10-24 19:49:40
--------------------------
90721
10035
Mình thấy cuốn sách này rất hay cả về nội dung lẫn hình thức, chất liệu giấy và hình ảnh minh họa rất đẹp, đọc cảm thấy rất thích thú vì có những câu chuyện công sở rất đời thường Việt Nam đan xen vào mỗi một quy tắc nên không gây nhàm chán khi đọc. Các quy tắc đều rất thiết thực như “mua đúng giá trị chứ đừng ham rẻ” hay “đừng nên xem tivi quá nhiều”, “đừng thẫn thờ”… đều rất thực tế và dễ áp dụng để từng bước chuyển từ tư duy người nghèo sang tư duy của người giàu. Một khi có tư duy người giàu rồi thì chuyện làm giàu chỉ là vấn đề thời gian. Một điểm không thích của sách là in trên giấy bóng nên buổi tối hơi khó đọc một chút.
4
140131
2013-08-12 21:40:14
--------------------------
63853
10035
Cuốn sách này bàn về một chủ đề rất nóng hổi trong xã hội đương đại, đó là cách làm giàu cho bản thân. Chủ đề chắc hẳn mọi người đều quan tâm, chứ không riêng gì những bạn trẻ. Chính vì thế mà nó rất phù hợp cho mọi lứa tuổi. Cuốn sách cũng có vài luận điểm khá thú vị và độc đáo, nhưng điểm đáng tiếc là cách triển khai vấn đề không được tốt. Tràn ngập từ đầu đến cuối là những lý thuyết dài dòng, có gì đó hơi khô khan và cứng nhắc, khiến người đọc khó lòng mà tiếp thu một cách dễ dàng. Thêm vào đó là một vài chỗ còn có những thông tin bị thừa thãi, không cần thiết để đưa vào sách.
Điểm mình thích ở cuốn sách là cách trình bày khá đẹp mắt.
Dù sao đây cũng là một cuốn đáng đọc, nếu bạn quan tâm đến vấn đề làm giàu này.
3
20073
2013-03-17 15:02:22
--------------------------
36749
10035
Sau khi đọc những cuốn sách thế này, thường là sẽ không có tác dụng với bạn nhiều lắm. Tôi nghĩ những cuốn đưa ra lời khuyên chung chung thì chỉ là bảng lý thuyết không hơn không kém. Cuốn sách thành công là khi đọc xong bạn vứt ngay nó xuống, đứng lên hừng hừng khí thế để bắt tay vào thay đổi thứ gì đó trong cuộc sống của bạn
3
23874
2012-08-15 09:15:37
--------------------------
35918
10035
Những vấn đề nêu ra trong sách là là bình thường, chẳng có gì gọi là mới mẻ với tôi. Vì một số vấn đề trong sách này chẳng cần đến người viết sách chỉ bảo thì tự bản thân người ta cũng đã nhận biết rồi. Có đôi lúc tôi chỉ đọc nó để cho hết đồng tiền mình bỏ ra mua chứ thật sự chẳng hứng thú gì mấy. Có nhiều quyển sách cũng nói về những vấn đề "hit" này. Nhưng nội dung bên trong và cả cách trình bày thì thực sự tinh tế, là có khoa học hơn nhiều. Với lại một điểm trừ nữa cho sách là hình ảnh minh họa bên trong quá cứng nhắc làm cho người đọc đôi lúc lại thấy khó chịu, thà không có minh họa thì tốt hơn, có thể còn tiết kiệm tiền in màu cho người đọc nữa chứ. Nói chungg là tôi chẳng thấy sách ấn tượng tí nào. Tác giả cần trau dồi nhiều hơn!
2
48819
2012-08-04 17:00:45
--------------------------
34289
10035
Cuốn sách này có những "chiêu" dạy kiếm tiền rất hay, nhưng mình thực sự không thích ở những điểm này:
 -Quá thực dụng, cứ như là kiếm tiền một cách mù quáng vậy; mình nghĩ nên rèn luyện lòng kiên nhẫn để thành công thì hơn
 -Cuốn sách mình nghĩ là dành cho lứa tuổi bước vào đời nhưng lại giải thích quá cặn kẽ (lớn rồi mà!) và hình ảnh thì quá ư là con nít.
 -Một số điều có thể nói là "ai mà chẳng biết" như: không nên mua những thứ bạn không thể trả, hay tránh xa những người có thể làm hại bạn,...
 Thuộc thể loại sách giáo dục cho các bạn trẻ mới bước vào đời còn nhiều tác phẩm hay hơn, hiệu quả hơn cuốn sách này rất nhiều. Những ai định mua nên suy nghĩ kĩ 1 chút.
2
43460
2012-07-20 11:37:42
--------------------------
33404
10035
Có lẽ, bạn không cần tất cả những gì được viết trong cuốn sách này nhưng bạn vẫn cần một lời gợi ý, một ý tưởng để bắt đầu sự nghiệp của mình. Ở đâu đó trong cuốn sách này, bạn sẽ tìm thấy động lực thôi thúc bạn hành động. Đồng tiền là vô cùng cần thiết cho cuộc sống, nhưng có một số người cho rằng sự giàu có đến từ tình bạn bền vững, những mối quan hệ gia đình tốt đẹp, sự đồng cảm của các đồng nghiệp, sự hoà hợp về nội quan mang đến cho tâm trí bạn cảm giác yên bình và nó được đo bằng giá trị tinh thần! Bạn đọc sẽ tìm ra bí kíp cho riêng mình và chuẩn bị cho mình những thứ cần thiết để tích luỹ giàu ! Chân thành cảm ơn các tác giả!
3
40293
2012-07-13 13:11:35
--------------------------
31587
10035
Tôi đã đọc một vài cuốn sách hay theo kiểu giáo dục phát triển tu duy như thế này vì vậy nên tôi cảm thấy cuốn sách này chưa thật sự làm tôi thỏa mãn. Nó chưa phân tích kỹ và chỉ ra những phương hướng rõ ràng hơn cho người đọc. Đâu phải cách đơn giản trong đó là "ít xem tivi" xem tivi mà kênh nước ngoài để trau dồi tiếng anh cũng khá được đó chứ? Tôi thấy cuốn sách này viết dựa trên những kinh nghiệm rút ra về tư duy làm giàu từ những cuốn khác nên nó làm cuốn sách này người đọc cảm giác không có dấu ấn riêng để có thể so sánh chọn lựa với những cuốn sách " best sell" khác. Tôi nghĩ tác giả cần có một sự trải nghiệm phong phú và thiết thực hơn, lời khuyên từ nhiều người mới tạo ra được sự riêng biệt cho cuốn sách. Có thể là ý kiến của riêng tôi, nhưng nếu bạn muốn mua nó khi bạn chưa đọc cuốn sách nào về giáo dục tu duy như thế này, thì đây cũng là một lựa chọn của bạn. 
1
44109
2012-06-27 13:36:26
--------------------------
31455
10035
Cuốn sách này có những "chiêu" dạy kiếm tiền rất hay, nhưng mình thực sự không thích ở những điểm này:
-Quá thực dụng, cứ như là kiếm tiền một cách mù quáng vậy; mình nghĩ nên rèn luyện lòng kiên nhẫn để thành công thì hơn
-Cuốn sách mình nghĩ là dành cho lứa tuổi bước vào đời nhưng lại giải thích quá cặn kẽ (lớn rồi mà!) và hình ảnh thì quá ư là con nít.
-Một số điều có thể nói là "ai mà chẳng biết" như: không nên mua những thứ bạn không thể trả, hay tránh xa những người có thể làm hại bạn,...
Thuộc thể loại sách giáo dục cho các bạn trẻ mới bước vào đời còn nhiều tác phẩm hay hơn, hiệu quả hơn cuốn sách này rất nhiều. Những ai định mua nên suy nghĩ kĩ 1 chút.
2
23953
2012-06-26 10:53:51
--------------------------
