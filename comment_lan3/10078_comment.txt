479768
10078
Mình là một fan bự của nhóc miko,mình thấy Miko thực sự rất dễ thương,đáng yêu và ngoài ra còn rất cá tính nữa.Mình thích nhất là Tappei với Miko hai người đó dễ thương quá chừng,ngại ngùng đúng với tuổi học trò thơ ngây trong sáng.Trong tập này mình thích nhất là chương "Ai là ai?",ở đây đã có một sự hoán đổi giữa anh chàng Yoshida và Tappei.Lúc cúp điện linh hồn của Tappei vẫn ở trong thân xác Yoshida và cậu ấy đã nắm tay Miko cho cô ấy đỡ sợ nhưng cô ấy vẫn nhận ra sự ấm áp từ trong tâm hồn của Tappei và gọi tên cậu ấy.Mình thấy rất tuyệt và càng ngày càng thích nhóc Miko hơn nữa.Cám ơn tiki đã đưa đến cho chúng mình một sản phẩm tốt như thế này.
5
851679
2016-08-12 09:53:32
--------------------------
474161
10078
Phần đầu tập này là mẩu truyện về Yoshida, cậu bé này không phải là nhân vật nổi bật trong truyện, mình không quá ấn tượng về nhân vật này nbu7ng khi đọc xong lại có cách nhìn khác hơn, và cũng thấy yêu thích Yoshida hơn. Cậu bé bị mẹ bắt phải đi học thêm từ sáng tới chiều ngay cả vào những ngày mà bạn bè đều được nghỉ, cậu bé cuối cùng quyết định trốn học để đi đến nhà Mari cùng Miko. 3 người vẽ tranh, ăn uống và chơi đùa cùng nhau, điều đó giúp 3 người trở nên thân thiết hơn so với những tập trước
5
80823
2016-07-11 02:34:23
--------------------------
457765
10078
Cuốn " Nhóc miko : cô bé nhí nhảnh - tập 15" chắc là cuốn duy nhất trong series Nhóc Miko có phông màu trắng mọi người nhỉ? Cảm giác khá là mới mẻ đó chứ.Trong tập 15 mình thích phần " tình bạn ba người" vì bản thân cũng hay rơi vào những vấn đề khá là í e giống 3 cô bạn Miko,Mari,Yuko nên đọc thấy đồng cảm ghê chứ.Sau là " Ghét học lắm",cô bé miko không biết nên dùng từ ngây thơ vô số tội hay bờm nữa,ghét học thế là trực tiếp " cúp" học luôn.Siêu quá chừng,trông vậy mà ghê thiệt !
5
1419914
2016-06-24 15:53:57
--------------------------
441185
10078
Mình thích tập này lắm đó . Mình Thích nhất là truyện Ai là ai ? Vì tập này tapper với yoshida va vào Nhau rồi tự nhiên bị đổi hồn à , ( truyện miko vừa vui vừa hay , ngây thơ lại có chút huyền bí đó ) tapper thì thành yoshida  , còn yoshida thì biến thành tapper . Nhưng miko vẫn nhận ra vì tính tình của hai người đó trái ngược mà ( mình kể đến đây thôi , các bạn muốn biết đoạn sau thì các bạn mua trên tiki nha , tiki vừa rẻ sách lại xịn nữa chớ ) . Ngoài ra còn cả một trang tranh cho các bạn tô màu thỏa thích đó . 
5
280483
2016-06-02 21:41:40
--------------------------
409544
10078
Miko luôn là một cô gái đáng yêu và dễ thương trong mắt bạn đọc . Trong tập chuyện này chúng ta biết đến nỗi buồn của Yoshida - một cậu bạn cùng lớp của Miko . Do bị mẹ ép buộc học quá nhiều nên Yoshida đã trốn học để đi chơi cùng với Tappei , Kenta , Mari cùng Miko khiến mẹ cậu ấy lo lắng . Nhưng qa sự việc lần này mẹ cậu ấy đã nhận ra được sai lầm và không ép cậu ấy học quá mức . Bên cạnh đó trong buổi cắm trại của lớp Tappei đã bắt gặp Kenta và Yuko hôn nha. Mặc dù biết họ yêu nhau nhưng mọi người không nghĩ tới mức phát triển như vậy . Nhưng dù sao cuốn truyện này vẫn gây cười cho người đọc
5
671301
2016-04-02 19:22:22
--------------------------
294654
10078
Nhóc Miko là một trong những truyện tranh mà mình yêu thích nhất. Miko được tác giả xây dựng rất hồn nhiên, vui tươi mọi thứ, từ biểu cảm, ngoại hình, tích cách đến cuộc sống. Mỗi khi đọc Miko, mình cảm thấy rất thoải mái và yêu đời. Trong tập này, có lẽ đối với mình, nổi bật hơn cả là tình cảm giữa Miko với bạn bè. Ước gì mình cũng có những người bạn thân như Miko có Yuko và Mari. Còn giữa Tappei và Miko thì cực kì dễ thương luôn, giống như thích nhau rồi mà chưa nhận ra ấy. Mình thấy truyện Miko rất dễ thương, vui tươi và hồn nhiên. Cám ơn Tiki đã giao hàng siêu nhanh cho mình.
5
247850
2015-09-09 15:53:13
--------------------------
292812
10078
Miko thực sự là 1 cô bé không thể không yêu, dễ thương không tả nổi. Dù bất cứ lúc nào cũng có thể cười tươi và tăng động được. Rất tràn đầy sức sống. Nội dung câu chuyện ở tập này có phần thêm 1 nhân vật mới, đó là cậu bé Shoma, đây gần giống như 1 phiên bản trẻ con hơn của Tappei vậy, chả trách cu cậu cứ bực tức khi Miko vui vẻ với Shoma. Rất dễ thương. Tập này rất thú vị và hấp dẫn cho những ai yêu mến cặp đôi Miko và Tappei nhé. Cùng nhâm nhi nào :)
5
281150
2015-09-07 17:25:27
--------------------------
289860
10078
Những ai đã theo dõi bộ truyện Nhóc Miko đến tận tập 15 này, chắc chắn cũng sẽ đều cảm thấy sự dễ thương đáng yêu vô đối của cô nhóc. Miko tuy rất ham ăn ham ngủ, đôi khi còn tị nạnh với em trai nữa, nhưng cô bé thực sự tốt bụng vô cùng. Thấy Yosida phải học hành vất vả, cô bé cũng cố gắng tìm cách để giúp Yosida được vui vẻ, thoải mái hơn. Ở tập này, chương "Tình bạn ba người" cũng rất hay. Dù là người lớn nhưng thực sự mình thấy rất ngưỡng mộ tình bạn bền chặt của 3 cô bé, đôi lúc chỉ cãi nhau chút xíu thôi rồi lại làm lành liền. Mong rằng truyện Nhóc Miko cứ hay mãi như thế này nhé.
5
178929
2015-09-04 22:19:23
--------------------------
288961
10078
Cô bé nhí nhảnh khai thác nhiều về vấn đề tình bạn thông qua những câu chuyện hết sức đáng yêu và ngộ nghĩnh. Trong tập 15 này mình rất thích chương "Hộp lưu giữ kỉ niệm", chỉ tiếc là có cơ hội bày tỏ rành rành như vậy mà Miiko và Tappei cũng không dám nói, nhưng cũng nhờ thế mà câu truyện thêm phần trong sáng, khiến người đọc cũng cảm thấy vui lây khi thấy bọn nhóc con đỏ mặt, băn khoăn trong cái tình cảm ngây thơ vẫn chưa thể gọi tên này. Trẻ con là thế, ngây thơ và hồn nhiên, ngay cả ghen cũng ghen một cách dễ thương và khiến người đọc bật cười vì độ trẻ con không kiểm soát nổi. Có lẽ chính vì những chương truyện rung động nhẹ nhàng như vậy nên mới khiến người đọc đọc một lần rồi vô tình bị thu hút lúc nào không hay.
5
364412
2015-09-03 23:59:24
--------------------------
213960
10078
Đã đọc nhiều truyện "Nhóc Miko- cô bé nhí nhảnh" và thậm chí là đọc tập 15 nhiều lần, đọc đi đọc lại mà không thấy chán. Rất cuốn hút. Tập 15 này chủ yếu viết về đề tài tình bạn. Với những nét vẽ siêu dễ thương, đề tài này càng trở nên hấp dẫn. Trong tình bạn có vô vàn sắc thái, có thể giận nhau đấy nhưng lại làm lành ngay. "Tình bạn ba người'' đề cập trong truyện cũng là một vấn đề gây đau đầu nhưng họ cũng đã vượt qua, tất cả đều nhờ tình yêu thương và giúp đỡ lẫn nhau của tình bạn. Mặt khác mình cũng thích "chuyện bên lề" đó là những câu chuyện chỉ gồm một trang nhưng bao quát nên quá trình hình thành ý tưởng của tác giả, hay chỉ là những câu chuyện đời thường của cô. Hay lắm
5
299614
2015-06-24 13:09:14
--------------------------
125543
10078
Đây là tập Miko đầu tiên tôi đọc đã dẫn tôi mê mệt bộ truyện này. Những tưởng nó sẽ giống Nhóc Maruko hay Asari, nhưng thật sự Miko có một nét riêng làm tôi thích hơn hẳn những bộ truyện khác như kiểu này. Nét vẽ đẹp và đáng yêu, những mẩu truyện gần gũi, buồn cười và cảm động hơn, đặc biệt là truyện tình củm giữa Miko và Tappei. Tôi đặc biệt thích mẩu truyện Buổi cắm trại, lãng mạn, tình củm vừa đủ và đặc biệt là làm tôi cười đau cả bụng với sự hồn nhiên của Miko. Tappei cũng rất tuyệt nữa. Cũng thật ghen tụ với Miko vì có một người cha tâm lý, còn mẹ Miko thì tính cách y hệt mẹ tôi nên cũng thông cảm cho Miko chuyện học hành lắm lắm :)). Tập này tôi đã đọc đi đọc lại nhiều lần mà không chán, một chuyện hiếm khi đối với truyện tranh. Cũng mong nhóc Miko có thể được xuất bản dài dài quá...
5
68745
2014-09-13 23:02:01
--------------------------
