54407
10186
Đọc cuốn sách nhỏ này, với mình, giống như được đi trên một chuyến du hành của thời gian, đến với những khoảng lặng bất chợt mà diệu kỳ của cuộc sống. Tác giả đã viết nên tác phẩm bằng những con chữ vô cùng chân thành, giản dị, nhưng vẫn độc đáo và mới lạ. Người đọc hẳn sẽ nhận thấy những xúc cảm sâu xa được gửi gắm nơi mỗi trang sách, có chút niềm vui nho nhỏ, chút hạnh phúc, chút buồn và tiếc nuối, và đây đó là sự lưỡng lự giữa quá khứ và hiện tại, giữa thực và hư. Cuốn sách đem đến những giây phút lắng lòng, như một cơn mưa thu dịu mát thổi đi những muộn phiền, đau khổ, tất cả chỉ bằng những con chữ thật dung dị và tinh tế.
4
20073
2013-01-09 09:51:41
--------------------------
