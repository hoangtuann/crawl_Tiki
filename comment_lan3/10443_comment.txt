333734
10443
Có thể nói đây là cuốn sách đã mang đến cho tôi rất rất nhiều sự ngưỡng mộ, sự khâm phục trước sự kiên cường, đầy nghị lực và tinh thần bất diệt của Steve Jobs. Quyển sách chỉ là những mốc cuộc đời quan trọng của Steve cũng như những biến động được mô tả ngắn gọn nhưng làm rõ được "Sự khác biệt" và thông qua đó con người ta cũng sẽ vô tình được đốt cháy lên ngọn lửa khát vọng được cố gắng được nổ lực. Và Steve tuy không phải là con người hoàn hảo nhưng ông đã trở thành một biểu tượng một thần tượng cho nhiều thế hệ mai sau, kể cả tôi ... 
5
71579
2015-11-08 11:20:20
--------------------------
285694
10443
Trước hết cám ơn First News lại cho tôi một quyển sách hay. Cám ơn GSTS. Huỳnh Ngọc Phiên đã chủ biên rất đầy đủ về Steve Jobs: hình ảnh, dẫn chứng, tư liệu... Tôi đã đọc nhiều sách về Steve Jobs, nhưng có lẻ do sách dịch, hay hành văn không phù hợp nên đọc rất dễ chán. Nhưng khi đọc quyển này tôi thấy rất dễ đọc, thu hút, và dễ nắm bắt. Có lẻ do tác giả là nhà khoa học và còn làm công tác giảng dạy, nên đã tạo nên quyển sách khác biệt từ nội dung đến hình thức trình bày.
5
115038
2015-09-01 07:44:26
--------------------------
254671
10443
Cuốn sách đã giúp tôi hiểu thêm nhiều hơn về cuộc đời của Steve Jobs, một con người đã góp phần thay đổi thế giới, đó cũng chính là mục tiêu của cuộc đời ông. Một con người dám nghĩ dám làm, dám thực hiện những điều mà người khác cho là điên rồ, luôn đòi hỏi những điều hoàn hảo nhất. Cuốn sách đã giúp tôi thấy được con đường sự nghiệp và tính cách của ông, truyền cảm hứng cho bản thân tôi cũng như nhiều bạn trẻ khác, dám khác biệt để thành công. Càm ơn tiki đã mang đến cuốn sách tuyện vời này. 
4
251335
2015-08-05 14:02:10
--------------------------
250958
10443
Tôi mua cuốn sách này vì từ lâu đã ngưỡm mộ Steve Jobs- người sáng lập ra Apple.Ấn tượng ban đầu của tôi là bìa sách được thiết kế giống như một chiếc ipad, có nút bấm và phím trượt để unlock, rất thú vị. Khi mở bên trong, tôi cũng rất hài lòng bởi chất lượng giấy, vàng và cứng rất tốt.Nội dung cuốn sách đem đến cho tôi nhiều cảm nhận và góc nhìn mới mẻ. Steve luôn có những ý nghĩ trong đầu khác hẳn người thường ,đó là những ý nghĩ táo bạo ,cũng  khá nguy hiểm ,dù ông bị đuổi việc tại chính công ty mình gây dựng nhưng ông vẫn theo đuổi ước mơ vì niềm đam mê vô hạn với công nghệ và mong muốn thay đổi thế giới tốt đẹp hơn.Cuốn sách là một thông điệp về sự sáng tạo và say mê nghề nghiệp cho mỗi bạn đọc trẻ. 
4
501195
2015-08-02 11:07:17
--------------------------
156090
10443
Đọc đi đọc lại cả chục lần nhưng vẫn cảm thấy hay và thích thú. Quả thật cuốn sách này đã truyền cảm hứng cho mình về Steve Jobs rất nhiều, nhưng tôi mong chờ hơn rằng tôi sẽ có thể hiểu vị thiên tài của quả táo cắn dở hơn nữa, vì cuốn sách này chỉ chú trọng phần nào về tính lịch sử của Jobs hơn. Tôi thật sự thích cách Jobs marketing sản phẩm của mình cũng như cái cách ông táo bạo, là một trong những người đầu kỷ nguyên công nghệ kỹ thuật tiến tiến, đặc biệt là smartphone. Dù gì cũng cảm ơn Tiki vì đã mang cuốn sách này đến bên mình
5
283944
2015-02-03 08:50:07
--------------------------
122974
10443
Tôi mua cuốn sách vài ngày sau khi Steve qua đời. Tôi đọc nó như một cách để tưởng niệm ông. Một phần nào đó ông đã góp phần thay đổi thế giới, thay đổi nhận thức của con người về thế giới. Mỗi sản phẩm ông làm đều có sự trau chuốt đến hoàn hảo từng chi tiết nhỏ (một người bạn của tôi đã nói như vậy). Khi đọc cuốn sách này, tôi càng hiểu rõ hơn cách làm việc của ông và cách ông mong muốn đối với những đứa con tinh thần mà ông sinh ra. Có nhiều điều tôi học được khi đọc về ông trong cuốn sách này, như mỗi việc làm dù nhỏ cũng cần có sự chỉn chu và toàn tâm toàn ý, như hãy dại khờ và khát khao để sống có ích mỗi ngày. :)
5
329565
2014-08-29 10:30:52
--------------------------
110099
10443
Steve Jobs - Sức Mạnh Của Sự Khác Biệt (The Power of Think Different) cung cấp cho đọc giả rất nhiều thông tin về cuộc đời cũng như tác phong trong công việc Steve Jobs.  Ý chí mạnh mẽ, tính cách độc lập, quyết đoán, tỉ mĩ và nghiêm tíc trong công việc, nỗ lực và luôn sống hết mình là những gì  tôi đã học được từ ông. Song , có lẽ vì "Steve Jobs - Sức Mạnh Của Sự Khác Biệt (The Power of Think Different"  không phải do chính tác giả chấp bút cũng không phải là thành quả của một công trình nghiên cứu về Steve Jobs nên vẫn chưa thể giúp độc giả hiểu rõ hơn về suy nghĩ và nội tâm của Steve Jobs. Dù vậy thì đây vẫn là một quyển sách hay nên đọc.
3
137369
2014-04-12 01:45:31
--------------------------
51169
10443
 Đọc cuốn sách này ta thấy được sức mạnh sáng tạo phi thường của "thầy phù thủy" Steve Jobs để biến Apple trở thành hãng công nghệ hàng đầu thế giới xuất phát từ những sự khác biệt. ông có những ý nghĩ trong đầu khác hẳn người thường ,đó là những ý nghĩ táo bạo ,nhưng khá nguy hiểm ,dù là những lần ông bị đuổi việc tại chính công ty mình gây dựng nhưng ông vẫn theo đuổi ước mơ , không chỉ ở cách chọn nhân viên làm việc mà ở cả những hành động suy nghĩ của ông cũng rất đặc biệt ,khác người , có thể gọi là trái ngược so với những nhà kinh doanh khác ,nhưng nhiều lúc đọc cuốn sách này tôi bị cụt hứng bởi những từ ngữ tiếng anh và những từ rất là máy móc về công nghệ khá phưc tạp mà tôi chả hiểu về máy móc cho lắm ,hi vọng lần tái bản tới sẽ có phần chú thích cho các cụm từ về công nghệ ,nếu thế thì cuốn sách sẽ rất tuyệt
4
62152
2012-12-18 20:58:38
--------------------------
48615
10443
Mình rất bất ngờ khi ở Việt Nam lại có một nhóm tác giả viết hay và lôi cuốn như thế về một danh nhân nổi tiếng thế giới. Những thông tin, những hình ảnh về cuộc đời và con người Steve Jobs có lẽ đã quá quen thuộc với giới yêu công nghệ. Nhưng cuốn sách này đem đến nhiều cảm nhận và góc nhìn mới mẻ. Chân dung của Steve Jobs được khắc họa rất đơn giản, dung dị và không cầu kỳ. Giống như một người bình thường nhưng có nhưng ước mơ và khát vọng lớn, ông đã tạo nên những điều kỳ diệu cho thế giới, thay đổi nền công nghệ toàn cầu và đạt được những ước mơ của mình. Cuốn sách còn gửi gắm một thông điệp về sự sáng tạo và say mê nghề nghiệp cho mỗi bạn đọc trẻ.
Bìa sách được thiết kế giống chiếc Ipad cũng là một ý tưởng độc đáo, góp thêm vào thành công của cuốn sách này.
4
20073
2012-12-02 12:21:12
--------------------------
28582
10443
Khi mới cầm cuốn sách này trong tay, tôi rất ấn tượng bởi bìa của nó. Bìa sách được thiết kế giống như một chiếc ipad, có nút bấm và phím trượt để unlock, rất thú vị. Khi mở bên trong, tôi cũng rất hài lòng bởi chất lượng giấy, vàng và cứng. Ngoài ra trong sách có rất nhiều hình ảnh đẹp của Steve Jobs thời trẻ, những trang giấy màu có hình của Steve qua từng mốc quan trọng trong cuộc đời ông. Nhưng thú vị nhất có lẽ là những hình ảnh biếm họa của ông, những lời nhận xét của những người nổi tiếng khác về Steve (đặc biệt là Bill Gate)
Về mặt nội dung thì tôi thấy tạm ổn. Dù cuốn sách đã miêu tả rất chi tiết cuộc đời của Steve Jobs và tạo cho người đọc cảm giác nể phục ông, lời văn của nó vẫn còn khô khan, nhất là khi nói về công nghệ, khiến người đọc sẽ thấy khó hiểu.
Tuy nhiên, đây là một cuốn sách rất đáng để đọc và khơi gợi cho ta nhiều nguồn cảm hứng trong cuộc sống.
4
38411
2012-05-29 09:27:23
--------------------------
