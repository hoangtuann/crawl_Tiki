230531
10182
Mình rất thích sách của cô Bùi Thị Sương vì cô là một người đầu bếp nhiều kinh nghiệm và rất tinh tế trong từng cách chế biến món ăn. Khi mua sách nấu ăn, mình quan trọng cả về nội dung và hình thức trình bày hình ảnh nữa, và quyển sách này đã không làm mình thất vọng. Món cuốn có lẽ là món khá đơn giản để chế biến so với những món nước phức tạp khác, nó là sự hòa quyện giữa nhiều loại mùi vị, màu sắc khác nhau và tùy theo khẩu vị của từng người bạn có thể thõa sức sáng tạo để  tạo nên một món ăn hài hòa và rất đậm tính chất Việt Nam. Tuy nhiên sẽ không thừa để bạn đầu tư thêm quyển sách này trong tủ sách nấu ăn của bạn khi mà quyển sách giới thiệu thêm rất nhiều về những bí quyết, những nguyên vật liệu để không những món ăn vừa ngon mà còn vừa đẹp mắt nữa! 
4
351847
2015-07-17 15:29:17
--------------------------
