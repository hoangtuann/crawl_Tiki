286974
10815
Nội dung tập 2 đọc gay cấn, hồi hộp hơn tập 1. tác giả có nhiều lý giải thú vị và mới mẻ về pháp thuật, đưa ra nhiều khái niệm, quan điểm độc đáo, tạo được nét riêng cho tác phẩm, giúp nó không bị chìm nghỉm giữa rừng truyện về đề tài pháp thuật quen thuộc. Sự trưởng thành, tiến bộ của cậu bé mồ côi Conn khi đi theo phù thủy Netery khiến tôi đọc không rời mắt được. Sách được bọc nilon kín nên vẫn mới toanh, không trầy xước gì, thật tuyệt. Bìa sách cũng cùng một tông màu tối huyền bí với tập 1, rất đẹp.
4
57459
2015-09-02 09:37:19
--------------------------
252054
10815
Truyện đẹp cả về hành thức lẫn nội dung ! Mình đã k thể nào rời mắt khỏi cuốn sách cho đến khi mẹ gọi mình đy ăn cơm ^ ^ ! Mình rất thích những cuốn sách về pháp thuật , và cuốn này cũng không phải ngoại lệ . Hơn nữa Tiki còn có chính sách giao hàng rất nhanh chóng và trên cả tuyệt vời . Sách được gói cẩn thận , tỉ mỉ nên không hề bị trầy xước , cứ như là mình đến nơi sản xuất mang nó về nhà ý ! Mình yêu Tiki !
5
664451
2015-08-03 13:00:42
--------------------------
5561
10815
Nếu nhìn lướt qua trang bìa, có lẽ cũng đủ để hấp dẫn ai đó. Nhưng nếu bạn đã xem phần một của cuốn sách với đề tựa kèm theo: “Đoạt lấy ma thuật rồi tẩu thoát” thì bạn sẽ tò mò mà đọc ngấu nghiến từ trang này đến trang khác. Và chính tôi cũng đã từng phải “chịu đựng” cái cảm giác ngấu nghiến ấy. Với cuốn 2 này, những li kì, hồi hộp lẫn bất ngờ của cuốn 1 không những được nâng cao mà có phần còn sắc sảo hơn. Ở đây, bạn có thể tìm ra những khái niệm táo bạo, độc đáo về những thế giới khác, tưởng gần gũi hơn mà xa lạ hơn, mầu nhiệm mà tuyệt diệu, hoàn toàn khác với suy nghĩ của nhiều nhà văn đương thời. “Pháp thuật có sự sống?”_tôi đã không nghĩ được điều này cho đến khi được đọc cuốn sách “cậu bé trộm ma thuật”. Khẳng định, bạn sẽ thích thú mà nhâm nhi từng chữ, tững chữ và ham muốn có trọn bộ 2 cuốn sách này. Đồng thời, cuốn sách có dạy về bảng chữ cổ tự rune(nếu bạn ham mê harry potter bạn sẽ biết loại chữ này) và một vài công thức nấu ăn, thực ra, tuy truyện hơi hoang tưởng, nhưng những kiến thức nó cung cấp thêm này lại có đôi chút hữu ích. Nếu bạn không tin tôi, hãy đọc thử rồi sẽ biết thôi…
5
5893
2011-06-03 13:15:37
--------------------------
