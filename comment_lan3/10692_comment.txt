408784
10692
Truyện cổ Andecxen từ lâu đã ghi dấu ấn với các thế hệ trẻ em. Những câu chuyện thú vị, hấp dẫn giúp rèn luyện trí tưởng tượng cũng như nhân cách của các em nhỏ. Từ những câu chuyện này mà biết bao em nhỏ đã có những giấc mơ đẹp, những bài học hay. 
Bìa sách là bìa cứng rất đẹp, chắc chắn, giấy in trắng, chất lượng tốt. Tôi nghĩ đây là quyển sách nên có trong tủ sách gia đình. Bé nhà mình rất thích các câu chuyện trong sách, cứ bắt mẹ đọc đi đọc lại vào mỗi tối trước khi đi ngủ. 
4
198939
2016-04-01 11:19:14
--------------------------
82722
10692
Truyện cổ andecxen là cuốn sách bổ ích giành cho lứa tuổi thiếu nhi trên khắp thế giới. Những câu chuyện tưởng như hoang đường trong cuốn sách lại là những bài học vô cùng quý báu cho trẻ thơ. Có thể thấy, đằng sau những câu chuyện là những triết lí sâu sắc về cuộc sống, về cách học làm người. Thông qua những câu chuyện này mà các em rút ra được vốn kinh nghiệm sống quý báu cho mình để từ đó dần trưởng thành hơn trong cuộc sống. Trong thời đại ngày nay, cùng với sự bùng nổ của công nghệ thông tin thì chắc hẳn ít nhiều trẻ thơ không còn mặn mà với những câu chuyện cổ này nữa. Tuy nhiên, sẽ là một thiếu sót rất lớn cho bất cứ ai chưa đọc những câu chuyện này vì nó là những bài học vô cùng bổ ích đấy. Nó sẽ là hành trang sống theo ta trong những chặng đường của cuộc sống đầy khó khăn, thử thách. Thử một lần đọc và cảm nhận nhé!
5
72874
2013-06-22 20:13:55
--------------------------
