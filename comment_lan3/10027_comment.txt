495546
10027
Một quyển sách tuyệt vời về cả nội dung lẫn hình thức. Sách sắp xếp câu hỏi theo mức độ nên mình rất vừa ý, tuy có nhiều câu khó làm mình tốn khá nhiều thời gian nhưng thực sự thời gian bỏ ra là vô cùng bổ ích. Về hình thức thì cuốn này bản to, đẹp, trang giấy dày, nói chung rất vừa ý. Cho 10 sao nếu có thể.
5
1894986
2016-12-13 12:52:12
--------------------------
488634
10027
Hóa ra quyển sách này là của NXB Đông Á, bảo sao chất lượng tốt thế.Mình mua cuốn này lâu rồi, cơ mà bây h mới để ý nó là của Đông Á. Vote cho Đông Á, rất có tâm :)
5
1091550
2016-11-04 00:35:44
--------------------------
483641
10027
Cuốn 300 câu đố tư duy của sinh viên Havard thực sự rất hay. Mình đã đi lùng rất nhiều sách liên quan đến tư duy, các câu IQ nhưng nhiều sách viết câu hỏi rất dài khiến người đọc, đọc xong không muốn động não nữa. Sách này hay, câu hỏi từ dễ đến khó,có đáp án ở sau, hệ thống câu hỏi phong phú. Sách in màu, giấy chất lượng tốt. Nói chung mình rất ưng ! Cảm ơn Tiki đã mang đến cuốn sách bổ ích này, mà giá còn phải chăng nữa chứ ! Sẽ ủng hộ tiki dài dài !
5
696970
2016-09-16 00:10:36
--------------------------
482259
10027
Mình mua sách về đọc được một thời gian. Về ưu điểm ban đầu Tiki đóng gói sách rất đẹp và cẩn thận mình rất hài lòng về bìa sách, những câu đố có trong sách có khối lượng kiến thức rất cao và rộng, bắt buộc chúng ta phải đầu tư thời gian để suy nghĩ cho ra.Sách phù hợp với các bạn học sinh sinh viên hoặc những người thích trải nhiệm những câu đó hóc búa, nâng cáo kiến thức cũng như khả năng suy luận logic và giúp khả năng phản ứng trong mọi tình huống.
5
855203
2016-09-04 22:37:42
--------------------------
473263
10027
Sách được in màu rất đẹp, giấy tốt nên dễ bảo quản. Các câu đố được sắp xếp từ dễ đến khó để người đọc dễ dàng làm quen với các câu hỏi trong sách. Nội dung các câu đố thì rất đa dạng, từ số đến hình đến chữ rồi đến ký hiệu. Ở những câu đầu bạn sẽ vượt qua hết sức dễ dàng nhưng ở độ khó trung bình thì bạn sẽ bắt đầu mệt mỏi đó. Hãy tạm cất những cuốn sách về IQ trong tủ sách của bạn đi đây mới là cuốn sách bạn cần.
5
14119
2016-07-10 13:34:49
--------------------------
472357
10027
300 Câu Đố Tư Duy Của Sinh Viên Trường Đại Học Harvard là cuốn sách rất tuyệt về nội dung và cả về hình thức. Chất lượng sách khá tốt, hình vẽ, màu sắc thì không cần bàn, rất đẹp và rõ ràng. Nội dung rất chuẩn, không mông lung, vô thẳng ngay các câu hỏi, có các câu rất dễ nhưng các câu khó dù xem đáp án vẫn  không hiểu, phải mò mẫm cả ngày mới hiểu. Nói chung sách rất kích thích tư duy, bạn phải động não nhiều và kiên trì mới giải được các câu hỏi khó. 
5
653217
2016-07-09 15:06:58
--------------------------
471851
10027
Lúc bạn rảnh rỗi hãy mang cuốn này ra làm :))
Những câu đố đôi khi khá hại não và hại mắt nhưng cũng làm não hoạt động.
Hè nghỉ mình hay lôi sách này ra làm, hoàn toàn ngẫu hứng, vì hè não mình ngủ đông ==
Cũng không chắc sinh viên harvard có làm mấy bài thế này không :3 Nhưng cũng khá thú vị đấy ạ.
Mẫu mã cuốn này miễn chê ạ, cùng với nội dung thì giá cả khá phù hợp.
Cái giải chỉ có đáp án, không nêu quy luật, nên với những câu không làm được ngồi ngu ngơ thật sự rất buồn.
5
513917
2016-07-08 22:47:05
--------------------------
468776
10027
Về hình thức mình thấy hình ảnh rất bắt mắt, nhiều màu sắc, duy chỉ bị một cái chưa ổn là có vài trang sách của mình bị rách, nhưng thôi cũng bỏ qua vì mua hàng tiki đã nên 1 lỗi nhỏ mình không để tâm làm gì, trừ đi 1 sao là được
về nội dung, sách có những câu đố khá giống trong mấy bài trắc nghiệm IQ. Có những câu thật sự rất hay, phải bỏ ra hàng giờ mới giải được. Đối với bạn nào IQ cao thì mình nghĩ chỉ vài chục phút. Mặc dù nó phân loại câu đố từ dễ đến khó nhưng có những câu bên phần khó các chỉ cần mất vài phút.
Sách dành cho bạn nào thích các loại câu đố hại não và có mong muốn nâng cao chỉ số thông minh 
4
899706
2016-07-05 14:58:40
--------------------------
460756
10027
Harvard là ước muốn của không ít người trong chúng ta, cái tên đó thật sự là 1 thương hiệu đầy quyến rũ, chỉ cần gắn trên bất cứ quyển sách nào cũng khiến chúng ta sẵn sàng bỏ ra 30s cuộc đời dừng lại đọc. Cuốn sách này cũng vậy, rất đáng để bỏ thời gian và đồng tiền. Không chỉ vô cùng vô cùng tư duy mà cuốn sách còn giúp bản thân mỗi người có kế hoạch, chiến lược rèn luyện tư duy cho bản thân hơn, không chỉ giúp việc học mà mọi việc trong cuộc sống hàng ngày đều cần đến chuyện này.
5
525433
2016-06-27 11:58:28
--------------------------
459219
10027
Tiki giao hàng nhanh hơn nhiều so với dự kiến, sách được đóng gói cẩn thận. Bìa đẹp, chất liệu giấy tốt, nội dung rõ ràng, được chia thành nhiều phần. Các câu đố rất hay, giúp phát triển tư duy của con người. Có thể dùng để rèn tư duy nhanh nhẹn cho các bé từ bậc tiểu học cũng như là một trò chơi thú vị cho cả người lớn. Đáng để mua nhé mọi người. Mong rằng tiki sẽ có thêm những khuyến mãi như thế này để tôi sưu tập thêm được nhiều sách hơn nữa.
5
1386839
2016-06-25 21:47:51
--------------------------
427048
10027
Về hình thức:
- Sách in rất đẹp, bìa và giấy đều dày dặn màu sắc đầy đủ, rất kích thích thị giác và trí não trong quá trình giải đố.
- Những câu đố thực sự rất hay, có lần mình ngồi trên xe buýt giải đố mà quyên hết mọi thứ xung quanh, ngẩng lên còn giật mình không biết đang đi tới đâu.
- Mình mua cuốn sách để giải vì hay đi xe buýt, luôn muốn có sách mang theo để đọc. Và mua cuốn này trước khi thi học bổng và thực tập vào công ty kiểm toán lớn, họ có bài thi IQ nên mình cũng muốn rèn thêm. Chắc cũng nhờ cuốn này mà mình làm phần IQ trôi chảy hẳn, rất logic. Giờ mình đã nhận được học bổng và thực tập sinh ở công ty đó rồi.

Cảm ơn Tiki và Các tác giả cùng nhà xuất bản!
5
828175
2016-05-08 10:25:25
--------------------------
424435
10027
Hôm nọ bạn mình được chị của bạn ấy mua cho bạn ấy quyển sách này , xem sơ sơ qua thì mình thấy cũng rất hay nên quyết định mua cho em trai mình mà mình toàn dành nhưng mà mình vẫn chưa giải được câu nào , chắc tại ngố quá lại thêm không có thời gian 
về chất lượng sách mình thấy rất ok , giấy trắng cứng , không bị nhăn  
có cách trình bày rất rõ ràng và dễ hiểu, đem đến những bài học thiết thực cho mỗi bạn học sinh, sinh viên, thậm chí cho mọi lứa tuổi.
5
544062
2016-05-01 19:33:33
--------------------------
369424
10027
Sách này trên tiki bán rẻ hơn so với thị trường. Cuốn sách to, chiều dài và chiều rộng chênh lệch không đáng kể. Giấy tốt, màu sắc in khá hài hòa, đẹp mắt, dễ nhìn. Lật những trang đầu cứ ngỡ là chọn nhầm vì thấy giống sách dành cho trẻ con hơn nhưng lật ra những trang sau thấy rất thích thú, các câu đố đi từ mức độ dễ đến khó và phù hợp cho mọi lứa tuổi. Cuốn sách khá hay và rất tốt cho sự phát triển của não. Cố gắng giải hết các câu đố và rồi bạn sẽ cảm thấy mình cũng là 1 thành viên trong trường đh Harvard.
5
1091656
2016-01-15 18:59:33
--------------------------
364111
10027
Cuốn sách này thực sự hấp dẫn. Mặc dù mới làm được mấy chục bài ở chương dễ nhưng chúng tôi đã bị cuốn hút. Tôi mới mua cuốn sách này hôm qua, sách được giao tới công ty và cả nhóm chúng tôi chụm lại cùng giải các câu đố một cách thích thú. Tính là mỗi ngày làm 10 câu đều đặn nhưng máu quá, chúng tôi làm tới 2 chục câu. Không biết đến càng đến chương khó thì tôi có thấy mình càng ngày càng ngu đi không nhưng chắc chắn sự luyện tập thường xuyên sẽ refresh tích cực tới trí tuệ mỗi người.
Về hình thức cuốn sách được in màu chất lượng tốt, rõ ràng.
Nói chung là đáng mua :)
5
953121
2016-01-05 15:31:39
--------------------------
361409
10027
Cuốn sách này cung cấp những trò chơi với những cấp độ khác nhau từ dễ cho đến khó. Tuy những câu hỏi nhìn thoạt chừng rất dễ nhưng thật sự rất khó và cao thâm. Đúng là những câu đố của sinh viên trường harvard cần 1 tư duy vô cùng tốt. Từ những câu hỏi không gian logic số học... Đi kèm với cuốn này cũng hay không kém đó là cuốn 300 trò chơi tư duy của sinh viên trường đại học harvard. Hai cuốn này đi kèm với nhau hay vô cùng lun. Không biết khi nào mình mới giải hết 300 câu này nhỉ?
5
646680
2015-12-30 22:50:34
--------------------------
359903
10027
Mình mua cuốn sách này từ lâu rồi nhưng thật sự vẫn chưa giải được câu đố nào..:)))..Chắc do mình ngố quá..Bìa và giấy chất lượng tốt..Màu bên trong sách đẹp..Nói chung là miễn chê..CHỉ có điều nếu mua quyển này về thì hãy xác định chắc chắn mình là người kiên trì và đào sâu suy nghĩ..Vì những câu hỏi mức độ dễ cũng chẳng ''dễ dàng'' gì...Chứ chưa nói là mức độ trung bình hay khó....Đáp án giải thích của quyển sách rất hời hợt..Sách chỉ đưa ra đáp án chứ ko giải thích cụ thể tỉ mỉ nên thấy đáp án rồi vẫn ko thể hiểu tại sao đáp án lại ra như vậy...Tóm lại nếu bạn nào tư duy được thì rất đáng hoan nghênh và có khi có tố chất trở thành người thành công...:)))..Đó là sự khác nhau giữa người thành công và thất bại:TƯ DUY
3
420354
2015-12-28 10:53:02
--------------------------
356722
10027
Trước đây mình đã từng mua quyển "Harvard inspires the student's 300 thinking games" của Amazon, phiên bản giành cho máy Paperwhite cách đây hơn 1 năm. Nếu các bạn muốn tìm hiểu thêm các trò chơi tư duy hay đã từng ao ước một ngày nào đó bước chân vào ngôi trường danh tiếng bậc nhất này thì không nên chần chừ mà đặt mua ngay 1 quyển sách này và đặt trên kệ sách của mình.

Cuốn sách có cách trình bày rất rõ ràng và dễ hiểu, đem đến những bài học thiết thực cho mỗi bạn học sinh, sinh viên, thậm chí cho mọi lứa tuổi.
5
107293
2015-12-22 12:04:59
--------------------------
353779
10027
Ôi thực sự phải nói là không buết do đầu óc mình nó chậm hiểu thật hay do dấu hiệu tuổi tác mà đến cái phần câu đó dễ vẫn làm sai bét. Nhiều câu khó đâu mà khó. Đọc đáp án rồi vẫn bực vì ko hiểu sao lại làm ra được như thế :)). Nhưng công nhận cuốn sách rất hay và bổ ích. Phần giới thiệu tác giả viết rất hay và mình thấy rất thấm. Phải thú nhận đọc cuốn sách này thấy đầu óc mình đỡ ì. Cũng có chút vận động trí não, thấy cũng minh mẫn hơn hẳn :3. Các e mà thi olympia phải tậu ngay mấy cuốn kiểu này. Rất bổ ích
5
922532
2015-12-16 21:16:57
--------------------------
353641
10027
Một trong số những vấn đề lớn của nhiều người là tư duy một cách thông suốt vấn đề nên để làm được điều đó tác giả đã tổng gộp lại nhiều điều phong phú hơn , nhiều cấu đố thông minh , lời giải đáp hoàn toàn chính xác , tả rõ mối quan hệ của tri thức sáng tạo là ở đâu , có nhiều góc cạnh đặc thù nên nhìn nhận nó ra sao mới có cả nhận thức về tầm ảnh hưởng sâu xa hơn , thêm nhiều điều lý thú , bổ ích nhằm cải thiện ở trẻ em .
5
402468
2015-12-16 16:23:18
--------------------------
344960
10027
Vai trò của trí thông minh thì ai cũng biết rồi. Qua đọc sách của dượng Tony,mình biết rằng bây giờ các doanh nghiệp đang dần dần      không quan tâm nhiều tới bằng cấp mà cái chính là chỉ số IQ và EQ của cá nhân. Đó chính là lí do mình tìm tới quyển sách này. Sách chia làm 3 phần rõ ràng cho cả 3 cấp độ người đọc. Phần dễ là phần mang tính chất giới thiệu dạng câu hỏi và là phần khời động cho 2 phần khó sau đó. Thực sự mình đã phải "toát mồ hôi" để làm những câu hỏi hóc búa trong sách. Ngày trước mình rất thích Harvard vì độ nổi tiếng của nó nhưng sau khi đọc sách mình lại càng yêu mến nó hơn vì mục tiêu đào tạo của trường là tạo ra những người có tư duy tốt chứ không đơn thuần chỉ là những tấm bằng.
Cảm ơn tác giả rất nhiều!
5
168271
2015-11-29 14:44:15
--------------------------
340580
10027
Mình bắt gặp cuốn sách này ở nhà sách Fahasha. Tựa đề và cách trang trí của cuốn sách khiến mình phải lập tức lên Tiki mua về.
Những câu đố trong cuốn sách bắt đầu từ mức độ dễ đến khó làm mình cứ bị cuốn hút không muốn bỏ cuốn sách xuống. Sách giúp chúng ta rèn luyện trí não, tư duy logic. Mình nghĩ nên mua về đễ thư giản vừa học vừa chơi thay vì làm những việc mất thời gian mà không đạt được kết quả gì. Cảm ơn tác giả đã cho chúng mình cuốn sách hay như vậy.
5
326896
2015-11-20 09:08:29
--------------------------
332040
10027
Câu đố tư duy của sinh viên trường đại học Havard. 
Khi nhìn thấy tên quyển sách và bìa sách là mình muốn mua ngay rồi bời vì ước mơ từ bé của mình là được vào trường Harvard và bìa sách khá là đẹp. Thật không thất vọng khi mua quyển sách này. Bắt đầu với những câu đố dễ và rồi tăng dần mức độ khó lên. Những câu đố khá là thú vị và được giải một cách rất logic chúng giúp chúng ta rèn luyện trí óc giúp não chúng ta có nhiều nếp nhăn. Nói chung quyển sách này rất đáng mua.
5
595883
2015-11-05 16:51:36
--------------------------
326476
10027
Chú ý đến quyển sách ngay từ tiêu đề và quyết định mua nó khi đọc nhận xét và đánh giá của các bạn.
Về mặt hình thức: sách in đẹp, chất giấy tốt, in màu đẹp gây nhiều hung thú cho người đọc hơn
Về nội dung thì thôi rồi: Rất tuyệt, hack não, càng làm càng thấy thú vị, đòi hỏi mình phải linh hoạt, nhạy bén trong suy nghĩ hơn nhiều. Các dạng câu hỏi đa dạng và phong phú. Rất đáng đọc, mức giá theo mình là rẻ hơn so với những giá trị mà cuốn sách mang lại cho người đọc. Không đọc rất phí
5
563632
2015-10-25 22:06:16
--------------------------
321534
10027
Tiki giao hàng nhanh lắm, 1 ngày sau khi mình order là có liền luôn, đã quá.
Đầu tiên cầm cuốn sách lên, sách nặng, giấy bìa và ruột chất lượng rất tốt, hình ảnh rõ ràng sắc nét, nội dung toàn câu hỏi đánh đố bắt người đọc vân động trí não, chia ra nhiều cấp độ làm người đọc không bị bất ngờ bay vô câu hỏi khó liền, nói chung tiền bỏ ra không phí cho cuốn sách như thế này. Xong cuốn này mình sẽ mua thêm 1 cuốn nữa là 300 Trò Chơi Tư Duy Của Sinh Viên Trường Đại Học Harvard cũng thuộc bộ này luôn, hay lắm mọi người
5
395923
2015-10-14 10:01:17
--------------------------
310640
10027
Cuốn sách to, in màu tất cả các trang, giấy dày và chất lượng tốt. Thích hợp để luyện não hằng ngày, chống bị bệnh Alzheimer đấy ạ.
Sau một đêm đọc không ngừng nghỉ, làm được vài chục câu hỏi, não mình muốn nổ tung rồi kiệt sức muốn ngất luôn.
Xin hỏi có ai cảm thấy có vấn đề với câu 18 không? Mình nghĩ là 6:25 mới đúng chứ, đồng hồ số 1 cách số 2: 260ph, số 2 cách số 3: 320ph, số 3 phải cách số 4 là 380ph, vậy đáp án sẽ là 6:25. Không biết cách giải thích hợp lý nào cho cái đáp án 5:25 của sách??

5
6274
2015-09-19 18:08:07
--------------------------
305312
10027
Nội dung cuốn sách là quá tuyệt luôn. Nhiều câu đó khiến cho IQ của bạn được rèn luyện đấy. Có nhiều câu hỏi bình thường, tuy nhiên lại quá khó luôn. Mình thường đưa lên lớp cùng các bạn trong lớp giải. Ai cũng say mê và ham giải hết. Mình dự định sẽ mua thêm cuốn tập 2 nữa. Nói về phần chung quanh thì sách có khổ sách không có độ là to, sách được bao đọc bookcare gọn gàng. Chất lượng giấy tốt. Phải nói là tiền nào của nấy luôn đó! Thanks for reading! Đừng ngại ngùng đưa nó vào giỏ hàng nhé ;)
5
504577
2015-09-16 21:57:46
--------------------------
299909
10027
300 câu đố tư duy của sinh viên trường đại học Harvard quả là một cuốn sách tư duy hấp dẫn. Trình bày rõ ràng, dễ hiểu. Các câu hỏi được sắp xếp từ dễ đến khó. Vừa có câu đố hình ảnh, vừa có câu đố chữ nhưng không đa dạng về thể loại. Các câu hỏi gợi nhiều hứng thú cho người đọc. Có luôn đáp án nên dễ dàng tra cứu khi làm xong. Phù hợp cả từ hs cấp 2 đến sv đh hoặc hơn nữa. Nói chung đây là cuốn sách bổ ích và hấp dẫn để rèn luyện trí não và áp dụng các phương pháp giải toán khác nhau
5
267843
2015-09-13 16:15:27
--------------------------
296635
10027
Đây là một quyển sách hay về rèn luyện tư duy và nâng cao chỉ số IQ. Sách to và chất giấy dày, in màu vô cùng đẹp mắt. Còn gì tuyệt vời hơn khi vừa có thể giải trí vừa nâng cao khả năng suy luận của mình. 300 CÂU ĐỐ TƯ DUY CỦA SINH VIÊN TRƯỜNG ĐẠI HỌC HARVARD với những câu đố khác nhau theo cấp độ từ dễ đến khó, vô cùng hấp dẫn và bổ ích. Việc rèn luyện được khả năng tư duy phân tích tốt hơn có lợi ích rất lớn trong quá trình học tập và làm việc của chúng ta.
5
177708
2015-09-11 10:43:22
--------------------------
291000
10027
 300 CÂU ĐỐ TƯ DUY CỦA SINH VIÊN TRƯỜNG ĐẠI HỌC HARVARD là quyển sách rất hay và bổ ích, thích hợp cho mọi lứa tuổi, đặc biệt là thế hệ học sinh trong thời đại mới, phát triển và hội nhập này. Sách ngoài cung cấp những câu đố bổ ích, còn chỉ ra những phương pháp rèn luyện khả năng tư duy nhạy bén và sự phản xạ nhanh nhạy nữa. Cuốn sách này đã giúp mình rèn luyện và nâng cao chỉ số IQ của chính mình rệt. Ngoài ra, sách còn là công cụ thú vị để cùng chơi và giải đố với bạn bè!
5
299660
2015-09-05 22:46:09
--------------------------
287402
10027
Cuốn sách hay, hay từ hình thức tới nội dung. Trong kỷ nguyên số, có rất nhiều kênh giải trí mà chúng ta có thể tìm đến như phim ảnh, game online,.. những thú vui chỉ mang tính giải trí thì 300 CÂU ĐỐ TƯ DUY CỦA SINH VIÊN TRƯỜNG ĐẠI HỌC HARVARD là cuốn sách rất cần thiết để giữ một bộ não khỏe mạnh. 
Cuốn sách được chia thành 3 cấp độ, độ khó tăng dần, nhưng theo mình nó  chỉ mang tính tương đối. Có thể câu đố phần dễ  lại khó xơi hơn cả câu đố phần khó. Nó còn phụ thuộc vào người chơi. Và mặc dù không thực sự đa dạng về thể loại câu đố, cuốn sách cũng cung cấp rất nhiều màn thử thách đầy cân não!
5
268628
2015-09-02 18:15:43
--------------------------
284006
10027
Về hình thức, khổ sách khá to, không quá dày, giấy bóng, chữ và hình vẽ đều rõ ràng. Còn nội dung thì... đương nhiên là quá tuyệt! Những câu đố đầu thuộc loại dễ nhưng cũng khá là thử thách người chơi. Có nhiều câu đố cùng loại giúp người chơi rèn luyện được tư duy, nâng cao khả năng giải đố nhanh và đúng thông qua sự quen thuộc và luyện tập. Càng về sau càng khó, nhưng chắc chắn nếu bạn chơi tuần tự thì cũng sẽ quen dần. Những câu đó này cũng rất thích hợp nếu muốn đem ra đố vui hoặc chơi cùng nhóm bạn bè.
5
118852
2015-08-30 15:26:00
--------------------------
281400
10027
Mình là người thích giải những câu đố cho nên đã mua cuốn sách về đọc và cảm thấy rất tuyệt. Ngay từ cái bìa cuốn sách đã thu hút người đọc, chữ nổi và màu sắc, bố cục hài hoà rất bắt mắt. Cuốn sách được chia làm 3 cấp độ: Dễ, Trung Bình, Khó. Tuỳ mỗi cấp độ người đọc sẽ có những màn "cân não" khá hấp dẫn. Có những trò chơi tương tự nhau, ban đầu thấy cũng dễ ăn nhưng nên lên cấp độ trên thì thấy quả là hóc búa. Cũng may mắn là cuốn sách có lời giải cho mọi câu đố nên mọi thứ dễ thở hơn. Điểm thú vị là dù mình thấy câu đố nó khó đến đâu thì câu trả lời lại khá là đơn giản và làm ta phải "ồ! vậy sao không nghĩ ra ta!" Với những bạn thích giải đố và phân tích như mình thì cuốn sách này rất đáng để đọc ^^
5
66202
2015-08-28 13:20:06
--------------------------
277236
10027
Bạn nào đang luyện IQ thì mua quyển này về nghiền ngẫm cũng tốt, sách bắt đầu từ dễ đến khó. Nhưng mà lưu ý là tuy mức độ dễ thì nó vẫn là dễ đối với... Havard nhé, cũng không dễ nhằn tý nào đâu. Còn đối với 1 đứa đã đi làm và buổi tối chỉ thích trò gì đó giải trí 1 chút như mình thì mua quyển này về đúng là...hại não! Có nhiều câu toàn là phải giải phương trình, mà mình thì quên mất mấy cái dạng đó từ lâu rồi! Giờ thì để 2 quyển havard lên kệ sách cho nó...sang chảnh thôi. Mà công nhận là sách đẹp thật đấy, dày và...nặng! Mở sách ra thấy đẹp nên không nỡ lòng nào mà quệt vào trong đó cả.
4
36188
2015-08-24 20:02:49
--------------------------
262206
10027
Tôi mua cuốn sách “300 Câu Đố Tư Duy Của Sinh Viên Trường Đại Học Harvard” này bởi vì sự tò mò được kích phát từ tựa đề cuốn sách mang lại. Để đọc được cuốn sách, tôi đã phải tư duy triệt để, và không ích lần “xoắn não”. Đọc hết cuốn và thực sự hiểu hết là cả một quá trình kiên nhẫn. Không hẳn là cuốn sách toàn những câu đó quá khó, nó được trình bày theo bố cục từ câu dễ đến khó dần; nhưng chắc tại khả năng tư duy còn kém của tôi mà cuốn sách khá khó “gặm” đối với tôi. Tuy vậy, cuốn sách rát có ích trong việc nâng cao tư duy, sự nhạy bén và sáng tạo của tôi. 
5
276840
2015-08-11 19:00:11
--------------------------
248333
10027
Cuốn sách này thực sự rất bổ ích cho những ai muốn rèn luyện khả năng tư duy của mình. Mình mua cuốn sách này khi phải chuẩn bị cho kì thi vào trường đại học và mình đã thực hiện cuốn sách này khoản một tháng và thực sự mình cảm thấy mình cải thiện được rất nhiều về khả năng tư duy hình học cũng như logic . Tuy nhiên các câu hỏi này thực sự rất khó và cần một quá trình rèn luyện bản thân lâu dài. Nhìn chung thì bìa sách và chất lượng giấy rất đẹp in rất nét( tuy nhiên cũng hơi tiết khi cầm bút quẹt vào) Nhìn chung đây là một cuốn sách rất hay cho những ai muốn cải thiện khả năng tư duy logic cũng như phản xạ của mình .
5
254228
2015-07-31 09:48:33
--------------------------
243016
10027
Mình mới vừa nhận cuốn sách cách đây vài ngày và mình thật sự rất hài lòng. 
Về chất liệu và trình bày: Sách được in màu với giấy giày và mịn, thực sự rất thích luôn ý. Cách trình bày logic đi từ dễ đến khó, tạo điều kiện dễ dàng cho người đọc nâng cao tư duy bản thân. 
Về nội dung: Các câu hỏi bao gồm từ nhận biết hình dạng, tư duy không gian, tư duy logic cho đến tư duy số học và suy luận đều đầy đủ. Người đọc có thể luyện tập một cách toàn diện.
Tóm lại, một quyển sách hoàn toàn đáng mua cho những ai muốn nâng cao tư duy! 
5
297694
2015-07-27 09:39:04
--------------------------
242540
10027
Mình rất thích giải những câu đố tư duy dạng này, như kiểu các câu hỏi trong Đường lên đỉnh Olympia nên cũng mua 1 cuốn. Sách khổ lớn, giấy dày, màu sắc in rất đẹp và bắt mắt. Nội dung gồm 300 câu đố tư duy từ mức độ dễ đến khó và có đáp án ở phía sau mỗi phần, rất thích hợp cho những bạn thích giải đố và muốn phát triển tư duy, logic. Có nhiều câu rất khó, phải "căng não" rất lâu mới giải được cho nên giải xong là đầu óc bấn loạn luôn. Nói chung là mình rất thích cuốn này.
5
512560
2015-07-26 19:32:24
--------------------------
235148
10027
Mình thì cũng 22 tuổi rồi cơ mà thích mấy câu đố logic cực, đều xưa toàn gặp câu dễ hay sao, mua quyển này về ở mức dễ cũng thấy mất thời gian khá lâu í.
Nói chung các câu trong quyển này đều rất rất hay, nơ ron thần kinh nở ra nhiều lắm í, haha. Khó nhưng vẫn cố gắng giải, càng khó mà giải ra lại càng thú vị đúng ko nào, hihi.
Mình ấn tượng về chất lượng bìa cũng như giấy và hình ảnh, rất đẹp và bắt mắt, bìa cũng dày dặn nữa. Đây là 1 cuốn sách rất hay cho bạn nào đam mê những câu hỏi logic.
5
361929
2015-07-20 21:29:51
--------------------------
230622
10027
Mình biết đến cuốn sách này là do một người bạn đem vào lớp, sau khi cả nhóm cùng nhau đấu trí một vài câu mức độ trung bình mình liền về nhà đặt mua một cuốn. Bìa sách trang trí đẹp, cứng cáp, giấy là loại tốt, không hại mắt, nội dung trình bày logic dễ lựa chọn câu hỏi. Về độ khó thì lúc đầu thấy tên sách là 300 Câu Đố Tư Duy Của Sinh Viên Trường Đại Học Harvard nên cứ nghĩ sẽ rất khó, nhưng thật ra ở mức độ dễ vẫn có thể trả lời được. Nếu làm cùng bạn bè còn cảm thấy rất lý thú hơn nữa, rèn luyện trí não rất tốt. Một cuốn sách rất đáng bỏ tiền và thời gian ra đọc.
5
155666
2015-07-17 16:23:42
--------------------------
230620
10027
Mình mê quyển 300 câu đố tư duy của sinh viên trường đại học Harvard này ngay từ khi nhìn thấy nó ở hiệu sách, nên khi thấy ở tiki có hàng mà lại được giám giá nữa là mình mua luôn.
Sách rất đẹp được in màu toàn bộ gồm các câu đố tư duy logic dành cho nhiều cấp độ từ dễ đến khó, tạo một sự thách thức lớn cho người đọc, có đáp án ngắn gọn ở đằng sau. Nói chung mình rất thích nội dung sách với các câu hỏi được biên soạn rất khoa học
5
153008
2015-07-17 16:21:00
--------------------------
228596
10027
Những câu đố trong sách được chia làm 3 phần : dễ, trung bình và khó. Mới đầu mình giải mấy câu dễ, tuy tự giải được cũng khá nhiều nhưng mình cũng gặp không ít khó khăn, và phải xem phần đáp án. Những câu đố cực kì thú vị, khiến mình như bị thôi thúc phải giải tiếp vậy. Bên cạnh đó, bìa sách trình bày đẹp, những trang trong được in cũng rất đẹp luôn. Khổ sách to, dày, trông gần giống hình vuông nên giở sách ra rất dễ mà không phải gấp bìa. Mình thực sự rất hài lòng về cuốn sách và dịch vụ của tiki. Cảm ơn tiki rất nhiều. 
5
668946
2015-07-15 17:07:19
--------------------------
228086
10027
Ban đầu mua sách này vì cái tên rất kêu của nó. Sau khi nhận được thì quả thật rất hài lòng. Trước hết là hình thức. Khổ giấy to, bìa cứng, các trang in màu, giấy chất lượng.... thật không còn gì để không vừa ý. Về nội dung các câu hỏi, thật ra mình nghĩ không đến nỗi phải mang tên Harvard(vì bản thân mình mặc định rằng Harvard là 1 thứ gì đó cao siêu không thể tưởng) bởi có nhiều câu không phải quá khó. Dẫu rằng, những câu hỏi thuộc dạng Test IQ thế này thì thật thú vị(có bạn nhắc đến Đường lên đỉnh Olympia làm mình cũng hồi tưởng 1 thời...). Nói chung, đây là quyển sách tốt về hình thức và chất lượng về nội dung.
4
502287
2015-07-14 22:23:10
--------------------------
220922
10027
Đây thật sự là 1 cuốn sách hay và bổ ích cho những ai yêu thích giải mã những câu đố. Ban đầu nhìn thấy cuốn sách mình cứ ngỡ cuốn sách này rất khó nhưng khi mua về mình thật sự rất hài lòng vì những câu đố trong cuốn sách rất hay và bổ ích. Mình muốn cải thiện khả năng tư duy của mình nên đã lựa chọn cuốn sách này. Giấy của cuốn sách rất tốt, k hại mắt, bìa sách trình bày đẹp, các câu hỏi được sắp xếp hợp lí theo mức độ khó tăng dần. Mình đã k ngần ngại chia sẻ cuốn sách này cho bạn bè cùng mua và bạn bè mình cũng rất hài lòng về cuốn sách. Mong những cuốn sách hay ngày càng có nhiều để cho những bạn đọc như mình có thêm nhiều kiến thức. Cảm ơn Tiki rất nhiều đã đem lại những cuốn sách hay cho mọi người.
5
596879
2015-07-03 09:38:12
--------------------------
220841
10027
Vào nhà sách và chìm đắm bởi những câu hỏi từ quyển sách này, khi bước ra đầu óc choáng váng. Về đến nhà lên tiki tìm ngay để mua, nhưng hết hàng mất, buồn lắm, đăng ký nhận thông báo khi có hàng liên tục luôn, một ngày đẹp trời nhận được tin nhắn có hàng. Vội vã đặt ngay!
Đó là câu chuyện của bản thân mình về sức hút của quyển sách này. Tuy mỗi lần giải đố những câu hỏi xong đầu óc mệt nhừ, quay cuồng, mặc dù mới chỉ đi đến cấp độ dễ nhất trong quyển sách, nhưng cảm thấy rõ rệt được đầu óc mình làm việc càng ngày càng khỏe hơn. 
Những người làm việc bằng tư duy nhiều thì những bài tập thể dục trí não này không thể thiếu được. 
Bên cạnh đọc những cuốn sách triết lý về cuộc sống thì  cuốn sách này là một người bạn bổ trợ không thể thiếu.
5
74132
2015-07-03 01:26:27
--------------------------
220836
10027
Đầu tiên khi nhận quyển sách ấn tượng nhất là kích thước, to hơn mong đợi nhiều, to hơn cả sách giáo khoa thông thường gần giống như quyển Album ảnh màu vậy, cả bìa và bên trong đều có chất lượng giấy rất tốt, giất trắng tinh, bìa màu dày, nội dung bên trong cũng màu nốt và nổi bật nhất là cực nhiều hình ảnh, bạn nào làm mấy bài test IQ hay tuyển dụng chắc sẽ cảm thấy rất quen thuộc.
Phần nội dung thì những gì tựa đề đề cập thì bản chất bên trong giống y vậy rồi, về mức độ thì tăng dần theo số thứ tự, càng phong phú càng thích hợp cho nhiều người đọc.
Một tác phẩm để rèn luyện trí tuệ, cải thiện tư duy, kích thích sáng tạo...
5
451219
2015-07-03 00:40:15
--------------------------
210729
10027
Ngay tiêu đề cuốn sách đã rất hấp dẫn người đọc , Harvard mà - có ai mà không tò mò . Cuốn sách có chất liệu tốt , giấy dày , màu sắc đẹp mắt . Các câu đố từ đơn giản đến phức tạp theo trình tự tư duy của não bộ giúp não bộ của bạn tha hồ phát triển . Từ tư duy logic , hình học , số học đến suy luận đủ các lĩnh vực . Sự lặp lại và nâng cao của các câu đố giúp người đọc dễ dàng hơn khi giài những câu tiếp theo ,ngoài ra còn kích thích sự sáng tạo . Mình khá hài lòng về cuốn sách và cảm thấy rất đáng khi bỏ tiền mua .
5
487567
2015-06-19 19:36:01
--------------------------
190892
10027
Mình không biết xuất xứ các câu đố trong sách có phải là từ đại học HARVARD danh giá hay không nhưng chúng là quá tuyệt cho não của bạn... Sách minh họa bởi những hình vẽ màu, các câu đố như test IQ và làm ta mường tượng đến chương trình : Đường lên đỉnh Olympia của đài truyền hình VTV.  Ít nhất mình nghĩ quyển sách cũng có tác dụng nhất định đến việc khơi dậy và phát triển tiềm năng trí tuệ, chúng ta càng chơi càng thích thú và giúp rèn luyện trí tuệ... Có thể kết hợp 1 nhóm để cùng chơi trong các cuộc tụ họp bạn bè...
4
594852
2015-04-30 14:19:16
--------------------------
163266
10027
Quyển sách này tôi đã mua được 4 năm nhưng mỗi khi mở ra và ngửi mùi thơm của giấy, tôi nhận ra rằng tình yêu mà tôi dành cho nó vẫn nguyên vẹn như ngày nào. Quyển sách đầu tiên tôi mua ở Đà Nẵng và ý nghĩa hơn khi đây là quyển sách đầu tiên mà má tôi cho tiền tôi mua. Những câu đố trong sách rất hay và bổ ích, giúp kích thích tư duy sáng tạo, tư duy hình ảnh, tư duy số học và logic. Có những câu đố rất khó nhưng cũng có những câu tưởng chừng như đơn giản nhưng tôi lại làm sai. Có những câu đòi hỏi chúng ta phải có nhiều thông tin như phải biết tên ca sĩ mới trả lời được, nói chung là rất đa dạng. Sách in màu, giấy rất tốt làm tôi rất ngại mỗi khi có ai muốn mượn quyển sách này.
5
387632
2015-03-04 16:39:31
--------------------------
157591
10027
Mình mua quyển sách này chung với cuốn "300 Trò Chơi Tư Duy Của Sinh Viên Trường Đại Học Harvard". Quyển này là 1 tập hợp rất nhiều câu đố đi từ mức dễ đến trung bình rồi đến khó, loại câu hỏi phân chia đều khắp các dạng tư duy khác nhau, có đại số tính toán, có hình học logic, có cả giải mê cung và nhiều dạng test IQ khác nữa. Mình thấy cách sắp xếp thứ tự các câu và cách ra đề rất thú vị, không khiến cho người đọc nản chí khi gặp câu đố khó mà ngược lại kích thích sự sáng tạo và buộc người chơi phải động não không ngừng. Đây là 1 bài tập luyện vô cùng hữu ích cho não bộ nên mình cũng ngồi miệt mài giải mỗi ngày vài câu. Lúc đầu nghĩ là sẽ khó lắm nhưng càng làm thì càng thấy các câu đố rất vừa sức, chỉ cần mình tập trung và kiên trì đến cùng sẽ giải ra tất cả. Nếu bạn nào bí quá thì có thể tham khảo đáp án ở phía sau nên sẽ rất tiện lợi cho bạn nào muốn rèn luyện trí lực và thử thách bản thân
5
66279
2015-02-09 15:22:26
--------------------------
142243
10027
Những câu đố thú vị và rất IQ. Trên thị trường có nhiều loại sách về tăng IQ nhưng lại lan man và lung tung, mình rất khó chọn lựa nhưng từ khi phát hiện ra cuốn sách này mình ko cần phải kiếm trên mạng  hay tự tổng hợp nũa mà cuốn sách nhày đã tự sắp xếp câu hỏi từ dễ đến khó với sự trình bày bắt mắt, sinh động giúp kích thích não bộ dùng, câu hỏi thú vị không nhàm chán đòi hỏi phải vắt óc suy nghĩ. Bạn nào muốn tăng trí thông minh thì đừng đi đâu xa mà hãy mua cuốn sách này mình tin là bạn sẽ không hối hận
5
433306
2014-12-17 22:54:23
--------------------------
124325
10027
mình từng lân la đi tìm sách về IQ nhưng thú thực là toàn thấy sách cho trẻ em. cho đến 1 ngày mình vô tình biết đến cuốn "300 câu đố tư duy của sinh viên trường harvard" này trên tiki, mình k ngần ngại đặt luôn và đây cũng là đơn hàng đầu tiên của mình trên tiki :) 
về hình thức: cảm nhận đầu tiên của mình là rất rất thích chất liệu giấy của sách, ngoài ra cách phối màu xanh đỏ, đậm nhạt cũng rất bắt mắt.
về nội dung: sách mở ra trong mắt mình một thế giới mới logic hơn, khoa học hơn. có những câu hỏi nhìn đã ra đáp án, có những câu phải giải qua rất nhiều bước, cũng có những câu thật hóc vì mình xem đáp án r mà vẫn k hiểu >.
5
403939
2014-09-06 18:28:39
--------------------------
111098
10027
Tôi thực sự bị hấp dẫn bởi nhan đề cuốn sách này : " Câu đố tư duy của sinh viên HARVARD " 

Đúng vậy ! HARVARD là ước mơ của triệu triệu sinh viên trên khắp thế giới , là một trong những ngôi trường nổi tiếng và tốt nhất trên thế giới ! Nơi đây đã sản sinh ra những con người vĩ đại , giàu có và quyền lực nhất hành tinh của chúng ta . 
Và HARVARD cũng là ước mơ của chính bản thân tôi nữa ! Nghe có vẻ xa vời nhưng mà vì đời không đánh thuế ước mơ và bởi vì tôi còn trẻ vậy thì sao không dám thử một lần chứ ! 

Tôi tin bất cứ ai cho dù chưa từng thích thú giải câu đố thì cũng bị cuốn sách này hấp dẫn ! Như có một ma lực bí ẩn nào đó đang tồn tại trong chính cuốn sách này ! 
Cuốn sách được sắp xếp một cách rất thông minh và tinh tế : Những mức độ câu hỏi được sắp xếp từ thấp đến cao không phải chỉ là ngẫu nhiên mà là một dụng ý khôn khéo của chính tác giả khi kích thích khả năng tò mò tư duy mà chinh phục đỉnh cao của mỗi con người ! Tất cả những câu hỏi đều rất đa dạng sáng tạo , không hề gây một cảm giác nhàm chán nào cả !
Đôi lúc bạn sẽ nản chí , cảm thấy thất vọng và muốn từ bỏ thì tôi mong bạn có thể lấy lại được bình tĩnh và bắt đầu tiếp tục chinh phục cuốn sách này bởi bạn sẽm được trả giá xứng đáng với những công sức của mình và tận hưởng cảm giác lâng lâng hạnh phúc khi giải quyết được câu hỏi khó cứ như mình vừa giải cứu trái đất vậy :) 
Bạn sẽ không cảm thấy thất vọng một chút nào đâu ! Tôi tin là như thế bởi không chỉ nội dung rất tuyệt mà ngay cả cách trình bày , chất lượng giấy hay in ấn đều gần như hoàn hảo mà khó có thể tìm ra một sai sót nào cả.Quyển sách này thực sự có thể chinh phục cả những độc giả khó tính nhất ! 
Đây có lẽ là một cuốn sách hiếm trên thị trường bởi vì không có quá nhiều cuốn sách đề cập đến thể loại này nên tôi nghĩ bạn sẽ hối hận nếu không nhanh tay ắm em này nếu bạn là một người yêu thích các câu đố , tư duy ! 

Tôi cảm thấy mình vừa đến gần HARVARD hơn nữa sau khi đọc cuốn sách này 
5
36336
2014-04-24 22:12:03
--------------------------
74978
10027
Chất lượng bản in của sách rất tốt, sách in màu đóng gáy keo chắc chắn. Nội dung tập sách là những câu đố tư duy nhằm khơi dậy tính logic, nhanh nhạy trong suy nghĩ. Các câu hỏi được sắp xếp theo mức độ từ dễ đến khó, do đó phù hợp cho cả học sinh lớp 5,6 và các bạn sinh viên. Gần đây những công ty, tập đoàn lớn tuyển chọn nhân viên đều có kèm phần kiểm tra tư duy, thiết nghĩ quyển sách này là tài liệu nên có cho những bạn sinh viên sắp ra trường.
4
99784
2013-05-16 09:10:15
--------------------------
71552
10027
Khi lật những trang đầu của cuốn sách này tôi phải nói thật rất bất ngờ về nó, về những câu hỏi vì phải chăng...chúng quá dễ, ít nhất đối với tôi - một học sinh có học lực ở mức bình thường.
Và, thật ra tôi đã lầm, tôi quên mất mình chỉ đang làm ở những câu hỏi ở mức độ dễ, qua phần mức câu hỏi bình thường và mức câu hỏi khó tôi đã thực sự phải chảy mồ hôi. Những câu hỏi của " 300 Câu Đố Tư Duy Của Sinh Viên Trường Đại Học Harvard" quả thật là không khó nhưng đòi hỏi phải tư duy rất rât nhạy vả tập trung vào vấn đề, tìm hiểu thật kĩ các dữ kiện và chi tiết.
Bí quyết để giải những câu hỏi này theo mình là tập trung và mục đích cuốn sách này mang đến cho chúng ta chắc có lẽ là sự tư duy, nó luyện cho chúng ta tính tư duy cao, sự nhạy bén và cả sự kiềm chế: khi giải không ra một câu đố nào đó.
Havard chắc hẳn là trường đại học mơ ước của biết bao nhiêu người và mình cũng không ngoại lệ, cuốn sách này là một bước ngoặc nhỏ đem Havard đến gần chúng ta hơn.
5
104997
2013-04-27 18:11:04
--------------------------
68138
10027
Đây là một cuốn sách rất bổ ích về tư duy. Ngay từ phần mở đầu đã được tác giả dẫn dắt bằng những lợi ích của việc giải những câu đố tư duy, và ví dụ bằng những câu hỏi thú vị, hấp dẫn tạo nhiều cảm hứng để đọc những phần tiếp theo. Cuốn sách cũng chia nội dung ra làm 3 phần, theo cấp độ từ dễ đến khó giúp người đọc thích nghi từ từ. Câu hỏi cũng đa dạng, phong phú, từ nhìn số, nhìn hình đến nhìn chữ. Màu sắc cũng đa dạng, bắt mắt  tạo hứng thú hơn cho người đọc. Khi giải ra được những câu đố, bạn sẽ thấy thích thú, hứng khởi vô cùng.
5
82716
2013-04-10 15:30:18
--------------------------
61500
10027
Cảm quan ban đầu của mình khi thấy cuốn sách là nó được thiết kế rất đẹp và bắt mắt. Khổ sách to tạo cảm giác hứng thú cho người đọc, đồng thới chứa đựng được nhiều hơn những kiến thức và thông tin trong đó. Cuốn sách sẽ đưa người đọc đến gần hơn với phương pháp giáo dục của phương Tây, cũng như hiểu thêm về cách thức mà các bạn sinh viên Harvard phát triển tư duy của mình. Có nhiều câu đố trong sách khá hóc búa nhưng vô cùng thú vị, chắc chắn sẽ gây được sự thích thú cho nhiều bạn đọc khác nhau, đặc biệt là các bạn trẻ. Cuốn sách là một cẩm nang bổ ích trên con đường học tập của mỗi chúng ta.
4
20073
2013-03-02 17:50:05
--------------------------
31856
10027
Đang lướt xem mấy chiếc đồng hồ, xem mấy cuốn sách, vì ngân sách của mình có hạn nên mình tìm đến quầy sách giảm giá, không ngờ lại tìm đc cuốn này. Là một người yêu thích khoa học, tiên về sử dụng não trái, mình rất thích chơi các trò chơi, câu đố về tư duy. Nhưng trên thị trường Việt Nam hiện nay không có nhiều loại sách như thế này, cũng vì vậy mà mình gặp chút khó khăn khi đến các quầy sách để "soi" nhưng cuốn sách về câu đố thử trí tư duy. Nhưng thật may mắn, mình đã tìm được một cuốn như vậy trên Tiki.vn, hơn nữa lại là trò chơi của chính các sinh viên của một trường đại học hàng đầu thế giới, trường đại học mà mình muốn được vào học. Và chắc chắn, cuốn sách này không thể không nằm trong giỏ hàng của mình được
5
36378
2012-06-29 21:30:45
--------------------------
