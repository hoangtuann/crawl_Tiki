466642
10069
Đổi Mới Tư Duy - 101 Cách Khơi Nguồn Sáng Tạo là một cuốn sách mỏng so với kiến thức mà nó mang lại, rất hữu ích. Mình đã xem nhiều cuốn sách về sáng tạo, cuốn nào cũng đều rất dày, và viết rất dài dòng, nếu như muốn tiết kiệm thời gian đọc thì vận dung cuôn này để phát triển khả năng sáng tạo rất tốt. Và những kiến thức từ nó cũng rất đỗi quen thuộc nếu đọc từ nhiều sách khác rồi. Có thể sử dụng nó như một cuốn cẩm nang, tin và dùng vậy.
5
74132
2016-07-02 16:56:24
--------------------------
348704
10069
Bạn mình mượn trên thư viện, đọc ké thấy rất hữu ích nên lên tiki tìm rồi đặt luôn : )
Cuốn sách là giúp người đọc khám phá ra nhiều cách suy nghĩ độc đáo hơn, sáng tạo hơn, loại bỏ lối tư duy cũ.
Sách chia từng mục rõ ràng, ngắn gọn, từng câu chữ rất dễ hiểu, có ví dụ- câu đố rất thú vị, chất liệu giấy cũng tốt.
Vì là viết về đổi mới tư duy, nên mọi người nên mua về rồi đọc đi đọc lại nhiều lần để ứng dụng thường xuyên hơn. 
5
591687
2015-12-06 16:40:11
--------------------------
347173
10069
Sách đề cập đến nhiều vấn đề gần gũi trong cuộc sống nhưng hầu như không mấy ai để ý đến. Có nhiều vấn đề mình đã giật mình nhìn lại vì mình cũng ở trong số đông những người có cái nhìn sai lệch về nó. Đọc cuốn sách này mình có thể khám phá ra nhiều cách thoát khỏi tư duy cũ, biết cách động não để "khơi nguồn sáng tạo". Đôi lúc mình cũng ngạc nhiên vì mình có những suy nghĩ độc đáo, mới lạ. Nếu trước đây mình có những ý tưởng hơi kì lạ là y như rằng mình sẽ không bao giờ nói ra chúng nhưng còn bây giờ, khi đã đọc qua cuốn sách này, mình có thể tự tin nói ra suy nghĩ của mình mà không ngại ngùng bất cứ điều gì hết.
4
976965
2015-12-03 23:48:18
--------------------------
319780
10069
Sau vài tuần chiêm nghiệm thì mình thấy đây là một trong những cuốn sách dạy kĩ năng bổ ích nhất mà mình từng đọc. Sách có rất nhiều kiến thức cần thiết mà mình có thể áp dụng trong công việc và cuộc sống thực tế. Cách viết ngắn gọn, xúc tích, mỗi phương pháp đều được phân tích một cách ngắn gọn nhưng đầy đủ và sắc sảo liên quan tới thực tế nhiều, giúp người đọc hình dung được rất nhiều điều trong cuộc sống. Tuy nhiên chất liệu giấy không được tốt cho lắm. Cuốn sách này sẽ tuyệt hơn nếu chất liệu giấy tốt hơn. Vài lời góp ý!
4
760378
2015-10-09 16:42:36
--------------------------
309230
10069
Nội dung chính của quyển sách là giúp người đọc khám phá ra nhiều cách thoát khỏi thói quen tư duy cũ kỹ, phát hiện ra những cách suy nghĩ độc đáo hơn, sáng tạo hơn, với những góc nhìn đầy tươi mới. Nhưng nếu chỉ dừng lại ở đó thì bất kỳ quyển sách giáo dục tâm lý nào cũng có thể làm được. Điều khiến tôi cảm thấy độc đáo ở quyển sách này chính là sự ngắn gọn súc tích, những ví dụ dễ hiểu, hơn thế nữa là những câu đố để ta vận dụng sau mỗi bài học. Tất cả những điều này đã khiến "Đổi mới tư duy - 101 cách khơi nguồn sáng tạo" trở nên cuốn hút chứ không khó nuốt như những quyển sách cùng thể loại khác. Nếu bạn đang kiệt sức vì không thể đưa ra những ý tưởng mới hay ho cho công việc hay trong cuộc sống, hãy thử nghiền ngẫm quyển sách này để mở rộng tư duy, khơi nguồn sáng tạo nhé!
4
639663
2015-09-18 22:51:00
--------------------------
306768
10069
Bản thân mình rất chần chừ trong hành động và không thể sáng tao. Suy nghĩ của mình đang rơi vào lối mòn, áp dụng những kiểu lý luận từng thích hợp của ngày xưa,có phần cố chấp và bảo thủ.Đôi khi còn thấy bị kẹt trong mớ bòng bong – đánh mất cơ hội tưởng tượng, thất bại không tìm ra giải pháp thỏa đáng cho vấn đề đang đối mặt và để nhiều cơ hội vượt qua mất.
Đổi mới tư duy - 101 cách khơi nguồn sáng tạo là cuốn sách mình kỳ vọng thay đổi bản thân,sống năng động,tích cực,hoạt bát và tự tin hơn.Mọi người đều có khả năng đổi mới tư duy để sử dụng trí não xứng đáng hơn, độc đáo hơn khi đã đọc qua quyển sách này.
5
707574
2015-09-17 19:48:29
--------------------------
298238
10069
Dạng sách như vậy mình đọc khá nhiều rồi nên nội dung không gì mới mẻ. 
Nhưng với một người chưa từng đọc về sáng tạo thì nơi đây tập hợp nhiều trò hay nhất, giúp đầu óc mở mang thêm. 
Nhiều ví dụ hay, thực tế, điển hình chỉ ra cách mà chúng ta có thể nhìn vấn đề khác đi. Lấy chúng làm câu đố cho bạn bè cũng được, để học hỏi lẫn nhau thêm. 
Cuốn sách nhỏ tiện gọn nhưng giấy tệ quá. Tâm hồn đẹp mà được chuyên chở bởi cơ thể xấu như Thị Nở thì có ai thèm nhìn tâm hồn đó. 
Phải chi chất lượng giấy và bìa tốt hơn. Vì nội dung rất chất lượng. 
4
535536
2015-09-12 15:56:07
--------------------------
280620
10069
Tư duy sáng tạo giúp chúng ta rất nhiều trong cuộc sống. Cuốn sách này ra đời nhằm đưa đến cho người đọc những phương án, biện pháp thích hợp để kích hoạt khả năng sáng tạo và để tăng cường khả năng tư duy của mỗi người trong một xã hội đầy cạnh tranh như ngày nay đòi hỏi mỗi ứng viên tương lai đều phải độc đáo, nổi bật trong đám đông.
Đây là một cuốn sách đáng mua song chất giấy hơi xấu, sách khá mỏng nữa!!! Dù sao cũng hữu ích phần nào đó cho người đọc!!!
4
186614
2015-08-27 20:38:16
--------------------------
280186
10069
Cuốn sách khá mỏng và cực kỳ nhẹ (khổ sách cũng khá là nhỏ nữa), nhưng nguồn cảm hứng mà tác giả Rob Eastaway đem đến cho người đọc thì không hề nhỏ chút nào. Nếu bạn muốn mở rộng chân trời cho mình, muốn bắt đầu sự nghiệp mới hoặc học 1 kỹ năng mới thì đây là cuốn sách dành cho bạn ^^ Khi đọc kỹ, quan sát và thấm được nội dung phong phú trong sách, bạn đang có cơ hội khám phá những phương pháp suy nghĩ sáng tạo trong mọi lĩnh vực đời sống, giúp bạn đổi mới và "sống" nhiều hơn mỗi ngày. Lời khuyên của tác giả, thách thức hay câu hỏi mà ông đặt ra sẽ giúp độc giả mở mang thêm đầu óc tư duy, trí tưởng tượng phong phú hơn và tự tin biến ước mơ thành hiện thực ! Cuốn sách được biên tập rất khéo (trình bày đẹp, mạch lạc và sinh động), câu từ của tác giả thì rất hay và không khô khan chút nào, mình rất thích, cảm ơn NXB Trẻ và Tiki đã giúp mình được cầm trên tay em nó ^^
5
75959
2015-08-27 14:40:11
--------------------------
276044
10069
Quyển sách nhỏ gọn, giấy màu vàng ngà đọc không chói mắt. Mình rất thích cách lý luận và dẫn người khác vào vấn đề của quyển sách này, nó còn trích dẫn nhiều câu nói rất ý nghĩa của cách doanh nhân, người nổi tiếng nữa. Những bài trắc nghiệm trong đây rất hay, khi phải suy nghĩ một câu trắc nghiệm khó, lúc đọc kết quả mình không khỏi ồ lên một tiếng, thì ra nó không khó, chỉ do lối tư duy mình chưa được rộng mở. Tuy nhiên, có một vài đáp án nên giải thích rõ ràng ra vì sao lại như thế thì sẽ hay hơn!
4
471735
2015-08-23 16:10:45
--------------------------
257769
10069
Một quyển sách nhỏ nhưng chứa đựng nhiều ý tưởng. Ưu điểm của sách là trình bày ngắn gọn, dễ hiểu, chữ viết, minh họa mạch lạc, dễ nhìn. Giấy xốp, nhẹ, vàng ngà, không gây loá mắt. Cũng vì sách mỏng, nhẹ nên các vấn đề cũng được trình bày đơn giản, có tính lý thuyết, chưa đi sâu, chủ yếu mang tính chất gợi ý, gợi mở vấn đề. Tuy nhiên đây cũng là một dàn bài khá lý thú đưa ra cho bạn đọc nhiều phương pháp tư duy, suy luận rộng mở hơn. Một trong số những nội dung mà mình thấy có tính ứng dụng cao và dễ thực hiện đó là hướng dẫn cách thức tổ chức một họp, cũng giống như phần mình đã được học kỹ năng mềm trước đây. Đầu tiên là xác định mục đích, đưa ra vấn đề. Sau đó đón nhận mọi ý tưởng mà không phê phán chỉ trích. Cuối cùng là đóng góp, phê bình chọn ra ý tửơng tiềm năng, tối ưu."Không ai mạnh bằng tất cả chúng ta cộng lại"- thế nên nghe ý kiến khác nhau của mọi người đôi khi có thể gợi cho chúng ta những ý tưởng sáng tạo mới, tốt và hợp lý hơn. Cũng thú vị đấy chứ ^^.
4
648692
2015-08-07 22:52:53
--------------------------
240877
10069
Lẽ ra cuốn sách này được 5 sao nhưng tôi chỉ đánh giá 4 sao vì chất liệu giấy để in ấn khá tệ, nó làm giảm mất thiện cảm của tôi về cuốn sách này. Tuy nhiên , thông tin giá trị của sách lại rất bổ ích cho tôi. Nó nêu ra nguyên nhân từng vấn đề một, phân tích mọi vấn đề cần phải thay đổi ở đâu, nhưng tôi vẫn cảm thấy nó thiếu một cái gì đó làm giảm giá trị của sách. Thế nhưng, tôi vẫn rất thích cuốn sách này, bạn cũng nên mua một cuốn để bổ sung kiến thức cho bản thân của bạn.
4
539768
2015-07-24 23:30:37
--------------------------
191707
10069
Đây là một cuốn sách hay, khá bổ ích. Cách viết ngắn gọn, xúc tích, liên quan tới thực tế nhiều, giúp người đọc hình dung được rất nhiều điều trong cuộc sống. Tuy nhiên có khả năng người đọc ( như tôi) khi chưa tiếp xúc với vấn đề nào đó nên khi đọc cuốn sách này sẽ cảm thấy hơi khó hiểu VD: về việc làm, giảm cân,... hơi nghiêng về phần lí thuyết một tí, ít thực tế. Nhưng nói tóm lại cuốn sách này vẫn rất hấp dẫn từ trang bìa cho đến nội dung. Là một cuốn sách hay nên mới nhận được mà tôi đã đọc gần hết. ( Nó rất hấp dẫn)
4
595429
2015-05-02 14:54:23
--------------------------
130241
10069
Quyển sách thật sự thú vị. Mình có một vài nhận xét để các bạn tham khảo :
- Sách viết về đổi mới và sáng tạo nên phù hợp với mọi đối tượng đọc giả. Sách trình bày đẹp, mục lục dễ tra cứu, có kích thước nhỏ, gọn có thể dễ dàng mang theo bên mình (đi làm, đi học, làm bài tập nhóm). Các phương pháp sáng tạo được trình bày theo từng đề mục, ngắn gọn, dễ hiểu.
- Sách có các bài toán giải đố kích thích bạn "suy nghĩ ngoài chiếc hộp". Cá nhân mình thấy số lượng các câu đố khá ít. Nếu tác giả dành một mục riêng để tập hợp thêm nhiều câu đố cho đọc giả giải đáp thì sẽ tuyệt hơn.
Tóm lại, nếu bạn yêu thích tư duy sáng tạo, đổi mới thì bạn nên mua sách. Những phương pháp, kỹ năng được trình bày sẽ giúp bạn ít nhiều gỡ rối những khó khăn trong công việc và học tập.
5
398179
2014-10-16 09:32:13
--------------------------
79747
10069
Đây là một trong những cuốn sách dạy kỹ năng bổ ích nhất, cũng như thú vị nhất mà mình từng được đọc. Cuốn sách có rất nhiều kiến thức cần thiết mà mình có thể áp dụng trong công việc và cuộc sống thực tế. Những phương pháp tư duy được tác giả đưa ra rất mới mẻ và độc đáo, làm người đọc bất ngờ và thích thú. Mỗi phương pháp đều được phân tích một cách ngắn gọn nhưng đầy đủ và sắc sảo, giúp người đọc có được một cái nhìn hoàn thiện nhất về cách đổi mới tư duy của chính bản thân mình. Quan trọng nhất, cuốn sách đã chỉ cho mình thấy rằng, đôi khi chỉ cần một thay đổi nhỏ trong suy nghĩ, tư duy và hành động thôi, cũng đã đủ để giúp chúng ta chạm tay tới những thành công mới mẻ rồi.
5
109067
2013-06-09 10:31:57
--------------------------
