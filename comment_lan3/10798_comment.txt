473242
10798
Ai cũng có thứ thuộc sở hữu của mình và đương nhiên chẳng ai mong muốn thứ thuộc về mình mất đi cả. Nhưng ở đời thì chẳng thể biết trước được điều gì hết đâu. Có những thứ xảy đến với ta mà ta không hề có một khái niệm hay sự phòng bị nào cả. Đây lạ một tác phẩm nhẹ nhàng với cốt truyện hấp dẫn làm lôi cuốn bạn đọc. Được sống ở một nơi không phải dành giật, đấu đá nhau, mỗi ngày trôi qua đều thật đặc biệt và bình yên thì ai mà không thích cơ chứ.
5
949022
2016-07-10 13:19:38
--------------------------
458784
10798
Khi đọc tựa sách mình rất ấn tượng với từ "Chốn này" vì rất ít và hình như chưa có ai dùng từ này để đặt tựa sách. Mình chỉ thấy nhiều tác giả hay dùng từ "nơi" thay vì "chốn" nên đặc biệt thích tựa sách.
"Có một nơi gọi là chốn này" làm mình liên tưởng tới "Alice ở xứ sở thần tiên" khi có một thế giới lạ lẫm, bí ẩn hiện ra trong ngòi bút nhẹ nhàng của Cecelia Ahern. Tác giả đã dùng giọng văn lôi cuốn, tinh tế để kể lại hành trình của nhân vật chính - Sandy. Qua đó, "Có một nơi gọi là chốn này" đã truyền tải nhiều ý nghĩa nhân văn và sâu sắc.
4
1157252
2016-06-25 14:34:08
--------------------------
412616
10798
Mình thích quyển sách này ngay từ những năm đầu ra mắt. Mỗi khi đi nhà sách mình luôn muốn ôm em nó về nhà nhưng ngặt nỗi túi tiền không cho phép. Thật may  vì đã mua được quyển sách này trên tiki mà còn giảm giá 50% nữa chứ.
Điều ấn tượng đầu tiên của mình là bìa sách, đẹp và gợi cho mình một cảm giác dịu ngọt. Ấn tượng thứ hai là tên sách, cái tên khiến người ta tò mò. Mình chấm điểm 10 cho hai phần này.
Mình biết Cecelina qua tác phẩm nổi tiếng nhất của cô P,S I love you. Sau đó mình đã tìm đọc những quyển sách còn lại của cô trong thư viện nhà trường như Cảm ơn kí ức, Nơi cuối cầu vồng, Nếu em thấy anh bây giờ. Các tác phẩm của Cecelina đều nhẹ nhàng trong từng câu chữ. Từ khi bắt đầu đến khi đóng trang sách lại mình luôn có cảm giác như đang được dẫn dắt bởi một sợi tơ ngôn ngữ mong manh. Vì thế mình đã kì vọng rất nhiều vào quyển sách này. Tuy nhiên có lẽ kì vọng quá nên mình đã hơi hụt hẫng một chút khi quyển sách khép lại. Vì cảm giác cách hành văn hơi dông dài. Nội dung truyện thì mới lại, thực sự rất lạ khi kể về một nơi chốn lý tưởng, không có phân biệt giữa người và người. Nội dung thì mình chấm 4 sao  vì nó không hay như mình nghĩ.
4
1162275
2016-04-08 05:42:46
--------------------------
411473
10798
Có Một Nơi Gọi Là Chốn Này à câu chuyện hấp dẫn từ đầu đến cuối. Nữ nhân vật chính từ đầu tiên đã được miêu tả khác so với những ngừơi xung quanh cô, nhưng cuộc đời của cô cũng khác lạ như vậy. Chuyện trinh thám này khá nhẹ nhàng. Nữ chính bỏ sức tìm những thứ mình mong ước để thảo sức lắp đày những khoảng trống, những câu hỏi mà cô thắc mắc từ lúc bé để rồi lại mong ước về cuộc sống hiện tại. Phải chăng con người có đôi lúc đừng nên cố chấp quá
4
653217
2016-04-06 05:41:14
--------------------------
398897
10798
Một cuốn sách có nội dung khá mới mẻ và lôi cuốn. Một cuốn sách nói về chốn này nơi mà chắc ai trong số chúng ta đều đã từng nghĩ về nó. Đó là thế giới nơi những đồ vật mất đi sinh sống. Một cuốn sách có giọng điệu lãng mạn gợi cho ta nhiều triết lí sâu sắc.Về hình thức mình không thích bìa sách lắm vì nó trông hơi rối có ch tiết không liên quan lắm đến nội dung cuốn sách chất lượng giấy in khá tốt. Với mình đây là tác phẩm hay nhất của Cecelia Ahern và mong sẽ có thêm những tác phẩm như vậy.
 
4
580169
2016-03-16 21:58:00
--------------------------
329915
10798
Cách xây dựng "chốn này" của tác giả khá hay, trước giờ mình vẫn hay thấy đồ vật bị mất tích một cách kỳ lạ rồi lại tìm ra được cũng kỳ lạ không kém mà không hề hình dung được có một nơi được gọi là "Chốn này" để tập hợp hết những người và vật như thế. Nhưng hình ảnh mà mình ấn tượng nhất trong sách là hình ảnh ngọn đèn trước hiên, hình ảnh đó vừa ấm áp mà lại vừa mất mát...

Từ sau khi đọc sách cứ thấy có gì "đi đâu mất" là mình lại nghĩ nó đã đến "Chốn này" rồi :D
5
83028
2015-11-01 17:12:24
--------------------------
280065
10798
Quyển sách cho thấy sự liên kết giữa người với người, giữa người với đồ vật. Mang hơi hướng huyền ảo không thực nhưng không phải là không thể nghĩ đến trong thế giới đầy những điều bí ẩn. Ngay cả khi mượn thư viện đọc xong, mình không ngần ngại mua sách về để đó có dịp đọc lần nữa để nghiền ngẫm. Quyển sách không quá khó hiểu nhưng nếu luôn mang suy nghĩ những điều xảy ra đều cần khoa học chứng minh thì không thể hiểu được. Với mình đây là quyển sách hay nhất của Cecelia Ahern.
5
202979
2015-08-27 13:40:11
--------------------------
208112
10798
Một tác phẩm nhẹ nhàng và nên thơ về sự mất đi và tìm thấy, Cecelina dựng lên 1 cốt truyện thật sự hấp dẫn và dẫn dắt người đọc cuốn vào nó thật tự nhiên và duyên dáng. Chốn Này thật là tuyệt vời, con người ta sống chan hòa với nhau chẳng có sự bất công hay kỳ thị  gì, tiền bạc chẳng là gì cả, nếu có dịp tôi cũng mong rằng mình sẽ được đến chốn ấy. Người ta có thể mất đi nhưng trong lòng người họ yêu thương thật sự thì họ vẫn luôn luôn hiện hữu. Sách khá dày, đôi lúc tác giả diễn giải mọi thứ quá dài dòng là tôi rất lười đọc, Jack đi 1 vòng lớn cả năm trời để tìm em mình và cuối cùng mọi chuyện được giải thích chỉ trong đôi ba trang sách, việc Sandy đến và đi ở Chốn Này cũng lờ mờ như 1 màn sương, đáng lẽ cái kết phải đẹp hơn nữa. Dù sao vẫn rất cám ơn tác giả với 1 quyển sách thật tuyệt này.
3
46905
2015-06-14 09:56:21
--------------------------
114952
10798
Đây là lần đầu tiên mình đọc truyện của Cecelia Ahern, tuy nhiên cô ấy đã cho mình sự thiện cảm ngay từ những chương đầu tiên. Truyện xoay quanh Sandy Shortt - một cô nàng cao vượt trội. Từ tuổi thơ mãi đi tìm những chiếc tất, những món đồ bị thất lạc và khó chịu với cô bạn hàng xóm, Sandy quyết định làm trong ngành tìm kiếm người mất tích và không may trong một hôm đang làm nhiệm vụ, cô lạc vào khu rừng bí ẩn dẫn đến xứ sở Chốn Này. Truyện dùng những từ ngữ miêu tả sinh động, có phần lôi cuốn. Tác giả đã diễn tả tính cách từng nhân vật thật hoàn hảo, mang đến cho người đọc những cảm xúc khác nhau về họ. Một câu chuyện nhẹ nhàng, sâu lắng, nhưng lại ẩn chứa những bài học có giá trị. Mọi thứ xung quanh ta đều quý giá, chúng ta nên trân trọng tất cả từng phút, từng giây, đừng nên hờ hững, lạnh lùng với chúng vì khi mất chúng ta sẽ cảm thấy thật trống trải. Truyện đã đánh vào tâm lí người đọc, làm mình cũng phải lắng lòng. Mình thích nhất là chương cuối của truyện, những dòng chữ nhẹ nhàng, rút ra ý nghĩa bài học cho cả tác phẩm. Mình nghĩ ai đọc xong cũng phải suy ngẫm về thông điệp đáng quý này. Thật sự thì mình không thể gấp lại khi đã đi sâu vào câu chuyện của Cecelia.
5
340113
2014-06-20 17:44:38
--------------------------
97149
10798
Tôi thích cách chơi chữ của tựa đề. "Chốn này", một mặt là nơi lưu giữ những thứ, những người lạc bước, dù vô tình hay cố ý, dù thực tế hay trong tâm tưởng, tác giả còn ngầm ý ám chỉ chốn này có thể là nơi mà những người đó thật sự thuộc về. Ngoài ra, chốn này còn là có nghĩa là hiện tại, thực tế cuộc sống mà khi lìa xa người ta lại khao khát quay về, như Sandy. Cô là kẻ luôn bỏ trốn khỏi cuộc sống, trốn khỏi những người yêu thương cô, để rồi khi tìm thấy thứ mình suốt cuộc đời bỏ công tìm, cô lại chỉ muốn quay về bên họ. Và tôi thích cái kết cục rằng, sau tất cả, họ (Sandy và Jack) quay về và bắt đầu lại cuộc sống của mình, bỏ mặc những đứt gãy đã qua (24 năm của Sandy và 1 năm của Jack).
4
123574
2013-10-19 13:34:21
--------------------------
84035
10798
Phải công nhận rằng Cecelia Ahern thực sự là một nhà văn trẻ tài năng. Tác phẩm nào của cô cũng để lại rất nhiều dấu ấn đậm nét, khiến mình muốn đọc đi đọc lại nhiều lần. Và "Có một nơi gọi là chốn này" là một tác phẩm như thế. Trong cuốn sách này, Cecelia tiếp tục dùng ngòi bút của mình để tạo nên những trang văn mang âm hưởng lãng mạn, ngọt ngào đầy sâu lắng. Không chỉ có vậy, cô còn đan cài vào tác phẩm những yếu tố kỳ ảo, khiến câu chuyện càng thêm phần hấp dẫn, kịch tính. Nhân vật nữ chính, Sandy, được tác giả miêu tả với những nét tính cách sắc sảo và mạnh mẽ, nhưng ẩn chứa sâu bên trong là một tâm hồn nhạy cảm, một trái tim tràn đầy tình cảm. Chính những điều này đã đưa Sandy vào một hành trình kỳ lạ, một hành trình tìm lại quá khứ, tìm lại những điều tưởng chừng đã mãi mãi rời xa.
Qua nhân vật Sandy, qua câu chuyện kỳ lạ đầy xúc cảm, Cecelia Ahern đã gửi gắm đến người đọc một thông điệp thật tuyệt vời, rằng hãy luôn biết trân trọng những gì mình đang có trong cuộc đời, dù đó là những điều nhỏ bé nhất.
4
109067
2013-06-28 15:50:52
--------------------------
56039
10798
Đây là cuốn sách thứ 3 mình được đọc của nhà văn Cecelia Ahern. Tuy nó không thực sự ấn tượng như những cuốn trước nhưng vẫn là một tác phẩm hay, đáng đọc và để lại nhiều dư âm sâu sắc cho mình. Tác giả kể một câu chuyện nhẹ nhàng, giản đơn nhưng lại mang những màu sắc bí ẩn, lạ kỳ. Hành trình của nhân vật chính, một hành trình tìm kiếm những mất mát, ẩn chứa rất nhiều ý nghĩa và thông điệp sâu sắc. Chỉ thông qua những chi tiết nhẹ nhàng, nhà văn đã đem đến một câu chuyện tinh tế, chất chứa nhiều xúc cảm và gửi gắm nhiều bài học cho cuộc sống mỗi chúng ta. Và một lần nữa, Cecelia Ahern đã chứng to được sức lôi cuốn của chính mình.
4
20073
2013-01-18 18:53:08
--------------------------
36274
10798
Mình yêu Cecelia Ahern, yêu những câu chuyện vừa thân thiện quanh đây, vừa xen chút kì bí và lôi cuốn và đều mang những triết lý cuộc sống của chị, yêu cái giọng văn vừa gần gũi lại lãng mạn, rất dễ lấy lòng một trái tim.
Biết chị qua cuốn " Nơi cuối cầu vồng", và mình không thể ngừng tìm kiếm thêm những tác phẩm của chị.  "Có Một Nơi Gọi Là Chốn Này" hấp dẫn mình ngay từ tựa đề. Vừa kì lạ, vừa có chút tinh quái, tinh tế, và mình đã không bỏ qua cuốn sách này.
Cecelia Ahern luôn biết cách làm mới mình, những câu chuyện của chị nhiều màu sắc khác nhau, cách kể và nội dung cũng khác hẳn nhau, chính điều đó làm nên sự thú vị và hấp dẫn dài hơi của bạn đọc với chị.
Với câu chuyện này, chị kể về một cô gái tên  Sandy Shott, Một cô bé 10 tuổi sau khi cô bạn hàng xóm mất tích thì luôn trăn trở, không yên về chuyện ấy. Có thể nghe hơi lạ, nhưng ngẫm lại mình mình thấy rất đúng, quả thật, khi mất đi một thứ từng thuộc về mình, bên cạnh mình, là của mình, đều vô cùng khó chịu. Nỗi băn khoăn ấy cứ ám ảnh khiến khi lớn lên, cô quyết định vào ngành cảnh sát, vào những cuộc săn tìm, rồi cô có cơ hội được lạc vào một vùng đất mang tên Chốn Này, đây là nơi lưu giữ mọi thứ từng bị mất tích. Ở đây, cô đã tìm ra ý nghĩa thực sự của cuộc sống với mình
Sự đan xen các yếu tố kì bí, pha đậm chất lãng mạn thường có với các triết lý sâu sắc về sự cảm nhận cuộc sống,  "Có Một Nơi Gọi Là Chốn Này" là một câu chuyện đủ hay, đủ hấp dẫn với tất cả chúng ta. Mình nghĩ là như vậy.

5
10984
2012-08-08 20:24:01
--------------------------
31631
10798
Một cuốn sách khác của Cecelia. Tuy cuốn sách này không lãng mạng, cuốn hút như Nơi cuối cầu vồng và có nhiều đoạn hơi dài dòng nhưng cũng khá hay. Đây là hành trình tìm kiếm những thứ,những người bị mất tích của Sandy Shott. Trong hành trình này cô đã lạc lối vào một nơi gọi là "Chốn này". Tại đây cô không những đã tìm thấy những người mà cô luôn bị ám ảnh phải tìm kiếm mà cô còn tìm thấy chính bản thân mình. 
Thực vậy, trong cuộc sống đôi lúc ta lạc lối, mải tìm kiếm những điều gì đó mà bỏ qua những khoảnh khắc, những người thực sự có ý nghĩa trong cuộc đời mình.Nhưng thật tuyệt vời khi chúng ta tìm ra lối thoát và được tìm thấy.
4
28102
2012-06-27 20:04:43
--------------------------
31629
10798
Một cuốn sách với cốt truyện vô cùng kì lạ về những những con người, đồ vật bị mất tích và được tìm lại. Từng tình tiết được dẫn dắt khéo léo, cuốn hút người đọc.
Nhân vật chính là Sandy Shortt – người luôn ám ảnh về việc phải tìm lại những thứ bị thất lạc – đến nỗi thành lập công ty tìm người mất tích. Cho đến khi chính cô cũng bị mất tích và tìm thấy thế giới của những con người mà cô đã hằng tìm kiếm bấy lâu.
Tất cả chúng ta đều đi lạc một lần trong đời. Và khi chúng ta học được điều mà tâm hồn cần có, con đường lại cứ thế mở ra. Sandy cũng vậy, khi cô nhận ra những giá trị đích thực của cuộc sống từ thế giới của những người bị mất tích, nhận ra “không có nơi nào như nhà mình”, cô đã tìm thấy con đường trở về cuộc sống thực, trở về với gia đình thân thương và người cô yêu quí.
Cuốn sách đã dạy tôi nhiều bài học quí giá về “Điều gì ta có được hẳn được lấy từ một nơi khác. Điều gì ta mất rồi sẽ đến nơi khác.” Luôn luôn là như vậy, mọi thứ luôn có chỗ của nó. Một cuốn sách mà chắc chắn bạn không thể bỏ lỡ!

5
10908
2012-06-27 19:02:30
--------------------------
6223
10798
Lần đầu tiên nhìn thấy quyển sách này, tôi đã bị ấn tượng bởi cái tên và bìa sách. Có một nơi gọi là chốn này... Chốn này chính là nơi mà những người và vật bị mất tích, hay nói đúng hơn, là những người và vật bị lãng quên. Những con người ấy cố gắng thoát khỏi Chốn này, bởi Không nơi đâu bằng nhà mình. Những ý nghĩa được rút ra từ câu chuyện này, thật sự không thể nói bằng lời, mà chỉ có thể cảm nhận bằng trái tim. Hãy tự mình đọc và cảm nhận nhé
5
4221
2011-06-14 09:49:54
--------------------------
