547349
10498
Nội dung của sách hay, không viết dài dòng, dễ theo dõi. Tuy nhiên chất lượng sách thì mình không thích tẹo nào. Giấy quá trắng, khá mỏng, chất lượng mực in kém, nhìn chữ cứ như là máy in sắp hết mực, chưa hết phông chữ cũng không được đẹp, khoảng cách giữa các dòng khá gần nhau, cho nên mỗi lần đọc hết một câu chuyện là phải dừng vì mỏi mắt lắm.
3
1932246
2017-03-19 08:51:00
--------------------------
461112
10498
Mình thấy nội dung thì rất được, mọi người nhận xét khen nhiều rồi nhưng mình rất không thích chất liệu giấy. Rẻ tiền vô cùng, giấy rất mỏng và trắng xóa đọc một lúc rất đau mắt, nhìn hình thì sách có vẻ đẹp và mang nét cổ cổ vậy thôi chứ hàng thật thì rất thất vọng, mình sờ chất liệu đã không muốn đọc nữa rồi, hại mắt cực kì. Giá mà NXB đầu tư hơn thì mình đã hài lòng hơn với cuốn sách này rồi, rất buồn. Còn về nội dung thì giống như một dạng tóm tắt những vở kịch vậy đó.
3
569970
2016-06-27 15:36:24
--------------------------
257082
10498
Tôi thích sự lãng mạn, nhân văn mà logic trong truyện, kịch của Shakespeare.Tôi rất bất ngờ vì xem tiểu sử cuộc đời ông, con người vĩ đại ấy đã từng làm chân giữ ngựa, soát vé ở cổng rạp hát; sau đó làm nghề nhắc tuồng, thợ sửa bản in, dần dần lên làm diễn viên, đạo diễn và nhà viết kịch. Quả không sai khi ông được mệnh danh là "linh hồn của thời đại". Tôi muốn các con mình cũng được tiếp cận, thưởng thức các tác phẩm của ông và cuốn sách  "Những Câu Chuyện Kể Của Shakespeare"  - Nhà sách Đinh Tị dù ngôn ngữ dịch trong tác phẩm đôi lúc còn trúc trắc chưa thực sự chau chuốt nhưng phần nào đã tóm lược, cho các bạn đọc nhỏ gia đình tôi tiếp cận được những câu chuyện, vở kịch vĩ đại của Shakespeare.
4
438149
2015-08-07 13:36:51
--------------------------
222255
10498
Đây là cuốn sách tác giả Charles, Mary Land biên tập lại những vở kịch của Shakespear như Macbet, Othelo, Romeo và Juliet, vua Lear... thành những câu chuyện nhỏ dễ đọc, dễ theo dõi nhưng vẫn giữ nguyên được ý nghĩa nhân văn sâu sắc mà Shakespear muốn truyền tải.
Đối với mọi người, đây là một quyển sách rất hay. Bởi không phải ai cũng có thời gian đọc những tác phẩm chính kịch đồ sộ của Shakespear. Vì vậy, việc viết lại những vở kịch vừa giúp mọi người nắm được nội dung câu chuyện, vừa giúp họ tiếp cận được phần nào với các tác phẩm lớn của ông.
Hơn nữa, tác giả dùng ngôn ngữ gần gũi, đơn giản nên cuốn sách rất dễ đọc, dễ đi vào lòng người giống như những truyện cổ tích.
4
164722
2015-07-05 10:50:57
--------------------------
180618
10498
Mình rất thích quyển sách này vì nó có nội dung rất hay, rất đáng tiền để mua. Các bạn có thể mua về kể cho các em nhỏ nghe mỗi ngày vì nó có nhiều câu truyện nhỏ bên trong, có giá trị nhân văn cũng như rèn luyện về đạo đức rất sâu sắc, nhất là cho các bé mới lớn đang trong giai đoạn nhận thức về thế giới xung quanh, về cuộc đời của các bé. Hi vọng quyển " Những Câu Chuyện Kể Của Shakespeare " có thể có ích giúp các bé hiểu được và nhận thức được về phần đạo đức thông qua lối kể truyện gần gũi mà giản dị của đại thi hào Shakespears. Rất đáng tiền để mua                                      .
5
576561
2015-04-09 20:12:30
--------------------------
