545805
10860
Xuất sắc bởi khuấy động cảm xúc và tâm can người đọc. Càng đọc những trang viết của cô Tư, càng thấy bải hoải những nỗi đau đến rụng rời. Đau thế mà không dứt nổi, cứ muốn tiếp tục dấn thân, để được nghĩ suy, để được đau rồi tự mình vỗ về tâm hồn mình nguôi ngoai.
Không nhiều tác giả gây cho mình xốn xang đến vậy. 
Ngòi bút cô như một viên ngọc quý, càng mài càng sáng. 
5
1450675
2017-03-17 14:23:16
--------------------------
467794
10860
Ngòi bút Nguyễn Ngọc Tư không phải là ngòi bút dễ lôi cuốn người đọc, nó đòi hỏi người ta phải suy nghĩ nhiều hơn đằng sau những ngôn từ chị viết, về các nhân vật và các số phận của chị viết ra. Tôi đã đọc tác phẩm nổi tiếng chuyển thể thành phim là Cánh Đồng Bất Tận, hay như tiểu thuyết Sông. Và ở tác phẩm này, sự sâu sắc của ngòi bút lại được nâng lên hơn nữa. Tác phẩm của chị luôn lắng lại trong lòng người đọc sự man mác, bang khuâng, khó diễn tả trọn vẹn bằng lời, nhưng mà nó rất thực, rất đời! 
4
572177
2016-07-04 11:56:38
--------------------------
457599
10860
Thôi thôi, bà Tư viết truyện buồn đứt ruột con người ta. Cứ vầy sao mà dám mua nữa, viết chi đâu mà buồn từ truyện này qua truyện khác, từ dòng này qua dòng khác, từ con chữ này qua con chữ khác. Buồn chi buồn dữ dạ Tư? Bộ đời bà không có cái gì vui để mà viết hả? Hay bà làm bộ bịa ra cái gì vui vui đi cho người đọc người ta cũng vui chút. Cắm đầu vô là bã buồn, bã buồn từ đầu đến hết sách bã vẫn buồn. Mà bã buồn đã lắm nha! Như cái kiểu kiêu hãnh sĩ diện ghê lắm, cái kiểu mà ta buồn thì kệ ta, ta không mắc mớ mấy người, mấy mà đừng có mà nói đến làm gì!!
5
607108
2016-06-24 13:20:30
--------------------------
359486
10860
Nếu Đảo là những câu chuyện về con người đậm chất nông thôn quê mùa thì Gió Lẻ lại mang hơi thở của thị thành nhiều hơn, mà đa số là những con người xuất thân từ quê lên tỉnh. Mang hương vị của những con người nửa mùa, dù sống nơi đô thị nhưng vẫn mang hơi thở của quê hương.

Cái hay của Nguyễn Ngọc Tư ở đây là những cái kết khó đoán, gây bất ngờ cho người đọc, mà sao toàn buồn tha thiết. Những phận người già trẻ lớn bé có đủ mặt để làm nên một bức tranh đời sống nhiều màu sắc, nhưng ta lại thấy mỗi tâm hồn là một sự cô đơn với bao niềm riêng chất chứa. 

Vẫn cách viết giản dị nhưng phải nói rằng cực kì tinh tế đi sâu vào lòng người, những câu chuyện có chiều sâu thực sự không bao giờ là đơn giản bởi nó bắt người đọc phải suy ngẫm. 
4
937231
2015-12-27 10:09:22
--------------------------
345128
10860
Những trang văn, những câu chữ của Nguyễn Ngọc tư luôn ám ảnh khiến người đọc phải trăn trở không thôi. Đọc một lần để rồi nhớ mãi. Là một nhà văn trẻ nhưng những tác phẩm và sự trải đời, sự thấu cảm mà chị mang đến cho người đọc không hề non nớt và hời hợt chút nào. Nội dung hay đã đành nhưng tiêu đề cũng hay không kém. Tiêu đề những tản văn, truyện ngắn của chị luôn để lại dấu ấn cho người đã đọc và sự tò mò cho những đọc giả có ý định mua sách của chị.
4
897666
2015-11-29 20:56:31
--------------------------
333905
10860
Ngay từ câu chuyện đầu tiên "Vết chim trời" đến câu chuyện cuối cùng là "Gió lẻ" đều mang một sắc thái - ám ảnh. Từ anh em đến người yêu, từ gia đình đến xã hội đều đang hình thành những vết nứt, vết rạn do tổn thương do con người gây ra cho con người. Có lẽ bất kì ai đọc những chuyện Nguyễn Ngọc Tư viết ra cũng đều bị ấn tượng mạnh bởi cách viết, cách nhìn, cách dùng từ của chị để nói về những cuộc đời trong trang viết của chị. Thật đáng tiếc nếu ai đó đọc sách mà chưa từng đọc qua chuyện mà chị viết. Riêng trong cuốn này mình bị rung động mạnh nhất bởi hai câu chuyện là "Sầu trên đỉnh Puvan" và "Gió lẻ", đọc xong nó mình tự nhiên nhận thấy rằng: thật ra cái "sầu" và cái "chết" nó không khác nhau là bao...
5
919635
2015-11-08 15:08:46
--------------------------
332072
10860
Đây là cuốn sách đầu tiên của Nguyễn Ngọc Tư mà tôi đọc, và tôi ngay lập tức có tình cảm với chị. Cách viết của Tư tuy nhẹ nhàng nhưng thoáng đượm buồn đau, để lại trong người đọc một cảm giác rất lạ - vừa đau vừa thương nhân vật. Những câu chuyện của chị hầu hết đều mang tính hiện thực cao và giản dị chân chất, chẳng hoa mĩ, màu mè, nhưng có thể dễ dàng khiến người đọc rơi nước mắt. Người đọc bị chị lôi cuốn, đi theo câu chuyện của chị, để rồi đau cùng những nhân vật chị tạo nên, đắm chìm trong những nỗi buồn riêng chung ấy tự bao giờ. Cuốn sách giúp tôi nhận ra nhiều điều hơn về cuộc sống, rằng ngoài những mặt tối tăm, xấu xa của xã hội hiện đại, đâu đó vẫn còn tồn tại thứ tình cảm thiêng liêng cao đẹp, rung động lòng người. Chị không viết về tình yêu đôi lứa mà xoáy sâu vào đời sống thường ngày của con người, đó là điều đáng quý mà những nhà văn hiện đại của Việt Nam nên học tập. 

5
937267
2015-11-05 18:22:15
--------------------------
320214
10860
Văn của Nguyễn Ngọc Tư luôn là sâu sắc mà lắng sâu trong lòng người đọc. Đọc qua một câu chuyện của chị, dù là ngắn hay dài đều khiến ta phải suy ngẫm, ngẫm xem cái hồn trong truyện, cái buồn trong truyện sâu rộng tới chừng nào. Quyển này vẫn thế, mỗi truyện là một sắc màu khác, nhưng đâu cũng thấy buồn, cái buồn lửng lơ đến cái buồn khắc khoải. Thực sự truyện của chị ảnh hưởng tới người đọc rất nhiều, giống như chị đã đem những góc khuất của xã hội ra để phơi bày, để ngẫm và để buồn dùm, đâu phải ai viết văn cũng khiến người ta lặng mình trong câu chuyện đâu. Thực sự đây là quyển rất hay và ý nghĩa.
4
634444
2015-10-10 18:50:42
--------------------------
315147
10860
Nguyễn Ngọc Tư là một nhà văn trẻ, người ta thường nói, người trẻ trải đời ít, cuộc sống ngày nay lại tốt hơn thời ông cha nên cái nhìn của họ còn non nớt, ít cảm nhận được nhiều góc cạnh của cuộc đời. Nhưng đối với chị thì khác, văn của Nguyễn Ngọc Tư không hào nhoáng, bóng bẩy, không hời hợt mà chân chất, giản dị như chính những con người Nam bộ. Những nhân vật trong tác phẩm của chị, đa phần đều thiên về phụ nữ, nhưng nỗi đau day dứt, những nỗi cô đơn, sự hối hận dằn vặt trong mỗi con người. Và từng góc khuất trong xã hôi, nỗi bất công thì ở đâu cũng có nhưng qua giọng văn của chị, ta vẫn thấy sự yêu đời, niềm tin mãnh liệt vào cuộc sống, vào tương lai phía trước, vào sự đổi thay từng ngày trên quê hương đất nước.

Nói không ngoa, Nguyễn Ngọc Tư chính là nhà văn có sức ảnh hưởng mạnh mẽ nhất đối với dòng văn học trẻ hiện nay, các tác phẩm của chị đều nằm trong top được yêu thích nhất, luôn trong tình trạng cháy hàng.

Hy vọng, với những nhiệt huyết của tuổi trẻ, Nguyễn Ngọc Tư sẽ cho ra đời những tác phẩm hay hơn nữa để cống hiến cho dòng văn học Việt Nam.
5
47857
2015-09-28 10:32:53
--------------------------
309554
10860
Nguyễn Ngọc Tư, những câu chuyện chị viết đều làm cho người đọc cảm thấy chạnh lòng. Họ đi vào những câu chuyện của chị lúc nào không hay, đi vào sâu quá và họ khóc cùng nhân vật. Tôi cũng vậy, tôi khóc, nhưng lại cảm thấy được an ủi rất nhiều, vì thấy như nhờ chị Tư có rất nhiều người đồng cảm và hiểu được những mảng trong câu chuyện đời mình, đời họ. Dù người cạnh mình đối diện đó là một quyển sách. Một người bạn sâu sắc, nghiềm ngẫm những triết lý bạn chia sẻ. Cảm ơn rất nhiều.
4
101774
2015-09-19 00:37:22
--------------------------
302632
10860
Sách của chị Tư bao giờ cũng buồn, nhưng luôn là những nỗi buồn nhân văn, nó cảm hóa và giúp con người hướng thiện. Chị luôn để trang viết của mình gắn với sông nước nghĩa tình, luôn khiến người đọc mãi ám ảnh về vùng đất ruột thịt của mình. Gió lẻ và 9 câu chuyện khác cũng mang đậm phong cách Nguyễn Ngọc Tư, mộc mạc, tự nhiên, đôi lúc nhiều điều quá mức hiển nhiên đến đáng châm chọc được chị nói bởi giọng văn rất tỉnh và hài hước, nhưng lại khiến độc giả phải nghiền ngẫm suy tư nhiều. Đọc sách chị, và tôi nhận ra có quá nhiều điều mình đã bỏ quên và vụt mất giữa cuộc đời.
4
27405
2015-09-15 12:59:42
--------------------------
279644
10860
Cứ mỗi lần cầm quyển sách mới mua về của chị Tư, mình luôn nghĩ, rồi câu chuyện này có buồn không đây, và lại buồn đến như thế nào?..."Gió lẻ và 9 câu chuyện khác" cũng mang chất buồn rất riêng đó. Buồn- không ào ào như gió cuốn, không khốc khô hàng nước mắt. Buồn thấm vào từng rãnh rẽ trên cơ thể, như cơn gió lẻ xé tan từng đêm quạnh. Chực rớt nước mắt khi nhìn những Sói, những "em", chết mảnh đời giữa hai nửa gia đình. Nhưng rồi, lại chát lòng, chua xót với chiếc xe máy chạy, trốn cả quá khứ, trốn tương lai, chạy vào đêm mưa, để nghe trái tim chết theo từng viên đá lở, dù vẫn sống khơi khơi. Ám ảnh với văn của chi Tư, với con đường dài như bất tận, khi con người đang cố sống khác người, để tồn tại trong thế giới nhốn nháo những "người"...
Không biết, rồi sẽ có những cuộc đua, trốn chạy, "giết chóc" nào tiếp tục xé lòng người đọc trong văn chị?
5
475837
2015-08-26 23:30:17
--------------------------
272699
10860
"Gió lẻ và 9 câu chuyện khác" mang đậm phong cách giản dị, mộc mạc nhưng triết lý sâu xa của nhà văn Nguyễn Ngọc Tư, mỗi nhân vật trong cuốn sách mang lại cho mình rât nhiều cảm xúc đan xen, ứng với mỗi câu chuyện là ngững cái kết ngỡ ngàng khiến chúng ta khó mà thoát ra được. Đây là một tập sách không nên bỏ lỡ cho những ai đã, đang và sẽ yêu văn Nguyễn Ngọc Tư. Và mình luôn là đọc giả trung thành của Tư.

4
342197
2015-08-20 12:15:03
--------------------------
267315
10860
"Gió lẻ" không biết là cuốn sách thứ mấy của chị Tư mà mình đọc bởi mình luôn dành tình cảm yêu quý cho chị, sách của chị, kể cả những mảnh đời trong chuyện của chị. Đọc xong mình buồn mấy ngày, tại thấy sách viết thật quá. Mình lên mạng tìm hiểu về những địa danh như Puvan, Thổ sầu,...xem có thật sự tồn tại không mà sao chị lại viết chân thật đến như vậy. Mỗi câu chuyện đều có những triết lí sâu xa khiến chúng ra cần phải thờ dài nghiền ngẫm. Đọc đi đọc lại mấy lần, càng đọc càng thấy buồn, càng nhìn thấy những số phận của nhân vật chị như hiện ra trước mắt mình, thấy cái làng Thổ Sầu buồn thê thiết, thấy màu tím hoa Sầu cùng lời nguyên chết người, thấy cả ông già bất lực trước tỉnh yêu và tuổi già,...Ám ảnh!
5
5257
2015-08-15 14:05:28
--------------------------
265863
10860
Tôi biết cô Tư là nhờ Gió lẻ. Tôi được thầy ngữ văn giới thiệu, thầy thích truyện của cô lắm nên thầy giới thiệu nhiệt tình lắm, kể tóm tắt truyện sơ qua cho tôi nghe. Nhưng khi đọc gió lẽ, đọc hết cả truyện tôi mới thấy hết cái nỗi buồn của nó. Các nhân vật được Em gọi tên theo cách em cảm nhận: anh tìm nội, ông buồn...nghe sao mà chua chát. Gió lẻ kể ra biết bao nhiêu phận người, tốt có, xấu có, giả tạo có. Truyện phơi lên tất thảy những thế sự cuộc đời, làm cho người đọc cảm thấy chạnh lòng. 9 câu truyện khác cũng buồn như vậy. Cô Tư toàn viết truyện buồn không hay sao ấy, những nỗi buồn chạm sâu vào tim mỗi người. Nói chung quyển truyện này hay. Yêu cô Tư!
4
84364
2015-08-14 10:53:50
--------------------------
251567
10860
Sau khi đọc các tác phẩm của Nguyễn Ngọc Tư mình rất thích giọng văn của tác giả này. Những người viết truyện thường lồng vào đó những tình huống gay cấn, lồng vào những câu chuyện bi hài kịch, bộ mới thu hút nhưng truyện của, Nguyễn Ngọc Tư không cần những thứ đó cũng thu hút được người đọc bởi chính giọng văn môc mạc, nhẹ nhàng và đi thẳng vào tâm can người đọc. Hơn thế nữa càng đọc lại càng thấm thía, có thể nói Gió Lẻ là một tác phẩm khá hay của Nguyễn Ngọc Tư
4
471112
2015-08-02 22:45:05
--------------------------
245979
10860
"Gió lẻ và 9 câu chuyện khác" vẫn là một tác phẩm rất "Nguyễn Ngọc Tư". Nhẹ nhàng, giản dị, không cầu kì hoa mỹ nhưng vẫn đầy ý nghĩa, đầy chiêm nghiệm. Cô như hóa thân vào từng nhân vật, sống đời sống của họ, hiểu họ và rồi kể cho chúng ta nghe những câu chuyện của họ một cách chân thực nhất, sống động nhất. gấp trang sách lại, ta như vẫn còn nhìn thấy những số phận ấy, những con người ấy ở đâu đó quanh đây, giữa cuộc sống bộn bề này. Đôi khi ta cũng cần sống chậm lại, suy nghĩ nhiều hơn, chiêm nghiệm nhiều hơn về cuộc sống này và cũng về chính bản thân ta.
5
353867
2015-07-29 17:25:00
--------------------------
206984
10860
Mình là độc giả trung thành của Nguyễn Ngọc Tư. "Gió lẻ" là một trong số những cuốn sách của Chị mà mình tâm đắc nhất. Mỗi câu chuyện đều mang đến cho mình nhiều cảm xúc, nhiều dư vị của cuộc sống. Đặc biệt với người đã quen với cuộc sống thành thị xô bồ, hiện đại như mình thì những câu chuyện của Chị giúp mình cân bằng lại cuộc sống, cảm nhận được những tình cảm trân quý, giản dị.
Giọng văn nhẹ nhàng, sâu lắng, sống động, "Gió lẻ" rất phù hợp là quyển sách gối đầu giường cho mọi người sau 1 ngày làm việc mệt mỏi!
5
600081
2015-06-11 10:14:34
--------------------------
204727
10860
Gió lẻ, và những câu chuyện khác của Nguyễn Ngọc Tư, mỗi khi kết thúc, là mỗi khi bắt đầu.
Tại sao tôi nói như vậy?
Phải chăng, vì cái kết mở, sâu thăm thẳm và cứ gợi bao suy nghĩ cho người đọc chăng?

Trong lí luận văn học, người ta gọi đó là Tiếp nhận văn học: Đời sống của tác phẩm văn học thật sự. sống động và đầy ấn tượng trong tâm hồn của độc giả.
Cứ không thôi suy nghĩ, rồi vẽ nên viễn cảnh cho nhân vật mà chị Tư để lại. 
Tôi nghĩ, sức sống của tác phẩm của chị Tự nằm ở đó.
5
323143
2015-06-04 19:58:26
--------------------------
141908
10860
Có lẽ bởi vì mình thích đọc những câu chuyện có kết, nên những truyện ngắn trong Gió lẻ đều có điểm chung là kết thúc mở - đặc điểm của Nguyễn Ngọc Tư, khiến mình không hứng thú lắm. 
Truyện nào cũng có chiều sâu, thông qua hình thức kể chuyện và tình tiết rất  bình dị. Tuy vậy màu sắc truyện có phần u ám, đen tối, đều đề cập đến những mặt tối xám, hôi hám của xã hội. Truyện không lên án ai, không ưu ái đề cao nét đẹp của ai, chỉ tập trung vạch mặt những gì xấu xa nhất, đau đớn nhất len lỏi vào cuộc đời con người, rất đỗi thực, cũng rất gay gắt: nỗi cô đơn không lối thoát trong "Sầu trên đỉnh Puvan"; gia đình li tán và sự vô tâm của cha mẹ sau khi ly hôn với con cái, cùng trái tim rách rướm đến nứt toác của Sói cùng con ma không tên trong "Ấu thơ tươi đẹp"; tình thầm của San và cuộc sống lắm tiền không tình của ông chủ trong "Tình thầm"; hình tượng người cha đầy tự trọng và tự ái trong "Chuồn chuồn đạp nước";v.v.
Bìa sách không đẹp. Những bức vẽ bên trong nhìn phù hợp với nội dung cuốn sách nhưng khiến người xem nổi da gà (cứ lật đến trang vẽ là mình phải lướt qua ngay).
Có lẽ ai muốn thấm được chất của Nguyễn Ngọc Tư nên lựa chọn một cuốn truyện dài của cô ấy thì hơn.
4
71319
2014-12-17 00:24:55
--------------------------
141160
10860
Đến với những tác phẩm của Nguyễn Ngọc Tư, bạn như trút hết mọi gánh nặng và những bộn bề lo toan của cuộc sống, tìm lại chính mình, nhìn lại chính mình giữa dòng đời tấp nập. Trước đây, tôi đã từng nghĩ tình yêu phải như trong những bộ phim Hàn Quốc đình đám: hào nhoáng, lãng mạn, choáng ngợp, ồn ào nhưng khi đọc những tác phẩm của Nguyễn Ngọc Tư (cánh đồng bất tận, gió lẻ, ngọn đèn không tắt,....) tôi mới dần cảm nhận một cách chân thực về cuộc sống. Tình yêu không cần ồn ào, vồn vã, chỉ bình thường thôi nhưng đủ lớn, đủ vĩ đại, đủ để giúp con người ta sống tiếp dù cho cuộc đời có tàn nhẫn và bụi bặm thế nào đi chăng nữa.
Con người sống với nhau, đến với nhau bằng tấm lòng chân thật là đã quý lắm rồi!
Tôi rất thích cách viết văn và cách nhìn về cuộc sống của tác giả: bình dị, sâu sắc và đặc biệt đậm chất Nam Bộ. Cái hay nữa là ở chỗ: càng đọc đi đọc lại nhiều lần bạn càng hiểu và càng phát hiện trong mình có nhiều cảm xúc và chiêm nghiệm mới trỗi dậy!
5
381380
2014-12-14 09:35:29
--------------------------
134623
10860
Mình đã đọc tác phẩm Sông ,rất tâm đắc về giọng văn của cô Nguyễn Ngọc Tư nên quyết định đầu tư vào các tác phẩm khác của cô.Quả là chẳng tiếc đồng tiền bỏ ra khi nhận lại được những sự chiêm nghiệm sâu sắc trong từng câu chuyện của cô.Và mình tâm đắc nhất là câu chuyện Sầu Trên Đỉnh Puvan.Phải nói câu chuyện cho ta thấy 1 sự thật là sống phải có 1 hy vọng ,để ta có thể sống tiếp dù cuộc sống có như thế nào, và nhân vật Vĩnh đã phải kết thúc cuộc đời mình trên đỉnh Puvan sau khi nhìn thấy bông Sầu nở vì nhận ra mình chẳng còn mục đích nào để sống.Có thể đó là 1 lời nhắc nhở đến mọi người về mục đích và giá trị của cuộc sống này!
5
378555
2014-11-10 14:32:57
--------------------------
131716
10860
Ngay từ hồi lớp 9, quyển đầu tiên mình đọc của Nguyễn Ngọc Tư là "Cánh đồng bất tận" mình đã thấy rất hay rồi. Từng truyện ngắn trong quyển sách này đều là chắt lọc tinh túy nhất của chị Tư, chưa tới mức siêu kinh điển nhưng lời văn sâu sắc, đều giản dị, mộc mạc và buồn kinh khủng. Chín truyện ngắn với chín cốt truyện độc lập nhưng không nhân vật nào thoát khỏi số phận buồn.

Buối tối cứ ngồi đọc sách này mà giật mình không ít, cứ nghĩ tới khi tình huống của nhân vật chính trong truyện ập đổ xuống đầu mình một ngày nào đó, một kết cục không thể tránh được, mình lại vừa buồn vừa sợ. Nhưng cuối cùng mọi thứ cũng kết thúc.
5
418158
2014-10-27 07:31:46
--------------------------
126725
10860
Mình chỉ mới đọc được 4 tuyển tập truyện ngắn của Nguyễn Ngọc Tư thôi, nhưng có một sự khác biệt khá rõ giữa 2 tác phẩm đầu tay và 2 tác phẩm sau này của chị. "Gió lẻ" đánh dấu một sự trưởng thành trong giọng văn và suy nghĩ của Tư, một độ "chín" mà với nó chị có thể viết ra những câu văn cắm sâu cắm đau vào trái tim người đọc. Khó để tìm ra một chủ đề chung cho tập truyện này, nhưng với mình đó là lời tựa của truyện ngắn cuối cùng:

"Tại sao người ta không thấy nhìn thấy mình khi mình còn sống?"

Không gì vỡ nát tâm can bằng việc mình là vô hình trước mặt những người thân yêu nhất. Đau thương quá, bi phẫn quá, tuyệt vọng quá, có còn gì trên đời này cho tôi nữa không? Giờ mới thấm thía, may mắn nhất là vẫn giữ được hi vọng, bao nhiêu gian khổ rồi cũng sẽ vượt qua thôi.

Tất cả truyện ngắn trong đây đều hay (trừ "Chuồn chuồn đạp nước" và "Tình thầm" mình đánh giá là hơi bị "nhẹ đô"), đặc biệt là bộ ba truyện liên tiếp "Sầu trên đỉnh Puvan", "Ấu thơ tươi đẹp" và "Núi lở" với những nghiệt duyên không thể nói là "thích" mà phải là "bị ám ảnh". Đã khóc không dứt sau khi đọc xong "Núi lở", tại vì ác quá mà, ác gì mà ác dữ thần! Cái ông lão già xọm, lụ khụ run rẩy, luôn khổ tâm đau đáu không yên, bị bỏ rơi chết chìm giữa hàng vạn đá núi kia là ba ruột của anh mà trời ơi?
5
96489
2014-09-20 00:48:56
--------------------------
123466
10860
Nói về 9 câu chuyện đầu tiên, 9 câu chuyện với 9 cách nhìn về cuộc sống khác nhau. Không trùng lập trong cách tư duy, cách kể chuyện, Nguyễn Ngọc Tư làm mình cứ đau xót cùng nhân vật. Trong 9 câu chuyện này thì Vết Chim Trời là truyện mà mình thích nhất. Hoàn toàn mộc mạc, gần gũi với cuộc sống. Câu chuyện bắt đầu với sự nghiệt ngã từ một lời nói bâng quơ của một người bà lúc nửa tỉnh nửa mê. Từ câu nói ấy mà cuộc sống của mọi thành viên trong nhà như đảo lộn, sống mà không hẳn là sống. Cứ lầm lũi, cứ hoài niệm về quá khứ mà nỗi đau hiện tại cứ âm ỉ.
Còn với Gió lẻ, đó là một câu chuyện hoàn toàn khác, thoát hẳn Cánh Đồng Bất Tận trước đây. Gió lẻ kể theo mạch dẫn đầy lạ lẫm. Một cô gái không nói tiếng người mà dùng tiếng của "loài vật", một anh chàng phụ xe muốn yêu nhưng không được đáp nhận, một tài xế xe cứ lầm lũi, chạy trốn tình yêu nhưng cuối cùng lại vướng vào tình yêu mà ông không mong đợi. Tất cả đều tự nhiên, đáng trân trọng.
Và câu hỏi mà GIó lẽ mang tới cũng đầy chua chát "Tại sao ta không nhìn thấy mình lúc còn sống?"
5
303773
2014-09-01 19:48:02
--------------------------
115025
10860
Truyện của Nguyễn Ngọc Tư thể hiện luôn giản dị, mộc mạc nhưng triết lý sâu xa. Đó là những câu chuyện buồn, nỗi buồn nhẹ nhàng thôi mà in sâu trong tim. Là những nỗi buồn nhỏ bé nhưng lại sâu sắc. Vì con người sinh ra là để làm đau nhau. 
Tiếng nói của con người quả có sức mạnh, sức mạnh đủ để giết chết một con người. Nhưng lời nói bâng quơ của Nương đã giết mẹ cô trong Cánh đồng bất tận, như mẹ của cô gái đã chết vì lời nói của ba cô trong Gió lẻ. 
Những nhân vật của Nguyễn Ngọc Tư cứ như bị cuốn vào vũng bùn của nỗi đau. Càng cố giẫy giụa, càng lún sâu, cuối cùng thì chết ngập vào trong đó.
4
309614
2014-06-21 20:35:22
--------------------------
79140
10860
Tôi đọc "Gió lẻ và 9 câu chuyện khác" của Nguyễn Ngọc Tư trong tâm thế đã đọc xong quyển "Cánh đồng bất tận" đã quá sức ám ảnh. Tôi trông mong mình sẽ tìm lại những cảm giác đó trong tập truyện ngắn này, và khi đọc xong còn lại trong tôi chỉ là cảm giác hơi hụt hẫng và khá trống rỗng. Không phải là chị Tư viết quyển này dở mà chắc vì tôi đã hy vọng quá nhiều nên tôi mới có cảm giác như thế. Vẫn với cách viết đó, dung dị, đời thường nhưng không tầm thường nhưng dường như trong tập truyện này NNT muốn viết khác đi, khác với phong cách viết mọi khi của chị nên tôi không thể cảm hết được. Truyện ngắn "Gió lẻ" chị viết đan xen nhiều tuyến nhân vật đôi lúc làm tôi hơi rối cả lên và đọc lại lần thứ 2 tôi mới hiểu hết phần nào câu chuyện. Những thứ phức tạp rối ren dường như không phù hợp với chị Tư cho lắm. Dù sao tôi vẫn luôn ủng hộ chị!
3
46905
2013-06-05 16:19:31
--------------------------
74572
10860
"Gió lẻ và 9 câu chuyện khác" của Nguyễn Ngọc Tư là một tập truyện rất hay và lôi cuốn. Mình đã đọc nó trong 3 ngày, nhưng thực sự cuốn sách đã ám ảnh mình nhiều tuần, nhiều tháng sau đó. Mỗi câu chuyện, một nhân vật, một số phận, một cuộc đời, đan xen vào nhau bằng chất liệu tinh tế và sâu sắc ngôn từ. Nguyễn Ngọc Tư đã triển khai những ý tưởng của mình một cách xuất sắc, tạo nên những câu chuyện chân thực và đẹp đẽ, nhưng đồng thời cũng gợi nên những nỗi buồn khó lòng vơi bớt. Mình thực sự thích cách tác giả miêu tả mỗi nhân vật trong những câu chuyện của mình, có gì đó vừa giản đơn vừa gai góc.
Đây là một tập sách không nên bỏ lỡ cho những ai đã, đang và sẽ yêu văn Nguyễn Ngọc Tư.
5
109067
2013-05-14 12:21:49
--------------------------
73783
10860
“Tại sao người ta không nhìn thấy mình khi mình còn sống” Bằng lời đề từ ấy, tác giả đã báo hiệu ngay sắc diện của truyện ngắn gío lẻ và các truyện khác. Nó như  sợi chỉ đỏ xuyên suốt toàn bộ văn nghiệp Nguyễn Ngọc Tư.
Mình ở đây có thể là chính nhân vật cô gái, “người ta không nhìn thấy mình”, tức là những người xung quanh cô không nhìn thấy cô, cô không tồn tại, cô biết sự thật nhưng không được nói, cô bị Ông Tám Nhơn Đạo “luồn tay qua áo”, cô kêu cứu những vợ ông ấy không nghe, có lẻ bà tường đó không phải là tiếng người.Đó là việc người đời đã khước từ cô, gia đình khước từ cô, để cô sống mà như chết, cô không có cái quyền của con người , quyền được giúp đỡ và quyền được vạch trần sự thật.
“Người ta không nhìn thấy mình” cũng có thể là con người không nhìn thấy được chính bản thân mình, cứ sống theo những thói đạo đức giả dối mà quên đi việc sống thật với bản thân mình. Họ không làm chủ được số phận của mình mà bị đưa đẩy theo những mưu mô, ham muốn của kẻ khác, để rồi đánh mất chính bản thân mình.
Một cách hiểu nữa có thể là : “nhìn thấy mình” đó là mục đích, là mong muốn , là khát khao và cũng là một tra vấn thường trực về bộ mặt và hình như cả về sự hiện hữu của con người, về ý nghĩa sống của con người. 
“Tại sao người ta không nhìn thấy mình khi mình còn sống?”, câu hỏi đặt ra vấn đề là đi tìm gái trị đích thực cho bản thân mỗi con người và chắc chắn rằng quá trình tìm kiếm đó không dễ dàng, để rồi nó chỉ được thốt lên khi con người đã trở thành “con ma”. 
4
72874
2013-05-09 23:11:17
--------------------------
49863
10860
"Gió lẻ và chín câu chuyện khác" là tác phẩm thứ hai mình đọc của nhà văn Nguyễn Ngọc Tư. Dù đọc đã lâu nhưng mỗi lần nhớ về nó mình lại có rất nhiều cảm xúc khác nhau. Nguyễn Ngọc Tư tiếp tục đem đến những câu chuyện nhỏ, đời thường và bình dị trong mọi khía cạnh. Nhưng chính sự đơn giản đó lại có một sức hút mạnh mẽ, lại có thể chạm vào mọi đáy sâu cảm xúc trong lòng mỗi người. Mỗi truyện đều nhẹ nhàng, buồn khôn xiết, chơi vơi như những dòng nước chảy xiết, tạo nên một dòng hồi tưởng xuyên suốt và mạnh mẽ. Trong cái khác lạ của những con chữ, Nguyễn Ngọc Tư đã xuất sắc tái hiện những cuộc đời, những số phận khác nhau, đã vẽ nên một bức tranh chân thực nhất về cuộc đời mỗi con người.
5
20073
2012-12-10 15:55:22
--------------------------
31579
10860
Gió lẻ và 9 câu chuyện khác là một tập truyện mang nỗi buồn như nỗi buồn sông nước, nên nó cũng mênh mang, vời vợi, khó mà nắm bắt và thấu cảm hết được. Kiểu buồn ấy cứ lãng đãng mà thẳm suốt theo từng trang sách, để rồi mọi thứ đều hiện lên trong bức màn mông lung, hư ảo. Không gay gắt, nhưng thật nhiều kiểu dằn vặt, thật nhiều cái đau đáu. Lần trở lại này, những nhân vật của chị, nhất là trong Gió Lẻ, lại mang nhiều nét dị biệt hơn nữa, có lẽ vì thế họ không thoát nổi nỗi buồn, mà ngày càng chìm lút trong nó, đến tận lúc không thể cứu lấy chính mình. Đọc mà thấy tê lòng quá.
4
27558
2012-06-27 12:03:33
--------------------------
29183
10860
Không có độ trong trẻo, gãy gọn, lắng đọng và thấm thía bằng tập Ngọn đèn không tắt, không có sự gay gắt và buồn bã khắc khoải được như Cánh đồng bất tận, tập Gió lẻ có lẽ là 1 trong những tập truyện ngắn nhạt nhất của Nguyễn Ngọc Tư. Nhưng bù lại, nó tập hợp được nhiều nét hay trong ngòi bút của NNT nhất (có lẽ vì thế mà nó không nghiêng hẳn về cái nào). Sách là 1 tập hợp những nỗi buồn, cái buồn xa xăm, mông lung của những người rày đây mai đó, không gắn mình trên bất kì mảnh đất nào. Cuộc sống của họ chơi vơi và vô định giống như bước chân của họ vậy. Và, mình khá bất mãn với NNT, khi chị cứ để nhân vật của mình trôi nổi vật vờ hay chìm nghỉm trong kiếp sống của mình mãi, cho đến khi có 1 kết thúc buồn, mà chẳng có lấy 1 sự vùng lên tự cứu rỗi cho chính mình. Có lẽ vì thế mà hầu như mỗi lần đọc truyện của chị lại có 1 cảm giác ngột ngạt khó bứt ra được.
4
26136
2012-06-03 18:02:04
--------------------------
26164
10860
So với các tập truyện ngắn khác của Nguyễn Ngọc Tư, tập này làm mình thất vọng hơn cả. Mình đã trông chờ nhiều hơn khi mình nghe đến cái tên “Gió Lẻ”. Theo cảm nhận của mình, tập truyện này có sự trau chuốt hơn trong lối viết, nhưng mình nghi ngờ 1 phần chính vì lẽ đó mà nó thiếu đi cái hồn vẫn thường thấy trong truyện của NNT. 

Có một khuynh hướng viết truyện của NNT nổi bật trong tập truyện này hơn bất kì tập truyện nào khác. Đó là sự sự tách bạch nhức nhối giữa đời và người, con người sống giữa cuộc đời nhưng lại không muốn sống trong nó. Họ bọc mình trong một quả bóng kiểu như tiểu vũ trụ của riêng họ, rồi mỗi cái động chạm nhẹ, vô tâm của người đời làm tiểu vũ trụ của họ rúm ró lại, ép họ đến ngạt thở mới thôi. Và như vậy, con người đã tự mình đóng mình sống đời khắc kỷ, tự mình đẩy mình ngày càng rời xa cuộc sống, ép mình vào một vũng lầy tù đọng. Vậy nên, tâm trạng mình khi đọc hết những tác phẩm trong đây là 1 nỗi bức bối khó tả, vì không có niềm hy vọng gì cả, không phải cuộc đời không tiếp nhận con người, mà ngược lại, bản thân họ không tiếp nhận cuộc đời. 

Sách đẹp, không bị lỗi biên tập nhưng có vài lỗi chính tả.
3
5548
2012-05-05 20:48:25
--------------------------
20913
10860
Đối với người mà tôi đặc biệt ngưỡng mộ và trân trọng, tôi thường không có gì nhiều để nói. Và Nguyễn Ngọc Tư là một trong những người như vậy. 
Đọc tập truyện này, tôi mất thời gian khá lâu, dù truyện không dài, và cũng không thiếu hấp dẫn. Nhưng tôi thật sự không thể nhanh chóng mà đọc từ truyện này qua truyện khác, vì từng truyện, từng truyện, đều nặng nề quá, đau lòng quá, ám ảnh quá. Cứ như người bệnh, tôi đi từ từ, từng trang từng trang một, và đưa vào đầu trọn những tâm tư nặng trĩu của từng mảnh đời, cảnh đời chồng chéo nhau. Ai cũng sống, sao phải sống khổ như vậy, sao lại phải hành nhau đến khổ như vậy? 
Đọc xong thì tôi cứ băn khoăn mãi một câu hỏi: trên đời này, sống đậm tình liệu có phải là không nên không? 
5
19019
2012-03-06 23:26:34
--------------------------
17567
10860
Đọc "Gió lẻ" như được làm quen một Nguyễn Ngọc Tư khác, không phải một Nguyễn Ngọc Tư với tâm hồn chất phác, giọng văn nhẹ nhàng mà là môt NNT mãnh liệt, đầy trăn trở với những mỗi quan hệ của cuộc đời, những mối quan hệ kèm theo những hệ lụy, những kết cuộc đau xót mà mỗi lần đọc xong một truyên người ta lại đắng lòng. Ngôn từ NNT sử dụng trong "Gió lẻ" chân thục đến trần trụi thể hiện được cái day dứt trong suy nghĩ về những mối quan hệ thể xác, những cái chết, những cuộc đời đầy bi thảm của từng nhân vật. Từng câu chuyện như đưa người đọc vào mê hồn trận giữa cái hiện thực với cái hư ảo, giữa thể xác và tâm hồn, giữa hiện tại và quá khứ và một tương lai mù mit. 
Nếu bạn lần đầu đọc truyện của NNT qua "Gió lẻ" có lẽ sẽ bị sock vì cốt truyện, vì lối hành văn, vì những cái kết ngỡ ngàng làm cho bạn khó thoát ra khỏi câu chuyện ngay được., nhưng đã quen với NNT thì bạn sẽ thấy đây là một con người khác của chị, một tâm hồn mạnh mẽ  như muốn nổi loạn, muốn gào lên với cuộc đời, muốn đánh thức những con người đang bị thoái hóa, trái tim đang dần hóa đá. Có thể coi đây là một sự trưởng thành hoặc giả là một sự thay đổi, được hay dở, chấp nhận hay không chấp nhận, tùy vào mỗi người, nhưng riêng tôi, đó mới chính là NNT mà tôi biết (qua những tập truyện) : dám thử những cái mới nhưng không bỏ cái cũ.
5
21769
2012-01-11 00:29:11
--------------------------
