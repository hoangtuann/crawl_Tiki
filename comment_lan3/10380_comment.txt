465892
10380
Bạn có muốn nuôi 1 chú cún con không? Cuốn sách này cung cấp khá đầy đủ các kiến thức để cho các nhà ở thành phố có thể chăm sóc một chú cún con cũng như kiến thức về việc một chú cún con được ra đời và lớn lên như thế nào... Đây là một cuốn sách có nội dung về khám phá, với nhiều kiến thức khoa học lý thú, đơn giản lại có nhiều tranh minh họa sinh động với màu sắc đẹp, hình ảnh đáng yêu rất hợp với các bé. Hơn nữa sách được in trên giấy bóng, đẹp, nhưng chữ hơi bé nên bạn nào cho bé đọc thì nhớ để ý đủ ánh sáng để bé không bị lóa mắt nhé. 
5
1066198
2016-07-01 15:32:08
--------------------------
258482
10380
Tôi rất thích những bộ sách như thế này, trình bày theo kiểu truyện tranh nhưng nội dung lại là khoa học, giúp bé làm quen với khoa học từ sớm- những câu chuyện có thật chứ không hề hư cấu. Nội dung kiến thức cũng vừa đủ để bé có thể hiểu được. Bé nhà tôi rất thích, nghe như nghe truyện tranh vậy, và bây giờ có thể kể được 1 vài nội dung trong sách. Duy chỉ có điều tôi thấy tranh chưa đẹp lắm, màu sắc thì ok nhưng tranh vẽ chưa được nét, ví dụ như quyển "Cún con" ban đầu bé nhà tôi cứ bảo là "Bạn bò", trông ko giống lắm thì phải.
4
298411
2015-08-08 16:08:56
--------------------------
