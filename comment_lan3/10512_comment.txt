489670
10512
Trước khi mua cuốn sách này mình đã rất trông mong nhưng sau khi nhận được cuốn sách thì mink rất thất vọng chất liệu giấy mỏng , ố vàng mấy trang cuối còn bị đóng lệch nhăn nhúm chữ in thì lại hơi mờ ... tuy nhiên nếu xét về phần nội dung thì cũng có thể là tạm ổn nội dung nói về hôn nhân của các vị vua rất thú vị.Mình chỉ mong Tiki có thể cải thiện lại chất lượng sách thì ok !!!
2
502066
2016-11-09 10:27:58
--------------------------
308350
10512
Sách viết về hôn nhân và tình yêu của một số danh nhân nổi tiếng trên thế giới. Những câu chuyện trong sách mình thấy các truyện viết về câu chuyện về của các vua hấp dẫn hơn những nhân vật khác như chuyện về Hiếu Văn Đế, Tùy Văn Đế, Càn Long khá là lôi cuốn. Tuy nhiên các chuyện này lại viết khá ngắn nếu được viết dài hơn sẽ càng hay hơn nữa. Ngoài ra tên của các danh nhân lại được viết theo lối phiên âm ra đọc vào khó chịu ghê như Êlidabet. Sách lại in trên giấy mỏng nên nhiều trang thấy cả mờ mờ chữ bên trang kia . Mong rằng lần tái bản sau sách sẽ được cải thiện. 
4
43129
2015-09-18 16:59:45
--------------------------
