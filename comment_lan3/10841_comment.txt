491868
10841
Một ngày nọ, một người bạn tốt của tôi chia sẻ với tôi về một quyển sách đã mang đến sự chữa lành cho cô ấy, và tôi được cho mượn. Thoạt đầu, phải rất kiên nhẫn để có thể cảm được từng lời văn của tác giả và cả dịch giả, phải hết sức để thả cho trí tưởng tượng của mình lướt kịp với những mạch văn trầm, bổng. Và rồi, tôi thật sự bị cuốn hút, có những đêm nằm nghiền ngẫm đến tận 2-3 giờ sáng vì không thể buông xuống, có điều gì đó chạm vào sâu thẳm trái tim tôi. Nếu chỉ nói về sách thì thật sự chưa có quyển sách nào lấy đi của tôi nhiều nước mắt và lấy đi cả những nỗi đau sâu kín như "THE SHAKE - NƠI TRÁI TIM HỘI NGỘ". Với tôi, đây quả là một quyển sách có thể mang người đọc đến với sự chữa lành diệu kỳ - một quyển sách đáng để đọc! Cảm ơn tác giả, dịch giả và những người đã góp phần mang quyển sách này đến với mọi người. Tôi không viết để góp phần quảng cáo cho Tiki, nhưng đây là trải nghiệm thật của chính tôi - dù đến giờ tôi vẫn chưa có cơ hội sở hữu quyển sách này cho riêng mình. Tôi là 1 khách hàng của Tiki và tôi gửi lời nhận xét khách quan từ email tdbaokhanh@gmail.com. Bạn nên thử trải nghiệm quyển sách tuyệt vời này! Chân thành!
5
592101
2016-11-21 15:36:02
--------------------------
468063
10841
Mình thích gọi quyển sách như cái tên thật của nó. "Căn chòi" là nơi người cha đối diện với nỗi đau, mất mát, sự dày vò. Đôi khi tất cả những gì chúng ta cần là chạm tới tận cùng của nỗi đau, khoét sâu vào từng ngõ ngách của vết thương để được chữa lành. Bạn không thể nào hết bệnh cho đến khi bạn chấp nhận mình đang bệnh và điều trị nó. Giữa một xã hội đầy dẫy những tổn thương, cay đắng, quyển sách như một liều thuốc làm êm dịu và rịt lành chúng để người ta có cớ để tin và hy vọng vào một tương lai tươi sáng hơn. Mình đã đọc quyển sách này 5 lần rồi, cũng đã mua gần 10 quyển để tặng và thật sự chưa bao giờ chán nó. Không phải ngẫu nhiên mà 1 quyển sách lại đứng đầu bảng xếp hạng trong 70 tuần và có hơn 10 triệu bản được bán. Hãy thử 1 lần tìm đọc, cảm nhận và nếu có thể đây cũng là lúc bạn đối diện với nỗi đau của mình để được chữa lành. Người ta không thể đổ nước vào một cái bình đã đóng, cũng như vậy, nếu bạn mở lòng đọc quyển sách này trong tinh thần tìm kiếm sự chữa lành cũng như hiểu hơn về chính mình, tôi tin sau khi gấp quyển sách lại, bạn sẽ nhìn cuộc đời này bằng cách khác đi, tích cực hơn và yêu thương hơn...
Suesan Nguyen
5
312809
2016-07-04 17:45:10
--------------------------
465903
10841
Mình được một người anh giới thiệu cho quyển sách này mùa giáng sinh. Ban đầu nhìn thấy tiêu đề mình đã thấy thích thú. Mình đã đọc quyển sách này hai lần, và nó thật sự rất ý nghĩa với mình, cuốn sách làm thay đổi cái nhìn cứng nhắc của mình về Thiên Chúa và cách mà Thiên Chúa vận hành, cách mà Thiên Chúa đối xử với mỗi người. Ngài thật sự không phân biệt người đạo Chúa hay đạo Phật, ai với Ngài đều là những người con đáng được yêu thương và đối xử như nhau bằng tình yêu như nhau. Qua cuốn sách này mình cảm nhận sự gần gũi và thực hữu hơn của Thiên Chúa và Chúa luôn bên bạn mặc dù nỗi đau lớn như thế nào và mọi thụ đều nằm trong ý muốn tốt đẹp của Ngài. Nếu bạn đang có nỗi đau hay một sự cô đơn nào mình nghĩ đây là cuốn sách bạn nên đọc
5
414317
2016-07-01 15:43:33
--------------------------
459164
10841
Lúc mình định mua thì sách đã hết hàng.Rất may là 1 ngày sau khi mình gưti yêu cầu nhập hàng,Tiki đã có ngay-quá nhanh quá nguy hiểm luôn.Cuốn sách tiy nhiên không hay như nình mong đợi.Phần đầu hơi rườm rà,và trong suốt câu chuyện,người cha đã trải qua nhiều trải nghiệm mà đối với mình là quá hoang đường.Hơn nữa những lời nói và bài học của Chúa trong cuốn sách cũng làm mình thấy khó hiểu nhiều phần.Cuốn sách chứa đựng một ý nghĩa lớn và rất nhân văn.Nhưng rất tiếc là không biết do cách viết hay do mình còn chưa đủ trình mà mình thấy khó để cảm nhận.
4
395480
2016-06-25 20:59:42
--------------------------
345500
10841
Bạn có thể phải nắm qua một chút kiến thức về đạo Chúa trước khi đọc cuốn sách này. Nhưng tin tôi đi, nếu bạn là người hiểu sâu sa ý nghĩa về tôn giáo (Phật/Chúa/Hồi giáo) thì bạn sẽ rất thích thú cuốn sách này. 
Tác giả gãi đúng chỗ ngứa của những người đang đau khổ, thất vọng, mất niềm tin vào tôn giáo. Vì họ cho rằng các đấng tối cao ấy (Chúa/Phật/Allah) phải cứu giúp con người, chứ không phải đem lại đau khổ cho con người. Nhưng có đúng như vậy không? Ý nghĩa thật sự của tôn giáo là gì?
Nếu bạn không phải là 1 con chiên ngoan đạo, bạn hoàn toàn có quyền bỏ đi khoảng 4-5 chương giữa cuốn sách. Nhưng điều đó cũng không làm giảm nhiều giá trị của ý nghĩa mà nó đem lại.
Hãy đọc nó, bạn sẽ cảm thấy hạnh phúc hơn với mọi thứ đến với bạn. 
4
84027
2015-11-30 18:23:57
--------------------------
228373
10841
Mới đầu đọc sách thực sự rất hấp dẫn, rất tò mò và đau đớn với gia đình Mack về nỗi đau của họ. Nhất là lúc Mack nhận được thư của Chúa và quyết định quay trở về nơi gợi cho anh những kỷ niệm đau thương nhất, nơi đó thực sự đã xuất hiện phép màu cổ tích... Nhưng thực sự là khi Mack gặp mặt 3 vị Chúa, cuộc đối thoại của họ khiến mình khó hiểu dần đều, nó quá miên man và có gì đó triết lý triết luận. Có lẽ những người theo Đạo đọc cuốn sách mới cảm nhận được hết ý nghĩa của nó. Cuốn sách thực sự không hợp với mình, mình không thể đọc hết cả cuốn sách và thực sự thấy thất vọng.
3
164722
2015-07-15 13:04:05
--------------------------
212856
10841
Đoạn đầu hơi khó hiểu. Nhìn chung là nội dung dàn trải. Các bạn có đạo đọc sẽ dễ hiểu khúc cuối hơn. Đại ý là Chúa luôn dõi theo chúng ta, dù cho chúng ta có trong nghịch cảnh, dù rằng chúng ta đã mất niềm tin nơi Ngài, trong đau khổ và không hiểu tại sao người thân của chúng ta phải ra đi nghiệt ngã, thảm khốc như thế. Khúc cuối nhanh nhưng đầy hình ảnh mô tả Thiên Đường, sẽ phù hợp cho các bạn mất đi người thân, hoang mang không biết họ sẽ như thế nào, về đâu, nơi đó có tốt lành như lời Ngài hứa không. để mà vững tin và bước tiếp. Và những gì chúng ta chịu đựng (thảm cảnh nơi thế gian, con gái bị thảm sát, mất xác) không phải là điều đáng sợ, nếu chúng ta biết được kết quả của bé nơi Thiên Đàng... Và cũng hiểu hơn rằng Ngài thật sự là người Bạn, thật sự có lắng nghe và suy nghĩ cho bạn, luôn luôn vì bạn.
3
482705
2015-06-22 22:57:00
--------------------------
181147
10841
Đoạn đầu của cuốn sách làm mình khá thích thú khi kể về quá khứ của Mack, tình cảm của anh dành cho vợ và các con. Nhưng từ sau đoạn Missy bị bắt cóc và Papa gửi thư cho Mack là khiến mình thấy hoang mang rồi. Mình không theo Đạo nên không hề thuộc những câu chuyện trong kinh Thánh. Những đoạn nói chuyện với Chúa mình chỉ thấy khó hiểu và buồn ngủ. Cố gắng lắm mới đọc được hết quyển sách trong tình trạng ngán ngẩm. Tác giả đưa ra ý tưởng rất hay về lòng bao dung và việc vượt qua nỗi đau nhưng lại quá khiên cưỡng về tôn giáo khiến cho một người phi tôn giáo như mình hoàn toàn cảm thấy lạc ra khỏi mạch ý tưởng ban đầu. Thực sự khi gấp cuốn sách lại mình chẳng còn đọng lại được chút hào hứng nào của những trang sách đầu tiên nữa, chỉ thấy mơ hồ và khó hiểu mà thôi. Có lẽ cuốn sách này chỉ dành cho những người mộ đạo hay ít nhất cũng có theo đạo chứ không dành cho những người như mình.
1
154281
2015-04-11 01:51:52
--------------------------
173255
10841
Mình đã tưởng tượng cuốn sách này phải đem đến sự bất ngờ, xúc động và hay một cảm xúc thật đặc biệt nào đó. Nhưng sự thật là hoàn toàn không. Nói đúng hơn là mình không đồng cảm được với những gì được viết ra. Câu chuyện mang đậm màu sắc tôn giáo, đây là chủ đề chính và xuyên suốt tác phẩm, khiến cho phần tình cảm của người cha trở nên nhạt nhòa. Những chi tiết huyền ảo lạ kỳ dày đặc, điển hình là cuộc đối thoại với Chúa được kể một cách cứng nhắc, làm mất đi tính chân thực của câu chuyện. Nhưng vấn đề lớn nhất là cách tiếp cận chủ đề tôn giáo của tác giả: dài dòng, khó hiểu, và nhiều điểm quá phiến diện. Con người có thể tin tưởng, yêu kính Chúa, nhưng liệu có cần quá sùng bái đến mức cuồng tín, đến mức đặt Chúa lên trên tất cả như thế. Liệu mọi chuyện trong cuộc sống thực của con người có bị chi phối bởi một đấng tối cao nào đó, chứ không do bản thân chúng ta quyết định? Có quá nhiều điểm mình không đồng tình với tác giả, khiến càng đọc càng thấy nhàm chán và đứt mạch cảm xúc.
Với một người không theo một tôn giáo nào, tác phẩm này thực sự là khó hiểu và khó cảm nhận, vì không phải ai cũng có thể đặt niềm tin vào thần linh hay những lực lượng siêu nhiên mơ hồ nào đó. Nhưng nếu gạt vấn đề tôn giáo sang một bên, thì đây vẫn không phải là một tác phẩm hay, khi tác giả đã gò ép người đọc vào những lý thuyết cứng nhắc đến vậy.
1
109067
2015-03-25 20:37:59
--------------------------
49605
10841
Đúng như những lời nhận xét dành cho nó, cuốn sách này là tác phẩm có chủ đề về Giáng Sinh hấp dẫn nhất, giàu kịch tính nhất, bất ngờ nhất mà cũng sâu sắc nhất. Ngay từ đầu truyện, tác giả đã đem đến một tình huống độc đáo, mở đầu cho chuỗi những sư việc bí ẩn cứ liên tiếp diễn ra sau đó. Hành trình của người cha Markenzie là một cuộc hành trình đặc biệt, một cuộc tìm kiếm sự thật, giải mã những điều bí ẩn, tìm lại quá khứ trong nỗi đau mất Missy, đứa con út thân thương. Câu chuyện đầy kỳ diệu này giống như một phép màu, nó sẽ làm bạn hồi hộp, bất ngờ, xúc động và cả bật khóc. Một tác phẩm quá tuyệt vời để đọc trong bất cứ thời điểm nào!
5
20073
2012-12-08 19:26:34
--------------------------
18434
10841
Trong một cuộc du lịch với gia đình, Mack đã đánh mất thứ mà anh phải hối hận suốt đời - đó chính là cô con gái út của anh. Nhưng sự việc không chỉ dừng lại ở múc độ kinh khủng đó. Vài năm sau, anh đã nhận được bức thư của... chúa! Ngài hẹn anh đến nơi cô con gái anh bị mất tích... và từ đó, câu chuyện kì bí đã bắt đầu.
Vâng, đúng là kì bí thật khi một người đàn ông được gặp chúa. Thật bất ngờ và li kì, đực biệt là với giọng kể của W. P. Young. Ông đúng là một thiên tài về lối kể chuyện hài hước và không kếm phần hấp dẫn.
Hãy đọc, thưởng thức, và để trái tim bạn cùng hội ngộ...!
5
23953
2012-02-01 19:59:49
--------------------------
13178
10841
Đã bao giờ bạn tuyệt vọng vì tìm kiếm một ai đó chưa? Bạn hụt hẫng lần theo các dấu vết, rồi đau đớn nhận ra tất cả chỉ là vô vọng. Khi quyết định dừng lại không tìm kiếm nữa; nhưng, mỗi đêm nhắm mắt lại, giấc mơ mang đến cho bạn một manh mối mới. Nắm lấy nó, lần theo nó và đến một ngày, như tất cả đã dược ai sắp đặt từ trước, bạn phát hiện dường như mình đã thấy tất cả. không biết phải làm gì, đón nhận nó hay từ bỏ? Vì biết đâu khi đối mặt với nó, vết thương của bạn sẽ càng đau hơn trước. "Nơi trái tim hội ngộ" đã viết về một cuộc tranh đấu như thế của một người bố tuyệt vọng tìm kiếm cô con gái bé nhỏ. Mack - tên nhân vật - đã vật lộn với lý trí để quyết định đối mặt với những gì Thượng Đế sắp đặt từ trước cho mình, và cuộc hành trình đó trải dài trong gần 500 trang sách đã thay đổi con người anh, và mạnh hơn là có thể thay đổi cả trái tim bạn như chính anh đã như vậy. Gấp sách lại và niềm tin cùng đức tin sẽ quay về với bạn, làm dịu nhẹ cuộc sống của bạn. 
5
7150
2011-10-20 11:35:18
--------------------------
