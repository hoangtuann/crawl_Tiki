369699
10789
Câu chuyện vẫn tiếp diễn và hay nhưng thật sự khó để giữ câu chuyện nghẹt thở từ tập đầu cho đến tập cuối có vẻ càng về sau các nhân vật mới xuất hiện không ấn tượng như trước, các chi tiết về những thanh kiếm hay các truyền thuyết không ấn tượng, có lẽ vì sự bất ngờ của độc giả đối với việc các nhân vật lịch sử nổi tiếng bất tử không còn gây được ngạc nhiên như các tập trước.
Mình thấy việc so sánh bộ này với Harry Potter là khá khập khiễng vì mỗi bộ truyện có sự khác nhau về tất cả bối cảnh, nội dung và thế giới trong truyện mà tác giả tạo ra. Mình vẫn ủng hộ cả hai bộ truyện này.
4
306081
2016-01-16 11:36:43
--------------------------
354092
10789
Nội dung của truyện vẫn tiếp tục, mang theo những câu chuyện mới, những người bất tử mới đến với Josh và Sophie. Tuy vậy, không có nhiều lắm những tình tiết mới được khai thác. Mạch truyện diễn ra hơi chậm, gây mệt mỏi và chán nản với độc giả. Bìa truyện được thiết kế rất đẹp, và là điểm cộng lớn nhất của mình đối với bộ truyện này. Những biểu tượng, totem được thiết kế rất khéo và tạo sự đối xứng rất thú vị, mang lại sức hấp dẫn cho quyển sách. Nói chung, series sách này không gây được ấn tượng và tiếng vang lớn lắm, dù nó vốn có thể thành công hơn thế. Thật đáng tiếc.
4
710839
2015-12-17 12:18:51
--------------------------
345308
10789
Năm 2012, khi người bạn giới thiệu về quyển sách "the alchemist" của Paulo Coelho, tôi lại mua nhầm quyển, tôi lại mua nhầm quyển "nhà giả kim", quyển đầu tiên trong bộ sách này, và thế là dính với bộ này từ đó. Quyển mở đầu đọc hay bao nhiêu thì càng về sau càng mệt bấy nhiêu. Mạch truyện ở tập này thể hiện sự lủng củng và đuối tình tiết của tác giả. Có thể đây là quyển dở nhất trong cả bộ truyện "Bí mật về Nicholas Flamel bất tử" của tác giả. Tuy nhiên, một điểm cộng nhỏ là truyện này gắn kết các giả sử và hiện tại khá khéo léo.
3
270700
2015-11-30 11:07:33
--------------------------
304901
10789
Cuốn này mình mua hơi lâu rồi, bây giờ mới đăng nhận xét. Vì đọc thấy khong hấp dẫn nên không có hứng để đọc liên tiếp.Không biết là do câu chuyện kéo dài hay không mình thấy các tình tiết không còn hấp dẫn lắm, tuy vậy vẫn có những tình tiết gây cấn nhưng chứ đủ để lôi cuốn người đọc. Mình hơi bất ngờ về việc Jonh phản bội lại chị gái, hai chị em sinh đôi gắn bó thế cơ mà. Tập này có vẻ không ổn lắm so với các tập trước, mong rằng các tập sau sẽ có những biến chuyển và tình tiết hay hơn, hấp dẫn hơn nữa.
3
236171
2015-09-16 18:37:15
--------------------------
287803
10789
Tôi mua cuốn truyện này cũng lâu rồi, bây giờ mới quyết định viết nhận xét. Là tập thứ tư trong serri Bí mật của Nicholas Flamel bất tử Yêu nữ thật sự đáng đọc cho những ai đang theo bước cặp sinh đôi huyền thoại. Điều tạo nên sự gay cấn chính là chuyện Bóng Tối mất tích gây hoang mang cho người đọc về việc ai sẽ bảo vệ cặp sinh đôi. Thêm vào đó Nữ Phù Thuỷ đang dần yếu đi và Nhà giả kim cũng vậy. Mặt khác Jonh lại phản bội lại chị mình làm co bé sốc nặng. Vẫn giọng văn đó đôi lúc cũng gay nhàm chán vì miêu tả quá nhiều một chi tiết (đối với tôi cảm nhận như vậy)
4
189149
2015-09-03 01:02:36
--------------------------
273503
10789
Trong tập này Josh lại bị Dee dụ dỗ. Mình thấy hơi vô lí và thất vọng.  Ban đầu mình đến với bộ sách do mình rất thích thể loại viễn tưởng, phiêu lưu. Một phần chắc do đã đọc Harry Potter nên truyện này có vẻ hơi nhạt. Truyện vẫn tiếp tục đan xen vào các tình tiết thần thoại cũng khá hấp dẫn, gợi được óc tò mò của người đọc và thấy được sự sáng tạo của tác giả. Các pha hành đọc cũng khá hay và kịch tính nhưng thưa thớt hơn. Nói chung tập này hơi chùng xuống hơn những tập trước. Mong rằng những tập sau sẽ lôi cuốn, hấp dẫn hơn nữa.
3
696755
2015-08-21 06:34:07
--------------------------
248944
10789
Trong tập này, tác giả nói nhiều về thời quá khứ của các nhân vật, làm đôi lúc mình thấy hơi chán khi đọc những đoạn đó. Nhưng cần có những tình tiết đó để người đọc hiều được những tâm trạng của nhân vật tại thời điểm hiện tại. Josh chưa bao giờ tin tưởng vào nhà giả kim, so với Dee thì Josh tin tưởng hơn, chính vì thế dễ bị Dee dụ dỗ.Tập sách này hơi nhạt so với những tập trước. Hy vọng các tập sau mình sẽ thấy hào hứng hơn khi đọc và đón chờ những tình tiết hấp dẫn mới.
3
389776
2015-07-31 11:25:46
--------------------------
228181
10789
Tôi thích bộ truyện này, nhưng phải đến khi đọc xong cả bộ, tôi mới cảm thấy mình hiểu hết từng tập truyện đã đọc. Các tình tiết đan cài vào nhau, như một tấm lưới thưa dần dần được bổ sung sợi ngang sợi dọc cho đến khi đầy chặt. Tập này Josh - cậu em trai trong cặp sinh đôi vàng bạc đã lạc bước, bị kẻ thù dụ dỗ. Tôi nghĩ tôi hiểu được cảm giác của cậu: cảm thấy sợ hãi và ghen tị khi người chị sinh đôi không còn giống mình, cảm thấy mình bị mờ nhạt và lạc lõng, và muốn làm điều gì đó để chứng tỏ. Giữa lúc ấy Dee xuất hiện, và với tài thuyết phục, hắn đã khiến Josh tin tưởng hơn là nhà giả kim. Tôi kể đến đây thôi, kẻo lại tiết lộ nội dung câu chuyện. Cả tập này là đấu tranh nội tâm của Josh, và đấu tranh của cả nhóm nhà giả kim tìm cách thuyết phục lại cậu bé.
4
82774
2015-07-15 08:22:09
--------------------------
115570
10789
Josh chưa bao giờ đặt trọn niềm tin vào Flamel, nhất là khi cậu biết ông vẫn còn giấu nhiều bí mật chưa tiết lộ với cậu. Sau khi được Mars đánh thức, Josh đã phát huy được năng lực của mình và nhanh chóng trở nên mạnh mẽ không kém cô chị Sophie. Tuy nhiên, với sự nghi ngờ đối với Flamel, cộng thêm những lời dụ dỗ của Dee, Josh đã bị hắn lợi dụng làm mồi nhử để giải thoát Mẹ Các thần, một sinh vật khủng khiếp.
Tiếp tục với những pha hành động đầy kịch tính, tác giả Michael Scott đã đưa người đọc đi sâu hơn vào thế giới kì ảo của con người, các Elder và nhiều sinh vật kì bí khác. Cuộc chiến giữa thiện và ác ngày càng trở nên gay cấn hơn.
Cùng đọc để được hoà mình vào những cuộc phiêu lưu kì bí và mong chờ tập tiếp theo :)
4
307488
2014-06-29 21:07:50
--------------------------
78644
10789
Đến với quyển thứ 4 trong series "Bí mật của Nicolas Flamel bất tử " tuy phong thái viết văn của tác giả vẫn ổn định nhưng cách hành văn đã bắt đầu mất dần sự lôi cuốn, một số đoạn thì mình thấy dường như đọc vào rất khó hiểu và dường như là không hình dung được cốt lõi, song thì vẫn có các điểm đáng khen ngợi đó là Michael Scott ngày càng mang lại nhiều kiến thức cho đọc giả và mình đã mở rộng thêm vốn hiểu biết  và thích các thần thoại cổ từ sau khi đọc xong bộ truyện này.
5
99609
2013-06-02 16:25:24
--------------------------
52165
10789
Lúc đầu mình biết đến bộ truyện này thông qua sự giới thiệu của thằng bạn, lại còn nghe người ta nói rằng bộ này là một sự thay thế xứng đáng cho Harry Porter. Chính vì thế mình đã rất háo hức khi mua nó, lúc đầu đọc cũng thấy hay hay, cũng là cuộc phiêu lưu trong một thế giới pháp thuật khá giống với Harry Porter. Nhưng càng đọc thì mình càng thấy truyện này không đuợc hấp dẫn cho lắm. Thứ nhất là truyện có quá nhiều nhân vật, điều này làm loãng đi các nhân vật chính, không khắc hoạ sâu được các nhân vật chính. Thứ hai là mỗi tập truyện lại diễn ra ở một thành phố khác nhau, tác giả cứ dẫn người đọc đi từ nơi này sang nơi khác, rồi lượng thông tin mà tác giả đem đến cũng khá nhiều nhưng cứ nhàn nhạt.
Nói tóm lại bộ truyện này đọc cũng được nhưng nó khó lòng để thay thế được sự hấp dẫn như Harry Porter
3
60663
2012-12-25 14:05:35
--------------------------
22014
10789
Có vẻ như tác giả đã "đuối sức" ở tập 4 này rồi (nhất là khi cốt truyện khá dàn trải và một số nhân vật xuất hiện khá mờ nhạt. Cũng có thể tập truyện này là cầu nối cho sự phát triển trong các tập sau-mình hy vọng thế!)-nhưng tình cảm mà mình dành cho hương vani và hương cam-Sophie và Josh thì sẽ không thay đổi! Cũng như sự ngưỡng mộ của mình với tác giả khi mà kiến thức về thần thoại học của ông lại sâu rộng đến thế.
Bởi vì mình đã đọc tập 1 cách đây gần 4 năm rồi-một khoảng thời gian đủ dài để có cảm giác gắn bó với các nhân vật và muốn tiếp tục đồng hành cùng họ. Hơn nữa, tại sao lại không tiếp tục khi mà mỗi tập truyện là một cơ hội tuyệt vời đưa mình đến gần hơn với những nhân vật thần thoại, lịch sử nổi tiếng, một cơ hội nhìn thế giới này thông qua đôi mắt của thời gian và phép thuật. Để từ đó biết trân trọng hành tinh và môi trường sống của chúng ta hơn. Bằng cách này hay cách khác, tác giả M.Scott đã rất thành công rồi!
3
12542
2012-03-19 15:24:13
--------------------------
20210
10789
Khi đọc xong quyển 1 trong bộ này, tôi quyết định mua ngay 3 quyển tiếp theo với hy vọng sẽ được nối tiếp dòng phiêu lưu tưởng tượng của mình. Thế nhưng, càng về sau, câu chuyện càng dài ngoằng ra, đọc rất mệt nhưng cũng phải ráng đọc hết. Đã tới quyển 4 nhưng chuyển biến bất ngờ duy nhất là Josh đã lao vào cùng với hội Dee, mặc lời của Sophie. Ác và hiền đã phân định rõ ràng. Có điều mình vẫn không muốn nhà Flamel chết. Dẫu sao cũng rất mong đợi quyển 5 để biết cuối cùng ai sẽ là người nắm giữ chiến thắng. ^^ 
3
16074
2012-02-27 22:51:39
--------------------------
18234
10789
Tôi đã xem 3 quyển đầu tiên của bộ truyện này, quyển 4 này có phần lê thê hơn 3 quyển trước nhưng vẫn tạo cho tôi cảm giác muốn xem quyển tiếp theo. Mỗi quyển chia ra làm nhiều chương, đọc hết mỗi chương, tác giả khéo léo gợi mở người đọc háo hức muốn đọc chương tiếp theo. Nếu yêu thích thể loại giả tưởng kì bí có pha 1 chút kiến thức khoa học địa lý ngày nay thì bạn sẽ không thể bỏ qua bộ truyện đặc sắc này.
p/s: mình ấn tượng với địa danh hòn đảo Alcatraz kì bí, một địa danh có thật ở San Fancisco và những câu chuyện có thật về hòn đảo này vô cùng kì lạ.
3
7177
2012-01-28 15:17:48
--------------------------
10952
10789
Nói thật, mới đầu truyện vô thì rất hay, rất gay cấn. Nhưng càng đọc càng mệt. Thời gian và không gian căn không đều nhau. Nhân vật không nổi bật. Đáng lí ra, hai nhân vật chính của chúng ta cho đến tập 3 ít nhất cũng phải làm ra được điều gì đó kì tích rồi chứ. Mãi cho đến bây giờ nguồn năng lượng thuần khiết của hai chị em chỉ xài vào toàn chuyện nhảm nhí. Lại quá nhiều nhân vật khiến cho người đọc cảm thấy rối mù cả lên. Quá nhiều lí lẽ, quá nhiều bí ẩn. Đọc riết mà mình bị lậm, chẳng biết phe nào là phe thiện, phe nào là phe ác hết. Nhưng thôi kệ. Trong khi chờ Percy Jackson phần 5 ra thì cũng có cái để mà coi!
5
11050
2011-09-06 23:16:33
--------------------------
7999
10789
 Truyện này thuộc thể loại phiêu lưu, giả tưởng thì phải. Mình thấy nội dung cũng được nhưng chưa có nhiều đặc sắc trong phong cách văn chương. Lời văn còn chưa gợi được nhiều liên tưởng sống động khiến mình mới đọc đã thấy khá mệt mỏi . Nhân vật thì chưa có gì nổi bật mấy,chẳng khiến mình ấn tượng. Được cái là có ý nghĩa nhân văn đó. Câu chuyện xảy ra với nhiều cuộc chiến bảo vệ cuộc sống  của vợ chồng nicholas và những người bạn. Họ đã chiến đấu rất ngoan cường và bảo vệ mọi người. Kết thúc câu chuyện họ đã tiêu diệt được thế lực hắc ám. Qua đó đã nêu ra chân lí của cuộc sống. Mình nghĩ truyện này rất có ý nghĩa.
3
7233
2011-07-10 22:41:48
--------------------------
