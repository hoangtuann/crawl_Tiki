464577
10867
Thực sự những quyển sách trong series Horrible Science rất lý thú, mình đã tìm thêm được 4 quyển nữa trên Tiki. Cách viết cuốn hút, nội dung sắp xếp rõ ràng, minh họa dí dỏm. Dù tựa sách là Khoa học rùng rợn nhưng minh lại thấy nó rất hấp dẫn chứ không rùng rợn tí nào. Đây đúng là tựa sách dành cho mọi lứa tuổi! Chưa kể giá cả cũng rất ưu đãi và hợp lý, rất đáng đồng tiền và phù hợp cho lứa tuổi học sinh như mình. Mọi người cũng nên theo dõi đọc đi nhé !
5
1362147
2016-06-30 11:34:53
--------------------------
454167
10867
Khảo cổ dễ sợ nằm trong series The Knowledge gần tương tự như Horrible Histories, đây là 2 series giới thiệu về lịch sử và khoa học mà mình rất thích. Trong tập sách này giới thiệu chi tiết về giới khảo cổ, tác giả Nick Arnold đã dùng văn phong đơn giản dễ hiểu cộng thêm sự dí dỏm hài hước. Ngoài ra, mình rất đánh giá cao yếu tố này của tác giả khi sáng tạo ra một số mục nghe có vẻ điên như: nguyệt san xác ướp, kịch bản phim của các nhà khảo cổ,...Hình vẽ rất sinh động được khéo léo minh họa cho từng câu chuyện, bìa sách đẹp, chất lượng in tốt.
4
632796
2016-06-21 20:14:14
--------------------------
385312
10867
Mình thấy thế giới khảo cổ rất thú vị, sự tái khám phá lịch sử trái đất cũng như loài người mang đến sự hiểu biết cho mọi người về quá khứ nhưng công việc của các nhà khảo cổ lại chưa được biết đến nhiều cũng như sự vất vả và tâm huyết của họ, sau khi đọc cuốn sách này mình biết được thêm  nhiều về công việc của các nhà khảo cổ cũng như cách họ đã xác định được tuổi của các cổ vật cũng như sự "kinh dị" trong công việc của họ vì đây là bộ sách khoa học kinh dị mà, mình cảm thấy lịch sử thú vị hơn nhiều sau khi đọc cuốn sách này
4
306081
2016-02-23 21:06:26
--------------------------
378587
10867
Mình rất thích series The Knowledge, đặc biệt là bộ Horrible Geography. Cách trình bày cũng như ngôn ngữ sử dụng rất dễ hiểu. Kiến thức được truyền tải một cách hài hước và rất nhẹ nhàng. Nhiều khi mình đọc thông tin và nhớ rất lâu mà không có cảm giác là mình đang tiếp nhận một khối lượng kiến thức khổng lồ. Đúng là một bộ sách tuyệt vời, thích cả cách viết và cách dịch thuật bộ sách, vừa xác thực lại vừa rất hay. Phải nói là tuyệt vời, chưa bao giờ hối hận vì bộ sách này
5
554574
2016-02-06 10:25:45
--------------------------
341898
10867
Mình rất thích các quyển sách trong serries này nên cố gắng sưu tập cho hết. Và cuốn sách này cũng không làm cho mình thất vọng. Với cuốn sách này, những kiến thức mà ta tưởng chừng như nhàm chán trở nên rất sinh động và vô cùng thú vị, ta sẽ biết rõ hơn công việc cũng như những khó mà những nhà khảo cổ ngày xưa đã phải trải qua. Dù là sách dành cho thiếu nhi nhưng mình nghĩ nó thích hợp với tất cả mọi người đặc biệt là với những người muốn khám phá thế giới bên ngoài
5
361863
2015-11-23 12:24:05
--------------------------
333529
10867
Với cách trình bày dễ vui nhộn hấp dẫn, cuốn sách đã truyền tải một cách ngắn gọn và dễ hiểu những kiến thức tưởng chừng khô khan. Hình ảnh minh hoạ kèm lời bình đặc sắc là điểm nổi bật cảu dòng sách Horrible Science này. Không chỉ cuốn Khảo cổ này nói riêng mà tất cả những quyển sách thuộc thể loại này đều cuốn hút và bổ ích. Không nặng về nội dung nhưng cũng không hẳn là truyện tranh giài trí, đây là sự kết hợp hoàn hảo cho những bạn thích đọc sách nhưng lại ngán những quyển khô khan.
4
65602
2015-11-07 23:25:52
--------------------------
305477
10867
Mình nhớ từ nhỏ, khi lần đầu tiên biết được về bộ môn khảo cổ học, mình đã có niềm ham mê đặc biệt với những gì thuộc về nền văn hóa và đời sống sinh hoạt của những con người sống ở thì quá khứ, ví dụ như họ ướp xác như thế nào, xây dựng lăng mộ ra làm sao, .v.v. Được viết bằng giọng văn tự sự xen lẫn biểu cảm vô cùng phong phú và hài hước, sách giúp mình tiếp cận những thắc mắc và giải đáp nó một cách hoàn toàn tự nhiên, không hề có chút khô khan nào như những bài giảng lê thê của thầy cô trên lớp. Mình cực kỳ thích những kiến thức bổ ích mà cuốn sách gửi gắm.
5
580478
2015-09-16 23:13:33
--------------------------
303174
10867
Bạn yêu thích việc tìm hiểu khảo cổ học thì sách này dành cho bạn. Nó chỉ cho bạn về những địa điểm bí mật, những cuộc khai quật đồ sộ, những người thám hiểm đi tìm những lăng mộ bị yểm bùa và bị trừng phạt, những kẻ tham lam đi tìm những kho báu cổ, những bí mật xung quanh xác ướp và những thứ kinh dị trong hầm mộ. Tuy vậy, sách được kể bằng lời nói hài hước nên bạn không hề cảm thấy nhàm chán, buồn ngủ tí nào đâu. Một cuốn sách tuyệt vời
4
381629
2015-09-15 18:18:33
--------------------------
295655
10867
Cuốn sách này vô cùng hài hước và bổ ích. Không chỉ đem lại nhiều kiến thức về khảo cổ, sách còn đem lại những thông tin thú vị trong môn Vật Lý và Lịch Sử nữa. Sách sẽ bổ trợ rất tốt cho học sinh cấp 2,3 trong việc học tập. Sách in giấy dày và cầm rất đã tay. Sách viết không phải theo kiểu "nhồi nhét" kiến thức, mà là theo kiểu lồng ghép câu chuyện để truyền tải kiến thức. Rất nhẹ nhàng, dễ nhớ và cực kì, cực kì vui nhộn...! Một quyển sách cần thiết cho những ai yêu khoa học nói chung. Bạn sẽ khám phá được nhiều điều mới mẻ từ quyển sách này!
5
802760
2015-09-10 15:33:16
--------------------------
282892
10867
Quyển sách chứa đựng nhiều điều mới mẻ mà mình chưa biết được. Nó cung cấp thông tin bổ ích cho người đọc về công việc khảo cổ và những chuyến hành trình. Ngoài ra, cách viết vui nhộn, hình ảnh sinh động làm mình rất thích quyển sách này. Mới mua nhưng mình đã đọc đi đọc lại 2 lần rồi. Cầm quyển sách rất vừa tay, dễ dàng lật trang, giấy thơm. Đặc biệt đối với những bạn thích khảo cổ, cuốn sách này rất thú vị. Thêm cả những câu đố nhỏ thử thách người đọc nữa.
5
153585
2015-08-29 15:31:56
--------------------------
267357
10867
Cuốn sách này thật thích hợp cho những ai yêu thích khảo cổ và lịch sử vì 2 lĩnh vực này hỗ trợ rất nhiều cho nhau. Sách gồm rất nhiều những chi tiết lý thú và đáng ngạc nhiên về ngành khảo cổ đi kèm những minh hoạ vui nhộn khiến người đọc thấy cuốn hút vô cùng. Các câu đố vui trắc nghiệm cũng làm tăng tính tương tác cho cuốn sách. Sách giúp mình hiểu thêm về công việc gian nan vất vả và không kém phần nguy hiểm của các nhà khảo cổ học. Sách in trên giấy xốp nhẹ dễ đọc tuy vẫn còn vài lỗi chính tả.
5
470937
2015-08-15 14:38:12
--------------------------
253636
10867
Khảo cổ chưa bao giờ thú vị đến như vậy.Cuốn sách này đã đem đến cho tôi bao nhiêu thông tin thú vị về những nhà khảo cổ cổ lỗ sĩ, những kẻ đào mộ rùng rợn và làm thế nào để trở thành một nhà khảo cổ.Không chỉ cung cấp những thông tin bổ ích, tôi nhiều lúc còn bật cười trước những hình ảnh minh họa vô cùng hóm hỉnh và hiểu ra nhiều điều trước cách giải thích đơn giản mà súc tích.Tôi đã đọc đi đọc lại cuốn sách này rất nhiều lần mà vẫn chưa chán.Thật đáng tiền mua.
5
280592
2015-08-04 16:28:23
--------------------------
239972
10867
Nếu bạn muốn làm một nhà khảo cổ, thì cuốn sách này rất có ích cho bạn. Nó giới thiệu tất tật những gì bạn cần biết khi đào bới và tìm kiếm, kể cả những sự thật từ buồn cười cho đến kinh khủng. Sách mô tả sinh động và hài hước, cộng với hình minh họa rất hợp phong cách của ngôn ngữ. Tôi đọc mà nghĩ hơi tiếc vì sách mỏng, sau đó lại nghĩ, thật may vì còn nhiều cuốn khác trong bộ này nữa, hehe. Vậy là có thêm một kho kiến thức thú vị và hấp dẫn đợi mình khám phá.
5
82774
2015-07-24 10:39:59
--------------------------
227362
10867
Khảo cổ tuy là một môn lịch sử nhưng cũng mang tính khoa học. Bước vào thế giới trong những trang sách là bạn được quay về những thế giới đã từng tồn tại trong lịch sử nhưng nay chỉ có ở dưới lòng đất, nơi những lăng mộ, xác ướp, bùa yểm, vật tế,... và những câu chuyện kỳ bí xung quanh sẽ thu hút chúng ta không ngừng. Những cuộc hành trình thú vị, những câu chuyện xa xưa, những thí nghiệm và trắc nghiệm được "bố trí" giữa các trang sách sẽ giúp chúng hiểu hơn những kiến thức tưởng chừng khá khô khan. Mình khuyên các bạn nên đọc thêm quyển Người Ai Cập - xác ướp cũng phải choáng váng cũng trong bộ sách này vì chúng cũng khá tương đồng với nhau mặc dù khác tác giả.
5
382812
2015-07-14 06:35:17
--------------------------
222703
10867
Khảo cổ thật thú vị. Bạn sẽ nhận ra điều đó ngay khi đọc qua cuốn sách” Khảo cổ dễ sợ “ này. Qua lời văn dí dỏm, hình ảnh minh họa sinh động, ngộ nghĩnh sẽ giúp chúng ta hiểu rõ hơn về môn khảo cổ học khó nhằn, về cách làm thế nào để khai quật một di tích , về cách nhận xét tuổi của vật cổ đó, tất tần tật tất cả đều có trong cuốn sách thú vị này. Một cuôn sách thích hợp , bổ ích cho những ai có niềm đam mê với môn khảo cổ hoặc ai muốn tìm hiểu môn này.
5
554214
2015-07-06 07:53:50
--------------------------
222689
10867
Đây là quyển khoa học Horrible Knowledge mình đọc đầu tiên trong cả sê ri và cực thích nó, nhất là những đoạn viết về xác ướp ai cập mình nhớ từng ý một dù đã đọc rất lâu trước đây rồi. Sách in đẹp, nhẹ, hình vẽ dễ thương, sinh động, lời văn không quá khó hiểu như một số quyển cùng sê ri. Bạn nào thích khảo cổ học phân tích bằng lời bình vui vui chắc sẽ rất thích quyển này, ví dụ như đoạn xác ướp có nội tạng sẽ được tách riêng trong bình còn óc bị xem là thừa thải nên bị ném cho mèo ăn.
5
120276
2015-07-06 02:47:03
--------------------------
202600
10867
Nếu bạn mơ ước trở thành một nhà khảo cổ học nhưng lại không thích xem các chương trình khảo cổ nhàm chán trên TV thì đây, cuốn sách này được viết ra ắt là dành cho bạn. Cuốn sách trình bày các thủ thuật trong khảo cổ, các vụ khảo cổ kinh điển, bí ẩn đằng sau các lời nguyền, sự thật về các công trình khảo cổ... tất cả được trình bày với văn phong hài hước kinh điển của bộ sách Kiến Thức Hấp Dẫn. Sách nhỏ gọn, bìa bắt mắt, minh họa dí dỏm, chất lượng in tốt, dịch thuật hay, sát nghĩa, chất lượng giấy tốt. Cuốn sách chắc hẳn sẽ mang đến cho bạn nhiều bất ngờ.
4
564333
2015-05-30 00:08:30
--------------------------
163185
10867
Chưa một cuốn sách nào trong bộ này làm mình thất vọng cả. Những thông tin và giá trị cuốn sách đem lại thật sự rất bổ ích.
Hình ảnh minh họa rất dí dỏm dễ thương, kết hợp với câu chữ và các thông tin "rùng rợn" khiến mình ngồi đọc 1 lèo hết cả sách. Nếu được lựa chọn giữa cuốn sách và đọc thông tin trên internet, chắc chắn mình sẽ chọn cuốn sách này, mặc dù nó vẫn bị giới hạn về lượng kiến thức. Tuy vậy, những thông tin nó đem lại đã được chắt lọc kĩ lưỡng, từ giới thiệu chung, những cuộc thám hiểm, những khám phá động trời, chắc chắn sẽ làm mình hứng thú hơn so với đọc 1 mớ thông tin tràn lan trên mạng nhiều.
5
9025
2015-03-04 12:39:27
--------------------------
25285
10867
Lần đầu tiên tôi biết đến cuốn sách này là nhờ sự tò mò của mình về sách. Không hiểu sao nhưng tôi ko có ấn tượng gì về sách cả. Đối với tôi, sách thật nhàm chán và tẻ nhạt. Nhưng khi lần đầu tiên tôi nhìn thấy tựa 1 cuốn sách trong kho tàng Horrible Science là CƠ THỂ CHÚNG TA MỘT THẾ GIỚI KỲ THÚ, tôi đã thay đổi suy nghĩ. Từ bìa sách đã làm tôi tò mò với những hình vẽ hài hước, dí dỏm không kém với nội dung. Với những câu hỏi, từ ngữ, hình ảnh minh hoạ, lời văn gần như rất ngớ ngẩn nhưng lại rất dễ đi vào đầu người đọc. Từ một cuốn sách đã kéo tôi lại gần hơn môt kho tàng Horrible Science, tôi bắt đầu quen thuộc với những tựa sách của Horrible Science: Thiên nhiên hoang dã, Thú dữ, Hoá học - Một vụ nổ ầm vang, ... Cũng như những quyển sách khác của Horrible Science, Khảo cổ dễ sợ mang trong mình những câu chuyện, thông tin dễ sợ qua cách ví von dí dỏm, hài hước được truyền đạt cho người đọc một cách dễ hiểu nhất. Lại môt lần nữa tôi được mở rộng tầm mắt qua môt cuốn sách gần gũi dễ hiểu của Horrible Science
4
35849
2012-04-23 13:27:32
--------------------------
9219
10867
Bằng những hình ảnh sinh động như truyện tranh, cuốn sách giúp cho cả trẻ em cũng có thể hiểu biết thêm về ngành khảo cổ và những điều dễ sợ qua những tình huống vui nhộn, thú vị trong từng trang sách. Cuốn sách cho thấy những kiến thức bổ ích. Mình thấy thay vì mua những cuốn truyện tranh, chúng ta nên mua những cuốn sách như cuốn này, đặc biệt cho trẻ em để mở mang kiến thức bằng một cách bớt khô khan hơn. Đây đúng là những cuốn sách mà mọi người nên có. Ngoài cuốn sách này, mọi người nên mua tron bộ sách của Nick Arnold để có được vốn kiến thức phong phú nhất.
3
9013
2011-07-31 17:14:54
--------------------------
