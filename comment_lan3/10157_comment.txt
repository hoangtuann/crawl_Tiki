79774
10157
Cuốn sách này không thực sự cuốn hút mình như mong đợi ban đầu. Mặc dù truyện ngắn cũng là một thể loại mình yêu thích nhưng những truyện trong "Khúc hát giờ kẹt xe" lại chưa đem đến nhiều cảm xúc chân thực hay những ấn tượng mạnh mẽ. Tác giả Ngô Thị Hạnh có một cách nhìn đời, nhìn người rất riêng, dường như chị luôn tìm kiếm những ý nghĩa sâu xa nhất trong những điều bình lặng nhất, những góc yên tĩnh nhất. Nhưng tác giả lại chưa đi được hết trọn vẹn những ý tưởng của mình, làm một vài truyện ngắn gây cảm giác lửng lơ, hụt hẫng cho người đọc. Nhưng dù sao đây cũng là một cuốn sách nên đọc, với những ai yêu thích văn học Việt Nam, vì bên cạnh những truyện chưa hay lắm cũng có một vài truyện khá thú vị.
3
109067
2013-06-09 12:17:19
--------------------------
28305
10157
Tôi được tặng cuốn sách này với lời đề tặng "Hay lắm, đọc đi" và tôi đã đọc. Truyện đầu tiên, ừ thì hay. Truyện thứ hai, ừ thì cũng tạm được. Truyện thứ ba, thứ tư.... Tôi cố đọc, đọc hoài mà không thấy cái "hay' ở đâu. Những câu chuyện, tôi không dám nói là nhạt nhẽo nhưng mà cách xử lý quá hời hợt, nhiều chỗ mơ hồ. Tệ nhất là có những chỗ giống như đang mơ, đang bị ảo giác vậy. Như là bài "Linh hồn hoa mai" hay bài gì đó mà có bức chân dung. Tác giả có ý tưởng không tồi, giọng văn cũng không tệ, nhưng cách xử lý, cách để hoàn thành câu chuyện của mình thì tệ quá. Mặc dù rất cố gắng để đọc hết, vì là sách được tặng với lời đề tặng, nhưng tôi đã không thể đọc hết được. Không phải là đậm chất thơ mà là sự hoa mĩ không cần thiết. Tôi cảm thấy mệt khi đọc cuốn sách này và tôi không có ý định đọc hết hay đọc lại lần nữa. Tất cả mọi thứ thuộc về cuốn sách này đều mơ hồ như là bị ảo giác vậy. Không thực, không nhân văn, và không có cả cái hay, cái mới trong văn học, dù xếp vào dạng tản văn thì cũng không thể đánh giá những câu chữ đó là hay chứ đừng nói đến việc xếp vào truyện ngắn.
1
23764
2012-05-26 10:21:21
--------------------------
