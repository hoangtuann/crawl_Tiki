375151
10559
6 khám phá bí mật ở trong nhà gấu con là cuốn sách dành cho thiếu nhi khai thác các vấn đề cần phải có của nhiều giai đoạn phát triển của trẻ khi cho tiếp cận với môi trường giáo dục với các phương pháp trong giai đoạn chứa đựng nhiều đồ vật khiến trẻ học được cách hình thành nên tự nhiên , hình vẽ in đậm , rõ ràng làm cho hứng thú với từ vựng được nâng cao làm chất lượng tốt , bìa bóng đẹp , hấp dẫn người xem thích thú với tác phẩm .
3
402468
2016-01-27 20:22:24
--------------------------
228730
10559
Mình ham sách giảm giá nên mua về mới biết là sách tranh, phù hợp hơn với trẻ mẫu giáo. Câu chuyện xoay quanh cuộc sống của nhà bác sỹ Gấu, mỗi một hành động nhỏ, một chi tiết, một sự việc đều gắn liền với các bài học về đạo đức, lòng yêu thương không phân biệt khác loài. Ngoài ra cuối một chương truyện có thêm những thông tin về các loài động vật và đặt ra các giả thiết để các em vận dụng trí nhớ. Sách khá bổ ích tuy nhiên đã quá tuổi đọc của mình. Mình chỉ đặc biệt thích tranh vẽ, tranh nước ngoài đẹp tới mức mình phải trầm trồ nức nở, màu sắc sinh động, sắc nét và cực kỳ tinh tế.
4
6502
2015-07-15 20:57:42
--------------------------
