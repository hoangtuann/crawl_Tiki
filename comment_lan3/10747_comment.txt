392447
10747
Ngay từ cái tiêu đề " Hạnh phúc là gì ?" đã khiến ta phải suy ngẫm. Người ta tự hỏi vậy hạnh phúc thực ra là gì và nghiền ngẫm cuốn sách để tìm lời giải đáp.
Hai từ hạnh phúc là thứ mà cả đời này người ta đi tìm, người ta dành giật. Và để đến được đích ấy, ta phải đánh đổi rất nhiều thứ. Cả niềm vui cả nỗi buồn, cả máu và nước mắt... 
Hạnh phúc làm nên màu sắc của cuộc sống, khiến ta thấy được ý nghĩa của cuộc sống. 
Một cuốn sách rất ý nghĩa. Cám ơn vì đã viết nó ☺☺
4
278759
2016-03-06 22:15:44
--------------------------
82637
10747
Từ trước đến nay, không ai có một đáp án lí giải dúng về định nghĩa của '' hạnh phúc''. Tôi chỉ biết nó à một cảm xúc  mà khiến con người ta cảm thấy tâm hồn được thanh thản, được nghỉ ngơi. Nhu cầu hạnh phúc của mỗi người là khác nhau, có người mong nhiều có người mong vừa đủ. Nhưng số phận đâu cho ta chọn lựa. ''Hạnh phúc là gì'' đưa đến người đọc từng câu chuyện về hạnh phúc. Đưa ta cùng trải lòng với họ, để cùng nhịp đập yêu thương cùng rung lên với hạnh phúc của họ. Và cũng để tự bản thân hiểu được hạnh phúc thực sự là gì. Vì đơn giản thực sự không phải là giả tạo. Thế thôi.
4
123837
2013-06-22 09:27:02
--------------------------
50709
10747
Chắc chắc rằng không một ai có thể định nghĩa được hạnh phúc một cách rõ rãng nhất. Và với mỗi người, hạnh phúc lại được hiểu theo một cách khác nhau. Người nghèo có thể sẽ xem hạnh phúc là khi có được một mái ấm, là tiền bạc ... Người giàu lại xem hạnh phúc là sức khoẻ, là những giờ phút gia đình ngừng tất thảy những công việc bận rộn để ngồi sum họp gia đình ... Trẻ em mồ côi thì hạnh phúc lại là có được một gia đình, có cha, có mẹ ... Nhưng dù hạnh phúc được định nghĩa như thế nào đi chăng nữa thì hạnh phúc cũng nên được hiểu đơn giản là khi con người ta có mơ ước, có đam mê, biết hài lòng với những gì mình đang có và biết cố gắng vì một ngày mai tốt đẹp hơn. Thế là đủ! Quyển sách ấn tượng với mình ngay từ đầu bởi mẫu mã đẹp và dòng chữ: "Thành công không phải là chìa khoá để có hạnh phúc, mà hạnh phúc mới chính là chìa khoá để thành công". Đáng đọc và suy ngẫm.
4
63487
2012-12-16 10:26:02
--------------------------
