517637
10488
Nội dung quyển sách khá lạc hậu, không còn đáp ứng được các kiến thức cần thiết cho một kì thi thật. Hình thức cũng khá xấu, khó sử dụng, không bắt mắt
3
1423683
2017-02-02 14:28:39
--------------------------
335238
10488
Cuốn sách Building Skills For The TOEIC Test - Chương Trình Luyện Thi TOEIC rất hữu ích cho các bạn luyện Toeic với nội dung hay kèm theo đĩa CD để tiện cho các bạn luyện kỹ năng Listening luôn. Bố cục của cuốn sách cũng dễ hiểu để các bạn dễ follow theo. Chăm chỉ luyện mỗi ngày một bài chắc chắn sẽ giúp các bạn nâng cao kỹ năng tiếng Anh của mình rất nhanh sau một thời gian nhất định. Tuy nhiên nội dung bài luyện của cuốn sách có lẽ cũng chưa được cập nhật lắm nên so với đề thi bây giờ chưa sát lắm. Tuy nhiên, dùng để luyện tập nâng cao trình độ thì cuốn sách này vẫn rất có ích :))
3
102823
2015-11-10 20:12:18
--------------------------
230700
10488
Cuốn sách này trên TIKI mặc dù ghi ngày xuất bản là 10/10/2011 nhưng khi mình mua về thì cuối sách lại ghi in xong và nộp lưu chuyển 6/2004. Vì là năm 2004 nên nội dung sách theo mình biết khá là lạc hậu. Sách nêu phần VI trong đề thi TOEIC là nhận diện lỗi sai.Còn phần VI đề thi thử TOEIC mà mình làm và có lần mình đã thi thật thì phần VI là chọn từ điền vào đoạn văn có chỗ trống. Phần VII trong sách chỉ nói về đọc-hiểu một đoạn văn, còn trong đề thi thì có cả phần đọc-hiểu 1 đoạn và đọc-hiểu 2 đoạn. Tuy nhiên sách có nội dung sách hay cung cấp rất nhiều mẹo bổ ích,hay trình bày logic, mỗi phần nêu rõ cách làm và tránh bẫy trong đề thi.
2
112336
2015-07-17 17:38:23
--------------------------
165598
10488
Cuốn Building Skills For The TOEIC phát hành bởi First New, và đương nhiên luôn gặp phải điểm yếu của First New.
Về phần xấu : Cuốn sách có bìa rất xấu, làm người đọc khó có hứng thú với cuốn sách, giấy mỏng và độ trắng không cao, cũng như mực in rất đậm nhưng lem luốt và thậm chí có những chấm đen do vẫy mực.
Về phần tốt : Cuốn sách đề cập đến các bài học nhằm luyện thi kì thi TOEIC nên được trình bày khá logic và phân chia bài rõ ràng, dễ nghiên cứu.
Cũng may nội dung đã cứu vãn được hình thức không mấy tốt của cuốn sách này !
3
298441
2015-03-10 18:48:15
--------------------------
