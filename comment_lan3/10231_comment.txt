298682
10231
Cuốn sách dạy những người học tiếng anh từ bước cơ bản đầu tiên học cấp tốc với cách phát âm như viết tiếng Việt khá dễ dàng. Tuy vậy, phần một số câu giao tiếp hàng ngày và trong công việc đưa ra khá khó hiểu và áp dụng. Về phần từ vựng được phân ra khá rõ ràng và dễ tìm kiếm nhưng chỉ tập trung ở một số danh từ, không có nhiều tác dụng cho việc học cấp tốc cho lắm. Tuy phần dạy phát âm không được khoa học nhưng cho người mới là cực kì dễ hiểu!
4
419803
2015-09-12 20:25:48
--------------------------
