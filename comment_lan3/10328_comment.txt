415189
10328
Xét về hình thức: cuốn sách có kích thước vừa phải - dễ dàng bỏ túi, bìa sách màu xanh hi vọng - rất "mượt", trang trí rất bắt mắt, lại còn có song ngữ, mình rất thích điều này.
Xét về nội dung: cuốn sách là những câu nói, những mẩu chuyện nhỏ được rút ra từ cuộc sống - rất ý nghĩa, giúp bạn thư giãn, cho bạn thêm động lực khi rơi vào bế tắc.
Đây thật sự là một cuốn sách nhỏ xinh, rất đáng đọc, cũng rất phù hợp để làm quà tặng cho bạn bè và người thân.
5
636735
2016-04-13 02:38:52
--------------------------
310923
10328
Sách có song ngữ Anh - Việt có thể giúp ta học thêm được những vốn từ tiếng Anh khá hay. Quyển sách rất ý nghĩa với nhiều câu nói hay và mẩu chuyện nhỏ thu thập từ nhiều nguồn rất bổ ích. Nói về tầm quan trọng của sự lạc quan trong cuộc sống thì đó là nhân tố không thể thiếu, nó giúp ta có một tinh thần tốt, làm mối quan hệ của chúng ta được tốt đẹp và gắn khít hơn và thấy thêm yêu đời, sống hạnh phúc hơn. Quyển sách giúp ta lấy lại được tinh thần khi trong những trạng thái buồn đau, tiêu cực, giúp ta sống tích cực hơn. Cảm ơn Nhã Nam đã đem quyển sách tinh thần này đến và khơi lòng của biết bao độc giả.
5
534452
2015-09-19 20:37:03
--------------------------
290404
10328
Minh đọc cuốn sách này khi mà tâm trạng đang rất buồn bã và mất niềm tin vào cuộc sống. Mình cũng không mong lắm vào tác dụng của tác phẩm này. Nhưng rồi đọc từng trang sách, từ từ mình nhận ra rất nhiều điều ý nghĩa trong cuộc sống thông qua những mẫu chuyện nhỏ rồi được đúc kết trong một câu danh ngôn ngắn gọn. Cảm ơn cuốn sách này rất nhiều, cảm ơn tác giả thật nhiều vì đã  tổng hợp các câu chuyện ý nghĩa này thành một cuốn sách. Mình nhất định sẽ mua nhiều cuốn sách kiểu này nữa.
5
310891
2015-09-05 12:37:48
--------------------------
211688
10328
Mình đã đọc hầu như gần đủ bộ của Thông điệp yêu thương rồi, nhưng quyển sách này là quyển mà mình yêu thích nhất. Sách trang trí đẹp, thu hút, song ngữ nữa. Nội dung thì toàn những câu châm ngôn, câu chuyện rất hay về sự lạc quan, ý chí vươn lên. Quyển sách này giúp mình suy nghĩ lạc quan, vui vẻ hơn mỗi khi gặp khó khăn hay buồn phiền trong cuộc sống, giúp ta có cái nhìn mới hơn về cuộc sống tươi đẹp hiện tại. Đây là quyển sách hay về truyền đạt cảm hứng, ý chí vươn lên... :)
5
554150
2015-06-20 22:21:04
--------------------------
152807
10328
Trong cuộc sống đôi khi ta cảm thấy quá mỏi mệt khi gặp nhiều thử thách khó khăn,rồi ta muốn buông xuôi tất cả,ta cảm thấy sao tiêu cực tâm hồn.Ta tưởng chừng khó vượt quá sóng Đời gập ghềnh,chông gai kia.Nhưng rồi ta nhận ra Đời vẫn đẹp và cuộc sống vẫn xinh tươi.Cuốn sách nhỏ với tên bìa sách thật ý nghĩa,mình thích cuốn sách này không chỉ vì nó được thiết kế quá dễ thương,đẹp mà còn là những bài học nhân sinh quan sâu sắc triết lý qua những câu chuyện nhỏ trong đây.Không những thế những truyện này là song ngữ Anh-Việt nên rất hay khi vừa được học Tiếng Anh lại còn được thưởng thức những câu chuyện hay,ý nghĩa.Cuốn sách tuy mỏng nhưng đã đọng lại cho mình những dấu ấn khó phai và Cuộc đời vẫn nở hoa khi ánh Mặt Trời còn rực rỡ.
5
456366
2015-01-24 12:05:43
--------------------------
123352
10328
Màu xanh là màu lạc quan , yêu đời .Đó cũng chính là thần thái mà bìa cũng như nội dung quyển sách muốn mang lại .Bản thân nội dung quyển sách đều được tuyển chọn một cách cẩn thận từng câu chuyện,từng câu châm ngôn , triết lý sống khá hay.Không chỉ chăm chút nội dung , hình thức trình bày từng phần cũng lô gic ,dễ thương ,phù hợp với nội dung.

Điều hay nhất ở đây chính là nội dung được in bằng hai thứ tiếng:tiếng anh và tiếng việt.Mình vừa thoải mái ,có tinh thần phấn khởi mà sách đem lại , vừa có thể học tiếng anh một cách nhẹ nhàng , vừa nắm được cái cốt lõi của người viết một cách sâu sắc nhất.Một quyển sách thật là lợi hại.♥

"Don't worry,don't worry,don't do this , be happy"  là câu nói động viên tinh thần của bản thân mình sau khi đọc sách.Mình tin chắc bạn cũng có thể tìm đơợc câu nói động viên bản thân qua chính quyển sách này như mình vậy.
5
192593
2014-08-31 22:11:25
--------------------------
90948
10328
có lẽ trong bộ sách Thông điệp yêu thương của Nhã Nam thì đây là 1 trong những cuốn mình thích nhất. Bìa sách đẹp, trang trí bên trong tỉ mì, bắt mắt và nội dung thì rất hay và phù hợp cho những ai đang xuống tinh thần hoặc khép mình so với thế giới. Mình đọc cuốn sách này khi mình đang xuống tinh thần trầm trọng, thất bại trong kỳ thi quốc gia, áp lực học tập, điểm số và các bài kiểm tra dồn sức ép lên mình khiến mình rất chán nản và cảm thấy mỗi ngày trôi qua thật thảm hại. Nhưng khi đọc cuốn sách này, qua những câu danh ngôn và những câu chuyện ngắn, mình thấy mình thật khờ quá, vì hóa ra áp lực cuộc sống không ai khác mà do chính mình là ra. Và rồi quan điểm sống của mình cũng thay đổi, trở nên tích cực hơn. Nếu bạn cũng đang trong hoàn cảnh giống mình trước kia thì mình khuyên bạn nên đọc cuốn sách này, và rồi bạn sẽ thấy điều kỳ diệu sẽ xảy ra!
5
144488
2013-08-14 17:26:24
--------------------------
85894
10328
Trong cuộc đời ai cũng phải trải qua những khó khăn, thử thách, đôi lúc bất cần đời mà ta từ bỏ. Cuộc sống không chỉ là món quà mà Thượng Đế ban cho, mà còn là một bức thông điệp Tạo Hóa muốn nói với chúng ta, muốn chúng ta vượt qua nó. Trong cuốn sách này, chỉ những câu thành ngữ hay những mẩu chuyện nhỏ bé thôi, nhưng cũng đủ khiến ta nhìn lại quãng đường đã đi, tự hỏi mình đã làm những gì, hay kích thích một tâm hồn vươn lên sống trọn vẹn cuộc đời dù ta lo lắng, bồn chồn hay buồn bã. Vì lạc quan mới có thể sống đẹp, đúng không?
5
100129
2013-07-07 17:44:58
--------------------------
