373866
10744
Một quyển sách rất hay, rất đáng đọc.
Mình vô tình đọc được review về quyển này lâu lắm rồi, mà hồi đó hết hàng thì phải. Công thêm bìa chẳng bắt mắt xí nào nên đến bây giờ mình mới mua. Thực tình thì nhận sách về thấy hơi thất vọng, sách cũ và bìa 1 bị ngắn so với ruột ở trong. 

Làm mẹ là bản năng của bất kỳ người phụ nữ nào, 4 năm nuôi dạy một đứa trẻ không hề có quan hệ máu mủ ruột rà với mình, Kiwako đã thực sự trở thành mẹ của đứa bé. Đoạn cô bé nhớ lại buổi sáng hôm mình "được" tìm ra, Kiwako không hề quan tâm gì khác ngoài chuyện con gái cô chưa được ăn sáng. Lúc đọc đến đó mình đã bật khóc, người phụ nữ ấy quá dũng cảm, quá mạnh mẽ để một mình nuôi dạy đứa bé trong 4 năm, rồi sau đó chấp nhận đánh đổi cả tự do của bản thân cho tương lai đứa bé. Lúc cô bị bắt chia cắt với đứa bé, tim mình cứ như thắt lại, cứ như có tiếng nói trong đầu mình thầm cầu nguyện rằng đừng để cảnh sát chia cắt hai mẹ con họ, mong rằng đâu đó có ai đứng ra bảo vệ họ và thanh mình rằng họ cô bé con là con ruột của người phụ nữ ấy. 
5
4539
2016-01-24 22:04:20
--------------------------
321790
10744
Mình không đọc nhiều tiểu thuyết Nhật Bản, có lẽ bởi cái không khí nhàn nhạt, cứ man mác buồn đôi khi khiến người đọc thấy bí bách và khó chịu nên khi đi nhà sách mình  thường lướt qua rồi để lại xuống giá. Tuy vậy khi đọc phần tóm tắt nội dung của Bản năng đã khơi gợi sự tò mò và hứng thú cho mình khi viết về hành trình trốn chạy của một kẻ bắt cóc. 
Bản năng không chỉ là câu chuyện của một người. Mỗi nhân vật trong Bản năng đều có câu chuyện của mình, họ dần hiện lên qua các trang sách mà để khi kết thúc ta chẳng thể ghét được ai. Các vấn đề hôn nhân, gia đình, tình dục, tình yêu, sự phản bội được đề cập đến nhưng bị ảnh hưởng bởi giọng văn và các tình tiết nối tiếp nhau nên người đọc bình thản tiếp nhận và thấu hiểu chứ không ngay lập tức lên án ầm ĩ, đánh giá đúng sai. Mình rất thích kết thúc của truyện và tiếp tục cho mình tưởng tượng hành trình tiếp theo của Kaoru và một lúc nào đó Kaoru và Kiwako sẽ gặp được nhau... Tóm lại Bản năng là một tác phẩm có giá trị, đáng đọc và khơi gợi nhiều suy ngẫm.
5
32192
2015-10-14 21:28:16
--------------------------
310162
10744
Khi nói về truyện trinh thám thì đối với mình của Nhật Bản là hay nhất .Nhưng với cuốn Bản năng thì nó hơn cả 1 cuốn truyện trinh thám mà đó còn là tình mẫu tử ,tình mẫu tử của Kiwako người phụ nữ bị ruồng vỏ và Kaoru cô bé con của người tình .Tình mẹ thiên liêng đan xen với cuộc trốn chạy - rượt đuổi đã làm nên câu chuyện tuyệt vời dưới ngòi bút sắc xảo của Misuyo Kakuta .Còn 1 điều mình chưa ưng ý lắm là bìa không đẹp và hút mắt đọc giả lắm dù cốt truyện thì tuyệt vời .
5
191784
2015-09-19 12:51:07
--------------------------
276854
10744
Mình đã mua ngay cuốn sách này sau khi biết nó cùng tác giả với Tôi bị bố bắt cóc. Dù hơi thất vọng vì sách in đã lâu, nên theo kiểu cũ, bìa không chăm chút, giấy cứng và trắng nhưng khi bỏ qua hình thức bắt đầu đọc thì mình bị cuốn hút ngay. Văn phong của tác giả nhẹ nhàng nhưng câu chuyện cực kỳ cuốn hút vì dẫu sao nó cũng là 1 kịch bản trinh thám thú vị. Tình mẫu tử và bản năng người mẹ được miêu tả trong sách thật hay khiến mính muốn có con ngay (mình đã có gia đình) để trải nghiệm những tình cảm đó. 

Mình cũng rất thích câu chuyện về loài ve và lẽ ra sách nên dịch đúng tựa gốc là "Ngày thứ 8" vì nó có liên quan đến triết lý của vòng đời con ve và ngày thứ 8 cũng là ngày cảnh sát bắt đằu chuyển hướng điều tra về Kiwako.

Đoạn chót mình mong mỏi 1 kết thúc khác như trong truyện và rất hồi hộp ở những trang cuối cùng, nhưng tiếc là không được như mong đợi.

Nhưng quyển sách vẫn thật hay và đáng đọc :)
5
16313
2015-08-24 13:50:03
--------------------------
244416
10744
Câu chuyện là sự lồng ghép giữa yếu tố trinh thám và tình cảm đầy cảm động. Minh chứng là Kiwako đã phải trốn tránh pháp luật suốt ba năm rưỡi khi cô lựa chọn bắt cóc con gái của người tính. Để rồi tình cảm của mẹ Kiwako chuyển biến phức tạp vừa hạnh phúc vừa day dứt. Câu chuyện hấp dẫn hơn khi đứa trẻ bị bắt cóc năm đó trưởng thành lại lặp lại hành trình của Kiwako. Thật may mắn là đến cuối cùng Erina chọn chọn cho mình lựa chọn  khác "Tôi muốn nghe nhịp sống của đứa bé. Tôi muốn nghe nhịp sống của tôi."
5
247455
2015-07-28 10:36:09
--------------------------
229165
10744
Cuốn tiểu thuyết này khá là hay vì nó nói lên những bản năng của người mẹ.Tuy nhiên điểm trừ duy nhất là cuốn tiểu thuyết này là có nhiều chỗ tác giả viết khá dài dòng, có đoạn không cần thiết nên làm người đọc hơi mệt.Nhưng đó cũng là điểm cộng cho cuốn sách này vì tác giả có thể miêu tả những dòng cảm xúc nội tâm vừa hạnh phúc vừa dày vò của một người không được làm mẹ khiến cuốn sách này rất cảm động.
4
699783
2015-07-16 15:27:18
--------------------------
