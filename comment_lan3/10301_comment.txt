61633
10301
Đây là một cuốn sách khá hay nói về tình yêu đôi lứa, đặc biệt là tình yêu của những bạn trẻ trong thời hiện đại này. Những câu chuyện trong tập sách không mới, nhưng bao hàm đầy đủ mọi khía cạnh khác nhau của cảm xúc: buồn vui, giận hơn, yêu thương. Tất cả đã làm nên những trang sách nhẹ nhàng, sâu lắng, đôi chỗ rất đáng yêu, khiến độc giả có thể đồng cảm và hài lòng. Cuốn sách giống như một khúc tình ca đương đại, nơi con người được tự do sống theo những gì mình đam mê và khát khao. Đơn giản nhưng tinh tế, sâu sắc, cuốn sách đã chạm đến trái tim của người đọc.
4
20073
2013-03-03 10:43:15
--------------------------
