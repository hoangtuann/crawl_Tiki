219036
10585
Cuốn sách gồm nhiều câu chuyện ngắn về cuộc sống của một cô bé cùng với người cha của cô bé. Tác giả với giọng văn nhẹ nhàng mộc mạc đã truyền đến người đọc những cảm xúc, những suy nghĩ, những lo âu của một người cha dành cho đứa con bé bỏng và của cô con gái dành cho người cha của mình. Thế giới tuổi thơ của những đứa trẻ là những trò chơi, những đòi hỏi bất tận, những câu nói ngây ngô, những lúc khám phá thứ nọ thứ kia, ... bên cạnh người cha cô bé đã có một tuổi thơ vui tươi, ngập tràn niềm vui và hạnh phúc, dù thỉnh thoảng vẫn bị đòn hay bị mắng vì làm sai gì đó nhưng vẫn không so được với niềm vui mà cô bé có được.
Hãy đọc và cảm nhận cuốn sách để không chỉ nhớ về tuổi thơ mà còn nhận ra những gì cha mẹ dành cho con cái chúng ta và cũng để làm kiến thức hay kinh nghiệm nuôi dạy con khi chính chúng ta trở thành cha mẹ.
5
96853
2015-07-01 08:26:02
--------------------------
64336
10585
Đúng như cái tên của mình, cuốn sách khắc họa được một thế giới muôn màu muôn vẻ của tuổi thơ. Tác giả làm sống lại trong lòng người đọc những ký ức về những tháng ngày mình còn là trẻ em, được sống, vui đùa mà không phải lo âu, không phải suy nghĩ thiệt hơn. Những câu chuyện trong cuốn sách có thể là những chuyện nhỏ nhặt nhất, đến những chuyện "to tát" nhất mà một đứa trẻ có thể gặp phải. Tất cả được chuyển tải bằng những câu văn gần gũi, giản dị và giàu cảm xúc, đem đến không chỉ một, mà là nhiều dư âm khó quên cho người đọc.
"Tuổi thơ muôn màu" cũng chính là một ước mơ tươi đẹp, mà ai cũng muốn được đắm chìm trong đó.
4
20073
2013-03-19 19:06:01
--------------------------
