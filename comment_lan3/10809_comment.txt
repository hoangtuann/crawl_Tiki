383073
10809
Phải nói là hình thức bìa bộ truyện Bí mật tình yêu phố Angel của Girlne Ya do Kim Đồng xuất bản rất đẹp, nhưng style font có hơi nhức mắt, nhiều lúc nhìn không ra chữ gì. Tập này cảm thấy ghét con nhỏ Anna nhưng công nhận nhờ sự có mặt của nhỏ mà làm nổi bật lên hình ảnh của một Tô Hựu Tuệ. Mình thích cách cư xử của Tô Hựu Tuệ trong mọi tình huống, luôn lựa chọn cách cư xử và giải quyết vấn đề một cách vô cùng thông minh, thật đúng đắn.
4
595381
2016-02-20 08:29:24
--------------------------
314388
10809
Như tôi đã nhận xét ở tập trước, Anna là một nhân vật gây nhiều tranh cãi, tuy nhiên thì... tôi rất thích cô ấy! Nếu bạn đã đọc Cô nàng xui xẻo, hẳn bạn đã hiểu cô ấy hơn. Một cô gái chảnh chọe với trái tim ấp áp được giấu kín.
Chẳng phải cô ấy đang dần dà giúp Tuệ và Dạ xích lại gần nhau hơn sao?
Tập này đã hé lộ thêm nhiều chi tiết bí ẩn ẩn giấu trong khu biệt thự, rất mong tập sau sẽ có nhiều manh mối hơn. Mấy màn xích mích giữa Tuệ - Dạ vẫn rất đáng yêu, dù là có thêm Anna. Tôi vẫn yêu cái lũ này lắm
5
640394
2015-09-25 22:16:03
--------------------------
265027
10809
Đây ắt hẳn là câu chuyện dành cho những ai đang sống nhiệt huyết ở lứa tuổi học trò cũng như những ai muốn hoài niệm lại tuổi học trò tinh quái ấy. Trong tập này mình rất ngưỡng mộ sự dũng cảm của Tô Hựu Tuệ, nào là lao vào nhà kho đang bốc cháy để cứu Anna (mặc dù Hựu Tuệ chả ưa nhỏ Anna chút nào), nào là nhanh nhẹn đẩy Tiểu Di khỏi miếng bê tông đang rơi, và còn sự dũng cảm âm thầm đó là chấp nhận bị bạn bè hiểu lầm chỉ vì muốn giữ điều bí mật-cái bí mật mà sẽ làm ảnh hưởng đến người bạn ấy. Kim Nguyệt Dạ tuy miệng mồm độc địa và hay chọc tức người khác nhưng đều xuất phát từ ý tốt, chuyện với thầy Hồ tuy cách xử lý hơi "ác" nhưng vậy mới giúp thầy tận gốc được. Mình thích nhất Lý Triết Vũ vì ở cậu ấy toả ra khí chất gì đó rất thu hút. Mạch truyện thay đổi khá hấp dẫn, đoạn đầu còn hài hước và rất teen nhưng đoạn cuối lại xảy ra 1 vụ bắt cóc thật gay cấn và hồi hộp. Văn phong của tác giả trẻ trung và dễ thấm. Chỉ có cỡ 2,3 lỗi đánh máy (thiếu chữ) thôi, mà bìa sách thì quá đẹp !
4
75959
2015-08-13 17:17:27
--------------------------
179263
10809
Cốt truyện nhẹ nhàng, chân thật pha chút lãng mạn và hài hước. Mình kết nhất là tập 4 này với những cuộc ẩu đả, tranh chấp hay những tình cảm nam nữ thời học sinh cấp 3 của Tô Hựu Tuệ và Kim Nguyệt Dạ trong căn hộ số 23 phố Angel đã dần dần thấm sâu, thêm gây cấn và lôi cuốn hơn bao giờ hết. Mình ấn tượng nhất với chương cuối của tập này, 1 căn nhà cháy, 2 con người cùng sự phối hợp ăn ý với các tình tiết làm người đọc hồi hộp đến tột độ. Và mình còn hài lòng với cách trình bày bìa rất ưa nhìn, không màu mè, không sặc sỡ nhưng khắc hoạ cho đọc giả được hình ảnh cực xinh đẹp và nữ tính của Tô Hựu Tuệ và nét hồn nhiên, đáng yêu của cô nàng Tô Cơ.
5
433071
2015-04-06 20:42:02
--------------------------
174718
10809
Tác phẩm này của Quách Ni thật sự rất hay, hấp dẫn. Càng đọc càng thấy hứng thú. Thật sự là có một sức hút vô cùng kỳ lạ, đã đọc tập một rồi lại càng muốn đọc những tập tiếp theo. Thuộc thể loại tiểu thuyết ngôn tình teen nhưng lại rất nhẹ nhàng, trong sáng. Tính cách nhân vật không ai lẫn vào ai, dường như mỗi người lại có một điểm riêng biệt. Rồi tình cảm tay ba dù đau khổ mà cảm động vô cùng. Hệt như Quách Ni đã dồn hết tâm huyết của mình để viết từng lời văn, lời nói vậy ! Mình rất thích tác phẩm này.
5
594111
2015-03-28 19:04:25
--------------------------
138415
10809
Tình tiết ở tập 4 li kì hơn rất nhiều so với 3 tập trước, sự phong phú về tình huống đã thể hiện tài năng của tác giả.Tập 4 là một tập khá xúc động, tôi rất thương Tuệ khi phải đối mặt với nhiều tình huống khó  xử, khiến Tuệ phải đi ngược lại với những người bạn thân, với Tô Cơ, với Dạ, với Vũ, thực sự Tuệ đã phải khổ tâm rất nhiều, luôn bị Anna ép buộc, luôn rơi vào tình thế khó khăn, Việc miêu tả tâm lí nhân vật trong tập này của tác giả khá là xuất sắc, đem lại cho truyện màu sắc phong phú hơn.Và rất may Tuệ đã lấy lại được niềm vi sau cuộc thi Miss Teen Milan, tôi rất thích tập này.
5
412050
2014-12-02 19:35:14
--------------------------
67941
10809
Anna ở tập này sao đáng ghét quá vậy! bắt Tuệ hầu hạ lại còn Làm Tuệ bị mất đi sự tin tưởng của bạn bè .... Hựu Tuệ lại thật tuyệt...vì người bạn thân có thể hi sinh mình... chỉ ức là sao Dạ không tin Tuệ cơ chứ.. hjc hjc.... Ghét girlne Ya quá cơ ... sao lại viết như vậy chứ... Yêu Dạ nhất ở tập ney` nè ! Dạ hiểu Tuệ . Luôn làm Tuê cười tươi... Vì Tuệ mà làm mọi thứ ... Mong dạ bình yên vượt qua sự cố này.. và dược ở bên Hựu Tuệ mãi
5
93704
2013-04-09 17:37:55
--------------------------
35607
10809
Đọc xong tập này tức Girlne Ya, tức ơi là tức.
Mình không hài lòng chút nào.
Sự xuất hiện của Anna làm mọi thứ rối tung lên. Đã vào căn biệt thự cỗ ở, còn bắt Hựu Tuệ làm như một oshin.
Không thích nhất là đoạn Hựu Tuệ đi xem film với Nguyệt Dạ, còn bị Anna lấy mất vé. Nguyệt Dạ cả đêm không về nữa chứ. Ôi, sao Hựu Tuệ yếu ớt thế không biết.
Mọi thứ như trái với tự nhiên.
Hựu Tuệ phải ra tay đánh Tô Cơ chỉ để bảo vệ bí mật, không để cô bạn đau lòng. Tất cả cũng chỉ tại Anna đó hâm dọa.
Còn nữa, lúc bị bọn Hắc Long bắt cóc, Hựu Tuệ đã không ngại nguy hiểm vào nhà kho cứu Anna. Nếu các bạn là một người từng bị Anna đối xử thê thảm như thế, các bạn có đủ can đảm vào nhà kho đang bốc cháy để cứu Anna không?
Tô Hựu Tuệ rất dũng cảm!
Thế nhưng rốt cuộc, trận đấu đó Nguyệt Dạ vẫn thắng. Girlne Ya thật bất công! Tại sao cậu ta không làm gì, vô dụng còn bị trật chân, chấp nhận ở lại chờ chết mà còn được thắng vinh quang cơ chứ? Hựu Tuệ làm vất vả, một tháng chịu mọi khổ sở thế mà thua. Ôi, bất công quá, tội nghiệp Tô Hựu Tuệ.
Mình vẫn theo dõi, hy vọng Girlne Ya cho Hựu Tuệ vinh quang một lần...
4
39173
2012-08-02 08:42:24
--------------------------
4605
10809
Chuyện gì sẽ xảy ra với Tô Hựu Tuệ khi ngài chủ tịch lại cho 1 đề thi oái oăm nhất thế gian và vị giám khảo lại chính là cô bạn barbie Anna không đội trời chung với cô?Để bảo vệ cái bí mật vế 1 quá khứ quá tàn nhẫn mà người bạn thân nhất của cô không được phép biết để tránh sự tàn nhẫn đó phá hoại cả 1 cuộc đời của người bạn đó,Tô Hựu Tuệ phải trở thành vật hi sinh, làm tôi tớ, thậm chí còn phải ra tay đánh chính người bạn mà cô yêu quý nhất và kết cuộc là tất cả đều bỏ cô ở lại,tất cả đều rời bỏ cô mà đi,để lại 1 mình cô ở khu biệt thự số 23 ma quái. Không 1 ai,không 1 ai hiểu cho sự hi sinh đó của cô. Tập 4 trái ngược hẳn với 3 tập đầu,không còn vui vẻ nữa,không còn nhí nhảnh nữa,mà nhuộm 1 màu phẫn uất,đau đớn,tủi nhục và cả tội lỗi nữa 
Tiếp sau đó là những tình huống gây cấn với trận đánh nhau cùng bọn xã hội đen, 1 tình yêu sâu sắc nhưng lại bị gia đình ngăn cả. Sự đánh đổi cả tính mạng chỉ để cứu người mà mình yêu. Liệu Kim Nguyệt Dạ và Lý Triệt Vũ có tình cảm gì đối với Tô Hựu Tuệ?. Liệu Tô Hựu Tuệ có đáp trả lại tình cảm đó không? Và cô sẽ đáp trả lại ai? Cuối cùng ai mới là người yêu cô nhất? Hãy tìm câu trã lời trong tập 4 của " Bí mật tình yêu phố angel"-1 tập truyện buồn nhưng thấm đẫm ý nghĩa của sự hi sinh, hi sinh vì tình bạn và hi sinh vì tình yêu
5
5191
2011-05-19 20:22:41
--------------------------
4249
10809
Tập này hơi buồn, Anna đối xử với Tô Hựu Tuệ như vậy là quá đáng, bắt Hựu Tuệ phục vụ, còn đem chuyện của Tô Cơ ra uy hiếp Hựu Tuệ, bắt Hựu Tuệ phải tát Tô Cơ, khiến cho hai người giận nhau. Nhưng Tô Hựu Tuệ thì khác, không còn sĩ diện như những tập trước, những việc làm cho Hựu Tuệ rất đáng yêu, còn Kim Nguyệt Dạ thì khỏi chê luôn, từng cử chỉ hành động, dù bên ngoài tưởng chừng như luôn hiếp đáp Hựu Tuệ nhưng thật ra lại luôn quan tâm đến cô, chấp nhận hi sinh mạng sống vì cô. Thật là mong chờ tập 5 quá đi, không biết Nguyệt Dạ có sao không.
5
1351
2011-05-12 20:32:58
--------------------------
4227
10809
Tập này mình thấy ghét Anna kinh khủng, không hiểu sao trên đời có người ác như vậy nữa, rất thích Hựu Tuệ ở tập này, làm mọi việc đều dễ thương. Kim Nguyệt Dạ lại càng tuyệt, mọi biểu hiện đều thể hiện hết là mình thích Hựu Tuệ rồi, thích cả cái cách mà Dạ kích tướng để cứu Hựu Tuệ, lúc nào cũng cố tỏ vẻ độc ác, gian mãnh nhưng luôn cười để không làm Hựu Tuệ sợ, kết quả là "hi sinh" vì Hựu Tuệ luôn rồi, hi vọng sẽ không có chuyện gì xảy ra, mong cho Dạ sẽ không có chuyện gì. Tập này tình tiết thiệt là hay.
5
4038
2011-05-12 11:07:59
--------------------------
