169920
10030
Tôi biết tiến sĩ Stephen R. Covey qua hai quyển sách của ông là 7 thói quen để thành đạt và thói quen thứ 8. Qua cách viết và trình bày trong sách, tôi nhận thấy cố tiến sĩ là một con người không những nguyên tắc trong công việc mà ông còn áp dụng những nguyên tắc vào cuộc sống riêng của mình để cảm thấy cân bằng giữa gia đình, công việc và đời sống cá nhân. Muốn lãnh đạo được mọi người, toàn thể công ty hay tổ chức, chúng ta cần phải quản trị và lãnh đạo thật tốt bản thân mình. Theo ý kiến cá nhân của tôi, chúng ta nên dùng quyển sách này vào việc quản trị và lãnh đạo bản thân của chúng ta và sau đó mới là gia đình và mọi người.
5
387632
2015-03-19 02:15:16
--------------------------
52811
10030
Stephen R. Covey là một tác giả rất nổi tiếng trên thế giới, ông có rất nhiều đầu sách thu hút bạn đọc, cuốn hay nhất của ông chính là cuốn "7 thói quen để thành đạt". Tiếp nối những thành công đó thì cuốn Nghệ thuật lãnh đạo theo nguyên tắc cũng là một quyển sách rất đáng đọc. 
Nếu bạn là một nhà lãnh đạo thì bạn không thể bỏ qua cuốn sách này, nó sẽ đem đến nhiều kiến thức bổ ích mà một nhà lãnh đạo cần có, từ những cách ứng xử bên ngoài đến cả cách hành động làm việc sao cho có hiệu quả nhất. Điều này không phải ai cũng làm được, mình thấy nhiều nhà lãnh đạo tuy tài giỏi trong công việc nhưng chưa chắc thành công trong việc ứng xử với cấp dưới, quyển sách này sẽ đem đến cho bạn một cái nhìn đầy đủ hơn những phẩm chất mà một nhà lãnh đạo cần có.
4
60663
2012-12-28 20:27:08
--------------------------
