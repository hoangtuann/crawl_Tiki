406030
10325
Thiệt tình là bé mình chưa đủ khả năng hiểu hết nội dung sách dạy. Vì bé chưa nhận diện nhanh mặt chữ số. Nhưng bé lại rất hiếu kỳ và ham học hỏi. Nên dù ko hiểu hết nhưng bé chỉ lật xem và xem nhiều lần thì có khả năng ghi nhớ về sau.
Mình thích sách vì hình ảnh đẹp, màu sắc tươi, chất lượng giấy tốt, nội dung phong phú dù hơi khó diễn đạt cho con hiểu. 
Nhưng nên kiên nhẫn với con vì toán học là cả 1 hành trình dài. Cần xây dựng cho con hiểu ý nghĩa thiết thực và đam mê học trong bé trước.
5
332721
2016-03-27 16:50:24
--------------------------
162193
10325
Ở quyển này các bé bắt đầu làm quen với các con số, làm quen với toán học. Các bài tập tập trung vào nhớ thứ tự các số từ 1 đến 20 bằng cách nối số, cách tính toán qua các phép cộng trừ đơn giản, cách cân bằng trọng lượng. Các bài luyện không quá khó nhưng đòi hỏi phải nhớ thứ tự trước sau của số. Để làm được bài tập trong sách, các bé phải viết được một số chữ cái như A, B, C và các số từ 1 đến 10. Sách phù hợp với các bé học lớp 1 hơn là mẫu giáo.
4
534227
2015-03-01 19:20:01
--------------------------
