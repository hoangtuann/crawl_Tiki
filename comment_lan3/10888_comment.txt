456883
10888
Hôm mình mua thì cả bộ này đều được sale hết lại đọc nhận xét thấy mấy bạn khen quá nên hốt đại về. Vậy mà đọc ngấu nghiến hết 3 quyển chưa đến một tuần. Hình minh họa dễ thương, lại rất hợp với trí tưởng tượng nữa chứ. Bìa cũng rất đẹp, chỉ có một điểm trừ nhỏ xíu là không có bìa gập. Nội dung thì rất sáng tạo, có nhiều đoạn rất lôi cuốn, giọng văn không quá rườm rà. Nói chung lại, nếu bạn yêu thích người cá thì nên sở hữu ba quyển sách này.
5
502455
2016-06-23 20:50:28
--------------------------
383891
10888
Tôi đã đọc tập 1 Emily Winsnap: Cái Đuôi Của Emily, và tôi rất ấn tượng. Nhưng tập 2 của bộ sách này không đem lại cho tôi cảm giác ấy. Tôi cũng chẳng rõ vì sao nữa, bởi cốt truyện vẫn hấp dẫn và kỳ bí, bối cảnh dưới đáy biển thậm chí còn đẹp và ấn tượng hơn. Tôi cực không thích tính cách Emily ở tập này, cô bé quá ngang bướng và ngốc nghếch, ở đoạn cuối còn dùng dằng mãi việc đối diện với con quái vật trong khi bao nhiêu mạng người, có cả mẹ cô bé, đang phụ thuộc vào việc đó. Hơn hết mạch truyện cũng có vẻ thiếu chắc chắn, các diễn biến nó không liên kết thế nào đó. Nói chung tôi cho 3 sao thôi.
3
386342
2016-02-21 13:57:34
--------------------------
349159
10888
Tiếp nối phần một câu chuyện về nàng tiên cá hiện đại Emily, phần hai tác giả đưa người đọc trải nghiệm cuộc sống mới của Emily với thân phận là người cá ở hòn đảo mới! Vẫn là một câu chuyện mới mẻ và rất thú vị, phần hai kịch tính hơn phần một vì ở tập này Emily đã gặp phải nguy hiểm chết người bởi tính tò mò của mình. Kết cấu chuyện hợp lý, hấp dẫn nhưng cũng như phần một, mình chưa hài lòng lắm về cách giải quyết vấn đề của câu chuyện, có phần hơi dễ dàng so với kỳ vọng của mình. Mình đã trông chờ sự bùng nổ và kịch tính hơn khi Emily đối đầu với quái vật.
4
135597
2015-12-07 21:01:28
--------------------------
333451
10888
Tôi không ngần ngại mua liền hai tập cuối của series về cô bé Emily Winsnap người cá ngay khi sách được giảm giá, bởi vì tôi đã đọc tập 1 và rất ấn tượng. Và tập 2: “Emily Winsnap và Quái vật dưới vực thẳm” còn hấp dẫn hơn nữa. Những bí ẩn về đại dương thì chưa bao giờ hết lôi cuốn, lần này độc giả sẽ cùng Emily tiếp tục khám phá thế giới huyền bí nơi đáy biển Becmuda, mà nhắc đến Becmuda hẳn ai cũng biết đó là vùng biển kì bí và đáng sợ thế nào. Tác giả Liz Kessler rất hay khi mượn dữ kiện có thật về Tam Giác Quỷ để sáng tạo nên một câu chuyện cực kỳ li kì và đặc sắc. Tôi thực sự rất thích cuốn này, nhưng chỉ cho 4 sao vì sách Phương Nam đầu tư không tốt, chưa được đẹp.
4
6502
2015-11-07 22:03:26
--------------------------
257374
10888
Mình đến với Emily Windsnap vào một lần đi chơi ở nhà sách Nha Trang, vô tình tìm thấy nó trong kệ hàng giảm giá. Lúc chọn mua cũng chỉ đơn thuần vì bìa đẹp đúng kiểu mình thích, nhưng không ngờ khi về đọc thì lại mê hẳn. Phiêu lưu, kỳ bí, thú vị, vui tươi, cũng không kém phần hồi hộp. Vô cùng ưng ý với quyển truyện này. Sau đó một thời gian, mình mới biết bộ này còn 2 tập nữa và quyết định đi lùng. Tiết là ở chỗ mình không có bán. Đứa bạn kêu lên mạng tìm thử. Cũng có tìm nhưng rồi cũng thôi vì không quen mua trên mạng. May sau này dần quen với cách mua hàng tiki nên sẵn hốt lun 2 tập còn lại. Không hề hối hận chút nào. Cho mấy đứa bạn mượn ai cũng khen hay hết. 
5
453611
2015-08-07 16:51:13
--------------------------
226455
10888
Tập 2 của bộ truyện về người cá này còn hay hơn cả tập 1. Câu chuyện về cô bé nửa người nửa người cá Emily Windsnap tiếp tục, gây cấn hồi hộp. . Bầu không khí rất thần thoại, rất cổ tích, rất bí ẩn, bao trùm câu chuyện có thể gợi sự tò mò trong bất cứ độc giả nào. Và sự trong sáng, hồn nhiên ở cách kể chuyện càng khiến cho câu chuyện thêm thu hút. Bộ truyện thích hợp cho những bạn thích truyện thể loại viễn tưởng, phiêu lưu hấp dẫn. Rất đáng đọc.
4
180961
2015-07-12 11:11:41
--------------------------
133343
10888
Tiếp nối với tập 1, tập 2 của bộ truyện mở ra những chuyến phiêu lưu của cô bé người cá đáng yêu Emily. Ở tập 2 này, mạch truyện có vẻ nhanh hơn với những pha đối đầu, chiến đấu đầy khó khăn của Emily với bọn quái vật. Bên cạnh đó, cũng như tập 1, tập 2 đề cao tình bạn đẹp đẽ, vượt qua mọi nguy hiểm của Emily và Shona. Đây có lẽ là tác phẩm đầu tiên về người cá mà nhuộm đầy tính phiêu lưu mà tôi được đọc. Cốt truyện lạ hòa với tình tiết đầy lôi cuốn, khá phù hợp với những bạn trẻ thích phiêu lưu.
4
82481
2014-11-04 22:57:03
--------------------------
81980
10888
Sau khi đọc xong tập 1 của bộ truyện - "Cái đuôi của Emily Windsnap" - tôi đã quyết tâm phải tìm mua bằng được tập 2. Và khi đọc đến dòng cuối cùng của tập 2, tôi biết cuốn sách đã không làm mình thất vọng. 
Rất tuyệt vời! Một câu chuyện đầy màu sắc cổ tích, nhưng cũng rất phiêu lưu, rất kịch tính, đồng thời lại có cả yếu tố trẻ thơ, trong sáng và hồn nhiên. Sự xuất hiện của con quái vật khiến tôi rất bất ngờ. Những tình tiết truyện cuốn hút tôi một cách kì lạ. Những gì tôi rút ra được sau khi đọc bộ truyện là một bài học về tình yêu thương, sự kiên trì vượt qua khó khăn và một bài học về sự đa dạng, phong phú, muôn hình vạn trạng cũng như sự bí ẩn của biển cả bao la. Rất thú vị!
4
24661
2013-06-19 14:14:44
--------------------------
