84009
10786
Có thể nói ngoại tình là một vấn đề trở nên phổ biến trong xã hội hiện nay. Ngoại tình chính là thể hiện phần " con" và phần " người" của mỗi con người. Tác phẩm đưa ra sự đấu tranh giữa phần "con" và phần " người" của nhân vật, dù yêu nhau, dù tình cảm mạnh mẽ, sâu sắc đến đâu, cho dù có phút yếu lòng, nhưng lý trí hay phần " người" mạnh hơn đã giúp cho nhân vật dứt ra được một cuộc tình ngang trái, bởi rằng " ngoại tình" sẽ đi đến đâu và sẽ phải đánh đổi cả gia đình hạnh phúc của mình, theo mách bảo của trái tim tìm đến với người đó, nhưng liệu rằng trái tim chỉ đường có đúng không?
Một câu chuyện, một thức tỉnh cho cuộc sống hiện đại, có phần " loạn" ở Việt nam hiện nay, khi mà ngoài tình trở nên phổ biến, ông ăn chả, bà ăn nem được đề cập rất nhiều trên các trang báo. Mong răng phần " người" ở mỗi người đủ mạnh, để chiến thắng phần " con" trong xã hội đầy cám dỗ, và đặc biết sau khi đọc tác phẩm này.
3
127507
2013-06-28 14:36:32
--------------------------
