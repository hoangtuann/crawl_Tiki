138289
10729
Mặc dù bìa cuốn sách giới thiệu là hữu hiệu dành cho nhiều lứa tuổi nhưng cá nhân mình là một người trẻ sau khi đọc xong cuốn sách này thì lại cho rằng nó chỉ phù hợp với bộ phận một số người và đăc biệt hữu ích đối với các bé tiểu học hoặc trung học cơ sở. Nội dung cuốn sách xoay quanh các phương pháp học tập hiệu quả như bản đò tư duy, cách đọc nhanh, cách chọn sách hay, cách tạo động lực trong học tập và những câu chuyện có ý nghĩa giáo dục về bác học Lê Quý Đôn. Đan xen trong cuốn sách là những hình ảnh minh họa, những câu chuyện, ví dụ rất hài hước giúp người đọc vui vẻ khi đọc sách. Không thể phủ nhận nội dung lành mạnh và có ích của cuốn sách nhưng mình thấy cuốn sách còn một vài nhược điểm như là chưa đi sâu vào phân tích vấn đề theo nhiều mặt và các nội dung còn đơn giản, không mang tính mới mẻ. Nhưng mình nghĩ cuốn sách sẽ rất hữu ích đối với các em nhỏ vì nó khá dễ hiểu và hài hước.
3
485671
2014-12-01 21:08:24
--------------------------
