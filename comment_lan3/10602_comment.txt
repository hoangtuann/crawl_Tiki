461475
10602
So với các tập truyện khác thì tập này khá nhạt. Hoạ sĩ Gosciny đã có những sáng tạo đối với nhân vật Jesse James và đồng bọn cướp của chúng, nhưng tiếc rằng, việc sự thật lịch sử bị bẻ cong lần này lại khiến cho câu chuyện tẻ nhạt và đơn giản. Diễn biến vẫn đúng chất ông Gosciny nhưng việc đơn giản hoá tính cách, bản chất, hình tượng nhân vật phản diện đã làm mất đi sự lôi cuốn, nghẹt thở với những pha tranh đấu cân tài cân sức. Về bản dịch của First News thì lúc nào cũng làm mình hài lòng, rất chuyên nghiệp, dịch rất hay.
4
668946
2016-06-27 20:37:06
--------------------------
