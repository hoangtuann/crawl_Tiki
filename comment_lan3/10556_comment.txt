476073
10556
Tôi rất ưng ý với quyển sách nhỏ này. Tôi có con nhỏ, rất thích nghe đọc truyện. Nhưng với kinh phí hạn hẹp nên tôi thường cân nhắc kỹ mỗi khi lựa chọn một quyển sách. Thật sự bây giờ với ma trận sách nhiều lúc tôi cũng bối rối do đó tiêu chí của tôi trước hết là những quyển sách kinh điển dành cho thiếu nhi giá cả phải chăng. Đây là một quyển sách đáp ứng tiêu chí trên. "Gió qua rặng liễu" là tác phẩm nổi tiếng của Kenneth Grahame, đây là quyển sách tôi đã đọc trước đây, sách có nội dung hay. Quyển sách này nhà xuất bản in khổ nhỏ, chữ to rõ ràng, nhiều hình minh họa màu, sách không quá dày, thích hợp cho trẻ nhỏ mới tập đọc sách. Chúc các bạn có sự lựa chọn ưng ý.
5
588918
2016-07-18 13:21:04
--------------------------
