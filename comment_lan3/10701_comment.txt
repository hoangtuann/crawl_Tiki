236237
10701
Cấu trúc cuốn sách này rất chặt chẽ, có các bài nghe từ dễ đến khó để luyện cho từng part của kỳ thi TOEIC. Tuy nhiên chỉ phù hợp với các bạn có ý định ôn lâu dài, còn với các bạn thi cấp tốc (ví dụ muốn thi trong 1 tháng) thì khó có thể hoàn thành hết các bài test trong sách. Theo mình đánh giá thì actual test của sách khó hơn thi thật nên luyện xong cuốn này đi thi sẽ không bị bỡ ngỡ. Rất highly recommend cho bạn nào đang ôn thi TOEIC, đặc biệt là các bạn còn yếu phần nghe.
5
362
2015-07-21 17:10:46
--------------------------
