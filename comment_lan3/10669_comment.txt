77928
10669
Câu chuyện làm mình thật sự xúc động. Mình yêu quý và cảm thương cô bé vì trong đêm Noel lạnh giá phải lặng lội bán từng que diêm để không bị người cha tàn ác đánh mắng. Mình xót xa khi thấy em mơ được ăn ngỗng quay, được sưởi lò sưởi và được ôm cây thông Noel, và mình rơi nước mắt khi em được gặp người bà quá cố, được cùng bà bao lên cao, bay mãi chẳng còn để chẳng phải nếm mùi đau khổ, tủi hờn của thế gian. Mình căm hận người cha tàn ác, căm hận những con người vô tâm đưa mắt nhìn em bé tội nghiệp...Qua câu chuyện, mình học được lòng nhân ái, sự quan tâm đến những người nghèo khó và không ngược đãi với trẻ em...
5
120263
2013-05-30 12:20:04
--------------------------
