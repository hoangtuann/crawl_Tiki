403846
10385
Mình rất thích tựa của cuốn sách này, nghe thôi đã hiện lên trong đầu đó là một câu chuyện tình dài và trên con dường tình yêu đó rất nhiều chong gai, đau khổ có, hạnh phúc có. Tuy nó không nhiều chi tiết kịch tính nhưng vẫn có nhiều những điểm nhấn rất hay. Những con nười đầy sức sống, mạnh mẽ vượt qua những bất hạnh để đến được với hạnh phúc của mình một cách ngọt ngào nhất, Tác giả đã gửi vào đây những tình cảm riêng của mình, những tình cảm ấy đề có một nét đẹp riêng của nó. Mình rất hài lòng với tác phẩm này.
4
782953
2016-03-24 12:32:58
--------------------------
360923
10385
Dường như con người không hề vô vọng trong cách giải thích trước mọi vấn đề khó khăn , dù là phận người phụ nữ nhưng ý chí vững bền càng làm ta khâm phục tính cách đa phần nhiều đức tín ngưỡng , có thể loại bỏ thói xấu của thứ tồn tại trên cõi đời này sự đen tối , nham hiểm đều mất đi những cái vô cùng viển vông , chứa tạo nên lực đẩy tiến tới sức chặt trong cách nghĩ cảm quan chỉ đơn thuần ban bố chưa đều đặn khiến ta xao lãng lòng mình .
3
402468
2015-12-30 02:39:38
--------------------------
354079
10385
Mình đọc cuốn Những con chữ tình yêu của Katie Fforde rồi, tiện Tiki giảm giá nên mình mua tiếp cuốn này. Văn phong của Katie Fforde nhẹ nhàng mà rất thu hút. Mình đọc sách của Katie Fforde thường đọc 1 lèo chứ không rứt ra được. Mà điểm mình thích nhất về truyện của Katie Fforde là xây dựng nhân vật rất phù hợp, cùng đẳng cấp, chứ ko phải vô lý như ngôn tình.

Mình mua cuốn này được tặng kèm 1 bookmark theo bìa sách nên nhìn khá dễ thương. Bìa in rõ đẹp, chất lượng giấy cũng ổn. 

Cuốn này điểm trừ duy nhất là chưa có cao trào lắm, mình nghĩ tác giả mà viết thêm 1 chút là xứng đáng 5 sao luôn. Đọc hơi nhẹ nhàng nên bạn nào mong kịch tính thì chắc không hợp.
4
43721
2015-12-17 11:58:53
--------------------------
309905
10385
“ Hành trình tình yêu”  của Katie vẫn theo trường phái nhẹ nhàng, lãng mạn, không hẳn là kịch tính, gây cấn nhưng vẫn có điểm nhấn. Một cuộc chay trốn của một cô gái và một phụ nữ, một chuyến du lịch trên biển của họ, cuộc gặp gỡ bất ngờ, một tình yêu lãng mạn, nội dung không có gì mới nhưng Katie Fforde đã thổi một làn gió sống động đầy thú vị vào tiểu thuyết qua cách ứng biến, cư xử của các nhân vật ; những con người đầy sức sống và mạnh mẽ, sau bao bất hạnh thì niềm tin vẫn còn đó và tình yêu vẫn mãi là chìa khóa của hạnh phúc.
4
39798
2015-09-19 10:41:17
--------------------------
306442
10385
Truyện đọc có cảm giác dài dòng quá, tác giả đi quá sâu vào những chi tiết không cần thiết, những miêu tả khá rườm rà nên khi đọc qua mấy chương đầu tôi cảm thấy hơi nản, vẫn cố gắng đọc đến hết truyện thì có thấy khá hơn. Chuyến hành trình đi đến Hà Lan của Jo và Dora diễn ra khá thú vị và cũng nhiều trải nghiệm, cùng đồng hành với họ còn có cả Marcus và Tom, chính cuộc du hành trên con thuyền này đã bắt đầu nảy sinh thứ tình cảm nhẹ nhàng giữa 4 người họ.
Truyện như là một bản nhạc êm dịu, không có sóng gió về cuộc sống đời thường của các nhân vật, như là một cuốn sách về gia đình vậy, nơi mà vui có, buồn có, ghen tuông có,… nhưng rồi tất cả đều đi đến tận cùng của cái kết viên mãn, người nào yêu nhau thì đến với nhau. Truyện nói về hai nhân vật chính là Jo và Dora nhưng tôi nhận thấy tác giả chỉ tập trung vào Jo, còn Dora với Tom thì nói không nhiều, tôi thích có thêm nhiều tình tiết lãng mạn cho cặp đôi này hơn. Nhìn chung thì tác phẩm này ổn, vừa đủ để tôi thấy hài lòng nhưng vẫn chưa đủ độ tinh tế và đặc sắc.

3
41370
2015-09-17 16:28:00
--------------------------
262786
10385
Thực sự thì tôi bất ngờ khi cầm cuốn sách trong tay vì đơn giản là tôi nghĩ rằng Jo và Dora là những người phụ nữ trẻ trung cùng tuổi. Vâng! Tôi đã nhầm. Và có vẻ tôi thích chuyện tình của Tom và Dora hơn vì họ còn trẻ, mặc dù chuyện của Marcus và Jo cũng khá hay ho và phải nói thêm là tôi khá ấn tượng với tình yêu lâu bền mà Marcus dành choi Jo nhưng thành thực mà nói tôi không hứng thú chuyện tình của người già cho lắm (xin lỗi, không phải tôi kì thì hay gì đâu). Câu chuyện khá nhẹ nhàng ngay cả biến động cũng không thực sự đáng chú ý. Nhưng nó vẫn cho tôi cái cảm giác tự do vì được sống theo ý mình, làm những điều mình thích, thử những cái mới mà mình chưa từng dám nghĩ đến, và đặc biệt là sẵn sàng đón nhận tình yêu ngay khi nó đến. Một cuốn sách phù hợp cho một ngày mưa lười biếng.
3
230601
2015-08-12 09:25:06
--------------------------
235919
10385
Hành trình tình yêu là một tiểu thuyết như được đúc kết từ cuộc sống, nhẹ nhàng, giản dị như những gì vẫn diễn ra quanh ta, đôi khi ta thấy mình đâu đó trong cuốn sách này. Văn phong của Katie Fforde không có gì quá nổi bật, các nhân vật của bà cũng vậy, không kịch tính, không táo bạo, nhưng cũng không nhàm chán. Jo và Dora là 2 kiểu phụ nữ phổ biến trong cuộc sống nên khá gần gũi với người đọc. Mạch truyện đều, các tình tiết hợp lý như vốn dĩ sẽ như vậy. Mình thích Marcus vì tình yêu ông dành cho Jo trước sau vẫn không thay đổi. Một cuốn sách nên đọc khi bạn cảm thấy thiếu niềm tin trong tình yêu.
4
646635
2015-07-21 14:00:38
--------------------------
218436
10385
Sau khi đọc Hành trình Tình yêu và Những con chữ tình yêu của Katie Fforde, tôi yêu thích giọng văn nhẹ nhàng, lãng mạn của nữ tác giả. Những trải nghiệm của các nhân vật trên những chiếc thuyền  và cách thể hiện tình yêu thi vị của các nhân vật thật sự cuốn hút tôi qua từng trang sách. Câu chuyện không có những kịch tính hấp dẫn vậy mà cứ nhẹ nhàng trôi qua từng trang sách. Cảm giác sau khi đọc xong truyện thật là thoải mái và cảm thấy yêu đời. Đối với tôi đây là câu chuyện đáng đọc
4
219702
2015-06-30 16:25:13
--------------------------
164999
10385
Cũng giống như các tác phẩm khác của Katie Fforde, Hành trình tình yêu cũng được viết với văn phong hiện đại mà nhẹ nhàng. Truyện của bà thường chú trọng vào các chi tiết, do đó hơi dài dòng, nhưng nội dung thì rất thú vị. Hành trình tình yêu kể về hai tuyến nhân vật chính là Dora, cố gái trẻ chạy trốn cuộc hôn nhân đã định và Jo, người phụ nữ tuổi trung niên đang trốn chạy nỗi đau bị người chồng bao năm đầu gối tay ấp phản bội. Họ cùng sống trên một chiếc thuyền và chuyến hành trình đi đến Hà Lan trên chiếc thuyền ấy cũng chính là chuyến hành trình đưa họ đến với bến bờ tình yêu thực sự. Truyện thực sự rất lãng mạn và không kém phần hài hước với nhiều tình huống hay ho và thú vị, mình chấm truyện này 4 sao.
4
17740
2015-03-09 13:54:15
--------------------------
154858
10385
Tôi thấy thích cuộc sống của Jo và Dara, nếu được thử cuộc sống lênh đênh tự do trên một chiếc tàu hẳn sẽ rất thú vị. Còn về lý do họ đến sống trên chiếc tàu đó – tôi thấy cũng gần như là một cuộc chạy trốn vậy. Đúng vậy, họ chạy trốn khỏi sự sợ hãi, thất vọng về đàn ông, những điều trong quá khứ khiến họ muốn đóng cửa con tim mình lại. Nhưng có hai người đàn ông đã đến và kiên nhẫn gỡ bỏ cánh cửa đó. Một happy ending quá tuyệt cho câu chuyện lãng mạn của hai người phụ nữ này ^^
4
418163
2015-01-30 16:35:13
--------------------------
82933
10385
Trước tiên đọc lời tựa của truyện mình thấy khá hay, hi hi. Khi đọc rồi thì đúng là không bị thấy vọng :) Có lẽ nét lãng mạn, nhẹ nhàng là nét riêng của tác giả rồi. Mình cũng có đọc vài truyện của bà và thấy mỗi truyện đều có những nét hay riêng. Về câu chuyện của Jo và Dora tuy không có nhiều kịch tính hay bất ngờ nhưng cũng rất thu hút. Mình rất thích theo dõi cuộc sống của Jo và Dora trên con tàu ba Chị em, thích những chuyện bình thường diễn ra hằng ngày trên con tàu đó. Tóm lại đây là một truyện khá hay, để lại cho người đọc nhiều cảm xúc thú vị.
4
126898
2013-06-24 10:48:23
--------------------------
