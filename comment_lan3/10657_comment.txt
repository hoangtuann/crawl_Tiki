503593
10657
Sách hay, nội dung ngắn gọn, súc tích phù hợp cho các bé mần non. Bé nhà mình rất thích. Đêm nào cũng năn nỉ mẹ đọc cho con nghe 5 truyện thì mới chịu ngủ, hôm nào ngoan còn năn nỉ mẹ đọc thêm 1, 2 truyện. Thấy con thích nghe kể truyện, nghe đọc sách mẹ rất vui vì đã tập cho con được 1 thói quen tốt.
5
1974384
2017-01-01 15:40:06
--------------------------
403252
10657
Mua sách bây giờ không khó! Nhưng để mua một quyển sách ưng ý cũng không dễ. Để mua được một quyển sách ưng ý mình đã bỏ khá nhiều tiền nhưng có nhiều quyển mình cảm thấy chưa hài lòng mấy! Nhưng với cuốn truyện 1001 câu chuyện bốn mùa thì mìn khá là hài lòng và cũng giới thiệu cho nhiều mẹ có con trong độ tuổi 2-5 tuổi! Thậm chí mình còn mua tặng cho một vài người bạn, nhưng khá là đáng tiếc khi mình nghe họ nói không có thời gian kể chuyện cho con nghe! Hơi buồn! Vì quyển sách có nội dung phong phú, hình vẽ đẹp và khá là hay! 
5
1085969
2016-03-23 14:55:33
--------------------------
