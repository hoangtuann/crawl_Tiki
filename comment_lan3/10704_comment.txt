33357
10704
Đối với phần lớn mọi người, việc thay đổi bản thân mình dường như là điều khó khăn nhất. Nhưng ai cũng hiểu, để hạnh phúc và thành công, chúng ta cần thay đổi rất nhiều như giảm thiểu và loại bỏ những thói quen xấu, xây dựng và củng cố các thói quen tốt. 

Trong cuốn sách Thay đổi (Switch) của 2 anh em Dan & Chip Heath - một cuốn sách tuyệt vời của 2 tác giả tôi rất mê, các tác giả nêu lên 3 vấn đề lớn trong việc thay đổi:
- Cảm xúc - với hình tượng là con voi
- Lý trí - với hình tượng là người quản tượng
- Cách làm - với hình tượng là con đường
Hãy đọc để tìm ra được hạnh phúc bạn đang kiếm tìm!
5
46938
2012-07-12 23:32:00
--------------------------
