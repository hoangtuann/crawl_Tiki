419662
10437
Cuốn Tuyển Tập Những Câu Hỏi Luyện Thi Năng Lực Nhật Ngữ tôi thấy nó cũng được, dành cho bạn nào muốn ôn luyện năng lực tiếng Nhật ở mức độ cao cấp (cấp 1 trong tiếng Nhật là cấp độ cao nhất).  Sách có 3 đề luyện thi với đầy đủ các dạng bài khác nhau chữ hán, từ vựng, ngữ pháp, đọc, nghe và phần sau là script phần nghe + đáp án.  Sách được nhà xuất bản boc bằng nilon rất dày giống như bookcare của tiki vậy. Nên mình rất hài lòng vì như vậy sẽ bảo quản được lâu và còn kèm CD luyện nghe nữa, mặc dù giấy cũng ko được tốt lắm. Hi vọng nhà xuất bản sẽ cho ra đời nhiều tập khác hơn nữa.
4
1302823
2016-04-21 18:17:42
--------------------------
166056
10437
Bạn sẽ khóc sau khi mua cuốn này nếu bạn không phải là người đã nắm vững Tiếng Nhật.
Dù biên soạn cho người Việt nhưng sách lại không có Romanji, thậm chí Furigana cho các từ Kanji khó. Ngay cả người Nhật cũng phải thi bằng Nhật ngữ và bằng Kanji, vậy người Việt ta học Tiếng Nhật nhắm có đủ sức để đọc được cuốn sách này ? Thậm chí không hiểu người ta nói gì trong đó.
Là sách ngoại ngữ tượng hình, sách lại in khá đậm nên nét chữ có khi bị chồng lên nhau nên khá khó nhận dạng Kanji. Bìa trang trí khá xấu và trông tối tăm. Một cuốn sách quá khó chăng ?
1
298441
2015-03-11 20:12:15
--------------------------
