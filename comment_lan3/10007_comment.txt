463714
10007
Thế là bộ truyện tranh Miko đã chào đón một nhân vật mới, đó là Momo - em gái của Miko. Lúc đầu mình không ngờ gia đình Miko lại chọn tên cho em bé một cách chóng vánh như vậy nhưng sau khi đọc chuyện bên lề mới biết tác giả cũng đã mất nhiều công sức để lựa chọn. Bé Momo nhìn đẹp hơn Miko hồi nhỏ quá trời nhưng không biết mốt lớn có sở hữu chiều cao giống chị mình không đây. Bìa của tập truyện này cũng được thiết kế rất dễ thương. Yêu dễ sợ!!!
5
709791
2016-06-29 15:52:35
--------------------------
463059
10007
Truyện tranh nhóc Miko kể về những câu chuyện đời thường hết sức ngộ nghĩnh, dễ thương của cô bé học sinh cấp 1 Miko sẽ đưa độc giả trở về những năm tháng tuổi thơ tươi đẹp, ngọt ngào mà chúng ta từng trải qua. Truyện được viết theo kiểu nhiều truyện nhỏ những mẩu truyện đem lại nột bài học đáng nhớ không kém phần đáng yêu. Truyện tranh nhóc miko là bộ truyện tranh mình yêu thích nhất vì sự dễ thương và hay ho của truyện. Bạn nào yêu thích kiểu truyện học trò tình cảm ngốc xít đáng yêu thì hãy mua đọc thử.
5
393298
2016-06-29 02:10:47
--------------------------
457776
10007
Cuốn " Nhóc miko : cô bé nhí nhảng - tập 19" là 1 trong những cuốn  mình cực kì thích.Đầu tiên là bìa truyện siêu đẹp,phông đã đẹp hình còn đẹp hơn lại ý nghĩa nữa ( miko và momo mặc đồ đôi vô cùng tình cảm ở hành động nhóc momo giơ bánh đưa cho miko ấy) nên mình thích lắm.Tiếp theo là nội dung truyện,đọc tập này lại thấy ghen tị với những bạn mà có anh chị em nhiều lắm,vì mình là con một nên đối với những cảm xúc của Miko khi được bế em,ghét khi mình và momo mặc đồ đôi mà mọi người chỉ khen nhỏ em...Đây thật sự là 1 tập hay và ý nghĩa mọi người nên đọc.
5
1419914
2016-06-24 15:58:02
--------------------------
432283
10007
Đây là quyển tôi rất thích vì có sự ra đời của nhóc momo - cô em gái của miko và mamoru. Câu chuỵên ấy thật ý nghĩa nó nói lên sự vất vả và thiêng liêng của người phụ nữ nói lên sự đau đớn trong phòng sinh bên cạnh đó là niềm hạnh phúc của một người phụ nữ khi được làm mẹ đó là điều tôi thật sự rất thích trong tập truỵên này tôi đã đọc đi đọc lại rất nhiều lần thấy nó rất ý nghĩa thấy yêu mẹ hơn cảm nhận được nỗi đau của mẹ qua từng bức tranh từng câu nói của nhân vật. Bên cạnh đó còn có những câu chuỵên hài hước thật sự thú vị và giải trí trong tập truỵên lần này
5
1266408
2016-05-18 21:26:38
--------------------------
311106
10007
Trong tập này Momo nhóc em của Miko chào đời,một bé gái thực sự rất đáng yêu qua nét vẽ của tác giả,nhìn Miko đi đưa mẹ vào viện để đẻ mà nhớ lần đầu tiên mình cũng chờ đợi ngoài phòng sinh ngồi trông hoài sốt ruột nữa khi chờ chị mình vào sinh em bé, cái cảm giác thật tuyệt khi nhìn thấy bé, được ẵm rồi ru bé ngủ..trong góc trò truyện tác giả có chia sẻ về thời gian hoàn thành một tập truyện 17 trang nhóc Miko, canh chia khung, vẽ phác thảo rồi đi nét cả nghĩ nội dung mới thấy được mỗi tập truyện mình cầm trên tay thực sự đang quý như thế nào.
5
165748
2015-09-19 21:32:01
--------------------------
297060
10007
Trong các tập trước, Miko luôn là cô bé vô tư, hồn nhiên, vô lo vô nghĩ. Nhưng đến tập này thì Miko chững chạc hẳn ra nhé, Miko có thêm em nữa mà. Trong tập này, mình thấy Miko đã biết lo lắng hơn cho mẹ và em. Miko còn đặt tên cho em nữa đấy nhé. Tập này do có nhân vật mới xuất hiện nên em ấy có mặt trong khá nhiều truyện. Nhưng không vì vậy mà làm cho truyện nhàm chán một chút nào cả. Nhờ vậy mà tình cảm chị em của Miko được thể hiện rõ nét hơn bao giờ hết. Mình yêu chị em Miko quá.
5
247850
2015-09-11 15:44:07
--------------------------
294792
10007
MOMO-thành viên mới trong gia đình Miko là một bé gái cực kỳ đáng yêu. Từ khi mẹ có thai cho đến khi em Momo ra đời, Miko đã lớn lên rất nhiều: biết lo lắng cho mẹ, không còn quậy cho căn phòng lộn xộn, biết quan tâm mẹ và em gái,...Đặc biệt, lúc ngắm nhìn Momo hay học cách ẵm em thì Miko lại rất nghiêm túc. Tuy nhiên, Miko vẫn rất nghịch ngợm và hậu đậu khi ở trường hay đi chơi với các bạn. Ngoài ra, tác giả Ono Eriko còn mang đến phần ngoại truyện cực kỳ hài hước nữa!
5
95034
2015-09-09 18:23:17
--------------------------
287270
10007
Ở những tập trước, Miko là cô bé hồn nhiên, nhí nhảnh nhưng khi đọc tập này mình thấy Miko chững chạc hẳn. Khi mẹ đang ở phòng sinh, cô bé hết sức lo lắng, đứng ngồi không yên, khóc vì lo cho mẹ . Được bế em trong lòng bàn tay, cô bé đã hết sức hạnh phúc,vui sướng. Tình cảm của Miko dành cho đứa em gái nhỏ được thể hiện qua cái tên ý nghĩa : Momo. Đọc tập này, tác giả làm cho người đọc cảm thấy thấm thía hơn về tình chị em. Cảm ơn ONO Eriko đã đem đến câu chuyện hay và ý nghĩa như thế này.
5
268628
2015-09-02 15:19:35
--------------------------
284507
10007
Tập 19 này có lẽ là một bước ngoặt quan trọng trong gia đình Miiko, báo hiệu những câu chuyện "cười ra nước mắt" được tác giả tung hứng trong những tập sắp tới. Momo đã chính thức là thành viên của gia đình Yamada- một cô bé vô cùng đáng yêu với đôi mắt to tròn và đôi má hồng đào phúng phính dễ thương. Miiko- một cô bé mà đôi khi khiến chúng ta nghĩ rằng vô lo vô nghĩ- lại vô cùng lo lắng cho mẹ, bồn chồn đến mức phải gọi điện cho mọi người để trấn an bản thân rồi lại lặng lẽ rấm rứt khóc. Miiko lo cho mẹ vì đường "sơn đạo" của mẹ khúc khuỷu, dẫn đến khó sinh, rồi vui sướng khi thấy Momo và đã tự mình đặt tên cho cô bé ấy. Tình ruột thịt thật gần gũi và thiêng liêng, và như bao lần mình đã khẳng định, những chương liên quan đến tình cảm gia đình luôn cho mình một cảm xúc khó tả, khiến mình trân trọng hơn bao giờ hết.
5
364412
2015-08-30 23:57:10
--------------------------
265283
10007
Tập này gia đình nhóc Miko chào đón thành viên mới, đó là Momo- cô em gái với cái tên có ý nghĩa "màu hồng". Khuôn mặt cô bé tác giả vẽ đáng yêu quá đi. Có vẻ việc chào đón thành viên mới đối với Miko và Mamoru đều gặp 1 chút rắc rối, nhưng cả hai vẫn háo hức và dành nhiều tình cảm cho cô em của mình. Trong tập này, lúc Miko đưa mẹ đi sinh, Tappei cũng có xuất hiện và đã trở thành chỗ dựa cho Miko rất nhiều. Cậu đã đi bộ dưới trời tuyết, qua 3 ga tàu để đến với Miko.Thật là 1 cậu bé ga lăng ^^. 1 điều thú vị khác ở tập này là Miko được Yoshida mời đến nhà ăn Giáng sinh, haha, nhìn cái cách tác giả vẽ Miko ăn thật dễ thương biết chừng nào. Đây thực sự là 1 tập truyện rất hay.
5
281150
2015-08-13 21:14:39
--------------------------
223924
10007
Tập này hay và ý nghĩa thật, Miko cùng với gia đình cùng đón chào thành viên mới - bé Momo (có nghĩa là màu hồng, nghe cái tên là thấy dễ thương rồi!), tên do Miko đặt cho em gái mình đấy. Cô bé Miko tuy bình thường hay quậy phá nhưng khi mẹ chuẩn bị sinh em bé thì cũng ra dáng người chị lắm đấy! Còn Tappei tuy hay chọc phá Miko nhưng lại rất quan tâm cô bé, biết Miko phải một mình chờ mẹ sinh em bé liền đi tới bệnh viện mặc trời tuyết, đúng là anh chàng tốt bụng.
Đọc Miko luôn đem đến mình những tiếng cười, những giây phút thư giãn, riêng tập này thì thấy ấm áp tình thân gia đình quá!
5
315293
2015-07-07 20:13:25
--------------------------
124216
10007
Tập này kể về việc gia đình của Miko đón một thành viên mới đó chính là em gái của Miko : Momo . Tôi rất thích cái tên này theo lời Miko Momo là một cái tên rất ý nghĩa và cũng rất đáng yêu . Cách Miko lúng túng khi thấy Momo bật khóc cách Miko tìm đủ trò để làm cho Momo cười làm tôi phải bật cười vì sao thấy Miko giống mình quá .Những nhân vật trong truyện được tác giả khắc họa rất dễ thương và nhí nhảnh đặc biệt là nhóc Miko láu lỉnh đáng yêu . Truyện kể về nhiều thứ tình bạn , tình thân . Những chủ đề về giới tình được tác giả khắc họa một cách nhẹ nhàng đáng yêu hết có thể . Phải nói rằng đây là bộ truyện viết về đề tài thiếu nhi hay nhất mà mình từng đọc . Vừa ý nghĩa lại vừa buồn cười
5
337423
2014-09-06 08:35:24
--------------------------
78179
10007
Đọc tập này tự nhiên nhớ lại hồi mình sắp có em. Vẫn là một Miko hồn nhiên nhí nhảnh nhưng tập truyện này đã cho tôi cái nhìn khác về cô bé này – một người chị đầy trách nhiệm, luôn quan tâm chăm sóc em của mình. Tác giả không chỉ dừng lại ở việc thảo những nét vẽ dễ thương vui tươi nhằm chọc cười bạn đọc mà đó còn là những câu chuyện hết sức ý nghĩa về tình bạn, gia đình hay những rung cảm đầu đời. Ai bảo Nhóc Miko chỉ dành cho thiếu nhi ? Mình học lớp mười hai rồi mà vẫn chết mê  bộ truyện này, nhiều đứa trong lớp sau khi đọc vài tập mình đưa cũng lên Tiki đặt mua cả bộ luôn.Vừa có thể giải trí vừa ý nghĩa. Mình nghĩ nhà xuất bản bỏ dòng chữ”Truyện tranh dành cho thiếu nhi đi là vừa!”
5
108847
2013-05-31 14:16:56
--------------------------
72824
10007
Nhóc Miko hấp dẫn từ đầu cho tới mãi bây giờ! Tập 18 gia đình Miko đã bí mật cho người xem biết về nhân vật mới sắp xuất hiện ở Nhóc Miko: Cô Bé Nhí Nhảnh - Tập 19. Khi đọc thì tôi thấy bình thường thôi, nhưng lúc đọc tới đoạn mẹ Miko sinh em bé thì thấy cực kì vui. Thế là từ nay truyện Nhóc Miko lại có thêm một bé gái cực kì đáng yêu. Đó là Momo. Câu chuyện về nhóc Miko lại tiếp diễn với thật nhiều tiếng cười và nhiều hơn nữa là những bài học quý giá dành cho các em nhỏ nhưng phải làm anh chị của đứa em nhỏ hơn mình. Những bài học rất đáng yêu làm tôi thấy truyện tranh thật sự cũng góp phần nào trau dồi tình yêu thương giữa những người thân trong gia đình, giữa bạn bè cho các tâm hồn trẻ thơ.
5
104652
2013-05-04 22:19:14
--------------------------
64439
10007
Miko phát hành vào những năm tôi học lớp 12, là thời kỳ chao đảo với việc ôn luyện cho những cuộc thi quan trọng. Nhưng chưa tuần nào bỏ lỡ Miko, vì nó mang lại cho chúng tôi những nụ cười giúp thay đổi không khí, giảm bớt căng thẳng. Nét ngộ nghĩnh trong hình vẽ, đáng yêu, nhí nhảnh và nhiều lúc ngây ngô trong lời nói, hành động của nhân vật làm người đọc cười đau hết cả bụng. 
Trước đây không được mua, bây giờ tôi phải sưu tập lại đủ bộ mới được :)
5
82490
2013-03-20 09:03:48
--------------------------
62092
10007
Tôi không phải dân ghiền truyện tranh! Đối với tôi, nhất là những manga Nhật tôi lại chẳng thích tí nào! Nhưng Nhóc Miko thì khác đấy, chính nhóc đã đưa tôi đến thế giới niềm vui. Những câu chuyện của nhóc cùng với đám bạn và gia đình đã cho tôi nụ cười. Những lúc căng thẳng, được ở bên nhóc Miko đó cũng niềm hạnh phúc đơn giản. Các nhân vật khác cũng dễ thương không kém, nào là Mari, cậu em trai, những đứa bạn trong lớp và cả ba mẹ Miko... Mỗi câu chuyện là một chuỗi những mắc xích tiếng cười trong sáng, vui nhộn, tinh ranh.
Dù đã lớn hay tuổi nhỏ, nhóc Miko luôn là người bạn thân thiết của tôi! 
Thích lắm, cô nhóc nghịch ngợm đầy đáng yêu này! Nếu chưa biết thì bạn phải mau mau làm quen với Miko đi nhé!

5
20191
2013-03-06 21:57:16
--------------------------
36732
10007
Nhóc Miko là câu truyện về cô bé Miko nghịch ngợm, lười học giống như nhiều cô cậu bé khác. Nhưng Miko lại có một tấm lòng ấm áp, biết bao dung, vị tha. Câu chuyện về Miko hài hước, đầy tiếng cười, nhưng bên cạnh đó cũng đậm chất nhân văn. Ai nói đằng sau những câu chuyện hài hước không có những bài học thú vị.
Bộ truyện này tôi đã từng xem và rất thích. Nhưng bị gián đoạn cách đây mấy năm ở tập 20. Bây giờ đã được tái bản. Tôi thật sự rất vui. Hi vọng bộ truyện sẽ được tiếp tục !
5
51345
2012-08-14 16:49:52
--------------------------
