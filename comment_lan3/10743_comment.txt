429205
10743
Cuốn sách nói về thiên nhiên và những điều mà chưa chắc ai cũng biết. Thiên nhiên là nơi mang đậm màu hoang dã và còn rất nhiều những điều lí thú. Cuốn sách ko khô khan như lời thầy cô giảng bài trên lớp mà hoàn toàn dễ hiểu. Nó đã tái hiện lại thiên nhiên nơi có những động vật hoang dã. Chúng giết nhau để sinh tồn hay sống cộng sinh với nhau trong hòa bình.
Mình có một bộ sách về KTHD nhưng mình thấy cuốn sách nào cũng hay và bổ ích cả. Cảm ơn tiki!  ^^
5
1337568
2016-05-12 20:02:50
--------------------------
405596
10743
Thiên nhiên hoang dã....nghe tên thôi đã thấy kích thích..nhìn đến cái bìa thì chính là rùng rợn. Các loài vật luôn luôn rất đáng ngờ, qua mấy nét vẽ cùng những lời văn hài hước, dí dỏm....thiệt là không thể sống nổi trong cái thiên nhiên quá ư là hoang dã này, nào rắn quấn quanh, nào cóc nhảy chồm hỗm.....rợn cả người......Nhưng đương nhiên ko chỉ giúp xả street mà nói còn cung cấp thêm bao nhiêu kiến thức cần thiết cho ta nữa, nếu không đọc chắc không thể biết loài động vật đa dạng, phong phú và nham hiểu thế nào đâu....Quả là không uổng khi mua quyển này....
5
860561
2016-03-26 22:35:35
--------------------------
397105
10743
Hầu hết các cuốn sách của Nick Arnold đều là những cuốn sách hay thú vị và khai sáng đầu óc về những kiến thức khoa học. Và cuốn sách này  cũng không ngoại lệ.Những thông tin trong cuốn sách được viết dưới dạng những câu những chữ rất ngộ nghĩnh, hài hước, dễ tiếp thu.Và ngoài ra nó còn được minh họa bằng những hình ảnh ngộ nghĩnh hài hước.Những kiến thức trong cuốn sách này trả lời cho chúng ta câu hỏi: Chúng ta đang sống trong sự nguy hiểm như thế nào.Khám phá tự nhiên thông qua cuốn sách này thật sự rất thú vị . Mình rất thích phong cách chuyên nghiệp về cách thức giao hàng và đóng gói của Tiki. Cảm ơn Tiki.
5
747943
2016-03-14 14:43:26
--------------------------
385447
10743
Thiên nhiên hoang dã rùng rợn đến rụng tim.Có thể mỗi sáng mai thức dậy bạn sẽ có một con mèo xù lông hay con chó tinh nghịch nằm phía cuối giường nhưng đây là quyển sách nói về những con vật có hàm răng khổng lồ,những con khác lại hút máu và sống ở nơi tăm tối,chúng tởm lợm đến kinh khủng nhưng nó là những khía cạnh hấp dẫn và hoang dã nhất của thiên nhiên và qua cuốn sách này bạn sẽ thấy thiên đối với bạn không còn xa lạ nữa mà hấp dẫn và phong phú vô cùng.
5
846812
2016-02-23 23:35:05
--------------------------
378771
10743
Mình rất thích tìm hiểu về thiên nhiên hoang dã và rất vui vì cuốn sách này có hàng trên tiki và còn được giảm giá những 30%, những cuốn trong bộ này khá là khó kiếm ở ngoài cửa hàng sách và còn đắt hơn nữa chứ.
Sách in đẹp, giấy màu ngà dễ đọc, tranh minh họa đẹp, các font chữ dễ nhìn.
Nội dung hay và thú vị như vẽ lên trước mắt mình một thiên nhiên hoang dã với các loài động vật như hổ, trăn và vô số các loài cây, động vật thú vị.
5
306081
2016-02-07 13:52:28
--------------------------
254390
10743
Đây là cuốn sách mình rất thích tron bộ Horrible science bởi vì mình rất thích khám phá thiên nhiên xung quanh. Mình cũng vô cùng yêu thích phong cách viết của Nick Arnold, một phong cách hành văn dí dỏm, đáng yêu cùng những bức tranh minh họa hết sức hài hước.Cuốn sách đã đem đến cho mình thế giới sinh động của những con vật ngoài thiên nhiên hoang dã. Tạo hóa thật bất ngờ và luôn công bằng, chúng ta ai cũng có những khả năng sẵn có để đối mặt với cuộc sống.Thật kì diệu biết bao!
5
280592
2015-08-05 10:37:28
--------------------------
247067
10743
Quyển sách 'Thiên Nhiên Hoang Dã" là quyển đầu tiên mình đã đọc trong bộ sách "Horrible Science" do một người bạn giới thiệu từ lâu và nó thật sự rất hay, mình thật sự rất thích bộ sách này. Luôn luôn là vậy hài hước và dí dỏm là đều không thể thiếu trong quyển sách này. Nó đã cung cấp cho mình rất nhiều những thông tin thú vị về thế giới của những loài vật, cách chúng sinh tồn, chống lại kẻ thù hay cả sự dẻo dai đáng nể mà con người không thể nào có được,.. Mình gợi ý các bạn có thể tìm thêm quyển sách "Thú Dữ" để có thể hiểu thêm về thế giới loài vật vô cùng rộng lớn của chúng ta nhé!
5
649807
2015-07-30 09:46:42
--------------------------
243857
10743
Mình cực thích bộ truyện này. Bữa trước mình đi xa mua 1 cuốn về làm quà cho em gái, ai ngờ ngay cả mình đọc vào cũng bị thu hút. Vậy nên mình quyết định mua thêm vài cuốn để cho em gái cũng như mình đọc. Cuốn sách này khai thác nhiều về Thiên nhiên, nó thật kì bí và có nhiều điều lí thú mà ta chưa được biết đến. Nó làm người đọc càng thêm hứng thú tìm hiểu về sự hoang dã ấy. Hình ảnh minh họa ngộ nghĩnh và thú vị, phong chữ cũng bắt mắt, đọc rất nhanh và để lại ấn tượng trong lòng người đọc.
4
39744
2015-07-27 20:12:21
--------------------------
218796
10743
Ai cũng biết rằng thiên nhiên chốn hoang dã là một chốn luôn đầy rẫy nguy hiểm những mấy ai biết rõ được về thế giới hoang dã đó. Với lời văn dí dỏm, dễ hiểu, hình ảnh minh họa sinh động ngộ nghĩnh, những thông tin bất ngờ về thế giới hoang dã ,… sẽ đưa bạn vào chuyến phiêu lưu kì thú cùng với các nhà thám hiểm dũng cảm giúp ta hiểu rõ hơn về các loài động – thực vật và cách chúng sinh tồn trong thế giới đầy rẫy nguy hiểm này. Một cuốn sách thú vị và bổ ích cho mọi người thích phiêu lưu khám phá trong thế giới tự nhiên bí ẩn này.
5
554214
2015-06-30 21:34:02
--------------------------
