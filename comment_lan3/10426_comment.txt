180753
10426
Ngân sách là kế hoạch cho những hoạt động trong tương lai. Nếu bạn hiểu được tầm quan trọng của ngân sách trong việc dự thảo và hoạch định chiến lược thì điều này sẽ giúp bạn thành công. Quyển sách này sẽ giúp bạn hiểu rõ những điều đã nêu trên đồng thời quyển sách sẽ trang bị cho bạn một số kiến thức như: cách thức chuẩn bị cho dự thảo ngân sách (thiết kế ngân sách để đạt mục tiêu), cách dự thảo ngân sách như thế nào và giám sát ngân sách một cách hiệu quả ra sao. Sách này có thể sử dụng cho nhiều đối tương. Sách trình bày ngắn gọn, rõ ràng.
5
588073
2015-04-10 05:16:43
--------------------------
