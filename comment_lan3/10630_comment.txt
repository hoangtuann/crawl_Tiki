531997
10630
Phải vất vả quyết tâm lắm mới đọc xong. Bìa sách giống hình lấy mấy trang web cho teen. Một câu chuyện không có gì nổi bật, cốt truyện quá bình thường, không có cao trào gì. Cách viết dông dài, kiểu như tác giả nghĩ rằng viết như vậy thì hay. Tóm lại cảm thấy khâm phục bản thân khi đọc hết quyển sách.
2
84106
2017-02-26 18:12:02
--------------------------
326936
10630
Mình thật sự không thích cuốn sách này lắm nhưng vẫn giữ nó lại.
Cuốn sách này không đọng lại trong mình quá nhiều, nó rất nhẹ nhàng, không kịch tính không sâu sắc.
Quỳnh- một cô gái khá hồn nhiên, vô tư cũng rất dễ thương trải qua một số chuyện mà dần khép kín và e dè hơn.
Đăng- tác giả xây dựng về nhân vật này khiến mình cảm thấy anh chàng không được mạnh mẽ lắm, không toát được khí chất của nhân vật nam chính trong các chuyện tình cảm, anh Đăng không đủ mạnh mẽ, mình thật sự không thích nhân vật này chút nào, có cảm giác anh này hơi đàn bà.
Tuy nhiên câu truyện cũng khá dễ thương, rất nhẹ nhàng.
2
334588
2015-10-26 21:03:23
--------------------------
302949
10630
Lại thêm một câu chuyện của Trần Thu Trang.Không quá kịch tính,nhưng đủ kích thích sự tò mò phải đọc đến hết để tìm đáp án.Và theo lời tác giả, khi có những tình tiết chưa đc giải đáp là để dành câu trả lời cho độc giả,một sự kết nối khá ăn khớp đã làm nên một cái kết làm thỏa mãn người đọc.
Và một điều nữa thích ở Trần Thu Trang là sự kết nối giữa các nhân vật trong các câu chuyện của mình. Từ Thái Vân trong " phải lấy người  như anh " , đến Hoài Đan trong " coktail tình yêu " và giờ là Đức của " để hôn em lần nữa ". Khiến ta giật mình thích thú khi nghĩ rằng câu chuyện lại gần gũi hơn, chân thực hơn khi các nhân vật trong các câu chuyện lại " quen biết " nhau
Gấp sách lại, với ham muốn đc đọc thêm nhiều hơn nữa.
4
777603
2015-09-15 15:53:16
--------------------------
233749
10630
Đã mua cuốn sách vào ngày 10/5/2015
Đây là cuốn sách thứ 3 của tác giả Trần Thu Trang mà tôi tìm hiểu sau khi đã đọc 2 cuốn sách " Phải lấy người như anh" và "Cocktail Cho Tình Yêu". Quả thực so với 2 cuốn sách trước, cuốn sách này không để lại cho tôi nhiều ấn tượng lắm. Vẫn là giọng văn tưng tửng, hài hước nhưng không kém phần sâu sắc của chị Trang nhưng trong cuốn sách này có vẻ như chị không khai thác được hết nội tâm cũng như không tạo hình được rõ nét tính cách của nhân vật chính giống như 2 tác phẩm" Phải lấy người như anh" và "Cocktail Cho Tình Yêu".Tôi cảm thấy tính cách của 2 nhân vật chính có phần chung chung và hơi mờ nhạt, đặc biệt trong truyện còn có nhiều khúc mắc chưa được sáng tỏ như thân thế của gia đình Quỳnh,hay diễn biến nguyên nhân dẫn đến cái chết của Hằng được lí giải rời rạc, không thuyết phục người đọc...Tuy nhiên, nếu để đọc trong lúc giải trí thì cuốn sách này cũng không tệ. Hi vọng sẽ sớm được đọc những tác phẩm mới của chị Trang
3
294286
2015-07-19 21:25:20
--------------------------
219364
10630
Đây là tác phầm đầu tiên của Trần Thu Trang mà mình đọc được. Thực ra thì mình đã đọc được phần lớn truyện qua trang mạng nhưng lại không phải là toàn bộ. Lần sau đó mình ghé qua tiki tìm nhưng lúc ấy thông báo hết hàng. Mãi tới gần đây lần nữa tìm lại, may mắn thay vì tiki đã có sản phẩm và mình đã đặt mua nó. Mình thấy được sự day dứt đau thương trong cảm nhận của Quỳnh dù cho cô không phải người có lỗi. Thực sự mình rất tiếc về sự việc đã xảy ra, nó khiến cho Quỳnh thay đổi từ một người con gái tinh nghịch trở nên ít nói... Với Đăng, Quỳnh là một cô gái đặc biệt khiến anh khó lòng quên đi, thiết nghĩ ngoài nụ hôn ấy ra còn có những lí do nào đó nữa. Xuyên suốt câu chuyện, ai cũng có thể bắt gặp những hình ảnh về một Hà Nội giản dị, cổ kính đã nhuốm dần cái màu thời gian xưa cũ: những con đường, những cơn mưa, những góc phố... Ở nhân vật chính, họ phảng phất đâu đó nét gần gũi, nhẹ nhàng của những con người Hà Nội đáng mến... Và họ nhất định sẽ hạnh phúc, bên nhau...
4
431542
2015-07-01 13:56:10
--------------------------
215527
10630
Vôn yêu thích Thu Trang qua giọng văn cá tính của cô trong Độc thân, cần yêu, nhưng đến tựa sách này thì không được ấn tượng như trước nữa. Mạch truyện thiếu sức hút, các nhân vật cá tính mà nhạt, cách hành văn cũng chưa tạo được sự khác biệt. Có vẻ cô hơi gồng quá nên chưa tạo nên được một câu chuyện cuốn hút xuyên suốt. Giọng  văn lần này của cô cũng đằm thắm hơn, không còn trẻ trung và sống động như trước. Bìa sách cũng không gây sự chú ý gì nhiều, có lẽ do mình quá kỳ vọng nên hơi hụt hẫng.
3
13083
2015-06-26 14:17:35
--------------------------
173716
10630
Tôi là một fan cuồng của Trần Thu Trang, có thể nói tất cả những tác phẩm của cô tôi đã đọc hết. Và đây cũng là một tác phẩm mà tôi rất thích. Đây là cuốn sách mà tôi đọc đến ba lần. Và sau mỗi lần đọc là chút gì đó lâng lâng, chút gì đó hoài niệm. Một câu chuyện tình có sự bắt đầu bằng một nụ hôn, và kết thúc cũng là một nụ hôn. Nếu hành trình Đăng tìm kiếm Quỳnh, giải thoát Quỳnh khỏi những ám ảnh ngày xưa. Đó cũng chính là hành trình Hải Đăng tìm thấy một nữa của mình, hành phúc của mình.
5
405354
2015-03-26 17:16:47
--------------------------
142229
10630
Câu chuyện tình yêu giữa Đăng và Quỳnh trong truyện Để Hôn Em Lần Nữa thật sự đẹp, ngọt ngào và lãng mạn.
Mối duyên của 2 người bắt đầu với tình nguyện ở những bản làng xa, mối duyên bị chia cắt sau cùng lại gặp nhau trong cùng một nơi làm việc. Mối duyên đó đã đúc kết nên thành một tình yêu đẹp giữa 2 người.
Câu chuyện tình yêu ngọt ngào mà Đăng dành cho Quỳnh làm người đọc ngưỡng mộ. Những mặc cảm nội tâm của Quỳnh đã được tình yêu của Đăng tháo gỡ, giúp Quỳnh dám sống thật với quá khứ của mình.
Một câu chuyện không cao trào, không kịch tính này nhưng vẫn có sức hút níu giữ mình đến hết quyển truyện, bởi lời văn sâu sắc hay bởi vị ngọt ngào trong tình yêu mà tác giả vẽ ra?
Một quyển sách cần đọc và cảm nhận!
4
303773
2014-12-17 22:23:28
--------------------------
116993
10630
Có lẽ là do tác giả chuyên nghiệp viết nên chuyện dù không phải motip mình thích cho lắm nhưng mình lại rất thích và thấy rất hay. Nhân vật nữ chính, nam chính, họ xuất hiện trong những khung cảnh thật dễ thương  như qua một lần cùng làm tình nguyện viên ở bản xa, gặp lại nhau trong cùng một văn phòng  và cùng làm việc, là 2 người duy nhất đội mưa đi làm vào ngày đại hồng thủy…
Đăng giúp cho Quỳnh vượt qua những khó khăn và mặc cảm tội lỗi đeo bám cô suốt 4 năm qua, hướng cho cô đến một cuộc sống mới: quá khứ không thể thay đổi nhưng mà hiện tại và tuong lai thì có.
Truyện rất chân thực, dễ thương và rất đáng đọc chỉ tiếc là bìa sách không đc đẹp cho lắm nhưng các bạn cũng nên đọc nó.
5
166271
2014-07-14 16:09:00
--------------------------
115846
10630
Truyện sắp xếp khá ổn. Nhưng đọc dường như không có điểm nhấn, đọc mau cho hết câu chuyện vì truyện cứ lẩn quẩn quanh cái thứ cảm xúc mà tác giả muốn bọc lộ ra. Triyeejn đọc không cao trào, cứ làng nhàng đến chán ngẫm. Khi đọc xong quay lại hỏi: ủa hết òi á hả. Tôi bị nó thu hút vì cốt truyện lạ- mới, xong tôi thất vọng  khi đọc nó. 
Cô nhân vật chính moeeu tả nội tâm chưa sâu sắc, đan xen những hình ảnh tưởng tượng( huyền huyễn ko ra huyền huyễn, hiện thực hk ra hiện thuc. Đọc rất ảo) làm chúng ta ko pt đọc 
Nói chung nhân vật ko để lại ấn tượng sâu sắc, đọc xong thẩy zo một góc
3
250007
2014-07-02 13:31:36
--------------------------
104505
10630
Tôi thiết nghĩ đa phần những bạn còn đang độc thân hay đang mang trong mình tư tưởng FA thì khi đến với "Để hôn em lần nữa", họ sẽ một lần nữa theo từng câu văn trong tác phẩm mà tìm về những cảm xúc, những dấu vết yêu thương (hay đau khổ) ngày xưa mà họ hoặc vô tình hoặc hữu ý che giấu suốt bao năm tháng qua. Để từ đó,  đôi lúc bạn sẽ phải xót xa cho Quỳnh, cho những đau khổ mà Quỳnh phải mang lấy mặc dù đó không phải là lỗi của Quỳnh. Trái tim của bạn sẽ đập mạnh hơn vì đọc thấy cái cách yêu thương nồng cháy của Đăng giành cho Quỳnh, Đăng yêu Quỳnh nhiều lắm nhưng anh vẫn chờ đợi đến một ngày Quỳnh bước ra khỏi "vỏ ốc" - được tạo nên từ những vết thương trong quá khứ để đến ngày đó Quỳnh sẽ sẵn sàng để yêu anh và để "được anh hôn lần nữa".  Chính vì những lý do trên tôi mới đặt tiêu đề cho nhận xét này là "Mối đe dọa cho hội FA", trái tim các hội viên sẽ trở nên mềm yếu đấy!
Điều làm tôi thích thú nhất sau câu chuyện tình yêu của Đăng và Quỳnh đó là "Hai người không quên nhau" trong phần ngoại truyện của tác giả, tác giả thật thông minh khi mang đến cho độc giả của mình một cơ hội để nhìn thấy một con người khác của Điệp ("nhân vật phản diện" trong Để hôn em lần nữa) - đáng trách nhưng cũng vô cùng đáng thương và giàu nghị lực sống và một cơ hội khác để độc giả có thể tự viết nên câu chuyện của riêng mình về các nhân vật trong tác phẩm.
4
193564
2014-01-14 08:54:34
--------------------------
101639
10630
Thích cách viết của Trần Thu Trang trong "Để Hôn Em Lần Nữa". Mạnh mẽ khi cần, và nhẹ nhàng khi có thể. Thật sự, đây không hẳn là một câu chuyện thật sự hấp dẩn, lôi cuốn..nhưng câu chuyện của "Để hôn em lần nữa" có sự bất ngờ. Thêm vào đó, lối viết văn giản dị đã mang người đọc trở về quá khứ rồi quay lại hiện tại mà không làm người đọc ngỡ ngàng. Nó đơn giản như món bảo bối của Doremon mà Quỳnh rất thích.
Tuy nhiên, nếu không gặp lại nhau, phải chăng Quỳnh và Đăng đã lạc mất nhau...khi cả hai mới chỉ dừng lại ở một nụ hôn? Đầu truyện cứ ngỡ, hai nhân vật phải có tình cảm sâu đậm và đặc biệt lắm...nhưng đâu ngờ...chỉ là một sự "cảm nắng nhẹ". Thật sự mà nói....vẫn có một chút cảm giác tình yêu của hai nhân vật chính chưa đủ lớn.
3
140655
2013-12-08 00:34:56
--------------------------
77221
10630
Tôi luôn thích cách viết của Trần Thu Trang, có một cái gì đó tôn trọng người phụ nữ, chính điều này thôi thúc tôi luôn sưu tầm những tác phẩm của chị. Truyện lần này cũng vậy, có một thứ gì đó sâu sắc mà tôi cảm nhân được. Bìa truyện không quá đẹp, cái tựa đề với khung đặt ở giữa truyện có thể nói hơi bị '' vô duyên'' , kích cỡ truyện cũng tương đối nhỏ, khá dễ đọc. Nội dung truyện cũng không quá mới lạ, cũng rất quen thuộc nhưng dưới ngòi bút của chị thì nó đã khác hẳn, rất chân thực. Tình yêu của hai nhân vật chính cũng vậy, rất nhẹ nhàng, ngọt ngào, bình yên, không có quá nhiều tình tiết cao trào nhưng rất sâu lắng.
4
57869
2013-05-26 17:27:26
--------------------------
61032
10630
Truyện của Trần Thu Trang luôn cho tôi một cảm giác chân thực rất kỳ lạ. Câu chữ của chị cho tôi những hình dung cụ thể và thật như bản thân đang đứng cạnh những nhân vật tiểu thuyết vậy, và "Để hôn em lần nữa" cũng không ngoại lệ.

Tôi ấn tượng với "Để hôn em lần nữa" ngay từ tựa đề. Cái tên ấy gợi mở nhiều thứ trong suy nghĩ của tôi, từ tưởng tượng của một sự xa cách đến một chàng trai thâm tình và có vẻ... mặt dày. Và khi lật những trang sách, tôi bật cười vì mình có vẻ "bắt được sóng" của tác giả thật, hoặc cũng có lẽ là bởi Trần Thu Trang biết cách hé mở qua tên sách. Và thích nữa, là bìa sách. Tôi luôn thích cách Trần Thu Trang chăm chút cho cuốn sách của mình, kể từ trang bìa. Bìa sách lần này đủ ... mập mờ, đủ nặng để gánh cái tên "Để hôn em lần nữa", và đủ để "câu" tim tôi.

Nhưng đó là phần vỏ, cái ruột của "Để hôn em lần nữa" khắc khá sâu trong tôi. Câu chuyện này, tôi thấy không có cái nét ngày cũ, như "Cocktail cho tình yêu" hay "Phải lấy người như anh". Nó đơn giản hơn, mà cũng phức tạp hơn. Những nỗi đau, day dứt của Thủy Quỳnh không mới trong văn học, nhưng cái thật của cách viết Trần Thu Trang lại cho tôi cảm giác câu chuyện này đang trải ra nội tâm của rất nhiều con tim.

Nhưng cái kết của truyện lại cho tôi chút hụt hẫng, chỉ là cảm giác, không mấy rõ nét.
3
23329
2013-02-27 17:53:01
--------------------------
59020
10630
Tôi ấn tượng với văn phong của Trang qua cuốn sách “Phải lấy người như anh” nhưng ở cuốn này tôi không tìm thấy được điều đó. Cốt truyện quá đơn giản nhưng câu chữ thì lại cố làm cho bay bướm, hoa mĩ. Thú thực tôi thích ngôn ngữ bình dân, dung dị hơn vì như thế nó dễ đi vào lòng người. Từ người có kiến thức phổ thông hay người có kiến thức sâu rộng, việc sử dụng từ ngữ bóng bẩy rất nên nhưng chỉ là một phần thôi chứ không phải tuyệt đại đa số như trong cuốn sách này.
Cách xây dựng nhân vật, tình tiết diễn biến không đẩy người ta đến cao trào, nó xuôi đều theo dòng nước không cuộn sóng làm ta cảm thấy êm ái và thư thả. Có điều tôi thích nhất ở một chỗ, ở cái chỗ mà không có cuốn sách nào làm được thì Trần Thu Trang đã làm đó là cho phép độc giả viết thêm cái kết cũng như những độc giả đóng vai lần lượt từng nhân vật để nói lên mong ước cũng như suy nghĩ của mình.
Tôi cũng là một người thích viết lách, nhưng khốn nỗi không tạo ra câu chữ bằng những ý tưởng trông như điên rồ của mình, nhờ có Trang mà tôi lại có dịp viết tiếp trên câu chuyện của cô, tạo thêm những tình huống lắt léo ở cái nơi bản Tin Tốc tọa lạc. Biết đâu chúng tôi sẽ hợp nhau?.

3
37782
2013-02-09 16:52:37
--------------------------
45718
10630
Cuốn tiểu thuyết này đưa người đọc đến với những miền cảm xúc chân thực khi yêu: có lãng mạn, có hạnh phúc, có nối buồn và những giọt nước mắt. Cảm giác nhẹ nhàng, bình yên được đan cài giữa những trang sách, làm cuốn tiểu thuyết này giống như một bản tình ca quen thuộc của bao người. Nhưng có lẽ mình đã đọc nhiều tác phẩm trước của Trang Trần, nên với tp này, mình thấy nó có nhiều điều trùng lặp. Cách thể hiện của truyện không mới mẻ, nội dung không có nhiều đột phá như mong đợi ban đầu, vẫn chỉ tập trung vào những tâm sự, cảm xúc đã quá quen thuộc trong văn chương của Trang Trần. Nhưng dù sao nó cũng khá hay và đáng đọc.
3
20073
2012-11-10 08:33:02
--------------------------
31344
10630
"Để hôn em lần nữa" tạo cho mình những cảm xúc khá là mới mẻ so với những truyện khác như "Phải lấy người như anh" và "Cocktail cho tình yêu" của Trần Thu Trang. Mình thích cách miêu tả quá khứ xen lẫn hiện tại, những bí mật chồng chất và tình cảm thầm lặng của Hải Đăng. Tình yêu trong "Để hôn em lần nữa" có một chút ngây thơ của tuổi trẻ, có một chút đau đớn bởi ký ức và những lãng mạn tất yếu khi yêu. Hai con người, chạy trốn nhau hay chạy trốn ký ức? Nhưng rồi không ai thoát khỏi lưới tình.
Đọc truyện này khiến mình tò mò, hồi hộp với những bí mật và cười ngọt ngào ở đoạn kết thúc. Ừ, thì yêu thương có bao giờ phai?
4
8776
2012-06-25 01:10:01
--------------------------
21834
10630
Mình đọc những chương đầu được đăng tải trong tranthutrang.com. Với lối dẫn chuyện vui vẻ, hóm hỉnh, dễ thương, trẻ trung, giọng văn của chị Trang đã khiến mình bật cười giữa buổi đêm. Truyện hấp dẫn bởi nụ hôn giữa chàng trai Đăng và cô sinh viên năm nhất Quỳnh trong chuyến đi tình nguyện của trường Đại học. Sau đó là một chuỗi các sự việc xoay quanh hai nhân vật này. 
Có những tình tiết khá đột ngột mà chị Trang rất khéo léo tiết lộ vào những chương tiếp theo. Cá nhân mình không thích Quỳnh, vì đã quá thích Đan và Thái Vân nhưng với tính cách của cô ấy như thế mới khiến anh chàng hotboy Đăng yêu cô đến thế.
Truyện cũng có thể coi là một bản tình ca ngọt ngọt ngào.
4
27232
2012-03-17 16:46:43
--------------------------
20262
10630
Đây là cuốn sách đầu tiên của Trần Thu Trang mà tôi được đọc. Giọng văn rất ấn tượng và độc đáo so với đa số các tác giả trẻ bây giờ. Cốt truyện khá quen thuộc, xoay vòng xung quanh cuộc sống của 2 người trẻ với nhiệt huyết và tình yêu mãnh liệt. Thế nhưng, với giọng văn hóm hỉnh nhưng vẫn rất sâu sắc, miêu tả chân thật nội tâm của những con người khao khát tình yêu đích thực, đã tạo nên 1 cuốn tiểu thuyết đáng đọc. Trong truyện, đôi lúc có những chỗ hành văn chưa thật chuẩn xác, nhưng chính cái "lỗi" ấy đã tạo nên chất mộc mạc, tự nhiên mà tác giả đang khắc họa cho Quỳnh và Đăng. Và kết thúc, tuy đơn giản, nhưng cũng là kết thúc đẹp nhất cho họ nói riêng, và cho những ai đang yêu nói chung.
"Để hôn em lần nữa' là 1 tác phẩm, đọc để thấy cuộc đời đẹp hơn. 
4
22968
2012-02-28 15:37:07
--------------------------
17365
10630
Một câu chuyện mang đậm văn phong của Trần Thu Trang, một giọng văn có nét riệng rất đặc biệt. Lãng mạn và rất thực là những gì có thể nói về cuốn sách này. Vẫn tạo cho người đọc chút mơ mộng về một chuyện tình  có hơi hướng Hoàng tử - Lọ lem nhưng không theo motip quen thuộc của các sách ngôn tình Trung Quốc mà có nét chấm phá khác lạ, khiến người đọc phải theo dõi câu chuyện từ đầu đến cuối nhưng tác giả dường như tạo ra quá nhiều sự lấp lửng mà đến cuối truyện vẫn không biết được như thế nào. Dù sao câu chuyện cũng tạo cho độc giả cảm giác rằng nó  có thể xảy ra ở bất cứ đâu trong thực tế, các nhân vật rất gần gũi và chân thực. Tuy nhiên, khách quan mà nói, quyển này không phải sách hay nhất của Trần Thu Trang, hình như đến nay vẫn chưa có cuốn nào có thể vượt qua được cuốn sách đầu tiên "Cocktail cho tình yêu" hay do ấn tượng đầu tiên bao giờ cũng sâu đậm nhất :)
3
12042
2012-01-06 18:11:31
--------------------------
15208
10630
Vừa đọc xong Để Hôn Em Lần Nữa của Trần Thu Trang, cốt truyện không mới lạ nhưng tình tiết được sắp xếp khá ổn nên khi đọc, người đọc vẫn muốn theo dõi tiếp cho đến hết truyện. Chỉ có điều hai nhân vật chính cứ như đang chơi trò rượt bắt mãi suốt câu chuyện mà ít có tiến triển gì thêm. Nhân vật không để lại ấn tượng sâu sắc nhiều như trong Cocktail Cho Tình Yêu. Nhưng dù sao đi nữa, tình yêu của họ cũng thật đẹp và không thể phủ nhận rằng cả hai nhân vật chính đều có những nét dễ thương riêng của họ mà khi đọc, đôi lúc bạn phải mỉm cười..
3
1315
2011-12-01 13:53:40
--------------------------
14649
10630
"Để hôn em lần nữa" là tiểu thuyết dài thứ 3 của Trần Thu Trang, sau "Cocktail cho tình yêu" và "Phải lấy người như anh". Khoảng cách gần 5 năm giữa 2 "đàn anh" và "hậu bối" này khiến người đọc có lẽ có phần quên lãng văn phong của Trang, trên một văn đàn tràn ngập truyện teen như hiện nay, và cả một số tác giả mới như Gào, Kawi... Sự trở lại này có tạo ra được ấn tượng hay không?
Người ta sẽ không lẫn đi đâu được một ưu điểm rất đặc biệt trong mỗi truyện mà Trang viết : khung cảnh thiên nhiên và cuộc sống xung quanh mà cô miêu tả, rất phù hợp với câu thơ của Nguyễn Du "Người buồn cảnh có vui đâu bao giờ". Dấu ấn của những câu truyện chính là bối cảnh mà cô tạo ra cho các nhân vật chính, từ HN, Sa Pa, Hội An...hay như trong "Để hôn em lần nữa" là một phần không khí miền núi nơi những người dân tộc Thái sống hạnh phúc và yên ổn. Nó trở thành chất men xúc tác cho tình yêu bình dị nảy sinh trong lòng Đăng, nhưng cũng trở thành khởi đầu cho sự trốn tránh quá khứ trong Quỳnh...
Chỉ có điều, tôi lại cảm thấy "Để hôn em lần nữa" có phần đuối hơn 2 tác phẩm trước, có lẽ vì nó có nhiều khúc mắc và câu hỏi mà gập sách lại, người đọc phải tự đặt ra và tự tìm hiểu, làm loãng đi cảm xúc bao trùm truyện mỗi khi đọc xong. Không chỉ là những câu hỏi cuối truyện mà Trang đã dự đoán trước rằng độc giả sẽ nghĩ tới, mà thật ra, chúng ta cũng không biết được chính xác lý do vì sao Đăng và Quỳnh chia tay, không biết được tình yêu của họ đã tiếp nối như thế nào mà dẫn đến sự ra đi của Phương... Không biết liệu Trang có sử dụng những điều ấy để tạo ra một câu chuyện mới hay không, nhưng mình thật sự rất trông mong được biết câu trả lời...
Dù sao, đây cũng là một sự trở lại thành công của Trang. Mong rằng trong tương lai gần chúng ta sẽ lại được đón đọc những tác phẩm của Trang thêm lần nữa.
4
353
2011-11-19 10:55:04
--------------------------
14080
10630
Mình đã đọc Phải lấy người như anh và Cocktail tình yêu. Có thể nói lúc đầu mình không hề thích lối viết của truyện nhưng càng đọc thì càng bị cuốn hút. Dường như càng viết tác giả càng hút được nhiều người đọc bởi những câu chuyện, những tình huống, cao trào để làm nên nhân vật của mình.
Truyện đậm chất thực tế khi đặc tả cuộc sống xô bồ hiện nay. Tình yêu dường như là một điều gì đó mà con người ta rất cần nhưng lại không biết cách nắm giữ và trân trọng để rồi vụt mất khỏi tầm tay. Nhưng cuối cùng qua bao sóng gió thì tình yêu đã "đơm hoa kết quả". Những cái kết hình như là cổ tích giữa đời thường mà mình rất thích. "Có đi được những con đường dài và lớn thì ta mới hiểu hết được giá trị của tình yêu".
5
16259
2011-11-04 20:06:49
--------------------------
13965
10630
Tôi không muốn bàn nhiều đến nội dung, vì điều đó xin hãy để bạn tự nghiền ngẫm. Nhưng bối cảnh, tình huống, nhân vật và nhiều điều khác từ truyện này sẽ đem đến cho bạn một cảm quan đẹp đẽ. Đôi lúc nhẹ nhàng, đôi lúc khiến bạn muốn mỉm cười, và đôi khi trái tim bạn sẽ rung lên cùng với nhịp đập của nhận vật. Cùng với mr. Đăng lạnh lùng và nồng ấm, với Quỳnh cá tính và nhẹ nhàng, Đức vui vẻ hay Điệp "sát thủ", bạn sẽ có quãng thời gian thư giãn và thoải mái, như một ngày cuối tuần, bên ấm trà ngon, và cuốn truyện của Trần Thu Trang.
5
16029
2011-11-02 14:55:58
--------------------------
12911
10630
Có những con người phải đi suốt một chặng hành trình dài mới có thể tìm được cho mình một bến đỗ. 
Hạnh phúc và khổ đau, niềm vui và giọt nước mắt. Những ký ức đan xen giữa quá khứ và hiện tại. Đối mặt hay lẩn tránh?????
Dĩ nhiên, ai cũng có thể dễ dàng nói là đối mặt với thực tại, nhưng muốn đối mặt đâu phải là dễ? Khi những tự ti, khi những yếu mềm đã ăn sâu vào trong thớ thịt? 
Hạnh phúc ở ngay trước mắt nhưng lại luôn xa tầm với, muốn nắm bắt, nhưng lại luôn phấp phỏng không yên....Bởi vì ta sợ.....ta sợ phải một lần nữa tan vỡ, sợ một lần nữa trái tim lại rơi lệ....sợ cả những ký ức ngọt ngào sẽ trở về day dứt mỗi đêm.....
Bởi vì ta tự ti, ta không tin bàn tay này có thể nắm chặt lấy hạnh phúc....Hạnh phúc thì mong manh, còn ta thì nhỏ bé quá đỗi....
Sự đấu tranh trong tư tưởng để vượt qua nỗi tự ti, vượt qua những sợ hãi và mặc cảm của bản thân để kiếm tìm hạnh phúc luôn là một hành trình dài vô tận..... Hi vọng đến cuối con đường, Quỳnh sẽ lại tìm về bên Đăng....Bởi những người yêu nhau, đi đến cuối hành trình sẽ lại trở về bên nhau...........
5
10679
2011-10-15 15:11:39
--------------------------
12661
10630
Mình đã đọc tác phẩm "phải lấy người như anh" của  Trần Thu Trang, tác phẩm có những tình tiết phát hoạ nội tâm của nhân vân rất sâu sắc , do vậy mà nhân vật của Thu Trang có nét riêng , khó quên trong lòng độc giả, nội dung gần gũi với cuộc sống thường nhật. Mình cũng đã đọc " tí ti thôi nhé" . Qua đó , mình thấy nhìn chung những tác phẩm của Thu Trang dành riêng cho phụ nữ bởi chỉ có họ mới hiểu tại sao nhân vật nữ chính lại hành động như vậy, chỉ có họ mới có những nỗi buồn sâu kín không thể nói cùng ai để rồi họ ôm đau khổ ấy 1 mình mong người họ yêu được hạnh phúc, hay nói đúng hơn nói đến phụ nữ Việt Nam là nói đến sự hi sinh vì những người mà họ yêu mến. Đọc và cảm nhận những tác phẩm của Thu Trang, nhẹ nhàng và sâu lắng. Mình rất mong chờ tác phẩm " để hôn em lần nữa" , như một món quà tặng tinh thần cho chính mình nhân ngày phụ nữ Việt Nam 20/10.
4
13177
2011-10-11 09:00:35
--------------------------
12541
10630
Mình rất thích Ly cocktail cho tình yêu của tác giả Trần Thu Trang
Thế nên cũng đã có nhiều kỳ vọng vào quyển sách mới xuất bản này 
Ngay ngày đầu tiên được phát hành là đã cầm trong tay rồi
những trang đầu viết khá ấn tượng, lối viết nhẹ nhàng, vẫn thoang thoảng phong cách từ hồi Ly cocktail cho tình yêu
nhưng về sau thì mình không còn cảm nhận được nữa
Câu văn quá dài, lại đan xen những hình ảnh tưởng tượng so sánh làm cho đọc rồi mà không nhớ nổi mình đã đọc gì 
Cốt truyện không có gì đặc sắc, có lẽ sẽ không thể kéo dài đến mức như thế nếu không phải bởi vì cách viết khá dài dòng 
Mình không thích nhân vật Quỳnh, bởi tính cách như cô có thể bắt gặp ở rất nhiều truyện khác
Cũng khó hiểu với tình cảm sâu đậm mà Đăng dành cho cô sau tận 4 năm xa cách chưa một lần gặp lại...mọi thứ dường như quá đỗi tự nhiên nhưng lại khiến người ta đặt ra câu hỏi là vì sao?
Có lẽ ấn tượng nhất là nhân vật Đức đã từng xuất hiện trong Ly Cocktail cho tình yêu, một nhân vật khá đáng yêu với cách ăn nói thẳng thừng và hóm hỉnh...nhưng có một số chi tiết tác giả miêu tả  mà mình không thích, dường như làm mất đy cái nét mà mình cảm nhận về Đức trong Ly cocktail cho tình yêu.
Dù sao đây cũng chỉ là cảm nhận của mình mà thôi.



2
1331
2011-10-09 14:44:17
--------------------------
