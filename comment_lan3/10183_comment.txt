266242
10183
Mình biết đến Nhà văn Nguyễn Quang Thiều qua tác phẩm "Mùa hoa cải bên sông". Thực sự đó là tác phẩm khiến mình băn khoăn và nuối tiếc rất nhiều, giọng văn và chất tình người trong những tác phẩm của bác đem lại cho người đọc rất nhiều xúc cảm. 
Có một kẻ rời bỏ thành phố là những mẩu truyện về Hà Nội,  về làng Chùa nơi tác giả gắn bó mấy chục năm qua,  mang đến cho cá nhân mình những trải nghiệm sống hữu ích, những triết lý giản đơn nhưng đầy ý nghĩa thiết thực. Tác giả đề cập đến những giấc mơ, mà con người dù vô tình hay cố ý đã bỏ qua , để rồi khi nhìn lại thấy nuối tiếc mà không thể lấy lại được. 


 
4
69833
2015-08-14 15:16:04
--------------------------
