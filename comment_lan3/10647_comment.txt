312420
10647
Cuốn sách này rất hữu ích với cá nhân tôi, khi có cả hai con trai đều trong độ tuổi sử dụng của cuốn sách. Hàng ngày, các phụ Huynh có thể đều đã và đang thử nghiệm các con mình với các trắc nghiệm trí tuệ trong cuộc sống. Tuy nhiên, tôi nghĩ nó không được hệ thong thì sẽ khó đánh giá được sự phù hợp và giúp con mình phát triển trí tuệ theo mong muốn. Cuốn sách giúp ta hệ thống hóa các trắc nghiệm, cả phụ Huynh và các bé cùng nhau trải nghiệm và vui vẻ bên nhau. Tôi nghĩ, đó là điều lý thú và bổ ích nhất của cuốn sách mang lại. Sau mỗi trải nghiệm, chúng ta tự đánh giá lại khả năng của các con và điều chỉnh tiếp. Thậm chí, mình có thể lặp lại trắc nghiệm này nhiều lần. 
4
572177
2015-09-21 11:06:26
--------------------------
