283410
10280
Tôi đã yêu thích tác giả Phương Trinh kể từ khi cô còn là học sinh cấp 3, qua những truyện ngắn trên báo Mực Tím, Phụ nữ ... của cô . Tôi từng rất thích lối viết nền nã, sâu sắc của nữ tác giả, nhưng sang đến tác phẩm này thực sự khiến tôi bất ngờ, một lối viết hoàn toàn mới mẻ, độc đáo và phần nội dung khó đoán, bất ngờ. Từng truyện ngắn độc lập, mang một sắc thái riêng biệt, đầy lôi cuốn. Những truyện ngắn của tác giả mang cái đẹp, mang hơi thở của thành phố và con người Sài Gòn, với những góc nhìn rất riêng, rất khác biệt. Tôi đã đọc hết và thật sự thích tác phẩm này !
4
24682
2015-08-29 23:09:21
--------------------------
57706
10280
Mình đã bị tiêu đề mới lạ và bìa sách ấn tượng của tác phẩm này thu hút ngay lần đầu nhìn thấy, và đã chọn mua nó ngay. Đây thực sự là một cuốn truyện ngắn với những điều mới mẻ, độc đáo và hấp dẫn. Ban đầu thì mình chưa thực sự thích lắm, những chỉ sau vài truyện ngắn ban đầu mình đã bị cuốn theo những dòng cảm xúc miên man mà cuốn sách này mang lại. Mỗi truyện có những đặc tả về cuộc sống, về con người và tình yêu, bên cạnh đó là những xúc cảm tinh tế và sâu lắng, tất cả đã làm nên một khúc nhạc "Bach" ấn tượng và khó quên. Cuốn sách không chỉ là một tập truyện đơn thuần, mà còn là cả một hành trình khám phá kỳ diệu về cuộc sống của mỗi người.
4
20073
2013-01-31 17:23:38
--------------------------
