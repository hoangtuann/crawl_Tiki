336476
10810
Nếu bạn đang trong một mớ hỗn độn giữa đúng & sai, nên & không nên, hoặc bạn đang tìm sự thật cho một bí mất,  một câu chuyện dối trá , một điều tồi tệ nào đó ...thì bạn hãy thử dành một buổi sáng cuối tuần rảnh rỗi đọc hết quyển So B. It- Bí ẩn một cái tên
Nếu bạn tin vào sự may mắn, tin vào chuyện thương đế luôn xuất hiện đúng lúc đúng chuyện khi bạn gặp chuyện xui rủi thì bạn cũng nên tin cô bé Heidi có khả năng đoán đúng trò tung đồng xu và cô ấy cũng rất giỏi  kiếm tiền nhờ sự may mắn khó tin này.
Nếu bạn tin vào tình yêu, tin vào hoa hồng, tin mọi chuyện trên đời này xảy ra đều có một lý do đúng, tin rằng ngay cả những người bị bại não cũng có thể nhớ thương, có thể yêu nhau, có thể sinh con thì còn chần chừ gì mà không thử đặt mua hoặc tìm điều dễ thương đến mức   diệu kỳ trong quyển sách bé nhỏ này.Chúng ta, con người sống trong cuộc đời này, “giàu có" hơn nhau ở chỗ mỗi người trong cuộc đời mình đã đi được bao nhiêu cung đường, vượt qua được bao nhiêu ngọn núi, đứng trên đỉnh núi bao nhiêu lần, rớt xuống vực sâu bao nhiêu bận.
Nếu tôi có một người mẹ bị bại não như Heidi, tôi không biết tôi có đi được trên những sợi dây cheo leo mắc từ đỉnh vực này sang đỉnh vực khác như cô bé ấy không.
Tôi khâm phục cô bé, ngưỡng mộ người mẹ và đặc biệt hơn hết là bác Bernei.
Họ thật là giàu có.
5
2671
2015-11-12 14:56:24
--------------------------
306039
10810
Cuốn sách nhỏ thôi nhưng nó đưa ta theo chân hành trình của cô bé mười hai tuổi Heidi một mình đi khám phá ý nghĩa của từ “soof” mà mẹ cô vẫn hay nói. Có vẻ như đó là một chuyến đi dài và không mấy dễ dàng cho cô, khi mà ròng rã nhiều năm trời cô sống với người mẹ bị thiểu năng cùng sự giúp đỡ của người hàng xóm tốt bụng, người bị mắc chứng sợ không gian rộng nên không thể đặt chân dù chỉ một bước ra căn hộ của mình. Trên đường cô bé gặp rất nhiều người tốt bụng và biết thêm được nhiều thứ. Tác giả đã chú ý miêu tả vừa đủ để ta có thể cảm nhận được nội tâm thông qua những hành động đôi lúc chính cô cũng khó giải thích nổi. Cốt truyện quả thật không phải dễ đoán được, mình thực sự bị câu chuyện thu hút từ đầu đến cuối. Truyện cũng rất đẹp, tình yêu của cha mẹ, tình yêu mẹ con, tình yêu của những người tốt bụng… đều rất đẹp và giàu tính nhân vân. Gì chứ mình khá là thích sách của Phương Nam, rất lạ, đôi khi khó đọc nhưng đọc rồi thì thấy rất đáng giá.
5
24386
2015-09-17 12:19:17
--------------------------
258487
10810
Mở đầu truyện đã bao trùm sự bí ẩn. Cô bé Heidi mười hai tuổi sống cùng người mẹ bị thiểu năng và bác hàng xóm lập dị, cuộc đời cô là một uẩn khúc với vô vàn dấu hỏi như cha cô bé là ai? Tại sao hai mẹ con lại đến sống ở ngôi nhà này? Người thân của họ đâu? Một ngày tình cờ Heidi nhìn thấy bức ảnh mẹ cô thời còn trẻ, từ đây Heidi một mình dấn thân vào cuộc hành trình tìm kiếm sự thật, tìm về quá khứ để biết thân phận thật sự của bản thân. Có điều mọi sự hóa ra lại khá đơn giản, không li kì bí hiểm như mình hình dung. Tuy nhiên câu chuyện giản dị lại mang rất nhiều ý nghĩa và thấm đẫm tình cảm. 
4
6502
2015-08-08 16:11:57
--------------------------
228229
10810
Ban đầu đọc phần giới thiệu mình cứ tưởng truyện sẽ mang màu sắc huyền bí và đôi chút trinh thám. Thế nhưng, truyện đơn giản chỉ là cuộc đời, không bí ẩn, không ly kỳ mà rất đỗi thân quen, giản dị và đầy xúc động. Đó là cuộc hành trình của cô bé mười hai tuổi Heidi đi tìm kiếm ý nghĩa của của từ "soof" mà người mẹ bị thiểu năng của cô vẫn hay nói. Trong chuyến đi đó, cô có dịp làm quen với những con người tốt bụng, hiểu thêm về người mẹ tội nghiệp và tìm lại cội nguồn của mình - nơi có người cha, người ông mà cô chưa một lần biết đến. Truyện thấm đẫm tình yêu thương và sự vị tha, để ta thấy rằng dù ở nơi đâu, tình yêu vẫn tồn tại và luôn là một điều kỳ diệu của tạo hóa. Truyện có kết thúc buồn, cảm động nhưng đầy ý nghĩa và tính nhân văn. Đây thật sự là một tác phẩm rất hay và để lại nhiều suy nghĩ trong lòng người đọc. 
4
387532
2015-07-15 10:19:44
--------------------------
161350
10810
Một quyển sách nhỏ nhưng chất chứa đầy cảm xúc. Truyện kể về hành trình của cô bé Heidi đi tìm sự thật về từ 'soof'. Khi lật từng trang sách ta sẽ cảm nhận được những chặng đường, những gian khó mà cô bé nhỏ tuổi gặp phải. Đến cái kết có lẽ hơi buồn cho mọi độc giả. 
Một quyển sách mang nội dung ấn tượng, đôi khi lại ám ảnh. Nhưng trên mọi sự nó cũng nhắn nhủ một thông điệp vô cùng triết lí trong tình yêu thương giữa con cái với cha mẹ và ngược lại - Tình yêu không phân biệt ai cả, cho dù đó là người cha hay người mẹ thiểu năng, nhưng tình cảm thì vẫn là nguyên vẹn
5
435062
2015-02-27 15:35:42
--------------------------
156445
10810
Cuốn sách này mình biết lâu lắm rồi nhưng bây giờ mới có dịp mua đọc. Sách nói về bé Heidi - 1 cô bé mồ côi cha và mẹ bị bệnh thiểu năng. Mỗi ngày cứ như thế trôi đi, ngày nào cũng lặp lại như ngày nào, mẹ ko thể chăm sóc cô nên bác Bernadette - một người hàng xóm tố bụng bị bệnh sợ khoảng rộng, không dám ra khỏi nhà - chăm sóc. Nhưng chỉ 12 tuổi thôi, cô bé đã phải đi dọc hết con đường New York để tìm kiếm một bí ẩn - từ Soof. Từ đó bé Heidi nghiệm ra rất nhiều điều và sự thật của quá khứ....
4
520065
2015-02-04 20:24:48
--------------------------
52548
10810
Đúng như cái tên cuốn sách, câu chuyện của cô bé Heidi dẫn dắt độc giả bằng những điều bí ẩn, những bí mật và sự thật đang bị giấu kín. Tác giả đã tạo nên một cốt truyện khá mới mẻ và độc đáo, với những nhân vật cũng đặc biệt không kém, đưa chúng ta vào một thế giới khác lạ và hấp dẫn. Người đọc như bị cuốn vào hành trình của cô bé Heidi, được tìm về những ký ức đã ngủ quên từ lâu, được đi đến tận cùng của cuộc sống với biết bao điều mới mẻ. Mình thực sự thích cách viết của tác giả, vừa tạo nên cảm giác chân thực và hồi hộp, vừa gửi gắm được vô số những triết lý về tình yêu thương và cuộc sống này.
Cuốn sách như một khúc ca diệu kỳ về những điều xung quanh chúng ta.
4
20073
2012-12-27 14:50:42
--------------------------
44600
10810
Đây là một trong những cuốn truyện khiến tôi phải đọc đi đọc lại để cố gắng nuốt gọn hết từng câu,từng lời . Câu truyện kể về một cô bé Heidi cùng một người mẹ và những bí ẩn Heidi đang tìm kiếm những câu trả lời. Một cuộc hành trình , một cuộc phiêu lưu , một cuộc rượt đuổi theo những dấu vết để khám phá ra bí mật , Heidi cuối cùng đã tìm ra được câu trả lời cho những thắc mắc của mình . Nội dung chỉ xoay quanh cái tên của mẹ cô bé , một cái tên khó hiểu và bí ẩn , nhưng những tình tiết , những bất ngờ , những điều hé mở ra xuyên suốt những trang chữ làm "So B,It - Bí ẩn một cái tên" lôi cuốn tôi làm tôi không thể dừng đọc tiếp được .Đây là cuốn truyện ảnh hưởng sâu sắc với tôi,một cuốn truyện khi bắt đầu đọc bạn sẽ chỉ dừng lại được khi nào đọc hết tới câu chữ ở trang văn cuối cùng.
Cuốn sách khiến tôi có nhiều khoảng lặng , để suy ngẫm , để  nhận thấy những điều thú vị và quan trọng trong cuộc đời.Đây là một cuốn sách một khi đã đọc , bạn sẽ không bao giờ quên được , các nhân vật trong sách : Heidi , mẹ cô bé Bernadette,Thurman Hill,.... ; cuộc phiêu lưu tìm kiếm sự giải đáp cho cái tên ; kết thúc bất ngờ ,... là những yếu tố tạo nên sự li kì và thú vị cho cuốn truyện . Một câu truyện cảm động về tình yêu thương , về tình người , và về các khoảng lặng giữa cuộc sống .
5
53391
2012-11-03 12:46:35
--------------------------
41972
10810
Đây quả thật là một câu chuyện rất cảm động. Truyện kể về một cô bé không biết rõ quá khứ của mình, không hiểu rõ mình đến từ đâu, cô sống chung với một người mẹ bị bệnh thiểu năng mà suốt ngày chỉ thích nói mỗi một tiếng So Be It. Rồi cô bé quyết định lên đường tìm hiểu nguồn gốc của bản thân, bí ẩn của tiếng nói của mẹ mình, cuối cùng vượt qua những khó khăn, thử thách cô cũng đã tìm được lời giải cho những thắc mắc của bản thân, tìm lại được gia đình của mình, sống hạnh phúc và kiên cường. Mình thật sự rất thích cách kể chuyện ngắn gọn và súc tích của tác giả, cách tác giả xây dựng tình huống truyện, tạo dựng cá tính và miêu tả nội tâm nhân vật đều rất tuyệt. Đây quả thật là một quyển sách đáng đọc.
Tuy nhiên mình cảm thấy không thích quyển truyện này ở chỗ, giấy rất tối, chữ lại nhỏ, truyện khá mỏng, giống với một quyển truyện bỏ túi nhưng giá thì lại hơi đắt, 65 ngàn chưa giảm giá. 
5
40753
2012-10-05 09:36:17
--------------------------
27040
10810
Tôi chưa từng nghĩ mình có thể đọc hết một cuốn sách lúc 2h sáng, nhưng So B. It- bí ẩn một cái tên lại cho tôi một trải nghiệm mới. Mạch truyện rất tự nhiên, nhẹ nhàng, rất dễ đồng cảm. Tôi thích cô bé Heidi vì những suy nghĩ rất hay (hay những dòng cảm nhận của tác giả Sarah Weeks) về cuộc sống, về con người. Kết thúc truyện khá buồn nhưng thứ đọng lại trong lòng người đọc không phải chỉ là mỗi buồn. Nó cho ta những hồi ức đẹp về lòng mẹ dù người mẹ có nhiều khiếm khuyết. Hãy để chúng ta tự cảm nhận và cho mình một góc riêng 
4
18706
2012-05-15 18:12:51
--------------------------
7674
10810
Tôi thường cho rằng một tác phẩm ngắn thì thật khó để chuyển tải một nội dung lớn nhưng điều tưởng như không thể đó lại được tác phẩm này thể hiện rất tốt. Câu chuyện nhẹ nhàng, nó không quá u ám buồn bã nhưng cái kết của nó thật sự gây cho người đọc nhiều nuối tiếc. Cách hành văn của tác giả không hẳn là đặc sắc nhưng nó lôi cuốn và tự nhiên. Điểm trừ duy nhất của quyển sách chính là mặt hình thức: sách không có bìa gập, giấy bìa cũng quá mềm, giấy bên trong mỏng, khổ sách quá nhỏ nên chữ chen chúc như kiến bò.Hãy đọc để thưởng thức nội dung và cố chấp nhân về mặt hình thức,
4
3562
2011-07-05 08:42:22
--------------------------
5654
10810
Số phận của bé Heidi có lẽ không ngọt ngào như cái tên dành cho bé. Phát hiện ra sự thật quá đỗi bất ngờ, cùng với sự mất mát quá lớn. Ta thấy bé dường như trưởng thành hơn, bản lĩnh hơn. Giọng văn của câu chuyện nhẹ nhàng và lôi cuốn. TÔi không nghĩ đó là những suy nghĩ của một đứa trẻ 12 tuổi, vì có lẽ bé lớn và suy nghĩ thấu đáo hơn lứa tuổi của mình. Xuyên suốt câu chuyện là những triết lý sâu sắc về những điều xảy ra xung quanh cuộc sống. Có quá nhiều điều để biết để hiểu. Nhưng câu hỏi lớn nhất đó là tại sao những điều Heidi biết về mẹ lại quà ít như vậy. Tìm hiểu đến ngọn ngành sự thật, "soof" cuối cùng chính là cội nguồn của tình yêu. Và đó là cách riêng mà mẹ dành cho Heidi. Và cũng chính vì điều đó Heidi mới càng thêm trân trọng những quá khứ, kỉ niệm đẹp đẽ về mẹ và tình yêu ko lời của bà. 
Cũng như Heidi là "soof" - là tình yêu của mẹ thì cũng ko ai biết được tại sao lại xuất hiện một Heidi trên đời. 
Tôi ấn tượng nhất đó là câu: "Có một số thứ trong cuộc sống mà một con người ko thể nào biết được"
Cuốn sách là một dư vị ngọt ngào như buổi trà chiều mà mẹ thường pha cho Heidi. Ấm áp tình người và những suy ngẫm sâu sắc. 
5
2935
2011-06-04 13:47:18
--------------------------
