375539
10467
Thành công sẽ không đến nếu bạn không ngừng nỗ lực . Đó chính là những gì mà quyển sách đề cập đến . Những bí quyết và hường dẫn trong quyển sách sẽ giúp bạn đến gần với thành công hơn và nếu áp dụng nó thường xuyên , bạn sẽ trở nên thành thạo và ít thất bại hơn . Bởi vì không có gì là hoàn hảo cả , đôi lúc sẽ vấp ngã nhưng đứng lên từ vấp ngã đó khiến bạn trưởng thành hơn. Cám ơn tác giả đã viết nên quyển sách này .
4
651562
2016-01-28 16:49:55
--------------------------
132375
10467
Theo mình thì đây là quyển sách được diễn giải một cách sinh động với rất nhiều hình vẽ ngộ nghĩnh khiến người đọc không nhàm chán khi phải đọc quá nhiều chữ. Những bạn bận rộn không có thời gian đọc nhiều thì có thể tìm quyển này, giọng văn của Jim Randel rất hóm hỉnh nhưng chứa đựng nhiều hàm ý sâu xa. 1 quyển sách chứa đựng nhiều ý nghĩa, nội dung có đề cập đến nhiều quyển sách nổi tiếng khác, giúp mình không cần đọc nhiều nhưng vẫn nắm được ý chính. Quan trọng nhất là hình thức của sách quá tuyệt khiến mình đọc hoài mà không chán! Thực sự đây là 1 quyển sách hay!
5
66279
2014-10-31 10:55:02
--------------------------
