305552
10332
Tìm đến quyển sách này, mua và đọc là vì lời cảm nhận của sách.

""Thành công – thất bại, yêu thương – oán hận, sự sống – cái chết… tất cả đều được tác giả đề cập và nhìn nhận bằng một quan điểm riêng, khác lạ nhưng thật thuyết phục.""

và tôi cảm thấy thật hài lòng với những quan điểm của tác giả về các mặt đối lập nhau. Truyện đều đều bình dị, nhe nhàng và gắn bó chặt chẽ với cuộc sống hàng ngày. Tôi thực sự thích tác phậm này

Về hình thức: Sách cũng được in rất đẹp, tôi cất sách trên kệ và xem nó như người bạn thân thiết
4
409134
2015-09-17 00:00:00
--------------------------
47081
10332
Cuộc sống là 1 chuỗi dài những yêu thương, mất mát mà không ai có thể biết trước được.
 Đọc quyển sách này, bạn sẽ thấy rằng không có lúc nào là quá muộn để quyết định sống thật với bản thân mình, làm những điều bạn thực sự muốn chứ không phải sống 1 cuộc đời theo cách mà người khác mong bạn sống. Tác giả có thể nói là người khá thành công trong lĩnh vực của mình nhưng tới một điểm nào đó trong cuộc sống, ông nhận ra đó không phải là mình. Ông từ bỏ hết tất cả, sự nghiệp, nhà cửa, tiền bạc, gia tài, người vợ không hiểu mình để đi về với biển - là nơi ông cảm thấy bình an và có được những điều tâm hồn mình thực sự mong muốn.
Tác giả quá dũng cảm. Nếu là tôi, có lẽ tôi không dám làm những điều đó, có lẽ tôi sẽ chấp nhận cuộc sống đó suốt cuộc đời. Có lẽ vì 1 phần biển gắn bó với tôi từ nhỏ, tôi yêu biển nhưng không khát khao biển. 
Mỗi chương là 1 câu chuyện, không có những tình tiết gay cấn, không có nút thắt nhưng hãy đọc đi, rồi bạn sẽ hiểu vì sao tác giả lại dám từ bỏ tất cả những điều mà biết bao người mơ ước có được chỉ để về với biển.
3
65561
2012-11-21 15:07:20
--------------------------
