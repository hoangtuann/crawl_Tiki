393440
10019
Lời nguyền của Quỷ độc là tập tiếp theo trong series Cậu bé học việc và Thầy trừ tà. Trong chuyến hành trình lần này, Thom cùng với thầy của mình phải đối mặt với Quỷ độc- thứ được mệnh danh là có thể len lỏi vào tâm trí và đọc ý nghĩ của người khác. Nó đã bị nhốt đằng sau cánh cửa bạc dưới lòng của một nhà thờ lớn. Ngoài ra, Thom cùng Gregory còn phải đối mặt với tay Phán quan chuyên lùng bắt các phù thủy hoặc những người như hai thấy trò Thom. Những cuộc đối đầu, chạy trốn, giải cứu và đấu trí với Quỷ độc và Phán quan hứa hẹn sẽ đem lại những giây phút phiêu lưu thú vị
4
632796
2016-03-08 19:38:10
--------------------------
219574
10019
Phần 1 tác giả đã khởi đầu không tệ, qua phần 2: Lời Nguyền Của Quỷ Độc thì không còn gì để nói, hay! Nhưng mà đúng là không nên đọc vào buổi tối thật, Mình thuộc loại gan bé nên đọc đoạn nào cũng sợ. Nhưng mà những lúc gay cấn thì cho dù sợ đến đâu cũng không thể rời cuốn truyện được. Đọc xong tập 2 cũng là kết thúc trận chiến đầy khó khăn với Quỷ độc. Công nhận là tập 2 quá hay, càng làm mình tò mò và muốn xơi luôn tập 3 trong đêm, nhưng mà thôi, mình sợ :)))
Bìa sách vẫn rất đẹp, giấy in cũng rất tốt. Khen chibooks vì truyện nào của Chibooks cũng đều chất lượng cả.
5
37970
2015-07-01 17:24:44
--------------------------
193834
10019
Mỗi tập truyện là một cuộc phiêu lưu của Tom và thầy trừ tà. Tập này cũng thế. Và thú vị rất nhiều so với phần 1. Ở phần này, Tom phải đối đầu với Quỷ Độc, 1 thế lực bóng tối nguy hiểm lâu đời và những rắc rối mà thầy Trừ tà phải đối diện với Phán quan cũng vô cùng nghẹt thở. Tuy nhiên điều ấn tượng nhất trong truyện lại là Alice. Cô đã cho máu Quỷ Độc với điều kiện Quỷ Độc không được làm tổn hại Tom. Điều đó cho thấy phẩm chất vô cùng cao đẹp và đức hi sinh của Alice.
5
407882
2015-05-08 21:23:34
--------------------------
82357
10019
Hay quá! Đọc tập 1 thì ngóng tập 2. Đọc tập 2 thì ngóng chờ đến tập 3! Lời văn đơn giản, văn phong giản dị, rất thích hợp cho thiếu nhi thích đọc truyện kinh dị Haha.. ==
Đúng quả thật là một người sống ở trung tâm nơi sinh sống của những ông kẹ trứ danh. Liệu nơi nhà văn sinh sống có còn ông kẹ xuất hiện nữa không nhỉ? Nếu có thì tuyệt quá! @@
Tập 2 này quả hay ý, Quỷ Độc đúng là lần đầu tiên nghe tên == chắc do ít đọc truyện thiếu nhi kinh dị quá! Nghe mới đầu cũng có vẻ ác độc và cái tên thuộc loại khủng nhưng vẫn thấy đây là một danh xưng khôi hài dành cho nó. Thà đặt là Quỷ nghe cũng tàm tạm được.. Mà từ " Độc" có lẽ là Độc Ác, chứ không phải như mình nghĩ là nó có khả năng phóng ra độc nhỉ @@
5
14620
2013-06-20 21:09:59
--------------------------
64164
10019
Hãy thử tưởng tượng cuộc sống ngoài đời sẽ như thế nào nếu nó xảy ra giống như trong truyện. Thật đáng sợ. Thầy trừ tà qua miêu tã quả là người phi thường, dù  lớn tuổi nhưng thầy vẫn khoẻ như vâm, đến cả cậu học việc cũng không thể theo kịp thầy mình. Những con quỷ dữ trong truyện thật đáng sợ. Chúng được miêu tả sống động dưới ngòi bút của nhà văn . Ông cũng rất thành công trong việc tạo nhưng tình huống truyện kịch tính và miêu tả tâm lí nhân vật xuất sắc.
4
46696
2013-03-18 12:57:46
--------------------------
54119
10019
Chà... biết nói như thế nào nhỉ!!! Nếu như phần 1 đã khiến tôi thất vọng thì phần 2 nãy không những đã lấy lại cảm hứng mà còn reo vào đầu tôi sự chờ đợi cho phần kế tiếp. Truyện khá hay, bắt đầu có những tình huống gay cấn và hồi hộp, tình tiết cũng trở nên bất ngờ ngoài dự tính của chính tôi. Có thể nói Joseph Delaney đã tạo được bước ngoặc từ phần 2 này đây.  Chẳng thể nào rời tay khỏi cuốn sách, đôi mắt cứ dõi theo từng chữ, từng chữ, kế đó là từng chương, chương này kế tiếp chương kèm theo sự tò mò, hồi hộp cho chính số phận của Thầy Trừ Tà, của Thomas Ward và của cả Alice. Tóm lại, tôi chỉ biết dùng 2 từ "tuyệt vời" cho tác phẩm mà tôi đang cầm trên tay mình đây "Lời nguyền của Quỷ Độc"!
4
35264
2013-01-07 14:25:46
--------------------------
53801
10019
Mình đã đọc phần 1 của bộ truyện này và cảm thấy vô cùng ấn tượng, nên đã tìm đọc ngay phần 2 để xem tiếp những gì sẽ diễn ra. Và quả thực, phần 2 này còn hấp dẫn và lôi cuốn hơn trước rất nhiều. Đây là một tác phẩm có những sáng tạo mới mẻ và độc đáo, tạo nên một thế giới ma quái rùng rợn những cùng kỳ thú, hút độc giả vào trong từng trang sách. Cuốn sách cho ta nhiều khám phá về giới phù thủy, về những phép thuật, những điều kỳ quái đến không ngờ trong một thế giới khác lạ. Xuất sắc trong cách tạo tình huống truyện, nhịp nhàng và lôi cuốn trong cách dẫn dắt truyện, tất cả những điểm này đã tạo nên một tác phẩm rất hay và đặc sắc, không nên bỏ qua với bất cứ ai.
4
20073
2013-01-04 22:17:15
--------------------------
41569
10019
Tôi bị thu hút bởi lời khuyến cáo "không được đọc sau khi trời tối" được in trên sách. Thế là quyết định mua thử để xem có gì rùng rợn không mà sách lại ghi như thế.  Qủa là không bị thất vọng. Đoạn mở đầu của truyện thực sự làm tôi tò mò nhất là vì lúc đầu tôi chưa hiểu định nghĩa "trừ tà" ở đây là gì. Tác giả đã tự xây dựng nên một thế giới huyền bí mới, không "đụng hàng" với những series truyện kỳ ảo khác. Đọc những lời chú thích của tác giả về những "thứ tà độc" cũng khá là thú vị, thú vị hơn nữa là ở chỗ tác giả đề ra hệ thống ký hiệu chỉ rõ ở đây có quái vật, ma hay ông kẹ kiểu gì, mạnh hay yếu, ai là người bắt được,...mang phong thái rất riêng. Mỗi nhân vật được tác giả khai thác theo kiểu "để ngỏ", nghĩa là khi có sự vụ nào mới xảy ra, nhân vật chính của chúng ta vẫn có thể quay lại gặp những nhân vật phụ này để tìm hiểu thêm kiến thức phục vụ cho sứ mạng mới của mình.
Tôi đã đọc xong 2 phần, chờ mãi những tập tiếp theo. Nay ra được tập 3 kể cũng coi được thỏa nguyện một phần
4
52407
2012-09-30 20:27:18
--------------------------
