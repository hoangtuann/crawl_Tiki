462577
10048
Truyện này nội dung thì khỏi bàn,so với các bộ truyện được xuất bản sau này thì đây là một trong những bộ mình yêu thích nhất vì nét ve nhân vật vẫn còn rất đẹp và nội dụng thì hay cực, ngày xưa ước ao mua nhưng ko đủ kinh tế, h có tiền rồi nên quyết rướt nguyên bộ về liền. Tất cả mọi thứ đều tuyệt vơi, cộng thêm mình còn sử dụng bọc sách của tiki nữa, nên phải nói là đối với nhưng người hâm mô doraemon thì bắt buộc phải mua bộ này, và để hoàn hảo hơn thì nên bọc sách của tiki luôn, chất lượng siêu tốt ^^
Chốt lại câu: đây là bộ truyện phải có nếu bạn là fan truyện tranh ^^
5
205426
2016-06-28 18:25:30
--------------------------
224943
10048
Doraemon bóng chày là loạt truyện để lại một ấn tượng sâu sắc trong lòng mình. Những chú mèo máy với ngoại hình, tính cách, xuất thân hoàn toàn khác nhau đã vượt qua những bất đồng và thể hiện sức mạnh của tinh thần đoàn kết khi lần lượt vượt qua những khó khăn càng lúc càng lớn lao. Không có một nhân vật nào là không để lại ấn tượng lớn trong lòng mình, từ Tora với tinh thần tích cực cao độ đến Almond kiêu hãnh và Shiro mạnh mẽ. Môn bóng chày qua những khung truyện chưa bao giờ thú vị và đầy hài hước đến vậy.
5
598425
2015-07-09 15:27:47
--------------------------
183096
10048
Thật sự thì đây là lần đầu tiên mình đọc Đôrêmon bóng chày qua lời giới thiệu của đứa bạn thân. Theo cảm nhận của mình đây là một câu chuyện khá hay kể về thế giới bóng chày ở thế kỉ 22, trong đó mình yêu thích nhất là đội trưởng Kưrôêmon. Hành trình đến với bóng chày và quá trình vượt qua siêu cầu thủ bóng chày, cũng lả người bạn thơ ấu của cậu Shirôêmon khiến mình cảm động vô cùng. Sau Đôrêmon, đây cũng là một siêu phẩm hay nên đọc với giá cả vừa phải. Nó chắc chắc sẽ làm vừa lòng những tín đồ của truyện Đôrêmon.
4
558345
2015-04-15 14:15:49
--------------------------
106923
10048
Truyện đặc sắc và lôi cuốn từ tập 1 cho đến tập cuối. Với những tình tiết mới mẻ mang lại cho người đọc những trận đấu bóng chày gây cấn và hấp dẫn, không những câu chuyện hay mà tuyến nhân vật cũng để lại trong lòng mình một hình ảnh khó mà quên và khiến mình phải phát cuồng vì tuyến nhân vật (chẳng hạn như Shiroemon, một cầu thủ khi nhỏ vốn là một kẻ vô dụng nhưng nhờ sự nỗ lực mà cậu đã chạm được tới thành công).
Một bộ truyện tuyệt vời mà mình đã bỏ lỡ nhiều năm vì nghe...chị mình nói không hay, nhưng cũng may vì một lần mình vô tình cầm phải cuốn truyện và cuối cùng là đâm ra mê mệt đến độ không thể nào rời được cuốn truyện.
P/s: Nếu bạn nào chưa đọc thì mình nghĩ các bạn nên đọc thử để cảm nhận câu truyện tuyệt vời này ^^
5
165261
2014-02-26 18:40:22
--------------------------
83472
10048
Mình đã từng đọc qua Doraemon Bóng Chày phiên bản cũ nhưng đã bỏ khá lâu do truyện lâu phát hành. Bẵng đi 1 thơi gian thì cũng quên luôn truyện vì bận nhiều việc quá. Giờ thấy có phiên bản mới thì mình rất vui vì đã có cơ hội được đọc lại bộ truyện mình rất thích. Đây là 1 tác phẩm có tính giáo dục cao của bác FFF. Truyện cho mình hiểu thêm về bóng chày - một môn thể thao rất được ưa chuông ở Nhật Bản - quốc gia mà mình rất thích và trên hết là giúp mình biết được sức mạnh của lòng quyết tâm. Đọc truyện, từng nhân vật với từng cá tính riêng, từng câu chuyện riêng tưởng chừng như khó hòa hợp nhưng có cùng niềm đam mê bóng chày đã cùng tao nên đội bóng Đôrê. Trải qua nhiều thất bại nhưng nhờ lòng quyết tâm, ý chí rèn luyện và niềm khát khao chiến thắng, đội Đôrê cũng là 1 trong những đội hàng đầu giải nghiệp dư. Xen lẫn trong những trận đấu là những câu chuyện hay, có ý nghĩa và không kém phần hài hước về tình bạn của các chứ mèo máy. Nếu đã mê Doraemon hay mê bóng chày thì không thể không đọc qua bộ truyện này !
5
91453
2013-06-26 18:20:05
--------------------------
