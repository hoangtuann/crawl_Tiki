340724
10071
Ngày nay, giữa 1 rừng thông tin về "thời kỳ vàng", về "giáo dục sớm", về dạy con biết đọc, phát triển bán cầu não phải, có lẽ nhiều bậc phụ huynh cũng như tôi, cảm thấy bối rối, mệt mỏi và đầy áp lực... Từng giây, từng phút của đứa trẻ đều cực kỳ quan trọng, "chờ đến mẫu giáo thì đã muộn", và nếu ta không làm gì cho con, giai đoạn đã qua sẽ không bao giờ lấy lại được, và con bạn sẽ phải chịu thiệt thòi hơn so với những đứa trẻ được bố mẹ đầu tư, tìm hiẻu và giáo dục sớm. Những bộ Flash Card cũng những buổi hội thảo đắt tiền được tung ra, ai có điều kiện thì đầu tư, ai không thì cố tìm tài liệu tự làm. Rồi bối rối khi con không hợp tác... Rồi cảm thấy mình thật bất tài, vô dụng, làm phí thời gian vàng của con, chưa biết cách dạy con... Có thể nói, cuốn sách này là 1 chìa khoá giúp bạn thoát khỏi những áp lực, những căng thẳng đó! Bạn hãy thư giãn đi! Vì đơn giản,  "bạn chỉ cần dành thời gian chơi đùa cùng con, trò chuyện với con về những gì xảy ra trong thế giới của con, cảm nhận và tận hưởng tình cảm ruột thịt. Vậy là bạn đã làm được điều tốt đẹp nhất mà các bậc cha mẹ có thể làm để giúp con phát triển trí tuệ, cảm xúc, đảm bảo con sẽ thành công trong tương lai" (trích từ nội dung sách). Hãy nhớ: Einstien never used flash cards!!!!
5
7496
2015-11-20 13:39:12
--------------------------
176446
10071
Mình đi mua quyển sách này ở hội chợ sách mới diễn ra và cái tựa khiến mình cảm thấy có chút bối rối. Khi cầm lên đọc sơ và khi đem ra tính tiền, có một số người nhìn mình như thể mình là người mẹ tham vọng lố lăng ham muốn xa vời. Mình có chút bực mình vì cái tựa dịch quá xa với tựa gốc.

Thời con mình sinh ra cũng đang là giai đoạn bùng nổ của các phương pháp Glen Doman, mình cũng bị cuốn vào vòng xoáy đấy vì ai mà không có nhiều hy vọng ở con, không muốn con mình được thông minh tài giỏi. Tuy nhiên sau đó mình mới biết mọi chuyện không phải màu hồng vì vừa chăm con vừa lo "giáo dục sớm" như thế thật ngoài khả năng.

Rồi mình cũng bỏ cuộc ngang và vô cùng áy náy lẫn lo lắng khi cảm giác con mình sẽ thua kém so với xã hội. Cuốn sách này lấy lại cho mình niềm tin đã mất, và mình cũng biết mình cần phải làm gì bây giờ :)
5
98833
2015-04-01 11:25:47
--------------------------
159339
10071
Ở thời buổi mà phụ huynh luôn muốn con mình " thông minh vượt bậc" thì cuốn sách này giúp nhiều phụ huynh " bình tâm" hơn mà không phải " chạy đua" với cái gọi là " giáo dục sớm". Sách phân tích nhiều khía cạnh hay về thế nào là " giáo dục sớm", các đồ chơi trên thị trường giúp ích gì cho con trẻ hay ba mẹ nên làm thế nào để giúp con phát huy hết khả năng vốn có. Là cha mẹ, ai cũng muốn con cái mình phát triển vượt bậc nhưng ít ai chịu khó nghiên cứu, tìm tòi để phát hiện ra cách giáo dục con tốt nhất, phù hợp nhất, giúp trẻ phát huy khả năng, vừa học, vừa chơi mà không gò bó, ép buộc trẻ phải làm theo ý muốn của cha mẹ. Tôi nghĩ đây là cuốn sách mà các bậc phụ huynh nên đọc để tìm ra được cách giáo dục con phù hợp.
5
15022
2015-02-16 13:36:14
--------------------------
