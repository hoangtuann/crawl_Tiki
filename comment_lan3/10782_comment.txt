10361
10782
Helen Berr làm tôi nhớ đến một người con gái anh dũng của Việt Nam - Đặng Thùy Trâm. Cùng chung hoàn cảnh - sống trong thời kháng chiến - những người phụ nữ thông minh và kiên cường đó đã chiến đấu để vượt lên số phận, vượt lên nỗi đau mà lịch sử đã tạo ra cho họ và cho đồng bào họ. Ở Helen Berr, ta thấy trên hết là niềm lạc quan và tình yêu với cuộc đời. Trước cái ngày định mệnh bị đưa vào trại tập trung dành cho người Do Thái, cuộc sống của cô đang rất sôi nổi và tươi tẵn với những người bạn, những giờ học trên lớp và những buổi hòa nhạc. Rồi chiến tranh đến, mang đi những giây phút êm đềm, nhưng cũng từ đây ta phát hiện sức mạnh nội tâm và tình yêu nước mạnh mẽ của Helen Berr. Cô muốn mình tiếp tục chiến đấu vì đất nước, vì những người Do Thái bị đối xử bất công chứ không muốn "rời đi" như một kẻ chạy trốn. Dường như trong mọi khó khăn và bế tắc cô đều tìm được lối ra bằng tinh thần lạc quan và thái độ sống kiên cường. 
4
9273
2011-08-26 14:57:07
--------------------------
