575513
10364
Cuốn sách khá dày gần 500 trang, nhưng giá khá rẻ. Nói chung đọc tập nào Dạy con làm giàu cũng hay. Mỗi tập có một nội dung phân tích khác nhau, tập này phân tích các đồn bẩy để định hướng vào mục đích của mình. Tiki gửi hàng rất nhanh, mình ở đà nẵng chỉ mất ba ngày sau khi đặt hàng. Cám ơn đội ngũ Tiki
5
5184678
2017-04-17 13:30:46
--------------------------
501225
10364
Đây là một cuốn sách hay, đáng để đọc. Tôi tiếp xúc với nó quá muộn và cảm thấy hối tiếc thời gian mình đã lãng phí. 
4
1132619
2016-12-28 08:21:35
--------------------------
460009
10364
Tập V này, có vẻ nó hơi lòng vòng và khó hiểu. Quá nhiều lí thuyết đối với bản thân tôi, chính vì vậy mà tôi không thể nhớ hết được, đọc cứ như một mớ lí thuyết đã được nói ở các phần trước, tưởng như cụ thể hóa nhưng tôi thấy dường như là vòng vèo hơn. Còn có những chỗ thì lại hơi chung chung quá. Nói chung, tôi khá thất vọng đối với tập này, nó không có nhiều hữu ích cho tôi lắm, ở những tập trước, tôi rút ra được bài học cho bản thân, còn ở tập này lại không như vậy.
3
920424
2016-06-26 18:38:02
--------------------------
450370
10364
Dạy con làm giàu tập V cung cấp cho độc giả phương pháp vì sao người giàu lại giàu nhanh chóng và nghỉ hưu sớm cùng cách thức để bạn có thể làm được như họ. Phương pháp sử dụng là phương pháp đòn bẩy, nhờ đó họ không đợi đến khi tiết kiệm đủ tiền mới làm điều đó mà sử dụng phương pháp đòn bẩy. Các chương đầu cung cấp lý do sao phải làm giàu để về hưu sớm, các chương sau cung cấp sức mạng đòn bẩy của trí óc, sự trung thực, hành động, thói quen ....
3
769150
2016-06-18 20:09:08
--------------------------
397382
10364
Cuốn Dạy Con Làm Giàu V - Để Có Sức Mạnh Về Tài Chính tập 5 là một trong những cuốn hay nhất và dày nhất của bộ sách Rich Dad - Dạy con làm giàu. Khi nhận, cuốn sách được bọc nilon rất cẩn thận để bảo quản sách không bị quăn bị ố. Sách tuy dày gần 500 trang nhưng cầm rất nhẹ tay. Nội dung cuốn sách được chia ra làm nhiều phần logic, mỗi phần đều có các chương giải đáp những thắc mắc, vạch ra những hướng đi, mở ra suy nghĩ cho người đọc còn đang bối rối trước nhiều điều của cuộc sống. Đọc xong cuốn này tuy không thể làm theo đc nhiều điều nhưng cũng thay đổi được phần nào cách nhìn nhận vấn đề, đặc biệt là về tài chính như tôi
5
854660
2016-03-14 21:23:27
--------------------------
294022
10364
Ở cuốn sách này tác giả chia sẻ khá nhiều kiến thức về sức mạnh tài chính. Ở phần 1, tác giả chia sẻ về cách làm thế nào để trở nên giàu có và được nghỉ hưu sớm. Hành trình từ bờ vực phá sản lên đến đỉnh cao của sự giàu có, và được nghỉ hưu trong vòng chưa đến 10 năm của ông cùng với vợ ông và người bạn thân nhất - Larry, được chia sẻ khá chi tiết. Và những điều mà cả 3 người họ làm bị mọi người nghi ngờ, nhưng chính vì sự nghi ngờ đó, họ đã mạo hiểm và thành công. "Hãy làm đi" để có thể nghỉ hưu sớm, làm việc không phải đề trả lương, và những người giàu có giàu không phải vì lương, vì được trả lương rất ít nhưng họ giàu tài sản có được là nhờ vào đầu tư. 
4
156324
2015-09-08 22:43:34
--------------------------
282409
10364
Có vẻ như rất nhiều người mong muốn được nghỉ hưu sớm nhưng vẫn đảm bảo tiền vẫn chảy vào túi, và số tiền đó dư để chúng ta sống một cuộc đời dư dả, không phải lo nghĩ. Vậy thì cuốn sách này chính là thứ mà họ cần. Tôi nghĩ đây là quyển sách hay nhất trong số tất cả những quyển sách về dạy con làm giàu của tác giả. Cuốn sách đưa ra những hướng dẫn cụ thể và thực tế để cho người đọc dễ áp dụng, đồng thời nó mang lại một nguồn cảm hứng lớn lao cho độc giả
5
169373
2015-08-29 09:32:24
--------------------------
278654
10364
Tôi đã đọc qua 2 quyển của bộ sách Dạy con làm giàu và quyết định tìm mua quyển 5 này vì bị kích thích bởi lời tựa Nghỉ hưu sớm Nghỉ hưu giàu ( Retire young Retire rich). Cũng là sách về lĩnh vực kinh tế nhưng tôi thấy nó không khô khan nhàm chán như một số sách khác tôi đã đọc. Tôi thích cách tác giả đề cập vì sao chàng David tìm gặp Người khổng lồ? Và chúng ta cũng cần đánh thức kẻ khổng lồ đang im ngủ trong chính bản thân chúng ta. Bằng cách nào? Quyển Dạy con làm giàu 5 đã giúp tôi tìm được câu trả lời. "Đôi khi chúng ta cần phải học cách không làm những điều mà chúng ta được dạy nên làm". Một quyển sách khá thú vị.
5
471107
2015-08-26 00:56:16
--------------------------
261866
10364
Tôi năm nay 23 tuổi, đã theo con đường làm nhân viên được 01 năm. Sau 01 năm làm việc tôi nhận thấy ngoài kinh nghiệm ra, mình không có được 1 tài sản nào giá trị cả. Tất cả là do tư duy lối mòn của thời đại công nghiệp mang lại. Trong khi hiện nay, thời đại công nghệ phát triển vượt bậc, việc trông chờ vào đồng lương để sống tốt, sống khỏe là bất khả thi. Biết được Bộ sách Rich Dad Poor Dad từ rất lâu rồi, tuy nhiên đến hôm nay tôi mới chính thức được nghiền ngẫm từng trang sách. Càng đọc, càng thấy rõ con đường vô định mình đang đi là sai lầm. Tác giả Robert đã vạch ra con đường mà mỗi chúng ta nên thay đổi: thay đổi trong tư duy tài chính, thay đổi trong phương pháp làm việc,... Cuốn Số V này theo tôi là cuốn hay nhất trong Bộ sách trên, nó  cung cấp cho người đọc câu trả lời HOW TO - phương thức để làm và vận dụng ngay chứ không phải chỉ là những lý thuyết suông. Cuốn sách này là một trong những cuốn nên có trên kệ sách của bất cứ ai ham học hỏi và muốn làm giàu càng sớm càng tốt.
5
78338
2015-08-11 14:44:05
--------------------------
135931
10364
Robert Kiyosaki bắt đầu sự nghiệp với 2 bàn tay trắng và trong 10 năm, ông quyết định nghỉ hưu mà vẫn tự do về tài chính. Ông tân hưởng cuộc sống mà tất cả chúng ta đều mơ ước. Một trong những tư duy khiến ông thành công đó là thành công trong một thế giới không tiền lương. Nghe có vẻ lạ nhưng thực sự vậy. Kiyosaki giải thích rằng hầu hết mọi người đều có 50 đến 100% thu nhập từ cái gọi là CÔNG VIỆC. Họ hoàn toàn sai lầm! Những người nghỉ hưu sớm mà vẫn giàu thu nhập thừ những nguồn bị động và những phần tích lũy được.
NGHỈ HƯU SỚM NGHỈ HƯU GIÀU là một quyển sách xuất sắc cho những ai muốn nghỉ hưu mà vẫn tự do tài chính và độc lập tài chính trong vòng 10 năm hoặc ít hơn, không cần biết hiện tại họ làm lương thế nào và mức độ giàu nghèo ra sao
Một quyển sách hay. Có thể là quyển hay nhất trong series DẠY CON LÀM GIÀU!
5
462453
2014-11-17 23:45:00
--------------------------
123704
10364
Ở cuốn sách này, tác giả đã phân tích về sức mạnh của các đòn bẩy và làm thế nào để sử dụng sức mạnh của nợ vào mục đích của mình. Tác giả khẳng định mọi người chúng ta đều có tiềm năng để giàu có và đưa ra lời giải thích rất thuyết phục. Tác giả khẳng định có 3 con đường để đi đến giàu có và muốn làm giàu phải sử dụng tiền của người khác. Làm việc ít, kiếm tiền nhiều là điều mà bất cứ ai cũng muốn và chỉ có nguồn thu nhập thụ động mới làm được điều này vì nó ít thuế nhất, mang lại thu nhập trong thời kỳ lâu dài.
1 câu nói mình ấn tượng nhất của tác giả là:" về hưu sớm và nghỉ hưu 1 năm là để bắt đầu cuộc đời 1 lần nữa". Tuy nhiên, mình thấy nhiều chỗ tác giả phân tích không làm sáng tỏ được đề mục đưa ra.
3
381173
2014-09-03 09:44:53
--------------------------
