374149
10436
Hài hước là phong cách quen thuộc của C.Craig và truyện này cũng không phải ngoại lệ. Tuy không hấp dẫn bằng “Hôn lễ tử thần” nhưng dù sao cuốn này vẫn đáng đọc hơn cả. Truyện bắt đầu bằng cuộc gặp mặt oái ăm giữa Shala & Sky khi chiếc máy ảnh của cô bị Sky tịch thu mất, vì chiếc máy ảnh mà hai người họ giằng co hết lần này đến lần khác và còn vô tình bị lôi vào một vụ án. Tuy vậy cũng nhờ chiếc máy ảnh mà cả hai mới có dịp tiến sâu trong một mối quan hệ đặc biệt đầy lãng mạn và cũng rất hài hước. Quả thật tôi rất thích nhân vật Shala, một cô gái đầy mạnh mẽ, bản lĩnh và cũng lớn “gan”, cô chả sợ cái gì hết và luôn luôn lý luận rất “logic”, buộc Sky hết lần này đến lần khác phải rối não với cô. Các nhân vật phụ như cha của Sky cũng khiến câu chuyện trở nên hấp dẫn hơn, ông luôn để các giấc mơ chỉ đường cho mình và luôn tin vào những giấc mộng báo trước, xác suất chính xác cũng khá là đáng nể.

Truyện đọc hay, nhiều chỗ dễ thương. Có thể nói các sách của C.Craig rất có sức hút nhưng lại được xuất bản không nhiều, nếu có cơ hội được đọc nhiều hơn sách của bà thì chắc chắn tôi sẽ tìm đọc.

4
41370
2016-01-25 17:30:38
--------------------------
360731
10436
Cuốn truyện không có điểm nào hấp dẫn người xem đi sâu vào quá trình hoàn thành mục đích cao đẹp làm ngẫm nghĩ về con người và cái tâm hồn của cái thứ yếu đem lại được , phải chăng sức mạnh ấy còn thoi thóp , nhỏ bé đến nỗi chưa thể bùng cháy dữ dội trong tác phẩm cần nhấn mạnh yếu tố lớn để phát huy thế mạnh của nhân vật cũng như tác giả đề đạt lên những thứ cần có trong thế giới vô hình , ảo ảnh này không đủ ta đắm chìm .
3
402468
2015-12-29 18:28:28
--------------------------
309542
10436
Mình quyết định mua quyển sách này vì tựa đề "Im miệng và hãy hôn em đi", nghe có chút gì đó rất bá đạo. Truyện của Christie Craig bao giờ cũng vậy, nhẹ nhàng, đáng yêu nhưng có chút gì đó rất hài hước. "Im miệng và hãy hôn em đi" có cốt truyện đặc sắc, hình tượng các nhân vật được xây dựng rất đặc biệt, sống động, chân thật, các tình tiết trong truyện vừa lãng mạn vừa hồi hộp, tạo nên một chuyện tình thú vị. Mình thật sự rất thích tác phẩm này của Christie Craig, đây là một tác phẩm rất đáng đọc.
4
115397
2015-09-19 00:28:41
--------------------------
278302
10436
Phải nói là mình khá ghét văn học phương tây vì những cuốn trước đây mình đọc dịch khá chán, vẫn như bê nguyên xi cuốn sách chưa dịch vào vậy nhưng với cuốn Im miệng và hãy hôn em đi này thì mình thấy rất thích thú, dịch giả dịch rất hay rất mượt không còn cái chất của cách viết phương tây nữa, các tình tiết trong truyện vừa gay cấn vừa đan xen lãng mạn, các nút thắt, những trắc trở được tác giả giải quyết ổn thỏa và lí thú, sách có chất lượng in tốt, giấy cầm hơi nặng tay nhưng khó cũ, rất thích
4
182813
2015-08-25 20:27:18
--------------------------
222029
10436
Tựa đề hấp dẫn và nội dung cũng hay không kém, cách thể hiện các nhân vật cũng rất chi tiết. Tác giả không chỉ tập trung vào rắc rối tình cảm của 2 nhân vật chính mà tất cả các nhân vật khác cũng có những rắc rối của họ trong vấn đề tình cảm. Mỗi câu chuyện được Christie Craig chăm chút và cuối cùng tất cả đều tìm được "tâm hồn đồng điệu" của mình. Rất thích cách xây dựng câu chuyện tình yêu lãng mạn xen kẽ với những tình tiết trinh thám làm người đọc vừa hạnh phúc với hồi hợp. 
4
471112
2015-07-04 20:40:39
--------------------------
82879
10436
Mỗi câu chuyện của Christie Craig luôn là một trải nghiệm thú vị, và mình cảm thấy thật sự thích thú những anh chàng cảnh sát Mỹ nhiều hơn sau mỗi tác phẩm của bà. Và câu chuyện lần này lại thêm một trải nghiệm hài hước nữa giữa Shala và Sky, và lần này, khi những bức ảnh của Shala là bằng chứng đắt giá thì Sky đã phải ra tay bảo vệ cô. 
Với những tình tiết hài hước, lãng mạn cùng với sự hồi hộp đan xen, Christie Craig luôn mang đến cho mình trải nghiệm thú vị không chỉ bởi những phong tục kỳ lại trong một thị trấn nhỏ bé, mang phong vị riêng mà còn ở tình cảm giữa gia đình, bạn bè, những con người rất sống động và chân thật.
4
125574
2013-06-23 22:01:00
--------------------------
59050
10436
Cá nhân mình rất thích truyện của tác giả Christine Craig nhưng nếu đem so sánh hai tác phẩm Hôn lễ thử thần với Im miệng và hãy hôn em đi thì mình thấy Tác phẩm Hôn lễ tử thần có phần vượt trội hơn. Về cả cốt truyện, nội dung và chi tiết thì Hôn lễ thử thần vẫn hay hơn so với tác phẩm này. Nhưng cách xây dựng câu chuyện tình yêu lãng mạn xen kẽ với những tình tiết  trinh thám làm người đọc vừa hạnh phúc với xúc cảm nhưng đồng thời cũng hồi hộp theo từng diễn biến câu chuyện. Nói tóm lại thì đây là một tác phẩm khá là được, đáng có mặt trong tủ sách của những bạn thích tiều thuyết lãng mạn. 
3
41936
2013-02-10 00:54:49
--------------------------
56395
10436
Christie Craig là tác giả nổi danh với phong cách viết hài hước, nhẹ nhàng với những cốt truyện hấp dẫn và những câu văn bạn không thể không bật cười. "Im miệng và hãy hôn em đi" là một minh chứng cho điều đó.
Một kẻ sát nhân cứ mãi đuổi theo một cô nàng xinh đẹp được bao bọc bở một  sky, một người nóng tính và cứ ham muốn chiếm lấy đôi môi và vị ngọt chiếc lưỡi nơi cô. Liệu gã sát nhân đó nguy hiểm hay anh nguy hiểm với cô hơn?
Một chiếc máy ảnh mang đến cho cô một kẻ sát nhân và một anh chàng cảnh sát đẹp trai, phong độ. Cô sẽ làm gì?
Có thể nói câu chuyện này hấp dẫn người xem ngay từ tựa đề với yếu tố lãng mạn và sự liên tưởng một nụ hôn nồng cháy kiêu ngạo của Shala
*Một số nhận xét khác:
_Bìa sách phản ánh đúng những gì mà tựa sách thể hiện
_Câu văn với nội dung hấp dẫn
Mình cho cuốn sách này 7/10 điểm
3
15919
2013-01-21 14:36:28
--------------------------
52647
10436
Im miệng và hãy hôn em đi là cuốn thứ 2 mình đọc của Christie Craig. Phải nói một điều rằng, không phải ai cũng có thể phối hợp được cả sự lãng mạn tuyệt vời, những pha hành động gay cấn và những màn hài hước ngất trời như kiểu Craig đã làm. Cả hai nhân vật chính của Craig, Shala và Sky đều là những con người cần mẫn, có tâm hồn phóng khoáng và nhiều tình yêu để cho đi. Xem họ đối đầu với nhau lúc ban đầu rất vui, nhưng mình càng hài lòng hơn khi thấy họ yêu nhau. Tuyến nhân vật phụ cũng làm cho câu chuyện hấp dẫn hơn nữa. Mình thực sự thích gia đình và những người bạn của Sky, đặc biệt là Lucas và thực sự hy vọng Craig sẽ viết một câu chuyện riêng cho anh.
4
5548
2012-12-28 08:11:32
--------------------------
