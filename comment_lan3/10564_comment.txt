271480
10564
Cuốn này nằm trong bộ "Mười vạn câu hỏi vì sao" mà tôi mua để giúp tôi trả lời các câu hỏi của cậu con trai 5 tuổi. Nhiều lần tôi thật sự bối rối trước những câu hỏi "hóc búa" của cậu con trai. Các bé tuổi này thích khám phá thế giới xung quanh bằng những quan sát và đặt những câu hỏi. Bên cạnh đó, những lúc rảnh rỗi chơi cùng con, đọc con nghe để nâng cao hiểu biết của con là những khoảnh khắc tuyệt vời của những người làm bố mẹ. Có những câu hỏi không phù hợp với tuổi của con, quá khó để con hiểu được thì tôi để dành khi con lớn hơn.
5
354501
2015-08-19 10:04:49
--------------------------
88456
10564
Cuốn sách giúp cho tôi hiểu biết nhiều hơn về những hiện tượng xảy ra xung quanh và trả lời những thắc những điều bình dị nhất mà có khi tôi cũng chưa từng đặt câu hỏi vì sao? Những điều câu hỏi thú vị có thể làm hành trang giúp tôi hiểu biết hơn và điều quan trọng hơn nữa sau này khi lập gia đình tôi có thể dạy con mình tốt hơn, giải đáp những câu hỏi của con mình. Giúp con mình hiểu biết hơn về những gì xảy ra xung quanh và điều quan trọng là cuốn sách rất hữu ích cho không chỉ tôi mà cho gia đình tôi sau này.
5
138472
2013-07-26 14:32:12
--------------------------
