579117
10470
Sách hay. Phù hợp cho bạn nào đam mê tìm hiểu về ngành Kinh tế học hành vi.
5
1730021
2017-04-20 20:53:58
--------------------------
549925
10470
Sách về tâm lý học hành vi khá hay. Tuy nhiên, không hiểu do từ ngữ hơi hàm lâm hay do cách dịch mà sách hơi khó hiểu. Cố đọc hết từng chương sách và suy ngẫm sẽ được rất nhiều ứng dụng của cú hích vào thực tế đời sống cũng như kinh tế.
4
433458
2017-03-21 22:32:41
--------------------------
549593
10470
Sách có nội dung hay, đọc khá cuốn hút. Tiki giao hàng đúng hẹn. Tuy nhiên sách bị lẫn lộn giữa các trang. Ví dụ trang 112 nhảy sang trang 154 và một số trang sau đó nữa. Đang đọc hấp dẫn thì tự dưng mất trang, đọc không hiểu gì, dễ cụt hứng.
Mong các bạn đọc cân nhắc kỹ trước khi mua tái bản này và tiki có hành động tích cực.
1
1062076
2017-03-21 15:27:23
--------------------------
464600
10470
Mình rất thích đọc thể loại sách liên quan đến kinh tế học hành vi và cũng đọc khá nhiều sách về thể loại này trước đây. Sau đó mình quyết định mua quyển này vì nhận được sự giới thiệu của một vài người bạn. Nội dung của quyển sách khá dễ tiêu hóa. Tuy quyển sách không dày nhưng mang đến nhiều thông tin thực sự mới lạ và hữu ích và đưa người đọc đến những nhận xét hoàn toàn mới lạ và khác biệt khiến mình thay đổi suy nghĩ rất nhiều sau khi đọc quyển sách.
4
196347
2016-06-30 12:07:27
--------------------------
451294
10470
Ai đã đọc sách về kinh tế nói chung và kinh tế học hành vi nói riêng thì cái sự khác biệt mà cuốn sách mang lại phải đặt ra xem xét

Mình khá ấn tượng về nhan đề: Cú hích-cuốn sách tạo nên sự khác biệt và bìa sách, nội dung thì như đã nói, là những điều đã ít nhiều được nói đến, khai thác trong những cuốn sách khác, những điều mà bạn rõ mừoi mưoi rồi. 

Tuy nhiên, bù lại, cuốn sách là tổng hợp (dù chưa thực sự đầy đủ) những cú hích trong cuộc sống đáng để tâm quyết định thành hay bại của một kế hoạch, dự án, công việc.... với việc trình bày, và phân tích dẫn chứng rõ ràng, rành mạch

À, mình còn chưa có động lực đọc nốt chương cuối của sách :(
2
600542
2016-06-19 16:40:16
--------------------------
427062
10470
Cuốn sách đưa cho ta cái nhìn về Kiến trúc lựa chọn, giúp ta hiểu về các tác động ảnh hưởng tới lựa chọn giúp ta đỡ bối rối trước những lựa chọn của mình.

Và cao hơn nữa, nếu thực sự áp dụng được những kĩ năng xây dựng những lựa chọn, mình có thể xây dựng những lựa chọn làm cuộc sống của mọi người xung quanh tốt đẹp và dễ dàng hơn nhưng vẫn đảm bảo tính tự do tuyệt đối của mọi người.

Cuốn sách đáng đọc với mọi người, và ước gì những lãnh đạo của Việt Nam cũng đọc những cuốn sách như thế này và dần thay đổi cách lãnh đạo làm cuộc sống nhân dân dễ dàng và hạnh phúc hơn!

Nhưng mình nghĩ cuốn sách này không phải đọc ngấu nghiến hết ngay, mà phải đọc từ từ, dần dần. Có thể thấy chưa ngẫm hết thì dừng vài hôm, hoặc đọc cuốn khác rồi tiếp tục đọc. Và nên đọc 2 lần, 3 lần,... tùy người để hiểu hết, mình mới đọc xong có 1 lần!

Cảm ơn Tiki, Nxb vàTác giả!
5
828175
2016-05-08 10:46:19
--------------------------
403218
10470
“ Cú hích  - Cuốn Sách Tạo Nên Sự Khác Biệt “ là một cuốn sách rất đáng để đọc. Một cuốn sách hơi khó đọc nhưng đầy sáng tạo, nội dung sách vừa mang tính hàn lâm và vừa mang tính thực tiễn rất cao, người đọc có thể ứng dụng những gì đã đọc trong sách để cải thiện cuộc sống của mình và những người xung quanh. Bìa thiết kế cũng tạm được, màu sách hơi buồn. Sách này có thể áp dụng được cho mọi lứa tuổi. Nói chung tôi không hối hận khi mua cuốn sách này. Tôi rất thích cuốn sách này.
4
730635
2016-03-23 14:14:25
--------------------------
398801
10470
Khi thấy tựa sách là "Cú hích", tôi đã mường tượng ra được nội dung của cuốn sách này, nhưng không nghĩ là cú hích của nó lại mạnh đến như vậy, một cú hích mang ta tới đích, mà nơi đó chắc chắn ta sẽ tìm được điều chúng ta cần. Đối với cuốn sách này, có lẽ là câu chữ hơi khó đọc, nhưng nếu tâm trí bạn dồn hết vào nội dung cuốn sách này, thì tôi đảm bảo rằng bạn sẽ không dứt nó ra được. Những điều tôi học được từ cuốn sách này chính là kinh nghiệm và áp dụng những kinh nghiệm vào thực tế. Đừng chần chừ nếu bạn muốn mua nó!
5
211114
2016-03-16 19:07:22
--------------------------
372609
10470
Mình được biết đến quyển sách này khi đọc một bài báo về 10 quyển sách có thể thay đổi cách suy nghĩ của bạn mãi mãi. Và quả thật như vậy, "Cú hích" thực sự là một tác phẩm với những quan điểm và tư duy mới lạ về việc đưa ra các lựa chọn trong cuộc sống, công việc cũng như gia đình. Tuy nhiên, so với lúc đọc bản tiếng anh, mình cảm giác bản dịch tiếng Việt khá khó hiểu, có lẽ vì sách chứa đựng rất nhiều thuật ngữ mới lạ không có sẵn trong tiếng Việt.
4
143116
2016-01-21 22:30:56
--------------------------
346298
10470
Nếu cuốn sách là một ứng dụng mới thì con người có thể đem ra học hỏi và trải nghiệm được nhiều thực tế hiện rõ . Cuốn mang lại tư duy khác , có nhiều thực tiễn mà con người chưa thực sự hiểu hết , vận dụng nhiều kiến thức cho cuộc sống phát triển và nổi bật hơn , có thể mang đến nguồn năng lượng mới dồi dào . " Cú hích " như là lời mấu chốt khẳng định cao về quyền lợi hay sức mạnh con người được sáng tỏ hơn . Tư duy cũng khác , tích cực , nhanh hơn .
4
402468
2015-12-02 12:43:45
--------------------------
342988
10470
Ấn tượng ngay từ lời tựa đầu tiên: "Cái đập cánh của con bướm có thể gây ra cơn địa chấn ở cách xa nó hàng vạn dặm", quả đúng vậy. Đọc quyển sách, tôi tự rút ra được khá nhiều điều bổ ích cho mình. Nhất là kinh nghiệm thành công được rút ra từ những cơ hội chớp nhoáng mà bạn bắt buộc phải đưa ra quyết định, nó có thể là quyết định đúng đắn, cũng có thể là quyết định sai. Nhưng chính những quyết định sai đó lại làm nên quyết định đúng sau này của bạn. Mong rằng NXB Trí Việt sẽ xuất bản được nhiều quyển sách bổ ích như vậy nữa.
4
105666
2015-11-25 15:39:04
--------------------------
338488
10470
Có bao giờ chúng ta tự hỏi tại sao chúng ta lại quyết định như vậy, hoặc điều gì khiến chúng ta hành động như vậy. Cuộc đời chúng ta có phải là do số phận an bài xếp đặt hay không? Cách chúng ta quyết định một vấn đề nào đó thông thường là theo phép thử sai, trong hàng nghìn phép thử thì ta chọn ra 1 phương pháp đúng, nhưng cách này thường khiến ta trả giá vì mất quá nhiều nguồn lực vào đó. Vậy điểm mấu chốt là ta phải tìm ra yếu huyệt, đi thẳng đến vấn đề và đó chính là "cú hích" giúp ta điểm đúng huyệt đó.
4
465332
2015-11-16 11:24:25
--------------------------
306492
10470
Lựa chọn là một phần hiện hữu trong từng khoảnh khắc cuộc sống. Sáng sớm chúng ta phải lựa chọn mình phải làm j đầu tiên, và tiếp theo, và tiếp theo... Chúng ta phải lựa chọn trong nhiều vấn đề sự nghiệp, tình cảm, gia đình, bạn bè,.. Tôi tìm đọc cuốn sách trên vì tôi muốn hiểu hơn về cách mỗi chúng ta lựa chọn, cách bộ não chúng ta đưa ra quyết định. Trước đó tôi không hề hình dung việc lựa chọn lại là một quá trình nghiêm ngặt và chịu tác động của nhiều nhân tố như thế.

Sách hơi khó hiểu nhưn nếu chú tâm, tôi nghĩ bạn sẽ học được nhiều thứ về cách chúng ta đưa ra quyết định. Điều đó rất có ích cho mỗi người.
5
258935
2015-09-17 16:50:14
--------------------------
297506
10470
Mỗi ngày chúng ta phải ra rất nhiều quyết định, nhưng không phải lúc nào cũng là quyết định đúng đắn và mang lại hiểu quả. “Cú hích” của Richard H. Thaler và Cass R. Sunstein là một cuốn sách tạo ra sự khác biết, giúp chúng ta có cái nhìn mới hơn về mọi sự việc. Tác giả chia ra nhiều phần rõ ràng, chi tiết từ con người, tiền bạc, sức khỏe, quyền tự do,… Ngoài ra còn có sự mở rộng và những ý kiến trái chiều nữa. Mình cảm thấy đây là quyển sách bổ ích, mang lại rất nhiều kiến thức.
Xin cảm ơn.
4
554150
2015-09-11 21:58:59
--------------------------
266505
10470
Bìa sách có ý tưởng hay, chất lượng của cả cuốn sách tốt và rất có giá trị thực tiễn. Đọc cuốn sách “Cú Hích - Cuốn Sách Tạo Nên Sự Khác Biệt” của các tác giả Richard H. Thaler và Sunstein, tôi đã có những nhìn nhận mới mẻ hơn về sự ảnh hưởng thị trường trong cuộc sống. Tự dưng, nhờ cuốn sách, tôi bắt đầu đam mê luôn lĩnh vực kinh tế học hành vi. Cách truyền tải hấp dẫn, không quá khô cũng hay giáo điều, các vấn đề được trình bày rõ ràng. Cuộc sống luôn có nhiều lựa chọn: việc chọn đúng là hướng đi đúng đắn cho thành công, còn sai là bài học cho thành công.
5
276840
2015-08-14 19:08:43
--------------------------
259133
10470
...nhưng hơi khó đọc và khó hiểu. Tôi là người đã nghiên cứu và tìm đọc về kinh tế học hành vi qua những đầu sách như Phi lý trí,... và tôi rất thích lĩnh vực này. Chuyên ngành và công việc của tôi liên quan đến đầu tư, đó là một lĩnh vực liên quan đến thị trường tài chính bậc cao, nơi kiến thức kinh viện từ các trường đại học thôi là chưa đủ mà quan trọng hơn là hiểu biết về tâm lý, cách người ta nhận thức và ra quyết định, cũng như ảnh hưởng của đám đông... Cuốn sách rất hay nhưng lối viết hơi dài dòng và khó hiểu, không thực sự phù hợp với các bạn trẻ chưa có nền tảng tốt về lĩnh vực này
3
473974
2015-08-09 09:31:11
--------------------------
256268
10470
Nếu là người yêu thích tâm lý học hành vi thì đây thật sự là một quyển sách rất nên đọc. Cùng với những quyển sách nổi tiếng khác được xuất bản ở Việt Nam về cùng chủ đề này như " Tư duy nhanh và chậm ", " Phi lý trí ", tuy đôi lúc có nhiều điểm tương đồng nhưng với cách tiếp cận phong phú, thú vị và rất tinh tế, quyển sách đồng thời bổ sung nhiều kiến thức mới, khiến ta không khỏi bất ngờ về những gì điều được truyền đạt khi đọc sách, mà còn tự chiêm nghiệm và tìm kiếm được động lực cho mình trong cuộc sống.
4
171151
2015-08-06 18:50:40
--------------------------
241591
10470
Cú hích của Richard Thaler thực sự đã tạo nên sự khác biệt. Cuốn sách như một cú hích của tác giả cho những ai đang băn khoăn, đang đắn đo trước nhiều định kiến, không hẳn là cuốn sách truyền động lực nhưng từ những bài học và lời khuyên của cuốn sách, bạn sẽ tìm ra chân lí và hướng đi cho mình. Nhẹ nhàng như một cái hích nhỏ vậy mà có khả năng kéo dài một chuỗi những việc phải xảy ra. Cái bạn cần là học tập từ bất cứ quyết định nào, vì cú hích nào xảy ra cũng có lý do của nó.
4
109789
2015-07-25 16:53:18
--------------------------
241540
10470
Cuốn sách mang tới cho mình rất nhiều góc nhìn khác, cách nhìn khác biệt trong việc thay đổi hành vi của người khác hay là cả một hế thống. 

Các gợi ý đưa ra không dễ ngấm, nhưng các ví dụ thì vô cùng hay, thú vị, khiến cho mình phải ngẫm nghĩ lại rất nhiều điều trong cuộc sống công việc marketing hàng ngày.

Một cuốn sách có tính ứng dụng cao không chỉ trong cuộc sống nói chung mà còn rất hữu ích cho những ai làm marketing, truyền thông và muốn tác động tới cộng đồng.
4
644734
2015-07-25 16:07:01
--------------------------
232568
10470
Cuốn sách như một "cú hích" cho ai đang thiếu những suy nghĩ trực quan cho cuộc sống. Cuốn sách sẽ giúp bạn nhìn nhận vấn đề bằng nhiều hướng khác nhau và giải quyết chúng theo những cách sáng tạo riêng, mang lại cảm hứng cho công việc cũng như nhiều mặt khác. Nó tạo cho tôi một động lực lớn để không ngừng cải thiện, cải thiện và cải thiện cách thức giải quyết những vấn đề của chính mình, không ngại ý tưởng đó là "điên rồ".
Tuy nhiện, cách trình bày khá khó hiểu nên để có thể cảm nhận tốt những gì sách đề cập cần có thời gian để đọc lại nhiều lần.
4
192671
2015-07-18 21:28:09
--------------------------
222669
10470
Mình đọc cuốn tư duy nhanh và chậm trước, trong đó Daniel Kahneman có nhắc đến Thaler khá nhiều, cha đẻ của kinh tế học hành vi, mình mới tìm đến cuốn này mong sẽ tăng thêm nhiều vốn kiến thức về kinh tế học hành vi. Nhưng hơi thất vọng một chút khi ý tưởng trùng lặp với tư duy nhanh và chậm khá nhiều, có lẽ Daniel Kahneman đã lấy những ví dụ của Thaler, điều này khiến cuốn sách không còn hấp dẫn lắm đối với rôi. Và tôi cũng không thích cách trình bày của sách cũng như size chữ to một cách khó đọc, nên khó hiểu được nội dung của sách. Tuy nhiên bên cạnh những nội dung trùng lặp với Tư duy nhanh và chậm thì những kiến thức còn lại cũng rất thú vị, nếu để tìm hiểu kinh tế học hành vi thì không thể bỏ qua.
3
74132
2015-07-05 23:51:14
--------------------------
70131
10470
 Mình biết tác phẩm này qua báo chí. Đó là một tác phẩm hay và có ý nghĩa, nó giúp mình hiểu rõ hơn về cuộc sống cũng như về chính mình, tìm thấy những gì tốt nhất cho mình. Nhưng cú hích làm chúng ta gục ngã nhưng nó giúp mình trở lại sau đó một cách mạnh mẽ và vững vàng hơn.  Giống như tựa đề đầu sách “Cái đập cánh của con bướm có thể gây ra cơn địa chấn ở cách xa nó hàng vạn dặm” (Hiệu ứng cánh bướm) – nếu chúng ta có sức mạnh , cùng nhau hợp tác chia sẻ những gì mình có chúng ta sẽ thành công và vượt qua mọi khó khăn thử thách.
4
63549
2013-04-19 10:50:55
--------------------------
68509
10470
"Cái đập cánh của con bướm có thể gây ra cơn địa chấn ở cách xa nó hàng vạn dặm." Câu nói ở bìa quả thực rất ấn tượng, làm tôi thấy thích thú với cuốn sách này mất rồi!
Hơn cả mong đợi, "Cú hích " còn hấp dẫn hơn thế nữa. Khác với những gì tôi tưởng tượng, nó đem tới một hướng tư duy mới mẻ. Thành công là nhờ những quyết định đúng rút ra từ kinh nghiệm, và kinh nghiệm có được là nhờ những quyết định sai; từ bí quyết đó, hai tác giả đã dẫn ta đến trước nhiều cánh cửa, là những sự lựa chọn trong cuộc sống. Nhưng ở đó không chỉ có mỗi ta mà còn có nhiều người khác, họ thì thầm với ta khiến ta bối rối trước những sự lựa chọn. Vậy thì làm thế nào để vượt qua những định kiến - những cú hích có tác động không nhỏ - và tìm được đúng cánh cửa phù hợp với mình? Tin tôi đi, bạn sẽ không phải thất vọng khi chọn cuốn sách này để tìm ra giải pháp cho mình. Đây mới chính là một cú hích thực sự có thể thay đổi hướng đi của mỗi người, để cuộc sống trở nên tốt đẹp hơn.
5
76230
2013-04-12 08:52:38
--------------------------
61663
10470
Ngay từ khi nhìn bìa sách, tôi đã có cảm giác đây là một cuốn sách đáng đọc, và quả thực số tiền mà tôi bỏ ra đã không bị lãng phí. Một cuốn sách đầy sáng tạo, hấp dẫn. Nó hội tụ cả tính hàn lâm và tính thực tiễn rất cao, bất cứ ai cũng có thể ứng dụng những gì được 2 tác giả trình bày để cải thiện cuộc sống của mình và của những người xung quanh. Cũng phải nói thêm rằng đối với những người chưa nghiên cứu nhiều về xã hội học thì cuốn sách này có thể hơi trừu tượng đôi chút. Dù vậy nó vẫn rất đáng đọc.
4
69743
2013-03-03 12:03:40
--------------------------
61514
10470
Sự khác biệt chính là những gì làm mình ấn tượng nhất khi đến với cuốn sách này. Tác giả đã tạo nên những trang viết đầy chất tri thức, với những lập luân sắc bén có thể thuyết phục cả những độc giả khó tính nhất. Đi theo một hướng khai thác mới mẻ và độc đáo, cuốn sách chỉ ra cho người đọc những bí quyết để thành công, những điểm đáng lưu ý để mỗi người có những quyết định đúng đắn nhất trong cuộc sống của mình. Tất cả được chuyển tải bằng ngôn ngữ sinh động, thu hút ngay từ những dòng đầu tiên. Đây chính là cuốn sách của cuộc sống, một tác phẩm khác biệt và hấp dẫn đến không ngờ.
4
20073
2013-03-02 18:19:51
--------------------------
