235297
10520
Tác giả Vũ Bội Tuyền đã bỏ ra công sức rất lớn để biên soạn cuốn sách 100 danh nhân khoa học nổi tiếng thế giới, điều này được minh chứng bằng độ dày của những dẫn chứng, các câu chuyện phong phú và bài học rút ra về cuộc đời mỗi danh nhân. Cuốn sách rất bổ ích, nó giúp người đọc hiểu về tiểu sử của những danh nhân khoa học trên mọi lĩnh vực, hiểu được họ đam mê công việc đến nhường nào và đã phải đánh đổi, cố gắng như thế nào để đạt được những thành tựu rực rỡ có ý nghĩa vô cùng lớn với nhân loại. Sau mỗi câu chuyện về một danh nhân, mình  tự thấy bản thân cần phải nỗ lực hơn rất nhiều để có thể gặt hái được thành công trong cuộc sống giống như gương của các nhà khoa học nổi tiếng thế giới.
4
164722
2015-07-20 23:39:51
--------------------------
