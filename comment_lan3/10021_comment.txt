472471
10021
Mình đã đọc rất nhiều tập Shin - Cậu bé bút chì, và tập truyện này cũng không phải ngoại lệ. Vẫn với những nét tinh nghịch, hài hước, những tình huống chân thực của cuộc sống gia đình, truyện không chỉ giúp mình thư giãn sảng khoái vì sự hài hước mà nhiều mẩu chuyện có giá trị nhân văn cao. Shin tuy nghịch ngợm nhưng tâm hồn cậu bé đầy tình yêu thương nhất là đối với gia đình nhỏ bé của mình. Tiki giao hàng nhanh. Chất lượng sách tốt. Tuy nhiên mình thấy nhiều đơn hàng bị tách ra làm nhiều đơn nhỏ nhất là truyện tranh thường hay về trước. Nhưng không ảnh hưởng gì nhiều lắm. Mình vẫn vote cho Tiki.
5
608186
2016-07-09 17:01:01
--------------------------
437964
10021
Mình rất thích đọc truyện shin cậu bé bút chì nên nghe nói ra tập đặc biệt là mình liền mua về đọc thử, toàn là những câu chuyện về cu shin và những sự việc xung quanh cậu nhóc tuy nhiên mình thấy quyển này không buồn cười cho lắm nếu buồn cười nhất chắc là lúc cu shin ngồi buôn điện thoại nhỏ mà nói giọng y như người lớn làm cho bố Shin tưởng mẹ Shin thế nào ngoài ra còn những chuyện rất hay và ý nghĩa khác nhưng mình thích buồn cười hơn nhưng truyện cũng khá hấp dẫn. Cảm ơn tác giả nhiều.
4
1102669
2016-05-29 07:47:38
--------------------------
64153
10021
Đang cầm trên tay cuốn truyện tập đầu tiên, tôi đang rất hạnh phúc và mắc cười trước những trò đùa tinh quái của cậu bé 5 tuổi này. Cậu đã đưa tôi trở lại những ngày hồn nhiên của tuổi thơ với ông bà, cha mẹ, bạn bè... Và với khuôn mặt đáng yêu hồn nhiên của shin với những động  tác quái lạ, kì quái đã đưa nhân vật của trẻ thơ trở nên độc đáo và có hồn. Không chỉ là cho trẻ em tuồi như chúng ta đọc để cười, để biết về shin mà bố mẹ cũng nên đọc, nhận xét, suy nghĩ về cách giáo dục con trẻ sao cho hợp lí, đúng đắn. Rất may cho tôi được đón đọc một bản đặc biệt gồm hình ảnh phong phú, sinh động, những nét chữ rõ ràng, cuốn truyện dài tập shin sẽ đưa bạn về tuổi thơ một cách chân thực nhất. Cảm ơn shin- cậu bé bút chì tuyệt vời. 
5
63348
2013-03-18 10:48:26
--------------------------
46265
10021
Thật tuyệt vời khi cầm trong tay quyển truyện này.Một câu chuyện thật dí dỏm và vui tươi. Những trò quái đản của cậu bé Shin đã tạo cho tôi một niềm thích thú và không thể ngừng đọc khi chưa đọc xong. Một cảm giác thật vui sướng làm sao. Những trò đùa quái dị của cậu bé Shin đã được tác giả viết lên thât hồn nhiên. Dù là một đứa trẻ rất tinh nghịch nhưng tình yêu cô giáo, bố mẹ và bạn bè vẫn luôn trong trái tim cậu bé.Và tôi càng thích thú hơn khi được xem những trò hề của cậu.Cảm ơn tác giả đã làm nên một quyển sách như thế này.Dù lúc đầu tôi đọc hơi khó một chút nhưng rồi sẽ lại quen thôi.Một quyển sách không chê vào đâu được.

4
63348
2012-11-14 10:48:44
--------------------------
