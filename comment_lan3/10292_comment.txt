287433
10292
Ảnh bìa tác phẩm là Keanu Reeves, phần nào đã nói lên giá trị của tác phẩm, tôi cho là vậy. 
A walk in the clouds là một phim điện ảnh kinh điển! Bạn không thể tìm được một tác phẩm tương tự ở những năm gần đây. 
Tác phẩm được viết dưới dạng tiểu thuyết chứ không phải kịch bản phim. Một không khí được tạo nên cho người đọc không khí nồng ấm, gần gũi, lôi cuốn.
Lời khuyên của tôi là bạn nên đọc tác phẩm này. 
( Tuy tác phẩm dịch này có nhiều chỗ sai lỗi chính tả, nhưng cũng chả là gì)

4
734606
2015-09-02 18:48:54
--------------------------
96206
10292
Có thể nói kể cả xem phim lẫn đọc sách thì tác phẩm này vẫn để lại những ấn tượng sâu sắc trong tôi. Không chỉ được trải nghiệm khung cảnh miền quê êm ả, thanh bình đầy màu sắc của thung lũng Napa miền Tây Nam nước Mỹ, mà tôi còn như được mở mang tầm nhìn qua những phong tục truyền thống Latinh độc đáo của người dân nơi đây, từ việc trồng nho đến việc ép rượu nho thật sự rất thú vị. Ngoài ra tình yêu của hai nhân vật chính là điều đáng chú ý nhất của tác phẩm này, vượt qua những rào cản, địa vị xã hội, đặc biệt từ người cha với tư tưởng lễ giáo khó tính của Victoria Aragon, anh chàng lính xuất ngũ Paul Sution bán socola đã đến được với người mà mình yêu thương. Một tình yêu quá đỗi ngọt ngào và chân thành đến từ hai trái tim vốn chịu nhiều những tổn thương trong quá khứ. Trên phim diễn viên Keanu Reeves thật sự vô cùng đáng yêu, thật thà. Anh đã thể hiện rất hoàn hảo vai diễn của mình đem đến cho người xem một cái nhìn mới về dòng phim kinh điển, một thể loại phim mặc dù đã cũ nhưng không bao giờ là nhàm chán mà vẫn sống mãi với thời gian.
5
41370
2013-10-06 18:03:06
--------------------------
48032
10292
"Dạo bước trên mây", theo mình, có thể xếp vào hàng những tác phẩm hay nhất trong những năm gần đây. Cuốn sách được chuyển thể từ một kịch bản phim nổi tiếng, và nó đã xuất sắc diễn tả lại những điều mà bộ phim chưa làm được. Câu chuyện quá nhẹ nhàng, sâu lắng như một khúc nhạc, kể về một người bán sô cô la, về câu chuyện của tình yêu thương và trái tim nồng ấm. Người đọc được trải lòng cùng với những cảm xúc rất chân thật và tươi đẹp, được phiêu du trên những miền đất như trong mơ, được đắm say trong sự ngọt ngào của những thanh kẹo sô cô la. Mình rất thích ý nghĩa của truyện này, khi nó đề cao giá trị của tình cảm con người. Thực sự tuyệt vời và đáng đọc!
5
20073
2012-11-28 16:01:17
--------------------------
27071
10292
Nội dung phim này thì mình biết rồi, rất hay, rất tình cảm. Nhưng sao phần chuyển thể từ kịch bản phim này mình thấy không được hay lắm. Không hiểu nguyên nhân vì sao, nhưng mình thấy có lẽ do người dịch. Bản dịch có nhiều chỗ khá sượng, không mạch lạc theo ngữ pháp tiếng Việt, mà chịu ảnh hưởng nhiều của ngữ pháp tiếng Anh. Lại còn những lỗi biên tập hơi vớ vẩn, với 1 kịch bản phim ngắn thế này thì số lỗi đó hơi quá nhiều so với mức có thể chấp nhận được. Mình có cảm giác các sách trong series chuyển thể từ kịch bản phim này không được chăm chút biên tập kỹ càng (hoặc đã không được biên tập) như series Tủ sách cánh cửa mở rộng cũng của NXB Trẻ, nên hơi thất vọng. Giá tiền lại quá mắc.
2
26136
2012-05-15 22:43:22
--------------------------
20266
10292
Tôi thích gọi bộ phim này là "Dạo bước trong mơ", bởi nó nhẹ nhàng, lãng đãng như một giấc mơ đẹp mà ta cố nhớ và buổi sáng. Không sex, không bạo lực, không tục tĩu. Chỉ đơn thuần là sự lãng mạn, ngọt ngào đong đầy trong bộ phim. Là tình yêu thương giữa người với người. Xem mà cứ thấy anh Keanu  thật thà, chất phác quá,đáng yêu quá. Chắc trên đời chỉ có mỗi Paul.Rồi, đâm ra mơ mộng hồi nào không hay B-). Chắc chắn ai đã từng xem phim này không thể không nhớ đến cánh đồng nho đẹp như tranh vẽ và những màn sương khói lãng đãng hư hư thực thực giữa đông. Tình yêu đến nhẹ nhàng mà đằm thắm giữa 2 nhân vật chính. Bỗng thấy tình yêu sao dễ chịu đến vậy, không toan tính, không mệt mỏi. Chỉ là sự đồng cảm và chân thành mà thôi. Một cuốn phim hay và một cuốn sách được chuyển thể chắc sẽ không làm tôi và bạn thất vọng, nhỉ?
4
4058
2012-02-28 16:58:22
--------------------------
19515
10292
Dạo bước trên mây là một bộ phim yêu thích của tôi. Có thể gọi là bộ phim yêu thích nhất trong dòng phim lãng mạn cũng không ngoa. Trái với những thể loại tình cảm Hollywood phần nhiều có chút gì đó suy tính trong cuộc yêu, bộ phim này ngọt ngào bởi sự giản dị từ mặt nội dung và tính chất phác, tấm lòng thuần hậu của mỗi con người trong đó. Những điều ấy lan toả khắp bầu không khí xanh ươm của vườn nho và trong hơi thở mỗi nhân vật. Mỗi nhân vật trong phim đều có cá tính của riêng họ, rất đậm nét mà cũng rất đời thường, nó góp phần tạo ra cái hồn cho tác phẩm. Nội dung không chỉ xoay quanh chuyện tình yêu mà còn cả về tình cảm gia đình và cội nguồn.

Tôi nghĩ nếu ai đã từng say mê mỗi khoảnh khắc trong phim thì không nên bỏ qua một cơ hội để tiếp cận nó qua từng con chữ như thế này. Có lẽ đây là tác phẩm kết thúc có hậu nhất trong series từ phim tới truyện của NXB Trẻ. Và phải nói cái hậu đó rất trọn vẹn.

5
5548
2012-02-18 18:26:36
--------------------------
