550401
10187
Tôi không chuyên về thể loại kinh dị trên văn đàn nhưng dạo gần đây cũng có đọc để "mở mang kiến thức" và thật tiếc là tôi lại chọn "Ngón Tay Quỷ" làm khởi đầu, đúng như cái câu "vạn sự khởi đầu nan" mà.
Không rõ lắm về tác giả Lăng My và cũng không muốn đi sâu tìm hiểu làm gì nhưng cách khai thác nguồn cảm hứng của cô ấy trong thể loại này buồn tẻ đến mức ngay khi nhập truyện mình đã muốn từ bỏ, cái bối cảnh "trụ sở ủy ban xã" ấy thì có bao giờ lôi cuốn được ai?
Tôi hầu như lướt qua đoạn đầu rồi không đọc gì cả, từ từ khép lại, đặt nó xuống.
Không phải lỗi của nhà xuất bản, tôi tin đây là lỗi của tác giả.
Cơ mà soi một tý, bìa sau không biết do nhà xuất bản Văn Học hay Bách Việt Books xử lý mà trông nhạt phết à nha?
1
5094338
2017-03-22 11:16:31
--------------------------
547645
10187
Mới đọc thử vài trang mình cảm thấy truyện khá bình thường không có cảm xúc gì mấy, nhìn thấy nhận xét của các bạn mình quyết định ko mua cuốn này cho lành, để tiền mua pháp y tần minh hay hơn.
2
5116333
2017-03-19 14:31:06
--------------------------
546753
10187
Đây là cuốn tiểu thuyết dở nhất mình từng đọc, cũng cố lật đến trang cuối coi có vớt vát được gì không nhưng không ngờ dở đều từ đầu đến cuối. Mới đầu mua vì thích thể loại kinh dị,nhìn tên và bìa sách khá hấp dẫn nhưng nội dung phải nói là tưởng không chán ai ngờ chán đến không tưởng. Mình thực sự không có một chút cảm giác kinh dị hay trinh thám khi đọc truyện này vì cứ đến đoạn chuẩn bị tưởng như kinh dị mình đã bị những lời văn miêu tả cảnh vật dài lê thê, quá rườm rà sến súa của tác giả làm thôi miên đánh rơi cảm giác luôn rồi. 
Mình cũng có góp ý cho bạn nào đang muốn học hỏi cách làm văn miêu tả thì nên đọc cuốn này. 
Nếu có giải tựa như mâm xôi vàng cho tiểu thuyết thì chắc đây sẽ là ứng cử viên nặng ký.
1
1375717
2017-03-18 15:01:59
--------------------------
501209
10187
Vất vả lắm mới đọc xong quyển này. Bìa sách không liên quan gì tới nội dung sách. Cách viết chi tiết quá thành lê thê, rườm rà, rời rạc. Nội dung chính lại quá đơn giản, dễ đoán. Mặc dù cũng có vài chỗ gây ngạc nhiên. Về phần kinh dị thì không có gì đặc sắc. Đọc cho biết thôi chứ không thích quyển này lắm.
2
84106
2016-12-28 05:07:54
--------------------------
459701
10187
Lần đầu tôi mua sách kinh dị của tác giả này và có cảm giác nó rất rất rất nhạt. Trung Quốc ấn tượng với tôi trong thể loại này là Lôi Mễ, cứ ngỡ cuốn sách sẽ khiến tôi có trải nghiệm khác ai ngờ nó quá nhạt. Sách in cũng tốt, bìa cũng được nhưng nội dung thì khó mà đọc nổi. Ngón tay xuất hiện trong truyện cũng chẳng có gì đặc biệt, Mặc dù vậy thì điểm tốt duy nhất của cuốn sách là giấy đọc không bị lóa, đau mắt. Tôi cũng không ưng ý với cuốn sách cho lắm.
2
419803
2016-06-26 13:02:42
--------------------------
323499
10187
Đầu tiên phải nói là truyện rất dài nên mình nghĩ chắc phải có nhiều chi tiết nhiều yếu tố kinh dị ghê lắm nhưng đọc rồi mới thấy chả có cái quái gì cả.Từ đầu truyện tới cuối giọng văn cứ đều đều,mình đọc nhiều truyện trinh thám nên đọc được gần nửa là đoán được hung thủ là ai lẫn cách gây án rồi thế mà tác giả cứ viết lòng vòng dài lê thê.Còn về kinh dị thì nói thật mình chả hiểu mấy nhân vật trong truyện này sợ cái gì nữa,lâu lâu có bóng ma rồi ngón tay này nọ nhưng đọc không hề cảm thấy hấp dẫn gì cả.Rốt cuộc không hiểu cuốn này nên xếp vào thể loại gì,có lẽ là do kì vọng quá nhiều nên đọc xong thấy thất vọng quá.
3
526322
2015-10-18 22:51:47
--------------------------
306801
10187
Lúc đọc qua về tác phẩm ở hội chợ có vẻ thấy hay hay, tên truyện với bìa truyện cũng đúng loại kinh dị nhưng khi mua về thì thật thất vọng. Thề là mình muốn đốt quyển này luôn đi ý, nội dung chả ra sao. Nhạt toẹt, thỉnh thoảng giật tít lên có bóng ma này nọ nhưng không hề làm cho người đọc có cảm giác hồi hộp, sợ hãi. Tác giả Lăng My mình chưa biết đến nhưng nếu bà ý còn viết truyện như thế này thì nên thôi viết sách đi là vừa. Truyện tệ nhất mà mình từng xem
2
619746
2015-09-17 20:05:45
--------------------------
294084
10187
Đây là cuốn sách mà khi mình đọc mình thấy buồn nhất buồn vì đã kỳ vọng quá nhiều ,khi nhìn bìa sách thì thấy khá rùng rợn và khi đọc cái tên cũng rất hấp dẫn mà sao truyện lại không được như những gì mình nghĩ nhỉ!Do kỳ vọng quá mà,những yếu tố không có gì là xuất sắc,truyện kinh dị mà không kinh dị nỗi,đọc thấy bình thường,chẳng đáng sợ gì cả!Các bạn nào cảm thấy thú vị thì cứ mua nhé đây chỉ là ý kiến của riêng mình thôi!!Mong rằng có bạn sẽ cảm thấy hay!!
2
413637
2015-09-08 23:12:21
--------------------------
195304
10187
Đây là một trong những câu chuyện nhạt nhất mà mình từng đọc. Yếu tố kinh dị quá thiếu muối, cứ nhàm chán lặp đi lặp lại mỗi một kiểu hù dọa mà mình tự hỏi sao nhân vật bị dọa lại có thể sợ được nhỉ?! Còn yếu tố trinh thám thì ôi thôi, đến cả mình còn đoán được hung thủ lẫn thủ đoạn gây án khi chưa đọc tới nửa cuốn truyện, thế mà nhân vật chính trầy trật mãi vẫn chả hiểu gì! Nói chung đây là một quyển sách khá nhẹ đô và theo mình chỉ thích hợp cho những ai mới vừa bắt đầu tiếp xúc với thể loại trinh thám kinh dị.
2
494249
2015-05-13 13:13:29
--------------------------
98823
10187
Rất tiếc phải nói rằng đâu là cuốn sách thuộc thể loại trinh thám- kinh dị làm tôi tốn thời gian đọc nhất( vì chán nên cứ phải dừng lại nhiều lần). Đối với tôi mà nói câu chuyện của Ngón tay quỷ vô cùng thiếu hấp dẫn và dễ đoán. Tôi đã có thể đoán ra được thủ phạm là Trường Hà ngay từ những chương đầu tiên. Có lẽ do đã quá quen thuôc với nhiều thể loại kinh dị nên tôi khá nhạy cảm khi đọc cuốn sách này. Tuy nhiên điểm cộng là cách kể chuyện của tác giả có tiết tấu vừa phải và có nút thắt mở, cộng thêm với chi tiết sáng tạo nhân vật hỗ trợ cho Trường Hà để cứu vớt chút ít được cho việc tạo nên sự mơ hồ của tác phẩm!
2
130305
2013-11-07 14:52:27
--------------------------
93040
10187
Thú thật đây là cuốn truyện kinh dị "buồn ngủ" nhất từ trước đến nay mình từng đọc. Không biết tác giả có định nghĩa thế nào về truyện kinh dị chứ cuốn này nếu mình được xếp chắc chắn mình không xếp vào hàng truyện kinh dị và mình cũng không biết xếp nó vào hạng mục gì nữa.

Truyện chỉ có một vài cảnh tạm gọi là có chất "kinh dị" khi con ma xuất hiện, giơ lên một ngón đầy ẩn ý, rồi một nhân vật sau đó chết. Nhân vật chính thì đau khổ vì bạn chết, đau khổ vì người tình cũ, vì đột nhiên có nhiều cô gái theo đuổi, vì làm chủ tịch xã, phải lo cáng đáng nhiều việc ... kéo dài gần như 70% cuốn truyện. 

Phải nói mình thật sự cố gắng và kiên nhẫn để đọc hết cuốn truyện hầu tìm lời giải, nhưng cốt truyện lê thê, thiếu cao trào lẫn cảm xúc đôi khi đánh gục mình ngay trên giường.
2
34650
2013-08-31 12:47:21
--------------------------
63282
10187
Đọc quyển sách này vào buổi tối, không biết có phải do vậy mà mình cảm thấy quyển sách này rất rùng rợn không. Cuốn sách này không thiếu những chi tiết kì quái, ghê rợn, kích thích trí tò mò của người đọc. Càng đọc, từng nút thắt một được mở ra, nhưng những điều kì dị bí ẩn kia thì ngày càng xuất hiện nhiều hơn, càng làm người đọc thấy hứng thú hơn. Tuy cốt truyện không mới nhưng tác giả rất biết cách khai thác trí tò mò của người đọc. Một quyển truyện kinh dị rất đáng để xem.
4
82088
2013-03-14 17:36:45
--------------------------
53389
10187
Cuốn tiểu thuyết này có một sức lôi cuốn kỳ lạ đối với mình, dù cốt truyện có thể không quá độc đáo và mới mẻ, nhưng được kể một cách tài tình và sắc sảo. Cuốn sách dẫn dắt người đọc vào một thế giới u ám, bí ẩn và có phần ghê rợn. Càng đọc, cảm xúc càng được dồn nén hơn nữa trong mỗi trang sách, đồng thời sự bí ẩn và kỳ lạ càng được miêu tả rõ nét hơn, khiến người đọc không khỏi cảm thấy ám ảnh. Nhưng chính những điều này góp phần tạo nên một tác phẩm đặc biệt, độc đáo và mới lạ trong cách dẫn dắt truyện, trong hình tượng những nhân vật. Và ẩn sau câu chuyện mang màu sắc ma quái này là những ý nghĩa nhân sinh đáng phải suy ngẫm.
4
20073
2013-01-01 21:35:44
--------------------------
