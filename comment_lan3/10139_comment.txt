355952
10139
Đọc được mấy trang đầu, cứ nghĩ hình như truyện này đọc rồi, hóa ra không phải đã từng đọc, mà vì cốt truyện khá giống những truyện khác, không có gì đặc sắc.
Vẫn là lọ lem gặp hoàng tử lạnh lùng, vẫn là cưới vì lý do nào đó rồi nảy sinh tình cảm, cũng vẫn là phía nữ bắt đầu tình yêu trước, hi sinh vì tình yêu trước nhưng không được đón nhận, vì thế mà từ bỏ, cũng từ đó phía nam mới nhận ra tình yêu, rồi vẫn là hối hận, là đau khổ, là giày vò nhau, rồi mới đi đến kết cục có hậu.
Dù là mô tuýp cũ nhưng đã gắng đi đến trang cuối cuốn truyện. Không phải vì không biết kết cục nên mới đọc, mà vì muốn lại sống trong giấc mơ của lọ lem, muốn đi vào cảm xúc của nhân vật mà hoang tưởng thêm 1 lần.
Thế nên ai có ý định hoang đường như mình thì đây cũng không phải là quyết định tồi
1
116972
2015-12-21 00:10:17
--------------------------
347834
10139
Có ấn tượng không mấy tốt đẹp với Mộc Phạn từ hồi đọc "Ánh sao ban ngày", đọc "Trần Thế" xong thì quyết rời bỏ, cạch mặt tác giả này luôn. Một tác phẩm mà mình không đủ sức đọc hết vì quá nhạt, quá nhàm và nhảm nhí. Truyện dài dòng, lan man và rất nhiều chi tiết thừa thãi. Mô tip cưới trước yêu sau đã quá phổ biến nhưng tác giả lại không biết tạo cho truyện của mình sự khác biệt so với các truyện khác, tâm trạng nhân vật thì lằng nhằng và lắm lúc khá là … dở hơi, càng đọc càng thấy bực.
1
630600
2015-12-05 01:02:25
--------------------------
333185
10139
Đã từng rất thích thú với cốt truyện, văn án. Nhưng đọc truyện thì có một số chỗ không cần thiết, nó không quá nổi bật, thu hut và làm mình thật thất vọng. Một cái kết có hậu nhưng cũng không chắc là sẽ có hậu bởi nó là một kết thúc mở. Xây dựng nhân vật, nội tâm nhân vật không lột tả được hết cho người đọc đôi khi mình thấy rất hụt hẫng. Nữ chính không thích cho lắm bởi có nhiều suy nghĩ, và cả tính cách của chị ấy. Chuyện tình cảm đôi khi khó nói nhưng đôi khi rất dễ dàng nhưng trong chuyện đã làm vấn đề căng thẳng, tự làm khó nhau, khổ nhau. Gây cảm giác rất nhàm chán.
1
599139
2015-11-07 16:05:37
--------------------------
225604
10139
Theo cái tựa là TRẦN THẾ thì cuốn truyện cứ làm tôi nghĩ là một một cuốn sách rất hay và rất tự tin về điều đó vì thấy câu truyện của Tiki (trích đoạn nhỏ) đưa ra rất thú vị, nhưng khi hàng vừa tới là lúc tôi cũng muốn hàng đi chỗ khác cho rồi, chẳng có sức hút gì cả, 44 chương mà đọc tới, xem lui thì thấy nội dung chẳng bằng một cuốn truyện ngắn. Tôi nghĩ tác giả có một ý tưởng rất tuyệt vời và tạo ra một cách tệ hại chẳng có gì hay, vui và chẳng một tí tình tiết hấp dẫn. Càng đọc thì đầu của tôi càng rối vì chẳng biết đây là gì nữa! Tôi nghĩ đây là sách dịch vì nội dung có nhiều tên của nước ngoài và cũng chẳng hay như văn nước Việt, người dịch cũng có khi là không biết gì về văn nên cũng chẳng làm cho nó sống động hơn, nếu có lần sau giới thiệu thì nhờ Tiki.vn giới thiệu THẬT LÒNG, chứ đừng có giới thiệu "bừa". 
1
468545
2015-07-10 17:49:29
--------------------------
176599
10139
Mình vốn thích đề tài cưới trước yêu sau, cũng như cuộc sống hôn nhân, vì các truyện này thường ít nhiều mang tính thực tế. Hơn nữa, mình rất thích Ánh sao ban ngày của Mộc Phạn. Nhưng truyện này so ra không bằng Ánh sao ban ngày. Nữ chính thì ích kỷ, nói thật nhiều lúc hơi khó ưa, đọc thấy mà thương cho anh nam chính, không hiểu anh làm sao mà yêu chị đến thế cho được. Nhiều lúc tác giả lại để các nhân vật thi nhau tự ngược, trầm trọng hóa vấn đề, khiến truyện bị kéo dài ra không cần thiết, đọc rất lan man, dài dòng. Giá như cắt bớt những đoạn thừa thãi đó đi thì nội dung truyện cũng không đến nỗi nào.
2
57459
2015-04-01 15:05:09
--------------------------
164265
10139
Ở “Ánh sao ban ngày”, tôi đã không thích, nhưng đến “Trần thế”, Mộc Phạn đã khiến tôi thất vọng không còn gì để cứu vớt nữa. Vẫn là đề tài cưới trước yêu sau, vẫn giọng văn dài dòng, lê thê, vẫn các tình tiết không cần thiết được đưa vào chẳng có tác dụng gì. Nữ chính vô cùng ích kỷ, chỉ biết lo cho mình mà không hề nghĩ đến cảm nhận của người khác, thế mà không hiểu sao Lâm Tự lại có thể yêu cô ấy được. Tình cảm của các nhân vật trong truyện nhập nhằng, rối rắm, vốn chẳng có gì phức tạp nhưng họ lại cố tình làm khó lên rồi dằn vặt nhau, thật mệt mỏi.
1
449880
2015-03-07 11:00:10
--------------------------
128217
10139
Một sự thật phũ phàng đấy là nội dung truyện hoàn toàn đối lập với cái bìa sách dễ thương. Đây là một câu chuyện lằng nhằng, nội tâm nhân vật thì mập mờ và nhiều mâu thuẫn, cách hành văn vừa lê thê vừa lủng củng, lại lắm tình tiết thừa thãi và cố tình phức tạp hóa vấn đề. Rõ ràng có những tình huống hết sức đơn giản, có thể giải quyết êm đẹp, vậy mà không hiểu sao các nhân vật trong truyện cứ thích làm quá lên như thế. Mối tình Lâm Tự - Lạc Trần cứ quay lòng vòng khiến tôi quá mệt mỏi và chán nản.
Đọc truyện này, tôi cực kỳ ghét nữ chính, chưa có nữ chính nào khiến tôi bực mình như vậy. Không phải chín chắn, cũng chẳng thấy đáng yêu ở đâu, tất cả những gì tôi thấy ở cô ấy là sự vô duyên và ích kỷ đến đáng sợ. Ừ thì cô ấy có tuổi thơ không hạnh phúc, nhưng có cần trở nên ích kỷ chỉ muốn nhận, không muốn cho như thế không?
1
393748
2014-09-30 01:13:34
--------------------------
123129
10139
Khi đọc tóm tắt truyện tôi đã thấy khá tò mò và cảm thấy bị thu hút nên đã quyết định mua nó về và hy bọng nó sẽ không phụ lòng mong đợi của tôi . Nhưng rốt cuộc tôi lại bị thất vọng tràn trề . Khoan bàn đến cốt truyện lan man dài dòng không có nhiều chi tiết thu hút người đọc cộng với cách kể của Mộc Phạm thật khiến tôi đau đầu . Những chỗ có nút thắt cần được giải tỏa thì lại càng bị thêm mắm thêm muối cho nó rối rắm hơn . Nhìn chung không thể chấp nhận được . Lần đầu tiên tôi thấy một nhân vật nữ chính vô duyên đến như vậy tôi thấy nên để cô ta làm nữ thứ thì tốt hơn . Chưa kể đến việc xoay hai người đàn ông như chong chóng trêu đùa tình cảm của họ lúc nào cũng tỏ ra vẻ yếu đuối ích kỉ đồi được đối xử tốt chứ không bao giờ cho đi cái gì . Thật sự không hiểu Mộc Phạm nghĩ sao mà lại xây dựng nhân vật nữ chính kiểu này
2
337423
2014-08-30 10:25:27
--------------------------
114752
10139
Ấn tượng với Mộc Phạn từ "Ánh sao ban ngày" nên tìm đọc "Trần Thế"
Nội dung trong truyện của Mộc Phạn rất đời thường, nhưng tâm lí nhân vật được khắc hạo rất sâu sắc.
Cả Lạc Trần hay Lâm Tự, nếu ai không kiên nhẫn, khi đọc vào vài ba chương đầu, sẽ cảm thấy phức tạp và vô cùng khó hiểu, dẫn đến nhàm chán và bỏ dở.
Mà cũng công nhận mình trâu bò mà, càng phức tạp thì mình càng dấn thân.
Hậu quả là chẳng hiểu sao, lại có cảm giác như bản thân như bị phân ra, lúc là Lạc Trần, lúc là Lâm Tự. Đau nhức nhối.
Nội dung hết sức bình thường. Chẳng sóng gió bão táp gì. Chỉ có hai nhân vật chính tự ngược nhau. Thế thôi!
Nhưng mà, quả thực, cuộc sống thực tại là như vậy đấy. Cuộc sống xô bồ khiến con người ta yêu mà không dám yêu, không dám thừa nhận, để rồi tự làm khổ nhau.
Truyện nhiều lúc khiến mình ức chế. Vô cùng ức chế!
Nhưng rồi cũng phải suy nghĩ lại, con người ta, đúng thật là như vậy đấy! Vô cùng phức tạp và mâu thuẫn.
Cái gọi là "tình yêu" thì trừu tượng lắm.
Nhưng "tình thân" thì rất gần gũi và thực tế. Nó giống như không khí vậy, rất rất cần thiết.

Truyện rất thích hợp cho những người nhạy cảm, sâu sắc, và thực tế! :)
5
53146
2014-06-17 22:20:00
--------------------------
80505
10139
“ Trần thế “ lúc đầu đọc phần giới thiệu mình cũng thấy khá thích nhưng đọc rồi mới biêt hình như mình đã hy vọng quá nhiều và bây giờ là thất vọng không gì tả được. Câu chuyện lan man dài dòng và rất nhiều chi tiết không cần thiết. Nữ chính thì không thể nào thích được một con người quá ích kỷ chỉ muốn nhận chứ không muốn cho đi ấy thế mà không hiểu sao Lâm Tự và Sở Dương Khanh lại có thể thích được. Tâm lý của nhân vật trong chuyện cứ quay đi quay lại mà không có tiến triển gì cả rất nhiều chỗ tác giả dường như đi vào bế tắc để cho nhân vật tự trôi vậy. Tóm lại là mình thấy chuyện này quá dài dòng, cách phát triển tâm lý  và khắc họa hình ảnh nhân vật rất thiếu logic. 
1
97873
2013-06-13 07:50:17
--------------------------
77586
10139
Truyện có cốt truyện khá hấp dẫn, khiến mình mua về ngay. Tuy nhiên, mình không thích truyện này lắm. Thứ nhất là do nhân vật chính, tính cách cô nàng sao sao ấy, hơi bị vô duyên, ích kỷ khi chỉ muốn nhận được tình cảm từ một phía, hơn nữa cô ta không xác định được tình cảm của mình.  Cách kể của Mộc Phạn cũng khiến người đọc hơi bị nhức đầu và chán nản, bởi nó lang mang dài dòng, tình tiết quá phức tạp, nội tâm nhân vật cũng mập mờ khó đoán, tình cảm nhân vật, các diễn biến thì dài lê thê gây cảm giác chán nản cho người đọc. Tuy nhiên mình đánh giá cao cái bìa truyện, rất nghệ thuật và cốt truyện cũng rất hay.
2
57869
2013-05-28 19:10:19
--------------------------
72244
10139
♥ Sau khi đọc đoạn giới thiệu mình cũng thấy khá thích. Cốt truyện không quá phức tạp, ý định ban đầu là đọc để giải trí, nhưng mà được nử cuốn truyện mình nhận ra là... không thể giải trí được @@.
♥ Thứ nhất là tính cách nhân vật. Nữ chính (mình quên tên luôn mặc dù vừa đoc hôm qua) quá ích kỷ. Khi vừa ký hợp đồng, cô gay gắt với chuyện em trai mình đến ở chung, điều này có thể cho là hợp lý, nhưng rồi sau đó, bao nhiêu lần nam chính nhượng bộ, cô vẫn thích thì ở lại, không thích thì đòi đi. Từ đầu cho đến lúc cô nhận ra tình cảm và quyết định từ bỏ vẫn rất hay, nhưng từ đoạn đó trở về sau không hiểu sao tình cảm lại trở nên mập mờ và cứ thế kéo đến 3/5 truyện còn lại.
♥ Thứ hai là tình tiết. Không có gì nghiệm trọng, cũng không quá căng thẳng, thế nhưng cách xử trí của từng nhân vật lại như muốn làm phức tạp hoá vấn đề lên, rồi từ đó kéo dài lê thê. Đọc được tới lúc nam chính nhượng bộ nữ chính tới lần thứ 2 hay 3 gì đó, mình nhìn lại bề dày và ngạc nhiên nhận ra chỉ mới được nửa cuốn sách, vậy thì nửa sau tác giả viết gì?
♥ Tua nhanh đoạn sau thì truyện tiếp tục lòng vòng và sau đó xoay quanh nv nữ  thứ chính. Đọc sơ đoạn kết rồi cuối cùng thì nữ thứ chính này theo người em trai của nam chính, cái kết khá là... ngớ ngẩn
♥ Truyện này mình cảm giác nếu chỉ dày một nửa thì quả thật rất tốt, miêu tả nội tâm nhân vật cũng như tình tiết hấp dẫn. Nửa sau thực sự không cần thiết và làm cho cuốn truyện trở nên mờ nhạt :)
2
77115
2013-05-01 11:11:11
--------------------------
58268
10139
Mình thật sự thất vọng vì sự lựa chọn khi mua quyển truyện này.Mình đọc lời giới thiệu truyện thì khá hy vọng về tính cách các nhân vật trong truyện vì thực sự thể loại truyện này mình đọc khá nhiều, nhưng khi đọc truyện này mình chán hoàn toàn mình chưa thấy nhân vật nữ nào lại có tính cách một màu và ích kỷ như trong truyện.Mình thấy nhân vật nữ chỉ muốn nhận lại chứ không muốn cho đi.Biết là nhân vật chính xuất thân từ cô nhi viện luôn khát khao tình yêu thương và luôn thận trọng khi trao tình yêu cho một ai đó nhưng cuộc sống nếu tình cảm mà luôn luôn phải dè chừng và lo sợ khi mình trao đi mà nhận lại không đủ thì đừng mong nhận được tình cảm lại nhất là một người không biết tình cảm là như thế nào như nhân vật  Lâm Tự.Mình thấy thương những người thân của hai nhân vật chính khi mà phải xoay trong vòng xoáy tình cảm của Lạc Trần và Lâm Tự.Tình cảm của hai người dai dẳng, dày vò lẫn nhau nhưng cùng chẳng phải kinh thiên động địa gì cho cam.Đọc truyện này xong mình bức xúc vô cùng.
1
48283
2013-02-03 18:37:37
--------------------------
51619
10139
Tác phẩm được viết bởi một nhà văn còn trẻ tuổi, nhưng đã thể hiện rất rõ sự trải nghiệm, trưởng thành và chín chắn trong suy nghĩ, đem đến nhiều ý nghĩa sâu sắc cho câu chuyện này. Một tiêu đề thật nhẹ nhàng và giản đơn, một câu chuyện tình yêu đẹp, lãng mạn song vẫn chân thực và gần gũi, với cách viết độc đáo và nhiều sáng tạo, đặc biệt xuất sắc trong những đoạn miêu tả tâm lý nhân vật. Tất cả những điều này đem đến sức hút diệu kỳ cho tác phẩm, làm người đọc khó có thể lãng quên những trang sách đẹp và nhiều màu sắc của nó. Khi gấp sách lại, hẳn mỗi người sẽ tìm thấy cho mình một bài học, một thông điệp riêng về tình yêu, cuộc sống, và sẽ thấy mình như được trưởng thành hơn rất nhiều.
4
20073
2012-12-21 23:05:49
--------------------------
37048
10139
Văn phong của Mộc Phạn trong "Trần thế" nhẹ nhàng tựa như một gơn nước giữa mặt hồ lặng sóng. Ngòi bút của cô thiên về mặt miêu tả tâm lý nhân vật, do đó, truyện của cô hệt như 1 hơi thở của cuộc sống với nhiều triết lý sâu sắc được lồng ghép gọn gàng, tỉ mỉ. Đọc "Trần thế" không phải để đi tìm một cốt truyện mới lạ, những tình tiết gây cấn, vì vậy có nhiều người cho là nhạt, lê thê và dài dòng nhưng cái hay của câu truyện là những giằng xé nội tâm, mâu thuẫn trong cảm xúc, ban đầu tưởng chừng rất khó giải thích mà chỉ sau khi chiêm nghiệm mới có thể hiểu được điều cốt lõi. Phải chăng một tâm hồn đồng điệu với nhân vật mới có thể cảm sâu đến vậy.  Vẫn với mô típ quen thuộc nhưng "Trần thế" hoàn toàn thoát khỏi hình tượng cô lọ lem ngốc nghếch và chàng hoàng tử hoàn hảo, họ đều có những khiếm khuyết với sự khác biệt giai cấp quá lớn nhưng lại tương đồng về mặt tính cách: quá thực tế, quá cố chấp. Hai người không phải điểm cộng trừ thiếu sót cho nhau mà lại là tấm gương phản chiếu của nhau do đó mâu thuẫn ngày càng gia tăng, tâm lí càng bị dằn vặt, mạch cảm xúc vì thế càng đa dạng biến hóa khôn lường. Đọc "Trần thế" ta có thể hiểu thế nào là 1 tình yêu chín chắn và thực tế. Có thể đó là sự an bài của số phận nhưng đó cũng có thể là câu chuyện xảy ra ngay bên cạnh ta. "Trần thế" có những tình huống chưa được miêu tả tỉ mỉ, trau chuốt nhưng tâm lí nhân vật lại vô cùng dào dạt. Đây là một tác phẩm xứng đáng nếu bạn muốn cảm thật sâu trong tình cảm con người 
4
16847
2012-08-18 10:23:09
--------------------------
33495
10139
Không ngoài dự đoán của tôi, văn phong của Mộc Phạn trong "Trần thế" không khác gì so với "Ánh sao ban ngày". Qúa dài dòng, lê thê, lan man ở những tình tiết không cần thiết, những nhân vật không cần thiết. Không biết tác giả đưa vào với mục đích gì, nhưng bản thân tôi chỉ có thể nói chán vô cùng. Vẫn mô tuýp cưới trước yêu sau. Có lẽ tác giả khá hứng thú với đề tài này. Nhưng truyện lại khá nhập nhằng. Tâm trạng nhân vật quá mâu thuẩn, quá phức tạp. Lạc Trần yêu Lâm Tự, nhưng không được hồi đáp, tình cảm dần nguội lạnh. Đến lúc nào đó, Lâm Tư cũng bắt đầu để ý đến Lạc Trần, cô tuy vẫn còn yêu anh, nhưng vì bị tổn thương nên luôn tìm cách trốn tránh. Cuộc rượt đuổi dài hơi giữa yêu không yêu cứ kéo dài lê lết. Khiến tôi cảm thấy sao mà mệt mỏi vô cùng.
1
24968
2012-07-13 22:48:25
--------------------------
29601
10139
môtip nữ 9 của Trần thế cũng kiên cường giống như ở Ánh sao ban ngày.Quá trình diễn tả nội tâm nhân vật được tác giả miêu tả rất được.Vẫn giống như Ánh sao ban ngày,câu chuyện là quá trình 2 người đấu tranh nội tâm,tìm hiểu nhau rồi dẫn tới tin tưởng và yêu thương nhau.Nói chung nếu bạn thích Ánh sao ban ngày thì  đây là câu chuyện đáng đọc.
4
14966
2012-06-07 10:49:25
--------------------------
