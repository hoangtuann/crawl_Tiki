400772
41
Một tiểu thuyết lãng mạn của Nicholas Sparks. Truyện có nhiều tình tiết cao trào, bất ngờ, cũng có những lúc nhịp chậm lại. Tâm trạng của từng nhân vật được thể hiện rõ qua từng chương. Xúc động nhất vẫn là tình cảm giữa Ronnie và cha cô Steve. Ronnie ngang ngược nổi loạn ban đầu, qua từng trang truyện dần thay đổi, cô trưởng thành hơn, yêu mến cha mình hơn, để rồi tuột mất cái hạnh phúc khi mới tìm lại được. Mình cũng không thích tiểu thuyết lãng mạn cho lắm nhưng sau khi đọc The Last Song thì đây không phải lựa chọn tồi.
5
593439
2016-03-19 21:01:09
--------------------------
319682
41
Không thể phủ nhận một điều là sách của Nicholas rất hay, "The last song" cũng không là ngoại lệ. Mình rất thích cách tác giả dẫn truyện trong tác phẩm này, nhẹ nhàng, lôi cuốn, dần dần dâng lên cao trào. Tác giả đã đề cao tình cảm cha con, lúc đọc tới đoạn mà bố Ronnie mất mình đã rất xúc động. Bên cạnh đó thì tác giả mô tả tình yêu của Will và Ronnie rất thật và gần gũi với người đọc. Lúc mình đọc xong có cảm giác rất hứng khởi và yêu đời hơn, trân trọng gia đình hơn. Cuốn sách này rất hay và có ý nghĩa.

5
485074
2015-10-09 12:56:51
--------------------------
270541
41
Quá hay, truyện có nội dung rõ ràng, câu chuyện tình yêu giữa ronnie và ưill chỉ là nền để làm nổi bật tình cảm giữa Ronnie và cha, mình đặc biệt ấn tượng với nhân vật cha của Ronnie. Thật sự khi đọc truyện xong mình hơi thất vọng về phim vì có cảm giác nó chủ yếu nói về mối tình của Ronnie và Will, không đề cập nhiều tới nhân vật cha (mà mình nghĩ đây là nhân vật quan trọng nhất truyện), kể cả bài hát cuối phim dù hay nhưng mà giống dành cho tình yêu hơn là tình cảm cha con. 
Đây là câu chuyện đầu tiên của Sparks mà mình đọc và cũng là câu chuyện mình thích nhất.
5
324542
2015-08-18 12:52:28
--------------------------
15785
41
Tôi đọc quyển này với lý do chính là vì nó của Nicholas. Lúc đầu tôi có phần ko thích vì kiểu teen teen nổi loạn của  Ronnie, thêm với cái cốt truyện có vẻ ko hấp dẫn và mới lạ như mấy quyển khác. Nhưng tôi vẫn kiên trì đọc, và càng đọc thì càng thấy quyết định của mình là đúng đắn. Ronnie ngang ngược chẳng qua chỉ là vì cô bé còn trẻ quá, nhưng qua từng trang truyện, bạn sẽ thấy cô bé trưởng thành lên từng ngày, ngày càng bộc lộ tính cách dịu dàng, quan tâm đến người khác, nhân hậu và nhạy cảm. Câu chuyện cảm động nhất ở mối tình cha con giữa Ronnie va ba mình, và những ngày cuối đời của ông, lồng trong đó là cách suy nghĩ của Ronnie với Will và cách mà cô đối mặt với khó khăn trong chuyện tình của mình. Kết thúc ngọt ngào có thể đoán trước nhưng vẫn ko giảm sự chân thực và tinh tế mà bạn có thể cảm nhận được. Nói chung đây là một cuốn sách nhẹ nhàng và có ý nghĩa mà bạn nên đọc, nhất là nếu bạn là 1 cô gái tuổi teen hoặc có 1 cô con gái tuổi này.  
4
14506
2011-12-11 00:56:48
--------------------------
11164
41
Đây là một tác phẩm nổi bật nói về tình yêu thương . Một cô bé teenager với tâm hồn nhạy cảm, hiền lành nhưng khép kín. Cô bé về thăm người cha của mình và ở đây đã học rất nhiều điều về cách xử sự . Với lối văn nhẹ nhàng và sắc xảo, tác giả đã khắc họa sắc nét sự chuyển biến một tâm hồn ngây thơ từ lối sống khép kín đến cởi mở hơn . Một tình cảm cha con thiêng liêng và một tình yêu trong sáng nhẹ nhàng dần được khắc họa 1 cách tinh tế , sâu lắng ... sự ngây thơ nổi loạn  dần trở nên trưởng thành hơn của cô bé qua cách dạy của người cha thật sự rất cảm động . Tuy đây là một câu truyện có kết thúc khá buồn , nhưng lại có rất nhiều ý nghĩa .
4
11384
2011-09-11 19:03:46
--------------------------
