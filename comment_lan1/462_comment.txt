573369
462
Trình bày bìa - ok, giấy màu vàng nâu - ok, sách nhẹ - ok. Chỉ có điều là - TẠI SAO? Tại sao lại phải cho cái cỡ chữ nhỏ đến mức đau mắt thế? 
Nhìn hình mình post chắc mọi người sẽ thấy trình bày bìa, in ấn đều đẹp chỉ có cỡ chữ là một điều không thể chấp nhận được! 
Cuốn này lại không hề mỏng, văn thì toàn những đoạn dài lê thê... mình không biết liệu rồi có đọc được hết không hay bỏ dở vì cỡ chữ quá bé, tức mắt. 
Bộ Collins Classics này mình có đặt mua vài cuốn, The Three Musketeers dày gấp 3 cuốn này nhưng vẫn đáng được đánh giá 4* vì cỡ chữ không hề nhỏ như vậy. 
Nếu hỏi liệu mình có recommend cuốn này cho người khác không thì câu trả lời của mình sẽ là: KHÔNG, không bao giờ.
1
1295088
2017-04-14 17:39:51
--------------------------
