446507
40
Quyển sách này là một câu chuyện dài với mình. Sau khi đọc vài trích đoạn trên mạng mình rất thích nên lên Tiki tìm mua ngay nhưng lúc mình có nhu cầu mua thì Tiki đang tạm thời hết hàng, phải đợi một thời gian nên mình mang về quyển tiếng Việt trước, mình đã rất thích phiên bản tiếng Việt đó.
Giờ thì đang cầm Dear John trên tay, vẫn chưa đọc hết nhưng cảm giác rất chi là hân hoan mặc dù đang đọc về một chuyện tình buồn.
Nicholas Sparks luôn có cách riêng để "kể" về các mối tình và theo mình nghĩ sách của ông thậm chí còn hay hơn nhiều so với những bộ phim cùng tên được chuyển thể.
Mình rất hài lòng về quyển sách này.
5
1046224
2016-06-12 12:45:18
--------------------------
242585
40
Đã từng coi bộ phim cùng tên chuyển thể từ tác phẩm này và cảm thấy không hay lắm nhưng vẫn quyết tâm mua cuốn này để đọc. Mới đầu cứ nghĩ nó sẽ là câu chuyện tình của hai con người trẻ tuổi nhưng khi kết thúc truyện tôi mới thấy nổi bật hơn cả và cảm động hơn cả là tình cha con giữa John và bố của anh ấy. Nhân vật tôi thất vọng nhất trong chuyện là nữ chính vì tôi cảm thấy cô ta thật ích kỷ nhưng có lẽ vì vậy mà tình cha con là cái mà tôi ấn tượng nhất về cuốn sách này. Tôi không hối hận vì mua quyển sách này.

5
77665
2015-07-26 20:00:55
--------------------------
105033
40
I have read Dear John in VI version and I really loved it. John and Savanah's love story really brought me to another world where there were just two peope being drown into love. Nicholas Sparks' novels are so awesome and I just want to take all his books home so badly. The most beautiful thing in this story is that John decides to leave the town although he still loves Savanah. I do not quite understand why he does that but I know it is because he loves her and he wants her to be happy with Tim.. This couple inspire me a lot.
5
275960
2014-01-22 14:30:46
--------------------------
57387
40
yes, it is a sad story.
in the very first pages, i can never imagine that the end would be like that. so tragic for John :(

however, it can be really boring if the story didn't end like that, because i see that life is not always perfect. you may fall in love with someone today, but tomorrow, there's something might let you down, and you change your mind. and sometimes, who are in love cannot get to marry each other, and some who are not in love will live with each other happily. it's a tragic truth of life.

poor john, i love the way he shows his love for Savannah, and the way he treats her so kindly, but Savannah's action is unacceptable. I hate her >"<
5
44904
2013-01-29 16:40:51
--------------------------
42819
40
Nicholas Sparks has many times proven his admiring talent in creating truly authentic love letters in all of his novels. Dear John are one of those and for me, may be the best collection of love letters ever, carrying vulnerable love that John and Samantha give each other. It is a beautiful love, a real one in which distance cannot stop them from their thoughts and love for the other. 
I have this book in both VN and EN versions, because I adore its language. They shake up my spirit of romance in the most genuine way that can only be found in Nicholas’ books. I have these two books on shelf and will surely read them times and again.

5
14506
2012-10-14 18:10:16
--------------------------
33614
40
Mới đọc xong Dear John vài ngày trước, và nó đã thực sự để lại trong tôi một nốt lặng. Không biết nữa, không biết có phải ám ảnh không, nhưng đây thực sự là một quyển truyện cuốn hút tôi qua từng từ một - lần đầu tiên. 
Có lẽ vì tôi còn nhỏ, nên tôi phần nhiều cảm động vì tình cảm của John với bố. Quá khứ đã đi qua và không thể lấy lại, và những nỗi đau vẫn còn đó mãi. Tiếc nuối và đau đớn quả thực là cảm giác khi tôi đọc đến đoạn người bố mất. Nhưng John đã vượt qua, như một người đàn ông, một người lính mạnh mẽ. 
Và còn lại, chính là tình yêu của John và Savanna. Một tình yêu đẹp, có thể là nhanh nhưng chắc chắn và sâu đậm. Hai con người cuốn hút lẫn nhau, cùng trưởng thành qua những lá thư. Nhưng cuộc sống là vậy, không gì có thể đoán trước, vậy nên có ai ngờ kết cục của một mối tình lại đau đớn đến vậy. Thế nhưng, cuối cùng thì John cũng chọn được cách phản ứng, chọn được cách sống tốt nhất cho tất cả mọi người chứ không chỉ cho riêng mình. 
Một phần nào đó tình cảm của tôi dành cho những người lính. Cuốn sách đã thực sự miêu tả được một phần của chiến trường ác liệt, nơi cái chết có thể đến trong nháy mắt. 
Ám ảnh chăng? Bởi Dear John thực sự rất hay. Không chỉ là một quyển truyện tình cảm thông thường, Dear John chạm đến những cảm xúc mong manh nhất của lòng người, những phút suy tư để không vấp ngã hay những khi cô đơn một mình không nơi nương tựa... Tất cả đều cô đọng lại, kết tinh lại trong cuốn sách này.
Đọc xong, lặng đi, và ngẫm nghĩ.
5
10907
2012-07-15 01:51:26
--------------------------
18442
40
Dear John, by Nicholas Sparks is a very well-written and intriguing book. It is a romance novel about a couple that is fighting the hardships of a long distance relationship. I find the story of Dear John very moving. I deeply appreciate the part that focuses on the father-son relationship. I’m not being too mushy but this part put tears in my eyes. A very compelling part that makes you love and respect your father more. If one is looking for a story about unconditional love and a great fan of Sparks' novels, well this is for you.
5
23703
2012-02-01 22:11:21
--------------------------
13135
40
Read the story by Nicholas Sparks finished third, one really has a fan of this author. The way the author describes emotional character, the character emotional development, wondering how his character is very natural, gentle and very honest.
With this story is not true that Savannah left its impression even though Savannah was built character quite the te.Doc Dear John I do not like the feel of Savannah Savannah little nao.Tinh for John only was the most emotional time does not pass the test film was created for Savannah thach.Trong reason than to break up by Tim John is sick I want to change care Sannah Alan is also the story I just felt her selfishness thoi.That sad for John as he loved her more than myself, the most impressive is his love child of John and Tim characters again, very noble.
5
9348
2011-10-19 18:21:05
--------------------------
13010
40
Read the three stories were published library of Nicholas Sparks, one really has a fan of this author. The way the author describes emotional character, the character emotional development, wondering how his character is very natural, gentle and very honest.
With this story is not true that Savannah left its impression even though Savannah was built character quite real. Most impressive is the love child of John and his character Tim again, very noble. Besides really impressed with your translations, word translation when reading really make you feel expresses the spirit of the story, very well. Again thank Nicolas.
Emotional father-son that Nicholas Sparks has portrayed in the series so successful. No need to use any of the art methods, they even used the words, all things are true feelings. First feelings which I'm sure he still rolled up in each time you think about his father. Each world has a father, a hero of you from time to time she grew up. The arms will be ready for you every time you stumble, maybe he will not help you get up but he certainly will not judge and stand ready to protect you. He also never said many words, who will always listen to you quietly, his father taught you everything you need in life sometimes just silence. In his silence filled with the tenderness, the patience, tolerance and above all, boundless love. Whether your father is, a billionaire, a man held or even an autistic patient, love of a father who will not change and it never stops.
5
9348
2011-10-17 20:33:53
--------------------------
12705
40
Actually I was reading Dear John I'm not impressed with that much love for seven years. I do not like Savannah, do not know why that first love Savannah for her true love to make me feel very superficial. If that is indeed the deep love, because if people are far from that time to split. I do not know, I've never loved someone so deeply, but I have seen so many women waiting for husbands faithful. Maybe they do not have love at first sight, such as Savannah, but they have stuff that people have called it confidence. It is also possible for other Asian concept to Westerners, I find that kind of love for John Savannah emotional hold just a powder, superficially. Kind of love that will burn up into a torch no one could extinguish the stimulus but no longer exists, becomes the love fades with time. For Savannah I just feel more disappointed. 

What really makes me shake in this book is the emotional relationship of father and son John. He was born into a very special family. Mom left when he had just rounded a month old and his father was a victim of autism. The father that even an art of communication behaviors small grower does not know he was growing up. The father is described in the story is a man of habit. Each day's first repeat exactly like a perfect circle. His bread for breakfast, bacon and omelet, he has been away from home in 7h35 'every morning, his phone never got a call and he has an endless passion for coins . The boy was baby John was so proud when his father always treated him like an equal, he imparted the most knowledge of the coins. He was sitting in his father's office for hours, was dad treated as adults. He found himself luckier than any boy in the world. Then he grew into a headstrong teenager. A normal person raising the age son was so difficult then, but his father was a patient with autism. He did not consider it as a passion more respectable, he thought it as a frightening obsession with his father. He grew up hating everything, destructive as any other child in his situation. The father ignored any sin that he has caused, he never once complained, never once judged. He is cooking breakfast for him, said he does not like to talk about the coins he has said nothing half-words. It was the most patient parent I have ever known, who never hugged or kissed John, who never expressed any emotion in front of John, including any verbal only. Every time John said: "I love you dad.". He just replied: "I love you, John à." It's just words for regular meeting between parents and children but for his speech encompasses all his love for John. And in most of the time he was only silence. Yes, he was silent and only the letter or that he expected the only exhibited a quiet but he knew his son would have felt. Making a normal father was very difficult, a father like that autism is a wonderful father. A father never taught or shown to the child class as anything, his father never complained or hit me, always quiet, only his father, who taught him more than anything a child son may require at his father. 
5
9348
2011-10-11 20:15:20
--------------------------
7992
40
I used to be afraid of loosing money on this book; I used to be hesitate to press on " Placed order" button. But when I went throught the last word of this book, it proves that I was wrong.
This is the first English book that I really could not stop to drink a cup of water to have energy for the next pages:) I was totally taken away by a simple writing style but long-term effective words. The story shows us a different aspect of love: The unconditioned love, just like what John did for Savannah , and ofcourse for Tim ! When the last page was closed, I was just sitting there and imagine how  the life of every characters in the story would be. Savannah and Tim , of course, would be less hard managing their life cos of John's 100% - in - silence help. But what does remain in my mind, untill now, is the moment that John realized what trully love means. It means our heart feels happy, warm, and "safe" when thinking of the smiles, the peace and the stability in life of the person we love; though we are  alone in the afterward road . 
Overall, I myself cannot recommend this book any higher! The Simpleness of writing style, The beauty of unconditioned love story, Nice  free book cover from Tiki.vn, and easy to carry the book with you everywhere... all of these positives gave me verry happy and interesting moments with the book.
Give it a place in your book-shelf!!!
5
6671
2011-07-10 18:17:41
--------------------------
3871
40
Lại là một quyển sách không thể chê vào đâu được của Sparks. Một chuyện tình tưởng "thường" nhưng lại dai dẳng đeo bám con người ta đến hết cuộc đời. Những câu từ đơn giản dẫn dắt hai con người từ lúc gặp nhau, để ý đến nhau, quan tâm đến nhau, rồi yêu nhau và xa nhau. Câu từ của Sparks chưa bao giờ là quá văn hoa, quá màu mè nhưng lại làm con người ta hồi hộp, nghẹn ngào thay cho nhân vật. Đọc Dear John và cùng John Tyree phiêu lưu cùng tình yêu với Savannah, mỗi trang truyện qua đi nhưng người đọc không biết chuyện tình này sẽ đi đến đâu, liệu hai con người này có đến được với nhau không. Một kết thúc không như mong đợi nhưng hàm chứa một tình yêu quá đẹp.
5
352
2011-05-05 19:54:03
--------------------------
3677
40
What can i say about this novel? Of course, MARVELOUS. Perhaps, each individual feels a different way about its content. For me, i only can sum up like this. When I'm reading it, i giggle in a childish way because i know what John would do next. I know that his relationship cannot work out as he and Savannah have some gaps between them. Those are invisible, but they are powerful enough to tear them apart. I know that he would sell the coin collection of his father for Tim's treatment and for the sake of his love for Savannah. When i finish the novel, close the book and let the emotion taken me all over, i feel sad. The sadness of what has not been completed and fulfilled. The sympathy for what John have been going through everyday.  
Love can touch you in many ways which it would. Yet, in this novel of Sparks, although it is sad, i feel that love has cashed a magic spell on both John and Savannah. Being together could have been a happy ending for the readers, but it would destroy something greater. Something i would like to refer as the art of love. What really connects John and Savannah was the memories they had shared in the very first time of John's leave at Wilmington.
The language is so simple, but it is touching. All i can say is that it was my best decision to buy this and make it one of my best collection of romantic novels.
I hope those with a romantic hearts and curiosity about what love is could read it some day. Believe it or not, it is better to curling up with a real book than to download it online and read it. Love is real, and so is the book. One thing you can do with a book is to hold it close to your chest and let the feeling taken you, but you are not able to do it with a computer or an Ipad.

5
4186
2011-05-02 17:42:25
--------------------------
3676
40
What can i say about this novel? Of course, MARVELOUS. Perhaps, each individual feels a different way about its content. For me, i only can sum up like this. When I'm reading it, i giggle in a childish way because i know what John would do next. I know that his relationship cannot work out as he and Savannah have some gaps between them. Those are invisible, but they are powerful enough to tear them apart. I know that he would sell the coin collection of his father for Tim's treatment and for the sake of his love for Savannah. When i finish the novel, close the book and let the emotion taken me all over, i feel sad. The sadness of what has not been completed and fulfilled. The sympathy for what John have been going through everyday.  

Love can touch you in many ways which it would. Yet, in this novel of Sparks, although it is sad, i feel that love has cashed a magic spell on both John and Savannah. Being together could have been a happy ending for the readers, but it would destroy something greater. Something i would like to refer as the art of love. What really connects John and Savannah was the memories they had shared in the very first time of John's leave at Wilmington.

The language is so simple, but it is touching. All i can say is that it was my best decision to buy this and make it one of my best collection of romantic novels.
I hope those with a romantic hearts and curiosity about what love is could read it some day. Believe it or not, it is better to curling up with a real book than to download it online and read it. Love is real, and so is the book. One thing you can do with a book is to hold it close to your chest and let the feeling taken you, but you are not able to do it with a computer or an Ipad.

5
0
2011-05-02 17:41:25
--------------------------
636
40
DEAR JOHN by Nicholas Sparks is a novel I would read again.
5
0
2010-11-27 10:19:20
--------------------------
