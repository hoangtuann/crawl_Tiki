408867
119
Mình đã đặt mua cuốn sách này vào khoảng 4 tháng trước và rất hài lòng về sản phẩm này của tiki. Thoạt tiên có thể thấy đối với một cuốn sách bìa mềm như Men are from Mars, women are from Venus thì hơn 200k là khá cao. Tuy vậy, đọc một thời gian thì mình cảm thấy không phí phạm tiền bạc chút nào. Quyển sách đã chỉ ra sự khác biệt trong cách suy nghĩ và hành động giữa nam và nữ, từ đó đưa ra những lời khuyên và ví dụ thực tế đi kèm nhằm giúp ta khắc phục mối quan hệ với nhau. Đây là một quyển sách rất có ích đối với những bạn nào thích khám phá về mặt tâm lý tình cảm. 
5
923880
2016-04-01 13:55:53
--------------------------
