574417
129
Mình đặt lúc trưa, sáng hôm sau nhận được hàng ngay. Đang trên lớp học a shipper vô tận trường ship luôn mới ghê. Sách đẹp hộp đẹp thơm nữa.
5
5188208
2017-04-16 01:27:01
--------------------------
571905
129
Mình là Fan bự của series phim này. Coi phim thấy chưa thoả mãn nên tìm sách đọc. Mà mấy lần lên Tiki thấy đều chưa mua được. Lần đầu lên coi thì thấy giá là 1tr8 mấy chục ngàn luôn. Thấy hơi chát nên định bụng thôi để dành dụm từ từ rồi mua. Cái lúc có được kha khá tiền thì lên thấy hết hàng mất. Lâu thiệt lâu cũng không thấy có hàng lại, cảm thấy buồn và nuối tiếc vô cùng. Tự nhiên, hôm đêm nọ lại mò lên coi thử. ÔI MẸ ƠI, NÓ GIẢM 50%. Mua liền không suy nghĩ. Lúc bấm mua mà tay cứ run run, sợ bấm zô nó báo hết hàng thì khổ. =))))
2 ngày sau thì nhận được hàng (dù mình ở tận hóc bà tó của Bình Dương.) Sách được đóng trong thùng carton rất cẩn thận. Rồi sau đó là đến hộp của sách được bọc kiếng. Nói chung là vô cùng ưng ý. 
Mình đang đọc quyển 1. Lối viết rất gần gũi, dù là tiếng anh nhưng cách hành văn rất dễ hiểu. Luyện xong bộ này chắc lên cơ tiếng anh luôn =))))
5
1738875
2017-04-13 08:28:17
--------------------------
569385
129
lúc đầu hoài nghi về sách rất nhiều vì sợ lậu, giá gốc hơn 1m8 mà giảm còn 943k thật bất ngờ. Là một fan ruột của series game of thrones trên HBO, nhưng phải nói đọc sách mang lại một trải nghiệm hoàn toàn mới. Quyển sách này phù hợp vs các mem đang cày foreign language, chắc chắn các bạn sẽ hài lòng về những ngôn ngữ mĩ miều và đầy khêu gợi. Hơi tiếc là nó ko phải bìa cứng. However it truly worth with any cent you have paid!
5
1809188
2017-04-10 17:58:54
--------------------------
569153
129
I just received the whole box set yesterday, and it was in perfect condition! I love Tiki delivery service also. The shipper was nice, and the service was so fast. I ordered on Friday morning and on Saturday morning I got it shipped. As for the books, I have never read them, but I watched the whole movie series and got so engrossed! Well I tried a few pages of A game of thrones already and the writing style was quite satisfying to me. Hopefully I will finish the whole box by this year :))
5
15681
2017-04-10 14:27:07
--------------------------
491169
129
Mình rất hài lòng về bộ truyện này. Sách trong tình trạng hoàn hảo, hộp ở góc có hơi tróc, nhưng toàn diện thì bộ này ở tình trạng tốt nhất có thể. Khi nhận hàng, ngoài hộp sách còn được bọc nilon cẩn thận nên không sợ xước trong quá trình di chuyển. Đi kèm còn có thêm cả bản đồ Westeros & the Free Cities. Mình mua trong đợt giảm giá nên nếu xét giá riêng mỗi quyển chỉ trên 200k chút xíu. Mỗi quyển đều dày cộp gần 800 trang nên phải nói là giá rất được. Nếu bạn là fan ruột của bộ truyện và cả tv series thì nên mua ngay đi! 
5
1265752
2016-11-16 18:34:55
--------------------------
490472
129
Mình mua đợt giảm giá còn 1tr090, lại được coupon 250k nên  mình đặt ngay. Đặt  giao tiêu chuẩn thì tưởng t2 hay t3 mới nhận được. Vậy mà sáng t7 người ta giao luôn, tiếc là tối đó mình đặt thêm Divergent mà không thấy sáng giao luôn. Bộ này 7 cuốn siêu nặng siêu dày luôn, bên trong còn được tặng bản đồ nữa, box hơi tróc 1 tí nhưng sách vẫn còn vẹn nguyên.
5
1859422
2016-11-12 22:47:08
--------------------------
