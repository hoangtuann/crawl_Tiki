572913
8
It's such a shame that I haven't reviewed this book earlier. You will be hooked right from the beginning. The first chapter of this book was the most fascinating chapter I of all of the books I have read so far. He succeed in setting the tone of the book with the first sentence: "So you’re all set for money, then?" the boy named Crow asks in his typical sluggish voice. This book is often said as a difficult read but it doesn't necessarily mean it's not intriguing. All of the characters are ordinary, common people just like us but they live true to themselves and try their best to fulfill what they need to do although it means they have to sacrifice their usual comfort lives. That's what I love about them.
5
5383
2017-04-14 10:44:39
--------------------------
28554
8
Kafka on the Shore is definitely not an easy to read book or a light read. After reading the book, I was left with many unanswered questions. Maybe what I understand is just the basic of the issue but I think it very meaningfulThe story gives you a picture and a thought about the meaning of your own life. It does not care about the ending of the story, I just want you to open your mind, feed your imagination, and finish the story anyway you like. 
The characters play an important part in the story’s plot. Everyone is a part of the big picture, without a single person, the picture cannot be finished, the story is unfold. Everyone has his own places and no matter how few you read about them, you still remember them. 

5
17940
2012-05-28 20:19:50
--------------------------
