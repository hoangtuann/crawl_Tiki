388316
74
Bạn đang thất vọng về bản thân, bạn buồn vì không đạp bằng người khác, bạn buồn vì không cao bằng người khác, và nhiều thứ bạn không thể bằng người khác, bạn thất bại còn họ thành công, họ vui vẻ, hạnh phúc ocnf bạn thì buồn tủi, bạn cảm thấy lạc lõng giữa dòng đời. Thì bạn hãy nhớ rằng còn nhiều người không bằng bạn nhưng họ vẫn thành công, vẫn vui vẻ với cuộc sống. Diều gì đã làm nên điều đó. Đó chính là sự dũng cảm chấp nhận hiện thực, là sự quết tâm vượt qua số phận. Và một tinh thần lạc quan, luôn mở rộng tấm lòng. Và hãy đọc cuốn sách để hiểu và đạt được điều đó.
5
1054846
2016-02-28 18:00:30
--------------------------
