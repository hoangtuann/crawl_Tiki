286094
8945
Khi đọc phần Giới thiệu nội dung truyện, mình thấy rất thú vị, nên đã mua cuốn này.
Truyện size khá lớn, dày, nội dung khá lan man.
Truyện có 2 nhân vật chính, nhưng lại có nhiều nhân vật phụ, tác giả lại đi quá chi tiết đời sống và các mối quan hệ của tuyến nhân vật phụ, khiến truyện dài dòng, lan man. Nhiều lúc mình phải bỏ qua các chi tiết này để đến với nhân vật chính. 
Trong khi đó, mối quan hệ giữa Phiêu Phiêu và Thụy Căn được miêu tả hời hợt, cảm giác như họ đến với nhau chỉ vì người này thích hợp làm chồng/ vợ của người kia hơn là tình yêu, nói quý mến, thích nhau thì hợp hơn.
Tính cách các nhân vật hài hước, suy nghĩ của Phiêu Phiêu không hề truyện, cô ấy rất thực tế, nhưng hoàn toàn vui vẻ và hài hước, đấy là điểm mạnh của Phiêu Phiêu, nhưng lại bị tác giả cho nghĩ quá nhiều đến việc của người khác, thể nào truyện lại dày thế!
Nói chung, ngoài hình thức ok thì nội dung rất bình thường!
3
170711
2015-09-01 13:59:16
--------------------------
178072
8945
Truyện hơi dài, phần đầu còn thú vị, nhưng phần sau thì hơi lê thê, tình tiết cũng kém đặc sắc hẳn đi, trong khi đáng lẽ có thể kết thúc ngay từ đoạn giữa cũng được. Mình thích cách nữ chính cố gắng hết mình để vun đắp cho tình yêu mà không hề tính toán. Nhưng nam chính thì hơi bị tự phụ đến khó ưa, tính cách cũng chán, mặc dù cái tâm lý tự phụ và "con cá mất là con cá to" vốn khá phổ biến. Một điểm trừ khác là truyện có mật độ văng tục, chửi bậy hơi bị dày đặc, xem mà choáng.
3
57459
2015-04-04 10:13:38
--------------------------
