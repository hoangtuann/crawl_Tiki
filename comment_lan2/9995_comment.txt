284447
9995
Dù với tư cách là người học tiếng Anh hay người dạy tiếng Anh thì mình cũng đánh giá rất cao bộ sách này.
Bất kỳ ai học tiếng Anh cũng không nên bỏ qua bởi vì có những lợi ích sau:
Một, nó cung cấp từ vựng cơ bản theo từng chủ đề giúp bạn có thể giao tiếp được. 
Hai, luyện nghe tương đối thích hợp vì không chỉ có nhiều tình huống phong phú sát thực tế mà còn có nhiều giọng khác nhau. 
Ba, người dùng có thêm kiến thức về những lĩnh vực khác như điện ảnh, sách truyện, âm nhạc nước ngoài. Đó là cái cần thiết để bạn có thể nói chuyện với người nước ngoài. Nếu không có kiến thức nền bạn sẽ quay đi quay lại những câu giao tiếp căn bản làm cuộc hội thoại trở nên nhàm chán. 

Mình biết Agatha Christie cũng nhờ một bài tập luyện nghe trong giáo trình này. Bài nghe nói về những tác giả trinh thám nổi tiếng và tác phẩm nổi bật của họ. 
5
535536
2015-08-30 22:33:50
--------------------------
