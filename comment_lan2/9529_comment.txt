504241
9529
Nếu ở cuốn thứ nhất “ Chiếc Lexus và cây Ôliu”  Thomas Friedman giới thiệu về toàn cầu hóa, tiến trình các quốc gia lựa chọn toàn cầu hóa như một cơ hội để phát triển. Đến cuốn thứ hai “ Thế giới phẳng”  toàn cầu hóa đã là một tất yếu, tiến trình toàn cầu hóa là động lực thúc đẩy sự phát triển của kinh tế thế giới đem lại thành công cho  nhiều quốc gia, dân tộc.
Đây là cuốn sách thứ ba của Friedman về chủ đề toàn cầu hóa.  Cuốn sách này nêu lên mặt trái của toàn cầu hóa đó là các vấn đề về cạn kiệt năng lượng và biến đổi khí hậu. Bên cạnh việc phân tích các mặt trái của toàn cầu hóa, tác giả cũng đưa ra các giải pháp và kêu gọi việc thực thi các giải pháp đó ở mỗi quốc gia, cộng đồng , doanh nghiệp hay mỗi cá nhân. Các giải pháp chính đó là: 
-	Khai thác, sử dụng tài nguyên  hiệu quả hơn
-	Nghiên cứu và sử dụng năng lượng sạch như là một giải pháp thay thế trong tương lai
-	Bảo vệ thiên nhiên và môi trường
 Rất nhiều ý tưởng hay, những ví dụ thực tiễn có giá trị  để hiện thực hóa các giải pháp trên được nêu lên trong cuốn sách như là bài học kinh nghiệm cho chính chúng ta trong quá trình hội nhập và phát triển khi mà Việt Nam là một trong những nước chịu ảnh hưởng nặng nề nhất của biến đổi khí hậu, của việc khai thác sử dụng tài nguyên thiên nhiên bao gồm cả tài nguyên rừng, biển, tài nguyên đất đai một cách thiếu thận trọng ảnh hưởng đến thế hệ tương lai.

Đến thời điểm này nhân loại đã rõ hơn và phải trả giá một mặt trái khác của toàn cầu hóa về mặt địa chính trị đó là sự bất bình đẳng ngày càng gia tăng trên toàn thế giới, trong từng khu vực hay trong mỗi quốc gia, tạo nên một làn sóng của chủ nghĩa dân tộc, ly khai. 
Có lẽ đây sẽ là chủ đề trong cuốn sách tiếp theo của Friedman!

5
469959
2017-01-03 11:02:16
--------------------------
453124
9529
Mình mua quyển sách này ngay sau khi đọc tác phẩm nổi tiếng Thế giới phẳng, và quyển sách thật sự đã không làm mình thất vọng. Vẫn một góc nhìn vô cùng mới mẻ, xuất phát từ thúc đẩy phải cách tân từ trung tâm kinh tế tài chính khổng lồ Hoa Kỳ, lần này với đích đến là đề tài khí hậu vô cùng thời sự, rất đáng mong đợi và dành thời gian tìm hiểu và thưởng thức. Bản dịch chất lượng, trình bày rõ ràng đẹp mắt, chất liệu giấy tốt và nhẹ tay, làm tăng thêm cảm giác hài lòng.
5
171151
2016-06-21 03:01:02
--------------------------
384924
9529
Sau khi đọc cuốn Thế giới phẳng của tác giả Thomas L. Friedman, tôi tìm kiếm mua thêm những sách của ông để đưa vào bộ sưu tầm sách của mình. Tìm hoài không có cuốn Nóng, phẳng, chật ở các nhà sách thì tiki đã tái bản cuốn này. Tôi rất mừng và mua liền. Thật không ngờ nội dung của sách khác với Thế giới phẳng, một nội dung rất hay, rất thiết thực và hữu ích trong cuộc sống hiện nay của mỗi chúng ta. Đó là Môi trường. Một thế giới xanh, một cuộc cách mạng xanh mà tác giả mong muốn thế giới có được. Về những nghiên cứu môi trường áp dụng vào nền kinh tế, về cuộc sống thiết thực và bảo vệ môi trường để con người có thể sống lâu hơn.Một cuốn sách rất ý nghĩa với tôi.
4
358906
2016-02-23 10:53:52
--------------------------
324070
9529
Điều đầu tiên mình muốn nói là mình đã mua cuốn sách này sau khi sở hữu hai cuốn Thế giới phẳng cùng với Chiếc Lexus và cây Ôliu.
 Những cuốn sách của Thomas L. Friedman luôn mang theo hơi thở của thời đại, tầm nhìn vĩ mô và những vấn đề thời sự nóng hổi trên thế giới. Tất nhiên, mình rất sợ lối hành văn hàn lâm, khô khan của những cuốn sách viết về kinh tế. Tuy nhiên, những cuốn sách của Thomas L. Friedman không như vậy, lối viết luôn gợi một sự tò mò ở người đọc, khiến bạn lật hết từ trang này sang trang khác theo một mạch chảy không ngừng. Cách dẫn dắt vấn đề của ông cũng rất thú vị, dễ hiểu dễ nhớ. 
Theo mình, nếu bạn đã lỡ thích Thế giới phẳng và Chiếc Lexus và cây Ô-liu thì bạn cũng nên có cuốn sách này trên kệ. 
5
759572
2015-10-20 12:49:17
--------------------------
197799
9529
Tôi biết tới nhà báo - tác giả Thomas L. Friedman qua các tác phẩm Thế giới phẳng, Chiếc Lexus và cây ô liu, Từng là bá chủ, Từ Beirut đến Jerusalem,... Tất cả các tác phẩm của ông đều thể hiện tầm nhìn vĩ mô về các vấn đề, sự kiện lớn của thế giới, nước Mỹ và toàn cầu hóa. Trong quyển sách này, ông chủ yếu đề cập tới những cuộc cách mạng xanh mà nước Mỹ cần thực hiện và những điều mà quốc gia cần làm để cải thiện chất lượng đời sống của người dân. Không chỉ riêng nước Mỹ, những giải pháp rút ra từ những chia sẻ của Thomas L. Friedman đều có thể áp dụng cho mọi quốc gia trên thế giới. Một quyển sách thể hiện tầm nhìn vĩ mô của Thomas L. Friedman.
5
387632
2015-05-18 15:12:48
--------------------------
136472
9529
Tác phẩm thực sự mang tính thời sự. Nêu lên hàng loạt các vấn đề nóng của thế giới hiện đại. Đọc tác phẩm, chúng ta sẽ hiểu thế giới ngày nay đang xoay chuyển như thế nào.
Như những tác phẩm trước của Thomas, cách dẫn truyện của ông quả là tuyệt vời, khó mà có thể đặt quyển sách xuống được khi mà chưa hoàn thành xong một phần nhất định, bởi nó lôi cuốn bởi câu hỏi nối tiếp câu hỏi, tại sao, tại sao và tại sao.
Cuốn sách cung cấp một khối lượng tri thức khá lớn bao phủ nhiều mặt của cuộc sống và nhiều quốc gia. Dĩ nhiên vấn đề trọng tâm vẫn là môi trường, nhưng quyển sách 577 trang này còn cho chúng ta cái nhìn toàn diện nhất, và  trả lời được câu hỏi tại sao, đồng thời cũng đưa ra những giải pháp sáng tạo.
Cuốn sách gồm 5 phần:
Phần I : Chúng ta đang ở đâu
Phần II: Nơi chim trời không bay qua
Phần III: Chúng ta đi tiếp như thế nào
Phần IV: Trung Quốc
Phần V: Mỹ

Khó mà có thể tóm lượt hết nội dung của cuốn sách trong vài dòng được, bởi một lượng tri thức rất lớn mà nó truyền tải.
Một tác phẩm nên đọc để biết rằng thế giới này đang vận hành như thế nào.
5
385588
2014-11-21 13:57:41
--------------------------
116507
9529
Cũng như những cuốn khác của T.Friedman, Nóng, Phẳng Chật đầy những nhận xét sắc bén của một nhà báo lão luyện. Nếu bạn chưa hiểu tầm quan trọng của các vấn đề môi trường đối với nền kinh tế toàn cầu, với tình hình địa chính trị thế giới thì cuốn sách này có thể xem là một sự dẫn nhập. Theo mạch ý tưởng đó sẽ là những giải pháp về năng lượng mới, về cách thay đổi nhận thức về sử dụng năng lượng.
 Khác với những cuốn như Thế Giới Phẳng, Chiếc Lexus Và Cây Olive, Từng Là Bá Chủ... vốn đem lại nhiều tranh cãi. Nóng, Phẳng, Chật là không thể tranh cãi đối với bất cứ ai. Rất đáng để xem.
5
374383
2014-07-09 09:39:02
--------------------------
54375
9529
Mình đã mua cuốn sách này vì tên tuổi của tác giả Friedman, người đã tạo nên tác phẩm nổi tiếng "Thế giới phẳng". Trong cuốn sách lần này, ông tiếp tục sử dụng hiểu biết thông tuệ cùng tài năng văn chương độc đáo để tạp nên một tác phẩm phản ánh chân thực bộ mặt của thế giới đương đại. Mỗi trang viết đầy ắp những thông tin, phát kiến mới mẻ và thú vị, với những vấn đề tưởng chừng cũ nhưng lại được nhìn nhận dưới nhiều góc độ tinh tế. Tác giả đã không chỉ đem đến một công trình nghiên cứu đồ sộ về môi trường, mà còn phân tích được những lợi ích mà một cuộc cách mạng xanh sẽ đem lại cho mọi người chúng ta. Một cuốn sách thực sự thú vị và có gái trị cao về nhiều mặt.
4
20073
2013-01-09 08:21:26
--------------------------
