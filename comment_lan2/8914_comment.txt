474768
8914
Đọc cuốn này khiến tôi phải suy ngẫm về nền giáo dục xung quanh tôi. Tôi thấy mình phải thay đổi từ ngay bây giờ về cách nghĩ cũng như cách tiếp thu những thứ trong cuộc sống. Cuốn sách này giúp tôi rất nhiều trong việc tự nhìn và cảm nhận bản thân. Bìa cuốn sách khá hấp dẫn với màu xanh, hình ảnh dễ thương  và ấn tượng  rất thu hút người đọc. Tựa đề gây hứng thú cho người đọc bởi chữ “GIÀU”và phông chữ dễ nhìn. Nội dung sắp xếp hợp lí không hề nhàm chán.
4
91465
2016-07-11 22:35:08
--------------------------
349900
8914
Cuốn sách tập trung vào vấn đề bằng cấp chính quy hay ra đời và tự tạo lập. Bản thân tôi cực kì thích cuốn sách này vì thú thật hơn 12 năm học trường lớp , tôi chỉ học cho qua và đứng mức khá, chỉ môn nào tôi yêu thích như vật lý hay anh văn mới điểm cao, lên đại học tôi lại càng chán ngán và thấy thật phí thời gian! Thế là tôi ấp ủ chuyển sang việc kinh doanh và ra đời ngay, nhưng bấy giờ tôi cũng bị vấn đề "còn bằng cấp thì sao?". Và nhờ Ellsberg, tôi giờ đây hoàn toàn tự tin tập trung vào  mục tiêu của chính mình, đó là tạo ra sự giàu có, thịnh vượng từ chính những gì tôi yêu thích làm.
Cuốn sách theo tôi nghĩ, có lẽ đã khá thiên vị cho hướng tự trãi đời hơn là học đại học, nhưng dĩ nhiên vẫn còn rất nhiều người đang mong chờ được vào làm cho các tập đoàn, doanh nghiệp lớn, vậy thì nên tập trung vào nội dung "tư duy giàu, tư duy nghèo" mà Ellsberg đề cập trong sách, vì điều này sẽ giúp cho mỗi cá nhân toả sáng dù đang làm gì ở bất kì hoàn cảnh nào!.
5
153783
2015-12-09 12:34:14
--------------------------
327497
8914
"Nền giáo dục của người giàu" là một trong những cuốn sách nổi tiếng và bán chạy ở nhiều nước. Khi đọc bạn sẽ có nhiều suy ngẫm về việc giáo dục hiện nay ở gia đình và xã hội. Quyển sách đem lại sự thú vị cho người đọc khi họ được chia sẽ về nền giáo dục của những người giàu như thế nào và cuộc sống của họ đã thành công ra sao. Nội dung quyển sách rất thực tiễn và bổ ích, nó giúp ta nhìn nhận được các vấn đề về giáo dục sâu sắc hơn, giúp ta thấu hiểu việc nên giáo dục cho con cái chúng ta như thế nào, cũng như bản thân của chúng ta cần phải thay đổi những gì để thành công hơn trong cuộc sống.   
4
294223
2015-10-27 22:47:55
--------------------------
311741
8914
Nền giáo dục của người giàu là một trong những quyển sách khởi điểm của mình trong loạt sách về tư duy làm giàu bên cạnh các tác phẩm kinh điển như Think And Grow Rich,v..v... Cuốn sách cung cấp những trải nghiệm thực tế làm giàu từ các tỷ phú và những con người giàu có, tổng hợp lại dưới 7 chương sách là 7 kĩ năng, kinh nghiệm. Cuốn sách đề cao tinh thần tự học hỏi trong cuộc sống và trí tuệ thực tế mới dạy cho ta những bài học đắt giá để phát triển chứ không phải mớ kiến thức sáo rỗng từ trường học – nơi thiếu hẳn sự mạo hiểm, quyết đoán, sáng tạo và liều lĩnh. 

3
565623
2015-09-20 08:11:21
--------------------------
288966
8914
Mình thích những câu chuyện tác giả đề cập trong sách và mình  cũng có cái nhìn tổng quát về nền giáo dục của người giàu là như thế nào trong cách họ tư dung và lập luận vấn đề. Điểm cộng nhỏ cho sách là bìa khá bắt mắt. Tuy nhiên theo ý kiến cá nhân, mình nghĩ sách chỉ mang tính chất để tham khảo vì sách cần có những dẫn chứng cụ thể và rõ ràng hơn nữa. Về mặt lý thuyết tác giả nói đến khá thuyết phục nhưng về thực tế để có thể làm giàu không đơn giản chút nào. 
2
276800
2015-09-04 00:04:27
--------------------------
259904
8914
Liên tưởng đầu tiên của mình khi thấy quyển sách này là nó khá giống bộ sách vô cùng nổi tiếng : Dạy Con Làm Giàu của Robert T Kiyosaki .
Hầu hết mọi người đều muốn giàu có, tuy nhiên không phải ai cũng đạt được mức tự do tài chính cần thiết của bản thân , liệu học thật nhiều và thật giỏi có trở nên giàu có ? Không hẳn như vậy, muốn giàu có cần có những tư duy khác biệt về đầu tư , maketing bản thân và nhiều thứ mà ở trường bạn không được học.
Một quyển sách hay và bổ ích.
4
554150
2015-08-09 22:30:38
--------------------------
254480
8914
Tôi mua sách vì thật sự tựa sách khá ấn tượng - Nền giáo dục của người giàu, trong xã hội hiện nay thì có lẽ ai cũng mong muốn được làm giàu và đó cũng được xem là một mong muốn chính đáng. Nội dung sách thật sự bám sát theo tựa sách nói về những quan điểm, tư tưởng mà những người giàu đã học được từ khi còn nhỏ. Tuy nhiên có một vấn đề là quyển sách này đưa ra các quan điểm của tác giả khá hay và khá thuyết phục nhưng lại không phải dựa trên các công trình nghiện cứu, khảo sát cụ thể lại không đưa ra được các dẫn chúng rõ ràng , nên các thông tin trong sách tối nghĩ mang tính tham khảo là chính.
2
42985
2015-08-05 11:28:10
--------------------------
237733
8914
Mình khá là thích quyển sách này. Đọc cũng rất thú vị, bởi vì tác giả là một người có lối suy nghĩ khác biệt, không phải ông cổ suý cho việc bỏ học để lập nghiệp, mà ông muốn hướng tới quan điểm rằng muốn thành công và trở nên giàu có thì bạn cần đến kiến thức thực tế thật nhiều, nhiều hơn những kiến thức được dạy trong thế giới học thuật sáo mòn. Tác giả cũng chỉ ra nhiều  cách thức để tiếp cận những người mà chúng ta muốn giao du để trở nên thành công hơn; cách thức  để tự học marketing trực tiếp mà không tốn kém nhiều tiền bạc, bí quyết kinh doanh và tự học kinh doanh của những người thành công trên thế giới;...Mình chỉ tiếc cái là quyển sách...hơi mỏng :) Đọc thì hay nhưng mình ước gì nó cụ thể hơn nữa. 
4
127819
2015-07-22 18:01:27
--------------------------
234754
8914
Muốn trở nên giàu có, hãy tư duy theo cách của những người giàu có!
Dọc theo 7 chương của quyển sách, tác giả đã giới thiệu 7 kỹ năng không thể thiếu của những người thành công đến độc giả bằng những ngôn từ dễ hiểu nhất. Thật đúng như tác giả đã chia sẻ, thành công không nhất quyết phải qua con đường đại học. Tất cả đều do những kỹ năng mềm sẽ quyết định bạn sẽ làm chủ hay làm thuê, những kiến thức tôi chưa từng được học khi còn ngồi ở giảng đường suốt 4 năm trời. 
5
390107
2015-07-20 15:46:49
--------------------------
216696
8914
Cuốn sách cung cấp những bài học thực tế về các kỹ năng mềm cần thiết khi đi làm và cả trong cuộc sống. Ngoài bằng cấp ra, cuốn sách chứng minh bạn đang thiếu những kỹ năng gì và những kỹ năng đó cần thiết ra sao. Và tác giả cũng chỉ dẫn cụ thể bạn cần phải học điều gì và ứng dụng nó ra sao. Cuốn sách mang những lời khuyên hữu ích, không sáo rỗng và dài dòng, rất phù hợp với những bạn muốn nâng cao tư duy và các kỹ năng cần có khi đi làm. Ngay khi bạn còn đi học, biết đến những kỹ năng này cũng thật sự cần thiết
4
13083
2015-06-28 09:10:16
--------------------------
196481
8914
Bằng cấp theo tôi chỉ là một chứng chỉ chứng minh rằng ban đã hoàn thành một khóa học. Nói về tình trạng thất nghiệp, bằng cấp không có lỗi. Lỗi là ở người trao bằng và nhận bằng mà thôi. Quyển sách Nền giáo dục của người giàu cho ta thấy điều đó cực kỳ đúng. Rất nhiều người giàu có, triệu phú hay tỉ phú đều không có bằng đại học nhưng không phải là họ không học, chỉ là họ không học ở trường mà thôi. Họ học những gì mà họ cho là cần thiết để làm việc và cống hiến. Quyển sách này góp phần thay đổi những tư duy cổ hủ, hạn hẹp và nâng cao những kỹ năng cần thiết cho chúng ta.
5
387632
2015-05-15 22:17:45
--------------------------
98831
8914
Thành công không chỉ dành cho những bạn đang ngồi trên ghế nhà trường mà còn dành cho tất cả mọi người. Cuốn sách hơi cổ vũ và ca ngợi những người bỏ học giữa chừng và đã thành công, nhưng một thực tế cho thấy là số người thành công và kiếm được việc làm đàng hoàng khi hoàn thành đại học vẫn chiếm tỷ lệ cao hơn.
Cuốn sách cũng cung cấp các kiến thức về Marketing cũng như về lãnh đạo cần thiết cho các bạn muốn lập nghiệp
Muốn thành công các bạn phải nỗ lực bản thân trong học tập thật nhiều, cho dù bạn có đang học đại học hay là bạn không đi học.
4
101762
2013-11-07 15:54:55
--------------------------
91920
8914
Một cuốn sách hay cổ vũ cho tinh thần tự học, học không ngừng nghỉ từ cuộc sống, đường phố chứ không chỉ đơn giản trên giảng đường. Ở cuốn sách này, Michael Ellsberg đã thực hiện phỏng vấn những tỷ phú thế giới và khám phá ra rằng, chính những gì họ học được từ trường đời hay “ trí thông minh đường phố” mới thực sự giúp họ thành công. Trường học chỉ dạy ta những kiến thức có sẵn và phải tuân theo nó, vì thế bào mòn đi sự sáng tạo và tính chấp nhận mạo hiểm, mà những điều này lại hết sức cần thiết để thành công trong môi trường cạnh tranh toàn cầu ngày nay. Cuốn sách còn nêu lên một ý tưởng rất hay: luôn luôn chọn con đường khó để làm thì sau đó mọi việc còn lại sẽ trở nên dễ dàng hơn. Rất đáng để suy ngẫm.
4
140131
2013-08-23 21:48:02
--------------------------
90204
8914
Trong cuốn sách này, tôi ấn tượng với rất nhiều cách nhìn nhận vấn đề của Michael Ellsberg, ví dụ như: 
“Người giàu không sử dụng kiến thức trong trường đại học để làm giàu, họ sử dụng cái gọi là "tri thức thực tiễn" hay "trí thông minh đường phố”. 

Hay là đoạn đối thoại của hai cha con, Bố nói: “Cameron, con phải học đại học. Con có thể mất nhà, mất công ty, tiền bạc, thậm chí là vợ con, nhưng không thể thất học. Học vấn là thứ duy nhất không rời bỏ con”.

Người con đáp lại rằng: “Điều đó đúng, nhưng con không đồng ý. Con vẫn đang học hỏi, tại trường đời. Cho dù con không ngồi trên giảng đường hàng ngày nhưng con vẫn học với nhịp độ nhanh hơn các bạn bởi họ phải cố gắng tiếp thu bài giảng của thầy cô. Còn con được học chúng bằng cách trải nghiệm chúng. Trong thời đại Internet bùng nổ, các bạn đồng môn khác sẽ chẳng đuổi kịp con được”.

Hay là khi Michael nói về sự ảnh hưởng của cá nhân đối với một tập thể, cụ thể như trong một môi trường làm việc. Nếu bạn sợ chịu trách nhiệm khi ra quyết định để bảo vệ công việc thì đây chính là sự bảo vệ yếu nhất mà bạn đang làm. Bởi một khi bạn không có sự ảnh hưởng nào thì có nghĩa là sự tồn tại của bạn không quan trọng, đến khi có sự giảm biên chế thì bạn có thể sẽ là một trong những người đầu tiên nằm trong danh sách ấy.

Và còn rất nhiều ví dụ và những lý luận khác nữa ở các lĩnh vực khác nhau để nói đến vì sao người khác thành công còn bạn thì không, họ đã làm thế nào,v.v... Với tôi, những điều mà Michale nói thực ra đã từng có người đề cập đến, nhưng họ chỉ nói một phần nào đó trong số những gì Michael đã thể hiện trong cuốn sách này, ví dụ như nếu bạn dám đảm nhận trách nhiệm thì bạn có thể sẽ đạt được thành quả mà người khác không bao giờ có, hay đại học không là tất cả,v.v... Với những ví dụ cụ thể, cách diễn đạt dễ hiểu cuốn sách không còn mang tính hàn lâm học thuật mà lại rất dễ dàng tiếp cận với người đọc. Thông qua những cuộc đối thoại của các nhân vật, bạn sẽ thấy được một cách nhìn khác đối với những vấn đề không còn mới nữa. Số đông không là tất cả và chưa chắc đã hoàn toàn đúng. Vấn đề là bạn có dám nghĩ và dám làm hay không mà thôi.

5
63376
2013-08-08 07:01:13
--------------------------
85825
8914
Sự giáo dục đối với một con người là vô cùng quan trọng, đặc biệt là khi còn trẻ vì nó sẽ gần như quyết định tương lai của cả một đời người. Cuốn sách khá thực tế và đưa ra nhiều khám phá, chỉ dẫn không mới nhưng rõ ràng là chính xác. Cuốn sách đã lí giải sự thành công của bao người danh tiếng, giàu có trên thế giới- họ giàu có vì chính năng lực thực sự của mình. Và cuốn sách cũng cho mình hiểu hơn, tại sao nước mình lại có nhiều sinh viên tốt nghiệp đại học mà thất nghiệp đến vậy!
Chỉ còn một năm nữa là mình sẽ thi đại học, nhưng đọc xong cuốn sách này, việc thi đại học không còn áp lực nhiều như mình vẫn tưởng nữa. Đại học không phải là tất cả. Cảm ơn tác giả Michael Ellsberg nhiều!
5
52262
2013-07-07 09:32:13
--------------------------
