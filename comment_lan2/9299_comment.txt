496084
9299
Ấn tượng mỗi cái "mục lục", nội dung câu từ lặp lại khá nhiều. 
Xuyên suốt quyển sách là những triết lý, trích dẫn ngắn & nhanh đến nỗi chăng cảm nhận được chút cảm xúc nào vì quá hời hợt & thiếu chiều sâu. 
Cũng có thể mình đã đọc qua một số sách khác như" thế giới nay rộng lớn quá & chúng ta còn nhiều việc phãi làm" hay " thay thái độ đổi cách sống" gần nhất là cuốn " bơ đi mà sống "cua Mèo lười nên vô tình chung lúc đọc mình bị so sánh nhiều quá.
Mà biết đâu mỗi ngừoi mỗi cảm nhận :) ai đó thấy ý nghĩa và hay thì sao 


1
1296075
2016-12-15 20:33:42
--------------------------
477016
9299
Bạn có muốn thấy được kế hoạch mà Chúa đặt ra cho cuộc đời bạn , sử dụng được tài năng mà Ngài đã ban cho bạn và bắt tay vào hành động để đạt được những thành công vĩ đại mà bạn hoàn toàn xứng đáng. Sau khi đọc xong cuốn sách này, mình đã có những bước đi đúng đắn hơn bởi sách đã có nói những lời như dẫn đường chúng ta vậy. VD: Thất bại đang đứng chờ kẻ bỏ cuộc giữa chừng. Bên cạnh đó, chất liệu sách rất tốt, trình bày đẹp, hợp lý về mọi mặt. Cực thích sách này luôn. Sách sẽ thay đổi cuộc đời ai muốn thành công thực sự. Thân!
5
1371377
2016-07-24 10:32:48
--------------------------
471986
9299
Hẳn khi bạn nào đọc xong cuốn sách này, bạn sẽ biết đến câu này ở đâu trong cuốn sách. "Một hành động có giá trị hơn ngàn ý định tốt." Cuốn sách có bìa ấn tượng thật đấy, 4 quả táo trong đó 3 quả đỏ và một quả xanh. Thật tuyệt nhỉ. Mỗi người chúng ta là duy nhất, dấu vân đã khẳng định điều ấy. Có một số chỗ ở cuốn sách này mình vẫn chưa rõ lắm. Nhưng đây là cuốn sách dành cho mỗi người chúng ta khi mà mỗi ngày qua chúng ta có lẽ không phải là chính mình. ^^
5
508336
2016-07-09 08:18:28
--------------------------
452866
9299
Mình chọn quyển này vì nghe cái tên rất có ý nghĩa. Và khi đọc xong cảm thấy lựa chọn của mình là hoàn toàn đúng đắn!
Sách được trình bày đẹp, rõ ràng trên giấy. Bìa thì thiết kế rất đặc biệt và khi đọc hết sách tôi mơi hiểu hơn về sự sắp xếp này ( 3 quả đỏ và 1 quả xanh). Mỗi chương sách đều là một lần khai sáng tư tưởng, giúp bạn thêm tự tin tronng tư duy của mình.
Nếu bạn muốn biết mình đang là ai, đang làm gì thì dây có thể là một lựa chọn tốt đấy nhé! Nhưng sách có những câu rất triết lý nên đôi khi cũng khó hiểu và nhàm chán. 
"Sinh ra là một bản thể, đừng chết như một bản sao"
5
491902
2016-06-20 21:17:38
--------------------------
439829
9299
Mình biết tới cuốn sách qua group Cộng đồng mê đọc sách trên facebook. Mình thấy cuốn sách này được các bạn đọc phản hồi rất tốt ,mình quyết định đọc thử qua vbooks.Và mình quyết định mua sách giấy cho bản thân và để tặng bạn bè nữa. Đây quả thực là một cuốn sách rất tuyệt. Qua cuốn sách tác giả không chỉ nói về con người và những thực tại, những khó khăn xoay quanh hành trình được sống là bản thân của mỗi người. Nếu bạn đủ can đảm theo đổi con người thật của bạn thì tất cả những điều tốt đẹp nhất mà bạn mong muốn sẽ luôn bên bạn. Bên cạnh đó những bài học mà Chúa dạy cũng được đưa vào trong cuốn sách khiến cho mình cảm thấy phần nào hiểu hơn về Thiên Chúa Giáo. 
Cám ơn tác giả, cám ơn tiki, cám ơn dịch giả và cả nhà xuất bản nữa.
5
1210318
2016-05-31 20:48:21
--------------------------
401346
9299
Mình tìm thấy cuốn sách này tên Waka-một trang web bán ebook của nhiều cuốn sách. Nhưng mình muốn mua hẳn sách giấy cho nên đã lên Tiki tìm thử. Mình bị ấn tượng bởi lời dẫn ở bìa sau sách: những lời nói của các đồ vật và quyết định mua. Thực sự nó đã giúp mình tìm ra nhiều điều của bản thân mà mình đã lãng quên: mình là CHÍNH MÌNH và là DUY NHẤT! Hãy sống theo đúng tính cách của mình, đừng để ta "chết như một bản sao" của người khác, trong khi đó mình đã được "sinh ra" từ một bản thể!
5
537523
2016-03-20 15:06:47
--------------------------
399645
9299
Tôi vừa nhận sách trưa nay, cách lúc này 12 tiếng, tôi đã đọc xong quyển sách này. Nó rất thu hút, bởi sự nhẹ nhàng, gần gũi nhưng rất sâu sắc. Tác giả là một mục sư nên luôn nhắc đến chúa và niềm tin với chúa, nếu ai đó không cùng tôn giáo thì cũng hơi khó để hiểu để tin. Tôi cũng là một trường hợp như vậy, tuy nhiên điều này không cản trở tôi được vì sự cuốn hút của nhưng điều rất hữu ích mà tác giả nhắc đến. Nếu ai đó đang thấy thiếu động lực, cần tìm cho mình lý do để tin vào bản thân, để bắt đầu niềm đam mê của mình, hãy đọc nó!
Rất cảm ơn tác giả đã viết nên quyển sách này!!!

4
138991
2016-03-18 00:28:00
--------------------------
355786
9299
Sách bao gồm những phân tích theo hướng tích cực giúp cho người đọc cảm thấy lạc quan và có nghị lực hơn trong cuộc sống cũng như trong công việc.

Kết thúc mỗi phần, sách đưa ra một câu châm ngôn hay một lời nói mang tính chất là một lời nhắc nhở giúp người đọc có một cách nhìn nhận sâu sắc và lạc quan hơn về các vấn đề trong cuộc sống.

Ngôn ngữ viết đơn giản giúp người đọc dễ hiểu và có pha thêm triết lý của thiên chúa giáo.

Sách hay phù hợp với nhiều lứa tuổi, đặc biệt là những người nào đang trong lo lắng, thật vọng... thì nên đọc!

4
88523
2015-12-20 20:10:34
--------------------------
314054
9299
Quyển sách này đối với mình mà nói cũng giống như một quyển Kinh thánh dành cho cuộc sống vậy. Vốn là một mục sư nên tác giả John Mason đưa vào sách những Lời Chúa hay những lời rao giảng tin mừng để người đọc có thể nhận thấy sự dẫn dắt của Chúa trong cuộc đời mỗi người.
Khách quan mà nói, hạn chế của quyển sách này có thể là những bạn đọc không theo đạo Thiên Chúa có thể sẽ khó cảm nhận hơn. Vì quyển sách củng cố rất nhiều niềm tin vào Chúa và lời dạy của Ngài. Còn với mình là một người có Đạo dĩ nhiên là cảm thụ rất nhiều những lời răn dạy trong sách.
Nhìn chung quyển sách này rất hay và có ích. Như tác giả đã nói, tốt hơn hết sau khi đóng quyển sách lại, chúng ta hãy bắt tay vào hành động và sống một cuộc sống mà mình nên sống.
5
140300
2015-09-25 11:20:12
--------------------------
304820
9299
Vì tên cuốn sách từng là 1 đề bài kiểm tra văn của mình nên khi vừa biết có quyển sách này, mình đã rất háo hức đặt mua. Có lẽ do tuổi còn khá nhỏ nên mình có đôi chỗ chưa hiểu nhưng nói 1 cách tổng thể, rất "chất". Tác giả viết sách với 1 lối tư duy rất logic. Cách nhà văn lập luận, cắt nghĩa, phân tích, đưa ra những dẫn chứng rất phù hợp, hoàn toàn không mang hình thức giáo dục hay nội dung xa rời thực tế như 1 số quyển sách khác. Mỗi người sinh ra đã là 1 cá thể độc laapjkhoong giống với bất kì ai, vì vậy tại sao chúng ta lại phải là "bản sao" của người khác. Với tôi, ẩn sâu trong từng dòng chữ là hướng dẫn về cách sống thành công nhà văn gửi gắm đến tôi. Đương nhiên tôi nghĩ rằng khi đọc "Sinh ra là một bản thể, đừng chết như một bản sao", chúng ta cũng sẽ biết rằng mỗi ngườ nhận ra thành công được chỉ dẫn đó theo 1 cách khác nhau.
2
635432
2015-09-16 17:38:54
--------------------------
294820
9299
Sách rất hay, lúc đầu được vài người giới thiệu nhưng không thích cho lắm. Rồi mua về đọc, mình bên phật nên đọc cuốn sách của tác giả này toàn nhắc đến chúa với kinh thánh có phần hơi khó chịu, nhưng thực sự thì thông điệp mà tác giả mang lại tất hay. Nó ý nghĩa ngay từ cái tên cho tới cái bìa của quyển sách này. Đã đọc cuốn này 3-4 lần rồi mà vẫn không chán, sau mỗi lần đọc lại thấy mình có thêm động lực sống và cầu tiến để phát triển. Trở thành một bản thể độc lập chứ khôg giống bất kỳ ai khác cả. Nói chubg là sách rất hay
5
695478
2015-09-09 19:02:29
--------------------------
290331
9299
Sinh ra là một bản thể
Đừng chết như một bản sao
Sau những chuỗi ngày ups and downs, khi mà dường cả thế giới, vạn vật, cả những người thương và ngay cả tôi cũng muốn ruồng bỏ bản thân, tôi đã lủi thủi quay trở lại lớp học tiếng Anh trong vô vọng và mơ hồ. Khi trở về lớp học, như một điều may mắn, một tia sáng, một tia hi vọng hé ra cho tôi, khi Thầy giáo tôi không nhắc về những bài, những vở, Thầy không đưa ra lời khuyên nào được cho tôi cả vì Thầy không biết được những khúc mắc, những bế tắc của tôi. Và ngay cả tôi lúc đó cũng rất mơ hồ, buồn mà vô hình… Nhưng còn hơn cả bất kỳ lời khuyên nào mà Thầy có thể nói cho tôi, Thầy đã giới thiệu cuốn sách này – Sinh ra là một bản thể, đừng chết như một bản sao. Cuốn sách như là một hành trang cho tôi để bước tiếp, tìm lại vẻ đẹp tâm hồn, sức mạnh con tim mình. Hơn thế nữa, cuốn sách cũng như là một minh chứng, là chuỗi những bài viết về người Thầy giáo dậy tiếng Anh của tôi – Thầy AND – Thầy cũng đã vượt qua biết bao khó khăn, lạc lối và mơ hồ để bây giờ có thể khẳng định mình và tung tăng trong chuỗi những năm tháng tuổi trẻ của Thầy mà không phải của ai khác.
Cuốn sách này là đầy giá trị nội lực trong đó, giúp người đọc khai phá được tiềm thức, tiềm năng bản thân mình, và kéo mình về với những ước mơ, hoài bão, khát vọng và đam mê..để mình lại được là chính mình, mà không lẫn vào ai khác.

5
780708
2015-09-05 11:39:03
--------------------------
289846
9299
Tôi xin mở đầu bằng một lời cám ơn chân  thành tới tác giả cũng như nhà xuất bản đã cho tôi được tiếp cận với một cuốn sách hay và ý nghĩa với cuộc sống tôi như vậy.  "Sinh ra là một bản thể,đừng chết như một bản sao" là cuốn sách gối đầu giường,dạy cho tôi biết phải là một bản thể của riêng mình, khác biệt và nổi trội xuất sắc, chứ không thể là bản sao của một ai khác. Tôi có một góp ý cho tác giả cũng như nhà sản xuất là cuốn sách kích thước khá nhỏ, làm mất đi vẻ mỹ quan. 
5
569658
2015-09-04 22:09:29
--------------------------
288818
9299
Đầu tiên là về hình thức: Thiết kế bìa tuy đơn giản nhưng toát lên được ý nghĩa xuyên suốt quyển sách, mình cũng rất ấn tượng với chất lượng giấy in, giày dặn, mực in đậm nhìn rất đã mắt các bạn ạ =))). Theo mình thì giá tiền này là tụi mình được hời rồi =).
Tiếp theo là về nội dung: Do đây là một quyển sách do Linh mục viết nên lời lẽ rất chi là nhẹ nhàng và từng từ từng từ đều có ý nghĩa, mình khuyên các bạn nên đọc nhiều lần quyển sách này để nó ngấm vào suy nghĩ và hành động thường ngày nhé. 
5
780655
2015-09-03 21:59:10
--------------------------
280146
9299
Tôi rất thích đọc các quyển sách về triết lý nên không bỏ qua quyển sách này. Quả thật quyển sách trên cả mong đợi của tôi. Vì là một Mục sư nên lối viết của tác giả rất sâu lắng, không phải chỉ đọc 1 mạch là hiểu ngay được. Để hiểu hết sự thâm thúy của tác giả, cần đọc và nghiền ngẫm, đôi khi chỉ một câu ngắn cũng khiến bạn phải bật cười hoặc trầm tư suy nghĩ về cuộc đời mình. Giá thành quyển sách lại khá rẻ, đây là một quyển sách rất đáng đọc!
5
151189
2015-08-27 14:08:31
--------------------------
270102
9299
Lần đầu tiên nhập cuộc đọc một cuốn sách mà tác giả là Mục sư. Cuốn sách này không phải là cuốn sách có thể đọc luôn tuồng 1,2 lần để chạm đến trang giấy cuối, mà nó là thứ để ở đâu đó trên bàn làm việc hoặc bàn học. Để mỗi ngày, ta lật ra dù chỉ một trang, đọc dù chỉ một đoạn ngắn, vẫn cảm thấy không bị mất liên kết hay lạc lỏng gì với cả cuốn sách. Những nội dung nhỏ trong sách đều được cất giữ và thể hiện thông qua những câu từ đơn giản nhất, ngắn gọn nhất sao cho người đọc có thể ghi chú ý này qua ý khác mà vẫn hiểu được sự liên quan giữa những mục với nhau.
Cũng khá thích cái bìa, rất đặt biệt và lột tả chân thực.
Cảm ơn Tiki.
4
67102
2015-08-17 22:51:49
--------------------------
221160
9299
Sinh Ra Là Một Bản Thể, Đừng Chết Như Một Bản Sao có một tựa đề rất hay. Ban đầu chưa đọc mình chỉ nghĩ đơn giản "ừ, chắc nó khuyên mình sống thật với bản thân mình", Nhưng đọc rồi mới biết, để đạt được điều đó quả thật không đơn giản, và cuốn sách còn mang lại nhiều ý nghĩa hơn thế. 
Với cách trình bày khá gọn với rất nhiều phần cho bạn đọc và chiêm nghiệm, ở đó tôi đã học được rất nhiều thứ. Phần một, tác giả dành nguyên chương này để giúp người đọc định hướng lại suy  nghĩ của bản thân, mục tiêu, sự sợ hãi, cách sống và rất nhiều thứ khác giúp bạn trở nên có nghị lực và sống tốt hơn. Phần 2 tác giả cho ta cách để ứng xử, xử lý các tình huống để kết nối với những người xung quanh...Nói tóm lại đây là một cuốn sách khá hay.
Bìa sách cũng rất đẹp, chất lượng giấy rất tốt.
4
37970
2015-07-03 15:30:32
--------------------------
172087
9299
Một cuốn sách rất hay và ý nghĩa, mang lại cho ta nhiều bài học quý giá, từng câu từng chữ rất sâu sắc giống như một tuyển tập những lời khuyên tuy nhiên đọc lại không hề có cảm giác đứt quãng, chung chung mà theo liền mạch của tác giả khiến nội dung của nó đi sâu vào lòng người. Mình rất thích cuốn này, nó mang lại những chuẩn mực tốt đẹp mà không quá lung linh hão huyền, hoàn toàn có thể cố gắng và trải nghiệm được, mình đã chắt lọc được rất nhiều những câu văn mang lại động lực cũng như cách để xây dựng cuộc sống từ quyển sách bổ ích này.
5
544333
2015-03-23 13:04:33
--------------------------
152695
9299
Quyển sách nhỏ nhắn, nội dung tinh gọn, rất đáng để đọc và chiêm nghiệm. Tập hợp những lời chỉ dẫn để bạn nhìn vào bên trong để biết mình muốn gì đồng thời   nhìn ra bên ngoài và nhận ra điều gì đúng, điều gì sai, tất cả mục đích để bạn nhận ra "Mình là một người đặc biệt và duy nhất, được Chúa tạo cho một giá trị nào đấy". Một điều hơi khó để cảm thụ cuốn sách là, xen rất nhiều câu trong Kinh thánh và hình ảnh Chúa, sẽ bị cảm thấy khó chịu bởi những người vô thần.
Có một câu rất hay trong sách theo cảm nhận của mình là: "Hãy là chính mình- còn ai khác có thể là bạn tốt hơn bạn được chứ?"
4
201502
2015-01-23 21:51:42
--------------------------
133005
9299
Một quyển sách về chủ đề kỹ năng sống mà bạn có thể tìm đọc khi cảm thấy mất phương hướng trong cuộc sống, muốn tìm kiểm bản chất đích thực của mình, tìm một con đường riêng để bước đi thay vì chạy theo những người khác. Điểm trừ duy nhất có lẽ là tác giả là mục sư nên nội dung sách có phần hơi thuyết giáo và mang đến cảm giác lạ lẫm cho những ai theo một tôn giáo khác hay chủ nghĩa vô thần. Tuy nhiên, đây cũng vẫn là một quyển sách đáng đọc vì sách giúp bạn tự tin hơn, tập trung vào những thứ bản thân có sẵn để từ đó xây dựng nên những mục tiêu và cố gắng đạt được để có được thành công.
3
4398
2014-11-03 13:44:09
--------------------------
107781
9299
Mình đã bị hấp dẫn bởi cái tựa của nó bởi xã hội ngày nay con người ta sống dưới mặt lạ quá nhiều, để sống là chính mình thường khó được chấp nhận.

Cuốn sách đã nêu lên những triết lí về bản thân mỗi cá thể dưới góc nhìn của một người Cha trong Thiên Chúa. Nó tạo động lực cho mình khá nhiều về việc cứ là chính mình đi và quên cái đám đông đằng kia!

Nhìn chung thì cuốn sách khá hay nhưng nặng về triết lý nên cũng hơi khô khan.
4
25073
2014-03-11 15:23:31
--------------------------
73884
9299
Cảm ơn tác giả John Mason đã viết quyển sách này, cũng như cảm ơn dịch giả sách của Thái Hà Book.

Đây là quyển sách vô cùng ý nghĩa với tôi. Sách viết đúng :"Đọc một quyển sách không phải là tìm ra cảm hứng mới, mà là dám hành động mới" :)

Sách viết rất chi tiết, cụ thể, mỗi vấn đề chỉ được trình bày từ một đến hai trang.Tuy nhiên vẫn đảm bảo chất lượng về nội dung cần truyền đạt. Cứ mỗi một vấn đề thì tác giả John Mason lại lấy một dẫn chứng làm ví dụ. Rất hay :)

Các bạn hãy mau cầm nó trên tay nhé. Rất nhiều bài học bổ ích :)
5
50283
2013-05-10 14:09:19
--------------------------
70756
9299
"Sinh ra là một bản thể, đừng chết như một bản sao" một cái tựa mà cuốn hút mình ngay từ cái nhìn đầu tiên. Nói thật là nhìn xung quanh thấy nhiều người giỏi hơn mình đôi lúc cũng cảm thấy buồn và ước ao gì mình được như họ. Nhưng sau khi đọc cuốn sách này xong thì mình đã thay đổi quan điểm. việc gì phải trở thành giống như những người xung quanh mình chứ? Trong khi lại có nhiều người vẫn ao ước được như mình đấy thôi. Từ đ1o mình hiểu ra rằng trong cuộc sống không ai là nhất cả, có người mạnh mặt này có người mạnh mặt kia, mội người đều có những điểm gì đó riêng biệt để tự hào. Vậy thì trở thành người khác làm gì chứ? Hãy cứ cố gắng làm tốt nhất mọi việc mà bạn có thể làm là bạn sẽ cảm thấy hạnh phúc lắm rồi.
5
60663
2013-04-23 10:26:24
--------------------------
69969
9299
Tuy vẫn chưa cảm nhận được hết ý nghĩa của cuốn sách (có lẽ cũng bởi một vài sự khác biệt nhỏ trong lối suy nghĩ?!), nhưng với mình, đây là cuốn sách NÊN đọc, để mỗi lần gấp  lại, tự thức nhắc nhớ rằng TÔI LÀ AI; TÔI ĐẶC BIỆT và DUY NHẤT. 

"Sinh ra là một bản thể, đừng chết đi như một bản sao" là những câu chuyện nhỏ được lồng ghép khéo léo với những điều trong kinh thánh, những kinh nghiệm của chính tác giả; bởi thế những chia sẻ trong cuốn sách trở nên rất "đắt":  "Hãy giống như những đứa trẻ…chúng thích sự thay đổi". "Sự tập trung sẽ thay đổi tất cả mọi thứ",  "Một hành động có giá trị gấp một ngàn lần một ý định tốt đẹp"...  Thoạt nhìn, tưởng có vẻ đơn giản, nhưng nếu thử chọn lọc và thực hiện 1 trong những điều đó quả thực là thử thách khó khăn.
 
Mỗi chương trong cuốn sách, tư thân nó đã là những định hướng, động lực rõ ràng và những thông điệp từ cuộc sống (nhìn vào bên trong, nhìn ra bên ngoài, hay nhìn lên bên trên). Tất cả đều với 1 mục tiêu duy nhất - hướng đến sự THAY ĐỔI TÍCH CỰC. 

Bìa sách, mới nhìn vào sẽ không thực sự ấn tượng, bởi hình ảnh, màu sắc, sự sắp xếp... Nhưng khi đọc xong cuốn sách này, mình hiểu tại sao lại có 4 quả táo 3 đỏ 1 xanh như thế: Tuy đơn giản nhưng đặc biệt, và dám khác biệt. Và cũng bởi: "Bạn sinh ra là 1 nguyên bản, đừng chết đi như một bản sao"

Một người bạn mình nói rằng: "Mỗi lần cảm thấy nghi ngờ về những quyết định, hay cảm thấy mọi thứ trở nên khó khăn, cứ lôi ra mà đọc"; và sau khi đọc 2 lần cuốn sách này, mình cảm thấy đúng! Nó như 1 cú xốc lại tinh thần để tiếp tục cố gắng!

1 cuốn sách đáng đọc và chia sẻ!
5
5592
2013-04-18 18:21:02
--------------------------
69612
9299
Tôi đã muốn mua cuốn sách này rất nhiều lần rồi nhưng lần nào cũng là người "chậm chân" nhất.
Đúng như cái tên rất ấn tượng, phần mục lục cũng khiến người khác phải tò mò. Nếu bạn chỉ đọc một lần, bạn sẽ không thể nào "thấm" được hết ý nghĩa mà tác giả muốn truyền tải. Hãy dành cho mỗi ngày mới của mình một câu chuyện nhỏ, để có thể cảm nhận một cách sâu sắc những thông điệp thú vị ấy.
Tuy nhiên, với một người không theo đạo, việc tác giả hay trích các câu nói trong Kinh thánh ra khiến tôi hơi bị "khớp". Thế nhưng, phần nội dung cùng cách diễn giải dễ hiểu hoàn toàn có thể "san lấp" lại những thắc mắc, khó khăn trên.
Tôi đã không thất vọng khi mua cuốn sách này, và hy vọng rằng bạn cũng thế!
5
64568
2013-04-17 08:25:48
--------------------------
68978
9299
“ sinh ra là một bản thể đùng chết như một bản sao”- một tựa đề vô cùng hay và đầy ý nghĩa.
Nếu đã từng băn khoăn tự hỏi mình là ai ở giữa cuộc đời nay? Giá trị của mình ở đâu? Mình có đang sống đúng, có ích? Thì có nghĩa bạn nên đọc cuốn sách này. 
Mỗi một người sinh ra đều có một giá trị nhất định. Bạn không giỏi điều này? Chả sao, bạn sẽ giỏi chuyện khác mà khiến nhiều người phải ghen tỵ, chỉ có điều liệu bạn đã nhìn nhận ra được điều ấy. Cũng hay như tự đề vậy, Cuốn sách dạy ta hãy biết trân trọng những gì mình đang có, biết yêu thưong sống có trách nhiệm với bản thân, bớt tự ti đi mà hãy nhìn những gì ta đang có chứ không phải không có. Bởi vì có nhà văn trẻ đã từng nói” sự phong phú của muôn vì tinh tú trong vũ trụ cũng không thể bằng sự phong phú của hàng tỉ tỉ tế bào trong cơ thể bạn, tạo nên con người bạn”. Đừng để mình chết đi như một bản sao mà hãy chỉ trọn vẹn là bạn. Một ý nghĩa đầy tính nhân văn. Đọc và suy ngẫm bạn sẽ thấy được mình trong đó, như tôi. Và chắc chắn đây sẽ là quà tặng sinh nhật ý nghĩa tôi muốn dành cho những người bạn của mình 

5
62182
2013-04-14 08:55:57
--------------------------
68842
9299
Bình thường khi mua sách , điều tôi quan tâm đầu tiên là bìa đẹp . Nhưng cuốn sách này lại khiến tôi ấn tượng từ tên sách trước , "sinh ra là một bản thể , đừng chết như một bản sao" . Vâng , con người , chẳng ai hoàn hảo cả , và dĩ nhiên , cũng chẳng ai có thể giống ai cả . Mỗi sự hiện diện của ai đó đều có một ý nghĩa nhất định . Có thể ta cảm thấy thật tự ti khi thấy mình hoàn toàn vô dụng hay kém cỏi so với người khác , nhưng tôi dám cá sau khi đọc cuốn sách này , bạn sẽ thay đổi ý nghĩ . Bạn là bạn , tôi là tôi , họ là họ , không ai giống ai và không ai có thể đem so sánh với ai cả . Nếu cuộc sống này tất cả đều giống nhau thì nó sẽ nhàm chán đến mức nào ?
5
93491
2013-04-13 17:50:02
--------------------------
68721
9299
Tựa đề của cuốn sách khiến tôi cảm thấy rất ấn tượng "Sinh ra là một bản thể, đừng chết như một bản sao". Quả thật, con người trong cuộc sống phù phiếm quá, họ so sách rồi ghen tị. . Mỗi người rõ ràng sinh ra hoàn toàn khác nhau, sao phải so sánh với người khác. Bạn sống ở đời phải là chính mình, phải là điều gì đó đặc biệt, để khi bạn biến mất mọi người sẽ nhớ tới bạn. Tôi từng quen một người bạn, cô ấy không hề thích vòng tay và nhẫn, không thích tóc ngắn... vậy mà sau này chơi với một cô bạn sành điệu, cô ấy bắt đầu cắt tóc ngắn và đeo vòng tay và nhẫn giống cô gái sành điệu đó. Mỗi người là một bản thể, có một giá trị riêng, sao phải cứ phải tập tành giống người khác? Bạn hòa nhập không có nghĩa là bạn phải "hòa tan". Cuốn sách này thật sự phù hợp cho những ai muốn tìm ra cái riêng trong con ngươi mình. Đọc cuốn sách, có thể bạn sẽ nghĩ khác trong nhiều việc, có thể tìm ra bản thân mình. Tôi đã biết mình là ai, và tôi tin rồi bạn cũng vậy.
5
100693
2013-04-12 23:56:31
--------------------------
68701
9299
Mình đã đọc cuốn sách này, và quyết định phải mua thêm nhiều quyển nữa để dành tặng bạn bè vào những dịp đặc biệt.
Với mình, đây là một cuốn sách thật sự rất ý nghĩa, những điều bổ ích nó mang đến ảnh hưởng lớn tới cảm xúc, suy nghĩ của người đọc.  "Sinh Ra Là Một Bản Thể, Đừng Chết Như Một Bản Sao" , như chính cái tên thú vị của mình, cuốn sách mang đến cho ta niềm tin vào bản thân, dạy ta biết cách yêu thương, trân trọng bản thân, biết nhận ra giá trị của bản thân mình không hề nhỏ, không hề thua kém so với những thứ cao cả lớn la mà bạn vẫn hay ngước lên, trông thấy, và cảm thấy mình bé nhỏ.
Chúng ta chợt nhận ra rằng, giữa hàng tỉ người trên khắp hành tinh, bạn cũng như tôi, và biết bao những con người khác, ai trong chúng ta cũng đều đặc biệt, cũng đều đáng trân trọng, cũng đều có nhưng điểm mạnh mà người khác không thể có. vậy thì cớ sao ta lại tự ti, hãy tự tin lên, để phát huy điểm mạnh của chính mình, để cống hiễn cho cuộc đời những gì mà ta có thể,.
Có như vậy, ý nghĩa cuộc sống với mỗi chúng ta mới thấy sự trọn vẹn.
Mọi thứ vẫn cứ diễn ra đấy thôi, và lắm khi ta thấy thất vọng với chính mình, ta chợt chùn bước trước những sai lầm, vấp ngã của bản thân, ta tháy mình thật thừa thãi... vậy là ta sai rồi. ta hoàn toàn có thể làm nên những điều thật sự đặc biệt, bởi chính mỗi chúng ta cũng đã khác biệt lắm rồi...
Một cuốn sách hay :)
5
10984
2013-04-12 22:32:29
--------------------------
68472
9299
Có những thứ trong cuộc sống này, người ta cứ nghĩ nó chỉ tầm thường, nhưng thực chất, đó mới chính là thứ làm nên một cuộc sống hoàn thiện.
Ngay từ tiêu đề cuốn sách đã khiến cho chúng ta phải suy ngẫm. Mỗi con người đều được sinh ra là một ban thể, nhưng phải sống như thế nào đó để khi chết đi, thì thế giới cũng phải có thay đổi vì mình, chỉ nhỏ thôi cũng được, chỉ đủ để chứng minh mình không phải một bản sao, mà là một con người, một con người thực sự.
cuốn sách chứa đựng những triết lí khai sáng, đơn giản nhưng sâu xa, cách viết thú vị, lôi cuốn, thực sự giúp tôi tìm thấy một phần con người mình nên làm.
một cuốn sách hay!
5
101433
2013-04-11 23:16:25
--------------------------
68386
9299
Tôi được biết về cuốn sách này thông qua một cuốn sách khác mà tôi rất yêu thích. Đó là cuốn "Nếu biết trăm năm là hữu hạn". Tôi bị hấp dẫn bởi tiêu đề cuốn sách: "Sinh ra là một bản thể, đừng chết như một bản sao"
Chỉ một câu đó và nó đã chạm tới tâm hồn tôi bởi một lẽ rất đơn giản, đó chính là điều tôi luôn suy nghĩ: Tại sao tôi lại khác mọi người đến vậy, tại sao tôi không giỏi việc này hay việc kia giống như người này hay người kia, thật sự tôi là ai và có giá trị gì trong cuộc đời này?
Những câu trả lời dần dần được hé lộ qua từng mục của cuốn sách. Mọi chuyện đôi khi thật sự đơn giản và chính ta là người làm nó phức tạp lên khi chạy theo cuộc sống của những người khác và bỏ quên chính mình.
Tất nhiên câu trả lời cuối cùng về bản thân phải do ta tự tìm ra, nhưng với cuốn sách này, tôi tin thời gian các bạn tìm ra con đường tới đó sẽ được rút ngắn nhiều. Ít nhất, tôi đã có thể tự tin nói rằng, tôi là một bản thể riêng biệt, và tôi tự hào về điều đó !

5
71367
2013-04-11 18:21:21
--------------------------
