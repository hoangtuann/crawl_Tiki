252518
8429
Con trai thú vị lắm. Có thể có những đề tài mà sách đề cập ít xuất hiện trong những màn đối thoại giữa bọn con trai với nhau, nhưng nhìn chung đây cũng là một cuốn sách đầy thú vị cho những ai muốn tìm hiểu bọn con trai chúng tớ nghĩ gì và nói chuyện gì, và cho những phụ huynh muốn hiểu hơn về tâm lý con cái khi chúng đến tuổi ăn, tuổi lớn. Đọc cuốn sách không chỉ để cảm cái thú vị của nó mà còn có thể tăng thêm rất nhiều kiến thức bổ ích, mà thường ngày có thể rất ngán ngẩm khi được nghe. Thật sự rất thú vị.
4
187067
2015-08-03 18:49:26
--------------------------
