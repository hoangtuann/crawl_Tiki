327441
9114
32 truyện ngắn không dày về dung lượng nhưng lớn về nội dung, lớn chẳng phải vì truyện đề cập đến vấn đề gì sâu xa, cao siêu; những câu chuyện đều dung dị, gần gũi, nhẹ nhàng mà thấm nhuần những giá trị nhân văn, những bài học ý nghĩa. "Chiếc hộp tình yêu" được tác giả Hoàng Mai viết bằng văn phong trong sáng, vui vẻ và đầy nhiệt thành, đem đến cho độc giả nhiều cung bậc cảm xúc. Tôi chỉ góp ý một chút để đơn vị sách làm việc chuyên nghiệp hơn, đơn cử là phần giới thiệu sách trên Tiki, không có gì khác ngoài mục lục truyện, như vậy sẽ khiến độc giả bối rối khi lựa sách và cũng làm giảm giá trị sách rất nhiều.
4
6502
2015-10-27 21:19:28
--------------------------
