557197
8806
chữ in rõ nét, sách thơm mùi giấy mới, nội dung trăm cuốn như một @@ giao hàng nhanh nhân viên thân thiện.
4
5116868
2017-03-29 00:03:35
--------------------------
529537
8806
Kích thước: 19x26.5 (cm) 
Hình thức: Sách in rõ ràng, dễ nhìn, giấy in màu hơi tối nhưng vẫn rất dày dặn, phần dịch nghĩa có cả tiếng Anh và tiếng Việt tuy phần in chữ tiếng Anh hơi mờ (do bản gốc) 
Nội dung: Phiên bản mới dễ học hơn nhiều so với bản cũ (dù bản cũ cũng khá ổn) do có cả audio (có thể tải được trên mạng) giúp cho phần phát âm rất nhiều. Có nhiều hình minh hoạ đẹp cũng giúp nhớ từ lâu hơn. Các mục trong sách được phân chia rõ ràng và có mục lục chi tiết ở ngay đầu. Theo mình thì kiến thức khá nhiều và học được hết quyển một và nửa quyển này là xem phim hiểu khá ok rồi. 
Nói chung là với những người mới học tiếng Trung thì bộ giáo trình Hán ngữ này là ổn nhất so với các sách khác kể cả sách giáo khoa nha. Khuyên các bạn mua k nên chần chừ vì bộ giáo trình Hán ngữ này hay hết hàng lắm í. 
5
2182951
2017-02-21 23:16:47
--------------------------
468907
8806
Sách tiki giao rất đẹp. Mình rất vừa ý.
Mình đã mua quyển thượng và mua cả quyển hạ này . Cuốn sách thật sự rất tuyệt vời. Trang trí đẹp mắt, kết hợp Tiếng Anh và Tiếng Hoa và dĩ nhiên có Tiếng Việt. Từ vựng, đoạn hội thoại,..... đều rất hay và đẹp. Font chữ đẹp, to, rõ ràng, trông rất hoàn hảo. Chỉ có duy nhất 1 khuyết điểm là cuốn sách có khổ hơi to và tính chính xác hơn là cuốn sách in chữ cũng khá to.
Thường thì bộ giáo trình sáu quyển cũ học châm hơn rất nhiều so với quyển mới này cho nên khi học theo bộ mới này thực sự tiếp thu rất nhanh. Thiết kế đẹp, font chữ lớn rất dễ nhìn và dễ đọc. Còn có phụ trợ thêm tập viết và dịch đoạn văn, bài tập cũng đa dạng... Bạn nào theo học tiếng trung thì nên mua bộ này, chắc chắn nó sẽ giúp các bạn nhanh chóng tiếp thu và dễ dàng tự học tập. 
 Tóm lại, mình khá hài lòng với quyển sách này, nhờ nó mà công việc học tập tiếng trung của mình trở nên dễ dàng hơn rất nhiều.
5
191926
2016-07-05 19:28:49
--------------------------
468653
8806
Mình mua quyền này lúc nhận được hơi bất ngờ vì sách to, khổ lớn hơn mình nghĩ, những nhờ vậy thấy rõ ràng, in đẹp và chất lượng tốt. Tuy hơi nặng
Được tiki bỏ hộp lần này cứng cáp nên không bị cong sách.
Bên trong chương trình rất rõ ràng, không chi chít in sát nhau khó đọc như mình nghĩ mà chia ra rất đơn giản, dễ học, dễ take note bên cạnh. Nói chung bản này không kèm CD nhưng mình thấy rất ok và hài lòng. Các bạn có thể tìm bản có CD phía trên.
5
1026200
2016-07-05 11:50:10
--------------------------
448168
8806
 Đây là quyển thứ 2 của tập 1 trong bộ 6 quyển giáo trình hán ngữ:
Tổng thể: sách dày, in chữ rõ ràng, có tiếng việt, chữ phiên âm và chữ Hán được viết rất to. Sách được bọc bookcare, nhìn sạch đẹp. (Tuy nhiên quyển mình bị bong rồi)
Nội dung: sách chia làm các phần theo thứ tự, bài mới, từ mới, chú thích tiếng việt, ngữ pháp - ngữ âm mới và cuối cùng là phần luyện tập. 
Sách không kèm theo CD nên khi muốn nghe thì phải tải file nghe trên mạng về. Hoặc có thể mua quyển có CD sẽ tốt hơn.
5
1401160
2016-06-15 15:14:45
--------------------------
445193
8806
Mình mua quyển giáo trình quyển hạ này kèm vs quyển thượng 1 và 301 câu tiếng trung vì sợ hết hàng thì khổ , giáo rất ưu đãi hơn nhà sách , hôm rồi có đi dạo nhà sách thì thấy mua ở tiki thì giá hấp dẫn hơn hẵng , nhưng tiếc nỗi là hội thoại không dịch , giá như dịch thì mình cho hẵng 5 sao ,tiếc quá , quyển này không kèm theo cd lại tiếc hơn nhưng bù lại tiki bán giá ưu đãi , gói hàng cẩn thận , giao hàng nhanh là ok rồi ,sẻ luôn ủng hộ tiki lâu dài.
4
950970
2016-06-09 18:26:36
--------------------------
397412
8806
Chất lượng cuốn sách tiêu chuẩn và đảm bảo. 
Thật buồn là phần hội thoại rất nhiều nhưng không có dịch nghĩa bên cạnh, khó khăn cho người tự học. 
Sách ngoài nghĩa Tiếng Việt còn được giải thích bằng tiếng Anh, chúng ta có thể củng cố luôn ngôn ngữ phổ thông quan trọng chẳng kém tiếng mẹ đẻ này. Giáo trình Hán ngữ này còn hướng dẫn viết từng nét chữ Hán, nét nào trước nét nào sau. Nội dung của giáo trình cũng rất phong phú: hội thoại, từ mới, chú thích, ngữ pháp, bài tập... rất nhiều...
3
1169299
2016-03-14 21:53:48
--------------------------
391285
8806
Thường thì bộ giáo trình sáu quyển cũ học châm hơn rất nhiều so với quyển mới này cho nên khi học theo bộ mới này thực sự tiếp thu rất nhanh. Thiết kế đẹp, font chữ lớn rất dễ nhìn và dễ đọc. Còn có phụ trợ thêm tập viết và dịch đoạn văn, bài tập cũng đa dạng... Bạn nào theo học tiếng trung thì nên mua bộ này, chắc chắn nó sẽ giúp các bạn nhanh chóng tiếp thu và dễ dàng tự học tập. 
Tóm lại, mình khá hài lòng với quyển sách này, nhờ nó mà công việc học tập tiếng trung của mình trở nên dễ dàng hơn rất nhiều.
5
554636
2016-03-05 07:59:12
--------------------------
372800
8806
Mình mua cuốn này nhằm mục đích ôn luyện lại tiếng trung.
Hồi trước học trong trường thì nó xài bản trung-anh, đọc khó hiểu bỏ xừ. Tuy nhiên cuốn này nó dịch tiếng Hán-Nôm của mình, đọc rất dễ liên tưởng tới tiếng Việt của mình nên học rất dễ dàng (lý do chính mình mua cuốn này). Bạn nào đang học muốn tiếng trung thì mình khuyến khích bạn nên bắt đầu bằng bộ này.
Nói chung mình khá hài lòng về cuốn này nói riêng và nguyên bộ nói chung, cứ theo nó thì ko cần lăng xăng với mấy bộ giáo trình khác về tiếng trung (dành cho người mới học).
5
64494
2016-01-22 11:18:18
--------------------------
348691
8806
Mình đã mua cùng lúc hay quyển Thượng và quyển Hạ.
Mình đã may mắn khi tìm được quyển sách rất bổ ích cho những người mới muốn học ngữ pháp.
Trang trí được đẹp mắt. Font chữ rõ, có chổ để rèn chữ nữa. Phần  ngữ pháp được chỉ rõ ràng, dễ hiểu. Phần bài tập giúp rèn luyện thêm kĩ năng. Sách còn viết về phần Ngữ Âm, chỉ cách phát âm rất rõ ràng không khó. Kèm đó là hình ảnh minh hoạ. 
Mà quyển sách hơi to, không được tiện cho lắm. Nhưng chất lượng tốt.

4
604786
2015-12-06 16:09:25
--------------------------
338939
8806
Đây là quyển sách tiếng Hoa hay, trình bày rõ ràng, chú thích cụ thể. Được tác giả dịch từ bộ sách gốc của Trung Quốc. 
Quyển sách này là quyển số 2 trong bộ 6 quyển từ Sơ cấp đến Trung cấp, giúp người học từng bước tiếp cận với tiếng Trung.
Quyển sách có kèm cả CD giúp người học luyện tập cả kỹ năng nghe trong quá trình học.
Với những nội dung mà quyển sách mang lại thì đây là một công cụ hỗ trợ tối đa cho việc tiếp cận ngoại ngữ của người học, giá thành phải chăng.
5
53347
2015-11-17 09:04:49
--------------------------
267617
8806
Cuốn sách này thật sự rất tuyệt. Nội dung trình bày rõ ràng, bắt mắt và dễ hiểu, có nhiều hình ảnh minh họa. Phần giải thích ngữ pháp đầy đủ, có nhiều bài tập và cả phần tập viết. Một cuốn sách rất tốt cho người mới học tiếng Trung. Mình rất hài lòng về cuốn sách này vì nội dung bổ ích mà giá cả lại hợp lí đối với học sinh như mình. Rất đáng để bỏ tiền mua
5
751905
2015-08-15 18:18:28
--------------------------
166050
8806
Mình đã mua quyển thượng và mua cả quyển hạ này . Cuốn sách thật sự rất tuyệt vời. Trang trí đẹp mắt, kết hợp Tiếng Anh và Tiếng Hoa và dĩ nhiên có Tiếng Việt. Từ vựng, đoạn hội thoại,..... đều rất hay và đẹp. Font chữ đẹp, to, rõ ràng, trông rất hoàn hảo. Chỉ có duy nhất 1 khuyết điểm là cuốn sách có khổ hơi to và tính chính xác hơn là cuốn sách in chữ cũng khá to. . So với quyển thượng, nó chẳng hề thua kém đàn anh mình chút nào ! Bạn hãy mua ngay quyển hạ này để học tiếng Trung Quốc nhanh chóng hơn và khoa học hơn nhé !
5
298441
2015-03-11 20:06:00
--------------------------
