417390
9113
Mưu hèn kế bẩn nơi công sở thật sự là quyển sách hay. Tình cờ mình đi nhà sách có thấy quyển sách này mình liền tò mò đọc thử. Qủa nhiên sách vạch ra cho chúng ta thấy những trò đê tiện xảy ra thường ngày ở chốn công sở như ma cũ ăn hiếp ma mới, ỷ làm sếp rồi bạc đãi nhân viêm...v.v.... Sách gây ấn tượng với mình ngay từ bức hình minh họa do tác giả thiết kế. Ngoài ra trong sách còn chứa đựng nhiều bài học quý giá đáng để chúng ta làm gương và suy ngẫm.
4
509425
2016-04-16 22:01:08
--------------------------
343731
9113
Theo tôi sách chỉ dùng để tham khảo, đọc để biết thêm chứ nội dung sách chỉ phản ánh một khía cạnh nào đó thôi.

Nội dung viết rõ ràng, sử dụng "Dự án" để phân tích và nêu lên các kế nơi công sở thông qua từng nhân vật giúp cho người đọc chưa biết thì nay sẽ biết các kỹ năng nhỏ ở chốn công sở nhưng chỉ là một phần nào đó thôi và có thể dùng để tham khảo, biết để mà tránh.

Ngôn từ dùng đơn giản, văn phong viết như trong đời thực. Đọc sách để tham khảo!
3
88523
2015-11-27 06:15:50
--------------------------
294541
9113
Thật sự ban đầu khi tôi mua quyển sách này tôi hy vọng rất nhiều vào nó. Tôi nghĩ nó sâu sắc hơn, nó đưa ra nhiều biện pháp để đối phó với những con người đó. Nhưng thật sự tác giả chỉ dừng lại ở việc chia sẻ, dẫn chứng 1 số ít câu chuyện để người đọc tham khảo.Cách giải quyết không nhiều.Về nội dung thì vẫn thực tế với cuộc sống nhưng nó không bao hàm sâu cho tất cả nhưng tôi cũng hơi thất vọng khi đọc quyển sách này. Nhưng dù sao đọc để tham khảo thôi cũng là bổ sung thêm 1 phần kiến thức cho chúng ta.
4
225624
2015-09-09 14:12:32
--------------------------
292882
9113
Không ủng hộ các bạn trẻ mới ra trường đọc những quyển sách tiêu cực này, sách đưa ra cái nhìn bi quan về công sở và đó chỉ là cái nhìn một chiều. Ai đọc thì chỉ đọc cho biết thôi, đừng học hỏi và cũng đừng nghĩ công sở là nơi đầy cạm bẫy. Dù cho bạn có gặp "mưu hèn kế bẩn" thì nó cũng chỉ giúp bạn trưởng thành hơn, chứ không đánh gục bạn được đâu, nên chả có gì phải xoắn. Trên sách có ghi "Skill for success" nhìn như không đúng với những gì sách truyền đạt.
3
8808
2015-09-07 19:08:14
--------------------------
289392
9113
Không biết có phải là mình quá ngây thơ hay thật sự cuộc sống nơi công sở có như thế không, nhưng đọc những dòng sách này mình thấy bi quan quá. Cách viết dông dài, vòng vo. Cách sử dụng từ ngữ quá tiêu cực. không nhiều kiến thức cần thiết. Các bước giải quyết vấn đề đưa ra lan man, dài- dai- d… Tại sao những quyển sách “ skills for success” luôn khiến mình thất vọng. Từ “ Người giỏi không phải học nhiều” cho đến “ Mưu hèn kế bẩn nơi công sở”. Quyển sách này giữ lại tham khảo thì cũng được, nhưng những gì bạn nhận được không chắc đến 1/20 độ dày quyển sách chưa.
2
411440
2015-09-04 14:12:29
--------------------------
278959
9113
Tôi đã bị ấn tượng ngay từ lần đầu bắt gặp tiêu đề cuốn sách, và đúng là tôi đã không phải thất vọng. Tôi chỉ mới đi làm được 2 năm nhưng cũng “may mắn” nếm trải một vài chiêu trò của các anh chị đồng nghiệp. Và lời khuyên cho bạn là nếu muốn thắng trong các trò chơi chính trị nơi công sở thì bạn cần phải đủ bản lĩnh và đọc hết cuốn sách này. Đây là cuốn sách có kết cấu khá thú vị với một câu chuyện lớn nhưng lại được chia nhỏ thành nhiều mẫu chuyện nhỏ giúp người đọc có thể dễ dàng theo dõi được mạch văn, đặc biệt cuối mỗi trường hợp còn có các tip nhỏ đúc kết lại những gì đã đọc. Đối với các bạn sắp sửa làm việc công sở nên chuẩn bị cho mình những kỹ năng để tự bảo vệ.
4
14119
2015-08-26 12:45:31
--------------------------
261742
9113
Mình nghĩ đây là một cuốn sách đáng để đọc, dành cho những ai có mục tiêu gắn bó với môi trường công sở lâu dài bởi những mưu hèn kế bẩn được tổng hợp lại trong sách là những gì đã, đang và sẽ vẫn diễn ra, chỉ là nó được dấu bí ẩn ở trong nên đôi khi chúng ta không nhận ra được. Mình thích nhất ở chỗ bên cạnh đưa ra những câu chuyện, sách còn có phần để cho người đọc tự suy ngẫm và trả lời câu hỏi rồi sau đó mới là phần giải đáp âm mưu đằng sau. Quả thật, với một người vừa mới đi làm như mình thì vẫn còn có những âm mưu mình chưa lường trước được. Đọc thấy mất tinh thần quá nhưng cũng cần phải hiểu để biết mà tránh. :(
5
201296
2015-08-11 13:02:59
--------------------------
234872
9113
Dù đã đi làm hơn 15 năm qua các công ty và  phòng ban khác nhau, nhưng khi thấy tiêu đề sách, mình đã đặt mua ngay lập tức, vì thật sự từ trước đến giờ chưa mình thấy có cuốn sách nào viết về mảng đề tài này cả, dù là bản tiếng Anh hay tiếng Việt. Đọc nội dung sách thì mình phải công nhận là các bạn Alpha book rất chịu khó sưu tầm để tổng hợp các kiểu ứng xử nơi công sở này thành một cẩm nang có hệ thống. Nó giúp người đọc soi rọi đối chiếu lại thực tiễn trong môi trường làm việc, đồng thời đưa ra các gợi ý giải quyết tình huống. Với  thâm niên công tác chưa nhiều, không dám lạm bàn nhiều, nhưng mình phải công nhận các tình huống này đều có thật tại các công sở. Các đề xuất giải quyết đôi khi chưa phù hợp hết, nhưng quan trọng là người đọc phải biết kết hợp vận dụng một cách khéo léo thuyết  phục hơn. Các bạn chuẩn bị đi làm ở công sở hay những bạn đã đi làm lâu năm nên xem đây là một tài liệu luyện tập kĩ năng làm việc trong môi trường cạnh tranh khốc liệt nhé. Nội dung hay nhưng cách trình bày hơi khó đọc, mình nghĩ nên in khổ sách và khổ chữ lớn hơn, kèm thêm các hình vẽ cho thêm phần cuốn hút
4
521993
2015-07-20 16:58:17
--------------------------
230981
9113
Đây là một cuốn sách rất hữu ích đối với học sinh,sinh viên sắp ra trường hay chuẩn bị kiếm 1 công việc đi làm thêm ngoài giờ như mình. Làm ở văn phòng công sở tưởng chừng công việc sẽ rất nhàn hạ, lương cao nhưng bên cạnh đó còn nhiều vấn đề "mưu hèn kế bẩn" mà cuốn sách đã đề cập đến, cuốn sách sẽ cung cấp cho các bạn cách nhận biết ngay từ đầu những dấu hiệu để phòng tránh hoặc có thể "phản đòn" ngược lại. Văn hóa công sở không phải nơi nào cũng lý tưởng như những công ty lơn như bạn biết, cuốn sách thực sự cần thiết và hữu ích trong xã hội hiện tại, nơi mà lợi ích của con người luôn đc đặt lên hàng đầu :(
5
453531
2015-07-17 20:50:40
--------------------------
141934
9113
Tôi là người khá khắt khe trong việc đánh giá một quyển sách, nhưng quả thật với quyển sách này, tôi nghĩ cho 5 sao vẫn còn là khiêm tốn.
Ngoài lời tựa rất hấp dẫn, phong cách viết của nhóm tác giả cũng rất cuốn hút người xem, khiến cho câu chuyện trở nên mạch lạc và có nội dung, chứ không chỉ đơn thuần là đưa ra những lời chỉ dẫn sáo rỗng như những quyển sách dạy kỹ năng sống khác.
Mặt khác, những mưu hèn kế bẩn mà tác giả đưa ra cũng rất phù hợp với môi trường văn hóa làm việc tại những công ty liên doanh như tôi đang làm, điều đó cũng góp phần khiến cuốn sách trở nên thực tế và hấp dẫn, chứ không sáo rỗng hay xa rời như những quyển sách kỹ năng mềm khác.
Tôi nghĩ, các bạn trẻ, nhất là các bạn mới ra trường,  bắt đầu bước chân vào chốn công sở, rất nên mua một quyển để tham khảo, không phải để hãm hại người khác mà là để biết được những thủ đoạn hèn hạ mà tránh.
5
498605
2014-12-17 08:58:58
--------------------------
94291
9113
Nội dung sách rất hay và độc đáo. Trong nhiều tựa sách về kỹ năng sống, chủ đề của cuốn sách nổi lên bởi cái chất lạ, chất riêng rất thú vị mặc dù bản chất vấn đề mà sách đề cập lại không mới. Đọc sách để có cái nhìn đa diện về chốn công sở - nơi mà mọi người váy áo lượt là, tác phong lịch sự nhưng đằng sau đó là bao nhiêu mưu mô, toan tính, ganh đua, ghen ghét. Sách được viết súc tích, rõ ràng, phân tích sâu và đưa ra các quan điểm thẳng thắn, rõ ràng. Tuy nhiên, có đôi chỗ tác giả sử dụng câu hỏi tu từ có thể gây một chút khó hiểu cho bạn đọc trẻ nếu chưa nắm được mạch ý của câu chuyện. Nhìn chung, đây là một cuốn sách hay, nên đọc.
4
164859
2013-09-12 11:43:10
--------------------------
85402
9113
Nội dung của cuốn sách rất hay và thú vị.
Cuốn sách đưa ra những tình huống rất đặc thù và những cách giải quyết rất hợp lý.
Chỉ là, cách trình bày sách có hơi chán so với cái bìa. Đối với lượng chữ và những nội dung kiểu này thì nên có thêm những điểm nhấn như: màu sắc, hình ảnh minh họa (như ngoài bìa), ... như vậy sẽ làm người đọc đỡ thấy chán hơn.
Kết luận:
Thật tốt khi đọc cuốn sách này nhưng mỗi ngày chỉ nên đọc 1 chuyện thôi, như vậy tiếp thu sẽ dễ hơn, không bị ngán mà mắt cũng không bị hoa.^^.
4
96853
2013-07-05 05:28:19
--------------------------
70782
9113
Mưu hèn kế bẩn nơi công sở-mình nghĩ đây là một cuốn sách cực kì thiết thực và hữu dụng cho những ai mới đi làm cũng như đã làm lâu năm. Trong công sở chuyện hãm hại lẫn nhau tranh công đoạt lợi để được đứng ở vị trí cao hơn kiếm nhiều tiền hơn giờ không phải là hiếm. Đối với những người mới bắt đầu đi làm thì nó hoàn toàn có thể gây sock (dù bạn có chuẩn bị tinh thần trước đi chăng nữa, bởi cuộc đời đâu như là mơ), vì vậy hãy đọc cuốn sách này. Nó sẽ đưa ra các tình huống cụ thể và hướng dẫn bạn giải quyết sao cho hợp lí hợp tình mà không làm nguy hại đến ai. Hãy đọc nó để hiểu thêm về những gì cuộc sống tranh đấu mang lại nhá.
5
60663
2013-04-23 12:26:48
--------------------------
70746
9113
Có thể nói, với ít nhất 8 tiếng làm việc nơi công sở, bạn như đang sống trong một xã hội thu nhỏ với đủ mọi loại người cùng với đủ mọi âm mưu đấu đá và tình nghĩa. Thế nhưng, ở cái nơi mà quyền lợi con người dễ bị đụng chạm lẫn nhau ấy, giúp đỡ thì ít mà đá nhau thì nhiều. Có lẽ những bạn đi làm rồi sẽ cảm nhận được sự thật phủ phàng như thế và đôi khi phải thốt lên sao giống mình vậy. Những trò đâm sau lưng, đá đểu, làm con tốt thí mạng hay làm bước đệm cho người khác,v.v... luôn xuất hiện nơi xã hội thu nhỏ ấy. Sự tổng hợp của Alphabooks sẽ giúp bạn phần nào để nhìn ra và tránh được những cái bẫy đang giăng sẵn để chờ bạn nhảy vào. Tuy nhiên, dù ở đâu đi nữa thì cũng có người tốt kẻ xấu, đừng bao giờ đánh đồng tất cả. Chúc bạn là người sáng suốt và thông minh.
5
63376
2013-04-23 09:53:29
--------------------------
70570
9113
Mưu hèn kế bẩn, cái xấu xa luôn tồn tại quanh ta mà ta không hề trông thấy. CÓ lẽ là nó ở trong mỗi con người từ trái tim đến cái bộ phận khác trong cơ thể. Nó chất chứa trong chúng ta ngày càng nhiều và ta bắt buộc phải học cách kìm nén nó. Nhưng đôi lúc lại có những người không thể nào làm được điều đó và cái xấu phát sinh. Cho dù bạn có thích hay không thì chuyện này vẫn luôn lan tràn trong công sở của bạn. Và cả của tôi. Hay bất kì ai khác. 
Xong sau đó thì tôi lục tìm khắp các trang mạng. Kiếm tìm tất cả các cuốn sách nói về sự xấu xa, bẩn thỉu. Ừ. Rồ cái tên có thể gọi là hơi độc nhất này "Mưu hèn kế bẩn", chính nó kéo tôi đến bài viết này. quả thật tôi bị hấp dẫn bởi hình ảnh trên bìa của cuốn sách. Nó khiến tôi tưởng tượng ra thật nhiều thứ mặc dù tôi chưa đọc gì bên trong. Và tiếp đến là lật từng trang sách. Những tràng cười phát ra liên tục từ tôi. Tôi cười những cái xấu xa đó, cười là tại sao nó lại bị con người phát hiện và bị tôi phát hiện cơ chư. Tôi cảm thấy thật thú vị vì đây là cuốn sách tốt, tốt, trên cả tuyệt vời với tôi.
4
84996
2013-04-21 22:17:13
--------------------------
