238075
9978
Một chất giọng lãng mạn trong một cuốn sách lãng mạn. Một cuốn sách với từng mẩu chuyện hấp dẫn, rung động mà không kém phần giản đơn, gần gũi, sâu sắc. Cô gái với cái tên Ngọa Lan, với cái phần cá tính khép kín, riêng biệt lại phải đối mặt với từng nấc từng nấc cung bậc cảm xúc, như dày vò, như giằng xé, như lên thiên đường, như xuống địa ngục, như yêu, như hận nhưng tất cả đều chỉ làm cho con người ta mạnh mẽ hơn. Cuốn sách để lại cho người đọc nhiều cung bậc cảm xúc cùng những lắng đọng, suy ngẫm sâu sắc về cuộc đời, về tình yêu, và về cả cái cách con người ta đứng dậy sau mỗi lần cuộc sống "ưu ái" dành tặng ta cú hích.
5
616532
2015-07-22 22:37:28
--------------------------
55426
9978
Ngọa Lan là một cây viết thực sự tài năng, có một cá tính văn chương nổi bật không lẫn và đâu, nhưng những tác phẩm của cô vẫn hết sức gần gũi, dễ hiểu và chân thành. "Ngày hôm qua" là một cuốn sách như thế. Tác phẩm không đưa người đọc đến với những thế giới xa lạ, mộng mơ, mà vẽ nên một bức tranh chân thực về cuộc sống xã hội đương thời. Từng nhân vật được khắc họa rõ nét và sống động, đặt trong một bối cảnh vừa quen vừa lạ, với những tình tiết sáng tạo và giọng văn giàu cảm xúc. Tất cả đã làm nên một tác phẩm rất thú vị, hấp dẫn và lôi cuốn qua từng trang, để lại nhiều suy ngẫm buồn vui về tình yêu, cuộc sống. Tác giả đã thực sự chạm đến cảm xúc của mình qua tác phẩm này.
4
20073
2013-01-14 12:39:04
--------------------------
