381963
9264
Thật sự thì thất vọng sau khi đọc quyển này. Lúc đầu vì cảm thấy tựa sách tạo một ấn tượng khá mạnh mẽ với mình nên mới quyết định mua nó. Nhưng nội dung thì gây thất vọng thật. Giọng văn dài dòng, đôi chỗ kéo dài không cần thiết, cứ cảm tưởng như là nếu không không có cô hàng xóm nhiều chuyện thì những người ở trong ngôi nhà chim công sẽ vẫn duy trì cuộc sống như vậy cho tới cuối đời luôn. Một câu chuyện quá ư là hư cấu, đã vậy chất liệu giấy không tốt nên có vài trang bị lem nhem sang trang sau nữa chứ.
3
350180
2016-02-18 13:03:49
--------------------------
380868
9264
Số 31 đường giấc mơ đang nói tới mục đích chính của con người khi bất cứ ai cũng đều làm ảnh hưởng đến xuyên suốt lên họ , Melinda và Leah chính là sự hồi phục bản lĩnh giữa sự sống đầy cay nghiệt đã khiến họ mất đi lòng tin , sự trung thành với chủ nghĩa họ theo và tin về sự che chở của thế lực chính nghĩa , sẽ có lúc những nhân vật sống trong đó có thể đạt tới mơ ước mở ra trước mắt , điển hình là những người bạn chung cảnh ngộ cùng nỗ lực sống còn .
3
402468
2016-02-15 23:59:01
--------------------------
362641
9264
Khá thích quyển sách này
Về hình thức: bìa sách khá đẹp, bắt mắt và có hơi chút khó hiểu, tiêu đề cũng ẩn khúc "Số 31 đường giấc mơ"
Về nội dung: đúng là quyển sách thay đổi về niềm tin trong cuộc sống. Số 31 đường giấc mơ đem lại cho mọi người cái nhìn hiện thực trong cuộc sống và thay đổi cách nghĩ, cách sống trong mỗi cá nhân con người, Ban đầu định không mua nhưng nhìn tiêu đề hay và phần giới thiệu cũng khá hấp dẫn lai có sale tận 67% nên mua luôn. Quyển sách rất ý nghĩa
4
1003449
2016-01-02 13:10:48
--------------------------
271988
9264
Bìa sách khá đẹp và bắt mắt nên mình cũng háo hức khi đặt mua sản phẩm.
Mở đầu truyện không gây ấn tượng nhiều lắm nếu không muốn nói là khá nhàm chán. Phải đọc sau khi nhân vật ông già Gus qua đời thi mạch truyện mới bắt đầu có những diễn biến thú vị.
Tuy đây là truyện lãng mạn nhưng không phải dạng sến như nhiều tiểu thuyết khác mà có những chi tiết rất thực tế, gần gũi với đời sống, không có những đoạn miêu tả quá xa vời như trong mơ.
Truyện cũng nêu lên một thực tế xảy ra ở nhiều người là tình trạng sống mòn, chấp nhận thực tế dù không hài lòng với nó nhưng không có cố gắng vươn lên.
Đểm trừ duy nhất của tác phẩm theo mình là sự xuất hiện của  Steve, một người đàn ông quà hoàn hảo cho Melinda dù đây là nhân vật không mấy nổi bật.
3
3847
2015-08-19 17:16:09
--------------------------
249512
9264
Một câu chuyện cũng thuộc thể loại lãng mạn, nhưng khác với các câu chuyện thông thường (thường là xoay quanh chủ đề 2 nhân vật nam nữ cuốn hút vào nhau). Đây lại là tổ hợp nhiều câu chuyện khác nhau về nhiều cuộc đời khác nhau. Mỗi cuộc đời, mỗi số phận đều bắt đầu từ những mảng tối mà không ai muốn chạm vào, cũng chẳng ai cố gắng xóa bỏ đi, cứ để mặc đó và dần bị chính mảng tối đó nuốt lấy.
Có lẽ mọi ng có thể nghĩ Toby và những người trong căn nhà đó được Leah kéo ra ánh sáng, nhưng t lại nghĩ, đôi khi nó là do định mênh sắp đặt (thực ra là nhà văn sắp đặt, nhưng thôi, cứ sống trong câu chuyện đi), đôi khi bóng tối ám ảnh nó k lớn như mọi ng vẫn sợ, chỉ là tự bản thân chôn chặt đi mà thôi. Và chính Leah cũng được những người trong ngôi nhà đó kéo ra 1 thế giới mới, thế giới không an toàn như cô vẫn có, nhưng thú vị và sống động hơn nhiều...
4
102292
2015-07-31 22:37:15
--------------------------
115058
9264
Cảm nhận đầu tiên của tôi là cuốn sách mang đặc trưng phong cách kể chuyện của châu Âu (tôi đã đọc nhiều tiểu thuyết khác của các tác giả châu Âu). Tuy nhiên, có thể do tuổi đời còn trẻ nên tôi chưa cảm nhận hết câu chuyện. Đối với tôi, câu chuyện này có những điểm nhấn riêng, làm cho độc giả cảm động, ví dụ như chuyện tình cảm của 2 nhân vật trẻ tuôi. Tuy nhiên, tôi thấy các tình tiết hơi dài, tập trung kể lể nhiều chi tiết nên có phần hơi loãng.
Bản in có nhiều lỗi chính tả, nhất là những chương giữa. Tôi là độc giả khó tính nên rất kỵ lỗi này. Hi vọng nhà xuất bản sẽ chú ý hơn trong khâu biên tập để các sản phẩm tiếp theo được tốt hơn.
3
40797
2014-06-22 10:51:57
--------------------------
106709
9264
Trước tiên, bìa sách đã gây ấn tượng với mình bởi sự mơ mộng của nó. Cách trình bày bên trong cuốn sách cũng rất tuyệt. Mới lật qua, mình đã tưởng đây sẽ là một câu chuyện tình yêu đầy lãng mạn của những con người trẻ tuổi xinh đẹp.

Chẳng ai ngờ đây lại là câu chuyện tình của một nhà thơ đã tứ tuần và cô hàng xóm của anh ấy. Toby và Leah đều là những con người kì lạ. Toby chẳng hề ra dáng một nhà thơ. Anh luộm thuộm, ít nói. Nhưng anh quan tâm đến người xung quanh hơn bất cứ ai. Leah lại rất tự chủ, độc lập, bản lĩnh và tinh tế. Điều gì đã đưa hai con người vốn là hàng xóm đã ba năm liền mà chẳng hề nói với nhau câu nào bỗng dưng đến với nhau? Chẳng điều gì cả. Đó là một sự tự nhiên. Một tình yêu rất nhẹ nhàng, rất giản đơn nhưng lại rất hấp dẫn.

Nhưng cuốn sách không chỉ là câu chuyện về tình yêu. Nó còn là câu chuyện về những con người chưa bao giờ sống cho thực sự, cho đúng nghĩa. Joanne, Ruby, Connor và Melinda, mỗi người đều có một số phận, hoàn cảnh, một tính cách riêng. Nhưng ai cũng có những mệt mỏi, đau khổ. Toby cũng vậy. Thật may rằng cuối cùng, họ đã vượt qua được những nghiệt ngã đó, kết thúc chuỗi ngày vô nghĩa và bắt đầu sống. Sống một cách có lý tưởng, có ước mơ, và có hạnh phúc.

Riêng với Toby, hình như Leah chính là một trong số những động lực để anh bắt đầu sống thật với con người mình.

Một cuốn sách nhẹ nhàng, mộc mạc, nhưng rất tuyệt vời.
4
24661
2014-02-23 12:48:50
--------------------------
94889
9264
Mình được biết đến tựa sách này qua mục giới thiệu sách trên tạp chí mình hay đọc. 
Đọc được mấy trang mở đầu mình thấy hơi chán. Khi cái báo tử đột ngột của cụ Gus đến, câu chuyện bỗng dưng mới mẻ hẳn. Đã có 1 thời gian mình ở vào tình trạng của Toby, nên có thể hiểu được sự khó tiếp xúc của Toby với 1 ai đó dù cho bạn ở rất gần họ. Mình thật sự thích tính cách của Leah. Chính cô đã thổi niềm tin yêu vào cuộc sống cho Toby, mang Toby quay trở lại với cuộc sống, thoát khỏi vũng lầy ký ức đơn độc mà anh tự tạo nên. Leah và Toby, có lẽ là minh chứng cho câu nói " những người trái ngược nhau thì lại càng yêu nhau". Tình yêu của họ thật dịu dàng và ngọt ngào. Đôi lúc con người bạn tò mò với thế giới xung quanh nhưng bạn lại ngại chạm tay vào nó để khám phá.Bạn chỉ đứng nhìn xa xa ở một góc khuất nào đó thôi, vì sợ người khác biết rằng bạn theo dõi họ. Mỗi con người trong căn nhà số 31 ấy đều có những ước mơ cho riêng mình, thời gian làm cho họ phải chạy đua với cuộc sống đồng thời cũng quên bẵng đi ước mơ ấy. Chúng ta tìm thấy mình trong truyện, chúng ta nuôi dưỡng ấp ủ nhiều hoài bão và hi vọng khi còn trẻ đến khi trưởng thành thì lại dần quên đi chúng. Leah và Toby đã khơi dậy những ước mơ cũ kỹ của những con người lạc lối và cô đơn ấy. Kết thúc truyện thật ý nghĩa khi tất cả họ đều tìm lại chính mình, đều có mục đích sống và đặc biệt là Leah và Toby cùng sống với nhau như họ từng mơ ước. Câu chuyện thật tươi sáng và lãng mạn đúng với cái tên "Số 31 Đường Giấc Mơ". Khép lại câ...
4
70419
2013-09-19 14:44:39
--------------------------
91302
9264
Đây là một cuốn sách vô cùng nhẹ nhàng mà đầy ý nghĩa. Người đọc sẽ bị cuốn theo cái trầm lặng của London, cái trầm lặng của những nhân vật trong này. Có lẽ sẽ chẳng có câu chuyện nào cả nếu ông cụ Gus không ra đi và được Leah bắt vào sáng hôm ấy. Chính cái chết của cụ đã khơi dậy tất cả, khơi dậy ước mơ của Toby cũng như những khách trong nhà trọ ấy. Toby-sau 15 năm sống trong vỏ bọc của chính bản thân-đã phải tự đặt câu hỏi: Mình đã sống vì cái gì và mình muốn gì? Mỗi khác trọ trong ngôi nhà ấy cũng có những giấc mơ của riêng mình: Con muốn trở thành một phi công, Melinda muốn một cuộc sống hạnh phúc bên người đàn ông của mình, Joann muốn quên đi quá khứ đau thương, Ruby muốn trở thành một nhạc sĩ, ca sĩ nổi tiếng. Nhưng để đến với ước mơ ấy, họ đã phải trải qua thật nhiều khó khăn, do cuộc sống đem lại, thậm chí là do chính bản thân họ tự tạo ra cho mình. Nhưng rồi Leah đã đến và giúp Toby tìm lại bản thân mình, rồi sau đó còn đi giúp những người khác trong căn nhà ấy. Quả thật, cái cuộc sống mà họ đã từng sống, cuộc sống của sự thu mình, sự cô đơn, bế tắc, tuyệt vọng, ngày ngày trôi đi mà không có đột phá vô cùng đáng sợ. May thay, đến cuối truyện, dù chưa chạm tay hẳn được vào giấc mơ của mình nhưng những con người sống trong căn nhà ấy đã tìm lại được bản thân và con đường cần đi của mình, cố gắng xây dựng một cuộc sống tốt đẹp hơn. Chắc chắn rằng một ngày nào đó, những giấc mơ ấy sẽ không còn xa vời với họ nữa.
Nội dung truyện ý nghĩa như vậy nhưng mình lại không thích các dịch của Chibooks cho lắm. Có một số ngôn từ bị dịch quá cứng nhắc, gây khó chịu cho người đọc. Đây ko phải là cuốn duy nhất mà mình để ý thấy như vậy.
4
73676
2013-08-17 18:03:56
--------------------------
89466
9264
Cuốn sách này đem lại cho mình một cái nhìn khá mới lạ về cuộc sống hàng ngày của những con người rất bình thường.

Toby là một anh chàng kì lạ. Anh là một nhà thơ, nhưng mình lại không thấy anh giống những nhà thơ bình thường tẹo nào cả. Anh ít nói, khép kín, nhưng giàu tình yêu thương. Anh sống một cuộc sống phẳng lặng, cho đến khi Gus chết và bố anh thông báo sẽ về thăm. Như thế, anh đặt ra cho mình mục tiêu cao cả: BẮT ĐẦU SỐNG. 

Mình thích tất cả những con người được miêu tả trong cuốn sách. Ruby là một cô gái có phần buông thả, cô ấy không biết tự tìm lối đi cho cuộc sống của mình, chỉ dựa dẫm vào người khác. Joanne, lúc đầu mình cứ tưởng cô ấy chỉ là một cô nàng bình thường thích đua đòi và có phần dị hợm, nhưng đọc rồi mới biết hoá ra cô ấy có nỗi đau riêng. Cô ấy từng nghiện, từng vào tù, từng mất con, và cô ấy trốn tránh quá khứ. Melinda là một phụ nữ nhân hậu nhưng khốn khổ vì sự dày vò của lương tâm. Connor, đó là một chàng trai trẻ đầy sức sống, và tình yêu của Con với Daisy thật đẹp. Cậu cũng đã băn khoăn, cũng đã sợ hãi, nhưng cuối cùng cậu vẫn đến với Daisy bằng cả trái tim mình. Và Con có ước mơ, cậu dám thực hiện ước mơ ấy.

Về phần Leah, mình không hiểu lắm về cô gái này, nhưng có vẻ cô ấy rất thực tế, rất thông minh, bản lĩnh. Tình yêu của Toby và Leah nảy nở một cách hoàn toàn tự nhiên, đến mức mình không biết họ đã yêu nhau từ lúc nào và bằng cách nào. Nhưng họ yêu nhau chân thành thật. Và mình thích điều đó.

Cái kết rất tuyệt. Mỗi người đều tự tìm được cho mình hạnh phúc. Ruby bán được bài hát của mình, Joanne trở về với Nick, Con thực hiện ước mơ với Daisy bên cạnh, Melinda đến với Jack, và Toby cùng Leah bắt đầu sống cuộc sống của riêng họ.

Thật nhiều bài học chứa đựng trong cuốn sách! Bài học về tình yêu, tình bạn, tình yêu thương, về những vấp ngã trong cuộc sống, về niềm tin và ước mơ...

Một cuốn sấch rất đáng đọc!
4
126218
2013-08-02 08:13:12
--------------------------
75939
9264
Mình thích cách vào truyện của tác giả, có một cái gì đó thông minh, dí dỏm, hài hước nhưng không hề thiếu cảm xúc, thích cách tác giả mô tả cảm nhận của ông bố về Toby  - đứa con có phần hơi cá biệt, thích cách ông để lại cho Toby căn nhà như tài sản thừa kế, thích cách Toby đăng tin cho thuê nhà, thích cả cách hồi đáp của các nhân vật sau này đến sống cùng anh. Để rồi tất cả những con người đặc biệt đó - hội tụ tại căn nhà Số 31, tất nhiên theo mình đó không phải những con người kỳ dị mà họ chỉ đang ở 1 đọan loạng choạng trong cuộc đời, bởi họ đều vừa trải qua 1 cú sốc. Cú sốc làm họ sợ, họ co lại, họ 'thủ",  họ không phải đang sống mà đang cùng nhau tồn tại, họ thực sự cô đơn giữa trốn đông người. Để rồi khi xảy ra biến cố, họ nhận ra rằng họ cần phải tiến lên để tìm lại hạnh phúc cho bản thân và thực hiệc giấc mơ mà họ hằng ấp ủ. Tất cả tuyến nhân vật chính từ Toby, Con, Melinda, Joanny, Ruby đều có 1 cuộc sống tốt đẹp hơn. Song mình thích hình ảnh gia đình Toby hơn cả. Đọc xong truyện thấy hài lòng và đi vào giấc ngủ 1 cách nhẹ nhàng. 


4
106757
2013-05-20 17:08:00
--------------------------
73926
9264
Một tác giả mới, một câu chuyện mới, nhẹ nhàng, không quá phức tạp nhưng vẫn rất cuốn hút người xem. Ngôi nhà số 31 đường Giấc Mơ là nơi chấp cánh cho những ước mơ của từng con người sống trong ngôi nhà này, mỗi con người với mỗi mảnh đời khác nhau, tất cả đều như những mảnh ghép hoàn thiện cho bức tranh của cuộc sống.
Đọc truyện ta có thể nhìn thấy hình ảnh của mình trong số họ, một phần nào đó trong cuộc sống của chính ta cũng giống như vậy. Toby là người chủ nhà tốt bụng đã cưu mang họ, có thể thấy ngôi nhà số 31 như một mái ấm yêu thương nhưng cuộc đời không phải chỉ cứ chôn chân ở một nơi như thế, họ cần phải ra đi để xây dựng tương lai cho chính mình. Chính sự tác động của Leah, cô gái hàng xóm đã thúc đẩy Toby, giúp anh nhận ra anh cần phải làm gì cho chính tương lai của anh cũng như cho những người thuê nhà. Lâu nay Toby chỉ làm trọn trách nhiệm, bổn phận của một người chủ nhà mà chưa bao giờ thực sự quan tâm, tìm hiểu đến họ cho đến khi Leah bước vào cuộc đời anh, nhờ cô mà anh đã hiểu thêm về những người xung quanh và cũng qua đó, một tình cảm mới đã chớm nở, suốt thời gian dài ngủ yên nay trái tim anh đã lên tiếng trở lại. Một mối tình dịu dàng, êm đềm đến từ cả hai phía giúp họ nhận ra cuộc đời này còn nhiều ý nghĩa biết bao, sau khi những người bạn cùng nhà ra đi để chắp cánh cho ước mơ của mình thì Leah & Toby cũng tiếp bước điều đó, cô và anh cùng ra đi, cùng xây dựng cho mình một tương lai mới, một cuộc sống mới hạnh phúc hơn với biết bao những dự định phía trước.
Ngôi nhà mà L.Jewell miêu tả như một nơi để ươm mầm, nuôi dưỡng những hoài bão, những khát vọng của con người và qua đó là động lực để giúp họ vào đời tìm kiếm tương lai, hạnh phúc cho riêng mình.
Một cuốn sách hay và thật sự nhiều ý nghĩa.
4
41370
2013-05-10 19:05:26
--------------------------
62917
9264
"Số 31 đường giấc mơ" là cuốn sách đầu tiên mình được đọc của Lisa Jewell những đã cảm thấy vô cùng thích thú và lôi cuốn bởi giọng văn giàu cá tính của tác giả này. Mỗi nhân vật trong cuốn truyện mang trong mình một ước mơ, một khát khao cháy bỏng, dường như họ chính là đại diện cho mỗi chúng ta. Xuôi theo những trang sách, người đọc như lật mở được những tầng sâu kín trong nội tâm nhân vật, khám phá ý nghĩa đích thức của cuộc sống. Cuốn truyện như một bài học sâu sắc mà tác giả muốn gửi gắm, một bài học về cách sống, cách đối diện với mọi sự xảy đến với mình. Vừa giúp thư giãn nhẹ nhàng, vừa giúp người đọc có được những chiêm nghiệm tuyệt vời, cuốn sách này thực sự không nên bỏ qua.
4
20073
2013-03-12 20:00:47
--------------------------
58420
9264
Thật sự, cuốn sách cuốn hút mình qua câu chuyện ở căn nhà - nơi dừng chân của giấc mơ. Mỗi khách trọ trong căn nhà 31 đều có ước mơ riêng. Ruby muốn trở thành ca sỹ nổi tiếng, Joane lại muốn chạy trốn lỗi lầm quá khứ. Ban đầu, Connor chỉ tìm 1 nơi trú ngụ nhưng ở đây cậu đã tìm cho mình 1 ước mơ và tình mẫu tử ngỡ như đã mất. Bà mẹ của Connor, Melinda tìm được giấc mơ của đời anh - người đàn ông của đời mình. Mà theo như Melinda "đã quá muộn để đi tìm lại chính mình". Leah đi tìm lại niềm vui sống, cứ "đơn giản là chính mình" đã lôi cuốn Toby để từ đó "bắt đầu sống". Mục tiêu cuối cùng anh đề ra đã đạt được. Câu chuyện tưởng chừng chạm đến đích đến thì đột ngột xảy ra rắc rối nhỏ do Ruby gây ra. Kết thúc câu chuyện là thành quả cho những người nỗ lực đi thực hiện ước mơ và sự trả giá cho sai lầm mình đã gây ra. Mỗi người khách trọ là 1 câu chuyện đời tư riêng mà nhờ Leah, Toby đã khám phá ra. Dường như, trước đây anh đã quá vô tình khép kín nội tâm bản thân quên đi bản thân cũng như lãng quên chính những người khách trọ. Toby là nhân vật chính nhưng Leah mới chính là nhân chất chủ chốt dẫn dắt câu chuyện. Từng người qua sự khám phá và giúp đỡ của Leah đã tìm được niềm vui đích thực của đời mình, Câu chuyện với những thân phận khác nhau nhưng gắn liền với nhau bởi chung 1 căn nhà ước mơ. Mỗi người đều có tính cách và hoàn cảnh riêng mà ta dễ bắt gặp trong cuộc sống. Cuốn sách này gần như nội dung của cuốn "Thiên ý". Số phận người đan xen nhau tưởng như rời rạc nhưng lại có mối quan hệ chặt chẽ và liên quan đến nhau. Rất hay và ý nghĩa!
5
14561
2013-02-05 00:18:01
--------------------------
