427154
9444
Mình mua sách này về cho em mình. Chất lượng giấy tốt và hình rất đẹp, mình rất hài lòng. Tuy nhiên thì sách phù hợp nhất là cho lứa tuổi từ lớp 3 trở lên, hơi khó tô đối với các em nhỏ hơn ( em mình học lớp 1 và nó bảo không thích vì khó tô lắm ) nên các bậc phụ huynh cần cân nhắc trước khi mua. Dù sao cũng không thể phủ nhận được chất lượng của sách và giá thì hợp lí, có thể sử dụng để làm quà tặng sinh nhật rất phù hợp. ( Nhận xét này của mình cũng đồng thời  là nhận xét chung cho cả bộ sách Tô Màu Công Chúa.)
5
451274
2016-05-08 15:07:25
--------------------------
344872
9444
Hình ảnh đa dạng phong phú , màu sắc hà hoà , đẹp mắt , thu hút mọi lứa tuổi đặc biệt là các bạn nhỏ và cả người lớn nữa . Tuy nhiên giấy hơi mỏng và không thể dùng màu nước được mà phải tô bằng màu sáp, dầu . Gía cả hợp lí ,  không quá đắt . Các bạn học sinh nhỏ tuổi sẽ rất thích và thoã sức tung tăng với những hình vẽ đáng yêu này . Thật sự rất có ý nghĩa khi làm quà sinh nhật cho các bạn nhỏ dễ thương .
5
860686
2015-11-29 11:49:08
--------------------------
