577485
8511
Một câu chuyện nhẹ nhàng. Nam lẫn nữ chính đều không quá nổi bật, và cả hai đều không phải hình mẫu lí tưởng của nhau. Nhưng sau tất cả những khó khăn, họ đã tìm được tiếng nói chung của hai con tim, ở bên nhau mãi mãi. Truyện không đặc sắc lắm, đọc để thư giãn thì được. Nếu được mua lại, chắc mình sẽ không mua
4
205707
2017-04-19 12:51:26
--------------------------
516577
8511
 Hai con người đều không lung linh đẹp đẽ hay tài giỏi nổi tiếng như bao cuốn tiểu thuyết ngôn tình khác, nhưng chính số phận đen đủi và những tình huống hài hước của hai nhân vật chính đã là điều cốt lõi để cuốn sách cuốn hút người đọc.
 Cuốn sách này mình đã dành cả một buổi tối để đọc và mình không hề thấy hối hận.
 Mình đặt cuốn này cùng nhiều cuốn khác nhưng tiki giao hàng rất nhanh. Hẹn 4,5 ngày sau mình mới nhận nhưng ngay ngày hôm sau Tiki đã đến giao hàng rồi. Anh nhân viên cũng rất vui vẻ và thân thiện, thái độ làm việc rất tốt.
 Ủng hộ cho 5 sao:)
5
1679082
2017-01-30 13:20:33
--------------------------
457871
8511
Mình cũng từng đọc một cuốn của "Tuyết Ảnh Sương Hồn", tên là "Gặp anh giữa hàng vạn người". Sau quyển ấy thì mình cũng quên hẳn tác giả này luôn vì thật sự cốt truyện rất nhạt, hầu như không để lại một chút ấn tượng gì trong lòng mình. Truyện kiểu như thế này mình đọc rất nhiều, và cuốn nào cũng bỏ lửng vì đoán ra tình tiết hết trơn. Mình mua cuốn này không để ý tác giả lắm, chỉ vì thấy tên hay hay và cái bìa cũng hay nên mua. Dù vậy, cuốn này cũng không tăng thêm cảm tình của mình cho tác giả. Khá nhạt.
3
452822
2016-06-24 17:00:54
--------------------------
450470
8511
Tôi đã từng đọc Luôn có người đợi anh, Gặp anh giữa hàng vạn người, Đông ấm hoa sẻ nở, và Ai là định mệnh của ai là cuốn thứ 4 tôi đọc của Tuyết anh sương hồn. Giọng văn chị đơn giản, thực tế, cứ như mình là nhân vật trong chuyện vậy. Đọc truyện của chị tôi còn tưởng mình đâng xem phim vì các nhân vật cứ như hiện trước mắt tôi.
Tuy nhiên cốt truyện lần này có hơi dài dòng và chưa gây ấn tượng gì nhiều ngoài giọng văn hay của chị. Nhưng tôi vẫn thích vì cái cách diễn đạt diễn biến truyện ấy
3
368535
2016-06-18 21:21:39
--------------------------
437551
8511
Truyện này mình tậu được tại một cửa hàng chuyên bán ngôn tình cũ gần nhà. Bìa sách khá ổn. Nhưng nội dung thì chưa hấp dẫn lắm. Theo mình thấy thì là thế. Điểm đặc biệt mà mình thích nhất truyện chính là hai nhân vật nam nữ chính đều không lung linh như những câu chuyện ngôn tình khác. Nhưng nội dung thì hơi 'thiếu muối'. Câu chuyện mặc dù đề cao tình yêu cũng như giá trị nhân văn nhưng mạch truyền cứ đều đều, 'sóng yên biển lặng' thành ra gây nhàm chán. Truyện chưa có đủ cao trào. 





3
465267
2016-05-28 13:42:07
--------------------------
436640
8511
Đây là một quyển ngôn tình không hề có mỹ nhân hay soái ca gì hết, chỉ đơn giản là những con người bình thường, cuộc sống bình thường, gia cảnh bình thương, gương mặt bình thường, thậm chí có chút xui xẻo vô tình đưa đẩy đến nhau, một câu chuyện hài hước và có chút thực tế (thật sự thì ở ngoài cũng khó kiếm được một mối tình như thế), đây là quyển tui ưng ý nhất của Tuyết Ảnh Sương Hồn, vì nó hài, nó nhẹ nhàng và nó không máu chó (dân sủng truyện ngọt).
Nghĩ rằng ai fan ngôn tình cũng nên đọc thử vì nó cực kì hay đó.
4
936775
2016-05-26 23:31:13
--------------------------
425586
8511
Truyện này hài hước và dí dỏm làm mình cười hoài. Dù là mấy phần đầu hơi chán nhưng vài phần sau là cực kì dí dỏm, nhất là khi hai nhân vật chính đến với nhau. Dù không phải là trai xinh với gái đẹp nhưng vẫn đậm chất ngôn tình. Hoàn cảnh giữa Bé bự và chàng trai thấp hơn mực nước biển là hoàn cảnh của nhiều người nên khi đọc cảm thấy rất thực tế. Đưa ta về hiện thực không mơ mộng như những truyện ngôn tình khác. Hai con người ấy là người rất bình thường và câu chuyện là chuyện tình giữa hai con người bình thường ấy và câu chuyện cũng rất là lãng mạn nhất là ở những phần cuối cùng của câu chuyện còn ở mấy phần đầu thì hơi chán.
3
1276622
2016-05-04 23:35:59
--------------------------
396914
8511
Giữa một rừng tiểu thuyết ngôn tình với nội dung Lọ lem - hòang tử thì đây thực sự là một quyển tiểu thuyết đặc biệt. Nội dung gần gũi, không rập khuôn theo mô tuýp cũ rích, hai nhân vật chính là những con người hết sức bình dị. Họ cùng yêu, cũng trải qua nhiều mối tình với những bạch mã hoàng tử và tuyệt thế giai nhân. Nhưng cuối cùng nhìn lại, người phù hợp nhất với mình vẫn luôn ở ngay bên cạnh. Cô nàng Bé bự và anh chàng "thấp hơn mực nước biển" đúng là định mệnh của đời nhau, câu chuyện của họ mộc mạc nhưng rất đẹp, rất rung động lòng người! Đây là một câu chuyện tình yêu rất đáng để bạn trải nghiệm! :)
5
733970
2016-03-14 11:08:46
--------------------------
388770
8511
Mình thích mô típ cuốn truyện này. Không phải là soái ca ngôn tình đẹp trai tài giỏi, con nhà giàu, lạnh lùng boy, không phải nữ chính đẹp nghiêng nước nghiêng thành, giỏi giang khéo tay, hiền dịu thục nữ hay mạnh mẽ girl. Tính cách hai nhân vật khá đặc biệt, và trong hoàn cảnh cũng đặc biệt luôn. Các tình huống thì dở khóc dở cười, cũng khá đúng trong cuộc sống hiện đại. 
Và mình thích cái tình yêu xuất phát từ bạn thân như thế này, hiểu rõ con người, tính cách đối phương, không cần giữ ý nhã nhặn.
Nói chung cuốn sách này khá hay về nội dung, nhưng trừ 1 điểm về phần bìa vì mình không thích lắm, hơi đơn điệu.
4
602517
2016-02-29 17:19:05
--------------------------
376559
8511
Mình rất thích truyện của Tuyết Ảnh Sương Hồn nên khi thấy cuốn này và bìa sách được thiết kế khá đẹp mắt , mình đã không đắn đo mà mua luôn . Câu chuyện dù còn hơi hướng ngôn tình nhưng cũng khá thực tế . Cả 2 nhân vật chính chẳng ai là hoàn hảo kiểu soái ca thục nữ ngôn tình cả . Mà chàng thì vấn đề về chiều cao , nàng thì vấn đề về cân nặng . Cả Phiên Phi và Nhất Minh đều muốn tìm một nửa quá hoàn hảo mà quên mất xem yếu tố có hợp với mình hay không nên đã dẫn đến nhiều tình huống dở khóc dở cười .
4
743273
2016-01-31 09:41:59
--------------------------
371005
8511
Khi gấp lại cuốn truyện này thì tôi cảm thấy đây là câu chuyện khá hài hước dí dỏm và không kém phần dễ thương. Ai là định mệnh của ai thì đây là câu chuyện về công cuộc tìm kiếm tình yêu của những nhân vật rất thực với đời sống. Có lẽ câu chuyện này khác biệt với những câu chuyện ngôn tình thời nay là do tác giả xây dựng hình tượng không cần quá đẹp đẽ giàu sang hay giỏi giang hay nhiều thứ khác đây là nhưng con người hoàn toàn bình thường, và chính số mệnh đã cho họ gặp nhau.
4
1062396
2016-01-18 20:15:40
--------------------------
367935
8511
Hạnh phúc là điều không thể thiếu , làm ta phản bội lại mọi người xung quanh mình được , cho nên dẫu sao khó khăn ta cần bươn trải hơn với đời để thấy thực rõ bản tính của mình còn chưa được minh xét gần nhất , hài hước với vấn đề gian nan , hết vận xui này sang vận xui khác đều làm ta thấy nhẹ nhõm như với chính mình , chỉ cần bất cứ điều sai lầm đều khiến ta phải quay ra hối lỗi hơn , truyện có khắc phục điểm yếu của mỗi người .
4
402468
2016-01-13 00:15:30
--------------------------
342034
8511
Khi mua cuốn này, mình rất phân vân vì cuốn đầu tiên mình đọc của Tuyết Ảnh Sương Hồn là "Gặp Anh Giữa Hàng Vạn Người" mình thấy nó không thật sự hay và đặc biệt nên mình nghỉ cuốn này chắc cũng vậy! 
Nhưng đến khi đọc rồi mình lại thấy nó rất hay! Rất ý nghĩa, 
Mình không phải mỹ nữ nên không cần phải tìm bạch mã hoàng tử cho mình! Mình không phải hoàng tử thì việc gì phải tìm công chúa chứ!!!
Phiên Phi và Nhất Minh hợp nhau cực đấy chứ! Nhưng lúc đầu vẫn muốn tìm một nữa theo tiêu chuẩn bản thân đã đặt ra từ đầu là phải thật "HOÀN HẢO" 
Bạn biết đấy, mỗi người sinh ra đều được ông tơ bà nguyệt định sẵn sẽ gặp một nữa thích hợp với bản thân mình rồi! Nên không cần phải lo âu, suy nghĩ và đặt ra hình mẫu lý tưởng quá cao khi bản thân mình lại không hoàn hảo ^^ 
Ngốc nghếch cũng được, xấu xí cũng được nhưng chỉ cần trái tim chân thành dành cho nhau thì việc gì ta không chấp nhận nó chứ! 
4
79420
2015-11-23 17:16:09
--------------------------
329824
8511
Quả thật là sau khi đọc tác phẩm này, mình đâm ra có hứng thú với thể loại ngôn tình có nhiều yếu tố hài hước. Truyện hấp dẫn mình ko chỉ những tình tiết vui nhộn trong tác phẩm mà còn vì cách viết của tác giả, vì 2 nhân vật chính ko phải là những nam thanh nữ tú, nam chính ko phải là soái ca, nữ chính ko phải là mỹ nhân như những truyện ngôn tình khác. Một câu chuyện thú vị cho các bạn, mình đọc đi đọc lại nhiều lần vẫn có thể cười to mỗi khi đọc :)
4
534621
2015-11-01 14:02:51
--------------------------
314083
8511
Khác với những quyển tiểu thuyết khác, đây là một quyển ngôn tình mà cả hai nhân vật chính đều là những con người rất bình thường, nam chính không phải một soái ca và nữ chính càng không phải là một mỹ nhân tuyệt sắc. Sau nhiều lần tìm kiếm một nửa còn lại của chính mình, cả hai đều không ngờ rằng định mệnh đã an bài cho họ là của nhau. Qua câu chuyện của hai nhân vật chính, làm ta nhận ra rằng hạnh phúc luôn ở rất gần bên ta, chỉ là do ta không nhận ra mà thôi.
4
514323
2015-09-25 12:41:16
--------------------------
313885
8511
Câu chuyện quá hay như làm tôi ước gì mình cũng như họ. Truyện nói về  2 nhân vật là bạn từ nhỏ để rồi thời gian trôi qua nhưng tình bạn không phải nhòa mà còn nhày sâu đậm dù ai cũng đi coi mắt nhưng đều thất bại . Có đối tượng được thì lại gập vấn đề làm cho 2 người thất vọng .để rồi quyết định yêu nhau thử để rồi họ nhận ra rằng chính mình mới là định mệnh của nhau.ai mà bỏ tác phẩm này thì tiết lắm đấy.i love (ai là định mệnh của ai)
4
831990
2015-09-24 21:00:43
--------------------------
291246
8511
Ngay từ đầu mình mua sách vì tò mò với tựa đề "Ai là định mệnh của ai" và giá cả sách cũng khá hợp lýcho 1 quyển tiểu thuyết. Chưa từng đọc qua tác phẩm nào của Tuyết Ảnh Sương Hồn nhưng qua cuốn tiểu thuyết này mình khá yêu thích tác giả. Văn phong nhẹ nhàng, gần gũi, cách xây dựng nhân vật cũng mới mẻ, không đi vào lối mòn của ngôn tình. Mạch truyện có nhẹ nhàng, có cao trào, đôi lúc pha thêm chút hài hước. Nói chung với mình đây là một quyển sách đáng đọc. Theo dõi câu chuyện của Phiên Phi và Chu Nhất Minh mới thấy được, thật ra nam nữ chính trong truyện cũng khá bình thường, đôi lúc chúng ta cũng dễ đang bắt gặp hình ảnh mình trong đó. Không có cô tiểu thư đỏng đảnh, không có mỹ nhân, không soái ca ... nhưng với câu chuyện gần gũi, chân thật này, chúng ta sẽ hiểu có những người sinh ra vốn để dành cho nhau, "ai là định mệnh của ai" sớm đã được an bài, đôi khi họ ở cạnh chúng ta, rất gần, nhưng thời điểm chưa tới mà thôi ^^
4
63487
2015-09-06 09:24:25
--------------------------
279193
8511
Điều tôi thích nhất ở " Ai là định mệnh của ai" ,chính là tác giả không cần xây dựng nhân vật chính là nam thần, tổng tài hay nữ chính tiểu bạch thỏ,... như các bộ ngôn tình khác. Nhất Minh và Phiên Nhi chỉ là những chàng trai, cô gái rất đỗi bình thường, thậm chí còn có nguy cơ "bị ế" nên chỉ sau một lời nói đùa, mới quyết định thử " ghép đôi" cùng nhau. Tình yêu bắt nguồn từ tình bạn của họ cũng trải qua đủ mọi cung bậc cảm xúc. Gấp quyển truyện có bìa màu xanh nhẹ nhàng, chợt ngộ ra: đôi khi ta mải mê tìm kiếm hạnh phúc ở một nơi xa xăm nào đó mà không chịu quay đầu nhìn lại, có thể định mệnh từ lâu đã ở ngay bên cạnh rồi! 
4
109583
2015-08-26 16:07:05
--------------------------
264839
8511
Đầu tiên là bìa truyện: tuy không nổi bật, bắt mắt lắm nhưng cũng khá đẹp mắt, giấy in cũng tốt. Về nội dung thì truyện đem lại cảm giác rất chân thực. Không giống với những truyện ngôn tình khác, nam chính cũng không phải kiểu soái ca, lạnh lùng rồi giàu có này nọ mà lại rất thật, có cảm giác nam chính là người mà chúng ta có thể gặp được ở đâu đó ngoài đời. "Ai Là Định Mệnh Của Ai" không những chân thực, còn là câu chuyện bình dị, đời thường pha lẫn với nhiều tình tiết hài hước.
4
147141
2015-08-13 15:01:17
--------------------------
264059
8511
Nếu bạn tìm kiếm một quyển ngôn tình lãng mạn, với soái ca hoàn hảo, thì Ai là định mệnh của ai chắc chắn không dành cho bạn.

Các bạn quen tìm đọc truyện để ngắm hình ảnh các anh chàng soái ca vừa đẹp trai vừa tài giỏi hẳn sẽ thất vọng lắm nếu gặp quyển này. Riêng mình thì rất hài lòng, vì câu chuyện rất khác biệt, cũng rất chân thực. Chàng "soái ca" Chu Nhất Minh không hề soái, bụng có mỡ, tóc cũng... có khả năng sẽ hói một chút. Cặp đôi trong truyện đều không phải những con người hoàn hảo, xinh đẹp, vận đào hoa lại có hơi tệ, nhưng hóa ra lại là cặp đôi hoàn hảo cho nhau.

Câu chuyện bình dị, đời thường, vậy nhưng lại tạo cho người đọc cảm giác đồng cảm, hài lòng. Mình thích quyển này còn vì màu bìa xanh dễ chịu của nó, nhìn bên ngoài không đậm màu như trong hình trên web đâu. Tuy có hơi không thích hình vẽ chibi, vì nó có vẻ trẻ con không hợp không gian truyện, nhưng nhìn chung quyển này đem trưng trên kệ sách cũng đẹp mắt.
4
28540
2015-08-12 23:29:39
--------------------------
242512
8511
cuốn Ai là định mệnh của ai của Tuyết Ảnh Sương Hồn thực khiến mình cảm thấy quá chân thực đi. về phần hình thức, màu sắc bìa khá tao nhã, tuy họa tiết không đẹp lắm nhưng trông khá teen và thuận mắt. Phần giấy in cũng khá tốt, chất lượng ổn, màu sắc giấy cũng dùng chống lóa mắt thật tiện lợi. về nội dung, sách đem lại cảm giác khá chân thực, Nhất là khi nói về nam chính, không phải kiểu soái ca lạnh lùng giàu có nhưng vẫn cuốn hút vì mình có thể tìm thấy họ trong hiện thực :)) Truyện cũng có đan xen thêm những tình tiết gây cười nên rất cuốn hút và hấp dẫn mình cho tới trang cuối cùng. Cuốn sách làm mình có một ấn tượng không tồi về tác giả Tuyết Ảnh Sương Hồn. :))
5
573527
2015-07-26 19:05:40
--------------------------
240019
8511
Mình mua cuốn truyện ngôn tình này cũng vì nghe danh tác giả. Và khi nhìn bìa của tác phẩm, tuy nó không thu hút hay nhìn sang như những quyển truyện khác nhưng bìa sách mang lại cho mình cảm giác gần gũi và trong rất teen. Nội dung truyện cũng lạ nhưng chân thực, nếu ở trong ngôn tình ta thường gặp nhũng Soái ca phong độ đẹp trai đáng ngưỡng mộ thì Ai là định mệnh của ai lại mang cho ta cảm giác nam chính là người mà ta có thể gặp được ngay ngoài đời mà không phải chỉ trên trang sách hay màn ảnh.
4
620321
2015-07-24 11:03:59
--------------------------
235102
8511
Mình mua quyển này vì ấn tượng với tên tác giả: Tuyết Ảnh Sương Hồn. Tên nghe kiêu dễ sợ =))...Về mặt hình thức bên ngoài thì quyển này chỉ dừng ở mức tạm được thôi. Bìa không đẹp mấy nhưng bù lại chất lượng giấy in lại rất tốt, nhìn lâu không bị chói mắt mà cầm cũng khá nhẹ nữa. Về nội dung thì mình thấy khá hay. Tác giả viết rất thực, đọc mà không dứt ra được. Quyển cũng hơi hơi dày mà mình đọc một lèo hết luôn =)) Mình thấy rất lạ ở cái cách tác giả xây dựng nhân vật. Thường thì những quyển ngôn tình như thế này thì luôn luôn là những chàng trai lạnh lùng hoàn hảo. Còn quyển này thì khác biệt một trời một vực luôn =)) Chẳng giàu có cũng chẳng đẹp trai. Lâu lâu còn xen mấy cái hài hước nữa. Đọc truyện này xong thì mình hơi hơi tin về tình yêu đích thực rồi...khá thực tế. Thêm cái nữa là tiki bọc bookcare rất đẹp, cẩn thận nên mình rất hài lòng. Cám ơn tiki 
4
278052
2015-07-20 20:52:45
--------------------------
209439
8511
Về hình thức bìa được thiết kế không đẹp cũng không xấu , thể hiện được một phần nội dung của câu chuyện . Về chất lượng hầu như không bị sai lỗi chính tả , dịch rất chuẩn so với bản gốc , chữ to rõ ràng . Về nội dung cuốn truyện kể về hành trình tìm kiếm chân mệnh thiên tử của một cô gái và một chàng trai . Đọc truyện tôi phải thốt lên rằng tác phẩm này rất chân thực và thực tế từ đầu đến đuôi . Muốn tìm được người yêu mình và cũng là người mình yêu là điều rất khó khăn , Vậy thì khi đã tìm được hãy nắm chặt trong tay và đừng bao giờ buông ra . Tác phẩm này quả thực rất ý nghĩa
4
662556
2015-06-17 16:00:31
--------------------------
198964
8511
Đây có lẽ là truyện ngôn tình lạ nhất mà mình từng đọc. Truyện rất đời thường, rất thực tế, không có soái ca như bao truyện khác. Chu Nhất Minh, nam chính của truyện, là người có chiều cao thấp hơn mực nước biển, thâm chí còn chưa nổi 1m70 nhưng rất quan tâm, lo lắng cho nữ chính. Yên Phiên Phi lại là cô gái " châu tròn ngọc sáng" thân hình đầy đặn. Hai người là bạn thanh mai trúc mã, cùng nhau lớn lên, cùng có người yêu rồi lại cũng chia tay. Cả hai đều ôm mộng về người yêu tương lai của mình, nhưng cuối cùng họ cũng đã tỉnh mộng, đã nhận ra rằng hạnh phúc không cần phải hoàn hảo, chỉ cần hai người yêu thương nhau là đủ.
Mình lại không thích kiểu truyện thực tế quá như thế, tuy nhiên cách viết truyện của Tuyết Ảnh Sương Hồn rất thú vị, hài hước làm cho truyện bớt nhạt.nhàm
4
606613
2015-05-21 10:28:58
--------------------------
197623
8511
Đúng là truyện rất thực tế và đời thường nhưng giọng văn của tác giả làm mình thấy rất nhạt. Cách miêu tả nhân vật cũng như những tình tiết của truyện đều không hay, đọc xong cũng chả thấy ấn tượng gì. Nhân vật chính khác các ngôn tình khác ở chỗ nàng là cô gái mũm mĩm còn chàng thì là người có chiều cao khiêm tốn. Đây đúng là nét độc đáo của truyện. Truyện cũng có vài phần hài hước, mình thích đoạn đi xem mặt của 2 nhân vật chính, những người mà họ gặp phải quả thật "đầy bất ngờ".
Dù sao thì truyện này cũng có cái hay riêng, tùy từng cảm nhận của mỗi người thôi.
2
31339
2015-05-18 08:09:55
--------------------------
195291
8511
Truyện khá lạ. Không phải motip lạ, cũng không phải tình tiết lạ, mà cái mới lạ ở đây là cách xây dựng hình tượng nhân vật. Đã lâu lắm rồi mình mới bắt gặp lại những nam chính, nữ chính bình thường giản dị đến như vậy. Nam chính không đẹp trai, chẳng thiên tài, không giàu có, không bá đạo, nữ chính cũng chẳng xinh đẹp thông minh (nhưng cũng không phải tiểu bạch), sau khi lội hết những quyển ngôn với hình mẫu ảo tung chảo thì chuyện tình nhẹ nhàng của cặp này lại làm mình thấy nhẹ lòng biết mấy.
5
449880
2015-05-13 12:31:16
--------------------------
193054
8511
Mình đánh giá cao truyện này bởi tính chân thực, gần gũi, đời thường của nó. Nam chính và nữ chính cũng là cặp thanh mai trúc mã nhưng không như nhiều truyện khác, không có việc nữ/nam chính yêu người khác còn người còn lại chờ đợi trong thủy chung, ở đây, cả 2 đều từng yêu người khác. Nam chính, nữ chính cũng không giống hình mẫu trong phần lớn truyện ngôn tình, không đẹp đến người thần ghen ghét, không giàu có, cũng chẳng bá đạo, nhưng như thế lại khiến mình cảm thấy gần với cuộc sống của chính bản thân biết bao.
4
393748
2015-05-06 13:16:29
--------------------------
191678
8511
Hai nhân vật chính là bạn thanh mai trúc mã, chơi chung từ nhỏ, biết tần tật các sở thích của nhau, luôn chia sẻ với nhau chuyện tình cảm của đối phương, hai người tìm kiếm một nữa của mình ở nơi xa xôi, không nhận ra một nữa của mình đang ở ngay bên cạnh ,may mà họ không bỏ lỡ nhau, tuy có hơi muộn truyện không nói về những nam thanh nữ tú,những anh tài tinh anh trong xã hội, mà nói về những  người bình thường cũng có khuyết điểm ,nhưng họ bù trừ vào nhau tạo nên một chuyện tình nhẹ nhàng giống với thực tế. Truyện có một điểm trừ là kể quá nhiều , ít câu thoại dễ gây nhàm chán.
3
313953
2015-05-02 13:14:20
--------------------------
175488
8511
Lần đầu tiên đọc một cuốn ngôn tình mà các nhân vật chính lại "giản dị" và gần gũi đến thế.
Nam chính rất bình thường, không đẹp trai, nhà không giàu, cũng chẳng thông minh, càng không bá đạo.
Nữ chính chẳng xinh đẹp, chẳng ngây thơ (nhưng cũng không đến nỗi cáo già)
Câu chuyện tình yêu giữa họ không hề đầy màu hồng cũng không hề bi ai mà rất thực tế, rất đời thường. Những câu chuyện tưởng chừng như có thể gặp được ở bất cứ đâu trong cuộc sống đã được tác giả khéo léo đưa vào những trang sách, hiện lên thật hài hước, vui vẻ.
5
445522
2015-03-30 12:38:00
--------------------------
175071
8511
Nam chính không đẹp trai, nhà không giàu, không thông minh lạnh lùng bá đạo, lại còn lùn?
Nữ chính không xinh đẹp mỹ miều, chẳng có tài năng đặc sắc, lại còn béo?
Nhưng quả thực tôi không lầm lẫn gì, đây đúng là một cuốn ngôn tình. Một chuyện tình bình thường, của những con người bình thường, có thể xảy ra trong đời thường, ở bất cứ nơi đâu. Tôi thích cách tác giả xây dựng nên một câu chuyện nhẹ nhàng và hài hước, từ những lần xem mặt và chia tay nối tiếp nhau của hai người, cho đến những tình huống hẹn hò và ghen tuông dở khóc dở cười. Tôi đặc biệt thích câu nói của nữ chính, "không có người yêu một trăm điểm, chỉ có hai người năm mươi điểm cộng lại thành một trăm điểm mà thôi." Con người có ai là hoàn mỹ? Biết dừng lại đúng lúc, biết trân trọng những gì mình có, chúng ta sẽ được hạnh phúc. "Tầm thường như sỏi đá cũng có thể hạnh phúc như hoa." Đã quen đọc những câu chuyện về nam nữ chính như hoa như ngọc, đọc rồi mới biết câu chuyện về sỏi đá cũng có cái hay riêng.
5
57459
2015-03-29 12:28:19
--------------------------
162424
8511
Lần đầu tiên mình đọc một cuốn ngôn tình mà nhân vật nam chính chẳng nhà giàu, đẹp trai lồng lộng, tài cán đầy mình và nhân vật nữ chính cũng chẳng xinh đẹp kiêu sa, hiền lương thục đức. 
Cốt truyện hài hước, nhiều khi cười đến đau cả bụng và rất gần gũi, giản dị, làm cho người đọc dễ có cảm tình. Chính thế nên bài học trong truyện mà Tuyết Ảnh Sương Hồn đưa đến dễ thấm vào lòng độc giả hơn : tình yêu và hạnh phúc thực sự được ban cho tất cả mọi người, dù người đó ngoại hình, gia cảnh, tài cán ra sao; chẳng qua là con người ta có chịu nhận ra món quà kì diệu ấy không, kể cả nó có đang ngay bên cạnh họ, lẽo đẽo theo họ trên khắp các ngả đường.
4
333831
2015-03-02 12:39:08
--------------------------
152081
8511
Một cuốn truyện thú vị và hài hước kể về mối tình duyên cuả một cặp thanh mai trúc mã. Phải công nhận cặp này hết sức xứng đôi vừa lứa : ngoại hình đều không xuất sắc, học lực dưới mức bình thường, gia cảnh bậc trung. Không những thế con đường tình duyên cuả họ cũng giống nhau y hệt, người này có đôi thì người kia cũng có, người này chia tay người kia cũng chia tay. Sự trùng hợp này càng thêm khẳng định họ là định mệnh cuả nhau.
Truyện xen kẽ quá khứ và hiện tại rất thú vị, tình tiết nhẹ nhàng,  nhiều chi tiết dễ thương và vui nhộn. Đây không phải cuốn truyện xuất sắc, nhưng đọc xong không thấy phí.
Điểm trừ lớn nhất là nữ chính tỏ ra vô lễ với bố và dì, dù 2 người này rất tốt.
3
403246
2015-01-21 23:24:08
--------------------------
140044
8511
Truyện nhẹ nhàng, khá hài, hơi ít cao trào, tuy nv chính không phải là nam thanh nữ tú xuất chúng như mọi truyện ngôn tìn khác, nhưng cặp đôi này rất dễ thương, có phúc cùng hưởng có nạn cùng chịu..Thích nhất câu độc thoại của nam chính "M kiếp, người ta yêu nhau ai cũng được hưởng hạnh phúc , sao tới lượt anh đây lại đen đủi thế này" rất dễ thương , mình thuộc luôn câu này luôn ^^ 
Tóm lại cốt truyện vừa phải, 2 nv yêu nhau hơi sớm nhưng có lẽ là thanh mai trúc mã từ nhỏ nên tình cảm cũng được gầy dựng sắn rồi, không vô lí cho lắm. Đáng đọc!!
4
187302
2014-12-09 21:39:02
--------------------------
137460
8511
. "Ai là định mệnh của của ai" là một quyển sách nhẹ nhàng, không hề tô hồng hiện thực mà rất thực tế. Nó không thích hợp với những ai mong chờ một mối tình oanh oanh liệt liệt hay nam chính nữ chính đẹp như thiên thần. Chuyện tình yêu của nhân vật chính dịu dàng và gần gũi vô cùng, là câu chuyện mà ta có thể bắt gặp ở bất kì đâu trong cuộc sống (nhưng cũng không kém phần éo le, dở khóc dở cười). Tuy nhiên, đây là lần đầu tiên mình đọc sách của Tuyết Ảnh Sương Hồn, có đôi chút thất vọng vì mình đã mong chờ một tác giả có khả năng sử dụng ngôn từ khéo léo và tinh tế hơn thế này, nhưng dù sao thì đây cũng vẫn là một quyển sách đáng đọc.
4
198872
2014-11-26 19:37:48
--------------------------
121198
8511
Phải nói đọc truyện chúng ta thấy giống như đó là một phần cuộc sống của chúng ta vậy . Khi đã có công việc cuộc sống ổn định chúng ta bắt đầu tìm kiếm một người bạn đời hoàn hảo . Công việc đó nói dễ thì không dễ nói khó thì không khó . Hai nhân vật chính trong truyện như hai cực nam châm luôn bị hút về phía nhau nhưng họ không nhận ra . Hai nhân vật chính bị tác giả quay như chong chóng hết lần này đến lần khác . Và cuối cùng thì họ chợt nhận ra người mình luôn tìm kiếm người mình luôn mong chờ hóa ra lại ở ngay cạnh mình . Cốt truyện nhẹ nhàng nhưng sâu sắc gây ấn tượng cho người đọc
5
337423
2014-08-18 09:02:59
--------------------------
116404
8511
Không giống như những hình ảnh nhân vật nam chính hoàn mĩ, nữ chính ngốc nghếch, xinh đẹp trong ngôn tình thông thường mà ở đây, tác giả xây dựng hai nhân vật chính gần như khác xa so với thế giới tiểu thuyết nhưng lại gần với thực tế.
Định mệnh giữa hai con người "thanh mai trúc mã" thường gặp nhưng lại có nhiều tình huống khác biệt như đến khi sắp ế rồi mới "thử yêu" vì " phù sa không chảy ruộng ngoài" mà, rồi cũng chia tay nhưng vẫn vấn vương. Số mệnh gắn kết họ lại với nhau, khiến mình như tin vào cái gọi là "duyên tiền định". Cốt truyện hay, thú vị, khác với mô típ ngôn tình thông thường tạo cho người đọc cảm giác mới lạ. Tuy vậy mình không thích bìa sách lắm, vì típ mình chỉ thích những hình vẽ chibi dễ thương hơn.
4
364829
2014-07-08 08:06:52
--------------------------
112694
8511
Không phải kiểu trai tài gái sắc lung linh giàu có như trong mô típ ngôn tình thường, cũng không phải công tử tiểu thư và bi kịch gia đình phá sản,.... Ai là định mệnh của ai với nhân vật chính là thanh mai trúc mã nhưng ngoại hình hoàn toàn bình thường thậm chí có phần hơi xấu so với khuôn mẫu nhân vật chính trong các tiểu thuyết thường hoàn mỹ không tỳ vết.
Mình thấy câu truyện này rất hay, Phiên Phi và Chu Nhất Minh luôn muốn tìm người hoàn hảo toàn diện cho riêng mình, họ luôn cố gắng hoàn thiện bản thân, phô bày cái đẹp trước những người mình thích nhưng lại quên rằng tình yêu chân thành luôn xuất phát từ trái tim chứ không phải chỉ dựa vào vẻ bề ngoài hào nhoáng.
Mạch truyện nhẹ nhàng, chân thực cùng những giây phút đẹp đẽ khi họ nhân ra người yêu thương luôn bên cạnh mình. Mình thích Ai là định mệnh của ai vì dường như truyện rất thực tế, mình cảm nhận được rằng những người không có vẻ bề ngoài vẫn luôn tìm được hạnh phúc cho riêng mình.
4
73178
2014-05-18 20:51:58
--------------------------
112449
8511
Rõ đúng là chúng ta không thể làm trái lại số phận đã vạch ra cho mỗi người.
2 nhân vật trong câu chuyện luôn muốntheo đuổi một mẫu hình lý tưởng do chính mình đặt ra. Nữ chính Yên Phiên Phi thì luôn mơ mộng về 1 chàng rể cao 1m8 trở lên,còn nam chính-Chu Nhất Minh thì lại luôn mê mệt trước 1 giai nhân :D.Nhưng dường như,sau những lần tìm kiếm đổ vỡ,2 người mới nhận ra mình chính là một nửa hoàn hảo của ng kia. Một cái kết thật viên mãn cho một mối tình "thanh mai trúc mã" đẹp. :)
Điều làm tôi thích nhất ở câu chuyện là tác giả xây dựng nhân vật rất thực tế,không hề theo mô típ quen thuộc là phải "trai tài gái sắc". Ai cũng có khuyết điểm về bề ngoài nhưng 50-50 sẽ tạo thành 100 hoàn hao .Lối kể chuyện vô cùng hài hước,dí dỏm,nhất là chi tiết tuổi thơ của 2 ng,khi Phiên Phi còn là 1 Bé Bự và Nhất Minh còn là 1 con Vi Sinh Vật :))). Những đoạn đối thoại cũng tự nhiên và hóm hỉnh. Mặc dù là ngôn tình nhưng nó làm cho người đọc rất thoải mái :)
5
122306
2014-05-14 19:51:08
--------------------------
102818
8511
Phiên Phi và Nhất Minh luôn tìm kiếm cho mình một người tình hoàn mĩ. Mà điều kiện về ngoại hình luôn giữ một vai trò quan trọng trong sự lựa chọn của họ. Nhưng qua nhiều lần cố gắng mà vẫn thất bại họ thỏa hiệp với hiện thực, từ bỏ giấc mộng hoàng tử – công chúa của mình, lui một bước, quyết định tìm một nửa phù hợp với bản thân và chính sự giác ngộ kịp thời đó đã giúp họ có được những phút giây hạnh phúc ngọt ngào khi ở bên nhau. Họ đối diện với nhau bằng con người thật của mình, không phải cố gắng che giấu đi những khiếm khuyết về vẻ ngoài của bản thân, lại càng không cần phải hạ mình để lấy lòng, hay nhân nhượng, chịu đựng ‘một nửa kia’ chỉ vì họ có ‘điều kiện’ xuất sắc hơn mình.
Truyện của Tuyết Ảnh Sương Hồn luôn mang màu sắc hiện thực, sự lãng mạn về mộng cảnh hoàn tử nắm tay cô bé lọ lem sống hạnh phúc trọn đời không hề tồn tại trong hầu hết các tác phẩm của cô. Để rồi gấp trang sách của Tuyết Ảnh lại đọc giả chỉ có thể ngậm ngùi: À, cuộc sống chính là như vậy! Khôngcần tìm kiếm hạnh phúc ở những nơi xa xôi. Đôi khi thỏa mãn chính là hạnh phúc.
4
203928
2013-12-23 18:04:19
--------------------------
97602
8511
Đa phần những nhân vật xuất hiện trong ngôn tình đều là những soái ca, mỹ nữ; nếu họ không xinh đẹp thì họ cũng tài cán đầy mình nhưng với Ai là định mệnh của ai thì đây là câu chuyện về công cuộc tìm kiếm tình yêu của những nhân vật rất thực với đời sống. Họ không xinh đẹp, hoàn hảo, cũng chẳng có năng lực gì đặc biệt nhưng chẳng nhẽ không xinh đẹp, không tài giỏi thì không thể có tình yêu sao. Dù là bất cứ ai đi chăng nữa thì vẫn có quyền nhận được yêu thương nhưng cớ sao con đường tình yêu của Phiên Phi và Nhất Minh lại đầy trắc trở như thế? Đứng trên quan điểm của tôi thì có lẽ ngay ban đầu bọn họ đã nhận định sai về tình yêu. Tình yêu không phải là quá trình tìm kiếm sự hoàn mỹ mà là quá trình tìm kiếm sự hòa hợp về tâm hồn và tính cách. Phiên Phi cứ mãi tìm kiếm bạn trai theo tiêu chuẩn không phải là bạch mã hoàng tử thì không được; Nhất Minh thì luôn mộng ảo với những người con gái xinh đẹp, kiêu sa nhưng cả hai đều quên xem rằng những người đó có thích hợp với mình hay không. Nếu mãi cố gắng với những thứ không phù hợp cứ như chúng ta đang cố mặc một chiếc áo quá chật hay lại lọt thỏm với chiếc áo quá rộng, về lâu về dài sẽ rất mệt mỏi.
Có lẽ vì chơi với nhau từ tấm bé, họ quá đỗi thân thuộc với nhau, thân đến mức không bao giờ nghĩ rằng sẽ trở thành người yêu vì vậy đã bỏ qua nhau. Nhưng thật may cho họ, đến cuối cùng họ cũng đã nhận ra được ý nghĩa của tình yêu. Không có một nửa hoàn hảo, chỉ có hai người thích hợp ở bên nhau mới khiến tình yêu trở nên hoàn hảo hơn. 
Và trong cuộc sống này, t6i chắc rằng cũng có rất nhiều Phiên Phi và Nhất Minh đang loay hoay tìm kiếm hạnh phúc của mình và liệu có ai sẽ có thể nhận ra hạnh phúc đích thực của mình là gì.
5
65800
2013-10-25 13:09:10
--------------------------
96347
8511
Một cuốn sách tình yêu tuy nhẹ nhàng nhưng đọng lại trong lòng người đọc những suy tư sâu lắng. 
Mô típ hoàn toàn khác so với ngôn tình Trung Quốc truyền thống. Nam chính không đẹp trai, tài giỏi, lắm tiền; nữ chính không đẹp nghiêng nước nghiêng thành, thân hình "liễu yếu đào tơ"; họ không là bạch mã hoàng tử hay nàng công chúa ngủ trong rừng sâu mà họ chỉ là những con người hết sức bình thường như bao người trong cuộc sống hằng ngày. Chàng cao chưa tới mực nước biển, nàng thì mũm mĩm đẫy đà.  Trong dòng xoay của cuộc sống, họ vẫn luôn mong mỏi tìm được một nửa còn lại, có thể cùng mình chung bước suốt cuộc đời còn lại, cùng mình chia sẻ những vui buồn hằng ngày. Tình yêu, đôi khi cứ ngỡ nó thật xa nhưng thật ra lại rất là gần. Khi Phiên Phi và Nhất Minh, đôi bạn thanh mai trúc mã, cùng nhau tiến cùng nhau lùi trong việc hẹn hò quyết định đi đến quyết định thử yêu thì họ đã thật sự phải lòng nhau, không thể trở lại mối quan hệ bạn bè thân thiết anh trai em gái như trước đây nữa. Ngôn từ nhẹ nhàng, có phần hóm hỉnh của Tuyết Ảnh Sương Hồn đã dẫn dắt người đọc trải qua những kỉ niệm thời ấu thơ, quá trình hẹn hò, tính cách và cuộc sống tràn đầy màu sắc của nhân vật. Người cùng ta đi hết cả quãng đời không nhất thiết tìm ở đâu xa mà có lẽ luôn luôn bên cạnh ta, giúp đỡ ta khi ta gặp khó khăn. Tuy xa tận chân trời mà gần ngay trước mắt. Bên cạnh đó, bìa sách được thiết kế rất lạ mắt và độc đáo, ai là định mệnh của ai.
Tôi ngốc nghếch, anh xấu xí
Ngốc thì ngốc, xấu thì xấu
Nhưng tâm đầu hợp ý
Anh xấu xí nhưng trái tim anh chân thành
Khiến cô bé ngốc nghếch này yêu anh tha thiết
Đôi tình nhân ngốc nghếch và xấu xi ấy
Nhân gian tìm đâu thấy
Coq chăng chỉ có trên trời cao. ;)
5
136118
2013-10-08 18:38:29
--------------------------
96007
8511
Đây thực sự là một câu chuyện hài hước về số phận đen đủi của hai nhân vật chính. Trong cuộc sống có lẽ không có ai có thể gặp nhiều chuyện đen đủi đến khôi hài như họ. 
Truyện không chỉ hấp dẫn người đọc vì sự hài hước mà còn bởi sự gần gũi với đời sống. Nó không có các nhân vật phi thực tế đẹp đẽ lung linh như các tiểu thuyết ngôn tình khác mà là những con người rất bình thường - ngoại hình bình thường, tài năng bình thường, công việc bình thường, đang trên đường tìm kiếm hạnh phúc. Họ cũng giống như bất kỳ ai,  mong mỏi một tình yêu đẹp đẽ, một người bạn đời hoàn hảo nhưng rồi dần dần họ nhận ra thứ họ thực sự cần cuối cùng chỉ là một người thấu hiểu mình, một người để mình được sống với chính bản thân mình.
Truyện còn mang đến một thông điệp hết sức ý nghĩa: Hạnh phúc không ở đâu quá xa xôi mà chỉ trong tầm tay với.
5
154426
2013-10-03 22:30:08
--------------------------
