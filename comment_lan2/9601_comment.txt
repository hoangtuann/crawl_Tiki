131775
9601
Mình mới sinh em bé, quả thực điều gì xảy đến với bé mình cũng đều luống cuống bí bách hết cả chân tay, nhiều khi thấy con bú xong trớ hết ra lại ngồi khóc mà không biết phải làm sao, không hiểu con có bị làm sao hay không. Sau đó một chị bác sĩ ở viện Nhi đã bảo mình mua cuốn sách này. Mình đã đặt mua ở tiki, rồi đến khi nhận được sách liền lôi ra đọc ngấu nghiến. Sách rất hữu ích, ngoài phần chăm sóc bản thân bà mẹ từ khi mang bầu đến sau khi sinh còn có cả phần chăm sóc trẻ, cách mặc tã, cách vệ sinh cho bé, thực đơn cho bé ăn, rất tỉ mỉ. 
Ngoài ra phần cũng rất quan trọng đó là sức khỏe của bé, các triệu chứng khi con mắc bệnh mà không phải ai cũng biết. Hơn nữa cách chăm sóc bé theo cuốn sách gần như theo kiểu của nước ngoài, mình so sánh với cách dân gian thì thấy rất hợp lý và khoa học. Sách được minh họa với nhiều ảnh chi tiết nên rất dễ hiểu.
Mình cảm thấy thật tiếc khi lúc mới mang bầu lại không đặt mua cuốn sách này, nhưng cũng may không quá muộn. Mình muốn gửi lời cảm ơn đến các bác sĩ đã bỏ công sức ra biên soạn lại sách để mở mang kiến thức cho các bà mẹ như mình.
4
45135
2014-10-27 15:13:05
--------------------------
