446259
8451
Mình đọc quyển "Chất độc nhức xương" đầu tiên, thấy hay nên tìm xem hết mấy cuốn khác trong loạt sách KTHD. Tuy nhiên mình cũng cố gắng sàng lọc, đọc thử trước nhiều cuốn rồi mới chọn mua, vì có một số chủ đề hơi xa lạ. Riêng cuốn này thì mình chọn vì thấy kiến thức gần với chương trình trong sách giáo khoa môn Sinh học. Đây đơn giản là một cuốn sách với nhiều câu chuyện bên lề về Động vật học, có tranh minh họa và lời thoại hài hước. Mình nghĩ không chỉ các bạn học sinh sẽ thấy hứng thú với cuốn sách này, mà phụ huynh và giáo viên cũng có thể tham khảo để giải trí một cách thông minh cũng như làm cho bài giảng của mình sinh động hơn.
4
758475
2016-06-11 22:50:01
--------------------------
436232
8451
Cuốn sách nghiên cứu nhiều loài động vật dữ dằn của thế giới, từ trên cạn xuống dưới nước đâu đâu cũng có những kẻ dữ dằn rình mò chỉ chờ được ăn thịt con mồi của mình. Cuốn sách đưa ra và phân tích những kẻ săn mồi đáng sợ với những kì tích mỗi năm về số lượng con mồi và những biện pháp phòng tránh nếu bạn không muốn mất một hay hai chân và tay cho chúng. Mục đích của cuốn sách là tìm ra loài động vật dữ tợn nhất hành tinh và kết quả  thật bất ngờ mà cũng chẳng bất ngờ cho lắm vì đó chính là... muốn biết loài thú dữ nhất là gì thì sao bạn không mua ngay cuốn sách này với giá cả thật phải chăng và vừa hợp với túi tiền.
5
1337568
2016-05-26 13:38:55
--------------------------
402758
8451
Ngày xưa cuốn sách này mắc lắm, dành dụm dữ lắm cũng chỉ dám mua 1 quyển. Giờ sách rẻ, giấy có mỏng hơn nhưng chất lượng vẫn tốt, chữ và hình đều rõ. Nội dung là về khoa học nhưng rất dí dỏm và bựa, dễ đọc, dễ nhớ. Mình đã làm hẳn 1 bài thuyết trình dựa hoàn toàn vào những gì trong sách viết. Đây là một quyển sách khoa học dùng để tham khảo giải trí và chém gió, đáng đồng tiền. Tuy nhiên nếu dùng để làm báo cáo cần dẫn nguồn thì chỉ nên lấy ý tưởng chứ đừng dẫn tên sách vào.
5
500980
2016-03-22 21:02:05
--------------------------
386912
8451
SINH VẬT VỚI CẢ ĐỐNG TRUYỆN LI KỲ HẤP DẪN!
Dạo bước nơi hoang dã với thú dữ và bạn sẽ khám phá:
Ai đã làm răng giả cho voi?
Những con rồng đất hơi thở hôi thôi sống ở đâu?
Nhà khoa học nào đã xơi rắn?
Nếu bạn nghĩ dạ dày bạn sẽ lộn trái lên vì những vấn đề khoa học,vậy thì hãy đọc tiếp để lần theo dấu vết của Sinh vật Dữ nhất Hành tinh.Bạn sẽ gặp những con gấu,những con voi to đùng và những con cá sấu hung tợn.Với những số liệu cực kì ấn tượng và những minh họa hết sức độc đáo!
5
846812
2016-02-26 12:46:31
--------------------------
385264
8451
Khi mình đọc nhận xét cuốn thiên nhiên hoang dã cùng bộ sách này có một bạn đã khuyên là nên đọc cuốn này để tìm hiểu thêm thì mình cũng đã mua một phần vì mình muốn có đủ bộ sách rất hay này. Cuốn này rất hay các bạn yêu thích các chương trình thế giới động vật hay thiên nhiên không thể bỏ qua vì nó cung cấp rất nhiều kiến thức rất cơ bản về các loài động vật hoang dã một cách thú vị và dễ hiểu để có thể tìm hiểu sâu hơn về thế giới còn bí ẩn với chúng ta: các loài thú dữ
4
306081
2016-02-23 19:58:50
--------------------------
372402
8451
Lần đầu tiên mình đọc một quyển sách nói về động vật mà không ngáp lên ngáp xuống. Bìa sách đẹp, nội dung và lời cũng khác hẳn với những cuốn sách giáo khoa thông thường. Không những vậy cuốn sách này còn cung cấp thêm cho mình những kiến thức về động vật hoang dã. Đặc biệt thú vị giống như chính mình đang tự đi đến nơi để tìm hiểu vậy. Và cái làm mình rất thích ở quyển sách này đó là có những hình vẽ minh họa sinh động làm cho cuốn sách trở nên thú vị hơn. Nói chung là rất đáng để đọc.
5
709791
2016-01-21 16:07:33
--------------------------
369725
8451
Đây là một cuốn sách viết về các loại thú dữ vô cùng hấp dẫn. Sách có nhiều hình minh hoạ rất vui và ngộ nghĩnh về các loài động vật được cho là hung dữ trong thế giới loài vật như cá mập, cá sấu, gấu, sư tử, hổ báo, voi, chó sói. Có rất nhiều chi tiết lý thú, bất ngờ về các con thú dữ này ví dụ như: gấu trắng thuận tay trái, cá sấu phi châu có răng hàm dưới nhìn được, cách nhận biết voi Châu Á và voi Châu Phi, khi gấu ngủ đông thì cũng không đi toilet luôn :), v.v, và còn rất nhiều những chi tiết thú vị khác..
5
470937
2016-01-16 12:18:17
--------------------------
331538
8451
Mình rất yêu thích thế giới động vật, vì vậy khi nhìn thấy bạn mình có cuốn sách này mình đã mượn và đọc ngay lập tức. Phải nói rằng chính cuốn sách này đã đưa mình biết đến bộ sách horrible science và yêu thích bộ sách từ lúc đó. Vì sao ư? Đơn giản vì chính bản thân quyển sách. Sách với tranh minh họa cụ thể, sinh động  cùng với cách trình bày cuốn hút dí dỏm thông qua một cuộc thi xem thử loài thú nào dữ nhất Trái Đất. Cuốn sách cung cấp những kiến thức sinh học cơ bản nhất về môi trường sống, về chuỗi thức ăn, về đặc tính của một số lài vật bạn vô cùng quen thuộc mhưng chứ chắc bạn đã hiểu rõ về chúng. Hấp dẫn ngay từ giây phút đầu tiên cộng thêm cái kết vô cùng bất ngờ sẽ làm bạn phải suy nghĩ nhiều điều khi đã gấp cuốn sách lại.
5
845104
2015-11-04 18:46:54
--------------------------
310782
8451
Mình mua quyển sách này với mục đích hiểu thêm về các loài thú dữ: cấu tạo và chức năng các bộ phận, tập tính sống, đặc điểm sinh học.... Và cuốn sách đã không hề làm mình thất vọng! Quyển sách cung cấp rất đầy đủ các thông tin trên, về các loài thú dữ như: cá sấu, rắn, sư tử, cá mập, hà mã, gấu, cá pirama... và khá nhiều các loại thú ăn thịt khác. Tóm lại đây là một cuốn sách hay, hài hước, dễ hiểu, văn phong cũng súc tích và hấp dẫn. Rất tuyệt vời!
5
802760
2015-09-19 19:45:35
--------------------------
273354
8451
Cuốn sách minh họa phong cách người lớn với những hình vẽ giản dị và hài hước kiểu Âu Mỹ. Điều tuyệt là nội dung thuộc thường thức cơ bản nhưng không hề chán ngán như sách giáo khoa. Mình chỉ cần đọc trong này là đủ nắm kiến thức như sách giáo khoa rồi. Là sách kiến thức nhưng mình họa rất nhiều, còn có nhân vật dẫn dắt cùng những màn nói chuyện bá đạo của họ lẫn những con vật trong truyện nữa. Sách không dày, cuốn nhỏ nên không chán ngán lắm, đọc cái vèo là hết còn tiếc, thông tin chắt lọc nên cũng dễ nhớ.
5
203037
2015-08-20 22:24:04
--------------------------
268818
8451
Chọn đọc cuốn ‘Thú dữ’ là chọn đi phiêu lưu cùng 2 nhân vật chính là nhà tự nhiên học Will và con khỉ Mickey thông minh của ông. Will và Mickey sẽ đưa bạn đi khám phá và bầu chọn ra loài động vật nguy hiểm nhất thế giới. Thoạt tiên là điểm qua vài khái niệm và thông tin cơ bản rồi đến tìm hiểu sâu về những loài được cho là nguy hiểm nhất như cá mập hung dữ, rắn, sư tử,… Những con số, thông tin ngắn được diễn tả qua các câu chuyện nhỏ sẽ giúp độc giả thích thú hơn.
5
673477
2015-08-16 21:07:16
--------------------------
263724
8451
Cuốn sách này sẽ cung cấp cho bạn thông tin về những loài động vật đáng sợ bậc nhất thế giới như Hổ, Sư tử, Sói, Trăn, Rắn, ... thông qua một cuộc thi cũng "đáng sợ" không kém để tìm ra loài động vật đáng sợ nhất thế giới. Về kết quả mình không bất ngờ lắm, giống như suy nghĩ ban đầu của mình, nhưng cũng mang đến sự thích thú rất hài hước cho cuốn sách. Không chỉ vậy, điểm mình thích nhất ở cuốn sách này là nó giống như một cẩm nang thú dữ, cho chúng ta một số cách phòng chống cơ bản để không bị nuốt chửng hay đớp mất một vài bộ phận khi đi vào lãnh thổ của những loài thú dữ tiêu biểu nhất.
4
113650
2015-08-12 19:51:55
--------------------------
254347
8451
Đây là cuốn sách mà tôi thích nhất trong bộ Horrible science, một cuốn sách tuyệt vời.Cùng với anh chàng William "ngầu" và chú khỉ Mickey thông minh, tôi đã trải qua biết bao nhiêu câu chuyện thú vị, dễ thương, dí dỏm mà cũng rùng rợn không kém. Đâu thực sự là một con thú dữ tợn, chúng ta làm thế nào để sống sót khi gặp những con thú đó? Cuốn sách này đã cung cấp cho tôi nhiều kiến thức thật sự bổ ích và cả những tiếng cười sảng khoái qua từng trang giấy, và cũng không kém phần ngạc nhiên khi biết con vật nào đã giành giải quán quân trong cuộc thi " con thú dữ tợn nhất ".
5
280592
2015-08-05 10:14:57
--------------------------
251784
8451
Cuốn nào của tác giả Nick Arnold cũng khiến tôi cười sặc sụa. Sách kiến thức khoa học, sinh học mà đọc hài như thể truyện cười! Nhưng như thế không có nghĩa là nó không đúng đâu nhé, kiến thức rất chuẩn, chỉ là tác giả có một phong cách viết hài hước thôi. Tôi nghĩ cuốn sách này và những cuốn còn lại trong bộ sẽ rất có ích cho các bạn thanh thiếu niên, vừa đọc để giải trí, vừa bổ túc kiến thức. Và những kiến thức này bạn cũng học ở lớp đấy nhé, thế là bạn có một bộ sách tham khảo quá tuyệt rồi!
5
82774
2015-08-03 10:32:49
--------------------------
223285
8451
Câu chuyện của thú dữ thật sự rất lý thú , toàn là những loài hung tợn nhất hành tinh. Tác giả miêu tả các sinh vật hung dữ này một cách rất sinh động, hài hước kể theo chân một nhà mạo hiểm hết sức vụng về tạo ra nhiều tình huống cười té ghế. Rồi nhân sự việc đó tác giả lôi kéo chúng ta vào thế giới huyền ảo của khoa học để chứng minh cho chúng ta thấy những loài thú mà ông đưa ra chính xác là những kẻ hung bạo nhất và có rất nhiều thông tin bổ ích mà chúng ta chưa được biết ở xung quanh mình. Tất cả đều rất thú vị qua dòng chữ, ảnh minh học và có cái kết rất độc đáo. Ngoài cuốn “Thú dữ” ra còn nhiều cuốn cũng thuộc cùng một tác giả và cùng chủ đề “Horrible Science” vô cùng hấp dẫn cho những ai yêu thích và muốn tìm hiểu về khoa học.
5
605634
2015-07-06 21:38:05
--------------------------
223216
8451
Một quyển sách rất hay và thú vị. Trong đó chứa những kiến thức rất hay và cần thiết cho chúng ta về thế giới của loài vật. Đây có vẻ là một quyển sách rất bổ ích với các bạn nhỏ đam mê khoa học hay thế giới tự nhiên. Với những lời giới thiệu, lời giải thich và những câu thoại dí dỏm của các nhân vật, tôi nghĩ rằng các em nhỏ sẽ càng tiếp thu kiến thức tốt hơn, đúng là vừa học vừa chơi mà. Trang bìa được trình bày trông rất bắt mắt. Với hình ảnh chính là chú sư tử cũng đã đủ để nói lên nội dung của cuốn sách rồi: Thú dữ". Tôi hy vọng rằng, sau này sẽ có thêm nhiều cuốn sách hay và bổ ích đến như vậy cho các bạn nhỏ để các bạn có thể tiếp thu kiến thức bổ ích một cách thoải mái.
3
13723
2015-07-06 19:49:06
--------------------------
220567
8451
Thiệt sự mấy quyền này mấy em nhỏ nhà mình đọc ko hiểu, nhưng mình cảm thấy rất thú vị. Quyển thú dữ này là một trong những quyển mình thích nhất trong cả sê-ri, chắc chắn bạn sẽ đi từ bất ngờ này đến bất ngờ khác khi phát hiện những gì mình nghĩ về thú dữ ngoài thiên nhiên rất khác so với mình được hoc trên trường lớp. Hình vẽ dễ thương, lời văn dí dỏm nhưng đôi chỗ dịch còn hơi khó hiểu. Giấy in đẹp và rất thơm nhé

4
120276
2015-07-02 18:35:36
--------------------------
217406
8451
Thú dữ khi nghĩ đến nó khiến ai cũng phải khiếp sợ, nó thật sự rất khủng khiếp. Qua cuốn sách này với những lời văn dí dỏm, hình minh họa rõ ràng, sinh động sẽ giúp chúng ta hiểu rõ hơn về những sinh đáng sợ nhất hành tinh này. Tuy đáng sợ nhưng nó thật sự rất thú vị. Có lẽ đôi khi đọc nó bạn sẽ phải thốt lên rằng :” Ôi thật là khủng khiếp! “ nhưng bạn cũng sẽ biết rằng thực ra các loài vật này cũng không thật sự muốn như vậy, lí do thì có lẽ khi đọc cuốn sách này bạn sẽ hiểu rõ hơn về điều đó. Một cuốn sách hay và bổ ích, không chỉ vậy cuốn sách này sẽ giúp ta nhận ra rằng những con thú trở nên hung dữ như vậy một phần cũng do chúng ta.
5
554214
2015-06-29 11:31:34
--------------------------
202469
8451
Đây là một quyển nằm trong bộ sách Kiến thức Kinh dị, được xuất bản ở Anh Quốc. Sách trình bày về thế giới tự nhiên một cách rất sinh động,t hú vị và lôi cuốn qua những hình vẽ, những mẩu chuyện có thật, những con số biết nói về loài "sát thủ" nhất trái đất, chắc hẳn kết quả sẽ khiến người đọc bất ngờ. Mình đã mua đủ một bộ sách này để tặng cho đứa em, và nó rất thích thú. Sách thích hợp với mọi lứa tuổi, đặc biệt là với các em học sinh đam mê tìm hiểu về thế giới.
5
564333
2015-05-29 17:22:02
--------------------------
163165
8451
Cuốn sách mở đầu khá thú vị khi đặt ra câu hỏi loài động vật nào là thú nguy hiểm nhất hành tinh, và thông qua đó giới thiệu, đưa ra rất nhiều thông tin bổ ích về các loài thú dữ, các mối nguy hiểm của chúng. Kết quả ở cuối sách sẽ khiến cho bạn bất ngờ về "ngôi sao" nguy hiểm này.
Điểm cộng lớn là các thông tin rất hay, hình minh họa nhí nhố rất thú vị và bắt mắt trẻ em. Em trai mình rất là thích bộ Horrible Science này. Nhà bạn nào có em nhỏ đến tuổi tìm hiểu khám phá thì nên mua bộ này nhé, đảm bảo sẽ bổ ích hơn nhiều so với chơi game hay coi tivi ^^
5
9025
2015-03-04 11:50:15
--------------------------
