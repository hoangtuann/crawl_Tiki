308413
8488
Cùng con rèn thói quen tốt - Cuộc sống là những câu chuyện về các bạn thú nhưng lại vô cùng gần gũi với các con. Qua những câu chuyện nhỏ nhỏ bố mẹ có thể giáo dục con mình nhiều đức tính tốt và các con dễ tiếp thu hơn. Các con mình rất thích nghe bố mẹ kể cho những câu chuyện trong quyển sách nhỏ này. Ngày nào cũng đòi nghe dù đã thuộc lòng các câu chuyện trong sách. Và thêm một điểm cộng cho sách nữa là sau mỗi câu chuyện có 2 câu hỏi cho các bé giúp các bé tập trung chú ý hơn vào câu chuyện để có thể trả lời được các câu hỏi liên quan. Bé nhà mình rất phấn khích khi trả lời đúng các câu hỏi. Mình đã sưu tầm đủ bộ sách này. 
4
663152
2015-09-18 17:42:55
--------------------------
270592
8488
Việc ăn uống của trẻ con bây giờ là nỗi đau đầu lớn của hầu hết các gia đình. Thói quen ăn uống không được rèn từ nhỏ khiến người lớn mệt mỏi và cũng ảnh hưởng đến tính cách của các con. Những câu chuyện như thế này giúp ích rất nhiều cho các con trong thói quen sinh hoạt hàng ngày, rèn nết ăn 1 cách nhẹ nhàng và ý nghĩa. 
4
298411
2015-08-18 13:38:36
--------------------------
264295
8488
Gồm nhiều câu chuyện ngắn, đơn giản  thích hợp các bé mầm non, dạy các bé có ý thức về những thói quen từ nhỏ. Trong truyện có nhịều con vật ngộ nghĩnh, đáng yêu, gây hứng thú cho bé, giúp bé bắt chước học theo những con vật này. Nét vẽ hoạt hình rất sinh động. Cuối mỗi truyện có câu hỏi giúp bé nhớ lâu,tập thói quen ghi nhớ tốt hơn. Bộ sách vừa là truyện, vừa là những bài học cho các bé dễ nhớ,dễ áp dụng. Tôi đã mua đủ bộ sách gồm 10 chủ đề khác nhau cho con đọc. 
4
512701
2015-08-13 09:11:29
--------------------------
