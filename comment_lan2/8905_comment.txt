475576
8905
Khi nhận bộ poster mình rất thích vì:
- Từ tiếng Anh được in to, rõ ràng kèm theo phiên âm chuẩn giúp trẻ dễ đọc
- Poster được in và phủ nhựa giúp không bị thấm nước, cứng cápkhó bị hư hỏng
- Hình ảnh rõ ràng giúp trẻ dễ nhìn và nhận biết
- Màu sắc rực rỡ bắt mắt, gây được thích thú từ trẻ
- Trong poster có nhiều chủ đề được chia khá rõ ràng và cụ thể giúp khi dễ dạycho trẻ hơn, trẻ cũng dễ dàng nhớ hơn.
Nói chung là mình khá ưng ý về bộ poster này. Hơn nữa là cháu mình nó rất thích học trên bộ này.
4
1357834
2016-07-15 10:51:42
--------------------------
400918
8905
Khi nhận được Poster mình rất ưng ý vi những điểm sau:

+ chất liệu poster: được in trên loại giấy phủ nhựa dày dạn, khó quăn góc và thấm nước
+ hình minh họa rõ nét, sắc sảo, nhiều màu sắc, như hình chụp và dán lên vậy
+ tiếng Anh chuẩn
+ phông chữ to khiến đọc dễ hơn, giúp các em nhớ mặt chữ rõ hơn

Poster có rất nhiều chủ đề hữu ích, mình đã sưu tầm đủ các chủ đề, dán kín các bức tường phòng bé hihi. Bé rất thích và mỗi ngày đều phải chỉ 1 vài hình đọc to lên.
4
63996
2016-03-19 23:16:43
--------------------------
382037
8905
Sản phẩm được in hai mặt với màu sắc tươi tắn với nhiều hình ảnh trong nhiều lĩnh vực rất khác nhau và rất hữu ích cho bé. Sản phẩm cũng được cập nhật tiếng Anh kèm phiên âm nên mình tận dụng để dạy bé thêm mỗi khi chơi với bé! ^^ Sản phẩm được đóng thành tập nên mình treo lên cho bé xem như dạng lịch treo. Tiki đóng gói cẩn thận và giao hàng khá nhanh tuy nhiên bị tách thành 2 đơn hàng làm mình phải đợi đợt giao hàng thì hơi phiền cho mình một chút vì chỉ có 1 sản phẩm ở đơn hàng sau thôi! ^^
5
351847
2016-02-18 14:49:57
--------------------------
281408
8905
Lúc nhận được bộ Poster này mình rất ưng ý. Chất lượng giấy tốt, hình ảnh minh hoa rõ ràng với phiên âm đầy đủ. Chủ đề đa dạng phong phú được sắp xếp theo từng loại chủ đề khác nhau. Một bộ poster khá hữu ích trong việc học từ của các bé. Mình là giáo viên tiếng Anh nên rất thích mua những loại poster kiểu này làm phần thưởng cho bé khi đạt thành tích học tập tốt. Vừa hợp túi tiền vừa huuuwx ích cho việc học tập của học sinh.Học trò mình rất thích thú khi nhận được những món quà như thế này. Mình cũng thấy bé học từ vựng nhanh hơn quan hững poster kiểu như này. Thực sự hữu ích.
4
75031
2015-08-28 13:23:41
--------------------------
276352
8905
Khi nhận được bộ poster mình cảm thấy rất hài lòng, hình ảnh đẹp, có phiên âm rõ ràng, chủ đề lại đa dạng phong phú. Con mình rất thích xem những hình ảnh trong đó. Tuy nhiên có nhược điểm là nhiều hình quá nên hơi rối mắt. Dù sao cũng cho 5 sao vì mình đã tìm rất lâu mới được bộ poster vừa ý như vậy. Giá cả lại rất hợp lí. MÌnh sẽ mua hàng thường xuyên trên Tiki, vì hàng chất lượng, giao hàng uy tín, mua  tiện lợi. 
5
751894
2015-08-23 21:58:45
--------------------------
231597
8905
Tôi đã đặt mua bộ poster này , cả tập 1 và 2. Thấy Tiki có tập 3 , thế là tôi đặt mua luôn.Chất lượng giấy của sản phẩm khá tốt,dày và bóng.Hình ảnh được in thật bắt mắt,nội dung phong phú với nhiều chủ điểm khác nhau, sẽ rất tuyệt vời khi các bạn đang muốn hướng dẫn cho con cháu mình những từ tiếng anh đơn giản đầu tiên.Phần hình minh họa cũng rất rõ ràng nên sẽ kích thích giác quan của các bé một cách tối ưu nhất.Tôi nhận thấy bộ poster này thật tuyệt.
5
551735
2015-07-18 10:55:14
--------------------------
230557
8905
Bộ Poster Học Tiếng Anh Bằng Hình Ảnh (Tập 3) gồm 8 tờ bao gồm nhiều chủ điểm như các loài hoa, các loại côn trùng, các loại chim v.v...bộ poster này có kích thước nhỏ gọn thích hợp treo ở bàn học hoặc để đâu đó trong phòng rất thuận tiên cho việc sử dụng mỗi tâm có hình ảnh và nghĩa tiếng anh, tiếng việt đi kèm không chỉ giúp các bé lớn học tiếng anh mà còn giúp các bé nhỏ nhận biết đồ vật
5
153008
2015-07-17 15:42:25
--------------------------
