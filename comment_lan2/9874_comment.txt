440979
9874
Mình rất thích cuốn sách này, đặt mua ngay khi nhìn tựa đề và lời giới thiệu, cuốn sách khá mỏng nhưng chứa nhiều thông tin đúc kết bổ ích và có sự chia sẻ từ kinh nghiệm sống thực tế của tác giả khi ở trong và ngoài nước, phần nào làm sáng tỏ thêm cuộc sống ở trời Tây sau khi lấy chồng nước ngoài, giúp người đọc hiểu thêm và bớt mộng tưởng cuộc sống đầy đủ tiện nghi khi được lấy chồng Tây. Cuốn sách này nhất định phải có trong tủ sách của các bạn nữ Việt Nam.
5
665690
2016-06-02 16:21:34
--------------------------
440537
9874
Điều đầu tiên là bìa sách rất đẹp, cầm lên rất sướng tay, thế nhưng có vẻ giá sách quá mềm nên không được tiki gói gắm kỹ lắm, nếu trước kia tiki giao hàng sẽ có vỏ bao trong từng quyển sách thì bây giờ quyển này lại không có, khiến cho phần bìa của nó hơi bong tróc vài chỗ. Tuy nhiên, nếu đánh giá nội dung thì mình thấy rất xuất sắc, rất hay có lẽ chính sự thực tế trong từng câu văn đã lôi cuốn mình :) càng đọc càng thấy hay mấy bạn ạ
5
643716
2016-06-01 22:26:56
--------------------------
412754
9874
Cuốn sách là quyển  nhật ký nhiều thông tin văn hóa được viết qua đôi mắt sắc bén của tác giả. Phù hợp cho những ai yêu và quan tâm đến văn hóa âu Châu, cũng như những ai muốn tiến đến 1 cuộc hôn nhân vượt biên giới!

Với 1 tách cà phê hoặc trà, cuốn sách sẽ là cách trải nghiệm những ngày nghỉ cuối tuần tuyệt vời và ý nghĩa nhất! :)

Đọc, tủm tỉm cười và khâm phục niềm tin vào tình yêu không biên giới của chị tác giả :) Chờ đợi 1 phiên bản sau hôn nhân của hành trình tìm kiếm tình yêu xứ người của chị!





4
1209514
2016-04-08 11:51:01
--------------------------
395786
9874
Đây không phải là tiểu thuyết, nó là kí sự về cuộc sống, cụ thể là cuộc sống của tác giả - một phụ nữ Việt ngoài 30, quyết định kết hôn với một người đàn ông Bỉ và chuyển đến Bỉ sống, đan xen vào đó là những câu chuyện góp nhặt từ những người bạn của tác giả, cũng là những cô vợ Đông lấy chồng Tây.

Sách đem đến nhiều ánh nhìn thực tế trong việc cưới chồng Tây, phải sống ở nơi đất khách quê người. Nó không hề đơn giản khi bạn phải trải qua rất nhiều thủ tục, giấy tờ, chờ hết cơ quan này đến cơ quan kia công chứng, chưa kể phải mất rất nhiều thời gian dịch nếu người chồng không nói tiếng mẹ đẻ là tiếng Anh.

Cái thứ hai, cuộc sống mỗi người, dù rằng đã kết hôn cũng rất độc lập. Đôi khi làm ta cảm thấy hối tiếc và tủi thân. Bạn đừng mong mình sẽ ăn không ngồi rồi ở nhà và chờ tiền chồng nuôi, vì đó quả là một gánh nặng cho xã hội. Vì thế, phải học tiếng, học luật để làm được gì đó có ích.

Cái thứ ba, dịch vụ y tế của Bỉ rất tốt. Tác giả khi mang thai và sinh con đều được chăm sóc rất cẩn thận, mặc dù có khá nhiều mâu thuẫn suy nghĩ trong việc sinh đẻ giữa văn hóa Đông - Tây.

Nói chung đây là một cuốn sách tốt. Có thể từ ngữ không bay bướm, nhưng bù lại ngôn ngữ đời thường, gần gũi của tác giả đã làm cho người đọc cảm thấy thân thuộc, dễ dàng cảm thông hơn.
4
104571
2016-03-12 14:49:17
--------------------------
390910
9874
Sách khá hay và thú vị, một góc nhìn mới mẻ về cuộc sống của những người phụ nữ châu Á lấy chồng nước ngoài mà ở đây là lấy chồng châu Âu, những ưu điểm, nhược điểm, những khó khăn thuận lợi và mất mát qua góc kể của một người trong cuộc thật không thể chân thực hơn dù có phần còn hơi sơ sài vì cuốn sách cũng khá ngắn, cuộc sống hôn nhân không phải lúc nào cũng thuận lợi nhất là khi lại còn có rào cản về ngôn ngữ, văn hóa, họ đã vượt qua điều đó và tìm được hạnh phúc dù có sự khác biệt
4
306081
2016-03-04 17:49:55
--------------------------
359185
9874
Cuốn chia sẻ những sự làm quen trong đời sống ở xứ xở khác nhau , rèn luyện sự thích nghi dần dần so với người đã quen với lối sống hàng ngày từ ngày chuyển đến nơi ở mới của chồng thì có sự đối lập từ cách nghĩ tới việc chuẩn bị lời nói , chào hỏi và xã hội đều cách xa sự hiểu biết đó , nếu ai chưa học được mà muốn có kinh nghiệm từ việc lấy chồng ở tây thì cần biết , tránh kịp sự phát triển của con người thời mới .
3
402468
2015-12-26 17:00:15
--------------------------
297929
9874
Mình vừa đọc xong cuốn này. Nói chung nội dung tạm ổn. Cung cấp cho người đọc cái nhìn khái quát về cuộc sống nơi xứ người, chuyện làm dâu Tây và các bà mẹ chồng Tây. Mình nhận thấy không phải bà mẹ chồng Tây nào cũng tốt, nhưng được cái là họ không xen vào chuyện gia đình của con cái quá nhiều. Cuộc sống thì ở đâu cũng vậy, cũng phải làm mới có cái ăn.

Những câu chuyện trong sách không có gì là nổi trội hay đặc sắc, đều đều từ đầu tới cuối. Cảm giác không được thỏa mãn cho lắm, vì mình muốn biết nhiều và chi tiết hơn về việc xin visa, xin việc, .... nhưng hầu như tác giả chỉ nói sơ qua. Văn phong kể chuyện đúng là hơi nhạt, đọc xong không đọng lại gì nhiều.
3
112457
2015-09-12 11:46:31
--------------------------
293587
9874
Sách tự truyện lột tả chân thực cuộc sống hôn nhân ngoại lai đến từ nhiều số phận, cũng như mở ra những thử thách, khó khăn mà bản thân nhân vật chính đã và sẽ cố gắng vượt qua. Theo cảm nhận bản thân, sách như cuộc hành trình xuyên suốt trong đó người phụ nữ không ngừng phấn đấu phát triển để giữ gìn một hạnh phúc bình dị cũng thật đỗi lớn lao, hạnh phúc cho gia đình, cho người thân
Riêng về vài điểm, mình thấy sách chưa rút ra được bài học thật sự sau những câu chuyện đan xen đời thường, ý nghĩa đúc kết, bài học sâu sắc, chưa thấy điểm này ở sách, tuy nhiên sách vẫn là một cẩm nang sống cho những phụ nữ trung lưu hướng ngoại, có mưu cầu thay đổi v.v....

4
590968
2015-09-08 14:34:24
--------------------------
277832
9874
Một quyển sách với chủ đề đang hot hiện nay: nhiều hụ nữ Châu Á đang xuất ngoại để lấy chồng Tây. Xyên suốt quyển sách là câu chuyện thật của chính tác giả, những trải nghiệm, cảm xúc được thể hiện hết sức chân thực. Từ những ngày đầu đôn đáo để xin visa, rồi lên đường sang một xứ sở xa lạ cùng anh chồng người Bỉ, rồi chuyện làm quen với các thành viên nhà chồng, đi làm, đi học, sinh con, du lịch... những niềm vui, nỗi buồn của tác giả cũng được chia sẻ nhiệt tình với những người bạn Việt bên trời Âu. Tuy nhiên, tác giả cũng có những khuyết điểm nho nhỏ, đó là phong cách kể chuyện hơi nhạt, có nhiều chỗ đọc rất chán do tác giả chỉ chăm chú nói về một vấn đề nào đó. Nhiều chi tiết cần thiết cho những bạn đọc có cùng hoàn cảnh như hồ sơ, thủ tục xin visa, việc làm khả dụng, các chính sách an sinh bên ấy,... thì không thấy tác giả đề cập nhiều. Nói chung, ưu nhiều hơn khuyết, đây vẫn là một quyển sách ổn, đáng đọc.
4
746933
2015-08-25 12:56:28
--------------------------
249350
9874
Sách kể về cuộc đời của người phụ nữ,không hoa trương,bóng bẫy nhưng rất thực.Lời văn thật dung dị,chân thật đã lột tả được những khó khăn vất vả của một cuộc sống nơi đất khách quê người chẳng dễ dàng gì từ việc đi học ,mang thai,xin Visa,học tiếng Hà Lan để chung sống với gia đình chồng.Cái khó khăn đó không phải chỉ riêng là của người phụ nữ trong truyện mà là khó khăn của những người phụ nữ phải đi lấy chồng xa.Đọc sách mà tôi cũng thấy xót xa cho những cô dâu lấy chồng Tây
3
187271
2015-07-31 15:38:41
--------------------------
245363
9874
Đây không phải là một cuốn sách lãng mạn kiểu ngôn tình, cũng chẳng phải tiểu thuyết văn học sến sẩm. Đây là một cuốn sách, một cuốn tự truyện của người phụ nữ "lấy Tây". Một cuốn sách chân thực về những mối tình ngoại lai, một cuốn sách nói về cuộc sống thực tế của những người phụ nữ Việt khăn gói theo chồng, hòa nhập với cuộc sống Tây Phương. Một cuốn sách dập tan những ảo tưởng nhung lụa về việc "lấy Tây", "theo chồng sang Tây". Tuy vậy, sự chân thực ấy cũng đồng nghĩa với những mối tình dung dị của những con người đến từ những nền văn hóa hoàn toàn khác biệt, về những mối quan hệ mới xung quanh những "cuộc tình ngoại lai" ấy, là tình bạn, là tình cảm gia đình gắn bó.
5
9511
2015-07-28 21:23:04
--------------------------
230593
9874
Phải nói thực là đọc đến gần một nửa cuốn sách mình vẫn liên tục cảm thán là may mà mua cuốn này. Đến lúc đọc hết thì quyết định đọc lần 2 cho nó... ngấm hơn. 
Sách phải nói cực chất, tất cả thông tin, những câu chuyện mà Kiều Bích Hương trải qua, từ chuyện xin visa, đến những người bạn, khi chị bụng mang dạ chửa và câu chuyện đi học, những chuyến du lịch với các gia đình vợ Đông- chồng Tây khiến mình không tài nào dứt ra được. Bởi tất cả câu chuyện ấy là thực- mình tin vậy và rất hữu ích, nó lột tả cuộc sống trần trụi chân thực của những người phụ nữ Á lấy chồng Tây, nó không hào nhoáng xa hoa như nhiều người vọng tưởng, mà nó có đấu tranh, có cố gắng và có cả sự công bằng. Đọc xong cũng rút ra khá nhiều kinh nghiệm cho cuộc sống bên Tây nếu ai có ý định du học, du lịch hay lấy chồng bên đó. Hiện mình đang tiếp tục tìm kiếm những cuốn sách như thế này để thêm kiến thức về những nơi cách mình nửa vòng trái đất kia
5
280862
2015-07-17 16:05:46
--------------------------
211879
9874
Khi bắt đầu đọc, mình có hơi bất ngờ vì cảm giác tác giả lấy chồng Tây không phải vì tình yêu mà đa phần vì lí do khác. Dần dần những câu chuyện của những cặp vợ chồng hé mở, về một thế giới không như là mơ, không phải lấy chồng Tây là sướng. Tuy nhiên tác giả vẫn may mắn khi gia đình chồng hoàn toàn yêu thương mình. Cả chuyện xin visa hay học tiếng cũng bày ra những sự thật trần trụi như những rắc rối trong thủ tục ở Việt Nam. Tuy nhiên mình thấy tác giả có vẻ tự tin vào bản thân quá và cũng có phần muốn thể hiện qua cuốn sách. Tóm lại đây là một cuốn sách chấp nhận được.
3
42878
2015-06-21 11:07:11
--------------------------
172896
9874
Mình thích những cuốn sách như thế này vì nó cho mình thấy được những điều mà mình có lẽ sẽ không bao giờ được trải nghiệm mà chỉ có thể nghe kể. Cuộc sống của những cặp vợ Đông chồng Tây đã được tác giả miêu tả hết sức chân thật với những ví dụ rất cụ thể là chính bản thân tác giả và bạn bè xung quanh chị. Đọc cuốn sách mình mới hiểu được là cuộc sống ở Tây phương cũng không hề dễ dàng, không có chuyện ngồi mát ăn bát vàng mà cũng phải đỏ mồ hôi công sức ra mới có được cuộc sống tốt đẹp. Sự khác biệt về văn hóa cũng là một điểm thú vị. Tóm lại theo mình thì đây là một cuốn sách rất hay và đáng đọc.
4
17740
2015-03-25 09:17:31
--------------------------
51604
9874
Dù mới đọc non nửa cuốn sách, nhưng tôi đã bị thu hút ngay từ những chương đầu tiên. Tôi tò mò chuyện 2 người thuộc 2 nền văn hóa khác nhau, giáo dục khác nhau, lối sống lối suy nghĩ và nếp thói của 2 châu lục khác nhau sẽ sống với nhau dưới 1 mái nhà như thế nào. Kiều Bích Hương đã diễn tả rất chân thực điều đó. Cô diễn tả sự khó khăn trong việc chạy vạy xin giấy tờ để có visa sang được Bỉ với chồng, lại bắt đầu tất bật đương đầu với những khó khăn để học tiếng Hà Lan, để hòa nhập cộng đồng, để bảo vệ mình khỏi cuộc sống xa lạ nhưng vẫn đủ gần để hòa nhập. Lời văn chân thật ra đôi chút dí dỏm, tác giả bóc từng nếp "ảo tưởng" ra như người ta bóc vỏ hành, để rồi mỗi lượt bóc xong, khi cái áo hào nhoáng được cởi bỏ để lộ những sự thật trần trụi và những gai nhọn khó khăn, thì người ta lại rơi nước mắt.
5
39157
2012-12-21 21:28:11
--------------------------
50980
9874
Lúc nhìn cái bìa trang sách mình có cảm giác nó rất "Chinese" và định bỏ qua. Nhưng sau khi click vào đọc lời giới thiệu thì mình quyết định mua và đọc.
Đây đúng là 1 tự sự chân thật của tác giả. 
Đọc xong tác phẩm này, mình càng hiểu hơn về những cuộc hơn nhân khác màu da và quốc tịch như thế này. Và hiểu ra 1 điều, trong tình yêu, sự chân thật và thành tâm sẽ xóa tan mọi ranh giới khác biệt. Chỉ cần bạn yêu thật lòng thì những cách  biệt sẽ được giải quyết. Và sự hòa nhập văn hóa sẽ cũng  không là trở ngại nếu bạn chọn đó là quê hương thứ 2 thực sự của mình!
Mình rất xúc động trước hình ảnh của mẹ chồng tác giả, không cay nghiệt, không so đo xét nét, nhưng rất thẳng thắn và luôn sẵn lòng giúp con cháu bằng cả một tình yêu bao la.
Rồi ngay cả tác giả, cái cách cô đối xử với con chồng, chứng minh cho câu "mấy đời bánh đúc có xương" là không chính xác trong trường hợp này!
1 cuốn sách không quá phức tạp, không quá khó hiểu, nhưng rất nhẹ nhàng, sâu sắc, và đủ để bạn có thể đọc, cảm nhận và đọng lại trong bạn những cảm xúc tích cực!
4
389
2012-12-17 19:45:32
--------------------------
47652
9874
Đọc cuốn sách mình thấy nó có chút gì đó giống như phóng sự, bút ký, ghi lại những điều có thực trong cuộc sống. Chính vì thế mà ban đầu mình không thích thú lắm với cuốn sách. Nhưng càng đọc càng thấy nhiều điều thú vị và mới mẻ. Cuốn sách là tập hợp những hình ảnh, những lát cắt về cuộc sống phương Tây, nơi có những cặp vợ chồng khác biệt nhau về văn hóa cũng như lối sống. Tất cả được diễn đạt bằng ngôn ngữ nhẹ nhàng, giống như những lời tâm tình thủ thỉ. Một câu chuyện tưởng chừng như bình thường nhưng lại là một phần quan trọng của xã hội hiện nay. Và mỗi người trước khi đưa ra quyết định hôn nhân đều nên nghiền ngẫm những bài học cuốn sách mang lại.
4
20073
2012-11-24 22:09:44
--------------------------
