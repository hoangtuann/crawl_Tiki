397491
9867
Lần đầu mình đọc tác phẩm có cách kể chuyện bằng nhiều hình minh họa tí hon đến như vậy, cứ như đang xem clip kể chuyện đời tôi theo công cụ mindmap ý. Với cách vẽ hài hước, chữ to chữ nhỏ nguệch ngoạc, đánh dấu, làm nổi gây nhiều ấn tượng và giúp não nhớ câu chuyện nhanh mà không cần đọc nhiều chữ. Mà vốn dĩ không nhiều chữ đâu, cũng ngốn hơn 400 trang sách, vậy mà cuốn hút, vừa đọc vừa cười không bỏ xuống được.
Nhưng tiki giao 1 cuốn với nhiều vết ố nên mình phải đổi lại, cuốn sau hoàn hảo.
4
269266
2016-03-15 00:25:49
--------------------------
394680
9867
Tom gates những điều bất ngờ đại loại thế là một quyển sách nói về cuộc sống hằng ngày và đầy thú vị của cậu bé tom gates về gia đình và bạn bè xung quanh được minh họa bằng tranh vẽ khá ngộ nghĩnh, bìa sách cũng đẹp, cuốn sách nhìn qua khá là dày nhưng nội dung thì ngắn lũn và mình thấy quyển sách này thích hợp cho những bạn thiếu nhi hơn so với tuổi mình, với nội dung như vậy thì có thể nói là giá bìa hơi bị mắc so với giá trị của nó.
3
851304
2016-03-10 20:06:25
--------------------------
386335
9867
Toàn cảnh về cậu bé Tom nghịch ngợm và chứa sự xoay quanh học lực và cuộc sống hàng ngày vô cùng kinh ngạc , khám phá ra những trò chơi gắn liền với trí tưởng tượng của chú , để làm cho chú không phải bắt buộc quậy phá , vẫn là  hình ảnh về cách chú thích đơn giản , dễ đọc và còn giải trí dung dị , đọc được cho mọi lứa tuổi độc đáo ngay cả trong vốn có đẩy sâu đến văn hóa của con người , giá thành hơi cao so với truyện tranh thiếu nhi .
4
402468
2016-02-25 15:07:27
--------------------------
247713
9867
Tom Gates là series nhật ký mình thích nhất trong số những nhật ký dành cho teen hiện nay. Đặc biệt là một fan cuồng doodle và những hình vẽ ngộ nghĩnh thì không thể bỏ qua được truyện này, bởi vì hình vẽ trang trí cho bìa và từng trang sách phải nói là siêu dễ thương luôn í >< Nội dung thì không phải bàn rồi, rất rất hài hước, hóm hỉnh. Một cậu bé Toms nghiện bánh xốp, ghét Toán và những điều xảy ra hàng ngày trong cuộc sống cực kì lý thú. Một cuốn sách giải trí siêu tuyệt vời phù hợp với mọi lứa tuổi!
5
385556
2015-07-30 16:40:41
--------------------------
53237
9867
Cuốn sách được đầu tư rất đẹp về mặt hình ảnh.Nét vẽ rất dễ thương.Nội dung đơn giản,hài hước.
"Tớ là Tom Gates. Khi thầy cô không hướng mắt chiếu tướng tớ thì tớ hay ngồi vẽ bậy và nghĩ về cả đống cách chọc tức chị Delia. Cho nên thầy cô nghĩ tới hay lo ra trong lớp và thiếu tập trung. Thầy cô nghiêm khắc phát sợ luôn. vì bây giờ rõ là tớ đang tập trung hết sức coi coi tớ nên ăn cái bánh quy nào trước đây!" Đây là lời giới thiệu rất ấn tượng của một cậu học sinh lớp 5 vô cùng hiếu động và rắc rối. Tom Gates (T3): Những chuyện kinh ngạc (đại loại thế) tiếp tục mang đến độc giả, nhất là các em thiếu nhi hâm mộ, yêu thích nhân vật này, những câu chuyện xoay quanh đời sống thường nhật của Tom, cùng những mối quan hệ chị em, cha mẹ, thầy cô và bạn bè được mô tả qua lăng kính nghịch ngợm đúng với lứa tuổi học trò.
5
83615
2012-12-31 13:28:18
--------------------------
45363
9867
Mình gặp quyển này ở nhà sách và thấy nó toàn là hình vẽ loạn xì ngầu rất dễ thương, thế là mình tậu ngay. Quyển này còn đầu tư về hình vẽ nhiều hơn series Wimpy Kid nữa. Nội dụng chuyện xoay quanh cuộc sống của cậu bé Tom lớp 5, được tái hiện bằng lời kể hóm hỉnh, dễ thương và rất chân thật theo mạch thời gian giống 1 cuốn nhật kí vậy. Cách trình bày sách không theo kiểu viết đơn thuần mà bằng hình vẽ rất trực quan (mấy bé chắc sẽ rất mê khi cầm một quyển sách xinh như thế :D)
Tuy nhiên xét về tổng thể thì quyển sách có vài điểm trừ: giá thành hơi cao so với mặt bằng chung của văn học thiếu nhi, nội dung còn hơi đơn giản quá, một cuốn to kếch xù mà đọc khoảng một ngày là xong :( 
4
11661
2012-11-07 22:58:33
--------------------------
44761
9867
Tôi thích cuốn sách này vì phản ánh chân thực cách nhìn và suy nghĩ yêu ghét ,lo ngại xấu hổ của một cậu bé mới lớn về thế giới xung quanh ,đâu đó cũng gợi nhớ đến quãng thời gian lúc tôi từng là một đứa trẻ . 
Theo ý kiên cá nhân của tôi đây là một cuốn sách được đầu tư kĩ về nội dung lẫn hình thức  .Giọng văn hài hước hóm hỉnh nhưng vẫn rất nhân bản . Hình ảnh thị phạm giúp người đọc dễ hình dung ngụ ý của tác giả 
Cuốn sách phù hợp cho mọi lứa tuổi vì tính chất nhẹ nhàng , vừa giải trí vừa mang tính giáo dục cao .

5
68110
2012-11-04 01:10:41
--------------------------
