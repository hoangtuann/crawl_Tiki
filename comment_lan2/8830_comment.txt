371050
8830
Trình bày đẹp mắt, phù hợp với đối tượng là học sinh. Giấy trắng, chất lượng tốt, rõ ràng.
Cuốn từ điển này được thiết kế, sắp xếp theo mẫu tự bảng chữa cái của Việt Nam nên rất tiện nghiên cứu, thuận tiện và dễ sử dụng.  Có cả phiên âm Latinh, chữ Hán và nghĩ tiếng Việt cụ thể. Vốn và số lượng từ phong phú, đơn giản nhưng đa dạng, đa nghĩa.
Đây là một cuốn từ điển không thể thiếu cho những người đã, đang học tiếng Nhật, một công cụ cần thiết cho việc học tập và nghiên cứu tiếng Nhật.
4
748918
2016-01-18 21:18:42
--------------------------
248586
8830
Mình mới học tiếng Nhật dù rất khó khi sử dụng từ điển nhưng cũng mua để tra dần cho quen. Sau gần 3 tháng sử dụng thì mình thấy khá ổn. Mực in rõ ràng, giấy dày vừa phải. Cuốn từ điển nhẹ, cầm vừa tay, dễ lật chứ không quá dày. Có phần chữ nổi bật so với phần dịch nghĩa nên rất dễ nhìn và tìm điểm nhấn. Trình bày rõ ràng. Tuy nhiên vẫn có vài lỗi khiến mình không hài lòng. khoảng 3 trang gần cuối chữ bị chạy ra sát lề ngoài, nhìn rất khó chịu.Nói chung thì nó vẫn là một cuốn từ điển tốt.
4
139133
2015-07-31 10:06:09
--------------------------
119106
8830
Nếu như các bạn đang muốn có 1 cuốn từ điển Tiếng Nhật hay thì bạn không chần chừ nữa mà hãy mua cuốn sách này ngay. Nó thật sự rất hay. Nhìn tổng thể cuốn sách rất đẹp, mùa sắc bắt mắc, mực in khá rõ. Ưu điểm của nó khá tốt. Nhưng cũng có vài nhược điểm nhỏ như : ở trang có từ " tuần " , mực không đều khiến mặt giấy bên phải cột thứ 2 bị nhòe rất nhiều và không đọc được. Ngoài ra các chữ Kanji hơi nhỏ. Các bạn nên mua kính lúp nhé !  Tuy vậy, khuyết điểm ấy chỉ xuất hiện trong các từ có bộ thảo quá nhiều thôi !
4
298441
2014-08-02 13:36:34
--------------------------
83713
8830
Quyển từ điển này mình thấy có một số ưu và khuyết điểm sau đây. Về phần trình bày thì mình thấy có nhiều ưu điểm. Trình bày rõ ràng - điều cần thiết hết sức đối với một cuốn từ điển, phần chữ đủ rõ và sáng, cân bằng với phần phiên âm, mang lại những lợi ích thiết thực khi tra từ, về phần nội dung thì cũng có ưu điểm khiến mình khá hài lòng, đặc biệt là phần trình bày theo các nét nghĩa đi sau mỗi từ. Có phần giải thích rõ mỗi từ liên quan thì đi với các từ nào làm mình có thể dễ dàng phân biệt được. Duy chỉ có một khuyết điểm quan trọng mà mình nghĩ cần phải cải thiện đó là phần từ vựng các trang cuối sách bị thiếu nét, không hiểu chỉ do quyển sách của mình thôi hay các quyển khác cũng bị vậy, mong nhà xuất bản xem xét kĩ.
5
28320
2013-06-27 15:50:44
--------------------------
