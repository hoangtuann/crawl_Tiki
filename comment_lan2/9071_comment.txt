189820
9071
Bộ truyện " Tô màu truyện cổ tích Việt Nam" là một trong những bộ không thể thiếu trong kệ sách của các bé. Trong câu chuyện Cây tre trăm đốt các bé như lạc vào thế giới cổ tích ở đó có kẻ giàu bụng dạ tham lam độc ác, người nghèo lương thiện nhân hậu có một tấm lòng vị tha. Qua câu chuyện các bé có thể học được những bài học quý giá, những cách cư xử hay trong cuộc sống, hơn thế nữa còn giúp bé mở rộng tư duy. Truyện còn có phần tô màu giúp bé vừa đọc truyện vừa nhìn hình mình tô màu giúp nhớ lâu hơn mà cuốn truyện còn sinh động hơn.
5
519052
2015-04-28 08:16:10
--------------------------
