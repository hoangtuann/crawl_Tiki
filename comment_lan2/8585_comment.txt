496518
8585
Vì có lần mình học Lý luận văn học về tác phẩm Mười lẻ một đêm của Hồ Anh Thái và mình khá bất ngờ. Trước giờ mình rất ít đọc những tác phẩm trong nước. Nhưng khi đọc xong quyển đó thì ngay lập tức mình lên đặt tiếp quyển này. Và cũng không ngoài dự kiến, sách vẫn không làm mình thất vọng.
Mình không bàn về nội dung, chỉ nói về bên ngoài sách.
Sách khá nhẹ so với vẻ bên ngoài, không quá to cũng không quá nhỏ, cầm rất chắc tay.
Giấy trắng đục, đọc lâu không làm mình bị mỏi mắt.
5
482697
2016-12-19 07:51:23
--------------------------
412621
8585
Cõi người rung chuông tận thế là tiểu thuyết mang tính luận đề rất rõ của Hồ Anh Thái.  Cách đặt tên nhân vật,  cách tổ chức kết cấu,  sự sắp xếp các chi tiết đều hướng đến thông điệp: cái ác đang tồn tại khắp thế gian, và con người sẽ diệt vong nếu cái ác chiến thắng.  Nhân vật Mai Trừng như một kiểu con người - Bao Công nắm giữ sức mạnh siêu linh để chống lại cái ác,  nhưng cũng vì vậy mà cô không thể sống bình thường,  không thể yêu.  Cho nên cuộc chiến với cái ác là cuộc chiến nhiều cam go của con người, nhưng đó là cuộc chiến tất yếu và tất thắng 
4
1088790
2016-04-08 06:43:02
--------------------------
406415
8585
Ban đầu, tôi cứ nghĩ câu chuyện sẽ phát triển theo motif Tội ác và Trừng phạt. Nhưng hóa ra, cái vòng tội lỗi - báo thù cứ trùng trùng, tưởng như không thể nào hóa giải nổi.
Những kẻ vô minh, chưa thức tỉnh còn đắm chìm trong dục vọng, tội lỗi và vòng lặp oán thù trùng điệp như đẩy cõi người tới bờ vực tận thế. Tới khi nào con người mới tự tỉnh thức để cứu mình ?
Tác giả không dưới 1 lần đề cập trong tác phẩm của mình: hãy đi đám ma thật nhiều, hoặc phải chứng kiến những cái chết, sẽ thấy tất cả những bon chen, danh lợi, dục vọng chẳng còn nghĩa lý gì. Trước cái chết, trước ngày tận thế của cõi người, ai sẽ giác ngộ như Đức Phật ?
3
1225335
2016-03-28 11:34:44
--------------------------
386376
8585
Truyện sát thực tế nhưng không ảo mộng làm ta lãng quên nhanh chóng , cõi người rung chuông nói về sự hướng thiện sau khi chuyển thành điều cực kỳ sống mong muốn chịu khó bám chặt lấy trí nhớ của người từng làm theo cái ác xui khiến , cái xấu ác có ngày làm anh mất hy vọng vào bản thân mà trở về đạo lý thông thường chỉ để vượt qua sự bình thản của tâm hồn mỗi người trước khi buông bỏ sự tham lam , tàn bạo của đời người , tiểu thuyết suy đổi tâm tính 
4
402468
2016-02-25 16:26:15
--------------------------
256528
8585
Cảm nhận của mình về cuốn sách này : 
Cuốn sách " Cõi Người Rung Chuông Tận Thế " này của Hồ Anh Thái khiến mình ấn tượng và muốn mua ngay để đọc .
Ở cõi chốn nào, con người đi rung chuông tận thế chứ ?
Vâng . Cuốn sách mang hơi thở của văn học đương đại : trở về với đồi sống con ngừoi trần thế . Nghẹt thở, chẹt ép trong những ngừoi trẻ, với hoang mang tình yêu, tình ngừoi, nhục dục. sự trả thù ...

Rung lên hồi chuông tận thế ấy, liệu rằng, có phải để tận thế hay không? 
Mong bạn sẽ tìm ra được câu trả lời cho câu hỏi này , khi đọc tác phẩm.




4
323143
2015-08-06 22:15:11
--------------------------
223337
8585
Tôi rất ấn tượng vơi squas trình hướng thiện, hoàn lương của nhân vật “Tôi” trong "Cõi người rung chuông tận thế" ban đầu cũng đồng lõa với cái ác, quyết tâm tìm cô gái trẻ Mai Trừng để trả thù cho ba anh chàng Cốc, Bóp, Phũ. Nhưng không ngờ, quá trình tìm kiếm ấy lại là một quá trình hướng thiện. Anh ta nhận thức được cái ác, thấu hiểu giá trị của cuộc sống và nỗi đau của con người, sẻ chia với Mai Trừng cái sứ mệnh thiêng liêng đi trừng trị cái ác trong cõi đời này. Và có lẽ chỉ có tình yêu thương con người và sự thức tỉnh của con người mới có thể hóa giải, diệt trừ tận gốc cái ác, xây dựng cái thiện. Ngay chính bản thân nhà văn cũng tâm niệm về tác phẩm của mình: “Kẻ làm ác ở đây bị tiêu diệt bằng chính điều ác mà chúng định gây ra cho người lương thiện, một thứ hình phạt tự thân.
Đọc truyện, tôi thấy một không khí choáng ngợp, một lối văn đặc sắc đến tuyệt vời và chính điều đó đã khiến tôi không ngừng bồi hồi và xúc động!
4
616731
2015-07-06 22:34:28
--------------------------
151917
8585
Mạch truyện liền tù tì những cái chết, sự trả thù, nhưng xen vào đó là ngôn ngữ người Việt hôm nay. Không lôi thôi lòng thòng. Chi tiết cô đặc và đắt. Nó ám ảnh ở dòng mở đầu như phũ phàng và tiếng thở dài nhẹ nhàng khi cô gái được giải khỏi lời nguyền oan nghiệt, trở lại là người bình thường được sống như người chung quanh cô. "Qua lửa qua máu qua nước... là cõi bình yên".Người ta gấp sách lại và nghĩ như vậy về  "Cõi người rung chuông tận thế."....
5
126889
2015-01-21 15:02:14
--------------------------
128431
8585
Cuốn sách có bìa ấn tượng nhưng chữ tên tác giả quá to làm chìm hẳn tên truyện. Khi đọc xong cuốn sách, không có 1 nhân vật nào có thể khiến tôi yêu thích cả. Vì xuyên suốt câu chuyện, là 1 thế giới người lớn với những nhục dục tầm thường, hèn mọn, với những con người, dù tuổi trẻ, bề ngoài đẹp đẽ cùng tương lai rộng mở nhưng bên trong đã thối nát đến cùng cực. Tác giả đã xây dựng nên những nhân vật rất là châm biếm bằng cách nói dí dỏm nhưng chua cay ẩn giấu. Nếu câu chuyện này không có cô gái với năng lực kì lạ kia, cùng câu chuyện những tình nghĩa của những cô dân quân trong quá khứ thì có lẽ nó sẽ là 1 bóng đêm đau đớn đáng sợ. Tuy nhiên, tôi rất cảm ơn cuốn sách đã cho tôi thấy những điều ẩn khuất trong cuộc sống ngoài kia, nơi có những đau khổ và bất công hiện hữu hằng ngày, cùng sự thật trần trụi về con người mà nó đã lột tả trong đó.
3
203037
2014-10-01 22:43:50
--------------------------
119651
8585
Tôi có thể nói một khi đã cầm cuốn này lên thì các bạn không thể nào bỏ xuống được. Tiết tấu khá nhanh và hồi hộp, diễn biến vô cùng lôi cuốn, có thể cảm nhận được nhịp tim đập thình thịch khi đọc từng con chữ trong truyện này. Văn của Hồ Anh Thái thì hơi có xu hướng tàn nhẫn và bi kịch nên ai mà thích kiểu văn chương cổ tích, kết thúc có hậu chắc sẽ không hợp với lối viết của ông. Đọc Hồ Anh Thái, ta sẽ thấy sự hiện diện của cái xấu lúc nào cũng song hành với cái tốt, và dù cái xấu có bành trướng đến đâu đi chăng nữa thì vẫn còn chỗ cho cái tốt xuất hiện trong cuộc đời.
3
100136
2014-08-06 14:31:12
--------------------------
114055
8585
Khi đọc đến 1/3 quyển sách, tôi tin là mình nhớ một Hồ Anh Thái của Namaska - Xin chào Ấn Độ hay Tiếng thở dài qua rừng kim tước hơn, hoặc có lẽ vì tôi quen với những mẫu chuyện ngắn, nên lại thấy Cõi người rung chuông tận thế ê a quá. Từ cái chết của 3 chàng trai, dẫn về câu chuyện cuộc đời của người chú, lại chưa kịp yên vị thì đã quay về hiện tại với công cuộc trả thù cô gái bí ẩn. Hồ Anh Thái bắt người đọc dịch chuyển giữa các quãng thời gian quá nhiều, đang dang dở với mạch chuyện này đã chuyển hướng sang tình tiết khác. Nó khiến tôi bị rối, giống như bạn bày một bàn ăn thịnh soạn ra trước mặt độc giả, sau khi cho họ ngửi bao nhiêu là mùi vị thơm ngon, lại giấu mất đi, đưa họ gặm đỡ miếng bánh mì khô cháy.
3
75906
2014-06-08 15:07:24
--------------------------
109420
8585
"Cõi người rung chuông tận thế" - một cái tên vô cùng ấn tượng khiến tôi quyết định phải mua cuốn sách này sau 5s. Từ những dòng đầu của cuốn tiểu thuyết đã khiến người ta hồi hộp và không thể ngưng đọc.Những cái chết lần lượt của những chàng trai ở đầu tác phẩm mang hơi hướng bí ẩn, ma mị làm tôi cảm thấy vừa nuối tiếc vừa hả hê vì những hành động tội ác của họ, Giọng văn của HAT vô cùng chân thực mà lôi cuốn.
 Điều khiến tôi hơi bất ngờ là phần DƯ LUẬN ( mà chủ yếu phỏng vấn tác giả) chiếm hơn 1/3 cuốn tiểu thuyết ở cuối truyện. phần này còn đôi chỗ lặp lại ý kiến của nhau, có thể không cần thiết cho lắm.
Về chất lượng in ấn thì tuyệt vời. Sách nhẹ, thiết kế bìa của KD rất tuyệt :)
4
41952
2014-04-01 12:11:44
--------------------------
