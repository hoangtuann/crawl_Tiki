493189
9104
Mình đã mượn con bạn đọc quyển này.SAu đó lại đi mua.'quả là một quyển sách hay.Không rõ là làm sao nhưng mỗi lần đọc,mình thấy có cái gì rất là nhẹ nhàng vậy,phải đọc mới biết :)))) Khuyên nên mua nhé
5
1517452
2016-11-28 17:41:11
--------------------------
375495
9104
Mua cuốn sách trong một hội sách cách đây bao lâu chẳng nhớ vì thấy cuốn hút khi đọc phần giới thiệu nhưng sau đó lại để ở một góc, không đá động tới trong thời gian dài. Và khi đọc xong, tôi đã cảm thấy mình rất may mắn khi có một quyển hay như vậy. Tất cả mọi thứ vẽ nên tác phẩm là các nhân vật lớn bé, chính phụ, mỗi người một tính cách, môt suy nghĩ vô cùng thú vị. Cậu bé Tom, nhân vật chính dễ thương, chăm chỉ và hiếu thảo đã trải qua bao nhiêu chuyện vui buồn lẫn lộn vẫn giữ được nét đáng yêu của một cậu bé hiền lành. Tôi rất thích tình tiết cậu gặp phải chú mèo khi vào vườn hàng xóm, vui vô cùng, cùng với một loạt tình tiết khác khiến tôi không thể buông sách, dù trong quán ăn hay trên xe buýt. Nhưng tính nhân văn chính là điểm nhấn tuyệt vời cho tác phẩm khi lòng tốt của cậu bé đã được đền đáp xứng đáng từ rất nhiều người quanh em. Tựa "Cà chua ngọt" hoàn toàn phù hợp và xứng đáng với tác phẩm này.
5
258584
2016-01-28 14:56:46
--------------------------
311952
9104
Câu chuyện lấy bối cảnh ở một làng quê nước Pháp,gợi lên trong tâm trí người đọc một cuộc sống nông thôn giản dị,yên bình.Nhưng những nhân vật ở đây thì chưa hẳn yên bình.Mỗi nhân vật có một hoàn cảnh riêng,một câu chuyện riêng,nhưng đều có điểm chung là có chút gì đó hơi lộn xộn,phá cách,bát nháo.Nhưng chỉ một chút cái hỗn độn thôi được kể qua giọng văn nhẹ nhàng,giản dị mà hết sức chân thực chứa đựng nhiều ý nghĩa.Theo mình đây là một tác giàu giàu tính nhân văn bởi sâu xa trong đó là tình yêu thương con người,sự trách nhiệm và những nỗ lực hoàn thiện bản thân.Mình rất hài lòng với cuốn sách này
5
395480
2015-09-20 14:19:14
--------------------------
243414
9104
Mở đầu truyện Cà chua ngọt là hình ảnh của người mẹ trẻ con cùng nhóc Tom kém mình 13 tuổi. Giữa hai mẹ con không thể biết ai trưởng thành hơn ai, đôi khi mẹ Joss phải gắng gượng lắm mới tỏ ra mình “có ích” như một người mẹ, trong khi nhóc Tom thì lại cố gắng trở thành chỗ dựa cho người mẹ trẻ con của mình, cái cách cả hai người quan tâm lẫn nhau không giống tình cảm mẫu tử khác. Mẹ và con trưởng thành cùng nhau, mặc dù nghèo đói, ít khi nào hai mẹ con được bữa no nê nhưng vì hai mẹ con đều ý thức được hành vi của mình là không tốt và vì họ không phải là “kẻ cắp thực sự” nên họ chỉ “mượn ở những chỗ có nhiều”.  Bằng chứng là việc nhóc Tom sau khi thu được thành quả từ những cây cà chua giống, cậu đã làm sốt cà chua để lại dưới gốc như lời cảm ơn chân thành dành cho những người chủ vườn tốt bụng. Quả thực, trên đời này có rất nhiều chuyện lạ, trong đó có câu chuyện của nhóc Tom này. Cậu qua nhà hàng xóm lấy về nào là khoai tây, carrot và lần nào cũng “bị” lão mèo Achab phát hiện. Nhưng cũng như ông bà chủ tốt bụng của lão, họ cố tình lờ đi việc “mượn lương thực”, rồi có lần lại cảm thấy buồn vì đã mấy ngày rồi Tom không ghé qua…Kết thúc truyện là hình ảnh đôi mắt sáng lấp lánh của Tom khi kể về khoảng thời gian cậu đi cùng bố Samy, là nụ cười mỉm mãn nguyện của Joss. Cuối cùng thì hạnh phúc cũng đã gõ cửa nhà họ, hơi muộn, nhưng rất đáng để chờ đợi.
5
349095
2015-07-27 14:08:07
--------------------------
239608
9104
Một cuốn truyện thiếu nhi không giống những cuốn truyện thiếu nhi khác: cậu bé Tom 11 tuổi cùng bà mẹ Joss 24 tuổi. Tom ăn trộm rau củ quả của nhà hàng xóm. Joss mải chơi, mải yêu đương... mà theo đánh giá chung của xã hội là một bà mẹ hư đốn. Rồi bố của Tom xuất hiện: một ông bố mới ở tù ra!!!
Các nhân vật hết sức hỗn độn và bát nháo, nhưng điều quan trọng là dù hoàn cảnh sống của họ có khó khăn đến đâu, họ vẫn không để mất đi sự lương thiện của mình. Qua suốt 200 trang sách, họ dần thay đổi từng chút một, trở nên tốt đẹp hơn và hoàn thiện cuộc sống của nhau.
Cái kết ấm áp và hạnh phúc với tất cả các nhân vật: gia đình Tom đoàn tụ và bà Madeleine già cả cũng tìm được sự an ủi cuối đời.
4
293599
2015-07-24 00:03:07
--------------------------
234176
9104
Đọc Cà Chua Ngọt, tôi bắt gặp một đời sống giản dị ở miền quê nước Pháp, nơi có những nếp nhà giản dị và vườn rau quả xinh xắn. Ở đó, cuộc sống yên ả trôi mang theo những phận người khiến ta bồi hồi thương cảm. Đó là cô gái trẻ với cậu con trai chỉ kém mình mười ba tuổi, là bà cụ sống hiu quạnh cùng hai con vật già nua, là chàng nhân viên phục vụ tang lễ muốn làm lại tất cả sau những năm đi tù. 
Trái đất quả là rất tròn khi những con người, những số phận tưởng chừng xa lạ lại quy về một mối trong sự liên kết vô hình. Dưới sự sắp đặt tài hoa và ý nhị của tác giả, những mảnh sáng tối trong cuộc đời quá khứ của các nhân vật lần lượt được tỏ bày, được thể hiện một cách nhẹ nhàng và tự nhiên như hơi thở. 
Tôi rất thích cách tác giả cuốn sách thể hiện những cung bậc cảm xúc và tâm lý ở các nhân vật của mình. Chỉ một chút e dè của Tom trước con mèo ba chân, cảm giác “nóng cả người” và hối lỗi về chuyện “đi mượn” đủ thấy hành động của nhóc Tom là vì bất đắc dĩ và cậu bé không phải là đứa trẻ tham lam. Sự bối rối của Samy khi bắt đầu sửa chữa cuộc đời, những vui buồn của Joss, vẻ cuống quýt của vợ chồng ông Archibald khi mời khách đến dùng bữa... Để rồi sau tất cả là hạnh phúc giản đơn mà quý giá của đoàn tụ, sẻ chia, của cuộc sống bình yên và của tình người. 

4
113977
2015-07-20 10:04:48
--------------------------
233783
9104
Mình mua sách 1 cách tình cờ . Chủ yếu là vì sách giảm giá và bìa xanh rất bắt mắt . Cho đến thời điểm này mình đã đọc nó 3 lần và cũng đã chia sẽ cho bạn bè mình 1 tác phẩm nhân văn sâu lắng nhưng hết sức nhẹ nhàng . Cả cho trẻ con và người lớn . Mình muốn đọc cho con cái của mình , để dạy cho nó biết yêu thương mọi người , biết phân biệt tốt xấu dù trong mọi hoàn cảnh vẫn biết lựa chọn điều đúng đắn như nhóc Tom vậy
5
77327
2015-07-19 21:47:13
--------------------------
231599
9104
Bị ấn tượng bởi tên và màu bìa cuốn sách, mình đã mua và đã đọc từ rất lâu rồi nhưng lâu lâu vẫn hay lấy ra đọc lại một vài đoạn, mỗi lần đọc lại cảm thấy cuộc sống lại chìm vào cuộc sống bình yên ở làng quê nước Pháp. Mỗi nhân vật trong truyện lại mang một khuyết điểm riêng nhưng mỗi người lại biết tìm cách để che lấp những khuyết điểm ấy bằng những hành động rất đỗi nhỏ nhặt và bằng tất cả tấm lòng của mình. Màu xanh của bìa truyện khiến người đọc cầm lên cũng đã thấy bình yên rồi, nội dung truyện cũng như tên truyện, tuy là cà chua nhưng lại rất ngọt, rất dễ thương ^^.
4
109427
2015-07-18 10:55:19
--------------------------
165533
9104
Một quyển sách hay đầy tính nhân văn dành cho các bạn nhỏ của chúng ta thông qua nhân vật Tom đáng yêu luôn mỉm cười và có trách nhiệm đối với mọi người, mọi việc dù sống trong hoàn cảnh khó khăn đến nhường ấy. Nhóc Tom tuy còn nhỏ nhưng rất độc lập trong suy nghĩ cũng như tự lập trong mọi việc! Đáng khen biết bao cậu bé sống chung với người mẹ đơn thân nhưng vô cùng "trẻ con", cậu thậm chí phải chăm sóc ngược lại cho người mẹ của mình! Mình đã thật sự xúc động với cách xây dựng tính cách nhân vật Tom của Barbara Constantine, thật đáng yêu và đầy nghị lực, mình đã đọc review của các bạn trước khi quyết định chọn mua Cà Chua Ngọt để đọc cho con trai mình mỗi tối trước khi đi ngủ. Mặc dù mình phải giải thích cho con khá nhiều từ ngữ nhưng mình rất hài lòng với những gì mà con trai mình học được từ nhân vật Tom. Mong rằng tất cả con trẻ của chúng ta đều được bồi đắp và nuôi dưỡng nhân cách như cậu bé này!
4
151061
2015-03-10 14:58:12
--------------------------
129477
9104
Sau khi đọc "Cà chua ngọt" tôi đã có những cảm nhận như sau:
- Về nội dung: 
Câu chuyện “Cà chua ngọt” mặc dù có nguồn gốc bắt đầu từ bi kịch gia đình đổ vỡ, sự xâm hại trẻ em đáng kinh tởm nhưng bao trùm toàn bộ câu chuyện lại là những tình huống rất ngọt ngào, nhẹ nhàng, hài hước, những trang viết thấm đẫm tình yêu thương con người. Hệ thống nhân vật của câu chuyện mặc dù mang nhiều khiếm khuyết, tổn thương nhưng lại hết sức chân thật, hiền hòa, đáng yêu. Tác giả đã tạo dựng một câu chuyện mang nhiều đối lập giữa hoàn cảnh xã hội với cá nhân từng nhân vật để từ đó người đọc, nhất là những độc giả trẻ, sẽ tìm thấy cho mình bài học nhẹ nhàng mà sâu lắng về lòng can đảm, dám đối mặt với hoàn cảnh khó khăn để phấn đấu, để vươn lên, để tìm thấy niềm vui từ những điều giản dị, nhỏ nhoi trong cuộc sống bộn bề, những niềm vui xuất phát từ tình yêu thương đồng loại, từ bản thể trong sáng của chính mình qua những nhân vật trong câu chuyện. 
-Về nghệ thuật kể chuyện:
Tác giả Barbara Constantine đã kể lại câu chuyện bằng câu chữ mộc mạc, chân phương, giọng văn nhẹ nhàng mà sâu lắng, nhịp độ chậm rãi nhưng vững chãi đưa người đọc đến với bối cảnh của vùng nông thôn nước Pháp với khí hậu trong lành, những khu vườn, những dòng sông, những ngã đường tươi mát, rợp màu xanh, với những con người bình dị mộc mạc, luôn thông cảm, thấu hiểu cho mọi người xung quanh. Độc giả sẽ tìm thấy những giờ phút thanh bình chìm đắm trong thiên nhiên, nhâm nhi câu chuyện đậm tình người giữa những bon chen, những bộn bề của cuộc sống và nghe lòng mình lắng lại đâu đó giữa những ban mai, giữa màu xanh cây lá mơn man mát lòng… 
Như dịch giả Nguyễn Thị Tươi đã dịch cuốn sách và đặt tựa là “Cà chua ngọt” – những trái cà chua mặc dù bản thân mang vị chua của thực tế cuộc sống cay nghiệt nhưng vị ngọt của những giọt mồ hôi công sức cố gắng sống phấn đấu vượt lên trên mọi hoàn cảnh đã tạo nên sự đối lập. Để cuối cùng chỉ còn cái ngọt ngào dịu nhẹ lan tỏa sâu trong tâm hồn.
4
38615
2014-10-10 13:55:53
--------------------------
126923
9104
Mình mua sách vì nhìn bìa dễ thương và tên vui vui. Cách viết văn rất tự nhiên, như văn tự truyện nhưng dù vậy ko hề làm cho người đọc thấy nhàm chán. Thường thì mình ko thích kiểu văn tự truyện nhưng cuốn sách này làm mình phải suy nghĩ lại, thực sự nội dung rất hay. Truyện cảm động, tác giả quả là bậc thầy khi đã dẫn dắt đc người đọc theo câu chuyện của mình. Kết thúc bỏ ngõ nhưng theo mình viết đến đó là đủ rồi, phần còn lại do mọi người tự suy nghĩ thôi, có hậu hay ko đều do chúng ta cả. 1 điều nữa khiến mình thích cuốn sách này vì những nhân vật trong truyện, trc đó họ có thể ko tốt nhưng bây giờ họ đã cố gắng thay đổi từng ngày để tốt hơn, chẳng phải tuyệt vời sao ^^
5
21567
2014-09-21 16:28:45
--------------------------
90288
9104
Tôi đọc cuốn sách này vì 1 người chị giới thiệu nhiệt tình rằng nó rất vui và thú vị về cốt truyện, 1 thằng nhóc có bà mẹ hơn nó 13 tuổi rưỡi, không được phép gọi mẹ, mà phải gọi là chị, 2 mẹ con nương tựa nhau mà sống, và đúng là không biết ai trưởng thành hơn ai. Khi đọc cuốn sách này thì nó không buồn cười như chị tôi kể, nhưng lại đọng trong tôi những cảm xúc rất nhẹ nhàng, cốt truyện về những mảnh đời với những nỗi niềm riêng, ai cũng có quá khứ không hoàn hảo, nếu không muốn nói là tồi tệ, nhưng mỗi người họ đều hướng thiện, và luôn tìm kiếm yêu thương, có thể sợi dây vô hình đã gắn kết họ với nhau. 1 kết thúc có hậu và hạnh phúc, mỗi nhân vật đều đã tìm được niềm vui trong cuộc đời của mình, đó chỉ là hạnh phúc đơn giản, khi ta nhìn những thứ xung quanh mình với 1 góc nhìn khác đi, Tôi cảm thấy vui khi gấp cuốn sách lại ^^. 
4
66696
2013-08-08 22:34:52
--------------------------
82993
9104
Đọc xong cuốn sách " cà chua ngọt" quả là ngọt ngào thật. Mình thích nhất nhóc Tom, dù là mới ít tuổi nhưng có tấm lòng nhân hậu, biết yêu thương mọi người xung quanh và nhất là người mẹ " trẻ con" của mình. Cậu không nghĩ cho bản thân mình mà nghĩ cho mẹ và bà già hàng xóm trước. 
Câu truyện có tính giáo dục sâu sắc cho giới trẻ hiện nay, không nên có lối sống buông thả để rồi phải chịu những hậu quả khó lường. Giáo dục con người ta sống phải  có tấm lòng nhân hậu, yêu thương đùm bọc mọi người trong gia đình nói riêng và hàng xóm nói chung.
Có những lúc cười ngặt nghẽo nhưng có lúc chùn xuống vì tình cảm của nhóc Tom với mẹ cậu.

Một cuốn sách nhẹ nhàng đáng để đọc.
4
89737
2013-06-24 15:59:32
--------------------------
74970
9104
Một câu chuyện nhẹ nhàng, cảm động, đủ để chạm đến tâm hồn của mỗi người.

Tôi thích cách xây dựng nhân vật của Barbara Constantine, một cậu bé Tom phải trộm cắp để sống qua ngày, có thể bạn sẽ ghét cậu ta vì dù gì thì ăn cắp cũng là một tật xấu, và việc cậu xóa hết dấu vết lại càng chứng tỏ cậu là một con người ranh ma. Tuy nhiên, không hiểu sao tôi không ghét cậu ấy tí nào.

Tôi vẫn thích Tom vì cậu có lòng thương, cậu có thể bỏ mặc bà Barbara Constantine té trong vườn nhưng cậu lại không thế. Một cậu bé có một người mẹ trẻ vì lí do bất đắc dĩ mới sinh ra cậu, không ai muốn, nhưng lại tốt bụng và đầy lòng yêu thương.

Đây là cuốn sách dạy ta tình yêu, dạy ta không được đánh mất lòng tốt của một con người ngay cả khi ở trong hoàn cảnh khó khăn nhất, dạy ta biết thế nào mới thực sự là khổ, dạy ta biết vực dậy đúng lúc, dạy ta không từ bỏ hi vọng.

Tôi sẽ trân trọng cuốn "Cà chua ngọt"♥
4
104997
2013-05-16 08:38:49
--------------------------
67434
9104
Ko có gì quá to tát, ko lên gân cũng chẳng kêu gào, một câu chuyện nhẹ nhàng, cảm động và sâu lắng. Mỗi một nhân vật, dù chỉ là thoáng qua, hay xuyên suốt toàn bộ tác phẩm đều hiện lên rất thực, rất đời.

Một cậu bé con đi trộm cắp rau quả ở những nhà hàng xóm chung quanh nhưng luôn theo một quy tắc "biết đủ là đủ". Ko phải em cho rằng việc đó là bình thường, em nào có muốn, nhưng dòng đời đã đẩy em đến bước đường cùng. Dẫu có thế nào, em cũng cương quyết chẳng đẩy lương tâm mình vượt quá lằn ranh.

Một bà mẹ trẻ vì dòng đời và lũ đàn ông háo sắc mà trở nên có phần cay nghiệt nhưng ko vì thế mà bớt hy vọng vào một tương lai tươi sáng hơn. Cô đã từng bước thay đổi, đã biết thế nào là tình mẫu tử, là tình yêu đích thực và tình yêu cuộc sống hiện hữu trong cô.

Một anh chàng đã từng vào tù ra tội, quyết thay đổi mình, ngỡ ngàng vì sự cô độc sau mười năm, bất ngờ nếm trải vị ngọt của tình thân.

Một bà già lú lẫn nhưng ẩn chứa cả một câu chuyện dài về cuộc đời đầy đau thương và nước mắt. Cặp vợ chồng già hàng xóm tốt bụng. Người phụ nữ cho đi nhờ xe. Ông bác sĩ già,... Tất cả làm nên một câu chuyện rất gần gũi với mỗi chúng ta.

Đọc xong, chợt nhủ lòng, làm người tốt cũng có điều tốt đấy chứ, và quan trọng nhất là phải biết mở lòng đón nhận, cũng như cảm ơn một cách chân thành vì những điều tốt đẹp mà cuộc sống này mang đến cho ta!
5
6576
2013-04-07 08:26:15
--------------------------
