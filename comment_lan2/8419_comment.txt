463194
8419
Mới đầu mình muốn đọc cuốn sách chỉ vì tò mò về sự nổi tiếng của tác phẩm này, sau khi đọc thì thực sự trở nên say mê nó bởi lối hành văn vô cùng nhẹ nhàng của tác giả, những món ăn trong tác phẩm do mẹ cô bé Laura nấu hay những bụi hoa quả rừng như dâu dại thực sự khiến mình vô cùng thích thú, một câu chuyện về gia đình thật êm đềm và ấm áp, từng câu chuyện nhỏ đó đã khiến cho người đọc dần nhận ra gia đình cô bé Laura thật giàu có và hạnh phúc biết bao, dù có lúc gia tài lớn nhất của họ chính là gia đình họ
5
1418564
2016-06-29 09:08:06
--------------------------
388371
8419
Bắt đầu series ở tập 3 , càng đọc càng yêu series này... !                                    Yêu những con người bất khuất, ko ngại gian khổ, chống chọi vs thiên nhiên để tạo ra những thành quả tươi đẹp! Tập 3 kể về người dân trog thị trấn Desmet chống chọi vs mùa đông khắc nghiệt nhất, dài nhất ( 27 năm/ lần ) ... Dẫu khó khăn nhưng họ vẫn can trường: thị trấn bảo bọc cho nhau trong mùa đông lạnh giá, Cap Garland và Almanzo wilder vượt bão tuyết để mua lúa giống cho thị trấn ... ! ! !
4
803518
2016-02-28 20:39:36
--------------------------
305094
8419
Tôi đã đọc đi đọc lại cả bộ NNNTTN không biết bao nhiêu lần, và đặc biệt là tập 6 này. Cả tập truyện chỉ toàn một màu trắng của tuyết, nhưng lại khiến người ta ấm áp và tràn đầy hi vọng. Họ yên lặng và hi vọng giữa mênh mông bão tuyết suốt  bảy tháng trời....
Tôi thật  lòng rất yêu thích series truyện này bởi những hình minh hoạt nhỏ đáng yêu, chất giấy nhẹ xốp, màu vàng dịu mắt.
Tuy nhiên, loại giấy này rất dễ ố nếu không được giữ gìn cẩn thận.
Nói tóm lại là hoàn toàn hài lòng về bộ NNNTTN lần này của NXB Kim Đồng, và hi vọng có được thêm nhiều bộ sách được xuất bản một các đầy đủ và đúng với bản gốc như thế này nữa.

5
36188
2015-09-16 20:40:48
--------------------------
260783
8419
Ngôi Nhà Nhỏ Trên Thảo Nguyên - Tập 6 Mùa đông bất tận của tác giả Laura Ingalls Wilder là một cuốn sách cực hay. Câu chuyện về gia đình Laura chống chọi với một mùa đông băng giá trong thời gian dài là bài ca ca ngợi sự kiên cường bền bỉ của con người trong cuộc chiến bất tận với tự nhiên. Văn phong của tác giả tự nhiên trong sáng giản dị, không hoa mỹ nhưng rất sâu sắc. Tôi rất thích hình vẽ của bìa sách cũng như chất liệu giấy in- giấy màu ngà xốp rất đẹp và cổ điển.
5
167283
2015-08-10 17:38:12
--------------------------
114007
8419
Mình tìm đến với bộ truyện Ngôi nhà nhỏ trên thảo nguyên một cách rất tình cờ và đó là một sự tình cờ thú vị. Bắt đầu với tập 3 "Trên thảo nguyên", mình đã dần dần tìm đọc tất cả các tập trước và sau đó cho đến quyển cuối cùng. Có người bạn đã nói vơis mình khi thấy mình cầm trên tay tập 6 này rằng :"Tập truyện này rất chán vì từ đầu đến cuối chỉ nói về một mùa đông". Nhưng thật ra càng đọc mình càng bị cuốn hút. Cả một tập truyện dày miêu tả cảnh Laura và gia đình và cả thị trấn phải gồng mình chống chọi với một mùa đông tưởng chừng không bao giờ rời đi, tuy vậy, câu chuyện không nhàm chán một chút nào. Một chuỗi những hoạt động, sự kiện, tâm lý và tình huống đan xen khiến bạn không thể ngừng đọc khi chưa biết số phận của từng nhân vật sẽ kết thúc ra sao khi đối mặt với thiên nhiên khắc nghiệt. Cách kể chuyện của Laura Ingalls đã tạo nên một bức tranh sống động và chân thực hết sức, cho người đọc cảm giác xót xa lẫn khâm phục sức sống và lòng quả cảm của con người không chịu khuất phục trước thiên nhiên và ngay cả sức chịu đựng của chính bản thân mình.
5
300926
2014-06-07 22:13:09
--------------------------
