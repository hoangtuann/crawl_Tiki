473032
8450
Cuốn sách “Horrible Histories – Người Viking hung bạo” của tác giả Terry Deary giới thiệu cho chúng ta rất nhiều thông tin về người Viking - một tộc người cổ ở Bắc Âu (Na Uy, Thuỵ Điển, Đan Mạch). Họ rất giỏi về nghề hàng hải và tài ba trong chiến trận. Người Viking thường không xâm chiếm đất đai để cai trị mà chỉ gây chiến và tạo thế lực buôn bán ở Bắc Âu và Đông Âu. Họ là nỗi lo sợ của đa số dân cư sống rải rác ven biển vì họ to lớn, mạnh bạo và dã man.
5
470567
2016-07-10 10:33:14
--------------------------
467553
8450
Học lịch sử qua sách vở quá khô khan và nhàm chán thì hãy tìm đến series Horrible Histories này đi các cậu. Mình cực thích luôn. Đọc vừa nâng cao kiến thức vừa giải trí. Thông tin trong cuốn này không hề nhẹ đâu nhưng rất dễ ngấm vì lối diễn đạt hài hước hóm hỉnh, hình vẽ sinh động, rất buồn cười và thú vị đấy. Dành cho cả trẻ em và người lớn, nên tìm mua trọn bộ để đọc, sách mỏng nhẹ đơn giản thôi, giá cũng rất nhẹ nhàng nên mua ngay đi nhé. Đảm bảo không hối hận.
5
569970
2016-07-03 23:21:16
--------------------------
284681
8450
Sau khi đọc xong sách, mình cảm thấy hơi rùng mình đó, nhưng chắc chắn mình sẽ đọc lại nó lần nữa thôi. Hay ơi là hay, nhưng huyền thoại của người Viking nè, những tục lệ và đời sống hằng ngày của họ nữa. Họ xây dựng của một đế chế hùng mạnh trên biển cả luôn nhé. Những thuyền buôn chỉ cần nghe tới tên những người Viking là đã phát khiếp lên hết rồi. Tuy nhiên, họ cũng có lòng dũng cảm và tự trọng cao lắm nhé. Sách đẹp, in rõ ràng, giấy tốt ... hình minh hoạ rất vui !
4
153585
2015-08-31 10:06:37
--------------------------
257345
8450
Trong cuốn sách này có những thông tin bổ ích mà trước giờ mình chưa bao giờ biết đến . Người Viking có những phong tục rùng mình , những huyền thoại đầy khổng lồ gầm gừ cùng những thần lùn xấu tính . Ngoài ra mình còn được biết tại sao dân Viking lại có những cái tên quái dị như Mông Mỡ, Cả Thộn , Hôi Rình , . . . hay làm cách nào để đục chiếc thuyền dài kiểu Viking , . . . Về mặt hình thức thì cuốn sách này khá tốt , màu in đẹp , giấy đọc không bị mỏi mắt và cách viết văn thì hài hước . 
3
295212
2015-08-07 16:30:35
--------------------------
251790
8450
Chắc hẳn bạn đã ít nhiều nghe nói đến người Viking và nền văn hóa Viking rồi. Ngay  cả trong sách lịch sử của chúng ta cũng có nói đến họ, nhưng quá sơ sài và chẳng có gì thú vị. Ở đây tác giả Terry Deary dành nguyên một tập sách để cho chúng ta thấy cuộc sống của họ như thế nào, họ làm gì và có những tập tục gì... Mọi thứ vừa bí ẩn vừa hay ho, và chẳng ai từ chối đọc những câu chuyện nhỏ hấp dẫn về một nền văn hóa hấp dẫn như thế này cả. Nào mời bạn, chúng ta cùng khám phá.
5
82774
2015-08-03 10:33:20
--------------------------
249087
8450
Tình cờ sau khi xem xong "How to train your dragon", trong lúc đang cực kỳ hứng thú về những nhân vật người Viking trong phim thì tôi lại được em trai tặng cho cuốn sách cực kỳ hay ho về tộc người này. Cứ lật qua mỗi trang sách, tôi như được bước vào một cuộc phiêu lưu mới. Được sống cùng với người Viking, tìm hiểu về những thói quen hàng ngày của họ, thậm chí là được tham gia vào những cuộc chiến đẫm máu của họ, đầu óc tôi như bị choáng váng vì những trải nghiệm ấy quá lí thú! Bên cạnh đó, hình minh họa trong sách cũng cực kỳ dễ thương, cùng với lời dẫn dắt theo lối kể chuyện của tác giả cũng khiến tôi không thể nào dứt ra khỏi cuốn sách này được. 
5
580478
2015-07-31 12:46:17
--------------------------
248284
8450
Cảm nhận sách dịch hơi bạo lực, bản gốc chắc cũng thế vì nhìn tiêu đề cũng cảm nhận được rồi, có lẽ vì thế mà nó dễ gây ấn tượng và thú vị vậy không, vì mình cũng khá thích xem phim kinh dị. Sách này mình nghĩ nên dành cho học sinh cấp 2 trở lên đọc thì hay hơn, lợi ích lớn nhất là giải trí, thứ 2 là tăng hiểu biết. Truyện có khá nhiều hình minh họa dễ thương, đọc cũng khá buồn cười, đọc để hiểu một phần lịch sử các nước Bắc Âu và con người nơi đây,
4
588698
2015-07-31 09:44:18
--------------------------
243919
8450
Đến với cuốn sách này, chúng ta sẽ được biết những câu chuyện hài hước nhưng cũng đầy bạo lực trong lịch sử của người Viking - một bộ tộc man rợn nổi tiếng trong lịch sử nhân loại. Bên cạnh đó là những nét văn hoá phong tục kì lạ và thú vị của họ, cũng như lối sống hung hãn mà rất dũng cảm, tự trọng của bộ tộc này... Sách có hình vẽ và những câu hỏi liên quan đến nội dung đọc nên người đọc sẽ thấy thú vị hơn và không cảm thấy mệt mỏi khi đọc.
4
113650
2015-07-27 20:55:09
--------------------------
243710
8450
Ăn như viking, sống như viking, phục trang như viking... mình một lần hoá thân vào cuộc sống viking khi bước vào quyển sách này. Ngôn ngữ của tác giả gần giống như ngôn ngữ nói, cùng với những hình ảnh minh hoạ vẽ nhưng cực kì sống động, làm cho những câu chuyện huyền thoại của tộc người Viking hiển hiện trên trang giấy. Bạn có thể tìm kiếm thông tin ở bất cứ đâu, nhưng cũng không đâu có thể độc đáo, dễ thương và tràn đầy tính "cá nhân" của tác giả như quyển sách này. Nhờ vậy mà những thông tin trong đây cũng như trong cả series Horrible rất dễ nhớ. 

5
120276
2015-07-27 17:22:49
--------------------------
243294
8450
Tôi rất thích đọc sách của Horrible Histories, những dữ liệu, con số, những trang lịch sử khô khan không còn khó nhằn qua những trang sách dí dỏm và hài hước. Thích tìm hiểu về người Viking- một tộc người sống ở Bắc Âu và thần thoại các vị thần Viking, tôi đã phần nào thấy được cuộc sống và tính "hung hăng" của họ :). Cuốn sách rất hữu ích. Đồng thời, những ai muốn tìm hiểu thêm về các vị thần Viking như Odin, Thor hay Loki ... thì có thể tìm đọc cuốn 10 huyền thoại Viking hay nhất mọi thời đại, cũng rất tuyệt vời!
5
705146
2015-07-27 13:03:41
--------------------------
227364
8450
Nằm trong series Lịch Sử Kinh Dị của tác giả Terry Deary nên với những ai mê khoa học, nhất là bộ môn lịch sử thì khó có thể bỏ qua quyển sách bổ ích này. Những kiến thức lịch sử khô khan về những huyền thoại Viking trong quyển sách này được hô biến thành những cuộc hành trình, câu chuyện, trắc nghiệm và những hình ảnh hài hước lý thú giúp cho những ai ngán đọc cũng đều dễ hiểu, dễ nhớ và trở nên yêu thích. Những bí mật ẩn sau, những cuộc chiến đổ máu được tái hiện khá đầy đủ, toàn cảnh, những vị thần trời ơi đất hỡi, những cái tên buồn cười hay những lý do kỳ quặc nhất,... Tóm lại đây là quyển sách bổ ích không thể thiếu trong tủ sách của những con mọt mê khoa học ^ ^
5
382812
2015-07-14 06:55:31
--------------------------
222822
8450
Có lẽ trong chúng ta ai cũng biết rằng sự dũng cảm và mạnh mẽ và có phần hung bạo của người tộc người Viking lừng danh này nhưng chúng ta vẫn chưa hiểu rõ hết về họ. Qua lời văn dí dỏm, dễ hiểu, hình ảnh minh họa sinh động, ngộ nghĩnh trong cuốn sách “ Viking hung bạo” sẽ giúp chúng ta hiểu rõ hơn về họ, về cách họ tồn tại, về cách mai tang khi chết hay hơn nữa là lịch sử của bộ tộc này, … Một cuốn sách thú vị và bổ ích cho những ai muốn tìm hiểu về dân tộc Viking mạnh mẽ này.
5
554214
2015-07-06 11:01:51
--------------------------
216161
8450
Horrible Histories của Terry Deary thích hợp cho mọi độc giả, mọi lứa tuổi có thể dễ dàng tiếp cận với lịch sử hơn so với những quyển sách lịch sử thông thường khác dù độ dày của nó có dày hơn đi nữa.
Những vấn đề giết chóc, máu me hay những vấn đề dễ gây ảnh hưởng đến tâm lý được trình bày dưới những bức tranh dí dỏm và hài hước có thể giúp độc giả được tiếp thu một cách thông thường. 
Khi đọc "Viking tàn bạo", tôi cảm thấy mọi góc nhìn đều khá ngộ nghĩnh, và người xưa có những tư tưởng quá bá đạo mà thời nay khó mà tưởng tượng được ^^
5
516261
2015-06-27 11:18:46
--------------------------
207519
8450
Người Viking là một dân tộc được nhắc nhiều đến trong các bộ phim với những hình ảnh có phần huyền bí. Tuy nhiên khi đọc cuốn sách này chúng ta lại có một cái nhìn khác về dân tộc Bắc Âu này. Họ không hoàn toàn là dân tộc bạo lực, man rợ hay độc ác như chúng ta vẫn tưởng tượng. Tác giả đã cho thấy một góc nhìn thật hơn về người Viking. Cũng như những cuốn khác trong bộ Horrible History, cuốn sách này trình bày các vấn đề lịch sử dưới góc độ hài hước, hình ảnh minh họa giản dị, dễ hiểu, thích hợp cho bất cứ ai yêu thích và muốn tìm hiểu thêm về lịch sử châu Âu
4
507583
2015-06-12 15:30:46
--------------------------
201993
8450
Nhắc đến Viking, có lẽ bạn nghĩ ngay đến những hình ảnh máu me, bạo lực và kinh dị... Cũng đúng, ở cái thời đại ấy, con người buộc phải man rợ với nhau để tồn tại. Tuy nhiên, sự thực lịch sử ấy được các tác giả đưa vào sách và trở nên nhẹ nhàng hơn rất nhiều và không kém phần hài hước. Sách sẽ kể về những câu chuyện thần thoại Viking lý thú, những cái tên Viking lạ kỳ, những phong tục Viking đặc sắc và những con người Viking hết sức chân thực. Sách trình bày đơn giản, nét vẽ minh họa dí dỏm, không màu mè, thích hợp để dành cho những ai ngán lịch sử khô khan, loại lịch sử hàn lâm... Mặc dù vậy, Viking hung bạo vẫn rất nghiêm túc về vấn đề kiến thức.
5
564333
2015-05-28 17:19:25
--------------------------
195582
8450
Đây là cuốn sách thứ ba mà mình đọc trong bộ sách Horrible Histories. Quả thực nó không làm mình thất vọng. Lối viết của tác giả rất tươi vui, tự nhiên, hài hước dù chứa đựng các kiến thức hàn lâm. Thay vì cứ viết ra những sự kiện lịch sử theo dòng thời gian, viết ra những đánh của tác giả cũng như của mọi người, tác giả đã cải tiến, cải biên lối viết ấy thành nhiều hình thức diễn đạt khác nhau như viết nhật ký, chuyên mục báo, chương trình truyền hình,... rất hài hước, tạo cảm giác thân quen hơn. Vì vậy nên những hình minh họa cũng sinh động và hài hước. Chất liệu giấy làm mình có cảm giác hoài cổ, mùi thơm thơm dễ chịu. Mình nghĩ quyển sách này phù hợp với mọi lứa tuổi.
5
45656
2015-05-14 05:15:57
--------------------------
