356019
9500
Sách này mình được tặng trong chương trình giảm giá của trên tiki nên không biết sách hay dở ra sao. Nhưng khi nhận sách thì sách bị rách ở một trang nên hình thức không được đẹp. Về nội dung sách chứa nhiều cảnh nóng không thích hợp với độ tuổi của mình, đọc có những cảnh nghe rờn rợn. Về ý nghĩa mình thấy khá hay bàn về vấn đề hạnh phúc qua một con người cụ thể để từ đó mỗi người có suy nghĩ riêng về 2 từ hạnh phúc. Nói chung cuốn sách không thích hợp với những người trẻ cho lắm nhất là những người thích mơ mộng bởi sách nghiêng về dòng văn học hiện thực hơn
3
637158
2015-12-21 09:27:41
--------------------------
52932
9500
Ngay từ những trang sách đầu tiên, mình đã bị lôi cuốn bởi cách viết hết sứ tinh tế và sâu sắc, cách dẫn dắt truyện nhịp nhàng và tài tình của tác giả. Qua từng trang, người đọc như được sống cùng nhân vật chính, được trải qua những cảm xúc chân thực nhất mà nhân vật phải chịu đựng. Câu chuyện về nhà triệu phú Sitvo là một câu chuyện giàu cảm xúc, một hành trình kỳ lạ và gian nan trên đường đời. Người đọc cảm phục tinh thần mạnh mẽ của cậu, vui với những thành công cậu đạt được, nhưng cũng đồng cảm với những mất mát, bất hạnh cậu gặp phải. Qua câu chuyện này, tác giả đã cho chúng ta thấy, tiền bạc và sự xa hoa không làm nên hạnh phúc thực sự của mỗi người, hạnh phúc nằm ở trong chính trái tim và tâm hồn chúng ta. Đây là một tác phẩm hay, hấp dẫn, gửi gắm được biết bao thông điệp tuyệt vời.
4
20073
2012-12-29 14:19:06
--------------------------
