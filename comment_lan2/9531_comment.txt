209276
9531
lịch sử việt nam giai đoạn Trịnh Nguyễn phân tranh rất phức tạp, ở ngoài miền Bắc thì nhà Trịnh hiện đang vào lúc suy yếu và sự tranh gìanh ngôi chức giữa các con của Trịnh Sâm đã gây nên cảnh đau thương tang tóc cho cả một nửa đất nước. đôi khi vận mệnh quốc gia lại phụ thuộc vào quyết định của một vài người... bố cục rõ ràng, nét vẽ sắc  sảo và lời văn hay hấp dẫn người đọc, tưu liệu lịch sử vô cùng phong phú qua những  câu chuyện lịch sử kể cả trong chính sử , và cả trong dân gian... nhiều nhân vật lịch sử được kể lại với nhiều tình tiết thú vị tái hiện lại một khung cảnh phong kiến xưa đầy chi tiết.
5
568747
2015-06-17 10:55:45
--------------------------
48002
9531
Mình đã được học những bài giảng lịch sử về thời kỳ Trịnh-Nguyễn phân tranh nhưng chưa thực sự hiểu hết về thời kỳ này. Nhưng khi cầm trên tay và đọc cuốn sách tranh này của NXB Trẻ, mình mới có thêm nhiều cảm nhận mới về lịch sử cũng như tiếp thu kiến thức dễ dàng hơn. Những sự kiện, biến cố, sự hoán đổi ngôi vị cũng như con đường sụp đổ của một triều đại, tất cả được thể hiện qua những hình vẽ sinh động và lôi cuốn, giống như một câu chuyện gần gũi với tất cả mọi người. Không có sự dài dòng khô khan của hầu hết các sách lịch sử khác, cuốn sách tranh này đem đến những cách tiếp cận mới mẻ và hấp dẫn cho mọi lứa tuổi, đặc biệt là các bạn trẻ.
4
20073
2012-11-28 13:35:18
--------------------------
