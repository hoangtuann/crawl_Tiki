288320
9941
Cuốn sách được trình bày rất rõ ràng, dễ hiểu, khoa học. Chia thành từng phần (từng dạng riêng) kèm theo lý thuyết và bài tập giúp người đọc dễ theo dõi cũng như nắm bắt bài. Đặc biệt ở phần tóm tắt kiến thức khá đầy đủ, bám sát sách giáo khoa, chia thành từng đề mục rõ ràng. Có khá nhiều bài tập từ dễ đến khó, rất thích hợp để tự luyện, có những mẹo trắc nghiệm nhanh.
Đây là một cuốn sách bổ ích cho các bạn tự học cũng như cần thêm tài liệu để củng cố kiến thức, luyện thi đại học.
4
452822
2015-09-03 15:12:09
--------------------------
246644
9941
Cuốn sách khá hay về phương diện giải nhanh các bài tập hóa, cung cấp nhiều công thức giải nhanh hữu ích đối với các bài toán hóa khó và cực khó. Không những thế cuốn sách còn giúp mình tiếp cận với lý thuyết hóa học một cách dễ dàng, giúp mình học lý thuyết không còn cảm thấy mông lung nữa mà rất cô đọng dễ hiểu dễ nhớ. Bên cạnh đó không thể phủ nhận rằng cuốn sách là hoàn hảo mà nó còn thiếu sót nhiều dạng bài tập cả lý thuyết lẫn toán hóa cho nên các bạn cần tham khảo các tài liệu khác nếu muốn phủ sóng hết các dạng bài.
4
77186
2015-07-29 18:20:54
--------------------------
