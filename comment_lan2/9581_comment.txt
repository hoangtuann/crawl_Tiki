398515
9581
Tôi thật là thất vọng khi mua quấn sách này. Tôi nghĩ là quấn sách này dậy cho chúng ta biết cách tự tin trong cuộc sống tôi tưởng đó là quấn sách hay. Tôi tưởng trong sách là một kho kiến thức để dúp chúng ta tự tin hơn nhưng trong sách toàn là những mẫu chuyện ngắn đọc không cảm thấy thú vị. Cũng chả dúp tôi tự tin hơn phần nào. Thành ra đây là một cuấn sách dành cho trẻ em. Để cho chúng tập đọc những câu truyện trong đó ! Thất vọng tràn chề khi bỏ tiền ra mua
2
1178372
2016-03-16 13:04:03
--------------------------
342485
9581
Em mình rất tự ti nên mình đã mua tặng em nó. Cuốn sách này không những dành cho học sinh mà phụ huynh đọc cũng rất tốt trong việc giúp đỡ khuyến khích động viên con cái. Ai cũng có thói quen so sánh giữa người này với người kia, giữa bản thân với kẻ khác. Cái gì cũng có hai mặt tốt và xấu. Sự so sánh đó cũng vậy, mình cho đó là những sự so sánh khập khễnh. Vì một người thì sẽ là chính họ chứ không phải ai khác thế nên họ chỉ cần giống bản chất của họ mà thôi. Và khi so sánh cũng nên đặt trên cùng một bình diện công bằng. Những sự so sánh khập khễnh sẽ khiến cho con trẻ ngày càng tự tin và đánh mất chính mình. Mà tự tin là thứ thiết yếu của con người , bên cạnh đó thêm sự nỗ lực thì thành công sẽ không quá khó khăn.
5
717149
2015-11-24 15:48:59
--------------------------
