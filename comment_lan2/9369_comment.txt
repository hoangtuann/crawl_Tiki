293619
9369
Mình từng có 1 chuyến du lịch ở Trung Quốc. Hướng dẫn viên giới thiệu rất nhiều về các vị tham quan xưng bá thét tiếng ở triều đại thời phong kiến TQ, mình có thể hình dung được bối cảnh lịch sử lúc bấy giờ một cách rất chân thật. Và một vị khách du lịch cùng đoàn cũng có nhắc đến quyển sách này và quyển "Đàn ông đàn bà Trung Quốc", được phiên dịch và sản xuất ở Việt Nam, phản ánh rất thẳng thắn bối cảnh xã hội và giúp đọc giả có cái nhìn sâu sắc qua các câu chuyện. Những ai thích tìm hiểu và khám phá lịch sử có thể tin mua cuốn sách này.
5
296457
2015-09-08 14:56:12
--------------------------
74942
9369
" 12 Đại tham quan Trung Hoa" là cuốn sách phản ánh chân thật và sinh động sự thật lịch sử thời phong kiến Trung Hoa thông qua việc kể lại những đại tham quan lớn của thời kì này. Mười hai nhân vật được đề cập trong sách đều là những nhân vật tiêu biểu đáng được lên án trong lịch sử nước này. Việc đưa ra những nhân vật trên, cuốn sách như gửi đến độc giả cái nhìn chính xác nhất về lịch sử với những sự thật hiện hữu khó lường vốn đó. Đồng thời qua đó bày tỏ thái độ phê phán với những gì mà những nhân vật được đề cập trong cuốn sách. Cuốn sách đáng được đọc để cung cấp thêm những kiến thức lịch sử rất quan trọng!
5
72874
2013-05-15 22:07:32
--------------------------
