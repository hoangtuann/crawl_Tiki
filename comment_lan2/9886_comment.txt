470176
9886
Mình biết đến tư duy NLP thông qua 1 người cô đã dạy mình và bản thân cũng đã từng thử trải nghiệm cái cảm giác "NLP" đó! Thực sự đối với mình mà nói chắc hẳn là cảm giác rất tuyệt! Đến với cuốn sách này mình như được mở mang nhiều hơn trong thế giới tâm hồn, về sự thành công hay thất bại đều xuất phát từ đâu, về cách suy nghĩ tích cực, tư duy sáng tạo ! Quyển sách được thiết kế thông minh, khổ sách khá to nhưng chất lượng giấy cực kì tốt! Tiki lại còn giảm giá nữa. Mình cho 5 sao!
5
619354
2016-07-07 11:02:51
--------------------------
414851
9886
Ndeus như quyển NCT tập một dạy cho chúng ta cách giao tiếp tốt và khoa học nhất thì quyển hai lại giúp chúng ta biết lập trình ngôn ngữ là như thế nào. 
Sau khi mua quyển tập một, tôi đã không ngần ngại gì mà lên Tiki đặt mua ngay quyển tập 2. Một bộ sách rất gữu ích. Chất lượng sách rất tuyệt vời nha. Giấy mịn, trắng, tốt, mực in đậm, rõ chữ, không nhòe. Bìa sách và nội dung bên trong được thiết kế rất đẹp, khoa học và xúc tích. Nếu bạn là một con người muốn trở nên thành công trong cuộc sống, bạn nên lựa chọn bộ sách này.
5
714420
2016-04-12 17:10:33
--------------------------
386848
9886
lập trình ngôn ngữ tư duy cực kì quan trọng cho sự phát triển con người. Tôi đã mua quyển NLP tập 1 , và lần này là đến tập 2. 
Cả 2 quyển sách bổ trợ cho nhau về kiến thức, . Tôi đã học được rất nhiều điều hay , thú vị , bổ ích. 
Làm thế nào để giao tiếp tốt , và thành công dễ dàng hơn. Câu trả lời ở trong quyển sách này. 
Về hình thức : sách được bọc trong lớp nilon , tiki lại còn gói trong một hộp cacton rất cẩn thận. Giấy đẹp, chữ rõ ràng. Sách cầm nhẹ tay.  Giá bìa kha rẻ.
 Nói chung đây là một quyển sách mà bạn nên đọc. 

4
789535
2016-02-26 11:08:38
--------------------------
295734
9886
Một người chị bảo tôi cần phải học thêm cách nói chuyện, thế là tôi biết đến NLP. Đây là 2 quyển sách tuyệt vời có thể giúp tôi phát triển bản thân. Lúc đầu mua 2 quyển này mục đích là để chữa bệnh không khéo ăn nói. Sau khi học NLP tôi phát hiện ra vấn đề của tôi nằm ở tâm lý. 2 quyển tôi đã xem sơ qua, luyện tập thì chỉ mới tới chương "Tạo NEO" thôi. Tôi rất thích bộ này, tôi nhất định sẽ nhai đi nhai lại cho đến khi thuận thục các kỹ năng, có thể làm chủ tư duy, phát huy tiềm năng sẵn có.
5
419763
2015-09-10 16:35:50
--------------------------
285505
9886
Nếu như với tập 1 của:"Phát huy tiềm năng cùng NLP" thì mình đã được biến đến về cách thức hành động cũng như tác dụng của việc áp dụng NLP thì đến với tập 2 đây mình được hướng dẫn nhiều cách hơn đề xây dựng và lập trình lại bộ não của mình. Đây thật sự là 1 cuốn sách hay và chỉ bằng những chỉ dẫn, bài tập nho nhỏ trong đây mà đã thay đổi gần hết những suy nghĩ trước đây của mình về bản thân, vế các mối quan hệ. Có thể nói NLP là 1 phát hiện vĩ đại của thế kỷ 20 và mình thật sự mong có những tác phẩm tiếp theo về NLP
5
39383
2015-08-31 21:57:50
--------------------------
282121
9886
phát triển tiềm năng và cùng lập trình ngôn ngữ tư duy là cuốn sách khá hay.tôi đã đọc và áp dụng khá nhiều vào cuộc sống và chỉ tiết là không thể vận dụng và hiểu hết cuốn sách hay này.
NLP được thế giới áp dụng khá nhiều và thành công dựa trên nó.từ năm 1970 người ta đã vận dụng và phát huy tối đa tiềm lực bản thân,hôm nay tôi đã được ngộ ra khá nhiều từ cuốn sách này.
con người có rất nhiều dạng cơ thể khác nhau,nhưng trong đá 2 dạng chíng chúng ta có thể dễ nhìn thấy nhất đó là ý thức và tiềm thức..để vận hành hoạt động của ta ý thức chiếm 10-15% còn lại là tiềm thức,nếu chúng ta có thể khai thác sức mạnh của tiềm thức thì khả năng và sức mạnh có thể tăng lên đến mức chúng ta không thể tưởng được.
nếu các bạn muốn phát huy tiềm năng bản thân hãy đọc nó để biết khả năng của con người là vô hạn như thế nào.mọi điều tưởng như không tưởng con người đều có thể tạo ra được (cách xa nhau về mặt địa lý vẫn thể liên lạc được,muốn khám phá vủ trụ con người vẫn làm được) tại sao bạn không thể làm gì được cả
4
467685
2015-08-28 22:16:37
--------------------------
276967
9886
NLP là một trong những phương pháp nổi tiếng nhất nhì trên thế giới để thay đổi tư duy và vươn tới những thành công trong cuộc sống. Các doanh nghiệp, các học viện đào tạo sử dụng rất nhiều NLP để giúp nhân viên hay học viên của mình thoát ra khỏi những vấn đề về mặt tâm lý đang cản trở họ trong cuộc sống. Mình được biết đến phương pháp này nên đã tìm hiểu một số sách và đây là một trong những cuốn đáng để đọc. Sách giúp bạn có cái nhìn khác về nỗi sợ và thất bại, giúp bạn tìm ra được cơ hội trong chính mớ bòng bong bế tắc, từ đó, giúp ta có những phản hồi tích cực trong cuộc sống đầy những khó khăn và thử thách.
4
71615
2015-08-24 15:45:40
--------------------------
268756
9886
Phát Huy Tiềm Năng Cùng NLP - Tập 2 là sự tiếp nối của quyển tập 1, phát triển một số thông tin mới hơn, gần gũi với cuộc sống hơn! Thông qua quyển sách Phát Huy Tiềm Năng Cùng NLP - Tập 2 này giúp cho tôi có nhiều khái niệm mới hơn, nhiều bài học hơn về bộ não của mình! Tâm đắt nhất với tôi là tôi có một khái niệm mới mẻ hơn về "THẤT BẠI": là cảm giác bạn đã không hành động đúng đắn hoặc chưa làm hết sức mình hay chưa đuổi kịp những người khác. Vậy ta phải làm thế nào với thất bại? Thông qua quyển sách này tôi học được một cách rất hay: hãy biến thất bại thành thông tin phản hồi, nó sẽ là một bài học kinh nghiệm giúp bạn bước thêm một bước đến thành công! Và còn rất nhiều điều thú vị... 
4
504087
2015-08-16 20:11:47
--------------------------
266985
9886
Mình đã đọc cuốn Phát huy tiềm năng cùng NLP tập 1, bây giờ lại được sở hữu thêm cuốn này nên thích vô cùng. Mình nhận thấy Tư duy con người cũng giống như những phần mềm được cài đặt trên máy tính vậy, tại sao nhiều người lại luôn than vãn, suy nghĩ tiêu cực bởi vì từ nhỏ họ đã bị cài đặt những suy nghĩ ấy qua môi trường sống, những  người họ tiếp xúc. Nhiều lúc mình cũng có những suy nghĩ như thế do đó cuộc sống không thay đổi theo hướng tốt hơn. Vậy thì làm sao để có thể thay đổi, chỉ bằng cách cài đặt lại suy nghĩ, đây chính là cuốn sách mình cần, không chỉ vậy mà nó còn cần cho rất nhiều người. Hãy đọc và cảm nhận nó.
5
256590
2015-08-15 08:30:40
--------------------------
213970
9886
Tôi đã có cơ hội trải nghiệm cả 2 quyển Phát Huy Tiềm Năng Cùng NLP (Tập 1 và 2). Riêng tôi, tôi thấy rằng quyển 2 có vẻ không hay bằng quyển 1. Không có nhiều điều mới lạ và thú vị như quyển 1. Quyển 2 thiên về những điều sâu thẩm bên trong một con người, khai thác nội tâm tuy nhiên trình bày hơi dài dòng, khá trù tượng. Quyển 1 làm tôi bất ngờ hơn. Nhưng không phải vì thế mà quyển sách này không hay, tôi khuyên các bạn trẻ nên đọc vì cả 2 quyển là những bài học rất quý giá và có ít để mỗi con người phát triển. Bìa sách đơn giản nhưng tinh tế chất lượng giấy thì khỏi phải chê. Quả thật đây là quyển sách đáng để các bạn trẻ tìm hiểu.
4
385726
2015-06-24 13:15:24
--------------------------
182535
9886
Mình đã từng đọc qua cuốn " NLP-lập trình ngôn ngữ tư duy 1" rồi và giờ là tiếp nối đến cuốn thứ 2. Hai quyển sách NLP thực sự rất hay và hữu ích kể cả với những người chưa bao giờ biết về NLP. Nội dung sách khá từu tượng đòi hỏi phải tượng tượng nhiều nhưng dễ áp dụng trong thực tế. Những cách làm chủ và lập trình bộ não, cách chúng ta làm chủ cảm xúc và suy nghĩ theo hướng tích cực. Tất cả đều thật tuyệt vời.
Điểm cộng cho sách là bìa rất hấp dẫn, giấy tốt, chữ in đẹp.
4
255414
2015-04-14 12:27:19
--------------------------
171877
9886
Tư duy (mindset) nằm trong não bộ. Lập trình ngôn ngữ tư duy chính là ;ập trình lại não bộ của chúng ta nhằm phát huy tiềm năng sẵn có một cách tối đa. NLP giúp chúng ta rất nhiều điều, từ tìm ra giá trị sống, xác định hệ thống niềm tin, đặt mục tiêu, đề ra chiến lược, làm chủ cảm xúc,... cho tới rút ra bài học kinh nghiệm sau mỗi thất bại là những gì NLP mang lại cho cá nhân tôi. Có thể quyển sách có nội dung hơi trừu tượng nhưng đây đã là một quyển sách viết về NLP dễ hiểu nhất trên thị trường Việt Nam rồi và mỗi cá nhân chúng ta cần đọc lại nhiều lần để hiểu vì đây mới chỉ là những kiến thức căn bản mà thôi.
5
387632
2015-03-22 20:50:56
--------------------------
171363
9886
Không giống như Phát Huy Tiềm Năng Cùng NLP (Tập 1) với nhiều triết lý trìu tượng khiến mình hơi khó hiểu và khó áp dụng, cuốn sách tập 2 này là các phương pháp để chuyển những thất bại trong giao tiếp thành thông tin phản hồi , cách học hỏi từ thất bại và cách nhìn mọi mặt của vấn đề từ góc nhìn của người khác. Nó cũng nêu ra các phương thúc ngôn ngữ, dẫn tới hành vi của mỗi người và tác dụng lớn của những ý định tích cực trong thế giới quan của mỗi người để từ đó biết cách tư duy không cực đoan trong mọi tình huống xấu nhất, xoa bỏ những cám xúc tiêu cực, thay đổi góc nhìn, mạnh mẽ, tự tin đến gần hạnh phúc và thành công.
 Giống như cuốn 1, chất lượng giấy in cuốn 2 tốt, bìa cứng dày dặn và cách trình bày rõ rành, mạch lạc, dễ đọc hơn !
4
485671
2015-03-21 20:08:05
--------------------------
77270
9886
Tôi đã có cơ hội được đọc quyển NLP tập 1 và vô cùng hào hứng khi biết TGM phát hành tập 2 của cuốn sách này. Bạn sẽ không thể bỏ qua từng câu chữ trong cuốn sách này, vì mỗi câu chữ, mỗi trang sách đều là tâm huyết của tác giả và là những trải nghiệm cũng như bí quyết đã được nghiên cứu sâu trong quá trình dài. Chúng  ta sẽ biết cách đặt câu hỏi trong từng hoàn cảnh và dễ dàng giải quyết chúng. Chúng ta có thể nắm bắt cơ hội và tạo động  lực cho bản thân để sống. Chúng ta sẽ có những lời khuyên giúp so sánh giữa nhiều khía cạnh về NLP để nhận ra những điểm khác biệt giữa mối quan hệ con người với nhau và với xã hội. Chúng ta sẽ hiểu nhận ra mục tiêu sống của mình khi đã nắm được những bí quyết bổ ích và hữu dụng này.
4
36331
2013-05-26 21:52:09
--------------------------
49919
9886
Khác với NLP 1 có phần hướng ra bên ngoài, nơi ta tiếp xúc và thể hiện khả năng bản thân trong  việc làm chủ các mối quan hệ, cuốn NLP 2 lại hướng về bên trong, để làm chủ chính bản thân ta, tuy vậy, nó lại không đi quá sâu tâm ta có gì, sẽ gặp vấn đề gì, mà giúp ta học cách chấp nhận, yêu thương cả phần tốt và phần chưa tốt trong con người mình, từ đó tìm cách chuyển hóa, hay cho bản thân ta nhiều sự lựa chọn hơn, ta "tự do" hơn trong việc chọn lựa cách hành xử cũng là mở ra hướng đi tốt đẹp hơn khi ta phải đối diện với khó khăn hay những tình huống bế tắc trong quá khứ và hiện tại. Đối với riêng tôi, tôi hiểu bản thân mình hơn, rất thú vị, hi vọng bạn sẽ gặt hái được nhiều điều hay khác!
4
56966
2012-12-10 20:15:17
--------------------------
