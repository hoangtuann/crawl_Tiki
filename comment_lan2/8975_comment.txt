339656
8975
Mình bị cuốn hút với giọng điệu và cách dẫn dắt của tác giả ngay từ chương đầu tiên. Thật ra thì mình đã đọc phần đầu của quyển sách trên 1 phần mềm đọc sách online, và đã quyết định mua nó ngay sau đó. Sách có những bài tập cho những phần riêng biệt được hướng dẫn cụ thể để bạn có thể thay đổi cách nhìn của mình qua những bài tập đó, từ đó phát huy được tối đa năng lực của bản thân. Lời khuyên khi đọc cuốn sách này là bạn đừng đọc hết nó trong một ngày , mà hãy đọc lần lượt áp dụng tuần tự các bài tập theo thời gian được đưa ra. Khi gấp sách lại bạn sẽ cảm giác trở thành một con người hoàn toàn khác, suy nghĩ mới mẻ hơn trước. Đây chỉ là cảm nhận của riêng mình thôi, hy vọng sẽ hữu ích cho những bạn đang định mua quyển sách này. Rất đáng để thử
5
712005
2015-11-18 14:33:34
--------------------------
291296
8975
Tác giả Mark Victor Hansen  đặt tựa đề cho cuốn sách "Kiếm Tiền Siêu Tốc" rất hấp dẫn người đọc.Cuốn sách được chia làm 4 phần, phần sau là tiếp bước cho phần trước. Ở phần đầu tiên tác giả đã đưa ra công thức cho một cuốc sống sung túc: Con đường ngắn nhất dẫn đến tiền bạc. Có cả trăm công thức để kiếm tiền một cách từ từ, nhưng hãy nhìn vào thực tế, bạn sẽ rút ra được công thức cho riêng mình nhờ áp dụng các công thức của tác giả. Phải dập tan ngay những ý nghĩ tiêu cực, và thay vào đó là những ý nghĩ tích cực, đưa ra những câu hỏi mang tính khích lệ cho bản thân, hãy thay đổi quá khứ và nhìn về tương lai. Và không có cách nào khác là phải tự thân vận động, rút ra bài học cho mình để kiếm tiền.

4
156324
2015-09-06 10:09:55
--------------------------
270937
8975
Mình tìm mua cuốn này sau khi nghe đến câu chuyện về tác giả đã mua 3 miếng đất trong vòng 57 giờ với chỉ 100 U$D; cộng thêm tựa sách hút người đọc :3. Coi xong áp dụng được liền luôn, mình rất thích hình ảnh hái những quả cây dưới tán thấp, cả con chim ruồi nữa, rất hay! Ngoài ra, hình thức trình bày của tác giả cũng rất thú vị, phù hợp "mọi" thể loại người đọc (mình thích đọc trang chẵn hơn ^^): vừa truyện, vừa lý thuyết (có bài tập "lau phải, lau trái" luôn :3) :P Cuốn sách này mang lại cho mình rất nhiều cảm hứng, mình đã bắt đầu thực hành từ lúc mình chưa đọc xong cuốn sách luôn ^^ hehe
5
575572
2015-08-18 18:49:15
--------------------------
251753
8975
Đầu tiên mình đọc quyển sách là bởi cái tên của nó khá thu hút, rất gợi trí tò mò ở người đọc. Nguyên nhân thứ hai khiến mình đọc quyển này bởi có tên tác giả Mark Victor Hansen, một tác giả khiến mình rất thích và ấn tượng bởi ông cũng đồng thời là một trong những người viết cuốn Sức mạnh của sự tập trung mà mình cho rằng đó là một trong những quyển kĩ năng sống hay nhất từng đọc. Khi đọc quyển này, mình thật sự hơi khó đọc một chút bởi một bên tác giả đề cập đến những lí luận của mình về cách kiếm tiền siêu tốc còn mặt kia là câu chuyện về một người phụ nữ có tính kiên cường rất cao, bà có ý chí làm giàu rất mạnh và luôn tìm mọi cách giúp đỡ mọi người để họ cũng có cuộc sống sung túc hơn. Chính vì trình bày như vậy nên hơi khó theo dõi vì dễ bị phân tâm trong lúc đọc. Về mặt nội dung, tác giả nên rõ tầm quan trọng của giá trị mỗi con người chúng ta. Ông cho rằng ai cũng có mặt sở trường của mình và hãy cố gắng phát huy nó, bạn sẽ đạt được thành công vượt trội. Mặt khác, phải luôn có chí tiến thủ, đừng vì những quan niệm lỗi thời mà áp đặt lên chí hướng của mình và làm chúng ta mất niềm tin ở cuộc sống. Về câu chuyện của tác giả mình thấy nó không được mang tính thực tiễn cao, nội dung có vẻ hơi khiên cưỡng vì thực tế hiếm khi có trường hợp như vậy. Nhưng nhìn chung, đây cũng là một quyển sách đáng để nghiền ngẫm nếu bạn có chí hướng làm giàu.
4
274995
2015-08-03 10:30:12
--------------------------
143847
8975
Quỷên sách này mình không thực sự ấn tượng như sách của Napoleon hill. Phần đầu của sách không hay lắm. Về sau thì các vấn đề tác giả đề cập khá hay nhưng đọc mà hiểu luôn thì thực sự rất khó. Khi nói đến lý thuyết thì tác giả nên kèm theo ví dụ minh họa cho dễ tiếp thu hơn là tách ví dụ riêng (viết ở trang lẻ của sách, rất bất lợi cho việc vừa đọc nội dung, vừa đọc lý thuyết). Mà trải dài sách là ví dụ về cuộc sống của Michelle, cái quan trọng là bà đã làm thế nào để kiếm hơn 1 triệu đô trong 90 ngày như thế nào thì tác giả lại không đề cập đến để chúng ta học hỏi kinh nghiệm. Nói chung, tác giả lớn như Mark và Robert thì giá trị nội dung ông mang lại luôn mới mẻ, ý nghĩa và giá trị thực tiễn cao.
Góp ý với tiki: mình mua sách tiki, chữ khá mờ, lại còn bị dính tờ vào nhau nữa chứ. Mong tiki xem kỹ trước khi giao hàng.
4
381173
2014-12-24 16:51:43
--------------------------
106470
8975
Nếu ai đã từng đọc qua các tác phẩm như 13 nguyên tắc nghĩ giàu, làm giàu của Napoleon Hill hay 9 bước đến tự do tài chính của Suze Orman sẽ thấy cuốn sách này càng tuyệt diệu hơn nữa khi nó là sự tổng kết, tập trung cao độ, gắn kết tất cả những kinh nghiệm những điều tưởng chừng như phi lí ở các cuốn sách khác, làm ra tiền ở cuốn Kiếm tiền siêu tốc nghe có vẻ quá nhanh, quá bất khả thi, nhưng quả thật nó có thực! chỉ là bạn vận dụng "tài nguyên" về trí óc và trái tim như thế nào để khai phá hết khả năng để làm nên 1 cú hích đột phá nhất. Qủa thật đôi khi đặt ra những mục tiêu dài hạn và thực hiện từ từ, vững chắc vẫn là 1 kế sách thong thả và an toàn nhưng nếu bạn đang ở tình trạng không thể không làm ra tiền và nhiều tiền là đằng khác thì bạn cần phải biết vận dụng cảm xúc kết hợp với tay chân như thế nào trong cuốn sách này.
5
153783
2014-02-20 17:17:35
--------------------------
