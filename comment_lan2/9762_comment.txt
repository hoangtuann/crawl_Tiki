334224
9762
Một cuốn sách in màu đẹp mắt và thú vị. Cậu bé Andersen 8 tuổi, lần đầu tiên tới Copenhagen tìm người bà con hỏi mượn tiền giúp mẹ đế cứu chữa cho người cha đang bị bệnh nặng. Một cậu bé thông minh, ngoan ngoãn và dễ thương luôn nhiệt tình giúp đỡ mọi người và đã được một người lạ giúp đỡ để có tiền chữa bệnh cho người cha đáng kính. Tại đây cậu tình cờ gắp anh em nhà Grim, cùng với chiếc mũ pháp thuật cậu bé đã có một cuộc phưu lưu kì thú. Cuốn sách phù hợp với các bạn nhỏ yêu thích truyện tranh.
3
29827
2015-11-09 09:17:37
--------------------------
