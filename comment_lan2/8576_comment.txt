420274
8576
Mình có khá nhiều các cuốn sách nói về khoa học, tự nhiên, vũ trụ, v.v. Đa phần trong đó là những cuốn sách dày cộp, hơi khô khan nhưng vì thích nên mình cảm thấy không sao cả. Còn riêng đối với cuốn "Thư viện tri thức-Khám phá thế giới tự nhiên" này thì hoàn toàn khác. Trang bìa cũng không có gì gọi là quá đặc sắc nhưng phải nói là nội dung và chất lượng bên trong cực kì tốt. Giấy vừa dày dặn vừa đẹp. Hình vẽ minh họa dễ thương chính xác. Nội dung phong phú. Điều mà mình hơi tiếc đó là sách không được bọc bóng ở bìa nên khá là dễ rách. Nhưng dù sao đây cũng là một cuốn sách rất hay!
4
634478
2016-04-22 22:30:49
--------------------------
