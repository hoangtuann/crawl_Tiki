73806
9050
Học Tiếng Anh là điều rất cần thiết lúc này và để đạt được kết quả tốt thì lúc nào cũng phải thoải mái nhất với bộ môn này. Không hóc búa, dài dòng như các môn học khác, Listening fun đã cho thấy sự vui nhộn cũng như dễ học đối với môn Tiếng Anh. Quyến sách là sự hội tụ đủ những kĩ năng nghe mà mỗi người cần phải học. Với những trò của Tý Quậy, các em sẽ được đắm mình vào tuổi thần tiên với những câu nói đơn giản. Thật là thích thú phải không nào? Ngay từ khi cầm trên tay cuốn sách ta đã thấy được trang bìa được trang trí rất đẹp mắt và phù hợp với các em nhỏ. Với quyến sách này thì hi vọng các em nhỏ sẽ vừa chơi và vừa học để có kết quả tốt nhất ở môn Tiếng Anh
4
63348
2013-05-10 09:06:31
--------------------------
