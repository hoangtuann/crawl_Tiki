408817
9443
Mình đã đăng kí mua cuốn Những câu chuyện về Bubu tập 3. Sau khi mua về mình mới xem nội dung các câu chuyện về Bubu có trùng với hai tập mà mình đã mua đó là Bubu ích kỉ và Bubu chơi với lửa. Cuốn Bubu ích kỉ mình mua ở tiệm sách nó cùng nội dung nhưng mình thấy tranh vẽ nó đẹp hơn. Với theo mình Tiki nên cho  trang sau của bìa sách cũng hiện lên trang quảng cáo nhằm giúp phụ huynh muốn mua thì cũng biết được đó là những câu chuyện nào để tránh mua bị trùng lặp như mình.
2
1130628
2016-04-01 12:33:20
--------------------------
265004
9443
Bubu không cẩn thận - Con mình chưa biết chữ nhưng đã lật truyện và đọc như biết chữ ấy. Mình đi làm cả ngày nên tối nào carot (con của mình - 3 tuổi) cũng đòi mẹ: mẹ ơi đọc truyện bubu cho con. Hỏi con muốn đọc bubu nào, là con lại nói: Bubu không cẩn thận. Đọc xong truyện là con lại nói: carot phải giống bubu, chơi xong phải biết cất đồ chơi. Đọc bubu đi lạc thì con luôn miệng hỏi: mẹ ơi, bubu đi đâu. Đọc bubu chơi với lửa thì con lại nói: ồ nóng quá. Những bày học cho con rất hay!
5
618449
2015-08-13 16:52:47
--------------------------
