394671
9090
Mình mới đang là học sinh thôi nhưng ước muốn làm giàu của mình là rất cao , mình đã thử qua kinh doanh một số loại hàng song lời  cũng không đáng là bao thậm chí còn lỗ và cũng chẳng duy trì được bao lâu , mình vẫn luôn thắc mắc tại sao người khác làm được mà mình chưa làm được . ngay khi nhìn thấy tựa đề sách trên tiki lại còn được giá khuyến mãi , mình không ngần ngại đặt mua luôn . Khi nhận hàng mình rất hài lòng , tiki còn cho xem hàng trước nữa . Sách dày đẹp in rõ nét , ưng lắm luôn , nội dung sách cũng phù hợp với cảm nhận từng người nhưng mình yêu nó ngay từ những trang đầu tiên . Đây là một cuốn sách đáng mua .
5
1185833
2016-03-10 19:47:56
--------------------------
313935
9090
Có thể nói đây là cuốn sách khởi nghiệp đầu tiên mà tôi đọc. Nó thú vị không phải ở lời văn hay hoặc dở mà ở cách tác giả dẫn dắt ta đi vào từng ví dụ, hướng dẫn ta từng bước khởi nghiệp để tự tạo dựng một doanh nghiệp. Trong sách có nhiều tư tưởng khác với suy nghĩ thông thường của chúng ta, có lẽ đây là do khác biệt văn hóa,kinh tế của từng nước, nhưng nếu bỏ qua những tiểu tiết ấy để thấy được phương pháp khởi nghiệp và vận dụng nó vào thực tế thì đây đáng làm cuốn sách gối đầu giường của bạn!
4
461215
2015-09-24 22:55:16
--------------------------
296661
9090
Tự do và Giá trị, Giá trị là con đường tìm đến tự do. Tôi cầm cuốn sách và đọc nó, cảm giác của tôi như nhặt được vàng vậy. Bản thân mỗi người chúng ta đều có những điều ấp ủ, những giấc mơ, nhưng chưa bao giờ nói ra, đơn giản vì chưa tìm được người đồng chí hướng để chia sẻ. Khi ban đọc cuốn sách này, bạn có cảm giác nó như người bạn tri kỉ mà ta đang tìm, đang cùng ta làm sáng tỏ những điều còn mơ hồ, củng cố thêm niềm tin ta sẽ bắt đầu làm cái gì đó... Tuy nhiên, để đi đến được tự do, ta cần phải khẳng định giá trị, giá trị ở đây có nhiều nghĩa rộng, có thể việc đầu tiên chính là kiến thức, nền tảng và kinh nghiệm thì xuất phát điểm tốt nhất có thể, từ đó dần dần khẳng định giá trị bản thân để đến cái tự do mà bạn mong muốn.
4
112376
2015-09-11 10:51:59
--------------------------
292598
9090
Cuốn sách này được tác giả chọn lọc và tuyển chọn những tấm gương và ý tưởng của những người dám nghĩ khác làm khác của tất cả những người trên thế giới mà tác giả từng biết, những người mà họ kiếm tiền bằng cách riêng của mình mà không theo một lối mòn có sẵn, và cái quan trọng nhất là họ kiếm tiền trên những sở thích của bản thân chứ không phải là bằng những công việc khiến họ nhàm chán. Làm việc theo những sở thích và điểm mạnh của bản thân 16h một ngày hơn là làm việc 8h một ngày cho một công việc mà bạn thật sự không thích. Tuy nhiên những mô hình này cần nhiều thời gian để có thể triển khai ở Việt Nam vì các bạn trẻ ở Việt Nam không thích sự mạo hiểm. :D
3
532043
2015-09-07 14:38:38
--------------------------
225385
9090
Cách mà tác giả cuốn sách dẫn dắt và kể những câu chuyện khá là ngông, một vài ý tưởng có thể chưa phù hợp để áp đặt nơi Việt Nam nhưng thực sự mà nói đọc cuốn sách thì những bạn đang còn lưỡng lự về khởi nghiệp và tinh thần tự do chắc chắn sẽ bắt tay ngay vào thứ mình muốn làm. Cuốn sách như một lực đẩy đưa con người ra khỏi vòng an toàn và sự lưỡng lự. Cho bạn tinh thần làm điều bạn muốn và làm điều bạn thích, mà vẫn được hưởng lương từ đó. Một cuốn sách vừa là động lực vừa là kiến thức cho việc khởi nghiệp.
5
109789
2015-07-10 12:06:45
--------------------------
212645
9090
Cuốn sách này chắc chắn sẽ phù hợp ch những bạn nào có tư tưởng "khác người", thích đi theo hướng tích cực thay vì lối mòn, thích tự do thay vì gò ép trong những quy định, nội quy của công ty, ngày làm 8 tiếng thật nhàm chán, và tôi là người thích những cái mới vì thế tôi rất thích cuốn sách này, tuy sách chưa thật sự thích hợp để áp dụng trong tình hình đất nước hiện nay khi mà truyền thống luôn chiếm số đông nhưng biết đâu được, nếu ai thích làm những gì mình mong muốn thì hãy áp dụng thử để tạo ra sự kỳ diệu cho cuộc đời mình.
Cuốn sách đáng đọc, tạo ra giá trị cho bản thân
4
462666
2015-06-22 17:57:22
--------------------------
194614
9090
Bạn mình có cuốn sách này, mình mượn bạn đọc và thấy nhiều điều hay. Bỗng một ngày công ty cắt giảm biên chế, nhiều người sẽ mất việc. Tuy nhiên, đó cũng chính là cơ hội để bắt tay vào một cuộc chơi mới, tại sao lại không khởi nghiệp ngay và luôn.
Với cuốn sách này, dù là viết cho một môi trường như Hoa Kỳ nhưng chẳng phải Việt Nam mình cũng đang nhảy vào xu thế toàn cầu hóa hay sao? 
Bạn có chấp nhận suốt đời làm thuê không? Bạn có đảm bảo một mai công ty gặp sự cố mình vẫn nằm trong số những người được giữ lại không (trừ khi bạn làm cho Nhà nước, mà Nhà nước đâu phải dễ mà vào) ? 
Vậy thì nên đọc cuốn sách này để vạch ra cho bản thân một phương án B hay phương án C ,... Cơ hội luôn tồn tại mà.
4
524112
2015-05-11 11:23:37
--------------------------
187570
9090
Khi tôi đọc cuốn sách khởi nghiệp với 100$ này, trong đầu tôi nghĩ: kiến thức trong sách rất thú vị, tạo ra sự khác biệt, làm thay đổi cái "quen thuộc" vốn trước giờ chúng ta đều dành mỗi ngày 8 tiếng đi làm và về nhà, mọi thứ đều nhàm chán. Cuốn sách chỉ ra phương pháp giúp mỗi người tìm ra niềm yêu thích kinh doanh, tự làm chủ công việc và tài chính của mình, tìm ra cơ hội ở xung quanh, tạo dựng sự nghiệp từ niềm đam mê của bản thân theo cách đơn giản nhất. Tuy nhiên có lẽ cuốn sách chỉ phù hợp làm cẩm nam cho những ai có hiểu biết nhất định về kinh doanh nhỏ lẻ và 1 đầu óc không sợ thất bại, ở VN mình thì kiểu kinh doanh này còn khá mới mẻ khi mọi người chỉ biết đi làm ở các doanh nghiệp, thích yên phận và sợ thất bại. Nói chung cuốn sách vẫn rất hay khi chúng ta đọc nó.
4
425604
2015-04-23 15:34:39
--------------------------
173351
9090
Cuốn sách khởi nghiệp với 100$ rất là bổ ích. Mặc dù những trường hợp trong sách chưa thật sự phù hợp với ở Việt Nam ta nhưng nó để lại cho tôi nhiều ấn tượng sâu sắc.
Ưu điểm của sách:
- Giúp ta sáng tạo ra nhiều cách kiếm tiền
- Học hỏi về nghị lực của những cá nhân đã thành công trong sách
- Mang lại sự giải trí cho người đọc
- Thúc đẩy phát triển năng lực bản thân mình
Khuyết điểm sách:
- Chưa phù hợp với tình trạng ở Việt Nam
Nói chung nếu các bạn yêu thích kinh doanh, muốn phát triển ý nghĩ của mình thì nên mua cuốn sách này về đọc để trải nghiệm những kiến thức rất bổ ý trong cuộc sống .
5
273559
2015-03-25 23:21:18
--------------------------
133324
9090
Cuốn sách là 1 tài liệu hay dành CHO MÔI TRƯỜNG KHỞI NGHIỆP VỚI INTERNET TINH GỌN, ko phải tất cả mọi người đều thích hợp với cuốn sách này.

Sau khi mình đọc qua lần 1 , và sẽ đọc tiếp lần 2, 3... mình rút ra một số vài ý chính là.

Ưu:
1. Cuốn sách chất lượng giấy tốt, cách viết dễ hiểu và có những quan điểm nhấn mạnh dễ hiểu bằng câu in đậm. Nếu bạn là người mới mình khuyên bạn nên đọc câu đó xem thích hợp với sở thích của mình ko rồi hẵng mua. Vì đây là cuốn viết dựa trên môi trường kinh doanh Mĩ.
2. Càng ngày thế giới càng phẳng và khoảng cách xóa nhòa nên mình định lượng 60% nội dung cuốn sách vẫn áp dụng vào viet nam và thực hiện được với các bạn đã biết kinh doanh trực tuyến ( ví dụ như tiki... một model thành công vì rất biết tôn trọng khách hàng. Còn tiki tương lai có thay đổi hay ko thì tự tiki lựa chọn)  
3. Nếu bạn biết về chiến lược 4 P của marketing thì kết hợp với cái này tuyệt vời nhất.

Nhược: chưa tìm thấy sẽ update ở dưới

Kết: cho 4 sao = 8/10 điểm vì sách ko hẳn đúng với phong cách vn nhưng hi vọng 1 ngày vn sẽ xóa nhòa khoảng cách đó với thế giới.
4
122377
2014-11-04 22:22:08
--------------------------
126297
9090
Đây là 1 bản kế hoạch chi tiết về tự do có tính hành động cao. Tại nhiều điểm của bản kế hoạch này, ta sẽ có cơ hội dừng laij và hành động theo kế hoạch của riêng ta trước khi tiếp tục học hỏi nhiều hơn về những gì mà người khác đã làm. Sách không chỉ tập trung trình bày cho độc giả thấy và hiểu những điều thực tiễn mà ta có thể thực hiên cho tương lai, mà còn đề cập đến kinh doanh vi mô- 1 cách kiếm sống tốt trong lúc tạo ra 1 cuộc sống độc lập và có mục đích. Cuốn sách này rất hay, không đao to búa lớn, không giáo điều khô khan, rất dễ hiểu, giá trị  thúc đẩy hành động cao. Cảm giác bạn nào đọc xong cũng có thể làm ngay được 1 cái gì đó.
4
381173
2014-09-18 09:15:38
--------------------------
117212
9090
Tự do và Giá trị, Giá trị là con đường tìm đến tự do. Tôi cầm cuốn sách và đọc nó, cảm giác của tôi như nhặt được vàng vậy. Bản thân mỗi người chúng ta đều có những điều ấp ủ, những giấc mơ, nhưng chưa bao giờ nói ra, đơn giản vì chưa tìm được người đồng chí hướng để chia sẻ. Khi ban đọc cuốn sách này, bạn có cảm giác nó như người bạn tri kỉ mà ta đang tìm, đang cùng ta làm sáng tỏ những điều còn mơ hồ, củng cố thêm niềm tin ta sẽ bắt đầu làm cái gì đó... Tuy nhiên, để đi đến được tự do, ta cần phải khẳng định giá trị, giá trị ở đây có nhiều nghĩa rộng, có thể việc đầu tiên chính là kiến thức, nền tảng và kinh nghiệm thì xuất phát điểm tốt nhất có thể, từ đó dần dần khẳng định giá trị bản thân để đến cái tự do mà bạn mong muốn.
4
371320
2014-07-16 10:28:32
--------------------------
115760
9090
Quyển sách này viết rất đơn giản, rõ ràng và dễ hiểu.

Tác giả đã làm một nghiên cứu với số đông những người khởi nghiệp thành công và chắt lọc lại thành những câu chuyện để làm rõ những mô thức mà tác giả đề cập đến.

Đọc sách này, bạn sẽ có thêm nhiều ý tưởng, động lức cũng như cách làm. Từng chi tiết rất nhỏ, rất nhỏ để chuẩn bị trong kinh doanh cũng được tác giả đề cập đến.

Ngoài ra, tác giả cũng là một người rất hay ho để những ai mê du lịch tìm hiểu.
4
39965
2014-07-01 14:23:22
--------------------------
