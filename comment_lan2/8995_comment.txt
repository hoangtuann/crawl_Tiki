369324
8995
Mary Balogh luôn giữ phong độ rất ổn định qua từng tác phẩm. Cuốn Tin em và yêu em có nội dung hơi giống Hạt ngọc ẩn mình của bà. Cũng là câu chuyện về nữ chính phải chạy trốn khỏi những người họ hàng độc ác, rồi trên con đường chạy trốn đó thì gặp nam chính. Nam chính của Mary luôn là những anh chàng mạnh mẽ, hết lòng bảo vệ nữ chính. Nữ chính thì vô cùng mạnh mẽ và kiên cường. Điểm không hài lòng là cuốn sách mình nhận từ tiki bị cào xước bìa rất khó hiểu. Mong sau này không gặp phải tình trạng đó nữa.
4
149610
2016-01-15 15:33:10
--------------------------
237697
8995
Vì đã trót thích tác giả Mary Balogh nên tôi đã mua quyển này. Tôi thích cách tác giả tạo ra cao trào, xung đột nội tâm của hai nhân vật chính. Tình cảm của Jane và Jocelyn ban đầu chỉ vì bản hợp đồng nhưng dần dà theo thời gian, theo những màn đấu khẩu, giữa hai người rốt cục cũng nảy sinh tình yêu thật sự nhưng lại chẳng ai chịu chấp nhận sự thật đó :)) Tuy cả hai đều có quá khứ đau thương riêng của bản thân nhưng may mà đến phút cuối cùng họ cũng hiểu ra người mà mình đang tìm kiếm chính là đối phương. Điểm trừ mình dành cho quyển này là cái kết có hơi vội vã  và có phần gượng ép nhưng đây vẫn là một quyển sách hay.
4
35746
2015-07-22 17:25:51
--------------------------
120693
8995
Luôn tin tưởng vào tác phẩm của Mary Balogh, mình đã mua cuốn sách này ngay khi nó được xuất bản. Cuốn sách với bìa sách được đầu tư công phu và ấn tượng hơn.
:Mary Balogh cuốn hút mình với những câu đối đáp thông minh giữa Jane và Jocelyn. Jane khẳng định mình bằng những câu nói mạnh mẽ, tự lập, không dựa dẫm vào ai. Câu thoại của Jane thông minh và hóm hỉnh đến nỗi ta phải bật cười sảng khoái và thỏa mãn với kiểu đối đáp của nàng.
Jane muốn ẩn mình nhưng nàng lại luôn gây nên tâm điểm. Jocelyn muốn tống cổ nàng đi nhưng lại cứ bị nàng cuốn hút. Dựng lên một bản hợp đồng tình nhân, Jane hài lòng với cuộc sống Jocelyn trao tặng: một căn nhà với những luống hoa, một phòng đầy sách do chàng chính tay chọn từng cuốn một tặng này, một nơi mà hai người thuộc về với những bình yên và tăm tối nhất cuộc đời được tiết lộ. Jocelyn trọng danh dự, lạnh lùng, cố chấp cũng có lúc phải yếu mềm và đớn đau.
Một cuốn sách bạn chẳng thể nào đặt xuống cho đến kết thúc câu chuyện. Từ những pha đối đáp hài hước đến những truy hân nóng bỏng ngọt ngào, từ những đau thương và xót xa đến dư vị hạnh phúc như những câu chuyện cổ tích mãi hạnh phúc về sau. Mary Balogh đã tạo nên một tác phẩm có chiều sâu như thế.
Tuy nhiên có một điểm trừ là khúc mắc giữa Jane và Jocelyn được giải quyết quá tuềnh toàn, hai người gắn kết lại với nhau khi mà những nút thắt vẫn chưa được giải quyết hết.
Mình cho cuốn sách này 9/10 điểm.
5
15919
2014-08-14 14:27:04
--------------------------
96950
8995
Sau một vài tác phẩm của Mary Balogh thì mình có nhận xét tổng quan và chủ quan là mỗi tác phẩm của bà thường đặt các nhân vật vào những tình cảnh cực kỳ éo le và tình yêu của họ phải trải qua vô vàn sóng gió để có thể đến được với nhau. Không biết tại sao mà chúng không để lại cho tôi nhiều ấn tượng, có vẻ hơi mờ nhạt là khác. 

Trở lại với "tin em và yêu em" có lẽ thành công nhất trong tác phẩm là phần bìa sách và dịch thuật. Truyện được thiết kế và dịch khá theo sát với bản gốc. Với phần nội dung, nửa đầu câu chuyện khá lôi cuốn, có thể thấy được Jane và Jocelyn - cả hai đều có những uẩn khúc phía sau quá khứ và chịu nhiều cay đắng. Một Jocelyn cay đắng đến mức chỉ có thể chấp nhận Jane như một tình nhân của mình và một Jane xinh đẹp, mạnh mẽ và độc lập. Có lẽ cả hai nhân vật đều quá 'cứng đầu' nên luôn làm tổn thương nhau hết lần này tới lần khác. Bên cạnh đó, câu chuyện có quá nhiều nút thắt, những khúc mắc cần giải tỏa khiến cho người đọc cảm thấy hơi lộn xộn và rất khó tập trung, càng về cuối câu chuyện càng có phần mờ nhạt và gượng gạo, có thể vì vậy mà để lại ấn tượng không nhiều lắm với mình.
3
125574
2013-10-17 11:31:15
--------------------------
87370
8995
Trong những cuốn sách đã đọc của M.Balogh thì đây là tác phẩm tôi đánh giá thấp hơn những cuốn trước. Phải thừa nhận bìa sách là thứ nổi trội nhất, rất đẹp và rất thu hút, nội dung truyện hay và nhiều kịch tính nhưng tôi chỉ ấn tượng với nửa đầu tác phẩm, rất hài hước và thú vị khi chứng kiến những lần Jane & Jocelyn đấu khẩu với nhau nhưng càng về sau cốt truyện có vẻ mờ nhạt dần và đôi lúc khó hiểu. Tôi thật sự không thể thấu được cách diễn đạt tâm trạng của hai nhân vật lúc này, nó hơi cứng nhắc và lộn xộn.
"Tin Em & Yêu Em" không để lại trong tôi ấn tượng gì nhiều bởi cách xây dựng nhân vật chưa thật sự hợp lí, còn nhiều những mâu thuẫn, những cảm xúc khó lí giải mà tác giả chưa nêu ra nhưng phải nói tác phẩm này là một sự mới mẻ của tình yêu mà M.Balogh mang đến cho người đọc, được trải qua những cung bậc cảm xúc trong truyện cũng giúp tôi tạm hài lòng với tác phẩm này.
3
41370
2013-07-17 19:02:38
--------------------------
83097
8995
Yêu cuốn sách này từ cái tên nhẹ nhàng đến bìa sách thanh thoát, ngọt ngào. Nội dung câu chuyện tuy không mới nhưng khi đọc những trang sách đầu tiên mình không thể cưỡng lại được sức hút kì lạ của nó. Mình không thể không bật cười trước tính cách trẻ con của ngài công tước Jocelyn, thích tỏ ra ta đây thật cool, thật badboy. Nàng Jane thì đúng chất tiểu thư cá tính, rất mạnh mẽ, miệng lưỡi đanh đá nhưng lại rất điềm đạm, tao nhã. Jane-Jocelyn vẽ ra 1 bức tranh tình yêu muôn màu. Bạn sẽ bị cuốn vào mạch truyện hấp dẫn, lôi cuốn. Tuy nhiên phần kết mình không hài lòng cho lắm. Tác giả có vẻ hơi gượng gạo khi giải quyết những rắc rối của Jane và Jocelyn. Tóm lại, bạn hãy thử đọc và cảm nhận về cuốn sách này ha!
4
5118
2013-06-25 12:12:43
--------------------------
81373
8995
Từ một bản hợp đồng yêu, hai con người ấy đã đến với nhau trở thành một đôi tình nhân. Ngọt ngào có yêu có nhưng chỉ dựa trên một bản hợp đồng khuông mẫu. Ra ngoài đó chính lại là sự lạnh lùng những trò đấu khẩu đến làm tôn thương nhau. Để rồi nhận4 ra trong khoảng thời gian đã qua cả hai đang đi tìm hạnh phúc riêng cho mình mà người đó lại chín là họ. Cốt truyện hấp dẫn, xung đột nội tâm lên đến đỉnh diểm, hạnh phúc lên đến cao trào là những điều có thể nhận xét cho quyển sách này. Người ta thường nói yêu bằng lí trí, nhưng cũng cần lắm sự mách bảo của trái tim.
4
123837
2013-06-16 18:56:46
--------------------------
79402
8995
Tôi được nhỏ bạn cho mượn truyện này, và khi đọc thì rất hài lòng. Tên truyện giản đơn nhưng cũng rất sâu sắc '' Tin em và yêu em '', một tên truyện rất hay. Bìa truyện cũng được thiết kế mĩ miều trông rất sang trọng và quý phái.
Nội dung truyện rất ổn, cốt truyện cũng khá mới mẻ không nhàm chán. Truyện đã hấp dẫn mình ngay từ những trang đầu tiên.
Tình yêu trong truyện thật đẹp, xuất phát từ những gì đơn thuần thôi nhưng lại rất có ý nghĩa, mình cũng rất thích cách hai người họ dần yêu nhau, rất ngọt ngào.
Văn phong của tác giả nhẹ nhàng và sâu lắng, dễ đi sâu vào lòng người đọc.
4
57869
2013-06-07 01:31:37
--------------------------
77044
8995
Tác phẩm thứ hai tôi đọc của Mary Balogh, More than a mistress (Tin em & yêu em) của Mary Balogh có lẽ gây được ấn tượng mạnh hơn cho tôi về văn phong của bà cũng như tài năng của nữ văn sĩ này.
Các tình tiết trong truyện rất hấp dẫn. Ngay từ những chi tiết ban đầu cũng có thể khiến người đọc háo hức và sẵn sàng cho những trang cuối cùng. Jocelyn Dudley và Sara Jane Illingsworth đều được xây dựng tính cách hết sức rõ ràng, một lời khen nữa cho tính cách của Jane, quá cá tính!
Đi từ những mâu thuẫn, những nút thắt, tình yêu của họ rổ hoa đẹp tựa một đóa hồng. Hương thơm của nó quyện chặt lấy tâm hồn người đọc mà đưa họ đi nếu nhiều cung bậc cảm xúc hơn, nhiều ý nghĩa sâu xa hơn.
Tuy nhiên, cái kết hay cao trào dường như chưa làm tôi hài lòng lắm. Quá nhiều nút thắt để phải tháo bỏ khiến cho đọc giả bị đánh lạc hướng hết lần này đến lần khác mà không biết đâu là nút thắt thật sự. Do đó, cao trào mà Mary Balogh muốn đặt ra hẳn đã "thấp" đi một nữa do những diễn biến ấy. Thêm nữa là cách mà tác giả "giải quyết" Charles - người bạn thân của Jane. Bà "quảng" anh ta đi như một thứ rác rưởi cần phải "quét" đi nhưng Charles thật sự không làm gì đến phải đáng bị như vậy ngoài việc là một nhân vật phụ, ngoài việc tưởng nhầm tình bạn của anh và Jane là tình yêu, ngoài việc đứng dậy bao vệ hạnh phúc của bạn mình - người anh tưởng là yêu anh.
Nhưng, ngoài phần kết ra thì câu truyện vẫn hay theo cách riêng của nó. Phải nói là hay hơn The temporary wife (Sự trả thù ngọt ngào) rất rất rất rất rất rất rất rất rất nhiều.
Mong sẽ được đọc những tác phẩm tiếp theo trong series The mistress của Balogh, đặc biệt là The secret mistress, về cô em út trong nhà Dudley - Angelina Dudley Heyward.
4
19049
2013-05-25 14:11:09
--------------------------
76960
8995
Tin em và yêu em có một cốt truyện không mới đó là tình nhân nhưng không vì thế mà nó trở nên nhàm chán bởi chính cách viết phóng khoáng của tác giả. Ban đầu họ đến với nhau là mối quan hệ nhân tình và có cả mộ bản hợp đồng cho mối quan hệ đó- một mối quan hệ về xác thịt nhưng cuối cùng với họ lại nảy nở một tình yêu mà không biết bông hoa tình yêu đó đã nở rộ lúc nào không hay. Ngay từ những trang đầu tiên của truyện thì đã là những màn đối đáp có phần nảy lửa của họ, những lời nói làm tổn thương đến đối phương nhưng tất cả chỉ để che giấu đi chính trái tim đang dần mất đi kiểm soát của bản thân. Tình yêu đôi khi vẫn chỉ đơn giản là vậy nhưng tại sao ngời ta cứ phải làm tổn thương nhau chẳng phải khi đã yêu một người thì nỗi đau của người đó cũng chính là nỗi đau của mình sao. Khi đã yêu thì phải tin tưởng nhau hai người có thể ở bên nhau được hay không không phải họ yêu nhau bao nhiêu mà họ tin tưởng nhau được bao nhiêu. Tin tưởng mang đến một tình yêu. 
3
97873
2013-05-25 07:15:19
--------------------------
76666
8995
Ấn tượng đầu tiên của mình về cuốn sách này khá tốt: bìa sách đẹp, trích dẫn gây tò mò cho người đọc, nội dung những trang đầu tiên thu hút qua những cuộc đối đáp rất thông minh của cả hai nhân vật chính, những tình huống đưa ra cũng logic nhưng càng về sau, lối suy nghĩ của Jocelyn và Jane làm mình khá bối rối. Đôi lúc, mình ngẫm mãi mà vẫn không hiểu được tại sao mọi thứ lại diễn ra như vậy, tại sao lại như thế này mà không như thế kia,... Gấp sách lại, mình cảm thấy hơi thất vọng và thật sự chưa hài lòng lắm về cái kết như vậy.
3
84284
2013-05-23 23:45:24
--------------------------
76601
8995
mình thấy bìa sách mang đúng hiệu phương tây nhưng màu tím đã làm cho nó thêm thơ mộng và hài hòa với nét cổ điển . nội dung truyện cũng tàm tạm . mình vẫn thích những truyện hài hơn những truyện tình cảm ướt át . giọng văn nhẹ nhàng , văn phong chính xác , tinh tế . nhân vật được xây dựng khéo léo và dường như hoàn hảo . tình yêu là một thứ tuy ngọt ngào nhưng lúc nào cũng lắm trắc trở . cơ hội và lý trí luôn khiến trái tim ta phải bâng khuân không biết sao cho đúng . nhưng khi bất chợt hai trái tim đã bắt gặp và hòa quyện thì tình yêu lại trở nên tươi đẹp hơn bao giờ hết .
5
114543
2013-05-23 19:51:46
--------------------------
76171
8995
"Tin em và yêu em" là một cuốn sách rất hay về tình yêu. Đôi khi tình yêu đến rất bất ngờ làm ta không thể ngờ tới, chính nó làm ta hạnh phúc nhưng cũng chính nó làm ta đau. Bằng văn phong nhẹ nhàng và lôi cuốn tác giả đã rất thành công trong việc xây dựng nhân vật và cốt truyện. Theo tôi thì cốt truyện cũng không mới cho lắm nhưng chính lời kể của tác giả khiến cuốn sách trở nên đặc biệt. Hai nhân vật chính đến với nhau với khái niệm là "tình nhân" nhưng sau đó họ không biết con tim đã thuộc về nhau từ lúc nào. Theo tôi thì những bạn thích văn học lãng mạn sẽ không bỏ qua cuốn sách này đâu và tôi cũng rất thích nó!
4
93778
2013-05-21 15:42:03
--------------------------
75501
8995
Khoa học đã chứng minh rằng, sự tiếp xúc về mặt xác thịt cũng có thể dẫn đến phát sinh tình cảm. ;)
Tình nhân là 1 chủ đề không mới, không cũ ;)
Tôi thích văn văn phong vừa phóng khoảng, lại tình cảm của Mary Balogh. Tôi thích tên sách cũng như bìa sách, cả cách xưng ta - em.

Rất kì lạ rằng, không giống như các độc giả khác, tôi lại không hề ghét mối quan hệ mang tên Tình Nhân ( mặc dù nếu người yêu tôi có tất nhiên tôi sẽ không chấp nhận =)) . Ngược lại, tôi khá hứng thú với những quyển sách với nội dung này.
- Mong rằng trong thời gian sắp tới đây là sẽ cuốn sách được đón đợi.
3
118694
2013-05-18 18:37:20
--------------------------
73378
8995
Tình yêu - Đến rất tự nhiên khi có sự hòa quyện và đồng cảm của trái tim. Nhưng đôi lúc tình cảm và lí trí luôn là hai thứ đồng hành và khiến cho ta luôn cảm giác không biết làm thế nào cho đúng. 
Trái tim mách bảo cần giữ chặt người mình yêu, làm cho người mình yêu cảm thấy hạnh phúc như chính niềm hạn phúc mình đang có. Nhưng đôi lúc chính lí trí quá mạnh khiến cho bản thân có những lời nói, hành động khiến cho đối phương cảm thấy hụt hẫn, bực tức, là thất vọng. 
Không quá hoàn hảo, không phải là vấn đề mới, nhưng quyển sách như một lần nữa gửi đến tôi một thông điệp "Đã  yêu - Thì phải tin - Khi tin tưởng thì mới có một tình yêu trọn vẹn" và đôi lúc đừng để lí trí khiến cho con tim trở nên đau nhói - vì khi bạn làm người yêu mình đau, thì bạn cũng đang thấm một nỗi đau không kém.
3
34754
2013-05-07 22:39:32
--------------------------
