397761
9279
"Doraemon bóng chày" – là một bộ truyện tranh của tác giả Mugiwara Shintaro & Fujio Fujiko. Bộ truyện này có nhiều tình tiết hấp dẫn, ly kỳ nhưng không kém phần mộc mạc, giản dị, hình vẽ đẹp, lời thoại độc đáo rất cuốn hút khiến bạn không thể rời mắt. Truyện cí nội dung kể về những trận đấu bóng chày của đội Dora do Kuroemon làm đội trưởng. Đội Dora có tinh thần đoàn kết rất cao. Trong đội ngũ ị Dora, mình ấn tượng nhất với nhân vật Chibiemon, cậu thường gây ra rắc rối cho cả đội nhưng cũng có lúc cậu cũng đã cứu nguy rất ngoạn mục. Một manga series đặc sắc!
5
1142307
2016-03-15 13:40:40
--------------------------
301405
9279
Chung kết, chung kết, chung chung kết rồi các bạn ơi. Đội Nhật Bản của chúng ta sau bao gian truân cách trở đã vào được trận chung kết trạm chán với kình địch Mĩ.
Trận đấu bóng chày đỉnh cao giữa các 'dị nhân' làn bóng chày thế giới sẽ mang đến những cảm xúc khó diễn tả bằng lời. Liệu đội nào sẽ vô địch ? Nhật Bạn hay là Mĩ đây? Phải đọc ngay đọc ngay thôi. Dự là Shiroemon, tuyển thủ xuất sắc của chúng ta sẽ có một tuyệt chiêu độc nhất vô nhị để tiếp đón đội Mĩ đấy.
4
188969
2015-09-14 17:08:15
--------------------------
221962
9279
Vậy là trận chung kết "tử chiến" hoành tráng của giải đấu WABC đã khép lại, và đội bóng chày nghiệp dư số một thế giới đã thuộc về đội Mĩ rồi. Tiếc ơi là tiếc, đội Nhật đã cố gắng rất nhiều, nếu như có cả Hiroshi tham gia trong đội tuyển thì chắc có lẽ tình hình đã khác rồi :(( Nhưng không sao vì Doras vẫn còn nhiều giải đấu phía trước. Những đồng đội trong đội tuyển Nhật Bản khi tham gia giải WABC bây giờ lại trở thành đối thủ của nhau trong giải vô địch bóng chày toàn Nhật Bản, thật hấp dẫn, Doras ngay trận đầu tiên đã gặp đối thủ mới là Monta, không biết sẽ phải đối mặt với điều gì nữa đây. 
5
471112
2015-07-04 18:49:53
--------------------------
