485268
8473
Cuốn sách khá mỏng, vì vậy mà mới nhận hàng hôm qua nhưng mình đã đọc một lèo hết 2/3 cuốn. Lối viết mới mẻ, chân thực, mình cực kì thích văn phong của chị này. Phức tạp nhưng lại rất đơn giản, chỉ cần nhìn đó khách quan hơn một chút, bạn sẽ nhận ra nhân vật của Khiêm Nhu thực đến độ bạn tưởng đang chứng kiến những cuộc đời thực trước mắt mình. Quan trọng là những cái kết, khiêm nhu không để những nhân vật của mìh đi lạc trong rừng chữ mà chị viết. Tôi thích chị ở điểm này. Nói chung thì cuốn sách này không dành cho những bạn mê ngôn tình hay mong muốn những câu chuyện ngắn kiểu "người người cười, nhà nhà vui"
5
970197
2016-10-01 09:04:47
--------------------------
430967
8473
Lối viết của Khiêm Nhu khá mạnh mẽ, đâm thẳng vào từng nhân vật- cũng chính là con người thật ngoài đời. Nhưng chính cách viết này khiến người đọc không quen cảm thấy không thoải mái. Nó quá trần trụi mà những sự thật trong cuộc sống, khi đi vào văn chương lẽ ra nên có cách nhìn thâm thúy hơn.  Đây là quyển sách mỏng tuy nhiên tôi đã mất khá nhiều thời gian để đọc một câu chuyện bên trong và rồi bỏ dở nửa chừng. Từng đoạn văn đôi khi không biết đó là lời của ai, của nhân vật hay tác giả khiến người đọc cảm thấy bối rối. Cốt truyện không nhiều gay cấn nhưng là sự thật ngoài đời thực, nhưng hình ảnh của tác giả mang đến đôi khi khiến đọc giả không hiểu ngụ ý của nó là gì. Tóm lại đây là tác phẩm không dành cho những người yêu thích cái thâm thúy trong văn chương.
2
610546
2016-05-16 12:33:31
--------------------------
402179
8473
Quyết định mua cuốn này chỉ vì tựa đề của nó, cái đề nghe rất hay vì thực sự tác giả Khiêm Nhu là cái tên lần đầu mình nghe.
Khi nhận sách bìa và giấy ngả vàng, nói chung khá hoài cổ
Khi đọc thì thất vọng hoàn toàn, không  có truyện ngắn nào mình nắm bắt nổi nội dung. Ở đây không phải chê tác giả viết không hay mà người đọc không thể hiểu được tác giả muốn nói gì. Những nhân vật cứ lẫn vào nhau rất khó hình dung và hiểu được tâm lý nhân vật.
Cuốn này là cuốn khó đọc và khó hiểu.
2
913721
2016-03-21 21:28:55
--------------------------
104476
8473
"Ngôi nhà không cửa sổ" của Khiêm Nhu  được tôi xếp vào danh sách "những quyển sách đọc 1 lần và không muốn đọc lại lần nữa'. Đúng là lối viết chị rất hay, rất lạ, cốt truyện những truyện ngắn cũng khá đặc biệt, miêu tả hiện thực khách quan xã hội nhưng tôi vẫn không cảm được. Có cảm giác truyện của chị rất là rối rắm, đọc khó hiểu, trong cùng 1 câu chuyện nhưng chuyện nọ lại xọ qua chuyện kia, chị miêu tả nhiều, kể nhiều nhưng phần lớn truyện ngắn có cái kết không thỏa đáng hay chẳng có gì cả, có cảm giác đó chỉ là 1 lát cắt ngẫu nhiên trong cuộc sống của 1 ai đó, không đầu không cuối, trước đó nhân vật đã làm gì ra sao không ai biết và sau đó nhân vật sẽ như thế nào chẳng ai hay. Đọc xong mà tâm trạng cứ lửng lơ, khó chịu, không hiểu là mình đã đọc được gì và hiểu những gì. Nói chung, quyển này tôi không thích lắm!
2
46905
2014-01-13 20:02:42
--------------------------
