553713
9930
Mình mua đồng thời 3 quyển, trong đó có quyển sách này và quyển nuôi con không phải là cuộc chiến, ăn dặm kiểu nhật. Vì mình chưa có bầu nên háo hức đọc ngay cẩm nang mang thai và sinh con và nuôi con không phải là cuộc chiến, nhưng quyển sách mang nặng tính khoa học, Lý thuyết, khoa giáo. nói chung mình ko thích quyển này
2
5100207
2017-03-25 10:08:49
--------------------------
432690
9930
Mình đang mang thai lần đầu và rất lúng túng trước vô vàn vấn đề liên quan mình tìm hiểu trên mạng.

Được giới thiệu quyển sách này, mình đã quyết định mua và cảm thấy cực kì tâm đắc. Sách in màu, hình ảnh minh họa sống động, trình bày rất logic và trả lời đúng cho những mối quan tâm của các bà mẹ đang mang thai. Đặc biệt sách được biên tập cùng bác sĩ Phượng (BV Từ Dũ) người đã đỡ đẻ khi mẹ mình sinh em bé mặc dù hôm đó bác sĩ không phải là bác sĩ đỏ cho mẹ mình, nhưng khi thất ca sinh khó, bác sĩ đã ko nề hà đỡ cho mẹ mình mẹ tròn con vuông. Sách được trình bày rất có tâm như người bác sĩ năm nào đã đỡ giúp mẹ mình!
5
1224066
2016-05-19 15:46:56
--------------------------
405589
9930
Sách này khá hay. Sách ghi khá chi tiết và giải thích rõ các vấn đề khi mang thai thường gặp và những bệnh lý gặp khi mang thai, khi nào bình thường, khi nào không. Chất lượng sách rất tuyệt. mình nghĩ nếu ai mang thai lần đàu nên sở hữu cuốn sách này. Nội dung cuốn sách rất hữu ích cho những ai chưa có kinh nghiệm về chuyện sinh con, cách chuẩn bị cho thành viên mới như thế nào là phù hợp và tâm lý của người cha người mẹ khi phải đối mặt với sự thay đổi trong cuộc sống

5
653217
2016-03-26 22:28:39
--------------------------
