501293
9232
Sức mạnh của trí tuệ xã hội vô cùng đáng kinh ngạc. Mình cảm thấy thật may mắn khi đọc được quyển sách này. Quyển sách có bài tự đánh giá về trình độ lắng nghe của bản thân và thật ngạc nhiên mình đã cho mình thang điểm là 20/100, quá thấp. Và mình mới bắt đầu phát hiện là mình đã thiếu sót điều gì trong khoảng thời gian coi thường các cuộc giao tiếp, xem giao tiếp là một điều thật chán ngắt và kinh khủng. Mình không cảm thấy thú vị khi giao tiếp vì sao? Vì mình đánh giá lời người nói theo hướng chủ quan của bản thân, áp đặt suy nghĩ của họ theo hướng của mình, mình chỉ muốn nghe những gì mình thích mà thôi,... Nhưng khi đọc quyển sách mình đã quyết định thay đổi suy nghĩ phiến diện của bản thân về giao tiếp. Bạn không cần phải nói thật nhiều để tỏ ra mình thông thái ra sao, bạn chỉ cần lắng nghe một cách chân thành và thái độ của bạn sẽ biểu hiện tất cả, 55% thành công trong giao tiếp là nhờ vào thái độ, hãy suy nghĩ kỹ.
5
405647
2016-12-28 10:16:24
--------------------------
435877
9232
Cách đây vài ngày, tôi mua cuốn sách sức mạnh của trí tuệ xã hội - tony buzan. Cuốn này tôi thấy khá hay, hài lòng hơn cuốn làm chủ trí nhớ của bạn cùng tác giả. 
Sách đề cập từng vấn đề, từng khía cạnh. Phân tích chi tiết. Mức giá thì không phải chê. Mà sao cuốn làm chủ trí nhớ bạn thì nội dung không hay mà lại mắc thế. Cuốn này đem áp dụng thực tế được, phù hợp văn hóa nơi công sở, làm việc. Nhưng tôi nghĩ tác giả nên kết hợp thực hành nhiều hơn. Sách này nặng tính lý thuyết.


5
696994
2016-05-25 21:34:09
--------------------------
397586
9232
Mãng xanh của sự sống còn
Ngày nay áp lực tiến hóa xã hội đã đẩy chúng ta gần như thoát khỏi áp lực chọn lọc tự nhiên. Việc thiết lập được mối quan hệ xã hội tốt là điều thiết yếu – đó chính là nền tảng của sức mạnh Trí tuệ xã hội. Cả cuộc đời chúng ta luôn đồng hành và nhận sự giúp đỡ từ người khác. Tất cả mối tương tác trong xã hội đều có thể mua bán được. Nếu chỉ dừng lại ở mức độ kiểm soát cái vỏ thô thiển bên ngoài đó – kiểm soát đồng tiền, chúng ta chỉ là nô lệ. Sức mạnh của trí tuệ xã hội giúp chúng ta kiểm soát và phát triển tốt các điểm trọng yếu tạo nên quan hệ sâu sắc, chân thật. Thêm một lần nữa, bậc thầy khai thác bộ não - Tony Buzan – giúp chúng ta tự định hướng và phát triển chính mình.
4
636922
2016-03-15 10:18:28
--------------------------
397161
9232
Nói thật thì mình không hài lòng lắm về cuốn sách này. Mình đọc được chừng nửa cuốn là ngán ngẩm lắm, vì mình trót đọc "Trí tuệ xúc cảm" (Daniel Goleman) rồi. Tự nhiên mình có sự so sánh giữa hai cuốn này. Nói chung, sách khá ngắn gọn, có nêu được tầm quan trọng của những kỹ năng thể hiện trí tuệ xã hội, nhưng không toát lên được cái vai trò và những phương thức để đạt được.
Ngoài cái bìa trang trí lộng lẫy ra, mục lục xem qua thì có vẻ hấp dẫn, còn lại mình ko nghĩ nó xứng đáng là best selling.
Tuy nhiên, em gái mình có vẻ hào hứng, lại còn khen sách hay. Đúng là mỗi người một cảm nhận. Thế là mình quyết định cho em gái luôn... May mà sách này giá tiền cũng thấp, chứ ko thì mình sẽ tiếc tiền mất !!! (À, lúc mình mua là 33,000; đến thời điểm cmt này thì chỉ còn 30,800vnd. Haiza).
3
358928
2016-03-14 15:47:53
--------------------------
322043
9232
Đầu tiên, tôi rất bắt mắt với bìa sách, rất teen và nổi bật. Tôi cũng được biết đến Tony Buzan qua lời giới thiệu của thầy cô và bạn bè, điều đó càng khiến tôi tò mò hơn về cuốn sách kỹ năng này. Nội dung trong sách khá chi tiết và có những ví dụ thực tế giúp ta dễ liên tưởng. Và sẽ càng tuyệt hơn nếu cuốn sách chi tiết hơn nữa và có điểm nhấn, highlight để người đọc ghi nhớ lâu hơn.
Tôi đã mua và tặng cuốn sách cho người bạn của mình, và đây là món quà tinh thần vô cùng tuyệt cú mèo. Cám ơn tác giả cũng như NXB đã mang cuốn sách này đến đọc giả Việt Nam.
4
95468
2015-10-15 14:42:40
--------------------------
310977
9232
Cuốn sách hay về trí tuệ xã hội, giúp người đọc hiểu thế nào về chỉ số trí tuệ xã hội, mức quan trọng của nó trong xã hội con người. Cuốn sách đem tới những kỹ năng đọc ngôn ngữ cơ thể, cách lắng nghe, thương lượng giúp người đọc thấu hiểu và tương tác hiệu quả với mọi người, từ đó bạn đọc có thể xây dưng và mở rộng mối quan hệ xã hội, hình thành phong thái và có thái độ đúng đắn, chín chắn hơn. Cuốn sách là kim chỉ nam để đạt được thành công trong xã hội.
5
447930
2015-09-19 20:53:29
--------------------------
306507
9232
Về hình thức, quyển sách cuống hút mình ngay từ cái nhìn đầu tiên. Bìa được thiết kế khá hiện đại. Trang giấy bên trong chất lượng khá tốt. Bố cục lại rất rõ ràng, thuận lợi cho người đọc (mình để ý thấy hầu như quyển nào của Tony Buzan cũng đều có ưu điểm này).
Về nội dung, tác giả Tony Buzan đã là bảo chứng cho dòng sách kỹ năng xã hội thực hành, mà quyển Sức Mạnh Của Trí Tuệ Xã Hội này là một minh chứng. Sách gồm nhiều chương mục. Mỗi chương là một bài học, giải quyết những vấn đề khác nhau mà người đọc cần cải thiện để nâng cao khả năng giao tiếp của mình như kỹ năng xây dựng mối quan hệ, thương lượng, lắng nghe.... Quyển sách có lối hành văn khúc chiết, đi ngay vào vấn đề nhưng lại giúp người đọc hiểu sâu sắc nội dung đang trình bày. 
Mình đã mua được hai quyển là Sức mạnh của Trí tuệ xã hội và Trí tuệ sáng tạo rồi. Lần sau sẽ mau thêm quyển Trí tuệ tâm linh nữa là đủ bộ 3 quyển. Mình thật sự rất thích bộ sách này của Tony Buzan. Rất bổ ích!
5
398179
2015-09-17 16:54:44
--------------------------
306261
9232
Sách mỏng và ngắn gọn, bạn có thể đọc trong thời gian rất ngắn nhưng cần thường xuyên ôn lại. Nội dung sách viết về điều lợi ích của việc giao tiếp với nhiều người. Chỉ bạn cách có thể mạnh dạn và dễ dàng tiếp xúc và giúp cho mọi người yêu mến bạn. Thực tế con người không thể sống vui vẻ khi tách biệt với người khác. Việc tiếp xúc nhiều với mọi người sẽ  giúp bạn sống lâu hơn và yêu đời hơn. Đó là những gì mình rút ra sau khi đọc sách. Sách đã cổ vũ mình mạnh dạn và mở lòng ra đối với người lạ.
5
65619
2015-09-17 14:24:51
--------------------------
284304
9232
Sức Mạnh Của Trí Tuệ Xã Hội của tác giả Tony Buzan quả thực là một cuốn sách bổ ích khi cung cấp cho người đọc những kiến thức quan trọng để có thể giao tiếp hiệu quả. Trong xã hội ngày nay thì giao tiếp thực sự là một kỹ năng cực kỳ cần thiết để có thể gặt hái thành công trong mọi lĩnh vực. Hi vọng trong tương lai mình sẽ có cơ hội tham khảo hết các tựa sách khác của tác giả Tony để có thể tiếp nhận thêm nhiều kiến thức bổ ích như thế này.
4
307488
2015-08-30 20:44:27
--------------------------
279810
9232
Sức Mạnh Của Trí Tuệ Xã Hội là cuốn sách rất hữu ích nó được tổng hợp từ những mẫu số chung về vấn đề của con người khi đứng trước các vấn đề giao tiếp, ứng xử với nhau. Ở đó tác giả đã tổng kết những cái chung nhất mà chúng ta cảm thấy đôi khi có mình có trong hoàn cảnh đó! Vậy chúng ta sẽ làm gì để có cách ứng xử nó hài hòa văn minh nhất? Đây là cuốn sách bổ ích rất phù hợp cho giới trẻ nói chung và xã hội Việt Nam của chúng ta khi mà những giá trị căn bản nó dần mất đi mà thay vào đó cách hành xử lại theo bản năng. Biết đâu khi có những cuốn sách "Sức Mạnh Của Trí Tuệ Xã Hội" làm gương thần đối chiếu, bản lề cho hành vi của chính mình ngày một văn minh hơn.
5
256925
2015-08-27 09:44:28
--------------------------
274456
9232
Sách tương đối mỏng nhưng chứa đựng những thông tin có ích một cách đầy đủ về các phương pháp nâng cao kĩ năng xã hội - một kĩ năng cực kì cần thiết cho sự thành công của một con người. Có thể kiến thức chuyên môn của bạn rất vững vàng nhưng nếu thiếu kĩ năng giao tiếp ứng xử thu phục lòng người và thuyết trình có sức hút thì cũng khó có thể tạo dấu ấn trong xã hội.
Một cuốn sách đáng mua. Các bạn yên tâm mua sách vì tác giả là bậc thầy về tư duy - Tony Buzan. 
5
186614
2015-08-21 22:40:11
--------------------------
265083
9232
Một cuốn sách kỹ năng tuy rất mỏng nhưng giá trị mà sách đem lại rất lớn, thông điệp mà sách truyền tải rất ý nghĩa. Cuốn sách giúp bạn khám phá, tìm hiểu về các kỹ năng xã hội mà 1 con người cần có để đạt được các thành công nhất định trong cuộc sống, như là khả năng làm chủ ngôn ngữ cơ thể, nghệ thuật lắng nghe, cách xây dựng mối quan hệ, thái độ và phong thái đúng đắn... Còn có 1 bảng khảo sát về trí tuệ xã hội để giúp bạn hiểu rõ hơn và biết cách áp dụng những điều đã được chỉ dẫn. Nếu bạn biết nghiền ngẫm và làm theo lời tác giả Tony Buzan thì bạn sẽ có tự tin hơn khi bước ra ngoài thế giới, sẽ trở thành người có khả năng giao tiếp tốt và toả sáng giữa đám đông, thái độ sống của bạn cũng theo đó mà tích cực hơn. Mình vô cùng ưng ý quyển sách này ! Khuyết điểm duy nhất là chất giấy mỏng quá.
5
75959
2015-08-13 18:06:37
--------------------------
204731
9232
Sau khi nhận được cuốn sách từ Tiki, mình đã đọc hết cuốn này chỉ trong 30 phút do sách tương đối mỏng. Tuy nhiên những gì mình nhận được từ cuốn sách thì không hề 'mỏng' chút nào. Đúng như tác giả nói, IQ và cơ bắp không phải là những điểm mạnh duy nhất cần phát triển nếu muốn thành công về mặt xã hội mà ta còn phải trau dồi thêm những kỹ năng vô cùng quan trọng khác để có thể thấu hiểu và tương tác hiệu quả với mọi người.10 chương trong cuốn sách là những kinh nghiệm, lời khuyên và chỉ dẫn cô đọng, súc tích mà không kém phần hấp dẫn của tác giả dành cho người đọc, giúp tạo động lực cho những người kém giao tiếp như mình. 
Đây là một cuốn sách hay, hữu ích với tất cả mọi người.
5
365004
2015-06-04 20:24:16
--------------------------
138088
9232
Bằng cách phát triển trí tuệ xã hội, bạn có thể hiểu và đánh giá tất cả mọi người bạn gặp; biết được những gì thúc đẩy họ , những nhu cầu cá nhân của họ là gì, và làm thế nào bạn có thể làm cho họ cảm thấy thoải mái và hài lòng được khi ở bên bạn . Với bốn bản đồ sinh động , cuốn sách này cho bạn thấy làm thế nào để cải thiện mọi khía cạnh của kỹ năng xã hội của bạn , cả trong các mối quan hệ và công việc, trở nên tự tin hơn, có một cuộc sống xã hội năng động và thành công , trở thành một người giao tiếp tốt hơn và tự tin khi phát biểu trước công chúng , cải thiện mọi khía cạnh kỹ năng xã hội của bạn , trong các mối quan hệ và công việc.
5
462453
2014-11-30 18:04:37
--------------------------
87955
9232
Trong cuộc sống ngày một hiện đại, khi mà xã hội ngày một "lớn" hơn trên nhiều nghĩa, chúng ta cũng cần gắn với nhiều người hơn, xây dựng nhiều mối quan hệ xã hội hơn, thì việc chúng ta có những hiểu biết nhất định và sự cư xử đúng mực trong cuộc sống là điều vô cùng quan trọng
Sức Mạnh Của Trí Tuệ Xã Hội của Tony Buzan phân tích nhiều khía cạnh, để chúng ta có thể tự khám phá và hiểu chính mình, xa xôi hơn là hiểu về mọi người xung quanh, về xã hội đang vận động quanh ta. Nhờ đó, chúng ta nhận ra được giá trị của bản thân mình, vị trí của mình, cũng như mọi thứ xung quanh. Và, ta nhận thấy, việc hòa mình vào cuộc sống này, không hề khó khăn, mà thật dễ dàng, chỉ cần chúng ta biết được những cách thức nho nhỏ, để biến cuộc đời mình trở nên có ý nghĩa nhất.
Đây là một cuốn sách hay, bổ ích và thực tế, nhất là đối với những người trẻ tuổi trẻ lòng,về chất lượng hình thức thì cuốn sách nào của First new cũng rất tuyệt rồi.
5
10984
2013-07-22 09:33:44
--------------------------
76890
9232
Mỗi cuốn sách của Tony Buzan dường như hướng con người tự mình khám phá bản thân và thành công. Mình là tín đồ của ông, dường như hàng loạt sách đều được mình săn đón và mình cảm thấy thay đổi thật sự. Sách ông viết về khoa học nhưng không khô cứng, thậm chí là rất thú vị. Nếu ai đã từng đọc "Sức mạnh của trí tuệ sáng tạo" thì "Sức mạnh của trí tuệ xã hội" sẽ làm bạn bất ngờ và lôi cuốn bạn. Cuốn sách hữu ích đối với tất cả ai muốn cải thiện mối quan hệ xã hội, khám phá và phát huy sức mạnh bản thân. Đọc rồi sẽ ngỡ ngàng hóa ra có rất nhiều điều mình nghĩ không thể nhưng thật sự rất dễ dàng, dễ dàng đến nỗi đã có ở mỗi người. Và thật sự có thể sau cuốn này bạn lại muốn đọc thêm "Sức mạnh của trí tuệ sáng tạo" hay "sức mạnh của trí tuệ tâm linh" chẳng hạn. Hãy đọc sách và bạn sẽ thấy thỏa mãn về những cách cải thiện và phát triển các mối quan hệ theo cách thức đơn giản nhất!
5
72064
2013-05-24 18:54:58
--------------------------
71312
9232
Tony buzan là một tác giả nổi tiếng trên toàn thế giới với rất nhiều tác phẩm được đông đảo mọi người đón đọc, như: Sơ đồ tư duy, sách dạy đọc nhanh,...có những cuốn của ông mà mình thấy rất hữu ích tuy nhiên cũng có những cuốn mà nó rất khó áp dụng vì không phù hợp với người việt chúng ta.
Tuy vậy cuốn Sức mạnh của trí tuệ xã hội này theo mình là rất hay, nó giúp chúng ta có thể cải thiện các mối quan hệ, trở thành người bạo dạn hơn trong xã hội, bởi chính các yếu tố xã hội như giao tiếp tốt thì đó sẽ là một nấc thang quan trọng trong sự nghiệp của chúng ta.
Một cuốn sách hay dành cho tất cả mọi người để có được cách ứng xử tốt hơn.
5
60663
2013-04-26 12:37:53
--------------------------
