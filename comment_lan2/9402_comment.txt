357993
9402
“Đam mê – bí quyết tạo thành công” của hai tác giả Covey và Colosimo thích hợp với những ai đang định hướng nghề nghiệp và muốn tìm kiếm việc làm nhiều hơn là những người đã có công việc ổn định. Nội dung sách nhấn mạnh đến khả năng bản thân. Nổi bật là bảng so sánh để nhìn ra sự khác biệt giữa lối tư duy thời đại công nghiệp và thời đại trí thức. Phần cuối sách là các chia sử và giải đáp các chiến lược, lời khuyên hữu ích cho việc chuẩn bị và tiến hành các bước để có thể vượt qua đối thủ, đạt được công việc thích hợp. Cuối cùng,điểm cộng cho sách bọc sách miễn phí.
3
37034
2015-12-24 15:52:52
--------------------------
289112
9402
Nghe cái tên khá hay và của R.Covey nên mình mua luôn. Cuốn sáchkhông làm mình thỏa mãn với tiêu đề nhưng phần nào cũng nói lên quan điểm của 2 người về niềm say mê, quyết tâm với công việc. Đồng thời cũng hướng dẫn chúng ta trong việc đăng kí xin việc ( mình thấy chưa phù hợp ở nước mình lắm) và  phát triển các mối quan hệ trong công việc trong thời đại công nghệ thông tin. Nói chung khá hợp cho ai bắt đầu khởi đầu giai đoạn xin việc, phát triển đam mê và duy trì các mối quan hệ công việc.
4
641403
2015-09-04 09:30:55
--------------------------
282991
9402
Đúng với cái tên của cuốn sách có đam mê mới tìm thấy được sự thành công,cuốn sách giúp mình biết được thành công rất dễ đạt được chỉ cần bạn biết bạn thích làm gì,đam mê lĩnh vưc nào ?....Lúc đầu mình cũng không biết bản thân mình đam mê gì nhưng sau khi đọc cuốn sách này mình dần nhận ra mình đam mê rất nhiều thứ và mình chắc chắc sẽ theo đuổi đam mê đó để thành công,cuốn sách nhỏ gọn và cũng dễ đọc nữa.Bạn nào chưa tìm được đam mê của mình là gì thì hãy mua cuốn sách này và tìm kiếm chúng  
5
48148
2015-08-29 16:44:45
--------------------------
262013
9402
mình mua cuốn sách này vào thời điểm đang xác định đam mê để chọn cho mình một nghề nghiệp phù hợp nhất. Thấy tên tác giả Steven R. Covey, cái không đắn đo mua luôn. Chắc bạn biết cuốn sách nổi tiếng của ông là gì rồi chứ. Quả là không sai lầm khi mua và những lời nhận xét của các tác giả danh tiếng ở phần bìa cuốn sách thật không sai. Chắc hẳn bạn đã từng nghe đến hai từ "đam mê" với những tư tưởng đi kèm như: hãy theo đuổi đam mê, thành công sẽ theo đuổi bạn hay bạn biết được những người thành công đều đã theo đuổi đam mê của mình nhưng bạn lại không biết mình đam mê gì, càng ngồi nghĩ xem đam mê của mình là gì thì lại càng không tìm thấy. Thì đây, cuốn sách này sẽ là người hướng dẫn cho bạn. Bạn sẽ thấy rằng đam mê không khó tìm như chúng ta nghĩ. Từ đó hãy dám theo đuổi đam mê cảu mình.
5
560697
2015-08-11 16:22:04
--------------------------
226873
9402
Tôi đã loay hoay trong một khoảng thời gian dài vì mất định hướng, không biết đam mê thực sự của mình là gì. Điều này thôi thúc tôi bỏ ra thời gian để tìm đam mê dù có khó khăn như thế nào đi nữa. Tôi đã mua được cuốn sách này trong một lần tìm kiếm trên tiki, dù cuốn sách chứa đựng nhiều triết lý sâu sắc mang tính cơ sở. Tôi đã suy nghĩ rất nhiều về những điều cuốn sách nói. Và tôi đã luôn cảm ơn cuốn sách này vì nó đã góp phần tạo nên một tôi hạnh phúc hơn rất nhiều.
5
15195
2015-07-13 08:41:01
--------------------------
224921
9402
Tôi tìm và mua rất nhiều quyển sách với từ khóa "đam mê" trên tiki.vn trong khi đang chán nản và mất phương hướng với công việc hiện tại của mình. Trong lúc này, tôi rất cần một cú hít tinh thần để vượt qua và tôi đã không ân hận khi đọc quyển sách. Sách gồm 02 phần khá ngắn gọn, bao gồm các mục chính: giúp bạn biết được thế mạnh của mình, khám phá động cơ làm việc, cống hiến khả năng tốt nhất, tìm một công việc như mong muốn và tạo dựng một công đồng quan hệ riêng. Tôi thật sự đã hiểu được mình hơn và có động lực hơn rất nhiều từ những nhân vật trong sách. Họ vô tình khám phá được đam mê và đuổi nó đến tận cùng. Họ đã rất thành công trong công việc và cuộc sống (thành công ở đây không phải chỉ là giàu có về tiền bạc). Tuy nhiên, quyển sách còn trình bày quá nhiều về vấn đề  xin việc (làm thuê), mà ít nói đến vấn đề tinh thần khởi nghiệp (làm chủ). Vì thế, một số bạn mong muốn khởi nghiệp sẽ hơi thất vọng khi đọc quyển sách này. Chúc các bạn sớm tìm được nguồn đam mê của chính mình. Thân mến!
5
115038
2015-07-09 14:45:07
--------------------------
190265
9402
Tôi là một độc giả rất hâm mộ cố tiến sĩ Stephen Covey và những quyển sách, video diễn thuyết của ông. Tôi học được ở những tác phẩm của ông rất nhiều điều, đặc biệt là về quản trị và tư duy con người. Ở quyển sách này, ông đã cho chúng ta biết thành tố đầu tiên và quan trọng nhất sẽ quyết định tới thành công của chúng ta trong tất cả mọi lĩnh vực trong cuộc sống. Đó chính là thành tố đam mê. Xác định những gì mình giỏi nhất, làm tốt nhất và thực hiện nó với một bầu nhiệt huyết đồng thời nuôi dưỡng niềm đam mê mỗi ngày là những gì tôi rút ra được từ quyển sách này. Tác giả viết rất dễ hiểu, không rườm rà và hấp dẫn tôi ngay từ cái tên của sách.
5
387632
2015-04-29 07:13:41
--------------------------
144752
9402
Mình đã quyết định mua cuốn sách này trên Tiki đợt vừa rồi.Muốn thành công thì cần rất nhiều yếu tố nhưng yếu tố quan trọng nhất và không thể thiếu đó chính là Đam mê.Không ai thành công tột bậc hay thành công rực rỡ mà không có yếu tố này cả.Đam mê giống như ngọn lửa nhiệt huyết,luôn cháy bùng trong ta và khiến ta khát khao mạnh mẽ với thành công.Stephen Covey-một diễn giả,tác giả hàng đầu thế giới,những ai mê sách kỹ năng sống thì không ai không biết ông đặc biệt qua cuốn 7 thói quen của người thành đạt.Cuốn sách này của ông  viết rất hay,dễ hiểu ,logic qua từng chương .Và đọc xong cuốn sách này,mình ngẫm ra rằng đam mê ai cũng có nhưng liệu có mấy ai can đảm theo đuổi đam mê của mình?Nhưng người hạnh phúc là người làm việc vì đam mê nên mỗi người trong chúng ta hãy dũng cảm để được sống là chính mình,sống với đam mê :)
5
456366
2014-12-27 22:34:07
--------------------------
72216
9402
Có thể nói Stephen Covey đã là một tác giả nổi tiếng trên khắp toàn thế giới, cuốn sách mà ông được biết đến nhiều nhất là "7 thói quen để thành đạt", ngoài cuốn đó ra thì mình cũng rất thích cuốn này của ông "Đam mê-Bí quyết tạo thành công"
Có thể nói nguồn động lực lớn lao nhất thúc đẩy một ai đó làm việc hăng say không ngừng nghỉ chính là lòng đam mê, nếu có lòng đam mê thì ta sẽ làm việc không biết mệt mỏi, chẳng những vậy mà còn có khả năng tăng năng suất và sáng tạo những điều mới lạ. Bởi vậy mới nói đam mê quan trọng đến như thế nào, nó chính là nguồn động lực cho mỗi chúng ta, hãy đọc cuốn sách này và hiểu thêm về đam mê và có thể bạn sẽ định hướng được cuộc đời bạn nhờ cuốn sách này đấy.
5
60663
2013-05-01 01:46:28
--------------------------
69644
9402
Tại sao ở những thế kỷ trước, con người có thể sáng tạo ra nhiều thứ và thành công trên nhiều lĩnh vực? Trong khi, ngày nay, thế kỷ 21 với những phát triển khoa học công nghệ rầm rộ, con người được hỗ trợ tối đa cho việc học tập, nghiên cứu và làm việc thì lại ít tạo ra kỳ tích như vậy? Một trong những nguyên nhân đó chính là bởi họ thiếu sự đam mê với một vấn đề nào đó. Trong "Đam mê-Bí quyết tạo thành công" đã đề cập đến vai trò quan trọng của sự đam mê trong công việc và học tập. 
Nếu làm một việc nào đó, mà bạn thiếu đi sự đam mê dành cho nó thì bạn sẽ nhanh chóng nhận ra sự nhạt nhẽo, lơ lững, thậm chí là mất thăng bằng với việc đó như thế nào. Thiếu sự đam mê sẽ làm cho bạn dễ dàng từ bỏ khi gặp bất cứ khó khăn nào dù nhỏ nhất. Từ đó, thành công sẽ cách bạn rất xa. Những nhà khoa học khi phát minh ra điện thoại, đèn điện, máy tính.v.v... Không phải họ làm một lần là được mà có khi cả trăm ngàn lần thất bại mới thấy được một chút hy vọng. Nếu không có sự đam mê ấy, thì con người chúng ta lấy đâu ra đèn điện, điện thoại, máy tính,v.v... Tuy nhiên, để tìm kiếm sự đam mê của bản thân, không phải ai cũng làm được và cũng không phải ai cũng dũng cảm để theo đuổi sự đam mê khi mà bạn không thể vượt qua được những cơm áo gạo tiền của cuộc sống. Cuốn sách như một sự khích lệ cho những ai đã và đang có niềm say mê nào đó. Và hãy dũng cảm lên để thực hiện. 
5
63376
2013-04-17 12:11:52
--------------------------
68241
9402
Lượn lờ hiệu sách và mình đã rinh được cuốn sách nho nhỏ này, có lẽ cũng bởi cái tên vừa thú vị, vừa thiết thực của nó. Quả là một cuốn sách hay, nó khiến mình có những suy nghĩ rất khác, về hai chữ "đam mê", về lòng nhiệt huyết, và về sự dám thực hiện những ước mơ trong đời. Điều này thật sự rất quý giá với mình, bởi mình đang là một học sinh lớp 12, còn quá nhiều bỡ ngỡ và phân vân trong việc lựa chọn một ngành học chuyên nghiệp, gắn liền với công việc của mình sau này.
Mình đam mê viết lách, mình cảm nhận được điều đó qua mỗi bài văn, trong từng câu chữ, khi mình cầm bút... nhưng mình biết , để thành công trong lĩnh vực này không hề dễ dàng, tìm kiếm được một công việc phù hợp, lại có mức lương cao và ổn định với nghiệp viết lách thì lại càng khó khăn hơn, và mình đã chọn kinh tế, một sự lựa chọn nằm trong xu hướng chung hiện nay, ừ thì không thể tiếp lửa cho đam mê của mình, nhưng ít nhiều cũng yên tâm hơn về cuộc sống sau này, mình nghĩ vậy, đù đôi khi trong lòng cũng nhen nhóm những sự tiếc nuối...
Mình nghĩ rằng không ít người trong các bạn cũng vậy, có người yêu thích thể thao, muốn được lân sân khấu với ước mơ làm ca sĩ, muốn được cầm trên tay những chiếc cọ màu sắc để vẽ những bức tranh sinh động... Nhưng rồi chẳng phải ai cũng biến mong muốn của mình trở thành hiện thực. Liệu đây có phải một thực tế đáng buồn :(
Mình đã băn khoăn lắm, cho tới khi nghiền ngẫm hết cuốn sách này, Covey và Colosimo đã mang đến những lời giải đáp hết sức khỏa đáng cho những băn khoăn trên. Không chỉ áp dụng trong kinh doanh, trong các tố chức, nó còn rất thiết thực với mỗi chúng ta.
Chính đam mê, là sức mạnh to lớn nhất, là động lực tinh thần không gì thay thế được giúp chúng ta say mê công việc, phát huy hết khả năng của bản thân và đạt được những thành công lớn, mang đến những cống hiến khác biệt cho nhân loại... Và mình cũng nhận ra rằng, chẳng có bất cứ điều  khủng khiếp nào có thể giết chết một đam mê, nuôi lớn một đam mê là nuôi dưỡng một cây thành công chờ ngay hái quả. Dù rằng cuộc sống giờ đây là đầy rẫy những rủi ro, thử thách, nhưng có hề gì, nếu trong tim ta luôn rực cháy một lòng nhiệt huyết cho sự đam mê của chính mình.
Mình đã có được sự lựa chọn mãn nguyện cho riêng mình ^^ nhờ cuốn sách hữu ích này. Mình sẽ là một nhà kinh tế trong tương lai, nhưng nhất định sẽ không bao giờ từ bỏ việc viết lách, từ bỏ sự sáng tạo lúc nào cũng hừng hực cháy trong tim, chảy trong từng mạch máu :)
Bởi đơn giản, Đó là Đam Mê- Bí Quyết Tạo Thành Công :)
5
10984
2013-04-10 23:12:47
--------------------------
