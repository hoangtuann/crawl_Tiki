460429
9813
Interactions 2 nối tiếp series Interaction của nhà xuất bản McGraw-Hill. Cuốn sách này không còn là những ngữ pháp cơ bản như cuốn đầu tiên nữa mà đã mở rộng thêm về thì, thể và các dạng động từ. Mình chọn mua cả bộ Interactions vì bị lôi cuốn về nội dung và cách trình bày khoa học trong cuốn sách. Interactions 1 và 2 thích hợp cho những bạn đang có như cầu ôn lại ngữ pháp căn bản cũng như trung cấp. Sách có phần ngữ pháp kèm hội thoại, và phần bài tập sau đó để củng cố thêm kiến thức.
5
76082
2016-06-27 00:58:16
--------------------------
