421139
9349
Phải nói rằng cuốn sách này thực sự rất hay,nếu không muốn nói là tuyệt vời!
Đây là cuốn sách mà bất cứ người nào làm việc trong lĩnh vực quảng cáo cũng cần phải có.
Sách là tập hợp những câu chuyện, những chia sẻ từ kinh nghiệm thực tế của một người đã dành hơn nửa cuộc đời cho công việc quảng cáo. Do đó, lối viết khá đơn giản, gần gũi và khá thực tế, tác giả đã thực sự khơi gợi cảm hứng trong mỗi người cũng như chỉ ra,định hướng cho bạn đến gần hơn với các ý tưởng mới mẻ, hữu ích.
Hãy đọc và cảm nhận cái hay của cuốn sách các bạn nhé!
3
614561
2016-04-24 15:27:10
--------------------------
328127
9349
Đây là một cuốn sách hay của Jack Foster, tác giả của "How to get ideas - Một nửa của 13 là 8". Với những trích dẫn, những phương pháp đơn giản mà hiệu quả để sản sinh ý tưởng, khơi dậy sự sáng tạo, cuốn sách tuy mỏng nhưng thật sự hay cho những ai muốn lãnh đạo và phát triển trong lĩnh vực quảng cáo. Không chỉ có vậy, với lối viết đơn giản, dễ hiểu và ngắn gọn, cuốn sách cũng chỉ ra những mẹo nhỏ cho những ai không ngừng tìm kiếm sự sáng tạo và những ý tưởng.
4
568029
2015-10-29 09:08:53
--------------------------
272368
9349
Về hình thức thì mình rất thích cả về chất lượng giấy, cách trình bày, trang trí, về nội dung cũng khá hay, giúp ích cho chúng ta rất nhiều trong cuộc sống, trong mọi lĩnh vực không chỉ riêng những lĩnh vực yêu cầu tính sáng tạo như marketing… tuy nhiên nó phù hợp hơn với những nhà lãnh đạo. Khi đọc nó bạn sẽ thấy rất quen thuộc nếu như đã đọc Một nửa của 13 là 8, hơi giống nhau về ngôn ngữ, về những câu chuyện, chính vì thế mà mình không thực sự thích cuốn sách này lắm.
3
504208
2015-08-19 23:30:06
--------------------------
157548
9349
Thật ra quyển sách này chỉ đọc để biết thôi chứ ứng dụng thì chỉ dành cho những người lãnh đạo.  Quyển sách mỏng nhưng khối lượng kiến thức, bài học, kinh nghiệm thì đặc biệt nhiều. Quyển sách chỉ ra rất nhiều cách để tạo môi trường làm việc vui vẻ, các mối quan hệ công sở vui vẻ, tinh thần vui vẻ trong công việc v.v... và vui vẻ chính là bí quyết để khơi nguồn sáng tạo. Cuốn sách chỉ ra nhiều cách ứng xử và đối ứng trong công việc tuy nhiên tôi khá nghi ngờ tính ứng dụng tại Việt Nam vì các cách thức trong quyển sách này chỉ phù hợp đối với cách lãnh đạo ở Phương  Tây, mà theo những gì tôi được biết, phong cách lãnh đạo giữa Tây và Đông  hoàn toàn khác biệt. Nhưng nói chung, quyển sách này có nhiều mẹo thú vị.
4
392352
2015-02-09 12:24:24
--------------------------
110347
9349
Ấn tượng đầu tiên với mình về quyển sách này, là nó mỏng, rất hợp với ý mình. Mình biết tác giả Jack Foster qua quyển một nửa của 13 là 8, ấn tượng với giọng văn và cách diễn đạt ngắn ngọn vừa đủ để hiểu, một kiểu tính cách thường thấy của những người làm trong ngành công nghiệp Quảng Cáo.

Đọc quyển sách, có cảm giác như được trò chuyện trực tiếp với với một giám đốc sáng tạo với kinh nghiệm đầy mình, Jack Foster đưa ra giới hạn của môi trường làm việc phổ biến hiện nay, mổ xẻ nó, cắt bỏ những thành phần gây ung bướu từ trong cách đối xử giữa người và người nơi công sở cũng như trong cuộc sống.

 Tác giả đưa ra giải pháp tối ưu để duy trì và cải thiện tình cảm giữa nhân viên với nhau, giữa các nhân viên và Leader để cùng hợp tác đưa ra những ý tưởng hay. Cùng hợp tác chứ không phải là chỉ huy người khác như cái quyền mà Leader có, và vô số những chiêu mà người làm trong ngành sáng tạo cần phải học hỏi.

Một cuốn sách vừa đủ để gọi là hay, đáng để đọc đi đọc lại nhiều lần, nếu biết cách vận dụng thì có thể áp dụng trong mọi lãnh vực cần sự giao tiếp chứ không riêng gì trong không gian công sở :)

Cám ơn Tác giả Jack Foster, dịch giả Thanh Vân, và những người ẩn danh đã góp phần tạo nên quyển sách này.
5
163702
2014-04-15 23:45:16
--------------------------
104944
9349
ấn tượng đầu tiên của mình về quyển sách này là .... hơi mỏng . Nhưng bù lại giấy và cách trình bày các kiểu thì khá được lòng mình . Các ý được sắp xếp theo từng mục một cách rất logic , mạch lạc ,dễ đọc và dễ hiểu . Hình ảnh minh họa cũng khá độc đáo và thú vị . Nhưng có lẽ quyển sách này phù hợp với những người lãnh đạo hơn là một đứa sinh viên yêu sáng tạo và muốn mở mang đầu óc đến với khu vườn của những ý tưởng . Chính xác hơn thì đó cũng chính là mục tiêu mà quyển sách này hướng tới : giúp cho những người lãnh đạo khai thác và khai sáng nhân viên của mình trong việc tìm kiếm ý tưởng . Cách chỉ dẫn trong cuốn sách cũng chưa thực sự có tính ứng dụng cao, còn khá là chung chung . 
Tóm lại nếu muốn mua quyển sách này thì mình khuyên các bạn nên đọc thử vài trang xem nó có thực sự phù hợp với bản thân hay không .
2
9357
2014-01-20 19:28:23
--------------------------
53682
9349
Đây là một trong những cuốn sách dạy kỹ năng hay nhất, lôi cuốn nhất mà mình từng được đọc. Dù đang là học sinh, sinh viên hay người đã đi làm, cuốn sách đều cung cấp những kiến thức vô cùng bổ ích để giúp ích cho công việc, cuộc sống mỗi ngày. Cách viết của tác giả rất đặc biệt, độc đáo và lôi cuốn, khiến mỗi trang sách nhẹ nhàng đi vào lòng độc giả. Những mệnh đề, luận điểm được triển khai một cách logic, mạch lạc, có sức thuyết phục cao, khiến người đọc nhìn nhận rõ sức mạnh của sự sáng tạo, nó có thể đem lại những thành công không ngờ tới. Thêm vào đó, mình rất thích minh họa trên bìa sách, rất đẹp và thú vị, tạo cảm hứng cho người đọc ngay từ lần đầu tiên nhìn thấy.
4
20073
2013-01-04 15:46:34
--------------------------
