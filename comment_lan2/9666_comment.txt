451860
9666
Cuốn sách cho ta thấy những bất cập của xã hội mà mỗi ngày ta phải trải qua để trưởng thành hơn . Tuy nhiên mình thấy cuốn sách dành cho 17 tuổi trở xuống vì đó là giai đoạn dễ nảy sinh những hiểu lầm và rắc rối khó giải quyết . Cuốn sách có thể dành cho phụ huynh có con trong độ tuổi nhí nhảnh ấy để hiểu và chia sẻ với con hơn . Những câu chuyện nhỏ trong cuốn sách trên sẽ giúp bạn có được cuộc sống vui tươi và dễ dàng hơn. Đọc để cảm nhận nó .
4
651562
2016-06-20 10:16:08
--------------------------
192780
9666
Mình đã ấn tượng sâu sắc chú Richard nhất là Tất cả chỉ là những cuyện nhỏ bởi chú ấy viết không dài dòng,rất ngắn gọn nhưng lại bao hàm rộng lại có chút hài hước nữa chứ.Trong cuốn sách này cũng thế,chú  Richard đã chỉ cho tụi trẻ chúng mình nhiều thông điêp cuộc sống hay và ngẫm thấy đúng là tụi Teen chúng mình cũng có cái tình thổi phồng đóa hóa mọi chuyện lên.Hic nhạy cảm quá mà.Nhưng kệ đi,quan trọng là tụi mình có những người trưởng thành dẫn đường chỉ lối cho như chú  Richard nè.Nhưng thật xót xa,là chú đã rời cuộc sống khi tuổi nghề đang rất sung sức.Rất buồn.Nhưng mình tin rằng chú đã sống thực sự với tất cả nhiệt huyết của mình rồi.Tuổi trẻ tụi mình cần học tập nhiều lắm.Cám ơn chú  Richard đã đến với Đời và đem đến nhiều điều kì diệu cho Cuộc Sống này.
5
456366
2015-05-05 17:41:33
--------------------------
148280
9666
Có lẽ ai cũng biết, First New luôn làm những cuốn sách hay như Hạt Giống Tâm Hồn rất hay và có giá trị nhân đạo. Và cuốn sách này cũng thế. Nó dạy ta cách nhìn nhận mọi thứ xung quanh thật đơn giản, ko nên lo lắng. Dạy chúng ta cách kiềm chế cảm xúc nóng giận của chính mình, cũng như những hành xử ko đúng mà ta thường mắc phải...
Đối với tôi, cuốn sách này còn thiếu 1 chút gì đó nên chỉ cho 4/5sao. Nhưng hỡi các bạn trẻ, những bạn thanh thiếu niên, nếu các bạn vẫn đang gặp rắc rối, hay lo lắng, lạc lõng ko biết mình sẽ như thế nào thì tôi khuyên bạn : Đây là 1 cuốn sách hay bạn cần đọc đấy :)
4
520065
2015-01-10 12:53:56
--------------------------
