238254
8543
Quyển sách này giảng về phật pháp nhiều hơn, tác giả kể những câu chuyện trong kinh phật để giáo dục con người. Cụ thể như con người muốn sống hạnh phúc thì hãy biết đủ, biết tiết chế lòng tham như người dân nước Bhutan, biết sống hòa hợp với thiên nhiên. Ngoài ra quan điểm về chữ nhẫn mà tác giả đưa ra trong sách cũng rất hay, nhẫn vì yêu thương nhau, vì thương mà nhẫn thì không gọi là nhục. Đó là lời khuyên chí lý cho các cặp vợ chồng trẻ hôm nay. Theo tôi, quyển này mang tính triết lý sâu xa hơn những quyển mà tôi đã đọc trước là Chuyện Trò và Nhật Ký Sen Trắng. 
4
277587
2015-07-23 07:53:30
--------------------------
