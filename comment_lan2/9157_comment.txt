465619
9157
So với kho tàng tục ngữ, ca dao, dân ca Việt Nam thì cuốn sách này khá ngắn, nhưng với cái nhìn sắc sảo & theo mình cảm thấy là cả cái tâm, tình yêu chân thành đối với một giá trị văn hóa Việt Nam, tác giả đã vẽ nên bức tranh tổng quan về vẻ đẹp của kho báu văn hóa dân gian, cái chất thơ, sự tinh tế dù hết sức mộc mạc của thứ "văn chương hạng hai" này.

Điểm mà mình mến hơn cả là tác giả đã kì công tìm hiểu ý nghĩa của nhiều câu ca dao, tục ngữ tưởng chừng như chẳng có ý vị sâu xa gì, giúp cho người đọc "cảm" được cái tư duy bình dân sắc sảo, duyên dáng mang đậm tính hình tượng và màu sắc minh triết Việt Nam.

Rất mong tác giả trong tương lai sẽ có 1 công trình nghiên cứu văn học dân gian với phạm vi rộng hơn nữa.
5
369572
2016-07-01 11:00:02
--------------------------
370990
9157
Nhìn cái tên VĂN HỌC DÂN GIAN CÁI HAY VẺ ĐẸP là tớ đã muốn ngốn nó luôn rồi . Cuốn sách đề cập, phân tích những cái hay cái đẹp của văn học dân gian qua những lời bình của tác giả và đặc biệt còn có những phát hiện của tác giả về thứ'' văn chương hạng hai'' này. Đọc cuốn sách này không chỉ cung cấp cho ta tri thức mà còn giúp ta về lĩnh vực cảm thụ văn học rồi từ đó có những cách nhìn nhận mới mẻ của riêng mình, vận dụng vào các bài làm văn ,...
5
482252
2016-01-18 19:52:27
--------------------------
358022
9157
Có thể nói rằng văn học dân gian như một cỗ xe ngựa chất chứa bao tình cảm của người bình dân. Tất cả vô cùng giản dị, chân chất, nhẹ nhàng nhưng khuất sau ngôn từ là biết bao nhiêu nét đẹp tâm hồn của nhân dân ta. Quyển sách tuy không quá dài nhưng đủ để truyền tải phần nào cái hay, cái tinh túy của một thời kì văn học. Cá nhân tôi, "Văn học dân gian, cái hay, cái đẹp" đã giúp tôi hiểu sâu sắc hơn về văn học dân tộc. Ngoài các bài giảng về văn học dân gian học ở trên lớp, tôi tìm về với quyển sách cùng với bao niềm thích thú. Cảm ơn Lê Dân đã viết nên một quyển sách rất ý nghĩa đối với tôi nói riêng và có lẽ là mọi người nói chung.
5
825484
2015-12-24 16:46:13
--------------------------
303440
9157
Văn học dân gian vô cùng gần gũi với người Việt Nam. Ai cũng có thể đọc ca dao nhưng có mấy ai chịu khó suy ngẫm về những gì được đọc, được nghe. Cuốn sách này tác giả đã sử dụng nhiều kiến thức ngữ dụng học cũng như những bài viết đều đã được đăng báo nên mình thấy độ tin cậy cao. Giọng văn sắc sảo nhưng không kém phần hóm hỉnh khiến cho mình thấy rất thú vị không bị nhàm chán. Nhưng cuốn sách mà tiki giao cho mình có vẻ cũ góc sách không còn mới nhìn không có góc cạnh . Nhìn cuốn sách như bị ẩm, nhiều chỗ bị vàng.
4
810501
2015-09-15 20:56:39
--------------------------
275075
9157
Vốn dĩ, ngôn ngữ dân gian xuất phát từ cuộc sống lao động bình dị của nhân dân, nên bản thân mang một nét rất mộc mạc và gần gũi. Cuốn sách đã làm sáng tỏ hơn vẻ đẹp, vị ngon của dòng sữa ngọt lành nuôi dưỡng tâm hồn dân tộc ta suốt bao thế hệ nay dưới cái nhìn của một thế hệ hiện đại với đầy đủ kiến thức về ngôn ngữ học cũng như tình cảm cho loại ngôn ngữ này. Cuốn sách là tài liệu rất tốt cho những ai muốn tìm hiểu thêm về tình cảm, suy nghĩ của nhân dân ta. Lúc mua cuốn sách này, tôi rất thích, vì mặc dù tôi không yêu cầu bao sách nhưng cuốn sách này lại được bao rất cẩn thận khi giao.
5
366416
2015-08-22 16:40:42
--------------------------
132732
9157
Gần gũi, mộc mạc là những từ ngữ mà các nhà văn dành tặng cho pho tàng văn học dân gian Việt Nam. Đọc cuốn sách này mới thấy ngôn từ đất nước mình thật đa dạng, phong phú cả lối chơi chữ cũng như phong cách nghệ thuật thật là rất tinh tế và ngọt ngào bởi các vần điệu đều ăn khớp với nhau thậm chí đối nhau từng chút một. 
Nhưng có lẽ... văn học dân gian của chúng ta đang ngày một mất đi??? Nhớ lại hồi bé, cái thời của mình, đi học về là hay được mấy bác hàng xóm "khảo" mấy câu ca dao tục ngữ để được nhận vài cái bánh, mấy cây kẹo! Còn bây giờ, mấy đứa trẻ hiện nay lại ôm game, mạng và đôi khi không thuộc nổi mấy câu ca dao tục ngữ ngày xưa... 
Và cuốn sách này xuất hiện như mang "văn học dân gian" lại gần hơn với mọi người, gợi nhớ với những thế hệ mà văn học dân gian như nguồn vui và cung cấp kiến thức, ngôn từ cho các thế hệ mà văn học dân gian nay đã trở thành "xa lạ". Đây thực sự là một cuốn sách hay và có giá trị.
5
34983
2014-11-02 09:42:22
--------------------------
70785
9157
Văn học dân gian của mỗi một quốc gia giống như một kho báu được truyền lại cho những thế hệ sau. Đó có thể là những bài thơ, bài vè, những câu đối, những bài đồng dao..., dù là thể loại gì đi nữa cũng đều chứa đựng trong đó những tinh hóa của văn hóa, thể hiện được suy nghĩ, ước mơ và khát vọng của con người. Chính vì những giá trị tuyệt vời ấy mà văn học dân gian luôn có một sức sống mãnh liệt với thời gian. Ở Việt Nam, chúng ta đã quá quen thuộc với những tác phẩm được những "người nghệ sỹ dân gian" sáng tạo nên, tất cả đều thật gần gũi, mộc mạc và giản dị, đôi khi mang cả chất dí dỏm, hài hước. Nhưng không phải ai cũng có thể hiểu hết giá trị của những tác phẩm dân gian ấy. Một cuốn sách với nhiều phân tích sắc sảo như thế này sẽ là một tư liệu bổ ích cho những bạn đọc yêu văn học dân gian nước nhà.
5
109067
2013-04-23 12:31:31
--------------------------
