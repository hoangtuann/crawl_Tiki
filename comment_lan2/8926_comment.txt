467452
8926
Mình rất yêu quý loài chó và đây quả là một câu chuyện tuyệt vời về 1 chú chó thông minh, trung thành và những con người nhân hậu.
Có lẽ cuộc đời đã không ưu ái cho Bim có được hạnh phúc bên người chủ đầu tiên của đời mình. Bim đã phải trải qua biết bao mất mát, buồn tủi, đau đớn hay những nỗi cô đơn giằng xé trong suốt cuộc hành trình đi tìm người chủ của mình. Mỗi dấu chân Bim đi qua đều đọng lại trong lòng tôi nỗi đau đầy cảm thông. Cứ dõi theo Bim, cu cậu đã gặp đủ loại người trong đời mình, những con người nhân hậu, tốt bụng với Bim hay những kẻ xấu xa, đối xử tệ bạc. Thật buồn khi đến giây phút cuối cùng, Bim không thể trụ lại được để đợi Ivan trở về với mình.
Tôi muốn gửi đến tất cả mọi người một điều rằng hãy đối tốt với chú chó của mình bởi chúng ta là gia đình duy nhất của chúng. Chúng sẽ không bao giờ rời bỏ ta vì bất cứ lý do gì. Vì thế hãy là một người chủ chân thành!
5
298852
2016-07-03 21:40:42
--------------------------
446696
8926
Tôi rất thích bìa sách nhưng ngoài đời không đẹp như thế. NXB in ẩu, nội dung thì hay đấy nhưng Tiki làm nhăn bìa, lại quá cũ nên ít tâm trạng để đọc. Cho 3 sao là hợp rồi nhé, thất vọng Tiki nhiều lần rồi! Sao cứ đến cái của tôi bìa lại xấu và dơ đến thế nhỉ?!
Đáng tiếc quá...Nội dung tuyệt cơ mà, nhấn mạnh với Tiki về bìa sách nhé: quá cũ và quá nhăn, còn phần NXB thì thôi cạn lời, in ẩu và sai nhiều lỗi chính tả. Lần sau tôi sẽ cân nhắc khi mua hàng tại Tiki
Xin lỗi!!!
3
1277403
2016-06-12 18:43:12
--------------------------
444166
8926
Chưa bao giờ có cuốn sách nào làm trái tim tôi cảm thấy rung động, thương xót và rơi nước mắt nhiều đến như vậy, có lẽ vì tình yêu đối với loài chó trong tôi quá lớn. Không biết từ khi nào mà tôi đã xem loài động vật 4 chân này như là 1 người bạn của mình và sau khi đọc tác phẩm này xong, tôi lại càng thương chó hơn bao giờ hết. Cũng may đây chỉ là truyện do tác phẩm viết ra chứ nếu ngoài đời mà Bim có thật thì chắc tôi sẽ còn thấy xót xa hơn nữa.
Tôi thích các tác giả miêu tả Bim từ khi sinh ra, từ khi còn bé vừa được sinh ra trên đời, tâm hồn khi đó ngây thơ và nghịch phá như 1 đứa trẻ. Cho đến khi nhận thức được người chủ phải chia tay mình, Bim như 1 đứa bé đã lớn bắt đầu hành trình đi vào xã hội với những va vấp trong cuộc đời, một cuộc hành trình gian nan và đau khổ nhưng chưa bao giờ dập tắt hi vọng tìm lại được chủ của mình. Lòng trung thành đáng giá như vậy biết tìm đâu ra giữa cuộc đời này? Nhiều khi đọc truyện mà nhói, mà xót thương cho Bim, cảm giác như bị một vết dao đang đâm vào tim vậy. Thật buồn khi cuối cùng số phận vẫn không mỉm cười với Bim. Có lẽ cũng là cái sối của nó.. 
Ngay cả khi viết những dòng nhật xét này tôi cũng đang rơi nước mắt...
Điểm trừ duy nhất là chất lượng sách chỉ ở mức trung bình, bìa sách quá mềm vừa nhận được là thấy bị gãy vài đường nhỏ nhỏ rồi. Nhưng vì nội dung tác phẩm quá tuyệt nên sẽ cho 5 sao luôn.
5
813663
2016-06-07 23:08:54
--------------------------
435099
8926
Khóc... khóc rất nhiều... Đó chính là cảm xúc của tôi khi đọc quyển sách này. Lời văn của Troieponxki giản dị nhưng vô cùng tinh tế và sâu sắc. Đó chính là cái tôi thích nhất trong cuốn sách này. Nhưng sách có một điểm trừ rất lớn. Như mấy bạn đã mua bên trên cũng đã phản ánh, sách được NXB in rất ẩu, các trang đều bị in sót chữ và mình thì thực sự không thích điều đó chút nào hết. Cứ tưởng những bạn mua bên trên đã phản ánh về lỗi này thì ở bên phía NXB sẽ sửa chữa cho những bản in lần sau, vậy mà những bản in lần sau vẫn y nguyên như vậy và hình như có phần tệ hơn. Về phần nội dung cuốn sách thì không có gì để chê, mà NXB lại cẩu thả thế này, thực sự mình rất thất vọng.
3
945772
2016-05-24 14:02:48
--------------------------
427188
8926
Ban đầu, khi nhận được sáh từ tiki tôi khá thất vọng vì bìa sách bẩn, nhàu. Tôi phải xé lớp giấy kiếng ra cho sạch sẽ hơn. Chất lượng giấy không tốt, tuy sách ít trang nhưng khá nặng. Nên tôi sẽ trừ 1 sao.
Về  nội dung, nước mắt tôi đã rơi khi đọc tác phẩm. Quá xúc động! Bởi lẽ tôi khá yêu chó. Chú Bim trong truyện trung thành, bất hạnh và giàu tình yêu thương. Chú chết khi chưa gặp lại chủ sau bao lâu bôn ba... đó là điều làm tôi đau xót!
Cuốn sách sẽ làm ta có suy nghĩ khác về loài chó và đối xử với chúng tố hơn. Chó là người bạn thân của loài người!
4
1131432
2016-05-08 16:13:25
--------------------------
419634
8926
Nội dung của Con Bim trắng tai đen thì không có gì phải bàn - đúng như các bạn khác đã review, đây là một trong những tác phẩm cảm động nhất về người bạn bốn chân trung thành của chúng ta.

Review này mình chỉ muốn phản ánh về hình thức và các lỗi in ấn của sách.
- Thứ nhất, bìa sách minh họa không đúng: hình ảnh chú chó ngoài bìa không hề có nét gì của nhân vật chính - con chó Bim giống Setter với bộ lông trắng và đôi tai đen.
- Thứ hai, ấn phẩm này đầy những lỗi chính tả và sót chữ khiến cho mỗi lần đọc cảm giác rất khó chịu, cứ như ăn cơm mà nhai phải sạn.
Với một tác phẩm văn học kinh điển như vậy mà NXB lại in ấn rất cẩu thả là một điều đáng lên án.
2
772211
2016-04-21 17:11:33
--------------------------
405894
8926
Một cuốn sách hay, một câu truyện cảm động về một chú chó khôn ngoan, đáng yêu, trung thành. Đọc truyện mà trái tim mình như thắt lại. Mình đọc và đã khóc rất nhiều, nhất là đoạn Bim bị bỏ lại trong rừng đêm mưa gió. có lẽ sẽ mình không dám đọc thêm một lần nào nữa nữa. Chú chó Bim rất tội nghiệp, chia tay chủ khiến cuộc sống của nó hoàn toàn đảo lộn, nó cứ chờ và chờ mãi, tìm và tìm mãi cho tới khi được gặp lại người chủ thân thương của mình
5
1089565
2016-03-27 13:33:55
--------------------------
403484
8926
Khi đọc được một nửa cuốn sách này, tôi đã bỏ dở và không đọc nữa, tôi không chịu nổi những nỗi nhớ khắc khoải của Bim, cũng không thể chịu nổi cái lạnh buốt của những người đối xử với Bim không chút tình thương. Phải rất lâu sau, lâu lắm, tôi mới cầm cuốn sách lên đọc lại, vừa đọc vừa day dứt đau lòng, vừa đọc vừa thương Bim đến chảy nước mắt. Tác giả có lẽ yêu động vật lắm, yêu đến mức nào mới có thể viết nên câu chuyện về một chú chó mà làm lay động trái tim hàng triệu người, hơn xa những câu chuyện về con người mà nhạt nhẽo không cảm xúc. Tôi lường trước được cái kết, nhưng dù cái kết có đau lòng đi nữa, xuyên suốt quyển tuyện này, tất cả các loại cảm xúc hòa quyện lại đã đủ để bù cho sự đau lòng ấy rồi, nhỉ?
5
968032
2016-03-23 21:16:48
--------------------------
369053
8926
Câu truyện khiến ta thực sự cảm nhận được ra mỗi người luôn có góc cạnh riêng chứa đựng thấm đẫm nỗi khổ , sự thất bại của lối thoát dẫn loài người đến sự diệt vong , chẳng hứa hẹn những điều tốt đẹp , sáng lạn ở phía trước mà chỉ cố nhoài người vô vọng , chú chó nhỏ có chân thành với xã hội chủ nghĩa của nó , phải là một chú chó có tình nghĩa đáng mến mà ta thấy phục chúng ở mọi lứa tuổi , Bim không còn mềm yếu như lúc bé .
4
402468
2016-01-14 23:48:51
--------------------------
355043
8926
Những tác phẩm văn học viết về loài chó luôn có sức cuốn hút mạnh với tôi, nhưng đây là tác phẩm mà tôi thích nhất. Tôi sẽ không bao giờ quên được sự trung thành, tình yêu của của Bim dành cho chủ và những người bạn của mình. Tiêu thuyết cho mọ người thấy rằng loài vật cũng có trái tim, tình thương và niềm tin.
Cao trào câu chuyện theo tôi được đây lên từ cú đã của tên thợ săn, từ lúc đó có những khi tôi cảm giác cổ họng mình nghẹn lại, trong lòng dậy nên bao nỗi thương cảm xen lẫn với sự căm giận kẻ ác. 
Kết thúc câu chuyện quả là rất buồn. Tưởng chừng như sau khi mở cánh của xe bắt chó là người chủ sẽ thấy được chú chó Bim của mình vẫn còn đó và nhận ra mình nhưng chú đã ra đi mà nguyên nhân của cái chết của Bim chắc chắn là mụ hàng xóm "người phụ nữ Xô viết" kẻ tôi căm ghét sâu sắc, kẻ ăn không ngồi rồi, lắm chuyện, luôn suy nghĩ những thủ đoạn hại người, ngay cả một chú chó cũng mang lòng thù hằn dai dẳng và rắp tâm làm hại.
Tuy nhiên, cuốn sách hay về nội dung nhưng về mặt xuất bản thì vẫn có nhiều lỗi đánh máy, hi vọng nxb chỉnh sửa kĩ hơn trong những lần xuất bản sau
4
566706
2015-12-19 06:56:23
--------------------------
340062
8926
Đọc về Bim tôi mới hiểu thêm được về loài chó, một loài vật quá đỗi thông minh và trung thành. Chú chó thông minh, đáng yêu, giàu cảm xúc đã đem lại cho người đọc những cảm giác từ khâm phục, yêu thương đến xót thương chú chó tội nghiệp. Là người mẹ, tôi đã đọc tác phẩm này, để truyền lại cho con thêm sự hiểu biết về loài chó nói riêng và động vật nói chung. Cho dù con chưa biết chữ, nhưng lúc nào cũng bảo "mẹ ơi mẹ kể con Bim cho con đi". và chắc chắn một điều rằng, đọc xong cuốn này sẽ chẳng ai còn ăn thịt chó nữa 
5
845629
2015-11-19 09:18:58
--------------------------
336911
8926
Con Bim trắng tai đen cũng giống như chú chó nhỏ Hachiko trung thành ở tỉnh Akita, Nhật Bản để lại cho tôi nhiều ấn tượng tốt đẹp. 
Đọc đến đoạn Bim lang thang giữa mùa đông ngập tuyết tôi đã không cầm nổi nước mắt, xót thương, yêu quý và cả khâm phục chú chó nhỏ trung thành đáng yêu này. Tôi nghĩ đến cảnh người chủ Ivan Ivanưts khi biết được những ngày Bim cùng cực đi tìm mình thì sẽ thương xót đến như thế nào.
Câu chuyện không chỉ đơn thuần để viết về chú chỏ nhỏ Bim trung thành mà là cách để làm cho tình yêu thương được nhân rộng và lan mãi.
Đọc câu chuyện này lại nhớ về "Sao không về Vàng ơi" của Trần Đăng Khoa, rồi "Tiếng gọi nơi hoang dã"... 
Cảm ơn ngòi bút tuyệt vời Trôiepônxki, cám ơn dịch giả đã chuyển ngữ một cách tinh tế và sống động để cho tình yêu dành cho Bim của độc giả càng thêm sâu sắc.
4
474214
2015-11-13 09:24:21
--------------------------
309717
8926
Trôiepônxki đã làm sống lại trong tôi những ký ức về ngày thơ ấu. Chú chó giàu tình nghĩa đã giúp cho tác phẩm trở nên nổi tiếng và có sức tác động mạnh mẽ không chỉ đối với những người có nuôi thú cưng. Tuy đề tài này cũng xuất hiện nhiều ở các tác phẩm khác nhưng không vì đó mà nó không có sức lôi cuốn, nét độc đáo riêng.
Tôi đã khuyến khích cháu mình đọc quyển sách này ngay và bé cũng rất thích nó, kết thúc tuy buồn nhưng đã để lại một ấn tượng mạnh mẽ với tôi.
4
623214
2015-09-19 08:56:13
--------------------------
284940
8926
Cuốn sách mang lại những cảm xúc rất trong sáng cho tuổi thiếu niên, tuy rằng kết cục rất buồn. tuy nhiên, có lẽ chính vì nỗi buồn ấy mà cảm xúc còn đọng lại rất lâu trong ký ức của tôi, nhớ về một thời đi học và đọc bất cứ cuốn gì có được. Nếu bạn chưa đọc, hãy dành một chút thời gian để cảm nhận. Nếu con của bạn chưa đọc, thì đừng sợ chúng buồn khi đọc truyện này. Vì có những nỗi buồn sâu lắng khiến người ta suy nghĩ chín chắn hơn. Và cũng là một cơ hội tốt, để cha mẹ và con chia sẻ cảm nhận sau khi đọc. Một tác phẩm văn học bổ ích, không chỉ dành cho tuổi thiếu niên.
5
335251
2015-08-31 14:03:56
--------------------------
219733
8926
G.Trôiepônxki thật sự là một nhà văn tài năng, bằng nghệ thuật xây dựng và miêu tả diễn biến tâm lí nhân vật đặc sắc ông đã dựng nên hình ảnh một chú chó Bim thông minh, dễ thương, đáng yêu, trung thành,... và tình cảm. Chú chó này như tái hiện hình ảnh một con người Liên Xô khi đứng giữa bao bất công, trái ngang, ghét bỏ của xã hội, có lúc hạnh phúc trong tình yêu thương có lúc lại cô độc trong sự hờ hững của xã hội. Tuy nhiên có ở một khu phố nhộn nhịp, đông vui hay ở giữa rừng thu đại ngàn thì Bim vẫn là tri kỉ, là một phần trong trái tim Ivan Ivanut nói riêng và trong tim G.Trôiepônxki cùng bạn đọc mọi thế hệ nói chung...tiếng Bim luôn vang vọng đâu đây.
Đây mãi là tác phẩm đặc sắc và là một trong số các tác phẩm hay nhất mọi thời đại.
4
672733
2015-07-01 21:26:52
--------------------------
202294
8926
Khi đọc đến những dòng cuối cùng của cuốn sách, nước mắt tôi đã thấm ướt một góc giấy. Nỗi đau khổ, tổn thương nặng nề mà Bim - một chú chó quá đỗi trung thành phải chịu đựng đã tố cáo chính sự ác độc, vô tâm, giả dối của con người trong xã hội. Cái chết của Bim chính là vết dao sắc lạnh đâm mạnh vào trái tim những con người yêu quý, trân trọng động vật như tôi. Tôi căm phẫn nhiều mà đau đớn, chua xót còn nhiều hơn.
Cuốn sách ấy, tôi không thể đọc lại lần hai, bởi lẽ, tôi biết tôi sẽ phải khóc thêm lần nữa, khi đồng hành cùng Bim trên con đường đời đầy bấp bênh, đau khổ ấy.
5
55506
2015-05-29 10:27:18
--------------------------
201794
8926
Đây là một cuốn sách rất ý nghĩa và gây cho tôi nhiều xúc cảm nhất trừ trước tới nay. Tất cả cảm xúc đối với tôi được diễn tả bằng hai chữ "tuyệt vời". Nếu ai đó có can đảm chấp nhận nỗi buồn, thì hãy thử đọc để khóc, để cảm thông, thấu hiểu, để nhìn nhận lại mọi sự xung quanh ta. Từ cách ta đối xử với mọi vật, mọi việc xem nó đã thực sự đúng đắn.
Chú chó Bim đã khiến thôi nhận ra rằng mình phải yêu mến những con vật xung quanh tôi, cảm phục sự trung thành tuyệt đối của loài vật có nghĩa.
Hãy thử đọc và cảm nhận.
5
614265
2015-05-28 11:31:11
--------------------------
164310
8926
Một tác phẩm tuyệt vời ! Một trong những cuốn sách kinh điển viết về động vật : Ngựa ô yêu dấu, Nanh trắng... và tất nhiên là Con Bim trắng tai đen.
Câu chuyện là một bài ca về chú chó Bim thông minh, tình nghĩa. Chú biết yêu, biết căm ghét, biết nghĩa tình. Một chú chó có nghĩa, trung thành như thế, yêu chủ tha thiết, vượt qua bao nhiêu hiểm nguy vất vả để tìm chủ. Nhưng cuối cùng, chú chết, chết dưới sự tê cứng của băng tuyết. 
Mình tin rằng không một ai có thể không khóc khi đọc hết cuốn sách này. Thông điệp của tác phẩm: Hãy yêu thương, hãy trân trọng, hãy vị tha. Đó là những đức tính cao đẹp trong cuộc sống.
5
540359
2015-03-07 13:56:12
--------------------------
160137
8926
Trước giờ tôi là một người chỉ yêu thích mèo. Tôi chưa từng nuôi chú chó nào nhưng một sự tình cờ đã đưa tôi gặp “Bim”, một chú chó rất tuyệt vời. Khi đã hiểu về cuộc đời và tính cách của nó tôi thấy rất thương nó và từ bây giờ tôi cũng yêu các chú chó như những chú mèo cưng của mình. Và ngày hôm nay cho phép tôi giới thiệu chú Bim tuyệt vời này với các bạn. 
Truyện đã ngợi ca tình bạn sâu sắc, lòng chung thủy của một chú chó đối với chủ mình, trong lòng nó hình ảnh của chủ không bao giờ phai nhạt cho đến khi nó chẳng còn hơi thở. Chú chó Bim như một con người thật sự biết yêu thương người tốt, biết ghét kẻ xấu, nó luôn tin vào những con người tốt bụng đã cứu giúp nó, trái tim nó cũng rất nhạy cảm, biết nhớ nhung, đau xót, hạnh phúc. Chưa bao giờ thế giới loài vật lại gần chúng ta như thế bởi tình thương, lòng bác ái chúng ta sẽ gần với những con vật bé nhỏ bên mình hơn.

Tác phẩm như một lời kêu gọi tình thương cho những loài động vật như Bim – đại diện cho những người bạn thân thiết, luôn trung thành với con người. Có lẽ vì thế mà đã có rất nhiều chú chó không gia đình đã được mang về nuôi và biết bao chú chó được mang tên Bim. Ở góc độ nào đó, có thể tác phẩm cũng lên án những con người tàn nhẫn với những con vật trung thành vô tội, làm giàu bằng sự sống của những con vật là người bạn thân của con người. Ngoài ra với nghệ thuật tả cảnh điêu luyện tác giả sẽ đưa chúng ta đến với bức tranh thiên nhiên tuyệt đẹp với cả bốn mùa.
5
487348
2015-02-22 14:06:31
--------------------------
123556
8926
Có lẽ đây là quyển sách đầu tiên mà mình không thể đọc hết vì sợ những gì tiếp tục xảy ra  với chú chó ấy.Con Bim Trắng Tai  Đen mang lại một cảm xúc mạnh mẽ nơi người đọc, họ không chỉ chú tâm vào từng trang sách mà còn liên tưởng đến cách cư xử của mình hằng ngày. Bim, một người bạn trung thành tận tụy với chủ nhưng lại có một kết thúc buồn với cái chết lạnh lẽo trong chiếc thùng sắt tây. Câu chuyện là một bài học không chỉ cho những con người với những tính cách tiêu biểu trong truyện mà còn cho chính bản thân mỗi chúng ta, biết quan tâm đến mọi vật kể cả những điều nhỏ nhặt nhất
4
282915
2014-09-02 14:34:07
--------------------------
102408
8926
Mình cực thích những tác phẩm về loài chó, Nanh Trắng, Tiếng gọi nơi hoang dã,.. Mỗi câu chuyện đều có nét cuốn hút riêng, với Con Bim trắng tai đen, mình đã suýt khóc khi đọc dòng giới thiệu sau sách, chính là câu "Băng tuyết rơi trên cánh mũi Bim không thể tan ra vì hơi ấm cơ thể không còn." Một tác phẩm rất rất đáng đọc :)
Đọc đi đọc lại không dưới 10 lần nhưng Bim luôn để lại cho mình cảm giác xót thương và khâm phục. Từ cuộc sống hạnh phúc của 2thầy trò với những trò quậy phá của một nhóc cún con, Bim lớn lên thành con chó săn thông minh, chỉ vì sự cố mà chia tay chủ. Một câu "tìm đi" mà Bim đã đi tìm, đi mãi, phải trải qua bao nhiêu đau đớn, khổ cực, đến khi chủ tìm được thì Bim đã đi rồi.
Tình cảm, lòng trung thành của Bim - cũng như bao nhiêu con chó khác - có lẽ còn trong sáng, thuần khiết & đẹp hơn thứ tình cảm giả dối mà ngày ngày nhiều người vẫn trao cho nhau ngoài kia.
5
33645
2013-12-18 14:55:04
--------------------------
83709
8926
Nếu ai đã từng xem phim Hachiko,từng khóc và đau lòng vì chú chó có lòng trung thành tuyệt đối thì ắt hẳn sẽ không thể bỏ qua "Con Bim trắng tai đen",nó chấp nhận mọi đau khổ,mọi gian nan chỉ mong được tìm thấy chủ của mình,ở Bim có lòng trung thành,có tình yêu dành cho chủ vô cùng to lớn-nó đã chứng minh cho con người rằng:loài động vật cũng có trái tim,có lí trí,có tình cảm và cũng có lẽ tình cảm mà nó dành cho chủ của mình còn lớn lao hơn nhiều so với tình cảm giữa người với người trong cuộc sống.Đọc để cùng cảm nhận,để cùng thấu hiểu với tình cảm của chú chó trung thành đến tuyệt đối này.
5
60572
2013-06-27 15:35:32
--------------------------
