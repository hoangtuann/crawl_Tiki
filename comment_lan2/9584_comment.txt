491831
9584
Mình mua quyển sách này thích nhất là vì có CD. Sách Tiki gửi nhanh, sách mới, chất giấy khá dày cầm thích thích :))) Khổ giấy tương đối lớn trong khi sách tương đối mỏng, không tiện mang đi nhưng rất tiện mở ra, không bị hỏng gáy :3
Sách hợp với những bạn mới làm quen với toeic, còn nếu bạn sắp thi thì tốt nhất cứ tới trung tâm thì hơn :3
4
513917
2016-11-21 12:49:10
--------------------------
457675
9584
Mình ôn thi TOEIC bằng quyển này đầu tiên. Sách chia dạng hướng dẫn và có bài tập từng phần. Nếu như mới ôn thì quyển này rất phù hợp vì bài tập khá dễ, dành cho những người mới bắt đầu hay ngữ pháp không vững. Cũng như làm quen dạng đề thi chứ không nhiều câu khó, tạo hứng thú học chứ không nhanh nản. Học trên sách cũng thoải mái hơn trên máy tính, tốt cho mắt, tạo động lực chăm chỉ làm hơn. Đối với mình là thế, mới đầu nên làm kiểu dễ dễ như này. Đề cuối sách cũng dạng dễ, cộng điểm lại khá cao, tạo hứng thôi không nên chủ quan nhé.
4
492988
2016-06-24 14:38:36
--------------------------
447660
9584
Cuốn Starter TOEIC này được biên soạn cho những người muốn làm quen với kì thi TOEIC, phần đầu là các chủ điểm ngữ pháp quan trọng, sau đó là các bài test giúp người đọc luyện tập các kĩ năng, cuối cùng còn có 1 bài test mô phỏng 1 đề thi TOEIC thật. Hình thức quyển sách rất tốt, trình bày khoa học, dễ tạo hứng thú cho người đọc, đĩa nghe tốt. Quyển sách này cùng với cuốn Very easy TOEIC là 2 quyển sách tốt giúp ta có những kĩ năng cơ bản nhất về kì thi TOEIC, từ đó dễ dàng học lên các level cao hơn!
5
917875
2016-06-14 17:16:59
--------------------------
419987
9584
Quyển này em dành mua cùng cuốn very easy toeic. Sách này rất hay, phù hợp với những ai bắt đầu hoặc chuẩn bị cho kì thi TOEIC. Khi nhận được sách em vui lắm, nó được bọc ngoài bằng nilon như quyển very easy toeic, khá dày, chất lượng giấy đảm bảo, nội dung cũng rất phong phú, nó đi kèm với 3 cd .Tuy nhiên cũng hơi phiền vì có 3 cd thì lại hơi nhiều, không tiện cho lắm. Nói chung là sách rất hay, mấy đứa bạn em cũng rất thích sách này . Tiki giao hàng nhanh , đóng gói chắc chắn  . Rất hài lòng với Tiki, cám ơn Tiki nhìu.
5
1284706
2016-04-22 12:54:18
--------------------------
397240
9584
mình mua sách này để học theo chương trình toeic của trung tâm, đặc biết thấy thích thú khi học với quyển sách này, nó gồm cả phần giải thích ngữ pháp cộng với bài tập. Có 1 điều duy nhất mình không vừa ý, đó là phần giải thích đều tiếng anh có 1 số chỗ rất khó hiểu, cần có người giải thích. Nói chung là rất tốt đối với người mất gốc tiếng anh như mình, mình đã mua sách này kèm với quyển very easy toeic, khi mua được kèm đĩa nhưng mình thấy không cần thiết lắm.
4
652998
2016-03-14 17:17:48
--------------------------
368199
9584
Nếu ai chưa biết về toeic thì cuốn sách này là cựa chọn hợp lý vì nó là sách dành cho người mới bắt đầu. Sách hướng dẫn về ngữ pháp chi tiết đơn giản, dễ hiểu. Ngữ pháp chặt chẽ, bài tập đa dạng dễ làm không khó áp dụng được ngữ pháp mình học. Sách giúp cho những ai không có thời gian và điệu kiện về kinh tế đi học tại trung tâm anh ngữ thì có thể tự học một mình. Chất lượng đĩa nghe ổn, rõ ràng, đúng chất giọng, sách khá là chất lượng. 
4
396128
2016-01-13 16:34:03
--------------------------
353453
9584
Mình được giới thiệu cuốn sách "starter TOEIC" này từ một người chị. Chị ấy ôn thi Toeic bằng cuốn này và một cuốn nữa, và chị ấy đi thi được 660 điểm. Mình có lướt qua nó rồi và thấy nó khá bổ ích, gồm các bài luyện thi được chi theo phần cụ thể, ôm sát cấu trúc đề thi. Bên cạnh đó cuốn sách này còn bao gồm các mẹo khi làm bài. Nói chung cuốn này khá ổn, bây giờ mình bắt đầu luyện thi và hy vọng sẽ đạt được kết quả như mong muốn. :) 
4
513404
2015-12-16 10:37:05
--------------------------
352075
9584
Thật sự lúc trước khi mình giải đề trong quyển này mình chẳng hình dung ra được Toeic là như thế nào nữa. Quyển sách này tổng hợp nhiều unit , mỗi bài chia theo cấu trúc đề thi thật, kèm CD nghe, đáp án phía sau cụ thể, chi tiết thích hợp cho những ai tự luyện thi ở nhà giống mình. Đây là một trong những quyển cơ bản nhất để làm quen với toeic. Sách hay thích hợp để tự luyện ở nhà.Những bạn tiếng anh đã tốt rồi thì nên mua những quyển lever cao hơn. 
5
43060
2015-12-13 16:18:09
--------------------------
339230
9584
Giấy rất đẹp và mịn. Quyển sách này rất phù hợp cho những bạn nào bắt đầu học Toeic. Nội dung không quá nặng. Tổng hợp đủ từ lí thuyết đến thực hành. Cấu trúc rất sát với các bài thi Toeic .  Sách  có kèm cả 3CD nữa để giúp bạn YGiấy rất đẹp và mịn. Quyển sách này rất phù hợp cho những bạn nào bắt đầu học Toeic. luyện nghe tốt hơn.. Mình mới mua quyển sách này hộ con bạn trên Tiki, vào đợt khuyến mại nên chỉ có 94k thôi vậy mà trước đó mình mua ở ngoài mất 134k lận. 
4
898981
2015-11-17 17:02:36
--------------------------
326399
9584
Sách hay, chia ra các dạng chủ đề ngữ pháp, cấu trúc, nhìn vào dễ học và hệ thống kiến thức.
Mỗi bài có phần đầu tiên là ôn lại kiến thức, sau đó có bài tập áp dụng. phần sau của cuốn sách là các bài test tương ứng với những ngữ pháp đã được học.
Sách này rất phù hợp với bạn nào đang bắt đầu học Toeic.
Nếu bạn nào muốn học toiec hoặc muốn nâng cao trình độ ngoại ngữ thì đây là quyển sách rất hữu ích. Sách sẽ rèn luyện kĩ năng ngữ pháp và kĩ năng nghe cho bạn.
Sách còn kèm theo 3 đĩa, dã vậy mua trên tiki còn được giảm giá nữa, khá thích.

4
285522
2015-10-25 18:29:24
--------------------------
324970
9584
Giới thiệu đôi chút hí hí, trường mình có dạy tiếng anh và đề nghị sinh viên dùng sách này. Ban đầu, thật sự là mình không thích cho lắm vì nghĩ là trường bắt ép sinh viên. Sau đó, lên tiki và biết có sách này và đã tậu cho mình và nhỏ bạn mỗi đứa 1 cuốn. Nhỏ bạn và mình đều hài lòng vì chất lượng trang sách cũng như giá thành, tại tụi mình là sinh viên mà. 
Hiện tại đã học xong sách và cảm nhận một điều: Phù hợp cho người mới bắt đầu tình yêu với Toeic.
5
95468
2015-10-22 14:37:39
--------------------------
322146
9584
Mình mới bắt đầu học Toeic, quyết định mua quyển sách này quả là không sai, kết hợp với những bài giảng trên youtube của Ms Hương và cd kèm theo sách, tuy mình chỉ mới học vài unit của sách thôi nhưng văn phạm và từ vựng của mình đã được củng cố khá nhiều, các bài nghe của sách và cd cũng khá rõ ràng và dễ nghe, mình đã mua trọn bộ giáo trình Toeic cho quá trình luyện thi của First News và hoàn toàn yên tâm về chất lượng khi đặt mua ở Tiki, mình đang luyện thi ở trung tâm GMToeic của cô Mai thầy Giảng, thầy cô cũng khuyên học viên mua bộ sách này nên các bạn có thể yên tâm về nội dung sách, ngoài ra thì bổ sung thêm bộ Economy cũng khá tốt đấy
5
864213
2015-10-15 17:53:47
--------------------------
320683
9584
Mình là sinh viên năm nhất, muốn học tiếng anh để chuẩn bị cho tương lai sau này. Vả lại trường mình cũng yêu cầu  mỗi sinh viên phải có toiec từ 550 trở lên mới được ra trường nên đây là cuốn sách mình lựa chọn để hoàn thành 4 năm đại học. Sách viết rất hay, in rõ, không bị mờ. Mình đã từng mua những quyển sách bị mờ, in lỗi, trang bị dính với nhau, hình không rõ nhưng quyền này rất đảm bảo. CD đọc rất rõ, lớn và không có lỗi. Nếu bạn nào muốn học toiec hoặc muốn nâng cao trình độ ngoại ngữ thì đây là quyển sách rất hữu ích. Sách sẽ rèn luyện kĩ năng ngữ pháp và kĩ năng nghe cho bạn. Nói tóm lại, đây là quyển sách nên mua, đã có khuyến mãi nên không gì phải ngại chuyện giá cả.
5
827274
2015-10-12 07:44:40
--------------------------
303263
9584
Mình từng ôn trung tâm ngoại ngữ, cô giáo giới thiệu cuốn này và mình cũng luyện theo cuốn này. Và bây giờ mình mua cho em gái mình một quyển mới để em ôn luyện.
Nói chung khi học với cô giáo có nhiều kinh nghiệm thì mình sẽ khai thác được tối đa tác dụng và hiệu quả mà quyển sách này mang lại. Nhưng nếu bạn tự học thì cũng không sao cả, cứ học theo lộ trình của cuốn sách và nắm bắt hết những gì mình đang học thì chắc chắn bạn sẽ thành công.
Tiki nên nhập thêm sản phẩm này mà không kèm CD nhe. Mình muốn mua thêm một quyển mà không cần lấy CD.
4
400247
2015-09-15 19:16:39
--------------------------
288544
9584
Năm trong bộ sách ôn thi Toeic, mình đã mua một cuốn tự học đó là Very Easy Toeic, thấy khá ổn nên mình tiếp tục cuốn tiếp theo này. Vì đã mua một lần cuốn sách kia nên khá tin tưởng về chất lượng sách. Nếu bạn đang muốn thi Toeic mà chưa biết hay chưa giỏi thì hãy mua ngay cho mình một cuốn này để bắt đầu học nhé, mình đảm bảo sẽ rất ok. Đĩa nghe rất rõ, giọng chuẩn, dể chuyển đuôi sang điện thoại để nghe cho tuyện hơn trong việc học! Mình đã và đạng học theo lộ trình của bộ sách này!
4
477886
2015-09-03 17:25:18
--------------------------
285995
9584
Mình cũng mới hạ quyết tâm ôn thi Toeic tham khảo nhiều ý kiến trên mạng thì tìm đến quyển sách này, mình thấy quyển sách này rất hay rất phù hợp với người mới bắt đầu ôn luyện cho ký thi Toeic, sách giúp mình ôn lại kiến thức ngữ pháp, các bài tập luyện nghe và đọc cũng vừa sức cho người mới bắt đầu, tạo một nến tảng kiến thức tốt cho kỳ thi, mỗi ngày học một bài và làm phần bài tập kèm theo giống như thực tập các ví dụ giúp bạn dể hiểu bài, nhớ và hiểu sâu hơn.
4
670130
2015-09-01 12:39:11
--------------------------
253856
9584
Mình là sinh viên, phải thi chứng chỉ toeic là điều bắt buộc không có nhiều thời gian và tiền bạc nên mình đã chọn cách tự luyện thi và mình bắt đầu với cuốn sách này, rất hay.  
Có cả phần giới thiệu và các mẹo, các bẫy khi làm bài trong kì thi toeic, chương đầu tiên là kiến thức ngữ pháp mà mình đã bỏ lâu rồi, giúp mình hệ thống và ôn lại kiến thức ngữ pháp, chương tiếp theo là các bài tập phần luyện nghe và đọc, cuối cùng là phần bài kiểm tra để luyện tập. Cách sắp xếp các phần học rất hợp lí mình học mà không cảm thấy mệt và chán. 
5
734495
2015-08-04 20:16:36
--------------------------
251462
9584
Cuốn sách vỡ lòng để luyện TOEIC của mình, bố cục sách rất chuẩn, đặc biệt là phần grammar hầu như bao quát tất cả các ngữ pháp cơ bản, thích hợp với những ai mất căn bản như mình. Phần listening bài nghe đa dạng nhưng thì theo mình hơi dễ, bù lại phần reading khá hay, trình bày tốt, có nhiều điểm ngữ pháp kèm theo. Phần mình thích nhất là Practice test, gồm nhiều bộ đề để tự luyện tập khá sát với đề thi thật. Sách có CD kèm theo, audio rất dễ nghe, nên thích hợp cho bạn nào mới làm quen TOEIC.
4
2330
2015-08-02 20:35:50
--------------------------
241938
9584
Hiện nay kì thi toeic có thể xem là kì thi chứng chỉ tiếng anh uy tín và cần thiết để có 1 công việc tốt hơn. Mình tin sách này rất hữu ích với những bạn đang muốn ôn thi toeic, mình nghĩ nếu hoàn thành xong sách này mà trọn vẹn cũng có thể đạt được 600 hay 650 điểm. Về kết cấu sách thì rất là ok: dễ đọc, dễ làm bài. Nếu các bạn có điểm xuất phát tiếng anh hơi thấp thì mình đề xuất nên làm quyển very easy toeic trước. Tuy vậy mình thấy giá cũng hơi cao một chút,
4
545325
2015-07-25 22:24:21
--------------------------
232676
9584
mình biết đến stater toeic vô tình học trên mạng và cô giáo nói nếu muốn học tốt toeic thì tủ sách bạn phải có ngay cuốn stater toeic nhé ! thế là mình lên kiti tìm và sở hữu ngay cuốn này... cuốn stater toeic third edition có 3 đĩa nghe, bên trong sách là từng bài học, những câu hỏi tương tự như kỳ thi toeic gần đây cho bạn thêm vững , có hình ảnh tăng sự hứng thú thêm cho bạn , phần sau có những mẫu đề thi, vì cuốn cũng mới phát hành nên đề thường tương tự cho bạn hiểu cấu trúc đề thi toeic cho bạn dễ nắm bắt, bên cạnh đó đĩa nghe sẽ giúp bạn phát triển kỉ năng nghe, thi toeic không phải dễ mà còn phải đạt điểm cao nữa... hy vọng với cuốn sách này bạn sẽ làm tốt các bài thi toeic
5
480717
2015-07-18 22:25:00
--------------------------
221450
9584
Cuốn sách Starter Toeic Third Edition này chủ yếu thiên về việc ôn lại các thành phần của cấu trúc ngữ pháp trong câu như động từ, tính từ, trợ động từ, trang từ,... cách thành lập câu. Cuốn sách giải thích tương đối dễ hiểu, trình bày logic, mạch lạc, dễ tra cứu, ngoài ra còn có bài tập đi kèm khi giải thích một điểm ngữ pháp nào đó, giúp người học có thể ứng dụng ngay tức khắc những gì vừa mới học lý thuyết xong, tất nhiên là có đáp án đi kèm phía sau sách. Ngoài ra cuốn sách này Tiki bán có kèm 3 CD rất thuận tiện cho việc luyện nghe
5
572238
2015-07-03 21:54:32
--------------------------
162397
9584
Vào năm 2007 khi phong trào thi Toeic bắt đầu rộ lên ở Việt Nam thì quyển sách Toeic Starter này được xem là một trong những quyển sách đầu tiên được First News mua bản quyền từ nước ngoài để xuất bản ở Việt Nam. Đây là quyển đầu tiên trong bộ 4 quyển luyện thi Toeic này và được định dạng theo Format mới phù hợp với chương trình thi Toeic ở Việt Nam hiện nay. Vì là quyển đầu tiên nên mức độ luyện về hình ảnh, từ vựng,.. rất phù hợp với những người mới bắt đầu làm quen với Toeic. Với giá thành khi xuất bản ở Việt Nam rẻ hơn rất nhiều so với bản gốc thì đây là một quyển sách đáng mua.
5
356759
2015-03-02 11:31:01
--------------------------
142776
9584
Mình nghĩ đây là một cuốn sách dùng để luyện thi Toeic rất hay và bổ ích. Vì trong cuốn sách này không chỉ có các đề Toeic để người học tự làm mà còn có thêm 12 bài về ngữ pháp để bạn ôn lại những công thức ngữ pháp. Đây là những cấu trúc ngữ pháp rất hay được sử dụng trong kỳ thi này. 
Về phần nghe, CD có chất lượng tốt, giọng đọc dễ nghe. Cuối sách còn có bản word để bạn kiểm tra lại. Chỉ có một lưu ý nhỏ là nếu bạn là người mới bắt đầu học Tiếng anh thì chưa nên học ngay cuốn này vội. Vì cuốn này yêu cầu người học phải có một chút nền tiếng anh căn bản.
4
145385
2014-12-19 21:19:52
--------------------------
120291
9584
Mình vừa bắt đầu luyện thi Toeic, không có kinh nghiệm chọn sách thì được một người chị chỉ cho cuốn này. Mình thấy nó cũng khá hay, thuận tiện cho người muốn tự học như mình: có phần gramma khá đầy đủ, trình bày dễ hiểu, những bài mini-test rất đa dạng, trình độ tương đối dễ, bài đọc hay, đề cập đến nhiều vấn đề thiết thực, từ vựng đơn giản, bài nghe dễ nghe, dễ hiểu, giọng đọc phát âm chuẩn và nói với tốc độ vừa phải, điểm trừ duy nhất là CD để thực hành nghe lại không sắp xếp đúng thứ tự bài, phải vừa nghe vừa chọn đúng phần mình cần. Nhìn chung, đây là 1 cuốn sách khá ổn cho những bạn muốn tự học ở nhà.
4
41592
2014-08-11 10:20:28
--------------------------
119678
9584
Mình theo học kinh tế nên những gì có thể học được ở quyển này thật sự rất bổ ích đối với mình. Các hình ảnh minh họa rất đẹp, có rất nhiều câu cú pháp ngoại ngữ mình không biết nên phải ôn lại hoàn toàn. Cách giải thích rất rõ ràng và đưa ra ví dụ tỉ mỉ, cùng những dạng bài tập chuyên sâu để dễ dàng ôn luyện. Mình rất thích quyển sách này vì cách trình bày khoa học và khổ giấy rất đẹp nữa. Giá thành so với mặt bằng chung của các quyển như tomato thì cũng không đắt lắm.
5
3276
2014-08-06 19:09:32
--------------------------
116678
9584
Mình đã từng tự ôn luyện thi TOEIC bằng cuốn sách này. Dạng bài tập khá dễ, phù hợp với những ai mới bắt đầu ôn- ngữ pháp và từ vựng chưa phong phú lắm. Những dạng bài tập nhỏ sẽ củng cố cho bạn thêm vững vàng hơn, trước khi bắt đầu làm một bài thi TOEIC thật sự. Sách được trình bày khá rõ ràng, dễ theo dõi trên giấy tốt khổ lớn. Thiết nghĩ các bạn hoàn toàn có thể tự học và đạt kết quả cao như mình (>800 TOEIC), nên mua đầy đủ bộ sách này từ dễ đến khó và mua thêm quyển BARRON's TOEIC để luyện thêm dạng bài tập.
4
308048
2014-07-10 20:55:04
--------------------------
102014
9584
Quyển sách này giúp mình định hướng rõ hơn với đề thi toeic, bên cạnh đó với những điểm ngữ pháp và hướng dẫn rõ ràng bằng tiếng Anh giúp mình có thể học thêm những từ gần nghĩa, cách diễn giải English by English, giấy in rõ ràng, đẹp mắt làm tăng hứng thù khi học của mình. Nói chung đây là 1 quyển sách cần thiết cho những ai đang muốn làm quen với dạng thi Toeic . Minh đang chuẩn bị mua thêm Toeic Analyst trong series của sách này, hihi, sách đẹp thành ra học cũng thích hơn mà mình cũng mở rộng thêm được kiến thức về kinh tế nữa.
4
95128
2013-12-13 10:07:26
--------------------------
49653
9584
Mình đã sử dụng cuốn sách này, rất bổ ích cho những bạn đã, đang và sẽ học và luyện thi Toeic.
Mình rất thích thú với cuốn sách này, trình bày rõ ràng, hình ảnh đa dạng và phong phú, và bài tập rất nhiều.
Cuốn sách trải đều trên hầu hết các lĩnh vực kinh tế.
Sách được trình bày trên khổ giấy lớn nên không tạo cho chúng ta cảm giác uể oải, chán nản, ngược lại, bởi ưu thế là được trình bày trên khổ giấy lớn nên tạo cho chúng ta một cảm giác thích thú khi học.
Hiện tại, tôi sử dụng bộ sách gồm ( Started toiec, Develop toeic,...) để tự học Toeic.
Tôi thấy cuốn sách rất phù hợp với những bạn muốn tự mình chinh phục ngôn ngữ này. Thật sự rất bổ ích và dễ sử dụng!
3
78790
2012-12-08 23:26:03
--------------------------
44749
9584
mình đọc phần giới thiệu sản phẩm, thấy thuyết phục nên mua.
mình chưa có kinh nghiệm mua sách luyện thi các chứng chỉ, nhưng mình thấy cuốn này phù hợp.
mình thi bằng c, nhưng cuốn sách này cũng có thể hỗ trợ đắc lực để mình lấy bằng c.
với cuốn sách này, bạn có thể tự ôn tập ở nhà, không cần phải đến các trung tâm luyện thi, tiết kiệm được thời gian và tiền bạc, hơn nữa lại rèn cho mình tính tự học nữa.
các bạn nào có ý định tự học ở nhà để thi toic hay chứng chỉ c  thì nên mua cuốn sách hữu ích này.

3
67247
2012-11-03 23:23:53
--------------------------
