460006
9848
Cuốn sách ấn tượng với tôi ngay từ trang đầu tiên, vì nó đánh ngay vào tâm lí của người đọc. Một đòn phủ đàu đẹp, rất đẹp, nói lên được những thứ mà người ta đang cần, thứ mà những nhà kinh doanh luôn muốn, nhất là với tôi, người vừa học kinh doanh. Ngoài việc giúp người đọc hiểu vì sao những người giàu lại thu lợi nhanh hơn, thì còn cống hiến thêm khá nhiều về tư duy chiến lược. Gấp cuốn sách lại, tôi tin không chỉ tôi mà nhiều người nữa, đã định hình trong đầu mình một số chiến lược, và sẽ nhanh chóng biến nó thành thực tế trong tương lai gần.
5
920424
2016-06-26 18:33:57
--------------------------
294088
9848
Ngay khi lật cuốn sách này lên, tác giả đã đi ngay vào vấn đề bằng câu hỏi "Làm thế nào để biến 10.000 đô la thành 10 triệu đô la trong 10 năm?". Nguyên nhân chinh  khiến nhiều người gặp khó khăn tài chính là do họ đã nghe theo những lời khuyên về cách sử dụng tiền bạc của những người nghèo hoặc những người bán hàng - đây là khẳng định của người cha giàu. Hãy học cách đầu tư, và trở thành một nhà đầu tư quyền lực, "Càng hiểu biết về đầu tư, bạn sẽ càng nhận được những lời khuyên có giá trị", hãy học hỏi những kinh nghiệm của những người giàu. Và phải hiểu được lý do người giàu tại sao ngày càng giàu hơn. Vì họ có thể kiếm tiền được ngay cả trong cơn suy thoái. Và hãy trở thành người chiến thắng trong đầu tư.
4
156324
2015-09-08 23:15:17
--------------------------
282386
9848
Đầu tiên, tôi rất hài lòng khi nhận được sách, sách được bao bọc trong bì rất sạch sẽ và mới. Cảm ơn Tiki vì chất lượng dịch vụ. Thứ hai, quyển sách này cũng là một trong những quyển sách mà tôi cảm thấy mình được mở mang đầu óc rất nhiều về lĩnh vực đầu tư và cách tạo nên bước đột phá trong thu nhập của mình. Mặc dù cách viết của ông còn lặp lại nhiều lần nhưng tôi nghĩ mỗi lần đọc được một điều giống nhau là mỗi lần não bộ có cách tiếp nhận thông tin khác nhau. Vì vậy tôi vẫn rất khuyến khích mọi người nên đọc quyển sách này
5
169373
2015-08-29 09:17:40
--------------------------
135949
9848
Cuối cùng thì Robert Kiyosaki cũng nói rõ và nhấn mạnh vào những cách để đạt được thành công trong tài chính. Cách ông viết vẫn lặp đi lặp lại nhiều lần, nhưng ít ra cuối mỗi phần Sharon Lechter tóm tắt và đưa ra ý chính rất rõ vàng và dễ hiểu. 
Quyển sách này dường như (đối với tôi) có ích hơn tất cả những quyển trước đó mà tôi từng đọc trong series DẠY CON LÀM GIÀU. Nó chỉ cho bạn cách tăng cường tài chính bằng cách sử dụng nợ tốt. Mặc dù một số người sẽ nói những phương pháp ông đưa ra mang rất nhiều rủi ro, sau khi đọc quyển sách tôi nghĩ những người đó sẽ phải suy nghĩ lại.
5
462453
2014-11-18 07:36:37
--------------------------
