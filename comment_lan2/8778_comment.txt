488206
8778
Mình rất ưng ý khi mua được cuốn sách "Nào đánh răng nhé!" cho con. Hình ảnh ngộ nghĩnh, sáng tạo, vui nhộn. Nội dung đơn giản, dễ hiểu mà ý nghĩa. Cuốn truyện tạo hứng thú cho bé trong việc vệ sinh răng miệng hàng ngày. Đánh răng không phải là nghĩa vụ nữa, mà trở thành niềm vui.
5
1804272
2016-10-31 18:20:58
--------------------------
471134
8778
Bộ sách Cùng chơi với bé được thiết kế khá hay, giống như trò chơi lật hình, ba mẹ có thể cùng chơi và cùng giúp bé học các kỹ năng cơ bản, cần thiết như chào hỏi, đánh răng, đi tắm... Ở quyển Nào đánh răng nhé, bé sẽ nhìn thấy bạn mèo Mike, bạn khủng long, bạn cún Koro, bé Yu đánh răng bằng bàn chải cho thật sạch. Có bạn gà Pi không có răng thì cũng lấy cái bàn chải kỳ cọ cái mỏ, thiệt là dễ thương hết sức. Sách tuy nội dung không có gì, chỉ đơn giản là hết bạn này tới bạn kia đánh răng nhưng cũng khá vui và làm bé thích.
4
460351
2016-07-08 11:16:14
--------------------------
243086
8778
Khi con mình khoảng một tuổi thì mình dạy bé đánh răng. Mình mua cho bé cuốn sách này để bé thấy việc đánh răng cũng rất vui, các bạn trong sách đều chăm chỉ đánh răng cả. Sách thiết kế lật hình lạ mắt, bé nhà mình rất thích lật đi lật lại các trang để xem các nhân vật cử động. Dù đọc nhiều lần nhưng mỗi khi mở sách ra lật từng trang thì bé vẫn reo lên thích thú và cười khanh khách, nhất là chỗ bạn khủng long thay vì đánh răng lại cầm cây kẹo mút.
5
82774
2015-07-27 10:21:30
--------------------------
236224
8778
Mình đã mua cả bộ Cùng chơi với bé của nhà sách Kim đồng và mình thấy nội dung của cuốn Cùng Chơi Với Bé - Nào Đánh Răng Nhé! là có điểm nhấn nhất ở chỗ các bạn khác đều cầm bàn chải đánh răng thì đến bạn khủng long tham ăn thì lại là một cái kẹo mút, bạn ấy cầm nhầm, cứ kể đến tình tiết này là bé nhà mình lại ồ lên thích thú mà tiện thể mình cũng có thể dạy thêm cho bé về việc ăn kẹo thì sẽ bị sâu răng.
Về hình thức của sách thì giống như các quyển khác cùng bộ mình thấy hình thức lật mở là điểm nổi bật nhất của bộ truyện này
4
153008
2015-07-21 17:02:10
--------------------------
180831
8778
Bộ truyện này được dịch từ một bộ truyện tiếng Nhật mà theo tôi được biết bộ truyện này rất được các bé ở Nhật yêu thích. Truyện được thiết kế đặc biệt dưới dạng lật hình, mang tính tương tác cao, bố mẹ có thể vừa kể chuyện vừa chơi với con, bố mẹ kể xong thì để bé tự lật hình. Truyện rất ít chữ, hình vẽ to, nét vẽ đẹp, màu sắc bắt mắt, thích hợp cho các bé từ 1 tuổi trở lên.Truyện “ Nào đánh răng nhé” dạy bé cách đánh răng sau khi ăn xong để giữ răng luôn luôn sạch bóng, miệng thơm tho.Bố mẹ hãy mua bộ sách này về để cùng chơi với bé nhé.
4
15022
2015-04-10 10:00:57
--------------------------
136174
8778
Bộ cùng chơi với bé này đúng như giới thiệu, rất đẹp. mình rất yêu. bé nhà mình mới hơn 1 tuổi, chắc chưa hiểu gì nhưng mà đã bị cuốn sách này thu hút rất nhiều. Mình mua 3/5 cuốn, thấy cuốn nào cũng đẹp, các bài học bổ ích, ngộ nghĩnh. Sách được in và xếp khoa học, sáng tạo, chất liệu đẹp, dầy.  Đúng theo phong cách Nhật Bản, các câu chuyện được đưa ra rất tự nhiên, nhiều khi khiến mình phì cười. Mình đọc và chỉ cho bé xem, bé cũng rất chú ý và rất thích. Mình rất hài lòng khi mua sản phẩm này.
4
24984
2014-11-19 15:05:42
--------------------------
