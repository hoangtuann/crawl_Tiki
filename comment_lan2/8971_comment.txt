465844
8971
Về hình thức trình bày: Sách khá mỏng cộng thêm in bìa mềm bên cầm trên tay rất èo uột, cảm giác không thoải mái. Tuy nhiên, giấy in lại khá dày dặn, mực in rõ ràng, không mờ, không phai. Mình khá thích điểm này. Ngoài ra tranh minh họa cũng khá phù hợp với nội dung từng mẩu truyện ngắn.

Về nội dung: Cuốn sách là tập hợp những câu chuyện hài hước tác giả góp nhặt từ cuộc sống hàng ngày. Mỗi câu chuyện mang một hàm ý sâu sắc về cuộc sống. Có yếu tố hài hước, gây cười tuy nhiên chưa đến độ. Cảm giác của mình tiếng cười ở đây mang lại chủ yếu là cười mỉa mai lối sống, thói suy nghĩ của những kiểu người khác nhau trong xã hội. Nếu các bạn muốn đọc cuốn này để giải trí theo mình không phù hợp lắm. 
4
21151
2016-07-01 14:50:57
--------------------------
452362
8971
Mình mua cuốn sách vì ấn tượng với cái tựa "Váy ơi là váy", nghe kiểu như một câu cảm thán vừa cười vừa mếu. Mở đầu vào, mình đã bị thu hút và rất ấn tượng với giọng văn này khi anh lý giải sự tích cái tên Hai Đầu Méo. Cuốn sách đem đến cho người đọc những tràng cười sảng khoải, nhưng ẩn sâu bên trong vẫn là sự châm biếm sâu sắc.

Sách không quá khó đọc, đơn giản, dễ hiểu. Những tựa ngắn tác giả đặt tên cho câu chuyện của mình đọc lên cũng đã buồn cười rồi. Sau cuốn này mình có mua thêm cuốn "Triều cường, chân ngắn và rau sạch". Bạn thấy đó, nghe cái tựa thôi đã thấy tò mò háo hức rồi.
4
186199
2016-06-20 16:29:28
--------------------------
330080
8971
 Mình bị thu hút bởi cái tên sách rất vui " Váy ơi là váy " của tác giả Trần Nhã Thụy nên đã đặt mua ở ti ki về đọc thử. Bìa sách khá ấn tượng. Nội dung sách gần gũi với cuộc sống, giọng văn hài hước. trào phúng, mang tính thời sự, phê phán, châm biếm. Đây là tác phẩm giả trí rất tốt, giúp đầu óc thư giãn, sảng khoái tinh thần. Mình rất thấy cuốn sách  " Váy ơi là váy " của tác giả Trần Nhã Thụy và không thất vọng khi đã quyết định mua nó.  
4
730635
2015-11-01 22:30:54
--------------------------
293622
8971
Trước đây mình là fan của báo Tuổi trẻ cười, nhưng 5 năm gần đây thì không mua nữa bởi chất lượng báo đi xuống. Tuy nhiên, với quyển váy vơi là váy thì có lẽ không thất vọng. Những tác phẩm được lựa chọn đều là những tác phẩm chất lượng, hài hước có tính trào phúng, thời sự, phê phán, châm biếm đúng chỗ. Tác phẩm để đọc giải trí rất tốt, mà cũng góp phần đả kích những thói hư tật xấu theo đúng phong cách của Tuổi trẻ cười trước đến giờ. Sách đáng mua, mình đã không thất vọng khi đặt mua
4
62433
2015-09-08 14:59:16
--------------------------
78839
8971
Mình đọc nhiều truyện trên Tuổi trẻ cười vài năm gần đây và cảm thấy hơi vô vị, ít có cười lắm. Nhưng với Váy ơi là váy, mình nghĩ là không hối tiếc khi mua. Mình và em gái cười nắc nẻ với từng mẩu chuyện. Mình lại là dân học xã hội nên cảm thấy rất hài lòng với tác giả, trào phúng đúng chỗ, đúng thời sự. Hài hước và nhìn nhận cuộc sống, xã hội ngày nay bằng một cái nhìn có hơi châm biếm. Đọc xong đầu óc rất thư giãn, sảng khoái tinh thần hơn. 
4
100289
2013-06-03 20:32:22
--------------------------
