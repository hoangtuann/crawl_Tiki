451236
9311
Mình rất thích tác phẩm này, mình đã từng đọc qua rồi, nó để lại cho mình một ấn tượng rất sâu sắc. Nhân vật mình thích nhất chính là nàng Mercedès. Nàng ấy cực kì đáng thương, mình thật sự thấy tiếc cho cô gái tuổi xuân phơi phới nhưng lại chịu nỗi bất hạnh to lớn đến thế.
Tuy nhiên hãy bỏ qua phần nội dung, cuốn sách này là dành cho những ai muốn luyện kĩ năng nghe tiếng Anh, mình cũng đang rèn skill nên mua rất nhiều cuốn của bộ sách này về nghe, giọng đọc của cd rất rõ ràng, tốc độ không quá nhanh, rất thích hợp để luyện đấy.
5
448018
2016-06-19 15:40:17
--------------------------
298680
9311
đây là quyền sách giúp cho các em học sinh cấp 1 luyện nghe và phát âm chuẩn rất tốt đặc biệt là  chiếc đĩa CD kèm theo sách rất hữu ích. Giọng đọc chậm dễ nghe và dễ hiểu và có cảm xúc phù hợp cho những người còn kém về phần phát âm. Bìa sách rất đẹp và cứng để bảo vệ sách, chữ in cũng rõ ràng còn kèm theo hình minh họa trông rất đẹp mắt . Nói chung đây là một quyển sách bổ ích ,cảm ơn nhà xuất bản first new về cuốn sách này
5
626659
2015-09-12 20:25:06
--------------------------
282197
9311
mình rất thích mua sách ở tiki sách được bao bọc cẩn thận mở ra là thơm mùi mực sách hay nhưng nội dụng khác với sách nguyên văn nhưng dễ hiểu giọng đọc chuẩn ( cách kể chuyện lẫn giọng đọc đều rất hay và có cảm xúc ! ) thích hợp cho học sinh cấp 1( có nhiều cấp độ để lựa chọn) nghe nói chung là thích  mình không thể ngừng đọc cuốn sách rất lôi cuốn hữu ích cho việc luyện kĩ năng nghe và đọc nhà xuất bản first new mình rất tinh tưởng mình đang sưu tập
5
376777
2015-08-28 23:17:23
--------------------------
281924
9311
"Bá tước Monte Cristo" là tiểu thuyết của Alexandre Dumas mà mình rất yêu thích từ khi còn ngồi trên ghế nàh trường nên mình đã không ngần ngại chọn mua cuốn sách này để thỉnh thoảng đọc sống lại những kỷ niệm một thời với tác phẩm kinh điển yêu thích  cũng một phần củng cố thêm kiến thức tiếng Anh và khả năng đọc hiểu của mình. Một công đôi việc.Sách được viết hoàn toàn bằng tiếng Anh với những "key words" chú thích bên dưới để thuận tiện cho việc học từ và đọc hiểu. Sách còn kèm đĩa CD giúp dễ dàng cho việc phát âm từ vựng. Một cuốn sách thú vị và bổ ích cho việc cung cấp kiến thức văn học lẫn việc học tiếng Anh.
5
75031
2015-08-28 20:03:19
--------------------------
64893
9311
Khi gấp cuốn sách lại, tôi không hề cảm thấy thanh thản, mãn nguyện hay hạnh phúc. Dù rằng kết chuyện, Edmon Dantes đã tìm thấy tình yêu từ nàng Hayde, có thể xem như đó là sự bù đắp của cuộc đời cho những đau thương khốn khổ mà chàng phải chịu đựng bởi lũ đê hèn.
Thế nhưng, Merxedes một thời của chàng thì sao?

Đừng nói rằng nàng là người mẹ còn người con trai dũng cảm làm niềm tin và điểm tựa. Người phụ nữ ấy, đã làm gì để phải mất mát nhiều đến thế? cả thời thanh xuân và cả quãng đời còn lại.

Giờ đây, cạnh nàng, một bên là nấm mồ của Fednang, một bên là của Edmon Dantes. Trời đất, con người ấy vẫn sống đấy thôi. Edmon của nàng vẫn đã hiện ra, vững vàng và bất khuất. Nhưng chính nàng cũng hiểu rằng, cuộc sống bất hạnh đã biến chàng thành Monte Cristo. nàng không yêu vị bá tước ấy.
Dường như, cuộc đời của nàng chấm dứt từ khi có tin Edmon đã chết. Cưới Fednang, cũng chỉ là thân xác. mấy chục năm trời, nàng sống không tình yêu, u uẩn và bất hạnh. Đến khi Edmon hiện ra, thì nàng thực sự đã mất hẳn.  "Chàng không còn nồng nhiệt như ngày xưa" nàng biết thế.
Nhưng giá như Edmon đừng trở về thì nàng sẽ chỉ chết một lần. Cái chết lần thứ 2 còn đau đớn hơn, bất hạnh hơn. 

Tôi đã không thể chịu đựng được khi nhận ra rằng, tình yêu nơi Edmon đã phai nhạt, chỉ còn lại dáng hình. Thù hận có thể khiến chàng thay đổi để trả thù, Đấy là quyền và  chàng phải làm. Nhưng thù hận sao lại làm nhạt nhòa trái tim chàng thủy thủ khi xưa?


5
50091
2013-03-22 13:33:43
--------------------------
