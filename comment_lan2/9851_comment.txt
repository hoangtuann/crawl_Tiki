77915
9851
Sau khi bộ truyện tranh đã đi hết 3/4 chặn đường (tập 33), chúng em đã thực sự thân quen và thích thú Đôraemon vì ở đó, chúng em được phiêu lưu cùng nhóm bạn Nô, Đô, Xu, Chai, Xê..., được tung bay trí tưởng tượng và được sống lại tuổi thơ diệu kì...Và chú Fujiko-F-Fujio đã cho chúng em ôn lại những mẫu chuyện vui và màu nhiệm bằng tập truyện Đôraemon đố vui, trong đó có Đôraemon quá khứ và tương lai-tập mà em đang nhận xét đây. Từ kiến thức có được ở Đôraemon từ tập 23-33, chúng em được thử thách bằng những câu đố hóc búa nhưng không kém phần thú vị, tính giải trí được lồng ghép một cách khéo léo vào cuộc trò chuyện của nhóm bạn nhỏ dễ thương làm chúng em không bị áp lực mà còn mê mẫn và dễ chịu...Ở tập này, chú Fujiko-F-Fujio vẫn giữ được nét vẽ giản dị, gàn gũi, mộc mạc nhưng đầy lôi cuốn như những tập trước, thật đáng khâm phục, những câu đố rất dễ thương, gắn kết với những gì mà chúng em đã từng đọc. Tóm lại, em rất yêu quyển sách truyện này, nó giúp chúng em luôn giữ mãi những hình ảnh đẹp, kỉ niệm thơ ngây trong sáng, kí ức thần kì về nhóm bạn Đôraemon-một hình ảnh đáng yêu, diệu kì...
5
120263
2013-05-30 11:15:09
--------------------------
