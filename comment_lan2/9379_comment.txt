471643
9379
Tôi đã mua cuốn “Truyện Cổ Tích Kinh Điển 3D - Vườn Hoa Của Người Khổng Lồ” vì đã được tiếp xúc với truyện này bằng tiếng Anh, nên mua xem bản tiếng Việt như thế nào. Ưu điểm: hình ảnh hình bìa hấp dẫn, (hình vẽ phong cách 3D nhìn rất sinh động), tất cả nhân vật đều vẽ rất dễ thương, trừ nhân vật chính là Người Khổng Lồ, theo quan điểm cá nhân. Có lẽ vì trước khi hiểu chuyện, Người Khổng Lồ phải luôn cau có, nên hình ảnh không được dễ thương lắm? Mực in đậm và rõ trên giấy cứng dày. Chất lượng sách như vậy tôi rất hài lòng. Khuyết điểm: không thấy trừ quan điểm đã trình bày ở trên. 
4
966571
2016-07-08 20:36:02
--------------------------
