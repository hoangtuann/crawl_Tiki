308961
9860
Đây là một truyện mà mình thấy có lẽ khá khó tìm trong thể loại ngôn tình vì nó đề cập đến vấn đề gia đình sau hôn nhân mà ít nói tới hai người đã gặp nhau, tìm hiểu nhau, yêu nhau, và đến với nhau thế nào. Ban đầu đọc truyện mình thấy khá mông lung, hai nhân vật chính này là vợ chồng nhưng có thật sự yêu nhau? Nữ chính thường xuyên gặp phải mối tình đầu của mình và có lúc nhớ về kỷ niệm hai người trước kia, nam chính dù thái độ rất lịch thiệp bình tĩnh trưởng thành nhưng lại như gần như xa. Cuộc sống và hôn nhân của họ có lẽ cũng chỉ cứ như mặt hồ phẳng lặng như thế, mông lung như thế nếu biến cố lớn dây dưa tới cả gia đình và người yêu cũ của Tu Văn không xảy ra. Biến cố ấy khiến cả hai nhìn nhận lại cuộc hôn nhân của mình, tình cảm của mình, cùng nhau tìm cách giải quyết và cùng nhau vượt qua, chứ không phải giấu giếm đối phương để khi sự việc đã xảy ra thì bản thân chỉ có nghĩa vụ thông báo với người kia một tiếng. Tuy vậy mình thấy rất khó hiểu với nhân vật Cam Lộ. Cô chấp nhận kết hôn với Thượng Tu Văn khi bản thân còn thấy mơ hồ về anh và ngay cả sau khi kết hôn, cô vẫn thấy nhiều điều cơ bản về cuộc sống và bản thân Thượng Tu Văn mà cô không hề biết, chẳng nhẽ kết hôn chỉ là xúc động nhất thời? Nhưng càng đọc mình càng thấy hai nhân vật chính có tình cảm  vô cùng sâu đậm với nhau, có lẽ cách biểu hiện của họ làm người khác thấy khó hiểu mà thôi, bởi vì có sâu đậm nên mới trách móc và giận dữ với người kia khi giấu giếm và lừa dối mình, vì sâu đậm nên mới học cách tha thứ, cách bao dung nhau, vì sâu đậm nên mới chấp nhận quay lại sống cùng nhau, nhường nhịn và hòa hợp. Mâu thuẫn trong gia đình có lẽ phải cần hơn sự cố gắng của một cá nhân mới có thể giải quyết được.
4
460557
2015-09-18 21:44:46
--------------------------
179045
9860
Có thể nói đây là một truyện 'nhạt như nước ốc' nếu đem nó so sánh với những bộ tiểu thuyết đình đám, hường phấn đầy trời hay ngược tâm chết đi sống lại trên thị trường hiện nay. Truyện chỉ xoay quanh cuộc hôn nhân của hai con người bình thường, một cuộc hôn nhân không được gắn kết bởi tình yêu, mà là sự cần thiết. Cuộc sống hôn nhân êm đềm, và những sóng gió xảy đến, những nguy cơ xảy ra đe dọa đến cuộc hôn nhân vốn êm ắng này. Anh chị vốn không ràng buộc gì nhiều, lại thêm nhiều thử thách, cám dỗ quá chi là tâm lí. Cái đáng quí là anh chị vẫn luôn tìm cách níu giữ và bảo vệ cuộc hôn nhân của mình, và ấm áp hơn nữa là tìm thấy được tình yêu thực sự thắp sáng thêm cho cuộc sống của hai người. Một câu chuyện đầy ý nghĩa, thích hợp cho một buổi chiều tà, một cốc trà hương tỏa.
4
209069
2015-04-06 12:43:59
--------------------------
177449
9860
Ban đầu đọc truyện, tôi thấy dường như Thượng Tu Văn không yêu Cam Lộ, và ngược lại. Họ lấy nhau vì đến thời điểm thích hợp, gặp một người thích hợp, nhưn rất nhiều cuộc hôn nhân khác ngoài đời thực. Nhưng càng đọc, dần nhận ra họ có yêu nhau đấy, chỉ là tình yêu đó bình lặng quá, khi sóng gió ập tới, nó lung lay tưởng chừng sẽ tan vỡ. Chính vì vậy, mới cần "gia cố tình yêu." Bẳng cách bao dung, thấu hiểu và thông cảm cho nhau, Cam Lộ và Tu Văn đã cứu được tình yêu và hôn nhân của mình. Dù có nhiều đoạn dài dòng quá, và tính cách của nữ chính Cam Lộ đôi khi hơi mâu thuẫn, nhẫn nhịn những lúc không cần nhịn nhưng khi thì lại nóng giận không đáng có, nhưng đây là một truyện đọc được.
3
57459
2015-04-03 10:06:49
--------------------------
116203
9860
Minh thích cuốn này của Thanh Sam Lạc Thác. Câu chuyện trong tiểu thuyết này rất gần gũi với cuộc sống, rất phù hợp với tình trạng của giới trẻ hiện nay. Nhiều căp vợ chồng trẻ hiện nay quen nhau thông qua giới thiệu của bạn bè, người thân. Sau một thời gian tìm hiểu, cảm thấy hợp nhau, có thể sống chung và xây dựng một gia đình đầm ấm liền đi đến hôn nhân. Cam Lộ và Thượng Tu Văn cũng vậy. 2 người quen biết, qua lại gần 2 năm thì quyết định kết hôn. Họ không trải qua một cuộc tình cuồng nhiệt, đắm say. 2 người có yêu nhau nhưng tình yêu họ dành cho nhau nhẹ nhàng như nước. Cam Lộ quyết định kết hôn với Tu Văn vì anh đem đến cho cô sự an toàn;  sự quan tâm, chăm sóc của anh khiến một người luôn phải lo lắng cho người khác như Cam Lộ có cảm giác được che chở. Còn Tu Văn cũng tìm được một cảm giác bình yên, thoải mái, cảm giác có một mái ấm thực sự khi ở bên cô. Tuy quen nhau 2 năm và sống chung 2 năm, nhưng 2 vợ chồng vẫn có những bí mật không thể chia sẻ cho nhau, đặc biệt là Tu Văn. Anh có một quá khứ cuồng nhiệt với mối tình thời niên thiếu và kết quả của cuộc tình ấy là sự mất mát không gì bù đắp được. Anh nghĩ rằng mối tình ấy đã kết thúc và thời gian sẽ làm phai mờ những kí ức đau khổ ấy. Nhưng không, “người cũ” vẫn không quên anh, vẫn muốn quay lại với anh, tìm đủ mọi cách để phá hỏng cuộc sống yên bình mà anh đang có. Mọi thứ đảo lộ, Lộ Lộ lạnh lùng với anh cộng với việc cô xảy thai khiến cuộc hôn nhân của họ đứng bên bờ vực ly hôn. Nhưng rồi 2 vợ chồng đã học được cách bao dung, thông cảm và tha thứ cho nhau. 
Câu chuyện khiến mình hiểu rằng, quan trọng nhất trong hôn nhân là sự tin tưởng, có tin tưởng mới có bao dung, thông cảm. Nếu hôn nhân mà không có sự tin tưởng thì chắc chắn sẽ tan vỡ.
4
14182
2014-07-05 23:08:36
--------------------------
108697
9860
Gia cố tình yêu, nói thật là cái tên truyện lúc đầu khiến mình không hiểu, gia cố là từ mình không mấy nghe thấy và sử dụng. Sau này hiểu theo ý mình và chắc là đúng, Gia cố tình yêu tức là thêm chút gia vị cho tình yêu, hôn nhân của họ thường thường nhạt nhạt, cần thêm chút gia vị để nó hợp vị, để họ hiểu nhau và sống hạnh phúc.
Diễn biến truyện rất chậm, lời văn giản dị gần gũi không hoa mỹ, nhân vật chính cũng không phải người quá đẹp quá tài và quá giàu mà truyện ngôn tình thường miêu tả. Tuy nhiên Gia cố tình yêu vẫn có thể dẫn bạn lật mở từng trang để tìm hiểu về cuộc sống hôn nhân của họ (nếu kiên nhẫn). 
Mình cảm thấy hơi nản khi đọc đến giữa truyện hay quá nửa gì đó bởi tác giả dường như đã kéo câu chuyện đi quá dài với một cốt truyện không mới, diễn biến chậm gần như có thể ru ngủ. Mình có thể đọc được truyện nhẹ nhàng diễn biến chậm, nhưng lại cảm thấy Gia cố tình yêu dài dòng quá, không thu hút. Tuy dài dòng nhưng không phải truyện nhảm.
Có lẽ đây là kiểu viết của Thanh Sam Lạc Thác, bạn nào hợp và quen rồi thì chắc sẽ thích.
3
49203
2014-03-21 19:46:51
--------------------------
97767
9860
Câu chuyện gia đình với nhiều tình tiết tự nhiên. Có hiểu lầm, có cãi vã nhưng cuối cùng đều xuất phát từ sự quan tâm dành cho cho của đôi vợ chồng trẻ Cam Lộ Và Thượng Tu Văn.
Ban đầu mình không thích nhân vật nam chính lắm vì mình cảm thấy anh rất hờ hững, giống như Cam Lộ chỉ là vật thay thế cho mối tình trước đó của anh.
Nhưng dần dần, qua những hiểu lầm và sự chu đáo, lối suy nghĩ của anh, mình cảm thấy Cam Lộ đã không chọn nhầm người.
Sự tin tưởng trong tình yêu là yếu tố quan trọng quyết định hạnh phúc. Đó có lẽ là điều mà tác giả muốn gửi đến độc giả. ^^
4
143438
2013-10-27 13:55:28
--------------------------
96216
9860
Cốt truyện của "Gia cố tình yêu" khiến tôi liên tưởng tới "Ánh sao ban ngày" của Mộc Phạn,cách viết nhẹ nhàng,không quá kịch tính nhưng đủ khiến người ta tò mò theo dõi.Lúc đầu cứ nghĩ Thượng Tu Văn vẫn còn yêu Hạ Tĩnh Nghi,nhưng thực ra anh kết hôn với Cam Lộ vì thực sự yêu và muốn xây dựng tương lai với cô,chứ không phải để lãng quên người yêu cũ,tôi khá thích tính cách của nam chính,và cả sự mạnh mẽ của nữ chính
Tuy nhiên,các mối quan hệ trong câu chuyện chưa thực sự rõ ràng,các mâu thuẫn có vẻ được xử lý khá chóng vánh,và các tình tiết liên quan tới thương trường,làm ăn...theo cảm nhận của riêng tôi là hơi rắc rối...Dù vậy,câu chuyện này cũng không quá phức tạp và đau đớn,rất dễ đọc
3
78343
2013-10-06 21:22:17
--------------------------
95060
9860
Đây là lần đầu mình đọc truyện của Thanh Sam Lạc Thác và cũng là lần đầu tiên đọc chuyện viết về cuộc sống hôn nhân.
Hôn nhân của Cam Lộ có thể trôi qua một cách bình lặng không thể nói là hoàn mỹ cho lắm nhưng cũng đủ làm nhiều người mơ ước. Hôn nhân của Cam Lộ dựa trên sự tin tưởng chồng và tình yêu đối với Thượng Tu Văn. Ban đầu đọc tôi cảm nhận như là thật sự Thượng Tu Văn không yêu Cam Lộ, chỉ đơn giản vì muốn quên đi tình cũ, nhưng rồi đọc rồi cảm nhận tình yêu của Thượng Tu Văn với cô sẽ không mãnh liệt như với tình đầu, nhưng sâu sắc và ngay từ đầu anh yêu Cam Lộ. Mỗi nhận vật chính đều có mối tình đầu đẹp đẽ, khó quên, nhưng rồi họ vẫn đến với nhau, yêu nhau, kết hôn, tình yêu của họ đầy yên ả không sống gió nếu không có sự xuất hiện của mối tình đầu của thượng Tu Văn. Sự xuất hiện đấy đã gây nhiêu biến cố cho cho cuộc sống hôn nhân của họ. Rồi sự thật dần dần hé mở từ từ và đầy bất ngờ, khi đọc đến đây tôi cũng như Cam Lộ không thể tha thứ được cho anh, đoạn này tác giả đã diễn tả rất tốt nội tâm của nhân vật. Nhưng rồi sau tất cả cô đã tha thứ cho anh. Cái hay của tác giả chính là cô đã viết để cho cả Cam Lộ và độc giả có thể tha thứ được cho sự che dấu quá khứ của Thượng Tu Văn. 
Đây cũng là cuốn sách tôi rất muốn đọc lại lần nữa :)...
4
175512
2013-09-21 12:24:24
--------------------------
94830
9860
Tác giả đã kể rất khéo léo về hiện thực đời sống hôn nhân giữa những người trẻ nơi thành thị.
Tình yêu chỉ là nền móng của hôn nhân, nếu thiếu đi những nguyên vật liệu khác như sự cảm thông, thấu hiểu lẫn nhau hay sự chia sẻ, bao dung giữa hai vợ chồng thì cuộc hôn nhân ấy khó mà bền vững.

Đúng như tên gọi, nữ chính Cam Lộ đã kiên trì đến cùng trên con đường "Gia cố tình yêu" - vun đắp cho cuộc hôn nhân với người chồng yêu dấu của cô. Theo từng trang truyện, tác giả bóc tách từng lớp, từng lớp câu chuyện, hé mở cho độc giả thấy những góc cạnh xù xì, mặt trái của một cuộc hôn nhân tưởng là như mơ.

Thật tốt là cuối cùng những nỗ lực của Cam Lộ và chồng cô đã có kết quả như ý. Câu chuyện được TSLT kể rất đẹp, câu chữ nhẹ nhàng nhưng mang nhiều ý nghĩa.

Đây quả thực là một quyển truyện đáng mua, đáng đọc và nghiền ngẫm :)
4
65543
2013-09-18 21:36:39
--------------------------
79583
9860
Vẫn như cũ, phong cách của Thanh Sam Lạc Thác vẫn rất cuốn hút người đọc, không gây cấn nhưng nhẹ nhàng dẫn dắt người đọc đến cuối câu chuyện. Tôi thấy các tác phẩm của Thanh Sam đều có liên quan với nhau, làm nên bố cục chặc chẽ. Cuộc hôn nhân có phần lí trí tìm bến đỗ yên bình pha lẩn chút tình cảm nảy mầm trải qua 2 năm dần sâu đâm hơn, đòi hỏi ở đối phương nhiều hơn và cũng dần phát hiện ra những lỗ hổng của cuộc sống vợ chồng.  Có lẽ ai đã kết hôn mới hiểu được những suy nghĩ cũng như quyết định của Cam Lộ trong cuộc hôn nhân với Tu Văn sau những sóng gió ấy. Tha thứ, chấp nhận và vun đắp lại gia đình. Một tác phẩm khá được, nên đoc !
4
8255
2013-06-08 01:51:18
--------------------------
64930
9860
Đây là tác phẩm thứ hai tôi đọc của Thanh Sam Lạc Thác. Cô vẫn là chính mình trong tác phẩm này. 

Xung đột trong hôn nhân là điều không thê tránh, nhưng bạn có đủ khôn ngoan và cảm tính khi giải quyết hay không? Cam Lộ trong câu truyện này đã làm được. Ban đầu, cô xử sự theo cảm tính vì đơn giản cô tin tưởng ở anh và anh đáng để có được sự tin tưởng từ cô, nhưng về sau là lúc lí trí lên ngôi, khi cô phát hiện mọi việc không đơn giản như bề ngoài của nó. Không phải là anh phản bội hay làm điều không đúng với cô, chỉ là cô mong anh đừng dấu cô mọi việc mà hãy chia sẻ cùng nhau vì họ là vợ chồng. Thật sự là rất đau trong khi cô là vợ anh nhưng lại là người biết mọi việc sau cùng. 

Ngoài ra, mối quan hệ mẹ chồng nàng dâu cũng khá phức tạp khi nó đã bị vấp váp ngay từ đầu, nhưng hãy cùng xem Cam Lộ làm sao để đưa nó về quỹ đạo vốn có nhé. Thật sự rất tài tình đấy.

Một lần nữa tôi lại nói, tác phẩm của Thanh Sam rất kén chọn người độc giả, nhưng nếu bạn là người kiên nhẫn thì bạn sẽ nhận ra được những giá trị cuộc sống, tình yêu đẹp đẽ mà tác giả muốn gửi đến người đọc qua từng con chữ.
4
80843
2013-03-22 18:09:14
--------------------------
44336
9860
Đây là một cuốn sách hay; hạnh phúc, đau thương và xen lẫn cả những sự ghen ghét. Bạn có thể cảm nhận được tình yêu của Cam Lộ và Tu Văn, họ rất yêu đối phương, nhưng họ chưa thật sự sẻ chia cho nhau những gì trong cuộc sống. Một cuộc sống gia đình hạnh phúc như Cam Lộ và Tu Văn, là cuộc sông mà ai cũng từng ước ao đến; NHƯNG, đằng sau sự hạnh phúc ấy, là một gia đình không toàn vẹn: che giấu quá khứ, chưa tìm hiểu về đối phương cho tường tận, không khí gia đình ngột ngạt...
Quyển sách này giúp bạn hiểu ra rằng, cuộc sông gia đình toàn vẹn không chỉ cần sự hạnh phúc mà còn phải sự cảm thông, chia sẻ
Truyện có những mâu thuẫn rất gần với đời sống hiện thực
Một kết thúc có hậu
Đây là một cuốn sách rất đáng đê mua đọc!
Thân!
4
66334
2012-11-02 01:04:21
--------------------------
