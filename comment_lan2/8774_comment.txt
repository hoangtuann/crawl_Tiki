339908
8774
Theo tôi thấy quyển sách này thì không nổi trội gì lắm. Tôi mua quyển sách này là do bìa sách đẹp, nhưng khi đọc tôi thấy hay, theo trí tưởng tượng của tôi thì cung điện versailles rất đẹp, đến từng loại thức ăn đồ uống, trang phục và cả con chó cũng đẹp nữa. Tất cả mọi người thì khỏi nói rồi, câu chuyện mang tính phiêu lưu và nhẹ nhàng. Cô bé làm nước hoa thông minh, tài giỏi và được trời ban cho cái "mũi" có thể ngửi được các loại mùi thơm mà không một ai làm được. Nhưng câu chuyện này hơi ngắn mặc dù có tới 2 phần
5
926703
2015-11-18 22:03:36
--------------------------
308054
8774
Cuốn sách này rất tuyệt, mở ra một thế giới vương quyền mà ở đó giới vua chúa, quý tộc sống trong nhung lụa, giàu sang và thừa mứa đồ ăn, thức uống hay là nước hoa. Họ có riêng một nhà bếp với rất nhiều nhân viên, người chế tạo nước hoa cũng riêng luôn. Marion là một cô gái thông minh và tài năng, người có biệt tài có thể chế ra được các loại nước hoa và cả cồn ngọt có thể khống chế được mùi hôi. Bởi thế cho nên cô mới bị bà hầu tước de Montespan lợi dụng cho kế hoạch ám sát của mụ ta. Trong truyện khá ấn tượng với con chó Pyrrhos của mụ, chó mà cũng biết ghen ăn tức ở giống như người, kết cục của nó là bị trúng độc mà chết bởi chính chủ nhân của mình. Tiểu thuyết của phương Tây, đọc khá cuốn hút, có điều tình tiết đi nhanh quá, đang đọc đoạn này thì đã tới đoạn Marion bị bắt cóc rồi, chẳng hiểu đầu đuôi ra sao, thì sau đó tác giả mới tua lại cảnh bắt cóc qua lời kể của nhân vật. Rồi ở truyện "Mùi hương sát nhân" cũng vậy, tác giả không tả cảnh mụ phù thủy Voisin bị đội cảnh binh của nhà vua bắt như thế nào mà đã dẫn độc giả tới ngay cảnh mụ bị bắt rồi và quan tòa đang lấy lời khai của Marion. 
5
27232
2015-09-18 14:23:07
--------------------------
125478
8774
Cũng như bạn Hải Yến, mình cũng nghĩ đó là 1 câu chuyện bí ẩn nhuốm màu lịch sử hoen ố nhưng vàng kim, và sẽ rất hồi hộp đến nghẹt thở. Nhưng hóa ra, đó chỉ là một câu chuyện nhỏ, sinh động, và hơi kỳ lạ một chút, 
Thật tiếc là tuy truyện đã bao gồm cả 2 phần 1&2, nhưng vẫn còn ít quá, sách còn mỏng. Cốt truyện diễn ra rất nhanh, cách giải quyết mọi việc cũng tương đối dễ dàng và dễ đoán, không có nhiều nút thắt, mở; nhưng dù gì đi chăng nữa thì đó vẫn là một câu chuyện nhẹ nhàng và thú vị.
3
140254
2014-09-13 16:07:52
--------------------------
103447
8774
Đọc qua phần giới thiệu thì mình đã mường tượng đây là một câu chuyện phiêu liêu, gay cấn đến nghẹt thở. 
Nhưng khi gấp cuốn truyện lại, thì đọng trong đầu mình nhiều nhất lại là sự nữ tính trong văn phong của tác giả.
Là một câu chuyện lịch sử hư cấu, nhưng nó vẫn khá sinh động, cảm động nữa. Tuy vậy, mạch chuyện diễn ra nhanh quá, đặc tả hơi ít. Đây chỉ đơn thuần là "nghệ thuật tự sự", nhưng đọc cũng đủ hay rồi.
Bìa truyện thực sự rất đẹp. Tuy vậy, giấy sách hơi mỏng, hơi khó lật trang.
4
87298
2013-12-30 21:38:41
--------------------------
