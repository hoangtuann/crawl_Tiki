112745
8749
Khi chọn một cuốn sách cho trẻ con, bạn không thể biết chắc được bé có thích không, cho đến khi bạn đọc cho bé nghe cuốn sách ấy. Bộ sách 4 cuốn này mình đặt mua hoàn toàn tình cờ, khi mở ra xem thử cũng không thích lắm. Sách mỏng, thơ không hẳn là có vần, được cái hình ảnh làm tạo cảm giác như búp bê, trông hay hay. Thế mà con mình thì nghĩ khác. Mới 18 tháng, bạn ấy nhớ khá tốt nội dung sách, cầm một cuốn lên có thể nói ngay đó là "Tàu hỏa của Luca" hay "Máy bay của Simon". Với bộ sách Bé gái tương tự, mình đã đặt mua hai lần cho hai bé, và thấy bé nào cũng hào hứng với những hình ảnh và nội dung trong sách.
5
82774
2014-05-19 16:19:20
--------------------------
