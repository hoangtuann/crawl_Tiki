321458
9503
Cuốn sách khá chi tiết cho người mới làm quen với thời trang nam. Tuy nhiên cũng cần biết căn bản may để thực hành dễ dàng hơn. Mình thì đang tỉ mẩn học may nên đang thử nghiệm.    
Các bước thực hành đều có hình minh hoạ và hướng dẫn nên mình khá ưng.
Các mẫu thiết kế đều là thiết kế cơ bản nhất cho nam chứ không có những kiểu may cách điệu hiện đại nên mình nghĩ cuốn sách này phù hợp cho những người học may đồ nam cơ bản. 
Nói chung đây là cuốn sách hữu ích

4
831269
2015-10-13 23:15:16
--------------------------
314439
9503
Sách tổng hợp các kỹ thuật may đồ nam, khá đủ các dạng phom dáng từ cơ bản  đến thời trang hiện đại. Mình chỉ biết may đồ nữ, nhưng sau khi vừa đọc vừa vẽ rập   và  thực hành theo hướng dẫn ở phần kỹ thuật thì may lên áo sơ mi và quần tây nam theo công thức trong sách cung cấp cũng khá ổn, mặc vừa không bị kích
Tuy nhiên, cần phải biết may căn bản (may được quần áo nữ, đầm váy) trước để hiểu những thuật ngữ chuyên ngành và ko bỡ ngỡ khi thực hành

4
49112
2015-09-26 01:39:55
--------------------------
135182
9503
Phần đầu cuốn sách cho bạn một cái nhìn chung về thời trang nam. Phần hai là kỹ thuật may, trình bày rõ ràng, tỉ mỉ cho bạn đọc các quy trình may áo vest, áo sơ mi, áo khoác ,quần tây, có hình minh họa từng bước một.
Có cả cách ủi vải thế nào cho đẹp, ... 
Và bạn cần học qua một lớp may căn bản thì khi đọc các sách dạy may sẽ dễ dàng hơn. Còn nếu bạn vừa đọc sách vừa lấy một sản phẩm tương tự để xem trực tiếp thì sẽ dễ hình dung hơn nữa.
5
441064
2014-11-13 19:09:04
--------------------------
