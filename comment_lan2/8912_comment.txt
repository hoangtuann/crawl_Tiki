562553
8912
Sách đọc hấp dẫn , dễ hiểu, dễ tiếp thu. Con gái mình đọc thấy rất thích , và hy vọng bé thực hiện tốt.
5
103851
2017-04-03 08:57:00
--------------------------
518885
8912
Quyển sách này thật sự rất bổ ích đối với tôi tôi đọc chỉ vừa một lát thôi là tôi đã bị cuốn hút bởi nó tác giả Tony Buzan đã thật sự viết ra một tác phẩm tuyệt vời quyển sách giúp tôi được điểm cao trong các bài kiểm tra học kì ở trường tôi rất là thích Và tôi cũng không hề cảm thấy rằng mình đã mua một tác phẩm ko hay và bản đồ tư duy giúp tôi học thuộc bài nhanh và dễ dàng hơn rất nhiều Cảm ơn Tony Buzan đã cho tôi một tác phẩm........ thật sự bổ ích
5
2032465
2017-02-04 17:23:20
--------------------------
495290
8912
Sách của Tony Buzan thì khỏi phải bàn về trình bày trang trí, cầm cuốn sách là mê bởi chất lượng giấy, màu sắc, quạn trọng nhất là nội dụng tuyệt vời, mình phải đọc xong rồi mới chuyền cho mấy đứa em, với kiến thức này những đưa em, đứa con, cháu của chúng ta sẽ có được một kĩ năng cực kì quan trọng trong học tập cũng như cuộc sống, ước gì mình biết cuốn này lúc còn nhỏ
5
882303
2016-12-12 08:00:13
--------------------------
425146
8912
Mình mua cuốn này năm mình lớp 9, lúc còn đang áp lực chuyện bài vở ôn thi vào cấp 3. Mình có nghiên cứu, làm theo. Thời gian đầu mình thấy có hiệu quả. Nhưng lên cấp 3 thì khác. Vì bản thân mình không tự ép mình học đều các môn ở trường, mình chỉ tập trug vào 3 môn khối chính nên để ẻm vào một góc ròi. Mỗi ng có một quan điểm khác nhau. Mình nghĩ nó phù hợp nhất đối với nhưngx em mới vào cấp 2, học theo pp này chắc chắn dẽ đem lại hiệu quả cao
3
53382
2016-05-03 20:29:13
--------------------------
359995
8912
Em năm nay đang là học sinh cấp Hai,vì áp lực bài vở rất nhiều nên em đã mua cuốn sách này để học tập theo phương pháp sơ đồ tư duy.Mặ dù phương pháp học tập này chưa được phổ biến rộng rải cho lắmm nhưng lại được rất nhiều nhà khoa học và doanh nhân thế giới đánh giá cao.Mấy ngày nay đang mò mẫm cái sơ đồ tư duy này và hi vọng nó sẽ giúp em thay đổi cách học và được hạng nhất khối.
Còn về chất lượng bìa và giấy thì rất bắt mắt hình ảnh sống động vô cùng ổn.Tóm lại em đánh giá cao về cuốn sách này,đặc biệt bổ ích cho các bạn học sinh,sinh viên.
5
999425
2015-12-28 14:25:22
--------------------------
353566
8912
Mình mới mua quyển này vì thấy nó nhận được nhiều lời nhận xét tích cực từ mọi người.Phải công nhận tiki giao hàng rất nhanh, sách mới nhận được lại mới và đẹp.Mình rất thích phong cách này của tiki.Còn nội dung thì khỏi chê.Sách in khổ to, giấy chất lượng, minh họa đẹp mắt với nhiều màu sắc.Cảm ơn tiki và cảm ơn những lời nhận xét của mọi người đã mang đến cho mình cuốn sách hay và thiết thực này.Mình viết nhận xét này mong nó sẽ có ích cho những bạn nào đang băn khoăn muốn mua quyển sách này,
5
91964
2015-12-16 14:08:33
--------------------------
306265
8912
Cá nhân tôi nghĩ quyển sách này phù hợp cho tất cả mọi người. Với bản đồ tư duy này chúng ta có thể lên kế hoạch, ghi nhớ mọi vấn đề một cách dễ dàng, chi tiết, khoa học và có hiệu quả. Khi đi học, hay khi đi làm chúng ta đều có thể vận dụng nó một cách linh hoạt vừa có thể nhớ lâu, in sâu vào não, vừa có thể kích thích các bán cầu não, khơi dậy và phát triển một cách tối ưu trí não của mình. Rất bổ ích và rất có hiệu quả khi áp dụng. Cám ơn tiki.
5
110777
2015-09-17 14:27:36
--------------------------
279331
8912
Sản phẩm rất đẹp , có nhiều màu sắc , thích hợp cho trẻ em và học sinh tiểu học. Mình nhận được sản phẩm rất vui nhưng nó dành cho trẻ em là chính mà. Sau khi đọc xong , mình đã cho đứa em mình đọc cuốn sách này, nghe có vẻ nó rất thích, bởi vì màu sắc đẹp nên nó rất thích.Cuốn sách hướng dẫn rất kĩ về cách học thế nào cho tốt vì thế hiệu quả sẽ rất cao. Chất lượng sản phẩm tốt
5
482616
2015-08-26 17:50:46
--------------------------
249367
8912
Sách này là sơ đồ tư duy dành cho trẻ em, với các trang giấy chất lượng cao, chữ to , in đẹp, nhiều màu sắc. Chính vì vậy mà các em sẽ lĩnh hội nhanh chóng các thông tin mà tác giả muốn truyền đạt lại. Tony Buzan giúp các em nâng cao hiệu quả học tập qua việc lập kế hoạch và ghi nhớ mọi việc qua các hình vẽ trên giấy, mà việc này thì do chính các bé tự tư duy sắp xếp nên các em sẽ ghi nhớ rất sâu. Phụ huynh nên chịu khó hướng dẫn các em làm quen từ từ thói quen xây dựng mind map này để các bé có thể vẽ nên những bức tranh tri thức cho chính bản thân.
5
544151
2015-07-31 15:49:47
--------------------------
245742
8912
Thực ra thì mình khá là ... hối hận khi mua quyển sách này, do không đọc kĩ. Đây là sách cho trẻ con, mình cứ nghĩ là một muốn dạy mind map một cách bài bản cơ. Mua về mới thấy bản sách to mà mỏng, hầu hết là hình vẽ, nhìn qua thì như một cuốn sách tranh bình thường. Tuy nhiên chính vì thế mà sách có khả năng truyền đạt thông tin khá tốt với trẻ, giúp trẻ rèn luyện tư duy hình ảnh, sử dụng mind map, vận dụng cả hai bán cầu não ngay từ khi còn nhỏ. Đồng thời sách cũng giúp trẻ giải quyết những vấn đề về các môn học có trong chương trình nữa. Mình nghĩ sách sẽ cực kì phù hợp nhà ai có em nhỏ đấy.
4
10907
2015-07-29 00:28:14
--------------------------
231923
8912
Trong quyển sách của Tony Buzan này mình đã được hướng dẫn đến với công cụ học tập hữa ích đó là " sơ đồ tư duy" một cách dễ dàng hơn bao giờ hết. So với các quyển sách khác việc sử dụng sơ đồ tư duy của mình trở nên tốt hơn. Nhờ đó mà việc ghi nhớ các bài học cũng tốt hơn so với lúc trước, thời gian học tập đươc tiết kiệm chỉ với một tờ giấy A4 và những cây bút màu. Cuốn sách này quả thật rất hữa dụng với các bạn học sinh. Ưu điểm của quyển sách này là có cách bày trí vô cùng sinh động cùng với sự hướng dẫn vô cùng dễ hiểu, ngoài ra các trang sách còn có có chất lượng khá tốt, chữ to và in màu nên đọc rất thích. 
5
583655
2015-07-18 14:16:12
--------------------------
217088
8912
Về mặt thẩm mỹ : Sách của Tony Buzan từ trước đến giờ hầu như quyển nào cũng đều được chau chuốt về góc thẩm mỹ . Bìa sách cho đến từng trang sách cũng được chau chuốt kĩ cương
Về nội dung
Tác giả Tony Buzan - cha đẻ của "sơ đồ tư duy " trong quyển sách này sẽ giúp cho chúng ta đi sâu hơn , tiếp cận gần hơn với việc ứng dụng sơ đồ tư duy vào việc học tập  . Giúp chúng ta mã hóa những điều cần nhớ , chỉ trên một tờ giấy A4 để ghi nhớ đơn giản hơn , tiết kiệm thời gian hơn
Cá nhân mình thấy việc học bằng sơ đồ tư duy rất rất hữu ích . Ngày trước của nhờ nó mà mình có thể đạt điểm tốt những môn học bài ( hơi khoe 1 tí ^^ ) 
Cảm ơn tác giả nhiều lắm
5
108594
2015-06-28 20:49:56
--------------------------
169762
8912
Qua quyển sách này, bạn sẽ biết về "sơ đồ tư duy", một phương tiện để nhớ, lập kế hoạch đơn giản mà hiệu quả. Các quyển sách của Tony Buzan chủ yếu về sơ đồ tư duy thì ở đây, chúng ta được tìm hiểu sâu hơn về mảng học tập. Mình có thể thoải mái sáng tạo, vẽ vời, trang trí theo ý muốn mà vẫn rất dễ để ghi nhớ các thông tin khi trình bày bằng sơ đồ tư duy này. Vừa tiện lợi là có thể làm ở mọi nơi chỉ với 1 tờ A4 và các thông tin muốn ghi nhớ, vừa hiệu quả là thông tin luôn được sắp xếp trong đầu mình theo từng "ngăn" mà khi muốn nhớ chỉ cần lục lại một cách nhanh chóng. 
 Rất hay và ý nghĩa!
5
540461
2015-03-18 21:25:09
--------------------------
161038
8912
Quyển sách giúp chúng ta tiếp cận với một khái niệm khá mới mẻ là "bản đồ tư duy". Lập bản đồ tư duy sẽ khiến việc học trở nên đơn giản hơn rất nhiều, kích thích khả năng sáng tạo của các em và hơn hết với công cụ này có thể giúp sử dụng được cả hai bán cầu não thay vì một bán cầu não theo cách học thông thường. Cuốn này Tony Buzan tập trung vào giới thiệu các thông tin cơ bản về cách lập "bản đồ tư duy" một cách hiệu quả nhất. Sách trình bày đẹp, logic, dễ hiểu. Tuy sách dày nhưng chữ khá to, rõ ràng phân chia màu cho từng phần nên rất dễ tìm và nắm bắt thông tin rất nhanh.
4
534227
2015-02-26 14:29:49
--------------------------
138714
8912
Mình mua cho đứa em hiện đang học cấp 2, nhỏ khen suốt. Nhỏ bảo cuốn này giải thích dễ hiểu, hình ảnh minh hoạ sinh động khiến nó thêm nhiều động lực để học phương pháp học mới này. Trong phòng học bây giờ mình thấy nhiều sơ đồ tư duy lắm, đọc cũng dễ hiểu vì nội dung rất cô đọng mà lại có nhiều hình ảnh đi kèm.  Mình rất mong qua cuốn sách này, không chỉ em mình mà các em khác trong tuổi từ 7-14 có thể trang bị cho mình phương pháp học mới hiệu quả hơn, điểm số trên lớp tốt hơn mà vẫn có thời gian vui chơi.
4
9695
2014-12-04 11:27:56
--------------------------
84143
8912
Quyển sách khiến tôi hài lòng bởi những bí quyết tuy mới mẻ nhưng rất thiết thực và hiệu quả. Chúng không chỉ là bí quyết, mà còn là giải pháp để thoát khỏi những phương pháp học tập khô khan, nhàm chán và kém hiệu quả trước đây, nâng cao hiệu quả học tập cho học sinh. Đặc biệt, tôi rất ấn tượng với lối trình bày không chỉ đẹp mà còn rất lôgic, dễ hiểu, dễ thực hiện của quyển sách. Tôi tin rằng, quyển sách là công cụ hiệu quả và thiết thực trong xã hội hiện đại ngày nay, khi mà Bộ giáo dục và đào tạo đang đề cao sự sáng tạo của học sinh để nâng cao hiệu quả và chất lượng học tập.
5
129990
2013-06-28 22:00:16
--------------------------
80765
8912
Là một học sinh ai cũng muốn mình đạt được nhiều thành tích cao ở trường để bạn bè, thầy cô và những người xung quanh kính trọng mình hơn, nhưng để làm được điều đó không phải là một chuyện dễ dàng, bởi vì hầu hết chúng ta đều không có phương pháp học và phải đổi mới không ngừng, ví dụ cấp 1 lên cấp 2, ở cấp 1 chỉ có 5 môn học, lên cấp 2 thì khoảng 13,14 môn thế nên ta sẽ bị shock, đây là cuốn sách giúp ta khắc phục điều ấy.Mặc dù trên thị trường hiện nay cuốn sách này đã quá quen thuộc nhưng hiện nay với bản dịch của first new thì ta có thể trông mong một điều gì đó đột phá hơn các bản dịch khác 
4
116168
2013-06-14 08:08:43
--------------------------
