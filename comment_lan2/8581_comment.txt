344848
8581
Bạn có muốn biết ai là người đã khai sáng nền văn minh của nhân loại không? Nếu bạn đã từng đặt cho mình câu hỏi đó thì đây, cuốn sách "Danh Nhân Khai Sang Văn Minh" sẽ giải đáp toàn bộ những thắc mắc của bạn. Cuốn sách  tóm lược về cuộc đời và những nhà phát minh khoa học, âm nhạc, vũ trụ....
Bạn sẽ được găp Jame Watt người đã phát minh ra máy hơi nước, Sự vĩ đại của Napoleon và nước Pháp, Andersen người đã mang thế giới cổ tích đến với chúng ta... Có quá nhiều điều thú vị trong cuốn sách này, bạn còn chờ gì nữa mà không khám phá chứ? 
Sách có hình ảnh minh hoạ giúp người đọc không cảm thấy nhàm chán. Đặc biệt mình nghĩ cuốn sách cũng có thể làm quà tặng cho các em nhỏ. Bià sách và giấy in đẹp. 
5
754539
2015-11-29 11:06:08
--------------------------
275733
8581
Quyển sách này nói về những danh nhân đã khai sáng và phát triển nền văn minh thế giới. Những danh nhân nổi tiếng đó là James Watt phát minh ra động cơ hơi nước, Goethe nhà thơ nhà tư tưởng, Mozart nhà soạn nhạc, Napoleon nhà độc tài Pháp, Beethoven nhà soạn nhạc, Gauss nhà toán học, Stephenson chế tạo tàu hoả, Vanderbilt vua đường biển, đại gia đường sắt, Andersen tiểu thuyết gia, Lincoln tổng thống Mỹ, Darwin tìm ra thuyết tiến hoá, Chopin nghệ sĩ dương cầm. Những câu chuyện trong quyển sách này được trình bày rất dí dỏm và thu hút người đọc.
5
698434
2015-08-23 10:15:02
--------------------------
270793
8581
Bìa quyển "Trường Học Danh Nhân Thế Giới - Danh Nhân Khai Sáng Văn Minh" của tiki giao cho mình không được đẹp như hình, không có hình nền hay đánh số tập như trên hình tuy nhiên được cái nội dung ổn phải nói là rất hay và thú vị mình rất thích, sách khá dày, các trang đều được in màu các mẩu truyện nhỏ về các nhân vật bên trong vừa vui, hài hước lại vừa cung cấp được rất nhiều thông tin cho các bé, hơn nữa hình minh họa rất sinh động chắc bé nào cũng thích.
4
153008
2015-08-18 16:43:14
--------------------------
