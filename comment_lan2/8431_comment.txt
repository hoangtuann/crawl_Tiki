471477
8431
Theo cá nhân thì mình không ưa truyện này lắm . Cỏ Dại là câu chuyện cảm động về tình bà cháu , tình bạn giản dị và trong sáng .... Truyện đã lấy được nước mắt của mình nhưng không hiểu sao truyện đọc rất mau chán . Chỉ đọc lần đầu là lần sau không muốn đọc nữa . Có lẽ tình tiết truyện quá ngắn và khô khan quá    . Về chất lượng của truyện cũng tạm ổn . Sách nhỏ gọn , giấy tốt , bìa đẹp mắt ưa nhìn nữa . Nên mình cho truyện bốn sao .
4
1027100
2016-07-08 16:07:47
--------------------------
449181
8431
Tự nhiên nghĩ đến sức sống bất diệt của cây cỏ dại, lên Tiki gõ thì thấy có truyện "Cỏ dại" luôn, nghĩ là sẽ nói về sức sống, ý chí vươn lên của nhân vật trong truyện, tham khảo nhận xét thấy đa phần là khen nên mua nhưng đọc rồi thì không được thỏa mãn. Là một quyển truyện dành cho thiếu nhi nên khá mỏng, mà mỏng thì câu chuyện cũng khá đơn giản. Những cô bé cậu bé ở quê nghèo, những nhu cầu đơn giản như ăn. mặc, đi học cũng là niềm khao khát của các em... rồi cũng có sự vượt qua khó khăn nhưng không như những gì mình đang tìm kiếm, không quá xuất sắc.
3
605606
2016-06-17 08:50:26
--------------------------
398824
8431
Truyện " Cổ đại" của nhà văn Hồng Thủy quả là một câu truyện rất hay và ý nghĩa. Câu truyện kể về tuổi thơ nghèo khổ của một cô bé vùng quê hết sức chân thực và sống đông. Câu truyện đẫ tái hiện cả một thời kì nghèo khổ của đất nước trên đà đi lên xây dựng. Có lẽ không ít người đọc đã không cầm nổi nước mắt trước câu truyện về tuổi thơ cô bé Hiên. Dẫu nghèo khổ,thiệt thòi những tâm hồn cô bé vẫn luôn sáng ngời vẻ đệp thật thơ ngây, trong sáng. Câu truyện hết sức nhẹ nhàng và sâu lắng này sẽ giúp người đọc tìm thấy một bến đậu bình yên cho tâm hồn sau nhịp sống hối hả của xã hội hiện đại...
5
1174527
2016-03-16 19:55:31
--------------------------
387168
8431
Quyển truyện đã mang đến cho mình một cảm giác rất là thoải mái, nhiều lúc đọc phải bật cười vì những lời nói vô tư của những cô cậu bé trong truyện. Truyện tuy không dài nhưng mang lại một sự giải trí cao và một ý nghĩa thật đẹp: đó là về tình bà cháu, tình bạn bè, tình cô trò. Đọc xong truyện tạo ra cho mình một cảm xúc nhẹ nhàng, bình yên như chính cảm xúc của câu chuyện mang lại. Cảm ơn tác giả đã mang đến cho người đọc một câu chuyện đầy tính nhân văn!!! Mong tác giả sẽ sáng tác thật nhiều câu chuyện hay như thế này nữa nhé !!!
5
1145723
2016-02-26 18:37:16
--------------------------
386451
8431
Vừa mới mua về là mình đọc ngay luôn, truyện hay quá, với cả ngoại thương Hiền, Hiền cũng thương ngoại. Không quá cầu kì, hoa mỹ, cứ chân chất thật thà thế này mà mình thích lắm. Giá khoảng 13000 không xứng với nó đâu, vì giá trị giáo dục của nó còn gấp 10 lần như thế. Có cái gì đó mộc mạc, giản dị, cách diễn đạt với cả cách dùng từ nữa, mình chưa bao giờ hối hận khi đặt niềm tin về cuốn sách này. Nếu có hơn 5 sao thì mức đánh giá của mình còn hơn thế nữa. Xin cảm ơn tác giả đã để lại một mẩu chuyện đáng yêu cho thiếu nhi nhé. Love all !
5
286362
2016-02-25 18:33:31
--------------------------
386309
8431
Bức tranh tươi sáng của lứa tuổi trẻ thơ mộng , trong sáng , mình cảm nhận được sư vui vẻ trong mắt người cô gái nhân vật chính ra sao khi cô phải chịu cảnh mồ côi , mất đi sự chăm sóc của người thân , cô sống tự lập , đơn giản hơn nhưng dễ hòa nhập với các bạn trẻ mới lớn , cỏ dại không quá dài dòng nhưng vẫn là câu truyện thân thiết với mọi người , miêu tả chậm rãi nhưng đi theo cảm xúc của tâm hồn học hỏi của tác giả việt .
4
402468
2016-02-25 14:27:34
--------------------------
249177
8431
Thực sự thì chuyện rất cảm động và rất hay về một số phận trẻ thơ khá bất hạnh khi em không cha không mẹ,chỉ còn lại duy nhất người bà.Truyện cho ta thấy nghị lực,nỗi đau và vô số điều khác khiến chúng ta cảm thông hơn về những số phận bất hạnh,nhất là trẻ nhỏ.Đây là một truyện tốt cho trẻ nhỏ đọc để có thể tập cách sống yêu thương và sẻ chia ,cảm thông với những bạn nhỏ như mình.Sau đó đọng lại trong chúng ta những bài học quý giá về cuộc sống và số phận !
5
337083
2015-07-31 14:11:16
--------------------------
