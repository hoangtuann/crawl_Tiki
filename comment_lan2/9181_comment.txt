223472
9181
Đầu tiên ấn tượng ở quyển sách này là tên của người viết sách, cô Cẩm Tuyết. Mình biết cô qua nhiều tập nấu ăn trên tivi nên thấy cuốn sách này là mua ngay. Phải nói là bìa sách nhìn dễ thương, khá lạ và gồm các món ăn ngon, khá dễ làm nhưng đòi hỏi người nấu phải bỏ công và tỉ mỉ.
Sách gồm các món ăn thuần túy Việt Nam: bánh xèo, bánh da lợn, bánh bèo, bánh bột lọc,.... Cách thức làm chi tiết, dễ hiểu, hình ảnh đẹp. Những ai yêu thích các món bánh truyền thống của nước mình thì đừng bỏ qua cuốn này. 
4
670267
2015-07-07 09:50:35
--------------------------
