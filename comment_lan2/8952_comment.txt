340208
8952
Quyển sách này viết khá dễ hiểu, câu hỏi và câu trả lời theo tình huống cụ thể, chia theo các lĩnh vực của cuộc sống. Tuy nhiên phù hợp với những người có kiến thức cơ bản về phong thủy (tính quái số, góc phong thủy, ..v.v..) Mình mua cho mẹ nghiên cứu lúc rảnh rỗi, nhưng chắc phải mua thêm một quyển phong thủy cơ bản nữa thì mới hiểu thêm để thực hành phong thủy trong bài trí nhà cửa được. Nói chung quyển sách này phù hợp với các bạn đã có kiến thức sơ bộ về phong thủy. 
3
16310
2015-11-19 13:46:55
--------------------------
