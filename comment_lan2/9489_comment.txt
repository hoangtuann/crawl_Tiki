476265
9489
Mình được bạn tặng cho quyển sách này khi vừa vào công ty mới. Sách cung cấp cái nhìn tổng thể đi dần đến chi tiết cho việc lên kế hoạch cho một chiến dịch PR hiệu quả. Tác giả khái quát các đầu việc và giải thích rõ việc cần thiết của từng việc. Cách thức và sự cần thiết của từng bước, từng loại chiến dịch khác nhau được phân tích kỹ càng không chỉ về mặt lý thuyết mà còn của cả những chiến dịch thực tế sinh động và dễ hiểu. Đây xứng đáng là một trong những quyển sách gối đầu cho những ai làm việc trong lĩnh vực PR.
4
203171
2016-07-19 15:06:57
--------------------------
408248
9489
Đây có lẽ là cuốn sách gối đầu giường của những ai đang làm việc trong lĩnh vực PR. Nó giống như một cuốn từ điển bách khoa PR chứa đựng vô số thông tin cần thiết. Tuy nhiên, cách trình bày của tác giả khá "hàn lâm" nên cũng hơi khó hiểu đối với các bạn newbie. Hơn nữa cuốn sách được minh họa không đẹp và mình lại 1 lần nữa phải nhờ đến sự cứu viện của cover sách xuân của Tiki :)). Dù sao, tóm lại, đây vẫn là một quyển sách hay và rất hữu ích.
4
1016856
2016-03-31 13:19:38
--------------------------
55870
9489
Những sản phẩm của First New luôn tạo được sự hài lòng nơi người đọc. Và cuốn sách này cũng không ngoại lệ. khi đọc cuốn sách này, bạn sẽ nắm bắt rõ và chi tiết hơn về ngành nghề mới mẻ trong cuộc sống này - PR. Sách nêu đầy đủ và rõ ràng những khía cạnh mà ta cần biết khi tiếp xúc với nghề này. Một nghề mới, nhưng nếu hiểu và dám khám phá nó thì ta ắt hẳn sẽ đạt được thành công. Cuốn sách này tiện lợi cho những ai mong muốn trở thành PR nhưng vẫn còn đang mắc kẹt giữa bộn bề suy nghĩ. Cuốn sách này rất cần cho những ai muốn tìm hiểu nhiều và cặn kẽ hơn về PR. Chắc chắn rằng khi cầm trên tay quyển sách này, bạn đã nắm chắc trong tay chiếc chìa khóa thành công cho cánh cửa nghề nghiệp đầu tiên trong đời bạn...
3
36331
2013-01-17 19:32:39
--------------------------
