408846
9470
Tớ mua cô bé Heidi vì vừa xem qua bộ phim hoạt hình này, nội dung phim vô cùng đáng yêu nên tớ quyết định tậu một cuốn truyện nữa, nhưng lúc tìm trên tiki lại tình cờ thấy cuốn này, vừa muốn có sách lại vừa muốn tự trau dồi thêm khả năng tiếng Anh nên tớ đã quyết định chọn nó.
 Nội dung truyện không chi tiết bằng phim hoạt hình nhưng với giá tiền và chất lượng sách thì lại đáng được 5 sao, sách có thể dùng chung với bút chấm đọc, còn có cả đĩa với giọng đọc địa phương, mỗi trang đều cung cấp từ vựng và câu khóa. Sau mỗi trang lại có bài tập để luyện kĩ năng đọc hiểu. Sau mỗi chap lại có một bài text nho nhỏ.
 Nói chung đây là quyển sách giúp ích rất nhiều cho việc học tiếng anh.
5
879184
2016-04-01 13:25:16
--------------------------
403694
9470
Tôi đã mua “Happy Reader - Cô Bé Trên Núi Cao - Kèm 1 CD“ trên tiki về cho con gái của tôi để vừa tập dịch sách tiếng anh và nghe tiếng anh vào buổi tối trước khi đi ngủ. Bé nhà tôi rất thích thú với “Happy Reader - Cô Bé Trên Núi Cao“. Cuốn sách này đã giúp con gái của tôi được nâng cao trình độ nghe và đọc hiểu tiếng Anh. Giọng đọc trong CD chuẩn, lại có âm nhạc nhẹ nhàng phù hợp với ngữ cảnh nên bé nhà tôi, tối nào cũng thích nghe. Với những từ mới có giải thích ở phần cuối mỗi trang sách đã giúp con tôi dễ dàng đọc hiểu các đoạn văn trong sách. Đây là cuốn sách phụ huynh nên mua cho con của mình học.

5
730635
2016-03-24 07:43:34
--------------------------
314973
9470
Cảm nhận của tôi khi nhận sách về là sách khá là nhỏ gọn. Tôi cũng đã đọc Heidi phiên bản tiểu thuyết nên khi đọc sách này tôi thấy khá bất ngờ khi tác giả tóm tắt nội dung ngắn gọn, khiến cho 1 quyển sách mấy trăm trang thành quyển sách 96 trang mà không bị mất nội dung. Rất dễ để có thể củng cố từ vựng của bạn khi đọc sách vì dưới mỗi trang sách tác giả đều ghi từ vựng ra và giải nghĩa cho bạn. Nếu bạn không biết cách đọc thì cái CD tặng kèm cùng sách rất hữu dụng với giọng văn rất chuẩn. Tôi còn cực kì thích những hình vẽ được minh họa rất sinh động trong truyện nữa. Mong các bạn thấy nhận xét của tôi có ích.
5
413619
2015-09-27 21:07:47
--------------------------
298211
9470
Đây là 1 trong những cuốn sách siêu yêu thích và gắn liền với tuổi thơ của mình. Thích đến nỗi hè nào cũng đăng kí thẻ thư viện để mượn đọc lui đọc tới.
Bây giờ lớn rồi, có điều kiện tìm mua thì lại ko tìm thấy bản tiếng Việt kiểu truyện tranh khổ lớn nữa. Nên mua bản tiếng Anh này.
Cuốn này đã được dựng phim hoạt hình, nhưng vì sợ mất đi tình yêu và cảm xúc với sách, nên ko xem, và chắc chắn sẽ ko bao giờ xem, dù có đọc đi đọc lại và tự tưởng tượng hàng trăm lần. hihi
5
418630
2015-09-12 15:33:01
--------------------------
49170
9470
"Happy reader" là tuyển tập sách được First News xuất bản nhằm đem đến cho bạn đọc những tác phẩm văn học kinh điển bằng tiếng anh. Tiêu biểu trong số đó là "Heidi- Cô bé trên núi cao" . Trong cuốn sách này, bạn sẽ được cung cấp phần từ vựng ở dưới mỗi trang, và hơn hết là có 1 CD kèm theo. Sau khi đọc xong, bạn có thể nghe CD lại 1 lần nữa để rèn luyện kĩ năng nghe cũng như cách phát âm ( cách kể chuyện lẫn giọng đọc đều rất hay và có cảm xúc ! ). Với cách học tiếng Anh như vậy, phần Đọc - hiểu ko còn nhàm chán nữa.
Một điều tuy đơn giản nhưng thể hiện được sự quan tâm của nhà xuất bản đối với bạn đọc trẻ, đó chính là bộ sách này được chia theo từng cấp độ ( từ grade 1 đến grade 5 ) giúp bạn có thể dễ dàng lựa chọn cuốn sách phù hợp với khả năng của mình. Ngoài "Heidi- Cô bé trên núi cao " ra, Bạn còn có các tác phẩm thú vị khác như " The old man and the sea" hay " Beauty and the beast" ... Thật sự mình không thể tìm được dù chỉ 1 điểm trừ ở đây, vậy nên 5 sao là lựa chọn cuối cùng  của mình^^
5
68435
2012-12-05 20:15:09
--------------------------
