378785
9437
Mình rất thích nhà đầu tư này nhưng cuốn sách này không làm mình hài lòng lắm. Warrent Buffett là một nhà đầu tư tài năng và thành công nhưng cuốn sách viết về cách đầu tư của ông thì chưa chắc đã được như thế. Không biết do phần dịch chưa tốt hay cuốn sách viết chưa tốt nhưng mình thấy cuốn sách này không được hay, không liền mạch và không có một ý tưởng chủ đạo nào, khá là lan man và không trọng tâm, qua cuốn sách mình cũng chưa rút được điểm nào giống nhau giữa việc đầu tư của Bufett và một cô gái và cái hay của việc đó, nếu học tập cách đầu tư điển hình của một cô gái là giày và quần áo nữa thì chắc kết quả càng tệ.
2
306081
2016-02-07 15:00:06
--------------------------
227353
9437
Cuốn sách, dường như đã thể hiện được cho người đọc bí quyết thành công của Warren Buffett. Cuốn sách hay dành cho những ai muốn và đang đầu tư chứng khoán. Cuốn sách chỉ cho bạn biết cách làm chủ "khí chất", điều khiển tình cảm và cảm xúc đầu tư của  mình. Ở trong một thị trường đầy biến động và khó lường, bạn sẽ làm gì khi cổ phiếu bạn vừa mua thì lại rớt giá?Bạn sẽ làm gì khi đang nắm giữ một cổ phiếu bạn đánh giá tốt nhưng lại rớt giá không phanh trên thị trường chứng khoán?Hay bạn vừa cắt một cổ phiếu vì đánh giá cổ phiếu đó không tốt nhưng lại bắt đầu lên vùn vụt?...Chỉ có sự phân tích để am hiểu thấu đáo về cổ phiếu, tính kỷ luật và làm chủ khí chất, cảm xúc bản thân , mới giữ cho bạn bình tĩnh ra được những quyết định chính xác...Tất cả điều này, đều được thể hiện trong sách...
5
39519
2015-07-14 04:38:51
--------------------------
141222
9437
Thành công của Warren Buffett là ông đã biết kiểm soát sự tự tin thái quá, tập trung vào mặt tích cực của sự bi quan, phớt lờ áp lực xung quanh, coi trọng con người và các mối quan hệ, hành động công bằng và có đạo đức…Điều thú vị, theo Lofton thì Warren Buffett luôn ý thức học tập các mặt tích cực của phái nữ như thái độ tôn trọng lời hứa, khả năng lập kế hoạch, khả năng giao tiếp… Hơn nữa, phụ nữ còn dành nhiều thời gian để nghiên cứu các lựa chọn đầu tư nên giúp họ tránh được những thất bại không đáng có. Họ cũng là những người sẵn sàng tìm kiếm những thông tin ngược với giả định ban đầu để cân nhắc kỹ lưỡng hơn trước khi mua cổ phiếu của bất kỳ công ty nào. Họ cũng ít bị tác động bởi những người xung quanh, do vậy họ luôn kiên định và có những quyết định đầu tư khách quan, sáng suốt hơn đàn ông.
Warren Buffett cho rằng phụ nữ như thế nhưng không hẳn ai cũng vậy. :))).Nhưng dù sao cũng không thể phủ nhận được thành công to lớn của ông khi vận dụng những thông tin đó. Không người nào có khả năng chọn cổ phiếu và đầu tư vào công ty có khả năng sinh lời và tồn tại lâu dài như ông. . Và ai thực hiện được những điều ông nêu ra chắc chắn sẽ trở nên giàu có từ thị trường chứng khoán.
4
381173
2014-12-14 15:31:27
--------------------------
