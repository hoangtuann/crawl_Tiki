464775
8571
Đúng là hiện nay, hằng ngày chúng ta tiếp cận rất nhiều thông tin về vũ trụ, từ tin tức truyền hình, báo đài đến phim ảnh, truyện tranh... Những thông tin về vũ trụ luôn hấp dẫn bởi vũ trụ lúc nào cũng bao la và đầy bí ẩn. Một quyển sách chứa đựng những thông tin chi tiết về từng ngóc ngách của vũ trụ, từ trời, đất, sao, trăng, đến người ngoài hành tinh, ufo... Thiết nghĩ bộ sách này nên được giới thiệu nhiều hơn để đông đảo bạn đọc từ trẻ đến lớn đều có thể đọc để bổ sung kiến thức quý giá cho mình.
4
460351
2016-06-30 14:39:28
--------------------------
442225
8571
Mình rất là thích khám phá nhất là mảng thiên văn học. Đây thực sự là một cuốn sách làm mình vừa ý. Sách được thiết kế rất đẹp, bìa đẹp với những trang màu và những hình ảnh minh hoạ sinh động giúp người đọc hứng thú với việc đọc hơn. Sách cung cấp cho học sinh những kiến thức về vũ trụ rất là bổ ích từ những hành tinh, vì sao, trái đất đến người ngoài hành tinh, UFO . Sách đúng là một cuốn sách thật sự bổ ích, đáng để mua và đọc. Giá tiền cũng rất phù hợp.
5
1351512
2016-06-04 17:53:05
--------------------------
399420
8571
" Khám phá bí ẩn thế giới tự nhiên về Vũ trụ" là một cuốn sách vô cùng tuyệt vời. Sách được viết cho lứa tuổi học sinh rất dễ hiểu, hình ảnh trực quan, sinh đông. Cuốn sách sẽ giúp người đọc giải đáp những bí ẩn của vũ trụ bao la. Vũ trụ kì bí ấy có bao điều chúng ta chưa từng biết tới và ai cũng muốn được khám phá. Cuốn sách đã cung cấp cho người đọc một lượng kiến thức lớn về những vì sao trong hệ mặt trời, về những bí ẩn của Trái Đất, về những chuyến thám hiểm không gian kì thú và cả những điều kì bí về người ngoài hành tinh và UFO. Đây quả là một cuốn sách thú vị.
5
1174527
2016-03-17 19:11:19
--------------------------
347502
8571
Mình vốn rất đam mê thiên văn văn học nên mỗi lần mua sách là mình phải mua ít nhất 1 cuốn về thiên văn .Cuốn sách này không có nhiều kiến thức chuyên sâu mà chỉ dừng lại ở mức độ tìm hiểu sơ lược nên rất thích hợp với ai 'thích' thiên văn học .tuy vậy ưu điểm của cuốn sách là độ mở nhiều ,hình ảnh minh họa phong phú ,bắt mắt,chất lượng giấy khá,giá cũng phù hợp túi tiền.Nói chung cuốn sách này là một tài liệu khá thú vị và hấp dẫn cho những ai thích thiên văn học.
4
543894
2015-12-04 15:41:10
--------------------------
346027
8571
Ai muốn trở thành nhà du hành vũ trụ? Ai muốn khám phá dải ngân hà và hệ mặt trời ? Thì hãy nhanh tay chộp lấy bí kíp này thôi. Minh hoạ rõ nét, hình ảnh sống động, chữ viết in rõ ràng, nội dung đầy đủ. Ai cầm cuốn sách trên tay cũng sẽ cảm thấy thích thú và muốn khám phá liền. Tuy nhiên thì có 1 số lỗi mình cũng đồng ý với 1 số bạn đã phát hiện ra, nhưng không sao đó chỉ là lỗi nhỏ, và nêú các bạn nhận ra thì có nghĩa là các bạn có tố chất của 1 nhà thiên văn rồi đó. 
5
754539
2015-12-01 19:14:36
--------------------------
343249
8571
Không mang tính chất học thuật hàn lâm, cuốn sách viết rất dễ hiểu, nội dung kiến thức được khái quát khá cô đọng khiến người đọc có thể dễ dàng tiếp thu. Thêm vào đó, có thêm tranh ảnh màu minh họa tạo thêm sự hứng thú cho người đọc, hình dung được phần nào về thế giới quan vũ trụ.
Tuy nhiên, trong cuốn sách vẫn còn mắc một số lỗi cơ bản về kỹ thuật đánh mấy, như trường hợp có sao Mộc lại nhầm thành sao Thủy, điều đó sẽ khiến đối tượng trẻ em khi đọc rất dễ nhầm lẫn..
4
405107
2015-11-26 03:58:50
--------------------------
343248
8571
Không mang tính chất học thuật hàn lâm, cuốn sách viết rất dễ hiểu, nội dung kiến thức được khái quát khá cô đọng khiến người đọc có thể dễ dàng tiếp thu. Thêm vào đó, có thêm tranh ảnh màu minh họa tạo thêm sự hứng thú cho người đọc, hình dung được phần nào về thế giới quan vũ trụ.
Tuy nhiên, trong cuốn sách vẫn còn mắc một số lỗi cơ bản về kỹ thuật đánh mấy, như trường hợp có sao Mộc lại nhầm thành sao Thủy, điều đó sẽ khiến đối tượng trẻ em khi đọc rất dễ nhầm lẫn..
4
405107
2015-11-26 03:57:50
--------------------------
239238
8571
Đề tài về thiên văn, vũ trụ luôn có sức hút đối với tớ, cũng nhờ cuốn sách này mà tớ đã biết nhiều hơn về thế giới mà ta đang sống, trong vũ trụ này tồn tại những điều dường như khó tin đối với chúng ta nhưng đó là sự thật, vũ trụ là nơi tồn tại những diều thần bí mà con người chưa thể lí giải được. Đây là cuốn sách về đề tài vũ trụ đầu tiên trong kệ sách của tớ, cuốn sách cũng khá thú vị, hình ảnh minh họa rõ ràng, đẹp, chất lượng giấy cũng rất ổn. Nói chung là tớ rất hài lòng về cuốn sách này
5
477877
2015-07-23 19:44:28
--------------------------
169527
8571
Mình vốn rất mê thiên văn. Vũ trụ luôn có sức hút lớn đối với mình. Muốn tìm hiểu thiên văn nên trên kệ sách nhà mình cũng không ít sách về đê tài này. Riêng quyển này mình thấy cũng khá ổn, rất nhiều kiến thức. Có nhiều ảnh minh họa, màu sắc đẹp, chi tiết. Nhưng mình vẫn không thích cách diễn đạt, không thu hút, hơi khô khan. Nhìn vào quá nhiều chữ, đọc một lúc thấy làm biếng đọc nữa. Nhưng nhìn chung cũng là một quyển sách hay, đáng đọc, dàng cho những ai mê thiên văn.
4
303016
2015-03-18 14:17:07
--------------------------
122694
8571
Cuốn sách rất hữu ích, có rất nhiều thông tin mà chúng ta không đơn giản để tìm được. Nhưng điều đó đã được ghi lại rất tỉ mỉ, đầy đủ, dễ đọc và có nhiều hình ảnh sống động. Những điều liên quan tới vũ trụ đã được tổng hợp và giải thích rõ ràng. Hạn chế của cuốn sách này là từ ngữ không thú vị cho lắm, không hấp dẫn và không có những mục phụ để tạo giao diện đẹp và sự thích thú cho người đọc. Tuy vậy cuốn sách vẫn rất đáng đọc. Mình rất hài lòng và mong đợt xuất bản tới cuốn sách sẽ hoàn hảo và được đón nhận nhiều hơn.
5
393797
2014-08-27 12:43:09
--------------------------
