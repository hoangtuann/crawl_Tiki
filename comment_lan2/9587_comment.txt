565461
9587
nếu ai có ý định bắt đầu học Toeic thì nên mua cuốn này. phần nghe và phần bài tập khá dễ có lời giải cho từng phần nghe và đáp án cho phần bài tập rất chi tiết. Đĩa nghe khá rõ. Nó cải thiện cho mình rất nhiều về phần nghe va cả phần ngữ pháp
5
1648355
2017-04-05 20:50:35
--------------------------
469515
9587
Cảm ơn tiki vì giá sách rất rẻ, mình xem hiệu sách mà giá 118 ngàn nhưng khi lên tiki xem chỉ có 88 ngàn nên mình đặt luôn. Nội dung sách thì khỏi bàn vì mình đã xem rất kỹ rồi mới đặt mua để về học. Đây là một cuốn sách khá hữu ích cho người học ôn toeic như mình. Hy vọng tiki sẽ có nhiều chương trình giảm giá nhiều hơn nữa để sinh viên bọn mình được tiết kiệm tiền cũng như có nhiều sự lựa chọn. Chúc tiki luôn phát triển và có nhiều khách hàng trung thành.
5
876109
2016-07-06 15:25:46
--------------------------
458943
9587
Quyển sách này rất được, thích hợp cho những bạn bắt đầu luyện thi TOEIC
các kiến thức trong sách này khá cơ bản, giúp củng cố lại kiến thức và có thể tự luyện tại nhà.
sách có chất lượng giấy tốt, mịn và dày; bố cục trình bày rõ ràng, dễ hiểu, bao quát được đề thi Toeic, học thấy rất là thích. có phần mini test để luyện tập.
Và có đính kèm CD, tiện lợi cho việc luyện nghe.
Mình mua lúc tiki giảm giá còn 80k, thiệt là may. Tiki đóng gói cẩn thận. Sẽ ủng hộ Tiki tiếp =))
5
476704
2016-06-25 17:01:32
--------------------------
422141
9587
Cuối cấp 3, vì học lệch môn nên tôi đã mất gốc tiếng anh. Đến giờ năm thứ 3 rồi, tôi mới tá hỏa cấp tốc. may mắn lần mò thế nào tìm được quyển này. Tôi đặt mua vì thấy những lời nhận xét rất tích cực từ những bạn đọc khác. Cũng chuẩn bị tâm lý sẵn sàng nếu như có không như giới thiệu. 
Nhưng không, cuốn sách cuốn hút tôi ngay từ những trang đầu tiên. Tôi thích cách trình bày nội dung bài học. Bên trái là nội dung bài học, bài tập ta tự làm. Đáp án được in ngược ở trang bên phải. Thật tiện lợi cho việc kiểm tra đáp án.
Rất mong tác giả có những quyển sách hay hơn nữa và Tiki sẽ giới thiệu thật nhiều cuốn sách bổ ích như thế này tới bạn đọc. Cảm ơn!
5
847848
2016-04-26 16:06:15
--------------------------
419971
9587
Nghe đến kì thi TOEIC em thấy rất hay, vì qua kì thi này em có thể nâng cao kiến thức cho mình. Nhưng thực sự thì em lại không biết bắt đầu học ở đâu . Nghe đứa bạn nói trên Tiki có bán cuốn Very Easy TOEIC cho người bắt đầu em mừng lắm. Mua được sách em rất hài lòng, nó được bọc ngoài bằng nilon chắc chắc, nội dung bên trong phong phú, sát với TOEIC, cuối sách còn có 2 cd để nghe, giá cũng hợp lí nữa, chỉ 88k,rẻ hơn ngoài hiệu sách. Sách này khá dày, chất lượng giấy ổn. Rất hài lòng với hàng của Tiki, cám ơn Tiki nhiều.
5
1284706
2016-04-22 12:41:30
--------------------------
400768
9587
Nghe 1 người bạn giới thiệu về quyển sách này nên về tìm trên tiki để mua. Ấn tượng đầu tiên là sách được in đẹp, chữ rõ ràng, không bị nhòe hay in lỗi, giấy in cũng đẹp, sách có đầy đủ các dạng bài trong đề thi toeic, nội dung của mỗi bài được trình bày chi tiết, dễ hiểu. Rất bổ ích cho những người luyện thi toeic ở nhà như mình. Và sách có đáp án và cách giải nên rất dễ học. 2 đĩa CD kèm theo chất lượng tốt, giọng nói dễ nghe, giúp cải thiệt được kỹ năng nghe. Quyển sách rất đáng mua.
5
497682
2016-03-19 20:57:57
--------------------------
400495
9587
Mình mua cuốn sách này với cùng với cuốn starter, giao hàng của tiki công nhận siêu nhanh luôn. Cầm sách trên tay mà sung sướng quá vì sách được bọc ni lông bên ngoài cẩn thận, đĩa thì được kẹp sau sách, bìa sách cứng cáp, giấy và chữ rất đẹp rõ ràng. Về nội dung cuốn này thì rất chi tiết, chia ra các đề mục ngữ pháp, bài tập, từ vựng, có đủ các phần trong toeic, hình ảnh phục vụ cho việc nghe rất rõ ràng. Tiki nên bán thêm nhiều cuốn sách như thế này.
3
652998
2016-03-19 13:45:12
--------------------------
382857
9587
Tiki giao hàng đúng hẹn, đóng gói kĩ lưỡng nên mình rất yên tâm khi mua hàng ở đây
Điểm cộng :
- Giá tiền vừa tầm so với những sách luyện TOEIC khác, phù hợp với học sinh, sinh viên. 
- Luyện kĩ năng nghe, đọc rất tốt. Học thêm được nhiều từ vựng phổ biến trong đề thi TOEIC, ôn lại các cấu trúc ngữ pháp quan trọng và thường gặp. Đĩa CD tặng kèm rất giúp ích cho listening skill của các bạn, giúp làm quen với phần thi nghe thực tế
- Trình bày sách rõ ràng logic, chữ in rõ không bị lỗi gì
Điểm trừ: không có
5
843972
2016-02-19 20:15:07
--------------------------
341016
9587
Mình đang cần ôn thi toeic mà chưa biết bắt đầu từ đâu. Thấy quyển này được mọi người nhận xét khá tốt nên mua về. Nói về chất lượng bề ngoài thì  mình thấy cực ổn luôn ý, chất lượng giấy tốt, đĩa nghe cũng tốt, ko bị vấp! Chất lượng bên trong thì mình thấy quyển sách khá bổ ích, phục vụ tốt cho những bạn muốn thi toeic mà chưa biết nên bắt đầu từ đâu, hoặc ko muốn phải đến các trung tâm học như mình. Những trang đầu vừa đưa ra dạng đề thi toeic, tips, và bẫy lỗi trong đề giúp mình rất nhiều trong các bài test ở tường. Nói chung là ấn tượng khá là tốt về quyển sách này. :)
5
624376
2015-11-20 23:44:47
--------------------------
324013
9587
Mình mua quyển này để ôn nâng cao kiến thức , để còn ra trường. Sách giấy tốt, chất lượng sách rất tốt. Mấy trang đầu còn có bí quyết làm bài và các cái bẫy trong đề thi, 1 trang tiếng việt, còn 1 trang tiếng anh, đọc rất dễ hiểu. Các trang in chữ và hình ảnh rất hợp lí về bố cục. Phần reading có đáp án ngay trang bài tập, nhưng đáp án viết chữ ngược lại so với sách, rất tiện lợi, không phải mở xuống mấy trang cuối sách. :). Mình mong mình sẽ học tập thật tốt với quyển sách này.
5
468807
2015-10-20 09:48:46
--------------------------
308614
9587
Đây là quyển sách cơ bản nhất trong bộ sách dùng cho người muốn luyện thi toeic, trình bày rõ ràng dễ hiểu .Người hcọ dễ dàng nắm bắt được cấu trúc này để học tốt .Chất  lượng sách thì không thể chê được ,bìa rất sang và chất lượng giấy dày và mịn lắm .Với hình thức xoay ngược đáp án trả lời người học dễ dàng nắm bắt được trình độ của mình khi làm xong bài có thể xoay lại để biết đáp án cho câu hỏi .Sách đi kèm Cd rất tiện lợi cho người muốn cải thiện khả năng nghe .Tất cả đề dễ dàng và bổ ích như tên gọi của quyển sách.
5
337888
2015-09-18 19:53:18
--------------------------
305070
9587
Cuốn sách là một động lực cho những bạn mới bắt đầu luyện thi Toeic. Nó gồm những bài học rất cơ bản cho mỗi kỹ năng trong bài thi Toeic. Nội dung của mỗi bài học được trình bày rất chi tiết, dễ hiểu giúp đỡ rất nhiều cho những bạn tự luyện thi ở nhà. Sách hỗ trợ đáp án và hướng dẫn giải nên giúp các bạn dễ hiểu hơn rất nhiều. Bên cạnh đó, đĩa CD được kèm theo cũng hỗ trợ cho người học cải thiện kĩ năng nghe cơ bản của bài thi Toeic. Sách rất phù hợp với những bạn lần đầu tiếp xúc với dạng đề Toeic
5
74657
2015-09-16 20:25:59
--------------------------
303511
9587
Mình đã bắt đầu học Toeic với quyển  Very Easy TOEIC mua ở Tiki này.
Mình rất hài lòng vì Tiki luôn gói hàng rất an toàn, sách nhanh đến tay sau khi đặt hàng, nhân viên giao hàng vui vẻ nữa.
Very Easy Toeic rất căn bản, giúp tìm hiểu ban đầu, tránh bỡ ngỡ gây nản cho người đang ôn luyện. 1 quyển sách kèm với đĩa nghe rõ ràng, nội dung minh hoạ trong sách cũng rõ nét.
 Bìa và giấy  quyển sách này khá tốt khá tốt, y như sách có bán trong mấy nhà sách mà giá thì rẻ hơn nhiều nên rất thuận lợi cho sinh viên như tụi mình.

4
403665
2015-09-15 21:22:25
--------------------------
295305
9587
Có thể nói "Very Easy Toeic" là cuốn sách bắt đầu tốt nhất cho những ai muốn tham gia vào kỳ thi Toeic.  Đúng như tên gọi, cuốn sách này thực sự rất dễ, nó tập trung các kiến thức cơ bản nhất của các kỹ năng. Mỗi bài học đều được viết rõ ràng và chi tiết, rất thuận lợi cho người đọc tự học, nghiên cứu. Sách còn có đĩa CD kèm theo để bạn học nghe dễ dàng hơn.
Sách khổ to, rất xịn, chắc chắn làm cho mình có nhiều cảm hứng để đọc. Đặc biệt là mua ở TIKI còn được nhân triết khấu và được bên TIKI bọc kiếng cẩn thận nữa nên càng vui.
5
145385
2015-09-10 09:31:17
--------------------------
294257
9587
Toeic được xem là 1 cứu cánh giúp các bạn bắt đầu với hành trình luyện tiếng anh nâng cao của mình, và ngày nay thì Toeic được xem là chuẩn đầu vào và đầu ra của rất nhiều trường Đại học lớn trong cả nước như Đại học Ngoại Thương, Đại học Kinh Tế, Đại học Bách khoa. Quyển sách là 1 người bạn thiết thực dành cho những ai mong muốn cải thiện khả năng tiếng anh của mình, đồng thời đặt nền tảng vững chắc cho phần ngữ pháp và luyện nghe. Bố cục quyển sách chia ra làm 12 Units, mỗi Units được thiết kế theo Format đề thi Toeic hiện tại, lần lượt từ dễ đến khó, rất phù hợp cho các bạn muốn luyện nghe ở nhà để nâng cao trình độ và tự luyện tập thêm. Phần Reading được thiết kế với các điểm ngữ pháp được giải thích rất rõ ràng và chi tiết, kèm theo là bài tập ứng dụng giúp người học luyện tập và ghi nhớ. Các bài đọc được thiết kế với những chủ đề quen thuộc, gần gũi và đa dạng, không chỉ giúp khả năng đọc hiểu mà còn nâng cao kiến thức xã hội, làm quen với các chủ đề trong tiếng anh. Đây thực sự là một người bạn tuyệt vời dành cho những người bắt đầu làm quen với Toeic.
5
765212
2015-09-09 09:43:27
--------------------------
293808
9587
Rất cần thiết cho người luyện thi toiec, tự ôn thi tại nhà, mình mới vừa nhận được hàng, tiki làm ăn rất uy tín, giá thành cũng rất cạnh tranh có khi thấp hơn cả ngoài thị trường hay nhà sách. Mình rất hài lòng về dịch vụ hỗ trợ khách hàng. Mình sẽ tiếp tục mua sách ủng hộ cho tiki.vn, đặc biệt là dịch vụ chuyển hàng miễn phí trong cả nước. mọi người cũng nên mua về đọc tham khảo y rất cần thiết cho học đại học, sinh viên chuẩn bị đi du học hay đi xin việc làm sau này.
5
787890
2015-09-08 19:03:31
--------------------------
288535
9587
Sắp ra trường rồi nên mình cần bằng toeic cho việc đi làm sau này cũng như để xét chuẩn đầu ra của trường mình nữa. Mà mình thì lại không đủ điều kiện để đăng ký học ôn ở trung tâm nên quyết định tự học, và lên mua sách ở Tiki thôi. Sách kèm với đĩa rất đầy đủ, nội dung trình bày rõ ráng, bìa và giấy khá tốt, y như sách có bán trong mấy nhà sách mà giá thì rẻ hơn nhiều. Cuốn sách này giúp mình tự học tiếng anh khá tốt, nên là bạn nào cung đang tự học như mình thì hãy sắm cho mình và bắt đầu thôi!
4
477886
2015-09-03 17:19:32
--------------------------
239246
9587
Đối với mình bằng TOEIC rất cần thiết cho việc làm sau này. Nhưng lại không có nhiều thời gian trống và tiền bạc để có thể tham dự các lớp luyện thi TOEIC bên ngoài nên việc tự học đối mình rất quan trọng. Vì thế mình đã quyết định chọn mua cuốn sách này của tiki, và mình đã rất đúng đắn khi mua nó. Cuốn sách này thực sự là bổ ích cho những người muốn tự ôn luyện TOEIC từ đầu. Sách có hướng dẫn chi tiết nội dung thi, các phần đọc, ngữ pháp, nghe rất rõ ràng và chi tiết. Có cả đáp án cũng như hướng dẫn giải nên không khó để theo kịp
5
108769
2015-07-23 19:50:20
--------------------------
142238
9587
Là một đứa trẻ được bố mẹ cho tiếp xúc với ngôn ngữ Anh khá sớm nên tôi vốn đã rất thích nó. Từ khi quyết định mua bộ sản phẩm này, tôi thấy rất hài lòng. Từ bìa đến trang giấy bên trong tất cả rất tuyệt vời, cùng với 2 đĩa CD. Ít nhiều gì tôi cũng được trau luyện hằng ngày nhiều hơn vì cảm thấy hưng thú với sản phẩm. Về phía đĩa, không có gì để bình cả, đĩa không bị đứng mà lại nghe rất rõ. Nói chung, với sản phẩm này phải là điểm 10 cho chất lượng.
5
90025
2014-12-17 22:36:25
--------------------------
67897
9587
Mình là một người đam mê Tiếng Anh và muốn học lấy bằng Toeic nhưng chưa có thời gian đi học thế là quyết định mua cuốn sách này để tự học ở nhà. Và mình rất hài lòng về nó , bổ ích dễ hiểu , đem lại thêm nhiều kiến thức cao hơn về tiếng anh rất thích hợp cho những bạn nào muốn học thêm luyện Toiec như mình , nhất là những phần mini test giúp ôn lại kiến thức một cách hiệu quả , đặc biệt là sách có kèm theo đĩa CD giúp cho việc luyện nghe thêm bổ ích hơn , mình rất thích cuốn sách này và mong tiki đem đến nhiều sách có ích về ngoại ngữ hơn cho độc giả nhé . Chân thành cám ơn Tiki 
5
100974
2013-04-09 12:41:09
--------------------------
64720
9587
Quyển sách này rất hữu ích với bạn nào muốn học Tiếng anh. Mình thích nhất là kỹ năng nghe, lần nào thi vượt lv cũng ôn lại và nghe thêm Target, rất bổ ích cho những bạn nào kỹ năng nghe chưa tốt. Cả phần ngữ pháp dành cho những ai mới học Tiếng anh. Những bài mini test mình thường căn thời gian để làm giống như đi thi thật, có mấy lần đề TOIEC trường mình bốc vào mấy phần trong đấy, làm ngon ơ. Đây là một quyển sách rất hay, mong tác giả có nhiều quyển như thế.
5
66442
2013-03-21 16:02:59
--------------------------
