546929
9390
Truyện này mình đọc từ chương đầu đã thấy hay rùi , tình tiết khá buồn cười nhưng những lúc nữ chính chia tay với các nam phụ cũng rất buồn .  Lúc Quan Thượng Tình xuyên không về gặp 4 huynh đệ Âu Dương thật hài hước , những tình tiết gặp các mỹ nam phụ cũng thật dí dỏm . Truyện này làm mình đọc biết bao nhiêu lần cũng ko chán !!!!
5
1940078
2017-03-18 19:22:27
--------------------------
428353
9390
So với những câu chuyện xuyên không khác thì quyển sách này có thể nói là bình thường, đơn giản, không gây được hứng thú cho người xem. Mình đọc chừng nửa quyển tập 1 thì đã không còn hứng thú đọc tiếp nữa. Cốt truyện bình thường, không mới lạ, vẫn là motip nữ chính xuyên không, không nhan sắc, không tài năng nhưng lại có rất nhiều anh chàng đẹp trai, tài giỏi vây quanh. Tính cách và hành động của nhân vật có phần hơi ấu trĩ, không thực tế. Chuyện tình cảm của nhân vật cũng rất thiếu tự nhiện, mình chẳng hiểu nổi tại sao bốn anh em họ Âu Dương tính cách khác biệt nhau một trời một vực lại cùng thích một mẫu con gái như vậy. Có thể do mình là người thực tế, không hay mơ mộng nên cảm thấy rất thất vọng về quyển sách này.
1
19416
2016-05-11 07:26:32
--------------------------
374555
9390
Nếu có được nhiều thể loại tính cách thì truyện không đến nỗi có giai đoạn nhàm chán , màu mè làm cho mỗi cá nhân được nhìn nhận chưa thông qua những nhân vật chính biến bản thân cốt truyện không được hay hơn nhiều lúc mỗi người gặp phải đều phải bỏ qua thái độ sống vẫn còn khắc phục điểm yếu của cái cách bổ sung được tâm hồn người đọc qua những lời văn rất uyển chuyển đạt tới điều mà người đọc mong muốn , hài hước một phần qua cái nhìn thực dụng từ nhân vật phụ .
4
402468
2016-01-26 16:17:32
--------------------------
340515
9390
Cuốn này được bán ở hôi sách với giá giảm mạnh chỉ còn một nửa, cơ mà đọc xong thì đúng là phải ngậm ngùi thốt lên “đúng là tiền nào của nấy”, truyện như viết cho trẻ con đọc vậy, vừa nhạt, vừa nhàm, vừa nhảm, vừa phi logic. Nữ chính đã không tài giỏi cũng thôi đi, đằng này đầu óc cô nàng cứ như người giời, trong truyện tình cảm thì mù mờ không biết bản thân yêu anh nào, dàn nam trong truyện thì quá nhiều, mĩ nam nhan nhản như mây và anh nào cũng yêu cô nữ chính không cần lý do, càng đọc càng thấy chả ra sao cả.
1
630600
2015-11-20 01:51:22
--------------------------
340436
9390
Sự thật thì từ khi nhìn thấy bìa sách, mình đã quyết định chạy đi mua ngay 2 tập về để đọc. Nói một cách tổng quát thì ngôn ngữ trong truyện khá dễ thương. Nhưng motip thì cũ quá. Vẫn là bạn nữ chính xuyên không, đùng một cái được 8 nam chính thích. Dàn mỹ nam thì hoàn hảo không tỳ vết, gia thế hoành tráng, nhan sắc ngời ngời vạn người mê. Còn nữ chính....thiết nghĩ nếu như nhan sắc của Thượng Quan Tình có thực sự đủ để làm xiêu lòng gần 10 nam chính thế kia thì tại sao trước đây không ai để ý đến nàng ấy? Theo mình, bạn tác giả còn quá non tay.
2
979350
2015-11-19 21:06:28
--------------------------
296190
9390
Mình là hủ nữ. và thể loại ngôn tình duy nhất mình đọc hiện giờ là xuyên không. truyện nào gắn mác xuyên không mình đều tìm hiểu. sau khi nhìn bìa và đọc giới thiệu truyện thì đã bỏ hơn 200 ngàn để mua. Và sau khi đọc thì quá thất vọng: quá nhiều nam chính và không biết ai là chính nhất, tình cảm của nữ chính thì không biết đường nào mà lần, thay đổi nhanh chóng, kết thúc nhưng không cảm nhận được tình yêu của Thượng Quan Tình với anh cả nhà Âu Dương, cách viết trẻ con thiếu logic, cũng không hiểu tại sao nữ chính có điểm gì tốt lại được nhiều anh vừa đẹp trai vừa tài giỏi yêu. Truyện thích hợp để giải trí khi buồn.
2
5348
2015-09-10 22:19:23
--------------------------
178996
9390
Thực sự truyện không được hay như phần giới thiệu. Thứ nhất là giọng văn cứng ngắc, nhiều chỗ hơi vô duyên, không biết là do người dịch hay do tác giả nữa. Thứ hai là không hiểu nổi tình cảm của nữ chính, yêu anh Yêu tinh kia nhanh và đơn giản quá, chỉ có một câu thừa nhận là yêu ảnh, thế là xong, không biết vì sao yêu nữa. Nhiều đoạn pha trò rất gượng, hài không thấy đâu, thấy cười không nổi. Lại thêm mô típ harem một dàn, quá trời anh theo đuổi nữ chính nữa.
2
57459
2015-04-06 10:59:24
--------------------------
152806
9390
Quyển sách này làm mình hơi thất vọng vì ấn tượng đầu với nó! Mình bị ấn tượng bởi thiết kế bìa nên quyết định đọc nó. Nhưng về nội dung thì phải nói thật là mình hơi thất vọng về nó. Cốt truyện không rõ ràng và có phần hơi nhàm chán. Tuyến nhân vật được xây dựng quá nhiều, tuy mỗi nhân vật là một nét riêng, một cá tính riêng nhưng vẫn không có nhân vật nam chính làm đại diện, làm nam chính trung tâm của truyện. Hơn nữa với những nam chính quá toàn vẹn thì nữ chính có phần hơi yếu thế, chưa tạo được ấn tượng cho mình!
Một quyển sách chưa tạo được sự đặc sắc về nội dung và mặt hình tương - ngoài yếu tố gây cười và thiết kế bìa là điểm cộng!
Mình đánh giá tác phẩm ở mức 2 sao cho bìa và tính hài hước của truyện!
2
303773
2015-01-24 12:01:08
--------------------------
152742
9390
Đọc truyện này, tôi cứ ngỡ như đang đọc một bộ  "hậu cung Thượng Quan truyện" - một vợ tám chồng vậy =.=. Tình tiết truyện rối rắm, chồng chéo, ức chế khó tả. Giữa dàn nam chính nam phụ hoàn hảo không tì vết kia, nữ chính Thượng Quan Tình với đầu óc lơ mơ ngu ngơ, ngây ngây ngô ngô,hồn nhiên quá mức cần thiết, suy nghĩ và hành xử như một kẻ ngốc, không có một tí nào gọi là “kiên định”, ngả bên nọ, nghiêng bên kia mà không biết cô ta thích ai. Truyện có tới tám chàng đẹp trai làm cho đầu óc tôi cứ xoay mòng mòng, nhân vật thì rõ nhiều mà ấn tượng để lại thì chẳng được bao nhiêu. Càng đọc càng thấy rối rắm tùm lum, đến giờ tôi vẫn không hiểu sao 8 anh chàng cứ lao vào cô nàng Thượng Quan Tình chẳng có gì đặc biệt kia, thật sự mệt mỏi và chán nản.
1
449880
2015-01-24 03:50:32
--------------------------
136969
9390
Đọc Phiêu du giang hồ xong mà tôi tức muốn hộc máu chả biết tác giả muốn truyền tải tới độc giả cái gì nữa . Lúc đầu khi quyết định đọc cuốn này là vì thấy bìa thiết kế khá dễ thương và đọc đoạn đầu cũng khá mới mẻ . Nhưng quả thật tác giả đã làm tôi thất vọng sau khi đọc xong . Có lẽ cái sai sót lớn nhất của truyện đó chính là xây dựng quá nhiều nhân vật . Điển hình ở đây là có quá nhiều nam chính đọc xong tập 1 rồi mà tôi vẫn còn chưa xác định được ai là chính ai là phụ . Mà anh nào anh nấy đều đẹp trai ngời ngời tài năng phong phú và tất cả đều phải lòng nữ chính . Phải nói tôi cảm thấy vô lý . Nữ chính đâu có tài giỏi hay xinh đẹp đến độ gặp vài lần anh nào cũng phải đổ gục . Nói chung truyện có nhiều tình tiết khá phi lý và thiếu muối
2
337423
2014-11-24 09:23:12
--------------------------
128751
9390
Bình thường, trong các truyện xuyên không, cổ trang, chị nữ chính có 3,4 anh theo đuổi là mình đã thấy bất mãn rồi, vậy mà truyện này còn có tới gần chục anh, người nào cũng ngời ngời chói sáng, hoàn hảo không tì vết, đến nỗi đọc mà chả biết đâu là nam chính, đâu là nam phụ. Bạn nữ chính Thượng Quan Tình thật là quá nhàm chán, không có một tí chủ kiến nào hết, ngây ngây ngô ngô, ngớ nga ngớ ngẩn, nói chung là ngoài may mắn ra thì chả thấy cô nàng này có gì hay ho mà cả 8 chàng mỹ nam như hoa như ngọc lại đâm đầu vào yêu dúi dụi. Cái kiểu nay ngả về bên này, mai nghiêng về bên khác của Thượng Quan Tình khiến mình ức chế kinh khủng, tự dưng có suy nghĩ… tám anh bỏ cô ta để đến với nhau đi còn hơn =___=
1
393748
2014-10-04 17:30:04
--------------------------
121675
9390
Lúc đầu đọc gặp tình tiết xuyên không thật sự mình rất thích, sau đó bỗng nhiên cô TQT thời cổ đại chết một cánh lãng nhách. Anh em nhà Âu Dương thì chấp nhận TQT mới một cách chớp nhoáng. Tình cảm của cô TQT này với 8 người con trai thì dở dở ương ương, lúc bảo thích người này, lúc lại bảo thích người kia, lúc thì trái tim rung động với người này, lúc thì lại thấy người kia thực hoàn hảo. Nếu không có tình tiết TQT bị trúng xuân dược, không biết cô nàng với 8 chàng trai này rốt cuộc chọn ai. Một dàn harem hoàn hảo không tỳ vết, cảm tưởng y như một loại ảo tưởng nào đó không hề có thật. Mặc dù ngôn tình là một loại ảo tưởng dành cho các cô nàng như mình, nhưng mà hai hay ba người gì đó thích là thấy cũng đủ, đây tới tận tám người mà người nào cũng hoàn hảo tựa nhau. Cuốn này đọc giết thời gian thì được, chứ nói tới tâm đắc thì thôi, cho mình xin.
2
13781
2014-08-20 20:45:50
--------------------------
104900
9390
Đọc vài chương đầu của quyển PHIÊU DU GIANG HỒ, tôi đảm bảo rằng người nào cũng sẽ bị cuốn hút vì mở màng của cuốn truyện khá lạ. Nhưng đọc một hồi tôi lại đảm bảo thêm một lần nữa là mọi người sẽ bị chị tác giả này làm cho loạn não vì không biết chị ta viết cái gì luôn. Chị ta tung ra một dàn mỹ nam các loại, khiến cho chúng ta ngụp lặn trong một đám trai đẹp đủ kiểu, rồi bối rối vô cùng vì không biết rốt cuộc cha nào là chính cha nào là phụ. Đến hết quyển 1 cũng chưa đoán được nữ chính thích ai (thấy ai nữ chính cũng thích) và có thể thành đôi với ai.
Dàn nhân vật tuy nhiều nhưng không rõ nét, đám trai này giống như là quân diễu hành đi ra đi vào mà chẳng để lại gì trong lòng đọc giả. Còn cái cô nữ chính thì múa may quay cuồng giữ đám đàn ông, mai bên người này, mốt bên người nọ, điên điên khùng khùng, dở dở ương ương chẳng đâu vào đâu.
Nội dung thì thất thường, đến lối hành văn cũng lên xuống không ổn định. Lúc thì dài dòng lê thê, lúc thì ngắn gọn, cô động, lúc lại dạt dào cảm xúc đến chảy nước, trên thì mới cảm xúc dâng trào vừa xúông dòng kế tiếp thì tưng tưng khó nuốt. Nói chung đọc quyển truyện này, Hạ Tiểu Mạt cho người đọc lên voi xuống chó đột ngột bất ngờ làm mạch cảm xúc vỡ vụn tè le toét loét.
2
180185
2014-01-20 01:06:47
--------------------------
104203
9390
Quyển sách từ cái tựa và chương đầu rất bắt mắt. Nhưng về sau văn phong của tác giả lại quá rời rạc, chỉ chú trọng miêu tả hoạt động mà không miêu tả nội tâm khiến tác phẩm (dù là ngôn tình) thiếu đi sự sâu sắc.

Thêm nữa, tuyến nhân vật quá ư là...kỳ quặc, không rõ tính cách, giống như là con rối bị điều khiển theo nữ chính (hay tác giả)...nói chung nhân vật nam chỉ có một màu: nghe lời bạn nữ chính!

Cuốn sách này rất tệ, nếu đọc để giết thời gian: xin hoan nghênh. 

Còn nếu bạn muốn tìm một tác phẩm hay để dành toàn bộ thời gian: xin xem xét lại.
1
92005
2014-01-09 15:39:36
--------------------------
101907
9390
Điều mình cảm thấy không hài lòng nhất là về các nhân vật nam trong truyện. Dĩ nhiên đã là tiểu thuyết thì nhân vật nào cũng hoàn hảo, về mặt này hoặc mặt khác, nên mẫu nhân vật nhàm nhàm như nhau cũng là điều dễ hiểu. Nhưng kiểu xây dựng nhân vật của tác giả thì quả thực là hơi quá gượng ép. Chị nữ chính thì cũng xinh đấy, cũng nỗ lực đấy, cũng dễ thương đấy, nhưng có đến mức mà phải có tới 5, 6 anh lao vào yêu như thế không? Mình biết nói thế này là hơi quá đáng, nhưng thực sự mình thắc mắc không hiểu sao tác giả không chuyển truyện này sang thể loại NP cho rồi. Đành rằng mấu chốt ở đây chỉ là tô điểm cho nhân vật nữ chính, nhưng tô điểm như thế thực sự hơi quá đà mất rồi. Nói chung, cốt truyện tác giả xây dựng cũng khá ổn, nhưng có quá nhiều nhân vật nam xoay quanh nữ chính, đầu tiên thì tưởng chị ấy thích Triều Lưu, lại còn cho hai người hội ngộ, xong rồi lại vì mấy lí do vớ vẩn mà quay sang thích Âu Dương Thiếu Nhân. Là độc giả thì thích những bí ẩn trong cuốn sách mà mình đọc, nhưng không thích cái kiểu nhập nhằng mà tới gần cuối truyện còn chưa biết nữ chính lấy anh nào. Thực sự thì truyện này chỉ phù hợp cho mấy bạn hủ ngồi mơ màng tưởng tượng xem anh nào sẽ yêu anh nào thôi, chứ chẳng phải là nữ chính sẽ yêu anh nào nữa...Quá nhiều chữ nhưng như thế dĩ nhiên khiến cuốn sách mất đi rất nhiều điểm hay. Một sao cho chất lượng in, một sao cho bìa, một sao cho sự hoàn hảo của các mĩ nam (thực ra thì tớ cũng là hủ mà!)
3
137364
2013-12-11 20:29:43
--------------------------
98134
9390
Nội dung không quá đặc sắc cũng không quá tệ hại. Lối viết tạm ổn nhưng chưa chi tiết và sâu sắc. Chỉ là có hứng thú với thể loại xuyên không nên mới mua về đọc.
Nếu tác giả trau chuốt cách viết của mình hơn thì có lẽ tác phẩm này sẽ ổn, có tính giải trí cao, giảm stress hữu hiệu. 

Nữ chính Thượng Quan Tình là hình tượng nhân vật dễ thấy trong ngôn tình. Xây dựng hình tượng nhân vật theo lối này vừa có ưu điểm lại vừa có khuyết điểm: Tuýp nhân vật dễ cho sự tiến triển của mạch truyện nhưng đồng thời cũng gây ra ít nhiều sự nhàm chán đối với một số người đọc. Nhưng có vẻ là việc xây dựng hình tượng nam cũng tạm bù đắp cho chút khuyết điểm này.

Nói chung, ai thích xuyên không nhẹ nhàng dễ đọc thì đây là một lựa chọn không tồi.
3
166566
2013-11-01 15:45:17
--------------------------
97951
9390
Thật ra thể loại xuyên không không còn mới lạ nữa, có rất nhiều truyện xuyên không khiến tôi mê mẫn như Ca tẫn đào hoa, Bộ Bộ kinh tâm. Mang một tâm trạng háo hức khi đọc Phiêu du giang hồ, tôi nghĩ có lẽ đây là một tác phẩm hài bởi cái bìa rất ngộ nghỉnh, nhí nhảnh. Thế nhưng, tôi lại thất vọng tràn trề. Tuyến nhân vật không có gì đặc biệt hay ấn tượng, từ nữ chính đến nam chính, thấy nhạt nhạt sao ấy. Còn bốn anh em Âu Dương thì thôi rồi, thực sự là mình bị rối bởi bốn anh em này, loạn hết cả lên. Mặc dù sau này đọc cẩn thận cũng nắm bắt được nhưng mà tình tiết truyện ngoài mục đích chọc cười có chủ ý, gượng gạo thì chẳng có gì cả. Cách viết của tác giả đôi lúc cảm thấy chưa chắc tay, không tự nhiên, khiến cho người đọc tôi đây cảm thấy bị ức chế và bực mình.
3
63376
2013-10-30 10:31:13
--------------------------
97524
9390
Mình rất thích đọc tiểu thuyết Trung Quốc thể loại xuyên không. Vì vậy khi thấy truyên xuất bản mình lập tức mua về đọc ngay. Thế nhưng, đọc xong tập 1 cảm thấy vô cùng thất vọng. Cốt truyện không lạ, có phần nhàm chán, vẫn là motip nữ chính bình thường, ngốc nghếch nhưng vô cùng may mắn, được rất nhiều anh đẹp trai yêu thích, vây quanh. Tác giả viết hơi non tay, khiến mình có cảm giác tác giả là một cô bé tuổi teen hay mơ mộng. Truyện có những tình tiết hài hước nhưng mình cảm thấy nó thiếu tự nhiên, không cười nổi.
1
78648
2013-10-23 22:54:58
--------------------------
93104
9390
Đọc hết tập 1 mà thấy gato quá đi mất với nữ nhân vật  chính Thượng Quan Tình. :))  
Bao quanh cô chỉ có trai đẹp, trai đẹp và trai đẹp, hỏi thế thì cái máu hám trai háo sắc của cô sao mà chịu được. 
Cái tài gì cũng không có ngoài trừ cái lì, bướng bỉnh, chỉ tổ khiến cho 4 huynh đệ Âu Dương lo lắng suốt thôi, còn làm cho cả Tả thân vương, Mặc Nguyệt, Mạch bang chủ và Triều Lưu ngây ngất :)) ôi thôi, các mỹ nam đều si mê cô nàng hậu đậu này rồi haha
5
91291
2013-08-31 22:14:51
--------------------------
81898
9390
Vẫn đi theo một motip cũ không quá ấn tượng nhưng vẫn thu hút được người đọc. Thượng Quan Tình một cô gái 17 tuổi lại được xuyên không trở về quá khứ qua sự cố của Giang Thần. Một cô gái 17 tuổi còn thiếu hiểu biết sự đời lại trở về một nơi mà mình chưa bao giờ đến, một nơi chỉ có trong sách vở hay phim ảnh điều đó làm cho những xử lý của cô nàng trở nên ngây ngô nhưng đó lại là điểm thu hút gây cười của truyện. Bìa truyện không đẹp nhưng lại rất dễ thương. Truyện không có cao trào hay kịch tính nên rất nhẹ nhàng kèm theo là rất nhiều các tình huống dở khóc dở cười làm nên một câu chuyện xuyên không rất hài hước. Ai là fan của xuyên không hay hài hước thì đừng nên bỏ qua chuyện này nhé.


3
97873
2013-06-19 07:41:00
--------------------------
75345
9390
Xuyên không đã trở thành một đề tài quen thuộc đối với tiểu thuyết ngôn tình Trung Quốc, tuy vậy nhưng ''phiêu du giang hồ'' vẫn rất lôi cuốn và thu hút độc giả. Truyện nhẹ nhàng, không có những tình tiết kịch tính mà ngược lại rất hài hước và dí dỏm, ngôn từ không hoa mĩ nhưng vẫn tạo cảm giác chân thực, sống động. Tuy nhiên, điểm duy nhất mà tôi không thích ở truyện này là nhân vật nữ chính quá may mắn, lại được quá nhiều mĩ nam yêu khiến truyện trở nên hơi nhàm. Nói chung, đây là một cuốn tiểu thuyết rất thích hợp để đọc giải trí.
4
104539
2013-05-17 23:00:11
--------------------------
70064
9390
Truyện xây dựng qua nhân xưng "tôi" - 1 cô gái 17t từ tk 21 làm cho truyện có phần hài hước. Tình tiết câu chuyện được diễn tả rõ ràng, sinh động, nhưng có 1 điểm mà đến giờ em vẫn còn phân vân, tình cảm của Tiểu Tình và 8 chàng trai quá rối ren, mặc dù biết  cô đối với mỗi một người, cô đều có 1 tình cảm sâu sắc nhưng so với tất cả thì tình cảm với Triều Lưu vẫn hơn nhưng cuối cùng người cô chọn lại chính là Thiếu Nhân, như vậy không phải có hơi lạ sao?!! Ngoài ra thì mọi thứ đều ổn, chỉ là em thấy hơi tiếc cho bọn Mỹ Nam còn lại, cùng yêu 1 ngườ con gái, nhưng cuối cùng lại phải ngậm ngùi làm "Thái giám" =]] nếu có thêm phần ngoại truyện cho đám Mỹ Nam đó có đôi hết thì mãn nguyện =]]
4
102914
2013-04-19 00:41:22
--------------------------
55740
9390
ban đầu vì ấn tượng bìa sách và cũng thích thể loại xuyên không hài nên tôi quyết định mua cuốn sách này. Nội dung khá ổn, vì nhân vật nam khá nhiều nên đất xuất hiên phân bố và hay lặp đi lặp lại. Nhưng tất nhiên tôi biết nhân vật nam chính là Âu Dương Thiếu Nhân, anh này đa tình, háo sắc nhưng cũng quân tử, đối với nữ chính cuồng nhiệt như lửa. Còn nữ chính - Thượng Quan Tình- thì tôi cho là khá may mắn, không không lọt vào thân phận của một nữ hiệp rồi còn được đính ước với một trong 4 anh chàng đẹp như hoa. Điểm tôi không hài lòng là nội dung truyện còn khá nhàm, lời thoại và chi tiết gây hài nhưng cũng hay lặp lại. Có lẽ tôi không phù hợp với lối hành văn như vậy. Thêm nữa khi nhận được sách thì tôi không thấy có bookmark. Rất là buồn tiki!
4
58464
2013-01-16 15:24:09
--------------------------
55052
9390
Mình cũng không ít lần đọc truyện vượt thời gian (nhưng toàn là truyện tranh) và mặc dù đây là một mô típ không mới nhưng quả thực không phải vì thế mà nó kém hấp dẫn, ngược lại ở mỗi tác phẩm đều toát lên môt phong cách khác nhau. Phiêu du giang hồ cũng là một tác phẩm xuyên không tuy không độc đáo nhưng vẫn toát lên một phong cách chân thực đầy màu sắc, thổi một làn gió mới nhẹ nhàng vào nhưng tâm hồn ưa tưởng tượng, thích mơ mộng, phù hợp với tuổi trẻ nhất là bạn gái. (Bản thân mình đã thích cuốn sách này ngay từ trang bìa bởi những hình ảnh rất ư ngộ nghĩnh). Mong sớm có tập 2!
5
85177
2013-01-11 22:26:30
--------------------------
54885
9390
Tiểu thuyết xuyên không của Trung Quốc vốn là đề tài từ rất lâu rồi và cũng được rất nhiều bạn trẻ yêu thích bởi sự kì ảo cũng như mang nhiều tính chất tưởng tưởng, Phiêu Du Giang Hồ cũng không nằm ngoài mô típ đó. Truyện không phải đề tài mới, cũng như nội dung về một cô nàng xoay quanh những anh chàng đẹp trai tài giỏi lại càng không mới, thế nhưng truyện vẫn có những nét riêng do tác giả tạo ra. 
Cô nàng Tiểu Tình trở về thời xa xưa và nghiễm nhiên trở thành trung tâm trong mắt bốn chàng trai Âu Dương, mỗi người một vẻ, nhưng cuối cùng cô cũng chọn người người mà cô yêu. 
Cái quan trọng của tiếu thuyết xuyên không chính là kết, cái kết làm sao cho người đọc còn nhớ mãi, trở về, ở lại, đó là băn khoăn rất lớn của nhân vật cũng như tác giả, thế nhưng cái kết này khiến tôi hài lòng, như chuyện phải thế. 
3
54418
2013-01-11 10:11:28
--------------------------
50472
9390
Có thể nói Phiêu Du giang hồ vẫn viết theo mô tuýp xuyên không - quay ngược thời gian, không mới mẻ trong dạo gần đây, tuy nhiên, trong truyện có một số điểm nhấn đáng chú ý. 
Trước hết là về tạo hình nhân vật Thượng Quan Tình. rơi vào quá khứ lại giống y một Thượng Quan nữ hiệp.  Truyện thu hút người đọc ở tính logic của nhân vật chính này. Mọi cách cư xử đều rất bình thường, không thuộc dạng "chỉ cần quay về quá khứ là trở thành siêu nhân".
Tiếp theo là tính hài hước của bốn anh em nhà Âu Dương. Tuy ban đầu có hơi rối, vì cả bốn Âu Dương cứ chạy loạn cả lên, chẳng phân biệt được ai với ai, nhưng càng về sau càng được phân biệt rõ ràng hơn. Bốn con người với bốn thứ tình cảm khác nhau dành cho Thượng Quan tiểu thư. Đặc biệt là anh chàng Âu Dương Y tính tình quái đản đến đáng yêu. Trời ơi, mong chờ tập tiếp theo đến dài cả cổ rồi. 
5
41922
2012-12-14 22:29:51
--------------------------
