430373
9778
Sách hay, tuy nhiên đúng như dịch giả đã nói ngay từ đầu, sách khá khó đọc. Các bài học giá trị cho quá trình kinh doanh chứng khoán, tuy nhiên nó được ẩn trong dạng lịch sử thị trường chứng khoán Mỹ. Nhiều lúc đọc khá là mệt mỏi , vì chúng ta cũng biết là thị trường chứng khoán là chuỗi lên xuống, nên đọc sách này giống như đang học lại lịch sử như hồi đi học với một chuỗi liên tục: từ năm này đến năm này, thị trường tăng bao nhiêu điểm, từ năm này đến năm này, thị trường giảm bao nhiêu điểm. Nhưng phải vượt qua được những điều đó, thì cuốn sách tặng bạn rất nhiều kinh nghiệm sống còn trên thị trường, tránh cho nhà đầu tư những khoản thua lỗ khủng.
Cuối cùng, xin chân thành cảm ơn dịch giả rất có tâm.
5
625854
2016-05-15 00:41:52
--------------------------
227355
9778
Bạn không thể đứng vững trên thị trường, nếu không có nguyên tắc. Cuốn sách thể hiện những chiến lược của những nhà đầu tư chứng khoán hàng đầu trong lịch sử chứng khoán. Ở cuốn sách, bạn có thể khám phá và trả lời những câu hỏi luôn canh cánh trong lòng của bạn như: Khi nào nên mua?Khi nào nên bán?Có nên cắt lỗ hay không?Giá này được chưa?Cổ phiếu đang ở mức đỉnh phải không?...Để rồi, bạn có thể tự mình,rút ra được những nguyên tắc đầu tư cho bản thân, những nguyên tắc mang phong cách của bạn. Nguyên tắc và kỷ luật thực hiện những nguyên tắc ấy, sẽ giúp bạn thành công trên thị trường.
5
39519
2015-07-14 04:51:09
--------------------------
