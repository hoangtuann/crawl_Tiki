562253
9574
Mình mua quyển này để ba mình đọc, và ông khen hay, có nhiều thông tin bổ ích, tiếp theo là sách rất nhẹ, chất lượng giấy tốt, mình sẽ tiếp tục mua các quyển còn lại
5
468213
2017-04-02 18:20:20
--------------------------
474629
9574
Bác sĩ tốt nhất là chính mình _ những lời khuyên bổ ích cho sức khỏe tập 2 tiếp tục tập 1 ở tập này tác giả tiếp tục cung cấp cho người đọc những kiến thức bổ ích để chúng ta có thể cải thiện, bảo vệ và nâng cao sức khỏe của chính bản thân. Vì chỉ có mình mới biết được chính xác tình trạng của bản thân mình nên từ những kiến thức đó chúng ta sẽ có thể phát hiện sớm những bất thường trong sức khỏe của mình. Đúng là một cuốn sách bổ ích.
3
958015
2016-07-11 20:09:31
--------------------------
453696
9574
Sau khi mua tập 1 thì mình đã quyết định mua thêm tập 2 vì sự bổ ích nó mang lại. Sách cung cấp cho ta  những thông tin thú vị về sức khỏe, y học để rồi từ đó ta có thể chăm sóc sức khỏe cho bản thân tốt hơn và cho những người thân của ta tốt hơn. Sách khá là mỏng, giấy cũng hơi vàng, chắc là do sách đã được xuất bản lâu rồi nhưng mà nội dung thì khỏi chê, giá cũng hợp lí. Đây là quyển sách cần có trong mọi gia đình. Nói chung là tốt.
4
151503
2016-06-21 14:04:21
--------------------------
449423
9574
Dù bạn còn trẻ hay đã già, nếu bạn mong muốn được sống vui vẻ, khỏe mạnh cũng như sống lâu, hãy đọc cuốn sách này. Nó thực sự là một cuốn sách với những lời khuyên rất hữu ích. Đọc cuốn sách, tôi phát hiện ra mình có những quan niệm, thói quen mà trước giờ mình vẫn thường nghĩ là đúng hóa ra lại là một quan niệm sai lầm gây ra nhiều tác hại xấu tới sức khỏe. Tôi nghĩ đây là một cuốn sách hay các bạn nên đọc,qua đó rèn luyện một lối sống lành mạnh để có sức khỏe tốt hơn. Sức khoẻ là một vốn quý mà ta cần trân trọng và gìn giữ, các bạn ạ.
3
873902
2016-06-17 14:08:09
--------------------------
268136
9574
Cuốn sách chứa đựng rất nhiều thông tin về những gì phải làm để giúp cơ thể khỏe mạnh và làm chậm quá trình lão hóa. Vì người ta sẽ sống lâu hơn bao giờ hết, thay vì phải trở nên chậm chạp và xấu đi, ai có thể giữ cho cơ thể và tâm trí của họ trong 1 trạng thái tuyệt vời. Điều này được thực hiện thông qua việc giảm căng thẳng,thông qua thực phẩm lành mạnh,  Chỉ có khoảng 30% ảnh hưởng của gen có thể tác động đến sức khỏe của bạn, vì vậy hầu hết sức khỏe tương lai của chúng ta nằm trong tay chúng ta quyết định. không bao giờ muộn cho một sự thay đổi trong thói quen lành mạnh để tạo nên sự khác biệt của trong sức khỏe trong tương lai. Cách trình bày đơn giản, dễ đọc và cũng rất dễ thực hành
4
359955
2015-08-16 04:50:18
--------------------------
261082
9574
Mình đã mua trọn bộ 9 cuốn này và thấy đây là 1 bộ sách khá hữu ích. Lúc đầu mua về chỉ nghĩ là để cho bố mẹ đọc. Nhưng khi đọc thử, ngay cả người trẻ như mình cũng thấy rất hay. Cuốn sách có những lời khuyên rất hữu ích về sức khỏe cũng như chỉ ra được những thói quen không tốt mà hàng ngày mình hay mắc phải. Sau khi đọc sách mình cũng như gia đình mình đã xây dựng cho bản  thân một lối sông đúng đắn và lành mạnh hơn. Mẹ mình còn mang sách đi sang hàng xóm giới thiệu miết. ^^.
Mình nghĩ tiki nên bán sách này theo bộ để người mua cho tiện vì lúc trước mình mua cứ phải mua lắt nhắt từng tập, lại còn hết hàng tập 1 (Tập 1 mình phải mua ngoài nhà sách )
5
186954
2015-08-10 21:07:07
--------------------------
148402
9574
Ưu điểm :
- Kiến thức cực nhiều
- Chi tiết từng li từng tí
- Đậm chất y dược
- Phù hợp với mọi lứa tuổi - đặc biệt là những bạn thích ngành Y
Khuyết điểm :
- Tiki giao sách bị cong vẹo, góc bìa bị gập, hơi cũ
- Ít hình ảnh
- Chữ nhiều
Nhưng đối với mình, khuyết điểm ấy ko quan trọng bởi vì lượng kiến thức mà các Tác Giả cung cấp vô cùng nhiều. Rất cụ thể, chi tiết. Điểm trừ ở đây là do Tiki giao mình sách cũ thôi. Còn lại với mình thì quyển sách này thật hữu ích
4
520065
2015-01-10 17:04:55
--------------------------
