296560
8565
Tác giả đã cho thấy sự tài tình của mình trong việc đưa ra những phương thức để có một cuộc sống giàu có không chỉ về tiền bạc mà còn về cả hạnh phúc. Với những ví dụ thực tế và bao quát mọi mặt trong đời sống, tôi tin chắc đây là một trong những tác phẩm tuyệt vời nhất về kĩ năng sống. Làm giàu ở đây không phải chỉ là giàu có về mặt vật chất nhưng là giàu có về tâm hồn và làm cho nó trở nên phong phú. Chắc hẳn rằng đây là cuốn sách cần phải có cho những người đang muốn tìm lại sự hứng khởi trong cuộc sống.
5
112376
2015-09-11 10:01:16
--------------------------
291146
8565
Đây không phải một quyển sách dạy làm giàu, mà là một quyển sách chỉ bạn cách sống thanh thản như một người giàu có nhất thế gian. Giàu có không phải tính bằng những dãy số trong tài khoản hay bạn đang sở hữu bao nhiêu của cải, mà là bạn đang tận hưởng cuộc sống như thế nào. Bạn có thấy ngon miệng khi ăn không, có ngủ ngon mỗi tối, đã khi nào bạn bỏ ra hàng giờ để ngắm nhìn bầu trời,  hay mua một bức tranh vì thực sự cảm nhận đuợc vẻ đẹp của nó? Bạn đã chia sẻ và giúp đỡ những nguời xung quanh thế nào? Có rất nhiều điều nhỏ bé mà chúng ta đã bỏ quên trong cuộc sống hàng ngày, tác giả đã giúp ta nhìn lại và trân trọng những thứ đơn giản nhưng lại là nguồn gốc của hạnh phúc. Đây không phải sách dạy làm giàu vì vật chất, mà là cuốn sách làm giaù cho tâm hồn. 
5
38598
2015-09-06 01:27:10
--------------------------
288384
8565
Cuốn sách nói rõ về định nghĩa giàu có.Gồm những câu chuyện ngắn hay và xúc tích.Làm cho người đọc hứng thú thay và không nhàm chán.Tuy có chút lưỡng lự khi mua nhưng tôi nghĩ rằng là người quyết đoán thì mới thành công .Quyển sách làm cho người đọc hiểu nhiều hơn về hạnh phúc,không chỉ đơn giản là tiền bạc mà còn là những thứ xung quanh mình.Để cho mọi người biết rõ thế nào là hạnh phúc thật sự và định nghĩa đúng đắn nó.Cám ơn hai dịch giả Uông Xuân Vy và Trần Đăng Khoa.
4
682270
2015-09-03 15:44:15
--------------------------
266508
8565
''Trên cả giàu có'' gồm những mẩu truyện ngắn xúc tích và lôi cuốn người đọc khi viết về những điều bình dị trong cuộc sống mà có thể bấy lâu nay ta chưa kịp nhận ra. Cuốn sách không những thức tỉnh người đọc nhiều điều mà còn đem đến cho ta những trải nghiệm mới mẻ giúp ta nhận ra được đâu là hạnh phúc chân chính và Alexander đã cho người đọc thấy hạnh phúc không chỉ là giàu có về vật chất, danh vọng mà ta có thể tìm thấy nó ở bất cứ đâu, bất cứ công việc nào xung quanh chúng ta. Mỗi hành động, ý thức của ta đều có thể đem lại hạnh phúc không chỉ cho ta mà còn cho cả những người xung quanh mình. Đồng thời cuốn sách cũng cho ta biết đến những giá trị vĩnh hằng và vẻ đẹp của chúng từ đó ta biết trân trọng hơn những giá trị nghệ thuật và hơn hết ta nhận ra cuộc sống này thật tươi đẹp và đáng sống biết bao! 
5
679758
2015-08-14 19:11:29
--------------------------
225014
8565
Tôi đã lưỡng lự với cuốn sách trước khi mua nó, bởi lời giới thiệu, bởi tôi nghĩ mình nghèo rớt mồng tơi thì chưa cần đọc cuốn này. Nhưng mà lỡ thích rồi, là vì tác phầm của Uông Xuân  Vi dich mà, nên cứ mua, để đó, chừng nào giàu ta đọc :))))
Và mua rồi thì làm gì có chuyện để sau, đọc ngay cho sốt, và chợt nhận ra mình đang giàu có. Hóa ra giàu có không phải chỉ cần nhiều tiền, mà còn cần vô vàn thứ khác đang ở xung quanh. Tôi mê mẩn bài thơ tặng mẹ ở những trang đầu rồi nhìn thấy mình cũng đã ngây thơ như thế. Rồi tiếp theo những trang sách sau đó, nó cho người đọc nhìn thấy chính bản thân mình đã bỏ quên những gì, đôi lúc sẽ thấy nó như tấm gương soi khuyết điểm. 
Mong là sẽ có thật nhiều ngườ đọc được cuốn sách này để thấy mình đang giàu có, để tìm thấy sự giàu có, và hơn cả là tìm thấy sự bình yên của cuộc sống.
5
204039
2015-07-09 17:39:45
--------------------------
155693
8565
Qua quyển sách này, Alexander Green đã cho tôi biết thế nào là hạnh phúc. Hạnh phúc không chỉ là giàu có về tiền bạc mà nó còn là những thứ quanh ta như một bữa ăn, một thanh chocolate, những quyển sách,... những thứ tưởng chừng là đơn giản. Tác giả đã thể hiện quan niệm bằng những mẩu truyện ngắn một cách súc tích và lôi cuốn người nghe.Nhưng điều làm tôi rất bất ngờ và càng thêm trân trọng quyển sách đó là nó được viết bởi một nhà đầu tư tài chính lỗi lạc. Xin cảm ơn tác giả và đội ngũ dịch thuật của TGM đã mang đến cho độc giả Việt Nam quyển sách  này.
5
387632
2015-02-01 20:56:55
--------------------------
98338
8565
Sách TGM  Books này mình không phải đắn đo nhiều rồi. Và cuốn sách "TRÊN CẢ GIÀU CÓ"  đã củng cố thêm niềm tin này.

Alexander Green đã cho mình biết những danh vọng, vật chất, những thõa mãn nhất thời sẽ mau chóng lu mờ, tan biến.  Những thứ đó sẽ chẳng bao giờ mang lại cho bạn cảm giác hạnh phúc thật sự. Hạnh phúc thật sự là khi bạn giúp đỡ, quan tâm người khác, biết trân trọng những thứ quí giá xung quanh, biết cái gì là thực sự quan trọng nhất đối với mình, đó là không phải công việc, tiền bạc, danh vọng, vật chất, quyền lực...Mà là những thứ hiện hữu xung quanh : gia đình, bạn bè, con cái, niềm vui khi được giúp đỡ người khác... Chính những thứ đó mới làm bạn cảm thấy cuộc sống ý nghĩa, thật đáng sống biết bao ! 

Chất lượng sách rất tốt. Giấy in đẹp, bìa thiết kế chuyên nghiệp, và còn tặng một bookmark "kute" nữa. Và ngôn ngữ dịch rất đúng "chuẩn TGM". Khỏi cần bạn gì nữa! Quyết định mua cuốn sách này là một trong những quyết định đúng đắn nhất của mình.
5
12249
2013-11-03 10:45:41
--------------------------
