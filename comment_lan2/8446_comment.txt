542757
8446
Rất tuyệt vời. Tôi rất hài lòng về những cuốn sách có giá trị giáo dục này
5
616782
2017-03-15 09:27:45
--------------------------
258318
8446
Mình tìm mua sách để tặng cho cháu, cảm thấy rất phù hợp. Một cuốn sách trong bộ truyện dành cho trẻ em, “Những câu chuyện về…” giúp ta đưa ra những bài học bổ ích để giáo dục cho trẻ. Cuốn sách với 9 câu chuyện nhỏ kể về tinh thần trách nhiệm, của những con người bình thường hay vĩ đại, người lớn hay trẻ con, và kể cả những động vật nhỏ bé khác, ai cũng cần có tinh thần trách nhiệm, với bản thân, với người thân, những người xung quanh mình và rộng hơn thế nữa.
4
501251
2015-08-08 13:53:52
--------------------------
