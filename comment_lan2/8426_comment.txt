471430
8426
Cuốn sách được viết với ngôn từ giản dị, rất dễ đọc. Thật sự ko thể dùng 1 ngôn từ nào để miêu tả hết độ 'đỉnh" của tác giả. Từ một cuộc sống cơ cực nơi làng quê cho đến chủ tịch tập đoàn. Tôi không biết rằng đằng sau thành công đó là bao nhiêu sương máu, đau thương,Nhưng tác giả "đạp đổ" tất cả. Điều mà đáng thán phục nhất ở ông là tư duy tích cực, trong những hoàn cảnh khổ cực nhất ông vẫn vực dậy và mạnh mẽ hơn bao giờ. Giờ tôi mới thấy những điều mà tôi cho rằng rất bi đác, chả là cây đinh gì so với tác giả. Sách cho tôi một động lực mạnh mẽ. Mỗi lần thấy mệt mỏi, nản chí, thiếu động lực, tôi đều nhớ đến ông Trung. Trân thành khuyên những ai đang chần chừ thì hãy click ngay vào dòng chữ "thêm vào giỏ hàng",  giá tiền quá rẻ so với giá trị cuốn sách.
5
899706
2016-07-08 15:32:26
--------------------------
339992
8426
Em trai mình đã tặng mình cuốn sách này trong khoảng thời gian mình bế tắc nhất: không ước mơ, không đam mê, hoàn toàn lạc lối và mờ mịt về tương lai. Ban đầu mình đọc nó chỉ vì sự trân trọng món quà này, sau rồi cứ thế mà bị cuốn hút. Những dòng tâm huyết của bác Hồ Văn Trung khiến mình một lần lại một lần nghiêm túc tự nhìn nhận lại bản thân. Và từ đó, mình biết thay đổi tích cực hơn, biết nỗ lực, cố gắng hết mình cho hiện tại. "Đừng bao giờ nản chí! Đừng bao giờ buông xuôi! Đừng bao giờ tuyệt vọng! " Những câu đó luôn lặp đi lặp lại trong tâm trí mình mỗi khi chán nản, giúp mình vực lại tinh thần, vững tâm hơn, có sức mạnh lớn lao hơn bất kì lời động viên nào.
5
478202
2015-11-19 00:08:14
--------------------------
315656
8426
Đọc cuốn sách này , rồi nghe audio , tôi thật không thể hết đỗi khâm phục nghị lực của doanh nhân Hồ Văn Trung. Ông thực sự là tấm gương sách về tinh thần nỗ lực không biết mệt mỏi để đạt được thành công. Từ những năm tháng gian nan thời sinh viên cho đến những ngày tháng vượt biên đầy khổ ải, rồi cả những tháng năm bươn chải nơi đất khách quê người. Gần như ông đánh đổi cả cuộc đời, theo đuổi cả cuộc đời, nỗ lực cả cuộc đời ông để có được thành công trên thương trường kinh doanh. Tôi chỉ đọc thôi đã cảm nhận được những khó khăn chồng chất đó. Thực sự ngưỡng mộ cái ý chí sắt đá của ông.
5
294072
2015-09-29 09:37:14
--------------------------
255348
8426
Tôi thấy rất khó để tìm một cuốn tự truyện hay của một doanh nhân thành đạt, và càng khó hơn nữa khi doanh nhân ấy lại là người Việt. Cuôn sách của chú Hồ Văn Trung đạt được cả 2 điều đó. Tôi bị cuốn hút tôi ngay từ nhưng trang đầu tiên và rồi càng đọc tôi càng cảm phục ý chí và tài năng của chú. Thật đúng với câu nói của một người bạn của chú trong cuốn tự truyên: "Bạn đã vượt qua chính mình". Tuổi trẻ chúng ta cần phải tích cực học hỏi những tấm gương sáng như chú Hồ Văn Trung, thầy Nguyễn Ngọc Ký, chú Nguyễn Thành Mỹ để ngày càng phát triển bản thân, giúp ích thật nhiều cho xã hội.
5
484093
2015-08-05 22:48:47
--------------------------
102467
8426
Những trang sách đầu tiên sẽ đưa người đọc trở về những ký ức thân thương nơi miền quê nghèo khó nhưng đầy ắp niềm vui trẻ thơ (bởi đó là hình ảnh gia đình, hình ảnh người mẹ hiền lam lũ một nắng hai sương), tiếp đến là những buồn vui kỷ niệm học trò mà ai đã một thời trải qua chắc chắn sẽ không thể nào quên.

Tuy nhiên, cuộc sống là sự đan xen giữa các thái cực khác nhau, các mảng màu sáng tối. Như tác giả đã nhận xét, ai sinh ra cũng có trái tim, có những trái tim ngập tràn nhịp đập yêu thương; nhưng cũng có những trái tim nhẫn tâm & tàn bạo trong lớp vỏ bọc nhân văn. Dù vậy, mỗi người mỗi hoàn cảnh, mỗi cuộc sống khác nhau; nên nhìn người ta để phấn đấu chứ không phải để buồn tủi & suy sụp... Nước mắt chỉ có thể giải tỏa nỗi buồn chứ không giải quyết được mọi chuyện.

Cuốn tự truyện là nguồn động viên to lớn để các bạn trẻ vững bước vào đời (không chỉ dừng lại ở khía cạnh cỗ vũ cho bầu nhiệt huyết tuổi trẻ, ý chí kiên định vững vàng, mà còn thấm đẫm tính nhân sinh & nuôi dưỡng tình thương dành cho gia đình, quê hương, đất nước)
5
67340
2013-12-19 13:43:01
--------------------------
