495884
8769
Câu chuyện ca ngợi lòng dũng cảm và thật thà của cậu bé Minho. Cậu đã dám viết lên sự thật lòng mình, dám đấu tranh nội tâm để cuối cùng quyết định vứt bỏ món bảo bối có thể giúp cậu được khen thưởng, được nổi tiếng. Đó là một quyết định đúng đắn và vô cùng mạnh mẽ, vì cậu không muốn lừa dối người khác, càng không muốn lừa dối chính mình. Và phần thưởng cho lòng dũng cảm đó là món quà đầy bất ngờ và tuyệt vời mà cậu ko dám mong ước. Đây là một câu chuyện dành cho thiếu nhi nhưng cũng đáng cho người lớn suy ngẫm. Vì người lớn đôi khi hành động và suy nghĩ ko bằng trẻ nhỏ. Truyện viết khá được, mình khuyến khích cho 5 sao :)
5
324504
2016-12-14 19:50:07
--------------------------
386409
8769
Tuần trước, tôi đã mua cuốn sách này và đọc nó. Tôi bị cuốn hút vào những câu chuyện, lời dẫn trong cuốn sách 'Cây bút thần kì'.
Minho đã đưa tôi về những kí ức tuổi thơ tuyệt đẹp. Đã bồi đắp cho tôi thêm nhiều suy nghĩ về cuộc sống. Những câu chuyện giản dị nhưng mang đầy màu sắc riêng biệt của chính Minho. Từng dòng chữ đầy tâm hồn đã để lại trong lòng người đọc những điều đáng suy ngẫm, những giá trị to lớn của cuộc sống hiện nay.Đó là sự đấu tranh đầy quyết tâm. Cuốn sách thật hay và ta hãy trân trọng nó.
5
1085176
2016-02-25 17:24:25
--------------------------
328942
8769
Truyện không dày lắm nhưng không hề nhẹ. Trong truyện cũng không có hình minh hoạ.(vấn đề này rất tốt)
Truyện này nói về những chuyện thường ngày ở trường của cậu bé Minho, về chiếc bút chì thần thánh. Khi đọc truyện này, bà chị bé của mình cũng đã ao ước có một chiếc, hihi.
Truyện khá là thú vị đấy, nhưng mà mình phải nói cái bìa sách nó lừa tình cực kì luôn. Cái bìa như thế này làm mình cực kì không có hững thú để ngồi đọc quyển sách này đầu tiên luôn đó. Không hiểu ai đã thiết kế cái bìa chối cả tỉ như vậy.
5
800850
2015-10-30 20:55:39
--------------------------
205926
8769
Công nhận đây là một cuốn truyện thiếu nhi hay và ý nghĩa. Tác giả đã lấy đề tài ngay từ cuộc sống xung quanh: các em học trò viết văn chỉ cốt để được điểm cao, những người chỉ hay nói những gì người khác muốn nghe chứ không dám giãi bày thật lòng. Cậu bé Min-ho đã trải qua một cuộc đấu tranh với chính mình để có thể nói lên suy nghĩ bản thân, gửi gắm tâm nguyện đến người cậu yêu thương, vì cậu đã hiểu được rằng những gì xuất phát từ trái tim sẽ đến được với trái tim.
Mình hài lòng với cách dịch nhưng phải nói thật là khâu thiết kế bìa cẩu thả đã làm tụt 50% độ hấp dẫn của cuốn sách. Mình ghét cay ghét đắng người nào đã chọn duyệt cái bìa này.
4
327126
2015-06-08 09:13:34
--------------------------
167164
8769
“Cây bút thần kỳ” đưa mình trở về với những ký ức tuổi thơ. Đối với mình bài tập làm văn là thử thách vô cùng khó khăn, luôn nhận điểm trung bình, dẫu rất muốn học tốt hơn nhưng mình không thể tìm ra cách ngoại trừ mong ước điều thần tiên sẽ đến với mình. Mình cũng sống cùng người Mẹ tảo tần, chịu thương chịu khó chăm lo cho đàn con vì ba mình mất khi mình học lớp 1. Mình cũng từng khao khát một ngày nào đó ba sẽ trở về, bù đắp lại những mất mát đã qua.
Minho thực chất là một cậu bé ngoan, cây bút chì đỏ đã đem lại cho cậu bé niềm vui lớn lao, nhưng cậu đã nhận ra nó vốn không thuộc về mình, không thể lúc nào cũng phụ thuộc vào cây bút ấy. Cuối cùng Minho cũng chiến thắng được bản thân mình, được nhận vào trường Nalara. Và sẽ còn rất nhiều thử thách mới đang chờ đón Minho.
5
467862
2015-03-14 08:13:31
--------------------------
148698
8769
Những mẫu chuyện vụn vụt quanh cuộc sống hằng ngày của Minho trong Cây Bút Thần Kỳ cho mình nhiều bài học quý giá trong cuộc sống.
Những bài học về tình người mang tính nhân văn vô cùng sâu sắc. Trong đó nổi bật lên là bài học về lòng trung thực của Minho. Phải qua bao nhiêu giằng vặt từ nội tâm, qua bao nhiều đấu tranh trong tư tưởng cậu mới có quyết định đúng đắn ấy :) Một sự quyết định sáng suốt và đầy cao thượng!
Quyển sách không phải là những câu chuyện quá hay, không quá nhiều tình tiết nhưng để lại những giá trị to lớn về bài học làm người.
Với văn phong tinh tế, cách kể truyện chậm rãi và có hồn, khắc họa tâm lý cũng như nội tâm nhân vật với những giằng xé đầy tinh tế, Cây Bút Thần Kỳ đã tạo nên một quyển sách hay và đáng để trân trọng nó!
5
303773
2015-01-11 11:40:34
--------------------------
116580
8769
Cây bút thần kỳ" là cuốn sách mang đậm tính nhân ái, tình yêu thương và triết lí sâu sắc. Mỗi một câu chuyện như là trang nhật kí mỗi ngày của Minho. Nhặt nhạnh những điều thường ngày ấy là sự giằng co nội tâm của Minho. Danh dự và trung thực. Có Thần Đèn-Bút chì đỏ đưa lối "cậu được bạn bè và thầy cô chú ý, và hơn hết cậu hạnh phúc vì thấy mẹ cười vui." Cùng với điều đó là sự tự dằn vặt bản thân về lòng trung thực. Và khi mọi rắc rối của Minho được tháo gỡ, đó cũng là lúc bắt đầu cuộc hành trình mới của Bút chì đỏ với cậu bé nghèo khổ Hyo Ju...
5
247455
2014-07-09 22:34:58
--------------------------
90563
8769
Cuốn sách cho ta hiểu thêm về lối sống đẹp trong cuộc sống vội vã hiện nay. Nó giúp ta sống chậm lại và hiểu thêm về lòng trung thực. Gieo cho ta hạt giống tốt vào tâm hồn. Rất ngưỡng mộ Min-ho vì đã dám hủy bỏ chiếc bút thần kì đó trong khi ai cũng muốn có. Đọc xong ngẫm lại một chút, tôi thấy xấu hổ về bản thân mình. Ngòi bút của Shin Soo Hyeon rất tinh tế khi miêu tả tâm lý,  diễn biến nội tâm một cách đầy dai dẳng, cuộc đấu tranh giữa chính nghĩa và phi nghĩa, giữa sự thật và dối trá.
5
128763
2013-08-11 15:22:18
--------------------------
