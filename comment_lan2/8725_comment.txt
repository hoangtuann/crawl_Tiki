462852
8725
Poster 2 Mặt - Các Loại Côn Trùng + Loài Bò Sát hình ảnh đẹp, sắc nét nhưng tông hơi tối. Đồng thời khổ lại nhỏ hơn những poster trước mình mua. Tuy nhiên mình hài lòng về nội dung. Poster phù hợp cho các bé đang làm quen với con vật và có khái niệm theo logic.
Một điểm nữa là poster hai mặt nên không biết phải dán mặt nào? mặt nào cũng phải xem mà cuộn vào thì lúc giở ra bị cong, khó thẳng hình để bé nhìn rõ.
Giá thì quá rẻ rồi nên mình mua cả bộ cho bé xem luôn.

4
895234
2016-06-28 22:15:29
--------------------------
357192
8725
Hình ảnh của poster Các loại con trùng và loại bò sát chân thật sống động, được in cả hai mặt nên tiết kiệm hơn so với mua poster lớn. Có cả tiếng anh lẫn tiếng việt sau này bé lớn vẫn dùng được. Bé nhà mình mới một tuổi nhưng rất thích, lúc thì bé chỉ con này con kia, lúc thì vứt, cắn, xé mà poster vẫn không bị rách. Mình đã tìm ở nhiều tiệm sách nhưng ít có các chủ đề quen thuộc về thế giới xung quanh như phương tiện giao thông, con vật nông trại để bé có thể khám phá thêm.
4
833970
2015-12-23 09:08:54
--------------------------
296565
8725
Poster 2 mặt “Các loại côn trùng + Loài bò sát” thật sự rất hấp dẫn đối với các bé nhà tôi. Bé nhờ mẹ chỉ cho từng con, rồi bé lặp lại. Sau này khi đã thành thạo bé còn nhận ra “A, con rết này có trong truyện Cóc kiện trời nè”, “Con châu chấu, bọ rùa… có trong truyện Cô bé Mác tin bảo vệ thiên nhiên nè”, “Con nhện có trong truyện Vì sao cua lại bò ngang nè”. Đây là những sản phẩm vừa có tính giải trí vừa có tính giáo dục cao, giúp bé được học hỏi một cách trực quan sinh động và nuôi dưỡng cho bé đam mê khám phá mọi thứ trong cuộc sống.  
5
332766
2015-09-11 10:04:13
--------------------------
293341
8725
Bé nhà mình mỗi lần được Mẹ cho về quê thăm Ngoại, bé rất thích nhìn ngắm những ban bướm bướm, hay chuồng chuồng bay lượn trong sân. Để kích thích con học tốt mẹ đặt mua trên Tiki Poster 2 Mặt - Các Loại Côn Trùng + Loài Bò Sát, cho hai anh em cùng học tiếng anh. Khi được Mẹ cho xem sản phẩm này hai anh em cùng reo lên thích thú, sản phẩm có chất lượng rất tốt, giúp cho việc học tiếng anh của trẻ bớt nhàm chán. Cám ơn Tiki rất nhiều, mình sẽ tiếp tục ủng hộ Tiki
5
764531
2015-09-08 10:50:00
--------------------------
279205
8725
Mua Poster 2 Mặt - Các Loại Côn Trùng + Loài Bò Sát tính ra giá thành khá là rẻ và tiết kiệm hơn mua poster cỡ lớn vì bé sẽ học được cả hai mặt nhưng chỉ có nửa giá tiền, Hình ảnh được in rất rõ nét, tất cả đều được chụp từ ảnh thật nên bé sẽ nhận biết được các con vật rất dễ dàng từ con bướm, con bọ cho đến con rết, con ruồi hay con chuồng chuồng đều thấy rõ từng chân tơ kẻ tóc luôn các bạn ạ! Tiếng anh thì có thêm cả phần phiên âm quosc tế nên bé rất dễ dàng đọc được. Cho 5 sao nhé!
5
722015
2015-08-26 16:12:38
--------------------------
