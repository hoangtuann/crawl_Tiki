542901
8470
Cuốn sách khá hữu ích và vui nhộn, mình tạm ko nói tới nội dung sách vì các bạn khác đã nói rồi, chung quy rất phù hợp với các bạn trẻ để thúc đẩy bản thân mạnh dạn đi trên một lối đi riêng. Sách nhẹ, chữ hơi nhạt nhưng tương đối dễ đọc, tuy nhiên sách mình có lỗi in ngược trang nhưng dù sao cũng ko quan trọng lắm nên mình lười ko muốn đổi, cũng tạm hài lòng.
4
912296
2017-03-15 10:06:13
--------------------------
295135
8470
Trước tiên là tựa sách rất gây tò mò nhưng bìa sách trình bày hơn chán. Cũng đắn đo rồi mới quyết định mua, và quả thật là một quyết định đúng đắn. Những trò quỷ quái không trái lương tâm của Hugh Macleod có một cách diễn đạt rất riêng. Hài hước, ngắn gọn nhưng đầy đủ ý nghĩa, đọc cảm thấy rất thích thú. Tìm hiểu thì biết được Tác giả là một họa sỹ chuyên vẽ tranh biếm họa và còn là một blogger có tiếng. Có lẽ nhờ vậy mà tác giả có một giọng văn vô cùng đặc biệt. Nói chung là một cuốn sách đáng đọc cho các bạn trẻ.
5
82495
2015-09-09 23:48:12
--------------------------
263101
8470
Đọc Phớt lờ tất cả và bơ đi mà sống thấy rất hay ho nên mình quyết định mua tiếp quyển này. Vẫn với cái giọng văn tưng tửng và đầy dí dỏm ấy, tác phẩm đã không làm mình thất vọng. Hãy đọc khi bạn cần đổi mới tư duy, thay đổi góc nhìn và muốn tìm đến sự sáng tạo. Nó sẽ mở ra cho bạn một chân trời khác, thay đi cặp kính bạn đang đeo bằng cách tặng cho bạn một thấu kính vạn hoa tuyệt đẹp. Bạn sẽ được nhìn Thế giới này theo kiểu tưng tửng của tác giả luôn, giọng văn rất ngông, rất bố đời nhưng rất thuyết phục, nói chung là rất Hugh Macleod 
Quyển sách sẽ không làm bạn thất vọng đâu :)))
4
464538
2015-08-12 13:01:22
--------------------------
197995
8470
Hãy đọc những khi bạn gặp phải quyết định khó khăn. Cuốn sách đã giúp tôi thư giãn và mỉm cười khi nhìn tháy nó. Với cách viết ngắn gọn, hài hước, đặc thù của ngôn ngữ blog, cuốn sách này đã chinh phục được tôi với những ý tưởng lạ lùng, thông minh, tinh tế, mạch lạc. Mỗi khi có nhiều điều phải suy nghĩ nhìn đến tựa đề và nhớ lại ngôn từ và tranh biếm họa của tác giả là mình lại mỉm cười. Nó thật sự rất có ích đối với tôi.
Cảm ơn Tiki rất nhiều. Người bạn đồng hành khi tôi tìm bất kỳ sản phẩm nào.
4
510184
2015-05-18 21:58:53
--------------------------
181990
8470
Nếu ai có ý định khởi nghiệp thì đây là một quyển sách không thể bỏ qua. Tác giả Hugo Macleod thực sự không làm người đọc thất vọng với tác phẩm này. Mở đầu tác phẩm, ông chia sẻ về quá trình khởi nghiệp của bản thân, về những suy nghĩ, quan điểm của bản thân trong những lúc khó khăn nhất, rồi ý tưởng khởi nghiệp đã đến với ông như thế nào. Với cách mở đầu như vậy Hugo khiến tôi bị cuốn hút theo, và vô cùng cảm thấy thích thú với những phần sau đó. Ông có phong cách viết rất thú vị, đôi khi là đầy châm biếm, rồi các hình ảnh minh họa nữa đầy hàm ý và cũng không kém phần vui nhộn. Nội dung của cuốn sách này đương nhiên là không thể chê vào đâu được. Còn về mặt hình thức, màu bìa đẹp, và bắt mắt, chất lượng giấy cũng rất tốt. Đó là những ưu điểm dễ thấy. Còn về nhược điểm thì, tôi thấy nên đặt vị trí của mục lục ở cuối sách thì dễ tra cứu hơn là vị trí hiện tại.
4
329218
2015-04-12 22:17:21
--------------------------
163462
8470
Hugh Macleod là một chuyên viên thiết kế quảng cáo nhưng đa số độc giả biết tới ông qua vai trò là một họa sỹ vẽ tranh biếm họa và một blogger nổi tiếng trên mạng Internet. Ông thường vẽ những tranh biếm họa không giống ai và thể hiện trong đó suy nghĩ, ý tưởng và sự sáng tạo đặc trưng của mình. Tôi đã mua quyển sách này một lần và đã bị mất, chứng tỏ có khá nhiều người hâm mộ Hugh Macleod và các tác phẩm của ông. Xin cám ơn công ty Alphabooks đã mang tác phẩm này tới tay độc giả Việt Nam với mức giá phải chăng, nhờ đó mà nhiều cá nhân đã biết nuôi dưỡng và phát huy sự sáng tạo của mình.
5
387632
2015-03-05 02:38:46
--------------------------
148340
8470
Có thể thấy rằng Những Trò Đùa Quỷ Quái Không Tai Hại của Hugh Macleod đã vẽ ra những màu sắc về tính chất của con người này qua cái nhìn hài hước đầy chất châm biếm.
Đó là những bài học vô cùng quý báo về sự kiên trì, cái tôi bản thân, sự đam mê hay đơn giản là lương tâm của chính con người mình. Qua cách nhìn mới mẻ và châm biếm, Hugh Macleod đã khai thác hết mọi góc cạnh giúp mình nhìn nhận lại vấn đề một cách có logic hơn, nhờ đó mà những sự việc rất dễ thấy để mình có thể ngẫm lại bản thân. 
Thêm vào đó là cách sắp xếp của tác giả khá liên kết và chặt chẽ, mình không thấy văn chương hay nội dung nào rời rạc cả :)
Văn phong chủ yếu lấy nghệ thuật châm biếm làm chủ đạo. Thêm vào đó là những ngôn từ bình dị, dễ gần nhưng đầy tinh tế và hàm súc.
Yêu thích và đánh giá cao cho tác phẩm này!
4
303773
2015-01-10 15:36:29
--------------------------
127747
8470
Sau khi đọc xong cuốn những trò quỷ quái không trái lương tâm tôi quả thật rất ấn tượng về cách viết của tác giả . Cách viết đó rất đặc biệt châm biếm một sự vật hiện tượng bằng những câu từ đậm chất văn chương giogj điệu hài hước khiến người ta phải bật cười . Những chi tiết được sắp xếp chặt chẽ liên kết với nhau . Nhìn chung tổng thể cuốn sách thật khó để bới móc ra chút khuyết điểm nào . Bởi vì tác giả đã rất chăm chút cả về nội dung lẫn ngôn từ . Thông điệp tác giả muốn truyền tài mà tôi thấy ý nghĩa nhất đó chính là hãy sống vì đam mê của mình và cháy hết mình vì nó . Chính những lời văn mang tính chất cổ vũ động viên của tác giả đã góp phần truyền tải thông điệp đó một cách sâu sắc hơn
5
337423
2014-09-26 16:12:57
--------------------------
105937
8470
Tác giả  Hugh Macleod có một cá tính mạnh mẽ, quyết đoán, phóng khoáng, hài hước, lạc quan, có chút  táo bạo nhưng cũng rất chân thành và đầy trách nhiệm. Xuyên suốt "Những Trò Quỷ Quái Không Trái Lương Tâm" Hugh Macleod không ngừng đề cập và nhấn mạnh về: lương tâm con người. sự thành thật, lòng kiên trì, niềm đam mê đối với công việc mình đang theo đuổi cũng như ý nghĩ và tầm quan trong của những điều đó đối với cuộc sống của mỗi người. Lối viết chăm biếm hài hước,  ngắn gọn, đơn giản, không màu mè. Không như những tác giả khác Hugh Macleod khiêm tốn, không nhắc nhiều đến thành công của bản thân. Mỗi  câu truyện trong"Những Trò Quỷ Quái Không Trái Lương Tâm" là một bài học quý giá, giúp người đọc có cái nhìn sáng suốt và lạc quan hơn trong cuộc sống cũng như tự tin để tiến hành "những kế hoạch quỷ quái" của mình
4
137369
2014-02-10 21:00:22
--------------------------
