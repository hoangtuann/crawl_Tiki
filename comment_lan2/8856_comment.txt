458190
8856
Nếu bạn là nhân viên trong công ty, thì bạn cần đọc cuốn sách này, vì đơn giản, đây là những điều có thể xảy ra trong tương lai. Tác giả đã viết cuốn sách này nhằm giúp mọi người hiểu hơn về những vấn đề không phải lúc nào bạn cũng thấy và xảy ra. Dù sao biết nhiều hơn, vẫn tốt hơn, lúc đó, bạn sẽ nhận ra và hiểu chuyện gì đang xảy ra.
Và nếu bạn là người chủ, thì bạn cũng nên đọc để hiểu hơn về tâm tư tình cảm của nhân viên, và những điều giúp nhân viên làm việc hiệu quả hơn.
4
65325
2016-06-24 22:46:16
--------------------------
