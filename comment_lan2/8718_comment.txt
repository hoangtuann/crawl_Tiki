463945
8718
Poster với thiết kế hai mặt in màu, chất liệu giấy bóng, khá cứng, màu sắc in sinh động, rõ nét. Một mặt là từ vựng về các đồ vật, sự vật trên đường phố, một mặt là về công viên. Cách trình bày đơn giản, dễ hiểu: phần trên là từ vựng về từng sự vật riêng lẻ, theo thứ tự Hình vẽ- tiếng anh- phiên âm- tiếng việt. Bên dưới là bức tranh mô phỏng cảnh vật với đầy đủ chi tiết. Hình vẽ rất đáng yêu, từ vựng nội dung khá đầy đủ. Khổ của poster là cỡ A3. Giá thành hợp lý, chất lượng tốt.
5
95034
2016-06-29 20:09:49
--------------------------
462865
8718
Poster 2 Mặt - Trong Công Viên + Trên Đường Phố hình ảnh đẹp, sắc nét, mô tả cảnh ở công viên và trên đường phố với những items rất cụ thể. Phía trên có hình của từng items và chú thích. Bé có thể nhìn được tổng thể một môi trường lại vừa quan sát được chi tiết từ đó hình thành ra khái niệm. Mình rất ưng ý nội dung của poster này. Mình giở ra cho bé xem và chơi tìm hình trong poster, bé rất thích trò này.
Tuy nhiên, khổ của poster thì hơi nhỏ hơn so với những poster khác. 
5
895234
2016-06-28 22:21:35
--------------------------
296543
8718
Trong quá trình mua sách phục vụ cho việc học và tự học của các thành viên trong gia đình, tôi thấy những sản phẩm của First News có chất lượng khá tốt, được làm kỹ lưỡng, tâm huyết. Poster 2 mặt “Trong công viên + Trên đường phố” tập hợp khá đầy đủ những thứ xuất hiện… trong công viên và trên đường phố. 1/3 phía trên của poster là những hình ảnh riêng lẻ, chú thích bằng tiếng Việt, tiếng Anh và phiên âm, 2/3 phía dưới vẽ quang cảnh của công viên và đường phố, bé vừa giải trí vừa học hỏi nhiều thứ mới lạ (bé nhà tôi rất thích chơi trò mẹ đố và con đi tìm từng vật xuất hiện bên dưới như xích đu, con chó…). Sản phẩm bổ ích, thú vị và giá cả phải chăng.
5
332766
2015-09-11 09:51:15
--------------------------
283549
8718
Poster 2 Mặt - Trong Công Viên + Trên Đường Phố, bé nhà mình rất thích những hình ảnh trên poster hai mặt này, bé nhỏ thì cứ chỉ vào hình hỏi mẹ con này con gì, cái này lá cái gì vậy mẹ, và con thich được chơi bập bênh này lắm, còn bé lớn thì cảm thấy việc học tiếng anh của mình trở nên nhẹ nhàng hơn. Mỗi ngày sau khi đi học về hai anh em lại đến chỗ mẹ dán poster này đó nhau để kiểm tra từ vựng. Một sản phẩm rất bổ ích, các mrj có con nhỏ nên mua dùng nhé
5
769621
2015-08-30 06:47:11
--------------------------
277945
8718
Poster 2 Mặt - Trong Công Viên + Trên Đường Phố chứa khá đầy đủ vốn từ vựng để bé học và thực hành khi đi công viên chơi cùng các bạn, sau khi học qua poster này bé dạn dĩ hơn, biết được nhiều hơn. Cuối tuần bé được mẹ cho đi công viên chơi, mẹ đó bế thử xích đu tiếng anh gọi là gì bé trả lời được ngay, hay cái cây tiếng anh đọc thế nào hả con bé liền nói là tree..Mình thấy rất hài lòng về giá cả cũng như chất lượng sản phẩm, chỉ hơn sáu ngàn mà có được sản phẩm cực tốt.
5
722015
2015-08-25 14:36:18
--------------------------
