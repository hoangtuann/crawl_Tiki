217844
9839
Đây là một tập hợp của rất nhiều bài viết của các học giả, giáo sư, nhà nghiên cứu tâm huyết đối với sự hưng thịnh của đất nước. Sách gồm 4 phần chính. Phần mở đầu là những góc nhìn rất khác và rất sâu sắc về việc biến đổi khí hậu về những hậu quả mà khu vực miền Trung đang phải gòng mình gánh chịu. Phần thứ hai là những câu chuyện rất thú vị về lịch sử khai mở sứ đàng trong. Tuy không phải kể về cuộc đời chính sửa của 1 vị anh hùng nào đó, nhưng lại cung cấp rất nhiều thông tin về sự hình thành và phát triển về con người và văn hóa của sứ đàng trong. 
Phần thứ ba là những chia sẻ về nền giáo dục, tuy nhiên cũng như sự cải cách giáo dục hiện nay, các bài viết chỉ đi vòng vòng các vấn đề mà không cho thấy được giải pháp nào rõ rệt. Phần cuối, chia sẻ nhiều câu chuyện về văn hóa làng cổ về của cây đa bến nước, mái đình xưa, việc bảo tồn những giá trị văn hóa cổ xưa là vô cùng cần thiết.
Sách sẽ hữu ích cho những bạn đang tìm kiếm những giá trị, những góc nhìn khác nhau về đất nước.
4
361233
2015-06-30 09:00:10
--------------------------
