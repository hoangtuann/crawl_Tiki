389989
8690
 Mình rất thích sản phẩm poster này. Chất lượng giấy rất tốt và khó bị xé rách bởi các bé hay nghịch ngợm. Trình bày đẹp và chọn từ tốt, toàn những từ thông dụng trong bếp nhưng lại ít được dậy ở các lớp học tiếng Anh. Tuy nhiên màu sắc và cách trình bày chưa được nghệ thuật cho lắm. (tuy thế so với các dòng poster khác thì trình bày thế này là tạm ổn, khá hấp dẫn với các bé). Sản phẩm này recommended cho các bạn muốn dạy bé tiếng Anh từ bé, có thể treo tranh này từ 2 hoặc 3 tháng tuổi và đọc cho các bé nghe, khi lớn có thể chỉ cho bé tự đọc, sẽ giúp bé thích tiếng Anh một cách tự nhiên.
4
95153
2016-03-03 03:31:10
--------------------------
