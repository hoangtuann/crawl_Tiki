489869
736
Mình mua là đợt giảm giá nên còn 400k thôi, được bọc nhựa nữa nên lúc khui hàng ra nhìn sách rất đẹp. Là bản script bởi vậy dù tiếng anh không tới đâu như mình đọc cũng hiểu được, vì toàn lời thoại các nhân vật thôi. Mình đọc chưa hết nhưng thấy khá hồi hộp ngay từ mấy trang đầu rồi, mới mẻ hơn so với 7 phần Harry trước đó.
4
88381
2016-11-10 12:44:17
--------------------------
487357
736
Thứ nhất mình phải nói cốt truyện vẫn rất xuất sắc như 7 người anh trước. Tuy được viết ở dạng kịch bản nhưng cuốn truyện, theo mình, vẫn khắc họa từng nhân vật một cách sâu sắc và hợp lí. Không một nhân vật nào là hời hợt cả. Ngoài ra, tuy có sự tham gia của 2 tác giả khác song các nhân vật chính hay phụ chính như Harry, Ron hay Hermione, Ginny vẫn thừa kế nguyên vẹn tính cách và bản sắc của 7 tập trước. Có khác chỉ là cách nói trưởng thành và già dặn hơn (nhất là Harry).
Truyện cũng mở ra nhiều khác biệt rất bất ngờ cho mình vào thời điểm 19 năm sau, tuy nhiên vẫn đảm bảo tính gần gũi và thân quen với 7 tập trước. Để không làm các bạn mất hứng thì mình sẽ #KeepTheSecrets những chi tiết này.
Song, bên cạnh đó mình vẫn cảm thấy hụt hẫng gì đó vì tập này không còn là cô JKR trực tiếp chắp bút nữa. Mình sẽ hi vọng nhiều hơn vào cuốn Fantastic Beasts and Where to Find Them (bản Script) chỉ do mình cô JKR viết mà thôi :). Không phải ích kỉ đâu nhưng đối với một fan trung thành như mình thì thế giới pháp thuật chỉ có cô JKR mà thôi.
Tiếp nữa, điều này mình không tính vào đánh giá sách. Là về hình thức bên ngoài, quyển sách rất đẹp, bìa cứng đen trông khá trang nhã. Có điều, chắc do vận chuyển mà phần bìa giấy bọc ngoài của mình bị cong vênh đôi chút. Nhưng không sao, vẫn là 5 sao cho cuốn sách này :).
5
1369222
2016-10-21 22:46:57
--------------------------
484080
736
Quyển sách này như đem mình quay lại tuổi thơ vậy, lại được hòa mình vào thế giới phù thủy một lần nữa. Quyển này không phải chỉ mình tác giả cũ viết nên giọng văn có một chút hơi lạ lẫm so với các tập trước. Nhưng còn lại đều có thể chấp nhận vì thời gian diễn ra câu chuyện này là 19 năm sau rồi, một thế hệ hoàn toàn khác, một câu chuyện mới, khác biệt đi cũng là điều dễ hiểu.
Còn về hình thức của quyển truyện thì quá đẹp. Bìa cứng ở trong, lớp giấy bìa bên ngoài sờ rát thích 
5
41367
2016-09-20 13:29:37
--------------------------
481938
736
Mình hơi buồn vì HP8 không giữ được sự logic gần như tuyệt đối của các tập trước. Có khá nhiều chỗ hổng và làm mình khá ngứa ngáy mà trước giờ mình đọc cô J chưa bao giờ mình thấy thế luôn. Ví như đoạn 2 đứa trẻ chưa trải đời mấy lần lượt qua mặt các pháp sư đầy kinh nghiệm của chúng ta, một - cách - không - vất - vả - lắm. Thiệt tình cái đoạn hôn hít của Albus làm tui phát ói :v
Anw, đã là fan thì k thể bỏ qua vì là nghĩa vụ và trách nhiệm rồi :v Với lại vẫn đảm bảo ending ngọt ngào sau tất cả.
3
297766
2016-08-31 23:25:09
--------------------------
479903
736
Mình đã mua sách của Tiki, đặt lâu lắm rồi và giờ đã có trong tay em yêu. Sách được bọc cẩn thận trong hộp, bìa đẹp vô cùng, lại còn được bọc bookcare nữa chứ. Đối với những ai là Potterhead thì nên mua để trưng bày, thêm em nó vào bộ sưu tập sách của mình. Còn về phần nội dung thì hơi thất vọng vì vốn dĩ cuốn này ko phải do cô JK Rowling viết nên mình cũng chả mong đợi gì nhiều. Sợ viết nội dung sẽ thành spoiler nên thôi. Nói chung mua cũng đáng tiền, hơn nữa sách này là bìa cứng, Tiki cũng giảm giá nên đừng ngần ngại mua nhá hihi
4
902028
2016-08-13 11:57:39
--------------------------
478818
736
Mình là một fan cứng của cô Rowling và đã đọc Harry Potter gần 12 năm, cho dù cô có viết thêm bao nhiêu cuốn HP nữa thì mình cũng sẽ mua. Nhưng phải chân thật nói rằng cuốn Cursed Child này có rất nhiều điểm trừ. Và rốt cuộc nó cũng chỉ là script  cho một vở kịch mà ở đó mọi chuyện diễn ra quá nhanh, quá gượng ép. Thế hệ của Harry 19 năm sau dường như vẫn chưa đủ khả năng để bảo vệ Bộ pháp thuật và thế giới phù thuỷ, chỉ một vài đứa trẻ thôi cũng suýt phá tan tành mọi thứ. Có thể khi dựng vở kịch cursed child rất hay nhưng plot của sách thì rất yếu và xây dựng nhân vật khá nhạt. Dù sao thì sách cũng khá hấp dẫn với một Potterhead như mình, và hơn hết nội dung vẫn rất nhân văn, rất ý nghĩa. Ps: sách khá to nhưng nội dung ít. Đọc tầm 1,5 ngày là xong.
3
88741
2016-08-05 13:22:38
--------------------------
478519
736
Chưa đọc hết nhưng nội dung khá thú vị và cuốn hút. Nhân vật chính là con trai của harry potter tuy nhiên các nhân vật đời trước vẫn xuất hiện khá nhiều, đảm bảo độ gần gũi với 7 phần trước. Quyển này là dạng kịch bản, thuần lời thoại các nhân vật chứ không phải sách truyện như bình thường nên nếu ai đọc không quen sẽ không thấy hứng thú. Vì chủ yếu là các câu thoại đời thường nên đọc rất dễ hiểu, thích hợp cho cả người tiếng anh chưa vững, muốn luyện thêm.
Điểm trừ là sách ship không được bọc nilon kín lại nên bọc sách hơi bị nhăn chút và bị ẩm. 
4
1373797
2016-08-02 23:09:17
--------------------------
