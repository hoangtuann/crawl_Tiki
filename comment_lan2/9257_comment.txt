330270
9257
tôi đã đọc hết cuốn này, và cảm thấy phù hợp cho việc hướng dẫn bé học tập, chuẩn bị cho việc sắp vào lớp 1, cũng như rèn luyện tư duy cho bé. 
Tôi cảm thấy, thay vì chia nhỏ thành tám tập, thì nhà xuất bản có thể có thêm lựa chọn là gộp lại thành một cuốn hoàn chỉnh thì sẽ tốt hơn cho phụ Huynh trong việc kiểm soát và xuyên suốt quá trình học cho con. 
Cuốn sách in màu khá dễ thương và bắt mắt, tạo cảm giác hung thú học tập cho các bé khi nhìn vào. 
4
572177
2015-11-02 11:56:29
--------------------------
