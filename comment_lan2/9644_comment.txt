52856
9644
Trong cuộc sống hối hả, tấp nập ngày nay, chúng ta thường quên đi sự tĩnh tâm trong chính tâm hồn mình. Cuộc sống tấp nập mang đến cho chúng ta sự thỏa mãn về vật chất và đó chỉ là niềm vui và hạnh phúc nhất thời. Chính vì vậy nên khi đã đạt được cái mình muốn thì con người chúng ta lại không cảm thấy thỏa mãn và lại đặt ra cho mình một mục tiêu mới cao hơn với ý nghĩ việc đạt được mục tiêu đó sẽ mang lại cho mình hạnh phúc. Nhưng đó là một sự lầm tưởng vì trong cuộc sống quá tấp nập và vội vàng này thì sâu thẳm trong tâm hồn chúng ta vẫn cần một sự tĩnh lặng, một sự lắng đọng để nhìn nhận lại tất cả những sự việc đã qua, cũng là để định ra cho mình một con đường mới, một mục đích mới mà chính bản thân mình cần chứ không phải là muốn. Điều đó sẽ giúp chúng ta lạc quan, yêu đời, vui vẻ để tiếp tục sống mỗi ngày. Tận hưởng cuộc sống - đó chẳng phải là hạnh phúc mà con người chúng ta luôn tìm kiếm mỗi ngày sao.
5
65552
2012-12-29 08:23:37
--------------------------
49865
9644
Cuốn sách này nằm trong series Hạt giống tâm hồn rất được nhiều người yêu thích. Và quả thực nó là một cuốn sách không nên bỏ qua. Nó đem đến biết bao khám phá thú vị và mới mẻ, với những trang viết giàu màu sắc và hình ảnh chân thực, kết nối tâm hồn với cuộc sống xung quanh mình. Cuốn sách tập trung vào phân tích lợi ích của thiên nhiên với sức khỏe, tinh thần của mỗi người, nhưng không đưa ra những lý lẽ khô cứng nhàm chán. Mà thay vào đó, cuốn sách để chúng ta tự tưởng tượng, dẫn dắt chúng ta vào những miền cảm xúc bất tận khi được đứng trước thiên nhiên tươi đẹp.
Nó thực sự hữu ích với những ai muốn tìm thêm nhiều niềm vui trong cuộc sống của mình.
5
20073
2012-12-10 16:07:30
--------------------------
