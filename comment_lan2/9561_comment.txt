476489
9561
Mình vừa mua thành công sản phẩm cuốn sách Lịch sử Việt Nam bằng tranh tập truyện Ngô Quyền đại phá quân Nam Hán. Mình mê nhân vật Ngô Quyền lâu lắm rồi. Ông có những chiến lược, chiến thuật rất thông minh, sáng suốt . Truyện tranh mô tả rõ nét hình ảnh, phác họa chân dung 1 vị lãnh đạo tài hoa . Truyện có bố cục rõ ràng. Khái quát sự ra đời, lớn lên, sự rèn luyện, khắc họa vị anh hùng văn võ song toàn cùng ý chí căm thù giặc. Truyện rất sát lịch sử. Mình cho 4 sao.
4
1468522
2016-07-20 21:02:06
--------------------------
162165
9561
Trận đánh trên sông Bạch Đằng dưới sự chỉ huy của Ngô Quyền đánh tan quân Nam Hán có thể được xem là trận thủy chiến hoành tráng nhất trong lịch sự Việt Nam chấm dứt thời kỳ đô hộ của Trung Quốc. Trận chiến này có thể sánh ngang với trận Xích Bích trong Tam Quốc Diễn Nghĩa. Trận chiến thể hiện sự chỉ huy tài tình của người anh hùng dân tộc Ngô Quyền (thể hiện qua sự tính toán được thủy triều lên xuống để chôn cọc dưới sông nhằm phá tan thuyền của quân Nam Hán) và sự đoàn kết, tinh thần đấu tranh giải phóng dân tộc của nhân dân ta.
5
356759
2015-03-01 17:58:38
--------------------------
