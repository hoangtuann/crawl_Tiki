378652
9088
Mình rất thích tạp văn của Nguyễn Việt Hà từ lúc đọc đàn bà uống rượu và đã đọc hết các cuốn tạp văn được xuất bản thành sách của ông, Tạp văn của Nguyễn Việt Hà có tất cả mọi thứ chuyện đông tây kim cổ, văn học nước ta, văn học trung quốc, văn học Nga, Pháp chuyện ca sĩ diễn viên người mẫu chuyện dại gia chân dài, chuyện nhà kinh doanh chuyện nghệ sĩ không thiếu một chuyện gì vậy mà không hề tạp, những câu chuyện đầy màu sắc không hề nhàm chán mang lại rất nhiều hiểu biết mà lại vô cùng hài hước.
5
306081
2016-02-06 17:41:49
--------------------------
344404
9088
Lúc đầu, tôi chọn cuốn sách này để đọc vì cảm thấy tò mò, hứng thú với tên sách và mục lục. Và rõ ràng, cuốn sách hay hơn tôi dự kiến, cảm giác rất phấn khởi, thú vị khi lật mở từng trang giấy. Nhiều lúc, nếu không bị ngắt quãng do công việc khác, có lẽ tôi đã đọc ngấu nghiến cho kì hết mới thôi. Tác giả vào tuổi cha chú của mình, lại là người Hà Nội, sống ở vùng đất kinh kì đã lâu, thế nên tôi học hỏi được rất nhiều từ ông, thêm yêu Hà Nội và những nét văn hoá nơi đây. Cuộc sống vẫn cứ trôi, luôn xô bồ và ồn ào, "Con giai phố cổ" là món ăn tinh thần hữu ích khi bạn muốn tìm cho mình cảm giác Hà Nội thật tuyệt vời như sách báo vẫn thường nói.
5
450250
2015-11-28 12:02:40
--------------------------
277835
9088
Đây là cuốn tản văn đầu tiên của Nguyễn Việt Hà tôi đọc. Đã bão hòa với thể loại tản văn luôn quanh đi quẩn lại với bàn tán về nhân vật, sự kiện cùng những bình luận ngẫm ngợi lặp đi lặp lại về ý tưởng, tôi không nhiều hào hứng khi bắt đầu đọc cuốn sách, nhưng càng đọc càng bị cuốn theo. Hành văn dí dỏm, hài hước với nhiều kiến thức văn học Đông Tây kim cổ, cách dẫn chuyện duyên dáng, khéo léo cứ kéo ta đi từ nhân vật này sang nhân vật khác, từ sự kiện này sang sự kiện khác khiến ta cứ tròn mắt hào hứng lắng nghe rồi gật đầu tắp lự với những bình luận, ngẫm ngợi của tác giả, hệt như bị thôi miên vậy.
Tuy vậy, đọc nhiều cũng thấy ngán với cách viết đảo tính từ lên trên danh từ, sự một tả hời hợt dễ dãi trong vài bài tản văn về phụ nữ...
Nói chung, tôi thích cuốn tản văn này, nhưng có lẽ nên đọc một cuốn tản văn của Nguyễn Việt Hà là đủ rồi.
3
404264
2015-08-25 12:58:05
--------------------------
170650
9088
Nguyễn Việt Hà có lối viết dí dỏm, từ ngữ chọc lọc và thể hiện cái nhìn sâu sắc và "xoáy" vào nhiều ngóc ngách trong đời sống xã hội Việt Nam nói chung và Hà Nội nói riêng. Mỗi cái kết của một mẩu "tạp văn" (đọc thêm về mẩu so sánh tạp văn, tản văn, và những thứ rẻo nhỏ đấy của anh) đều khá thâm cay và khiến mình suy ngẫm. Anh dẫn thêm những phần Đông Tây kim cổ, từ Lỗ Tấn đến nhân vật truyện Kim Dung để bóng gió lái sang chuyện thực đời thực và người thực. Kiểu như người đọc đang bận há hốc mồm say sưa nghe anh kể chuyện ở xứ tít mù, rồi bất chợt thinh lặng khi anh nối nó với chuyện ngõ ngách ngay sát nhà mình, kéo người đọc xuống mặt đất rất bình thản, rất "ăn nhập vào vấn đề"! 

Cái tựa Con giai phố cổ thực ra là một tựa tạp văn của anh, chứ "Con giai phố cổ" cũng có vài chuyện nói về đàn bà, con gái, phụ nữ khá hay. Mấy mẩu tạp văn cái nọ xọ cái kia, nhưng tính cách bông đùa hài hước của tác giả thì vẫn như thế, chẳng lẫn vào đâu được.
4
461845
2015-03-20 14:38:00
--------------------------
121295
9088
Đây là tạp văn của Nguyễn Việt Hà đầu tiên mình đọc. Ban đầu mình cũng không mấy hứng thú với thể loại tạp văn nhưng khi tò mò giở ra xem thì mình rất ấn tượng. Cách hành văn dí dỏm, hài hước, "khả năng càn lướt đề tài" và lối xài từ đúng chất "cao bồi Hà Nội". Đọc lần đầu dễ khiến người ta bật cười nhưng rồi lại chuyển sang ngẫm nghĩ mới thấy thấm "cái triết lý đặc sệt tinh thần đường phố Hà Nội", mới hiểu được rất nhiều những suy tư của Nguyễn Việt Hà về cái xưa - nay, cũ - mới, thiện - ác và nhất là tìm thấy ở đây một Hà Nội của "bọn con giai phố cổ"
5
92071
2014-08-18 20:45:22
--------------------------
