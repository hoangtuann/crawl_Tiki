65946
9635
Sách là một người bạn tri kỷ - cái điều mà ai cũng biết. Nhưng trong xã hội ngập tràn thông tin và đầy những rối ren cuộc sống này. Thật khó có thể tìm được một cuốn sách đem đến cho mình lợi ích thực sự! "Chỉ cần bạn chấp nhận cuộc sống, cuộc sống sẽ chấp nhận bạn" - một câu nói giản dị mà xúc tích. Thật không thể để thiếu đi sự vui vẻ, lạc quan để đối mặt với cuộc sống và con người thời đại cạnh tranh này. Cuốn sách này như một sự thu gọn sự lắng đọng của thời gian, giúp bạn bỏ đi những căng thẳng trong cuộc sống và tìm về với chính mình - với một con người hạnh phúc sẵn có! Đọc nó, cảm nghiệm nó, thực hành nó, cuộc sống của bạn sẽ thú vị biết bao, vui vẻ biết bao. Biết đâu sự vui vẻ ấy lại giúp bạn thành công hơn trong công việc cũng như trong cuộc sống? Chúc bạn vui, cảm nhận được hương vị đặc trưng của cuốn sách đặc biệt này và thành công hơn trong cuộc sống!
5
40641
2013-03-28 14:34:05
--------------------------
