408393
8486
Từ ngữ dễ hiểu, hình vẽ sinh động nhiều màu sắc và bắt mắt, nội dung rất phù hợp với trẻ học mẫu giáo. Giúp con học hỏi, bắt chước làm theo các nhân vật trong truyện để được khen ngợi. Các nhân vật trong truyện rất dễ thương và đáng yêu. Cốt truyện xoay quanh cuộc sống hàng ngày nên rất gần gũi với trẻ, giúp trẻ xử lý các tình huống gặp hàng ngày một cách tốt nhất. Rèn luyện tính tự giác, biết cách giữ gìn sức khỏe cho bản thân và cho những người xung quanh.
5
957838
2016-03-31 16:58:56
--------------------------
216385
8486
Mình đã mua 1 bộ cho bé đầu nhà mình cách đây khoảng 2 năm và bé rất thích bộ sách này, ngày nào cũng phải đọc vài câu chuyện trc khi đi ngủ mới chịu. Nhờ bộ sách mà bé biết giữ vệ sinh cá nhân, biết lễ phép với mọi người hơn,v.v..... Nhưng do thời gian lâu quá và "mật độ" giở sách liên tục nên sách bị hư vài quyển. Mình vẫn quyết định mua bù lại những quyển bị hư để đọc cho bé sau của mình nữa.
Quyển sách gồm nhiều câu chuyện nhỏ về chủ đề sức khoẻ. Lồng ghép với những nhân vật ngộ nghĩnh, đáng yêu giúp bé nhớ lâu. Hình ảnh đẹp, sinh động, dễ thương. Cuối mỗi câu chuyện còn có 2 câu hỏi giúp bé nhớ lâu và tập thói quen tóm tắt và ghi nhớ tốt hơn. Bộ sách vừa là truyện tranh vừa là những bài học nho nhỏ cho các bé dễ nhớ, dễ áp dụng.
5
663152
2015-06-27 17:24:31
--------------------------
