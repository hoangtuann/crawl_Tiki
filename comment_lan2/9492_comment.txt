426606
9492
Nếu đã xem phim '"cô gái với hình xăm rồng" thì khi chắc chắn bạn sẽ cực thích cách tạo hình của nhân vật Mikael Blomkvist và Lisbeth Salander trong phim với tài tử Daniel Craig và nữ minh tinh Rooney Mara. Một nhân vật nam với vẻ điển trai đào hoa, thông minh; nhân vật nữ tinh quái, có phần lập dị, và cực kỳ thông minh cứ như cô sở hữu cho mình cái IQ 200 của bác học. Trong phần kế tiếp "Cô gái đùa với lửa" này tác giả lại xây dựng thêm Salander cái kiểu cách thông minh và không kém phần lập dị ấy, cũng lỗi sống phóng túng thiên về cảm xúc nhân vật, cái hay mà không ít tác giả khác bắt chước nhưng không thể nào thành công. Mạch truyện có phần chậm, nhưng lôi cuốn. Thực ra lúc viết nhận xét này mình chỉ vừa đọc tới trang 300 của cuốn sách nhưng quyết định viết vì cái cách mà nhà biên tập phát hành tại VN đưa những ngôn từ và dịch thuật một cách cẩu thả làm mình bức xúc đến nỗi phải dừng đọc để làm công việc này. Điểm trừ duy nhất cho tới lúc này đó là những từ ngữ sau khi dịch cứ ngỡ như mình đọc bằng google translate, tuy không mắc phải bất cứ một lỗi chính tả nào, nhưng người đọc phải đọc đi đọc lại ít nhất là 2 lần 1 đoạn văn chỉ để hiểu được nội dung. Chưa kể là thay vì viết "tình dục"', sách lại viết là "tính dục" hay "dớ dẩn" ... những tiểu tiết này tuy có thể bỏ qua nhưng lặp đi lặp lại khiến mình thấy khó lòng chấp nhận. Điểm trừ cho nhà xuất bản.
Còn về nội dung chắc mình phải gửi nhận xét rồi hoàn thành nhanh nhất có thể để tiếp tục rút ra nhận xét.
4
125159
2016-05-07 11:00:44
--------------------------
303757
9492
Cô Gái Đùa Với Lửa là tập thứ hai trong bộ ba tác phẩm Millenium của tác giả Stieg Larsson viết về những góc khuất của xã hội Thụy Điển. Cũng như tập đầu tiên, tập thứ hai này rất cuốn hút, đưa người đọc theo chân bộ đôi Salander - Blomkvist khám phá bí ẩn của vụ giết người. Sách này xuất bản năm 2014 (tái bản) nhưng bản mình nhận được hơi cũ và bị ố vàng vài chỗ, có lẽ khâu bảo quản sách của Tiki chưa được hoàn chỉnh lắm. Vấn đề dịch thì cũng như các bạn đọc khác nhận xét, chưa được trau chuốt lắm.
4
154124
2015-09-15 23:15:59
--------------------------
299848
9492
Đến với " Cô gái đùa với lửa", là thêm một lần được trải nghiệm và phiêu lưu cùng tác giả bậc thầy trong truyện trinh thám, phiêu lưu Stieg Larsson. Sự thông minh, sắc xảo của nhân vật, kết hợp với lối dẫn truyện đầy bất ngờ, nhưng lại gần gũi với người đọc, lôi cuốn trong từng con chữ, khiến cho chúng ta khó mà có thể bỏ dở cuốn sách được, người đọc như bị cuốn theo vào cuốn sách theo từng chi tiết nhỏ nhất, mỗi nhân vật lại để lại một ấn tượng khác biệt, và đó cũng chính là sự thành công trong việc xây dựng nhân vật của Stieg Larsson.
5
172185
2015-09-13 15:44:17
--------------------------
280787
9492
Nếu như ở phần một "Cô gái có hình xăm rồng", tôi được biết đến cô gái Salander với trí thông minh phi thường, cùng với gia thế, xuất xứ bí ẩn thì sang phần 2 "cô gái đùa với lửa", giúp độc giả hiểu rõ hơn về Salander, giải quyết được thắc mắc tại sao một cô gái rất bình thường nhưng sở hữu trí tuệ cùng cá tính cực kì đặc biệt như vậy. Stieg Larsson rất thành công khi xây dựng được nhân vật Salander, một cô gái đầy cá tính, và có lẽ là cá tính nhất mà tôi từng được đọc, không chỉ riêng với Salander, những nhân vật khác đều có đất để thể hiện bản thân mình, mặc dù không cần dùng nhiều ngôn từ, không cần dông dài, nhưng mỗi nhân vật trong sách của ông đều để lại ấn tượng trong tôi. 
Về phần nội dung sách, thì với bản thân mình, cuốn sách cuốn hút từ đầu tới cuối, có bạn cho rằng đoạn ban đầu hơi lan man nhưng mình lại cảm thấy chẳng có một chi tiết nào gọi là thừa cả, ngay khi cầm cuốn sách lên, mình đã bị cuốn theo từng bước chân của Salander cho tới Blomkvist trong hành trình đi chứng minh sự vô tội cho cô bạn gái của mình, mặc dù có đôi lúc hơi khó chịu vì lỗi dịch thuật, nhưng tạm chấp nhận được. Cuốn sách không đơn thuần là trinh thám, bởi vì tác giả muốn mang lại một thông điệp đầy sức nặng, đầy tính thời sự: Đề Cao và Trân Trọng Phụ Nữ - một thông điệp của thời đại và Larsson đã làm rất tốt điều đó với ngòi bút của mình. 

4
304901
2015-08-27 22:07:04
--------------------------
279392
9492
Mình đã từng xem qua bộ phim Cô gái có hình xăm rồng và cảm thấy phim khá hay nên đã quyết định chọn mua tác phẩm này. Nội dung thì có vẻ ổn nhưng dịch thì khá lủng củng, rời rạc mà mọi người hay gọi là kiểu "google dịch" ý. Đáng ra với cách dịch này thì mình sẽ không cho cao như vậy nhưng xét về hình thức của cuốn sách và giấy in cùng với số lượng trang dày đặc như thế nên có lẽ mình sẽ cho 4 sao. Mong công ty phát hành cuốn sách này khi tái bản cuốn sách này sẽ chú tâm phần dịch hay hơn nữa để tác phẩm đến với tay đọc giả Việt được hoàn thiện hơn. Nhờ đó, giá trị của tác phẩm trong mắt người đọc cũng được nâng lên.
4
548542
2015-08-26 19:11:30
--------------------------
264766
9492
Trong quyển sách thứ nhất "Cô gái với hình xăm rồng" Lisbeth được khắc họa nhiều về tài năng và trí thông minh thì trong quyển thứ hai này, tính cách và quan điển sống của cô được khắc họa rõ nét hơn. Ngoài ra ở tập này, nhà văn Stieg Larsson đã gỡ từng nút thắt về tuổi thơ của Lisbeth, liên tiếp đưa ta từ bất ngờ này đến bất ngờ khác.
Phần mở đầu truyện hơi lan man nhưng khi vào đến cốt truyện chính thì tiết tấu trở nên nhanh và dồn dập hơn. Tác giả tạo hàng loạt các nút thắt, các bí ẩn khiến chúng ta không thể bỏ cuốn sách xuống và mong muốn được nghiền ngẫm cuốn sách để giải tỏa hết tò mò và khúc mắc. Stieg Larsson đúng một cây bút có tài kể chuyện và dẫn dắt tuyệt vời!
Ngoài cốt truyện tuyệt vời ra thì điểm trừ lớn nhất của sách khiến mình chỉ chấm 3/5 sao chính là dịch thuật. Đây là quyển sách dịch tệ nhất mình từng đọc! Lủng củng và nhiều câu tối nghĩa, không hiểu được ý của người dịch. Mình không hiểu người dịch và biên tập sao có thể cho ra một quyển sách chất lượng dịch kém như vậy. Nếu không vì cốt truyện xuất sắc có lẽ mình đã bỏ ngang quyển này vì chất lượng dịch. Mong là sách sẽ được tái bản với chất lượng dịch tốt hơn.
3
339399
2015-08-13 13:59:42
--------------------------
258341
9492
Người dịch:		Trần Đĩnh
NXB:			Phụ Nữ
Thể loại:		Trinh thám
Bìa:			Đẹp – Ấn Tượng
Chất lượng in, giấy: Tốt, sách dày, giấy nhẹ
Chất lượng dịch:	Tốt
Nhận xét:		Cuốn Cô gái có hình xăm rồng quá ấn tượng nên tôi mua luôn cuốn hai và ba ngay. Cuốn hai Cô gái đùa với lửa cũng hay ác liệt, ngốn ngấu có mấy ngày xong hơn 600 trang sách. Một lần nữa, cuộc phiêu lưu kỳ quặc của hai kẻ lì lợm Blomkvist và Salander đã chiếm hữu trái tim của bạn đọc khắp nơi trên thế giới. Stieg Lasson có tài trong việc đề cao phụ nữ Thụy Điển nói riêng và chị em trên thế giới nói chung.
Khuyên: 		Rất hay, phải đọc.

5
129879
2015-08-08 14:19:11
--------------------------
254196
9492
Tiếp nối thành công của Cô gái có hình xăm rồng, Stieg Larsson trở lại với siêu phẩm Cô gái đùa với lửa. Lần này Stieg Larsson miêu tả rõ nét hơn một cô gái tên Salander Lisbeth. Cô ấy thực sự làm mình bất ngờ. Dưới bộ mặt ấy là một con người mạnh mẽ đến lạ thường nhưng cũng không kém phần cô độc. Vẫn là những màn rượt bắt, truy đuổi sát sao đến nghẹt thở, những tên tội phạm đáng bị trừng phạt lẩn trốn ngày đêm âm mưu hãm hại lại những người bảo vệ chính nghĩa bằng cách của họ, quá đỉnh.
4
120193
2015-08-05 08:10:57
--------------------------
249146
9492
Ở quyển sách này tác giả Stieg Larrson đã miêu tả rõ hơn về nhân vật Salander Lisbeth. Một cô gái tưởng chừng như thật kì dị và chẳng giúp ích được gì cho đời nhưng chớ nên đánh giá vội. Nội tâm cô gái này như là cả một thế giới phức tập và không dễ gì cho mọi người xâm nhập vào, nhưng tác giả đã miêu tả một cách khá là đặc sắc và lôi cuốn. Ở nửa đầu quyển sách thì cũng chưa có gì hay, hơi lê thê với cách dẫn truyện nhập nhằng nhưng ở nửa sau quyển sách khá là kịch tính và gây cấn. Cô nàng Salander này đã làm cho người đọc ngạc nhiên với tính cách mạnh mẽ , khả năng vi tính siêu hạng cùng đời sống tình dục khá là phóng khoáng nhưng sâu trong tâm trí cô vẫn chỉ là cô gái độ 25,rất muốn yêu và được yêu , nhưng những vết thương trong lòng cùng tuổi thơ không mấy tốt đẹp đã làm cô phải khép kín lại tâm hồn của chính mình và chẳng cho ai bước vào được.
5
621509
2015-07-31 14:08:35
--------------------------
245558
9492
"Cô gái đùa với lửa" là câu chuyện trinh thám về vụ án mà chính Lisbeth Salander là kẻ tình nghi số 1 và bị truy nã trên toàn quốc. Cực kỳ gay cấn? Tất nhiên, nhưng cũng như tập 1, sự gay cấn chỉ diễn ra ở nửa sau cuốn sách. Nửa đầu vẫn chậm chạp, lề mề với đủ thứ chuyện con ong cái kiến như Lisbeth Salander đi mua đồ IKEA, trang trí nội thất, làm sandwiches, pha cà phê... Cùng với những câu chuyện về nạn buôn người và ngành công ngịiệp tình dục. Cũng không sai khi nói rằng Lisbeth Salander chính là nhân tố duy nhất đủ hấp dẫn để giữ độc giả tiếp tục tập trung đọc sách cho đến khi... câu chuyện chuyển biến và trở nên hay ho hơn. Thực sự rất ấm lòng khi cô gái lập dị và cô độc ấy, khi bị giới truyền thống cả nước truy đuổi và bôi nhọ, vẫn có những người bạn thực sự đứng lên tin tưởng và ủng hộ cô (dù trước đó cô đã chuồn ra nước ngoài suốt cả năm trời, không chào tạm biệt).
Trong cuốn sách này có một nhân vật đặc biệt dị thường là một gã khổng lồ không biết đau. Nhưng điều khiến độc giả cảm thấy khó tin hơn nữa là các nhân vật "biết đau" lại cũng chẳng dễ hạ gục chút nào: dù bị đạn găm vào não hay rìu bổ xuống nửa đầu, họ vẫn sống nhăn. Kỳ diệu!
4
293599
2015-07-28 22:56:00
--------------------------
211466
9492
Một lần nữa ta có dịp khâm phục tài năng của Stieg Larsson, thông qua nhân vật nữ chính ông đã giúp ta hiểu thêm về bối cảnh xã hội của các nước Bắc Âu, không hề bình lặng như vẻ ngoài của nó. Dù là truyện trinh thám nhưng cũng đầy ắp tính nhân văn.
Cũng giống như quyển đầu tiên thì những trang đầu tiên có thể làm bạn quyết định cất quyển sách vào một góc nào đó, nhưng chỉ cần một chút kiên nhẫn, vượt qua được nó bạn sẽ có được một câu truyện với tiết tấu nhanh hơn và vô cùng lôi cuốn. Salander lại một lần nữa gây ấn tượng với bạn đọc bằng sự mạnh mẽ, bằng sự thông minh và cả những góc khuất bên trong nội tâm của cô nữa. Có lẽ không ít những người như tôi, càng đọc càng thấy yêu quý Salander hơn, càng mong muốn được theo bước cuộc hành trình của cô qua từng trang sách.
Và điểm trừ, tiếc thay, có lẽ lại là dịch thuật, Một vấn đề có thể làm hỏng đi cả một cuốn sách, có thể khiến nhiều người đánh giá không cao về cuốn này. Nhưng nếu bạn đã đọc xong Cô gái có hình xăm rồng thì đừng ngần  ngại rinh về Cô gái đùa với lửa nhé, không hề uổng đâu!
4
459919
2015-06-20 16:39:42
--------------------------
158026
9492
Tuy không phải là fan hâm mộ những bộ truyện trinh thám phá án vì những vụ án khiến tôi sợ hãi, nhưng Cô gái đùa với lửa đã kích thích trí tò mò của tôi và tôi đã bị cuốn hút vào những tình tiết hấp dẫn của câu chuyện. Mặc dầu ban đầu Lisbeth không có nhiều ấn tượng đẹp trong tôi, chỉ là một cô gái kì lạ, cá tính và hơi không bình thường, nhưng theo thời gian, những yếu tố tốt đẹp trong cô dần được bộc lộ. Tôi không thương hại cô bởi cô có một quá khứ tổn thương,nhưng tôi khâm phục cách cô vượt qua khó khăn ấy. Quả là một cô gái thông minh, và hấp dẫn. Tình tiết truyện thì liên tiếp có nhiều nút thắc, nhiều câu hỏi, nhiều bí ẩn cần được khám phá. Tuy nhiên, điểm trừ là đôi khi dịch giả dịch câu văn tôi không hiểu ý nó muốn nói gì. Có lẽ khâu dịch thuật không kĩ đã khiến cho một câu chuyện trinh thám mất đi một phần hấp dẫn của nó. Hi vọng đợt tái bản sẽ có sự điều chỉnh này từ NXB.
3
63376
2015-02-11 07:51:30
--------------------------
141789
9492
tôi là 1 fan của bộ tiểu thuyết Millennium và đã đọc tập 1 cùng phần đầu tập 2 bản tiếng anh và cực kỳ thích thế nhưng bản dịch này thật sự quá tệ. Tôi không hiểu người dịch có làm việc đàng hoàng hay không hay có biên tập viên hay không nhưng thực sự là bản dịch này quá khó đọc. Cuốn sách đầy những câu tối nghĩa, diễn đạt không ra tiếng việt (đấy là mới đọc đến 1/4) khiến người đọc thật sự là phải nhăn mặt. Cảm giác như ăn 1 bát cơm đầy sạn vậy. Thật sự rất buồn khi phải chấm cuốn sách mình thích có 2/5 sao nhưng bản dịch này thật sự phá hỏng 1 cuốn truyện trinh thám xuất sắc
2
90637
2014-12-16 18:38:07
--------------------------
140952
9492
Lisbeth Salander là một thiên tài bị cả xã hội nhìn nhận như một người thiểu năng tâm thần, không đủ sức để tự cai quản bản thân mình. Nếu như qua phần 1, tính cách của nhân vật Salander chỉ được khắc họa qua những việc mà cô trải qua, sự cứng rắn, lạnh lùng của cô đối với xã hội thì ở quyển 2, Salander có chiều sâu hơn rất nhiều. Ở Cô gái đùa với lửa, Salander là một con người có đạo đức công việc và đạo đức sống, người đọc hiểu được động cơ, luồn lách theo cách suy nghĩ của bộ óc thiên tài đấy để rồi hiểu được cách sống bị xem là bệnh hoạn và tàn nhẫn của cô.
Stieg Larsson có cách viết tiểu thuyết thông mình và dồn dập. Câu chuyện của ông thật đến từng điếu thuốc lá, từng cái bánh pan pizza trong giỏ hàng của nhân vật, mọi thứ đều ngăn nắp, đề logic và khoa học. Ông không viết văn một cách ngẫu hứng, mà ngược lại, Stieg Larsson là một nhà báo khoa học viết văn. Ông thấu hiểu những suy nghĩ và ham muốn của người phụ nữ với con mắt bình quyền, đầy tôn trọng.
Người Việt Nam chúng ta khi đọc bộ ba tiểu thuyết này sẽ có nhiều ý kiến trái chiều về cách sống đặc biệt là của người phụ nữ. Tuy nhiên, tác phẩm đã mang đến một thông điệp trân trọng và cảm thông đối với người phụ nữ cũng như suy nghĩ và sự lựa chọn của họ.
5
458765
2014-12-13 10:38:09
--------------------------
113215
9492
Đọc phần đầu, mình cảm thấy hơi chán vì truyện thuần miêu tả chuỗi ngày của Salander. Nhưng khi vượt qua những trang đầu rồi, bạn không những sẽ hiểu hơn các phần sau, mà còn bị cuốn vào guồng câu chuyện và không cách nào dứt ra được.
Truyện phác hoạ nên cả một làn sóng ngầm dưới lớp vỏ bọc Dân chủ và hoàn hảo của xã hội Thụy Điển: nơi con người kì thị lẫn nhau, nơi những người tưởng chừng thanh liêm nhất lại ở dưới vũng sâu của tội ác,... Càng đọc, bạn sẽ càng hăng, vì các tình tiết cứ thắt lại, bí ẩn chồng bí ẩn, dày lên như một màn sương. Số lượng nhân vật trong cuốn 2 cũng nhiều hơn cuốn một, đem đến sự đa diện nhiều chiều cho quyển sách.
Mình thấy quyển hai thiên về hành động nhiều hơn, đọc mà adrenaline muốn tràn ra ngoài:), nhưng việc phá án, khám phá bí mật thì cá nhân mình thấy cuốn 1 làm tốt hơn:)
5
62260
2014-05-27 22:20:00
--------------------------
76966
9492
Mình hoàn toàn khâm phục tài năng văn chương Steig Larsson. Ông đã tạo nên những tác phẩm mang màu sắc trinh thám cực kỳ hấp dẫn và lôi cuốn, với nhiều những chi tiết độc đáo, mới mẻ. Thêm vào đó ông luôn có cách riêng biệt để nhìn nhận những vấn đề hóc búa, gai góc, nên cuốn sách nào được ông viết ra đều có một chất rất riêng, không thể pha trộn. "Cô gái đùa với lửa" là một tác phẩm như thế. Trong tập hai này, tác giả tiếp tục đưa người đọc đến với cuộc hành trình của cô gái kỳ lạ Lisbeth Salander, một con người đã chịu nhiều tổn thương về thể xác lẫn tinh thần. Những đoạn miêu tả tâm lý, hành động, và những dòng hồi ức về tuổi thơ Salander được tác giả viết một cách xuất sắc, làm nổi bật được chiều sâu trong tính cách nhân vật. Qua hình ảnh Salander, qua những khắc họa chân thực trong tác phẩm, người đọc thấy được một phần trong xã hội Thụy Điển: một xã hội không bình yên, luôn tồn tại đây đó những tội ác nhằm vào phụ nữ. Tác phẩm này, bên cạnh chất trinh thám ly kỳ, còn là một tiếng nói mạnh mẽ bảo vệ nhân quyền, thể hiện cái nhìn đầy chất nhân văn của tác giả.
5
109067
2013-05-25 08:14:19
--------------------------
61646
9492
mình đã nhảy cóc khi không đọc cuốn số 1: cô gái với hình xăm rồng mà đọc luôn cuốn số 2, cô gái đùa với lửa
đây là một cuốn truyện trinh thám mà theo mình đánh giá khá hay: cách mô tả tâm lý, cách những nhân vật đưa Libeth vào tròng của sự dối gian và mưu mô. Bản năng sinh tồn và ý chí đạo đức trên 1 khuôn khổ cô tạo ra
Ấn tượng ban đầu mà Libeth tạo ra cho người đọc có lẽ cô tâm thần thật, rối trí và chẳng khác nào 1 cô nàng quái dị với đầy hình xăm và khoen khắp nơi trên cơ thể. Nhưng tài năng không thể phủ nhận, cái khoảng thời gian cô trải qua khi còn thơ ấu lần lượt mở ra khiến ta cảm phục và xót thương cô nhiều hơn.
Đây quả thật là cuốn tiểu thuyết trinh thám cuốn hút và hấp dẫn mà bất cứ ai yêu thích thể loại này không nên bỏ lỡ
Điểm trừ cho bộ truyện này có lẽ thiên về văn phong dịch nhiều hơn, không hiểu sao từ tính dục làm mình khá khó chịu khi đọc nó. Một số câu chửi thề mặc dù khắc họa được nhân vật nhưng nếu tác giả dịch 1 cách nhẹ nhàng hoặc với tần số ít đi đôi chút thì câu chuyện đã hay hơn nhiều.
Ngoài ra, đây là truyện của Thụy Điển nên các tên nhân vật khá dài, khó nhớ, và truyện lại có đặc điểm có quá nhiều nhân vật xuất hiện. Điều cuối cùng mà mình không thích đó là hơn 1/5 cuốn sách mô tả lại về vụ việc của tập 1: cô gái với hình xăm rồng. Nếu muốn đọc lại cuốn số 1 thì dám chắc mình đã nắm được toàn bộ nội dung của tập đó.
Tuy nhiên vì sự khắc họa nhân vật thông minh, mình cho cuốn sách này 8/10 điểm
4
15919
2013-03-03 11:24:36
--------------------------
47159
9492
Đây là tác phẩm đầu tiên của nhà văn này mà mình đọc. Một câu chuyện trinh thám, kết hợp yếu tố ly kỳ, tâm lý, làm mình bị cuốn hút từ đầu đến cuối. Tác giả đã rất tài tình khi kể một câu chuyện tưởng chừng bình thường, nhưng lại ẩn dấu biết bao điều bất ngờ và không tưởng. Với ngôn từ và cách diễn đạt thẳng thắn, nhưng không kém phần uyển chuyển tinh tế, câu chuyện thực sự có một sức nặng, làm độc giả nghẹt thở đến giây phút cuối. Thêm vào đó là chiều sâu ý nghĩa nhân văn mà tác phẩm gửi gắm, cũng làm mình rất hài lòng, vì đây không chỉ là một câu chuyện giải trí thông thường, mà để lại rất nhiều dư vị khó quên.
Nhưng mình không thích bài cuốn này cũng như những cuốn cùng series, quá đơn giản và không mấy ấn tượng. Nếu Nhã Nam xuất bản cuốn này có lẽ mình sẽ thích hơn.
4
20073
2012-11-22 14:58:55
--------------------------
