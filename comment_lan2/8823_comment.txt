560117
8823
Xin chia sẽ cùng mọi người vài cảm nhận của mình về tác giả.
Mình may mắn mua được 1 quyển sách rất mỏng của ông. Tuy "nhỏ" nhưng mình đọc từng câu từng chữ đều rất tâm đắc. Sau này mình tìm thêm vài cuốn khác nữ. Nhưng mà lần này mình đọc 1 quyển cũng phải rất lâu mới đọc xong.
Tác giả là một người rất uyên thâm nên sách của ông phải đọc và suy ngẫm mới có thể hiểu được. Hy vọng mọi người có thể cùng thảo luận những gì bản thân tâm đắc nhé.
Chúc mọi người ngày mới vui vẻ.
WW
5
291395
2017-03-31 11:21:19
--------------------------
497509
8823
Tôi mua quyển này cốt để luyện viết mà thiết nghĩ đó cũng là mong muốn chung của những bạn đọc giả khi nhìn qua tựa sách này.
Tôi có vài lời nhận xét sơ sài, nhưng tôi nghĩ là quan trọng.
Quyển sách chỉ ta cái khẩu quyết, nghĩa là cái định hướng giúp ta đừng lạc lối trên con đường viết văn.
Với những bạn cần 1 quyển sách để đọc vào mà hành văn hay hơn, học lấy cái phong cách viết thì quyển Để Thành Nhà Văn này không đáp ứng được. Mà tôi nghĩ cũng chẳng có quyển sách nào đem lại cho bạn được việc ấy. Chỉ bằng cách đọc lấy thật nhiều, ngẫm nghĩ thật sâu những tác phẩm văn học Việt Nam, ghi chép cẩn thận lại những cách dùng từ hay, câu cú lạ cũng như không ngừng rèn luyện viết văn thì bạn mới mong sở đắc được một ngòi bút có nội lực.
Tôi có 1 vài tác giả mà cả ngôn từ lẫn văn phong đều vào hàng tinh túy của Việt Nam, bên tả chân có Vũ Trọng Phụng, Nguyễn Tuân, Nguyễn Công Hoan,...bên lãng mạn có Tự lực Văn Đoàn,...và còn nhiều tên tuổi khác nữa. Tôi chắc rằng khi bạn đọc lấy 1 ông bạn sẽ được "dẫn dắt" đến những ông khác. Mà một khi đã "dính" vào cái thú vui này rồi thì khó lòng mà dứt ra được. 
Hy vọng đôi dòng này sẽ có ích đối với bạn đọc.
5
1550020
2016-12-21 15:10:44
--------------------------
443396
8823
Về hình thức: sách bìa gập đựoc in trên giấy xốp Phần Lan ngà vàng không lóa mắt, mỏng và nhẹ nên cầm lâu ít bị mỏi tay. Bìa sách đơn giản, trang nhã. Khổ chữ vừa phải không quá nhỏ. Đánh giá chung về hình thức là ổn.

Về nội dung: sách chủ yếu được chia làm 2 phần bàn về: thuật viết văn và thuật bình luận.

Thứ 1, về thuật viết văn: Thầy Thu Giang hướng dẫn cặn kẽ cách viết từ một bài văn ngắn đến 1 tác phẩm văn học. Tôn chỉ chung ở đây là văn phong giản dị, tránh quá rườm rà cầu kỳ lập dị.

Thứ 2, về thuật phê bình: yêu cầu đặt ra là phải có Tâm và Tầm. Tâm tức là phải có óc nhận xét khách quan đánh giá công bình dù là tác phẩm của tác gia mình yêu thích cũng phải có cái nhìn không thiên lệch vì đó là lương tâm, là đạo đức của nhà phê bình.
Còn Tầm nghĩa là trình độ khi phê bình phảo ngang hoặc hơn tác gia, ngoài ra còn yêu cầu văn phong thanh nhã, óc tinh tế sáng suốt nhạy bén.

Trên đây là một số ý kiến của tôi sau khi đọc tác phẩm lần đầu. Vì đây là tác phẩm có chiều sâu với dung lượng kiến thức khá lớn nên việc đọc nhiều lần rất được khuyến khích. Hy vọng vài dòng chữ ít ỏi này có thể giúp ích được phần nào cho quý độc giả.

Cá nhân tôi từ lâu đã ngưỡng mộ văn phong và sự uyên bác của Thầy Thu Giang nên khi một tác phẩm của Thầy về thuật viết văn và phê bình được xuất bản, tôi không hề do dự sở hữu nó, và kết quả là khi cầm quỷên sách trên tay tôi cảm thấy thật mãn nguyện.

Xin thành kính tri ân Thầy Thu Giang vì tấm lòng bao la cùng học thức uyên thâm của Thầy là những tài liệu, nguồn kiến thức quý giá cho hàng hậu học chúng con.
5
745842
2016-06-06 20:29:28
--------------------------
420952
8823
Cuốn sách tuy mỏng nhưng ngôn ngữ dung dị, sắc sảo. Những câu chữ như muốn nói tâm can cho những ai cầm viết hành nghề viết văn.
Ngoài ra, những lời gan ruột chỉ dạy của Thu giang Nguyễn Duy Cần bên ngoài cho ai viết văn còn cho bất cứ ai muốn trở thành người tử tế.
Bởi lẽ, khi có một tâm thế như một nhà văn mà Nguyễn Duy Cần đòi hỏi thì ắt rằng người đó sẽ rất tử tế.
Những câu nói như Hạng tầm thường không làm sao hiểu nổi hạng vĩ nhân phi thường, nên họ chỉ nhìn thấy tiểu tiết, những cái tầm thường giống họ mà thôi. Cho thấy sự bất công trong phê bình một tác phẩm văn chương. Cũng như cho thấy kẻ thiếu kiến thức, nông cạn trong cái được nói được bàn tới mà đã chê bai chỉ trích người khác.
Hay, quá tuyệt vời cho những ai suy ngẫm về cuộc sống chứ không riêng chuyện viết văn.
4
317483
2016-04-24 09:29:14
--------------------------
417280
8823
Thật sự khi mình tìm đoc quyển sách này, vì hi vọng sách sẽ chỉ cho mình những bí quyết, kĩ năng để giúp mình viết văn hay hơn. Nhưng thực sự không phải. Xuyên suốt toàn bộ quyển sách là đức tính của nhà văn chân chính. Viết vì nghệ thuật, vì con người hay vì vật chất.
Tuy sách mỏng, nhưng để hiểu hết nó thì chắc chỉ có nhà văn mới hiểu được thôi.
Chất lượng giấy thì cũng giống bao quyển sách của cụ Thu Giang Nguyễn Duy Cần, giấy nhẹ, vàng nhưng dễ bị ố.
3
125876
2016-04-16 17:33:52
--------------------------
392409
8823
Mua quyển sách này khá lâu và đọc cũng được vài bận nhưng nói chung là có lẽ tác giả với con mắt của một nhà nghiên cứu nên viết sách đọc hơi khô khan khó hiểu tí. Đọc lần đầu tiên là mình còn lớp 11, khi ấy vẫn chưa hiểu rõ được tường tận. Bây giờ đã lên đại học đọc lãi vẫn thấy nhiều thứ vẫn còn chưa thông. Nhưng quả thực đây là một quyển sách hay. Những góc nhìn nhận của tác giả khá tinh tế và có những lời khuyên, lời nhận xét chân thành. Cuối sách còn có những trích dẫn mình cho là khá hay và bổ ích dành cho những người thích văn học. Phần trích dẫn này cho thấy tác giả là một người đọc rộng hiểu sâu, tinh tường những tác phẩm từ trong nước đến thế giới. Nếu có cơ hội bạn hãy sắm một quyển này và để dành chiêm nghiệm từ từ nhé !
4
92965
2016-03-06 21:28:38
--------------------------
300890
8823
thật sự muốn viết một cái gì đó để thỏa mãn lòng sáng tạo,lòng đam mê, nhưng thật sự để trở thành một nhà văn quả thực vô cung khó khăn, viết được một cuốn sách hay truyền được một tư tưởng cho người đọc thật không dễ. thấy sai lầm lớn nhất của nhiều người là viết ra chỉ đơn giản là được xuất bản, được người khác biết đến mà không phải viết bằng cái tâm của mình. sách của cụ quả thật đọc để ngẫm chứ không phải đọc để mua vui, qua chuyện, rất là hâm mộ cụ, hỏi rằng thế gian này có bao người có tâm viết sách như cụ Thu Giang....?
5
191095
2015-09-14 12:05:13
--------------------------
275930
8823
Giống như cuốn tự truyện nho nhỏ tổng hợp những kinh nghiệm của chính cụ Thu Giang, "Để thành nhà văn" sẽ giúp ta hệ thống lại kiến thức của mình và giúp ta định hướng lại mục đích của ta trong nghiệp viết lách. Kể cả bạn không mong muốn một cuộc đời gắn liền với văn học và nghĩ suy, vẫn nên đọc qua một lần để cảm nhận về cái gọi là chữ ĐỨC trong cuộc sống mà mỗi người phải chết để miệng sống mang theo. Bài học không bao giờ cũ. theo mình là vậy.
 
3
292321
2015-08-23 14:10:29
--------------------------
227132
8823
Mình là một người theo ngành kinh tế, nhưng thích cụ Cần nhiều lắm. Vì những tư tưởng cụ Mang lại cổ rồi nhưng vẫn tồn tại vượt thời gian. Mình mua cuốn này không phải vì mình muốn thành nhà văn, nhưng cái tư tưởng mà cụ dạy trong đó rất cần thiết cho mọi người, về nhiều phương diện chứ không chỉ là để làm nhà văn. Nếu bạn nghĩ đây là một cuốn sách hướng dẫn cách viết văn thì không phải. Mà là dạy tố chất thì đúng hơn. Và nhiều điều hơn là chỉ để làm nhà văn không mà mình rút ra từ cuốn sách mỏng này. Sách mỏng nhưng đọc rất lâu, vì nó ẩn chứa rất nhiều điều. 
5
74132
2015-07-13 15:43:54
--------------------------
213204
8823
Mình mua cuốn sách này trên tiki để tặng cho đứa em của mình, nên có đọc ké. Sách mỏng, nhỏ, phù hợp cho người mới bắt đầu bước chân vào nghề. Sách viết ngắn gọn nhưng phải chăng vì thế mà khá chung chung, đơn giản và sơ sài quá ? Sách không bàn sâu vào vấn đề để trở nên tài năng khi viết văn mà chỉ bàn về đức tính cần có của một nhà phê bình văn học. Cá nhân mình không thích cuốn này bằng những cuốn sách khác của Nguyễn Duy Cần. Mình nghĩ chỉ người mới bước chân vào nghề thì đọc để biết thêm vậy thôi.
3
114793
2015-06-23 15:23:59
--------------------------
202304
8823
Tác phẩm này được Thu Giang viết ra, như thể là một tuyên ngôn của ông, dành cho những ai có ý định dấn thân vào nghiệp viết lách. Để thành một nhà văn, như trong tác phẩm này đã nhẫn mạnh, chúng ta không phải chỉ viết một cách đơn thuần bằng những câu từ hoa mỹ, mà còn phải bằng cái tâm. Mình thích nhất ở chỗ ông chỉ ra rằng: người tác giả trước hết phải có một chuyện cực kỳ tâm huyết, để có thể kể ra bằng cả tâm can của mình, với toàn thể độc giả. Sau khi có chuyện để nói, tác giả phải nung nấu nó, hoàn thiện nó, suy nghĩ hoàn thiện nó. Sách tuy viết đơn giản nhưng súc tích, ý vị và dễ hiểu - như phong cách thường thấy của Thu Giang. Chúc các bạn tìm được động lực cho nghiệp viết lách.
5
564333
2015-05-29 10:45:50
--------------------------
179476
8823
Cuốn sách này không như mình mong đợi của thầy Thu Giang Nguyễn Duy Cần. Đối với những bạn ít đọc về lý thuyết viết lách, phê bình văn học thì có thể thấy cuốn sách khá hay nhưng với mình thì nó chưa đủ để mình cho vào sách gối đầu giường, nghĩa là cần đọc đi đọc lại, nhai đi nhai lại. Nội dung khá là chung chung và đơn giản, có lẽ đọc để có thêm cảm hứng và nghĩ tích cực về sự viết.
Mình mong chờ Việt Nam mình sẽ xuất bản nhiều sách về bản thân việc viết lách hơn trong tương lai.
2
457880
2015-04-07 12:24:37
--------------------------
141854
8823
Viết lách không chỉ là một sở thích mà đối với mình, nó còn là một công việc, một niềm đam mê. Thế nên, mình luôn cần những tài liệu hướng dẫn, chỉ bảo về việc viết văn, nhưng những loại sách như thế này lại khá khó tìm ở thị trường sách Việt Nam. May sao mình đã tìm thấy cuốn này của tác giả Thu Giang Nguyễn Duy Cần. Sách viết ngắn gọn, dễ đọc và dễ hiểu nhưng mình chắc chắn là các bạn vẫn phải đọc đi đọc lại nhiều lần thì mới nhớ hết được, bởi vì những thứ cần thiết để trở thành một nhà văn không phải ít. Ngoài ra, sách còn có thêm phần hướng dẫn trở thành một nhà biên tập nữa, mình thì lại không hứng thú với mảng này nên chủ yếu đọc cho biết.
5
37817
2014-12-16 21:20:39
--------------------------
121109
8823
Trong sách này, tác giả chỉ chủ yếu trình bày về một khía cạnh là nhà phê bình văn học thôi. Trong khi nhà văn thì lĩnh vực cầm bút phải rộng hơn nhiều. Đồng thời một nhà văn phải luyện tập, tìm tòi để viết ra sao cũng không được đề cập đến mà chỉ nêu vỏn vẹn vài đức tính mà một nhà phê bình văn học cần có. Theo tôi nên đổi tựa sách là “Những đức tính cần có của một nhà phê bình văn học” thì đúng hơn. Dù sao đi nữa, đọc sách này ta cũng thấy được tấm lòng của tác giả, một người cầm bút rất quan tâm đến nền văn học nước nhà và mong muốn nước ta sẽ có những tác giả xứng đáng với danh từ đẹp đẽ mà mọi người thường gọi: “nhà văn”.
3
277587
2014-08-17 10:20:29
--------------------------
