321163
9749
Mình là bác sĩ mới ra trường và làm tại khoa nội tiết. Dù đã được học và đọc rất nhiều về nội tiết nhưng mình vẫn chọn mua quyển này để có thể cảm nhận được những kiến thức hàn lâm được nhìn dưới con mắt của một văn sĩ - bác sĩ. Quyển sách viết rất gần gũi, như một câu chuyện nên cũng giúp mình một phần trong cách giải thích với bệnh nhân vì nội tiết học đã khó và giải thích để bệnh nhân hiểu được còn khó hơn rất nhiều . Cảm ơn Giáo sư rất nhiều !
5
266671
2015-10-13 13:06:13
--------------------------
310506
9749
Cuốn sách viết về đề tài y học, những tưởng khô khan, khó hiểu; nhưng dưới ngòi bút tài hoa đầy tính nghệ sỹ của giáo sư Nguyễn Chấn Hùng thì trở nên rất thú vị, bình dị và dễ hiểu với đại đa số người đọc. Việc lấy ví dụ trực tiếp là các nhân vật nổi tiếng trên thế giới cũng giúp người đọc chú ý nhiều hơn.  Đây là một cuốn sách y học hữu ích dành cho cộng đồng, giúp mọi người nâng cao hiểu biết và bảo vệ sức khỏe . Xin cám ơn giáo sư. 
5
710306
2015-09-19 16:39:30
--------------------------
292040
9749
 Là một sinh viên y, cá nhân tôi rất ấn tượng với cuốn sách này. Việc ghi nhớ các loại tuyến, các loại hormon, không phải điều dễ . Nhưng với sự so sánh rất tài tình, cùng với những câu chuyện nhỏ liên quan đến tác dụng của từng loại tuyến khiến việc ghi nhớ trở nên dễ dàng hơn. Cuốn sách khá hay, ngôn ngữ dễ hiểu không bác học nên ai cũng đọc và hiểu được. 
 Giấy in đẹp và nhiều hình minh hoạ nên đọc cũng đỡ nhàm chán, không có cảm giác như đọc một bài nghiên cứu khoa học vĩ đại, khó hiểu.
5
428793
2015-09-06 22:43:14
--------------------------
272722
9749
Mình mua cuốn sách qua sự giới thiệu của một người bạn, đây thật sự là cuốn sách rất hay, và mình bị cuốn hút bởi cách viết rất dễ đọc, nhiều liên tưởng, với giọng văn rất gần gủi của Giáo sư, những khái niêm khô khan của y học được tác giả chuyển thành hình ảnh rất dễ nhớ, cùng với những ví dụ được đưa ra rất hay đôi lần khiến tôi phải bật cười. Kỳ Diệu Dàn Hòa Tấu Nội Tiết của Giáo sư Bác sĩ Nguyễn Chấn Hùng thật “Dễ thương và ảo diệu”. Tôi rất hài lòng với cuốn sách này!
5
342197
2015-08-20 12:33:48
--------------------------
196562
9749
Mình học  ngành điều dưỡng nên việc chọn lựa sách sao cho phù hợp và hiểu sâu hơn về chuyên ngành là điều cần thiết. Kỳ diệu thay mình được giảng viên giới thiệu cho quyển này, đọc rất hay và rất có ích. Phải nói là có những điều giảng viên không hề nói trên lớp nhưng trong quyển sách lại có, thật là thú vị. Bìa sách khá đẹp, màu nhìn rất tương đối, không bị chói mắt nhưng giấy hơi mỏng. Mình đã mua thêm cuốn “Cẩm nang phòng trị ung thư” của tác giả này luôn, cũng hay lắm đó, mong rằng sẽ có được những cuốn sách thú vị hơn nữa.
4
13723
2015-05-16 07:47:16
--------------------------
157297
9749
Lâu rồi mới có một cuốn sách y học khiến mình đọc mê mẩn như vậy. Sách được viết bởi một giáo sư nhưng không hề khô khan triết lí mà trái lại rất sâu sắc và hấp dẫn. Lượng thông tin mang lại vô cùng bổ ích, lại cụ thể và dễ hiểu nữa. Giá của quyển sách này không đắt nhưng lợi ích của nó thì không đếm xuể. Đọc sách, ta như được làm giàu thêm vốn kiến thức, vừa có động lực để hăng say tập luyện thể thao, ăn ngủ điều độ. Mong rằng ai cũng có thể đọc và làm theo, để ngày càng tự mình nâng cao sức khỏe
5
352150
2015-02-08 11:06:31
--------------------------
