68372
9315
"Đi hết hai trăm trang sách,ta biết được đâu là bản tánh của tâm thức, từ đó, tránh được ba sai lầm (tà kiến) lớn nhất của đời người là tri giác sai lầm, suy tư sai lầm và kiến thức sai lầm. Đọc để có cái nhìn khách quan về thực tại, từ nhu cầu, đạo đức con người đến xây dựng xã hội hài hòa." Tôi luôn tự hỏi rằng đâu là an vui giữa đời thực? Và tôi đã tự tìm kiếm câu tả lời cho mình, trong quyển sách này. Đọc nó, cái thiện bao trùm khắp quanh tôi. Cho dù là về hành động, lời nói hay ý thức thì tôi cảm thấy chúng đến với tôi đều quá yên bình. "Con Người Toàn Diện, Hạnh Phúc Toàn Diện" cuốn sách tạo nên sự yên bình cho tất cả mọi người cũng như cho tôi. Nó là một cuốn sách tốt, hợp với những ai muốn tìm kiếm niềm vui, lẽ đời và nó cũng vừa với túi tiền của tất cả mọi người. Đây là một cuốn sách hay và nên đọc.
5
84996
2013-04-11 17:33:18
--------------------------
