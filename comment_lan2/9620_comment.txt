465828
9620
Ban đầu, tôi thực sự ngạc nhiên, bởi tác giả của quyển sách tuyệt vời này lại là một bác sĩ chứ không phải một nhà văn. Ừm, mà ngẫm lại, tại sao bác sĩ lại không thể viết sách hay, khi mà những gì ông viết ra cũng chính là những điều ông thực sự trải nghiệm trong cuộc sống, không một chút hư cấu hay phóng đại. Roger Cole, với sứ mệnh mang đến những yêu thương, không chỉ trao cho những bệnh nhân của ông những viên thuốc, những bài tập trị liệu, mà còn truyền cho họ niềm tin yêu đối với cuộc sống, với con người, trao cho họ sự sẻ chia thông cảm kịp thời và đúng lúc, để họ có thêm thật nhiều dũng khí để đương đầu với bệnh tật. Trên con đừng đấu tranh ấy, có người thành công, có người thất bại, nhưng chí ít họ cũng đã từng chiến đấu hết mình không ngơi nghỉ. Bìa sách màu đỏ với những cách hoa phớt hồng bay bay, chính là yêu thương đang lan tỏa khắp mọi nơi. Một quyển sách đẹp từ hình thức đến nội dung.
5
109583
2016-07-01 14:36:19
--------------------------
139241
9620
"Sứ mệnh yêu thương" là cuốn sách khá hay, chắc chắn sẽ không làm bạn thất vọng. Cuốn sách có bìa ngoài ấn tượng, đẹp mắt. Nội dung tuy không mới nhưng văn phong chân thực kết hợp với lối dẫn dắt hấp dẫn của tác giả đã khiến cho người đọc không thể rời mắt khỏi từng trang sách. Cuốn sách là tập hợp những câu chuyện giản dị mà cao đẹp trong đời sống hàng ngày về tình yêu thương chân thành của con người, khát vọng tìm kiếm cái đẹp trong tâm hồn, các bài tập thiền,.. giúp tâm trí thư giãn, thoải mái và giúp ta nhận ra nhiều giá trị cao đẹp trong cuộc sống. Cuốn sách có tính nhân văn sâu sắc, có tính giáo dục, giản dị nhưng vẫn đầy triết lý,... chắc chắn sẽ làm hài lòng cả những độc giả khó tính nhất.
4
414919
2014-12-06 15:08:59
--------------------------
50720
9620
"Sứ mệnh yêu thương" đã đem đến cho mình những giờ phút thật nhẹ nhàng, thảnh thơi nhưng cũng đáng nhớ và sâu sắc. Cuốn sách là những câu chuyện, những chia sẻ rất đỗi chân thành và giản dị, những bài học về sự yêu thương và cách để chúng ta đạt được sự bình yên, cân bằng trong cuộc sống. Mỗi triết lý trong sách đều được viết một cách đơn giản, có thể không quá mới mẻ nhưng đủ tinh tế và giàu sức thuyết phục. Qua đó, mỗi người sẽ thu lượm thêm nhiều bài học bổ ích, để làm cuộc sống của mình trở nên hạnh phúc hơn, tốt đẹp hơn. Khi một cuốn sách giúp chúng ta thay đổi cuộc sống, đạt được niềm vui thì đó chính là một cuốn sách tuyệt vời.
4
20073
2012-12-16 10:51:16
--------------------------
