203108
9639
Mình đặt mua sách này bên tiki cũng khá lâu rồi, hôm nay mới đọc hết toàn bộ nội dung của sách. Sau khi đọc hết sách " Khi mọi điểm tựa đều mất" mình có vài nhận xét như sau:
Về hình thức: Bìa sách có thiết kế khá đẹp, chất lượng giấy in và màu mực tốt.
Về nội dung: Nội dung sách khá đặc biệt, vì phần lớn các sách kỹ năng sống thường hướng vào việc giúp con người tìm kiếm hạnh phúc từ những yếu tố bên ngoài thì " Khi mọi điểm tựa đều mất" lại hướng vào yếu tố nội tâm. Nhờ đó, người đọc sẽ tìm thấy những phương pháp hữu hiệu để trải nghiệm hạnh phúc.
3
538881
2015-05-31 12:05:53
--------------------------
68155
9639
Tôi tìm đến quyển sách này khi tôi đang trong nỗi tuyệt vọng, không lối thoát sau cuộc tình tan vỡ. Sau đó, tôi quyết định mua nó và đọc thử xem làm thế nào để tôi thoát ra khỏi cảm giác này.  
Ban đầu, nó gợi nhớ cho tôi về những kỉ niệm xưa vì nó liên tục đề cập đến 2 từ "hạnh phúc". Có 2 loại hạnh phúc: hạnh phúc không bền vững là hạnh phúc về vật chất, theo đuổi vật chất để có đc hạnh phúc. Còn 1 cái là hạnh phúc dài lâu, tồn tại trong chính mỗi con người chúng ta  hay còn gọi là hạnh phúc tự thân, nó không bao giờ mất trừ phi ta đánh mất chính bản thân mình. Cuốn sách dạy ta phải biết yêu thương bản thân hơn thông qua việc ăn uống đầy đủ, bảo vệ sức khỏe,... Có thể tôi cũng phần nòa vơi đi nỗi buồn và đang cố gắng làm theo cuốn sách để có được hạnh phúc đích thực.
5
80996
2013-04-10 16:42:21
--------------------------
