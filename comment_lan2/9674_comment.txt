54010
9674
Hà Nội là một mảnh đất ngàn năm, một thủ đô rộng lớn với sự giao thoa của những nét truyền thống và sự hiện đại mới mẻ. Mọi người hẳn sẽ ấn tượng với vẻ đẹp của cảnh quan, con người, những nét đặc sắc trong ẩm thực, văn hóa, âm nhạc. Nhưng nói về Hà Nội thì còn phải nhắc đến thơ ca. Suốt một chiều dài lịch sử đã để lại nhiều bài thơ kiệt xuất về mảnh đất này, thể hiện tình yêu của người viết với đất Thăng Long, Kinh Kỳ. Cuốn sách nhỏ này chính là một tập hợp những bài thơ như vậy. Thêm vào đó là những bình luận ngắn gọn nhưng tinh tế và sâu sắc, sẽ giúp đọc hiểu thêm những nét đẹp của thủ đô qua những bài thơ lôi cuốn này.
4
20073
2013-01-06 17:09:13
--------------------------
