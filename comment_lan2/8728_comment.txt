471405
8728
Cuốn sách này có đề là dành cho cả người lớn và trẻ em. Không biết các bạn thấy thế nào nhưng riêng tôi nghĩ là nó phù hợp với trẻ em hơn.
Các câu chuyện đều về những con người có số phận cực bất hạnh từ nhỏ và cuối cùng vươn lên đạt được những thành công mà người đời nhớ mãi. Tuy trong sách viết rất nhiều chuyện nhưng tôi thấy chỉ có tầm 5-6 chuyện là truyền cảm hứng người đọc.
Cuốn sách này thật sự hay hơn nữa nếu nó loại bỏ bớt các yếu tố lịch sử ( ngày tháng nơi chốn địa điểm), vì cái mà người đọc cần ko phải là một câu chuyện chính xác mà là một câu chuyện có khả năng truyền lửa. Cần chú ý hơn về cách kể.
Điểm đáng khen nhất là chất lượng sách rất tuyệt vời, và tôi cũng có chút hụt hẫng về kích cỡ sách-không to như mong đợi . ^_^
3
899706
2016-07-08 15:08:02
--------------------------
361040
8728
Mình nghĩ đây sẽ là quyển sách khá bổ ích cho học sinh - nhất là những bé cấp 1 và 2. Cảm giác mỗi câu chuyện giống như 1 cổ tích giữa đời thật vậy! 

Sẽ rất tốt nếu các bậc phụ huynh biết tất cả 25 mẫu chuyện trong quyển sách này và kể lại cho con họ - theo cách của bản thân. Điều này vừa giúp phụ huynh có thêm tính kiên nhẫn trong việc dạy con, còn những đứa trẻ sẽ có thêm nhiều vị anh hùng trong tâm hồn và cố gắng phấn đấu như vị anh hùng của chúng!

Tuy nhiên, quyển sách có thể sẽ không như mong đợi đối với 1 số bạn quá mong chờ vào việc có thêm động lực và thay đổi bản thân sau khi đọc xong cuốn sách này. Bởi mỗi câu chuyện được tóm tắt khá gọn - và gần như là sơ lược về những con người phi thường xung quanh ta mà thôi.
3
191358
2015-12-30 11:30:46
--------------------------
327518
8728
Sách giống như tiểu sử cùng những mẩu tóm tắt nhỏ về cuộc đời của những con người đầy nghị lực, vượt khó khăn thử thách để thực hiện những điều phi thường. Những câu chuyện thực tế, không hào nhoáng như người hùng trong phim ảnh, cho ta cái nhìn chân thực và rõ ràng. Cách trình bày của sách (giống như tập hợp một bộ hồ sơ) nếu đọc liên tục thì mình thấy bị nhàm chán, thỉnh thoảng lại xem một chút thì thấy hay. Về hình thức thì mình thấy sách đẹp, giấy tốt, khổ nhỏ có thể dễ dàng mang theo. Mình khá hài lòng về sách.
4
39590
2015-10-27 23:23:47
--------------------------
119172
8728
Khi đọc về giới thiệu sách tôi thấy rất hào hứng và đã đặt mua luôn. Sau khi đọc xong thì cảm thấy hơi hụt hẫng(có lẽ do tôi trông đợi quá nhiều). Cuốn sách là một bản tóm tắt khá sơ lược về tiểu sử của các nhân vất nổi tiếng, mỗi nhân vật chỉ khoảng 5 - 6 trang, chưa thật sự sống động hay "đánh thức những ước mơ bị lãng quên" giống như giới thiệu sách. (ít nhất là đối với bản thân tôi)
1
387630
2014-08-02 20:55:52
--------------------------
101965
8728
Khi đọc phần phần trích dẫn sách trước khi mua về J.F. Kenedy tôi thấy rất tuyệt vời nên đặt mua ngay quyển sách. Tuy nhiên, sau khi đọc được 1/2 quyển sách tôi thấy rất rời rạc. Không phải vì quyển sách nói về 25 câu chuyện đời riêng lẻ mà bởi ngay trong từng câu chuyện, cách mô tả không gây cảm hứng ( trừ mẩu chuyện về Kenedy - có lẽ do ông là người tôi hâm mộ ), cách kể vắn tắt không làm người đọc thấy được sự khó khăn vươn lên của các nhân vật.
2
110100
2013-12-12 16:03:38
--------------------------
90534
8728
Câu truyện cuộc đời của 25 nhân vật khác nhau mà theo tác giả là có những đóng góp tích cực cho xã hội và có 1 cuộc sống ý nghĩa. Đọc quyển sách này , tôi có thể tìm ra những điểm tương đồng của 25 nhân vật này một cách khá rõ ràng. 
Đầu tiên tất cả bọn họ đều có xuất phát điểm thấp, bất kể về tinh thần, thể chất, gia cảnh hay địa vị xã hội. Có lẽ chính vì vậy mà sức sống của họ mãnh liệt hơn, "điều gì không giết được ta sẽ khiến ta mạnh mẽ hơn". 
Kế đến, tất cả bọn họ đều yêu sách, hay có thể nói là khao khát kiến thức bất kể là lĩnh vực khoa học, văn hóa hay nghệ thuật thể thao. Có 1 sự thật là không có bất kì người nào không học mà thành công, cái học ở đây ko nhất thiết là đến trường mà là tự học.
Đam mê là điều dễ dàng nhận ra nếu muốn thành công. Dù là thành công với bất cứ ngành nghề nào. Có đam mê mới có động lực, mới dẫn tới sự chăm chỉ luyện tập và mới trở thành người giỏi nhất.
Điều cuối cùng là tất cả bọn họ đều đóng góp thành công của mình cho sự phát triển của xã hội. Đó mới là thành công đúng nghĩa ! 
4
22174
2013-08-11 00:00:45
--------------------------
