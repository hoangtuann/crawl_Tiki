313417
8871
Dù là tiểu thuyết nhưng có thể thông qua những sự kiện, suy nghĩ và hành động của nhân vật, phần nào hiểu thêm về văn hóa của Tây Tạng.
Truyện phiêu lưu mờ ảo, với những địa danh xa lạ cho những ai chưa từng tìm hiểu gì về Tây Tạng, nhưng phần nào thắp lên một chút tò mò, hào hứng trong lòng người đọc về đất nước này. Người dân khá sùng đạo hay như những người theo chủ nghĩa vô thần là mê tín dị đoan.

Nhưng sống giữa nơi thiên nhiên hùng vĩ đồng thời khác khốc liệt, không khí loãng, chưa tiếp cận nhiều với khoa học kỹ thuật hiện đại, thì sự sống và cái chết chỉ trong gang tấc, tình yêu và của cải sẽ dễ dàng vụt qua tay nếu gặp phải kẻ mạnh và có dã tâm. Thì sự tin tưởng tuyệt đối vào thần linh này cũng là 1 điều dễ hiểu và là một nét đáng yêu của dân tộc này.

Đọc để thấy một tình yêu hoang dã ở vùng biên cương này!
4
117197
2015-09-23 16:20:16
--------------------------
169138
8871
Mình mua cuốn Truyền thuyết về tình yêu và phép thuật xứ Tây Tạng này là do thấy bìa được trang trí khá đẹp và cốt truyện tóm tắt cũng hay.
Nhưng ...
Văn phong dài dòng, khó hiểu làm người đọc hoang mang. Dùng quá nhiều các từ ngữ khó nhớ dù đã phiên âm làm người đọc lẫn lộn. Hình ảnh sử dụng lại trừu tượng hóa, hoang đường hóa làm người đọc thấy kém phần thực tế, dù câu chuyện có thiên phần về tâm linh.
Tốt nhất các bạn hãy xem xét lại rồi hẳn ra quyết định mua nó, vì cuốn sách giá khá mềm, chữ in màu cũng đẹp !
1
298441
2015-03-17 19:35:24
--------------------------
102470
8871
Câu chuyện kể về hành trình thay đổi nội tâm đầy sóng gió của tướng cướp Garab, cuộc đời đã thay đổi ông thành con người tốt bụng, giàu tình thương. Đan xen trong chuyến phiêu lưu ấy là ma thuật của các pháp sư Đạo Bon đại diện cho sự tham lam, ích kỷ và phép màu của tình yêu & đức tin. 

Dù nữ tác giả Alexandra David Neel rất nổi tiếng về các đầu sách tâm linh về các Nhà sư Tây Tạng, nhưng theo điểm đánh giá cá nhân của mình thì quyển "Magie d'amour et magie noire" này chỉ đạt mức độ trung bình. Có lẽ là do bà viết theo thể loại tiểu thuyết, cho dù đây là câu chuyện có thật, nhưng cũng khó tránh khỏi việc tạo cho người đọc cảm giác hoang đường.
3
67340
2013-12-19 14:09:34
--------------------------
