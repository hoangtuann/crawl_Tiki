310288
9286
Cùng với cuốn "Bởi chúng ta yêu nhau", tập thơ này tập hợp những bài thơ tình nhưng là của những tác gia nổi tiếng thời kỳ thơ mới như Huy Cận, Xuân Diệu. Có những bài thơ rất lạ, đúng như có bạn nhận xét là không phải là những tác phẩm tiêu biểu, đặc sắc, tuy nhiên đối với tôi đó chính là sự thú vị, bởi lẽ tôi có cơ hội được biết thêm nhiều bài thơ mới về tình yêu của những tác gia mà tôi yêu thích. Chọn lọc các bài thơ trong cả cuốn thật sự rất phù hợp và ăn ý với nhau, cùng mang một âm hưởng nhẹ nhàng, lãng đãng, đôi khi không có lấy một chữ "yêu" mà sao lại thấy ngọt ngào và ấm áp đến vậy. Một tập thơ mà bạn đọc yêu thơ tình nên có. 
4
25914
2015-09-19 14:09:02
--------------------------
137560
9286
Đây là sản phẩm đầu tiên mua ở Tiki làm mình cảm thấy hơi thất vọng. Thất vọng không phải ở nội dung mà là ở hình thức. Giá như quyển thơ được xuất bản với kích thước to hơn (cỡ sách phổ thông, 13 x 20.5 cm chẳng hạn), in một màu trên chất liệu giấy bình thường, thì có lẽ hồn thơ trong từng tác phẩm sẽ được diễn đạt tròn vẹn hơn rất nhiều.
Về nội dung, nếu bạn là người yêu thơ và đang yêu, thì nội dung tập thơ đủ thỏa mãn những cảm xúc rất riêng của thể loại thơ tình.
3
139827
2014-11-27 10:46:25
--------------------------
73135
9286
tôi đã rất háo hức mong chờ được cầm trên tay cuốn sách này nhưng khi tôi nhận được thì thật sự thấy rất thất vọng.. đầu tiên nói về nội dung ở đây là bài thơ của một số nhà thơ nổi tiếng như Xuân Diệu, Huy Cận nhưng cá nhân tôi thấy đó chưa phải là những bài thơ hay nhất, nổi bật nhật của những hồn thơ nổi tiếng này.
thứ 2 tuy chất liệu giấy đẹp, có in màu, hình ảnh nhưng khổ sách bé như thế với giá 45.000 là quá cao so với những cuốn sách khác. Đó chỉ là nhận xét chủ quan của cá nhân tôi về cuốn sách này
2
33410
2013-05-06 17:34:44
--------------------------
