374704
9236
Tôi đặt hàng ngay khi thấy cái tựa sách, nhận hàng mà lòng lâng lâng, tiki gói rất kĩ ,   sách phải nói khá dễ thương ngắn ngắn nhưng dày, dày nhưng nhẹ. Tác giả làm tôi rất tò mò về Tây Hạ, về biên giới Mông Cổ - Trung Quốc. Ngòi bút khá lôi cuốn nhưng đối với riêng bản thân tôi thấy một vài chi tiết không thích cho lắm. Tôi lại rát thích Cố Phi Ngư nên mấy điều đó bỏ qua một bên đi, Và tác giả rất dễ thương khi hết ngay khúc gay cấn, đọc xong chỉ muốn bán nhà để mua mấy quyển còn lại, phải nói series ''Tử thư tấy hạ'' đã làm ví tiền của tôi tổn thương nặng nề, Nhưng đáng. Bookcare plastic của tiki thì khỏi chê vào đâu được.
5
807942
2016-01-26 21:46:04
--------------------------
104497
9236
Mình đã xong quyển thứ ba của TỬ THƯ TÂY HẠ - BIÊN GIỚI KHÔNG BÓNG NGƯỜI. quyển sách này tổng cộng là 671 trang, với số trang như vầy cũng có thể nói là khá dài nhưng đối với tôi 671 trang này chẳng là cái đinh gì cả, tôi đọc truyện mà thấy nó ngắn ngủn, đọc chưa đã gì cả thì truyện đã hết rồi. Mà truyện hết ngay tại mấy chỗ độc độc không chứ, khiến đọc giả sựng đầu vì tức, phải tìm ngay quyển kế tiếp mà đọc, nếu không đọc sẽ ngủ không yên ăn không ngon.
Với lại, tôi rất khâm phục CỐ PHI NGƯ, tác giả tạo ra những bí mật rất là hay rất là cuốn hút, khiến đọc rất tò mò muốn biết bí mật đó là gì nên lao đầu vào đọc để tìm lời giải  đáp, càng đi tìm thì lại càng phát hiện ra nhiều bí ẩn khác mà nhiều bí ẩn đan xen với nhau nhưng không khiến cho đọc giả bị rối rắm, cái nào ra cái đó đàng hoàng.
Và đặc biệt là tôi rất thích cái bí ẩn cần giải đáp ở cuối quyển 3 này. Đó là “ai là nội gián trong lão K”. Đọc các tác phẩm khác thì tôi còn có khả năng suy đoán người này người nọ chứ đọc của CỐ PHI NGƯ thì tôi bó tay chịu thua. Tôi thật sự không thể nào đoán được kẻ nội gián là ai. Ai cũng không có khả năng là nội gián nhưng phải có người là gián. Tác giả quá tài, giăng lưới khắp nơi làm đọc giả kẹt cứng không thoát ra được.
5
180185
2014-01-13 23:33:05
--------------------------
62161
9236
Đây là tập thứ 3 của Tử thư Tây Hạ: Biên giới không bóng người của tác giả Cố Phi Ngư, mình đã đọc hai tập trước của bộ tác phẩm này là Mộ người sống thần bí trong tử thư và U hồn bí ẩn, phải nói là hai tập trước của bộ sách này thực sự rất hay và lôi cuốn.
Truyện xoay quanh anh chàng Đường Phong-một nhà nghiên cứu văn hoá trẻ tuổi của Trung Quốc, anh đã bị kéo vào một bí ẩn với khởi nguồn là bức kệ tranh ngọc, từ đó bị lôi cuốn theo một tổ chức bí ẩn của chính phủ. Từng bước từng bước nhóm người của Đường Phong đang tìm cách lần ra những bí ẩn giấu sau bức kệ tranh ngọc này. Truyện thực sự rất lôi cuốn, vừa bí ẩn vừa kích thích trí tò mò của ngươi đọc. Theo mình nghĩ thì đây là một tác phẩm thích hợp cho các bạn thích thể loại trinh thám lịch sử lẫn văn hoá. Một tác phẩm cực kì thành công của tác giả Cố Phi Ngư.
4
60663
2013-03-07 10:44:56
--------------------------
