335319
9052
Mình rất thích cuốn sách Lessons for IELTS Listening ngay từ khi lật ra trang đầu. Ấn tượng đầu tiên của mình về cuốn sách này là màu sắc bắt mắt, cách bố cục rõ ràng, có logic của nó theo từng unit với chủ đề rất thiết thực. Sau mỗi bài tự luyện là phần đáp án ở sau sách. Mình rất ưng ý vì ở đáp án có phần script, giúp người nghe có thể hiểu được rõ hơn nội dung và bắt chước theo cách người bản xứ đọc. Tuy nhiên phần audio hơi bị lẫn tạp âm và sách còn có lỗi, nhưng nói chung, quyển sách ấy làm mình rất hài lòng.
4
923880
2015-11-10 21:22:40
--------------------------
274104
9052
Mình thấy sách có điểm tốt và chưa tốt... sách có gợi ý nhiều tips để đạt điểm cao, và phần từ vựng chọn lựa và phân chia phù hợp cho mỗi bài và được chia theo từng chủ đề cụ thể,hình ảnh đẹp mắt và chữ viết rõ ràng , tuy nhiên không có các bài tập test tổng hợp phù hợp với kì thi, chủ yếu mỗi unit chỉ luyện kỹ năng từng test 1. Ngoài ra mọi thứ đều ổn, học xong có thể nắm vững kỹ năng để làm task 1, task 2.
4
655306
2015-08-21 17:28:06
--------------------------
268605
9052
Mình dùng sách này được nửa năm rồi và kết quả là khả năng listening của mình được nâng cao đáng kể. Điểm cộng của cuốn sách là bên cạnh việc các bài nghe được phân chia theo những chủ đề thường gặp trong ielts thì sách còn cung cấp các tips kĩ năng trong quá trình làm bài cũng như từ vựng mới. Mình cảm thấy quyển sách đặc biệt phù hợp với các bạn đã có nền căn bản về listening và đang bước đầu luyện làm quen với các dạng nghe trong ielts. Cám ơn tiki đã giới thiệu quyển sách này đến với những người học TA như bọn mình :D

4
276504
2015-08-16 17:15:40
--------------------------
