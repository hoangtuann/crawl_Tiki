132017
8437
Đây là lần đầu tiên tôi đọc tác phẩm của  Quỳnh Scarlett và thật sự bị ấn tượng . Tôi đã quá quen với những tiểu thuyết lãng mạn đi theo mô típ thông thường lọ lem yêu hoàng tử nên trong thâm tâm muốn đọc được một câu chuyện gì đó mang tính chất đột phá ngoài sức tưởng tượng .  Quỳnh Scarlett đã đáp ứng được mọi nhu cầu mà tôi đưa ra . Phải nói cách viết của cô rất độc đáo và khác lạ nó có gì đó bí ẩn và cũng khá thu hút tôi . Tôi chưa từng thấy cô gái nào như Hạ Hương quả đúng như lời tác giả giới thiệu ngìn người mới có được một người kì lạ như Hạ Hương . Khâm phục cái cách mà cô nhìn nhận tình yêu của mình và sẵn sàng đấu tranh vì nó . Nếu hạnh phúc không tự về tay mình thì mình phải đấu tranh vì nó đúng không nào ?
5
337423
2014-10-29 08:48:58
--------------------------
130334
8437
Tôi đã khá quen với những cuốn tiểu thuyết kinh điển, nhưng thật sự khi đọc cuốn sách này tôi nhìn thấy đâu đó trong cô gái Hạ Hương này là chính bản thân tôi. Lối văn nhẹ nhàng, có những vấn đề tế nhị như sex cũng được tác giả miêu tả không quá phàm, sex là vấn đề nóng nhưng đã không gây bỏng trong cuốn sách này.
Yêu cuồng nhiệt, sống hết mình và quan trọng hơn, cuốn sách đã mang hình tượng của cô gái Hạ Hương vào lòng của nhiều cô gái trẻ, độc thân như cô...
5
387048
2014-10-16 22:10:35
--------------------------
116369
8437
Đọc sách ngoài giải trí còn để cảm nhận và đưa tầm nhìn ra một tầm xa mới vượt giới hạn cũ...
Có lẽ tôi khá cổ điển nên cuốn sách hoàn toàn ko phù hợp với tôi. Chuyên yêu chuyện tình cảm được diễn tả khá trần tục, không hề sâu sắc như tôi mong đợi trong một câu chuyện tình. Với tôi tình yêu là mối liên hệ sâu sắc gắn kết nhau hơn chỉ là chú trọng miêu tả chuyện tình dục ( phần này từ đầu đến cuối dường như đc tác giả mô tả cụ thể chi tiết nhất ) 
Với tôi nhân vật cô gái này rất hấp dẫn nhưng chỉ ở ngoài hình ( tác giả mô tả cô đẹp) , ngoài ra cô không có nét đẹp bên trong của một phụ nữ châu á về tính cách và suy nghĩ. Tôi nghĩ tuy đã trưởng thành và dám yêu dám thử hết mà ko đắn đo mà lại nhiều khi cư xử thiếu kiềm chế, thiếu tế nhị, như vậy khá trẻ con. Cô gái này với tôi không hề hấp dẫn!  
Tôi đánh giá cao việc tác giả mô tả những quang cảnh trong chuyến hành trình của họ. Khá đẹp. Và cũng khiến tôi ao ước được tự trải nghiệm một chuyến đi đầy thú vị như thế. Ngoài ra văn phong, cách diễn đạt, khắc hoạ nhân vật tôi nghĩ khá nhạt nhẽo. 
Tôi hơi thất vọng khi đọc xong cuốn sách này.
2
290522
2014-07-07 19:05:23
--------------------------
110194
8437
Đây ko phải kiểu tình yêu ủy mị, bi lụy của tuổi sv mà là chuyện của những người đã trưởng thành, dám yêu và trao hết mọi thứ với ít đắn đó và đủ "già" để biết và chịu trách nhiệm với những gì đã làm.

Hạ Hương trong truyện ko phải kiểu cô gái cổ hũ, nhút nhát, vì thế, các mối tình của cô cũng vậy. Nên truyện hợp với những cô gái đã qua 25 và chừng 27, những người đã từng yêu vài lần, ko còn ngu ngơ vụng dại như cái kiểu yêu thời 19, 20 nữa và có cách suy nghĩ phóng khoán về tình yêu trong thời buổi hiện đại này. 

Đọc xong "Ngoại tình với cô đơn" (cùng tác giả) rồi đến quyển này như vậy mới gọi là đủ 1 hành trình yêu, phiêu du nhưng rồi sẽ có hồi kết đẹp... Cô gái nào ở tuổi Hạ Hương (bao gồm cả mình) cũng mong muốn sau cùng, người cần cũng đến. 

Đọc để có thêm niềm tin vào tình yêu trong đời thực.
5
324103
2014-04-13 16:02:47
--------------------------
