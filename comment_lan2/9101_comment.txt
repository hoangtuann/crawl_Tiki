461432
9101
Một phong cách hoàn toàn khác của tác giả Rowling. Nếu ai từng say đắm Harry Potter thì chắc hẳn sẽ hoàn toàn bất ngờ với bà trong tác phẩm này. Nếu trong Harry Potter chúng ta đắm chìm trong một thế giới phép thuật dù có sự tàn ác nhưng vẫn đầy sự trong trẻo tươi sáng. Còn trong " Khoảng trống" chúng ta đối diện với một xã hội trần trụi có đầy rẫy những sự thật u tối. Tuy vậy tác giả vẫn làm cuốn hút người đọc theo từng trang sách tinh tế của mình. Cuốn sách khá dày nhưng được in trên chất liệu giấy xốp nên không hề nặng.
4
405360
2016-06-27 20:12:39
--------------------------
395114
9101
- Các sáng tác của cô Rowling đều có một đặc điểm chung, không ai hoàn hảo.

Barry - nhân tố gây ra toàn bộ câu chuyện - cứ tưởng là một người đàn ông tuyệt vời, quan tâm tới đội đua thuyền,... hoá ra lại chẳng thể làm vợ mình hanh phúc. Đối thủ của ông là một kẻ quyền lực, lai đi lừa dối vợ, dan díu với vợ của đồng nghiệp đã khuất. Bà Shirley là một người phụ nữ phục tùng chồng con, ước ao được người khác thương cảm và kính trọng như Mary Fairbrother. Còn Mary có được tình yêu của tất cả mọi người, nhưng lại bị bỏ rơi bởi người chồng quá cố. Gavin có một người tình hết mực yêu thương hắn, nhưng hắn lai chọn Mary thay vì cô, và kết cục là mất tất cả..... Đến cả những đứa trẻ như thằng Stuart và Andrew, tình bạn của chúng nó thật tuyệt vời, cuối cùng lại chẳng còn gì. Cô bạn Gaia xinh đẹp hoàn hảo lại không hoà thuận với mẹ,.... Thật tuyệt vời khi đọc một tác phẩm mà tác giả khai thác được toàn bộ câu chuyện một cách khéo léo, tài tình. Quyển sách rất "xám". Mọi mặt tốt xấu của con người đều được phơi bày trần trụi. Nếu muốn tìm một tác phẩm rùng rợn khi đọc câu giới thiệu "chiếc ghế khuyết của Barry Fairbrother" (như bản thân tôi lúc đầu) thì hãy sẵn sàng cho một sự thật đây: quyển sách này lột hết những lớp vỏ ngoài của con người ra, đào bới cho hết những ngóc ngách suy nghĩ của con người, đừng trông chờ vào một vụ án mạng.

Có vài lỗi đánh máy trong ấn bản này, nhưng nếu vì vậy mà chấm 4 sao thì quá uổng. Chính ngòi bút của tác giả đã biến những câu chuyện giản đơn trong thì trấn nhỏ Pagford thành một xâu chuỗi những sự kiện lỏi cuốn người đọc đến hết. Vì lẽ đó, 5 sao là xứng đáng với tác phẩm này.
5
411530
2016-03-11 14:06:54
--------------------------
284973
9101
Có vẻ như với The Casual Vacancy - Khoảng trống thì J. K. Rowling nhận được nhiều nhận xét không tích cực lắm từ phía độc giả. Nhưng quả là một sự so sánh vô lý khi đem Harry Potter và The Casual Vacancy lên bàn cân, khi đối tượng mà hai tác phẩm nhắm đến và thể loại, chủ đề cũng khác nhau hoàn toàn. Khoảng Trống nhắm đến đối tượng độc giả trưởng thành khi đề cập đến các vấn đề xã hội như ma túy, tệ nạn, sự tranh giành quyền lực, sự suy đồi đạo đức, các mối quan hệ giữa cha mẹ, con cái, vợ chồng... Đây có thể coi là một sự vượt bậc trong lối viết của cô Rowling khi Khoảng Trống thật sự đặc sắc và đầy tính thời sự. 
4
307488
2015-08-31 14:29:55
--------------------------
238747
9101
Mở đầu là cái chết của Barry Fairbrother và thị trấn Pagford tưởng chừng êm đềm thì sau cái chết ấy, hé mở hàng loạt những điều ngang trái.
Thị trấn Pagford chẳng khác nào một xã hội được tác giả khéo léo lồng ghép những vấn nạn thực tế vào: nơi đó có bạo lực, có cha đánh con, nạn tẩy chay của học sinh, thanh niên dùng chất kích thích, nạn nghiện ngập, tình dục vị thành niên,... Cả người lớn trong thị trấn bé nhỏ này cũng thế: họ luôn tìm cách xâu xé, nghi ngờ nhau hòng giành lấy vị trí mà Barry đã để lại. Cốt truyện khiến người đọc phải suy ngẫm và bị ám ảnh lâu dài.
Mình mua cuốn sách này (và mua phiên bản bìa cứng giá đắt hơn) không phải vì hy vọng sẽ được đọc một tuyệt tác khác bên cạnh Harry Potter của J.K.Rowling (cuốn sách này hếp dẫn theo một cách khác với những gì bà đã làm trong Harry Potter: dùng tình huống và tâm lý đễ dẫn dắt và cuốn hút độc giả để rồi đưa đến một kết cục nằm ngoài dự đoán khiến ai cũng phải bất ngờ), mà là vì muốn đọc một tác phẩm do J.K.Rowling viết nhưng với một chủ đề khác, một cách thể hiện đầy gai góc khác. Nếu nói đọc một lèo từ đầu đến cuối truyện thì đó sẽ là một lời nói dối, vì không ít lần mình phải gập sách lại để suy ngẫm, vì số lượng nhân vật và tình tiết truyện đôi khi đan xen vào nhau, và gập sách cũng để suy ngẫm đôi chút, có những tình tiết dù đơn giản và tiểu tiết nhưng cũng thật thương tâm. Tóm lại cuốn sách này không làm mình thất vọng, bạn đừng kỳ vọng nó sẽ tạo nên kỷ lục như Harry Potter, mà hãy xem nó là một tác phẩm khác đứng ngoài cái bóng mà J.K.Rowling đã tạo được với kiệt tác trước đó.
Mình thích phiên bản bìa cứng này từ khi còn xem hình khi bản gốc xuất bản. NXB Trẻ thật sự đã trau chuốt cho phần bìa ngoài lẫn bìa trong: bìa trong màu đen có mạ đỏ phần tựa trông rất sang trọng, có thể chưng sách chỉ với bìa trong trên giá sách cho nổi bật. Giấy thơm, xốp, mềm, nhẹ; phần gáy được khâu, dán khéo léo, trong quá trình đọc sách không lo bị bong tróc, phần trang lót bằng giấy sần dày màu vàng trông rất bắt mắt,...

5
38610
2015-07-23 13:41:03
--------------------------
189191
9101
Bóc trần từng lớp của một góc khuất xã hội đương đại. Vẻ hào nhoáng bên ngoài và những góc tối lẫn khuất bên trong được phô bày với những đối lập thấy rõ qua ngồi bút sắc bén, sâu cay của Rowling. 1 ghế trống, như một chất xúc tác thiết yếu làm lộ dần những mầm rễ của những hạt giống tội lỗi trỗi dậy. CHÚNG đã ở đó từ rất lâu, lâu lắm rồi, nhen nhúm và lấn khuất, chỉ chờ đến đây để phơi bày dưới ánh sáng mặt trời. Tất tần tật thu nhỏ xã hội thành một thị trấn nhỏ bé, nhưng chứa đựng trong nó là những xấu xa: là những tranh giành quyền lực, là những bóc lột và thù hằn, là những lợi ích bị bỏ quên và tước đoạt, là những tuổi vị thành niên bồng bột, nổi loạn, thiếu suy nghĩ; là những cảm xúc và tình cảm lẫn lộn, là những little-dirty-secret lần lượt phơi bày với bóng ma bàn phím. NHƯNG trong bức tranh với gam màu buồn là tình người, là những sắc màu sôi nổi, là những thấu hiểu, là
Umrella - Rihanna trong đám tang... :)
4
166935
2015-04-26 19:23:40
--------------------------
168128
9101
Rowling đã dành hàng chục năm trời cho Harry Potter, và giờ là lúc bà thoát khỏi thành công cũ để tìm cho mình một hướng đi mới.
"Khoảng Trống" được giới thiệu là cuốn tiểu thuyết dành cho người lớn, tuy vậy, nó vẫn nhận được đón nhận rất lớn từ phía các thế hệ độc giả. Sự chân thực, trần trũi và gai góc của một xã hội u ám được Rowling khắc họa rất rõ nét trong "Khoảng Trống". Chúng ta vẫn có thể nhận ra được giọng văn tinh tế của Rowling, nhưng "Khoảng Trống" chắc chắn sẽ là một bất ngờ lớn đối với fan ruột của bà hay cậu bé phù thủy Harry Potter.
Bản in này của NXB trẻ sử dụng bìa gốc của tiểu thuyết. Giấy in nhẹ, cuốn truyện cầm chắc tay và không gây mỏi khi đọc. Tuy nhiên, sau một thời gian không dài thì cuốn truyện của mình có hiện tượng chảy keo dính ở gáy, khiến cho các trang sách bị xô lệch, nhìn từ mặt ngoài vào sẽ không phẳng như những cuốn khác mà lổn nhổn trang thấp trang cao, trông cực kỳ phản cảm. Vậy nên mình sẽ chỉ cho cuốn sách này 4 sao.
4
48662
2015-03-15 23:51:39
--------------------------
142018
9101
Xã hội nào cũng có sự phức tạp của riêng nó, dù vẻ ngoài có vắng lặng và yên bình đến đâu đi chăng nữa.
Những câu chuyện trong cuốn sách này rất gần gũi, gần gũi đến mức khắt nghiệt và trần trụi, phô diễn được những mặt xấu trong tính cách con người, những mặt tốt đẹp có khi lại gượng gạo và trở nên quái gở.
Rất thực tế, trong cái khắc nghiệt của cuộc sống, không những ta ghét thói xấu, mà thật sự là ta còn rất buồn, rất cảm với họ.
Tuy nhiên, hệ thống nhân vật khá rối, tự người đọc sẽ phải đọc kỹ và liên kết quan hệ của họ lại với nhau, nếu không thì đọc mãi vẫn không hiểu.
4
66322
2014-12-17 13:52:24
--------------------------
90153
9101
...Cái chết của một nhân vật quan trọng đã để lại một khoảng trống. Từ khoảng trống đó, các khoảng tối, sự dối trá trần trụi, sự ích kỉ được che giấu lần lượt phô bày. Và càng xót xa hơn khi tất cả sự xấu xa của người lớn của xã hội lại được bóc trần và bị phán xét bởi những đứa trẻ mới lớn ở  Pagford .
Là tác phẩm dành cho người lớn nhưng J.K. Rowling vẫn thể hiện sự hiểu biết một cách tinh tế về thế giới trẻ em của mình với cô bé  Krystal ngỗ ngược nhưng trái tim ngập tàn tình yêu thương. Cái chết của cô bé và đứa em tội nghiệp của mình cũng đã để lại một khoảng trống nhưng đó là khoảng trống thức tỉnh. Khoảng trống đó đã thức tỉnh bà mẹ nghiện ngập tội nghiệp của em và thức tỉnh tất cả những con người đang che đậy, đang toan tính....
Giá của cuốn sách có thể làm ai đó e ngại nhưng nó xứng đáng được trả như vậy!
5
141614
2013-08-07 15:05:03
--------------------------
76451
9101
Bản đặc biệt của cuốn sách "Khoảng Trống" này được NXB Trẻ in khá đẹp và chỉn chu, giấy nhẹ cầm không mỏi tay, bìa cứng và áo jacket dày dặn, được in phủ nhũ dòng chứ The Casual Vacancy bắt mắt. Bookmark có 2 loại giấy và vải đỏ. Cuốn sách được in đẹp, đóng xén cẩn thận, xứng đáng với gía bìa 280.000đ.
Về nội dung cuốn sách tuy có nhiều độc giả phàn nàn có lẽ do họ quá ăn sâu trong tư tưởng về J.K Rowling gắn chặt với cậu nhóc phù thủy Harry Potter, nhưng thực sự cuốn sách này chính là sư lột xác trong văn học của nhà văn nữ nổi tiếng nhất nước Anh và cả thế giới. Nó chứng minh tài năng văn chương của bà là không thể phủ nhận. Bà đã viết rất chân thực và trần trụi về một xã hội thu nhỏ nơi đó có những ẩn ức tình dục của lứa tuổi thiếu niên và trung niên, bạo lực gia đình, sự đối đầu muôn thuở mẹ chồng – nàng dâu, nỗi tuyệt vọng của phụ nữ khi thiếu tình yêu đích thực, nỗi chán chường của đàn ông vì không thể sống bản lĩnh, thói đạo đức giả như một căn bệnh mà mọi loại người trong xã hội đều mắc phải. Có rất nhiều nhân vật cùng những tình tiết phức tạp trong cuốn sách nhưng tất cả được JK Rowling xử lý lần lượt rất khôn khéo, trơn tru gọn gàng mà chẳng bỏ xót một mắt xích nào.
Một cuốn sách hấp dẫn về nội dung và đẹp về hình thức thì không có lý do gì để bạn bỏ qua nó trên giá sách gia đình của mình.
4
29716
2013-05-22 23:00:25
--------------------------
66803
9101
Mảng đời sống hiện thực đã được nhiều tác gia lấy làm chủ đề cho tác phẩm của họ. Nhưng đối với nhà văn J.K. Rowling. Cũng cùng chủ đề ấy. Bà cho ra một tác phẩm hơi hướng lạ thường.
Lạ mà quen. Ắt hẳn khi đọc vài trang đầu, bạn sẽ bối rối vì những chi tiết mà tác giả đề cập đến. Nhưng cái rối ban đầu lại giải thích cho những tình huống phía sau.
Ông Fairbrother chết, cái chết đột ngột gây tiếng nổ tại thị trấn nhỏ. Xung quanh cái chết, thay vì thương tiếc cho gia quyến. Những người hàng xóm trong thị trấn lại rỉ rả ,tranh dành để thông báo cái chết ấy. Họ xem đó như một trò chơi thắng thua. Vì cái vị trí "vàng" mà Fairbrother để lại sau khi chết đã hấp dẫn họ. Những tính toán, tham vọng, dục vọng. Trong quyển tiểu thuyết này, tất cả những điều đen tối của xã hội Anh sẽ được lột tả hết. Một khía cạnh của người đứng ngoài khi nhìn vào sẽ dễ dàng cảm nhận được hơn. 
Đây hẳn là cuốn tiểu thuyết táo bạo từ ngôn từ đến hình ảnh. 
4
8802
2013-04-02 13:33:08
--------------------------
65818
9101
Sau nhiều năm, chính xác hơn là 6 năm, một lần nữa nhà văn từng là say mê bao thế hệ người trẻ Việt nay trở lại với Khoảng trống. J.K.Rowling ,vẫn với cách viết hấp dẫn mà mê hoặc , những trang bút và câu chữ dễ hiểu, nhưng có phần thấm thía và chua cay cho một hiện thực ngàn đời mà bất cứ một xã hội nào cũng đối mặt. Đó là dối trá, ích kỷ và những sự thật đôi khi không bọc mật. Trong cuốn tiễu thuyết dài hơi này ,là một cái nhìn khác của nữ văn sĩ, một cái nhìn vẫn u tối bắt nhịp từ series Harry Potter qua, nhưng là trần trụi và khó tiêu hóa hơn so với tác phẩm khác.
Những người như lũ trẻ chúng tôi ,trải qua tháng năm cùng Harry Potter ,đã 6 năm trôi qua từ ngày HP7 ra đời tại Việt Nam, có lẽ đó là khoảng trống vừa đủ ,một độ lùi cần thiết để trưởng thành hơn, đối điện với những sự thật mà Khoảng Trống ,đã ,đang và sẽ mãi bóc mẽ để phô ra cái nhìn của một người lớn qua con mắt quan sát của Rowling ,những sự thật hiển nhiên, những con người hiển nhiên ,trong một thế giới tưởng tượng ,nhưng lại thật hơn bất kỳ một thế giới nào. Ở đó có  dục vọng, tham tàn và ích kỷ, có máu và những món cơ hội mang đến từ tình dục .Một thế giới đầy cảm dỗ , nhưng, cũng có những góc sáng, những đứa trẻ là góc sáng, hình tượng của người anh hùng đã khuất là góc sáng. Đến phút cuối, góc sáng ấy có bừng thành ngàn vạn ánh nắng xóa tan sương mù và bóng tối của Pagford không ,đó là câu chuyện mà tôi đang mong hồi kết .
Trên tất cả, đây là một tiểu thuyết đáng để đọc, văn phong vẫn gần gũi và lôi cuốn như thời Harry Potter, có khác chăng, chỉ người nào dám can đảm thì sẽ vượt qua.
3
90012
2013-03-27 20:34:43
--------------------------
