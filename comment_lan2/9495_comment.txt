241015
9495
Trên bục giảng, đâu phải chỉ để giảng cho người ta nghe. 

Để đứng được trên bục giảng, người làm thầy phải đứng được trên chính đôi chân mình và đưa tay với thêm nhiều mảnh đời khác nữa.

Bậc làm thầy, thì trước hết phải làm tôi. Thấy được cái giá trị nơi người học cần khám phá hơn là cái lợi nhuận thu được từ việc thuyết giáo của mình.

Tôi từng có những người học trò tự kỷ, bị bạn bè xem như một đứa không bình thường. Chỉ mình tôi mới là người hùng của em ấy, vì tác phẩm handmade của em chỉ có mình tôi chiêm ngưỡng và trầm trồ khen ngợi.

Em là một chàng trai thông minh trong dáng dấp ngô ngố của trẻ tự kỷ, nhưng tôi nhận thấy tác phẩm của em chẳng tự kỷ chút nào.

Một người thầy, tôi đứng nhìn bạn bè trêu ghẹo em hay về phía em. Tôi chọn cách len vào thế giới của em, và tôi bị xem là giáo sinh thực tập đồ bỏ vì không bình thường như em. 

Rốt cuộc, trên bục giảng tôi đã nhìn thấy gì...
5
202585
2015-07-25 08:55:54
--------------------------
220833
9495
Chỉ cần đọc tựa đề thôi, ta có thể nghĩ đến ngay nghề được nhắc đến trong câu chuyện mà tác giả muốn gửi gắm đến người đọc. Chỉ có những người mang trong mình đầy nhiệt huyết lắm thì mới đến nghề nhà giáo, tôi đã dành tặng nó cho cô giáo của mình, một người luôn tràn đầy nhiệt huyết với các thế hệ học trò. Một món quà ý nghĩa dành tặng ngày nhà giáo Việt Nam.
5
458844
2015-07-03 00:35:27
--------------------------
217188
9495
Vừa tốt nghiệp và đang chơi vơi giữa việc thất nghiệp, đã bảo rồi, nghề giáo nghèo lắm lại còn khó xin việc thế mà còn cứ đâm đầu vào. Vừa hay, cuốn sách ra đời, mò mẫm vào Tiki đặt hàng cũng chỉ vì cái tiêu đề ấn tượng "Trên Bục giảng" vì cái bìa cũng đẹp, đẹp một cách nhẹ nhàng đến cuốn hút.
Lướt từng trang sách đọc với cái vẻ....chả chút gì ấn tượng, thậm chí cáu gắt bực tức khó chịu với Brad Cohen khi ông ta quá chủ quan đến hà khắc và tự kỷ với cái chứng bệnh của mình. Thế nhưng khi thấy ông ta gục đầu trong xe khóc khi không thể xin việc, một ông giáo khóc...khiến tôi cảm thấy mình rùng mình, nghĩ tới bản thân mình mà.....khóc theo. 
Gấp cuốn sách lại cung với sự cảm thông và định kiến, tôi chỉ cảm thấy mình cũng có thể làm được như ông ấy khi mình có điều kiện tốt hơn ông ấy, mình vẫn đang có học sinh dù chẳng phải là một giáo viên chính thức của bất kỳ trường học nào. Cho đến khi khoác balo đến thành phố xin việc, bế tắc khi bị chê "quá trẻ" và bị loại từ vòng gửi xe tất cả các trường học, không muốn trở về nhà, và bám trụ lại thành phố bằng nghề gia sư....tất cả giống như lặp lại những gì đã đọc được từ Brad Cohen. Tôi lật dở lại từng trang và đọc ngấu nghiến, đọc đi đọc lại những chi tiết ông ta khóc, ông ta cười và lấy động lực đi tiếp con người trồng người này, thắc mắc tới cả tỉ lần với bản thân lại sao cứ đâm đầu vào nghề này.
...Cuốn sách đáng để đọc, đáng để suy ngẫm, đáng được nhận nhiều sự yêu mến của tất cả mọi người, nhất là những người đã đang và sẽ là "thầy", những người khao khát, yêu thích được làm "thầy" mà không được làm vì bất cứ lý do nào, với tất cả học sinh sinh viên để hiểu về "người thầy" để biết về sức mạnh của sự nỗ lực và bản chất của thành công.
Thông điệp của cuốn sách mỗi người tìm được khác nhau, và có một thông điệp mà tôi tìm được từ đó rằng "tình yêu là một điều vô cùng kỳ diệu"
5
204039
2015-06-28 22:40:30
--------------------------
210556
9495
Nghề cao quý nhất trong các nghề cao quý đó chính là nghề dạy học. Để trở thành một nhà giáo thực thụ không phải là một điều đơn giản mà ngược lại đó là một chặng đường vô vàn khó khăn, trắc trở. Đối với người bình thường là vậy nhưng với tác giả - là một người sẵn mang trong mình một căn bệnh thì lại còn khó khăn đến nhường nào? Nhưng anh đã không ngại khó khăn đó mà quyết tâm trở thành một nhà giáo giỏi được mọi người quý trọng. Cuốn sách đã kể lại một cách chân thực và sâu sắc cuộc đời của một người giáo viên từ khi còn ngồi trên ghế nhà trường cho đến khi đã đến được vị trí tôn quý nhất. Đây là một cuốn sách rất hay để học tập về tính vượt khó trong cuộc sống, một bài học rất về nghề giáo mà các nhà giáo nên đọc. Và hy vọng mọi chúng ta sẽ luôn biết quý trọng những người làm nghề cao quý đó.
5
527557
2015-06-19 16:54:07
--------------------------
198904
9495
Từ khi có quyển sách này, hằng năm tôi đều mua ba quyển để tặng thầy cô giáo. Từ khi đọc được quyển sách, tôi lại càng kính trọng nghề nhà giáo chân chính. Chân chính ở đây là những nhà giáo làm đúng với lương tâm nghề nghiệp của mình, đặt quyền lợi của học sinh lên trên hết và sẵn sàng đấu tranh vì điều đó. Brad Cohen đại diện cho hình mẫu đó. Từ tuổi thơ của mình, ông đã không ngừng chiến thắng nghịch cảnh và trở thành một thầy giáo chân chính, yêu nghề.  Ông đã truyền cảm hứng cho rất nhiều nhà giáo trên thế giới thông qua nội dung quyển sách đầy ý nghĩa này. Nhà giáo chân chính mà tôi gặp và học từ trước tới bây giờ chỉ có thầy Vũ Khắc Ngọc.
5
387632
2015-05-20 23:43:41
--------------------------
128122
9495
Mua quyển sách này rất lâu rồi nhưng mãi gần đây mới đọc hết. Thật sự nó không cuốn hút vì chỉ là như một quyển nhật kí nói về cuộc đời của một người thầy bị mắc chứng bệnh Tourette. Nhưng qua nó, người đọc mới cảm nhận được nhiệt huyết được niềm đam mê của chính tác giả. Lí do tôi mua nó chỉ đơn giản vì tôi trong tương lai sẽ trở thành một cô giáo và tôi nghĩ nó sẽ cần thiết với mình. Đọc nó, tôi mới thấy quyết định của mình đúng đắn. Lí do mà tôi học để làm 1 người thầy chính là vì ước mơ, vì bọn trẻ như chính tác giả.
4
268696
2014-09-29 13:22:12
--------------------------
50025
9495
    Ngay từ khi đọc tựa sách với cái tên nhẹ nhàng "Trên bục giảng" ngay lập tức đã cho mình những ấn tượng, những vấn vương về người thầy. Có nghề nào là gắn với bục giảng, với bụi phấn nhiều như thế đâu? Mỗi trang sách là một lời yêu thương, là lời giảng về cuộc sống rằng trên bục giảng ấy đã có biết bao nhiêu những con người tâm huyết, biết bao nhiêu con người sống trọn đời với nghề giáo tưởng như khô khan, nhạt nhòa. Những hy sinh thầm lặng hiện lên đầy xuc động, gửi gắm biết bao nỗi niềm cảm xúc chân thành, dường như một người học trò đang kể chuyện về thầy cô.Một món quà tuyệt vời và chân thành thay lời tri ân nhân ngày nhà giáo bạn ạ.
5
77624
2012-12-11 18:20:20
--------------------------
49528
9495
"Trên bục giảng" thực sự hay và cảm động, nó kể một câu chuyện thật chân thực, để lại nhiều dư âm tốt đẹp trong lòng người đọc. Không chỉ là một câu chuyện về người giáo viên, cuốn sách còn là hành trình vươn lên, chiến thắng những nỗi đau và bệnh tật để đạt được ước mơ của mình. Ai có thể ngờ rằng Brad, một cậu bé bị chứng rối loạn thần kinh, lớn lên lại có thể trở thành một nhà giáo, một người hết lòng yêu thương học sinh của mình. Tất cả đều nhờ vào sự thấu hiểu, sự quyết tâm và lòng nhiệt huyết. Thực sự cuốn sách này không chỉ dành đến cho các giáo viên, mà còn cho tất cả chúng ta, để học thêm những bài học bổ ích và ý nghĩa.
4
20073
2012-12-08 10:10:07
--------------------------
47589
9495
Một món quà thật ý nghĩa dành tặng cho thầy cô.Những người cha, người mẹ thứ hai của chúng em. Cuốn sách như phác họa thật rõ nét và chân thực phần nào những khó khăn vất vả của nghề trồng người. Qua cuốn sách chúng em càng thấm thía hơn tấm lòng của thầy cô,biết quý trọng từng lời dạy mà thầy cô ân cần bảo ban. Chúng em nhiều khi thật vô tâm.Thầy cô nhỉ. Nghề giáo,có những lúc vui,có những lúc buồn và thậm chí nghiệt ngã,bế tắc. Cuốn sách như truyền tiếp thêm ngọn lửa yêu nghề để thầy,để cô vững vàng lướt trên những khó khăn của nghề. Những tưởng nghề giáo là nhẹ nhàng,êm ả nhưng không,đằng sau đó có cả niềm vui và không ít nước mắt.Nhưng em luôn tin,với một nghị lực đủ mạnh thầy cô sẽ lướt trên những khó khăn đó để rồi nuôi hi vọng dạy dỗ hàng thế hệ học sinh nên người và góp phần nào đó,dù nhỏ nhoi nhưng rất đáng trân trọng cho nền giáo dục nước nhà. Chúc quý thầy cô luôn mạnh khỏe và tràn đầy nhiệt huyết với nghề.
5
40822
2012-11-24 18:42:08
--------------------------
46866
9495
Tôi chắc chắn bạn sẽ khóc khi đọc cuốn sách này. Ai sinh ra cũng đều muốn được cắp sách tới trường, nhưng anh chàng trong cuốn truyện này lại không thể, vì chứng bệnh lạ, luôn lên cơn co giật và phát ra những âm thanh kỳ lạ. Thời kỳ ấy, khi chứng bệnh lạ còn chưa được biết đến, người ta sợ và xa lánh anh như một bệnh dịch, đau đớn hơn nữa, anh còn không được đến lớp. 
Thế nhưng, ước mơ được đi học, với nghị lực phi thường của mình, anh đã chiến đấu hết mình với bệnh tật để đạt được ước mơ đứng trên bục giảng, trở thành một giáo viên. Ước mơ đó cũng như ước mơ dắt dìu và bảo vệ bất kỳ học sinh nào đã từng như anh. 
Giá mà, ai cũng có tâm huyết và yêu nghề bằng cả trái tim của mình. 
4
54418
2012-11-19 07:52:21
--------------------------
45889
9495
Một cuốn sách hay và có ý nghĩa để dành tặng những người cha, người mẹ thứ hai trong cuộc đời của chúng ta nhân ngày Nhà Giáo Việt Nam 20/11. Cuốn sách là một hành trình sống đầy nghị lực của một chàng trai bị mắc hội chứng bẩm sinh Tourette. Nhưng trên tất cả những sự kì thị, xa lánh, anh đã đứng dậy để sống, để chiến đấu, trở thành một người giáo viên. Nhưng khác với mọi người, hành trình bước đi trên bục giảng của anh cũng lắm chông gai thử thách. Cuốn sách vừa là một bài học về nghị lực sống, vừa là một lời cổ vũ động viên cho những ai không may gặp nhiều khó khăn trong cuộc đời.
5
11387
2012-11-11 10:05:50
--------------------------
