438671
9303
"Hãy nói yêu em" cũng không quá đặc sắc mà lại nhẹ nhàng ve vuốt tâm hồn người đọc.Cảm xúc của nhân vật cũng quá rõ ràng và dễ đoán.Câu chuyện có bố cục logic nhưng dường như thế vẫn chưa là đủ.Những tình tiết nên lôi cuốn và gay cấn hơn nữa. Những phần đầu được viết khá tốt nhưng đến những phần sau thì đuối dần.Cái kết HE luôn làm vừa lòng người đọc nhưng chưa thực sự để lại ấn tượng sâu sắc.
Những điểm cộng của truyện là chất lượng giấy đẹp, tốt; bìa sách mịn nhưng chưa đẹp
3
584588
2016-05-30 09:45:37
--------------------------
375700
9303
 Derek Malory & Kelsey Langton là 2 nhân vật chính được ưu ái để nhìn ra thế giới ngẫu nhiên và đầy hạnh phúc , ngọt ngào và sóng gió nhưng chàng đứng đầu một gia tộc có thể giúp lấy lại được những sự tự do trong hoàng gia , dù chàng sinh ra trong gia đình có nhiều sự nghiêm khắc , bộc lộ được cả cái quy luật lớn nhất , khi chàng Derek có suy nghĩ là phải một mình thực hiện được những cái thông minh , quyết định nắm chặt lấy cơ hội quý giá .
3
402468
2016-01-29 00:00:09
--------------------------
329358
9303
Đây là cuốn đầu tiên mình đọc trong series truyện về nhà Malory. Ban đầu vì sợ đọc giữa chừng sẽ không theo kịp mạch truyện nên mình cũng hơi ngần ngại nhưng không ngờ mình vẫn có thể tiếp thu tốt nội dung cuốn sách. Mỗi câu chuyện trong series là một hành trình mới, một cuộc tình mới đầy phiêu lưu và cuốn hút độc giả. Có những chi tiết mình phải đọc các tập trước để giải đáp  nhưng mình lại thấy như vậy lại càng tăng thêm hứng thú cùng hồi hộp. Một câu chuyện tình yêu nhẹ nhàng, đầy dư vị lãng mạn mang mình đến những cảm xúc bất tận về đôi tình nhân Kelsey Và Derek bên lề những định kiến xã hội. Tuy nhiên nếu những bạn nào có ý định theo đuổi series Malory nhưng muốn nếm trải những pha hành động thì nên nếm thử tập 8. Rất cảm ơn cô Johanna vì đã tạo ra một tác phẩm như vậy. 
5
392456
2015-10-31 16:55:53
--------------------------
310188
9303
Đọc truyện này là phải đọc cả series Malory nếu không muốn rơi vào trường hợp mù thông tin. Tình cảm trong truyện khá nhẹ nhàng, lãng mạn nhưng hơi thiếu một chút gì đó cuồng nhiệt. Tình tiết truyện khá hay nhưng tổng thể lại chưa có cao trào khiến người đọc thích thú. Nếu thích một câu truyện tình yêu đơn thuần thì bạn có thể đọc cuốn sách này. Nếu cần một cuốn sách có chút gì đó nồng nhiệt thì mình nghĩ nó không phù hợp với bạn cho lắm. Bìa sách khá hợp tuy rằng màu in hơi mờ, không được rõ ràng lắm
4
5314
2015-09-19 13:05:55
--------------------------
309927
9303
Mình rất thích đọc những tiểu thuyết mang hơi hướng cổ điển của các nước phương Tây, luôn gợi lên cho mình sự hoài niệm và sự lãng mạn như cổ tích vậy, đó cũng chính là lý do đầu tiên khi thấy bìa truyện mình đã không do dự mà kết nạp cuốn sách này vào tủ sách của mình. Tuy nhiên, mình thấy nội dung khá bình thường, đơn giản, thiếu tính hiện thực đặc trưng của dòng văn học này và cái kết quá dễ đoán ngay khi đọc những chương đầu, chỉ có một điểm mình thích là tình cảm gia đình ấm áp trong truyện. Thật sự thì mình thấy đây chỉ là câu chuyện hiện đại đặt trong bối cảnh cổ đại mà thôi.
4
39798
2015-09-19 10:59:25
--------------------------
248898
9303
Mình rất thích văn học lãng mạn nên đã không ngần ngại mua quyển này, tác giả có phong cách viết khá ổn định. Thật sự sẽ khó hiểu và cảm thấy mối quan hệ giữa các nhân vật rối mù nếu chưa đọc Ma lực tình yêu và Gã cướp biển quý tộc (chính xác thì mình chưa đọc nên cứ thấy loạn loạn @.@). Tg nhắc đến các cặp đôi trước hơi nhiều, cặp đôi chính bị chìm giữa những câu chuyện của tác phẩm trước.
Vẫn phải nói về mặt tình cảm thì 0 chê vào đâu được, nhẹ nhàng, hài hước, pha chút hành động dù chưa thật gay cấn. Nếu bạn đã mua 1 quyển thì nên chuẩn bị tâm lý thu gom cả những quyển kia, nhớ đọc theo thứ tự ^ . ^
3
358411
2015-07-31 10:53:07
--------------------------
235807
9303
Do đã đọc nhiều tác phẩm khác của Lindsey nên mình thấy mô tuýp trong truyện khá quen thuộc. Điểm khác biệt là nhân vật nữ chính trong Hãy nói yêu em - Kelsey - từ một tiểu thư quý tộc phải bán mình vào nhà chứa. Xém chút nữa nàng đã bị rơi vào tay người em họ bệnh hoạn với sở thích kì dị của Derek. Đoạn Derek giải cứu cho Kelsey và trừng phạt người em họ đó làm mình bậc cười. Bìa sách rất đẹp, đậm chất cổ xưa. Nếu bạn không quá khó tính và đòi hỏi điều mới lạ thì đây vẫn là một quyển sách lãng mạn đáng mua.
5
35746
2015-07-21 12:57:10
--------------------------
228230
9303
Ngay khi đọc nội dung chính của cuốn sách tôi đã hình dung về một Derek có phần lạnh lùng và cục cằn nhưng hoàn toàn trái ngược với suy nghĩ đó, Derek hiện lên trong tôi với vẻ ân cần và trái tim nồng ấm.   Kelsey thì vừa có sự mềm yếu mỏng manh của một tiểu thư nhưng cũng vô cùng mạnh mẽ và cảm xúc sẵn sàng bùng nổ khi cần thiết. Tôi thích cách nhân vật có thể thể hiện được cùng lúc nhiều phần trong tính cách của mình dưới ngòi bút của Johanna Lindsey. Một điều đặc biệt của cuốn sách mà khiến tôi cũng cảm thấy thích thú chính là tình cảm gia đình của nhà Malory : Những con người vô cùng cá tính cùng tụ họp lại, họ nói chuyện, cười đùa , khiêu khích nhau, nhưng trên hết tôi yêu cách họ bảo vệ cho nhau. Cuốn sách nhẹ nhàng nhưng không thiếu phần kịch tính, tóm lại là một cuốn sách đáng đọc.
4
230601
2015-07-15 10:19:53
--------------------------
214649
9303
Mình để ý thấy ngôn  từ của các cuốn tiểu thuyết lãng mạn cổ điển hơi hướng Anh Quốc này luôn có 1 điểm chung trong ngôn ngữ: khó hiểu. Khá nhiều chỗ mình đọc 3-4 lần vẫn không hiểu ẩn ý đằng sau, phải lướt qua dù trong lòng hơi khó chịu. 1 trong những điểm khó hiểu là Kelsey có đủ lòng tự tôn của con gái Bá Tước để không bán mình cho 1 nhà chứa. Và nếu định mệnh xui khiến , và người mua cô là ai khác rất kinh khủng, thường xuyên hành hạ nhân tình khiến họ chết đi sống lại, thì cô sẽ như thế nào? Tình huống truyện khá...ảo diệu. Không biết là do văn phong của tác giả hay do người dịch khiến tác phẩm rơi vào trạng thái buồn ngủ, vì đọc 1 hồi bạn sẽ tự hỏi ''Ủa vậy là sao? Chuyện gì vậy? '' bạn khó có thể xác định ai đang đối thoại với ai, ai nói câu nào, và nhân vật trong truyện đang ở đâu. Bạn sẽ khá rối loạn. Mình mất cả hơn 2 tuần mới đọc xong cuốn này, trong khi các cuốn khác thì chỉ nửa ngày.
Nội dung cũng không thật sự hấp dẫn, không có điểm cao trào của tác phẩm, quá nhẹ nhàng đâm ra nhàm chán.
2
138737
2015-06-25 11:53:33
--------------------------
210191
9303
Đây là cuốn tiểu thuyết thứ 2 của tác giả Johanna Lindsey mà tôi đặt mua tại tiki.vn. So với cuốn "Nàng công chúa lưu lạc", nội dung của tiểu thuyết này chưa thật đặc sắc lắm. Kelsey Langton xuất hiện ở đầu tiểu thuyết mang lại một bất ngờ nhỏ đối với người đọc, khơi gợi sự tò mò cho người đọc.  Derek Malory xuất hiện với hình tượng giống như những mô típ cũ: đẹp trai, giàu có, phong độ và hào hiệp.... để cứu mỹ nhân. Chuyện tình nảy sinh giữa hai con người một cách nhẹ nhàng nhưng thiếu tính cao trào cho đến cuối tiểu thuyết, khi thân phân thật sự của Kelsey Langton được hé lộ. Văn phong lãng mạn, nhẹ nhàng tác giả đã khiến người đọc thấy được một mảng xã hội của tầng lớp quý tộc và một cái kết có hậu cho những nhân vật trong truyện!
3
377506
2015-06-19 08:16:56
--------------------------
132176
9303
Cuốn sách của tác giả : Johanna Lindsey nói về một chuyện tình lãng mạng và đầy bất ngờ giữa Derek Malory và Kelsey Langton, nàng vì hi sinh bản thân cứu lấy gia đình mà phải lao vào con đường bán thân và gặp được chàng, cuộcc gặp gỡ định mệnh ấy đã đưa cả hai vào một bước ngoặc tình cảm đầy lãng mạng.

Về phần nội dung của câu chuyện rất hấp dẫn và giúp ta liên tưởng tới một cuộc tình đẹp với kết thúc viên mãn. Nhưng về phần lời viết của tác giả chưa thật sự cuốn hút và có vẻ khô khốc. Hai nhân vật gây ấn tượng với nhau bằng thể xác hơn là bằng tính cách. Chuyện tình yêu cũng nhẹ nhàng, nhưng như vậy làm chúng ta cảm giác không thật và quá dễ dàng. Câu chuyện cũng không có sóng gió hay biến động gì lớn gây cao trào cuốn hút cho người đọc.

Nói chung cuốn sách chưa phải là tác phẩm xuất sắc nhất của tác giả Johanna Lindsey. Nhưng nếu bạn là một người thích sự lãng mạng nhẹ nhàng và một câu chuyện tình yêu êm đềm thì có lẽ cuốn sách sẽ là sự lựa chọn tốt cho bạn.
3
357244
2014-10-30 09:05:07
--------------------------
120685
9303
Mở đầu câu chuyện, Johanna Lindsey đã tạo cho người đọc tò mò về số phận của Kelsey Laton. Là con gái của Bá tước Lanscastle đệ tứ, phải bán mình trong một nhà chứa ô danh nhất London? Ngay từ hình ảnh đầu tiên, ở nàng là một sự quả quyết dấn thân chấp nhận của sống là tình nhân của kẻ khác, nhưng cũng chút gì đó e lệ, sợ hãi, chút đoan trang của một dòng dõi quý tộc đã sụp đổ từ lâu. Có gì đó trong cô khiến ta vừa thích thú vừa tò mò. Derek Malory xuất hiện như một vị thần hộ mệnh bảo vệ cô khỏi một gã xinh trai nhưng ánh mắt đầy nhẫn tâm và độc ác.
Lòng thương người của anh đã biến cô thành nhân tình của mình. Một nhân tình đầy thông minh, mê hoặc, với nét duyên dáng thanh tao trong cách ứng xử, giao tiếp thường ngày. Derek Malory ngày càng bị nàng mê hoặc hơn và bất chấp những rào cản xã hội, anh đến với cô bằng tấm chân tình xuất phát từ trái tim. Để rồi vừa vui mừng vừa tự trách bản thân khi biết được thân phận thật sự của nàng.
Tuy nhiên, chính vì sự nhẹ nhàng của cốt truyện khiến người đọc hơi chút hụt hẫng. Cả hai như trong sách cuốn hút nhau bằng thể xác hơn là tâm hồn, đọc lời tựa ta cảm thấy khía cạnh gai góc và éo le của sự việc, ta mong chờ vào một tình yêu vượt qua những rào cản của xã hội để đến với nhau. Một cuốn sách thích hợp đọc vài buổi chiều hè, hơn là một tác phẩm đáng để chiêm ngẫm.
*Một số nhận xét khác:
- Bìa cuốn sách thiết kế khá xấu, phông nền quá nhạt.
- Văn phong lãng mạn nhẹ nhàng.
- Câu chuyện chưa thật sự đặc sắc lắm
Mình cho cuốn sách này 5/10 điểm
2
15919
2014-08-14 13:41:33
--------------------------
109031
9303
So với 3 cuốn sách trước của Johanna Lindsey đã được xuất bản thì đây là tác phẩm mà tôi yêu thích nhất. "Hãy Nói Yêu Em" kể về câu chuyện tình ngọt ngào giữa Derek Malory & Kelsey Langton, với tư cách là người thừa kế của dòng họ Malory, hầu tước Haverston kế tiếp, anh là người rất có trách nhiệm và cũng tránh tạo ra những bê bối nhưng cuối cùng anh đã bất chấp tất cả, không màng đến những tai tiếng để kết hôn với Kelsey, tình nhân và cũng là người phụ nữ mà anh yêu bằng cả trái tim. Cô là mẫu phụ nữ mà tôi rất thích, xinh đẹp, điềm đạm và thông minh, một con người dám hy sinh bản thân vì nguời khác và cũng có một trái tim rất nhạy cảm.  Derek & Kelsey cũng là cặp đôi đáng yêu nhất trong gia đình Malory và Derek thật sự là chàng trai vô cùng dễ thương, anh đối xử với tình nhân của mình bằng một thái độ rất nhã nhặn và tôn trọng, tình yêu mà Derek trao cho Kelsey thật khiến cho người khác phải ghen tỵ.
Gia đình Malory phải nói là gia đình đoàn kết nhất mà tôi được biết, tình anh em, tình máu mủ ruột thịt, tất cả đều hiện lên cách sống động trong tác phẩm này. Truyện thật sự rất hay và có ý nghĩa, hy vọng Bách Việt sẽ tiếp tục gửi đến độc giả những phần còn lại trong series này.
5
41370
2014-03-26 20:07:21
--------------------------
103611
9303
Trong gia tộc Malory thì Derek có vẻ "ngoan" nhất (không nổi loạn, quậy phá như ông chú James ^^). Tình yêu của Derek rất quyết đoán, không có chuyện để người yêu bị người ta bắt nạt, người đời vùi dập dè bỉu vì thân thế cơ hàn, thậm chí gia đình cũng không phải là chướng ngại của anh. 

Mặc dù mua nàng Kelsey một cách bột phát trong cuộc rao bán, nhưng khi đã xác định được tình yêu với nàng, anh đã thể hiện ngay chứ không phân vân một phút liệu gia cảnh nàng có làm liên lụy đến danh tiếng gia tộc mình không

Nếu tình yêu của Derek bị ngăn cấm một cách dữ dội, bị trải qua muôn vàn khổ cực sóng gió, chứng tỏ anh ta yếu đuối không có năng lực dẹp yên miệng đời và bảo vệ người yêu. Nhưng truyện của Johanna thì không thuộc tuýp lối mòn đó. Nhân vật của bà mạnh mẽ từ tinh thần đến tinh lực ^^. Và điều đó làm mình rất thích truyện của Johanna, đọc không nhàm chán, rườm rà. Câu từ tuy không hoa lệ nhưng cuốn hút, súc tích, cộng thêm phần tính cách nhân vật đáng yêu làm nên một Johanna rất "chất".
5
168270
2014-01-01 18:45:22
--------------------------
103109
9303
Đây là một trong những tác phẩm ít thành công nhất của Johanna Lindsey, câu chuyện về Derek và Kelsey có phần hơi nhạt nhẽo dưới các nhìn nhận của mình. Câu chuyện không có nhiều điểm nhấn. Tình yêu của Derek và Kelsey có thể khai thác được thêm ở rất nhiều khía cạnh, thân thế của Kelsey tuy cao nhưng vì hoản cảnh cô mới gặp Derek, tình cảm của hai người có thể phát triển với nhiều tình tiết hơn cả một diễn biến khá đều và tẻ nhạt. Tuy nhiên, trong những tác phẩm trước về gia tộc Malory mình thấy nhân vật Derek có phần hơi tẻ, nhưng qua câu chuyện này cũng cảm thấy anh có phần tốt đẹp nhất và là người đứng đắn nhất. Một điểm cộng nữa cho "hãy nói yêu em" chính là việc mình rất thích gia đình Malory luôn gắn bó và che chở nhau trong bất kỳ hoàn cảnh nào. Sự đùm bọc và tình yêu vô điều kiện này cũng chính là một phần trong sự thành công của tác phẩm được đặt dưới xã hội London phồn hoa nhưng khá phù phiếm.
3
125574
2013-12-27 10:45:19
--------------------------
98359
9303
Đây là tác phẩm đầu tiên của Johanna Lindsey khiến tôi thất vọng. Cốt truyện Hầu tước yêu sâu đậm nhân tình làm tôi chú ý. Nhưng khi đọc tôi không khỏi cảm giác hụt hẫng. Bởi tôi mong chờ một chuyện tình đẹp và đầy sóng gió. Tôi mong chờ để thấy được khi họ đên với nhau và bị xã hội ngăn cản, họ sẽ làm những gì để vượt lên trên những rào cản xã hội? Để chứng minh về một tình yêu đích thực không phân biệt giai cấp, không câu nệ giàu sang...?
Và, tôi không cảm nhận được những rung động của cả hai nhân vật, tôi không cảm nhận được họ hướng về nhau như thế nào và tình cảm của họ đã phát triển được bao xa. Đối với tôi, Kelsey chỉ đơn thuần là biết ơn Derek và dùng bản thân mình để trả ơn anh; còn với Derek, đơn giản anh chỉ coi Kelsey như một vật sở hữu!
Hơn nữa, dường như câu chuyện khá "loãng" khi không chỉ tập trung vào hai nhân vật mà còn có quá nhiều chi tiết không cần thiết về những người thân của Derek.
Đó là cảm nhận của riêng tôi.
Mong chờ một tác phầm hay hơn của Johanna Lindsey
1
173004
2013-11-03 13:55:37
--------------------------
83091
9303
Ngay từ khi cuốn sách xuất hiện trên Tiki, mình đã mê nó rồi. Sau khi đọc sơ phần giới thiệu, mình càng quyết tâm có được nó. Sau khi đọc đến nửa câu chuyện mình cảm thấy thật là đúng đắn khi tậu em ấy về nhà. Câu chuyện tình yêu của Kelsey và Derek thiệt tuyệt vời và đáng ngưỡng mộ, Họ luôn nghĩ cho nhau và hướng về nhau bằng tình cảm chân thành. Johanna Lindsey đã miêu tả những cảm xúc của Derek dành cho Kelsey thật dễ thương và ngọt ngào. Mình hoàn toàn không thể cưỡng lại sức hút của Derek. Đây là cặp đôi hoàn hảo nhất của nhà Melody.
5
5118
2013-06-25 11:33:46
--------------------------
78704
9303
Truyện lấy bối cảnh quý tộc Châu Âu ngày xưa thế nên xuyên suốt câu chuyện đều mang một vẻ quý tộc, trang trọng và xa hoa, mình vốn rất thích thể loại này.
Nội dung truyện rất hay, truyện đã cuốn hút mình ngay từ những dòng đầu tiên. Chuyện tình của hai nhân vật chính thật khiến người khác cảm động, khâm phục. Họ sẵn sàng vượt qua mọi lễ giáo phong kiến để nghe theo tiếng gọi của trái tim, tình yêu của họ thật đáng ngưỡng mộ và mơ ước. 
Truyện đã cho mình nhiều cảm xúc khác lạ, đặc biệt dạy cho mình thêm bài học về tình yêu. Lời kể của tác giả rất chân thực và sống động, gần gũi, câu văn trau truốt, tình tiết, tiết tấu cũng khá hợp lý.Đặc biệt mỗi nhân vậ, đặc biệt là hai nhân vật chính đều có những nét riêng biệt.
4
57869
2013-06-02 23:38:19
--------------------------
76865
9303
Một câu chuyện ngọt ngào và đày lãng mạn trong giới quý tộc anh cũ. Một quý ông cho rằng hôn nhân tình yêu là xiềng xích gông cùm - một tiểu thư quý tộc phải bán mình vào nhà chứa ô nhục nhất ở london họ đã gặp nhau, yêu nhau và làm thay đỏi lẫn nhau. Tình yêu là một điều tuyệt vời nó đến với bạn một cách bất ngờ như tình yêu của Derek và Kelsey. Hãy nói yêu em là một trong những câu chuyện ngọt  ngào nhất mà tôi từng đọc. Tình yêu của Derek và Kelsey chân thành và ngọt ngào cùng sự quan tâm của những người thân trong gia đình như dòng nước ấm chảy vào tâm hồn tôi. Có lẽ chính tình yêu và sự giúp đỡ của gia đình đã giúp cho cuộc hôn nhân kì lạ của Derek và Kelsey  đên bén bờ của hạnh phúc.  
4
120261
2013-05-24 17:09:07
--------------------------
72896
9303
"Hãy nói yêu em" lại là một tác phẩm nữa của  Johanna Lindsey. Ngay từ bìa sách, cuốn truyện như đã chạm tới trái tim của mỗi độc giả. Bằng lối viết lãng mạn và ngọt ngào, tác giả như vẽ lên một bức tranh tình yêu thật đẹp, ai cũng phải mơ ước. Nhân vật nữ trong tác phẩm có thân phận không-ai-muốn nhưng cô lại tìm thấy tình yêu của đời mình cùng với một chàng trai mà ai-cũng-mong-muốn. Quả là tình yêu thật éo le, nhưng lại có một sức mạnh để vượt qua mọi rào cản và rồi tiến về nơi hạnh phúc. Cuốn truyện này thực sự lôi cuốn mình trong từng trang sách, và khi đọc xong thì cảm thấy tiếc tiếc. Mình rất thích cuốn sách này!
4
93778
2013-05-05 10:52:29
--------------------------
72229
9303
Đã từng đọc hai trong bốn tác phẩm của Johanna Lindsey xuất bản ở Việt Nam. Và Hãy nói yêu em (Say you love me) là tác phẩm thứ ba. Không hề thất vọng tí nào mà còn vượt ra khỏi những mong muốn của tôi lúc ban đầu.
Thứ nhất, lời giới thiệu truyện đã là một thành công nhỏ tạo nên sự tò mò trong lòng người đọc. Thứ hai, bìa sách quả là một ... tác phẩm nghệ thuật. Hai điều ấy đủ khiến người đọc phải bỏ túi tiền ra để mua cho bằng được - như tôi là một ví dụ điển hình.
Tuy nhiên, theo tôi nói, không hề tiếc nuối vì đã mua. Nội dung rất cuốn hút. Kết cấu chặc chẽ hơn nhiều truyện trước đây của bà rất nhiều, phải nói là rất rất nhiếu! Johanna Lindsey xây dựng nhân vật nam chính rất khác - khá là khác so với những nhân vật nam thường trực của bà. Derek Malory, như đã gặp trong Gentle rogue (Gã cướp biển quý tộc) còn trong Magic of you (Ma lực tình yêu) thì không biết có gặp không vì tôi chưa đọc tác phẩm đó, tưởng chừng cũng là một gã trai mê gái, quyến rũ đến mê hôn, giàu nức vách đổ tường, mạnh mẽ, độc đoán - những điều tôi thấy chán khi cứ mãi nhai đi nhai lại ở các nhân vật nam. Thật ra thì trong câu truyện của chính mình, anh chứng minh điều ngược lại. Cũng đẹp trai =.= nhưng không đến nỗi bậc nhất trong mắt nữ chính. Cũng giàu có - nức vách. Nhưng điều làm anh khác biệt là thái độ nghiêm túc và có trách nhiệm trong từng hành động, không đùa giỡn, không vô tâm và đặc biệt là một người hết sức lương thiện. Quá khác biệt trong cả gia tộc Malory ấy chứ. Chính điều đó khiến tôi có thiện cảm hơn với Derek và dường như yêu nhân vật đó hơn bất kì nhân vật khác.
Còn Kelsey Langton - một cô gái khác biệt, quá khác biệt với nhiều mẫu phụ nữ Lindsey vẫn hay đặc ra. Có lẽ chính vì vậy mà hai nhân vật ấy tạo nên một cặp đôi khác biệt cũng nên. Cô vẫn là một người phụ nữ dũng cãm, yêu gia đình mình. Cô không hề nóng tính (như những nhân vật nữ trước) mà hết sức thông minh. Kelsey biết nghĩ suy và chấp nhận với thực tại chứ không cố chấp đối kháng để rồi khi đã làm rối tung beng câu chuyện lên thì mới phát hiện ra có cố chấp cũng chẳng được gì. Thích Kelsey ở chỗ đấy đây!
Đến với nhau qua một buổi đấu giá đã là một nút thắc mở đầu cho câu chuyện và đồng thời cũng tạo cho ta hứng thú khi mới bắt đầu đọc. Và rồi những chi tiết về sau khiến ta đọc mãi đọc mãi không ngừng. Lindsey nhẹ nhàng giới thiệu các nhân vật qua từng chi tiết nhỏ, không có một giọng văn hài hước nhưng cứ khiến ta cười mãi cười mãi.
Yêu nhẹ tựa một làn gió mát. Đến và làm mát lòng ta tự bao giờ thì có ai hay. Tình yêu của Kelsey và Derek là như thế. Không có những cuộc rượt đổi mãi chả thấy điểm dừng. Không có những cuộc đối đầu nảy lửa. Không có bất kì xung đột nào - trừ một chút ban đầu nhé ^^. Vậy mà họ như yêu nhau lúc nào không hay. Yêu từ cách hành xử của nhau. Yêu từ cách người kia nói chuyện, từ cách họ bầu bạn. Khiến cho người ta cứ mơ mơ như đi trên một nơi xa lắm - nơi tình yêu nhẹ hẫng và ... yêu dễ quá!
Trong câu chuyện còn có tình cảm yêu thương của gia đình Malory, tình anh em, tình mẫu tử - tuy không nhiều nhưng vẫn là một điểm gì đó tạo nên câu chuyện. Tất cả hòa quyện làm cho Say you love me cân bằng về mọi mắt chứ không đơn thuần chỉ có tình yêu.
Tất cả, tất cả tạo nên một câu chuyện xuất sắc mà tôi được đọc từ Johanna Lindsey. Yêu lắm tiểu thuyết này - nhất là nhân vật ấy nhé! Mong bà sẽ có nhiều đột phá hơn trong các sáng tác của mình.
5
19049
2013-05-01 09:21:29
--------------------------
68269
9303
Câu chuyện này làm mình nhớ đến một bộ phim, trong đó nhân vật nữ cũng bị rao bán, bị tống vào nhà chứa để đàn ông mua vui, chịu nhục nhã, ê chề,... Nhưng cuối cùng tình yêu cũng tìm đến cô, công bằng cũng trả lại cho cô cuộc sống sung túc, hạnh phúc suốt đời. "Hãy nói yêu em" đã nói thay những điều như thế, kết cấu chuyện rất đơn giản, không có gì cầu kì, hướng theo một mạch tương đối chặt chẽ. Johanna Lindsey đã tạo nên những xung động mới trong lòng người đọc, đã phác họa được một bức tranh của xã hội Anh cận đại với những thú chơi đê tiện của đàn ông, những thói hư tật xấu của tầng lớp quí tộc, những lề thói ràng buộc con người. Đối lập với tất cả những điều đó chính là tình yêu chân chính, tình nghĩa gia đình ruột thịt, người ta có thể vì thế mà gạt bỏ tất cả, rất ý nghĩa.
3
4223
2013-04-11 08:53:44
--------------------------
64432
9303
Cuốn truyện đề cao tình yêu đích thực và tình cảm gia đình, cũng như khuyên nhủ những đôi tình  nhân không được quên mất việc nói lời yêu để giữ lửa mối quan hệ. Cá nhân mình đọc cuốmn này là duy nhất trong series 5 cuốn, vì thế nhiều chỗ  mình đọc mà... không hiểu gì và cảm thấy nó khá thừa thãi và nhàm chán. Cuốn này cũng có những cảnh rất là...nóng, miêu tả chi tiết nội tâm nhân vật, cùng những đoạn tả cảnh gia đình đem lại cho mình cảm giác như là gia đình Malory rất gắn kết vs nhau.
Một cuốn truyện đáng đọc nếu bạn là fan của tiểu thuyết tình cảm những  năm 1800
4
25903
2013-03-20 01:47:29
--------------------------
64426
9303
Một câu chuyện rất nhẹ nhàng, trong sáng và đặc biệt là cực kì cực kì lôi cuốn. Ngay từ khi đọc những trang đầu tiên cuốn sách này mình biết mình đã phải lòng nó. Thật không hổ danh là một trong những tác giả tiểu thuyết lãng mạn cổ điển tuyệt vời nhất Johanna Lindsey đã vẽ lên 1 câu chuyện hay và chan chứa tình yêu thương lứa đôi cùng tình cảm gia đình. Kelsey là một cô gái quá ư thông minh, nhạy cảm. Derek là một hình mẫu quí ông lí tưởng của giới quí tộc châu Âu xưa. 1 câu chuyện tình quá đẹp và đầy quyến rũ. Đây phải là một cuốn sách gối đầu giường của tất thảy các cô gái mê văn học trung đại phương Tây!
5
98370
2013-03-20 00:19:09
--------------------------
59644
9303
Một cuốn truyện đầy tình cảm về gia đình, tình yêu và sức mạnh lớn lao của chúng. Mình thực sự thích gia đình Malory khi đọc hết tập 5 của series này. Yêu tình yêu chân thành của Derek dành cho Kelsey cũng như của Jason dành cho Molly, tình thân của gia đình lớn, sự vĩ đại của James trong việc giải quyết những bọn xấu. Nếu không có những sự tình cờ thì hẳn trên đời này sẽ chẳng còn có định mệnh, một tình yêu được thể hiện qua sự chân thành đến từ con tim dù không quá mãnh liệt nhưng cũng khiến cho người đọc cảm thấy ấm áp và thực sự thích tình yêu ấy và cả nhân vật nữa. Điều mình ấn tượng nhất là cách giải quyết các nút thắt rất hợp lý và đầy bất ngờ, mạch truyện rất mạch lạc và lôi cuốn. Và có thể nói Derek là nhân vật mà mình thích nhất trong series này, và điều đó làm cho câu chuyện của anh là quyển truyện mà mình thích nhất.
4
12054
2013-02-16 14:27:09
--------------------------
59590
9303
Tình yêu là 1 điều kỳ diệu, kết nối những trái tim yếu mềm để khiến chúng mạnh mẽ hơn.
"Say you love me" là một trong những quyển sách yêu thích của tôi. Chuyện tình trong quyến sách này nhẹ nhàng như 1 dòng nước ấm chảy qua tim mình, đọng lại những cảm xúc đáng yêu khiến cho 1 người chưa từng yêu cũng muốn được yêu và yêu như 2 nhân vật trong câu chuyện. Nhưng tôi nghĩ điều mà cuốn sách muốn chuyển tải chính là hãy can đảm nói lời yêu, hãy can đảm nắm giữ tình yêu khi bạn tìm thấy người xứng đáng. 
5
5747
2013-02-15 22:13:23
--------------------------
