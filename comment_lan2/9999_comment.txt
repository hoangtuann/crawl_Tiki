439344
9999
Mình mua cuốn này chung với cuốn English Grammar In Use (xanh lá), cuốn đó là giải thích cách sử dụng từng loại ngữ pháp, còn cuốn sách A Practical English Grammar Exercises là giúp ôn tập kĩ hơn về các ngữ pháp đó.

Bài tập trong sách có hầu hết các loại ngữ pháp mà chúng ta sử dụng, bài tập tăng dần từ dễ đến khó, sắp xếp theo từng mục dễ tìm kiếm nên rất phù hợp với việc ôn tập.

Các bạn nếu muốn ôn tập cho mình ngữ pháp tốt hơn thì nên mua đầy đủ cả hai cuốn để dễ dàng cho việc ôn tập
5
89607
2016-05-31 09:10:47
--------------------------
381099
9999
Cũng như quyển lý thuyết thì đến quyển bài tập này cũng tình bày nhìn khá là nhiều chữ, tuy nhiên mình thấy bài tập khá hay và khó. Có bài tập cho từng phần và có cả bài tập tẩm hợp để củng cố kiến thức ( đây là điều mình khá thích ở sách ). Nói chung đây là cuốn sách khá bổ ích cho các bạn muốn trau dồi ngữ pháp tiếng anh, từ cơ bản đến nâng cao đều có thể dùng được sách này.
Lượng bài tập trong sách khá nhiều, mà sách lại đóng khổ nhỏ, nên sách khá là dày, việc mang đi coi bộ không được tiện lắm.
3
125948
2016-02-16 16:23:25
--------------------------
359454
9999
A Practical English Grammar Exercises, cuốn sách bài tập tiếng anh nâng cao tuyệt vời, nó rất bổ ích và giúp tôi rất nhiều. Trước khi tìm đến nó, tôi học tiếng anh cũng khá lắm nhưng sau những bài bài kiểm tra điểm thấp tôi khó chịu vô cùng bởi trước giờ việc học tiếng anh với tôi không khó. Vậy tôi sai ở đâu, ngã từ lúc nào? Tôi mệt mỏi và thế là bạn tôi đã giới thiệu cuốn sách này, nó đến với tôi thật bất ngờ. Sau khi sử dụng nó, tôi đã tiến bộ hơn hẳn, tôi thích thú với kết quả này và đang cố gắng hơn để hoàn thành các bài tập trong quyển sách. Tôi vô cùng an tâm khi có nó ở bên.
4
914758
2015-12-27 08:51:41
--------------------------
276687
9999
Mình mua cuốn sách này cùng với cuốn Ngữ Pháp Tiếng Anh Thực Hành - Understanding And Using English Grammar - Third Edition (lý thuyết và thực hành). Cuồn này có những bài tập ngữ pháp nâng cao hơn. Sách trình bày bài tập theo từng chủ đề và có đáp án rõ ràng chi tiết. Điểm yếu của sách là chưa có các bài tập trắc nghiệm. Sách này thuần luyện ngữ pháp theo phương pháp cổ điển là làm bài tự luận. Sách có đáp án nhưng không có phần diễn dãi chi tiết, nên các bạn tự học sẽ hơi khó khăn một chút. Nhưng một khi đã dùng đến cuốn này thì chỉ cần đáp án cũng đủ để các bạn hiểu vấn đề.
3
71615
2015-08-24 11:38:00
--------------------------
160167
9999
Mình mua cuốn sách này hồi năm ngoái và đã làm được gần hết các bài tập trong đó rồi. Với mình, ngữ pháp là chìa khóa quan trọng để mở các cánh cửa kỹ năng trong tiếng Anh. Nếu không có vốn kiến thức ngữ pháp chắc chắn thì sẽ không dễ vượt qua được những kỳ thi quốc tế - mình cho là vậy. Hồi mua cuốn sách này, mình chỉ nghĩ đơn giản là dùng nó để ôn luyện mà thôi. Nhưng giờ thì mình mới thấy nó thật hữu ích. Không chỉ giúp mình hệ thống lại kiến thức đã học mà còn bổ sung, nâng cao và rút ra những lỗi sai mà trước giờ mình không để ý đến. Nhờ vậy mà mình đã hoàn thiện hơn trong các bài essay cũng như speaking được chuẩn xác hơn. Mình cực kỳ yêu thích cuốn sách này. Cảm ơn Tiki và NXB. Hy vọng mọi người cũng học được nhiều điều hay từ cuốn sách.
5
433873
2015-02-22 19:34:19
--------------------------
