339710
9270
Mình thường mua những cuốn truyện ngắn bằng tiếng anh để rèn luyện khả năng đọc hiểu và nâng cao vốn từ vựng của bản thân. Cuốn sách này khá đơn giản với mình, thêm vào đó, sách còn kèm CD, giúp mình luyện nghe nữa. Dạng sách này rất hợp với các bạn muốn học tiếng anh nhưng không quá khuôn mẫu, phù hợp với mức độ tiếng anh căn bản. Mình sẽ đầu tư thêm mảng sách này để có thể giúp mình rèn luyện và nâng cao kỹ năng giảng dạy ngoại ngữ cho các bé ở trình độ tiểu học
5
481887
2015-11-18 15:56:01
--------------------------
305190
9270
Mình từng đọc tác phẩm "Anne tóc đỏ dưới chái nhà xanh" bằng tiếng Việt nên quyết định mua cuốn này để trau dồi thêm vốn tiếng Anh.
Vì đây là sách học ngoại ngữ nên nội dung sách được biên tập lại và giản lược đi rất nhiều nhưng nhìn chung vẫn giữ được nét đáng yêu, trong sáng của Anne tóc đỏ.
Các từ ngữ trong sách khá đơn giản, thích hợp với các bạn có trình độ tiếng Anh căn bản. Sách lại được tặng kèm cả CD nên có thể kết hợp giữa việc đọc sách và nghe CD để rèn luyện thêm khả năng tiếng Anh.
4
138880
2015-09-16 21:13:39
--------------------------
