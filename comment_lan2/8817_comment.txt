426929
8817
Tuy chỉ là nam phụ, nhưng có lẽ hầu hết độc giả đều mong muốn một cái kết thật đẹp sẽ đến với Lệnh Viên và Thế Huyền.
Sau khi đọc xong bộ truyện này, trong đầu tôi chỉ còn lại cái tên "Thế Huyền". Y là nam phụ trong "Đế hoàng phi", nhưng trong lòng tôi thì sẽ mãi là nam chính. Tôi bị Thế Huyền hấp dẫn ngay từ lần đầu tiên khi y xuất hiện trong bộ dạng yếu ớt nhưng mạnh mẽ mà kiên cường. Y yêu Lệnh Viên - cô cô của mình. Tuy biết rằng tình yêu của y đối với vị cô cô này là trái với luân thường đạo lí, nhưng y vẫn cứ yêu, vẫn cứ mãi giữ mối tình đầy đau thương này trong lòng.
Y là Thiếu đế Bắc Hán, nhưng cũng là Bùi Vô Song. Khi là Thế Huyền, y không thể bước qua làn ranh giới địa vị, không thể nói xóa bỏ thân phận để nói tiếng yêu nàng. Nhưng chỉ khi là Bùi Vô Song, y mới có thể trút bỏ tất cả, mới có thể tự do yêu nàng, mới có thể đường hoàng bảo vệ nàng.
Y yêu nàng đến vậy, yêu đến nỗi khi bản thân lâm vào hiểm cảnh, y vẫn chỉ nghĩ đến nàng. Nhưng rồi đổi lại, y được cái gì chứ?
Một tình yêu đầy oan trái, đầy ám ảnh, đầy đau thương, đầy nước mắt.
Một cái chết đầy thương tâm, đầy xót xa, đau đến không thể nói nên lời.
Cho đến cuối cùng, y vẫn chỉ nghĩ đến nàng, chỉ mãi mong nàng được bình an. Đối với y, chỉ cần như vậy là đã đủ rồi.
Khi đó, tôi như bị giáng một đòn mạnh vào tim, tưởng như không thể thở nổi. 
Tôi không tin một người như y, một Thế Huyền kiên cường mạnh mẽ bao năm chống lại bệnh tật lại ra đi một cách đầy bất ngờ như vậy. Tôi không tin rằng y đã chết, lại cứng đầu, coi đó là mưu kế của y nhằm tiêu diệt thế lực của Thụy vương. Nhưng trên đời không có cái gọi là "tưởng", sự thật vẫn sẽ mãi là sự thật. Đến cuối cùng, y chết, nhưng vĩnh viễn không thể biết được sự thật về mối quan hệ giữa y và Lệnh Viên, sự thật về mối nghiệt duyên oan trái này.
Trong những bộ truyện tôi đã từng đọc, có lẽ đây là mối tình thấm đẫm nước mắt, khiến tôi cảm thấy ám ảnh nhất, day dứt nhất, cũng là đau đớn nhất.
5
1244104
2016-05-07 22:55:16
--------------------------
368707
8817
Đế hoàng phi cũng giống như nhiều tiểu thuyết tình cảm hiện đại khác , mang nhiều sự kỳ bí , lôi cuốn theo mạch dòng các tình tiết , khá vui , náo nhiệt ở hình ảnh các tranh cãi về lối xử sự của nhiều nhân vật đan chéo lên nhau làm phải dáo dác tìm lại sự kiện hành động của thế huyền , Bùi vô song hay cả Lệnh viên đều phải có sự tranh giành còn phía của giai đoạn ganh ghét , hơn thua còn bộc phá cách mà chịu đựng nó bị lên án gay gắt .
4
402468
2016-01-14 14:40:51
--------------------------
366459
8817
Đọc xong Đế hoàng phi, mình có cùng cảm nhận với khá nhiều bạn, đối với mình, nhân vật đặc sắc nhất, đáng nhớ nhất và đáng thương nhất chính là Thiếu đế Thế Huyền.
Nếu được xếp thứ tự các nam nhân dành tình yêu cho Lệnh Viên, mình thích và ấn tượng nhất với Thế Huyền, tiếp theo là Thẩm Ngọc Trì, kế tiếp là Khánh Vương và cuối cùng mới là Doãn Duật.
Hình tượng Thế Huyền thật sự đã để lại ấn tượng sâu sắc gần như là ám ảnh trong lòng mình. Một vị Thiếu đế bề ngoài nhu nhược nhưng thực tế là người có khát vọng, có trí tuệ và có một trái tim biết yêu thương mãnh liệt. Thế Huyền và Bùi Vô Song - hai thân phận trong cùng một con người, một thân phận để cai trị thiên hạ và lạnh nhạt với Lệnh Viên, còn một thân phận để cảnh báo nàng, để đôi lúc có thể bộc phát sự quan tâm của hắn đối với nàng. Thế Huyền thật sự rất khổ sở , và hắn mang theo tình yêu 'trái luân thường' ấy vào cõi vĩnh hằng. Thật sự trong tất cả sự mất mát và chết chóc của truyện, duy lúc Thế Huyền ra đi mình mới xúc động đến chảy nước mắt, còn những người khác cùng lắm chỉ là thấy tiếc nuối mà thôi.
Sau Thế Huyền thì người mình thích hơn cả là Thẩm Ngọc Trì, dù nhân vật này chỉ xuất hiện chút ít phần đầu truyện, nhưng chí ít ta đều biết tình cảm hắn dành cho Lệnh Viên là thật lòng, rằng hắn là người duy nhất cho Lệnh Viên được những tháng ngày yên bình cuộc sống phu thê bình đạm trước khi bước vào vòng tranh đấu khắc nghiệt. Cái chết của Thẩm Ngọc Trì là mất mát đầu tiên Lệnh Viên gánh chịu để bước lên ngôi vị Trưởng Công Chúa, nhân vật này tuy đất diễn cực cực ít, nhưng vẫn ghi dấu sâu đậm trong tiềm thức mình.
Còn Khánh Vương, hắn thật lòng yêu Lệnh Viên, dù hắn là một con người có dã tâm, nhưng mình tin tưởng luyến ái của hắn dành cho nàng là không hề giả dối. Nếu xét ra nhân vật này còn đáng ấn tượng hơn Doãn Duật. Mình không hiểu vì sao nhưng Doãn Duật - mối tình đầu của Lệnh Viên và cũng là người may mắn đến cuối cùng được sống trọn đời bên nàng, lại không gây được cảm tình thật sự nơi mình. Trong lòng mình Doãn Duật cùng lắm là bến đỗ cuối cùng cho cuộc đời sóng gió của Lệnh Viên, ừ thì tình cảm hai người cũng gặp không ít trắc trở, nhưng mình vẫn cảm thấy Doãn Duật khá mở nhạt, tình cảm giữa hai người cũng không khiến mình cảm động dù hắn cũng yêu nàng suốt kể từ thời hai người còn non trẻ.
Rốt lại, Đế hoàng phi thực sự là một thiên truyện rất hay, rất đáng đọc và suy ngẫm. Lệnh Viên đã mất nhiều, nhưng cũng được nhiều, và nếu theo mình thì thứ nàng đánh mất đau đớn nhất chính là Thế Huyền, giây phút nghiệt ngã nhất trong cuộc đời nàng chính là thời điểm phát hiện Thế Huyền và Bùi Vô Song là một. Tác giả kết thúc như vậy là vừa đủ, ngắn gọn mà khắc lại nỗi nuối tiếc khôn nguôi trong lòng độc giả !
4
473539
2016-01-09 23:25:25
--------------------------
331791
8817
Số phận của nữ chính quả thực rất bất hạnh.Đầu tiên cô phải chịu cảnh mất người thân, mất gia đình để gánh vác giang sơn, tranh đấu với ruột thịt. Rồi đem mình cầu thân để đổi lấy bình an cho quốc gia, cuối cùng hôn phu lại bị chết. Cuộc tranh giành quyền lực chính trị diễn ra không ngừng, và cô luôn bị tính kế, dù cố gắng thoát ra đến mấy cũng không được. Cô đã phải hi sinh rất nhiều, kể cả thân xác, kể cả tình yêu thật sự của mình để đánh đổi bình an của quốc gia, cuối cùng cô được gì, vẫn là nước mất nhà tan.

Thậm chí đến cuối cùng, khi ta tưởng rằng cái kết đã viên mãn, thì cô biết được sự thật phũ phàng về Vô Song_Thiếu Huyền. Đó sẽ là day dứt cả đời ám ảnh cô.

Mình rất thích nhân vật Thiếu Huyền, và thấy tiếc cho cái chết của anh, thậm chí cái chết ấy được miêu tả khiến mình thấy anh chỉ là một nhân vật nhỏ bé, không có nhiều giá trị trong truyện.

Nói chung, đây là một chuyện ngược toàn tập, nhưng mà không quá đặc sắc, nhiều lúc đọc sẽ khiến bạn khó chịu không ít.
2
403246
2015-11-05 08:54:48
--------------------------
235563
8817
Mình đọc văn án cứ ngỡ nam chính là Thế Huyền. Đọc quyển 1 thấy TH rất tốt, rất thích TH. Nhưng không ngờ TH lại chết, xót xa nhất là gián tiếp vì Lệnh Viên mà Thế Huyền lâm vào tình cảnh khốn cùng, đất nước suy vong, cuối cùng còn phải chết. Còn về Doãn Duật và Lệnh Viên, tình cảm của hai người này, theo mình, hoàn toàn không thể làm vơi đi nỗi ám ảnh vì TH chết, không thể làm mờ đi tình yêu sâu sắc, cao cả của Thế Huyền. Truyện này dường như là quá bất công với nam phụ, quá nghiêng về nữ chính. Đặc biệt là lúc khi Lệnh Viên biết tất cả chân tướng, dường như sự đau đớn, hối hận của nữ chính là hoàn toàn không đủ. Tuy nhiên mình khá hài lòng với kết thúc truyện: để cho con của Doãn Duật với phi tần khác lên làm vua, 2 người rời đi sống cuộc sống bình thường.
4
610259
2015-07-21 10:12:23
--------------------------
213480
8817
Gấp cuốn sách lại, mình đã thẫn thờ rất lâu, cảm thấy lòng nặng nề không tả nổi. Cuối cùng nàng và Doãn Duật cũng đã được ở bên nhau, tránh được cảnh đấu đá, cũng đã có những tháng ngày yên ổn, hạnh phúc, nhưng hạnh phúc đó liệu có thể vẹn tròn khi mà nàng đã ngỡ ngàng phát hiện ra Thế Huyền và Bùi Vô Song kỳ thực là một? Nàng được ở bên cạnh người nàng yêu, nhưng người thân thì chẳng còn, hạnh phúc sao? Vui sao? Mình đã khóc khi Thế Huyền chết nhưng lòng vẫn chỉ canh cánh về “cô cô” – người cô không chung dòng máu mà chàng đã yêu trong thầm lặng suốt bao năm.
5
630600
2015-06-23 20:28:38
--------------------------
206693
8817
Đọc xong tập hai, tôi cũng cảm thấy hài lòng vì cái kết của câu chuyện. Tôi hài lòng không phải vì cái kết viên mãn, không phải vì Lệnh Viên cuối cùng rồi cũng được ở bên người mình yêu mà tôi hài lòng vì sau tất cả mọi chuyện thì Lệnh Viên đã biết được tình cảm sâu sắc, chu đáo của Thế Huyền dành cho mình cũng như cả thân phân thật sự của vị Thiếu đế trẻ tuổi ấy. Qủa thật đây là tác phẩm ngôn tình đầu tiên mà tôi cảm thấy thích nam phụ Thế Huyền hơn tất cả, mặt khác nam chính Doãn Duật tôi cứ luôn cảm thấy nhân vật này cứ mờ nhạt so với Thế Huyền. Tuy rằng tình cảm của Doãn Duật dành cho nữ chính cũng không thua kém Thế Huyền, nhưng qua lối kể của tác giả tôi vẫn chưa cảm nhận được sự sâu sắc trong suy nghĩ, trong nội tâm nhân vật và đặc biệt là trong tình tình yêu của cả hai. Nhưng với cuốn một, Hoại Phi Vãn Vãn miêu tả vô cùng chi tiết những hành động lẫn những suy nghĩ đằng sau đó của nhân vật, đặc biệt là tình tiết giữa Thế Huyền và Lệnh Viên. Khi có sự xuất hiện của BVS xen vào, tôi phân vân không biết ai sẽ là nam chình và sau này biết được BVS-TH vốn là một, tôi đã mừng biết mấy. Tôi mừng vì LV không phải lựa chọn một trong hai, mừng vì LV đã từng thừa nhận với TH rằng mình thích BVS. Nhưng tất cả dần rơi vào tuyệt vọng khi LV hoàn toàn không hay biết gì cả và rồi thì DD xuất hiện-mối tình đầu thanh mai trúc mã của LV. Qủa thật tôi buồn cho số phân của LV nhưng tôi còn buồn hơn cho TH- vị thếu đế trẻ tuổi tài năng luôn hết mình vì xã tắc Bắc Quốc nhưng cuối cùng đổi lấy được chỉ là sự suy tàn của nước nhà. Thật tàn khốc!!! Tàn khốc cho nữ chính và cho cả TH. Đây là cuốn tiểu thuyết có kết thúc mà so với nhiều người đó là cái kết vô cùng viên mãn nhưng đối vói tôi thì nó chẳng viên mãn chút nào cả

5
190618
2015-06-10 12:39:05
--------------------------
170962
8817
Tôi rất thích thể loại cung đấu và đặc biệt yêu thích Hoại Phi Vãn Vãn qua tác phẩm "Mệnh Phượng Hoàng". Vì vậy tôi đã tìm và đọc "Đế Hoàng Phi" - một tác phẩm cung đấu khá được của HPVV. 

     Đọc xong cuốn sách, trong đầu tôi bỗng nghĩ, khi viết cuốn sách này, HPVV có lẽ không dựng trước một cốt truyện mà như nhiều tác giả ngôn tình khác,  xây dựng nhân vật chính trước rồi các tình tiết truyện mới nghĩ đến đâu viết đến đấy. Bởi ở tập một, tác giả đã vẽ nên một "thiếu đế" rất xuất sắc, đẹp trai, tài giỏi, thông minh, cơ trí. Chàng dù không nắm  thực quyền nhưng luôn chủ động dành lấy quyền lực của mình. Chàng đáng lẽ phải "giệt tận gốc" của  vị "cô cô" đang nắm giữ giang sơn của mình nhưng lại không nỡ ra tay. Chàng nảy sinh tình cảm không nên có với cô cô của mình, thứ tình cảm trái với luôn thường đạo lý, trời đất không dung. Nên hận thì không thể hận, không nên yêu thì lại yêu. Dày vò như vậy, đau đớn như vậy, tôi cứ ngỡ chàng là nam chính, ngỡ rằng đến cuối cùng HPVV sẽ cho chàng một cái kết viên mãn. Nhưng không ngờ vị thiếu đế xuất hiện từ đầu truyện, được miêu tả nội tâm tỉ mỉ kia lại không phải nam chính. Chàng không đi được đến cuối cùng, không tìm được hạnh phúc, đến lúc chết vẫn không biết được kì thật "cô cô" không phải là cô ruột của chàng. Chàng ra đi vẫn lưu luyến bóng dáng người con gái ấy, vẫn đau đớn dày vò vì "thứ tình cảm trái với luôn thường đạo lý" ấy, chàng ra đi vẫn ôm lấy chấp niệm của mình.

    Tôi đã nghĩ rằng HPVV thay đổi nam chính ngay giữa khi câu chuyện đang viết dở. Bởi Lệnh Viên đã từng thừa nhận với Thế Huyền rằng nàng thích Bùi Vô Song - một thân phận khác của Thế Huyền. Vẽ ra cho tôi cái ảo tưởng rằng sau khi nàng biết thân phận của chàng, chàng và nàng sẽ hạnh phúc bên nhau. Thế nhưng sau này lại xuất hiên một mối tình từ năm mười ba tuổi của nàng - Doãn Duật. Nàng yêu Doãn Duật, Doãn Duật cũng yêu nàng, hai người có một kết cục viên mãn. Thế nhưng tôi thật sự không thích Doãn Duật. Có lẽ bởi hiệu ứng "gà con", tôi nhìn thấy Thế Huyền trước, mặc định chàng là nam chính, thế nên không thích bất cứ ai ở bên nữ chính ngoài chàng. Hoặc bởi lẽ tôi đau đớn, xót xa cho tình yêu, cho sự cô độc của Thế Huyền, muốn chàng được hạnh phúc. Hay chỉ đơn giản tôi thấy tình yêu của chàng với Lệnh Viên khắc cốt ghi tâm hơn Doãn Duật.

     Với tôi, Thế Huyền mới là nam chính của bộ truyện. Vậy nên khi chàng chết đi, tôi đã không còn động lực, kiên nhẫn để dở tiếp cuốn sách. Tôi chỉ đọc lướt qua thật nhanh một vài chi tiết tiếp theo rồi xem ngay cái kết. Cuối cùng Lệnh Viên cũng biết được thật sự Bùi Vô Song - vị sư thúc luôn dịu dàng của nàng là ai, biết được tình yêu sâu đậm của Thế Huyền dành cho nàng, biết được chàng vì nàng mà hi sinh như thế nào. Nhưng, biết rồi thì sao? biết rồi thì thế nào? biết rồi thì làm được gì? khi mà chàng và nàng đã sinh ly tử biệt. Một cái kết có thể là HE với tác giả, với một số đọc giả khác. Nhưng với tôi, đó là SE bởi Thế Huyền của tôi, nam chính của tôi đã không tìm được hạnh phúc của mình.

     Đối với tôi "Đế Hoàng Phi" đã kết thúc ngay sau khi thiếu đế băng hà. Nếu đem đặt ĐHP lên bàn cân với Mệnh Phượng Hoàng, tôi vẫn thích MPH hơn. Tuy nhiên, xét về tổng thể, ĐHP là một bộ truyện cung đấu khác hay, đáng đọc trong thời kì "hàng hóa khan hiếm" hiện nay. Nếu bạn yêu thích MPH, bạn cũng nên tìm đọc ĐHP để hiểu rõ hơn về mối quan hệ của các nhân vật trong MPH.
4
62019
2015-03-20 23:25:26
--------------------------
166213
8817
Cũng chẳng mong được cái kết nào hơn cái kết này: Lệnh Viên thu về được giang sơn, nàng lấy người mình yêu, sống một cuộc đời không tranh giành, đấu đã, đúng như ước nguyện của nàng. Tuy có phải hi sinh biết bao nhiêu thứ, làm trái tim nàng đau đớn, quặn thắt hết lần này sang lần khác, nhưng với một bộ cung đấu, đặc biệt là có sự khốc liệt của chính trường nhiều nước thế này, là quá êm đẹp rồi.
Mình không thích nhân vật Thế Huyền như nhiều bạn nhận xét, với mình, tình cảm y dành cho cô cô ngay từ tập một đã có chút gì đó gượng gạo, khiên cưỡng. Và mình cũng không thích phong cách hoàng đế khá nhu nhược như y. Không bá đạo và kiên cường như anh Khâm trong Mệnh phượng hoàng.
5
333831
2015-03-12 09:21:50
--------------------------
127676
8817
Không biết có ai như mình, khi đọc truyện này thì người mình mong muốn đến với Lệnh Viên nhất không phải là thế tử Doãn Duật, càng không phải là Khánh Vương điện hạ, mà chính là vị thiếu đế tài năng đoản mệnh Thế Huyền hay không... Vì ủng hộ cho tình cảm của Thế Huyền từ đầu đến tận khi khép sách lại, nên đối với mình Đế hoàng phi là một câu chuyện tình buồn đến day dứt. Dù biết Lệnh Viên yêu sâu sắc Doãn Duật, nhưng mình vẫn thầm hy vọng và chờ đợi Lệnh Viên một lần ngoái lại nhìn đến Thế Huyền, nhìn đến sự hy sinh thầm lặng bao năm của y, chỉ tiếc là kết cục lại không như mình mong muốn...
5
105666
2014-09-25 23:34:44
--------------------------
123546
8817
Một câu truyện rất hay. Tôi thích đọc thể loại cổ trang, cung đấu như vậy. Tôi cũng sẽ không nói gì về nội dung truyện, tôi chỉ nói về nhân vật. Tôi thích Thế Huyền - một nam tử dám yêu cô cô của mình nhưng tiếc thay, trong mắt Lệnh Viên - cô cô của y, y mãi chỉ là một đứa trẻ. Doãn Duật - nam tử chung thủy, cả đời y chỉ có Kiều Nhi. Khánh Vương - y cũng thật lòng thật dạ với Lệnh Viên, nhưng vì ngai vị, vì công sức chờ đợi bao nhiêu năm,cái cách y làm với nàng, nàng không thể chấp nhận. Nhờ đọc truyện này, mà tôi cũng đã đến với một câu truyện tuyệt vời khác - nó cũng chính là thời sau của truyện này. Tôi yêu cái cách viết truyện của Vãn Vãn, trong truyện mọi chi tiết đều rất logic, giữa hai truyện với nhau, sự logic, nối kết đó vẫn được thể hiện. Nếu đọc mà không thực sự nhập tâm vào từng nhân vật, tôi không nghĩ, độc giả có thể nhận ra được điều đó
5
119412
2014-09-02 13:51:04
--------------------------
108576
8817
Câu truyện này tôi tình cờ được biết đến qua nhiều người bạn trên mạng giới thiệu cho. Có lẽ đây là một trong những câu truyện hay nhất, ám ảnh nhất mà tôi đã từng đọc. Cái kết tuy là SE nhưng tôi vẫn cảm thấy có một cái gì đó rất buồn. Buồn vì trong truyện đã có quá nhiều người phải hi sinh. Buồn vì đến cuối cùng Lệnh Viên mới nhận ra được hóa ra "hai con người đó" là một. Dẫu biết đến cuối cùng Lệnh Viên đã lấy được người mình yêu, đã lấy lại được giang sơn nhưng còn những người thân của nàng đã lần lượt rời bỏ nàng, ra đi mãi mãi. Lệnh Viên đã chịu hết nỗi đau này đến nỗi đau khác, cảm giác như những gì mình đã cố gắng bảo vệ nhưng cuối cùng lại bị hủy hoại hết. Những nỗi đau này có lẽ sẽ bám theo nàng suốt cuộc đời!

Và nhân vật mà tôi ấn tượng nhất không phải là nam chính Doãn Duật là mà Thiếu đế. Đến cuối cùng, trước khi chết, chàng vẫn luôn nghĩ tới nàng, mặc dù cái chấp niệm "nàng ấy là cô cô của ta" luôn bám theo chàng nhưng đúng là cuộc đời trớ trêu, hai người vốn không hề có quan hệ ruột thịt, vậy mà đến khi nhắm mắt, chàng vẫn không biết được sự thật đó, tình cảm bấy lâu nay của chàng với người chàng luôn gọi là "cô cô" đến cuối cùng vẫn không có kết quả. Khi đọc đến gần cuối câu truyện mình vẫn mong kì tích sẽ xuất hiện nhưng truyện mang đậm chất đời thực, cuộc đời rất tàn khốc, đến cuối cùng kì tích ấy vẫn không xảy ra. Gía như chàng sớm biết được sự thật này thì chắc chắn câu chuyện sẽ qua một bước chuyển khác. Nhưng "giá như" chỉ là "giá như", vì cuộc đời nào có "giá như"!

 Chàng chết còn nàng thì sau hơn 20 năm mới biết được tường tận chuyện chàng làm cho nàng. Một cái kết thật khiến cho người ta day dứt!
5
27946
2014-03-20 09:40:26
--------------------------
98266
8817
Trong chốn thâm cung , bao nhiêu mưu kế , hại người , thật là khó đoán . Những chi tiết trong truyện vô cùng sinh động . Cảm giác như được koi bộ phim " Cung tam kế " , cũng đấu đá tranh giành như vậy !! Tôi thắc mắc là bộ truyện hay như vậy mà tại sao không có người đọc và bình luận !!! 

Thật sự tôi cảm thấy tiếc và thương nhất là Thế Huyền : đem lòng yêu cô cô của mình , một mối tình không bao giờ có thể bắt đầu. Đến cuối truyện , tác giả muốn Lệnh Viên biết được Thế Huyền và Bùi Vô Song là một thật khiến người ta đau lòng . Thà cả đời không biết . 

Còn người mà có lòng dạ khó đoán và tính toán hơn người chính là Thái Hoàng Thái Hậu . Bà ấy thật đáng sợ . Dù chỉ xuất hiện trong truyện có vài trang nhưng những việc bà ta làm đều tính đường đi nước bước cho đến mấy chục năm sau.

Dù sao cuối cùng Lệnh Viên và Doãn Duật cuối cùng cũng được ở bên nhau , họ đã được như ý nguyện , không tranh giành , đấu đá nữa .... Vậy cũng có thể coi là HE rồi .  
Vì trước giờ tôi biết những chuyện tranh giành trong cung không bao giờ có kết cuộc tốt , như là : Khuynh thế hoàng phi  , Đông Cung ,....
5
90456
2013-11-02 19:55:50
--------------------------
