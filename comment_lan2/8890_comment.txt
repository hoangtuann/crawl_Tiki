473269
8890
Đọc xong cuốn một mà muốn bay ngay vào đọc kết thúc của cuốn 2 chứ không muốn đọc khúc đầu nữa. Truyện viết cũng được nhưng hơi dài dòng, đọc một lúc mà có cảm giác chán chường. Rất may là truyện kết thúc HE, và tôi rất ấn tượng với cách xử lý tình huống xảy ra của nhân vật nữ chính. Quá hay, quá thông minh, quá bất ngờ. Mình cứ nghĩ một cô gái thì không thể nào có sự suy xét tỉ mỉ, cẩn thận đến như vậy được. Đến nhân vật nam chính cũng phải khâm phục.
3
949022
2016-07-10 13:42:10
--------------------------
462572
8890
Mình mua quyển này lúc nó đang giảm 40%, thấy khá rẻ nên mua luôn. Bối Vi Lan viết truyện rất hay, truyện kiểu nhẹ nhàng nhưng không kém phần lãng mạn. Đọc truyện này, thấy thương cho tình cảm đơn phương của nhân vật nữ chính, một tình yêu đơn phương thầm lặng. Kiểu như là chỉ cần người mình yêu hạnh phúc là đủ rồi vậy. Một tình yêu vượt qua cả không gian và thời gian. Tưởng chừng như thời gian sẽ làm nhạt nhòa thứ tình cảm đơn phương ấy, nhưng không... nó như một cơn gió thổi bùng lên khiến nó mãnh liệt hơn nữa vậy. Mình thật sự thích câu chuyện này, đủ ngọt ngào, đủ trắc trở để có thể đến được với nhau
5
278052
2016-06-28 18:18:30
--------------------------
405936
8890
Câu chuyện được giải đáp toàn bộ trong tập 2, Diêu Nhạn Lam thoạt nhìn rất nhỏ bé, luôn cần sự che chở nhưng có suy nghĩ cũng như ý trí không hề dễ dàng khuất phục, vẫn là mô típ hy sinh bản thân để cứu lấy người thân và người mình yêu, và vẫn biết rằng sẽ chẳng đơn giản để có được điều như mình ước nguyện...nhưng tác giả vẫn không để câu chuyện đi quá xa với thực tế, rất gần gũi với xã hội hiện tại khi quyền lực bị thao túng thì đồng tiền sẽ làm được tất cả, bản chất con người cũng vì thế mà thay đổi để cuối cùng liệu ai đó có thể thoát ra để quay lại được như xưa...
4
1196547
2016-03-27 14:43:39
--------------------------
377980
8890
Một cuốn truyện mới với sự lột xác trong cách viết của Bộ Bộ Vi Lan : nhẹ nhàng và sâu sắc hơn . Sau khi đọc hết tập 1 , mình đã tìm luôn đến tập 2 với mong muốn biết được diễn biến câu chuyện sẽ còn đi đến đâu . Tình yêu đơn phương của Khánh Đệ trong ngần ấy năm khiến minh đọc mà xót xa quá . Cô cũng như Diêu Thượng Lam , có thể hì sinh hết minh vì người minh yêu . Có lẽ cái sai của các nhân vật là họ đã sống trong một xã hội thối nát bị thao túng bởi quyền lực . Dù sao thì đến cuối cùng DTL cũng đã nhìn ra chân tình của TL , tạo ra kết truyện làm mình hài lòng .
4
743273
2016-02-03 22:52:50
--------------------------
375494
8890
Truyện là tấm gương của thế hệ trẻ tuổi , suy nghĩ chín chắn cho tình yêu và quyết định sáng suốt trước ngã rẽ , bởi khi phải trả giá cho những hành động của mình thì cô gái Khánh Đệ vẫn dung tha cho tất cả , mọi đề nghị trước khi công khai còn phải được xem xét điều kiện phạm phải của Thượng Nghiêu trước mối tình dang dở , bất chấp sự ghét bỏ nhưng đành phải rời bỏ những thứ mình phải đánh đổi lấy , cô gái có sự tươi sáng để mọi người dựa vào . truyện đáng để đọc .
4
402468
2016-01-28 14:55:23
--------------------------
357647
8890
Sinh nhật mình, mẹ mua tập một truyện này mà không hỏi ý kiến... mình rất không vui vì nhìn bìa truyện không hợp gu. Nhưng đọc xong tập một mình phải lên Tiki mua tập hai ngay. Truyện hay hơn mình tưởng nhiều. Về nội dung, phải nói là truyện khá buồn, thậm chí hơi bi kịch, nhưng mạch truyện lại logic không hề gượng ép. Tác giả xây dựng nhân vật khá trau chuốt về nội tâm suy nghĩ. Mình nghĩ truyện này không giống kiểu truyện mì ăn liền mà khiến người đọc phải suy nghĩ một chút, rất giá trị. Mình khá thích cách hành văn nhẹ nhàng của tác giả nữa đó.
4
249905
2015-12-23 22:00:59
--------------------------
310502
8890
Đây là tác phẩm đầu tiên của Bộ Vi Lan mà mình đọc, lúc nhìn thấy bìa sách mình rất ấn tượng, nhìn rất đặc biệt. Nội dung truyện khá thực tế, tác giả không hề xây dựng một nhân vật hoàn hảo tới từng chi tiết. Mình rất thích tập một nên đã tiếp tục đọc tập hai, và phải nói rằng Bộ Vi Lan đã không làm cho mình thất vọng. Qua bao nhiêu khổ đau, thù hận, cuối cùng Thượng Nghiêu cũng hiểu rõ được điều quan trọng nhất trong cuộc sống của mình, kịp thời trở về để tìm lại Khánh Đệ, tạo nên một kết thúc viên mãn nhất. Đây là một tác phẩm rất hay, rất đáng để đọc.
4
115397
2015-09-19 16:36:58
--------------------------
290546
8890
Cũng như moi cuốn truyện khác ,tập hai luôn là mấu chốt của câu chuyện!Ở tình yêu nơi đâu các bạn sẽ bắt gặp những hình ảnh quen thuộc của những bộ phim Trung Quốc hiện đại!Những con người có số phận lầm than,cực khổ,phải chịu nhiều thiệt thòi!Và khi họ đã mất hết người họ yêu thương thì họ sẽ không còn là chính họ!Họ sẽ bất chấp mọi giá để báo thù!họ sẽ không còn là chính họ nữa!một câu chuyện đầy biến cố và chính hận thù sẽ thay đổi con người của họ!Họ không thể trở thành con người hiền lành nhân hậu như trước nữa!!
3
413637
2015-09-05 15:02:48
--------------------------
271294
8890
Ở tập 2 này tiếp tục câu chuyện còn dang dở ở tập 1, dường như các chi tiết quan trọng của truyện đều nằm ở tập 2 khiến mình thấy hơi áp lực vì một lúc lại đề cập đến quá nhiều vấn đề. Rồi còn xuất hiện thêm nhân vật mới nữa, có lúc mình cảm thấy hơi rối. Quá nửa tập là truyện không vui nhưng đọc đến đoạn nam chính Thượng Nghiêu đã nhận ra bản thân yêu Khánh Đệ nhiều thế nào mình thấy khômg uổng công kiên nhẫn đọc. Cái kết màu hường phấn giúp cho mình cảm thấy truyện này cũng không quá nhạt.
3
89637
2015-08-19 00:57:40
--------------------------
231624
8890
Vì thấy nội dung giới thiệu khá hay nên mình liền mua , đây cũng chính là cuốn sách đầu tiên mình đọc của Bộ Vi Lan . Tác giả không làm mình thất vọng, với lối văn nhẹ nhàng , trao chuốt khiến người đọc không cảm thấy nhàm chán. Khánh đệ - một cô gái kiên cường , mạnh mẽ , mình ngưỡng mộ tình yêu của Khánh Đệ dành cho Thượng Nghiêu. Cốt truyện rất mới không như những quyển ngôn tình đầy lãng mạn nhưng quyển sách này rất hấp dẫn , nhiều tình tiết lôi cuốn . Một cái kết có hậu cho hai nhân vật chính khiến mình rất hài lòng .
5
391948
2015-07-18 10:55:43
--------------------------
175059
8890
Thực sự tôi thích tập 1 hơn một chút, dẫu có đắng cay nhưng cũng ngọt ngào. Trong tập 2, vấn đề trả thù, những nút thắt chốn quan trường được đề cập đến quá nhiều, và nặng nề nhất là việc Khánh Đệ không muốn thấy chàng trai mình yêu thương nay đã là một con người khác nên đành rời xa anh. Đến lúc đó, Thượng Nghiêu mới biết anh yêu cô nhiều hơn mình nghĩ. Quả vậy, thường đến lúc mất đi con người ta mới biết thứ đó quan trọng với mình tới nhường nào. Thật may là cuối cùng Thượng Nghiêu cũng có thể buông bỏ tất cả hận thù để tìm về cô gái bé nhỏ của anh. Tôi thích giọng văn của Bộ Vi Lan, đậm tính hiện thực, điểm chút buồn thương, chút u ám ngày mưa nhưng rồi ngày mai trời lại sáng.
4
57459
2015-03-29 12:06:54
--------------------------
156738
8890
Ở tập hai này thì tôi thấy hơi mất kiên nhẫn vì quá nhiều nhân vật mới xuất hiện cùng với các mối quan hệ chốn quan trường khiến cho tôi dễ bị rối nếu không tập trung nhớ tên nhân vật. Tuy nhiên, những cố gắng trong sự nghiệp cùng sự trưởng thành dần dần trong con người của Thượng Nghiêu. Sự tự chủ, độc lập trong tình cảm của Khánh Đệ cũng là một bước trưởng thành của cô trong tình cảm dành cho Thượng Nghiêu, yêu nhưng độc lập, không phụ thuộc, phục tùng đã khiến Thượng Nghiêu nhận ra bản thân xưa nay chỉ thụ hưởng chứ không cố gắng yêu cô. Diễn biến tâm lý nhân vật tăng theo thời gian và cũng rất hợp lý. Tuy câu chuyện dài nhưng đó là điều khó tránh khỏi. Bộ Vi Lan một lần nữa đã tạo được dấu ấn qua tác phẩm Tình yêu nơi đâu này trong lòng tôi. Đặc biệt, bìa của cả hai tập rất đẹp, Nhìn trên máy đã đẹp, cầm trên tay còn đẹp hơn. Bách Việt đã có tiến bộ trong việc thiết kế bìa và nội dung sách. Hi vọng tương lai BV sẽ có nhiều đầu sách hay hơn nữa.
3
63376
2015-02-06 07:29:54
--------------------------
151766
8890
Là lần thứ hai đọc tác phẩm của Bộ Vi Lan (BVL) sau Trầm Hương Uyển, thật sự làm cho tôi càng ngày càng cuốn hút với những câu chuyện mà tác giả tạo nên, nhân vật của BVL không hào nhoáng, không ảo tưởng mà mỗi nhân vật đều có một tạo hình riêng, một tính cách và sự trau chuốt riêng.

Tôi ngưỡng mộ tình yêu của Khánh Đệ, khâm phục ý chí và nghị lực của Trần Bắc Nghiêu, cảm thương cho số phân của gia đình họ Diêu, yêu cái hòa nhã, dễ chịu của Tần Thạnh, mãn nguyện với tình yêu dù muộn nhưng ấm áp của Ái Đệ và Hắc Tử, và ngoài ra còn những nhân vật phụ khác như Nhị Hóa, Chu Quân, Bành Tiểu Phi... họ là những con người trẻ, luôn có mơ ước, hoài bão và nhiệt huyết, không ai không phải đứng giữa ngã ba đường, nhưng họ đều cố gắng đưa ra những quyết định không phải làm quá nhiều người tổn thương!

Đọc xong câu chuyện, tôi vừa thấy thỏa mãn, vừa thấy an lòng, cảm thấy một niềm an ủi, thực sự những câu chuyện, những nhân vật của BVL rất thực tế, không quá cưỡng cầu, cũng không huyễn hoặc nhân vật của mình, BVL chỉ muốn hướng độc giả đến với những gì tốt đẹp nhất của cuộc sống, dù bạn ở đâu, chỉ cần cảm thấy hài lòng với những gì mình làm, tự tin sống với con người thật của mình, vì đời người chỉ cần một chữ "tình" mà thôi! :)
5
34656
2015-01-20 22:09:09
--------------------------
137278
8890
Quả thật khi đọc Tình yêu nơi đâu của Bộ Vi Lan tôi luôn thấy có hơi hướm của Cho anh nhìn về em của Tân Di Ổ (Vì tôi đọc Tân Di Ổ trước). Chắc bởi 2 bộ truyện này có một số chỗ tương đồng, ví dụ như nữ chính lớn lên trong môi trường không hạnh phúc, tính cách tương đối khép kín, nhân vật chính từng ngồi tù... và cả giọng văn cũng như tâm trạng man mác buồn trong quyển 1 của cả 2 tác phẩm nữa.
Tôi đánh giá Tình yêu nơi đâu 4.5 sao bởi tính thực tế của câu chuyện cũng như vì tác giả không hình tượng hóa các nhân vật như trong ngôn tình khác. Các nhân vật đều có cá tính, có nội tâm riêng, ngay cả nhân vật phụ cũng hay, có vai trò không thể thiếu trong tác phẩm. Nữ chính tính cách tương đối ổn, tuy nhiên tôi thích em gái nữ chính hơn, là phụ nữ nên phải nín nhịn nhiều, nhưng đến lúc tức nước vỡ bờ thì đâu ra đó. Nam chính thì khỏi phải bàn, yêu thương nữ chính hết mực nhưng không vì thế mà lụy tình và thiếu quyết đoán. 
Tác phẩm tuy còn đôi chỗ đánh máy nhầm, đặc biệt là đoạn cuối tập 2, lúc Khánh Đệ lúc Ái Đệ loạn hết cả lên nhưng tóm lại đây vẫn là một tác phẩm hay, mang lại nhiều cảm xúc cho đọc giả khi thưởng thức!
5
31339
2014-11-25 18:21:04
--------------------------
117891
8890
Đọc xong hai tập của tình yêu nơi đâu tôi càng khẳng định chắc  chắn một chân lý rằng chỉ cần có niềm tin và hy vọng tình yêu vĩnh cữu sẽ đến với bạn . Minh chứng ấn tượng nhất chính là mối tình của Khánh Đệ và Thương Nghiêu trong truyện . TRuyện có tiết tấu vừa nhanh vừa chậm lúc lên lúc xuống làm tim mình cứ đập thình thịch hoài . Thật may sau bao nhiêu khó khăn gian khổ họ cũng đã đến được với nhau . Một cái kết thật có hậu và vừa lòng bạn đọc .
4
337423
2014-07-24 09:39:00
--------------------------
116889
8890
Qua cuốn truyện này, tôi dường như cảm nhận thêm được sự diệu kỳ của tình êu cũng như số mệnh. 16 năm trước, Khánh Đệ và Khương Thượng Nghiêu dường như chỉ là những người lướt qua đời nhau trong phút chốc. Huống chi Khương Thượng Nghiêu còn có người thương Diêu Nhạn Lan. Ấy mà bây giờ, họ đã trở thành một phần của nhau rồi. Nhưng anh thay đổi nhiều quá, con người anh giờ đây toàn những mưu toan, toan tính. Thật may là cuối cùng anh đã hiểu ra. Tôi đọc câu nói: "Nếu một ngày anh phải vào tù, em có thể như trước kia, tiếp tục đến thăm anh không?" Mà trực trào nước mắt. Tôi rất thích tình yêu mà Bộ Vi Lan tạo cho hai nhân vật chính, sự nhẹ nhàng, tinh tế mà sâu sắc xuyên suốt trong tác phẩm của cô.
Một điều hay nữa là việc Ái Đệ kết hôn với Hắc Tử. Đối với cặp đôi này tôi chỉ có thể nói là: hài hước. Ban đầu đọc truyện tôi không thích Ái Đệ vì sự hồ đồ và đanh đá của cô, còn Hắc Tử thì cũng không phải nhân vật thu hút sự chú ý của tôi. Nhưng khi họ đến với nhau thì tôi nhận ra họ dành cho nhau, một cặp đôi rất dễ thương!
5
123326
2014-07-13 12:30:01
--------------------------
98577
8890
Ấn tượng đầu tiên của mình về Nhạn lam là một cô gái đẹp luôn có nụ cười tươi trên khuôn mặt xinh xắn. Lúc đầu mình nghĩ Nhạn Lam yếu đuối và ngây thơ, không biết sự đời, nhưng không phải vậy, Nhạn Lam vẫn đủ sáng suốt để phân tích thiệt hơn về việc ứng xử với Ngụy Hoài Nguyên sợ anh ta làm hại Thượng Nghiêu, trước khi chết vẫn để lại lá thư đủ làm nhà họ Ngụy chật vật một phen. Nhạn Lam không ngốc nhưng quá nhẹ dạ, cả tin, trong lúc đang tuyệt vọng mới bị Ngụy Hoài Nguyên lừa. Cô ấy đánh đổi hạnh phúc để cứu Thượng Nghiêu, cứu mẹ, nhưng đáng tiếc đến cuối cùng chẳng thể cứu được ai cả. Nhạn Lam thật đáng thương, mình vẫn còn nhớ cô gái với nụ cười vừa tự tin vừa ngượng ngùng đến tìm gặp Khánh Đệ và nói: "chào em, chị là Diêu Nhạn Lam", giá như cô ấy đủ mạnh mẽ, bình tĩnh như Khánh Đệ thì có lẽ mọi chuyện sẽ khác....
4
39798
2013-11-05 13:24:03
--------------------------
89467
8890
"Tình yêu nơi đâu" có cốt truyện khá mới lạ và thực tế. Nữ chính Khánh Đệ không tài giỏi xinh đẹp rực rỡ như nữ chính của những truyện khác nhưng bù lại tâm hồn của cô lại vô cùng đẹp đẽ. Cô yêu thương, bao dung và sẵn sàng đợi chờ nam chính bao nhiêu năm. Nam chính Thượng Nghiêu cũng không thuộc dạng công tử nhà giàu đẹp trai lung linh như những chuyện khác, anh có một tuổi thơ khá cơ cực, sau khi lớn lên cũng phải chịu nhiều bất hạnh, hàm oan vào tù, người yêu chết nhưng chính nhờ tình yêu của Khánh Đệ mà anh được sống một cuộc sống mới.
Câu chuyện diễn ra ở một thế giới bị thao túng bởi quyền lực, tiền tài và những âm mưu đen tối, ân oán giang hồ nhưng nổi bật lên giữa thế giới đó vẫn là tình người đẹp đẽ.
Nếu "Sự nhầm lẫn tai hại" là một câu chuyện hài hước với văn phong nhẹ nhàng, dí dỏm thì "Tình yêu nơi đâu" lại là sự lột xác của Bộ Vi Lan. Giọng văn vẫn nhẹ nhàng, mượt mà, đôi lúc lại mang đến cảm giác u ám, bất lực nhưng đôi lúc cũng rất tươi sáng và tràn đầy yêu thương. Sau khi gấp trang sách cuối cùng lại, dư âm của truyện vẫn còn đâu đây.
Theo mình, "Tình yêu nơi đâu" là một câu chuyện rất đáng đọc 
4
123606
2013-08-02 08:16:42
--------------------------
