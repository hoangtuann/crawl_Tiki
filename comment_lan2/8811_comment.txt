553897
8811
Câu chuyện rất thực đi vào lòng người , từ ngữ phong phú , cá tính .... tuy nhiên sản phẩm sách mình nhận được thì lại vô cùng xấu : bìa con , trầy xước , bẩn bụi ... đáng khen là tiki giao hàng rất được , đúng lịch
4
2111495
2017-03-25 13:13:52
--------------------------
520289
8811
Tác phẩm là một bức tranh đẹp về một thế hệ của thủ đô, những con người năng động và luôn sống vì đam mê
4
1524844
2017-02-07 09:31:15
--------------------------
417097
8811
    Lá rơi trong thành phố là câu chuyện của những thanh niên trẻ của thủ đô Hà Nội ngàn năm văn hiến. Nói về thế hệ trẻ hà Nội tác giả đã xây dựng nhân vật có cái tên thân mật Củ Đậu trong hành trình từ thời thơ ấu cho đến khi trưởng thành, làm một nhân viên văn phòng. Tác phẩm không có những tình tiết giật gân gay cấn mà rất nhẹ nhàng, sâu lắng, đồng thời tác giả Lê Xuân Khoa không hề gò bó các nhân vật trong cái khuôn nào cả mà để cho những dòng cảm xúc dẫn dắt họ, cho họ sự tự do. Đọc tác phẩm ta như cảm thấy có một chút mình trong đó, sẽ thấy một thời tuổi trẻ tràn đầy nhiệt huyết xen lẫn cảm xúc tiếc nuối, day dứt.
5
748237
2016-04-16 10:31:52
--------------------------
281907
8811
Mình hiếm khi có đủ kiên nhẫn để đọc hết một cuốn sách dài cảu một tác giả trẻ Việt Nam nào đó, song lần này với "lá rơi trong thành phố " đó lại là ngoại lệ. Bằng cách viết chân thành, giản dị , tự nhiên không chút màu mè hoa mĩ nhưng rất sâu sắc Lương Đình Khoa bằng cách nào đó đã khiến người đọc không thể bỏ dở cuốn sách. Nhẹ nhàng, sâu lắng nhưng cũng không kém phần sôi nổi, sinh hoạt và đời sống của những người trẻ trong cuốn sách hiện lên dưới ngời bút của Khoa thật rõ nét và sống động. Nhiều kỷ niệm được khơi gợi qua từng dòng viết qua những bài hát được Khoa chia sẻ trong truyện. Những bài hát một thời mình từng mê đắm.Yêu thêm tuổi trẻ yêu thêm Hà Nội qua những trang sách của Lương Đình Khoa. Mong sẽ được đọc thêm nhiều tác phẩm mới của anh.
4
75031
2015-08-28 19:51:54
--------------------------
160910
8811
Đây là một tác phẩm hay tạo cho mình nhiều cảm xúc. Cốt truyện khá thú vị thêm vào lối kể chuyện khác biệt và từ ngữ rất xuất sắc. Là một tác phẩm của người trẻ và là một tác phẩm tạo cho mình nhiều sự liên hệ đến bản thân trong quá trình trưởng thành. Mình thích những đoạn kể đan xen như thể tất cả chỉ là những hồi ức, nghĩ sao viết thế, không cầu kì. Bìa sách và những hình mình họa cũng rất đẹp, mình rất thích. Nói chung là một tác phẩm rất hay  và rất trẻ trung từ hình thức đến nội dung.
5
116202
2015-02-26 03:56:15
--------------------------
155608
8811
Đọc xong một tác phẩm dài hơn 400 trang trong vòng chưa đến 1 ngày, cảm xúc dạt dào quá khiến mình phải ngay lập tức lên Tiki để chia sẻ những suy nghĩ của mình về " Lá rơi trong thành phố " :)

Lâu lắm rồi, thật sự là đã lâu lắm mình mới đọc được một  quyển truyện viết về giới trẻ trong thời đại mới khiến mình tâm đắc đến thế.  Giọng văn của tác giả quá sức cuốn hút, không hề cầu kỳ, trau chuốt, nó nhẹ nhàng và thản nhiên đến lạ, khiến mình có cảm giác không phải mình đang đọc đâu, mà hình như là chính Lê Xuân Khoa đang kể cho mình nghe đấy.

Viết về một nhân vật hoản toàn chúng ta có  thể bắt gặp ở bất cứ nơi nào trong đời sống, Củ Đậu - một anh chàng IT mê âm nhạc, từng câu từng chữ của tác giả cứ thế đưa chúng ta chìm đắm trong thế giới của anh, thế giới của những người trẻ trong thời đại toàn cầu hóa, từ anh chàng Củ Đậu thời sinh viên đến anh chàng Củ Đậu nhân viên văn phòng, chúng ta có thể cười trước những cuộc nói chuyện đầy hài hước của Củ Đậu với bạn, cười trước sự ra đời của tác phẩm kinh điển " Đẹp chồng chất ", cũng sẽ đôi lần lòng lắng lại trước những dòng văn nói về tình yêu của anh với mối tình đầu, giọng văn nhẹ nhàng mà sao lòng buồn quá đỗi...

Cũng có một điểm cần lưu ý, đó là các chương trong câu chuyện không hề  được sắp xếp theo một trình tự thời gian. Đây là điều lúc đọc khiến mình hơi khó khăn một chút, bởi lẽ có thể chương này là nói về Củ Đậu sinh viên, chương sau đã nói về anh lúc đã đi làm. Thật sự là mình đã phải cần vài phút để sắp xếp lại những suy nghĩ, trước khi đọc tiếp chương sau :)) Hơi lăn tăn lúc đọc, nhưng càng về sau mạch truyện càng logic nên không thể chê bất cứ điều gì được.

Có một điều mình yêu nhất về Lá rơi trong thành phố, đó là những dòng tác giả viết về Hà Nội. Về con phố Lò Đúc đầy ắp kỷ niệm của Củ Đậu, về áp lực cuộc sống khiến dòng đời luôn hối hả và cả về... trận lụt lịch sử năm 2008 nữa.  Là một " nhân chứng " được chứng kiến và trải qua trận lụt đó, khi đọc tác phẩm này và được gợi nhớ lại , mình thật sự thấy rất vui :D

Lê Xuân Khoa đã từng nói rằng tác phẩm của anh chỉ là một câu chuyện bình thường về con người và cuộc sống. Phải rồi, bình thường nhưng không hề tầm thường, tác phẩm không mang tính triết lý quá sâu sắc nào cả, nhưng sau khi đọc nó,  mình tin chắc các bạn sẽ  " cảm " nó, như mình :)
5
134015
2015-02-01 14:55:23
--------------------------
143580
8811
"Lá rơi trong thành phố" là câu chuyện về cuộc sống của một lớp thanh niên, một nhóm nhân viên văn phòng cùng những vấn đề xoay quanh cuộc sống của họ. Họ sống trong một bầu không khí hối hả, hối hả để cháy trọn tuổi trẻ, hối hả để giành lại hay là tái tạo lại năng lượng sống của mình. Chuyện cứ thế tĩnh lặng và chầm chậm trôi, không gì nhiều nhưng lại như rất nhiều chuyện đã diễn ra để lại một dư âm đậm nét trong lòng người. Cách viết của Lê Xuân Khoa giản dị, gần gũi và rõ ràng, khắc họa chính xác cái hồn của tuổi trẻ qua những câu nói, những hành động.
"Lá rơi trong thành phố" đậm đà phong vị Hà Nội, ờ đâu đó trong lòng thủ đô cổ kính này vẫn còn rất nhiều những con người trẻ tuổi đang lao đi để kiếm tìm một cuộc sống đích thực. Nhạ nhàng và bình dị, nếu bạn thực sự quan tâm đến một thế hệ, bạn sẽ thấy mình đang trôi đi theo dòng chảy của tác phẩm từ lúc nào.
4
4223
2014-12-23 18:42:36
--------------------------
98545
8811
"Lá rơi trong thành phố" kéo người đọc vào những mảng cảm xúc réo rắt, giữa những ngược xuôi bộn bề của không gian và thời gian. Quá khứ, hiện tại và tương lai tháo gỡ từng nút thắt nhỏ trong những dày vò của cảm xúc. Để thấy hiểu hơn những góc nhỏ tâm hồn giữa lòng thủ đô phồn hoa. 

Cách Hành Tây rời xa Củ Đậu không phải là sự trốn chạy. Có chăng đó là vì cô biết họ sẽ khó thuộc về nhau vì những áp đặt môn đăng hộ đối, sự xứng đáng và tôn trọng nhau từ suy nghĩ trong lối sống của giới trí thức sinh ra ở vùng đất hiếu học. Nhưng ở họ niềm tin mãnh liệt về tình yêu vẫn còn đó trong tâm khảm của cả 2, len lỏi vào sâu từng ngóc ngách tâm hồn, như một bí mật họ giữ riêng cho họ mà thôi. 

Rồi Thanh xuất hiện như làn gió ấm, nhưng có đủ sức để kéo Củ Đậu ra khỏi mớ ngỗn ngang hỗn độn của tiếng yêu chưa thành trong quá khứ. Những gì con người ta chưa đạt được đều sẽ bị ích kỷ nhen nhóm và phủi đầy một lớp bụi mờ. Tiếp tục trải nghiệm với Lê Xuân Khoa về những nhân vật anh xây dựng, để thấy lá chỉ rơi trong thành phố hay cứa nát tâm hồn mỏng manh của độc giả!
4
11586
2013-11-05 08:02:39
--------------------------
94378
8811
Một trong những cuốn tiểu thuyết Việt Nam về giới trẻ đương đại hay nhất đối với mình. Ngạc nhiên là giữa thời buổi nhiễu nhương, ai cũng muốn xù lông múa vuốt như hiện nay lại có nhà văn chọn cách kể chuyện khiêm nhường nhưng cuốn hút đến thế.  "Lá rơi trong thành phố" không đao to búa lớn, không triết luận dông dài, chỉ có đời sống thường nhật của những nhân vật dường như ta đã gặp đâu đó. Tác giả có khả năng cảm thụ cuộc sống cực kỳ kinh tế. Những trang văn của anh chân thực, sống động. Cách nhìn nhận vấn đề khách quan và cởi mở. Cảm xúc mà Lê Xuân Khoa đem tới bằng câu chuyện của Củ Đậu là hiếm thấy trong số những cuốn sách trong nước thời gian gần đây. Mình mong là còn nhiều người biết đến và đọc cuốn sách này.
5
172309
2013-09-13 12:55:13
--------------------------
92950
8811
Mình mất khá nhiều thời gian để đọc cuốn sách này. Không phải vì nó không hấp dẫn, mà vì tác giả mang lại quá nhiều những cảm xúc khác nhau, khiến mình muốn đọc chậm lại để thưởng thực dư vị của từng trang sách. "Lá rơi trong thành phố" không phải là một cuốn sách đặc biệt, chủ đề của nó không mới, cách dẫn truyện cũng rất giản dị, nhưng bằng sự đồng cảm và thấu hiểu, Lê Xuân Khoa đã làm cho câu chuyện của mình trở nên lôi cuốn một cách kỳ lạ. Xuyên suốt tác phẩm, hình ảnh của một Hà Nội trầm tư đang đứng trước những thay đổi của thời đại cứ trở đi trở lại, với biết bao trăn trở, biết bao nỗi niềm, khiến người đọc, dù ở đâu đi chăng nữa, cũng cảm thấy mình như trở thành một phần của cái thành phố ấy. Tác giả đã hòa trộn con người, tình yêu, những ký ức, và cả một chút hài hước nhẹ nhàng vào trong cuốn sách, tạo nên một câu chuyện khó có thể nào quên lãng.
Gập cuốn sách lại, mình nhận ra mình đã tìm thấy được rất nhiều điều. Đó là ý nghĩa của cuộc sống, của tình yêu thương và niềm hy vọng; đó là những bài học về cách sống, cách đối mặt với sự đổi thay; hay chỉ đơn giản đó là những vẻ đẹp nhỏ bé mà đôi khi ta không để ý, như một chiếc lá vàng mùa thu khẽ rơi trong một góc phố nhỏ.
4
109067
2013-08-30 14:19:01
--------------------------
88419
8811
Cuối cùng cũng có cuốn này. Một cuốn dễ đọc nhưng không dễ nắm bắt hết những ẩn dụ bên trong. Một câu chuyện tưởng như đơn giản lại không hề đơn giản. Hà Nội trong sách không chỉ có những mái nhà liêu xiêu, những khu phố cổ như tranh Bùi Xuân Phái, mà còn có những công trình mới từng ngày mọc lên, những tòa nhà chọc trời, một đô thị đầy khói bụi. Cảnh tượng hai nhân vật chính trong truyện là Củ Đậu và Hành Tây gặp nhau giữa đám ùn tắc khiến ta ngẫm ngợi về khoảng cách giữa con người với con người trong thời đại tên lửa: họ đứng hai bên lề một con đường mà không thể tiến lại gần nhau vì bị ngăn trở bởi dòng xe cộ. Đi vào những chủ đề gai góc, nhưng tác giả không tạo cho người đọc cảm giác mệt mỏi, nặng nề hay bi lụy. Giữa hoàn cảnh khó khăn, các nhân vật trong truyện vẫn luôn lóe lên một niềm hy vọng, khát vọng được yêu, được sống và vượt lên trên tất cả, đó là tình người sâu đậm. Tóm lại là mình rất thích.
5
138367
2013-07-26 11:22:09
--------------------------
87470
8811
Tôi là một độc giả ở Sài Gòn, chưa từng biết thông tin về Lê Xuân Khoa trước đây và cũng hiếm khi đọc sách của các nhà văn trẻ Việt Nam. Cách đây một tuần, tôi tình cờ nhìn thấy cuốn này ở hiệu sách. Không hiểu điều gì đã thôi thúc tôi mua nó và đọc một lèo thâu đêm. Khác với nhận định một số báo viết về cuốn sách, tôi cho rằng cuốn sách không hề hiền. Nhà văn hẳn là người táo bạo và “điên” khi dám đi vào những cấp độ tâm lý gai góc với một giọng điệu bình thản, điềm nhiên. Cái đáng sợ ở đây là tất thảy tiếng cười và nước mắt đều nuốt ngược vào trong và đôi khi trộn lẫn một cách tàn nhẫn. Những trang sách làm tôi nhớ Hà Nội, nhớ những con đường nhỏ, hàng cây xanh, những con người ít lời nhiều ý. Nói chung, nhà văn khá khiêm tốn khi nói chỉ kể một câu chuyện. Nó thực sự đã tạo cho tôi nhiều cảm hứng, không thể không viết những dòng này sau khi đọc lại lần thứ hai…
5
136440
2013-07-18 14:45:21
--------------------------
87186
8811
Đúng như bạn ở trên nhận xét, sau khi đọc hết truyện, có một cảm giác nao nao. Tôi luôn thích những kết thúc trọn vẹn, các nhân vật chính đạt được thành công và hạnh phúc. Nhưng có lẽ để lại một khoảng trống cho người đọc là dụng ý của anh Xuân Khoa khi dùng kết thúc mở để khép lại cốt truyện có bố cục chặt chẽ và mạch lạc. Đọc cuốn này, tôi ấn tượng với phong cách dung dị của anh Xuân Khoa cùng với những bức tranh minh họa có gì đó rất hồn nhiên. Các nhân vật trong truyện hồn nhiên sống giữa biến đổi thăng trầm của dòng đời. Những ai chỉ muốn giở nhanh các trang để xem chuyện gì sẽ xảy ra thì sẽ chẳng thấy chuyện gì xảy ra cả vì các tình tiết rất đỗi bình thường như đời sống hàng ngày. Đọc truyện này như thưởng thức một bản nhạc Jazz, những cái hay nằm trong những cái tưởng chừng bình bình mà lại tràn đầy suy cảm, giống như những chiếc lá thản nhiên quay vòng trong gió.
5
54039
2013-07-16 09:25:28
--------------------------
87118
8811
Tác giả viết chắc tay, ban đầu thấy giống như những mảnh rời rạc nhưng càng đọc càng bị ngấm vào câu chuyện. Một bức tranh sinh động từ từ hiện rõ. Giọng văn đậm chất Hà Nội, nhẹ nhàng, hài hước, ẩn sau đó là những ngẫm ngợi sâu lắng về một thế hệ. Những cảnh và người được miêu tả duyên dáng và chân thực. Nhà văn dụng công trau chuốt các chi tiết nhỏ. Bối cảnh đời sống xã hội trong truyện rất đương đại và thực tế. Không phải ai cũng sẽ thích cách kết của cuốn sách, một cái kết mở đan xen giữa hy vọng, tươi sáng và lạc lõng, mơ hồ, nhưng cuộc sống luôn như vậy. Nhìn chung, đây là một cuốn hay, có tư duy mới mẻ và tìm tòi trong cách viết. Tôi sẽ tiếp tục chờ đón tác phẩm tiếp theo của tác giả.
5
132514
2013-07-15 16:09:31
--------------------------
