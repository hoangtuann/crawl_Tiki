219914
9837
Nội dung sách trình bày những kỹ thuật cần thiết khi làm về tiếp thị trực tuyến như: SEO (tối ưu hóa công cụ tìm kiếm), tạo blog, tiếp thị qua thư điện tử,... Ngoài ra, sách còn cho bạn biết những sai lầm thường thấy và cần tránh trong lĩnh vực tiếp thị trực tuyến. Sách viết rất đầy đủ phần lý thuyết, có những ví dụ đi kèm rất dễ hiểu nhưng thiếu hình ảnh minh họa làm cho phần nội dung không sống động. Tuy nhiên, đây cũng là một quyển sách đáng tham khao khi nghiên cứu lĩnh vực tiếp thị trực tuyến.
4
356759
2015-07-02 08:10:48
--------------------------
