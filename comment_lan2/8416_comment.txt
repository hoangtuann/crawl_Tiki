435340
8416
Nối tiếp quyển tập một, tập hai của quyển sách trò chơi toán học lí thú tiếp tục là những câu đó toán học thú vị , tuy có ít hơn tập một, tập hai chỉ có 11 chủ đề nhưng các chủ đề đều rất hay và mỗi câu đó trong những chủ đề rất thú vị và số lượng câu đó trong mỗi chủ đề rất nhiều. Đọc xong hai cuốn sách này càng làm cho tình yêu và niềm say mê toán học của người đọc ngày một to lớn hơn. Tóm lại đã không quyết định sai khi mua cả hai tập của quyển sách này.
3
958015
2016-05-24 22:48:34
--------------------------
386345
8416
Sách có điều khiển hoạt động toán học của học sinh , giúp khai phá kiến thức tính toán vận dụng vào cuộc sống nên vừa học vừa chơi với phần thi thử tài đoán nhanh , được biên soạn bởi giáo viên nên có thể tin tưởng hơn , sách có 11 trò chơi nhớ lâu không những thế còn chứa đựng sự vui vẻ , sáng tạo , không có sự khó khăn mà có những trò rất đơn giản trong phân biệt khả năng của tầng lứa tuổi một , phân loại ngẫu nhiên và cố gắng 
4
402468
2016-02-25 15:21:40
--------------------------
333115
8416
Sách của thầy Lê Hải Châu rất hay, mỗi lần đọc xong một quyển làm mình có hứng thú và tình yêu, tìm tòi học hỏi toán học dù mình không giỏi lắm để hiểu hết sách của thầy, có những bài phải tìm tòi và hỏi rất lâu mới có đáp án nhưng chính vì thế mà trình độ của mình được cả thiện đáng kể, nhất là khả năng tư duy và suy nghĩ không còn bó buộc nữa. Tập 1 và tập 2 của quyển này mình đều mua và đã đọc. qua đó thấy được đúng như tên sách, đại số và giải tích toán học đúng là lý thú và thú vị, mỗi quyển đều có cái hay riêng và vẻ đẹp của toán học là điều không thể bàn cãi
4
495463
2015-11-07 13:47:00
--------------------------
288034
8416
Là phần hai của bộ sách " Trò chơi toán học lý thú " của nhà giáo nhân dân Lê Hải Châu sưu tầm và biên soạn. Tập một có 19 trò thì tập hai này chỉ có 11 trò. Tuy nhiên mỗi trò chơi trong đó lại có nhiều phần khác nhau cùng chung chủ đề nhưng nội dung khác nhau nên thật ra là rất nhiều trò chơi chứ không phải chỉ 11 trò. Những trò chơi được sưu tầm biên soạn tỉ mỉ, gây nhiều hứng thú cho người đọc. Bạn chỉ cần có tinh thần khám phá, thích giải câu đó là có thể đọc chúng chứ không cần đến khả năng toán học cao siêu. Nếu toán học là một môn khoa học khô khan thì những cuốn sách này làm cho nó bớt khô khan đi và đi vào cuộc sống.
4
504882
2015-09-03 10:58:04
--------------------------
195950
8416
Đây là một bộ sách có thể thay đổi hoàn toàn những định kiến, thành kiến xấu về  Toán học. Cùng với những câu chuyện tự nhiên, lý thú về nội dung lẫn hình thức, sách hứa hẹn sẽ rất hữu ích đối với những người yêu Toán, muốn tìm hiểu sâu sắc về bộ môn này. Vì vậy, sẽ bồi dưỡng đam mê nghiên cứu Toán học, tạo ra góc nhìn độc đáo, sáng tạo cuốn hút người đọc ngay từ cái nhìn đầu tiên. Đó là những cảm xúc riêng của mình, hy vọng các bạn sẽ tìm cho mình một cảm hứng nào đó trong và sau khi đọc hai cuốn sách này.
4
561261
2015-05-14 22:42:23
--------------------------
