483919
9472
Cuốn sách dễ khiến người đọc tò mò vì tên khá sốc "Quyển sách này sẽ cứu cuộc đời bạn". Chỉ vì ngẫu hứng thấy nó trong nhà sách và có một đợt giảm giá cực mạnh của tiki, tôi đã đặt mua và hoàn toàn hài lòng. Nếu cuộc sống của Richard ban đầu bề ngoài hào nhoáng bao nhiêu thì bên trong buồn tẻ bấy nhiêu. Nhưng một sự may mắn đã thay đổi nó, một bước ngoặt mà không phải ai cũng có được và rồi mở cánh cửa đã khép kín bấy lâu nay, Richard dần dần mở lòng và cuộc đời đã ngã sang một hướng khác tốt đẹp hơn. Cuốn sách chứa đầy giá trị nhân văn sâu sắc. Không có lời bình luận nào hơn của chính bạn khi bạn thưởng thức tác phẩm này. Có khi cuốn sách này sẽ thật sự "cứu cuộc đời bạn" cũng nên.
5
258584
2016-09-18 19:10:52
--------------------------
334130
9472
Cuộc sống của Richard hoàn hảo đến phát chán , ông giàu có, thành đạt , có một huấn luyện viên riêng , một nhân viên tư vấn dinh dưỡng và một người giúp việc . Suốt ngày chỉ tập thể dục tại nhà , ăn những món ăn tốt cho sức khoẻ và ngắm nhìn một cô gái bơi lội qua cửa sổ . Đó là trước khi ông gặp một cơn tai biến dẫn đến việc nhập viện và nhận ra mình cô đơn đến nhường nào , sau đó như một phép màu ông gặp Anhil - chủ một tiệm bánh donut . Cuộc đời ông bắt đầu thay đổi , ông quan tâm đến người khác nhiều hơn , về người phụ nữ xa lạ ở siêu thị , về Anhil, về con trai mình Ben và người vợ đã ly hôn, ...và thậm chí còn rơi vào cuộc phiêu lưu nhỏ thú vị cùng cậu diễn viên nỗi tiếng để cứu vật cưng của cô bé hàng xóm ... Dù cái kết không phải là một kết thúc hạnh phúc nhưng lại là một kết thúc đẹp khi ông cảm thấy mãn nguyện trong làn nước .
4
438372
2015-11-08 21:46:30
--------------------------
177747
9472
Trong 2 năm vừa qua mình liên tục gặp khó khăn trong cuộc sống,sự thay đổi quá nhanh và dồn dập tại môi trường làm việc,nhiều khủng hoảng và chuyện buồn gia đình đã khiến mình ngày trang khép kín.Mình đã rơi vào cảm giác cô đơn,lạc lõng ngay cả trong tổ chức của chính mình,xa lạ với mọi người xung quanh.
Tình cờ đi hội sách và chọn cuốn này,mình đang rất cố gắng vượt qua rào cản cảm xúc của chính mình.
Qua 139 trang sách đầu,mình thấy hình ảnh mình đang phản chiếu qua nhân vật Richard,mình cũng có những cơn đau đớn, dằn xé như thế
Mình chưa đọc hết quyển sách,nhưng mình hy vọng quyển sách sẽ cứu lấy cuộc đời mình.
5
341767
2015-04-03 19:45:49
--------------------------
173891
9472
Tôi như cảm nhận được về con người và cuộc sống của Richard Novak theo từng trang sách. Một cuộc sống đi từ sự tẻ nhạt và vô vị đến một bước ngoặt sống động và đầy cảm xúc. Những con người, những cái bánh donut, những cảm xúc vui buồn hờn giận giữa cha và con trai, những khoảnh khắc ngắn ngủi của hạnh phúc để bù đắp 13 lãng quên đi cuộc sống. 
Có lẽ điều khiến cho tôi không thể quên hay khuyên bạn nên đọc quyển sách này là câu nói của Richard Novak vào phút cuối:
"Bố ở đây, bố sẽ vẫn luôn ở đây, ngay cả khi con không thấy bố, bố vẫn ở đây."
5
182147
2015-03-27 03:55:49
--------------------------
162155
9472
Sau khi xem qua nội dung và bị hấp dẫn bởi nhận xét của mọi người,mình đã quyết định mua cuốn này,lúc đó mình cũng đang gặp 1 số vấn đề như nhân vật chính-Richard nên cũng hy vọng 'cuốn sách này sẽ cứu cuộc đời' mình . Nhưng sau khi đọc xong,mình đã rất hụt hẫng bởi cái kết,quá trình Richard hòa nhập lại với cuộc sống ,dù tác giả đã thêm vào nhiều tình huống 'không tưởng' nhưng vẫn kém hấp dẫn,và cuốn sách cũng không dễ đọc như mình nghĩ . Tuy nhiên vẫn có 1 điểm cộng rất lớn cho cuốn sách này đó là cách tạo dựng nhân vật,tất cả họ đều rất thú vị,sinh động,nội tâm sâu sắc.Mình yêu nhất là nhân vật Ben-con trai của Richard.Câu bé thật tuyệt vời !
Đó chỉ là ý kiến cá nhân của mình thôi,còn tùy vào quan điểm mỗi người nữa ^^ . Nhưng nói chung cuốn sách này cũng rất đáng có mặt ở tủ sách nhà bạn.
3
76459
2015-03-01 17:35:56
--------------------------
139234
9472
Cuốn sách rất hay, bổ ích và thực sự rất cần thiết với những ai đang đứng trên bờ chênh vênh của cuộc đời. Cuốn sách kể về những câu chuyện vượt qua khó khăn của tác giả, các tình huống tưởng chừng như không còn hy vọng nào nữa, ngàn cân treo sợi tóc,... Bằng những nỗ lực hết mình vượt qua khó khăn, thử thách, tác giả đã đúc kết những kinh nghiệm đạt được trong chặng đường dài của mình trong cuốn sách. Cuốn sách được viết với ngôn ngữ nhẹ nhàng, dễ hiểu, xen lẫn văn phong hài hước và lối dẫn dắt thuyết phục, đi vào lòng người. Cuốn sách thực sự đã chạm đến những mảnh đời khó khăn nhất, khiến họ nhận thấy được sự đồng cảm và ý chí vươn lên trong cuộc sống. Cuốn sách có tính thực tế cao. Chắc chắn sẽ không làm mọi người thất vọng.
5
414919
2014-12-06 15:05:40
--------------------------
85276
9472
Cuốn sách không "giật gân" hay thực sự gây hồi hộp, cuốn hút mãnh liệt mà trái lại, vô cùng nhẹ nhàng, bình yên và ấm áp. Kể cả những biến cố liên tục xảy đến làm thay đổi cuộc đời Richard cũng được kể với giọng điệu bình thản đến bất ngờ, cứ như là không có gì xảy ra.Quả thật cuốn sách này sẽ cứu cuộc đời của ai đó, chỉ cần bạn thy đổi thái độ thì qua từng ngày, cuộc đời bạn sẽ thú vị và đáng sống hơn rất nhiều, đó là một trong những thông điệp cuốn sách muốn độc giả cảm nhận được. Từng trang của cuốn sách như những chiếc bánh donut mà tác giả dành tặng người đọc. Bìa sách thiết kế đơn giản mà thu hút, nội dung lại thực sự sâu sắc, tôi đã không hối hận khi bỏ ra số tiền khá lớn so với 1 cuốn sách thông thường để đem tác phẩm tuyệt vời này về nhà.
5
63189
2013-07-04 18:36:38
--------------------------
83041
9472
Đây là một cuốn sách dễ đọc và thật khó để đặt xuống một khi đã "dính" vào nó. Câu chuyện nói về Richard - một doanh nhân thành đạt trong công việc nhưng cô đơn trong cuộc sống riêng tại Los Angeles muốn làm cái gì đó để thay đổi cuộc đời mình. Người đọc sẽ không cảm thấy một sự tiếc nuối rõ ràng nhưng thật sự bị cuốn hút một cách phức tạp vào quãng đời trung niên kỳ lạ của người đàn ông này. Sau một lần tình cờ gặp chủ tiệm bánh rán Anhil với sự thông thái và tính cách thú vị, Richard bắt đầu để ý kết bạn với những người cũng đang cần bạn như mình suốt dọc đường đi.

Những con người ông tình cờ gặp trên đường được mô tả sinh động và thú vị đến mức bạn sẽ muốn kết bạn với họ ngay lập tức. Cuốn sách còn dành nhiều đoạn xen kẽ để đi sâu vào mối quan hệ giữa Richard với người thân của mình để từ đó cho thấy biến chuyển của mối quan hệ đó sau khi ông gặp những người bạn mới.

Cuối cùng, cuốn sách đưa đến một thông điệp hết sức giản dị nhưng sâu sắc "Con người đều là những cá nhân luôn cần có bạn bè và cần được yêu thương, không quan trọng ta đến từ đâu hay giàu đến thế nào, tất cả đều giống nhau ở nhu cầu đó".
5
63189
2013-06-24 23:46:21
--------------------------
73755
9472
Cuốn sách này lạ và độc đáo ngay từ cái tiêu đề. Chính vì thế nó được in rất to, rõ ràng và chiếm hết trang bìa để độc giả buộc phải chú ý. Đây không phải một cuốn sách kỹ năng mà là một câu chuyện cực kỳ lôi cuốn, rất hấp dẫn, nhẹ nhàng, tinh tế. Những bài học triết lý sống không hề đao to búa lớn, được lồng ghép trong những tình huống, cảm xúc rất riêng khiến người đọc cũng phải suy ngẫm và tự tìm hiểu ở bản thân. Rechard từ một người tin rằng mình đã có tất cả, nhưng sự khép kín đã vây lấy anh khiến cuộc sống của anh nhạt nhòa vô vị chẳng có màu sắc nổi bật. Nhưng câu chuyện Richard trở thành bạn của Anhil - chủ nhân của một tiệm bánh donut như một ngã rẽ trong cuộc đời anh, giúp anh chợt nhận ra thế giới này không hề vô vị như mình nghĩ, mà nó phụ thuộc rất nhiều vào cách quan sát nhìn nhận vấn đề. Tình yêu, tinh thần, cảm xúc chính là những chất xúc tác khiến cuộc sống trở nên thi vị và tuyệt vời hơn,
4
29716
2013-05-09 21:11:06
--------------------------
63598
9472
Richard tưởng như đã có được mọi thứ mình muốn: tài sản, địa vị, sức khỏe,...Nhưng cuộc sống của ông hết sức buồn tẻ, nó là một chu trình lặp đi lặp lại đến chán chường. Vào một tối nọ, bước ngoặt xảy đến trong cuộc đời ông - ông bị đau dạ dày phải vào bệnh viện. Việc này dẫn đến cuộc gặp gỡ của ông và Anhil - chủ của hàng bán bánh donut. Có thể nói những chiếc bánh thơm ngon ấy là chìa khóa dẫn dắt Richard vào cuộc hành trình tìm lại những thứ ông đã đánh mất khi tự tách mình ra khỏi đời sống xã hội. Quyển sách là tập hợp những "chuyện trên trời" có thể xảy ra với một người bình thường, nhưng cách hành văn của tác giả khiến những "chuyện trên trời" ấy diễn ra vô cùng hợp lí (và có phần hài hước nữa). Mang tính triết lí sâu sắc nhưng không phải là những áng văn lên giọng dạy đời, tác giả A.M.Homes chỉ "kể", kể một cách chân thực và đời thường nhất có thể để độc giả tự ngẫm ra những gì cô muốn truyền tải. Đọc xong quyển sách thì mẩu đối thoại giữa Nick và Ben vẫn để lại những dư vị ngọt ngào trong lòng mình: " Bố ở đây, bố vẫn luôn ở đây, ngay cả khi con không thấy bố, bố vẫn ở đây."
Nếu quyển sách không cứu cuộc đời bạn, thì ít nhất nó đã cứu cuộc đời Richard Novak. Mà nó đã cứu ai, dù chỉ một người, thì cũng đáng đọc đúng không bạn? :)
4
11661
2013-03-16 19:12:21
--------------------------
52978
9472
Một quyển sách nhẹ nhàng và ngọt ngào như những chiếc donut tươi mới, thơm ngon.

Một người đàn ông trung niên, tự thu mình vào thế giới riêng với các công việc ko tên hàng ngày: thức dậy, ngắm một nàng thơ đi bơi hàng sáng, thể dục, đọc báo, kiểm tra tài khoản chứng khoán, ăn những món hữu cơ chán ngắt nhưng lại tốt cho sức khỏe,…

Một cơn đau, là nỗi đau thể xác, là cơn đau tinh thần hay là gì khác nữa không ai biết, đã mở ra những gặp gỡ tình cờ. Gặp gỡ, ko chỉ là anh chàng hiệu bánh, mà còn một bà nội trợ bế tắc, là gặp lại con trai, là trò chuyện với vợ cũ, bắt lại liên lạc với em trai và cha mẹ già, là thực sự trò chuyện với bà giúp việc lâu năm, là một hố sụt đẩy ông ra khỏi căn nhà buồn tẻ, rồi chuyển đến bờ biển, kết bạn cùng lão già nhà bên và hàng đống hành động có thể bị đánh giá là bốc đồng ở cái lứa tuổi ấy.

Ông đã rơi nước mắt cho bản thân, khóc cùng nỗi cô đơn của cậu con trai và nhẹ nhàng kiếm tìm hạnh phúc nơi người phụ nữ ông yêu nhưng không tranh đấu tới cùng trong quá khứ để có thể duy trì hạnh phúc cùng bà. Mỗi một hành động đều giúp mở ra cánh cửa mới trong cuộc đời người đàn ông này. Một cơ hội tìm về những lỗi lầm trong quá khứ, đối diện với thực tại và nắm bắt những cơ hội trong tương lai!

Mỗi một con người trong cuộc sống này rồi sẽ đối diện với khoảnh khắc đó, cái khoảnh khắc mà ông gọi là NÓ. Nhưng NÓ chính xác là gì, là cái chết, là một khởi đầu mới hay đơn thuần NÓ chỉ là nó? Độc giả hãy tự tìm câu trả lời cho chính bản thân mình khi lật giở từng trang sách nhỏ!
5
6576
2012-12-29 16:31:40
--------------------------
51566
9472
Phải nhấn mạnh đây không phải là một quyển sách triết lí, càng không phải là quyển sách chuyên cung cấp cho người đọc những yếu quyết sống. Đây đơn thuần chỉ là một quyển truyện gần như là một dạng tiểu thuyết,  Có điều quyển sách này mang đến cho người đọc một triết lí sống rất đơn giản, nhẹ nhàng mà tinh tế theo từng đợt sóng cảm xúc mãnh liệt. Thông qua quá trình chuyển đổi từ cuộc sống nội tâm khép kín có phần tự ti thành một người lạc quan hơn, cở mở hơn của Rich, ta phải thốt lên rằng "cuộc sống này quả có rất nhiều điều bắt ngờ". Thật vậy ta không thế biết được những bất ngờ nào sẽ đến trong cuộc đời mình, sẽ thay đổi bản thân ra sao. Từng bước từng bước, quyển sách dẩn cởi bỏ mọi rào cản trong cuộc sống thường nhật, đưa ta đến gần hơn với lăng kính thường nhật đa sắc màu.
Cảm ơn Homes đã mang đến cho độc giả một món ăn tinh thần đầy màu sắc, nhẹ nhàng nhưng lại nhiều ý vị.
4
41922
2012-12-21 17:24:07
--------------------------
49623
9472
Cuốn sách này chứa đựng một câu chuyện rất hấp dẫn, với sự nhẹ nhàng tinh tế và cả những cảm xúc mãnh liệt. Quá trình mà Richard chuyển từ một người sống khép kín sang một người yêu đời và hòa đồng, và gặp được Anhil, được miêu tả một cách chi tiết, sống động đến bất ngờ. Đúng là trong cuộc sống chẳng ai biết trước điều gì sẽ xảy đến, cũng như sẽ có những thay đổi nào. Richard thực sự may mắn khi gặp được Anhil, người sẽ làm cuộc đời ông trở nên ngọt ngào hơn, nhiều màu sắc hơn. Cuốn sách giống như một bô phim tâm lý, với những cái nhìn đa chiều và mới mẻ, chắc chắn sẽ làm bạn không thể rời mắt.
Đặc biệt là tên cuốn sách sẽ gây sự tò mò cho độc giả, buộc họ phải cầm lên và hào hức muốn thưởng thức cuốn truyện này. "Quyển sách này sẽ cứu cuộc đời bạn " rất hay 
5
79219
2012-12-08 20:17:29
--------------------------
47682
9472
Cuốn sách này chứa đựng một câu chuyện rất hấp dẫn, với sự nhẹ nhàng tinh tế và cả những cảm xúc mãnh liệt. Quá trình mà Richard chuyển từ một người sống khép kín sang một người yêu đời và hòa đồng, và gặp được Anhil, được miêu tả một cách chi tiết, sống động đến bất ngờ. Đúng là trong cuộc sống chẳng ai biết trước điều gì sẽ xảy đến, cũng như sẽ có những thay đổi nào. Richard thực sự may mắn khi gặp được Anhil, người sẽ làm cuộc đời ông trở nên ngọt ngào hơn, nhiều màu sắc hơn. Cuốn sách giống như một bô phim tâm lý, với những cái nhìn đa chiều và mới mẻ, chắc chắn sẽ làm bạn không thể rời mắt.
Đặc biệt là tên cuốn sách sẽ gây sự tò mò cho độc giả, buộc họ phải cầm lên và hào hức muốn thưởng thức cuốn truyện này.
4
20073
2012-11-25 08:39:43
--------------------------
