402183
9203
Cuốn Phương Pháp Học Tốt Hơn Trường Học Tốt của giáo sư Thư Đức Sơ, chuyên gia giáo dục Việt Nam. Những tư tưởng của tác giả thật sự khoa học, kịp với xu hướng giáo dục hiện tại, tuy nhiên nhiều bố mẹ còn bận nhiều việc mà không biết được cách giáo dục trẻ của mình có quá nhiều điều sai trái. Cuốn sách được bọc trong nilon mỏng để bảo quản cho tốt, giấy sách xốp mềm và nhẹ, bìa sách bắt mắt, màu hồng với icon trẻ em tinh nghịch. Sách có rất nhiều điều hay ho để áp dụng tỏng cuộc sống gia đình. Tuy nhiên có một vài quan điểm tôi có lẽ tôi sẽ không áp dụng.

5
854660
2016-03-21 21:41:10
--------------------------
397386
9203
Khi mua cuốn sách này mình mong nội dung là cách chỉ dạy cách học tốt và nhanh trong thời đại hiện nay. Cuốn sách mang lại nhiều điều hơn mình nghĩ, nó luôn đặt tâm lí học sinh hàng đầu và tâm sự với ông bố bà mẹ cách dạy con chứ không phải cho con đi học ở đâu, thế nào. Bằng trải nghiệm của chính tác giả và con gái của mình, tác giả đã cho ra cuốn sách khá thuyết phục với nhiều chứng cớ chặt chẽ có thật. Sau khi đọc cuốn sách mình có ý định thay đổi lại cách học tập, học theo năng lực và tự tìm hiểu chứ chẳng thể nhờ và bố mẹ hay thầy cô. Trước đây mình ngại mua những cuốn sách học tốt vì sợ sẽ bị nói là học sách giải nhưng những cuốn sách học tốt đó sẽ bổ sung cho mình kiến thức vững hơn. Tốt nhất là nên làm xong hãy đọc sách học tốt hoặc tham khảo cách trình bày và dẫn dắt trong sách.
5
1145129
2016-03-14 21:26:47
--------------------------
309560
9203
Đây là một câu chuyện thực tế cuả một người cha đã cố gắng tìm ra phương pháp riêng để nuôi dạy con gái của mình. Chính vì là kinh nghiệm thực tế của tác giả nên phương pháp đó càng gần gũi và dễ áp dụng cho người đọc. Mặc dù không tham vọng con cái mình sẽ trở nên tài giỏi như cô con gái của tác giả nhưng ít nhất mình không còn chán nản với cách dạy và học ngày nay. Phương pháp thì như vậy nhưng sự thích ứng của mỗi trẻ mỗi khác, sự kiên trì và nỗ lực của cha mẹ mỗi khác cho nên ứng dụng vào rồi đừng nói sao sách viết vậy, tôi thấy hay đấy nhưng sao con tôi áp dụng lại thế này. Hãy đọc và rút ra cho mình những gì phù hợp với mình với chính con mình.
4
43129
2015-09-19 00:43:20
--------------------------
253658
9203
Xã hội ngày càng phát triển, có nhiều ông bố bà mẹ tin tưởng tuyệt đối vào các ngôi trường nổi tiếng , họ cứ nghĩ rằng nếu con cái họ được học những trường  danh tiếng, thì con họ chắc chắn sẽ thành công . Đó là quan niệm sai lầm, theo tác giả chúng ta nên có phương pháp dạy con đúng đắn, bồi dưỡng phẩm chất đạo đuc, năng lực và thói quen cho con. Tác giả từ kinh nghiệm trong việc dạy cô con gái từ học sinh yếu kém,chán học đã trở thành 1 học sinh xuất sắc, 12 tuổi đã thi đỗ Đại học,sau đó dỗ thạc sĩ, tiến sĩ, đó là nhờ tác giả đã đưa ra các phương pháp học tốt, giúp con yêu thích việc học và trở thành học sinh xuất sắc .
4
512701
2015-08-04 16:46:35
--------------------------
239906
9203
Khi đọc cuốn sách mình thấy rất ngưỡng mộ tác giả, không những có một phương pháp dạy học đặc biệt phù hợp với mọi học sinh mà trên hết là tình cảm của người cha dành cho cô con gái của mình. Khi biết con không muốn đi học Thư Đức Sơ đã tự dạy ở nhà, chứng minh cho mọi người thấy rằng học tập không khó, có thể vừa học vừa thư giãn, tạo ra niềm yêu thích với môn học, học không còn quá áp lực và mệt mỏi sẽ đạt được kết quả tốt nhất. Đây là điều có ý nghĩa thiết thực với các bậc phụ huynh cho con em mình.
5
7304
2015-07-24 10:05:12
--------------------------
192617
9203
tôi mua rất nhiều sách về vấn đề giáo dục con cái nhưng đây có lẽ là một trong hiếm những cuốn sách tôi ưng ý nhất. Lối viết giản dị với những câu chuyện rất thực tế về một phương pháp giáo dục rất mới làm tôi cảm thấy rất thú vị khi đọc. Nền giáo dục của Việt Nam hiện nay có nhiều nét khá giống với Trung Quốc theo miêu tả của tác giả nên những bất cập của bản thân tác giả cũng chính là bất cập của chính chúng ta. Tôi đã đọc một hơi hết cả cuốn sách. Tôi nghĩ các bà mẹ có con đang học tiểu học hay có thể là lớn hơn nữa cũng nên có một cuốn như thế này trong tủ sách của gia đình.
5
600200
2015-05-05 10:04:07
--------------------------
192616
9203
tôi mua rất nhiều sách về vấn đề giáo dục con cái nhưng đây có lẽ là một trong hiếm những cuốn sách tôi ưng ý nhất. Lối viết giản dị với những câu chuyện rất thực tế về một phương pháp giáo dục rất mới làm tôi cảm thấy rất thú vị khi đọc. Nền giáo dục của Việt Nam hiện nay có nhiều nét khá giống với Trung Quốc theo miêu tả của tác giả nên những bất cập của bản thân tác giả cũng chính là bất cập của chính chúng ta. Tôi đã đọc một hơi hết cả cuốn sách. Tôi nghĩ các bà mẹ có con đang học tiểu học hay có thể là lớn hơn nữa cũng nên có một cuốn như thế này trong tủ sách của gia đình.
5
600200
2015-05-05 10:04:00
--------------------------
90717
9203
Cảm giác của tôi khi đọc xong Phương Pháp Học Tốt Hơn Trường Học Tốt giống như khi đọc cuốn Lưu Diệc Đình- Cô gái Harvard, đều được viết từ chính tình cảm và trải nghiệm của những ông bố bà mẹ yêu thương con hết mực, thầy Thư Đức Sơ cũng như bà Lưu Vệ Hoa xứng đáng là những Dale Carnegie của Trung Quốc về phương pháp giáo dục thời hiện đại. Quyển sách đề cập đến vấn đề tự học, về phương pháp giáo dục " Liên kết hỗ trợ Tự học" để vấn đề học tập nan giải trở nên đễ dàng và hứng thủ. Phương pháp giúp con gái tác giả hoàn thành chương trình học 8 năm chỉ trong 2 năm và 12 tuổi đỗ đại học từ một học sinh chán nản, đang tuột dốc nhanh chóng và bị thầy cô giáo và bạn bè gọi là ngu ngốc. Sau sơ đồ tư duy, trí nhớ siêu đăng, các phương pháp ghi chép... thì đây là một cuốn sách nhằm nâng các phương pháp đó lên một tầng cao mới.
“Người thầy thông minh sẽ không bắt bạn bước vào cung điện trí tuệ của thầy, mà hướng dẫn bạn mở ra cánh cửa trí tuệ của riêng mình.”

5
72723
2013-08-12 21:22:44
--------------------------
