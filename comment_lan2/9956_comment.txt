311710
9956
Truyện rất hay và gần gũi. Hơn nữa đúng như mục đích quan trọng của mình nó giúp mình cải thiện khả năng tiếng anh. Vừa đọc truyện vừa nghe băng thật sự là cách học tiếng anh hay, hiệu quả mà lại không hề nhàm chán. Nó giúp mình cải thiện vốn từ vựng, khả năng nghe nói và nhớ từ một cách thoải mái, đơn giản mà chẳng phải học thuộc hay chép phạt để nhớ. 1 cảm giác thú vị khi học anh văn và khiến mình thích thú học tiếng anh hơn. Bạn chỉ có thể học được một ngôn ngữ khi bạn thoải mái tìm hiểu nó chứ không phải gò bó ép mình học mà.
4
96598
2015-09-20 01:07:39
--------------------------
244873
9956
Cuốn truyện viết về một người phụ nữ và đứa con nhỏ.Cô bị nghi ngờ về chuyện tình cảm của mình,nhưng cuối cùng cô đã tìm được một cuộc sống hạnh phúc của mình.Câu truyện khá là thú vị,với giọng kể của nhà văn  Nathaniel Hawthorne ,chắc chắn sẽ mang cho bạn một cảm giác thú vị.Hơn nữa,cuốn truyện còn liệt kê sẵn các từ mới và và việc sử dụng băng cùng với giọng đọc rất tuyệt vời sẽ cải thiện khả năng nghe nói và giúp bạn nhớ từ nhiều hơn.Cuốn sách chỉ với 41000đ nhưng sẽ giúp bạn rất nhiều trong viejc học tiềng anh của mình
5
395184
2015-07-28 15:44:32
--------------------------
