343955
9102
Tôi biết cuốn sách này từ cô giáo của tôi, cô Phạm Thị Thúy,cô là tác giả của cuốn sách Kỹ Năng làm cha mẹ , Thai giáo, phương pháp giảng dạy sư phạm. Và có một sự thực cần thú nhận là bộ ba cuốn sách này luôn được tôi mua để tặng quà cho bạn bè trong ngày đám cưới, quà tốt nghiệp khi bạn tôi ra trường.
Tôi mua nó không hẳn vì đó là cuốn sách cô giáo tôi viết, mà ba cuốn sách ấy thực sự ý nghĩa cần thiết cho những người bạn của tôi. 
Mỗi khi đi đám cưới bạn bè thân tôi không mang theo tiền bỏ vào phong bì, tôi mua sách và tặng bạn ấy, và tôi vẫn nhớ những cái ôm nghẹn ngào của bạn khi được nhận cuốn sách này.
Tôi nghĩ là nếu bạn cần mua một cuốn sách để tặng làm quà đám cưới cho người bạn thực quý mến hãy chọn cuốn sách này.
ai bước vào một đời hôn nhân chẳng khát khao ước mong làm những người ba mẹ tuyệt vời.
5
339993
2015-11-27 14:58:44
--------------------------
270670
9102
Quyển sách tập hợp các kỹ năng dành cho các bậc phụ huynh có con giai đoạn từ 0 đến 6 tuổi. Với những chia sẻ của quyển sách, chúng ta sẽ dạy con biết yêu thương,  biết sống, dạy con biết tự bảo vệ mình. Đồng thời chúng ta sẽ biết được vai trò của cha mẹ trong việc giáo dục con, cách xử lý khi con ương bướng hay chia sẻ với con về giới tính, về các kỹ năng trong cuộc sống... Quyển sách rất hay và bổ ích dành cho các bậc cha mẹ muốn làm bạn cùng con. Cám ơn tiki.
5
110777
2015-08-18 14:48:36
--------------------------
70880
9102
Con cái là món quà kì diệu nhất mà cha mẹ được ban tặng. Những tưởng làm cha làm mẹ là công việc bản năng nhưng thực tế lại rất nhiều rắc rối và khó khăn, nhất là trong thời đại công nghiệp hóa ngày nay. Cuốn sách cung cấp những kĩ năng hữu ích để cho cuộc sống gia đình trở nên trọn vẹn hơn rất nhiều. Điều cha mẹ mong muốn cho đứa con hơn cả những gì tốt đẹp nhất con có thể tưởng tượng được. Bởi lòng cha mẹ bao la như biển Thái Bình, mênh mông như những địa dương bao la nên dù là ở lứa tuổi nào cha mẹ vẫn luôn luôn quan tâm chăm sóc đặc biệt ở lứa tuổi mới chào đời từ 0-6 tuổi. Thật là tác phẩm đặc sắc và hữu ích dành cho những bậc cha mẹ
5
14072
2013-04-23 21:09:34
--------------------------
