470007
8764
Cuốn sách được in với khổ rộng, khá to, giấy tốt, chữ rõ ràng. Sách gồm 4 kĩ năng nghe, nói , đọc, viết nhưng thật tiếc là không kèm theo CD, mình phải tự lên mạng và tải về. Có kèm đáp án để tự kiểm tra lại. Ngoài ra còn có các test mẫu bao gồm học thuật và phổ thông rất hay, sau đó là phần giải thích cặn kẽ chi tiết. Cuối cùng là các audioscript của các phần nghe, mình có thể tham khảo để hiểu thêm nội dung của các đoạn hoại thoại. Nhìn chung đây là cuốn sách luyện IELTS khá tuyệt vời với mình.
4
635412
2016-07-07 08:02:50
--------------------------
278349
8764
Mình rất thích các quyển sách của Barron's, quyển nào cũng rất hay và trình bày chi tiết, dễ hiểu, rất thích hợp cho các bạn tự học ở nhà. Riêng với quyển Barron's IELTS International English này thì nổi bật hơn các quyển khác, nó giúp phát triển cả bốn kĩ năng nghe, nói, đọc, viết. Phải nói đây một cuốn sách hoàn hảo. Nhờ cuốn sách này mà mình đã thích học tiếng Anh hơn, việc học tiếng Anh trở nên thoải mái và đơn giản hơn, đặc biệt là không còn quá sợ kì thi IELTS như trước kia nữa.
4
192644
2015-08-25 20:58:44
--------------------------
