427986
9852
Hay và thật hữu ích, đó là cảm nhận cuối cùng sau khi đọc xong hết cuốn sách. 
Tuổi mới lớn, hầu hết đều có chung tính tò mò, thích khám phá, tìm hiểu nhưng với tâm sinh lý tuổi dậy thì lại rất hay ngại ngùng, không dám thổ lộ hay chia sẻ với những người có kinh nghiệm giới tính hơn như cha mẹ, người thân. Và cũng không ít bậc cha mẹ mỗi khi trò chuyện với con cái về vấn đề nhạy cảm trên thì lại rất e dè, hoặc tránh né, hoặc trả lời 1 cách qua quýt không thỏa đáng khiến con cái họ cũng chẳng dám hỏi han mặc dù trong lòng mang rất nhiều thắc mắc. Vì vậy cuốn sách này cũng như rất nhiều cuốn sách bổ ích khách của First News như là cầu nối giải đáp nhiều thắc mắc, nhiều câu hỏi mà các bậc phụ huynh bỏ ngỏ vì quá ngại ngùng, hoặc chăng có quá ít thời gian chuyện trò cùng con cái, cũng như giải đáp phần nào trí tò mò của con trẻ cũng như nâng thêm phần hiểu biết giới tính làm hành trang bước vào đời của giới trẻ. 
Một cuốn sách hay, đáng được đưa lên kệ vào bộ sưu tập tủ sách gia đình và để dành tặng con sau này của mình. Thật đấy!
4
929376
2016-05-10 13:32:42
--------------------------
332586
9852
thật tình thì mình đã định mua quyển sách nay 5 tháng trước đây trong một lần ghé TiKI xem sách . thật may lần này trong list được giảm, 10k chứ 50k vẫn xúng .
 Giới trẻ việt nam đang trên con đường hội nhập và nhiều bạn trẻ chỉ biết đối đầu và không đồng ý với suy nghĩ không thông thoáng của cha mẹ, mà quên đi mất chính bản thân mình cần trang bị đủ kiến thức, hành trang trên con đường hội nhập. Quyển sách này có những kiến thức căn bản nhất để bạn trẻ và đặc biệt là bạn nữ bảo vệ chính mình trước những lối sống mở trong thời buổi hội nhập. 
Bạn trẻ và bạn cần bản lĩnh, cần hơn nữa là biết tự bảo vệ mình, mau và đọc nó đi các bạn gái. Mình làm về y tế dự phòng mà còn mù mờ, đọc để biết và khi có kiến thức mới nói đến tự chịu trách nhiệm cho cuộc đời mình, và làm chủ vận mệnh mình, bạn nữ ah. Thân 
5
170788
2015-11-06 16:59:09
--------------------------
330778
9852
   Trên lớp học của mình bạn cùng lớp có quyển này,thấy tiêu đề hay hay nên mình mượn về đọc thử và thấy rất thí vị có nhiều chia sẻ giải đáp những điểu mình băn khoăn mà mình ngại không chia sẻ với mẹ. Tác giả viết nội dung dễ hiểu,hình thức trình bày đẹp mắt thu hút người đọc...Cám ơn tác giả nhiều
  Đây là một cuốn sách bổ ích nên mình muốn sở hữu nó để lấy đó làm hành trang giúp mình tự tin hơn khi bước vào cuộc sống để mình luôn vui vẻ với độ tuổi 20
5
890085
2015-11-03 11:19:52
--------------------------
236785
9852
Là một người mẹ của hai cô con gái, tôi nghĩ mình có đầy đủ kiến thức về tâm sinh lý tuổi dậy thì để hướng dẫn cho các con, thế nhưng có những vấn đề lại không hề dễ dàng để trao đổi với con nhất là đề tài giới tính và tình dục, vì thế tôi đã mua tặng con quyển sách này để con tìm hiểu. Đọc qua quyển sách tôi thấy cách viết của tác giả rất rõ ràng, dễ hiểu, nội dung phong phú, cung cấp nhiều kiến thức bổ ích, thiết thực, bìa sách đẹp, hình ảnh minh hoạ được in màu rõ ràng, dễ thương, giá sách cũng không đắt lắm.
5
664812
2015-07-21 23:58:50
--------------------------
197953
9852
*Điều bạn chắc chắn sẽ tìm được ở quyển sách này đó là kiến thức về tình dục. Tình dục của giới trẻ luôn là vấn đề nhức nhối vì không phải ai cũng am hiểu nó và cũng bởi vì đơn giản chúng ta được dạy đó là vấn đề của người lớn và khi lớn chúng ta sẽ biết nhưng đâu phải ai cũng muốn chờ đến độ tuổi lập gia đình mới biết tới “trái cấm”. Một số bạn trẻ thật sự đã “vượt rào” từ sớm nhưng vẫn chưa hiểu rõ và biết tự bảo vệ sức khỏe cho cá nhân và bạn tình để rồi dẫn đến hậu quả đáng tiếc.
*Đối tượng tác giả nhắm tới là các bạn nữ ở độ tuổi vị thành niên vì có lẽ nữ giới nếu không biết chăm sóc bản thân tốt khi quan hệ tình dục sẽ dẫn đến những hậu quả nghiêm trọng.
*Mình nghĩ đây là quyển sách mà các bạn nữ nên đọc để biết cách chăm sóc tốt cho chính mình hơn. Vả lại đây là quyển sách màu khá đáng yêu và bổ ích.
5
45667
2015-05-18 20:40:41
--------------------------
