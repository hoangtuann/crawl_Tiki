326656
8439
Tựa sách cứ ngỡ như là một cuộc giằng xé nội tâm giữa cái suy nghĩ truyền thống đậm nét á đông, phụ nữ phải hy sinh bản thân vì gia đình vì chồng con và suy nghĩ hiện đại tân tiến bât chấp mọi giá để tìm hạnh phúc kể cả là ngoại tình. Nhưng không vậy, nội dung sách chỉ xoay quanh lối sống, suy nghĩ của một cô gái trẻ vừa đủ độ chính chắn nhưng lại rất biết tăng hương vị cho cuộc sống qua những mối tình, những quần là áo lượt, những địa điểm ăn chơi đàn đúm và quan trọng nhất là vô cùng tự tin về vẻ đẹp bản thân, sức quến rũ chết người toát ra tự thân người phụ nữ. Chát lượng giấy cũng tạm ổn, tuy nhiên sách có đôi chỗ sai chính tả.
4
387183
2015-10-26 11:54:19
--------------------------
141921
8439
Quyển sách này đã gây ấn tượng mạnh với tôi ngay từ khi đọc cái tên bìa sách. Một phần nào đó nội dung câu chuyện cũng được thể hiện ra cho người đọc . Đọc xong quyển sách này tác giả đã khiến tôi suy ngẫm rất nhiều . Thế nào mới gọi là tình yêu thực sự ? Nữ chính trong truyện là một cô gái mạnh mẽ kiên cường nhưng khi bị tổn thương cô cũng rơi nước mắt như những cô gái bình thường khác . Tình yêu cô cần là gì ? Cô không cần đối phương luôn bảo vệ mình cô chỉ cần cùng anh nắm tay chống lại mọi khó khăn là được rồi . Một cuốn sách ý nghĩa khiến độc giả phải suy ngẫm nhiều
4
337423
2014-12-17 07:41:40
--------------------------
