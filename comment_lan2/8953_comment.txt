457362
8953
Giữa một rừng các tác phẩm Sci-fi dành cho teen hiện nay, The Maze Runner nổi lên như một bộ truyện đáng đọc nhất, tác giả xây dựng câu chuyện với một bối cảnh khác biệt, tuyến nhân vật đa dạng và đề cao tính sinh tồn, yếu tố tình cảm sến súa được giảm đi, rất hợp sở thích của mình! Tập đầu bộ sách đưa người đọc tới thế giới giả tưởng tương lai, nơi những người trẻ bị nhốt cùng nhau trong một khu gọi là Trảng, bao quanh là 4 bức tường cao hàng chục mét, họ phải tìm cách sinh tồn và thoát khỏi nơi đáng sợ này. Nhịp truyện khá nhanh, các tình tiết bất ngờ xuất hiện liên tục làm bạn khó lòng dời mắt khỏi trang sách... 
Tuy hài lòng về nội dung nhưng mình rất không thích giấy xốp dùng cho bộ này, sách dày dặn và nhẹ nhưng dễ ố, khó bảo quản trong điều kiện nóng ẩm ở VN.
3
112948
2016-06-24 10:28:20
--------------------------
452780
8953
Mình biết đến bộ tiểu thuyết Giải mã mê cung này qua bộ phim cùng tên. Nội dung sách rất hay, tiết tấu nhanh. Mình rất thích kiểu truyện sci-fi như thế này, cũng giống như Đấu trường sinh tử hay Divergent. Tuy nhiên, màng bọc của sách tệ quá, lúc mới mua về đã muốn bong ra rồi. Thiết kế bìa sách khá đẹp, chất lượng in và dịch cũng rất tốt. Mình đọc xong phần 1 chỉ trong vòng 2 ngày. Cuốn sách rất hay, phù hợp với mọi lứa tuổi. Bộ phim không thể diễn tả hết nội dung và cảm xúc như khi đọc sách được.
4
361461
2016-06-20 20:30:24
--------------------------
446483
8953
Mình chưa đọc hết bộ này, nhưng đã từng xem phim. Nếu xem phim bạn cực kì hồi hp, không biết những đứa trẻ có thoát ra khỏi được mê cung hay không thì luc đọc truyện, bạn lại đứng ở từng trang sách, nghiền ngẫm cảm xúc của nhân vật. Và mình khuyên là nếu tài chính cho phép thì bạn nên mua combo 3 quyển, vừa tiết kiệm vừa không làm đứt mạch cảm xúc. Bìa của sách khá đẹp tuy nhiên mình không thích lớp nilon bao ngoài bìa (không phải bọc plastic của tiki mà chỉ giống như một lớp nhựa mỏng cân trên bìa sách) lớp này rất dễ tróc, khi trọc còn làm mất màu sạch, làm mất thẩm mỹ của cả cuốn sách
5
492342
2016-06-12 11:35:47
--------------------------
427033
8953
Thực ra mình là fan của phim GMMC nên mình mới mò đi mua bộ này.Ban đầu mình rất lười đọc vì đã xem qua phim rồi.Nhưng sau đủ tháng ngày chây lười, rốt cuộc mình cũng có động lực để lật ra những trang đầu tiên.
Truyện được kể dưới góc nhìn của Thomas, riêng mình thì không thích cách dịch "nó" lắm, nhưng thôi kệ.Nếu để so sánh thì cũng giống như những tín đồ Harry Potter thích đọc truyện hơn là xem phim vậy, những cuốn sách bao giờ cũng đem thế giới của câu chuyện bay xa hơn những gì phim ảnh đem lại.Mặc dù bạn có thể dùng những khuôn mặt trong phim để gán cho những nhân vật, nhưng thế giới thực thụ mà GMMC-bản sách mang lại khốc liệt và đau thương hơn nhiều.Kiểu như, ban đầu bạn sẽ thấy ở Trảng vẫn có những con người cố gắng cùng nhau vượt qua khó khăn, vẫn còn le lói chút hi vọng rồi mọi chuyện sẽ ổn thôi.Nhưng kì thực, bạn vẫn sẽ biết thực ra mọi chuyện đang đi xuống, đang héo mòn dần và lúc đó, những tình tiết dồn dập, mất mát cứ thế diễn ra.
4
104257
2016-05-08 09:52:49
--------------------------
420549
8953
Giải Mã Mê Cung
Cuốn sách khá dày nhưng khi cầm trên tay bạn sẽ cảm thấy rất dễ chịu. Vì thế mà tôi đã ráo riết đọc nó suốt những ngày gần nay.
Cuốn sách có mạch truyện chặt chẽ, cụ thể, không nói quá dài dòng khiến người đọc dễ hiểu và dễ dàng bám sát mạch truyện. Truyện có nhiều nhân vật nhưng mỗi người có nét riêng, không ai bị phai nhạt. Đây là tài năng thiên phú của James Dashner
Với những đọc giả yêu các bộ tiểu thuyết viễn tưởng như tôi thì chắc chắn Giải Mã Mê Cung là 1 lựa chọn khó cưỡng lại.
5
1151345
2016-04-23 13:13:58
--------------------------
408596
8953
Jame Dashner là một nhà văn chuyên viết chuyện cho tuổi teen. Giải mã mê cung có lẽ là tác phẩm thành công nhất của ông. Câu chuyện xoay quanh về một nhóm thanh niên bị nhốt trong hộp với một kí ức không rõ ràng. Và từ đó họ phải tìm mọi cách để thoát khỏi mê cung khổng lồ đó với đầy những sinh vật kì dị và nguy hiểm. Câu chuyện giả tưởng mang đầy kịch tính từ đầu đến cuối khiến cho người đọc không thể rời mắt khỏi quyển sách.
Đây là quyển sách đáng mua dành cho các bạn trẻ thích thể loại giả tưởng hành động.
5
316030
2016-03-31 22:16:40
--------------------------
401581
8953
Bộ truyện "The Maze Runner" dành cho những bạn nào thích thể loại phiêu lưu, khoa học viễn tưởng. Mình xem phim phần 1 trước rồi mới mua truyện về đọc vì tò mò diễn biến của phần sau. Nên mình lên Tiki đặt 1 lần nguyên bộ 3 tập luôn. Rồi đọc 1 lèo hết cả 3 tập. Toàn bộ truyện hấp dẫn, lôi cuốn. Lời văn của tác giả James Dashner dẫn dắt nhịp nhàng, dễ dàng cuốn hút người đọc.
Bạn chắc chắn sẽ phải đọc đến tận trang cuối cùng của bộ truyện này khi bắt đầu những dòng đầu tiên.
5
829609
2016-03-20 22:45:49
--------------------------
388659
8953
NXB:			Kim Đồng
Người dịch:		Hoàng Anh
Thể loại:		Phiêu lưu dành cho teen
Bìa:			Đẹp
Chất lượng in, giấy:  	Tốt
Chất lượng dịch:	Tốt
Nhận xét:		 Xem phim của truyện này thấy hay, truyện không hay bằng phim. Lạ. Phim nói chung giữ đúng kịch bản của truyện nhưng hình ảnh mê cung trực quan, sống động; nhím sầu cũng dễ sợ hơn. Tình tiết của truyện còn lê thê làm nhịp nhanh của sự kiện không sống động mặc dù dịch giả cố gắng sử dụng đúng văn phong của tuổi teen. Hy vọng tập hai sẽ tốt hơn.
Khuyên: 		Hay, nên đọc.

4
129879
2016-02-29 13:55:47
--------------------------
386257
8953
đây là một quyển sách cho teen và vì thế nó chỉ dừng lại ở mức một câu chuyện chứ không thực sự sâu sắc. Tuy nhiên, cốt truyện vô cùng gay cấn, nghẹt thở, lời văn của tác giả đúng văn phong Mỹ, vô cùng dễ hiểu và nhanh chóng, lại không bị đứt rời hay lủng củng. Mỗi lúc buông sách xuống là trí tò mò lại thúc giục bạn phải cầm lên và đọc cho hết những thứ còn lại, để xem xem sự thực dằng sau tổ chức đó là gì, vì sao lại có Terese - người con gái duy nhất, người cuối cùng, mê cung sẽ được giải không,...?
Bìa sách thiết kế tốt.
Giấy ổn
Khổ sách dài, thon hơn các sách thông thường nhưng nhìn vẫn đẹp.
Nội dung nhanh, gay cấn.
4
277478
2016-02-25 13:04:26
--------------------------
378126
8953
Biết đến bộ truyện này nhờ xem phim The Maze Runner và mình thấy quả thật truyện thú vị và hồi hộp hơn hẳn phim.Phim có lẽ  chỉ giống 20-40% truyện.Cái cách mà tác giả diễn đạt thật sự làm người đọc có thể dễ dàng hòa nhập và hình dung ra khung cảnh của truyện.Tuy nhiên theo mình có lẽ đối với một số người không quen đọc thể loại giống như thế này có lẽ sẽ cảm thấy có đôi chút hơi khó hiểu và mơ hồ. Nhìn chung thì Giải mã mê cung rất tuyệt vời và đáng đọc
4
440850
2016-02-04 13:58:41
--------------------------
373134
8953
Truyện này khá gay cấn, hồi hộp và có nhiều đoạn gây bất ngờ. Tuy nhiên, mình thấy nhiều đoạn nó cứ bị hơi cứng, đó là về lí do của các cuộc phiêu lưu, và kết quả của nó ấy. Và đọc truyện thì mình cảm thấy thích các nhân vật phụ hơn nhân vật chính - nhân vật chính tính cách nhiều điểm khó chấp nhận và rất hơi hợt. Với mình, ở series này có một điểm cộng đó là chuyện tình yêu khá mờ nhạt, nội dung truyện tập trung vào yếu tố phiêu lưu, yếu tố tình cảm chỉ chiếm phần nhỏ hầu như không đáng kể. 
3
450546
2016-01-22 23:01:31
--------------------------
372408
8953
Giải mã mê cung là tủ sách dành cho teen nên ca từ đều rất thiên hậu , có lợi về sau nên không có sự căng thẳng tranh giành quá quyết liệt , đều khiến cho mọi người sợ sệt , chỉ có thế lực cao siêu che chắn những thứ hiện lên quang đãng , bình thường như bao nhiêu người mới thật sự có tinh thần vì đồng đội của mình một vài những năm tháng như bao nhiêu người để thử thách trái tim của con người là gì , tại sao lại phải bỏ ra những thứ lớn lao để đạt được mục đích .
4
402468
2016-01-21 16:14:32
--------------------------
371385
8953
Mình được biết về phim và đi xem phim trước khi biết đến sách, và tận sau khi coi phim gần một năm mình mới đặt mua sách. Nhưng mình không hề thất vọng và thậm chí sách còn hay hơn phim.
Thomas là nhân vật chính, nhưng được xây dựng không hề hoàn hảo hóa lên, mà rất "người", biết sợ sệt, và biết rơi nước mắt khi đối mặt với nỗi sợ quá lớn.
Một tác phẩm thuộc dòng Khoa học viễn tưởng, thậm chí còn là loại mạnh khi luôn được dồn lên đến cao trào, bất ngờ, kịch tính, cảm giác như muốn bóp nghẹn tim người đọc.
Và sau khi đọc cả 3 tác phẩm thì đây là cuốn xuất sắc nhất trong bộ này mà các bạn nên đọc qua.
4
477889
2016-01-19 14:36:15
--------------------------
360104
8953
Cũng đã lâu lắm rồi mới có một quyển sách làm cho mình đọc liền tù tì thâu đêm thế này :)) 
Phải công nhận rằng ý tưởng, diễn biến câu chuyện mà tác giả xây dựng khá hấp dẫn, lôi cuốn người đọc bằng một loạt những câu hỏi dồn dập cần lời giải đáp. Chuyện gì sẽ xảy ra nếu Thomas không vượt qua được thử thách mê cung? Thứ gì đã xảy ra bên ngoài thế giới thực? VSAT rốt cuộc sẽ định làm gì tiếp theo?
Chỉ những đứa trẻ có lòng quyết tâm và can đảm cực độ mới dám mạo hiểm cả tính mạng của mình để tìm lối thoát cho bản thân và cho đồng đội. Theo chân Thomas và các bạn của cậu, một thế giới phiêu lưu không kém phần bí hiểm như mở ra trước mắt.
Điểm trừ cho truyện là phần miêu tả tính cách nhân vật hơi hời hợt, cần được chú trọng hơn nữa.
Nhưng túm lại thì mình rất hài lòng về cuốn sách này, với phương diện đánh giá là một con mọt fantasy :)
4
282245
2015-12-28 18:13:47
--------------------------
353918
8953
Nếu ai đã từng xem film được chuyển thể từ bộ truyện này sẽ thấy khi đọc truyện, ta như bị cuốn hút vào hơn qua từng câu chữ - lời văn diễn tả so với trên film. Câu chuyện lôi cuốn người đọc vào một mê cung đầy những hiểm nguy và phức tạp, và rồi dẫn dắt người đọc theo một chuyến phiêu lưu kỳ bí. Càng đọc sâu vào, người đọc càng thấy thắc mắc và muốn đọc tiếp. Tôi nghĩ các bạn nào đang có nhu cầu mua thì nên đặt mua để được hoà mình vào cuộc phiêu lưu này. Đặc biệt là những ai đã từng xem film. Các bạn sẽ có sự so sánh giữa truyện và film dựng từ truyện rõ ràng hơn.
4
555834
2015-12-17 07:30:10
--------------------------
340222
8953
Đã được coi phim này nhưng nay tôi lại quyết định tìm đọc phiên bản nguyên tác the maze runner. Dù sao thì đọc 1 tác phẩn truyện cũng có nét hay riêng của nó, phải không nào? Tuy nhiên, tôi hơi thất vọng với cách mở đầu của tác giả khi bắt đầu câu chuyện. Nó khá là dài dòng. Thế nhưng khi càng về sau, cách viết của James Dashner có nét gì đó cuốn hút khiến tôi phải bỏ thời gian ra để có thể đọc hết cuổn sách này. Cuộc hành trình tìm lối thoát ra khỏi mê cung của Thomas và những người bạn thật sự rất thú vị và hấp dẫn. Đây là 1 tác phẩm rất đáng để dành thời gian để đọc nó! 
4
314290
2015-11-19 14:21:04
--------------------------
336768
8953
Bìa sách đơn giản nhưng cũng khá bắt mắt. Có mấy trang màu cũng đẹp. Nội dung thì miễn chê. Cốt truyện sáng tạo, thú vị, đưa người đọc đến những giây phút hồi hộp, bất ngờ nhất. Cứ mỗi lần tưởng chừng như chuyện sẽ tốt đẹp hơn nhưng phía trước còn nhiều điều kinh khủng khác nữa. Rất ấn tượng với việc khắc họa nội tâm, tính cách của các nhân vật, chỉ tiếc là việc miêu tả ngoại hình nhân vật chưa được chú trọng lắm. Nhưng nói chung là hay, đáng đọc. Thích hợp cho mấy bạn yêu thích thể loại viễn tưởng.
5
477880
2015-11-12 21:57:29
--------------------------
329191
8953
Đây là quyển mở đầu trong bộ ba series the maze runner. Với đề tài hậu tận thế, cốt truyện mới mẻ, cuốn sách thật sự không làm tôi thất vọng. Tình tiết hồi hộp, bí ẩn và mơ hồ nhưng như vậy càng làm tôi tò mò và thích thú hơn. Mê cung và lời giải, sự thật của toàn bộ mọi chuyện là thế nào ? Một câu hỏi mà phải đọc ta mới biết câu trả lời. Chất lượng sách đẹp, giấy nhẹ, bìa in chữ nổi, tôi thật sự thích sản phẩm này của NXB Kim Đồng
4
187187
2015-10-31 11:11:21
--------------------------
327724
8953
The Maze Runners là tác phẩm thích hợp cho những ai đam mê trong những câu đố, những bí mật, những Biến số. Đưa người đọc từ lớp mê cung này đến từng lớp khác, một cách kì bí nào đó mà chúng ta vẫn lọt thỏm trong cái thế giới của James Dashner dù đã thoát ra. Có điều, phần dịch của Nhà xuất bản Kim Đồng hơi thất vọng khi không truyền được không khí gấp rút, mạch ngắn và dồn dập, một số từ ngữ riêng của tác phẩm nên để theo nguyên tác thay vì dịch ra.
4
354956
2015-10-28 13:41:09
--------------------------
319948
8953
Đầu tiên là về chất lương sách thì phải nói là ảnh bìa rất đẹp, nhưng lại không được bọc plastic nên mình đọc xong rất sợ bị xước. Bên trong, dòng chữ được in ấn rất đồng đều, không quá to, dễ đọc. Còn về nội dung thì chủ yếu là dành cho tuổi teen là chính vì có tính giải trí, hành động. Cảm xúc, tâm trạng của nhân vật cũng được tác giả chú ý khai thác rất nhiều, nhất là Thomas. Từ đầu người đọc đã được đặt ra một bí ẩn là tại sao Thomas lại bị lạc vào mê cung nay. Nói chung thì đây là một trong những tác phẩm đã góp phần tạo nên tên tuổi của James Dashner
4
853231
2015-10-10 00:24:20
--------------------------
151098
8953
Là một quyển sách đọc được, mang tính giải trí cao, nhưng đôi chỗ hơi thiếu sự cân đối khiến cốt truyện mất đi sự gay cấn nên có, cũng như cảm xúc của nhân vật chính Thomas thực sự hơi khó mà phân tích nổi. Các nhân vật còn lại cũng được xây dựng khá mờ nhạt, thiếu dấu ấn, cho nên dù câu chuyện tạo nên rất nhiều người chết, nhưng không đọng lại trong lòng độc giả chút nào gọi là thương cảm nổi khi mà họ mờ nhạt như vậy, thậm chí dù là nhân vật gần gũi với nhân vật chính nhất không may bỏ mạng vì một lý do cao cả, cũng chỉ khiến tôi tròn mắt ngạc nhiên thôi chứ không thể than thở cho cậu bé nổi. Nói cách khác, tôi cảm giác có lẽ tác giả quá đề cao tính hành động mà quên mất tính cảm xúc cho một câu chuyện, điều đó vô tình khiến mạch truyện đều đều như một cái máy đọc, có những đoạn nên gay cấn dồn dập thì do miêu tả không khí xung quanh cũng như cảm xúc nhân vật chính chưa đủ mà thành ra chậm đi nhiều. Tuy về cuối truyện tình huống đó có được cải thiện đôi chút, nhưng bấy nhiêu vẫn chưa đủ để trở thành một câu chuyện nhất định phải đọc.
Nhìn chung, giải trí được, ngôn từ cũng như giọng văn tác giả rất tốt, thế nhưng nhiều chỗ hơi “đơ” nên vô tình khiến tôi khó mà tập trung cũng như cảm nhận thế giới mà tác giả gửi gắm.
Hy vọng tập tiếp theo sẽ cải thiện tình trạng này.
3
40130
2015-01-18 17:11:44
--------------------------
140213
8953
Có thể nhận ra 1 điều là thông qua tác phẩm thì tác giả đã tạo nên 1 bối cảnh thế giới mới như bao tác phẩm viễn tưởng khác nhưng có nét đặc trưng riêng để thu hút người đọc, đặc biệt là ở quyển 1 - Giải Mã Mê Cung.
Quyển 1 này đã được chuyển thể thành phim, tuy khá dài gần 2h nhưng ngoài những hình ảnh hùng vĩ của những bức tường hay những chi tiết của mô hình Quỷ Độc gây ấn tượng với người xem nhưng vẫn chưa thể hiện hết những gì mà sách có thể truyền đạt cho chúng ta, sự nguy hiểm, hồi hộp, tâm lý nhân vật hay đơn giản là nổi bật những vai diễn cá nhân mỗi người.
Thomas xuất hiện quá chớp nhoáng và gồng gánh cả team với tốc độ khá là ảo diệu, trong khi đó Minho lại khá mờ nhạt và bị lu mờ bởi Thomas, nhân vật nữ chính xuất hiện khá muộn và vai trò duy nhất là đem 2 theo lọ thuốc giải vẫn chưa thực sự được thể hiện gì nhiều, tác giả đã rất thành công trong việc xây dựng đặc trưng trong ngoại hình lẫn tính cách của từng nhân vật nhưng lại quá tập trung vào nhân vật chính ở phần 1 này.
4
451219
2014-12-10 15:09:21
--------------------------
131798
8953
Được tặng nguyên bộ ba cuốn Giải mã mê cung, cùng lúc với thời điểm bộ phim đang hot trong rạp. Câu chuyện bắt đầu nhanh, mạch truyện dòn dập cuốn tôi theo từng trang giấy, vừa coi mà vừa muốn biết tiếp theo sẽ ra sao, kết thúc sẽ thế nào. Cho đến cuốn thứ 2, tác giả cho người đọc quay mòng mòng bởi một số sự kiện thật giả lẫn lộn và khó hiểu. Một đống những câu hỏi cứ liên tục xuất hiện khi theo dõi từng bước chân của Thomas. Cảm xúc cứ bị cuốn theo và kết thúc phần hai là một cái khó chịu cần được giải tỏa ở phần ba. Kết thúc cuối cùng cho người đọc một cái gì đó hụt hẫng, nuối tiếc, hơi ghét ghét một tí bên cạnh một kết thúc có hậu. Theo nhận xét của tôi là tuy truyện có phần dài dòng trong một số tình huống, nhưng cũng là một bộ truyện đáng để xem dành cho những người yêu thích thể loại phiêu lưu.
3
34721
2014-10-27 16:44:54
--------------------------
129395
8953
Tôi biết đến bộ truyện này qua thông tin về bộ phim được chuyển thể từ nó. Vốn là một fan cứng của dòng YA này, chỉ cần vài dòng giới thiệu là tôi quyết định mua nó ngay (tôi mua ở Hội sách thành phố năm nay). Rất là hào hứng, tôi tin tưởng ít nhất nó cũng hay bằng một phần nào đó so với The Hunger Games. Nhưng sau khi gấp cuốn sách lại, phải nói là tôi khá thất vọng. Kịch tính, hấp dẫn vẫn có ở đó, những ý nghĩa nhân văn sâu sắc về tình bạn, về nỗ lực vượt qua thử thách cho dù khó khăn thế nào, cho dù ta đã thất bại biết bao nhiêu lần, nhưng sao tất cả những điều ấy, đối với tôi vẫn chưa chạm đến một mức cần thiết, cái nào cũng hời hợt, cứ đều đều. Kết cấu truyện không có gì mới, cũng cái kiểu kết mỗi chương là 1 cái twist giật gân nào đó, vấn đề là mấy cái đó cũng không khó đoán lắm.
Nói chung, đây là một truyện tạm chấp nhận được, phù hợp với các bạn teen ưa thích khám phá và những điều bí ẩn hoặc là những người thích đọc liền tù tì. Ai mà đọc cái kiểu ngẫm ngẫm đọc chút chút rồi ngưng để suy nghĩ thì chắc là chán chết. Còn với tôi, tôi chắc chắn sẽ không đọc cuốn này lần 2 và cũng không mua tiếp 2 phần tiếp theo.
3
68002
2014-10-09 16:48:48
--------------------------
128846
8953
Với một lối kể truyện dồn dập ngay từ những giây ban đầu của bộ truyện, tác giả đã tạo nên một cuộc phiêu lưu vô cùng hấp dẫn, kịch tính, gây cấn khiến cho người đọc không thể đặt sách xuống mà phải đọc và suy đoán đến tận trang cuối cùng. Kể cả khi đọc hết bạn vẫn muốn tiếp tục đọc lại, tiếp tục chìm đắm trong Trảng, tiếp tục phiêu lưu cùng Thomas và Teresa khám phá bí mật của mê cung khổng lồ. Tuy vẫn còn hạt sạn khi tác giả không đi sâu vào nội tâm nhân vật và cảnh vật nhưng thế giới và những sự việc xung quanh nhân vật đã tạo nên một bức tranh vô cùng hoàng mỹ
5
349338
2014-10-05 09:37:28
--------------------------
128337
8953
Ban đầu mình mua quyển sách là vì thấy ngoài rạp bộ phim chuyển thể cùng tên sắp ra mắt. Mình xem phim trước, đọc sau. Dù đã biết trước hết nội dung nhưng khi đọc rồi vẫn bị sốc.
Nó không giống với The Hunger games. Nó là một chuỗi những kinh hoàng nối tiếp nhau không bao giờ dứt. Bạn không biết trước được điều gì sắp diễn ra, rồi bạn sẽ quen dần với những điều bất ngờ, nhưng cái sau còn khủng khiếp hơn cái trước. Chỉ đến khi đóng quyển sách lại, bạn mới có thể tạm thở phào nhẹ nhõm, nhưng thay vào đó là cảm giác thèm muốn quyển sau ngay lập tức. 
Lắm lúc, mình cảm thấy rất thương cho các đứa trẻ trong truyện. Thật ngạc nhiên là chúng không hóa điên khi phải trải qua chừng ấy chuyện. Cứ mỗi lần tưởng mọi chuyện đã khá hơn thì hóa ra phía trước còn nhiều thứ khủng khiếp hơn đang chờ đợi.
Tuy vậy, đây là một bộ sách hay đáng để đọc dành cho những bạn có đam mê về thể loại này.
5
23627
2014-10-01 09:59:28
--------------------------
123949
8953
Một nhóm những thiếu niên nam sinh sống tại Trảng, nơi ẩn chứa nhiều bí mật do một tổ chức có tên VSAT quản lí. Tại đó, họ phải tìm cách thoát ra khỏi một mê cung thay đổi lời giải hằng đêm và được canh gác bởi quái vật Nhím sầu. Mọi chuyện bắt đầu thay đổi khi Thomas và Teresa được gửi đến Trảng với thông điệp: mọi chuyện sắp kết thúc. Liệu họ có tìm được lời giải để thoát khỏi đây và khám phá ra sự thật về nơi này?
“The Maze Runner” cũng có những nét tương đồng với những bộ trilogy giả tưởng khác như ‘The Hunger Games” và “Divergent”, nhưng thay vì chiến đấu với những thế lực xấu như trong những bộ truyện trên thì trong “The Maze Runner”, VSAT tuy che giấu nhiều bí mật, nhưng trong những kí ức bất thường của Teresa, tổ chức này là tốt. Điều này khiến cho bộ truyện thêm hấp dẫn, khiến người đọc luôn háo hức lật những trang tiếp theo để khám phá hết những bí mật còn bị che giấu.
4
307488
2014-09-04 16:06:53
--------------------------
120565
8953
Nhân dịp NXB Kim Đồng giảm giá 30%, tôi có mua bộ ba quyển này và đọc từ đầu cho đến cuối. Thật sự rất dễ lôi cuốn vào vòng xoáy, vào những câu hỏi, vào nhửng uẩn khúc đằng sau từng chương. Điều nổi bật và cũng là ưu điểm của tác giả là dường như ông luôn "trên cơ" người đọc. Một khi người đọc dần hình thành kết quả của những manh mối mà ông cố tình đưa ra, thì đột nhiên lại xuất hiện thêm nhiều uẩn khúc, nhiều bí mật khiến độc giả bị bẻ ngoặt sang hướng mới. Không nói ngoa khi ví bạn đọc cuốn sách này như ngồi bên tay lái phụ trong cuộc đua xe trên dãy núi hiểm trở của tay đua cự phách. cái cảm giác phấn khích không thể chối bỏ tạo nên thành công New York Times Bestseller của bộ truyện. Điểm trừ duy nhất có lẽ là James không đi sâu khai thác nhân vật, không trả lời cho những khúc mắc liên quan riêng tư đến nhân vật. Một bộ truyện đáng có và đáng đọc.
4
50671
2014-08-13 13:17:18
--------------------------
105082
8953
Thật sự là một cuốn truyện cuốn hút, James Dashner thật sự quá tài tình khi tạo 1 câu chuyện với quá nhiều câu hỏi, để rồi tháo nút từ từ khiến độc giả gần như không thể bỏ xuống được, để rồi kết thúc với một cái kết không thể nào hồi hộp hơn nữa. Nhịp truyện nha, cuốn hút, ít bị bỏ lửng tình tiết khiến cho người đọc cảm thấy đọc rất mượt, dễ nuốt, miêu tả tâm lý nhân vật khá xuất sắc, chỉ tiếc là nội tâm nhân vật chính không được trau chuốt bằng các nhân phụ, và James Dashner cũng  chưa thực sự chú trọng phần miêu tả nhân vật và cảnh vật lắm. Nhưng nhìn chung, đây vẫn là một cuốn truyện hay đáng để rinh về :)
5
184272
2014-01-23 15:18:50
--------------------------
96628
8953
Phải công nhận điều đúng đắn mà Dashner đã làm được xuyên suốt tác phẩm không phải là cung cấp đủ tất cả thông tin để trả lời những câu hỏi dần hình thành trong tâm trí người đọc mà là những thông tin đủ để thúc đẩy người đọc tiếp tục theo dõi diễn biến câu chuyện. Sự tò mò đã khiến tôi đọc một mạch cho đến cuối cuốn sách và có một thực tế là cách hành văn của tác giả rất trôi chảy, nhiều kịch tính và khá “dễ nuốt”. Tuy nhiên có một chút bất ngờ về những cơ hội ông đã bỏ qua thay vì nắm bắt để lôi cuốn người đọc sâu hơn vào câu chuyện hay quan tâm nhiều hơn đến số phận các nhân vật, một điều lạ nữa là việc xây dựng nhân vật phụ dường như được chăm chút hơn nhân vật chính – vốn nhiều tính cách hơi khó chấp nhận và đôi chút hời hợt. Tóm lại, câu chuyện mà tác giả phát triển hoàn toàn có thể gọi là hay, nhiều sáng tạo, mang tính giải trí, thỏa mãn óc tò mò nhưng cần một độ sâu và tinh tế hơn trong cách viết.
3
63189
2013-10-13 10:09:26
--------------------------
95583
8953
Như tựa của tác phẩm, toàn bộ câu chuyện như một Mê cung mà tác giả muốn ta phiêu lưu, tìm kiếm lời giải. Tưởng tượng như chính mình nhập vai vào Thomas, song hành cùng nhân vật. Khi bạn bước vào câu chuyện, kí ức bạn sẽ là một mảng giấy trắng, càng đọc càng nhiều câu hỏi xuất hiện, càng khiến ta tò mò muốn đọc tiếp để tìm lời đáp cho những câu hỏi đó. Đó chính là điểm lôi cuốn của truyện. Xuyên suốt câu chuyện không thiếu các pha hành động gây cấn, hồi hộp với lối kể sinh động của tác giả. Cho đến cuối câu chuyện vẫn còn một thắc mắc được bỏ lửng để mở ra một câu chuyện tiếp theo cho phần 2.
Truyện thích hợp cho những bạn yêu thích thể loại viễn tưởng và thích phiêu lưu, khám phá.
4
163605
2013-09-27 22:39:25
--------------------------
