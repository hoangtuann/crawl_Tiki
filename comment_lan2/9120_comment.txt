439340
9120
Cuốn sách rất hay. Đầy tính nhân văn. Đây là một cuốn sách kết thúc có hậu. Nhưng ở phần thân truyện đầy bi thảm kể về cuộc đời của tác giả. Bị mẹ hành hạ đánh đập và trải qua nhiều lần làm con nuôi và những lần đi ăn cắp vặt. Bằng lối diễn đạt thú vị, cuốn sách khiến ta cảm thấy hồi hộp, lôi cuốn khi đọc qua từng trang. Qua đó giúp ta hiểu rõ hơn về cuộc sống của những người khổ sở. Và khiến cho ta thêm yêu cuộc sống hiện tại mà mình đang có.
5
1343129
2016-05-31 09:04:57
--------------------------
332398
9120
Là quyển thứ 2 trong bộ 3 tự truyện của Dave Pelzer, tôi không nghĩ rằng chặng đường thơ ấu đầy đau đớn của tác giả lại kéo dài đến như vậy. Nếu như trong phần đầu cho thấy tuổi thơ của tác giả luôn gắn liền với đòn roi và những trò tai quái của bà mẹ mặc dù khi ấy ông chỉ là một đứa trẻ còn rất nhỏ thì khi sang phần 2, Dave đã lớn hơn một chút thế nhưng độ tàn nhẫn của bà mẹ cũng tỉ lệ thuận theo, cha ông thì lại quá yếu đuối, nhu nhược không thể bảo vệ ông trước mẹ mình. Thật không thể tưởng nổi bà ấy lại có thể nghĩ ra những cách hành hạ con trai mình như vậy. Về sau Dave may mắn thoát ra khỏi ngôi nhà của mình nhưng cuộc sống nơi trại trẻ cũng không khá hơn với ông, một đứa trẻ phải tự mình tồn tại giữa cuộc sống đầy khắc nghiệt, đương đầu với những khó khăn, thử thách trong khi bản thân mình còn chưa hiểu hết về cuộc sống này, quả thật Dave Pelzer có một tuổi thơ không thể bất hạnh hơn. 

Nhưng cuộc sống không lấy đi tất cả của con người, đó là nội dung trong phần cuối của truyện, Dave trưởng thành, bước ra từ bóng tối và một cuộc sống tốt đẹp hơn đã đến với ông.

4
41370
2015-11-06 11:55:26
--------------------------
285481
9120
Chắc  hẳn ai đã đọc "Không nơi nương tựa" thì sẽ rất mong đợi đến "Đứa trẻ lạc loài". Tác phẩm kể về cậu bé David bị mẹ ruột hành hạ, ngược đãi, đến mức cậu phải tự mình tìm đến sự che chở của những thầy cô giáo. Ngòi bút của tác giả vẽ nên bức tranh chân thực đến mức người đọc thấy đau như chính mình là nahan vật trong truyện. Cậu bé bị dằn vặt đến mức cậu không dám nghĩ đến việc phải về nhà. Và rồi cậu cũng đã thoát ra khỏi người mẹ tàn nhẫn nọ và làm con nuôi của người khác đến sáu lần nhưng vẫn không tránh khỏi sự hành hạ. Mặc dù vậy, cậu vẫn không đánh mất niềm tin vào cuộc sống, vào sự kỳ diệu của lòng yêu thương để mong đợi đến một cái kết kỳ diệu. Không chỉ mang giá trị của một tác phẩm văn học nhân đạo, Đứa trẻ lạc loài còn là một tiếng kêu, một lời kêu gọi về quyền trẻ em, về sự bảo vệ trẻ em trên toàn thế giới.
5
24486
2015-08-31 21:33:50
--------------------------
283891
9120
Tập 1 của quyển này làm mình quá ám ảnh nên mình quyết định mua ngay tập 2, và nó đã không làm mình thất vọng. Mang sự thật trần trụi và xúc động, thuở ấu thơ của cậu bé David khiến mình thật sự xót xa. Bị mẹ ruột mình hành hạ tàn nhẫn, phải chịu đựng những hình phạt khủng khiếp, cuộc sống luôn dằn vặt và khổ đau; ấy vậy nhưng may mắn là xung quanh Dave vẫn còn những thầy cô luôn thông cảm, hỗ trợ và giúp đỡ cậu. Nhờ đó mà tâm hồn David không bị chai sạn. Cậu vẫn chưa bao giờ mất niềm tin vào cuộc sống. Cậu tôi luyện được cho mình một ý chí mạnh mẽ và khi trưởng thành là một con người tuyệt vời. Mỗi trang sách mang đến vị đắng đọng lại trên đầu lưỡi người đọc. Khi đọc xong sách mình đã thử tự tưởng tượng bản thân mình là David, mẹ mình đối xử với mình như mẹ của David, và mình nghĩ là mình sẽ không thể chịu được quá một ngày như vậy. Thật đau đớn cho những đứa trẻ phải sống trong bạo lực gia đình. Cảm thấy bản thân thật may mắn khi mẹ không phải lúc nào cũng hiểu mình, nhưng luôn yêu thương mình vô điều kiện...
5
434574
2015-08-30 13:49:46
--------------------------
266058
9120
Mình mua cuốn sách này cùng một lần với cuốn Không nơ nương tựa của tác giả Pelzez và cả 2 cuốn sách đều không khiến mình thất vọng.
Về cuốn sách này, nó thực sự khiến mình xúc động ngay từ những trang viết đầu tiên, và càng về sau lại càng ám ảnh. Xuyên suốt cuốn sách là nỗi bất hạnh của một đứa trẻ khi ngay từ nhỏ đã không nhận được sự yêu thương, chăm sóc đúng mực của cha mẹ mà ngược lại, còn phải chịu sự ngược đãi tàn nhẫn từ chính người mẹ bị bệnh của mình. Đọc sách, ta thấy yêu thêm cuộc sống may mắn mà mình đang có đồng thời cảm thông cho số phận của những người bất hạnh hơn mình. 
5
138880
2015-08-14 13:03:45
--------------------------
242699
9120
Khi đọc cuốn sách này nó làm tôi đau lắm,đã làm tôi khóc, thương cho cái số phận bi đát của cậu bé David. Một đứa trẻ con mà phải trải qua bao nhiêu đau khổ, thâm trầm. Một đứa trẻ đáng ra ở cái tuổi của David thì rất cần tình thương yêu của người mẹ nhưng ở đây lại không như thế. Mẹ cậu bị bệnh nên đã hành hạ cậu một cách tàn nhẫn khiến cậu phải lo sợ khi mỗi lần về nhà, những lần hành hạ của mẹ cậu đã ám ảnh cậu suốt tuổi thơ của mình. May mắn thay một thời gian sau cậu được nhận nuôi nên có thể gọi là cậu đã thoát khỏi cái địa ngục đó. Sau 6 lần được nhận làm con nuôi cậu cũng đã cảm nhận được tình thương là như thế nào mà trước đây cậu thường hay nghĩ tới.Đây là một câu chuyện xúc động cho những bậc làm cha làm mẹ, hãy biết yêu thương con cái và hiểu con của mình hơn.
5
615286
2015-07-26 21:24:52
--------------------------
197890
9120
Lúc đầu khi chọn mua cuốn " Không nơi nương tựa" mình cứ nghĩ đó chỉ là một cuốn sách độc lập và không có các phần khác . Nhưng nào ngờ khi đọc phần giới thiệu mình lại thấy Không  nơi tựa có thêm hai phần tiếp theo là " Đứa trẻ lạc loài " và " Người đàn ông đi ra từ bóng tối". Và khi đọc Đứa trẻ lạc loài mình đã cảm nhận được nhiều hơn cả nỗi đau của cậu bé. Khi cậu bé thoát ra khỏi căn nhà của người mẹ, cứ tưởng hạnh phúc sẽ đến với cậu, nhưng cậu lại gặp hết rắc rối này đến khó khăn khác, khiến cho cậu từ một đứa bé nhút nhát trở thành một đứa quậy phá có phần bất trị. Câu chuyện của cậu khiến mình hiểu hơn tâm lí của những bạn hs cá biệt ở lớp mình. Tuy mình không thể nào tha thứ cho các hành động của các bạn ấy nhưng mình đã hiểu và cảm thông hơn với những học sinh cá biệt đó.
5
310891
2015-05-18 18:57:19
--------------------------
133995
9120
Sau khi đọc "Không nơi nương tựa" và giới thiệu tập "Đứa trẻ lạc loài", tôi cứ suy nghĩ tại sao sau khi thoát khỏi mẹ, cậu lại phải đến tận 6 gia đình làm con nuôi? Rồi mọi thứ rõ ràng hơn, mọi chuyện không hề đơn giản, cậu không thể hoàn toàn thoát khỏi người mẹ độc ác ấy. Và việc hòa nhập với môi trường mới là một thử thách lớn, cùng với hậu quả của nạn bạo hành, cậu dần hình thành thói quen 'ăn cắp vặt" chỉ đơn giản để chứng tỏ và được chấp nhận bởi những đứa trẻ khác. Và cậu gặp rất nhiều rắc rối, đến nỗi bị đổ oan là kẻ đốt cháy trường học và phải vào trại giáo dưỡng. Nhưng rồi cậu đã vượt qua tất cả để đến với bố mẹ nuôi cuối cùng của mình. Câu chuyện rất cảm động và đáng để đọc.
5
208405
2014-11-07 21:15:48
--------------------------
103837
9120
Cuốn sách là một tiếng kêu đau xé lòng của một tuổi thơ dữ dội và đầy ám ảnh. Chỉ như một lời kể của cậu bé Dave Pelzer, hoàn toàn chân thực, không mang nhiều tính triết lý sâu sắc nhưng lại khiến ta phải suy nghĩ nhiều, rất nhiều. Nước Mĩ văn minh bậc nhất, nơi nhân quyền được đề cao bậc nhất, thực sự tồn tại nạn bạo hành khủng khiếp vậy sao? 

Tuổi thơ là quãng thời gian êm đềm và đẹp đẽ nhất của đời người, nhưng tuổi thơ của Dave... bị chính mẹ ruột, anh em ruột, và cả cha ruột tổn thương và đau đớn. Đối mặt với những trò hành hạ kinh tởm của mẹ, Dave rất bản lĩnh và thông minh, một ý chí phải nói là rất rất kiên cường mới có thể vượt qua được những chuyện như vậy. Đáng để cho chúng ta - những người đang có gia đình hạnh phúc, cha mẹ yêu thương phải suy ngẫm và cảm thấy bản thân mình thực sự là quá may mắn...
5
66429
2014-01-04 18:53:35
--------------------------
79728
9120
"Đứa trẻ lạc loài" là một trong những cuốn sách cảm động nhất mà mình từng được đọc. Khi đọc đến đoạn giữa tác phẩm, mình đã phải dừng lại một chút, vì khi đó cảm xúc quá mãnh liệt, nỗi đau đớn đã lên đến cao trào như muốn bóp nghẹt trái tim người đọc. Những lời kể của Dave Pelzez quá chân thực đến độ, làm mình cảm tưởng như chính bản thân cũng đang trải qua những bất hạnh, những đau khổ của anh. Dave, khi đó chỉ là một đứa trẻ thơ dại, nhưng đã phải chịu đựng biết bao sự hành hạ tàn ác  của chính người mẹ ruột. Những nỗi đau thể xác và những thương tổn tâm hồn đã làm cuộc đời Dave chìm ngập trong bóng tối, sợ hãi và dằn vặt. Câu chuyện của Dave thực ra cũng là câu chuyện của rất nhiều trẻ em đang phải chịu cảnh bạo hành gia đình từ chính những người ruột thịt. Vậy nên, tác phẩm này, ngoài việc kể lại cuộc sống đầy bão tố của Dave, còn có một ý nghĩa cao cả hơn, đó là góp một tiếng nói mạnh mẽ bảo vệ quyền được sống và được yêu thương của mỗi trẻ em trên thế giới.
5
109067
2013-06-09 09:18:17
--------------------------
