280366
8487
Quyển "Cùng Con Rèn Thói Quen Tốt - Hành Vi" của nhà xuất bản Mỹ thuật gồm khoảng 5 câu truyện nhỏ dạy các bé về ứng xử hành vi trong giao tiếp như cách chào hỏi, phép lịch sự khi ăn uống trong nhà hàng, cách nói lời cảm ơn, cách chia sẻ đồ đạc hay đơn giản là việc bắt tay, các câu chuyện khá thú vị, hình vẽ khá dễ thương với các nhân vật như mèo con, lơn con, sóc con, gấu con v.v.. chắc chắn sẽ khiến các bé rất thích thú và với hướng dẫn của bố mẹ các bé sẽ học được các bài học từ sách và dần dần hình thành được nhứng thói quen tốt
4
153008
2015-08-27 16:48:27
--------------------------
256721
8487
Tôi chọn mua vì thấy do Nhà xuất bản Mỹ Thuật phát hành, quả nhiên đúng chất " Mỹ thuật", cực kì đẹp, màu sắc hài hòa ngay đến người lớn cũng thích, hình vẽ hóm hỉnh hài hước các con vật. Những mẫu chuyện nhỏ, kèm theo những tình huống giúp để dạy cho bé hiểu. 10 cuốn của bộ này rất hay và ý nghĩa, giá lại quá hời nữa. Tuy có vài câu chuyện ở phần "Mẹ hỏi con trả lời" nhiều câu hỏi bị sai hoặc câu hỏi vô lí không tìm được đáp án nhưng nhìn chung đây là bộ rất cần thiết cho bé.
4
57408
2015-08-07 09:00:52
--------------------------
