252524
8900
nghe tên thì mình tưởng là truyện tình cảm lãng mạn có nội dung chắc phải thú vị lắm vì tên khá là ngộ, nhưng mở ra thì bị bất ngờ đến bật cười. Đây là một cuốn sách tổng quan về lịch sử phát triển của Toilet, những thông tin thú vị mà ta có thể sẽ chẳng bao giờ biết, những thông tin bên lề, bên dưới, bên trong, bên ngoài, phụ kiện đính kèm cái toilet,... Đôi lúc cũng nên thư giãn bằng những cuốn sách như vậy, đặc biệt phù hợp với những người thích tìm tòi thông tin trong cuộc sống hàng ngày!
4
381969
2015-08-03 18:53:40
--------------------------
196435
8900
Tôi thấy nó rất tuyệt vì có rất nhiều thông tin mà cả lẫn người lớn và trẻ con đều không biết những thứ này.  Tôi chắc là ai xem nó chắc chắn sẽ bị cuốn hút ngay, bên trong ngoài nói về bồn cầu mà còn nói về những cái khác như những mục riêng trong sách: cá sấu sống dưới ống cống hay... Nói về những sự thật ẩn giấu phía sau chiếc bồn cầu, còn nói về giá trị và cả thời gian những chiếc bồn cầu đầu tiên trên đời mà trước giờ chưa ai quan tâm tới. Nhờ cuốn sách này mà chắc chắn các bạn đọc sẽ hiểu rõ hơn bồn cầu quan trọng như thế nào đối với con người? Mong sẽ có nhiều người đón đọc nó nhiều hơn và hiểu rõ về bồn cầu.
4
400003
2015-05-15 21:12:16
--------------------------
