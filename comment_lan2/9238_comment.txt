390472
9238
Tôi đã cất công tìm kiếm một cuốn sách về giới tính để giúp cô em gái đang tuổi dậy thì của mình hiểu rõ hơn về tuổi của cô ấy. Và tôi quyết định mua cuốn này.
Cuốn sách đề cập tới đủ các vấn đề xung quanh giới tính: sự thay đổi cơ thể khi dậy thì, sự hình thành một con người, em bé ra đời ra sao, tình dục an toàn, rối loạn hành vi tình dục, lạm dụng tình dục... Rất đầy đủ. Và quan trọng là những vấn đề trên được trình bày với giọng văn hóm hỉnh, ngôn ngữ update, không hàn lâm nên không gây nhàm chán và rất gần gũi, dễ hiểu. Ví dụ "bố Bin bảo "Này gã đàn ông tương lai, bây giờ "cái ấy" của những thằng con trai đã được giao thêm nhiệm vụ mới. Nó đang được rèn luyện và đào tạo để khi con lớn lên thì nó ra chiến trường..."Khẩu súng" tốt là khẩu súng có những viên đạn chất lượng tốt...Nhiệm vụ của khẩu súng là phải làm mình thấy thoải mái, cân bằng sinh lý và duy trì nòi giống. Đây là một trách nhiệm vinh quang mà ông trời giao cho những người đàn ông con ạ."
5
624508
2016-03-03 22:29:12
--------------------------
345109
9238
Từ lâu, chuyện về tình cảm trai gái, vấn đề giới tình và những điều liên quan luôn là một vấn đề nhạy cảm, rất khó nói, bàn luận với nhau. Nhất là các bậc cha mẹ, không biết nên dạy con những vấm đề đó như thế nào để con có thể hiểu rõ những thay đổi của bản thân khi bước vào tuổi dậy thì. Với quyển sách này, tác giả đã "hướng dẫn" chúng ta, một cách ý nhị, có thể trò chuyện với con cái về những vấn đề thay đổi của tuổi mới lớn. Quyển sách này thật hữu ích!
4
746395
2015-11-29 20:20:57
--------------------------
290181
9238
Một cuốn sách khá hay nói về đề tài giới tính. Khi đọc nó tôi biết thêm được nhiều kiến thức về giới tính, về những điều mình còn mập mờ mà không biết ai để hỏi. Cách ví von, gọi tên và kể chuyện rất nhẹ nhàng, dễ hiểu, đôi khi giống một câu chuyện cổ tích nào đó. Nói chung là hữu ích. Rất cảm ơn tác giả về những chia sẻ cùng kiến thức.
Tuy nhiên, nếu tác giả phân theo tuổi thì sẽ hay hơn, tức là ở độ tuổi nào thì tiếp nhận kiến thức nào là phù hợp. Bởi tôi chẳng biết khi nào thì sẽ cần biết điều gì. Hơn nữa, cách trình bày sách không thật sự ấn tượng, không lôi cuốn tôi cho lắm. Hi vọng là sẽ có nhiều hình ảnh minh họa theo phong cách teen teen thật ấn tượng và thú vị.

2
504952
2015-09-05 09:53:34
--------------------------
270654
9238
Trò chuyện về giới tính, phải nói thật là một chủ đề khá nhạy cảm và còn nhiều loay hoay trong quá trình thực hiện ở Việt Nam. Đến trường khi đến phần giáo dục giới tính thì thường các thầy cô giáo thường hay nói qua loa, đại khái, ở nhà thì bố mẹ đôi khi nghĩ là ở trường chắc chắn sẽ dạy, hoặc ngại ngần thậm chí là quát khi con hỏi đến. Vì thế trẻ thường ít nhận được sự giáo dục đúng đắn từ nhà trường lẫn gia đình, mà thường tự tìm hiểu dưới dạng sách báo, trang web...và đôi khi là đi lầm đường vì con đường tìm hiểu đó.
Đây là một cuốn sách NÊN MUA, nên đọc, tham khảo và áp dụng đối với các bậc phụ huynh có con đang trong giai đoạn phát triển dậy thì. Cuốn sách viết khá hay, dễ hiểu, đi sâu vào tâm lý của cả phụ huynh lẫn con trẻ!
4
104897
2015-08-18 14:35:27
--------------------------
73639
9238
Việt Nam đang dần trở thành nước có tỷ lệ phá thai ở tuổi vị thành niên cao nhất thế giới, chỉ vì những sự kém hiểu biết về giới tính. Chúng ta từ bậc cha mẹ đến con cái thường ngại nói với nhau về những vấn đề tình yêu hay tình dục, chính vì vậy mà giới trẻ thường có nhận thức không đúng đắn hay thiếu hiểu biết về tâm sinh lý của chính mình cũng như những nhu cầu cơ bản để có những cách hay biện pháp giải tỏa thích hợp. 
Đây thực sự là một cuốn sách bổ ích, ý nghĩa để các bậc cha mẹ cũng như con cái tìm hiểu để hiểu rõ hơn và cơ thể chúng ta. Nhìn nhận đúng đắn những chuyện cần thiết mới mang lại cho giới trẻ một tương lai an toàn.
4
54418
2013-05-09 12:34:53
--------------------------
