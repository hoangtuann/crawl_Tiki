471634
8917
Tôi đã mua cuốn “Truyện Cổ 3D Kinh Điển: Na Tra Đại Náo Đại Dương” vì nhìn bìa hình ảnh có vẻ rất hấp dẫn, và đẹp (hình vẽ phong cách 3D nhìn rất sinh động), tất cả nhân vật đều vẽ rất dễ thương, nhất là trang 8, đoạn “Quái vật từ đâu ra mà xấu xí vậy, không thèm nói chuyện với ngươi”...Mực in đậm và rõ trên giấy cứng dày. Chất lượng sách như vậy tôi rất hài lòng. Khuyết điểm: không thấy trừ trong đoạn cuối trang 19, Na tra được Thái ất chân nhân tặng “súng lửa”. Không biết vũ khí tối tân này sao lại có thể xuất hiện thời này, và trong hình cũng không hề thấy cầm vật gì có hình dạng để có thể gọi là “súng lửa”! Quyển sách không nhiều trang, hy vọng NXB sẽ cẩn thận hơn với sản phẩm của mình.
4
966571
2016-07-08 20:13:20
--------------------------
