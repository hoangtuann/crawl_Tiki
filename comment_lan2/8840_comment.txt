473450
8840
Lứa tuổi thanh thiếu niên là lứa tuổi đẹp nhất của đời mỗi người. Cuốn sách “Tâm tình tuổi teen” mang lại cho chúng ta rất nhiều câu chuyện đầy ý nghĩa và nhân văn về tình bạn, tình cảm gia đình, những rung động đầu đời của tuổi học trò. Cuốn sách là món quà ý nghĩa dành tặng cho các bạn đọc tuổi teen cũng như cha mẹ và thầy cô. Bởi ai trong chúng ta cũng đều trải qua những cung bậc cảm xúc và tình cảm của tuổi teen và đó là một phần kí ức tươi đẹp trong mỗi con người.
4
470567
2016-07-10 16:21:55
--------------------------
317284
8840
Tâm Tình Tuổi Teen là quyển sách được chắt lọc từ những câu chuyện xung quanh đời sống của những teen với lời văn lôi cuốn, hấp dẫn, phù hợp với lứa tuổi này. Sách đã hoàn toàn thể hiện được tâm tư của tuổi teen, gây ấn tượng cho người đọc. Bìa sách khá đẹp với màu sắc hài hòa, nhất là thích hợp với các bạn nữ, giá sách rẻ, hợp túi tiền sinh viên đồng thời đem lại nhiều kiến thức bổ ích giúp teen phát triển nhân cách của mình cũng như biết cách ứng xử trong cuộc sống
3
402581
2015-10-02 23:23:05
--------------------------
298932
8840
Ban đầu chọn mua cuốn sách " tâm tình tuổi teen " do bị cuốn hút bởi bìa sách khá đẹp và cũng muốn hiểu rõ hơn tâm tư của lứa tuổi chưa hết trẻ con mà cũng chưa phải người lớn này . Nhưng khi đọc quyển sách tôi càng ấn tượng hơn khi quyển sách được phân chia thành từng phần rõ rệt với nội dung vô cùng bổ ích . Nó khiến người đọc nhận ra rất nhiều điều hay , thú vị . Nó như một phần hành trang trên con đường trưởng thành vậy . Đây là một cuốn sách đáng để đọc 
5
734279
2015-09-12 22:27:22
--------------------------
238947
8840
Mình mua cuốn sách này một phần là vì bìa đẹp, một phần cũng vì tựa sách là "Tâm tình tuổi Teen" nên mình nghĩ là rất hợp với mình. Cuốn sách này đối với mình giống như một cuốn cẩm nang để bước vào tuổi mới lớn đầy mộng mơ và đáng để ghi nhớ. Cuốn sách chia làm 5 phần với 5 nội dung khác nhau: Lắng nghe âm thanh của cuộc sống; Thưởng thức khúc nhạc êm ái từ cõi lòng; Cái giá của sự trưởng thành; Thắp sáng ngọn lửa tuổi xuân và Mỉm cười cảm ơn cuộc sống. Bao gồm những câu chuyện về con cái-cha mẹ, thầy-trò, bạn bè,... Một cuốn sách nhẹ nhàng và đáng yêu khiến bạn nhớ mãi!
4
537523
2015-07-23 15:25:56
--------------------------
