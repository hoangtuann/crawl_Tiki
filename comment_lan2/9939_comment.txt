89177
9939
Đây là một trong 2 cuốn sách Phân Loại Và Phương Pháp Giải Các Chuyên Đề Hóa Học mà mình đang tìm kiếm. Nhờ thầy Vũ Khắc Ngọc giới thiệu, mình đã mua được cuốn sách này (không phải ở Tiki.vn). Lúc đó mình học 11, sau gần 2 tuần nghỉ tết đọc cuốn sách này thì kỹ năng giải hóa của mình cao hơn hẳn, nhờ nắm chắc kiến thức nên mình đạt kết quả cao trong kỳ 2 . Nội dung cuốn sách tập trung vào phương pháp như bảo toàn khối lượng, tăng giảm khối lượng,  nó được biên soạn rất kỹ với nhiều ví dụ điển hình và hay. Hiện mình có kế hoạch mua cuốn tiếp theo về hữu cơ vì năm nay mình lên 12 rồi. Nếu bạn muốn đậu đại học thì mình khuyên nên mua luôn  2 cuốn 
5
113831
2013-07-30 21:47:35
--------------------------
83399
9939
Hóa học 12 phần hữu cơ dễ hơn vô cơ, cho nên mình đã mua và dùng cuốn sách này. Những kiến thức được bác Hưng biên soạn rất kỹ. Sau kiến thức là những dạng toán cơ bản, rồi đến nâng cao, mỗi dạng đều có bài tập ví dụ và cách giải chi tiết. Sau ví dụ bài tập là phần bài tập thực hành giúp mình củng cố kiến thức, nhưng không có lời giải bên dưới mà có hướng dẫn giải đằng sau quyển sách. Điều đó giúp mình có thể tra cứu lại kết quả, bài nào không biết có thể lật ra xem cách giải, rất hữu ích.
Vì cuốn sách này được biên soạn theo trình tự từ dễ đến khó cho nên khá là có tác dụng với cả các em lớp 10 và cả 11. Nếu mất căn bản thì tìm đọc rất có ích, nếu vững căn bản thì tìm đọc cũng được vì trong này còn có phần nâng cao.

4
50499
2013-06-26 14:10:24
--------------------------
70618
9939
Nếu đang là học sinh lớp 12 và ngay cả là học sinh lớp 10 hay 11, nếu cần bổ sung các kiến thức hoặc nếu muốn có những điểm chú ý quan trọng khi làm bài hoặc cần một số vấn đề mở rộng và cách làm nhanh các bài trắc nghiệm Hóa thì nên tham khảo thêm cuốn sách này. Tôi đã luyện quyển này năm học 12 từ 3 đến 4 lần trong năm học và gần thi đại học thì tôi xem lại 1 lượt. Cách trình bày trong sách cũng rất dễ hiểu, có thể tự học ở nhà để nâng cao kiến thức cũng như phản xạ nhanh khi làm bài tập. 
Nếu bạn muốn nâng cao điểm số môn Hóa thì bỏ qua, vì ở trường mình, sách của Đỗ Xuân Hưng là tác giả được nhập số lượng sách vào thư viện lớn nhất và được học sinh mượn nhiều nhất. 
5
34754
2013-04-22 12:17:33
--------------------------
49702
9939
học là cả một quá trình nỗ lực và phấn đấu lâu dài. Nhưng học thì có những môn ta thích, có môn không. Năm lớp 12, tôi rất sợ môn hóa, đặc biệt là hóa vô cơ, tôi không nắm chắc kiến thức và khả năng làm bài tập cũng tệ...... Và chị tôi đã tăng tôi cuốn sách này, tôi nhận thấy rằng, nó thật hữu ích cho tôi, tôi dần dần lấy lại được căn bản, bắt đầu tập trung vào ôn môn hóa, tôi dần bớt sợ nó hơn, tôi dần phân biệt được các dạng bài tập, làm thành thạo hơn.
Sách viết khá rõ ràng, các dạng bài tập đa dạn, được phân nhiều  loại  phong  phú.....Nó giúp cho ta thêm phần tự tin để chuẩn bị bước vào kì thi đại học gay go.
4
78790
2012-12-09 13:02:10
--------------------------
