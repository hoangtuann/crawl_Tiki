489471
9883
Mình đã đặt cuốn sách này từ rất lâu nhưng hôm nay mới viết nhận xét. Thấy cái tên bìa sách và hình ảnh minh họa hay hay, mình đã ấn vào link xem, bất ngờ hơn là cái tên tác giả thấy quen quen, hóa ra là cô bạn học chung đại học, đúng là trái đất tròn, mình đọc và tìm hiểu qua nội dung và quyết định đặt luôn. Câu chuyện hết sức dễ thương và trong sáng, cách gọi tên ngộ nghĩnh, khá lạ của các nhân vật trong truyện, những thử thách mà họ cùng vượt qua, những tranh luận vui và hài hước...tất cả tạo nên sự thu hút và thú vị cho câu chuyện, khắc sâu tình bạn giữa loài vật với nhau, dạy chúng ta nhiều bài học sâu sắc. Bìa đẹp, giấy trắng, chữ to, rõ, minh họa dễ thương. Mình rất thích cuốn sách này.
4
132424
2016-11-08 08:58:57
--------------------------
55436
9883
Những cuốn sách thiếu nhi luôn có một sức hút mạnh mẽ với nhiều độc giả. Không chỉ những độc giả nhỏ tuổi, mà cả người lớn cũng sẽ ít nhiều tìm thấy những điều thú vị khi đến với những tác phẩm này. "Ngôi nhà bay" là một cuốn sách thiếu nhi như vậy. Tác giả đã tạo nên một thế giới sinh động, đầy màu sắc, với những nhân vật quen thuộc nhưng được khắc họa tinh tế và đáng yêu. Những chi tiết về ngôi nhà bay, về chuyến hành trình đến với những vùng đất khác nhau được kể một cách lôi cuốn không ngờ. Mình rất yêu mến tác phẩm này và hy vọng sẽ được dọc nhiều hơn nữa những tác phẩm thiếu nhi của Việt Nam.
4
20073
2013-01-14 13:01:02
--------------------------
