459343
8801
Khi cầm quyển sách trên tay thì ấn tượng đầu tiên của mình là nó rất đẹp, bìa cứng cán nhũ lại có bao sách bookcare của Tiki, về mặt hình thức thì hoàn toàn không có điểm nào để chê :))
Đúng như tên gọi, quyển sách này tập hợp những câu chuyện ngắn đó đây nên lúc đọc đôi khi dễ lâm vào tình trạng chưa kịp cảm hết câu chuyện này thì đã nhảy sang câu chuyện khác không có gì liên quan :)) mình thường đọc xong 1-2 câu chuyện thì ngừng một lúc mới đọc tiếp, nếu không thì sẽ đọc như cái máy, chả cảm nhận được gì.
4
24079
2016-06-26 01:19:57
--------------------------
419800
8801
Đây là quyển sách thứ ba mình tìm đọc sau Nghìn lẽ một đêm và Thần thoại Hy Lạp. Bộ sách thật sự rất hay và ý nghĩa khi cung cấp những câu chuyện từ khắp nơi trên thế giới. Nhưng một trăm truyện chắc vẫn chưa đủ đễ diễn tả hết những cái hay mà câu chuyện muốn gửi gắm. Qua quyển sách tôi cũng đã học thêm được nhiều chân lý, những ý nghĩa châm biến mà từng câu chuyện đề cập. Cảm ơn tác giả  đã mang đến cho mọi đoc giả một kho tàng truyện thật hữu ích.
5
509425
2016-04-22 00:01:45
--------------------------
361875
8801
Cuốn sách thực sự rất tuyệt vời, nội dung chọn lọc hay, biên tập kĩ, minh họa cẩn thận và đặc biệt là đẹp mê ly. Sách in bìa cứng, có lớp bọc mạ vàng óng ánh, to, dày, có lụa đỏ đánh dấu trang làm người đọc có cảm giác đấy là cuốn sách mà mấy truyện cổ tích hay nhắc đến, mỗi tối lấy ra, mẹ hay bà đọc cho một truyện và nó cứ dài bất tận. Lâu lắm rồi mình mới nhận được cuốn sách khiến mình vui thích thế. Thực sự rất hài lòng khi dùng nó làm món quà cho con mình
5
23764
2015-12-31 20:16:07
--------------------------
356245
8801
Lúc mới nhận hàng quyển này thực sự là thấy rất phấn khích vì truyện rất đúng gu của mình. Tuy chưa đọc hết nhưng qua vài câu chuyện đầu thì mình thấy khá hay và đa dạng. Cực kết cái bìa rất đẹp luôn, tuy nhiên mình không thích lớp áo khoác lắm  vì nó dễ nhăn. Giấy hơi mỏng nhưng không phải loại giấy xốp nên mong là lâu ngày nó không bị ố vàng. Truyện có khá nhiều hình minh họa nên có vẻ sinh động. Cho quyển này khoảng 9/10 ^^ Nếu nội dung của 100 truyện mà  không có gì người lớn hay phản cảm thì mình nghĩ có thể dùng quyển này làm quà tặng cho con nít rất hay.
4
413317
2015-12-21 15:28:51
--------------------------
314545
8801
Nhận được sản phẩm thích ơi là thích vì bìa ngoài rất đẹp và bắt mắt. Bia cứng lại có thêm lớp vỏ bọc phụ tạo hình óng ánh bên ngoài. Nội dung chuyên thật phong phú, trải dài trên cả không gian và thời gian hầu như bao quát hết những nét đặc sắc của các nền văn hoá lớn giúp mình có thêm kiến thức về thế giới xung quanh mình. Câu truyện không chỉ phù hợp với trẻ em mà người lớn như mình cũng rất ham đọc. Phần trang trí minh hoạ đi kèm cũng miễn chê, càng làm tôn thêm sức hấp dẫn cho truyện.
5
93234
2015-09-26 12:09:13
--------------------------
278523
8801
Tôi luôn thích đọc truyện cổ, đặc biệt là thích những truyện mới lạ và có nội dung phong phú, và ngay những truyện đầu tiên đã đủ sức thuyết phục tôi. Đặc sắc và thú vị lắm, đa số tôi đều chưa đọc, ngoài truyện cổ còn có những truyện hiện đại viết về khoa học, về giáo dục. Tưởng chừng như sắp xếp đa thể loại như vậy sẽ dễ không ăn nhập và rời rạc, thế nhưng mọi thứ đều xuyên suốt, hợp lý, cốt truyện rộng mở bổ sung cho nhau nên không thấy nhàm chán. Quả là đáng đồng tiền khi mua cuốn này.
5
6502
2015-08-25 22:31:43
--------------------------
275759
8801
Quyển sách là tập hợp 100 câu chuyện khắp nên trên thế giới, theo đúng kiểu "đó đây" vì gần như không có một mô tuýp để liên kết các nội dung trong từng câu chuyện cũng như theo chiều thời gian hay địa lý nơi xảy ra tình huống trong chuyện. Về cách trình bày thì nhà xuất bản Đông A luôn làm mình hài lòng như các quyển Nghìn lẻ một đêm, Thần thoại Hy Lạp. Nhưng theo ý kiến cá nhân nội dung dịch của truyện không hay, thiếu sức lôi cuốn với người đọc, có thể sẽ phù hợp với các bạn nào thích hiểu biết, giúp tăng thêm hàm lượng tri thức.
2
8934
2015-08-23 10:36:16
--------------------------
