303023
8809
Sách thiên về nghiên cứu, thiếu tính thực tiễn, đi quá sâu vào chuyên môn khiến gây khó hiểu cho người đọc. Nội dung chung chung, thiếu tính cụ thể, chính xác, mang tính vĩ mô nhiều hơn là vi mô nên hầu như không áp dụng được vào các vấn đề cụ thể trong kinh doanh thông thường. Dự báo chiến lược trong kinh doanh cần sát với thực tế hơn chứ không phải chỉ là ngồi phán những vấn đề cao xa như thế này, sẽ rất khó áp dụng trực tiếp vào tình hình doanh nghiệp. Nói chung mình khá thất vọng về cuốn sách này.
2
14095
2015-09-15 16:43:47
--------------------------
254854
8809
Trước khi đọc quyển sách thì mình khuyên các bạn nên tìm những quyển sách về luyện phát âm trong tiếng anh (tác giả Trần Mạnh Tường cũng đã xuất bản quyển Luyện phát âm và Đánh dấu trọng âm tiếng Anh). Sách có rất nhiều thiếu sót, thứ nhất sách chỉ giới thiệu chung chung mà không hướng dẫn cụ thể là làm sao đọc đúng các từ trong tiếng anh, thứ hai là sách nên có các bài đọc mẫu trên đĩa cd để người đọc có thể kiểm tra xem là mình đọc đúng hay sai. Sách này chỉ dành cho những có khả năng phát âm cực tốt muốn luyện thêm khả năng đọc.
2
588073
2015-08-05 15:43:15
--------------------------
254847
8809
Khác với những quyển sách về chiến lược khác, quyển sách này mang tính nghiên cứu hơn là tính ứng dụng thực tiễn. Quyến sách trình bày rất nhiều nội dung trên diện rộng và mang tầm vĩ mô liên quan đến nhiều nội dung: dự báo trong chiến tranh,... chứ không phải riêng về lĩnh vực kinh doanh. Mình đã đọc quyển sách này 2 lần rồi nhưng thực sự là mình không hiểu hết nổi. Theo đánh giá của mình thì quyển sách này chỉ dành cho những nhà chiến lược thực thụ nghiên cứu mang tính hàn lâm chuyên sâu. 
5
588073
2015-08-05 15:38:52
--------------------------
