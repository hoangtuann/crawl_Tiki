386125
9952
Có thể nói "7 chiến lược thịnh vượng và hạnh phúc" là một trong những quyển sách viết về cách xây dượng thành công và hạnh phúc hay nhất mà tớ từng đọc. Quyển sách được viết với văn phong đơn giản, hàm chứa nội dung cô đọng mà tác giả muốn truyền tải. Sách bao gồm nhiều lời khuyên và kiến thức dành cho các yêu cầu cơ bản trong cuộc sống chúng ta: giải phóng sức mạnh của mục tiêu, tìm kiếm tri thức, học cách để thay đổi, để tự do tài chính, để làm chủ thời gian, nguyên tắc kết giao và xây dựng phong cách sống. Tóm lại, đây là một quyển sách bạn nên đọc, dù bạn là ai.
5
363649
2016-02-25 05:55:47
--------------------------
294323
9952
Tôi biết đến cuốn này nhờ vào thày Nguyễn Mạnh Hùng của Thái Hà Book và Tiki . Đây là 1 cuốn sách không quá dài chỉ hơn 200 trang (nên rất dễ cho những bạn ngại đọc sách dày ), chất giấy và bìa sách khá tốt . 
Còn về nội dung thì nghe cái tên cũng biết rùi . Sách rất thực tế và dễ áp dụng , không mơ mộng và giáo điều như một số sách cùng thể loại . Tác giả đưa cho chúng ta một câu hỏi lớn tại sao lại phải thay đổi , và khi bị dồn vào đường cùng thì ta sẽ phải thay đổi mình như thế nào ( dẫn chứng từ chính bản thân tác giả). Sau đó là xem lại mục tiêu , các mối quan hệ ,các nguồn lực và quản lý thời gian . Rồi cuối cùng cách thiết lập một chiến lược tài chính thịnh vượng. Cuối sách cũng có một vài nghệ thuật sống nho nhỏ mà chúng ta có thể áp dụng được ngay !!!
4
630821
2015-09-09 10:35:49
--------------------------
259694
9952
Cuốn sách này mình được một người bạn giới thiệu mua. Nhiều lần ra nhà sách tìm mua mà ko thấy có. Thấy tiki có hàng nên đặt mua ngay. Sách được tiki bọc plastic khá đẹp mắt và sạch sẽ. Jihm Rohn là một trong các tác giả nổi tiếng trong sách truyền động lực cuộc sống. Cuốn sách cho ta lần lượt 7 chiến lược cụ thể của sự thịnh vượng và hạnh phúc. Mỗi chiến lược đã viết một cách cụ thể, dễ hiểu và sinh động. Nó giúp ta có cái nhìn khác đi về sự thịnh vượg và hạnh phúc thật sự trong cuộc sống.
5
691865
2015-08-09 19:01:07
--------------------------
254033
9952
Nếu có thể mình sẽ cho 6 sao cho quyển sách này. Sách là những lời tâm huyết của Jim Rohn gởi đến chúng ta. Sách rất thích hợp cho bạn nào chưa tìm thay mục tiêu và ước mơ của mình hay đang nản chí. Bằng tài năng của mình, Jim Rohn đã diễn đạt mọi thứ thật dễ hiểu và dễ nhớ, súc tích ngắn gọn nhưng rất mạnh mẽ. Bạn mình cũng đọc thử và cảm thấy thích quyển sách này. Có lẽ mình sẽ mua thêm quyển nữa để tặng nhưng tiếc hiện giờ tiki hết hàng rồi
5
565415
2015-08-04 22:51:07
--------------------------
239711
9952
4 Câu hỏi đơn giản của Jim Rohn trong quyển sách này có thể truyền động lực thành công cho các bạn!
1. (Why?)
Có thể bạn đang tự hỏi mình rằng: Tại sao phải học? Tại sao phải giữ kỷ luật? Tại sao phải khổ cực tập luyện? Tại sao mình không giàu… Tại sao và tại sao?
và câu trả lời là..
 
2. Why not?
Tại sao không thử xem mình học giỏi đến mức nào? đọc được bao nhiêu cuốn sách? đi được bao xa? làm được những gì? Tại sao không?
Đừng đổ lỗi cho hoàn cảnh, cho những thứ bên ngoài. Nhưng tại sao ta không thử giải quyết những câu hỏi đó thay vì chỉ ngồi đặt câu hỏi và không làm gì cả?
 
3. Why not you?
Ai cũng có khả năng để trở nên tốt hơn, kiên trì hơn, thông minh hơn, giỏi giang hơn, chín chắn hơn, trưởng thành hơn. Vậy thì  “Tại sao không phải là mình?” Mình xứng đáng có được những thứ này cơ mà. 

4. Why not now?
Đâu là thời điểm tốt nhất để bắt đầu thực hiện tất cả những điều này. Không phải quá khứ, không phải tương lai, mà là hiện tại. Chúng ta đang sống trong một thời đại thay đổi nhanh đến chóng mặt, và nếu như ta không bắt kịp ngay hiện tại, ta sẽ bị bỏ rơi lại. Vậy thì tại sao ta không bắt đầu ngay lúc này?

Một cuốn sách tuyệt vời!
5
715394
2015-07-24 03:56:58
--------------------------
168805
9952
tôi biết tới tác giả Jim Rohn là do 1 người thầy của tôi là Jim Nguyen giới thiệu. Người thầy của tôi là 1 trong số học sinh của ông Jim Rohn. Tư tưởng, các bài giảng, những cuốn sách của ông ảnh hưởng tới rất nhiều người, làm cho họ trở nên tốt hơn, hạnh phúc và thành công trong cuộc sống. Tôi rất mong mình và cũng như tất cả các bạn sẽ được tiếp cận với bộ sách của Jim Rohn và lĩnh hội những điều hay áp dụng cho bản thân để được hạnh phúc và thành công như mong muốn
5
584396
2015-03-17 10:34:07
--------------------------
160237
9952
Theo mình, quyển sách này là 1 tác phẩm xuất sắc có lời văn gần gũi và thiết thực nhất mà mình từng biết. Xuyên suốt quyển sách là những kinh nghiệm của chính tác giả sau cả đời từng trải từ 1 người nghèo khó không có 1 xu dính túi đến 1 doanh nhân thành đạt và có tầm ảnh hưởng. Sách được cấu trúc thành 11 chương trải dài qua 7 chiến lược, mỗi chiến lược có thể gồm từ 1 - 3 chương, mỗi chương không quá dài nên đọc không hề ngán mà lại có cảm giác muốn đọc thêm. Từng dòng chữ có sức mạnh tạo cảm hứng cực kỳ lớn cho những ai muốn vươn đến sự thành công và hạnh phúc trong cuộc sống. Đó không hản là cuộc sống giàu có bậc nhất của 1 doanh nhân thành đạt bởi mỗi người có mục tiêu và ước muốn khác nhau. Điều quan trọng là quyển sách đã truyền cảm hứng cho người đọc biết khai thác những tiềm năng và sức sống trong chính bản thân mình để thấy cuộc đời thú vị và ngọt ngào biết bao!
5
66279
2015-02-23 10:59:32
--------------------------
69560
9952
- Khi đọc bất kì cuốn sách nào tôi cũng suy nghĩ: Mình có cần nó không? Thời điểm này đã đúng lúc chưa?
Và với 2 suy nghĩ đó tôi lựa chọn sách rất cẩn thận và cuốn 7 Chiến Lược Thịnh Vượng Và Hạnh Phúc này chính là 1 trong số những cuốn sách đó.
Xuyên suốt cuốn sách chính là câu chuyện kể về 1 con người rất nghèo nàn (cả về vật chất lẫn tinh thần) để rồi cần mẫn học hỏi, ứng dụng, sáng tạo những tri thức mới và rồi trở thành 1 người thành đạt.
Ngắn gọn, xúc tích và hàm chứa rất nhiều quy luật để Thịnh Vượng như đặt mục tiêu, tìm nhà cố vấn, hành động,... Và quy luật để có được Hạnh Phúc như kiểm soát thời gian, có nhiều bạn và sống đẹp.
Nhưng với 220 trang và chuyển tải 40 năm kinh nghiệm, sẽ là 1 thử thách để bạn có thể hiểu và vận dụng chúng đấy!
Chúc bạn may mắn và thành công.
4
81019
2013-04-16 21:21:13
--------------------------
