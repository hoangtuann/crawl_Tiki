442409
9645
Vẫn như những lần khác, cứ tưởng là tác phẩm văn học, không ngờ chỉ là bản tóm tắt mà thôi. tuy nhiên nếu như dùng để học từ vựng thì tóm tắt như vậy là rất phù hợp, có hình minh họa, không gây nhàm chán lại còn có kèm theo đĩa CD nữa. Nói chung là nếu dùng cho thiếu nhi thì rất phù hợp, nhưng mà mình cũng thích lắm. Phong cách giao hàng nhanh, đóng gói đẹp, kĩ càng, nhân viên giao hàng siêu dễ thương ( dù mình cứ hẹn tới hẹn lui rất nhiều lần ) :3
4
707800
2016-06-04 22:40:49
--------------------------
432601
9645
Sách nội dung sơ lược chưa đầy đủ, nhưng cũng đủ nắm được diễn biến chính cuẩ chuyện. Hình thức tạm ổn.
nếu sách có song ngữ thì rất tuyệt. 
sách hợp với người có trình độ tiếng anh trung cấp hơn là sơ cấp.
nhưng để luyện đọc thì rất hay.
mình mới mua mấy bộ sách happy reader về cho nhỏ nhà mình đọc, nhưng mình đã đọc trước rồi.
CD chất lượng khá tốt, đọc trên laptop hay copy vào máy điện thoại đều ok.
Chắc chắn nhỏ nhà mình sẽ rất thích. mình sẽ mua vài tập happy reader nữa!
3
1319762
2016-05-19 13:41:29
--------------------------
347679
9645
Nội dung của tác phẩm văn học nổi tiếng "Hiệp sĩ Đông Ki Sốt" thì đa phần mọi người đều đã biết rồi. Sách happy reader này cũng chỉ là một bản tóm tắt của chuyện mà thôi. Tuy nhiên, nội dung truyện được tóm tắt đủ ý, súch tích, cô đọng, lối hành văn cũng khá hay. Trong cuốn sách còn có chú thích nghĩa của một số từ mới cho người đọc, đĩa nghe có bao bì và bọc trong túi đựng đĩa dày dặn, chắc chắn. Bìa sách và chất lượng giấy cũng rất tốt. Giọng đọc trong đĩa nghe cũng khá chuẩn, từ từ, truyền cảm, hỗ trợ tốt cho cả người lớn và trẻ em nếu có nhu cầu luyện nghe nói
4
678687
2015-12-04 20:58:25
--------------------------
304387
9645
Mình đặt mua quyển sách Happy Reader - Hiệp sĩ Đông Ki Sốt - Kèm 1 CD, với mục đích chính là cho bé nhà mình luyện tiếng anh đẻ việc học bớt đi sự nhàm chán nên mình thương tìm trên Tiki những quyển sách hay để mua cho con. Quyển sách này được bọc một lớp bao kiếng bên ngoài rất cẩn thận, dĩa CD nghe rất rõ, các đoạn văn viết đơn giản dễ hiêu, phía sau mỗi trang ddeeefu có ghi chú thêm phần từ vựng giúp việc học của bé dễ dàng hơn. Mình rất hài lòng về sản phẩm này
5
769621
2015-09-16 13:08:43
--------------------------
155507
9645
Hiệp sĩ Đông Ki Sốt là tác phẩm được trích đoạn dạy trong sách giáo khoa Việt Nam. Với việc ban đầu mình đọc từ sách giáo khoa thì nhân vật hiệp sĩ hiện lên với vẻ ngớ ngẩn và tâm hồn khác người. Nhưng sau khi đến với cuốn sách phiên bản đầy đủ như thế này thì mình cứ như được hé lộ từng chi tiết ẩn ý đằng sau về những con người khác trong cùng một xã hội lúc ấy, những hoàn cảnh đã tạo nên người hiệp sĩ kia. Qua những tuyến nhân vật phụ, mình bắt đầu hình thành nên sự cảm thông cho nhân vật Đông Ki Sốt. Không ít thì nhiều, tác phẩm đã mang giá trị nhân văn sâu sắc. Một điều đáng lưu ý khác là mình học thêm được rất nhiều từ vựng mới trong phiên bản tiếng anh này. Chất liệu giấy bên trong rất tốt và cả những dòng để ghi chú trong từng trang thật sự rất hữu ích.
4
366024
2015-02-01 10:32:37
--------------------------
