446747
9583
Đầu tiên phải nói đến hình thức của sách: bìa cứng rất đẹp, giấy trắng, dầy, nhìn như 1 cuốn sổ tay vậy
Về nội dung: Thật sự là khi đặt mua cuốn này mình đã kì vọng rất nhiều, nhưng khi mở ra mới biết là những thuật ngữ từ năm 2004. Hơi cũ so với hiện tại nhưng cũng giúp ích mình khá nhiều. Tuy nhiên, sách cần cập nhật thêm những thuật ngữ mới, vì có nhiều từ mình dò mà không có.
Nói chung, sách có đầy đủ những thuật ngữ cơ bản, phù hợp cho những bạn chuyên ngành tài chính thị trường chứng khoán.
3
300875
2016-06-12 20:50:49
--------------------------
412003
9583
Về hình thức, quyển từ điển này được in bằng giấy mỏng, bìa cứng cáp và mực in rõ ràng. Nhìn chung là ổn! Về nội dung, sách cũng tương tự nhiều loại từ điển khác, trình bày theo kiểu Anh-Anh-Việt với đầy đủ ví dụ minh hoạ. Các từ đơn và thậm chí là các cụm từ về chứng khoán được diễn giải song ngữ nên rất dễ hiểu. Thêm nữa là sách xuất bản từ năm 2004, cũng khá lâu rồi nên giá cũng mềm mặc dù chất lượng thì rất ổn cho bất kì thời đại nào!
4
66279
2016-04-06 22:15:53
--------------------------
399076
9583
Từ Điển Thuật Ngữ Thị Trường Chứng Khoán - Anh-Anh-Việt là một trong những cuốn sách tài liệu không nhiều ở trên thị trường nói về chứng khoán. Sách trình bày khoa học, Theo thứ tự: Từ tiếng Anh - Từ tiếng Việt tương đương - Giải thích từ bằng tiếng việt - Giải thích từ bằng tiếng Anh. Sách xuất bản 12 năm về trước, tức là năm 2004. thì cũng có phần bên trong sách bị ố ngả màu vàng, bìa sách cứng giữ cho sách được bền . Sách phù hợp cho những bạn nào học, tra cứu về kinh tế.
5
854660
2016-03-17 09:52:22
--------------------------
293475
9583
Tôi tìm mua cuốn sách chỉ vì đang cần nghiên cứu chuyên ngành kinh tếc cũng như ôn lại những kiến thức từ lâu đã học. 
Sách được dịch vụ Bookcare của tiki đóng gói rất kỹ và đẹp. 
Còn về nội dung, tuy đã là sách xuất bản đã lâu nhưng vẫn hữu ích với tôi. Các từ ngữ trong quyển sách ngoài được dịch ra nghĩa tiếng việt rõ ràng và còn được định nghĩa giúp cho việc tra cứu từ được hiểu rõ ràng hơn. Sách không những giúp ích cho những người tìm hiểu về thị trường chứng khoán mà còn có thể giúp cho sinh viên kinh tế hay những người thuộc khối ngành kinh tế khác như phân tích tài chính, kế hoạch đầu tư,v.v....
4
358906
2015-09-08 13:12:29
--------------------------
256182
9583
Quyển sách này khá phù hợp với người mới làm quen với tài chính, chứng khoán. Có giải thích tiếng Việt, tiếng Anh. Điểm cộng là hình thức ổn, bìa sách rất chắc chắn, sách vừa phải, không quá cồng kềnh nên tiện mang đi học hoặc mang đi thư viện hoặc mang theo người để tra cứu. Tuy nhiên, chất lượng giấy thì mình chưa được hài lòng lắm, nếu chất lượng giấy tốt hơn thì chắc chắn là mình sẽ cho tới tận năm sao liền. Nhưng vì chất lượng giấy nên chỉ được 4 sao thôi nhé
4
127881
2015-08-06 17:23:58
--------------------------
248495
9583
Ban đàu mình rất hí hửng vì nhận được quyển đúng chuyên ngành đang cần nghiên cứu..Nhưng mà khi mở tấm ni long bọc bên ngoài sách thì thấy sách đã xuất bản tự bao giờ 2004 .Mình không biết là thuật ngữ về chứng khoán từ 2004 đến 2015 có thêm hay thay đổi gì không .Nhưng mà nhận được cuốn sách quá lâu so với hiện tại thì mình không hài  lòng chút nào. Đặc biệt là trên Tiki của mình ghi là xuất bản 01/01/2012 .So với giá cả thì cũng quá đắt so với ban đầu . 
2
578375
2015-07-31 10:01:51
--------------------------
168548
9583
Ấn tượng ban đầu của mình về quyển từ điển thị trường chứng khoán này là với giá rẻ vậy mà được làm bìa cứng nhìn rất đẹp và chắn chắc, nội dung được in ấn trên giấy trắng rất rõ ràng. Về phần nội dung, quyển từ điển này ngoài chức năng giúp người sử dụng có thể tra từ từ tiếng anh sang tiếng việt đồng thời còn làm rõ nghĩa của từ đó giúp người sử dụng có thể hiểu sâu hơn. Ngoài phần ngữ nghĩa, một số từ còn được minh họa bằng hình ảnh nhìn rất sống động. Quyển sách này thực sự hữu ích cho các bạn sinh viên ngành tài chính, ngành chứng khoán cũng như những người làm việc có liên quan đến thị trường chứng khoán.
5
578058
2015-03-16 20:59:14
--------------------------
161360
9583
Đây là một cuốn từ điển thực sự cần thiết cho những ai đang nghiên cứu hoặc thường xuyên tiếp xúc những tài liệu tiếng anh về thị trường chứng khoán nói riêng và thị trường tài chính nói chung. Các từ ngữ trong quyển sách ngoài được dịch ra nghĩa tiếng việc còn được định nghĩa một cách rõ ràng giúp cho người tra cứu từ có thể hiểu kỹ hơn. Sách không những được dùng cho những người tìm hiểu về thị trường chứng khoán mà còn có thể dùng cho những người thuộc khối ngân hàng, phân tích tài chính... Một quyển từ điển nên có.
4
356759
2015-02-27 16:18:16
--------------------------
