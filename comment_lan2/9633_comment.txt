451677
9633
Thật sự mà nói tập truyện này cũng có nhiều câu chuyện buồn ,mang lại cảm giác tiếc nuối cho người đọc.Mình sinh ra là một đứa trẻ có đầy đủ cha mẹ, và cũng lớn lên trong sự đủ đầy về vật chất. Có thể mình sẽ không hiểu được hết nỗi đau mà những đứa trẻ mồ côi phải chịu đựng, hoặc bị cha mẹ ruồng bỏ,hoặc không có khả năng chăm sóc, hoặc phải đi làm con nuôi, nhưng, qua tập sách này của hạt giống tâm hồn, phần nào, mình cũng hiểu được rằng, những người con nuôi, vẫn luôn xứng đáng nhận được tình yêu thương của cha mẹ, và những người nhận con nuôi, đứa con không cùng huyết thống của mình, cũng vẫn mang trong mình tình yêu thương vô bờ bến, của bản năng làm cha mẹ, và bản năng của đạo lí làm người. Thật xúc động biết bao,khi thế giới xung quanh ta thực sự vẫn vô cùng tốt đẹp
5
1313025
2016-06-19 23:24:01
--------------------------
359628
9633
Có ý nghĩa mang lại sức mạnh tâm hồn từ bên trong , phát triển bền vững , lâu dài từ suy nghĩ xa xôi bởi tác giả thông suốt tận tường bản chất của con người đối mặt với xuyên thường , có khi tỏ rõ cho người đọc khả quan của tính bền vững tâm lý , ngoại cảnh được mở lối , câu từ trong sách khá quen thuộc dẫn đến lý tưởng thường nhật được phân tích dễ dàng đi vào thói quen tạo cho người đọc hấp dẫn kỳ lạ , cần được bồi dưỡng trong tâm trí .
4
402468
2015-12-27 16:00:44
--------------------------
138054
9633
"Những bờ vai nương tựa" hãy là bờ vai của những người khác, để khi khó khăn, ta còn có bờ vai để dựa vào. Cuốn sách như một triết lý sâu sắc về tình người, tình đời,.. về sự cho và nhận. Rằng bạn sẽ không cảm thấy hạnh phúc khi cứ mãi cho đi, hoặc mãi nhận lại. Có những người cha mẹ không có con, họ khao khát một đứa con, họ ban cho những đứa trẻ mồ côi một hạnh phúc được gọi cha mẹ, họ cũng được nhận lại cái đối xử với cha mẹ của những người con đang khát khao hạnh phúc. Ấy là họ đã tìm được hạnh phúc cho riêng mình và ban cho người khác hạnh phúc. Cuốn sách có ý nghĩa nhân văn sâu sắc với những câu chuyện cảm động, thấm đẫm tình đời, tình người.
5
414919
2014-11-30 15:14:53
--------------------------
56355
9633
Những mẩu chuyện có thật được kể lại với giọng văn bình dị, nhẹ nhàng, đôi khi hóm hỉnh nhưng khiến người đọc không khỏi xúc động trước những trớ trêu của số phận. Không ai được tự chọn cho mình 1 cuộc đời hoàn hảo, nhưng những mảnh đời không hoàn hảo lại có thể tìm đến với nhau và lấp đầy những khiếm khuyết đó. Những cặp vợ chồng khao khát 1 đứa con, những đứa trẻ sinh ra khao khát được gọi 1 tiếng mẹ, tiếng cha...họ đã gặp nhau, dù tình cờ, hữu ý hay thậm chí là gặp trong những giấc mơ, họ là những mảnh ghép không hoàn hảo được số phận đem đến với nhau và cùng tạo ra 1 bức tranh hoàn hảo về tình mẫu tử, lòng bao dung và niềm tin về hạnh phúc.
4
63189
2013-01-21 09:01:45
--------------------------
