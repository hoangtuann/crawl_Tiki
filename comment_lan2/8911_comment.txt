279285
8911
Bộ phim cùng tên từng làm tôi mê mẩn, nên khi mua được bộ sách " ngôi nhà nhỏ trên thảo nguyên", tôi rất vui như lại được tìm về với tuổi thơ của mình. Tập một kể về gia đình Ingalls những ngày sống trong Đại Ngàn ở miền Wisconsin. Đối với Laura và Mary, đó là những tháng ngày vô cùng hạnh phúc, được chơi giữa trời tuyết, được học cách làm kẹo đường, được tham gia vũ hội, được nghe kể những câu chuyện kỳ thú,... Lối viết nhẹ nhàng bình dị nhưng lại mô tả một cách chân thực, sống động cuộc sống của một gia đình nông dân ở miền tây Hoa Kỳ thời ấy. Quyển sách như một bức tranh đầy sắc màu lung linh. 
5
109583
2015-08-26 17:20:58
--------------------------
108115
8911
Đây là tập đầu tiên của series "Ngôi nhà nhỏ trên thảo nguyên". Cuốn sách tuy chỉ mỏng thôi nhưng lại cho ta biết rất nhiều điều mới mẽ về văn hóa Mỹ, về những con người bình dị trong một gia đình. Sự nghịch ngợm của 2 chị em Laura và Mary nhưng luôn biết nghe lời, một người cha tâm lý và mạnh mẽ, một người mẹ ân cần và đảm đang luôn xuyên suốt câu chuyện. Cuốn sách này rất phù hợp để đọc cho bé nghe, vì nó chứa đựng những điều rất thú vị, bên cạnh đó còn có những bài học sâu sắc về sự nhường nhịn, vâng lời, yêu thương... Bạn nên đọc thử!!!
5
194496
2014-03-15 00:48:10
--------------------------
95531
8911
"Ngôi Nhà Nhỏ Trên Thảo Nguyên " là một trong những series sách mình yêu thích nhất. Câu chuyện về một gia đình bình thường với lời văn giản dị nhưng lại lay động tâm can sâu sắc bởi những tình cảm thiêng liêng trong ấy: tình vợ chồng, tình cha con, tình mẹ con, tình chị em và cả tình yêu gắn bó với mái nhà thân thương, với từng con vật nuôi, với mảnh đất quê hương. Hơn nữa qua đó mình còn như đươc chứng kiến tận mắt bức tranh của nước Mỹ những năm cuối 1870 với đầy đủ thay đổi và biến động. Trong cái xã hội xô bồ vội vã, những quyển sách như "Ngôi Nhà Nhỏ Trên Thảo Nguyên " là cốc nước ấm đánh thức ký ức ấu thơ của mọi người. Mình chờ mong những tập còn lại vô cùng!
 Lần xuất bản này Kim Đồng đã cho ra mắt ấn phẩm có giấy đẹp, sờ vào rất thích. Mình còn đặc biệt hài lòng với bộ bìa được sử dụng lại từ ấn bản đầu bên Mỹ
4
167283
2013-09-27 09:24:15
--------------------------
92929
8911
Hồi nhỏ mình đã rất say mê khi xem bộ phim "Ngôi nhà nhỏ trên thảo nguyên", và khi lớn, những thước phim "cũ mà mới" ấy vẫn có một sức hút kỳ lạ đối với mình. Nên mình thực sự rất vui khi nguyên tác văn học của bộ phim này được xuất bản tại Việt Nam. Đây là một tác phẩm rất hay và thú vị, từng câu chuyện nhỏ hợp lại thành một câu chuyện lớn, vẽ nên bức tranh về một vùng thảo nguyên xanh tươi và xinh đẹp, về cuộc sống luôn tràn đầy tình yêu thương và sự sẻ chia. Tác giả Laura Ingalls Wilder viết về những câu chuyện thơ ấu của chính bà, từng hình ảnh, từng câu văn đều thấm đượm những cảm xúc chân thành mà bà dành cho gia đình, cho những người thân yêu, cho quê hương mình. Đây thực sự là một tác phẩm kinh điển của văn học thiếu nhi, nhưng trên hết, nó là một bài ca về tình yêu thương mà ai cũng muốn đắm chìm vào trong ấy.
4
109067
2013-08-30 13:16:26
--------------------------
90355
8911
Đến hôm nay mình mới biết tiki có bộ truyện này thì lại "hết hàng"
Mình đã đọc đi đọc lại series NNNTTN tới lần thứ N trên kindle rồi mà vẫn còn say mê!!!
Khi thì ở cánh rừng bát ngát của Big wood, lúc ở trên thảo nguyên mênh mông, năm khác lại là bên dòng suối Plum, trong thị trấn...cô bé Laura dần dần lớn lên với cuộc sống dường như là ở mọi nơi của nước Mỹ.
Ở những nơi hoang vu ấy, họ làm nhà, trồng trọt, săn thú...và luôn luôn trong mỗi tập truyện đều có đủ mùa nắng cháy da, mùa đông khắc nghiệt, và những mùa Giáng Sinh ấm áp và luôn luôn đầy ngạc nhiên.

Cứ như thế, Laura cùng gia đình mình bôn ba vất vả nhưng hạnh phúc và yêu thương nhau.

Rất mong NXB Kim Đồng xuất bản đủ bộ để mình còn rinh về đọc lại, dù sao thì đọc sách giấy vẫn thích hơn nhiều!
5
36188
2013-08-09 16:27:17
--------------------------
