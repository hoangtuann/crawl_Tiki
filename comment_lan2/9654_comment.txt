467378
9654
Cuốn sách mang những câu chuyện ngắn nhưng nhẹ nhàng về đêm Noel, giải thích nguồn gốc của những bài hát giáng sinh, sự xuất hiện của máng cỏ, cây thông Noel. Mỗi câu chuyện mang một nội dung, ý nghĩa khác nhau nhưng đều ngắn gọn và hấp dẫn. Lời văn nhẹ nhàng truyền cảm, ngọt ngào mang lại nhiều xúc cảm cho người đọc. Chữ viết rõ ràng, trình bày minh họa cẩn thận trên nền giấy trắng; bìa sách đơn giản nhưng hài hòa, Tiki bọc sách cũng rất cẩn thận. Mình cảm thấy khá hài lòng về quyển sách này.
4
648692
2016-07-03 19:35:39
--------------------------
332252
9654
Mình mua cuốn sách này trong một lần đi hội sách ở trường. Cuốn sách bao gồm những kiến thức ngắn gọn về nguồn gốc Giáng Sinh cũng như phong tục của các nước vào ngày này. Thêm nữa, sách còn mang đến cho người đọc những giây phút thật bình yên với các truyện ngắn ý nghĩa về Giáng Sinh. 
Về cách trình bày, mình cũng cho điểm tuyệt đối vì sách trình bày dễ theo dõi cùng với những hình minh họa rất dễ thương :) Giấy mịn, sờ vào cảm giác rất thích. Trang bìa được trang trí với những hình ảnh đặc trưng của Giáng Sinh. Cuốn sách thật sự là món quà ý nghĩa cho mỗi người :)
5
252020
2015-11-05 22:40:54
--------------------------
