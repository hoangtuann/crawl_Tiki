452343
9462
Cuốn sách này mặc dù được viết bởi tác giả nước ngoài, các cuộc khảo sát nêu ra trong sách đều được thực hiện với những người đàn ông nước ngoài, nhưng tôi vẫn thấy có nhiều điểm đúng đối với đàn ông Việt (ít nhất là với ông xã tôi ^^). Cuốn sách khiến tôi thay đổi một số suy nghĩ và cách hành xử với đàn ông. Cảm thấy yêu chồng hơn và không còn khó chịu với những tính cách của anh mà mình không thích, bởi vì giờ mới biết à hóa ra đấy là tính cách chung của đàn ông rồi. Một cuốn sách bổ ích!
4
689454
2016-06-20 16:19:01
--------------------------
389059
9462
Mặc dù tựa sách chỉ dành cho phụ nữ nhưng mình thấy cuốn sách này hữu ích cho cả nam lẫn nữ. Bởi nhiều khi chính bản thân nam giới cũng không hiểu bản thân mình nghĩ gì. Các cụ bảo biết địch biết ta trăm trận trăm thắng nên cuốn này đúng với cả nam lẫn nữ.
Bên cạnh đó khác với những cuốn sách khác là cuốn sách này phân tích dựa trên những thông tin thu thập thực tế nên có tính áp dụng cao chứ không dựa trên các thông tin cá nhân chủ quan của 1 tác giả.
4
409107
2016-03-01 10:54:22
--------------------------
58898
9462
Điều làm mình thích thú ở tựa sách này là mặc dù tiêu đề là "Bí ẩn nam giới", nhưng trên tiêu đề sách có dòng chữ nhỏ 'For women only", thường thì những ta sách nói về nam thì nên để nam giới đọc, mình nghĩ là cuốn sách này đề cập đến nam giới dưới góc nhìn của một người thuyết minh đễ phái nữ hiểu biết hơn về phái nam.  Dừng nghĩ nam giới là 'giống người vô tri', chỉ khi này cầm sách lên, hay tiếp xúc với nam giới, mới thấy nam giới mình thật là những 'sinh vật yếu mềm' nhưng khó bộc lộ hơn thôi :)
5
38610
2013-02-08 08:33:31
--------------------------
