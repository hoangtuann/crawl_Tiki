220611
9919
Vị Tái là một tác giả xa lạ với tôi, nhưng nội dung và cốt truyện chính là thứ dẫn tôi đến với "Trách em thật quá xinh". Với tôi, đây là dường như là một câu chuyện đáng để đọc, đáng để suy ngẫm, vì trong mỗi người chúng ta, đều sẽ có những phút giây bồng bột và làm những chuyện hoang đường, quan trọng là chúng ta biết đâu là điểm dừng, và biết ngã chỗ nào thì đứng lên tại chỗ ấy.

Ngày trẻ, Hướng Vãn và Mạc Bắc đều là những đứa con bị tổn thương, vì gia đình, vì quan hệ của người bề trên, họ đã làm ra những sự việc tưởng chừng như chỉ cần bước thêm một bước nữa là không thể quay đầu, nhưng cũng nhờ những nỗi đau ấy mà họ mới gặp được nhau, tuy chẳng có quá trình nhưng đã có một kết quả, đó chính là Mạc Phi.

Tôi rất thích cậu bé này, trong cuốn sách luôn khẳng định rất nhiều lần, cậu bé có những tư duy rất người lớn, đó chính là do môi trường trưởng thành của mình - gia đình đơn thân, có thể mọi người đều cho tác giả có hơi làm quá, nhưng trong đời sống của chúng ta, vốn dĩ cũng có rất nhiều đứa trẻ như thế, có chăng là nền văn hóa giữa các nước khác nhau nên chúng sẽ biểu lộ theo những cách khác nhau.

Chuyện tình của Hướng Vãn và Mạc Bắc cũng nhờ Mạc Phi mà đâm chồi, nảy lộc, đi theo một trình tự rất tự nhiên và nhẹ nhàng. Tôi thực sự cảm thấy được sự kiên trì và tình yêu của Mạc Bắc, anh đã cố gắng hết sức vì con trai và người phụ nữ của mình, người đàn ông như thế, dẫu ngày trước có là sai lầm cũng đều có thể thứ tha.

Ngoài ra, câu chuyện còn xoay quanh nghề nghiệp của Hướng Vãn trong giới showbiz phức tạp, từ đó cho thấy một Hướng Vãn mạnh mẽ và khôn ngoan, nhưng cũng tiếc cho tình bạn của Hướng Vãn và Quản Huyền, đã chấm dứt trong vòng xoay đen tối kia. Với tôi, cuốn sách này rất đáng để bạn xem đấy!
4
34656
2015-07-02 19:52:48
--------------------------
208976
9919
Trong tập 1, giới showbiz với nhiều mặt trái, nhiều góc khuất đã được phô bày chân thực và nhiều lúc khiến người đọc xót xa, có lúc mình đã sợ rằng truyện này sẽ không có kết thúc tốt đẹp, nhưng thật may vì cái kết vẫn là viên mãn, một cái kết đẹp cho cả Mạc Bắc, Hướng Vãn và Mạc Phi. Trong cả bộ truyện này, mình khâm phục nhất là Hướng Vãn, khi mà cô đủ mạnh mẽ, bản lĩnh để bỏ lại quá khứ đằng sau lưng và đứng lên. Cậu bé Mạc Phi cũng cực kỳ dễ thương, thông minh, cute hết biết .
3
393748
2015-06-16 16:34:41
--------------------------
175510
9919
Cả tập 2 là quá trình Mạc Bắc kiên trì theo đuổi Mạc Hướng Vãn, sưởi ấm lại con tim chai sạn của cô. Điều này một phần có công lớn của bé Mạc Phi, cậu bé siêu siêu dễ thương, vì Hướng Vãn vô cùng yêu con, và luôn suy nghĩ cho con. Mình đã rất lo lắng khi quá khứ của Hướng Vãn bị phơi bày, nhưng Mạc Bắc đã chứng tỏ được tình yêu và niềm tin của anh dành cho Hướng Vãn, bảo vệ được mẹ con Hướng Vãn bình yên trong vòng tay anh. Một kết thúc mỹ mãn cho gia đình nhỏ của 3 người.
5
57459
2015-03-30 13:24:02
--------------------------
164901
9919
Đọc xong tập này tôi thầm thở phào nhẹ nhõm. Nhẹ nhõm vì cuối cùng gia đình nhỏ 3 người ấy cũng được đoàn tụ 1 cách hạnh phúc. Cha con nhận nhau, còn hai người Hướng vãn và Mạc Bắc, họ bắt đầu 1 khởi đầu mới với tình yêu làm nền tảng, không còn là sự vui chơi qua đường như trước đây nữa. Quả thực cậu bé cũng đã góp phần làm một chiếc cầu nối để ba mẹ mình bắt đầu với nhau. Những toan tính trong giới showbiz vẫn còn đó, nhưng nó sẽ xa vời đối với Hướng vãn và hạnh phúc của cô.
4
198257
2015-03-09 08:04:40
--------------------------
162788
9919
Tập này mình cứ nghĩ là sẽ chán vì tình tiết đa số cũng không còn gì mà khai thác, nhưng thực không ngờ cũng đọc hết liền 1 mạch không ngừng lại được luôn. MHV từ một cô gái ăn chơi lêu lổng nay đã trở thành 1 bà mẹ đơn thân tốt tính, dạy con rất hay và chuẩn, mình rất thích cách cô dạy con, vừa nhu vừa cương, cứng rắn có mà mềm mỏng cũng có. Cuối cùng gia đình 3 người được đoàn tụ một cách hạnh phúc và vui vẻ, thật là 1 happy ending.
4
469377
2015-03-03 10:23:37
--------------------------
155091
9919
Lần đầu tiên tôi đọc truyện lấy bối cảnh là nền giải trí nên thấy cũng khá là mới lạ. Truyện có nhiều tình tiết và nút thắt nên dù hai tập tôi vẫn thấy không dài. Chỉ có điều chuyển biến tính cách của nhân vật Mạc Hướng Vãn dường như chưa được hợp lý cho lắm? Trong tập 1 cô có vẻ còn rất trẻ con, ăn chơi lêu lổng là nhiều, nhưng khi đã làm bà mẹ đơn thân thì cô lại quá hoàn hảo. Cái cách cô dạy con có bà mẹ nào mà không ngưỡng mộ cơ chứ @@ Nam chính cũng vậy, anh quả có quá nhiều tính tốt. Hai người gộp lại là thành một cặp hoàn hảo luôn.
3
126898
2015-01-31 08:42:12
--------------------------
150478
9919
Đọc xong tập một , tôi háo hức đọc tiếp tập 2 . Tác giả không hề khiến một "con mọt sách" như tôi thất vọng . Tôi đã đọc rất nhiều bộ truyện ngôn tình nói về giới showbiz hay vấp ngã của tuổi trẻ , tuy nhiên "trách em thật quá xinh" lại đem đến cho tôi rất nhiều cảm xúc khác nhau . Câu chuyện tuy nhẹ nhàng , không có quá nhiều cao trào hay gay cấn , càng không có các màn tranh chấp với người thứ ba hay tài sản . Mạc Hướng Vãn , một bà mẹ đơn thân giàu nghị lực khiến tôi nghĩ đến một câu nói : một đứa trẻ sơ sinh và một người phụ nữ mất chồng , tưởng chừng người mẹ là chỗ dựa cho đứa bé , nhưng thực ra chính đứa bé còn đỏ hỏn ấy mới chính là chỗ dựa vững vàng cho người mẹ . Mạc Phi chính là chỗ dựa vững vàng của Hướng Vãn , cũng có thể coi cậu bé Mạc Phi chính là "điểm tựa" mà Mạc Hướng Vãn cần để "nâng cả trái đất" . Mạc Bắc , người đã cho Mạc Phi một nửa sinh mệnh để có mặt trên đời . Anh là một người đàn ông có trách nhiệm , lương thiện và "mặt dày" . Hình tượng của anh khiến tôi nhớ đến một câu viết mà tôi đã từng đọc trong một cuốn truyện khác "muốn theo đuổi được một cô gái , điều kiện tiên quyết là da mặt phải đủ dày" . Mỗi nhân vật Vị Tái viết đều nổi bật theo cách nào đó . Không kịch tính , không phô trương , không phóng đại hóa hình tượng nhân vật , nhưng theo tôi Vị Tái đã rất thành công khi viết bộ truyện này . Một bộ truyện nhẹ nhàng , khá sát với thực tế , không có vẻ hào nhoáng  , các nhân vật rất gần gũi với thực tế . Chắc hẳn khi đọc xong bộ truyện này , những cô gái còn độc thân như tôi chắc chắn sẽ có mong ước giản dị : có gia đình êm ấm , sống cuộc sống : tháng ngày tĩnh lặng , kiếp này bình yên thế là đủ rồi . Tác giả đưa ra thông điệp rất ý nghĩa : quay đầu là bờ .
5
301782
2015-01-16 20:48:33
--------------------------
116248
9919
Mạc Hướng Vãn và Mạc Bắc, không ít thì nhiều đều cho mình thấy được nét tuổi trẻ của giới trẻ hôm nay,mai sau và cả trước đây. Từ những cô bé, cậu bé ngoan ngoãn trở nên hư hỏng, sa vào con đường ăn chơi lệch lạc, nhưng cũng rất may họ đã kịp quay lại , một lần nữa bước đi trên con đường đúng đắn. Ai trong chúng ta tuổi trẻ chưa từng nổi loạn? Dù như thế nào đi chăng nữa thì nổi loạn cũng có nguyên nhân của nó. Truyện làm mình rút ra một điều, đằng sau mỗi người đều là một câu chuyện, người lớn chúng ta luôn luôn nhìn vào những đứa trẻ hư đó mà răn dạy, trách móc chúng nhưng rất ít người hỏi nó nguyên nhân vì sao? Không phải ai cũng may mắn như Mạc Bắc và Hướng Vãn có cơ hội làm lại từ đầu, đi sai một bước cả đời phải sống trong hậu quả đáng sợ. Ngoài những mặt trái của giới showbiz thì cuốn sách còn cho mình thấy được sự quan tâm, thấu hiểu, giáo dục đúng cách trẻ em và cả trẻ vị thành niên của cha mẹ, người thân là cần thiết đến dường nào.
5
39798
2014-07-06 12:41:13
--------------------------
