475491
8502
Đối với mình có lẽ câu truyện này nó khá là nhẹ nhàng, cứ bình bình như thế. có cái vị ngọt, vị chát vị chua. Truyện có lẽ chỉ dành cho những cô gái nhẹ nhàng, còn đối với những cô nàng cá tính mạnh thì có lẽ sẽ cảm thấy nhàm chán. Là một truyện đọc để giải trí để ngẫm nghĩ khá hay, tuy đôi lúc không xảy ra những kịch tính cao trào đỉnh điểm nhưng cũng đủ để tạo ấn tượng tốt. Nói chung một quyển truyện dành cho những cô nàng nhẹ nhàng, thích những điều bình dị xảy ra mà thôi :)
3
1276139
2016-07-14 21:17:50
--------------------------
436841
8502
Mua cuốn sách này lúc đang được giảm giá, giá trên tiki khá tốt so với giá bìa. Cá nhân mình rất thích bìa sách này, màu xanh lá cây nhẹ nhàng, với lại giấy in cũng dày dặn nữa. Tình tiết câu chuyện nhẹ nhàng, lời văn mạch lạc. Truyện không có quá nhiều nhân vật phụ mà thay vào đó tập trung chủ yếu vào hai nhân vật nam nữ chính, Chu Lạc và Đại Đổng. Mặc dù vậy nhưng mình vẫn thích nhân vật nam phụ Diệp Minh Lỗi hơn, chỉ tiếc là phân đoạn dành cho anh này hơi ít.
3
701535
2016-05-27 11:42:45
--------------------------
375089
8502
Tổng thể, đầu tiên bìa sách thiết kế không thu hút được người xem. 
Thứ 2 không có bookmark. Nếu nhận xét 1 cách toàn diện thì tác phẩm này chỉ tạm ổn. 1 câu chuyện nhẹ nhàng không hấp dẫn lắm. Nội dung bình thường nhưng cách hành van lưu loát, rõ rành, dứt khoát. Còn về các nhân vật chính mình mình không thể thích được nữ chính. Cô ấy lúc nào cũng có cái nhìn phiến diện với Minh Lỗi và thiên vị Đại Đổng. Nữ tính có 1 tính cách nhạt không thể thu hút người đọc. Chung qui theo mình đây chỉ là câu chuyện đọc cho vui k đủ kích thích người đọc
3
47916
2016-01-27 19:33:03
--------------------------
367060
8502
Một điểm cho thiết kế bì vì nhìn là có cảm giác lãng mạn tuy nhiên hình thực tế của trang bìa không sắc nét. Nội dung thì có vẻ mới mẻ vì nam chính vẫn đẹp trai, tài giỏi nhưng không giàu có như những truyện khác, thêm vào đó nam phụ thì hội tụ cả đẹp trai, tài giỏi và giàu có nhưng không ỷ thế, cậy quyền làm khó người khác, còn nữ chính thì do được gọi là thần đồng nên vì thế mà thể chất không theo kịp tài trí nên mới bị liệt vào hàng ế. Tuy nhiên tình huống để hai nhân vật chính xa nhau thì không hợp lý nên chỉ được bốn sao thôi.
4
583423
2016-01-11 13:00:53
--------------------------
360575
8502
Cảm giác nhẹ nhàng , lôi cuốn vào các khía cạnh của tính cách nhiều độc đáo , dễ thương của hoàn cảnh ra đời , mục đích và phong thái rõ ràng , chau truốt mỹ quan được khai thác làm toát lên khung cảnh hữu tình , bên cạnh đó là những câu chữ giản dị bình thường lại làm dấu ấn dành riêng cho người đọc theo dõi không ngừng nghỉ , bí mật theo dõi những đối lập cạnh tranh với tự nhiên mà tác giả tạo ra cho cuốn truyện , điều làm nên cái văn phong đầy lỗi lạc .
3
402468
2015-12-29 14:08:12
--------------------------
353956
8502
Duy chỉ một ngày mà mình đã "ngốn" hết một quyển :) , công nhận "Dựa vào hơi ấm của em" hay thật. Cái kết thật lãng mạng làm sao nhưng bản thân mình lại thấy tôi tôi cho Diệp Minh Lỡi sao sao á. Minh Lỗi có gì thua anh kia đâu : Giàu có, Đẹp trai có, Ga lăng có (khúc cuối anh cởi áo vest khoác lên cho Đồng Đan nhìn dễ thương dễ sợ). Nội tâm nhân vật này sâu lắm luôn.. Còn Chu Lạc sao lại nói là "chống ề", trai đẹp theo quá trời quá đất mà... Nhìn chung thì câu chuyện này đã để lại ấn tượng sâu sắc cho tôi về nội dung của câu chuyện lẫn cả về tác giả Cúc Tử nữa.
5
279073
2015-12-17 09:44:06
--------------------------
334273
8502
Mình chưa đọc truyện này nhưng khi đọc giới thiệu thì cảm thấy nó rất hay.
Một người con gái chỉ vì quá thông minh mà không thể kết bạn.Nhưng có ai đã nói rằng khi thượng đế đóng một cánh cửa thì sẽ mở cho bạn một cánh cửa sổ và Chu Lạc không chỉ được mở 1 cánh mà tới 2 cánh cửa sổ tràn đầy nắng ấm.Đó chính là sự bù đắp của ông trời dành cho Chu Lạc.Nhưng sẽ tốt hơn khi chỉ xuất hiện một người vì nó sẽ không khiến cho cô ấy phải đưa ra sự lựa chọn.
2
943875
2015-11-09 10:25:36
--------------------------
296425
8502
Sách có chất liệu giấy nặng ,đây là cuốn sách dày và dài!Trang bìa không được đẹp như trong hình!Nội dung không có gì đặt biệt!Mình không thích nội dung cho lắm vì nội dung không có gì là xuất sắc và hấp dẫn cả!Bình thường mình đã quen những nhân vật nam chính đẹp trai,giỏi giang,nhà giàu quen rồi nên cuốn sách này nhân vật  nam chính bình thường quá(thợ sửa xe)nên mình không thích đọc lắm!Nếu như nhân vật nam chính trong cuốn sách này xuất sắc thì cuốn sách này sẽ rất hay!Một cuốn sách có tình huống khá nhẹ nhàng!!!!!!
3
413637
2015-09-11 07:33:44
--------------------------
254039
8502
Đối với mình có lẽ câu truyện này nó khá là nhẹ nhàng, cứ bình bình như thế. có cái vị ngọt, vị chát vị chua. Truyện có lẽ chỉ dành cho những cô gái nhẹ nhàng, còn đối với những cô nàng cá tính mạnh thì có lẽ sẽ cảm thấy nhàm chán. Là một truyện đọc để giải trí để ngẫm nghĩ khá hay, tuy đôi lúc không xảy ra những kịch tính cao trào đỉnh điểm nhưng cũng đủ để tạo ấn tượng tốt. Nói chung một quyển truyện dành cho những cô nàng nhẹ nhàng, thích những điều bình dị xảy ra mà thôi :) 
3
311155
2015-08-04 22:58:14
--------------------------
241779
8502
Đây là một câu chuyện nhẹ nhàng , hơi hóm hỉnh nhưng cũng rất sâu sắc. Một cô gái vì xung quanh không một người làm bạn mà để khoảng khắc đẹp nhất đời trôi qua một cách vô vị, đến tới lúc đi làm thì cuộc sống của cô vẫn vô vị như vậy. Đến nỗi dù có đủ tố chất làm một người " dâu hiền vợ thảo" mà cuối cùng cô vẫn ế. Và rồi  tình yêu của chị ấy cũng đến, mà còn là đến nhanh như sao xẹt với nhiều tình huống rất hài. Truyện nhẹ nhàng, rất dễ thương, mình rất thích!
4
433184
2015-07-25 20:42:51
--------------------------
227148
8502
 Tớ từng đọc truyện này trên mạng trước đây nhưng dài quá nên bỏ ngang chừng. Cho tới khi xách nó về nhà, tớ mới thật sự thích nó. Chị nữ chính rất dễ thương, nam chính cũng rất  bá đạo, cũng bỏi vậy mà diễn biến khá nhanh, tới giờ tớ vẫn thắc mắc tình tiết nhanh như vậy mà cuốn sách sao lại dày đến thế. tớ cức kì thích khúc đầu, lúc chị tỏ tình với lại khúc nam phụ cảm thán giọng văn dí dỏm khiến tớ thấy thật gần gũi. Nếu có lí do tớ không thích chuyện này thì chắc có lẽ là do cái lúc ly hôn ấy, tớ nhìn không vừa mắt chút nào, khi anh quay về cũng thế, biết là ai trong hoàn cảnh đó đều nghi ngờ vợ mình nhưng sao anh lại mặt dày bám vào chị ấy như thế, vì vậy tớ khá thích cái tát của chị cho anh... dù sao anh cũng là tình yêu của chi cho nên mình không ghét anh lắm,..
4
429599
2015-07-13 16:09:41
--------------------------
224265
8502
Cuốn sách "Dựa vào hơi ấm của em" chiều ngang dọc hơi lớn nhỉ. :D Tuy nhiên về nội dung thì tương đối hay. Mình thích hình ảnh nhân vật nữ chính, trước hết đã hiện ra là một con người tài giỏi chứ không phải xinh đẹp. Cốt truyện hay, giọng văn nhẹ nhàng, nhiều cảm xúc và không thiếu yếu tố dí dỏm, đáng yêu! Tuy nhiên, tình tiết truyện hơi nhanh làm đọc giả hơi "choáng"...Câu chuyện tình yêu của nhân vật chính diễn ra "tốc độ" hơn những cuốn tiểu thuyết khác khá nhiều đấy! Nam chính mới đọc mình bị lẫn. hihi!
5
292005
2015-07-08 13:14:00
--------------------------
177054
8502
Đúng như lời giới thiệu, mới đầu đọc mình chả biết ai mới là nam chính nữa. Nam nữ chính yêu nhau hơi nhanh, kết hôn cũng nhanh, ly hôn lại càng nhanh. Mà cái lý do ly hôn của anh nam chính hơi bị bi kịch và gượng ép quá. Một điểm nữa là nữ chính hơi thiếu công bằng đối với nam phụ, tuy anh là một công tử nhà giàu kiêu ngạo, nhưng có thể nói tình cảm anh dành cho cô là thật lòng. Tuy nhiên, ngay từ đầu chị đã không để anh cùng với anh nam chính chung một điểm xuất phát. Đáng lý tác giả nên để nữ chính dằn xé nội tâm một chút, còn đằng này cô chưa bao giờ để Diệp Minh Lỗi có cơ hội cả.
3
57459
2015-04-02 13:49:45
--------------------------
153314
8502
Mình yêu thích lối viết và cách dẫn truyện của tác giả trong quyển Dựa Vào Hơi Ấm Của Em này! Vừa nhẹ nhàng lại vừa cảm xúc, cách dẫn dắt tự nhiên nên mình không cảm thấy chán mặc dù phần đầu của truyện khá dài và chưa nhấn mạnh vào điểm chính.
Điều mình thích là câu chuyện tình yêu giữa Lạc Lạc và 2 chàng mỹ nam xung quanh nàng. Câu chuyện vừa đẹp, vừa lãng mạn nhưng cũng cực kỳ hài hước. Nhưng cách "dàn xếp" của Cúc Tử có phần thiếu tinh tế :3 Theo mình thấy thì nên để cho Lạc Lạc phải phân vân giữa tình yêu với 2 chàng này như vậy thì mới tạo được sức hút và tò mò cho đọc giả. Đằng này, rõ ràng nàng giành tình yêu phần nhiều cho Đại Đổng. Nếu không có hạt sạn này có lẽ mình đã đánh giá rất cao ở tác phẩm này rồi :)
Với giọng vặn nhẹ nhàng, dí dỏm nhưng đầy cảm xúc, Cúc Tử thực sự đã phát họa nên một câu chuyện ngôn tình rất đẹp. Thêm vào đó là chất văn không hề hòa lẫn với tác giả nào khác trong dòng truyện ngôn tình này! Có thể đọc vào là nhận ra ngay :)
Một câu chuyện tình khá hay và nhiều điểm nhấn!
3
303773
2015-01-25 21:30:08
--------------------------
124551
8502
Khi đọc giới thiệu trên tiki mình đã nghĩ đây chắc sẽ là một cuốn truyện hài hước khi cô nàng thần đồng lại bị vào danh sách gái ế thế nhưng khi mình đọc thì lại thấy thất vọng vì truyện lan man, dài dòng, không có trọng tâm. Cô nàng 28 năm không một ai theo đuổi đột nhiên hoa đào lại nở rộ với 3 chàng trai công tử hào hoa đẹp trai như những mô típ truyện cũ vẫn thường gặp. Xoay quanh Diệp Minh Lỗi và Đại Đổng khiến Chu Lạc bị xoay như chong chóng và tận khi mình đọc đến gần những chương cuối vẫn chưa thể xác định được ai là nhân vật nam chính. Những tưởng sau khi ly hôn một cách vô lí với Đại Đổng, Chu Lạc sẽ có những chọn lựa khác, tìm được người đàn ông xứng với mình nhưng rồi cuối cùng cô vẫn chọn người chồng cũ, người đã không tin tưởng cô, đột ngột li hôn, mang đến cho cô những nỗi niềm đau lòng. Mình cảm thấy truyện quá dài dòng, những chương cuối chỉ kéo dài thêm truyện như không còn gì để nói. Tuy nhiên vẫn có giọng điệu hài hước, gây cười cùng với những tình tiết trẻ con ở cô gái đã 28 tuổi khiến truyện không đến nỗi quá nhàm chán. Cuốn này mình thật sự không thích lắm.
3
73178
2014-09-07 23:12:07
--------------------------
114713
8502
Dựa vào hơi ấm nơi em - rất dễ thương và ấm áp. Cuốn sách không có nội dung mới mẻ, cũng chẳng có cao trào kịch tính nhưng không hiểu sao mình lại rất thích. Nữ chính được xây dựng mình cho là hơi phi lý một chút nhưng nam chính lại rất dễ thương, anh ấy sẽ là bờ vai vững chắc cho nữ chính cả đời. Còn nam phụ Minh Lỗi thì mình lại thấy hơi tội một chút, vì cho dù có bản chất thương nhân nhưng quá thực anh ấy cũng có những tình cảm thật sự, nữ chính đã thiên vị Đại Đổng hơn nên không nhìn ra mặt tốt của anh ấy thôi
4
16417
2014-06-17 15:41:04
--------------------------
108728
8502
''Dựa vào hơi ấm của em'' thực sự không phải gu của mình, chỉ là rảnh rỗi đọc mà thôi. 
Thật ra, truyện không quá tệ, văn phong cũng có nét riêng, không giống như nhiều truyện ngôn tình Trung Quốc khác- chán, giống nhau và vừa đọc là biết trước kết thúc. Một điểm hay ở truyện là có rất nhiều kiến thức bên lề, được cung cấp dưới hình thức kể chuyện nên không chán. Môt câu chuyện nhẹ nhàng, dễ thương nhưng lại khiến mình hơi thất vọng. Phần giới thiệu làm cho mình tưởng sẽ có một cuộc chiến gay gắt giữa hai mỹ nam thế nhưng thật ra lại không phải vậy. Ngay từ đầu, rõ ràng nữ chính đã thiên vị cho Đại Đổng, nghĩ xấu về Diệp Minh Lỗi. Những câu chuyện như vầy, người kể hay sẽ khiến người đọc tò mò bằng cách làm cho nữ chính không biết chọn ai, tạo ra hình huống cân bằng giữa cả 2 đối thủ cùng một kết thúc bất ngờ chẳng hạn.
 Vậy nên, lí do mình thất vọng là vì mình mong chờ nhiều hơn ở truyện, mong chờ sự gay cấn, đấu tranh nội tâm trong lòng nữ chính.
3
106566
2014-03-22 05:42:48
--------------------------
98425
8502
Nếu nhận xét một cách toàn diện về tác phẩm này thì theo tôi nó không thật sự là quá hay nhưng nó có nét đặc sắc riêng, lôi cuốn. Tuy nội dung cũng bình thường nhưng khiến tôi xem mãi miết và không thể buông sách xuống được, bởi:
 - Văn phong lưu loát, viết rõ ràng, mạch lạc, dứt khoát.
- Ngôn ngữ hài hước, dí dỏm nhẹ nhàng.
-Nội dung của chương nào có cài đặt mục đích gì sẽ được giải quyết ngay trong chương đó chứ không lằng nhằng, dài dòng lôi thôi.
-Nội dung khép lại của một chương thì sẽ mở ra vấn đề cần giải quyết cho chương mới nên khiến người đọc không bị chán.
-Với lại, câu chuyện không chỉ chú mục chăm chăm vào tình yêu mà còn cung cấp kiến thức phổ thông cho đọc giả, giúp đọc gỉa mở mang kiến thức.
Bên cạnh những điểm hay thì tác phẩm còn có những mặt hạn chế. Đó là tâm lý của nhân vật nữ chính Lạc Lạc không phù hợp thực tế cho lắm. Tôi nghĩ có lẽ tác giả muốn viết  về một tình yêu không phải chỉ chú trọng vào nghề nghiệp, giàu có của con ngừơi ngay lần đầu biết nhau mà là dù là một thợ sửa xe bình thường nhưng chỉ cần nhân phẩm tốt thì cũng có được tình yêu chân thành. Nhưng tôi thấy tác giả cho nữ chính có cái nhìn phiến diện và quá khích về nam chính 2-Diệp Minh Lỗi. Theo tôi nghĩ, con người đôi lúc có cái nhìn không đúng về người nào đó qua lần đầu gặp mặt nhưng thời gian tiếp xúc lâu thì sẽ thay đổi cách nhìn khi biết về con người thật của họ. ở đây Lạc lạc đã quá khắc khe đối Minh Lỗi và hơi thiên vị cho Đại Đổng nên tôi thấy ở chỗ này của chuyện không phù hợp thực tế lắm.
4
180185
2013-11-04 09:15:42
--------------------------
