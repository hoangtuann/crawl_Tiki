552945
8780
Mình mua thử cho con quyển này và k thất vọng. Hình vẽ vô cùng đang yêu! Sẽ mua trọn bộ 5 quyển
5
753201
2017-03-24 14:00:55
--------------------------
489831
8780
Cuốn sách ehon này rất hay. Bé nhà mình đọc từ lúc 10 tháng tuổi. Khi đó bé mới chỉ biết giở trang sách. Giờ bé được 1 tuổi rồi. Bé rất thích thú với các hoạt động trong lúc tắm của các bạn cún, gà, khủng long và bạn Yu. Bé có thể bắt chước cách các bạn rửa tay, kì lưng, cọ chân và gội đầu. Sách ehon dạy trẻ một cách nhẹ nhàng việc đi tắm thì sẽ cần làm những gì. Khi trẻ tắm thì trẻ sẽ tự giác thực hiện theo một cách thích thú chứ không bị ép buộc chút nào
4
1299359
2016-11-10 10:07:53
--------------------------
471125
8780
Ở quyển Cùng chơi với bé- đi tắm thật thích, bé sẽ tìm hiểu về việc đi tắm thông qua hình ảnh bé Yu, bạn cún, bạn gà... đi tắm. Rửa chân ra sao nè, rửa tay ra sao nè, rửa mặt, gội đầu ra sao nè, rồi kỳ cọ thân thể như thế nào, rồi xịt nước , lấy khăn lau khắp mình cho sạch sẽ thơm tho nè. Qua hình ảnh như vậy, bé sẽ thấy việc đi tắm sẽ thích thú hơn và biết cách tắm sao cho sạch nữa. Sách này chắc chỉ thích hợp cho các bé từ tuổi rưỡi đến ba tuổi là cùng hà.
4
460351
2016-07-08 11:06:22
--------------------------
240375
8780
Cuốn ehon đáng yêu này vẽ các bước "đi tắm": Tắm này, gội đầu này, kì cọ này, lau người này...  Với các nhân vật đáng yêu, bạn gà Pi, bạn khủng long, bạn cún Koro và bé Yu, bé sẽ thấy đi tắm cũng thật vui. Bé sẽ nhớ đi tắm thì mình làm những gì này, sẽ thích thú bắt chước các bạn trong sách. Sách được thiết kế đặc biệt, mỗi lần bé lật giở trang sách sẽ thấy điều bất ngờ hay ho. Mình thấy kiểu sách này rất hấp dẫn các bé, vừa được xem hình vừa được thao tác tay mà.
5
82774
2015-07-24 15:57:21
--------------------------
236219
8780
Điểm nổi bật của sách là hiệu ứng lật mở khá thú vị và tạo sự tương tác trong khi kể giữa bố mẹ và con cái, hình ảnh màu sắc, mỗi trang chỉ có một hình thui. Quyển Cùng Chơi Với Bé - Đi Tắm Thật Thích! này sẽ dạy bé việc kỳ cọ lần lượt từng bộ phận khi tắm
Nội dung đơn giản không có gì đáng nói cả, nói chung thích hợp mua để làm trò chơi cho con mà đặc biệt là các bé dưới 2 tuổi mỗi tội mình thấy giá là hơi đắt
4
153008
2015-07-21 16:56:36
--------------------------
182103
8780
Tôi được biết đây là bộ truyện của Nhật và rất được các bé ở Nhật yêu thích. Truyện có hình vẽ to, dễ thương, nét vẽ đẹp, màu sắc hài hòa, thích hợp cho bé từ 1 tuổi trở lên.Truyện “ Đi tắm thật thích” dạy bé cách đi tắm, kỳ cọ, rửa mặt mũi, chân tay, gội đầu và lau người sau khi tắm xong.Cứ mỗi trang sẽ có một nhân vật xuất hiện. Truyện được thiết kế dưới dạng lật hình, chữ rất ít, giúp bé không bị phân tâm, chỉ chú ý vào hình và lời kể của bố mẹ.Bố mẹ hãy mua bộ sách này về để cùng chơi với bé nhé.
4
15022
2015-04-13 11:15:49
--------------------------
