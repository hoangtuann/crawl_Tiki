393811
6312
Mình mới nhận được quyển này, nhìn bên ngoài bìa cứng rất đẹp hoành tráng, nhưng khi mở ra mình hoàn toàn thất vọng bên trong của sách. Giấy bên trong rất mỏng, hình ảnh thì chỉ có 2 màu và mờ. Nhà nào có em bé cho xem thì không học được cách phân biệt màu sắc đâu.
Mình chưa đọc phần nội dung nên không nhận xét. Hy vọng nhà xuất bản coi lại chứ bìa thì hoành ráng ruột thì dở tệ thì làm cho người cầm hụt hẫng thật. Nếu có tái bản thì làm ảnh màu đẹp đẹp tí, giấy dầy hơn. Khách hàng sẵn lòng bỏ thêm tiền mua miễn sao sản phẩm mình đẹp, hữu ích cho mấy bé nhìn hình và kích thích niềm vui thú đọc sách của bé.
Thân!
Văn Huynh
2
1206792
2016-03-09 13:22:32
--------------------------
