332457
4385
Học thêm chuyên ngành luật mà nguồn sách tham khảo dạng bài tập như thế này rất ít! May mắn là tìm đc một quyển trong lúc lang thang trên Tiki. Hy vọng Tiki sẽ bổ sung thêm nhiều sách chuyên ngành Luật hơn!
Theo mình sách rất có ích cho mình trong học tập, vì thực tiễn công việc mình không chuyên về luật  nên cần các tình huống cụ thể như sách này! Cảm ơn tác giả, cảm ơn Tiki. Mình đang tiếp tục ngâm cứu, "luyện" xong sẽ nhận xét chi tiết hơn!
Điểm trừ nho nhỏ là ở chất liệu giấy in theo mình là khá tệ, sách tham khảo cần chất liệu tốt hơn vì cần lật đi lật lại nhiều lần, tra cứu thông tin điều khoản, ... mà giấy thế này thì nhanh hư lắm!
5
355461
2015-11-06 13:10:59
--------------------------
232992
4385
Cuốn sách bài tập luật tố tụng hình sự có nêu rõ chi tiết từng vụ án khác nhau, có sự phân tích rõ ràng và kết luận cụ thể từng tình huống trong vụ án. Đây là cuốn sách hay và có hàm lượng thông tin phong phú. Căn cứ vào những tình huống thực  tế, những câu hỏi đa dạng, đáp án ngắn gọn, rõ ràng, sách đáp ứng được nhu cầu của đối tượng nghiên cứu luật, đang học luật, giúp chúng ta có sự phân biệt tốt hơn về các khái niệm, các thủ tục, trình tự về tố tụng hình sự.
4
42133
2015-07-19 14:20:58
--------------------------
