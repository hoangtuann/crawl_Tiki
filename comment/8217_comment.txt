532781
8217
Đã đọc không biết bao nhiêu lần và chia sẻ cho bạn bè đọc ai cũng bảo hay lắm , tinh tế , lôi cuốn hấp dẫn
4
133327
2017-02-28 12:13:24
--------------------------
488963
8217
Mình mua sách được hơn 1 năm, nhưng mới đọc được 10 chương. Lúc đầu mua cứ nghĩ là của tác giả Nhật ai ngờ tác giả  là người Việt Nam. Xét về nội dung truyện thì quyển này pha trộn giữa ngôn tình, kiếm hiệp, bang phái,và hư cấu kinh khủng. 
Ôi đọc mà cảm thấy nhân vật chính đọc tung hô lên đến tận trời, được khen xinh đẹp đa mưu túc trí này nọ,... nhưng mà mình cảm thấy chả có gì là thông minh cả, vì các tình huống đưa vào vô duyên kinh khủng. Nếu mà truyện được đăng trên mạng chắc mình cũng chả đọc, mà nay lại phải mất tiền mua sách thế này cảm thấy tiếc tiền kinh khủng.
1
519650
2016-11-05 18:38:28
--------------------------
418864
8217
" Vương xà" nghe tên thì có thể thấy rất hay, nhưng truyện khiến mình hoàn toàn thất vọng. Đọc đoạn đầu thấy còn tạm ổn, Bạch Xà hiện lên khá thú vị, cá tính. Nhưng càng sau càng lằng nhằng, rối ren, thêm vào đó là rất hư cấu. Câu chuyện này lại giống các câu chuyện khác của Zen, thường có những điều rất phi thực tế, làm truyện thiếu chân thật, phóng đại quá nhiều nên đọc chỉ cảm thấy " điêu" . mình không thích văn phong của tác giả này, hư  cấu quá nhiều nên không hay. Đây là ý kiến riêng của mình, mong mọi người bỏ qua nếu mình viết gì không đúng.
2
1276662
2016-04-20 09:34:38
--------------------------
378115
8217
Mình bị cuốn hút bởi cái bìa, và tên tác giả cứ ngỡ là người Nhật. Và sau khi lên mạng tìm hiểu mới hiểu hóa ra đây là tác giả Việt Nam. Mình không thể hiểu tại sao tác giả có thể nghĩ ra một câu chuyện phi lí như thế này. Nếu ngay từ đầu cho truyện là ảo tưởng, phép thuật gì đó... thì chắc chắn sẽ đỡ hơn. Nhưng nhân trong truyện theo mình biết là con người "bình thường". Nào là chém giết người, phi lên không đáp đất nhẹ nhàng, thoắt ẩn thoắt hiện như kiếm hiệp Trung Quốc... rồi cảnh nữ chính mỗi khi đánh nhau mà lơ là bị kẻ thù đánh trúng, nam chính lập tức chạy tới đỡ đòn rồi ôm lấy cô vào lòng!? Không thể đọc tiếp nữa. Quá sức hư cấu. NXB sao lại có thể đem in một câu chuyện như thế này. Trong khi có rất nhieu62 tác phẩm hay, nhân văn, ý nghĩa lại không in? Tuy vậy mình có thể thấy ở một số chỗ lời văn của tác giả rất hay, nếu như bớt đi xíu thì đây chắc sẽ không phải câu chuyện tệ như thế này :((
1
638689
2016-02-04 13:36:42
--------------------------
362942
8217
Có thể nói đây là một trong những cuốn sách làm tôi khó chịu nhất từ trước đến giờ . Nhìn bìa cũng đẹp nhưng chất lượng giấy rất xấu dễ cong mép . In sai lỗi chính tả tùm lum , in chỗ đậm chỗ nhạt . Về nội dung thì khỏi phải nói rõ ràng là một thảm họa . Cách viết lủng củng nhạt nhẽo và cách tác giả xây dựng các tình huống trong truyện phải nói là nhảm nhí và vô lý hết cỡ . Cảm thấy thật là phí thời gian khi đọc cuốn sách này
1
337423
2016-01-03 08:29:04
--------------------------
259233
8217
Mình mua cuốn này cũng vào đợt giảm giá, nhưng quan trọng không phải do giảm giá mà vì tựa truyện, tên tác giả khiến mình nghĩ đây là truyện Nhật. Khi đứa em mở ra đọc thì kêu mình là truyện Việt Nam. Thực ra mình cũng không quá để tâm nếu truyện viết hay nhưng chính những tác phẩm như thế này khiến mình có cái nhìn rất mất thiện cảm đối với những tác phẩm của tác giả trẻ hiện nay.
Truyện giống một cuốn ngôn tình được viết cẩu thả. Nhân vật thì giống trong tưởng tượng và tình tiết, tâm lý đều bị cường điệu. Cuốn sách mua đáng tiếc nhất của mình.
1
94893
2015-08-09 10:55:27
--------------------------
258095
8217
Đợt tiki có chương trình khuyến mãi đơn hàng trên 300k tặng một quyển sách. Trong list sách tặng mình thấy cuốn này bìa rất đẹp, tên tác giả lại tiếng nhật (hóa ra là bạn trẻ Việt), nên tò mò chọn. Phải nói rằng bìa rất đẹp, giấy rất đẹp, rất thu hút. Nhưng đáng tiếc ngoài điểm cộng duy nhất là bìa đẹp thì cuốn sách này rất...nhạt. Cốt truyện nhạt, nhân vật được tác giả thần thánh hóa quá mức, tính cách lại thay đổi phi logic, văn phong teen lộn xộn. Nói chung may mà sách free, nếu không thì rất phí tiền mua.
2
96857
2015-08-08 10:53:15
--------------------------
162924
8217
Mình đã được tặng quyển sách này cách đây không lâu lắm...... Và '' Vương Xà'' là quyển tiểu thuyết thứ hai mà mình đọc của tác giả Mikahawa Zenkura sa '' Siêu Quậy Trường King World '' .Có thể tác giả mới viết nên câu từ, cách viết chưa hay lắm, chưa được trau chuốt kĩ càng lắm... Đôi khi có nhiều chỗ khó hiểu , chưa có chiều sâu lắm.... Tuy vậy mình vẫn khá thích nó....
Có nhiều lúc tác giả đã tưởng tưởng quá mức...Nhưng đã là tiểu thuyết thỳ sự tưởng tưởng đó là yếu tố cần thiết...............
Câu truyện kể về cuộc đời và tình yêu của những con người bị tổn thương sâu sắc : Huyết Xà, Bạch Xà, Độc Xà, Hắc Xà.... Những con người dũng cảm ấy đã phải Vượt qua bao nỗi đau, nỗi hận, để đến được với nhau.........
4
445182
2015-03-03 18:19:44
--------------------------
139825
8217
Bị thu hút bởi cái tên Vương xà, đọc tiếp phần giới thiệu, cứ tưởng là sẽ là một quyển sách với nội dung gay cấn, tranh đấu hấp dẫn. Đến khi lật ra đọc được chừng 1/5 là thấy không nuốt nổi. Thất vọng, thất vọng... không còn gì để nói. Bỏ qua phần hình thức (chẳng đẹp đẽ gì cho cam), rồi thì sai chính tả, lặp từ, câu cú dài dòng. Xin lỗi chứ đây là quyển sách đầu tiên làm cho tôi ức chế như vậy. Dựng lên một dàn nhân vật lạnh lùng, cô độc, tài giỏi như siêu nhân, nội tâm thì đau khổ, dằn vặt... chẳng đâu ra đâu. Ôm đồm nhiều thứ quá mà cái nào cũng mờ nhạt. Đọc mà như nghe như trẻ con kể chuyện. Cầm quyển sách đọc chút là bỏ xuống, đợi hết ức chế mới đọc tiếp, chẳng lẽ mua xong rồi vứt vào thùng rác. Từ nay tôi cạch tác giả này luôn.
1
19920
2014-12-09 09:01:24
--------------------------
114266
8217
Phải nói rằng đây là một trong những tác phẩm kinh khủng nhất tôi từng đọc. Chưa kể đến phần nội dung, ngay cả hình thức là đã không chấp nhận được rồi: bìa sách màu ngà, nhìn rất cũ; phần mặt cắt thì không hoàn chỉnh, nhìn rất khó chịu.
Nội dung thì còn tệ hơn nữa, nào thì lỗi sai chính tả, lời văn thô thiển, ngô nghê, câu văn thì dài dòng thê lê, khó hiểu. Tôi dù có dốt cũng là người VN, thế mà có những câu viết bằng tiếng VN tôi đọc chẳng hiểu gì. Chưa kể đến lỗi dùng từ, viết tiếng Anh ngay cả trong lời dẫn, viết sai cấu trúc và ngữ pháp nữa. Truyện thì hoang đường, dở hơi, nào là bang phái, nhảy từ trên cao đáp xuống nhẹ nhàng, nào là biến mất như kiếm hiệp TQ; nào là cảnh sát quyết đấu người trong bang, CIA, hai viên đạn cùng một đường thẳng va nhau cái rầm, rồi nhìn qua gương chiếu hậu bắn vỡ bình xăng xe phía sau như hành động Mỹ. Ba thằng Nhật, TQ, Mỹ chỉ nói cùng một ngôn ngữ thôi, đó là tiếng Việt. Cách đưa và giải quyết tình huống cũng gượng gạo, vô duyên. Những hành động, câu nói vô cùng ngốc nghếch của nhân vật thế mà tác giả lại bảo là thông minh, đa mưu túc trí. Chưa kể cốt truyện ra sao, chỉ cần đọc trang đầu tiên là thấy không muốn đọc nữa rồi.
Sách bây giờ cũng chẳng phải rẻ bèo rẻ bọt gì mà cứ xuất bản vô tội vạ như thế. Các NXB, làm ơn làm việc có lương tâm có tinh thần trách nhiệm một chút, để đọc giả yên tâm khi bỏ tiền ra mua sách, chứ đừng để người ta phải đọc rồi mới dám mua.
1
220006
2014-06-11 10:17:11
--------------------------
114065
8217
Bị thu hút bởi cái tên Vương Xà. Đầy quyền lực. Cùng với đôi mắt. Trong, sáng nhưng lặng, vô hồn.
Vì tác giả còn khá trẻ nên văn phong vẫn còn đôi chỗ khiếm khuyết nhưng càng về sau càng viết hay hơn , thích cách tác giả miêu tả vẻ đẹp của các nhân vật trong truyện, nhưng đôi lúc sự miêu tả đó không cần thiết, làm tình tiết truyện hơi loãng. Nhìn chung khá ổn.
Về nội dung càng về sau càng hấp dẫn, hình ảnh nhân vật được định hướng từ những trang đầu truyện, nhưng thay đổi dần theo cốt truyện.
Đọc đến giữa truyện cứ ngỡ Hắc Xà là người đau khổ nhất, nhưng càng về sau càng thương Bạch Xà. Chưa cảm nhận trọn vẹn thì tình cảm đã bị vùi dập, hình tượng người cha sụp đổ hoàn toàn. Gần như mất đi ánh sáng của bản thân. Đôi mắt ấy là của Bạch Xà...
Những tình cảm đan xen với nhau tạo nên Vương Xà, tình yêu, tình anh em, , tình gia đình, lòng hận thù, sự ghen ghét,...
Một cuốn sách nửa thực nửa ảo, đầy quyền lực nhưng u tối, đau đớn nhưng hạnh phúc.
Chỉ có những đoạn diễn biến quá nhanh khiến người đọc hơi thật vọng. 
Muốn nhìn thấy một Vương Xà hoàn thiện hơn vì rất thích cốt truyện và cách viết của tác giả.
Nhìn chung, đây không phải là một cuốn sách mà ai cũng thích. Dành cho những người trẻ, thích sự bi thương nhưng kết thúc tốt đẹp.
4
92151
2014-06-08 18:23:33
--------------------------
