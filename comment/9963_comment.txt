323129
9963
Mình đặt mua quyển sách này vì nó có cái tên gây tò mò. Về hình thức, bìa sách không có gì nỗi trội, giấy xấu và chữ khá nhỏ, đọc mõi mắt. Về nội dung, tập truyện này dường như không có đối tượng đọc giả đại chúng,mình còn khá trẻ và cảm thấy không bị hấp dẫn bởi các câu truyện ngắn trong sách. Những đề tài về ngày xưa, tác giả đã tạo nên một bầu không khí khá u ám trong truyện. Nó không quá đặc sắc, không cao trào, lại không đủ để gọi là nhẹ nhàng. Có lẻ vì không trải qua thời đại trước nên không cảm nhận được cái hồn của truyện...
2
741708
2015-10-17 20:52:15
--------------------------
300795
9963
Dạ Ngân là một trong số ít nhà văn nữ ở thời điểm hiện tại của  Việt Nam mình đánh giá cao, có phần nhỉnh hơn Nguyễn Ngọc Tư. Ở Nguyễn Ngọc Tư cái gì cũng bắt buộc phải gân guốc, khốc liệt, đôi khi mình cảm thấy làm quá, đời đâu có đẫm buồn tới vậy. Còn Dạ Ngân thì có 1 chất rất hay, kết hợp tuyệt vời giữa dịu dàng nữ tính và hơi cá tính, nổi loạn nhưng nhìn chung rất đậm chất đàn bà, khi đọc truyện của bà ta có thể cảm nhận được là chỉ có thể là một tâm hồn phụ nữ tinh tế mới viết ra được những cung bậc cảm xúc hơi cuồng ngông mà vẫn sâu lắng này. Tập truyện ngắn này chưa hẳn là một tác phẩm nổi bật của bà nhưng vẫn đủ tạo cảm giác cuốn hút bằng sự tinh tế này.
4
26136
2015-09-14 10:53:39
--------------------------
55082
9963
Đó là những trang nhật kí về những cảm xúc vừa xa vừa gần thì có lẽ sẽ đúng hơn là những tập truyện ngắn. Những trang sách mở ra vô vàn những nỗi buồn, những vị mặn của nước mắt, của cuộc sống. Cảm xúc, kí ức ngọt ngào, đắng cay đan xen pha trộn vào nhau một cách phức tạp và rắc rối. Ở đó là những bức tranh khắc họa về con người, thiên nhiên, cảnh vật, mà trong mỗi chi tiết đó lại chứa đựng những nỗi niềm riêng biệt. Văn phong nhẹ nhàng, lắng đọng, thấm như chính tiêu đề của nó Chưa phải ngày buồn nhất. Gấp trang sách lại, không khóc nhưng khóe mắt sẽ cay, trong lòng những dư vị vẫn còn nhưng được xếp lại ngăn nắp hơn. Chúng ta sẽ nhận thấy rằng thật ra vẫn chưa phải ngày buồn nhất đâu, bởi ngày buồn nhất, lại là ngày mà chúng ta mất đi một thứ gọi là yêu thương cơ. yêu thương gia đình, yêu thương bạn bè và yêu thương chính bản thân mình, ^^
5
9481
2013-01-12 00:12:43
--------------------------
49966
9963
Mình đã chọn đọc cuốn sách này vì tiêu đề của nó gợi lên rất nhiều sự tò mò. Và mình đã bị cuốn sách thu hút từ lúc nào không biết, chỉ biết khi gập sách lại, thì những câu chuyện của nó còn mãi vang vọng trong tâm trí. Mỗi truyện trong tập này đều giống như những trang nhật ký, những trang viết tản mạn về một quãng thời gian vừa xa xưa vừa như mới đây, những kỷ niệm cùng ký ức buồn vui đan xen nhau phức tạp. Ở đó, nhà văn trải lòng mình ra, chất chứa cảm xúc vào mỗi hình ảnh của cảnh vật, con người, cũng như kể những câu chuyện nhẹ nhàng mà sâu sắc về bao điều xung quanh mình. Hẳn khi đọc xong ai cũng sẽ cảm thấy buồn man mác, nhưng nỗi buồn đó không làm bạn khóc, nó sẽ chỉ để lại những dư âm đặc biệt mà thôi...
4
20073
2012-12-11 12:37:21
--------------------------
