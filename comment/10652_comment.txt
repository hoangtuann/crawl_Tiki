265156
10652
Vương Tiểu Cái là một trong những thành viên của Thất Tinh , không giống như Công Tôn Thắng có thể suy nghĩ thấu đáo và hành động cẩn trọng , hắn là một người hữu dũng vô mưu . Chỉ có võ nhưng lại không có văn lại luôn không để người khác vào mắt . 
Đọc tập này khiến chúng ta hiểu rõ hơn về Tiều Cái, trước khi đọc bộ truyện này đọc tiêu để mình tưởng hắn là người tài giỏi , dù hữu dũng vô mưu nhưng cũng hành hiệp trượng nghĩa . Ai ngờ chỉ đơn thuần là kẻ dùng võ không màng đến tính mạng người khác. Điều này khiến mình rất thất vọng về hắn . 
5
335856
2015-08-13 19:43:18
--------------------------
155577
10652
Đây là chương truyện đem lại cho mình nhiều suy nghĩ. Việc Tiều Cái cùng những huynh đệ của mình bày mưu cướp chúc thọ được ca ngợi là việc làm anh hùng, nhưng mình không hoàn toàn nghĩ vậy. Họ nói rằng đó là của bất nghĩa, đáng cướp, nhưng khi cướp thì họ lại chia nhau để tiêu, phần còn lại sau đó mang lên Lương Sơn, chứ không hề chia cho dân nghèo, những người mà họ cho là bị bóc lột, vậy không phải bất nghĩa sao? Không những vậy, họ còn đẩy cả Dương Chí, một người trung thành, chỉ muốn lập công chuộc tối, đi đến chốn không còn đường quay đầu phải đi làm cướp, vậy không phải là tàn nhẫn sao? Chương truyện này cũng cho chúng ta thấy được bản chất của Tiều Cái, một người mang chủ nghĩa bạo lực, coi thường mạng người, không thể có tố chất để làm lãnh đạo nghĩa quân sau này.
4
472173
2015-02-01 13:20:48
--------------------------
