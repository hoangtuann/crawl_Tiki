258047
3810
Mình mua cặp cuốn này với cuốn vô cơ và mọi sự hạn chế của cuốn vô cơ hình như đã được cải thiện trong cuốn hữu cơ này. Về phần bài tập thì với mỗi dạng đã tăng lên rất nhiều ( khoảng chừng từ 20 - 30 bài). Bài tập bám sát chương trình và bám sát thi trung học quốc gia nhiều hơn. Về độ khó của bài tập cũng cải tiến hợp lí, độ khó nâng cao dần nên thích hợp cho mọi học sinh. Còn về phần lí thuyết và cách phân dạng bài tập thì vẫn vậy, vẫn đầy đủ mạch lac và thêm vào một vài lí thuyết nâng cao.
5
647979
2015-08-08 10:29:06
--------------------------
