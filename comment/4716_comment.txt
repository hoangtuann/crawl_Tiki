420142
4716
Tôi rất thích văn học Nhật Bản. Vì nó gắn với đời thường khá nhiều và mang tính nhân văn sâu sắc. Cuốn sách này cũng vậy. Rất hay. Tôi như tìm thấy đâu đó trong câu chuyện chính bản thân mình. Sống nội tâm khép kín, không dám thể hiện cảm xúc suy nghĩ của bản thân để rồi đến khi người ấy không còn nữa thì thấy hận bản thân mình vô hạn vì đã không nói những lời yêu thuơng với người ấy, vì hờ hững nghĩ rằng thời gian còn nhiều. Tôi khuyên bạn hãy thử mua và đọc nó để biết mở rộng bản thân mình hơn làm cho người khác hiểu minh hơn.
4
958480
2016-04-22 18:57:04
--------------------------
375895
4716
Nhân vật nữ chính, 20 tuổi, đầy cô đơn và hờ hững với cuộc sống. Chuyển đến Tokyo sống và tìm việc làm, sống với người mợ già nhưng tâm hồn trẻ trung. Chiro  dường như vô tâm và giận dỗi với tất cả mọi người, với người mẹ luôn bảo bọc, với cậu người yêu chỉ mê game và coi cô như người bạn gái chỉ để làm tình, với bà mợ già mà theo cô là gàn dở. 
Những mối quan hệ cứ dần xấu đi, theo thời gian khiến Chiro dần trưởng thành hơn và thay đổi mình, giống như khi cô bắt đầu cuộc sống ở Tokyo là mùa xuân, và cuối truyện thì một mùa xuân mới lại mở ra cho Chiro. 
Những tác phẩm của Nhật luôn mang màu sắc nhẹ nhàng và đầy cảm xúc . nhưng thực sự rất ngọt ngào và gây nghiện
4
69833
2016-01-29 14:26:09
--------------------------
363805
4716
Ấn tượng với thiết kế bìa sách cùng với tựa "Ngày đẹp trời để cô đơn", sau khi đọc lướt qua phần giới thiệu nội dung của tác phẩm, Mình càng thêm thích và quyết rinh ngay về. 1 phần lý do nữa là khi đó tâm trạng đang khá tệ, cảm thấy cô đơn, lạc lõng giữa bộn bề cuộc sống nữa hy vọing cuốn sách này sẽ giúp ích gì đó cho bản thân.
Về nội dung câu chuyện về cuộc sống ở ngưỡng cửa vào đời của Chizu, cuộc sống của cô ấy khá tách biệt với xung quanh cô ấy tự khép suy nghĩ quanh quẩn trong đầu, cô ấy không thiết tha gì với danh vọng, với những mong ước gì cả, cô ấy sống dường như không lấy tương lai làm điểm đến mà chỉ là để bản thân trôi nổi với cuộc sống hiện tại, không níu giữ gì cả ngay cả đối với tình yêu của chính bản thân mình cũng vậy.
Đọc dần về sau tôi mong muốn sẽ có 1 cánh cửa sáng lạng sẽ cứu vớt cô ấy khỏi cái lòng lẩng quẩng ấy hay thậm chí 1 tia hy vọng, nhưng nói thiệt đọc đến những trang cuối tôi phải nghiền ngẫm và thấy khá hụt hẫng khi cô ấy đã buông thả bản thân, trở thành tình nhân của cái gã Andou, cuộc sống của cô ấy sẽ lại giống như xưa, không gì thay đổi cả và 1 điều tôi cảm thấy buồn cho cô ấy trong tình cảnh đó không ai tìm ra cô ấy kéo cô ấy lên khỏi vực sâu "cô đơn" ít ra cũng có 1 chút hy vọng gì đó chứ.
Giọng kể chân thành mang tính nhật ký cuộc sống hằng ngày của Chizu không khô khan, gò ép.
Đọc cảm thấy cô ấy cũng hơi giống mình nhưng với cái kết thúc thì cảm thấy buồn, cô đơn tăng thêm. Đọc xong  bản thân càng cảm giác cô đơn, chán nản .
3
393052
2016-01-04 22:46:07
--------------------------
334750
4716
Đối với mình kết thúc những mẫu truyện của Nhật Bản luôn khó hiểu. Như là kết thúc mở vậy mà mở rồi vẫn không hiểu. Theo mình thấy truyện như đề cập đến một cô gái trẻ như không có một mục đích, không có gì để tiếc nuối nhưng vẫn luôn khám phá mọi thứ xung quanh nhưng cô hình như cũng không có một ảm xúc gì. Truyện cũng đề cập đến sự lạnh lùng vượt mức qui định, về sự đấu tranh tâm lí luôn khiến nhân vật phải đắng đo. Nhưng thật sự truyện rất hay. Lối văn dù lạ nhưng lại gửi gần ý nghĩa vào đó
4
483852
2015-11-09 23:26:32
--------------------------
301995
4716
Các tác phẩm văn học Nhật đa phần đều hơi khó hiểu nhưng tác phẩm "Ngày Đẹp Trời Để Cô Đơn" thì lại giản dị, dễ hiểu. Tác phẩm kể về câu chuyện của cô gái trẻ. Câu chuyện ấy, có lẽ mỗi người trẻ đều trải qua. Đó là những ngày tháng ta cảm thấy chán tất cả mối quan hệ, không biết làm gì, không biết phải thế nào. Nhưng rồi, tất cả chúng ta đều đi qua và trưởng thành.Những ai đang thấy lạc lối, cô đơn, hãy tìm đọc cuốn sách này để tìm ra hướng đi cho mình.
5
151980
2015-09-14 22:18:38
--------------------------
272641
4716
Tôi luôn thích văn học và tiểu thuyết của Nhật, vì nó lúc nào khiến tôi thấy mình trong đó, luôn có tính nhân văn và có giá trị khá sâu sắc.
Khi nhìn thấy tiêu đề “Ngày đẹp trời để cô đơn” như một sự thu hút ngay sự tò mò của mình. Vì thực sự tâm trạng ngay lúc ấy của mình rất, rất cô đơn, lạc lõng. Những câu văn và những lời đối thoại rất giản dị, mạch văn chậm, nhẹ nhàng êm ả và hầu như cốt truyện chỉ đơn giản là những câu chuyện thường ngày trong cuộc sống. Nhưng khi đọc câu chuyện cảm nhận như chính mình trong đó, cảm giác như sự đồng cảm và muốn tìm kiếm một thứ gì đó hay một điều gì đó để cứu giúp tâm trạng này của mình. ''Ngày đẹp trời để cô đơn'' là ngày để ở một mình, để suy ngẫm cuộc sống mình đang có. Lời khuyên cho những người đang cô đơn, lạc lối trong chính cuộc sống của mình hãy đọc cuốn sách này.
5
409686
2015-08-20 11:01:25
--------------------------
264609
4716
Mình đã nhận được sách Ngày Đẹp Trời Để Cô Đơn, mình mua cuốn sách này là để làm quà tặng cho học sinh. Sau khi đọc xong, học sinh mình rất thích. Cô pé nói trong sách có nhiều điều cô pé cần học hỏi và cảm ơn mình rất nhiều. Mình cảm thấy rất vui vì đã tặng một món quà ý nghĩa và có ích như thế cho học sinh của mình. Mình cảm ơn tiki nhiều nha. Và học sinh mình rất thích kiểu đóng gói của tiki rất nhiều. Và còn nói với mình sẽ mua sách ở tiki.
4
408956
2015-08-13 11:56:39
--------------------------
226838
4716
Mình vốn thích văn học Nhật, tình cờ đọc được một đoạn nhỏ trong cuốn này và bị thu hút nên đặt mua. Cốt truyện dễ hiểu, không quá mù mịt như nhiều tác phẩm văn học Nhật khác mà mình từng đọc; cảm xúc trong truyện có nét tương đồng với mình hiện tại nên khi đọc rất nhập tâm. Minh họa bìa dễ thương, toát lên được nét Nhật. Tuy nhiên điều làm mình thất vọng là khi bóc hộp đựng hàng của Tiki ra thì trên tay mình là một cuốn sách bìa hằn vết gập, mép lộp rộp có phần bị bong giấy, giấy bên trong cắt không đều. Đã quen sưu tập sách nên nhìn thấy cuốn này bị tình trạng như vậy mình rất buồn. Chỉ hy vọng sách đặt mua online cũng có thể đẹp và không lỗi như những lần mình tự tay lựa chọn ở nhà sách.
3
689264
2015-07-13 01:07:18
--------------------------
215435
4716
Tình cờ thôi, một lần đi lòng vòng trong nhà sách tôi đã mua nó. Chỉ bởi vì bị cuốn hút bởi bìa sách hình chú mèo đen độc đáo. Trong câu chuyện, ta thấy được cuộc sống đó đầu ắp nỗi cô đơn, trống trải vì Chizu tự cô lập chính mình. Rồi dần thế, mọi mối quan hệ đều trở nên xấu đi. Nhưng có lẽ bà Ginko đã thay đổi tất cả. Ta có thể thấy rõ được cách nhìn nhận cuộc sống của người già và người trẻ là hoàn toàn khác nhau. Những người đang cô đơn, lạc lối trong chính cuộc sống của mình hãy đọc sách, lắng nghe lời bà Ginko, bạn sẽ thấy cuộc sống này thú vị hơn đấy. 
Tuy kết truyện có thể không làm người đọc thoả mãn. Nhưng trong đó, sự cô đơn đã không còn nữa mà thay vào đó là ánh nhìn về một tương lai tươi sáng hơn.
4
11782
2015-06-26 12:47:01
--------------------------
215112
4716
Câu chuyện mang nỗi buồn khá mơ hồ, một cô gái tỉnh lẻ lên Tokyo, cuộc sống trong căn nhà nhỏ gần sân ga với bà lão cô độc, nơi cô theo đuổi tình yêu, lẽ sống trong khi vẫn chật vật với cuộc mưu sinh ở thành phố lớn. Dường như một cô gái trẻ lại có nhiều ưu phiền vật lộn hơn cả bà lão đã gần đất xa trời. Tác giả sắp xếp hai nhân vật đối nghịch ở cạnh nhau như làm nổi bật nghịch lý đó.Quả thật khi đọc sách lại nghĩ vu vơ rằng cô đơn có gì không tốt, họ vẫn yêu vẫn mất mát, tổn thương nhưng dường như cuộc sống vẫn nhẹ nhàng trôi, và từ nơi đó. một ngày kia cô gái nhỏ đã trưởng thành.
3
419116
2015-06-25 21:55:11
--------------------------
200640
4716
Không thể tin được 1 ngày đẹp trời của tôi lại có thể cảm nhận đến tột độ nỗi cô đơn vì 1 quyển sách khá bé. Không phải kiểu tiểu thuyết trữ tình sướt mướt nhưng bảo đảm là bản thân đã "tự nguyện" để cảm nhận được cô đơn đủ nhiều trong 1 ngày đẹp trời.
Chú mèo lững thững, đối mặt với cái cô đơn nhàn hạ như thể : ờ, cũng thường thôi mà... Thích thú , cô đơn nhưng cũng không kém phần yêu thương là cảm giác tôi nhận lại được, bù lại cho 1 ngày đẹp trời nhưng cô độc.
Không chắc sẽ khiến tôi đọc lại nhưng chắc chắn, 1 ngày đẹp trời nào đó nếu lại cô đơn, tôi sẽ tìm về nó.
5
140875
2015-05-25 17:11:53
--------------------------
198087
4716
Đượm mùi cô đơn và căng mùi nhựa sống cộng thêm một chút nỗi buồn man mác pha lẫn nữa, đó là những gì mình cảm thấy sau khi đọc xong tác phẩm. Ngày đẹp trời để cô đơn cũng tương tự như nhiều câu chuyện khác đến từ Nhật Bản như Vĩnh biệt Tugumi, Đôi mắt ấy vẫn ở trên giường hay cuốn sách mà đa phần chúng ta đều biết là Rừng Nauy... từ đầu đến cuối là một mạch văn chậm chạp, trôi qua rất nhẹ nhàng êm ả và hầu như cốt truyện chỉ đơn giản là những câu chuyện thường ngày, không kịch tính nhưng cái thu hút và đặc biệt của các tác phẩm này lại nằm ở chính chỗ ấy. Đọc xong mới thấy hơi thở của tác phẩm đã tuồg vào tim người đọc và khó mà dứt ra được.
4
274995
2015-05-19 09:16:15
--------------------------
195916
4716
Cũng chỉ là tình cờ thôi, tôi mua nó trong một hội sách ở trường đại học. Đọc tựa đề lên tôi cảm thấy dường như có mối liên kết giữa mình và cuốn sách. Có lẽ do tâm trạng cũng đang chênh vênh, quyết định mua về. 
  Tôi thích những chú mèo, nó chẳng có vẻ gì là cô đơn cả, dù luôn đi một mình. Phong thái như bà hoàng. Những người độc thân nuôi mèo cũng như vậy. Họ chấp nhận một mình và không lấy làm phiền vì điều đó. Bà Ginko như vậy đấy. Chiro khi đến ở với bà cũng như vậy. Cô đơn cũng có cái vui riêng của nó. 
  Tôi thích mẫu đối thoại này: 
 "- Nhân lúc còn trẻ mà yêu đương vài lần cũng tốt biết bao. 
  - Những chuyện kiểu này...trống rỗng quá"
  Có vẻ nó chạm vào được thứ sâu bên trong tôi. Cảm giác đồng điệu.
4
30531
2015-05-14 21:33:25
--------------------------
182571
4716
Những từ ngữ rất giản dị đưa người đọc đến với từng tâm trạng của Chiro qua 5 chương, từ cô bé mới đến sống với bà Ginko đến khi nhìn lại căn nhà khi xưa ở trên tàu điện. Nó tràn đầy nỗi cô đơn, không gian trống vắng bao trùm lên ngôi nhà của bà Ginko và Chiro, , 2 người tưởng như xa lạ nhưng thực chất lại rất thân thiết, yêu thương và quan tâm lẫn nhau. Chiro dần trưởng thành trong các mối quan hệ nhưng cái cô đơn, cảm giác chỉ có một mình lại bủa vây lấy cô, khiến cô luôn cảm thấy bất an về những mối quan hệ xung quanh. hình ảnh kết của câu chuyện có gì đó mở ra trong tâm trí người đọc như thể CÂU CHUYỆN TIẾP THEO se do chinh ban tao nen. ''Ngày đẹp trời để cô đơn'' là ngày để ở một mình, để suy ngẫm cuộc sống mình đang có..................................
4
564406
2015-04-14 13:17:14
--------------------------
