327784
9487
Sách chỉ rõ là dành cho sinh viên, những người chuẩn bị có sự thay đổi về cách thức học cũng như môi trường sống. Sách được viết bởi những người đã từng là sinh viên và đạt được những thành công nhất định nên mình thấy rất đáng học hỏi. Sách đề cập đến nhiều vấn đề kinh điển mà người sinh viên thường gặp phải, từ phương pháp học tập cho đến rèn luyện các kỹ năng bổ trợ, rồi cả tìm học bổng và đi du học. Phải nói cuốn sách rất sát với thực tế sinh viên mà mình đang trải qua. Nó tạo được sự hào hứng và mong muốn khám phá, đặc biệt là với các tân sinh viên. Mình nghĩ mỗi bạn chuẩn bị bước vào đại học nên trang bị một cuốn để có một đời sinh viên đáng nhớ.
5
39590
2015-10-28 14:58:52
--------------------------
324976
9487
Đây là sách mà cô giáo mình giới thiệu và mình rất vui vì điều đó. Cuốn sách quả là người bạn thân của sinh viên, nhất là tụi năm nhất loi nhoi như mình.
Theo ý kiến cá nhân thì mình nghĩ bìa bắt mắt một chút thì sẽ thu hút hơn. Cho dù để kệ sách thì vẫn hấp dẫn mình, nhờ đó mình sẽ nhớ mà đọc đi đọc lại lời khuyên từ sách.
Điều cuối cùng mình xin góp ý là sách nên có ảnh minh họa nhiều hơn để thuận tiện cho việc ghi nhớ. 
Cám ơn tác giả, NXB và Tiki đã đem đến món quà giá trị này đến với sinh viên.

3
95468
2015-10-22 14:52:27
--------------------------
303732
9487
Biết sách này vào 1 lần đi nhà sách. Nên quyết định đặt mua, thật sự không làm mình cảm thấy thất vọng. Lời văn lôi cuốn dí dỏm, đặc biệt rất teen. Chia sẻ những bí kíp học , làm, cân bằng giữa chơi và học, học bổng.... Không những vậy mà còn có những phần câu hỏi mà nếu bạn chưa đọc quyển sách này có khi cũng chẳng biết hỏi ai nữa. Nên bản thân mình thấy cuốn sách như một người bạn thật sự cho sinh viên vậy!!! Mong rằng các bạn học sinh, sinh viên hãy tìm đến với người bạn này, bởi vì bạn ấy có quá nhiều điều hữu ích mà. Cuối cùng mình xin chân thành cảm ơn các tác giả đã gửi đến cho chúng tôi một tác phẩm hay như vậy .Mà lại là tác phẩm của các tác giả Việt Nam nữa chứ !!
4
11836
2015-09-15 23:04:43
--------------------------
287700
9487
Bước chân vào đại học là thời điểm tôi hoàn toàn bỡ ngỡ và ngỡ ngàng về cách học cũng như môi trường khác hẳn trường phổ thông trước đây. Sau một kì kết quả học tập không được tốt, để tìm cách cải thiện tôi đã đi đọc nhiều nơi và xin nhiều góp ý. Sau rồi tôi đã tìm thấy cuốn sách này. Cuốn sách đã định hướng lại cho tôi trong phương pháp học và cách học đại học hiệu quả hơn, cái mà tôi đang thiếu để cải thiện kết quả học tập của mình. Nhược điểm thì tôi nghĩ nó có một nhược điểm ở phần bản đồ tư duy, tác giả viết chưa kĩ và tôi thấy hơi khó hiểu.
4
443561
2015-09-02 22:47:04
--------------------------
267046
9487
Một quyển sách khá hay và thú vị "made in Việt Nam", "Bí quyết thành công sinh viên" là 1 quyển cẩm nang thực sự cần thiết cho giới trẻ, đặc biệt là các bạn học phổ thông và Đại Học. Quyển sách được biên soạn bởi các tác giả Việt Nam nên lời lẽ, ngôn từ rất gần gũi với người đọc, nhiều bí quyết được các thầy cô truyền lại rất dễ hiểu bà dễ áp dụng,... Mỗi chương là 1 vấn đề mà giới sinh viên thường gặp phải và đi kèm đó là những kinh nghiệm, phương pháp đề ra. Tôi khá thích phần viết về sử dụng Internet và việc học tiếng Anh, mặc dù không nhiều nhưng vừa đủ để tạo động lực, phần Luyện đọc nhanh và Bản đồ tư duy có vẻ cần viết thêm nữa vì nó khá hay nhưng hơi ngắn và thiếu. Tóm lại, đây là 1 quyển sách khá tốt và cần thiết cho sinh viên hiện nay!
4
46905
2015-08-15 10:01:02
--------------------------
260474
9487
Mình nghĩ mấy bạn sinh viên nên mua quyển sách này vì nó thật sự rất hữu ích, lời văn viết rất dễ hiểu sẽ giúp bạn vượt qua giai đoạn sinh viên một cách thành công, nhưng cũng có nhiều phần viết chưa sâu lắm cần tập trung nhiều hơn vào phần phương pháp quyển sách sắp xếp các chương rất hợp lí nếu bạn đang là một học sinh cấp ba thì bạn rất cần phải mua quyển sách này để có được sự chuẩn  bị kĩ lưỡng cho sự thành sau này cũng như con đường chinh phục ước mơ.

5
722428
2015-08-10 14:16:42
--------------------------
190517
9487
Cuốn sách thực sự rất hữu ích cho các bạn trẻ đang là học sinh phổ thông hay bậc đại học, cao đẳng. Cuốn sách hướng dẫn cho các bạn biết các học hợp lý, những điều cần thiết và quan trọng đối với một học sinh, sinh viên như mục đích của việc đi học, tương lai của bạn như thế nào trong tương lai nếu bạn không chịu đầu tư thời gian trong thời học sinh, sinh viên.
Cuốn sách do người Việt Nam viết nên tôi nghĩ các bạn sẽ dễ đọc và hiểu. Cách trình bày của cuốn sách khá khoa học. Hạn chế của sách là lý thuyết nhiều và câu chuyện thực tế ít nên sẽ không thú vị như một số sách của nước ngoài.
4
302942
2015-04-29 18:39:41
--------------------------
92581
9487
Bản thân mình cũng đang là tân sinh viên năm 2013, nên mình rất háo hức chọn những quyển sách viết về môi trường Đại học. Đặc biệt là những trang kinh nghêệm dành cho sinh viên.

Quyển sách này quả thực là món quà tinh thần rất lớn với bản thân. Nội dung phù hợp với học sinh chuẩn bị bước lên nấc thang sinh viên ^^ . 

Sách dạy những cách kết bạn, những phương thức học tập thông minh bâc nhất như : Sơ đồ tư duy, phương thức 80/20 chìa khóa, cũng như cách điều phối thời gian học tập, sinh hoạt trong cuộc sống Đại học sao cho phù hợp nhất. 

Ngoài ra, trong sách còn giới thiệu những trang web săn lùng học bổng của những quốc gia nổi tiếng với những trường đại học hàng đầu như Anh, Mỹ, Singapore...

Hy vọng những bạn học sinh sắp bước chân vào cánh cửa đại học nên tự tặng mình quyển này nhé. Chúc các bạn thành công!
5
50283
2013-08-27 22:30:59
--------------------------
