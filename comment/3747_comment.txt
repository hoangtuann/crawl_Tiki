452986
3747
Tôi rất ấn tượng với lời giới thiệu về cuốn sách này. Không ngoài mong đợi, các truyện ngắn có cốt truyện thực sự mới lạ, độc đáo. Cái cách mà tác giả truyền tải thông điệp rất ý nhị và sâu sắc. Ai đang muốn sự mới mẻ và cách tư duy khác lạ thì có lẽ sẽ rất thích cuốn này. Tuy nhiên, đối với những ai tìm kiếm sự trọn vẹn thì đây không phải cuốn sách phù hợp cho lắm. Có một số truyện kết thúc mở, khiến tôi thấy hơi hụt hẫng. Nhìn chung, theo ý kiến của tôi thì đây là một cuốn sách khá hay.
4
76230
2016-06-20 22:29:29
--------------------------
432111
3747
"Đột nhiên có tiếng gõ cửa" theo mình đánh giá là một tác phẩm khá độc đáo, với nhiều tầng ý nghĩa sâu sắc. Tuy nhiên, có thể do mình không hiểu lắm văn hóa của người Israel nên một số câu chuyện mình không hiểu tác giả muốn gửi gắm gì trong đó. Một điều mình thích là lời văn khá độc đáo, thú vị và mới mẻ, thêm vào đó cái gì đó rất kì lạ. Có lẽ phải trải nghiệm thêm, đọc lại cuốn sách vài lần nữa, mình sẽ ngấm được nhiều hơn. Có lẽ đó chính là điểm đặc biệt của Etgar Keret.
3
676453
2016-05-18 16:26:54
--------------------------
394793
3747
Cuốn sách này gây ấn tượng với mình ngay từ tên tác phẩm và bìa sách nhưng đọc nội dung của nó thì khá khó hiểu. Thực sự mình không đủ kiên nhẫn để đọc hết tác phẩm này những mình nghĩ ai hiểu được nội dung mà tác giả truyền đạt thì chắc nó cũng khá hay (bạn mình bảo cuốn này khá hay). Tuỳ vào cách đọc cách suy nghĩ của mỗi người mà có cái nhìn cách đánh giá về tác phẩm lại khác nhau. Tuy nhiên cũng có khá nhiều điều khiến ta phải suy nghĩ. Giọng văn mang vẻ tưng tửng bất cần mà mình rất thích.
4
580169
2016-03-10 22:18:16
--------------------------
312770
3747
May mắn dạo tiki mà phát hiện tập truyện này. Thực sự là một cuốn sách thú vị từ nền văn học Israel.
Phong cách kể của tác giả rất cuốn hút: người đọc cứ cuốn đi theo từng bước đi, cái nhìn của các nhân vật, tham giai vào những hoàn cảnh đôi khi rất đơn giản nhưng toát lên sự kỳ lạ.
Lối xây dựng câu chuyện có nhiều tình tiết tuy bề ngoài bình thường nhưng lại mang chất "siêu thực". Đôi lúc tác giả gợi mình nhớ đến những tình tiết trong các tiểu thuyết của Haruki Murakami
Đôi lúc một truyện ngắn còn mang lại nhiều hứng khởi, cảm xúc và thậm chí có thể khiến bạn suy nghĩ nhiều hơn cả một cuốn tiểu thuyết dài.
5
104913
2015-09-22 00:38:36
--------------------------
310117
3747
Vừa được nghe giới thiệu là mình đặt quyển này liền. Chú nhà văn có cách kể chuyện rất lạ và dễ thương. Đặc biệt trí tưởng tượng cực kì phong phú, mình như lần nữa được ngồi nghe kể chuyện cổ tích, có điều cổ tích hiện đại và tửng quá thể :) À có vài truyện mình không hiểu lắm, không sao, với những mẫu truyện dễ thương còn lại mình thấy hài lòng khi có quyển sách này. Đọc để cảm nhận cuộc sống theo cách rất khác, có lúc buồn cười, có lúc bùi ngùi hối hận, có lúc sảng khoái yêu đời. Hy vọng Tiki sẽ mang về thêm nhiều sách có trí tuệ thế này để mình có dịp cảm nhận nhiều khía cạnh khác nữa.
5
168274
2015-09-19 12:25:50
--------------------------
302801
3747
Đây là một cuốn sách được đại sứ quán Isreal giới thiệu nên mình quyết định mua, khi cầm cuốn sách này trên tay và đọc các dòng chữ trên bìa mình có cảm nhận là nó rất thu hút. Đọc cuốn sách này quả thật rất thu hút với văn phong khác biệt với các tác phẩm mình thường đọc, nó đã đem đến một trải nghiệm mới lạ. Các bạn nên dành thời gian từ từ đọc và cảm nhận những mẫu truyện trong cuốn sách này, Chúc các bạn có những cảm nhận thú vị với cuốn sách này
4
471238
2015-09-15 14:38:56
--------------------------
292649
3747
Ban đầu khi mới nhìn qua bìa sách được thiết kế với hình chiếc cửa sổ cũ kĩ, u ám và tựa sách Đột nhiên có tiếng gõ cửa, gợi lên một cảm giác rờn rợn, tôi cứ nghĩ đây hẳn là truyện trinh thám hoặc ma quái huyền hoặc nhưng khi đọc Đột Nhiên Có Tiếng Gõ Cửa của Tác giả Etgar Keret, tôi lại thấy hết sức thú vị. Giọng văn tưng tửng có gì đó bất cần đối với thế giới hiện tại. Một số truyện có vẻ hơi khó hiểu, tôi nghĩ người đọc cũng cần phải có trải nghiệm và tư duy nhất định mới cảm nhận được.
5
465332
2015-09-07 15:10:41
--------------------------
285463
3747
Lúc đầu mình còn chẳng biết có sự hiện diện của quyển sách này nữa là... Chủ yếu mua dùm nhỏ bạn thân. Nhưng lúc mở đơn hàng, Nhìn quyển sách mình có cảm giác hứng thú lạ thường, chưa kể cuốn sách được đại sứ quán Israel giới thiệu thì làm sao bỏ qua được. Thế là cầm lên đọc luôn và quả là không ngoài mong đợi. Tình tiết câu chuyện với mình khá lạ và thú vị. Một nhà văn kể chuyện cho những con người muốn thoát ly khỏi cái thế giới đau khổ và khốn nạn của thực tại. Câu chuyện bắt đầu và "đột nhiên có tiếng gõ cửa", không ngờ tiếng gõ cửa định mệnh lại làm nhà văn không còn đường thoái lui và bắt đầu những câu chuyện kì quái...
5
144721
2015-08-31 21:10:53
--------------------------
236461
3747
Khi đọc tựa đề tôi cứ sờ sợ rằng sẽ có con ma thò tay bắt mất tôi khi tôi nghe tiếng gõ cửa và mở nó ra. Ấy vậy mà tôi đã mở cánh cửa khi "Đột nhiên có tiếng gõ cửa" vang lên và khiến tôi cười ha hả nhưng cũng thấm thía. Khi đọc tôi thấy khó hiểu nội dung mà ý nghĩa tác giả muốn truyền đạt nên đã chọn câu truyện bất kỳ để đọc, có truyện hài hước nhưng cũng có truyện thật sâu lắng. Ví dụ như "Cô là loài vật nào?" nói về con trai hay hỏi và câu trả lời luôn làm cậu phật ý cho đến khi một cô phóng viên Đức nói theo kiểu ma quái và ông bố đã bẻ gãy ý nghĩa của câu trả lời đủ để con trai mình hài lòng... 
Kỳ thực cuốn sách này khá khó hiểu, nếu hiểu theo bề nổi thì đơn giản nhưng như vậy sẽ mất đi chất hay đặc sệt mà tác giả muốn truyền đạt, tôi sẽ ngâm cứu quyển sách lại một lần nữa để chắc chắn rằng không có trò chơi Just for fun trong đây.
4
26363
2015-07-21 20:13:43
--------------------------
212903
3747
Mình cũng đã theo dõi về buổi giới thiệu sách của đại sứ quán! Đúng là có bất ngờ về mấy câu chuyện đầu tiên về sự hàm ý sâu xa mà tác giả muốn nói. Nhưng có một số câu chuyện thì ko thể cảm nhận nổi, đọc xong hơi bị đơ vì không hiểu nổi ý nghĩa hay đơn giản chỉ là mình ko hiểu được. Đề cập vấn đề giới tính hơi thẳng. Không hợp với gu đọc cho lắm, nhưng phải công nhận là có những chuyện thật sự đáng suy ngẫm và giật mình! Túm lại là cũng là một tác phẩm hay với giọng văn tưng tửng và bất cần. Lột tả những cảm xúc và tâm trạng của nhiều nhân vật khác nhau ở từ câu chuyện!....Chắc kén người đọc uyên thâm. hihi
4
309333
2015-06-23 00:05:53
--------------------------
204086
3747
Mình đã tham dự buổi giao lưu của tác giả và đã mua cuốn sách này.
Không biết có phải do khác biệt về vùng sinh sống nên tư duy cũng khác biệt hay không, nhưng thật sự mình đọc một số truyện cũng thấy hiểu hiểu, một số thì không biết mục đích tác giả muốn gửi gắm đến điều gì, cái cười trong tư duy của tác giả quá khác so với mình.
Cá nhân mình thì không thích thể loại truyện ngắn như thế này, có lẽ vẫn chưa đạt tới trình độ để hiểu được nó.
Có nhiều truyện ngắn tác giả đề cập khá sex, mà mình nhận xét là khá biến thái, mặc dù nhìn ở ngoài tác giả rất hiền lành.
3
74132
2015-06-03 07:53:51
--------------------------
