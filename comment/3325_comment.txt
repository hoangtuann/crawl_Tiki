433553
3325
Ngoài những kiến thức trên lớp và trong sách giáo khoa thì học sinh cần có những nguồn tham khảo khác, đặc biệt là đối với môn tập làm văn. Cuốn sách "Tập Làm Văn Lớp 5" này được biên soạn hợp lý với chương trình học hiện nay, không chỉ hướng dẫn giải bài tập trong sách giáo khoa, nó còn hướng dẫn học sinh cách tìm hiểu vấn đề, cách lập một dàn bài, mà bên cạnh đó còn có thêm những bài văn mẫu để học sinh có thể tham khảo từ đó viết ra được những bài văn của riêng mình, rất hữu ích.
5
471112
2016-05-21 11:56:22
--------------------------
