454058
8757
Những câu chuyện ngụ ngôn gây nhiều tiếng cười và để lại bài học thâm thúy cho người đọc. Qua câu chữ tiếng anh mình lại thêm được bài học về tiếng anh. Quá ư là lợi ích!
Sách đẹp, giá tốt, cuốn hút và tạo cảm giác thích thú khi đọc. Đĩa đọc rất rõ, giọng chuẩn và và rất cuốn hút. Cách học ạn văn này hiệu quả lắm đó. Mình đã nâng cao trình độ nhờ vào chuỗi sách này. Giá ở tiki cũng rẻ, giao hàng nhanh, sách giao nguyên vẹn nên sẽ ủng hộ hoài.
3
1235004
2016-06-21 18:26:21
--------------------------
451162
8757
Về mặt nội dung thì mình thấy rất được, những câu chuyện ngụ ngôn luôn ẩn chưa những bài học sâu sắc. Tuy nhiên vì nghe bản tiếng Anh nên cũng hơi khó, phải nghe lại vài lần mới hiểu hết được. Cá nhân mình đánh giá nội dung rất hay.
Về bản tiếng Anh và cd đi kèm. Mình thấy bộ sách happy reader này rất có ích trong việc luyện kĩ năng nghe và đọc hiểu. CD đọc khá rõ và tốc độ thì cũng vừa phải, số lượng từ mới cũng không bao nhiêu nên luyện nghe rất tốt đấy.
4
448018
2016-06-19 14:34:11
--------------------------
390351
8757
"Những câu truyện ngụ ngôn Aesop nổi tiếng" là một cuốn sách rất hay và bổ ích. Nó rất thích hợp đối với những người muốn nâng cao khả năng tiếng Anh. Cuốn sách có hình ảnh minh họa rất rõ ràng và đẹp nên càng bắt mắt hơn. Sách có cả đĩa CD, giọng ở đĩa rất hay, không bị rè. Về nội dung thì theo mình là ổn.
Còn về phần hình thức, bìa được thiết kế rất đẹp, đơn giản, đẹp và bắt mắt, giấy tốt, đọc không bị lóa, mỏi. Nhìn chung thì đây là một cuốn sách rất bổ ích.
5
1142307
2016-03-03 18:44:05
--------------------------
311848
8757
Nội dung sách là những câu chuyện ngụ ngôn Aesop. Sách không được dày lắm, bìa cũng ổn, khi nhận được hàng thì sách + CD được bọc chung với nhau khá là kĩ càng.  Sách có nhiều hình vẽ rất bắt mắt :) . Cứ qua một chapter là sẽ có một phần trò chơi nhỏ để ôn lại những câu chuyện phía trên. Mình thích nhất ở cuốn sách này đó là sau mỗi câu chuyện được kể sẽ có một câu nói tóm tắt và rất ý nghĩa phía bên dưới. Sách thích hợp với người muốn rèn luyện kĩ năng nghe và phát âm tiếng anh.
4
675686
2015-09-20 11:29:27
--------------------------
243488
8757
Cuốn sách này dành cho cả người lớn và trẻ em. Với người lớn, đã quen với những truyện ngụ ngôn xưa cũ, như con cáo và chùm nho, thì việc vừa nghe đĩa vừa đọc bằng các từ tiếng Anh trở nên bớt khó khăn hơn, khiến người học thêm phần hứng thú và đỡ nản. Chữ nào không rõ nghĩa thì mở sách, đã có tra từ sẵn. Với trẻ em, việc cho bé nghe âm tiếng Anh chuẩn sẽ giúp tạo phản xạ với tiếng Anh ở bé, nhất là sau đó bé lại được nghe mẹ kể lại chuyện bằng tiếng Việt và giải thích nếu bé chưa hiểu. Sách hơi mỏng nhưng giấy đẹp, chữ rõ ràng, cầm lên rất dễ chịu/
5
82774
2015-07-27 14:51:20
--------------------------
206286
8757
Các tác phẩm ngụ ngôn của Aesop đã nổi tiếng quá lâu, nhưng khi đọc bằng tiếng Anh thì lại có cách tiếp cận khác hẳn. Sách sử dụng từ vựng đơn giản, có hình ảnh minh họa cho từ vựng lẫn cho câu chuyện. Giấy tốt, có sẵn CD để có thể nghe và luyện. 
CHỉ có điều từ vựng lúc thì ghi lúc lại ko ghi cách phát âm cho nên muốn biết thì bắt buộc phải nghe CD chứ chưa học phát âm bằng cách đọc ngay được. Vừa có thể đọc truyện để giải trí, vừa có thể học tiếng Anh. Đây là cuốn sách không nên thiếu trong bộ sưu tập và cũng phù hợp với trẻ em và những người mới học vì đơn giản, dễ hiểu.
5
162924
2015-06-09 11:53:26
--------------------------
