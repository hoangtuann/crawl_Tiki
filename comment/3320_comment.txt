445320
3320
Mình mê series này lâu rồi, truyện vô cùng hay. Phần 2 này rất kịch tính, hấp dẫn, lôi cuốn. Đã cầm quyển truyện lên đọc rồi thì khó mà dứt xuống được. Tác giả xây dựng bối cảnh, nhân vật và tình huống đều tuyệt vời! Cực kỳ gây cấn và ấn tượng. Nhưng cá nhân mình thấy bìa Phần 2 không được đẹp so với phần 1 và 3. Mình là một người đánh giá sách qua bìa, nếu không đọc phần 1 thì có lẽ bìa kiểu này mình sẽ không bao giờ mua đâu. Tuy nhiên về giấy thì ổn, êm tay. 
5
400930
2016-06-09 23:48:22
--------------------------
393749
3320
Đầu tiên bìa sách không đẹp và có chút cứng nhắc đối với mình, làm mất cảm giác vì là một người thích sự đẹp đẽ long lanh của bìa sách. Bìa là một phần quan trọng của cuốn sách, tuy thế Vờn lửa vẫn nổi bậc vì nội dung của nó , quá cuốn hút và gây cấn, các tình tiết mới lạ hấp dẫn ngay từ đầu kéo dài đến phút cuối, cao trào rất nhìu nhưng bình lặng lại không. Phần 2 này trái ngược với phần 1 như bổ khuyết cho nhau.
Về phần giấy sách đẹp và dễ cầm trên tay.
4
1061830
2016-03-09 11:31:46
--------------------------
369284
3320
Tập 2 của series còn tệ hơn nữa trong vấn đề biên tập. Quá nhiều lỗi sai, từ chính tả, đánh máy, thiếu từ đến câu cú lủng củng. Đọc một cuốn truyện hay mà cứ vài đoạn rồi vài trang gặp lỗi thì rất bực mình. Tôi chợt nghĩ, có khi Chibooks không có biên tập viên thì phải, nếu không thì là quá cẩu thả. Dịch giả cũng vô trách nhiệm chẳng kém. Tôi đánh giá 4 sao bởi vì nội dung của cuốn sách, nếu đánh giá thấp nó chỉ vì khâu biên tập làm việc tắc trách thì quả là thiệt thòi cho nó. Tập Vờn Lửa vẫn kịch tính, gay cấn như phần 1, và có thêm những chi tiết mới mẻ. Đặc biệt là phần thoại, các nhân vật có nhiều câu đối đáp hay, thông minh và thực sự hài hước chứ không chỉ hài hước nửa vời như cuốn trước.
4
386342
2016-01-15 14:44:44
--------------------------
334010
3320
Những ai đã đọc phần 1 series Skulduggery Vui Vẻ hắn sẽ không bỏ qua phần 2 là cuốn Vờn Lửa. Quả thực là phần 2 hấp dẫn hơn, nội dung mở rộng ra, thêm nhiều các tình tiết mới mẻ nên đọc khá lý thú, đặc sắc. Và đặc biệt là phong cách truyện vẫn giữ được sự hồn nhiên, vui vẻ như phần 1. Ngày càng yêu mến thầy trò Skulduggery và Valkyrie. Nghe nói bộ truyện này có tới chín phần, nếu truyện vẫn giữ được phong độ như những tập đầu thì mình tin độc giả sẽ không ngần ngại mua trọn bộ.
4
6502
2015-11-08 18:09:25
--------------------------
300141
3320
Về hình thức, bìa sách không đẹp lắm, trông khá sơ sài, có vẻ như nhà xuất bản không đầu tư lắm vào bộ truyện này. Về nội dung, cốt truyện hấp dẫn hơn so với phần 1, tình tiết nhanh, gay cấn. Các nhân vật phản diện được xây dựng cũng đáng sợ đấy, nhưng có lúc cũng có những hành động, lời nói ngây ngô đến buồn cười. Đó là điểm rất đặc biệt của bộ truyện. Quyển này cũng tiết lộ một phần về cuộc sống trước kia của Skuldugerry nhưng anh vẫn còn là một ẩn số khiến mình rất tò mò. Theo mình biết, ở nước ngoài bộ truyện này đã kết thúc với 9 quyển, hi vọng Chibooks sẽ dịch hết bộ truyện này.
4
62057
2015-09-13 19:12:03
--------------------------
283561
3320
Vờn Lửa là phần hai của truyện skulduggery Vui Vẻ. Nó không còn đơn thuần chỉ là cuộc đối thoại như phần một. Phần hai này khiến tôi như bị lôi cuốn hơn với các cuộc chiến kịch tính, đầy ngoạn mục. Nó hấp dẫn một cách kì lạ, những tình tiết xảy ra trong câu chuyện đa dạng và phong phú hơn. Không gian bối cảnh được thể hiện nhiều hơn chứ không hạn chế như phần một. Nếu các đọc giả nào là fan của truyện viễn tưởng thì bộ series Skulduggery Vui vẻ là một lựa chon đúng đắn. 
Tôi đang có trong tay đã ba quyển trong trọn bộ series. Tôi hy vọng cuốn cuối cùng sẽ có mặt trên tiki sớm nhất có thể.
5
661393
2015-08-30 07:18:14
--------------------------
