472098
4670
Lần đầu tiên mình biết đến sách của báo hoa học trò cũng cách đây mấy năm rồi do ban mình giới thiệu. Tò mò nêm mình cũng mua về đọc thử nào ngò nghiện đến giờ luôn.
Cuốn sách này nằm trong bộ " Hãy nói yêu thôi đừng nói yêu mãi mãi" gồm có 3 cuốn.
Bìa sách dày, thiết kế bìa không quá cầu kỳ nhưng vẫn đẹp, màu xanh nhìn rất hài hòa. Khổ sách lớn bằng quyển sách giáo khoa làm lúc nhận hành mình hơi bất ngờ. Giấy in tốt, mực không bị lenm. Có nhiều hình vẽ minh họa bên trong sách nữa. Nếu có điều kiện các bạn nên mua cả một bộ này về đọc, rất hay.
5
398235
2016-07-09 09:55:09
--------------------------
384104
4670
đối với mình thì sách truyện được xuất bản bởi Báo Sinh Viên Việt Nam - Hoa Học Trò thì rất tuyệt , Bìa chất liệu giấy tốt, cứng cáp. Hình vẽ bên ngoài rất đẹp, thơ mộng.  cuốn sách này hoàn toàn không làm mình thất vọng. Những câu chuyện được viết nên bởi những trái tim trẻ vầ đầy nhiệt huyết.thật tuyệt khi được sở hữu quyển sách này trong tay , 1 chút ngọt ngào pha lẫn yêu thương. Đúng là 1 tác phẩm đáng đọc . nhất định sinh nhật lần này của bạn kia mình sẽ mua cả bộ cho bạn ấy 
5
544062
2016-02-21 20:07:14
--------------------------
323412
4670
Lúc nhận được sách mình đã khá bất ngờ, vì với giá này mà sách khá lớn. Bìa chất liệu giấy tốt, cứng cáp. Hình vẽ bên ngoài rất đẹp, thơ mộng. Những câu chuyện ngắn trong đây đều mang hơi hướm mơ mộng hơi quá nhưng mình không hề phiền lòng,cũng có những câu chuyện ngắn dễ thương, nhẹ nhàng. Hình vẽ minh hoạ trong cũng rất đẹp. Trước khi sưu tầm sách, đây là quyển sách đầu tiên mình có được, vì thế khi tái bản mình không ngại ngần gì mua ngay. Có lẽ quyển sách nào dưới  H2t đều có tiêu chí "rẻ, đẹp, hay, chất lượng" cả
4
320688
2015-10-18 19:07:06
--------------------------
306116
4670
Lí do đã đưa mình đến với tác phẩm này đó là vì mình đã bỏ lỡ seri "Hãy nói yêu thôi đừng nói yêu mãi mãi" lần trước nên lần này khi sách được tái bản lại thì mình quyết định mua về. Theo cảm nhận mình thì đây là 1 bộ truyện thành công với những bài viết của những người trẻ việt như Phạm Lữ Ân, Công Huy. Cũng chính vì nó được viết bởi những cây bút này nên những câu chuyện trong đây rất gần gũi với mình và mình như được nhìn thấy mình trong những câu chuyện trong đó vậy. Nó đem lại cho mình 1 cảm giác nhẹ nhàng về thứ tình cảm ngây ngô, bồng bột của tuổi trẻ, 1 chút ngọt ngào pha lẫn yêu thương. Đúng là 1 tác phẩm đáng đọc
5
39383
2015-09-17 13:03:08
--------------------------
278172
4670
Mình cũng không ngờ là cuốn sách lại to như thế (y) mình nghĩ với giá rẻ thì sách sẽ khá là nhỏ và không dày mấy nhưng mình rất bất ngờ khi nhận được hàng đó. Bìa sách đẹp, nhìn thơ mộng dễ sợ ^^ có điều khuyết điểm lớn nhất là chất lượng giấy. Mình không thích loại giấy này, quyển nhỏ nhỏ còn đỡ, quyển to như này mà sử dụng loại giấy này thì mình thấy không thích hợp, cầm không đã == các truyện bên trong là truyện ngắn nhưng nội dung thì không chê đâu được, chẳng kém các truyện dài đâu. Nói chung là sách ổn ^^
4
698682
2015-08-25 17:57:21
--------------------------
256350
4670
Mình chưa bao giờ bỏ lỡ bất cứ cuốn sách nào trong tuyển tập "Hãy nói yêu thôi, đừng nói yêu mãi mãi". Bởi vì mình rất tin tưởng những tác giả được tuyển chọn như Phạm Lữ Ân, Đoàn Lê Công Huy. Và cuốn sách này hoàn toàn không làm mình thất vọng. Nhũng câu chuyện được viết nên bởi những trái tim trẻ và đầy nhiệt huyết. Mỗi câu chuyện mang theo những mảnh đời khác nhau, những cảm xúc vui buồn khác nhau.Có thể có những truyện mang hơi hướng cổ tích nhưng mình không thấy phiền lòng vì điều đó. Vì cuộc sống, đôi khi cũng cần những phép màu.
5
62056
2015-08-06 19:59:15
--------------------------
252957
4670
Đây là quyển sách mà tôi cảm thấy ưng ý nhất trong bộ 3 quyển " Hãy nói yêu thôi, đừng nói yêu mãi mãi " . Các mẫu truyện của Phạm Công Luận, Ploy, Phan Ý Yên, Phan Hồn Nhiên ...đều hấp dẫn sự ham đọc sách của những bạn trẻ như tôi. Đôi khi có 1 chút gì đó sâu lắng chạy trong từng con chữ, 1 chút gì đó tinh tế, sắc bén được gợi lên từ chính văn phong của tác giả, 1 chút gì đó "cổ tích" và "nhiệm màu" mang hơi hướng về tình cảm, yêu thương và cũng có 1 chút gì đó hồn nhiên, vui tươi như thủơ tuổi teen. Bìa sách với màu xanh cực mát mắt, trang trí bìa rất đẹp và không kém phần bắt mắt. Nếu muốn mua, thì cuốn sách này là 1 sự lựa chọn ko tồi.
4
531757
2015-08-04 02:43:32
--------------------------
252547
4670
Lần đầu tiên mình đọc 1 cuốn trong serri 3 tập của" Hãy nói yêu thôi, đừng nói yêu mãi mãi" lúc đầu tựa đề khiến mình không thích lắm bởi mình là tuýp người chug thuỷ. Nên mình không mua nhưng được tiki tặng vào hoá đơn trên 500 ngàn nên mình đọc thử. Nó khiến mình lớn hơn trong suy nghĩ: không có gì bất biến đặc biệt đối với con người, người nào nói yêu bạn, hãy tin, còn ai nói bạn yêu bạn mãi mãi, là không chân thật. Đọc xong cuốn thứ 1, mình đã đặt mua trọn bộ của truyện.
Những tản văn muôn màu, kết thúc bất ngờ. Giờ nguyên bộ nằm trong kệ sách mình rồi, lâu lâu lấy ra đọc lại.
Cám ơn tiki
4
684106
2015-08-03 19:12:25
--------------------------
218772
4670
Đây là 1 tác phẩm nằm trong bộ 3  " Nói yêu thôi, đừng nói yêu mãi mãi" từng rất hot với các bạn đọc và đặc biệt là với mình . Mình biết đến tác phẩm qua 1 lần Tiki giảm giá mình rủ vài người bạn đặt ké chung , nó lựa quyển này . Khi nhận sách nó đọc xong có cho mình đọc và mình đã mê luôn , bây giờ mình đang đặt mua cả bộ . Mình khuyên các bạn đang yêu và đã yêu nên tìm đọc . Nội dung truyện tuyệt vời với những câu văn nhẹ nhàng nhưng sâu lắng , hình ảnh minh họa rõ nét , đẹp dễ hình dung . Và mình rất thích bìa sách , được đầu tư trang trí rất đẹp
4
562392
2015-06-30 21:21:13
--------------------------
212207
4670
Người ta cứ cho rằng: " còn là học sinh thì đừng nên yêu". Điều đó cũng đúng. Nhưng tình yêu tuổi teen đẹp lắm người ta à. Những cảm giác, những tình cảm ngây thơ và hồn nhiên. Ẩm ương có, chín chắn cũng có. Hãy nói yêu thôi đừng nói yêu mãi mãi là những câu chuyện tình yêu những bài tản văn mà khiến người ta khi đọc muốn mình trẻ lại một lần nữa để được yêu thương, được trải lại những cả giác kì lạ ấy. Tôi thực sự rất rất thích những câu chuyện những quyển sách như thế này. Từng muốn mình sẽ sở hữu toàn bộ series Trái tim thức tỉnh. Và chắc chắn sẽ là như vậy. Cuốn sách rất đáng để đọc và giữ cho riêng mình.
5
561541
2015-06-21 23:40:15
--------------------------
190505
4670
Trước đây, khi báo Hoa học trò ra mỗi tuần tôi đều để dành tiền mua cho kỳ được vì là học sinh chưa có nhiều tiền, tôi rất thích báo này và càng thích hơn khi cuốn sách "Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi - Trái Tim Tỉnh Thức" được xuất bản. Một cuốn sách hay, nhẹ nhàng, ý nghĩa cùng lời văn gần gũi, những câu chuyện tình cảm nhẹ nhàng thời học sinh, "tuổi teen" với biết bao cảm xúc, khoảnh khắc "cảm nắng" một ai đó sẽ mãi là hồi ức đẹp với bất cứ ai sau này hồi nhớ lại. Cuốn sách thiết kế đẹp, lời văn nhẹ nhàng, sâu lắng cùng cốt truyện không mấy xa lạ là những điều tôi tâm đắc nhất ở cuốn sách này. Một cuốn sách hay nên đọc.
5
425604
2015-04-29 18:04:38
--------------------------
190336
4670
"Trái tim tỉnh thức" là cuốn sách tôi sở hữu đầu tiên trong bộ " Nói yêu thôi, đừng nói yêu mãi mãi", thật không uổng khi bỏ thời gian ra đọc. Không có lời nào ngắn gọn và xúc tích hơn để phát biểu ra khi đọc xong là "Đáng đọc", vì đó là những cung bậc cảm xúc, những câu chuyện của mỗi tác giả, không rập khuôn cũng không thấy bị trùng lặp, cứ đọc rồi sẽ thấy tự nó thú vị. Ngoài ra trong sách còn có những hình ảnh minh họa khá bắt mắt, tạo ấn tượng khá tốt khi đọc. Và có một điều nằm trong những sở thích của mình là đọc những tập truyện ngắn có nhiều tác giả sáng tác, nó khiến tôi có cảm giác không nhàm chán, như được mở rộng cách suy nghĩ trong mỗi ngòi bút của mỗi tác giả vậy, không bị đồng điệu.
5
510045
2015-04-29 12:10:59
--------------------------
179435
4670
Tôi  đọc  Hoa học trò khá lâu và  Hãy nói yêu thôi đừng nói yêu mãi mãi là một trong nhữ g cuốn sách đầu tiên trong tủ sách của tôi. Tuy lúc ấy còn bé nhưng sách như tan  chảy vào tim tôi. Những truyện  ngắn về tình yêu luôn sâu  thẳm và tưởng chừng như bấy nhiêu ấy đã nói hộ cả lòng tôi. Vui, ghét, buồn, giận tất cả đều có đủ trong quyển sách này. Nhìn chung. Nếu bạn cần những lời khuyên về tình yêu thì  quyển sách  này viết ra là để dành cho bạn. Không phí tiền 1 chút nào đâu ạ!
4
254539
2015-04-07 10:16:09
--------------------------
175465
4670
Đã lâu rồi mới thấy lại tập sách này, cơ mà nhìn cái bìa có vẻ lãng mạn hơn ngày trước nhiều đấy nhưng không hiểu sao mình cảm thấy nó vẫn không thu hút bằng. Truyện là tập hợp của nhiều cây bút trẻ nổi tiếng hiện giờ nên khi đọc, truyện mang màu sắc của hiện đại, của tình yêu, của sự nhẹ nhàng êm dịu tuy cuốn này giọng văn có hơi trầm và buồn hơn những cuốn khác. Đọc mỗi một câu chuyện lại thấy rõ phong cách viết văn của từng tác giả, khá đa dạng và phong phú, nhiều lúc từng lời văn cũng mang tính triết lý nên mỗi khi đọc qua tôi lại phải ngẫm nghĩ lại ý của tác giả. Nhưng càng nghĩ thì càng thấy sự sâu sắc trong từng nội dung, vì thế trong những bộ sách mà Hoa Học trò phát hành thì tôi lại thích Hãy Nói Yêu Thôi, Đừng Nói Yêu Mãi Mãi hơn tất cả.
4
41370
2015-03-30 11:27:14
--------------------------
172970
4670
truyện nhẹ nhàng tình cảm mà cũng rất sâu lắng. lúc đọc cũng buồn lắm. nội dung hay không bị dàn trải. truyện cũng rất ý nghĩa. nên đọc một lần. được cái bìa đẹp giấy tốt. mình đọc ngấu nghiến quyển này trong đúng 2 tối. bởi vì là đi mượn. đọc xong muốn đọc tiếp nữa ấy. mình thích cả bộ truyện hãy nói yêu thôi đừng nói yêu mãi mãi. truyện nào đọc cũng hay. hình như truyện này là truyện cuối cùng hay sao ấy các bạn nhỉ. đọc thấy buồn hơn mấy tập trước nhiều lắm
5
571210
2015-03-25 12:05:28
--------------------------
171181
4670
Hẫy nói yêu thôi đừng nói yêu mãi mãi chắc chắn sẽ là cuốn sách yêu thích của bạn. Mình nhớ không nhầm thì Trái Tim Thức tỉnh là tập cuối của series này và cũng là tập mình thích nhất. Truyện được viết bởi nhiều tác giả , nhiều văn phong khác nhau,nội dung cũng rất đa dạng và phong phú không hề nhàm chán. Theo mình nhớ thì tập này khá buồn nhưng cũng rất nhẹ nhàng hợp để đọc thư giãn. Truyện rất đáng đọc.
  So với bản ngày trước thì bản bây giờ được đầu tư hơn nhiều ,giấy cũng chất lượng hơn mà giá chẳng thay đổi mấy.
5
526401
2015-03-21 13:26:13
--------------------------
170267
4670
Khi đã cầm cuốn sách trên tay, mình vẫn không tin là mình đã có nó! 
Cuốn sách này đã ra mắt trước đó lâu rồi, mà trước kia mình chỉ đc đọc ké của đứa bạn vì không kịp mua thì đã hết hàng! 
sách của 2! thì khỏi bàn về nội dung, quá tuyệt, lối viết trau truốt, nhưng cũng rất tình cảm. nhẹ nhàng nhưng cũng rất sâu sắc. 
Nếu như trước kia, bạn chưa kịp sở hữu cuốn sách này, thì còn chần chừ gì nữa. 
Điểm cộng của cuốn sách không chỉ nằm trong "ruột" với cách dàn trang, hình minh họa bắt mắt, dễ xem, mà mình thấy đợt tái bản này, bìa sách cũng rất mới mẻ, tươi sáng. Cuốn sách tưởng như đã cũ mà lại chẳng hề cũ cả về nội dung lẫn hình thức
5
190083
2015-03-19 19:38:56
--------------------------
