512657
58
Khi nhận được quyển sách này tôi có hơi thất vọng vì nó nhẹ quá, chất giấy cũng không thích lắm, màu tối, chữ nhỏ, khó đọc cho những ai có thị giác không tôt lắm. Tôi thấy giá của cuốn sách là hơi chát so với chất lượng giấy như thế này. Về nội dung thì không phải nói nhiều rồi, đây là tác phẩm hay và đáng đọc. Tôi chọn bản tiếng Anh để có thể cảm nhận rõ hơn về ý tưởng của tác giả, những điều mà khi dịch sang tiếng Việt ta sẽ không hiểu hết được. Tóm lại, coi như số tiền đó là mua những dòng văn của tác giả vậy
4
257617
2017-01-18 19:52:46
--------------------------
490281
58
Sách rất hay và rất bổ ích cho những bạn ham tìm hiểu và học hỏi chuyên ngành cũng như đọc sách English. Giá tiền khá hợp lí, tuy nhiên hàng sau khi nhận có phần cong 4 góc của bìa sách và sách có phần hơi khó đọc cho những bạn mắt kém (cỡ chữ khá nhỏ và giấy khá là đen).
5
1462553
2016-11-11 22:39:36
--------------------------
469346
58
Quyển sách bản tiếng Anh này có một cái bìa thật đẹp, đầy ý nghĩa, sách nhỏ gọn và chất lượng giấy không được tốt như bản tiếng Việt vì đây là phiên bản paperback. Nội dung sách thì đã quá kinh điển và tuyệt vời. Tôi rất thích nhân vật người cha là luật sư Atticus. Cách ông dạy con và đối xử với mọi người thật tử tế và chân thành đáng ngưỡng mộ, nhờ những người như ông mà chúng ta hiểu được kỳ thị chủng tộc là một tội ác ghê tởm, black lives matter, all lives matter. Atticus nói:  “You can never truly understand a person until you consider things from his point of view – until you climb into his skin and walk around with it.” / “Con không bao giờ thực sự hiểu một người cho đến khi con xem xét mọi việc từ quan điểm của người đó… tức là con sống và cư xử y như anh ta." 
5
88741
2016-07-06 12:03:49
--------------------------
249140
58
Đã đọc quyển "to kill a mocking bird" từ rất lâu nhưng lúc rảnh thì lại muốn lật lại những trang sách trong quyển sách ấy của tác giả Hamper Lee. To kill a mocking bird hay còn gọi là Giết chết con chim Nhại. chim Nhại là loài chim mang điềm lành cũng là biểu tượng xuyên suốt trong cả quyển chuyện. Câu chuyện kể nhiều về nhân vật  người Bố Autticus - một luật sư đáng kính và lũ trẻ thường hay chơi đùa với nhau Scout, Jemvà các bạn.Nổi bật trong nội dung quyển sách là ý nghĩa không được giết chết chim nhại cũng như phải bảo vệ những người tốt bụng, phải bênh vực sự công bằng mà không phân biệt màu da, sắc tộc. Tác phẩm lôi cuốn trên còn được dựng thành phim rất hấp dẫn. 
4
635964
2015-07-31 14:08:04
--------------------------
19390
58
Tôi yêu quyển sách này! Tôi đã đọc nó tại trường từ khi tôi mua và không thể nào có thể bỏ nó xuống được! Tôi nghĩ rằng đây là một câu chuyện hay dành cho cả trẻ em cho đến người lớn. Tuy nhiên, khi cho trẻ em xem quyển sách này thì phải thật cẩn thận vì có 1 vài chỗ sẽ gây tác động mạnh mẽ đến người đọc. 

Quyển sách nói về những năm 1930 tại một thị trấn nhỏ ở Maycomb, Alabama. Câu chuyện đã để lại cho tôi nhiều ấn tượng và suy nghĩ nhiều… nhất là về phương diện gia đình và sự bình đẳng của mỗi một con người. Các nhân vật đều được phát triển theo những dòng thời gian, địa điểm xuyên suốt cả câu chuyện.

Quyển sách rất hay. Đáng để đọc và đáng để suy nghĩ!

5
17933
2012-02-16 16:23:33
--------------------------
