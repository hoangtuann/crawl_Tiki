423010
2627
Tôi chọn mua sách này cho con để con có thể tự học nhưng hiệu quả không cao. Tên sách là "Tuyển chọn đề ôn luyện và tự kiểm tra Tiếng Việt" thì chỉ đúng phần đầu vì tương ứng với mỗi tuần học, sách chọn hai đề kiểm tra đưa vào; ngoài ra không có phần hướng dẫn hay đáp án, nhự vậy làm sao học sinh tự ôn luyện và tự học được. Tôi không hiểu sao các tác giả, người biên tập nội dung và nhà xuất bản ĐHSP lại bỏ sót phần này trước khi xuất bản và lưu hành sách. Mong các tác giả bổ sung vào sách phần hướng dẫn, hoặc ít nhất phải đưa đáp án vào, chứ không thì điều các tác giả hi vọng là "cuốn sách sẽ là tài liệu bổ ích giúp các em học sinh ôn luyện" sẽ là điều xa vời. 
1
257944
2016-04-28 16:06:23
--------------------------
