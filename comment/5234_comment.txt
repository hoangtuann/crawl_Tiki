410490
5234
Mình đặt 2 cuốn sách "Đảo chìm Trường Sa" và "Trường Sa" cùng 1 lúc. Bởi do cùng 1 NXB nên không hề nghĩ 2 cuốn sách này giống nhau. Cuối cùng, sau khi đọc xong quyển Đảo chìm Trường Sa, mở cuốn này ra thì thấy giống hệt. Chỉ khác cuốn này đưa phần thơ lên trước, phần truyện ngắn và nhận xét của độc giả xuống sau. 
Về nội dung thì thực sự rất hay, rất cảm động. Tuy nhiên có lẽ NXB nên chỉ để 1 cuốn thôi cho người đọc đỡ hiểu nhầm, mua 2 quyển giống hệt nhau về đọc, dù tên 2 cuốn sách khác nhau. 
4
676453
2016-04-04 10:50:54
--------------------------
