418459
3541
Một cuốn sách chất chứa rất nhiều điều đáng để suy ngẫm... Những vần thơ, những câu chữ trong truyện dẫu vô cùng giản đơn, mộc mạc nhưng như thể nói hết được tiếng lòng bên trong một con người. Thích nhất là những vần thơ, ngắt nghỉ tưởng vô tình nhưng hữu ý, giống như dòng cảm xúc ngập ngừng đứt quãng theo từng tiếng nấc nghẹn khi đứng trước những tổn thương, hay những ngã rẽ buộc mình phải lựa chọn... Đọc Lạc nhau giữa thành phố, bỗng trở nên yêu thơ September Rain đến lạ! Cảm giác như chính bản thân đang trải lòng ra trên từng trang giấy, rằng từng câu, từng chữ, từng vần điệu thơ kia đều của mình, thuộc về mình vậy...
5
1309789
2016-04-19 11:15:04
--------------------------
394772
3541
"Lạc nhau giữa thành phố", tựa hay, chứa đựng nhiều nỗi buồn dịu dàng. Làm sao ta sống đựơc mà không bíêt yêu thương kẻ nào, nhưng cũng bởi vậy ta đâu thể thương hết tất cả những người đã được sắp đặt sẽ bước chân vào đời mình. Suy cho cùng  ai rồi cũng sẽ có lúc phải bước qua một người, hoặc nhìn một người bước qua mà thôi!

Lạc mất nhau, ai mà chẳng đắng, chẳng đau,.lý trí vụn vỡ, trái tim tan nát. Mặc dù chia tay thật ra chưa hẳn là điều gì đáng sợ. Mọi thứ vẫn tiếp diễn chẳng chút ngập ngừng. Chẳng phải có câu "Cầu Vồng sau cơn mưa" sao.
3
672530
2016-03-10 21:50:20
--------------------------
318386
3541
Thật sự thì nếu đang trong một tâm trạng tồi tệ hay đang lúc trải qua một khoảng thời gian cô đơn thì mình nghĩ nên dành một cảnh báo "vàng" cho mọi người. Ngay truyện ngắn đầu tiên "Yêu thương vô vọng" đã làm tim mình nghẹn lại, nghẹn vì cũng chẳng biết mình từng như thế, hay là quá xót thương cho phận con gái chúng ta :). Càng đọc càng bị cuốn hút với những câu chuyện trong sách, nhiều tình huống nhưng đa phần đều dẫn đến một kết thúc buồn, đúng như tiêu đề "lạc nhau" của sách. 
Một điểm trừ nhỏ đối với mình đó là các bài thơ của September Rain chưa thực sự làm mình thỏa mãn. Có chút cảm giác các bài thơ hơi nhạt nhòa và không liên kết được với các truyện ngắn :).
4
481719
2015-10-05 21:18:30
--------------------------
288675
3541
Quyển sách này tưởng chừng như viết ra cho những trái tim đã từng tổn thương, đã từng lạc lối trên hành trình tìm kiếm hạnh phúc. Không chỉ là tuyển tập truyện ngắn,"Lạc nhau giữa thành phố" còn là sự đan xen của những vần thơ tạo cảm giác mới lạ và thích thú. Chất lượng giấy tốt, bìa đẹp và phông chữ nhìn cũng hợp mắt nữa. Thật sự cảm ơn những tác giả như Phạm Anh Thư, September Rain, Nguyệt Nguyên... đã viết nên một quyển nhật kí thú vị để mọi người để thể tìm thấy chính mình đâu đó qua từng trang viết.
5
330563
2015-09-03 19:54:38
--------------------------
258994
3541
Tiki gửi sách nhanh quá. Tối mình dành cả thời gian để đọc nó.. mỗi tản văn bài thơ trích dẫn đều mang một nỗi buồn một câu chuyện riêng của tác giả,  sách như viết cho cuộc đời mình, dù yêu ai làm gì nếu duyên không đủ dày thì cũng phải chia ly.. khóc than cho một chuyện tình yêu thương cách mấy nhưng lỡ một phút hững hờ cũng sẽ lạc mất nhau ở chốn 7 tỉ người dừng chân. Ta sẽ tìm thấy bản thân ta trong mỗi bài thơ những con chữ trong quyển sách này. Không phí thời gian của bạn nếu đã chọn đọc nó đâu
5
719684
2015-08-08 23:30:36
--------------------------
211332
3541
Cuốn sách đặc biệt hợp với các bạn đang hoang mang và cô đơn vì tình yêu. Mình nhìn thấy bản thân nhiều năm trước trong những câu chuyện ở cuốn sách này. Mình không biết diễn tả sao, nhưng cảm giác bắt được điều gì đó làm bản thân đồng cảm, cảm thấy rất mừng và trân quý. 
Các bài thơ cũng làm mình ấn tượng, mình đang tính in ra để dán nho nhỏ trên tường cho đẹp phòng.
Về các trình bày trong sách: font chữ đẹp, nhìn thoáng, sách cầm vừa tay.
Cảm ơn các bạn tác giả và nhà xuất bản nhiều.
5
4397
2015-06-20 14:04:14
--------------------------
210529
3541
Tình yêu là một đề tài muôn thở mà biết bao nhà văn, nhà thơ và những con người đam mê văn học muốn và đã viết về nó. Những câu chuyện, những vần thơ mà họ viết có thể là những câu chuyện bất chợt họ nghĩ ra hay vô tình được chứng kiến và cũng có thể đó chính là những câu chuyện của chính bản thân họ. Và Lạc Nhau Giữa Thành Phố chính là một tác phẩm như thế, những câu chuyện và những vần thơ được tác giả rất chân thực và rất đi sâu vào lòng người, đọc cuốn sách này làm cho mình có cảm giác đang đi giữa dòng cảm xúc của chính bản thân khi mà mình cũng đã từng trải qua những chuyện như thế. Với những ai muốn ôn về quá khứ, nhớ về những kỉ niệm đẹp đã từng có, những mong manh, những thương nhớ và nỗi yêu vô bờ thì Lạc Nhau Giữa Thành Phố sẽ giúp ta làm được điều ấy.
5
527557
2015-06-19 16:41:49
--------------------------
