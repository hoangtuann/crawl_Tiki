464753
8573
Khám phá bí ẩn thế giới tự nhiên về côn trùng phải nói là rất hay và thú vị. Thế giới côn trùng thật là phong phú và đa dạng, mỗi cá thể, mỗi loài có những đặc tính và hình dạng khác nhau. Có những loài côn trùng ta rất quen thuộc và có cả những loại côn trùng nghe lạ lạ một chút, nhưng qua những trang sách này đã giúp ta biết nhiều hơn về chúng. Đọc xong quyển này bất chợt nhớ đến truyện Dế Mèn Phiêu Lưu Ký của nhà văn Tô Hoài nè.

4
460351
2016-06-30 14:25:37
--------------------------
346020
8573
Sao chưa có ai đọc bộ sách Khám Phá Bí Ẩn Thế Giới này nhỉ? Tựa sách hay, hình ảnh minh hoạ sống động bắt mắt nữa. Mình cực kì yêu thích khám phá thế giới tự nhiên, dù đây là bộ sách dành cho lứa tuổi thiếu như nhưng mình vẫn thích. Hồi bé cũng có ước mơ lớn lên làm nhà khoa học nghiên cứu về động vật về thiên nhiên quanh ta. Bây giờ ngồi ở nhà đọc sách thôi cũng biết được thế giới ngoài kia có những gì rồi. Cuốn sách này dành cho những bạn muốn tìm hiểu thế giới các loài động vật trên trái đất. 
5
754539
2015-12-01 19:07:13
--------------------------
