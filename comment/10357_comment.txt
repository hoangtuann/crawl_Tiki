475663
10357
Mình đang định mua quyển này thì vào phần đọc thử xem trước, kết quả là quyết định không mua nữa. Lí do rất đơn giản.
Phần nói về diện tích, tác giả ghi là 9,5 triệu km2 và chiếm 1/5 lục địa toàn cầu. Dường như có sự nhầm lẫn tỷ lệ dân số thì phải, vì TQ đứng thứ 3 TG về diện tích, lục địa chỉ bằng một nửa Nga và tương đương Canada. Vậy ra Nga chiếm 40% toàn cầu, TQ và Canada mỗi nước 20% nữa. Ba nước này đã 80% sao?
Ngay trang đầu đã sai nghiêm trọng vậy, thì có lẽ rất khó để người đọc tin tưởng vào một sản phẩm đọc để biết sự thật như sách loại này.
2
343420
2016-07-15 16:24:10
--------------------------
285822
10357
Một quyển sách có dung lượng vừa phải để giới thiệu với độc giả về địa lý của một trong những nước lớn trên thế giới - Trung Quốc. Không quá nhiều những thông tin chuyên ngành, chi tiết nhưng sách đã đưa ra một hướng tiếp cận mạch lạc, dễ đọc dễ hiểu, mang tính khái quát về đất nước Trung Quốc từ Bắc xuống Nam qua nhiều vùng miền khác nhau. Sách được chia nhỏ thành từng chương rất khoa học, nội dung được dịch cẩn thận, chi tiết, in màu đẹp mắt, tranh minh họa phong phú. Dùng làm sách tham khảo để mở rộng kiến thức sẽ rất phù hợp.
4
32513
2015-09-01 10:35:22
--------------------------
273233
10357
Nếu bạn đang tìm kiếm một cuốn sách khái quát về địa lý của Trung Quốc thì không nên bỏ qua cuốn sách này. Qua Địa lý Trung Quốc, bạn sẽ có những thông tin cơ bản nhất về những vùng miền ở Trung Quốc. Tôi không nói cuốn sách này sẽ đầy đủ những thông tin mà bạn tìm kiếm, bởi vì thật sự Trung Quốc rất rộng, muốn hiểu nó thông qua một cuốn sách như thế thì không bao giờ là đủ cả. Sách đẹp, có những hình ảnh màu giúp bạn liên tưởng dễ dàng hơn những địa điểm ở Trung Quốc.
5
5485
2015-08-20 20:14:26
--------------------------
240922
10357
Thật sự mà nói, sách viết về Trung Quốc thì không bao giờ có thể đủ. Từ sự bao la, rộng lớn, hùng vĩ của thiên nhiên cho đến sự lớn mạnh của đất nước, sự lâu đời của văn hóa, thì quả thực mỗi quyển sách chú biên về Trung Hoa Đại Lục chỉ như một nét bút chấm phá vậy. Tuy vậy, thành thực mà nói, thì "Địa Lý Trung Quốc" hầu như mô tả hết toàn bộ một Trung Quốc kì vĩ, rộng lớn từ Bắc xuống Nam, từ tận Nam Á sang giáp Thái Bình Dương. Sách viết khái quát. Không phải là cuốn giáo trình cho những người nghiên cứu Trung Quốc, nhưng là một cuốn cẩm nang cần thiết cho những ai muốn tìm hiểu về đất nước này, hay có dự định đặt chân đến khám phá, du lịch tại đây. Có thể nói, đội ngũ làm sách vô cùng tâm huyết và chuyên nghiệp khi đầu tư công sức không nhỏ từ việc dịch giải, chú thích,...Sách còn được đầu tư bìa giấy tốt và in màu rất đẹp. Cầm sách trên tay, quả không thấy phí tài chính bỏ ra. Chỉ biết nói 1 từ "Hảo" mà thôi. Cảm ơn Tiki rất nhiều!!!
5
164758
2015-07-25 00:28:39
--------------------------
