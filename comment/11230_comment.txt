446399
11230
Cùng vói tập 14 thì tập 2 này là tập thứ hai mà nhóm Suneo, Jaian, Shizuka không theo cuộc hành trình từ đầu đến cuối nhưng độ hay thì không hề thay đổi. Câu chuyện mở ra trước mắt chúng ta một hành tinh như trong mơ với nhiều tài nguyên và sự trong sạch. Chỉ đáng tiếc, hành tinh thanh bình ấy lại đang phải đối mặt với một lực lượng vô cùng. Cũng nhờ sức hút ở đó yếu mà Nobita nghiễm nhiên trở thành một anh hùng, nó nhắc mình nhớ, trên đời này không có ai yếu đuối hết.
5
1420970
2016-06-12 10:15:32
--------------------------
395892
11230
"Doraemon" – là moitj bộ truyện tranh của tác giả người Nhật Bản Fujio Fujiko. Bộ truyện tranh có nội dung xoay quanh cuộc sống hàng ngày và những cuộc phiêu lưu ly kỳ, hấp dẫn của Nobita và nhóm bạn của cậu. Qua những câu truyện đó, ta có thể rút ra được rất nhiều điều thú vị và bổ ích như: lòng thương yêu, lòng dũng cảm và tình bạn cao đẹp giữa Nobita và Doraemon. Nobita thường bị Chaien bắt nạt nhưng luôn có Doraemon giúp đỡ trong việc đối phó với Chaien và giúp cậu học tập tốt hơn. Một bộ truyện tranh thú vị!
5
1142307
2016-03-12 16:20:30
--------------------------
386386
11230
Mình nhớ tên cũ của tập này truyện là "Bí mật hành tinh màu tím" bây giờ tái bản lại thì đổi tên thành "Nobita và lịch sử khai phá vũ trụ",  đọc truyện mà ký ức ngày còn bé cứ ùa về, nhớ lắm những nhân vật trong truyện. Trong tập này Nobita trở thành người hùng, dù ở hành tinh màu tím cậu được mọi người xem là siêu anh hùng và mong muốn cậu ở lại nhưng cậu vẫn chọn quay lại trái đất ở đó nobita chỉ là một cậu bé vụng về hậu đậu lại hay bị bắt nạt. Nhưng trái đất chính là nơi gia đình và những người bạn của cậu cùng chung sống, đoạn cuối chia tay cảm động đến phát khóc luôn.
5
471112
2016-02-25 16:46:06
--------------------------
383765
11230
đây là tập thứ 2 trong bộ truyện dài doraemon,nobita đã đến với thế giới của người bạn ropull bằng sự ngẫu nhiên khi lối vào tàu vũ trụ lại thông với đáy sàn nhà trong phòng học của nobita.Ở trái đất,nobita là người hậu đậu,yếu ớt nhưng khi lên hành tinh koyakoya thì cậu bỗng trở nên mạnh mẽ phi thường nhờ lực hút yếu ở hành tình này.Và cùng với sự giúp đỡ của doraemon,shizuka,suneo,jaian,nobita đã chiến đấu với bọn người xấu muốn chiếm đoạt khoáng sản để bảo vệ hòa bình trên hành tinh này.Đáng lẽ tập này nên giữ lại tên cũ "bí mật hành tinh màu tím" thì sẽ hay hơn 
5
1158730
2016-02-21 10:50:20
--------------------------
367169
11230
Một câu chuyện mới nữa đến với Nobita cùng các bạn. Nobita cũng Doraemon đã vô tình đến hành tinh Koyakoya - nơi rất xa với Trái đất ngoài vũ trụ, thông qua sàn phòng của mình. Ở đây có những sinh vật rất đáng yêu, cùng lực hút yếu hơn trên Trái Đất rất nhiều nên Nobita cùng các bạn của mình đã trở thành những anh hùng với sức mạnh phi thường cứu giúp dân cư trên hành tinh này thoát khỏi các kẻ ác đang âm mưu phát tan nơi đây để khai thác 1 loại khoáng sản quý chỉ nơi đây mới có ( đọc đến đoạn này tự dưng mình ngô nghê nghĩ đến 5 bạn nhỏ của chúng ta như là 5 anh em siêu nhân vậy hi )
Hình ảnh cuối truyện khi Nobita đã chia tay những người bạn ở hành tinh Koyakoya để trở lại cuộc sống bình thường thì cậu vẫn luôn nhớ về những kỷ niệm đẹp đã trải qua cùng những người bạn Ropull, Chami, Krem nơi xa xôi... mỗi khi nhìn các bông hoa tuyết - món quà được tặng nhằm nhắc nhớ đến mọi người, cũng khiến mình rất xúc động.
p/s: tập này tác giả cũng mang đến cho độc giả kiến thức về bước nhảy Alpha cũng khá thú vị
4
117702
2016-01-11 18:28:00
--------------------------
364179
11230
Trong tất cả các tập Doraemon truyện dài, mình vẫn ấn tượng và mê tập này nhất. Truyện kể về một nền hành tinh với nền khoa học vô cùng phát triển, con người hiền hoa đáng yêu, họ phải đối mặt với những kẻ hiếu chiến muốn khai thác khoáng sản quý mà bất chấp tất cả. Đây là tập hiếm hoi mà cậu bé hậu đậu, yếu ớt Nobita được trở thành "siêu nhân", "Người hùng" (cũng vì hành tinh này có lực hút yếu hơn trái đất rất nhiều) và đã cùng bạn bè sát cánh bảo vệ cuộc sống cho hành tinh ấy. Trong truyện này, nhân vật đáng yêu không chịu nổi, nhất là mấy con voi bay, xinh lắm luôn 
5
449880
2016-01-05 18:23:20
--------------------------
360139
11230
"Nobita và lịch sử khai phá vũ trụ" là tập truyện đã nói lên ước mơ của tôi từ tấm bé. Tôi mong mỏi được chu du trong dải ngân hà, được khám phá ra hành tinh mới, được cùng bạn bè vui vẻ chơi đùa, được trải nghiệm những thú vui tại một nơi không phải là trái đất. Doraemon không phải ngẫu nhiên mà trở thành tác phẩm in dấu ấn trong tâm trí tuổi thơ tôi, tôi thích Doraemon vì nó góp phần dựng xây ước mơ tôi. Tôi yêu chú mèo máy ấy vì chú mang đến niềm vui cho bao người trong đó có cả tôi. Doraemon là một truyện ngắn nhưng mang giá trị nhân văn sâu sắc
3
362041
2015-12-28 19:45:58
--------------------------
195819
11230
Tập truyện này kể về một nền văn hóa tồn tại trước trái đất rất lâu, khoa học đã rất phát triển, và cùng tồn tại trong Dải Ngân Hà. Các tình tiết truyện ăn khớp với nhau một cách đáng ngạc nhiên, từ khi Nobita mơ thấy Ropull trên tàu vũ trụ, đến việc cách cửa nhà kho dẫn đến tấm ván trên phòng Nobita. Nobita đến đất nước của Ropull và trở thành "siêu nhân" đi giúp đỡ mọi người chống lại tập đoàn khai thác Gattlie. Truyện mang đến cho người đọc nhiều cảm xúc và bài học, về đức hy sinh, về lòng quả cảm, sự kiên cường chống chọi, và trên hết là tình bạn cao đẹp. Không còn một từ nào để miêu tả nổi nữa! Thật sự rất đáng để mọi người cùng đọc và danh thời gian suy ngẫm những điều tưởng chừng bình dị như vậy. THÍCH TRUYỆN CỰC KỲ !
5
611569
2015-05-14 18:27:09
--------------------------
168491
11230
Mỗi câu chuyện doraemon lại mang đến cho mình những cảm xúc khác nhau, những tiếng cười hay những giọt nước mắt trẻ thơ lúc mình còn nhỏ.
Nói thật, toàn bộ truyện doraemon mình không bỏ sót một quyển nào, dù là vòi mẹ mua sách cũ rồi về nâng niu trau chuốt từng chút một.
Đến khi lớn rồi, khi đã biết nhìn đời bằng những con mắt không còn mơ mộng, không còn mơ ước đến phép thuật dịu kì nữa, đôi lúc đọc doraemon mình lại cảm thấy thoải mái và dễ chịu.
Yêu lắm, yêu lắm.
5
482828
2015-03-16 19:40:50
--------------------------
161616
11230
Trong 24 tập truyện dài Doraemon, tôi ấn tượng với tập này nhất. Thật lòng mà nói, tôi vẫn thích cái tên cũ “Bí mật hành tinh màu tím” hơn tên bây giờ. Vẫn là cuộc phiêu lưu của nhóm bạn Doraemon, Nobita, Shizuka, Suneo, Jaian đến hành tinh tím nhỏ bé xinh đẹp, hỗ trợ cư dân ở đó trong lao động và sát cánh bên họ trong bảo vệ hành tinh tươi đẹp của họ, nhưng khác với các tập truyện khác là vì sức hút ở hành tinh này rất yếu nên cậu nhóc Nobita vốn yếu như sên bỗng chốc trở thành anh hùng. Những loài vật ở hành tinh tím này từ chim chóc, thỏ đến voi đều đáng yêu cực kỳ.
5
393748
2015-02-28 15:39:11
--------------------------
