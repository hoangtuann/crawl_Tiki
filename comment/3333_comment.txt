447880
3333
Sau khi trở về từ Ai Cập 2 chị em sinh đôi và Rick lại tiếp tục tìm ra bí mật về Kilmore Cove, tấm bản đồ', Olivia,... các câu hỏi dần được giải đáp,tuy mạch truyện có chậm và kém hấp dẫn hơn tập 2 nhưng cũng rất thu hút. Từ nay cả 3 sẽ chính thức trở thành những anh hùng bảo vệ Kilmore Cove, tập sau chắc chắn rất đáng mong đợi. Mong rằng nhà xuất bản sẽ nhanh chóng ra mắt tập 4. Các bạn cũng nhớ đón đọc, theo mình câu chuyện còn dài và còn nhiều hấp dẫn
4
938445
2016-06-14 23:28:31
--------------------------
402910
3333
Kích thước hơi bé so với tưởng tượng của mình, có lẽ cũng vì thế mà nhìn bìa sách cảm thấy hơi quá dù mình là đứa rất thích màu mè nổi bật. Kích thước bé nên cũng khó lúc cầm đọc vì chữ sát với gáy sách quá. Tuy nhiên nội dung thì miễn chê. Mang đúng chất "phiêu lưu", vừa vui nhộn vừa dữ dội, không quá nhanh trong diễn biến truyện. Rất mong tiki có thể hỗ trợ thêm những cuốn sách như thế này nữa, mọi người nên mua cuốn này. đây là một cuốn sách hay
3
979895
2016-03-22 23:31:53
--------------------------
383572
3333
Trái với kỳ vọng, tập 3: Ngôi nhà gương lại thua kém tập 2. Sau khi bị mụ Oblivia cướp trắng trợn tấm bản đồ thì Rick và Jason trở về thế giới hiện thực (Oblivia đáng ghét đến nỗi chỉ là nhân vật thôi mà mình cũng ghét kinh khủng), cùng với Julia họ tiếp tục tìm hiểu những bí mật xoay quanh Ulysses Moore, cùng với đó là tính cách lấy lại tấm bản đồ. Nhiều vấn đề trong tập này đã sáng tỏ, nhưng diễn biến chậm quá, phải đến tập sau mới lại xuyên thời gian. Tựa đề Ngôi nhà gương khiến mình hình dung ra một nơi rất kỳ lạ và thú vị, nhưng thực tế lại không có gì mấy. Dù sao vì mạch truyện cuốn hút và có nhiều gợi mở nên vẫn mong chờ được đọc tập tiếp theo.
3
386342
2016-02-20 22:08:02
--------------------------
334142
3333
Một bộ sách hay là càng những tập về sau càng hấp dẫn. Và bộ Ulysses Moore là như vậy. Phần 3: "Ngôi nhà gương" rất rất hấp dẫn, sau những tình tiết bí hiểm ở các tập trước thì sự sáng tỏ trong tập này đủ sức làm thỏa lòng bạn đọc, nhưng mọi chuyện chưa khép lại đâu nhé, tất cả mới chỉ là bắt đầu mà thôi, bộ truyện này có tới 13 tập cơ mà, ở Việt Nam mới xuất bản đến tập thứ 3. Dựa trên yếu tố phiêu lưu, hành động, thám hiểm, kỳ bí tác giả Pierdomenico Baccalario đã vô cùng sáng tạo, mình tin rằng những tập tiếp theo rất đáng kỳ vọng.
5
6502
2015-11-08 22:06:06
--------------------------
301185
3333
Truyện dần gay cấn và hấp dẫn người đọc hơn cùng với sự chạy đua kịch liệt giữa hai bên. Quay trở lại sum họp tại lâu đài, cả 3 đứa trẻ khám phá được có nhiều cánh cửa thời gian trong thị trấn. Chúng cố gắng tìm kiếm lại tấm bản đồ bị cướp và tìm mọi cách bảo vệ những nơi có cánh cửa thời gian khỏi mụ Oblivia. Sự thật về sự bí ẩn của thị trấn dần hé lộ. Chân tướng, tham vọng của mụ Oblivia dần phơi bày. Đồng thời bọn trẻ cũng dần nghi vấn về cái chết của người chủ cũ. Có thật sự ông đã chết hay còn sống? Đáng tiếc cho ông thợ đồng hồ đã tin lầm Oblivia kể cho mụ nghe bí ẩn của thị trấn. Hối hận vì sai lầm của mình ông đã bỏ trốn sang thế giới khác. Tuy nhiên ông có bị mụ Oblivia tìm ra không thì phải chờ đáp án trong các tập kế. Tuy tập này tình tiết dồn dập nhưng mình lại không thích bằng tập 2. Mong chờ các tập tiếp theo.
4
43129
2015-09-14 15:25:28
--------------------------
219024
3333
Mình biết đến quyển này khi mượn được tập một của người bạn thân. Tính mình thích đọc mấy truyện huyền bí, phiêu lưu, đầy bí ẩn nên vừa đọc xong quyển này là kết ngay. Nay mình đã có trong tay 3 tập của bộ truyện và đang rất mong chờ những tập tiếp theo của bộ truyện. Một điều mình rất thích đó là khoảng cách chữ rộng, thoáng, không chi chít như nhiều truyện chữ dài khác nên khi đọc rất thoáng mắt và giúp mình nắm bắt tình tiết dễ hơn. Nếu bạn chưa tùng thì hãy thử một lần theo dấu chân của cặp chị em song sinh Jason, Lulia và người bạn đồng hành Rick đi khám phá những điều mới mẻ nhé ;)
5
633907
2015-07-01 08:11:31
--------------------------
