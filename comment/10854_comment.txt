53998
10854
Mình đã đọc nhiều cuốn sách thuộc thể loại chân dung văn học nhưng hiếm có cuốn nào để lại nhiều ấn tượng đậm nét như cuốn sách này. Ngay từ tiêu đề thôi đã có một cái gì đó hết sức đặc biệt và mới lạ. "Những người rót biển vào chai" kể cho chúng ta nghe  những câu chuyện thú vị và lôi cuốn về những gương mặt tài năng trong nền văn học Việt Nam, mỗi trang viết là một bức chân dung chân thực và rõ nét. Tác giả không chỉ viết bằng sự hiểu biết, mà còn bằng tình cảm chân thành, trong sáng dành đến cho những nhân vật ấy. Cuốn sách như một phút lắng lòng cùng văn chương, gửi gắm biết bao điều tuyệt vời cho người đọc.
4
20073
2013-01-06 15:16:53
--------------------------
