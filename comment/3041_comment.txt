255835
3041
Truyện tranh Việt Nam, nhất là truyện tranh cho thiếu nhi gần đây đã được quan tâm nhiều hơn rồi, vì thế mình rất ủng hộ những cuốn truyện như thế này. Truyện có các chi tiết dựa trên truyện cổ tích Việt Nam, dùng các hình ảnh quen thuộc như rồng làm mưa, cá chép hóa rồng.... tạo nên đặc trưng riêng, khác với truyện nước ngoài. Nội dung tuy chưa mới nhưng cũng thú vị vì được viết dựa trên những tình tiết của truyện cổ tích. Mình thích bài thơ ở cuối sách lắm, nếu như cả cuốn được viết bằng thơ như thế thì hẳn cuốn sách sẽ còn tuyệt vời hơn.
5
82774
2015-08-06 14:07:21
--------------------------
