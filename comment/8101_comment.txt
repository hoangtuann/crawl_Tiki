361148
8101
Những câu chuyện về lòng dũng cảm là một cuốn sách khá thú vị gồm những mẩu chuyện nhỏ, nhẹ nhàng giống như một lời tự sự kể về những khó khăn của các bạn nhỏ và cách các bạn ấy dũng cảm chấp nhận sự thật và vượt qua khó khăn với sự giúp đỡ của gia đình và những người xung quanh. Những câu chuyện đó đem lại giá trị nhân văn, có tính giáo dục nhưng không giáo điều, thu hút các bạn đọc nhí. Bộ sách này có nhiều cuốn về lòng yêu thương, lòng vị tha, về tình bạn,... 
4
219702
2015-12-30 14:58:49
--------------------------
