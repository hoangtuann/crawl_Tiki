489492
11404
Mình không bàn về nội dung sách vì nó thuộc hàng kinh điển rồi. Cái mình muốn nói là về hình thức của nó. Mình mua cuốn này để sưu tập lại nên cực kỳ thất vọng khi nhận sách. Giấy xấu, thô, đóng gáy nhìn rất cẩu thả. Font chữ cũng xấu. Mình là đứa chuộng đồ đẹp, đặc biệt là sách. Vậy nên nhìn 2 cuốn này mình ngán đến nỗi không muốn đọc. Quá thất vọng!!!!
2
453012
2016-11-08 10:50:00
--------------------------
473499
11404
Đây là một cuốn sách rất hay nói về xã hội nước Anh ngày xưa. Đây cũng là một trong những đề tài tôi rất thích nên tôi rất tâm đắc với cuốn sách này. Song, không biết là vì tôi không quen với lối hành văn của nhà văn nước ngoài hay sao mà bản thân tôi cảm thấy tác giả có nhiều phần giải thích rất dong dài. Thậm chí những thứ giải thích đó không hề cần thiết. Theo tôi, người đọc sẽ tự rút ra được bài học trong một quyển sách mà họ tìm đọc mà không cần bất cứ lời giải thích nào. Hội chợ phù hoa thật ra là một câu chuyện rất dễ hiểu nhưng tác giả lại giải thích nhiều vô kể. Thậm chí có phần giải thích nhiều đến mức khiến người đọc khó chịu. Đấy là điểm duy nhất tôi không thích ở Hội chợ phù hoa.
3
59453
2016-07-10 17:25:30
--------------------------
446585
11404
Giọng văn của tác giả đậm chất trào phúng, châm biến. Mặc dù, tác giả cứ phải khẳng định liên tục trong tác phẩm rằng Amelia là nhân vật chính của nhưng độc giả có thể dễ dàng nhận thấy nv chính ở đây là Rebecca. Mỗi khi viết về Amelia- một cô tiểu thư nhà giàu trong sáng thánh thiện tác giả không khỏi có chút giễu cợt, bởi vì được bao bọc trong nhung lụa từ bé nên cô trở nên cả tin và có phần ngu ngốc, mờ nhạt với một tính cách khá tầm thường như thể cô là "thánh sống" vậy. Còn Rebecca- một cô gái có xuất thân tầm thường nhưng sắc sảo, quỷ quyệt và quyết đoán đã từng bước tiến vào tầng lớp thượng lưu. Tuy phê phán cô nhưng tác giả cũng có những lời văn rất nhân văn cho cô. Có nhiều người coi Rebecca là nhân vật phản diện nhưng tôi thấy cô chính là mẫu người phụ nữ dám nghĩ dám làm, biết mình muốn gì và hành động để giành lấy điều mình muốn, nếu sinh ra trong một gia đình khá giả hơn, có bố mẹ yêu thương chỉ dẫn không bị tầng lớp trên khinh thường cản trở do xuất thân của mình liệu cô có phải hành động như thế. Thông qua cái nhìn của Rebecca, những người ở tầng lớp thượng lưu bị lột trần bộ mặt thật giả dối của mình. 
Và chính tác giả đã viết amelia như con chim non trong tổ trong tổ ấm thì Rebecca là con chim ưng với móng vuốt sắc nhọn tự do vùng vẫy trên bầu trời. Cho dù cái kết không có hậu lắm dành cho Rebecca nhưng cô sinh ra với một đôi cánh và nó dùng để bay nên nếu có rơi xuống thì cũng chẳng sao.
5
1207687
2016-06-12 15:32:43
--------------------------
435216
11404
Đây là một tác phẩm kinh điển mà ở đó đã vẽ ra bức tranh rộng lớn của xã hội Pháp thế kỷ XIX. Một xã hội đúng như cái tên của tác phẩm là Hội chợ phù hoa. Giới thượng lưu mang bộ mặt xa hoa, hào nhoáng, sang trọng nhưng lại ẩn giấu bản chất bên trong đầy rẫy sự xấu xa, giả dối. Những điều đó được thể hiện rõ nét thông qua nhân vật Rebecca, một cô gái không từ bất kì thủ đoạn nào để được leo lên tầng lớp quý phái, trở thành một quý bà thượng lưu, đẳng cấp. 
Hội chợ phù hoa đã cho người đọc cái nhìn toàn cảnh về xã hội Pháp thời bấy giờ, nhất là ở tầng lớp thượng lưu.
4
622631
2016-05-24 18:45:34
--------------------------
392832
11404
Nghe danh tiếng tác phầm này từ lâu rồi nhưng khi cầm lên tay, tôi mới biết đây không hề là một tác phẩm dễ đọc, đọc nhanh hay đọc lướt như những cuốn tiểu thuyết lãng mạn tôi thường đọc. Ở những trang đầu hơi tạo cảm giác ngán nhưng sau đó tôi dần dần bị cuốn theo dòng truyện lúc nào không hay. Hai cô gái là bạn cùng trường nhưng khi bước chân vào giới xã hội thượng lưu lại có những chuyển biến dẫn đến những kết cục đáng được nhận. Có lẽ tôi cũng như mọi độc giả khác, thích cái sự châm biếm, trào phúng của tác giả tạo nên trong câu chuyện này.
4
454536
2016-03-07 19:27:26
--------------------------
379660
11404
Chốn xa hoa , phù phiếm lôi cuốn cô gái vào sự khó chịu , kiểm soát được nỗi buồn xâm chiếm lâu nay , trọn bộ 2 cuốn sắp xếp được quá trình học hành gian khổ của từ bỏ thói quen xấu xí đấy để dùng làm công việc bảo vệ lấy chính sinh mạng của người có thế lực trong cả xã hội , thấm nhuần các công việc để chờ đợi sự tiến triển mới của tương lai , loại trừ khuyết điểm lớn nhất của cô là sự mờ nhạt trong chính ma lực .
4
402468
2016-02-11 23:42:19
--------------------------
338640
11404
Đọc Hội chợ phù hoa vào thời điểm này có vẻ không phù hợp lắm nhưng đem lại nhiều giá trị nhân văn hơn là giải trí về tinh thần. Có lẽ cũng chọn lựa đối tượng theo lứa tuổi đọc vì đây đúng là tác phẩm không phải dễ nuốt. Mình chưa đọc tác phẩm kinh điển nào mà ngôn ngữ cổ nhiều như vậy. Đôi chỗ mình còn phải đọc đi đọc lại mới hiểu được tác giả muốn nói gì. Dành cho bạn nào muốn giết thời gian và đang thiếu hụt những cảm xúc về đời thường. 
5
854791
2015-11-16 17:02:03
--------------------------
326342
11404
Đúng như tên gọi Hội Chợ Phù Hoa, nhà văn William Makepeace Thackeray đã khắc họa lại đúng bản chất của một xã hội phù phiếm điển hình không chỉ ở nước Anh lúc bấy giờ mà đó còn là cả một xã hội xa xỉ, phù phiếm phù ẩn hiện tận đến ngày nay. Cái giá trị của con người dường như chẳng còn là nhân cách, là đạo đức mà chỉ còn là mưu mô, giả dối và tiền bạc, gấm lụa khoác lên đã làm cho một kẻ hèn mạc, bỉ ổi trở thành một trưởng giả, một người thuộc tầng lớp quý tộc.
Nhân vật chính là cô gái thông minh, đa tài Rebecca. Xuất phát ban đầu cô ấy là người có tư chất, hoàn toàn có thể tiến thân bằng chính năng lực thực của mình. Nhưng một phần vì xã hội ưa thích cái mác xuất thân danh thế, phần vì bản tính vụ lợi, tham lam, Rebecca đã sử dụng chính sự thông minh kèm với tính xảo quyệt của mình để từng bước leo lên bậc thang danh vọng. Cô ta không từ một thủ đoạn nào để đạt được vị trí mà cô ta khao khát. Cô ta đạp bằng mọi nấc thang đạo đức, lừa đảo, phản bội, chiếm đoạt và cả cướp bóc những thứ vốn dĩ thuộc về những người cao quý khác về cho chính mình. Bằng tất cả thủ đoạn xảo trá, quỷ quyệt của mình, cô ta đã tạo ra một vỏ bọc hoàn hảo bên gấm lụa phấn son. Nhưng "sống trong chăn mới biết chăn có rận", cũng chỉ có gia đình cô ta mới hiểu cái giảo hoạt và gian ngoa của cô ta. Cái kết thúc của cô Rebecca thật sự không khiến người đọc ngạc nhiên.
Bên cạnh nhân vật chính, nhà văn  Thackeray còn xây dựng một hệ thống nhân vật mà mỗi người là một cá tính, một hoàn cảnh điển hình. Đọc tác phẩm Hội Chợ Phù Hoa, sẽ không ít lần ta tức giận trước sự ngây ngô, cả tin đến mức ngu ngộc của nhân vật nữ, cũng không ít nhân vật quá quan tham, lố lăng... Tất cả đều tạo thành một bức tranh hoàn chỉnh về cái xã hội suy đoài đạo đức.
Những giá trị mà tác phẩm Hội chợ phù hoa mang lại quả thật đã được thời gian chứng minh. Đây thật sự là tác phẩm kinh điển, cần có trong tủ sách gia đình chúng ta.
4
281896
2015-10-25 14:11:34
--------------------------
157273
11404
Với giọng văn trào phúng sâu sắc,William Makepeace Thackeray đã khắc họa rõ nét một xã hội thượng lưu nước Anh lúc bấy giờ với những nhân vật mang tính cách đặc trưng,riêng biệt.Nhân vật chính-Rebecca,một cô gái thông minh nhưng vô cùng xảo quyệt.Với tham vọng được giàu sang,đặt chân vào xã hội thượng lưu ,cô không từ mọi thủ đoạn trong việc lấy lòng,quyến rũ bất kì ai có lợi cho mình,phản chồng ,phản bạn,bỏ con.Nhưng cuối cùng,"quả báo nhãn tiền",cô nhận ra tất cả chỉ là "phù hoa",phù du,không có hạnh phúc.Cái giá cô phải trả vô cùng đắt!
Tôi vô cùng ngưỡng mộ tình cảm của anh chàng Đốpin.Anh yêu cô nhiệt thành,với tất cả sự trân trọng và ngưỡng mộ.Anh chắp cánh cho cuộc hôn nhân giữa bạn thân và người mình yêu,không chút toan tính.Theo đuổi cô 15 năm trời,tình yêu cuối cùng cũng được đền đáp xứng đáng.Tôi cũng mừng cho Amelia-cô gái trong sáng,đức hạnh,chung thủy được có hạnh phúc."Hội chợ phù hoa"đúng là một kiệt tác văn học,phản ánh được thế giới nội tâm con người ,cuối cùng chứng minh luật nhân quả luôn luôn đúng.Hãy đọc và cảm nhận sự thật đằng sau bộ mặt nạ !
5
415349
2015-02-08 09:29:28
--------------------------
111761
11404
Cả một xảy hội thu nhỏ với đầy đủ các thành phần của nó, trong đó nổi bật lên là nhân vật chính Rebecca, với tính cách tinh ranh, lưu manh cực kỳ ấn tượng. Như một con rắn len lỏi lên những bật thang ngày càng cao của xã hội thượng lưu. Thế nhưng lưới trời lồng lộng, không ai, dù khôn ngoan hay tinh ranh đến mấy cũng có thể thoát được luật nhân quả muôn đời. Kết cuộc, người tốt cũng được hưởng hạnh phúc. Kẻ mưu ôm cũng phải nhận trái đắng cho những toan tính của mình. Văn chương châm biếm, trào phúng sâu sắc. Lối viết văn vững chắc, già tay, gây ấn tượng sâu sắc trong lòng người đọc. Mình cứ băn khoăn tự hỏi không biết có phải Vũ Trọng Phụng đã lấy cảm hứng từ đây để viết Xuân Tóc Đỏ hay không?
5
337028
2014-05-03 12:35:08
--------------------------
