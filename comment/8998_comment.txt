334012
8998
Phải nói là một quyển sách hay nhưng nội dung của nó khiến cho người đọc khó nắm bắt được, có lẽ vì câu từ trong sách viết làm cho tôi thấy khó hiểu. Bởi lẽ đây cũng là lần đầu tiên tôi được tiếp xúc với cách viết như thế này.
Nội dung sách lúc đầu chủ yếu nói về tình hình thị trường chung ở những năm cuối thế kỷ 20. Tôi phải đọc 3 lần mới hiểu được quy luật và cách vận dụng của các ngân hàng vào việc điểu chỉnh giá vàng.
Và một câu nói vui của tôi : nếu tôi được đọc quyển sách này ở những năm 1990 thì chắc giờ tôi cũng kiếm một món lời lớn.
3
840786
2015-11-08 18:13:19
--------------------------
295744
8998
Lúc tôi tìm kiếm ở các nhà sách trên mạng, gần nhà, các thư viện, thì có thể nói đây là quyển sách duy nhất nói về đầu tư vàng. Nội dung quyển sách cho tôi rõ các khái niệm cơ bản về vàng, vàng thừ đâu mà có, vàng sản xuất như thế nào, các quốc gia nào lưu trữ vàng nhiều nhất.....Đáng buồn những nội dung không bao gồm những chiến lược đầu tư, những điều tôi cần tìm hiểu trong quyển này không có, quyển này do đặt mua trên mạng, tôi chưa thể đọc hết tường tận nội dung, nếu là ở nhà sách tôi sẽ không mua quyển này.
2
419763
2015-09-10 16:44:32
--------------------------
282899
8998
Cuốn sách viết khá chi tiết về thị trường vàng, về cách thị trường vàng ra đời và tồn tại. Tuy nhiên, tất cả những gì cuốn sách này viết chỉ dưới dạng vĩ mô, nơi mà các bên trong thị trường chủ yếu là các ngân hàng trung ương, còn đối với chiến lược đầu tư vào vàng hầu như không có. Nếu ai đó muốn biết thêm về thị trường vàng dưới dạng học thuật thì đây là một lựa chọn không tồi, còn nếu muốn tìm các chiến lược đầu tư kinh doanh vào thị trường này thì cuốn sách này là hoàn toàn vô nghĩa.
4
703335
2015-08-29 15:35:09
--------------------------
265409
8998
Trong một cuốn sách có tiêu đề "Đầu tư vào vàng", tôi nghĩ không nên đưa cả những khái niệm có phần...hóa học vào như ký hiệu hóa học của vàng là gì. Ngoài điểm trừ đó ra, các phần khác viết đều rất dễ hiểu, cách thế giới tài chính hiện đại và quá khứ sử dụng vàng trong nền kinh tế, hay ảnh hưởng của nó tới các vấn đề khác...đều khá ngắn gọn và dễ hiểu. Với nội dung chỉ gói gọn trong 280 trang thì lượng kiến thức như vậy là phù hợp.
4
268746
2015-08-13 22:16:30
--------------------------
168566
8998
Lúc đầu mình mua quyển sách với ý định là hiểu rõ về vàng, thị trường vàng cũng như chiến lược đầu tư vào vàng. Nhưng khi sở hữu rồi và đọc qua thì mình thực sự chỉ hiểu được vàng, chung chung về thị trường vàng còn phần chiến lược đầu tư thì thực sự mình chưa thấy đề cập trong quyển sách như tựa đề quyển sách là "Đầu tư vào vàng". Mình nghĩ quyển sách này chắc chỉ đọc để hiểu biết thêm về vàng là gì, khai thác vàng, đặc tính của vàng,... còn để có thể đầu tư vào thị trường vàng thì chắc quyển sách này không phù hợp.
1
578058
2015-03-16 21:31:41
--------------------------
162709
8998
Trước giờ bản thân tôi cứ nghĩ vảng chỉ làm trang sức. Sau này tôi còn nghe tới buôn vàng chứ không bao giờ nghĩ tới chuyện đầu tư vào vàng. Khi đọc quyển sách này tôi mới vỡ lẽ ra là có cả đầu tư vào nó. Có lẽ những hiểu biết về vàng của tôi quá nông cạn nên nội dung trong sách còn khá khó hiểu đối với cá nhân tôi. Vàng là một kim loại có giá trị nên rất nhiều người đổ xô vào nó, đặc biệt là ở Việt Nam. Vàng có những quy định giao dịch khắt khe và những quy định khác do chính phủ ban hành. Để tìm hiểu về đầu tư vàng thì độc giả hãy tham khảo quyển sách này. Một tư liệu hữu ích cho ta về các khái niệm đơn giản về vàng - kim loại có giá trị lớn.
4
387632
2015-03-03 01:38:53
--------------------------
162143
8998
Trên thế giới hiện nay, vàng, chứng khoán, ngoại hối được xem là những kênh đầu tư phổ biến. Nếu như chứng khoán là kênh đầu tư trong giai đoạn nên kinh tế tăng trưởng thì vàng là kênh đầu tư trong điều kiện nền kinh tế lạm phát cao. Nội dung quyển sách cung cấp cho chúng ta những kiến thức khá là đầy đủ để chúng ta có thể hiểu rõ hơn về thị trường vàng trên thế giới tuy nhiên nội dung chỉ xoay quanh những khái niệm, còn về phần chiến lược thì chưa cụ thể không có phần chuyên sâu. Đây có lẽ là thiếu sót của quyển sách này.
3
356759
2015-03-01 16:53:43
--------------------------
72891
8998
Cách đây 3000 năm, vàng đã trở thành một loại hàng hóa đặc biệt, vượt trên mọi loại hàng hóa khác, vượt khỏi phạm vị của một dân tộc, một lục địa, vàng trở thành phương tiện thanh toán, cất trữ xuyên biên giới bởi những đặc tính của nó.
Ngày nay, tại Việt Nam, lạm phát vẫn tăng cao nên người dân vẫn luôn có xu hướng đầu tư vào vàng để ít chịu ảnh hưởng nhất. Đây là một kênh đầu tư siêu lợi nhuận nhưng cũng ẩn chứa rất nhiều rủi ro. Thị trường vàng trong nước biến động liên tục và rất khó dự đoán, vẫn còn khoảng cách xa với giá vàng thế giới. Cuốn sách này sẽ cung cấp các thông tin hữu ích cho các bạn trẻ và các người mới tham gia vào thị trường vàng về những kiến thức cơ bản nhất của vàng và sự lưu thông của vàng trong nền kinh tế. Chắc chắn đọc xong cuốn sách này, người đọc có cơ sở vững chắc để hiểu đúng thị trường vàng và đưa ra những quyết định đầu tư tốt nhất cho mình.
3
29716
2013-05-05 10:44:16
--------------------------
72436
8998
Thị trường kinh tế hiện nay phụ thuộc vào đồng tiền nhưng tiền lại phần lớn không nằm trong chính các ngân hàng mà nằm trong chính người dân. Nhưng với tình hình kinh tế không ổn định, với mức lạm phát cao, tăng vọt và phát triển kinh tế ì ạch của một số nước trên thế giới thì thay vì đầu cơ tích trữ tiền bạc thì họ lại đổ vào vàng. Nghe có vẻ mới mẽ nhưng thực ra những việc làm này dường như đã ăn mòn vào tâm trí người dân từ rất lâu rồi. Cuốn sách cho chúng ta cách nhìn có thể nói là đúng đắn và chính xác nhất về đầu tư vào vàng.
4
99341
2013-05-02 17:16:43
--------------------------
