195312
6719
Người đẹp và quái vật là một câu chuyện cổ tích đã có từ rất lâu rồi. Câu chuyện mang đến cho chúng ta một điều mới mẻ trong thế giới cổ tích ấy. Trong đó, tôi ấn tượng với nhân vật chính nhất. Một cô gái thôn quê vì cứu cha mình mà phải ở lại lâu đầu của một quái vật. Nhưng cô đã cảm hóa được tên quái vật đáng sợ ấy và giải cứu cho lâu đài thoát khỏi lời nguyền. Tôi ấn tượng với cô gái ấy ở sự hiếu thảo, cũng như sự thẳng thắng của mình. Trong tòa lâu đài ấy, có lẽ chỉ có cô mới cãi lại tên quái vật, và cô đãmang đến ánh sáng cho những kẻ đang chờ một người mang cây đuốc đến cho họ.
5
13723
2015-05-13 13:48:14
--------------------------
