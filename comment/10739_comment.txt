477256
10739
Những cuốn sách trong bộ Horrible Geography đối với tôi mà nói cứ như những kho báu vậy. Những cuốn sách mang trong mình vô vàn kiến thức, nhưng cách diễn đạt lại rất mềm mại và lôi cuốn, khiến người đọc không cách nào dứt ra khỏi mỗi trang sách được. Những kiến thức phổ thông và cả những kiến thức độc đáo, tất cả đều dễ dàng được người viết truyền tải qua những trang sách ngắn gọn nhưng lại đầy đủ. Nếu như bạn có niềm yêu thích say mê bất tận đối với khoa học nhưng lại hoàn toàn không thích những quyển sách dày cộp cả trăm trang, thì đây chính là cuốn sách dành cho bạn .
5
291067
2016-07-26 09:13:32
--------------------------
385288
10739
Mình là một người rất yêu thích khám phá nên một cuốn sách về các nhà thám hiểm là một cuốn sách không thể bị bỏ qua với mình và quả thật đây là một cuốn sách hay, nó cho thấy tinh thần khám phá của con người từ ngàn xưa đến nay, chúng ta không ngừng muốn thám hiểm những vùng đất mới dù việc chúng ta làm với những vùng đất đó không phải lúc nào cũng tốt đẹp nhưng thế giới ngày nay sẽ không thể nào phát triển như bây giờ được nếu không có các nhà thám hiểm thủa xưa đã giong buồm đi khám phá những vùng đất mới, và đó luôn là một câu chuyện thú vị cần phải đọc.
5
306081
2016-02-23 20:43:34
--------------------------
253186
10739
Từ nhỏ mình đã luôn ao ước được chu du vòng quanh thế giới, ngắm nhìn những quang cảnh đẹp đẽ của thiên nhiên.Khi đọc cuốn sách này, mình đã cảm thấy thực sự thỏa mãn về điều đó.Lật từng trang sách, cùng với cô bạn Hiển và chú chó đáng yêu, mình cảm giác như đang được đi trên cùng một con thuyền chu du thế giới với Magellan, đi cùng những nhà buôn cổ xưa trên con đường tơ lụa, tiến đến Nam cực lạnh toát cùng Amudsen...Để tìm ra những vùng đất mới,họ đã phải trải qua vô vàn những thử thách của thiên nhiên, của sự gan dạ và sự đam mê.Ham muốn được thám hiểm mạnh mẽ hơn bao giờ hết.Địa lý chưa bao giờ rùng rợn đến thế.
4
280592
2015-08-04 11:09:23
--------------------------
249078
10739
Đã từ rất lâu rồi, tôi vẫn hằng mong ước một ngày được đi du lịch, thăm thú khắp nơi và học được nhiều kiến thức từ những nơi mà tôi sẽ đặt chân tới. Và cuốn sách này như đã phần nào hiện thực hóa giấc mơ của tôi. Đọc nó, tôi như bị cuốn theo dòng thời gian cùng những bước chân thám hiểm của những con người gan dạ và đầy lòng quyết tâm, những người đã làm nên trang lịch sử vĩ đại khi khám phá được vùng đất mới như Columbus, Magellan, Marco PoloKingsley, James Cook,... Tôi cũng được trải nghiệm cảm giác khi leo núi, khi băng qua sa mạc,... Tất cả điều đó như đánh thức niềm đam mê được "xê dịch" từ nhỏ của tôi vậy. Cuốn sách này cực kì thú vị đó!
5
580478
2015-07-31 12:38:23
--------------------------
241461
10739
Với "Những Nhà Thám Hiểm Hăm Hở", Địa Lý đã không còn là những mớ lý thuyết khô khan, rối nhằng nữa mà đã trở nên vô cùng thú vị với những hình ảnh minh họa sinh động và hấp dẫn. Những vùng đất mới, kì bí và đầy hiểm nguy đã được khám phá. Từ những ngọn núi cao ngút ngàn, những vùng biển bí ẩn, những hang động sâu hoắc, những sa mạc nắng rát da hay vùng địa cực lạnh đến tê răng,.. Tất cả đều lưu lại dấu chân của con người, những nhà thám hiểm vĩ đại và can đảm như Columbus, Marco Polo, Magellan, Mary Kingsley,.. Thật sự là một quyển sách thú vị và bổ ích
5
649807
2015-07-25 14:58:13
--------------------------
240263
10739
Trước khi đọc quyển sách, mình chỉ hiểu "khám phá" theo nghĩa "đi tìm một vùng đất mới". Có lẽ những nhà thám hiểm ngày xưa chỉ việc cầm theo những món đồ cần thiết, lên đường và lần theo những con đường không dấu chân người. Thế nhưng bắt đầu đọc quyển sách, mình nhận ra mọi chuyện không đơn giản vậy... Rùng rợn mà cũng rất hăm hở! Có lẽ một vùng đất nhưng phải biết bao nhiêu nhà thám hiểm, bao nhiêu năm trời đi đi lại lại, mới xác định được nó nằm ở đâu trên địa cầu, rộng lớn bao nhiêu, có những địa hình như thế nào, thời tiết ra sao... Có những vùng đất tưởng là của châu lục này nhưng lại là một châu lục khác, có những vùng trời ma quái bí ẩn, những vùng núi tuyết thiếu oxy, những cuộc hành trình vĩ đại dù thiếu thốn thiết bị máy móc. Quyển sách rất sinh động, dễ hiểu, thú vị, đặc biệt biến những người lúc đầu hoàn toàn không quan tâm trở thành những "chuyên gia" nghiên cứu địa lý và lịch sử du ký thế giới.

5
120276
2015-07-24 14:24:20
--------------------------
224987
10739
Từ xa xưa, có những nhà thám hiểm dấn thân vào nguy hiểm để tìm những vùng đất mới, để nghiên cứu khoa học. Đọc sách này bạn sẽ gặp họ, dưới nét vẽ biếm họa hài hước và mô tả cũng hài như thế. Nhưng sự thật thì vẫn là sự thật, bạn sẽ có dịp mở mang hiểu biết, về các nhà thám hiểm và việc thám hiểm thực sự. Biết đâu sau này bạn lại có cảm hứng làm một nhà thám hiểm thì sao, những kinh nghiệm trong sách sẽ có ích cho bạn đấy.
5
82774
2015-07-09 16:58:19
--------------------------
222714
10739
Để có được một kho tàng kiến thức địa lí to lớn như ngày hôm nay cho chúng ta thì những nhà phát kiến địa lí cũng góp một phần không nhỏ trong này. Qua lời văn dí dỏm, dễ hiểu, hình ảnh minh họa sinh động, ngộ nghĩnh,… sẽ giúp chúng ta hiểu rõ hơn về những tên tuổi đã làm nên lịch sử như Columbus, Magenllan, James Cook,… hay ai đã là người chứng minh được rằng trái đất là hình cầu chứ không phải hình cái đĩa hay hình vuông như những lời phỏng đoán trước đây. Một cuốn sách thú vị và bổ ích để biết rõ hơn về những nhà thám hiểm dũng cảm.
5
554214
2015-07-06 08:31:51
--------------------------
