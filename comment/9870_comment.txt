454797
9870
The Jungle Book – Cậu bé rừng xanh - Joseph Rudyard Kipling phiên bản Happy reader 350 từ kể về cậu bé Mowgly bị bỏ rơi trong rừng từ nhỏ và được đàn sói nuôi lớn. Trong quá trình trưởng thành của mình cậu bé Mowgly đã trả qua rất nhiều cuộc hành trình thú vị cùng những người bạn và những người anh em tuyệt vời của mình. Câu chuyện tuy kể về cuộc sống nơi rừng xanh nhưng thấm đẫm tình người
Thông điệp mà câu chuyện muốn truyền tải đến cho bạn đọc chỉ đơn giản là: Chúng ta đều là anh em. Chúng ta cùng chung dòng máu nên chúng ta hãy yêu thương nhau ("we are all brothers. We have the same blood" so let's love eachother)
5
301718
2016-06-22 09:47:05
--------------------------
161331
9870
The jungle book là tập truyện viết cho thiếu nhi của nhà văn nổi tiếng nguời Anh, Rudyard Kipling. Ông cũng là nhà văn người Anh đầu tiên đạt được giải Nobel trong lĩnh vực văn học. Tập truyện xoay quanh cuộc sống của người-sói Mongli sống giữa bầy sói trong rừng già và hiểu được tiếng của muông thú. Chỉ qua một vài mẩu chuyện trong sách cũng khiến người đọc thấy được những tình cảm chân thành, ấm áp  từ đó sẽ giúp thêm hiểu, thêm yêu cuộc sống xung quanh mình. Sử dụng Jungle book phiên bản này cũng làm cho việc học tiếng Anh trở nên dễ dàng. Giải nghĩa từ ngắn gọn, cặn kẽ, có các bài tập ở cuối mỗi chương sẽ giúp người sử dụng luyện tập, hệ thống lại các thông tin đã đọc và nghe. Đặc biệt các tranh minh họa trong truyện rất ấn tượng.
4
534227
2015-02-27 14:05:22
--------------------------
