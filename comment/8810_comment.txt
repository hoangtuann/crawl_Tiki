209849
8810
mình không ấn tượng nhiều về nội dung truyền đạt của cuốn sách mà chỉ ấn tượng văn vần của tác giả hết sức hóm hỉnh hài hước mà thôi. sách này thì mình cho không hợp với thời hiện đại như thế này khi những cặp yêu nhau có xu hướng ít về hay yêu lâu năm mới dẫn ra mắt gia đình người yêu mà thôi nên những quy tắc này có vẻ không có ứng dụng thực tiển cao lắm . ngoài ra ấn tượng nhất là dòng chữ lưu ý được in đậm với lời cảnh báo hài hước ,chắc có lẽ tác giả đã đoán định trước những yếu tố hài hước và bất ngờ rồi. nên sách này mình nghĩ ứng dụng thực tế không cao như nhìn từ cổ lên không được nhìn trên xuống và không được nắm tay,.. sách này theo mình thì mang yếu tố làm cười bạn đọc nhiều hơn.
3
642439
2015-06-18 11:50:11
--------------------------
186203
8810
Cuốn sách như một quyển "bí kíp" gối đầu giường của những ông bố có cô con gái tuổi teen. Đó là những cách đối phó đầy hài hước trước những đòi hỏi của cô gái mới lớn, những viễn cảnh tranh chấp trong gia đình mà đôi lúc các ông bố cũng phải đầu hàng con gái mỉnh. Và sau đó là một loạt giải pháp mà tác giả đề xuất để giúp các ông bố thoát khỏi tình cảnh "dở khóc dở cười". Nhưng hãy cẩn thận vì tác giả đã cảnh báo trước rằng kế hoạch đó sẽ bị cản trở bởi 3 người phụ nữ: Vợ, Con Gái và Mẹ Tự Nhiên!
4
596855
2015-04-20 20:18:42
--------------------------
94826
8810
Chỉ có 8 nguyên tắc thôi, chưa đầy hai bàn tay đếm nhưng được viết thành cuốn sách dày gần 400 trang! Mới bao nhiêu thôi đã thấy tò mò, không biết vị từ-phụ nào mà kỹ tính đến thế, "khó khăn" đến thế, chăm sóc tướng-công-tương-lai của con gái kỹ lưỡng đến thế. Đọc qua vài dòng đầu tôi lập tức bị cuốn hút vào lối viết hài hước, đậm chất của một bậc-phụ-huynh đáng kính. Ông rất hiểu con gái mình, rất mong con gái mình được hạnh phúc dù đấy chỉ mới là người yêu của cô... Những căn dặn, đôi chỗ thậm chí là răn đe... rất chi li, tỉ mỉ, đã khiến bất cứ chàng trai nào "lỡ dại" trót yêu con gái ông cũng dè dặt, nhưng càng đọc những lời tâm huyết này của người cha, chàng trai ấy càng thấy rõ tình yêu mà ông dành cho con gái, cô ấy là tài sản quý giá nhất của cuộc đời ông, mà ông nâng niu chăm sóc nên anh chàng phải "liệu hồn" với ông. Chấp nhận cho một người đàn ông khác bước vào cuộc đời cô gái, đối với người cha vừa là một sự kiện vừa đáng mừng vừa đáng sợ...(!) Bởi thế mà việc viết cuốn sách này càng có ý nghĩa với ông, và việc người yêu của con gái đọc quyển sách này của ông là một việc làm tối quan trọng, là bước đầu tiên khi "dám" quen con gái ông... Đọc cuốn sách bạn sẽ thấy một cách rất khác nữa của những ông bố trong việc thể hiện tình yêu với con gái của mình......
3
56679
2013-09-18 21:10:23
--------------------------
