404750
8818
Về bề ngoài thì miễn chê nha, sách mang đậm màu sắc cổ trang TQ, bìa rất đẹp và chất liệu giấy cũng rất ổn.
Về nội dung thì tôi có đôi lời muốn gửi đến tất cả mọi người đã đang và sẽ có ý định đọc. Những lời tâm huyết của một người quá thương nam phụ.
Đây là cuốn sách đầu tiên tôi đọc của Hoại Phi Vãn Vãn và có lẽ đó cũng là cuốn cuối cùng. Vì tôi đã quá khổ sở, quá đau lòng mỗi lần nghĩ đến Thế Huyền, hoàng đế đáng thương của tôi.
Tôi đã từng đọc rất nhiều, rất nhiều những câu truyện vui, cũng đã lọt hố truyện ngược với Thương Ly. Đau đớn và khổ sở đến nghẹt thở. Nhưng chưa bao giờ, chưa bao giờ ám ảnh về một nhân vật nhiều đến mức này. Từ dạo đọc xong Đế Hoàng Phi, tôi chưa từng dám bước chân vào một câu truyện buồn nào nữa. Thế Huyền cứ ở trong đầu tôi mãi, nghĩ đến lại thấy nặng lòng và xót xa quá. Hoàng đế tội nghiệp của tôi. 
Đối với tôi, nam chính trong truyện là xuất hiện đúng người, đúng thời điểm. Nhưng tình yêu trái ngang của Thế Huyền mới là thứ giữ tôi ở lại, đó mới chính là thứ đục khoét sâu vào lòng tôi. Nam chính không để lại ấn tượng gì, đối với riêng tôi. Nam chính của Đế Hoàng Phi là Thế Huyền, mãi mãi, vĩnh viễn là anh!
5
88471
2016-03-25 17:52:53
--------------------------
390045
8818
Mình vốn là người không thích đọc cung đấu vì cảm thấy lằng nhằng và thâm độc nên nhìn bộ truyện dày cộp này đã ngán. Nhưng mà đọc thử thì bị cuốn hút dần dần. Lao vào vòng tranh đấu với hai cô cháu Thế Huyền và Lệnh Viên, khỏi phải nói, mặc dù biết là không bao giờ có thể, mình vẫn mong một kết cục gì đó có thể gọi là tốt đẹp. Cảnh Thế Huyền đưa tiễn Lệnh Viên lên kiệu hoa sang Nam Việt là ấn tượng nhất. Như mọi người đều nói : giống một nam tử chuẩn bị cưới một nàng dâu về nhà chứ không phải người cháu đưa cô của mình đi lấy chồng. Rồi quyến luyến không buông tay, mãi đến khi Lệnh Viên phải gắt lên tuyệt tình. Tội nghiệp Thế Huyền, đúng là nghiệt duyên. Lệnh Viên thì một lòng vì Bắc Hán, trong lòng nàng ôm giữ mãi mối tình thưở niên thiếu rồi vẫn phải buông, chấp nhận lấy một người chưa từng gặp mặt ở nơi xa xôi, những tưởng đã có thể sống yên thì lại vấp phải tranh đấu tại nơi đất khách quê người. Chờ tập 2.
4
414111
2016-03-03 09:57:50
--------------------------
372738
8818
Theo như cảm nhận của tôi khi theo dõi xong bộ truyện này là quá dài dòng, lê thê. Có thể Hoại Phi Vãn Vãn hơi tham khi xây dựng quá nhiều nhân vật, quá nhiều tình tiết khiến cho câu chuyện rắc rối , làm cho người đọc khó theo dõi , hay bị lẫn lộn tên giữa các nhân vật. Thú thật tôi đã cố hết sức đọc xong tập 1 và nhảy qua tập 2 lật khúc cuối đọc luôn cho xong, chứ cái kiểu lòng vòng như vậy đọc rất ức chế, tưởng đâu khúc cuối kết thúc mau lẹ ,ai ngờ nó vẫn dài dòng, dẫn đi một hồi rồi mới kết thúc.
2
313953
2016-01-22 09:42:33
--------------------------
371436
8818
Lúc đầu đọc truyện tưởng trừng như Nguyên Trinh là nhân vật chính nhưng ai ngờ đi đến gần hết truyện thì biết được đó là Doãn Dạt có thể nói tình yêu của nữ chính vượt qua quá nhiều tai ương. Tuy nữ chính chẳng thông minh cũng không ngu ngốc nhưng mà "Hồng nhân thì bạc phận" nhưng chẳng sao cả vì đến cuối cùng uối cùng, nàng cũng gặp được tri kỉ hết lòng với mình, sống cuộc sống miễn cưỡng xem là viên mãn và có thể bảo vệ mình đến hết cuối đời. Đế Hoàng Phi thực sự là một trường thiên truyện rất hay về sự ra đời và kết thúc của một hoàng triều Bắc Hán, về cung đấu, về mưu về thủ đoạn và rất đáng đọc.
5
1062396
2016-01-19 16:17:36
--------------------------
368733
8818
Đế hoàng phi tập 1 có phần thích thú riêng đối với độc giả khác nhau vì có được sự tính toán ngoại cảnh của lối diễn xuất rất xuất thần , sắc sảo điều hành ra tất cả những con người với mối liên kết thân thiết , không có điểm thừa làm mất thời gian của cả quá trình , mấy người trong câu truyện đều có phong cách nổi bật , không chỉ làm bất bình mà trải dài lời thoại khá riêng rẽ , phục vụ cho đa số các đối lập trong cá nhân bồi đắp .
5
402468
2016-01-14 15:22:19
--------------------------
331788
8818
Vì đọc Mệnh phượng hoàng nên mình tìm mua bộ này cho đủ hệ liệt. Điều đầu tiên là có cảm giác văn phong bị tụt đi do với Mệnh phượng hoàng. Hai là truyện này khiến mình xoắn não và đoán đâu mới là nam chính :D

Nói thật là mình thích Thiếu Huyền nhất, cơ mà mấy đoạn miêu tả nữ chính với Thiếu Huyền là mình đã đoán ra hai người này không phải ruột thịt rồi. Anh nam chính thật sự thì lại có vẻ mờ nhạt quá, anh ấy xuất hiện không nhiều, thân phận không cao, mà cũng không làm được gì quá là ấn tượng cả.

Mình không thích nữ chính cho lắm, có lẽ vì nhân vật này đã phụ nhân vật mà mình thích, nhưng có lẽ cô đã phải chịu quá nhiều đau khổ, cho nên không thể đem lại ngọt ngào cho người khác được.
3
403246
2015-11-05 08:47:47
--------------------------
330638
8818
Đọc xong truyện này mà tớ muốn bật khóc ngay cho đỡ xót ấy, bạn nào nghĩ đây là truyện HE cũng kệ, tớ vẫn cho rằng truyện này kết chẳng có hậu chút nào, cho dù tới cuối cùng thì Lệnh Viên và Doãn Duật được ở bên nhau hạnh phúc trọn đời. Tớ luôn cảm thấy tình cảm Lệnh Viên dành cho Doãn Duật thật ra không sâu đậm bằng tình cảm nàng dành cho Thế  Huyền, nếu nàng phát hiện ra Thế Huyền và Bùi Vô Song chính là một người sớm hơn một chút, biết đứ cháu vẫn gọi nàng là cô cô chính là người nàng yêu thì mọi thứ sẽ thế nào?  Cái chết của Thế Huyền khiến tớ đau không sao thở nổi.
5
630600
2015-11-02 23:07:19
--------------------------
282946
8818
Kết thúc tập một là rất nhiều cảm xúc thương cảm của tôi dành cho Lệnh Viên- một người công chúa cao quý nhưng phải chịu nhiều đau khổ bất hạnh. Mở đầu truyện là cái chết của phò mã vừa đọc đến đây tôi nghĩ truyện chắc sẽ có rất nhiều đau khổ dành cho cô gái này.Kết thúc tập một là  cuộc hôn nhân của Lệnh Viên  để giúp cho đất nước có mối giao hảo tốt đẹp. Nhưng Lệnh Viên không phải là người duy nhất tôi thấy thương cảm mà tôi còn rất thích Thế Huyền- một vị hoàng đế trẻ tuổi, bệnh tật, mưu mô và dành cho Lệnh Viên tất cả tình cảm của mình làm tôi rất xúc động khi đọc đến chỗ nhân vật này.  Tôi rất mong chờ được đọc tập 2 của quyển sách này!
5
413619
2015-08-29 16:09:27
--------------------------
241148
8818
So với Mệnh Phượng Hoàng, ngay từ bìa sách của Đế Hoàng Phi đã đượm vẻ u buồn, lạnh lẽo. Câu chuyện về chốn hậu cung cũng vậy.. Đây là một trong những truyện cung đấu hay nhất, với diễn biến hợp lý, tình tiết logic, liên kết chặt chẽ, giọng văn mạch lạc và nội tâm nhân vật được lột tả rất sâu sắc, cũng là truyện có cái kết khiến cho người đọc hết sức day dứt. Thương thay cho Lệnh Viên, cho Thế Huyền, sống không được là chính mình, ngay cả đến quyền cơ bản nhất của một con người là yêu thương người khác cũng không làm được.
5
271035
2015-07-25 11:07:39
--------------------------
226076
8818
Ấn tương đầu tiên của tôi về cuốn sách này là bìa sách.Bìa sách ảm đạm và u buồn như cuộc đời của Lệnh Viên.Nàng là đại trưởng công chúa vừa thông minh lại xinh đẹp nhưng cuộc đời lại bất công với nàng.Chỉ vì một câu nói mà nàng bị xem là "hồng nhan họa thủy", bị chính những người thân của mình ghẻ lạnh,xa cách,bị xem là mối họa của đất nước.Rồi khi nàng làm giám quốc đại trưởng công chúa lại bị cháu trai - người mà nàng coi là người thân duy nhất hiểu nhầm.Đoc xong "Đế hoàng phi",tôi không khỏi nuối tiếc cho Lệnh Viên cho cuộc đời và số phận của nàng.Tóm lại đay vẫn là một cuốn sách hay và cũng là một màu sắc nên có trong tủ sách.
5
609388
2015-07-11 13:08:51
--------------------------
206659
8818
Khác với những người hâm mộ Hoại Phi Vãn Vãn, mình đến với tác giả này lần đầu là qua tác phẩm Đế hoàng phi chứ không phải Mệnh phượng hoàng. Phải nói rằng là thể loại cung đấu vẫn luôn rất rất hấp dẫn mình và nếu so với Đông Cung khiến cho người đọc bật khóc, ám ảnh thì Đế hoàng phi lại mang đến sự thực tế, luyến tiếc và thậm chí là cả day dứt. Với tạo hình của nữ chính Lệnh Viên vừa thông minh, cứng rắn, mạnh mẽ vừa dịu dàng có đôi lúc yếu đuối của một người con gái quả thật gây cho mình ấn tượng sâu sắc. Nữ chính trong truyện được tác giả làm mới mẻ, một người con gái nắm giữ trong tay quyền lực tối cao, tự bản thân cai quản cả lục cung....Thật bản lĩnh, đây là kiểu nữ chính hiếm thấy so với nữ chính của các  ngôn tình hiện nay. Thật mong chờ khi minh đọc xong quyển hai của Hoại Phi Vãn Vãn!!!!


5
190618
2015-06-10 11:23:03
--------------------------
206432
8818
Mình biết đến “Mệnh phượng hoàng” trước “Đế Hoàng Phi”, nhưng lại ám ảnh với Đế Hoàng Phi hơn, có lẽ cũng bởi nó khốc liệt hơn, tàn nhẫn hơn, xót xa tê tái hơn. Ít nhất Tang Tử của Mệnh Phượng Hoàng vẫn còn có Hạ Hầu Tử Khâm sát cánh bên cạnh, còn có Tô Mộ Hàn âm thầm chở che, còn có được một kết thúc có hậu, còn Lệnh Viên thì sao? Cô đau khổ hơn nhiều. Mình thấy nhiều bạn thích Thế Huyền, nhưng mình thì không, đơn giản là mình cảm thấy tình cảm Thế Huyền dành cho cô chưa phải chân thành, dù mình vẫn biết nói chuyện thân thành với đế vương thì thật quá xa xỉ. Truyện này cung đấu cực hay, bạn nào mê cung đấu thì đừng nên bỏ qua.
5
449880
2015-06-09 17:53:41
--------------------------
205796
8818
Sau Mệnh phượng hoàng thì Đế hoàng phi chính là một bộ truyện đã chiếm một vị trí lớn trong lòng tôi. Hai bộ truyện của Hoại Phi là hai bộ ngôn tình đầu tiên tôi đọc và từ đó về sau không có thêm bộ truyện nào vượt qua được ngưỡng cửa cảm xúc mà chúng đã đem lại cho tôi.
Cảm nhận chung của tôi chính là quyển 1 hay hơn quyển 2. Không nói nhiều về văn phong của Hoại Phi vì đối với tôi ngôn ngữ kể và tâm truyện của Hoại Phi khá xuất sắc, cung đấu hay, trạch đấu đều tốt. Không biết có phải quyển 1 nhân vật tôi thích có nhiều đất diễn hay là quyển 1 thật sự hay hơn nhưng tôi ưu tiên quyển 1 và tôi cũng đã đọc lại quyển 1 không ít lần.
Có lẽ rất nhiều bạn đọc có cảm nhận chung như tôi, Thế Huyền là nam chính, không thì Bùi Vô Song là nam chính nhưng cuối cùng hai người đó lại là một và là nam phụ. Không biết đây là thành công hay không thành công của Hoại Phi khi xây dựng nhân vật nam phụ quá nổi bật, đánh bật cả nam chính ít nhất là với tôi.
Thế Huyền đã để lại trong lòng tôi hai chữ “đau thương”. Y mới chỉ mười chín tuổi thôi nhưng cuộc đời ấy đã phải sống quá bất hạnh. Khi còn sống chỉ lo tranh giành quyền lực với chính người cô cô mà mình yêu thương, khi giành lại quyền lực thì cô cô đã rời bỏ y sang cầu thân Nam Việt, khi chết đi thì giang sơn cũng mất mà cô cô cũng không hề hay biết.
Nếu Thế Huyền không thể khiến cô cô vui thì Bùi Vô Song sẽ thay y khuyên ngăn nàng, giúp nàng tìm đến hạnh phúc. Sai lầm lớn nhất của cuộc đời y chính là đã yêu cô cô ruột của mình. Tôi còn ám ảnh mãi lời nói đau thương của Thế Huyền “Trẫm không nỡ.” y không nỡ buông tay nàng mà nàng khi đã buông thả quyền lực cho y, cũng buông luôn y, để y còn lại cô độc.
Y là hoàng đế, y có tất cả nhưng lại là chẳng có gì khi đến cả người mình yêu thương cũng không thể đem lại hạnh phúc cho nàng, không thể giữ nàng lại.
Cái chết của Thế Huyền đã khiến tim tôi như vỡ ra vậy. Y chết trong lặng lẽ, chết trong cô độc, chết không ai hay biết trong cảnh nước mất nhà tan. Cuối cùng cũng thật may vì vẫn còn Dương phi yêu y thật lòng, cam tâm ôm y và chết theo y.
Kết truyện là “hai bóng hình trong kí ức đó hợp lại làm một” trong Lệnh Viên, hình bóng của Thế Huyền và Bùi Vô Song. Cuối cùng cô cô mới biết, cuối cùng cô cô có hiểu y hay không?
Thế Huyền khiến tôi nhớ đến Tô Mộ Hàn trong Mệnh Phượng hoàng. Số tôi thật bất hạnh a, yêu cả hai nam phụ trong hai bộ truyện của Hoại Phi -_- yêu rất nhiều, yêu đến đau khổ T.T thương tâm quá thương tâm!!!!
5
654102
2015-06-07 18:41:12
--------------------------
204733
8818
Đây là một trong những truyện cung đấu hay nhất, với diễn biến hợp lý, tình tiết logic, liên kết chặt chẽ, giọng văn mạch lạc và nội tâm nhân vật được lột tả rất sâu sắc, cũng là truyện có cái kết khiến mình day dứt rất nhiều. Mình thấy thương cho Lệnh Viên, nói đời cô sóng gió ba đào, hồng nhan bạc phận cũng chẳng ngoa. Mình cũng đau cho Thế Huyền, mình đã từng ôm hi vọng rất nhiều rằng Lệnh Viên rồi sẽ đến với Thế Huyền, nhưng trong mắt nàng, chàng mãi chỉ là đứa cháu gọi nàng là cô cô mà thôi. Cái chết của Thế Huyền khiến mình đau lòng vô cùng. Chẳng thà cả đời không biết Thế Huyền và Bùi Vô Song là một thì có lẽ sẽ không đau đến thế.
5
393748
2015-06-04 20:26:30
--------------------------
176929
8818
Nếu như Tang Tử trong Mệnh Phượng Hoàng hạnh phúc vì có được một Hạ Hầu Tử Khâm luôn yêu thương cô, vì cô mà chắn mọi sóng gió, thì Lệnh Viên lại không được như vậy. Từ đầu đến cuối, mọi nỗ lực và hi sinh của cô đều bị phủ nhận, Bắc Hán vẫn mất, Thế Huyền và Chiêu nhi cũng chết. Nước mất nhà tan, mãi về sau nàng mới được ở bên người nàng yêu, Doãn Duật, mối tình thời son trẻ, mối tình nàng từng từ bỏ vì giang sơn. Nhưng cuộc sống của họ cũng không tính là hạnh phúc trọn vẹn. Chưa kể đọc sang Mệnh phượng hoàng mới thấy cuộc sống của Lệnh Viên về sau cũng đầy đau khổ, khi Doãn Duật ra đi trước nàng, để lại nàng một mình chống đỡ hậu cung cho con trai Tử Khâm. Nhưng Tử Khâm trước sau vẫn có ít nhiều ngăn cách với nàng, không thấu hiểu được tình mẫu tử của nàng. Thực sự, tôi không hiểu tại sao phải tàn nhẫn với Lệnh Viên đến vậy. Cuối cùng, cuộc đời nàng còn lại gì?
5
57459
2015-04-02 09:20:27
--------------------------
166158
8818
Mình mua cuốn Đế hoàng phi này vì tình yêu với Mệnh phượng hoàng. Mình tò mò muốn biết những tác phẩm khác của Hoại Phi Vãn Vãn sẽ như thế nào.
Những tưởng mô típ và văn phong của Mệnh phượng hoàng sẽ ảnh hưởng tới Đế hoàng phi, nhưng không phải như thế. Nếu Mệnh phượng hoàng là hình ảnh về một nữ tử mạnh mẽ, kiên cường, có hậu thuẫn, mang trong mình mệnh phượng ít nhiều đã được tiên đoán, và Mệnh phượng hoàng mang lại cho độc giả sự kiên cường của nhân vật chính thì Đế hoàng phi lại nhuốm đầy vẻ xót xa. Xót xa cho nữ tử đẹp đến khuynh thành, sinh ra với thân phận cao quý vô cùng nhưng chẳng được bọc trong nhung lụa. Cuộc đời nàng bấp bênh chìm nổi từ nơi này qua nơi khác, gắn bó mình với người này, rồi người khác mà chẳng thể tìm được cho mình một nơi bình yên.
Nhân vật mình ghét nhất trong tập 1 chính là Thế Huyền - Bùi Vô Song. Mình không hiểu được tình cảm phức tạp và khó hiểu mà y dành cho Lệnh Viên. Thà rằng là Dương Ngự thừa còn khiến người ra có cảm tình hơn.
Phần cung đấu trong truyện khá ít, chỉ điểm qua vài nét, chứ không nhiều và cụ thể như mệnh phượng hoàng. Mình nghĩ đây không mang nhiều nét của truyện cung đấu cho lắm.
5
333831
2015-03-11 23:28:03
--------------------------
159894
8818
Biết đến hoại phi vãn vãn từ mệnh phượng hoàng,nhưng sang đến đế hoàng phi tôi lại hơi ghét tg.
Cô viết về cuộc đời.Lệnh Viên với quá nhiều bất công.Cuộc chiến chốn cung đình tàn nhẫn biết mấy.
Chỉ vì 1 lời tiên đoán vu vơ mà nàng bị gả đi xa khi chỉ mới 13 tuổi,hi sinh mối tình đầu đầy trong sáng của mình.nhiều năm sau nàng trở về gánh trên vai trách nhiệm của một giám quốc đại trưởng công chúa lại hi sinh tiếp tự do.đất nước nội loạn, các hoàng tử Nam Việt đến cầu hôn nàng hi sinh tiếp hạnh phúc của mình.Khi nhận ra người cầu hôn mình thay cho Dận Vương là người mình yêu nhiều năm trước.Nàng cũng nhẫn tâm từ bỏ.Đến lúc nước nhà rơi vào tay gian thần nàng hi sinh luôn thân thể của mình cho Khánh Vương để hắn giúp mình giành lại đất nước.
"Hồng nhan bạch cốt,cẩm tú giang sơn"rốt cuộc nàng còn lại gì?
Lệnh Viên à?nếu như nàng biết được cho dù mình có hi sinh tất cả như thế,cũng chẳng thể thay đổi được vận mệnh diệt vong của Bắc Hán.Thì nàng có tiếp tục hi sinh không?hay nàng sẽ để lại một chút gì đó cho mình,và cho người nàng yêu thương?
sự tồn vong cùa một đất nược há lại có thể thay đổi chỉ vì sự hi sinh của một nàng công chúa?
Lúc biết Chiêu nhi là con của nàng và phò mã tôi rất sốc,còn ác được hơn nữa không trời?sau lại biết nàng vì sinh non Chiêu nhi mà mất trí không biết mình có con,lại bị tổn thương không thể làm mẹ đc nữa tôi rất đau lòng.
Khi đất nước rơi vào tay họ Hạ Hầu,Doãn Duật cũng chết,để lại 1 mình lệnh viên tiếp tục ở trong cung giúp con trai trông coi hậu cung.không thể nói nó là HE được.
Đọc mà xót xa quá.
Về các nam chính qua t2 viết tiếp.Dài quá r.
4
472523
2015-02-20 12:23:33
--------------------------
152869
8818
Đọc tác phẩm này tôi thấy cảm thông với Lệnh Viên, sinh ra và lớn lên trong hoàng tộc , số phận của mình không được chính mình định đoạt.
Tập này mình rất cảm thông với số phận của phò mã. Bởi là nhân vật phụ nên không có kết cục tốt, Nhưng 2 người cũng là được vợ chồng mấy năm cũng an ủi 1 phần cho anh.
cảm thấy rất tội nghiệp Thiếu đế , phải giả làm 2 nhân vật mới có thể tiếp cận Lệnh Viên đường đường chính chính, chỉ khi là Vô Song thì mới có thể bên LV mà không phải e dè vai  vế...
Cốt truyện mới  đối với mỉnh :3 
Nội tâm nhân vật dược miêu tả cụ thể, dễ hiểu.
Mình đang mong chờ đọc tập 2 :3
Bìa sách đơn giản nhưng đẹp. :3
nói chung không uổng phí money và time vào trong 1 cuốn sách hay :)
5
486917
2015-01-24 14:52:52
--------------------------
135833
8818
Về nội dung của truyện thì đúng là không còn gì để bàn cãi, nó có rất nhiều  tình tiết đan xen với nhau làm người đọc đi từ bất ngờ này tới bất ngờ khác. Các nhân vật chính đều có cá tính riêng, ai cũng có những tâm cơ của riêng mình, tôi thích nữ chính Lệnh Viên - thông minh, bản lĩnh nhưng cũng có những lúc ghen tuông hờn dỗi và yếu đuối, các nam chính tôi cũng đều thích: thiếu đế Thế Huyền quá ấn tượng, Doãn Duật quân tử, điển hình cho mẫu nam thần ngôn tình, và cả Khánh Vương mưu kế thâm sâu, ẩn mình đợi thời cơ nữa...
Nhân vật phụ cũng có nét riêng, họ đều là những nút thắt nhỏ nhưng không thể thiếu trong suốt câu chuyện. Phải lâu lắm rồi tôi mới đoc được 1 tác phẩm mà bản thân đánh giá cao như vậy!!
Về hình thức, bìa đẹp, đơn giản nhưng toát lên được phong cách cổ trang và có cảm giác đượm buồn. Tuy nhiên giấy in không tốt, rất dễ bị ố và bị vàng mặc dù đã được tôi gìn giữ cẩn thận.
5
31339
2014-11-17 17:38:13
--------------------------
110972
8818
Bìa không quá bắt mắc, khi mới cầm quyển tiêu thuyết này trên tay tôi cứ nghĩ nam chính trong truyện là Trinh Nhi (Hoàng thượng) nhưng khi đọc đến gần cuối quyển 1 tôi mới biết thì ra không phải, người được sắp duyên cùng Lệnh Viên là Doãn Dật.
Có thể nói một điểm chung qua hai truyện cung đấu đó là bên cạnh nữ chính luôn có một vị sư phụ trong chùa, cả hai đều yêu nữ chính, đều tìm mọi cách bảo vệ nữ chính. 
Tác giả cho Lệnh Viên biết Thế Huyền là Bùi Vô Song quá trể, để lại sau cái kết thúc truyện đó là nỗi đau dai dứt của Lệnh Viên.
Truyện kết thúc HE làm mình rất vừa ý, mong má Vãn Vãn mau chống ra truyện mới.
5
144489
2014-04-23 09:43:12
--------------------------
108007
8818
Nữ chính là đại trưởng công chúa, cô cô của vua (Nguyên Trinh hoàng hậu trong bộ Từ Thứ nữ)

Nàng bị người ta nói là tai họa, bị phụ hoàng và hoàng huynh muốn giết

Mẫu hậu nàng vì muốn nàng được bình yên, ban đầu là muốn nàng xa gả, sau lại làm cho nàng trở thành công chúa hộ quốc

Mẫu hậu nàng qua đời, nàng mới biết tất cả

Hoàng đế, cháu của nàng, vừa yêu vừa hận mình, lại còn nghĩ rằng nàng có ý đồ soán ngôi

Sau đó nàng vì quốc gia mà đi hòa thân, dây dưa vật lộn một đoạn thời gian nàng rốt cuộc có mang, cha của dứa bé lại là kẻ làm nàng mất nước, giết cháu nàng

Cuối cùng, nàng cũng gặp được tri kỉ hết lòng với mình, sống cuộc sống miễn cưỡng xem là viên mãn.

Nhân vật nữ chính này thì, ừm, nàng không quá ngu ngốc, cũng không quá độc ác. Thân phận cao quý, cuộc đời lại như bèo dạt mây trôi, tang thương nối tiếp, những người đàn ông bước vào đời nàng đều mang đến cho nàng những đau khổ. Đây có lẽ cũng là số phận chung của mấy vị công chúa ngày xưa, nhưng nữ chính này cũng có thể xem là mạnh mẽ, cuối cùng có thể tìm được một bến đỗ sau nửa đời lênh đênh.

Tay viết của Vãn Vãn rất chắc, đặc biệt là thể loại cung đấu này, rất ủng hộ mấy bộ của chị, mong là sẽ được mua bản quyền xuất bản hết ở Việt Nam ^^~
4
25766
2014-03-13 21:44:18
--------------------------
102996
8818
Sau khi đọc Mệnh Phượng Hoàng tập 1, trong thời gian chờ đợi cuốn 2, tôi đã đặt mua Đế Hoàng Phi vì rất thích những cảnh cung đấu trong truyện của Hoại Phi Vãn Vãn. Và Đế Hoàng Phi ko hề làm tôi thất vọng. Mạch truyện được kết nối liên tục từ đầu đến cuối, với những bí mật cung đình nửa kín nửa hở đan xen nhau, nhiều lúc gỡ đc nút thắt này, ngỡ đã hết ai ngờ lại là "thắt trong thắt", có những bí mật đến cuối truyện mới được bật mí. Nói về phong cách hành văn, hầu như truyện của HPVV ko hề có những điểm thừa, ko câu view bằng những cảnh nóng mà thực sự xoáy thẳng vào tâm trí người đọc bởi mưu kế, suy nghĩ, cá tính nhân vật. Nàng công chúa Lệnh Viên được xây dựng với một tình cách nhất quán từ đầu đến cuối, và rất hợp lý: mặc dù thông minh tuyệt đỉnh nhưng vẫn có đôi lúc mềm yếu rất nữ nhân, nhưng cũng chính sự mềm yếu "mốt chút đó" lại thay đổi cả vận mệnh Bắc Hán, khiến nàng ân hận suốt cuộc đời. Đau thương, tuyệt vọng, tranh đấu để sống, rồi lại đau thương,... Đế Hoàng Phi thực sự là một trường thiên truyện rất hay về sự ra đời và kết thúc của một hoàng triều Bắc Hán, về cung đấu, về mưu lược, về những cách suy nghĩ đôi lúc ngỡ cực đoan nhưng rất cần thiết trog chính trị. Tôi chờ đợi đón đọc những tác phẩm tiếp theo của Hoại Phi Vãn Vãn.
5
170765
2013-12-26 10:25:13
--------------------------
