466713
3419
Đây là tác phẩm thứ hai của Musso mà mình đọc, sau Central Park. Thật sự thì mình đã không thể ngừng đọc và hoàn tất chỉ sau vài giờ...Musso vẫn luôn nổi tiếng với những cốt truyện độc đáo pha lẫn nhiều thể loại mà không hề bị khô cứng. Lần này là sự kết hợp của lãng mạn, trinh thám (có nhưng không nổi bật) và cả chút mĩ thuật về các bức hoạ nổi tiếng thế giới. Đọc mà cảm giác như ta đang sống trong màn đêm của một bảo tàng nổi tiếng tại Paris, có chút tối tăm và huyền ảo lạ kì. Cả phần kết là một sự sáng tạo đặc sắc, Musso đã rất tinh tế trong những câu văn nói về thời điểm giữa sự sống và cái chết, khi các linh hồn sắp rời xa thế giới thực được gặp nhau và trao nhau những tình người nồng hậu... Mình rất có lời khen cho dịch giả vì đã truyền tải được chất thơ trong giọng văn Muso và có lẽ cũng làm các độc giả phải rơi lệ khi gấp lại một cuốn sách hay! Cảm ơn Tiki vì chất lượng sách và dịch vụ giao hàng vô cùng chuyên nghiệp!
5
1324602
2016-07-02 19:55:33
--------------------------
446627
3419
Đây là cuốn sách đầu tiên của Guillaume Musso mà mình đọc, từ lời giới thiệu của một đứa bạn, nôm na nó nói là sự đấu trí giữa cảnh sát và siêu trộm, hay lắm, nên mình quyết định mua ngay vì cũng thích truyện trinh thám. Không chỉ là cuộc đấu trí mà đó còn là sự cạnh tranh giữa hai người đàn ông, cùng có tình yêu mãnh liệt, dành cho 1 người phụ nữ, nhưng không biết thể hiện tình cảm đó như thế nào. Cảnh sát với chiến tích lẫy lừng, người mà cô con gái của gã siêu trộm tranh đem lòng yêu mến, và anh cũng yêu cô. Sự gặp gỡ giữa hai con người này là định mệnh, cũng là do con người sắp đặt. 
Những tình tiết xen kẽ giữa quá khứ và hiện tại, thực tế và "hôn mê" dẫn người đọc đi từ bất ngờ này đến bất ngờ khác, lãng mạn, hồi hộp.
5
301644
2016-06-12 16:36:04
--------------------------
440256
3419
Mình biết Marc Levy trước khi biết đến Musso. Nếu như Marc Levy là ông hoàng của tiểu thuyết tình cảm lãng mạng nước Pháp thì Musso cũng là tiểu thuyết gia lãng mạng không kém, tác phẩm của ông mang chất hư cấu nhiều hơn Marc Levy, những chính những tình tiết hư cấu đó làm nên nét đặc sắc của truyện ông. Musso càng lúc càng khiến mình mê mệt các tác phẩm của ông. Trong Nếu đời anh vắng em, càng đọc về sau mình càng không thể ngừng được, hấp dẫn lôi cuốn trong cách tác giả dẫn dắt người đọc, Gabrielle, cô gái 33 tuổi, tại sao lại không đến gặp Martin trong cuộc hẹn 15 năm trước, để rồi 15 năm sau khi gặp lại nhau, tình yêu vẫn nguyên vẹn trong họ, như họ mới vừa xa nhau ngày hôm qua. Rồi chợt McLean, người cha mà cô không hề biết có tồn tại, bất thình lình xuất hiện trong đời cô, để rồi họ chưa kịp thể hiện tình yêu cha con thì lại phải sắp xa nhau khi McLean thông báo với cô rằng ông mắc bệnh ung thư giai đoạn cuối (viết tới đây chắc nhiều bạn suy nghĩ sao giống phim Hàn Quốc thế), để rồi 2 người cô yêu thương: Martin và Matthew lại đứng ở 2 đầu chiến tuyến khi dấn thân vào cuộc rược đuổi giữa một bên là cảnh sát, một bên là tên cướp lão làng, cứ những tưởng rằng tác giả lại viết ra một câu chuyện buồn, nhưng không, chuyện kết thúc hết sức tinh tế nhưng có phần hư cấu, khi đột nhiên lại có thêm sự xuất hiện của mẹ Gabrielle. Một tác phẩm rất hay và đầy tính nhân văn khi bà chấp nhận cuộc sống thực vật, mà như bà nói trong chuyện là bà như một "công chúa ngủ trong rừng", để rồi nhiều năm sau bà tặng tấm vé "Sống" cho Martin, để thanh thản ra đi cùng với McLean. Diễn biến đầy kịch tính nhưng không kém phần quyến rũ. Rất cảm ơn tác giả vì đã viết ra một tác phẩm làm lay động lòng người.
5
627472
2016-06-01 14:12:36
--------------------------
392642
3419
Đây không phải là tác phẩm hay nhất của ông. Nhưng đây là một tác phẩm diệu kỳ cho ta thấy được sức quyến rũ của tình yêu thời niên thiếu của Martin- Gabriell và cả tình yêu của những ngừoi lớn tuổi như Archie-Valentine. Tưởng rằng một cuộc truy sát nghẹt thở nhưng hoá ra lại là 1 kế hoạch xuất sắc dẫn dắt đến tinh yêu. Tưởng rằng mọi thứ đều đã kết thúc vào đêm Noel nhưng hoá ra lại là sự mở đầu cho một thử thách. Trong câu chuyện tình yêu này Musso cho các đọc giả của ông gặp lại bác sĩ Elliott Cooper trong "Hẹn em ngày đó". Trong" nếu đời anh vắng em" tôi cực kỳ yêu mến nhân vật Archie bởi ông thực chất là một con người rất tốt và mang đầy đủ tình yêu thương, lòng vị tha, sự thấu hiểu cũng như sự chở che bảo vệ của một vị cha già đáng kính dành cho đứa con gái, cho cả ngừoi vợ mà ông hết lòng yêu thương và đặc biệt là dành cho "chàng rễ Martin" của mình, là khi cả 2 cùng rơi từ độ cao 70m của cầu Cổng Vàng, ông đã ôm chặt lấy Martin để che chở cho anh khởi những chấn thuơng nặng nề nhất vì thế mà ông đã nhận hết hậu quả về thể xác cho bản thân. Còn khi cận kề cái chết, trong cơn hôn mê đáng lí ra ông sống còn Mảrtin sẽ chết nhưng ông đã nhườg cái sự sống ấy cho Mảtin. Kết thúc của một tác phẩm thật quá bất ngờ . Còn về hình thức thì ổn nhưng cái bìa xấu quá mong Nhã Nam sẽ cho xuất bản với cái bìa cảm xúc hơn.
4
704900
2016-03-07 13:03:36
--------------------------
385998
3419
Một cuốn sách về tình yêu vô cùng tinh tế và sâu sắc.
Có mỗi cái tôi không thích nữ chính cho lắm, nhưng tổng thể cuốn truyện, tôi thấy rất hay. Archibald, ông ta thật sự là một thiên tài, dù ở cái tuổi ấy. Tác giả đã cho thấy điều ấy trong những cuộc rượt đuổi không ngừng nghỉ của ông ta và Martin - cậu con rể tương lai. Bố vợ ăn trộm và con rể cảnh sát. Một cốt truyện thật sự rất độc đáo. Bên cạnh đó, truyện cũng có một cái kết rất đẹp, có lẽ là cái kết hoàn mỹ, viên mãn nhất trong tất cả những tiểu thuyết tôi từng đọc của Musso. Đến với "Nếu đời anh vắng em", bạn thật sự sẽ được chìm đắm trong quang cảnh thơ mộng nơi đất Pháp dưới ngòi bút tuyệt vời của tác giả.
5
640394
2016-02-24 21:17:09
--------------------------
366182
3419
Cuốn sách này là cuốn sách gần như duy nhất Guillaume tạo nên một tình yêu kéo dài qua thời gian( cùng với Hẹn em ngày đó) và tôi thích hai tác phẩm này với tình yêu như vậy.
Guillaume là tác giả trinh thám lãng mạn, nhưng theo tôi đọc rất nhiều tác phẩm của ông thì trong một cuốn sách ông chỉ phát huy được một trong hai thôi, và cuốn Nếu đời anh vắng em mang tính lãng mạn hơn trinh thám.
Câu chuyện với hai tình yêu song song đẹp đến rơi nước mắt, câu chuyện của hai người yêu nhau suốt 13 năm và câu chuyện của hai người yêu nhau dù ở hai thế giới khác nhau vẫn chờ đợi nhau mãi mãi.
Câu chuyện lãng mạn xuất sắc!
5
622800
2016-01-09 13:38:10
--------------------------
300421
3419
Đây là cuốn đầu tiên mình đọc của tác giả Guillaume Musso. Thực sự là rất hấp dẫn và lôi cuốn. Với mình truyện là một chủ đề mới hoàn toàn so với những tiểu thuyết tình cảm khác mà mình đã đọc.Nam Nữ chính trong câu chuyện có một tình yêu vừa đắm say vừa sâu sắc. Dường như họ sinh ra là để cho nhau, cho dù thời gian cũng không thể chia cắt được. Họ yêu nhau, bằng thứ tình yêu đã ngấm vào máu, bất chấp mọi thứ...Nhưng ở giữa họ lại có bố của Gabrielle.. .Liệu bên hiếu, bên tình làm sao cho trọn vẹn?. Hãy đọc và cảm nhận các bạn nhé. Một cái kết bất ngờ đấy!
5
724772
2015-09-13 22:23:22
--------------------------
286731
3419
Hay, hay, nội dung tuyệt vời, giấy đẹp đọc không mỏi mắt, lời văn chau chuốt, mình bị lôi cuốn từ trang đầu tiên, từ bức thư tình ngọt ngào của chàng trai người Pháp lãng mạn, rồi tới tình yêu nồng cháy giữa chàng và nàng, sự chia tay đau đớn, rồi đến cuộc rượt đuổi gay cấn giữa chàng cảnh sát trẻ và tên trộm lừng danh, sau đó là cuộc gặp gỡ định mệnh giữa 3 con người được số phận gắn kết với nhau....lật từng trang sách có cảm giác như đang thưởng thức một "bữa ăn tối" tuyệt vời với món "khai vị" tình yêu ngọt ngào, món "chính" "bùng cháy" với những cuộc rượt đuổi, và món "tráng miệng" tan chảy" với một cái kết đầy viên mãn!
5
409211
2015-09-01 22:52:06
--------------------------
285457
3419
Đây là cuốn thứ 6 trong bộ sưu tập Guillaume Musso của mình. Nếu cần một tiểu thuyết lãng mạn nhẹ nhàng mà đầy lôi cuốn trong tình tiết thì có lẽ nên chọn một tiểu thuyết của Guillaume Musso. Ấn tượng nhất trong câu chuyện này đối với mình có lẽ là thứ tình yêu thiêng liêng của 2 người, họ tìm được một nửa thật sự của nhau, và đối với họ không gì có thể thay thế được người kia. Dù có bao nhiêu năm cách xa, thì cảm giác thổn thức của trái tim khi gặp lại nhau vẫn như ban đầu. Đó là tình yêu thật sự mà mình cũng tin tưởng. Còn sự thú vị trong truyện, có lẽ phải kể đến bác sỹ Elliott Cooper, nhân vật chính trong " Hẹn em ngày đó"; Và người tình kiêm đồng nghiệp trước của Martin Beaumont - đại úy Karine Agneli, cùng hai sỹ quan Capella, Diaz chính là những người vớt cuốn sách của Tom Boyd, nhân vật chính trong cuốn " Cô gái trong trang sách "( chương 35). Giống như những con đường khẽ giao nhau ở một điểm, rồi mỗi người họ lại tiếp tục cuộc hành trình, lại tiếp tục viết tiếp cuốn tiểu thuyết của đời mình.
4
730244
2015-08-31 21:06:10
--------------------------
242685
3419
Đây là một cuốn sách khá ổn, cách viết đậm phong cách của nhà văn Guillaume Musso.
Phần đầu cuốn sách được kể với nhịp điệu khá chậm: cuộc tình chớp nhoáng của 2 nhân vật chính, cuộc sống của Martin hậu chia tay, và những miêu tả về Archibald Mclean. Theo tôi, phần này khá chán. Câu chuyện bắt đầu lôi cuốn hơn khi sự thật được hé lộ và Gabriella đứng trước 1 quyết định khó khăn.
Điểm nhấn của cuốn sách chính là đoạn ở sân bay, 1 cách giải quyết đầy bất ngờ, rất "Musso"
À, trong truyện này có 1 nhân vật khá thú vị, đó là bác sĩ Elliot Cooper và người yêu ông - nhân vật chính trong truyện "Hẹn em ngày đó".
Nhìn chung, đây là một cuốn sách đáng đọc 
4
400912
2015-07-26 21:10:25
--------------------------
235766
3419
Nhân vật nữ chính được khắc họa trong hoàn cảnh đầy bi kịch và rối bời! Cha cô và người yêu đầu của cô - một là cảnh sát, một lại là trộm. Một câu truyện đầy mê hoặc và kịch tính được xây dựng trên nền của một Paris hoa lệ cổ kính lên thơ rồi đến cả bờ biển San Francisco thơ mộng tuyệt vời.
Truyện được dựng như một cuốn phim tuyệt vời!
Bìa sách chân thực được tạo lên từ hình những mảnh ghép trong trò chơi xếp hình kinh điển với khuôn mặt nam được lộ rõ còn nhân vật nữ thì bị khuyết. Nó để lại cho người nhìn một sự mơ hồ khó đoán trước. 
Tình yêu, tình phụ tử luôn khó có thể chọn lựa được. Cùng khám phá xem nữ chính của cuốn phim này xử lý ra sao.
4
334680
2015-07-21 12:15:16
--------------------------
229846
3419
Mình đã đọc một lèo hết cuốn sách . Lãn mạn, li kì. Giọng văn được chau chuốt , các tình tiết đan xen thật khéo léo khiến người đọc không thể đặt cuốn sách xuống cho đến trang cuối cùng. Cách tác giả khiến câu chuyện thực tế đến mức nó gần như xảy ra với chính chúng ta, với những người quanh chúng ta làm câu chuyện hấp dẫn. Tác giả gần như cho chúng ta một gợi ý trong việc lựa chọn giữa hai người yêu thương nhất . Đây là một cuốn sách HAY về nội dung và ĐẸP về hình thức .
4
469423
2015-07-17 09:15:50
--------------------------
209590
3419
Mình thích cách mà tác giả vẽ nên câu chuyện, rất lãng mạn nhưng cũng rất thực tế. Con người ai cũng sẽ có lúc phải lựa chọn, nhưng làm thế nào để lựa chọn giữa hai người yêu thương nhất thì ít người rơi vào tình huống ấy. Tác giả miêu tả rất hay, từ phong cảnh, đường phố cho đến tâm trạng nhân vật. Cốt truyện hay, gay cấn, tình tiết đan xen, lối viết nhẹ nhàng lôi cuốn, ngay cả cách mà tác giả cho nhân vật chính giải quyết vấn đề cũng rất bất ngờ và đầy thú vị. Sách có bìa đẹp, bắt mắt, giấy in chất lượng tốt, chữ in đẹp, dễ đọc.
4
564333
2015-06-17 20:40:19
--------------------------
207009
3419
Những câu chuyện tình yêu trong tác phẩm của Guillaume Musso lúc nào cũng mang lại một cảm giác bình yên và lãng mạn nhất. Những thứ tác giả mang đến trong tác phẩm lúc nào cũng nhẹ nhàng và rất chân thật, đôi lúc làm ta nhầm tưởng ngẫu nhiên như là số phận. Hai người đàn ông, một là người yêu và một là cha của mình, Gabrielle thật sự rất mong manh và lúc nào cũng bị giằng xé giữa tình yêu và gia đình. Một cái kết hồi hộp và thiêng liêng, những cái vé máy bay giữa sự sống và cái chết tạo ra một câu chuyện huyền ảo có chút hư cấu, nhưng mang lại một vẻ riêng biệt cho tác phẩm của Musso. Những sự hy sinh và tha thứ luôn là những tiêu chí xuất hiện ở trong những tác phẩm của tiểu thuyết gia này.
4
537001
2015-06-11 11:01:05
--------------------------
205476
3419
Mâu thuẫn không quá cao trào giữa các nhân vật là điều tác giả chưa lột tả để hướng được cảm xúc người đọc thăng hoa . Tuy nhiên , trong tác phẩm này ta có thể thấy được những đoạn dẫn đầy triết lí , tình yêu duy nhất trong đời được tìm thấy chỉ 1 lần . Tình yêu giữa các nhân vật lãng mạn nhẹ nhàng và chân thành từ trái tim đến trái tim . 
Tác giả xây dựng cho chúng ta cánh cổng giữa sự sống và cái chết để suy ngẫm và cuộc sống hiện tại của chúng ta . Và đâu đó bên kia ngưỡng cửa thiên đàng và địa ngục tình yêu vẫn tồn tại vĩnh hằng .
4
458991
2015-06-06 21:47:11
--------------------------
204858
3419
Khi bắt đầu, ta mãi mê say đắm với tình yêu, thứ tình yêu mà tác giả muốn chúng ta nuốt chửng bằng mọi giá. Buồn thay, ái tình đó chỉ nghẹn lại ngang cổ. Và rồi bỗng sự chạm trán của hai người đàn ông, kẻ trốn người đuổi. Một cảnh sát và một tên trộm, cường tráng ngang nhau, thông minh ngang nhau, tài năng ngang nhau, cố chạy thoát khỏi đám đông. Đến lúc gần như bắt được, khi người đọc bắt đầu nhập tâm vào, thì đáng tiếc là gây cấn chỉ kéo dài một giây.

Nhiều tình huống đan xen mà tác giả chưa làm rõ. Các nhân vật ít ấn tượng, họ mãi mãi đắm mình trong cuộc điều tra không cần thiết, thay vào đó nên là các hành động mà người đọc chờ đợi xem họ đối phó với những tình huống này ra sao. Những cuộc đối thoại cũng gây nên hành động sai lầm của các nhân vật ...
3
7141
2015-06-05 09:24:39
--------------------------
204450
3419
Mỗi tác phẩm của Musso là một sự sáng tạo rất khác và rất riêng, và mình tự hỏi liệu có môtip cũ được lặp lại? Nhưng không. Musso tạo nên tình huống tưởng như bị bế tắc, nhưng cũng tìm được cách giải quyết cuối cùng. Lôi cuốn, hấp dẫn và dễ bị cuốn theo mạch truyện, cuốn sách phù hợp để tìm kiếm chút gì đó phiêu lưu, hành động nhưng không kém phần lãng mạn. Và thực sự cuộc truy đuổi này quan trọng ở sự lựa chọn của Gabrielle, hay ở cách mọi người lựa chọn để đối mặt với cuộc đời của mình?
5
26110
2015-06-04 06:50:55
--------------------------
