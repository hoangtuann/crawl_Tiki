429086
5215
Hai vụ án đáng quan tâm lả vụ cướp nhà băng và vụ bức tường đỏ. Trong vụ cướp nhà băng tội phạm rất thông minh khi có kế hoạch giả trang thành con tin và cho nổ bom số tiền giấy cùng các con tin bị chúng mặc đồ y như cướp để đánh lừa cảnh sát. Vụ án bức tường đó thì thú vị hơn nhờ tác giả lồng vào đó là những tư tưởng của Khổng Minh Gia Cát Lượng, cách chơi cờ tướng và hai thanh tra nổi tiếng giỏi và chính nghĩa. Lâu lâu có được tập hay thấy cũng đáng tiền mua. 
5
535536
2016-05-12 14:11:21
--------------------------
362139
5215
Conan tập 65 nối tiếp tập truyện chiếc két Takuni với bao điều thú vị. Kaito Kid - chàng siêu trộm lãng tử lại xuất hiện và Conan tiếp tục lật tẩy từng chiêu trò ảo thuật của anh chàng. Truyện còn có sự xuất hiện của Khổng Minh rất ngầu trong tập bức trường đỏ. Qua Conan tôi học được rất nhiều kinh nghiệm sống và cả những kiến thức mới mẻ như trong tập này là sự bù màu giữa màu xanh và màu đỏ. Nhiều lúc tôi cảm thấy đọc Conan sẽ rất hữu ích vì vừa giải trí lại học hỏi được nhiều điều.
3
362041
2016-01-01 13:01:11
--------------------------
294511
5215
vụ bức tường đỏ ban đầu thấy ghê nhưng nhờ có khổng minh Morofushi Komei và Conan mà vụ án được sáng tỏ. hiệu ứng màu sắc từ màu đỏ và màu trắng thành màu xanh... từ đó mình biết được là vụ này xuất phát từ những màu sắc ở trong bệnh viện bởi đồng phục của các bác sĩ. vụ chiếc két sắt Takuni thì mình lại cảm thấy hài hước hơn là thấy ghê vì những bẫy mà két sắt gây ra. và thú vị hơn khi mà báu vật mà ông bác Suzuki ở trong két lại là chú chó Pluto dễ thương... truyện tranh nét rất đẹp và chất lượng giấy tốt nữa... thật sự rất thích.
3
568747
2015-09-09 13:49:20
--------------------------
197588
5215
Tập 65 tập trung chủ yếu vào vụ án bức trường màu đỏ bí ẩn và hấp dẫn. Nhân vật mới có biệt danh là Khổng Minh xuất hiện với sự thông minh, tài năng và gương mặt cũng làm mình nhớ tới Khổng Minh - Gia Cát Lượng. Càng ngày các vụ án càng kì bí, lôi cuốn người đọc hơn với các tình tiết ngày được một mở rộng cùng các nhân vật mới có nhiều cá tính mạnh mẽ, đặc biệt hơn. Hình ảnh trong truyện cũng ngày một đẹp hơn, lột tả được vẻ đẹp và tính cánh nổi bật của từng nhân vật. Đây là một tập truyện hay và đáng để dành thời gian đọc.
5
45656
2015-05-18 00:07:29
--------------------------
183955
5215
Vẫn là những tình tiết nhẹ nhàng nhưng không kém phần kịch tính, Conan tập 65 tiếp tục đem lại những vụ án hay không phụ lòng người đọc. Có lẽ vụ án ấn tượng nhất trong tập này là vụ bức tường lửa, với sự xuất hiện của một gương mặt mới toanh đầy bí ẩn với biệt danh:"Khổng Minh". Nhân vật này khiến mình khá tò mò và thích thú. Mong là ở các tập sau "Khổng Minh" sẽ xuất hiện nhiều hơn và những bí ẩn đằng sau nhân vật này sẽ dần được hé mở. Còn về hình thức thì quyển truyện này vẫn giữ nguyên được chất lượng tốt vốn có, với nét mực sắc, đậm và đều màu và sử dụng loại giấy tốt, không quá dày cũng không quá mỏng.
4
558345
2015-04-16 21:39:04
--------------------------
