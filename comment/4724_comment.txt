473618
4724
Thật sự là đọc tập 2 xong là đọc tập 3 liền luôn chứ không chờ đợi được nữa để tiếp lục được cuốn vào những vụ án của Bản thông báo tử vong này. Tiếp tục vụ án chia cắt thi thể rùng rợn mà 18 năm trước Đinh Hạo vẫn không thể phá án được. Tác giả phân tích rất là chi tiết khiến mình tưởng tượng không khỏi rùng mình mỗi tối. Thác mắc nối tiếp thắc mắc với vụ huyết án ở tòa nhà Long Vũ. Mình chẳng thể nào không ngừng suy nghĩ về cách gây án của Eumenides để  thể lẻn vào tòa nhà Long Vũ được rất nhiều người đứng bảo vệ thế mà vẫn vào được để gây ra vụ huyết án này. Thật li kì và hấp dẫn không thể ngừng đọc.
5
1453730
2016-07-10 19:24:15
--------------------------
431539
4724
Tập 3 Bản Thông Báo Tử Vong (Số Mệnh 2) tháo gỡ những bí ẩn đưa ra từ phần 1, theo tôi về mặt ý tưởng tác giả làm khá tốt, sự thật tuy không quá sock nhưng đủ đáp ứng được mong đợi của độc giả. Sẽ có bất ngờ về vụ án chia cắt thi thể nếu bên đơn vị sách làm cẩn thận, chu đáo hơn, không đặt tựa chương tiết lộ hết nội dung như vậy. Dù sao tôi vẫn đánh giá cao tập này hơn tập Số mệnh 1, dù nhiều đoạn vẫn dài dòng.
4
386342
2016-05-17 15:40:39
--------------------------
375738
4724
Nhiều giai đoạn được thực hiện để nhu cầu còn cần tới khi đưa cách nhân vật còn thiếu sự hậu thuẫn từ phía người đáng tin cậy dành cho độc lập một cách văn minh hơn , giấu đi sự yêu thích của cả một mặt những ai biết gắn bó phân tích , bình phẩm chưa đi đến đâu , đây là vụ án được thắt chặt nên an ninh cần được đảm bảo lâu hơn , rõ hơn biết đâu có lúc từ giai đoạn đầu thử thách cam go mà bản chất lại dần được biết đến sau .
4
402468
2016-01-29 04:13:47
--------------------------
357478
4724
Tập này nối tiếp tập trước, và là tập mang lại nhiều lời giải cho những câu hỏi của người đọc ở những tập trước: về sự thật về Viên Chí Bang tại sao trở thành Eu đời 1, về nhân thân của Eu kế tục, về cái chết của cha Eu, cũng như hé lộ về nguồn gốc tài chính của Eu...

Tuy nhiên vẫn có những hạt sạn: vẫn sai lỗi chính tả (tuy có phần ít hơn, đơn của là "mũ lưỡi chai" thay vì "mũ lưỡi trai", sai tên người (đoạn mô tả Hàn Hạo bị Eu lợi dụng thì viết thành Mộ Kiếm Vân bị lợi dụng). Trên hết là lỗi về việc thổi phồng quá mức về tài năng, công nghệ của Eu sử dụng bất chấp thời gian (truyện xảy ra vào năm 2002, mà Eu có thể gắn con chip nghe lén nhỏ xíu vào vỏ  dt, mà nguồn điện hoạt động lại thông qua pin  dt => quá vô lý).
Kế đến là khả năng của Eu quá lợi hại, chả biết Eu học từ ai mà có thể: phản trinh sát, cao thủ mạng, cao thủ cận chiến, cao thủ ngân hàng, hay la hacker...Có vẻ Eu là siêu nhân :D

1 chi tiết nữa là khi đọc cảm giác giống Việt Nam ở chỗ tham nhũng, lũng đoạn, lợi dụng chức quyền của cán bộ: đơn củ là Đinh Chấn dù có là truyền kỳ, đội trưởng đội cảnh sát hình sự đi nữa mà vẫn có thể đổi trắng thay đen, bắt người bắn tỉa làm bia mà bất chấp tiền đồ của người lính đó. Vả lại khi đã điều tra ra ông 1 tay che trời như vậy mà không có biện pháp nào trừng phạt dành cho ông ta cả mà vẫn kính trọng. Vả lại xây dựng ông như 1 vị thánh khi từ chức để đi tìm 1 lý tưởng cao xa là tìm nguồn gốc của tội ác, để chữa lành từ gốc đó trong suốt 10 năm ròng nhưng không mang lại lợi ích cụ thể nào hết. Ngay cả tâm hồn con của ông cũng chưa chữa lành được.

Nói túm lại, phần này chẳng có gì hay
4
23913
2015-12-23 17:36:17
--------------------------
306634
4724
Mình đã đọc cuốn 1 - "Bảng thông báo tử vong" và có cảm nhận cách hành văn và đặt vấn đề hao hao giống series của Lôi Mễ. Người đọc nếu tinh ý cũng có thể phần nào đoán ra một vài điểm mấu chốt của câu truyện. Một điểm trừ nữa là vẫn có lỗi chính tả trong sách
Tuy nhiên cách xây dựng nhân vật Eu khiến cho người đọc phải băn khoăn và cũng không biết nên ủng hộ hay phản đối Eu. Có lẽ trong mỗi chúng ta cũng từng có cảm giác "thù ghét" ai đó mà không thể làm gì được, phải chăng lúc đó chúng ta cũng muốn có 1 Eu trên đời thậm chí trở thành Eu như La Phi hồi còn là sinh viên?
Tất cả những điều này khiến tôi háo hức khi hôm nay nhận được phần 2 và phần 3 của series này
Tôi sẽ đợi phần 5 dịch xong sẽ mua nốt 2 cuốn còn lại
4
777678
2015-09-17 18:08:57
--------------------------
291474
4724
Mình được bạn giới thiệu đọc những tác phẩm của Chu Hạo Huy. Mình rất thích thể loại trinh thám này. Hồi trước giờ mình chỉ đọc Ngôn tình trinh thám như Hãy nhắm mắt khi anh đến của Đinh Mặc, chuyển sang đây bị ghiền luôn. Văn phong tác giả rất mãnh liệt, có những đoạn đấu trí, những màn cao trào, khi bắt đầu đọc thì không thể nào dừng được Bìa sách được đầu tư ấn tượng. Điểm mình không thích lắm là giấy  trắng, đọc không dịu mắt như giấy phần lan, và không được bọc màng co nữa.
5
52955
2015-09-06 12:16:52
--------------------------
289999
4724
Về nội dung: Hình như mọi cuốn hút hấp dẫn của mấy tập trước dồn hết vào tập này, mình đọc mà chỉ muốn đọc vèo phát hết luôn vì sự tò mò, hiếu kì và cuộc đấu trí đầy kịch tính này mang lại. Đôi lúc cảm giác như nghẹt thở với từng câu chữ.
Về hình thức: Bìa sách khá là bắt mắt, đúng kiểu thiết kế bìa mình thích. Nhưng có điều là đôi khi đang đọc cảm xúc dâng trào thì lại bị hẫng một nhịp bởi lỗi đánh máy và có vài câu dịch làm mình khó hiểu.
4
50935
2015-09-05 00:09:57
--------------------------
231052
4724
Mình đã đọc hết cả ba phần của series này , và mỗi cuốn mình chỉ mất một ngày để đọc. Thật sự là mình không thể nào dừng đọc được vì những tình tiết trong câu chuyện quá hấp dẫn và lôi cuốn đối với mình.Đây thật sự là một trong những tác phẩm viết về trinh thám mà mình yêu thích nhất .Những tình tiết trong truyện có lúc bị đẩy lên cao trào khiến mình không thể dừng mắt để làm việc khác được.Như cái tựa đề của tác phẩm,tên sát thủ trong truyện thật sự là một tên sát nhân mà có thể giết chết được đối phương một cách vô cùng dễ dàng.
4
483530
2015-07-17 21:13:21
--------------------------
223460
4724
Tôi mua cuốn đầu tiên của seri truyện này chỉ vì tôi đã đọc 1 cuốn khác cùng tác giả trước đây. Tuy nghiên sau đó tôi đã thực sự bị cuốn hút và phải mua ngay 2 cuốn 2,3 còn lại. Đọc một lèo không thể ngừng hết cả bộ để rồi tự mình suy ngẫm lẽ đúng sai của cuộc đời, ý nghĩa của từ" công bằng" và "số phận", cái ác phải bị trừng phạt nhưng không thể không khiến người trong cuộc cảm thấy ray rứt dằn vặt về sự lựa chọn thực thi đúng chức trách hay đúng theo nhân quả. Một bộ sách đáng để đọc với đầy đủ các sự bất ngờ và lối tư duy logic chặt chẽ. Rất xứng đáng để mua. Khi nào sẽ có cuốn tiếp vậy tiki nhỉ?
5
46791
2015-07-07 09:28:10
--------------------------
213275
4724
Trí tuệ của tôi đã thực sự bị khiêu chiến khi xem đến cuốn này. Ngay cả những tình tiết nhỏ nhất cũng chứa đựng đầy yếu tố bất ngờ.( Chẳng hạn như chi tiết Hàn Hạo được hai tên đệ tử của A Hoa dẫn đường chạy trốn cảnh sát, tưởng như bọn chúng đã "cùng hội cùng thuyền" với nhau, thế mà vẫn có điều bất ngờ xảy ra). Thế nhưng truyện vẫn có những khoảnh khắc khiến lòng ta chùng lại, như chi tiết Eumenides và cô gái mù Trịnh Giai có những đồng cảm sâu sắc với nhau. Dường như chính điều này làm cho nhân vật sát thủ máu lạnh ấy trở nên "hào hoa và lãng mạn"? Không biết bạn thì sao, riêng tôi, vẫn mong muốn hắn đừng có một kết thúc đáng buồn!
4
38833
2015-06-23 16:39:54
--------------------------
213175
4724
Bìa truyện sáng, thiết kế đẹp và nội dung tóm tắt hay, khiến cho người khác phải tò mò. Nội dung cốt truyện sâu sắc và hồi hộp, gây cấn. Thật sự khi đọc đến phần kết của cuốn sách, lúc La Phi tìm ra được Eumenides, mình hòan toàn bất ngờ. Truyện không chỉ đem đến cho người đọc sự hồi hộp, tò mò mà còn là những bài học về tình đồng nghiệp, về nhân sinh và nó gửi đến một ý nghĩa: pháp luật ở bất cứ nơi đâu, thời đại nào cũng đều có kẽ hở. Và đội khi cũng chính vì những kẽ hở đó mà con người không còn tin vào pháp luật nữa. Từ đó, họ sẽ trở thành tội phạm, muốn tự tay mình tiêu trừ cái ác. Nhưng rồi, cuối cùng họ cũng phải chịu trói trước pháp luật.
5
135452
2015-06-23 14:39:59
--------------------------
192090
4724
Đây là một trong số không nhiều truyện thuộc thể loại trinh thám của Trung Quốc mà mình đánh giá là khá hay. Lồng trong câu chuyện đấu trí căng thẳng giữa cảnh sát và tội phạm là sự va chạm giữa nhân sinh quan và cách nhìn nhận cuộc sống của các nhân vật. Trong đó nhân vật chính, cảnh sát La Phi là một người thông minh, có óc quan sát và cách suy luận kín kẽ, khác người. Tuy nhiên anh có một quá khứ không vui vì những sai lầm tưởng như vô tình. Anh cũng có mối liên hệ với kẻ chủ mưu những tội ác và đang thách thức cảnh sát.
5
790
2015-05-03 16:56:02
--------------------------
167023
4724
Tiếp nối thành công của 2 tập trước, ở tập 3 này, chúng ta sẽ tiếp tục đc chứng kiến cuộc đối đầu đầy hấp dẫn và kịch tính giữa cảnh sát và Eumenides. Không những thế, lần này La Phi và đồng đội còn phải đương đầu với A Hoa và Hàn Hạo trong vụ hung án trong phòng kín ở tòa nhà Long Vũ, đồng thời phải chạy đua với Eumenides để phá vụ án chia cắt thi thể 1.12. Đặc biệt là trong tập này sẽ chứa đựng những chi tiết khiến độc giả vô cùng bất ngờ, đẩy trí tò mò của độc giả lên đến đỉnh điểm, đã cầm sách lên đọc thì ko thể nào đặt xuống đc. Theo mình là thực sự rất đáng đọc!!!
5
561610
2015-03-13 21:34:56
--------------------------
