472655
8614
Người thầy Trần Văn Khê tuy đã ra đi nhưng sự nghiệp của thầy vẫn còn mãi. Nhờ có thầy, những nhạc cụ dân tộc, những di sản văn hóa phi vật thể của Việt Nam mới được công nhận, nhờ có thầy mà rất nhiều bộ hồ sơ về giá trị văn hóa Việt Nam đã được UNESCO xét duyệt. Đọc "Hồi ký Trần Văn Khê", ta sẽ hiểu được tấm lòng bao la của thầy dành cho nền âm nhạc dân tộc Việt Nam, hiểu được của thầy không phải là tự nhiên mà có, tất cả đều do nỗ lực không ngừng nghỉ của thầy.
5
169341
2016-07-09 21:01:28
--------------------------
466543
8614
Một cuộc đời quá đỗi truân chuyên, quá đỗi những gian nan thử thách mà bậc cây da cây đề giáo sư Trần Văn Khê  đã trải qua và nổ lực làm việc bất kể ngày đêm, dù ở những vùng đất xa xôi, hay những lúc thân mang trọng bệnh cũng chỉ mong mang tinh thần, giá trị đẹp của âm nhạc dân tộc đến với thế giới. Đọc rồi mới thấy cái cơ cực, cái tâm và cũng như cái tầm chưa bao gìơ nhỏ của người thầy lớn. đọc để thấy quý trọng cái hay cái đẹp của dân tộc mình và cố gắng phát huy nó.
4
643050
2016-07-02 13:33:25
--------------------------
269002
8614
Qua những lời tự sự của GS Trần Văn Khê, tôi có thể hiểu thêm rất nhiều về những nét văn hóa truyền thống dân tộc và cả trong cách sống ở đời. Thầy là một tấm gương sáng cho chúng ta noi theo về tinh thần học hỏi không ngừng, biết giữ gìn văn hóa truyền thống dân tộc và đem cái hồn văn hóa ấy vươn xa đến tầm thế giới. 
Quyển sách như những trang nhật ký ghi lại cuộc hành trình tìm hiểu, yêu thích và truyền đạt vốn cổ đến với thế hệ sau. Thật đáng quý!
5
24360
2015-08-16 23:49:35
--------------------------
