366377
5426
Đây là một tiểu thuyết khá hấp dẫn và kịch tính. Mình đã đọc tiểu thuyết này trên trang điện tử vnexpress.net, và theo dõi từng kỳ. Những người phụ nữ trong câu chuyện này, dường như ai cũng rất xinh đẹp quyến rũ và hầu hết đều sống vì người đàn ông mà mình yêu. 
Motip của câu chuyện không mới. Tuy nhiên, dưới ngòi bút của tác giả, lại trở nên gần gũi, sống động, cứ như những nhân vật đó sống bên cạnh chúng ta vậy. 
Nếu bạn là một cô gái mộng mơ, vẫn tin tưởng trên đời này có soái ca, và trăm năm tình yêu vẫn như một, thì đừng nên đọc câu chuyện này.
4
501097
2016-01-09 20:28:24
--------------------------
345846
5426
Mình rất quan tâm đến đề tài viết về tham nhũng hay nông thôn của văn học Trung Quốc. Vì vậy mình mua cuốn này về đọc. 
Về nội dung truyện cũng ok, tuy nhiên kịch tính thì không nhiều. Nội dung khá rõ ràng rồi nên không gây nhiều bất ngờ cho người đọc. Mình đã từng đọc cuốn "đóng vai người tình" - tiểu thuyết cùng đề tài rất hay nhưng mua không có :(
Nhân vật Hứa Thiếu Phong - ông quan chồng được xây dựng tính cách mình thấy chưa "đã" lắm. Làm tới cục trưởng rồi nhưng vẫn phải nhìn người trên, phòng người dưới, dễ dàng bị cấp dưới là phó cục trưởng hạ gục bằng những bất cẩn không đáng chút nào. Mà khi xảy ra biến cố lại không biết nhờ cậy được ai? dựa vào mối quan hệ mơ hồ từ bạn thân cô bồ nhí !!!! Thật ra thoát được ko phải vì mối quan hệ này mà do sự thể ko đến nỗi nghiêm trọng để mà bị truy cứu!
Ông này liêm minh không? Không! Ông thủ đoạn không? Cũng có nhưng ko tới! Trong khi đối thủ của ổng là cục phó, thấp hơn nhưng lại có đầy đủ mưu mô, có "chống lưng" vững chắc, dám ra mặt đối đầu với cục trưởng! Thấy hơi phi lý!
xây dựng nhân vật vợ chồng nhà bí thư Đảng ủy rất liêm minh, bà vợ cực kì khôn khéo trong cách xử lý tình huống. Vậy mà khi Hứa Thiếu Phong bị điều tra có bồ nhí, đã ra kết luận là không đúng, nhưng đến khi điều chuyển nhân sự lại không bảo vệ nhân sự triệt để?!! Có phải là liêm nhưng không minh? (thà kết luận là "Có" rồi xử lý triệt để thì tính cách nhân vật rõ ràng hơn)
Nói chung, cách viết truyện, xử lý tình huống có hấp dẫn. Tuy nhiên, xây dựng nhân vật đối với mình chưa thực sự thuyết phục

3
895234
2015-12-01 13:52:25
--------------------------
336998
5426
Đây là một cuốn tiểu thiết hiện đại, phản ánh hiện thực xã hội Trung Quốc trong chốn quan trường. Truyện mang đậm những sách lược chính trị, những thủ đoạn cạnh tranh trong giới chức đương thời. Bên cạnh đó là các chiêu trò, kế sách của các bà vợ quan - quan hệ với nhau dựa vào mối quan hệ, cấp bậc của các đức ông chồng. 
Truyện là những bài học sâu sắc về các mối quan hệ trong xã hội, trong công việc, tình cảm vợ chồng, tình yêu trai gái, sắc -dục... đều tựu trung trong cuốn truyện này.
Lối viết lôi cuốn, các tình huống gay cấn dẫn dắt người đọc như sống trong bối cảnh của câu chuyện, nhờ đó truyện trở nên hấp dẫn đối với nội dung không quá mới mẻ này!
 
 
5
895614
2015-11-13 13:08:20
--------------------------
336438
5426
Mình it đọc thể loại ngôn tình - tâm lý xã hội nhưng đọc truyện này cho mình nhiều trải nghiệm that sự hay. Truyện kể về cuộc chiến của hai người đàn bà với một người đàn ông quyền lực. Mình chưa đủ lớn để hiểu được những chuyện như này có xảy ra như những lời văn không nhưng mình tin là phụ nữ luôn có cách ứng xử thong minh và khôn khéo nhất. Đọc truyện hồi hộp, kịch tính như xem một bộ phim hành động hoặc như đọc truyện của Sidney Sheldon vậy. Mình khá là hài lòng.
5
854791
2015-11-12 13:50:31
--------------------------
