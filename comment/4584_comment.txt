430781
4584
Trên tiki có rất nhiều bản Thi nhân Việt Nam, nhưng mình quyết định chọn mua quyển này, vì đây là quyển có nhiều đánh giá tốt nhất. Quả thực là lúc mua về rất bất ngờ, vì không nghĩ quyển sách đẹp hơn rất nhiều so với ảnh chụp, thậm chí còn đẹp hơn mấy cuốn ngôn tình mua cùng. Và nội dung bên trong cũng không phụ thời gian mình trông đợi, mặc dù mất hai tháng mới đọc xong, nhưng mà lời bình của Hoài Thanh vô cùng sâu sắc, giọng văn điêu luyện, ngoài lời bình ra còn có nhiều bài thơ do các tác giả sáng tác, mình có thể đọc được rất nhiều bài thơ hay chưa đọc. Thực sự rất vui khi mua được quyển sách này.
5
697477
2016-05-15 21:26:38
--------------------------
427878
4584
Mình đã mua cuốn sách này, bìa dày và cứng cáp, giấy khá tốt. Quyển sách nổi tiếng trong làng thơ VN rất hay, nó nói về quá trình phát triển của thơ mới như thế nào, mặt tích cực và tiêu cực trong quá trình đó ra sao, giới thiệu những nhà thơ nổi tiếng cùng với những tác phẩm để đời của họ, thật đáng để mua và đọc.
Nhưng về bản thân mình không phải là một người cảm thụ thơ sâu sắc nên có một số bài thơ đọc không hiểu cho lắm
Cuốn sách này rất hữu ích với những bạn đang ôn thi đại học có môn Văn, và những bạn học khoa Văn trong các trường đại học VN
5
733793
2016-05-10 10:27:07
--------------------------
419986
4584
Hai nhà phê bình Hoài Thanh và Hoài Chân đã đưa ra những lời nhận xét tinh tế về tư tưởng và tình cảm của nhiều nhà thơ mới. Hàn Mặc Tử điên cuồng trong tình ái, Huy Cận với những vần thơ ảo não, Xuân Diệu khát sống thèm yêu. Bên cạnh đó, còn những nhà thơ quen thuộc với chúng ta suốt chương trình học phổ thông. Kèm theo những lời bình sâu sắc là trích dẫn những bài thơ động lòng người như : Nhớ rừng, Vội vàng, Tình trai, Tràng giang, ... Quyển sách là món quà quý giá mà mỗi người nên có.
4
1061830
2016-04-22 12:53:52
--------------------------
393442
4584
Đây là một cuốn sách rất bổ ích cho những ai gắn kết cuộc đời với văn chương. Phong văn Hoài Thanh Hoài Chân rất sắc bén, tạo nên vô số phê bình lập luận giàu giá trị.Giọng văn điêu luyện, thật đáng khâm phục khi tác giả viết cuốn đó trong độ tuổi trên dưới 20, vậy mà đã có những nhận xét sâu sắc, những phát hiện độc đáo. Sách dày dặn, bìa cũng xinh đẹp, nên mua quyển này hơn mấy quyển tái bản rẻ hơn vì mình thấy chất lượng cuốn này đẹp hơn hẳn.Đây quả thực là 1 quyển sách tuyệt vời,là cái nhìn sâu sắc và toàn tiện về thơ mới cũng như các thi nhân trong phong trào này. 
5
858738
2016-03-08 19:40:45
--------------------------
355961
4584
Mình mua sách này cũng đã được hai tháng rồi. Bây giờ viết nhận xét thì chỉ ước gì lúc đó mình không nên mua. Bởi vì bây giờ Tiki giảm giá mạnh quá, còn nhiều hơn cả lúc trước nữa, làm mình thấy... tiêng tiếc. =))
Sách giới thiệu về thi nhân Việt, còn có vài tác phẩm tiêu biểu của họ nữa. Năm sinh của các nhà thơ được viết bằng tiếng Pháp =))
Giấy tốt, in cũng được, bìa ổn, vận chuyển ok. Nói chung, đây là cuốn sách nên mua từ Tiki để ngâm cứu =)))))
4
325563
2015-12-21 00:39:05
--------------------------
352688
4584
Vấn đề đôi mắt và thời đại luôn đi song hành với nhau. Tác phẩm thành bại cũng do yếu tố này mà có thể trở thành tiếng vang hay không. Cũng như người làm nghệ thuật trước hết phải thấu hiểu thời đại, thấu hiểu con người thì mới có thể thấu hiểu nghệ thuật. Hoài Thanh- Hoài Chân đã chỉ cho tôi điều này trong cuốn sách " Thi nhân Việt Nam". Cuốn sách là tuyển tập phê bình của 2 nhà phê bình, nghiên cứu văn học xuất sắc, ưu tú. Một số tác giả được 2 ông đề cập đến như Huy Cận, Xuân Diệu, Hàn Mặc Tử,.. đều là những tác giả có đóng góp lớn vào nền văn học nước nhà. Cách biên soạn tỉ mỉ, trau chuốt trong từ từ ngữ, nét nghĩa, lời bình luận sâu sách, giàu thuyết phục, 2 ông đã để lại trong lòng người yêu văn học những xúc cảm mới mẻ, sâu sắc về nghệ thuật.
5
821598
2015-12-14 21:48:33
--------------------------
340937
4584
Thi Nhân Việt Nam là cuốn sách có gần như đầy đủ tất cả những nhà thơ trong giai đoạn Thơ Mới , những người đã in dấu ấn riêng của bản thân, đã đặt nền móng cho thi ca dân tộc, khẳng định và chứng minh cái tôi, tính bản ngã .Có thể nhắc đến trên thi đàn Thơ Mới Hàn Mạc Tử, Nguyễn Bính ,Xuân Diệu,Lưu Trọng Lư , Thế Lữ, Xuân Quỳnh,Huy Cận,...Mỗi người một phong cách , một cá tính nhưng sự đóng góp và sức ảnh hưởng đã làm thay đổi bộ mặt của nền thơ ca Việt Nam 
5
915482
2015-11-20 21:12:12
--------------------------
287425
4584
tôi rất thích cuốn sách này thống nhất cả nội dung và hình thức. Một cuốn sách rất đáng đồng tiền mà các bạn thích văn nên đọc để nâng cao hơn về kiến thức của mình. Tựa như một cuốn bách khoa toàn thư có cả tác giả tác phẩm của họ từ năm 1932 đến năm 1941 do Hoài Thanh và Hoài Chân tổng hợp.Các công trình nghiên cứu của thơ mới cho người đọc giả một cái nhìn toàn diện hơn về giai đoạn thơ văn năm đó. Thế Lữ, Tế hanh, Hàn Mạc Tử, Nguyễn Bính, Lưu trọng lư,..
5
686606
2015-09-02 18:36:22
--------------------------
259198
4584
Tôi rất hài lòng về cuốn sách này. "Thi nhân Việt Nam" là sự khám phá và đánh giá đầu tiên đối với Thơ mới. Cuốn sách như một quyển từ điển tra cứu về tác giả và các tác phẩm của họ trong phong trào Thơ mới. Cách trình bày khoa học, mở đầu là tiểu sử cuộc đời nhà thơ, phong cách rồi đến các sáng tác tiêu biểu, đầy đủ và rõ ràng. Bên cạnh đó còn có những lời bình phẩm về việc nghiên cứu Thơ mới, giúp người đọc có cái nhìn chân thực, đúng đắn. Cuốn sách đáng có đối với ai muốn tìm hiểu nền văn học nước nhà.
5
608852
2015-08-09 10:34:42
--------------------------
223185
4584
Mình nhận được sách vào hôm nay (6/7/2015) ! Đây là quyển tái bản nên bìa đẹp, giấy tốt, hơi ngả vàng để tránh mỏi mắt khi đọc lâu...Rất hữu ích đối với những người yêu thích văn học... Giọng văn điêu luyện, thật đáng khâm phục khi  tác giả viết cuốn đó trong độ tuổi trên dưới 20, vậy mà đã có những nhận xét sâu sắc, những phát hiện độc đáo! Quả là thiên tài. Đây là một quyển sách tuyệt vời và vô cùng bổ ích. Mở rộng tầm nhìn của chúng ta về thi ca, như một tấm bia không bao giờ hao mòn giúp khơi gợi lòng tự hào về một thời đại lung linh ngày trước.  Một nguồn tư liệu quý báu !!! Một quyển sách rất đáng tiền !!!

5
681802
2015-07-06 18:58:27
--------------------------
222059
4584
 Tôi đến với " Thi nhân Việt Nam" do nó là đứa con tâm hồn của một nhà phê bình văn học tôi rất thích- Đó là Hoài Thanh,phần cũng vì mục đích học tập.Quyển sách này đã tạo nên 1 sức hút rất đặc biệt cho tôi ngay những trang đầu tiên,và rồi một nguồn cảm hứng mãnh liệt trong từng con chữ của 2 anh em Hoài  giúp tôi có thể cảm được thơ. Đây quả thực là 1 quyển sách tuyệt vời,là cái nhìn sâu sắc và toàn tiện về thơ mới cũng như các thi nhân trong phong trào này. "Thi nhân Việt Nam" sẽ rất hữu ích với những ai yêu văn học đồng thời cũng là bước đệm đưa ta đi sâu vào tâm hồn của thi ca
5
647133
2015-07-04 21:48:29
--------------------------
207912
4584
Đây là một trong những tác phẩm cơ bản nhất, tiên phong trong lĩnh vực sưu tập, bình phẩm văn chương. Đó như một sự ghi nhận cho những đóng góp mới mẻ, đầy sáng tạo nhưng lại mang những tư tưởng, quan điểm sâu sắc, tinh tế của các thi sĩ Thơ mới trong thời kì mà những thành kiến, định kiến của đại đa số các tầng lớp trong xã hội đều chưa thể thoát khỏi những hào quang chói loá từ quá khứ, từ những vần thơ Đường luật quy củ. Có thể nói, những nỗ lực, cố gắng của hai tác giả là rất đáng quý, trở thành nguồn tư liệu quý báu cho thế hệ sau.
5
561261
2015-06-13 15:40:12
--------------------------
197755
4584
Tác giả của cuốn sách là hai anh em  Hoài Thanh-Hoài Chân, Hoài Thanh vừa là nhà văn cũng đồng thời là nhà  phê bình, nghiên cứu văn học xuất sắc, ưu tú. Cuốn sách ra đời vào năm 1941- thời kì đỉnh cao của phong trào thơ mới khi mà các tác gia nổi bật của thời kì này như Xuân Diệu, Hàn Tử, Huy Cận đã sản sinh ra những tác phẩm để đời và vì thế cuốn sách đã gói gọn cái phong tào thơ mới bằng lẽ sâu sắc của hai anh em Hoài Thanh-Hoài Chân. cuốn sách được biên soạn hết sức tỉ mỉ và dày công cho ta một cái nhìn chân thật, đầy đủ về phong trào thơ mới, được sắp xếp rất dễ nhìn dễ thuộc. Giọng văn  hết sức điêu luyện, lời bình sâu sắc giàu sức thuyết phục đã chỉ cho ta thấy cái ưu cũng như cái hạn chế của các tác gia thời kì này.
5
351857
2015-05-18 13:29:54
--------------------------
