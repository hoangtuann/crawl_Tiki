298654
9789
Để mà nói tôi thực khá nản khi nhận được cuốn sách. Sách về đến tay tôi mà bìa thì quăn, bên trong còn hơn nhau mà nhìn như là sách cũ bị đọc qua rồi. Đây là một trong những cuốn sách mình mua tặng cho bạn mình ngày sinh nhật mà bị thế mình không biết phải làm sao. Giữ lại đọc thì nội dung khá sơ sài, các món ăn được giới thiệu trong toàn cuốn sách không phải món nào cũng tự các bạn độc giả có thể làm được tại nhà. Cách bày biện cũng khá sơ sài và khiến tôi không hề ưng ý!
2
419803
2015-09-12 20:11:39
--------------------------
244916
9789
Khi nhận được cuốn sách, quả thật mình có hơi bất ngờ vì sách khá mỏng và kích thước cũng không lớn lắm. Nhưng mà trong sách cũng có nhiều món ăn khá lạ, mình chưa nghe qua bao giờ như là vây nấu măng tươi, sốt gò, canh hoa thị nấu thả,... Các món ăn được xếp theo thứ tự nguyên liệu khá hợp lý, mỗi món được trình bày ngắn gọn trong 1 trang. Tuy nhiên, có rất nhiều món nguyên liệu lạ, khó tìm hoặc cách chế biến phức tạp nhưng sách lại không hướng dẫn kỹ cho lắm, vả lại cũng không có hình minh họa như nhiều sách ngày nay nên nhìn cũng hơi chán. Sách có thể phù hợp với những ai đã thạo việc bếp núc rồi!
3
10908
2015-07-28 16:09:54
--------------------------
223044
9789
Quyển này tuy mỏng nhưng lại có khá nhiều món ăn, rất đa dạng. Dễ có mà khó cũng có. Điểm chung là tất cả các món đều chỉ được giới thiệu cách làm ngắn gọn, có món chỉ vài dòng luôn. Đối với món dễ thì cách trình bày chỉ dẫn như vậy là hợp lý, dễ hiểu, nhưng có một số món phức tạp hơn, nhất là những nguyên liệu lạ lạ thì như thế lại thành ra quá khó hiểu, không hề có những điểm lưu ý hay hướng dẫn sơ chế kĩ càng. Có lẽ nó thích hợp với những người đã quen việc nội trợ nấu ăn rồi.
3
413317
2015-07-06 15:11:55
--------------------------
