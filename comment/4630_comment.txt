451577
4630
Bộ sách "Một ngày làm..." Có 5 cuốn và cuốn nào cũng hay cả! Với mình thì niềm đam mê về đất nước Ai Cập xa xưa huyền bí thật là một sức hút mãnh liệt. Các công trình kiến trúc và những vị Pharaoh uy quyền thật đem lại nhiều thắc mắc và lòng ham muốn được khám phá! Khi thấy cuốn này mình phải mua ngay kẻo hết hàng! Sách có màu và tranh minh hoạ dí dỏm, thông tin đưa ra không quá vĩ mô mà phù hợp với mọi lứa tuổi, đặc biệt là trẻ em. Mình khá hài lòng về cuốn sách này :)
4
946399
2016-06-19 21:54:09
--------------------------
443123
4630
Một bộ sách rất hay cho các bé học hỏi thế giới bên ngoài. Trong đó gần gũi và hay nhất là quyển "Một ngày làm Pharaoh". Không chỉ những đứa trẻ mà người lớn cũng có thể hiểu hơn về các tục lệ từ xa xưa của người Ai Cập cổ, về các tín ngưỡng thần linh, về cách tôn thờ và tin tưởng vị vua Pharaoh vĩ đại và những hoạt động sống của ông ta trong cung điện xa xưa và đặc biệt hơn hết là về các bí mật của Kim tự tháp. Quyển sách là vô vàn những bài học hay cho con em cũng như những người như chúng ta.
5
599570
2016-06-06 11:28:25
--------------------------
435737
4630
Khi đọc cuốn sách này bạn sẽ được đóng vai hoàng đế Ai Cập hay Pharaoh, được học những bài học để trị vì đất nước, hiểu biết thêm về lịch sử địa lý, văn hóa của Ai Cập cổ đại. Dường như bạn thực sự trải qua cuộc đời của một Pharaoh chính hiệu, được học các nghi lễ, biết tới các vị thần linh mà người Ai Cập xưa thờ tụng, được học những điều mà một vị Hoàng đế Ai Cập phải học và tuân thủ nghiêm ngặt. Bạn sẽ đóng vai một Pharaoh từ khi còn nhỏ tới khi thành một Hoàng đế thực thụ và rồi tự xây cho mình những kim tự tháp nguy nga để " trở về với cát bụi ". Đọc cuốn sách này không chỉ hướng dẫn bạn cách để trở thành một vị Pharaoh mà còn giới thiệu thêm về những văn hóa, phong tục của đất nước với 3000 năm lịch sử sông Nin. 
4
1337568
2016-05-25 17:18:46
--------------------------
379218
4630
Bí ẩn trước cái lịch sử được hình thành nên tính độc lập cao của người dân ai cập từ 2000 năm , thoát ra khỏi cái vẻ uy nghiêm , lạnh lùng là sự quyết định về cai quản vùng sa mạc , mới đầu tiên chàng vua trẻ pharaoh luôn phải dè chừng trước câu hỏi khó khăn , sống điều kiện cần vươn lên vị trí cao hơn , trong cả không gian và thời gian liên tục làm việc , vác nặng các khối đá di chuyển theo dọc con sông thần bí , giải thích nhiều điều mới .
4
402468
2016-02-10 01:04:36
--------------------------
259014
4630
Bạn từng ước mơ được làm pharaoh vĩ đại? Bạn muốn nắm quyền trị vì trong tay? Bạn hãy thử hóa thân thành Pharaoh qua cuốn sách ‘ Một ngày làm Pharaoh’ này. Bạn sẽ được dạy mọi điều cần thiết cho một Pharaoh vừa lên ngôi. Bạn sẽ phải học cách cư xử như một Pharaoh, cách chiến đấu như một Pharaoh, công việc hang ngày của Pharaoh. Và bạn còn được cung cấp danh sách những vị thần hộ mệnh mà bạn không thể quên. Và khi chinh chiến đã mệt, bạn có thể quay về cung điện và xây dựng những kim tự tháp, bức tượng,… để đời. 
5
673477
2015-08-09 00:14:36
--------------------------
247134
4630
Pharaoh nghĩa đen là "ngôi nhà lớn", chính là cung điện nơi Pharaoh sinh sống. Pharaoh vừa là vị vua tối cao của Ai Cập cổ đại, vừa là một vị thần trên trần thế. Đó là những thông tin cơ bản về Pharaoh.
Đây thực sự là một cuốn sách thoả mãn tất cả những tò mò của bản thân tôi về Pharaoh -  từ khi chưa là thái tử, đến khi thành một pharaoh thực thụ điều hành đất nước và chết đi, được ướp xác thế nào và đi vào "thế giới ngầm" (tức những niềm tin của người Ai Cập về "âm phủ" theo văn hoá phương Đông) ra sao?
Bạn cũng sẽ đhiểu rõ hơn về các vị thần trong văn hoá tâm linh Ai Cập nữa, thật thú vị biết bao!
Sách in màu, hình ảnh minh hoạ đẹp, rất chi tiết và cả những câu thoại nhỏ kèm theo rất hóm hỉnh.
5
292190
2015-07-30 09:52:53
--------------------------
