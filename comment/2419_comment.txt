566265
2419
Sách rất nhẹ dù rất dày. Chữ hơi nhỏ nên đọc hơi vất vả
4
180490
2017-04-06 21:16:07
--------------------------
503001
2419
Chào các bạn, nếu bạn nào có ý định mua mà còn đang do dự thì nên mua ngay. Mình mới nhận sách ngày hôm qua từ Tiki, sách cầm rất đã, chất lượng in rất tốt, nhẹ dễ cầm, nằm ngửa đọc rất sướng, giá thành hợp lý.
5
1435297
2016-12-31 09:43:57
--------------------------
494514
2419
Khuyên các bạn không nên mua bộ này. Box set cực mỏng, ọp ẹp. Trong quá trình vận chuyển kiểu gì cũng nát.
Sách chữ rất nhỏ các dòng chi chít nhau, không có ảnh minh họa.
1
1860768
2016-12-07 11:05:02
--------------------------
487970
2419
Do chỉ chia thành 2 cuốn, khổ lại nhỏ (10.5*17 cm) nên mỗi cuốn đều rất dày (hơn 1050 trang và 735 trang). Mỗi trang thì rất mỏng, phải cẩn thận khi lật trang, nhưng chất lượng giấy tốt, không thấy chữ mặt bên kia. 2 cuốn đựng trong box set rất đẹp, phù hợp cho các bạn muốn đọc và sưu tầm. Nhưng khi mình nhận hàng xong, mở hộp Tiki ra thì thấy do box set nhỏ nên được để đứng trong hộp (gáy sách quay xuống đáy hộp) chứ không để nằm ngang như bình thường, và cũng không chèn hộp. Kết quả của không chèn hộp và trong hộp chỉ có 1 box set 2 cuốn là box set bị rách và sách bị móp góc. Dù mình đã chọn bookcare và tiki cũng đã trừ bookcare của mình nhưng sách không được bọc (có thể hiểu được vì box set rất sát, nếu bọc sẽ không nhét vừa). Tiki đóng gói, giao hàng rất nhanh nhưng mình nghĩ nên coi lại chuyện chèn thùng hàng kỹ hơn với những đơn hàng sách, tránh xảy ra chuyện như mình, mất công đổi trả rất mệt. Về phần mình thì mình không muốn đổi trả vì dù box set rách, sách bị móp góc nhưng mình sợ nếu đổi lại box set khác thì nó còn rách hơn, nát hơn nếu Tiki vẫn đóng gói kiểu này.
3
673130
2016-10-29 07:09:59
--------------------------
487970
2419
Do chỉ chia thành 2 cuốn, khổ lại nhỏ (10.5*17 cm) nên mỗi cuốn đều rất dày (hơn 1050 trang và 735 trang). Mỗi trang thì rất mỏng, phải cẩn thận khi lật trang, nhưng chất lượng giấy tốt, không thấy chữ mặt bên kia. 2 cuốn đựng trong box set rất đẹp, phù hợp cho các bạn muốn đọc và sưu tầm. Nhưng khi mình nhận hàng xong, mở hộp Tiki ra thì thấy do box set nhỏ nên được để đứng trong hộp (gáy sách quay xuống đáy hộp) chứ không để nằm ngang như bình thường, và cũng không chèn hộp. Kết quả của không chèn hộp và trong hộp chỉ có 1 box set 2 cuốn là box set bị rách và sách bị móp góc. Dù mình đã chọn bookcare và tiki cũng đã trừ bookcare của mình nhưng sách không được bọc (có thể hiểu được vì box set rất sát, nếu bọc sẽ không nhét vừa). Tiki đóng gói, giao hàng rất nhanh nhưng mình nghĩ nên coi lại chuyện chèn thùng hàng kỹ hơn với những đơn hàng sách, tránh xảy ra chuyện như mình, mất công đổi trả rất mệt. Về phần mình thì mình không muốn đổi trả vì dù box set rách, sách bị móp góc nhưng mình sợ nếu đổi lại box set khác thì nó còn rách hơn, nát hơn nếu Tiki vẫn đóng gói kiểu này.
3
673130
2016-10-29 07:09:59
--------------------------
