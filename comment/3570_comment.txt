398831
3570
Cuốn sách có nhiều dạng đề hay và mới. Có nghị luận xã hội, nghị luận văn học. Trong nghị luận văn học còn có văn học Việt Nam và cả văn học nước ngoài (nhiều sách tham khảo khác đã lượt bỏ bớt phần này) Nội dung rất chi tiết. Có dàn ý tham khảo (đề phòng thầy cô giáo bắt các bạn viết dàn ý) và bài văn được viết một cách trau chuốt, giàu cảm xúc, giàu hình ảnh. Sách được in trên giấy chất lượng tốt, không quá sáng làm ảnh hưởng đến mắt. Đọc sách này mình đã làm văn rất tiến bộ , điểm được cải thiện lên rất nhiều- điều mà trước đây mình đã không làm được vì lựa chọn sách chưa phù hợp
4
258450
2016-03-16 20:07:35
--------------------------
355629
3570
"125 bài và đoạn văn hay " là một cuốn sách hay và rất hữu ích . Sách tổng hợp nhiều bài văn ở các dạng đề khác nhau, phân tích chi tiết và đưa ra dẫn chứng rõ ràng, rất sát theo nội dung và chương trình học.  Sách là một sự chọn lựa hữu ích để chúng ta tham khảo và rèn luyện kĩ năng viết văn. Sách rất phù hợp cho các bạn đang chuẩn bị tốt nghiệp và cho kì thi THPT Quốc Gia. Là một cuốn sách mà các bạn nên sở hữu và nên có trong tủ sách của mình :))
4
1037190
2015-12-20 13:09:46
--------------------------
326538
3570
"125 Bài Và Đoạn Văn Hay Lớp 12" là quyển sách tổng hợp nhiều bài văn nghị luận xã hội cũng và nghị luận văn học. Của các bạn học sinh cũng như các nhà lý luận phê bình. Các bài văn dựa trên cấu trúc chuẩn của đề thi THPT Quốc gia, nên rất hữu ích trong việc ôn luyện. Sách hệ thống các cách viết đoạn văn, cho đến viết các bài văn dài. Tuy nhiên, các bài văn chỉ tạm dừng ở mức đủ chứ chưa thực sự hay lắm! Sẽ là một sản phẩm tuyệt vời cho những ai mới bắt đầu với việc ôn tập viết văn.
4
302082
2015-10-26 04:23:01
--------------------------
304910
3570
Quyển sách 125 bài văn hay này rất hữu ích. Phân tích các tác phẩm trong chương trình rất kĩ, sâu sắc và tinh tế. Rất nhiều đề văn nghị luận xã hội hay để các bạn tham khảo. Quyển sách này rất phù hợp cho những bạn ôn thi trung học phổ thông quốc gia, đặc biệt là các bạn thi khối D giống như mình. Các bạn mình mượn sách của mình đọc thử cũng đều khen. Tác giả cũng là một cái tên quen thuộc với các bạn học sinh trung học phổ thông vì nhiều quyển sách phục vụ cho việc học tập môn ngữ văn, mình cũng đã sở hữu hai quyển sách của tác giả này!
4
693223
2015-09-16 18:44:02
--------------------------
