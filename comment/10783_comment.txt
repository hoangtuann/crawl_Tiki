360108
10783
Chứa nhiều hương vị ngọt ngào , sâu lắng về tình người với mối quan hệ xa xôi , nhiều bí ẩn kỳ lạ , phóng sự về một con người có trái tim mềm yếu cần phải rời bỏ đi tất cả điều thân thuộc về số phận , sự vực dậy của tinh thần kỳ diệu sau vấp ngã, xô bồ để đến với người có ý nghĩa đặc biệt quan trọng , nếu một trong số những con người như thế thì xã hội sẽ rộng mở với người có tấm lòng cao cả , sáng sức chứa đựng tâm hồn .
4
402468
2015-12-28 18:18:49
--------------------------
339703
10783
So với những cuốn khác của Rachel Gibson thì cuốn này hay hơn.
Kết thúc thì không bất ngờ, có thể đoán trước được ngay từ đầu, và vẫn là những tình tiết lãng mạn kiểu Mỹ tốc độ và đam mê. 
Nhưng với Giã biệt tình xa, mình đã thích Rachel hơn một chút, vì tuyến nhân vật đã có sự thay đổi. Vẫn là anh chàng đẹp trai, nhưng lần này đã thành ông bố đơn thân (tất nhiên, vẫn nóng bỏng). 
Vẫn là tình yêu dựa nhiều vào thể xác, nhưng tâm lý nhân vật đã có nhiều dằn vặt và tranh đấu hơn hẳn. 
Đặc biệt, nhân vật cậu con trai nhỏ là điểm nhấn, tạo ra được khúc ngoặt ở cuối truyện, làm cho câu chuyện khác đi so với những chuyện tình yêu thường gặp ở các cuốn trước của Rachel
4
20161
2015-11-18 15:44:34
--------------------------
298385
10783
Nếu đã đọc Hãy cứu em, Vướng phải tình em, Không phải tình hờ, khi đọc cuốn sách này  bạn sẽ thấy nó thật sự khác biệt ba tác phẩm trước. Truyện gần như xây dựng song song việc miêu tả tâm lý của cả Hope và Dylan khiến chúng ta không nhàm chán. Ngoài tình yêu, truyện còn xoay quanh về tình cảm gia đình, khiến cả câu chuyện thực tế ( khác với một số tác phẩm chỉ tập trung xoay quanh nói về tình yêu ).  Kết thúc truyện, Hope có nơi trái tim cô thuộc về, có sự nghiệp, và có cả chính mình.

nội dung thì ổn nhưng hình thức có vẻ hơi tệ, nhà xuất bản nên xem xét lại trình bày cũng như chất lượng giấy khi tái bản cuốn sách này
4
93717
2015-09-12 17:56:14
--------------------------
279991
10783
Đây là cuốn thứ 4 của Rachel Gibson mà mình đọc. Quả thực mình thấy chán khi đọc tới cuốn thứ 2, Vướng phải tình em, vì Rachel lặp lại nhiều tình tiết, chẳng hạn nữ chính là nhà văn, mồ côi; nam thường có con hay cháu như thể muốn nói đó là biểu hiện của một người đàn ông có trách nhiệm với gia đình và là đối tượng đáng để kết hôn vậy. 
Ngoài ra hai người sẽ bị hấp dẫn bởi bề ngoài của nhau, một kiểu rất Mỹ. 

Những cuốn của Rachel làm mình liên tưởng tới các truyện tình cảm hai cuốn thời mới lớn mình từng đọc nhằm giải toả những nỗi cô đơn và mong muốn có bạn trai, yêu và được yêu; hiện tại mình đã không còn đọc chúng. Khác nhau ở chỗ truyện của Rachel được bao bọc bởi cái bìa đẹp hơn mà thôi. Chúng có thể dành cho trí thức giải khuây khi rỗi. 

Nhưng Giã biệt tình xa thì khác, và đó là lý do khiến mình viết nhận xét. Tính cách nhân vật nam và nữ mới hơn và thú vị. Lối viết hài hước nhiều lần khiến mình bật cười. Và thị trấn Gospel được miêu tả với những chuyện lạ làm cho người đọc sảng khoái. 

Tóm lại Rachel đã sáng tạo hơn trên cái lối mòn mà cô đã đi với Hãy cứu em, Vướng phải tình em, Không phải tình hờ. 
3
535536
2015-08-27 12:58:47
--------------------------
276952
10783
Nội dung cuốn sách không quá đặc sắc nhưng mình thấy đọc để giải trí cũng ổn. Tuy nhiên, về chất lượng giấy in và trình bày thì quá tệ. Không biết có phải cuốn sách của mình là bản "lỗi đặc biệt" hay không mà cầm cuốn sách không khác gì sách lậu. Giấy chất lượng kém và không biết kiểu gì lại có mùi khó chịu. Bên cạnh đó, khi đọc thì mình cảm thấy đặc biệt khó chịu vì những lỗi chính tả gặp phải. Đọc rất mất hứng.
Đánh giá 3 sao cho Giã Biệt Tình Xa vì nội dung khá hay.
3
94893
2015-08-24 15:35:28
--------------------------
56539
10783
Đây có lẽ là cuốn sách tôi thích nhất của R.Gibson, một tác phẩm hay và để lại trong tôi nhiều ấn tượng, không giống như những cuốn trước đây "Giã Biệt Tình Xa" có cốt truyện diễn ra chậm rãi, không đặt nặng vấn đề về Sex nhưng chủ yếu làm nổi bật diễn biến tâm lý nhân vật. Qua tác phẩm ta có thể thấy rõ Dylan là một người cha tuyệt vời, một cảnh sát trưởng gương mẫu nhung anh vẫn là một con người với trái tim cô đơn cùng với những bí mật chôn giấu đã khiến anh dằn vặt và chưa bao giờ cảm thấy hạnh phúc thật sự. Còn Hope, một nữ phóng viên xinh đẹp đến Gospel để nghỉ ngơi và tìm cảm hứng, cô cũng gặp những vấn đề trong cuộc sống và nó gây cho cô không ít khó khăn nhưng khi gặp Dylan, trái tim cô đã rung động, cô yêu anh nhưng cũng như anh, cô cũng có những bí mật không thể tiết lộ và chính vì điều đó đã khiến cho tình yêu của hai người không thể trọn vẹn, những nghi ngờ, hiểu lầm phát sinh đã làm cho hạnh phúc vừa mới chớm nở của họ tưởng chừng tan vỡ nhưng theo tôi thấy thì Adam con trai của Dylan chính là cầu nối để hai người có thể trở về bên nhau, Adam chỉ là một cậu bé 7 tuổi nhưng cậu sống rất tình cảm và tâm lý, Adam là nhân vật khá quan trọng trong tác phẩm này, cậu mong muốn bố mẹ tái hợp nhưng Adam cũng hiểu rằng bố mình chỉ thật sự hạnh phúc khi được sống với người mà mình yêu, Adam đã chấp nhận Hope, đã để cho cô đến với bố mình và đã chấp nhận cô là một người thân trong gia đình.
Nội dung hay và đầy cảm xúc, không chỉ nói về tình yêu đôi lứa, R.Gibson còn làm nổi bật tình cảm gia đình, tình cha con. Theo tôi đây là cuốn sách đáng đọc.
5
41370
2013-01-22 12:01:25
--------------------------
