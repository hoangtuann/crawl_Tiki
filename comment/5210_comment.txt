471765
5210
Mình hâm mộ nhân vật Shin từ lâu, Shin có nét tính cách tinh nghịch, "ngây thơ đến lạ lùng" :). Mỗi một tập truyện Shin lại là những câu chuyện cực kì đáng yêu, buồn cười không chịu được. Truyện được in màu khoảng 10 trang đầu rất đẹp, các trang sau in trắng đen và cách đọc cũng sẽ là đọc ngược, "đúng chất" Manga - Comic Nhật Bản luôn. Cả nhà mình ai cũng thích đọc truyện Shin, mình cũng đã sưu tầm rất nhiều tập truyện Shin để trong phòng để khi nào thấy chán thì lấy ra đọc.
5
688398
2016-07-08 22:04:38
--------------------------
421269
5210
Những câu chuyện thường nhật về cuộc sống của Shin tiếp tục được tác giả khắc họa qua những mẩu truyện ngắn vui nhộn nhí nhảnh. Sự lém lỉnh và lắt léo của Shin khiến người đọc đôi khi phải thốt lên ôi trời sao mà tinh quái thế. Tuy nét vẽ khiến nhiều người cảm thấy khá xấu nhưng bù lại nội dung mang lại tiếng cười cho người đọc. Có thể bạn cảm được và thấy truyện hay có thể không. Đơn giản tùy vào giá trị quan của mỗi người, câu chuyện sẽ được cảm nhận theo cách khác nhau.
5
1162275
2016-04-24 19:27:08
--------------------------
404853
5210
Những câu chuyện về bé Shin vô cùng hài huớc với bao tình huống dở khóc dở cười trong cuộc sống hằng ngày. Sau một ngày học tập và mệt mỏi được thả hồn cùng những trò đùa vui nhộn của bé Shin và những câu chuyện hằng ngày của gia đình bé khiến tôi cảm thấy thư giản rất nhiều. Đặc biệt với tập 46 này còn có thêm câu chuyện tình yêu lâm ly bi đát của cô giáo Mastuzaka. Rất vui và thú vị. Tiki vẫn như mọi khi giao hàng rất nhanh và bảo quản vô cùng tốt.
3
958480
2016-03-25 20:05:02
--------------------------
378521
5210
Truyện sống động, mang tính hài hước nhiều, tranh ảnh dễ thương, những câu chuyện xoay xung quanh gia đình Nohara, đặc biệt là cậu bé Shinosuke-một cậu bé 5 tuổi vô cùng nghịch ngợm, mang một khuôn mặt đáng yêu, tròn trĩnh-luôn làm mọi trò để quậy phá!
Câu chuyện lần này lại xung quanh Bạch Tuyết cùa Shin-một con chó con màu trắng-, lần đầu tôi gặp Bạch Tuyết, thì nó đang bị bỏ rơi, và tôi đã gặp được chủ của Bạch Tuyết, mọi thắc mắc của tôi đã được giải đáp!(đối với tôi, Bạch Tuyết là một nhân vật mà tôi vô cùng yêu thích)
5
97553
2016-02-05 22:00:15
--------------------------
331349
5210
Truyện hài hước, dễ thương, làm mình cười quá trời luôn. Shin- Cậu bé bút chì có những hành động hết sức kì quặc nhưng hài hước. Thấy em mình nó thích đọc nên mình cũng mượn đọc thử, ai ngờ bị lôi cuốn luôn. Đọc đi đọc lại mãi mà chẳng chán. Truyện vui, hình vẽ đẹp, sinh động nên mình rất thích truyện Shin. Sau mỗi giờ học, có Shin đọc là vui ngay, hết mệt liền, càng đọc càng chẳng muốn hết truyện chút nào. Truyện không chỉ dành cho các học sinh mà còn có thể dành cho các bậc cha mẹ khi họ muốn quay về tuổi thơ hồn nhiên của  mình nữa.
5
713550
2015-11-04 12:45:23
--------------------------
324714
5210
Cuốn truyện Shin - Cậu Bé Bút Chì (Tập 46) này như đưa hay gợi lại những kỉ niệm thời tuổi thơ của chúng ta !! , nghịch ngợm , " siêu quậy "  như thế nào . Cuốn truyện này như bao cuốn truyện Shin cậu bé bút chì khác , Cu Shin vẫn hay quậy phá , tinh nghịch , bày đủ trò với mọi người xung quanh mình . Cuốn này rất hay , đều có những thứ mà mình vừa kể trên !! Cuốn truyện này thật là dễ thương , hay và hấp dẫn !! 
5
603156
2015-10-21 19:47:25
--------------------------
168059
5210
1 câu chuyện tuyệt vời dành cho tuổi thơ. không chỉ là 1 tác phẩm hay dành cho thiếu nhi mà Shin- cậu bé bút chì còn là 1 tác phẩm mang tính công chúng, có thể cho mọi lứa tuổi đọc hàng ngày, để giải trí, thư giãn. những tình tiết hấp dẫn của truyện luôn khiến tôi thấy lôi cuốn, bất ngờ và vô cùng buồn cười !! có lẽ đối với mọi người cũng vậy! truyện của tác giả có tính trẻ ngây thơ cộng với nét vẽ hết sức sáng tạo mà có phần chân thực, hài hoà, làm nên 1 tác phẩm bất hủ không thể bỏ qua. ai đã từng đọc rồi thì có lẽ không thể rời mắt khỏi từng trang truyện ngộ nghĩnh, đáng yêu của Shinnosuke-cậu bé bút chì. ở truyện này, tôi thậm chí không tìm thấy 1 khuyết điểm nào! truyện quá hay và  hấp dẫn, mọi người nên xem qua để có thể trở về 1 thời tuổi thơ hồn nhiên, ngây thơ của mình
5
557441
2015-03-15 21:23:09
--------------------------
