445316
10338
Mình đọc Miko từ rất lâu rồi, từ hồi bé xíu đã đi mướn truyện đọc. Miko cũng như Đô rê mon là một phần tuổi thơ của mình. Truyện rất dễ thương, cô nhóc Miko nhí nhảnh, hay gây chuyện, giống với mọi đứa trẻ tiểu học khác. Truyện Miko xoay quanh cuộc sống thường ngày của Miko cùng bạn bè và gia đình. Bên cạnh đó còn rút ra được nhiều bài học ý nghĩa. Truyện hay, nhưng tập 1 này là tập đầu tiên nên nét vẽ còn chưa đẹp lắm. Nhưng dù sao cũng rất hay và hài hước.
5
400930
2016-06-09 23:39:33
--------------------------
425652
10338
Thật may vì trên tiki còn tập 1 Miko, mừng muốn chết luôn!  Đây là sự mở đầu của Miko cho 26 tập truyện sau này. 
Về tập này vì là tập đầu nên đường nết tác giả vẽ còn hơi cứng,  chưa được sắc sảo,  nhưng mấy tập sau thì đẹp vô cùng.  Chuyện tình giữa tappei và miko thật đáng chú ý a!  Miko vui tươi và hồn nhiên, trong đó cũng có cô bé tên Miho, thích Tappei mà không được để ý nên tức lắm, rồi trong một lần cắm trại,  Miko và Tappei được  bốc thăm chung nhóm với nhau,  Miho tức, đòi đổi thăm với Miko,  Miko nhà ta ngây thơ quá,  đổi liền,  nhưng cho dù thế nào thì người cùng Miko đến đích cũng là Tappei mà thoy, hahaha đáng đời chị Miho,
Truyện hay, các bạn đón đọc trên Tiki.vn
5
1085299
2016-05-05 09:51:43
--------------------------
424993
10338
Mình rất thích đọc truyện tranh Nhật Bản dành cho thiếu nhi, đặc biệt là Miko. Những quyển Miko vừa có những câu chuyện giản dị, hài hước, vừa mang tính giáo dục,... rất phù hợp để xả xì- trét sau những ngày thi căng thẳng cuối năm. Đan xen vào đó là những mẩu truyện hết sức là "tình củm" của Miko và Tappei mà chắc chắn ai cũng đã từng trải qua những cảm xúc hồn nhiên và tươi đẹp giống như thế. Trong những tập đầu, mình thấy nét vẽ của tác giả còn hơi cứng nên hình vẽ không được đẹp như những tập sau. Mấy tập sau hình đẹp khỏi chê.
5
709791
2016-05-03 13:06:20
--------------------------
424726
10338
Mới đọc tập 1 mà đã cười rụng răng :3! Một đứa chuẩn bị lên lớp 12 ngồi đọc cười sằng sặc như hâm, bố nhìn thấy bảo lớn rồi còn đọc truyện tranh T_T! Mà Miko đáng yêu quá chừng ^^. Ngộ ngộ, ngu ngu =))) nhưng rất tốt bụng và dễ thương ^^. Được lòng mọi người. Mới đọc thôi mình đã rất thích. Thích yuko vs kenta ^^, kenta dễ thương :v. Thích tappe vs miko, cặp này thì như chó vs meò=)). Mà đôi này buồn cười, hai anh chụy đều thích nhau mà bày đặt giả vờ :3. À thích cả truyện bên lề của tác giả, nhất là tạo hình tác giả với con gái, cười không chịu được=)))
4
648608
2016-05-02 16:57:27
--------------------------
418360
10338
Đợt cắm trại, thấy lũ bạn ngồi trong lớp chuyền tay nhau đọc mấy quyển này ( tập 1, tập 2) rồi thì tò mò muốn đọc thử nên mượn. Mình khá khó tính trong việc chọn sách vì sợ đọc vào sẽ khó hiểu. Nhưng đọc xong mới thấy dễ thương vô cùng. Bao nhiêu câu chuyện vụn vặt quanh thế giới của một cô bé lớp 5 và những người bạn cũng trở nên rất đáng yêu. Mạch truyện cũng rất logic, thích hợp cho trẻ con. Vậy là sau đó, mình giới thiệu cuốn này cho các bạn khác cùng đọc và còn đọc trên mạng nữa; từ đó mới biết chuyện còn rất cảm động.
5
975584
2016-04-19 08:11:41
--------------------------
386572
10338
Tôi đã quá cái tuổi đọc những câu chuyện về nhóc Miko. Nhưng giữa những bộn bề của cuộc sống hiện nay, cũng nên cần những tác phẩm như thế này để có thể cân bằng được đầu óc. Tôi đã cười rất nhiều bởi cô nhóc Miko đầy đáng yêu và không kém phần tinh nghịch. Và tôi lại được một lần nữa trở về với tuổi thơ đầy tươi mát của mình. Mỗi câu chuyện của Miko không đơn giản chỉ là hài hước, nó còn là những bài học sâu sắc cho cuộc sống của ta. Nhóc Miko khiến tôi thêm yêu quý những người xung quanh mình...
5
211114
2016-02-25 21:29:05
--------------------------
383466
10338
Đúng! Tiêu đề của mình là như vậy! Sao lại dễ thương dữ vậy trời? Tóc ngắn, lùn lùn, ăn nói dễ thương là một nét không thể nào nhầm lẫn của Miko-chan. Mình thích nhất tập của Cô Gái Tuyết đó! Cô ấy đẹp mê hồn luôn, ai dè Tappei còn bị cô ta "lôi kéo" đến không cảm xúc. Khúc cuối thật là cảm động, nhờ súp của bà mà Tappei dần hồi tỉnh, à quên...tại sao mình lại không nhắc đến công lao và tình bạn của Miko và Mari nhỉ? Hai cô nàng đã lặn lội từ ngôi làng nhỏ lên núi tuyết đầy chông gai. Tình bạn quả là đẹp các bạn ha?
5
1034042
2016-02-20 20:27:08
--------------------------
358830
10338
Để mua được quyển 1 mình đã phải đợi tiki rất là lâu. (Hình như hiện tại hết hàng nữa rồi :3). Cuối cùng mình cũng có đủ bộ Miko rồi, một cảm giác thật vui vẻ :3
Nói về Miko thì miễn bàn chất lượng đi, vì không chê vào đâu được, hơn nữa quyển này của mình còn được tiki bọc lại nữa, nhìn rất tuyệt :3 cơ mà do mới tập 1 nên nét vẽ chưa mượt lắm.
Về nội dung thì vẫn như mấy quyển khác, đều là những câu chuyện rất giản dị, đời thường đôi khi có yếu tố....hư cấu (xuyên không đồ :v). Lồng vào đó là những tình cảm, tình bạn, tình thân, tình yêu học trò..trong sáng vô tư...hay những bài học quý giá mà Eriko-san muốn gửi gắm. Đọc Miko cứ cảm giác nhẹ nhàng, thoải mái. Miko mãi là tuổi thơ của mình.
Cám ơn tiki vì nhờ có tiki mà mình đã gom được đủ bộ Miko :3
5
967569
2015-12-25 22:34:06
--------------------------
319082
10338
Một câu chuyện nhẹ nhàng về cuộc sống của một cô bé vô cùng bình thường, không có gì đặc biệt nhưng có tấm lòng vô cùng trong sáng và nhân hậu. Tôi đã mua tất cả các tập truyện miko, tôi cực kì thích truyện này luôn, vô cùng đáng yêu và siêu hài hước, phù hợp với mọi lứa tuổi để giải trí. Giá thành cũng không quá đắt. Nội dung truyện không li kì, phiêu lưu nhưng không hề nhàm chán. Và trong đó, tác giả cũng lồng ghép các câu chuyện kinh dị nhưng không hề kinh dị :')
5
78298
2015-10-07 20:39:56
--------------------------
314905
10338
Đây là truyện thiếu nhi đầu tiên sau nhiều năm mình mới đọc. Nét vẽ đơn giản, nội dung cũng đơn giản nhưng lại hài hước, dí dỏm. Mỗi một tập truyện dù hơi ngắn nhưng vẫn mang nhiều ý nghĩa khác nhau. Mình thích nhất là chuyện tình trẻ con của Miko và Teppei. Có nhiều nhân vật khác cũng rất thú vị. Truyện rất gần gũi và phù hợp cho mọi đọc giả. Lúc rảnh rỗi đọc giải trí rất tốt, mình sẽ mua đủ các tập kế tiếp luôn!!!! Viết thêm để cho đủ 100 từ nè!
5
365221
2015-09-27 16:18:33
--------------------------
307986
10338
Mình mua cho con gái truyện Miko vào dịp bé nghỉ hè. Khi đọc xong truyện này, con gái rất thích và đòi mẹ mua thêm. Mình đã mua thêm 10 quyển cho con gái.Cháu rất thích đọc, vì chuyện kể những chuyện đời thường,rất dễ thương và gần gũi với học sinh Tiểu học. Không chỉ nội dung truyện rất dễ thương, cuốn hút mà hình thức sách rất đẹp, Nét vẽ dễ thương ,sinh động, ngộ nghĩnh. Theo mình đây là bộ truyện tranh rất thích hợp cho thiếu nhi. Bạn sẽ không thấy phí tiền bạc khi mua tặng con bộ truyện này.



5
512701
2015-09-18 13:48:36
--------------------------
298844
10338
Nhờ trang bìa minh họa dễ thương trên phông vàng đầy nổi bật, một cô bé chân ngắn đã xuất hiện trong cuộc đời mình một cách tình cờ như thế... Miiko đã mang đến vô cùng nhiều những niềm vui, những giây phút giải trí tuyệt vời, đan xen vào đó là những bài học nhẹ nhàng nhưng thấm thía, từ tình bạn, tình cảm gia đình, cho đến tình cảm học trò trong sáng... Nét vẽ trong tập 1 có thể chưa đẹp và mượt như những tập sau, nhưng những câu chuyện hài hước tuyệt đối không làm mình thất vọng! Sẽ gắn bó với Miiko dài dài, cảm ơn tác giả đã tạo ra một người bạn dù không có thật nhưng vô cùng đáng yêu và dễ thương này!
5
364412
2015-09-12 21:46:29
--------------------------
292792
10338
Hình vẽ Nhóc Miko  khá dễ thương, tinh nghịch, và đặc biệt là nhân vật chính thì " ngắn cũn". Truyện xoay quanh Nhóc Miko khi ở trường với những người bạn như Mari hay Tappei...ở nhà với ba mẹ và cậu em Mamoru những câu chuyện hết sức bình thường nhưng điều làm nên sự thú vị đó là sự hồn nhiên, vui nhộn, ngộ nghĩnh của những nhân vật trong truyện và thấy mình hồi nhỏ qua những mẩu truyện đó. Một cuốn sách giải trí khá tuyệt và nên đọc dành cho cả phụ huynh. Mình sẽ mua cho đủ bộ để đọc.
5
165748
2015-09-07 17:12:00
--------------------------
288635
10338
Trong tập nhóc miko - cô bé nhí nhảnh tập một lần này thì mình thích nhất tập truyện ngắn " Tỉnh tỏ là ... tỏ tình " tập ngắn này nói về việc mari thích một anh chàng kia , nói chung là rất vui , mari nhờ miko giúp nhưng miko lại không giúp được gì vì miko rất là ngốc . Tập này có cũng hài hước , vui nhộn và cũng đầy ý nghĩa như là mấy tập trước . Miko chính là một trong những cuốn truyện mà mình thích nhất . Miko là một hình thức giải trí rất tốt , đem lại tiếng cười , mỗi khi chán là mình lại đọc miko và mình lại cười bể cả bụng :) hihi 
3
635786
2015-09-03 19:00:04
--------------------------
284277
10338
Mình mua sách cũng khá lâu rồi mà đến giờ mới nhớ để đến để viết nhận xét. Lần đầu tiên mình đọc Nhóc Miko chắc cách đây cũng 5 - 6 năm rồi, cái hồi mà chưa tái bản và chưa đẹp như bây giờ ấy. Nghiền đến nỗi mỗi ngày đều phải ra tiệm thuê một lần 7 - 8 cuốn về đọc cho đã, Miko cũng giống như tuổi thơ của mình vậy. Bẵng đi đến giờ mình mới có dịp đọc lại, cảm xúc vẫn giống trước đây, vẫn thích chết đi được các nhân vật trong truyện. Nét vẽ của tác giả rất đáng yêu, nội dung truyện lại chân thật và vô cùng dễ thương, mang lại cho mình cảm giác thư giãn sau một ngày dài căng thẳng. Truyện tuy có nhiều chi tiết hư cấu, nhưng lại không làm giảm tính thực tế, đôi khi lại còn có một bài học nào đó sau từng câu chuyện nhỏ nhặt của Miko. Mình chỉ có thể nói là mình thích em nó cực luôn TvT
5
419484
2015-08-30 20:22:45
--------------------------
253696
10338
Lần đầu tiên biết đến Nhóc Miko là khi mình đang tình cờ tìm sách. Nhìn bìa tập 1 này hết sức dễ thương là mình đã muốn mua ngay rồi. Đọc rồi mới thấy cái thú vị của Miko. Những câu chuyện về tình bạn, gia đình, trường học, xen vào một chút tình cảm của tuổi mới lớn,... Tất cả đã cho mình như được sống lại cái tuổi hồn nhiên thơ ngây vô tư ngày nào. Thực sự Nhóc Miko không chỉ là câu chuyện dành cho các bé thiếu nhi mà những người lớn như mình cũng nên đọc.
5
178929
2015-08-04 17:23:46
--------------------------
195744
10338
Thông thường những quyển truyện tập đầu có nét vẽ thô sơ chưa hoàn chỉnh lắm nhưng nội dung truyện rất bao cười, dí dỏm luôn khiến bạn đọc vote và hóng các tập tiếp nối. Nét truyện đơn giản gần gũi, thân thuộc, ngọt ngào đến đáng yêu là yếu tố luôn mang đến tình cảm sâu sắc với độc giả, và "Nhóc Miko" đã làm được điều đó. Cảm ơn tác giả Ono đã sáng tác ra 1 thước truyện mới mẻ, không nhàm chán và thú vị vô cùng đã chinh phục được các con mọt truyện thời nay. Và Ono ngày càng sáng tạo hơn khi các tập truyện về sau nét vẽ đã hoàn chỉnh hơn, mức giá thì khá ổn định với 1 quyển truyện hay vô cùng như vậy. Những góc hướng dẫn, tâm sự, ngoại truyện là những phần đặc biệt tô điểm sinh động và độc quyền hơn cho tác phẩm.
5
534452
2015-05-14 16:12:34
--------------------------
187440
10338
Miko luôn là cô bé nhí nhảnh, hoạt bát biết giúp đỡ quan tâm đến người khác. Đó luôn là ấn tượng của tôi về cô bé. dù cho có đọc đi đọc lại đến ngàn lần vẫn không ngán. Và tôi đã cất công đi sưu tầm từng cuốn miko đến đầy đủ cả bộ và thấy quả thật không đáng tiếc hay hối hận chút nào. Đây đúng là lựa chọn đúng đắn. Tôi yêu từng mẩu câu truyện mà tác giả Ono đã viết lên, thật trong sáng và đáng yêu nhường nào. Tôi sẽ về giới thiệu cho bạn bè về đọc thử và tin chắc ai đọc vào cũng sẽ cảm thấy mê như tôi
5
523370
2015-04-23 10:54:24
--------------------------
183910
10338
Lần đầu tiên đọc truyện Miko  là 2 năm vể trước khi đó tôi thấy nhỏ em đang đọc và cười nức nẻ làm kích thích tính tò mò của tôi, nên tôi lấy đọc thử và thật bất ngờ tuy là truyện tranh thiếu nhi nhưng nó có một sức hút khó tả bởi hình ảnh trong truyện miko rất vui nhộn, bởi những câu chuyện nhí nhố tuổi học trò nhưng không kém phần sâu sắc, với mỗi nhân vật là những tính cách khác nhau như miko nhí nhảnh hồn nhiên, cô bé mari thì luôn mong muốn mình một họa sỹ truyện tranh và yuko thì đằm thắm, đáng yêu ….. và còn nhiều điều thú vị nữa, truyện nhóc miko thật sự là một món ăn tinh thần tuyệt vời cho mọi người
4
602070
2015-04-16 20:47:40
--------------------------
182111
10338
Nhớ hồi xưa còn học cấp 3 tôi chờ từng ngày 1 để đi thuê từng tập của Miko, vì quen với chị cho thuê truyện nên lúc nào tôi cũng  được đọc trước nhất khi Miko về đến tiệm :P Tôi là con mọt truyện từ nhỏ, thể loại gì cũng đọc, thiếu nhi thì có Miko, cún con Momo, Asari tinh nghịch, nhóc Marưkô,...
Giờ 22 tuổi rồi, hôm qua tự dưng nghe con bé em nhắc truyện nhóc Miko thấy nhớ quá nên mò lên Tiki xem thử và hối hận ngay vì .... thích quá. Giờ lớn rồi, đi làm có tiền nên quyết tâm sẽ rinh trọn bộ Miko về đọc lại (dù đã đọc 2 lần).
Bạn trai tôi bảo thích thì lên mạng mà đọc, đỡ tốn tiền mua về để chật nhà, đúng là con trai mọi người nhỉ? Đọc trên mạng làm sao thích bằng cảm giác được cầm quyển Miko trên tay mà đọc từng trang 1 rồi cười ngả cười nghiêng.
5
573671
2015-04-13 11:44:27
--------------------------
178581
10338
Đọc Miko từ cấp 1, đến bây giờ đã cuối cấp 3 đọc lại vẫn còn y nguyên cả, xúc lúc ban đầu, thú vị, hấp dẫn và rất tinh tế :D
Bộ truyện xoay quanh Miko, gia đình gồm ba, mẹ, em trai, em gái và những người bạn của Miko.
Cốt truyện đơn giản, dễ nắm bắt nhưng lại rất thu hút bạn đọc ở những phản ứng của Miko với những chuyện đời thường, chuyện gia đình, bạn bè và cả tình cảm nữa :))
Tappei và Miko là một cặp đôi rất dễ thương, xuyên suốt bộ truyện mình luôn ủng hộ cặp đôi này :))
Báo xấu
4
601649
2015-04-05 10:44:06
--------------------------
177951
10338
Đọc Miko từ cấp 1, đến bây giờ đã cuối cấp 3 đọc lại vẫn còn y nguyên cả, xúc lúc ban đầu, thú vị, hấp dẫn và rất tinh tế :D
Bộ truyện xoay quanh Miko, gia đình gồm ba, mẹ, em trai, em gái và những người bạn của Miko.
Cốt truyện đơn giản, dễ nắm bắt nhưng lại rất thu hút bạn đọc ở những phản ứng của Miko với những chuyện đời thường, chuyện gia đình, bạn bè và cả tình cảm nữa :))
Tappei và Miko là một cặp đôi rất dễ thương, xuyên suốt bộ truyện mình luôn ủng hộ cặp đôi này :))
5
323135
2015-04-03 22:31:59
--------------------------
174141
10338
Mình đọc hồi mình còn học lớp 5 năm và thích nó từ lần đầu tiên đọc. Mình bị Miko hút hồn bởi Miko - Một cô bé rất dễ thương, đáng yêu và có một tấm lòng tốt bụng. Nhưng người bạn của Miko như Tappei, Kenta, Yuko, Mari,... cũng vậy, ai cũng rất dễ thương. Mình cảm thấy ganh tỵ với Miko và có một đứa em rất dễ thương lại còn đảm đang, người mẹ khó tính nhưng rất yêu thương con cái và cả người cha hiền lành tốt bụng nữa. Mình rất thích đọc truyện Miko
5
583850
2015-03-27 14:38:52
--------------------------
137926
10338
Các bạn tin không mình chờ nó xuất bản lại đến mấy năm rồi đó.Mình đọc được năm lớp 7 và lập tức thích nó bởi nét trong sáng dễ thương và những bài học nho nhỏ mang lại ở mỗi câu chuyện.Lúc đó bạn mình bạn là nó không còn nữa nên mình không còn đọc nữa.Cho tới bây giờ khi thấy nó mình order liền cả bộ đọc ngấu nghiến vì nó quá dễ thương hi hi 
Cốt truyện trong sáng giữa tình bạn tình yêu tuổi học trò và các bài học được rút ra sau mỗi câu chuyện làm chuyện cháy hàng là thế đấy 
Món quà ý nghĩa của trẻ con
5
406836
2014-11-29 19:39:03
--------------------------
112222
10338
Mặc dù nhiều người vẫn nói rằng tôi trẻ con suốt ngày chỉ biết đọc những bộ truyện dành cho con nít nhưng con nít thì sao, mặc kệ. Mỗi lần đọc Miko tôi lại cảm thấy rất vui. Lần đầu tiên đọc truyện chẳng biết sao không thể nào quên được. Miko là một cô bé hồn nhiên, vô tư, đôi khi hồn nhiên đến mức không cảm nhận được tình cảm của người khác nhưng Miko thực sự rất rất đáng yêu. Tôi cảm thấy có chút ghen tị với Miko vì có một người mẹ tuy hơi khó tính nhưng tâm lí, người cha thì dịu dàng, Mamoru thì rất đảm đang và những người bạn như Tappei, Yuko, Mari, Kenta,....... Tôi đã từng ước rằng mình có thể là mộn nhân vật trong truyện thì hay biết mấy ngày nào cũng có thể vui đùa,trò chuyện. Tôi muốn sau này lớn lên có thể như Ono Ẻiko, có thể đem niềm vui cho mọi người( mặc dù công việc rấc cực nhọc ^.^). Mong sao cho Miko luôn hồn nhiên, trong sáng như vậy, mong sao cho bộ truyện Miko- cô bé nhí nhảng đừng bao giờ kết thúc
4
344731
2014-05-10 18:20:27
--------------------------
101297
10338
Tình cờ hôm nay lôi Miiko 1 ra đọc,cảm giác vẫn thú vị như khi đọc lần đầu vậy. Trong tập này,có một truyện mang tên "Nứơc mắt của Mari". Tuy mình mới đọc truyện này trong quyển Selection mới ra,nhưng khi đọc lại,vẫn phải gật gù tự hỏi sao mà Miiko giống mình gê gớm,sao tác giả Ono lại có thể xây dựng tình huống và lời thoại tự nhiên đến vậy. Từ chuyện bị nói là theo đuôi bạn,đến chuyện không hợp tính cách mà tẩy chay,..đều đựơc Ono xây dựng vô cùng tự nhiên và hợp tâm lý. Đúng là giận hờn con nít mà! Tiếp đó ,mình lại đựơc phen cảm động trong câu chuyện "Áo ấm kỉ niệm",một phen ghen tị với Miiko bởi có ngừơi mẹ cá tính và tâm lý như Rie. Mong là sẽ đựơc đón đọc nhiều tác phẩm của Ono hơn nữa!
0
78605
2013-12-03 23:27:29
--------------------------
90959
10338
Truyện nhóc Miko đúng chất với thể loại truyện tranh dành cho thiếu nhi,truyện có nét vẽ rất đáng yêu với cái miệng to khi cười to gần bằng mặt,dáng vẻ lùn,tròn của Miko-nhân vật chính, rất đặc biệt và dễ thương.Còn về cốt truyện thì khỏi chê, mỗi một mẩu chuyện là một tràn cười sảng khoái vì những trò nhí nhố của Miko và những người bạn của cô bé và ẩn sau đó là những bài học về bạn bè,gia đình,...tuy đó là những điều nhỏ nhặt thôi nhưng đáng để chúng ta học hỏi và trút kinh nghiệm.Tác giả thật hay khi dạy chúng ta nhiều điều qua nhóc Miko mà không hề thuyết giáo,lên lớp, qua mỗi câu chuyện chúng ta sẽ tự nhận ra.Khi đọc nhóc Miko mình cảm thấy rất thoải mái,giải street và đọc đi đọc lại mà không chán.Nhóc Miko thật sự rất đáng để đọc (:
5
75632
2013-08-14 18:29:57
--------------------------
80508
10338
Lần đầu tiên đọc Miko là mình đã thấy kết ngay rồi, phải tìm bằng được mấy tập còn lại để đọc. Vì sao mà nó lại gây thiện cảm với bạn đọc như thế thì phải đọc mới biết, cuộc sống hằng ngày của Miko rất gần gũi, bạn có thể thấy chính mình trong đó, xoay quanh là các câu chuyện về gia đình, bạn bè vui nhộn nhưng cũng cực kì cảm động và ý nghĩa. Riêng mình thấy đây là cuốn truyện rất thích hợp đọc để giải khuây những lúc buồn chán, đọc Miko rồi bạn sẽ cảm thấy cuộc sống của trẻ thơ thật đầy màu sắc và không có những lo toan về cuộc sống, giống như cô bé Miko nhí nhảnh dễ thương này vậy :)
5
113987
2013-06-13 08:13:27
--------------------------
78980
10338
Đây là một cuốn truyện hay,rất đáng để đọc. Mỗi câu chuyện là một bài học, mang đậm tính giáo dục nhưng vẫn rất hài hước. Tranh vẽ của Ono Eriko sinh động,đáng yêu. Mình thích nhất là truyện miko đươc đi du lịch nhưng vẫn phải làm bài tập vì mình có thể tìm thấy chính mình ở câu chuyện ấy.(Mình đã từng bị một lần như vậy ^^) Mỗi khi buồn, mình thường lấy miko ra đọc, những câu chuyện xoay quanh miko làm mình cảm thấy vui hơn. Mình yêu miko lắm. Bạn cũng thử đọc xem sao!
5
37232
2013-06-04 17:59:38
--------------------------
72219
10338
.Một câu chuyện"kinh dị" nhẹ nhàng,xem chẳng thấy sợ chút nào, trái lại còn làm bộ truyện dễ thương hơn nữa . Thế nhưng , chính những điều bình thường ,cùng với chút chút "kinh dị" ấy đã làm cho Nhóc Miko càng trở nên thú vị hơn đối với mình . Thiệt vậy đó "Nhóc Miko" dành cho tất cả những ai muốn sống mãi trong lứa tuổi ngây thơ, trong sáng, ngọt ngào khó quên ấy . Hẳn các bạn cũng không khó nhận ra bộ truyện tranh giải trí lành mạnh, mang tính giáo dục cao "Nhóc Miko"đã và sẽ mãi là món quà không thể thiếu dành cho những lứa tuổi tụi mình bây giờ.
5
93696
2013-05-01 08:15:18
--------------------------
71365
10338
Dù đã 17 tuổi rồi nhưng Miko vẫn là bộ truyện mình yêu thích. Qua câu chuyện về Miko và những người bạn xung quanh, mình có thể nhận ra nhiều điều về gia đình, bạn bè, đặc biệt là cốt truyện rất yêu nhộn và đáng yêu nữa. Bên cạnh đó cũng có những câu chuyện rất cảm động, mình đặc biệt thích phần "bé gái ở Hiroshima" , mình đã khóc... vì mình có thể cảm nhận được nỗi đau của người Nhật Bản khi nhớ về thảm kịch này...
Miko quả là một câu chuyện dễ đọc dễ cảm, rất thích hợp để chúng ta đọc những lúc buồn chán, nó đem lại cho chúng ta tiếng cười và cả những bài học đáng quý về cuộc sống nữa... Yêu Miko lắm lắm :)
5
113987
2013-04-26 18:11:52
--------------------------
67786
10338
1. Đây có lẽ là bộ truyện tranh thiếu nhi mà tôi thích nhất. Chỉ là những mẩu chuyện nhỏ về cuộc sống xung quanh một cô nhóc tiểu học, hậu đậu, ham chơi nhưng dễ thương, tinh nghịch, năng động, lại có thể khiến cho người đọc cảm thấy ấm áp, vui vẻ. Đọc truyện ta còn có thể cảm nhận được tình cảm gia đình, bạn bè, thầy trò và cả tình thương giữa con người với con người. Đây không còn là cuốn truyện chỉ dành cho lứa tuổi thiếu nhi nữa, mà còn là bộ truyện được nhiều người trưởng thành yêu thích và tìm đọc. Đọc để giải trí và nhiều khi cũng là để suy ngẫm, vì những hành động, suy nghĩ trong sáng, dễ thương của lứa tuổi học trò mà khi trưởng thành người ta đã vô tình lãng quên.

5
104539
2013-04-08 21:18:52
--------------------------
65803
10338
Nhóc Miko- 1 bộ truyện rất hay và ấn tượng! Khi đọc nó, bạn có thể vui, buồn với những kỉ niệm khi Miko lười biếng bắt nạt nhóc Mamoru, Miko giận dỗi với Mary hay thậm chí là vò đầu bứt tai Tappei khi bị trêu là lùn tịt. Nhưng trên hết, qua bộ truyện này, ta cảm nhận được cuộc sống trong sáng, vui vẻ của 1 cô nhóc lười biếng, ham ăn, ham chơi nhưng cũng hết sức hồn nhiên và chân thành. Chắc chúng ta không quên những lần Miko viết thư tay cho ba để khuyên ba giữ gìn sức khỏe, nhờ mẹ sửa lại cái áo rất đẹp mà Miko rất thích để tặng lại cho Mary. Và chắc bạn cũng chẳng thể quên cái lần Miko được bầu làm cán sự lớp đã thức thâu đêm để làm cờ cho lớp dự hội thao, tuy được đi du lich nhưng cũng đã ở lại khách sạn để làm hết bài tập Tết…..Đây quả là 1 bộ truyện hấp dẫn khiến bạn không thể rời mắt khi chưa đọc hết 21 tập truyện. Thân!
5
103772
2013-03-27 19:07:31
--------------------------
41321
10338
Hihi hùi đó học TA ra chơi hay xuống gần trường mấy cô chú bán truyện, lựa 1 hồi mua vài cuốn nhóc miko về đọc, ấn tượng lắm luôn á, đọc mà toàn cười thôi. Qua mấy ngày sau hỏi mua tập tiếp theo cô chú bảo là ko có, đi các nhà sách và những nơi bản truyện cũ ngta nói ko có truyện này, năm 2007 chứ mấy, ghiền nhóc Miko lắm luôn, mà ko hĩu lí do ts ko thấy bán nữa.tiếc lắm, bây giờ thì tái bản lại rồi, mừng lắm á :x. cám ơn Tiki, cám ơn Miko, vì mỗi lần đọc nhóc Miko là quên hết buồn rầu, và đôi lúc thấy mình y hệt nhóc Miko =)) tí ta tí tởn....
5
28992
2012-09-28 23:20:39
--------------------------
41060
10338
Qua những câu chuyện vui nhộn về cô bé Miko cùng gia đình, bạn bè...chúng ta có được những tràng cười sảng khoái nhưng cũng thật thấm thía, sâu sắc...Nhóc Miko giúp ta gợi nhớ về tuổi thơ của mình: ngây thơ, vô tư, phá phách nhưng đôi khi cũng tỏ ra "người lớn", biết trân trọng, giúp đỡ mọi người. 
Tôi đã đọc đi, đọc lại không biết chán những mẩu chuyện này dù   đã qua lứa tuổi học trò...nhất là khi gặp chuyện buồn hay áp lực thì tìm đến nhóc Miko tôi lại thấy lòng nhẹ nhõm, bình yên. Qua mỗi trang truyện, tôi lại được một lần chiêm nghiệm về cuộc sống, thêm yêu quý những người thân thiết xung quanh. Để hoài niệm về tuổi thơ, để trân trọng hiện tại, để bước đến tương lai đầy hân hoan mong chờ... cảm ơn nhóc MIko!
5
18307
2012-09-26 21:37:13
--------------------------
39460
10338
Truyện tranh Miko tôi đọc đã lâu rồi mà bây giờ vẫn còn ghiền, muốn sưu tập cả bộ luôn đây!
Tôi thích nhân vật Miko, trong sáng, hồn nhiên, nghịch ngợm nhưng hiểu chuyện hơn bao giờ hết - một giá trị nhân văn đáng được ghi nhận. Thậm chí có những điều rất nhỏ, xảy ra trong cuộc sống đời thường, với những kiến thức tưởng chừng rất phổ thông nhưng không phải ai cũng biết, cũng rất phù hợp cho trẻ nhỏ và cả người lớn phải tìm hiểu về nó.

5
55029
2012-09-12 14:56:28
--------------------------
34982
10338
Truyện tranh Miko mình đọc đã lâu rồi , hồi nhỏ cứ một lần ba mẹ đi vắng thì toàn lẻn qua nhà bạn chơi rồi cứ ôm mãi cuốn truyện Miko , đọc hoài mà không chán . Và rồi cứ ngỡ rằng bên mình luôn có một cô bạn trong sáng , nghịch ngợm , đáng yêu tên là Miko . Có thể nói , truyện đã mang lại một góc nhìn rất đáng yêu về tuổi thơ , về tình bạn và về gia đình . Bây giờ đã thành một thiếu nữ cấp 3 rồi nhưng mà vẫn mê mẩn truyện khi hồi còn nhỏ vậy , bởi mỗi lẫn đọc Miko thì như tìm lại được một phần của tuổi thơ vậy.
5
48993
2012-07-27 13:00:05
--------------------------
34822
10338
Đọc truyện này từ lúc học THPT mà bây giờ vẫn còn ghiền,  muốn sưu tập cả bộ luôn đây! Nét vẽ của truyện thật dễ thương,càng về sau càng hay. Những tình tiết của truyện lúc nào cũng sống động, đôi lúc đang đọc mà tưởng mình là Miko, cùng vui, cùng buồn với Miko! Những lúc buồn mà lấy Miko ra đọc là cười ra nước mắt luôn, bao nhiêu buồn bã kéo nhau đi chơi hết! đọc đến mấy khúc Tappel chọc Miko mà cười đau ruột, công nhận tác giả One Eriko tài và hài thật nên mới có thể làm ra bộ truyện này! Cảm ơn tác giả rất nhiều và mong tác giả tiếp tục sáng tác ra những bộ truyện thế này nữa nhé!!
5
45875
2012-07-25 23:13:58
--------------------------
29353
10338
Mình đọc nhóc Miko hồi mới vào đại học, đến giờ vẫn cực kỳ ấn tượng với cô bé này. Nếu có ai hỏi mình thích Doraemon hay nhóc Miko hơn, kể cũng khó trả lời (2 cuốn đều là truyện thiếu nhi, nhưng thể loại hơi khác nhau), mình sẽ chọn nhóc Miko. Vì đọc Doraemon cũng hay, nhưng thỉnh thoảng không thích lắm chuyện Nobita cứ ỷ lại vào bửu bối của Doraemon. Còn nhóc Miko lại có nhiều câu chuyện vui vui xung quanh cô bé này, vừa là những bài học về tình cảm gia đình, chị em, bạn bè. Đôi lúc mình tìm thấy cả chính mình ngày xưa trong nhóc Miko, nên cảm giác rất là gần gũi. Điểm chưa ưng ý lắm, là có 1 vài câu chuyện hơi thiếu tính thực tế, như phần phụ truyện về 2 cô bé và con búp bê chẳng hạn, nhưng nhìn chung đây là 1 bộ rất dễ thương.
5
26136
2012-06-05 14:31:00
--------------------------
28162
10338
Tôi đã 22 tuổi đầu rồi, nhưng cứ không nhịn được để tìm đọc bộ truyện hấp dẫn và đầy bổ ích này.

Có thể bạn sẽ cười tôi là lớn già đầu mà còn thích đọc mấy thể loại này. Tôi lại nghĩ bạn nên đọc thử đi, rồi sẽ biết tại sao tôi say mê nó. Với những tìm tòi và đầu tư rất công phu cho từng nội dung và từng nét bút vẽ, Ono Eriko đã thực sự khắc hoạ được một kiệt tác để đời của bà. Tôi thích cách bà xây dựng nhân vật Miko, trong sáng, hồn nhiên, nghịch ngợm nhưng hiểu chuyện hơn bao giờ hết - một giá trị nhân văn đáng được ghi nhận. Thậm chí có những điều rất nhỏ, xảy ra trong cuộc sống đời thường, với những kiến thức tưởng chừng rất phổ thông nhưng không phải ai cũng biết, cũng rất phù hợp cho trẻ nhỏ và cả người lớn phải tìm hiểu về nó.

Đọc Miko, tôi lại thấy cuộc sống này tươi sáng và đáng mong đợi biết chừng nào ;)
5
37779
2012-05-24 22:13:34
--------------------------
