287978
10875
Bộ sách được đựng trong túi rất dễ thương. Nội dung cuốn sách phù hợp với trẻ nhỏ, hình vẽ sinh động, màu sắc rất bắt măt. Đặc biết với mức giá rất hợp lý khi mua tại tiki.vn. Mình thấy đây là cuốn sách đầu tiên giành cho trẻ nhỏ có kết hợp trò chơi, câu hỏi sau mỗi chuyện. Cho con làm quen sách với những cuốn sách như thế này tạo tiền đề cho con ham đọc sách sau này. Con gái mình gần 2 tuổi, cháu rất thích những quyển sách này. Đang chờ mua bộ 4 quyển sách tiếp theo của Vườn ươm trí tuệ. 
4
419952
2015-09-03 10:17:47
--------------------------
5191
10875
Bộ sách khá đơn giản, hình ảnh rất đẹp, những câu chuyện ngắn gọn, lý thú giúp trẻ tập những thói quen tốt, phát triển kiến thức cơ bản của trẻ,...Giữa các câu chuyện có đan xen vài trò chơi nho nhỏ giúp trẻ không bị chán. 
Thích hợp cho trẻ 2-3 tuổi mới làm quen với sách.
4
3859
2011-05-28 16:51:34
--------------------------
