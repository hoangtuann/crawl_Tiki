530017
9853
Mình đọc khá nhiều sách của tác giả trong nước lấn ngoài nước, thật sự rất ấn tượng với Doãn Dũng , lần đầu cầm cuốn sách này lên chỉ vì tò mò sau đó đã bị cuốn hút bởi những trang đầu tiên, cách hành văn mới lạ, ngòi bút lạnh lùng tựa như Nam Cao nhưng ẩn sâu là một nỗi niềm suy tư thương cảm cho số phận của những nhân vật trong truyện. Có lẽ Doãn Dũng đã rất thành công với cuốn sách này, mỗi câu chuyện gây cho người đọc một nỗi ám ảnh khác nhau, nếu như Tuấn điên với chiếc rọ mõm thì Ca mang mùa bão đến và người đàn bà với cái nick sáng trên yahoo... tất cả đã mang cho độc giả được trải nghiệm như chính nhân vật trong truyện, buộc ta sau khi gấp cuốn sách lại phải thẫn thờ suy ngẫm nhiều giờ. 
5
892139
2017-02-22 19:11:49
--------------------------
368446
9853
Bóng anh hùng của tác giả có sự u mê về miền tối tăm của cá nhân , cảm nhận thấy sự vô tư , yêu đời hòa chung với không khí đời thường , anh tập trung lại nhiều câu truyện vốn gần với nhiều người như ta , trải nghiệm đời nhanh nhạy , minh mẫn , tỉnh táo phân tích các góc độ của mỗi người không mấy quen biết đều khắc ghi nỗi niềm còn thiếu sót , có điều vẫn là sự vui tính , bắt mắt ở cách trình bày , nội dung sáng sủa .
4
402468
2016-01-13 23:05:07
--------------------------
350319
9853
Thật sự đây là một quyển sách rất ấn tượng với mình về nội dung. Lần đầu tiên mình đọc một quyển sách mà ít được nhắc tới, một tác giả không nổi tiếng ( có lẽ mình cảm thấy thế vì đây là lần đầu tiên mình thấy tên tác giả lạ) và quyển sách này cũng không nổi tiếng vì chắc không được pr nhiều.Đọc qua, mình thấy tác giả có một phong cách viết văn rất lạ, rất sáng tạo, không giống một ai. Có một chút gì đó hơi bựa bựa, hơi quá người lớn và chắc chắn rằng sách này không phù hợp với một đứa con nít mười mấy tuổi như mình. Nhưng thật thì sách rất hay, nhân vật "tôi" có cái khí thế, phong thái rất rành đời, lãng tử và sống bất cần đời, vô tư. Mình không dám đọc hết quyển sách mà chừa lại tới chục năm nữa rồi đọc tiếp vì có lẽ cái tuổi của mình chưa hiểu đươc hết tác phẩm này.
3
521504
2015-12-10 12:28:41
--------------------------
288130
9853
Sau khi mua chuyện không lạ, thấy cuốn này có vẻ là lạ nên mình quyết định mua Bóng Anh Hùng. Quả là không trái với dự kiến, đây đúng là một cuốn sách giá trị.
Vẫn giọng văn tưng tửng, thong thả quen thuộc nhưng Doãn Dũng mở ra một cánh cửa khác : trái với những câu chuyện tếu táo đời thường trong cuốn sách trước, Bóng Anh Hùng tập hợp những chuyện ngắn sâu lắng, nhiều suy tư tâm sự đôi khi ám ảnh mình mấy hôm. Hy vọng mỗi tác phẩm lại khám phá ra những điểm mới nữa trong phong cách viết của anh.
4
327308
2015-09-03 12:53:58
--------------------------
243608
9853
Mua cuốn sách này khá lâu rồi nhưng hôm qua mình vừa mới đọc, phần vì sách mỏng và cũng phần vì những câu chuyện quá hay, mình đọc ngấu nghiến và đọc hết trong chưa đầy một tiếng. Không nhớ rõ lý do vì sao mình mua Bóng Anh Hùng, chỉ nhớ là vô tình đọc một câu chuyện trong cuốn sách ở đâu đó, và tự nhiên tìm đến mua. Bóng Anh Hùng của Doãn Dũng không phải là câu chuyện dài, nhưng là tập hợp nhiều truyện ngắn cho bạn nhiều suy tư, tiếc là chắc do bìa chưa cuốn hút nên mình thấy ít ai biết cuốn sách này.
5
109789
2015-07-27 16:00:06
--------------------------
161633
9853
Doãn Dũng quả là một nhà văn đa tài. Với cuốn truyện trước đó của anh là "Không lạ", giọng văn của anh tếu táo bao nhiêu thì sang cuốn "Bóng anh hùng" này, giọng văn của anh lại sâu lắng bấy nhiêu. Từng mẩu chuyện đều mang trong mình một giá trị nhân văn không hề nhỏ, chuyển tải đến người đọc những thông điệp rất sâu sắc và cảm động. Có những câu chuyện rất ám ảnh.Trong đó xuất sắc nhất phải kể đến truyện ngắn Bóng anh hùng được lấy làm tên cho tập truyện. Mình chờ đợi các sáng tác tiếp theo của anh!
4
17740
2015-02-28 16:28:00
--------------------------
