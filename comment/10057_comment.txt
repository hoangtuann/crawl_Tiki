402671
10057
Bài Tập Ngữ Pháp Tiếng Anh - Đại Từ & Số Từ có bìa màu xanh dương và chiếc cầu nổi tiếng của nước Anh rất bắt mắt. Cuốn sách nhỏ nhắn và mỏng, vì lượng kiến thức cũng không nhiều và khó như những khía cạnh ngữ pháp của tiếng anh. Sách đa dạng với nhiều kiểu bài tập và lượng bài cũng rất là nhiều, nội dung bài cũng không khó. Tuy nhiên sách hơi cũ vì xuất bản từ năm 2011 đến nay cũng được 5 năm rồi  nên cũng có phần trang giấy bị nứt và bong ra.
3
854660
2016-03-22 19:00:52
--------------------------
373957
10057
Cuốn sách Bài Tập Ngữ Pháp Tiếng Anh - Đại Từ & Số Từ này có bìa nhìn rất bắt mắt. Các bài tập rất phù hợp cho những người mới bắt đầu học tiếng Anh . Độ cũng dần dần nâng cao . Mình thấy phần đại từ và số từ trong tiếng anh cũng không khó lắm , nhưng lại rất quan trọng vì trong đề thi thể nào cũng có , mà cũng nhiều khi gây nhầm lẫn . Cuốn sách sẽ giúp ta cải thiện dần dần về mảng tiếng anh này . Cảm ơn tiki.
5
1096195
2016-01-25 09:39:20
--------------------------
