469479
3846
Trong chương trình Ngữ văn lớp 12, học sinh được tìm hiểu bản "Tuyên ngôn độc lập" năm 1945 của Hồ Chí Minh. Bên cạnh những kiến thức hỗ trợ từ nhiều nguồn thông tin từ sách tham khảo, trên internet,...; tôi nghĩ rằng cuốn sách "Sự ra đời của bản Tuyên ngôn độc lập" do Vũ Kim Yến sưu tầm và biên soạn chắc chắn là một tài liệu cần thiết cho tất cả mọi người (trong đó có học sinh viên) khi học tập, nghiên cứu, giảng dạy về bản Tuyên ngôn. Điểm mà tôi thích nhất khi đọc cuốn sách này là giá trị lịch sử khách quan trong chương 2: Những câu chuyện kể, những hồi ức về sự kiện Chủ tịch Hồ Chí Minh viết Tuyên ngôn độc lập. Đọc chương này, hành trình mà bản Tuyên ngôn ra đời như được hiện ra cụ thể trước mắt người đọc. Theo những dòng hồi ức đó, người đọc cũng lạc dòng lịch sử, trở về một cột mốc tự hào của dân tộc.
4
420248
2016-07-06 14:44:06
--------------------------
