395412
9240
Truyện Đóng Cửa Thả Boss thật sự làm mình cảm thấy quá thất vọng. Lúc đầu đọc phần giới thiệu thấy cũng hay nên mua về đọc thử, ai ngờ làm mình quá thất vọng. Cốt truyện cũng bình thường, không có gì đặc sắc. Chỉ được một điều là bìa sách rất đẹp, tông màu sáng, hài hòa, chất lượng giấy cũng tốt nữa. Mình thấy nữ chính cũng được  tính cách mạnh mẽ, thẳng thắn, rõ ràng, kiên cường. Đó cũng là một điểm cộng cho truyện này. Nhưng nếu có cao trào thì sẽ hay hơn và truyện không nên theo môtip cũ vì như thế rất nhàm chán.
2
1145355
2016-03-11 21:20:21
--------------------------
233504
9240
Truyện có nội dung hay. Chỉ là chương đầu viết mông lung quá. Tả cảnh, diễn giải khoa hiểu thật. Nhưng đến giữa truyện, mình thấy câu từ ổn định. Có nội dung. Hay. Nhẹ nhàng, ấm áp. Lại thêm nam thứ thâm tình Nguyễn Chính Nam nguyện tiếp tục sai. Nữ chính vô tư đáng yêu, nhưng vẻ đáng yêu không được tác giả miêu tả kỹ càng. Vừa đọc truyện, mình vừa tưởng tượng ra nữ chính An Tín, nhưng không tưởng tượng ra. Nam chính ổn nhưng lạnh nhạt quá. Truyện hài. Ngoại truyện đọc thấy xót cho nam thứ quá. Mình không cho truyện 5* vì chương đầu viết lung tung khoa hiểu, về sau ổn hết. Nữ chính tính cách hồn nhiên, vui vẻ, quyết đoán. Nói chung mình thấy truyện không đến nỗi bị chê nhạt đâu.
4
489524
2015-07-19 18:10:57
--------------------------
179029
9240
Truyện viết với mô tip cũ, lúc đầu là nữ truy nam. Tình yêu của nữ chính thật ngốc nghếch và mù quáng, đôi lúc làm mình thực sự ức chế giùm chị. Nam chính thực ra cũng có tình cảm với chị, nhưng không dám cho chị bước vào thế giới của mình, bởi chị là một cô gái năng động, xung quanh như có vùng ánh sáng ấm áp, hấp dẫn anh, chính vì vậy anh không muốn làm lu mờ ánh sáng đó, không muốn làm tổn thương chị. Dằn vặt nhau bao phen, hai người cuối cùng cũng đến với nhau. Điều làm truyện mờ nhạt bởi những đoạn ngược tâm mình thấy lãng lãng sao đó, tình cảm của nam chính cũng khá là mờ nhạt, nhiều lúc không nhất quán. Điều cứu cánh cho bộ này là những tình tiết hài hước, dễ thương của chị nữ chính.
3
209069
2015-04-06 12:08:50
--------------------------
177033
9240
Mới đọc giới thiệu, mình cứ tưởng đây là truyện võng du nên ham hố nhào vào, không ngờ đây chỉ là truyện ngôn tình bình thường, đúng là bé cái lầm. Nữ chính cứ gọi là nổi bần bật, mạnh mẽ, kiên trì, giỏi giang,... nhưng nam chính Dụ Hằng cứ nhàn nhạt, lại thêm cái lý do "ghét mái tóc xoăn" siêu củ chuối. Chuyện tình của hai người cũng chẳng có gì mới mẻ, thà để Nguyễn Chính Nam làm nam chính còn hơn, có khi còn cuốn hút hơn ấy. Nhân vật Tướng Công Nửa Đêm khá thú vị thì lại không được khai thác nhiều. Tóm lại truyện hợp để đọc giết thời gian, không nên kỳ vọng nhiều.
3
57459
2015-04-02 13:09:01
--------------------------
131010
9240
Đọc văn án, mình nghĩ đây là truyện thể loại nữ truy nên khá hào hứng, nhưng thật sự chất lượng truyện không như những gì mình mong đợi.
Nữ chính An Tín khá ổn, mạnh mẽ, lạc quan lại giỏi giang, nói chung là tính cách thú vị, nhưng nam chính Dụ Hằng không có gì đặc biệt, phải nói là quá mờ nhạt, đọc xong mà trong mình chẳng đọng lại ấn tượng gì về anh ta cả. Cái lý do “ghét mái tóc xoăn” của anh ta thật là gượng gạo và nhàm chán. Mình thích cá tính của Chính Nam hơn.
Truyện được viết theo hướng hài hước, nhưng mình cảm thấy "độ hài" của nó chưa đủ. Motip cũng quen thuộc, tình tiết thông thường, nói chung là truyện này chỉ ở mức hai sao rưỡi thôi.
3
393748
2014-10-21 20:06:25
--------------------------
109320
9240
''Đóng Cửa Thả Boss'' không quá hay nhưng có thể đọc để giải trí. 
Thật ra ban đầu mình thấy truyện rất cuốn hút, khác với phần bìa, mình còn tưởng nó là một câu truyện buồn cơ. Nhưng càng đọc thì hình như sức hút lại càng giảm. Truyện xuất hiện khá nhiều vai nam nhưng lại chưa khai thác được hết, đọc xong truyện mới cảm thấy hơi dư thừa. Mình khá thích nhân vật Nguyễn Chính Nam, còn hơn cả nam chính Dụ Hằng luôn.  Theo mình, dù là nam chính nhưng Dụ Hằng khá mờ nhạt, cá tính không rõ, hình như bị bạn tác giả bỏ rơi luôn rồi. Mình thấy tác giả ôm dồn quá nhiều thứ. Có nhiều tình tiết có rồi để đó, chẳng phát triển thêm (như là về nhân vật Tướng Công Nửa Đêm ấy). Có khi mình thấy tác giả đi quá nhanh nên không chú tâm vào nội tâm nhân vật . Cốt truyện được, đôi chỗ không hợp lí, cũng không thật kịch tính, gay cấn. Càng đọc về sau mình càng thấy nản. Với lại nhận xét đôi chút về bìa nhé: bìa không đẹp, màu xấu.
Nói chung, nếu bạn đang muốn tìm một câu truyện để đọc cho vui, thư giãn và không quá khó tính thì ''Đóng Cửa Thả Boss'' là một sự lựa chọn hợp lí.
3
106566
2014-03-30 21:10:16
--------------------------
90549
9240
Mỗi người có một ý kiến riêng nên tôi cảm thấy cuốn truyện này khá là nhàm chán.
Đầu tiên là tựa, đọc vào đã thấy khó hiểu. TIếp theo đó là cốt truyện, một môtip quá là quen thuộc. Xuyên suốt câu chuyện không có gì nổi bật. Cách miêu tả và xây dựng hình ảnh 2 nhân vật chính là An Tín và Dụ Hằng không có gì mới mẻ và khá là hư ảo ,ví dụ như việc Boss Dụ Hằng bị dị ứng với mái tóc xoăn của An Tín- khá là chán, hoàn toàn không gây ấn tượng với người đọc. Nội dung câu chuyện đọc cảm thấy khó hiểu. Nhưng nhân vật phụ Ngu yễn Chính Nam thì có phần nổi bật, thu hút hơn nam chính. Tóm lại, đây cũng có thể tạm coi là một câu chuyện tình cảm bình thường pha 1 chút hài hước, mang tính giết thời gian chứ không để lại nhiều dấu ấn đối với người đọc.
2
96609
2013-08-11 10:26:30
--------------------------
89493
9240
Tác phẩm này tuy là thể loại hài, vui nhộn nhưng không quá nhạt vì tác giả có đầu tư cho nội dung truyện, tuy motip không quá mới mẻ. Hơn nữa, có lẽ do cách viết của tác giả, nên truyện không được đặc sắc cho lắm.

Nhân vật nữ chính khá ổn, tính cách mạnh mẽ, thẳng thắn, rõ ràng; trong công việc lại giỏi giang, chứ không ngu ngơ. Còn nhân vật nam chính thì tác giả khắc họa một số tình tiết còn mơ hồ, nhưng tổng thể thì khá ổn. Khi đọc truyện, có thể người đọc sẽ không biết được nam chính bắt đầu thích nữ chính từ lúc nào, nhưng mà dần thì sẽ cảm thấy được nam chính rất thâm tình.

Truyện càng về sau thì càng tiến bộ hơn phần đầu truyện. 

Nói chung, truyện đọc ổn, thích hợp để giải trí.


3
7317
2013-08-02 12:09:33
--------------------------
84060
9240
Nhìn vào bìa sách và tên truyện tôi cứ ngỡ đây là một truyện hài. Lúc đọc truyện mới biết, đây là một truyện vô cùng sâu sắc. Cốt truyện rất ổn. Tuy nhiên tôi không thích cách viết của tác giả Tứ Mộc.  Một vài tình tiết hơi nhanh và nhạt, có đôi chỗ phải đọc lại vài lần mới hiểu. Ban đầu, tôi không có chút ấn tượng gì với nam chính Dụ Hằng, tác giả khắc họa nhân vật không đặc biệt lắm, còn nhân vật Nguyễn Chính Nam thì tôi khá thĩch. Nhưng càng về sau, nam chính được nói đến nhiều hơn, nhưng cũng vẫn không làm cho tôi ấn tượng. Nói thật là tính cách của bạn nam chính không hay tí nào. Đọc vài chỗ song ngẫm lại tôi không biết bạn nam chính có thật sự yêu bạn nữ chính không nữa, cứ gượng gạo thế nào ấy. Còn về nữ chính An Tín, lúc đầu tôi khá yêu thích cô, vì cô thông minh, tài giỏi, có đôi khi hơi ngây ngô nhưng lại không kém phần sắc sảo, tính cách vui vẻ lạc quan, dám yêu đám hận, có lẽ đó cũng là lí do mà các bạn nam phụ khác cũng thích nữ chính. Tình tiết truyện tôi cho là tạm ổn, nếu là độc giả dễ tính thì chắc sẽ hài lòng, còn khó tính một chút thì có lẽ sẽ vẫn chấp nhận được. Nếu mà tác giả dành một chút thời gian cũng như câu chữ để chau chút, xoáy sâu một chút vào chi tiết Tướng Công Nửa Đêm trong game online và về nhân vật Cash thì truyện sẽ hay hơn. 
4
25683
2013-06-28 17:14:02
--------------------------
