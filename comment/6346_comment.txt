476542
6346
Câu chuyện này nếu được bán ở nhà sách thì chắc tận 75000, với số tiền này tôi thừa sức mua được hàng tá cuốn sách kinh điển trên toàn thế giới với những bài học, những cách đối nhân xử thế, những cách để trở thành người có học chứ không phải cứ đụng độ nhau rồi làm hung làm hăng lên. 
Mới đọc vào đã có một đống chuyện xảy đến. Nói thẳng ra thì cô sinh viên này chẳng có văn hóa gì cả, thích đánh thì đánh cứ như côn đồ vậy, không sao chấp nhận nổi. Đã ăn cướp xe người ta mà còn đánh người cứ như vừa ăn cướp vừa la làng vậy. Đó là dữ dằn, là bất lịch sự, là vô văn hóa chứ không phải mạnh mẽ hay cá tính. Sau đó, tôi mới biết cô sinh viên đó học kém môn luật, thảo nào không biết cách ứng xử sao cho đúng đắn. 
Còn nam chính là giảng viên đại học có tính cách cũng không chút nào thuyết phục. Tôi không nghĩ rằng giảng viên là người có học thức cao rộng mà lại suy nghĩ thiếu chín chắn đến như thế, có vô lý không? Tác giả viết cứ như điều khiển một bàn cờ theo cách cứng nhắc nhất vậy!
Chung quy lại thì thứ này chỉ để thỏa mãn thị hiếu cho mấy em nhỏ nông nổi mà thôi chứ in ra sách để làm bài học nhân văn thì tôi thà mua mấy quyển vừa rẻ vừa ý nghĩa còn tốt hơn. Nhà xuất bản có ham lợi nhuận quá không khi cứ in tràn lan rồi tố cái giá trên trời mà không để ý nội dung ra sao, nó có ảnh hưởng gì xấu cho giới trẻ không.
1
975584
2016-07-21 11:11:19
--------------------------
453561
6346
Đây là cuốn truyện có motip khá quen thuộc và không có gì xa lạ với những bạn yêu thích truyện teen. Trước hết là về hình thức, mình thấy bìa truyện chưa được bắt mắt, chất liệu giấy cũng không được mịn. Về nội dung thì truyện không có điểm nhấn, cũng chỉ là xoay quanh mối tình thầy trò quen thuộc, mình có cảm giác truyện hơi dài dòng, đôi khi có những tình tiết không đáng kể mà cũng phải kể ra. Hơn nữa, Uyên là cô sinh viên mà dám đùa với giảng viên như vậy, thế mà Uyên không bị sao cả, mình thấy hơi vô lý. Truyện diễn biến quá nhanh, chưa có gì đặc sắc, cốt truyện quá quen thuộc. Mong tác giả cố gắng viết những tác phẩm mới mẻ hơn nữa .
2
562321
2016-06-21 12:18:30
--------------------------
447136
6346
Theo cảm nhận của bản thân mình thì cuốn chuyện rất lôi cuốn nhưng có phần không đạt. Cô nàng tên Uyên trong chuyện quá kiêu ngạo làm những chuyện gây khó sự cho anh chàng tên Khoa. Nếu là đó là ngoài đời thật rất khó sử lý. Có điều bù lại là chuyện tình của họ rất đẹp, vui nhộn không dịu dàng như các truyện khác nó bạo lực mạnh mẽ. Tình yêu không phân biệt tuổi tác, đến với nhau vì trái tim. Với mình thì chuyện không gọi là hay tuyệt vời nhưng cũng không phải dở, tác giả nên sửa lại tính gây họa của cô nàng nhân vật chính
3
1300330
2016-06-13 15:56:02
--------------------------
425509
6346
Vì trước khi mua sách mình thường đi đọc tóm tắt và nhận xét, sau đó mới quyết định mua hay không. Quyển này cũng vậy. Và thật sự thì, cuốn Cáo sa bẫy cáo, có tóm tắt hay hơn nội dung thật sự khá nhiều^^ Tình tiết truyện dễ đoán, quen thuộc, những câu thoại mang cảm giác "không thật" (dù biết là tiểu thuyết thường phi thực tế nhưng mình vẫn thích tính sát thực một chút). Truyện không có điểm nhấn, đọc xong một lần sẽ "trôi" luôn. Điểm cộng để níu kéo là bìa sách đẹp, giấy hơi mỏng mùi nồng (nếu bạn thích ngửi mùi sách giống mình thì sẽ thích điều này).
Nhưng khách quan thì tác phẩm này không tệ lắm với một ngòi bút mới như Sâu.
3
440932
2016-05-04 21:07:54
--------------------------
392034
6346
Cáo sa bẫy cáo có thể nói là một tác phẩm cũng hơi lặp lại các motif cũ, nhưng vì có thêm các tình tiết vui nhộn giữa nam, nữ chính nên không đến nỗi nhàm chán. Điểm trừ của tác phẩm chính là nội tâm nhân vật chưa được khắc họa rõ nét, đúng hơn đối với một số độc giả như mình sẽ không lưu lại ấn tượng lâu. Nhịp truyện có lúc chậm quá hoặc nhanh quá mức cần thiết. Đôi khi, ở một vài phân đoạn diễn tả tình cảm của hai người chưa thật sự đủ sâu để gây nên sự suy nghĩ của độc giả. 
3
632796
2016-03-06 08:22:52
--------------------------
387855
6346
“Cáo sa bẫy cáo” là một cuốn sách thú vị ngay từ cái tên.Đây là một cuốn truyện tiểu thuyết tình cảm ngọt ngào  nhưng cũng không kém phần hài hước, dễ thương.Khoa – chàng giảng viên đại học với vẻ ngoài đạo mạo cùng nụ cười hiền hòa luôn thường trực trên khuôn mặt điển trai. Nhưng đằng sau vẻ bề ngoài ấy là một chàng trai hai mươi bảy tuổi với tính cách có phần hiếu thắng, lấy chuyện trả đũa những người từng “đắc tội” với anh làm niềm vui và chưa từng yêu đương.
Uyên – cô sinh viên có vẻ ngoài dễ thương, nụ cười hiền lành cùng nét tính cách lạc quan, cá tính mạnh mẽ. Một cô gái có trái tim từng bị tổn thương bởi tình cảm gia đình bị chia cắt, luôn sợ hãi chuyện tình yêu.
Một lần lạc đường trong con ngõ nhỏ, một cuộc rượt đuổi có phần bạo lực, Uyên chính thức “đắc tội” với Khoa, mở đầu cho những tình huống oan gia sau đó.Những tình huống dở khóc dở cười rồi dần đến tình yêu của 2 người.
5
917899
2016-02-27 19:52:52
--------------------------
359181
6346
Một câu chuyện về tình thầy trò hài hước, giọng văn nhẹ nhàng, ấm áp đã gợi lên cho người đọc những tình cảm sâu lắng, miên man, đôi chút dí dỏm. Tên truyện khá ấn tượng, cốt truyện độc đáo. Mình rất thích truyện này! Đây là truyện đầu tiên mình đọc về tình cảm thầy trò. Mình không đọc truyện teen Việt nhiều nhưng mình nghĩ đây là một truyện đáng đọc cho những ai thích sự nhẹ nhàng, lãng mạn pha chút tiếng cười. Khoa và Uyên là nhân vật chính của nội dung truyện. Hai người, hai tính cách sẽ đem đến cho mỗi người chúng ta nhiều suy nghĩ cũng như tình cảm. 
Ngoài ra, sau khi mua quyển sách này mình khá hài lòng. Bìa sách đẹp, trình bày rõ ràng, thu hút, ấn tượng ngay từ cái nhìn đầu tiên.

3
510263
2015-12-26 16:57:27
--------------------------
344294
6346
Tình yêu của hai nhân vật chính là Khoa và Uyên khá nhẹ nhàng, nhưng đủ sâu sắc, không quá dằng xé nhưng đủ sóng gió để khi trải qua nó, họ nhận ra mình yêu nhau bao nhiêu, nội dung không mới mẻ, vẫn là motip cũ nhưng vẫn lôi cuốn mình. Chất lượng giấy và mực in rất ổn, cầm nhẹ tay lắm. Điểm cộng nữa là bìa sách rất dễ thương, có tặng kèm bookmark xinh xắn. Tiki giao hàng nhanh chóng, gói rất cẩn thận. Tóm lại mình khá là hài lòng với sản phẩm này.
4
569626
2015-11-28 08:12:24
--------------------------
305441
6346
Hấp dẫn bởi tên của sách ""Cáo Sa Bẫy Cáo"" , tên rất hấp dẫn, bìa truyện cũng rất đẹp, tình tiết trong câu truyện cũng hấp dẫn không kém.

Về hình thức: Sách được in ấn đẹp, chất liệu giấy tốt, chữ in đẹp và rõ.

Về nội dung: Truyện kể về một chàng giảng viên hiếu thắng, và một cô gái hiền lành dễ thương, nhưng là một người bị tổn thương và sợ hãi tình yêu. Truyện kể về một đôi oan gia ngõ hẹp, những cái bẫy chàng giảng viên đặt ra để giăng bẫy cô nàng, và cuối cùng lại sập bẫy bởi cái bẫy của chính mìn. Truyện hài hước, vui nhộn, rất vui khi thấy, quyết định mua và đọc truyện này.
4
409134
2015-09-16 22:57:40
--------------------------
288052
6346
Truyện khá hay, lôi cuốn người đọc xuyên suốt từ đầu đến cuối truyện. Hai nhân vật chính khá thú vị, tác giả đã có một sự kết hợp độc đáo. Truyện gây tò mò cho người đọc về diễn biến các tình huống, cũng như các chiêu trò tiếp theo của hai nhân vật chính. Nhưng có lẽ tác giả còn quá trẻ và chưa lập gia đình nên về cuối truyện các tình tiết, nhìn chung là không có thực tế cho lắm. Thằng nhóc con của hai nhân vật chính cũng khá lém lỉnh, tôi thích thằng nhóc này. Nói chung có thể xem đây là một cuốn truyện đáng để đọc.
4
751437
2015-09-03 11:13:31
--------------------------
247318
6346
Đây là tác phẩm Việt Nam tôi cực kỳ thích. Câu chuyện nhẹ nhàng, lãng mạn lại không kém phần hài hước. Khi đọc tôi không thể nhịn cười trước những chiêu trò của hai nhân vật chính. Thực tế ai cũng đưa hai nhân vật đều sa vào chính cái bẫy mà mình đặt ra. Tôi thích tính cách của nữ chính vì đâu đó là một phần tính cách của tôi. Uyên - mạnh mẽ, ngang bướng nhưng lại rất yếu mềm. Còn Khoa làm t nhớ tới Dương Lam Hằng. Khoa lúc đầu bày đủ trò để chọc Uyên, xong cuối cùng lại rơi vào ' bẫy tình'. Khoa yêu chân thành, luôn cố gắng dành cho Uyên những gì tốt đẹp nhất. Vất vả lắm, tình yêu chân thành mới đơm hoa. Bên cạnh đó, tôi cũng thích chị của Khoa. Tôi thích những cô gái mạnh mẽ, luôn làm điều mình thích, tự do bay nhảy, không quá phụ thuộc vào người khác. Nhưng khi cần dịu dàng thì rất ư quyến rũ. Thích câu chuyện này lắm lắm. 
5
553694
2015-07-30 10:09:31
--------------------------
221673
6346
tự tay đi mua quyển này.bìa thì đẹp nội dung cũng rất hay nhưng hình như còn mờ nhạt chưa sâu sắc nên ít để lại gì mặc dù mình mới đọc xong tối qua.cũng có ấn tượng 2nhân vật chính nhưng có vẻ phi thực tế quá.làm gì có cô sinh viên nào dám cạo cả đầu thầy.mình lại thấy 2 phần tính cách trái ngược trong Khoa nó hơi mâu thuẫn.về cơ bản thì chấp nhận được.cái hình tượng này làm mình nhớ đến Dương Lam Hàng vẻ lịch lãm cuốn hút dịu dàng của anh có pha chút này trong Khoa thì phải có lẽ do có ảnh hưởng
4
517957
2015-07-04 10:25:12
--------------------------
208495
6346
Đây là cuốn truyện đầu tiên mình dám mua. Mình và nhỏ bạn bị thu hút bởi bìa truyện và nhan đề ngay khi nhìn thấy nó ở nhà sách Fahasa. Trước đây mình đọc một vài truyện teen trên mạng nhưng thật sự rất nhàm chán. Nói chung là mình không thích truyện Việt Nam. Nhưng cốt truyện nằm ngoài tưởng tượng của mình. Nó hay và thu hút lần đầu khi đọc. Sự xấc láo của cô học trò tên Uyên và tính cách cáo già của chàng giảng viên tên Khoa. Tình huống truyện hài hước và cách đối đáp khá hay. 2 nhân vật trải qua 1 vài chuyện mới đến được với nhau. Truyện này khá được hơn so với các truyện teen khác. Có điều giấy khá là mỏng.
5
569514
2015-06-15 11:08:47
--------------------------
175266
6346
Mình thích tính cách của nữ 9: mạnh mẽ, cá tính & có bản lĩnh, cả vẻ đẹp được tác giả miêu tả cách sắc sảo. Vẻ bên ngoài của nữ 9 Hạ Uyên có vẻ nữ tính, yếu đuối, nhưng thật sự bên trong cô hoàn toàn ngược lại. Mở đầu là 1 cuộc rượt đuổi và cả 2 nhân vật 9 gặp nhau, trong hoàn cảnh rất ư là éo le, hài hước. Càng về sau, những cuộc gặp gỡ càng thú vị, cũng trong tình huống éo le và hài như lần gặp đầu tiên. Không chỉ có nữ 9 mà còn có chị của nam 9 tính tình cũng giống hệt nữ 9. Cốt truyện đặc biệt, không thể xếp nó vào chung với những loại truyện teen có motip như nhau, cũng không phải kiểu phóng đại lên như mọi người nói. Cuốn sách này rất thực tế! Tóm lại, cuốn sách này chỉ hợp với những người thực tế như nó thôi!
4
366101
2015-03-29 20:18:09
--------------------------
158215
6346
Đầu tiên là hình thức, bìa truyện đẹp, thuận mắt và hợp lí, tuy nhiên chất lượng giấy không được tốt, làm giảm đi chất lượng đã thấp của Cáo sa bẫy cáo. Sâu đã không làm đổi mới motif cũ của truyện, một điều mà tôi mong chờ từ tập truyện này, hơn nữa lời đối thoại không được tự nhiên, lời văn hơi gượng ép. Những tình tiết xây dựng hời hợt, diễn biến nhàm. Tuy nhiên được cái tác giả không sùng bái ngôn tình quá mức, vẫn biết đặt tên nhân vật một cách thuần Việt, cộng cho Sâu 0.25 sao nữa là 1.5 :)
2
435897
2015-02-11 21:13:28
--------------------------
133169
6346
Cáo sa bẫy cáo, một câu chuyện rất hay. Mình rất thích cốt truyện này. Cáo giăng bẫy để bắt con mồi nhưng lại vô tình mắc một chân vào chính cái bẫy đó. Sâu đã rất thành công khi viết truyện này. Mình đã không thể nhịn được cười khi đọc. Uyên một cô gái mạnh mẽ, hung dữ, bướng bỉnh và giỏi võ nhưng cũng có lúc cô thật yếu mềm. Còn Khoa suốt 27 năm giữ trái tim lành lặn, giữ cho bản thân tự do thì bây giờ lại bị Uyên lấy mất, bị chính con mồi của mình lấy đi trái tim. Có thể nói một câu chuyện rất hay và hấp dẫn mình
5
464521
2014-11-04 12:09:25
--------------------------
132644
6346
Đầu tiên phải nói là chất lượng giấy rất tệ. Bề ngoài đã không thu hút như vậy làm sao đáng số tiền đồng tiền bát gạo của độc giả. Mong bên nhà xuất bản chú ý hơn.
Cái tên khiến tôi liên tưởng đến một câu chuyện motif quen thuộc ngay lập tức, và tất nhiên tôi không nhầm. Tên nhân vật và địa danh rất thuần Việt, văn phong tác giả khá, nhưng cách triển khai câu chuyện không tốt, tình tiết quá nhanh, đôi chỗ dư thừa và phi logic. Xây dựng kiểu nhân vật quá quen thuộc, hơi nữa lại cực kì cường điệu, lời thoại nhiều lúc gượng gạo, hành động cũng rất bất thường. Tôi không thích cách mà nữ chính thiếu tôn trọng thầy giáo của mình đến vậy, ngay cả ở đoạn đầu cũng thấy cô ta quá ư xấc láo một cách bất bình thường khi cướp xe người ta và thản nhiên đánh người như vậy. Cả nam chính cũng không hề ra dáng người giảng viên trí thức cao một chút nào.
Tuy nhiên so với một số truyện teen khác gần đây thì tác phẩm này tốt hơn nhiều. Nhưng chỉ phù hợp với những đối tượng nhất định và thích hợp để giải trí nhiều hơn là suy ngẫm.
1
294086
2014-11-01 21:06:18
--------------------------
131980
6346
Tôi chú ý vì tựa đề khá lạ và gây sự tò mò.Khi có nó trong tay,quả là đọc đến độ không dừng lại được.Câu chuyện tình của anh giảng viên và cô học trò cá tính nhưng vẫn ngây ngô đáng yêu của mình.Đúng là khi yêu thì muôn vàn cảm xúc xảy đến,2 nhân vật đã phần nào lột tả hết.Mạch cảm xúc,tình tiết đưa người đọc từ bất ngờ này đến bất ngờ khác,tôi tưởng chừng câu chuyện tình đã đi đến hạnh phúc thì đột ngột thấy chương"Chia tay",nhưng đó là sự thách thức họ gặp phải để biết tình yêu của mình lớn thế nào thôi.Rồi tình yêu ấy đã tạo nên 1 gia đình hạnh phúc ngập tràn. Thật sự quyển truyện này tôi đã đọc đi đọc lại 3 lần.
4
386616
2014-10-28 22:05:39
--------------------------
127384
6346
Lần đầu tiên tôi biết đến Sâu là khi tình cờ vào Trang mạng xã hội văn học và đọc thấy thông tin về cuốn sách "Cáo sa bẫy cáo". Chỉ đơn giản là cái tên truyện nghe hay hay nên mới chú ý đặc biệt và tìm đọc. Nhưng khi đọc rồi thì không thể dừng lại được. Thực sự bị ấn tượng bởi lối viết truyện đơn giản, tự nhiên nhưng lại logic khiến người đọc không thể rời mắt.
Tôi bị cuốn vào câu chuyện tình cảm của Khoa và Uyên. Cứ nghĩ đó là một trò đùa của hai con người có thừa sự kiêu ngạo và bản chất ngang tàng để không sập bẫy của nhau nhưng cuối cùng mọi chuyện lại hoàn toàn ngược lại. Đúng là trong chuyện tình cảm chẳng thể nói trước được điều gì...Kết thúc câu chuyện là một kết thúc đẹp, sau bao ngày chạy đua với tình cảm họ cũng đã ở bên nhau trong một gia đinh hạnh phúc ngập tràn .....
3
403479
2014-09-24 09:02:15
--------------------------
126060
6346
Công nhận là truyện không phải viết chuyên sâu về biểu hiện nội tâm nhân vật cũng như truyền đạt ý nghĩa nhân văn. Nó theo loại hình quen thuộc là để giải trí cho giới trẻ. Nhưng không phải là nó thiếu sắc sảo. Truyện này thuộc loại cao hơn so với những truyện teen hay thần tượng bình thường, nhưng lại chưa đủ tầm để liệt vào tác phẩm văn học. Nên sẽ chia ra hai trường phái. Một bộ phận người trẻ rất thích vì nó cái gì cũng "vừa phải", suy nghĩ vừa phải, vui,... Một bộ phận khác thì không thích ứng với loại này được, khi đọc một cuốn sách mà cuối cùng chả có gì để đọng lại và suy nghĩ. Thế thôi. Với một người viết trẻ, tác phẩm đầu tay như vậy là ổn rồi. Có nét sắc sảo trong ngòi bút để phát triển về sau.
Còn cái phần giấy in này nọ thì do nhà in rồi, một phần gây mất thiện cảm nữa. Tôi sẽ cho 4sao về truyện của người viết ( Nếu tác phẩm thành giấy mà giấy quá tệ thì chỉ có thể được 3sao.)
Mong nhà in, tác giả trẻ có thể tiếp tục phát triển hết khả năng, để gởi đến cho chúng tôi những tác phẩm chỉnh chu, hoàn thiện hơn.
4
424826
2014-09-16 21:17:52
--------------------------
125125
6346
Nhìn thấy bạn mình có mua quyển này, giở ra xem thì thấy chất liệu giấy không tốt lắm, chữ in cũng không làm mình thấy hài lòng. Mình nghĩ ở lần biên tập sau cần cải thiện nó.
Về mặt nội dung thì mình chấp nhận được, dù là mô tip khá quen thuộc nhưng tác phẩm đã có những điểm nhấn riêng của nó. Văn phong thì nhẹ nhàng và đầy hài hước, mạch truyện cũng khá nhanh làm người đọc cảm thấy không buồn chán hay nản. Tuy có nhiều tình tiết trùng lấp nhưng mình thấy truyện này khá ổn so với một ấn phẩm đầu tay của một tác giả trẻ.
Hy vọng tác giả vẫn giữ được những xúc cảm đầy hài hước và lãng mạn để vẽ nên những câu chuyện tình đẹp khác!
3
303773
2014-09-11 13:06:47
--------------------------
123098
6346
Lần đầu mình đọc được các đoạn trích, giới thiệu... của sách trên facebook, mình đã bị cuốn hút ngay lập tức. Mình còn nhớ có đoạn giới thiệu: "Nếu bạn đã quá chán nản tiểu thuyết ngôn tình thì đây là một sự lựa chọn thú vị, tiểu thuyết đi theo một cách mới..."
Nên mình đã quyết định mua thử, sau khi đọc đến trang thứ 50 thì mình đã không tài nào chịu nổi nữa. 
Tiểu thuyết có nội dung, có tình tiết. Nhưng mọi thứ diễn biến quá nhanh, quá nhiều thứ gọi là "trùng hợp, định mệnh" làm truyện càng lúc càng nhạt. Tác giả không đi sâu vào nội tâm nhân vật, chỉ chú trọng tình tiết, mạch truyện quá nhanh làm cho độc giả ngạt thở. 
Mình nghĩ tiểu thuyết sẽ thích hợp cho các bạn thích những bộ phim thần tượng. Còn nếu yêu cầu cao về chiều sâu, lắng đọng, suy ngẫm thì đó cũng là một phần thiếu của tiểu thuyết.
1
94867
2014-08-29 23:06:47
--------------------------
123063
6346
Thời nay cái gây ứng tượng đối với giới trẻ khi mua sách là tên truyện và bìa. Và thật sự Cáo Sa Bẫy Cáo là cái tên khá mới lạ, cùng với bìa sách vẽ tinh xảo và màu sắc đẹp đẽ. Nội dung cũng khá ấn tượng đối với người đọc, Uyên là một  cô gái cá tính và mạnh mẽ, dám nghĩ dám làm. Còn Khoa là một anh chàng có  trách nhiệm nhưng cũng không kém phần ga-lăng. Nói tóm lại, tác phẩm này cũng để lại một ấn tượng đẹp đối với đọc giả và cũng rất đáng để đón đọc. Tuy nhiên giấy in có phần hơi không được tốt lắm, cái này chắc phải nhờ nhà in xem lại.
4
182234
2014-08-29 19:53:54
--------------------------
122932
6346
Tác phẩm có cách viết rất tốt và tự nhiên, dưới góc độ là tác giả trẻ và là tiểu thuyết đầu tay thì cũng là rất khá. Tuy nhiên nội dung cần được chú ý hơn chứ không nên chỉ đọc để giải trí đơn thuần. Hi vọng tác phẩm sắp tới tác giả sẽ truyền đạt vào câu chuyện ý nghĩa nào đó.
Cũng hơi thất vọng về giấy. Loại giấy xốp, nhám nên sách tuy dày nhưng vẫn nhẹ khi cầm, có lẽ để hợp với bìa nên nhà in chọn loại giấy có màu hơi vàng dễ gây cảm giác giấy cũ.
4
32823
2014-08-28 21:17:09
--------------------------
122534
6346
Nội dung của cuốn sách này thì không cần phải bàn, vì nó khá là quen thuộc. Những kiểu truyện như thế này đầy trên mạng, mô tip y hệt nhau, kể chuyện tình yêu hài hước, oái oăm theo kiểu phóng đại lên. Đọc giải trí cho vui thì được chứ chẳng có giá trị gì. Tuy nhiên, khi nhận được hàng, mình có phần bực bội. Ngoài cái bìa đẹp ra, còn lại, giấy bên trong nhìn như giấy tái chế, nhìn vào mép sách là thấy được sự kém chất lượng so với những cuốn còn lại ngay. Thật là khó chịu khi phải mua một cuốn sách với giá cửa hàng, nhưng chất lượng lại chợ búa.
2
220006
2014-08-26 14:38:51
--------------------------
