495381
4615
Đối với mình đây là quyển sách rất đáng đọc. Nó mang lại cho mình một góc nhìn khác về cái gọi là "Tầm nhìn", trở thành cái gần gũi, chân thật, và đầy động lực chứ không phải xa vời như một số lí thuyết tổ chức có đề cập đến. Và bởi những hiểu biết mà quyển sách đem lại xuất phát từ thực tế và thực nghiệm nên rất dễ tiếp thu và thấm nhuần. Mình đánh giá cao từng luận điểm mà Mohammed bin Rashid Al Maktoum đề cập tới, cũng như cách hành văn của ông vẫn còn bộc lộ bản tính của một dân tộc bá quyền. Thông qua quyển sách, mình biết thêm về Dubai và thế giới Ả-rập, và bản chất dân tộc họ. Quyển sách rất hay và bổ ích!
5
79835
2016-12-12 16:36:13
--------------------------
470278
4615
Mình là khách quen của tiki, cuốn sách này mình đặt cùng với một cuốn nữa là " KHông Đánh Mà Thắng". 

Từ trước giờ qua các phương tiện thông tin đại chúng, mình ngưỡng mộ hoặc kinh ngạc về những thứ có chữ Dubai: khách sạn Atlantic, Đảo Cây Cọ, và tòa tháp cao nhất thế giới...nhưng giấc mơ có thật hay thiên đàng ngay dưới trần thế kia thực sự nó là một quá trình lao động vất vả và vinh quang của lớp lớp thế hệ những người con sống trên miền đất sa mạc nóng bỏng. Mình nhớ lại hai câu nói, một của tác giả"bởi đất nước chúng tôi toàn là sa mạc nên không có gì cản trở tầm nhìn của chúng tôi cả", rất thi vị. Một câu nói khác của người Mỹ "thế giới không tự dưng tốt đẹp, sỡ dĩ nó tốt đẹp là vì có những người luôn cố gắng vì thế giới tốt đẹp hơn, bao dung hơn".
5
249921
2016-07-07 12:45:15
--------------------------
432966
4615
mình từng đọc nhận xét tốt từ các bạn đã mua sách nhưng có lẽ vì cuốn sách về nội dung chính trị nên mình cảm thấy hơi khô khan và có tính chất chung chung nên mình cảm thấy cuốn sách không thực sự cuốn hút như tựa cuốn sách "là tầm nhìn thay đổi quốc gia " và chất lượng giấy trong sách cũng không được đẹp, nhưng nếu bình chọn là 5 sao thì mình thấy nội dung sách này chưa xứng tầm nên đề nghị các bạn trung thực hơn trong nhận xét của mình để người mua sau khi mua không cảm thấy bị hụt hẫng, tiki.vn giao hàng nhanh mình rất thích điểm này   
3
713046
2016-05-20 09:22:43
--------------------------
317910
4615
Ban đầu mới mang cuốn sách này về, cũng tự hỏi không biết bao giờ mới đọc xong. Sách rất là dày, khổ to, có một số trang in màu các hình ảnh tư liệu. Cuốn My vision - Tầm nhìn thay đổi quốc gia này, thật ra lại không quá khó đọc. Tất cả những điều mà nhà lãnh đạo của Dubai - cũng chính là tác giả cuốn sách này viết - thật sự rất chân thật. Cái tài lẫn cái tâm của ông đã tạo nên một tầm nhìn tuyệt vời để kiến tạo một vương quốc Dubai như hiện nay. Mình thấy sách thích hợp cho những ai làm công tác quản lý, đặc biệt là quản trị nhân sự, vả cả các bạn trẻ đọc để mở mang và thêm động lực vươn cao. 
5
531312
2015-10-04 16:44:46
--------------------------
303093
4615
Thực sự là chưa bao giờ, ngay cả trong trí tưởng tượng của mình, mình có thể cảm thấy xúc động khi đọc một cuốn sách thuộc đề tài lãnh đạo, một đề tài dường như là cứng nhắc. Nhưng mình đã cực kỳ xúc động khi đọc xong cuốn sách này! Mình không chỉ cảm nhận được tài năng kiệt xuất của người lãnh đạo mà trên hết là cái tâm, là tình yêu dành cho người dân của quốc vương Dubai. Nhắc đến Dubai, người ta nghĩ đến những điều không thể trở thành có thể, những kỳ tích, những phép màu, đọc cuốn sách và bạn sẽ hiểu được là vì sao. Hãy đọc sách và bạn sẽ cảm nhận được tinh thần của nhà lãnh đạo chân chính!
5
579087
2015-09-15 17:35:56
--------------------------
292363
4615
Bây giờ gần như cái gì có gắn chữ "DUBAI" đều mang trong mình 1 sự cuốn hút rất khó cưỡng lại, cuốn sách này là 1 trong những thứ như vậy. Ban đầu mình mua chỉ vì khá hứng thú với Dubai, chứ chả phải vì cuốn sách này được ghi bởi ông hoàng thân Mohammed nào đó. Nhưng không ngờ là sau khi đọc khoảng 50 trang đầu tiên thì đã được đại khai nhãn giới hoàn toàn.

Sách được đặt tên là My Vision là rất đúng, khó tưởng tượng được những cái đầu tầm cỡ đến như thế nào mới có những tầm nhìn xa hút vào tương lai và can đảm vững vàng để đi ngược lại mọi điều được cho là hợp lý như vậy. 

Sách đọc dễ hiểu và bổ ích, mọi người nên mua đọc nhé.
5
660186
2015-09-07 11:08:14
--------------------------
283483
4615
Tác phẩm dành cho những người quan tâm đến kinh tế và sự thịnh vượng giàu có của quốc gia Trung Đông nổi tiếng với rất nhiều kỉ lục và thành tự về kinh tế Dubai với đảo nhân tạo lớn nhất thế giới, khách sạn 7 sao đầu tiên của thế giới, tòa tháp cao nhất thế giới...đằng sau của những thành tựu vĩ đại này của Dubai là một tầm nhìn của một nhà lãnh đạo tài ba. Đó không phải chỉ đơn giản là một chiến lực một chính sách mà nó là tầm nhìn làm thay đổi cả một quốc gia. Đó phải là tầm nhìn vĩ đại. Tác phẩm còn cho ta biết rõ hơn về tính cách con người Dubai nói riêng và tìm hiểu về thế giới dầu mỏ nói chung. Rất đáng tham khảo.
5
581993
2015-08-30 00:24:31
--------------------------
231723
4615
Một cuốn sách thu hút được người đọc ngay từ những trang đầu tiên. Cuốn sách là những dòng tâm sự, là những kinh nghiệm quý báu  của một vị lãnh đạo đáng kính về chặng đường phát triển của Dubai cũng như UAE. Không chỉ đem lại cho độc giả một cái nhìn rõ nét về sự thịnh vượng và phát triển của Dubai cũng như UAE, mà Quốc vương Dubai đồng thờ là Thủ tướng của UAE Mohammed bin Rashid AI Maktoum còn mang đến cho chúng ta bức chân dung về một vị lãnh tụ tài đức vẹn toàn, một con người với "tầm nhìn" toàn diện, sâu xa, một nhà lãnh đạo tài ba cũng như là một con người với một tâm hồn thông tuệ. 

5
596779
2015-07-18 11:27:19
--------------------------
212534
4615
Thời gian gần đây , những nhà nước hồi giáo tự xưng IS đã gây ra những vụ bao lực , khủng bố trên thế giới , làm mọi người có cái nhìn không thiện cảm hay nặng hơn nữa à ác cảm với đạo hồi . Tuy nhiên , không phải đạo Hồi xấu ,mà là vì có những cá nhân, tổ chức nhân danh đạo hồi để bắt thế giới chú ý đến mình .
Quyển sách là một tự truyện vô cùng gần gủi của nhà lãnh đạo Dubai kiêm thủ tướng của UAE nói về cách ông đã đinh hướng tầm nhìn của mình thành tầm nhìn của Dubai như thế nào . Từ việc tinh gọn bộ máy nhà nước đến việc xây dựng những khu phức hộp , cảng biển đồ sộ , miễn thuế tái xuất sản phấm , chính phủ điện tử và hơn hết là gớp phần cộng đồng Hồi giáo theo hướng tích cực về kinh tế lẫn chính trị .
Hãy đọc quyến sách này nếu bạn muốn biết sự vĩ đại về tăng trưởng kinh tế của Dubai .
Xin cảm ơn .
5
554150
2015-06-22 16:04:06
--------------------------
179083
4615
Trước giờ tôi chỉ biết tới Dubai qua báo chí và Internet. Dubai là một trung tâm kinh tế mới nổi của thế giới qua các hình ảnh biểu tượng như tháp Buji Khalifa, khách sạn 7 sao Burj Al Arab,... hay sự xa hoa của người dân Dubai như nuôi động vật hoang dã, đi siêu xe, ATM vàng hay điện thoại dát vàng và kim cương cùng sự bành trướng của các ông chủ ra toàn thế giới. Sự phát triển thần kì của Dubai là một dấu chấm hỏi lớn cho nhiều quốc gia. Nhưng khi đọc xong tác phẩm này thì có lẽ chúng ta sẽ hiểu sự phát triển đó có công lao to lớn của các vị quốc vương của tiểu quốc này và đặc biệt là tài năng lãnh đạo của quốc vương Mohammed bin Rashid Al Maktoum.
5
387632
2015-04-06 13:56:07
--------------------------
