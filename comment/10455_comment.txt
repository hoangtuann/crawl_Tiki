421919
10455
Lớn lên với những tác phẩm văn học thiếu nhi nổi tiếng quả là một điều may mắn. Khi tìm lại được cuốn sách đã theo mình suốt thời thơ ấu, mình cảm thấy vô cùng hứng khởi. Cũng những câu chuyện đó, những nhân vật quen thuộc, những tình tiết không lẫn vào đâu được,... Lời tựa đầu của cuốn sách viết không được khéo và đã sơ ý làm lộ nội dung của câu chuyện (spoiler alert) vì vậy có thể sẽ làm phật ý các bạn chưa từng đọc qua câu chuyện này. Mong phía nhà xuất bản có thể xem xét biên tập lại phần lời mở đầu của cuốn sách. Sách được đóng rất đẹp và rất nổi bật khi trưng bày trên kệ. 
5
131126
2016-04-26 08:18:14
--------------------------
212986
10455
Thế giới loài vật sinh động và chân thật hơn qua lời văn của Seton. Các hành động theo bản năng của loài vật tựa như được điều khiển bởi những suy nghĩ và sự phán đoán của chúng, làm ta thấy được có lẽ loài vật cũng có lí lẽ riêng của chúng. Cuộc đời của Lobo, chú chó nhỏ đáng yêu trung thành,.. chân thật như chính cuộc sống của một đời người. Nó sinh động và tự do biết bao. Nhưng chính những tác động của con người làm cuộc sống của chúng trở nên tiêu cực và tồi tệ hơn là trở nện tốt đẹp và an lành. Chính những hành động của con người mà những bản năng của chúng, sự thong dong, tự do của chúng thay đổi, từ đó làm mất đi vẻ đẹp của loài vật của thiên nhiên.
Cuốn sách này rất đẹp và ý nghĩa. Nhưng theo ý kiến cá nhân thì vẽ bìa như thế này không gợi tả hết được cuộc sống loài vật nhưng mặt khác tạo được sự hài hòa với nội dung các câu chuyện.
-Thân-
5
186149
2015-06-23 09:36:14
--------------------------
206949
10455
Nội dung kể về đời sống, đặc tính của các con vật rất hay và cảm động. Các con vật sống rất chung thủy với nhau, tình cảm của chúng không khác gì con người chúng ta hết! Qua câu truyện này, mình thấy động vật không những thông minh mà chúng còn gan dạ, dũng để có thể làm con vật hoang dã của thiên nhiên. Câu truyện đã làm mình yêu thương, gần gũi động vật, thiên nhiên hơn. Thế nên mình muốn cho cuốn sách này 100 sao, nhưng vì ở đây là tối thiểu 5 sao nên đành vậy.
5
508336
2015-06-11 05:52:15
--------------------------
