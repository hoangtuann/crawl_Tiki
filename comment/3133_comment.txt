491848
3133
Câu chuyện rất ý nghĩa, nhân văn. Và điều khiến mình ấn tượng nhất chính là trong quá trình đi tìm gia đình hoàn hảo, Nhím đã được trò chuyện với cả một gia đình có cặp đôi là 1 cặp bố thay vì bố và mẹ, chắc tượng trưng cho cặp đôi gay. Mình khá thích vì điều này.

Mà sao truyện của NXB Kim Đồng lúc nào cũng phiên âm tên nhân vật, dĩ nhiên cho bé dễ đọc cơ mà viết nguyên ra cũng được mà, thời buổi toàn cầu rồi cứ đọc "Ca-di", "I-an" chán chán sao á :D
4
821333
2016-11-21 13:27:42
--------------------------
381486
3133
Chuyện kể về chú nhím Ca-mi, chú không bao gio cảm thấy hài lòng về gia đình của mình dù rằng chú luôn được hết mực yêu thương cưng chiều. Chỉ vì bố không cho chú đi bắt sên , chú đã quyet định bỏ nhà ra đi. Trên đường đi, chú gặp lừa con, là con nuôi của 1 cap vợ chong ngựa. Chú gặp bạn ếch chỉ sống với mẹ đơn thân, không có bố bên cạnh. Chú gặp lợn rừng nhà, chú lợn này khác biệt so với các anh chị e của nó. Chú gặp bê con, có đến hai ông bố. Chú gặp chim nhạn sống với bác cú, vì cha mẹ chim nhạn không bao giờ ở nhà. Chú gap sói con, có đến cả chục anh chị em. Thế nhưng, tất cả điều hài lòng voi gia đình của mình. 

Nhím nhận ra , gia đình mình mới nơi hoàn hảo nhất vì minh đặt trái tim vào đó.
5
443490
2016-02-17 14:08:46
--------------------------
