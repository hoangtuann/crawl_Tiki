340210
10159
Bìa đẹp cực, nhưng hơi tối quá, nó cũng lột tả được phần nào cái tiêu đề "Ngày Tình Nhân Cuối Cùng" nó cho mình thấy đây chắc hẳn là một cuốn sách buồn, những cái kết mà không mấy ai muốn- đặc biệt là các đôi tình nhân. Để rồi nó in đậm vào tâm chí mỗi người, mình thấy tội cho họ-những con người chắc hẳn sẽ có những ngày tình nhân đầy màu hồng như như trước nữa :(
Biên tập chưa làm kĩ cho cuốn này, thấy văn phong hơi ngượng, nhưng không ảnh hưởng đến quá trình cảm thụ cho lắm :)
4
844803
2015-11-19 13:50:35
--------------------------
270300
10159
Mua vì lần đầu đọc truyện Ba Lan. Muốn tìm hiểu xem tâm tư với màu sắc văn học của truyện Ba Lan nó như thế nào. 

Tuy nhiên, đọc qua vài truyện đầu thì thấy màu sắc u ám quá. Hụt hẫng và trầm buồn là tông màu chung, giống như tựa tập truyện.

Mình chỉ có 1 số nhận xét sâu về cách dịch và biên tập thôi. Mình không tìm được thông tin nói rằng quyển này là dịch trực tiếp từ tiếng Ba Lan hay bản tiếng Anh hay tiếng nào khác. Nhưng nói chung văn phong mình cảm nhận không mấy rõ ràng. Biên tập còn sót lỗi khá nhiều và rất cơ bản đủ để nhận ra, cứ vài trang lại thấy 1 lỗi.

Hy vọng ở những cuốn sau biên tập sẽ chú ý hơn :) 
4
56847
2015-08-18 09:30:34
--------------------------
96536
10159
Ai cũng khát khao hạnh phúc và tình yêu nhưng bất đắc dĩ là tỉ lệ nghịch với sự khó khăn để đạt được chúng, việc mất đi quá ư dễ dàng và đột ngột. Mình mua tập truyện ngắn "Ngày tình nhân cuối cùng" ngay sau khi đọc mẩu truyện đầu tiên mang tên "Bất ngờ". Truyện ngắn, có một tình huống hết sức đặc sắc, chỉ trong vỏn vẹn 7 trang đã khắc họa nên một tình yêu đẹp đẽ và một bi kịch xót xa. Có người từng nói: "Con người chỉ nhìn thấy những gì họ muốn thấy." Người đàn ông trong truyện cũng vậy và kết quả là anh đã chấm dứt sinh mệnh người mình yêu, chấm dứt hạnh phúc của mình. Yêu là đẹp đẽ, khoan dung, tin cậy, nhưng yêu cũng là xấu xa, ích kỉ, nghi ngờ. Càng là thứ quý giá thì càng nên hết lòng nâng niu che chở nếu không một khi mất đi sẽ không cách nào tìm lại được nữa.
4
181945
2013-10-11 17:00:46
--------------------------
79784
10159
"Ngày tình nhân cuối cùng" có lẽ là tập truyện ngắn hay nhất mà mình từng được đọc. Nền văn học của Ba Lan vẫn còn khá xa lạ với người đọc Việt Nam, nhưng chỉ qua tác phẩm này thôi cũng đủ để chúng ta hiểu thêm một phần nào đó về nền văn học này. Những câu chuyện trong "Ngày tình nhân cuối cùng" chủ yếu mang âm hưởng buồn, ảm đạm, đôi khi là sầu khổ, nhưng cũng có những khoảnh khắc đầy vui tươi, hạnh phúc và ngọt ngào. Tất cả được diễn tả một cách chân thực và tinh tế qua mỗi trang sách, khiến cho những câu chuyện này, dù rất ngắn nhưng cũng làm cho người đọc phải xao xuyến mãi khôn nguôi.
Đẹp, buồn, sâu lắng như một khúc nhạc tình ca, tác phẩm này là một món quà dành đến cho tất cả những ai luôn khao khát tình yêu và hạnh phúc trong cuộc đời mình.
4
109067
2013-06-09 12:53:27
--------------------------
47413
10159
Mình đã chọn đọc cuốn sách này vì cái tên rất lãng mạn, và vì mình cũng muốn cảm nhận thêm dư vị tình yêu của những người châu Âu. Cuốn sách thực sự mang những màu sắc rất phương Tây, hiện đại và trẻ trung, nhưng cũng gần gũi và thân thuộc với mọi độc giả. Từ những điều nhỏ nhặt, vụn vặt nhất trong đời sống hàng ngày, cuốn sách đã vẽ nên một bức tranh chân thực và sống động về tình yêu, với mọi cung bậc cảm xúc khác nhau. Cuốn sách dường như có thể chạm vào mọi ngóc ngách trong tâm hồn người đọc, làm họ đồng cảm, yêu thích và say mê.
Đây chính là những gì tuyệt vời nhất về tình yêu trong cuộc sống của chúng ta!
4
20073
2012-11-23 16:44:37
--------------------------
26426
10159
"Ngày Tình Nhân Cuối Cùng" - một tựa sách gợi cho tôi cảm nhận bi thương, hụt hẫng, và một dự cảm không mấy tốt đẹp về số phận và cuộc đời của mỗi con người trong truyện - mà ở đây tôi mạn phép không tiết lộ để giữ trọn nội dung câu chuyện :")

16 truyện ngắn - 16 cuộc đời, 16 trải nghiệm :") Vui có, buồn có, ngọt ngao có, đau đớn và nước mắt thì lại là thứ không thể thiếu :") Đọc xong cuốn sách, ta hiểu đươc "Chung quy con người vẫn là con người, dù là ai, dù ở đất nước nào thì vẫn biết đau đớn, tủi cực, và len lỏi hạnh phúc vào những ngày cuối đông :") 16 câu chuyện như dòng suối nhỏ bé len lỏi vào trái tim ta, làm trái tim ta như bừng tỉnh bởi trải nghiệm ngọt ngào mát mẻ và ướt đẫm của nó.

Hi vọng mọi người cũng thích cuốn sách như tôi ^^
5
38398
2012-05-08 20:59:42
--------------------------
