559281
4478
Tôi là một fan Manchester United. Với tôi Sir Alex là huấn luyện viên vĩ đại nhất mọi thời đại. Thật sự rất muốn có cuốn sách này để hiểu hơn về Sir và quá trình thành công của ông.
5
5133596
2017-03-30 14:14:02
--------------------------
552569
4478
Thật sự mà nói tiki.vn quá chuyên nghiệp. Mình rất hài lòng.! Còn nội dung Sách thì mình cũng đã đọc online qua rùi, mình mua để đọc cho con mình nghe thui. kaka
5
4715326
2017-03-24 09:10:35
--------------------------
488614
4478
Là một fan trung thành của Quỷ Đỏ nên tôi quyết định mua đọc mà không nghĩ ngợi. Đúng là một quyển sách tuyệt hay được viết bởi huấn luận viên Sir Alex vĩ đại. Có những bí mật đã được hé lộ  khi được Sir kể lại và còn  nhiều sự thật rất thú vị, bất ngờ khác. Bìa sách cứng cáp cầm đọc  đã tay thiệt, có điều đọc lâu hơi bị mỏi tay :)
5
439447
2016-11-03 22:07:42
--------------------------
472751
4478
Sir Alex Ferguson là một chiến lược gia vĩ đại của bóng đá thế giới. Ông đã gặt hái được tất cả những danh hiệu cao quý trong sự nghiệp huấn luyện của mình cùng với câu lạc bộ giàu truyền thống nhất nước Anh - Manchester United. Tôi là một Fan của Quỷ đỏ thành Manchester và của HLV Sir Alex Ferguson, tôi rất hào hứng khi sách Hồi Ký Alex Ferguson đã có bản dịch tiếng Việt và đã đặt mua. Sau khi đọc sách Hồi Ký Alex Ferguson tôi có một số nhận xét như sau: Nhìn chung sách trình bày bìa đẹp, trang nhã, chất lượng giấy ổn. Tuy nhiên bản dịch còn theo kiểu "word by word", một số trang in lỗi và còn lỗi chính tả.
4
1383131
2016-07-09 22:42:01
--------------------------
470368
4478
Không chỉ là một HLV bóng đá, Alex Ferguson còn được tôn vinh như một nhân cách lớn, một nhà lãnh đạo tài năng với ánh mắt chiến lược. Khả năng tư duy, cách đối nhân xử thế, năng lực quản trị, xử lí khủng hoảng, duy trì khát vọng.... những bài học, kinh nghiệm quý giá từ sự nghiệp lừng lẫy Alex Ferguson mang đến nhiều điều bổ ích cho tất cả mọi người, ở mọi tầng lớp, ngành nghề. Tất cả những điều đó đều có thể tìm thấy trong cuốn Hồi ký đặc sắc này. Hồi ký Alex Ferguson không chỉ dành riêng cho những ai yêu bóng đá mà còn là cuốn sách nên có của tất cả những ai muốn tìm hiểu về nghệ thuật lãnh đạo, quản trị nhân sự... 

5
545501
2016-07-07 13:59:39
--------------------------
465325
4478
1 fan chân chính của Manchester United sẽ nói:  HLV Alex Ferguson  là 1 tượng đài và là 1 phần lịch sử đội bóng chúng tôi. Và chắc hẳn với bất kì ai xem Sir Alex là thần tượng, thì họ không thể bỏ qua cuốn sách này. “Hồi kí Alex Ferguson” đã đem lại tất cả những gì xung quanh con người vĩ đại này. Đó là cơ duyên đưa ông đến với Manchester United, là những năm tháng đầu đầy khó khan với 5 năm không danh hiệu, là lứa cầu thủ 1992 được ông phát hiện là đào tạo thành kỳ tài rồi cùng ông chinh phục đỉnh vinh quang, hay đó là những người mà ông yêu mến và tôn trọng. Tất cả làm nên một cuốn sách chứa đựng không chỉ là triết lý bóng đá, là cách nhìn người và dùng người, là bộ sưu tập danh hiệu và sự kính trọng của làng túc cầu, mà còn là tất cả cuộc đời của ông, cuộc đời mà ông đã chọn gắn liền với Quỷ đỏ thành Man.
Ông đã đến như một vị cứu tinh và ông ra đi khi đã là một huyền thoại.

5
517383
2016-06-30 23:20:40
--------------------------
456198
4478
Sir Alex Ferguson là vị HLV hiếm có và có thể nói là độc nhất vô nhị trong làng túc cầu giáo từ trước tới nay, ông không chỉ đem lại cho MU hàng chục danh hiệu lớn nhỏ, mà đằng sau đó còn là những câu chuyện thú vị... Cuốn hồi kí đưa ta về những ngày tháng huy hoàng nhất của MU cũng như cuộc đời huấn luyện của Sir Alex, những khó khăn cũng như thành công đằng sau chiếc cup. Những câu chuyện thú vị về các học trò siêu sao của ông cũng được đề cập trong cuốn sách, từ David Beckham cho đến C. Ronaldo, rất thú vị. Đây là cuốn sách không thể thiếu đối với bất cứ fan hâm mộ túc cầu nào không chỉ riêng fan MU!
4
112948
2016-06-23 11:42:53
--------------------------
429730
4478
Bất cứ ai có một sự quan tâm đặc biệt đến bóng đá thì đều biết đến Sir Alex Ferguson - không chỉ là huấn luyện viên xuất sắc nhất trong lịch sử bóng đá Liên hiệp Anh mà còn là một trong những huấn luyện viên xuất sắc nhất trong lịch sử bóng đá thế giới. Cuốn sách đã tiết lộ những bí mật chưa bao giờ được bật mí của Manchester United trong thời kì cầm quân của ông. Hơn hết khi đọc cuốn sách ta có thể hồi tưởng lại một thời kì lịch sử huy hoàng và tiếc nuối khi hiện tại ManU vẫn chưa thể tìm kiếm một huấn luyện viên phù hợp với mình.
5
1050097
2016-05-13 19:56:56
--------------------------
422858
4478
Nếu có thể hack mình sẽ cho cuốn này 6 sao. Thích M.U. từ năm 2006 và cũng từ đó đã thần tượng Sir. Trải qua 7 năm có thăng có trầm với đội bóng, có lẽ nốt trầm nhất chính là mùa hè năm 2013. Cuốn sách này chính là tư liệu quý giá nhất cho đến thời điềm này cho những ai muốn tìm hiểu thêm về đội bóng và HLV vĩ đại nhất của nó. Nếu bạn là fan của Quỷ đỏ thì đừng bỏ qua nhé. 
Thực ra thì có một vài lỗi chính tả, rất không đáng có với một tác phẩm như thế này, nhưng k đáng kể. Ban biên tập chú ý nhé.
5
1209819
2016-04-28 11:07:36
--------------------------
389320
4478
Mặc dù không phải là fan bóng đá, nhưng chắc hẳn cái tên Alex Furgeson là cái tên mà rất nhiều người biết đến. Tôi đọc quyển sách này không chỉ muốn tìm hiểu những mẫu chuyện về Sir hay các cầu thủ ngôi sao, càng không phải về cách cầm quân khiển tướng, mà trên hết là cách mà ông kiên trì để đạt được thành công như ngày hôm nay. Một quyển sách rất hay để đọc và suy ngẫm! Và cũng rất vui vì đã mua được quyển sách này trên Tiki, giá rẻ và chất lượng sách cũng rất tốt. Cảm ơn Tiki!
5
937260
2016-03-01 20:30:24
--------------------------
383397
4478
Tôi không phải fan bóng đá, không phải fan MU, cũng chưa từng biết đến Sir Alex. Đọc nó chỉ vì thằng bạn thích và giới thiệu quá nhiều. Thế nên xét ở phần cương vị độc giả tôi nghĩ mình cũng có thể đưa ra vài lời nhận xét khách quan. Cuốn hồi ký này của Sir Alex đã làm được điều mà mọi cuốn hồi kí phải làm được: tiết lộ những điều bí mật. Ông có thể khiến người ta bất ngờ, và tất nhiên điều đó cực kì thú vị. Dành cho những ai tò mò, dành cho những ai yêu MU, và dành cho cả những người không biết đến chủ của cuốn sách này là ai, Sir Alex đã viết được một cuốn hồi ký như vậy đấy.
4
278389
2016-02-20 18:02:17
--------------------------
382679
4478
Hồi trước lúc cuốn này mới ra cứ tự nhủ để đi mua mà rồi quên mất luôn, nay thấy Tiki có đợt giảm giá lớn (điển hình là cuốn này giảm đến tận ba mươi phần trăm) thì vào hốt ngay. Về phần cuốn sách thì viết rất chi tiết, đủ tất cả các mốc thời gian của Sir từ khi mới đến MU cho đến lúc từ giã sự nghiệp, từ những huyền thoại ban đầu như Roy Keane, Eric Cantona, David Beckham,...  cho đến những tài năng trẻ bây giờ như Wayne Rooney, Ronaldo,... Tất cả những con người vĩ đại ấy đã được Sir nhắc đến với tất cả sự tôn trọng. Một lần nữa cảm ơn Tiki đã mang đến cuốn sách tuyệt vời này. Glory, Glory Man United.
5
534442
2016-02-19 14:29:20
--------------------------
375327
4478
Cuốn này mình không mua ở tiki vì lúc đó hết hàng. Cảm giác khi đọc sách là hơi thất vọng vì những lỗi sai tên người và đôi chỗ dịch không chuẩn. Hy vọng bản tái bản sẽ sửa chữa được các lỗi này. Về nội dung thì trên cả tuyệt vời vì mình là fan của Manchester United  gần 20 năm rồi và rất hâm mộ Sir Alex tuy nhiên có 1 vài quan điểm của Sir Alex mình không đồng tình cho lắm. Một cuốn sách bổ ích cho những ai có mong muốn theo nghề huấn luyện.
5
934783
2016-01-28 11:03:13
--------------------------
360152
4478
Biết đến quyển sách từ khá lâu nhưng bjo mới tìm được địa chỉ tin cậy như thế này để mua, sẽ còn ủng hộ tiki nhiều hơn nữa trong thời gian tới. Đang mong chờ 1,2 cuốn sách nữa nhưng đang hết hàng, mong rằng sẽ có sớm để có được trên tay những cuốn sách hay và tuyệt vời như thế. Quyển sách không chỉ cung cấp cho ta những kiến thức và hiểu biết về cuộc sống của Sir trong suốt chiều dài lịch sử ông gắn bó cùng ManUnited mà còn cho ta hiểu rõ hơn nhiều điều về cuộc sống và con người của vị cha già đáng kính trong cuộc sống hàng ngày không phải về bóng đá.
5
1030031
2015-12-28 20:05:24
--------------------------
356505
4478
Nói thẳng ra mình không phải fan MU nhưng không hiểu sao MU lại thu hút sự chú ý của mình nhiều hơn hẳn so với rất rất nhiều các đội bóng nổi tiếng khác. Và danh sách tên các huấn luyện viên bóng đá mình biết cũng chỉ duy nhất có tên của Sir Alex. :v Mình mua cuốn sách này tặng cho học sinh mình đi dạy gia sư, vừa là quà sinh nhật, vừa kỷ niệm em nó. Thằng bé sướng phát cuồng cười không ngớt. :) Mình không ham thú đá bóng nhưng mình biết Sir Alex là người thầy Vĩ đại của MU, đã đưa MU lên những đỉnh cao. Đọc một vài mẩu chuyện trong sách, thấy đúng là đã bỏ tâm cho một điều gì, người ta cũng sẽ làm bằng được. Một cuốn sách những người yêu MU nên có. :) Đồng thời cũng cảm ơn Tiki nhiều nha, mình mua trúng đợt giảm giá nên càng vui tợn. :D
4
922382
2015-12-21 23:17:41
--------------------------
323181
4478
Huấn luyện viên huyền thoại, thần tượng của đội bóng mình thức đêm dài dài. Đọc qua hồi ký mới thấy vì sao ông ấy trở thành huyền thoại. Con người với cá tính cực mạnh, quyết đoán hiểu tâm lý và thế mạnh của từng cầu thủ. Ông dám đưa những cầu thủ trẻ tuổi ra sân để có được một "thời" MU. Ông từng bị chỉ trích không thể thành công với đám trẻ đó, nhưng sự quyết đoán, kiên định không thay đổi đã làm nên tên tuổi của ông. Ông "máy sấy"  đúng hiệu nóng tính, chửi thẳng mặt cầu thủ. Ông thẳn tính và nhìn nhận mọi việc ở một khía cạnh khác. Đọc hồi ký của ông để có thể nhìn vấn đề ở một góc nhìn thẳng thắn hơn!
5
466316
2015-10-18 00:03:43
--------------------------
320446
4478
Tôi thật sự rất thích quyển sách này. Sir Alex Ferguson - một huấn luyện viên huyền thoại không chỉ của Manchester United mà còn của cả thế giới. Ông đã từng từ chối dẫn dắt Arsenal và Tottenham để về dẫn dắt M.U, khi ấy chỉ là đội bóng xếp gần bét bảng tại Anh. Và cũng chính vì thế, ông đã dẫn dắt M.U đi hết vinh quang đến vinh quang khác và trở thành đội bóng thành công nhất trong kỷ nguyên Premier League. Cuốn sách này được ông viết khi ông đã giải nghệ, nói về cuộc đời huấn luyện viên của ông, về những đồng nghiệp, những học trò của ông một cách rất sâu sắc. Tôi rất hâm mộ người đàn ông này !

5
741108
2015-10-11 11:02:09
--------------------------
311897
4478
Một quyển hồi ký cần có của mọi fan của Manchester United. Đọc sách chúng ta sẽ biết được rất nhiều chuyện "thâm cung bí sử" trong nội bộ đội bóng ManU. Những suy nghĩ của Sir Alex Ferguson về những cầu thủ được coi là biểu tượng của câu lạc bộ như Cantona, Roy Kean, Beckham, Rio Ferdinand, Rooney..., về đối thủ đặc biệt Mourinho, về những quyết định mang tính lịch sử của ông. Bìa sách trình bày giống bản gốc, chất lượng giấy rất tốt, cầm nhẹ tay và đọc lâu không mỏi mắt nên mình rất thích. 
5
151280
2015-09-20 12:44:54
--------------------------
308816
4478
Alex Ferguson là vị huấn luận viên gắn bó 27 năm cuộc đời với Man United. Là fan trung thành của quỉ đỏ, đồng thời cũng cực kì yêu quí vị huấn luyện viên này nên tôi cực kì phấn khích khi đặt mua.Quả thật tiki không làm tôi thất vọng về khoản giao hàng thần tốc của mình.
Cuốn hồi kí là những câu chuyện được chính ông kể về quá trình tìm kiếm tài năng ở CLB, cũng như cách ông đã tạo nên một quỉ đỏ như ngày hôm nay. Đọc nó càng cảm thấy khâm phục người đàn ông đáng kính trọng này.
Bản thân tôi nghĩ đây là một cuốn sách đáng mua.
4
574248
2015-09-18 21:08:06
--------------------------
280274
4478
Tôi là một fan trung thành của Manchester United, chính vì vậy, khi cuốn hồi ký này của Sir Alex Ferguson được dịch ra tiếng Việt, tôi đã đặt mua ngay không ngần ngại. Cuốn sách là những bước thăng trầm trong suốt sự nghiệp huấn luyện của Ngài, vui có, buồn có, thành công vô số nhưng cũng không ít lần thất bại. Cuốn sách làm tôi thêm khâm phục vị cha già đáng kính mà các fan của Manchester United sẽ không bao giờ quên. Tuy nhiên, tôi thấy bản dịch của cuốn sách này chưa thật sự ổn, nhiều câu còn khá lủng củng gây khó hiểu cho người đọc cũng như một vài lỗi chính tả . Dù sao thì đây vẫn là một cuốn sách phải có đối với những ai yêu mến bầy Quỷ đỏ!   
4
253094
2015-08-27 15:45:50
--------------------------
271105
4478
Không quá khó để nhận định, hồi ký của Alex Ferguson chắc chắn sẽ là tập tự truyện bán chạy nhất nhì của giới bóng đá. Bởi lẽ, trong sự nghiệp gần 40 năm cầm quân của mình, ngài “máy sấy tóc” đã đi qua không biết bao nhiêu sự kiện quan trọng của làng thể thao, dìu dắt những huyền thoại bóng đá mà chúng ta biết đến ngày nay.
Tương tự như cách cầm quân của Ferguson, quyển hồi ký được viết bằng giọng văn thẳng thắn, đầy cảm xúc, không ngại “va chạm”. Kể cả khi nhắc đến những đối thủ truyền kiếp trên sân cỏ, hay những kỷ niệm tiêu cực được báo chí lúc bấy giờ bới móc… ngài Ferguson vẫn luôn giữ nguyên giọng văn cá tính như thế. Tất nhiên, trong một số hồi ức Sir Alex Ferguson vẫn thể hiện sự hối tiếc, nặng lòng trước một vài quyết định trong quá khứ của mình, đặc biệt là những trường đoạn liên quan đến các học trò thi đấu không thành công hoặc chuyển nhượng sang đội bóng khác vì mâu thuẫn. Nhưng, những khoảng trầm ấy mới chính là yếu tố cần thiết để làm nên một cuốn hồi ký.
Cầm trên tay tập sách dày gần 350 trang này, điều đầu tiên gây thiện cảm với tôi là bìa sách được in đẹp, gần như giống hoàn toàn với ấn phẩm nguyên gốc
4
712895
2015-08-18 20:58:10
--------------------------
263773
4478
Sir Alex Ferguson - Người cha già của đế chế mang tên Quỷ đỏ Thành Manchester. Dù là fan, hay kể cả có là anti-fan của MU, dù bạn có ghét bất kì thành viên nào của MU, thì có 1 con người luôn xứng đáng để bạn kính trọng, để bạn học tập... và đó không ai khác chính là Alex Ferguson. Dưới sự dẫn dắt tài ba của mình, Ngài đã đưa một đội bóng đi từ khó khăn chồng chất lên đến đỉnh cao danh vọng, gặt hái mọi thành công mà một CLB bóng đá có thể vươn tới.
Trong cuốn hồi kí này, ông đã cho chúng ta thấy một phần không nhỏ về ý chí, về nghị lực, về những khó khăn chồng chất mà ông đã trải qua cùng đội bóng con cưng thân yêu. Cũng như khi ông vui sướng tột độ đón nhận những chiếc cúp vô địch cùng đội bóng. Đã là fan của MU, bạn sẽ không thể bỏ qua tác phẩm này.
Về hình thưc, sách được thiết kế đơn giản với 2 màu chủ đạo Đỏ - Trắng, rất phù hợp với tính cách giản dị và đầy nội lực của chính Ngài Alex. Chất lương in tốt, giấy tốt.
5
489671
2015-08-12 20:33:06
--------------------------
263240
4478
Thật tuyệt vời khi được cầm trên tay cuốn sách này, cảm ơn Tiki rất nhiều. Về hình thức đẹp, mẫu mã bìa, giấy đều rất ưng ý, nhưng nếu mấy bức ảnh bên trong là ảnh màu thì tốt biết mấy. Còn nội dung thì khỏi phải nói rồi, đọc cuốn sách này giúp tôi hiểu rõ thêm rất nhiều điều về cuộc đời, những sách lược tài tình của Sir Alex Ferguson, những điều thú vị mà tôi chưa từng biết bên trong MU. Nói tóm lại là fan bóng đá tôi khuyên bạn nên mua cuốn sách này, chỉ khi cầm trên tay và đọc bạn mới hiểu được hết giá trị của nó.
5
251229
2015-08-12 14:05:27
--------------------------
260312
4478
Là fan ruột của đội bóng Quỷ Đỏ từ rất lâu, khi còn là một cậu bé, mình đã thích, yêu và hâm mộ cuồng nhiệt đội bóng này, lớn lên một chút thì tìm hiểu kỹ hơn về đội bóng mình yêu thích. Là fan của Man United mà không biết đến huấn luyện viên Alex Ferguson thì thật thiếu sót vì chính ông là người mang lại thành công nhiều nhất cho Man United. Đọc qua cuốn sách, xem qua hình ảnh làm bao nhiêu ký ức với bóng đá, với Man United từ bé của mình quay về, ngưỡng mộ ông, yêu mến ông và đội bóng nhiều hơn nữa. Bên cạnh bóng đá, mình càng mến mộ ông vì ông có cuộc sống gia đình êm ấm, hạnh phúc và nhiều sở thích thú vị như mình.
5
158753
2015-08-10 12:03:58
--------------------------
253416
4478
Cuốn sách là những hồi ký đã qua của người cầm quân vĩ đại của bóng đa Anh, của manchester united. Đọc tác phẩm không chỉ hiểu biết thêm về ngài Alex Ferguson, hiểu thêm về bầy quỷ đỏ mà còn giúp ta biết được những triết lý, những cách ứng sử trong cuộc sống. Mặc dù ngài Alex ferguson đã nghỉ hưu nhưng ngài mãi là một tượng đài không thể thay thế trong lòng người hâm mộ bóng đá cũng như nhưng người đã và đang yêu mến manchester united. Mãi mãi một tình yêu với bầy quỷ đỏ. 
5
724481
2015-08-04 14:10:42
--------------------------
248911
4478
Sir Alex đã không còn ngồi trên chiếc ghế huấn luyện của MU nữa, không còn được thấy ông nhai kẹo cao su như một thói quen nữa nhưng cuốn sách này đã đưa tôi về những miền ký ức đẹp ấy. Từng trang sách là từng kỷ niệm, tấm lòng của vị thuyền trưởng đáng kính và cũng có phần nghiêm khắc đã bộc bạch hết cho những ai trót một lần gia nhập hội yêu mến Quỷ Đỏ. Không những cuốn sách này là quà tặng có giá trị chỉ riêng cho fan MU mà còn cả những tín đồ bóng đá gần xa nữa! Vì Sir Alex đã trở thành một tượng đài mà ai đam mê túc cầu đều phải biết và ngã mũ cuối chào.
5
593589
2015-07-31 11:07:41
--------------------------
241493
4478
Nếu là một fan MU, dù không đọc sách hay mọt sách, trên kệ của bạn cũng không thể thiếu cuốn này được. Mình đã mua tổng cộng 4 cuốn Hồi kí Alex Ferguson để dành tặng anh, em, bạn bè những người thân của mình mà là fan quỷ đỏ. Khỏi phải nói nhìn thấy cuốn sách, ai cũng nâng niu như báu vật, đúng là fan cuồng bóng mà. Quyển sách rất hay, cho ta khái quát về cuộc đời và những suy nghĩ rất thật của vị huấn luyện viên này. Sách thiết kế tông màu chủ đạo trắng đỏ, đơn giản nhưng đẹp và ý nghĩa, giấy cũng đẹp.
5
393545
2015-07-25 15:26:37
--------------------------
237700
4478
Quyển sách này đã khắc họa chân dung của Sir Alex Ferguson, là người cha, người thầy, người lãnh đạo, là bố già của MU, đội chủ sân Old Trafford suốt 27 năm; là người đã đưa MU từ 1 câu lạc bộ bình thường, bậc trung đến một trong những đế chế vững mạnh nhất của nước Anh và châu Âu; người đã làm nên những điều kì diệu khi đưa một đội hình không quá nhiều sao giành được quá nhiều danh hiệu. Quyển sách ghi lại những suy nghĩ rất thật của Sir, về clb, về Ronaldo, Rooney, Beckham, Rio... về cách ông lãnh đạo đội bóng, cho thấy ông yêu MU như thế nào và MU đã trở thành một phần máu thịt của ông ra sao... Đáng mua, đáng đọc đối với tất cả fan của MU cũng như anti-fan MU
5
62433
2015-07-22 17:27:27
--------------------------
230897
4478
Biết đến cuốn này vì đọc báo Bóng Đá và quyết định mua nó vì là fan ruột của Sir Alex Fegruson. Sách cho ta biết được những bí mật bên trong "Nhà hát của những giấc mơ", về sự ra đi đầy bí ẩn của Beckham, Ronaldo,.. Viết về một thế hệ 92 của những Giggs, Scholes,anh em nhà Neville,.. đã tạo nên cú ăn ba thần thánh vào năm 1999 và biến M.U trở thành một trong những CLB nổi tiếng nhất hành tinh. Sách được in ấn đẹp đặc biệt có nhiều hình ảnh màu đẹp . Là Fan M.U rất thích quyển này 
5
454116
2015-07-17 20:16:16
--------------------------
219070
4478
Tác phẩm Hồi ký Alex Ferguson như một đoạn phim chiếu chậm lại toàn bộ quãng thời gian cầm quân của Ngài Alex Ferguson qua xuyên suốt sự nghiệp huấn luyện viên của Ngài với bao thăng trầm, biến cố, phong ba bão táp, cũng như các cung bậc cảm xúc tột đỉnh của các chiến thắng vang dội cùng với đội bóng Manchester United. 
Đọc tác phẩm giúp ta cảm nhận được cái nhìn sâu sắc của Ngài Alex cũng như quan điểm cá nhân được bộc lộ một cách thẳng thắn về cầu thủ, ban lãnh đạo câu lạc bộ MU, các đối thủ, các huấn luyện viên đối đầu trong suốt sự nghiệp cầm quân của Ngài. Bên cạnh đó cách nhìn về gia đình, gốc rễ của mọi thành công, cũng đã được Ngài đề cập đến.
Sách được trinh bày bìa đẹp mắt, nội dung hay và được dịch thuật chỉnh chu mặc dù vẫn còn một số ít lỗi chính tả. Dù có là cổ động viên của Manchester United hay không thì đây vẫn là một tác phẩm xứng đáng để các bạn đọc, đặc biệt là các bạn đọc yêu thích môn bóng đá, trải nghiệm và đánh giá.
5
618799
2015-07-01 08:58:06
--------------------------
202478
4478
Những tò mò bất tận. Đó là cảm giác của tôi khi lần đầu thấy cuốn sách này xuất hiện, đặc biệt khi Nxb Trẻ phát hành bản tiếng Việt. Tôi đã đợi quá lâu để mua nó và đợi thêm... 5 ngày để Tiki chuyển sách cho tôi kể từ khi ấn nút mua hàng!

Những câu chuyện dc kể trong tác phẩm này làm thỏa mãn trí tò mò và những câu hỏi thắc mắc trong nhiều năm qua của các fan MU. Mối quan hệ với các đồng nghiệp của Sir Alex, mối quan hệ của ông với các hảo thủ. Tiêu biểu như vụ "tống cổ" Beckham, Roy Keane...

Sẽ rất tuyệt nếu bạn đọc cuốn sách này cùng với cuốn "Thiên sử về Quỷ đỏ" do Alphabooks phát hành!

Hồi ký Sir Alex dc in ấn rất tốt, bìa đẹp và nhiều ảnh minh họa. Với fan MU, đây có thể ví như một "báu vật"!
5
598102
2015-05-29 18:39:08
--------------------------
202256
4478
Tôi là một fan hâm mộ của quỷ đỏ thành Manchester cho nên không thể không đón đọc quyển hồi ký của vị huấn luyện vĩ đại nhất trong lịch sử câu lạc bộ. Alex Ferguson đã mang tới cho chúng ta tầm nhìn xa trông rộng, cá tính mạnh mẽ và trên tất cả là tình yêu mà ông dành cho bóng đá và Manchester United qua những trang sách mà theo tôi nghĩ có lẽ ông đã dành rất nhiều tâm huyết để viết lên. Đối với ông, một cầu thủ có tư tưởng nổi loạn không thể nào tồn tại ở Manchester United, dù đó có là Roy Keane, David Beckham hay Cristiano Ronaldo. Cách viết tự truyện của ông đã phần nào nói lên điều ấy.
 Hình thức của quyển tự truyện này cũng được làm rất tỉ mỉ .Sách được thiết kế khá bắt mắt với tông màu trắng chủ đạo và giấy xốp cũng rất thơm.
5
313837
2015-05-29 07:01:29
--------------------------
201543
4478
sách nói về Sir người cha già của fan manu. đến với mu từ năm 1999 từ cú ăn 3 đó đã gây ấn tượng rất nhiều với tôi. kính trọng tài năng của nhà huấn luyện , nhà quản lý tài ba Sir .cuốn sách nói về các thâm cung bí xử cũng như cuộc đời huấn luyện của ông. sách có đề cập đến vụ chiếc giày bay của beckham hay cú đá cổ động viên của catona. lúc mềm lúc mỏng thật sự ông không chỉ tài năng về chiến thuật, nhìn người , phát triển tài năng mà ở đó còn là nhà quản lý giỏi có khí độc tài của mình. 26 năm với mu ông đêm lại nhiều danh hiệu và vượt mặt Liv để thành câu lạc bộ giàu thành tích nhất nước anh hiện nay. cuốn sách viết rất chi tiết và đầy đủ quãng thời gian ông làm huấn luyện viên cho đến khi ông nghỉ hưu
5
642439
2015-05-27 17:19:47
--------------------------
200615
4478
Mình đã mua cuốn sách này ở 1 hội chợ diễn ra tại bảo tàng phụ nữ, đang chán nản vì đi ngày cuối nên ít sách và bất chợt thấy cuốn này, mình như kiểu bắt đc vàng :))
Cuốn sách là cuốn hồi ký của sir về những điều đáng nhớ khi còn dẫn dắt MU. Ở cuốn sách bạn sẽ khẳng định đc rằng hành động tay của sir run trong trận chung kết với Bar là do sợ hãi, chứ không phải do lạnh "Tôi lo lắng vì chúng tôi đã bị họ phong tỏa toàn diện!". Và bạn sẽ thấy đc sự tiếc nuối trong 2 trận chung kết với Bar của sir và sir đã dùng từ "giá như", và ko chỉ mình ông tiếc mà tất cả fan MU đều tiếc.
Trong truyện có chương dành riêng cho Kean, Beck, van Nistelrooy... nhưng lại ko có chương dành riêng cho gigg hay schole, đơn giản là ông đã có chương dành riêng cho lứa cầu thủ 92 và khi đọc nó bạn sẽ hiểu đc ông yêu họ đến mức nào, và khi đã yêu rồi thì không cần phải nói nhiều.
Tóm lại, qua cuốn hồi ký các bạn sẽ có đc rất nhiều thông tin bổ ích, và nó rất đáng có trong tủ sách của fan MU nói riêng, fan bóng đá nói chung!!!
5
387240
2015-05-25 15:19:41
--------------------------
198962
4478
Mình là một fan trung thành của MU từ khi còn là một cô bé. Nên cũng không có gì là lạ khi sinh nhật vừa rồi mình được chị gái tặng cuốn sách này. Chắc có lẽ những ai hâm mộ MU đều không thể không biết đến người thầy huyền thoại ấy người đã giúp MU trở thành một đội bóng lịch sử của bóng đá Anh. Tuy nhiên, không phải những gì về Sir chúng ta đều biết được, nhờ quyến sách này, mình đã biết và hiểu thêm về con người huyền thoại ấy. Biết được khả năng dẫn dắt và những nguyên tắc mà ông đã đề ra cho các cầu thủ của mình. Cuộc sống xung quanh ông thật khiến  chúng ta tò mò, và nhờ tác phẩm mà nó dần dần được hé lộ. Tác phẩm là một câu chuyện hay, hấp dẫn và làm cho mình cảm thấy yêu quý và khâm phục hơn nữa vế ông.
5
450212
2015-05-21 10:23:19
--------------------------
192076
4478
Ngay từ hồi còn nhỏ, mình đã yêu mến MU, lại càng khâm phục và ngưỡng mộ Sir Alex. Trong lòng mình, ông là một tượng đài bất hủ. Đọc cuốn sách này, mình càng yêu quý, cảm phục ông hơn. Mình biết đến ông từ ngày bé khi mới 9 tuổi. Từ đó mình rất ấn tượng với  ông, quen với hình ảnh ông nhai kẹo cao su trên khán đài. Đọc cuốn sách mình có cảm giác như được nói chuyện trực tiếp với Sir, một thứ gì đó rất nhẹ nhàng, mộc mạc và tình cảm. Mình hiểu thêm về cuộc sống của Sir, hiểu hơn về gia đình và những người bạn của Sir. Mình cũng hiểu về mối quan hệ của Sir với các cầu thủ. Sir đã rất cố gắng để giữ vững sự yên ổn trong phòng thay đồ. Cuốn sách rất hữu ích đối với người hâm mộ Sir và câu lạc bộ Manchester United. Cảm ơn Tiki đã giúp mình và nhiều người khác nữa có được cuốn sách này
5
384088
2015-05-03 15:50:59
--------------------------
182368
4478
Sau khi đọc xong cuốn "Thiên sử về Quỷ Đỏ" thì mình ngóng chờ cuốn này được  dịch ra Tiếng Việt vì bản Tiếng Anh giá chát quá.Khi đọc trên báo thông tin quyển hồi kí này được biên dịch ra Tiếng Việt thì mình lên Tiki đặt hàng ngày. Đọc quyển sách này sẽ cho chúng ta có được một cái nhìn khác về Sir. Một cái nhìn khác với những gì báo chí thuê dệt. Từ những ngày đầu khó khăn cho tới những năm vinh quang cùng Manchester United. Cuốn sách còn cho ta thấy những mối quan hệ giữa Sir và cầu thủ cũng như những sở thích đời thường rất giản dị. Đây là cuốn sách hay cho những người quan tâm về bóng đá cũng như về Manchester United.
5
467813
2015-04-13 22:11:49
--------------------------
178971
4478
Chỉ cần nghe tựa đề là mình đặt hàng ngay. Tiki giao hàng rất nhanh và trong 1 buổi tối mình đã đọc hết những mẫu chuyện trong đó. Những mẫu chuyện dường như đơn giản, có thể bạn đã từng nghe qua nhưng chắc chắn rằng đó chỉ là những mẫu chuyện thêu dệt của báo chí, về những mẫu chuyện của beckham, catona, mourinho, rooney, rio...thực sự khiến những fans hâm mô cảm động. Cái cách mà " lão già" quản lý đội bóng lúc nó cực đoan, nhưng ông luôn đảm bảo đội bóng của mình đi đúng hướng. Với đội ngũ tài năng ở Manchester từ người quản lý trang phục đến con trai của ông - Darren, đến David Gill - Giám đốc điều hành....Manchester United trở thành đội bóng giàu thành tích nhất ở PL.
Fans hâm mộ Mu nên mua cuốn này về trải nghiệm!
5
482909
2015-04-06 10:11:40
--------------------------
175613
4478
Ngay từ hồi còn nhỏ, mình đã yêu mến MU, lại càng khâm phục và ngưỡng mộ Sir Alex. Trong lòng mình, ông là một tượng đài bất hủ. Đọc cuốn sách này, mình càng yêu quý, cảm phục ông hơn. Cuốn sách với giọng văn rất giản dị, nhẹ nhàng xen chút hóm hỉnh, ông đã cho mình thấy lại một thời huy hoàng của Quỷ Đỏ, những câu chuyện không chỉ trên sân bóng, cho mình biết những ngôi sao mà mình hằng ngưỡng mộ đã thành tài ra sao, cho mình thấy Sir có tầm nhìn xa trông rộng, có tài, có tâm như thế nào. Mua cuốn sách này về quả thật không hề hối tiếc.
5
449880
2015-03-30 17:40:31
--------------------------
175413
4478
Dù bạn có thích Manchester United hay không, chỉ cần thích bóng đá một chút thôi, bạn cũng sẽ biết đến Sir Alex. Ông ấy là một tượng đài. Nhưng dù có vĩ đại đến đâu cũng là con người. Tự truyện của ông cho ta thấy những góc sau sân cỏ, nơi vinh quang và thất bại ám ảnh không chỉ vì sự hâm mộ mà vì ta phải đối mặt với chính bản thân ta. Những gì Sir Alex viết là điều đáng suy ngẫm cho những chiến lược gia, những cầu thủ, và cho cả những người đang sống trong thế giới khá nhiều hư vinh.
Một điểm cộng cho quyển sách này là bản dịch khá tốt.
4
187500
2015-03-30 09:43:21
--------------------------
175125
4478
Anh tôi là rất yêu Manchester United. Anh ấy cũng xem Alex là thần tượng. Vì thế tôi quyết định mua tặng anh. Vì tò mò tôi đọc thử. Thật không thể ngờ ngòi bút tinh tế ấy là của một huấn luyện viên. Một người có tâm vừa có tài đã gầy dựng câu lạc bộ của ông trở thành huyền thoại. Cuốn sách không chỉ giúp bạn hiểu hơn về Alex mà còn khắc họa tài tình những gì mà huấn luyện viên phải đối mặt. Cuốn sách sẽ mang đến những xúc chiến thắng vở òa trong chính chúng ta. Ông không chỉ là một huấn luyện viên mà còn là người cha của đội bóng. Alex là một huyền thoại trong giới túc cầu.
5
584394
2015-03-29 14:46:45
--------------------------
175008
4478
Hồi ký Ferguson là quyển sách nói về người cha , người thầy, 1 chiến lược gia vĩ đãi của bóng đá thế giới. tôi yêu ông về tính cách , các danh hiệu ông đem về cho đội bóng, cách xây dựng và quản lý nhân tài..  và chính xác hơn yêu ông từ cái cách ông đã vực dậy 1 manu kiệt quệ, khô cứng liên tục thua trận . ngày 6 tháng 11 năm 1986 đánh dấu triều đại vĩ đại của ngài Alex . ông mất 6 năm để đưa quỷ đỏ trở lại quỹ đạo và xác lập vị thế thống trị của mình trên bản đồ châu âu. với lứa 92 đầy tài năng cùng ông đem đến vinh quang . với đó tính cách của ông cũng rất được kính trọng : không 1 ai trong đội bóng dù có siêu sao cỡ beckham hay k16 thì cũng không bằng tập thể quỷ đỏ thành manchester .. nếu bạn muốn tìm hiểu về manu hay là fan manu thì tôi nghĩ quyển sách hồi kí này sẽ rất có ích cho các bạn.. đặc biệt là những bạn sinh viên có niềm yêu thích Manu  . với giá cả và cách phục vụ của tiki tôi nghĩ các bạn sẽ hài lòng ... cảm ơn, I Love Manu , I Love Tiki
5
595984
2015-03-29 10:50:58
--------------------------
173994
4478
Sau bao ngày chờ đợi, hóng hớt mãi cuối cùng tôi cũng đặt được cuốn sách này trên tiki. Nội dung thì khỏi phải bàn rồi, qua giọng văn của Sir, thời hoàng kim của Manchester United cũng như những kỉ niệm cùng những Cantona, Giggs, Beckham, Scholes, Roy Keane, ... hiện về trong tôi. Dù bạn có yêu hay không thích Manchester United đi chăng nữa, thì tôi tin đây là một trong những cuốn sách đáng mua,đơn giản nó được viết bởi một người vĩ đại, người đã truyền tình yêu bóng đá đến với rất nhiều người trên thế giới này - Sir Alex Ferguson.
5
340213
2015-03-27 12:01:12
--------------------------
173456
4478
Ngài Alex Ferguson là một huấn luyện viên vĩ đại mà tôi rất khâm phục và ngưỡng mộ mặc dù tôi không phải là Fan của Quỷ đỏ thành Manchester. Khâm phục ông ở chỗ những danh hiệu mà ông đạt được, cách ông ươm mầm và đào tạo ra nhiều danh thủ tài năng cho làng bóng đá xứ sở sương mù và thế giới. Càng đáng ngưỡng mộ khi ông hết mực trung thành với Manchester United mặc dù được rất nhiều đội bóng khác lôi kéo và ấn tượng nhất là cách ông chỉ huy đội bóng đầy rẫy những ngôi sao. Cuộc đời huấn luyện huy hoàng của ông đã được chính ông kể lại trong quyển hồi ký này với văn phong đầy nhiệt huyết của một huấn luyên viên bóng đá huyền thoại. Không chỉ dành cho những ai yêu bóng đá, quyển hồi ký này được dành cho tất cả mọi độc giả với cách truyền cảm hứng để thành công của Alex Ferguson.
5
387632
2015-03-26 11:19:29
--------------------------
