379438
5598
Cuộc hẹn giữa Sato và Takagi đã diễn ra không như mong đợi, xảy ra quá nhiều chuyện khi buổi hẹn bắt đầu. Tiếc cho anh chàng Takagi vì không thể cầu hôn được Sato vì đã đánh mất chiếc nhẫn, mặc dù Sato không biết nhưng chắc chắn tình cảm của cô nàng đã dành hết cho Takagi. Connan quả là cao thủ khi phá án mà không cần phải nói nhưng vụ án này đã quá dễ biết ai là thủ phạm rồi. Mối tình xưa của ông tiến sĩ thật là đẹp, ông ấy rất muốn gặp lại người xưa và đã được toại nguyện. Nội dung tập này hay và không kém phần hồi hộp, đặc biệt thích lúc cánh hoa rẻ quạt rơi, nhìn thật lãng mạn.
5
13723
2016-02-11 08:59:17
--------------------------
378023
5598
Trung sĩ Takagi và thiếu úy Sato đã có một hẹn hò hay ghê cơ, dù đánh mất cái nhẫn quý giá, nhưng trong tay Takagi đã có thứ quí giá hơn rồi.
Mối tình hoa rẻ quạt và tiến sĩ Agasa đã được đội thám tử nhí giúp đỡ, dưới những  chữ số, mọi người đã tìm ra mối tình đầu của tiến sĩ Agasa! Một người phụ nữ chung thủy có vóc dáng mảnh mai, xinh đẹp lại là mối tình đầu của bác tiến sĩ Agasa và cũng là chị gái tốt bụng thời xưa của Ran, bây giờ đã là người đã làm hãng hiệu fueasa nổi tiếng của nhật . Qủa thật là người phụ nữ có đầy đủ phẩm chất tốt đẹp! (Mong cô ấy xuất hiện ở những tập sau nữa.)
5
97553
2016-02-04 06:24:49
--------------------------
356901
5598
Có nhầm lẫn gì ở đây không ?
Vụ án ma tuý ở công viên Conan phán đoán kẻ giao hàng là người thuận tay trái vì chữ bị nhoè. 
Do viết bằng tay trái tay người này quẹt vào chữ viết trước đó chưa khô hẳn nên chữ mới nhoè. 
Nhưng người Nhật viết từ phải qua trái mà, đọc truyện cũng từ phải qua trái không lẽ lại viết từ trái qua, tức là viết ý sau trước sao ?
Không hiểu chỗ này nghen hay là biên tập chưa kịp sửa nội dung, dùng bản cũ vậy ?
5
535536
2015-12-22 16:25:51
--------------------------
337669
5598
Tập này kể về cuộc hẹn giữa cảnh sát Takagi và Sato, công nhận lúc hẹn hò cô Sato trông dễ thương thật nhưng mà cuộc hẹn chả được yên vui gì cả tại mấy người trong sở cảnh sát cứ phá đám hoài. Lúc chú Takagi lấy nhẫn trong cặp ra, mình đã hi vọng chú ấy sẽ đưa được nó cho cô Sato chớ. Mối tình lá rẻ quạt của tiến sĩ Agasa đẹp thật. Cô ấy đẹp kiểu sang trọng làm sao ấy. Cô ấy thật là giỏi, tự tạo dựng được hãng thời trang nổi tiếng Fuesa cho mình và cũng là một người kiên trì, chung thủy cứ 10 năm lại đứng đợi bác tiến sĩ làm mình thấy cảm động quá.
5
952213
2015-11-14 14:09:36
--------------------------
300096
5598
Tội nghiệp Sato với Takagi ghê, đi hẹn hò mà cứ như bị truy nã vậy. Mà đến cả đi chơi cũng không được yên, cứ bị mấy người đồng nghiệp ở sở cảnh sát phá (mà cầm đầu là thanh tra Shiratori ấy, bực ghê) Còn nữa, mặc dù tiếc ơi là tiếc khi Takagi không thể trao chiếc nhân cho thiếu úy Sato nhưng Takagi lại nhận được một thứ còn đáng giá hơn thế, thật là thích cặp đôi cảnh sát này quá đi. Câu chuyện về mối tình đầu của tiến sĩ Agasha cũng dễ thương không kém, nhờ nhóm thám tử nhí mà cuối cùng ông tiến sĩ cũng gặp lại cô bé ngày xưa. Cô bé 40 năm rồi vẫn còn đợi ông tiến sĩ, hi vọng nhân vật này sẽ còn xuất hiện ở những tập sau.
5
471112
2015-09-13 18:33:46
--------------------------
295319
5598
Phần đầu kể về buổi hẹn hò của Takagi và Sato , Takagi định tặng nhăn cho cô nhưng không may bị tráo đổi chiếc balô anh đang cầm,  nhưng cũng vì vậy nhóm người Conan và Sato đã bắt được tên trùm ma túy Yukura Asakichi. Thanh tra Takagi đã làm mất chiếc cặp quý nhưng lại có trong tay một thứ đáng giá hơn... Ngoài ra kể về mối tình đầu 40 năm trước của tiến sĩ Asaga,  hẹn ông cứ 10 năm nữa lại gặp nhau ở chỗ cũ và gợi ý những con số  thế là Conan và nhóm bạn bắt tay vào giải câu đố. Và những vụ án giết người trong Conan 
5
732692
2015-09-10 09:38:18
--------------------------
286530
5598
đồng hồ đo thời gian liên tục chạy... mối tình hoa rẻ quạt khiến cho mình cảm động ghê cơ, hình như những nhân vật trong truyện đều được bác aoshô yama khắc họa cho một mối tình thật đẹp hoặc ít nhiều đều có những kỉ niệm đẹp về tình yêu, thích nhất những mối tình chờ đợi, chỉ có Ran chờ Shin thôi là khổ rồi nhưng còn nhiều người chờ đợi nữa. những kí hiệu đi tìm thôi đã đủ mệt nhưng lúc thấy được người tình của bác sĩ đứng đợi và tiếp tục đứng đợi nữa thì thật sự mình phục quá. có lẽ cô ấy 10 năm lại đến đợi tiến sĩ.
4
568747
2015-09-01 20:38:41
--------------------------
196231
5598
Tập này tuyệt đối không nên bỏ lỡ cho những ai yêu bộ đôi Sato-Takagi nhá nhá ♥ Một cuộc hẹn hò rất rất là tình củm của Takagi và Sato, Takagi thậm chí còn đem theo nhẫn nữa nhé, nhưng lại bị kì đà cản mũi là một vụ án đột nhiên xảy ra ở nơi hẹn và thanh tra Shiratori. Tập này còn có vụ án Cà ri - gọi vậy cho nhanh vì có liên quan đến cà ri ấy mà - cũng rất hấp dẫn nữa. Nhưng thấy hay nhất và đặc biệt nhất phải nói đến là mối tình đầu của tiến sĩ Augasa, mình đã bật cười không ngừng khi nghe tiêu đề đấy :))
5
530790
2015-05-15 16:08:46
--------------------------
