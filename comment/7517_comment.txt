243238
7517
Những cuốn sách độc đáo này thật sự thu hút trẻ nhỏ với hình ảnh ngộ nghĩnh, màu sắc bắt mắt vô cùng! Tôi đã mua rất nhiều truyện tranh của Phan Thị, có loại sticker dán hình trong truyện, tăng khả năng tư duy cho bé, cũng như rèn luyện sự khéo léo của đôi bàn tay bé nhỏ nữa nhưng khi tiếp tục mua thì hết hàng. Đợi thông báo có hàng của Tiki dài cổ mà chưa thấy! Hy vọng Tiki nhanh chóng cập nhật một số sách truyện tranh của Phan Thị. Đó là món ăn tinh thần không thể thiếu cho trẻ nhỏ nhà tôi! 
5
286211
2015-07-27 12:18:51
--------------------------
