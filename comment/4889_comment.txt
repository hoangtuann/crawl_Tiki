575265
4889
Mình rất thích mua những loại sách thế này cho bé nhà mình (18 tháng)

Tiki giao hàng nhanh, đóng gói cẩn thận.

Bé rất thích vì màu sắc đẹp, nhiều bài đồng dao hay, dễ đọc, dễ nhớ.

Bé nhà mình nhờ đọc những quyển thế này mà đã thuộc được rất nhìêu ca dao, vè, thơ cho trẻ em.

Ngôn ngữ dùng trong sách rất phong phú, không bị ảnh hưởng vùng miền nên rất dễ dạy cho bé.

Nhưng khuyết điểm là giấy hơi mỏng nên nhanh rách vì bé còn chưa bíêt cách giữ gìn, nên phải dán băng keo trong ở mép trang.
5
65672
2017-04-17 09:41:08
--------------------------
