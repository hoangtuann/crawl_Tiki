403279
11171
Quyển sách này ngoài việc đã trích dẫn về các văn bản Hiến pháp 1992, 2013 còn kết hợp thêm Tiếng Anh chuyên ngành. Sách rất phù hợp với sinh viên Luật như mình trong việc tìm hiểu và cải thiện vốn từ Tiếng Anh pháp lý. Tuy nhiên quyển sách này chỉ dịch sang Tiếng Anh ở văn bản Hiến pháp 2013 chứ không đưa toàn bộ các văn bản khác sang Tiếng Anh nên làm mình hơi buồn. Quyển sách nhìn chung là Ok. Tiki đóng gói cẩn thận, giao hàng nhanh chóng. Cám ơn Tiki đã cung cấp sách hay và giá cũng rẻ hơn so với Nhà sách mà lại được giao hàng tận nơi.
5
106288
2016-03-23 15:32:29
--------------------------
369411
11171
Nhiều hiến pháp được biết đến như chỉ rõ cách liên tưởng ,nắm vững những phong cách , nghiên cứu hiến pháp của việt nam từ nhiều năm trước đây , khi chưa phổ biến tiếng anh như hiện giờ thì bổ sung ngoại ngữ làm say mê với giới trẻ hơn về cách thông thạo ngoại ngữ với xã hội , phát triển đều rõ ràng , phân biệt rạch ròi , tổng thể chưa có sự sâu sắc nào vượt qua nổi cuốn có điều kiện hiểu biết của người dân , người ngoại quốc muốn phác thảo .
4
402468
2016-01-15 18:32:48
--------------------------
