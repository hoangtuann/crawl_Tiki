472936
4337
Mình đã đọc hết toàn bộ các truyện Doraemon từ truyện ngắn đến truyện dài, mỗi tập đều thể hiện khả năng thần thông xuất chúng của nhóm bạn Doraemon. Nhưng khi mua tập truyện này mình lại bất ngờ khi biết rằng còn rất nhiều điều mà trong suốt quá trình đọc các tập truyện ngắn truyện dài vẫn chưa biết. Cuốn truyện rất hay và thú vị. Nếu ai chưa hiểu rõ toàn bộ về Doraemon và nhóm bạn, hãy tìm mua tập truyện thú vị này nhé !!! Doraemon quả là cuốn truyện hay cho mọi thế hệ !                                                                                                               
5
971913
2016-07-10 08:42:23
--------------------------
419658
4337
Cuốn Đại Từ Điển Doraemon Và Những Người Bạn này khi nhận được mình khá bất ngờ. Vì cuốn sách dày quá, mà cầm thì rất là nhẹ. Là người đọc hét 45 quyển truyện ngắn và 24 cuốn truyện dài Doraemon cũng biết kha khá về từng nhân vật nhưng cuốn sách có nhiều điều mới mẻ quá mà mình chưa được biết. Rất hay và thú vị lắm. Lớn rồi đọc lại  truyện vẫn bị cuốn hút và vẫn thấy các nhân vật dễ thương quá chừng. Doraemon một thứ truyện qua tay không biết bao nhiêu thế hệ ! ^^ 
5
854660
2016-04-21 18:08:57
--------------------------
252544
4337
Mình là fan của Doraemon từ nhỏ , tự nhận là biết sạch sành sanh rồi mà vẫn cảm thấy quyển này cực kì đáng đọc. Nếu bạn nào còn chưa rõ, thì sẽ biết thêm rất nhiều điều hay ho, đáng yêu, còn như mình thì cũng nhớ lại được nhiều tiểu tiết đã từng quên.
Về phần hình thức thì bìa đẹp quá rồi, giấy cũng nhẹ tay, và mình cũng có đặt dịch vụ bookcare nên thấy long lanh hẳn.
Hy vọng nhà xuất bản xuất bản thêm nhiều nhiều sác truyện dạng như thế này nữa , là fan nên mình vô cùng hài lòng và mong đợi !!!  
5
186614
2015-08-03 19:08:03
--------------------------
226673
4337
Nhận cuốn này thì cảm thấy rất ưng ý. Có 2 bìa, bìa ngoài in màu, bìa trong trắng đen. Nội dung thì nói tất tần tật về Đoraemon, Nobita, Shizuka, Suneo, Chaien. Truyện mang tính chất giải trí, hài hước. Xem xong phần của Doraemon thì mắt chữ A, mồm chữ O. Dù biết Doraemon là robot cấp cao nhưng không ngờ là Doraemon lợi hại tới vậy (Mặc dù có nhiều tình huống trớ treo). Thực sự rất thích cuốn này ^^ Tiki giao hàng nhanh nữa, tuyệt vời ^^
5
367488
2015-07-12 19:36:50
--------------------------
184568
4337
Tôi rất thích bộ truyện Doraemon , vì vậy tôi muốn hiểu thêm về thông tin nhân vật và một một số bảo bối chính của Doraemon . 
Tôi đã quyết định mua cuốn Đại từ điển Doraemon và những người bạn để tìm hiểu thêm về họ . 
Truyện được thiết kế khá rất đẹp , giấy xốp , nhẹ . Nội dung trình bài rõ ràng từng phần về mỗi nhân vật . Tôi đặc biệt chú ý Dekisugi , cậu giống như một mẫu nhân vật nam hoàn hảo trong truyện của Fujio -sensei - thông minh , học giỏi , tốt bụng , đẹp trai và hơn hết là giỏi thế thao và thích nấu ăn . Tôi còn biết thêm một số thông tin về Shizuka và Jaien nữa ! . 
Nhìn chung đây là một cuốn truyện hữu ích cho fan của truyện Doraemon đấy !
5
335856
2015-04-17 23:33:44
--------------------------
