464899
10702
Thiếu nữ toàn phong là 1 trong số ít những tác phẩm ngôn tình mà mình thích, nếu là fan của Minh Hiểu Khê vậy thì lại càng không nên bỏ qua bộ truyện này. Truyện thì cũng vẫn theo 1 motif khá cũ về hình tượng nhân vật đó là nữ chính đúng chuẩn thánh mẫu và nam chính lạnh lùng ít nói nhưng thâm tình, cơ mà ta vẫn khá thích :3 Bởi vì truyện không chỉ theo đuổi đề tài về tình yêu nam nữ, thậm chí phải nói là ở trong đây tình yêu nam nữ không được nhắc đến nhiều mà chủ yếu là nói đến quá trình nữ chính Thích Bách Thảo từng bước trở thành tuyển thủ Taekwondo đứng trên đỉnh cao. Lại nói mặc dù khá là ghét bánh bèo nhưng mà ta chẳng làm sao mà ghét nổi Thích Bách Thảo tại vì bạn... quá là đáng yêu. Mình đã đọc gần hết các tác phẩm của MHK thì cảm thấy bộ truyện này chị đã viết khá hơn rất nhiều rồi. Nhưng vẫn mong sau này có thể có 1 tác phẩm bứt phá hơn :3
4
666736
2016-06-30 16:43:11
--------------------------
386068
10702
Đây là truyện duy nhất của Minh Hiểu Khê mà mình có thể nuốt trôi. Dù giọng văn vẫn còn sến và motip truyện thì vẫn kiểu 1 nữ 3 nam nhưng chí ít, việc tác giả lồng ghép về võ thuật vào chuyện tình khiến mình cảm thấy đỡ nhàm và có động lực đọc hơn, vì mình khá thích taekwondo. Thích Bách Thảo cũng là nữ chính duy nhất của MHK để lại ấn tượng tốt đẹp trong lòng mình bở sự nỗ lực bền bỉ, kiên trì, bởi lòng quyết tâm không gục ngã của cô ấy. Khá thích nữ chính này, còn nam chính thì mình có cảm tình với Nhược Bạch hơn cả.
4
630600
2016-02-24 23:36:07
--------------------------
337245
10702
Mình biết đến Minh Hiểu Khê qua tác phẩm Bong bóng mùa hè và thật sự rất thích tác phẩm đó, sau đó thì tìm tất cả truyện của cô về đọc. Thiếu nữ toàn phong là tác phẩm tôi thích nhất trong những tác phẩm đó. Nó cuốn hút, lôi cuốn đến từng chi tiết, làm tôi không thể đoán trước được cái kết của câu chuyện. Nó cho tôi thêm sự hứng thú với bộ môn võ thuật mà trước đây thường cảm thấy nhàm chán. Một Bách Thảo đáng yêu, một Nhược Bạch trong nóng ngoài lạnh ddefu mang lại cho tôi sự yêu thích vô cùng. Nói chúng là rất thích bộ truyện này :D
4
362963
2015-11-13 20:33:08
--------------------------
311462
10702
Minh Thiểu Khê là một nhà văn rất tài năng, nhiều truyện của tác giả này được làm phim khiến mình cảm thấy rất khâm phục . Điều khiến tác phẩm ngôn tình này đặc biệt hơn nữa là việc bà đan xen thông điệp gửi tới giới trẻ là hãy sống hết mình , hãy thỏa sức đam mê , hãy cháy hết mình để thể hiện bản lĩnh như nhân vật Thích Bách Thảo . Nhờ có sự đam mê cô nàng đã chinh phục được rất nhiều người về khả năng của mình cũng như chiếm được trái tim của anh chàng Nhược Bạch sư huynh. Đây là 1 cuốn tác phẩm rất thú vị
5
781966
2015-09-19 23:19:31
--------------------------
268942
10702
Thiếu nữ toàn phong hiện đã có bản truyền hình, nên chắc nhiều người biết đến hơn rồi. Hồi đầu nhìn thấy bộ này có chút nản, vì nó... quá nhiều đi. Suy nghĩ đầu tiên là "Minh Hiểu Khê thật khỏe viết" Nhưng vì đang rảnh nên mình vẫn đọc, cũng vì tin tưởng tác giả một phần nữa. Không ngờ đọc mà bị cuốn hút đến vậy, không dứt ra được, có hôm thức đến sáng chỉ để đọc cho xong một tập. Cá nhân mình vẫn thích Nhược Bạch nhất, chắc tại mình thích mẫu lạnh lùng! 
4
512077
2015-08-16 22:50:59
--------------------------
248769
10702
Tác phẩm này là tác phẩm hay nhất mình từng đọc ,truyện đan xen nhiều chi tiết tình cảm rất dễ thương của nhân vật , Hiểu Quỳnh mình thích mà ấn tượng hơn là cây cỏ Dại Thích Bách Thảo , tác giả Minh Hiểu Khê đã thực sự cho chúng ta thấy từng khía cảm tâm lí và tính cách của nhân vật , dịu dàng như Sơ Nguyên , lạnh lùng như Nhược Bạch , hoạt bát như Hiểu Quỳnh , mạnh mẽ như Bách Thảo , diễn tả cốt truyện ngày càng hấp dẫn khiến người đọc muốn biết nữa :)
5
678718
2015-07-31 10:14:36
--------------------------
248755
10702
Khi đọc sang tập 2 thì mạch cảm xúc của mình bắt đầu tăng lên rất nhiều , truyện có cốt truyện rất hay diễn ta rất chân thành các khía cạnh của Bách Thảo , đan xen nhiều trận đấu giữa các võ quán ,rất hay khi tác giả diễn tả về các động tác chi tết của trận đấu , cảm xúc của nhân vật cũng từ đó mà được bọc lộ , truyện đan xen những tình tiếc dễ thương . Hiện nay cũng ra phim rồi nhưng mình nghĩ các bạn nên đọc để có thể cảm nhận được cái hay của tác phẩm , Minh Hiểu Khê là tác giả viết sách rất hay bạn ạ !!!
5
678718
2015-07-31 10:13:59
--------------------------
236137
10702
mình đã đọc đi đọc lại bộ này mấy lần nhưng vẫn thấy rất hay. Thật sự rất khâm phục ý chí và nghị lực của Bách Thảo, hâm mô tình yêu thầm lặng mà Nhược Bạch dành cho cô ấy, sẵn sàng hi sinh tất cả, cuối cùng tình yêu ấy cũng được đền đáp. Mình cũng thích sự dịu dàng của Sơ Nguyên, cả anh chàng Đình Hạo nữa :) 
Cái cách Minh Hiểu Khê miêu tả về những trận đấu cũng rất hấp dẫn
Một câu chuyện tình cảm động, hơn thế nữa là bài học về nghị lực, ý chí vươn lên
5
487845
2015-07-21 15:58:06
--------------------------
197736
10702
Mình không thích truyện Minh Hiểu Khê nhưng lại theo dõi được bộ truyện này đến tận lúc kết thúc. Truyện có nói về môn võ Taekwondo khá là hấp dẫn, đúng với sở thích của mình luôn. Nữ chính Bách Thảo của truyện cũng có cá tính, mà không ích kỷ như mấy bạn nữ chính khác của Minh Hiểu Khê nên mình có cảm tình hơn nhiều. Đoạn đầu truyện mình rất thích sự dịu dàng chu đáo của Sơ Nguyên, đến đoạn sau thì thích Nhược Bạch, dù thế nào thì mình vẫn cảm thấy Nhược Bạch là người thích hợp với Bách Thảo nhất trong 3 người.
4
393748
2015-05-18 12:37:43
--------------------------
179329
10702
thật là bị cuồng truyện này rồi nên mò lên tiki mà tập nào cũng muốn nhảy vào bình luận :))
thật sự mình thích nhân vật Bách Thảo.là một người mạnh mẽ đúng như cây cỏ dại mọc bên vách đá vậy.học hỏi ở BT rất nhiều.khâm phục Minh Hiểu Khê vì đã miêu tả những trận đấu thật là quá hấp dẫn,làm cho mình cảm thấy như thể là đang có mặt ở đó theo từng cử chỉ và hành động của nhân vật vậy.càng đọc càng thấy hay và muốn đọc mãi.cảm thấy thích cả ba nhân vật nam chính :)
5
586877
2015-04-06 22:32:31
--------------------------
158068
10702
Ở tập thứ hai này, tôi không quá bất ngờ khi truyện bắt đầu rơi vào motif thường tình là một gái ba trai, nhưng cách Minh Hiểu Khê miêu tả các trận đấu Teakwondo khá thật đã phần nào khiến tôi nguôi ngoai. Chắc hẳn trước khi bắt đầu vào viết thì cô đã phải đi tìm hiểu về môn võ thuật này, cá nhân tôi đánh giá rất cao tinh thần biết lo cho những đứa con tinh thần của mình như vậy. Trong tập này thì có vẻ tác giả đã lộ rõ việc thắng thua trong tình yêu của ba anh chàng nhà võ này rồi, hơn nữa cách miêu tả mối liên quan giữa Đình Hạo và Bách Thảo có phần mờ nhạt, có thể đây là chủ ý của tác giả, hoặc cô không thích nhân vật này. *cười*
3
435897
2015-02-11 13:03:22
--------------------------
138304
10702
Vẫn là như vậy, tôi vẫn thích truyện bởi những tình huống truyện và cách xây dựng nhân vật của Minh Hiểu Khê. Tập 2 đã miêu tả tâm lí nhân vật nhiều hơn so với tập một, diễn biến cũng dần phức tạp hơn, các trân đấu xuất hiện nhiều hơn và cách miêu tả của tác giả vầ Taekwondo quả tà rất tuyệt, hẳn ràng cô đã bỏ ra nhiều thời gian để tìm hiểu về môn võ thuật nàyVà sự kiên cường, mạnh mẽ và niềm tin của cô vào sư phụ làm tôi rất cảm phục.Tuy nhiên trong tình yêu cô có vẻ rất nhút nhát, như một chú nai nhỏ ngây thơ luôn đưa sừng ra để tự về, dường như tình yêu của cô đã dành hết cho võ thuật và sư phụ. Tôi thích nữ chính.
5
412050
2014-12-01 22:23:44
--------------------------
117613
10702
Từ nhỏ mình đã thích võ thuật và khi thấy một cuốn truyện về võ thuật, mình đã rất thích, mà đây còn là cuốn sách của tác giả mình yêu thích. 
Những tình tiết trong câu chuyện đều lôi cuốn mình. Ví dụ như những trận thi đấu Taekwondo của Bách Thảo đã làm cho mình thấy sự tỉ mỉ trong cách di chuyển của cô gái cũng như sự quan sát tinh tế về võ thuật của Hiểu Khê. Hay ví dụ là tình yêu của 2 anh chàng đối với một cô gái. Sơ Nguyên dễ mến, ân cần từng chút một; nhưng ngược lại với tính cách khác thì Nhược Bạch lạnh lùng nhưng trái tim lại thật ấm áp. 
Mình rất thích cái kết của câu chuyện vì nó đúng như ý mình mong muốn.
Thật sự cảm ơn Hiểu Khê về bộ tập truyện Thiếu nữ toàn phong 
4
338455
2014-07-20 21:29:09
--------------------------
91937
10702
Trên con đường chạm đến đỉnh cao vinh quang, bao nhiêu oan ức khổ cực đều bị Bách Thảo xem như gió thoảng mây trôi. Đối với cô gái kiên định quật cường ấy, không gì có thể coi là chướng ngại vật. Vừa đi học, vừa đi làm, vừa luyện tập võ thuật, vừa dọn dẹp ở võ quán… vân vân mây mây vô số việc… Thực sự giống với các anh hùng cách mạng trường kì kháng chiến. Cô gái ấy phải mạnh mẽ, phải kiên trì bền bỉ đến nhường nào chứ? Đó là chưa kể…

["Mày biết không, sư phụ mày là kẻ vô liêm sỉ! "
"Sư phụ mày là loại người xấu mà người ta viết trong sách, mày là tiểu khốn nạn của một kẻ đại khốn nạn!"
"Mày theo sư phụ, vậy là mày nhận cướp làm cha!"
…
Vì những câu đó, cô đã đánh nhau không biết bao nhiêu lần với bọn trẻ của võ quán, mỗi lần dù bị bọn chúng xúm vào đánh thâm tím mặt mày, cô cũng tuyệt nhiên không để bọn chúng được hời…]

Bách Thảo sùng kính sư phụ như đức tin của mình. Bảo vệ danh dự cho sư phụ thì có là gì chứ? Cô hiển nhiên cho đó là việc mình phải làm, cũng là việc duy nhất cô có thể làm cho ông, khi đã bị đuổi khỏi võ quán. Nhưng cô có biết, cần bao nhiêu nghị lực, bao nhiêu gan góc mới có thể đứng lên vì sư phụ? Cô hẳn là không biết mình đã dũng cảm như thế nào. Việc đó, việc mà ngay cả Quang Nhã, con gái ông cũng không dám làm, nhưng cô thì lại có thừa dũng khí. Vì sư phụ, cô dường như có thể đối đầu với cả thế giới. Những lời dèm pha đó, thái độ khinh thường đó, tất cả đều không làm cô mảy may nghi ngờ. Cô không tin, không muốn tin, trừ khi đó là lời sư phụ nói với cô, bằng không có chết cũng không tin…

["Đúng! Tôi là đệ tử của Khúc Hướng Nam!".]
["Cho nên tôi biết sư phụ mình là người thế nào! Sư phụ là người tràn đầy tinh thần Taekwondo! Phẩm chất cao quý, lương thiện, là một người lương thiện chân chính! Đại sư không thể, cũng không có tư cách bôi nhọ danh dự người khác ở đây!"]

Một Thích Bách Thảo như vậy, có thể không thán phục, không yêu quí sao? Câu trả lời chắc hẳn là không :)
5
27490
2013-08-24 00:46:30
--------------------------
85605
10702
Tôi mới đọc tập 1 hôm trước và hôm nay tôi đã ngay lập tức ôm tập 2 mà đọc một mạch bởi truyện quá hay nên tôi không muốn làm mất mạch cảmm xúc. Tôi muốn đọc ngay để cạm nhận hết được giá trị câu chuyện. Không giống với tập một, hầu như chỉ để cập đến Taekwondo, tập hai chủ yếu xoay quanh tình cảm của Bách Thảo, Nhược Bạch, Sơ Nguyên và Đình Hạo. Tôi đọc và thực sự rất ngờ trước ĐÌnh Hạo bởi không ngờ anh lại dành tình cảm cho cô! Nhưng, có lẽ thật không may cho anh bởi Bách Thảo không như những cô gái khác, cô khá khép kin, anh càng tấn công cô càng thu mình lại. Tội nghiệp Đình Hạo Quá. CÒn Sơ Nguyên, ở tập 1 tôi rất thích anh, có lẽ bởi sự dịu dàng mà anh dành cho Bách Thảo. Nhưng sang tập 2, có lẽ tình cảm của tôi lại nghiêng về Nhược Bạch nhiều hơn. Tôi dành tình cảm cho anh có lẽ bởi cái cách mà anh yêu Bách Thảo. Anh yêu, nhưng yêu thầm kín. Anh tuy có lẻ lạnh lùng, nhưng thực sự anh là một người rất ấm áp. Anh luôn âm thầm đứng sau quan tâm chắm sóc Bách Thảo. Lúc anh may lại áo cho Bách Thảo, tôi đã khóc, khóc bởi cảm động trước sự quan tâm mà anh dành cho Bách Thảo. Thật cảm động trước tình yêu của anh quá cơ. Còn Bách Thảo!! Ở tập này, tôi đã trải quá rất nhiều cảm xúc cùng cô, vui có, buồn có, cảm phục có. Tôi đã vui đến mức muốn nhảy cẫng lên khi cô thằng Đình Nghi ở phòng tập. Như thế là từ nay Đình Nghi sẽ không còn kiêu ngạo, khinh thường Bách Thảo nữa. và tôi rất thắc mắc, trước tình cảm của Sơ Nguyên, Nhược Bạch, ĐÌnh Hạo? Cô sẽ như thế nào? Cô sẽ chọn ai đây!! Tôi thật sự thắc mắc và mong chờ tập 3 và 4!!
5
24380
2013-07-05 22:24:13
--------------------------
81410
10702
Minh Hiểu Khuê thật sự có biệt tài ở khoản xây dựng tình cảm các tuyến nhân vật. Khi đi từ đầu đến khi hình thành tình cảm và trở nên sâu đậm thì các tuyến nhân vật có tính cách hành vi cũng như tình huống bộc lộ rất tự nhiên. Tập một như mở màn với ánh sáng chói lọi thì tập hai có phần chìm hơn nhưng lại nổi bật và hiện rõ mọi thứ cần nhìn thấy dưới ánh ban mai vậy. Thật sự thì phong cách cùng lối viết cũng như cách xây dựng tình huống quá cuốn hút, ngạc nhiên và đầy thích thú. Minh Hiểu Khuê còn khéo trong việc thu hút người đọc hồi hộp chờ những bước tiến tình cảm nữa. Rất mong chờ các tập tiếp theo của Thiếu nữ toàn phong.
5
28320
2013-06-16 23:58:44
--------------------------
74450
10702
Tập 2 rõ ràng có chuyển biến rõ rệt hơn khi Bách Thảo nhận quá nhiều sự quan tâm từ các sư huynh. Cô gái đó đơn thuần nhưng không đến nỗi ngu ngốc để nhận thấy sự quan tâm của các chàng trai.
Ở tập đầu, Nhược Bạch khá lạnh lùng nhưng ở tập 2, vai trò của anh được khẳng định rõ, và đến cuối anh là người dành được trái tim Bách Thảo. Lạnh lùng đâu hẳn là vô cảm, vô tâm chứ! Cách anh huấn luyện và trông nom Bách Thảo, yêu thương anh dồn hết vào đó. Mặc dù không nói ra, nét tính cách này, có thể gọi là ngạo mạn chăng?
Tuy nhiên mình lại nghiêng về phía Sơ Nguyên hơn. Có thể vì anh là người lưu tâm Bách Thảo trước, hoặc cũng có thể vì chị Khê miêu tả anh quá chân thực, quá cảm động đi. Sơ Nguyên ở tập 2 tiếp tục dịu dàng, hiền dịu và ngày càng tỏ rõ tình cảm của mình với Bách Thảo hơn. Tiếc là ở tập này chưa giải quyết xong vấn đề tình cảm rắc rối này!
Cũng phải nhắc đến Đình Hạo, trái với cô em tiểu thư, anh là người duy nhất mạnh mẽ bày tỏ với Bách Thảo. Phải cứng rắn như vậy a~ Mặc dù Bách Thảo đơn thuần thật, tấn công như vầy cô nàng sẽ tự thu mình lại. Vốn đã không dạn dĩ với mọi người rồi.

Vẫn đan xen những buổi tập, thi đấu nhưng đa phần nội dung chi phối tập 2 là chuyện tình cảm của Bách Thảo. Không biết cuối cùng rồi Bạn Cỏ này chọn ai nữa
4
53823
2013-05-13 21:26:44
--------------------------
72905
10702
Lại đầy những bất ngờ của Minh Hiểu Khuê dành cho độc giả. Ở tập 2 này, vẫn là những trận teakwondo nhưng đầy kịch tính, quyết liệt hơn nữa... Tôi thấy ghét Đình Nghi, bởi cô ấy ích kỉ, chiểu biết cho riêng mình. Hết lần nàn lần khác chết nhạo Bách Thảo. Nếu tôi là Bách Thảo, không hiểu tôi có xử xự đc như cô không? Chắc là tôi bay vào đập Đình Nghi quá...dễ thế lắm. Tôi thương Nhược Bạch, hoàn cảnh gia đình anh khó khăn nhưng lại luôn hết lòng vì Bách Thảo, còn hi sinh rất nhiều vì cô. Cảnh Nhược Bạch ngồi khâu từng đường kim mũi chỉ bộ võ phục cho Bách Thảo thật khiến người ta cảm động...Bách Thảo cũng vì Nhược Bạch mà gắng sức tập luyện, và đã chiến thắng Đình Nghi trong trận giao hữu. Có lẽ Nhược Bạch vui lắm. Tôi thật mong có 1 HE đối với 2 con người này...:) Đọc truyện, tôi có cảm giác như chính mình đang đặt mình vào từng nhân vật vậy
4
59890
2013-05-05 11:26:00
--------------------------
72272
10702
E hèm, lần thứ 2 đọc Thiếu nữ toàn phong 2 rồi. Vậy mà mình vẫn thấy truyện rất cuốn hút, đôi lúc mìh vẫn phải bất ngờ vì các tình tiết xảy ra. Tập này ta dần hé mở quá khứ của Khúc Hướng Nam( đoạn đó làm mình rất xúc động), các chàng trai cũng thể hiện tình cảm với Bách Thảo ngày một rõ ràng hơn. Ừm, tập này không chỉ toàn là những cảnh đấu võ nữa mà còn có nhữg đoạn tình cảm rất lãng mạn! Ôi cha, đúng là một tác phẩm cuốn hút và khi đọc xong, cảm xúc vẫ còn trong mình chứ không trôi tuột đi tẹo nào ^^
5
44678
2013-05-01 13:21:45
--------------------------
64157
10702
 Nếu gọi tập 1 là Ánh sáng ban mai cho mình thì tập 2 là đỉnh của đỉnh, nó đã làm trái tim mình tan chảy, và không thiếu những phút giây đập bình bịch vì hồi hộp!
 Tập 1 mình đã tập quen dần với những đoạn gay cấn, hồi hộp, nhất là những lúc Bách Thảo đấu võ, thế mà tập 2 mình vẫn không ngăn được cảm xúc của mình! Hồi hộp quá đi mất, đã vậy  Đình Hạo còn dần xuất hiện nhiều hơn, và đã bắt đầu " tấn công" Bách Thảo ^^. Tình cả của Bách Thảo sẽ thuộc về ai đây???
  Đây quả là một tiểu thuyết hay, lôi cuốn và đánh dấu sự phát triển vượt bậc của chị Khê, tuy truyện kể về Taewondo nhưng không hề bạo lực và nhàm chán chút nào, hơn nữa còn vô cùng cảm động nữa đấy. Mình đã học được rất nhiều điều từ tác phẩm, ví dụ như phải kiên cường để vượt qua khó khăn nè, phải biết tin tưởng người mình yêu thương nè ( như Bách Thảo với Khúc Hướng Nam đó ^^). Tập 2 thật sự rất cuốn hút, khiến mình không thôi mong chờ được đọc tiếp tập 3! Hấp dẫn quá đi!!!!!!
5
44678
2013-03-18 11:54:52
--------------------------
31231
10702
Câu chuyện đã chuyển biến thật bất ngờ khi tưởng chừng Nhược Bạch chỉ là một nhân vật phụ nhưng trong phần 2, anh đã thể hiện rõ bản thân và chỗ đứng trong chuyện của mình. Nhược Bách sư huynh khi đọc tập 1, mình thấy anh ấy quả thực rất lạnh lùng, khó gần, thật bất hạnh cho Bách Thảo khic ó một sư huynh như thế! Nhưng qua phần 2, tình cảm của mình dành cho Nhược Bạch sư huynh ngày càng lớn hơn khi thấy anh thật sự là một người có tâm hồn ấm áp, luôn lo lắng cho Bách Thảo, dõi theo cô ấy, sẵn sàng làm bất cứ việc gì vì cô, nhưng cô lại không hay, không biết, dành tình cảm của mình cho Sơ Nguyên sư huynh. Dù cho Sơ Nguyên là một người tốt, nhẹ nhàng, hiền dịu, rất biết cách quan tâm, chăm sóc cô nhưng mình vẫn thích Nhược Bạch hơn. Đình Hạo lại là một nhân vật không gây ấn tượng mạnh cho mình lắm, nhưng hình ảnh anh hiện ra giữa những trang truyện vẫn thật đẹp, thật lung linh. Giữa tập 2 là lời đề nghị Bách Thảo làm bạn gái của Đình Hào và lúc kết lại là một lời tỏ tình của Sơ Nguyên khiến mình vô cùng lo lắng. Liệu Bách Thảo sẽ chọn ai đây giữa 3 chàng trai xuất chúng này?
5
28163
2012-06-23 11:06:40
--------------------------
29516
10702
Thoạt đầu đọc xong tập 1, mình vẫn chưa cảm thấy thật sự cuốn hút... Nhưng sang đến tập 2 thì..... Ôi!!! Hay khôn xiết, hay không thể diễn tả hết bằng lời!!!
Thích nhất là khúc Đình hạo tiền bối ngỏ ý hẹn hò với Bách Thảo... Ôi! Đọc đến đoạn ấy mà tim mình cứ như muốn nhảy ra ngoài vì sung sướng (haha, cứ như mình là người được tỏ tình ý :">)
Ở tập 2, Nhược bạch sư huynh cũng không còn mờ nhạt như ở tập 1. Trong tập 1, mình cứ ngỡ anh ý là nhân vật phụ cơ, nhưng sang đến tập 2 thì anh ấy đã trở thành nhân vật "không-thể-thiếu" trong toàn bộ tác phẩm! Lời thoại của anh cũng nhiều hơn, tính cách và suy nghĩ của anh cũng được bộc lộ nhiều hơn!!! Yêu quá!! :x
Ngất ngây nhất là ở đoạn cuối, vâng, ngay trang cuối cùng ấy, .... Ôi ôi!!!  Hay không chịu nổi... Đọc xong tập 2 là nhất định phải mua liền liền tập 3, tập 4 mới thoả mãn ^^
5
40122
2012-06-06 19:46:23
--------------------------
28324
10702
Mình rất thích các tác phẩm của Minh Hiểu Khê. Mình đã đọc từng tác phẩm, mỗi tác phẩm đều có điểm hay, cái lạ của riêng từng nhân vật. Nhìn chung, các nhân vật nữ trong truyện Minh Hiểu Khê đều đẹp, đều có cái nhu cái cương trong con người họ. Bách Thảo là 1 cô bé rất khác với các nhân vật nữ trưởng thành của Minh Hiểu Khê, ở cô bé, ta thấy được tính cách của 1 cô bé vừa lớn, vừa là tính cách của một người có đam mê nhiệt huyết cao.
Câu chuyện tình cảm trong sáng và những ước mơ cháy bổng, giống như Minh Hiểu Khê đã nói nó không hề vẩn bụi thật tuyệt vời biết bao khi mà bên cạnh Bách Thảo có 1 Sơ Nguyên ấm áp, dịu dàng như ánh trăng . 1 Đình Hạo ga lăng. Bách Thảo vẫn cảm nhận được, tình cảm của Nhược Bạch anh không ấm áp như Sơ Nguyên, cũng không ga lăng như Đình Hạo nhưng trong anh, Bách Thảo là báu vật duy nhất mãi mãi không ai thay thế được. Mình cực kì thích Thiếu nữ toàn phong!!! Truyện của Minh Hiểu Khê quả là rất rất hay!!! Đã đọc rồi là không thể dừng lại được!!!
Đây là một thể loại khá mới lạ , cốt truyện rất hay, sâu sắc và hồi hộp. Bạn nào yêu thích những tác phẩm trước của Minh Hiểu Khê thì không thể bỏ qua được cuốn sách này. 
4
39173
2012-05-26 11:38:41
--------------------------
17423
10702
Tôi đã đọc xong tập 1 và tập 2 của Thiếu nữ toàn phong được 30 giây. Liền vội ngồi dậy viết comment. TNTP thực sự rất hay, khiến tôi cũng như gà bông và BFF như bị cuốc chặt vào nội dung câu chuyện. Sự bộc lộ tình cảm của ba chàng trai Đình Hạo, Sơ Nguyên cũng như Nhược Bạch với Bách Thảo thật sự vô cùng đáng yêu và hết sức chân thành, xuất phát từ trái tim. Theo tôi, Nhược Bạch là chàng trai tuyệt vời nhất. Tôi thích cái cách Hiểu Khê của tôi miêu tả về Nhược Bạch và tình cảm của anh ấy. Anh ấy không bộc lộ nhiều rằng anh ấy thích Bách Thảo nhiều như thế nào, chưa vội tỏ tình với Bách Thảo nhưng luôn bên cạnh cô ấy, dõi theo cô ấy. Tôi ngày càng khâm phục Minh Hiểu Khê bởi sự sáng tạo cũng như biến đổi ngoạn mục phong cách của mình không theo mô típ cộp mác chính mình nữa. 
Hôm nay là thứ bảy, đúng 12 giờ đêm. Tôi sẽ chúc BFF và gà bông của mình ngủ ngon, ôm quyển TNTP tập 2 vào lòng, tự nhủ rằng ngày mai sẽ rinh ngay về TNTP 3 đầy mong đợi...
5
21502
2012-01-08 00:02:40
--------------------------
16265
10702
Phải nói thật là, mình sợ Minh Hiểu Khuê rồi đó! Tại sao cô ấy có thể khiến mình đang hy  vọng thành thất vọng được nhỉ? Khi Bách Thảo đánh thắng Đình Nghi ở phòng tập, mình đã vui mừng khôn xiết, bởi vì từ đây Đình Nghi sẽ không còn kiêu căng vênh váo nữa. Thực ra mình cảm thấy có lẽ Đình Nghi cũng không phải người quá xấu xa gì. Vậy mà Bách Thảo vẫn bị phân biệt, là những hành động cố ý vô cùng xấu xa. Bách Thảo ơi cố lên nhé! Vả lại dù sợ Minh Hiểu Khuê lắm nhưng truyện của MHK mình vẫn sẽ săn lùng ráo riết, để cho đỡ sợ mà!
5
19409
2011-12-18 18:12:15
--------------------------
15462
10702
Lần đầu tiên tôi bắt đầu đọc tiểu thuyết Trung Quốc là vào học kì 2 năm lớp 7. Quyển "Sẽ có thiên thần thay anh yêu em" của Minh Hiểu Khê là quyển đầu tiên tôi đọc. 
Trong suốt gần 3 năm qua, tôi đã đọc không biết bao nhiêu quyển tiểu thuyết, nhưng có lẽ dư âm của lần đầu tiên lúc nào cũng mãnh liệt hơn nhiều. Cách viết của Minh Hiểu Khê không làm bạn xoáy mình vào không gian trong truyện một cách nhanh chóng, mà phải đọc từ từ, đặt mình vào tình tiết truyện để cảm nhận, để rồi bạn mới phát hiện dần dần mình yêu quyển sách ấy hơn, yêu những nhân vật trong ấy hơn, và yêu luôn cả tác giả.
Thiếu nữ Toàn Phong là một bước tiến mới trong cách viết của Minh Hiểu Khê, cô lấy trọng tâm là môn võ Taekwondo để nói lên niềm yêu quý đối với môn võ này của cô bé Bách Thảo. Tuy truyện không dẫn dắt vào tình cảm nhiều như những quyển khác, nhưng xen lẫn vào đó vẫn có chút dư vị của tình yêu, là sự quan tâm của ba chàng trai Nhược Bạch, Đình Hạo và Sơ Nguyên. Vậy rốt cuộc ai sẽ nắm giữ trái tim của Bách Thảo? 
Vẫn là Minh Hiểu Khê, phải đến cuối cùng tôi mới biết được kết cục, tôi yêu cách viết tự nhiên ấy, vì nó làm tôi không ngừng hồi hộp và mong chờ quyển tiếp theo :)
5
9032
2011-12-06 10:12:41
--------------------------
15129
10702
Sau khi đọc xong tập 1, mình cứ nghĩ Bách Thảo sẽ chiến thắng Đình Nghi. Nhưng thật không ngờ, cô lại bị Đình Nghi đánh cho mặt mũi bê bết máu, khán giả hết sức bất ngờ và ánh mắt hiện ra hàng ngàn tia lo lắng và kinh sợ. Thật là bất công cho Bách Thảo. Vì ngay cả khi cô đánh thắng Đình Nghi với tỉ số 2-0, trên tivi lại chỉ chiếu những cảnh Đình Nghi tấn công cô. Bên cạnh đó, chuyện tình cảm của Bách Thảo với 3 anh chàng: Nhược Bạch, Sơ Nguyên và Đình Hạo cũng không kém phần "gay cấn". Những lo lắng của 3 chàng trai dành cho cô đều thể hiện rõ tình cảm chân thật đối với Bách Thảo. Cuối truyện, Sơ Nguyên đã tỏ tình với Bách Thảo. Lúc ấy mình cảm thấy hơi buồn, vì mình muốn người nói ra điều đó là Nhược Bạch sư huynh cơ. Nhưng ai biết được điều gì sẽ xảy ra, vì chính Minh Hiểu Khê cũng phát triển câu chuyện của mình một cách tự nhiên cơ mà! Mình thật sự đang rất trông chờ tập 3 và 4 của "Thiếu nữ toàn phong". 
5
13126
2011-11-28 21:38:34
--------------------------
14978
10702
Câu chuyện nói về Thích Bách Thảo, môn võ Karate và các chàng trai đẹp theo đúng motip quen thuộc của tác giả. Nhưng chính tác giả lại không làm cho motip đó trở nên nhàm chán mà có 1 cái hay riêng, không củ ai khác. Từ nhân vật đến cách miêu tả sự vật, sự việc đều linh hoạt và làm thu hút mình qua từng trang sách và tiếc nuối khi kết thúc một tập truyện. Trong cuốn truyện này, là cả 1 quá trình cố gắng không ngừng của Bách Thảo, làm mình có những lúc mừng cho Bách Thảo, buồn cho Bách Thảo, xúc động vì Bách Thảo. Mình yêu chính nhân vật Bách Thảo, như XuXu của chị Sakura vậy, nhưng cản đảm và tự tin hơn rất nhiều. Nóng lòng chờ tập truyện mới của M.H.Khê :x
5
17910
2011-11-25 23:08:53
--------------------------
14668
10702
ôi! đọc hết tập này tôi cảm thấy như tim mình muốn rớt ra ngoài vì hồi hộp ấy, con đường đầy những khó khăn của Bách Thảo bắt đầu rồi, thật là lắm chông gai, nhưng bên cạnh cô lại luôn có những tình yêu thương che chở, bảo vệ cô. Tập 2 cuốn hút hơn cả tập 1, bởi sự xuất hiện của những ganh ghét, xảo trá, bất công rất đời thường làm cho ta cảm thấy Bách Thảo, Sơ Nguyên, Đình Nghi. Nhược Bạch, Đình Hạo, ...... như đang thật sự tồn tại trong cuộc sống này ở một nơi nào đó. Ở tập này, tình huống truyện trở nên thật cao trào, diễn biến một cách hồi hộp khiến tôi bị cuốn vào đó, không thể nào rời ra được, cảm nhận như mình hòa vào trong là một người trong cuộc vậy......
5
5415
2011-11-19 18:02:14
--------------------------
11577
10702
Đây là 1 câu chuyện, lúc mới đọc tôi có cảm giác nhàm chán, motip truyện bình thường theo kiểu của MHK. Nhưng càng đọc tôi càng bị cuốn hút vào câu chuyện lúc nào không hay! Cuộc đời của cô bé yêu Taekwondo - Thích Bách Thảo - mang nhìu vất vả, nhưng bên cạnh cô bé có nhiều người quan tâm, yêu thương cô thật lòng. Câu chuyện diễn ra thật tự nhiên, khiến đọc giả không thể đoán được ý đồ của tác giả. Càng làm tăng thêm tính tò mò cho độc giả.
Bên cạnh cô bé có 3 chàng trai, quan tâm, yêu thương, nhưng liệu ai có thể chiếm hữu trái tim của cô? 
4
6102
2011-09-22 13:57:24
--------------------------
