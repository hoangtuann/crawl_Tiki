498009
147
Mình từng đọc cuốn sách này phiên bản tiếng Việt, tuy nhiên mình đã phải bỏ giữa chừng vì cách dịch các từ kiểu chửi thề trong tiếng Anh sang tiếng Việt đọc rất khó chịu. Mình đã quyết định mua phiên bản tiếng anh để đọc và hiểu được tái sao nó là một cuốn sách nằm trong các list recommend... Và nó đã không làm mình thất vọng, mình học được rất nhiều thứ từ nó và không ít lần suýt bật khóc khi đọc. 
5
336455
2016-12-22 13:26:09
--------------------------
483915
147
Có lẽ hơi lạ, nhưng nhờ thần tượng của mình mình mới đến với cuốn sách này. Thật ra mình là Fan của BTS - một nhóm nhạc Hàn. Chắc hẳn rằng ai cũng lấy làm lạ khi nhóm nhạc ấy và quyển sách này có gì liên quan đến nhau? Đấy là đùa đấy thôi. Sự thật là thần tượng của bọn mình vừa cho ra đời một loạt các phim ngắn, và giải mat các phim đó thật sự rất thú vị. Từng cảnh quay đều có những suy luận logic riêng và rồi tự mỗi người nghĩ ra một cốt truyện khác nhau. Tuy là não bị thắt bím đấy, nhưng thật ra cũng có phần thú vị.
Lúc giải mã shortfilm, mình vô tình nghe nhắc đến chi tiết trong cuốn "Bắt trẻ đồng xanh" này, và thật sự mình đã rất tò mò về nó. Thế là nhờ Tiki, người bạn quen của mình mà mình tậu ngay một cuốn. Và cứ tưởng ràng nó tieng Việt, khi mở trang sách đầu tiên ra thì mình bất ngờ vô cùng. Sách khá nhẹ, khuôn sách nhìn chung nhỏ thế mà giá thành lại đến 170k. Xong hơi hụt hẫng, nhưng mình vẫn cố gắng đọc và cho nhận xét một cách khách quan nhất. Và có lẽ đây là một quyết định hoàn toàn đúng đắn khi mình mua cuốn sách này, bản tiếng Anh làm cho một đứa chưa bao giờ có ý định dịch tiếng Anh mà vẫn phải thích thú như mình. Tuyệt. Có lẽ như mình, mới đọc vài trang đầu thì vẫn chưa thấm. Nhưng đến trang thứ 7, thì thứ ngôn ngữ "ngông" ở cuốn sách này hoàn toàn kéo mình về một thế giới riêng. Chưa bao giờ mình cảm nhận nét đẹp của văn học nước ngoài nó lại có phần sâu sắc thế.
Tóm lại, cảm ơn thần tượng đã mang mình đến quyển sách này. Hoàn toàn hoàn toàn hài lòng về nội dung của nó. Giá trị vuột xa cả giá thành. Nếu yêu thích văn học, đừng bỏ qua quyển sách này.
5
286362
2016-09-18 18:46:44
--------------------------
435238
147
Mình thích cuốn này kinh khủng. Đây là cuốn duy nhất trong đống sách của mình có cả bản tiếng Việt và tiếng Anh. 

Thiên hạ bảo cuốn này chán òm, tại sao lại vào danh sách truyện kinh điển. Ối chời. Nói thật với bạn đúng là nội dung chả có gì mấy, nhưng cách hành văn và cảm xúc của nhân vật rất thú vị. Chỉ cần bạn hợp gu với cuốn sách từ những trang đầu, bạn sẽ gật gù khoái chí đến khi cuốn sách khép lại. 

"Mình thích một quyển sách đôi khi cũng mắc cười một tí". Và đây là một quyển sách khiến mình bật cười haha rất nhiều lần.
5
141646
2016-05-24 19:28:15
--------------------------
426366
147
Mình đã được đọc quyển "Bắt trẻ đồng xanh" này bằng tiếng Việt, một tác phẩm văn học đứng hàng kinh điển thế giới. Tuy nhiên nó vẫn gặp những phản ứng trái chiều, ngôn ngữ của tác phẩm có thể nói hơi "ngông", nhưng đằng sau là những giá trị sâu sắc. Có thể mọi người sẽ ước có một người bạn để chia sẻ mọi chuyện vui buồn như nhân vật trong truyện. Bản tiếng Anh này giúp mình trau dồi thêm tiếng Anh rất tuyệt vời. Bìa sách tiếng Anh rất đẹp, giấy xốp, rất nhẹ, khổ giấy nhỏ có thể bỏ túi được.
4
578193
2016-05-06 19:00:08
--------------------------
