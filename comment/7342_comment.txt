293500
7342
Tôi mua quyển tập này và một vài quyển khác để chuẩn bị cho đứa cháu vào lớp 1. Quyển vở được in đầy đủ, rõ ràng các chữ cái, viết nét đứt cho các bé tập tô, luyện viết, luyện chữ đẹp, có cả từ, cụm từ, các hình ảnh sinh động minh họa để nêu rõ chữ cái cần nhớ. Quyển tập này có thể dành cho bé tập viết từ lúc 04 - 05 tuổi để quen dần với các chữ cái. Chúng ta không nên ép trẻ mà cần tạo thói quen học tập tốt theo giờ cố định.
3
169332
2015-09-08 13:30:06
--------------------------
