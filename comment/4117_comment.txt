454721
4117
Trừ việc không kèm theo CD
Thì quyển sách này thực sự bổ ích
Nắm vững được nhiều từ vựng là nhu cầu cơ bản khi học ngoại ngữ.Ma việc học ngoại ngữ có thành công hay không phụ thuộc rất nhiều vào từ vựng mà. Cuốn sách này đã giúp mình
- Nắm vững cách học phiên âm và phát âm chuẩn.

- Hiểu rõ và vận dụng tốt các kỹ năng để giao tiếp tốt.

- Nắm vững quy trình học.

- Sử dụng tốt công cụ nghe nói đọc hiểu.

- Biết tự tra từ điển.
Mình cuốn C mà hình như không có ấy nhỉ???
4
1342185
2016-06-22 08:30:24
--------------------------
416689
4117
Đây là một quyển sách cần thiết cho người học muốn cũng cố một số từ vựng và quyển sách này còn trình bày 400 câu giao tiếp tiếng Anh thông dụng. Điều đặc biệt là sách có CD nên giúp người học rèn luyện kỹ năng nghe, nói, đọc đúng nội dung trong sách này. Quyển sách này đã giúp tôi tích lũy thêm một số từ vựng mới và giúp tôi dễ dàng nói các mẫu câu theo phản xạ. Tôi vẫn sẽ tiếp tục sử dụng quyển sách như tài liệu tiếng Anh hằng ngày để tôi có thể nâng cao khả năng sử dụng ngôn ngữ hết sức cần thiết trong cuộc sống hiện đại ngày nay.
4
855770
2016-04-15 15:40:03
--------------------------
374211
4117
Sổ tay từ vựng Tiếng Anh rất có ích. Nó tóm gọn từ vựng chỉ trong 1 quyển sổ mini khổ 9x15cm. Lại nhỏ gọn có thể mang học mọi lúc mọi nơi! Bạn vừa học từ vựng vừa có thể học những cách sử dụng, cụm từ, các cách giao tiếp, các thành ngữ, tục ngữ trong mục Vui học tiếng anh xen kẽ với các từ vựng này. Ngoài ra, cuối sách còn có 400 câu giao tiếp tiếng anh thông dụng, rất có ích cho việc giao tiếp hàng ngày mà bạn cần ghi nhớ. Tuy nhiên theo mình sách này cần thiết cho các bạn học các từ vựng cơ bản. Bởi vì những từ vựng khó thì thật sự không có trong đây. Nhưng quyển sách mini này rất có ích cho mình khi cần nắm lại kiến thức. Cảm ơn Tiki đã mang lại quyển sách này
MÌNH THẬT SỰ KHÔNG HỐI HẬN KHI ĐÃ MUA QUYỂN SÁCH NÀY!!!
4
1036588
2016-01-25 20:14:51
--------------------------
322560
4117
Lần đầu tiên mình thấy cuốn sách này là khi mình mua cuốn Luyện siêu trí nhớ từ vựng nên được tặng kèm. Cuốn sách khá là nhỏ gọn, trình bày khá đẹp mắt. Điểm mình thích nhất là cuốn sách không chỉ đơn thuần là giấy trắng không thôi mà còn có nhiều hình ảnh minh họa rất thú vị nữa. Cuốn sách này còn có nhiều thành ngữ lạ và hay như "Sick as a dog, một số cụm động từ và từ vựng liên quan đến một vài chủ đề nhỏ nhỏ. Cuối sách còn có 400 câu giao tiếp Tiếng Anh thông dụng nữa. Tuy nội dung của mỗi phần không nhiều lắm nhưng với giá như vậy là rất ok rồi. Mình đang đặt thêm một cuốn như này để tặng nhỏ bạn nữa. Cảm ơn Tiki rất nhiều vì cuốn sổ tay này.
5
570117
2015-10-16 15:23:00
--------------------------
308244
4117
Cuốn sổ tay Từ vựng tiếng anh trình độ B này rất tiện dụng và hữu ích. Tuy kích thước cuốn sổ nhỏ bé nhưng có số lượng từ vựng đa dạng phong phú. Kích thước cuốn sổ nhỏ bé , lại tiện lợi mình có thể bỏ vào túi và sử dụng bất cứ lúc nào. N hờ vậy mà vốn từ vựng của mình được cải thiện lên rất nhiều. Hơn nữa trong cuốn sổ tay này còn có những hình ảnh rất  minh họa rất sinh động dễ hiểu giúp mình ghi nhớ từ dễ dàng hơn. Cuốn sổ này rất hữu ích.
4
604794
2015-09-18 16:05:02
--------------------------
304904
4117
cuốn sách sổ tay từ vựng tiếng anh trình độ B này làm mình khá hài lòng,với kích thước nhỏ, dễ mang đi đem theo túi để học ở bắt kỳ nơi nào ,trong sách từ vựng hơi ít nhưng ngược lại có các câu giao tiếp khá hay giúp nâng cao khả năng giao tiếp , ngoài ra một số từ còn có các hình ảnh minh họa giúp dễ giểu hơn, màu giấy của sách khá đẹp, nói chung là về mặt trình bài thì khá tốt, tuy nhiên, giá hơi mắc, phần tra từ hơi khó nhìn nên hơi khó tra, cuốn sách này thích hợp để học từ vựng và nâng cao khả năng giao tiếp 
4
770043
2015-09-16 18:38:39
--------------------------
185402
4117
Mình mới dùng cuốn Sổ Tay Từ Vựng Tiếng Anh Trình Độ B này trong một khoảng thời gian ngắn nhưng mình thấy vốn từ của mình được cải thiện khá nhiều. Cuốn sách này có những bài kiểm tra và đánh giá trình độ từ vựng khá hay, từ đơn giản với những từ dễ nhớ và hay gặp cho đến những từ khó nhớ. Hơn nữa cuốn sổ tay còn có hình ảnh sống động, trình bày bố cục khá mạch lạc nên tạo cảm giác thoải mái cho người dùng. Trước đây, vốn từ vựng của mình chưa nhiều, và mình khó có thể nhớ lâu một từ tiếng anh, nhưng từ ngày có cuốn sổ tay này, mình thấy phần từ vựng của mình được bù đắp khá nhiều. Tuy nhiên đối với một vài từ ngữ tiếng anh trong cuốn sổ tay, mình thấy chưa được hài lòng lắm về nghĩa chuẩn của từ. Nhưng mình thấy khá an tâm khi sách được xuất bản bởi nhà xuất bản đại học quốc gia Hà Nội!
4
611652
2015-04-19 12:03:43
--------------------------
