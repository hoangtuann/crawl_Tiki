429195
12193
Trước đây khi học lịch sử, luôn có những " tấm gương " được nhắc đến như Lê Chiêu Thống, Trần Ích Tắc, ... với " vai trò " như là những kẻ phản bội của dân tộc, những kẻ cõng rắn cắn gà nhà. Nhưng trong tác phẩm "Bánh xe khứ quốc" của Phan Trần Chúc thì không hoàn toàn là như vậy, từ cuộc sống khó khăn, cùng cực từ khi tới sinh ra và lớn lên trong ngục tù, sự đè đầu cưỡi cổ của chúa Trịnh, những lần thấp thỏm lo âu khi quân Tây Sơn tiến ra Bắc Hà, hay những toan tính về việc nước cùng với niềm hy vọng có thể lại làm chủ đấy nước đã tạo nên một Lê Chiêu Thống đáng thương hơn là đáng ghét. 
Trong bộ sách góc nhìn sử việt của Alpha book mình thực sự thích những cuốn tiểu thuyết lịch sử như thế này.
Các bạn nên đọc qua và cảm nhận nhé
4
470908
2016-05-12 19:45:52
--------------------------
329978
12193
Lê Chiêu-Thống chính là một trong những nhơn-vật hết sức lạ-lùng trong quốc-sử. Đối với quốc-gia, việc Chiêu-Thống thụ-phong từ Tôn Sĩ-Nghị, quỳ giữa long-đình mà nhận tước An-Nam Quốc-vương, lại rước Thanh-viện giày xéo quê-hương, thiệt là tội cùng trời khôn tả. Nhưng ngẫm lại, vì cơ-đồ Lê-thị, vì nghĩ đến công-nghiệp tổ-tông gầy dựng qua trăm năm mà phút chốc rơi vào tay kẻ ngoại-tộc, Chiêu-Thống thực đáng là bậc hiếu-tử và hết lòng vì tông-tộc họ Lê. Nhưng lại, lòng trời không tựa nhà Lê, lại thêm thói đi cầu ngoại-bang, đặt lợi-ích của gia-đình - trong đó có cả lợi-ích cá-nhơn - trên cả lợi-ích của quốc-gia và dân-tộc, thực là điều bất-khả-chấp. Nhưng lâu nay, chúng ta hiềm vì lẽ mãi-quốc của Chiêu-Thống mà không tường-tận được suy-tưởng của con người này, vì vậy mà lên án. Nhưng đọc xong cuốn sách này, thiết-tưởng độc-giả cũng nên công-minh với nhơn-vật này, dầu có lên án nhưng vẫn cảm-thông phần nào, ngẫm ra vì vậy cũng hay hơn. 
5
34195
2015-11-01 20:12:46
--------------------------
318934
12193
Lê Chiêu Thống là một nhân vật đặc biệt, vừa đáng thương, vừa đáng ghét, ghét đến tột độ vì hành động rước voi giày mả tổ, cõng grắn cắn rà nhà, cầu viên quân ngoại bang, nhưng đứng trên tình cảm con người mà nhận xét thì y cũng đáng thương hại khi là người cuối cùng hứng chịu cái cơ bĩ cực của triều đại nhà Lê, mọi tội lỗi dường như chỉ đổ lên đầu y, đứng nhìn cái cơ nghiệp tổ tiên để lại bị người khác cướp đoạt, âu cũng là một bất hạnh thống thiết rồi. Viết về nhân vật này tính ra cũng khá ít sách vở, các bạn đọc quyển sách nhỏ này cũng sẽ biết được thêm nhiều chi tiết, nhiều uẩn khúc. Sách in bìa đẹp, bắt mắt, giấy tốt, tuy in hay biị sai chính tả.
4
564333
2015-10-07 12:45:55
--------------------------
