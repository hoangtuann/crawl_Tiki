460017
9571
Có khá nhiều mẩu chuyện được đưa ra ở trong tập VÌ này, những câu chuyện rất ý nghĩa, có tác dụng rất lớn trong việc truyền động lực cho tôi. Qua những câu chuyện, tôi học được khá nhiều, về sự độc lập tài chính. Và để có được sự độc lập đó, quả là không dễ dàng gì. Muốn độc lập được về tài chính, bạn phải nỗ lực, và phải mạnh mẽ, bản lĩnh để đối diện với nhiều thứ. Sự mạo hiểm, tiên lượng về một sự thất bại, nhưng phải có tàm nhìn, rút kinh nghiệm, và phải chinh phục được nỗi sợ đó
4
920424
2016-06-26 18:43:26
--------------------------
357243
9571
mình chưa từng có niềm vui với kinh doanh nhưng vì môn học nên mình cố gắng tìm hứng thú với Kinh doanh. nhiều ngày tìm kiếm, cuốn Dạy con làm giàu (RICH DAD POOR DAD) được nhiều người khuyên nên đọc... mình quyết định lên đọc thử online, TUYỆT VỜI!!! cuốn sách rất cuốn hút, mình đã cố gắng mua gần như trọn bộ (vì chỉ đọc những cuốn có chủ đề gần gũi và cần thiết). một cuốn sách mang niềm cảm hứng và dạy về IQ tài chính rất hay. Không quá trễ để làm giàu, chỉ cần biết cách để tiền có thể tự kiếm ra tiền giúp mình mà thôi.
5
915633
2015-12-23 10:32:53
--------------------------
294044
9571
Ở cuốn sách này, tác giả chia sẻ về "Những câu chuyện thành công". Tác giả học được trò chơi tiền bạc từ người bố giàu, và ông đã tạo ra trò chơi CASHFLOW ( vòng quay tiền mặt). Trò chơi này dạy bạn những kĩ năng về tài chính thông qua sự vui thích, bắt chước và hành động. Trò chơi này hết sức đặc việt, nó sẽ thử thách, chỉ dạy và yêu cầu bạn suy nghĩ như những người giàu có. Các câu chuyện thành công về tài chính trong cuốn sách là nhờ họ chơi trò chơi trò chơi tiền bạc. Không bao giờ là quá sớm để bắt đầu độc lập về tài chính, tác giả chia sẻ về những câu chuyện của các nhà triệu phú trẻ tuổi, nhờ vào chơi trò chơi tiền bạc của ông mà họ nhận thức được "Không bao giờ là quá sớm" để thành công về tài chính,
4
156324
2015-09-08 22:53:26
--------------------------
282380
9571
Tôi là một người dễ được truyền cảm hứng bởi những câu chuyện thành công có thật, nhớ những tấm gương thành công mà tôi cảm thấy mình cần phải nỗ lực phấn đấu nhiều hơn để có thể đạt được tầm cao như họ. Quyển sách tập hợp những câu chuyện thành công, là một quyển sách vô cùng phù hợp với tôi. Đọc những câu chuyện thành công, tôi không những được truyền cảm hứng mà còn học được rất nhiều từ những bài học thành công của những tấm gương được đề cập trong sách. Cảm ơn tác giả rất nhiều!
5
169373
2015-08-29 09:12:59
--------------------------
45484
9571
Trước cuộc sống ngày càng khó khăn, mỗi con người luôn phải biết học cách dùng tiền sao cho hiệu quả. Nhưng thực chất điều đó là không dễ. Bởi lẽ không hề có quy tắc, lời hướng dẫn cho việc kiếm tiến. Chúng ta chỉ biết tìm kiếm những công việc có thu nhập cao, thế là vui rồi. Nhưng thực chất đằng sau những đồng tiền ấy luôn chứa đựng những cơ hội . cơ hội để ta tự do tài chính mãi về sau. Đó mới là mục đích chính mà ta cần nhắm tới. bộ quyển sách này sẽ giúp tôi và bạn thực hiện được điều đó. Chính quyển sách này đã giúp tôi tìm kiếm được những cơ hội cũng như biết cách lập kế hoạch sao cho hợp lí. Nhờ vậy biết sử dụng đồng tiền cho hiệu quả  và có lợi.
4
70130
2012-11-08 20:57:18
--------------------------
