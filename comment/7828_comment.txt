537593
7828
Truyện Tô Hoài luôn mang một màu sắc riêng, tuy là truyện về các con vật nhưng mang nhiều ý nghĩa và gợi ra những sự hồn nhiên,trong sáng của tuổi thơ
5
743590
2017-03-08 11:15:08
--------------------------
479766
7828
Tôi chưa từng được nghe tên cuốn sách này, nhưng tôi vẫn đặt mua, bởi nó là truyện của nhà văn Tô Hoài.
Quả đúng như vậy, giọng văn Tô Hoài không lẫn đi đâu được.
Vẫn là các loài động vật quen thuộc: Bọ Ngựa, chim chóc các loài, mèo, chó, dê, lợn... nhưng vào văn Tô Hoài, lại trở nên hoặc hoạt náo, hoặc trầm tư, hoặc tinh nghịch, hoặc hợm hĩnh, cũng suy nghĩ, tâm tư và tình cảm như con người.
17 câu chuyện nhỏ, đưa người đọc vào 1 thế giới các loài vật sống động và tươi vui, như 1 tấm vé đưa ta trở lại khoảng trời tuổi thơ đầy màu sắc... 

5
675088
2016-08-12 09:42:05
--------------------------
444607
7828
"Tôi : Thế đã đến Việt Nam chưa ?
Bồ Nông : Rồi.
Tôi : Thật đấy ư ?
Bồ Nông : Ai trên trái đất này bây giờ mà không náo nức muốn đến đất nước Việt Nam anh hùng. "
Đây là 1 đoạn trong " Chú bồ nông ở Sa-mác-can " mà mình yêu thích nhất! Tô Hoài đã đưa độc giả đến với 17 câu chuyện về các loài vật thân thuộc,gẫn gũi với loài người. Mỗi câu chuyện lại đưa ta về những không gian khác nhau của những miền quê Việt Nam. Nhiều kỉ niệm, kí ức đã sống lại với mình qua từng trang sách mà Tô Hoài mang đến. Mình khá thích cuốn sách này về cả nội dung và hình thức!
4
601943
2016-06-08 18:57:47
--------------------------
435094
7828
Chú Bồ Nông Ở Sa-Mác-Can là tuyển tập truyện ngắn viết về các loài vật. Nội dung khá nhẹ nhàng, mộc mạc, giản dị mà rất đỗi ấm áp và tình cảm. Những câu chuyện xoay quanh cuộc sống của con mèo, con chó, con dê,... thật gần gũi, thật chân thực xiết bao. Văn phong của nhà văn Tô Hoài thì khỏi nói rồi, sâu sắc, sinh động, từ ngữ vô cùng linh hoạt uyển chuyển và có phong vị riêng nên đọc có cảm giác tâm đắc. Còn về hình thức, tôi thấy NXB Kim Đồng khá đầu tư cho ấn phẩm này, bìa đẹp, minh họa tuyệt vời, trình bày cẩn thận, và giá tiền không hề mắc chút nào.
4
386342
2016-05-24 13:57:07
--------------------------
161485
7828
Lại O Chuột, ông Mèo, chú Dê, chị Bò hay chàng võ sĩ Bọ Ngựa...dần dần được đặt chân vào thế giới thật qua ngòi bút tuyệt vời của Tô Hoài. Những sự vật của ông rất gần rất quen mà đâu đó ta gặp trong sân, trong vườn hay chính trong nhà của chúng ta. Đọc truyện của ông mà ngỡ ra rằng muôn loài sao mà phong phú quá đều có cuộc đời như người vậy. Truyện dành cho con trẻ nên giọng văn rất trong trẻo, tranh minh họa nhiều. Tuy là tranh đen trắng nhưng vẫn rất đẹp, ngộ nghĩnh và đáng yêu.
5
534227
2015-02-28 01:36:58
--------------------------
