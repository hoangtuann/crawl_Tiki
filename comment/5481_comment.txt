312427
5481
đây là cuốn tập cơ bản rèn kỹ năng tập tô cho các con sắp vào lớp một. Tôi thấy cuốn tập được in trên giấy chất lượng tốt, in rõ ràng, tạo cảm giác ham muốn "tô vẽ" của các con. Tuy là con trai tôi còn khá lười trong việc tô chữ nhưng con vẫn hoàn thành tốt theo hướng dẫn tô nét của cuốn tập in (như tô từ trên xuống, từ trái qua...)
Các phụ Huynh có thể lựa cuốn này để hướng dẫn con làm quen với các chữ và các nét cơ bản đầu tiên. 
4
572177
2015-09-21 11:14:16
--------------------------
282189
5481
Sách tập tô nét căn bản giúp bé làm quen với cách cầm bút, cách tập viết trên giấy để chuẩn bị vào lớp một khỏi bỡ ngỡ. Bây giờ mà vào lớp một không biết gì thì rất tội cho bé vì không theo nổi bạn bè và bài học. Cha mẹ từ hè trước khi vào lớp một nên tập cho bé viết những nét cơ bản như sách giới thiệu để bé quen với việc cầm bút, cầm đúng và biết cách viết cũng như viết tốt và nhanh và chính xác hơn từ đó làm nền tảng để bé viết chữ cái và chữ số thành thạo hơn.
5
138619
2015-08-28 23:08:02
--------------------------
