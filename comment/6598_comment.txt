421600
6598
So với các tác phẩm khác của Dan Brown, pháo đài số không có lượng kiến thức khổng lồ về văn hóa, lịch sử mà chỉ có một vấn đề của thời đại công nghệ: bảo mật dữ liệu. Do đó, cốt truyện khá đơn giản. Nhưng cái tài của tác giả là lồng ghép nhiều bức tranh riêng biệt vào một câu chuyện, các nút thắt được mở ra từ từ qua từng chương, khiến người đọc hồi hộp theo dõi. Ranh giới giữa cái thiện và cái ác rất mơ hồ, cho đến chương cuối cùng bạn mới vỡ lẽ ra. Hãy đọc và phiêu lưu cùng các chuyên gia giải mã.
2
342849
2016-04-25 14:28:50
--------------------------
416191
6598
Tôi tìm đến "Pháo đài số" sau khi đọc xong tất cả các tác phẩm về cuộc phiêu lưu của Robert Langdon của Dan Brown. 
Cuốn sách này đã tạo ra cảm giác mới lạ cho tôi. Dù vẫn là cách viết xuất sắc trong miêu tả tâm lý nhân vật và xâu chuỗi các sự việc diễn ra rất logic của Dan Brown. Nhưng cuốn sách này đã chứng minh rõ hơn trí tưởng tượng tuyệt vời và không có giới hạn của ông. Một câu chuyện gay cấn, đầy rẫy nguy hiểm không lường trước được khiến tôi đi từ bất ngờ này sang bất ngờ khác và nhiều lần phải thót tim. 
Điểm trừ đáng tiếc duy nhất của cuốn sách này là bìa sách chưa truyền tải được đúng nội dung của cuốn sách.
3
281738
2016-04-14 17:25:08
--------------------------
404612
6598
Từ lúc cầm quyển sách trong tay, mình đã háo hức và mong đợi rất nhiều. Và một lần nữa, Dan Brown lại không làm mình thất vọng. Mình tìm đến Pháo đài số sau khi đọc xong thiên thần và ác quỷ, mật mã Davinci. Và cả 3 quyển này đều rất gay cấn, các tình tiết hấp dẫn khiến mình không thể rời mắt nổi. Rất khâm phục trí tưởng tượng tuyệt vời của tác giả. Tuy ông không miêu tả kỹ tâm lý nhân vật (cảm nhận riêng của mình) nhưng ông thật sự để lại cho mình rất nhiều ấn tượng bằng những giả thiết của ông. Ông đã chứng minh chúng một cách khoa học, thuyết phục. Nói chung đây là một cuốn sách đáng đọc. Becker đã trở thành nhân vật yêu thích của mình. Tuy nhiên, mình hơi thất vọng với bìa sách một chút, cảm giác nó vẫn chưa đủ để thể hiện nội dung truyện
4
1178040
2016-03-25 14:15:34
--------------------------
396212
6598
Khi gấp trang cuối cùng của cuốn sách lại, trong mình tồn tại một cảm giác khó tả, thực sự thì mình không tin là nó đã kết thúc! Cốt truyện phải nói là tuyệt vời, hấp dẫn và khó đoán, bạn không thể tin những gì mà tác giả dẫn dặt bạn đâu, người đứng đằng sau mọi chuyện là nhân vật khiến bạn bất ngờ nhất đấy, cái con người mà bạn tin tưởng nhất, khiến bạn rút ra một bài học quý giá trong chính cuộc sống này, nỗi đau lớn khó xoá nhoà là nỗi đau khi mà người ta tin tưởng nhất lại hãm hại và phản bội ta. Xây dựng hình tượng nhân vật phải nói là dấu ấn mạnh trong độc giả, nhân vật phản diện như Holihot hay Strathmore làm mình căm hận cay đắng, trái lại lại dành một tình thương cảm đặc biệt cho anh chàng David tội nghiệp, cho nàng Susan không mạnh mẽ như ta tưởng, à cái kết tuyệt vời lắm các bạn nhé, mình hoàn toàn tỉnh táo hơn Susan, với tính cách tự phụ của Holihot, mình tin chắc hắn tự tin sẽ hạ được mục tiêu David xấu số nên mới nhắn về cho chủ như vậy, chứ ngài Dan mà để David chết thì đây là cái kết dở chưa từng có trong lịch sự văn học tiểu thuyết, và dĩ nhiên, tác giả đáng kính của chúng ta sao có thể khiến độc giả thất vọng được nhỉ hihi, ngoài ra phần kết còn khai sáng về mối quan hệ giữa giám đốc tập đoàn Numatech Nadataka và Tankado, rất ấn tượng, nhiều chi tiết kinh dị và hành động làm mình ám ảnh 3,4 ngày. Tóm lại, chấm 5 sao là hoàn toàn thật lòng mình đó các bạn !
5
1173756
2016-03-13 07:04:26
--------------------------
390510
6598
Một lần nữa, Dan Brown lại tiếp tục khẳng định tài năng phi thường của mình trong lĩnh vực truyện trinh thám.Cuốn sách là câu chuyện của những thiên tài với bộ óc phi thường, những người đa tạo nên thế giới của các loại mật mã, những người đã biến những con số, những thuật toán khô khan trở thành thứ vũ khí sắc bén và là trung tâm của những cuộc rượt bắt đến nghẹt thở.Dan Brown đã thể hiện cho người đọc thấy được tư duy logic của ông cũng như những hiểu biết về mật mã hay các cơ quan tình báo Mỹ.Khi những con số khi tạo thành mật mã tuần hoàn đã nắm giữ quyền lực đủ để xoay vần nền an ninh của một quốc gia, quyết định sự sống còn của một tổ chức tình báo đã khiến câu chuyện trở nên kịch tính đến không ngờ
5
295322
2016-03-03 23:40:36
--------------------------
389966
6598
Pháo đài số là quyển thứ 2 mình đọc trong số các tác phẩm của Danbrown.
Những tình tiết gay cấn, những nguy hiểm cận kề, những kế hoạch, những âm mưu , thủ đoạn... tất cả được viết với mạch văn li kì, lôi cuốn, hồi hộp đến phút chót và kết quả thì không ai ngờ được nó lại đơn giản như thế , số 3 ^^
nhưng vẫn thích cái bìa sách trước hơn, cái bìa này nó cứ sao sao ý, không ma mị được như cái bìa có hình con mắt.
Một tác phẩm đáng đọc.
4
1157982
2016-03-03 01:06:36
--------------------------
375085
6598
Dan Brown là một tác gia nổi tiếng từ lâu lắm rồi, nhưng mình chưa đọc tác phẩm nào của ông cả nên khá tò mò, vì vậy mình chọn Pháo đài số - tác phẩm đầu tay của tác giả để bắt đầu. Chuyện khởi nguồn từ mối căm thù giữa hai dân tộc Mỹ - Nhật, hay nhỏ hơn là giữa anh chàng lập trình viên Takando với phó giám đốc cơ quan an ninh Hoa Kỳ Stratmore. Cái kết quá bất ngờ, làm mình phì cười vì cứ nghĩ Pháo đài số là cái gì đó bí ẩn lắm, hóa ra là chả có Pháo đài số nào cả, chỉ là một con sâu máy tính kịch độc. Điểm trừ là bản in chữ quá to, đọc mà có cảm giác đây là sách dành cho học sinh tiểu học vậy.
4
662390
2016-01-27 19:25:05
--------------------------
374649
6598
Truyện khá hay, nhưng chưa đủ để với tới " mật mã Da Vinci " trước đó của Dan Brown
Đúng là tác giả có một kiến thức thật sâu rộng, điều này thể hiện qua nhiều khía cạch của tác phẩm
Brown là người kể chuyện giỏi, các chương chuyện đan xen qua các không gian và thời gian khác nhau làm cốt truyện trở nên thực sự thú vị.
Tuy nhiên phần kết khá dài, nhiều người có thể đoán ra được, nhìn tổng quan thì đây là một cuốn sách khá hay và nhiều tình tiết thắt mở nút như bộ phim vậy
3
474979
2016-01-26 19:08:18
--------------------------
338938
6598
Nếu như những chương đầu tiền là hành trình đầy nhẹ nhàng và có phần chậm chạp thì những nửa cuối cuốn truyện là nhưng khúc nhạc dồn dập, buộc người đọc một khi đã cầm sách lên thì sẽ đọc liền mạch bởi những chi tiết lôi cuốn tiếp diễn liên tục.
Cùng với đó là cái kết hoàn toàn bất ngờ. Cho dù một người có óc sáng tạo đến mức độ nào cũng khó có thể đoán ra được cái kết. Ẩn chứa sau câu chuyện còn là tình người, những lối thoát không ngờ tới mà kẻ đặt bãy đưa ra cho nạn nhân của hắn. Câu chuyện hấp dẫn đến tận dòng chữ cuối cùng. Hoàn toàn xứng đáng với mức đánh giá cao nhất
5
940384
2015-11-17 09:01:24
--------------------------
332087
6598
tác phẩm này rất hay. ban đầu mình biết đến dan brown qua biểu tượng thất truyền, sau đó bắt đầu tìm đọc các tác phẩm khác, bây giờ mình đã có đủ bộ truyện của ông.
tác phẩm pháo đài số để lại rất nhiều ấn tượng cho bản thân mình. tác phẩm cho mình biết nhiều hơn của công việc của cục tình báo, khiến mình cuốn theo những chi tiết, tình tiết li kì, hồi hộp theo bước chân của susan. nhân vật susan cũng cho mình ấn tượng mạnh về người phụ nữ hiện đại thông minh, sắc xảo.
đây là tác phẩm mà bất kì một mọt truyện hay những người yêu thích trinh thám nên đọc

5
513275
2015-11-05 19:10:58
--------------------------
330739
6598
không còn là những biểu tượng cổ xưa hay ý nghĩa tâm linh nhiều mà thay vào đó là những mật mã với nhiều hình thức logic đòi hỏi óc tư duy, vận dụng của con người trong việc giải mã. những thông tin được tiết lộ, bị kiểm soát khiến cho người có cảm tưởng nó diễn ra trên phải  rất rộng, toàn cầu chứ không phải một khu vực nhất định nào đó. NSA, trung tâm CRYPTO xâm nhập mọi thứ, ta lại có cảm tưởng như đang nói về chuyện chính phủ Mỹ lén nghe nội dung thông tin của công dân ở nhiều nước vậy. li kì, đầy căng thẳng mới khiến độc giả thích thú.
4
568747
2015-11-03 09:57:07
--------------------------
291145
6598
Về hình thức, mình thấy chất lượng sách nhìn chung còn chưa tốt lắm. Giấy hơi vàng, gây mỏi mắt khi đọc, xốp và dễ rách. Bìa sách mềm, chưa có sự cứng cáp đặc trưng. Và sách có rất nhiều lỗi chính tả - một trong những điểm trừ lớn nhất. 
Còn về nội dung, thì đấy đúng là một tuyệt phẩm của Dan Brown với nội dung vô cùng ly kì và hấp dẫn. Mạch truyện gấp rút, khẩn trương, khiến người đọc khó có thể rời sách được. Nhìn chung, đây cũng là một cuốn sách khá tốt, và sẽ trở nên tuyệt hơn nếu được chữa những lỗi về hình thức đã nêu trên.
4
299660
2015-09-06 01:24:56
--------------------------
287013
6598
Vốn mê Dan Brown từ cuốn Mật mã Da Vinci, rồi Hỏa ngục, Điểm dối lừa, nên mình yên tâm mua Pháo đài số. Quả thực sách đọc rất hay, mạch truyện nhanh, càng về sau càng dồn dập nhưng vẫn rõ ràng, dễ hiểu, chỉ có những lúc nói về mật mã các kiểu là cần đọc kỹ một chút. Sách in đẹp, giấy xốp, có bao bì bọc kín bên ngoài. Chỉ bực mình là mật độ lỗi chính tả hơi nhiều, trong đó có nhiều lỗi rất cơ bản, rất đơn giản, không hiểu sao vẫn còn sót lại, đọc khá khó chịu, mong rằng sách sẽ được tái bản có sửa chữa để những người mua sau không phải khó chịu thế này nữa.
4
57459
2015-09-02 10:16:26
--------------------------
278455
6598
Về mặt hình thức, sách đẹp, nhẹ, giấy xốp hơi ngả vàng và cỡ chữ khá to nên đọc khá là thoải mái, tuy nhiên sách lại có khá nhiều lỗi chính tả ngay từ những trang đầu tiên, những lỗi này khá nhỏ và không ảnh hưởng gì đến nội dung truyện nhưng đôi lúc vẫn khiến mình cảm thấy khó chịu. Vì sách dày mà lại có thiết kế bìa mỏng nên khi đọc nếu không cẩn thận sẽ khiến sách dễ bị bong gáy.
Về mặt nội dung, mình hoàn toàn hài lòng với câu chuyện li kì này. Mình bị cuốn hút ngay từ khi câu chuyện mới bắt đầu và ngày càng hồi hộp hơn trước những tình tiết gay cấn về phía cuối truyện. Không chỉ là một cuốn tiểu thuyết đơn thuần, tác phẩm này còn cung cấp cho mình nhiều kiến thức về mật mã, mã hóa, số hóa... 
4
138880
2015-08-25 21:45:13
--------------------------
276366
6598
Trước nay mình chỉ coi phim của Dan Brown và đây là lần đầu tiên mình đọc tác phẩm của ông. Quả thật dù là một trong những tác phẩm lớn đầu tay nhưng truyện đã thể hiện kịch tính ngay từ những trang đầu tiên. Dù là một hành trình nguy hiểm và phức tạp nhưng truyện lại rất dễ theo dõi, phải nói mình cực kì khâm phục ngòi bút của tác giả. Cuốn sách giúp mình biết được một ít về thuật giải mã, biết tạo ra vài mật mã đơn giản, và dù không đi sâu vào tin học nhưng mình cũng biết được kha khá về thế giới công nghệ thông tin hiện đại qua cuốn sách này. Tuy nhiên sản phẩm có một điểm trừ khá lớn là có quá nhiều lỗi chính tả. Cứ đọc vài trang là mình lại thấy một lỗi, đôi khi là những lỗi khá cơ bản nữa. Bù lại thì sách rất nhẹ, chữ in to, rõ ràng đọc rất thoải mái.
4
131208
2015-08-23 22:08:18
--------------------------
265141
6598
Cảm giác đầu tiên khi cầm cuốn sách là quá nhẹ so với kích thước khủng của sách, nhẹ hửng luôn. Tuy nhiên đây lại là ưu điểm vì sách cầm đọc rất dễ dàng không bị mỏi tay, nhược điểm là giấy mỏng, dễ rách, bìa cũng mỏng và dễ bong gáy. Mình đọc trang đầu tiên là bị cuốn vô luôn. Mình rất ngạc nhiên về lượng kiến thức của tác giả. Tác phẩm đề cập đến một vấn đề nhạy cảm là an ninh quốc gia mà cụ thể ở đây là cơ quan tình báo cao nhất của Mỹ phải giải một mật mã bí ẩn và phức tạp. Với bút lực mạnh mẽ, Dan Brown đã dẫn dắt người đọc vào thế giới của tình báo và mê cung công nghệ thông tin, sáng tạo nhiều tình tiết gay cấn đẩy mạch truyện lên đến đỉnh điểm rồi từ đó các nút thắt được gỡ ra, mật mã được gải đáp một cách đơn giản và kết thúc rất bất ngờ. Rất cảm ơn tác giả vì đã cho mình một lượng thức bổ ích, tuy nhiên có vài từ chuyên môn của dân công nghệ mình không hiểu lắm *.*  Nhưng mình cảm thấy quyển sách rất xứng đáng với số tiền bỏ ra..
5
719449
2015-08-13 19:20:48
--------------------------
262090
6598
Truyện của Dan Brown luôn là những tác phẩm kinh điển. Trong các trang truyện của  mình, D.B luôn thể hiện một sự hiểu biết rộng lớn về tất cả mọi thứ, sự đầu tư về các chuyên ngành mà như ngay trong cuốn Pháo đài số là ngành mật mã. Phải nói những cuốn truyện của D.B là của một chuyên gia được đào tạo chuyện nghiệp chắp bút viết nên. Tuy là tác phẩm có tên là Pháo đài số nhưng đọc truyện mình không cảm thấy khô khan chút nào, bởi vì xuyên suốt truyện là tình yêu của 2 con người tài giỏi, luôn khát khao tìm về với nhau.
4
366277
2015-08-11 17:11:21
--------------------------
258640
6598
Đây là cuốn truyện đầu tiên của Dan Brown mà tôi đọc. Truyện có bố cục khá hay, các chương được sắp đặt sao cho ngắn dần để đẩy nhanh, tăng thêm không khí căng thẳng, khẩn trương khiến khi đọc ta có cảm giác hồi hộp, muốn lật nhanh để biết tiếp diễn biến. Đồng thời, cuốn sách cũng gồm 2 truyện song song nhau, theo chân nhân vật chính Susan và người yêu của cô. Pháo Đài Số nói về mật mã máy tính và một hệ thống máy móc cùng cơ sở của NSA tuy không hợp lý với thực tế nhưng chắc chắn cũng khiến độc giả hài lòng và bị cuốn hút cho đến trang cuối cùng. 
5
740662
2015-08-08 18:22:44
--------------------------
213844
6598
Đây là một tác phẩm hay. Tôi đặc biệt rất thích các tác phẩm tiểu thuyết trinh thám, cung cấp nhiều kiến thức cho người đọc. Những kiến thức về mật mã và giải mã khiến tôi ngạc nhiên và thích thú, Vốn hiểu biết tăng lên rất nhiều. Dan Brown vốn có tài trong việc kết hợp giữa khoa học và trí tưởng tượng, vì vậy mặc dù khối lượng kiến thức trong tác phẩm không quá sâu rộng nhưng vẫn khiến người đọc phải hồi hộp dõi theo từng trang sách. Và cái kết lúc nào cũng bất ngờ.
Về hình thức: Thì loại giấy cũng như các cuốn khác của Dan Brown, xốp, nhẹ tác phẩm 600 trang nhưng không hề cảm thấy nặng. Phần bìa tuy biết là bìa mềm sẽ mỏng, nhưng vẫn thấy quá mỏng manh so với bề dày của cuốn sách, rất dễ rách và bong gáy.
Và còn một điều nữa là lỗi chính tả rất nhiều. Đó chính là điểm trừ về hình thức, in ấn.
4
136087
2015-06-24 10:35:04
--------------------------
158561
6598
Trong từng cuốn sách, hiện thân của dan brown rất khác nhau. Ở thiên thần và ác quỷ, ông chứng minh rằng mình là một giáo hoàng thực thụ có hiểu biết sâu rộng, ở hoả ngục, ông cho ta thấy một nhà khoa học với tâm hồn nghệ thuật của văn thơ và hội hoạ, thế nhưng ở Pháo đài số, chúng ta có thể nhận ra trong đó không chỉ là thiên tài trinh thám mà còn cả một nhà lập trình, giải mã bậc nhất. Đọc sách, Dan Brown sẽ đưa chúng ta vào thế giới thông tin của chính phủ Mĩ với vô vàn những thuật toán lý thú. Có thể nói, đây là cuốn sách hay nhất mà tôi từng đọc.
5
487348
2015-02-13 09:56:19
--------------------------
157438
6598
Mỗi lần đọc một cuốn sách của Dan Brown, tôi không chỉ thán phục khả năng viết tiểu thuyết của ông mà còn là vốn hiểu biết sâu đến mức chuyên ngành của ông về tất cả mọi lĩnh vực.Trong "Pháo đài số", ông đã đưa người đọc đến một trong số những cơ quan cao nhất nước Mỹ, NSA, và đưa đến một trong số những vấn đề nổi cộm nhất về tin học hiện nay, chính là cuộc chiến về thông tin. Câu chuyện bắt đầu việc NSA bị đe dọa bởi một lập trình viên về một thuật toán không có lời giải, một thứ có thể đe dọa bảo mật thông tin trên toàn thế giới. Ta được chứng kiến hành trình tìm cách đánh bại "Pháo đài số" của Susan, những âm mưu sâu xa đằng sau NSA và những vụ án mạng gây ra ở Tây Ban Nha bởi một sát thủ để truy tìm mật mã. Kết thúc truyện khiến ta không khỏi bất ngờ, không chỉ bởi vì cái cách mà mối nguy hiểm được đẩy lên tới đỉnh điểm và được giải quyết hết sức đơn giản mà còn bởi khả năng phán đoán và giải quyết quyết đoán của các nhân vật. Truyện đặt ra câu hỏi về lương tâm và đạo đức của con người đối với những việc mình làm thông qua hình ảnh các lập trình viên của NSA và để lại trong người đọc nhiều suy ngẫm.
5
291067
2015-02-08 22:05:22
--------------------------
156163
6598
đây là cuốn sách thứ 4 của Dan Brown mà mình đọc. Nó thực sự rất  cuốn hút người đọc khiến mình đọc liền mạch cả chiều
Cũng giống như những tác phẩm trước, ông lại mang cho ng đọc 1 khối thông tin mới về những tổ chức, về các thuật toán của dân IT, các   con số, mật mã lôi cuốn người đọc. Rất thích  hợp cho những nghười đam mê truyện trinh thám như  mình. Khác vs những tác phẩm khác về Robert Langdom, truyện kể về  1 nhân vật khác là Susan Fletcher, người phụ nữ thông minh , cuốn hút vs đầy tình tiết bất ngờ  diễn ra
điều mình ko thích ở cuốn sách chính là mùi sách hơi nồng và bị sần sùi, . Nhưng dù gì đây là cuốn sách hay nhất của Brown mà mình từng đọc
5
332492
2015-02-03 13:49:52
--------------------------
143050
6598
Đây là một trong những quyển của Dan khi đọc mình có thể nhìn thấy trước được cái kết. Việc tác giả xen kẽ giữa câu chuyện của Susan và David cũng khá thú vị, nhưng chưa hẳn là thực tế lắm. Cá nhân mình thấy không phải là quá xuất sắc so với các tác phẩm khác cảu Dan Brown, nhưng khá đáng để theo dõi để nhìn thấy khía cạnh mới trong truyện trinh thám, tìm hiểu nhiều hơn về chuyên nghành và các tuyến nhân vật với tính cách mới lạ.

Điểm trừ của tác phẩm là phần in ấn. Thật sự kém vì giấy mỏng, sách nhẹ và lỗi chính tả rất nhiều. Nếu nhà xuất bản có tính sản xuất thêm truyện của Dan mình thật sự mong là sẽ khắc phục những lỗi không đáng trên để tác phẩm có thể đến với tay người đọc trọn vẹn hơn.
4
159554
2014-12-21 12:21:55
--------------------------
130667
6598
Đây có thể coi là một trong những tác phẩm đầu tiên của Dan Brown, mặc dù không nổi tiếng như Mật mã Da Vinci hay Thiên thần và ác quỷ, song cũng là một tác phẩm ấn tượng thời đó. Tôi không phải dân IT nên khi đọc cuốn sách này, những kiến thức về mật mã và giải mã khiến tôi hầu như choáng ngợp và thích thú. Dan Brown vốn có tài trong việc kết hợp giữa khoa học và trí tưởng tượng, vì vậy mặc dù khối lượng kiến thức trong tác phẩm không quá sâu rộng nhưng vẫn khiến người đọc phải hồi hộp dõi theo từng trang sách. Và cái kết lúc nào cũng bất ngờ, đó cũng là một thành công của nhà văn viết truyện giả tưởng này. 

Thêm vào đó, ấn phẩm này bìa đẹp hơn bản trước, trông đồng bộ với các cuốn sách khác của Dan Brown. Nếu là fan của thể loại trinh thám, hay đặc biệt hơn là của Dan Brown, bạn không nên bỏ qua tác phẩm này.
4
387413
2014-10-19 11:46:47
--------------------------
125070
6598
Đây là truyện đầu tiên của Dan Brown mà mình đọc hồi sinh viên. Sau 8 năm mình đọc lại mà vẫn nguyên cảm giác hồi hộp như ban đầu. Nhưng ở lần đọc thứ 2 thì dễ hiểu hơn do ít nhiều đã có kiến thức về máy tính. Thực tế thì các thuật ngữ chuyên ngành IT trong truyện cũng được diễn giải khá đơn giản rồi. Truyện rất hay, nút thắt được dâng lên đỉnh điểm từ giữa truyện, kết thúc cũng đầy bất ngờ. Cốt truyện khác so với loạt truyện về giáo sư Langdon ở phần sau và có nét hấp dẫn riêng.

Mỗi tội chi tiết về tay sát thủ thì chưa logic lắm, khó có chuyện 1 sát thủ chuyên nghiệp mà khó lòng hạ sát 1 thày giáo đến như vậy, kể cả là may mắn. Song chi tiết này là dễ hiểu vì mình nghĩ tác giả cũng đành phải làm thế chứ không thì làm gì còn truyện. Cũng như giáo sư Langdon ở các bộ về sau thì người đọc biết chắc chắn có gặp nguy hiểm thế nào thì vẫn sẽ sống sót. Nhưng chi tiết đó cũng không làm giảm tính hồi hộp của tác phẩm. Một tác phẩm 5 sao mà sẽ mua một ngày nào đó để sưu tập.
5
411096
2014-09-10 22:19:40
--------------------------
118891
6598
Pháo đài số là quyển sách đầu tiên mình đọc của Dan Brown nhưng mình thật sự bị nó lôi cuốn, văn phong gọn ghẽ, khúc chiết và cực kì logic,cuốn sách cung cấp cho ta nhiều kiến thức mới lạ và đầy hấp dẫn,cho ta cái nhìn đầy mới mẻ nhưng tăm tối của cục tình báo trung ương Mỹ CIA, đọc quyển sách này, bạn sẽ bị cuốn theo trên từng bước đi của nhân vật, những bí mật đan xen bí mật, mật mã giấu trong mật mã, một mật mã vòng đe dọa đến sự thay đổi toàn thế giới. Nhịp truyện nhanh và gay cấn khiến bạn không thể rời quyển sách cho đến khi bạn đọc xong nó. Quyển sách thật sự là món quà cho những ai thích đọc sách trinh thám, hành động như mình, cực kì hay và không thể bỏ qua.
5
86983
2014-07-31 19:18:30
--------------------------
118596
6598
Về nội dung, không có gì phải phàn nàn về "Pháo Đài Số". Vẫn những tình tiết gây cấn, những nguy hiểm cận kề, những âm mưu, dối trá... Nhân vật đáng chú ý ở đây không hẳn đã là David Becker hay Susan Fletcher, những con người thừa thông minh và đủ các chuẩn mực để trở thành nhân vật chính, đáng hâm mộ của độc giả. Nhân vật khiến tôi suy nghĩ và ấn tượng lại là Trevor Strathmore, một người tài giỏi, trọng danh dự, nhưng đến cuối lại đẩy bản thân vào bế tắc. Suy nghĩ, hành động của ông dần bị cuốn theo cảm tính, cuốn theo cái ông gọi là tình yêu (yêu nước, và yêu người con gái ông xem như thiên thần).
Về hình thức, tôi có hơi tiếc nuối một chút khi đã lựa chọn ấn phẩm bìa mềm. Ưu điểm ở đây là sách tương đối nhẹ và giá thành thấp hơn. Tuy nhiên, chất lượng giấy không được tốt lắm, mùi giấy khá nặng (bạn nào có cái mũi nhạy cảm nên cân nhắc một chút trước khi mua). Xét cho cùng, một cuốn sách dày và nội dung lôi cuốn như vậy, thì bạn cũng không nên ngại ngần chọn ấn phẩm có chất lượng cao một chút. Hình thức đẹp, sang trọng sẽ phần nào tăng giá trị sách hơn, và còn giúp bạn bảo quản lâu hơn.
5
313783
2014-07-30 08:45:47
--------------------------
118167
6598
Mình đọc cuốn sách này từ khi nó mới phát hành, cũng khá lâu rồi. Muốn tìm mua lại sách thật rất khó, thật may là đã được xuất bản lại.
Cuốn sách này cần phải đọc trước và chung với Điểm dối lừa sau đó Thiên thần và ác quỷ, sau đó mới đọc Mật mã Davinci (Nhiều bạn không biết thấy the Davinvi code hot quá đọc trước là hố đó).
Nhân vật chính là Susan và Becker. Lần này họ phải chiến đấu với cái gọi là tội phạm vi tính nhằm bảo vệ cái mà chính chúng ta cũng không biết nó tốt hay xấu. Khi mà NSA, trung tâm CRYPTO đã lén lút xâm nhập tất cả bí mật của chúng ta bằng cách đọc trộm mail mà chúng ta cứ ngỡ là an toàn và tuyệt đối bảo mật bằng TRANSLTR. 
Phần truyện viết với mạch văn li kì hấp dẫn, hồi hộp đến phút chót và kết quả thì không ai ngờ tới là nó dễ đến thế (Y như Davinci, đọc đi lòng vòng thót tim rồi bất ngờ).
Điểm trừ là mình không thích cái bìa, thích cái bìa có hình con mắt hơn trông nó ma mị và nguy hiểm hơn nhiều.
Nói chung tác phẩm này rất rất đáng để đọc.
5
195704
2014-07-26 16:43:36
--------------------------
