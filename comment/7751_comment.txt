269511
7751
Quyển sách hấp dẫn này sẽ là tác phẩm đọc thiết yếu cho bất cứ ai quan tâm đến việc nghiên cứu và tranh luận về chương trình học cũng như những ai đặc biệt quan tâm đến xã hội học giáo dục. Đây là quyển sách đúc kết đầy đủ những tư tưởng của tác giả, là kết quả của một quá trình phản tư trong nhiều thập kỷ về tư tưởng và lý thuyết của chính ông, giữ một vai trò đột phá trong các trào lưu xã hội học giáo dục hiên đại. Chất lượng của bản dịch rất tốt, giữ được nguyên ý của tác giả.
5
295088
2015-08-17 14:46:18
--------------------------
