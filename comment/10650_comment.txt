377836
10650
Vị thế mà chàng trai có được chính là từ những việc làm thất thoát từ hạn chế suy nghĩ khiến các vướng mắc học được cá tính lớn mạnh tới từ trong sức lực cao lớn khi khỏi biến cố tới từ trong chính gia đình này mỗi khi không có một người nào chứng kiến đến sợ hãi , đại náo trại thanh phong kết thúc cho chuỗi ngày bi tráng , nhổ bỏ gốc rễ tồn tại với thế giới tự nhiên còn sao phải uổng phí ngày lĩnh công sức dồn vào hết đời toàn bộ các bậc cao tài .
5
402468
2016-02-03 15:04:37
--------------------------
272400
10650
Vậy là bộ tiểu thuyết Thủy Hử ( câu chuyện nơi bến nước ) của Thị Nại Am đến đây là kết thúc rồi . 
Mỗi tập truyện đều có một nhân vật chủ chốt, từng câu chuyện riêng lẻ mà lại gắn kết với nhau vô cùng chặt chẽ . Người xấu có, người tốt cũng có, tuy không phải lúc nào kẻ xấu cũng bị trừng phạt nhưng chính vì vậy mới làm nên cái thực của của tác phẩm . Trong cuộc sống không phải lúc nào cái ác cũng thua, cái thiện cũng thắng, nhưng làm người phải có quan điểm riêng và không để những điều tệ hại xấu xa làm lung lạc . 
Bộ truyện này giống như một lời nhắc nhở cũng giống như một trang sử hào hùng được hiện lên trước mắt người đọc .
5
335856
2015-08-20 01:12:47
--------------------------
