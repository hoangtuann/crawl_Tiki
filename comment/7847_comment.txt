454447
7847
Poster hình dạng này bao gồm các hình dạng cơ bản và kèm với đồ vật quen thuộc để tập sự liên tưởng cho bé.
Phải khen là màu sắc in ấn rất đẹp, tươi và "thật", hình ảnh rõ ràng ,sắc nét chứ không bị mờ, nhoè như poster trước mình mua.
Mình rất ưng sản phẩm này, mới nhận về tối giở ra cho cháu mình xem nó cũng rất thích, chỉ vài lần là đã biết rồi.
Về khoản giao hàng, tiki không nên giao vào thứ 7 vì thông thường địa chỉ giao người dùng sẽ cung cấp địa chỉ ở cơ quan, thứ 7 sẽ nghỉ.
5
895234
2016-06-21 22:39:42
--------------------------
301152
7847
Chỉ với các hình đơn giản nhưng poster đã đưa ra rất nhiều hình ảnh minh họa thú vị, giúp các bé dễ hình dung và liên tưởng, giúp các bé dễ nhớ và dễ phân biệt các loại hình dạng. Màu sắc tươi tắn, sinh động, hình ảnh rõ nét, chân thực thu hút được các bé. Bên cạnh đó với song ngữ Anh Việt giúp các bé vừa học, vừa chơi, vừa khám phá ngôn ngữ. Bé nhà mình đã phân biệt được các hình dạng cơ bản rồi. Rất bổ ích và thú vị. Cám ơn tiki. 
5
110777
2015-09-14 14:59:12
--------------------------
293367
7847
Poster gồm những hình dạng cơ bản, giúp bé nhận biết và phân biệt các hình căn bản, tưởng tượng hình học. Ngoài các hình dạng, poster có chú thích cách gọi các loại hình bằng tiếng Anh và Việt rất hữu ích và tiện lợi để dạy bé. Bé nhà mình giờ biết các hình rồi hay liên tưởng các vật dụng gia đình lắm, vd cái bàn hình chữ nhật, cái bánh hình tròn,... Có điều khi nhận hàng không biết do đóng gói hay vận chuyển mà poster mình nhận được bị gãy gập, phải đè lại cho thẳng, nhưng dù sao khi dán lên tường gia cố các cạnh thẳng lại cũng không sao.
4
138619
2015-09-08 11:10:11
--------------------------
