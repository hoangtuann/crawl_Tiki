541613
7759
Thấy cuốn này có bìa theo phong cách mới nên mua thử xem sao dù giá hơi cao. Nội dung thì không có gì cần nói vì ai đọc Đôremon rồi đều biết. Phần thiết kế bìa cứng có một bìa rời bọc ngoài cơ bản là đẹp. Khổ truyện hơi nhỏ nên nếu xét về số trang theo lượng giấy thì cũng chỉ bằng những cuốn bình thường thôi. Chất giấy như các truyện khác nhưng giá bìa tận 45.000, giá lúc mình mua là 40.000. Có thể nói là hơn được cái bìa nhưng như vậy giá này quá cao. Cuốn bình thướng giá 16.000 vậy cuốn này nếu 20.000 đến 24.000 hợp lí hơn. Nếu bạn nào mua  sưu tập thì được còn mua đọc thì mua những cuốn bình thường cũng được cho tiết kiệm chi phí.
4
148734
2017-03-14 17:10:20
--------------------------
394752
7759
Phải nói là mình cực kì may mắn khi mua được cuốn này với giá sale 50%. Cảm ơn Tiki.
Bìa cứng kèm bìa rời ở ngoài. Nhỏ bằng lòng bàn tay, đút túi mang đi đâu cũng được. Tập này là về những câu chuyện cảm động nên đương nhiên mình rất thích. Sau gần chục năm mình mới đọc lại Doraemon ấy chứ mà vẫn khóc như thường. Tuổi thơ đúng là không dễ gì mà xóa nhòa được. Nếu có nhiều tiền hơn chắc chắn mình sẽ mua cả bộ. Bởi vì nhắc lại là sách cực kì đẹp ấy.
5
302453
2016-03-10 21:25:11
--------------------------
310025
7759
Thật ra lúc mới nhận được sách, cảm thấy hơi hụt hẫng.Vốn dĩ đặt sách không hay quan tâm lắm đến thông tin về kích cỡ.Lúc nhận cứ tưởng cả 1 bộ to về trưng bày giá sách với đống truyện sưu tầm cho thích, nhưng đến lúc nhận sách thì lại là sách khổ nhỏ. Nhưng mang về xem thì vẫn thấy rất ưng ý, cảm giác trân trọng những quyển truyện này vô cùng.Đó chính là thứ tài sản quý giá mà mình sẽ gìn giữ mãi mãi, bây giờ cũng như về sau này, có thể mai sau cho con mình đọc thì sao.Yêu lắm Doraemon.
5
698658
2015-09-19 11:37:18
--------------------------
304812
7759
Tập này tổng hợp những câu chuyện cảm động trong các tập truyện ngắn, mỗi câu chuyện lại như một sự bất ngờ mà chú mèo máy đem đến cho độc giả mọi lứa tuổi. Dù đã phát hành từ rất lâu nhưng mèo máy Doraemon vẫn mê hoặc độc giả khiến ai cũng không ngừng yêu thích chú. Sách khổ nhỏ nên cầm rất vừa tay, bìa sách cứng cáp và giấy tốt, hơi ngả vàng và mỏng nhẹ nên cầm đọc rất dễ dàng. Tuy nhiên do phiên bản đặc biệt nên giá khá cao. Mình đang cố sưu tầm hết bộ
4
13083
2015-09-16 17:35:59
--------------------------
300369
7759
 Sách là bìa cứng và có kèm cả bookmark tiện lợi cho việc đọc sách nên mình rất hài lòng. Vì vậy mà giá thành của nó cũng đắt hơn bình thường rất nhiều. Điều đó khiến cuốn sách khó đến tay bạn đọc hơn, giá như tiki nhập thêm cả bản bìa mềm nữa thì sẽ có thêm sự lựa chọn cho những ai đã đam mê chú mèo mấy mà không có điều kiện để bỏ ra 450 nghìn đồng để mua trọn bộ 10 tập truyện. Nội dung của truyện thì miễn bản vì chú kèo máy Doremon lúc nào cũng mang đến những câu truyện rất thú vị
5
356167
2015-09-13 21:53:50
--------------------------
268496
7759
Khi truyện được gửi về, cầm trong tay cuốn truyện này, mình cảm thấy hơi ngạc nhiên. Vốn khi mua truyện tranh mình cũng không quan tâm lắm đến kích thước nên khi cầm, mình thấy nó hơi nhỏ so với những truyện tranh khác, chỉ có 10.5 x15 cm nhưng bù lại, số trang cũng nhiều hơn.
Đầu tiên về phần bìa, vừa giản dị vừa dễ thương, có điều là bìa rời nên khó bảo quản được tốt, khi đặt lên kệ sách mà sơ ý 1 chút là bìa sách bị nhăn ngay. Có điều, sách là bìa cứng và có kèm cả bookmark (sợi dây đỏ) tiện lợi cho việc đọc sách nên mình rất hài lòng.
Về hình thức, mình luôn thích font chữ của Kim Đồng trong Doraemon, vừa tròn trịa lại dễ thương, đọc rất thích mắt. Mực in cũng rất đậm, không bị mờ hay bị nhòe gì cả, hơn nữa giấy sờ cũng khá tốt. Nhưng có 1 khuyết điểm là khi mình đọc, mực dễ bị dính vào ngón tay lắm nên khi lật sang trang khác, thường khiến trang bị dính bẩn, có khi còn dính vào bìa sách trắng tinh nữa, nhìn khó chịu lắm. Mình thấy tốt nhất khi mua về nên bọc sách lại cho kĩ, để bìa không bị dính bẩn, lại không bị rớt ra trong khi cầm đọc (vì đây là bìa rời) nữa. 
Về nội dung thì khỏi phải bàn cãi nữa rồi. Tập hợp những câu chuyện cảm động trong Doraemon rất cảm động và đáng suy ngẫm.
Nói chung, cuốn truyện này rất đáng đọc và đáng mua.
4
145185
2015-08-16 15:33:52
--------------------------
240740
7759
Mạch chính của bộ Doraemon từ xưa đến giờ vẫn là tạo những giây phút vui vẻ cho người đọc, vì vậy nên tuy tập 5 của Doraemon theo chủ đề là Những câu chuyện cảm động nhưng mình đọc vẫn thấy rất hài hước, đáng yêu và hấp dẫn. Đặc biệt câu chuyện về bồ công anh, đặc biệt là Tạm biệt Doraemon và Kỷ niệm về bà lần nào đọc lại cũng làm mình sụt sùi vì cảm động và chạm vào nơi sâu nhất, và mình nghĩ nhiều bạn đọc những mẩu truyện trên cũng cảm thấy rất xúc động giống như mình. 
5
120276
2015-07-24 21:45:31
--------------------------
