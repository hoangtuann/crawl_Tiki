332025
2847
Tập sách gòm ba câu chuyện:"Chiếc đuôi của gấu", "Chú khỉ thông minh", "Bóng cây biết chạy". Những câu chuyện nhẹ nhàng, dí dỏm.
Sự ngờ nghệch và ngốc nghếch của gấu đã bị cáo lừa đến cùng, cũng vì quá tin bạn nên gấu đã bị đứt đi cái đuôi đẹp đẽ của mình, và cáo vẫn luôn là cáo gian xảo của khu rừng xanh.
Còn chú khỉ nhỏ vì muốn trị tội cọp vì đã cắn , làm bị thương chân thỏ, nên thỏ đã nghĩ ra kế và tìm cách làm cho những hàm răng săc nhọn của cọp phải bị rụng hết. Khỉ con rất thông minh. Mỗi ngày, khỉ đều mang kẹo đến cho cọp, khi răng cọp bị đau khỉ trực chờ sẵn lấy kìm nhổ hết răng cọp giữ, cứ như thế cọp không còn một chiếc răng nào để cắn , cũng như ăn thịt các loài thú khác. 
Và cuối cùng là bài học mà bác dê đã giải thích cho heo con về sự di chuyển của bóng cây, đó là do sự di chuyển của mặt trời.
Mỗi con vật, mỗi câu chuyện riêng vừa hài hước, vừa thú vị
5
680594
2015-11-05 16:24:06
--------------------------
