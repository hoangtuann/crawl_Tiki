455514
6824
Chú vịt con đúng là chú vịt con. Chú không hiểu vì sao trên bầu trời đầy sao, dưới mặt ao cũng có vô vàn các ngôi sao mà sau khi chú đã lặn xuống ao ì oạp ăn sao rồi mà khi lên bờ những ngôi sao vẫn ở đó. Ngốc nghếch hơn là khi Vịt mẹ hỏi các ngôi sao có mùi vị như thế nào thì Vịt con mới chợt nhớ và tự ngẫm lại là ừ nhỉ, những ngôi sao có mùi vị như thế nào nhỉ? Thực ra câu chuyện này bé lớn một chút có thể trả lời câu hỏi vì sao. Còn bé nhỏ thì chắc cũng giống Vịt con không hiểu nổi.
4
1433819
2016-06-22 16:33:08
--------------------------
334965
6824
Cuốn “ Vịt con ăn sao” là một trong 8 cuốn trong bộ “ Những người bạn ngộ nghĩnh” của Nhà xuất bản Nhã Nam. Truyện có tổng cộng là 19 trang. Bạn Vịt nhìn lên bầu trời ban đêm thấy có rất nhiều ngôi sao và nghĩ rằng không biết có cách nào ăn được những ngôi sao đó không. Rồi bạn lại nhìn xuống ao, thấy hình ảnh những ngôi sao trên mặt ao, bạn cứ ngỡ đó chính là những ngôi sao bạn nhìn thấy trên bầu trời kia và bạn liền lội xuống ao để ăn những vì sao đó. Nhưng khi mẹ bạn Vịt hỏi bạn “ ngôi sao có vị gì?” thì bạn lại không trả lời được. Đúng là trẻ con có cách nghĩ hoàn toàn khác với người lớn.
3
15022
2015-11-10 12:07:22
--------------------------
314173
6824
Bằng những câu chuyện thật đơn giản xoay quanh những người bạn nhỏ, tạo nên những triết lý nhân văn, mang tính giáo dục cao đối với con trẻ. Mình rất thích cách thiết kế của sách, màu sắc hài hòa sinh động. dùng những cuốn sách này dạy cho con biết về thế giới xung quanh và những bài học đầu đời thật ý nghĩa. Rất mong sẽ có nhiều những cuốn sách hay như thế này ra đời để tạo ra một nguồn kiến thức nuôi dưỡng tâm hồn con trẻ trong quá trình trưởng thành. Mình rất thích những câu truyện trong bộ sách này. Bé nhà mình cũng rất thích và cứ kêu mẹ kể đi kể lại.
4
84640
2015-09-25 15:25:34
--------------------------
286784
6824
Mình mua một bộ truyện này luôn, trong đó có truyện này là mình ít thích nhất về cả nội dung lẫn hình ảnh, dù nét vẽ cũng rất đẹp rồi.
Nhưng bé nhà mình thì nói mẹ đọc đi đọc lại, đến tối ngủ vẫn nhờ mẹ kể lại chuyện con Vịt đớp đớp?
Mình nghĩ tư duy trẻ con và người lớn khác nhau. Có thể mình thích ngộ ngĩnh, dễ thương, còn các bé thì lại thích những điều mới lạ, và cần lý giải? "Vịt con ăn sao" theo mình là một câu chuyện như thế!
3
614101
2015-09-01 23:39:59
--------------------------
252258
6824
Câu chuyện ngộ nghĩnh này kể về một chú vịt nhỏ muốn đớp sao dưới ao. Chỉ thế thôi, và chẳng có chuyện gì xảy ra cả. Câu chuyện vui vui và buồn cười sẽ khiến bé thích thú, bởi sự nhầm lẫn ngây thơ của chú vịt. Bố mẹ có thể vừa đọc cho bé vừa hỏi bé, vịt con đã nhầm như thế nào? Sao lại có những ngôi sao ở dưới ao nhỉ? Bạn vịt có ăn được những ngôi sao đó không....? Đặt câu hỏi cũng là cách giúp bé tư duy và phát triển trí não đấy.
3
82774
2015-08-03 15:48:55
--------------------------
