369400
9614
Mình đã tìm mua một loạt những quyển sách về mẹ và bé do các bác sĩ nước ngoài viết và được bác sĩ Nguyễn Thị Ngọc Phượng hiệu đính, do mình tin rằng kiến thức chuyên môn của các bác sĩ sẽ cho mình thông tin chính xác nhất. 

Quả thật đúng như vậy, quyển sách này cung cấp cho người đọc các kiến thức rất khoa học về sự phát triển của bé, qua đó giúp mình theo dõi sức khỏe và hỗ trợ giúp bé phát triển. Cái mình mong đợi thêm nữa là phần thực hành thì không có nhiều. Điều mình thích ở quyển này là cách viết ngắn gọn, trình bày khoa học, logic, không lặp đi lặp lại như một số quyển sách dài dòng khác. 

Theo mình nghĩ để giúp bé phát triển toàn diện thì bạn cần đọc nhiều quyển sách có nhiều quan điểm và phương pháp khác nhau để có cái nhìn toàn diện, rồi bạn sẽ biết điều gì là thích hợp cho con của mình.   
4
3602
2016-01-15 18:13:32
--------------------------
