453334
6707
Cũng giống như bao câu chuyện cổ tích khác, Lọ Lem cũng gợi lại cho mình nhiều kỷ niệm tuổi thơ. Câu chuyện vẫn giống như phiên bản hoạt hình mà mình từng xem. Truyện kể về cô bé Lọ Lem phải sống khổ sở cùng mụ dì ghẻ và hai cô chị đanh đá. Dù bị bắt làm việc vất vả, cô vẫn rất đẹp, vẫn luôn yêu đời, thương người và ấp ủ giấc mơ về một vị hoàng tử. Cô bé ở hiền gặp lành, nhận được sự giúp đỡ của bà tiên, cưới được chàng hoàng tử và sống hạnh phúc mãi mãi.
5
1420970
2016-06-21 10:29:11
--------------------------
271395
6707
Cảm nhận của mình về Truyện "Ngày Xửa Ngày Xưa - Cô Bé Lọ Lem" do Alphabook phát hành là khá ổn, sách có giá hơi đắt một chút nhưng câu chữ cũng như nội dung truyện được trau chuốt và xử lý biên dịch khá kỹ lưỡng phù hợp với một câu truyện cổ tích cho các bé, hình ảnh 3d đôi khi trông hơi gượng gạo nhưng cũng khá đẹp điểm trừ là mình thấy bà tiên trong truyện xấu quá trông chả giống bà tiên phúc hậu gì cả trông như một bà nông dân nghèo vậy.
4
153008
2015-08-19 08:53:38
--------------------------
193775
6707
Câu chuyện cổ tích này không biết đã tồn tại bao lâu nhưng nó vẫn có sức hút rất mạnh với những cô bé. Chắc hẳn ai cũng như tôi, rất ấn tượng với cô bé Lọ Lem dễ thương kia. Một cô bé đẹp, hiền lành và rất giỏi giang. Cô phải chịu đựng bà mẹ kế ác độc cùng với hai người con của bà ấy. Lọ Lem như là người hầu trong chính ngôi nhà của mình. Nhưng kết thúc cô đã có hạnh phúc của mình, đó là chàng hoàng tử. Câu chuyện cổ tích là câu chuyện cổ tích đầu tiên mang tôi đến với thế giới thần tiên đầy thơ mộng. Dù đã lớn nhưng mỗi lần được nghe người khác kể câu chuyện này, tôi lại cảm thấy rất thích thú và lại lắng nghe câu chuyện này một lần nữa, như thể đang là một đứa con nít vậy.
4
13723
2015-05-08 18:45:23
--------------------------
