421305
7914
Một câu chuyện cực kì trong lành ấm áp ngọt ngào và cảm động. Những miêu tả cảm xúc tinh tế chuẩn xác và rất rung động. Quá trình đi tìm con gái của một ông bố kì quặc nhưng đẹp vô cùng. Chỉ là mình không hiểu được hành động của người mẹ. Trong những suy nghĩ của người bố thất lạc cô như một thiên thần, nhưng cô đã khiến những đứa con và những người cô yêu rơi vào bế tắc. Những con người riêng với những nỗi đau riêng , những tổn thương và những mất mát nhưng họ vẫn chọn cho mình tâm hồn thật đẹp. 
4
622800
2016-04-24 21:11:31
--------------------------
406821
7914
Cách mở đầu câu chuyện, cũng như cách kể chuyện trong tác phẩm này với tôi là lạ, giọng kể đan xen giữa 2 người, 1 con gái và 1 người cha. Câu chuyện không có cao trào, kịch tính, mà chỉ là những câu chữ nhẹ nhàng, đầy suy ngẫm cho người đọc. Lúc đầu, tôi cứ ngỡ đó là người cha hiện tại của Alice, ông Malcome, nhưng đến hơn nửa cuốn mới hiểu được rằng đó là ông David, và dần dần cảm nhận được tình yêu ông dành cho đứa con gái chưa từng biết mặt của mình, tình yêu đó lớn đến nỗi nó có thể giúp ông vượt qua được những hành hạ của cơn đau tim. Và tôi cũng đau lòng khi Alice và Kal đã không vượt qua được định kiến của cha mẹ mà phải chia rẽ tình yêu sâu đậm đến vậy. Tôi thích cái kết câu chuyện này, một cái kết mở cho mọi người tự cảm nhận. Hoàn thành cuốn sách này, tôi cũng lắng đọng và nghĩ nhiều hơn về tình yêu thương vơí mọi người, hãy trân trọng những gì ra đang còn và đang có...
4
627037
2016-03-28 22:44:27
--------------------------
392593
7914
Thật bất ngờ khi tôi vô tình lại mua được một cuốn sách cảm động như thế này. Nếu so sánh với cuốn Hãy chăm sóc mẹ, thì tôi thích cuốn Mười Điều Tôi Học Được Về Tình Yêu này hơn nhiều. Tình yêu của mỗi người cha có một cách thể hiện khác nhau, nhưng chúng đều xuất phát từ trong tâm của bản thân họ, tình tình cảm thiêng liêng vốn có giữa cha và con. Dù là cha ruột hay cha nuôi, thì những người cha trong cuốn sách này đã giúp tôi nhìn lại mình, nhìn lại gia đình mình để cố gắng không vô tình hay cố ý bỏ lỡ những điều có thể làm mình phải hối hận khi đã quá muộn màn.
4
454536
2016-03-07 11:55:07
--------------------------
296403
7914
Một tác phẩm nhẹ nhàng mà sâu lắng. Trước tiên là tựa sách và bìa sách rất đẹp, hay và cuốn hút. Nội dung phải nói là rất cảm động về tình cảm gia đình. Bản thân câu chuyện không có những cao trào hay kịch tính, chỉ đơn giản là tình yêu của người cha dành cho con mình. Lối viết của tác giả cũng rất ấn tượng, tỉ mỉ nhưng không quá trau chuốt. Đọc xong mới hiểu thêm thế nào mới gọi là "tình yêu đích thực". Mười điều tôi học được về tình yêu thực sự là một quyển sách nên đọc cho tất cả mọi người, để thêm trân trọng những tình cảm thực sự trong cuộc sống này.
5
82495
2015-09-11 06:12:53
--------------------------
277907
7914
Đây là một cuốn sách rất cảm động về tình cảm gia đình-tình cha con. Tác giả có cách xây dựng ngôn ngữ đầy màu sắc và biểu cảm.Những rắc rối của cuộc sống gia đình, nỗi đau do mất mát người thân hay sự tan vỡ của một mối quan hệ được miêu tả thật sống động và chân thực. Ở đâu đó trong cuốn sách chúng ta cũng có thể thấy được niềm hy vọng ẩn giấu. Tình yêu là cốt lõi của cuốn sách này với tất cả những khía cạnh phức tạp của nó.
4
324945
2015-08-25 14:04:50
--------------------------
228905
7914
Mình tình cờ nhìn thấy truyện trong một đợt giảm giá của Tiki, vì giá khá rẻ, tựa truyện lại hay nên mình quyết định mua. Truyện nói về một chủ đề rất đời thường và bình dị nhưng lại vô cùng sâu sắc, đó là tình phụ tử. Hành trình trở về bên cạnh và chăm sóc người cha giúp Alice thấy được rất nhiều thứ và thấu hiểu hơn người cha mà trước nay đối với cô vẫn luôn quá xa cách. Để từ đó, Alice nhận ra rằng dù cho cách thể hiện của ông có như thế nào thì cô vẫn là đứa con mà ông rất mực yêu thương, và ông vẫn là một người cha mà cô luôn kính trọng. Truyện có lối viết khá nhẹ nhàng nhưng sâu lắng và thật sự rất cảm động. Mình chỉ chưa hài lòng lắm ở phần giấy in hơi vàng và cũ. Hi vọng nhà xuất bản sẽ cải thiện chất lượng giấy để tác phẩm trở nên tuyệt vời hơn. 
5
387532
2015-07-16 10:05:42
--------------------------
190697
7914
Câu chuyện nhẹ nhàng mà ấn tượng.

Thoạt đầu mình cứ tưởng nội dung quyển sách sẽ nói về tình yêu lãng mạn giữa hai người nhưng không ngờ đây là tác phẩm nói về tình cha con.

Câu chuyện vô cùng cảm động,tuy người cha không có bất cứ thứ tài sản giá trị nào trên người nhưng ông có được tình thương của một người cha đối với con gái và đó chính là thứ tài sản quý giá nhất ông có thể dành cho cô con gái của minh.

Lối văn viết truyện của tác giả rất ấn tượng,cách dùng từ ngữ đặc sắc và phong phú.Ngoài ra bìa truyện mang màu sắc rực rỡ,rất hợp với nội dung của câu chuyện.

Tác giả làm mình rất hài lòng.
5
412058
2015-04-29 23:51:38
--------------------------
159802
7914
Về cách viết, văn phong của tác giả thì nhìn chung không không có gì quá đặc biệt cuốn hút mình. Tuy nhiên bản thân câu chuyện trong "Mười điều tôi học được về tình yêu" lại thật cảm xúc, giản dị và nhẹ nhàng, lúc thì trong sáng như buổi nắng sớm, lúc lại buồn bã như một bản nhạc không lời giữa đêm khuya. Những nhân vật trong cuốn sách, họ lọt thỏm giữa xã hội xô bồ và chật chội kia, họ có biết bao nỗi buồn và mất mát. Nhưng họ vẫn sống, vẫn yếu, vẫn nuôi một hy vọng cháy bỏng dù nhiều người có thể cho đó là ngốc nghếch. Những người cha, những đứa con thân yêu có thể lạc mất nhau giữa đường đời, nhưng ở một khoảnh khắc nào đó, họ vẫn lưu giữ được hình ảnh của nhau trong trái tim. Đó có lẽ chính là ý nghĩa lớn lao của tình yêu, nó kéo con người lại gần bên nhau theo những cách thật kỳ diệu.
Cuốn sách nhẹ nhàng, sâu lắng, với những đoạn miêu tả tâm lý rất thú vị, nó làm mình liên tưởng rất nhiều đến một bộ phim ngày xưa mình từng xem là "Love Actually" - Yêu thực sự. Đúng, có lẽ chỉ có tình yêu thực sự mới là điều quý giá nhất trong cuộc sống này.
3
109067
2015-02-19 12:35:26
--------------------------
143402
7914
Mười điều tôi học được về tình yêu mang lại cho người đọc những phút giây cảm động về cuộc sống gia đình cũng như tình cảm cha con vượt qua bao năm tháng. Cuốn sách là thông điệp đầy ý nghĩa mà mỗi người trong cuộc sống cần hiểu. Truyện là sự kết hợp của miêu tả nội tâm, các diễn biến tâm lý sâu sắc của nhân vật và lối văn tự sự đã góp phần đem lại cho chúng ta một cái nhìn mới hơn và khác hơn đối với tình yêu thiêng liêng của các bậc sinh thành.
Bìa sách đẹp, giá cả cũng rất phải chăng thích hợp với điều kiện của nhiều người mua hàng.
4
500425
2014-12-22 23:13:42
--------------------------
101172
7914
Phải nói là đã lâu lắm rồi tôi mới được đọc một quyển sách mang lại cho tôi nhiều cảm xúc đến thế. Ban đầu đọc có hơi khó hiểu vì lời giới thiệu truyện làm tôi hiểu lệch đi nội dung tác phẩm bên cạnh đó truyện còn được sắp xếp xen kẽ giữa những dòng tự truyện của người cha và người con gái. Tuy nhiên sau khi đọc xong cuốn sách tôi mới thấy được tình yêu của cha mẹ lớn lao như thế nào đặc biệt là với người cha trong truyện. Tình yêu là thứ bao trùm lên cả tác phẩm, suốt mạch truyện dài tôi cảm nhận được tình yêu thương đặc biệt của 2 người cha, tuy mỗi người đều có cách biểu lộ khác nhau nhưng tình cảm mà họ dành cho con mình là thứ không gì thay thế được, Một người cha đã hy sinh cả đời cùng với sự cao thượng âm thầm để nuôi dưỡng đứa con không phải của mình khôn lớn và sự khắc khoải của ông khi không thể hòa hợp được với con cho đến chết ông vẫn nhắc đi nhắc lại "bố thương các con đều như nhau", tôi đã rất xúc động khi đọc đến đây, và một người cha vô gia cư với lòng yêu thương đứa con ruột nhiều năm không gặp đã tìm kiếm con suốt một thời gian dài với nỗi nhớ nhung khôn xiết để rồi khi tìm được con, ông đã thể hiện tình cảm của mình một cách vô cùng đơn sơ, giản dị, sự lo lắng, lóng ngóng về bản thân mình khi đứng trước mặt con gái làm tôi thấy nghẹn ngào quá đỗi.

Ngoài tình cha con được nhấn mạnh trong truyện, tác phẩm còn đề cập đến tình yêu, thứ tình yêu mà để lại trong lòng người đọc cảm xúc buồn man mác khi 2 con người không thể đến được với nhau vì khoảng cách, vì những nhận định sai lầm trong suy nghĩ. Trải dài cả mạch truyện là một cảm xúc nhẹ nhàng, đầy lắng đọng, tôi đã đọc và suy ngẫm rất lâu về tác phẩm này, một điều khá hay, rất chính xác đối với nội dung truyện mà tôi vô cùng tâm đắc được trích ra từ tác phẩm đó là: "Yêu không cần có lý do, nhưng chắc hẳn cần rất nhiều lòng can đảm". 

"Mười Điều Tôi Học Được Về Tình Yêu" là bài học quá đủ để ta hiểu thêm về các đấng sinh thành của mình. Một cuốn sách rất đáng đọc, tôi hy vọng rằng sẽ có nhiều người cùng đọc tác phẩm này và cảm nhận được thông điệp đầy ý nghĩa mà tình yêu mang lại.
5
41370
2013-12-02 17:06:26
--------------------------
