300788
2701
Đã đặt mua cùng với sách dạy xếp hình cho trẻ em. Sách có bìa mềm, màu sắc rực rỡ, giấy bên trong khá là đẹp, hình ảnh rõ ràng bắt mắt, nội dung các truyện không quá dài phù hợp với khả năng tập trung lắng nghe của các bé. Đa số các truyện kể là của nước ngoài nên đôi lúc giọng văn có thể là khó hiểu hơn so với sự hiểu biết của các bé (bố mẹ phải giải thích thêm - bé nhà mình đặt câu hỏi rất là nhiều khi nghe kể chuyện). Nói chung, là nhờ có sách này mà mẹ con mình có nhiều thời gian đối thoại hơn, sẽ đầu tư khoản đọc sách cho con nhiều hơn nữa. Cám ơn dịch vụ và sản phẩm của tiki, rất hi vọng sẽ nhận được nhiều sản phẩm hữu ích và giá cả hợp lý hơn nữa của tiki.
4
408828
2015-09-14 10:49:33
--------------------------
