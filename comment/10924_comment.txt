400726
10924
Thật đúng với danh mục Nghệ thuật sống đẹp. Cuốn sách đã mang đến cho người đọc một cái nhìn toàn diện và đầy đủ hơn với nhiều triết lí nhân sinh. Chúng ta vẫn thường hay nghĩ thành công là đạt được đỉnh cao của tham vọng thỏa mãn ước nguyện mà đó phải là một ước nguyện to lớn. Nhưng sau khi đọc cuốn sách này ta thấu hiểu rõ hơn về hai chữ Thành Công. Sẽ không có khái niệm nào chung nhất. Thành công đôi khi là kiếm đủ tiền nghỉ hưu ở tuổi 50 , hay giành giải nhất cuộc thi triển lãm nghệ thuật, thành công trong việc nuôi dạy con cái... thành công đôi khi là thứ giản dị nhất...
5
872351
2016-03-19 20:10:23
--------------------------
398677
10924
Đây là một sách hay với nội dung đơn giản giúp chúng ta có thể nắm bắt được những kiến thức và góc nhìn của tác giả muốn gửi gắm đến đọc giả. Tuy nhiên, đa phần tác giả gửi gắm thông điệp  thông qua những mẫu chuyện nhỏ kể về nhiều nhân vật nên đôi lúc làm người đọc chưa cảm thấy hấp dẫn. Cần đưa ra nhiều lời bình, nhận xét của tác giả  nhiều hơn nữa.  Qua góc nhìn của mình về thành công tác giả cung cấp nhiều lời khuyên rất thiêt thực và hiệu quả cho đọc giả, giúp khơi dậy khát vọng thành công trong mỗi người.
3
62072
2016-03-16 16:34:02
--------------------------
375549
10924
Tác giả đã đơn giản hóa vấn đề khi chỉ xem thành công chỉ là cuộc chơi mà thôi . Bởi nếu thắng là ta đã chinh phục nó, vượt qua được thử thách của bản thân và đã thành công . Còn nếu thua thì không sao cả đó chỉ là bắt đầu thôi, và bạn đang có một bài học , một kinh nghiệm cần được thực hiện và tránh những lỗi lầm cần thiết. Xuyên suốt quyển sách là những bài học bổ ích và nó sẽ mang bạn đến gần với thành công hơn nếu bỏ ra thời gian để ngẫm nghĩ và thực hiện nó.
4
651562
2016-01-28 17:03:51
--------------------------
233167
10924
Thật không ngờ, cuốn sách này lại bổ ích đến vậy. Khi mua tôi đã từng nghĩ nó cũng như bao cuốn sách dạy kĩ năng sống khác, và mấy ngày đầu tôi đã không vội đọc nó, nhưng kể từ khi mở sách ra, và mỗi ngày đọc một chương, tôi đã có một suy nghĩ hoàn toàn khác. Cuốn sách cho tôi kiến thức và kĩ năng để phân bố thời gian học tập cụ thể , thời gian biểu hợp lí trong năm cuối học lớp 12 này và thời gian nghỉ ngơi, và cả cách học hiệu quả, những kĩ năng để làm cho thành công của mình tiến tới nhanh hơn nữa. Đằng sau mỗi kĩ năng là những ví dụ điển hình của những người đã từng trải qua và thành công...Rất đáng để đọc
4
205891
2015-07-19 15:07:27
--------------------------
166122
10924
Cuốn sách này mình đọc bắt đầu từ chương cuối và có vẻ hợp với những điều mình đang cần.

Những cuốn sách nói về đề tài này không ít và cũng nhờ vậy mà đáp ứng nhu cầu của độc giả đa dạng hơn. 

Mình tìm thấy trong cuốn sách này một số điều mà trước đây mình chưa từng biết qua: 
- "thừa nhận thành tựu cá nhân", 
- đặt năng lượng của mình vào những suy nghĩ, việc làm hay con người nào cũng cần cân nhắc như đặt việc gì vào khoảng thời gian nào của bản thân, 
- gợi ý về quản lý thời gian của tác giả đối với mình khá hay, 
- chương "Cơ hội sẽ đến" và đoạn "Tìm con đường riêng" là chương và đoạn mình thích nhất. Mình thấy được cảm hứng của tác giả trên từng câu chữ.

Và mình cũng nhận thấy để nhận ra trong một cuốn sách đâu là đề tài tác giả quan tâm nhất, đâu thật sự là những trải nghiệm của tác giả không phải dễ dàng.
4
32038
2015-03-11 21:54:15
--------------------------
