493240
8312
Truyện này dùng cho trẻ em nhưng cách dán gáy của truyện này không chắc chắn, dễ bị đứt rời từng tờ khi đọc. NXB này cần phải cải tiến lại cách đóng gáy của truyện này. Mới mua được 1 ngày mà đã bị đứt rời hết 1/3 cuốn truyện.
2
1543065
2016-11-29 08:22:39
--------------------------
196363
8312
Hồi nhỏ cũng đọc nhiều chuyện cổ tích lắm nhưng mà không nghĩ lại có nhiều nàng công chúa đến vậy. 101 nàng công chúa được phân loại dựa những đặc điểm và tích cách nổi bật của các nàng. Dưới mỗi câu chuyện là một vài dòng cảm nhận nhỏ như là một thông điệp ẩn chứa của câu chuyện. Rất phù hợp để dạy trẻ con với cuốn sách này. Mà người lớn đọc cũng hợp luôn .^^ Thực ra mình ban đầu mua cuốn sách này với ý định sau này có con sẽ mỗi tối đọc ru con một chuyện mà giờ tối nào không ngủ được cũng lôi ra đọc rồi mơ mộng . Chắc sau này khỏi cầm sách cũng kể được cho con nghe luôn^^
4
75031
2015-05-15 19:15:09
--------------------------
140672
8312
Muốn tuổi thơ của con gái ngập tràn hình ảnh xinh đẹp của các nàng công chúa trong truyện cổ tích nên tôi đã quyết định mua quyển sách này. Ngay lần đầu tiên mang quyển sách này về con gái tôi đã rất thích vì sách được vẽ rất đẹp, lung linh nhiều màu sắc. Mặc dù mới hơn 1 tuổi, chưa hiểu được nội dung câu chuyện nhưng tối nào bé cũng tự đi lấy sách hoặc nhắc mẹ “chúa..” . Khi nghe mẹ đọc đến từ “công chúa” hoặc “ nhà vua” thì bé chỉ tay vào hình “vua, vua” “chúa, chúa”.. Đồng nghiệp của tôi thấy thế nên cũng gởi tôi mua giúp vài cuốn về tặng các cháu. Nhưng có một điểm chưa hài lòng về sách là như hình tượng nàng Bạch Tuyết vốn tóc ngắn đen, mặc áo choàng cổ rộng, chỉ cần nhìn vào hình là biết ngay nàng Bạch Tuyết thì đây lại vẽ rất khác, sáng tạo ra hình ảnh không như tuổi thơ bao thế hệ trước đã ghi sâu vào tâm trí. Và cách hành văn thì có hơi bị trùng lặp..
4
433488
2014-12-12 09:43:57
--------------------------
