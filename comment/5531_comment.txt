460729
5531
Mình thích cái bìa, nó tạo được ấn tượng đầu tiên khá tốt đẹp. Sau khi đọc thử nội dung giới thiệu thì mình quyết định mua. Mình thích những cuốn sách như vậy, viết về những người con gái mạnh mẽ, sống thật với bản thân, không e ngại sự khác biệt, yêu thích sự cá tính mỗi người. Cuốn truyện có những tình tiết khá hài hước, đấu võ mồm cũng rất gai góc. Sự hài hước pha lẫn vào từng tình huống làm mình đọc vừa cười nhưng cũng phải suy ngẫm kha khá. Đôi khi chuyện hài chưa hẳn chỉ để cười.
5
890619
2016-06-27 11:29:26
--------------------------
312543
5531
Tôi đã đọc nhiều tác phẩm thể loại này nhưng bị ấn tượng bởi thiết kế bìa sách, có gì đó vừa hài hước và châm biếm của cuốn sách Tôi Không Phải Là Phan Kim Liên của Tác giả Lưu Chấn Vân . Và đúng như bìa sách đã minh họa, phần nội dung kể về hành trình chứng minh bản thân giữa biết bao cái giả giả thật thật trong cuộc đời này của một người phụ nữ. Giọng văn của tác giả tưng tửng pha lẫn sâu cay làm người đọc vừa cảm thấy sự sắc sảo vừa đôi lúc thấy buồn cười với các tình tiết trong câu chuyện. 




5
144957
2015-09-21 15:57:45
--------------------------
