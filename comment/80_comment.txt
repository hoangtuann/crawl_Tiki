462163
80
Thực sự thì cuốn Oxford Essential Dictionary này có số từ quá khiêm tốn nếu bạn đang học cấp 2, cấp 3 hay đại học sử dụng. Về hình thức nó như một cuốn sách giáo khoa, có hình minh họa và trình bày rất khoa học dễ dùng. Tuy nhiên vấn đề là học sinh tiểu học ít khi dùng từ điển Anh - Anh và cũng vì giá hơi cao. Nếu mình chọn mua một cuốn từ điển Anh Anh thì mình sẽ mua một cuốn có đầy đủ mọi thứ để học lâu dài như cuốn Oxford Advanced Learner's Dictionary vậy!
2
88741
2016-06-28 12:23:57
--------------------------
