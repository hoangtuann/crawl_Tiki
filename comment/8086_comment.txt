255686
8086
Con mình rất thích thể loại dán hình như của cuốn sách này, sách vừa có nhiều màu, hình ảnh cũng rất sinh động bắt mắt, cách trình bày nội dung của từng trang đều theo chủ đề nên mình  có thể hướng dẫn con quan sát, tập trung và sự khéo léo dán từng hình theo yêu cầu của chủ đề được mẹ đọc và hướng dẫn. Tuy nhiên em bé còn khá vụng về nên có những hình dán bé quá nên bé dán chưa được đẹp nhưng nhìn chung thì mình khá hài lòng với bộ sách này. 
4
575234
2015-08-06 11:45:31
--------------------------
194594
8086
Nhìn chung sách được thiết kế khá sinh động. Con mình rất thích, mua về là đòi dán ngay. Nhưng khi dán được một vài trang thì mới phát hiện ra hình dán của 2 trang bị cắt mất chi tiết.
Nguyên nhân là sách có bấm lỗ tròn tất cả trang sách ngay chiếc bánh donut đen nhưng một cặp hình dán lại được đặt ngược so với trang sách nên khi cắt lỗ tròn bị khuyết mất. Bỏ qua phần đó thì khá hài lòng về nội dung và hình ảnh của sách. Sách đẹp với các nhân vật Disney mà bé hay thấy trên kênh Disney nên rất thích.
3
138619
2015-05-11 10:02:40
--------------------------
