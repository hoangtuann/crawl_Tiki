132682
6997
Đây thực sự là cuốn sách hay khi nói về toàn diện văn hóa, xã hội, tôn giáo của 3 đất nước Đông Nam Á: Thái Lan, Java và Philippines.
Cuốn sách mang đến cho người đọc, và nhất là những ai học về chuyên ngành Đông Nam Á nhiều kiến thức mới mẻ, thú vị để hiểu hơn về tôn giáo cũng như con người và nguồn gốc tại sao " lại như vậy" ở các nước Đông Nam Á này. 
Tuy nhiên cuốn sách cung cấp quá nhiều kiến thức, lại có đôi chỗ đan xen tùm lum gần như không rõ ràng là phần nào, nước nào nên nếu muốn đọc lướt qua tìm thông tin chính thì chắc hẳn các bạn sẽ thấy rối rắm và nhầm lẫn. Lời dịch cũng có vẻ không được mượt lắm khiến cho tác phẩm này mất đi cái hồn mà tác giả đã dày công nghiên cứu. Nhưng vì những cuốn sách về thể loại này không nhiều ở Việt Nam, cộng với 2015 sẽ là năm hợp tác qua lại giữa các nước ASEAN nên việc tìm hiểu về các nước trong ASEAN là vô cùng cần thiết, chính vì vậy cuốn sách này vẫn là cuốn sách cần thiết cho những ai muốn tìm hiểu thông tin cũng như những ai muốn nghiên cứu về các nước Đông Nam Á.
3
34983
2014-11-01 22:55:37
--------------------------
132681
6997
Đây thực sự là cuốn sách hay khi nói về toàn diện văn hóa, xã hội, tôn giáo của 3 đất nước Đông Nam Á: Thái Lan, Java và Philippines.
Cuốn sách mang đến cho người đọc, và nhất là những ai học về chuyên ngành Đông Nam Á nhiều kiến thức mới mẻ, thú vị để hiểu hơn về tôn giáo cũng như con người và nguồn gốc tại sao " lại như vậy" ở các nước Đông Nam Á này. 
Tuy nhiên cuốn sách cung cấp quá nhiều kiến thức, lại có đôi chỗ đan xen tùm lum gần như không rõ ràng là phần nào, nước nào nên nếu muốn đọc lướt qua tìm thông tin chính thì chắc hẳn các bạn sẽ thấy rối rắm và nhầm lẫn. Lời dịch cũng có vẻ không được mượt lắm khiến cho tác phẩm này mất đi cái hồn mà tác giả đã dày công nghiên cứu. Nhưng vì những cuốn sách về thể loại này không nhiều ở Việt Nam, cộng với 2015 sẽ là năm hợp tác qua lại giữa các nước ASEAN nên việc tìm hiểu về các nước trong ASEAN là vô cùng cần thiết, chính vì vậy cuốn sách này vẫn là cuốn sách cần thiết cho những ai muốn tìm hiểu thông tin cũng như những ai muốn nghiên cứu về các nước Đông Nam Á.
3
34983
2014-11-01 22:55:35
--------------------------
