488430
5421
Mình thích quyển sách này phù hợp với các bạn đang học Ielts trình độ 5.0 như mình. Từ mới lặp lại nhiều lần trong bài nên rất dễ nhớ. Sp được free ship Tiki giao hàng rất nhanh. Ủng hộ
5
723013
2016-11-02 18:30:34
--------------------------
476222
5421
Mình vừa mới nhận được sách, siêu thích luôn, quyết tâm luyện với em nó. Giao hàng được free ship nữa ạ, sẽ ủng hộ tiki lâu dài, hy vọng sẽ có nhiều sách hay hơn nữa, đặc biệt là sách học anh văn. Bây giờ bắt đầu nghiên cứu sách đây. Các bạn muốn nâng cao từ vựng có thể tham khảo nhé mọi người. À  còn có cả CD nữa, cảm thấy hài lòng với số tiền bỏ ra so với chất lượng sách nhận được. Mình mua cả bộ 3 quyển luôn ạ, nghe nói là độ khó tăng dần.
5
1482956
2016-07-19 12:12:54
--------------------------
353386
5421
Sách này dân Ngoại Ngữ mua nhiều lắm, từ thời các thầy cô dạy mình đã dùng rồi. Điều đó để chứng minh là cuốn sách rất đáng để những ai muốn tăng cường khả năng Ngoại ngữ của mình thì nên dùng. Tuy nhiên, quan trọng nhất vẫn phải là tự bản thân phải luyện tập chuyên cần. Và mình nghĩ nên dùng sách nhiều lần, đọc lại để hiểu rõ hơn cấu trúc thông tin cần thiết! Tuy nhiên sách in chữ hơi khó nhìn và chất lượng bìa sách  hơi mỏng. Dĩ nhiên, quan trọng nhất vẫn là nôi dung bên  tring!
4
480400
2015-12-16 00:15:39
--------------------------
327359
5421
Cuốn sách này rất thích hợp để tăng vốn từ vựng và kỹ năng đọc hiểu, mình thấy nó thực sự hữu ích. Nó không chỉ có các bài tập thực hành mà còn có CD giúp mình luyện nghe hiệu quả. Sau khi học xong một vài chủ điểm, từ vựng và kỹ năng nghe của mình lên nhiều. Một vài chủ đề rất thú vị giúp mình dễ dàng tiếp thu vốn từ mới hơn. Sách có giấy in tốt, chữ rõ ràng, những từ mới được in đậm rất khoa học. Mình hoàn toàn hài lòng với giá thành và chất lượng của sản phẩm này.
5
381981
2015-10-27 18:07:39
--------------------------
238381
5421
Đây là cuốn sách tiếng anh bổ ích. Quyển sách này mang lại cho tôi vốn từ vựng phong phú và hiểu biết về xã hội bổ ích.Để học từ vựng tiếng anh thì quan trọng là bạn phải gặp nó thường xuyên. Các từ mới trong quyển sách này xuất hiện không chỉ tại chủ điểm liên quan mà còn được nhắc lại nhiều lần qua các bài tập. Tôi đã học được nhiều từ mới qua cuốn sách này.
Ngoài ra các dạng bài đọc hiểu như điền từ, đúng -sai- không có thông tin được luyện rất nhiều. Khả năng làm bài đọc hiểu sẽ được cải thiện nhanh.
5
276790
2015-07-23 10:03:03
--------------------------
