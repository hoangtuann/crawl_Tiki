394499
2906
Sách này mình được hướng dẫn mua để học tại trung tâm cho trình độ sơ cấp. cuốn của mình thì được bán kèm với 2 đĩa CD chứ không phải 3 DVD như ghi trên sách. Sách được in rõ ràng, giấy trắng dày dặn nên ấn tượng đầu tiên là khá bắt mắt. Sách có chú thích bằng tiếng Việt đầy đủ nên rất phù hợp cho những bạn mới tập làm quen với hán tự như mình. Phần bài tập trong sách đều co đáp án phía sau nên nếu thích thì các bạn có thể tự mày mò học cũng được.
Điểm trừ duy nhất thì có lẽ là sách nên có nhiều phần bài đọc hơn để mình có thể nhớ mặt chữ và cách sử dụng từ trong câu.
4
770743
2016-03-10 14:56:01
--------------------------
335705
2906
Mình đi dạy thêm tiếng Hoa, mình kiếm khá nhiều giáo trình cho các bạn mới bắt đầu học ngoại ngữ này. Cuốn sách khá chi tiết và theo mình nó khá dễ cho các bạn nắm một cách tổng quát nhất về phương pháp học. Nó bắt đầu với việc học và phát âm bảng chữ cái, mà việc học bảng chữ cái và phát âm chuẩn là điều rất quan trọng. Vì nó là nền móng cho các bạn học sâu hơn sau này, nếu móng ban đầu không vững thì sau này cái nhà sẽ lung lay. Nên cuốn sách bố trí cho việc phát âm trước để các bạn có thể đọc từ vựng bằng phiên âm sau khi vô bài đọc, thuận tiện hơn cho việc đọc của các bạn. Các bài trong đó đều có đoạn văn, đoạn đối thoại và giải thích từ vựng. Cuốn sách kèm theo DVD nên các bạn học nghe và luyện khá dễ. Mình khuyên bạn nào thích học tiếng Trung thì bắt đầu với cuốn này, vì mình dạy thì thấy cuốn này hiệu quả, người học cũng dễ đi vào bài một cách logic. 
4
416043
2015-11-11 13:35:07
--------------------------
330529
2906
Giáo trình luyện nói tiếng trung cấp tốc cho người bắt đầu (tập 1) gồm các tình huống giao tiếp cơ bản cho người muốn học nói cấp tốc. Tổng cộng có 15 bài mỗi bài một chủ đề, có hình vẽ minh hoạ trong từng bài. Tập 1 và tập 2 có phiên âm Pinjin rõ ràng.  Học kèm đĩa CD thì sẽ rất nhanh biết nói, nhưng phần luyện tập ngữ pháp hơi ít. Sách được in đẹp và rõ nét, nhưng giấy trắng nên dễ bị mỏi mắt khi ngồi học lâu. Giá tiền sách hơi mắc.
4
128735
2015-11-02 20:15:10
--------------------------
