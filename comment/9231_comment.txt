329981
9231
Đây là một cuốn sách cần có cho bất kì ai muốn nghiêm túc làm quen với việc viết văn để có thể tránh khỏi những lỗi ngữ pháp và chính tả không cần thiết. Sách được chia làm nhiều mục với các ví dụ minh họa sống động và dễ hiểu, khiến người đọc bị thu hút không sợ nhàm chán. Hơn nữa, cuốn sách cho chúng ta thấu hiểu và yêu quý hơn thứ ngôn ngữ dân tộc vốn dĩ giàu đẹp và phong phú là tiếng Việt của chúng ta.
Mong được đọc nhiều sách hay hơn nữa từ tác giả. 
4
76179
2015-11-01 20:14:26
--------------------------
229616
9231
Cuốn sách với bố cục rõ ràng, cụ thể. Cái hay của quyến sách là tác giả đã đan xen vào đó là những câu chuyện ngôn ngữ khá thú vị và bổ ích.Điều đó làm cho nội dung viết về ngôn ngữ không còn khô khan mà lôi cuốn, hài hước và hấp dẫn. Tôi nghĩ nó cần thiết cho tất cả người Việt, cần thiết cả trong cuộc sống hàng ngày, trong giao tiếp ở nhiều môi trường chứ không chỉ dành riêng cho những ai nghiên cứu về ngôn ngữ và văn học. Ngôn ngữ là vốn quý của dân tộc, Vì thế, ai cũng cần biết sử dụng một cách tốt nhất với thái độ trân trọng và hiểu biết.
4
352816
2015-07-16 22:09:15
--------------------------
179480
9231
Mình thích cho cuốn này 4,5 sao nhưng không có.
Đọc cuốn này mới thấy Tiếng Việt quả là khá phức tạp nhưng hay, đặc biệt với người Việt có thể đã đang tự tin với khả năng tiếng mẹ đẻ của mình rồi nhưng sau khi đọc có lẽ sẽ cảm thấy niềm tin của mình đôi khi là do quá tự tin.

Mình cũng thích cách viết của tác giả, có gì đó mạch lạc, khúc chiết và đôi khi làm mình ngạc nhiên rồi bật cười, nói chung là chất. Có cơ hội sẽ đọc cuốn khác của tác giả.
4
457880
2015-04-07 12:31:17
--------------------------
147447
9231
Trong cuộc sống chúng ta dễ dàng bắt gặp những lỗi viết câu rất cơ bản ở nhiều nơi, từ một mẩu thông báo nhỏ, một bài báo hoặc cả những buổi hội họp lớn. Có những lỗi nhiều người mắc đến nỗi đó đã trở thành một điều hiển nhiên, và từ sai mặc nhiên thành đúng. Những điều như thế tuy nhỏ nhặt như rất nguy hiểm, theo thời gian nó sẽ làm mất đi sự trong sáng của Tiếng Việt. Cuốn sách này là một cơ hội để tự nhìn lại những gì chúng ta đã "đối xử" với tiếng mẹ đẻ của chúng ta, và chính tôi khi đọc đã cảm thấy giật mình vì bấy lâu nay đã mắc phải những lỗi sai mà mình không hề hay biết! Thiết nghĩ trước khi nói hay viết ra những lời hay ý đẹp, cần phải nói (viết) đúng!
4
368015
2015-01-07 21:44:23
--------------------------
62535
9231
Tiếng Việt đang cần lắm sự phục hổi.
Dù có xem ti vi, xem báo, những cuộc đàm thoại, những kênh thông tin luôn phải sử dụng ngôn ngữ chính xác và chuẩn đến từng câu chữ thì ta lại dễ dàng bắt gặp lỗi câu, cả lớn lẫn nhỏ. Tuy rằng không mấy ai để ý và cũng chẳng tiện bắt lỗi, nhưng lâu dần chính chúng ta lại làm mất đi cái hay của Tiếng Việt, nhường chỗ cho cái thuận tiện vội vã, đúng như hiện tượng "để lâu câu sai hoá đúng"! Nhưng giờ phải sửa thế nào, phải sửa làm sao để Câu sai thành đúng, đúng lại thành hay, hay sử dụng làm sao cho đúng lúc, đúng chỗ thì thật là khó khăn. 
Những kiến thức về luyện từ và câu dạy trong chương trình học có vẻ như vẫn chưa đủ cho mình, nên mình nghĩ trong số những câu mình vừa viết đây chắc hẳn sẽ có một số lỗi sai nhất định mà chính mình cũng không nhận ra sai ở đâu nữa, có lẽ cuốn sách này sẽ giúp mình hiểu thêm phần nào!
3
22852
2013-03-10 00:09:58
--------------------------
