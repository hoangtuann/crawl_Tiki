572533
4462
sách khá ok. phù hợp cho hs luyện thi khi phân từng chuyên đề. tiki giao hàng nhanh và đảm bảo. thanks!
4
5180499
2017-04-13 21:18:30
--------------------------
206940
4462
Đây là cuốn sách rất hay và phù hợp với học sinh
Ưu điểm: 
- Về hình thức: bìa đẹp, mát tay, chất lượng giấy khá tốt
- Về nội dung:
. Bám sát kiến thức sách giáo khoa cũng như chương trình thi của học sinh
. Cách viết chính xác dễ hiểu, vừa giúp cho những ai học kiến thức mới cũng như ôn lại kiến thức cũ
. Các phần được phân chia rất hợp lí, giúp học sinh nắm vững được cách làm và hiểu được cách áp dụng
. Bài tập phong phú, đa dạng, phần đáp án hướng dẫn giải khá chi tiết đầy đủ 
Khuyết điểm: Bìa mỏng và không có phần gấp đánh dấu trang nên dễ bị cong khi sử dụng được một thời gian
Nói chung theo đánh giá của mình thì đây vẫn là một cuốn sách rất hay và bổ ích.
4
466113
2015-06-11 01:01:20
--------------------------
