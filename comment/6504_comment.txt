478925
6504
Không diễn ta được cái hay của 1 tác phẩm mang hơi hướng của lịch sử, cái hấp dẫn của tiểu thuyết... có thể là do người dịch. Đoạn đầu còn có sự nhầm lẫn đáng chê trách về lịch sử. Ngoài ra ở những trang 150 chữ mặt trước và sau hằn lên nhau rất khó đọc ( không hiểu nxb Văn Học đã làm chất lượng kiểu gì ) cách diễn đạt của người dịch như viết cho trẻ con xem, thiếu sự hùng hồn, mạnh mẽ của truyện viết về Tần Thủy Hoàng !
Đó là nhận xét về tác phầm, còn với Tiki rất thích cách vận chuyển và bán hàng của bạn.
1
648970
2016-08-05 22:40:13
--------------------------
358096
6504
Như chúng ta đã biết, Tần Thủy Hoàng là một Hoàng vị hoàng đế hung bạo nhất trong lịch sử Trung Quốc. Ông điên rồ đến mức khi mới được đăng quang khi 13 tuổi đã cho xây dựng lăng mộ cho chính mình và có nhiều đóng góp cho Trung Quốc như xây dựng Vạn Lý Trường Thành, tạo ra La Bàn,... nhưng đặc biệt nhất ông để lại dương thế chính là lăng mộ của ông. Và cuốn sách này nói về những mảnh ghép lịch sử trị vì của ông mặc dù đây là tiểu thuyết không phải lịch sử.
5
290697
2015-12-24 19:41:00
--------------------------
204108
6504
Sách có bìa cứng, trình bày đẹp, kích thước lớn (16x24 cm), chất liệu tốt, chữ in rõ ràng và trọng lượng nhẹ thích hợp cả cho việc cầm trên tay đọc và trưng bày trong tủ sách sau khi đọc xong cũng rất đẹp.
Mặc dù chỉ là tiểu thuyết chứ không phải là sách lịch sử nhưng nội dung của sách bám sát với lịch sứ chứ không hư cấu nhiều. Với cách viết này người đọc sẽ hiểu biết nhiều hơn về lịch sử, về triều đại đầu tiên thống nhất được Trung Quốc. Có lẽ do bám sát lịch sử và ít hư cấu cũng như chỉ tập trung vào một nhân vật là Tần Thủy Hoàng nên đọc không được hay như những bộ tiểu thuyết khác của Trung Quốc như Thủy hử, Tam quốc diễn nghĩa, Đôngchu liệt quốc
4
504882
2015-06-03 09:09:37
--------------------------
191137
6504
Tần vương Doanh Chính - Tần Thủy Hoàng là một vị vua nổi tiếng trong lịch sử Trung Hoa. Với thành tích thống nhất đất nước, xây dựng Vạn Lí Trường Thành và cả những chính sách ngu dân như đốt sách, chôn học trò,... Tần vương đã trở thành một biểu tượng của sự chinh phạt và tàn bạo trong mắt mọi người. Thậm chí đã có một bộ phim nói về ông và thần thánh hóa Tần vương cùng lăng mộ của mình. Nhưng với Tần Thủy Hoàng diễn nghĩa, chúng ta sẽ thấy một Tần Vương có môt thân thể bình thường, một tính cách khác lạ và một tham vọng to lớn trong việc thống nhất đất nước. Ông có những điểm rất đáng để chúng ta học hỏi và áp dụng trong cuộc sống ngày nay. Sách được làm giấy tốt, đặc biệt là bìa cứng của nó giúp chúng ta dễ dàng hơn trong việc bảo quản.
5
387632
2015-05-01 08:25:58
--------------------------
