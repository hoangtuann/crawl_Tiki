234803
4977
Tôi đã mua cuốn sách 150 món ăn ngon này và đã thử nấu những món ăn trong đó cho mình và gia đình. Điều tôi tâm đắc là: tác giả đã lựa chọn được những món ăn ngon trên khắp đất nước đển đưa vào cuốn sách nhỏ gọn tiện lợi này. Nhưng cuốn sách có quá nhiều món, mà sách lại nhỏ gọn nên cũng vì thế mà các bước và cách thức thực hiện từng món vẫn còn những thiếu sót. Ví dụ như: bước này nên làm trong bao lâu, hay nên để lửa lớn hay nhỏ hay vừa... Sách chỉ dành cho những ai đã có tay nghề nấu ăn từ trước, còn những người mới học nấu ăn thì ko nên mua cuốn sách này.
3
633374
2015-07-20 16:15:24
--------------------------
199071
4977
Mình đã đọc cuốn sách 150 món ăn ngon hằng ngày và có những lời nhận xét như sau:
Thứ nhất về hình thức cuốn sách được thiết kế cũng rất bắt mắt, chất liệu giấy in trắng và độ bền khá tốt. 
Thứ 2 về nội dung cuốn sách khá hay, Nội dung chất lượng của mỗi món ăn có phần công phu và tâm huyết của tác giả đã bò ra. Nhưng có một số nội dung các món ăn chưa được đầu tư kỹ lưỡng nội dung còn rất sơ sài. 
Thứ 3 Cuốn sách này nói chung cơ bản đã đáp ứng được yêu cầu mong mỏi của quý độc giả. Nhưng với một số độc giả khó tính thì vẫn còn rất nhiều vấn đề cần phải hoàn thiện để cuốn sách được tốt hơn.
Một lần nữa cảm ơn tác giả đã có một tác phẩm tốt.
4
484466
2015-05-21 14:12:08
--------------------------
