42408
10061
Thần đồng đất Việt là bộ truyện tranh nói về tài và đứa của Trạng Tí. Trạng Tí là một nhân vật được hình tượng hóa từ rất nhiều ông trạng từ thời xa xưa của nước ta. 
Ở trạng Tí ta sẽ thấy được Lương Thế Vinh cân voi, Mạc Đĩnh Chi lấy đom đóm bỏ vào trứng để học, thấy được Phùng Khắc Khoan và nắm hạt giống mang về nguồn lương thực mới cho nhân dân..v.v. 
Qua bộ truyện tranh này, chúng ta sẽ thấy được rất rõ chân dung các ông trạng, những người hiền tài, quan thanh liêm của nước Việt từ thời dựng nước giữ nước cho đến tận bây giờ. Đó là những con người tài trí, tài đức, yêu thương nhân dân hết lòng phục vụ tổ quốc. Không chỉ thế, ta còn thấy được bộ mặt của những kẻ xâm lược, muốn thôn tính nước ta nhưng không thành. 
Bên cạnh đồng hành với trạng Tí là những người bạn ấu thơ, là người dân, điều ấy phản ánh rằng, người tài giỏi đến mấy cũng cần có đứa để lấy lòng tin của nhân dân bá tánh.
4
54418
2012-10-09 09:33:41
--------------------------
