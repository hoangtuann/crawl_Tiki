412950
5344
Đọc truyện này tôi thấy hơi sợ
Không phải chỉ sợ vì truyện cô bé 11 tuổi bị bắt cóc trở thành một nô lệ tình dục đúng nghĩa mà đáng sợ hơn cả là cái quá trình khi cô sống cùng tên bắt cóc và những thay đổi trong tâm lí của nữ chính.
 Đây là câu chuyện câu chuyện có thật về cuộc đời của  Jaycee, nó làm tôi thấy khâm phục tác giả vô cùng khi có thể sống qua những tháng ngày như thế, thậm chí cô ấy dường như còn quên mất mình là ai, sống biệt lập với thế giới bên ngoài.
Đáng sợ hơn là một cô bé 11 tuổi, vốn lẽ ra nên căm ghét tên bắt cóc mình, nên tìm cách chạy trốn, nhưng dường như cô bé đã bị tê liệt, bị hắn làm cho trở nên phụ thuộc và thụ động, đến nỗi mà cô dễ dàng thỏa hiệp với cả những chu cấp nhỏ nhặt nhất của tên bắt cóc, coi đó là lòng tốt của hắn
Thật sự buồn vô cùng cho một số phận bị đánh cắp như vậy
4
538945
2016-04-08 18:18:59
--------------------------
169868
5344
Khi đọc quyển sách này mình có rất nhiều cảm xúc: thương cảm, đau xót, khó tin và cả tức giân.
Mình thương cảm cho gia đình của Jaycee và cả chính cô ây nữa. Chỉ vì tham muốn dục vọng của một kẻ xa lạ, mà họ đã mất đi vô số điều quan trọng trong cuộc sống. Nhưng cũng nhờ vậy, mình mới thấy bản thân còn hạnh phúc và may mắn hơn bao người, cám ơn Jaycee đã nhắc nhở mình về điều đó.
Mình thấy đau xót, vì Jaycee đã phải đối mặt với quỷ dữ ở cái tuổi còn quá nhỏ; mà xung quanh cô lại không có lấy một đôi tay nâng đỡ, che chở. Đôi khi mình rơi lệ, thấy đau đớn cho người con gái kia, cô ây mất hết tất cả: gia đình, bạn bè, tri thức và trên hết là sự TỰ DO.
Đây là câu chuyện có thật, nhưng thật tình mình không thể tin được là có những con người tàn ác như Phillip, và cũng có những kẻ vô tâm như Nancy. Có đôi lúc mình rất tức giận Jaycee, tại sao có những cơ hội đã đến, mà cô ấy không nắm giữ và bỏ trốn. Nhưng rồi mình chợt nhớ, Philipp đã giam cầm cô ấy quá lâu, khiến cô ây quá dữa dẫm vào nơi này và mất đi khả năng đấu tranh, thật tiếc vì cô ấy phải sống và phát triển trong một môi trường mà cái căn bản nhất cũng không có!
3
113010
2015-03-18 23:20:41
--------------------------
158996
5344
Ngay từ khi đọc tiêu đề cuốn sách "Cuộc đời bị đánh cắp - Hồi ức của một nô lệ tình dục", mình đã nghĩ đến một câu chuyện đầy bi kịch, nước mắt, và đau đớn. Nhưng những gì bên trong tác phẩm còn vượt xa những tưởng tượng đó. Câu chuyện này gây rúng động bởi chính những gì chân thực, đời thường nhất, bởi những lời kể vừa sắc lạnh vừa đầy tình cảm của tác giả. Một cô bé tuổi teen, đáng nhẽ sẽ có cả một cuộc đời tươi đẹp phía trước, nhưng sự lạm dụng của kẻ xấu đã đẩy cô vào những bi kịch tưởng chừng không có hồi kết. Những đoạn kể về thời gian Jaycee bị lạm dụng quá tăm tối, nhưng cũng ẩn chứa bên trong đó sức mạnh như một lời cảnh tỉnh cho mỗi người đọc. Giờ đây Jaycee đã đứng lên, đã nói ra sự thật cho mọi người, rồi nỗi đau sẽ dần lành lại, nhưng đó hẳn sẽ những ký ức cô sẽ khó có thể dễ dàng quên đi.
"Cuộc đời bị đánh cắp" xúc động trong từng trang sách, giữa những tuyệt vọng ta vẫn nhìn thấy ánh sáng của niềm tin. Cuốn sách giống như một "hạt giống tâm hồn" giúp bạn hiểu thêm về cuộc sống, với đầy đủ những gam màu sáng tối đối lập nhau.
4
109067
2015-02-14 14:52:50
--------------------------
150509
5344
Cũng như nhiều nhận xét khác, nhận xét của mình chia làm hai phần.
- Về mặt hình thức: sách có thiết kế bìa đẹp, khá là đơn giản, màu vàng nhạt của nó, tấm hình chụp cũ của cô Dugard gợi lên một màu buồn, nhưng mình thích, và cả quả thông được chèn vào mang một ý nghĩa đối với cuộc đời cô. Về chất lượng giấy thì nxb Trẻ luôn sử dụng giấy xốp nên rất nhẹ, đóng sách cũng tốt.
- Bây giờ là phần quan trọng. Khi đọc quyển sách này quả thực mình cũng phải lấy chút ít dũng cảm vì biết rằng sắp sửa đọc ít nhiều những nội dung nhạy cảm. Câu chuyện này đã nhiều lúc khiến mình kinh ngạc, rợn người trước những hành động dã man và bên cạnh đó còn có nhiều hành động không kém phần giả tạo, những lời biện hộ, những suy nghĩ điên rồ và sai lệch gieo vào đầu cô. Những 'kẻ đó' đối tốt với cô chỉ để lừa lọc, khiến cho cô tin vào chúng. Cô bị giam cầm suốt 18 năm liền, sinh ra 2 đứa con... Thật sự cô đã bị đánh cắp cuộc đời. Dù vậy, xuyên suốt cuốn sách có thể thấy cô là một người rất dũng cảm và mạnh mẽ dù phải chịu nhiều đau đớn về thể xác lẫn tâm hồn, và cô biết đặt tình yêu thương đúng chỗ, cô luôn nhớ đến mẹ, đến em gái mình, đến người thân, đến bạn mình, luôn hết mực yêu thương con mình, yêu những con vật nuôi của mình. Cô có một tâm hồn đẹp. Tâm hồn ấy xứng đáng một cuộc sống tốt đẹp hơn thế. Đã có những lúc cô có cơ hội để giải thoát nhưng cô đã không làm. Cô sợ nhỡ có chuyện gì xảy ra, sợ không bảo vệ được các con mình và còn sợ nhiều thứ khác nữa. Đến khi cô được giải thoát và hỗ trợ bởi FBI, chính quyền, cô phải tập thích nghi với thế giới bên ngoài và điều này khá là khó khăn, cô phải học lại rất rất nhiều. Nhờ có bác sĩ tâm lý và vòng tay của gia đình, cô đã có sức mạnh vượt qua. Thật sự rất mừng cho cô :)
Cảm ơn cô đã biết cuốn sách này vì nó đã tiếp thêm động lực sống cho cháu, cho cháu thấy cuộc đời này đáng sống và rằng cháu thật may mắn. Dù có thể nào ta cũng phải thật kiên cường. Cháu đã học tập được điều đó. Hy vọng quãng thời gian còn lại của cô sẽ thật ý nghĩa, chúc cô và gia đình sẽ mãi hạnh phúc, có được cuộc sống mình mong muốn.
Nhận xét của mình tuy có nhiều thiết sót nhưng vẫn mong là nó bổ ích. Vẫn còn nhiều điều muốn nói nhưng mình phải nhường lại cho cuốn sách ^^ Điểm trừ duy nhất là trong sách có một vài lỗi chính tả.
5
93237
2015-01-16 21:44:15
--------------------------
