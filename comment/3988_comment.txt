568313
3988
Cuốn sách có tên là Khẩu ngữ nhưng các câu lại viết  toàn chữ lào mà không có phiên âm,sách chỉ phù hợp với những người đã có 1 trình độ nhất định về tiếng Lào,do đó cần tìm hiểu trước khi mua.
2
1340720
2017-04-09 15:08:38
--------------------------
377146
3988
Cuốn khẩu ngữ tiếng lào này rất bổ ích cho những người học tiếng Lào. Sách bìa chắc chắn,giấy in rõ nét,giấy tốt. Nội dung sách hay,khá đầy đủ. Đa dạng trên các kiểu câu, tiếng việt và tiếng lào đầy đủ.Nhiều câu gần gủi với đời sống,những câu hỏi thăm,tục ngữ rất hay. Có nhiều từ vựng rất bổ ích và giải thích những chổ khó hiểu. Dịch vụ giao hàng tốt rồi nhưng cuốn sách bị gãy một chút ở mép sách, chắc là do quá trình giao hàng. Mong Tiki sẽ khắc phục được vấn đề trên. Cảm ơn!  
4
410182
2016-02-01 18:53:07
--------------------------
