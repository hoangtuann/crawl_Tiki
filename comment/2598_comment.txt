407103
2598
Bộ sách "Lớn lên mình sẽ làm nghề gì nhỉ" gồm rất nhiều trò chơi cho các bé, mình đã mua cho bé nhà mình và tặng cháu nữa, cả hai bạn be bé ấy đều mê tít bộ sách này. Bạn nhà mình còn đóng vai cô giáo để dạy mẹ xem xe cứu hỏa trông thế nào, xe cứu thương thì ra sao... giúp cả hai mẹ con học thêm được khá nhiều kiến thức.
Tuy nhiên hình vẽ trong sách màu sắc chưa đẹp lắm, một số hình nhìn khó đoán là vật gì nên nhiều khi mình cũng hơi lúng túng khi dạy bé.
4
1066198
2016-03-29 14:26:51
--------------------------
379374
2598
Em mình rất thích những thứ liên quan đến cứu hỏa và luôn ước mơ trở thành một chú lính cứu hỏa chữa cháy nên mình đã mua cho bé quyển sách này . Tuy nhiên khi nhận được sách thì nó khá mỏng . nhưng nó vẫn luôn là sách gối đầu giường của nó . nó mang sách đén bất cứ đâu và bất cứ chỗ nào . Tóm lại là bé rất thích sách . Đọc sách và nhờ thêm sự hướng dẫn của mình và bố mẹ nên bé đã hiểu thêm được về những chú lình cứu hỏa , về nghề cứu hỏa rất nhiều . cảm ơn tiki !!!
3
679440
2016-02-10 22:14:36
--------------------------
255472
2598
Cuốn sách cho bé biết những thông tin căn bản về nghề cứu hỏa thông qua các câu đố với hình ảnh minh họa chi tiết như bài về các vật dụng của lính cứu hỏa, phân biệt tình huống nào có thể gọi cứu hỏa, ... Vừa là một sách hay để học hỏi vừa là một cuốn sách giải trí cho các bé vừa học vừa chơi.
Nhóc cháu mình rất thích các chú lính cứu hỏa, một phần vì các chú lính cứu hỏa làm những công việc dũng cảm và giúp đỡ người khác như chữa cháy và cứu người, một phần vì ba của bé là lính cứu hỏa nên khi có cuốn sách này thì bé rất thích, ngày nào đi học về cũng lấy sách ra đọc, đánh vần còn chưa thạo nhưng vẫn đánh vần luôn miệng. Có thể nói cuốn sách này cũng góp phần kích thích hứng thú đọc sách của bé.
5
96853
2015-08-06 07:33:59
--------------------------
