412805
10834
Minh đã mua quyển sách này cách đây không lâu nhân đợt giảm giá gần 50% của Tiki. Mình đánh giá đây là một quyển sách có nội dung hay, bìa sách đẹp, bắt mắt. Tuy nhiên hình ảnh trắng đen nên nhin không sống đông lắm. Nội dung quyển sách phong phú, nhiều kiến thức mới về sinh vật được cung cấp. quyển sách này rất thích hợp cho những bạn trẻ đam mê khoa học và ham muốn tìm hiểu kiến thức về sinh học. Đây thật sự là một quyển sách rất đáng có mặt trên kệ sách gia đình bạn.
4
105202
2016-04-08 13:56:39
--------------------------
399745
10834
Tôi vừa mua sách này định cho con gái xem. Tiki giao hàng rất nhanh. Tuy nhiên, mở quyển sách thì thấy hơi thất vọng chút. Sách khá dầy, trình bày rõ ràng về nhiều loài sâu, bọ. Trong nội dung sách có nhiều hình ảnh sâu bọ rất sắc nét, song lại là hình ảnh trắng đen. Nếu nhà xuất bản tăng giá lên gắp đôi mà in hình màu thì những quyển sách thế này sẽ sống động và gần gũi hơn với trẻ nhỏ. Theo tôi sách khoa học cho thiếu nhi ngoài nội dung rõ ràng, chính xác thì hình ảnh là phần rất quan trọng giúp các cháu có nhận thức rõ ràng về kiến thức truyền đạt trong sách.
3
183099
2016-03-18 10:51:12
--------------------------
399391
10834
Mình vừa nhận được sách thì mở ra xem luôn, sách in trắng đen thôi nhưng cũng đầy đủ thông tin về loài ruồi, bọ. Vốn yêu thích tự nhiên nên mình rất thích tìm hiểu thêm về các loài côn trùng, cây cỏ... Sách này có hình minh họa rất chi tiết, dù đen trắng cũng có thể nhận dạng rất rõ ràng. Thông tin cũng rất lý thú và còn có một số mẹo nhỏ với các loài côn trùng nữa. Đây là cuốn sách rất đáng mua, có thể vừa để tham khảo vừa có thể cho mấy đứa cháu tìm hiểu thêm về thiên nhiên.
5
723139
2016-03-17 18:19:54
--------------------------
253301
10834
Là một quyển trong Bộ Sách Giữ Gìn Thiên Nhiên, Chuyện Ruồi Bọ nói lên nhiều điều kì thú về nhóm Ruồi, Bọ và một số loài côn trùng khác chuồn chuồn, sư tử kiến, chấy sách, phù du,…Sách có bìa in màu đẹp, các trang bên trong thì chỉ in hình trắng đen nhưng hình ảnh cũng rất rõ ràng, ấn tượng, vì khi người xem đọc thông tin về một côn trùng nào đó mà có hình ảnh minh hoạ kèm theo thì lượng kiến thức thu được thật là đáng quý. Sách chỉ khoảng 130 trang nói về chuyện ruồi bọ, nhưng đây không hề là chuyện tào lao, đọc cho vui, cho qua thời gian đâu nhé. Loài ruồi bọ nào có hại hay có ích đều sẽ được nêu rõ, chưa kể con người chúng ta còn mô phỏng cách bay của chúng để chế tạo máy bay nữa…
5
544151
2015-08-04 12:38:11
--------------------------
168180
10834
Với giá tiền như vậy, tôi không nghĩ rằng mình sẽ nhận được một cuốn sách vô cùng chỉn chu như thế này. Sách tuy không có hình màu minh họa, nhưng những hình minh họa trắng đen vẫn rất rõ ràng và dễ nhìn. Hình trong sách được trình bày đẹp mắt, ngẫu hứng, không rập khuông nhàm chán như kiểu trong sách giáo khoa. Nội dung khá đầy đủ, có những mục đóng khung nho nhỏ bật mí những thú vị bên lề của loài côn trùng được nhắc đến. Đây là một cuốn sách rất hữu ích với những ai muốn tìm hiểu về thế giới tự nhiên. Thật may mắn vì tôi đã kịp mua đầy đủ cả bộ.
5
57860
2015-03-16 09:46:57
--------------------------
