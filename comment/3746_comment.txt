486131
3746
Đã đọc cả 2 tập và mình đánh giá cao tập 1 hơn. Mình thích cách hành văn của tác giả, thẳng thắn, mạch lạc và đúng trọng tâm. Ngay từ những dòng đầu tiên đọc thử trên tiki thì mình đã muốn mua rồi vì bản thân khá cuồng những cuốn tiểu thuyết liên quan đến ma cà rồng.
Bìa đẹp, hút mắt người nhìn, nội dung cũng khá dễ đoán nhưng không nhàm chán. Chất lượng giấy in cũng tốt, tuy có một vài lỗi chính tả nhỏ nhưng không quá quan trọng.
Đây là cuốn sách đầu tiên mình đặt trên Tiki và rất hài lòng về cách làm việc của Tiki.
4
1659151
2016-10-09 23:19:18
--------------------------
452816
3746
Mình rất thích thể loại huyền bí về ma cà rồng và tình yêu giữa ma cà rồng và người. Vừa thấy truyện là mình mua ngay đến khi thấy tác giả thì ngạc nhiên vì tác giả Hàn Quốc nên mình nghĩ chắc là truyện teen rồi, đến khi đọc thì lôi cuốn ngay. Mình thấy tình cảm dần dần phát triển giữa 2 người mà không một người một ma cà rồng chuyên hút máu, mạch truyện cuốn hút người đọc, kịch tính nhưng cũng ngọt ngào lãng mạn. Bìa truyện khá ấn tượng vì sự huyền bí mình rất thích. Mong có nhiều tác phẩm hay như vậy của tác giả.
4
581978
2016-06-20 20:48:11
--------------------------
427843
3746
Mình rất ít khi đọc tiểu thuyết nhưng thực sự đã bị lôi cuốn bởi quyển sách này . Bìa sách mang vẻ mộng mị , nội dung lôi cuốn và lời văn rất tốt . Câu chuyện được xây dựng kịch tính và từ từ tháo gỡ từng gút mắt , nhẹ nhàng đưa họ vào tình yêu từ lúc nào không hay , miêu tả tâm ý nhân vật cũng rất sắc xảo . Truyện  thuộc thể loại giả tưởng , mang nhiều tình tiết bất ngờ đó là điểm mình thấy rất thích ở truyện . 
Đây quả thật là cuốn sách không nên bỏ lỡ .
5
794067
2016-05-10 09:45:47
--------------------------
410156
3746
Truyện lôi cuốn người đọc ngay từ tên truyện ''Ma cà rồng và em'',truyện rất hay và hấp dẫn,miêu tả ngoại hình ,tâm lí nhân vật một cách hoàn hảo ,diễn biến câu chuyện lôi cuốn,kịch tính nhưng không kém phần lãng mạn ...suy nghĩ của chàng trai Louis ma cà rồng thực sự rất sâu sắc,có 1 tình cảm đẹp với cô nàng cấp 3
Truyện sẽ diễn biến tiếp theo như thế nào,thực sự là rất tò mò, cảm ơn vì đã cho tôi 1 câu chuyện khiến tôi hứng thú và phấn khởi ,hạnh phúc.quả thực rất hay ...1 chuyện tình đẹp
5
1252515
2016-04-03 17:04:44
--------------------------
397922
3746
               Khi mới nhìn thấy tên của cuốn truyện và màu sắc của nó thôi tôi cũng đã cảm thấy một điều gì đó khá bí ẩn và hơn hết là sự rùng rợn của nó. Bìa truyện trang trí không quá cầu kì kiểu cách . Sau khi nhìn qua cuốn truyện tôi cảm nhận được rằng :" chắc hẳn tác giả là một người khá đơn giản ( nhưng có lẽ chỉ là suy đoán). Nhưng sau khi đọc xong cuốn truyện đầy bí ẩn ấy tôi mới nhận ra:"Tác giả hẳn là một người rất tinh tế bởi khi đọc truyện tôi cảm thấy như đang là một trong những nhân vật trong cuốn truyện vậy".
5
568619
2016-03-15 16:44:40
--------------------------
392871
3746
Truyện khá kịch tính và lôi cuốn người đọc bước vào câu chuyện. Tuy tên truyện không mấy hay cho lắm nhưng nội dung có thể nói là không thê chê được. Lại được chuyển thành phim truyền hình thiết kế nhân vật khá tuyệt mỹ. Cả hai cuốn một và hai khá dày nhưng cũng đủ lượng trang giấy bút mực tạo lên tác phẩm rất ưu chuộng với giới trẻ. Chàng trai ma cà rồng Louis cao ngạo và một cô bé Seo-Young dễ thương tạo nên câu chuyện ngọt ngào về đêm. Đọc nó khiến tôi không kiềm được mà cứ lật sang trang mới rời lại trang mới đến khi hết cuốn rồi chẳng hay, quả là một cuốn sách hay.
4
896453
2016-03-07 20:30:43
--------------------------
388620
3746
Quyển sách là một trong số những quyển sách viết về ma cà rồng rất hấp dẫn tôi. Từ bìa sách mang vẻ cổ kính quyến rũ cho đến chất lượng giấy in cũng khá tốt. Thích nhất vẫn là cách hành văn của tác giả và luôn cả cách người dịch sách làm cho từng chi tiết miêu tả nhân vật rất sống động, có phần trẻ con, dễ thương nhưng cũng đầy sát khí cho nhân vật  Louis và nhân vật Seo Young mang đúng nghĩa là một cô nữ sinh trẻ trung, năng động, có phần mơ mộng và có cả những tình cảm của tuổi mới lớn thật đẹp. Tình cảm nhẹ nhàng đó cứ dần lớn trong cả hai người cho đến một lần Seo Young nhìn thấy một chàng trai giống hệt như cậu mà nào biết rằng đó mới chính là hình dáng thật của Louis khi đã trưởng thành. Mái tóc đen nhánh, thân hình quyến rũ và đôi mắt đỏ nổi bậc chỉ duy nhất của ma cà rồng, ngay lúc đó, cô biết mình đã ko thể thiếu cậu trong cuộc sống. Trải qua bao nhiêu khó khăn và đâu đớn liệu rằng họ có thể đến với nhau không? Câu chuyện kết thúc mở và vẫn còn tiếp tập hai. Nếu như bạn là một fan của ma cà rồng hay những quyển sách lôi cuốn, lãng mạn thì quyển sách là lựa chọn tốt cho bạn.
4
1170529
2016-02-29 12:29:23
--------------------------
362569
3746
Qua xá hay luôn. Lâu lắm rồi mới có tiểu thuyết khiến mình hứng thú như vậy.
Thứ nhất là bìa nhìn rất lôi cuốn, hấp dẫn
Thứ hai, trang giấy trắng. chữ rõ ràng tuy nhiên còn một vài lỗi chính tả nhỏ trong lúc đánh máy
Cuối cùng là cốt truyện khiến mình đọc miết không rời. Shin Ji Eun thật là giàu trí tưởng tượng. Lời văn chặt chẽ, liên kết. Đặc biệt khi đọc vào thì cảm thấy  cảm xúc của các nhân vật rất thật. Biểu cảm tốt. Đây là cuốn sách rất bổ ích cho mình.
5
700281
2016-01-02 10:52:54
--------------------------
353900
3746
Tác giả người Hàn quốc ra đời tác phẩm này chỉ có một thứ giữ người đọc ở lại với bản chất đó là con người và cái thiện ác ở lại hay ra đi , có nên dừng lại trước khi quá muộn , để ta hối hận với cuộc đời nữa không , còn việc làm một con ma chuyên hút máu người nhưng có tình yêu níu giữ thì bản thân nó lại không mang theo cái xấu , độc địa mà cần một thứ sâu sắc , có ý nghĩa quan trọng với chúng , dù sao vẫn mang dòng máu của người và quỷ .
4
402468
2015-12-17 04:51:14
--------------------------
342210
3746
Mình vừa tậu 2 cuốn. Tiki giao hàng rất nhanh, dịch vụ của Tiki luôn tốt. :)

Mình đọc review thấy mọi người khen nhiều nên tò mò mua thử. Truyện cũng được chuyển thể thành phim nên nghĩ truyện cũng 'hot'. Nhưng mà mới đọc có mấy chương thấy ko lôi cuốn lắm. Cuộc gặp gỡ giữa Seo Young và Louis chẳng hấp dẫn, thú vị tẹo nào. Hi vọng đọc tiếp sẽ khá hơn. Cách hành văn (hoặc do cách dịch) khiến mình chưa có cảm tình với truyện, với nhân vật...Sẽ cố gắng đọc tiếp và update review.
3
30721
2015-11-23 23:07:13
--------------------------
338711
3746
Điều đầu tiên mình muốn nói là cuốn sách thực tuyệt vời. Không từ khi nào mình đã đi cuồng "MA CÀ RỒNG". Và cùng là lần đầu tiên mình đọc sách, tác giả là người Hàn Quốc.Lúc đầu có chút phân vân nhưng vẫn mua. Khi đọc hết sách, không cảm thấy tiếc mà quyển sách rất hợp với giá tiền.
Nội dung khỏi chê. Nói về một người con trai Ma cà rồng tên là Lube Lousi, đã giao kết với ng con gái tên là Seo Young để tìm được bông hoa Ma cà rồng. Cuộc sống bị đảo lộn từ ki gặp ng tên Lube Lousi đó. Nhưng sau nhiều lần cùng anh tham gia vào cuộc tìm kiếm, cô đã không tránh khỏi những rung động với anh. Cô đã thầm thích anh, nhưng anh là Ma cà rồng làm sao có thể thích con người được? Cuộc sống sẽ ra sao của anh và cô? 
Bìa sách cực đẹp, khỏi chê. Vừa nhìn đã cuốn hút lòng người. Nữ tác giả viết sách rất thú vị, dẫn dắt mọi người đến hồi hộp - bất ngờ. Mong sẽ ra tập 2 sớm.

5
604786
2015-11-16 19:27:24
--------------------------
337909
3746
Mình thường đọc các tác phẩm của nước ngoài, phần lớn là Âu Mĩ, kế là Nhật. Đây là lần đầu tiên mình đọc cuốn sách mà tác giả là người Hàn Quốc.
Ban đầu, do quen với lối viết nhịp tấu nhanh của những cuốn sách Âu Mĩ mình mua, nên "Ma cà rồng và em" đối với mình thì tiết tấu có hơi chậm.
Tuy nhiên, dù cảm nhận được "sự chậm" ấy, nhưng mình không hề thấy nhàm chán, ngược lại, từng câu tưng chữ như râm rỉ rót từ từ từ từ vào mạch đọc. 
Đặc biệt, lối hành văn của tác giả, cách miêu tả tâm lí, nhân vật rất sinh động, làm người đọc như cảm nhận từng trang sách như từng scene của một bộ phim, có cảm giác mường tượng được rõ nét cả nhân vật và không gian của câu chuyện.
Cuốn hút một cách nhẹ nhàng đến nỗi dù môn thi cuối cùng sắp đến nhưng mình đã phải dành nguyên một ngày để đọc xong "Ma cà rồng và em" mới có thể tập trung vào ôn bài được.
Chắc mình cũng sắp sửa hốt luôn cuốn tập 2, cơ mà tập 2 "Ma ca rồng và em" mình thấy hơi thất vọng về bìa. Nếu bìa tập 2 thiết kế theo phong cách như tập 1 thì ấn tượng và độc đáo hơn. 
(klq cơ mà yêu tiki ghê cơ. đặt sách ngày thứ 6, bảo là thứ 7 chủ nhật không làm mà sáng thứ 7 mình đã nhận được sách, chưa đầy 24 tiếng! Tiki tuyệt bá cháy!)
5
63415
2015-11-14 23:05:41
--------------------------
328633
3746
Mình có một niềm đam mê mãnh liệt với ma cà rồng. Cuốn sách này tuy chưa thể nới là thực sự hoàn hảo nhưng cũng đem đến cho người đọc trải nghiệm khá thú vị và mới mẻ. Một giao kết giữa con người và ma cà rồng. Câu chuyện tình cảm của Seo Young và Louis có thể khá dễ thương và ngọt ngào nhưng giọng văn hơi dài dòng và lan man làm mình hơi không thoải mái lắm. Có một điểm mình không thích ở câu chuyện này là việc không có ma cà rồng nữ trong thế giới ma cà rồng. Tuy bối cảnh giao kết là ở Hàn Quốc nhưng những ma cà rồng trong truyện đều có xuất thân từ châu Âu thời trung cổ, tại nơi mà có cả ma cà rồng nam và nữ, điều này làm câu chuyện chưa hợp lí lắm nhưng cũng tạo nên nét độc đáo của riêng nó. Mình sẽ theo dõi tiếp bộ truyện. Cảm ơn  Phúc Minh vì đã xuất bản một cuốn sách như vậy. Cuối cùng mình rất hài lòng với thời gian giao hàng của Tiki và sẽ tiếp tục với nhiều đơn hàng nữa
3
392456
2015-10-30 09:23:15
--------------------------
292314
3746
Nội dung chuyện xoay quanh tình yêu giữa ma cà rồng Louis và Seo-Young - một nữ sinh trung học. Nghe thì có vẻ giống bộ Chạng vạng nhưng nội dung truyện hoàn toàn khác hẳn và có sức hút riêng. Đã xem thử vài tập phim cùng tên nhưng mình thấy truyện được viết khác và hay hơn phim rất nhiều. Bìa sách đơn giản quá nhìn không bằng bìa của Hàn. Tập 1 kết thúc ngay cao trào nên mình rất ngóng tập 2. Louis rồi sẽ chọn Seo Young hay ngôi vị chúa tể? Cô bé Hoa ma cà rồng rồi sẽ ra sao, cô sẽ chọn ai làm chúa tể? Beak Han có thoát khỏi thân phận Haft trở thành ma cà rồng thực thụ hay không? Bao điều cần giải đáp mà tập 2 giờ không thấy bong dáng đâu. Sách đánh sai và nhiều lỗi chính tả kinh khủng chữ nọ xọ chữ kia đọc mất hứng bực mình dễ sợ nếu mà không có nội dung hay đỡ lại thì thôi rồi. Mong rằng Tiki đưa các phản ảnh của đọc giả đến NXB để họ làm việc nghiêm túc ở cuốn 2 hơn.
5
43129
2015-09-07 10:32:49
--------------------------
247117
3746
Nghiền rồi đấy. Đây là quyển sách tiểu thuyết đầu tiên mình đọc của Hàn Quốc . Nó thật sự rất hay , thú vị và cuốn hút. Nội dung chuyện xoay quanh tình yêu giữa ma cà rồng Louis và Seo-Young - một nữ sinh trung học . Tác giả rất biết cách dẫn truyện và tạo nút thắt cho câu chuyện một cách tài tình . Về phần bìa truyện mình thật sự rất thích , nó được thiết kế rất đơn giản không cầu kì. Với những ai là fan hâm mộ những câu chuyện lãng mạn , viễn tượng thì đây sẽ là một quyển sách rất thích hợp. Mà khi nào mới dịch tập 2 vậy, Mong quá!!!!!!!!
Nhưng sao bìa sách của mình không giống như trên hình tí nào, không nhìn rõ được tiêu đề. Sai nhiều chính tả. 

5
705131
2015-07-30 09:51:22
--------------------------
231645
3746
Đây là quyển sách tiểu thuyết đầu tiên mình đọc của Hàn Quốc . Nó thật sự rất hay , thú vị và cuốn hút. Nội dung chuyện xoay quanh tình yêu giữa ma cà rồng Louis và Seo-Young - một nữ sinh trung học . Tác giả rất biết cách dẫn truyện và tạo nút thắt cho câu chuyện một cách tài tình . Về phần bìa truyện mình thật sự rất thích , nó được thiết kế rất đơn giản không cầu kì. Với những ai là fan hâm mộ những câu chuyện lãng mạn , viễn tượng thì đây sẽ là một quyển sách rất thích hợp. Mình rất mong tập 2 !!!
5
391948
2015-07-18 10:56:04
--------------------------
223913
3746
Bìa sách thiết kế rất đơn giản nhưng ấn tượng, vẫn thích bìa sách của hàn hơn và còn nữa là tiki giao cho mình bị rách ở bìa sau của sách, mình định đổi nhưng gửi bưu điện thấy bất tiện nên vẫn giữ đọc. Khi đọc rất cuốn hút ,hấp dẫn và nhiều câu viết rất lôi cuốn. Mình thì thường không đọc. truyện nhiều nhưng nó đã gây ấn tượng bởi bìa sách và tác giả đã viết lách rất hay, mình mới đọc những phần đầu thôi đã thấy hồi hộp và khá vui pha nhiều cái thú vị . Hy vọng khi đọc xong truyện này sẽ có tập 2 luôn nhỉ. 
5
419285
2015-07-07 19:58:17
--------------------------
218953
3746
Từ đó đến giờ mình xem phim Hàn QUốc nhiều rồi nhưng chưa đọc cuốn tiểu thuyết nào của Hàn cả
Và tác phẩm này đã thực sự, thực sự... khiến mình KHÔNG HỀ THẤT VỌNG ♥
Ngay từ khi mình đọc cái lời giới thiệu là đã thích truyện này lắm rồi, thế là quyết định tậu về luôn =)))
Bìa sách thiết kế đơn giản, không cầu kì nhưng lại khá ấn tượng. Nhưng thú thật là mình thích bìa sách của Hàn hơn ="> ( hình mấy lăng kính trong nhà thờ í bạn)
Về nội dung thì không thể không khen được. Với câu chuyện tình yêu giữa chàng Ma cà  rồng Louis và cô gái loài người Seo Young, tác giả Shi Ji Eun đã dẫn dắt độc giả vào thế giới của sự huyền bí, của những chuyến phiêu lưu kịch tính nhưng không kém phần lãng mạn và ngọt ngào đặc trưng của Hàn Quốc, tất cả cùng đan xen tạo nên 1 tác phẩm cực kì xuất sắc khiến bạn một khi đã đọc là không thể nào dứt ra được =">
Mình mong nhà xuất bản nhanh nhanh cho ra mắt tập 2 đi, chứ dừng ngay cái đoạn kịch tích làm dân tình chờ dài cổ lắm rồi ="<
Hồi đó tình cờ xem được anime về ma cà rồng cái rồi thích mê luôn. Cuốn tiểu thuyết lần này cũng vậy. Chỉ là tình cờ lướt ngang qua Tiki, tình cờ thấy cái tựa đề "Ma cà rồng và Em"... ♥
5
472746
2015-06-30 23:36:28
--------------------------
207228
3746
Vô tình mình lướt ngang đây thấy tựa quyển sách này làm mình rất ngạc nhiên , mình không nghĩ  '' Ma Cà Rồng Và Em '' lại được xuất bản ra sách !!
Mình đã xem bộ phim này khá lâu rồi , nó nói về chuyện tình yêu của một anh chàng vampire - Louis và một cô bé học sinh - Kang Seo Young .
Xem phim Hàn Quốc thì nhiều rồi nhưng chưa bao giờ mình đọc một quyển sách ( tiểu thuyết ) của Hàn Quốc  . 
Không nhiều quyển sách nói về Vampire cho lắm nên mình rất hứng thú với nó ....
Diễn biến khá thú vị , kì bí , hồi hộp , pha lẫn chút hài hước thật sự làm cho người đọc không thể nào bỏ qua
Mình chưa đọc truyện nên không biết nó có khác phim hay không , nhưng nếu bỏ qua chắc chắc ta phải hối tiếc =]]]]]]]]]]]]]]
5
445182
2015-06-11 19:14:14
--------------------------
202838
3746
Shin Ji Eun quá xuất sắc khi lựa chọn một chủ đề cuốn hút và hấp dẫn người đọc. Lật từng trang sách cứ như là đang ở trong câu chuyện vậy, rất hấp dẫn.
Chuyện tình giữa cô học sinh cấp 3 Seo Young ngây thơ và chàng ma cà rồng quyến rũ tên là Lube Louis sẽ như thế nào? Liệu họ có đến được với nhau, điều này làm mình rất tò mò. Cách dẫn dắt và tạo nút thắt cho câu chuyện rất thông minh, làm cho mình bị cuốn hút theo từng trang sách. Cách miêu tả từng nhân vật rất tinh tế và chi tiết.
Có thể nói đây là một cuốn sách về chủ đề ma cà rồng hay nhất mà mình từng đọc.
Bìa mềm, trang sách dày, màu cũng trắng, chữ in rõ.
Đây là một cuốn sách hay đáng để mua!
5
621744
2015-05-30 18:01:41
--------------------------
202011
3746
Mình đã từng xem bộ phim này rồi nó thực sự rất hay và lôi cuốn mình vô cũng thích và rất vui khi biết được Ma cà rồng và em được chuyển thể thành sách ^^. nội dung xoay quanh câu chuyện anh chàng ma cà rồng Louis cực kì đẹp trai và cô nàng học sinh trung học Kang Seo Young với tham vọng muốn trở thành chúa tể ma cà rồng nên Louis đã tiếp vận Kang Seo Young với mục đích tìm được loài hoa quý. Đôi lúc còn có những tình huống dở khóc dở cười khiến bạn một khi đã cầm nó trên tay thì sẽ không muốn bỏ xuống. Đây là một cuốn. Sách rất hay và đáng mua và đặc biệt không thể thiếu đối với những ai là fan của thể loại lãnh mạng pha chút kì Bí này.
4
589886
2015-05-28 17:56:47
--------------------------
201476
3746
Truyện Ma Cả Rồng và em thực sự cuốn hút mình ngay từ bìa sách đến nội dung khá Logic nhưng không kém phần hài hước. Truyện kể về  một anh chàng Ma cà rồng đẹp trai, tham vọng trở thành chúa tể Ma Cà rồng tên là Louis và cô học sinh trung học bình thường Kang Seo Young. Với một chút phiêu lưu, kì bí kết hợp với những tình huống dở khóc dở cười sẽ đem đến cho bạn nhiều cảm xúc. Một cuốn truyện rất đáng mua.
Nếu bạn là Fan của những cuốn truyện lãng mạn pha lẫn nhiều yếu tố ly kì thì đây thực sự là cuốn truyện bạn nên đọc
4
280007
2015-05-27 15:34:16
--------------------------
201118
3746
Về hình thức bìa truyện mặc dù đơn giản và giản dị nhưng lại mang chút gì đó rất cổ điển thu hút tôi từ cái nhìn đầu tiên . Về chất lượng giấy in khá tốt , mềm , rõ ràng nhưng vẫn còn vài sai sót trong dịch thuật . Về nội dung đây là lần đầu tiên tôi đọc truyện của tác giả Shin Ji-Eun. và không thể phủ nhận là cô đã viết rất tốt . Nền tảng câu chuyện ổn định và vững vàng . Cách xây dựng thế giới nơi mà ma cà rồng và con người sống chung thật sự rất logic . Thích tính cách của nam chính có vẻ gì đó rất lưu manh . Nữ chính còn khá mờ nhạt chưa thực sự bộc lộ được tính cách của mình
4
635463
2015-05-26 17:58:43
--------------------------
200152
3746
Ấn tượng đầu tiên đó chính là bìa sách. Tuy đơn giản. Nhưng mình thích sự đơn giản đó. Nội dung xoay quanh chàng ma cà rồng Louis và cô nữ sinh với những tình huống giở khóc giở cười. Nội dung chuyện mạch lạc, cuốn hút người đọc. Mình thực sự ngưỡng mộ tác giả, xuất thân từ một sinh viên kĩ thuật mà có thể viết nên những câu văn gảy gọn, trau chuốt đến như thế. Quay lại phần hình thức, truyện được in với chất lượng giấy khá tốt, ít sai chính tả. Nói chung, cũng nên chi tiền ra mua về đọc =)
4
587976
2015-05-24 09:55:58
--------------------------
