412044
5921
Ngoài những nhân vật đã quá kinh điển như Scoopy Doo, Tin-Tin, nay tôi biết thêm về nhân vật Zozo (phiên âm từ tên George nếu bạn nào muốn lên mạng tìm lại nguyên gốc để xen). Zozo rất nghịch ngợm, chưa tới mức phá hoại gây hậu quả nghiêm trọng nhưng cũng để người ta rối beng lên. Cũng may là tất cả các kết chuyện Zozo đều kết thúc có hậu. Các bé trai thích khỉ Zozo lắm, y như khỉ là đồng minh cho những trò quậy đó vậy.
Giấy in trắng nhưng hơi bóng, chữ nhiều nên đọc hơi nhức mắt.
5
63996
2016-04-06 23:09:34
--------------------------
273916
5921
Quyển sách là người bạn nhỏ thân thiết của con gái tôi. Mỗi ngày tôi phải đọc đi đọc lại cả chục lần cho bé nghe. Bé cứ thích mê chú khỉ nhỏ Zozo vô cùng nghịch ngợm, tò mò nhưng rất đáng yêu. Thông qua quyển sách bé còn học hỏi được nhiều bài học bổ ích cho bản thân mình. Hình vẽ đẹp, sinh động, đáng yêu cũng là yếu tố khiến bé thích thú. Và giờ đây Zozo đã trở nên rất thân thuộc với cả gia đình của tôi. Quyển sách rất thú vị. Cám ơn tiki.
5
110777
2015-08-21 14:46:50
--------------------------
253425
5921
Con trai  3 tuổi của mình thích khỉ Zozo vô cùng. Số lần cu cậu đòi bố mẹ, ông bà đọc cho mỗi tập truyện có lẽ lên đến hơn chục lần rồi, mà lần nào mở ra đọc cu cậu cũng háo hức và cười khanh khách. Zozo thật là một chú khỉ hiếu kì, nghịch ngợm hết thuốc chữa, chú luôn tay luôn chân với những trò nghịch và những tò mò mới mẻ của mình, dù sau đó xảy ra chuyện gì thì lại ân hận, còn tự hứa là sẽ không tò mò nữa.... Zozo có tính cách giống một đứa trẻ hiếu động, và chắc hẳn đó là lý do tại sao các cậu bé nghịch ngợm lại thích Zozo đến thế.
5
82774
2015-08-04 14:27:18
--------------------------
248809
5921
Không có gì để hối hận để vợt cả bộ về cho con trai khi cậu nhóc chưa được 1 tuổi^^ Bây giờ gần 3 tuổi rồi, bé say mê bộ này lắm, chú khỉ nghịch ngợm là vậy đấy nhưng mỗi câu chuyện đều biết hối hận và sửa sai cho những lỗi lầm mà chú gây ra.
Tối nào 2 mẹ con cũng say sưa với những chuyến phiêu lưu kì thú của chú khỉ, sau đó là những nhắc nhở bé con của mẹ ko vấp phải các sai lầm giống Zozo nữa!
Mong có nhiều bộ truyện như thế này hơn nữa!!!!
5
490928
2015-07-31 10:16:23
--------------------------
141665
5921
Khỉ Zozo Hiếu Kỳ là tập truyện với nhiều hình vẽ minh họa đẹp, văn phong chau chuốt, thực sự là một món quà ý nghĩa đối với các bạn nhỏ. Mỗi truyện lại mang tới những nội dung thú vị, cuốn hút khiến các em bé nhỏ tuổi khả năng tập trung kém vẫn kiên trì lắng nghe ba mẹ đọc hay ngắm nhìn từng bức tranh về anh bạn khỉ đáng yêu. 
Bộ truyện xoay quanh những hoạt động gần gũi, thân thuộc hàng ngày đối với các bạn nhỏ. Cái tên khỉ Zozo đã trở nên thân thuộc đối với bạn bé nhà mình!
5
195898
2014-12-16 10:46:16
--------------------------
141663
5921
Khỉ Zozo Hiếu Kỳ là tập truyện với nhiều hình vẽ minh họa đẹp, văn phong chau chuốt, thực sự là một món quà ý nghĩa đối với các bạn nhỏ. Mỗi truyện lại mang tới những nội dung thú vị, cuốn hút khiến các em bé nhỏ tuổi khả năng tập trung kém vẫn kiên trì lắng nghe ba mẹ đọc hay ngắm nhìn từng bức tranh về anh bạn khỉ đáng yêu. 
Bộ truyện xoay quanh những hoạt động gần gũi, thân thuộc hàng ngày đối với các bạn nhỏ. Cái tên khỉ Zozo đã trở nên thân thuộc đối với bạn bé nhà mình!
4
195898
2014-12-16 10:45:29
--------------------------
