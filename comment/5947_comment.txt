559205
5947
Nhìn cuốn sách dày, có vẻ ngán ngẩm nhưng khi đọc thì bị cuốn hút. Mọi điều bí ẩn về mật mã, từ thời xa xưa đến bây giờ, quá trình hình thành mật mã, quá trình bảo mật hoá nó. Giải thích cực kì hay, chỉ từng bước về việc mã hoá rất tỉ mỉ. Có hẳn ví dụ cho bạn tự làm để hiểu thêm về mật mã ấy nhe. Các loại mật mã thông dụng thường được sử dụng đề được giải thích hết.
5
562260
2017-03-30 12:45:48
--------------------------
436115
5947
Mình quyết định chọn cuốn sách sau khi đọc một số đánh giá và nhận xét của các bạn đọc, và như các bạn thấy đấy, cuốn sách được đánh giá và cho điểm rất cao. Nói thật khi mua về mình có hơi ngao ngán do lật qua sơ sơ thì thấy nào là số, nào là ký tự,... nên mình sợ đọc không nổi. Nhưng khi quyết tâm đọc rồi bạn sẽ thấy nó cung cấp cho bạn nhiều kiến thức về mật mã như thế nào, rất là hấp dẫn.
Về hình thức của cuốn sách: sách khổ hơi nhỏ so với nhiều tác phẩm cùng lĩnh vực, sách chứa nhiều kiến thức nên dày, về giấy thì không phải bàn, giấy tốt và màu của giấy không khiến người đọc đau mắt, đấy là điều mình rất thích.
4
1176595
2016-05-26 10:10:41
--------------------------
428069
5947
Tuy chủ yếu tập trung vào mã hóa theo chữ cái latin, nhưng sách đã nêu đủ các cơ bản về các phương pháp mã hóa và cách trao đổi thư. Sách dễ đọc bằng những câu chuyện lịch sử và cũng giúp ta biết được 1 phần của những chiến thắng lớn của phe Đồng Minh trong thế chiến thứ 2. Đọc sách mới thấy trí tuệ con người thật siêu phàm.
Riêng mình, sau khi đọc hơn 1/2 cuốn thì bắt đầu lập 1 khóa mã và thay đổi các mật khẩu của các tài khoản theo khóa đó. Vừa dễ nhớ và vừa khó cho người khác lần mò.
5
799169
2016-05-10 16:04:55
--------------------------
417953
5947
Khoa học về mật mã luôn thứ con người luôn truy tìm nhằm một mục đích nào đó. Từ  ngày xưa người ta sử dụng mật mã để bảo mật thông tin, để luôn đảm bảo rằng chỉ có người gửi và người cần được nhận thông tin mới có thể giải mã được ý nghĩa này. Ngày nay cũng vậy khi thời đại công nghệ thông tin bùng phát mật mã luôn được người ta ngụy trang khéo léo và tinh vi hơn. Nhưng với những người tò mò và muốn khám phá ra mọi thông tin thì cuốn sách này đúng là cuốn cẩm nang sống. Quyển sách thật sự rất bổ ích.
5
509425
2016-04-18 11:07:06
--------------------------
413296
5947
Từ mật mã hoán vị, mật mã thay thế, mật mã vigenere, đến cỗ máy giải mã engima, mã hóa lượng tử, cuốn sách là một thư viện đầy đủ về các loại mật mã: cách sử dụng, những biến cố lịch sử liên quan, và cách giải những mật mã đó được con người tìm ra như thế nào. Luôn có một cuộc chiến không ngừng giữa những người tạo mã và giải mã, cuộc chiến đó vẫn tiếp tục đến ngày nay (Apple và FBI là ví dụ điển hình). Cuốn sách cung cấp một cái nhìn toàn diện về vai trò của mật mã trong cuộc sống, xã hội, trong chiến tranh. Phần mật mã lượng tử sẽ khá khó hiểu nếu bạn không có kiến thức cơ bản về vật lí lượng tử. Nhưng phần cũng chỉ chiếm một chương trong cuốn sách mà thôi. Nói chung tìm hiểu về mật mã rất thú vị, hoàn toàn có thể ứng dụng vào cuộc sống hàng ngày. Còn gì thú vị hơn khi một dòng chữ, với người khác là một dãy các kí tự vô nghĩa, với bạn thì là một lời...tỏ tình cơ chứ!? =]]
5
791697
2016-04-09 12:44:12
--------------------------
410102
5947
Tôi rất thích đọc những cuốn sách và nghiên cứu về những điều bí ẩn, trong đó có mật mã. 
Cái tôi thích ở cuốn sách này không phải là những phân tích kỹ thuật mật mã kỳ công mà là cách giới thiệu những bộ mật mã từ cổ chí kim rất dễ hiểu thông qua các ví dụ minh họa.
Qua cuốn sách, những người yêu thích mật mã sẽ có cái nhìn rộng hơn về lịch sử mật mã thế giới.
Tuy nhiên, các bộ mật mã được trình bày trong sách còn ít so với tên cuốn sách. Tôi mong muốn sẽ có thêm nhiều cuốn sách về mật mã khác đầy đủ hơn nữa để thỏa mãn niềm đam mê của tôi và những người yêu mật mã như tôi.
4
756470
2016-04-03 15:19:37
--------------------------
396715
5947
Quyển sách này sẽ cho bạn một cái nhìn khái quát về mật mã đúng như tên gọi của nó. Bắt đầu với mật mã cổ điển và kết thúc với mật mã hiện đại. Bạn sẽ không cần bất kì một kiến thức chuyên môn nào để có thể hiểu được hết quyển sách, vì tác giả đã đơn giản hóa mọi khái niệm. Nó là một cuốn sách dành cho mọi người, ở mọi trình độ kiến thức muốn tìm hiểu mật mã một cách nghiêm túc hay chỉ xem cho biết.
Bạn nên đọc qua quyển sách này.
4
588975
2016-03-13 22:41:11
--------------------------
395866
5947
Tôi đã có 2 cuốn sách rất của vị của Simon đó là Mật mã và Định lý cuối cùng của Fermat (rất hay nếu các bạn chưa có hay tìm đọc)

Tất cả sách của Simon hầu như đều phù hợp cho trình độ học sinh cấp 2 trở lên vì nó được tác giả viết rất dễ hiểu và thú vị, bạn không cần phải có IQ cao ngất mới hiểu hết được cả cuốn sách và cuốn Mật mã này cũng không ngoại lệ.

Nó sẽ dẫn dắt chúng ta nhìn lại lịch sử dưới góc nhìn của mật mã học. Cuốn sách bắt đầu bằng những dạng mật mã  đơn giản nhất ra đời từ cuối thế kỉ 16 và kết thúc ở cuối thế kỉ 20 bằng việc giới thiệu về ý tưởng mật mã lượng tử, một loại mật mã được cho là bất khả chiến bại dựa trên lí thuyết lượng tử.
1. Máy Enigma
2. Giải mã các văn tự cổ xưa
3. Phân phối chìa khóa mã
4. Mật mã lượng tử

Rất thích hợp với các bạn thiên về khoa học tự nhiên.
5
1096825
2016-03-12 16:00:51
--------------------------
387276
5947
Cuốn sách kể về lịch sử của mật mã bằng những câu chuyện có thực, cách dẫn dắt đầy cuốn hút và những kiểu mật mã kì diệu. Tác giả viết rất dễ hiểu, cộng với nhiều hình minh họa giúp người đọc dễ hình dung. Cuốn sách được chia làm nhiều chương với từng thời kỳ của mật mã, mỗi chương là một câu chuyện về chính loại mật mã đó.
Về hình thức thì khỏi bàn: gáy không bị quăn khi giở sách, không có lỗi chính tả, không dịch tên kiểu phiên âm tiếng Việt, ..., nói chung là đáp ứng những yêu cầu cơ bản của người đọc sách. Lần tái bản này không hiểu sao NXB nâng giá hơi cao so với lần trước, nhưng không nên vì thế mà bỏ qua cuốn sách này.
5
576569
2016-02-26 21:14:47
--------------------------
384704
5947
Mật mã là lĩnh vực mà mình yêu thích từ bấy lâu nay, nên tất cả những tác phẩm liên quan đến mật mã mình đều cố gắng tìm đọc và mua cho bằng được về chưng ở tủ sách. Những cuốn sách như mật mã davinci hay pháo đài số mình đều đã mua và đọc qua nên khi thấy quyển sách này ở nhà sách mình đã muôn mua nó từ lâu nhưng thật sự là giá sách quá đắt. Nhưng mà cũng bù lại là chất lượng sách rất tuyệt. Tiki giao hàng rất nhanh mình vừa đặt chiều hôm kia mà hôm nay đã nhận được sách rồi. Tạm cho 4 sao và mong chờ rất nhiều ở nội dung của sách.
4
132597
2016-02-22 21:06:46
--------------------------
377982
5947
Một cuốn sách nói về mật mã rất hay và bổ ích. Khi đọc cuốn sách này, chúng ta như bị cuốn vào từng chi tiết qua những câu chuyện cả hết sức sinh động của tác giả. Trí tò mò đã thôi thúc người đọc ngay từ trang bìa sách và quả thật tôi đã không thất vọng khi mua quyển sách này. Trên quan điểm đó, tác giả đã biết cách làm hài lòng các tín đồ đam mê mật mã học, khai thác triệt để đến từng ngõ ngách của quá trình khai sáng mật mã, mở ra một chân trời mới vô cùng khó khăn và cũng hết sức thú vị, lôi cuốn đối với mật mã.
5
733791
2016-02-03 22:53:30
--------------------------
366681
5947
Là người có niềm yêu thích và tìm thấy thú vị trong những con số, những mật mã ưởng chừng như là khô khan thì ngay khi biết tới cuốn sách này tôi đã hết sức chú ý, cuốn sách không chỉ cung cấp cho độc giả một cái nhìn tổng quan về thế giới mật mã, từ những điều đơn giản đến những điều phức tạp nhất. Từ những mật mã của buổi đầu khai sinh cho tới những mật mã hiện đại ngày nay có sự giúp đỡ của máy tính và siêu máy tính để giải mã. Sách được biên soạn kỹ và chất lượng giấy in khá tốt. Tiki giao hàng nhanh và cẩn thận.
4
730145
2016-01-10 12:55:49
--------------------------
361879
5947
Cuốn sách này đã thu hút tôi ngay từ cái tên của nó, "Mật mã" những thứ mà khiến cho con người ta rất tò mò. Tôi tò mò từ cái cách từng mật mã được viết đến cái cách để giải mã, những thứ tưởng chừng như rất khó này lại có thể viết ra rất dễ hiểu bỏi tác giả Simon Singh. Bạn sẽ tìm thấy ở đây những cách mà người xưa đã giao tiếp bí  mật như thế nào. Bên cạnh sự phát triển của mật mã, thì sách cũng để cập đến sự phát triển của phá mã. Đây giống như là một cuộc chạy không điểm dừng, tất cả đều phụ thuộc vào cái đầu của mỗi người
4
754601
2015-12-31 20:20:29
--------------------------
317889
5947
Thầy giáo Vật Lí của tôi đã giới thiệu cho tôi quyển sách này. Là sách về khoa học nhưng không hề khô cứng và lí thuyết chút nào. Khoa học kết hợp với các câu truyện lịch sử là sự kết hợp tuyệt vời. Những câu truyện về cuộc chiến khốc liệt của phe giải mã và phe tạo mã đã được tác giả mang ra cho người đọc qua góc nhìn của cả 2 phe. Ngoài những lời giải thích chuyên sâu về mật mã, còn có cả những câu chuyện đời thường và nhân văn, cụ thể là con người thiên tài Alan Turing đã có một cuộc đời khó khăn chỉ vì căn bệnh đồng tính thời đó chưa được xã hội công nhận, rồi cả một nhà giải mã với căn bệnh lâu năm làm ông luôn ngất xỉu mỗi khi ngạc nhiên, cả vui lẫn buồn. Cuốn sách còn cho tôi thấy công dụng mấu chốt của mật mã trong các cuộc chiến tranh thế giới. Bìa đẹp, giấy tốt, chỉ có một khuyết điểm nho nhỏ là hơi đắt. Chúc các cuốn sách này được nhiều người yêu quý.
5
818882
2015-10-04 15:56:08
--------------------------
301817
5947
Tôi tình cờ được biết quyển sách này qua một người đồng nghiệp cấp trên giới thiệu. Tôi bị thôi thúc bởi việc cầm trên tay quyển sách này qua những mẫu chuyện hấp dẫn và có chút dang dỡ của đồng nghiệp tôi. Khi cầm trên tay được quyển sách, đọc xong quyển sách này. Tác giả đã đưa chúng ta từ những câu chuyện hấp dẫn này đến những câu chuyện hấp dẫn khác theo dòng lịch sử của nhân loại về mật mã học. Quyển sách hướng đến những độc giả mong muốn tìm hiểu khoa học, nên tác giả đã giải thích một cách rất 'bình dân' các quy trình mã hóa dọc theo chiều dài lịch sử phát triển của nó. Đọc xong quyển sách này chúng ta như mở rộng ra về 1 lĩnh vực cho riêng mình. Quyển sách này rất đáng được đọc cho những ai đang sử dụng Internet ngày nay.
5
755291
2015-09-14 21:01:29
--------------------------
294736
5947
Cảm giác ban đầu của tôi khi thấy đề tựa: "Mật Mã - Từ Cổ Điển Đến Lượng Tử" hẳn 99% là dính tới những thuật giải phức tạp (cá với bạn là ngay cả người chuyên môn cũng không muốn gặp lại chúng) nhưng suy nghĩ ấy quay ngoắt 180 độ khi tôi nghiền ngẫm tới con chữ cuối cùng của Simon. Cuộc chiến an toàn thông tin hiện nay vẫn sôi sục và nóng bỏng hơn bao giờ hết, và các bạn hãy cứ an tâm phần nào đi nhé - công lý và cái thiện luôn tất thắng.

Tác phẩm quả thực tạo cảm giác bồi hồi từ những dòng khơi mào, trích dẫn từ những thành tựu quá khứ cho đến gây cấn, hồi hộp khi chứng kiến tiềm lực kỷ nguyên số trong thời đại tân tiến hiện nay. Ấn tượng nhất là câu chuyện về Alan Turing - người mà bất cứ lập trình viên nào từ khi mò mẫm làm quen các ngôn ngữ lập trình như Pascal, C, C++... đều ngả mũ kính trọng trước cống hiến vô vàn của ông cho ngành công nghệ thông tin.

Nói sơ về các sự kiện trong cuộc đời của Alan Turing thì ấn tượng nhất và xúc động nhất chính là giây phút ông hoàn thành và vận hành thành công cỗ máy Enigma sau bao tháng trời dốc tâm mất ăn mất ngủ và đấu tranh thuyết phục Bộ quân sự. Và cũng chính từ giây phút ấy, lý trí và tình cảm của con người được cho là khô khan, tự kỷ, khó gần này đã làm rớt nước mắt bao người: Ông đã quyết định không thông báo sự việc tàu ngầm địch sẽ phóng thủy lôi vào tàu của bên ta vì lý do: nếu ta chặn được đợt công phá này thì quân địch sẽ biết ta đã giải được mật mã và bọn chúng sẽ chuyễn sang mật mã khác, lúc ấy, công trình hàng tháng trời và mọi công sức của nhóm Alan sẽ đổ sông đổ biển...

Quyết định vô cùng khó khăn giữa tình cảm và lý trí. Và bạn sẽ càng nể phục hơn nữa khi ông nén mọi đau khổ đau lòng để cho ra quyết định cuối cùng được cho là "vô cùng tàn nhẫn" - hy sinh người thân của mình, còn gì đau khổ hơn khi chỉ biết đứng lặng câm và nuốt nước mắt trong lòng.

Song vì thế đem lại chiến thắng đánh bại quân phát xít, và mở ra công cuộc chú trọng cho ngành "Mật mã học".

Bản tái bản này rất đẹp, nhẹ, dù giá hơi đắt nhưng âu cũng đáng đồng tiền. Đây là một cuốn sách rất đáng để mua về nghiền ngẫm đấy!

5
67967
2015-09-09 17:23:22
--------------------------
289788
5947
Đây là một quyển sách rất hay và hữu ích cho những bạn yêu thích mật mã. Quyển trình bày khá tổng quát các loại mật mã thông dụng từ cổ điển đến hiện đại, cách hành văn mạch lạc, dễ hiểu nên bạn không cần bạn giỏi toán hay lý để có thể hiểu được. Ngoài ra, nhiều đoạn tác giả còn viết theo kiểu kể chuyện nên cũng đem lại kha khá kiến thức, nhất là thời ký giữa các nhà giải mã và phá mã đối đầu kịch liệt trong 2 trận thế chiến. Tuy sách hơi mắc nhưng rất đáng tiền mua, mình chắc chắn quyển anyf sẽ không làm các bạn thất vọng đâu.
5
582958
2015-09-04 21:33:43
--------------------------
280699
5947
+ Thứ nhất thì mình cực kì bức xúc với nhà xuất bản trẻ, cực kì vì trời ơi sách này bị tăng giá lên quá chừng mà vẫn dùng bản dịch cũ chứ không có hiệu chỉnh lại gì hết nên mình khá bực vì tái bản lần 2 rồi mà vẫn còn lỗi in ấn , hay dịch thuật này nọ thì thật khó chấp nhận được với mức giá bị độn lên quá chừng như thế này.
+ Thứ hai : Sách hay khỏi bàn mặc dù đôi trang hơi thắc mắc hay khó hiểu nhưng mà bạn sẽ ko thất vọng đâu , quyển này như là một lược sử về mật mã từ cổ đại đến ngày nay,  và tác giả viết rất dễ hiểu nhưng đôi chỗ bạn cũng cần động não để hiểu nữa :) 
4
36336
2015-08-27 21:21:16
--------------------------
259598
5947
Quyển sách nói về những loại mật mã và nguồn gốc ra đời của nó. Từ câu chuyện về cái chết của nữ hoàng Mary xứ Scotland đến thế giới lượng tử kì bí. Quyển sách sẽ cung cấp cho bạn vốn tri thức về mật mã, đồng thời cho bạn kiến thức về lịch sử.  Sách còn hơi khó hiểu lúc đầu, nên phải nghiền ngẫm, đọc đi đọc lại. Bìa mềm nhưng vẫn đẹp, mang chút kì bí. Sách thiết kế đẹp, in chữ rõ ràng, giấy màu ngà theo kiểu vintage; sách dày 550 trang nên khá nặng. Dù hơi đắt nhưng kiến thức mang lại rất lớn.
4
388181
2015-08-09 17:23:31
--------------------------
230972
5947
Một cuốn sách khám phá khoa học, tri thức trong lĩnh vực công nghệ thông tin (cụ thể hơn là vấn đề Mã hoá). Mình là dân trong ngành, sách đã mở mạng cho mình những kiến thức mới mà mình chưa từng biết qua. Quyển sách đi từ cổ đến kim để mang lại cho người đọc tri thức toán diện về một ngành khoa học không mới nhưng cũng không cũ. 
Sách khoa học nhưng vẫn chứa đựng một cốt truyện đầy cuốn hút, làm mình không thể rời mắt khỏi những trang giấy.
Việc dịch sát nghĩa và thiết kế sách đẹp, nhẹ (dù nó khá dày) làm người đọc cảm thấy thoải mái trong việc mang đi lại.

5
334680
2015-07-17 20:45:38
--------------------------
223319
5947
Quyển sách được dịch sát nghĩa. Khi đọc bạn sẽ được tác giả khơi lên sự tò mò, sự dẫn dắt tài tình của tác giả đi từ góc cạnh xã hội, lịch sử. Quyển sách giống như thuật lại lịch sử đi kèm với sự phát triển mật mã từ cổ điển tới lượng tử. Sách rất thích hợp cho những ai mới đầu tìm hiểu về mật mã, sẽ hiểu thêm sự đấu tranh giữa hai phe tạo mã và giả mã. Tuy nhiên, tác giả chưa đi sâu vào mật mã, chỉ dừng lại ở mức cơ bản, tổng quát, giới thiệu cho nên sách chỉ thích hợp với các bạn mới bắt đầu với mật mã.
4
455000
2015-07-06 22:16:37
--------------------------
215318
5947
Quyển sách này rất tuyệt vời cho những ai hứng thú với mật mã và thông qua chúng để biết thêm về lịch sử, văn hoá của một đất nước, một dân tộc, một con người. Nội dung nói về lịch sử và cách sử dụng, cách giải một số loại mật mã giữa vô vàn loại mà con người sáng tạo ra. Sách rất dày, nên nhìn rất nản, ban đầu mình cũng vậy. Nhưng vì viết về từng loại mã, nên bạn có thể nhảy bất kì đâu và đọc bất cứ khi nào có hứng thú (nên vậy đi vì đọc hại não lắm). Dịch thuật có một số lỗi. Và chát nhất là giá, Trẻ mỗi lần tái bản không hiểu làm trò gì mà đội lên kinh khủng vậy.
5
374756
2015-06-26 10:21:32
--------------------------
213833
5947
Cuốn sách được dịch khá tốt. Nhưng vì bản chất của nó nên người ngoài cần đọc kĩ một chút. Tưởng tượng. Suy ngẫm.
Tuy nhiên, vẫn có một số lỗi in ấn nhất định. Như thiếu một hình minh họa, phụ lục E gì đấy phần mã viết sai một chữ cái, phần phụ lục có 2 lỗi lận. Bạn nào mua rồi sẽ thấy. Giải mật mã giải từng chữ, nên yêu cầu sự chính xác là đương nhiên.
Khoảng giữa cuốn sách có đề cập tới troyjan. Cuốn sách nói là "con ngựa thành Troy". Bạn nào thấy lạ thì chú ý.
Mong là lần tái bản sau sẽ tốt hơn.
4
513433
2015-06-24 10:23:57
--------------------------
211880
5947
Tác phẩm là một công trình khoa học tinh tế và chuyên sâu về mật mã và những bí ẩn xung quanh còn bỏ ngõ của nó. Xuyên suốt quyển sách. tác giả đã dẫn dắt người đọc đi từ nguồn gốc của mật mã, những phương pháp từ cũ đến mới trong việc con người tạo ra mật mã trong nhiều thời kỳ khác nhau của lịch sử. Các câu chuyện minh họa về mật mã rất thú vị và sống động, một số chương còn đưa ra một số "bài tập" áp dụng việc giải mã vừa được tác giả nhắc đến. Hấp dẫn và lý thú, đây là một quyển sách nên đọc
4
102118
2015-06-21 11:07:12
--------------------------
185263
5947
Một cuốn sách rất hay và kích thích trí tò mò của người đọc. Những ví dụ dễ hiểu lôi cuốn người đọc từng bước 1 tới những cái phức tạp.
Mình thích đoạn đầu bởi những ý tưởng của người xưa về việc giấu thư mà bây giờ hầu như không thể tưởng tượng được, như biết lên đầu rồi chờ tóc mọc ^^!
đoạn cuối lôi cuốn mình bởi mình cũng làm công nghệ thông tin và yêu thích vật lý, hiểu được sự hình thành và ra đời của RSA mà bây giờ ứng dụng rất rất nhiều trên internet.
Mình đã tìm thêm sách của tác giả Simon Singh nhưng tiki toàn báo hết hàng :D
5
441514
2015-04-19 09:45:24
--------------------------
174455
5947
Đây là một cuốn sách khoa học khá hiếm hoi mà lôi cuốn mình muốn đọc ngay từ đầu đến cuối, khó có thể dứt ra được!
Tác giả mô tả quá trình phát triển của mật mã theo thứ tự thời gian, từ đơn giản đến phức tạp, và đan xen với đó là các sự kiện lịch sử có liên quan đến sự biến đổi của mật mã. Các ví dụ minh hoạ đưa ra được giải thích rõ ràng, rành mạch, chỉ cần chú ý một chút là bạn có thể hiểu được và vận dụng để giải các loại mã tương tự.
5
24396
2015-03-28 02:45:41
--------------------------
156909
5947
Qua cuốn sách của Simon Singh, một thế giới về mật mã đã được mở ra từ thời cổ đại cho đến hiện tại. Từ xa xưa, khi mà chữ viết xấu hiện cũng là lúc mật mã được phát minh ra. Qua từng trang sách, ta được nghe những câu chuyện thú vị về mật mã, từ những phương pháp giấu thư nổi tiếng như dùng thắt lưng, viết thư lên đầu,... cho đến kỹ thuật giấu thư của các điệp viên hiện đại bằng các cuốn vi phim siêu nhỏ. Điều làm tôi cảm thấy thích thú nhất chính là phần mật mã ở cuối cuốn sách thử thách khả năng hiểu biết của người đọc sau khi đọc xong cuốn sách bằng cách hóa giải những mật mã ở phía sau. Đây là một cuốn sách hay dành cho những ai đam mê tìm hiểu về khoa học mật mã, môn khoa học tưởng chừng như phức tạp nhưng cũng hết sức lý thú.
5
291067
2015-02-06 21:31:21
--------------------------
138001
5947
Cuốn sách The Code Book được Simon Singh trình bày rất dễ hiểu, bạn không cần phải lo cho dù bạn không thật sự giỏi về toán học, vì mỗi khi gặp một khái niệm toán học có vẻ lạ lẫm thì Simon sẽ dừng lại vài dòng, thậm chí cả trang sách, để giải thích nó rồi mới đi tiếp. Do đó chỉ cần bạn biết làm bốn phép tính cộng-trừ-nhân-chia cùng với một sự tò mò về thế giới mật mã là đủ để đọc cuốn sách.Hơn nữa về ý kiến của mình thì các bản in sau khá tốt hơn bản trước điểm cộng nữa sách khá dày , nó như cuốn từ điển ấy những loại giấy được in thì nhẹ nên cầm được tay tuy vậy điểm trừ việc độn giá lên gần gấp đôi so với giá đầu thì điều nãy cũng gây phản cảm ko tốt đến nxb .
5
197530
2014-11-30 10:52:55
--------------------------
