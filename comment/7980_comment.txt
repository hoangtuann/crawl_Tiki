267814
7980
Tôi từng mua rất nhiều sách Bé Tìm Hiểu cho em bé nhà nhưng trước giờ chưa thấy loại sách nào thực sự tốt về mặt nội dung và hình thức cả. Đa số chỉ chú ý vào mẫu mã và tính đa dạng nhưng không hề chú ý đến tâm lí trẻ em, chỉ toàn đưa vào nhưng nội dung khó hiểu và gây khó khăn cho cả người lớn. Nhưng dòng sách Bé Khám Phá Môi Trường Xung Quanh này lại hoàn toàn khác. Không chỉ mẫu mã đẹp, chất lượng sách vừa cứng vừa in rõ nét phù hợp với lứa tuổi mầm non hiếu động mà cả về nội dung cũng rất phù hợp và cực kì dễ học cho các bé. Tôi hài lòng với sản phẩm này.
4
484225
2015-08-15 21:17:34
--------------------------
