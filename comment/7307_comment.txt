395516
7307
Cuốn sách Cải Cách Giáo Dục Nhật Bản do Tác giả Ozaki Mugen viết khá thú vị, cung  cấp những nội dung có liên quan đến nền giáo dục của nhật bản. sách gồm 8 phần chính, là những sự xuất phát của giáo dục thời hiện đại, của chế độ thiên hoàng, thời taishou, thời kì động loạn, hay sau chiến tranh. mỗi phần đều có những nội dung nhỏ chi tiết về thời diểm đó. sách khá dày hơn 300 trang. thông qua sách có thể hiểu đc biến động của nên giáo dục. và thật sự khâm phục đất nước nhật bản, một đất nước nghèo nàn về tài nguyên nhưng lại sức mạnh thì rất vô biên bởi họ đã vận dụng được sự đoàn kết từ nguồn tài nguyên sẵn có chính là con người.
4
854660
2016-03-11 23:25:36
--------------------------
317576
7307
Một quyển sách kể về các hành trình hình thành phát triển giáo dục qua các giai đoạn, nhờ Tây phương hóa, sự phối hợp nhịp nhàng giữa các kỹ nghê, tự chủ trong giáo dục, chủ nghĩa quốc gia và chủ nghĩa quốc tế. Tập trung vào tiểu học và đại học. Đây nói cơ bản là sách lịch sử, đọc tuy hơi khó nhằn vì nhiều thuật ngữ lạ, xét về nội dung thật sự thần kỳ. Các chính sách cải cách cũng bị nhiều người dân sợ phải thay đổi phản đối, rồi các thế lực tận dụng cơ hội chia lẽ, giành quyền. Nhưng cuối cùng họ cũng thích nghi được, thay đổi rồi sẽ lớn mạnh.
4
419763
2015-10-03 20:01:38
--------------------------
302064
7307
Khi được biết rằng Hàn Quốc đã bắt chước giáo dục của Nhật Bản để phát triển, tôi đã tìm ngay đến những cuốn sách thế này. Hãy nhìn Hàn Quốc là hiểu họ đã đúng đắn như thế nào khi học tập theo Nhật Bản. Từ một nước nghèo đói phải nhận viện trợ IMF, Hàn Quốc đã từ nước "đi xin việc" thành nước "đi cho việc làm". Cuốn sách này với tôi là một cuốn sách về lịch sử cải cách của Nhật Bản đầy đủ nhất. Nó cho cái nhìn tổng quát hơn về nền giáo dục này. Và rồi ta nhận ra rằng, nền giáo dục của Nhật Bản đã có thời y chang như Việt Nam với những vấn đề nhức nhối hiện nay. Nhưng họ đã thành công, tại sao chúng ta lại không được ?
5
496319
2015-09-14 22:59:20
--------------------------
283631
7307
Làm thế nào để một Nhật Bản nhỏ bé lại thành một cường quốc, gây sóng gió cho cả châu Á trong Chiến tranh Thế giới thứ hai? Và làm thế nào để nước Nhật điêu tàn có thể trỗi dậy lần nữa sau khi bị chiến tranh tàn phá, làm được điều mà cả thế giới gọi là "bước nhảy thần kì"? Một trong những nhân tố chính phải kể đến cải cách giáo dục, mà mở màn là cuộc Minh Trị Duy Tân. Giáo dục Nhật Bản dù trong thời kỳ nào cũng đặt nặng tính cộng đồng, yêu cầu mỗi cá nhân phải cống hiến hết mình cho tập thể, và rộng hơn là cho đất nước. Tất nhiên, điều này có cả mặt lợi lẫn hại, và phần nào hơi cực đoan, nhưng không thể phủ nhận nó đã thay đổi đất nước Nhật Bản. Sách viết khá hay, tiếc là bìa sách và phần trình bày hơi đơn điệu, kém hấp dẫn.
4
57459
2015-08-30 09:16:18
--------------------------
224519
7307
Sự thay đổi trong nền giáo dục của Nhật Bản chính là nguyên nhân chính để có được một Nhật Bản như ngày hôm nay.Sách đi từ công cuộc cải cách giáo dục từ thời Minh Trị cho đến cuối thế kỉ 20 nhưng điều làm mình ấn tượng nhất đó là trong những năm đau thương của cuộc chiến tranh thế giới lần thứ hai,dù có mất mát đau thương nhưng giáo dục vẫn là quốc sách hàng đầu của người Nhật.Gạt qua nỗi đau nước mất nhà tan,người Nhật vẫn tin vào tương lai thông qua giáo dục và không chỉ vậy họ càng cố đẩy mạnh hơn cuộc cải cách để đưa đất nước sớm thoát khỏi thương đau.
Sẽ là 1 lời giải đáp đầy đủ cho những ai còn thắc mắc về sự thành công của Nhật Bản ngày hôm nay.Với chất lượng giấy khá tốt và chữ in rất rõ ràng,quyển sách thật sự rất hữu ích với những ai yêu thích quốc gia này.
4
368991
2015-07-08 20:36:50
--------------------------
139008
7307
Đây là một cuốn sách khá hữu ích cho những bạn có câu hỏi trong đầu "Tại sao Nhật Bản lại trở thành một cường quốc mà chiến tranh đã tàn phá họ ?" Đó là liều thuốc văn minh mà Fukuzawa Yukichi đã mang lại, trong đó có giáo dục.
Giáo dục đã làm thay đổi Nhật Bản. Tác giả đã đề cập tới những vấn đề cấp bách của thời đại lúc đó, số liệu chính xác, và cả những quá trình cải cách lâu dài. Về hình thức, cuốn sách khá đẹp, bìa khá tốt, trình bày ở trong bắt mắt nhưng hơi lộn xộn.
4
298441
2014-12-05 19:55:53
--------------------------
112373
7307
Quyển sách này mô tả về cải cách giáo dục ở Nhật bắt đầu từ thời Minh Trị, sau đó trải qua một số lần cải cách mang tính dấu mốc như sau chiến tranh thế giới lần thứ 2, những năm 80 của thế kỷ XX. Nhờ cải cách mà Nhật Bản thoát khỏi phương tây xâm lược, trở thành nền kinh tế lớn trên thế giới.

Nhật Bản những năm 80 của thế kỷ XX chuộng bằng cấp, chạy theo điểm số, học thêm, luyện thi ... như nền giáo dục Việt Nam hôm nay.

Việt Nam hô hào giáo dục là quốc sách. Thực tế người người, nhà nhà đều cố gắng đầu tư cho giáo dục, tuy nhiên vẫn là vòng lẩn quẩn không lối thoát. Chỉ khi chính phủ mạnh tay thì may ra!
3
129812
2014-05-13 14:21:15
--------------------------
