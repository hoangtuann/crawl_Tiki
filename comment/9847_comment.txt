460474
9847
Mình đã yêu Thi Định Nhu ngay khi đọc Gặp gỡ Vương Lịch Xuyên, phải nói là mình cực kết thể loại này. Nó khiến mình day dứt, cảm thương vì sự ngược tâm của hai người yêu nhau; buồn vì những khúc mắc, chia lìa của họ; và khâm phục họ vì họ thực sự đã tìm lại nhau sau những chông gai của cuộc sống. Nghị lực yêu cũng là một loại cố gắng, sự cố gắng trong khó khăn về cơ thể và day dứt về lương tâm. Nhưng tình yêu đâu có lỗi, họ có thể buông bỏ tát cả nhưng vẫn cố nắm chặt lấy đôi tay của nhau. Được như vậy là mãn nguyện rồi
5
636210
2016-06-27 04:59:48
--------------------------
460301
9847
Tôi rất thích các tác phẩm của Thi Định Nhu, ở cô có 1 điều mà không tác giả nào có được như điều ta có bao giờ nghĩ thì nó sẽ là sự thật hay như lối viết tràn ngập hội thoại mới mẻ, thú vị của cô. Nếu nói Vương Lịch Xuyên thảm thì Mộ Dung Vô Phong còn thảm hơn: như 1 chiếc lông vũ, mong manh đến đáng sợ.Tôi ấn tượng với Sở Hà Y, dù tác giả không trực tiếp nói thì chúng ta cũng hiểu nàng chính là đệ nhất kiếm khách, điều này khiến tôi cảm thấy rất mãn lòng. 
Đôi nhân vật chính tình cảm rất sâu nặng, tan hợp nhiều lần như vậy thì càng khẳng định được tình cảm của họ, họ vẫn luôn khiến cho người đọc cảm thấy thú vị, mới lạ, mặn nồng như những ngày đầu. 
Thi Định Nhu viết chuyện rất thật, điều ta không ngờ tới thì nó lại là thật như việc Mộ Dung Vô Phong đã tàn tật lại còn mất 1 chân vô cùng thảm, cốt truyện rất mượt mà mà rẽ sang 1 hướng khác, trong một bối cảnh khác, ở 1 vị thế khác. 
Dù chuyện hay là như vậy đấy nhưng đôi khi tôi cảm thấy thổ huyết vì tác giả. Ví dụ như chuyện có "làm" hay "không làm" giữa cặp chính để rồi tôi ngã ngửa khi biết Hà Y có con, đó không chỉ là 1,2 lần thôi đâu.
Nói chung đây là 1 tác phẩm hay rất đáng đọc.
5
305705
2016-06-26 22:19:11
--------------------------
452038
9847
Nếu xét cả bộ Tam mê hệ liệt.có lẽ chỉ đọc được phần truyện của cặp đôi phụ. Văn phong của tác giả không tôi nhưng quá dông dài gây nên nhiều chỗ hơi nhàm chán
Kết thúc  truyện có hậu và cảm động nhưng t không đánh giá cao tác phẩm này
Có lẽ đây là motif ngôn tình cổ đại kiếm hiệp mấy năm về trước
Lần đầu đọc truyện của Thi Định Nhu, không biết các truyện khác của chị có hay hơn không, nhưng thực sự đối với cá nhân tôi cảm thấy không hợp bộ này
2
596518
2016-06-20 13:13:19
--------------------------
416678
9847
Truyện của Thi Định Nhu Luôn viết theo văn phong bi nhưng cuối cùng vẫn là HE. Sau khi mình đã đọc quyển Gặp gỡ vương lịch xuyên thì mình quyết định mua tiếp quyển Mê Hiệp ký. Về bìa sách thì khá ổn, thiết kế gam màu nhu nhìn nhẹ nhàng. Về giọng văn thì giọng văn nhẹ nhàng sâu lắng, đôi lúc buồn thương nhưng cực kỳ ngọt ngào. Về nhân vật thì không thể không yêu nam chính. Tác giả xây dựng nam chính có một cái thu hút riêng mà làm đọc giả không thể nào ghét được. Về nội dung thì Có bi, có ngọt. Cách hai nhân vật đến với nhau, tự nhiên cứ như suối gặp sông là sẽ hòa vào, mây gặp gió thì sẽ mưa vậy. Dù chỉ gặp nhau 3 ngày, nhưng hai người cứ như đã ăn ở với nhau mấy đời mấy kiếp. Tình yêu giữa họ mới nhìn dường như rất nhạt, nhưng lại sâu không gì sánh bằng. Không hề trao nhau bất kỳ lời nói yêu thương nào, nhưng cứ như vốn dĩ họ gặp nhau và yêu nhau là điều đương nhiên vậy, mối quan hệ phát triển cực kỳ nhanh nhưng không một chút gượng ép, hay bất hợp lý. Nói chung đây là tác phẩm hay của thi định nhu
5
47916
2016-04-15 15:24:17
--------------------------
372669
9847
Mê Hiệp Ký là tác phẩm đầu tiên của Thi Định Nhu mà mình đọc qua, vì được bạn giới thiệu và khá hứng thú với thể loại nam chính bị tật nguyền. Nhìn chung, cốt truyện không quá hấp dẫn, thể loại cũng không quá mới lạ nhưng đọc cũng khá ổn. Về cơ bản, tâm lý nhân vật diễn biến khá hợp lý, có ngược tâm chút nhưng không phải do nhân vật tự hành hạ lẫn nhau, ok, tiếc là lúc yêu nhau thì nhanh quá nên có chút khó hiểu. Tính cách nam chính trong mắt mình khá là thú vị và có chút dễ thương, nữ chính mạnh mẽ giỏi giang, đọc khá được 
4
630600
2016-01-22 01:34:38
--------------------------
360354
9847
Tạo ra cái nhìn mê hoặc , đầy sự hấp dẫn tới người xem độc đáo , kiến tạo lên nhiều điều có ý nghĩa mới cho người xem thích thú , cảm giác thư giãn với cốt truyện nhẹ nhàng , dễ đoán kết thúc cuối cùng nên nhớ nhiều chi tiết vui vẻ , bất ngờ ở đời thường , nắm bắt được tâm lý của nhân vật nhanh , tạo ra nối mạch cảm xúc rất thuận lợi , mang theo điều cần nhớ giúp nhận ra điều cốt lõi của con người , cá tính hiện thực xã hội làm nên  .
3
402468
2015-12-29 01:36:35
--------------------------
303705
9847
Tập 1 nối với tập 2 rất liền mạch, nhưng lại không làm người đọc giật mình nếu nhỡ có để 1 thời gian lâu mới đọc lại. Nội dung vẫn thế, vẫn làm mình ôm sách lên là si mê đọc không bỏ xuống được.
Sau khi đọc review của nhiều bạn và từ trải nghiệm của bản thân mình quyết định sẽ không mua Mê Thần Ký và Mê Hành Ký nữa. Đơn giản là vì mình chỉ muốn câu chuyện kết thúc ở đây thôi, cũng chỉ muốn theo dõi chuyện đời chuyện tình của cặp Vô Phong - Hà Y thôi không hơn.
Rất chi là cuốn hút, mọi cảm xúc đều rõ ràng cao trào. Những màn tranh đấu không miêu tả quá dài nhưng rất máu me, cảnh tình tứ thì tình nồng ý đậm. Mình thích kiểu truyện này.
Tội nghiệp cô gái nhỏ, nước mắt đã phải rơi bao nhiêu trong tập này cơ chứ. Ở 1 số cảnh chia ly, ví dụ như gần cuối truyện, mình không thấy cảm xúc của Hà Y được diễn tả nhiều, chỉ nói về cảm nhận của Vô Phong là chính thôi. Nhưng cũng có thể hiểu được, vì đọc rồi mới biết chị ý có thể luôn thầm lặng có mặt trong cuộc sống của Vô Phong. Vậy nên không hề đáng chê.
Khổ thân chàng này lắm, trong tập này đã phải chịu biết bao nhiêu thống khổ đây... Thực sự anh ý rất đáng thương, mặc dù nghĩ về nhân vật này mình vẫn thấy có 1 khí thế áp bức, làm người ta không thể dùng lòng thương hại mà coi nhẹ được. 1 sao mình trừ đi có lẽ là vì tác giả đã có phần quá đáng với Vô Phong. Mình không thấy đáng trách đến thế nhưng chăng hiểu sao vẫn thấy muốn cho 5 sao mà k cho được.
Kết ở đây là quá ổn thỏa mỹ mãn rồi. Ngắn hơn thì sẽ thấy hụt hẫng lắm, nhưng nếu tác giả có ý viết tiếp thì kể cả có 1 số điểm mình hơi tham, thấy chưa thoả nhưng phần này không thể viết dài hơn được cũng là dễ hiểu. Cảnh ở đoạn kết cũng rất viên mãn, xoa dịu được bao nhiêu là vết thương lòng của mình ở mấy chương trước.
Truyện này từ tập 1 sang tập 2 đều giữ vững được phong độ, chỉ có lên chứ không có xuống, mình rất khoái.
4
198644
2015-09-15 22:47:40
--------------------------
302708
9847
Tập hai của Mê hiệp ký còn đặc sắc hơn cả tập một, nếu như trong tập đầu chủ yếu nói về tình yêu của Sở Hà Y và Mộ Dung Vô Phong, thì tập hai tập trung vào những khó khăn nguy hiểm mà họ cùng vượt qua, họ sẵn sàng đánh đổi tất cả để được ở bên nhau, mặc cho bản thân có đau khổ như thế nào. Tình tiết trong truyện thật sự rất thú vị, đôi khi còn pha lẫn chút hài hước. Cũng may là cuối cùng kết thúc truyện cũng là happy ending, đây thật sự là một tác phẩm xuất sắc của Thi Định Nhu.
5
115397
2015-09-15 13:38:25
--------------------------
300909
9847
Ai nói Hà Y không hạnh phúc, ai nói nàng không được tự do, sự tự do của nàng chính là tự do yêu Mộ Dung Vô Phong, tự do ở bên chàng. Nàng hạnh phúc khi được sống cùng, chết chung với người nàng yêu, một cách cam tâm tình nguyện, thậm chí là đánh đổi tất cả chỉ để có được vòng ôm của người ấy, ai nói nàng không hạnh phúc. Hai người đứng giữa ranh giới quỷ môn quan, nàng không ngần ngại mà cùng chàng lao xuống vực thẳm, trong tình cảnh hai người đều sắp chết nơi, nàng còn thừa hơi giận dỗi khi nữ nhân khác có khả năng từng là người yêu cũ cuat chàng, thế là quay ngoắt mặt không thèm nhìn. Cái chuyện tấm màn thêu của Hà Y, quả thật là buồn cười, Mộ Dung Vô Phong rõ ràng nhìn thế nào cũng là bầy gián, thế mà lại mở miệng khen, bầy bướm sống động, khiến nàng cười đến không nhặt được mồm. Những đoạn ngọt ngào thì ôi thôi là ngọt, thật yêu chết mất thôi.
5
95900
2015-09-14 12:22:47
--------------------------
291390
9847
Kết thúc ở tập 1mà không đọc tập 2 thì thật không chịu nổi. truyện quá lôi cuốn, dẫn dắt người đọc không thể dừng lại. Ở tập 2 dường như các tình tiết càng được tác giả trau chuốt, thêm gay cấn, và một chút rùng rợn, xót xa. Mộ Dung Vô Phong thật khiến người đọc phải đau lòng. Đọc mà cứ phải thấp thỏm lo âu việc hai nhân vật cuối cùng có đến được với nhau hay không vì tình tiết câu chuyện ngày càng khiến mình thấy xót. Cuối cùng cũng có thể thở phảo nhẹ nhõm khi một kết thúc viên mãn dù có chút tiếc nuối. Hy vọng Định Thị Nhu sẽ tiếp tục cho ra đời nhiều tác phẩm hay hơn nữa.
4
280592
2015-09-06 11:15:41
--------------------------
260639
9847
Đọc xong tập một, tôi càng thêm háo hức muốn biết kết quả mối tình vừa cay đắng vừa ngọt ngào của Vô Phong và Hà Y. Sang quyển hai, nội dung truyện càng được đẩy lên cao trào khi Hà Y xông vào Đường môn để cứu Vô Phong, hay khi bí mật về thân thế của chàng dần hé mở. Cách xây dựng tuyến nhân vật đa dạng, truyện dài nhưng không nhàm chán mà luôn có những tình tiết nối tiếp như sóng sau xô sóng trước, làm người đọc không thể rời mắt. Tôi rất hi vọng sách sẽ được chuyển thể thành phim vì đây quả thực là một tác phẩm tuyệt vời.
5
109583
2015-08-10 15:58:44
--------------------------
253549
9847
Tập hai của Mê hiệp ký ngọt ngào hơn tập một nhưng cũng đắng cay hơn tập. Cả hai nhân vật đã phải chịu nhiều khổ sở, khó khăn. Tôi đã phải rùng mình khi đọc đến khúc Sở Hà Y đến Đường Môn để cứu Mộ Dung Vô Phong. Người Đường Môn quá độc ác, quá thủ đoạn, người ta đã bị liệt rồi còn chặt chân nữa, thật đáng sợ. Đọc tới chỗ này tôi thấy Thi Định Nhu ngược nhân vật của mình ghê quá. Nhưng rồi họ cũng đến được với nhau. Happy Ending !!! Thế thôi. ( Cười)
5
461324
2015-08-04 15:36:03
--------------------------
239046
9847
Với mình thì Mê Hiệp Ký thật sự rất hay và đặc biệt . Cá nhân mình thì không thích gì truyện kiếm hiệp cho lắm , đến với Mê Hiệp Ký khi chưa xem qua giới thiệu , lúc đầu mình còn tưởng đây là một quyển ngôn tình nên lúc nhận được truyện đọc sơ qua mấy trang đầu mình khá buồn và thất vọng. Do tiếc tiền nên mình cố gắng đọc, có trải nghiệm dành thời gian đọc sách mình mới thấy hết được ưu điểm của cuốn sách này. Có 3 lý do làm mình cảm thấy Mê Hiệp Ký đáng tiền mua : Một là cách hành văn ,giọng văn của tác giả , tình tiết không quá nhanh không quá chậm logic . Hai là cách xây dựng nhân vật nam nữ chính , nam lạnh lùng nữ ngây thơ hợp nhau. Ba là kết thúc rạng rỡ , happy ending . Chúc các bạn lựa chọn được quyển sách phù hợp !
5
387355
2015-07-23 16:46:14
--------------------------
178305
9847
Tập 2 có nhiều chi tiết ngọt ngào hơn, nhưng cũng đi cùng với lắm đắng cay, cảnh tượng cũng hãi hùng hơn. Tôi đã phải rùng mình khi đọc tới đoạn Hà Y đến Đường Môn cứu Vô Phong, vì sự tàn nhẫn của người Đường Môn, vì những hình phạt ghê tởm, khủng khiếp mà Vô Phong phải gánh chịu. Nó đáng sợ tới mức tôi tự hỏi liệu Thi Định Nhu có phải là "mẹ ghẻ" như mẹ Phỉ hay Tân Di Ổ hay không mà ngược nhân vật của mình ghê quá. Cuối cùng, khi thấy họ hạnh phúc bên nhau, tôi mới có thể thở dài nhẹ nhõm. Tiếc là không có ngoại truyện nào về Ngô Phong – Mộ Dung Tuệ, cha mẹ của Mộ Dung Vô Phong, trong khi đây là một chuyện tình rất có tiềm năng khai thác.
5
57459
2015-04-04 20:21:47
--------------------------
167988
9847
Tập 2 của Mê Hiệp Ký khá mệt và nhiều cảnh ám ảnh, đầy biến thái khiến người hay đọc kinh dị như mình cũng thấy ghê tởm.
Ấn tượng nhất ở những đoạn về Đường Môn, một nhóm ác quỷ tàn độc, bọn chúng hành hạ nạn nhân của mình bằng nhiều cách không còn gì đáng ghê hơn. 
Đứng giữa địa ngục trần gian đáng sợ ấy, Sở Hà Y và Mộ Dung Vô Phong vẫn phải tranh đấu, vẫn phải đau khổ rất nhiều. Tuy vậy, đóa hoa tình yêu sau bão của họ lại nở rộ rất đẹp, thật may là kết thúc ngọt ngào ấm áp. Bản thân mình sau khi đã bị tác giả “hành” mệt thế này không có ý định sẽ đọc lại bộ này một lần nào nữa, tuy vậy nhưng phải công nhận rằng Mê Hiệp Ký khá hay và ấn tượng.
4
382812
2015-03-15 19:48:25
--------------------------
153058
9847
Mình thích câu chuyện tình yêu giữa Hà Y và Vô Phong. Một Hà Y với vẻ ngoài lạnh lùng và lang bạt giang hồ lại nảy sinh tình cảm sâu sắc với một Vô phong phải gắn chặt đời mình trên chiêc xe lăn. Có đôi chút cảm thương cho Vô Phong nhưng cũng có đôi chút khâm phục ý chí của anh. Tuy bị liệt nhưng anh không tự oán trách bản thân mình mà ngược lại anhn vẫn thấy yêu cuộc sống này và là một vị thần y lừng danh cứu giúp người dân!
Câu chuyện tình yêu giữa họ đến một cách tự nhiên và vô cùng đẹp đẽ. Có những phân đoạn làm mình cay khóe mắt khí Hà Y buộc chặt thân mình để nguyện mất cùng Vô Phong làm cảm xúc mình không thôi trực trào :'(
Tuy không phải là một cốt truyện mới lạ nhưng Mê Hiệp Ký vẫn tạo được cái hay đặc trưng cho riêng nó. Câu chuyện tình giữa Vô Phong và Hà Y chắc chắn sẽ in sâu vào tâm hồn người đọc!
Một tác phẩm mới lạ và thành công của Thi Định Nhu!
4
303773
2015-01-24 23:22:30
--------------------------
106104
9847
Mê Hiệp Ký là tác phẩm đầu tiên của Thi Định Như mà tôi đọc. Cốt truyện không hay nhưng lạ. tuy không hay nhưng rất đáng để bỏ thời gian ra đọc.
Tác giả xây dựng nội dung truyện rất đơn giản tình tiết thì tùy ý nhưng lại rất tự nhiên, tính cách nhân vật rất bình thường giản dị nhưng trong bình thường gần gũi đó lại rất quái đảng và hơi lập dị tý chút.
Tính cách của nhân vật cổ quái nên tình yêu của họ cũng rất quái khiến người đọc cũng hiểu không nổi luôn. Không hiểu vì lẽ gì mà Vô Phong lại yêu Sở Hà Y và vì sao Sở Hà Y không chút do dự liền đáp lại Vô Phong.
Nói chung tình tiết của truyện có nhiều chỗ phi lý khó hiểu giải thích không nổi nhưng do Thi Định Như rất biết cách đào hố để bẫy độc giả nên dù tình tiết đôi khi có phi lý cỡ nào thì đọc giả vẫn sẵn sàng nhắm mắt bỏ qua lắm lúc còn khen là bẫy này hay quá, độc quá và nguyện ý gãy cổ chết trong cái bẫy mà chị giăng ra.
3
180185
2014-02-14 01:20:25
--------------------------
105300
9847
Nhiều bạn nói nhân vật nam chính quá thảm, nhưng mình thấy rất là ổn rồi. Tuy bị liệt phải ngồi xe lăn, nhưng chàng vẫn chấp nhận số phận, không oán trách, luôn muốn tự dựa vào sức mình. Ngoài ra chàng còn là một thần y nổi tiếng trong thiên hạ, còn Hà Y là một kiếm khách đệ nhất, hai người một người lạnh lùng, một người ngây thơ, cá nhân mình cảm thấy rất hợp nhau.
Trở về truyện, nếu như tập 1 Vô Phong đã bị liệt, thì tập 2 chàng còn thê thảm hơn, bị cắt mất 1 chân, bị giam vào ngục tối, lại bị cơn bệnh hoành hành đến chết đi sống lại. May thay bên cạnh chàng luôn có Hà Y. Đặc biệt mình cảm động nhất đoạn Hà Y buộc chặt chàng với mình, đợi đến khi tim chàng ngừng đập sẽ cùng nhau rơi xuống núi, quả thật là một tình yêu tuyệt vời.
Tuy nhiên, truyện vẫn có một vài thiếu sót nhỏ nhưng rất nhức mắt. Nếu thay từ " lão bà "  thành từ " thê tử " , thay từ " lão công " thành từ " tướng công " thì sẽ hay hơn, sẽ mượt hơn và phù hợp hơn bởi hai nhân vật chính còn rất trẻ.
4
174349
2014-01-27 15:02:04
--------------------------
86086
9847
Nghĩ bản thân là gánh nặng cho người mình yêu, quyết tâm để cô ấy rời xa là một sự đau đớn khôn cùng. Sự rời đi của Hà Y như một cơn lốc, kéo đi mọi sức sống, kéo đi thế giới của anh. Giờ đây trong anh chỉ còn trống rỗng đến vô cùng. Nhưng những thử thách và khó khăn vẫn còn ở phía trước. Số phận không cho anh ngừng lại giữa đường mà phải tiếp tục với tấm thân tàn phế. Nếu không có Hà Y, không có ý chí mạnh mẽ thì có lẽ Vô Phong đã không còn hoặc nếu còn sống, anh chỉ là một cái xác không hồn mà thôi. Cuộc đời con người, mỗi một người đều có một mãnh ghép nhất định. Nếu ai tìm được mãnh ghép còn lại của đời mình và biết trân trọng yêu thương nó thì sẽ có được hạnh phúc. Vô Phong và Hà Y, họ đều là những người đã từng mất mát, khát khao hạnh phúc nên họ biết quý trọng những gì mình đang có và nắm giữ nó. Trân trọng, nâng niu cuộc sống từng ngày bên nhau.
5
63376
2013-07-09 09:17:21
--------------------------
80401
9847
Đọc qua vài truyện của Thi Định Nhu, tôi thấy Thi Định Nhu thích xây dựng hình tượng nam chính không hoàn hảo. Không tàn tật thì cũng ốm đau gì gì đó và Mê Hiệp Ký cũng không phải ngoại lê. Tôi rất thích Mộ Dung Vô Phong, tuy chân bị liệt nhưng rất tài năng, tính cách lạnh lùng, rất ghét bị người khác thương hại hay giúp đỡ, ở huynh ấy toát lên khí chất rất thu hút tôi. Còn với nữ chính Hà Y thì tôi lại càng thích hơn, cá tính, mạnh mẽ, kiên cường lại thẳng thắng, võ công lại giỏi nữa. Tình tiết truyện ban đầu khá cuốn hút nhưng càng về sau thì càng... khó nhai. Gần cuối tập 1, tác giả hành hạ nhân vật (cả độc giả nữa) đến đau lòng. Kết thúc tập 1 lại là Mộ Dung Vô Phong tự tử, làm sụp đổ hình tượng trong lòng tôi >.<. Sang đến tập 2 thì không có gì khá hơn. Có vài chỗ tình tiết ì ạch, nhất là đoạn nói về bệnh tình anh nam chính, đúng là hành xác người ta, đọc không chịu nổi luôn, cứ phải lật mấy trang sách thật nhanh. Cũng may là truyện có kết thúc tốt đẹp, xem như có thể an ủi được tôi. 
3
25683
2013-06-12 18:17:08
--------------------------
74444
9847
Lần đầu đọc môt tuýp tiểu thuyết cổ đại với nhân vật nam chính vô cùng yếu ớt, tôi thật sự không kỳ vọng lắm, vì dù sao thì với những kiểu truyện về giang hồ như thế này tôi vẫn thích những nam chính mạnh mẽ, uy nghi hơn. Dẫu vậy cuốn tiểu thuyết này đã không để tôi thất vọng, Hà Y và Vô Phong có một chuyện tình mà không hẳn bất kỳ ai cũng có thể "vô tư" mà sống, mà hưởng thụ một cách chân thành đến thế. Đoạn tình của họ hoàn toàn không quá duy mỹ, nhưng nó thật sự đẹp, ở đời có thể vì 1 ai đó mà yêu thương, mà không màng sống chết cũng không hẳn là điều gì quá ngu muội.
5
23726
2013-05-13 21:18:06
--------------------------
56728
9847
Khi nhìn thấy tình yêu của Hà Vy và Vô Phong, tôi chỉ có thể thốt lên rằng tình yêu đó quá tuyệt vời. Hai con người, hai tính cách, hai số phận với những suy nghĩ riêng. Một Vô Phong luôn mặc cảm tự ti vì căn bệnh của mình, luôn phải đối diện với chán nản, khổ sở vì bệnh tật. Và nếu như cuộc sống của anh là làm bạn với khoảng không gian càng nhỏ bao nhiêu càng tốt thì Hà Y lại là con người lang bạt, cô không quan tâm đến điều gì ngoài những chuyến đi. Thế nhưng, khi gặp nhau họ đã yêu nhau say đắm, như yêu nhau từ kiếp trước. Và rồi vì yêu, mỗi người trong họ đều cố gắng, cố gắng bỏ bớt cá tính riêng của mình, tự trói buộc mình với người kia để hòa hợp làm một. Tình yêu đó thực sự quá đẹp, quá cảm động. Thế nhưng, chàng trai lại mang trong mình những mặc cảm về thân phận, liệu điều gì sẽ đến, họ có cùng nhau vượt qua được hay không là điều làm tôi tò mò khi đọc hết tập một. Dù sao cũng mong tập 2 sẽ là một kết đẹp. Bởi họ đã cố gắng rất nhiều bên nhau rồi.
4
54418
2013-01-24 08:32:46
--------------------------
48297
9847
      Yêu! Chỉ vì một chữ "yêu" mà Hà Y từ bỏ cuộc sống tự do tự tại, từ pỏ con người sống vô ưu vô lo của mình trước đây mà theo và chăm lo cho Vô Phong từng li từng tí. "Yêu" là thứ ma thuật kì diệu gì màk khiến 2 con người gặp nhau, bên nhau, sống chết không rời? 
       Ai từng đọc cả 2 tập của MHK  đều cảm thương cho số phận MDVP cũng như tình yêu cùa HY và VP, tuy vậy nhưng dù cho tật nguyền, bệnh tật, yếu ớt, VP vẫn tìm đc người yêu mình, HY vẫn tìm đc người yêu nàng hơn cả bản thân nàng, tuy đó là cuộc hành trình gian nan, nhưng mấy ai trong đời tìm đc hạnh phúc như họ. 
        Từ nhỏ MDVP đã mặc cảm về thân thể tàn tật bệnh hoạn của mình, nhưng từ khi HY bước vào cuộc đời y thì tất cả đã thay đổi, y cười nhìu hơn, biết lo lắng, yêu thương,nâng niu cho một người con gái....Và chỉ ở bên HY, MDVP mới một lần cảm thấy yêu tấm thân tàn của chàng....Vì HY cũng yêu nó !!!
         MHK là một trải nghiệm tuyệt vời về câu chuyện tình yêu đầy trớ têu bất hạnh nhưg đan xen những nụ cười hạnh phúc của đôi vợ chồng trẻ,khi đã đọc thì k thể dừng lại vì tình tiết hấp dẫn, ngôn từ thức tế giản dị dễ hiểu...Qủa là một cuốn sách không thể bỏ qua.
5
18665
2012-11-30 11:32:51
--------------------------
