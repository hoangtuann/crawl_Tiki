317206
6304
Nhân vật nam chính được xây dựng theo phong cách lạnh lùng nhưng ẩn sâu trong đó là một trái tim mềm yếu luôn nhớ nhung về mối tình đẹp đẽ thời sinh viên. Nhân vật thì một lòng một dạ cho mối tình đầu. Tuy nội dung chưa thực sự cuốn hút nhưng đủ để cuốn hút. Tác giả cần đi sâu tâm lý nam chính để diễn giải hình tượng nhân vật hơn. Ngay từ đầu thì có thể đoán đây  là một kết thúc HE.
Có thể đây là một tình yêu đẹp mà bạn nữ nào cũng mong muốn có thể qyau lại đối với tình cũ còn thương !!!
4
401385
2015-10-02 20:56:14
--------------------------
169799
6304
Đã có một thời từng yêu sâu đậm. Tình không biết bắt đầu từ đâu nhưng lại vô cùng sâu đậm. Từ một bài luận văn rồi đến một tình yêu ngây thơ trong sáng, hết sức thuần khiết. Thế nhưng số phận trớ trêu đẩy Phương Nghiên và Giang Đào sang hai con đường khác. Chỉ vì những hiểu lầm năm xưa đã đem lại cho họ những năm tháng khổ đau. Thời thanh xuân đầy ngọt ngào với một tình yêu trong trẻo. Nhưng đi qua thanh xuân, con người trưởng thành hơn và thay đổi hơn. Giang Đào làm tổn thương Phương Nghiên nhưng cô vẫn chấp nhận, vì cô cho rằng ngày xưa cô đã nợ anh, giờ là lúc cô sẽ lảm mọi thứ để anh luôn được tốt nhất. Rốt cuộc là anh nợ cô hay cô nợ anh cũng không thể hiểu rõ. Duy chỉ có một điều từ trước đến sau tình chưa bao giờ thay đổi.
Thế đấy trong tình yêu không có ai đúng ai sai chỉ có ai rung động với ai. Qua bao nhiêu sóng gió, anh và cô trở về với nhau để hoàn thành nốt tình yêu dang dở thời thanh xuân, để viết tiếp cho những tình cảm khắc cốt ghi tâm. Một cốt truyện hay và ý nghĩa.
5
519052
2015-03-18 22:09:29
--------------------------
140942
6304
Số phận bắt chúng ta mất đi người ta yêu quý, nếu không, chúng ta sẽ mãi mãi không biết được người ấy quan trọng đến nhường nào.Người ta thường nói tình đầu đẹp nhưng không bền ,Khi còn trẻ tình yêu giữa Phương Nghiên và Giang Đào chưa thực sự sâu đạm tuy dũng cảm cùng nhau cao chạy xa bạy nhưng họ vẫn không thể nắm chặt tay nhau trước sức ép của gia đình . Tình huống họ gặp lại nhau cũng thật trớ trêu . Cô từ một tiểu thư giàu có phút chốc đã biến thành hai bàn tay trắng . Anh từ một chàng sinh viên không có gì nay đã trở thành giám đốc của một công ty . Hai người họ đều biết rằng gặp lại nhau là không có ích gì yêu nhau lại càng không nên . Nhưng lí trí đâu thể nào bảo trái tim làm theo ? Một câu chuyện hay đáng đọc
4
337423
2014-12-13 10:03:59
--------------------------
