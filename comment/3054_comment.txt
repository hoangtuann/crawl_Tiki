292674
3054
Trong suy nghĩ của tôi truyện Bubu khá đơn giản, chỉ thích hợp với bé từ 1 đến 4 tuổi, nhưng với loạt sách “Bé học theo chủ điểm cùng BuBu”, các tác giả đang giúp Bubu “lớn” dần lên, nội dung phong phú hơn, hấp dẫn hơn, truyền tải nhiều kiến thức hơn. Truyện “BuBu đi chợ Tết” không chỉ vẽ ra không khí của mùa xuân, của những ngày giáp Tết (bé nhà tôi bảo “Đọc truyện này con mong mau đến Tết quá”) mà còn mang đến nhiều kiến thức bổ ích: Tết Nguyên đán là gì? Các loại hoa phổ biến ngày Tết, loại hoa tượng trưng cho mỗi miền, các loại quả chưng ngày Tết có ý nghĩa gì, các loại thực phẩm ngày Tết (nếu tác giả viết thêm chi tiết mẹ mua bao về để lì xì ngày Tết, rồi giải thích cho BuBu hiểu lì xì mang ý nghĩa gì nữa thì hoàn hảo). Nói chung là sách thú vị, bổ ích, điểm 5/5 cho BuBu nhé. 
5
332766
2015-09-07 15:35:29
--------------------------
