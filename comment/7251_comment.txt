474528
7251
Lần đầu nhìn thấy sách thì thấy sách đẹp. Giấy đẹp. Bìa đẹp. Mùi sách thơm. Cảm thất không bõ tiền mình bỏ ra. Đây là tập truyện dài được mình yêu thích nhất trong series truyện dài doremon.
Lần này đang muốn mua lại cả bộ truyện dài để quay lại với tuổi thơ đây. Truyện ngắn thì đọc hơi nhanh hết. Truyện dài đọc cho tâm lí đỡ bị ngắt quãng. Mới đầu là mua truyện này cho đủ đơn. Nhưng mua xong quyết định mua cả bộ luôn. Lại mất vài tram nghìn rồi. Ahuhu. Nhưng tiền nào của nấy. Vì sách rất đẹp
5
1384702
2016-07-11 16:00:50
--------------------------
446393
7251
Cuộc phiêu lưu lần này nếu mình nhớ không lầm thì nó được sáng tạo nên từ một mẩu truyện ngắn trong bộ 45 tập truyện ngắn. Dù những tình tiết bí ẩn trong tập này chưa đủ độ chín nhưng đổi lại cuộc hành trình lênh đênh trên biển của cả nhóm làm mình rất thích, cách Nobita vượt lên nghịch cảnh rất đáng khâm phục, để lại nhiều suy ngẫm trong ta. Còn đoạn mình thích nhất trong truyện chính là đoạn cuối, chiếc túi thần kỳ đầy tự hào, kiêu hãnh của Doraemon bị biến thành đồ đựng rác!
5
1420970
2016-06-12 10:10:50
--------------------------
406894
7251
Doraemon là một trong những cuốn truyện mà mình hồi bé mình thích, nhất là các tuyển tập truyện dài. Bởi mình  thích những cuộc phiêu lưu đầy mạo hiểm đầy rẫy những thử thách của nhóm bạn nobita ,  nhưng lại được diễn đạt bằng những hình ảnh sinh động, ngộ ngĩnh và  tạo ra nhiều  tiếng cười cho đọc giả nhí... hồi ho n tiểu học mình mê truyện này nhất nên cứ rảnh ra là đọc, cô không cho thì cưa giấu trong chăn úp úp mở mở mà sskcj zong một cuốn là cũng đủ mỏi rồi.@
5
1158536
2016-03-29 09:12:55
--------------------------
405074
7251
Gần như tất cả những đứa trẻ đều từng mơ ước được phiêu lưu hoang đảo và tìm kiếm kho báu nhỉ? Nhóm bạn Nobita đã khiến bao nhiêu đứa từng một thời mơ làm "cướp biển Cariber" thèm nhỏ dãi khi bỗng dưng lọt vào một thế giới đầy hải tặc và kho báu ở thế kỷ 19. Ẩn sau những xáo trộn thời gian và không gian, sau những vụ mất tích bí ẩn là một âm mưu đầy tham lam, và nhóm bạn của chúng ta quyết ngăn chặn điều đó. Vẫn là những ý nghĩa giáo dục sâu sắc được gửi gắm, Doraemon một lần nữa lại mang bao nhiêu màu sắc và nụ cười đến các bạn đọc nhỏ tuổi của chúng ta.
4
968032
2016-03-26 01:05:00
--------------------------
396341
7251
"Doraemon" – là một bộ truyện tranh rất hấp dẫn và ly kỳ của tác giả người Nhật Bản Fujio Fujiko. Mỗi khi đọc "Doraemon", ký ức tuổi thơ lại ùa về trong mình. Mỗi khi đọc Doraemon, những cuộc phiêu lưu của nhóm bạn đó lại cuốn hút mình, một lần nữa. Qua những câu truyện, những cuộc phiêu lưu của nhóm bạn tri kỷ đó, chúng ta rút ra được rất nhiều bài học quý giá và bổ ích như: về lòng dũng, về tình yêu thương,... nhưng trên tất thảy, "Doraemon" dạy chúng ta một tình bạn cao đẹp không dễ có được nó. Đây là bộ truyện tranh mình thích nhất.
5
1142307
2016-03-13 11:55:37
--------------------------
382201
7251
Dường như sự vòi vĩnh của Nobita không bao giờ chấm dứt khi cậu thấy Suneo được gia đình cho đi nghỉ mát ngoài biển và tất nhiên cậu bị cho ra rìa, Như thường lệ, cậu lại vòi vĩnh Doraemon một chuyến đi biển mà không biết rằng nguy hiểm đang chờ phía trước - một cơn bão đã cuốn nhóm bạn trở về thời mà cướp biển còn lộng hành. Nhưng bằng sự dũng cảm, nhóm bạn đã đánh bại tên đầu sỏ - một tên tội phạm thời gian quay về quá khứ để lai tạo những con vật đã tuyệt chủng.

5
534442
2016-02-18 19:37:44
--------------------------
253225
7251
Đây là một cuốn truyện, có lẽ làm mình nhớ nhất trong loạt truyện dài Doraemon bởi vì đây là cuốn đầu tiên mình đọc trong bộ series ấy, mặc dù khi ấy mình còn rất nhỏ. Nội dung câu chuyện này rất hay, mình cảm thấy nó đặc biệt nhất trong bộ truyện. Mình rất thích hình ảnh chú cá heo, chú rất thông minh và cũng rất gần gũi. Ngoài ra, thiết kế bìa truyện cũng rất đẹp, nhìn rất bắt mắt. Ngoài cuốn "Chú khủng long của Nobiata" - cuốn truyện đầu tiên của loạt truyện dài thì cuốn "Nobita du hành đến biển phương nam" này là cuốn thứ hai mà mình thích nhất!
5
13723
2015-08-04 11:33:18
--------------------------
168262
7251
Kho báu là điều mà ai không muốn khám phá chứ, đối với cậu bạn nobita của chúng ta cũng vậy, cái đó chính là sự hồn nhiên trong cậu bé.
Cũng chính vì cái tính ưa khám phá của nobita, mà cả bọn nhỏ bị cuốn vào một cuộc phiêu lưu hết sức thú vị. Đáng yêu và trùng trùng những nguy cơ.
Nhưng chính lòng chính nghĩa và bảo vệ bạn bè, họ đã vượt qua.
Quen biết với những người bạn là những hải tặc thứ thiệt.
Trở về quá khứ, gặp những người trong quá khứ và kẻ thù tương lai, hỗn độn, nhưng cũng không khỏi khiến người đọc cười rộn rã.
Yêu quá đi.
5
482828
2015-03-16 11:46:37
--------------------------
