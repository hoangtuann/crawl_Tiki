465136
8016
Thạch Lam là nhà văn lãng mạn Việt Nam những năm 30-45 nên nhân vật trong sáng tác của ông được đi sâu miêu tả tâm lí với những cảm xúc tinh tế, có sức lay động thế giới nội tâm con người. Nếu các nhân vật trong sáng tác của các nhà văn hiện thực có sự đấu tranh nội tâm sâu sắc thì nhân vật của Thạch Lam trước một nghịch cảnh, nhân vật cảm thấy khó có thể vượt qua, không có khả năng để vượt qua cái ngưỡng ranh giới giữa hạnh phúc và khó khăn. Có lẽ vì lối viết đơn giản nên hiện tại những bạn trẻ thường không say mê tìm hiểu văn phong của Thạch Lam. Tuy nhiên những tác phẩm của ông vẫn mãi có sức vang trong nền văn học dân tộc  và là những mẫu truyện ngắn mà chúng ta nên tìm hiểu và khám phá. 
5
825484
2016-06-30 21:08:55
--------------------------
396440
8016
Văn phong của Thạch Lam có một cái gì đó vô cùng nhẹ nhàng và trong sáng, mình tìm đến cuốn sách qua Hai Đứa Trẻ, trong các tác phẩm mà mình đọc ở cuốn sách này, cá nhân mình rất thích Cô Hàng Xén ( Xin phép cho mình không để tên riêng của các  tác phẩm trong ngoặc kép nhé) , giống như có một lòng đồng cảm da diết với người phụ nữ này, tuổi thanh xuân dành hết để lo cho cha mẹ, đến tuổi gả chồng, còn phải lo cho chồng con, lấy chồng hệt như gánh nặng lắm, gánh nặng đốt cháy thanh xuân của người thiếu nữ...
5
1014806
2016-03-13 14:34:30
--------------------------
334088
8016
Thạch Lam có nhiều tác phẩm nổi tiếng, chạm đến lòng trắc ẩn của con người mặc dầu cuộc đời của ông rất ngắn! Với tôi, Hai đứa trẻ là tác phẩm chạm sâu nhất, để lại nhiều ấn tượng nhất. Tôi có cảm tưởng văn phong của ông không chau chuốt, không hoa hoè mà chân thực đến mức có cảm giác vô cùng tinh tế. Hai đứa trẻ chính là hồi ức và kỉ niệm của một thời thơ ấu của chính tác giả. Có lẽ do đó là những cảm xúc thật, những gì đã diễn ra đã được tác giả miêu tả tỉ mỉ đến chân thực. Ánh sáng và bóng tối trong truyện như là một thủ pháp nghệ thuật dựng truyện của ông. Nói tóm lại, nó hay đến nỗi khi đọc, ta thấy như có ta của một thời ấu thơ trong đấy, như chính ta đang trải nghiệm cuộc sống của cái làng nghèo với tiếng còi tàu mỗi đêm, với cái không khí buồn hiu hắt, chầm chậm, đơn điệu của cuộc sống ngày ngày ở nơi đây.

5
724772
2015-11-08 20:27:28
--------------------------
298406
8016
Đến với các tác phẩm của Thạch Lam tức là bạn đã đặt chân vào thế giới của những ước mơ, niềm hi vọng và cuộc sống giản dị của con người. Chỉ bằng những lời văn vô cùng giản dị nhưng chứa đầy sự chân thành, tình yêu của tác giả, các tác phẩm của ông đều để lại trong lòng người đọc 1 dư vị khó quên! Truyện ông viết thường không có cốt truyện, không có sự lôi cuốn, gay cấn như tác phẩm khác nhưng ở đó vẫn chứa 1 nét riêng của Thạch Lam. cCuốn sách  tổng hợp đầy dủ các truyện của ông  nên đọc rất thích, bìa làm màu tím tạo cảm giác nhẹ nhàng, thanh thản, chất lượng giấy in cũng đẹp, tôi rất thích!
4
478050
2015-09-12 18:07:25
--------------------------
287408
8016
Một cuốn sách bổ ích cho thế hệ trẻ. Các tác phẩm của ông động lại trong lòng đọc giả ôợt cảm giác khó quên. Dù vậy thiết kế sách không đẹp tinh tế như tác phẩm làm mất đi vẻ vốn có của nó. Mình không thích nhất là màu giấy, giấy ngà thì đọc đỡ mỏi mắt hơn là dùng giấy trắng. Lúc đầu mình không biết nên mua 5 cuốn cùng nhà xuất bản như vậy mong sớm được cải thiện để cuốn sách có giá trị vê hình thức lẫn nội dung của nó đáng có.
4
686606
2015-09-02 18:18:47
--------------------------
261974
8016
mình theo học khối D nên mình rất cần những cuốn sách tham khảo như thế này. "Thạch lam - tác phẩm và lời bình" quả là tài liệu bộ tài liệu bổ ích. Cuốn sách đã đưa ra cho mình những tác phẩm đặc sắc của ông và cùng với đó là những lời bình xuất sắc. Điều này giúp cho mình vừa được thưởng thức mà còn được cảm thụ tác phẩm sâu hơn, độc đáo hơn. Ngoài nội dung chất lượng, mình còn đánh giá cao cuốn sách này ở chỗ chất lượng giấy tốt, font chữ rõ ràng, dễ nhìn. nói chung đây là một tài liệu học văn tuyệt vời!
4
306288
2015-08-11 15:49:56
--------------------------
250790
8016
Trên một bàn tay năm ngón, nếu ta kể tên những nhà văn có đóng góp lớn nhất với sự chuyển biễn trong nhận thức và thị hiếu, ta có dùng ngón út để biểu trưng cho Thach Lam. Văn chương ông uyển chuyển, duyên dáng với cái nét duyên ngầm như người con gái Bắc Bộ. Nhưng đẻ cảm được nó ta phải tốn công tốn sức không ít. Nhưng câu chuyện như không có chủ đích này phải được sắp xếp theo trình tự thời gian sáng tác của tác giả thì người đọc mới thẩm thấu được. Đinh Tị biên tập quá kém, trình bày cũng không tốt. Nói chung là ngứa mắt!
1
292321
2015-08-02 10:52:25
--------------------------
205640
8016
Sách thiết kế khá gọn gàng, dễ dàng mang theo bên mình để đọc ở mọi lúc mọi nơi. Cuốn sách sẽ gây hứng thú cho người đọc bằng những tác phẩm nổi tiếng của Thạch Lam và ngay sau đó là những bài bình có chất lượng, giúp bạn dễ cảm thụ hồn thơ của Thạch Lam hơn. Tuy nhiên có nhiêu tác phẩm in trong sách không có bài phê bình đính kèm, gây khó hiểu cho người đọc. Chất lượng sách khá tốt, bìa dày, chữ in rõ ràng. Nói chung, quyển sách này cũng đáng nên đọc.
5
302082
2015-06-07 11:14:22
--------------------------
191175
8016
Thạch Lam là một trong những nhà văn mình yêu quý bởi phong cách viết nhẹ nhàng, giọng văn trong sáng, giản dị và ẩn sau các tác phẩm là những ý nghĩa vô cùng sâu sắc. Đọc các tác phẩm của Thạch Lam khiến tâm hồn như dịu lại và mang theo một nỗi buồn man mác. Những lời bình trong cuốn sách đều rất hay, rất chính xác, đã diễn tả được cái hồn của tác phẩm cho người đọc hiểu hơn về văn Thạch Lam. Đây thực sự là một cuốn sách rất đáng đọc, nó sẽ giúp các bạn nhiều trong việc học tập cũng như là cảm thụ thơ văn trong chương trình Trung học phổ thông
5
606127
2015-05-01 09:44:27
--------------------------
101539
8016
Khá thu hút khi nhìn thấy quyển sách phê bình về tác giả mình yêu quý. Văn Thạch Lam luôn luôn là như thế. Một chút mong nành, một chút buồn, một chút thi vị ngọt ngào và tinh tế khôn lường. Mỗi khi đọc bất cứ trang văn nào của ông đều cảm nhận sự thanh lịch và kiêu kì của một tâm hồn phiêu lãng. Quyển sách với những lời lời nhận xét rất đúng và những chiêm nghiệm sâu sắc của tác giả. Đây hẳn là một tài liệu bổ ích cho các bạn yêu văn nói riêng và các bạn chuẩn bị cho kì thi đại học sắp tới. 
Một góp ý nhỏ đối với quyển sách là chất lượng in ấn chưa tốt lắm và trình bày khá loãng. Nhưng vẫn có thể chấp nhận được.
3
182747
2013-12-06 17:54:58
--------------------------
