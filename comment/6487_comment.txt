464910
6487
Đây là một cuốn sách rất thích hợp cho các bé, tuy vậy người lớn đọc cũng có thể bị mê :))) Những câu chuyện đời thường của một cô bé con hồn nhiên, vui vẻ sống cùng cha trên một hòn đảo hoang vắng. Đọc sách ta mới thấy cuộc sống của hai cha con như hòa vào cùng thiên nhiên, làm bạn với các loài động vật rất dễ thương và ngộ nghĩnh. Ngôn từ đơn giản, dễ hiểu và rất sống động, đủ để hấp dẫn các bé cho đến trang cuối cùng, đặc biệt truyện còn mang đến những bài học rất ý nghĩa, hay ho cho các bé, giúp các bé có thêm nhiều hiểu biết về thiên nhiên xung quanh mình.
4
411723
2016-06-30 16:55:36
--------------------------
425167
6487
Khi đọc vài trang đầu, mình cảm thấy không hấp dẫn lắm nhưng khi đọc hết chương 1 thì mình không thể bỏ xuống. Tác phẩm cực hay không chỉ với trẻ em mà còn đối với người lớn. Cuộc phiêu lưu của bé Nim hấp dẫn từ đầu đến cuối. Thêm vào đó là phần email giữa Nim và nhà văn, giữa Nim và bố Jack cũng rất dí dỏm, thông minh và rất đáng yêu. Một tác phẩm đáng để đọc đi đọc lại. Nội dung truyện vừa hiện đại vừa mang tính khoa học giả tưởng nhưng cũng rất thuyết phục. Những đoạn văn tả cảnh sinh động, như thể mình đang ở trong truyện. Một tác phẩm rất đáng để cho các bé đọc...vừa giải trí, vừa giàu trí tưởng tượng...vừa khuyến khích lòng can đảm và tình yêu thiên nhiên.
5
436550
2016-05-03 21:28:34
--------------------------
310021
6487
Truyện có chất rất trẻ con: những nhận định ngây ngô, những hành động dũng cảm không suy tính, những tình cảm ngây thơ chân thật... Những từ ngữ đơn giản nhưng gây được ấn tượng tốt đối với độc giả. Tác phẩm giống như một đứa trẻ đang lớn với thế giới riêng mà chúng nghĩ ra. Và vì thế thì làm sao ta lại không yêu một đứa bé như vậy. Đôi lúc để có thể trở lại thế giới tuổi thơ đầy màu sắc thì một cuốn sách thiếu nhi như vậy sẽ là một tấm vé tốt để bạn có thể quay ngược thời gian và làm sống lại cảm giác ngày còn thơ bé
4
5314
2015-09-19 11:35:16
--------------------------
298592
6487
Một câu chuyện dễ đọc đối với trẻ con, nội dung cũng hấp dẫn với trẻ. Dù là bản phim điện ảnh bay truyện mình đều thấy hấp dẫn. Qua Đảo của Nim mình nghĩ trẻ sẽ dễ cảm nhận được tình yêu thương gia đình, sự dũng cảm, tự lập của Nim và cả niềm hy vọng và sự cố gắng khi ta đang gặp hoàn cảnh khó khăn...
Xem phim không dưới ba lần rồi mà lần nào cũng thấy thu hút, nên khi thấy Tiki có bán sách mình đã mua ngay, một cuốn sách không thể thiếu trong bộ sưu tập sách thiếu nhi của mình!
5
49203
2015-09-12 19:50:55
--------------------------
214746
6487
Đây là truyện dài mà tôi đã đọc cho con tôi nghe đầu tiên vào buổi tối trước khi đi ngủ. Trí tưởng tượng siêu hạng và ngôn từ phong phú và lôi cuốn của tác giả đã hấp dẫn cả hai mẹ con tôi vào từng chi tiết trong câu chuyện. 
Nhóc của tôi đã buồn vì mẹ bị "tiêu hóa" trong bụng của cá  voi mất rồi; nhóc lại cười với những phép so sánh của tác giả "..nó đã chán làm thuyền và giờ nó muốn thử làm tàu ngầm...", rồi nhóc cũng rất thích rất thích tình bạn của Nim và cô nàng sư tử biển cùng chàng trai mồng biển bé bỏng. Tôi và con đã có những giây phút rất vui cùng với tác phẩm này. 
Nhưng, sau khi kết thúc có hai câu hỏi của cu cậu làm tôi băn khoăn suốt, con hỏi "sao không tìm được mẹ của Nim vậy Mama?" và câu thứ hai là "Sao Nim lại thích ở lại đảo mà không về với đất liền? Thật sự tôi không biết phải trả lời như thế nào với con.
4
461851
2015-06-25 14:23:09
--------------------------
