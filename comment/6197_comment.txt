348368
6197
Tôi đã đọc quyển sách này từ khá lâu, trước 1975 và không nghĩ rằng sách có cơ hội được tái bản, vì tác giả từng bị xem xét là "có vấn đề". Do đó rất vui khi sở hữu và được đọc lại quyển sách Nguyễn Thái Học, do người đồng chí của ông là Nhượng Tống biên soạn.
Sách viết về Nguyễn Thái Học không nhiều, quyển sách này tuy mỏng nhưng là một tài liệu chân thực nhất viết về ông và những đồng chí trong Việt Nam Quốc dân đảng. Có thể vài điều về quan điểm của tác giả không phù hợp với chính kiến ngày nay, nhưng nhìn chung đây là một tài liệu lịch sử có giá trị và còn là một câu chuyện rất bi tráng, cảm động về những con người yêu nước.
4
502606
2015-12-06 00:04:55
--------------------------
132536
6197
Tôi đã biết quyển sách này từ lâu và nay mới có dịp tìm đọc và nhận thấy tác giả viết rất trung thực và chính xác những sự kiện lịch sử. Tuy có một vài nhận xét chủ quan của tác có hơi thiên lệch. Và vài quan điểm có vẻ hơi lỗi thời nhưng những điều đó không làm giảm nhiều giá trị lịch sử và tư liệu của quyển sách này. Quyển sách thật sự có ích cho những ai yêu sử và muốn tìm hiểu sâu về lịch sử nước ta thời Pháp thuộc đặc biệt là tiểu sử một vị anh hùng dân tộc. Vì tác giả cũng là người cộng sự của Nguyễn Thái Học nên những hiểu biết và nguồn tư liệu của tác giả đưa ra rất đáng tin cậy. Đặc biệt là chương cuối tác giả viết về Cô Giang, với nhiều đoạn rất cao đẹp và cảm động. Hy vọng những quyển sách xưa và quý như thế này sớm được tái bản lại hết để các bạn trẻ có thêm những tài liệu tìm hiểu về những giai đoạn thăng trầm trong lịch sử đất nước. Nhân đây nói về hình thức của quyển sách. Trước hết, là bìa sách: với bức ảnh trắng đen rất trang nhã đúng với nội dung và giai đoạn ra đời của quyển sách. Giấy và chữ in khá rõ ràng. Tôi thấy thật đáng tiền khi sở hữu quyển sách này.
4
277587
2014-11-01 10:10:02
--------------------------
