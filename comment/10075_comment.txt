463733
10075
Học cùng lớp với Miko là một cô bạn cực kì giỏi bói toán, bằng chứng là nhờ phương pháp của bạn í đã làm Tappei cực kì bối rối khi bị nói là rất thích Miko. Vui hơn nữa là ở tập sau sẽ xuất hiện một nhân vật mới: EM GÁI MIKO!!! Làm lúc đầu mình tưởng Miko sắp chuyển nhà tới Hokkaido thiệt. Đó là về phần nội dung. Còn về phần bìa thì tập này thiết kế rất đẹp và khiến mình... đói bụng (hì hì, ngại ghê). Sản phẩm nào của "Nhóc Miko" cũng đều rất tốt, duy chỉ có trong quá trình vận chuyển sách của mình bị nhăn góc.
5
709791
2016-06-29 16:06:59
--------------------------
457238
10075
Chắc hẳn ai cũng một thời phát cuồng về những bộ truyện tranh của Nhật Bản . Bên cạnh Conan, Doraemon thì Nhóc Miko: Cô Bé Nhí Nhảnh cũng là một trong những bộ truyện xuất sắc của Nhật Bản . 
Nhóc Miko không là cuốn sách dùng để giải trí mà còn là một cuốn sách rất nhân văn, những tình huống hài hước trong quyển truyện còn để lại bài học trong cuộc sống. 
Cầm quyển truyện lên tay giống nhưng đang soi gương vậy, nhóc Miko giống như bản thân mình thu nhỏ, bản thân mình của thời thơ ấu. 
5
505164
2016-06-24 08:55:27
--------------------------
447202
10075
Đây thật sự là một bộ truyện tranh thiếu nhi rất dễ thương, hài hước và chứa đầy ý nghĩa về cuộc sống. Truyện chỉ là những mẩu truyện ngắn về cuộc sống đời thường xung quanh một cô nhóc cấp một nhưng lại hàm chứa bao điều, bao bài học về tình cảm gia đình, tình bạn, tình thầy trò, những rung động trong sáng của lứa tuổi học trò, những bài học về giáo dục giới tính và cả tình thương giữa người với người. Truyện không chỉ hợp với lứa tuổi thiếu nhi mà còn hợp với những người đã trưởng thành, không chỉ đem lại cho người đọc những giây phút giải trí mà còn là những suy ngẫm về thời thơ ấu mà ta đã vô tình quên đi. Nói chung đây là một bộ truyện rất hay và đáng đọc.
5
912535
2016-06-13 17:28:17
--------------------------
442205
10075
Truyện quá dễ thương luôn. Chỉ là những điều đơn giản hằng ngày của một cô bé học sinh tiểu học thôi mà thực sự rất thú vị. Chuyện về trường lớp, bạn bè khiến mình cứ nhớ hoài lúc còn học tiểu học. Tập này mẹ của Miko có em bé mà Mamoru tưởng ba chuyển công tác làm mọi người cứ nghĩ Miko phải dọn đi. Nhưng vậy mới biết được tình cảm của mọi người dành cho Miko. Và biết tình cảm của cả Tappei nữa. Truyện So-co-la tình yêu cũng rất vui. Nói chung mình rất thích.
5
366389
2016-06-04 16:40:07
--------------------------
433651
10075
Đây cũng là một trong những cuốn truyện Miko mình rất thích. Ở tập này, Miko cũng sắp có em bé rồi và sẽ có những mẫu chuyện khác về tình anh chị em rất dễ thương. Mình cũng rất thích mẩu chuyện "Let's study" và "Chạy đi Urana", đó cũng là một bài học ý nghĩa với chúng ta: nếu cố gắng sẽ có thành công. Mình sẽ tiếp tục theo dõi cô bé nhí nhảnh này qua những tập truyện tiếp theo. Hi vọng NXB Trẻ sẽ phát hành nhiều truyện Miko nữa để mình có phương tiện giải trí.
4
945933
2016-05-21 15:31:05
--------------------------
429379
10075
Miko là một cô bé dễ thương, nhí nhảnh và vô tư. Cô và Tappel lúc nào cũng cãi nhau nhưng có chuyện xảy ra thì họ lun cổ vũ, giúp nhau vượt qua. Tình cảm của trẻ con ngây thơ và trong sáng, làm tôi chỉ muốn ở thế giới đấy mãi mãi. Tình bạn của ba người Miko, Mari, Yuko thật đẹp và ngọt ngào luôn bên nhau đôi khi giận hờn vu vơ đấy nhưng lại rất dễ thương. Truyện Nhóc Miko sẽ đưa bạn về thời thơ ấu với những suy nghĩ và hành động vội vàng, ngây thơ, tình cảm.
5
108514
2016-05-13 09:22:43
--------------------------
371543
10075
Đây là quyển truyện miko với màu sắc sặc sỡ tươi rói .Tập này của truyện phải nói là cực hay.Tập này có những chương truyện cực kì hay như let's Study hay chạy đi uruna ngoài ra còn có những câu chuyện bên lề của tác giả cũng rất thú vị là phần mình thấy thích đọc khi kết thúc một chương truyện hay ho.Phải nói là tác giả cực kì sáng tạo,lúc nào cũng đổi mới cốt truyện và những câu thoại thì cực hay luôn.Mình cũng đã đọc nhiều truyện miko nhưng mình thấy tập này rất mới mẻ nhiều chuyện hay.Mình mong tác giả sẽ vẽ miko nhiều hơn nữa.
5
682270
2016-01-19 20:10:38
--------------------------
366997
10075
Miko cô bé nhí nhảnh, đúng như cái tên, mà không chỉ miko, tôi thấy dường như tất cả các nhân vật trong truyện này đều toát lên một loại nhí nhảnh riêng biệt, và chúng đều đáng yêu đến lạ. Yuko, Mari, Kenta, Tappei và các bạn nhỏ khác, cùng với Miko sẽ đưa bạn trở về tuổi ấu thơ trải nghiệm thêm một lần nữa những câu chuyện xen lẫn vui buồn ở một không gian mới, sẽ rất khác. Với Miko bạn sẽ không thấy nhàm chán, bạn sẽ đọc đi đoc lại nhiều lần mà vẫn cảm thấy như lần đầu.
5
742732
2016-01-11 10:48:51
--------------------------
314253
10075
Nhóc miko - cô bé nhí nhảnh tập 18 là một trong những cuốn miko mà mình yêu thích nhất . Trong tập lần này thì mình thích nhất là tập truyện ngắn Lets study trong tập này rất mắc cười , lúc đầu thì Miko đòi mẹ cho học bằng được xong thì về sau thì ngán -> y chang ngoài đời luôn . Bìa truyện lần này rất dễ thương , màu sắc tươi sáng . Miko dùng để giải trí là rất tuyệt vời , truyện phản ánh đời thực qua những khung bậc dễ thương nhí nhảnh , truyện cũng dạy cho ta rất nhiều bài học quý giá . Miko rất hay . 
4
635786
2015-09-25 17:41:35
--------------------------
311048
10075
Trong tập này nhóc Miko cũng như gia đình Miko sắp chào đón thêm một thành viên mới nữa, niềm vui lẫn háo hức của nhóc Miko khi sắp có một đứa em ..mình thích nhất tập này là mẩu truyện "trò chơi bói toán" cả cái trò nắm ngón tay xem ai thích bạn hay ghét bạn, hay "chạy đi Urana " mình hiểu rằng bói toán hãy coi nó là một trò chơi giải trí thui chứ đừng tin vào nó quá  mà hãy tin vào chính sự nỗ lực của mình..Nhóc Miko dạy cho mình nhiều điều hơn. Cám ơn tác giả khi đã sáng tác ra một bộ truyện tranh thực sự rất hay và thú vị .
5
165748
2015-09-19 21:13:46
--------------------------
293522
10075
Mình mua cuốn này ở Tiki khá lâu rồi nhưng bây giờ mới viết nhận xét. Miko giống mình lúc nhỏ thật đấy, thấy bạn bè có gì cũng muốn có nhưng sau đó lại không hoàn thành. Miko có những biểu cảm thật dễ thương và hài hước. Mình đang buồn mà đọc Miko cũng phải bật cười thành tiếng. Mình thấy đây là bộ truyện giải trí rất phù hợp không chỉ với trẻ em mà ngay cả với người lớn như mình nữa. Hi vọng Miko đã đang và sẽ là món ăn tinh thần với mọi nhà.
5
247850
2015-09-08 13:46:29
--------------------------
287649
10075
Mình vô cùng ấn tượng với chương Lets Study trong tập 18 này. Let's Study đã cho thấy tình yêu thương bao la của mẹ Miiko dành cho Miiko dù Miiko có mắc phải lỗi lầm. Vì việc học của Miiko, dù biết Miiko lười nhưng mẹ cũng đồng ý đăng kí cho Miiko, bài tập thì được gửi hàng tháng nhưng Miiko với bản tính lười nhác cứ chất thành đống cho đến khi bị mẹ phát hiện ra. Mẹ Miiko đương nhiên rất tức giận, nhưng đã dịu lại và chỉ bảo cho Miiko từng chút một. Nhờ thế điểm của Miiko đã tiến bộ, và mình càng thêm yêu quý tình mẫu tử mà tác giả muốn truyền tải cho ta: mẹ luôn yêu thương chúng ta dù chúng ta có mắc phải lỗi lầm, luôn từ từ dìu bước và dẫn ta đi theo con đường đúng đắn. Rất cảm ơn tác giả đã sáng tác ra những chương truyện tràn đầy tình yêu thương gia đình như thế- khiến mình càng thêm yêu và trân trọng gia đình hơn!
5
364412
2015-09-02 22:06:32
--------------------------
239862
10075
Đọc phần này mình thích nhất là phần Let's Study  , không biết ở VN có cái này không nhỉ ? Làm đúng còn được tặng quà nữa chứ , rõ vui . Nhưng mình thấy Miko lười học quá , suôt ngày ham chơi với đi bơi nữa :) Miko hay bị điểm kém :) khác hẳn với người thích Miko thì học giỏi xong lại còn cao nữa chứ . Khác nhau 1 trờii 1 vục luôn . Tappie với Miko thì chỉ khác nhau về chiều cao thôi chứ lười thì chắc cũng giống nhau cả :))
4
713167
2015-07-24 09:39:53
--------------------------
212674
10075
Đây cũng là một trong những cuốn trong series Nhóc Miko mà tôi thích nhất. Tập này Miko sắp có em bé rồi. Thật sự rất bất ngờ và mong chờ, chắc chắn sẽ là những câu truyện ngọt ngào, xúc động về tình chị em nữa. Tập này cũng có một mẩu truyện tôi rất thích. Tác giả đã lồng ghép một bước ngoặt mới trong gia đình Miko (truyện có em bé đó) cùng với sự phát triển trong tình cảm của tappei và Miko trong một trò chơi bói toán thật dễ thương. Bản thân tôi không tin tưởng lắm vào mấy trò phán đoán nhưng câu truyện trong sách lại rất gần gũi, đáng yêu làm tôi không thể ghét được, thậm chí còn thích thú hơn khi Tappei đã "vô tình" thể hiện tình cảm với Miko. 
Nói tóm lại, tôi rất hài lòng khi mua cuốn sách này, mong tác giả nhanh nhanh sáng tác để cho tôi được tiếp tục theo dõi cuộc sống dễ thương của Miko quá.
5
68745
2015-06-22 18:50:24
--------------------------
74142
10075
Đọc tới tập 18 này thì tôi ấn tượng nhất là hai yếu tố:
Thứ nhất là chuyện xem bói toán, không hiểu có tin hay không mà sau khi đọc tập này xong tôi đều nhờ những người xung quanh chọn một trong những ngón tay của mình để biết được mức độ tình cảm họ dành cho mình! Những người bình thương thì không ai chọn ngón cái hết, thế nhưng Tappei lại chọn và đưa ra một lí do rất hài hước! Đây là cái ấn tượng ngộ nghĩnh nhất trong tập này!
Thứ hai là chuyện nhóc Miko sẽ có thêm thành viên mới lại càng hấp dẫn. Không biết là trai hay gái đây ta?
Cuối cùng là bói toán có thể dự đoán tương lai nhưng chính chúng ta là người có thể thay đổi tương lai! Đây là thông điệp mà tôi thấy tác giả muốn gửi đến cho bạn đọc!
5
104652
2013-05-11 21:04:07
--------------------------
73048
10075
Miko một cô bé lười học, ham chơi, hoạt bát, nhí nhảnh, hay giúp đỡ bạn bè và được mọi người yêu quý .
mỗi ngày của cô luôn tràn đây bất ngờ và thú vị trong đó có những rắc rối hết sức buồn cười. em trai của cô là một người khá hoàn hảo về mọi mặt nên cô rất ghen tị dẫn đến hay cãi nhau nói chung là truyện rất hay . very good!

Truyện làm tôi như sống lai tuổi thơ của mình. Đọc truyện là một cách thư giãn tốt nhất sau mỗi giờ học căng thẳng của tôi : đặc biệt là truyện nhóc Miko
4
74761
2013-05-06 08:05:59
--------------------------
