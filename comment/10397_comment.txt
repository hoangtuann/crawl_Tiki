332766
10397
Ưu điểm cuốn sách: hay, cung cấp nhiều thông tin hữu ích về sức khoẻ sinh sản, tình dục. Cuốn sách này giúp cho nhiều người có những thông tin bổ ích hơn.
Nhược điểm: cuốn sách này là một trong hai cuốn của một tập sách cũng không dài dài lắm; nên việc tách làm hai quyển gây khó khăn cho việc tìm và mua sách; giá cả sách hơi đắt, cần chỉnh sửa. Nếu có thể thì nên in thành một quyển để giảm chi phí.
Dù sao thì cũng xin cám ơn tác giả. Nếu có nhận xét gì chưa hợp lí thì xin mong được tác giả bỏ qua. Xin cám ơn trang mạng này đã có các cuốn sách hay như trên. Có gì xin được thông cảm nếu  như  nhận xét không phải ở chỗ nào đó. Mong nhận được phản hồi từ trang mạng mua bán trực tuyến Tiki này sớm. Xin cám ơn. Chúc cho tác giả và trang mạng luôn có những điều tốt!
5
937733
2015-11-06 21:57:10
--------------------------
