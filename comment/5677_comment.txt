473027
5677
Nội dung thì không có gì phải chê hết, bìa cũng rất đẹp và cuốn hút, mình biết Roald Dahl từ cuốn Charlie và nhà máy socola, phải nói sách nào của Roald Dahl mình cũng thấy hay, dành cho mọi lứa tuổi chứ không chỉ thiếu nhi. nhưng mình lại thấy vọng với font chữ của quyển này. Font chữ rất khó đọc, còn nhỏ liu tiu nữa, mình đọc thấy rất mỏi mắt, phải vừa đọc vừa dụi mắt cho đỡ mỏi ấy. Trừ điểm ấy ra thì mình nghĩ đây đúng là một quyển sách đáng tiền mua.
5
713935
2016-07-10 10:30:45
--------------------------
377360
5677
Vốn hâm mộ Roald Dahl qua những tác phẩm Charlie và nhà máy sô-cô-la, James và quả đào khổng lồ, Danny - nhà vô địch thế giới..., thời gian gần đây mình lại tiếp tục tìm mua những tác phẩm khác của nhà văn này cho con gái đang học lớp 4.
Con gái đã tỏ ra vô cùng thích thú truyện Sophie và tên khổng lồ, làm mình quá tò mò phải tạm ngưng một quyển sách khác để đọc tác phẩm này. Để rồi hai mẹ con có chung một "thần tượng": anh bạn BFG với cách nói ngọng và ngược ngộ nghĩnh cùng những suy nghĩ đậm chất người mà con gái thấy thật dễ thương còn mẹ thì đón nhận như những bài học sâu sắc.
5
62966
2016-02-02 13:15:11
--------------------------
375288
5677
“Câu chuyện về Sophie và tên khổng lồ” của Roald Dahl thật hấp dẫn với nội dung lạ lùng đến mức không tưởng về một cô gái nhỏ dễ thương và một tên khổng lồ tốt bụng. Nhà văn Roald Dahl đã đem đến một câu chuyện kỳ thú, lôi cuốn, đưa người đọc khám phá vương quốc những người khổng lồ và vương quốc của những giấc mơ! Điểm thú vị của câu chuyện là xuất hiện nhân vật nữ hoàng nước Anh, làm cho câu chuyện mang dáng vẻ hư mà thực. Các tác phẩm của Roald Dahl quả thực không truyện nào giống truyện nào, luôn khiến mình thích thú và háo hức được đọc tiếp!
5
135597
2016-01-28 09:27:58
--------------------------
173526
5677
Để những người có bề ngoài kỳ quái cũng nhận được thiện cảm vì vẻ đẹp trong tâm hồn của họ. Mình thích những truyện như thế, và cũng muốn các bạn nhỏ của mình hiểu rằng - nếu như ta sinh ra ko thể đẹp - thì cũng có rất nhiều cách khác để ta đẹp trong mắt mọi người.

Cuốn sách này dịch ra được tiếng Việt hẳn là cả một nỗ lực lớn lao của dịch giả vì để chuyển tải hết những cái ngọng nghịu của bạn khổng lồ ra tiếng Việt thật ko dễ dàng chút nào.

Tối qua mình cũng gạ được bạn nhỏ ngủ sớm, để chú khổng lồ tốt bụng còn thổi giấc mơ vào cửa sổ cho bạn í...
5
581500
2015-03-26 13:12:31
--------------------------
147904
5677
Đây là một cuốn sách thuộc dạng những câu chuyện siêu dễ thương mà chắc chắn mình sẽ đọc cho con vào giờ đi ngủ trong một ngày tương lai nào đó. Một câu chuyện nhẹ nhàng, ý nghĩa nhưng vẫn đủ hấp dẫn và li kì để cuốn hút bất cứ ai, kể cả nhiều người lớn. Sophie và tên khổng lồ không đơn thuần là một cuốn sách đơn giản, dễ hiểu dành cho trẻ em mà còn chứa đựng nhiều triết lí sâu sắc và bài học cho cuộc sống. Mình chỉ thấy tiếc rằng lần tái bản này không còn giữ lại hình minh họa cũ vốn đẹp hơn nhiều.
4
248911
2015-01-09 10:18:09
--------------------------
