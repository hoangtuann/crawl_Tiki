287020
6615
Mình mua cuốn sách này kèm với cuốn Quản trị dự án hiện đại. 
Cuốn sách không chỉ tóm tắt lại lý thuyết một cách súc tích mà còn có nhiều bài tập phong phú, bài tập vừa có bài tập về tình huống vừa có bài tập về tính toán.
Các bài tập trong cuốn sách giúp bạn nâng cao kỹ năng quản trị dự án của mình bằng cách chỉ cho bạn cách thực hiện một sự và kiểm soát tiến trình thực hiện nó.
Các tình huống được đưa ra trong cuốn sách cũng là những tình huống thực tế mà bất cứ nhà quản trị nào cũng phải giải quyết khi thực hiện dự một dự án. 

5
280007
2015-09-02 10:22:42
--------------------------
