221835
5941
Theo mình đây là một cuốn sách hay về cách chế biến các món ăn chay. Phù hợp cho các chị em phụ nữ như mình. Thỉnh thoảng nhà mình cũng làm cúng các món chay vào các ngày rằm lớn nhưng mình hầu như không biết làm món chay gì cả; tới chùa ăn thì thấy các sư cô với các phật tử nấu đồ chay từ nấm và các loại củ quả sao ăn mà ngon. Mình thấy cuốn sách hình minh họa đẹp, các nguyên liệu cũng dễ kiếm mà chế biến cũng đơn giản, dễ hiểu, trình bày rõ ràng
5
572238
2015-07-04 15:10:01
--------------------------
