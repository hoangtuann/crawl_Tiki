448320
9669
Ánh lửa tình bạn là một cuốn sách hay và ý nghĩa. Sách mang đến cho người đọc những thông điệp đầy ý nghĩa về tình bạn. Tình bạn luôn là thứ tình cảm trong sáng, đẹp đẽ nhất của tuổi học trò. Luôn là thứ tình cảm thiêng liêng, đáng quý trọng biết bao. Tình bạn giúp ta nhân đôi lệ niềm vui mỗi khi ta vui, giúp ta chia bớt đi nỗi buồn khi ta buồn, giúp ta đứng lên sau mỗi lần ta gục ngã và cũng giúp ta có những bài học quý báu về cuộc sống
4
1101166
2016-06-15 20:25:33
--------------------------
433514
9669
"Ánh lửa tình bạn" - cuốn sách với bao nhiêu câu chuyện đẹp về tình bạn vĩnh cửu. Nó giúp độc giả nhận thức được sự kì diệu của tình bạn. Kể cả khi ta lìa xa thế giới này, tình bạn sẽ mãi luôn bên ta. Khi gặp khó khăn, hoạn nạn, khi có vui buồn, tình bạn luôn là thứ giúp ta đứng lên và có thêm động lực. "Thời gian trôi qua, mọi thứ sẽ tàn phai, chỉ có tình bạn là ở lại"
Giấy và chất lượng in tuyệt vời. Tuy nhiên bìa của cuốn sách cần được trau chuốt hơn.
5
1087013
2016-05-21 10:31:24
--------------------------
322968
9669
Tin hay không thì bạn cũng không thể phủ nhận rằng trên cuộc đời này có những tình bạn vĩnh cửu, mãi mãi theo năm tháng, đẹp hơn tình yêu đôi lứa và quý giá không thua gì tình thân ruột thịt. Theo tôi nghĩ có được một người bạn tốt là một trong những điều may mắn nhất trong cuộc đời mỗi người. Qua các bài viết, các mẩu chuyện nhỏ trong cuốn sách tâm hồn "Ánh lửa tình bạn" tôi đã thấm thía được rất nhiều điều mà có khi bởi cuộc sống thường nhật bộn bề tôi chợt lãng quên để rồi đánh mất lúc nào chẳng hay. Cuốn sách còn giúp tôi biết cách cư xử hợp lý, đối đãi chân thành với bạn bè và biết cách giữ lửa tình bạn, Một cuốn sách thật hay và ý nghĩa.
4
6502
2015-10-17 14:37:11
--------------------------
308719
9669
Ánh lửa tình bạn là một cuốn sách hay. Từ cách trình bày bìa đến bố cục bên trong sách đều rất phù hợp cùng với thiết kế  nhỏ gọn dễ dàng cầm tay khi mang theo. Cuốn sách đưa ta đến những câu chuyện giản dị, những lời khuyên chân thành về tình bạn  được đúc rút từ chính bài học của cuộc sống. Cũng nhờ đó mỗi chúng ta nhìn nhận sâu sắc hơn về giá trị của một người bạn tốt và quý trọng nó như một món quà vô giá tuyệt vời. Hãy biết cho đi mà không tính toán và lúc đó điều diệu kì của tình bạn sẽ cho chúng ta tất cả.
4
494448
2015-09-18 20:36:29
--------------------------
280721
9669
Thật hạnh phúc, thật may mắn khi ta có được những người bạn tốt. Những phút giây bên bạn sẽ là những khoảng ký ức đẹp nhất trong tuổi trẻ của mỗi người. Tình bạn ấm áp, bập bùng như ánh lửa vậy, và sẽ soi rọi dẫn đường ta khi ta lạc lối. Mình mong cuốn "Ánh lửa Tình bạn" này có thể được nhiều bạn đọc hơn nữa. Đọc để quý trọng từng phút bên bạn hiện tại, đọc để thêm yêu những người bạn ta đang có, đọc để suy ngẫm và trân trọng hơn những tình bạn đẹp đẽ này :)
5
39471
2015-08-27 21:36:30
--------------------------
222285
9669
Tôi đã mua cuốn sách này trên tiki 2 lần, một l;ần cho tôi và một lần cho người bạn của tôi. Sau khi đọc cuốn sách, tôi có nhiều cảm xúc lắm. Chắc bởi do lối trình bày và nội dung tuyệt vời của nó. Đọc cuốn sách này, ta sẽ có thêm được những bài học để hiểu được giá trị thiêng liêng cũng như cách gìn giữ một tình bạn, một người bạn cho bản thân mình. Cuốn sách này như thay lời tôi nói lên tất cả những tình cảm, sự đáng quý mà tôi muốn  gửi đến bạn mình. Không quá cầu kì, nhẹ nhàng, đơn giản mà đầy ấn tượng!
5
292005
2015-07-05 11:42:52
--------------------------
203049
9669
Bạn là người đồng hành cùng với ta trong suốt khoảng thời gian trưởng thành, nếu đó là 1 người bạn đúng nghĩa. Tình bạn chỉ thật sự bền vững khi nó xuất phát tự sự chân thành của mỗi người, không lợi dụng, không so đo tính toán thiệt hơn. Có được 1 người bạn như vậy, chả khác nào chúng ta có cả 1 bầu trời yêu thương. Đọc "Ánh lửa tình bạn" để thấy trân trọng hơn thứ tình cảm mà mình đang có, để biết mà giữ gìn, làm cho nó ngày một sáng hơn. Hãy cảm ơn cuộc đời nếu bạn đã có cho mình 1 tình bạn tuyệt vời như thế!
4
131327
2015-05-31 09:22:56
--------------------------
