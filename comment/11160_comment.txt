299091
11160
Đây là lần đầu tiên tôi đọc sách của Chip Heath & Dan Heath. Cuốn sách có tựa đề rất hấp dẫn là “Quyết đoán”, tuy nhiên nội dung trình bày hơi bị rối. Vẫn với cách trình bày tương tự với các độc giả phương Tây là trình bày vấn đề bằng phương thức nghiên cứu trường hợp – tức Case study. Mở đầu quyển sách là vấn đề Shannon có nên sa thải Clive hay không? Rồi từ đó tác giả tiếp tục trình bày ý tưởng của mình một cách đầy rối rắm. Luận điểm, luận cứ và luận chứng được trình bày một cách không rõ ràng, nhập nhằng với nhau. Do vậy mà người độc rất khó tiếp thu tư tưởng của tác giả vì vốn dĩ vấn đề không được trình bày một cách logic, mạch lạc. Tôi tự hỏi không biết đây là lỗi của dịch thuật hay là từ chính bản gốc. Tóm lại, tôi không thích cuốn sách này lắm.
3
329218
2015-09-12 23:52:51
--------------------------
275508
11160
Ra quyết định là kĩ năng quan trọng không chỉ trong công việc mà còn trong cuộc sống hàng ngày. WRAP là một công cụ về ra quyết định có chiến lược và quy trình, nhằm giúp chúng ta tránh khỏi những bế tắc và đạt được những gì mình mong muốn.  Các ví dụ trong cuốn sách khá nhiều, sinh động và thiết thực. Cuối mỗi chương được tóm tắt giúp người đọc có cái nhìn tổng quan nhất và tiếp thu cuốn sách tốt nhất . Cuộc sống như một chuyến đi, những ngã rẻ chỉ chiếm rất ít thời gian nhưng nó lại quyết định tất cả những phần còn lại.  
4
77641
2015-08-22 22:28:25
--------------------------
191839
11160
Mình khá là tò mò khi tham khảo các cuốn sách của hai tác giả này. Quả thực mình đến với cuốn sách này trong một dịp khá tình cờ, chính vì vậy mà đọc xong nó mình cũng cảm thấy bất ngờ không kém. Nói về cuốn sách này thì nó có nhiều cái để chúng ta học hỏi mặc dù tác giả viết hơi khó hiểu và phần dịch thuật có đôi chỗ làm cho người đọc lúng túng. Tác giả viết theo mạch của bản thân làm đọc giả phải tư duy và kết dính các thông tin lại với nhau. Tuy nhiên mình thích lối viết này vì nhờ cách viết như vậy chúng ta sẽ phải hình dung những điều tác giả nói và logic tất cả dữ liệu. Có thể nhiều người sẽ thấy cuốn sách ko được logic và chặt chẽ. Mặc dù vậy, hai tác giả đã dẫn ra cho chúng ta thấy mọi việc trong cuộc sống để từ đó đưa ra cho chúng ta lời khuyên bổ ích trong việc đưa ra các quyết định. Trong đó mình cực thích thú với WRAP và các lời khuyên cũng như bài tập thực hành của tác giả. Một quyết định đúng đắn, quyết đoán cũng sẽ là yếu tố rất quan trọng trong sự nghiệp và cuộc sống của bạn. Dẫu mình chỉ áp dụng được 1/100 những gì mà tác giả viết nhưng mình cảm thấy khá hài lòng khi chọn cho mình cuốn sách này. Thực sự đọc xong cuốn sách này, đọc và suy nghĩ những gì tác giả viết, mình luôn tự hỏi "Liệu quyết đoán có dễ như trong mộng?"
4
185178
2015-05-02 22:54:09
--------------------------
117642
11160
Mình đã khá kỳ vọng vào 2 anh em nhà Heath vì đã nghe danh họ lâu. Đã quyết định đọc cuốn này trước cả 2 cuốn còn nổi tiếng hơn của họ là Made to Stick & Change.

Tuy nhiên, nói chung là hơi thất vọng và còn hơi nghi ngờ khả năng hành văn một cách mạch lạc và trình bày vấn đề một cách logic trong 2 cuốn còn lại.
---

Điểm mạnh của Quyết đoán:
- Dựng lên 1 khung để ra quyết định gọi là WRAP trong đó: W - Widen your options (Mở rộng lựa chọn của bạn); R - Reality-Test your assumption - Thử nghiệm giả định của bạn; A - Attain distance before deciding - Giữ khoảng cách với các lựa chọn trước khi ra quyết định; P - PREPARE to be wrong - Sẵn sàng để sai. Phần về Reality - Test your assumption - rất giống với quan điểm trong các cuốn sách về lean. Thử nghiệm & đo lường rồi ra quyết định. Tất nhiên, do nó nằm trong cuốn sách về ra quyết định nên chỉ đề cập rất sơ lược. Nếu muốn tìm hiểu nhiều hơn về phần này thì Khởi nghiệp tinh gọn cũng khá tốt để bắt đầu.
- Một số lời khuyên phải nói là rất thú vị về việc kỹ thuật phỏng vấn nhân sự hoặc lựa chọn nhân sự. Ví dụ, chuyện người phỏng vấn thường có xu hướng tự tin vào khả năng "đánh giá con người" của mình trong có 1 giờ phỏng vấn. Trong khi sự thật thì ngược lại, và vì vậy khéo léo đưa một bài tập như thật cho ứng viên làm là một cách thông minh để thật sự biết về khả năng của ứng viên.
- Giúp người đọc có một ý thức về quy trình ra quyết định. Nếu rèn luyện liên tục theo quy trình trên thì mình nghĩ chúng ta sẽ khá hơn rất nhiều.

Điểm yếu:
- Điểm mình muốn nói đến đầu tiên là khả năng liên kết các câu chuyện và các chương với nhau. Đọc cuốn sách này rất hay bị "lạc"/ bị trôi trôi đi. Vì 2 tác giả có vẻ hơi bị hào hứng quá với cái framework mà mình nghĩ ra. Và thế là họ cứ kể kể các câu chuyện mà đôi khi không nhìn lại xem nó đang bổ trợ và liên kết với nhau xuyên suốt như thế nào. Ví dụ như khi tác giả đưa ra framework WRAP trên thì các chương lại không hướng thẳng đến 4 gợi ý trên. Người đọc sẽ phải tự tìm cách mà liên kết nó lại. Và vì vậy, nó có vẻ không giống lắm với truyền thống viết luận của phương Tây: cực kỳ chặt chẽ, nhất quán và logic. 
- Dịch không tốt lắm. Phải nói là khá trúc trắc và mình nghĩ một phần lớn cái cảm giác trên là do người dịch đã không đủ tầm hoặc tâm để truyền tải một cách trong sáng cuốn sách này.

---
Kết luận:
- Mua Quyết đoán, khi bạn đang băn khoăn một vấn đề cụ thể gì đó và nó tương đối lớn. Vì dù có thể còn một vài khiếm khuyết, nhưng nó sẽ giúp bạn khai mở một số điểm cực kỳ quan trọng trong quá trình ra quyết định. Ví dụ nó sẽ giúp bạn bớt cảm tính trong quá trình ra quyết định.
- Nếu quyết tâm mua - như mình, thì hãy dành cho nó sự kiên nhẫn. Một cái bút gạch dòng, bút chì, sticky note có thể giúp bạn nhặt nhạnh được nhiều thứ hay ho để áp dụng.
3
3789
2014-07-21 10:28:38
--------------------------
