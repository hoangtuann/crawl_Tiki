459668
8233
Ngay từ tựa đề tác phẩm đã gây tò mò cho mình, và sau khi đọc xong hoàn toàn không thất vọng. Cuốn sách mang lại cho bạn những tiếng cười rất tự nhiên, nhẹ nhàng nhưng lại châm biếm thâm thúy, đọc đi đọc lại vẫn thấy được ý nghĩa của nó, không hề thấy nhạt đi. Các chương trong sách tiếp nối thành 1 câu chuyện hoàn chỉnh nhưng bạn hoàn toàn có thể đọc riêng lẻ mà không mất đi ý nghĩa. 
Có lẽ vì sách xuất bản đã lâu nên chất lượng bìa và giấy không vừa ý mình lắm, giấy xỉn màu còn mực in đôi chỗ bị lem. Nhưng về bản dịch thì mình đánh giá cao hơn bản của Nhã Nam mới đây, đọc rất thú vị, tuy đã bị lược mất 1 số chương!
4
112948
2016-06-26 12:36:31
--------------------------
278411
8233
Mình rất hối tiếc khi mua cuốn này, bởi vì đã có phiên bản dưới tên "Ba gã cùng thuyền - chưa kể con chó". Tôi không hiểu sao tựa sách thay đổi mà đến tên tác giả cũng khác khiến mình nhầm lẫn. Mua về mới tá hỏa nhận ra là đã đọc rồi. Nội dung cuốn hay thì rất, dí dỏm, hài hước triết lý kiểu Anh rất tinh tế. Nhưng đọc cuốn của Nhã Nam có cảm tình hơn nhiều, bìa đẹp, giấy láng mịn, dày dặn. Còn bản này hình như do đã xuất bản lâu rồi nên hình thức cũ kỹ, giấy mỏng tèo lại hơi đen đen.
3
6502
2015-08-25 21:22:04
--------------------------
245772
8233
Truyện đọc rất vui, hài hước, không phải kiểu đùa cợt thô lỗ đâu mà là những tiếng cười thâm thúy, sâu sắc và đầy trí tuệ, khiến độc giả phải vỗ đùi đánh đét mà khen hay. Ngay từ cái bối cảnh kể về "3 gã cùng thuyền, chưa kể con chó" đã báo trước 1 câu chuyện vui nhộn rồi. Dù rằng câu chuyện đã xảy ra từ cách đây rất lâu, rất lâu, nó vẫn giữ nguyên giá trị châm biếm trong thời hiện đại.
Cuốn sách mỏng, nhưng giá trị nó mang lại cho người đọc không bé nhỏ chút nào.
4
293599
2015-07-29 00:51:14
--------------------------
