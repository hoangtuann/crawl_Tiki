463762
4902
Nghệ thuật mắng là cuốn sách có đề tài lạ và độc đáo, hữu ích. Bìa sách đẹp, giá hợp lí mà hình thức trình bày rất đẹp và khoa học. Nội dung sách nhẹ nhàng, cách diễn giải thú vị và hợp lí. Ngô Lệ Na đã nêu đầy đủ nhiều tình huống thường gặp và cách xử lí phù hợp. Sách không chỉ dạy chúng ta cách "mắng" mà còn khuyến khích chúng ta lúc nào nên mắng và lúc nào không, khi nào cần dùng hành động để chứng minh là mình đúng. Bên cạnh đó, tác giả cũng chỉ ra thái độ cần có để cãi nhau một cách thẳng thắn mà không phá vỡ mối quan hệ tốt đẹp.
4
563925
2016-06-29 16:26:54
--------------------------
336399
4902
“ Nghệ thuật mắng “ là một cuốn sách lạ, khá là thú vị khi nhìn thấy tựa sách, cuốn này chỉ ra một phương diện giao tiếp đối ngược hoàn toàn mới tác phong mềm mỏng của mình, đương nhiên trong cuốn này cũng có yếu tố mềm mỏng. Nói chung là khá hay. Sách khá mỏng, thời gian để đọc cũng khá là nhanh, khoảng 2 đến 4 tiếng. Các bạn nên đọc thử để thấy được nhiều phương diện khác trong giải quyết xung đột. Sách bọc một lớp bóng mỏng nên bọc với bookcare không đẹp, 2 lớp bọc cứ dính vào nhau như có nước.
4
787058
2015-11-12 13:00:46
--------------------------
324628
4902
Vốn là người hiền lành, hay nhẫn nhịn, đột nhiên thấy tựa đề cuốn sách "Nghệ thuật mắng" không khỏi khiến m tò mò. M đã mua nó và quả không thất vọng!
Quyển sách tuy mỏng nhưng hầu như trang nào cũng có kiến thức đáng học hỏi. Tác giả đặt ra: Tại sao chúng ta không dám "mắng"? , Cách để phản pháo ông chủ, bậc bề trên mà không làm mất lòng họ, ... quả thực rất hữu ích cho bất kì ai. 
Giờ tôi đã tự tin hơn trong việc đưa ra lý luận để phản pháo lại người khác. Tuy nhiên, sẽ còn phải luyện tập nhiều bởi kiến thức chỉ có đưa vào thực hành mới phát huy tính hiệu quả! Cảm ơn tác giả Ngô Lệ Na và nxb Thanh Hóa đã mang đến một tác phẩm lạ - bổ ích! 
5
339241
2015-10-21 17:02:58
--------------------------
319717
4902
Mắng cũng là cả một nghệ thuật và dĩ nhiên tôi khá vụng về vì điều ấy. Nhưng từ ngày đọc cuốn sách này tôi đã biết cách ứng xử hơn, đơn giản cũng chỉ là bình tĩnh hơn,  biết cách đoàn kết mọi người đứng về phía mình. Cuốn sách với lời lẽ nhẹ nhàng, không đao to búa lớn nhưng thực sự có tác dụng sâu sắc với tôi, giúp tôi tránh được những xung đột, hoặc sự nhẫn nhịn không cần thiết... Cách thiết kế cũng khoa học, bắt mắt, giá cả cũng tương đối hợp lý.
Nói tóm lại thì:đây là một cuốn sách hay đáng để cho mỗi chúng ta tự đọc và chiêm nghiệm.
3
702741
2015-10-09 13:36:18
--------------------------
307942
4902
Đây là cuốn sách có cái tên khá lạ trong nghệ thuật giao tiếp, đi sâu vào việc "mắng", chính xác hơn phải là làm thế nào để phản pháo, đáp trả đối phương và để không bị người khác có cớ gây hấn với mình . Tuy sách cũng mỏng, nội dung đơn giản nhưng những thông điệp, câu chuyện, bài tập của sách khá là hấp dẫn, có tính ứng dụng tốt. Đọc cũng vui vui, mình canh me mãi vì đợt biết sách này thì nó hết hàng, giờ mua được rồi, nói chung cũng là đáng đồng tiền bát gạo
4
588698
2015-09-18 13:19:28
--------------------------
264800
4902
Sách này từ đầu mới đọc tựa đề chắc hẳn ai cũng cảm thấy tò mò và lần ngay đến đoc thử, bắt mắt từ tựa đề, đến bìa rất đẹp, bên trong nội dung khá bao quát, dễ đọc, dễ hiểu, gọn nhẹ,dễ cầm, tiện lợi. Giá cả cũng rất hợp lí. Sách này mình nghĩ chúng ta ai cũng nên đọc mộ lần trong đời, hơn thế là dành cho những người nhút nhát, rụt rè, yếu lợi thế trong xã giao nơi cộng đồng,xa hôi. Giúp chúng ta rút ngắn tình thế tranh cãi,không nên tranh cãi đến cùng, nói lý dễ nghe dễ giảm bây bầu không khí tranh cãi.Rất đáng mua để đọc!!
5
621499
2015-08-13 14:34:25
--------------------------
243318
4902
tôi nghĩ quyển sách phù hợp cho những bạn có tính nhút nhát, chưa hiểu cách bảo vệ bản thân trước những tình huống đời thường. Quyển sách có hướng dẫn cũng khá chi tiết về cách thức xử lý khi đối mặt với việc tranh cãi để bản thân không mất quyền lợi. cũng có nêu một vài tính huống mà hay gặp trong cuộc sống và cách xử lý phù hợp. Khi đọc thì nên đi kèm thực hành để tọa phản xạ ứng biến nhanh nhạy. 
Bản thân tôi đọc xong rút ra cũng được khá khá :3
4
462978
2015-07-27 13:20:40
--------------------------
239562
4902
Bạn có thấy lạ khi đọc tựa sách không,’Nghệ thuật mắng’? Mình mua cuốn sách này một phần vì tựa đề của nó, một phần là vì mình khá hiếu thắng nên hy vọng qua cuốn sách mình có thể chiến thắng trong các cuộc đấu khẩu. Sách khá hay, nói về các trường hợp mà ta nên lên tiếng ‘mắng’, các phương pháp đấu khẩu trong từng trường hợp với những đối tượng khác nhau,…Và quan trọng nhất mình học được, đó là đâu là thời điểm thích hợp để dừng lại, bởi vì bạn có thể là người thắng trong cuộc đối đáp nhưng chưa chắc bạn đã thắng thật sự.
5
673477
2015-07-23 23:10:48
--------------------------
