491418
7881
Lúc đầu mình rất sợ học tiếng anh. Cứ nghĩ tới nó thôi là mình thấy sợ. Đang là sinh viên nên mình rất cần kiến thức ngoại ngữ, nhất là tiếng anh. Vậy là mình quyết tâm lên tiki tìm sách để học. Lúc đầu nhiều sách tiếng anh quá, nên mình hơi bị rối. May mắn là mình tìm thấy quyển này, ngữ pháp mình yếu nên cần bổ sung. Thế là mình quyết định mua nó. Mới nhận được lúc sáng là mình bắt tay vô học ngay. Sách hướng dẫn rất dễ hiểu, có ví dụ minh họa rõ ràng. Bổ trợ đầy đủ tất tần tật ngữ pháp trong tiếng anh luôn. Nên bạn nào tiếng anh còn yếu thì có thể mua về học lại. Cuốn sách này giúp ích cho các bạn rất nhiều đấy.

5
470308
2016-11-18 13:20:36
--------------------------
479151
7881
Sách khá dày và chất lượng giấy tốt, hướng dẫn dễ chịu, sách cầm khá nặng tay, kích thích nhỏ gọn, có thể mang theo mọi lúc. Hiện tại mua sách này để ôn lại kiến thức và dạy kèm cho em trai luôn. Nó thấy mh học cũng đòi học theo. Rất cảm ơn tiki. Hy vọng tiki có thể cũng cấp thêm sách hay và hữu ích cho mọi người, ngoại ngữ là 1 môn học khá thông dụng hiện nay, vì vậy rất cần thiết. Mong rằng sau khi đọc xong quyển sách này sẽ giúp đỡ tôi nhiều hơn trong công việc và cuộc sống !
4
1552807
2016-08-07 22:50:14
--------------------------
459357
7881
Cuốn sách viết về ngữ pháp tiếng anh , một trong những điều quan trọng khi học Tiếng Anh . Quyển sách thực sự bổ ích . Trình độ ngữ pháp Tiếng Anb của mình đã cải thiện đáng kể kể từ khi học cuốn này . Lúc trước kiểm ta trình độ mình là A2 , còn bây giờ là B2 . Thật sự rất tuyệt vời :")) Dù vậy , sách vẫn có những điểm khó hiểu , khó nuốt . Nhưng với mức giá này thì đây là một cuốn sách đáng mua ^^ Ngoài ra Tiki giao hàng nhanh , hàng đóng gói cẩn thận , sẽ còn mua ở đây dài
5
1088552
2016-06-26 02:31:20
--------------------------
449686
7881
Mình cực kì dỡ phần luận tiếng anh, sau khi tìm hiểu mấy đứa bạn thì được nó giới thiệu cho cuốn này, mình mượn đọc thử sau một thời gian thì thấy có tiếng bộ nên mình quyết định mua nó về, mình thấy bìa giấy cứng, chất lượng giấy tốt, bên trong có nhiều hình ảnh dễ học hơn nữa nhờ đọc nó mà bài luận của mình được cải thiện rất nhiều và hơn nữa vốn từ vựng của mình cũng từ đó mà được trau dồi bổ ích, mình thật sự rất thích cuốn sách này. Cảm ơn Tiki đã cho mình cơ hội tiếp xúc với một quyển sách hay như vậy!
4
1371468
2016-06-17 21:32:11
--------------------------
430721
7881
Cuốn sách này rất hay và chưa nhiều kiến thức bổ ích cho học sinh như kiến thức căn bản và nâng cao. Ngoài ra trong sách có giải thích rất kỹ nhiều điểm ngữ pháp khó giúp ta có thể tự học và không cảm thấy nhàm chán. Sách nhỏ gọn, ta có thể mang theo mọi lúc mọi nơi để đọc. Bìa ngoài của sách trông còn rất bắt mắt. Từ khi có cuốn sách này khả năng tiếng anh của em tiến bộ hơn rất nhiều. Qua đây em muốn cám ơn Tiki đã mang lại cho em một cuốn sách hay và bổ ích như thế!
4
986146
2016-05-15 19:37:37
--------------------------
413371
7881
Quyển sách này được viết rất hay, nội dung đầy đủ, kiến thức ngữ pháp phong phú, dễ hiểu, chất lượng sách tốt, bìa đẹp, giấy dày. nếu ai mới bắt đầu học tiếng anh, thì đây chính là một quyển sách hay với đầy đủ kiến thức giúp học tốt tiếng anh. Đúng với tên bìa sách "Học Nhanh Ngữ Pháp Tiếng Anh Từ A Đến Z". Bạn sẽ không hối hận khi bỏ ra 110.000đồng để mua quyển sách này. nó sẽ giúp bạn học tốt lên nhiều đấy. Sách dễ đọc, giúp mọi người mau nhớ, 110k quá hợp lí.
5
1212007
2016-04-09 16:11:37
--------------------------
401794
7881
Cuốn sách dày 462 trang, chất lượng sách tốt, bìa gấpvà đẹp, chất lượng in tốt.Giá bìa 110k cũng là hợp lí. 
Về nội dung : hiện nay trên thị trường có khá nhiều các đầu sách bề tự học ngữ pháp tiếng anh, tuy nhiên mình vẫn chọn mua quyển này và ưng ý vì đã mua nó.Kiến thức đầy đủ, không bị trùng lặp và được trình bày khoa học, dễ hiểu. 
Những chú ý quan trọng được đóng khung và in màu xanh rất bắt.mắt và giúp tăng hiệu quả ghi nhớ.
Cuốn sách nên mua và rất phù hợp với các bạn tự học.
4
1157982
2016-03-21 12:00:14
--------------------------
378010
7881
Với những người học Tiếng Anh như mình thì có trên tay một quyển ngữ pháp Tiếng Anh là cần thiết, và đây cũng chính là cuốn sách mà mình cần! <3
Nội dung đầy đủ, cách trình bày rõ ràng, dễ hiểu, lại còn có thêm những hình ảnh minh họa rất đẹp nữa. Giấy dày dặn, chữ vừa phải rất dễ nhìn, giá cả lại phù hợp. Mình rất hài lòng. :)))
Nếu bạn đang băn khoăn không biết nên chọn cuốn ngữ pháp Tiếng Anh nào trong rất nhiều sách ngữ pháp trên thị trường thì nên chọn cuốn này!
4
890754
2016-02-04 01:39:33
--------------------------
359893
7881
Một quyển sách trình bày ngắn gọn nhưng cực hiệu quả với những bài hướng dẫn kèm ví dụ minh họa. Ngữ pháp trong tiếng Anh thực sự rất cần thiết nhưng nếu cầm lên một quyển sách mà toàn chữ là chữ, cộng với một màu đen từ đầu đến cuối sẽ không thể nào tạo cảm hứng được cho người học. Nhưng quyển sách này thì khác. Nó có nội dung logic, dễ hiểu, màu chữ đẹp và cách trình bày thoáng, quả thật rất hữu ích cho những người mất căn bản về ngữ pháp và họ có thể lấy lại niềm cảm hứng khi cầm trên tay quyển sách này. Hay, ngắn gọn, dễ hiểu, đáng tiền, đó là những gì quyển sách mang lại cho người đọc.
5
274995
2015-12-28 10:36:47
--------------------------
351220
7881
Tập sách nhằm cung cấp những kiến thức về danh từ, tính từ, trạng từ, ... và mang đến những tri thức cơ bản, hiện đại giúp nâng cao một cách vững chắc trình độ ngữ pháp tiếng Anh của mình. Cuốn sách tập trung đi sâu vào khai thác nội dung của hầu hết tất cả các điểm ngữ pháp trọng tâm - cơ bản, đặc biệt được trình bày kỹ lưỡng, giải thích cặn kẽ cách dùng và quy luật người học cần nắm vững theo nguyên tắc giảng giải từng bước với các ví dụ minh hoạ, đồng thời cũng mang tính đa dạng, phong phú để tạo cho sinh viên hứng thú trong việc học tập.
5
939854
2015-12-11 21:33:09
--------------------------
347522
7881
Lúc đầu mình rất sợ học tiếng anh. Cứ nghĩ tới nó thôi là mình thấy sợ. Đang là sinh viên nên mình rất cần kiến thức ngoại ngữ, nhất là tiếng anh. Vậy là mình quyết tâm lên tiki tìm sách để học. Lúc đầu nhiều sách tiếng anh quá, nên mình hơi bị rối. May mắn là mình tìm thấy quyển này, ngữ pháp mình yếu nên cần bổ sung. Thế là mình quyết định mua nó. Mới nhận được lúc sáng là mình bắt tay vô học ngay. Sách hướng dẫn rất dễ hiểu, có ví dụ minh họa rõ ràng. Bổ trợ đầy đủ tất tần tật ngữ pháp trong tiếng anh luôn. Nên bạn nào tiếng anh còn yếu thì có thể mua về học lại. Cuốn sách này giúp ích cho các bạn rất nhiều đấy.
4
119935
2015-12-04 16:21:35
--------------------------
310820
7881
Mình đã mua tới hai quyển Học nhanh ngữ pháp từ A đến Z, một quyển cho mình và một quyển tặng cho đứa bạn. Trước khi mua, mình đã vào xem nhận xét của các bạn để quyết định mua hay không. Và giờ đây, khi được cầm trên tay quyển sách này, mình cực kì ưng ý. Đầu tiên, mình khá ấn tượng về bìa và chất  lượng giấy. Giấy phải nói là cực kì trắng, cực kì dày. Tiếp đến đó chính là cách trình bày của nội dung trong sách. Cách trình bày logic, dễ hiểu, có kèm thêm cả hình ảnh minh họa giúp mình không phải rối bù trong cả đống chữ nặng nề như trước... Đây thật sự là một cuốn sách hữu ích!
5
339927
2015-09-19 20:01:04
--------------------------
300348
7881
Mình mua cuốn sách này để dạy cho một bé lớp 8 lấy lại căn bản tiếng Anh. Cuốn sách rất là có ích vì nó được soạn rất là chi tiết và bắt đầu từ các kiến thức căn bản cho người mới học tiếng Anh luôn. Sách có kết cấu khá logic, chữ được in 2 màu đen và xanh nên nhìn vào không bị rối mắt. Tập sách lại có hình minh họa nữa. Một cuốn sách rất bổ ích dành cho ai mới học hoặc muốn lấy lại căn bản tiếng Anh. Sách của mình còn được TIKI bọc sách bookcare rất cẩn thận nữa. Cực thích luôn á :)
5
671746
2015-09-13 21:39:05
--------------------------
286898
7881
Mình mua cuốn sách này để dạy cho một bé lớp 8 lấy lại căn bản tiếng Anh. Cuốn sách rất là có ích vì nó được soạn rất là chi tiết và bắt đầu từ các kiến thức căn bản cho người mới học tiếng Anh luôn. Sách có kết cấu khá logic, chữ được in 2 màu đen và xanh nên nhìn vào không bị rối mắt. Tập sách lại có hình minh họa nữa.
Một cuốn sách rất bổ ích dành cho ai mới học hoặc muốn lấy lại căn bản tiếng Anh.
Sách của mình còn được TIKI bọc sách bookcare rất cẩn thận nữa. Cực thích luôn á :)
5
197647
2015-09-02 07:16:26
--------------------------
260395
7881
Hôm trước mình có đi nhà sách thấy Học Nhanh Ngữ Pháp Tiếng Anh Từ A Đến Z là mình mua ngay. Bìa sách đẹp và bắt mắt, giấy thì dày và tốt, chữ in đẹp nhưng điều mình không thích ở chỗ là chữ in màu xanh ( nhưng cũng không quan trọng lắm). Về nội dung thì dễ hiểu, tổng hợp được nhiều kiến thức cần thiết và bổ ích. Rồi mình lên mạng tìm thông tin về quyển sách này mới trên tiki có bán nhiều loại sách hay và rẻ nữa nên từ đó mình luôn mua đồ trên tiki.
5
704900
2015-08-10 13:02:43
--------------------------
239453
7881
Mình thấy sách này lượng kiến thức phong phú. Từ danh từ, tính từ, trạng từ,... tất cả đều nằm gọn trong cuốn sách ma thuật này. Cách trình bày đẹp mắt, phông chữ khá ổn không to cũng không nhỏ quá, rất dễ nhìn. Cuốn sách giải thích chi tiết giúp người dùng vô cùng hứng thú và không hề nhàm chán khi dùng sách. Đúng cái tên ngoài sách, học nhanh ngữ pháp tiếng anh từ a đến z, sách rất dễ hiểu làm cho người đọc mau nhớ. Giá cả khỏi miễn chê luôn nhá. Luôn yêu thích em này.
5
98754
2015-07-23 21:45:50
--------------------------
232140
7881
Học nhanh ngữ pháp tiếng anh  từ a đên z là một quyển sách khá hay cho những người muốn trau dồi thêm kiến thức tiếng anh cũng như mới bắt dầu học English.Với kiến thức ngữ pháp sâu rộng trải dài được trình bày từng phần thích hợp ,giúp cho người học dễ dàng hơn,Cuốn sách nhỏ gọn ,bìa giấy cứng thơm .Tuy nhiên sách nên bổ sung thêm phần hình ảnh để làm phong phú bài giảng và bổ sung thêm phần Câu điều kiện thì cuốn sách này đích thực là 1 cuốn bách khoa toàn thư về môn Tiếng anh .
5
672261
2015-07-18 16:55:25
--------------------------
224252
7881
Đối với ngữ pháp tiếng Anh thì hiện nay trên thị trường quả thật có rất nhiều,nhưng không phải sách nào cũng hay, vì các sách ngữ pháp thường có sự trùng lặp rất lớn.Tuy nhiên quyển sách này hoàn toàn không có sự trùng lặp đó.Cũng đi từ các thể thì căn bản và các hình thức của từ nhưng sách lại được trình bày theo một phong cách mới,không theo các"mô tuýp" sách ngữ pháp khác.Với nội dung khá lớn nhưng được trình bày cô đọng và dễ hiểu,sách rất thích hợp cho các bạn tự học.Bìa sách đẹp, chất lượng giấy thì khỏi  phải bàn.Đây quả thật là 1  quyển sách hay từ nội dung cho đến hình thức.
5
368991
2015-07-08 12:48:03
--------------------------
223899
7881
Đây là cuốn sách tổng hợp toàn bộ các nội dung kiến thức của tiếng anh đúng như tên gọi của nó.Các chuyên đề về từ loại , các mảng cấu trúc ngữ pháp từ lớn tới nhỏ đều được trình bày một cách đầy đủ chi tiết và kĩ lưỡng giúp mình có thể hiểu sâu hiểu rộng về các vấn đề ngữ pháp mình còn thắc mắc và băn khoăn.Chưa kể cuốn sách nhìn cũng đẹp và mùi giấy cũng khá thơm.Nó tạo cho mình một hứng thú khi học các bài học tưởng chừng như khô khan.Thật hay và hữu ích.
5
578865
2015-07-07 19:37:58
--------------------------
