300754
8430
Mình chưa bao giờ đọc tác phẩm của nhà văn Đoàn Thạch Biền,mình mua cuốn này vì một lần lượn lờ trên tiki tình cờ thấy nên mua,nghe cái tên khá ấn tượng ,bìa sách cũng đẹp nên đã mua,nhưng nội dung đối với tôi khá hay,một cuốn sách thật sự sâu lắng,tôi đã đọc rất nhiều truyện ngắn tản văn,nhưng đây là cuốn sách tản văn rất khác biệt với những cuốn tản văn khác mà tôi từng đọc,các bạn nên đọc cuốn sách này và cảm thấy rất mới mẻ!Tôi mong nhà văn sẽ có thật nhiều tác phẩm hay!
4
413637
2015-09-14 10:21:17
--------------------------
255434
8430
Nhà văn Đoàn Thạch Biền dĩ nhiên cũng có viết truyện ngắn và tản văn, nhưng có vẻ như truyện của ông không hợp xu hướng bây giờ và khá nhiều bạn trẻ không thích ứng được cách ông hành văn. Cách viết văn của ông không trau chuốt, bóng bẩy, không nhiều câu gút gây sốc mà ông viết theo kiểu đơn giản hơn, thông minh, tinh tế, đôi khi pha 1 chút hài hước khiến ta mỉm cười nhẹ nhàng, nhưng cũng đầy ẩn ý và triết lý nếu ta đủ khả năng cảm nhận. Một nét duyên ngầm khó mà tìm được.
Bìa sách đẹp, lãng mạn cùng 1 tựa đề gây sốc vừa đủ để bạn thích thú và tò mò để tìm mua và đọc quyển truyện của ông. 1 chiêu trò mà chắc chỉ ông có thể đặt được cho truyện ngắn cũng như quyển sách cùng tên của mình. Nếu còn lăn tăn, bạn có thể đọc thử truyện ngắn này và bạn sẽ thấy mình nói không ngoa :)).
5
41744
2015-08-06 00:59:08
--------------------------
142539
8430
Có người nhận xét truyện của nhà văn Đoàn Thạch Biền đối thoại nhiều quá, truyện ngắn như thế thì không hay. Tôi thì ngược lại, đã trót yêu kiểu viết văn thông minh, có duyên ngầm, đã xem nhất định phải cười mím chi của nhà văn, nó khác với kiểu viết phải cười khúc khích của nhà văn Nguyễn Nhật Ánh. Tôi đọc truyện của nhà văn ĐTB từ năm 20 tuổi, đến bây giờ mái tóc tôi đã pha sương. Trên 30 năm đọc đi đọc lại truyện của nhà văn vẫn không thấy nhàm chán, đôi khi còn thấy mình trẻ theo nhân vật nữa chứ.Trong các truyện của nhà văn, viết đơn độc và lạ nhất có  lẽ là truyện " Nhà tiên tri ảo". Truyện có tình tiết gây cấn, ma quái, hấp dẫn, người đọc không đoán được khúc sau như thế nào, vì thế mà cực kỳ lôi cuốn .Rất tiếc nhà văn không sáng tác thêm cho loại truyện này.
5
386734
2014-12-18 23:07:06
--------------------------
