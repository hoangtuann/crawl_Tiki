464186
10625
Nếu đọc để tìm ý tưởng kinh doanh thì có lẽ không thích hợp vì hơi chung chung. Nhưng đọc để lấy cảm hứng thì bạn chọn đúng sách rồi đấy. Cuốn sách đưa ra 100 ý tưởng bằng những cau chuyện nhỏ. Và hạn chế là, bạn biết đấy, nói thì dễ, làm mới khó. Nhưng điều quan trọng vẫn nằm ở bản thân bạn. Đọc - chọn lọc - học - áp dụng. Tác giả đã truyền tải rằng ai cũng có thể làm giàu. Nhược điểm duy nhất của sách là mình có cảm giác gò bó ở con số 100 hơi khiên cướng.
4
54635
2016-06-29 23:17:05
--------------------------
426487
10625
Khởi nghiệp thật sự là điều không dễ dàng gì vì có quá nhiều yếu tố chi phối nó. Và nó là một điều rất khó khăn đối với người trẻ vì chưa có nhiều kinh nghiệm trong kinh doanh. Những kiến thức hàn lâm trên trường lớp không thể áp dụng hết được trong cuộc sống, chính vì vậy mà mới thực sự cần trải nghiệm, học hỏi những người đi trước. Cuốn 100 Ý Tưởng Kinh Doanh Tuyệt Hay sẽ giúp ngắn phần nào khoảng cách đến thành công trong kinh doanh. Sách cung cấp rất nhiều kiến thức có giá trị, cách hành văn của tác giả cũng rất dễ hiểu ! Thực sự đây là cuốn sách hay 
5
1295734
2016-05-06 23:30:41
--------------------------
417796
10625
Mình là dân ngoại bang, không học về lĩnh vực kinh doanh nhưng một lần tình cờ cùng giúp những người bạn học ngành này nên mình hiểu thêm chút xíu về lĩnh vực kinh doanh. Ban đầu gia nhập vào lĩnh vực này cũng hơi bở ngỡ nhưng khi mình tìm đến quyển một trăm ý tưởng kinh doanh tuyệt hay này làm mình thay đổi những suy nghĩ mơ hồ về thế giới kinh doanh vốn không phải địa phận của mình. Sách đưa ra những ý tưởng hay và độc đáo cung cấp cho người đọc nhiều tư liệu cũng như nhiều kiến thúc bổ ích,
4
509425
2016-04-17 20:55:16
--------------------------
319992
10625
Chất lượng sách khá tốt. Được Tiki gói bọc và vận chuyển rất cẩn thận.
Một cuốn sách với những ý tưởng được xây dựng bằng những mẩu chuyển nhỏ, đưa ra những ví dụ cụ thể kết hợp với những lời khuyên xây dựng ý tưởng từ người viết.
Cuốn sách đọc dễ hiểu nhưng để có thể lắng đọng cần có một chút trải nghiệm về công việc kinh doanh vì cuốn sách cung cấp những kiến thức nền tảng. Khi đó, bạn sẽ nhận ra những thứ mình đang thiếu hụt. Còn đối với những người đang tìm kiếm, lên kế hoạch kinh doanh thì sẽ có cảm giác hơi xa vời và chung chung. 
3
651857
2015-10-10 09:30:37
--------------------------
296818
10625
Cuốn sách là một kho tổng hợp những ý tưởng mới tuyệt vời theo nhiều cách và tùy từng thởi điểm, nó truyền cho tôi rất nhiều cảm hứng. Bạn sẽ thấy một loại keo dán được cho là vô dụng lại đem về món tiền kếch xù khi nó biến thành giấy dán văn phòng như thế nào, và còn rất nhiều những ý tưởng thú vị khác, đơn giản nhưng tuyệt hay mà tôi chẳng thể ngờ tới.
Chắc chắn quyển sách sẽ đem đến cho bạn một cảm giác mới nếu bạn còn đang băn khoăn với những ý tưởng khởi nghiệp hoặc kinh doanh của mình.

5
385695
2015-09-11 12:25:34
--------------------------
292151
10625
Mình là người đam mê kinh doanh vì vậy rất thích đọc thể loại sách kinh doanh - làm giàu. Cuốn sách này rất khác biệt, tác giả cho người đọc thấy được cơ hội làm ăn - kinh doanh luôn hiện hữu xung quanh bạn, mỗi ngày bạn đều thấy được hàng loạt cơ hội để "hái ra tiền" một cách đơn giản nhưng bạn đã quá vô tâm mà phung phí cơ hội. Qua tác phẩm của mình, Jeremy Kourdi muốn nhắc nhở chúng ta rằng "ai cũng có thể làm giàu nhờ kinh doanh nếu như chúng ta nắm bắt được cơ hội và vạch ra được kế hoạch cụ thể, cùng với đó là phải có niềm đam mê mãnh liệt, nội dung sách trình bày rất cụ thể, chi tiết, chất liệu giấy sậm màu làm cho mắt dễ chịu khi đọc, ngoài ra sách của mình còn được bọc plastic. Xin cảm ơn tác giả và cảm ơn Tiki rất nhiều!
5
644564
2015-09-07 03:11:03
--------------------------
260666
10625
Nằm trong bộ sách 100 Ý Tưởng ,100 Ý Tưởng Kinh Doanh Tuyệt Hay là quyển sách vô cùng bổ ích và thú vị , ngoài những ý tưởng về quản trị như quản trị nguồn nhân lực , quản trị sự đổi mói , nó còn có những tư duy vô cùng mới và độc đáo .
Bạn nên đọc hết quyển sách và chọn ra cho mình những ý tưởng bạn tâm đắc nhất , thực tế nhất với bản thân và môi trường của bạn .
Mình thấy quyển sách này khá  hay và bổ ích cho mọi người .
5
554150
2015-08-10 16:18:15
--------------------------
216560
10625
Mình tin rằng bất cứ ai cũng có một phần mong muốn làm chủ, nhưng sẽ vì nhiều lý do mà chúng ta lại không đạt được ước mơ của mình. Trong đấy, lý do được nhiều người nhắc đến nhất là không có ý tưởng. Thật ra, ý tưởng luôn ở quanh chúng ta, nhưng chỉ vì ngọn lửa kinh doanh chưa bùng cháy mạnh mẽ nên chúng ta lại không thể đạt được ước mơ của mình.

Quyển sách này muốn nói về việc thực ra bạn không cần ý nghĩ quá cao siêu, chỉ cần là những ý tưởng vô cùng bình thường, nhưng nếu bạn có thể kết hợp với niềm đam mê vô hạn của mình, bạn có thể biến nó thành một ngọn lửa bùng cháy mạnh mẽ đến mức bạn không thể ngờ được. Đây sẽ là một quyển sách giữ lửa kinh doanh tuyệt vời cho bạn trong quá trình xây dựng ước mơ của mình.
5
140922
2015-06-27 22:17:24
--------------------------
12315
10625
Khởi sự và phát triển thành công một doanh nghiệp là bài toán đau đầu đối với rất nhiều doanh nhân. Thực tế là không ít người đã thất bại. Song, cũng rất nhiều người đã đạt được những thành công vang dội, nhờ xác định được và biết chú trọng những nhân tố làm nền tảng cho sự tăng trưởng bền vững của các doanh nghiệp mà họ làm chủ. Câu hỏi đặt ra ở đây là những nhân tố nào làm cho các nhà kinh doanh có được thành công như vậy? 

Cuốn sách “100 ý tưởng kinh doanh tuyệt hay” với lời văn cô đọng, rõ ràng cho từng ý tưởng, dễ hiểu với người đọc sẽ là chiếc cầu nối để cho người đọc sẽ trở thành nhà doanh nhân, nhà quản lý giỏi, đồng thời cũng dễ nắm bắt được nhưng cơ hội kinh doanh nhanh chóng hơn. Chẳng hạn như: để thành công, việc đầu tiên trước khi bắt đầu khởi sự một doanh nghiệp là phải lập một kế hoạch kinh doanh hoàn chỉnh. Từ khâu phát triển một ý tưởng kinh doanh, nghiên cứu thị trường và tính khả thi của ý tưởng đó, đến việc quản lý các hoạt động kinh doanh hàng ngày,…
5
12462
2011-10-05 21:43:55
--------------------------
