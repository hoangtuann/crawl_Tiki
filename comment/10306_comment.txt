243409
10306
Ai cũng luôn dằn vặt bản thân khi phải đắm chìm trong những khổ đau do chính mình hay do người khác đem lại. Lẽ thường tình ở đời, có được thì cũng sẽ có mất đi. Quan trọng là cách ta đối diện với nó có khôn ngoan hay không, hay cũng chỉ yếu đuối than khóc và gây hệ lụy đến nhiều người hơn nữa. Cuốn sách giúp chúng ta học cách buông bỏ và chấp nhận cái lẽ được mất này như thế nào. Hơn thế nữa, chính nó sẽ giúp chúng ta xua tan đi những buồn phiền mà không đáng phải có.
5
16510
2015-07-27 14:05:34
--------------------------
130630
10306
Trong đời người ai lại không có được và có mất, điều quan trọng là chúng ta nhìn sự việc đó như thế nào? Ai cũng từng trải qua những biến cố của cuộc đời và ai từng phải bước qua những nỗi đau mất mát, liệu bạn sẽ nhìn nó ở con mắt như thế nào mới là điều quan trọng. Cuốn sách này sẽ nói cho chúng ta biết, chúng ta cần làm gì và phải đối mắt như thế nào những thứ chúng ta được và mất
" Khi bạn mất đi một kho báu quý giá thì bạn sẽ nhận lại được một kho báu báo khác quý giá hơn không kém điều bạn đã đánh mât" tuổi trẻ, tình yêu, sự nghiệp là ba thứ luôn quan trọng với con người nhưng khi chúng ta đã đánh mất nó thì chúng ta sẽ phải làm sao? Cuốn sách này sẽ khơi sáng cho bạn nên nhìn những việc đó như thế nào
5
398689
2014-10-19 09:38:40
--------------------------
