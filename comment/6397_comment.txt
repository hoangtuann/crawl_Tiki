560841
6397
Sản phẩm chữ in cực kỳ mờ, mong công ty sẽ cải thiện hơn
2
5124794
2017-04-01 06:22:31
--------------------------
542733
6397
Mình đã mua cuốn sách này tuần trước, ưu điểm là giá rẻ hơn so với thị trường bên ngoài, nội dung cũng bám sát chương trình tiếng Nhật, giảng giải dễ hiểu, phù hợp với mọi đối tượng tự học, hoặc du học sinh. Nhưng nhược điểm là giấy in bị nhoè, thậm chí mất chữ không thể đọc được. Hi vọng Tiki có thể kiểm tra sản phẩm trước khi giao hàng cho khách. Cám ơn!!!
3
13536
2017-03-15 09:24:50
--------------------------
536409
6397
Mình đặt mua sách này so với quyển 1 của NXB Từ Điển Bách Khoa thì quyển này giấy không tốt bằng, chữ in lại nhỏ (nhìn không rõ nhất là các chữ Kanji), thỉnh thoảng có trang bị mờ. Nói chung có cảm giác như đang dùng sách in lậu vậy.
3
1101238
2017-03-06 13:50:55
--------------------------
496570
6397
Mình khá hài lòng  với sách cả về hình thức, chất lượng và giá cả
4
1994104
2016-12-19 14:14:28
--------------------------
493751
6397
Truớc đó mình cũng đã từng mua sách ở tiki.vn rùi. Sách đẹp đuợc bọc bookcare cẩn thận. Lần này giao khá nhanh truớc cả thời giạn dự kiến 1 ngày. Nhưng chất luợng quyển này mình k hài lòng lắm. Do rất tin tuởng tiki nên không kiểm tra. Sách lần này bị mất chữ, có chứ mất hết nên khá là khó khăn cho nguời đang học như mình. Vì mình sẽ phải tìm hiểu lại trên mạng thay vì đọc dk trong sách vì mất thông tin. (Mất từ mới). Mong là tiki sẽ chú ý hơn trong sản phẩm của mình. Còn lại thì mình khá hài lòng. Thân gửi.
2
1623331
2016-12-02 12:26:16
--------------------------
477457
6397
Dành cho những người mới học tiếng nhật và làm quen với những mẫu câu đơn giản nhưng lại dùng khá nhiều trong giao tiếp của người Nhật. Mình thích nhất đó là hệ thống từ vựng của cuốn sách khá phong phú giúp mình mở rộng vốn tiếng nhật rất nhiều. Ngoài ra thì phần giải thích mẫu câu rất chi tiết và tỉ mỉ giúp người đọc hiểu rõ từng mẫu ngữ pháp. Phần mở rộng trong sách đều là những văn cảnh trong cuộc sống và cung cấp khá nhiều kiến thức cũng như cách nói của người Nhật.
5
646922
2016-07-27 08:22:24
--------------------------
443128
6397
Chỗ mình ở có một Thầy giáo người Nhật Bản, Thầy dạy miễn phí để giao lưu văn hóa Nhật Bản, mình đi học trễ 1 tháng nên phải mua sách ngay. Mà không phải trung tâm nên không bán sách, phải tự tìm mua. Bình thường thấy Minna No Nihongo trên Tiki chạy lắm, lúc nào cũng "Hết hàng", không ngờ hôm đó trúng đợt khuyến mãi, lại có hàng, vậy là ôm luôn cuốn 1 và cuốn 2 luôn cho lẹ. Giáo trình hay, có hình ảnh minh họa, đặc biệt là bài nghe. Cuốn này là bài 26-50, học xong thi N4 được. Nhưng mình thấy phải làm thêm bài tập, luyện giải đề nhiều mới thi được, chứ học xong cuốn 2 này vẫn còn thiếu lắm.
4
1091973
2016-06-06 11:46:20
--------------------------
381066
6397
đây là cuốn sách khác hay và dễ hiểu... nó không chỉ cung cấp 1 lượng từ vựng dồi dào mà còn có cả ngữ pháp đc viết rất chi tiết... rất hay.... sau khi học xong 25 bài ở quyển 1... và hiện tại thì mình đang học quyển 2 ...nhờ có cuốn sách này mà h đây mình có đã có 1 vốn kiến thức về tiếng nhật khá vững chắc.... và mình rất muốn nói lời cảm ơn đến tiki.... 
Tiki book hiện nay ko chỉ là nơi cung cấp khá nhiều sách nhật ngữ mà còn là nơi có giá cả khá rẻ... so với mặt bằng chung của cả nước.
5
1027720
2016-02-16 15:00:14
--------------------------
364297
6397
đây là 1 tài liệu hữu ích giúp quá trình đọc sách giáo khoa Honsatsu Minna dễ dàng hơn, nhờ vào  danh sách từ mới và giải thích ngữ pháp dễ hiểu, rõ ràng. Sách đợt mình mua được giảm 29% nhưng chất lượng không tốt lắm, các chữ trong sách in không thiếu nét, tuy nhiên có vẻ hơi mờ và giấy in thì làm mình cảm giác đây là sách photo lại hoặc sách lậu. Nhưng đây cũng là 1 ưu điểm vì cuốn Minna I mình mua ở tiệm mặc dù giấy rất đẹp, nhưng chính việc in đậm đã khiến cho các nét của chữ Hán bị rối vào nhau không thể nhìn được. Với cuốn sách này, tuy chữ hơi mờ và giấy không đẹp lắm nhưng các nét rõ ràng dễ nhìn giúp việc học Kanji tiện hơn. Hi vọng sắp tới Tiki có thêm cuốn Shin nihongo trình độ trung cấp.
4
365004
2016-01-05 22:09:34
--------------------------
279816
6397
Bạn đã mua và trải nghiệm cuốn Minna No Nihongo (Tập 1) và cả cuốn bản dịch của nó thì tại sao lại không nhanh tay sở hữu ngay cuốn Minna no Nihongo tập 2 này. Cuốn sách này thực sự rất hữu ích! Nó sẽ cung cấp cho bạn những điều mà bạn cần biết về tiếng Nhật và còn hữu ích hơn nữa trong mảng giao tiếp thông qua các đoạn hội thoại đi từ đơn giản đến phức tạp! Chưa hết bạn sẽ trau dồi được cả kĩ năng nghe nữa. Sách biên dịch, viết bằng tiếng Việt nên khá là dễ học. Mình cũng rất ấn tượng với phần phụ lục ở cuối sách rất hay và vô cùng bổ ích. N4 đang đợi bạn. Hãy mua ngay và trải nghiệm. P/s: Giá ở Tiki book là mềm nhất đó nghen! Nhật ngữ tiến lên!
5
760846
2015-08-27 09:53:26
--------------------------
237802
6397
Đây là phần II của bộ sách luyện tiếng Nhật sơ cấp nổi tiếng. Qua được 25 bài ở cuốn I, bạn đã tương đương trình độ N5 và học tiếp cuốn II này để đạt được N4. Tuy nhiên, theo kinh nghiệm của mình, N5 hoàn toàn đạt được nếu hoàn thành Minna no Nihongo I nhưng N4 thì cần đọc thêm các sách bổ trợ khác. Có thể tham khảo cuốn Tổng hợp bài tập chủ điểm Minna No Nihongo II mà Tiki đang bán (nhưng hơi ít bài tập luyện nghe) hoặc tài liệu của giáo viên/người hướng dẫn của bạn.

- Về hình thức sách: bìa mềm, chắc chắn, khổ to A4, giấy trắng, in sắc nét, không có lỗi chính tả.

- Về giá cả Tiki: hiện tại tốt nhất. 
5
66139
2015-07-22 19:27:41
--------------------------
229617
6397
Mình đang tìm mua quyển sách bài tập II tiếng Nhật thì không tìm thấy nên mua thử quyển này về. Vì là sách giải thích ngữ pháp bằng Tiếng Việt nên đọc dễ hiểu, từ vựng đầy đủ, giải thích rõ ràng có ví dụ. Quan trọng là cuối sách còn có trang mục lục về trợ từ, thể động từ, các ngữ pháp có viết kèm cấu trúc và có bảng tự động từ cùng với tha động từ... Mình mừng hết biết, thật sự cám ơn tiki vì quyển sách này và mong tiki sẽ bổ sung sách bài tập Tiếng Nhật cũng như các tài liệu luyện nghe tốt như vậy.
4
261627
2015-07-16 22:09:39
--------------------------
