303859
5142
Yêu thích Nam Phái Tam Thúc từ bộ Đạo Mộ Bút Ký, nên khi thấy bộ Đại Mạc Thương Lang này mình đã mua ngay. Có vẻ như nhược điểm của Nam Phái Tam Thúc là khởi đầu thường không gay cấn và hấp dẫn cho lắm, giống trường hợp của Đạo Mộ Bút Ký, ban đầu hơi đơn giản và dễ đoán nhưng càng đọc thì lại càng thích thú và lôi cuốn. Cốt truyện hay, các tình tiết xây dựng hợp lý, từng bí ẩn dần được hé lộ, khiến chúng ta cứ chầm chậm bị cuốn vào mạch truyện, không biết chuyện gì sẽ xảy ra. Một quyển sách đạo mộ đầy huyền bí và hấp dẫn.
5
41744
2015-09-16 00:35:25
--------------------------
296858
5142
Mình yêu thích tác giả Nam Phái Tam Thúc từ khi đọc series Đạo mộ bút ký nên đã mua cuốn sách này. Theo cảm nhận của mình thì Đại mạc thương lang không hấp dẫn bằng Đạo mộ bút ký nhưng vẫn có nét đặc sắc riêng. Cách xây dựng tình tiết truyện khá hợp lý, tính cách nhân vật thu hút. Cuộc phiêu lưu của các nhân vật rất hồi hộp, cuốn hút trong bối cảnh rừng rậm, vực sâu, diễn biến bất ngờ cùng nhiều yếu tố huyền bí. Chất lượng sách in tốt, bìa đẹp, chất giấy dễ đọc.
4
177708
2015-09-11 12:48:00
--------------------------
285609
5142
Khi thấy tác giả là Nam Phái Tam Thúc thì mình quyết định phải mua ngay. Vì trước đó đọc Đạo mộ bút ký đã rất khâm phục trí tưởng tượng tuyệt vời của tác giả và lần này cũng vậy.Mở đầu truyện thì mình vẫn chưa thấy cuốn hút lắm. Nhưng càng về sau thì sự xuất hiện của nhưng nhân vật bí ẩn càng làm mình tò mò và hăng say đọc. Đến khi đọc đến những chương cuối thì thật sự làm mình yêu thích vô cùng. Ngoài nhưng yếu tố kì bí thường thấy ở thể loại truyện đạo mộ, lần này có có thêm mối tình của nhân vật chính nữa khiến mình thấy rất thú vị. Mong chờ những tác phẩm tiếp theo của Nam Phái Tam Thúc :)
4
111393
2015-08-31 23:32:39
--------------------------
265571
5142
Đây là bộ truyện rất hay của Nam Phái Tam Thúc mà mình đã đọc. Giọng văn giản dị mà hấp dẫn có thể là do tính tự sự của nó.. Mạch truyện vừa phải rất cuốn hút, tình tiết trong truyện được sắp xếp bố cục tốt, đẫn dắt lôi cuốn khiến mình khó mà dừng lại được.Truyện đọc cần để ý kỹ tình tiết tránh bỏ sót gây ra khó hiểu. Chất liệu giấy vàng đọc khá dễ chịu cho mắt, bìa cũng khá đẹp đối với mình. Tóm lại đây là truyện đáng đọc với những ai thích thể loại phiêu lưu bí hiểm
4
386817
2015-08-14 01:19:48
--------------------------
260659
5142
Đây là bộ truyện thứ 2 của tác giả Nam Phái Tam Thúc mà tôi đã đọc. Tuy tôi thích Đạo mộ bút ký hơn nhưng Đại mạc thương lang vẫn được tôi đánh giá là hấp dẫn. Truyện có tình tiết li kỳ, kịch tính nối tiếp kịch tính, rùng rợn nối tiếp rùng rợn. Người đọc k thể rời mắt khỏi từng trang sách và càng tò mò muốn biết kết cục câu chuyện sẽ ra sao. Cộng với bộ này có bìa đẹp, chất lượng in khá tốt chữ k bị nhòe. tóm lại nếu bạn thích phiêu lưu mạo hiểm thì có thể đọc cuốn này! 
5
626285
2015-08-10 16:12:01
--------------------------
171680
5142
Đây là một tác phẩm không dành cho những người yếu bóng vía. Nó là một tác phẩm vô cùng vô cùng rùng rợn. Tác phẩm này khiến chúng ta như trở thành một phần trong đó, sợ hãi vô cùng nhưng cũng không thể rời mắt dù chỉ một giấy. Nó khiến chúng ta luôn phải tự hỏi: tiếp theo là chuyện gì? đằng sau đó là gì? liệu có thể thoát không? Nhưng có lẽ đó là số phận, một số phận đau đớn, đáng sợ đã buộc nhân vật "tôi" và đòan thám hiểm vào với nhau, trong một chuyến đi chết chóc, rùng rợn.
Đây là một tác phẩm chỉ dành cho những ai có một mạnh mẽ , không sợ những cảnh rùng rợn đến dựng tóc gáy thui, thật đó, tin tui đi.
5
525123
2015-03-22 12:39:24
--------------------------
149862
5142
Đây là câu chuyện theo tôi là kén người đọc, tôi đã phải đọc kĩ 2 lần bộ truyện này thì mới ngấm được nội dung tuyệt vời của câu truyện. Tin tôi đi, đây là "cực phẩm của cực phẩm", theo dòng mạch truyện mà tôi nổi da gà, đầu óc tôi căng như dây đàn (kể cả đọc lần thứ  2 lại khi lấn dần theo mạch truyện.
      Ở đây, trong câu truyện, số phận đã vạch ra cho nhân vật "tôi" và toàn bộ đoàn thám hiểm, những tình huống lý kì, nguy hiểm xảy ra phần nào liên quan đến số phận đó. "Tất nhiên sẽ dẫn đến tất nhiên", nghe có vẻ đúng đắn nhưng khó hiểu phải không? Bạn hãy đọc cuốn tiểu thuyết này đi, câu nói trên sẽ chứng thực cho tất cả :)
5
361752
2015-01-15 01:14:07
--------------------------
