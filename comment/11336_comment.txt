454445
11336
Một vài nhận xét sau khi đọc"
- Đúng/ sai là 2 chân trời đối lập, ta không thể đứng 1 chân bên này và 1 chân bên kia, đó chính là cách dẫn dắt của Michael Sandel.
- Hãy đọc và suy nghĩ, từ đó rút ra được bài học gì là do ở chính bạn.
- Cuốn sách đọc dễ hiểu nhờ sự xâu chuỗi của tác giả. Bạn sẽ nhìn thấy những lý do để hình thành lên kết quả, và cả những mâu thuẫn trong nó.
Đây quả là 1 kho tàng biện luận, với cách suy luận logic của Michael Sande, ông đã tạo nên một tác phẩm xuất sắc. Đây quả là 1 cuốn sách không chỉ để tham khảo, rất nên đọc và áp dụng nó trong việc xây dựng lên bản thân chính chúng ta,
5
744749
2016-06-21 22:38:32
--------------------------
435252
11336
Cuốn này tác giả viết theo kiểu nêu ra các vấn đề, tình huống cụ thể, quan điểm của các bên đối lập và cả quan điểm của tác giả, sau đó người đọc tự đúc kết và suy ngẫm. 
Các tình huống thì đa dạng nhưng không phải cái nào cũng phù hợp với văn hóa tiền bạc và quan điểm của Việt Nam, nhưng nhờ vậy ta có cái nhìn rộng mở hơn. Đọc cuốn này cũng có thêm kiến thức và tư duy của kinh tế thị trường, với một người không có nhiều kiến thức kinh tế như mình thì cuốn này rất bổ ích. Sách này có nhiều đoạn mình đọc lướt do hơi nặng đầu, rồi sau đó đọc lại thì thấy rõ ràng hơn. 
3
141646
2016-05-24 20:06:52
--------------------------
399243
11336
Tiêu đề quyển sách có thể gây hiểu nhầm đối với những độc giả hiếu kỳ cũng như những người chỉ lướt quá. Có thứ gì mà tiền không thể mua được trên thế gian này chứ. Chương đầu dẫn nhập đem lại cho người đọc rất nhiều ví dụ về tiền có thể mua được gần như mọi thứ (từ quyền sinh con, quyền săn bắn tê giác, thậm chí mua được cả lá phiếu bầu cử...). Chương 2 tác giả giúp đưa ra 2 khái niệm đang còn nhiều tranh cãi hiện nay: "Tiền không mua được gì" và "Có thứ gì tiền mua được nhưng không nên mua" ... Lối viết rõ ràng, cụ thể kèm theo chú thích chi tiết của tác giả giúp người đọc dễ dàng theo dõi. Một điểm nổi bật của quyển sách này với những quyển sách cũng loại đó chính là việc tác giả đưa ra từng luận điểm về từng vấn đề cụ thể và giải quyết ngay tại chỗ chứ không nợ hay né...
5
373558
2016-03-17 14:22:27
--------------------------
355579
11336
Bìa sách đẹp, trang nhã. Giấy in tốt, nhẹ. Tuy nhiên, không trích dẫn rõ nguồn bản dịch của nhà xuất bản nước ngoài, tên tác giả trình bày cũng không thống nhất (Lúc thì Michael Sandel, lúc thì Michael J. Sandel). Phần phụ lục trích dẫn dẫn sách tham khảo hoặc hướng dẫn sách đọc thêm cũng không có. Đặc biệt ở trang cuối sách, hơi bần tiện khi vừa hết nội dung là không có nổi một trang trắng cuối mà ghi luôn đội ngũ biên tập, bản in... mặt sau. Phần giới thiệu về tác giả cũng còn sơ lược.
Về nội dung cuốn sách: cuốn sách có phân đoạn rõ ràng, dẫn chứng thuyết phục, trích nguồn rõ ràng, viết đơn giản dễ đọc và dễ tiếp nhận. Tác giả nêu ra được vấn đề căn bản của kinh tế thị trường và trong tương quan với đạo đức để người đọc đánh giá khách quan và tự chọn lựa cách nghĩ.
4
570954
2015-12-20 11:47:26
--------------------------
343843
11336
Tiền không mua được gì? Đừng hi vọng tác giả trả lời thẳng thắn cho bạn rằng đồng tiền thực sự không mua được gì. Triết lý là những gì bạn tự chiêm nghiệm được đi từ góc nhìn của tác giả đến góc nhìn của bạn. Đạo đức con người sẽ cho phép giới hạn của đồng tiền đi đến đâu. Cuốn sách này thật sự rất hay và cho ta nhiều kiến thức nhưng không phải đọc để giải trí , đọc để hiểu liền, để kể cho nhau nghe...mà đọc để suy nghĩ, góp nhặt thêm nhiều điều bổ ích cho trí tuệ mỗi người.
5
648203
2015-11-27 12:06:29
--------------------------
330713
11336
sách nói về những điều trong cuộc sống hằng ngày tưởng chừng như hết sức đơn giản nhưng khi nhìn chúng dưới một khía cạnh khác thì nó lại hoàn toàn trở nên phức tạp và rất khó lý giải.
Tác giả đã đưa ra những trường hợp cụ thể, nêu lân những quan điểm theo nhiều khía cạnh khác nhau để nhìn nhận một vấn đề. Để cho mọt người đọc cùng suy ngẫm và đặt mình vào trong những con người ở những vị trí khía cạnh đó để xem xét sự việc. Khi đọc xong mỗi người sẽ tự rút ra được những bài học khác nhau tùy vào cách cảm nhận và tiếp cận vấn đề của mỗi người.

4
853546
2015-11-03 09:18:01
--------------------------
311488
11336
Là một quyển sách đề cập đến một trong những vấn đề đạo đức trong xa hội hiện nay, đó chính là Tiền và những đạo đức của đồng tiền. Sách là tuyển tập những ví dụ sinh động,  từ đó rút ra những điều về mặt đạo đức khiến mỗi ai chúng ta đều phải suy ngẫm. Sách cũng mang đến cho người đọc những thông tin mới về các khái niệm gắn liền cuộc sống mà hằng ngày chúng ta thường nghe. Tuy nhiên, sách sẽ khá khó đọc cho một số người mới làm quen với dòng sách này.
Về hình thức trình bày, mình rất hài lòng với sách này cũng như các quyển sách khác thuộc bộ sách Cánh cửa mở rộng. Tóm lại , đây là một tựa sách đáng đọc của mỗi người.
4
315066
2015-09-19 23:23:03
--------------------------
310876
11336
Trong một cuốn sách trước của tác giả Michael J. Sandel, cuốn "phải trái đúng sai", tôi đã đọc, những cảm xúc đầu tiên của tôi về nó thật lạ lùng, tư duy của tôi được khai mở, tôi như kẻ cô độc trong chiếc bình đóng kín cho đến khi đọc nó, những gì cuốn sách mang lại là những góc nhìn dưới con mắt của đạo đức và kinh tế, rằng sự lên ngôi của đồng tiền luôn nằm trong những quy chuẩn đạo đức, ảnh hưởng của nó không thể đo lường bằng những công cụ mà bằng con mắt và trí tuệ của con người. Tôi tiếp tục tìm đọc những tác phẩm khác nhưng tạo thêm một ấn tượng mới đó là cuốn sách đồng tác giả, cuốn "Tiền không mua được gì?", có những thứ người ta tìm cách gán cho nó một giá trị quy đổi, nhưng có những thứ khi gán cho nó một giá cả, nó dường như bị hạ thấp giá trị, và giá trị con người khi bị dán nhãn giá, con người không còn là chính mình, cuốn sách với tiêu đề triết lý nói lên điều tác giả muốn gửi gắm, đây thực sự là cuốn sách khiến nhân loại phải nhìn về những giá trị vĩnh hằng.
5
284397
2015-09-19 20:19:21
--------------------------
291316
11336
Đạo đức và lợi ích luôn là bài toán khó để có thể dung hòa. Tác giả đã truyền tải nhiều vấn đề xã hội được giải quyết bằng lợi ích kinh tế. Nếu xét về hiệu quả hiện hữu thì là có ích nhưng xét về khía cạnh đạo đức thì không phải là việc làm lâu dài như các vấn đề về y tế, giáo dục, dân số, môi trường…. Đồng thời quyển sách cũng cho người đọc một nội dung định nghĩa khác về hối lộ  - sử dụng công cụ khuyến khích bằng tiền để giải quyết vấn đề xã hội.
4
355080
2015-09-06 10:30:13
--------------------------
274047
11336
Vẫn với lối viết súc tích và nhiều ví dụ cụ thể như trong quyển "Phải trái đúng sai", trong quyển sách này, giáo sư Michael Sandel đã đặt ra những câu hỏi liên quan tới một đều không thể thiếu trong thời đại ngày nay của chúng ta, đó là tiền. Đọc quyển sách này sẽ giúp ta hiểu rõ tại sao đồng tiền và các giá trị thị trường lại có sức ảnh hưởng lớn đến vậy trong xã hội hiện đại ngày nay. Và cùng với đó, GS Sandel cũng đưa ra nhiều ý kiến của mình trong vấn đề đang nhức nhối này.
5
143116
2015-08-21 16:23:07
--------------------------
242839
11336
Mình khá ấn tượng bởi tên sách, một điểm cộng nữa là bìa sách và chắc chắn một điều là.... Sách của nhà xuất bản trẻ, hơn nữa lại nằm trong tủ sách " Cánh cửa mở rộng" do chính Giáo sư Ngô Bảo Châu giới thiệu thì không có gì phải bàn cãi rồi.

Lúc mở đầu tác phẩm, tác giả khéo léo đưa ra một loạt các vấn đề liên quan đến trị giá và mệnh giá tiền. Thậm chí tác giả còn giới thiệu đến việc con người ta quảng cáo ở đâu? trên xe bus, trên người và thậm chí là mượn các bộ phận cơ thể người để quảng cáo.

Sách này khá hay tuy nhiên... không nên vội vã đọc nhanh sẽ dẫn đến khó hiểu và chán nó đấy  ^^
4
90063
2015-07-26 23:57:43
--------------------------
215156
11336
Có nhiều lập trường khác nhau về sự chi phối của tiền trong cuộc sống hiện đại, và tác giả Micheal Sandel có một cái nhìn rất mới về những gì mà tiền không thể mua được. 
Vẫn theo cách tiếp cận với việc đưa ra các khía cạnh mâu thuẫn của vấn đề cần giải quyết, từ đó độc giả có thể đưa ra đánh giá của riêng mình. Hơn nữa, các tình huồng tác giả đưa ra rất thực tế và thú vị, nên nội dung cuốn sách trở nên sống động và dễ tiếp thu.
Điểm trừ duy nhất sau khi minh đọc xong cuốn sách là nội dung bên trong không ăn nhập lắm với tiêu đề, thế nhưng so với những gì cuốn sách mang lại thì cũng không đáng kể lắm
5
208712
2015-06-25 23:10:41
--------------------------
113750
11336
Một trong những biệt tài Michael Sandel là có thể biến những tranh luận cũng như những học thuyết triết học khô khan thành những câu chuyên hết sức "giật gân" kiểu Mĩ.
Sandel vẫn tiếp tục lối viết cũ . Nếu như bạn đã có dịp xem qua cuốn  " Phải trái đúng sai" . Thì Sandel vẫn "thả" người đọc vào thế lưỡng nan. Giưa 2 tình huống  khó giải quyết. Ông không hề áp đặt một cách giải quyết nào khả dĩ cả mà buộc người đọc phải tự đưa ra nhận xét của mình dự vào "đạo đức" của mỗi người.
Một cuốn sách dễ đọc đối với hầu hết mọi người nhưng vẫn thực sự khó khăn với một số ít chưa đọc qua cuốn "Phải Trai Đúng Sai" của ông hoặc chưa có hình dung cụ thể nào về triết học (thuyết công lợi,...).
Một vấn đề nữa là tên cuốn sách là "What money can't buy" nhưng lại được dịch với tựa đề khó hiểu "Tiền Không Mua Được Gì" đã biến cuốn sách theo một hướng hoàn toàn khác so với một tựa sách hoàn toàn theo hướng mở.
4
2795
2014-06-05 08:21:44
--------------------------
111545
11336
Cuốn sách này bàn về chuyện tiền bạc làm hủ bại nhiều giá trị tinh thần. Trong cuốn này, Sandel vẫn dùng phương pháp mà mọi người đã quen thuộc trong loạt bài giảng Justice: What’s the Right Thing to Do?: đưa ra vấn đề, sau đó đưa tiếp lập luận ủng hộ hoặc phản đối của người khác, và cuối cùng dùng lập luận chính mình để bẻ gãy lập luận trước. 
Nói chung cuốn này đọc được, nhiều ví dụ thực tế nên đọc thấy gần gũi, thú vị, mà cũng nhờ nhiều ví dụ ngoài đời thực nên ta có thể thấy được thế giới lắm trò đến mức nào. Hơn nữa, đây là một cuốn dễ đọc cực kì, gần như chẳng có gì khiến người đọc (ở đây là tui) phải ngừng lại để băn khoăn này nọ, phải nói là dễ đọc hơn nhiều so với cuốn Justice kia (vốn đậm chất triết học hơn). Tuy nhiên, ý tưởng truyền tải trong sách lại quá sức ít ỏi. Toàn bộ cuốn sách dường như là một tập hợp các ví dụ được tác giả chọn lọc có chủ đích nhằm minh hoạ cho lập luận và ý tưởng của mình. 

Tóm lại, nó chỉ đọc được, chớ không thoả mãn (ít nhứt là đối với tui).
3
31799
2014-04-30 14:10:29
--------------------------
110893
11336
Sau khi đọc quyển "Tiền không mua được gì", mình có một vài nhận xét như sau:
- Trong từng trường hợp cụ thể, tác giả đều nêu lên quan điểm của mình rất rõ ràng, cái nào đúng/ sai đều rất tường mình. Đây là cách viết mà bạn có thể tìm thấy được trong những tác phẩm khác của Michael Sandel.
- Người đọc được dẫn dắt và tiếp cận vấn đề thông qua những tình huống thực tế đã xảy ra, từ đó có thể tự suy ngẫm hoặc cùng suy ngẫm theo những phân tích sâu sắc của tác giả sau mỗi tình huống này. Đây là cách viết mà mình rất thích, vì nó không quá giáo lý giáo điều, bạn cũng có thể bắt gặp cách viết này trong những quyển sách của Dale Carnegie. Bạn hãy đọc và suy nghĩ và rút ra được bài học gì là do ở chính bạn.
- Bên cạnh viết và phân tích từng trường hợp, trong một số chương tác giả còn nhắc lại các trường hợp đã nói, bằng cách so sánh để người đọc dễ sâu chuỗi các dữ kiện và dễ hình dung.
- Cuối cùng, cách trích dẫn của tác giả cho thấy, để viết được quyển sách này, ông đã phải đọc và tìm kiếm rất nhiều tài liệu, sách, báo. Kết hợp với cách suy luận logic của mình, ông đã tạo nên một tác phẩm thành công và xuất sắc như vậy. Theo mình, đây không phải chỉ là một quyển sách nên đọc mà là một quyển sách tham khảo có thể dùng để học kèm với các môn về đạo đức trong quản lý, kinh doanh và trường học.
5
178374
2014-04-22 11:41:54
--------------------------
