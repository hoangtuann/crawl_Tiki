474503
4171
Cuốn truyện có kết thúc khá buồn, khi mình đọc phần cuối xong thì ngồi gấp sách lại mà ngồi thẫn thờ luôn. Tiếc cho Tris và Tobias, tình cảm vủa họ đẹp nhưng kết thúc không có hậu, tội nhất là Tobias. Mình thích cách miêu tả tâm lí của nhân vật này vào khúc cuối của tác giả, không ướt át như ngôn tình (mình khá ghét vì mấy nam thần trong này thường rất uỷ mị), tác giả không nói nhiều nhưng rất tinh tế khi lựa chọn chi tiết như cách đứng, nói chuyện, biểu cảm gương mặt, hành động hay. Đáng lí mình cho 5 sao nhưng sụt lại 4 thì do quá trình đầu câu chuyện khá dài dòng, không có nhiều bất ngờ, tập này mình vote cho Caleb đã trở lại là 1 người anh thật sự sau khi đã là một người phản bội. Mình tiếc cho Urial, mình thích nhân vật này lắm, vui tánh, dễ thương. Bìa sách nhìn mạnh mẽ, có điều mình không biết đó là biểu tượng của caìi, nhưng ấn tượng.
4
1263001
2016-07-11 15:41:35
--------------------------
464751
4171
Mình là một người rất giàu tình cảm, nên đọc khúc kết của bộ truyện mình không kiềm nổi xúc động. Đây có lẽ là một trong những cảnh mà mỗi khi nhắc đến bộ truyện Divergent là mình sẽ nhớ ngay đến đoạn cuối. Bi kịch nhưng cũng đầy mạnh mẽ. Tác phẩm rất hay, so với những cuốn trước thì mình thích nhất là phần này. Kịch tích lúc này đây được đẩy đến đỉnh điểm, khi mọi người bị bức vào đường cùng. Tình cảm của Tris và Tobias cũng căng đến đỉnh. mọi chuyên thế nào thfi mọi người đọc truyện sẽ biết. Rất cảm xúc. Cảm ơn tác giả vì đã làm nên một tác phẩm như thế này.
5
774087
2016-06-30 14:22:56
--------------------------
463035
4171
Mình rất thích coi phim mấy thể loại như Hunger Game hay Harry Potter. Và một hôm tình cờ mình bật HBO và thấy chiếu phim kẻ dị biệt là phần 1 của bộ truyện này. Mình xem xong cảm thấy quá hay nên quyết định lên mạng tìm hiểu thêm thông tin mới biết rằng phim này có cả truyện. Và rồi mình không ngần ngại lên tiki đặt hàng ngay cuốn tập 1. Sau khi đọc xong thấy hay quá không dứt ra được nên tiếp tục mua thêm. Truyện hay tình tiết gay cấn khiến cho độc giả như hòa mình vào bối cảnh trong truyện. Nói chung là thích lắm.
5
393298
2016-06-29 01:24:16
--------------------------
455424
4171
Bên ngoài bức tường ấy là gì? Chắc hẳn tất cả mọi người sống trong thế giới ấy, tuy đến từ các môn phái khác nhau nhưng đều tự hỏi mình câu hỏi này. Nhưng chỉ có những kẻ mang danh xưng "Divergent" mới dám đi ra ngoài để tìm hiểu điều đang được ẩn giấu đằng sau bức tường kia, tìm hiểu về quá khứ, số phận, định mệnh hay chính là tương lai của hộ sẽ ra sao.
Một cuộc sống khác, nơi các môn phái không tồn tại, liệu có êm đẹp?
Những hành động không lường trước được của các nhân vật khiến câu chuyện càng thêm lôi cuốn.
Và chuyến phiêu lưu đã nhận được câu trả lời xứng đáng.
4
494594
2016-06-22 15:45:38
--------------------------
437267
4171
Thật sự mình rất buồn sau khi đọc xong quyển này. Đây có lẽ là phần gay cấn, kịch tính và có nhiều mất mát nhất so với hai phần trước, các nhân vật mình yêu thích lần lượt ra đi, nhưng có lẽ không ngờ nhất đó là Tris. Khi đọc đến đoạn đó thì mình thật sự bỡ ngỡ và buồn đau cho tình yêu của hai người bọn - giữa Tris và Tobias. Khi mọi chuyện đã đến lúc yên bình thì tình yêu của họ lại bắt đầu trắc trở. Nhưng mình nghĩ với kết thúc này nó sẽ khiến ta nhớ đến bộ truyện nhiều hơn, bởi vì đau thương luôn khắc sâu vào trí óc chúng lâu hơn bất kì xúc cảm nào khác.
5
749597
2016-05-27 22:07:25
--------------------------
429720
4171
Divergent là bộ truyện mình đánh giá cao có nội dung rất sáng tạo về ý tưởng: một xã hội với 5 môn phái khác nhau được lập dựa trên tính cách, kĩ năng của họ và trong hệ thống xã hội ấy bắt đầu xuất hiện những con người hội tụ của cả 5 môn phái, sau đó là những bí mật được ẩn giấu. Ban đầu, khi đọc tập 3 biết rằng đây chỉ là một cuộc thí nghiệm thì mình có phần không chấp nhận được và khá là không hài lòng vì mình rất thích ý tưởng truyện về một xã hội như kia, đọc đến đoạn về GH và GL lại càng không thích. Nhưng đoạn kết đã hoàn toàn thay đổi suy nghĩ của mình và đúng nên được đánh giá là một cái kết hay và sâu lắng. Cái chết của Tris đến khiến mình vô cùng bất ngờ vì cứ nghĩ cô là nhân vật chính mà như nhiều truyện sẽ được bảo toàn mạng sống. Cái chết của Tris tạo nên một ý nghĩa cho truyện về sự hy sinh, can đảm, vị tha cũng như tình cảm dành cho những con người chúng ta yêu thương. Và cách Số Bốn cũng như những người khác đối mặt sau đấy cũng truyền cảm hứng rất nhiều về cuộc sống. "Chỉ nên để ai đó hy sinh thân mình vì anh nếu đó là cách tốt nhất để họ biểu thị tình yêu đối với anh", "Có rất nhiều cách để can đảm trong thế giới này ... Đôi lúc nó chẳng hơn gì việc nghiến răng chịu đựng cơn đau, chịu đựng những công việc hàng ngày, từ từ tiến bước đến một cuộc sống tốt đẹp hơn". Sau cùng tập cuối này là một cái kết không hoàn hảo nhưng trọn vẹn và để lại cho người ta một ý nghĩa xuyên suốt câu chuyện: hãy can đảm lên!
4
28222
2016-05-13 19:44:49
--------------------------
426181
4171
Ở quyển 3 này, tác giả Veronica Roth đã sử dụng hai ngôi kể, của Tris và cả Four nữa. Mặc dù biết được suy nghĩ nội tâm của cả hai nhân vật thì cũng tốt. Nhưng nó làm cho người đọc cảm thấy hơi rối và khó chịu. Mặc dù vậy thì ngôn ngữ kể chuyện của tác giả rất lôi cuốn, nhiều tình tiết hấp dẫn không có gì để chê. Nhiều người không thích cái kết của truyện. Cái chết của Tris quá đau buồn. Khiến Four rất đau khổ và dằn vặt. Nhưng mình nghĩ đó mới chính là điểm nổi bật làm cho người đọc cảm động và đó cũng là lý do tại sao series này lại ăn khách đến như vậy.
4
361461
2016-05-06 12:58:09
--------------------------
329835
4171
Đọc tới khúc cuối rồi mới thấy tất cả chỉ là vật thí nghiệm của chính phủ. Tris chết tuy đau buồn, tang thương nhưng lại đem đến tia hy vọng cũng như tương lai sáng sủa hơn cho thành phố. Các môn phái bị phá vỡ, trật tự mới được thiết lập, tự do hơn, hạnh phúc hơn là điều mà Tris muốn. Hình thức truyện khá sáng tạo, được kể qua 2 giọng của Tris và của Four giúp người đọc hình dung ra cuộc sống sau này khi không còn Tris. Cảnh cuối, cảnh Four rải tro của Tris có lẽ là cảnh cảm động nhất với mình.
5
392367
2015-11-01 14:31:42
--------------------------
329691
4171
Hình thức bên ngoài rất tốt, bìa đẹp. Quyển này giảm bớt các lỗi chính tả của 2 quyển đầu. Nội dung buồn quá, Tris sống vì mọi người mà lại chết, thấy tội nghiệp quá. Nên để cho Tris sống hoặc cả Tris và Four đều chết thì sẽ có có hậu, hay hơn. "Tôi siết chặt tay Tris, cầu nguyện rằng mình siết đủ chặt, tôi sẽ gửi sự sống lại vào cơ thể em, và rồi em sẽ lại hồng hào lên và tỉnh dậy" Đoạn này đọc xúc động, tội nghiệp cho Tobias. Đoạn đầu nói nhiều về toàn cái gì gì về tái lập, hỏng gen đọc lướt qua cho rồi nên chẳng nhớ được bao nhiêu. Nếu giảm bớt đoạn đó và thêm chi tiết vào đoạn cuối thì sẽ hay hơn rất nhiều. Tóm lại, nếu đã đọc 2 quyển trước rồi thì đọc luôn quyển này cho trọn vẹn.
5
697696
2015-11-01 10:45:12
--------------------------
318794
4171
Tôi yêu series Divergent của Roth từ khi bộ phim này chưa công chiếu, và càng yêu hơn nữa câu chuyện ấy khi được tận mắt nhìn thấy Tris và Tobias bằng xương bằng thịt quạ màn ảnh. 
Vì thế, nên khi được tin tập ba đã được xuất bản, tôi không chần chừ gì mà đã đặt mua ngay. Thế nhưng Roth, đi ngược lại với sức lôi cuốn của hai tập trước, đã làm tôi khá thất vọng. 
Đầu tiên là ngôi kể được chuyển đổi liên tục từ Tris sang Tobias. Được hiểu cảm xúc của Four là một điều rất tuyệt vời, nhưng khoảng cách giữa các ngôi kể có vẻ như quá gần để tôi có thể thoát ra khỏi mạch cảm xúc. Đang đắm chìm trong những dòng suy nghĩ miên man về khát khao được bước ra khỏi hàng rào, cũng như nỗi yêu hận dành cho Caleb của Tris, tôi lại phải buộc cảm xúc của mình thay đổi để nhập vào sự trách cứ, lo lắng và yêu thương của Four dành cho bạn gái, song song với nỗi buồn về mối quan hệ lạnh lẽo của anh với Evelyn.
Thêm vào đó, cái chết của Tris. Đọc truyện rồi, các bạn sẽ hiểu. Nó sơ sài đến đau đớn, và thê lương đến tột bậc. Điều duy nhất tôi hài lòng về sự ra đi của cô, chính là phần mà Four tự dằn vặt, cũng như những xúc cảm của anh sau khi cô qua đời, và trật tự được lập lại. 
Nhìn chung, cuốn sách vẫn là một câu chuyện đẹp, với ngôn từ khéo léo, đầy ẩn ý và những con người bất khuất, dũng cảm, sống vì một lý tưởng đẹp đẽ. Tôi đọc những trang cuối cùng và đúc kết cho mình được giá trị của sự "khác biệt". Tuy kết thúc của series không làm tôi hài lòng lắm, nhưng có thể khẳng định rằng đây là một bộ truyện đáng đọc. Giữa cuộc đời bon chen, thực tế đến tàn nhẫn này thì ba cuốn sách của Roth giống như một nghĩa cử đẹp vậy - cho ta thấy được thực ra đời cũng rất đẹp, rất công bằng và đáng để sống. 
 
4
279039
2015-10-06 23:21:24
--------------------------
306934
4171
Vì hai cuốn trước thật sự rất hay và hấp dẫn nên mình đã quyết định mua cuốn này mà không hề do dự. Tuy nhiên cuốn này có vẻ không được bằng hai cuốn trước. Nội dung có hơi dài dòng và khó hiểu một chút, cái kết lại quá bi thương. Mình đã hi vọng một cái kết có hậu hơn nên có hơi hụt hẫng. Tuy vậy, mình vẫn không thể bỏ cuốn sách xuống một khi đã cầm lên. Điểm cải thiện rõ rệt nhất của cuốn này so với hai cuốn trước là chất lượng giấy và khâu in ấn, bìa cũng đẹp nữa. Nhìn tổng quát mà nói thì mình thấy cuốn này và cả hai cuốn trước đều rất đáng đọc.
4
234502
2015-09-17 21:27:15
--------------------------
284113
4171
Nội dung hay nhưng ngôn từ còn hơi nhàm chán, gây mất hứng thú cho người đọc. Cách luân phiên thay đổi ngôi kể cho câu chuyện cũng một phần nào giảm bớt nhàm chán. Tris và Tobias đã hòa thuận hơn trước. Trong tập này, những thắc mắc của các phần trước đã được giải đáp, đặc biệt là về vấn đề Divergent, khi nhóm bạn của Tris và Tobias ra khỏi thành phố và gặp những người tưởng như đã chết, những bí mật dần lộ ra. Kết thúc truyện khá buồn và bị bỏ lửng khi nhân vật Tris chết và Tobias phải sống trong giằn vặt đau khổ. Mình không biết tác giả có viết thêm phần tiếp theo không, mình đang rất muốn mua.
Bìa đẹp, chữ rõ, chất lượng giấy tốt.

3
487348
2015-08-30 17:17:13
--------------------------
271520
4171
Có vẻ như tác giả khá "đuối" khi câu chuyện đến hồi kết. Nhiều chỗ rất dài dòng khiến mình cảm thấy hơi rối, nhất là phần giải thích về gen và các cuộc thử nghiệm trong quá khứ. Kết thúc thì khá nhanh, mình nghĩ cái chết của Tris rất "lãng xẹt". Tóm lại là mình hơi thất vọng về tập 3 này so với 2 tập đầu. Tuy nhiên điểm cộng là bìa sách rất đẹp và việc sử dụng phương ngữ hầu như không còn (có lẽ do phản hồi không tốt lắm với 2 tập đầu) .
3
307488
2015-08-19 10:28:36
--------------------------
252392
4171
Bìa đẹp, chất lượng giấy tốt hơn hẳn tập 1 và tập 2, nhưng cái kết là điều không hề mong đợi. 
Phần đầu dài dòng và phức tạp với những giải thích về gen và nguyên nhân của cuộc sống trong và ngoài thành phố. Và có những hoang mang với những dối trá bao trùm dối trá, nổi loạn hay ý định nổi loạn liên tiếp diễn ra. Và dường như có quá nhiều sự hy sinh, quá nhiều đau thương. Mình đã đọc không thể dừng lại được, chờ đợi một cái kết có thể tốt đẹp hơn, hồi hộp để rồi bị hụt hẫng. 
Câu chuyện được kể dưới góc nhìn và suy nghĩ của 2 nhân vật, để có thể được tiếp cận nhiều chiều. Những suy nghĩ của Tobias ở phần cuối thực sự xúc động và đáng suy ngẫm.
Một cái kết theo mình là chưa được toàn vẹn, nhưng cũng mở ra một cuộc sống mới đáng mong đợi hơn với các nhân vật. Chờ đợi Four.
4
206364
2015-08-03 17:02:35
--------------------------
252018
4171
Sau khi đọc xong 3 phần Divergent, Insurgent và Allegiant, cảm giác của tôi là khác hụt hẫng khi các nhân vật chính trong truyện lại hy sinh. Tuy nhiên, cả bộ truyện dù khoa học viễn tưởng nhưng lập luận logic về nguồn gốc gen và các thí nghiệm về sửa chữa gen. Là một bộ truyện không thể thiếu cho các bạn trẻ mê truyện viễn tưởng tương tự như Húng Nhại vậy.
Điểm cộng cho phần cuối này là: Chất lượng giấy được cải thiện đáng kể so với 2 phần trước, bìa đẹp, cắt gọn gàng.
P/S: truyện này có ra phim rạp chứng tỏ là rất hay các bạn nhé
5
540281
2015-08-03 12:27:43
--------------------------
248751
4171
Sự hấp dẫn của tác phẩm sau khi mình đọc hai phần trước thực sự là điều khiến mình quyết định mua quyển sách này. Nhưng sau khi gấp lại trang sách, cái ấn tượng của mình về sự hấp dẫn trong phần ba này chỉ là một mớ hỗn lộn. Mình thực sự không thích lắm cái khái niêm về kiểu gen và cái đoạn nghiên cứu, thực sự nó rất khó hiểu. Tâm lí của hai nhân vật Tris và Four được dẫn dắt theo điểm nhìn và lối kể riêng đã cho người đọc hiểu rõ hơn nhưng nhiều khi mình thấy khá khoai, hại não khi phải cố hiểu rõ họ đang cảm nhận gì. Ngoài ra, cái kết chưa thực sự chinh phục người đọc cho lắm. Tuy vậy, bìa sách và chất lượng in đã cứu cả quyển sách. Mình thấy rất đẹp...
4
177339
2015-07-31 10:13:49
--------------------------
243599
4171
Sau khi đọc hết phần 1 Divergent và phần 2 Insurgent mình đã trông chờ phần 3 này rất nhiều, trông chờ một kết thúc thật hay và ấn tượng. Diễn biến của tập này vẫn hấp dẫn và kịch tính, nhưng có chút hơi khó hiểu và rắc rối ở phần giải thích "các nguồn gốc gen" và " các thí nghiệm sửa chữa gen", mình thấy hơi hụt hẫng vì Divergent thì ra chỉ là những "gen lành" mà thôi. Sau bao nhiêu cuộc đối đầu, cuối cùng Tris lại hi sinh, thật không chịu nổi! Kết thúc buồn quá đi, tội nghiệp Số Bốn nữa. Tập này được kể theo lời của 2 nhân vật nên cảm xúc, suy nghĩ của cả 2 người được biểu lộ rõ hơn. Về chất lượng thì giấy khá tốt nhưng giấy hơi bị tưa, hi vong nxb sẽ cắt lại đẹp hơn.
4
315293
2015-07-27 15:50:22
--------------------------
240015
4171
Mình thực sự rất thích bộ này và đã săn lùng được trọn bộ. Tuy mua được đã lâu nhưng chưa có đọc, nhưng ấn tượng đầu là bìa và chất lượng giấy tốt hơn rất nhiều so với hai phần trước. Trong khi ở hai phần trước giấy rất xấu, để một thời gian rất nhanh vàng và được cắt không cẩn thận nên có đoạn thụt ra thụt vào. Đối với những người yêu thích sách truyện như mình điều đó thực sự không thể chấp nhận được. Nhưng ở phần này NXB trẻ đã cải thiện được chất lượng giấy, giấy không đến nỗi chứ vẫn rất dễ bị ố vàng. Nhưng công đoạn cắt gọt đã được cải thiện hơn nhiều. 
4
380059
2015-07-24 10:59:08
--------------------------
236243
4171
Mình rất thích cách thiết kế bìa của bộ truyện này, rất ấn tượng. Giấy ngà nhẹ nên cầm rất vừa tay mặc dù nhìn bề ngoài cuốn sách khá dày. Về hình thức, mình đánh giá 5 sao.
Tuy nhiên cốt truyện của phần 3 mình hơi thất vọng một chút, có vẻ không thu hút lắm. Phần 1 viết rất kịch tính và ngôn từ rất thú vị. Nhưng phần 3 thì giảm sút hơn so với mức mình kỳ vọng cao trào ở một series truyện như thế này. Cho nên mất một sao vì điều này nhé.
Dù sao, mình vẫn chờ đợi Tiki phát hành cuốn Four cho series này. 

4
9500
2015-07-21 17:15:35
--------------------------
235821
4171
Phần cuối của Series Divergent, đây là phần rất bất ngờ. Bất ngờ khi biết còn có thế giới bên ngoài đang theo dõi thành phố và bất ngờ đến cái chết của... Tris. Sau bao lần nguy hiểm đến với mình, cuối cùng đến phần cuối Tris đã không sống được nữa. Truyện để lại nhiều cảm xúc và nhân văn. Dũng cảm luôn là cái được nói đầu tiên. Trong ba phần của Series này, tất cả bìa sách đều rất tuyệt vời ^^ nội dung cũng độc đáo không kém. Đây sẽ là truyện mình đọc lại nhiều lần trong đời :)
5
167640
2015-07-21 13:08:26
--------------------------
234228
4171
Mình mê bộ truyện này ngay từ tập 1 nên cũng đã cố gắng tìm mua trọn bộ để đọc. Tập 3 thì có vẻ hơi rối rắm hơn so với tập 1 và 2. Kết thúc truyện hơi lãng xẹt, không có hậu tí nào nào cả, khiến cho mình cứ ấm ức mãi. Cái chết của Tris không thỏa đáng vì cô chết một cách không tương xứng với những gì cô đã trải qua. Sau bao nhiêu gian lao, nguy hiểm, thương tích vẫn không chết nhưng giờ lại chết chỉ vì 1 viên đạn bắn xuyên qua hông. Nhưng đây cũng là một cái kết mà ít tác giả nào có thể viết được, vì nó làm cho câu chuyện đi sâu vào lòng người đọc hơn, chúng ta có thể nhớ tới và không thể quên những cảm xúc trong từng trang truyện. Các nhân vật,dù phản diện hay chính diện,cũng mang đến cho người đọc nhiều bài học khác nhau, ý nghĩa riêng. Kết thúc truyện, các nhân vật vẫn sống và trải nghiệm cuộc đời rất bình thường sau khi sự việc xảy ra, cuộc sống vẫn tiếp diễn. Và mình vẫn hi vọng Christina và Tobias sẽ đến với nhau sau khi đã cùng nhau trải qua nỗi đau mất đi người yêu thương nhất.....
3
126889
2015-07-20 10:36:20
--------------------------
218777
4171
Allegiant là phần cuối cùng của bộ truyện mà tôi bắt đầu đọc gần đây. Ngay từ những trang đầu tiên đã làm tôi khá tò mò về thế giới mà tác giả tạo ra. Và khi bắt đầu đọc thì thật sự khá hay cho đền khi tôi đọc hết tập 3.
Tuy nhiên ngẫm nghĩ lại tôi thấy rằng tập truyện này hơi bị rối rắm trong cốt truyện: Thế giới trong thế giới, nổi loạn trong nổi loạn, phản bội trong phản bội và hình như tác giả hơi thẳng tay trong việc "giết" các nhân vật của mình (tới tập 3 chỉ còn vài người từ tập 1 là còn sống !?!) Bạn đọc sẽ quan tâm nhất khi đọc cuốn truyện này chắc là truyện Tris chết vì anh trai, thật ra tôi thấy hơi không thỏa đáng khi cô chết một cách hơi không xứng với những gì đã trải qua. Nhưng có lẽ đó là cách tác giả thể hiện sự tàn nhẫn của cuộc đời chăng? Tuy nhiên, có 1 điểm cộng là tác giả xây dựng nhân vật Tris và một số nhân vật khác khá hay trong tập này. Và tôi nghĩ khi đã đọc 2 tập trước thì cũng nên đọc tập 3 này cho trọn vẹn.
3
180112
2015-06-30 21:23:22
--------------------------
215488
4171
Mình chỉ bắt đầu đọc sách khi xem phần 1 - Divergent trên tivi và ngay lập tức bị cuốn hút. Thế là đi mua ngay sách để đọc. Mình đọc cả ba phần rồi, phần nào cũng có cái hay riêng. Nhìn chung các quyển sách được NXB Trẻ thiết kế đẹp mắt mặc dù còn xót vài lỗi chính tả. 
Về phần nội dung của tác phẩm thì mình chỉ dùng 1 từ để nói: Hấp dẫn. Các chi tiết chuỗi sự việc được Veronica diễn tả hết sức lôi cuốn. Có lẽ một phần là nhờ người dịch mà ngôn ngữ nhân vật rất sâu sắc. 
Mình sẽ không nói nhiều về cái kết của câu chuyện - một cái kết mà ít tác giả nào dám thực hiện. Có vẻ Veronica cũng là một Dauntless nhỉ ?!?
Không biết NXB Trẻ có xuất bản tiếp cuốn cuối cùng không nhỉ, cuốn Four ấy? Mình mong là sẽ đọc được suy nghĩ của Four.
5
589549
2015-06-26 13:36:54
--------------------------
201353
4171
Về hình thức bìa được thiết kế nhìn rất là bắt mắt và ảo diệu , phải nói là tôi rất thích kiểu bìa như thế này . Về chất lượng tôi cho điểm 10 nhưng còn sai sót về một số lỗi chính tả và cần nên có chú thích giải nghĩa một số từ khó hiểu trong sách để độc giả tiếp cận sách được dễ dàng hơn . Về nội dung truyện quả thực rất hay và hấp dẫn . Đúng kiểu tập sau hay hơn tập trước nhiều . Trong tập này nghệ thuật mikeeu tả tâm lý của nhân vật được tác giả diễn đạt rất tài tình . Các cuộc chiến sinh tử thì ôi thôi không chỉ hồi hộp mà còn rất cuốn hút . Truyện truyền tải rất nhiều thông điệp ý nghĩa và một trong số đó khiến tôi ấn tượng sâu sắc là hãy dũng cảm đấu tranh vì lợi ích của bản thân
5
635463
2015-05-27 09:38:58
--------------------------
198707
4171
Phần 3 là phần cuối cùng kết thúc series Divergent. Tris, theo từng phần của series thì ngày càng trưởng thành hơn, mạnh mẽ hơn, tôi cảm thấy thực sự khâm phục, người anh hùng của tôi.  Allegiant có vẻ như đã khơi bày hết sự thật đằng sau một thế lực bị che kín bấy lâu, một nơi ngoài hàng rào mà con người luôn muốn khám phá ra. Vì sao họ, những con người bên ngoài hàng rào kia lại muốn tìm ra Divergent? Phần 3 cho tôi thấy được sự hồi hộp khi lần theo từng chi tiết, khiến cho tôi cảm động trước tình cảm mà Evelyn dành cho cậu con trai mình, Tobias. Và tôi thực sự đã hoàn toàn bất ngờ và tiếc nuối với cái kết cuối cùng...Cảm ơn Veronica đã viết ra một thế giới ảo ảnh và cũng đầy chân thật và thuyết phục....Cảm ơn vì điều đó và tất cả. Một cuốn tiểu thuyết không thể nào quên được...HÃY DŨNG CẢM.
5
455956
2015-05-20 16:22:39
--------------------------
196439
4171
Đi mua ở nhà sách về định khoe các bạn thì thấy đứa nào cũng đọc qua hết rồi. Nói chung chắc chắn đây có lẽ là phần đem lại cho tôi nhiều sóng gió nhất và có lẽ cũng là phần khó hiểu nhất. Do trong cuốn truyện này, hết lần này đến lần hác bí mật bị phơi bày. Không bết tin ở ai, không nơi đâu có thể đem lại cảm giác đúng nghĩa là "nhà". Họ cố gắng để phơi bày ra sự thật với niềm tin sự thật luôn là điều tốt nhất. Nhưng đau phải vậy, tìm ra sự thật đồng nghĩa với việc phải chịu đựng được nỗi đau mà nó gây ra và rời bỏ quê hương của mình. Nhưng nếu không phơi bày sự thật nghĩa là đang sống trong thế giới ảo, không biết cuộc sống thực tế như thế nào và tự lừa dối bản thân. Mặc dù cái kết rất buồn, ai cũng mất đi người thân, Tris và Uriah. Đọc đến khúc họ chuẩn bị lìa đời là nước mắt nước mũi cứ thế tuôn trào ra. Không đọc phần này chắc là sẽ hối hận lắm.
5
591187
2015-05-15 21:17:13
--------------------------
185933
4171
Đây là phần cuối cùng của loạt series đình đám Divergent và nó là một trong những cái két nhiều cảm xúc nhất mà mình từng biết. Trong phần này bà Evelyn Johnson - mẹ của Tobias đã xoá bỏ chế độ môn phái, và cùng những người vô môn phái lên nắm quyền, kiểm soát điều khiển cả thành phố. Những Allegiant - một nhóm người muốn phục hồi chế độ môn phái, bí mật hoạt động chống đối chính quyền với chính quyền của Evelyn, nhằm lật đổ bà ta, khôi phục lại các môn phái. Một cuộc chiến tranh tàn khóic nữa có khả năng sẽ nổ ra - quyết định vận mệnh, tương lai của thành phố cho đến mãi mãi về sau. Tris cùng các bạn của cô đã mạo hiểm vượt qua hàng rào, đến cái thế giới bên ngoài đầy rẫy những bí ẩn và nguy hiểm kia. Họ đã khám phá ra rất nhiều điều, những bí mật đen tối nhất đã bị thời gian chôn vùi, những âm mưu mà theo đó là vì Lợi Ích Lớn Lao Hơn. Tris sẽ làm gì để hàn gắn khoảng cách giữa cô và Tobias, khi những bất đồng đã dần dần len lỏi vào giữa họ? Cô và những nguòi bạn sẽ làm gì để ngăn chặn âm mưu của những người ở Cục nhắm vào thành phố quê hương họ? Nội dung đầy ấn tượng, dĩ nhiên. Hơn nữa, đây là một quyển sách cực kỳ nhiều vấn đề liên quan đến khoa học, nếu bạn không đọc kỹ sẽ không thể hiểu được sách đang nói về cái gì. Nhưng cái làm mình ấn tượng ở đây là những quyết định của tác giả. Nó mang đầy sự bất ngờ, mới mẻ, kịch tính và có thể có cả...sự choáng váng (bạn sẽ hiểu mình nói gì nếu đọc truyện). Cô ấy đã đi một nước đi mà rất hiếm tác giả nào có thể làm được. Mình nghĩ có thể nhiều bạn sẽ không hài lòng với cái kết thúc của truyện, nhưng không thể phủ nhận  rằng cái kết của nó "Dị biệt" và độc đáo như chính cái tên của loạt truyện - Divergent. Cảnh báo cuối cùng này: bạn hãy để sẵn một hộp khăn giấy bên cạnh khi đọc phần cuối, vì bạn sẽ không thể nào kìm được nước mắt đâu.
Phải nói là loạt sách lần này của Nhà xuất bản Trẻ cực kỳ chất lượng luôn. Bìa giống như bìa gốc đẹp rồi nhé. Từng con chữ in rõ ràng sắc nét, rất dễ đọc. Cách dịch lần này cũng ổn, khôn dùng một chút phương ngữ nào, dịch rất hay và dễ hiểu. Cả quyển sách hầu như không có chỗ nào in sai hay mắc phải cái lỗi cỏn con nào. Nói chung tất cả mọi thứ đều tuyệt vời, từ nội dung cho đến hình thức.
5
415536
2015-04-20 10:26:12
--------------------------
