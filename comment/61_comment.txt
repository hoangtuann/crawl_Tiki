77718
61
Tập 1 này mình đi mượn đọc của bạn. ^^ thấy quyển này hay mà đúng teen thế. Nội dung thì ngòn ngọt, ai cũng như tìm thấy mình trong đó.
Mình yêu những hồi ức trong tâm hồn, rồi thành ra yêu luôn cả những mùa hè nữa, dù bình thường mình thích mùa đông hơn. Nó cũng không chỉ đơn thuần là những mẩu nhật ký của cô bé con mà chứa nhiều điều hơn thế. Rất tuyệt!
p.s: mà bản anh này hay hơn bản việt theo mình!
Dù có đôi chỗ mình không hiểu rõ cho lắm, nhưng vấn khá ổn. 

5
111612
2013-05-29 11:25:58
--------------------------
37038
61
Đọc cái này hay hơn bản Việt thật ! Tuy vốn từ mình không tốt lắm , nhưng với quyển từ điển mình đã đọc xong nó! Thật thích khi đọc bản chính gốc . Có nhiều đoạn hay hơn khi dịch ra tiếng Việt nhiều . 
Không một quấn sách về mùa hè nào mình đọc tuyệt vời hơn cuốn này ! 
Một mùa hè thật đẹp và ý nghĩa ~!
5
51977
2012-08-18 06:08:49
--------------------------
19470
61
Tuyệt!! Ngôn từ không khó hiểu, khá dễ đọc và nắm bắt! Đọc nguyên bản thích hơn nhiều vì có nhiều đoạn khi dịch ra tiếng Việt đã lược bớt!! Đọc trọn bộ chính gốc, tình yêu của họ mặn mà hơn nhiều!! Chịu khó tra một số từ mới, chịu khó ngẫm nghĩ thì chẳng chán lắm đâu khi phải đọc cuốn truyện bằng tiếng Anh, nó còn giúp nâng cao khả năng ngoại ngữ nữa chứ!! 
Bạn sẽ tìm thấy nhiều điều ở cuốn sách này ngoài tình yêu và mùa hè đấy! :)
5
25744
2012-02-18 09:03:56
--------------------------
