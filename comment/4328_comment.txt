479024
4328
Mình luôn thích sách của Nhã Nam và khá là tin tưởng. Thường thì bìa sách rất đẹp, dày dặn. Nhưng quyển sách này là 1 của hiếm không nằm trong số đó. Bìa sách mỏng, dễ quăn, cái hình minh họa thì... hơi nhạt nhòa, không xứng lắm với cái tình của nội dung câu chuyện. Ý kiến cá nhân mình là thế. 
Bên trong có nhiều trang bị mờ đôi chỗ.
Còn nội dung câu chuyện.. không dành cho trẻ em dưới 16 tuổi và những tâm hồn quá trong trắng ^^. Mọi người nên tự khám phá thôi. Nói chung là hay tuyệt ^^
4
711140
2016-08-06 19:23:15
--------------------------
406147
4328
Cốt truyện lạ, không có nhiều chi tiết bất ngờ nhưng đủ làm người đọc thấy cuốn hút. Tác giả cũng miêu tả những cảm xúc và rung động của tình yêu rất chi tiết và chân thực. Truyện không xây dựng ra các nhân vật hoàn hảo, mà ai cũng có nhược điểm nào đó muốn che giấu. Để rồi khi gặp được tình yêu thực sự của cuộc đời, người ta không còn phải giấu đi những nhược điểm đó nhưng vẫn trở nên đẹp hoàn hảo trong mắt người yêu mình. 

Truyện cũng cho thấy sức mạnh của tình yêu: khi gặp đúng một nửa của cuộc đời, người ta sẽ thấy mình có trách nhiệm hơn của cuộc sống, không chỉ với bản thân mà còn với người mình yêu, và cố gắng nhiều hơn để vượt qua các khó khăn gặp phải. 
4
586824
2016-03-27 20:03:19
--------------------------
384201
4328
Thực sự thì khi nhìn bìa quyển sách này mình đã nghĩ luôn rằng sẽ không mua. Vì mình là đứa ưa hình thức, cái gì phải đẹp mới thích, mới mua. Nhưng ngày hôm đấy khi vào nhà sách mình có đọc qua phần giới thiệu nội dung ở bìa sau và quyết định mua thử về đọc ( tại trong thời gian ấy không có sách để đọc ^^). Và thề là mình cảm thấy không hề phí tiền chút nào luôn. 
Mình đã đọc qua vài quyển sách có nội dung kiểu công tước đẹp trai, giàu có, nguy hiểm nhưng hấp dẫn yêu cô tiểu thư xinh đẹp, thông minh như "Chiến dịch trái tim" - Amanda Quick, "Rồi cũng khép lại những tháng ngày đơn độc" - Julia Quin, "Tiền chuộc trái tim" - Meg Cabot nhưng bản thân thấy "Giá nào cũng yêu" hay hơn cả. Bởi vì nó không đơn thuần mô típ như trên mà cái cách Lisa Kleypas tạo ra tình huống 2 người gặp nhau, nảy sinh tình cảm và ở bên nhau nó khác hẳn với 3 quyển sách mình đã đọc. Đúng như tên sách, cả hai bất chấp tất cả để yêu, bằng mọi cách, bằng mọi giá dù chưa chắc chắn về tình cảm của đối phương. Thế nên khi đọc xong mình đã ngây ngất vương vấn cả tuần trời liền :))))
Đây là quyển sách đáng đọc, đáng suy ngẫm. Nếu là bạn, bạn có bất chấp mà giá nào cũng yêu không?
5
445157
2016-02-21 22:05:36
--------------------------
383856
4328
Kết cấu câu chuyện theo mình là hơi thèa ở khúc đầu. Thật lòng thì phần đầu câu chuyện làm mình suýt là nản không muốn dọc nữa. Nhưng mình lại tiếp tục đọc để rồi nhận ra câu chuyện càng về sau càng tốt, nội dung hay hơn và văn phong ổn hơn.
Tình yêu của John và Lottie lãng mạn lắm, tuýp lãi mạn của mình giống như hai nhân vật này, cùng nhau vượt qua khó khen, ở bên nhau, cần có nhau như vậy.
Bìa rất đẹp, bao sách ổn, về hình thức theo mình như vậy là ok lắm rồi.
3
622800
2016-02-21 13:18:36
--------------------------
352583
4328
Chờ đợi đã lâu cho tới bây giờ mới được cầm quyển cuối cùng trong series Bow Street Runners trên tay, thật sự là một bộ truyện rất hay và cũng rất lãng mạn. Nick Gentry có thể từng là một tên tội phạm khiến cảnh sát phải khốn đốn, vất vả mỗi khi muốn tóm được anh, thế nhưng giờ đây Nick đã là một chàng cảnh sát phố Bow đầy nhiệt huyết luôn săn lùng những tên tội phạm nguy hiểm và cũng luôn đạt được thành. Tuy nhiên quá khứ của anh là một bí ẩn và điều đó luôn dằn vặt, ám ảnh anh trong mỗi giấc ngủ. Cho đến khi anh gặp Lottie. Họ gặp nhau và cũng sớm kết hôn vì lợi ích của Lottie, có thể cho rằng cả hai đến với nhau không phải do tình yêu nhưng ngay từ lần đầu chạm mặt là tôi đã thấy ngay tiếng sét ái tình giữa họ rồi. Câu chuyện ngọt ngào giữa họ khiến tôi cực kì thích thú, cách mà Nick chăm sóc cho Lottie hay cách mà Lottie quan tâm đến anh để lại nhiều ý nghĩa khi nói về tình yêu.

Truyện của Lisa Kleypas thì luôn luôn có điểm chung về những tình huống nóng bỏng và độ nóng đôi lúc cũng được đẩy lên đến cao trào nhưng tôi lại thấy tác giả viết khá tinh tế và không hề lộ liễu một chút nào. Truyện cũng mang sự cân bằng trong cả trinh thám lẫn lãng mạn, tuy trong truyện này phần trinh thám không được hấp dẫn như hai cuốn trước, có cảm giác hơi lãng xẹt nhưng xét về mô tả nội tâm lẫn cách suy nghĩ điên rồ của tên bá tước Radnor hay sự rượt đuổi kịch tính của Nick với tên tội phạm theo tôi là không rõ nguồn gốc, mặt mũi cũng khá lôi cuốn.

Nếu đánh giá thì có lẽ truyện này không bằng hai quyển còn lại nhưng xét theo tính lãng mạn thì tôi lại khá thích cuốn này và cũng rất cảm động trước lòng nhiệt thành lẫn tình yêu son sắc mà Nick luôn dành cho Lottie.

5
41370
2015-12-14 19:36:16
--------------------------
350207
4328
Lãng mạn, nhẹ nhàng, lôi cuốn.

Quả thực đọc cuốn này mình cảm giác như đang xem phiên bản "hay" và "thơ" hơn của "Fifty Shades Of Grey" ý :))

Nhân vật nam đúng mô tuýp yêu thích của phụ nữ, mình không phải bàn thêm. Còn nhân vật nữ mình thấy tác giả xây dựng rất hay, trong sáng nhưng rất cá tính, không hề yếu đuối ngây thơ bánh bèo.

Ngoài ra, các nhân vật phụ cũng khá đặc biệt, vị trí của các nhân vật phụ cũng góp phần làm rõ câu chuyện (chứ không mịt mờ như trong "Fifty Shades Of Grey")

Điểm cộng cuối cùng là bìa sách quá sức dễ thương <3 
3
1032006
2015-12-10 08:35:41
--------------------------
257776
4328
Không yêu sao phải trả giá??? 
Khi đọc giới thiệu sách mình rất ấn tượng với câu nói này. Và quả không thất vọng khi cầm trên tay cuốn truyện. Lời khen đầu tiên là bìa minh họa rất đẹp và màu chủ đạo là màu mình thích nhưng phần giấy bìa lại hơi mỏng dễ bị quăn. Nhưng điều đó không quan trọng vì nội dung câu truyện thực sự lôi cuốn khiến bạn đọc chìm sâu vào thế giới của nick và lottie mà tác giả đã tạo ra với mình khó có thể đặt cuốn truyện xuống vì nó quá sức hấp dẫn. 
4
613896
2015-08-07 22:58:44
--------------------------
226396
4328
Trước hết, về hình thức, tôi có lời khen ngợi cho Nhã Nam, luôn cho ra những bìa sách vô cùng đẹp và ấn tượng! Đặc biệt, mình đã có bookmark của quyển sách này nên cực thích khi đọc nó!
Đây là một trong những câu chuyện tình yêu - lãng mạn cực kì xuất sắc mà tôi từng được đọc qua. Câu chuyện tình yêu hết sức ngọt ngào đó khiến chúng ta không khỏi xúc động! Một người tiểu thư trốn chạy tình yêu của vị bá tước, cuối cùng lại đem hết lòng mình, tim mình ra yêu một chàng cảnh sát không quen biết, đầy xa lạ, không thân thích! Cô yêu đến đắm say, và đồng thời còn nhận ra ở chàng cảnh sát đó những đức tính tốt đẹp. Tinh yêu có khả năng cảm hóa lòng người!
Nói chung, đây là một kịch bản hoàn hảo, một câu chuyện lãng mạn, và hết sức ngọt ngào!

4
616731
2015-07-12 07:25:18
--------------------------
196218
4328
Đây là một tác phẩm đầu tiên mình đọc của tác giả này. Phải nói đây là một tác phẩm ngọt ngào, rất lãng mạn và cũng kịch tính. Cầm quyển sách lên mình muốn đọc một mạch cho đến hết quyển
Lúc đầu mình không ấn tượng lắm với Nick và Lottie, tuy nhiên từ đến 3 chương cuối làm mình rất ấn tượng với tác phẩm này.
Trong truyện mình ấn tượng nhất với gia đình của Lottie và ông Radnor. 
Mình cũng sẽ đọc hai quyển đã xuất bản của tác giả này.
Như những quyển của Nhã Nam thì bìa sách cũng rất đẹp, tuy nhiên có điều Lottie tóc vàng nhưng trên bìa là màu nâu, phân vân muốn hỏi người vẽ bìa quá. 
Sách mình mua một số chỗ bị mờ, chắc có lẽ chỉ lỗi một số quyển.
4
343388
2015-05-15 15:49:27
--------------------------
186056
4328
Một trong những tác phẩm của Lisa Kleypas mà để lại cho mình ấn tượng sâu nhất. Mình đã luôn tò mò về Nick Gentry trong 2 tác phẩm trước cùng series là "ngoài vòng tay anh là bão tố" và "người tình của tiểu thư Sophia". 
Cuộc đời tăm tối của Nick Gentry - tử tước John Sydney luôn có một khoảng tăm tối nào đó trong quá khứ mà không một ai có thể hình dung được cho đến khi đọc xuyên suốt hết "Giá nào cũng yêu". 
Mối tình của Charlotte và Nick bắt đầu không phải bằng tình yêu, kết hôn cũng không phải vì yêu, nhưng bằng cách nào đó Lottie và Nick lại trở nên hoàn hảo cho nhau. 
Truyện vẫn mang phong cách của Lisa Kleypas, nghĩa là vẫn luôn nóng bỏng, lãng mạn, nhưng lẩn khuất đâu đó trong "giá nào cũng yêu" vẫn cứ để lại ấn tượng cho mình hơn hẳn các tác phẩm khác của LK.
4
125574
2015-04-20 15:12:16
--------------------------
179392
4328
Đã quen biết sơ sơ với Nick Gentry trong hai tập truyện trước (Người tình của tiểu thư Sophia & Someone to watch over me), nên tôi nghĩ câu chuyện này sẽ có nhiều điều đặc biệt hơn. Nhưng cốt truyện lại hơi thất vọng sau đoạn mở đầu khá ấn tượng, giữa những cảnh ân ái ít có thêm điều gì hay mới lạ về nhân vật, nhất là nửa quyển sau. [Spoiler] Trong câu chuyện còn có một điều gì đó rất tăm tối về cách đối xử của cha mẹ Lottie với cô, dường như họ hai tay dâng tặng cô cho ngài Radnor để lão muốn làm gì thì làm. Chi tiết này dường như chỉ để kết nối những điểm tương đồng về cuộc đời của Lottie và Nick mà tôi thấy hoàn toàn không cần thiết.
3
116202
2015-04-07 04:02:55
--------------------------
179167
4328
Lại một tác phẩm nữa mang đậm nét Lisa Kleypas được phát hành. Vẫn những câu chữ lãng mạn và nóng bỏng, tác giả đã kể nốt chuyện tình giữa Nick Gentry (tử tước John Sydney) và Charlotte - chặng cuối của series Bow Street Runners. Không quá gay cấn, hồi hộp. Không quá cao trào hay nhiều bí ẩn, chỉ có cốt truyện nhẹ nhàng, say đắm. Suy cho cùng thì đó là điều mà tôi vẫn mong đợi ở một tiểu thuyết lãng mạn lịch sử. 
Thêm một điểm cộng nữa là bìa sách khá dễ thương, có kèm thêm một phụ bản màu ở bên trong.
5
470073
2015-04-06 16:36:24
--------------------------
