352955
10721
Nhà mình ăn chay một tháng tận 4 ngày, có nhiều đám giỗ cũng làm toàn chay nên cuốn sách này thật sự rất hữu ích. Mặt ngọt đều có đủ, thực đơn đa dạng phong phú. Dễ chế biến, không tón nhiều thời gian, nguyên liệu không quá khó tìm và quá mắc. Món ăn trong sách trình bày đẹp mắt. Các món ăn cung cấp đủ chất dinh dưỡng, không có đồ mặ mà ăn vẫn rất ngon miệng. Ăn chay mà tăng cân còn nhanh hơn ăn mặn nữa. Quan trọng là mua trong đợt giảm giá nên giá hết sức mềm ^^
5
580575
2015-12-15 12:20:14
--------------------------
