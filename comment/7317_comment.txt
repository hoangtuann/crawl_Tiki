476875
7317
Đối với tôi truyện " Mệnh phượng hoàng " khá hay . Nội dung chật chẽ . Nên thích hợp cho mấy bạn thích truyện cung đấu
- Tuy là truyện có kết thúc HE nhưng mà tôi vẫn thấy buồn . Cá nhân tôi thích Tô Mộ Hàn hơn là Hạ Hầu Khâm Tử . Tô Mộ Hàn hy sinh để bảo vệ Tang Tử không ít lần . Cái kết thúc không biết là Tô Mộ Hàn còn sống hay là không . Đọc xong câu truyện này mà tôi ám ảnh và khóc cho Tô Mộ Hàn.
Mấy bạn muốn mua câu truyện này thì cứ mua đii nha!!! Nhất là mấy bạn bị nghiện truyện cung đấu
4
608056
2016-07-23 07:57:00
--------------------------
475880
7317
Trong 4 tập của bộ Mệnh phượng hoàng, tập 4 này là viết yếu tay nhất. Những chi tiết trong truyện không còn khít và logic như mấy tập kia nữa. 
Mình rất hài lòng với cái kết của Mệnh phượng hoàng. Dù kết SE thì thường nhớ lâu hơn kết HE như thế này. Chỉ tiếc cho Tô Mộ Hàn, dù với nhân vật tiên sinh này, tác giả đã cho một cái kết mở, không thấy nói Tô Mộ Hàn chết hay sống nên độc giả như mình vẫn còn hi vọng mong manh. 
Tóm lại, đây là một bộ ngôn tình cổ cực kì đáng đọc. Không thể chê được bất cứ điểm gì của cuốn sách này!
4
81138
2016-07-17 08:17:02
--------------------------
469496
7317
Truyện rất lôi cuốn, liên kết chặt chẽ, nội dung cũng hấp dẫn. Mình thích sự thông minh của Tang Tử, cũng thích sự kiên cường của nàng, nàng xử lý mọi tình huống một cách rất hay. Nam chính này mình không vừa ý lắm, vì có nhiều lúc, hắn thấy nàng gặp nguy hiểm nhưng bất lực không thể làm gì, khiến mình cảm thấy hắn không đủ năng lực bảo vệ nữ chính. Còn về Tô Mộ Hàn, mình thích nhân vật này, y gần như đồng hành cùng Tang Tử trong suốt những thời gian nàng gặp nguy hiểm, tin rằng việc này đã làm cho rất nhiều độc giả cảm động.
Diễn biến hợp lý, cốt truyện lôi cuốn, không nhàm chán cũng không ngược tâm, là một tác phẩm đáng để đọc!
4
1452484
2016-07-06 15:07:22
--------------------------
458611
7317
Ấn tượng về sự thông minh của Tang Tử,Thái Hậu và cả Hạ hầu tử khâm.Ngay từ đầu mình bị lôi cuốn bởi sự thông minh của Tang Tử nhưng khi gấp lại tập 4 mình thấy hơi buồn cho số phận của Tô Mộ Hàn và Thanh Dương.Các nhân vật phụ khác cũng có số phận không hề đẹp nhưng hai người kia khiến mình buồn thế đấy.Chẳng biết là tại sao."Đi theo ta,thiệt thòi cho ngươi rồi".Dường như câu nói này khắc sâu vào trong tâm trí mình vậy.Số phận của Kẻ yêu đơn phương sâu sắc chỉ có thể là đau đớn vậy thôi.May mắn cho Tang Tử và Hạ hầu là số phận an bài họ sẽ thuộc về nhau.Họ hiểu nhau,họ yêu nhau.Tình yêu của họ sâu sắc và rất ấn tượng.May mắn cho họ thật đấy.Nhưng mình vẫn hơi buồn này.
5
1124269
2016-06-25 12:18:28
--------------------------
454785
7317
Cá nhân tôi thích Tô Mộ Hàn hơn là Hạ Hầu Tử Khâm. Tô Mộ Hàn giống như ánh sáng trong lúc Tang Tử đang bị vùi dưới đất, giúp cô lớn lên, giúp cô trưởng thành. Còn Hạ Hầu Tử Khâm chỉ là một người qua đường may mắn tiện tay đổ một cốc nước làm Tang Tử nở rộ. Nam chính thực sự là người nhìn thấy nữ chính khi cô không là gì cả chứ không phải khi thấy một cô gái thông minh kiên cường. Người đáng được yêu là người không tiếc sinh mạng bảo hộ cho Tang Tử lúc mũi tên phát ra chứ không phải người thấy mà không thể làm được gì. 
5
620815
2016-06-22 09:32:53
--------------------------
437655
7317
Cuối cùng cũng xem xong trọn 4 tập "Mệnh Phượng Hoàng" của Hoại Phi Vãn Vãn. Kết thúc truyện để lại nhiều cảm nhận cho tôi. Tác giả đã khắc họa thành công  một cung cấm đầy rẫy những âm mưu và thủ đoạn của các cung phi để tranh giành tình cảm của bậc đế vương. Người ta thường nói bậc đế vương thường bạc tình nhưng khi xem xong tác phẩm này, tôi cảm thấy Hạ Hầu Tử Khâm là một vị hoàng đế anh minh đối với việc nước và cũng hy sinh nhiều vì tình yêu của mình. Kết truyện mọi thứ đều trọn vẹn, ai có tình thì sẽ được đến với nhau, dù cung cấm vẫn còn đó những tranh giành nhưng cuối cùng Tang Tử cũng bước lên ngôi vị hoàng hậu theo lời tiên đoán của vị thầy bói kia và được hạnh phúc bên Tử Khâm. 
Khi gấp trang sách lại, tôi chỉ cảm thấy đau lòng cho Tô Mộ Hàn ở phần ngoại truyện, cuối cùng y có chết không? Khi hai nhân vật chính đi Phong Sĩ đã không thấy mộ y? Vậy có được xem đây là một kết thúc mở cho y? Giống như lời Tiểu Lý Tử: "Tình đầu mãi mãi mang lại cho người ta cảm giác tốt đẹp, nhưng không phải tình yêu nào sau khi ra hoa cũng có thể kết trái… Nếu có thể thì hãy để sự đẹp đẽ trong khoảnh khắc này dừng tại đây." 

5
672200
2016-05-28 16:50:34
--------------------------
414900
7317
Sang tập 4, tác giả dường như đã mở ra cả 1 chân trời mới cho người đọc. Thay vì hoàng cung chật hẹp Hoại Phi Vãn Vãn đã đưa chúng ta ra cả chiến trận. Thay vì những toan tính chốn hậu cung là trí tuệ và tài mưu lược nơi chiến trường. Tang Tử đã trở về với dung mạo vốn có của mình. Mang theo 1 thân phận mới đầy cao quý, tôi quả thực vô cùng khâm phục tác giả đã có thể đưa từ 1 câu truyện cung đấu thông thường trở thành 1 mê cung-mỗi đi 1 mới nhưng lại chẳng cảm thấy chán nản. Tôi thấy tiếc cho Tô Mộ Hàn, cuối cùng vị tiên sinh ấy cũng chết. Giá như không phải vì mũi tên đỡ cho Tang Tử, giá như đến cuối cùng Thanh Dương không giữ gương mặt vô cảm ấy thì độc giả chắc hẳn thoải mái phần nào. Chúng ta luôn hiểu, Tô Mộ Hàn chưa từng vơi đi tình yêu đối với Tang Tử, Tô Mộ Hàn hắn ta mãi nguyện hy sinh vì người con gái mình yêu.
5
305705
2016-04-12 18:49:58
--------------------------
412555
7317
Đây là một truyện cung đấu khá hay, theo cảm nhận . Mình rất thích nữ chính của truyện này
Tang Tử-  một cô gái mạnh mẽ, thông minh. Giữa chốn thâm cung đầy rẫy hiểm ác, nàng tự lực chống chọi, có được tình yêu của bậc đế vương
Ngôi hậu là điều  nàng xứng đáng có được.
Mạch truyện liền mạch,hấp dẫn người đọc
Có một điểm mình hơi tiếc đó là kết cục của Tô Mộ Hàn. Là người thay đổi số phận của Tang Tử, cũng làm người yêu nàng sâu đậm không kém Hạ hầu tử khâm, tô mộ hàn không thể thổ lộ,bày tỏ sự quan tâm TT mà chỉ đứng nhìn từ xa
Bìa truyện hơi hướng cung đình,đẹp
5
598547
2016-04-07 22:25:53
--------------------------
409016
7317
Năm ngày hết 4 cuốn sách dày gần 500 trang, nói nhanh thì thật chẳng có gì là nhanh, bởi vì khi hết truyện rồi cảm giác vẫn rất bồi hồi, tiếc nuối. Khi nàng đến Phong Sĩ, ta mong sao nàng thấy được một dấu vết gì đó của Tô Mộ Hàn. Nhưng khi không thấy gì ta nghĩ hóa ra thế mới hay. Bởi trong tim ta vẫn mang niềm hi vọng chàng ấy sẽ có được cuộc sống êm đềm bên người yêu chàng ấy hơn sinh mệnh - là Thanh Dương. Có thể vì chàng ấy không phải là Đế vương nên tình yêu của chàng ấy trong sáng và thuần khiết hơn. Ta yêu chàng và luôn mong chàng được hạnh phúc! ( Vài lời gửi đến nhân vật yêu thích )
5
724772
2016-04-01 18:10:54
--------------------------
357290
7317
Mình thích nhất và cũng tiếc nhất cho nhân vật Tô Mộ Hàn, suốt đời anh không có hì là vui vẻ, đến cuối cùng tuy rằng sống chết không rrỗ nhưng với mình anh luôn sống. Không được đáp lại tình cảm, anh lại còn phải đứng về phe địch của đệ tử mình, cũng là người mà anh yêu. Nói về HHTK không biết sao mình thích nhất lúc tác giả viết nàng không có ai che mưa thì hắn cũng không cần. Một tác phẩm dài và ám ảnh mình rất lâu, đáng đọc trong những tác phẩm cung đấu hời hợt lan tràn như bây giờ.
4
1069868
2015-12-23 11:58:20
--------------------------
345208
7317
Không hề ngoa khi nói Mệnh Phượng Hoàng là một trong những truyện cung đấu top đầu, rất đáng đọc. Đọc Mệnh Phượng Hoàng tôi như bị cuốn sâu vào vòng xóay quyền lực trong thâm cung, bao nhiêu sóng gió thăng trầm ấy. Hoại Phi Vãn Vãn không biết đã tốn bao tâm tư để hạ ngòi bút viết được một bộ truyện hay thế này. Cung đấu là "đặc sản" của ngôn tình, một khía cạnh hay nhưng khó nhằn lại được chị khai thác thành công đến thế. Tình tiết truyện logic, có đầu có cuối, từ sự việc này dẫn dắt đến sự việc khác vô cùng hợp lý. Xây dựng tính cách nhân vật cũng chẳng chê vào đâu được. Rất hay!
5
872849
2015-11-30 00:19:13
--------------------------
341585
7317
Mặc dù đã kết thúc truyện nhưng vẫn có nhiều chi tiết khiến cho người đọc suy đoán. Như cái chết của Tô Mộ Hàn, không biết có phải ảnh chết thật không nhưng mình hy vọng ảnh vẫn sống. Như Diêu Thục phi sống trong cung như nào cũng không thấy nhắc tới, nhưng kết thúc vậy là được rồi, mọi thứ đều viên mãn. Không uổng công mình đọc xong trong bốn ngày. Mặc dù nội dung được tác giả kéo ra khá dài đọc cũng hơi ngán một chút, nhưng nếu mê cung đấu, chắc chắn sẽ không bỏ qua bộ này.
5
688967
2015-11-22 13:44:26
--------------------------
313021
7317
Tới tập bốn này thì mình đã đọc xong hoàn toàn bộ truyện này. Dù truyện rất dài nhưng chẳng cảm thấy tiếc thời gian chút nào cả mà chỉ cảm thấy vui và hạnh phúc vì mình vừa mới thưởng thức xong một bộ truyện tuyệt vời. Tới đây không còn đơn thuần chỉ là những đấu đá trong cung nữa mà là một loạt những khó khăn trong chiến tranh giữa các phương. Tới hồi kết thì mọi bí mật về các nhân vật và tâm lí đã được giải quyết. bạn sẽ thấy ngưỡng mộ và yêu thích nhân vật Tô Mộ Hàn cực kì. Chàng là một mẫu người lí tưởng với tình yêu thầm lặng và hy sinh vô hạn. Kết thúc truyện cũng đặc biệt có hậu làm vừa lòng mọi thế hệ độc giả.
5
636935
2015-09-22 17:37:14
--------------------------
309593
7317
Vậy là đã tập cuối rồi, đọc xong cả bốn tập truyện mà chẳng muốn dừng chút nào, chẳng muốn nó kết thúc chút nào. Truyện của Hoại Phi Vãn Vãn thực sự gây ra cho người đọc nỗi ám ảnh, nỗi buồn và sự ấn tượng trong việc xây dựng cốt truyện và hình tượng nhân vật. Và rồi cứ thế ta cầm cuốn truyện lên để đọc, để tận hưởng cái ám ảnh đó- dần dần trở thành chất gây nghiện cho mình, khiến cho mình hễ đọc là không ngừng được, hễ cầm lên là không bỏ xuống được và hễ đọc xong là không ngừng ám ảnh 
5
190618
2015-09-19 01:30:29
--------------------------
309471
7317
Đây là bộ cung đấu đầu tiên mà mình đọc nên nó đã để lại rất nhiều cảm xúc cho mình. Mặc dù phải trải qua tận 4 tập mới luyện hết bộ này nhưng đây vẫn là bộ truyện cung đấu đáng đọc vì nó rất lôi cuốn và thú vị. Mình rất xót cho nhân vật nam phụ Tô Mộ Hàn, thích anh này nhất xong đến Cố Khanh Hằng rồi cuối cùng mói tới nam chính Hạ Hầu Tử Khâm @@. Dù cái kết là HE nhưng mình vẫn thấy không trọn vẹn lắm nhưng biết sao được vì thể loại cung đấu này là thế mà!
5
536908
2015-09-18 23:51:44
--------------------------
278149
7317
Đây là bộ truyện cung đấu mà mình thích nhất. Trong tập cuối này cảm thấy xót xa cho Tô Mộ Hàn - Thái tử tiền triều, dù yêu Tang Tử nhưng vẫn chấp nhận buông tay vì biết mình không sống được bao lâu nữa, giữ nàng lại chỉ làm khổ nàng nên chỉ có thể thành toàn cho tình yêu của Tang Tử nhưng dù vậy vẫn âm thầm bảo vệ nàng, ấn tượng nhất với câu nói của Tô Mộ Hàn "Ta cùng lắm chỉ trộm của nàng một viên trân châu, nhưng nàng thì trộm mất cả trái tim ta" . Cũng cảm thấy xót xa cho Cố Khanh Hằng - tri kỷ và cũng là thanh mai trúc mã của nàng, chàng là người gặp Tang Tử đầu tiên nhưng chàng không phải là người cuối cùng của nàng. Trải qua bao chông gai, bao thử thách cuối cùng Tang Tử cũng lên ngôi hoàng hậu như lời dự đoán năm xưa. Dù cái kết không được như ý muốn nhưng có lẽ đây cũng là cái kết viên mãn dành cho bộ truyện này.
5
678496
2015-08-25 17:35:50
--------------------------
251180
7317
Mình đã đọc truyện này trên mạng và cảm thấy rất thích nên quyết định mua đủ bộ về để khi nào thích thì có thể lấy ra đọc lại ngay. Mình thích cách tác giả xây dựng tính cách nhân vật và diễn biến của truyện. Đọc truyện có cảm giác tò mò không biết số phận các nhân vật như thế nào, đặc biệt là Tô Mộ Hàn. Đây là nhân vật mình thích nhất trong truyện này. Lúc đọc cũng hi vọng sẽ có 1 happy ending cho cả 3 nhân vật nhưng khi đọc cái kết của tác giả, cảm thấy đây là 1 cái kết cũng khá hợp lý. 
5
180086
2015-08-02 15:08:18
--------------------------
239375
7317
Thực sự mà nói thì truyện rất hay, rất thích Tang Tử, không những vậy cũng rất thích Tấn vương và Hiểu vương, nhưng không thích Hạ Hầu Tử Khâm lắm, Hạ Hầu Tử Khâm đối với Tang Tử như vậy, người đọc sẽ thấy quá mức bất công, nhỉ ? Về bản chất Hạ Hầu Tử Khâm quá dung túng cho Phất Hy, nếu Phất Hy không phải nhân vật phản diện, nếu Phất Hy thực lòng đối tốt với A Tử giống như Tô Mộ Hàn đối tốt với Tử Khâm thì cũng đáng, nhưng Phất Hy lại như vậy, nó ra là xấu tính, vậy có đáng không. Nhiều người cho rằng vị trị của Phất Hy là Tô Mộ Hàn là như nhau, đều là nam phụ và nữ phụ, nhưng lại không nghĩ đến Tô Mộ Hàn tốt như vậy, còn Phất Hy như thế nào. Thực ra mà nói, kết thúc như vậy đối với Tô Mộ Hàn, quá tội !
5
309218
2015-07-23 21:05:21
--------------------------
237891
7317
Đây là tác phẩm cổ trang hay nhất mà mình từng đọc vì không đơn giản chỉ là cung đấu giữa các phi tần mà còn là cảnh ra trận của một phi tần một Tang Tử thông minh tuyệt đỉnh. Những nhân vật nam như Hạ Hầu Tử Khâm, Tô Mộ Hàn hay Cố Khanh Hằng mình đều thích vì mỗi người có một nét riêng khiến người ta thích tất cả đều ở bên Tang Tử. Kết thúc đẹp thật sự viên mãn cả đôi đường và Mệnh Phượng Hoàng của thầy bói năm nào đã thành sự thật,
5
464921
2015-07-22 20:44:22
--------------------------
225191
7317
Truyện rất dài nhưng trải dài câu chuyện là nam nữ chính mà mình lại không thích nữ chính nên thấy bộ này cũng bình thường. Bạn nào muốn thử cảm giác tiếc nuối, ức chế đến nghiến răng kèn kẹt thì hãy đọc bộ này. Nam nữ chính không thuộc dạng mình thích, nữ chính không phải giỏi giang và suy nghĩ không thấu đáo mà thuộc kiểu ăn may. Chỉ tiếc cho 2 nam phụ. Biết là cung đấu thì không tránh khỏi hi sinh nhưng thật sự mình đã mong bộ này SE. Để cho Tô Mộ Hàn ra đi như vậy, để cho Cố Khanh Hằng vứt tương lai của mình đi như vậy thật không cam lòng.
3
212906
2015-07-09 22:50:07
--------------------------
221699
7317
Mệnh Phượng Hoàng và Thượng Cung là hai quyển cung đấu mà mình thích nhất. Nếu như trong Thượng Cung nữ chính Ninh Vũ Nhu, ngay từ đầu đã sắc sảo, đa đoan thì trong Mệnh Phượng Hoàng nữ chính Tang Tử lúc đầu chỉ là một cô bé, cô bé ấy từ từ trưởng thành qua năm tháng, qua mỗi biến cố. Không thể phủ nhận trong quá trình đó Tang Tử nhận được không ít sự giúp đỡ đặc biệt là Tô Mộ Hàn.
Dù là hoàng đế trên vạn người Hạ Hầu Tử Khâm, tiên sinh Tô Mộ Hàn hay người luôn bên cạnh bảo vệ nàng Khanh Hằng, ba người với tình yêu làm cho mình cảm thấy vô cùng xúc động.
Hạ Hầu Tử Khâm- lúc đầu không thích nam chính này bởi vì trước đó hắn cũng có người để thích… Nhưng bên cạnh Hạ Hầu Tử Khâm, Tang Tử thật sự rất vui…Tình yêu của hắn đối với nữ chính không phải vừa gặp đã yêu, mà được bồi đắp rồi trở nên vô cùng sâu nặng đến nỗi có thể dùng “giang sơn, tính mạng đổi mỹ nhân” (Mặc dù hắn chẳng để chuyện đó xảy ra)
Tô Mộ Hàn: đây là nhân vật gây xúc động mạnh, hắn- một người chẳng tranh với đời, hắn chỉ muốn một cuộc sống bình yên bên “Tử Nhi” của hắn. Nếu hắn vượt qua tấm rèm thì đã với tới nàng nhưng “giang sơn đã đổi” hắn vốn không thể đem lại cho nàng thứ nàng muốn thôi thì đành rụt tay, mảnh đời còn lại của hắn cũng hy sinh tất cả vì nàng.
Cố Khanh Hằng: có lẽ đối với Tang Tử đây chỉ là một tri kỷ, bạn thanh mai trúc mã. Khanh Hằng là người gặp nàng đầu tiên. Những tưởng “đợi nàng lớn lên thì ta sẽ được ở bên nàng trọn đời” nào ngờ tâm nàng chẳng đặt vào đây. Một chữ “Được” đáp lại lời nhờ vả xin suất nhập cung của nàng tựa như ngàn cân. Khanh Hằng cũng vì “Tam Nhi” của hắn mà không tiếc mạng sống… Đối với hắn một lần gặp gỡ là cả đời chỉ một.
5
121629
2015-07-04 11:09:00
--------------------------
218981
7317
Đây thật sự là một truyện cung đấu rất hay, với cốt truyện hấp dẫn, có nhiều tình huống kịch tính, những đợt sóng dồn dập từ những chương đầu tới tận cuối truyện. Hai nhân vật chính là Tang Tử và Hạ Hầu Tử Khâm được khắc họa hết sức ấn tượng, mình đặc biệt thích tính cách mạnh mẽ bảnh lĩnh của Tang Tử cũng như cách mà hai người tin tưởng vào nhau. Mình thích cả Tô Mộ Hàn nữa, anh ấy là sư phụ, cũng là nam phụ tuyệt nhất, vậy mà cuối cùng là không sống được, thấy hụt hẫng vô cùng.
5
630600
2015-07-01 01:34:46
--------------------------
210690
7317
Kết thúc đẹp – Sau bốn tập dày với biết bao sóng gió thì cũng là lúc kết thúc trọn vẹn. Mệnh Phượng Hoàng đã trở thành hiện thực, Tang Tử lên làm hoàng hậu. Mặc dù truyện vẫn còn khá khiên cưỡng và tính kịch, nhưng chung lại cũng chấp nhận được. Người khiến bản thân mình tiếc nhất là Cố Khanh Hằng, trong ba nam chính có lẽ anh bị “hắt hủi” nhất, một tình yêu thầm lặng nhưng không kém mãnh liệt được gìn giữ trong khoảng cách - hộ vệ. Nói chung truyện không xuất sắc nhưng hay, đọc được.
4
650262
2015-06-19 18:47:15
--------------------------
187875
7317
Hết rồi, kết thúc rồi. Đọc xong cả tuần rồi mà vẫn còn ám ảnh. Yêu Hậ Hầu Tử Khâm, Yêu Tang Tử, xót Tô Mộ Hàn. Cái kết không nói rõ Tô Mộ Hàn có chết không, mình hi vọng chàng sẽ không chết, sẽ sống tốt ở một nơi nào đấy ... thương chàng vô cùng - không có mĩ nhân, không có cả giang sơn. Hạ Hầu Tử Khâm có tất cả, Tang Tử một lòng với chàng, dù hiểm nguy cũng quyết giữ lại giang sơn cho chàng.
Bộ Ngôn tình - Cung đấu mà mình thực sự rất thích. Nam Nữ chính đều rất thông minh, có thể làm chủ vận mệnh của mình, dù tình huống có khó khăn cũng luôn có cách vượt qua, vì người kia mà vượt qua. 
Bộ này mà lên phim thì quá hay, không biết ai vào vai Nam Nữ chính mới hợp nhỉ?
5
371434
2015-04-24 11:07:26
--------------------------
181745
7317
Đây là bộ ngôn tình cung đấu đầu tiên mà mình đọc. Rất hay nhưng cũng quá đổi xót xa. Tô Mộ Hàn là nhân vật mà mình thích nhất và cũng làm mình đau lòng nhất vì cuộc đời bi kịch đó. Sau một đêm đầy lửa, số phận đã thay đổi tất cả. Tính cách bí ẩn có phần lạnh lùng nhưng tinh thông và quyền lực đã làm mình lầm tưởng rằng anh chính là nv chính của câu chuyện. Qua một đem đầy lửa, từ một thái tử sắp kế vị lại mất đi cả giang sơn, nói xa hơn là người mình yêu và cả sức khỏe. Tác giả đã xây dựng hình tượng Tô Mộ Hàn quá đẹp, đến nổi mình không thể nào yêu mến được nam chính của truyện. HHTK và Tang Tử có lẽ đã được định sẵn là một đôi, nhưng vì sao tác giả lại để cho TMH hi sinh vì TT nhiều đến vậy? Những lần cô gặp nguy, không hề có sự xuất hiện của HHTK mà lại là TMH, anh là người hứng chịu tất cả cho TT, từ máu me cho đến cánh tay phải, cuối cùng là mạng sống của chính mình. Quá bất công. Mình đã từng mong câu chuyện sẽ có một SE cho tất cả. Nhưng cái chết của TMH có vẻ sẽ là một sự giải thoát cho tất cả. Thật đau lòng...
3
50402
2015-04-12 13:07:47
--------------------------
178372
7317
Các tấm màn bí mật được vén lên, được giải thích rõ ràng và hợp lý. Thế nhưng, trong tập cuối, người khiến tôi ấn tượng nhất không còn là Hạ Hầu Tử Khâm, mà là Tô Mộ Hàn, người thái tử tiền triều có số phận long đong. Yêu mà không thể nói, chỉ có thể thành toàn cho tình yêu của Tang Tử; mang gánh nặng gia tộc trên vai, dù không màng danh lợi, nhưng lại không thể vứt bỏ; mang tấm thân bệnh tật triền miên, chẳng được sống một ngày khỏe mạnh. Chàng ra đi mà không nhìn mặt Tang Tử lần cuối, vì không muốn để nàng phải bận lòng. Rốt cuộc chàng yêu Tang Tử đến thế nào? Kết thúc viên mãn dành cho hầu hết mọi người, những người có tình đều hạnh phúc bên nhau, chỉ trừ Tô Mộ Hàn.
Tóm lại, Mệnh phượng hoàng xứng danh là đỉnh cao cung đấu. Đây cũng là một trường hợp hiếm hoi mà tôi thấy tựa truyện sau khi đổi lại hay hơn tựa gốc (Từ thứ nữ đến hoàng hậu).
5
57459
2015-04-04 21:46:58
--------------------------
170363
7317
Mình ít khi đọc truyện cung đấu, thường chỉ đọc vài chap rồi bỏ dở vì ngại, duy có truyện này là đọc từ đầu đến cuối. Nhân vật nữ chính có cá tính mạnh và thông minh, tuy nhiên hình như các phi tần trong hậu cung hiếm ai có phẩm chất đủ tốt để được sủng ái (dù mưu hèn kế bẩn thì có thừa), nên đôi lúc mình cảm thấy quá hiển nhiên cho việc nữ chính không có đối thủ. Nam chính thì vừa mạnh mẽ vừa ấm áp, tính tình thú vị, lại chân thành yêu nữ chính. Điểm đáng tiếc nhất trong truyện chắc là Tô Mộ Hàn, từ một thái tử tương lai có trong tay tất cả, để rồi chỉ sau một đêm thiên hạ đã đổi chủ, cả một triều đại mất đi nhẹ nhàng mà nuối tiếc như vậy, rồi đến khi người chết đi cũng lặng lẽ và vô danh như thế. Mỗi chap truyện khá là dài, mình đọc muốn hộc hơi, nhưng truyện này có cốt truyện hay, gây tò mò, đã đọc thì khó rời mắt ra được.
4
513440
2015-03-19 22:06:53
--------------------------
167111
7317
Tập cuối khép lại câu truyện với kết thúc vừa đáng yêu, nhẹ nhàng, giải quyết những bí mật được mở ra ở các phần trước.
Đúng như mình dự đoán, Tô Mộ Hàn là Thái tử tiền triều, phổi bị tổn thương trong trận phóng hỏa năm ấy. Hàn ca có lẽ là nhân vật mà mình quan tâm nhất trong toàn bộ câu truyện, hơn cả những nhân vật chính, nỗi đau đớn bệnh tật, thình yêu, tình cảm gia đình giày vò không đáng có trên con người không màng danh vọng ấy, chỉ vì anh là Thái tử của một triều đại đã qua
Tập cuối mới thấy Hạ Hầu Tử Khâm lộ ra vẻ si tình đích thực, cảnh nhõng nhẽo như trẻ con đòi được quan tâm đầu tiên, ghen tị với Tô Mộ Hàng vì được Tang Tử khóc thương, rồi lại kêu đau tim khi chị khóc thật là đáng yêu biết mấy :))) 
Mình cũng có ấn tượng tốt với hoàng đế đại tuyên, có thể thoáng thấy tình cảm của anh này với Tang Tử là tình huynh muội thực sự, dù quan hệ ấy chỉ vì lí do chính trị\
Nhưng nói gì thì nói, kết thúc câu truyện cung đấu này bằng việc các phi tần khác đều bị thất sủng, thất thế, mình vẫn cảm thấy có tí hụt hẫng, đang đấu nhau hay như thế kia mà... :))
4
310296
2015-03-14 00:10:12
--------------------------
166553
7317
So với các tập khác, tôi không thích tập 4 bằng, dù tập 4 hé lộ tất cả bí ẩn trong các tập trước. Có lẽ là do chuyện cung đấu và chuyện nảy sinh tình cảm không còn là trọng tâm, tập này kể chuyện đôi nhân vật chính cùng nhau vượt qua thử thách để tìm đến hạnh phúc của mình. Nhưng tôi rất thích phần kết, vì nó thỏa đáng - người tốt hưởng hạnh phúc, kẻ xấu chịu trừng phạt.

Hai nhân vật chính thực sự được xây dựng rất hay, rất cuốn hút và cá biệt. Tình yêu của họ rất cảm động và để lại nhiều ám ảnh.
4
557111
2015-03-12 21:24:56
--------------------------
164234
7317
Dù biết Tô Mộ Hàn sẽ không thể sống thêm đưojc nữa nhưng tôi cũng giống như Tang Tử mong rằng anh có thể sống. Dù biết là suy nghĩ ích kỉ nhưng tôi cũng không đành lòng khi biết TMH đã ra đi. Tình cảm mà TMH dành cho TT quả thực quá thầm lặng, nhiều lần tôi đã không nén nỏi xúc động. Tôi cũng tiếc cho Cố Khanh Hằng nữa, một đời thầm lặng đi bên cạnh bảo vệ một người con gái, chỉ cần nàng sống tốt thì anh cũng sẽ thấy hạnh phúc. Tôi vẫn thích Khanh Hằng với Tang Tử hơn nhưng có lẽ HHTK và TT sinh ra là để dành cho nhau rồi. Tác giả đã xây dựng tuyến nam phụ quá hoàn hảo khiến tôi không thể nào thích nổi nam chính. Kết truyện có lẽ là phần ai cũng biết: Tang Tử trở thành hoàng hậu đúng như lời tiên đoán năm ấy, nhưng có một điều không ngờ và cũng khiến tôi rất xúc động là Dư thái y cùng An  Uyển Nghi trốn khỏi cung một cách trót lọt, bắt đầu một cuộc sống khác đầy tình yêu. Còn Vãn Lương cô cũng may mắn sinh hạ con trai cho Tấn Vương và được phong làm Tấn Vương phi. Một kết cục không thể mỹ mãn hơn.
Tuy nhiên truyện cũng để lại cho tôi nhiều tiếc nuối như chuyện tình của Thừa Diệp và Phất Dao. Dù chỉ là TMH kể lại nhưng cũng khiến tôi cảm thương sâu sắc cho cặp uyên ương xấu số này và cũng thêm căm ghét Phất Hy
5
80511
2015-03-07 08:40:18
--------------------------
162668
7317
Tập 4 này đã vén bức màn bí ẩn cho rất nhiều chuyện ở tập 3, mọi chuyện đều rất hợp lí và logic với những tập trước.
Ở tập cuối này, Tang Tử đã chứng minh được mình hoàn toàn xứng đáng với ngôi vị Hoàng hậu, xứng đáng với tình yêu thương trọn đời của Hạ Hầu Tử Khâm. Liệu có nữ nhân nào dám hi sinh lòng tự kiêu của mình, để đồng ý trở thành một "phế phi", cuối cùng "bệnh mà chết", khiến các phi tần khinh rẻ? Liệu có nữ nhân nào đã có được một danh phận công chúa cao quý, được nghĩa huynh quý trọng, mà vẫn bướng bỉnh ra chiến trường, chịu gian khổ, thậm chí sẵn sàng đón nhận cái chết để bảo toàn cho giang sơn của người mình yêu? Còn ai xứng đáng hơn Đàn phi, có được vị trí của bậc mẫu nghi thiên hạ ấy?
Cái kết cho tất cả mọi chuyện đã gần viên mãn. Kẻ chống chịu phạt, kẻ theo được trọng. Mình đặc biệt chú ý đến câu chuyện tình yêu đẹp và rất có hậu của An Uyển nghi, đó là phần thưởng xứng đáng cho những công sức mà nàng và người nàng yêu thương bỏ ra.
Nhưng đau đớn nhất để lại trong tâm hồn người đọc là Tô Mộ Hàn. Số mệnh bạc bẽo của y, con người của y, tình yêu tha thiết y dành cho Tang Tử đã làm mình rơi nước mắt. "Ta cùng lắm chỉ trộm của nàng một viên trân châu, nhưng nàng thì trộm mất cả trái tim ta." Có lẽ, sau khi đọc Mệnh phượng hoàng, đây là khoảng trống lớn nhất mà truyện để lại cho mình.
Như lời tiên đoán về mệnh phượng hoàng mấy năm trước, Tang Tử đã trở thành hoàng hậu. Nhưng quãng thời gian ấy phải đánh đổi bằng biết bao công sức, nước mắt, hi sinh biết bao người như Triêu Thần, Tình Hòa ..., phải qua biết bao nhiêu thử thách, gian khó.
Một bộ cung đấu kinh điển!
5
333831
2015-03-02 23:11:23
--------------------------
155089
7317
Đã kết thúc Mệnh Phượng Hoàng dài ròng rã bốn tập. Là sự viên mãn hạnh phúc của Hạ Hầu Tử Khâm và Tang Tử, nhìn ngài lạnh lùng uy nghi trên triều vậy mà có lúc cũng con nít, nhõng nhẽo rất đáng yêu khi ở bên vợ. ^_^ Sau này Tang Tử đã được làm hoàng hậu như đúng lời tiên đoán Mệnh Phượng Hoàng.
Người khiến mình tiếc nuối nhất là Cố Khanh Hằng - tình yêu được giữ gìn trong từng khoảng cách, hy sinh cả một cuộc đời thăng tiến chỉ để làm hộ vệ, đứng từ xa bảo vệ nàng, nhìn nàng hạnh phúc. Còn Tô Mộ Hàn - mình hoàn toàn không thể hiểu hay cảm được tình yêu của người này với Tang Tử, có chăng thích và thương chàng chỉ ở tập 3 khi hai người cùng mắc ở chân núi đá mà thôi.
Tuy vậy, những con người trong Mệnh Phượng Hoàng đã tìm cho mình một kết cục tốt đẹp nhất có thể, như vậy là trọn vẹn rồi.
4
382812
2015-01-31 08:29:05
--------------------------
152972
7317
Tập 4 kể về cái kết có hậu của truyện. Rốt cung, Tang tử sau nhiều thử thách đã thực sự trở thành chủ nhân chốn hậu cung được vạn dân kính nể như ước nguyện ban đầu của nàng. Thế nhưng, nàng cũng phải trả giá rất nhiều cho tham vọng ấy. Hai nam phụ là Cố Thanh Hằng, Tô Mộ Hàn thật đáng thương vì dù rất yêu nàng nhưng lại chỉ có thể đứng từ xa làm mọi điều bảo vệ. Cái kết hơi dông dài, gây cảm giác hơi vô lí cho người đọc nhưng là cái kết đẹp cho bộ truyện!
5
292697
2015-01-24 20:46:55
--------------------------
152607
7317
"Trong hoàng thất vẫn có chân tình." và Hạ Hầu Tử Khâm đã chứng minh được điều đó. Trong tam cung lục viện, chỉ muốn tìm một nữ tử có thể cứng rắn, vững chãi, thông minh, không cần sự bảo vệ của hắn (vì hắn biết mình không thể làm được điều đó), một chữ "Đàn" đã ẩn chứa kỳ vọng, tình yêu thương, sự chúc phúc của hắn dành cho Tang Tử. 
Tang Tử chấp nhận xông pha trận mạc, đấu trí với Tô Mộ Hàn, "chỉ cần là giang sơn của hắn thì nàng có chết cũng phải bảo vệ". 
Với Tô Mộ Hàn chỉ một câu nói "Ta trộm của nàng một viên trân châu, nhưng nàng thì trộm mất trái tim ta", nghe tha thiết nhưng thật nhói lòng. Một tiên sinh lạnh nhạt sau bức màn che, đã Tang Tử tất cả cả tài năng, nhưng lại không cách nào níu kéo nàng lại. Giả dụ... chỉ giả dụ thôi, nếu Tô Mộ Hàn không bị thương nặng, không biết trước sinh mạng ngắn ngủi của mình thì liệu có ra sức ngăn cản Tang Tử nhập cung, và đề nghị nàng đi theo mình không? Cũng có thể lắm... Tô Mộ Hàn như một nốt nhạc trầm bổng lướt qua làm nhói đau trái tim người đọc, có ngài mới có Tang Tử hôm đó, và cũng vì có ngài mới có Công chúa Đại Tuyên, tức Hoàng hậu thiên triều hôm nay.
Tác giả đã dùng "kế trong kế" rất hay, chưa đến phút chót vẫn không biết được mình đã bị ai hại, chốn cung cấm là thế, trên chiến trường cũng thế... nhưng mình hoàn toàn mãn nguyện với kết cục, Tang Tử trở thành hoàng hậu  được Hạ Hầu Tử Khâm vĩnh viễn yêu thương, bảo vệ. Cái kết của Tô Mộ Hàn nào ai có biết chắc, có thể ngài đã... hoặc có thể vẫn chưa... nhưng chỉ biết ngài luôn một lòng mong cho người trong tim luôn hạnh phúc. Một Cố Khanh Hằng với nụ cười đẹp như gió thổi, không tiếc thân mình bảo vệ Tang Tử, bảo vệ giang sơn xã tắc, cuối cùng được trở về phục chức và dạy võ công cho hoàng tử.
5
336728
2015-01-23 16:56:51
--------------------------
150457
7317
1 cuốn truyện cung đấu hấp dẫn đến tận những trang sách cuối cùng. 1 cuốn truyện làm mình phải đọc đi đọc lại không biết bao lần mà không chán. 1 Tang Tử mạnh mẽ thông minh, 1 Hạ Hầu Tử Khâm đã chứng minh rằng hoàng gia cũng có chân tình. 2 anh chị đã vượt qua rất nhiều khó khăn những âm mưu thủ đoạn để cuối cùng có thể hạnh phúc mỹ mãn ở bên nhau. Có lẽ tiếc nuối nhất của truyện chính là sự ra đi của Tô Mộ Hàn. Có lẽ số mệnh của anh do anh sinh ra là thái tử. 1 con người không tranh đấu nhưng vẫn luôn bị đưa vào trong những âm mưu quyền lực. 1 tình yêu khắc cốt ghi tâm sẵn sàng hy sinh để bảo vệ người mình yêu nhưng không thể bày tỏ, chỉ có thể chúc phúc cho người mình yêu ở bên 1 người đàn ông khác. Có lẽ cái chết của anh chính là sự giải thoát đối với anh nhưng lại để lại nhiều tiếc nhuối cho những người luôn ở bên cạnh anh và cho nhiều độc giả
5
489860
2015-01-16 19:59:17
--------------------------
144721
7317
Từ đầu đến cuối ta vẫn yêu Hạ Hầu Tử Khâm tha thiết, nếu cho ta chọn giữa chàng và Cố Khanh Hằng hay Tô Mộ Hàn, ta cũng đều chọn y. Vậy cho nên kết cục tuy làm ta đau lòng vô cùng song cũng hạnh phúc vô cùng, quá trình có nhiều tiếc nuối, trong cuộc đấu tranh giành chốn hậu cung phải hy sinh biết bao người, phải ra đi biết bao người, khóc hết nước mắt khi Triêu Thần ra đi, khi Tô Mộ Hàn không còn, khi thấy sự thanh thản của Khanh Hằng khi huynh ấy ra biên cương,... khép cuốn sách mà nước mắt rơi không ngừng, ta yêu Hạ Hầu Tử Khâm, Cố Khanh Hằng, Tô Mộ Hàn... cực yêu
5
95900
2014-12-27 20:35:35
--------------------------
137341
7317
Đó giờ luôn thích cung đấu,nhưng chỉ xem phim thôi,còn đọc tiểu thuyết thì chưa vì lười quá.Lúc dạo trên Tiki thì thấy cuốn này,bị thu hút bởi cái tên "Mệnh phượng hoàng",có thể đoán nội dung là 1 nữ tử bình thường từng bước tiến lên hoàng hậu rồi,sau quyết định mua khi đọc xong giới thiệu : nữ chính thông minh sắc sảo,tham vọng đúng kiểu mình thích + bìa truyện trình bày quá đẹp.Đó giờ cung đấu hầu hết toàn thấy chủ yếu là nữ nhân hậu cung với nhau,anh thượng 1 là bù nhìn,2 là vô tình,giờ bộ này anh Khâm quả là minh quân + cực phẩm nam nhân,Tang Tử dù thông minh nhưng vẫn kém hơn anh 1 bậc,hầu như cô nghĩ gì làm gì ảnh đều biết hết,tính trước hết,hành đông xuất quỷ nhập thần,lại chung tình,tin tưởng cô tuyệt đối.Những lúc 2 người bên nhau vừa thú vị,vừa mùi mẫn kinh khủng,rất là ngọt,ngược cũng không quá nhiều,vì tin yêu nhau tuyệt đối mà,nên đọc rất mãn nguyện.Tới quyển 4 Tô Mộ Hàn tái xuất thì quả thật bắt đầu khóc hết nước mắt,anh Khâm dù cũng lao tâm khổ tứ nhưng cuối cùng vẫn có giang sơn và mỹ nhân,còn anh Hàn vốn là huyết mạch hoàng tộc lại chịu đủ tai ương,bệnh tật dày vò,các bên tranh giành lợi dụng,những gì A Tử có được hôm nay đều do Tô Mộ Hàn cho,cuối cùng chẳng đâu vào đâu,kết mở nhưng vẫn thấy bất công cho anh T__T ~ ghét nhất truyện chắc là cha & mẹ kế của A Tử + Dao phi.Tình tiết chặt chẽ,hấp dẫn,mưu kế của các phi tần đều thâm sâu,mà chắc thâm nhất là Thái Hậu rồi
5
162987
2014-11-25 22:52:13
--------------------------
136522
7317
Đọc truyện mà cảm xúc hạnh phúc, xúc động, hận thù cứ đan xen. Những câu chuyện  xoay vòng trong thế giới quyền lực tàn khốc đó được tác giả dẫn dắt hết sức lôi cuốn, bạn sẽ chẳng đoán trước được việc gì sẽ xảy ra sau đó, cũng như từng suy nghĩ hành động của nhân vật được phân tích cực kỳ logic, 2 điều này đã làm nên điều tuyệt vời của tác phẩm, 1 khi cầm lên sẽ chẳng bao giờ dừng lại được. 
Hận là vì nhiều lúc HHTK vẫn để Dao Phi trong lòng, hận là khi hiểu lầm TMH lợi dụng Tang Tử, hận nhiều khi người ta vì quyền lực mà hãm hại nhiều người vô tội. 
Nhưng chợt vỡ òa, xúc động khi biết trong chốn thâm cung ấy vẫn tồn tại những tình cảm chân thật đến vậy. HHTK lựa chọn yêu TT, sự kiêu ngạo của bậc đế vương hắn cũng vứt bỏ, đến cả tính mạng hắn cũng chẳng cần. Xúc động khi CKH quyết hoàn thành lời hứa ngây ngô lúc bé mà không màng nguy hiểm vào cung bảo vệ TT. Đặc biệt là TMH, "hắn chỉ lấy trộm của nàng 1 viên trân châu mà nàng lại lấy đi của hắn 1 trái tim", đến giây phút cuối cùng của cuộc đời, hắn cũng vì nàng mà đỡ lấy mũi tên ấy. 
Có lẽ lúc đầu bạn sẽ cảm thấy 1 Tang Tử đáng thương nhưng đến cuối nàng là người khiến người ta ngưỡng mộ nhất. Không ngưỡng mộ sao được khi có 3 người yêu thương nàng hết mực, vì nàng mà quyết hi sinh tính mạng. Bằng sự thông minh và lý trí của mình, Tang Tử đã có được những thứ xứng đáng thuộc về mình. Cái kết rất hợp lý và cũng là cái kết trọn vẹn cho tất cả nhân vật trong truyện.
5
180814
2014-11-21 19:32:51
--------------------------
125591
7317
Càng đến khúc cuối cùng mạch truyện ngày càng hấp dẫn giống như là chạy nước rút vậy ta phải liên tục lật sang trang này để theo dõi diễn biến cuối cùng . Vì nếu bỏ lỡ dù chỉ là một tình tiết nhỏ nhoi thôi cũng quyết định đến toàn bộ câu truyện . Nói thật tôi rất hài lòng về cái kết này của Hoại Phi Vãn Vãn . Sau khi giả chết lúc quay về Tang tử đã không còn là Đàn Phi tướng mạo tầm thường năm xưa . Nàng đã lột xác trở thành một công chúa ai ai cũng ngưỡng mộ . Trí thông minh của nangf thì không cần bàn cãi đến nữa . Kết thúc Tang Tử trở thành hoàng hậu ở bên cạnh Hạ Hầu Tử Khâm . Rốt cuộc lời nguyền về mệnh phượng hoàng đã trở thành sự thật . Từ một người con của tiểu thiếp bị khinh rẻ nàng đã trở thành người dưới một người trên vạn người . Đọc truyện mình đã rất thích nhân vật tang Tử rồi những kẻ ác đều bị trừng trị thích đáng . Thiên Phi thì bị điên Thiên Lục thì đi tu âu đó cũng là cái kết có hậu cho tất cả
5
337423
2014-09-14 11:21:56
--------------------------
123000
7317
Truyện dài nhưng không gây nhàm chán bởi cốt truyện hấp dẫn, nhiều tình huống kịch tính, những đợt sóng ngầm chốn thâm cung trải dài từ đầu tới cuối truyện.
Hình tượng Tang Tử và Hạ Hầu Tử Khâm được khắc họa hết sức rõ nét và đầy ấn tượng, họ thật đúng là một đôi trời sinh. Khâm phục nhất ở chỗ họ yêu nhau, tin tưởng nhau một cách tuyệt đối. Nam phụ Cố Khanh Hằng và Tô Mộ Hàn cũng rất tuyệt, vừa tài giỏi vừa si tình, chỉ hơi tiếc vì cuối cùng Mộ Hàn không sống được.
Điểm đặc biệt là truyện không hề dùng cảnh nóng để câu khách, mình rất hài lòng.
5
393748
2014-08-29 12:48:17
--------------------------
116594
7317
Rỏ ràng đề tài hậu cung tranh giành quyền lực là một đề tài không phải là mới trong giới ngôn tình, nhưng Hoại Phi Vãn Vãn vẫn rất thành công trong việc khai thác triệt để đề tài này. Về nội dung, cốt truyện được xây dựng khá phức tạp, các tình tiết đan xen chồng chéo, khơi gợi trí tò mò của người đọc nhưng lại không rối rắm. Về yếu tố tình cảm, rõ ràng tác giả khá thành công trong việc miêu tả cảm xúc của nhân vật phụ, nhưng có lẽ lại không thành công lắm trong nhân vật "ta", bởi lẽ tâm tình của nhân vật rất chồng chéo phức tạp, nên tác giả miên tả còn hơi gượng ép, chưa mang đến cái nhìn trọn vẹn cho nhận vật. Về hình thức, có lẽ vì là khai thác quá sâu và triệt để các câu chuyện nơi cung đình như từ đấu đá giữa các phi tử đến chiến tranh và tranh chấp giữa các quốc gia... nên về dung lượng thì bộ sách lại trở nên quá dài, gấy bất lợi cho người đọc. Tuy nhiên, tuy dài nhưng truyện đi theo một cốt truyện xây dưng rất rõ ràng và logic, những cao trào hợp lý và cách đóng mở vấn đề khá phù hợp. Truyện là một tác phẩm khá hay, không làm uổng phí thời gian của độc giả.
4
377625
2014-07-09 23:34:05
--------------------------
116228
7317
Chưa từng có một bộ ngôn tình nào có nhiều nhân vật tôi thích như "Mệnh phượng hoàng". Mỗi nhân vật đều được tác giả với một cá tính riêng. Tang Tử mạnh mẽ, thông minh, thấu hiểu lòng người, tuy mưu mẹo nhưng rất lương thiện. Hạ Hầu Tử Khâm lại gây ấn tượng với niềm tin tuyệt đối chàng dành cho Tang Tử. Tô Mộ Hàn khiến người ta thương tiếc khôn nguôi. Còn Cố Khanh Hằng là người tạo cảm giác yên bình, dễ chịu. Tôi còn thích rất nhiều nhân vật khác như thái hậu, Phương Hàm, Vãn Lương, Triêu Thần, Tình Hòa, Thiên Lục, An Uyển nghi, Từ thái y, Ngọc Tiệp dư, Tuyên hoàng, Thanh Dương, Liêu Hứa, Tấn vương, Hiển vương, Tiểu Lý Tử, thậm chí cả Hàn vương và Phất Diêu. Mỗi nhân vật đều để lại trong tôi những cảm xúc riêng, nuối tiếc có, dễ chịu có. Hoại Phi Vãn Vãn viết rất chắc tay, không có chỗ nào bị phô cả, mạch truyện cũng rất logic, cuốn hút. Mong rằng có thể được đọc tiếp những tác phẩm khác của cô, về các nhân vật trên thì càng tốt (cơ mà ít ít nam thứ nữ thứ thôi).
5
198872
2014-07-06 09:18:26
--------------------------
113652
7317
Mình đã đọc hết 4 cuốn của bộ truyện này mà không hề bỏ qua bất kỳ tình tiết nào. Tác giả xây dựng cốt truyện quá lôi cuốn, mới mẻ & không gây nhàm chán. Tình tiết của truyện có thắt nút cũng có mở nút 1 cách logic. Nhân vật trong truyện khá nhiều, riêng phi tần của vua đã có hơn 5 người nhưng mỗi nhân vật đều có 1 tính cách riêng, không ai nhầm lẫn vào ai. Tang Tử & Hạ Hầu Tử Khâm dường như sinh ra là dành cho nhau. Chẳng những xinh đẹp, thông minh hơn người, 2 con người này lại có cách bảo vệ và yêu thương nhau rất kỳ lạ. Hạ Hầu Tử Khâm chỉ vì muốn Tang Tử có thể sống sót trong chốn hậu cung mà đã bao lần từ chối gần gũi nàng. Một vị vua anh minh, nghĩ cho dân, cho nước, lại luôn muốn bảo vệ người con gái mình yêu 1 cách vững chắc. Còn về phần Tang Tử, nàng thật may mắn khi có 3 người đàn ông luôn có thể sẵn sàng hy sinh bản thân để bảo vệ nàng. Nàng từ 1 con người chẳng có thân phận bay lên thành phượng hoàng, đường đường chính chính là hoàng hậu của một đất nước, lại còn có con của Hoàng Đế, địa vị của nàng giờ đây chẳng ai ngờ tới, chẳng ai dám tin nổi. Từ đầu đến cuối, mặc dù Tang Tử dùng rất nhiều mưu mẹo nhưng bản chất nàng luôn tốt lành, chẳng nỡ làm hại ai, tuy bên ngoài mạnh mẽ song rất nghĩa khí, tình cảm, lời hứa với những người nàng yêu thương, dù họ có phản bội nàng, nàng vẫn không nỡ ra tay. Người con gái ấy quả thật rất xứng đáng để có 1 kết cục tốt đẹp như thế bên cạnh người nàng yêu thương.
5
38403
2014-06-03 18:30:44
--------------------------
113105
7317
Bộ truyện này hoàn toàn có thể thu hút sự chăm chú của người đọc từ những trang đầu tiên. Cá nhân mình thích đọc những truyện nội cung tranh đấu và nội dung trong truyện không làm mình thất vọng. Tác giả phân tích và miêu tả tâm lí nhân vật kĩ lưỡng, già dặn, và cũng xen kẽ những đoạn trong sáng nhưng không làm mất giá trị nhân tâm là tâm sâu khó lường trong hoàng cung.
4
162508
2014-05-26 18:03:13
--------------------------
