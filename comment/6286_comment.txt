558049
6286
Cuốn sách được tác giả (Dan Roam) chia thành 4 ngày học, với 1 bộ công cụ mà tác giả đề ra để giúp độc giả thực hành phương pháp của mình, xen lẫn với đó là ví dụ của các công ty mà Dan đã giúp giải quyết vấn đề.
 Nhưng có lẽ do phương pháp khá mới, mà mình mới chỉ đọc 1 lần, lời văn của tác giả chưa giúp thoát hết ý nên hơi khó nắm bắt, tư duy bằng hình ảnh để giải quyết vấn đề nên có lẽ cần phải có sự chỉ dẫn trực tiếp của tác giả thì ta mới thật sự nắm rõ phương pháp. Hoặc cũng có thể phải đọc cả cuốn "chỉ cần mẩu khăn  giấy" cũng của Dan Roam thì mới nắm rõ được phương pháp này chăng, vì mình chưa đọc nó?!
 Mình khuyên các bạn nên đọc lại nhiều lần, và quan trọng là phải thực hành thật nhiều nếu muốn nắm vững phương pháp này, trên mạng cũng có nhiều video về vấn đề này, bạn nào giỏi tiếng anh có thể tham khảo. Còn hiện tại thì mình vẫn chưa thật rõ ràng về hình vẽ thông minh, ai đã nắm vững phương pháp rồi thì có thể chỉ dẫn thêm cho mình  đc ko,  "fb: Tiến ơi", thanks!
 Còn về chất lượng in, giấy thì tốt, tiki kết hợp với giaohangnhanh nên chuyển hàng rất nhanh,  cảm ơn!
4
412309
2017-03-29 16:42:13
--------------------------
473216
6286
Cuốn sách này nằm trong bộ sách khá nổi tiếng của tác giả Dan Roam. Cuốn đầu tiên là Chỉ cần mẩu khăn giấy, đây là cuốn thứ hai. Nếu như Chỉ cần mẩu khăn giấy thiên về lý thuyết, hướng dẫn nhiều hơn thì Hình vẽ thông minh thiên về thực hành, giải quyết vấn đề nhiều hơn. Cuốn sách rất sinh động và bổ ích, trong sách có rất nhiều những hình vẽ để bạn có thể áp dụng các lý thuyết và hướng dẫn trong Chỉ cần mẩu khăn giấy để thực hành ở cuốn sách này.
4
381211
2016-07-10 13:01:43
--------------------------
452696
6286
Mình chưa đọc cuốn Chỉ cần mẫu khăn giấy, chỉ đọc cuốn Hình vẽ thông minh để thực hành thôi. Nhận xét chung thì sách hơi kén người đọc đấy, thích hợp cho những bạn thích tư duy hình ảnh.
Nội dung sách hơi khô tí vì đây là những chỉ dẫn của tác giả để thực hành phương pháp này. Thêm vào đó cũng khá phức tạp mặc dù là tác giả có đưa ra ví dụ cụ thể. Đôi khi mình đọc có cảm giác phi thực tế và rườm rà tí (có lẻ mình chưa nắm lý thuyết rõ). 
Nếu các bạn siêng thực hành, chắc chắn các bạn sẽ thành công với phương pháp này. À một phần nữa, các ví dụ của tác giả về kinh tế khá nhiều ấy, mình không học về kinh tế nên đôi khi có hơi khó hiểu.

Bạn nào đã đọc, đã thắm, có thể chia sẻ thêm cho mình hiểu thêm, bản thân mình cũng có nhiều điều thắc mắc không biết hỏi ai. ^^
3
1336468
2016-06-20 19:38:56
--------------------------
449466
6286
Trong cuộc sống, học tập, công việc và ngay cả tình yêu, luôn có những vấn đề, có vấn đề đơn giản, cũng có phức tạp. Với tôi, nhất là trong việc học tập, thể hiện ý tưởng hay có khi cần giải thích điều gì cho một ai đó, tôi luôn gặp khó khăn. Và cuốn sách này thực sự giải quyết được vấn đề đó của tôi, không hoàn toàn nhưng nó giống như một kim chỉ nam chỉ tôi biết phải làm gì. Mọi vấn đề với tôi trở nên rõ ràng, dễ hiểu hơn rất nhiều. Không hoàn toàn ở đây là do cuốn sách đề cập chủ yếu đến các vấn đề trong kinh doanh. Mình nghĩ cuốn sách thích hợp nhất với những người là doanh nhân trong việc thể hiện các ý tưởng, chiến lược kinh doanh. Nhưng nếu biết áp dụng, các vấn đề trong cuộc sống cũng trở nên dễ dàng hơn.
4
873902
2016-06-17 15:14:36
--------------------------
442184
6286
Là phần tiếp theo của cuốn " Chỉ cần mẩu khăn giấy" cuốn sách Hình vẽ thông minh phát triển những nội dung đã đề cập trong phần 1 lên một mức độ mới đối với người đọc.
Cuốn sách vẫn khá dày, được in màu bắt mắt, chất liệu giấy làm nên cuốn sách vẫn cùng loại với phần 1 nên có thể nói là rất tốt.
Nội dung cuốn sách thay vì giới thiệu phương pháp, cung cấp lý thuyết cho người chưa biết gì về tư duy bằng hình vẽ, đã mang cho người đọc những bài tập trực quan để người đọc có thể áp dụng vào giải quyết các vấn đề trong cuộc sống. 
Tựu lại, nếu đã có Chỉ cần mẩu khăn giấy, bạn nên mua luôn cuốn này
5
815063
2016-06-04 15:43:40
--------------------------
423191
6286
Mình khá thích những quyển sách nói về cách tư duy thông minh và nhanh nhạy, đây là một quyển sách như vậy. Dễ đọc, dễ áp dụng và hiệu quả cao. Mình sẽ mua tiếp phần 2 của tác giả: Chỉ cần mẩu khăn giấy. Giấy sách trơn, mát và dày. Giá giảm 40.000đ rất tiết kiệm.Tuy nhiên khi nhận sách mình thấy trang cuối bị bong giấy và một góc sách bị cong. Mong tiki cẩn thận hơn trong việc vận chuyển. Còn đâu mình chưa có vấn đề gì phải chê trách tiki cả. Sẽ luôn ủng hộ tiki lâu dài.
5
467511
2016-04-28 21:49:26
--------------------------
420685
6286
Cái tên và bìa sách thực sự làm mình thích thú và đến cả khi đọc phần giới thiệu cũng không làm mình hết hứng thú với sách dù cho cả đám bạn xì xào cuốn sách không phù hợp lắm. Thực sự, đọng lại trong mình về cuốn sách là khả năng "tự sướng" khá cao của tác giả (mình không có ý chê bai hay xúc phạm, dù gì đây cũng là kinh nghiệm và thành công của tác giả, mình thực sự trân trọng điều đó và thực sự khâm phục tác giả vì khả năng áp dụng những lý thuyết cực kì hiển nhiên vào công việc một cách hoàn hảo). Sách trình bày theo ngày học rất khoa học tuy nhiên nội dung thì khiến mình hụt hẫng. Dù vậy, cuốn sách cũng phần nào thúc đẩy sự thích thú đưa hình vẽ vào công việc của mình và khâm phục những gì tác giả làm được. Còn những điều dạy trong sách khá hiển nhiên => kết luận là nếu bạn cần thêm động lực thì nên đọc sách này
1
600542
2016-04-23 18:19:39
--------------------------
381894
6286
Nội dung sách mang lại cho người đọc nhiều hứng khởi.
Chỉ tiếc là quyển sách của mình bị đóng lỗi 16 trang, nhưng mất bill nên không đổi sách được. 
Cách trình bày cũng như truyền thông tin và cảm hứng cho người đọc lôi cuốn và hấp dẫn.
Khi trình bày các hình vẽ, tác giả tạo được sự gần gũi và quen thuộc giúp người đọc dễ hình dung và dễ thấu hiểu.
Quyển sách này nên được đọc bởi các bạn tuổi teen. Sách sẽ hướng các bạn đến một không gian của tư duy và trí tuệ một cách hiệu quả.
4
1108887
2016-02-18 11:40:21
--------------------------
332956
6286
Là một trong những sách bán chạy nhất trên thế giới, thầy dạy kỹ năng mềm của tôi yêu cầu các sinh viên mua về đọc để giới thiệu với các sinh viên khác, không chỉ giới thiệu về cuốn sách viết hay như thế nào mà chủ yếu là để tất cả các sinh viên áp dụng được cách sử dụng hình ảnh trong việc truyền tải thông điệp. Trước giờ nhiều người nghĩ thuyết trình hay thì phải sử dụng powerpoint, nhưng sau khi đọc quyển sách này, chúng tôi biết được rằng, không phải tất cả bài thuyết trình nào cũng trình bày hay bằng cách sử dụng slide, mà vừa truyền tải nội dung vừa vẽ những hình ảnh cho người khác xem cũng giúp cho việc ghi nhớ sau về nội dung cần truyền đạt! Rất hay và bổ ích!
5
774332
2015-11-07 10:51:29
--------------------------
332804
6286
Quyển sách " Hình vẽ thông minh " là cuốn sách được Dan Roam viết nâng cao  và kế tiếp cuốn sách “ Chỉ cần mẫu khăn giấy “ . Cuốn sách " Hình vẽ thông minh " thực tế hơn nhiều so với cuốn sách “ Chỉ cần mẫu khăn giấy “. Đây là cuốn sách thực hành rất hay và sống động. Tôi rất thích cách vẽ đơn giản đã được Dan Roam hướng dẫn. Đây thật sự là một cuốn sách rất có ích cho tôi trong việc giải quyết mọi vấn đề trong công việc hằng ngày.
4
730635
2015-11-06 23:04:55
--------------------------
318184
6286
Hồi trước mình ít khi đọc sách về kinh nghiệm sống vì những sách loại này thường viết theo mô tuýp giống giống nhau. Nhưng vì lỡ đọc CHỈ CẦN MẨU KHĂN GIẤY cũng của Dan Roam rồi nên đọc thêm HÌNH VẼ THÔNG MINH vậy. Nếu so sánh thì HÌNH VẼ THÔNG MINH thực tế hơn rất nhiều so với quyển kia. Quan điểm cá nhân mình nghĩ vậy. Nội dung khá mới theo lối viết bình dị và thực tế nên mình dễ áp dụng lắm. Không cần phải tẩn mẩn vẽ đẹp để truyền đạt ý tưởng, chỉ cần những nét chấm phá đơn giản cũng đủ để tái hiện vấn đề. Quyển sách phù hợp cho những bạn có ý tưởng nhưng không biết cách trình bày. Với lối viết của dịch giả thực sự rất dễ để mình thực hành, kiểu đọc đến đâu thử vẽ vài nét (vào sách luôn cũng được). Thật là hay ho.
4
39173
2015-10-05 11:31:49
--------------------------
311261
6286
Nếu "Chỉ cần mẩu khăn giấy" là sách lý thuyết thì "Hình vẽ thông minh" lại là cuốn sách thực hành đầy thú vị và sinh động, dễ hiểu khiến ta thích thú chẳng muốn buông. Và đúng như lời tựa "Giải quyết những vấn đề phức tạp bằng hình ảnh đơn giản", quyển sách này đã đơn giản hóa mọi vấn đề nan giải các công ty lớn nhỏ vẫn đau đầu tìm cách đối phó, bằng đồ thị hình ảnh đơn giản không ngờ. Bạn không giỏi powerpoint? Chẳng sao cả. Mục đích của chúng ta là giải quyết vấn đề, không phải tái hiện vấn đề thật "đẹp", vì "khi cần suy nghĩ, đặc biệt về hình ảnh, chúng ta sẽ làm tốt hơn nhiều nếu thoát khỏi những giới hạn của con chuột, bàn phím,.."
4
367386
2015-09-19 22:22:03
--------------------------
307244
6286
Mình biết đến tác giả Dan Roam qua sách "Chỉ cần mẩu khăn giấy" nên quyết định mua thêm cuốn "Hình vẽ thông minh". Đây là phần tiếp theo nên nâng cao hơn nữa. Sách "Chỉ cần mẩu khăn giấy" hơi thiên về lý thuyết, còn "Hình vẽ thông minh" sẽ giúp chúng ta thực hành nhiều hơn, nên trong khi đọc cần lấy giấy và viết ra vừa đọc vừa thực hành luôn sẽ nhanh hiểu và dễ thấm hơn. Đọc xong chúng ta sẽ cảm nhận được đúng như những điều tác giả nói "Không có cách nào hiệu quả hơn để chia sẻ một ý tưởng với người khác bằng việc phác họa một hình ảnh đơn giản".
5
515497
2015-09-17 23:59:05
--------------------------
305871
6286
Mình chưa đọc cuốn sách Chỉ cần mẫu khăn giấy của Tony Buzan, tuy nhiên, cuốn sách hình vẽ thông minh không được hay như ban đầu mình đã tưởng tượng. Mặc dù cách tác giả thể hiện các vấn đề bằng hình ảnh minh họa dễ hiểu và khá ấn tượng khi từ những yếu tố bình thường trong cuộc sống có thể liên tưởng và trình bày được một cách đặc sắc và ấn tượng vấn đề của bản thân để bán sản phẩm cho nhà đầu tư, nhưng mình cảm thấy không được thiết thực trong cuộc sống. Chỉ dùng đối với những người muốn nghiên cứu về thị trường, các vấn đề tài chính và kinh tế.
4
453533
2015-09-17 10:44:46
--------------------------
305224
6286
Tác giả Dan Roam có 2 cuốn sách là Chỉ cần mẩu khăn giấy và Hình vẽ thông minh. mình đã rất phân vân khi chọn mua một trong 2 cuốn và cuối cùng đã quyết định đặt cuốn mà trước vì đã tham khảo nhận xét của mọi người. Cuốn sách thật đúng như lời khen, cách viết của tác giả rất thân thiện, giảng giải rất kĩ càng mà giấy lại rất dày, bản in đẹp nên bạn nào có thích vẽ luôn trong sách cũng không can gì. Tuy nhiên, vì liên quan đến thực hành, mà bản thân mình chưa phải gặp nhiều vấn đề khó khăn lắm nên cuốn sách chưa thật sự phù hợp với mình, và mình vẫn chưa có nhiều cơ hội thử tác dụng của nó trong cuộc sống thường ngày.
5
227371
2015-09-16 21:25:50
--------------------------
292580
6286
Mình thích cuốn này đã lâu mà nay mới có dịp đặt mua trên tiki. Đợt hàng này tiki giao chậm quá, mình đợi mòn mỏi hàng mới về đến tay nên có hơi không vui nhưng khi cầm trên tay quyển sách thì cảm thấy rất hài lòng. Chất lượng giấy in dày, tốt, thơm mùi sách mới, hình in màu rất đẹp, bìa sách cứng cáp dễ thương, sách dày và to. Nội dung thì rất hay, giúp mình hiểu thêm và luyện tập về những phương pháp ghi nhớ tổng hợp kiến thức bằng hình vẽ cực kỳ bổ ích, có hình minh hoạ của tác giả rất sinh động, mình thích cách vẽ này. Nói chung, được cầm trên tay một quyền sách gốc, nội dung thú vị, hình thức tuyệt vời thì quá là tuyệt luôn
5
55927
2015-09-07 14:28:25
--------------------------
280953
6286
Cuốn này là một cuốn thực hành, cách để trình bày nội dung qua những hình vẽ. Và cuốn sách giúp cho mình hiểu được phương pháp của tác giả sâu hơn, nó thiên về cách thức tác giả đã sử dụng để trình bày những vấn đề hóc búa cho đối tác, và mình chỉ việc làm theo sự hướng dẫn của tác giả. 
Nếu bạn đi theo đúng phương pháp của tác giả, thì bạn sẽ rất dễ dàng giải quyết những vấn đề hóc búa mà không cần phải đau đầu tìm lời nào đó để giải thích. 
Tuy nhiên, do mình đi ngược, tức là mình đã thực hành trước, nên thành ra, có vài điều vẫn còn chưa hiểu, vì vậy mình khuyên bạn nên đọc trước cuốn lý thuyết về phương pháp này, "Chỉ cần mẫu khăn giấy"
5
65325
2015-08-28 00:57:36
--------------------------
268346
6286
Mình chưa đọc cuốn Chỉ cần mẩu khăn giấy của tác giả này nhưng mình cũng có một số liên hệ nhất định về ý tưởng của tác giả. Trước đây mình đã đọc Tôi tài giỏi bạn cũng thế của Adam Khoo, nói về cách kích hoạt bán cầu phải của não bộ để làm việc hiệu quả hơn, hay sử dụng sơ đồ tư duy của Tony Buzan. Hình vẽ thông minh chú trọng hơn vào việc phân tích vấn đề theo từng khía cạnh để phác thảo nên bức tranh tổng thể, không chi để mô tả vấn đề mà còn để giải quyết nó. Bản thân mình chưa đạt được nhiều kết quả với cuốn sách này nhưng mình tin rằng đây là cuốn sách rất hữu ích với mọi người.
5
59771
2015-08-16 12:17:21
--------------------------
259871
6286
Tôi không phải là một người thông minh gì. Việc học những lý thuyết, khái niệm khô khan cũng khó khăn với tôi. Hay như chỉ ngồi nghe những kiến thức khô khan, những hình vẽ cứng ngắc cũng khó vào đầu tôi. Tôi không đọc cuốn "Chỉ cần mẩu khăn giấy" như nhiều bạn, vì tôi cần một cuốn khiến tôi phải thực hành hơn là một cuốn chỉ có lí thuyết. Có thể bạn mua cả 2, vì thực hành thì đều cần những lí thuyết căn bản. Nhưng với tôi, đọc cuốn sách này, được thực hành nhiều hơn cuốn "Chỉ cần mẩu khăn giấy" sẽ hợp với tôi hơn. Cá nhân tôi thấy cuốn sách đem đến cho tôi cảm giác tôi hiểu biết cuốn kia ít cũng vẫn hiểu cuốn này. Khi bắt đầu đọc nó, tôi đọc khái niệm nhưng không nhiều lắm là bạn đã thực hành và lại khái niệm, rồi thực hành... liên tục khiến tôi cảm thấy cuốn hút. Những ví dụ những bài tập đưa ra luôn gợi cho tôi liên hệ đến công việc hiện tại đang làm.

Nội dung lôi cuốn, logic, cách tác giả đưa ra vấn đề, rồi cách người ta giải quyết thế nào, cách tác giả giải quyết cùng 1 vấn đề bằng cách khác ra sao, tác giả sẽ giải thích rõ ràng từng vấn đề, ngọn nguồn của nó. Tôi thật sự bất ngờ trước những cách giải quyết tưởng như đơn giản hay tưởng như phức tạp lại có tính logic khi đã giải quyết xong nó. Sau đó, chính tôi lại được thực hành giải quyết vấn đề tương tự tác giả đưa ra và của chính tôi chỉ bằng những hình vẽ đơn giản nhưng rất có ý nghĩa. Tôi thật sự bị lôi cuốn vào cách giải quyết các vấn đề dù lớn dù nhỏ hằng ngày của chính mình chỉ bằng những hình vẽ đó.
Nếu bạn thật sự bắt đầu đọc nó, hãy chuẩn bị thời gian thật kĩ, cố gắng đừng để bị gián đoạn nhất có thể nếu không tư duy có thể không được liên tục. 

Hãy thử bắt đầu và cảm nhận cuốn sách theo cách của bạn. Còn ở trên là ý kiến của riêng tôi sau khi đọc xong: Hình vẽ đơn giản nhưng dễ ghi nhớ.
5
472150
2015-08-09 21:59:23
--------------------------
244662
6286
Quyển sách "Hình vẽ thông minh" này là cuốn sách nâng cao của "Chỉ cần mẫu khăn giấy", dùng để giải quyết vấn đề phức tạp hơn mà "chỉ cần mẫu khăn giấy" không giải quyết được. Tất nhiên tuy đơn giản nhưng những công cụ nâng cao trong sách đòi hỏi độ phức tạp và khó hơn trước rất nhiều. Đây là một cuốn sách thực hành hay. Mình rất thích cách vẽ đơn giản mà truyền tải được nhiều thông điệp như trong quyển sách trình bày. Cuốn "Hình vẽ thông minh" thiên về thực hành hơn so với "Chỉ cần mẫu khăn giấy". Chuẩn bị sẵn bút và giấy khi đọc nhé vừa đọc vừa thực hành luôn, thú vị lắm. 
Quyển sách nhỏ này đã tạo cho mình một cuộc sống "sống động" hơn trước kia rất là nhiều, không chỉ áp dụng cho công việc kinh doanh mà còn cho những hoạt động hằng ngày nữa.
4
74132
2015-07-28 12:57:05
--------------------------
242731
6286
Một quyển sách bắt mắt ngay từ khi nhìn trên bìa, chữ in cũng rất rõ ràng đẹp, đặc biệt là nội dung và hướng dẫn khá chi tiết để thực hành với chiếc bút chì trên tay. Quyển sách đã gợi mở cho mình cách vẽ như thế nào vừa giúp tiết kiệm thời gian, vđơn giản và dễ dùng khi muốn trình bày, giải thích một vấn đề trong công việc một cách rõ ràng hơn. Nó giúp mình tăng khả năng truyền đạt thông tin, ý tưởng bản thân. Một quyển sách kỹ năng đáng để đọc và nó như là bài tập thực hành sau quyển "Chỉ cần mẩu khăn giấy".
5
575705
2015-07-26 21:48:58
--------------------------
233021
6286
Sau khi đọc xong cuốn Chỉ cần mẩu khăn giấy, Dan Roam, tôi tiếp tục đọc tiếp cuốn  Hình Vẽ Thông Minh này để tiếp tục tìm hiểu phương pháp tuyệt vời và hấp dẫn này. Khi thấy tôi đọc, bạn tôi xem và nhờ tôi đặt 2 cuốn này đủ biết bộ sách này  cuốn hút mọi  người  như thế nào. Tôi đang cố gắng áp dụng các phương pháp này vào công việc của mình. Tuy khó nhưng tôi sẽ cố gắng áp dung. Một số phương pháp tôi củng có thể  tìm thấy ở cty mình  như kế hoạch thi công, các bước tìm và khắc phục sự cố máy móc...
4
192938
2015-07-19 14:25:16
--------------------------
231248
6286
Hình vẽ thông minh, đây là một quyển sách khác cùng tác giả với quyển Chỉ cần một mẫu khăn giấy. Mình đã đọc Một mẩu khăn giấy, nó rất hay và bổ ích nên đã quyết định đặt mua quyển sách này qua tiki. Tác giả đã chứng minh rằng hình vẽ khiến ta tiếp thu kiến thức nhanh và hiệu quả hơn, đồng thời cũng chỉ ra cách tạo nên ý tưởng của những hình vẽ đó. Nếu như Một mẩu khăn giấy cho thấy những vấn đề bao quát, Hình vẽ thông minh lại cụ thể hơn. Các bạn nên đọc cả 2 quyển sách này.
5
673477
2015-07-17 22:42:55
--------------------------
215967
6286
Cùng với quyển "Chỉ cần chiếc khăn giấy", "Hình vẽ thông minh" là một trong 2 tác phẩm rất hay của tác giả  Dan Roam về tư bằng hình ảnh và phát triển ý tưởng cũng như hế thộng lại các thông tin trong công việc cũng như cuộc sống hằng ngày.
Nếu quyển "chỉ cần chiếc khăn giấy" là những kiến thức được truyền đạt cho người đọc thì quyển "Hình vẽ thông minh" là những bài tập thực hành để người đọc có thể vận dụng các phương pháp tác giả đã đề cập vào từng hoàn cảnh trong cuộc sống thông qua những ví dụ điển hình cho từng trường hợp. 
Với những kiến thức mới mẻ về phương pháp tư duy mới, tuy sẽ khó áp dụng cho những người mới bắt đầu, nhưng nếu biết cách vận dụng, bạn sẽ thấy kết quả mà nó mang lại rất lớn.
5
315066
2015-06-27 01:18:11
--------------------------
214818
6286
Đã khá lâu rồi, mình mới mua được cuốn sách mà có thể đọc liền tù tì trong mấy ngày mà không thấy chán. Mình không có đọc cuốn "Chỉ cần mẫu khăn giấy" trước, nhưng nếu chịu khó đọc kỹ và làm bài tập trong sách, thì mình nghĩ vẫn nắm được nội dung. Lúc xem đáp án bài tập thì vỡ lẽ ra được rất nhiều cái hay. Khi đọc sách lúc nào cũng kè kè theo cây bút chì, giất note, nhưng bạn đừng dùng gôm nhé, vì như thế bạn sẽ sa đà vào tiểu tiết.
Về phần mình, mình cũng đã áp dụng cách vẽ để nhìn lại các vấn đề mình đang gặp phải.
Và cuối cùng là, hãy vẽ thật nhiều từ những vấn đề nhỏ nhất, để rèn luyện kỹ năng, sau khi đã tìm được cách giải quyết thì cũng phải kiên trì mới đem lại kết quả tốt. Chúc các bạn thành công
5
35152
2015-06-25 15:34:59
--------------------------
210767
6286
Hình Vẽ Thông Minh - một cách tư duy thú vị . Cuốn sách có chất liệu giấy dày , có nhiều khoảng trống để tôi tha hồ sáng tạo , áp dụng những lý thuyết trong sách . Đọc sách , tôi phát hiện ra những vấn đề tưởng chừng như phức tạp , nan giải lại được giải quyết bằng những cách vô cùng đơn giản với chỉ một mẩu giấy và những chiếc bút màu . Khi vẽ ra giấy mọi thứ trở nên rõ ràng , sinh động và đơn giản hơn hẳn . Não phải của chúng ta là để tư duy hình ảnh cơ mà , tại sao lại không tận dụng nó .  Hãy đọc và cảm nhận , biết đâu bạn lại tìm được cách giải quyết vấn đề của mình.
5
487567
2015-06-19 20:15:16
--------------------------
189382
6286
Tôi đọc cuốn "Chỉ cần mẩu khăn giấy" trước khi đọc cuốn sách này. Nếu bạn chưa đọc nó, hãy dành ra vài ngày đọc để bạn có thể hiểu rõ hơn các lý thuyết mà Dan Roam đưa ra, có như vậy bạn mới có thể thực hành tốt được. Cuốn sách này nhìn cách mà tác giả chỉ cho chúng ta xem chừng dễ hiểu lắm nhưng khi tôi thử nghiệm với vấn đề của tôi thì nó quả là "rừng rậm của những cánh rừng rậm". Có nhiều khi tôi cảm thấy sung sướng điên lên như sưu tập được món đồ trang sức đắt tiền khi thấy tác giả giải quyết vấn đề bằng hình ảnh nhẹ tựa lông hồng nhưng khi đối mặt với vấn đề của tôi thì tôi không thể nào hình dung ra nổi mình sẽ làm gì với đống đổ nát đó tiếp theo. Mặc dù vậy, đây là cuốn sách khá hay vì nó giúp chúng ta nhận ra vấn đề ở đâu, chúng ta lạc ở điểm nào. Theo tôi thì như thế là tuyệt vời lắm rồi vì có nhiều lúc tôi chẳng biết mình đã bí thế ở đâu và vấn đề của mình chính xác ở nơi nào trong vài trăm trang giấy mà tôi phải xử lí mỗi ngày. Tôi thích cuốn sách này cũng như cách mà ông đưa ra các giải pháp. Tôi thấy nó khá hay. Nhiều lúc tôi tự hỏi rằng tại sao vấn đề chỉ là một mớ rối bù không thể giải quyết mà quên rằng chính tôi đã hiểu sai vấn đề mình đang đối mặt. Có lẽ với một chút trí tưởng tượng, khả năng vẽ (mặc dù không đẹp lắm) và óc phán đoán, đây sẽ là cuốn tư duy hình ảnh hợp với tôi hơn mọi cuốn dày cộp khác. Bạn đừng lo nếu bạn không biết vẽ và cũng đừng ngại ngần khi mua hai cuốn sách này vì tác giả sẽ chỉ cho bạn có nhiều thứ còn tuyệt vời hơn các công cụ phần mềm mà ta sử dụng hằng ngày.
5
185178
2015-04-27 10:32:55
--------------------------
177831
6286
Đây là một cuốn sách dạy cách tư duy về hình ảnh . Đây là một cách thức tư duy đơn giản nhưng lại vô cùng hiệu quả , không hề mất đi sự sáng tạo và độc đáo . Đối với tôi đây là một cách tư duy mới lạ .  Cuốn sách có những hướng dẫn rõ ràng và bài tập để chúng ta thực hành .  Trước khi đọc cuốn sách này , chúng ta nên mua cuốn " Chỉ cần mẩu khăn giấy " để xem trước lí thuyết , sau đó mới thực hành . 
Quả thực đây là một cuốn sách vô cùng  bổ ích , bạn nên sở hữu một cuốn để nghiên cứu . Tôi đã nhận được sách và rất hài lòng . ♥
5
335856
2015-04-03 21:20:19
--------------------------
170798
6286
Thời đại chúng ta đang bước vào giai đoạn số hóa với tốc độ như một cấp số nhân. Ngày hôm qua bạn có thể chưa biết máy tính là gì nhưng bạn chắc chắn phải biết facebook, mạng xã hội. Nhưng không phải mọi thứ công nghệ đều có thể làm hoàn hảo hơn con người. Bởi công nghệ là do chính con người chúng ta tạo ra. thời kì số hóa huy hoàng nào là do con người khởi nguồn. Có những phương pháp giúp chúng ta truyền tải thông tin một cách dễ dàng hơn như chưa từng có. Bạn không thể ngờ được rằn, những hình vẽ đơn giản nguyệch ngoạc còn truyền tải tốt hơn là trang trình chiếu powerpoint nếu bạn biết tận dụng đúng cách. Bạn không thể ngờ rằng hình ảnh đơn giản đó lại đem cho bạn một cách tư duy mới là và thật hiệu quả. Mọi thứ dường như nằm trong tầm tay của bạn. Chỉ cần một cây bút trong tay và một mẩu giấy nhỏ là chúng ta có thể bắt đầu được rồi đấy. Hãy đọc và thực hành ngay thôi!
5
474991
2015-03-20 19:35:41
--------------------------
163355
6286
Có vẻ như tác giả Dan Roam ưa thích sự đơn giản khi bỏ qua những bài Powerpoint rắc rối. Tôi cũng có sở thích như vậy. Tư duy hình ảnh không cần sự rườm rà, phức tạp. Cái ta cần là tư duy hình ảnh sẽ giúp chúng ta giải quyết vấn đề và phát huy ý tưởng một cách đơn giản nhưng độc đáo. Tôi rất thích những quyển sách có nhiều hình ảnh và tuyệt vời hơn khi đó là những hình ảnh đầy màu sắc. Màu sắc kích thích tư duy. Ở đây, tác giả Dan Roam đã cho chúng ta biết thế nào là tư duy hình ảnh và kèm theo đó là những bài tập giúp chúng ta thực hành và phát huy khả năng tư duy hình ảnh ở mức cao nhất. Một quyển sách nên có trên giá sách của bạn.
5
387632
2015-03-04 19:58:03
--------------------------
126135
6286
Phưng pháp "tư duy bằng hình ảnh" trong quyển "Chỉ cần mẩu khăn giấy" thật sự rất hay, tuy nhiên trong quyển "chỉ cần mẩu khăn giấy" lại chủ yếu tập trung về lý thuyết, chứng minh sự hiệu quả và các ứng dụng của phương pháp nên khi đọc xong quyển sách đó mặc dù đã có thể áp dụng phương pháp tư duy bằng hình ảnh nhưng thật sự là vẫn còn lóng ngóng, chưa thành thạo và chưa khai thác hết được ưu điểm của phương pháp.
Rất may mắn và đúng lúc là giờ đây đã có quyển sách thứ 2 trình bày về phương pháp tư duy bằng hình ảnh đó là "hình vẽ thông minh" và quyển sách này lại mang tính thực hành rất cao, hướng dẫn rất tỉ mỉ để áp dụng phương pháp và có cả ví dụ và bài tập để luyện tập.
5
42985
2014-09-17 13:04:35
--------------------------
