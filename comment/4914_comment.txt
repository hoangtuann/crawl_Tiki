523189
4914
Truyện hay :)) mình vốn thích tác giả này,và một lần nữa ko bị thất vọng 
Tranh minh hoạ dễ thương,đẹp

5
326475
2017-02-11 02:14:48
--------------------------
470443
4914
Sách đẹp, minh họa đẹp, cách viết chữ khi to khi nhỏ gây tò mò và đỡ nhàm chán.

Ông bố kể câu chuyện ngắn mọt cách dài ngoằng và hơi rối, hơi hoang đường, cũng thú vị và có tính kịch tích nhưng nhờ như vậy mà hấp dẫn lũ trẻ. Nhân vật Khủng Long mới đọc cứ tưởng là ÔNG, hóa ra lại là BÀ.

Con trai tôi đã đọc một mạch hết quyển sách mà không hề dừng lại, cháu rất thích quyển sách này. Cháu biết là chuyện bịa nhưng cháu nói là cũng giống thật :)
5
778630
2016-07-07 15:21:56
--------------------------
462205
4914
Mình chọn mua cuốn sách này vì cái bìa dễ thương và vì cái tên Neil Gaiman. Mình đã đọc Coraline và Câu chuyện nghĩa địa của cùng tác giả, vì thích cả 2 cuốn trên nên mới mua Còn sữa là còn hy vọng. Sách mỏng, kích thước chữ khá lớn lại có nhiều tranh minh họa nên mình đọc rất nhanh. Nội dung sáng tạo và đáng yêu, cách kể chuyện đậm chất Neil Gaiman, hình minh họa đẹp và hợp. Chỉ có điều tranh minh họa màu in kèm bên trong thiết kế rất kỳ, khi mở ra dễ bị rách, mình vừa giở ra là rách ngay phần giữa tranh. Thêm một điểm không hài lòng nữa là sách được giao cho mình nhìn bẩn và hơi cũ, chắc tại sách được xuất bản cũng lâu rồi.
4
234502
2016-06-28 13:09:48
--------------------------
461185
4914
Quyển sách này Neil Gaiman viết cho con trai mình, nên chẳng ngạc nhiên khi nó mang màu sắc khác hẳn với tác phẩm "Coraline" mang màu sắc u tối trước đó.
Đến với Còn sữa là còn hy vọng có lẽ là cả một thế giới khác, một ông bố trải qua đủ chuyện kỳ lạ trên đời để mang về hộp sữa (và cả một câu chuyện phiêu lưu thú vị) cho các con. 
Một câu chuyện sẽ không làm hài lòng hết tất cả các đọc giả của ông ở Việt Nam, nhưng sẽ đủ làm thỏa mãn những người có trí tưởng tượng tuyệt vời.
4
92827
2016-06-27 16:50:04
--------------------------
353296
4914
Sách in ấn đẹp, nhiều hình minh họa dễ thương. Mình chỉ không hài lòng 1 điều, có 1 trang là tranh gấp vào theo chiều dọc. Mình không hiểu sao Nhã Nam lại gấp kiểu đó, mình mở ra là rách ngay 1 đoạn nhỏ giữa bức tranh, mà gấp như thế mình chắc chắn là ai mở tranh ra xem thì cũng rách. Bản US mình mua cũng không gấp như thế, bản UK thì mình không có nên không biết. Tiếc là Tiki không có phần gửi kèm ảnh, nếu không mình đã có gửi ảnh cho các bạn xem.
3
29518
2015-12-15 21:39:46
--------------------------
305766
4914
Là một fan hâm mộ của tác giả Neil Gaiman, tôi tìm đến cuốn sách này trong niềm háo hức sau khi đã đọc xong câu chuyện nghĩa địa và Coraline hai cuốn sách đó đều đem lại cho tôi những câu chuyện phiêu lưu thú vị, đậm màu sắc huyền bí. Tuy vậy khi đọc cuốn sách này thì tôi cảm thấy nó không còn cuốn hút mình như hai cuốn kia nữa. Một câu chuyện hơi nhạt xoay quanh lời kể của người cha sau khi đi mua sữa về cho hai đứa con. Có thể tôi đã quá tuổi để đọc câu chuyện này nên không cảm nhận được hết sự thú vị của nó. 
3
139598
2015-09-17 09:47:21
--------------------------
295268
4914
Mình mua sách sau khi đã say mê hai cuốn trước của Neil Gaiman, Coraline và Câu chuyện nghĩa địa, và mặc dù biết cuốn này k cùng đề tài với hai cuốn trước, mình vẫn muốn rinh về cho đủ bộ sưu tập, và tò mò muốn biết một tác giả "Neil Gaiman u ám" sẽ xoay sở thế nào với nội dung truyện hoàn toàn thiếu nhi và dễ thương thế này. Đọc xong mình chỉ có thể thốt lên, làm con của một tác giả như ông này thật là sung sướng, tha hồ nghe kể chuyện mọi lúc mọi nơi, muốn đề tài gì chỉ cần yêu cầu là có ngay, nhưng vì thế nên nhiều lúc cũng nhức đầu lắm nhé, chẳng biết bố xạo hay thật đâu, vì truyện ứng tác mà cũng logic quá đi, còn cố tình để lửng lơ cái đồng tiền vàng dưới đáy bình sữa làm bằng chứng nữa. 

Truyện khá ngắn, lại nhiều tranh minh họa rất đẹp và sống động nên đọc cũng rất nhanh, xong có thể ngồi lật giở lại ngắm nghía. Giấy in tốt, dày và có độ láng, in tranh lên rất đẹp, xứng đáng với kỳ vọng ở NXB Nhã Nam. Mạch truyện nhanh, lại chuyển biến không ngừng nên cực kỳ hấp dẫn với thiếu nhi, cỡ 7,8t trở lên, hoặc người lớn hồi teen như mình chẳng hạn. Truyện đan lồng đủ các yếu tố hấp dẫn thiếu nhi như cướp biển, khủng long cho bé trai, ngựa hồng cho bé gái, du hành xuyên thời gian, người Maya, người ngoài hành tinh, ma cà rồng (có cả Edward tai tái và "Tina" chạng vạng). Đáng yêu nhất là cái cách ông bố luôn khéo léo nâng vị trí của hộp sữa lên, từ chỗ là bằng chứng cho câu chuyện của ông thuyết phụ cướp biển, tới chỗ là vật quyết định sự tồn vong của cả Trái đất lẫn vũ trụ, vừa sáng tạo vừa cực kỳ thú vị. Nếu mình là những đứa trẻ con ông, chắc chưa kịp nghe hết truyện đã quên hết mình giận bố vì lí do gì rồi.
4
450198
2015-09-10 08:44:26
--------------------------
254472
4914
Ấn tượng đầu tiên là cái bìa nên đã quyết định mua ^^. Khi mở ra thấy tranh vẽ nên cũng thích, nhân vật hơi kinh dị. Người bố thật sáng tạo khi nghĩ ra được câu truyện thú vị đó: một cuộc phiêu lưu trên con đường mua sữa. Giọng văn rất thật hợp với trẻ con, có lẽ vì đây là cuốn sách tác giả viết dành tặng con trai mình. Nhưng khó là "Còn Sữa Là Còn Hy Vọng" : nếu trẻ con đọc sẽ k hiểu hết được ý nghĩa trong đó, còn người lớn thì lại thấy nó nhàm chán nếu họ không phải là người tuýp người thích đọc truyện thiếu nhi. Nó hợp với những ai muốn trở về tuổi thơ ^^.
4
717449
2015-08-05 11:21:52
--------------------------
237576
4914
"Còn Sữa Là Còn Hy Vọng" có lẽ không làm hài lòng những fan của Neil Gaiman tại VN, nhất là sau cơn sốt gần đây của "Coraline". Theo mình, cũng dễ hiểu thôi. Đây là cuốn sách tác giả viết để dành tặng cho đứa con trai của mình, một chú nhóc tiểu học, thì "Còn Sữa Là Còn Hy Vọng" là hoàn toàn phù hợp. Mong rằng Nhã Nam sẽ tiếp tục dịch và xuất bản những truyện như Sandman và Stardurst của cùng tác giả Neil Gaiman. (Mình rất thích tranh minh họa trong truyện này, thật sự rất có hồn)
4
292321
2015-07-22 16:04:22
--------------------------
216135
4914
Dù là mình hi vọng sẽ được đọc Ánh sao ma thuật trước, nhưng thế này cũng không sao.

Sách mua về, lập tức phải đọc một lèo từ đầu chí cuối cho đã xD. Đọc xong thì đúng kiểu vừa dễ thương, vừa đáng yêu, vừa có tí màu sắc kinh dị của Neil Gaiman, lại vừa ẩn chứa vô vàn những bất ngờ thú vị.

Đây là một cuốn sách, mà trong đó bạn đọc vừa thấy một anh bố trẻ vụng về (được bá hoạ sĩ minh hoạ y chang cái ông ở 22B phố Baker =))), cũng kiểu tóc đen xoăn xoăn, áo khoác kiểu 9th và khăn quàng, mặt cũng kiểu xị xị) và đàn con tinh ranh, sẽ thấy anh ta yêu con như thế nào, và có một trí tưởng tượng siêu phàm ra sao - kiểu "Mồm miệng đỡ chân tay" vừa đáng yêu vừa buồn cười; và một đống những kiến thức về vũ trụ, cổ sinh vật học, khảo cổ... blah blah, mà trong đó trung tâm là nghịch lý thời gian - khi hai phiên bản của cùng một vật ở hai khoảng thời gian khác nhau chạm vào nhau thì sẽ tạo ra nghịch lý, và BÙM! Neil Gaiman vẫn cứ xây dựng được một cốt truyện tưởng vô lý đùng đùng, plot hole tứ tung nhưng hoá ra lại vô cùng logic - kiểu kiểu Sherlock mới cả DW (xin lỗi T"T, mình lại lên đồng). Nếu đọc xong mà không hiểu thì là do người đọc chớ nỏ phải do tác giả.

Tóm lại đây là một cuốn sách mà fan của thế loại sci-fi hay những thanh niên héo úa tuổi xuân vì BBC phải đọc =)).
5
102543
2015-06-27 10:50:47
--------------------------
206425
4914
Nội dung giản dị, dễ thương cho các em bé giàu trí tưởng tượng. Hai anh em chờ bố mua sữa đến "một năm rồi ấy chứ". Mình ngạc nhiên. Ồ, phải rồi, trẻ con mà. Và khi bố về, cậu chuyện phiêu lưu của bố bắt đầu. Lời văn hóm hỉnh, hài hước, nội dung nhẹ nhàng đã làm nên sự thành công của cuốn sách. Dường như đó là một khía cạnh của cuộc sống về vấn đề "gà trống nuôi con" khi mẹ vắng nhà, sự vô tâm của các ông bố và câu chuyện thú vị được viết lên thay lời xin lỗi con trẻ. Chắc chắn cá em bé sẽ rất hứng thú với câu chuyện và sẽ tha thứ vì sự chậm trễ của bố!
5
616461
2015-06-09 17:37:01
--------------------------
193185
4914
Khác với những tác phẩm có phần u ám khác của Neil Gaiman, đây lại là một câu truyện trẻ con hóm hỉnh và tươi sáng. Người bố vui tính lấy từ chính hình tượng của Gaiman đã rất thông minh khi chế biến chuyến đi mua sữa về trễ của mình thành một chuyến phiêu lưu không thể ngờ tới để kể cho hai đứa con. Những hình minh họa vui nhộn càng khiến cuốn sách bắt mắt và thú vị hơn. Có lẽ mỗi người một ý, nhưng đối với tôi, đây là một cuốn sách hay và đáng đọc. Câu chuyện du hành thời gian tưởng cũ mà lại như mới. Cách giải quyết tình huống hóm hỉnh mà hợp lý. Đây rõ ràng là một lựa chọn tốt đễ đổi gió cho các bạn đọc yêu thích nhà văn Gaiman.
5
148922
2015-05-06 19:59:04
--------------------------
179074
4914
Mình thì không ấn tượng lắm về quyển sách này có lẽ mình trông đợi nhiều hơn khi đọc review trước, và lời giới thiệu. Giống quyển truyện tranh với với nhiều tình tiết, chuyển biến nhanh, ít mô tả, thiên về mô tả hành vi, chuyển đổi.
Điểm sáng là lời văn trong sáng.
Câu chuyện theo mình nghĩ là lời chống chế dễ thương nhất của người bố cho chuyện về trễ khi đi mua sữa. Một người bố thông minh hóm hĩnh khi sáng tác câu chuyện có nội dung liên quan đến những sự việc gắn liền với sự tưởng tượng về thế giới của 2 đứa con.
3
319884
2015-04-06 13:38:00
--------------------------
165999
4914
Còn Sữa Là Còn Hy Vọng chứa đựng nhiều điều tuyệt vời dành tặng cho thiếu nhi và cả những người lớn đã từng là thiếu nhi như mình!
Mình thích chất văn của Neil, giản dị mà hàm súc, sinh động, vui tươi, hài hước nhưng ẩn chứa nhiều tầng ý nghĩa trong đó. Không quá khó khăn để thấy rằng nhiều  triết lý về cuộc sống được ẩn chứa trong từng câu chữ trong quyển sách. Nó dạy cho mình sự lạc quan, dạy cho mình sống vui vẻ và dạy cho mình không thôi hy vọng vào cuộc sống đẹp đẽ này!
Với cốt truyện ấn tượng, nhiều màu sắc, Neil đã dẫn dắt mình vào một thế giới đầy mê hoặc và sáng tạo. Thật sự khâm phục cho những gì mà Neil đã vẽ ra trong tác phẩm này!
Một quyển sách chứa đựng đầy những phép màu cổ tích và nhiều niềm vui!
5
303773
2015-03-11 18:47:05
--------------------------
160111
4914
Khác với hai tác phẩm "U ám" trước của Neil Gaiman, Còn sữa là còn hy vọng lại là một câu chuyện có vẻ hoang đường và cực kỳ vui nhộn khác. Câu chuyện có thể xem là cuộc phiêu lưu của người bố ra ngoài trái đất, trở về quá khứ, đến thời điểm trong tương lai khi đi mua sữa ở cửa hàng góc phố. Tại đây ông gặp khủng long, cướp biển, ma cà rồng... - Những nhân vật chúng ta thường bắt gặp khi đọc các câu chuyện dành cho trẻ con. Có lẽ ai cũng nghĩ rằng đây chỉ là tưởng tượng của người bố, nhưng thật bất ngờ, khi hai đứa con cầm hộp sữa đã phát hiện ra một đồng xu cổ - đồng xu mà cướp biển tặng cho người bố. Thật là một câu chuyện ly kỳ hết sảy. Sẽ thật thiếu sót nếu không nhắc đến phần minh họa của Chris Riddell với nhân vật người bố lấy nguyên mẫu từ chính Neil Gaiman - Minh họa của ông hài hước, sinh động, rất lôi cuốn. Đây là một quyển sách với minh họa bằng tranh rất hay, phù hợp với trẻ nhỏ.
5
146361
2015-02-22 08:16:44
--------------------------
