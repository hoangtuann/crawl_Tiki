460370
6523
Đây là một tuyển tập các câu chuyện của bác Tô Hoài về Hà Nội thời Pháp thuộc, và nó thực sự rất thu hút, kể cả đối với một người miền Trung như tôi.
Bạn có thể hình dung về sự hiếu kỳ và chăm chú của một người khi nghe người khác kể những câu chuyện về vùng đất và con người mà họ chưa từng nghe bao giờ, đó chính là cảm giác của bạn khi đọc tác phẩm này, kể cả bạn có phải là người Hà Nội hay không. Vì Hà Nội trong tác phẩm là Hà Nội của một thời rất xa, một Hà Nội chứa đựng những con người với nếp sống khác hẳn bây giờ.
Giọng văn của bác Tô Hoài thật sự tuyệt vời, rất tự nhiên, pha một chút trào phúng nhẹ nhàng, và đặc biệt thể hiện một óc quan sát cực kỳ tinh tế, đã đẩy những câu chuyện về tuổi thơ và tuổi trẻ của bác như sống động hẳn lên, trên nền một xã hội thời Pháp thuộc đầy lạ lẫm với người trẻ bây giờ.
Đây là một cuốn sách rất đáng đọc với những người yêu thích Hà Nội nói riêng, và văn hóa lịch sử Việt Nam nói chung.

4
61077
2016-06-26 23:30:31
--------------------------
358369
6523
Cảm giác đầu tiên khi nhìn cuốn sách là mừng tượng được hình ảnh của phố phường Hà Nội khi nhìn thấy bìa sách, hình thức sách khá đẹp, ngáy sách đều, trang sách cũng không mỏng manh lắm.
tôi mua sách này để giành tặng Ba, Ba là người con vùng đất Bắc, cảm giác lúc nào cũng nhớ về ký ức tuổi thơ xưa cũ, thấy trưa Ba nằm đọc mà cứ trầm ngâm
bản thân tôi khi đọc cuốn sách này cảm giác như cái gì đó mộc mạc và yên bình trong từng con đường từng góc phố, cái gì đó hiền hòa đầy ắp trong từng khu xóm thôn quê.
4
218665
2015-12-25 10:38:13
--------------------------
338609
6523
Trên tiki còn có hai cuốn truyện cũ hà nội tập một và tập hai tận 200k, so với phiên bản bìa cứng 90k này không biết có gì khác ?
Xét về nội dung, mình vô cùng thích giọng văn của tô hoài. Những gì ông viết trong TCHN là những gì mộc mạc, giản dị, chân thực nhất về hà nội. Từ chuyện những người hành xóm, chiếc xe đạp, hay phố buôn người, 36 phố phường, tất cả đều phản ánh những sự thật đen tối của hà nội lúc bấy giờ, ẩn sâu trong đó là những nỗi khổ hạnh của triệu kiếp lầm than. Ta vừa xót xa vừa cảm phục tô hoài, phải là người con yêu quê hương, đất nước lắm lắm, ông mới làm nên một cuốn sách về hà nội hay đến thế!
5
642004
2015-11-16 16:09:52
--------------------------
284301
6523
Mua được một cuốn sách bìa cứng với mức giá này thật là vui. Tất nhiên, giấy là giấy thường thôi, nhưng thế cũng được. Mình thích bìa sách, vẽ tranh phố cổ theo phong cách Bùi Xuân Phái, vừa nhìn đã biết là Hà Nội. Những truyện ngắn, tản văn trong sách miêu tả những khía cạnh nhỏ nhặt, quen thuộc của đời sống hàng ngày đất Hà Nội, từ quê ra phố, từ lời ăn tiếng nói đến phong tục tập quán, rồi cái ăn cái mặc... Bao trùm cả cuốn sách là tình cảm của tác giả dành cho đất và người Hà Nội, rất đỗi dịu dàng và ấm áp, khiến người đọc cũng đắm chìm trong tình cảm ấy.
5
57459
2015-08-30 20:40:35
--------------------------
183164
6523
"Truyện Hà Nôi cũ" là một trong những tập truyện ngắn mà tôi thích nhất của nhà văn Tô Hoài bởi qua những mẫu truyện nhỏ tôi như thấy được cả Hà Nội xưa từng, hàng cây, góc phố, ngõ nhỏ mà chứa đựng trong nó là biết bao tình cảm chân thành của những con người bình dị mà gần rủi trong cuộc sống thường ngày, truyện với lời văn nhẹ nhàng sâu sắc của tác giả Tố Hữu để lại cho đọc giả chúng ta một cảm giác nhẹ nhàng êm ái lướt theo từng con chữ, từng trang sách mà chẳng thể nào dừng đọc được cho đến hết truyện.
5
602070
2015-04-15 16:28:39
--------------------------
