500705
10438
Ấn tượng đầu tiên có lẽ đến từ bút danh Phiên Nghiên, nghe lạ lẫm mà thu hút. 
Khi bắt đầu đọc các câu chuyện, các tâm sự trong cuốn sách tôi cảm thấy khá hài lòng với cách viết chân thật, không màu mè và có chút nuông chiều cảm xúc của tác giả. Tôi bắt gặp bản thân trong hầu hết các lời văn của chị, từ cách suy nghĩ về " những người có tính nhạy cảm" cho đến những quan sát tinh tế trong cuộc sống đời thường như việc cảm nhận được tình cảm người cha khi chở đứa con nhỏ đi học buổi sáng, đeo trên lưng là balo búp bê baby...
Từ những cảm xúc mới yêu cho đến những suy nghĩ nồng nhiệt của tuổi trẻ.
Trong vô vàng quyển sách ra đời mỗi ngày với cách viết "mì ăn liền" thì cá nhân tôi nghĩ đây là quyển sách đáng đọc vì lời văn khá chân thành và tràn ngập tâm sự.
Tôi đã đọc lại hai lần và khá thích cuốn sách này.
4
110493
2016-12-27 11:06:23
--------------------------
458864
10438
Mình mua trong một đợt giảm giá vì thấy nhiều review khen nhưng thật sự hơi thất vọng. Dù tác giả có văn phong của riêng mình không bị trộn lẫn nhưng chưa thật sự làm độc giả ấn tượng. Giọng văn của tác giả tựa như những trích dẫn hay entry vì vậy dễ làm mình quên.
Về bìa sách thì cũng không có gì ấn tượng. Tuy nhiên tên của tác giả rất đặc biệt "Phiên Nghiên" rất nhạc, rất thơ vì từ láy. Mình nghĩ điểm thu hút duy nhất đó chính là tên tác giả, các phần còn lại thường thật thường. 
3
1157252
2016-06-25 15:53:58
--------------------------
409991
10438
Một cuốn sách rất đáng để đọc. Trong Trái tim son trẻ, tản văn có, những câu chuyện cũng có. Đọc lên tôi có cảm giác như nhìn thấy một phần bản thân mình trong đó. Nếu có thể, bạn hãy đọc Trái tim son trẻ của PHiên Nghiên, đọc và cảm nhận như cảm nhận về chính thời tuổi trẻ của mình đang hiện ra trước mắt. Đây là cuốn sách đầu tiên tôi đặt mua ở tiki và rất hài lòng. Cảm ơn tiki rất nhiều. Chắc chắn mình sẽ tiếp tục với những đầu sách khác ở tiki..
4
1218946
2016-04-03 12:49:34
--------------------------
295440
10438
Mua cả một chồng sách vì Tiki giảm giá, nhưng về đọc chỉ ưng nhất cuốn này. Văn phong nhẹ nhàng, nữ tính dễ thương đến lạ. Rất nhạy cảm, rất đa mang giống ai đó. Đọc mà bị ám ảnh bởi cách viết ngọt ngào của con gái nam bộ. Đôi bài hơi lan man nhưng vẫn rất ưng, từ ngữ hay và cách so sánh cũng rất đáng yêu. Cảm xúc gần gũi, chân thực, giàu suy ngẫm. Không thích chất giấy lắm nhưng cách thiết kế khá đơn giản và dễ đọc
  Mong chờ những tác phẩm tiếp theo của chị Phiên Nghiên sẽ được xuất bản :) 
5
576128
2015-09-10 11:16:23
--------------------------
249978
10438
Mình không rõ cơ duyên nào khiến mình mua sách, nhưng khi đọc mình cảm thấy khá hài lòng vì những cảm xúc của người viết mà mình có thể cảm nhận được sự đồng điêu. Nó viết về những xúc cảm của tuổi trẻ, những nỗi buồn của tuổi trẻ cùng rất nhiều suy nghĩ, trăn trở rất trẻ... Tuy nhiên có vẻ vì cảm xúc quá tuôn trào cộng với vốn từ và văn phong phong phú của tác giả mà đôi lúc mình thấy câu cú hơi lan man, bài viết trở nên dài dòng nên thỉnh thoảng đọc mình cảm giác như bị lạc hic... Dù sao đây cũng là một tác phẩm hay đáng để đọc và ngẫm, nhất là với những bạn có tâm hồn văn chương, lãng mạn ^^
4
51292
2015-08-01 01:22:11
--------------------------
216640
10438
Lần đầu tiên tôi biết đến Phiên Nghiên và chọn đọc thử cuốn sách đã xuất bản từ lâu này - sách viết về tuổi trẻ - một trong những mảng đề tài mà tôi thích. Trước tiên, tôi cảm thấy may mắn khi mua được cuốn sách này với giá ưu đãi nhiều trên Tiki.
Dù mọi người cho khá nhiều lời khen, cá nhân tôi vẫn thấy đây chỉ là một cuốn sách tạm ổn: bìa sách tạm ổn, bố cục trình bày tạm ổn, phong cách viết cũng tạm ổn. Đúng cách viết blog, có thể hiện được chất riêng của tác giả thật nhưng có lẽ không phù hợp lắm với tôi.
3
433873
2015-06-28 00:43:30
--------------------------
198631
10438
Đã đọc kha khá tản văn lẫn tạp bút nên tìm được một tản văn hay tạp bút nào hay , với mình là điều hết sức thích thú. Thực ra mà nói đây không phải là cuốn tập văn quá xuất sắc, nhưng ít ra nó đã đánh động trái tim mình, đã khiến tim mình rung lên những điệu đồng cảm nên với mình đây là cuốn sách đáng đọc. Nhẹ nhàng, sâu sắc mà không sến sẩm , "Trái tim son trẻ"  bằng cách nào đó đã giúp mình soi lại quãng đời tuổi trẻ mình đã đi qua, giúp mình thấy lại một quãng đời tuổi trẻ đã đánh mất- những nỗi buồn tuổi trẻ bảng lảng xa xôi mà người trẻ nào cũng hẳn đã gặp phải. Đọc để cảm thông và chia sẻ cho những nỗi buồn tuổi trẻ...
4
75031
2015-05-20 13:14:30
--------------------------
177647
10438
Đây là lần đầu tiên mình biết đến Phiên Nghiên và đọc những dòng văn của chị ấy. Dù tuổi đời còn rất trẻ nhưng chị đã có cho mình vốn sống rất phong phú và chiêm nghiệm được nhiều thứ trong cuộc sống. Với giọng văn mộc mạc nhưng tinh tế, ngòi bút của chị đã chạm được đến trải tim những người trẻ như tôi. Giá nhơ bìa sách được chăm chút nhiều hơn thì đây thực sự là 1 quyển sách rất hoàn hảo và đáng đọc. Dù sao thì mình vẫn rất hài lòng với "Trái tim son trẻ" của Phiên Nghiên.
5
131327
2015-04-03 16:09:29
--------------------------
171328
10438
Đây là cuốn sách hay nhất tôi từng đọc. Lời văn mộc mạc, chân thành, nhưng sâu sắc .Cuốn sách khiến tôi như yêu thêm cuộc sống thường ngày, làm tôi nhớ đến tất cả mọi người xung quanh : cha, mẹ, thầy cô ,... 
Cuốn sách thật mộc mạc, không cầu kì, hoa mĩ, nhưng nội dung , tình cảm ẩn chứa trong sách hết sức to lớn . Tôi nhớ mãi câu nói : '' ...  tình yêu vẫn chưa hề tự vẫn trong trái tim mướt xanh của mình..."
Đây thực sự là một cuốn sách hay, nên mình hy vọng, mọi người có thể đọc và cảm nhận nó .
5
359810
2015-03-21 18:38:58
--------------------------
75472
10438
Tôi hoàn toàn bất ngờ khi biết được Phiên Nghiên sinh năm 1987, bởi nàng quả thực rất trẻ để gói ghém những suy nghĩ rất sắc bén, những cảm xúc rất có chiều sâu đòi hỏi phải một vốn sống vô cùng phong phú thì mới viết ghi ra trọn vẹn được. Phiên Nghiên đã làm được điều đó. Toàn bộ những gia vị chua, cay, mặn, ngọt của tuổi trẻ đã được truyền tải hết trong cuốn sách này. Phiên Nghiên tôn trọng từng gia vị cảm xúc ấy, cô không cho nó rằng bồng bột tuổi trẻ, rằng thiếu suy nghĩ mà đơn giản chỉ là, ở cái lứa tuổi này, con người ta nên sống bằng tất cả những đam mê vốn có, bằng ngọn lửa nhiệt thành nhất mà mình đang sở hữu.
4
100136
2013-05-18 15:54:25
--------------------------
66855
10438
Trái tim son trẻ của Phiên Nghiên là quyển tản văn hay nhất tôi từng đọc. Lời văn mộc mạc, giản dị nhưng ẩn sâu trong đó là những cảm xúc chân thực, những trải nghiệm sâu sắc của tác giả. 
Người đọc Trái tim son trẻ có thể cảm nhận được sự gần gũi, sự tươi mới, trẻ trung và cả sự nhạy cảm của một người trẻ tuổi. Những cảm nhận về cuộc sống thường ngày, về quê hương, về người thầy, người cha, bạn bè, tình thân ..., và  về những người dưng không quen biết, vội đi ngang qua phố đông đánh rơi giọt nước mắt thấm ướt trái tim tác giả.
Đây quả thật là một quyển sách rất đáng đọc, nếu bạn yêu văn chương, nhất định bạn không thể bỏ qua.
5
85985
2013-04-02 20:33:24
--------------------------
50185
10438
Trước khi mua, mình cũng khá tò mò về tựa sách và những lời giới thiệu về quyển sách. Khi đã cầm trên tay, nó cũng chưa đủ hấp dẫn mình bằng những quyển sách khác mua cùng đợt. Nhưng một khi mình đã thực sự đọc nó, thì có cảm giác Phiên Nghiên đã đem mình về với những gì bình dị nhất nhưng lại có khả năng khiến ta nhớ mãi không quên. Những câu chuyện nhỏ về cảm xúc của một cô gái trẻ hết sức chân thật và gần gũi, như mở ra trước mắt mình một cuộc sống muôn vàn sắc màu. Cảm ơn Phiên Nghiên đã nhắc mình nhớ về những điều hết sức gần gũi nhưng đôi khi lại bị lãng quên.
4
23340
2012-12-13 00:22:29
--------------------------
48160
10438
Mình phải nói rằng mình rất thích thú được đọc cuốn sách này. Nó tràn ngập những ý tứ hay, những ngôn từ đẹp, trong sáng, diễn tả được rất nhiều điều mới mẻ của cuộc sống xung quanh. Qua mỗi bài viết, qua mỗi chương trong cuốn sách, ta như được khám phá thêm những góc cạnh của con người, tình yêu và cuộc đời; được đắm chìm trong những cảm xúc tươi mới và tinh khôi, những cảm xúc ngọt ngào bất tận. Tác giả đã đem hết sự tâm huyết trong văn chương, cùng với sự nồng nhiệt của tình yêu đời, yêu con người vào mỗi câu chuyện nhỏ, khiến chúng gần gũi và đáng yêu vô cùng. Thêm vào đó là cách trình bày sách khá đẹp và bắt mặt, chắc chắn sẽ gây hứng thú cho bạn đọc.
5
20073
2012-11-29 12:59:32
--------------------------
44059
10438
Tôi chỉ vừa 21. Đọc những câu chuyện của Phiên Nghiên, tôi như muốn thời gian ngừng lại để tôi có thể sống lâu hơn trong tuổi trẻ này. 
Trước đây, tôi thường dằn vặt mình về những sai lầm, những cảm xúc tội lỗi. Nhưng giờ đây, tôi thấy vừa lòng, bởi tôi trẻ, và dù là sai lầm, hay đúng đắn, tôi đã sống một tuổi trẻ trọn vẹn như vậy.
Như Phiên Nghiên nói, "đành rằng giông bão, mà trái tim em vẫn son trẻ, niềm tin của em vẫn rõ ràng, và tình yêu vẫn chưa hề tự vẫn trong trái tim mướt xanh của mình..."
Và dù thế nào, hãy cứ đứng lên nhé!
4
42976
2012-10-30 15:00:29
--------------------------
43810
10438
Đọc "Trái Tim Son Trẻ" là nhìn thấy bản thân mình, thấy tuổi trẻ của mình. Tôi tìm thấy mình qua từng trang sách của Phiên Nghiên. 

Tôi thích góc nhìn của Phiên Nghiên, không quá gai góc nhưng cũng chẳng dịu dàng. Cách chị nhìn cuộc sống, mang đến những điều thú vị và mới mẻ. Là yêu thương mà không sợ tổn thương, là sự va vấp để trưởng thành. Là chút nồng nàn của mùa cũ. Tất cả hòa quyện vào nhau qua từng trang sách.

Đây là một cuốn sách chưa bao giờ  gây nhàm chán. ^^
4
8289
2012-10-27 22:54:10
--------------------------
28346
10438
Tôi thích những suy nghĩ giản dị,  chân phương nhưng không hề thiếu chiều sâu trong câu chữ của Phiên Nghiên. Một dòng tư tưởng có thể là tầm thường với người này nhưng lại đánh thức ở kẻ khác những khát vọng sống rất mạnh mẽ, rất thật thà. 
 Không nhất thiết phải là một người trẻ mới có được một trái tim còn son trẻ. Tôi đã từng gặp rất nhiều những người già có trái tim an nhiên của một người trẻ và nhiều người trẻ có trái tim già cỗi – họ ngại yêu thương, ngại thức tỉnh, ngại khổ đau… chỉ muốn cầm chắc lấy những gì được đặt để , được trao tặng một cách bền vững và không thử thách. Kể cả trong tình yêu.
Cảm ơn tác giả đã gởi tặng cuộc sống bộn bề này một quyển sách hay, một tâm hồn đẹp và một trái tim chứa nhiều quá những thanh xuân dành cho cuộc đời.







4
22772
2012-05-26 15:45:55
--------------------------
22500
10438
Một cuốn sách làm say người đọc bởi nhựng tình vảm giản dị rất đời thương của một trái tim trẻ đầy nhiệt huyết.Trái tim ấy không chỉ rung lên bởi những rung động tình yêu đôi lứa mà thiêng liêng hơn thế.Đó là tình yêu máu mủ ruột rà, tình yêu với quê hương,là những cảm thông với những con ngời nhọc gánh mưu sinh bỗng gặp trên đường.Người trẻ hiện lên trong tác phẩm của Phiên Nghiên đẹp vô chừng và là mỗi người trong chúng ta đây.
Những câu văn không cầu kì trau chuốt,nhưng là thực,đã chạm đến trái tim mình.
Đọc để cảm nhận để yêu thương nhiều hơn
Quyển sách cho một ngày đẹp trời ^^
5
25128
2012-03-23 21:20:45
--------------------------
