476812
8930
Quyển sách này cũng giống như những quyển Happy Reader khác, đều rất gọn nhẹ, chữ in to, rõ ràng. Đĩa cd đi kèm đọc nghe khá hay, thêm phần nhạc đệm nữa. Mình thích đọc những quyển tiếng Anh có kèm nghĩa từ vựng thế này hơn. Vì nó kích thích khả năng suy nghĩ của mình để tìm ra nghĩa hoàn chỉnh của câu hơn là việc đọc sách song ngữ, mình đọc được một xíu mà thấy không hiểu được là liếc qua phần tiếng Việt liền. Nói chung đây là quyển sách có ích với mình.
5
1296172
2016-07-22 18:06:03
--------------------------
465043
8930
Nhằm nâng cao vốn từ tiếng Anh và khả năng đọc hiểu, mình đã mua quyển sách này về. Mình đã thử đọc truyện Dracula nguyên gốc rồi nhưng thật sự rất khó hiểu, nhưng với bản viết lại thực sự đọc rất ngấm. Truyện hay, lại có tranh minh họa kèm theo, sau mỗi chương lại có phần kiểm tra đọc hiểu và nghe. Phần duy nhất mình không thấy hài lòng là các từ mới không có phần phiên âm. Đối với những ai muốn học tiếng Anh mình khuyên nên mua quyển sách này cùng với những bộ truyện khác của Happy Reader. 
5
1217475
2016-06-30 19:25:03
--------------------------
388132
8930
"Dracula – Bá tước Dracula" là một cuốn sách rất hay và bổ ích. Đọc cuốn sách này chúng ta sẽ tìm hiểu được nhiều từ vựng, kĩ năng đọc hiểu cũng nâng cao nên đây là một quyển sách rất thích hợp cho những bạn muốn nâng cao khả năng tiếng Anh. Giọng đọc ở đĩa CD cũng rất hay, không bị rè. Cuốn sách này còn giúp các bạn học tiếng Anh không bị nhàm chán ( vì tìm hiểu tác phẩm văn học ).
Về hình thức, bìa được thiết kế đơn giản nhưng rất đẹp. Giấy in cũng tốt. Nhìn chung thì đây là một cuốn sách hay và bổ ích.
4
1142307
2016-02-28 10:29:53
--------------------------
283935
8930
Trong thời đại hiện nay thì Tiếng Anh vô cùng quan trong, thế mà khả năng đọc hiểu Tiếng Anh của mình lại không tốt, vốn từ cũng rất hạn chế nên mình đã quyết định mua quyển sách này.
Về nội dung thì không có gì để nói. Còn về hình thức thì sách in giấy tốt, có hình minh họa cũng rất đẹp, dưới mỗi trang là những từ mới, để khi đọc có thể thuận tiện hơn, không phải ôm thêm quyển từ điển. Không chỉ thế mà ở dưới mỗi trang ấy còn có thêm một số câu hỏi để kiểm tra khả năng đọc hiểu của bạn nữa.
CD thì rất dễ nghe và đúng giọng Mỹ.
3
395466
2015-08-30 14:33:01
--------------------------
222494
8930
Ngày nay việc sử dụng tiếng anh đang ngày càng phát triển, vì thế bổ sung kĩ năng đọc và nghe là một điều tất yếu. nên mình chọn mua quyển sách này do được  bạn giới thiệu. nội dung sách hấp dẫn người đọc kể về nguồn gốc của Bá Tước Dracula. Đĩa CD tặng kèm rất tốt, giọng rõ, phát âm theo chuẩn Mỹ ( mình thích hơn giọng Anh), kể rất truyền cảm, luyện khả năng nghe tóc, sách in rõ ràng, dễ đọc, trình bày theo bố cục Logic. Tiki giao hàng nhanh nên mình rất ưng ý, nói chung là sản phẩm tốt!
4
508472
2015-07-05 18:53:44
--------------------------
171099
8930
Để nâng cao trình độ nghe tiếng anh, mình đã mua rất nhiều cuốn sách luyện nghe và trong đó có cuốn này "Happy Reader - Bá Tước Dracula" sau khi áp dụng một thời gian mình có nhận xét sau:
-Về nội dung đây là một câu chuyện cổ tích vì thế rất quen thuộc với những bạn thích đọc thể loại này, vì truyện được kể bằng tiếng anh và với giọng bản xứ nên việc đọc trở nên dễ dàng hơn.
-nhiều hình ảnh minh họa sinh động.
-ngoài ra còn có phần dịch nghĩa cho một số từ vựng trong bài, vì thế khỏi phải mất công vác thêm cuốn từ điển to tướng để dịch.
5
274381
2015-03-21 10:53:46
--------------------------
139394
8930
Bá Tước Dracula đã là một câu chuyện nổi tiếng trong văn học phương Tây mà có lẽ ai cũng đã từng nghe đến. Ở trong cuốn sách này, câu chuyện được rút ngắn lại để phù hợp hơn, đem lại cho người đọc những ý chính để hiểu câu chuyện. Bên cạnh đó là hệ thống từ 1000 mới tiếng anh sẽ giúp người đọc nâng cao phần nào vốn từ của mình, kèm theo một CD phát âm chuẩn và âm thanh sinh động giúp người nghe luyện kĩ năng nghe tiếng anh. Đây là một cuốn sách rất phù hợp với những ai mới học tiếng anh, đặc biệt là các em nhỏ. Tuy nhiên, hệ thống từ mới không hoàn toàn khó, hầu hết là những từ thông dụng và chỉ được giải nghĩa chứ không kèm phát âm, nên với những người học tiếng anh nâng cao thì cuốn sách này không phù hợp lắm.
4
113650
2014-12-06 20:48:39
--------------------------
122939
8930
Đây là một quyển sách phù hợp với nhiều người.Với cách viết hấp dẫn, hình  tượng nhân vật được khắc họa rõ nét,nó thực sự mang lại sự thích thú cho người đọc, khiến họ muốn khám phá bí ẩn bên trong " chiếc rương" ấy. Tuy nhiên, nhiều chi tiết không thật sự rõ ràng( vì đây là bản tóm tắt) làm người đọc đôi khi không hiểu rõ. Sách được in với chất lượng tốt, có nhiều trang để ghi chú. Nhưng với cấp độ cao nhất trong Happy Reader, nhiều phần từ vựng hơi đơn giản nên ít được chú ý nhiều như phần từ trong các sách của MacMilan hay Oxford Bookworm.Dù sao, nó vẫn là quyển sách hay và đáng mua
4
282915
2014-08-28 21:52:26
--------------------------
121290
8930
Với những người đang có trình độ tiếng Anh tầm trung thì cuốn sách này là lựa chọn số 1. Cuốn sách là trích dẫn lại tiểu thuyết nổi tiếng Bá tước Dracula của Bram Stoker, lời viết lại rất dễ hiểu, từ vựng dễ nhớ và khá thông dụng. Thêm vào đó, những từ hơi khó một chút còn có cả chú thích rõ ràng ở dưới. Ngoài ra, trong cuốn sách còn có cả một số thông tin ngoài lề thú vị như Hình tượng bá tước Dracula ngoài đời, người đàn bà khát máu Bathory,...(dĩ nhiên là bằng tiếng Anh), cuối mỗi chương còn có những bài kiểm tra nho nhỏ xem bạn có thực sự hiểu nội dung truyện không. Về phần âm thanh, lời dẫn truyện được đọc diễn cảm bởi những người bản xứ, tạo cảm giác rất chân thực, âm nhạc nền lôi cuốn khiến người nghe như đang thực sự tham gia vào câu chuyện. Nếu luyện nghe bằng cách này, chắc chắn, khả năng nghe của bạn sẽ tốt lên rất nhiều.
Đây là cuốn sách hay và bổ ích cho những bạn muốn cải thiện khả năng đọc và nghe tiếng Anh, lại tạo cơ hội cho các bạn tìm hiểu về các tác phẩm kinh điển với lời văn gọn gàng, dễ hiểu. Tôi thực sự rất thích đọc các tác phẩm Happy Reader!
5
36896
2014-08-18 20:33:28
--------------------------
