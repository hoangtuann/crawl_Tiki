491953
11220
Đây là một trong những tập Doraemon mà mình thích nhất. Minh nhớ không lầm thì lần đầu tiên tập này được tái bản dưới dạng lá thư thứ 2 sau khi lần đầu đã xuất bản, sau đó được đặt tên là ngôi sao cảm. Đến tận bây giờ những tình tiết của tập này mình vẫn còn nhớ như in
5
1564543
2016-11-22 09:21:32
--------------------------
445598
11220
Hồi xưa đọc tập này là do thuê ở hiệu sách, một cuốn sách cũ nát, rách tả tơi, băng dính dán lia lịa nhưng độ hấp dẫn qua từng trang chuyện thì càng ngày càng tăng lên, khiến mình bỏ qua luôn phần hình thức tồi tệ. Câu chuyện lần này một lần nữa đưa ta về với tuổi thơ với những ước mơ ngây thơ được hóa thân thành hay được nói chuyện với các loài động vật thân thiện và đáng yêu xung quanh ta. Câu chuyện cũng là một hồi chuông cảnh báo mỗi người trong chúng ta về vấn đề bảo vệ môi trường.
5
1420970
2016-06-10 15:42:09
--------------------------
422018
11220
Lần đầu tiên đọc tập này là hồi nhỏ đi mướn truyện,lúc đó cuốn này có tên "Ngôi sao cảm",cái tên khiến mình nhớ nhưng sau đó muốn tìm đọc lần nữa thì lại tìm ko thấy.Bây giờ đổi tên thành "Nobita và hành tinh muông thú"thì thấy nó quá bình thường.Rất thích tập này,nội dung hay,hấp dẫn,ly kỳ lại mang nhiều thông điệp ý nghĩa về tình bạn,tình đoàn kết và đặc biệt là ý thức bảo vệ thiên nhiên,bảo vệ cây cối để có một môi trường trong lành,sạch đẹp,tươi xanh.Tuy tên truyện đã thay đổi nhưng sức lôi cuốn của tập này vẫn không hề giảm đi đối với mình.
5
1158730
2016-04-26 12:15:21
--------------------------
399676
11220
"Doraemon" – là một bộ truyện tranh của tác giả người Nhật Bản Fujio Fujiko mà mình rất thích. Truyện có nội dung xoay quanh cậu bé Nobita – một cậu bé lười biếng, yếu thể thao, học kém,... nhưng cậu có lòng dũng cảm và tình yêu thương vạn vật. Trong tập này, chúng ta sẽ được đi theo nhóm bạn Nobita đến hành tinh muông thú – một hành tinh rất trong lành, sạch đẹp và rất yêu hòa bình. Đọc "Doraemon" mà không thể rời mắt khỏi trang truyện. Đọc "Doraemon" còn giúp ta rút ra được nhiều bài học ý nghĩa và sâu sắc. Một bộ truyện tranh hấp dẫn.
4
1142307
2016-03-18 06:46:20
--------------------------
386402
11220
Mình đã đọc truyện này rất lâu rồi, từ khi chỉ còn là một đứa bé nhưng bây giờ cầm lại nó trên tay, lật từng trang và hòa theo cuộc phiêu lưu đầy thú vị và hấp dẫn của Doraemon, Nobita, Shizuka, Jaian, Suneo đến một hành tinh có các con thú đều biết nói, biết hành động y như con người, thậm chí nền văn minh ở đó còn hiện đại hơn Trái Đất gấp bội. Dù tên nhân vật trong truyện khi tái bản lại đã có sự thay đổi để đúng với nguyên tác nhưng cũng không làm giảm đi sự hấp dẫn của truyện với mình.
5
471112
2016-02-25 17:10:28
--------------------------
377774
11220
Thông qua cuộc phiêu lưu và cứu giúp hành tinh muôn thú thoát khỏi cuộc xâm lược của hành tinh Nimuge thì Tác giả đã nêu lên, cũng như giáo dục cho các độc giả nhí những vấn đề rất thời sự và nhân văn ( tính đến thời điểm này thì truyện cũng đã xuất bản 26 năm rồi ). Đó là việc con người chặt phá tài nguyên rừng, sử dụng quá nhiều năng lượng, chiến tranh dẫn đến những biến đổi ô nhiễm môi trường nghiêm trọng ảnh hưởng đến không chỉ con người mà còn muôn loài trên Trái đất (vì vậy mà tác giả đã tạo ra hành tinh muôn thú ý là muôn loài không chịu trách nhiêm cho những sai lầm của loài người nên được sống trên một hành tinh thật xinh xắn dành riêng cho động vật) và cũng khuyên nên sử dụng tiết kiệm các tài nguyên. Mình rất hài lòng với nội dung tập này.
P/s : thông qua nét vẽ của Tác giả, các loài động vật được tiến hóa như loài người nhưng vẫn mang những hình hài đặc trưng của động vật rất dễ thương, và các bạn nhỏ mỗi bạn được đội nón động vật mình thấy rất đúng với đặc trưng của mỗi người dễ thương hok kém ak 
5
117702
2016-02-03 11:41:39
--------------------------
357936
11220
Mình tuy lớn rồi nhưng vẫn mê đọc doremon lắm. Vì mê doremon từ hồi còn bé nên đến bây giờ vẫn còn thích đọc lắm mỗi khi rảnh. Cá nhân mình thì mình thích đọc doremon truyện dài, vì nó là những cuộc phiêu lưu vào những vùng đất và xứ sở thần kỳ. Giúp cho bốn nhân vật chính trong truyện tha hồ khám phá và tìm hiểu những điều thú vị và ngạc nhiên xảy đến trong cuộc hành trình. Và lần này nhóm bạn lạc vào thế giới muôn thú thần kỳ, hết sức thú vị khi đọc truyện lun
5
936274
2015-12-24 14:54:45
--------------------------
333345
11220
Mình đọc tập này từ cách đây khá lâu, với tên "Ngôi sao cảm", rất thích, qua câu chuyện, nhóm bạn Doraemon lại dẫn mình đến với hành tinh của những loài muông thú, một hành tinh lý tưởng nơi xanh sạch đẹp cùng cư dân yêu hòa bình, yêu công lý và biết bảo vệ môi trường. Truyện không chỉ để giải trí mà còn mang giá trị nhân văn sâu sắc, như thông điệp về tình bạn, nghĩa khí, lòng dũng cảm, sự đoàn kết trong lúc khó khăn cũng như ý thức bảo vệ môi trường để có một môi trường sống trong lành như ở Ngôi sao cảm
5
630600
2015-11-07 20:25:49
--------------------------
