406535
3427
Chú lợn con đi tìm bạn chơi cùng, không có ai ngoài khỉ con nhưng khỉ con chê bạn ú không thèm chơi chung, thế là lợn tích cực tập thể dục nhằm giảm cân, lợn con tập vất vã nhưng kết quả không đạt được bao nhiêu, bỗng đau có tiếng cười rúc rích, lợn con nhìn lên thì ra là 2 bạn thỏ, sau khi hiểu lý do, 2 bạn thỏ cho rằng lợn von phải mũm mĩm mới dễ thương, thế là cả 3 chơi đùa vô cùng vui vẽ.
Cốt truyện dễ thương phù hợp tâm lý trẻ em, hình vẽ ngộ nghĩnh, chất lượng giấy tốt, nếu có điều kiện thì mua luôn cả bộ 12 quyển cho con, rất đáng đồng tiền.

5
357674
2016-03-28 15:18:49
--------------------------
405455
3427
Mình vô tình biết đến tuyển tập truyện về chú lợn con vui vẻ Ba Bu này, sau khi mua Lợn con muốn giảm béo ( mới đầu chỉ vì cái tiêu đề) và đọc qua nó thì quyết định sẽ sưu tập đủ bộ truyện này để giành tặng đứa cháu đáng yêu nhà mình. Vì ngôn từ trong  truyện rất dễ hiểu, nội dung lại mang tính giáo dục cao, tranh vẽ đúng nét ngây thơ của trẻ thơ. Cũng không quá nhiều chữ khiến trẻ con nản không muốn đọc. Đây thực sự là một cuốn sách hay và hợp lý để tạo cho trẻ niềm say mê đọc sách ngay từ nhỏ. Các mẹ, các cô dì chú bác nhà có trẻ nhỏ thì mình khuyên hãy mua cuốn sách này.
5
587182
2016-03-26 19:52:23
--------------------------
319834
3427
Lúc đầu tiên nhìn thấy cuốn sách này thì không có ấn tượng gì đặc biệt lắm vì thấy nét vẽ rất là ngô nghê, thậm chí là có phần nghệch ngoạc như là vẽ chơi, không phải vẽ minh họa cho ấn phẩm truyện tranh. Nhưng vì cái nhan đề Lợn con muốn giảm béo nghe cũng ngộ nghĩnh, gợi tò mò nên mở ra đọc ngay. Câu chuyện rất đơn giản và có nhiều chi tiết hài hước, đồng thời cũng có những ý nghĩa mang tính giáo dục cho trẻ em. Sau khi đọc xong thì không còn cảm thấy nét vẽ này là xấu mà lại thấy rất là một mạc, giản dị và tạo nên sự gần gũi cho các bạn đọc nhỏ tuổi.
4
531312
2015-10-09 19:41:24
--------------------------
