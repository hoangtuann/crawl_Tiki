432311
2526
Cuốn sách này vẽ cũng được,đầu sách có mấy tấm tranh màu.Nhưng chắc vì sách mỏng nên nội dung cũng không có nhiều,diễn biến khá đầy đủ nhưng rất nhanh,mình  không hiểu tại sao tình cảm của Romeo và Juliet sao lại nhanh như thế và mình thấy nó không diễn tả được tình cảm của họ sâu sắc đến mức nào và cũng chẳng thấy cảm động tẹo gì.Trong khi mỗi lần xem Romeo và Juliet thì nước mắt mềnh tuôn như thác cơ TvT .Em thấu nó chỉ như một bản tóm tắt vô cùng ngắn gọn.Tuy nhiên đọc đến trang cuối mình vẫn thấy rơm rớm :") nói chung là phù hợp cho trẻ em
3
616034
2016-05-18 22:00:58
--------------------------
418669
2526
Từ rất lâu Romeo và Juliet đã đi vào lòng người, cặp đôi đáng thương ấy lại được làm lại trong Series Kiệt Tác Văn Chương này, series truyện tranh này mình thấy tạm được. Mình mua về cho một đứa em, khi mở ra thì thấy hình vẽ tạm, nội dung đa số lược đi những tình tiết nhỏ, tình yêu của hai người thật đẹp nhưng lại phải chia xa như thế, mỗi lần xem Romeo và Juliet đa số mình bị ám ảnh vài ba ngày cơ, nói chung việc làm Romeo và Juliet theo bản comic này chỉ ở mức tạm.
3
558720
2016-04-19 18:51:25
--------------------------
364219
2526
Từ hồi xưa em rất thích cặp đôi Romeo và Juliet, đọc truyện về họ thì em lại khóc, cảm thấy thương xót cho họ, cảm thấy bất công vì tình yêu của họ đẹp như vậy mà phải nhận lấy cái kết đau khổ là chết, nhưng điều này đã làm cho cặp đôi này trường tồn theo thời gian, không ai là không biết câu chuyện tình bi thảm của cặp đôi sấu xố này
Cuốn này nét vẽ cũng được, nhưng nhìn Romeo và Juliet hơi buồn cười, Juliet có chân mày hơi đậm :3 nhưng mà, mặc dù diễn biến của cuốn này hơi nhanh nhưng vẫn truyền tải khá đầy đủ nội dung.
Em đã khóc khi xem trang cuối, tác giả minh họa thật sự đã cho họ có 1 khúc kết thật sự có hậu. :)
5
84723
2016-01-05 19:36:43
--------------------------
311849
2526
Các bạn biết không, Romeo và Juliet là 1 câu chuyện cảm động giữa chàng trai Romeo và cô gái Juliet của 2 dòng tộc khác nhau. Nhưng điều đắng cay là 2 dòng tộc này luôn có mối tư thù với nhau. Cuốn sách đã tả lại sâu sắc hình ảnh cặp đôi hoàng hảo này. Qua những trang sách đen trắng dày đặc của cuốn sách này có thể thấy rõ sự tâm đắc và tình cảm của tác giả cho cuốn sách. Mình đã xem qua phim ảnh nhiều lần nhưng từ lúc nghe nói Tiki ra bán quyển sách này, mình đã rất háo hức để được mua và đọc . 
Điểm trừ duy nhất cho quyển sách này là vì nó không có sự lôi cuốn qua những ô truyện. 
Chào thân ái!
4
816535
2015-09-20 11:32:06
--------------------------
