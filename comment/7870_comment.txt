479265
7870
Mình gặp khó khăn khi đưa con vào giấc ngủ- và mình đã quyết định thử dùng phương pháp đọc sách cho bé nghe. Hi vọng con sẽ hợp tác. Hàng mới nhận trên tay, cảm nhận ban đầu là sách bắt mắt- hình ảnh màu sắc sống động. Con gái mình sẽ rất thích cho xem.  Mình định mua cả bộ sách này nhưng có cuốn lại hết hàng nên sẽ ủng hộ tiki dần dần. Nhiều lần mua sách từ tiki thấy yên tâm và ưu điểm là giao hàng khá nhanh ( thường sớm hơn dự kiến)
3
1360726
2016-08-08 15:04:25
--------------------------
295386
7870
Kể một câu chuyện cho bé trước giờ đi ngủ như một chất xúc tác giúp khơi gợi cảm xúc cho trẻ nhỏ, giúp trẻ có những suy nghĩ tích cực, và đó là điều rất quan trọng trong giai đoạn hình thành nhân cách này. Câu chuyện với những chú gà, chú vịt vô cùng dễ thương và quen thuộc sẽ rất thu hút trẻ và coi chúng như những người bạn của mình đang kể về những hoạt động thường ngày mà bé cũng có thể tự mình bắt gặp. Mình tin rằng đây sẽ là những bài học rất có ích cho trẻ.
3
29716
2015-09-10 10:40:13
--------------------------
