471709
4983
Từ một luật sư giàu có và quyền lực, Julian đã từ bỏ tất cả sau cơn đột quỵ thập tử nhất sinh, để đi tìm lại động lực sống và ý nghĩa cuộc đời. Anh đã chọn Ấn Độ làm nơi đặt chân, đất nước huyền bí và là cái nôi của Phật giáo. Anh đã đến và gặp được các nhà hiền triết Sivana và may mắn được tiếp xúc với nguồn trí tuệ thông thái của họ. Dưới sự hướng dẫn của các vị thiền sư, anh đã tìm ra bảy yếu tố tạo nên cuộc sống hạnh phúc. Anh làm theo, trải nghiệm và đã tìm lại được chính mình
5
205349
2016-07-08 21:35:44
--------------------------
456609
4983
Đây là một trong những cuốn sách tuyệt vời viết về sự hoàn thiện cá nhân. Thông qua nhân vật trong câu chuyện, chúng ta có thể tự nhận ra bản thân mình với những rắc rối, lo toan trong cuộc sống. Đọc cuốn sách tôi như hiểu ra những vấn đề của mình, từ đó đặt ra những kế hoạch và những dự định mới phù hợp hơn để hướng tới mục đích hạnh phúc. Tôi không ngại ngần khi giới thiệu cuốn sách cho những người thân quen ̣̣̣- theo như gợi ý của tác giả - bởi vì đơn giản rằng nó thật có ích cho những người thân của bạn.
5
1088094
2016-06-23 16:02:56
--------------------------
395667
4983
Mình rất thích sách của tác giả Robin Sharma. Mình đã đọc "Nhà lãnh đạo không chức danh", "Ba người thầy vĩ đại" rồi đến cuốn "Tìm về sức manh vô biên" mình đã mua ngay khi biết đây là phiên bản tiếng Việt của "The monk who sold his ferrari". Đi tìm sức mạnh tìm ẩn bên trong con người, đi sâu vào con tim để tìm ra mong ước thật sự. Rèn luyện một tinh thần mạnh khỏe và một thể chất mạnh khỏe sự kết hợp đó sẽ mang lại cho bạn một sức bật phi thường để vượt qua mọi thử thách.
4
204540
2016-03-12 11:09:44
--------------------------
327450
4983
Tìm về sức mạnh vô biên.. một cuốn sách nói về hành trình tìm về lại chính mình. Cuộc đời đôi khi đẩy chúng ta đến đỉnh cao của sự nghiệp nhưng đồng thời cũng vùi lấp dần những giá trị hạnh phúc xung quanh. Tìm về sức mạnh vô biên. Tìm hiểu rõ hơn cái mà bản thân mình muốn chưa hẳn đã là tiền. Người đàn ông trong câu chuyện đã từ bỏ tất cả. để đi tìm những người hiền triết và học nhiều bài học vô giá. Cuốn sách như những lời chia sẽ, chân thành, sâu sắc, ý nghĩa, và cuốn hút người đọc ngay từ trang đầu tiên. Sách của Robin Sharma luôn không làm cho người đọc phải thất vọng. Hay, rất hay. mình chỉ nói được có nhiêu.
5
422028
2015-10-27 21:30:29
--------------------------
322075
4983
Trước tiên, tôi xin cảm ơn đến người bạn của tôi trong 1 lần vào thăm bệnh bố tôi tại bệnh viện đã tặng tôi cuốn sách đọc cho đỡ buồn này. Tôi nhận sách với mục đích đọc cho khuây khỏa và không mong đợi sẽ nhận được gì từ cuốn sách này ... Nhưng không, cuốn sách của Robin Sharman như giúp tôi khai sáng tâm trí, càng đọc, tôi càng cảm thấy lối đi trong cuộc đời mình.

Văn phong và cách viết của Robin Sharman gần gũi và bám sát vào mục tiêu của từng mục, đưa ra các biện pháp cụ thể để rèn luyện cho bản thân.

Đối với những ai đang chán nản, đang cần vực dậy tâm trí thì đây là một quyển sách quý báu cho các bạn. Hãy đọc và cảm nhận ở một nơi yên tĩnh, bạn sẽ thấy cuốn sách rất thú vị.
5
861781
2015-10-15 15:25:48
--------------------------
220479
4983
Đây là quyển sách tuyệt vời của Robin Sharma, ngôn từ đơn giản dễ hiểu. Đọc quyển sách này tôi nhận ra sức mạnh bên trong con người mình một cách sâu sắc và rõ ràng hơn bao giờ hết. Quyển sách cũng đưa ra hướng dẫn định hướng để phát triển, khai sáng tiềm năng vốn có của mỗi con người. Tôi đã đọc rất nhiều sách của Robin Sharma, và thực sự rất đáng để sở hữu đầu sách của tác giả này.
5
173621
2015-07-02 16:45:51
--------------------------
195932
4983
Tìm về sức mạnh vô biên của tác giả Robin sharma mang đến cho chúng ta những cách để xây dựng một cuộc sống viên mãn và có nhiều nghĩa. Trong cuộc đời một con người ngoài sức mạnh cơ bắp chúng ta còn có sức mạnh tinh thần mà ngay cả bản thân chúng ta còn chưa khai thác hết được. Dựa vào những câu chuyện hư cấu, tác giả Robin sharma đã chỉ cho chúng ta cách để sống tốt hơn là giúp đỡ những người khác những người cực khổ mà không chút vụ lợi. Một cuốn sách rất hư cấu nhưng ý nghĩa. Xin cảm ơn ông vì tất cả.
5
387632
2015-05-14 22:02:37
--------------------------
