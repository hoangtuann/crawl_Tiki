456264
9106
Ở Tiki bánh cuốn này rẻ hơn so với giá nhà sách nên mình đã đặt luôn một quyển về. Hình thức của cuốn sách thì chẳng có gì để chê cả: cách lượng giấy tốt,  in rõ ràng. Tuy nhiên về phần nội dung thì quyển này khá là khó hiểu và khô khan vì đây là thể loại kịch phi lí. Sẽ rất khó để có thể hiểu được thông điệp của quyển sách nếu không đọc trước bản tiếng Việt về tác phẩm này không có cao trào, cốt truyện và nội dung cụ thể. Thêm vào đó các ngôn từ trong sách là ngôn ngữ nói nên nhiều chỗ mình phải tra cứu mới hiểu được. Dù chỉ có 800 từ thôi nhưng mình thấy nó còn khó hiểu hơn nhiều quyển 1000 từ.
3
1224595
2016-06-23 12:26:46
--------------------------
