377383
4336
Tôi biết đến Shin - Cậu Bé Bút Chì là nhờ một người bạn giới thiệu. Tôi có theo dõi fanpage của Nhà xuất bản Kim Đồng thì thấy bản bìa màu của truyện được ra mắt tại Việt Nam nên tôi đã mua tại Tiki. Shin - cậu bé bút chì quả thực là một quyển truyện rất thú vị với nhiều tình huống cười ra nước mắt với cậu nhóc siêu quậy cùng cô em không kém cạnh. Truyện hay, và vui hơn khi được cầm trên tay những trang sách màu siêu đẹp. Shin - Cậu Bé Bút Chì chính là món quà tinh thần lớn nhất cho trẻ em.
4
308255
2016-02-02 14:03:35
--------------------------
340465
4336
Shin-cậu bé bút chì là một quyển truyện rất thú vị với nhiều tình huống truyện độc đáo và ngộ nghĩnh với những bài học giáo dục nhẹ nhàng nhưng được tác giả lồng ghép vào những trang truyện một cách khéo léo. Các trang truyện được tác giả vẽ bằng những nét vẽ ngây thơ và hồn nhiên gây cho người đọc những cảm giác thích thú. Đặc biệt khi mua sách ở tiki chúng ta có thể nhận được rất nhiều ưu đãi từ tiki. Quyển truyện này giúp chúng ta có thể nhìn lại bản thân và để nhận biết điều tốt xấu trong cuộc sống
5
858875
2015-11-19 22:06:40
--------------------------
