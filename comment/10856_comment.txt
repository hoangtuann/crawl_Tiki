477886
10856
Không biết có phải tôi được đứa bạn của tôi giới thiệu quá mức về cuốn sách hay không mà sau khi đọc xong cuốn sách tôi không thật sự đánh giá cao nó, nó không hề thỏa mãn được cơn thèm tuyển thuyết của tôi.
Đặc biệt cái kết lại là một sự luyến tiếc đến cực độ cho hai nhân vật chính, còn các tình huống trong chuyện thì hơi nghiêng về ngôn tình, lãng mạn nhưng lại thiếu đi tính chất bất ngờ cho người đọc ( trừ cái kết), mọi người gần như có thể đoán được chuyện gì sắp xảy ra tiếp theo.
3
678540
2016-07-29 13:08:59
--------------------------
409006
10856
Nói thật quyển sách này không làm mình hài lòng lắm.
Kể từ đoạn Damen bị đầu độc thì mình đọc không bị cuốn hút vào cuốn sách nữa, cho đến đoạn cuối thì mình hoàn toàn thất vọng.
Tình yêu của Damen dành cho Ever quá lớn, còn Ever thì hình như qúa ngây thơ, ngây thơ đến nỗi tin vào kẻ thù, tin vào kẻ đã đầu độc Damen rồi nghĩ hắn sẽ cho cô thước giải. Khi đọc đến đó thì coi như xong, hết thuốc chữa.
Nếu biết cuốn truyện có kết thúc như thế thì mình sẽ không bao gio mua.
3
818906
2016-04-01 17:22:52
--------------------------
401291
10856
Mở đầu khá dài dòng, đến giữa truyện thì bắt đầu kịch tính và hút người đọc hơn. Thế nhưng kết thúc lại rất nhạt. Có một số đoạn miêu tả tâm lí nhân vật, những sự việc gần như không liên quan mấy đến câu chuyện mà mình khá khó hiểu, có lẽ do k hợp văn phong nước ngoài chăng? Tác giả có vẻ hơi dìm nam chính khi miêu tả đoạn Damen phô bày những suy nghĩ trong đầu mình. Bìa sách và chất lương giấy rất ấn tượng, còn nội dung thì k gây hấp dẫn cho lắm.
3
971974
2016-03-20 14:09:22
--------------------------
369196
10856
Nối tiếp tập 1 Bất tử, tập 2 Trăng xanh này lại lôi cuốn theo một cách riêng. Ở tập này nhân vật xấu xa Roman xuất hiện, khiến mối quan hệ của Ever và Damen ngày càng tệ đi, nhất là khi Damen bị Roman đầu độc, khiến anhd đối xử tệ với Ever. Nhưng Ever không vì thế mà bỏ rơi anh. Cô vẫn có tìm cho ra nguyên nhân dẫn đến sự thay đổi của Damen và tìm thuốc giải độc giúp anh. Dù có đảo ngược quá khứ để tìm lại cuộc sống trước kia nhưng cuối cùng Ever vẫn chọn quay về bên Damen.
4
149610
2016-01-15 12:05:06
--------------------------
154310
10856
Câu chuyện tình yêu của hai người đã bắt đầu kịch tính. Nhưng mà khúc đầu thì hài hước kinh khủng. Thích nhất đoạn Ever xuống hầm bí mật của Ever. Cô phát hiện được anh chàng người yêu của mình sống qua các thời kì, chu du khắp mọi nơi. Cách kể chuyện của tác giả hài hước, hóm hỉnh vô cùng. Với lại mình ghét thằng Roman. Ôi cái thằng đểu giả. Đọc đoạn có mặt nó là ghét vô cùng. Hu hu. Ever bị Roman lừa, Roman hại cả Damen nữa. Nhưng liệu cô có cứu được Damen không? Đó là câu hỏi cần các bạn trả lời trong tập này.
4
120193
2015-01-28 21:56:47
--------------------------
130814
10856
Đó là những nhận xét của mình về phần Trăng Xanh này: Ma mị, gay cấn và cuốn hút đầy hấp dẫn!
Phần 2 này giải thích cũng như nối tiếp những dang dở, những bí mật còn gói gọn trong bọc của nó ở phần 1. Với lối kể có khúc nhẹ nhàng, có khúc đầy cao trào nghẹt thở. Lối kể cũng tiến bộ hơn phần 1 rất nhiều. Dẫn dắt người đọc tốt, kết cấu ít rời rạc hơn so với phần Bất Tử trước đó.
Thiết kế bìa thì khá đẹp, phân lẫn chút u ám, chút ghê rợn và kích thích trí tò mò.
4
303773
2014-10-20 10:16:36
--------------------------
89251
10856
Tập 1 vì đã nghiên cứu xong, tuy cái kết khá gượng gạo nhưng mình cũng nhanh chóng đọc sang tập 2. Diễn biến tập 2 có vẻ hấp dẫn hơn tập 1, nhưng tác giả lại thay đổi bối cảnh liên tục, khiến cho khi mình đọc đôi lúc phải ngừng lại để phân tích thì mới có thể hiểu được. Dù biết trước rằng Ever sẽ chọn lựa hợp lý nhưng mình cũng cảm thấy tội nghiệp cho Damen vì hi sinh hết lòng vì Ever, giờ lại phải đứng nhìn người yêu mình đấu tranh với nhân vật mà mình không ưa tí nào - Roman. Bìa tập 2 có vẻ cuốn hút hơn, hình ảnh đơn giản và đẹp hơn, nên mình rất hài lòng khi đọc tập này.
4
100129
2013-07-31 12:38:04
--------------------------
77492
10856
Nghe tên cứ tưởng Twilight, đó cũng là một ấn tượng ban đầu. Khi biết truyện là phần 2 của Bất Tử, mình cũng khá bất ngờ. Có thể quyển truyện là câu trả lời cho kết thúc khó hiểu của Bất Tử chăng??  Theo mình, Phần hai hay hơn phần 1 nhưng khi đọc truyện, mình không nhập vai vào được nhân vật như những tiểu thuyết khác. Ở tập hai này, truyện khá nhẹ nhàng, tập trung vào tâm lý nhân vật hơn, ít gây cấn hơn . Cách dẫn dắt truyện của tác giả tiến bộ hơn phần 1, nó hấp dẫn và lôi cuốn hơn nhiều. Điểm cộng nữa cái là bìa thiết kế rất đẹp, có sức hấp dẫn cao.
3
57869
2013-05-28 12:09:45
--------------------------
76723
10856
"Trăng xanh - Blue moon" làm mình liên tưởng đến cuốn sách nổi tiếng "Chạng vạng" của Stephenie Meyer. Cả hai cùng là những tác phẩm ly kỳ, hấp dẫn, có sự đan xen giữa những chi tiết kỳ ảo, bí ẩn với những đoạn miêu tả tâm lý nhân vật sâu sắc. Nhưng "Trăng xanh" làm mình yêu thích hơn, bởi lẽ nó có một độ dài vừa phải, và cũng bởi những tình tiết trong truyện có phần kịch tính, tiết tấu truyện nhanh và lôi cuốn hơn, đủ để làm độc giả say sưa đọc từ đầu đến cuối mà không cảm thấy nhàm chán, mệt mỏi. Càng lật mở những trang sách, lại càng cảm thấy thích thú và hào hứng, vì tác giả đã tạo ra một thế giới bí hiểm nhưng chân thực đến bất ngờ. Tác giả đặt nhân vật nữ chính, Ever vào một tình thế tiến thoái lưỡng nan khi cô buộc phải đưa ra một lựa chọn duy nhất, mà lựa chọn nào cũng đều để lại những mất mát và tiếc nuối. Bạn sẽ chẳng biết chuyện gì sẽ xảy đến tiếp theo, chỉ khi trang cuối cùng khép lại bạn mới vỡ òa trong sự bất ngờ và những xúc cảm mãnh liệt nhất.
Một trong những cuốn sách tuyệt vời cho độc giả trẻ.
4
109067
2013-05-24 10:26:09
--------------------------
76422
10856
Cuốn sách này mang nhiều trải nghiệm mới mẻ cũng như những rối rắm trong tình yêu. Cứ tưởng khi đã tiêu diệt được Drina - người tình của Damen thì chuyện tình cảm của Ever và anh chàng sẽ tốt đẹp hơn. Nhưng không biết đâu lại xuất hiện thêm anh chàng Roman - một người bất tứ gây biết bao rối rắm. Chẳng những nhân vật trong truyện gặp rắc rối mà cả người đọc cũng vậy. Tôi cảm thấy hơi khó hiểu phải đọc đi đọc lại vài lần. Ngoài ra khu vườn khu vườn mùa hè - môt nơi có biết bao điều kì diệu và chúa biết bao bí ẩn. Và điều khiến chúng ta tiếc nuối và đôi khi cảm thấy nghẹt thở là việc Ever phải đối đầu với những việc làm độc ác của Roman, tìm mọi cách dể cứu Damen nhưng tất cả đều thất bại. Tuy câu chuyện có phần rối rắm nhưng đó cũng chính là những trải nghiệm mới đầy thách thức.
3
82925
2013-05-22 21:15:02
--------------------------
52780
10856
Cuốn này còn khiến mình đau đâu hơn cả “Bất tử”. Nhân vật Roman xuất hiện quá đột ngột và không tự nhiên. Là một người bất tử giống Damen, Roman  vẫn  luôn nuôi dưỡng một tình yêu mãnh liệt và vô cùng bền bỉ với Drina suốt bấy lâu nay. Để rồi chính tình yêu đó đã hóa thành ngọn lửa căm hờn và khát khao trả thù sau cái chết của Drina.Xuyên suốt “Trăng xanh” đều tập trung nói về sự trả thù của Roman và những nỗ lực cứu vãn không thành của Ever.Sự đối đầu này xem ra khá thú vị và gay cấn. Nhưng suốt câu chuyện, mình lại có cảm giác như đang đọc một vở kịch hơn là một tiểu thuyết. Các phân cảnh cứ thay đổi liên tục từ quá khứ đến hiện tại rồi lại sang một thế giới khác. Câu chữ được diễn đạt quá dài dòng nhưng vẫn không rõ nghĩa, đôi lúc thục sự không thể nắm bắt được rốt cuộc tác giả đang viết về điều gì. Có lẽ mình sẽ phải cân nhắc lại việc có nên mua tiếp tập ba của bộ tiểu thuyết này hay không. 

1
13387
2012-12-28 19:04:42
--------------------------
47529
10856
Mình tìm đến cuốn sách này vì sự tò mò, vì sự hấp dẫn mà cái tên "Trăng Xanh" đem lại. Một cuốn truyện với phong cách kỳ ảo lãng mạn, giống với "Chạng Vạng" và nhiều truyện khác. Nhưng cuốn sách này hấp dẫn mình hơn, làm mình cảm thấy hài lòng hơn. Đó chính là bởi cách dẫn dắt câu chuyện của tác giả rất hấp dẫn, những tình tiết có diễn tiến nhanh nên đọc không cảm giác mệt mỏi. Thêm vào đó là ngôn ngữ miêu tả tâm lý, cũng như tình huống ngặt nghèo của truyện, tạo nên những nút thắt đến ngẹt thở. Trên hết, đây là một câu chuyện về tình yêu, về những sự lựa chọn có thể làm thay đổi cuộc sống con người, về những ngã rẽ không lường trước mà ai cũng sẽ phải đối mặt. "Trăng xanh" thực sự hấp dẫn và thú vị đối với mình. ^^
4
20073
2012-11-24 09:18:11
--------------------------
29318
10856
Hồi mình ra nhà sách và thấy cuốn này. Mình đã đọc thử lời giời thiệu ngoài bìa. Thật sự thấy rất tò mò. Nên đã mua về nhà đọc cuồn đầu tiên là "Bất tử", tuy Phần 1 mình thấy kết thúc hơi hụt hẫng(giống như một bạn đã từng nhận xét) nhưng bù lại phần hai tác giả có vẻ đã cố gắng bù đắp cho nên đối với mình, mình hài lòng về phần 2, diễn biến truyện có chậm nhưng đó lại là cái hay riêng của truyện, nhẹ nhàng(thế nên những bạn thích đọc lôi cuốn giật gân thì có thể ko thích), mang nét bi thương, dằn vặt, nội tâm nhân vật tốt đó chứ. Mình đọc mà còn thấy đồng cảm, xót thương cho Ever. Còn về kiếp trước của nv nữ ở phần 1 ko kể ra cụ thể nên một số nguời sẽ cảm thấy khó chịu và cho rằng tác giả đã quá hời hợt thì phần hai tác giả đã đưa chi tiết này vào. Với lại theo mình đọc và hiểu thì Damen ko thích nhắc đến quá khứ vì quá khứ của anh kể cả chuyện tình với Ever cũng gian nan khổ sở.Vì vậy để khắc họa một phần tính cách của Damen,tác giả đã giấu nhẹm nhiều chi tiết thay vì để anh khai tất tần tật, thế sẽ mất vui,chẳng còn gì để tò mò nữa . Nói chung phần hai khá good.
4
13964
2012-06-05 06:36:17
--------------------------
22079
10856
Đọc xong truyện này tự dưng tiếc, tiếc cho Damen, tiếc cho tình yêu của anh, tiếc cho việc anh yêu phải một người như Ever, ngoài ra còn thấy may mắn, may mắn vì mình không lâm phải tình cảnh như Ever.

Đọc gần như trọn vẹn 2 tập truyện Bất tử và Trăng xanh (Trăng xanh đang nghiên cứu, căn bản đoạn kết và lựa chọn của Ever thật khiến người ta.... thở dài) có thể thấy những bi kịch tình yêu đều xây dựng trên những kẻ không si tình thì cũng (nói không hoa mỹ) là những kẻ mang đầy dục vọng và thèm khát. Đặc biệt là những người này đều sống rất lâu rất lâu, thời gian mài mòn họ, làm họ mất đi nhiều khao khát, nhưng lại tăng hơn nhiều cố chấp.

Nếu Damen si tình, thì anh cô chấp với tình yêu cảu Ever, kiếp này sang kiếp khác, năm này qua năm khác, anh một mực đợi cô và yêu cô.

Nếu coi Drina yêu Damen, thì tình yêu đấy qua thời gian cọ sát lại dần thay đổi, trờ thành một loại cố chấp ham muốn Damen, ham muốn tình cảm của Damen.

Và Roman, một nhân vật phản diện nhưng cũng hết sức chung tình, anh yêu Drina, si mê cô, thậm chí dần dần cũng thành ham muốn cô, những hỡi ôi, cô chỉ một mực hướng về Damen.

Còn Ever thì sao? Cô có tình yêu của Damen, nhưng đó là chưa đủ, cô vẫn không hể buong bỏ quá khứ, cô không thể buông bỏ gia đình mình, nên lựa chọn của cô đưa đến chỉ có triền miên dằn vặt, dằn vặt cô và chính người cô yêu.

Vì những truy đuổi bất tận kia mà bi kịch khời đầu, kết thúc của nó chẳng qua chỉ là mở màn cho c\một bi kịch mới.
3
18387
2012-03-19 21:31:04
--------------------------
6749
10856
Tập một đã cuốn hút người đọc vào tinh tiết của câu truyện,  tập hai lại càng hồi hộp, hấp dẫn hơn khi Ever phải đối mặt giữa 2 sự chọn lựa khó  khăn nhất đó là quay lại quá khứ cứu gia đình hay cứu người mà cô yêu nhất, Damen. Tuy tôi  hơi ngạc nhiên và thấy không hài  lòng lắm trước sự chọn lựa của Ever nhưng suy nghĩ kĩ mới thấy cũng hợp lý nếu là tôi thì tôi cũng sẽ hành động như vậy. Truyện đề cao tình cảm giữa người với người, đặc biệt là tình yêu được  lồng ghép trong cốt truyện tuy quen thuộc nhưng dưới ngòi bút của tác giả nó đã được đưa lên một tầm cao mới tạo nên một nét riêng, một sự khác biệt mà chỉ có thể tìm thấy trong bộ truyện bất tử này. 
5
6536
2011-06-22 23:29:16
--------------------------
