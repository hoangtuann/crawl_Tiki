489836
3420
Mình đã nhận được sách sau bao ngày chờ mong. Sách được in màu đẹp, lại không bị mùi khó chịu. Sách hướng dẫn cụ thể các hoạt động trò chơi kích thích phát triển của trẻ từ 0 - 6 tuổi. Tác giả phân tích cụ thể mỗi trò chơi phát triển theo 8 loại hình trí thông minh của trẻ. Rất đáng để mua và cùng dành thời gian chơi với con một cách say mê và vui vẻ.
4
734643
2016-11-10 10:20:58
--------------------------
485710
3420
Mình mới mua cuốn sách "Giờ chơi đến rồi" này vì muốn chơi cùng con mà nhiều lúc lại nhức đầu kg biết bày trò gì để cùng con những giây phút vui vẻ và đồng thời cũng giúp con học hỏi. Rất may khi biết tới cuốn sách này. Sách hướng dẫn các nguyên vật liệu cần có và nêu ra thật nhiều các trò chơi phù hợp với từng lứa tuổi cho các bé mẫu giáo. Tuy nhiên theo mình thấy kg phải tất cả mọi trò chơi trong sách đều áp dụng được vì nó hạn chế một số vật liệu mà mình kg mua được. Tiki giao hàng nhanh và cẩn thận. 
3
1114461
2016-10-05 14:53:29
--------------------------
466813
3420
Quyển sách dược thiết kế rất đẹp với nhiều hình ảnh minh hoạ bắt mắt, chất lượng giấy bóng rất tốt. Nội dung sách bao gồm nhiều gợi ý về các trò chơi có thể chơi cùng bé theo từng độ tuổi. Các trò chơi được đề cập trong sách chủ yếu là những trò tương tác, cần có người hướng dẫn và chơi cùng bé, rất tốt cho sự phát triển của bé, tuy nhiên cần có sự đầu tư thời gian của ba mẹ. Một số trò trong sách đề xuất theo mình là chưa phù hợp với bé nhỏ vì có nhiều chi tiết nhỏ (như chơi với hạt đậu) có thể gây hóc, nguy hiểm cho bé.
4
438329
2016-07-02 22:32:24
--------------------------
465299
3420
Hình ảnh tươi sáng, săc nét. Chât lượng giấy tốt, dày dặn. Các trò chơi cho bé dưới 1 tuổi hơi ít, trên 1 tuổi thì nhiều hơn. Từ khi có cuốn sách này mình không phải đau đầu nghĩ ra các trò chơi cho con. Có cuốn sách này bố mẹ không nhất thiết phải mua nhiều món đồ chơi cho con. Trong sách có khá nhiều trò chơi từ các vật dụng handmade, rất tuyệt. Phần nhận xét về các kỹ năng mà bé đạt được thông qua từng trò chơi cũng rất hữu ích. Lúc đầu cũng khá phân vân không biết có nên mua sách này không, nhưng nhờ vào những nhận xét tích cực từ các mẹ, mình đã mua và không hề hối tiếc.
5
274713
2016-06-30 23:02:22
--------------------------
464731
3420
Sách in trên nền giấy tốt, xen kẽ hình ảnh minh họa tuy chưa đẹp (đẹp tùy mắt).
Nội dung tích cực cho các bậc làm cha mẹ muốn con cái thụ hưởng các trò chơi lành mạnh và sáng tạo. Mình có cảm giác người viết tư duy theo lối miền Bắc nhiều hơn. Nhưng dù sao đi nữa, vẫn tốt hơn là các bậc cha mẹ hay dẫn con đi ăn thức ăn nhanh và quăng cho cái tablet. Sách cũng hướng dẫn mình và con cái thắt chặt sợi dây tình cảm thiêng liêng. Mình sẽ cố gắng đọc hết!
4
1121073
2016-06-30 14:07:00
--------------------------
464587
3420
Giờ chơi đến rồi là một quyển sách đẹp về hình thức, chất lượng giấy dày đẹp, hình ảnh minh hoạ nhiều màu sắc. Sách đã giới thiệu nhiều trò chơi gợi ý cho bố mẹ để cùng vui chơi với con. Tuy nhiên theo mình thì phần viết giới thiệu hơi nhiều trong khi phần trò chơi thì không nhiều lắm. Điểm cộng là sách đã phần nào giúp các mẹ đỡ nhức đầu suy nghĩ bày trò chơi gì cho con. Các trò chơi được hướng dẫn trong sách đa phần là handmade giúp cả mẹ và con đều có thể phát huy hết được tính sáng tạo của mình.
4
460351
2016-06-30 11:47:50
--------------------------
462426
3420
Nhận sách mình thấy rất thích phần hình thức. Khổ sách nhỏ như các quyển tiểu thuyết nên mình có thể bỏ vào giỏ sách để đọc trong lúc rảnh rỗi. Sách được in màu trên nền giấy dày, trình bày hợp lý nên nhìn không bị lóa mắt. Về nội dung có phần lý thuyết và hình ảnh hướng dẫn thực hành kỹ năng. Từ khi có quyển sách mình đã áp dụng một số trò chơi trong sách với con mình. Cũng từ đó hai mẹ con mình càng gần gũi thân thiết hơn. Bé thấy mình rãnh là đòi chơi không à.
5
861432
2016-06-28 16:07:23
--------------------------
435157
3420
Tôi biết được quyển sách này thông qua facebook nhưng vẫn chưa có dịp ghé nhà sách để mua vì thông tin hữu ích trong đó. Tình cơ trông thấy qua Tiki, tôi chọn ngay không suy nghĩ.
Thời đại công nghệ, bố mẹ luôn tất bật với công việc, đôi khi họ cũng cuốn con theo công nghệ số, đó là một điều không hay. Quyển sách không chỉ đơn thuần là các trò chơi mà còn có cả lý thuyết, giúp bố mẹ hiểu được tường tận vấn đề tại sao lại chơi trò chơi này, trò này phù hợp với tính cách của bé ra sao...
Sách viết bằng giấy bóng, khá khó đọc và chữ nhỏ.
3
1017785
2016-05-24 15:23:17
--------------------------
421461
3420
Từ khi cuốn sách được quảng cáo trên facebook, mình đã thấy rất thích. Khi vào tiki thấy có bán sách này mình đã đặt mua liền. Đây là một cuốn sách cực kỳ hữu ích cho cha mẹ khi không muốn con cái dành quá nhiều thời gian với các thiết bị điện tử. Các trò chơi đều có nguyên liệu dễ kiếm, dễ làm và phù hợp với từng giai đoạn phát triển của trẻ, kích thích sự sáng tạo của con cũng như tạo mối liên kết giữa cha mẹ và con cái nhiều hơn. Hình minh họa rất đẹp, rõ ràng, dễ hiểu. Cám ơn tác giả đã viết nên một quyển sách có ích đến vậy. 
5
117222
2016-04-25 10:17:48
--------------------------
390223
3420
Một quyển sách hay và thú vị. Nhưng không dễ để áp dụng thực tế, dù nhiều trò có vẻ rất đơn giản. Bản thân mình dù lúc nghe giới thiệu trên fanpage thì thấy vô cùng hứng thú, nhưng mua về thì thấy hơi lãng phí. Vì thật ra với trẻ con bất cứ vật dụng gì cũng mới lạ và biến thành đồ chơi của trẻ. Có nhiều trò ghi chú dành cho bé 2 tuổi nhưng trẻ cần có vốn từ nhất định, hiểu ý cha mẹ nhiều hơn thì trò chơi mới hiệu quả. Điểm cộng là sách in rất đẹp và dày
3
150859
2016-03-03 15:12:22
--------------------------
373329
3420
Điểm nổi bật nhất của sách là màu sắc đẹp, in màu. Giá cả hợp lý so với những cuốn sách in màu khác.
Cuốn sách này sẽ giúp các bà mẹ có những ý tưởng để chơi cùng con từ những đồ dùng sẵn có, có nhiều trải nghiệm thú vị mà việc mua đồ chơi sẵn trên thị trường không mang lại được.
Có vài hoạt động hơi khó thực hiện nhưng đa số là các hoạt động đơn giản nhưng vui vè. 
Sách hoạt động và trò chơi cho con hơi ít trên thị trường hiện nay. Cuốn sách này rất đáng để các bạn tham khảo
4
619311
2016-01-23 14:21:39
--------------------------
353580
3420
Nhiều phụ huynh có góc nhìn lo lắng cho con cái về việc dạy dỗ con theo tư duy như nào , mạch lạc và lôi cuốn nhiều người đi sâu vào khai thác khả năng toàn diện của trẻ như sự tư vấn cách học hiệu quả lớn , tác giả ghi ghép rất đầy đủ , phóng khoáng tạo ra môi trường làm việc với trẻ vô cùng thoải mái , thay đổi được nhiều kỹ năng của trẻ nhỏ , khá tin tưởng vào cách đi sâu tâm lý mạnh mẽ của trẻ khi đối phó với nhiều tình huống khác nhau .
4
402468
2015-12-16 14:16:17
--------------------------
352780
3420
Cuốn sách rất đẹp, chỉ dẫn khá chi tiết cách làm và cách chơi các trò chơi dành cho các lứa tuổi. Cuốn sách đi lấy tuổi của trẻ làm mốc dựa vào đó đưa ra các trò chơi phù hợp với nhận thức của các con và tiện cho bố mẹ theo dõi. Những trò chơi được đưa vào cuốn sách rất dễ thực hiện qua những đồ vật xung quanh lại không tốn nhiều thời gian và công sức chuẩn bị nên bố mẹ có nhiều thời gian tập trung chơi với trẻ mà không quá ngại vì công việc chuẩn bị phức tạp, cầu kỳ.
4
388606
2015-12-14 23:47:49
--------------------------
340647
3420
Trẻ con cần gì nhất ở bố mẹ? Không phải là những đồ chơi, vật dụng đắt tiền... mà đơn giản chỉ là thời gian của bố mẹ - được chơi cùng bồ mẹ. Qua việc chơi cùng con, bố mẹ và con sẽ có thêm thời gian gần gũi, chia sẻ, gắn bó và hiểu nhau hơn. Vì thế mình luôn muốn dành thời gian nhiều nhất để chơi với con, tương tác trực tiếp với con, để con không bị quyến rũ và sa đà vào ti vi, ipad... Nhưng thú thật, đôi lúc mình bị bí, không có ý tưởng, không nghĩ ra được trò mới để 2 mẹ con cùng chơi với nhau! Và thật may mắn khi mình tìm được cuốn sách này! Sách có hình ảnh màu sắc đẹp, nội dung thiết thực, hướng dẫn rõ ràng, dễ hiểu, các trò chơi thích hợp từng độ tuổi giúp cho bố mẹ áp dụng cho con cái theo từng giai đoạn phát triển của bé. Không những thế, sách còn dạy cho trẻ và cả mình về sự sáng tạo, tiết kiệm, khi  tận dụng lại những vật dụng tưởng chừng như bỏ đi, lại được đem ra làm các trò chơi vừa vui, vừa hứng thú. 

4
7496
2015-11-20 11:11:12
--------------------------
333691
3420
Sách in màu đẹp, hình ảnh sinh động, tuy nhiên chữ nhỏ nên mình thấy hơi khó đọc. Sách giới thiệu cho các phụ huynh có con nhỏ những trò chơi bổ ích, sáng tạo để phát triển cho con các kỹ năng. Tuy nhiên các trò chơi vẫn chưa đa dạng lắm, nhưng trong điều kiện trẻ em hiện nay bị thiếu chỗ vui chơi và cha mẹ không có nhiều thời gian dành cho con thì những quyển sách như thế này sẽ giúp cha mẹ có những khoảng thời gian vui đùa cùng con trẻ với những trò chơi tự chế. 
4
524888
2015-11-08 10:36:59
--------------------------
311428
3420
Cuốn sách được trình bày bằng giấy màu đẹp với hình ảnh minh họa sinh động, nội dung toàn diện từ khái niệm cơ bản về đồ chơi và tác động đến sự hình thành kỹ năng của trẻ, đến những thứ cần chuẩn bị để làm đồ chơi cũng như cách làm đồ chơi phù hợp cho từng giai đoạn phát triển của trẻ. Qua cuốn sách, bố mẹ hiểu thêm về vai trò quan trọng của việc được vui chơi đối với trẻ để dành thêm nhiều thời gian tự làm những món đồ nhỏ xinh cho những đứa con thân yêu của mình
4
65046
2015-09-19 23:10:44
--------------------------
307614
3420
Sách có hình ảnh màu sắc đẹp, nội dung thiết thực, các trò chơi thích hợp từng độ tuổi giúp cho bố mẹ áp dụng cho con cái theo từng giai đoạn phát triển của bé. Mình thích ở chỗ là sự sáng tạo đồ chơi, những vật dụng tưởng chừng như bỏ đi nhưng được đem ra làm đồ chơi cho bé, vừa tiết kiệm, vừa vui, phụ huynh và con cùng làm thì càng hiểu nhau hơn.
Tuy nhiên chữ viết hơi nhỏ, nếu chữ viết to hơn thì sẽ dễ theo dõi, về thị cụ thì nếu được tác giả cho biết luôn địa chỉ mua đồ (những thứ ít có ở nhà sách, ví dụ như: vải nỉ, hồ nước...) ở đâu để phụ huynh dễ dàng tìm mua.
4
711670
2015-09-18 10:35:57
--------------------------
306916
3420
Cuốn sách "Giờ chơi đến rồi" là cuốn cẩm nang cho cha mẹ dạy con trẻ thông qua các trò chơi trí tuệ, giúp kích thích sự phát triển trí não của trẻ. Sách in màu, giấy đẹp, với các hình ảnh minh họa giúp cha mẹ dễ dàng thực hiện và chơi cùng con. Bé nhà mình tỏ ra rất thích thú với những trò chơi này. Trong một thế giới mà công nghệ đang làm thay đổi mọi thứ, những trò chơi như thế này sẽ giúp trẻ tránh xa những cám dỗ từ các trò chơi điện tử và lại giúp cha mẹ và con cái thêm gắn kết.
4
532862
2015-09-17 21:20:45
--------------------------
302421
3420
Tiêu đề cuốn sách Giờ chơi đến rồi thực sự thu hút khiến mình muốn tậu ngay một cuốn. Đây giống như là một kim chỉ nam dành cho các gia đình muốn thông qua việc chơi trò chơi kích thích trí não các bé. Sách hướng dẫn chi tiết, rõ ràng, hình minh họa đẹp, giấy tốt tuy có vài trò hơi khó thực hiện. Mình đã làm thử một vài trò chơi trong đó và bé rất thích thú. Tác giả của cuốn sách có fan page trên Facebook, bố mẹ nào thích hay thắc mắc thì cứ lên đó trò chuyện và chia sẻ. 
4
32656
2015-09-15 10:53:54
--------------------------
294368
3420
Quyển sách Giờ chơi đến rồi  là quyển cẩm nang của các bậc cha mẹ, sách gợi ý những cách làm đồ chơi cũng như cách chơi với con thế nào để kích thích trí não trẻ phát triển. Có thể chúng ta sẽ không thể có đủ thời gian để thực hiện các trò chơi trong đó. Tuy nhiên thành thực mà nói thì cha mẹ nào làm hết các trò chơi trong đấy, cũng như có thể có nhiều thời gian để cùng chơi với con thì quả thật là những cha mẹ tuyệt vời.
Sách viết khá hay và hữu ích với những hướng dẫn khá cụ thể và dễ làm.
4
728028
2015-09-09 11:16:46
--------------------------
293696
3420
Mình có em bé 2 tuổi. Đang ở tuổi khám phá, bất cứ đồ vật gì cũng là đồ chơi của bé. Từ khi có cuốn sách này mình học được rất nhiều kinh nghiệm để làm đồ chơi cho bé. Đồng thời cũng giúp kích thích những kỹ năng bé học được và phát triển toàn diện. Rất thích cách đánh giá qua mỗi trò chơi bé học được gì, kỹ năng gì phát triển. Qua đó mình cũng hiểu được em bé nhà mình hơn. Thế mạnh của bạn ấy là gì. Từ đó sẽ chú trọng phát triển thêm nhiều trò chơi cho bạn hơn.
4
351
2015-09-08 16:03:06
--------------------------
293616
3420
Đây là cuốn sách mình rất tâm đắc khi mua. Mình tình cờ biết đến sách thông qua bạn bè giới thiệu trên FB. Mình đã rất tò mò không biết sách sẽ đưa ra các hướng dẫn như thế nào. Sách đưa ra các hướng dẫn cụ thể cho từng trò chơi. Đặc biệt lồng ghép vào các hình ảnh, màu sắc rất rõ ràng dễ hiểu. Mình đã rất băn khoăn vì không biết chơi gì với con. Sau khi mua sách này thì mình  không còn lo lắng nữa. Sách là một kho tàng trò chơi mà mình có thể áp dụng thay đổi mỗi ngày để chơi với bé. 
5
133716
2015-09-08 14:55:02
--------------------------
293303
3420
Sách khá hữu ích cho cha mẹ. Đặc biệt trong thời đại này, các bậc làm cha mẹ có rất ít thời gian cho con trẻ và có nhiều thú vui thời đại khác như ti vi, smart phone nên cuốn sách sẽ bổ ích rất nhiều. Nhưng có lẽ hữu ích nhất là giai đoạn bé còn nhỏ, khoảng 2 - 4 tuổi. Các bà mẹ có con còn nhỏ thì nên mua sớm và tranh thủ chơi sớm. Sách trình bày đẹp, màu sắc bắt mắt, hình ảnh minh họa dễ hiểu. Khuyết điểm của sách là cỡ chữ nhỏ, mình đọc còn khó nên mình không dám cho bé con mình đọc, dù bé thấy sách bé rất thích. Mình nghĩ loại sách này thì nên có cỡ chữ lớn hơn, vì nhiều khi mình vừa xem vừa thực hiện nên chữ nhỏ rất bất tiện.
4
304770
2015-09-08 10:13:18
--------------------------
292027
3420
Sách  có hình minh họa rất đẹp, mỗi trò chơi là 1 hình, rất dễ dàng thực hiện .Khi đọc xong quyển sách này, mình thấy có thể biến mọi thứ đơn giản tưởng chứng như vô dụng để biến thành đồ chơi cho con. Sách chia thành 2 chương, chương 1 giúp hiểu rõ hơn về nhưng hoạt động vui chơi của trẻ (vận dụng đa giác quan, kể chuyện sao cho hấp dẫn, chọn đồ chơi cho con..., vật liệu cần để làm trò chơi). Chương 2 chia thành 4 mức độ trò chơi cho trẻ từ 0 đến trên 3 tuổi. Mình mua quyển này cùng với quyển mỗi ngày 1 trò chơi, Cuốn Giờ chơi đến rồi nội dung hấp dẫn, bắt mắt hơn . Chỉ có 1 khuyết điểm đó là font chữ hơi nhỏ 

5
163590
2015-09-06 22:32:13
--------------------------
286147
3420
Sách dày, bìa đẹp, sách màu nên màu sắc coi rất thích mắt. Sách có chỉ cách làm các trò chơi cho trẻ. Mình mới thử một vài trò thì thấy cháu cũng thích. Để dần rồi nghiên cứu xem sao. Nhận thấy đây là một quyển sách rất hữu ích đối với các bậc phụ huynh, có thể phát triển toàn diện cho trẻ và cũng vừa chơi với con. Sách trình bày đẹp cũng làm cho mình thấy hứng thu hơn. Sách màu và mình mua với giá 69.000 thì không quá mắc. Một sự lựa chọn hài lòng.
5
136323
2015-09-01 14:33:48
--------------------------
283939
3420
Sách được thiết kế đẹp và những trò chơi vô cùng sáng tạo. Các mẹ có con dưới 1 tuổi nên đầu tư sách này sớm để có thể áp dụng được nhiều trò chơi được hướng dẫn trong sách. Mình mua sách khi bé mình đã trên 3 tuổi nên không tận dụng được nhiều. Tuy vậy, sách cũng cho mình nhiều ý tưởng để tự thiết kế trò chơi cho con. Sách cũng đưa ra những bằng chứng khoa học về phát triển trí tuệ kho trẻ khi chơi. Bố mẹ hãy bớt chút thời gian để chơi với con nhiều hơn, chơi "chất lượng" hơn!
5
331353
2015-08-30 14:36:46
--------------------------
271760
3420
Đôi khi cuộc sống bộn bề kéo bạn theo vòng xoáy khiến bạn "quên" mất những đứa con của mình. Hãy để trên đầu giường cuốn sách này để luôn nhắc nhở bạn hằng ngày, để cùng con khám phá những điều kì diệu qua những trò chơi nhỏ. Có những trò chơi có vẻ..."trẻ con" quá, đơn giản quá so với bạn nhưng với con trẻ, đó là cả một thế giới mới. Cảm ơn hai bạn Mai Trang và Thu Hằng đã mang đến cho các bé những trò chơi thú vị.
4
405619
2015-08-19 13:54:42
--------------------------
270256
3420
Quyển sách được thiết kế đẹp và nội dung gần gũi thân thiện với bố mẹ. Mình đi làm cả ngày nên thời gian giành cho con là rất ít. Tuy nhiên, mình vẫn rất muốn chơi với con và qua đó giúp con phát triển toàn diện. Quyển sách này đưa ra các nội dung lý thuyết và thực tế về nội dung các trò chơi sẽ kích thích trẻ phát triển toàn diện ra sao. trẻ em hiện nay không chỉ có phát triển về cân nặng mà còn phát triển toàn diện về nhiều loại hình trí thông minh. Do đó, qua quyển sách này, mình biết cần phải vận dụng các trò chơi để giúp trẻ phát triển. Tuy nhiên, mình rất mong sách sẽ bớt các nội dung lý thuyết mà thêm nhiều hơn các nội dung trò chơi thực tế, đơn giản mà các bố mẹ có thể dễ áp dụng cho các con.
4
300425
2015-08-18 08:27:18
--------------------------
269648
3420
Cuốn sách được dành tặng cho các bạn thiếu nhi, Ấn tượng đầu tiên khi cầm cuốn sách lên chất lượng in ấn và cách trình bày rất tốt, đây là sách Nhã Nam nên chất lượng sách cũng được đảm bảo. Về nội dung cuốn sách thì đã đưa ra các phương pháp các minh chứng rõ ràng cùng cách dẫn chuyện hay đã tạo nên một câu chuyện thú vị. Đây là cuốn sách chủ yếu viết về các phương pháp và cách vui chơi, học tập với trẻ nên có thể người lớn khi xem qua sẽ thấy không thú vị. Mình cũng rất ấn tượng về dịch vụ giao hàng, dịch vụ chăm sóc và phản hồi ý kiến của khách hàng từ tiki. Sách được bọc cẩn thận và giao hàng nhanh.
4
730145
2015-08-17 17:02:33
--------------------------
266003
3420
Là một người mẹ nhiều khi bận rộn với công việc, khi được về bên cạnh con để con có những giờ chơi vui vẻ với mẹ là một điều mong tưởng của mình. Thật sự nhiều khi chẳng biết chơi gì với con, mình tò mò khi thấy quyển Giờ Chơi Đến Rồi, mua ngay nó và đọc ngay trong vòng 2 ngày. Quyển sách giúp ích mình rất nhiều trong các giờ bên cạnh con, nắm bắt tâm lý bé và chơi các trò chơi với con thật vui vẻ. Thấy con thích thú là một niềm vui của mình. Cảm ơn Giờ Chơi Đến Rồi.
5
431238
2015-08-14 12:15:27
--------------------------
261421
3420
Sách rất đẹp, thú vị và hữu ích. Mỗi ngày ở cạnh con, bạn có biết bao nhiêu thời gian và cần nghĩ ra biết bao trò chơi để cùng con trải qua những khoảng thời gian hạnh phúc đó. Sách cho bạn tất cả những gợi ý cần có cho các lứa tuổi, đồng thời có những phân tích xác đáng về các kỹ năng của các trò chơi mang lại cho bé. Sách in màu và có hình minh họa rất đẹp, ấn tượng và dễ hình dung. Nếu bạn là một người mẹ và đang bí trò chơi với con, đây là câu trả lời đáng giá nhất
4
478786
2015-08-11 08:46:34
--------------------------
253068
3420
Mình cũng hay sưu tầm các trò chơi để chơi cùng con nhưng nhiều khi đọc xong rồi quên mất hoặc không lưu lại. Có một quyển sách như thế này thật tiện lợi những lúc không biết chơi gì với con. Tác giả cũng phân ra trò chơi dành cho từng lứa tuổi và đánh giá trò chơi sẽ giúp phát triển kĩ năng nào, ví dụ vận động tinh, vận động thô.... Đây là 1 ý tưởng hay và hữu ích. Tác giả cũng viết hướng dẫn chi tiết và có hình minh họa đẹp cho mỗi trò chơi. Tóm lại mình thích cuốn sách này và nghĩ đây là một quyển sách mà các bố mẹ trẻ nên tham khảo.
5
289860
2015-08-04 09:24:25
--------------------------
252204
3420
Bé của mình rất thích cuốn sách này. Sách rất đẹp, có rất nhiều hình ảnh minh họa thực tế sống động, màu sắc tươi sáng bắt mắt. Các trò chơi trong sách khá đơn giản, dễ thực hiện, được hướng dẫn rất cụ thể, rõ ràng bằng hình ảnh. Cả nhà có thể cùng xem sách, và cùng chơi với nhau rất vui vẻ, giúp kích thích sự khám phá, trí tưởng tượng, cảm xúc của các bé, các bé sẽ rất thích. Ngoài ra còn giúp các bé tránh xa tivi, ipad hay điện thoại, giúp tình cảm cả nhà thêm gắn bó.
4
476955
2015-08-03 15:14:57
--------------------------
247871
3420
Quyển sách này đúng là 1 cẩm nang trò chơi cho bé. Hướng dẫn rất cụ thể từ cách làm màu vẽ đến cách làm đất nặn homemade cho bé chơi mà không sợ độc hại. Sách được in màu toàn bộ từ đầu trang đến cuối trang. Hình ảnh thực tế, dễ thực hiện. Đặc biệt các trò chơi đều đã được phân theo các độ tuổi. Hướng dẫn các trò chơi ngắn gọn, dễ hiểu . Mình sẽ giới thiệu quyển sách này cho các bạn bè của mình để họ không phải đau đầu khi chọn đồ chơi cho con 
4
727266
2015-07-30 16:54:56
--------------------------
236914
3420
Mình đã mua quyển sách sau khi tham gia vào page "Giờ chơi đến rồi" và trang facebook của 2 tác giả. Mình bị cuốn hút bởi các trò chơi thú vị trên các trang này và quyết định mua sách. Quyển sách không làm mình thất vọng: sách có hình thức đẹp, in màu, các trò chơi được sắp xếp theo độ tuổi và các kỹ năng bé đạt được thông qua mỗi trò chơi. Một số trò chơi mình đã biết khi tham khảo các website nước ngoài, giờ được thể hiện trong sách dưới hình thức khác sáng tạo hơn, hệ thống hơn. Quyển sách là động lực để mỗi ngày mình dành thời gian chơi cùng con, làm đồ chơi cho con. Mình cũng đã giới thiệu cho bạn bè mua quyển sách này và cũng nhận được nhận xét khá tích cực. Tuy nhiên, các trò chơi chủ yếu dành cho bé 2 tuổi trở lên. Con mình dưới 1 tuổi thì số trò chơi còn hơi ít.
4
381665
2015-07-22 08:28:59
--------------------------
225322
3420
Trước giờ mình luôn băn khoăn không biết tìm đồ chơi thích hợp và an toàn cho con. Bên cạnh đó, đồ chơi còn phải thích hợp để mẹ và bé cùng chơi, giúp con vừa chơi vừa học. Quyển sách ra đời như một cuốn cẩm nang bỏ túi, hình ảnh sống động, lời văn ngắn gọn, dễ hiểu, giúp mẹ không phải suy nghĩ nhiều về việc chơi cùng con. Con mình vừa cai nghiện máy vi tính xong nên đây là bảo bối giúp cả nhà mình chơi vui vẻ. Tuy nhiên, để chuẩn bị các trò chơi như sách hướng dẫn, các mẹ phải chịu khó tìm tòi nguyên liệu cộng một chút khéo tay, một chút kiên nhẫn và một chút thời gian. Có như vậy mới được hưởng thành quả từ con yêu các mẹ nhé!
4
143088
2015-07-10 10:41:35
--------------------------
223647
3420
Sách được in màu toàn bộ từ trang đầu đến trang cuối, hình ảnh và màu sắc bắt mắt nổi bật, nên nhìn cuốn sách này có vẻ rất chi là snag chảnh. :D
Hình ảnh thực tế, dễ thực hiện cho cả người lớn và bé nhỏ. Mỗi trò chơi đều hướng dẫn dụng cụ cần thiết, các bước chơi thích hợp và phân chia theo độ tuổi.
Hướng dẫn trò chơi tương đối ngắn gọn, gần giống như kiểu gạch đầu dòng truyền thống nên dễ hiểu, không màu mè, kiểu cách.
Một cuốn sách thú vị.
4
566059
2015-07-07 13:35:59
--------------------------
219964
3420
Sách rất đẹp được in màu toàn bộ, nhiều hình ảnh và màu sắc. Điểm cộng là các ảnh trong sách đều được chụp từ trò chơi thực tế rất đẹp và dễ hình dung. Đôi lúc mình chỉ cần nhìn hình ảnh là có thể tự tạo ra trò chơi cho bé nhà mình hoặc kể truyện cho bé từ những hình ảnh đó. Hướng dẫn của các trò chơi thì cũng khá ngắn gọn dễ thực hành, nguyên liêu tổ chức thì cũng khá dễ kiếm tuy nhiên vẫn đòi hỏi các bạn có thời gian và chịu khó làm.
Nếu bạn nào không có điều kiện mua sách thì có thể tham khảo thêm ở facebook giochoidenroi đây là facebook của tác giả cũng có rất nhiều thông tin, bạn có thể tham khảo để tổ chức trò chơi cho bé giúp bé phát triển trí não và phản xạ
4
153008
2015-07-02 09:09:05
--------------------------
218250
3420
Mình chọn mua cuốn sách này từ khi đọc được trích đoạn giải thích khá chi tiết về việc các trò chơi đóng vai trò quan trọng với bé thế nào ở đầu sách. Kiến thức không xa lạ gì, nhưng không phải bà mẹ nào cũng được tiếp cận. Và mình tự dưng nghĩ một điều, nếu các bà mẹ đều hiểu được "chơi" là cần thiết và có ích thế nào với bé, sẽ không ai mắng con là nghịch ngợm vớ vẩn hay phá phách nữa. Những trò chơi trong sách, hầu hết mình đều biết rồi. Nhưng mình vẫn cho rằng thật đáng công khi mua cuốn sách này, bởi vì nó quá đẹp và truyền cảm hứng: để về nhà lục lọi làm đồ chơi cho con, để bày trò chơi cùng con mỗi ngày. Một cuốn sách đẹp đến mức người ta cứ phải đọc dù đã biết rồi, đẹp đến mức thôi thúc người ta phải làm những thứ đẹp đẽ thú vị tương tự như thế, chẳng phải tuyệt lắm sao.^^.
5
82774
2015-06-30 14:53:07
--------------------------
218154
3420
Sách được in ấn sắc nét, màu sắc tươi sáng. Mình đồng quan điểm với 1 mẹ là phần lý thuyết hơi dài, nhưng với mình đó là vì trước đó mình đã đọc những lý thuyết về vận đọng ở những chỗ khác rồi. Còn về một cấu trúc, nội dung sách thì cách giải thích lý thuyết ở sách này là rất hữu ích, tổng hợp và dễ hiểu. Phần thực hành phân tích rõ ràng các lợi ích của trò chơi. Ngoài ra có thêm cả phần chú thích cho mẹ sau mỗi lần thực hiện khá hay. Việc này giúp cho mẹ một phần lưu lại những kỉ niệm cho con, hay là lần sau thực hiện lại trò chơi này sẽ có những trải nghiệm mới thú vị hơn.
3
471616
2015-06-30 14:15:43
--------------------------
215266
3420
Giờ chơi đến rồi tập hợp nhiều kinh nghiệm nuôi con của tác giả. cầm quyển sách một điều làm tôi không hài lòng một tí là tác giả đề cập nhiều vào phần lý thuyết trong khi điều tôi mong đợi là thật nhiều hơn nữa những trò chơi. Tuy nhiên thì phần các trò chơi tác giả hướng dẫn rất dễ thực hiện, kích thích sự khám phá, trí tưởng tượng, làm giàu thêm cảm xúc, mối quan hệ mẹ con. và theo tôi sách này rất cần cho những bà mẹ muốn giành thời gian chơi với con, trước đây tôi luôn giành thời gian cho con, luôn nghĩ ra trò cho bé vui chơi nhưng cũng cạn kiệt không suy nghĩ ra được, sách này đã bổ sung giúp tôi trong việc đó.
3
667019
2015-06-26 08:56:13
--------------------------
212295
3420
Đây là cuốn sách dành cho các bà mẹ "bí" trò chơi cho con yêu của mình. Chạm vào sách sẽ hiểu được một thế giới thú vị thế nào mà không có Ipad, tivi hay điện thoại. Các trò chơi bổ ích dần hiện ra, từ đơn giản như tờ giấy A4 với cây bút chì, cái chén, cái ly, vài viên đá... đến phức tạp như cắt giấy thủ công, tô vẽ... Con của bạn sẽ từ những giờ chơi mà lớn lên, phát huy trí tưởng tượng, yêu thích trò chơi dân gian, say mê, hứng khởi. Cả nhà vì thế cũng tràn ngập tiếng cười. Thật bổ ích cho phụ huynh và cả những đứa trẻ, tất nhiên.
4
477652
2015-06-22 10:35:19
--------------------------
210031
3420
Ban đầu mình cứ phân vân không biết nên mua hay không. Nhưng sau đọc nhận xét của các bạn mình quyết định mua về đọc thử. Mình vừa mới nhận sách nhưng cầm sách lên đã thích ngay rồi. Sách nhỏ vừa, bạn có thể đem đi bất cứ nơi đâu để đọc. Cách trình bày nội dung dễ hiểu, dễ theo dõi và phần hướng dẫn trò chơi cũng rất ngắn gọn, dễ hiểu... Mình sẽ nghiên cứu quyển sách này thật kỹ để kéo các con ra khỏi cái tivi, để kéo các con lại gần hơn với mình. Cám ơn các tác giả đã giúp cho cha mẹ bận rộn thời nay có thêm nguồn tham khảo để gần con hơn.
5
663152
2015-06-18 18:41:32
--------------------------
207916
3420
Mình đã nhận được sách hôm qua và háo hức giở ra xem ngay. Quả k sai với kỳ vọng của mình,cuốn sách này thực sự rất thú vị. Các hình ảnh minh hoạ trong sách được trình bày sinh động, đẹp mắt, nội dung được đầu tư kĩ lưỡng. Có thể nói hai tác giả của cuốn sách thật sự tâm huyết với tác phẩm của mình, các trò chơi được trình bày đều dễ hiểu và dễ làm theo, bên cạnh là phần đánh giá kĩ năng tỉ mỉ chi tiết mà mỗi trò chơi sẽ đem lại cho bé. Với 1 cuốn sách được đầu tư chi tiết cả về hình thức và nội dung với giá k hề đắt thế này thỉ rất xứng đáng trở thành"must- have" trong tủ sách của các bà mẹ, giống như mình
5
657859
2015-06-13 15:52:12
--------------------------
204945
3420
Từ ngày có quyển sách này thì mình có nhiều thời gian hơn để gần gũi con cái! Làm theo các hướng dẫn trong này mình và con cái luôn có các giờ phút thật vui vẻ và thư giãn bên nhau! Không con những trò chơi nhàm chán như trước! Những trò chơi trong sách rất mới mẻ và sáng tạo! Mình rất hài lòng và cảm ơn tác giả rất nhiều! Không chỉ chơi mà qua đó trẻ còn học hỏi được nhiều thứ! 
Thiết kế sách cũng rất đáng yêu và sáng tạo không kém nữa chứ! Màu sắc rất bắt mắt và chất lượng giấy rất tốt! Hình toàn toàn ưng ý với sản phẩm này!
5
360586
2015-06-05 12:18:54
--------------------------
204841
3420
Được biết đến cuốn sách qua 1 người bạn và  qua panpage giờ chơi đến rồi.Cảm thấy thật may mắn Vì vuốn sách quá bổ ích. Cảm ơn tác giả của cuốn sách nhiêù lắm.
Cuốn sách có màu sắc đẹp, dễ nhìn, dễ hình dung. Đặc biệt ở cuốn sách có điểm khiến mình rất hứng thú đó là nhưng e bé được chụp hình trong cuốn sách là những bé Việt Nam khiến mình cảm thấy cuốn sách viết ra là để cho bé nhà mình. Không như những cuốn sáh nước ngoài mình vẫn mua về. 
Một lần nữa cảm ơn 2 tác giả rất nhiều.
5
651427
2015-06-05 07:30:13
--------------------------
204292
3420
Tôi rất thích nội dung cuốn sách này, vì nó có những trò chơi rất hay và bổ ích cho con trẻ. Khi chúng ta "chơi" cùng con, chúng ta có thể hiểu con mình hơn, kích thích cho con phát triển trí não, thông minh và sự phản xạ nhanh nhẹn... chúng ta cũng sẽ có cảm giác như được sống lại thời ấu thơ của mình với những trò chơi thú vị cùng con!!! 
Đặc biệt 2 tác giả của cuốn sách này rất xinh đẹp, tài năng và năng động và tôi rất ngưỡng mộ 2 người này!
5
651430
2015-06-03 15:43:41
--------------------------
203893
3420
Mình được tặng cuốn sách này hôm qua, dành cả tối để xem thực sự thấy sách đẹp và nhiều ưu điểm quá:
*Sách in màu trên chất liệu giấy đẹp nên khi cầm trên tay rất thích, và là sách ảnh có hình các trò chơi với các bạn nhỏ chơi nên cảm giác rất gần gũi
*Hướng dẫn đơn giản, dễ hiểu có thể làm được
*Một điều mình thích nữa là có phần đánh giá trò chơi theo từng tiêu chí trí thông minh khá thú vị.

Nói chung, rất hài lòng với sách, hôm nay mình đã đi mua thêm vài quyển để tặng bạn bè và bạn bè mình cũng rất thích.
Mong các tác giả sớm ra sách khác :) 
Cám ơn Tiki đã giới thiệu sách này cho bố mẹ nhé.
5
5210
2015-06-02 16:19:51
--------------------------
203768
3420
Mình biết đến cuốn Giờ chơi đến rồi qua fanpage Giờ chơi đến rồi trên facebook. Bản thân mình thấy sách được thiết kế đẹp, in màu, nhiều hình ảnh và hướng dẫn rất cụ thể về các trò chơi với con theo độ tuổi và trò chơi đó giúp con phát triển những kỹ năng gì. Chương 2 của sách đưa ra các trò chơi cho bé dưới 1 tuổi, từ 1 đến 2 tuổi, từ 2 đến 3 tuổi và trên 3 tuổi. Cùng với cuốn sách này thì ba mẹ tha hồ có những khoảng thời gian hữu ích cùng chơi với con, vừa chơi mà vừa dạy con về nhiều điều xung quanh.
3
15022
2015-06-02 11:13:52
--------------------------
203204
3420
Dù đã có 1 quyển nhưng mình vẫn mua thêm để tặng bạn bè, vì sách rất hay và thú vị. Sách hướng dẫn nhiều trò chơi và cách ba mẹ tồ chức trò chơi với con để con không cảm thấy chán mà dừng chơi nửa chừng. Sách được in màu, dễ dàng cho ba mẹ hình dung và tra cứu các trò chơi. Các ông bố bà mẹ nào đang bí trò chơi với con thì có thể mua quyển này và quyển "Mỗi ngày 1 trò chơi" để tạo nên những phút giây giải trí thật sự chất lượng bên con trẻ sau 1 ngày làm việc căng thẳng.
5
36651
2015-05-31 15:57:51
--------------------------
