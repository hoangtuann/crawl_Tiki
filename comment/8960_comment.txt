296085
8960
-Mình mua quyển này chung với quyển Tô Màu Công Chúa Cổ Tích - Tập 2 để tặng em mình, phải nói là chất lượng so với giá tiền rất hợp lí. Khổ lớn, bìa bóng tươi sáng hơn cả ảnh mẫu, số trang phù hợp, hình ảnh được in rất nét, rõ ràng, nền hoa văn rất đẹp, dễ dàng cho các bé sáng tạo màu sắc cho riêng mình. Tăng tính thích thú cho bé. Tuy nhiên, điểm trừ duy nhất cho sản phẩm này là giấy hơi mỏng chỉ thích hợp dùng chì màu khô hoặc sáp dầu để tô. Hi vọng nhà sản xuất lưu ý hơn về phần này và khắc phục được nó.Tks :)))))
5
701204
2015-09-10 21:22:56
--------------------------
