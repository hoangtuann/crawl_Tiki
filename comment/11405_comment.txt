490097
11405
Iliad cùng với Odysses là hai bộ sử thi lớn của Homer nói riêng và Thần thoại Hy Lạp nói chung, nếu bạn nào có hứng thú với nền văn mình Hy Lạp, mình nghĩ là không nên bỏ qua hai cuốn sách này, hai cuốn sách này cũng phần nào ảnh hưởng đến tư duy của người Hy Lạp cổ đại, nếu bạn nào từng đọc cuốn Cộng Hòa và Ngày cuối trong đời Socrates, hẳn các bạn sẽ nhận ra là Plato có  nhắc tới một số quan niệm sống mà người đời thường rút ra được từ hai tác phẩm này, quả thực là tác phẩm này vô cùng công phu, trình bày đẹp, nhưng về mặt dịch thuật thì mình cảm thấy chưa hài lòng lắm, bác Hoan sử dụng văn phong khó hiểu , ngôn từ chưa thuần Việt, thiếu các dấu phẩy để ngắt câu và các từ nối. Nếu bạn nào muốn đọc hai tác phẩm này, các bạn nên chọn một nơi nào đó yên tĩnh ngồi đọc, đọc sâu chứ đừng đọc nhanh, thì mới có thể cảm nhận được nhưng triết lý sâu xa trong hai tác phẩm này, hai tác phẩm này không giống như một tác phẩm văn học hay tiểu thuyết giật gân thời nay đâu. 
3
366877
2016-11-11 05:26:03
--------------------------
472352
11405
Với những ai yêu mến những thần thoại của Hy Lạp và lịch sử văn minh của khu vực này không thể bỏ qua những tác phẩm như thế này, cùng với Odisey, Iliad là một trong những tác phẩm được xếp vào tủ sách kinh điển. Với cái nhìn của tôi khi tìm hiểu về lịch sử văn minh thế giới nó cung cấp cho tôi những hiểu biết về một thời đại văn minh của khu vực này, những con người với những tính cách của một vị anh hùng, những tình tiết lý thú và hồi hộp trong tác phẩm này chắc có lẽ đã xuất hiện nhiều trên màn ảnh. 
Cám ơn tiki nhiều vì đợt giảm giá mà tôi có thể mang về cho mình trọn bộ của Hommer.
5
816829
2016-07-09 15:00:05
--------------------------
382022
11405
Quyển sách khá tuyệt nhưng đáng tiếc dịch giả lại sử dụng quá nhiều từ Hán-Việt xem ra không hợp với văn phong Tây phương cho lắm. Đôi khi nhiều chỗ lại viết dư ra như "xạ thủ từ xa", "thần linh chiến tranh"... không đồng nhất trong việc sử dụng một cách gọi danh từ các tiếng gốc Hy Lạp, tiếng Anh, vd như troia-troa-troy-trojan, Hector-Hektor mặc dù người đọc vẫn ngầm hiểu tuy hơi bị bấn loạn. Thay vào đó nên để các danh từ này vào phần phụ chú sẽ hợp hơn.Nếu có tới được tai dịch giả hy vọng sẽ tiếp nhận ý kiến đóng góp. Ngoài ra sách khá dài, do mình còn nhỏ tuổi đọc vẫn chưa "ngấm" cho lắm.
4
721169
2016-02-18 14:32:24
--------------------------
365760
11405
Mình được nghe cô giáo chủ nhiệm kể rất nhiều về cuốn sách này , mình tìm mua rất nhiều nơi đều không có nhưng thật may đã tìm thấy trên tiki mà giá lại rẻ hơn. Cuốn sách này thật sự rất hay và bổ ích . Các câu chuyện được lồng ghép vào những chi tiết kì ảo một cách hoàn hảo , một khi đã đọc thì không thể rời mắt. Cuốn sách được thiết kế bìa rất đẹp , giấy cũng là giấy dày . Mình thực sự rất thích cuốn sách này .
Còn về phần đóng gói vận chuyển thì tiki làm rất cẩn thận , sách được xếp gọn gàng trong thùng giấy nên không lo bị cong mép . Mình rất hài lòng khi mua hàng tại tiki . Cảm ơn tiki . Mình nhất định sẽ ủng hộ tiki lâu dài .
4
598652
2016-01-08 18:05:11
--------------------------
358038
11405
Iliad - Đây là một thiên niên sử cổ đại của người Hi Lạp. Nhà thơ mù Homer đã sáng tạo nên tác phẩm bất diệt này. Tác phạm kể lại cuộc chiến vĩ đại trong thời kỳ cổ đại ở Hi Lạp. Hình ảnh những người anh hùng được miêu tả khá đẹp : Achilleus dũng mãnh, odysees thông minh, Menelaos anh hùng, ...  Hình ảnh những vị thần cổ đại Hi Lạp được miêu tả khá sinh động khoa cũng giống con người biết tức giận, tham lam, ích kỷ... Tuy nhiên tác phẩm này chỉ kể lại một phần của trận chiến phải chi kể lại toàn bộ trận chiến thì hay hơn nhiều nhỉ (^_^)
4
899370
2015-12-24 17:14:55
--------------------------
325116
11405
Thật ra thì tôi cũng đã đọc Thần thoại Hy Lạp rất nhiều lần rồi nên cũng biết rõ về cuộc chiến thành Troy và diễn biến của nó. Tuy nhiên, vì muốn tìm hiểu sâu hơn nữa, vả lại Iliad cũng là một trường ca bất hủ của Homes nên muốn tìm đọc, ai ngờ khó hiểu quá mức. Cách dịch của người dịch thực sự rất khó thấm. Tôi cảm thấy như người dịch chỉ cứng nhắc dịch theo công thức chứ không linh hoạt biến nó ra thành ngôn ngữ gần gũi để bạn đọc dễ hình dung. Ví dụ lần đầu tiên đọc câu "Achilles bước chân thoăn thoắt nói..." tôi cứ nghĩ là ông Achilles ông đứng lên ông bước đi nhanh ơi là nhanh chứ, ai ngờ "bước chân thoăn thoắt" là đặc tính của ổng. Mà trong tác phẩm không thiếu những câu như vậy, đọc khá là nản. Tôi đọc dở chừng khó hiểu quá nên chuyển qua Odysseus đọc luôn (mặc dù theo thứ tự thì Odysseus xếp sau Iliad). 
Tôi nghĩ rằng ai mà chưa biết về cuộc chiến thành Troy mà mò Iliad đọc luôn chắc chả hiểu gì hết mất.
3
220900
2015-10-22 20:47:52
--------------------------
310962
11405
Mua cuốn sách này chưa bao giờ là sai lầm của mình. Chưa tính đến nội dung, Iliad bản này đã gây một ấn tượng rất mạnh mẽ: hình thức đẹp tuyệt vời: bìa mang màu cổ kính, bọc hai lớp rất chắc chắn. Mình thật sự rất thích loại giấy in này, màu sắc rất hài hoà. Về nội dung, không còn gì có thể bàn cãi: đây là áng văn vô tiền khoán hậu của Homer, là đỉnh cao, là niềm tự hào của La Mã, của châu Âu. Cuộc chiến thành Troa được thuật lại trong cuốn sách qua thể loại văn vần mang hơi thở sử thi cổ kính. 
5
366416
2015-09-19 20:46:20
--------------------------
296257
11405
Tôi đã biết và mong muốn có được "bộ đôi" sử thi nổi tiếng này (cùng với Odyssêy) của Homer từ đã lâu, và đã vô cùng sung sướng khi tìm được hai tác phẩm này trên tiki. Trước đây, tôi cũng đã từng mua một cuốn Iliat - nhưng đó là "phiên bản rút gọn" chỉ vẻn vẹn 152 trang với giá bán chỉ có 9000đ. 
Iliad - tôi đang cầm trên tay đây - thực sự là thiên trường ca bất hủ, là bài ca chiến trận về những người anh hùng  Hi Lạp, La Mã thời cổ đại. Đọc tác phẩm chúng ta sẽ cảm thấy rợn ngợp với không khí hào hùng của cuộc chiến thành Troy (Troia), chúng ta sẽ bắt gặp các nhân vật trong thần thoại Hi Lạp, những chiến binh anh hùng Achilles, Hector, Odysse,...- những con người quyết chiến đấu đến cùng vì danh dự cộng đồng...
Cuốn sách được in dày dặn, chất liệu giấy tốt, bìa đẹp. Với tôi, "bộ đôi" Iliad và Odyssey là là bảo thư không thể thiếu trên giá sách.
5
334669
2015-09-10 22:48:27
--------------------------
259770
11405
Sau Odysse thì Iliad là tác phẩm thứ hai của Homer mà tôi mua. Thiên trường ca này thậm chí còn hoành tráng hơn cả Odysse. Cuộc chiến thành Troy tôi đã được xem quá nhiều trong truyện kể thần thoại, trong phim ảnh, kịch nói nhưng đọc nguyên tác cảm giác đúng là khác hẳn. Lãng mạn, bi tráng, anh hùng, đau thương, yêu, hận, danh dự, vĩ đại.... tất cả tập hợp trong thiên trường ca tuyệt vời này có thể làm bất cứ ai mê đắm. Dù giá tiền của nó không phải là rẻ nhưng nội dung bên trong đích thực là vô giá. Ngay cả bức tranh bìa cũng đẹp tuyệt vời!
5
167283
2015-08-09 20:09:33
--------------------------
251327
11405
Đây là cuốn sách mình muốn mua từ hồi học lớp 10 cơ .Sau khi nghe cô giáo dạy Văn kể chuyện là mình mê luôn từ đó.Mình đã nhiều lần tìm trên mạng nhưng không thấy,thật may là sau đó nhà xuất bản đã tái bản lại cuốn này.Cái bìa thiết kế cũng đẹp,giấy bên trong là loại ngà vàng.Mình thích loại giấy này.Bản dịch khá tốt.Khi đọc bạn sẽ mê sử thi Hy Lạp luôn cho mà xem: kịch tích, hấp dẫn, bất ngờ , huyền bí.Thông qua những sử thi này, người Hy Lạp cũng lồng vào đó những đức tính tốt của vị anh hùng
5
187271
2015-08-02 17:37:10
--------------------------
240872
11405
Đối với những tác phẩm dịch, đặc biệt là những tác phẩm mang cả hơi hướm lịch sử lẫn văn hóa, việc dịch thuật như thế nào khiến cho người đọc cảm thấy được hào khí, được hơi thở, được sống lại trong hoàn cảnh lịch sử là rất quan trọng. Nếu tác phẩm được giữ nguyên những cái tên như trong bản gốc tiếng Hy Lạp của bộ sử thi, như là Aias thay vì Ajax, hoặc Akhilleus thay vì Achilles trong Anh Ngữ, tôi sẽ cảm thấy hài lòng hơn. Tuy nhiên, bản dịch sang tiếng Việt phần nào cũng khỏa lấp được đam mê tìm tòi học hỏi của phần đông khán giả Việt Nam.
4
137953
2015-07-24 23:25:24
--------------------------
185145
11405
Một cuốn sử thi kinh điển và quá nổi tiếng của Homer, tác phẩm không kể lại toàn bộ cuộc chiến thành Troy mà chỉ nói đến giai đoạn gần cuối cuộc chiến từ lúc Achilles tức giận bỏ đi cho đến lúc vua Priam đến đòi xác của Hector. Tác phẩm là đỉnh cao của nghệ thuật miêu ta khi mà những cảnh cận chiến được Homer miêu tả cực kỳ chi tiết và sinh động. Bên cạnh đó tác giả cũng kể lại chi tiết tiểu sử gia đình của những vị anh hùng tham gia cuộc chiến, vì vậy có quá nhiều tên Latinh, có thể gây khó khăn khi đọc tác phẩm.. Có một điều mình không hài lòng lắm về cuốn sách này đó là dịch giả dùng nhiều từ Hán Việt, không hợp với văn phong phương Tây chút nào.
4
472173
2015-04-18 23:01:08
--------------------------
159481
11405
Iliad là mộ trong 2 bộ sử thi hay nhất của Homer. Nếu như Odyssey chủ yếu mang tính phiêu lưu và mạo hiểm thì Iliad lại mang hơi thở chiến trận của những trận đánh vô cùng máu lửa. Iliad là câu chuyện về cuộc chiến thành Troy nổi tiếng để đòi lại nàng Helen kiều diễm bị Paris đưa đi, dẫn đầu bởi vua Agamemnon và người anh hùng vĩ đại Achilleus. Cuộc chiến diễn ra hơn mười năm trời, với biết bao nhiêu đau thương mất mát. Cuộc chiến kết thúc với cái chết của người anh hùng Achilleus, và con ngựa thành Troy đã khiến cho bức tường thành sụp đổ. Dù phe Hy Lạp giành lấy chiến thắng, nhưng thiệt hại cho cả hai bên là không thể đo đếm được. Bằng tài năng văn thơ của mình, Homer đã kể lại một câu chuyện sử thi hào hùng nhất thời cổ với những câu chuyện và những điển tích đi vào lịch sử thế giới như gót chân Achilleus, con ngựa thành Troy,... Một kiệt tác của văn học thế giới.
5
291067
2015-02-17 10:15:20
--------------------------
111111
11405
Có lẽ nhắc đến Thần thoại Hy Lạp thì không đâu ngoài 3 tác phẩm: Iliad, Odyssey của Homer và Thần phả của Hesiod. Cả 3 đều là những bộ sử thi lớn thuộc hàng cổ nhất của nhân loại, nếu không đọc được 3 bộ này thì thật phí cả đời ^^! Nhớ hồi cách đây mấy năm cằm cuốn Thần thoại Hy Lạp đọc mà không bỏ xuống được nhưng chưa bao giờ được đọc trọn vẹn bộ Iliad cả. Homer khắc họa lên một người anh hung Achilles vừa ngang tàn, dũng cảm nhưng vẫn chứa đầy tình người. Phải chi dịch được sang thành thơ luôn thì hay quá nhưng mà lúc đó chắc hổng hiểu hết tác phẩm đâu. Dù sao cũng rất thích khi biết bộ này dịch từ bản gốc tiếng Hy Lạp, hihi!
Mình thích loại giấy in cuốn này, hơi ngả vàng vàng nhìn rất thích, hình minh họa cũng khá đẹp, không biết có phải hình từ cuốn gốc tiếng Hy Lạp không nữa, hi! Dịch thuật khá tốt và it mắc lỗi chính tả.
Sách hơi dài nên đọc cũng hơi lâu mới xong nhưng đọc xong rất thỏa mãn, thỏa mãn hơn cả lúc xem phim Troy. Giờ phải để dành cho cuốn Odyssey nữa!
4
61362
2014-04-25 02:07:19
--------------------------
