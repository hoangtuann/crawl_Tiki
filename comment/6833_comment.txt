510745
6833
Sách nội dung càng đọc càng thấy nội dung cũng có nh điều thú vị những chữ viết sai nh quá. Nên xem lại chính tả
4
1478397
2017-01-14 22:04:17
--------------------------
482451
6833
Cuốn sách là câu chuyện của một cậu bé tự kỷ nhưng có trí tuệ thần đồng về môn toán. Đọc cuốn sách cho ta cảm nhận được thế giới qua cách nhìn của những đứa trẻ tự kỷ - một hành trình đầy khác lạ nhưng cũng rất giản đơn. Đây là một cuốn truyện thú vị, nhưng cần sự kiên nhẫn, cũng như ta cần sự kiên nhẫn với những người tự kỷ.

Cả câu chuyện tôi thấy tội nghiệp nhất chính là ông bố. Tình yêu của ông dành cho đứa con là vô tận, từ cách ông kiên nhân chăm sóc cho tới cách ông cố gắng gần gũi con. Nhưng chỉ vì một sai lầm đã khiến ông mất đi niềm tin của đứa trẻ. Như vậy ta mới thấy được tác động của những hành động đến tâm lý trẻ em.

Câu chuyện có lúc hài hước, có lúc kịch tính, có lúc sâu lắng, nhưng đọng lại cuối cùng là tính nhân văn của tác phẩm cũng như thông điệp mà tác giả muốn truyền tải tới bạn đọc.
4
70206
2016-09-06 15:14:15
--------------------------
472510
6833
Có nhiều điều ấn tượng trong cuốn sách này, con số được sử dụng trong tác phẩm này là những con số nguyên tố và đánh cho từng đoạn của tác phẩm. Câu chuyện khá thú vị xoay quanh cái chết của con chó, và câu chuyện đi tìm thủ phạm của chú bé đặc biệt. Cái nhìn của tác giả được thông qua cái nhìn của cậu bé để tiếp cận thế giới nên đọc tác phẩm ta có thể cách nhìn nhận vấn đề nó sẽ khác so với những tác phẩm khác, đôi khi những suy nghĩ ngược và khác của cậu bé lại làm cho chúng ta phải suy nghĩ..
5
816829
2016-07-09 17:50:18
--------------------------
469866
6833
Cuốn sách này và cuốn Hoàng tử bé là hai cuốn sách đã để lại trong tôi ấn tượng sâu sắc nhất, "The Curious Incident of the Dog in the Night-time" là một cuốn sách độc và lạ đến ngạc nhiên. Nó khơi dậy ở tôi nhiều điều mới mẻ mà một trong số đó là cảm hứng dành cho văn chương. Đây cũng đồng thời là một cuốn sách rất thông thái nên có những đoạn tôi phải đọc nhiều lần và thật chậm để thấu và hiểu. Thật sự đây là một cuốn sách không đáng bị do dự khi chọn mua. :)))
5
1061576
2016-07-06 21:56:59
--------------------------
461854
6833
Mua cuốn này cũng được gần 1 tháng rồi mãi mới đọc xong do bận công việc quá ít thời gian đọc.Sách này rất hay viết về 1 cậu bé tự kỉ đi điều tra cái chết của con chó hàng xóm mà hung thủ chính là bố của mình,bị bố nói dối về cái chết của mẹ,nhưng thật ra mẹ cậu ta ly thân với bố cậu ấy,cậu ta vẫn đi điều tra khắp nơi hỏi mọi người xung quanh xóm về cái chết con chó cho dù bị bố cậu ta ngăn cấm.Sách này không hẳn là thể loại trinh thám đâu mọi người ah.
4
1290543
2016-06-28 07:39:26
--------------------------
455304
6833
Mình không quá khắt khe với sách. Với cuốn này mà xếp vào thể loại trinh thám thì mình thấy không đúng. Nó như là tự sự ấy, chỉ là kể lại những mẩu chuyện lung tung mà tác giả sau khi được khuyến khích đã gom lại thành một cuốn sách. 
Mình quyết định mua cuốn sách này vì nó nằm ở mục Trinh thám, và tiêu đề liên quan tới một con chó (mình thích chó mèo lắm), mình đã hy vọng khá nhiều về nó.
Lúc nhận được hàng mình khá bất ngờ vì cuốn sách mỏng gọn như cuốn vở học sinh. Nội dung câu từ hay bị lập lại, có một chút lủng củng mà theo mình nghĩ do người dịch muốn giữ lại nguyên bản văn phong của tác giả (không phải một nhà văn chuyên nghiệp). 
Nói chung thì mình hơi thất vọng về một cuốn sách thuộc mục "Truyện trinh thám" như này. Ai mong muốn ở nó một cuốn sách gay cấn, bí ẩn, âm mưu nham hiểm thì không phải cuốn này. Nó nhẹ nhàng, đơn giản hệt như suy nghĩ của một đứa trẻ đối mặt với những chuyện xảy ra xung quanh, làm người đọc hiểu hơn về suy nghĩ của một đứa trẻ bị tự kỷ.
4
890619
2016-06-22 14:50:36
--------------------------
429424
6833
Nhiều người mua nó chỉ vì thích trinh thám rồi sau đó lại tỏ ra thất vọng vì chi tiết trinh thám nhạt. Nhưng đọc xong để mà nói vậy thì thật không biết cảm nhận. Ngoài việc điều tra về cái chết của Wellington, câu chuyện còn bộc lộ được suy nghĩ và cảm xúc của một cậu bé mắc bệnh tự kỉ. Và mới đọc cũng cảm thấy rất sáng tạo, nhất là đoạn đánh số chương bằng số nguyên tố. Hình ảnh so sánh cũng rất đẹp và lạ. Ngoài ra, cuối truyện còn có phần giải bài toán được đặt ra trong đề kiểm tra trình độ A của Christopher.
Nhiều người ở trường tôi cứ coi khinh những ai bị tự kỉ. Hễ thấy ai lẩm bẩm một mình là chọc ghẹo ngay. Nhưng tự kỉ là một căn bệnh không ai ai muốn mắc phải. Đáng nhẽ phải thương họ thay vì cười nhạo họ mới đúng. Chỉ mong khi đọc xong cuốn sách ấy, họ có thể thấu hiểu và đồng cảm với những người đó.
5
975584
2016-05-13 10:16:46
--------------------------
394719
6833
Lúc đầu nhìn cái bìa đã khá ấn tượng và như có 1 cái gì đó rất lạ lạ nên quyết định mua cuốn này, nghe  giới thiệu cũng thấy hấp dẫn rồi, đúng là 1 quyển tiểu thuyết trinh thám khá lạ qua lối suy nghĩ của 1 đứa nhóc mới hơn 15 tuổi
Truyện kể về con chó bị cái bồ cào đâm chết, một thằng nhóc 15t quyết định truy tìm ra kẻ đã giết con chó rồi từ đó xuất hiện rất nhiều nghi vấn xung quanh cái chết của con chó và hé lộ rất nhiều chuyện. Hóa ra người giết chết con chó k ai khác chính là bố nó, và người nói dối mẹ nó đã chết cũng là bố nó, ông ta giấu nhẹm tất cả lá thư mẹ nó gửi cho nó hòng cắt đứt mọi liên lạc. Nó cảm thấy sợ hãi và chỉ muốn sống cùng mẹ nó. Kết thúc rất có hậu và khiến cho người lớn chúng ta phải suy ngẫm lại về những hành động của mình đối với bọn trẻ
Bên cạnh đó còn có rất nhiều bài toán khá thú vị, christopher giỏi toán, vật lý nên cũng truyền cảm hứng cho người đọc về hai bộ môn lí thú này. K những mang tính logic mà còn giúp ta ngộ ra nhiều điều lí thú trong cuộc sống
3
195428
2016-03-10 20:45:17
--------------------------
369596
6833
Lúc đầu đọc cuốn sách mình đã bị ấn tượng và thu hút về cách kể chuyện của nhân vật. Mình thật sự khâm phục tác giả khi đã đi sâu vào thế giới nội tâm của những đứa trẻ đặc biệt và đã viết nên một tác phẩm hết sức sống động và chân thực. Tuy một số phần cảm thấy hơi khó hiểu nhưng đây vẫn là một cuốn sách đáng đọc. Khi đọc xong, mình đã cảm thấy có sự đồng cảm vài phần với những đứa trẻ như vậy, và đôi khi cũng nhìn thấy bản thân mình trong đó. 
Một cuốn sách hay và hấp dẫn.
5
374081
2016-01-16 00:28:54
--------------------------
316240
6833
Lần đầu tiên đọc cuốn này đã cách đây 4 năm rồi, vô tình đọc ké của một đồng nghiệp. Đồng nghiệp mình hôm đó nổi hứng đọc sách thiếu nhi còn có một cuốn là "Chuyện nhỏ trong thế giới lớn". Hôm đó đang buồn chán vì rớt mạng nên cầm lên đọc thử không ngờ rất cuốn hút nên đọc luôn 1 lèo lên xe đi công tác mà vừa đọc vừa cười ^^. Đến bây giờ mới nhớ ra và tìm mua đọc lại. Chỉ còn nhớ mang máng cốt truyện và giọng văn rất vui hóm hỉnh và hấp dẫn cảm giác như nghe Ringo hát Maxwell's Silver Hammer vậy. 
5
722542
2015-09-30 14:56:50
--------------------------
306296
6833
Thoạt đầu khi mới nhìn bìa sách tôi tưởng đây là một cuốn sách trinh thám nhưng khi đọc rồi thì nhận ra mình đã lầm. Cuốn sách được viết ở ngôi thứ nhất cho ta một cái nhìn khác về trẻ tự kỉ, những suy nghĩ của những đứa trẻ đó. Tuy thế giọng văn không hề u tối mà cảm giác rất đáng yêu, như thể người bình thường như chúng ta không chịu hiểu những gì mà những đứa trẻ tự kỉ cố nói. Giọng văn nhẹ nhàng, hóm hỉnh làm nên một tác phẩm đầy ấn tượng. Bạn sẽ không muốn cho ai mượn cuốn sách này đâu.
"Tôi nhìn thấy năm chiếc xe đỏ trên đường nên hôm nay là một ngày siêu tốt!"
5
141642
2015-09-17 14:52:20
--------------------------
281567
6833
Mới đầu khi nhìn tựa đề và bìa cuốn sách thì tưởng đây là sách trinh thám thiếu nhi, nhưng lúc đọc thì mới biết đây là cuốn sách nói về trẻ tự kỷ. Tuy nói về trẻ tự kỷ nhưng giọng văn không hề u ám, tác giả đã hóa thân vào nhân vật một cách tuyệt vời, thấu cảm sâu sắc, làm cho mình hiểu thêm một chút về trẻ tự kỷ: cách suy nghĩ, cách cảm nhận, cách xử sự. Giọng văn nhẹ nhàng, hài hước nhưng ấn tượng. Nói chung đây là một cuốn sách rất đáng đọc.
5
464492
2015-08-28 15:06:26
--------------------------
265660
6833
Hình ảnh về con chó bị đâm đã đưa mình đến đây bởi chính cái tên sách.Tò mò về cốt chuyện: con chó chết ra sao và tại sao lại chết. Bìa sách rất đẹp và giấy cũng vậy.
Bí ẩn  về con chó lúc nửa đêm là một câu chuyện mới lạ, với nội dung hoàn toàn khác, không phải về tình yêu, cũng không phải trinh thám mà về một cậu bé  Christopher được coi là tự kỷ, không bình thường....Đọc tác phẩm cho ta thấu hiểu, đồng cảm hơn về những suy nghĩ, mong muốn của người mắc bệnh tự kỷ, họ cũng có ước mơ, có khát khao về cuộc sống. Đó cũng chính là thông điệp mà tác giả muốn gửi gắm qua câu chuyện. Truyện hay và cảm động
4
309333
2015-08-14 08:30:01
--------------------------
259620
6833
Cuốn sách đã mang đến cho tôi những cung bậc cảm xúc khác nhau, từ bất ngờ cho đến đau đớn tới tột cùng. "Bí ẩn về con chó lúc nửa đêm" như đưa ta vào thế giới của một cậu bé có những suy nghĩ lạ kỳ, nhưng đâu đó sâu bên trong tâm hồn thì cậu bé vẫn rất cần một gia đình, tình thương yêu của ba lẫn mẹ chứ không của riêng một ai! 
Nói riêng về vụ án con chó Wellington, tôi thật sự rất bất ngờ và chắc hẳn Christopher cũng vậy, không thể ngờ hung thủ mà bấy lâu cậu tìm kiếm lại chính là người luôn bên cạnh cậu, chăm sóc, quan tâm cậu - cha cậu. Và khi đã tìm ra hung thủ thì lần lượt các bí ẩn đau buồn khác từ từ được mở ra mà chỉ ai đã từng đọc qua cuốn sách này mới hiểu được.
4
17474
2015-08-09 17:41:15
--------------------------
259156
6833
Trước hết tôi muốn nói đây không phải là truyện trinh thám, nên ai mê trinh thám thì đừng nhìn lầm mà mua cuốn này vì tưởng nó là trinh thám nhé. Còn thì, đây là một cuốn sách khá thú vị nói về cậu bé mắc chứng tự kỷ Christopher, kể ở ngôi 1 dưới lời cậu bé. Bằng cách đặt truyện kể ở ngôi 1, tác giả giúp ta có thể trải nghiệm cách nhìn của một đứa trẻ tự kỷ về thế giới xung quanh, người đọc có thể thấy rõ ràng là không hề đơn giản chút nào mà cậu bé lại còn có những suy nghĩ khiến ta cảm thấy nể. Thêm vào đó, ta cũng có thể rút ra những bài học từ truyện, như Christopher, cậu luôn thu mình với những gì mà bản thân cậu biết nhưng cuối cùng đã dám bước ra thế giới, ra khỏi vùng an toàn của cậu, cùng với tình yêu thương và sự giúp đỡ của cha mẹ, cuối cùng hình thành nên trong cậu suy nghĩ và cũng là lời cuối cùng của cuốn sách: "tôi có thể làm được bất cứ việc gì".
5
740662
2015-08-09 09:57:00
--------------------------
258979
6833
Mình mua quyển sách này bởi mình khá tò mò với bìa truyện và phần mở đầu, mình cũng không phải fan của thể loại này nên có lẽ mình không thể cảm nhận hết quyển sách này.Truyện có lối văn khá rối rắm khiến mình không thể hiểu được cũng như nắm bắt được mạch của câu chuyện, các tình tiết trinh thám thì hầu như không có gì là nổi bật. Theo mình đây vẫn là tác phẩm ẩn chứ nhiều tính nhân văn bởi tác giả đã khai thác được vấn đề tự kỉ thông qua cậu bé Christopher giúp ta hiểu hơn về thế giới của cậu cũng như với những người mắc chứng tự kỉ.
3
536908
2015-08-08 23:13:36
--------------------------
255414
6833
Tự Kỷ - Autism là gì vậy ... trong giới thiệu về sách hay nhan đề của sách không hề đề cập tới vì sao tôi lại nói về từ này trong nhận xét của mình. 
Vì tôi muốn nói cám ơn tác giả Mark Haddon và dịch giả Phạm Văn đã mang tới độc giả 1 cuốn sách này. Cùng với bộ phim Temple Grandin, đây là 2 tác phẩm mà tôi mong muốn mọi người biết đến.
Xét về mặt cấu trúc truyện, tuy là 1 người khá khó tính về kết cấu truyện và đọc tài liệu về trẻ tự kỷ nhưng tôi vẫn bị lối dẫn truyện cuốn hút, giọng văn rất *đúng* là phong cách của Christopher - cậu bé Tự Kỷ trong câu chuyện  có thể sẽ khiến người đọc cảm thấy lạ lẫm nhưng lại là cách rất tốt để người đọc hiểu được suy nghĩ và cách nhìn cuộc sống của Christopher như thế nào.
Đối với một người luôn mong muốn mọi người hiểu đúng về tự kỷ, chắc chắn tôi sẽ trích dẫn lại rất nhiều đoạn văn của *Bí ẩn về con chó lúc nửa đêm*, 
5
60707
2015-08-06 00:38:33
--------------------------
245161
6833
Nếu bạn mua quyển sách này vì bạn ham thích thể loại trinh thám thì tôi nghĩ bạn không nên mua nó. Tính trinh thám trong truyện rất ít, và mình cũng không biết quyển này thuộc danh mục gì nữa. Đối với mình, quyển này hơi nhàm chán và khó đọc. Và mình cũng không tìm ra vẻ hài hước của nó ở đâu cả, có lẽ do không có tình cảm với quyển này từ những chương đâu tiên. Truyện ngắn nên mình chỉ mất 2 ngày để đọc xong, nhưng khi đọc xong nó chẳng để cho mình ấn tượng gì cả, vì thế bạn nào muốn mua quyển này thì phải xem những chương đầu tiên trước nhé.
3
616945
2015-07-28 19:20:53
--------------------------
244842
6833
Cách dẫn dắt câu chuyện hay và sinh động và thật.. khác lạ. Dường như không thể đoán được tiếp theo sẽ có những chuyển biến gì. Đôi lúc làm người đọc mỉm cười thích thú, đôi lúc lại gây xúc động đến không ngờ. 
Một tác phẩm đáng để đọc, khi mà nhiều người còn chưa hiểu rõ được về tính cách, cách suy nghĩ và hành động của những cô bé, cậu bé như vậy.
Một cuốn sách đáng để gởi tặng và khuyến khích nhiều người cùng đọc và tìm hiểu một thế giới khác xung quanh ta. :) 
5
200028
2015-07-28 15:14:26
--------------------------
241399
6833
Nếu bạn muốn mua quyển này vì nó xếp vào hạng mục trinh thám thì cõ lẽ bạn sẽ phải thất vọng. Chất trinh thám rất ít, gần như không có. Truyện chỉ mượn vụ án con chó bị giết để lột tả cuộc sống của Christiopher, một cậu bé bị tự kỉ, đam mê khoa học và ngại tiếp xúc với người khác. Toàn bộ câu chuyện kể về các hoạt động thường nhật của cậu bé và công cuộc điều tra vụ án. Truyện hơi khó đọc do văn phong được viết bởi một cậu bé tự kỷ, do đó nếu muốn thấm được tác phẩm này cần phải đặt mình vào vị trí của cậu bé. Lời khuyên của mình là các bạn hãy đọc thử vài chương, nếu thấy hợp hãy mua.
3
582958
2015-07-25 13:58:30
--------------------------
237872
6833
Truyện đưa chúng ta vào cuộc hành trình đầy thú vị của cậu bé Christopher để tìm ra người đã giết chú chó Wellington và yếu tố trinh thám nhẹ nhàng đó khiến truyện trở nên hấp dẫn và hồi hộp hơn. Truyện là thế giới qua góc nhìn của Christopher - một cậu bé cực kỳ thông minh nhưng lại mắc bệnh tự kỷ - đó là thế giới có một sự logic riêng, đầy màu sắc và phức tạp hơn chúng ta nghĩ rất nhiều. Cốt truyện mới lạ và rất hay, đoạn kết lại khá bất ngờ, tuy nhiên vì viết theo cách nhìn nhận và những suy nghĩ của Christopher nên từ "và" được sử dụng rất nhiều, lại có những phương trình, bài toán giải thích khá rắc rối, khiến truyện hơi dài dòng và đôi lúc nhàm chán. Nhưng bù lại truyện mang đến những thông điệp vô cùng ý nghĩa về tình người, về sự hòa hợp với thiên nhiên, cuộc sống và đặc biệt là sự đồng cảm, thấu hiểu sâu sắc của tác giả đối với những người mắc bệnh tự kỷ, vì họ cũng ước mơ, hoài bão và tình yêu như bất kỳ một con người bình thường nào. 
4
387532
2015-07-22 20:29:14
--------------------------
228128
6833
Mình vừa mua quyển này để tặng cho đứa em nhân sinh nhật của nó, và thật sự hối tiếc... vì đã không mua hai cuốn.
Quả thật đó rất hay, không hoàn toàn là 1 câu chuyện trinh thám nhưng có những tình tiết bất ngờ, đôi khi hồi hộp, đôi khi lại thấy nhẹ nhàng. Giọng văn thì mình thấy chân thật, dễ tiếp thu cốt truyện, ko buồn ngủ một số sách khác mình đọc.  Mình thực sự cảm động và đồng cảm, có khi hòa nhập với suy nghĩ của cậu Christopher.

Nhưng mà ở phần bức thư mà mẹ gửi cho Christopher, có nhiều từ sai chính tả lắm. Mình tự hỏi là do trong nguyên tác bản Tiếng Anh người mẹ viết sai chính tả hay do lỗi của người dịch, chứ có nhiều lỗi rất ngớ ngẫn...

Mình khuyên nếu các bạn muốn tìm 1 quyển sách hay để đọc trong kỳ nghỉ hè này thì đây là lựa chọn hàng đầu :)
4
563134
2015-07-15 00:18:50
--------------------------
225489
6833

"Bí ẩn về con chó lúc nửa đêm " là một câu chuyện đặc biệt. Vì nhân vật chính hay người kể chuyện là một cậu bé 15 tuổi rất thông minh, thích toán học và vật lý, yêu động vật nhưng sợ chỗ đông người và muốn ở một mình trong không gian chật hẹp. Vâng, Christopher là một cậu bé tự kỷ, bị chứng Khủng hoảng hành vi. 
    Bắt đầu bằng cái chết của một con chó và việc muốn điều tra ra hung thủ của Christopher nhưng cả quyển sách đã bộc lộ thế giới xung quanh qua cách nhìn của người tự kỉ. Hiếm thấy những miêu tả khuôn mặt hay cảm xúc của các nhân vật mà thay vào đó là những dòng viết về quần áo, giày dép hay cảnh vật và mùi hương vô cùng chi tiết kỹ càng. Truyện xen kẽ giữa hiện tại và quá khứ, là các sự việc ở trường, ở nhà hay đôi khi chỉ là lan man về một định luật, giả thuyết nào đó. Mình thích phần nói về chuyến đi đến nhà Mẹ ở London của "tôi". Viết rất thật và sâu sắc, một cậu bé đã dũng cảm và quyết vượt qua sợ hãi và ồn ào. Nhưng cảm động nhất là khi kể về Cha. Cha là người chăm sóc, quan tâm tới mọi thứ, biết tất cả mọi thói quen trong cuộc sống của Christopher, là người thường giơ bàn tay phải, xòe các ngón tay muốn cậu chạm vào, là người luôn kiên nhẫn với cậu dù có một lần đã nổi nóng và hai cha con lao vào đánh nhau. Nhưng Cha cũng luôn là người xin lỗi và nói : “Christopher này, con có hiểu là Cha yêu con không?”. Và đặc biệt ở đoạn cuối sách, mình đã bật khóc. "Cha không hiểu con ra sao, nhưng thế này...
thế này đau đớn lắm. Con ở trong nhà nhưng không chịu nói chuyện với cha... Con phải tập tin cha... Và cha không cần biết bao lâu... Nếu ngày đầu một phút, hôm sau hai phút, sau nữa thì ba phút và cứ thế hết năm này qua năm khác thì cũng không sao cả." Cha không hoàn hảo nhưng Cha quá tuyệt vời và yêu thương. 
   Tác giả hẳn phải là rất hiểu và gắn bó với người tự kỷ. Như thế giới của Christopher không bộc lộ nhiều cảm xúc, thích khép kín, có các quy tắc riêng nhưng luôn tràn ngập ước mơ và nhiều suy nghĩ đặc biệt. Và có biết bao người như Cha, như Siobhan, như bà hàng xóm Alexander và cả như Mẹ đã, đang và cố gắng để làm quen và giúp đỡ, yêu thương những người tự kỷ. Một quyển sách vô cùng đáng đọc và suy ngẫm. Hãy làm nó xuất hiện trong tủ sách của chúng ta nhé! 

5
271828
2015-07-10 15:02:38
--------------------------
203259
6833
Cuốn sách này được viết ra từ sự thấu hiểu sâu sắc của Mark Hadon về những người mắc bệnh tự kỉ. Từ suy nghĩ đến hành động của họ đều  được viết một cách chân thực nhất.  Câu chuyện bắt đầu từ một tình tiết đơn giản: một con chó nhà hàng xóm bị chết, một cậu bé mắc bệnh tự kỉ lại xuất hiện tại đó và bị nghi ngờ là giết chết con chó. Câu chuyện ngỡ tưởng chỉ là hành trình đi tìm kẻ giết hại con chó. Nhưng không ngờ nó lại mở ra nhiều tình tiết bất ngờ và mang tính sâu sa như vậy. TRuyện kết thúc rất bất ngờ, nên đọc!
5
560820
2015-05-31 19:16:26
--------------------------
175234
6833
Tôi có quen một vài người có người thân mắc chứng tự kỉ. Qua những câu chuyện của họ, tôi hiểu rằng bệnh nhân tự kỉ cần sự hỗ trợ, cảm thông và nhẫn nại rất lớn từ gia đình và xã hội. Họ cần ít nhất một người thân thiết mà họ tin tưởng ở bên cạnh chăm sóc mình cả đời.
Cuốn sách này không chỉ viết về một cậu bé mắc chứng tự kỉ, mà còn kể cho người đọc câu chuyện của cả những người thân ở bên cạnh cậu bé. Đó là những thầy cô giáo tận tâm và thấu hiểu ở trường dành cho trẻ bị rối loạn hành vi, đã luôn ở bên dạy dỗ, tạo điều kiện cho cậu học hành và tiến tới ước mơ của mình (trở thành một nhà toán học xuất sắc). Đó là người hàng xóm già thân thiện, không hề có cái nhìn kì thị về cậu - một đứa trẻ khó khăn trong giao tiếp. 
Đó là người mẹ ích kỉ, yếu đuối và có phần thiếu trách nhiệm, người đã gián tiếp gây nên rắc rối lớn nhất trong câu chuyện và trong cuộc đời cậu bé. Đó là người cha yêu thương và vô cúng nhẫn nại của cậu, dù rằng khi nổi nóng lên thì ông rất đáng sợ, và ông cũng phạm phải vài sai lầm khi chăm sóc và nuôi nấng đứa con bị tự kỉ của mình, nhưng đọc đến những trang cuối cùng, ông chính là nguyên nhân khiến tôi phải khóc, bởi một đứa trẻ mắc chứng tự kỉ như cậu bé lại cực kì may mắn khi có một người cha yêu thương cậu đến vậy.
5
68751
2015-03-29 19:01:37
--------------------------
145985
6833
Mình nghĩ tiki xếp sách vào thể loại "Truyện trinh thám" là không chính xác, đúng hơn là không đầy đủ. Mượn câu chuyện của một chú chó bị giết trong đêm, và cuộc hành trình giải mã bí ẩn đó, cuốn sách cho người đọc một góc nhìn mới mẻ qua lăng kính hoàn toàn mới. Cuốn sách vì thế khá lôi cuốn, mình đọc chỉ trong 1 buổi là hết ^^. Một vài đoạn gợi cho mình nhớ cuốn "Nhật ký hoàn toàn có thật của một người Anh điêng bán thời gian", vì cách kể chuyện thông qua một nhân vật thiếu niên đặc biệt.
Câu chuyện có lúc hài hước, lúc sâu lắng, giản dị, nhưng đọng lại cuối cùng vẫn là tính nhân văn của nó.
4
20604
2015-01-02 09:15:22
--------------------------
143949
6833
Cuốn sách đặc biệt, cốt truyện khá lạ, đáng đọc!
Câu chuyện được kể dưới đôi mắt của một cậu bé mắc chứng tự kỷ nhưng lại sở hữu bộ não cực kỳ thông minh. Với khả năng quan sát hơn người và tâm hồn trong sáng, cậu bé đưa ra những kiến giải đặc biệt thú vị về những điều bình dị nhất diễn ra xung quanh mình, chính những nhận xét đó khiến người đọc đi từ ngạc nhiên này đến ngạc nhiên khác.
Trước khi mua cuốn sách này tại Tiki, tôi đã được một người bạn giới thiệu và cho mượn về đọc. Đọc được năm trang đầu, tôi liền muốn giữ lại cho mình để thưởng thức và chiêm nghiệm về cuộc sống bằng một góc nhìn đặc biệt mà tôi chưa từng nghĩ tới, và thế là tôi đã quyết định đi mua cuốn mới để trả lại chon người bạn kia.
Cuốn sách giúp tôi trở nên nhẫn nại hơn khi đối xử với người khác, lắng nghe và đồng cảm nhiều hơn là chỉ trích và xét đoán.
Vì là người khá cầu toàn nên chỉ có một điểm rất nhỏ chưa vừa ý là sách còn vài lỗi chính tả; bìa sách chưa thật sự thu hút ánh nhìn độc giả (như tôi)...Hy vọng lần tái bản sau sẽ thật hoàn thiện.
4
520325
2014-12-24 22:50:52
--------------------------
143098
6833
Truyện với lối hành văn khá thú vị khi kể về hành trình tìm ra thủ phạm giết con chó nhà hàng xóm. Đọc truyện ta thấy rằng cậu bé Christopher này có vẻ hơi khác với những đứa trẻ bình thường về lối suy nghĩ, có thể là do hoàn cảnh gia đình chăng? Nhưng những tư duy của cậu bé khiến người đọc cảm thấy thích thú và cách miêu tả còn khiến người đọc cảm thấy lôi cuốn vào mạch tư duy của cậu. Cần phải khẳng định rằng cậu bé này có đầu óc "không bình thường". Trong hành trình khám phá ra thủ phạm, chính cậu cũng lại khám phá ra được bí mật của gia đình mình.
Bằng lối viết hóm hỉnh và lôi cuốn, tác giả thực sự là một nhà văn uyên thâm và có tài thấu cảm
4
414969
2014-12-21 15:21:41
--------------------------
139671
6833
Chúng ta đã quá quen với đề bài "Hãy tượng tượng bạn là ai đó và kể lại câu chuyện nào đó dưới góc nhìn của mình". Những câu chuyện luôn luôn có thể thay đổi hoàn toàn màu sắc của nó dưới 1 góc nhìn khác vì cảm nhận của mỗi người về mỗi sự vật, sự việc là không giống nhau. Và câu chuyện "Bí ẩn về con chó lúc nửa đêm" này sẽ được kể dưới góc nhìn của Christopher - một cậu bé mắc bệnh tự kỷ, không thể hiểu những người xung quanh, thân thiết với động vật, cảm thấy sợ hãi trước những thay đổi bất thường nhưng lại rất giỏi toán. Cậu cứ thế sống trong thế giới của riêng mình cho đến khi con chó của hàng xóm cậu bị nạn, cậu muốn tìm ra nguyên nhân và cuộc điều tra đã dẫn cậu tới những bí ẩn khác nữa... Chi tiết các bạn nên tự tìm hiểu. Nhưng hãy tin tôi, khi tôi mượn bạn tôi cuốn truyện này, tôi đã không muốn trả lại. Câu chuyện có cái kết đẹp, làm sáng lên những ý nghĩa nhân văn nhân đạo (theo cách nói của một số người) nhưng cá nhân tôi thì chỉ thích lối suy nghĩ khác người của cậu bé :)
4
373749
2014-12-08 11:10:23
--------------------------
137549
6833
Mình tình cờ thấy 1 link suggest về 29 cuốn sách quý ông cần đọc, bìa sách tiếng Anh trông giống hệt "Plato và con thú mỏ vịt bước vào quán bar" bản tiếng Việt ;), nên mình tò mò lướt qua và quyết định mua thử.

Rất tuyệt vời, cầm sách lên không thể bỏ xuống, đúng là phải đọc (strongly recomment).

Con gái 10 tuổi của mình cũng đọc say sưa như mẹ, tất nhiên còn nhiều chỗ không hiểu nhưng bạn ấy sẽ còn đọc lại cuốn này vài lần nữa, khi bạn ấy lớn thêm lên).
5
197697
2014-11-27 09:23:16
--------------------------
