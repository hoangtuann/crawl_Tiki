321945
11952
Câu chuyện ngọt ngào như giọt mật vậy. Mình đặc biệt thích các cô bé da ngăm bánh mật, và mình thích thú khi đọc đến đoạn mẹ bánh mật hứng lại dòng mật chảy, đem làm bánh và còn nặn cho cô con gái 1 chiếc nón cực xinh để cô bé không bao giờ quên mang nón nữa. Đó là sự săn sóc đầy yêu thương và dịu dàng của mẹ cho con gái!
Ở Bích Khoa luôn có sự sáng tạo và độc đáo. Tranh minh hoạ lại cực dễ thương và đáng yêu. Đây chính là câu chuyện mà bất kỳ bé gái nào cũng mê tít. 
5
62763
2015-10-15 11:00:13
--------------------------
315241
11952
Chuyện vẽ đẹp, đúng với tiêu đề, nhìn nét vẽ rất kỳ bí!
Mình mua vì muốn ủng hộ người Việt Nam sáng tạo! Mình rất hãnh diện vì Việt Nam mình có những tác giả vẽ chau chuốt như vậy!
Tuy nhiên, mình nghĩ nội dung các câu chuyện có hơi hướm hù dọa, phản khoa học, na ná như một số tác giả châu Á khác. Cá nhân mình nghĩ chuyện cho trẻ con thì nên khuyến khích sáng tạo, khơi dậy tình tò mò và tinh thần ham học hỏi của trẻ thì hợp lý hơn là áp đặt hù dọa!
3
614101
2015-09-28 15:01:36
--------------------------
