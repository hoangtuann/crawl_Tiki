344185
4810
Quyển sách khá to nhưng lại mỏng quá trong khi giá lại đắt. Có lẽ giá đắt là do cái CD.
Cái CD này chất lượng âm thanh khá tốt nhưng lúc mình mua về nghe mãi mới biết là phần nghe dùng ở chỗ nào.
Giấy trắng, sáng. Mực in nét, chữ to, rõ ràng.
Trước mỗi bài học đều có một đoạn đọc hiểu khá dài.
Bìa dày nhưng không phải loại bìa gập nên mình mới để trong cặp có một tí đã bị quăn mép với mép bị tẽ giấy ra í, nhìn xấu lắm.
Cuối sách là phần đáp án, dùng để check lại đáp án rất tiện nhưng có một số câu đúng sai thì chỉ có đáp án mà không giải thích, chuyện này khiến mình mơ hồ sau khi check đáp án.
4
800850
2015-11-27 22:53:12
--------------------------
269646
4810
Mình mua quyển sách này để luyện toefl cho em trai mình (khoảng tầm lớp 5). Mình thấy nội dung phù hợp với các bé cấp 2,3 
Trong sách có câu hỏi để làm bài tập. Các dạng câu hỏi rất đa dạng. Tuy nhiên sách vẫn chưa giải thích chuyên sâu lí do đúng, sai. Cũng giống như sản phẩm khác trong bộ sách này, số lượng câu hỏi hơi ít.
Và chất lượng của CD hơi kém 
2
90370
2015-08-17 17:01:35
--------------------------
