388959
9017
- Suốt cả cuốn sách, mình cảm nhận được sự u ám trong cách viết của tác giả. Về nội dung, không thể bàn cãi đây là một tác phẩm kinh điển. Tuổi mười lăm dễ lầm lạc, vấp ngã. Bài học "chọn bạn mà chơi" được thể hiện rõ qua cả hai phần truyện. Do đọc lời người dịch trước nên mình đã biết trước "bộ mặt thật" của Kasia và Ewa, nhưng khi đọc vẫn thấy rõ cả hai đều đối xử với Marysia có chút tốt, chút nguy hiểm, chút lợi dụng, chút giúp đỡ. Nói thật thì mình chỉ mới đọc một lần nên có lẽ vẫn chưa nắm hết 100% nội dung truyện. Về hình thức, ban đầu, mình thấy có vẻ như bìa không được đẹp, nhưng khi cầm sách trên tay mới cảm nhận rõ được độ sắc lạnh trong đôi mắt cô gái. Đặc biệt nhất, dịch thuật không tế nhị lắm khi hoạch toẹt cả ra khá nhiều chữ có từ thay thế thích hợp hơn. Mình hi vong ở những tác phẩm khác của Lê Bá Thự.
4
411530
2016-03-01 00:08:29
--------------------------
301542
9017
Tác phẩm đáng sợ, cô gái Marysia bạn đã làm gì với chính mình, đã biến mình thành cái gì thế kia? Tớ đã thấy sốc, căm ghét, ghê tởm hành động của bạn với một người có công với đất nước Ba Lan đẹp tươi của bạn. Cớ sao thế chứ? Cô bạn Marysia tội nghiệp ơi bạn đã bị lợi dụng, bạn coi họ là bạn nhưng họ coi bạn là cái gì cơ chứ? Không là gì cả đúng chứ? Họ tốt với bạn lúc đầu nhưng rồi về sau thì sao? Bạn đã mất hết rồi nhưng bạn có thể làm lại cơ mà tại sao lại làm vậy? Rất nhiều câu hỏi tớ muốn hỏi bạn đấy. Tác phẩm đáng sợ về tuổi ô mai!
5
508336
2015-09-14 18:28:26
--------------------------
175683
9017
"Cô gái không là gì", một tiêu đề lạ lùng, và cũng là một câu chuyện lạ lùng viết về một đề tài quen thuộc, tuổi mới lớn. Ngay từ đầu, mình đã bị cuốn hút vào những dòng tự sự đầy chân thực của nhân vật chính, cô bé Marysia. Câu chuyện của cô, là một hành trình đầy biến động từ một cô bé quê mùa thành một người con gái sành điệu và phù phiếm, và là một cái nhìn lạnh người về thực trạng của một xã hội hiện đại đầy cạm bẫy. Marysia hẳn vẫn là một cô bé trong sáng, đáng yêu nếu như không bị những người bạn xấu và cả thôi thúc sống trong giàu sang dần nhấn chìm cô vào bùn lầy. Những tình tiết cứ tiếp diễn mà chẳng cần những ngôn từ đao to búa lớn, vì chỉ cần bằng giọng văn đời thực, tác giả đã lột tả mọi cung bậc cảm xúc mà Marysia trải qua. Nỗi đau đớn của nhân vật chính là khi, ngay trong lúc tưởng chừng hạnh phúc nhất, cô lại nhận ra sự ê chề, nhục nhã bởi những ước mơ xa hoa gây ra cho mình.
Một cuốn sách đầy sức mạnh, diễn tả sự xáo trộn và nỗi đau của tuổi trẻ bồng bột, của một cuộc sống với biết bao gam màu sáng tối lẫn lộn nhau.
4
109067
2015-03-30 19:59:29
--------------------------
167118
9017
"Cô Gái Không Là Gì" không đơn thuần là một cuốn sách kể về tuổi mới lớn hay những biến đổi xung quanh nó. Marysia không chỉ là nhân vật được tạo ra để trở thành trung tâm câu chuyện hay gián tiếp truyền tải thông điệp tới độc giả, mà còn chính là lăng kính phản ánh nên cái gọi là "tự do".
Mình thấy đây là cuốn sách hơi khó đọc. Mọi suy nghĩ, mọi lời tự sự hay những hành động phản ánh tầm nhận thức của Marysia qua từng phần của truyện dường như đều ẩn chứa nhiều tầng ý nghĩa khác nhau.
3
48662
2015-03-14 00:53:10
--------------------------
