482985
10083
Trong tập 10 này mình ấn tượng với mẩu truyện " ai đó" tình bạn đẹp giữa Makorin và Miko Miyuki .. tình bạn là mãi mãi, hay "một ngày của Mamoru" không có em trai nấu cơm nhóc Miko vô bếp nấu mà mọi thứ trong bếp lộn xộn và thức ăn Miko nấu thì khỏi nói luôn đọc xong chỉ ôm bụng cười và thương cậu em Mamoru khi có một bà chị vụng như thế. Mình đã đọc bộ truyện này khá lâu nhưng bây giờ mới có đủ tiền mua đủ cả bộ khi một lần thấy trên Tiki bán, cám ơn Tiki nhiều nếu không chắc mình không biết mua ở đâu để có đủ bộ truyện này.
5
1379094
2016-09-10 18:04:25
--------------------------
474152
10083
Đọc đến tập này thì mình cũng đã đọc qua nhiều mẩu truyện về kì nghỉ đông hay dịp lễ valentine rồi mà mãi vẫn không thấy Miko lớn. Đọc mẩu truyện về dịp lễ tình nhân trong dịp này thì tự nhiên lại nghĩ chắc Miko cũng lớn lớn rồi, cũng biết ghen khi tưởng rằng Tappei nhận quà của Miho rồi nhưng khi đọc đến khúc Yuko đến tìm Kenta rồi Kenta bỏ đi, trùng hợp sao bụi bay vào mắt Tappei làm chảy nước mắt và Miko anh dũng ra hỏi Tappei thích Kenta hả thì mới nhận ra hình như mình hiểu lầm, thật ra Miko vẫn chưa lớn. Còn phát hiện thêm thì ra hủ nữ là nơi nơi đều có
5
80823
2016-07-11 02:34:48
--------------------------
462329
10083
Mình đã từng đọc bộ truyện tranh nói về một cô bé tinh nghịch, đó chính là cô nhóc Asari. Nhưng khi tìm đọc đến bộ truyện tranh "Nhóc Miko- cô bé nhí nhảnh" cho mình một cảm giác khác. Một bên là sự tinh nghịch, bên kia là sự nhí nhảnh. Đây đều là những nét tính cách rất dễ thương của các cô nhóc. Ở Miko, chúng ta sẽ thấy một cô bé có những suy nghĩ rất đơn giản, mộc mạc. Cô nhóc này đối xử với mọi người, với bạn bè rất tốt, không hề có sự tính toán. Trẻ em trên Thế giới cứ như Miko thì chắc hẳn đâu đâu cũng có tiếng cười. Một bộ truyện tranh nên đọc.

4
174917
2016-06-28 15:02:06
--------------------------
450374
10083
Mình có nguyên một bộ truyện của Miko luôn rồi.
Truyện dễ thương cực kì, tuy đã qua cái tuổi đọc truyện tranh nhưng mình vẫn khó dứt ra khỏi em Miko này.
Thích Tappie với Miko quá đi ngọt ngọt cực kì, nhưng hai em ấy còn quá nhỏ nhỉ.
Mong muốn tập sau của Miko ra nhanh để còn hốt về đọc.
Mua gì cũng tiết tiền đối với Miko là không bao giờ tiếc.
Dễ thương quá trời, mắc cười có cảm động có, đáng yêu có đôi lúc cũng tình tiết gây cấn nữa.
Phục tác giả quá đi sao mà sáng tác ra được quyền truyện hay dữ vậy trời

5
414716
2016-06-18 20:15:10
--------------------------
441216
10083
Trong miko tập 10 này truyện mình thích nhất là truyện" một ngày của mamoru " nhất luôn . Nội dung nè : một ngày , như thường lệ miko ngồi nhà chơi , xem tivi . Khi đó mamoru thì phải nấu cơm nè , giặt giũ và nhiều việc khác nữa . Một ngày , vì quá mệt nên mamoru viết một lá thư viết " Mệt lắm rồi " . Mẹ và miko thấy thì tưởng rằng . . . Mamoru định nhảy lầu tự tử ( tưởng tượng ghê quá đấy )  nhưng hoá ra mamoru đang chơi ở nhà Yuka mới chết . Chi tiết thì các bạn mua nha còn nhiều truyện hay trong tập 10 này mà.

5
280483
2016-06-02 22:19:51
--------------------------
416228
10083
Mình mới order tối hôm trước là sáng hôm sau Tiki đã giao hàng, đóng gói cẩn thận thích ghê luôn! 
Bé con nhà mình (đang học lớp 4) nói rằng đây là bộ truyện đang "Hot" ở trường. Truyện kể về những câu chuyện thường ngày của cô bé có tên Miko, một cô bé giàu tình cảm, rất thương mẹ và em của mình. Qua cách kể chuyện giản dị nhưng không kém phần hấp dẫn của tác giả Ono Eriko, truyện đã thu hút được các cô cậu bé lứa tuổi này. 
Cám ơn Tiki đã giao hàng nhanh chóng và cẩn thận với giá lúc nào cũng rẻ hơn giá bìa!
4
895614
2016-04-14 18:46:22
--------------------------
384100
10083
Vi diệu! Đó là 2 từ muốn nói với bộ truyện này đấy! Nó mang đến cho mình nhiều cảm xúc qua mỗi chap truyện, nó có rất nhiều nội dung xoay quanh cô nhóc lùn lùn đáng yêu ấy. Quả thật ngoại hình của Miko chẳng có gì đặc sắc, thậm chí là khi mới đọc mình còn chê là xấu (khen Miho đẹp mới tức) :( Bây giờ thì thấy Miho là không thích rồi. Còn Miko, cô nhóc chả có gì nổi trội về ngoại hình lẫn học vấn lại thu hút chúng ta như vậy. Học thì dở, ngoại hình lùn nhưng tâm hồn lại vô cùng trong sáng.Tính cách tự nhiên, cởi mở chả biết gì về tình yêu tình iếc. Nhiều lúc mình muốn mình phải giống cô nhóc nhưng......tâm hồn mình "trong tối" lắm! Haha...
5
1034042
2016-02-21 19:58:15
--------------------------
342015
10083
MÌnh cố gắng bỏ heo để mua được mấy cuốn Miko này. Và từ tháng một đến bây giờ đã tích lũy được mười ba cuốn. MÌnh thích nhất mẫu truyện khi Miko biến thành học sinh cấp ba. Nhìn nó cứ vui vui sao ấy. Thích bạn Tappie nhiều lắm. Bạn thích Miko mà sao không nói ra đi ? Cảm ơn bạn Kenta luôn gán ghép cho Tappie với Miko thành đôi cho dù tội bạn Miho quá trời. Bạn Mari mau kiếm người mình thích đi nha. Cứ ngây thơ như thế nhé Miko. Làm nhiều người sụp đổ vì bạn lắm đó nha. 
5
945879
2015-11-23 16:28:22
--------------------------
311157
10083
Trong tập 10 này mình ấn tượng với mẩu truyện " ai đó" tình bạn đẹp giữa Makorin và Miko Miyuki .. tình bạn là mãi mãi, hay "một ngày của Mamoru" không có em trai nấu cơm nhóc Miko vô bếp nấu mà mọi thứ trong bếp lộn xộn và thức ăn Miko nấu thì khỏi nói luôn đọc xong chỉ ôm bụng cười và thương cậu em Mamoru khi có một bà chị vụng như thế. Mình đã đọc bộ truyện này khá lâu nhưng bây giờ mới có đủ tiền mua đủ cả bộ khi một lần thấy trên Tiki bán, cám ơn Tiki nhiều nếu không chắc mình không biết mua ở đâu để có đủ bộ truyện này.
5
165748
2015-09-19 21:50:01
--------------------------
305997
10083
Đúng như tên truyện, cô bé nhí nhảnh nên truyện lí lắc lắm. Đọc vào những ngày mưa buồn buồn là vui luôn. Mấy cuốn Miko còn có cái hay là có chuyện bên lề về tác giả, mình thích cái này nhất. Đọc cuốn này thương Mamoru, có bà chị Miko hơi vô tâm, lại vụng; nhưng cuối cùng cũng hiểu ra được vấn đề. Tiki giao hàng rất đúng ngày, mình đặt từ chủ nhật mà thứ 5 lấy được luôn (vừa đúng 4 ngày theo giao hẹn). Bạn giao hàng còn rất đặc biệt, giao đến lúc trời không mưa, bạn ấy về thì mưa nên thật tội nghiệp bạn ấy. Cám ơn Tiki nhé.
5
686590
2015-09-17 11:56:37
--------------------------
298676
10083
Mình mua ở Tiki nhiều nhất có lẽ là bộ truyện Miko. Mình rất yêu mến Miko. Những câu chuyện thường ngày của cô bé tưởng chừng như rất bình thường nhưng dưới ngòi bút của Ono Eriko, những câu chuyện ấy trở nên hấp dẫn hơn bao giờ hết. Mình thích những câu chuyện liên quan đến gia đình của Miko. Miko nhìn vậy chứ thương mẹ và em lắm đấy nhé. Miko dễ thương ghê quá đi. Bìa truyện tập này gồm Miko và những người bạn của cô bé nên mình cũng thích nữa. Cám ơn Tiki đã giao hàng siêu nhanh cho mình.
5
247850
2015-09-12 20:23:15
--------------------------
291690
10083
Tình cảm gia đình lại một lần nữa khiến mình rung động bởi sự thành thật có phần ngây ngô nhưng hết sức chân thành của Miiko. Miiko có thể lắm lúc giận mẹ vì mẹ hay mắng Miiko, hay kêu Miiko phải học tập theo thằng em Mamoru, nhưng thẳm sâu trong trái tim Miiko mẹ là một người tuyệt vời, đã nuôi lớn, chăm sóc Miiko chu đáo dù có gặp phải bao khó khăn, vất vả khi hạ sinh Miiko. Với những suy nghĩ từ tận đáy lòng, Miiko đã viết nên bài văn với tất cả những tình cảm, những sự biết ơn sâu sắc dành cho đấng sinh thành của mình, khiến bố mẹ không khỏi cảm động và tự hào, cũng khiến mình cảm thấy biết ơn vì đã được sinh ra nhiều hơn!
5
364412
2015-09-06 16:37:23
--------------------------
252739
10083
Nhóc Miko - Cô bé nhí nhảnh rất thú vị và đáng yêu. Hồi nhỏ mình cũng rất nghiền, nghiền và nghiền nhóc Miko này vì tạo hình nhân vật hết sứ dễ thương, những mẩu chuyện tuy ngắn và xoay quanh nhân vật Miko nhưng không hề gây cảm giác nhàm chán mà trái lại còn có thể gây nghiện như Doraemon vầy. Mặc dù bây giờ "nhớn" rồi nhưng mình vẫn rất thích mấy thể loại truyện tranh này. Cứ mỗi lần buồn buồn mình lại lôi nó ra đọc, ắt hẳn tâm trạng từ đó là tốt lên ngay lập tức.
5
368588
2015-08-03 21:40:02
--------------------------
249863
10083
Mỗi lần đọc "Nhóc Miko" là mỗi lần nhen nhóm trong mình những cảm xúc thú vị. Ở tập này mình rất thích câu chuyện "tình củm" của Miko và Tappei. Đúng là tình cảm của con nít nên trong sáng và dễ thương vô cùng. Cũng như Doraemon, "Nhóc Miko" là những câu chuyện về tình bạn, tình yêu, gia đình, cuộc sống,...mang tính giáo dục rất cao. Không chỉ dành cho trẻ em mà những lớn như mình cũng nên đọc, để được sống lại trong thời thơ ấu, cái tuổi được vô tư hồn nhiên như Miko.
5
178929
2015-07-31 23:07:37
--------------------------
249029
10083
mình rất thích tình cảm trong sáng giữa Miko và Tappaie. Thích cái sự hồn nhiên và ngốc ngốc của hai đứa trả đã thế còn nhỏ tí còn biết ghen nữa chứ cực kì đáng yêu. Đôi lúc thấy Miko vô tư hồn nhiên quá nên nhiều chuyện không theo đúng hướng nhưng cũng đôi lúc tôi cảm nhận được sự chững chạc đang lớn dần lên trong quá trình trưởng thành của cô bé. Mặc dù đã không còn ở cái tuổi như Miko nữa nhưng mỗi khi đọc lại tôi vẫn rất rất thích và có lẽ tình cảm với Miko sẽ không bao giờ mờ nhạt trong tôi
5
133796
2015-07-31 12:08:00
--------------------------
239853
10083
Đôi lúc mình không thích Miko lắm vì có lần 1 bạn tỏ tình với Tappie và bắt cậu ý đi ăn thì Miko cứ đi theo kiểu như Ghen nên phá đám hết lần này với lần nọ ý . Và cũng ngược lại . Như thế cho thấy là Tappie với Miko cũng thích nhau mà :) Nhưng thấy tội cho Yoshida quá vì Miko không hiêu được tình cảm mà cậu cho cho Miko . Dù sao Miko tính cũng khá là trẻ con và có hơi phân khờ khạo mà . Dù sao Miko cũng khá là dễ thương 
4
713167
2015-07-24 09:37:02
--------------------------
213936
10083
Ấn tượng đầu tiên với mình là bìa truyện rất đáng yêu. Có lẽ là một buổi tiệc chúc mừng ''Nhóc Miko- cô bé nhí nhảnh" đã lên đến con số 10 rồi chăng? Bìa truyện rất dễ thương bao quát hầu hết các nhân vật của truyện. Vẫn là xoay quanh cô bé Miko với những tâm tư tình cảm của cô bé, dễ thương quá! Tuy nhiên trong truyện có một câu chuyện về nhóc Mamoru mà mình ấn tượng, vì bị Miko vô tư ức hiếp quá mà cậu đã đi ra ngoài và mọi người được một phen hốt hoảng. Thế nhưng mọi hiểu lầm đã được giải quyết và gia đình Miko lại trở về với sinh hoạt bình thường của mình. Điểm mình không thích lắm là câu chuyện kinh dị có vẻ không được hay cho lắm.
4
299614
2015-06-24 12:47:22
--------------------------
75594
10083
''Nhóc Miko! Cô bé nhí nhảnh'' thật sự là một bộ truyện tranh thiếu nhi rất hay, rất đáng đọc. Bộ truyện xoay quanh cuộc sống của cô nhóc Miko học cấp một ngây thơ và dễ thương. Truyện không chỉ có hình vẽ rất đáng yêu, mềm mại chau chuốt mà nội dung truyện cũng có nhiều ý nghĩa sâu sắc nói về tình bạn, tình thầy trò, tình cảm gia đình, giáo dục giới tính và cả những tình cảm trong sáng của lứa tuổi học trò. Đây là một bộ truyện không chỉ dành cho lứa tuổi thiếu nhi mà còn thích hợp với cả những người đã trưởng thành, giúp họ có thể nhớ lại một thời tuổi thơ trong sáng, vô tư đã vô tình bị lãng quên bởi sự xô bồ của cuộc sống hiện nay.
5
104539
2013-05-19 00:39:52
--------------------------
31599
10083
Đã quá tuổi để đọc một bộ truyện tranh thiếu nhi, nhưng đọc Nhóc Miko mình vẫn cảm thấy như mình chính là cô nhóc đó vậy. Những câu chuyện rất bình thường như chính những chuyện thường xảy ra chung quanh chúng ta đã được tác giả vẽ nên một cách sinh động, hài hước, và mỗi câu chuyện cũng như một bài học dành cho tất cả mọi người, kể cả các em thiếu nhi hay là phụ huynh cũng vậy. 
5
16445
2012-06-27 14:46:20
--------------------------
