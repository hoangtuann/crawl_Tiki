471398
11137
Dành cho những ai cảm thấy mất động lực sống và chần chừ trong việc theo đuổi mục tiêu của mình.
Ban đầu khi nhìn thấy cuốn sách này tôi ko có lấy 1 tý cảm xúc gì, tôi nhớ là đã lướt qua nó trên dưới 5 lần. Nhưng trong 1 lần dạo quanh trên "sachchonloc", tôi lại vô tình bắt gặp nó, vì đang chán nên tôi cũng thử xem qua phần giới thiệu. Tôi bị cuốn hút ngay từ những dòng đầu tiên. Thế là tôi phóng ngay qua Tiki (vì tôi quen mua ở đây) để đặt mua.
Về hình thức thì sách của TGM là cực tốt rồi, còn nội dung, tôi thật sự ko biết diễn tả nó thế nào, nó quá tuyệt vời!!!!! Và đây cũng là cuốn sách đầu tiên khiến tôi rơi nước mắt. Tôi muốn gửi lời cảm ơn trân thành nhất đến tác giả cũng như TGM. 
5
899706
2016-07-08 14:57:43
--------------------------
417409
11137
Đây là một trong những quyển sách cảm động từ trước tới nay mình từng đọc. Ấn tượng đầu tiên khi tiếp xúc với quyển sách là sách được thiết kế đẹp mắt, chất lượng in tốt đúng phong cách của TGM, ngoài ra mình còn ấn tượng với câu "Cuốn sách khiến cho những người cứng rắn nhất cũng phải rơi nước mắt". Mình liền tức tốc mua ngay tác phẩm này về đọc và quả nhiên quyển sách này đã không làm mình thất vọng, bức tranh về một cậu bé yêu hòa bình đã lấy đi nước mắt và những cảm xúc khó tả trong tôi.
5
509425
2016-04-16 22:31:55
--------------------------
344521
11137
" Bạn có thể học từ sách cũng như học từ trường đời, nhưng tôi chắc chắn rằng cái giá phải trả cho việc học từ sách thấp hơn cái giá phải trả cho việc học từ trường đời rất nhiều.", Trần Đăng Khoa viết. Sứ giả yêu thương, quyển sách rất đáng đọc bởi nó khiến ta có cái nhìn sâu sắc hơn về con người, về cuộc sống. Buồn thương cho những số phận không may nhưng luôn lạc quan với cuộc sống. Tôi đã rơi lệ rất nhiều khi đọc cuốn sách này. Giờ tôi dường như có cảm giác hơn, đồng cảm hơn với những số phận không may. 
5
914758
2015-11-28 16:24:43
--------------------------
292604
11137
Đây là quyển sách sẽ hữu ích cho những ai thấy mình sống trong đau khổ, thấy mình luôn là nạn nhân của những thiệt thòi, thấy mình không thiết sống, thấy mình có quá ít thời gian để làm việc gì đó,.... Quyển sách này dành cho bạn, dành cho tất cả mọi người.

Thông điệp truyền tải của quyển sách: Hãy sống hết lòng, sống yêu thương, SỐNG TRỌN VẸN TRONG TỪNG GIÂY PHÚT VÀ "HÃY NHỚ VUI SỐNG KHI CƠN GIÔNG BÃO ĐI QUA" Biểu tượng cảm xúc heart

Quyển sách có giá trị suy ngẫm hơn khi đây là hoàn cảnh thực, không phải hư cấu hay tưởng tượng để khuyên chúng ta: hoàn cảnh thực của cậu bé Mattie J.T.Stepanek và mẹ Jeni Stepanek
Đây là clip nói chuyện của cậu bé Mattie trong chương trình của Oprah:
https://www.youtube.com/watch?v=B2Rg9TTuoDE
Đây là clip của bé trong chương trình Larry King Live:
https://www.youtube.com/watch?v=D3ALna5Kg_M
(Xuân đưa clip để nói chắc chắn rằng: cậu bé này có thực)
4
448995
2015-09-07 14:41:06
--------------------------
248045
11137
Sau "BA ơi mình đi đâu", sau " trên bục giảng" thì đây là cuốn sách tiếp theo mang đến một cách nhìn thật khác. Vẫn là những nỗ lưc phi thường, là nụ cười đè nén nỗi đau, là sự vật lộn chống chọi với sự thống khổ, của trái tim cứ bị bóp nghẹn hang ngày. Và cuốn sách này cho tôi nhìn thấy thêm một tình yêu được nhân rộng khắp nơi.
Rằng cái gì xuất phát từ trái tim sẽ đến được với trái tim. Tôi cứ suy nghĩ mãi về cảnh mấy mẹ con không có việc làm, không có nguồn thu nhập, nhưng vì long tốt ở khắp mọi nơi nên họ đã được giúp đỡ để có nhà ở, để có thể tới bệnh viện định kỳ, và nhờ có đã duy trì được sự song của một nhà thơ tuổi nhỏ mà tâm hồn không hề nhỏ. Cậu bé ấy là thiên thần, thiên thần của tình yêu thương, đã gắn kết được bao con người, đem đến bao nụ cười cho mọi người xung quanh.
Tựa đề nói rang đến những người mạnh mẽ nhất cũng sẽ phải rơi nước mắt. Nhưng tôi không nghĩ thế, những người cảm nhận được tình yêu của cậu bé có thể sẽ không khóc, mà sẽ mỉm cười bởi đó chính là thông điệp mà cậu bé muốn mang tới với mọi người. Người ta mỉm cười với hạnh phúc, đôi khi mỉm cười có thể trào nước mắt, nhưng đó là nước mắt hạnh phúc, bởi cậu bé là sứ giả của yêu thương cơ mà.
5
204039
2015-07-30 17:10:16
--------------------------
210946
11137
Trước khi đọc "Sứ giả yêu thương", mình luôn than vãn sao cuộc sống bất công với mình thế, thường xuyên cảm thấy khổ sở về những gì mình muốn có mà không có được, so sánh ganh tỵ với bạn bè. 
Và sau khi đọc xong câu chuyện của cậu bé Mattie, cách nhìn cuộc sống của mình đã thay đổi, cảm thấy biết ơn vì Chúa cho mình được sống, được thực hiện ước mơ, được khỏe mạnh. Cảm nhận được sức mạnh của tình yêu thương giữa con người với nhau. Cảm nhận được khao khát được sống của cậu bé phải chống chọi với căn bệnh loạn dưỡng cơ. "Sứ giả yêu thương" giúp mình nhận ra một chân lý đó là khi cho đi yêu thương sẽ nhận lại yêu thương!
5
248664
2015-06-19 22:32:51
--------------------------
207407
11137
Nếu bạn là một người chuộng hòa bình, cuộc sống còn nhiều thách thức ở tương lai, mặc dù bề ngoài trông có vẻ nghị lực nhưng trong lòng lại luôn hoang mang và đôi khi không biết lựa chọn của mình có đúng hay không... thì cuốn sách này là một nguồn cảm hứng lớn. Đây không phải là cuốn kĩ năng, định hướng, chỉ việc cho bạn nhưng lại là câu chuyện mang tới cho bạn niềm lạc quan, tin yêu và đam mê lớn với hòa bình. Mattie Stepanek - một cậu bé người Mỹ mắc chứng loạn dưỡng cơ hiếm gặp, không thể cứu chữa. Những tưởng cậu lại sớm bất lực từ giã cõi đời như anh chị trước đó của mình. Nhưng không, Mattie đã làm nên nhiều điều kì diệu bằng chính tình yêu với cuộc sống, bằng "khúc tâm ca" của mình và trọng trách mà Chúa đã ban cho. Cậu cùng với người mẹ- tác giả cuốn sách này- đã bền bỉ chiến đấu với bệnh tật, chiến đấu với bất hạnh cuộc đời. Và, nụ cười của Mattie luôn tỏa sáng trong những buổi diễn thuyết đầy ấn tượng. Gần 14 năm sống ở cõi đời. Ngắn ngủi một đời người nhưng bất tử một trái tim.
5
51578
2015-06-12 10:51:17
--------------------------
189818
11137
Nói thật, nếu có 10 sao thì tôi cũng sẵn sàng nhấn :) Con bạn thân mua cuốn sách này nhưng bị tôi giành đọc trước. Nghe nói hay lắm! 
Và thế rồi, Mattie hiện ra trong trí tưởng tượng của tôi với người mẹ đáng kính của cậu trong đám tang có những người lính cứu hỏa, với màn hình và những email gửi cho Oprah Winfrey- một người phụ nữ quyền lực thế giới đáng ngưỡng mộ, những chuyến đi nước Mỹ, những buổi thuyết trình, những lần điều trị, những lúc cậu lên cơn nguy kịch..Tôi thật chả hiểu sao mãi đến khi đọc "Sứ giả yêu thương" tôi mới biết đến cái tên Mattie Stepanek ! Lẽ ra tôi phải biết cậu ấy lâu rồi chứ! 

Mattie bảo:
"Bất kể bạn cầu nguyện theo cách nào, bất kể bạn chọn cái tên nào để chỉ Đấng tối cao, thì điều quan trọng nhất chính là hãy trở thành một người tốt đẹp hơn nhờ ý thức mộ đạo của mình"... Tôi nghĩ tới IS!
Mattie là người của Công giáo. Từ lúc mầm sống là cậu bắt đầu được gieo xuống thì sứ mệnh Lạc Quan &Hòa Bình cũng được thiết lập.  Mat xứng đáng là một Nhà thơ, nhà hoạt động vì hòa bình và một chính trị gia tuyệt vời dù cuộc đời chỉ điểm đến tiếng chuông thứ 14. 
Chính cậu cũng thôi thúc người ta, nhưng là sự thôi thúc thầm lặng từ bên trong, lắng nghe và cảm nhận Khúc Tâm Ca của chính mình cũng như của người khác.......
Phải mất khoảng 1 tuần tôi mới dám cầm tiếp cuốn sách lên để đi đến phần kết mà mình biết chắc sẽ lại ngồi khóc. Nhất đinh một ngày tôi sẽ đến Mỹ và thăm bức tượng đồng của vị Sứ giả này  !
5
415064
2015-04-28 07:14:08
--------------------------
131020
11137
Nhìn tiêu đề sách, mình không nghĩ nó sẽ khiến mình rơi lệ, dù chỉ một lần. Thế mà nó lại khiến mình cảm, khóc rất nhiều lần...

Từ phi thường không đủ để nói về Mattie. Một cuộc đời của 1 cậu nhóc chỉ sống chưa quá 14 năm. Vậy mà những suy nghĩ trong đó, khiến mình phải lắng lòng mà suy nghĩ... 

Hòa bình và tình yêu - Các câu chuyện xuyên suốt quyển sách đều đem đến cho mình 1 cảm giác, một sự thấu cảm không thể diễn tả bằng lời. Chính cái cảm đó làm cho mình đọc quyển sách một cách say mê. Thường khi đọc sách mình hay chú ý xem có gì để học hỏi, để suy nghĩ. Nhưng quyển sách này, khiến mình đi theo mạch cuộc đời của Mattie. Nhanh, chậm, trầm, bổng, cay, đắng, ngọt, bùi... 

Vì thế, khi đọc từ trang 400 trở đi, cũng là lúc cần một trải nghiệm cuộc đời đủ để thấu hiểu được thế nào là ranh giới sinh tử và sự bình an trong tâm hồn dù tương lai có tăm tối đến đâu chăng nữa. 

Một quyển sách rất rất rất đáng đọc. 

Nhưng có 1 ghi chú nhỏ: Những ai đã từng trải qua nỗi đau, vết thương lòng, hay sự bất lực trước một tình cảnh nào đó, cả những cảm giác ái biệt ly khổ... thì đọc quyển sách này rất thấm... và một cách tự nhiên, nước mắt sẽ rơi...

9/10 Cho quyển sách này.
5
8714
2014-10-21 20:40:15
--------------------------
120554
11137
Đánh giá của bạn (Gửi vào Ngày 13 tháng 8 năm 2014):
tôi đã cầm cuốn sách lên và rồi tôi đọc nó tới 3h sáng chỉ vì không thể dừng lại và dứt nó ra được. một câu chuyện khiến người mạnh mẽ nhất cũng phải rơi nước mắt. Mattie có một cuộc đời ngắn ngủi nhưng những gì cậu bé để lại là cả một DI SẢN....một cậu bé mắc căn bệnh hiếm có ngay từ khi cất tiếng khóc chào đời, nhưng hãy nhìn xem Mattie đã sống như thế nào trong hơn 13 năm, cậu bé có những suy nghĩ chính chắn mà ẩn chứa trong đó đong dầy tình cảm yêu thương khiến cho người đọc phải xúc động, một cậu bé tươi cười và có khiếu hài, nhìn nhận mọi thứ thật khách quan...Mattie có quá nhiều điều mà chúng ta phải học hỏi...!!!
5
299762
2014-08-13 12:32:12
--------------------------
115389
11137
Tôi chọn quyển sách này sau khi mua một quyển trước đó để đơn hàng của tôi đủ điều kiện được tặng 300 tiki xu. Nhưng sau khi đọc lời mở đầu, chỉ lời mở đầu, tôi nhận ra đây là quyển sách thật sự đặc biệt. Chính vì thế, dù mới chỉ đọc đúng 98 trang sách, tôi vẫn muốn viết nhận xét này để giục tất cả đọc "Sứ Giả Yêu Thương". (ko phải lời mở đầu dài 98tr đâu!) 
Có thể bạn cũng như tôi, tôi không biết 2 người trên bìa sách là ai, tôi không biết Mattive là ai? và cái tựa sách nghe kiểu như hơi 'cải lương' - đó là những gì tôi nghĩ khi mới nhìn vào, tôi nghĩ nó thật sướt mướt và đó cũng là lý do vì sao tôi không bỏ tiền mua quyển sách trước đây. 
Thật vui nếu bạn không nghĩ những ý đại loại như tôi, nhưng nếu bạn giống tôi, với tư cách là người đã mua sách và dù chỉ mới đọc được gần 1/4 quyển sách, tôi giơ tay thề với bạn rằng quyển sách thật sự rất rất hay (tôi nghĩ từ 'rất rất hay' chưa truyền đạt được trọn vẹn giá trị mà quyển sách mang lại- ít ra là đối với tôi). 
Tôi không biết làm thế nào để cho bạn biết đầy đủ về 2 người trên bìa sách, một cách tối giản nhất: người phụ nữ kia là tác giả quyển sách này, và là mẹ của cậu bé; bà kể cho bạn nghe câu chuyện về cuộc đời bà và các con bà - câu chuyện kiến những người cứng rắn nhất cũng phải rơi nước mắt. Không phải rơi nước mắt theo kiểu quỵ lụy, mà là thán phục và thấm đượm. 
Và cũng không chỉ đơn thuần là câu chuyện, có những giá trị nhận thức cao đẹp, có những hành động khiến người ta nghiêng mình, có những nét hồn nhiên của trẻ thơ, có những niềm xúc cảm đặc biệt trào lên khi đang đọc dỡ chương mà tôi không thể kể cho bạn nghe được, bạn cũng không thể nào biết được, cảm được - trừ khi bạn đọc 98 trang đầu sách như tôi.
Một lần nữa tôi chắc chắn sự đặc biệt của quyển sách và từ 'rất rất hay' không thể nào truyền đạt hết giá trị mà quyển sách mang lại. ! .
5
41250
2014-06-27 19:36:42
--------------------------
