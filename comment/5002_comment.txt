537877
5002
Cuốn sách được dịch quá tệ, cứ như quăng vô google dịch vậy. Tên nhân vật quá lộn xộn, câu văn không đủ nghĩa. Không rõ bản gốc thế nào, nhưng bản dịch khiến mình không hiểu nổi tại sao nó thành best seller nữa. 
2
1360304
2017-03-08 18:01:08
--------------------------
523505
5002
Mình không biết liệu có phải lỗi của người dịch hay của nhà xuất bản.Nhưng mà cách trình bày nội dung,cách sử dụng ngữ pháp và đặc biệt là cách dịch của cuốn sách là không ổn.Mình cảm giác cuốn sách không thể hiện sự hài hước và phong cách viết của tác giả ban đầu.Sách thiếu chú thích một cách trầm trọng,sears;barnes and noble;... đều ko hiện diện tại việt nam nên cần một chú thích nhỏ dưới đáy trang hoặc cuối sách.Vẫn còn một số lỗi ngữ pháp nhỏ nhưng quả thật khó thể hiểu với cuốn sách thứ tư !Cách dịch thật quá lười biếng,mình cảm giác bản dịch này như đến từ google dịch và chỉ được chau truốt và sửa lỗi.Lí do mình nói vậy là vì cách sử dụng câu từ trong quyển này không giống với tiếng việt mà giống với tiếng anh hơn.Nói nôm na nó QUÁ giống bản tiếng anh và nhiều chỗ còn thậm chí thiếu cả cách diện đạt mà tác giả cho vào ban đầu,khiến cho cách kể chuyện quad nhàm chán trong khi bản tiếng anh lại đầy đủ hơn rất nhiều,bạn sẽ nhận ra ngay khi đọc đến tầm trang 70.Cách dịch này có thể nói là quá "lười" và thiếu muối theo ý kiến của mình.Người dịch theo mình nghĩ,chắc chắn không có kiến thức về việc kinh doanh và 90% không tìm hiểu về những gì có trong sách.Ví dụ điển hình là khi người dịch dịch thành phố Menlo Park thành công viên Menlo và ném vào quyển sách một câu rất vô duyên:"...công ty đặt trụ sở tại công viên Menlo".Còn nhiều vd khác nữa nhưng bạn có thể tự tìm hiểu khi đọc quyển sách.Túm váy lại thì cuốn sách này có những điểm trừ là -Cách diễn đạt ,-Cách sử dụng ngữ pháp, -Cách dịch, -Còn nhiều chố người dịch không hiểu ý tác giả và -Thiếu chú thích một cách trầm trọng.Xin cảm ơn ~
2
481684
2017-02-11 18:35:18
--------------------------
477825
5002
Amazon.com là một web thương mai điện tử vô cùng nổi tiếng. Trước đến giờ chưa bao giờ nghĩ một cty muốn hoạt động phải cần nhiều thứ và am hiểu mọi lĩnh vực như vậy. Trở thành người giàu nhất đứng thứ 3 thế giới vượt mặt Warran Buffet. Bezos làm tôi dường như mở rộng tầm mắt, tôi định vị được mình đang cần gì cho kế hoạch bản thân, và cuốn sách làm tôi hiểu rõ hơn về hoạt động của một công ty. Ông thật sự rất tự tin và lạc quan với mọi bước đi của bản thân. Cuốn sách rất đáng đọc...rất đáng 
5
130605
2016-07-29 02:35:10
--------------------------
458977
5002
Cuốn sách tóm gọn lịch sử trang thương mại điện tử tiên phong lớn nhất thế giới amazon.com, nêu lên những thành công cũng như thất bại trong quá trình khởi nghiệp, khắc họa chân dung cha đẻ của Amazon - Jeff Bezos. Đây sẽ là những bài học vô cùng quý báu và những kinh nghiệm xương máu cho những ai đã, đang và sẽ làm về thương mại điện tử. Sách rất dày nhưng Tiki lại bán với giá rất rẻ, chất liệu giấy mềm mại, chất lượng in tốt. Mình rất hài lòng về cuốn sách này.
5
1016856
2016-06-25 17:49:54
--------------------------
452990
5002
Sau khi đọc Jeff Bezos Và Kỷ Nguyên Amazon đã giúp tôi hiễu ra rằng để gây dựng nên một doanh nghiệp thành công phải trải qua những khó khăn gian khổ, bên cạnh đó cần phải có sự nổ lực hết mình, kiên trì trước những thất bại trước mắt để gặt hái được những thành công cho bản thân cũng như cho doanh nghiệp. 
Tại sao dưới cái tên Amazon lại có một cái mũi tên và giống như hình mặt cười như vậy. Bởi nó thể hiện sự chu toàn và cống hiến hết mình của một doanh nghiệp là phải đi từ A đến Z. 
Sách rất có ý nghĩa cho những bạn trẻ có ước mơ thành lập công ty, doanh nghiệp.
5
1178691
2016-06-20 22:33:28
--------------------------
448602
5002
Jeff Bezos quá xuất sắc, thông minh và đầy tỉnh táo, sáng tạo, đầy chiến lược, bản lĩnh, quyết đoán trong mọi thời điểm. Đây có thể nói là một cuốn sách gối đầu giường cho mỗi người, động lực cho những ai đang có ý định kinh doanh về ngành bán lẻ. Qua đó ta có thể biết được lý do tại sao mà Amazon.com là trang web thương mại điện tử lớn nhất trên thế giới hiện nay, và nó xứng đáng được như vậy qua bao nhiêu thử thách cam go, khó khăn vất vả hiện nay.
5
172185
2016-06-16 11:18:13
--------------------------
424289
5002
Nội dung cuốn sách điểm qua các thời kỳ của amazon.com - website tiên phong trong thương mại điện tử từ lúc thành lập đến hiện tại, cho chúng ta biết được những quyết định dẫn đến thành công cũng như thất bại của công ty này. Những ai quan tâm đến thương mại điện tử không nên bỏ qua, vì bạn có thể tìm được những vấn đề cũng như gợi ý về giải pháp mà bạn đã hoặc sẽ gặp phải.
Nhưng điều tôi thích ở cuốn sách là việc khắc hoạ chân dung của Jeff Bezos với tính cách độc tài, tầm nhìn của một thiên tài cộng với nỗi ám ảnh về trải nghiệm khách hàng của ông. 
Với 2 thiên tài trong lĩnh vực công nghệ là Steve Jobs và Jeff Bezos có tính cách khá tương đồng nhau. Những quyết định được coi là có tầm nhìn xa của 2 vị này không đến từ những cuộc khảo sát hay những cuộc thảo luận nhóm nào cả, mà đến từ cảm nhận của cá nhân mình, không cần chứng minh, không cần giải thích để thuyết phục người khác. Nửa tá quyết định của các ông cũng thất bại, để thấy dù được coi là thiên tài trong lĩnh vực của mình chuyện thất bại cũng là bình thường, sự khác biệt ở đây có chăng là nhận ra sai lầm để sửa sai nhanh chóng, vậy sao chúng ta không dám quyết định nhỉ?
Một điều đặc biệt thú vị là sự ám ảnh của ông về "cảm giác trải nghiệm của khách hàng". Một trong những quyết định nổi tiếng đó là ông quyết định dừng email marketing - phương thức kiếm hàng trăm triệu đô cho công ty vì phàn nàn của một khách hàng. Ông đọc tất cả email gửi về jeff@amazon.com, nếu trong số đó có email khiếu nại mà ông cho là nghiêm trọng, thì nhân viên phụ trách liên quan phải dừng tất cả các công việc lại dù quan trọng đến đâu để giải quyết và giải trình về nội dung bức thư.
Nếu mỗi chúng ta đều có ám ảnh về về công việc của mình như vậy thì chắc khó mà thất bại được
5
3844
2016-05-01 12:13:25
--------------------------
422390
5002
Nếu bạn muốn khởi nghiệp thì đây là 1 sự lựa chọn tốt. Về triết lí lãnh đạo và tính cách của jeff bezos được minh họa một cách rất rõ ràng. Song song với nó là quá trình, biến cố, phát triển của tập đoàn amazon được mô tả khá rõ ràng, cách giải quyết khó khăn, cách đối nhân xử thế,... tôi đã học được rất nhiều từ việc ra quyết định cho đến quản lý công ty của bản thân. Khi đọc các bạn nên đọc chậm để có thể hình dung được 1 cách rõ ràng chứ không nên chỉ đọc lướt qua.

4
190211
2016-04-27 08:13:38
--------------------------
389372
5002
Các bạn đang muốn kinh doanh trên internet nhất thiết phải đọc cuốn sách này! Sách này cùng với cuốn Tỷ phú bán giày là những tác phẩm khá hay. Qua cuốn sách này chúng ta sẽ hiểu thêm về việc kinh doanh trên internet khốc liệt như thế nào. Có rất nhiều trang Web bán hàng trên internet nhưng vì sao chỉ có một số ít là thành công trong khi phần lớn là không hiệu quả. Các bạn sẽ hiểu "quả cầu tuyết" có tác dụng lớn như thế nào trong kinh doanh. Nội dung sách thì không phải bàn cãi, nhưng có một điểm trừ đó là sách in có rất nhiều chữ bị mờ, thực sự với một người  yêu sách và quý trọng sách như mình thì không hài lòng.
4
944188
2016-03-01 22:08:33
--------------------------
364625
5002
Trong thời đại hiện nay, nếu đã dùng Internet thì chắc không ai là không biết đến Amazon, trang web bán hàng trực tuyến lớn nhất thế giới. Và người sáng lập, điều hành không ai khác chính là Jeff Bezos. Khả năng quản lý và cải tiến của người đàn ông này thật khủng khiếp. Đặc biệt ông có con mắt suy xét sản phẩm của công ty từ góc độ của khách hàng, do đó ông luôn liên tục cải tiến trang web và cách phục vụ khách hàng như dịch vụ Amazon Prime giao hàng trong 2 ngày.
5
465332
2016-01-06 15:46:03
--------------------------
358448
5002
1. Hình thức: giấy tạm ổn, bìa dày, font chữ đễ đọc, sách khá nặng nên nếu ai nằm đọc sẽ rất là mỏi tay
2. Nội dung: Đây là một cuốn sách viết về kinh doanh rất hay, miêu tả con người Jeff Bezos rất chân thật và đầy sức cuốn hút. Chúng ta sẽ học được rất nhiều điều từ các sự kiện liên quan đến Jeff Bezos.
Tuy nhiên đây là một cuốn sách về kinh doanh cho nên đôi khi bạn đọc sẽ cảm thấy hơi khô khan và buồn ngủ. 
Kết luận: Sách hay, có rất nhiều bài học cho chúng ta, sách kén người đọc
4
842121
2015-12-25 11:49:28
--------------------------
353715
5002
Có thể nói Amazon là một trong những mô hình thương mại điện tử có thể nói là thành công nhất hiện nay trên toàn cầu và Tiki cũng đang trên con đường phát triển giống như cái cách Amazon đã làm. Mình đang rất hứng thú với lĩnh vực thương mại điện tử nên không ngần ngại mua cuốn này khi phát hiện nó nói về thương hiệu và đề tài mình đang quan tâm. Hiện nay, chưa có sách nào về thương mại điện từ mà mình thích hơn cuốn này trừ sách chuyên ngành. Sách tóm lược lại quá trình sinh ra và phát triển của Amazon dưới sự lãnh đạo tài tình của Jeff - một phong thái lãnh đạo tuyệt vời cùng tầm nhìn vĩ đại.
Khổ sách lớn nên chữ lớn rất dễ đọc, trình bày rất thích. Nếu ai có quan tâm đến thương mại điện từ thì đây là cuốn sách nên đọc.
5
55927
2015-12-16 19:22:20
--------------------------
337672
5002
Một điểm trừ cho cuốn sách là phần giới thiệu tác giả hơi dài dòng! Nổi bật nhất trong tác phẩm là khả năng lãnh đạo tài tình, tư duy định hướng dự đoán của Jeff Bezos! Mình học kinh doanh nên mua sách này về đọc, đút kết cho bản thân những kinh nghiệm , phục vụ cho việc kinh doanh của tương lai! Mình không quan trọng hóa hay đào sâu cách viết của tác giả, nhưng những gì mình đọc sơ qua thì thật sự ngưỡng mộ ông Jeff! Cảm ơn tiki, dù đơn hàng giao quá trễ, mình thông cảm được vì chị nhân viên có gọi điện xin lỗi! Tiki lớn mạnh, trở thành Amazon của Vn nhé!
5
370905
2015-11-14 14:19:57
--------------------------
314016
5002
nếu bạn là 1 fan của tiki ngay từ những ngày đầu thì đọc hơn nửa cuốn sách bạn sẽ nhận ra tiki đang đi những bước đi của Amazon 1 cách khéo léo và khôn ngoan.Điều đáng quý nhất trong tư tưởng của Jeff xuyên suốt cuốn sách là mong muốn đem lại cho khách hàng trải nghiệm mua sách và giá trị lớn nhất , mặc dù đôi lúc nó có hơi tàn nhẫn 
Để làm được như Amazon.com nó không những đòi hỏi 1 tầm nhìn đi trước thời đại mà cả sự tận tâm với công việc và khách hàng của những bộ óc tuyệt vời. Một cái nhìn khá toàn diện về thương mại điện tử và Amazon.com
good book !! should read
4
114124
2015-09-25 10:01:52
--------------------------
312847
5002
Tôi mua cuốn sách này, bởi vì tôi bị mê hoặc bởi trang web AMAZON và huyền thoại của nó. Trước khi thương mại điện tử ở Việt Nam phát triển, thì chúng tôi đã học về thương mại điện tử trên ghế nhà trường, từ năm đầu của thập kỷ 2000. Và tôi đã mua hàng từ Amazon, từ những cuốn sách đoạt giải Nobel kinh tế những năm đó, cho tới các sản phẩm gia dung, đặc biệt là sản phẩm cho con mình gần đây. Tôi chỉ có thể nói, Amazon thật tuyệt vời! Tuy nhiên, tôi chả biết gì về Amazon và ông chủ của nó, một cậu bé đã từng là học sinh "cá biệt". Đọc cuốn sách này, tôi mới hiểu được hơn về lịch sử hình thành và quá trình hoạt động của bộ máy khổng lồ Amazon. Tôi nghĩ, dù mình không làm về ngành thương mại điện tử, nhưng nó giúp cho tôi có cái nhìn chân thực về nó. 
5
572177
2015-09-22 10:58:26
--------------------------
306669
5002
Đây không phải là một cuốn sách mang tính lý thuyết như những cuốn cách học làm giàu khác, mà đây là một câu chuyện chân thực về hành trình của Bezos trong quá trình lên ý tưởng và dẫn Amazon tiến tới thành công. 
Tuy nhiên, có một vài chi tiết đầu truyện hơi lan man một chút vì giới thiệu quá nhiều nhân vật, không đi vào trọng tâm (với tính chất là câu chuyện do một người khác kể lại chứ không phải do chính Bezos viết), nên gây cảm giác nhàm chán. Thực sự vào đến giữa sách mình mới bắt đầu có hứng thú và đi theo hết mạch câu chuyện.
Nếu bạn đang có ý định đi theo lĩnh vực thương mại điện tử thì có thể học được nhiều bài học quý giá từ cuốn sách này, về cách thức làm việc, vượt qua khó khăn, cũng như sự kiên định đi theo ý tưởng đã vạch ra từ đầu của ông. Đây thực sự là một câu chuyện truyền cảm hứng!

5
585310
2015-09-17 18:38:18
--------------------------
295099
5002
Là một người tuyên phong trong một lĩnh vực kinh doanh vô cùng mới trong cuộc bùng nổ của các công ty dotcom đó là thương mại điện tử . Amazon đã trở thành tượng đài trong lĩnh vực này và là hình mẫu cho những công ty thương mại điện tử khác học tập .
Quyển sách nói về thời kì thịnh vượng mà tác giả gọi là "Kỷ Nguyên" của Amazon cùng với Jeff Bezos,cha đẻ của nó . Qua quyển sách bạn sẽ thấy Jeff Bezos khá giống với thiên tài công nghệ Steve Jobs , như một vị phù thủy có thể vẻ ra những định hướng vô cùng chính xác .
Một quyển sách hay về thương mại điện tử mà mọi người nên đọc .

5
554150
2015-09-09 23:13:54
--------------------------
292537
5002
Tác giả dựa trên những bài phỏng vấn trực tiếp để viết về Jeff và Amazon. Nếu bạn đã từng đọc qua tiểu sử của Bill Gates và Steve Jobs thì đây là một cuốn sách tiếp theo nên đọc. Jeff là một người có ảnh hưởng rất lớn đến thương mại điện tử trên thế giới và cũng là 1 trong những người châm ngòi cho cuộc chiến thương mại điện tử như ngày nay ở mọi quốc gia. Có nhiều tập đoàn lớn trên thế giới chịu ảnh hưởng từ văn hóa, cung cách làm việc của Amazon. Hay nói cách khác Jeff là 1 trong những người anh cả của thương mại điện tử mà kinh nghiệm và câu chuyện của anh xứng đáng được mọi người biết đến!
5
668704
2015-09-07 14:02:24
--------------------------
290612
5002
Tò mò và mua Jeff Bezos Và Kỷ Nguyên Amazon khi gần đây có rất nhiều thông tin về Amazon như môi trường làm việc hay Fire phone. Jeff Bezos là xây dựng Amazon không chỉ trở thành một công ty bán lẻ siêu hạng mà còn là 1 công ty công nghệ khá thành công.
Dù hiện nay nhiều chính sách quan điểm của Jeff không được nhiều người ủng hộ nhưng cuốn sách mang đến những câu chuyện, kinh nghiệm mà Jeff đã trải qua khi chèo lái con thuyền Amazon vượt qua biển lớn khó khăn. Cuốn sách hữu ích cho những người hướng đến con đường kinh doanh.
5
512469
2015-09-05 16:27:56
--------------------------
290275
5002
Cuốn sách là sự tóm lược những giai đoạn thăng trầm của Amazon một cách chân thực đến mức khá là trần trụi. Những ai đam mê muốn tìm hiểu về Amazon nói riêng và thương mại điện tử nói chung đều có thể tìm được rất nhiều thông tin quý giá trong cuốn sách. Quá trình triển khai một công ty thương mại điện tử, những thách thức gặp phải, những bài toán phải giải đáp và những mục tiêu tối quan trọng cần hướng tới, tất cả đều được thể hiện rõ nét trong từng trang sách, theo từng giai đoạn hành trình của Amazon. Bên cạnh đó, tôi thực sự giật mình và bất ngờ trước những thông tin chứa đựng trong cuốn sách. Suốt thời gian đọc, tôi cảm giác như đang ngồi trên một con tàu giữa một cơn bão biển. Phải nói là Jeff Bezos có một tầm nhìn xa tới kinh ngạc và mức tập trung cao độ tới mức khắc nghiệt để đưa được con tàu Amazon vượt qua muôn vàn sóng gió như vậy. Cuốn sách cũng cho người đọc biết được về tính khốc liệt của thị trường thương mại điện tử, nơi mà để đổi lại những trải nghiệm tuyệt vời của khách hàng là sự đấu tranh không mệt mỏi của bản thân Amazon và các đối tác. Thậm chí, đó còn là sự đau đớn tới mức hủy diệt của những đối tác là nhà xuất bản và bán lẻ sách truyền thống. Thực sự ngưỡng mộ cách mà Amazon dồn toàn nguồn lực cho việc cải thiện trải nghiệm mua sắm của khách hàng. Nhưng góc độ cá nhân, tôi không thích thái độ và quan điểm "báo đốm và linh dương" của công ty này trong mối quan hệ với các nhà cung cấp. Nhưng nếu không có những cái đầu lạnh như Jeff Bezos và những người ở Amazon thì chắc là cũng không có nền tảng thương mại điện tử phát triển mạnh mẽ như ngày hôm nay. 
5
11085
2015-09-05 10:54:47
--------------------------
290022
5002
Jeff Bazos là một trong những nhân vật có tầm ảnh hưởng nhất thời đại internet hiện tại, bên cạnh những gì Amazon đang thực hiện, mà còn nhờ những dự án lớn lao sắp tới. Quyển sách đã khắc hoạ chân dung Bazos từ nhiều góc độ, cùng với đó là sự phát triển của Amazon, thông qua con người, thành tựu đạt được và triết lí kinh doanh. Thông qua những câu chuyện về cách Amazon vượt qua khó khăn, mỗi người đọc đều cảm thấy học hỏi được từ Bazos cho cá nhân cũng như doanh nghiệp của mình. Tuy nhiên, khác với tự truyện Steve Jobs, quyển sách này được viết dựa trên những bài phỏng vấn với Bazos và người thân quen chứ không phải cuốn tiểu sử về Bazos được ông trực tiếp đóng góp thông tin nên một số chi tiết có thể không phản ánh đúng thực tế.
5
561173
2015-09-05 00:58:35
--------------------------
288359
5002
Jeff Bezos - cha đẻ của một cửa hàng online đồ sộ nổi tiếng khắp thế giới. Ông đã lập nên một 'kỷ nguyên Amazon' và kỷ nguyên Amazon khổng lồ này cũng đã khẳng định tên tuổi của ông. Cách ông giải quyết hay xử lí một vấn đề khó khăn thật sự rất thông minh, khéo léo và rất rất logic. Từ những khó khăn khi mới ban đầu lập nghiệp cho đến những vấn đề lớn hơn khi việc kinh doanh được mở rộng, ông đã giúp cho lớp trẻ thấy được nhiều kinh nghiệm khi chọn cho mình một khởi đầu về kinh doanh. 
5
136569
2015-09-03 15:32:32
--------------------------
287165
5002
Đây là một cuốn sách hay và thú vị cho những ai muốn khởi nghiệp. Sách đã khắc hoạ đầy đủ chân dung và cuộc đời của Jeff Bezos - một CEO đầy tài năng với nụ cười lớn nổi tiếng, từ lúc đi làm thuê cho đến lúc khởi nghiệp với cửa hàng sách trực tuyến Amazon và sau này là cửa hàng triệu đồ Amazon. Có những lúc Amazon đã thua lỗ, nhưng với một tầm nhìn chiến lược và luôn kiên định với mục tiêu của mình, Jeff Bezos đã lèo lái con thuyền Amazon vượt qua những khó khăn, trở ngại để đi đến những thành công. 
5
57405
2015-09-02 12:51:31
--------------------------
286852
5002
"Jeff Bezos Và Kỷ Nguyên Amazon" là cuốn sách cần phải được đọc nhiều lần, nghiền ngẫm những suy nghĩ, cách xử lý của Jeff Bezos. Ông giải quyết vấn đề một cách rất logic, khéo léo. Ý tưởng thành lập amazon cũng rất tình cờ đối với ông, và ông đã bắt tay vào thực hiện ý tưởng của mình. Trải qua bao nhiêu thăng trầm, gian nan từ ngày thành lập, cho đến nay Amazon đã xây dựng nên một kỷ nguyên mới về sách. Đối với những ai chuẩn bị lập nghiệp thì đây là một cuốn sách không thể thiếu, hãy học những cách mà Jeff Bezos đã làm để có thể giúp cho doanh nghiệp vượt qua giai đoạn khó khăn như ông đã làm.
4
156324
2015-09-02 01:00:34
--------------------------
275826
5002
Quyển khách có độ dày trung bình (khoảng 400 trang) tuy nhiên lại được in khổ lớn nên không thể đọc nhanh được. Thông tin của sách theo mình nghĩ là khá toàn diện khi trải dài từ khi Jeff Bezos còn bé, cho đến khi ông đi làm thuê, rồi mở Amazon, rồi những thời khắc mà Amazon đối mặt với những vấn đề sinh tử.
Quyển sách này dĩ nhiên hữu ích nhất với người khởi nghiệp kinh doanh. Bạn có thể học được rất nhiều từ các Jeff Bezos tư duy và ra quyết định (cứ xem cái cách ông chọn vợ sẽ thấy tư duy của ông logic đến mức nào).
Đối với các bạn không làm kinh doanh, quyển sách sẽ hữu ích ở khía cạnh khác, tạm gọi là khía cạnh "kỹ năng sống". Cái cách ông kiên trì và vượt khó xứng đáng là bài học lớn cho hành trình theo đuổi lý tưởng.
4
134241
2015-08-23 11:42:28
--------------------------
266773
5002
 Có nhiều quyển sách viết về Amazon nhưng với mình tâm đắc nhất ở quyển này là tính cách cá nhân trong công việc & trong cuộc sống của Jeff Bezos được khắc hoạ rõ nhất. Bên cạnh đó, những biến cố, những cột mốc của Amazon được miêu tả lại trực quan sinh động; ngoài ra có vô vàn trường hợp trong kinh doanh mà Amazon gặp phải & cách họ đã giải quyết như thế nào, thật đáng để học hỏi. Với mình, những kiến thức này vẫn còn rất hữu dụng ở thị trường Việt Nam, cực kỳ khuyến khích bất kỳ ai đã & đang làm TMĐT nên tìm đọc quyển này.

Về tính cách cá nhân của Jeff Bezos có nhiều điểm đáng lưu ý:

1. Ông ấy khởi nghiệp ở độ tuổi chín muồi của sự nghiệp, sau một thời gian dài đi làm tích luỹ kinh nghiệm, mối quan hệ và tiền bạc. Tư duy toán học mang đến cho Jeff nhiều lợi thế khi làm kinh doanh: thực tế, tập trung vào con số hiệu quả, tính toán các chỉ số khác nhau để kiểm chứng giả thuyết (hơi ngớ ngẩn là có 1 phép tính đơn giản sai lệch và nhờ có nó mà có Amazon ngày nay). Mối quan hệ với các nhà đầu tư trong lĩnh vực tài chính giúp ông ấy có được những nguồn tiền đầu tư đúng lúc và may mắn cứu công ty thoát khỏi vực thẳm phải đến những hơn 10 lần. "May mắn cũng là một dạng tài năng" hơi bị đúng trong trường hợp này.

2. Jeff rất quyết đoán và "máu lạnh". Sự bình tĩnh của Jeff (và đôi lúc là sự độc đoán, ích kỷ tiêu cực) đã giúp Amazon tồn tại qua những giai đoạn khó khăn nhất. Khi tất cả mọi người đều nghi ngờ những việc ông làm, điều duy nhất ông đã phản ứng lại lúc đó, là ông cho họ thấy lòng tin của ông vào chính mình lớn hơn cả thảy và không một chút run sợ hay nao núng. Có lẽ để đạt được tới cảnh giới này phải thực sự bỏ ngoài tai tất cả lời người khác nói về mình hay xã hội đánh giá về mình như thế nào.

3. Jeff rất thực dụng, từ việc chọn vợ (có hẳn xác xuất để chọn được người bạn đời thông minh đàng hoàng nhé), đến việc chọn những người cộng sự ban đầu, cũng cho thấy rằng mọi việc của ông đều có sự tính toán từ trước. Trừ vợ ra, những người được cho là đồng sáng lập với Jeff thời gian ban đầu ở Amazon cũng đều được Jeff cân nhắc và tính toán "tiễn" họ ra đi vào đúng từng thời điểm công ty chuyển mình. Bởi vì.. xem số (4)

4. Ông rất đề cao tính công nghệ và khả năng sáng tạo. Ông cho rằng Amazon được xây dựng trở thành một công ty công nghệ chứ không phải là công ty bán lẻ trực tuyến. Ông luôn muốn những lớp nhân viên kế thừa được tuyển dụng về sau phải luôn giỏi hơn người trước để nguồn nhân lực phải tự tranh đấu, học hỏi để giỏi hơn mỗi ngày. Ông không thích người thoả mãn với một kết quả nhất định, Ông đòi hỏi rất cao ở tất cả mọi người trong Amazon và bản thân mình, không được ngừng sáng tạo, ông cần sự đổi mới (innovation), ông cần một cuộc cách mạng thay đổi liên tục.

5. Ông không tin vào khái niệm Work - Life balance. Đối với ông, hai thứ đó là một và cần phải quyết liệt với những quyết định của mình đưa ra.

6. Bản thân ông cũng không hẳn là một nhà lãnh đạo tài ba, nhưng hình ảnh Jeff Bezos được phác hoạ trong sách này rất rõ lão là một tay buôn thứ thiệt, giữ bí mật kinh doanh, giữ suy nghĩ của mình và không chia sẻ với bất kỳ ai kể cả người thân thiết nhất. Jeff chưa bao giờ là nhà điều hành giỏi như trong quyển sách này miêu tả, ông cũng không được lòng nhân viên về sau.

7. Bonus: KLQ tới tính cách cá nhân nhưng ông này đầu tư nhỏ lẻ vào các công ty Internet trước thời kỳ bong bóng dot-com diễn ra khá nhiều. Ông được hưởng lợi từ một số công ty thành công, một trong số đó có Google.

Về Amazon:

1. 5 năm đầu khởi nghiệp của Amazon được miêu tả rất chi tiết & rõ ràng, tìm hiểu kỹ hơn đó cũng sẽ là hình ảnh phản ánh 5 năm đầu khởi nghiệp của bạn: sẽ có lúc công ty phát triển nóng, phát triển tới mức không kiểm soát được; sẽ có những lúc công ty rơi vào hố đen bế tắc tưởng không thoát nổi; sẽ có những giai đoạn khủng hoảng niềm tin; sẽ có vô vàn vấn đề giữa founder & middle managers (các công ty ở VN hiện nay cũng đang gặp phải); sẽ có những lúc công ty không còn khả năng thanh toán tiền mặt và rơi vào trạng thái nợ nần; sẽ có những khoảnh khắc người đồng sáng lập rời bỏ đi với nhiều điều tiếng ở lại,... Rốt cuộc thì ở mỗi vấn đề đó, Amazon đã giải quyết như thế nào? Tìm câu trả lời trong sách nhé
grin emoticon

2. Các khoản lỗ lớn nhất của Amazon rơi vào thời điểm trước bong bóng dot-com diễn ra: những thương vụ mua bán sáp nhập với giá trị cao ngất ngưỡng (bong bóng xì hơi thì các công ty được đưa về giá trị thật); các khoản thu mua để độc quyền phân phối một số mặt hàng trọng điểm như dịp lễ Giáng sinh (và rồi ôm hàng thua lỗ),...

3. Từng đau đầu và tranh cãi quyết liệt vào những lúc cho ra đời sản phẩm mới hay ý tưởng mới. Có lẽ nhiều công ty TMDT trong nước cũng từng gặp vấn đề này nhất là khi mò kim đáy bể giữa thị trường mênh mông rộng lớn khi chưa biết đúng - sai và tính hiệu quả như thế nào? Amazon đã giải quyết vấn đề này thiệt hay, thiệt trọn vẹn cho cả đôi bên và một lần nữa phải ngả mũ thán phục bạn Jeff ở tài linh hoạt của bạn ấy.

4. Kết thân với các chuỗi bán lẻ và học THẬT NHIỀU THỨ từ việc bán lẻ thông thường để áp dụng cho Amazon. Họ học từ công nghệ quản lý kho bãi, đến cách bày trí trong các siêu thị, vận chuyển kho hàng, cách khách hàng suy nghĩ, logic trong hành vi mua hàng thường gặp,... Amazon mời những người kỳ cựu ở BestBuy, Walmart,.. về làm việc cùng với mình. Để "săn" được những người này, chưa từng là điều dễ dàng, bạn Jeff là một thiên tài thương thuyết và đàm phán, chưa kể rất biết cách quan sát và tiếp cận những đối tượng mục tiêu của mình bằng những cách không thể tưởng tượng được
grin emoticon

Tóm gọn lại học được từ Amazon: Hãy học mơ mộng và suy nghĩ như một công ty bán lẻ đồ sộ nhưng để công nghệ hiện thực hoá giấc mơ ấy thành con số.
5
3401
2015-08-14 22:55:42
--------------------------
259662
5002
Nếu bạn muốn khởi nghiệp, đầy là một trong những cuốn sách phải đọc. Dù chỉ là thông qua câu chữ, người đọc có thể cảm nhân được sự vất vả và ý chí phi thường của những nhà khởi nghiệp để vượt qua mọi thách thức. Amazon với sự dẫn dắt của Jeff Bezos đã trở nên thật vĩ đại, nhờ tầm nhìn chiến lược đúng đắn và sự kiên định để đạt được mục tiêu. 

Cuốn sách vô cùng thực tế, có thể phá mộng của những kẻ nghĩ kinh doanh đầy màu hồng. Nhưng cùng với đó là nguồn cảm hứng vô tận cho những ai sẵn sang nhảy vào biển lửa thương trường. 

4
172669
2015-08-09 18:31:27
--------------------------
255606
5002
Cái tên Jeff Bezos, đó chính là lý do khiến tôi phải đạt mua quyển sách ”Jeff Bezos và kỷ nguyên Amazon” này trên Tiki. Và khi nhận được quyển sách này trong tay, tôi thấy quyển sách này bìa sách khá đẹp, nội dung trong quyển sách này đề cập đến cuộc đời của Jeff Bezos và sự hình thành cũng như phát triển của công ty Amazon. Đã cho rất nhiều sách viết về chủ đề này nhưng đều không rõ ràng, chi tiết và đầy đủ, và quyển sách này đã giải quyết được vấn đề đó. Nhìn chung, theo tôi đây thật sự là một quyển sách rất đáng đển chúng ta mua về đọc.
4
42985
2015-08-06 10:33:13
--------------------------
254516
5002
Sau cuốn Dốc hết trái tim của CEO Howard Schultz, tôi tìm đến với Amazon của Bezos. Càng đọc "Jeff Bezos Và Kỷ Nguyên Amazon", tôi càng cảm thấy một Bezos vị kỉ, khắc nghiệt khắc hẳn với một hình ảnh của Howard Schultz - cha đẻ của Starbucks. Nếu như ở Starbuck, tôi nhận thấy sự cầu toàn đến từ tình yêu dành cho cafe và sự cầu toàn đến tuyệt đối với chất lượng và trải nghiệm cafe của Howard dẫn đến thành công của thương hiệu Starbucks. 
Ở Amazon, sự cầu toàn đến từ bộ óc với tầm nhìn vĩ đại, tham vọng và siêu việt của Bezos. Một chiến lược bán hàng luôn hướng đến khách hàng và đằng sau đó là một giải pháp kinh doanh theo quan điểm "bánh đà xe đẩy" của CEO Bezos. Một con người đặc biệt với nụ cười lớn, lấn át và quyền lực, một chính sách tiết kiệm đến hà khắc với nhân viên và cả bộ máy hoạt động của Amazon, một con người luôn bị ám ảnh bởi công nghệ và cửa hàng triệu món
5
79649
2015-08-05 11:53:46
--------------------------
247590
5002
Tôi là mua hàng trên amazon rất nhiều, nhưng rất ít biết về người sáng lập Jeff Bezos hay những gì phía sau hậu trường trong sự hình thành và phát triển của công ty này. Trong khi Amazon giờ là một mô hình thương mại điện tử thành công , cuốn sách này cũng  trình bày chi tiết nhiều tính toán sai lầm và những sai lầm trong lịch sử của nó - hầu như tất cả đều do Jeff Bezos - ".  Jeff Bezos và kỷ nguyên Amazon chứa đựng nhiều ví dụ về sự khôn ngoan, sự kiêu ngạo, và cả những sai lầm ngớ ngẩn và cách  làm sẵn sàng chấp nhận rủi ro của Bezos.
4
65620
2015-07-30 11:22:48
--------------------------
240891
5002
Amazon - 1994 - Được đặt tên trùng vời con Sông lớn nhất thế giới. Thể hiện 1 Khát khao tham vọng vô cùng lớn của Jeff Bezos. 
Đây không chỉ là 1 Cuốn sách đơn thuần khô khan phát họa dòng chảy của con sông Amazon hùng vĩ mà nó vừa là Bài học của Khởi nghiệp, Bài học của sự thất bại, Bài học của sự Kiên định, Bài học của Tầm nhìn Lãnh Đạo ... 
Qua câu chuyên ngắn gọn này cho thấy 1 quy luật từ Cổ xưa cho đến nay và sẽ mãi tồn tại trong cuộc sống rằng: Con người cùng trên 1 con thuyền ra biển lớn thì chung tay trong tay vượt qua Sóng gió, bão táp. Đến khi Sóng yên biển lặng thì lại buông tay nhau ra ... ?
 Vì đâu nên Nỗi? Bối cảnh tạo ra? Hay chính chúng ta tạo ra?
"Giặc Ngoài còn chưa đánh mà đã mâu thuẫn Nội bộ thì chưa đánh đã Thua".
Làm sao mà Jeff Bezos lại vượt qua những cuộc Khủng hoảng Lãnh đạo Nội bộ vô cùng khốc liệt? khủng hoảng Lãnh đạo nội bộ  của 1 Doanh nghiệp còn lớn hơn rất rất nhiều Khủng hoảng Kinh tế nói chung. Nó có thể nhấm chìm mọi thứ!
5
374855
2015-07-24 23:46:12
--------------------------
240781
5002
Mình thật sự hâm mộ Jeff Bezos, người sáng lập đế chế bán lẻ huyền thoại Amazon, và qua cuốn sách, mình lại càng hiểu rõ hơn, sâu sắc hơn về con người tài năng này. Amazon là nơi luôn đặt khách hàng là trọng tâm, mọi dịch vụ đều dựa trên lợi ích mang lại cho khách hàng, ít ai biết rằng trong 4 năm đầu tiên hoạt động, Amazon không thu về lợi nhuận, ấy vậy mà nó đã phát triển mạnh mẽ đến ngày hôm nay. Điều đó chứng tỏ người lãnh đạo của nó phải cực kỳ tài năng và đặc biệt. Đọc cuốn sách, tôi đã thấy một người đàn ông rất mực quyết đoán với những ý tưởng tưởng như điên rồ mà lại đột phá, một con người chăm chỉ và luôn quyết tâm theo đuổi lý tưởng của mình. Lối kể chuyện của tác giả Brad Stone cũng là một phần nguyên nhân khiến tôi rất thích cuốn sách này. 
4
641578
2015-07-24 22:09:43
--------------------------
235585
5002
Rất may là tác phẩm này vẫn chưa được hoàn thiện hoàn toàn về lời dịch (theo cá nhân tôi), vì để có thể truyền tải hết mọi nội dung trong tác phẩm của Brad Stone đòi hỏi người dịch tốn rất nhiều công sức. Và nếu như tác phẩm này hoàn hảo hơn có lẽ số trang phải dày gấp hai lần ấn phẩm hiện tại. Nhưng dù sao, nội dung vẫn có thể giúp người đọc nắm được những tình tiết quan trọng trong lịch sử phát triển Amazon, những giai đoạn cốt yếu, những nhân vật cũng như những chiến lược mang tính lịch sử xây dựng nên công ty. Đây đơn thuần là một cuốn sách cung cấp cho ta nhiều thông tin hữu ích và chút gì đó động lực từ khí chất của Jeff, còn muốn thu thập thêm kiến thức và hiểu rõ hơn về Amazon thì hãy đọc lại nhiều lần quyển sách này và dĩ nhiên là phải bên cạnh một chiếc máy tính để sẵn sàng tra khảo thông tin khi cần thiết.
4
603059
2015-07-21 10:25:00
--------------------------
231018
5002
Nếu tự mình cố gắng làm việc phạm sai lầm và sửa sai thì sẽ rất khó khăn và tôn nhiều thời gian để có thể có được thành công. Chính vì vậy, để nhanh chóng học hỏi thêm được kiến thức và kinh nghiệm thì ai cũng cần phải có những người cố vấn. và "ai che lưng cho bạn" đã giúp độc giả biết cách duy trì mối quan hệ với những người cố vấn, tạo được sự thân thiện khi nói chuyện với mọi người để từ đó mọi người có thể chia sẻ kinh nghiệm, góp ý cho mình để mình có những quyết định đúng đắn hơn.
5
274812
2015-07-17 21:04:28
--------------------------
211611
5002
phải công nhận rằng jeff rất quyết đoán và kiên trì trong những quyết định của mình. Nhờ vậy ông đã tạo ra một Amazon - thương hiệu nổi tiếng khắp thế giới  mà Google phải e ngại. Sách rất hay. Nó như một cuốn lịch sử về Jeff và Amazon và cũng là cuốn sách đem đến cho nhiều người 1 cách nhìn khác về những con người thành công.
Bìa sách chất lượng tốt nhưng cách trình bày hình ảnh không được hay và tinh tế lắm. Đáng lẽ ra hình ảnh cái thùng nên đặt ở giữa và chiếm toàn bộ khung hình nhưng nó không như vậy. Ngoài ra, cuối trang sách còn in lỗi
4
323288
2015-06-20 20:49:47
--------------------------
204918
5002
Amazon.com là trang web thương mại điện tử lớn nhất trên thế giới hiện nay. Họ có kho hàng lớn nhất thế giới và tự hào tuyên bố rằng hàng hóa nào, mặt hàng gì họ cũng có. Lúc đầu tôi cho rằng họ kiêu ngạo và phách lối. Nhưng sau khi đọc song quyển sách mà tôi đang viết review cộng với một số tài liệu về amazon và ông chủ Jeff Bezos thì tôi mới biết rằng họ đúng. Amazon cho phép các công ty mở gian hàng trên website và thu tiền hoa hồng. Ông chủ của họ, tỷ phú Jeff Bezos là một người có tính cách, tư duy quản trị khá đặc biệt và ông đã cho thấy rằng những cách đó hiệu quả tới mức nào. Tác giả Brad Stone đã cho thấy sự tương đồng ở khả năng sáng tạo, tính ganh đua của Jeff Bezos rất giống với Bill Gates và điều này cũng được mang vào văn hóa của Amazon. Amazon tập trung hướng tới khách hàng, giúp khách hàng giải quyết vấn đề bằng sản phẩm của mình và điều này rất đáng để chúng ta học hỏi trong kinh doanh, đặc biệt là bán lẻ. Tất cả những điều cần biết về Amazon và nhà sáng lập của họ đều được tác giả Brad Stone làm sáng tỏ trong quyển sách này.
Đây là quyển sách rất đáng tham khảo với những ai đã, đang và sẽ kinh doanh thương mại điện tử.
5
313837
2015-06-05 11:28:05
--------------------------
197167
5002
Khi đọc cuốn sách, tôi nhận thấy sự tương đồng rõ nét của Tiki.vn và Amazon.com. Nhưng hơn thế nữa, để đưa amazon từ một cửa hàng bán sách Tiếng Anh trở thành một đế chế bán lẻ chúng ta không  thể không nhắc đến vai trò của Jeff Bezos. Với sự nhạy bén,táo bạo,tầm nhìn ,tham vọng cùng với cách ăn nói khéo léo, sự lãnh đạo tài tình Jeff Bezos ,Amazon đã trở thành gã khổng lồ trong nhiều lĩnh vực kể cả hàng không vũ trụ..Bên cạnh đó,cuốn sách còn cho thấy một tư duy kinh doanh không giống ai của Jeff Bezos như nguyên tắc “Hai chiếc Pizza” hay văn hóa xung đột để tạo ra sáng tạo..
5
542608
2015-05-17 07:38:56
--------------------------
183261
5002
Một quyển sách khá hay! quyển sách viết về nhà sáng lập Amazon- Jeff Bezos và quá trình hình thành cũng như phát triển của Amazon. Quyển sách dù chỉ là những dòng như phóng sự nhưng qua đó cũng làm cho người đọc thấy được những khó khăn, thách thức và cả mưu mô mà Jeff Bezos phải trải qua. Đồng thời, quyển sách tường thuật những nhân viên, những người từng tiếp xúc và làm việc cũng Bezos làm cho tác phẩm khách quan hơn! điểm trừ là viết hơi rườm rà, nhất là phần giới thiệu Jeff Bezos!
4
474969
2015-04-15 20:12:04
--------------------------
158455
5002
Theo nhận xét của cá nhân tôi, Amazon là hệ thống bán lẻ trực tuyến lớn nhất trên thế giới. Và người sáng lập Jeff Bezos theo tôi là một thiên tài trong kinh doanh. Ông quả thực là người có tài ăn nói và rất thông minh. Ông đề cao tính cạnh tranh khi xây dựng văn hóa Amazon, đề cao giá trị và vai trò của khách hàng đối với Amazon khi để riêng ra một chiếc ghế và bảo đó là chiếc ghế dành cho khách hàng trong mỗi cuộc họp của ban điều hành Amazon. Ông thường nói rằng sứ mệnh của Amazon là tập trung hướng tới khách hàng. Bezos và nhân viên thực sự tập trung hướng tới đem lại lợi ích cho khách hàng, nhưng đồng thời cũng cạnh tranh không ngừng với đối thủ và thậm chí với cả đối tác. Tất cả đã có trong tác phẩm Jeff Bezos Và Kỷ Nguyên Amazon của tác giả Brad Stone.
5
387632
2015-02-12 19:35:47
--------------------------
