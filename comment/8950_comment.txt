462520
8950
Tôi rất thích đi du lịch vòng quanh Thế Giới. Vì thế mà tôi luôn tìm đến những cuốn sách du lịch để đọc, với mục đích là du lịch qua từng trang sách. Cuốn sách này đã cung cấp khá đầy đủ thông tin về văn hóa con người của nước Úc. Tuy nhiên đây chỉ là một cuốn sách với mục đích cung cấp thông tin đến bạn đọc chứ không phải là những cuốn sách phượt của nhiều tác giả hiện nay. Vì thế thông tin đến bạn đọc đôi khi hơi khô khan, khó thấm. Nhưng dẫu sao đây cũng là một cuốn sách bổ ích.
4
174917
2016-06-28 17:27:41
--------------------------
415334
8950
Từ lâu mình đã rất thích nước Úc nên đã đặt mua quyển sách này như bổ sung vào bộ sưu tập của mình về đất nước yêu thích. Quyển sách về phần hình thức cũng khiến mình khá hài lòng, in màu khá đẹp và chất lượng giấy cũng tốt. Nội dung thì chỉ có thông tin khá căn bản thôi nên mình nghĩ quyển này phù hợp với đa số các bạn trẻ hoặc em nhỏ, đọc để có thông tin khái quát về một vùng đất mới. Còn với những ai kỳ vọng nhiều thông tin hữu ích về du lịch hoặc cuộc sống ở vùng đất xinh đẹp này thì quyển sách không thể đáp ứng được.

3
255882
2016-04-13 10:53:32
--------------------------
354623
8950
Mình là người có tình yêu to lớn với nước Úc nên đã đặt mua quyển sách này với hi vọng có thể tìm hiểu về đất nước yêu thích của mình nhiều hơn.
Ấn tượng đầu tiên của mình sau khi nhận được sách đó là chất giấy đẹp, hình ảnh màu sắc cũng không chê vào đâu được nhưng riêng phần nội dung thì lại sơ sài quá. Giá tiền so với nội dung quá chênh lệch. Mình đã hi vọng sẽ có thật nhiều trang viết về những thành phố lớn, ẩm thực và các địa điểm du lịch nổi tiếng nhưng rất tiếc là bên trong gần như chẳng nói nhiều về chúng. 
Hi vọng trong tương lai sẽ có những quyển sách về du lịch các nước như thế này, nhưng với lượng thông tin dồi dào hơn.
3
418919
2015-12-18 11:24:59
--------------------------
308503
8950
Không có thể nói gì hơn là trang bìa sách "Vòng quanh nước Úc" quá là đẹp! Đẹp lắm luôn! Mình sẵn lòng mua liền mà không phải đắn đo suy nghĩ. Khi mua về mình cũng rất hài lòng về nội dung quyển sách mang lại. Mình rất thích nước Úc và quyển sách này càng làm cho mình thêm yêu nước Úc nhiều hơn nữa. Về chất lượng giấy mình rất thích, hình ảnh rất là sắc nét, chữ rõ ràng - dễ đọc - đỡ chán. Giá một quyển mình thấy đắt lắm, cũng nhờ Tiki giảm một ít nên thấy cũng được. Cảm ơn Tiki!
5
558557
2015-09-18 18:52:37
--------------------------
291408
8950
Sách rất đẹp với thiết kế và trang trí cầu kỳ nhưng không rối mắt. Chất lượng giấy và bìa sách rất tốt, những hình ảnh sắc nét và chân thực về đất nước Úc, xứ sở của chuột túi và đà điểu Emu, của Nhà hát Cánh buồm,... Tuy nhiên, nội dung trong sách còn khá sơ sài và đơn giản. Cụ thể hơn, thông tin và lượng kiến thức có trong sách còn rất ít và đơn điệu. Nhìn chung, đây là một quyển sách khá đắt so với giá tiền 34000 đồng. Hy vọng sách sẽ được tái bản vào một ngày không xa với lượng kiến thức được bổ sung nhiều hơn.
3
299660
2015-09-06 11:25:25
--------------------------
249317
8950
Tôi mua cuốn sách này vì từ lâu đã muốn du lịch sang nước Úc.Đây là một cuốn sách ảnh và tôi hoàn toàn không biết cho đến khi cầm trên tay(lúc đó chưa có chức năng đọc thử).Sách chỉ có khoảng 30 trang hoặc hơn chút vậy mà giá 34k tôi thấy quá đắt.Tôi thấy cuốn sách chỉ đưa ra nét chung chung về nước Úc , tôi đang muốn du lịch Úc nên mới mua cuốn sách để hiểu thêm về đất nước này nhưng thông tin hoàn toàn chẳng có gì.Mong rằng tác giả sẽ có những tác phẩm đầy đủ nhiều thông tin hơn cho người đọc chứ không sơ sài và đơn điệu như thế này.
2
501195
2015-07-31 15:15:59
--------------------------
206347
8950
Vòng Quanh Thế Giới - Nước Úc là quyển sách rất thú vị khi cho mình khi muốn tích lũy những hiểu biết của mình về nước ngoài nói chung và nước Úc nói riêng. quyển sách này cho mình biết rất nhiều điều về lịch sử hình thành và phát triển của nước Úc. không những thế còn cho mình biết về các văn hóa ẩm thực, thể thao, quyền lực chính trị và nhân dân, cản quan và đời sống,...của con người nước Úc. phải nói đây là lựa chọn tuyệt với của mình khi muốn khám phá đất nước Úc.
5
363272
2015-06-09 14:26:46
--------------------------
