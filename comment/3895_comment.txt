412715
3895
Cuốn sách là hành trình đi tìm tình yêu cuộc sống bản thân của 4 con người 4 hoàn cảnh khác nhau. Một Iphone trẻ trung xinh xắn mang trong mình tình yêu với một chàng trai đã lâu không quay trở lại. Một gã Bốn Chín 28 tuổi nhưng vẫn chỉ ăn chơi đàn đúm phá gia chi sản. Một Tóm Tém đáng thương bị ông chồng say rượu đánh đập nhưng không thể bỏ được vì tình yêu quá lớn. Một Thuốc Lá vỏ ngoài cứng rắn bên trong lại yếu mềm mong manh, đem lòng yêu thương dành cho chàng cảnh sát Áo Xanh nhưng không thể nào nói ra chỉ vì cô chỉ là một đứa "bán hoa".
Truyện khá thú vị nhưng nó chỉ ấn tượng mạnh với tôi từ khi Thuốc Lá mất. Nếu những cuốn truyện khác ai cũng muốncó một kết cục tất cả cùng có hậu. Nhưng theo tôi thì việc Thuốc Lá mất chính là điều thích hợp nhất cho cả nhân vật và cả cuốn sách. Đây là lúc để các nhân vật bị đánh thức nhận ra những điều quý báu của cuộc sống mà họ đã bỏ lỡ. Đây cũng là chi tiết cảm động ý nghĩa khiến bạn đọc yêu thích câu chuyện hơn
4
910770
2016-04-08 10:44:23
--------------------------
352123
3895
Mình quyết định mua sau khi đọc được phản hồi khá tốt cũng như điểm sao trung bình gần như tuyệt đối Nhưng có lẽ truyện không như mình kỳ vọng. Lối văn kể dài dòng lê thê, từ cuộc sống, góc nhìn của nhân vật phụ, nghề nghiệp..v..v..  đến tả cảnh vật xoay quanh đó như thế nào, mình nghĩ nên kể sơ qua để người đọc nắm bắt hơn là tập trung quá nhiều về các vấn đề ấy mà không xoáy sâu vào cặp chính để làm nổi bật hơn. Với motip nữ chính xinh xắn đáng yêu, nam chính thiếu gia đẹp trai bất cần đời  vì nữ chính thay đổi có lẽ không hợp với mình Truyện có lẽ hợp với các bạn tuổi teen hơn. Còn bìa sách thì đẹp, tên truyện gây tò mò, chữ vừa mắt, không bị lem. 
2
187006
2015-12-13 20:14:23
--------------------------
334524
3895
Tác giả đã xây dựng các nhân vật của mình bằng những nét điển hình nhất của xã hội hiện đại. Một iPhone xinh đẹp "cuồng" Apple với niềm tin về mối tình đầu trong sáng, một Gã Chảnh 49 giàu có và thích chơi ngông, một người phụ nữ bình thường Tóc Tém và một Thuốc Lá dạn dày sương gió. Tất cả đều mang trong mình vết thương về mặt tinh thần nhưng vẫn tìm cách vượt qua nó.
Truyện hay còn ở lối viết văn nhẹ nhàng như đi thẳng vào tim người đọc.
Điểm tôi không thích ở truyện này chính là bìa. Trước đó tôi đã thấy bìa truyện này bên trang Gacsach.com (dù tác giả chỉ đăng tới chương 7) và rất ấn tượng với thiết kế bìa bên đó nên khi truyện được xuất bản tôi không thích thiết kế bìa lắm.
4
894652
2015-11-09 18:03:27
--------------------------
334089
3895
Đọc tiêu đề " Chuyện Iphone và gã chảnh mang mật danh 49 ", mình nghĩ đây là một cuốn ngôn tình sến sẩm cho mấy bé cấp ba cơ =)) nhưng vẫn mua. Về nhà đọc, thấy một hình dung khác hẳn. Từng nhân vật hiện ra, rõ nét dưới những biệt danh họ được đặt cho.
Đằng sau câu chuyện không chỉ là kết thúc đẹp của Iphone và 49, mà đó còn là những mảnh đời, những gặp gỡ để ta nhận ra trái đất tròn lắm, những người yêu thương nhau rồi sẽ gặp lại nhau.
Là Sữa Đá, là Tóc Tém, là Thuốc Lá, là Iphone, là 49, là Chanh Leo, là Áo xanh, là Sóc Nâu ... tất cả những con người ấy đã góp phần làm nên một Đà Lạt thập đẹp, mà nếu ai đã từng tới Đà Lạt dù chỉ một lần, sẽ không bao giờ quên được.
5
585214
2015-11-08 20:32:15
--------------------------
286407
3895
Cũng là câu chuyện tình yêu giữa những con người với nhau. Nhưng lại không ảo diệu như những truyện teen mới lớn. Tác giả trẻ nhưng có sự già đời trong cách viết. Chuyện tình yêu đẹp và cả những tình bạn chân thành mang theo một chút xót xa trong cuộc đời, trong kết cục của mỗi con người. Thương cho Thuốc Lá, tiếc cho chuyện tình chưa nói của cô và Áo Xanh, dư luận và điều tiếng xã hội luôn đi trước tình yêu. Không phải cứ nhân vật chính thì sẽ hạnh phúc. Trong cuộc đời này, ai cũng là nhân vật chính trong cuộc đời mỗi người. Sống làm sao để mai này lỡ không thấy được ánh đèn vẫn không thấy tiếc như Thuốc Lá. Yêu bản thân, trân trọng gia đình khi còn trẻ, đứng lên và chủ động tìm kiếm hạnh phúc thật của mình, tuổi trẻ ngắn lắm. 
4
367570
2015-09-01 18:31:11
--------------------------
257132
3895
-Đầu tiên thì tôi nhận xét về bìa của quyển sách. Rất đẹp, dễ thương và sáng tạo. Giấy tốt. Tôi hài lòng về quyển sách này
-Về nội dung thì ban đầu đọc thì tôi thực sự không thích thú cho lắm và thực sự nghĩ mình đã phí một số tiền. Và tôi thừa nhận là tôi sai vì càng đọc thì tôi lại càng không thể dứt ra được, thậm chí còn có chút tiếc khi đã lật hết trang cuối cùng. Quyển sách này đã cho tôi một động lực rất lớn để tiếp tục kiên trì trong con đường tìm kiếm người mình yêu thương và làm tôi muốn đứng trên nóc nhà thế giới rồi hét thật lớn rằng: "Tôi yêu gia đình tôi lắm lắm!". Quyển sách này dạy tôi hãy biết trân trọng từng giây phút bên gia đình, bên ba mẹ, mỗi nhân vật trong đây đều có một quá khứ không đẹp, không hề có chữ ba mẹ trong tiềm thức, tôi đọc mà thấm thía biết bao. Đến chương mà tác giá đúc kết lại câu chuyện rồi khuyên chúng ta đừng như Thuốc Lá, đến một lần nhìn mặt người thân còn không có làm tôi cảm thấy buồn tủi và tiếc thương cho những khoảng thời gian tôi quên mất gia đình... Thôi tôi không viết nữa, tôi về với họ đây 
5
709193
2015-08-07 14:12:46
--------------------------
248328
3895
Quả thật đây là một câu chuyện tình yêu rất dễ thương. Tất cả các nhân vật đều đáng yêu, với nhưng tên gọi ngộ nghĩnh miêu tả ngắn gọn nét nổi bật của từng người. Tưởng chừng chẳng có điểm chung nào giữa họ, một anh chàng công tử nhà giàu, cô sinh viên, chị gái gọi, cô công nhân, anh công an, bà bán hàng nước,... nhưng tất cả họ đã ở đó cùng gắng bó, yêu thương, cùng nhau trải qua nhiều biến cố trong một quãng thời gian không dài không ngắn, đủ để sau này mỗi khi nhớ lại vẫn thấy đau vì người thiếu vắng, thấy nhớ những chiều nhàn rỗi bên ván cá ngựa kéo dài. Có những thứ dở dang, có thứ vỡ lại lành, và có cả sự gắn kết của những con người yêu thương đến với nhau sau nhiều hiểu lầm, trắc trở. Câu chuyện hay, cảm động và lôi cuốn.
4
21640
2015-07-31 09:48:08
--------------------------
242166
3895
Đây là cuốn sách thứ hai do tác giả Việt sáng tác mình đọc. Và mình thật sự rất bất ngờ khi cầm cuốn sách trên tay, nhỏ nhắn xinh xinh. Về nội dung motip thì không có gì khác lạ, nhưng cái cách tác giả dẫn dắt cảm xúc vào câu chuyện thì rất là tuyệt. Phải nói đây là cuốn sách về tình bạn rất y nghĩa, giữa một chàng đại gia, 1 cô gái ngây thơ, 1 người phụ nữ bất hạnh và 1 cô gái bán hoa. Ngọt ngào, sâu sắc, đọng lại trong mình rất nhiều suy nghĩ về tình bạn...
5
295019
2015-07-26 10:36:43
--------------------------
198818
3895
Ấn tượng đầu tiên của cuốn sách này khi tớ mua về đó chính là tựa sách: Chuyện Iphone Và Gã Chảnh Mang Mật Danh 49. Câu chuyện quá dễ thương cuốn hút tớ ngay từ những trang đầu, đọc rồi mà muốn đọc lại lần hai, lần ba. Tình tiết trôi một cách chậm rãi nhưng không hề nhàm chán, đôi lúc khiến bạn phải bật cười vì cách thể hiện của các nhân vật quá ư là đáng yêu.
Bên cạnh câu chuyện dễ thương này chính là những thông điệp đầy ý nghĩa mà tác giả Thu Dương muốn truyền đạt đến tất cả mọi người: Dám yêu!
Bìa sách thì đẹp khỏi chê, tớ rất thích cách minh họa này!
Một cuốn sách hay!
4
621744
2015-05-20 21:23:02
--------------------------
196007
3895
Trưa qua nhận được sách, tối về 22h ôm cuốn sách định đọc chút xíu trước khi ngủ, ai ngờ bị cuốn hút đọc hết cuốn sách luôn tới 24h30 mới ngủ, báo hại sáng nay không thể dậy sớm học bài. Nhân vật khá phong phú, 4 nhân vật nữ đại diện cho 4 số phận khác nhau. Tuy nhiên đọc xong sách mình cứ bị liên tưởng tới tiểu thuyết bởi các nhân vật chưa được đời thực lắm.
Tuy nhiên nôi dung tác giả muốn truyền đạt cũng khá hay, Dám yêu, Dám thể hiện, và Dám sống hết mình vì Tình Yêu của mình đừng để sau này phải nuối tiếc vì bất cứ điều gì mà mình chưa dám làm, dám thử.
4
628484
2015-05-15 06:19:01
--------------------------
192281
3895
Chuyện rất cuốn hút, hay nhé, cốt truyện rất mới lạ. Đọc xong một chương là muốn đọc luôn chương kia, đọc không ngừng nghỉ. Nói chung, Thuốc Lá có một số phận khá bi thảm . Đọc về Thuốc Lá, ta thấy một khía cạnh khác của cuộc đời. Và có một sự thích nhẹ nhân vật Iphone.Đọc đoạn đầu em thấy mắc cười lắm, nhất là cái khúc Gã tưởng là iPhone sẽ ngả đầu vào vai Gã mà khóc, rồi Gã sẽ dùng lời lẽ an ủi, rồi nâng mặt nàng lên, rồi hôn nàng, v.v... Cuối cùng lại hụt hẫng, em thấy rất vui. Còn cái đoạn mà chị nói Gã đi loanh quanh căn nhà của “người yêu tương lai”, em thấy Gã thật đáng yêu.
5
624007
2015-05-04 10:41:13
--------------------------
192274
3895
Dịp nghỉ lễ lượn phố sách mua. Vì thấy cái bìa lạ nên mua về. Cũng không hy vọng gì nhiều, nhưng đọc xong trong 1 ngày và thấy rất... ĐÃ.
Bạn tác giả viết ấn tượng. Hình tượng các nhân vật xây dựng không biết có nguyên mẫu và có là những câu chuyện thật không mà đọc như thắt cả lòng, cảm giác như mình chỉ cần giơ tay ra là chạm được vào họ cả rồi.
Ấn tượng nhất phải nói tới là cuộc đời của chị Tóc Tém, có lẽ vì mình đã từng chứng kiến câu chuyện gia đình của một người bạn giống hệt chị rồi nên đồng cảm lắm. Và thích cả anh Sóc Nâu nữa - tuy nhân vật phụ, nhưng cực kỳ hấp dẫn. Hy vọng bạn tác giả viết câu chuyện dài nữa về anh Sóc Nâu nhé.
5
4397
2015-05-04 10:16:45
--------------------------
