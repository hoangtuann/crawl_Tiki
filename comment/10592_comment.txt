174976
10592
Công chúa gồm hai truyện chứa đựng nhiều ý nghĩa biểu tượng. Yếu tố văn hóa, xã hội, con người thâu tóm trong những tình huống kịch tính. Nó không định hình tư tưởng, cũng không chủ tâm tạo ra một sự phân định rạch ròi. Câu chuyện đầy những dụ ngôn mà con người luôn phải hoài nghi, tra vấn. Mạch truyện vừa phải, đôi lúc khá chi tiết nhưng chính lối diễn đạt nhấn mạnh vào hành động giúp cho tác phẩm lôi cuốn và hấp dẫn. 

Cá nhân tôi đánh giá rất cao những tác phẩm nằm trong Tủ sách tinh hoa này. Riêng quyển Công chúa in khổ vừa vặn, nhẹ, dễ mang bên mình. Tuy không nằm trong "Kiệt tác" nhưng rất đáng đọc.
3
172604
2015-03-29 10:09:56
--------------------------
30157
10592
Mình đã đọc giới thiệu nội dung trước khi mua, thấy nội dung của Công Chúa khá hay và lý thú nên cầm về. Kết quả là không được như ý lắm, vì tác giả viết thiên về tình tiết quá, không nói gì nhiều đến nội tâm nhân vật, và lối kể khá vắn tắt, đều đều, không có chi tiết bất ngờ. Lại thêm bản dịch cũ không được dịch hay lắm và không được biên tập lại. Sách cũng không được in tốt lắm, giấy xấu, bìa và các trang cắt còn hơi xéo nhau, nhiều trang còn dính với nhau. Mình thấy hình như NXB Phương Nam xuất bản bộ Tủ Sách Tinh Hoa Thế khá là ẩu.
3
5548
2012-06-12 11:09:59
--------------------------
29273
10592
Lần đầu tiên mình đọc sách của D.H. Lawrence, và thấy thích tác giả này ngay. Tập này gồm có 2 truyện, với tiêu đề ngắn gọn súc tích: Công chúa và Con cáo. Truyện ngắn của Lawrence có hơi hướng mang rợp màu sắc ngụ ngôn, nhưng là ngụ ngôn cho người lớn chứ không phải trẻ em, bởi vì kết thúc của tác phẩm khá là nghiệt ngã, và vì câu chuyện thì nhiều sức nặng hơn lối văn phong nhẹ hẫng ông đã dùng. Như trong truyện Công chúa, sức hoang tưởng của người cha đã được đẩy lên cao độ đến mức tạo thành 1 nét tính cách ngạo mạn và kiêu căng, và điều đó lây sang cả con gái. Nàng công chúa hoang tưởng và cha nàng xa lánh và khước từ tất cả những ai không chịu quỵ luỵ hay ngưỡng mộ trước họ. Nàng cuồng tín đến độ ý nghĩa sống duy nhất của nàng là trở thành 1 nàng công chúa, chứ không phải 1 người phụ nữ tầm thường được yêu thương và hạnh phúc. Nếu có phút giây nàng xiêu lạc khỏi ý nghĩ ấy thì phải rũ bỏ ngay không khoan nhượng. Điều đó dẫn tới sự điên cuồng và cái chết không đáng có của 1 người. Dĩ nhiên nàng có bị tác động, nhưng miễn nàng còn được là công chúa cao quý và trinh nguyên, điều đó chẳng hề chi. Và cuối cùng, bỏ qua niềm vui được tận hưởng cuộc sống của mình, nàng trở thành không phải 1 nàng công chúa, mà là 1 con rối vô tri bị điều khiển bởi những hoang tưởng của người cha đã khuất. Có thể chỉ 1 chút thay đổi thôi, câu chuyện sẽ khác đi, nhưng nó là thế và chỉ có như thế nó mới thành nên 1 tác phẩm trọn vẹn. Châm biếm nhẹ nhàng, như có như không nhưng sắc sảo, thâm thuý vô cùng. Thêm chút gia vị lãng mạn đủ để những ai dù biết kết cục ngay từ đầu cũng không khỏi ngẩn ngơ tiếc rẻ.
4
26136
2012-06-04 20:09:23
--------------------------
