180972
4242
Theo quan điểm của mình, mình nghĩ quyển sách khá cần thiết đối với trẻ em đang trong giai đoạn nhận thức đồ vật xung quanh. Sách có nhiều hình vẽ ngộ  nghĩnh và dễ tô, dễ nhận dạng giúp các bé nhìn nhận các vật dễ hơn và theo cách đáng yêu. Bé cũng có thể phát triển khả năng tìm tòi, sáng tạo những bức tranh ngộ nghĩnh khác dựa vào hình mẫu đó. Ngoài ra thì tập tô cũng giúp bé khéo tay, thực hiện các động tác nhanh nhẹn hơn, như là cầm bút màu dễ dàng hơn, giúp cho bé khi đi học sẽ quen hơn với việc viết bút mực. Sách cũng có giấy đẹp, trắng và giá thành rẻ nên các mẹ có thể dễ dàng chọn lựa.
5
602696
2015-04-10 18:05:45
--------------------------
