385826
10051
Năm câu chuyện của năm tác giả với những cung bậc tình yêu khác nhau. Nhưng có lẽ mình bị gây ấn tượng nhất là câu chuyện số 3: Đừng quên em của tác giả Justine Larbalestier. Tác giả lồng câu chuyện tình yêu vào một tình huống bi kịch với một kết thúc đầy ám ảnh. Một tình yêu đẹp phải đánh đổi bằng sự ghẻ lạnh, xa lánh thậm chí cả mạng sống của mình. Đọc đến đoạn cuối mà mình không kìm được xúc đông. Tình yêu thật sự sẽ còn mãi và đáng được mọi người vinh danh. Cuốn sách có lẽ là món quà ý nghĩa trong dịp Valentine.
4
93234
2016-02-24 17:24:49
--------------------------
48017
10051
Đây là một cuốn sách rất hay và phù hợp với nhiều lứa tuổi. Đúng như cái tên nhẹ nhàng của nó, 5 truyện ngắn trong tác phẩm này đều mang màu sắc nhẹ nhàng, lãng mạn. Với lời văn hiện đại, trẻ trung và lối diễn đạt thông minh sắc sảo, 5 nhà văn tên tuổi của dòng văn học kỳ ảo đem đến những câu chuyện tuyệt vời với những cảm nhận mới mẻ. Chuyện tình yêu, cuộc sống đan xen trong cái phức tạp của một thế giới kỳ lạ, tất cả đã dẫn dắt người đọc vào một cuộc hành trình tìm kiếm những cảm xúc nguyên sơ và đẹp đẽ nhất của con người. Ẩn sau vẻ kỳ ảo là những thông điệp tuyệt vời về tình yêu được gửi gắm, chắc chắn cuốn sách sẽ mê hoặc nhiều người.
4
20073
2012-11-28 14:54:06
--------------------------
31463
10051
5 tác giả nổi tiếng, 5 truyện ngắn lãng mạn !!! Đây không chỉ đơn thuần là những truyện ngắn về tình cảm lãng mạn mà trong đó còn pha thêm một chút lung linh huyền ảo của thế giới tưởng tượng. Các truyện không kết thúc mà để mở để mỗi người có thể nghĩ theo cách mình muốn. Trong tập truyện này mình đặc biệt thích tác giả Melisa Marr - chuyên viết về thế giới tưởng tượng, tiêu biểu là bộ truyện " Vẻ đẹp nguy hiểm ". Truyện được in trên giấy tốt, giá 80k là rất ok :D 
5
38556
2012-06-26 11:06:50
--------------------------
