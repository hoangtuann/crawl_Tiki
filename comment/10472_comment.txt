301580
10472
Bộ túi: Chuyện Kể Về Loài Vật - Thỏ Rừng Robin là bốn tập truyện, bốn cuộc phiêu lưu nho nhỏ của chú thỏ Robin tinh nghịch, mưu trí. Với trí thông minh và lòng tốt bụng của mình, chú đã giúp đỡ cho các bạn mình và làm được nhiều việc rất ý nghĩa: đưa bạn sóc đi chữa bệnh, tìm các con cho chị chuột, giúp các bạn vịt thoát khỏi tên cáo, tìm nhà mới cho gia đình sóc sau cơn bão... Những câu chuyện dễ thương mang tính giáo dục cao cùng với nét vẽ sinh động là món quà cho con mà tôi ưng ý.
5
479122
2015-09-14 18:58:27
--------------------------
