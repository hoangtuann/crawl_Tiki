461674
8879
Trong tập phim ba lần đánh Bạch Cốt Tinh là 1 trong những tập làm tôi bức xúc nhất và không muốn xem lại nhất vì sự phũ phàng và không hề tín nhiệm đệ tử của mình của Đường Tăng! Nhưng... tập truyện này lại không đặt nặng vấn đề đó lắm, Ngộ không sau khi đánh hạ hình dạng cải trang thứ ba của Bạch Cốt đã diệt luôn chân thân của mụ, nên việc Ngộ không bị đuổi đi diễn ra sau khi mụ chết và đất diễn của Bạch Cốt trở nên ít ỏi. Câu chuyện nhảy sang đoạn công chúa Bảo Tượng...
Tôi khá thích nét của bộ này nhưng sao tập 7 có hơi sến súa, giải quyết tình huống hơi nhanh, lúc Đường Tăng khóc... cứ thấy như làm nũng... Nên hơi không quen với hình tượng mạnh mẽ của ông thường ngày...
Tôi cảm thấy Ngộ không trong bộ này rất có tính người, biết điều, cũng ma lanh lắm, tôi thích các nhân vật cũng bộc lộ thái độ rõ ràng hơn Tây Du Ký 1986 (dù theo kiểu hài), ví dụ như đoạn Ngộ Không bực bội ra mặt vì Ngọc Hoàng đưa ra trừng phạt có phần thiên vị với yêu quái (xổng xuống thiên đình) làm bậy hại người suốt mấy chục năm.
4
998926
2016-06-27 22:40:12
--------------------------
