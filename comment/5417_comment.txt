520829
5417
Câu chuyện kịch tính từ giây phút đầu tiên. Cách tác giả mô tả tâm lý nhân vật cực kỳ hấp dẫn, lôi cuốn. Đọc trang này rồi thì không thể không lật tiếp trang sau (mình ngốn rất nhanh). Nhưng đọc đến cuối vẫn còn vài chỗ chưa thoải mái lắm, nếu thêm 1 vài tình huống Thanh tra chủ động hoặc đừng phụ thuộc nhiều quá vào sự chủ động của tên sát nhân thì câu chuyện sẽ trọn vẹn hơn tí xíu.
Nói chung, không cảm thấy tiếc thời gian khi theo câu chuyện này !
4
176201
2017-02-07 21:12:41
--------------------------
486600
5417
Một tên giết người thông minh, tài tình và thích làm thơ. Hắn tạo ra những dấu vết, bằng chứng giả sau những vụ giết người để chơi với cảnh sát, để gửi đến họ một thông điệp ẩn "Lũ cảnh ngu ngốc sát xấu xa và không thể bắt được ta." Nhưng đằng sau động thái này không chỉ có vậy, điều sẽ được giải mã sau đó.

Nhưng đám cảnh sát không chỉ toàn lũ ngu, hắn có một đối trọng - Thanh tra đặc biệt Gurney, được người ta gọi là Sherlock Gurney như một thám tử có bộ óc sắc bén và thông minh nhất sở cảnh sát Poeny.

Dù là vậy, trong cuộc rượt đuổi hắn luôn là người đi trước cảnh sát rất xa. Luôn là người đi trước, cho đến tận những phút cuối cùng.

John Verdon gần như đã tạo lên một hình tượng mới về nhân vật phản diện, vô cùng lôi cuốn, vô cùng xảo quyệt, vô cùng phong cách.
5
503756
2016-10-15 09:41:15
--------------------------
479183
5417
Khi đọc những dòng dẫn dắt trên Tiki, mình đã bị lôi cuốn và không ngần ngại nhấn "Mua ngay". Cầm trên tay cuốn tiểu thuyết, mình bị thôi thúc và nghiền ngẫm nó ngay lập tức. Một cuốn tiểu thuyết trinh thám đầy bí ẩn, khiến người đọc không khỏi tò mò về cách thức gây án: tại sao? làm thế nào? hung thủ là người hay một tên quỷ biết đọc suy nghĩ? Sự tinh tế trong cách thức miêu tả, sự khéo léo trong cách thức dẫn dắt câu chuyện của tác giả khiến người ta không thể rời mắt. Và rồi vỡ oà khi đoạn kết đến gần. Nếu bạn còn lưỡng lự, thì tôi khuyên bạn: nó thật sự đáng đọc đó! Tiki giao hàng lúc nào cũng cẩn thận, sách in rất đẹp, bìa sách và chất lượng đều đáng 5 sao!
5
899318
2016-08-08 10:02:07
--------------------------
472069
5417
Cuốn sách này rất phù hợp với những bạn ưa thích thể loại trinh thám. Ngay từ đầu câu chuyện đã dẫn dắt người đọc đến vũ tàn sát kinh hoàng dần dần chuyển đến kịch tính, tuy nhiên càng về sau kết cấu câu chuyện trở nên khá phức tạp phải đọc kĩ mới hiểu hết được nội dung, dẫn đến nhiều lúc mình ngại đọc nhưng khi đọc xong cuốn sách này phải nói thật từ lời văn đến lối dẫn dắt câu chuyện khá hấp dẫn người đọc. Mình khuyên baj nào là fan trinh thám không nên bỏ lỡ cuốn sách này.
4
112968
2016-07-09 09:32:43
--------------------------
454427
5417
Nhan đề truyện đã hướng người đọc đến một câu chuyện liên quan trực tiếp đến suy nghĩ của nạn nhân. Qua lời kể của một cảnh sát đã về hưu nhiều kinh nghiệm trong phá án, những suy nghĩ của ông về bức thư kì lạ được gửi đến người bạn lâu ngày không gặp, cái chết bất ngờ của anh ta và thêm một số cái chết tưởng chừng không liên quan đến nhau đã được thể hiện chân thực qua giọng văn gay cấn, lối viết tài tình của tác giả. Cuối cùng, hung thủ không ai ngờ tới đã phải xuất đầu lộ diện, trả lại công bằng cho nạn nhân. Trong truyện ta còn thấy cuộc đấu trí thú vị giữa cảnh sát và thám tử các bang. Thật sự là một cuốn truyện trinh thám đáng đọc.
5
1074693
2016-06-21 22:27:18
--------------------------
439060
5417
Tiều thuyết hình sự đầu tay của John Verdon đã nhận được không ít lời đánh giá tốt  đẹp chobạn đọc tham khảo trước khi chọn mua. Mà hay thật! Ở đây tôi thật sự muốn nhấn mạnh tới yếu tố dịch thuật. Bởi lẻ, đọc tác phẩm văn học hay sách nước ngoài phần chuyển ngữ là vô cùng quan trọng đối với độc giả người Việt. Gặp những người dịch non tay, đọc một hồi là thấy lùng bùng, nhức đầu, ngán ngẩm và cuối cùng là không hiểu hoặc hiểu sai nội dung, ý nghĩa. Khi ấy cho dù tác phẩm gốc có hay cỡ nào đi nữa cũng không thể được đánh giá đúng. Cuốn Hãy Nghĩ Tới Một Con Số của John Verdon đã may mắn được dịch giả Tâm Hiền chuyển ngữ rất mượt mả, sinh động đọc không thấy những chổ "sường sượng" thường gặp trong truyện dich. Tôi xin chân thành cảm ơn dich giả.  
4
128936
2016-05-30 20:33:00
--------------------------
425198
5417
Ngay từ những dòng chữ mở đầu câu chuyện, giọng văn của John như đã thổi một làn khói thôi miên làm tôi không thể đẩy mình ra khỏi cuốn sách. Đi sâu hơn theo giọng văn ấy, biên độ giao động của nhịp tim ngày càng tăng lên với tần số giảm đi ngày càng nhanh. Những tình tiết, câu đố, những lối miêu tả, cách dùng từ, yếu tố tâm lý, tình yêu, sự lơ đãng,... tất cả được gói gọn trong một bọc mang tên "Nghệ thuật tài ba" của John.
Cái kết cuối cùng như một cú sốc khi hung thủ lộ diện.......
Đừng bỏ lỡ một chuyến đi đáng giá này, đặc biệt khi bạn thích truyện trinh thám!
4
761866
2016-05-03 22:53:57
--------------------------
391761
5417
Mình là fan hâm mộ dòng trinh thám, đặc biệt là dòng  trinh thám nước ngoài. Nếu như Conan Doyle có Holmes, Agatha Christine có Thám tử Poirot, thì John Verdon rất thành công với Dave. Cốt truyện cực kì lí thú, hồi hộp, hấp dẫn trước những bí ẩn của các con số. Đọc cảm thấy thõa mãn,tuy không quá xuất sắc như holmes nhưng " Hãy nghĩ tới một con số" có một nét hay rất mới lạ, muốn biết thì các bạn nên mua, sẽ không phí tiền đâu.Đợt này mình được tiki tặng bookcare nên cầm sách rất đã tay.
5
587976
2016-03-05 20:10:29
--------------------------
381468
5417
Đây là một quyển sách đáng đọc.

Mình chưa đọc nhiều về thể loại trinh thám, hầu hết chỉ đọc những tác phẩm được viết bởi những tác giả tên tuổi trong dòng trinh thám này. Đây là lần đầu tiên mình đọc một tác phẩm đầu tay và nó, không hề khiến mình thất vọng. Là sự kết hợp giữa những chi tiết cực kỳ logic và lập luận sắc bén, mọi thứ được móc trong một mắt xích hoàn hảo không kẽ hở, những chi tiết tưởng chừng không liên quan lại gây ra những thay đổi lớn trong câu chuyện, và hung thủ xuất hiện trong một tình huống không ngờ tới nhất, nhưng lại đầy hợp lý. Đôi khi khiến mình phải vò đầu bứt tai kiểu "Tại sao mình lại không nghĩ đến điều này chứ?" . Xen kẽ vào đó là những chi tiết rất đời thường nhưng lại đầy ý nghĩa.

 Mình thích nhân vật Madeleine nhất, một nhân vật phụ, là vợ của Dave, sở hữu một tâm hồn tinh tế, và cái đầu thông minh có thể chú ý đến những thứ rất nhỏ nhặt nhưng lại là mấu chốt của vấn đề. 

Phần mở đầu câu chuyện có hơi dài, nhưng nếu xét tập truyện trong một series thì không dài, một cách đầy đủ miêu tả tính cách, môi trường xung quanh nhân vật chính, làm nền tảng cho mọi hình dung của người đọc. Cá nhân mình, đây là một quyển sách tuyệt vời, hi vọng rằng Nhã Nam sẽ tiếp tục xuất bản những tập tiếp theo.
5
1143952
2016-02-17 13:37:53
--------------------------
375041
5417
Đây là truyện trinh thám đầu tiên mình đọc mà tác giả không phải người trung quốc. Tên truyện tuy k để lại nhiều ấn tượng nhưng nội dung rất hấp dẫn. Hủng thủ rất tài giỏi lừa được cả cảnh sát, sau mỗi vụ án đều để lại lời nhắn cho địa điểm mình sẽ giết người tiếp theo, cho đến cuối cùng nếu không đưa ra quyết định đặt bom giết tất cả mọi người thì có lẽ cũng không thể bị bắt. Tuy nhiên mình thấy hơi không hợp lí ở chỗ hung thủ vừa có thể lực, vừa biết chế tạo bom vừa thông thạo máy tính. Ở ngoài đời tìm được một người hội tụ những điểm trên đều rất khó, cho nên mình thấy k thật cho lắm. Nhưng nói chung quyển sách rất đáng đọc, không nên bỏ lỡ. Cả hung thủ và cảnh sát thông minh khiến mọi người phải kinh ngạc.
5
354369
2016-01-27 17:41:32
--------------------------
342358
5417
Với cách miêu tả chi tiết, dẫn dắt người xem ngay từ đầu, thật sự nó đã lấy được tình cảm của mình từ chương 1. Nhân vật được lột tả hết sức chi tiết, chả ai thiếu, cũng không ai thừa, mỗi nhân vật đều ảnh hưởng đến suy nghĩ và cảm xúc của câu chuyện.
Tác giả biết cách khai thác trí tò mò của người đọc qua từng chương trang sách.
Bí ẩn được đẩy lên cao trào ở những chương sau.
Với hàng loạt đánh giá từ những trang nổi tiếng, thật sự mình nghĩ nó xứng đáng.
Hy vọng những cuốn sau của tác giả này sẽ được dịch tiếp.

5
390984
2015-11-24 11:13:29
--------------------------
324044
5417
Mình mới nhận được cuốn sách cách đây một ngày, mặc dù mình chưa đọc hết nhưng cuốn sách cũng mang lại cho mình khá nhiều ấn tượng. 
Thứ nhất là về mặt hình thức. Sách có bìa khá ổn, tông màu đúng chất mình thích, chất lượng giấy tốt, sách khá nặng. 
Thứ hai là về mặt nội dung. Mình chỉ nói về cảm nhận cá nhân, tránh tóm tắt để không bị spoil. Một trong những tag truyện mình thích nhất là mysterious. Cuốn sách này có tag mình thích. Nội dung khá lôi cuốn ngay từ những trang đầu tiên. Theo cá nhân mình, đây là một cuốn sách khá hay và đáng có trong tủ sách của những độc giả khoái mysterious như mình. 
5
759572
2015-10-20 11:24:47
--------------------------
321112
5417
Mình chắc chắn cho cuốn sách này 5 ngôi sao. Vì câu chuyện quá xuất sắc, cách viết cũng tuyệt vời luôn nên không có gì để phàn nàn cả. Các nút thắt mở trong truyện đều rất logic và tự nhiên. Cách dẫn dắt đến hung thủ cũng rất hấp dẫn và bất ngờ khi kẻ giết người lại nằm ngay trong số nạn nhân. Tác giả tạo được những câu hỏi vô cùng lý thú và lần lượt giải quyết một cách tài tình. Có điều, mình không thích  cách nhà văn xây dựng nhân vật thám tử Gurny ở chỗ ông này có thể giới tình cảm rối rắm, nội tâm hơi “yếu yếu” sao đấy. nói chung tính cách không được hay ho cho lắm theo ý mình. Ngoài ra còn vài  điểm nữa mình không hiểu là sau khi được nghe kể vể người phụ nữ bị đâm ở cổ và con trai bà ta rồi kiểm chứng lại với trung úy Nardo, nắm được thông tin về tên tuổi nhân dạng nọ kia của bà và ông chồng với đứa con. Thì đến đây hẳn Gurny đã rõ Jimmy là hung thủ vụ này ( dù có thể chưa kết nối được hung thủ với nhân thân hiện giờ của hắn ), vậy tại sao Gurny lại không gọi ngay cho đồng đội để lấy thông tin về Jimmy Spinks ? mà lại tiếp tục lòng vòng ở nhà của Dermott để rồi chui xuống bẫy. Thậm chí cả trước khi đến chỗ Dermott nữa, nếu đã biết là hung thủ dán giấy ngay trước cửa nhà  Gregory  và ngay chỗ anh này ngồi ăn thì chắc chắn là hắn có lảng vảng quanh đấy rồi, cộng với việc hắn có khả năng vào được hộp thư của Greg thì nhiều khả năng hắn là người trong vùng, sao không xem xét khía cạnh đó??
Dù sao thì, tóm lại câu chuyện cũng rất thú vị , ai là fan truyện trinh thám, nhớ đừng bỏ qua cuốn này nhé! 

5
9229
2015-10-13 10:39:13
--------------------------
320255
5417
Ngay từ tựa đề cuốn sách đã khiến người đọc tò mò. Ngay từ đầu tác giả dẫn dắt câu chuyện theo lối đuổi bắt không ngừng, khiến người đọc đắm chìm trong câu chuyện và luôn tự hỏi rằng "Làm cách nào?". Câu chuyện đuổi bắt kèm lối viết khôn khéo, tác giả giữ được người đọc tò mò đến cuối câu chuyện và rồi vỡ lẽ ra được cách thức mà tên tội phạm đã thực hiện.
Ngoài ra thì bìa sách đẹp, chất lượng giấy tốt và khá nhẹ, thích hợp cho việc mang theo mọi lúc mọi nơi :)
4
385712
2015-10-10 20:58:23
--------------------------
298196
5417
Bìa của cuốn sách rất hợp với nội dung, tất cả đều xuất phát từ một con số. Bạn nhận được một phong bì, bên trong là một bức thư yêu cầu bạn hãy nghĩ đến một số tự nhiên không quá 1000 và mở phong bì còn lại. Ngạc nhiên là nó lại chưa con số mà bạn vừa nghĩ. Diễn biến vừa phải, nhưng hơi sa đà vào việc "như thế nào" hơn là truy bắt hung thủ. Ý tưởng khá độc đáo, vậy mà kết lại gây ức chế vì không rõ hung thủ. Tổng kết lại dây vẫn là cuốn trinh thám đáng để đọc.
4
374756
2015-09-12 15:22:09
--------------------------
291384
5417
Về hình thức, đây là một quyển sách đẹp với trang trí bìa hấp dẫn người đọc, chất lượng giấy và bìa sách tốt. Sách dày, đẹp, và nhẹ. Còn về nội dung, đây là một trong những tác phẩm trinh thám hay nhất mình từng đọc. Cốt truyện hấp dẫn, văn phong cuốn hút, cách hành văn chuyên nghiệp khiến những ai đã cầm sách lên lại dễ dàng buông xuống. Những tình huống thắt nút - mở nút ly kì, những bí mật mà không ai có thể đoán ra cho đến tận phút cuối,... tất cả đã làm cho cuốn sách này trở thành một tác phẩm tuyệt vời!
4
299660
2015-09-06 11:11:16
--------------------------
289616
5417
Tác phẩm viết khá hay đối với thể loại truyện trinh thám., Truyện tạo sự cuốn hút ngay từ đầu, mạch truyện phát triển khá logic, nhưng cũng sẽ khó lí giải đối với độc giả, tạo cảm giác phải đọc đến cuối cùng. Tác giả xây dựng bối cảnh hay, mạch truyện xuyên suốt, vững chãi, nhân vật được có chiều sâu tâm lý. Miêu tả cũng rất chi tiết.Bìa sách được in khá đẹp, giấy trắng, dịch khá tốt. Nói chung đây là quyển sách truyện trinh thám rất hay, xứng đáng để mua, để đọc với mọi người.
5
62433
2015-09-04 17:27:29
--------------------------
274949
5417
Mình mua quyển sách này do mình là fan truyện trinh thám cộng với bìa sách rất ấn tượng. Chất lượng quyển sách rất tốt. Sách dày nhưng tương đối nhẹ và đẹp. Cốt truyện cuốn hut, gay cấn tuy có đôi chỗ hơi chùng xuống nhưng rất hay. Bạn phải đọc hết quyển thì mới hiểu được tất cả các bí mật chứ khó có thể đoán trước được điều gì. Nói chung đây là quyển sách truyện trinh thám rất hay. Mình sẽ không kể về nội dung vì như vậy sẽ là mất đi nhiểu điểm thú vị. Các bạn hãy tự khám phá nhé.
4
696755
2015-08-22 14:18:20
--------------------------
263291
5417
So với một tác phẩm đầu tay thì cuốn này viết rất tốt. Tác giả xây dựng bối cảnh hay, mạch truyện xuyên suốt, vững chãi, nhân vật được có chiều sâu tâm lý. Miêu tả cũng rất chi tiết. Cảm giác như đang xem bộ phim chiếu trước mắt chứ không phải đọc truyện nữa. Dù cuối cùng tác giả giải thích bí mật của con số có hơi gượng, không thuyết phục. Và đoạn đầu của truyện có những câu viết hơi dài, đọc dễ hụt hơi mà khó hiểu ngay trong 1 lần đọc. Nhưng cuốn truyện có rất nhiều điểm cộng cho ý tưởng, cách khắc họa nhân vật. Về tổng quan, đây là một cuốn nên có trong tủ sách của độc giả yêu trinh thám. Nếu một ngày có 48 tiếng, tôi e bạn sẽ đọc một lèo cho đến trang cuối. 
4
696174
2015-08-12 14:22:52
--------------------------
254126
5417
Mình rất khâm phục tác giả khi nghĩ ra được cách mà hung thủ chọn được những người bị đọc vị con số 658, câu hỏi mà mình đau đầu từ đầu cho tới cuối cuốn sách mới được giải đáp một cách thỏa đáng, còn những chi tiết khác thì phần nào mình đoán ra được, có lẽ do nó được đề cập từ khá nhiều truyện tranh trinh thám khác. Truyện có kịch tính, có cao trào nhưng điểm mà mình chưa thích là dường như tác giả lan man những chi tiết phụ có hơi dài, và có những lúc tưởng như đi thẳng tới manh mối kia để giải quyết vụ án nhưng lại phải chờ, nói tóm lại, đây là một cuốn sách trinh thám đáng đọc và đáng có trong tủ sách nhà bạn.
4
304901
2015-08-05 00:10:30
--------------------------
241232
5417
Lần đầu tiên khi nhìn thấy bìa sách mình đã quyêt định mua ngay.Mình đã rất hài lòng, nó là một cuốn tiểu thuyết trinh thám kịch tính và đặc sắc. Không chỉ bìa sách bắt mắt, nhưng nội dung bên trong của nó sẽ làm bạn ngạc nhiên hơn. Từng tình tiết xuất hiện, những suy đoán, kết luận sẽ thu hút các bạn. Tác giả viết rất lôi cuốn, đặc biệt mình thích những câu thơ trong truyện thể hiện tính sáng tạo cao và tính logic cao. Một bí ẩn đã được giải đáp, thể hiện tính nhân đạo.
5
393794
2015-07-25 12:13:13
--------------------------
240453
5417
Tác giả đưa ra việc anh bạn học cũ thời đại học của tay thanh tra nhận được lá thư đoán đúng con số mà anh ta nghĩ đến đến 2 lần (số 658 và 19), anh bạn học nhờ sự giúp đỡ của anh thanh tra, sau đó anh bạn học bị sát hại và có rất nhiều chứng cứ, tiếp đó những vụ án mạng khác cũng xảy ra, những suy luận tưởng chừng đi đúng hướng lại không dẫn đến kết quả. Trong truyện này rất thích mấy câu thơ, mấy ông cớm và gã công tố dở hơi
3
38833
2015-07-24 16:47:43
--------------------------
239364
5417
Đây thật sự là cuốn sách về trinh thám rất hay. Mình là một fan ruột của trinh thám nhưng không khỏi bất ngờ trước cách viết đầy lôi cuốn và ấn tượng của John Verdon. Tác giả đã đưa người đọc từ bât ngờ hay đến bất ngờ khác, minh như cuốn theo nhịp thở của sách. Lo lắng, hồi hộp, háo hức và bồi hồi trước từng mảnh ghép của vụ án dần dần được tiết lộ. Tuy phần kết có hơi gấp gáp và khiến người đọc hẫng, nhưng những tình tiết dẫn đến kết quả của vụ án đã bồi đắp được phần nào. Xuất sắc về yếu tố tâm lý 
5
135669
2015-07-23 20:55:26
--------------------------
224934
5417
Nội dung cuốn trinh thám này đúng là có rất nhiều điều mới lạ và gây nhiều bất ngờ. Một người không quen biết có thể đoán chính xác được con số trong đầu bạn hay không và làm như thế nào? Mối hận thù mà những nạn nhân đã gây ra là gì để phải nhận lấy cái chết như thế? Theo tôi đây là một cuốn trinh thám khá xuất sắc về các yếu tốc tâm lí, hồi hộp , bất ngờ ở đoạn kết và giúp rút ra được nhiều kinh nghiệm cho cuộc sống của mình.
4
129176
2015-07-09 15:00:24
--------------------------
223415
5417
Nội dung tạm được, không quá nhiều bất ngờ và những tình tiết khiến độc giả phải nhảy dựng lên. Cách viết thì khá dài dòng và lan man, tác giả quá tập trung vào những mạch truyện phụ chẳng liên quan mấy đến mạch truyện chính. Tác giả đã đặt ra những dấu chấm hỏi trong các vụ án, rồi sau đó truyện được kết lại với những câu hỏi đó, được bỏ lửng không ai trả lời.
2
400663
2015-07-07 01:25:14
--------------------------
214153
5417
Có vẻ lời giới thiệu quyển này làm không ít bạn tò mò và rước về đọc, mình cũng là 1 trong số đó :3 Cộng thêm cái bìa sách quá bắt mắt và kích thích nữa. Đây có thể nói là 1 trong những quyển trinh thám chất lượng nhất mình từng đọc. Các yếu tố trinh thám được phân tích kĩ, sâu và logic, motip vụ án mới lạ và không thể ngờ được. Mặc dù quá trình phá án còn hơi chậm chạp, nhưng như thế cũng là hợp lý vì manh mối thực sự quá ít. Khúc cuối khá là hay và kịch tính, 2 bên cân não đấu trí với nhau rất hồi hộp. Nghe nói đây là 1 quyển nằm trong series về vị cảnh sát này, rất hào hứng và mong được sớm đọc tiếp những tập khác.
4
413317
2015-06-24 16:43:21
--------------------------
213888
5417
Theo giới thiệu thì đây là tác phẩm trinh thám đầu tay của John Verdon, và là tác phẩm đầu tay đáng ngưỡng mộ, vì tác giả viết rất chuyên nghiệp.

Mình đã bị thu hút ngay từ tên truyện và để trí tò mò điều khiển vì đoạn giới thiệu ở bìa sau: "Bạn nghĩ tới một con số tự nhiên không quá 1000, xác suất để ai đó đoán được con số bạn chọn là bao nhiêu?" Mình quá tò mò là đằng khác, vời trò đoán số này, vì sao có thể đoán được người ta nghĩ gì nhỉ? Là lừa bịp hay thiên tài? Logic hay hư cấu? Từ động cơ này, mình đã mua sách và hoàn toàn chìm đắm vào đó.

Về vụ án, điểm nhấn chính là trò đoán số kỳ ảo. Hung thủ viết thư cho nạn nhân để cập đến trò đoán số, nạn nhân đoán và bị hắn biết chính xác. Không chỉ một mà còn lặp lại lần hai. Sau đó là hàng loạt chi tiết bí ẩn: tấm séc bị gởi trả, cái chết kỳ bí của Mark Merelly, những hàng dấu chân biến mất đột ngột, những manh mối để lại liên kết các vụ án và các nạn nhân tưởng như xa lạ với nhau, nhưng chi tiết kỳ quặc tưởng như vô lý nhưng khi ghép lại thì đã trở thành bộ ghép hình hoàn hảo. Càng đọc, mình càng thích truyện này, vì yếu tố kỳ bí lôi cuốn sự chuyên tâm và say mê của độc giả, sau đó là những lời giải thích hợp lý và thuyết phục. Quá đỉnh.

Ngoài ra, văn phong của John Verdon, và của dịch giả dịch rất hay, những bài thơ được dịch một cách mượt mà theo vần điệu, các câu thoại hết sức độc đáo và lắc léo mà nếu không đọc kỹ có thể bạn sẽ thấy khó hiểu, nhưng khi đã thấm rồi thì thật tuyệt, một cách viết trí tuệ, hài hước, rất đáng tôn là bậc thầy.

Hơi tiếc đoạn kết một chút, khá gấp gáp và không được thuyết phục lắm. Nhưng dù sao đây cũng là một tác phẩm rất trí tuệ và cuốn hút.
4
13737
2015-06-24 11:38:24
--------------------------
209341
5417
Một giọng văn mới trong thể loại trinh thám tâm lí và John Verdon đã ra mắt xuất sắc với Hãy Nghĩ Đến Một Con Số - cuốn sách mà một khi cầm lên thì sẽ đọc mải miết đến khi mắt đỏ hoe cả lên, tớ thề đấy!
   Tác giả với khả năng dẫn dắt trôi chảy rất thông minh và không có gì phải nghi ngờ, sức sáng tạo đáng nẻ của ông có dịp phát huy trong việc tạo ra những nhân vật nhiều mưu mẹo, cốt truyện kịch tính và nhiều sắc thái và không dễ gì quên được.
5
529844
2015-06-17 13:20:01
--------------------------
198007
5417
Đây là một bộ trinh thám xuất sắc,điều thu hút đầu tiên là bìa đẹp có sáng tạo.Đọc truyện rồi mới thấy cái thủ thuật mà thủ phạm dùng để lừa mọi người.Quả thật từ đầu đến cuối qua suy luận của mọi người rất khó để đoán được thủ phạm nhưng đến dần cuối khi thủ phạm lộ diện thì mình thấy rất bất ngờ.Truyện tạo ra sự lôi cuốn đã đọc là không thể dứt ra được,mang đến cái nhìn khác hẳn về truyện trinh thám.Một điểm trừ là mình thấy cách mọi người trong truyện suy luận nhiều lúc còn lòng vòng quá
4
526322
2015-05-18 22:23:15
--------------------------
185298
5417
Truyện đã lôi cuốn mình ngay từ cái bìa. Lúc đầu cũng hơi sợ vì mình lại “xem mặt mà bắt hình dong”, nhưng thật hên là truyện hay, đúng như cái bìa của nó. Có điểm mình thấy hơi kì 1 chút là nhân vật trong truyện hơi già rồi, gần 50 thì phải, nhưng dịch giả lại dùng từ ‘anh” để dịch làm mình thấy ngồ ngộ, lẽ ra xưng “ông” thì sẽ hợp lý hơn. Nhưng đó chỉ là 1 điểm trừ nho nhỏ thôi. Vụ án trong truyện làm mình bất ngờ thấy rõ, không theo motip nào mình từng đọc cả. tác giả có vẻ có thế mạnh trong việc miêu tả tâm lý, song song với vụ án, mình cũng khá thích thú khi đọc những đoạn về tâm lý nhân vật chính thay đổi theo từng ngày. Đây quả thực là 1 cuốn sách hay.
4
418163
2015-04-19 10:32:48
--------------------------
180591
5417
Đây là một trong những cuốn sách viết về truyện trinh thám mà mình thực sự khâm phục tác giả nhất. Một câu chuyện thu hút, lôi cuốn làm cho mình không dứt ra được. Khi mình đọc sách, giống như mình đang là nhân vật chính trong truyện, từng bước hòa vào những vụ án, nó giúp mình cảm thấy tội nghiệp hung thủ chỉ vì phải trải qua một cuộc sống bất hạnh mà hắn đã trở thành một người đàn ông máu lạnh như vậy. Tác giả lồng ghép mạch truyện rất chi tiết nhưng có vẻ như nhân vật chính rất hay bị phân tâm bởi những truyện xung quanh. 
Nói chung đây là 1 cuốn truyện trinh thám đáng đọc. Bìa sách rất ý nghĩa, có một sự liên quan đến cốt truyện :) Nếu bạn muốn có thêm một cuốn sách trinh thám hoàn hảo thì đây chính là cuốn sách viết riêng cho các bạn. Nó giúp mình cảm thấy hiểu biết hơn về cuộc sống, cuộc đời, nó mang cho ta hiểu về triết lý trong cuộc sống. Mang đến những vụ ấn tượng và logic làm cho mình rất muốn theo nghề cảnh sát để có thể phá những vụ án này nhưng có lẽ mình sẽ gặp nhiều trở ngại lớn. Hi hi nói vậy thôi nhưng mình đã rút ra được những điều tốt đẹp giúp mình có thể sử dụng trong cuộc sống
5
389227
2015-04-09 19:40:22
--------------------------
175877
5417
Chưa được đọc về tác phẩm này, thế nhưng khi vừa đọc xong những dòng tóm tắt ở hiệu sách đã khiến mình không thể kiềm lòng được, chỉ mún chạy ào về nhà nhờ tiki đặt em nó về cho mình thôi. Một sự sáng tạo cùng lối đi hoàn toàn mới, khiến cho những fan hâm mộ ở thể loại trinh thám như mình hoàn toàn bị cuốn hút, tò mò và mong muốn được cầm trên tay để trải nghiệm những màn đấu trí tuyệt đỉnh như thế. Mong là cũng có nhiều bạn đọc có cùng đam mê với minh ủng hộ để những vị tác giả có nhiều động lực viết tiếp những câu chuyện hấp dẫn hơn
4
197572
2015-03-31 10:34:00
--------------------------
175399
5417
Mình chấm quyển này 8.5/10 điểm. Lý do là: về hình thức rất tốt, bìa sách nhìn liên tưởng đến nội dung truyện, sáng tạo và bắt mắt. Nội dung vụ án rất mới lạ, quả thật đến gần cuối mình có đoán ra được hung thủ nhưng đọc vẫn thấy rất hấp dẫn nhờ màn đấu trí gay cấn của ông cựu thanh tra và tên hung thủ. Nhưng lý do mình không cho cao hơn 8.5 là do trong phần đầu suy luận của cựu thanh tra và mọi người còn hơi chậm làm người đọc khá sốt ruột, và ở đoạn cuối việc hung thủ tự lộ mình làm mình cũng không thích lắm.
4
476360
2015-03-30 08:27:27
--------------------------
173926
5417
Truyện trinh thám này có cốt truyện mới lạ nhất trong số những quyển trinh thám mình đọc gần đây. Không phải là kiểu kịch tính gay cấn kiểu hành động, mà là sự hồi hộp căng thẳng theo từng nhịp, nhanh có, chậm có khiến người đọc cũng lên xuống theo luôn. Điểm mình thích nhất là ở đây, tác giả không phải cho nhân vật chính của chúng ta quá hoàn hảo, hay có thể chiến đấu 1 mình, mà hầu hết các màn phân tích suy luận là sự hợp tác ăn ý của rất nhiều người, và vai trò của họ là không thể thiếu trong việc vén bức màn bí ẩn.
4
157796
2015-03-27 10:20:48
--------------------------
166147
5417
Thực sự mới đầu đọc review về nội dung, mình không thực sự có hứng thú với em í. Nhưng rồi nhìn bìa, mình lại không kìm lòng nổi mà rước em ấy về.
Dự định của mình là đọc những em khác mà mình mong mỏi bấy lâu trước. Thế mà chả biết sao lại lôi em ấy ra nghiền trước. 
Quỹ thời gian eo hẹp khiến mình không thể đọc một mạch tác phẩm này và đaya là điều khiến mình tiếc nuối. Bởi thực sự đây là 1 tác phẩm rất cuốn hút.
Cũng là truyện trinh thám, cũng là án mạng, cũng là phá án nhưng vụ án này lại rất khác. Tuy không có nhiều thời gian nhưng từ vụ án đầu tiên mình đã không thể bỏ xuống cho đến vụ án cuối cùng. Bởi mình rất tò mò, rốt cuộc thủ phạm là ai, là người như thế nào mới có cách giết người lạ lùng và tài tình vậy?
Phần mở đầu hơi dài. Mình đọc cứ mong mãi tới đoạn Mark Merelly chết. Nhưng dài, không có nghĩa là không hay. Bởi cả phần 1 đã đưa ra những manh mối, những dấu hiệu về 1 con người khó hiểu. Mọi thứ như mù tịt trước mắt Dave Gurney và Mark cũng như người đọc. Tuy nhiên, không thể nói là mình không hứng thú với những manh mối này mặc dù mình hầu như không thể hiểu tại sao hung thủ lại có thể đoán trúng con số 658, tại sao lại chọn Mark và làm cách nào mà mọi hành tung của hắn lại hoàn hảo như vậy? Và 1 thứ nữa làm mình tò mò - điều gì đã xảy ra với tuổi thơ của Dave và đứa con trai 4 tuổi của Dave đã mất như thế nào? Thật là xung quanh toàn sương mù mà!
Sang đến phần 2 và 3, án mạng xảy ra và cảnh sát tiến hành phá án. Mình thích mọi chi tiết trong chương này bởi nó như dẫn người đọc dần tới cuối đường hầm, nơi mà ánh sáng đang trải dài. 
Đọc phần này, mình cũng có 1 chút suy luận, cũng như tác giả cũng muốn người đọc có được những suy nghĩ, phán đoán của riêng mình. Và rồi, thực sự bất ngờ, tác giả khiến người đọc phải giật mình vì hung thủ thật quá quen thuộc. Và hắn đúng là 1 tên tội phạm kiệt xuất cùng tuổi thơ bất hạnh. Nói thật, mình vừa thấy kinh, vừa thấy cảm phục, là vừa thông cảm với hắn. Suy cho cùng, hắn lại chính là nhân vật mình thích nhất Biểu tượng cảm xúc pacman
Còn Dave quả thực là 1 cảnh sát về hưu rất xuất sắc. Mọi suy luận của anh đều rất chặt chẽ, logic và chính xác. Chỉ có điều, anh dường như khá dễ bị phân tâm bởi những điều xung quanh. 
Tác phẩm không chỉ đơn thuần là 1 cuốn tiểu thuyết trinh thám. Bởi tác phẩm này chứa đựng rất nhiều điều về cuộc sống, về con người, về triết lí. Đọc tác phẩm này, người ta không chỉ thỏa mãn về các hung thủ thực hiện án và cách phá án mà còn rút ra được khá nhiều cho bản thân.
5
369887
2015-03-11 23:05:31
--------------------------
157378
5417
Đây là một trong những cuốn truyện trinh thám hay nhất tôi từng đọc. Mạch truyện hấp dẫn, lôi cuốn kì lạ, khiến tôi không thể dứt ra khỏi cuốn truyện và không thể không cùng tư duy, cùng phán đoán với nhân vật chính. Điều làm nên cái hay của truyện là tính bất ngờ, không hề dễ đoán như nhiều truyện trinh thám tôi đọc trước đây. Nhưng chính vì thế, việc tự phán  đoán giảu thích được từng tình tiết, khúc mắc trong truyện, hay việc nhận ra được sự sơ suất của thám tử ( mà là sơ suất rất quan trọng, ko hiểu tại sao thám tử như thế lại bỏ qua, có thể là chủ ý tác giả kiểu người giỏi mấy cũng có sơ sót :p ) trở thành niềm vui tuyệt vời hơn cả. 
Tóm lại đây là cuốn truyện trinh thám rất đáng mua, đáng đọc ♥
5
289430
2015-02-08 17:42:06
--------------------------
153846
5417
Một quyển sách hấp dẫn thật sự, hấp dẫn ở những chi tiết nhỏ nhất. Nói về mạch truyện, nó không dồn dập, gay cấn như những quyển truyện trinh thám khác nhưng cũng không phải quá nhẹ nhàng mà là vừa đủ để người đọc có thể nắm bắt và cảm nhận được hết những tình tiết của vụ án cũng như hiểu rõ hơn tâm lý của những nhân vật. Nói về nội dung, tác giả đã rất tài tình khi lồng ghép những câu chuyện nhỏ về gia đình thanh tra Gurney xen lẫn những tình tiết về vụ án, những chi tiết về quá khứ và tuổi thơ của hung thủ cũng đã khiến người đọc hiểu hơn về tâm lý của hắn và phức cảm của hắn với mẹ mình. Hơn nữa, những chi tiết nhỏ nhưng đậm mùi trinh thám như cái ghế trong vườn, chai rượu bốn hoa hồng, đôi giày hồng ngọc hay những vần thơ cũng sẽ khiến người đọc tò mò và muốn tiếp. Bí ẩn về cách thức hung thủ đoán được con số nạn nhân nghĩ trong đầu nghe qua có vẻ bất khả thi nhưng lại được tác giả hóa giải vô cùng đơn giản và khó ngờ đến. Tóm lại, là một quyển truyện trinh thám đáng đọc. À về phần dịch thì dịch giả kì này đã làm rất tốt, đã dịch rất hay và uyển chuyển những vần thơ của hung thủ, khiến nó không mất đi chất thơ mà còn thêm màu bí ẩn.
5
36133
2015-01-27 18:46:22
--------------------------
142827
5417
Mình vốn thích đọc thể loại trinh thám nên đã đặt ngay khi cuốn này vừa lên Tiki. Có thể nói đây là một trong những cuốn sách trinh thám hấp dẫn nhất mình từng đọc.
Câu truyện bắt đầu với lời mong gặp mặt của một người bạn đại học lâu ngày không gặp của thám tử đã về hưu Dave Gurney. Từ cuộc gặp mặt ấy, Dave đã biết được một sự lạ lùng đến ghê sợ khi bức thư gửi từ một kẻ lạ mặt đến người bạn kia đoán trúng con số mà bạn ông nghĩ đến. Kể từ đó, những lá thư, những vụ án mạng nối tiếp nhau.
Đó cũng là lúc thám tử Dave dấn thân vào cuộc điều tra cùng tất cả cảnh sát bang, một cuộc đấu trí giữa kẻ sát nhân căm thù kẻ nghiện rượu và coi thường cảnh sát, luôn để lại những manh mối cố ý đánh lừa quá trình điều tra với cảnh sát. 
Đọc rồi là cứ đọc mãi. Dần dần bí mật về kẻ sát nhân cũng được hé lộ dần qua những manh mối và sự tư duy logic rất tài tình của thám tử Dave.
Nếu bạn chỉ đọc một cuốn tiểu thuyết trinh thám cho năm thì mình nghĩ đây chính là cuốn bạn nên đọc.
Cuốn này nằm trong series gồm 4 cuốn. Mong Nhã Nam xuất bản sớm cho đỡ phải ngóng chờ.
Đáng đọc! 5/5 sao!
5
459083
2014-12-20 04:58:59
--------------------------
