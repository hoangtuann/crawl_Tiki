393097
9176
Từ lâu tôi đã thích những cuốn sách có đề tài về bảo vệ công lý và lẽ phải nên tôi đã mua ngay cuốn sách này. Về hình thức bìa đẹp, giấy nhẹ, biên tập, nội dung dịch sát, ít lỗi chính tả. Mình chưa đọc nhưng rất háo hức để đọc cuốn sách này. Đọc xong sẽ đặt mua tiếp các cuốn sách khác của tác giả A. J. Cronin là Lâu đài của người bán nón và Thành trì như bình luận của các bạn. Cảm ơn tiki đã đem đến những cuốn sách hay như vậy cho độc giả!
5
97460
2016-03-08 09:43:21
--------------------------
312551
9176
Thanh gươm công lý là một tác phẩm thật sự hay và giàu tính nhân văn. Điều gì là chân lý thì dù có thử thách thế nào, gian khổ ra sao nó vẫn giữ nguyên bản chất của nó như sự thật không thể thay đổi. Paul đã quyết tâm đòi lại sự công bằng cho người cha của mình, và chính nghĩa luôn luôn thắng phi nghĩa. Đọc review thôi đã thấy nó là một tác phẩm kinh điển rồi. Tác phẩm cho ta thấy được một điều rằng: Luôn tin vào chân lý rồi ta sẽ được đền đáp xứng đáng. Cũng giống như mấy bạn, rinh luôn cả Lâu đài người bán nón và Thành trì về cho đủ bộ của Cronin. hi. Thanks tiki!
5
309333
2015-09-21 16:06:32
--------------------------
309358
9176
Một tác phẩm đáng đọc cho những ai yêu dòng văn học hiện thực cổ điển. Truyện đã phơi bày mặt trái của xã hội tư bản bấy giờ, người có địa vị, có danh vọng và được ca tụng chưa hẳn là kẻ tốt. Bên cạnh đó tác giả Cronin cũng đã nêu lên chân lý: chính nghĩa và công lý sẽ luôn chiến thắng một khi ta quyết tâm không từ bỏ. Sau khi đọc xong mình đã quyết định mua luôn Lâu đài người bán nón và Thành trì của Cronin luôn cho đủ bộ 3 quyển. 
Về hình thức, sách không có gì phải chê. Bìa đẹp, giấy tốt, nhẹ và xốp, biên tập kỹ càng, rất ít lỗi chính tả. 
5
391324
2015-09-18 23:22:18
--------------------------
286411
9176
Paul Mathry biết chắc chắn rằng cha mình không hề giết người. Ông hoàn toàn vô tội. Vì thế, chàng trai trẻ quyết tâm tìm mọi cách để minh oan cho cha mình và giải thoát ông khỏi nhà tù nơi ông đang bị giam giữ và phải lao động khổ sai. Va cuối cùng, người cha đáng thương của Paul Mathry cũng đã được minh oan, nhưng sau những tháng ngày chịu đựng sự bất công và tra tấn, ông đã trở thành một con người khác! Từ câu chuyện đó, những vấn đề về xã hội, về con người, về chân lý và công bằng được đặt ra, chắc chắn sẽ để lại trong lòng người đọc nhiều suy tư. Về mặt mỹ thuật, bìa sách được thiết kế đẹp, phù hợp không khí của tác phẩm, đồng bộ với 2 cuốn còn lại trong bộ ba cùng tác giả. Đây lả một cuốn sách thực sự đáng đọc.
5
307488
2015-09-01 18:35:36
--------------------------
256234
9176
A. J. Cronin là tiểu thuyết gia mà tôi cực kỳ ngưỡng mộ bởi những triết lý và quan điểm tuyệt vời mà ông đưa vào các tác phẩm của mình. Beyond this place (THANH GƯƠM CÔNG LÝ) không phải là một tiểu thuyết có độ dài hoành tráng nhưng những gì nó đem tới thì quả thực lớn lao. Người nào phạm tội thì người đó phải chịu sự trừng phạt, không thể chỉ vì lý do nào đó mà đem tội lỗi ấy vấy bẩn lên một người vô tội. Điều đó là không công bằng và phi lý tới cực điểm.
5
167283
2015-08-06 18:18:16
--------------------------
205202
9176
Đây là một cuốn tiểu thuyết đầy ấn tượng, không chỉ bởi nội dung mà còn ở những bài học được lồng ghép vào trong truyện.
Nhân vật chính của câu chuyện là anh chàng Paul tìm lại công lí cho cha mình - mắc phải tội giết người. Và những bí ẩn đằng sau vụ án của cha mình. Chứa đựng tình người và cả bộ mặt hiện thực của xã hội lúc bấy giờ
Nhà văn A. J. Cronin quá khéo léo đã tạo nên một tác phẩm tuyệt vời và có tầm ảnh hưởng như thế này.
Đây là một cuốn tiểu thuyết rất hay, bạn nên đọc
5
621744
2015-06-06 06:33:04
--------------------------
201356
9176
Paul đã làm được điều đó anh ấy đã làm cho công lí phải xuất hiện, phải hiện diện. Một diều tưởng chừng như khó có thể thực hiện nhưng Paul đã làm được điều đó! Paul đã chống lại hệ thống tư pháp lỗi thời của nước Anh thời đó bằng tình thương yêu cha, bằng niềm tin vào lẽ phải, bằng sự giúp đỡ của những người bạn...Nhưng những kẻ đại diện cho pháp luật đã tìm mọi các để ngăn Paul lại và suýt nữa đã làm được điều đó nếu không có... Thật đáng ngưỡng mộ, khâm phục họ mà, họ là những người dù bị đe dọa, uy hiếp hay khi đã nhận được sự nhượng bộ vẫn kiên quyết không chấp nhận vẫn tiến về phía trước về phía công lí. Bạn sẽ biết ngoài Paul ra còn có nhiều người nữa sẽ luôn bên anh cùng anh làm cho công lí phải xuất hiện. Kết thúc truyện ba Paul đã thay đổi nhưng... , mình mong Paul và cô gái kia sẽ gặp lại nhau. Không gì là không thể cuốn sách này đã nói lên điều đấy!! ^^
5
508336
2015-05-27 09:46:05
--------------------------
141192
9176
Thanh gươm công lý ra đời từ cách đây khá lâu - năm 1953, và đã có chỗ đứng trong danh sách The New York Time Best-seller nhiều tuần liên tiếp, đủ để biết sức hấp dẫn của tiểu thuyết này đến nhường nào. Cuốn sách kể về cuộc hành trình đòi lại công bằng cho cha của Paul - nhân vật chính trong tác phẩm. Anh phát hiện được một sự thật rằng, người cha bấy lâu thất lạc của mình vẫn chưa chết, thực ra ông đang ở tù và chịu mức án chung thân. Trong cuộc hành trình của mình, anh đã phát hiện ra bao nhiêu khúc mắc và uẩn khuất đằng sau bản án của cha mình. Qua đó, tác giả đã làm rõ sự bất công trong việc xét xử và ban hành án tử cho người nghèo, những người vô tội nhưng không có công, đồng thời đòi lại bình đẳng và công bằng cho họ. Tác phẩm như một lời tuyên bố, một bản cáo trạng dành cho hệ thống pháp lý đương thời.
Về mặt nội dung, tác phẩm này làm tôi rất hài lòng. Và tuy lời dịch của dịch giả không thực sự xuất sắc, thế nhưng cũng quá đủ để người đọc cảm nhận được tinh thần mà tác giả muốn gửi gắm trong tác phẩm này.
Cảm ơn Nxb Trẻ đã đem đến cho độc giả Việt Nam một tác phẩm có giá trị như vậy.
5
293316
2014-12-14 12:15:29
--------------------------
87206
9176
lâu rồi tôi mới đọc được 1 cuốn sách hay như vậy. Nội dung cuốn sách nó về hành trình tìm lại công lý sau bao nhiêu năm trời cho cha thoát tội giết người của 1 chàng trai trẻ. Tình cảm gia đình hiện rõ qua câu chuyện khi Paul chấp nhận hi sinh cuộc sống hiện tại, lao vào biết bao nhiêu cực khổ, thậm chí trở thành kẻ vô gia cư có thể chết trong đường phố bất cứ lúc nào. Tác phẩm cũng thể hiện những bộ mặt trái của xã hội lúc đó, việc 1 người tốt lương thiện có thể bị thay đổi đến mức nào khi bị bỏ tù oan ức hàng chục năm trời. Bản dịch theo đánh giá chủ quan là không thực sự chỉnh chu, sâu sắc, đôi chỗ hơi gượng nhất là việc dùng 1 số từ có thể làm cho các độc giả trẻ hơi khó khăn nhưng xét vào bối cảnh của tác phẩm có thể nói là ổn. 1 cuốn sách hay, 1 bản dịch chấp nhận được, không quá xuất sắc nhưng vẫn đủ sức làm hài lòng đọc giả
5
90637
2013-07-16 12:15:39
--------------------------
