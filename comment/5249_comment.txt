472978
5249
Cuốn sách 600 động từ bất quy tắc này quả thực rất hữu ích với những người muốn nâng cao trình độ tiếng anh và đang tìm sách để tự học như mình. Sách kích thước nhỏ gọn, vừa tay, dễ dàng mang theo; sách in màu, được trình bày rõ ràng, dễ hiểu và đặc biệt là nội dung giải thích rất cặn kẽ, dễ hiểu, dễ học. Ngoài ra còn kèm CD phát âm chuẩn, nghe rất được. Cuốn sách này đã giúp mình đc điểm cao môn anh. Mình luôn thích sách của the windy và đây quả thực là cuốn sách đáng để mua
4
1115883
2016-07-10 09:31:24
--------------------------
447047
5249
Đúng là cuốn sách này rất bổ ích cho những ai muốn học tiếng anh. Cách trình bày học, dễ thuộc. Nếu mà muốn tìm cũng rất dễ vì xếp theo bảng chữ cái. Cực thích luôn. Đĩa CD không bị xước, mình test thử rồi okay lắm. Mỗi tội mình chả hiểu bóc cái vỏ đĩa CD thế nào mặc dù đã có hướng dẫn thế là mình làm hỏng luôn cái vỏ :v Quyển cỡ vừa nhưng nếu mà bé hơn 1 tí nữa thì tiện lợi hơn việc mang đi dùng trong sinh hoạt, đặc biệt là mang đi học như mình =)))))
5
1286291
2016-06-13 13:36:38
--------------------------
435405
5249
Trời ơi,mình vốn đọc sách tùy hứng nên cần có cái kích thích,mình thích cách trình bày này kinh khủng luôn mặc dù đối với nhiều bạn thì hơi màu mè,chất lượng giấy tốt mà màu sắc cũng đẹp nữa,ngày nào cũng đọc hết,sách viết hay còn rõ ràng nữa.Nghe đĩa có người bản xứ phát âm chuẩn lắm.Rất nhiều động từ BQT mà trước đây mình không biết,mình chỉ học sách 360 động từ thôi,bây giờ biết thêm nhiều từ tuyệt thật.
Còn mở rộng thêm phần bài tập cho mình tự luyện nữa,rất hữu ích.
Mình thuộc 600 động từ trong vòng 2 tuần.
Sách của MCBooks đúng là rất tốt luôn.
5
1189963
2016-05-25 01:49:42
--------------------------
399567
5249
Tôi rất thích và đọc rất nhiều sách của The Windy,may mắn là tôi được người bạn giới thiệu cho cuốn sách 600 động từ bất quy tắc này,bạn tôi đã thi được điểm cao môn anh văn nhờ quyển sách nhỏ nhắn này đấy.Order sách trên tiki và khi nhận được sách tôi cảm thấy rất ưng vì cuốn sách được thết kế nhỏ gọn, vừa tay,có thể bỏ túi và cầm theo bất cứ nơi đâu để học mọi lúc.Ngoài ra,sách được in màu,rất rõ ràng,trình bày rất logic,làm cho người đọc dễ hiểu.Tôi khuyên những bạn nào đang học môn anh văn cũng như đang tìm sách để tự học tại nhà thì hãy nhanh tay sắm ngay cho riêng mình một cuốn nhé.
5
83467
2016-03-17 22:04:16
--------------------------
385510
5249
Động từ bất quy tắc tiếng Anh không phải dễ dàng học và nhớ và càng khó dần khi cấp độ lên. Cuốn sách 600 Động Từ Bất Quy Tắc Tiếng Anh (Kèm CD) khá tiện ích vì bây giờ đa số chỉ là sách liệt kê động từ bất quy tắc thôi chứ hiếm sách nào giải thích cụ thể, tường tận như này. Sách gồm 2 phần lý thuyết và bài tập. Phần lý thuyết có những chủ đề khá hay như: những lưu ý, nhưng cấu trúc theo sau ở thì quá khứ, bảng đtừ bất quy tắc, cách sử dụng một số từ dễ gây nhầm lẫn hay những ví dụ minh họa..
4
854660
2016-02-24 06:58:29
--------------------------
356134
5249
Sách về nội dung được chia thành các trường hợp khi nào dùng động từ các quy tắc, cách them ed và cách phát âm.
600 động từ được chia theo bảng chữ cái. Đồng thời tổng hợp các động từ khi ở cột 2 và 3 không thay đổi thanhf 1 list riêng.
Rồi phân biệt một số động từ hay dùng lẫn lộn với nhau và cho ví dụ để nhớ. Cho ví dụ các động từ thương hay sử dụng. 
Vì list này có 600 động từ mà trong thực tiễn chỉ dùng khoảng 200 từ bất quy tắc nên cơ một số từ đã xưa và không còn được dùng tới . Các bạn nên tra lại từ điển oxford để khỏi học tốn thời gian với các từ cổ :)
Sách kèm cd nên các bạn vừa học vừa nghe nhớ lâu
4
60249
2015-12-21 12:56:12
--------------------------
287596
5249
Sách này được tặng cho mình vào ngày sinh nhật.Sách rất hay. Cung cấp đầy đủ các động từ bất quy tắc mà một số sách trước đây mình dùng không hề có. Các thì được hướng dẫn dễ hiểu, dễ áp dụng làm mình có thể biết thêm nhiều ngữ pháp mới. Cuối sách có kèm đĩa CD hướng dẫn đọc với phát âm chuẩn,  hay, dễ nghe. Sách của MCBooks hay, giấy cũng thích và rất muốn gắn bó lâu dài với nhà xuất bản này!!!  Rất xứng đáng khi bỏ tiền ra để mua quyển sách bổ ích này
5
588562
2015-09-02 21:28:49
--------------------------
282817
5249
Mình đã đặt sách này và nhận được nó vào ngày hôm qua, sách rất hay, có nhiều động từ bất quy tắc, làm mình dễ dàng vận dụng trong các bài học hơn, ngoài ra đi kèm với sách là phần CD, phát âm chuẩn và mình cực thích. Kể cả bìa sách được trang trí rất lạ mắt, chất lượng giấy tốt. Nói chung thì quyển sách này đối với mình là trên cả tuyệt vời, nó làm cho việc học tiếng anh của mình thêm hiệu quả mặc dù trước đây mình rất ít khi tìm hiểu về tiếng anh, nhưng quyển sách này đã làm cho mình vượt qua ý nghĩ lười học tiếng anh đó ^^
5
714041
2015-08-29 14:13:19
--------------------------
280581
5249
Mình năm nay lớp 8 nên cũng sử dụng nhiều đến động từ bất quy tắc. Quyển sách này cũng có nhiều ưu điểm hay nhưng nó cũng có những nhược điểm không thể phủ nhận. Đầu tiên sách được trình bày chi tiết nhưng chưa logic theo mình thì bài  1 của sách nên là bảng động từ đi đã rồi hãy giải thích các phần khác sau. Cái bài 4 trong sách giải thích chưa được rõ dàng còn chỗ khó hiểu. Kích thước nhỏ gọn nhưng dày và cứng hơn so với những cuốn động từ khác . Phần CD của cuốn sách phát âm chuẩn mình rất thích . Một lỗi trầm trọng là phần mục lục của cuốn sách in sai số trang gây nhầm lẫn khi tìm kiếm thông.
3
613138
2015-08-27 20:18:25
--------------------------
229882
5249
Năm lớp 12 mình đã rất lo lắng vì việc học Tiếng Anh của mình,đặc biệt những hơn 300 động từ bất quy tắc mình còn chưa nắm vững nữa cơ.Mừng quá là mình đã tìm ra cuốn sách này và nó đã giúp ích mình rất nhiều trong việc học môn ngoại ngữ.Các bạn biết không,cuốn sách này không chỉ là 300 động từ bất quy tắc mà lên đến 600 động từ chưa hết trong sách còn đưa ra những ví dụ của các động từ này khiến chúng ta có thể hiểu cách dùng của nó,và các bạn lại có CD để nghe nữa chứ không như ngày xưa các anh chị không có internet toàn học tiếng Anh câm.Rất tuyệt phải không nào ^,^ mà cuốn sách này lại do MCbooks xuất bản nên mình rất hài lòng bởi giấy rất là mịn,dày dặn đã thế lại có màu làm chúng ta không mỏi mắt nữa.Đây quả thực một cuốn sách cần có cho chúng ta theo đuổi ước mơ chinh phục môn ngoại ngữ quốc tế này.
5
511194
2015-07-17 09:31:32
--------------------------
