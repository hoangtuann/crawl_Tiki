261412
9785
Sách bày cho ta cách nấu những món ăn bổ dưỡng phục vụ cho mục đích chữa bệnh: chữa bệnh tiểu tường, bổ gan thận, bổ máu, thức ăn cho thai phụ, chữa bệnh thường gặp cho trẻ, có cả cho đàn ông yếu sinh lý. Cái hay của sách là trong mỗi món ăn đều có mục đích công dụng để người đọc biết món ăn này sẽ tác động lên phần nào trong cơ thể, ăn tốt cho việc gì.
Tuy nhiên, mỗi món ăn được trình bày quá ngắn gọn, đặc biệt là 'Cách chế biến' chỉ vài dòng khá sơ sài, chung chung (hay do tôi không giỏi việc bếp núc nên thấy khó). Hơn nữa, bày những món ăn chữa bệnh thì ít nhất cũng nên có đôi trang giới thiệu về tác giả để người đọc cảm thấy yên tâm tác giả là người có hiểu biết, có kinh nghiệm trong lĩnh vục này mới đúng.
3
520793
2015-08-11 08:32:43
--------------------------
