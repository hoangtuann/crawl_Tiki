474799
2782
Tác phẩm này của Phong Điệp, chứa đựng đầy những bế tắc và khổ đau của các nhân vật- những kiếp người nhỏ bé với số phận éo le, bất hạnh, đè nặng trên vai là gánh nặng mưu sinh, là cơm áo gạo tiền. Cuộc sống của họ đầy lo âu, vất vả và bế tắc. Một vài truyện ngắn trong 10 truyện này đã làm tôi khóc rất nhiều, khóc vì cảm thương cho cuộc sống tuyệt vọng của họ, khó vì bản thân tôi cũng cảm thấy bế tắc, cảm thấy họ chẳng thể nào thoát khỏi kiếp sống đó.
4
412050
2016-07-11 22:50:05
--------------------------
