420462
11301
Truyện nói về một anh chàng sát thủ người Nhật, Hình như là phần 2, phần 1 tôi chưa đọc tuy nhiên cũng hiểu được 1 phần qua cốt truyện. 
Vị sát thủ này làm tôi hình dung ngay đến Jame Bond, nam tính, mạnh mẽ, và cả tính thu hút phụ nữ. Nói như vậy có lẽ hơi thiển cận, nhưng thực sự những tiểu thuyết về sát thủ, điệp viên, đây là cuốn đầu tiên. Nội dung truyện khá thu hút, có yếu tố chính trị, tuy nhiên khá rắc rối và khó hiểu, nếu tìm hiểu sâu sẽ rất thú vị. Chuyện tình cảm ít, chỉ thêm tí gia vị cho truyện đỡ khô cứng.
Điều tôi băn khoăn nhất có lẽ là lai lịch của nhân vật chín, tại sao lại trở thành sát thủ, cách anh ta áp dụng các biện pháp an toàn mọi lúc mọi nơi khiến người đọc như tôi cũng thấy khó chịu, thì ngoài đời có những con người như vậy tồn tại hay không? kĩ năng giết người điêu luyện đáng sợ, bị CIA truy đuổi, luôn có kế hoạch trốn chạy đến nơi an toàn, xây dựng một vỏ bọc an toàn làm tôi co cảm giác anh ta như một vị thánh.
Để xây dựng một tác phẩm như thế này đòi hỏi kiếm thức khá sâu rộng về luật, CIA, chính phủ, võ thuật, y học...... một tác phẩm khá thú vị,.
4
416211
2016-04-23 10:51:38
--------------------------
348230
11301
Cuốn báo thù là tập thứ hai trong seri bảy tập của jain john. Trước tập này là tập sát thủ Tokyo. Cả hai tập này là sự tiếp nối nhau. Truyện có tình tiết khá hấp dẫn kịch tính. Mình thích truyện trinh thám nên thấy hay. Tuy nhiên mình thấy vẫn còn một số nhược điểm. Tác giả cần cố gắng hoàn thiện hơn nữa trong các truyện tiếp theo để có nhiều truyện trinh thám hay hơn nữa. Nhìn chung cuốn này khá hay và vẫn đáng để mua và đọc. Hi vọng cuốn tiếp theo hay hơn.
4
1013915
2015-12-05 20:15:07
--------------------------
279507
11301
Cũng tương tự như cuốn Sát Thủ Tokyo, nội dung của cuốn này cũng chưa đủ để đạt đến mức hay. Cách hành văn khá rườm rà, dài dòng làm cho tiết truyện dài, làm cho đọc giả dễ ngán. Do đã lỡ mua Sát Thủ Tokyo nên mình đành mua luôn cuốn này nhưng cuốn này cũng chưa tạo cho được dấu ấn gì cả. Thật ra thì series này cũng khá dài, mình hi vọng những tập sau sẽ hay hơn để mình có thể tiếp tục đọc vì sau sự thất vọng của 2 tập đầu thì mình không có ý định đọc tiếp nữa. Chất lượng sách và cách trình bày sách ổn chỉ có phần nội dung sách thì không như mong đợi nên không làm mình hài lòng lắm.
3
548542
2015-08-26 21:17:38
--------------------------
276242
11301
"Báo thù" là phần 2 tiếp sau cuốn "Sát thủ Tokyo", đọc nói chung cũng được giống như phần 1 vậy, có kịch tính và cao trào, có những nút thắt mở hồi hộp, đúng chất hành động gay cấn kiểu văn học Mỹ và phim Mỹ. Về chuyện tình cảm thì hơi đáng tiếc cho mối tình của John Rain và nữ nghệ sỹ piano. Tôi không thích cô gái người lai trong tập này lắm, có vẻ như mỗi tập anh ta sẽ có một người tình thì phải. Nghe đâu bộ này có bảy phần, chẳng biết là còn tiếp diễn bao nhiêu vụ việc ám sát nữa khi mà nhân vật luôn muốn giải nghệ. Kể ra kết thúc ở hai phần là được rồi, tiếp tục có lẽ sẽ nhàm chán.
3
6502
2015-08-23 20:01:04
--------------------------
258882
11301
Đây là quyển sách tiếp theo của cuốn " Sát thủ Tokyo" , cách kể của Barry Eisler có gì đó khá lôi cuốn người đọc. Nhưng có thất một chút thì ban đầu cuốn sách vì kể hơi dài dòng. Phần này nói về cuộc sách của Rain sau khi giết Holtzez - giám đốc phân cục của CIA, và cũng thật tiếc cho anh chàng Harry tài giỏi vì một chút nông nỗi mà phải chết, cũng từ lúc đó chuyện càng trở nên hấp dẫn hơn có thêm nhiều  trận đấu nảy lửa, kịch tính. Không biết có thêm tập mới của bộ series này không nữa???
4
290697
2015-08-08 22:00:26
--------------------------
256010
11301
Chất lượng in, giấy: Tốt
Chất lượng dịch:	Tốt
Nhận xét:		Rain báo thù cho người bạn thân mặc dù anh đã gác kiếm ở Osaka. Yakuza máu lạnh Nhật Bản, đặc vụ CIA xảo quyệt, một câu chuyện hồi hộp đến nghẹt thở. Cũng giống ở phần trước, Sát Thủ Tokyo, Osaka được miêu tả tinh tế, đẹp một cách trầm mặc ở bề nổi và biết bao sóng ngầm, gian trá ở mặt kia. Vì bạn, một lần nữa Rain lại ra tay để rồi anh phải đối đầu với những thế lực mà bản thân anh phải kiêng sợ. Một mối tình nhẹ nhàng như hoa đào nở. Đời sát thủ đâu biết được ngày mai. Chuyên nghiệp, trắng đen rạch ròi.
Khuyên: 		Hay, nên đọc

4
129879
2015-08-06 15:32:32
--------------------------
235471
11301
Các nhận xét tiêu cực có vẻ từ các bạn chưa đọc tập trước (sát thủ Tokyo). Nếu như vậy thì oan cho cuốn này quá.
Các nhân vật có sự xuyên suốt từ tập trước, nếu không theo dõi từ đầu chắc chắn sẽ cảm thấy khó hiểu.
Ở tập này hành động kịch tính không nhiều mà đi sâu hơn vào nội tâm nhân vật, đưa ra cái nhìn sâu sắc hơn về một sát thủ không chỉ biết giết chóc. Phần báo thù cho bạn hơi "đơn giản", gây đôi chút hụt hẫng cho các fan của thể loại trinh thám hành động.
Tôi đánh giá cuốn này "chỉ" 4 sao vì hơi đuối so cuốn một, tuy nhiên vẫn giới thiệu bạn mua cuốn này và dĩ nhiên là cuốn "sát thủ Tokyo". Bản thân mong chờ tập tiếp theo của sê ri này
4
28303
2015-07-21 09:04:37
--------------------------
227416
11301
Mình đọc những lời giới thiệu thì thấy khá hấp dẫn vì viết về 1 sát thủ máu lạnh. Nhưng đến khi đọc sách thì thấy không được như mong đợi. 
Các tình tiết trong truyện viết lan man và dài. Các nhân vật rối rắm nên cũng khó nhớ, cứ phải lật lại đầu sách để xem lại. 
Truyện chủ yếu miêu tả tâm trạng của nhân vật chính John, các cảm xúc và cả các đoạn làm tình giữa John và các cô gái nên cũng chán. Đợi chờ những tình tiết gay cấn và ngẹt thở mà chẳng thấy đâu cả, Kết thúc nhanh chóng và có hậu nên khiến truyện giống như những quyển tiểu thuyết tình cảm. Cá nhân mình thì thấy hơi tiếc vì đã mua quyển sách này.
1
126889
2015-07-14 10:08:01
--------------------------
210959
11301
Thực sự khi đọc xong cuốn đầu tiên về John Rain trong sát thủ Tokyo, mình khá mong chờ với phần Báo thù này. Nhưng có vẻ mong chờ nhiều thất vọng sẽ nhiều, truyện có vẻ không đào sâu vào yếu tố kịch tính và hành động. Mình đã nghĩ John sẽ rất điên khi thực hiện việc báo thù cho người bạn của mình, nhưng mình không cảm nhận được tính cách sát thủ của John được thể hiện thực sự qua câu chuyện này. Đã từng trông đợi những đặc vụ xảo quyệt và tình tiết nghẹt thở, nhưng không thấy:(
3
26110
2015-06-19 22:41:43
--------------------------
149356
11301
mình mong đợi cuốn này sẽ gặp một gã sát thủ điển trai, lạnh lùng ra tay tàn khốc nhưng càng đọc lại càng thấy mình mơ hão quá.
câu chuyện k phải là dở nhưng mà là thấy rất nhàm, cốt truyện tác giả cố xây dựng thật gay cấn nhưng thật sự đọc vào k có cảm giác mong chờ cái tiếp theo, cách jonh rain báo thù cho harry cũng k có gì ghê gớm hay hồi hộp cả. cách tác giả hay sử dụng thêm các từ tiếng nhật và phiên dịch lại nó khiến mạch truyện có vẻ bị ngắt quãng.
thật thất vọng
3
187796
2015-01-13 14:47:32
--------------------------
128110
11301
Câu chuyện được viết về một sát thủ chuyên nghiệp, ban đầu có vẻ gay cấn...  nội dung câu chyện thực ra không nhiều nhưng có lẽ vì không hợp với lối viết trải dài, mô tả mất đến hàng mấy trang làm tôi cảm thấy chán.
Mãi đến hơn 2/3 cuốn truyện, khi "sát thủ" bắt đầu thực hiện cuộc báo thù cho Harry thì tôi mới nhận thấy phần hấp dẫn chút ít, kết thúc câu chuyện là một kết cục có hậu cho Sát thủ và Naomi.
Phần bìa và phần dịch của cuốn truyện tạm ổn.
Đánh giá cho sản phẩm là 3*.
3
310373
2014-09-29 10:17:39
--------------------------
