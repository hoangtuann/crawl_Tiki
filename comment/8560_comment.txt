349514
8560
Tôi rất thích những cuốn sách kiến thức cho trẻ em vì có hình minh họa và giải thích ngắn gọn nhưng dễ hiểu. Phần lớn mọi người khi đã cầm quyển sách này trên tay đều chăm chú đọc cho đến trang cuối cùng vì bắt gặp trong sách toàn những câu hỏi về các hiện tượng rất phổ biến mà ai cũng đã từng trải qua nhưng không phải tất cả mọi người đều biết lý do tại sao những hiện tượng đó lại xảy ra. Có những cuốn sách như thế này sẽ hỗ trợ rất nhiều cho người lớn khi cần giải đáp cho  những thắc mắc của các bạn nhỏ.
4
922206
2015-12-08 16:53:31
--------------------------
264700
8560
Gần sắp lên 3 rồi, con bắt đầu hỏi mẹ hàng loạt câu hỏi về cơ thể như: Phổi là gì? Phổi để làm gì? Đầu gối để làm gì? Mũi để làm gì?...Do đó mình đã tìm qua nhiều sách về cơ thể người để minh họa và diễn giải cho con. May sao tìm được quyển sách này. "Em Muốn Biết Vì Sao: Bụng Em Sôi Ùng Ục" là một quyển sách bổ ích dành cho lứa tuổi thiếu nhi. Với tranh minh họa đẹp, nội dung sách khá cơ bản để các em nhỏ tìm hiểu về các bộ phận và chức năng của các bộ phận đó trong cơ thể. Một trong những quyển sách cần có để thỏa mãn phần nào sự hiếu kỳ khôn cùng của các cô cậu nhỏ.
5
352823
2015-08-13 13:01:49
--------------------------
253140
8560
Mình đã tìm mua nhiều quyển sách nói về cơ thể con người cho bé đọc nhưng quyển này là ấn tượng nhất. Hình vẽ to, rõ ràng, đẹp, nội dung  những hiện tượng, bộ phận xoay quanh cơ thể bé , những điều diễn ra hàng ngày được gải thích hết trong quyển sách này như hắt xì hơi, nổi gai ốc, đau bụng. Hình minh họa tim , phổi, dạ dày ...giúp bé hiểu và biết chính xác các bộ phận bên trong cơ thể. Mình gọi đây là sách bách khoa tòan thư nhỏ xíu về cơ thể viết cho trẻ em :)
5
110100
2015-08-04 10:38:43
--------------------------
