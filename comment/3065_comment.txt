358142
3065
Mình biết tới quyển sách này do tình cờ 1 lần sinh hoạt trong câu lạc bộ tiếng anh của trường được các anh chị khóa trên giới thiệu. Các anh chị nói nếu muốn thi TOEIC đạt điểm cao (> 750 điểm) nên luyện nghe trong quyển sách này. Mặc dù đã được xuất bản năm ngoái nhưng cho đến lúc mình mua xong thì mình thấy kiến thức của nó đem lại cho mình không lạc hậu tí nào. Tuy nhiên, quyển sách này đòi hỏi phải biết được một số lượng lớn từ vựng mới có thể hiểu được, phát âm rất chuẩn. Những bạn muốn đạt điểm cao thì nên luyện phần nghe trong quyển này
5
297258
2015-12-24 21:07:27
--------------------------
