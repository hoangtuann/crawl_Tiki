391821
2668
"10 bí mật của tình yêu" có nội dung khá thú vị. Mặc dù lối kể chuyện lặp lại theo một cấu trúc nhưng khá dễ hiểu nên ngươi đọc có thể hiểu ngay. Tác giả viết về một chàng trai bị mất niềm tin vào tình yêu, thật may mắn khi tại đám cười của người bạn anh đã gặp được một người thầy chỉ cho anh tìm lại tình yêu của mình. Mỗi phần chàng trai gặp một người thầy khác kể cho anh một cầu chuyện và một bí mật. Sách cho biết cách thể hiện tình yêu, và quan trọng nhất là duy trì và hàn gắn tình yêu đó. Qua những câu chuyện bạn có thể hiểu được những khúc mắc trong tình yêu thường bắt nguồn từ đâu. Và tìm được cách giải quyết vấn đề của mình.
Hãy đọc sách với tâm trạng vui vẻ bạn nhé.
3
204540
2016-03-05 21:04:03
--------------------------
