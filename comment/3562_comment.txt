463761
3562
Mình đã thích quyển sách này từ lâu  mà không có đủ tiền mua vì giá bìa nó hơi đắt. Nay mình được bạn bè tư vấn trang Tiki, mình hốt ngay em này về luôn, mình vui lắm. Phải đánh giá rằng chất lượng quyển sách rất tốt vì nó vẫn còn giữ nguyên cái bìa trong được bọc bên ngoài. Từng nét chữ, từng trang sách vừa rõ vừa sạch, không có trang nào bị bất cứ lỗi gì. Thật an tâm khi đặt sách trên Tiki. Mua hàng trên Tiki vừa chất lượng vừa tiết kiệm được nhiều tiền thật là thích.
5
1395382
2016-06-29 16:26:20
--------------------------
217702
3562
Đầu tiên, mua sách về nhìn bề ngoài đã :) Cầm trên tay cuốn sách này thấy rất hài lòng. Vì bìa sách chắc chắn, giấy in cũng đẹp, không bị lem hay nhoè mờ như nhiều loại sách tham khảo giá rẻ trên thị trường. Về nội dung, sách tuy chỉ có 125 bài và đoạn, ít hơn so với nhiều sách khác nhưng chất lượng thì không phải bàn, hầu như những bài mình đã đọc đều hay, đề văn sát với chương trình sách giáo khoa. Chắc vì hình thức và nội dung đều tốt nên giá sách khá chát, khiến mình phải cân nhắc khá lâu trước khi rước về :)
4
74616
2015-06-29 20:11:47
--------------------------
