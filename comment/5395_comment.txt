567153
5395
Cuốn sách hay nhất mà mình đã từng đọc. Cảm ơn tác giả
4
4790999
2017-04-08 00:42:19
--------------------------
531425
5395
Cuốn sách đưa ra những mẫu chuyện về những bạn trẻ đang gặp chướng ngại về cuộc đời họ và đâu đó là hình ảnh của tôi. Thật may măn khi được đọc quyển này và có một góc nhìn mới hơn về cái lứa tuổi tưởng là biết thật nhiều, nhưng thật ra lại ko biết gì. 
5
2184106
2017-02-24 23:42:50
--------------------------
511529
5395
Mỗi người vào mỗi khoảng thời gian sẽ có những cuốn sách để lại dấu ấn không thể xóa nhòa . Với tôi, hiện tại đó là Quiet, là The Defining Decade - Tuổi 20. Thật may mắn khi đã đọc được cuốn sách này trong những năm tháng tuổi 20 đầy hoang mang, khủng hoảng. 
Thiết nghĩ mỗi người trẻ tuổi 20 ít nhất hãy đọc một lần.

"Tương lai không được viết trên những vì sao. Không có sự bảo đảm nào cả. ... Bạn đang quyết định cuộc đời mình ngay lúc này đây "
5
89906
2017-01-16 15:51:21
--------------------------
471020
5395
Mình đọc quyển sách này khi đã gần 23 tuổi. Thời điểm đó mình đã đi làm được một năm, trải qua ba công ty khác nhau. Tuổi trẻ nhiều hoài bão và tham vọng, thật sự lúc đó mình cảm thấy khá lạc lõng, hoang mang về cuộc sống, công việc mình đang làm. Đọc những mẫu truyện của tác giả Meg Jay tư vấn cho các bạn trẻ, mình thấy đâu đó hình ảnh của bản thân mình - một người suy nghĩ khá hời hợt, thiếu kinh nghiệm, sống và quyết định dựa vào cảm nhận của người khác. Nhờ quyển sách này, mình đã can đảm rẽ hướng sự nghiệp, mở rộng các mối quan hệ, dám trải nghiệm và yêu tuổi trẻ của mình hơn. Nhìn chung, các bạn sinh viên (17- 20 tuổi) nên đọc quyển sách này sớm để thêm trân trọng tuổi trẻ vì mỗi người chỉ có một tuổi trẻ mà thôi!
5
948935
2016-07-08 09:56:59
--------------------------
464263
5395
Đây là một trong những cuốn sách khá hiếm hoi thuộc thể loại self-help mà tôi thấy nó thực sự có ích, tức là nó có nghĩa thực tiễn, không mang nặng lý thuyết, phân tích khá sâu, không mang tính dạy bảo bạn "phải" làm cái này cái để đạt được cái kia mà giống như giúp bạn thấy được con đường của chính mình. Một điểm tôi không thích là nó hơi rườm rà ở các ví dụ, câu chuyện ( đặc điểm chung của self-help :D), dù nó có súc tích ngắn gọn hơn các cuốn sách khác mà tôi từng đọc nhưng vẫn không thích :(
4
256405
2016-06-30 00:11:36
--------------------------
458248
5395
Mỗi phần trong cuốn sách bao gồm những câu chuyện được trích dẫn từ những buổi trị liệu tâm lý của tác giả, qua mỗi câu chuyện người đọc sẽ nhận thấy được phần nào cảm xúc suy nghĩ của mình trong đó. Cuộc đời chúng ta chỉ có sự nghiệp và tình yêu hoặc tình yêu và sự nghiệp. Đối với sự nghiệp, tác giả đã trình bày những sự lo lắng bâng khuâng bấp bênh của những con người trẻ tuổi khi đưa ra lựa chọn về công việc của bản thân, ảnh hưởng của sự bán thất nghiệp đến đời sống của giới trẻ, những suy nghĩ đang diễn ra trong đầu của họ vì bị ảnh hưởng bởi quan điểm "bạn có thể làm được tất cả mọi việc bạn muốn". Đối với tình yêu, gia đình, quan điểm của tác giả là muộn chưa hẳn là sẽ tốt hơn, tác giả đã đưa ra mô hình 5 tính cách để giúp ta nhận biết được những nét đặc trưng của đối phương để có thể hoà hợp với nhau tốt hơn. Nói tóm lại, mình đã học được rất nhiều điều hay khi đọc được quyển sách này, còn bạn thì sao?
5
12054
2016-06-24 23:33:39
--------------------------
448076
5395
Tôi mua cuốn sách này khi tôi tròn 20, khi tôi đang ở độ tuổi chông chênh nhất..vừa nhìn thấy tiêu đề t đã vớ ngay mà không suy nghĩ, và đó cũng là lần đầu tiên t đặt mua sách trên tiki. Thật ra cuốn sách này không phải gọi là quá lôi cuốn để có thể kiên trì đọc nó vì đôi khi nó thật sự rất khô khan. Nhưng nó cần cho những ai sẽ đang và đã 20. Không phải là những lời nói sáo rỗng nói suông mà cuốn sách là sự lồng ghép những câu chuyện thực tế  trong quá trình tác giả trao đổi và điều trị cho bệnh nhân tâm lý của mình ở những chủ đề nhất định. Cuốn sách có thật sự có ích hay không là tuỳ thuộc vào cách ng đọc tiếp nhận nó, nhưng t nghĩ bạn sẽ có thêm nhiều định hướng nhiều giải pháp cho tuổi 20 và cả 20+ của mình khi đọc cuốn sách này
4
517383
2016-06-15 12:26:52
--------------------------
432374
5395
Như trong sách, những năm tuổi 20 là thời kì phát triển thứ 2 của não bộ sau
thời kì chúng ta mới được sinh ra, là thời kì mà chúng ta háo hức, muốn trải
nghiệm với những điều mới mẻ. Đi cùng với đó là những lúc tụt cảm xúc, và những 
lúc bồng bột của tuổi trẻ.
Mình thích hình ảnh tác giả ví những năm tháng tuổi 20 "như những chiếc máy bay
rời thành phố New York để đến nơi nào đó ở phía Tây. Ngay sau khi cất cánh, một
thay đổi nhỏ trong hành trình sẽ tạo nên những khác biệt giữa việc hạ cánh ở
Seatle hay ở San Diego. Nhưng một khi máy bay đã gần đến San Diego, chỉ cần một
cú ngoặt lớn, nó sẽ lại chuyển hướng bay về phía Tây Bắc."
Trong sách tác giả không nói ta nên làm gì mà đưa ra những câu chuyện giống với
ta để phân tích và giải đáp, chuyện như trúng tim đen của mình khi nói vể công
việc và tình yêu.
Trong công việc, khi ra trường ta hay nhảy việc nhiều mà quên đi cái quan trọng
là tích luỹ vốn sống sự nghiệp. Ta hay bị phân vân giữa những hướng đi mới, bị 
bối rối giữa những lựa chọn
nhưng
thực chất ta chỉ có khoảng 6 hướng đi, và chính bạn thích mình muốn điều gì.
Và bạn nên bắt đầu 1 mảnh ghép đó. Từ mảnh ghép đó để ghép lại mảnh ghép lớn hơn.
Trong sách, mình thấy tác giả đưa ra một chi tiết hay về cách viết đơn xin việc.
Nhà phỏng vấn thích nghe một câu chuyện từ quá khứ liên quan đến hiện tại về
nghề của bạn, nó là một quá trình, một vốn sống để họ biết bạn có phù hợp với
công việc và hướng phát triển của bạn trong tương lai.
Đây là một cuốn sách hay nên đọc.
4
70936
2016-05-19 00:52:21
--------------------------
409306
5395
Mình được giới thiệu quyển sách này từ người bạn thân. Bản thân mình đã đọc và cảm nhận nội dụng sách hay và thiết thực. Tác giả là người có kinh nghiệm nhiều năm làm vịêc và tư vấn cho những khách hàng trong độ tuổi 20. Nội dung quyển sách được hình thành từ những trải nghiệm của bản thân cô và những khách hàng của mình.
Cá nhân mình nhận ra nhiều điều hay và bổ ích khi đọc quyển sách này. Mình nhìn thấy bản thân trong những lo lắng, sợ hãi và tình huống mà tác giả nêu ra. Mình nhận ra những quan điểm sai lầm hay những khúc mắc đã có trong khỏang thời gian sau khi tốt nghiệp đại học và cảm thấy mất phương hướng với cuộc sống.
Mình đánh giá cao giá trị mà tác phẩm mang lại cho người đọc trong việc giúp những bạn trẻ sắp và đang trải qua tuổi 20 của mình. 
Nếu lời dịch đựơc rõ ràng và thống nhất hơn thì mình nghĩ người đọc sẽ dễ hiểu và dễ liên kết nội dung hơn :)
4
7681
2016-04-02 11:54:45
--------------------------
408575
5395
Người ta thường nói 20 là tuổi đẹp nhất của con người. Đó là những năm tháng bạn mới bước ra cuộc đời với biết bao ước mơ, lí tưởng, khao khát được thể hiện bản thân, tự do bay nhảy. Nhưng đó cũng là độ tuổi của những khủng hoảng với một số bạn bị đẩy ra ngoài xã hội mà không xác định được mục tiêu, lí tưởng sống của mình là gì
Cuốn sách mang tính chất giáo dục nhưng không hề khô khan mà ngược lại nó như một lời tâm sự, chia sẻ về tuổi hai mươi thông qua những câu chuyện nhỏ.
5
1210645
2016-03-31 21:46:55
--------------------------
407227
5395
Đây là quyển sách nằm trong top hay nhất mà mình từng đọc. Cuốn sách này có ý nghĩa giáo dục rất lớn và gần như ngang tầm với  những cuốn sách của Adam Khoo. 20- cái tuổi mới bước vào đời với những bước chập chững ban đầu nhưng lại là cái cột mốc quan trọng để ta đưa ra những quyết định đúng đắn cho cuộc đời  Vì vậy mình nghĩ các bạn nên đọc quyển sách này để có thể đưa ra những quyết định sáng suốt cho cuộc đời các bạn. Bên cạnh đó mình thấy bìa sách cứng và đẹp chỉ tiếc là không có bookmark.
4
462768
2016-03-29 18:33:09
--------------------------
405735
5395
những ai đang và đã trải qua tuổi 20 đều có những vấn đề và rắc rối trong cuộc sông. Cuốn sách như một lời giải đáp, một lời khuyên dành cho những vấn đề đó. Lời văn không hề cứng nhắc hay khô khan mà ngược lại rất dễ đi vào lòng người. Những ước mơ, những hoài bão và tương lai của bạn sẽ có được từ những cố gắng, dám nỗ lực, dám vươn lên của bạn hôm nay. Hãy đọc và cảm nhận, bạn sẽ có được những lời khuyên và động lực, tác phẩm không chỉ dành cho nhữn bạn tuổi 20 mà còn phù hợp với những bạn chưa và đã trải qua nữa :)
4
582712
2016-03-27 09:02:30
--------------------------
403005
5395
Càng đọc bạn sẽ càng cảm thấy tuổi trẻ thời 20 như quy luật 80/20 vậy. Những năm tháng của tuổi 20 chiếm khoảng 20% nhưng quyết định cả một tương lai lâu dài về sau. Sách còn giúp tôi có những quyết tâm và không lạc lối hơn ở độ tuổi đầu đời. Có những quan niệm lối mòn dường như được dỡ bỏ về tình yêu, tuổi trẻ và gia đình. Tuy nhiên mục tiêu của sách này rất hạn chế người đọc. Khoảng gần 30  tuổi trở đi bó sẽ không còn tác dụng nữa vì tác giả chỉ hướng đến dẫn dắt cho những người nằm tầm trong khoảng 20.
4
1144711
2016-03-23 06:56:27
--------------------------
397805
5395
Tôi chỉ ước được đọc quyển sách này sớm hơn. Nhưng có lẽ vẫn chưa quá muộn, tôi vẫn chưa bước qua sườn dốc của cuộc đời (như nhiều người nói, là tuổi 30). Hy vọng tôi bắt đầu hoạch định lại cuộc đời bây giờ là không quá ngớ ngẩn.
Quyển sách không phải là 1 phát kiến, những điều viết trong đó là những điều bạn đã từng nghe, thậm chí nghe nhiều lần, nhưng cách truyền đạt của tác giả rất thấm, bạn sẽ tìm thấy mình vài lần trong những dòng kể chuyện của bà. Những giải pháp và lời khuyên của bà đưa ra cũng không hề sách vở, mà thực tế, đôi lúc quá phũ phàng. Tôi thực sự hy vọng những bạn trẻ sẽ tìm đọc quyển sách này, để không phải có những khoảng thời gian tiếc nuối như tôi.
4
739383
2016-03-15 14:21:42
--------------------------
392723
5395
Trước đây tôi có đọc một quyển sách cũng có tên tương tự là "Nếu Tôi Biết Được Khi Còn 20" của Tina Seelig và tôi đã kì vọng nó cho mình một cái nhìn rõ ràng về độ tuổi đầy thay đổi này. Nhưng thật ra nó chỉ đơn thuần là một quyển sách về khởi nghiệp, thiết lập mối quan hệ và quản trị. ĐÂY mới chính là quyển sách mà (theo tôi) những người trong độ tuổi 20 thật sự cần.

Quyển sách được viết bởi một nhà tư vấn tâm lí trị liệu, Meg Jay, về những con người trong độ tuổi 20 đã từng đến hỏi xin lời khuyên của bà về công việc, tình yêu và cuộc sống. Từ những câu chuyện đó, bà bắt đầu giải mã những quan niệm sai lầm về tuổi trẻ và đưa ra một cái nhìn thực tế hơn về việc hoạch định tương lai.

Đã bao giờ bạn đứng trước quá nhiều sự lựa chọn và cách giải quyết của bạn là trì hoãn quá trình đưa ra quyết định? Bạn nghĩ rằng còn quá sớm để gắn bó với việc nào đó và rằng bạn còn nhiều thời gian để bay nhảy giữa công việc và những mối tình chóng vánh? Đây là những điều mà đa số những người đang trong quá trình trưởng thành và hoàn thiện nhân cách chắc chắn sẽ mắc phải. Điều bạn cần phải nhớ là mọi sự việc sẽ không tự nó yên ổn khi bạn già đi nếu không hề có sự chuẩn bị nào. Trưởng thành là một quá trình tâm lí không phụ thuộc vào độ tuổi và việc bạn nhận ra điều đó càng sớm bao nhiêu, càng sống có trách nhiệm với bản thân và lo nghĩ cho tương lai nhiều bao nhiêu sẽ quyết định mức độ hạnh phúc của bạn ở phần còn lại của cuộc đời.

Tóm: 4/5, bản dịch có vài chỗ tối nghĩa. Sách dành cho "tuổi trẻ lạc lối"
4
680759
2016-03-07 15:45:59
--------------------------
389365
5395
Cuốn sách này cực kỳ thiết thực với độ tuổi mới lớn. Dù là sắp đến, chạm ngưỡng hoặc mới bước qua tuổi 20 thì bạn cũng nên đọc quyển sách này. Cuốn sách đưa ra những câu chuỵên thực tế, qua đấy như một lời khuyên, lời dạy bảo chân tình. Nếu bạn đang loay hoay với tuổi 20 với những rắc rối tâm lý, bạn nên cầm sách trên tay và suy ngẫm để hòan thiện bản thân mình. 20 tuổi là bước chân đầu tiên chập chững vào đời, hãy đọc những cuoín sách kĩ năng để bản thân thêm vững vàng và hiểu bíêt.
3
857944
2016-03-01 21:51:40
--------------------------
385139
5395
Mình đã đọc rất nhiều review về cuốn sách, nó là 1 trong những cuốn sách sách giúp vượt qua khủng hoảng tuổi 20. Vì không thể tìm mua version tiếng Anh nên mình đành mua version tiếng Việt. Cuốn sách rất có ý nghĩa, nói rất đúng từ công việc, tình yêu, các mối quan hệ với những câu chuyện thực như là những dẫn chứng minh họa.
Mình thấy đúng nhất là bạn bè thân thiết chơi với nhau chỉ là để gặp nhau tâm sự, chơi đùa vì chúng ta có nhiều điểm chung, nhưng chưa chắc chúng ta đã học hỏi thêm được nhiều vì họ và ta có nhiều điểm chung. 
Và chúng ta lại nhận được sự giúp đỡ nhiều từ những người bạn mới quen hoặc ít khi gặp nhau điều này rất đúng với bản thân mình.
Còn trong tình yêu cuốn sách nói rất đúng về tình trạng hôn nhân trễ, mình cũng là người xu hướng kết hôn trễ, và luôn nghĩ rằng cứ từ từ, nhưng rồi đó chính là sự trì hoãn, đến khi hơn 30 mình sẽ phải lo lắng và áp lực vì không lo chọn người yêu trước 30.
Về công việc thì càng đúng hơn vì mình đang đi làm nên càng nhận thấy tác giả viết rất đúng.
Nói chung những ai đang gần ra trường thì nên đọc cuốn sách này, hoặc thậm chí những ai đã đi làm cũng nên đọc.
5
696593
2016-02-23 16:47:49
--------------------------
377290
5395
Xuyên suốt toàn bộ cuốn sách là thông điệp: những năm 20 là tuổi của khủng hoảng, thay đổi, trải nghiệm và trưởng thành.
Điều thích nhất trong cuốn này là tác giả chắt lọc những kinh nghiệm, ví dụ thực tế trong quá trình điều trị tâm lý cho những khách hàng tuổi 20, kết hợp với một vài dẫn chứng khoa học có tính thuyết phục cao. Nội dung súc tích và dễ cảm nhận :)))))
Đọc sách mà cảm tưởng như đang nói chuyện với chính bản thân mình :)
Mình sẽ mua thêm vài cuốn tặng bạn bè...
5
628918
2016-02-02 09:41:33
--------------------------
362735
5395
Mình biết đến quyển sách này qua 2 nguồn.Nguồn thứ 1 là từ anh Nguyễn Tiến Đạt- một bạn trẻ nổi tiếng với việc dịch cuốn sách Quiet và đạt 8.5 ielts nhờ tự học. Nguồn thứ 2 là từ bài tedtalk của tác giả Meg Jay với tiêu đề " Tại sao 30 không phải tuổi 20 mới?"
Quyển sách này chính xác viết về những gì mà tác giả Meg Jay chia sẻ trong bài tedtalk. Nếu các bạn muốn review nội dung cuốn sách trước khi quyết định mua sách, mình chân thành khuyên các bạn hãy xem bài tedtalk 15 phút này. Bạn sẽ thấy thấm thía bởi những điều mà tác giả- một giáo sư ngành tâm lí học này nói đến. Cô là người chuyên tư vấn tâm lí cho các bạn trẻ trong độ tuổi 20-30, và cô nhận ra được những vấn đề mà họ đang gặp phải. 
Chẳng hạn, trong tác phẩm, Meg Jay nhắc đến một trường hợp của anh chàng Ian. Mình xin trích lại những dòng bộc bạch của anh chàng này khi đến với cô: "Ian nói với tôi rằng những năm tháng tuổi 20 của cậu giống như ở giữa đại dương, giữa một vùng nước lớn không xác định. Cậu không thấy đất liền, nên không biết cần đi về hướng nào. Cậu cảm thấy choáng ngợp trước viễn cảnh mình có thể bơi đến bất kỳ đâu hoặc làm bất cứ điều gì. Cậu cũng đồng thời cảm thấy tê liệt trước sự thực rằng cậu không biết điều nào trong số những điều bất kì được coi là lựa chọn đúng đắn. mệt mỏi và vô vọng ở tuổi 25, cậu nói rằng cậu đang đạp nước tại chỗ để có thể sống sót". Liệu bạn có bao giờ cảm thấy như vậy không?
Đó có thể là tâm trạng của bất cứ bạn trẻ nào trong ngưỡng cửa tuổi 20, khi các bạn rời ghế nhà trường và phải đưa ra lựa chọn về tương lai sau này. Cuốn sách này dành cho các bạn trẻ đang gặp khủng hoảng tinh thần của tuổi 20- trong công việc, tình yêu, mối quan hệ... Những chương sách đều là những case vô cùng điển hình về khủng hoảng mà các bạn sẽ thấy ở chính mình và bạn bè của mình. Mình rất mong các bạn sẽ tìm thấy câu trả lời cho mình khi tìm đọc cuốn sách. 
5
917004
2016-01-02 18:29:13
--------------------------
353229
5395
Mặc dù đã qua tuổi 20 rồi nhưng tôi vẫn quyết định mua quyển sách này do tình cờ thấy được trên wall page sách trích dẫn. Và khi đọc xong tôi cảm thấy hối hận, hối hận vì tại sao khi 20 tuổi tôi không biết đến quyển sách này, hối hận vì tại sao khi tôi 20 tuổi tôi lại không rèn cho mình thói quen đọc sách...
Tác giả không cố ép chúng ta đi theo một hướng nào đó được cho là đúng đắn như cách cổ truyền thường làm, mà là mở ra toàn cảnh, và giúp ta thấy trước những điều có thể bắt gặp và để cho chúng ta tự mình chọn hướng tối ưu.
5
901826
2015-12-15 19:53:00
--------------------------
293156
5395
Khi mà bạn choáng váng với những câu hỏi "Tôi là ai?" "Đam mê của tôi là gi?".. Mình đọc cuốn sách này trong lúc mình cần nó nhất, cần lối đi để kéo mình ra khỏi cơn khủng hoảng tuổi 20, khi mình tự ti cảm thấy bản thân không có gì hết, và dằn vặt vì chưa tìm ra đam mê của mình. Meg Jay định hướng giải quyết qua những trường hợp bà đã nghiên cứu và tư vấn, làm rõ điều ta cần làm. Nói cho ta biết cuộc khủng hoảng danh tính tuổi 20 rất nhiều người gặp phải và nên đón nhận như thế nào và đi qua nó bằng cách nào. Đọc sách như mình chính là những nhân vật trong đó và đang được tác giả tư vấn thật sự. Cảm thấy may mắn vì đã đọc.
5
492988
2015-09-08 01:10:15
--------------------------
291234
5395
Giai đoạn từ 19 đến 25 nó quan trọng hơn những gì bạn nghĩ, giống như đi đến một ngã 3, bạn không thể quay đầu lại, bạn phải lựa chọn hoặc bên trái, hoặc bên phải. Bạn thấy ngợp, lưỡng lự, stress, xung quanh toàn màu xám khi phải đưa ra quyết định, đối mặt với thực tại. Những bước đi đầu tiên trên đường đời nếu không vững sẽ khó lòng đưa bạn đến đích mong muốn.
Cuốn sách này với mình giống như một thanh vịn giúp mình thêm vững tâm khi đối mặt với những biến chuyển mới. Cuốn sách gồm 3 phần rõ rệt "Công việc - Tình yêu - Trí não và cơ thể" và mỗi phần là những câu chuyện từ những người bệnh nhân của bác sĩ mà khi đọc mình cảm nhận được đâu đó có cả câu chuyện của mình. Mình tin khi các bạn trẻ đọc những dòng nội dung từ cuốn sách cũng sẽ có cùng suy nghĩ với mình. 
Mặc dù là viết cho tuổi 20, nhưng cá nhân mình nghĩ bạn trẻ từ 17, 18 tuổi cũng nên đọc thử. 
4
508869
2015-09-06 09:13:03
--------------------------
287394
5395
Những năm tuổi 20, những năm tháng đối mặt với sự khủng hoảng, với những quyết định quan trọng có sức ảnh hưởng sâu sắc, có tính định hình đến cả cuộc đời. Rất cần những lời khuyên bổ ích và thực tế, có tính áp dụng cao để đến khi 30, nhìn lại, mình không muốn tự hỏi bản thân "Khi 20, mình đã làm gì, để bây giờ hối tiếc." Quyển sách xoay quanh 3 chủ đề chính: Công việc, Tình Yêu, Trí Não và Cơ Thể giúp bản thân mình biết, mình đang ở đâu và cần làm gì trong hiện tại để không hoang phí và hối hận ở tương lai. 
5
71704
2015-09-02 18:04:34
--------------------------
272766
5395
Qua hơn 300 trang sách, Meg Jay muốn gửi đến chúng ta thông điệp về sự bình thường của khủng hoảng tuổi 20 mà ai cũng từng phải trải qua trong cuộc đời. 

Thông qua việc giúp đỡ các bạn trẻ với những bối cảnh và vấn đề khác nhau trong công viêc, học tập, cuộc sống, tình cảm,.. tác giả âm thầm gửi đến cho riêng người đọc những bài học đa dạng khác nhau.

Tác giả muốn nhấn mạnh thông điệp là sự chần chừ với niềm tin sai lầm là "chúng ta còn trẻ, chúng ta còn thời gian" để sửa sai, để tìm kiếm, để lựa chọn,... vô hinh trung đưa ta lạc lối và hụt hẫng khi chúng ta bước gần đến tuổi 30.

Nếu bạn đang trong tuổi thanh xuân của mình, bạn nên mua nó, đọc nó, để định hình cho tuổi trẻ của mình.
5
482216
2015-08-20 13:28:21
--------------------------
268126
5395
Cuốn sách rất hay và bổ ích đối với một người đang trong tuổi 20 như mình. Xuyên suốt cuốn sách là những lời khuyên, là thông điệp gửi tới những người trẻ. " Hãy bắt đầu cuộc sống của bạn và tự chịu trách nhiệm về nó ngay từ bây giờ". Cuồn sách bao gồm nhiều ví dụ hay, bổ ích về cuộc sống, các trải nghiệm từ thành công cho tới thất bại của nhiều bạn trẻ trong độ tuổi 20+. " Xin đừng hoang phí tuổi 20" _ thông điệp rất hay và ý nghĩa của tác giả. 
5
141810
2015-08-16 02:49:19
--------------------------
261518
5395
Sách rất hay. Nội dung hữu ích, thực tế cho những bạn trẻ chuẩn bị bước vào một giai đoạn mới và cực kỳ quan trọng của cuộc đời mình. Sách được chia thành từng phần, nhấn mạnh vào những vấn đề mà các bạn trẻ thường hay gặp phải ở độ tuổi này và có những định hướng khá thú vị. Bìa sách đơn giản, giấy không quá mỏng nhưng không được mịn lắm. Nhưng so với nội dung thì hoàn toàn có thể chấp nhận được. Chỉ tiếc là tôi không được đọc cuốn sách này trước khi tôi 20.
4
395298
2015-08-11 10:17:53
--------------------------
259427
5395
Tôi sắp 20 và tôi đọc cuốn sách này . Cuốn sách là rất nhiều những kinh nghiệm,bài học quý báu cho tuổi 20 sắp tới của tôi và cả sau này nữa.Như tác giả có nói :Tuổi 20 là lúc một lần nữa ta được tái sinh trong cuộc đời này,đó là lúc ta có thể học được nhiều điều,là những năm tháng sẽ định hình cuộc đời ta sau này.Tôi tự nhủ: Mình phải tận dụng triệt để  những năm tháng tuổi 20 đẹp đẽ nhất của cuộc đời để tạo nên tương lai của mình sau này .
5
409358
2015-08-09 14:17:12
--------------------------
245235
5395
Một cuốn sách rất đáng đọc cho các bạn ở độ tuổi 20. Sách đưa ra lời khuyên rất chân thực, bổ ích từ tác giả (một người đã nghiên cứu, lắng nghe và làm việc với lứa tuổi 20 trong suốt sự nghiệp của bà). Sách cũng chứa rất nhiều các ví dụ bổ ích từ trải nghiệm của rất nhiều bạn ở lứa tuổi 20 chỉ ra cả những sai lầm, lạc lối và cả thành công của họ. Cuốn sách cung cấp rất nhiều thông tin đã được nghiên cứu và kiểm chứng để tác giả đi tới kết luận rằng lứa tuổi 20 nên sông hiến và sống hết mình vào sự nghiệp và các mối quan hệ nghiêm túc. Tuy nhiên, sách được dịch khá lủng củng. 
4
167542
2015-07-28 20:16:39
--------------------------
240098
5395
Cuốn sách này có nội dung phù hợp cho những người trong những năm hai mươi tuổi của mình.
Nó giúp có cái nhìn thật thực tế để sẵn sàng mọi thứ nhất là tâm lý để không bị bỡ ngỡ về việc mọi thứ không như mình nghĩ, và từ đó vạch ra con đường riêng, đúng đắn cho mình.
Tác giả không cố ép chúng ta đi theo một hướng nào đó được cho là đúng đắn như cách cổ truyền thường làm, mà là mở ra toàn cảnh, và giúp ta thấy trước những điều có thể bắt gặp và để cho chúng ta tự mình chọn hướng tối ưu.
Đây là sách hay nên đọc.
5
441359
2015-07-24 12:00:37
--------------------------
222009
5395
Đối với một người đang ở tuổi 20, đọc cuốn sách như có thể thấy bản thân mình ở đâu đó, cũng với những thắc mắc, băn khoăn, trăn trở không của riêng ai đó. Và rồi những con người trong sách đã giải quyết những rắc rối, những điều khó nghĩ ấy như thế nào? Kết quả ra sao? Cuốn sách, bằng việc giải thích và lập luận theo cái nhìn của tâm lý học, đồng thời kể chuyện về những người ở tuổi 20 khác như dẫn chứng sinh động, đã khiến mình nhận ra nhiều thứ và nên thay đổi thế nào. Đây là cuốn sách rất đáng đọc.
5
157478
2015-07-04 20:16:00
--------------------------
210139
5395
Nội dung sách đề cập khá rộng các chủ đề, sau khi đọc xong nếu bạn vận dụng được nội dung trong sách vạch ra thì cuộc sống của bạn sẽ thay đổi theo hướng tích cực. Tuy nhiên có chút ý kiến cá nhân là tuổi 20 thì có lẽ các bạn sinh viên trước và đang tuổi 20 là thành phần đọc giả chính nên nếu sách hướng trực diện vào đối tượng này thì có lẽ sẽ làm hấp dẫn các bạn sinh viên hơn, từ đó giúp cho các bạn sinh viên có suy nghĩ về lối sống của mình sao cho phù hợp, bước đệm để dẫn tới thành công sau này. Mình đánh giá đây là quyển sách khá hay. Các bạn nên thử đặc biệt là các bạn sinh viên
4
347802
2015-06-18 23:24:12
--------------------------
183273
5395
Sau khi đọc xong sách, tôi thường huyên thuyên với em để kể về những thú vị trong đó. Với cuốn sách này, tôi có một quyết định khác, tôi muốn mình tổ chức lại - giống như một bài nói chuyện trước đông người - để nói với em. Vì em đang ở những năm đầu của tuổi 20. Tôi muốn, em sử dụng tuổi 20 của em một cách tuyệt vời. 

Tôi được biết tác giả Meg Jay khoảng tháng 6 năm ngoái. Sau 9 tháng tôi được biết rõ hơn về điều tác giả quan tâm, đó chính là chúng tôi, những người trong độ tuổi 20. 

Tôi cám ơn tác giả đã nghiên cứu về độ tuổi của tôi, của em, của chúng tôi. 
Cám ơn Alphabooks đã chọn cuốn sách này để xuất bản ở Việt Nam.
5
32038
2015-04-15 20:39:47
--------------------------
181930
5395
Theo tôi, cuốn sách phù hợp nhất cho các bạn trẻ chuẩn bị ra trường. Cuốn sách sẽ hành trang quý báu giúp các em định hướng cuộc sống của mình, vượt qua những bỡ ngỡ, hoang mang, mất phương hướng trước khi bị đẩy vào cuộc đời. Nếu tôi đọc cuốn sách này sớm hơn, có lẽ tôi đã có những quyết định khác trong công việc và tình cảm. Dù sao thì vẫn chưa quá muộn cho bản thân và những người em trong gia đình.  Cùng với 7 thói quen của các bạn trẻ thành công, là những cuốn sách " cực kỳ cần thiết"cho các bạn trẻ.
5
65046
2015-04-12 20:05:43
--------------------------
171406
5395
Cuốn sách như một cuốn cẩm nang không chỉ dành riêng cho những bạn mới bước sang tuổi 20, mà còn dành cho những người đã đi qua tuổi 20 nhưng vẫn không nguôi cảm giác chênh vênh giữa thói đời.
Không phải một cuốn sách chỉ toàn những giáo điều khô khan mang nặng lí thuyết, "Tuổi 20 - Những Năm Tháng Quyết Định Cuộc Đời Bạn" đem tới cho độc giả những câu chuyện cùng những kinh nghiệm quý báu về cuộc sống, về những nhận thức bạn cần phải tìm ra cho chính mình trong những năm tháng ngã rẽ của cuộc đời. Đọc cuốn sách, ta nhận ra rằng, tuổi trẻ chợt đến mà cũng chợt đi vội vàng, nếu không kịp nắm bắt những điều quý báu mà tuổi trẻ đem lại, ta sẽ chỉ nhìn thấy trong tương lai mình những mảng màu xám xịt.
5
48662
2015-03-21 21:16:16
--------------------------
168639
5395
tuổi trả là những khát khao, tuổi trẻ là những mê say ấm ôm bao tháng năm yêu tuyệt vời. Dù chỉ mới 19 tuổi non, có thể là sự bắt đầu nhẹ nhàng của tuổi trẻ đầy sức sống,chưa 20 nhưng thật sự thấy cuộc sống không chỉ là một màu hồng, nhiều mới mẻ, hứng thú cũng nhiều cạm bẫy, biết làm sao để bước đi tới đích cuối cùng mà vẫn còn chút hời hợt của tuổi trẻ, một chút nhộn nhịp, thật sự thanh xuân như một cơn mưa rào, dù bị cảm nhưng vẫn muốn đẫm mình trong cơn mưa ấy lần nữa.....theo mình nghĩ cuốn sách này thật sự bổ ích cho chúng ta nó dạy chúng ta cách bước đi, dạy cách thành công và dạy cả cách giữ thanh xuân dù qua đi nhưng vẫn hiện diện.. thật sự cảm ơn tác giả
5
263975
2015-03-16 23:11:10
--------------------------
160436
5395
Tôi một chàng sinh viên năm 2,chuẩn bị bước qua cái tuổi 20 đầy mới mẻ,bên cạnh những thuận lợi và những thách thức đang chào đón chúng ta, thử nghĩ xem nếu mà chúng ta sẵn sàng đem cai tâm của minh đi giúp đỡ người khác,tại sao không chứ,hay đã tham gia vào những cuộc chơi vô cùng phung phí thời gian và tiền bạc,những đồng tiền có thể chúng ta chưa tự tay làm ra hay do tự mình làm,cũng không nên làm như thế...đấy cũng chỉ là nhưng vấn đề mà mình sẽ đọc đến,một quyển sách để mình vào tuổi 20.
4
385020
2015-02-24 12:30:38
--------------------------
158994
5395
Cuốn sách rất phù hợp cho những bạn trẻ đang băn khoăn, trăn trở hay lạc lõng giữa tuổi trẻ của mình.Về quyển sách này, với riêng mình thì nó là một phần để bạn nhìn lại,Cuốn sách không phải là lí thuyết khô khan, nó kèm những bài học ẩn trong những câu chuyện của những bạn trẻ đã từng trải qua cái thời kì "khủng hoảng"như một lời tâm sự sẻ chia thân tình và nội dung cực kỳ phù hợp với tất cả bạn trẻ nào cùng lứa tuổi 20 như mình Đây là quyển sách  nên đọc một lần để chiêm nghiệm lại tuổi trẻ và những quyết định của bản thân.
5
326298
2015-02-14 14:49:31
--------------------------
155894
5395
Về quyển sách này, với riêng mình thì nó là một phần để bạn nhìn lại,
Liệu con đường bạn đang đi có đúng đắn hay không, và quan trọng hơn hết, nó dẫn bạn đến đâu.
Bản than mình cũng từng như Helen, thích bùng nổ và không mục đích, như thể sau mỗi thất bại bạn sẽ lại đỗ lỗi 100 trách nhiệm lên hoàn cảnh. trong khi chính bản than bạn mới là yếu tố có thể xoay chuyễn cở nào cũng được. 
Thế nên, đối với những người trẻ, cần thiết phải mang câu hỏi “tôi là ai?” cho đến khi bạn tìm được chính mình thì thôi, và càng biết sớm đi tìm hiểu thì càng tốt. cuốn sách này sẽ cho bạn biết nên đi hướng nào, còn nếu bạn đang sai đường lạc lối, nó sẽ là tấm gương cho bạn nhìn lại, rồi đi tiếp, nên dù có quá 20 tuổi bạn vẫn còn kịp để xem và truyền lại cho con cháu : ))
5
42071
2015-02-02 15:00:58
--------------------------
139998
5395
Cuốn sách rất phù hợp cho những bạn trẻ đang băn khoăn, trăn trở hay lạc lõng giữa tuổi trẻ của mình. Tuổi trẻ chúng mình ai cũng có những ước mơ cao đẹp, muốn bay nhảy, khám phá. Mỗi người chúng ta đều có những hướng đi khác nhau cho những năm tháng tràn đầy sức sống này. Tuy nhiên, không phải ai cũng đi đúng hướng, có người trải qua rồi thì cảm thấy đầy vốn sống, tương lai hứa hẹn, nhưng cũng không ít người cảm thấy hối tiếc, ước gì mình được sống lại và làm lại những năm tháng quan trọng ấy...
Cuốn sách không phải là lí thuyết khô khan, nó kèm những bài học ẩn trong những câu chuyện của những bạn trẻ đã từng trải qua cái thời kì "khủng hoảng" này, và với những bài học ấy, ta thấy được tương lai hoàn toàn khác của họ, một tương lai đầy hứa hẹn vì họ đã dám rẽ hướng và dấn thân...
5
66202
2014-12-09 20:18:04
--------------------------
138900
5395
Mình chưa bao giờ thích đọc những quyển sách dạy làm người hoặc như thể loại "hạt giống tâm hồn" như thế này. Tình cờ được một người bạn thân mua tặng quyển này trên Tiki. Đây là lần đầu tiên mình đọc và rất bất ngờ với nội dung của quyển sách này. Không hề khô khan, dịch rất mượt mà, đọc dễ chịu vô cùng, như một lời tâm sự sẻ chia thân tình và nội dung cực kỳ phù hợp với tất cả bạn trẻ nào cùng lứa tuổi 20 như mình. Đây là quyển sách mà mình nghĩ bất kỳ ai dù đã trải qua cả tuổi 20 cũng nên đọc một lần để chiêm nghiệm lại tuổi trẻ và những quyết định của bản thân.
5
104497
2014-12-05 11:33:18
--------------------------
