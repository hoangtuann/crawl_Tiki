219572
8122
Đây là cuốn sách tập hợp những truyện cổ tích hay và kinh điển mà trẻ em trên thế giới đều được ba mẹ đọc trước khi đi ngủ.
Truyện có khổ nhỏ nhắn, không tranh ảnh minh họa hay hình màu, chỉ có chữ và chữ, vì thế, cuốn sách này phù hợp với trẻ trên hai tuổi, có ý thức khi ngồi nghe ba mẹ đọc truyện hơn là những đứa bé sơ sinh thích mò mẫm và xé giấy.
Tôi vẫn cất giữ cuốn sách này chờ em bé lớn hơn để đọc cho bé nghe, nhưng không phải vì thế là chưa từng đọc, tôi đọc cho chính tôi trước khi đi ngủ. Vì vậy, với tôi, đây không chỉ là cuốn sách của con trẻ mà còn là của cả người lớn
4
75153
2015-07-01 17:23:10
--------------------------
