472087
4213
Phải nói thật khi chọn mua quyển sách này tớ không nghĩ rằng nó lại nói về chuyện tình cảm hôn nhân giữa "đàn ông" và "phụ nữ". bởi vì tên bìa sách "nói luôn cho nó vuông" như là một cuốn sách hài vậy. Vì thế mà khi cầm cuốn sách lên đọc tớ đã khá bất ngờ. Còn về bìa sách tớ không hài lòng lắm vì có vẻ sách đã cũ bìa sách ngả vàng. Cuốn sách này phù hợp với những bạn gái muốn hiểu về đàn ông cũng như đàn ông thấu hiểu phụ nữ cũng như chính mình. Và mình khuyên những bạn còn quá trẻ không nên mua quyển này vì nó khá là "hại não" vì sách toàn nói về tình yêu và hôn nhân thôi.
4
112968
2016-07-09 09:45:42
--------------------------
395695
4213
Tôi mua sách nói luôn cho nó vuông vì qua lời giới thiệu của bạn bè và những nhận xét của bạn đọc khen ngợi. Và cũng là vì sự tò mò của bản thân. Tiêu đề sách gợi cho tôi cảm giác gì đó thật hài hước.  Đúng vậy với lối kể chuyện hài hước mà dí dỏm của tác mà không kém phần sâu sắc để lại trong lòng người đọc một cảm giác khó có thể nào quên được.  Mỗi câu chuyện hài hước đi cùng với đó là một bài học.  Cuốn sách cực kì thu hút là tôi k thể rời
5
1131536
2016-03-12 12:00:47
--------------------------
378989
4213
Cuốn sách này là cuốn cùng tác giả với cuốn Cư sử như đàn bà, Suy nghĩ như đàn ông nó khá tuyệt vời làm tôi cảm thấy đôi khi ai cũng có sự cuốn hút riêng nhưng tác là là nam nên đôi khi đồng cảm suy nghĩ của đàn ông hơn là phụ nữ. Rất mong tiki lại nhập sách này để bán! Mình nghĩ mọi người nên đọc nó vì nó rất hay Rất xừng đáng cho 5 sao Bìa sách đẹp nội dung tốt đúng là bìa cuốn sách có phần chưa đẹp  Mong sớm được mua thêm cuốn sách này để tặng người thân
5
454434
2016-02-08 21:01:54
--------------------------
279932
4213
Ban đầu mình mua sách là vì thấy lời quảng cáo rất hay, với lại tiêu đề quyển sách đầu tiên tuy mình chưa đọc nhưng thấy rất ấn tượng: Cư xử như đàn bà, suy nghĩ như đàn ông rồi cuốn sau là Nói luôn cho nó vuông nên mình bị thu hút ngay.
Quyển sách bìa đẹp, bắt mắt nhưng nội dung khá chán , mình chỉ đọc được mấy trang đầu là dẹp luôn. Tuy cố gắng đọc đến lần 2, lần 3 rồi cũng vậy, chỉ đọc dc mấy trang.. Thực sự không thu hút như lời quảng cáo
2
714142
2015-08-27 11:38:24
--------------------------
232311
4213
Ban đầu mình đặt mua cuốn sách này vì tiêu đề "Nói luôn cho nó vuông", vừa tò mò về nội dung, vừa buồn cười không biết đằng sau tiêu đề ấy là những gì.
Mình bắt đầu đọc ngay khi đơn sách đến, và thực sự là mình khá ấn tượng với nội dung này. Steve Harvey, với giọng văn hài hước nhưng vẫn không kém phần hiểu biết, như mở ra một cuộc trò chuyện cởi mở, vui vẻ về một điều : đàn ông thật sự nghĩ những gì?
Nối tiếp thành công của Cư xử như đàn bà, suy nghĩ như đàn ông; mình nghĩ đây là một cuốn sách rất đáng tiền, phù hợp cho những người trẻ.
Đáng đọc. 4,5/5 sao vì mình không thích cái bìa cho lắm.

5
459083
2015-07-18 18:36:36
--------------------------
