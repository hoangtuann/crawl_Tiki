576105
10872
về nội dung cuốn sách thì khỏi bàn cãi, thích hợp với những người có ý tưởng tiến tới chứ k phải cứ trì trệ vĩnh viễn tư duy cũ, cho 3 sao bởi thất vọng về tiki quá, cuốn sách có bên ngoài bị nếp ở góc sách, bọc nilong mỏng của cuốn sách cũng bị tróc ra, hàng giảm giá có khác, chắc sách cũ để kệ m.n cùng xem nên nó tan nát như vậy. Hơi kỹ tính vì mình quan trọng cả bề ngoài và bên trong, nguyên đơn hàng 714000d mà sách nào cũng kiểu như vậy hết, thất vọng cho tiki quá, ngay cả những cuốn đã bọc nilong kiểu như hàng mới nhưng khui ra cũng kiểu vậy, thôi mua gì ghé nhà sách nếu bạn muốn một cuốn sách hoàn mĩ k tì vết, còn nếu thích hàng rẻ thì tiki vậy
3
489579
2017-04-18 00:28:47
--------------------------
558249
10872
mình đã phải rất phân vân xem có nên mua sản phẩm này hay không, nhưng rồi suy nghĩ cuối cùng mình cũng đã thêm nó vào giỏ hàng. khi mới nhận được sách nhìn chung thì bìa sách được bọc thêm 1 lớp nilon nên khá sạch sẽ không sợ bị dây bẩn ra vẫn có thể lau đi được. phần thứ 2 là mình muốn khám phá phương pháp mà tác giả muốn đề cập ở đây là gì vì mình cảm thấy rất bị thuyết phục với các phương pháp như khổ luyện 10000 giờ, hay như làm việc 20h. và mình khi vọng khi mình khám phá nội dung của nó mình sẽ nhận được nhiều điều bổ ích từ tác giả. hihi
\
4
1496253
2017-03-29 20:09:56
--------------------------
451521
10872
Đọc xong cuốn sách, cảm thấy Tim là 1 con người độc đáo. Anh đưa ra nhiều nội dung thú vị: ứng dụng quy luật Parkinson và nguyên lý 80/20, cách tổ chức chuyến du lịch vòng quanh thế giới với chi phí thấp nhất, cách thuê ngoài-sử dụng trợ lý ảo để giải phóng bản thân thoát khỏi cuộc sống 8-5 v.v. Con số 4 giờ/tuần là ẩn dụ của quy tắc ''Làm ít được nhiều'' 
Tuy nhiên, những nội dung này chỉ ứng dụng trong lĩnh vực kinh doanh và ở nước ngoài nhiều hơn môi trường làm việc nhà nước.
5
314171
2016-06-19 21:01:06
--------------------------
435682
10872
Tuần Làm Việc 4 giờ” sẽ chỉ cho bạn thấy những suy nghĩ và mục tiêu sống của bạn đang có vấn đề. Bạn cần thay đổi để vừa có thể tận hưởng được cuộc sống vừa hoàn thành tốt công việc của mình.
Trong cuốn sách này, Timothy Ferress sẽ cho chúng ta thấy: Làm thế nào để có được lối sống hoàn toàn tự do của một triệu phú mà không cần phải có 1 triệu đô la?

Chỉ cần một câu trả lời đơn giản, bạn nên biết tách biệt thu nhập với thời gian và tạo ra lối sống lý tưởng cho mình, đi du lịch vòng quanh thế giới và tận hưởng những điều thú vị nhất trên hành tinh này.
4
506806
2016-05-25 15:39:29
--------------------------
432604
10872
Tôi rất thích cuốn sách này, từ hình thức tới nội dung. Cuốn sách có một phong cách viết thú vị, tựa như một câu chuyện hay ho khiến người ta muốn khám phá.
Cuốn sách có lối viết kiểu tự sự dễ đọc, bố cục mạch lạc dễ hiểu và đã cung cấp rất nhiều thủ thuật hay giúp những người làm công việc công sở tiết kiệm thời gian và giảm stress. Hơn thế nữa cuốn sách giúp chúng ta thiết lập một cuộc sống “ngăn nắp hơn” và vui vẻ hơn nhiều.
Cuốn sách này không giúp chúng ta hướng đến sự tự do, thịnh vượng và hạnh phúc, chính bạn sẽ làm điều đó, qua những gợi ý và những bài tập nhỏ kèm theo từng chương của sách.

5
641971
2016-05-19 13:44:52
--------------------------
429102
10872
Đây có lẽ một trong số những cuốn sách bán chạy nhất mà tôi từng mua. Để mua nó ở tiki tôi đã canh chừng, chờ chực các thể loại....nói chung là rất lâu mới mua được ẻm về. Nội dung cuốn sách thì như tôi đã nói ở tiêu đề, bác Tim đã dạy cho chũng ta khá nhiều cách hữu hiệu để quản lí thời gian và một số mẹo nhỏ khá hay ho. Và cũng có một số triết lí sống được viết trong cuốn sách này mà theo tôi thấy là khá hay, nó đã đánh thức tôi khỏi giấc mơ của những người tầm thường và trở thành một người có ước mơ lớn. Khuyết điểm của cuốn sách này có lẽ là ở phần dịch, người dịch vì để không giết hết bản gốc mà đã dịch khá cứng gây mất đi sự thú vị, đôi lúc khó hiểu, đây chỉ là nhận s\xét của riêng tôi. Khuyên các bạn muốn học ngành kinh doanh hoặc đang là doanh nhân nên tìm mua cuốn sách này nhé
4
299058
2016-05-12 14:49:35
--------------------------
427623
10872
Thật tốt khi bạn biết được thời gian mà bạn sẽ bỏ ra sắp đến đây sẽ giúp bạn thành công như thế nào.Nếu một tuần bạn bỏ ra cả 48 tiếng đồng hồ chăm chỉ làm việc học tập mà vẫn chưa hoàn thành theo ý mình thì bạn nên đọc cuốn sách này.Ngay khi nhận sách mình đã rất ưng ý vì bao bì đẹp,chất giấy tốt.cuốn sách sẽ đưa ra giải pháp trong cuộc sống cho bạn,cho bạn những mục đích và công việc cụ thể,để 4h thật sự có ý nghĩa với bạn.Cảm ơn tiki giao hàng rất kĩ càng.
5
761874
2016-05-09 19:01:56
--------------------------
412198
10872
Những lời sau đây của tôi chia sẻ về cuốn sách này thực sự rất là hiệu quả, lúc trước tôi hay kiểm tra mail thường xuyên, lướt fb, v,v,.... mà chả biết đích đến là gì, từ khi đọc cuốn sách này tôi thay đổi hẳn, thời gian tôi có được dư ra rất nhiều, tôi tập trung vào những công việc khác như đi giao lưu tìm kiếm khách hàng thay vi cứ ngồi riết trước màn hình máy vi tính. Thu nhập của tôi cũng dần chuyển sang tự động - thụ động thay vì trước kia tôi phải đi tìm khách hàng. giờ đây tôi đã có cách tăng thêm doanh số mà không cần phải bận tậm nhức đâu hơn trước mà hiệu quả lại bằng hoặc cao hơn. Tôi mới chỉ đọc được qua 1 lần và đang hứng thú nên vào đây chia sẻ cùng mọi người. Bộ book love tặng kèm cũng có ý nghĩa hay đại khái có câu người biết chữ mà ko đọc sách thì cũng như người không biết chữ mà thôi. hehee
5
1232356
2016-04-07 11:28:48
--------------------------
397870
10872
Mình mua cuốn sách này tặng cho anh chồng mình, khi chỉ đọc vài dòng thôi anh í đã khen quá chừng. Sau khi đọc mỗi chương, chồng mình lại tóm tắt cho mình nghe. Mỗi phút giây đọc sách sẽ giúp chúng ta nghiệm ra nhiều điều, học hỏi được nhiều thứ để áp dụng vào cuộc sống của mình. Cuốn sách sẽ rất phù hợp cho những ai mong muốn tiết kiệm thật nhiều thời gian để đi du lịch đây đó. Cuốn sách không chỉ bạn phải làm gì, nhưng nó lại giúp bản thân bạn nhận ra tại sao mình cứ bị quẩn quanh bởi vòng xoáy thời gian. 
4
305113
2016-03-15 15:46:59
--------------------------
373794
10872
Tôi là người muốn khởi nghiệp và mong tìm được nguồn kiến thức bổ ich và tiến bộ về cách thức làm việc thật sự thông minh , kinh doanh tự vận hành chứ không phải gò mình vào trong 1 lịch trình kín mít và không khác gì ngồi làm văn phòng. Cuốn sách này đã giúp tôi điều đó.
Không quan trọng bạn sinh ra như thế nào, cái quan trọng là tư tưởng bạn ra sao, đặc biệt khi muốn thành công!. Cuốn sách này có thể gây mâu thuẫn, thậm chí là ảo tưởng đối với những ai đã in sâu vào tâm trí cách làm việc theo hệ thống, chu trình. Đối với tôi, quyển sách này hoàn toàn xứng để xem xét và thử nghiệm.
5
153783
2016-01-24 18:02:37
--------------------------
369578
10872
Cuốn sách hấp dẫn từ tiêu đề sách và cũng gieo vào lòng bạn đọc không ít những hoài nghi: có thể nào chất lượng công việc được đảm bảo tỏng thời lượng làm việc ngắn như vậy? Vâng, không những chất lượng công việc đảm bảo mà thu nhập còn vượt trội so với việc làm công ăn lương, ngày làm 8 tiếng, sáng cắp ô đi, tối cắp ô về. Điều đặc biệt là khi thay đổi tư duy của bạn về việc nhất nhất lập trình cuộc đời và sự nghiệp của bạn theo 1 lối mòn và quy luật chung bạn sẽ làm sống dậy và theo đuổi những đam mê một thời bị bỏ quên của mình và còn khám phá ra những sở trường, năng lực tiềm ẩn trong con người bạn 1 cách rất kì diệu. Bằng chứng là có những người không chỉ là nhà khoa học thành công mà còn gặt hái những thành tựu về nghệ thuật, thể thao. Những lĩnh vực tưởng chừng như không hề có mối liên hệ gì với nhau hay để theo đuổi 1 lĩnh vực người ta phải từ bỏ những niềm đam mê khác
5
905408
2016-01-15 23:32:38
--------------------------
361489
10872
Giả cả thì quá rẻ, không còn gì phải bàn , sách hay nên gần như ngày nào cũng đọc thì may ra mới chịu được , tác dụng thì mình nghĩ rõ hơi khó để cảm thụ , nhưng ngay từ đầu sản phẩm đã thấy bì của sản phẩm hơi chói nẽn có khó chiụ 1 chút gía của sách rẻ hơn so với nhiều nơi . Sau khi đọc và thực hành những kiến thức từ quyển sách tôi đã xây dựng được một hệ thống kinh doanh tự động hóa hiệu quả. Tôi rất vui được chia sẻ và kết nối với những người bạn cùng yêu thích quyển sách này.
4
844635
2015-12-31 06:49:28
--------------------------
354987
10872
Mình nghe audio book được 4 chương và quyết định mua cuốn sách này để dành cả đời luôn luôn lúc nào cũng có nó, vì các kiến thức quá chân thật và bổ ích.
Mình đang làm việc hơn 10 tiếng mỗi ngày với áp lực đè nặng và ko thể tập trung ở công sở mà phải mang những việc quan trọng về nhà làm, ở công sở chỉ toàn những việc gấp cần giải quyết. Mình cảm thấy cuộc sống quay vòng ko ý nghĩa mặc dù mình yêu công việc nhưng thời gian đâu để đi chơi, giao lưu, du lịch và làm nhiều sở thích khác mà gọi là tận hưởng cuộc sống? Chưa kể ôm đồm làm hết mọi thứ mà hiệu quả lại ko tốt hết mức thì làm sao phát triển khả năng lãnh đạo và có được tự do tài chính để tận hưởng cuộc sống.
Mình sẽ giới thiệu cuốn sách này đến mọi người mà mình biết, và dù họ thích nó hay không, mình vẫn chân thành muốn tất cả mọi người thay đổi lối suy nghĩ mặc định đi làm từ 8h-5h mỗi ngày thì mới đạt đủ hiệu quả.
5
358576
2015-12-18 23:32:14
--------------------------
334494
10872
Đây là cuốn sách dành cho những ai muốn cuộc sống của mình thật "hoàn hảo" theo đúng nghĩa "cuộc sống", khi bạn muốn thoát khỏi cái vòng luẩn quẩn của công việc truyền thống. Tôi đã mua cuốn này kèm theo bộ sách Nguyên lý 80/20 từ 1 năm trước, đến giờ tôi vẫn đang nghiền ngẫm để hiện thực hóa những điều trong đó. Hy vọng tôi có thể hiện thực nó một cách sớm nhất!
Một điểm cộng nữa cho Tiki về cách đóng gói sách! Tôi rất hài lòng khi cầm trên tay hộp sách của Tiki!
5
467203
2015-11-09 17:42:46
--------------------------
298333
10872
Là một cuốn sách khó đọc, khó hiểu, khó thực hiện. Nhưng vì KHÓ nên nó mới mang lại một thành quả bất ngờ. Nếu mọi người ai cũng thực hiện được như cuốn sách thì sẽ không thể có nhóm NR được.
Riêng D,E,L thì mọi thứ đều hay và thực tế. Phần A thì lại quá trừu tượng và không gần gũi với người Việt.
Tóm lại là sách hay, ai muốn sống "chậm" đừng tìm đến cuốn sách này.
Ngoài ra giá trị lớn nhất của cuốn sách là những người đọc làm theo sẽ tiết kiệm được rất nhiều thời gian. Nên đọc sau cuốn "Thành công không chớp nhoáng" của Rory Vaden.
4
590813
2015-09-12 17:04:48
--------------------------
294307
10872
Bạn có muốn làm việc 4 giờ một tuần thay vì 50 giờ một tuần? “Tuần làm việc 4 giờ” của Timothy Ferriss sẽ giúp bạn thực hiện được điều đó bằng việc quản lí thời gian hợp lý hơn , sắp xếp công việc theo thứ tự ưu tiên mà vẫn đảm bảo được thu nhập và có thời gian nhiều hơn cho những thứ mình thích . 
Quyển sách được chia thành 4 phần: Definition - Xác định, Elimination - Loại bỏ, Automation - Tự động hóa, Liberation - Tự do. Qua đó bạn sẽ tự mình áp dụng những lý thuyết đó vào cuộc sống và công việc của mình sao cho hợp lý nhất . Lời khuyên của mình là các bạn nên thử nghiệm một tuần sau đó nên điều tiết mọi hoạt động lại sao cho hợp lý nhất .
Một quyển sách hay và bổ ích . 

5
554150
2015-09-09 10:18:55
--------------------------
290046
10872
Nhìn thấy tiêu đề cuốn sách " Tuần làm việc 4 giờ" làm tôi vô cùng tò mò để biết nội dung của Timothy Ferriss và muốn xem có kẻ nào điên rồ không. Nhưng ôi không!!! Quả là một thiên tài, anh đã chia sẻ cho tôi rất nhiều kiến thức quí giá về thuê ngoài và tư duy rất chính xác để có thể sống tốt và hạnh phúc hơn trong cuộc sống cũng như công việc.
Khi đọc cuốn sách này thì tôi có cảm giác như anh đang đứng trước mặt mình để trò chuyện tâm sự chia sẻ những kinh nghiệm của mình. Từ đó tôi rất khâm phục khả năng tiết kiệm thời gian của anh.
5
501854
2015-09-05 06:50:05
--------------------------
287007
10872
Đang mệt mỏi vì công việc triền miên thì thấy cuốn sách này. Không tin lắm vào tựa sách nhưng cũng muốn biết nó như thế nào, biết đâu tìm được ý gì đấy hay ho và giải phóng được mình khỏi đống công việc hiện tại, nên quyết định đặt mua. Um... đọc hết chương một mà vẫn không xi nhê gì. Tác giả nói lòng vòng lòng vòng đọc nhiều khi phát chán và muốn bỏ dỡ nữa chừng. Quay lại đống công việc bộn bề, lại quay lại đọc tiếp. Kiên nhẫn vài lần thì dần dần nắm bắt được một số ý hay hay.... mặc dù làm việc 4h 1 tuần vẫn chỉ là giấc mơ cho một người làm công ăn lương như mình, nhưng nó cũng giúp cho mình thảnh thơi hơn một chút, có thời gian cho mình hơn một chút. Một số ý mình thích nhất là: ăn kiêng thông tin, nghỉ hưu ngắn hạn và tự động hoá. Cũng gọi là thoã mãn và đạt được yêu cầu trước khi mua sách :) Cảm ơn tác giả. 
3
688644
2015-09-02 10:12:45
--------------------------
286535
10872
Một ngày làm việc 8 tiếng từ tám giờ sáng đến năm giờ chiều, một tuần làm việc 48 tiếng, một tháng làm khoảng 208 tiếng, một năm làm việc khoảng 2.496 tiếng. Tuổi thọ trung bình của người Việt Nam  là 73 tuổi, tuy nhiên làm việc tính đến 65 tuổi thôi. Vậy tổng số giờ làm việc trong cuộc đời một con người là khoảng 162.240 tiếng. Thay vì làm việc như vậy ta chỉ cần bỏ ra 81.120 tiếng cho thời gian làm việc (hiệu quả vẫn như cũ) thời gian còn lại để dùng quan tâm đến bản thân, đến những người xung quanh. Để làm được điều đó bạn chỉ có cách tăng năng suất lao động gấp đôi (làm việc hiệu quả hơn...) Câu trả lời bạn có thể tìm thấy trong sách này!!! Tuy nhiên đó là cách hiệu quả áp dụng với người nước ngoài. Còn với người Việt Nam có hơi khó áp dụng xíu nhưng như sách nói "Trau dồi khả năng bỏ qua có chọn lọc" - Thay đổi bản thân không quá khó khăn và đau đớn như bạn nghĩ đâu 5tinh :)
4
636088
2015-09-01 20:42:56
--------------------------
273665
10872
Tựa đề của cuốn sách là điều mà tôi đang tìm kiếm, nên tôi đặt mua ngay cuốn sách, do có nhiều từ ngữ chuyên môn nên hơi khó hiểu, bạn phải đọc đi đọc lại rồi ngẫm nghĩ nhiều mới có thể hiểu được những điều tác giả muốn truyền tải đến chúng ta, tuy nhiên đây quả thật là 1 con đường tuyệt vời cho những ai đang tìm kiếm sự tự do, quyền nắm bắt và điều khiển cuộc sống theo ý muốn của bạn, sống 1 cuộc sống mà bản thân bạn mơ ước, 1 cuốn sách rất đáng để đọc.
4
736524
2015-08-21 10:57:09
--------------------------
264780
10872
Tò mò với tựa đề sách chính là lý do khiến tôi phải đặt mua quyển sách ”tuần làm việc 4 giờ này” này trên Tiki. Và khi nhận được quyển sách này trong tay, tôi thấy quyển sách này bìa sách đẹp, giấy in rõ ràng, nội dung trong quyển sách này đề cập đến một nội dung khá đặc biệt. Đã có rất nhiều sách viết về chủ đề kỹ năng làm việc nhưng chưa c1o quyển sách nào thật sự đặc biệt , và quyển sách này đã giải quyết được vấn đề đó. Nhìn chung, theo tôi đây thật sự là một quyển sách rất đáng để chúng ta mua về đọc và suy nghĩ về nó.
1
42985
2015-08-13 14:10:01
--------------------------
255950
10872
Trước đây mình hay ôm đồm công việc thành ra xoay qua xoay lại hết 1 ngày mà mình chưa thực sự đạt được thứ gì cho riêng mình và gia đình, hầu hết thời gian của mình đều để hết cho công ty. Từ khi đọc quyển sách này mình đã có dư thời gian và bắt đầu thực hiện ước mơ của mình, những việc mình muốn làm mà vẫn đảm bảo được kết quả công việc trong công ty. Thú thực, lúc đầu áp dụng không dễ dàng gì, thậm chí mình còn nghĩ là chỉ có ở nước ngoài mới áp dụng được chứ ở VN thì làm gì được. Nhưng hãy thử, chắc chắn bạn sẽ tìm ra cách để áp dụng nó dù ít dù nhiều.
4
99983
2015-08-06 15:03:31
--------------------------
249892
10872
Đây thật sự là quyển sách hay và đáng đọc. Mình biết đến tác giả khi coi Ted. Nghe anh ấy diễn thuyết mình đã nghĩ "ảo quá" làm gì có chuyện anh ta làm được tất cả những việc ấy và đều thành công khi thời gian ít ỏi. Rồi mình Google và thật bất ngờ với những thành công anh ấy đạt được nên mình đã quyết định mua cuốn sách này. Qua cuốn sách giúp mình quản lý thời gian khoa học hơn, tập trung giải quyết những công việc quan trọng và hiệu quả hơn. Chúc các bạn se tìm được bài học về quản lý thời gian của mình qua cuốn sách này
5
622431
2015-07-31 23:10:40
--------------------------
235986
10872
Tác giả là một chàng trai khá bình thường nhưng thực ra cũng không bình thường lắm, anh ta lanh chanh luôn làm những viêc mà mình thích, không thích thì thôi, vậy nên rất nhiều công cuộc kinh doanh của anh ta trước đó đã đổ bể nhưng không bỏ cuộc(cái này mih cần phải học) cuối cùng a ta cũng thành công với việc kinh doanh một loại nước tăng lực dành cho thể thao, cách a ta nói dễ dàng lắm đọc sách cảm giác khá thoải mái, nhất là vụ giải quyết bế tắc hay làm gì đó mà không tự làm được thì a ta thuê người xuyên quốc gia giải quyết hộ thế là rảnh tuần làm việc 4h là vậy. Nói chung a ta khá khôn đấy. Trong sách nhiều trang địa chỉ lắm vừa đọc vừa check mạng xem được đấy. :D
4
206164
2015-07-21 14:28:16
--------------------------
219490
10872
Trước khi đọc cuốn sách mình nghĩ, giá như một ngày có nhiều hơn 24h thì tốt.
Sau khi đọc thì mình nhận ra rằng dù một ngày có 48h đi chăng nữa thì vẫn có quá nhiều việc mình muốn làm. 
Ta nên tập trung vào những việc cần thiết và quan trọng, không nên suốt ngày lướt nét rồi than thở rằng chúng ta có quá ít thời gian nhưng lại có qua nhiều việc. Vì ai cũng chỉ có 24h thôi nên hãy tập trung vào những việc khiến chúng ta hạnh phúc. Để khi lúc ta đi ngủ không còn gì phải lo lắng băn khoăn.
Mình có quen một em trên mạng đã khuyên mình một câu mình thấy rất ý nghĩa.
Nói ít là tốt, hem cần suy nghĩ băn khoăn.
5
248870
2015-07-01 15:38:01
--------------------------
214167
10872
Một quyển sách đưa ra khái niệm với về làm việc cũng như đặt ra những giới hạn mới của con người về thành công của mình. Tuy tác giả năm nay mới chỉ ngoài 30, nhưng những thành tựu mà anh đạt được thật khó tin. Nhưng điều hay nhất chính là tác giả đã chứng minh rằng mọi thứ mình có thể làm được nếu mình có thể.

Mình rất thích những bài tập tác giả đã từng cho sinh viên của mình làm, ví dụ như liên lạc với tổng thống, hẹn ăn trưa với một tỷ phú...Điều đáng quý nhất ở đây chính là tác giả lại tận tâm truyền lại kinh nghiệm mình đã làm cho nguời đọc.

Khi đọc quyển sách này, cuối mỗi chương sẽ có bài tập, các bạn có thể làm thử nhé.
5
140922
2015-06-24 17:06:09
--------------------------
200481
10872
Cuốn sách này là cuốn sách mà tôi đã tìm kiếm từ rất lâu, may thay tôi tình cờ được một người bạn giới thiệu. Nó trở thành quyển sách quý của cô ấy mỗi khi vấp phải những khó khăn. Đến lươt tôi cũng vậy, cuốn hút ngay từ những dòng đầu tiên, đó cũng chính là những suy nghĩ và kết quả mà tôi muốn đạt được trong khi vẫn có nhiều thời gian để làm những việc khác mà mình yêu thích. Sống cuộc sống không bị bó buộc về thời gian mà kết quả công việc vẫn rất tốt, nói trắng ra thời gian sẽ do mình quản lý chứ không phải cứ một ngày phải làm đủ. 8 tiếng hay hơn thế nữa. Có những luật lệ luôn là luật lệ, nhưng ắt hẳn vẫn sẽ có những cái gọi là ngoại lệ, chí ít ra tôi muốn được như vậy. Bên cạnh đó, cuốn sách còn nêu ra những định nghĩa về sự giàu có mà tôi cảm thấy rất tâm đắc. Mong là cuốn sách sẽ đến tay của nhiều bạn trẻ Việt Nam hơn để giúp cho giới trẻ Việt Nam gcungx như những nhà doanh nghiệp hãy để ý đến hiệu suất công việc hơn là thời gian làm việc. Nhưng nếu các bạn vì đam mê và cống hiến mà muốn làm nhiều hơn thì đó lại là một điều rất đáng mừng!
5
416043
2015-05-25 09:08:06
--------------------------
159781
10872
Quyển sách đưa ra một định nghĩa mới về thành công. Thành công không chỉ được đo bằng tiền bạc. Thành công là khi bạn có thể kiểm soát được thời gian của mình, làm được những việc mình yêu thích và sống dư dả nhờ điều đó. 
Quyển sách cũng hướng dẫn chi tiết về cách làm thế nào để kiểm soát thời gian, để làm việc hiệu quả thay vì chuyên cần và năng suất.
Tuy nhiên, cách trình bày ở một số chương còn rối rắm. Mực in ở những chương đầu không tốt, chữ in ra cả hai mặt giấy, rất khó đọc. Cách đóng gáy sách cũng không ổn nên mới đọc được một thời gian đã long ra. 
Nếu sách tái bản khắc phục được những nhược điểm này thì sẽ thu hút người đọc hơn.
3
217640
2015-02-19 08:37:54
--------------------------
150082
10872
Mình rất thích cuốn sách này, từ hình thức tới nội dung. Cuốn sách có một phong cách viết thú vị, tựa như một câu chuyện hay ho khiến người ta muốn khám phá. Tác giả là một chàng trai trẻ nhưng đã có cách suy nghĩ mới mẻ, thoát khỏi số đông, và khiến cho bất cứ người đọc nào cũng thấy hưng phấn và tràn ngập năng lượng để thử. Lồng ghép trong các câu chuyện là những bài học, những lời khuyên rất thực tế, cụ thể, đồng thời cuối mỗi chương là những bài tập nhỏ để ta có thể củng cố lại những điều được biết. Những bài tập này như một nấc thang nhỏ dẫn dắt tới sự thay đổi lớn. Mặc dù có vài điều không thể làm được do khác biệt quốc gia nhưng nhìn chung đây là một cuốn sách rất đáng đọc, nó tạo cảm hứng để thay đổi những lối nghĩ thường ngày về làm việc và tận hưởng cuộc sống.
5
164812
2015-01-15 17:38:10
--------------------------
141151
10872
Tôi thật sự đánh giá cao quyển sách này!
Tôi đã đọc rất nhiều sách về đào tạo con người, quản lí cuộc sống để tiến đến thành công nhưng hầu hết tất cả những cuốn sách đó điều khuyên bạn đại khái là: phải bớt thời gian nghỉ ngơi, la cà với bạn bè lại mà tập trung hết vào công việc nếu bạn không muốn về già phải lao động vất vả để bù đắp cho thời tuổi trẻ đã lãng phí, nào là làm việc thật tích cực thời trẻ để già mới hưởng thụ.......
Cuốn sách Tuần làm việc 4 giờ này lại khác, nó khuyến khích bạn sống CHẬM lại, tận hưởng cuộc sống đang lặng lẽ trôi, dành nhiều thời gian quan tâm đến bản thân và những người mình yêu thương hơn, làm ít đi nhưng chất lượng không hề giảm mà lại tăng lên, vừa làm vừa hưởng thụ chứ không phải đợi đến già chân chậm mắt mờ, lẩm cẩm, đầy bệnh tật rồi mới nghĩ đến chuyện tận hưởng những đồng tiền mà thời trẻ mình kiếm ra.
Tôi đã đọc đi đọc lại cuốn sách này 4 lần, hiện tại tôi đang cố gắng thực hiện theo chỉ dẫn của tác giả Timothy Ferriss. Bây giờ, cuộc sống của tôi đang được cải thiện đáng kể, tôi không còn tham công tiếc việc như trước nữa nhưng kết quả việc học thì tăng lên. Mỗi tội bạn biết đấy: làm theo thì không hề dễ, bạn phải kiên trì.
5
381380
2014-12-14 08:42:00
--------------------------
136444
10872
Thông qua tìm hiểu trên mạng mình được khuyên nên mua cuốn sách trên và cuốn sách đã không làm mình thất vọng.Có thể nói ngay tiêu đề sách đã cho tôi một cảm giác khác lạ.Cuốn sách giúp tôi thay đổi cách tiếp cận công việc,cho tôi thấy nó là niềm vui,niềm yêu thích chứ không phải bạn bị ép buộc làm.Theo như nội dung được truyền tải thì tôi nghĩ sách nên đổi tiêu đề là " Tuần làm việc 0 giờ" thì có vẻ hợp lý hơn bởi công việc đã là cuộc sống của bạn rồi!Cuối cùng mình hy vọng nó có thể giúp đỡ đươc cho nhưng ai đang cảm thấy bị áp lực bởi chính công việc mình đang làm và thay đổi thay độ theo hướng tích cưc hơn!
5
294890
2014-11-21 11:08:19
--------------------------
132978
10872
Tôi e rằng khi đọc tiêu đề của cuốn sách The Four hour work week của tác giả mọi người lễ lầm tưởng rằng mục tieu của cuốn sách là làm việc cực cực ít thời gian mà lại mang về một đống tiền trong lúc vẫn đang đi chơi .Dù tác giả viết rất hay và có nhiều ý tưởng xuất sắc ,nhưng tôi cho rằng mục tieu của công việc là đóng góp và cống hiến cho xã hội và đem lại nhiều ý tưởng ý nghĩa thay đôi lớn trong cuộc sống đối với những người có Liên quan,nếu bạn làm được điều tôi vừa nêu thì dù có làm 40 tiếng 1 tuần thì bạn vẫn cảm thấy hạnh phúc tràn trề.Nó thực sự đáng đọc cho nhưng ai muôn cống hiến ,vì nó tiết kiệm thời gian để tập trung được nhiều thứ có ích hơn nữa .Thân
5
454005
2014-11-03 10:08:29
--------------------------
92299
10872
"Tuần làm việc bốn giờ" là một quyển sách hay, độc đáo và táo bạo. Bỏ qua những yếu tố khác biệt về văn hóa, tác giả Timothy Ferriss đã truyền đạt những định nghĩa rất mới nhưng rất phù hợp với thời đại ngày nay.

Một số bài học tôi đã được chia sẻ rất hữu ích:

+ Sự hạnh phúc không phải đo lường bằng sự giàu có. Để hạnh phúc, cần đạt được sự cân bằng giữa các yếu tố: thu nhập - thời gian làm việc linh hoạt - sự tự do.

+ Nếu tôi chăm chỉ làm nhân viên làm việc 8h/ngày, sau đó tôi sẽ trở thành ông chủ và làm việc 10h/ngày.

+ Thu nhập tuyệt đối giúp bạn đảm bảo cuộc sống, nhưng thu nhập tương đối mới là thước đo của sự thịnh vượng.

+ Và rất nhiều tư tưởng mới lạ khác nữa ...

Quyển sách không giúp bạn hướng đến sự tự do, thịnh vượng và hạnh phúc, chính bạn sẽ làm điều đó, qua những gợi ý và những bài tập nhỏ kèm theo từng chương của sách.

Đúng như cảnh báo, ĐỪNG ĐỌC SÁCH NÀY NẾU NHƯ BẠN CHƯA SẴN SÀNG TỪ BỎ LỐI SỐNG HIỆN TẠI CỦA MÌNH
4
138711
2013-08-26 14:59:00
--------------------------
67287
10872
Đây là một cuốn sách hay, táo bạo về cuộc sống của những con người làm thế nào giảm số giờ làm việc, tăng năng suất lao động, và tận hưởng cuộc sống (đi du lịch vòng quanh thế giới, làm những việc họ mơ ước)

Mình rất thích cuốn sách này, nhưng với bản dịch tiếng việt mình vẫn chưa ưng ý lắm.
Cuốn sách đề cập đến những vấn đề mới lạ, có khá nhiều định nghĩa vì thế nếu không được giải nghĩa một cách đầy đủ có thể gây khó hiểu cho người đọc.

Mặc dù có những ví dụ trong cuốn sách chỉ phù hợp với điều kiện ở nước ngoài, nhưng bạn vẫn có thể áp dụng một cách
linh hoạt, như trong bài tập mướn trợ lý ảo, bạn có thể thực hành mướn những người làm việc tự do (freelancer) trên những trang web tin cậy.

Để đạt được một cuộc sống như mơ ước, cần sự phấn đấu rất lớn từ bản thân. Đừng bao giờ ngừng cố gắng.

ps: chất lượng đóng sách không tốt lắm, mình mới đọc chưa được một tuần, một số tờ giấy đã bị long ra.
4
35152
2013-04-06 09:06:52
--------------------------
64853
10872
Đã từ lâu tôi thấy vô lý khi cứ phải làm việc công sở 8h - 5h ?
Tuần làm việc 4h là cuốn sách thực hiện hóa ước mơ
Thoát khỏi cuộc đua tranh, làm nhiều không có nghĩa được nhiều .
Sự lựa chọn hơn mọi nỗ lực, bận rộn chỉ là hình thái lười – suy nghĩ.
Cuốn sách hướng về những bước " thực hành ngay " áp dụng ngay
Sống theo cách mà bạn muốn
Xóa bỏ các giới hạn về thời gian trước kia, làm việc bằng hiệu quả chứ không tốn quá nhiều thời gian.
bước chân tự do phía trước, dám bước ra khỏi số đông, để gia nhập nhóm NewRich :)

4
37796
2013-03-22 09:00:29
--------------------------
5851
10872
Lạ bởi tiêu đề đã nói lên gần như trọn vẹn thông điệp của tác giả gửi gắm trong cuốn sách, nhưng với độc giả ở Việt Nam dường như còn xa lạ.
Cuốn sách không khuyên bạn từ bỏ công việc mình đang làm mà là giảm thời gian làm của bạn, nếu bạn đang làm thuê cho công ty tư nhân - ưu tiên hiệu quả công việc hơn thời gian bạn hiện diện ở công sở, nếu bạn đang tự mình làm chủ một doanh nghiệp hoặc một cơ sở kinh doanh bạn có thể áp dụng những phương pháp của tác giả để giao bớt việc lại cho nhân viên.
Cuốn sách không những là một món quà dành tặng những người sống và làm việc theo nguyên tắc 80/20, nguyên tắc làm ít được nhiều, rằng chỉ 20% việc bạn làm trong ngày mang lại 80% hiệu quả mà còn dành cho những ai bận rộn, không có nhiều thời gian dành cho gia đình, bạn bè và cả chính cho bản thân, vì rằng cuộc đời không dài như bạn nghĩ, hãy sống chậm lại, làm ít đi và yêu thương nhiều hơn.

4
5623
2011-06-07 17:01:38
--------------------------
