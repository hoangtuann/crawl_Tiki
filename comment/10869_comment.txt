517633
10869
Đây là một quyển sánh rất đáng đọc, được xếp vào hàng những quyển sách kinh điển của văn học thế giới. Lối viết văn chứa đựng những triết lí sâu sắc, những nhận định giá trị về văn chương, cái đẹp về thiên nhiên hay con người
5
1423683
2017-02-02 14:26:30
--------------------------
474780
10869
Cuộc sống đã được khắc họa một cách rõ nét và vô cùng chân thực trong 2 tác phẩm Bông hồng vàng và Bình minh mưa.qua ngòi bút của tác giả. Sự tinh tế và tài năng của tác giả được thể hiện qua từng nét nhỏ của 2 tác phẩm này. Qua hai tác phẩm này, cái đẹp của cuộc sống đã được tác giả truyền tải rõ nét đến bạn đọc,và những câu từ ấy đã làm chúng ta cảm thấy vui vẻ hơn, và yêu cuộc đời này hơn nữa. Bìa sách đẹp,và so với hai tác phẩm hay tuyệt được gộp lại trong quyển sách này thì giá bìa rất phải chăng.
5
412050
2016-07-11 22:41:23
--------------------------
356997
10869
Cuốn sách này giúp ích rất nhiều cho việc học tập của mình và trong đời sống. Truyện ngắn dễ đọc nhưng vẫn đầy ý nghĩa. Theo mình thấy thì truyện của Paustovsky luôn có nội tâm phức tạp, mô-típ lãng mạn, có nét trầm lắng nhưng nhiều tâm tư trong tính cách, có nhiều tình huống gặp ngỡ ngẫu nhiên, run rui, tình cờ gợi lên định mệnh tác động rất nhiều lên nhân vật.

Mình thích nhất nội dung trong mẫu truyện ngắn trong sách như "Bụi Quý", "Lẵng Quả Thông", "Bình Minh Mưa" và "Cầu Vồng Trắng". Mình thấy cuốn sách này rất đáng để dành ra thời gian rảnh để đọc.

4
306753
2015-12-22 19:47:18
--------------------------
340912
10869
Tôi đã từng đọc ngấu nghiến cuốn sách này khi được cầm nó trên tay. Nó là món quà vô cùng đáng yêu từ cô giáo dạy văn đáng kính của tôi. Mỗi  tác phẩm trong cuốn sách là một câu chuyện với văn phong riêng biệt, đem đến cho người đọc một hay nhiều bài học thấm đượm triết lí qua những câu từ, hình ảnh đậm chất nghệ thuật. Bên cạnh những cuốn sách thuộc dòng văn thị trường, cuốn sách văn học này không hề mất đi sức cuốn hút của nó. Đọc từng trang sách, cảm nhận từng câu chuyện hay những chi tiết nghệ thuật bằng mọi xúc cảm của bản thân, tôi học được nhiều hơn khả năng cảm thụ văn học cũng như có lối tư duy nghệ thuật tốt hơn.
5
478202
2015-11-20 20:11:59
--------------------------
330854
10869
Quyển sách này thực ra rất nổi tiếng trong giới phê bình lí luận. Phần đầu là những ghi chép tản mạn về những vấn đề lí luận của chính Pauxtovsky, những trải nghiệm của chính nhà lí luận nổi tiếng này. Phần thứ hai là những câu chuyện khá hay và đặc sắc. Phải kiên nhẫn lắm mới có thể đọc hết và hiểu được một phần nào đó từ quyển sách kinh điển này. Nó thực sự khó đọc và khiến cho những ai không đủ kiên nhẫn sẽ từ bỏ ngay khi chỉ đọc được một vài trang sách. Đây là quyển sách đáng đọc để hiểu sâu kĩ hơn về lí luận văn học!
5
616731
2015-11-03 14:02:07
--------------------------
322013
10869
Tôi chỉ có thể dùng một từ đẹp để nói về tác phẩm này. Có lẽ là cuốn sách thứ 2 sau Giết con chim nhại tôi thích nhất.
Những trang văn của ông dường như không chỉ để kể chuyện, ông gợi lên cho chúng ta cái đẹp qua từng câu chữ, cái đẹp của thiên nhiên, của tình người, của cuộc sống.
Mọi dòng ông viết đều đẹp - đối với tôi là như vậy.
Một cuốn sách rất đáng để mỗi người chúng ta ai cũng nên có, nên đọc, để sau khi gấp cuốn sách lại, ta thấy cuộc đời dù trong bất cứ hoàn cảnh nào, cũng có thể tìm được niềm vui và ý nghĩa.
5
249482
2015-10-15 13:49:43
--------------------------
275029
10869
khi cuộc sống vồn vã đến gần, mọi thứ, kể cả văn học. Những cây bút chạy theo thị trường pha loãng dần chất nhân văn cần nhuần thấm trong tác phẩm, nhạt nhòa dần hình thức nghệ thuật. May thay, giữa đống hỗn loạn ấy, " Bông hồng và bình minh mưa" xuất hiện giữa tủ sách của tôi, gần như là tâm điểm. tác phẩm không chỉ gây ấn tượng bởi ngòi bút tài hoa mà còn bởi thông điệp ẩn chứa bên trong: mọi nhà văn phải thu nhặt "hạt bụi vàng" của cuộc sống để đúc thành "bông hồng vàng'. Là "bụi vàng", tuyệt nhiên không phải là hạt bụi lấm tấm vương khắp trần ai. Ngoài ra, cuốn sách gây ấn tượng bởi bìa sách rất đẹp, giấy tốt.
5
366416
2015-08-22 15:59:07
--------------------------
261408
10869
Cuốn sách “Bông Hồng Vàng Và Bình Minh Mưa” là tập hợp của các truyện ngắn của nhà văn người Nga K. G. Paustovsky. Những mẫu truyện ngắn rất hay và ý nghĩa; lối viết sâu sắc đầy triết lí buộc người đọc phải suy tư đồng sáng tạo cùng với tác giả thì mới có thể hiểu được một phần nào đó. Cuốn sách cung cấp rất nhiều tư liệu cho việc làm văn trong nhà trường bởi những ý kiến văn học về giá trị tác phẩm, quá trình sáng tạo của nhà văn cùng những suy ngẫm rất bổ ích. Tôi thích ý kiến này của Paustovky: “Hạnh phúc sẽ tự mất đi khi nào người ta tự thỏa mãn về nó. Hạnh phúc sẽ chỉ bền vững khi người ta luôn luôn vươn tới và hoàn toàn khát vọng.”
5
276840
2015-08-11 08:26:15
--------------------------
254505
10869
Mình đọc cuốn Bình minh mưa hồi cấp 2, khi chưa có thói quen đọc sách. Ấn tượng với truyện ngắn Cây tường vi, và vì nó mà mình tìm mua quyển này. Truyện vẽ nên khung cảnh nước Nga xa xôi cùng nét đẹp mới lạ của nó, thiên nhiên thật tinh khôi, e ấp qua những nụ hoa đỏ buổi sớm mai; thật hùng vĩ qua những cánh đồng, khu rừng tít tấp. Cô kỹ sư, anh phi công, họ xây dựng đất nước từ vùng quê hoang sơ, đầy hoài bão và lòng kiên định, thật vui khi họ tìm thấy tình yêu, nắm giữ nó. Mình như nhìn thấy 1 Lặng lẽ Sa pa thứ 2, với cái kết rõ ràng hơn... Và đó chỉ là 1 trong nhiều câu chuyện tg kể cho ta nghe trong tp này.
5
358411
2015-08-05 11:43:02
--------------------------
228106
10869
Điều đáng nói là tác phẩm kinh điển ngày càng hiếm trên thị trường. May mắn thay, mình tìm được cuốn này ở Tiki. Trước đó, mình đã phải đọc bản photo của nó. Thực ra, "Bông hồng vàng và bình minh mưa" không khó đọc như một số nhận xét trước đó. Mình đọc một lèo gần hết phần Bông hồng vàng chỉ trong một buổi. Paustovsky đưa ra rất nhiều ý kiến hay về nghề văn, đồng thời, ở một số quan điểm của nhà văn, mình bắt gặp nhiều ý tương đồng với suy nghĩ của mình. Mình chẳng thể diễn tả thành lời trước khi đọc của Paustovsky. Cuốn này thu hút mình đến nỗi đã lên Tiki tìm thêm vài truyện vừa mà tác giả giới thiệu trong này nhưng không thấy. Mình hi vọng Tiki sẽ cập nhật thêm truyện của Paustovsky nhé (Như "Vịnh mõm đen" chẳng hạn)
5
21000
2015-07-14 22:57:11
--------------------------
223629
10869
Có vẻ đây là một cuốn sách khó đọc, nhưng những cuốn sách khó đọc thì thường là những cuốn sách hay. Bởi vì trong đó ẩn chứa rất nhiều giá trị sâu sắc mà chúng ta phải suy ngẫm mới có thể hiểu được, và khi đã "thấm" được những giá trị ấy rồi thì thật tuyệt vời. "Bông hồng vàng và bình minh mưa", một cái tựa rất hay cho một cuốn sách. Thật tuyệt vời nếu được như tác giả, đi đến những nơi trên Trái Đất này và tìm hiểu những điều mới mẻ xung quanh mình. Để hòa mình củng với thiên nhiên và những con người nơi đất lạ. Tôi thích trang bìa của quyển sách này, nhìn nó rất đẹp, cũng chứa không ít điều bí ẩn khiến người khác tò mò. Trang bìa như thể đang tái hiện lên một phần của con đường mà tác giả đi qua vậy.
3
13723
2015-07-07 13:07:53
--------------------------
214943
10869
Từ những trang đầu mình đã thấy đây là một quyển sách khó đọc.Tác phẩm này bạn phải đọc từ từ và nghiền ngẫm từng triết lí câu văn trong đó,có nhiều đoạn mình phải dừng lại rất lâu để hiểu xem tác giả muốn nói gì,bạn phải đọc thật chậm và cảm nhận nó.Mình khâm phục tác giả vì những thông điệp nhân văn đã được gửi gắm trong truyện,từng câu truyện ngắn với mỗi nhân vật số phận khác nhau nhưng ẩn sâu trong đó là tình yêu thiên nhiên yêu cuộc sống quý trọng những gì đang có,ước gì một ngày nào đó mình cũng sẽ được đi du lịch khắp nơi như tác giả vậy.Mình ấn tượng với cái tên truyện Bông hồng vàng và Bình minh mưa và quả thật tác phẩm này không làm mình thất vọng sau khi đọc xong,nó nhẹ nhàng như một cơn mưa.
4
526322
2015-06-25 17:25:30
--------------------------
212161
10869
Bông hồng vàng và bình minh mưa - một tác phẩm xứng đáng được liệt vào danh sách những danh tác đáng đọc nhất mọi thời đại. Cuốn sách đã góp phần làm rạng danh thêm cho nền văn học Nga - Xô Viết. 
Ẩn đằng sau vẻ băng giá bên ngoài, Konxtantin Paustovski sở hữu một tâm hồn Nga tuyệt đẹp với trái tim cháy rực lửa yêu thương. Ông biết rung cảm sâu sắc với cảnh đẹp thiên nhiên và sự hồn hậu của những con người không chỉ của nước Nga mà cả những nơi ông đã đi qua. Trẻ em được khắc họa đẹp nhất trong truyện của ông. Những cậu bé, cô bé Nga và cả một số nước lân cận hiện lên trên những trang viết vừa ngây thơ, vừa giàu trí tưởng tượng và những phút giây nghiêm nghị hay nghi ngờ của chúng thật dễ khiến ta bật cười. Hãy tìm đọc tác phẩm nhé.
4
337674
2015-06-21 21:21:36
--------------------------
108689
10869
Cám ơn Paus , vốn dĩ k thích đọc truyện , nhưng sự tò mò về cái tên Sách, đã khiến tôi tìm đến và đọc nó, tác phẩm khơi gợi cho những bạn trẻ về 1 tình yêu và khát vọng khám phá thế giới văn chương, một con đường đầy sáng tạo thú vị nhưng cũng đầy trở ngại. Lối viết giản dị, đầy chất thơ mà dĩ nhiên không thể không nhắc đến tài năng của dịch giả. Luôn muốn đọc lại mỗi khi buồn, để thấy yêu hơn những giọt mưa lấp lánh, những buổi sáng dịu dàng và những cánh rừng thanh khiết trong ban mai
4
312999
2014-03-21 19:10:03
--------------------------
30263
10869
Được một người bạn giới thiệu về cuốn sách này mình đã tìm đọc thử bởi từ lâu đã biết tới ngòi bút của Paustovsky nhưng chưa có dịp tận mục sở thị. Khi mua về quả ngoài sự mong đợi. Cuốn sách rất tuyệt vời. Là sự kết hợp của hai tác phẩm Bông hồng vàng và Bình mình mưa của cả tác giả, những câu chuyện, văn bản ngắn trong cuốn sách đã đưa đến cho mình những hình ảnh về  thiên nhiên, con người và cuộc sống hết sức phong phú, chân thực qua cách nhìn của Paustovsky. Cuốn sách chứa đựng những triết lí sâu sắc mà có lẽ chỉ để hiểu câu chữ cũng đã ngốn nhiều thời gian chứ đừng nói thấm được cái triết lí đó. Mình mất hơn 2 tháng để đọc nó nhưng bản thân đã thấy mình có nhiều sự trải nghiệm hơn qua từng trang sách. Khi mà mọi người đều chạy theo những tác phẩm Best seller, tiểu thuyết hút khách thì mình nghĩ với những ai ham muốn giá trị sống đích thực nên đọc cuốn sách này.
Tuy nhiên sách in chất lượng giấy hơi mỏng, mềm thành ra việc đọc nghiền ngẫm dễ khiến sách mau hỏng. Mong rằng 2 tác phẩm sẽ được xuất bản riêng với chất lượng tốt hơn phục vụ bạn đọc.

4
25953
2012-06-13 12:50:36
--------------------------
28149
10869
Bông hồng vàng và Bình minh mưa thật sự không là một cuốn sách dễ đọc. Nó nặng về tính chuyên môn, triết lý, và những bạn trẻ hay đọc truyện nhẹ nhàng chắc không đọc hết nổi.
Tuy nhiên, đây là một cuốn sách đầy tính nhân văn. Tính nhân văn được thể hiện qua từng mẩu chuyện nhỏ àm tác gải đề ra. Nếu chú ý một chút, suy nghĩ một chút, thấu hiểu một chút, bạn sẽ nhận ra sức hấp dẫn của cuốn sách này. Cái quan trọng nhất mà sách đề cập chính là vẻ đẹp và bản chất của văn chương. Văn chương tự nó toát lên vẻ đẹp, không cầu kỳ, không ép buộc, và con người với tấm lòng chân thành nhất sẽ dẫ dàng cảm thụ được. 
Có thể người đọc sẽ thấy tác phẩm rất khó hiểu và giáo điều. Nhưng khoan đặt sách xuống. Bình tâm một chút, bạn sẽ hiểu ngay triết lý nhân văn cao đẹp của truyện thôi.

4
33375
2012-05-24 21:27:36
--------------------------
