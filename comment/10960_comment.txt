468877
10960
Sách hay, khái quát tất cả nội dung cần thiết cho những bà mẹ mới sinh con lần đầu. Quan trọng hơn hết là đọc xong cuốn sách này, các mẹ trẻ sẽ yên tâm "đối phó" với thời kì nhạy cảm nhất của con (thời chưa biết nói, chưa biết kêu đau) và đối phó với những kinh nghiệm "gia truyền", "cổ truyền" đôi khi phản khoa học của những người thân xung quanh. 

Đọc sách chẳng bao giờ thừa, đọc những cuốn sách bổ ích xong thấy tự tin hơn hẳn khi nuôi con, nhất là khi trong nhà không có bác sĩ và những bà mẹ ở xa các thành phố lớn, nơi bác sĩ nhi có trình độ chuyên môn "không biết đâu mà lần". 


4
1112105
2016-07-05 18:11:46
--------------------------
369356
10960
Đây là một cuốn sách hay và bổ ích cho những ai đang nuôi con nhỏ. Cuốn sánh có hình minh họa và chỉ dẫn rõ ràng. Các bà mẹ rất nên cần một cẩm nang như thế này, đặc biệt là những người lần đầu tiên làm mẹ, rất lúng túng trong việc chăm sóc con trẻ. Mình mua cuốn "Cẩm Nang Chăm Sóc Và Điều Trị Bệnh Trẻ Em" này trong thời gian Hội sách online nên cũng được giảm hơn một chút. Sách có mục lục từng bệnh rõ ràng người đọc có thể xem từng phần khi cần thiết.
4
837021
2016-01-15 16:56:28
--------------------------
303908
10960
Chắc hẳn bà mẹ nuôi con nào cũng mong muốn con mình luôn được khỏe mạnh, đó là điều quan trọng nhất. Nhưng không phải ai cũng biết cách chăm sóc con khi trẻ bệnh, nhất là khi thấy trẻ bệnh, cha mẹ ai cũng lo lắng và cuống quýt cả lên, do vậy trang bị những kiến thức đối với những căn bệnh trẻ thường hay gặp phải và cách chăm sóc trẻ những lúc cần thiết rất quan trọng. Mình tìm kiểu kỹ rồi và quyết định mua quyển "Cẩm Nang Chăm Sóc Và Điều Trị Bệnh Trẻ Em" này. Một gợi ý cho các bạn không có thời gian nhiều để xem hết 1 lúc cuốn sách này là xem trước mục lục xem có những mục nào và ghi nhớ, sau này khi nào cần sử dụng đến mục nào mình sẽ mở ra xem chi tiết tiết kiệm thời gian lắm đó. Hi.
4
812775
2015-09-16 05:37:57
--------------------------
251534
10960
đây là cuốn sách rất hay, hôm trước mình mua quyển sách về đọc hôm sau đưa bé nhà mình đi tiêm về bị sốt sau đó ứng dụng chỉ dẫn trong quyển sách này thật hiệu quả,quyển sách này không chỉ điều trị bệnh trẻ em mà còn có phần cấp cứu cho em bé, mình hi vọng mỗi bậc cha mẹ nên đọc để phòng khi có chuyện xảy ra có thể xử lý kịp thời, hay có thể nói nó là quyển sách gối đầu hay.mình mong nó sẽ giúp mình trong quá trình chăm sóc bé những lúc bé bệnh!

5
696125
2015-08-02 22:07:40
--------------------------
239781
10960
Nếu bạn là người cẩn trọng trong việc chăm sóc sức khỏe của con thì đây hẳn sẽ là một quyển sách bổ ích và cần thiết cho bạn. Trong quyển sách này, bạn có thể tìm được từ những bệnh thông thường đến những bệnh nghiêm trọng, từ triệu chứng đến cách theo dõi, từ đó bạn có thể lựa chọn được cách "ứng xử" phù hợp với bệnh của con. Có những bệnh phải đi bác sĩ để điều trị bằng thuốc, bằng các can thiệp y tế khác nhưng có những bệnh chỉ cần theo dõi tại nhà và để cho bé tự khỏi bệnh - là một cách tăng thêm sức đề kháng cho bé. Thực sự đây là một quyển cẩm nang chi tiết, đầy đủ và cần thiết cho các bậc cha mẹ!
5
3112
2015-07-24 08:39:03
--------------------------
