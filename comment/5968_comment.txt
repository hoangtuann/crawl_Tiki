431493
5968
Đọc giới thiệu nội dung mình thấy Bản Thông Báo Tử Vong có vẻ giống với Ánh Sáng Thành Phố của Lôi Mễ, cũng kiểu nhân vật sát thủ thay trời hành đạo, trừng trị những kẻ xấu thoát được lưới pháp luật. Đúng là cái khung truyện tương tự thế nhưng Bản Thông Báo Tử Vong lại khai thác theo một cách hấp dẫn riêng và dần mở rộng đề tài ra. Điều đầu tiên mình cảm nhận được ở tập 1 BTBTV là tác giả viết rất logic, chặt chẽ, theo sát phương pháp phá án của trinh thám hình sự. Truyện đọc khá trôi chảy, nhân vật cũng dễ ưa nên mình còn thích hơn series Phương Mộc của Lôi Mễ. Cơ bản là truyện càng đọc càng hay, cực kỳ đặc sắc, biến chuyển khôn lường khiến người đọc không khỏi ngỡ ngàng, và nhất là các vụ án mạng sau luôn đáng sợ hơn các vụ trước nên mang đến sự phấn khích cao độ. Thực sự mình thích tác phẩm này. Tập 1 vẫn là tập xuất sắc nhất, bởi mình đã đọc trọn bộ rồi nên có thể dám chắc điều đó.
5
386342
2016-05-17 14:32:54
--------------------------
408468
5968
NXB:			Văn Học
Người dịch:		Hương Ly
Thể loại:		Trinh thám
Bìa:			Đẹp
Chất lượng in, giấy:  	Tốt
Chất lượng dịch:	Tốt
Nhận xét:		Nhiều tình tiết đang cài phức tạp, càng đọc càng thấy thích. Các âm mưu, kế lồng trong kế, nhân vật tưởng vô hại lại quá nguy hiểm, nhân vật tưởng không ai địch nổi bỗng nhiên chết lãng xẹt. La Phi, nhân vật chính, có quá khứ quá phức tạp, quan sát tinh tế, nhạy cảm. Hết rồi ưu điểm. Nhược điểm a??? Khoảng hơn mười lỗi chính tả. Kiến thức sử dụng mạng, thư điện tử hơi tệ. Cách giải thích các sự kiện khá đơn giản, thiếu tính hợp lý.
Khuyên: 		Đọc được.

4
129879
2016-03-31 19:30:05
--------------------------
372673
5968
Từ kết thúc của bộ truyện cho độc giả cảm giác gai góc về một phần của con người , đối tượng mang đến cái nhìn yếu ớt , nằm dài chờ đợi bởi thế lợi mà cấu kết bởi sự đơn giản tuần hoàn lặp đi lặp lại giúp cho đường đời trở thành dễ thở hơn , bản thông báo tử vong là cái kết được biết trước cảnh báo cho sinh ra những trở ngại lớn hơn nhằm kết thúc ra cái sự việc rõ ràng , khó khăn đảm bảo được sức nặng cần đối phó .
4
402468
2016-01-22 02:52:12
--------------------------
347477
5968
Vẫn luôn có cảm tình đối với tiểu thuyết trinh thám của TQ thế nên vẫn mua về đọc:
- Về mặt trình bày thì khá bắt mắt, với dòng marketing "khiêu chiến với trí tuệ của bạn", thế nhưng nó chưa thật sự như thế. Trong truyện vẫn còn vài lỗi dịch thuật: như là sót chữ Hán, viết sai chính tả.
- Về mặt nội dung thì khi đọc đến vụ án đầu tiên quả thật rất lôi cuốn, làm mình luôn tò mò thủ pháp giết người của hung thủ. Nhưng càng đọc về sau thì có vẻ như tác giả chỉ tạo tiền đề cho các sự kiện tiếp theo diễn biến suôn sẻ. Thế nhưng nó chưa thật sự suôn sẻ. Như vụ án bắn nhau ở công viên Song Lộc Sơn, mình không nghĩ việc 'trộm long tráo phụng' lại diễn ra trôi chảy không chút vấn đề như vậy, điểm khó chịu nhất như bạn Hùng đã comment, đó chình là đầu đạn. Thử hình dung, viên đạn bắn vào người, rồi gắp gỡ ra rồi tráo viên đạn khác vào mà không để lại manh mối nào sao. Đọc đến đây cảm thấy lý do khá gượng ép.
Chưa kể tới vụ nổ bom của Viên Chí Bang hại chết Mạnh Vân. Ban đầu mình còn nghĩ rằng Mạnh Vân có tình cảm với Viên Chí Bang, nên muốn giải cứu anh ta bằng mọi giá chứ. Còn nếu nói về tranh đấu giữa La Phi và Mạnh Vân để làm cớ cho Mạnh Vân giải cứu Viên Chí Bang thì có vẻ chưa đủ thuyết phục.

Nói chung đây là tác phẩm vẫn đáng đọc vì nội dung mach lạc, bô cục chặt chẽ, hành động liên tục, vẫn khá hấp dẫn đối với mọi người, đương nhiên đừng bơi móc các sự kiện như mình và bạn Hùng :)
4
23913
2015-12-04 14:56:48
--------------------------
319995
5968
Mình cũng thuộc dạng ghiền sách trinh thám, lâu lâu cũng đá xéo ngôn tình...
Nhìn chung cuốn sách này đáng để đọc và suy ngẫm....nhưng thật lòng mà nói, cái nguyên nhân đầu tiên dẫn đến tội ác của Emanuens k đủ thuyết phục mình lắm, tranh tài kiểu này không đủ tính cạnh tranh để dẫn lối tội ác của nhiều năm sau này, sự logic không đủ ở nhiều tình tiết trong truyện, tâm lý bị lấn át...
Nhưng nó đủ phản ánh sự chân thực của xã hội...sự tham lam, tình yêu mù quáng, cạnh tranh cái không đáng...dẫn đến tội ác


4
728441
2015-10-10 09:48:45
--------------------------
314634
5968
Điểm cộng của Bản thông báo tử vong là tình huống truyện rất hấp dẫn mặc dù gần đây đã trở nên quen thuộc. Cách triển khai, cấu tứ của tác giả đã làm cho nó không bị cũ hay nhàm chán, ngược lại vẫn rất cuốn hút. Truyện có cao trào và càng đi đến gần đích, khi câu chuyện dần được mở ra thì cao trào càng dâng cao, rất hấp dẫn.
Điểm trừ là truyện có rất nhiều nhân vật nhưng không đẩy cho nhân vật nào lên làm chính hẳn, cảm giác hơi phân tán khi đọc. Ngoài ra động cơ trả thù của Viên Chí Bang khá là thiếu thuyết phục. Không hiểu sao một người bạn gái hắn ta đã chủ động chia tay lại thay thế được tình bạn sâu sắc nhiều năm giữa hắn và người bạn chung phòng, để dẫn đến cái chết thảm của bạn gái người bạn đó cùng một loạt những cái chết sau này khác. Tác giả có thể nghĩ được một lí do tốt hơn để thay thế thì độ thuyết phục sẽ tăng lên nhiều.
Nhìn chung đây là một cuốn sách hay, đọc xong cảm thấy thỏa mãn.
4
102259
2015-09-26 18:55:45
--------------------------
286691
5968
Đây là quyển sách trinh thám đầu tiên mà tôi được đọc của Trung Quốc. Và sau đó tôi cũng thích thể loại này nhờ "Bản thông báo từ vong". Cách dẫn dắt câu chuyện của tác giả thật logic và tuyệt hảo, dẫn người đọc đi nhiều vòng lớn rồi lại quay về. Khiến tôi không thể đoán trước được thứ gì sắp diễn ra. Và khi đọc thì tôi bị lôi cuốn đến mức chẳng dừng lại được, có đôi khi tính tò mò lại thôi thúc tôi lật đến trang cuối chỉ để biết được chân tướng. Đây là một tác giả thật tuyệt vời và thông thái trong việc xây dựng tình huống và nhân vật, thêm những tình tiết bất ngờ lôi cuốn. Tôi như bị lôi vào diễn biến của câu chuyện mà chẳng thể rời ra được. 
5
500025
2015-09-01 22:21:46
--------------------------
281064
5968
Dạo gần đây tiểu thuyết trinh thám rùng rợn của Trung Quốc có vẻ đang lên nên sau cuốn "Đề thi đẫm máu" của Lôi Mễ thì mình quyết định đọc đến cuốn này. Cũng với mô-típ sát thủ hàng loạt đấu trí với cảnh sát, kịch tính của truyện chủ yếu có được từ việc hung thủ gửi "bản thông báo tử vong" đến nạn nhân cùng với sự bao vây của cảnh sát nhưng văn phong của Chu Hạo Huy có vẻ thiên về miêu tả tâm lý nhiều hơn, trong khi cái cần thiết của truyện trinh thám là các tình tiết hấp dẫn thì lại bị tâm lý lấn át và có đôi chỗ còn khó hiểu về mặt logic. Truyện khá thu hút tuy sự rườm rà ở nhiều điểm còn khiến mình lưỡng lự khi cân nhắc mua những cuốn tiếp theo. 
3
199482
2015-08-28 08:34:11
--------------------------
260348
5968
Mình khá thích đọc các thể loại phá án-kinh dị, nhưng đây là lần đầu đọc sách của tác giả Trung Quốc. Truyện khá hấp dẫn, lôi cuốn; khi đọc có vài chi tiết tuy vụn vặt nhưng hoá ra là gốc rễ mấu chốt cả vấn đề. Lôi Mễ dẫn dắt câu chuyện khá hấp dẫn, nhưng có hơi rườm rà và thiên về tình cảm một chút so với các truyện khác. Không mang quá nhiều màu sắc kinh dị nhưng vẫn cảm nhận được sự u ám trong từng phân đoạn. Mình đọc cả ba cuốn thì thấy là tình tiết về sau khá khó đoán, làm cho câu chuyện lôi cuốn hơn :)
4
719564
2015-08-10 12:26:44
--------------------------
257511
5968
Cốt truyện ổn và cách truyền tải nội dung của tác giả không có gì phải bàn, nhưng chắc kỳ vọng của mình hơi cao nên không cảm thấy ấn tượng với tác phẩm. Những đoan so tài tay đôi và đấu trí giữa tên sát nhân cùng nhóm cảnh sát gồm những cá nhân tài năng khá lôi cuốn, nhưng mình bắt gặp một số chi tiết có vẻ như từng xuất hiện ở một số cuốn tiểu thuyết trinh thám - hình sự Trung Quốc khác (và dường như đã trở nên quen thuộc với các fan của thể loại truyện này).
Đọc xong cuốn này nhưng mình cũng chưa muốn mua 2 cuốn tiếp theo. 
3
419258
2015-08-07 18:43:24
--------------------------
231019
5968
Mình đã đọc hết cả ba phần của series này , và mỗi cuốn mình chỉ mất một ngày để đọc. Thật sự là mình không thể nào dừng đọc được vì những tình tiết trong câu chuyện quá hấp dẫn và lôi cuốn đối với mình.Đây thật sự là một trong những tác phẩm viết về trinh thám mà mình yêu thích nhất .Những tình tiết trong truyện có lúc bị đẩy lên cao trào khiến mình không thể dừng mắt để làm việc khác được.Như cái tựa đề của tác phẩm,tên sát thủ trong truyện thật sự là một tên sát nhân mà có thể giết chết được đối phương một cách vô cùng dễ dàng như trở bàn tay.
4
483530
2015-07-17 21:04:38
--------------------------
186742
5968
Có lẽ vì cái bóng của Lôi Mễ trong lòng quá lớn nên khi đọc "Bản thông báo tử vong" mình thấy không thực sự bị thu hút.
Về văn phong của tác giả, theo mình thì nó quá rườm rà và màu mè, có rất nhiều đoạn khi đọc lên có cảm giác dông dài và khá gượng. Mặc dù là tiểu thuyết trinh thám nhưng lại giống một cuốn tiểu thuyết tâm lý xen lẫn yếu tố giết người và phá án hơn.
Các màn đấu trí, rượt đuổi trong truyện cũng khá hay nhưng không đủ để mình tiếp tục muốn đọc tập 2.
2
142727
2015-04-21 20:52:11
--------------------------
179061
5968
Khi đọc truyện này, mình cảm giác như độc giả cũng buộc phải tham gia vào quá trình lựa chọn tương tự như những nhân vật trong truyện. Bởi vì quan điểm chính nghĩa của họ khác nhau, suy nghĩ về sự công bằng của họ cũng khác nhau, nên họ có những chọn lựa khác nhau.

Câu hỏi trong đây là rốt cuộc chính nghĩa nằm ở đâu? Mình khi gấp sách lại vẫn khó có thể trả lời. Ai đúng ai sai, cuối cùng vẫn không thể biết được. Những người mà thủ phạm sát hại đều là những người đáng chết, nhưng mà con người đâu thể tự tiện quyết định sống chết của nhau? Nhưng chính những nạn nhân đó đều đã từng không mảy may quan tâm đến sống chết của người mà họ từng hại.

Cơ bản thì mình thấy bộ này hay, đáng để đọc và suy ngẫm. Nó cũng rất thực tế, bởi rõ ràng xã hội bây giờ chính là như thế.

Nếu có ai rảnh thì mình nghĩ nên đọc qua, không phí tiền hay công sức đâu
5
502530
2015-04-06 13:07:11
--------------------------
151017
5968
Trước nay mình đã quen đọc thể loại trinh thám nặng nề và rùng rợn, có thể nói là gây ám ảnh và để lại ấn tượng khá nặng nề. Vì vậy mà đến khi đọc một truyện trinh thám kiểu như Bản thông báo tử vong, mình rất thích vì truyện có nhịp điệu nhanh, mạnh và dồn dập khiến mình đã đọc là đọc một lèo không muốn ngừng lại.

Khi đọc truyện này khiến mình liên tưởng đến việc xem một bộ phim hành động gây cấn và hấp dẫn, với những màn đấu trí và những cuộc rượt đuổi ngoạn mục giữa cảnh sát và sát thủ. Dù cảnh sát có bày binh bố trận như thế nào, giăng thiên la địa võng ra sao thì Eumenides vẫn cứ có thể ra tay giết người theo như Bản thông báo tử vong được gởi đến chính nạn nhân từ trước bằng trí tuệ và năng lực hơn người của hắn.

Theo như chính câu đề từ "khiêu chiến với trí tuệ của bạn", mình thật sự cho rằng đây là truyện đấu trí đúng nghĩ, cả 2 bên đối nghịch đều một chín một mười, không phải cảnh sát yếu thế, mà chỉ là đối thủ ngang tầm của Eumenides vẫn chưa chính thức"lên sàn".

Phần này thì  Eumenides vẫn chỉ còn ẩn trong bóng tối nên ngoài những kỹ năng sát thủ được huấn luyện chuyên nghiệp đầu đầu óc thông minh vượt trội ra nhân vật này vẫn chưa thể hiện được nhiều. Còn lại bên giới cảnh sát, mỗi cá nhân đều được xây dựng với cá tính riêng: đội trưởng Hàn Hạo lạnh lùng và sắc bén, nữ giảng viên tâm lý Mộ Kiếm Vân xinh đẹp và dày dạn bản lĩnh, chuyên viên IT xuất sắc Tăng Nhật Hoa dù luôn lộ vẻ bất cần và nhàn tản nhưng khi đã bắt đầu vào nhiệm vụ là chỉ biết dốc hết sức mình để làm. Và vai chính của chúng ta, cảnh sát La Phi tuy ở p1 này vẫn chỉ là một người khách đóng vai trò "cưỡi ngựa xem hoa" nhưng mỗi phân đoạn anh xuất hiện đều để lại ấn tượng sâu đậm và thể hiện được ít nhiều bản lĩnh toàn năng của mình. Họ là tổ hợp hoàn hảo cho trận chiến với  Eumenides vẫn còn kéo dài 

Mong chờ những phần tiếp theo của series này.
4
13737
2015-01-18 13:31:28
--------------------------
148862
5968
Sau khi đọc bộ truyện của bác Lôi Mễ, mình đã rất kì vọng vào truyện trinh thám Trung Quốc và quyết định mua cuốn sách này.
Truyên cũng hay, các chi tiết về logic không gây khúc mắc gì nhiều, nhưng không thể nói là cẩn mật như bộ truyện của Lôi Mễ. Điều này có thể hiểu được vì tác giả không phải là người trong ngành. Vả chăng bộ truyện này có lẽ thiên hơn về đấu trí kiểu Death Note (xin lỗi vì đã so sánh với truyện tranh, dù sao số sách trinh thám chữ mình xem qua cũng không nhiều). Lời mở đầu của dịch giả hơi lố.
Về kết thúc của tác phẩm, dù gây hụt hẫn, nhưng chứng tỏ là vẫn sẽ có những tập sau nên an ủi được chút ít. Dù mình vẫn thích cái kết riêng cho từng tập trong bộ tiểu thuyết dài hơn.
Có một số chi tiết mình vẫn vướng mắc (có thể có spoil):
- Vụ Hàn Thiếu Hồng: Mình không tin là người ta có thể điều khiển ngần ấy người theo cách đó.
-Vụ Đặng Hoa, như bạn Trần Mạnh Hùng có nhắc: ông chủ ấy mặc một cái áo đỏ bên trong bộ áo của vệ sĩ. Quá nổi bật. Không thể giải thích là sắp đi gặp đối tác nên phải mặc hờ cái áo bên ngoài vì hoàn toàn có thể thay áo trên máy bay. A Hoa là một vệ sĩ xuất sắc, lại đứng ngay cạnh ông chủ, thế mà không nhận ra biểu hiện lạ của Hàn Hạo khi anh ta đến gần ông, cũng như lúc anh ta rút súng ra, A Hoa cũng để yên cho anh ta nổ súng.
-Viên Chí Bang đào tạo được tên sát thủ toàn tài, không có ghi chép, đúng thế. Nhưng anh ta dạy võ nghệ, bắn súng,... cho thằng nhóc như thế nào khi mà anh ta bị tàn tật. Không loại trừ là anh ta giả vờ, nhưng với khoảng cách quá gần khi bom nổ, lại lâm vào tình trạng nguy kịch ở bệnh viện, có cảnh sát, y tá, bác sĩ xác nhận, anh ta cũng khó mà đủ lành lặn sau vụ nổ bom.
3
440577
2015-01-11 20:23:59
--------------------------
148820
5968
Khi đọc truyện này xong, mình thấy truyện vẫn chưa thực sự khơi dậy được sự kích thích trong lòng người đọc cho lắm. Nhưng mình vẫn công nhận đây là một tác phẩm hay, mình thích cách mà La Phi cùng với Eumenides đối đầu nhau. Nhưng có lẽ phần mình thích nhất là La Phi cùng cô bạn gái cũ cá tính của mình- Mạnh Vân cùng so tài hơn.... Tình huống truyện lấy ra cũng khá đặc sắc nha, là mỗi lần Eumenides muốn "tử hình" ai đó, hắn sẽ gửi "bản thông báo tử vong" tới người đó, mà mục đích chính lại là khiêu chiến với chính nghĩa, tìm ra đối thủ thực sự trong trò chơi "đẫm máu" này.... Khá liều lĩnh, cơ mà mình thích, cũng thích cách La Phi đặt mình vào vị trí đối thủ để tìm ra lỗ hổng của đối thủ nữa. oa,... thực sự khi đọc truyện mình rất thích những đoạn so tài "tay đôi" lắm!!! Nên mình cảm thấy truyện này tuy không xuất sắc như " Đề Thi Đẫm Máu" nhưng nhìn chung thì truyện cũng đã thể hiện được nét đặc trưng riêng của mình. Không hẳn như tựa đề " khiêu chiến với trí tuệ của bạn" nhưng chắc chắn sẽ làm bạn phải hứng thú với những cuộc "đấu trí" trong truyện.
4
495887
2015-01-11 18:30:44
--------------------------
143191
5968
Tác phẩm hội tụ khá đầy đủ những điều cần thiết để tạo nên một quyển sách trinh thám, tình tiết hấp dẫn đầy bí ẩn, nhiều yếu tố bất ngờ tiếp nối, những tình huống phân tích suy luận khiến khán giả khó mà cưỡng lại sức hút của sự tò mò ở bản thân.
Tuy nhiên đó chỉ là ở sự khởi đầu của tác phẩm, đầy trí thông minh, đầy yếu tố logic nhưng càng về cuối tác phẩm càng lộ rõ nhược điểm của mình, logic thì có nhưng tạo cho người đọc 1 cảm giác bị dẫn dắt  một cách khá thô và ràng buộc, cuối cùng vẫn còn nhiều câu hỏi đặt ra sau đoạn kết, rất nhiều điểm nghi vấn chưa được giải đáp hoàn hảo cho người đọc một cách trọn vẹn.
Sự tài hoa hài hoà giữa tài năng và tội ác của tên tội phạm Eumuenides đối đầu cũng những bộ óc thông minh xuất chúng tao thành 1 tác phẩm hay dù vẫn còn vài hạt sạn nhỏ và dấu chấm cuối cùng lại khá mờ nhạt. Hy vọng trong tương lai tác giả sẽ có những tác phẩm hoàn hảo hơn.
4
451219
2014-12-22 02:20:56
--------------------------
139374
5968
Bản thông báo tử vong hấp dẫn tôi ngay từ khi đọc tóm tắt truyện . Một tên tội phạm như thế nào mà có thể thoát khỏi vòng vây tứ phương của cảnh sát hết lần này đến lần khác một cách dễ dàng đến như vậy cơ chứ . Tại sao hắn làm được mà không hề để lộ sơ hở ? Quan trọng nhất là hắn là ai ? Không biết bao nhiêu câu hỏi đã xuất hiện trong đầu tôi khi đọc câu chuyện này . Mạch truyện rất gay cấn và hồi hộp từng câu chữ bạn phải để ý kĩ càng mới có thể phát hiện ra được manh mối mà tác giả để lại . Những gì tưởng chừng như không thể nhưng sau khi loại trừ hết khả năng lại biến thành có thể . Một cuốn sách trinh thám trên cả tuyệt vời
4
337423
2014-12-06 19:43:30
--------------------------
133182
5968
Là một người đọc mê tiểu thuyết trinh thám và từng đọc những tác phẩm như Sherlock Holmes, tôi cảm thấy tác phẩm này thực sự không làm tôi hài lòng. Cốt truyện và diễn đạt khá lôi cuốn, khiến người đọc phải luôn theo mạch văn của tác giả, suy nghĩ, tìm tòi và khám phá, đoán nhận thủ pháp và soi kĩ từng đầu mối. Trong giai đoạn này tác phẩm khá thú vị. Tuy nhiên, khi đọc xong tác phẩm, một cái kết hụt hẫng, khiến tôi cảm thấy không một chút hài lòng. Hi vọng rằng những tác phẩm của Chu Hạo Huy có thể đạt được những dấu ấn tốt hơn trong lòng người đọc
3
39744
2014-11-04 12:54:56
--------------------------
133034
5968
Trong truyện, những tình tiết có phần hấp dẫn và lôi cuốn, làm cho người đọc phải động não mà suy ngẫm, phân tích. Đọc từng trang sách, tôi không thể ngưng lại để chờ đợi lúc nào khác có thể tiếp tục đọc. Tuy nhiên, khi đọc xong tác phẩm, tôi cảm thấy thực sự hụt hẫng và có chút phẫn nộ, hứng thú thực sự vẫn không thể xác định đó là ai, vẫn chưa thực sự có một lời giải thích đáng. Tôi hi vọng hơn thế, ít ra tác phẩm nên có một cái kết có thể thuyết phục được người đọc.
3
453922
2014-11-03 16:15:21
--------------------------
131154
5968
Về cơ bản, bố cục của câu chuyện chặt chẽ, nội dung được trình bày mạch lạc, có thứ tự, lớp lang rõ ràng, có thể coi là hấp dẫn. Tuy nhiên, có một số sự việc trong truyện thật sự khiên cưỡng, tôi thắc mắc về vài điểm như sau:
1. Mạnh Vân – Diễn biến tâm lý và hành động không nhất quán? Nhân vật này từ đầu đến cuối chỉ xuất hiện đúng một lần duy nhất trong đoạn hồi tưởng của Viên Chí Bang, nhưng cũng có thể đoán được cô là người thông minh xuất chúng, chỉ vừa tỉnh dậy sau khi bộ đàm phát nổ ngay bên tai, người bình thường khó mà nhìn nhận vấn đề nhanh như cô, ký ức của La Phi về cô cũng khẳng định điều đó, đồng thời cũng khẳng định cô là người mặc dù hiếu thắng nhưng tâm tư tinh tế, ý chí kiên định, đặc biệt rất yêu thương và tin tưởng vào La Phi. Câu hỏi đặt ra ở đây rất rõ ràng: nếu như cô tin rằng quả bom đó là do La Phi gài lại để thử thách cô, lại biết được quan hệ thân thiết giữa La Phi và Viên Chí Bang như vậy, nếu hợp tình hợp lý thì phải suy luận rằng đây là một trò đùa quá đáng, vì muốn ép cô nhận thua mà La Phi lôi thêm ông bạn thân nhất vào, quả bom đó là giả, chỉ có thể là như thế, mà vốn dĩ chuyên nghành của cô là Tâm lý học, chưa từng được đào tạo qua việc tháo dỡ bom mìn, đề thi này rõ ràng là làm khó cho cô, chẳng việc gì cô phải tham gia vào để mà làm trò cười cho La Phi, cô chỉ việc phủi tay bỏ đi, chẳng việc gì phải nhận thua hay thắng trong trường hợp như thế này, thế thì chứng cứ hoàn hảo của Viên Chí Bang rõ ràng là bị hủy hoại từ trong trứng nước, xác suất thành công cực thấp, nếu không muốn nói là zero. Ở đây còn một điểm nghi vấn nhỏ: trước ngày bom nổ, Viên Chí Bang có mượn bộ đàm của Mạnh Vân để cài vào một thiết bị nổ, nhằm mục đích tạo ra vụ nổ giả để lừa La Phi, tuy nhiên xét đến quan hệ của La Phi và Mạnh Vân, nếu như sau đó họ đổi bộ đàm cho nhau thì sao? Rất không chặt chẽ ở chi tiết này.
2. Án mạng của Hùng Nguyên và Bành Quảng Phúc ở mỏ động – Chắc chắn Hàn Hạo là hung thủ! Xét về trình tự thời gian cũng như cách thức bố trí nhân sự cộng thêm tâm lý đề phòng của Hùng Nguyên sau thất bại trong “vụ ám sát Hàn Thiếu Hồng”, có thể loại bỏ Doãn Kiếm và Liễu Tùng ra khỏi danh sách tình nghi, kể cả hai người này có là đồng phạm đi chăng nữa thì cũng không có cách nào phối hợp với nhau ở khoảng cách xa như thế được. Còn lại hai nghi phạm. Xét đến trường hợp Bành Quảng Phúc là hung thủ, cũng không được, bởi nếu như Bành Quảng Phúc có cách để mở cái khóa của Eumenides thì vẫn còn cái còng tay của Hàn Hạo ở tay bên kia, trường hợp này khó có thể nghĩ Bành Quảng Phúc là hung thủ được, vả lại anh ta cũng chết cùng lúc với Hùng Nguyên, vậy thì chỉ còn lại Hàn Hạo, chỉ cần đọc đến đoạn quả bom nổ tung mỏ động, xóa hết mọi chứng cứ là có thể chắc chắn đến 99% rồi, lí do rất rõ ràng: một người vừa nhận thất bại như Hùng Nguyên, tính cảnh giác sẽ tăng lên gấp đôi, lại là người mà La Phi khen rằng “võ công siêu đẳng”, lại có súng trong tay, chỉ người thân cận nhất mới có thể ra tay với anh ta thôi, mà người thân cận nhất với anh ta trước khi chết chỉ có Hàn Hạo, một tay vỗ vai dặn dò cẩn thận, một tay thò dao ra cắt cổ, quá hợp lý. Nói thật, đọc đến đoạn này mình cứ tưởng Eumenides phải có hai người, một là Hàn Hạo, một là kẻ đã giết Hàn Thiếu Hồng, tuy nhiên vẫn có chỗ không hợp lý bởi nếu thế thì Hàn Hạo chỉ việc trèo lên gác giết luôn Hàn Thiếu Hồng sau khi đổi ca với Hùng Nguyên, như thế vừa an toàn lại vừa bí ẩn, không cần thiết phải bày ra cái màn kịch ở bãi đỗ xe làm gì, nhỡ bị bắt lại không phải là ngu ngốc sao, quá mạo hiểm. Điểm này được lí giải ở nghi vấn thứ ba, cũng là điểm đáng nghi vấn nhất.
3. Vụ bắn cảnh sát ở công viên Song Lộc Sơn – Quá nhiều điểm nghi vấn, quá nhiều điểm sơ hở! Vụ việc này lí giải cho việc Hàn Hạo tại sao lại cứ khăng khăng giết Bành Quảng Phúc cho bằng được, vạn bất đắc dĩ mới phải giết thêm cả Hùng Nguyên, việc này thì rõ ràng rồi. Nhưng các điểm bất hợp lí ở đây: Thứ nhất, Chu Minh – người bị Hàn Hạo giết chết rồi đổ tội giết Trâu Tự - có bắn một phát súng, viên đạn đi trượt và va vào vách đá hoa cương, ở đây cần xét một chút về mặt kĩ thuật, một viên đạn sau khi bắn ra khỏi nòng súng, tùy vào loại thuốc đạn được nhồi sẽ có nhiệt độ dao động từ 350 – 400oc, tức là về cơ bản, với sức nóng như vậy, ta có thể chạm trổ tùy ý lên trên viên đạn, cũng tức là khi viên đạn ma sát với nòng súng sẽ tạo nên đường khương tuyến (cũng giống như dấu vân tay, mỗi một khẩu súng trên thế giới này đều có đường khương tuyến khác nhau, không khẩu nào giống khẩu nào), tốc độ cũng dao động từ 950 – 1200m/s, với loại đạn tiêu chuẩn của súng ngắn (ở đây không xét đến đầu đạn mềm, đầu đạn khoét lõm hoặc đầu đạn chạm nổ.v.v...), cùng với các chỉ số trên, chắc chắn viên đạn sẽ bẹp dí dù cho có chạm vào bề mặt đá hoa cương theo chiều nào đi chăng nữa, vậy thì về cơ bản, mưu kế đổi trắng thay đen của Hàn Hạo không thể nào thực hiện được. Thứ hai, Hàn Hạo sau khi lấy viên đạn ra khỏi người Trâu Tự có đem ra hồ nước để rửa sạch vết máu, và an nhiên không có ai phát hiện ra, việc này là điều bất khả, bởi vì muốn làm sạch đầu đạn, không chỉ rửa mà hết được, chắc chắn trên viên đạn vẫn còn lại các vi chất của máu, sợi vải, thuốc súng... nếu như có người để tâm tìm tòi, lập tức sẽ lòi đuôi ngay. Thứ ba, quy trình khám nghiệm hiện trường cơ bản và giám định pháp y chắc chắn có thể làm rõ vấn đề, ở trong truyện có nói đến việc Doãn Kiếm là người thụ lý vụ án, nếu chỉ có một mình Doãn Kiếm đảm nhiệm tất cả các khâu thì không sao, nhưng còn bác sĩ pháp y, nhân viên khám nghiệm hiện trường thì sao, chắc chắn phải có người nhìn ra vấn đề gì chứ, không thể chỉ dựa trên lời khai của một mình Hàn Hạo rồi đóng vụ án lại được.
4. Vụ việc Đặng Hoa thay hình đổi dạng ở sân bay – Tác giả đánh giá sai về tâm lý nhân vật! Từ trong truyện, có thể thấy Đặng Hoa là người tâm tư kín mật, không có một chút sơ hở, mấy chục năm lăn lộn cả trong hai giới hắc bạch, kẻ thù vô số, có cả người thế thân, đủ biết ông ta cẩn thận đến mức độ nào. Vậy mà lại mặc một cái áo phông đỏ bên trong áo vét, đứng giữa một rừng vệ sĩ áo sơ mi trắng, nổi bật đến nỗi Eumenides đứng ở đầu kia của sân bay cũng nhận ra được, quá sức vô lý...???
Phải thừa nhận rằng đây là một câu chuyện hay, lôi cuốn, có phần nào đó hấp dẫn, nhất là vụ ám sát Hàn Thiếu Hồng, rất đặc sắc... Tuy nhiên, cái gì mà “khiêu chiến với trí tuệ của bạn” chứ? Một chiêu marketing không hợp lý chút nào. Ngay cả lời dịch giả ở đầu truyện nữa, gấp sách lại, cảm thấy mấy lời đó cứ lố bịch thế nào ấy... Theo tôi, đây vẫn là một tác phẩm đáng đọc, tuy không đặc sắc như mong muốn. Chỉ mong dịch giả, công ty phát hành và nhà xuất bản bớt khoa trương đi một chút, không chắc đã không thu hút được độc giả đâu. Thân!
3
380388
2014-10-22 21:57:20
--------------------------
129253
5968
Nói chung quyển trinh thám này rất hay,đang đọc thì mình cứ suy nghĩ mãi mà chẳng biết ai là thủ phạm thực sự,những tình tiết rất gay cấn,kịch tích,không thể   nào mà bỏ quyển sách xuống được,cứ đọc mãi để biết xem hung thủ là ai,tôi cũng đang đọc tác phẩm,đọc tới đoạn cảnh sát Hùng Nguyên bị cứa cổ,không biết hung thủ làm sao mà ra tay nhanh gọn đến như thế,rồi hắn ta trốn đi đâu,cứ như người vô hình,nhưng nếu so sánh với truyện trinh thám của Lôi Mễ thì truyện của Lôi Mễ hay hơn
4
443261
2014-10-08 12:50:24
--------------------------
129239
5968
Quả thật là một tác phẩm đúng như lời giới thiệu ở trên. Rất hay và rất lôi cuốn. Tôi là một fan của dòng truyện trinh thám hình sự TQ, có thể nói tác phẩm này khi so sánh với những tác phẩm của Lôi Mễ thì đúng là một 9 một 10. Lúc đầu đọc thì có cảm giác cốt truyện giống giống như trong "Ánh sáng thành phố", nhưng không nó được truyền tải rất khác, và tôi thích nhất ở chỗ mọi diễn biến câu truyện đều rất logic hợp lý từ đầu đến cuối. Tuy nhiên nó cũng có một số khuyết điểm là truyện quá ngắn ( đọc không đã gì hết trơn ^^) ; và trong khi đọc khó có thể bỏ xuống được ( tốn cả một đêm của tôi ). Mong rằng Cổ nguyệt sẽ tiếp tục cập nhật trọn bộ series của tác giả này. Hãy đọc đi và với những fan của dòng truyện trinh thám chắc chắn nó sẽ không làm các bạn phải thất vọng đâu.
Biết đâu bạn sẽ có nhận xét về phần khuyết điểm giống như tôi :)
5
89685
2014-10-08 10:49:02
--------------------------
