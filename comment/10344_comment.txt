379949
10344
Sách gốc có nội dung khá hay về lý thuyết. Có những phân tích xác đáng và phù hợp thực tế dành cho những ai muốn tìm hiểu sâu hơn về nghệ thuật lãnh đạo. Điều đáng tiếc là cách dịch vẫn không làm toát lên được ý của sách. Nhiều chỗ đọc rất khó hiểu hoặc không đúng với văn phong Việt. Tuy nhiên nếu bỏ qua những hạn chế về dịch thuật, những bạn mới tìm hiểu về lãnh đạo vẫn có thể tham khảo sách này. Sách có hình thức rất ổn, giấy mỏng, hơi dày và nặng.
2
86984
2016-02-13 11:51:34
--------------------------
180329
10344
Tôi đang học quản trị kinh doanh. Nhưng trong suốt quá trình học, tôi thấy các giảng viên và những quyển giáo trình đa phần là đề cập tới quản lý chứ chưa hề đề cập tới những kiến thức về lãnh đạo. Những cuốn sách, tài liệu viết về lãnh đạo thường rất khó đọc, thậm chí là cực kỳ mang tính hàn lâm. Nhưng sau khi tham khảo quyển sách này thì tôi đã có một cái nhìn hoàn toàn khác. Văn phong ngữ pháp trong sách viết rất rõ ràng, không mang tính hàn lâm, thích hợp với cả những người không chuyên mới nghiên cứu về thuật lãnh đạo như cá nhân tôi. Tôi thật sự rất thích những tác phẩm dày trên 500 trang như thế này vì kiến thức rất rộng, phong phú. Một cuốn sách cần có nếu như chúng ta muốn biết sâu hơn về thuật lãnh đạo.
5
387632
2015-04-09 09:54:12
--------------------------
