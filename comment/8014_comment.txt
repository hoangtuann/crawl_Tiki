115307
8014
Khi quyết định đặt cuốn sách này với suy nghĩ là sách của oxford thì có thể yên tâm nhưng khi nhận hàng thì mình mới biết là đã nghĩ sai. Quả thực với giá thành thì cuốn sách này chỉ được về thiết kế bề ngoài còn cách bài trí các bài học thì rất chung. Mặc dù đã có các ví dụ cụ thể của từng mảng nhưng vẫn chưa đề cập đấn các lưu ý quan trọng cần biết và chưa thể chuyên sâu vào mỗi vấn đề, có lẽ là do lưu lượng của sách. Nói chung thì so với một người đang trong bước đầu học tiếng anh thì không nên dùng quyển này bởi lẽ có thể sẽ khó hiểu và bị bỏ sót một số vấn đề quan trọng.
1
284396
2014-06-26 13:03:01
--------------------------
