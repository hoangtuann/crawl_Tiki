458628
3322
Bộ truyện 7 viên ngọc rồng đã gắn liền với tuổi thơ của mình. Lớn rồi đọc vẫn thấy hồi hộp gay cấn và hấp dẫn. Mình lên Tiki mua bộ này rất hài lòng về chất lượng sách và tiki giao hàng cũng rất nhanh và đảm bảo nữa.
Về nội dung tập 29 này là một cuộc chiến đầy ác liệt với các robot sát thủ độc ác và vô cùng tàn ác. Goku do bị bệnh đã bị trong thương tưởng chừng trận chiến sẽ thất bại thì bất ngờ Vegeta đã trở thành Siêu saiya và đánh bại tên robot cứu mọi người. Khi mọi người đanh đuổi theo tiêu diệt tên con lại thì hắn đá kích hoạt 2 robot tiếp theo khiến mọi người gặp nguy hiểm hơn.
5
766204
2016-06-25 12:37:00
--------------------------
303419
3322
Yêu nhất của Dragon ball chính là tính bất ngờ này,không ai là mạnh nhất vĩnh viễn.Songoku đã nhận thất bại đầu tiên từ khi thành siêu saiya và bây giờ Vegeta là người mạnh nhất.Đã có sự hoán đổi vị trí cho nhau chứ không như các truyện khác.Tập này nhà xuất bản có mấy trang in bị mờ đi,đọc hơi khó chịu xí.Nhưng mà mình thông cảm vì truyện này đã xuất bản lâu lắm rồi.Bây giờ tái bản bị như vậy cũng là bình thường.Tuy vậy đọc trên mạng đâu có bị vậy đâu,hy vọng nhà xuất bản rút kinh nghiệm 
4
204594
2015-09-15 20:47:43
--------------------------
297499
3322
Dragon Ball là bộ truyện tranh đã gắn liền với tuổi thơ của mình, mặc dù mình là con gái nhưng mình rất thích và thấy bộ truyện rất hay, gửi gắm trong đó là những thông điệp về tình bạn cũng như triết lý làm người. Bộ truyện mình đọc hồi ấy mang tên là "7 viên ngọc rồng" xuất bản từ năm 1995, giờ ai còn giữ chắc cũng thành đồ cổ mất rồi. Mình muốn sưu tầm lại, thật may mắn là nxb Kim Đồng đã tái bản lại bộ truyện với bản quyền đầy đủ. Truyện được giữ nguyên dạng như bên Nhật, khâu dịch thuật rất ổn, chất lượng in thì khỏi bàn cãi, vì bìa rời nên phải tốn thêm một khoản bọc truyện để bìa không bị xộc xệch hay gập gãy, mình nhìn mà xót truyện lắm! Trong tập 29 này, mình biết thêm một điều khi bác  Akira Toriyama chia sẻ rằng bác ghét chuột kèm theo cái hình rất vui! Nhan đề tập này là Gôku đại thất bại, truyện ngày càng gay cấn khi 2 tên rôbô sát thủ xuất hiện, nhưng cũng chưa phải tên 17 và 18 như dự đoán,Trunks đã vượt thời gian trở lại để báo tin cho mọi người và đưa thuốc cho Gôku, khỉ con của chúng ta thì bị đau tim (bộ cũ là bị đau bụng do ham ăn), mọi chuyện trở nên khó kiểm soát nhưng các anh hùng vẫn chiến đấu đến cùng để bảo vệ Trái Đất.
5
68108
2015-09-11 21:55:36
--------------------------
240886
3322
Một cuộc phiêu lưu đóng lại, thì chuyến phiêu lưu khác lại mở ra.Lần này, mối nguy hiểm không đến từ người ngoài hành tinh nữa mà lại là từ chính người Trái Đất. Trunks đã xuất hiện. Và chắc chỉ có Akira mới nghĩ ra được việc kết hợp hai nhân vật không ngờ ấy lại với nhau để...có nhân vật Trunks. Nhìn mặt Goku khi biết Trunks là ai chắc chẳng khác mặt mình khi đọc truyện này lần đầu.
Không thích bìa rời cho lắm vì khó bảo quản, nhưng dù sao chất lượng truyện của KĐ rất tốt, in đều đẹp, xếp 1 hàng sách không hề bị lệch chữ....=v=
4
291719
2015-07-24 23:40:23
--------------------------
223362
3322
Nói chung mình rất thích bìa rời nhưng bìa rời của Dragon Ball nhiều khi làm mình khá bực mình. Bìa nhiều khí gập không sát, gấp dư ra khiến nhiều khi truyện bị lệch gáy, sục xịch rất dễ bị gãy bìa. Nhưng dù sao cũng tạm chấp nhận được. Trong tập này thì sự xuất hiện của cậu bé đến từ tương lai và những dự báo khủng khiếp đen tối đã buộc mọi người phải rèn luyện để chuẩn bị cho 1 cuộc chiến mới. 2 tên Robot đã xuất hiện nhưng thật sự rất nguy cho nhóm Goku vì họ hoàn toàn không nhận ra khí của chúng? Akira đã khéo léo dẫn truyện theo 1 cuộc chiến liên chiều không-thời gian, mở ra 1 chương mới thật kịch tính, hấp dẫn.
4
166935
2015-07-06 22:52:30
--------------------------
221985
3322
Dragon Ball là bộ truyện đầu tiên mình mua khi sưu tầm truyện tranh . Dù không phải bộ truyện hay nhất nhưng là bộ truyện đánh dấu mốc thời gian mình bắt đầu mua và sưu tầm truyện , có thể nói nó rất ý nghĩa . 
Nhân vật chính của truyện là cậu bé Goku , qua thời gian cậu bé dần lớn khôn và mạnh mẽ hơn . Thực sự lúc đầu khi thu thập đủ ngọc rồng rồi lại ước cái quần chip làm mình thấy vừa tiếc vừa buồn cười . Goku khi lớn cơ bắp cứ phải nói là cuồn cuộn .
Thích bộ truyện này không chỉ bởi nét vẽ mà còn bởi nó rất ý nghĩa . Dù không nói rõ nhưng tình bạn của họ là một sự gắn bó sâu đậm , không có sự phản bội . Họ sát cánh bên nhau và chiến đấu hết mình .
5
335856
2015-07-04 19:39:17
--------------------------
