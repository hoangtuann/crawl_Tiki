445778
5260
Mình mua cuốn này với mục đích học chữ Hán. Mình cũng rất mê thơ văn Hán cổ, & hoành phi câu đối thì có thể nói là đại diện cho sự hàm súc, chữ ít nhưng cả ý nghĩa lẫn sắc thái thì đủ đầy trọn vẹn.

Mình không thích lắm ở điểm khổ sách hơi to, nếu in khổ 13x20cm thì cầm vừa tay hơn, nhưng được cái sách in chữ Hán to vừa phải, rất rõ nét, kể cả các chữ phức tạp & nói chung đủ chỗ để tập viết ngay bên cạnh mỗi câu đối và chú giải nếu cần. 
5
369572
2016-06-10 21:06:50
--------------------------
