331809
7106
Bí mật sẽ mãi mãi là bí mật, cho đến khi nó được đưa ra ánh sáng, và khi đó nó không còn là bí mật nữa mà nó sẽ trở thành một phần câu chuyện của lịch sử.
Cuốn sách cho chúng ta thấy trong thế giới đa sắc màu vẫn còn nhiều mảng tối, vẫn còn đó những âm mưu, những vụ ám sát, những kế hoạch âm thầm, lặng lẽ. Đã có những vụ việc được làm sáng tỏ, được giải thích một cách tương đối logic và được cộng đồng thế giới chấp nhận. Nhưng vẫn còn những vụ chưa có lời giải đáp, để lại dấu hỏi lớn cho nhân loại và cũng có những vụ sẽ mãi mãi chìm vào trong bóng tối, sự thật ra đi mãi mãi cũng những nhân chứng lịch sử. Cuốn sách tập hợp khá nhiều vụ việc lớn, chấn động trên khắp thế giới đảm bảo sẽ mang lại cho bạn những phút giây lý thú.
4
546478
2015-11-05 09:23:24
--------------------------
162666
7106
Cuốn sách là  thước phim tư liệu ghi lại từng chi tiết trong những vụ kỳ án ám sát đen tối nhất trong lịch sử thế giới. Rất hay nhưng sao lại ít người nhận xét vậy nhỉ ?
Cuộc chiến của các thế lực ngầm vẫn luôn diễn ra từng ngày, nhân vật đứng đằng sau lại là những người có sức ảnh hưởng lớn về chính trị. Sự im lặng của bóng tối, các cuộc ám sát chính trị gia kinh động đến hàng triệu người , gây nên nhiều bí ẩn, phức tạp và liên quan đến nhiều thế lực ngầm. Rồi sự thật có che giấu cách mấy vẫn sẽ từ từ được phơi bài !!!
5
570150
2015-03-02 23:08:05
--------------------------
