494982
5188
Mình cực kì thích bộ sách này. Hơi tiếc là mua không đủ bộ và vẫn chưa thể làm đúng được bài học "chỉ ba mươi phút một ngày", cái này không đơn giản tí nào. Mình hi vọng mình sẽ nghiêm túc hơn với hành trình dạy con hi hi. Cám ơn Tiki 
5
985626
2016-12-10 05:11:49
--------------------------
163109
5188
Vì quyển này dành cho độ tuổi 5-6-độ tuổi bắt đầu đi học nên nội dung sách đã khó lên rất nhiều nhưng nếu mẹ đã bắt đầu với các quyển khác của bộ sách này từ sớm thì cũng không làm khó các bé lắm đâu. Vẫn tập trung vào các mảng để nâng cao khả năng ngôn ngữ, đặc biệt là khả năng toán học ở độ tuổi này. Các bé sẽ được khuyến khích để sáng tác một câu chuyf ện, học cách cư xử với bạn bè và người lớn tuổi, học cách tìm hiểu sự vật tự nhiên và xã hội, nhận biết đâu là hành động xấu đâu là hành động tốt...Nếu bé đã đọc tốt rồi mẹ nên cho bé tự đọc xong rồi hỏi lại bé. Muốn trở thành "người mẹ tốt" thực sự không khó nhưng đó là cả một hành trình dài và mỗi người có một cách thể hiện khác nhau và cách thể hiện như sách giới thiệu cũng là một trong số vô vàn cách mẹ thể hiện tình thương yêu với con mình.
4
534227
2015-03-04 09:34:57
--------------------------
