341735
11182
Tâm trạng của mình trong suốt quá trình đọc "Buồn buồn vui vui" cũng hệt như tên gọi quyển sách vậy, buồn buồn vui vui. Vui vì có thể đọc được những dòng tâm tình, chia sẻ thật lòng, chân thành của Nguyễn Quang Sáng, qua đó phần nào hiểu thêm về con người và cuộc đời ông, và những người được ông nhắc đến (như Trịnh Công Sơn chẳng hạn). Càng vui bởi giọng văn, phong cách của ông thật giống như mình đã nghĩ, thoải mái và đậm chất Nam Bộ. Nhưng mình có chút buồn, nói đúng hơn là một xíu thất vọng, dù đã cố gắng không đặt quá nhiều kì vọng lúc đầu. Về phần Nguyễn Quang Sáng, có lẽ do viết thoải mái nên có nhiều đoạn ông viết giống văn nói hơn, đôi lúc lại hơi lung tung do ông kể hết chuyện này đến chuyện kia rồi lại quay về chuyện cũ, đoạn kết của vài bài lại chưa thật là "kết", cứ như vẫn chưa viết đoạn kết vậy. Về phần nhà xuất bản, mình không biết có phải do ông không ghi hay không, nhưng rất nhiều bài không có ngày tháng bên dưới, hình như có bài viết cho báo nhưng lại không có chú thích là viết cho báo nào (dù bên trên ông có ghi), ngày tháng năm nào, số bao nhiêu, có thể mình khắt khe chỗ này quá nhưng mình vẫn hi vọng dưới mỗi bài viết của ông sẽ có ghi ngày tháng, nếu viết cho báo đài thì nên trích cả tên, số vào. Đọc xong chỉ có đôi dòng cảm nhận ngăn ngắn như thế...
4
198872
2015-11-22 21:02:27
--------------------------
340314
11182
Biết đến ông qua truyện ngắn Chiếc Lược Ngà cực kỳ nổi tiếng, tôi đã mua thử quyển này và đọc. Đơn giản là vì giá khá mềm và tôi cũng tò mò liệu "ghi chép cuối đời chưa từng công bố" có gì bí mật giờ mới bật mí hay không. Nhìn chung, tác giả đã chắt lọc lại một số truyện ngắn đã từng đăng trên các báo và mới viết gần đây; bên cạnh đó là phần lớn các tản văn chia sẻ về đời, nghề,... Văn phong ông nhẹ nhàng nhưng ý nghĩa, sâu sắc. Nó đã cho tôi hiểu thêm nhiều về nhà văn. Tiếc là có một số tản văn rất hay nhưng bị cắt ngang (chẳng hạn vì "thời tiết chính trị"), mà nay ông đã thành người thiên cổ, coi như không bao giờ biết thêm được gì nữa. Nhưng dù gì, đây cũng là quyển sách hay bạn nên đọc.
5
448654
2015-11-19 17:55:31
--------------------------
312894
11182
Đọc Buồn buồn vui vui, tôi thấy một hình ảnh khác của tác giả Nguyễn Quang Sáng, một người anh, người em, người cha và người chồng ngoài đời thực. Tôi biết ông ra sao với cố nhạc sĩ Trịnh Công Sơn, ra sao với con trai út là đạo diễn nổi tiếng Quang Dũng  hay nhiều người nổi tiếng nữa. Họ cũng như mình, mộc mạc, giản dị và đầy tình người. Chùc cho nhà văn luôn sống mãi trong lòng bạn trẻ yêu văn học Việt Nam. Sau khi đọc xong cuốn tản văn nhỏ mỏng này, tôi sẽ tìm đọc thêm các tuần san báo Tuổi Trẻ có đăng bài của nhà văn.
4
292668
2015-09-22 12:18:48
--------------------------
287582
11182
tôi được biết ông qua tác phẩm chiếc lược ngà trong chương trình văn học lớp chín. Rung động trước tác phẩm của ông tôi tìm đến buồn buồn vui vui. Bạn thân ông là trịnh Công Sơn làm tôi rất ngạc nhiên. Và càng ngạc nhiên hơn khi biết ông là tác giả của tác phẩm mùa gió chướng và cánh đồng hoang. Thực sự rất mến mộ tài năng của ông. sách đẹp nhẹ có thêm các tư liệu về ông ở cuối trang giúp ta hiểu về ông và cũng như các tác phẩm. Những mảnh truyện còn lại cuối đời. 
5
686606
2015-09-02 21:20:22
--------------------------
242791
11182
Được học truyện ngắn Chiếc Lược Ngà của nhà văn từ những ngày nhỏ nhưng càng lớn tôi cũng đã quên đi tác phẩm này cũng như một nhà văn Nguyễn Quang Sáng rất xúc động mà lúc đó tôi còn nghĩ nhà văn là nhân vật chính trong câu chuyện này. Về những ghi chép của nhà văn trong Buồn Buồn Vui Vui làm tôi thấy sảng khoái. Cách viết "tuôn trào" tự nhiên của nhà văn thật gần gũi, như chính con người miền Tây của ông. Đọc những dòng ông viết cảm giác như ông đang nói chuyện, kể chuyện cho một người bạn nghe chứ không phải "viết" để "đọc" mà là ngồi nói chuyện nhau nghe. Cũng trong quyển này, giúp người đọc biết được về gia đình ông đặc biệt về mối quan hệ thân thiết giữa ông và nhạc sĩ Trịnh Công Sơn. Một quyển sách hay, cần có và nên có, cũng là một cách tri ân...
5
151536
2015-07-26 22:31:53
--------------------------
