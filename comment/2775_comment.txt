309374
2775
Dếrôbốt - Nhân Tài Ảo Thuật (Tập 18) - Gia Sư Chúc Tết với những câu chuyện mang đậm không khí của mùa xuân và đặc biệt là ngày Tết. Nhũng câu chuyện khá vui nhộn và hóm hỉnh nhưng lại rất gần gũi với mọi người như là đi chúc Tết, lì xì, đưa ông Táo về trời,... Ngoài phần truyện chính, tập truyện này còn giới thiệu đến độc giả chân dung và tiểu sử của nhà ảo thuật gia nổi tiếng Dynamo. Chất lượng giấy in có phần dày hơn và tốt hơn các tập truyện trước. Mong chờ những tập tiếp theo từ Tiki.
4
408969
2015-09-18 23:26:08
--------------------------
