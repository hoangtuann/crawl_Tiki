356430
11016
Theo mình thì chất lượng sách rất tốt, hoàn toàn là sách mới, là sách xuất bản mới. Thích nhất vẫn là giấy. Chất liệu giấy tốt, dày, và không gây mỏi mắt. Lối trình bày của sách vô cùng là khoa học, chia ra từng phần để dẫn dắt người đọc đi vào bài học. Sách đặc biệt vô cùng thích hợp với các bạn tự học. Nội dung của sách thì khái quát khá chi tiết về tiếng Anh thương mại, tiếng anh Văn phòng và đc chia thành nhiều chủ đề nhỏ khác nhau. Mỗi chủ đề đều bắt đầu với một table nhỏ chứa toàn bộ nội dung bài học của chủ đề đó. Sau đó là vô số bài tập nhỏ để áp dụng. Nói chung là sách rất phù hợp cho các bạn đang muốn luyện thi các chứng chỉ anh văn quốc tế.
5
1031107
2015-12-21 21:17:55
--------------------------
