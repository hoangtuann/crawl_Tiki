480342
8423
Mình nghĩ cuốn sách này rất hay nhưng không như kì vọng, có nhiều câu chuyện quá sến sẩm, chỉ có vài câu chuyện hay. Mình rất thích câu chuyện về 1 cô gái thử lòng của chàng trai. Người con trai muốn được làm quen với 1 tác giả của cuốn sách, 2 người liên lạc và làm quen với nhau qua thư nhưng không hề biết mặt nhau. Người con trai thực sự đã yêu cô gái qua sự cảm nhận của tâm hồn cô gái. Khi gặp nhau thì cô gái có nhờ 1 bà cô to béo, dáng thô kệch cài bông hoa trước ngực đi gặp chàng trai, cô gái nhắn cho chàng trai rằng e là người con gái cài bông hoa trước ngực. Khi chàng trai gặp thì có hơi thất vọng về ngoại hình nhưng thưc sự chàng trai đã phải lòng tâm hồn của cô rồi. Sau đó chàng trai đã mời bà cô ấy đi ăn, đến nhà ăn thì bà cô ấy bảo nếu anh ấy mời cô đi ăn tối thì hãy bảo là tôi chờ ở rạp phim đối diện. Tình yêu là điều đẹp đẽ xuất phát từ tâm hồn. 
3
749678
2016-08-17 09:54:10
--------------------------
222343
8423
Bìa sách không quá hoa lá, cầu kì, rất đơn giản nhưng thu hút vì nhìn nó rất thoáng và sáng! Đó mới là thành công mở đầu của cuốn sách này.
Còn về nội dung thì lại rất ý nghĩa, nhiều cảm xúc. Những câu chuyện, những câu nói đều rất hay, nhất là đối với những ai đang cảm nhận về tình yêu. Có cho thì sẽ có nhận! Nếu đã yêu thì sẽ được yêu. Tình yêu không bao giờ mất đi cả, nó chỉ lan truyền từ người này qua người khác.
Cuốn sách này sẽ là tình yêu của rất nhiều độc giả, những con người đã, đang và sẽ được yêu thương! 
5
292005
2015-07-05 13:03:53
--------------------------
203542
8423
Mình mua cuốn sách này bởi vì thấy bìa sách nó quá đẹp, quá tươi, quá đúng chất (thích cái bìa sách ngay lần đầu nhìn thấy). Không chỉ bìa sách đẹp mà nội dung sách cũng rất hay. Những câu chuyện tình yêu chứa nhiều cảm xúc, đọng lại trong lòng người đọc những cảm xúc ngọt ngào và xúc động.
Sách có những câu châm ngôn rất hay, rất nhiều ý nghĩa.
Nếu bạn đang yêu, sẽ yêu và đã yêu hãy đọc cuốn sách này để cảm nhận những cảm xúc ngọt ngào len lỏi qua từng trang sách, để cảm nhận yêu thương!
5
621744
2015-06-01 16:47:38
--------------------------
196426
8423
*Đây thật sự là một trong những cuốn sách cảm động mà mình đã từng đọc các bạn à. Yêu cuốn sách này từ khi mới nhìn thấy vì “bé” quá đẹp và nổi bật mà không phải quá cầu kì. Sách màu, có hình, dễ thương.
*Khi đọc cuốn sách, mình càng thấy yêu yêu “bé” này nhiều hơn. Các bạn biết vì sao không? Vì những câu chuyện trong quyển sách cho ta thấy được nhiều điều ý nghĩa của tình yêu. Nó có ngọt, có đắng, có chua chát, hạnh phúc và đau khổ nhưng trên tất cả yêu và được yêu là điều mà tất cả chúng ta cần giữa dòng đời đông đúc và nhịp sống hối hả này. Hãy cho mình được quyền yêu thương và nhận yêu thương từ người khác, hãy cho khi bạn có thể và hãy tin rằng những người có duyên sẽ gặp lại nhau. Và rồi dù có chậm hơn người khác, hãy cứ chân thành và tin rằng sẽ có một ngày, bạn tìm được một nửa của mình.
*Sách còn có thêm một phần là những câu châm ngôn Anh – Việt khá hay và ý nghĩa.
*Hãy sưởi ấm con tim mình và mọi người bằng những câu chuyện ý nghĩa từ cuốn sách này các bạn nhé!
5
45667
2015-05-15 20:58:03
--------------------------
153617
8423
Về hình thức, bìa sách không được thu hút cho lắm, nhìn hơi đơn điệu, tuy nhiên chất liệu giấy khá tốt, chữ bên trong được in màu rất đẹp. Khổ sách khá nhỏ nhắn và dễ thương. Bìa sách cứng cáp và bóng đẹp. Tuy nhiên cuốn sách khá nặng nên cầm lâu dễ bị mỏi tay.
Về nội dung, đây không phải những câu chuyện tình yêu "mì ăn liền" mà những chuyện được tuyển chọn trong đây đều rất chín chắn, chững chạc, nhẹ nhàng mà triết lý. Đôi khi đó chỉ là tập hợp những câu danh ngôn, những câu nói hay về đề tài tình yêu, thế nhưng đều khiến ai nấy phải suy ngẫm và chiêm nghiệm. Những câu chuyện hay, những câu từ đẹp, tất cả sẽ được đem đến cho bạn trong cuốn sách nhỏ xinh này !
5
75959
2015-01-26 20:40:13
--------------------------
146705
8423
Có những cuộc tình chỉ như gió thoảng mây trôi thoắt cái là quên ngay . Có những mối tình mặc dù biết nhớ đến chỉ gặp đau khổ nhưng không tài nào dứt ra được . Yêu ai đó là một cảm xúc kì diệu và cũng kì lạ nhất thế gian này . Quyển sách này đã diễn tả rất tốt khía cạnh đó . Thật không có gì hạnh phúc hơn khi bạn yêu một người và người đó cũng yêu lại bạn .Tình yêu, dù là Được Yêu mà không thể Yêu, dù là Yêu mà không Được Yêu, dù là mối tình đầu, hay mối tình cuối, dù là tình yêu của tuổi trẻ hay tình yêu của tuổi già… vẫn luôn đẹp đẽ như nhau, vẫn luôn là điều kỳ diệu của cuộc đời này.
Quyển sách là quà tặng tuyệt vời cho những ai đã đang và sẽ Yêu và Được yêu.
4
337423
2015-01-05 08:11:00
--------------------------
