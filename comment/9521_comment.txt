478063
9521
Một quyển sách có lẽ không chỉ chứa đựng bí quyết kinh doanh mà còn là bí quyết để sống hạnh phúc nữa. Mình chỉ nghĩ đơn thuần rằng người cho đi sẽ được nhận lại, một cách thụ động, tuy nhiên, mình hoàn toàn bất ngờ cho đến quy luật thứ 5 rằng ''chìa khóa của việc cho đi một cách hiệu quả chính là luôn sẵn sàng nhận lấy'' phải suy ngẫm thật lâu mới có thể hiểu được triết lí trong thông điệp mà tác giả muốn gửi đến bạn đọc, rằng bạn luôn luôn xứng đáng được nhận lại khi bạn đã cho đi. Đáng đọc
5
618204
2016-07-30 17:08:05
--------------------------
448554
9521
Quyển sách là một câu chuyện ngắn về bí mật về sự thành công của anh chàng Joe. Thông qua sự tiếp xúc với những người thành công (người dám cho đi những kinh nghiệm của mình) chúng ta hiểu được rằng để đạt được thành công thì bản thân mình nên mở lòng, chia sẻ cho mọi người xung quanh về những thành tựu, cũng như kinh nghiệm mà mình có được với mong muốn giúp đỡ người khác. Chỉ một vài bữa trưa tiếp xúc với những người thành công, anh chàng Joe có những bài học ý nghĩa, và sau này anh cũng thành công khi điều hành 1 công ty lớn.
Quyển sách khá hay và ý nghĩa!
4
401314
2016-06-16 09:59:28
--------------------------
441605
9521
Tôi thích những cuốn sách nói về kinh doanh nhưng mang đậm chất nhân văn. "Người dám cho đi" thì tôi lại đặc biệt rất hứng thú, và cảm thấy rất cảm kích sau khi đọc xong, vì cảm giác như nhận đươc món quà thật giá trị.

Câu chuyện trong quyển sách thật sự ý nghĩa khi nó nuôi dưỡng cho tâm hồn chúng ta luôn nhìn nhận cuộc sống và công việc với bao điều tốt đẹp. Giúp chúng ta cư xử với nhau một cách rộng lượng với tấm lòng chân chật. "Ta cho đi vì ta thích như thế, đó không phải là chiến lược, đó là chân lý cuộc sống."
5
861653
2016-06-03 15:41:27
--------------------------
431578
9521
Cuốn sách viết dễ hiểu, bài học đúc rút ra rất rõ ràng. Hình thức khá nhỏ gọn thuận tiện mang theo khi đi du lịch. Cá nhân mình hiểu rất rõ ràng nội dung trong sách nhưng chưa ứng dụng được mấy. Mỗi một chương đều có nội dung cụ thể, lượng thông tin vừa phải nên người đọc dễ hiểu và suy ngẫm kĩ càng. Nhưng muốn thực hiện được cũng không dễ dàng, còn tuỳ thuộc vào bản thân mỗi người có bỏ được tính ích kỉ của mình hay không. Chúng ta cần sẵn sàng cho đi, sẵn sàng đặt lợi ích người khác lên trên.
5
562102
2016-05-17 17:14:10
--------------------------
428426
9521
Một cuốn sách triết lý lẽ thường tình trong cuộc sống, có cho là có nhận. Nếu dám cho đi thì sẽ được nhận lại những phần khác. Theo tôi sách rất có ích cho giới trẻ hiện nay trong cuộc sống đầy bon chen. Giúp cho thanh thiếu niên hiểu được lẽ sống mà giúp ích cho xã hội, hãy cho đi rồi sẽ nhận lại, đừng đặt lợi ích của chính bản thân lên hàng đầu mà hãy nhìn những người khác. Hãy suy nghĩ cho chính họ thì chính bạn sẽ nhận lại được nhiều hơn nữa. Mong rằng cuộc sống sẽ luôn như vậy 
4
358906
2016-05-11 10:51:55
--------------------------
415611
9521
Người dám cho đi - một quyển sách làm thay đổi suy nghĩ, thay đổi cách sống ích kỷ và chỉ biết nghĩ cho bản thân. Bạn sẽ thấy được rằng cho đi không phải là mất, mà là bạn sẽ nhận được rất nhiều. Cho đi là giúp người khác, giúp được cả bản thân mình nữa. Bạn sẽ không thất vọng khi cầm quyển sách trên tay đâu. Sách nên đọc ấy nha. Cho những ai muốn thay đổi bản thân. 
Tiki đóng gói cẩn thận, giao hàng dễ . Nói chung là mình hài lòng và sẽ tiếp tục ủng hộ nữa nha.
5
754470
2016-04-13 19:37:02
--------------------------
391806
9521
"Người giám cho đi" một cái tên tuyệt vời. Sách được viết dưới dạng một cậu chuyện nên rất dễ hiểu. Câu chuyện kể về một người đàn ông gặp khó khăn trong kinh doanh và may mắn gặp được những người thầy dạy anh những bài học vô cùng quý giá và ý nghĩa trong cuộc sống. Giám cho đi là một lối sống vô cùng tuyệt vời. Nếu bạn là một người đang cố gắng vươn tới thành công thì đây chắc chắn là một cuốn sách phải đọc. Không chỉ riêng về kinh doanh bạn có thể áp dụng những bài học từ cuốn sách này vào cuộc sống hàng ngày. Chắc chắn niềm vui và hạnh phúc sẽ đến với bạn mỗi ngày.
Chắc chắn cuốn sách này không làm bạn thất vọng. Hãy thật vui vẻ khi đọc bạn nhé.
5
204540
2016-03-05 20:50:54
--------------------------
386611
9521
Một cuốn sách nói về bí quyết cho những người kinh doanh nhưng cũng mang đầy triết lí về cuộc sống, về một cách sống đúng. Bạn sẽ không tìm ra được một điểm gây khó chịu cho bạn trong tác phẩm này. Với những triết lí mới mẻ cộng với lối dẫn dắt câu chuyện không gây nhàm chán, bạn sẽ thẩm thấu được tất cả những gì mà tác giả muốn gửi gắm. Một cuốn sách gắn liền với thực tế và bạn có thể áp dụng nó một cách hoàn hảo trong đời sống. Hãy mua nó ngay nếu bạn đang trên con đường kinh doanh hay sắp đi theo kinh doanh.
4
211114
2016-02-25 22:19:29
--------------------------
343171
9521
Sách đã truyền tải những thông điệp, những bài học quý giá để thành công trong kinh doanh và trong cuộc sống.
Cách dẫn dắt của tác giả khiến bạn cuốn vào câu chuyện và thực sự trở thành một phần trong đó. Khi đọc cuốn sách, đâu đó bạn sẽ thấy hình ảnh mình trong đó với những ích kỷ, khiếm khuyết, những lo toan, bộn bề như nhân vật...và rồi, sau khi đọc xong cuốn sách, sau tất cả những trả nghiệm ta nhận ra nhiều bài học đáng giá không chỉ trong kinh doanh và còn cả trong cuộc sống.
Cứ cho đi, cứ sống thật với bản thân ... rồi một ngày nào đó bạn sẽ thành công.
5
614561
2015-11-25 21:46:10
--------------------------
323270
9521
Tôi đã muốn đặt mua quyển này ở Tiki sau khi được cô giáo giới thiệu, nhưng hết hàng nên đã đặt mua ở nơi khác. Quyển sách mỏng này chứa đựng những nguyên lý cơ bản nhất để đạt đến một cuộc sống thành công. Tiền bạc không phải là đích đến của công việc, mà là "hậu quả" của công việc. Càng chiếm được cảm tình của mọi người thì mọi việc kinh doanh sẽ tiến hành thuận lợi, vì vậy phải dám CHO ĐI, phải phục vụ người khác trên cả họ mong đợi, phải tăng giá trị vào sản phẩm của mình.
Tuy nhiên, vì nội dung gói gọn nên có vẻ như đã bỏ qua những điều khó khăn khi tiến hành kinh doanh, những việc kinh doanh trong sách diễn ra có vẻ quá thuận lợi và nhanh chóng sẽ khiến người đọc nhầm tưởng là chỉ cần áp dụng những quy tắc này là sẽ thành công.
4
471751
2015-10-18 11:30:14
--------------------------
321689
9521
Đây là một cuốn sách đặc biệt đối với mình. Nội dung của nó làm thay đổi suy nghĩ của mình rất nhiều. Hãy sống như mình mong muốn. Những kinh nghiệm quý báu của tác giả được truyền đạt rất sâu sắc chân thực. 
Bài học từ thực tiễn, từ sự trải nghiệm và từ chính bản thân với bao hoài bão, thâm tâm đi cùng ước mơ. Không có gì khiến tuổi trẻ phải thua mình, không bão tố nào quăng quật được ý chí sắt thép nếu có sự kiên quyết bền vững.
Cuốn sách được trình bày rất logic. Chất lượng giấy phần lan, rất nhẹ khi cầm
5
720797
2015-10-14 17:09:24
--------------------------
320808
9521
Câu nói của nhà Thơ Tố Hữu đúng ra phải nên làm đề tựa cho cuốn sách này. Đây là cuốn sách thể hiện đúng với triết lý của câu nói trên. Bản thân tôi luôn tâm niệm câu nói nhưng đôi khi trong cuộc sống phải “cho” nhiều hơn “nhận” đã luôn chống chế cho sự ích kỷ của mình bằng cách “không cho nữa” vì biết có được “nhận” lại không mà “cho” nhiều thế! Thế rồi khi đọc xong cuốn sách nhỏ bé này theo đúng nghĩa đen của nó đã thực sự thay đổi tư duy của tôi về sự “cho” và “nhận”. 

Hãy cứ  cho, cho và cho! Đừng bao giờ trông mong hay thậm chí suy nghĩ về việc mình sẽ “nhận” lại được bao nhiêu vì khi sự “cho” của mình đã được ghi nhận thì phần “nhận” sẽ gấp nhiều lần hơn đến từ trong cuộc sống, từ một người khác mà mình mãi không nhận ra. Một cuốn sách thực sự nhỏ bé, không chứa đựng bất kỳ suy nghĩ nào về tôn giáo, triết lý đạo đức mà chỉ là một câu chuyện rất giản đơn thế nhưng khi gấp lại, lại cho ta một sự chiêm nghiệm rất “đời”! Đây là môt cuốn sách thực sự ý nghĩa, đã thay đổi hoàn toàn tư duy ích kỷ của tôi trước đây và thực sự làm cho tôi cảm thấy mình quảng đại và phóng khoáng hơn khi biết “cho” nhiều hơn. Hãy cứ “cho” thôi! 

5
665899
2015-10-12 13:23:45
--------------------------
312508
9521
Một câu chuyện thực sự thú vị, và những lời nhân vật nói trong sách sẽ làm bạn thật sự bất ngờ vì suy nghĩ "dám cho đi" của họ, thật tuyệt vời là sau những sự cho đi đó làm họ ngày càng thành công, ngày càng hạnh phúc và mọi người càng ngưỡng mộ tài năng của họ. Trong cuốn sách này, đâu đó bạn sẽ thấy hình ảnh của chính mình, những suy nghĩ chật hẹp của mình, và sau khi đọc xong, suy nghĩ của bạn như được bức phá, rất ngoạn mục! Không dài dòng, không hoa mỹ, không quá nhiều triết lí phức tạp. Một câu chuyện nhiều thú vị và mang đến sự dễ chịu cho những áp lực hiện tại (nếu có) của bạn.
5
581820
2015-09-21 13:58:48
--------------------------
308199
9521
Mình được một người bạn giới thiệu quyển sách này và cũng khá thích thú với những quan điểm của tác giả Bob Burg & John David Mann về sự cho đi và sự nhận lại. Ở mỗi chương tác giả luôn lồng ghép những yếu tố hài hước khiến việc đọc sách trở nên thú vị hơn rất nhiều. Đọc xong quyển sách mình cảm thấy thấm cái câu "Giá trị một con người được thể hiện qua những gì họ cho đi chứ không phải những gì họ nhận được". Về phía dịch giả mình thấy nhóm dịch lưu loát, văn phong cũng rất hay và cũng có rất nhiều sáng tạo. Chỉ có một điều hơi hơi không thích đó là bìa sách thiết kế không đẹp và cách in sách cũng không đẹp ^^!
4
615616
2015-09-18 15:46:33
--------------------------
303804
9521
Một cuốn sách khá mỏng trình bày một câu chuyện thật dài đầy trải nghiệm, triết lý và đáng giá. Đọc để bớt suy nghĩ cá nhân vị kỷ đi, đọc để sống cởi mở hơn, rộng lòng hơn, thời đại này là thời đại của những người cho đi, cho thật nhiều đừng khư khư cá nhân nữa, cho đi để nhận được nhiều hơn. Đây thực sự là một cuốn sách đang đọc cho những người quan tâm đến kinh doanh, ai bảo kinh doanh là cứ phải vụ lợi toan tính, thời đại đó đã xa rồi.
4
588698
2015-09-15 23:39:45
--------------------------
294226
9521
Một câu truyện ngắn nhưng đầy ý nghĩa và triết lý. Cuốn sách thật dễ đọc, dễ cảm nhận. Bằng một câu truyện dẫn dắt, cuốn sách đưa người đọc đến với 5 bí quyết để đạt được thành công đỉnh cao và những bài luyện tập rất đơn giản.
Tuy vậy để thực hiện được 5 bí quyết thành công tột đỉnh thật không dễ dàng. Sẽ cần đến nỗ lực luyện tập hàng ngày không chỉ trong hành động mà phải từ trong sâu thẳm tư duy và tâm hồn.
Đật thật sự là cuốn sách rất hay, rất xứng đáng để mọi người đọc và trân trọng!
5
585545
2015-09-09 09:06:18
--------------------------
290351
9521
Thông thường chúng ta luôn suy nghĩ "Mình sẽ nhận được gì nếu làm thế này, thế kia", chứ ít khi nghĩ "Mình sẽ làm được gì cho mọi người". Quyển sách này chính là một sự thức tỉnh cho lối sống tiêu cực cho hầu hết mọi người hiện nay, thông qua câu chuyện về anh bạn Joe tác giả muốn 5 quy luật của mình sẽ hướng mọi người đến suy nghĩ "cứ cho đi rồi sẽ nhận lại nhiều hơn", sau khi đọc xong mình đã áp dụng vào thực tế ngay và mọi việc diễn ra rất đúng như mong muốn. Xin cảm ơn tác giả và cảm ơn Tiki!
5
644564
2015-09-05 11:57:19
--------------------------
284830
9521
Tôi đã đọc quyển "Người dám cho đi (The go-giver)" đã 3 lần rồi, đến bây giờ đọc lại vẫn rất hay. Năm nguyên tắc trong quyển sách này không chỉ là những bài học đơn thuần về kinh doanh mà còn là những bài học rất hữu ích trong cuộc sống. Tôi đã giới thiệu quyển sách này cho rất nhiều bạn bè của tôi. Sau khi đọc xong, ai cũng thích quyển sách này. Đối với họ quyển sách là cánh cửa dẫn đến sự rộng mở về các mối quan hệ, để họ nâng cao và giữ vững các mối quan hệ mà họ đã có. Quyển sách "Người dám cho đi" là quyển sách tuyệt vời dành cho những ai muốn thành công. Thân!
5
494481
2015-08-31 12:11:52
--------------------------
281435
9521
Tôi chưa có sở thích đọc sách cho đến khi đọc quyển sách này.
Thật bất ngờ và không thể tin nổi sau khi đọc và áp dụng những điều sách đề cập đến.
Tôi đã học thuộc lòng 5 quy tắc và áp dụng vào từng việc cụ thể trong cuộc sống.
Thực ra tôi đã từng nghe về điều "cho thì có phúc hơn là nhận" trong Kinh Thánh, nhưng với cùng ý tưởng đó kèm theo những mẩu truyện thực tế đã nhấn mạnh và làm người đọc hiểu thêm và sâu hơn về những điều quan trọng về cách đối nhân xử thế để từ đó tạo ra thành công cho người khác và rồi sẽ nhận được sự thành công từ người khác đem đến cho mình!
5
764577
2015-08-28 13:36:58
--------------------------
278978
9521
"Người Dám Cho Đi" một lần nữa giúp tôi có cái nhìn thật hơn về cuộc sống. Hấp dẫn, lôi cuốn những là tính từ thích hợp hơn cả để dành cho cuốn sách này. Năm nguyên tắc ví như năm chìa khóa mở cánh cửa thành công trong cuộc sống. Đây không chỉ đơn thuần là quyển sách viết về một câu chuyện kinh doanh mà là một bài học đầy kinh nghiệm cho những ai muốn thành công. Hãy đọc lại nó ít nhất vài lần để khám phá những điều kỳ diệu ẩn chứa trong quyển sách này.
4
606077
2015-08-26 13:12:26
--------------------------
277751
9521
Cuốn sách chỉ là 1 câu chuyện của nhân vật chính sống trong cuộc sống hiện đại, phải trải qua hàng loạt suy tư, lựa chọn, đánh giá cơ hội để đạt được thành công trên con đường sự nghiệp của mình.
Cuốn sách sẽ giúp người đọc có một cái nhìn đúng đắn hơn về sự sẻ chia, sự cho đi để cùng tạo ra giá trị chung. Riêng mình, cuốn sách đã cho mình hiểu được rằng cho đi không có nghĩa là cho những thứ mình có để giúp đỡ người khác mà cho đi còn là cho người khác cơ hội được giúp đỡ mình.
Khuyến khích các bạn trẻ như mình đọc để trưởng thành hơn, để giúp đất nước Việt Nam đi xa hơn. Những anh chị, cô chú cũng nên đọc nó để có cái nhìn rộng mở hơn cho xã hội thêm đẹp.
4
115672
2015-08-25 11:15:57
--------------------------
275435
9521
ấn tượng đầu tiên với tôi có lẽ đây sẽ là một quyển sách rất khô khan, nhưng càng đọc tôi lại càng bị lôi cuốn. Câu chuyện về ý tưởng kinh doanh của Joe đã đem đến cho mình bài học giá trị của cuộc sống về quy luật cho - nhận. Đọc để cảm nhận và hiểu hơn về những trắc trở trong cuộc sống. Tôi rất hài lòng, cảm ơn Tiki.
“Nhiều người chỉ cười khi họ nghe rằng bí quyết của thành công là cho đi… Rồi, lại một lần nữa, nhiều người sẽ không tới gần được cái thành công mà họ hằng ao ước.”


5
383983
2015-08-22 21:45:47
--------------------------
267680
9521
Quyển sách của mình được bọc plastic khá đẹp mắt. Đây là một cuốn sách mà mình muốn mua từ lâu nên đã nhanh tay đặt ngay trên tiki. Nội dung cuốn sách là một câu chuyện thú vị trong cuộc sống. Câu chuyện về ý tưởng kinh doanh của Joe đã đem đến cho mình bài học giá trị của cuộc sống về quy luật cho- nhận. Không cho đi bao giờ thì sao nhận lại được. Cuốn sách giúp ta nhìn nhận lại những gì mình đã trải qua.    Nó còn giúp mỗi người có suy nghĩ đúng đắn khi tạo dựng các mối quan hệ.
5
691865
2015-08-15 19:46:18
--------------------------
267087
9521
Người Dám Cho Đi là một món quà tuyệt vời từ Pindar - Ngài chủ tịch gửi đến cho những độc giả, những Vị khách của thứ Sáu. Qua câu chuyện mạch lạc đầy cuốn hút, từng triết lí về thành công hiện ra hết sức bất ngờ mang đầy sự thú vị. Cho đi không chỉ mang đến thành công trong công việc mà còn trong cuộc sống, với gia đình, bạn bè và những người ta quen biết. Với quy luật Năm Giá trị mà tác giả chia sẻ, bạn sẽ không khỏi ngưỡng mộ và cảm kích những gì họ đã dành tặng cho bạn khi đọc cuốn sách này. 
5
179695
2015-08-15 10:41:31
--------------------------
261055
9521
Lúc đầu cầm quyển sách tôi tưởng đây chỉ là một cuốn sách triết lý khô khan, nhạt nhẽo, nhưng khi chỉ mới đọc những trang đầu tiên, tôi biết là tôi là sai lầm. Chứa đựng trong sách là một câu chuyện hấp dẫn, một tuần học đổi đời đối với một nhân viên văn phòng chỉ cố gắng dành lấy mọi thứ. Không chỉ là bài học của nhân viên ấy mà còn là bài học cho bất kì ai đã và đang đọc Người dám cho đi, cuốn sách cho ta hiểu một điều, khi ta cho đi càng nhiều thì ta sẽ nhận được càng nhiều và chắc chắn nhiều hơn những gì mình đã cho đi.
5
194412
2015-08-10 20:51:56
--------------------------
256403
9521
Từ sau khi đọc quyển sách, tôi chú ý đến hành động của mình hơn, chú ý đến cảm xúc của người khác hơn. Những điều mà trước đây tôi cho là trái ngược thì giờ đây tôi vui vẻ thực hành, và đúng là tôi nhận lại được niềm vui, thật là bất ngờ!
Điểm cộng nữa là bìa sách in đẹp, giấy dày, chữ rõ, khiến tôi đọc 1 mạch ko thấy chán.
Tuy lần này đặt hàng tôi nhận được hơi lâu vì trục trặc bên phía bưu điện nhưng dù sao tôi cũng rất hài lòng.
Cám ơn tiki!
4
682767
2015-08-06 20:45:07
--------------------------
249650
9521
Quyển sách này không nói lý thuyết suông, mà thông qua câu chuyện, nhân vật chính dần dần hiểu ra những giá trị cần có trong cuộc sống, thấy rõ sự vận động của các giá trị ấy tác động ra sao tới cuộc sống của mình; và cùng lúc ấy, người đọc cũng nhận thấy điều ấy. Sau khi đọc quyển sách này, mình thấy mình trở nên rộng rãi hơn trước, sẵn sàng cho đi/ giúp đỡ bạn bè trong khả năng giới hạn của mình, không phải vì mình muốn được nhận lại, mà chỉ vì "tử tế với người khác thì chẳng mất mát gì cả".
4
650466
2015-07-31 22:49:35
--------------------------
243076
9521
Giá trị của bạn được quyết định bằng việc bạn cho đi chứ không phải nhận lại. Những người thường có ít hơn lại cho đi nhiều hơn.

Sau khi đọc xong quyển sách này, mình đã mua không dưới 10 quyển để tặng cho bạn bè và người thân, thậm chí còn dùng nó làm quà tặng trao giải thưởng cho các hoạt động của CLB nữa.

Cuộc sống của chúng ta sẽ thay đổi khi chúng ta biết quan tâm, dành tình cảm vô vị lợi cho những người xung quanh mình, đây thực sự là một cuốn sách đáng đọc cho bất kì ai.
5
21466
2015-07-27 10:15:47
--------------------------
221504
9521
"Người dám cho đi" đã thay đổi suy nghĩ của tôi về nhiều vấn đề trong cuộc sống. Thật sự quyển sách rất cuốn hút khiến tôi đã đọc một mạch từ đầu đến cuối.
5 nguyên tắc được đưa ra trong quyển sách này hoàn toàn trái ngược với những gì mà tôi được học và được "dạy" ngoài đời từ những người khác.
Sau khi đọc lại quyển sách này thêm vài lần nữa, mỗi lần một chương, tôi đã thật sự nhìn thấy được tại sao lại có sự trái ngược như vậy. Tôi nhận ra hầu hết những người, những mô hình kinh doanh thành công thật sự hiện nay đều dựa trên những nguyên tắc cốt lõi này. Làm tốt hơn những gì người khác mong đợi, tạo ra nhiều giá trị hơn, những điều mà trước đây luôn hiện hữu trước mắt nhưng tôi đã không nhìn thấy, vì những người thật sự thành công thì quá ít so với những người "thành công ở lưng chừng".
Đây không chỉ đơn thuần là quyển sách viết về một câu chuyện kinh doanh, hay nói về chủ đề kinh doanh. Những nguyên tắc được đề cập đến ở đây có thể áp dụng trong nhiều mặt của cuộc sống hằng ngày, thậm chí có thể cải thiện các mối quan hệ của bạn. Hãy vận dụng nó một cách linh hoạt.
Nếu có thể, bạn nên đọc lại quyển sách này định kỳ. Mỗi lần đọc lại có thể bạn sẽ nhận ra thêm một điều gì đó hữu ích, giống như nó đã xảy ra với tôi vậy!
4
541710
2015-07-03 22:49:41
--------------------------
218750
9521
Được lòng ghép vào một câu truyện cuốn hút với bài học trong mỗi tình huống , Người Dám Cho Đi là quyển sách về kỹ năng hữu ích không kém những quyển Đắc nhân tâm hay Đừng bao giờ đi ăn một mình .
Hãy phục vụ nhiều người nhất có thể , bạn sẽ nhận được cả thể giới . Bạn hãy nghĩ khi làm một việc cho ai đó bạn sẽ đạt được điều mình muốn thì tư duy của bạn sẽ mở ra theo hướng cho đi nhiều hơn .Và cuối cùng là quy luật nhận lại những gì mình đo cho đi .
Với mình đây là một quyển sách rất hay và mọi người nên đọc nó .

5
554150
2015-06-30 21:02:01
--------------------------
214616
9521
Chỉ muốn nói là mình cực kết cái tên sách cũng như cái bìa.. hihi ^^
The Go- giver - Người dám cho đi ♥ ..
ko chỉ trong kinh doanh, ngoài xã hội cũng vậy, người biết cho đi, dám cho đi luôn là người hạnh phúc nhất ^^
Giống như mấy lần đi về chùa làm công quả, giống như sinh viên đi tình nguyện vậy.. cảm giác giúp đc người khác, làm việc có ích thực sự tuyệt vời
haizzzzzz .. cố gắng học thật tốt, có cuộc sống khá khá một chút.. để có thể giúp được cho nhiều người khác.. ước nguyện lớn nhất là cả đời được đi đây đi đó, dành cả đời làm việc thiện nguyện như trong quyển sách này ♥
4
357488
2015-06-25 10:55:28
--------------------------
213601
9521
Tôi đã đọc nhiều sách và nghe nhiều về triết lý "cho đi" và "phụng sự" như nội dung cuốn sách này đưa ra. Nhưng cuốn sách vẫn tạo cho tôi một cảm giác cực kỳ lôi cuốn và thú vị. Cách dẫn dắt câu chuyện của tác giả rất sâu sắc và khơi gợi niềm say mê. Tôi nghĩ mỗi người đều sẽ tìm được một điều gì đó cho mình từ cuốn sách này để ứng dụng vào cuộc sống, vào công việc của mình. Và có thể, sẽ tìm được con đường đi đến thành công cho mình như nhân vật Joe trong câu chuyện.
5
192329
2015-06-23 22:27:32
--------------------------
210489
9521
Tôi rất thích cuốn sách này, một câu chuyện nhỏ nhưng lại chứa đựng ý nghĩa lớn về sự cho đi và những triết lý kinh doanh thành công, những quy luật trong kinh doanh được gói gọn rất đơn giản nhưng rất sâu sắc, và điều rất hay là tất cả những áp dụng quy luật trong kinh doanh cũng có thể áp dụng cho tất cả các vấn đề của cuộc sống. Người giàu và thành công không phải là người chỉ có nhiều tiền, tài sản mà là người có thể cho đi thật nhiều giá trị trong cuộc sống. Và để học được nhiều điều thì dù ta có đang đứng ở vị trí địa vị nào, ta phải luôn biết làm mình trống rỗng để thu nạp kiến thức, biết hạ cái tôi xuống để đón nhận kiến thức của nhân loại. 
Cốt truyện hay, thu hút, giọng văn dễ hiểu và ý nghĩa sâu sắc.
5
315472
2015-06-19 15:54:18
--------------------------
207132
9521
Công bằng mà nói thì triết lý "cho là nhận" như trong sách không phải lần đầu được xây dựng nên. Tuy nhiên với cách trình bày nó ứng dụng trong kinh doanh như thế nào thì có lẽ sách này là quyển đầu tiên mình biết =)) (nếu không kể đến thực tế kinh doanh theo kiểu cộng đồng như ở Nhật Bản). Câu văn dễ hiểu, súc tích, đôi chỗ thú vị và hấp dẫn, có lẽ là theo kiểu thường thấy ở các loại sách khác cùng loại như vầy - mình cho là loại phát triển bản thân. Cố truyện hay, sâu sắc và ý nghĩa, trình bày đẹp, giấy tốt, bìa ấn tượng.
4
564333
2015-06-11 15:41:03
--------------------------
186366
9521
Kinh doanh- thường xuyên gắn liền với suy nghĩ lấy được bao nhiêu, how much you will gain, how many you will get, đặt bản thân-lợi nhuận của mình lên làm mục tiêu. Tuy nhiên quyển sách này thay đổi 180o suy nghĩ ấy. 
Cốt lõi của quyển sách này là thay đổi nhìn nhận về kinh doanh; không phải đặt lợi ích bản thân lên hàng đầu, mà là sẵn sàng cho đi, sẵn sàng đặt nguyện vọng của người khác lên trên mình, luôn làm tăng giá trị cho người khác- và như vậy, vô tình càng làm cho mình trở nên sung túc hơn bao giờ hết. 
Triết lý lạ, dẫn dắt thú vị, có tính ứng dụng cao, 4sao!
4
196374
2015-04-20 23:34:35
--------------------------
170939
9521
Trong xã hội chúng ta có rất nhiều người dám giành lấy nhưng dám giành thì nhiều nhưng giành được thì lại ít. Những người dám cho đi thì lại không như thế, họ không giành giật với ai nhưng những gì họ có được thì lại khiến nhiều người phải ước ao. Cuốn sách này khiến mình thực sự mở mắt.  Cuốn sách nói thế này: quên cái tư duy cùng thắng đi, hãy đặt lợi ích của người khác lên trên lợi ích của bản thân, hãy sống thật, nếu kinh doanh thứ bạn bán đó chính là bản thân bạn... Những quy luật được đưa ra rất hay và có cả cách áp dụng không chỉ sử dụng trong công việc mà còn áp dụng trong cuộc sống gia đình. Có điều thành công của Joe ở cuối sách, mình thấy phần nhiều là do may mắn đem lại hơn là do áp dụng các quy luật . Các quy luật rất hay nhưng liệu chúng ta có đủ can đảm cho đi?
4
491545
2015-03-20 22:57:29
--------------------------
168131
9521
Người dám cho đi - Một cuốn sách dành cho những ai đang muốn gặt hái được thành công trong sự nghiệp và cuộc sống. Câu chuyện trong sách giúp người đọc dễ hình dung và dễ nhớ được các quy luật đem đến sự thành công. Đúng với tinh thần của một người dám cho đi, câu chuyện của từng nhân vật trong sách cũng là cách mà họ cho đi qua việc họ chia sẽ với mọi người cách giúp họ thành công trong cuộc sống. Đọc xong nó có lẽ sẽ giúp ta hiểu hơn "Vì sao những người thành công giàu có thật sự thì họ lại càng thành công hơn?". Hãy đọc nó, bạn sẽ biết được câu trả lời và bạn sẽ biết mình nên làm gì để thành công, để hạnh phúc trong sự nghiệp cũng như trong cuộc sống.
5
413460
2015-03-16 00:08:02
--------------------------
136474
9521
Người ta bảo rằng nếu bạn muốn giàu có thì có rất nhiều cách, song chung quy lại chỉ gói gọn trong ba điều: mỉm cười, cho đi và tha thứ. Trong cuốn sách này tác giả đã mượn câu chuyện của một chàng trai tên Joe đầy tham vọng và luôn muốn thành công nhưng cậu chỉ đi theo lối mòn mà những người khác thường hay làm. Từ một cuộc gặp gỡ mang tính định mệnh với Pindar, một người rất thành công và được nhiều người biết đến bởi nhân cách cao quý của mình đó là dám sẵn sàng truyền đạt tất cả bí quyết thành công của mình cho người khác. Và cuộc đời của Joe, từ cuộc gặp gỡ này, đã bước sang một trang mới với thành công trải rộng phía trước. Quyển sách đã làm sáng tỏ hơn ý nghĩa của sự cho đi trong cuộc sống này. Và mình nghĩ rằng, cho dù bạn có là người kinh doanh hay không thì cũng hãy đọc quyển này bởi nó truyền đạt một thông điệp sống rất ý nghĩa. Đọc để hiểu hơn về ý nghĩa của sự cho đi và 5 bí quyết thành công của một người kinh doanh là như thế nào trong xã hội hiện đại ngày nay.
4
274995
2014-11-21 14:37:28
--------------------------
114727
9521
Cũng như câu nói "bỏ con tép để bắt con tôm", muốn có được một "con tôm" thì trước đó bạn phải hy sinh một "con tép". Nội dung trong quyển sách này cũng vậy, MUỐN ĐƯỢC NHẬN LẤY THÌ BẠN PHẢI CHO ĐI, nhưng "cho" như thế nào thì đó là cả một nghệ thuật, một "bí quyết" mà theo như Pindar (nhân vật trong quyển sách đã nhận ra và chia sẻ với mọi người) thì đó là "Bí quyết kinh doanh". Nhưng theo tôi, nó còn hơn thế, những bí quyết đó không chỉ đem đến cho ta thành công trong công việc mà còn giúp ta có được những mối quan hệ tốt đẹp hơn trong cuộc sống, và chính những mối quan hệ tốt đẹp đó sẽ hỗ trợ cho công việc của chúng ta. 
Nội dung quyển sách được viết theo lối kể chuyện, một câu chuyện xuyên suốt từ đầu đến cuối làm ta không cảm thấy nhàm chán. Nhưng hãy xem quyển sách này như một quyển "bí kiếp" và thực hành theo nó chứ đừng coi nó như một quyển truyện thông thường,  tôi tin bạn cũng sẽ đạt được thành công như chàng Joe trong câu chuyện.
5
33144
2014-06-17 16:51:35
--------------------------
52529
9521
Tôi đã đọc đi đọc lại cuốn sách này rất nhiều lần và hiểu ra là một trong những cách giúp bạn đi đến thành công chính là "dám cho đi". Trong truyện, Joe đã có những trải nghiệm thú vị về những chia sẻ từ những người bạn thành công khác của Pindar. Cách kết thúc truyện rất khéo léo, Joe đã trở thành một trong số những người thành công ấy và anh đã chia sẻ kinh nghiệm của mình,  chính là "cho đi", Joe áp dụng những điều đã học được từ Pindar & những người bạn với mong muốn giúp đỡ những người đang gặp bế tắc trong công việc có thể vươn lên và gặt hái được thành công. Đây là cuốn sách nên đọc và có thể là sách "gối đầu giường" cho những ai khao khát gặt hái thành công trong sự nghiệp cũng như trong cuộc sống.
5
46111
2012-12-27 13:38:05
--------------------------
