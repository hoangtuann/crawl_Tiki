297767
3431
Là con gái, mình rất ngưỡng mộ các nữ doanh nhân thành công trong cuộc sống và công việc nên mình hay quan tâm đến các cuốn sách được viết bởi các chị , các cô đi trước, Thứ 2 là mình từng được biết cô Nhan Húc Quân qua cuộc thi "Tài năng Lương Văn Can" nên mình rất hào hứng khi mua cuốn sách của cô. Cuốn sách kể về các giai đoạn, những khó khăn trong cuộc sống của chính bản thân cô. Tuy nhiên, các câu chuyện trong sách lan man, tủn mủn, chưa sâu sắc lắm. Nhiều câu chuyện không mang lại bài học gì cả. Nên cảm nhận để lại cho người đọc chưa sâu. Cách sắp xếp các câu chuyện cũng không hài hòa.
1
294072
2015-09-12 09:11:16
--------------------------
289797
3431
Mình mua sách này vì tò mò muốn biết cái gì là "chính mình". Sách nhẹ cầm vừa tay rất thoải mái,bìa đơn giản in chữ nổi làm mình rất vừa ý khi nhận được. Nhưng nội dung khiến mình khá thất vọng vì bố cục các mẩu truyện ngắn lung tung. Bên cạnh một vài mẩu tự sự hay và có ý nghĩa, có nhiều mẩu chuyện cụt ngủn làm mình "tụt hết cảm xúc", song song là cách trình bày của tác giả dài dòng và có nhiều chỗ ghi tắt, kí hiệu nhưng không giải thích làm mình không thể nắm bắt được ý đồ của tác giả!
2
665690
2015-09-04 21:39:12
--------------------------
257934
3431
Quyển sách tập trung những câu chuyện ngắn nhưng rất cảm xúc và khiến cho người đọc phải suy nghĩ nhiều. Cầm cuốn sách lên, điều đầu tiên mình rất khâm phục tính cách của bà Nhan Húc Quân, một nữ doanh nhân rất thành đạt. Nhờ có chí lớn, luôn hướng đến một tương lai tốt đẹp hơn, lúc nào cũng có trách nhiệm với bản thân mình và những người xung quanh mà bà đã vươn ra được biển lớn. Đó là kết quả của những ngày chịu đựng gian khổ trong cuộc sống, chắt chiu từng đồng để có tiền mua một chiếc xe đạp đến cách dạy con, cách đối nhân xử thế rất chân thành và bên cạnh đó bà còn là một người mẹ mẫu mực nữa. Cách sống đó rất đáng để chúng ta ngưỡng mộ và học hỏi nhưng bên cạnh đó, những câu chuyện trong quyển sách ngắn quá, tuy có thể giúp người đọc cảm nhận được những thông điệp mà tác giả muốn trao gửi nhưng nó quả thực chưa đủ độ sâu và bề dày để gửi gắm trọn vẹn hơn điều tác giả muốn nói nhưng nhìn chung, đây vẫn là một quyển sách đáng được đọc.
4
274995
2015-08-08 08:44:26
--------------------------
246636
3431
Cuốn sách là những mẩu chuyện nhỏ ý nghĩa trong cuộc sống thường ngày, những lời khuyên và những kinh nghiệm phong phú đưa tới những ý nghĩa mà con người muốn tìm thấy trong cuộc đời. Đi vào thế giới của những câu chuyện ấy, con người cảm thấy cuộc sống có ý nghĩa hơn và tìm ra lỗi thoát trong cuộc sống đầy khó khăn.Mỗi người chúng ta hãy đọc và cùng suy ngẫm để tin rằng tình người luôn tồn tại trên thế giới này. Cuốn sách giúp mình biết trân trọng những giá trị sống nhỏ bé mà quan trong trong cuộc sống bộn bề hôm nay để thêm tự tin đương đầu với khó khăn thử thách, để hoàn thiện bản thân và sống cuộc dời đẹp hơ, có ý nghĩa hơn. Đây là một cuốn sách rất đáng để dành thời gian đọc và suy ngẫm !
5
485605
2015-07-29 18:20:15
--------------------------
