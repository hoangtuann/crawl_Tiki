379777
5063
Mình không thích tập này nhất trong truyện Connan, nếu không nói đến hơi nhàm một chút. Đọc xong cuốn truyện lại không đọng được chút gì hết. Hay nhất có thể là vụ lấy oán báo ân của ông vận động viên, đơn giản nhưng mình thấy ý nghĩa, lần này ông Mori cũng giỏi hơn trước, Connan chỉ cần gợi ý một chút là ông ấy hiểu ra ngay. Mình vẫn cho 4 sao vì nội dung cũng không đến nỗi quá chán, so với những tập trước, tập này hơi bị “lép” một chút nhưng vẫn ok. Hy vọng những tập sau tác giả sẽ đầu tư hơn về nội dung để câu chuyện càng thêm hấp dẫn.
4
13723
2016-02-12 16:40:09
--------------------------
377137
5063
Conan còn nhỏ mà biết rõ về cá và câu cá cùng các thuật ngữ chuyên ngành cá quả là thánh cá rồi. 
Vụ án đó là trái tim của tập này nếu xét về mặt thông tin. Chứ cách thức gây án cũng bình thường thôi. Vì các thông tin rất thú vị và bổ ích. 
Tập nào có bọn con nít là dở ẹc, ngoại trừ Ai cá tính, mấy nhóc kia phiền quá. Mình thích xem Hattori thôi. 
Nói chung truyện này dành cho thiếu nhi, người lớn nên đọc của Agatha, lôi cuốn, bí ẩn và ngạc nhiên ở phút cuối cùng. 
Conan cũng từng mượn nhân vật Marple và Poirot của Agatha trong một tập truyện. Cứ như là tác giả đọc hết truyện trinh thám trên thế giới rồi mới viết Conan nên ở Conan cái gì cũng có. 
5
535536
2016-02-01 18:25:26
--------------------------
362351
5063
Conan tập 45 thật là hay! Có ai nghĩ rằng đi đường vòng lại nhanh hơn so với đi đường tắt nhưng đông? Conan đem đến cho người đọc hết bất ngờ này đến bất ngờ khác. Bằng chứng ngoại phạm tưởng chừng như hoàn hảo đã bị Conan thông minh tìm ra lỗ hổng cùng với suy luận logic và đưa ra kết luận vạch trần tội ác của tên sát nhân. Bên cạnh đó, Mori còn rất dễ thương khi chuẩn bị trước cho Ran ( thực chất là cho Kisaki) một quả bóng tennis có chữ kí của một ngôi sao... bóng chày trước khi ông này chết. Thật quá hay và dễ thương.
3
362041
2016-01-01 20:35:26
--------------------------
342824
5063
Sản phẩm Thám Tử Lừng Danh Conan 45 ( Tái Bản 2014 ) thật là chất lượng và hay . Miếng bìa không bị trầy xước , nhẵn bóng , lán mịn , giấy thì chẳng kém gì bìa cả , giấy in xốp , dễ nhìn , đẹp ... cắt giấy vừa đủ ngang với tấm bìa . Cốt truyện thì cũng chẳng kém gì giấy và bìa ... tập truyện có đủ khoảng 10 file , file nào cũng gay cấn và hấp dẫn cho người đọc . Truyện này cũng hơi nhiều file nên mình đọc chẳng xuể tí nào , không chán!
5
603156
2015-11-25 09:07:11
--------------------------
299518
5063
dù Conan 45 này không phải là tập có nhiều vụ án gay cấn và khiến Conan phải đau đầu nhất nhưng cuốn truyện này cũng có những vụ án khá hay.Ngay từ đầu là vụ án về đầu độc qua thủ thuật "kết dây" trong câu cá, rồi sau đó là vụ án Nose Toshizo -là nạn nhân.Mà thủ phạm lại có một bằng chứng ngoại phạm tuyệt vời .Nhờ sự suy luận tài tình thì không có gì khó đối với Conan có thể phá được vụ án mà đi bằng đường vòng để rút ngắn thời gian .Hay ^^
4
416933
2015-09-13 12:21:46
--------------------------
289904
5063
Những vụ án trong tập này cũng không có gì rắc rối để suy đoán nhiều vì đa phần nhìn là biết thủ phạm là kẻ nào rồi, vấn đề là hung thủ có chứng cớ ngoại phạm hoàn hảo nên vấn đề là chờ xem Conan tìm ra mánh khóe để vạch trần hung thủ thôi. Lần này vụ án nhóm thám tử nhí đi câu cá khá hay, kết thúc lại có hậu nữa nên thích lắm. ĐOạn cuối khi cùng mọi người trở về, nhìn Haibara cười khi cảm nhận được tình cảm của bạn bè xung quanh mà thấy xúc động ghê. 
4
471112
2015-09-04 22:44:41
--------------------------
184322
5063
Tập này với mình cũng khá hay, nhất là phần vụ án khi tiến sĩ Agasa và nhóm thám tử nhí đi câu cá. Thật cảm động khi bọn trẻ lớp 1B còn nhỏ mà đã biết nhớ đến ngày sinh nhật của tiến sĩ Agasa và cùng quyết tâm câu được thật nhiều cá để làm quà cho tiến sĩ. Không gian xung quanh như vỡ òa khi Genta sau một hồi vất vả đã câu được một loại cá ngon. Ngoài ra, trong tập này, mình cũng rất ấn tượng với câu nói của Conan đã làm thay đổi suy nghĩ tiêu cực của Haibara, khiến cô trở nên vui vẻ hơn.
4
558345
2015-04-17 15:59:54
--------------------------
