467596
12296
Cốt truyện u ám, đầy vẻ quaí dị đã cuốn hút người đọc từ những trang đầu tiên. Có thể xem, bộ liên hoàn 7 viên bi + tù nhân thần Mặt Trời là hay nhất trong toàn bộ tác phẩm về Tin tin.
Đã mê Tintin từ lúc nhỏ. Nay được tái bản, có bản quyền, thì quyết mua cho đủ bộ. Bộ truyện Tintin này có khoảng 21 - 24 cuốn, không biết những quyển như Tintin ở Congo, hay Tintin ở Xô Viết, Tintin và hồ cá mập ... có được xuất bản hay không ?!
Quyển truyện màu sắc đẹp, rực rỡ, nhưng lời dịch đọc khá khô cứng. Nhưng ít ra người dịch cũng không đến nỗi dịch tên riêng thành Hát-đốc, Tuốc-nơ-son là mình mừng lắm rồi.
Trí Việt đã xuất bản được 10 quyển, như vậy là cũng xem như đã đi được nửa chặng đường. Hy vọng mọi người cũng sẽ được đọc bộ truyện cực hay này.
5
434482
2016-07-04 01:08:16
--------------------------
395799
12296
The Adventure of Tintin - Những cuộc phiêu lưu của Tintin có lẽ đã quá quen thuộc với lứa tuổi 8x, đầu 9x. Tuy là một bộ truyện tranh cũ của phương Tây, nhưng câu chuyện của Tintin đối với tôi vẫn chưa bao giờ là lỗi thời. Hấp dẫn, lôi cuốn, thú vị, dễ thương đó là tất cả những gì tôi cảm nhận được từ Tintin. Nếu bạn là người chuộng Manga chưa từng đọc một tác phẩm nào của Comic trước đây thì có lẽ bạn nên khởi đầu với Tintin, có thể nó sẽ khiến bạn yêu thích Comic đấy.
5
677589
2016-03-12 14:59:11
--------------------------
