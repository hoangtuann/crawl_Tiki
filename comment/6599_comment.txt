448689
6599
Tuy chưa đọc song tác phẩm,nhưng nghe thấy tên của tác phẩm thôi, tôi đã say như say thuốc rồi.Tác phẩm cho ta biết được một nền văn hóa sâu sắc của việt nam được phát triển qua từng thời kì khác nhau.Qua đó tác phẩm cho ta thấy cụ đào duy anh là một người có kiến thức sử học,văn hóa rất uyên bác và thâm hậu.Một mình cụ đã bỏ công sức và thời gian để nghiên cứu và viết nên một tác phẩm tuyệt vời về nên văn hóa nước nhà để cho con cháu đời sau có thêm nguồn tài liệu để học tập.
5
768145
2016-06-16 13:23:25
--------------------------
417495
6599
Mình thật sự rất bất ngờ vì nhìn vào bìa sách lại là Nhã Nam xuất bản. Từ trước đến giờ mình thấy Nhã Nam chỉ xuất bản sách văn học thôi, nhất là văn học nước ngoài! Bởi vậy mình rất háo hức muốn biết quyển sách này sẽ như thế nào, và mình không phải thất vọng. Hình thức đẹp, nhưng mình muốn nói đến nội dung bên trong. Đây là quyển sách mà mọi người Việt Nam nên có trong nhà, để biết được lịch sử văn hóa của dân tộc ta. Nó cho ta biết dân ta từ cái ăn cái mặc, nếp sống lối ở...Không thể không mua!
5
230629
2016-04-17 07:05:48
--------------------------
364935
6599
Nội dung sách này khá hay và lý thú. Tôi nhận thấy những hình ảnh trích dẫn mang đậm phong cách nghệ thuật vùng Đông Á và Đông Nam Á, quen thuộc và gẫn gũi. Sách có cách trình bày khá hợp lý khi gắn kết văn hóa và lịch sử. 
Học giả Đào Duy Anh viết tác phẩm này khá sớm, trước thập niên 40 của thế kỷ trước nên tư tưởng của tác phẩm còn khá gần gũi, chưa bị nhuốm màu sắc hiện đại. Đây là một tư liệu quý cho những ai muốn tìm hiểu văn hóa nước Việt, 4000 ngàn năm văn hiến.
4
953133
2016-01-07 07:49:52
--------------------------
342507
6599
Những năm đầu thế kỷ 20, với sự giao thoa của 2 nền giáo dục Khổng giáo và Phương Tây, nước ta đã sinh ra một thế hệ trí thức trẻ trung, giàu sức sống. Những con người như Võ Nguyên Giáp, Nam Cao, Đào Duy Anh...bằng những đóng góp trong lĩnh vực của mình, đã tạo dựng nên những cơ sở cho một đất nước Việt Nam hiện đại. Và nói một cách công bằng, cho đến tận ngày nay, vẫn chưa có được những người thừa kế xứng đàng

Đào Duy Anh viết Việt Nam Văn Hóa sử cương khi ông ở tuối 30. Khiêm tốn nhưng không hề tự ti, đúng như tên hiệu Vệ Thạch của mình, ông đã cần mẫn tích góp những viên đá để dần dần lấp biển học bao la không bờ bến.

Gấp lại những trang cuối cùng của cuốn sách, dù có rất nhiều vấn đề gây tranh cãi, nhưng chúng ta cũng kịp nhận ra hình ảnh của một thanh niên chín chắn và thông tuệ. Với một vốn kiến thức toàn diện và thang giá trị sống rất rõ ràng về mọi mặt của xã hội. Vì thế, cuốn sách không những mang lại chúng ta một cái nhìn chân thực hơn về đời sống của ông cha, nó còn mang lại cho chúng ta nhiều bài học về tư duy khoa học trong việc nhìn nhận thế giới xung quanh.

Ngoài ra, mình rất hài lòng với chất lượng in ấn và trình bày của Nhã Nam. Chi phí cuốn sách cũng không hề nhiều so với nội dung rộng lớn mà nó mang lại. Một cuốn sách sẽ có giá trị lâu bền trong rất nhiều năm nữa. 
4
532620
2015-11-24 16:43:59
--------------------------
310069
6599
Một cuốn sách gần trăm năm nhưng vẫn còn giữ nhưng giá trị của nó, một tập hợp công phu, bải bàn và thật đầy đủ của cụ Đào Duy Anh về văn hóa Việt Nam. Lúc cụ viết cuốn sách với mong muốn bảo tồn những nét văn hóa dân tộc trước cuộc xâm lược của thực dân thì giờ đây đất nước độc lập nhưng hội nhập sâu rộng tiềm tàng nhiều nguy cơ về việc gìn giữ những giá trị ngàn đời của dân tộc. Tôi đọc và mong là sách sẽ đến được với nhiều người trẻ để ta có hội nhập thì cũng không bị đồng hóa
5
252046
2015-09-19 11:59:38
--------------------------
286349
6599
cuốn sách khá đẹp và dày, giấy tốt, có bao ngoài nhìn rất thích. Về nội dung, đây là một cuốn sách rất bổ ích về lịch sử văn hóa và tinh thần của nhân dân ta, được tác giả dày công nghiên cứu. Từ những việc rất nhỏ trong sinh hoạt hàng ngày cho đến những phong tục tập quán độc đáo cũng được mô tả và giải thích tỉ mỉ, với rất nhiều hình ảnh cụ thể giúp người đọc rất dễ hình dung. "Dân ta phải biết sử ta", đây là một tài liệu rất hay cho những ai hừng thú trong việc tìm về cội nguồn dân tộc
4
366416
2015-09-01 17:23:55
--------------------------
273588
6599
Quyển sách dày với nhiều kiến thức bổ ích về Việt Nam qua các thời đại, thời kỳ lịch sử, những nét văn hóa truyền thống của cha ông ta ngày trước, những phẩm chất, tính cách của người Việt thời xưa để đúc kết, so sánh và học hỏi để tiếp tục giữ vững và phát huy những truyền thống tốt đẹp, xây dựng Việt Nam ngày càng giàu đẹp, văn minh. Với nhiều hình ảnh minh họa sinh động càng làm tăng thêm giá trị của quyển sách. Quyển sách rất bổ ích. Cám ơn tiki đã chia sẻ.
5
110777
2015-08-21 09:24:14
--------------------------
263172
6599
Vì quyển sách viết về lịch sử của cả một đất nước nên nó gần như chỉ phân tích được những mặt nổi và nói chung chung chứ đưa đưa đến cho người đọc một cái nhìn toàn diện và sâu sắc về từng vấn đề. Nhưng với thời gian nghiên cứu như vậy và độ dày như vậy thì nội dung như vạy có lẽ là đạt yêu cầu rồi. Dù gì thì tên quyển sách cũng là "Sử cương" mà :)). Nếu bạn muốn có một cái nhìn bao quát về lịch sử Việt Nam thì bạn nên tìm đọc nó, sách còn có cả hình ảnh minh họa thực tế cho người đọc dễ hình dung nữa. Hình thức trình bày khá đẹp. 
Đánh giá chung là ổn đấy.
4
464538
2015-08-12 13:32:10
--------------------------
259853
6599
Nếu không tìm hiểu quá khứ, tìm hiểu tiến trình lịch sử và sự hình thành trầm tích văn hóa của dân tộc, ta khó có thể lý giải nguyên nhân, nguồn gốc và biểu hiện của nhiều hiện tượng văn hóa trong xã hội đương đại. Với Sử cương, cụ Đào Duy Anh đã khái quát cho người đọc những nét chung nhất về văn hóa Việt trên các mặt phong tục, con người, nếp sống, trang phục, lễ hội.. giúp ta có cái nhìn tổng quan về truyền thống văn hóa của ông cha ta ngày trước, từ đó có cái nhìn so sánh về những chuyển biến của phong tục và văn hóa trong bối cảnh toàn cầu hóa và giao lưu văn hóa hiện đại. Mặt khác, ngay từ những năm đầu thế kỷ 20, cụ Đào đã đúc kết được những phẩm chất, tính cách chung nhất của người Việt, trong đó có tốt, có xấu. Là những độc giả cấp tiến, chúng ta qua đó soi rọi lại mình, phát huy mặt mạnh, khắc phục điểm yếu, cùng nhau xây dựng hình ảnh người Việt mới, hiện đại, và văn minh hơn. ** Sách in đẹp và trang trọng, có hình ảnh minh họa sinh động, không thể thiếu trong bộ sưu tập của người thích tìm hiểu văn hóa nước nhà.
4
392686
2015-08-09 21:42:46
--------------------------
231137
6599
Khi cầm quyển sách trên tay, tôi không biết quyển sách này có khác gì Trong cõi hay không không, hay lặp lại như một bản sao? Tôi vô cùng băn khoăn! Nhưng khi đọc xong, buông quyển sách xong, tôi hết sức hài lòng và thỏa mãn khi được tiếp cận với những nét văn hóa đặc sắc, những điều ngỡ như bình dị nhưng  lại mâng những nét đẹp văn hóa truyền thống văn hóa dân tộc, đi sâu vào tiềm thức của người dân đất Việt! Tôi như được biết đến những nét văn hóa mới, những nét sinh hoạt của nhiều dân tộc, địa phương...
Dưới góc nhìn của một chuyên gia, văn hóa dân tộc hiện lên từ những điều nhỏ nhất, từ những nét rất độc đáo, đa dạng, nhiều chiều, khiến cho tôi hết sức thú vị khi đọc quyển sách này!
4
616731
2015-07-17 21:59:25
--------------------------
158138
6599
Bối cảnh xã hội đương thời khi mà tác giả viết quyển sách này rất tương đồng với hoàn cảnh Việt Nam hiện nay. Cách đây gần một trăm năm khi mà học giả Đào Duy Anh còn đang là một trí thức thanh niên, đã chứng kiến sự tràn vào của văn hóa Pháp, tấn công trực diện và làm biến dạng những văn hóa cổ truyền hàng ngàn năm của dân tộc. Ông đã nghĩ đến việc nghiên cứu và phân loại rõ ràng những yếu tố cấu thành nên nền văn hóa Việt Nam.

Ngày nay, Việt Nam đã bước vào hội nhập, đang đứng trước việc một lần nữa mở cửa với khối ASEAN và thế giới sau nhiều năm khép kín với bên ngoài. Giới trẻ gần như không có một ý niệm nào về bản sắc và văn hóa của cha ông nên dù chưa thực sự mở cửa mà văn hóa Hàn Quốc, Nhật Bản đã tung hoành trên mọi lĩnh vực. Đọc cuốn sách này, ta được học lại từ những việc thường ngày cơ bản nhất mà cha mẹ vẫn làm nhưng ta không hay để tâm tới ví dụ như: thờ cúng tổ tiên, giỗ Tết, cách ăn uống... cho đến những việc hệ trọng trong cuộc đời một người: sinh đẻ, cưới xin, làm nhà, ma chay... Những việc phức tạp hơn như quan hệ trong dòng họ, vợ chồng theo quan niệm Nho giáo, thế nào là chữ hiếu, thế nào là quan hệ trong làng xã và việc tế tự Thành Hoàng ở đình... cũng được trình bày chi tiết và dễ hiểu. Mọi mặt đời sống xưa kia được nhắc đến, nhiều khái niệm cổ truyền dùng trong lối ăn nói hàng ngày được giải thích để ta cùng hiểu và sử dụng cho chính xác.

Sách có thêm những hình vẽ và ảnh minh họa từ thế kỷ trước giúp bạn đọc trẻ không cảm thấy nhàm chán và dễ hình dung. Phần kinh tế và chính trị sinh hoạt có hơi khô cứng với số đông bạn đọc nhưng phần văn hóa xã hội thì rất hay và đáng để tìm hiểu. Muốn tồn tại được trong một thế giới phẳng phải hiểu rõ được nguồn cội của mình, mong rằng sẽ có nhiều người trẻ tìm đọc những tác phẩm công phu về văn hóa như thế này. (Tác giả lấy tư liệu từ rất rất nhiều nguồn nên thông tin đầy đủ và phong phú, phản ánh cả Bắc, Trung, Nam chứ không chỉ là làng quê Bắc Bộ như nhiều sách khác)
4
114275
2015-02-11 17:20:39
--------------------------
133154
6599
Đây là cuốn sách được xem là đầu tiên trong lịch sử nghiên cứu về văn hóa tại Việt Nam của một học giả uyên bác Đào Duy Anh. Vì đã được viết cách đây khá lâu nên được đúc kết một cách trung thực, khách quan nhất về bối cảnh và con người Việt Nam những thời trước, tuy nhiên văn hóa thì luôn luôn thay đổi trong một xu hướng tất yếu của giao lưu, tiếp xúc nên những điều về văn hóa sau này sẽ được tiếp nối bởi các công trình nghiên cứu của các nhà khoa học thời nay.
4
418013
2014-11-04 10:28:37
--------------------------
119350
6599
Việt Nam Văn hóa sử cương của cụ Đào Duy Anh từ rất lâu đã là tài liệu tham khảo không thể bỏ qua của các nhà nghiên cứu văn hóa, lịch sử nước nhà. Vì thế, tôi ko nói về nội dung nữa. Trong bản in mới  này, Nhã Nam đã trình làng 1 tác phẩm nổi tiếng với hình thức cực đẹp. Bìa sách đẹp; trình bày trang trọng, rõ ràng; chữ: cả font và kích thước đều rất ổn; in sắc sảo; hình ảnh bổ sung mang tính minh họa càng làm sách trở nên lung linh hơn; giấy tốt. Và, đặc biệt, giá thành cho 1 tác phẩm đc nhuận sắc và in ấn công phu thế này là quá ổn! Cầm lên rồi, ko thể ko mua!
5
193954
2014-08-04 15:59:59
--------------------------
