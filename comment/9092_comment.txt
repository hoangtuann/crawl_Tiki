457674
9092
So với các cuốn sách khác trong bộ sách Horrible Science thì mình thấy cuốn này kém phần thú vị hơn. Những thông tin về vũ trụ và thiên văn học được trình bày hơi rối, khó theo dõi. Mình nghĩ nếu sách về thiên văn được in màu thì sẽ dễ tưởng tượng hơn. Tuy nhiên với một đề tài sâu rộng và khó như thiên văn thì nhìn chung sẽ rất khó trình bày để có thể truyền tải thông tin đến đối tượng người đọc là trẻ em. Bù lại sách cũng có nhiều thông tin khá lý thú với nhiều hình vẽ minh hoạ vui nhộn.
3
470937
2016-06-24 14:38:15
--------------------------
435795
9092
Cuốn sách này thật sự, thật sự rất hay và lý thú. Lời văn hài hước, dí dỏm, giàu hình ảnh nên rất lôi cuốn người đọc vào từng trang sách. Nó có những hình ảnh minh họa thật sống động mà hài hước. Cuốn sách này đem đến cho ta những điều mà ta chưa chắc đã biết về thái dương hệ với cuộc gặp mặt, trò chuyện với những cư dân nơi hành tinh Táo Dầm. Ta sẽ phiêu lưu qua các vì sao và được biết đến những hành tinh khác mà ta mới chỉ nghe nhắc đến tên với những thông tin " lạnh cả sống lưng ". Mỗi hành tinh, mỗi ngôi sao, một mặt trăng của một hành tinh nào đó đều mang trên mình một cá tính riêng, tính cách riêng. Nói chung thì cuốn sách này phải nói là thật tuyệt vời ông mặt giời. Không chỉ cuốn sách này mà mình còn thích nhiều cuốn sách khác trong bộ sách Khoa Hoc Rùng Mình. Cảm ơn nhà xuất bản trẻ đã dịch lại bộ sách này để đem nó đến với người đọc Việt Nam. Và cảm ơn tiki đã góp phần quảng bá, phân phối tủ sách này. Thân.
4
1337568
2016-05-25 18:29:54
--------------------------
434242
9092
Nội dung cực kỳ phong phú nhưng viết không khô khan mà lại rất hấp dẫn, hình minh họa thì hài cực luôn, đọc đi đọc lại vẫn không thấy chán, mở sách ra thì cảm giác như mình được phiêu lưu vậy. Sách mở ra một không gian to lớn, đi từ sao này đến hành tinh khác, thú vị này đến thú vị khác nhưng chỉ gói gọn ở mấy trang sách thôi. Mình rất thích cách viết của tác giả, phóng khoáng, hài hước nhưng cần đến một tầm hiểu biết uyên thâm. Mình mua quyển này cũng lâu rồi, trước khi mình biết đến Tiki nữa, nhưng nhờ nó mà giờ mình muốn lên Tiki tìm đọc các quyển khác nữa của bộ =))
5
852971
2016-05-22 17:33:11
--------------------------
397117
9092
Những cuốn sách của Nick thực sự rất dễ đọc và tiếp thu. Cùng với những hình ảnh đáng yêu, ngôn ngữ hài hước dí dỏm dễ đi vào lòng người, giúp mình cũng như mọi người dễ dàng nhớ được những kiến thức vốn vô cùng khô khan.Ngoài ra với cuốn sách này, bạn sẽ có câu trả lời cho câu hỏi :Thực ra ngoài không gian rộng lớn kia đang xảy ra việc gì. Thực ra những ngôi sao trên trời kia có đặc điểm gì.Hệ Mặt Trời trong sách thực ra có đặc điểm gì. Còn vô vàn các câu hỏi khác có lẽ các bạn từng thắc mắc ví dụ như người ngoài hành tinh..... sẽ được giải đáp trong cuốn sách này. Cuối cùng cảm ơn Tiki vì những ưu đã và chính sách giúp mọi người có thể mua hàng rẻ nhất và tiện lợi nhất.
5
747943
2016-03-14 15:04:59
--------------------------
388885
9092
Thấy đánh giá quyển này khá tốt nên mình vác cùng quyển vật lý của các lực và hóa học những vụ nổ về đọc chơi. Ấn tượng đầu là nó còn hấp dẫn hơn mình tưởng nữa. Những kiến thức trong quyển sách này rất bổ ích nhưng cách truyền đạt không hề khô khan, ngược lại còn vui nhộn và cuốn hút. Sách mỏng mỏng cầm cũng thích thích, đặc biệt là minh họa siêu nhộn. Nhờ quyển này mà mình bắt đầu lò dò đi tìm mua thêm nhữn quyển khác trong đầu sách khoa học rùng rợn.
5
566413
2016-02-29 21:33:53
--------------------------
386907
9092
Khoa học với đủ những thứ bên trong!
Không gian,các vì sao và người ngoài hành tinh bắt đầu từ một hành tinh khác:
Bạn sẽ lần lượt khám phá:
Chiêm tinh gia sát nhân?
Tại sao không gian làm ruột gan chán phèo
Mặt trăng có mùi gì?
Nếu bạn nghĩ mình chịu được sự rùng rợn của KHoa học rùng mình thì hãy đọc tiếp và khám phá không gian xa xăm,tham gia các câu lạc bộ ngắm sao nhưng cũng đầy cuốn hút,kì lạ và vượt qua cả suy nghĩ thông thường.Vũ trụ chưa bao giờ rùng rợn đến thế.
5
846812
2016-02-26 12:39:18
--------------------------
386493
9092
Đây chính là vuốn sách tạo sự yêu thích của bạn với vũ trụ. Mở cuốn sách, ta được ngồi trên phi thuyền đi đây đó. Ta được tìm hiểu về vũ trụ và các hành tinh, biết về "con đường đi" của nó trong vũ trụ. Mình mua cuốn này định là cho em trai đọc thế mà sau khi đọc một vài trang lại ngấu nghiến cho tới trang cuối cùng. Thật hấp dẫn! Mình thích =)))).
Nội dung thì rất tuyệt nhưng có một điểm trừ là giấy chưa được đẹp, mực một vài chỗ còn bị nhòe.
4
1070263
2016-02-25 19:34:55
--------------------------
379360
9092
Mình mua cuốn sách này đã lâu nhưng tuần trước mới đọc. Đọc xong mình muốn chém gió xíu và cũng kiếm tikixu luôn. Đây chỉ là nhận định của mình các bạn tham khảo cho vui. Xét về những người đọc sách, cuốn sách này mang hai tác dụng khác nhau. Thể người đầu tiên, ít đọc các bài báo về thế giới quanh ta, vũ trụ, cuốn sách trở thành cẩm nang cho ta. Nhưng ngược lại, đối với những người thích tìm tòi, đọc ba cái mơ mộng trên internet, và trong đó có mình, cuốn sách trở nên vô dụng, vô dụng và hết sức vô dụng khi mà cái Internet quá phát triển và bị những quyển sách về tư duy lấn áp. 3 sao là dành cho loại người đầu tiên mình nói.
3
828378
2016-02-10 21:31:38
--------------------------
369858
9092
Mình vốn rất thích những cuốn sách tìm hiểu khoa học nhưng cuốn sách này thì đặc biệt làm mình thích thú, cực kì hài hước và thú vị với phần tranh minh họa rất đặc biệt.
So sánh các hành tinh với thức ăn? Loài người xấu xí trong mắt người ngoài hành tinh vì chỉ có hai mắt? Sự ra đời và phát triển của các ngôi sao,những bí mật của mặt trời, các hành tinh trong hệ mặt trời của chúng ta, các nhà chiêm tinh học và cuộc hành trình khám phá của họ tất cả những điều thú vị đều có trong cuốn sách này. 
Khoa học thực sự không khô khan một chút nào cả.
Mình rất thích series này.
5
306081
2016-01-16 16:59:59
--------------------------
337961
9092
Cuốn sách này là một cuốn sách vô cùng bổ ích cho những bạn thích tìm hiểu vũ trụ, những hành tinh trong hệ mặt trời, mặt trăng, mình thích nhất là những hình ảnh dí dỏm, sau mỗi hành tinh lại là một câu truyện của Táo dầm, dẻo quẹo. Mỗi hành tinh lại có một thông tin thú vị và bổ ích. Bộ sách Horrible Science này giúp làm mới vấn đề đối với những người dù không hiểu biết về những truyện .... trên trời. Cùng với các hành tinh là chuyến du hành của các phi hành gia, các nhà chiêm tinh, cùng những bài thực hành thú vị và bổ ích
4
886384
2015-11-15 06:59:52
--------------------------
329979
9092
Mình rất thích vũ trụ nên " canh me " trên Tiki có sách thể loại này là tranh thủ đưa nàng về tủ sách luôn . Nếu ta phải khá đau đầu vì Lược Sử Thời Gian thì đây lại là một quyển sách giải thích dễ hiểu , hấp dẫn lại kèm thêm hình ảnh minh họa rất ngộ nghĩnh và những từ ngữ khiến ta thấy vũ trụ như được kéo gần về phía mình hơn . " Không Gian - Các Vì Sao - Và Người Ngoài Hành Tinh " là một quyển sách không thể thiếu cho những người có niềm đam mê Thiên văn học 
5
617834
2015-11-01 20:13:24
--------------------------
310797
9092
Sách viết ngắn gọn, dễ hiểu và hài hước. Nội dung sách cung cấp thông tin về các vì sao, mặt trăng và về các hành tinh trong hệ mặt trời...  Người đọc được dẫn dắt qua câu chuyện những chuyến phiêu lưu của Táo Dầm trong vũ trụ cùng nhiều nhà du hành khác. Ngoài ra sách còn có rất nhiều thí nghiệm nho nhỏ để các độc giả nhỏ tuổi có thể làm theo. Tóm lại đây là một quyển sách rất thú vị và bổ ích, nội dung hay! Thích hợp có mặt trên tủ sách của mọi trẻ em ham thích khoa học.
5
802760
2015-09-19 19:53:57
--------------------------
309073
9092
Có khi nào bạn ngước lên bầu trời đêm đầy sao và tự hỏi rằng ngoài khoảng không gian bao la kia sẽ là một vũ trụ như thế nào? Bạn thắc mắc và chưa tìm ra câu trả lời? Yên tâm, cuốn sách này sẽ trả lời giúp bạn ! Bạn sẽ được tận hưởng một chuyến du hành ngoài vũ trụ khi đọc cuốn sách này.Từ vũ trụ bao la đến những vì sao,mặt trời rực rỡ ;rồi mặt trăng xấu xí;... Tất cả những gì ngoài vũ trụ đã được tác giả viết ra một cách đơn giản , ngôn từ dễ hiểu có kèm hình ảnh sinh động để bạn có thể dễ dàng hình dung ngoài vũ trụ kia là những gì.Mình rất là hài lòng về cuốn sách thú vị này.
5
57884
2015-09-18 22:09:51
--------------------------
294059
9092
Mình rât thích đọc những quyển sách tìm hiểu về khoa học vũ trụ. Quyển sách giới thiệu cho ta về các ngôi sao, mặt trời cũng là một ngôi sao tuy lớn nhưng cũng nhỏ hơn nhiều so với nhiều ngôi sao khác, có rất nhiều sao còn nóng và sáng hơn cả mặt trời, rồi tìm hiểu về các hành tinh trong hệ mặt trời thông qua lời kể hài hước, dí dỏm của người ngoài hành tinh Táo dầm. Một cuốn sách dành cho cả người lớn và trẻ em. Bộ sách có rất nhiều đề tài thú vị khác mình sẽ sưu tầm đủ
4
381629
2015-09-08 23:00:52
--------------------------
287937
9092
Lúc đầu mình không định mua quyển này vì với cái tên "Không Gian - Các Vì Sao - Và Người Ngoài Hành Tinh" vì nghĩ kiến thức của nó có vẻ xa xôi không thực tế, nhưng đọc được nhiều comment tốt nên mình vẫn mua và thấy không có gì đáng tiệc cả, sách cung cấp các kiến thức về vũ trụ, nhân vật người ngoài hành tinh có chăng là một cách kích thích các bé tìm hiểu và ước mơ. Điểm trừ là sách mình nhận bìa sách cong keo và bọc không cận thẩn 1 chút gì, mình phải bỏ bọc và vuốt lại bìa cho phẳng mới ok
4
153008
2015-09-03 09:45:52
--------------------------
270945
9092
Không chỉ chứa đầy những con số khổng lồ của một vũ trụ vĩ đại, cuốn sách này còn cho chúng ta biết về những hành tinh anh em trong hệ mặt trời của chúng ta. Vũ trụ thật rùng mình mà cũng thật lí thú. "Không gian - Các vì sao - người ngoài hành tinh" chính là cuốn sách cần thiết giúp chúng ta hiểu thêm về trái đất, và thấy nó vẫn "dễ thương" quá chừng khi bảo tồn sự sống của loài người, không khắc nghiệt như Thủy tinh, không ngược đời như kim tinh, không đáng sợ như hoả tinh... Và biết đâu cuốn sách này lại là nguồn cảm hứng cho người đọc khám phá sự sống ngoài hành tinh trong hàng nghìn ngân hà bao la.
4
113650
2015-08-18 18:55:32
--------------------------
253213
9092
Khoa học với đủ thứ chuyện bên trong.Không gian, các vì sao và người ngoài hành tinh đến từ một hành tinh khác.Vũ trụ to lớn và nguy hiểm nhưng cũng tuyệt đẹp và đầy bí ẩn đang chờ chúng ta khám phá. Trong tương lai con người sẽ khám phá ra nhiều điều thú vị hơn nữa.Cuốn sách này thật sự hấp dẫn, lôi cuốn và cũng không kém phần rùng rợn về những dữ kiện quái dị, những câu đố lộn ruột và những bức tranh ghê gớm.Vừa học vừa chơi, tôi đã học được nhiều  điều và có những giây phút thư giãn đáng giá bên những trang sách này.
5
280592
2015-08-04 11:27:25
--------------------------
240725
9092
Mình không hứng thú với những gì ngoài trái đất lắm - ban đầu là vậy, vì có vẻ nó hơi viển vông, xa vời, hay nói cách khác là "cao siêu". Nhưng đến với bộ Horrible Science, đặc biệt là quyển sách "vượt cả hành tinh" này, có cảm giác như mỗi người đọc trở thành một nhà khoa học nghiệp dư vậy. Tác giả rất hài hước hoá thân thành "táo dầm" đi khám phá vũ trụ, đem về những thông tin hữu ích từ tuốt ngoài mặt trăng, những hành tinh khác, lội dòng lịch sử tìm về những nhà du hành vũ trụ. Quyển sách rất hữu ích cho trẻ em, đồng thời cũng mang đến cái nhìn vui nhộn và gần gũi cho người lớn. 
5
120276
2015-07-24 21:38:26
--------------------------
239944
9092
Mình bị ấn tượng bởi cuốn sách này từ cái nhìn đầu tiên. Đây bạn hãy xem cuộc đối thoại của hai sinh vật ngoài hành tinh xanh lè bí ẩn về con người: "chỉ có hai mắt" - "xấu tệ". Mình quyết định mua liền, và đã không phải thất vọng. Nội dung cuốn sách cũng hài hước như cái bìa vậy, nhưng là cách nói hài hước thôi, chứ còn kiến thức thì rất chính xác và nghiêm túc. Mình chưa bao giờ thấy đọc sách khoa học mà lại ...phẻ thế này. Vui nhộn một cách tưng tửng, và lại rất bổ ích, giống như kiểu một món ăn rất vừa miệng mà lại còn tốt cho sức khỏe nữa ý.
5
82774
2015-07-24 10:25:24
--------------------------
230453
9092
Nếu đọc xong quyển sách này và yêu thích vũ trụ, thì mình nghĩ bạn nên tìm đọc thêm quyển Thiên Hà - Thật đáng ngạc nhiên của tác giả Kjartan Poskitt, cũng thuộc bộ sách khoa học này, đó cũng là quyển sách khá hay và bổ ích.
Với những hình vẽ ngộ nghĩnh và hóm hĩnh, quyển sách đã đem đến cho chúng ta những kiến thức về các hành tinh, tìm hiểu những điều kỳ lạ trong không gian vũ trụ hay tham gia vào những câu chuyện thú vị về các vì sao và thậm chí là... người ngoài hành tinh, ngoài ra có thể thử trắc nghiệm để thử xem vốn hiểu biết về những thứ kỳ quặc của mình ra sao.
Tóm lại, nếu ai là fan của hai chú Nick và Tony thì khó mà bỏ qua quyển sách bổ ích này, nhất là những ai yêu thích thế giới "đen thui" ngoài kia. ^^
5
382812
2015-07-17 14:54:13
--------------------------
222704
9092
Không gian ngoài vũ trụ là gì mà nó khiến nhiều người phải thích mê đến vậy ? Để lí giải cho điều đó thì chỉ cần đọc qua cuốn sách “ Không gian các vì sao và người ngoài hành tinh “ thì bạn sẽ hiểu ngay thôi. Qua lời văn dí dỏm, dễ hiểu, hình ảnh minh họa sinh động ngộ nghĩnh, … của tác giả tài hoa Nick Arnold sẽ giúp bạn hiểu vì sao các phi hành gia trước khi bay ra ngoài trái đất lại không được ăn đậu, vũ trụ rộng lớn đến nhường nào, … tất tần tật điều đó sẽ có trong cuốn sách này. Một cuốn sách thú vị và bổ ích cho mọi người.
5
554214
2015-07-06 08:03:15
--------------------------
217251
9092
Không gian các vì sao luôn là điều huyền bí với mỗi chúng ta, những bí mật trong vũ trụ luôn gây một sức hút tò mò lớn đối với loài người. Bên ngoài trái đất có những gì? Cuốn sách này sẽ giúp bạn khám phá phần nào câu hỏi đó! Với giọng văn hài hước, những câu chuyện bên lề thú vị được lồng ghép khiến những kiến thức khoa học vũ trụ tưởng chừng khô khan khó nuốt lại trở nên vô cùng dễ hiểu và đầy hứng thú. Cuốn sách tập trung khám phá về dải ngân hà và hệ mặt trời của chúng ta với những điều kỳ bí, có khi lại khá “kinh khủng”, dải ngân hà có 200.000 tỷ ngôi sao, thế nhưng có đến 99,9999% vũ trụ hoàn toàn trống rỗng, hấp dẫn chưa nào? Nếu là người yêu thích tìm hiểu về vũ trụ huyền bí, bạn không nên bỏ qua cuốn sách này!
5
135597
2015-06-29 01:11:23
--------------------------
168156
9092
Đã đến lúc tạm biệt những trang sách khô khan khó nuốt trong nhà trường, vứt bỏ những giờ giảng nhàm chán và những con chữ chẳng đọng lại não quá mấy ngày. Bộ sách này cho người đọc một quan điểm mới về việc đọc lấy kiến thức. Rất khùng điên, vô cùng hài hước, không hề có sự nghiêm túc đến ngao ngán của những khoa học trên giảng đường đại học. Đọc và tôi mê mẫn, lại cố công sưu tập thêm cho đủ bộ sách này. Chỉ tiếc mỗi quyển ngắn quá, đọc hết rồi mà lại ngẩn ngơ.
5
515736
2015-03-16 06:51:44
--------------------------
130687
9092
Cuốn sách không chỉ mang rất nhiều kiến thức về Hệ Mặt Trời, các vật thể ngoài không gian mà còn được trình bày rất vui nhộn và sinh động. Mặt dù là sách khoa học vũ trụ, nhưng nó không khô khan và nhàm chán, ngược lại, bạn có thể cười lên ở bất cứ trang sách nào! Bạn có thể đọc đi đọc lại bao nhiêu lần mà vẫn không thấy chán! Mình đã đọc 4 lần rồi! Thật sự rất thú vị! Phong cách của Horrible Science!!
5
381603
2014-10-19 14:00:09
--------------------------
