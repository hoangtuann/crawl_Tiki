362657
7365
Mình rất thích văn của bọ Lập , một phần vì bọ từng trải và một phần vì bọ có lối viết rất giản dị mà sâu sắc , sâu tận vào trong tâm can người đọc.
Chiến tranh cướp đi của chúng ta quá nhiều và hậu quả của nó để lại cũng là vô cùng đau đớn .
Cuốn sách còn giúp những người trẻ trân trọng hơn cuộc sống và tình yêu mà mình đang có bởi hạnh phúc là vô cùng mong manh . Thật xót xa cho những tình yêu thời chiến "cách nhau có một thước đất mà gọi hoài không nghe thấy nhau"
5
844106
2016-01-02 14:24:20
--------------------------
182091
7365
Bìa sách gợi cho người ta 1 nỗi buồn man mác, còn cái tựa đúng là gợi lên cho người ta cảm giác chông chênh, mong manh. Ai mà không muốn được hạnh phúc? Nhưng cuộc đời vốn rất phũ phàng, những mong muốn tưởng chừng giản đơn của họ lại cứ gặp phải bao nhiêu là trắc trở và bất hạnh. Vòng xoáy của  chiến tranh, của đói khổ nào có chừa có tha cho bất cứ ai. Kết thúc của truyện buồn, nhưng vẫn mang lại cho người ta niềm hy vọng,dẫu niềm hy vọng đó có mong manh thế nào chăng nữa.
4
472572
2015-04-13 10:03:44
--------------------------
160635
7365
Mình vốn không thích đọc truyện ngắn lắm, nhưng những truyện trong quyển sách này thì mình công nhận là nó khá hay. “hạnh phúc” – hai từ ngữ quá đơn giản để thốt ra, nhưng lại là cái đích khó mà vươn tới của biết bao người. Những câu chuyện của Nguyễn Quang Lập làm cho mình thấy xúc động, xen lẫn với những đau thương mất mát mà chiến tranh mang lại chính là những tình cảm ấm nóng giữa con người với nhau, dù cho kết thúc có buồn, thì hành trình để đi đến kết thúc ấy cũng không làm cho con người ta phải hối hận.
4
418163
2015-02-25 09:03:16
--------------------------
109421
7365
Tựa đề của cuốn sách rất phù hợp với nội dung. Những mẩu chuyện riêng biệt của từng số phận khác nhau, nhưng đều giống nhau ở một điểm đó là hạnh phúc là điều ai cũng mong muốn, chờ đợi, hy vọng và mong ước, và đó là những niềm hạnh phúc thật giản đơn, vậy mà trong hoàn cảnh xã hội trong và sau chiến tranh với nhiều mất mát, loạn lạc và tổn thương, hạnh phúc thật quá mong manh. Mong manh như chính mạng sống của người lính trên chiến trường, như số phận của những con người đang sống mà luôn mang trong mình hoài niệm và nỗi đau của quá khứ, để rồi không thể tỉnh táo mà tiếp tục cuộc sống thực tại được nữa. 
Những nỗi buồn man mác cứ lan tỏa và lớn dần qua từng câu chuyện, nhất là khi những câu chuyện đó đều mang một cái kết thúc buồn.
5
53977
2014-04-01 12:15:51
--------------------------
105938
7365
Không biết nhận xét thế nào hơn, mình chỉ có thể nhận xét rằng quyển sách này khá tuyệt cho những ai yêu thích văn học cách mạng, văn chương thời chống Pháp, chống Mỹ cứu nước có nội dung như các tác phẩm đã học thời trung học. Mỗi câu truyện của tác giả viết trong cuốn sách đều chứa đựng như dư vị riêng, màu sắc riêng nhưng màu sắc buồn man mác luôn ẩn chứa thường trực trong mỗi câu truyện của tác giả. Thoạt nhìn mình có thể thấy, cái kết của mỗi câu truyện đều là những cái kết buồn, tha thiết buồn và tưởng chừng như không có hậu. Nhưng khi ngẫm kỹ lại thì đấy chính là điều tác giả muốn gởi gắm cho độc giả đó là những đau thương và mất mát trong chiến tranh, đau thương đó nhưng lắm tình người, tình đời, dù bạc bẽo đến đâu, trái tim của con người thời ấy vẫn thật thà, thuần khiết một tình yêu quê hương, yêu gia đình...
5
270580
2014-02-10 21:27:04
--------------------------
