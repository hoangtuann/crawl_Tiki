379335
8346
Cuốn không những chỉ nói về tính cách của đa số những niềm yêu thích , ước mơ của nghệ thuật luôn có vai trò tinh tế , điểm mạnh lớn chính là cuộc sống được đối diện hoàn hảo , không có mặt khó khăn , chạy lại với thành công đỗ đạt , ngày càng so sánh xã hội , màu sắc tô vẽ đem tới sự nhiệt huyết ở trong đó có tất cả các tài năng vượt trội để mỗi khi ngắm nhìn ta đều thấy yêu thích đời người , sống cho cái thiện .
4
402468
2016-02-10 19:24:38
--------------------------
356336
8346
Bộ sách "Quà tặng cuộc sống'' gồm 6 tập nhưng đây là tập đầu tiên mình mua về! Cả về hình thức và nội dung của bộ sách đều rất thu hút người đọc! Từ những câu truyện về tình thầy trò, cuốn sách cho chúng ta những bài học trong đời sống, cách nghĩ tích cực hơn, yêu đời hơn. "Cuộc đời không phải chỉ là những lần thi cử.." câu nói ấn tượng ngay từ khi mình đọc quyển truyện (ngay mấy trang đầu). Một câu nói ấn tượng khác đối với mình: ''Điểm số không phải là cơ sở quyết định xem con là người như thế nào và con có thể làm gì cho đời. Đừng tự đánh giá mình theo cách nhìn của người khác. Trong mỗi người đều tồn tại một sức mạnh vĩ đại và khả năng đặc biệt. Nếu con thấy được một ánh sáng nhỏ nhoi từ hình ảnh lớn lao của chính mình, biết được bản thân mình là ai, biết được con đã mang gì đến cho thế giới này thì khi đó con sẽ có thể làm cho cả thế giời này hoàn toàn thay đổi".
5
768112
2015-12-21 18:31:07
--------------------------
170773
8346
Đây là một trong những quyển sách nằm trong đơn hàng đầu tiên khi mình mua trên Tiki. Thật sự đọc rất hay và ý nghĩa. Nói lên tình cảm thầy trò thiêng liêng, cảm động .Cuối mỗi câu chuyện đều có suy ngẫm, giúp mình rút ra được nhiều bài học quý báu. Bìa sách thiết kế cũng rất đẹp, bắt mắt. Giấy tốt, in ấn đều rất ổn. Lâu lâu lấy ra đọc lại cảm thấy rất hay, thấm đượm tình cảm thầy trò. Mình định sưu tầm trọn bộ nhưng Tiki hết hàng mất rồi! Huhuhuhu T_T
5
303016
2015-03-20 18:29:30
--------------------------
