491727
8168
Khế Ước Xã hội là một trong những quyển sách kinh điển đề ra ra các nguyên tắc của một nhà nước của dân, do dân, và vì dân. Không dành cho tất cả mọi người, cuốn sách này dành cho những bạn đam mê triết học, chính trị và luật và những bạn muốn theo các ngành thế này. Cần đọc nó với sự nghiền ngẫm, tuy không dành cho tất cả mọi người nhưng nội dung của cuốn sách lại có giá trị sâu sắc trong xã hội, bởi vì quả thực chúng ta đang sống trong thời đại của các nền dân chủ: xã hội ngày nay được hình thành nhờ những nền tảng được dựng nên bởi các triết gia thời phong trào Khai sáng ở châu Âu như J.J.Rouseau. Mặc dù có lẽ là mang tinh hàn lâm, nhưng mình nghĩ rằng mọi người nên đọc nó để hiểu được các quyền, các hiểu biết và lý lẽ chính trị, mọi người không nên thờ ơ với chính trị vì nó có liên quan mật thiết đến cuộc sống của mọi người sống trong một cộng đồng xã hội.
Mình không mua trên tiki mà mua ở ngoài vì trước đây tiki hết hàng, hẳn là cùng loại thôi. Bìa sách được thiết kế đẹp, tuy sách mỏng nhưng nội dung có giá trị, có vẻ nhàm chán nhưng bổ ích.
5
366877
2016-11-20 12:15:31
--------------------------
425674
8168
Như tất cả các loại sách về đề tài này, cuốn sách không dành cho tất cả mỗi người. Nó đòi hỏi ở bạn một sự hiểu biết nhất định và một nghị lực, kiên nhẫn, một tâm hồn rộng mở để, để hiểu.
Nhưng khi đã đọc và tiếp cận được bạn sẽ không thấy thất vọng về những gì 'giao tiếp' được với sách. Nó giúp chúng ra liên hệ và có những suy nghĩ sâu sắc hơn về thời đại chúng ta đang sống.
Trước khi đọc được cuốn này nên đọc cuốn Bàn về tinh thần Pháp luật của Montesquieu (Cuốn này tiki không có bán T.T); Luật sư tài ba (Cuốn này Tiki hết hàng đã lâu nhưng chưa thấy về), sẽ thấy dễ tiếp cận hơn.


5
596204
2016-05-05 10:17:47
--------------------------
417394
8168
Một cuốn sách gối đầu giường của dân Luật, đây là một cuốn sách mà các người học luật, làm luật cần thiết phải đọc trong quá trình học tập, nghiên cứu của mình bên cạnh Chính trị luận và Tinh thần pháp luật. Khi nghiên cứu cả 3 cuốn này sẽ mang lại một kiến thức nền tổng quát về nhà nước, xã hội và pháp luật, trong đó người nghiên cứu sẽ biết được những vấn đề cơ bản như nguồn gốc của nhà nước và pháp luật, sự cần thiết của sự hình thành nhà nước và pháp luật.
5
1241959
2016-04-16 22:08:44
--------------------------
402340
8168
Mua quyển sách này do ông thầy dạy triêt dụ dỗ nhưng không ngờ nó lại làm tôi nghiềm ngẫm đọc từ ngày này qua ngày khác. Đọc không phải cho xong mà vừa đọc vừa nghiềm ngẫm. Ngẫm ra rằng cuộc sống phức tạp này suy cho cùng cũng là bắt nguồn từ những thứ đơn giản- đó là những khế ước xã hội.
Có lẽ những người trẻ sẽ ít để ý những quyển sách dạng này, nhưng nếu muốn sống chậm một chút để suy ngâm để rồi sẽ sống nhiệt thành và thấu hiểu hơn thì có lẽ đây là quyển sách cần thiết. Thân.
3
54517
2016-03-22 09:26:09
--------------------------
329081
8168
Ưu điểm:
- Bìa sách đơn giản, dễ gây ấn tượng
- Chất liệu giấy bền, đẹp
- Nội dung sách sát với bản gốc
- Những kiến thức cơ bản về triết học và chính trị khá trình bày khá dễ hiểu, mang đến sự thông suốt nơi người đọc
Nhược điểm:
- Sách không dài lắm nhưng đọc dễ gây chán.
- Thiếu những dẫn chứng cụ thể từ thực tế.
Nhưng nhìn chung lại, "Khế Ước Xã Hội" của tác giả Jean - Jacques Rousseau  cũng mang lại nhiều kiến thức về nhân quyền, lý luận chính trị qua đó giúp ta hiểu thêm về bộ máy điều hành và phát triển nhà nước... 
4
144447
2015-10-30 22:59:05
--------------------------
234071
8168
Cuốn sách này được khá nhiều người tâm đắc về triết học và chính trị chọn làm sách gối đầu giường. Nếu bạn có đam mê tìm hiểu về chính trị, nhà nước, nguồn gốc hình thành và xu hướng phát triển của nhà nước thì chắc hẳn không thể bỏ qua được cuốn sách này. Tuy nhiên muốn đọc Khế ước xã hội bạn phải có các kiến thức cơ bản về triết học, lý luận nhà nước và pháp luật thì mới có thể nắm được các học thuyết cơ bản của tác giả. Nhìn chung sách khá khó đọc và cần nhiều thời gian để nghiên cứu.Sách được dich thuật khá tốt, bản in và chất lượng giấy cũng rất đẹp.
4
198406
2015-07-20 08:56:41
--------------------------
134045
8168
Thật ra trước đây tôi chưa hề biết đến cuốn sách này tôi chỉ bắt đầu đọc nó khi được các giảng viên tỏng trường giới thiệu.
Đây quả thật là một cuốn sách hay đề ra một trong những học thuyết dẫn đến sự hình thành của Nhà nước rất cần thiết đối với sinh viên đặc biệt là sinh viên Luật  tuy nhiên cuốn sách nêu lên những vấn đề khá trừu tượng do đó đòi hỏi người đọc phải dành nhiều thời gian để nghiền ngẫm suy luận để tìm ra được triết lý của cuốn sách này.
4
415874
2014-11-07 22:56:24
--------------------------
