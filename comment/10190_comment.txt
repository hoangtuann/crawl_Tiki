528629
10190
Đầu tiên là chất lượng sách hơi kém , giấy mỏng và xấu ! Một số truyện có kết thúc nhảm (kiểu như báo mộng thấy , r tình cờ gặp hung thủ kể lại) , phi logic !
2
2119180
2017-02-20 18:04:19
--------------------------
500797
10190
Mua vì giảm giá nhiều. Có vài truyện hay cũng có vài truyện nhạt vô cùng.
4
2006156
2016-12-27 12:50:57
--------------------------
73519
10190
Mình mua truyện này trong một dịp tình cờ và nó đã làm mình thất vọng. Nhìn từ ngoài, bìa được thiết kế khá đẹp, là một thanh gươm của công lý, những vụ án mạng rồi cũng sẽ được giải quyết vì công lý luôn thắng gian tà. Xét về nội dung, cốt truyên khá hay và truyện có cách mở đầu lôi cuốn và hấp dẫn nhưng càng về sau, truyện càng rời rạc và nhàm chán, lời văn đôi lúc hơi khó hiểu. Một khuyết điểm nữa là truyện sử dụng nhiều tên nhân vật khiến cho người đọc rất khó để nắm bắt mạch truyện, diễn biến lại khá nhanh. Khúc cuối, truyện có những yếu tố bất ngờ khiến cho truyện cũng khá ổn.
2
57869
2013-05-08 20:40:16
--------------------------
