241613
6450
Nháp của Nguyễn Đình Tú quả là một cuốn tiểu thuyết cực kỳ ấn tượng. Tràn ngập trong sách là sex, có lẽ đây là lý do khiến nó từng bị từ chối in rất nhiều lần, nhưng tác giả viết sex không phải để nói sex mà là chỉ thông qua đó để hướng tới những vấn đề đậm chất nhân văn, những vấn đề liên quan đến tâm hồn rất nhức nhối của loài người. Đó là sự lạc lối về lương tri, là nỗi hoang mang không biết đâu là thiện - ác trong cái xã hội đảo điên đen - trắng hiện tại. Những nhân vật trong Nháp quay cuồng vùng vẫy muốn thoát ra khỏi cái bẫy cuộc đời mà mình sụp vào đồng thời tìm cho mình niềm hy vọng trong kiếp người đầy đau khổ. Quả thật là một cuốn tiểu thuyết tuyệt vời!
4
167283
2015-07-25 17:26:13
--------------------------
