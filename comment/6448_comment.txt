547833
6448
Sách được viết dưới dạng những mẫu truyện trong cuộc sống, xoay quanh con người Do Thái. Tôi có thể chiêm nghiệm được sự đúng đắn trong từng câu chuyện theo tư duy riêng và thử áp dụng vào cuộc sống, bạn sẽ bất ngờ đấy.
Cuốn sách nên có trong tay
5
1021535
2017-03-19 20:54:16
--------------------------
127276
6448
Tôi đã và đang tò mò rất nhiều về tài năng kinh doanh được xem là thiên bẩm của người do thái. Háo hực với tiêu đề bao nhiêu, tôi hoàn toàn thất vọng về nội dung cuốn sách. Có cảm giác tác giả cuốn sách cóp nhặt từng bài viết, từng mẩu chuyện từ internet, biên soạn sơ sài và được cho xuất bản vội vàng, đi kèm những câu truyện minh họa không được chứng thực. 

Sẽ tốt hơn nếu nó là một tác phẩm được nghiên cứu và đúc kết từ những doanh nhân do thái đich thực.

Bổ sung:
Tôi đã tìm được một phần giải đáp cho sự  thành công của dân tộc Do Thái qua tác phẩm "Quốc Gia Khởi Nghiêp". Vây nên xin mao muội giới thiệu tới các bạn quân tâm.
2
162959
2014-09-23 16:05:06
--------------------------
