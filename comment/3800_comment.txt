230641
3800
"Phương pháp giải hóa học phần hữu cơ lớp 12" là cuốn sách giải giúp mình hoàn thành tất cả các bài tập như chuẩn bị bài và các bài tập trên lớp một cách hoàn chỉnh. Đem lại cho mình được điểm cao hơn. Giúp mình từ một học sinh trung bình môn hóa lên được học sinh khá môn này. Sách còn có nội dung rõ ràng, câu văn mạch lạc với nhiều ví dụ mới lạ. Mình nghĩ đây là một cuốn sách dành cho các bạn đang học 12 rất nên xem. Cảm ơn tiki và tác giả đã đem đến cho mình cuốn sách này.
5
655012
2015-07-17 16:42:59
--------------------------
206531
3800
Thiết kế bìa đẹp, trình bày tên sách sáng sủa dễ nhìn, hình minh họa bìa bắt mắt.Giá tiền phù hợp. Nội dung chia làm 5 chuyên đề rõ ràng và theo thứ tự:Este-lipit,cacbohidrat(gluxit), amin-aminoaxit-protein,polime-vật liệu polime, tổng hợp hữu cơ. Mỗi chuyên đề chia làm 3 phần: phần tóm tắt kiến thức cơ bản ngắn gọn, dễ hiểu giúp người học nắm vững kiến thức căn bản; các dạng bài tập cơ bản cung cấp các dạng bài tập phổ biến thường ra trong các đề tuyển sinh cao đẳng đại học được tác giả giải một cách gọn gàng, súc tích; phần bài tập luyện tập giúp người học có thể tự luyện để đánh giá năng lực của bản thân
5
351857
2015-06-09 23:50:21
--------------------------
