249286
4934
Sách đa số là hình ảnh.Chữ thì rất ít.Mỗi món ảnh sẽ có ảnh của những người nổi tiếng ăn trong quán đó.Mình thấy quán Mì vịt tiềm ngon nhất không phải quán trong như sách nói đâu,mì vịt tiềm ở quận 4 là ngon nhất vậy mà không thấy được đề cập trong sách.Giá như vậy là quá chát.Mở sách ra không như mình mong đợi,thông tin rất ít.Bạn nên tra mạng các món ăn ngon sẽ được nhiều thông tin hơn là đọc cuốn sách này.Điểm trừ nữa là sách không đề cập giá cả của món ăn như ở trên mạng
3
187271
2015-07-31 15:00:01
--------------------------
221823
4934
Về thiết kế bìa sách khá ấn tượng và cũng khơi gợi nên nội dung có trong sách. Mình vốn cũng là một tín đồ ăn quà vặt lúc còn ở Sài Gòn, bây giờ đã về quê sống nhưng lâu lâu có dịp vào Sài Gòn là mình cùng bạn bè lê la hết hàng nà đến quán khác.  Tuy nhiên khi đọc quyển sách này mình mới thấy các quán quà vặt mà mình từng ghé trong hơn 9 năm ở Sài Gòn chưa là gì, khi nào có dịp vào lại SG nhất định mình sẽ ghé thử. Cuốn sách rất hữu ích cho những bạn đi du lịch muốn khám phá ẩm thực phong phú và đa dạng ở Sài Gòn.
5
572238
2015-07-04 14:45:35
--------------------------
166696
4934
Mình mua quyển này ngay trong hội sách Tiki 3/2015 ở Lê Thánh Tôn. Cảm nhận của mình là quyển sách giới thiệu khá nhiều món ăn ngon từ quán ăn sang trọng cho đến vỉa hè. Có thể nói tác giả đã bỏ công sức sưu tầm không ít. Hình ảnh trực quan sinh động, chỉ cần đọc lời văn và xem hình là đã thấy thèm! Đây đúng là quyển sách dành cho những bạn có tâm hồn ăn uống như mình :-P

Nhược điểm: Có 1 số quán mình đi thử như trong sách giới thiệu thì không được ngon lắm và giá thành hơi mắc. Tuy nhiên đây chỉ là nhận xét chủ quan của mình thôi vì mình chưa đi hết các quán xá còn lại trong sách :D
4
33861
2015-03-13 10:54:03
--------------------------
