314595
9861
Tác giả khéo léo vô cùng khi xây dựng các tuyến nhân vật với nhiều cảm xúc tâm lý chồng chéo nhau. Một nàng Catherine Rose Gagnon quá ư bí hiểm với một chấn thương tâm lý từ năm 13 tuổi. Một chàng cảnh sát Bobby luôn tin vào trực giác nghề nghiệp của mình, chưa bao giờ mắc sai lầm nhưng liệu điều đó có mãi là tuyệt đối?!?!? Số phận đưa họ đến với nhau trong một tình huống ngàn cân treo sợi tóc, trong đó tính mạng cong người được đem ra đánh cược. Gay cấn, hồi hộp và nghẹt thở là những gì mình cảm nhận được về tác phẩm này. Sự thật có còn là sự thật nếu bí mật chưa được hé lộ hoàn toàn??!?
5
93234
2015-09-26 15:24:14
--------------------------
283537
9861
Truyện khá hồi hộp, gay cấn, phân tích tâm lý sâu sắc và chi tiết. Câu hỏi từ đầu đến cuối truyện mình mới giải đáp được là ai mới là kẻ nói dối, là hung thủ thật sự, liệu những việc đó là sự cố ngẫu nhiên hay có bàn tay ai đó sắp đặt. Tâm lý nhân vật khá phức tạp, bí ẩn và khó đoán. Càng về sau càng hấp dẫn khi các tình huống được đẩy lên cao trào. Một cuốn sách hay, kích thích tư duy tìm hiểu, suy luận, tò mò. Tuy nhiên, truyện đôi chỗ còn gượng ép, thiếu thuyết phục. Mạch truyện đôi chỗ hơi dài dòng, không phù hợp với tiết tấu của một truyện trinh thám - tâm lý.
5
360787
2015-08-30 06:08:51
--------------------------
275299
9861
Mở đầu truyện đã khá hồi hộp, gay cấn. Khi án mạng xảy ra, các tình tiết sau đó dồn dập, đẩy cao trào lên đỉnh điểm với đầy sự bí ẩn và khó hiểu. Tâm lý nhân vật được khắc họa khá tốt, cảm thấy đồng cảm và thương xót cho tuổi thơ của nhân vật Lisa Gardner, chính quá khứ khủng khiếp đã khiến cô trở thành con người nham hiểu về sau này. Nói chung truyện đọc được nhưng không để lại nhiều ấn tượng, theo mình thì cuốn trinh thám phương tây này chỉ thuộc hạng B, C.
3
6502
2015-08-22 19:55:07
--------------------------
226449
9861
Đây là tiểu thuyết tâm lý hình sự. Tác giả nữ nên đôi lúc hơi lê thê dài dòng, chú ý nhiều đến tiểu tiết khiến nhịp độ của vụ án chậm lại chứ không như không thể bỏ xuống như các lời bình của sách đã có. Và vì là tác giả nữ nên phần nào khắc họa tâm lý của nhân vật nữ chính có phần sắc xảo, chính xác hơn nhân vật chính nam. Phần đầu dẫn dắt còn nhiều gượng ép, thiếu thuyết phục. Cái hay là ở chỗ những nhân vật của tác phẩm đều đạt đến độ "đơn độc" nhất định, ai rồi cũng "một mình chống lại mafia", là tòa án lương tâm, sự chênh vênh giữa có tội hay không có tội, nên làm thế này hay làm thế kia; rồi một mình chống lại "dư luận", báo chí tường thuật một chiều, bạn bè đồng nghiệp, người thân không đối đầu nhưng cũng hiềm nghi, ái ngại... Một ưu điểm nữa của tác phẩm là có một cái kết hay, lạ, thể hiện phần nào bản chất của xã hội phương Tây: tự do đến độ ... kỳ cục.
3
129879
2015-07-12 11:05:08
--------------------------
224911
9861
Đây là quyển sách đầu tiên mình mua trên tiki trong danh mục truyện trinh thám.Thật sự truyện làm cho mình muốn toát mồ hôi khi đọc,với những tình tiết gây cấn đan xen vào nhau,khiến mình không thể nào bỏ quyển sách xuống.Trong suốt câu truyện,người đọc luôn luôn bị quảnh quanh bởi câu hỏi: Ai mới là kẻ giết người thật sự,vì ai cũng đều nằm trong diện tình nghi hết.
Cũng chính vì tác giả miêu tả nội tâm nhân vật quá sắc sảo nên lại càng lôi cuốn người đọc.Chuyển biến tâm lý của nhân vật dồn dập khiến mạch truyện như hối thúc người đọc phải suy luận nhanh hơn là Bob hay là Cat hay là ai đang đứng sau những vụ mưu sát đó.Cao trào đưa đỉnh điểm lên cao để rồi cái kết của truyện thật bất ngờ.Vượt qua khỏi sự suy nghĩ của người đọc khi mà chính bố chồng của Cat là kẻ chủ mưu.
Tình tiết gây cấn,mạch truyện căng thẳng dồn dập,thắt mở linh hoạt đã tạo nên 1 ấn tượng khó phai với mình sau khi đọc xong truyện này.Đây quả thực là 1 câu truyện rất hay.
4
368991
2015-07-09 14:15:12
--------------------------
204804
9861
Điểm nổi bật của tác phẩm là các phân tích tâm lý sâu sắc vả chi tiết. Người đọc dường như cảm nhận rõ ràng từng suy nghĩ và cảm xúc của nhân vật, bởi lẽ chúng rất gần gũi, rất đời thường. Mình cực kỳ thích màn "thẩm vấn" của ông thẩm phán Gagnon với Bobby.
Về mặt nội dung, tác giả rất khéo léo khi che đậy những chi tiết đắt giá cho tới phút chót, và câu hỏi lớn chứ chờn vờn trong đầu mình từ đầu đến cuối truyện là ai mới là người nói dối. Có lẽ điểm trừ duy nhất của quyển sách là thiếu đi một vài cao trào thật sự để tạo điểm nhấn đủ mạnh mẽ cho tác phẩm.
4
494249
2015-06-04 23:34:44
--------------------------
203851
9861
Ngay từ đoạn đầu truyện đã mở ra một câu hỏi khiến người đọc phải suy luận theo từng dòng chữ từng trang giấy, rốt cuộc ai mới là người có lỗi trong "tình huống" đó, liệu đó là sự cố ngẫu nhiên hay là do bàn tay con người sắp đặt? Cách phân tích tâm lý nhân vật có thể nói chính là điểm nhấn cho cuốn sách này. Thật khó để biết liệu nhân vật đó rốt cuộc là người bị hại hay chính là kẻ chủ mưu, liệu người đó đã làm đúng hay sai. Những tình tiết dồn dập khiến cuốn sách trở nên gây cấn và người đọc chỉ muốn tiếp tục khám phá ra những bí ẩn chứa đựng trong đó.
5
471112
2015-06-02 14:41:50
--------------------------
198394
9861
Đây không hẳn là một cuốn sách trinh thám, đây còn là một cuốn sách về tâm lý. Không hẳn là thuần trinh thám vì chúng ta luôn biết "kẻ giết người" là ai, chẳng cần phải điều tra; nhưng lại đau đầu vì "tình huống" xảy ra sự việc đó. Mới đầu đọc, rõ ràng chỉ nghĩ Bobby thực sự chỉ làm công việc của một cảnh sát cần làm. Nhưng càng đọc đến những biến chuyển tâm lý, thì lại cảm thấy có gì đó vừa đúng vừa không đúng. Còn Cat, lúc đầu mang đến cảm giác, cô ta rõ ràng rất mờ ám, nhưng càng đọc, lại thấy cô ta không có gì không đúng. Những biến chuyển tâm lý lẫn những biến chuyển sự kiện dồn dập khiến câu chuyện càng trở nên khó phán đoán cho đến trang cuối cùng.
Một tác phẩm đáng đọc!
4
291719
2015-05-19 21:21:07
--------------------------
78685
9861
Tôi luôn có niềm yêu thích với những truyện trinh thám, nó không quá nhẹ nhàng nhưng đôi lúc lại đòi hỏi khả năng tư duy của con người, truyện luôn mang màu sắc bí ẩn nhưng khi tìm thấy câu trả lời, ta lại thấy thật nhẹ nhõm.
Đến với truyện lần này cũng vậy, ngay từ những trang đầu tiên, truyện đã thu hút tôi một cách kỳ lạ, tôi cứ liên tục lật nhanh từng trang để thỏa trí tò mò của mình. Đúng là một tác phẩm bán chạy, tôi thích cách tác giả tạo ra những nút thắt rồi lại gỡ nó ra một cách hợp lý, cách mà tác giả khắc họa tâm lý nhân vật thật thần kỳ, mỗi người đều bí ẩn và chờ người đọc khám phá, lời văn mạch lạc, cốt truyện hấp dẫn, truyện như thôi thúc người đọc tìm ra lời giải đáp cuối cùng.

4
57869
2013-06-02 19:41:07
--------------------------
71742
9861
Đa số những cuốn sách mà tôi đã đọc đều có một mở đầu khá lang mang vì dù gì thì đó cũng là một phần giới thiệu đẻ người đọc quen dần với mạch truyện và thường những đoạn giới thiệu ấy thường làm cho người đọc cho cảm giác khó chịu và nhàm chán nhưng Đơn Độc lại là một ngoại lệ.
Tôi bắt đầu cảm thấy hào hứng khi đọc đơn độc ngay từ khi cầm trên tay Đơn Độc.
Một cuốn truyện trinh thám với nhiều tình tiết đan xen khá thú vị, mạch truyện dồn dập, hồi hộp khiến người đọc không thể dừng sách. Đơn độc đã thành công khi khiến cho người đọc tò mò vì những tình tiết quá khó đoán trước cũng những tình huống hóc búa mà nếu bỏ sách xuống mà chưa biết lời giải thì bạn sẽ khó lòng mà ăn ngon, ngủ yên.
Tôi tự cho Đơn độc điểm tối đa vì nó không chỉ là một cuốn sách trinh thám mà còn là một cuốn sách kích thích trí thông minh, tư duy và sự tò mò của nhiều người :)
5
104997
2013-04-28 15:44:57
--------------------------
49847
9861
Đây là một cuốn sách trinh thám không đơn giản để hiểu ngay, mà trái lại, khá phức tạp trong các tình huống cũng như cách dẫn dắt câu chuyện. Tác giả đã tạo cho mỗi nhan vật một vẻ bí ẩn khó nắm bắt, khiến người đọc phải đi sâu và tìm hiểu kỹ mới có thể hiểu xem "vấn đề" gì đang xảy ra bên trong con người họ. Càng lật về những trang sau cuốn sách càng hấp dẫn khi các tình huống được đẩy lên cao trào, khi sự lựa chọn sống-chết được đưa ra, khi tâm lý nhân vật càng trở nên lắt léo và bí hiểm hơn bao giờ hết. Tóm lại, đây là một cuốn sách rất đáng đọc cho những bạn đam mêm thể loại này, cũng như những bạn muốn tìm cảm giác mới lạ trong văn học.
4
20073
2012-12-10 14:47:56
--------------------------
42113
9861
Mỗi khi đọc một cuốn sách mới, tôi thường cho phép trong 100 trang đầu tiên sẽ đọc hết cho kỳ được dù hay hoặc dở để xem nó có thu hút mình đọc tiếp hay không. Tuy nhiên với cuốn sách này, tôi thấy cũng chẳng cần đến 100 trang vì ngay vài chục trang đầu, mạch truyện đã đủ lôi cuốn trí tò mò của tôi. Cái hay của truyện là tác giả đối với mỗi nhân vật của mình đều đưa ra vài sự kiện chính yếu trong cuộc đời người đó để rồi khi lồng vào hiện tại tạo khúc mắc cho độc giả ở chỗ mỗi nhân vật đều có khả năng "làm điều ác". Rồi chính vì vậy mà độc giả lại càng muốn xem kỹ hơn từng hành động hay mẩu đối thoại của từng nhân vật sau đó để có sự suy đoán cho riêng mình. Tôi đọc đến 200 trang vẫn chưa xác định được 2 nhân vật chính Lisa và Bobby thật sự "có vấn đề" hay không nữa. Cuốn sách này thật sự xứng đáng với những lời khen tặng được trích dẫn ở trên.
4
52407
2012-10-06 17:29:12
--------------------------
