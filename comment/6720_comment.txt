315193
6720
Tôi tình cờ được biết đến quyển sách này trên tivi, thông qua VTV sách. Đây là tác phẩm của nhà báo Nguyễn Triều-một cây bút lâu năm và dạn dày kinh nghiệm. Quyển sách là tập hợp những bài viết mang tính châm biếm về nhiều khía cạnh trong cuộc sống, được phô diễn bởi 2 hình thức chính là thơ và tản văn ngắn, với lối viết nhẹ nhàng và không kém phần hài hước, tác phẩm rất thích hợp để giải trí trong lúc trà dư tửu hậu. Tuy nhiên điều đáng tiếc là các hình ảnh minh họa trong sách có đôi nét vụng về và cẩu thả, chất liệu giấy in sách là loại giấy xốp hơi ngả màu vàng, do đó sách nhìn không được trang trọng cho lắm, bên cạnh đó, giá quyển sách được niêm yết là 68.000đ, giá hơi cao so với những gì mà quyển sách mạng lại cho người đọc, mong ràng trong những lần tái bản sau đó công ty phát hành sách Phương Đông sẽ rút kinh nghiệm để mang đến cho bạn đọc một tác phẩm chỉnh chu hơn.
Tái bút: xin được phép trích dẫn một cụm thơ ngắn của tác giả
"Đàn ông nông cạn giếng thơi
Đàn bà sâu sắc như cơi đựng trầu
Giếng thơi chết được ai đâu
Anh hùng, vua chúa cơi trầu khó qua"
3
469919
2015-09-28 13:44:09
--------------------------
