374751
9832
Ấn tượng bởi nhân vật cô ca sĩ QT. Sách của Nguyễn Quỳnh Trang không dành cho những cô nàng hay mơ mộng. Có lẽ mình là người trầm tính nên luôn thích những nỗi cô đơn trong giọng văn của tác giả. Mình cũng từng có một người bạn thân khác giới giống như tình bạn của Jona và Kun, để rồi cũng lạc lõng đến cùng cực khi tình bạn ấy tan vỡ. 
Mỗi ngày vài chương truyện, chậm rãi cảm nhận từng cung bậc cảm xúc của nhân vật, để thấy rõ hơn cái tình giữa người lạ thành quen, và cái phũ của những người quen thành lạ.
Hơi khó để hiểu được sách nếu chưa từng trải qua cô đơn :)
4
122324
2016-01-26 23:41:45
--------------------------
60180
9832
Tôi 17 tuổi và đang cầm trong tay quyển sách này. Thật sự đọc xong tôi cảm thấy có phần chơi vơi và thiếu thiếu chút gì đó. Có lẽ tuổi 17 chưa có thể cảm nhận được hết chăng? Tôi rất thích cách viết truyện của cô Quỳnh Trang, có gì đó mơ hồ và thu hút người đọc. Những nhân vật trong truyện đều mang 1 màu sắc riêng và có những cách sống rất khác nhau. Họ trưởng thành về tuổi tác nhưng dường như vẫn còn non nớt về tinh thần. Cuộc sống là thế, có những con người vẫn luôn mang bên mình cái vỏ bọc của sĩ diện, tự trọng mà quên khuấy đi những điều tưởng chừng như quá quen thuộc kề bên. Bởi lẽ, kí ức trong họ phôi pha chỉ trong nháy mắt. Họ như những đứa trẻ 7 tuổi mắc kẹt trong mớ tâm lí và kí ức nhập nhằng của lứa tuổi 30. Qua đó ta có thể thấy thông điệp "Mất kí ức" mang lại có giá trị nhân văn khá cao và rót vào tâm hồn mỗi người những dòng suy nghĩ sâu sắc cùng những lẽ sống cần biết và trân trọng...
3
36331
2013-02-21 11:47:57
--------------------------
52953
9832
"Mất ký ức" là một trong những tác phẩm mình yêu thích nhất của nhà văn Nguyễn Quỳnh Trang. Tác phẩm này của chị thực sự mang một sắc màu mới mẻ, với cách kể và diễn tả độc đáo, cùng những suy nghĩ rất riêng biệt và giàu cá tính. Từ đầu đến cuối truyện, ta bắt gặp những gương mặt, những con người, những dòng hồi tưởng miên man, bất tận. Tất cả những điều này hòa quyện vào nhau, gắn kết bởi những câu văn giàu chất trí tuệ, tạo nên sự lôi cuốn cho người đọc. Dường như mỗi chi tiết trong truyện đều quá chân thực, sống động, đều đang nói với chúng ta những điều của cuộc sống này. Đây thực sự là một cuốn sách thú vị, một câu chuyện giàu tính nhân văn, rất đáng để thưởng thức.
4
20073
2012-12-29 15:19:15
--------------------------
