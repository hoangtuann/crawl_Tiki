346641
10737
Một tiểu thuyết nổi tiếng của nền văn học Úc. Tình yêu luôn là đề tài muôn thuở của văn chương nhân loại. Đọc cuốn sách này mình nhận thấy đã là phụ nữ thì phải biết yêu quý bản thân mình. Phải biết chỉnh chu về ngoại hình, biết chăm sóc cho nó, phải tự tin, độc lập tài chính. Để không bị phụ thuộc vào đàn ông, vì đàn ông họ yêu bằng con mắt. Khi con mắt họ không yêu thì phụ nữ sẽ dễ bị phu thuộc vào họ. Như nhân vật nữ chính trong tác phẩm, khi cô đã phẫu thuật thẩm mỹ để thay đổi gương mặt của mình bỗng trở nên xinh đẹp hơn, thì người chồng của cô lại thấy cô hết sức quyến rũ và muốn được chinh phục cô. Cốt truyện không mới nhưng cách viết của tác giả khiến cho người đọc bị cuốn hút vào từng câu chữ. Mặc dù có đôi chỗ giọng văn hơi dài nhưng tóm lại đây vẫn là tác phẩm đáng để bạn mua.
3
936274
2015-12-03 06:55:11
--------------------------
276416
10737
Mình rất thích đọc những tác phẩm có nội dung như câu chuyện này. Có thể khi bạn đọc sẽ thấy nội dung của truyện đã bắt gặp ở nhiều phim truyền hình hiện nay, đó là những cô gái yếu đuối, lương thiện, dễ tin người nên thường bị lợi dụng và bị hãm hại, nhưng nhờ vào sự kiên cường của mình mà họ đã thay đổi để trả thù những kẻ gieo sự bất hạnh cho cuộc đời mình. Trở về Eden cũng không nằm ngoài mô tuýt như vậy, nhưng với cách xây dựng tình huống và tính cách của nhân vật mà tác giả đã luôn tạo ra sự bất ngờ cho người đọc cho đến cuối câu chuyện và chắc chắn bạn sẽ thấy thỏa mãn với kết thúc của tác phẩm. 
4
194744
2015-08-23 23:07:11
--------------------------
256842
10737
Đọc rồi mới hiểu rõ hơn cái câu nói được copy paste nhiều lần trên các diễn đàn, các trang mạng xã hội rằng: Là phụ nữ nhất định phải giảm cân, phải đẹp, và phải yêu bản thân mình trước, phải có một công việc để làm để luôn được tự do là chính mình.
Chẳng có ai tốt với mình hơn chính bản thân mình, chẳng ai hiểu rõ mình hơn chính bản thân mình, và chẳng ai có quyền phán xét mình nếu mình chưa cho phép. Mình thích cách xây dựng nhân vật của tác giả, từ một cô gái nhẹ dạ, cả tin, yếu đuối về tình cảm đã vực lên từ những nỗi đau tưởng chừng như vô tận, tình cảm là thứ người ta khó mà điều khiển được, và đi kèm với nó là nỗi đau, có mấy ai đủ mạnh mẽ để vực lại tất cả sau những tổn thương tinh thần tựa như cả hàng ngàn tấn nặng đè bẹp trái tim.
Bạn thân, chồng, hay người yêu, tất cả là một cuộc chiến, từ thế hệ này đến thế hệ khác. Dục vọng xâm chiếm tinh thần, thế mới hiểu, con người yếu đuối nhất không phải là khi phá sản, không phải khi thất nghiệp, không phải khi mất một thứ gì đó, một ai đó, mà là khi không thể làm chủ được chính tình cảm của bản thân.
Cũng cùng là một con người, nhưng một tình yêu chân thành đôi khi vẫn chưa đủ với một người đàn ông, những kẻ sống bằng thân dưới, như nhân vật chính, khi cô béo xù xì với đầy những ngấn mỡ, sự già nua vẫn hiện hữu dù cho lòng chân thành có lớn đến bao nhiêu, và khi cô xinh đẹp kiêu hãnh bên một thân hình khỏe mạnh, thì cả sự chân thành cũng đôi khi bị che khuất.
Người ta sẽ nhìn thấy có tới cả tỉ điều mâu thuẫn xuyên suốt cả câu chuyện, lý trí và trái tim đấu tranh vật vã, cả và sự ngờ vực. Vậy rốt cuộc con người sống với nhau, sự chân thành từ trái tim đôi khi vẫn chưa đủ hay sao?
Mình không đọc được hết tác phẩm, vì quá nhiều cảnh nóng và ngôn từ quá thô, quá thật, nếu lược bỏ những trang sách ấy có lẽ cuốn sách sẽ hợp với thế hệ trẻ Việt Nam hơn, phong cách và lối sống phương Tây phức tạp quá, mình thích lời giới thiệu tác phẩm và rất tò mò về kết thúc nên đã đặt mua, nhưng mình còn chẳng dám giữ lại trên kệ sách vì nhà có em nhỏ. Có lẽ cuốn sách hợp với những người đã có gia đình hơn.
2
204039
2015-08-07 10:39:14
--------------------------
192091
10737
Nhân vật chính - Stephany Harper thực sự là một người phụ nữ mạnh mẽ, với cái mốc ban đầu chỉ là con gái của một nhà tư bản giàu nhất nhì nước Úc, thừa kế tài sản cha để lại, ngây thơ và dễ tin vào đàn ông - khiến sau này chị bị lợi dụng và hãm hại. Nhưng sau đó, chị đã đứng dậy, trở về trả thù những kẻ gây ra đau khổ cho mình trước đây, trả thù bằng một cách ngọt nhất mà cũng cay nhất. Từ đầu đến cuối truyện, tính cách mạnh mẽ của chị bộc lộ ngày càng rõ - một nhân vật nữ điển hình.
Nhưng thực sự thì mình không thích lối sống phương Tây cho lắm. Mặc dù biết rõ phương Tây phóng khoáng, tự do, nhưng đọc tác phẩm, khi đến những cảnh nóng - mà quá nhiều, mình hơi rùng mình. Đặc biệt, mình không thích nữ chính ở chỗ, cô dễ tin đàn ông quá mức, và đến kết thúc truyện mình nhẩm đi nhẩm lại, ôi trời, cô ngủ với tất cả 6 người đàn ông, trong đó lại có hai anh em ruột nữa chứ. Thật sự là mình thấy hơi...
Bìa sách đẹp.
3
220900
2015-05-03 16:57:12
--------------------------
163770
10737
Cuốn sách thuộc dạng tình cảm tâm lí tuy nhiên sự trả thù và thù hận khiến tôi có một chút không thích. Tuy nhiên nếu trong trường hợp của cô gái thì tôi đúng là muốn trả thù thật nhưng tôi thích cái kết câu chuyện:một kết cục có hậu. Tuy nhiên sự thù hận khiến tính nhân văn của tác phẩm giảm đi một cách đáng kể. Song dù sao đi nữa, tác phẩm vẫn là một cách để ta hiểu được những khía cạnh quanh cuộc sống do tác giả mang đến. Mặc dù vậy, lỗi bản in khiến sự hấp dẫn câu chuyện giảm đi đáng kể đấy ==
3
566401
2015-03-05 21:19:12
--------------------------
144771
10737
Đọc Trở về Eden, một lần nữa tôi phải công nhận rằng những cuốn sách có nội dung trả thù đều rất hấp dẫn, lôi cuốn độc giả đến từng câu chữ ( như Nếu còn có ngày mai, Bố già, Bá tước Monte Cristo...). Lúc đầu đọc thấy Tara Well đã quay trở lại là Stephany mà vẫn đang còn đến nửa quyển, hóa ra có đến 2 cuộc đấu tranh đầy mất mát xoay quanh tình yêu, tiền bạc và dục vọng ở đây. Tác phẩm có một số thông điệp ý nghĩa, giọng văn gần gũi. Tuy nhiên còn một số chi tiết làm người đọc khá bực mình bởi có nhiều tình huống Stephany hành động một cách vô lí. Ví dụ như cô đã k chắc chắn rằng cảnh sát sẽ đến mà đã cho Greg biết sự thật ở vườn Eden, kế hoạch hoàn hảo phạm sai lầm ở phút cuối và chắc thành công được chỉ là do may mắn. Ngoài ra thì tôi thấy bản in này còn sai sót về lỗi chính tả, tên riêng viết không thống nhất lúc Tây lúc ta, lời dẫn chưa xuống dòng đã đến lời thoại,..không phải lặp lại 1 mà rất nhiều lần.
3
468950
2014-12-27 23:37:18
--------------------------
118765
10737
Cốt truyện của "Trở về Eden" thuộc dạng kinh điển, kết thúc có hậu, không có tình tiết bất ngờ. Cũng có vài tình tiết chưa hợp lý lắm, trong đó đáng chú ý nhất là việc Stefanie là một doanh nhân sừng sỏ, điều hành cả tập đoàn lớn, mà lại không biết cách nhìn người, giao tiếp, và thậm chí quá ngây thơ như vậy. Bản in này của NXB Văn Học có quá nhiều lỗi, một khuyết điểm lớn của khâu biên tập. Tôi đánh giá truyện này ở mức trung bình, nhưng những lỗi in một cách quá coi thường độc giả trong bản này làm cho nó trở nên dưới mức trung bình.
2
3406
2014-07-31 09:26:07
--------------------------
48828
10737
"Trở về Eden" giống như một bộ phim với những cảnh quay đẹp, gợi tả được nhiều góc cạnh của không gian, thời gian, tâm lý con người. Tác giả dẫn mọi người vào một thế giới tươi đẹp đầy hạnh phúc, rồi đột ngột làm họ bất ngờ với những tình huống xấu xảy đến. Chính ngôn ngữ lôi cuốn và một cốt truyện chứa đựng nhiều bất ngờ đã làm cuốn sách trở nên hấp dẫn với cả những ai không quen thuộc với thể loại truyện này. Không chỉ kể về một cuộc trả thù người chồng bội bạc, kẻ vô tâm đã đẩy Stephany vào bể khổ, cuốn sách còn là một bức tranh sinh động về con người, với những khám phá tuyệt vời về ranh giới giữa thiện - ác, trắng - đen, yêu - ghét. "Trở về Eden" thực sự hay và đáng đọc. Một cuốn sách không nên bỏ lỡ.
5
20073
2012-12-03 16:31:08
--------------------------
35536
10737
Đây là một quyển sách có nội dung khá ấn tượng, một cuốc trả thù mà theo tôi là khá ngoạn mục. Tuy nhiên tôi vẫn cảm thấy nó có phần không thực cho lắm, vì một người chồng mà lại không thể nhận ra được vợ mình dù rằng cô ấy đã phẫu thuật thẩm mỹ thay đổi khuôn mặt thì quả là cũng hơi khó tin. Tôi có thể thấy được tác giả đã rất cố gắng để cho ta một tác phẩm hay, tuy nhiên với giọng văn chậm, và nhiều lúc là dài dòng bạn sẽ dễ dàng cảm thấy chán với tác phẩm này. Lúc mới mua về tôi chỉ  đọc phân nửa truyện mà thôi, gần 800 trang cho một cuộc trả thù với rất ít tình tiết thì quả thật là khá dài. Có thể nói tác phẩm không hay bằng bộ phim cùng tên được thật hiện. Theo tôi thì đây không phải là một tác phẩm xuất sắc. 
5
17940
2012-08-01 14:45:16
--------------------------
