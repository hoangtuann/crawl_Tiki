465492
5251
Khi mình nhận được sách thì mình nhận thấy bìa sách hơi cũ, có chỗ bị mòn, có lẽ do đã để lâu trong kho, tuy nhiên, các trang bên trong vẫn mới nên vẫn chấp nhận được.
Sách có nhiều đề và cả đáp án nên có thể luyện tập được nhiều và đối chiếu dễ dàng. Nhưng các hình thức bài thi trắc nghiệm còn ít, chưa phong phú (chỉ có 3 dạng) nên mình chưa luyện được nhiều dạng bài. Và mình còn phát hiện có những đáp án trong sách chưa chính xác. Nhìn chung thì đây cũng là một cuốn sách khá hữu ích để rèn luyện thêm tiếng Anh.
3
1370609
2016-07-01 08:04:09
--------------------------
417877
5251
Mình có rất nhiều sách của The Windy. Và đây là một trong những cuốn sách tiếng anh tốt nhất trên thị trường. Sách của The Windy luôn có cái gì đó rất khác với các cuốn sách đang bày bán tràn lan trôi nổi của các dòng sách khác. Nếu như được lựa chọn mình vẫn luôn tin dùng sách của The Windy. Mình đã luyện thi xong trình độ A. Cuốn sách có những đề thi dạng trắc nghiệm rất hay và bổ ích giúp ích chúng ta trau dồi và cũng cố thêm kiến thức. Cảm ơn The Windy và Tiki đã mang đến cho đọc giả những quyển sách tuyệt vời,
5
509425
2016-04-18 00:06:29
--------------------------
385502
5251
Cuốn sách Tổng Hợp Trắc Nghiệm Tiếng Anh Trình Độ B (Có Đáp Án) là cuốn sách của The Windy MC Book - Nhà xuất bản đại học Quốc gia Hà Nội thì luôn yên tâm về chất lượng. Bìa sách gập cứng vừa phải, giấy trắng, mực in rõ không bị nhòe, cuốn sách dày nhất trong bộ 3 quyển A,B,C này (hơn 450 trang gần 500 trang). Nội dung phong phú đa dạng các kiểu bài tập. Quyển sách có 70 đề thi => rất nhiều. Đó là phần 1 còn phần 2 thì có thêm 600 câu hỏi luyện thêm nữa. Mình thấy phù hợp với năng lực những người học tiếng Anh ở trình độ B. 
5
854660
2016-02-24 06:20:40
--------------------------
287124
5251
Về chất lượng: quyển sách này thuộc công ty sách The Windy nên tôi luôn hài lòng, giấy tốt, trình bày rõ ràng, dễ đọc. Các phần, các ý được trình bày đúng như mẫu đề thi trên lớp, có thể mang về dùng tham khảo và luyện tập thêm sau khi học.
Về nội dung: toàn bộ đều là đề trình bộ B nên nâng cao một chút so với quyển trình độ A, câu hỏi nhìn chung không khó, cũng được, khá phù hợp cho học sinh, sinh viên sử dụng. Tuy nhiên, nhiều câu không hiểu lắm nhưng đáp án không giải thích gì nhiều nên hơi hẫng. 
Tóm lại, đây là một quyển đề tham khảo khá tốt, nên mua về luyện tập để nâng cao trình độ tiếng Anh. :)
4
382812
2015-09-02 12:13:43
--------------------------
233024
5251
Mình nghĩ rằng với một cuốn sách học tiếng anh như vậy, lại còn của nhà xuất bản The Windy, thì chắc chắn giá tiền sẽ không rẻ chút nào. Nhưng mà khi trên Tiki có giảm giá, mình liền nhanh tay mua luôn một cuốn về. Mặc dù vậy nhưng điều mà mình thấy hài lòng nhất là Tiki gửi email xin lỗi về việc sách bị nhăn nhẹ, điều đó nghe thì rất nhỏ nhưng Tiki đã cư xử rất trách nhiệm và giữ đúng uy tín. Mua được cuốn sách về thì mới phát hiện ra, sách rất có giá trị, các dạng bài tập trắc nghiệm phong phú, đa dạng mà không quá khó với trình độ B, làm mình chỉ muốn cầm bút lên và làm luôn. Kết luận mà nói, đây là cuốn sách hay, tuyệt vời và cực phù hợp cho khả năng tiếng anh B.
5
676659
2015-07-19 14:25:38
--------------------------
225772
5251
Trước tiên mình rất ấn tượng với chất lượng giấy của sách.Sách có giấy dày,bìa dày.Không chỉ có vậy chữ in trong sách thì rất rõ ràng,không bị nhòe chữ.Còn về nội dung thì mình thực sự không còn gì để nói.Sách báo quát toàn bộ các dạng trắc nghiệm có thể ra trong kì thi chứng chỉ quốc gia.Các đề thi đều có đáp án đầy đủ.Tuy nhiên sách chỉ có điều khiến mình hơi thất vọng 1 tí là ở những câu khó,sách không có lời giải chi tiết nên cũng gây cho mình một chút khó khăn trong việc tìm hiểu lời giải.Nhưng xét về tổng thể thì đây là một quyển sách hay.
3
368991
2015-07-10 21:55:20
--------------------------
196138
5251
Sách được chuyển đến nơi mình rất đúng ngày, sách bọc gói rất cẩn thận và chất lượng khá tốt. Bìa sách dày, trang giấy in rõ ràng và đẹp, mực không bị nhòe. Hơn nữa, sách khá bổ ích với mình khi mình muốn củng cố trình độ tiếng anh B vì sách có các kiểu bài rất hay, mình thích nhất là phần chữa lỗi sai và phát âm ở cuốn sách này! Phần thì tiếng anh và dạng từ cũng khá ổn, đáp án khá chuẩn. Các bài test luyện thi vừa tầm với trình độ B, 600 câu trắc nghiệm đa dạng. Mình rất hài lòng với sản phẩm sách này của tiki!
5
611652
2015-05-15 13:20:30
--------------------------
