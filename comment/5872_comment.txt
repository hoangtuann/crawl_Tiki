476321
5872
Sản phẩm đẹp trang sách vừa đủ , không mỏng mà cũng không dày. Tiki giao hàng rất êm , nhanh, sản phẩm được giữ cẩn thận . Sách nội dung hay vừa học tiếng Trung và học tiếng anh lun , công nhận sách hay . Và mang thêm nhiều niềm hứng thú cho người mới bắt đầu vào con đường học tiếng Trung . Thỏa mãn đam mê đi du lịch ở nước ngoài đặc biệt là Trung Quốc . Sách trình bày logic và trang trọng giúp người học không chán trong lúc học . Tôi đã mua và đang học theo những kiến thức quý giá trên sách . Cuốn sách đầu tiên mình thấy trình bày phong phú và đẹp cũng như nội dung rất hay.
4
1510035
2016-07-19 21:29:45
--------------------------
308933
5872
Mình đang học tiếng trung nên ra nhà sách tìm mua mấy cuốn về học, thật ra sách ngoài thị trường rất nhiều nên hơi khó chọn, hơn nữa, giá cả không giảm giá nên khá mắc. May sao lên tiki thấy có cuốn này của the zhishi. Phải nói cuốn tuy nhỏ nhưng hình thức đẹp, nội dung sắp xếp hợp lý. Nhìn rất thẩm mỹ và có giá trị. Tuy nhiên, chất lương giấy in chắc không tốt lắm nên đôi chỗ sách bị mờ. Còn lại thì mình đều hài lòng, đây là cuốn sách hữu ích cho các bạn mới học tiếng trung như mình.
5
492391
2015-09-18 21:38:25
--------------------------
