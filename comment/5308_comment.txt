289426
5308
Sách in rất đẹp, mình đặc biệt thích những hình minh họa nho nhỏ ở mỗi trang sách, nhìn rất đáng yêu, tăng thêm hứng thú khi đọc. Đây là sách cho thiếu nhi, nhưng mình thấy cũng rất hữu ích cho người lớn. Đúng như tựa sách, đây là tập hợp những câu chuyện hay nhất, cảm động nhất về tình cảm gia đình, tình cha mẹ, tình anh em, bạn bè, thầy trò. Nhiều câu chuyện được kể rất ngắn gọn nhưng có ý nghĩa sâu sắc, và làm mình rất xúc động. Cuốn sách cho mình thấy cuộc sống này thật đẹp biết bao.
5
476955
2015-09-04 14:37:14
--------------------------
