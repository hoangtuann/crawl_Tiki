472569
7387
Mệnh phượng hoàng là một trong số những truyện cổ đại cung đấu hay nhất mà mình từng đọc, bao gồm phượng ẩn thiên hạ, phượng tù hoàng! Khác với nam chính trong hai truyện kia, Hạ Hầu Tử Khâm có vẻ dễ thương hơn, nhưng TK lại có con với phi tần khác, dù anh là vua nên chuyện này là bình thường nhưng mình không thích lắm, cả việc anh cứ day dứt về Dao Phi làm mình cũng hơi khó chịu hic, nhưng nói chung anh vẫn là một nam chính khá tuyệt. Nữ chính Tang Tử cũng giống như Hoa Trứ Vũ và Sở Ngọc, đều là dạng nữ cường thông minh! Mình thích nữ chính dạng này! Rất thích! Tang Tử không hề yếu đuối chút nào, TT và TK như một đôi trời sinh, họ tin tưởng nhau tuyệt đối, mình thích một tình yêu như thế. 
Có một điều không thích là trong bốn quyển được giao cho mình chỉ có quyển này là loại giấy vàng, lại còn bị ố rất nhiều, như second hand í, buồn dã man
4
900041
2016-07-09 19:43:59
--------------------------
444539
7387
Thấy các bạn nói không đọc đến giữa tập 2 thì hơi nản.k muốn đọc nữa nhưng mình thấy nó đâu đến nỗi nào đâu.Chắc do cảm nhận của mỗi người rồi...Tuy đọc nhận xét của mọi người về tập này là hơi nản nhưng mình vẫn cố chấp tìm mua cùng với cả mấy tập nhưng giờ không cảm thấy hối tiếc chút nào.Ở tập này đúng là có hơi nan man một chút nhưng chỉ một chút thôi mà.Có sự xuất hiện của Phất Dao khiến cho tập này vẫn rất lôi cuốn mình .Tập này mình thấy tình yêu của 2 nhân vật len lói .Bắt đầu rõ nét hơn.Mình sẽ tiếp tục đọc tập còn lại 
5
1124269
2016-06-08 16:43:06
--------------------------
434153
7387
Tập này quá tuyệt vời, cuộc tranh đấu càng ngày càng gây gắt giữa các cung phi với nhau. Diêu Thục Phi xảy thai nhìn thì bình thường nhưng thật ra trong đó là cả một âm mưu to lớn được chuẩn bị từ rất lâu. Tang Tử và Hạ Hầu Tử Khâm luôn bên nhau dù cho nó chỉ là những khoảng thời gian chớp nhoáng nhưng theo tôi thấy thì Tử Khâm có lẽ thật sự có tình cảm với Tang Tử, vì chỉ khi nào chàng ở bên nàng chàng mới bộc lộ một vài cảm xúc nhưng giận dỗi, làm nũng,...với mình nàng mà thôi. Cuối tập này Tang Tử ngã xuống vách Nam Sơn, Tử Khâm đã gọi "Đàn Phi" với giọng pha lẫn cảm giác tan nát cõi lòng, không biết tập 3 sẽ ra sao, tôi rất mong chờ!
5
672200
2016-05-22 13:46:28
--------------------------
405572
7387
Tập hai khắc họa rõ nét hơn về nhân vật Thái Hậu và Tang Tử cũng có vẻ đã lên tay. Sự xuất hiện trở lại của Phất Hy có vẻ như sẽ làm cho cặp ba này mệt hơn, nhwung chắc sẽ làm cho Hạ Hầu Tử Khâm thấy rõ hơn tình cảm của mình đối với Đàn phi. Và cả Tang Tử cũng vậy. Có vẻ như Tô Mộ Hàn đã xuất hiện với một thân phận khác và một cái tên hoàn toàn mới lạ. Tác giả viết truyện rất cuốn hút, làm ta say mê không bỏ sách xuống được. Phải đọc ngay tập 3 đây!
5
724772
2016-03-26 22:18:25
--------------------------
340935
7387
Quả thật khi qua tập 2 mình thấy hơi tác giả hơi đuối, hầu như những chi tiết vẫn không có gì đặc sắc, thậm chí còn cảm thấy hơi dông dài, nhưng chính vì thế mới có những tình tiết hấp dẫn đằng sau. Giờ mình đã hiểu nếu không theo dõi hết thì sẽ  không thể biết nó hay dở như thế nào. Từ lúc Hàn Vương xuất hiện, mình đã thấy các tình tiết được đẩy lên tới cao trào, rồi còn cả Dao Phi nữa, mình cũng đã lờ mờ đoán được thân phận của Hàn Vương. Mình sẽ theo tiếp tập 3 xem diễn biến như nào. 
5
688967
2015-11-20 21:05:59
--------------------------
313016
7387
phần hai trong bộ truyện này chủ yếu tập trung miêu tả lại cuộc sống đầy khó khăn, bon chen trong hoàng cung đầy khắc nghiệt mà mỗi khi nhắc đến, ai cũng phải dè chừng. Thế nhưng Tang Tử cũng rất thông minh mà bạn sẽ cảm nhận được qua từng suy đoán rất khôn ngoan, sắc sảo của cô ấy. Dù sống giữa môi trường như vậy nhưng cô cũng không hề bỉ ổi, ti tiện như bọn người hèn nhát, yếu đuối và dã tâm chút nào. Lối viết văn lôi cuốn cùng cốt truyện hấp dẫn chắc chắn sẽ không làm bạn thất vọng đâu!
5
636935
2015-09-22 17:21:13
--------------------------
309444
7387
Tập 2 có nhiều tình tiết cao trào hơn tập 1 nhưng vẫn giữ vững phong đô, không mất đi cái chất hay của truyện. Tập này đã xuất hiện những mâu thuẫn giữa những nhân vật chính làm cho truyện càng lúc càng sinh động hơn, khắc họa chân thật hơn cuộc sống khắc nghiệt nơi cung cấm. Tang Tử là một người con gái rất quyết đoán và thông minh nên mình tin chắc nàng sẽ có nhiều kế sách mà đối đầu với Phất Dao. Rất mong chờ vào tập 3 và mong tốc độ truyện đi nhanh hơn tí vì thế này thì có hơi chậm rồi.
5
536908
2015-09-18 23:43:13
--------------------------
301794
7387
Với một truyện cung đấu mà nói, tình tiết của mệnh phượng hoàng có thể nói là quá đơn giản? Tình cảm quá nhạt nhòa đến nỗi mình không thể hình dung tại sao lại nảy sinh tình yêu? Là một người có tham vọng vượt qua số phận, trèo lên cao làm phượng hoàng, có vẻ như nữ chính quá yếu đuối? Thực ra bản thân mình thấy việc này khá thực tế, nhưng đem tình tiết như vậy vào truyện khiến mình cảm giác cốt truyện bị nhạt đi, khá loãng. Thân phận của Tô Mộ Hàn mình đã đoán ra ngay từ khi đọc tập1 đoạn Thái tử chết. Chẳng hiểu sao nghe câu Thái tử chết là mình đã cảm nhận được có việc đằng sau sự thật ấy. Tình tiết truyện phải nói là quá dễ đoán....
3
192908
2015-09-14 20:52:05
--------------------------
278582
7387
Đây là truyện cung đấu đầu tiên mình đọc, ngày xưa tránh thể loại này như tránh tà vậy mà đọc Mệnh Phượng Hoàng rồi mê không biết đường về. Sở dĩ mình không thích cung đấu chính vì nam chính đa phần là vua, mà vua thì ân trạch ban phát muôn nơi, nhưng truyện này làm mình thay đổi hoàn toàn suy nghĩ về tình yêu của đế vương. 
Hạ Hầu Tử Khâm - Yêu mà như không yêu, yêu mà để cho người mình yêu tự học cách sống trong chốn thâm cung đầy toan tính. Yêu là đầy người yêu vào lãnh cung. Chắc vì cái cách yêu khác người độc nhất vô nhị này của chàng nên chàng mới là một trong 3 vị vua để lại ấn tượng sâu đậm và yêu thích nhất của mình. Đọc hết truyện nhớ nhất câu của vị hoàng đế nước láng giềng Quân Ngạn "Hãy tin tưởng hoàng gia cũng có chân tình. Tam cung lục viện không phải là niềm hy vọng của một đế vương nhưng chân tình thì ai cũng hy vọng có được." Cũng vì đây là lời của hoàng đế khác nói về nỗi lòng hoàng đế nên mới khiến mình thỏa mãn đến vậy.
5
270977
2015-08-25 23:00:44
--------------------------
272001
7387
Tập 2 thực sự hấp dẫn hơn tập 1, nếu tập 1 chỉ có cung đấu thì ở tập 2 này những bí mật quá khứ về Phất Hy - người Hạ Hầu Tử Khâm từng yêu và Tô Mộ Hàn dần dần được hé lộ. Tình cảm của Hạ Hầu Tử Khâm và Tang Tử dần trở nên thân thiết, sâu đậm hơn nhưng giữa hai người lại xuất hiện một bức tường ngăn cản đó chính là Phất Dao - em gái giống hệt chị mình Phất Hy. Nhưng khi HHTK nói "Nàng cũng không hiểu trẫm" thì mình đã biết được anh ta yêu Tang Tử rồi, yêu thật lòng rồi và mình bắt đầu mong chờ đoạn sau. Cuối tập khi Tang Tử ngã xuống vực cùng Hàn Vương thì mình càng khẳng định điều đó. Mình rất mong chờ tập 3.
5
678496
2015-08-19 17:29:12
--------------------------
264802
7387
Mệnh phượng hoàng là bộ truyện cung đấu rất hay, rất đáng đọc. Truyện kể về sự tranh đấu, tranh giành tình yêu của hoàng thượng giữa các vợ của vua. Đọc truyện này cảm thấy sự tranh giành quyết liệt, đấu đá nhau nhưng cũng vô cùng kín đáo và khôn khéo. Nam chính là hoàng thượng đẹp trai, lạnh lùng, mặc dù có nhiều vợ nhưng tình cảm của anh dành cho nữ chính rất nhiều. Nữ chính từ nhỏ đã không được gia đình coi trọng, cô được nói là có mệnh phượng hoàng nhưng mọi người đều cố giấu. Cô được một người chỉ điểm cho mình, càng lớn lên, cô càng xinh đẹp, vào cung và tình cờ trở thành vợ của vua cùng các chị của mình, từng bước, từng bước cô vượt qua họ, trở thành người phụ nữ quyền lực nhất trong hậu cung
5
451462
2015-08-13 14:37:10
--------------------------
225136
7387
Hay hơn tập 1. Thật sự mình cảm thấy tập 1 rất dở, mạch truyện thì vô lí. Sang tập này thì có nhiều tình tiết kịch tính hơn, nhiều bí mật cũng dần được hé mở, có cao trào hấp dẫn độc giả. Trong 4 tập của Mệnh Phượng Hoàng thì tập này có lẽ là tập hay nhất. Tình cảm của 2 nhân vật chính thì mới bắt đầu nhưng đã có hiểu lầm, tạo tình huống để có những tập sau. Riêng truyện này, mặc dù là cung đấu nhưng mình lại không thích nam chính. Có lẽ là vì nam chính xuất hiện sau và hơi mờ nhạt nên mình lại thích 2 nam phụ hơn. Tập này là được nhất trong 4 tập rồi, vì mình cảm giác như những tập sau tình tiết thật là dài dòng, thừa thãi. Hoặc có lẽ do mình không thích nữ chính =.=
3
212906
2015-07-09 21:32:21
--------------------------
207816
7387
Được – Tình yêu trong cung cấp là một điều mong manh nhưng vẫn khá đẹp. Mình thích đoạn Hạ Hầu Tử Khâm bình thường nghiêm túc, lý trí là vậy nhưng vẫn biết ghen ^ ^ tuy vậy, hắn vẫn cho nàng lựa chọn giữa sư phụ và hắn, có lẽ bởi vì hắn không chắc chắn cũng có thể đó là một phép thử khôn ngoan. Nếu ở tập 1 nhân vật còn khá nhạt với mình thì qua tập 2 ngoài hai nhân vật chính, các nhân vật khác còn nhạt hơn. Cuối tập còn xuất hiện Dao Phi - tình địch của Tang Tử. Nhận xét chung là tập 2 được hơn tập 1, có sự cao trào.
3
650262
2015-06-13 10:21:54
--------------------------
207088
7387
Tiếp nối tập một của Mệnh Phượng Hoàng, ở tập hai độc giả được thấy rõ hơn sự sắc xảo, thông minh cùng sự trưởng thành trong suy nghĩ của Tang Tử qua việc nàng xử lí chuyện chốn hậu cung cũng như cách đối nhân xử thế của nàng. Ở tập hai này, các âm mưa chốn hậu cung dần trở nên gay cấn hơn, thâm độc hơn nhưng Tang Tử vẫn xử lí tất cả một cách ổn thỏa. Thật là người con gái đầy bản lĩnh!!! Bên cạnh đó, chuyện tình giũa nàng và Hạ Hầu Tử Khâm cũng dần trở nên thân thiết, sâu đậm hơn nhưng lại có một bức tường ngăn cản Tang Tử đi vào tấm lòng của HHTK chính là mối tình đầu đã chết - Phất Hy và giờ là Phất Dao- em gái giống hệt Phất Hy. Cuối tập hai thì câu chuyện đã dần trở nên kịch tính hơn rồi. Ôi thật mong chờ để đọc tập 3!!!
5
190618
2015-06-11 13:56:59
--------------------------
187650
7387
Quả thật, mình đã hơi nản khi đọc đến giữa tập 2 của truyện vì không còn thấy nó hấp dẫn nữa. Nó bắt đầu nhàn nhạt, các nhân vật cứ vờn qua vờn lại mấy cái đấu đá hậu cung, không có gì sắc nét, nổi trội đáng mong chờ. Bởi nhân vật mình thích phải có sự rõ ràng của tình yêu, yêu hay ko yêu, hiểu hay ko hiểu, nếu chỉ hời hợt với nhau thì mình ko cần. Vậy mà đến cuối tập 2 khi Dao Phi xuất hiện, khi Tang Tử tát Hạ Hầu Tử Khâm, khi chàng nói "Nàng cũng không hiểu trẫm" thì mình đã biết được anh ta yêu Tang Tử rồi, yêu thật lòng rồi và mình bắt đầu mong chờ đoạn sau. Cuối tập khi Tang Tử ngã xuống vực cùng Hàn Vương thì mình càng khẳng định điều đó. Giờ thì không thể không tìm đọc tập 3 rồi nhé! (đang định bỏ, hi hi)
4
371434
2015-04-23 18:43:06
--------------------------
178339
7387
Tình yêu giữa Tử Khâm và Tang Tử dần rõ nét, nhưng lại phải đối mặt với một thử thách lớn, là cô gái Phất Dao giống hệt mối tình đầu của Tử Khâm. Trong tập này, các bí mật dần được hé lộ, những âm mưu rục rịch tiến hành. Nhưng có lẽ tập 2 chỉ đóng vai trò cầu nối, nên nhiều lúc cảm thấy nhịp truyện hơi chậm, không quá kịch tính. Đến cuối tập 2, mọi chuyện mới bắt đầu tăng tốc, khi Tử Khâm phong phi cho Phất Dao, tôi đã rất ấm ức cho Tang Tử, nhưng khi Tang tử rơi xuống vực, nghe tiếng gọi tan nát cõi lòng của Tử Khâm, tôi tin là chàng thực sự yêu Tang Tử.
4
57459
2015-04-04 21:02:35
--------------------------
166379
7387
Nếu tập 1 chỉ nhắc đến cuộc chiến nơi cung cấm thì ở tập 2 này, những bí mật trong quá khứ liên quan đến  cô gái Phất Hy đáng thương, đến Tô Mộ Hàn, các mối liên quan giữa họ với tiền triều và tâm tư của Thái hậu dần lộ ra, dự cảm còn rất nhiều ngạc nhiên hơn nữa ở các tập tiếp theo, mà tác giả chưa trình bày ra ngay, thậm chí, vẫn cái thái đổ chậm rãi, phân tích rõ ràng, sâu sắc và tinh tế của Tang Tử, lại khiến người đọc như mình thấy tò mò , tò mò, tò mò :3 
Không biết Hạ Hầu Tử Khâm với Tang Tử sau này sẽ thế nào, hi vọng anh nam chính này có thể chỉ thương một mình Tang Tử, vì mình không thích nam chính không rõ ràng chuyện tình cảm
4
310296
2015-03-12 16:33:07
--------------------------
163761
7387
Không có gì đặc biệt
Tôi thấy tập 2 này mọi diễn biến trong cung cấm diễn ra vẫn còn yên lặng quá. Dần dần nhiều bí ẩn, sơ hở lộ ra nhưng tác giả vẫn chưa giải thích vội, để dành cho tập sau mới rõ ràng. Xưa nay đều biết tình đầu khó quên vì thế Hạ Hầu Tử Khâm cũng không ngoại lệ. Bóng hình suốt 5 năm liệu có thể dễ dàng bỏ xuống được. Đáng tiếc nàng ta không yên phận như Tang Tử, mà mong muốn hoàng thượng có mình nàng ta, há sao có thể được. Đọc chỉ thấy càng đáng tiếc cho Phất Hy mà thôi
4
80511
2015-03-05 20:56:40
--------------------------
162545
7387
Ấn tượng bởi con người có sẵn những phẩm chất "phượng hoàng" của Tang Tử trong tập 1, mình rất tò mò xem ở tập 2, những phẩm chất ấy sẽ được bộc lộ như thế nào.
Thứ nhất là ở cuộc chiến chốn hậu cung, Tang Tử đã làm rất tốt, cộng thêm may mắn trời ban. Cô đã vô cùng cẩn trọng trong việc gặp gỡ Cố Khanh Hằng, nhưng vẫn luôn âm thầm bảo vệ huynh ấy. Việc phán đoán chính xác và xử lí xuất sắc trong vụ Diêu Thục nghi sảy thai đã giúp Tang Tử giành được một đồng minh rất lớn mạnh cho mình - Thái hậu. Tuy nhiên, mình không thích cách Tang Tử xử lí vụ Sơ Tuyết, đáng nhẽ ra nên nghe lời Phương Hàm mà giết người diệt khẩu thì hơn. Nếu đứng sau vụ Sơ Tuyết là một thế lực khác thì có lẽ Đàn phi đã sớm bị tống vào lãnh cung, tệ hơn là bị ban chết. Nhưng với kinh nghiệm còn yếu mà làm được thế thì cũng là khá tốt rồi.
Trong tập này, tình yêu giữa Hạ Hầu Tử Khâm và Tang Tử mới chớm nở, mình còn chưa kịp cảm nhận hết thì sóng gió đã tới. Hàn Vương - nhân vật bí ẩn đằng sau chiếc mặt nạ bạc là ai mà khiến trái tim Tang Tử tưởng chừng sắt đá phải run rẩy, nức lên từng hồi, khiến cô có những hành động vô cùng thiếu lí trí? Liệu nó có nguy hiểm không? Tình cảm của Hạ Hầu Tử Khâm cũng gặp phải cơn sóng lớn khi có sự xuất hiện trở lại của Phất Hy, bóng hình mà bảy năm qua hắn từng mong nhớ?
Nói chung, tập 2 có thể nói là tập hay nhất của bộ truyện này, rất li kì, và cuốn người xem nhập tâm vào câu chuyện.
4
333831
2015-03-02 18:11:16
--------------------------
155084
7387
Tình yêu giữa Hạ Hầu Tử Khâm và Tang Tử chỉ mới bắt đầu, vẫn còn đầy những âm mưu, hiểm nguy ẩn sâu trong cung cấm giai lệ. 
Mở đầu tập hai là cảnh Đàn Phi về thăm nhà cũ, người cũ. Tuy vậy, nhà cũ thì đã không nhận nàng, còn người cũ thì tưởng chừng đã đi mất từ lâu bỗng xuất hiện cùng với một người con gái bí ẩn.
Mình đặc biệt thích cách cư xử của Hạ Hầu Tử Khâm, ngài ghen với sư phụ của Tang Tử nhưng ngài vẫn cho nàng sự lựa chọn chứ không ép buộc bằng quyền vua.
Tang Tử. Tô Mộ Hàn.
Là sao đây, sẽ là quên hay nhớ mãi?
Và rồi Dao Phi xuất hiện, người giống Tô Mộ Hàn cũng xuất hiện ngay lập tức đã làm thay đổi gần như cục diện toàn câu chuyện. Đọc đến đoạn phong phi, thấy hơi ấm ức cho Tang Tử, tình yêu của Hạ Hầu Tử Khâm bắt đầu khiến mình cứ khó hiểu và mơ hồ...
4
382812
2015-01-31 08:02:20
--------------------------
152953
7387
Mệnh phượng hoàng là bộ truyện hay, nhất là tập hai. Bởi tập này là khi Tang tử vào cung, làm Đàn Phi và nhiều tình tiết hấp dẫn về âm mưu nơi cung đấu. Lúc này, nàng không chỉ đối phó với Thiên Phi, Thiên Lục mà còn đối phó với những nữ nhân khác chốn hậu cung ba ngàn giai lệ. Điều ấy càng khiến truyện trở nên hấp dẫn hơn bao giờ hết. Truyện này dù thua Đông cung, Thương Ly, Thượng Cung nhưng vẫn rất đáng để đọc. Bởi truyện cho thấy tình yêu đậm sâu giữa những tuyến nhân vật và ý nghĩa dù nhiều khi truyện hơi dài.
5
292697
2015-01-24 20:34:08
--------------------------
106578
7387
Thiết kế đẹp mắt của tác phẩm làm tôi bị thu hút và muốn mua ngay nhưng e ngại nội dung không hay nên đã tìm đọc qua. Thế rồi tôi bị thu hút vào một câu chuyện hấp dẫn với những màn cung đấu gay cấn, hiểm ác nhưng vô cùng thú vị, sâu sắc. Cốt truyện tuy không mới nhưng câu chuyện thì rất mới. Tác phẩm hay là do tác giả đã xây dựng thành công hình tượng nhân vật : thông minh, khôn khéo, nội tâm sâu sắc. Hay từ trong cách miêu tả tâm lý nhân vật đến những hành động khôn khéo mà   họ làm, hay từ từng đường đi nước bước được tính toán cẩn trọng đến cách nhìn xa trông rộng của nhân vật, hay từ những lời nói đầy ẩn ý đến tình cảm mà các nhân vật dành cho nhau. Nó làm tôi say mê, thích thú và như bị cuốn vào đó. Rất vui khi đọc được một cuốn sách " tốt cả gỗ lẫn nước sơn"  như tác phẩm này.
5
5314
2014-02-21 23:17:12
--------------------------
