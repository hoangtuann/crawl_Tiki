424719
11337
Chưa bao giờ đọc quyển sách khoa học nào hay đến thế. Có thể nói đọc là ghiền. Hay và thu hút không kém gì những quyển tiểu thuyết phiêu lưu mạo hiểm thú vị nhất. Tự hỏi nếu mình được đọc quyển sách này từ thuở còn thơ thì chắc đã yêu thích các môn tự nhiên hơn nhiều.

Các bạn học sinh nên đọc quyển sách này. Tuy nó khá dày, chất lượng giấy chưa tốt, nội dung chỉ toàn chữ là chữ nhưng tác giả viết hay lắm, gần gũi lắm. Đọc để thấy khoa học cũng lãng mạn, cũng nên thơ, cũng huyền bí lắm. Đọc để thấy tim mình cháy lên khát vọng đến cái vô hạn. Cái vô hạn ấy là tri thức, là những điều kì thú của cuộc đời, của vũ trụ.

Cảm ơn tác giả Trịnh Xuân Thuận. Cháu yêu bác rồi.
5
30454
2016-05-02 16:47:12
--------------------------
369877
11337
Quyển sách được viết rất dễ hiểu và gần gũi với cuộc sống. Các bạn không phải chuyên ngành Toán cũng có thấy đọc và hiểu được nội dụng. Cuốn sách đề cập đến vấn đề vô hạn trong toán học, nghệ thuật và khoa học nói chung. Ta thấy được rằng con người sợ khi đề cập tới vô hạn vì đó là một khái niệm rất trừu tượng, nhưng có những nhà khoa học khát vọng đạt tới cái vô hạn để mở rộng tri thức của nhân loại. Sách có những trang ảnh màu rất đẹp để người đọc có thể hình dung 1 cách dễ dàng
5
150158
2016-01-16 17:33:48
--------------------------
180465
11337
Đây là một trong những quyển sách mà mình tâm đắc nhất của tác giả Trịnh Xuân Thuận. Chất lượng sách khá ổn và màu mực in chữ cũng không bị lem. Bìa sách cũng rất đẹp màu xanh lam và pha chút đen thể hiện về một tuyệt vời nào đó,đặc biệt nhất là hình con mắt được in trên bìa của sách.Về nội dung thì khỏi bàn cãi, quyển sách đưa ta du hành vũ trụ, thế giới của các con số và của cả con người để cho chúng ta biết những điều kì thú về sự vô hạn.Các bạn hãy đọc và cảm nhận nó theo cách riêng của mình
5
579340
2015-04-09 15:01:07
--------------------------
