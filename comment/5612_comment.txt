411182
5612
Tuổi thơ tôi gắn liền với những câu chuyện Đôremon. Những câu chuyện về những bửu bối thần kì đem lai cho tôi những kí ức đẹp về một tươi lai tươi sáng màu nhiệm. Những mẫu chuyện Đôremon bạn có thể đọc đi đọc lại đọc bất cứ lúc nào. Bạn không cần đọc theo thứ tự tập mà cứ thế có cuốn nào là ôm đọc mãi không chán. Tôi đã mua cả bộ truyện Đôremon này để cho em tôi đọc. Rất hay. Tiki giao hàng và bảo quản hàng rất tốt. Cảm ơn Tiki rất nhiều.
3
958480
2016-04-05 17:45:04
--------------------------
337409
5612
Làm sao đây, tuổi thơ của mình gắn liền với Doraemon, thời gian trôi nhanh, bận rộn, bây giờ mới có thể mua cho mình một cuốn Doraemon 45 để hoàn thành nốt cuộc phiêu lưu của Nobita cùng với chú mèo máy đến từ tương lai. Dù là tập cuối nhưng câu chuyện dường như vẫn chưa kết thúc, muốn, rất muốn có nhiều hơn những mẩu chuyện thú vị về tình bạn, tình thương, có những lúc đánh nhau sứt đầu mẻ trán, nhưng vẫn luôn coi nhau như anh em ruột thịt không thể cách rời. Cảm ơn tác giả Fujiko-F-Fujio rất nhiều!
5
613850
2015-11-13 23:51:01
--------------------------
228914
5612
Đây có vẻ là một chặng đường dài. Mặc dù là tập cuối nhưng câu chuyện vẫn chưa kết thúc. Sự ra đi của rác giả đã để lại trong lòng người đọc những cảm giác luyến tiếc, đồng thời câu chuyện về chú mèo máy đáng yêu vẫn chưa đến hồi kết. Đó chắc chắn là một điều đáng tiếc cho tất cả mọi người. Nhưng có lẽ, kết thúc giữa chừng như vậy có thể sẽ hay hơn chăng. Bạn đọc sẽ luôn luôn nhớ đến tác giả và tác phẩm. Mọi người cũng không cần phải nhận xét cái kết nó tốt hay xấu. Đôi khi, hãy để chính bản thân mình tạo ra một cái kết cho câu chuyện, như vậy có thể sẽ thú vị hơn. Với tất cả mọi người, Doraemon đã từng là một người bạn rất đáng yêu đã từng lớn lên với chúng ta, chúng ta hãy luôn giữ gìn kỷ niệm ấy để mãi không quên về một tuổi thơ.
5
13723
2015-07-16 10:22:12
--------------------------
