182208
6094
Tôi đã mua cuốn sách này tặng cho cháu trai tôi. Thực sự khi nhận cuốn sách tôi rất vui, cuốn sách trông to đẹp, bìa cứng, tranh vẽ và chữ rất dễ đọc. Cháu trai tôi đã đọc hết ngay và cảm ơn tôi vô cùng vì những chuyện về cướp biển rất hài hước và kỳ thú này rất hấp dẫn cháu. Cuốn sách gồm 21 câu chuyện về nghề cướp biển. Những câu chuyện này đều rất thú vị, hài hước nhưng cũng rất ý nghĩa. Cuốn sách không chỉ dành cho các bé trai mà còn hấp dẫn các bé gái vì những câu chuyện như câu chuyện về cô con gái Julia của cướp biển…
5
88785
2015-04-13 15:47:25
--------------------------
