575941
4693
Sau khi đọc cho con nghe xong cuốn sách này cùng với nhiều cuốn khác trong toàn tập "Khám phá thế giới" mình rất ngạc nhiên và có phần thất vọng khi tên của cuốn sách đã được dịch sai khác hoàn toàn với bản gốc. Trong khi tựa tiếng Anh của bản gốc là: " Fishing with father" thì bản Việt lại dịch thành "Tình cha con". Các cuốn khác cũng không tránh khỏi tình trạng trên, được cố ý dịch khiến cho cái tên sách và nội dung bên trong dường như chẳng mấy liên quan và khiến người đọc dễ hiểu nhầm.
May là nội dung cuốn sách cũng không đến nỗi dở. Tranh vẽ và ngôn ngữ đơn giản, dễ hiểu cho nên những em bé hơn 1 tuổi từng tiếp xúc với sách cũng có thể tập trung lắng nghe được.
3
620117
2017-04-17 21:18:52
--------------------------
363491
4693
Mình mua trọn bộ Khám phá thế giới của nxb Kim Đồng cho bé nhà mình và đây là bộ truyện bé nhà mình thích nhất. Truyện kể về việc 2 bố con nhà mèo đi câu cá, nội dung không phức tạp và được dịch thành thơ nên rất dễ đọc và dễ nhớ. Bé nhà mình rất thích mấy câu cảm thán trong truyện như "Ối hi hi" hay "Ối chà chà". Nhờ có đọc truyện này mà bé nhà mình cũng suốt ngày gọi bố là "Bố ơi, bố à" như mèo con trong truyện gọi bố. Ngày nào bé cũng bắt bố đọc cho truyện này và lại líu lo đọc theo truyện. Mình khuyến khích bố mẹ nên cho con đọc truyện này để tăng tình yêu thương của bố với con.
5
592123
2016-01-04 13:22:51
--------------------------
332469
4693
“Tình cha con” là một trong số 12 cuốn trong bộ sách Khám phá thế giới do nhà xuất bản Kim Đồng phát hành. Mình nghe giới thiệu về bộ sách này đã lâu mà giờ mới đặt được hàng trên Tiki vì thỉnh thoảng mình lại thấy có thông báo hết hàng. Sách dành cho các bé ở độ tuổi từ 2 đến 6 tuổi. Cuốn này có 21 trang, hình vẽ dễ thương, ít chữ. Câu văn cũng được dịch ra dạng thơ, có vần điệu với nhau. Mình có thắc mắc là hai chú mèo trong sách này là cha-con, nhưng tác giả lại vẽ hai chú không giống nhau lắm.Mình rất thích bộ sách này.
3
15022
2015-11-06 13:26:55
--------------------------
