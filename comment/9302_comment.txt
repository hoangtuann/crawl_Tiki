569033
9302
Tôi đã mua cuốn sách này và tôi cảm thấy nó rất thú vị. Tôi luôn biết rõ tôi không lý trí và không lý trí đến mức nào khi ra quyết định và cuốn sách này giúp tôi thấy rõ sự không lý trí của TẤT CẢ mọi người khi ra quyết định- theo đó, kể cả một người cho mình lý trí nhất thì cũng sẽ lâm vào tình huống "phi lý trí" khi ra quyết định.
5
1735889
2017-04-10 12:01:04
--------------------------
568961
9302
Lúc đầu tôi cảm thấy ko mặn mà với tựa sách, nhưng khi đọc nội dung thì cảm thấy sách viết rất hay. Trái với cảm nghĩ đầu tiên là sách sẽ khô khan, cuốn sách đưa ra các dẫn chứng cụ thể, gần gũi, và rất dễ hiểu. Chúng ta có thể áp dụng trong cả kinh doanh, các mối quan hệ xã hội
5
432271
2017-04-10 11:05:42
--------------------------
540681
9302
Ban đầu tôi cũng nghe mọi người nói sách hay nên tôi mua đọc thử. Cuốn sách lột trần sự thật phi lý trí của con người trong các lựa chọn, tác giả phân tích rất kĩ làm tôi thấy ngạc nhiên rằng, những điều nhỏ nhặt mình chẳng bao giờ để ý hóa ra lại là những sự việc có thể dự đoán được.
5
2059790
2017-03-13 11:32:45
--------------------------
490462
9302
Mình chưa đọc hết nên không dám review nhiều nhưng đây thực sự là một cuốn sách đáng để bạn bỏ tiền túi mua về nghiền ngẫm. Những ví dụ, thí nghiệm mà tác giả đưa gia vừa gần gũi vừa dễ hiểu mang lại những cách nghĩ mới cho mình về chính bản thân mình! Bạn nghĩ mình thực sự là một người tiêu dùng khôn ngoan? Sau cả khoảng thời gian dài đắn đo, bạn tin rằng mình đã có một lựa chọn đúng đắn? Tôi khuyên bạn nên thử đọc cuốn sách này để có được những đánh giá, chọn lựa thông minh hơn hay chỉ đơn giản là một cách nhìn mới, cách nhìn khác mà bạn chưa từng nghĩ tới trước đây.
5
889822
2016-11-12 21:49:14
--------------------------
478204
9302
Đây là một cuốn sách dễ đọc và bổ ích. 
Dan Ariely đã mang đến cho người đọc những cách tiếp cận mới mẻ về cách con người thực hiện hành vi của mình. Rất nhiều thứ tác động một cách thần kỳ và trực tiếp đến quyết định của chúng ta mà chúng ta chưa bao giờ để tâm tới. Vẫn nghĩ mình hành động lý trí và đã ra quyết định sau những cân nhắc hết sức hợp lý mà không mảy may suy tưởng đến việc ta đã bị dẫn dắt bởi các yếu tố ngoại cảnh.
Đây là cuốn sách dành cho những bạn muốn tìm hiểu thế giới xung quanh, tìm hiểu về con người và thấu hiểu rõ hơn chính mình. Một quyển sách xứng đáng có mặt trên kệ sách của bất kỳ ai.
4
605785
2016-07-31 21:37:44
--------------------------
447951
9302
Cuốn sách nói về một đề tài rất thú vị sau mỗi hành động ta thường nghĩ mình đã suy nghĩ thấu đáo tuy nhiên con người có rất nhiều khi lười suy nghĩ hành động theo kinh nghiệm đã trải qua trong quá khứ trong khi mọi thứ luôn thay đổi cần phải có suy nghĩ linh hoạt hơn, nhất là gần đây nhiều người còn trẻ tuổi đã có những thành kiến vô cùng   cố hữu trong đầu điều đó ko chỉ ngăn họ đến thành công mà còn kiềm hãm sự phát triển tự nhiên của họ, trực giác ko hẳn đúng nhưng không bao giờ là hoàn toàn sai    
4
713046
2016-06-15 08:38:32
--------------------------
391063
9302
Con người vô cùng lý trí!
.
.
.
Hả?! Chời ơi nói thiệt hông có đâu!
Chẳng hạn như tui! Tui đã từng mãnh liệt tin rằng mình lý trí lắm, mình ngon lắm! Mỗi sáng dậy sớm cẩn thận chuẩn bị cơm nước đi học cho an toàn cũng như tiết kiệm để có ngày đặt chân đến Sa Pa thành công như hằng đêm vẫn thường ôm mộng tưởng nhưng chỉ cần buổi chiều đi ngang qua hàng bún mắm, bánh canh, rau câu, bánh plan, bánh mì, bánh bèo, bánh tráng (đã vậy bánh tráng còn hơn trăm loại) ... là bao nhiêu lý trí, dự định, kế hoạch, tiền bạc, Sa Pa các kiểu đều đồng loạt được M4U nhập vào hát chung một bài "Tan biến". Haizzz! Thiệt là thê lương quá mà!
Nói chung là, mọi sự mình làm nó cũng không được lý trí mấy như mình hay nghĩ đâu! Hông tin hả? Hông tin mấy bạn kiếm cuốn Phi Lý Trí của ông Dan Ariely đọc đi là tin liền. Thiệt! Tuy là sách xuất bản đã lâu, dạo gần đây cũng hổng mấy ai nhắc đến nhiều nhưng cũng từng lẫy lừng là best - seller đó, nghe là thấy xịn dòiii hehe. Mới nhìn thì có vẻ thuộc dạng một loại khó nuốt nhưng kỳ thực chỉ cần tập trung một chút, kiên nhẫn một chút, chậm lại một chút và khi đã bắt nhịp kịp thì đây chắc chắn là một cuốn sách đầy cảm hứng và lôi cuốn cực kỳ. Nội dung chính của cuốn sách xoay quanh những câu chuyện, những thí nghiệm chứng minh những hành động phi lý trí vẫn đang diễn ra có hệ thống hàng ngày mà chúng ta chẳng hề nhận ra- điều mà tác giả đã đúc kết được qua đời sống của chính ông từ đó đưa ra lời khuyên cũng như giải pháp để giúp chúng ta sống lý trí và thành công đúng với mức mà chúng ta nên nhận. Sách này bi giờ trong nhà sách lẫn trên tiki khó kiếm dưng mà ở ngoài phố Trần Huy Liệu mình thấy vẫn còn, cũng mới, giá cũng xêm xêm Tiki. Hông thì tiết kiệm hơn, mua ebook đọc giống tui nè rồi tiền dư không biết làm gì thì để dành đi Sa Pa!!! (hay là để mua bánh tráng gì đó huhu.)
5
1092313
2016-03-04 21:38:15
--------------------------
377106
9302
Đúng là một cuốn sách hay mở rộng tầm mắt. Dạo này mình có nhiều lúc "Wide Awake". Quả là một cuốn sách hay, từng câu nói, thí nghiệm của ông đều chứng minh cho đề tài ông nói. Không như vài cuốn sách chỉ dựa vào lí sự cùn. Cuốn sách đưa ra vấn đề, làm thí nghiệm trong thực tế rồi rút ra cho ta những vấn đề ta không bao giờ nó quan trọng. Một cuốn sách rất cần thiết đánh vào tâm lí và thị hiếu của mọi người. Phi lí trí giúp ta nhận ra những vấn đề của ta, ta phi lí trí như thế nào
5
383240
2016-02-01 17:36:02
--------------------------
375815
9302
Tình cờ được bạn giới thiệu cho cuốn sách này và mình tò mò mua về đọc xem sao. Tựa đề sách khá thu hút mình nên mình đã kịp thời mở ra đọc trước vài trang. Và quả thật sách không làm mình thất vọng tí nào; từ cách trình bày đến nội dung. 

Sách trình bày khá logic, chia ra từng chủ đề riêng nên không nhất thiết phải đọc từ đầu đến cuối. Mình có thể mở ra bất kỳ phần nào và đọc. Điều đó giúp mình thấy thú vị hơn. Nói về nội dung sách thì quả thật tác giả viết rất ngắn gọn, xúc tích, cách truyền tải đi thẳng được vào nội dung chính. Mình rất thích điều đó. Sách như một cuốn cẩm nang kỹ năng sống mà bạn có thể đọc bất cứ lúc nào, bất cứ trang nào bạn thích. Không cần phải ép mình "ngốn" hết trong 1 lần, cứ từ từ, chậm rãi nhưng sẽ hiểu và học được rất nhiều điều hay. Cảm ơn Tiki đã giúp truyền tải được những giá trị tinh thần này.
5
132829
2016-01-29 11:32:46
--------------------------
323842
9302
Mình cảm thấy khá ấn tượng với cuốn sách này, nó trình bày những điều hằng ngày chúng ta vẫn làm một cách phi lý trí một cách rất dễ hiểu và gần gũi. Trong sách có rất nhiều nghiên cứu khoa học về các hành vi và tâm lý của con người nhưng được viết rất rõ ràng và dễ hiểu chứ không phức tạp như các bài luận văn hay những cuốn sách khác. Tuy là đọc hiểu về những hành vi phi lý trí của mình nhưng mình cảm thấy rất khó để sửa đổi được nó, hay có lẽ mới đọc qua một lần nên chưa thấm? Tóm lại mình thấy đây là một cuốn sách đáng đọc và mang lại thêm nhiều kiến thức.
5
450592
2015-10-19 20:05:30
--------------------------
303086
9302
Một cuốn sách đào sâu khai thác khía cạnh phi lý trí của thứ tưởng chừng như lúc nào cũng lý trí – tư duy con người. Qua nhiều thí nghiệm và nghiên cứu của tác giả, những phát hiện mới về suy nghĩ của con người trong mỗi quyết định, mỗi hành động được làm sáng tỏ rất thú vị, tỉ như 1 cây bút có giá 8 đô, nếu biết có 1 chỗ cách đó 15p đi bộ chỉ bán cây bút này với giá 7 đô thì hầu hết người ta sẽ sẵn sàng lội bộ đi mua. Cũng như vậy, chỉ thay cây bút 15 đô bằng bộ áo vét 450 đô, chỗ cách đó 15p đi bộ cũng bán bộ vét đó rẻ hơn 8 đô thì hầu như chẳng ai làm điều đó. Còn nhiều thí nghiệm nữa với kết quả thú vị lắm, cũng là một nguồn tham khảo khá tốt cho ai học marketing để biết thêm về hành vi khách hàng đó :)
5
199482
2015-09-15 17:26:57
--------------------------
295852
9302
“Phi lý trí” – Dan Ariely. Đây là quyển sách khá hay nói về lý trí, cũng như hành vi của con người. Lý trí là quyền năng mạnh mẽ nhất mà tạo hóa ban tặng cho con người. Tác giá cho chúng ta thấy: đôi khi chúng ta phi lý trí hơn chúng ta tưởng và khám phá ra những động lực vô hình ẩn sau những quyết định của con người. Sách cho chúng ta biết sự thật về tính tương đối, quan điểm sai lầm về cung – cầu, sự kiểm soát và trì hoãn, tác động của bối cảnh đến tính cách,… Sách hay và bổ ích, bổ sung nhiều kiến thức.
Xin cảm ơn. 

5
554150
2015-09-10 18:02:29
--------------------------
293629
9302
Cuốn sách này thật sự rất cuốn hút nó làm cho tôi thay đổi lại cách suy nghĩ của bản thân rất nhiều. Trong qua trình đọc tôi cảm nhận được rât nhiều thứ trong cuộc sống qua cách phân tích của tác giả nó trở nên mới mẻ hơn. Đây nên là một sự lựa chọn sáng suốt cho những ai muốn tìm một cuốn sách gói đầu giường, nó là một tư liêu tốt và nhất là những người học kinh tế nên đọc qua nó. Chúc các bạn có một trải nghiệm lý thú với quyển sách này
5
471238
2015-09-08 15:02:15
--------------------------
293391
9302
Mình nghe danh cuốn sách này trên cộng đồng goodread giới thiệu và quyết định mua. Giờ đọc xong phải lên đây review lại ngay. Thiệt là như được khám phá một vùng đất mới vậy, không ngờ những quyết định của con người lại có tính hệ thống mà chính mỗi người không ý thức được. Sách có rất nhiều các thông số và hình ảnh minh họa một cách rõ ràng, bìa cũng dày và cứng nữa. Nhưng có lẽ cần phải có thời gian để tự chiêm nghiệm, thực hành nhiều hơn nữa, lúc đó mới gọi là kiến thức của mình. Rất đáng để đọc
5
548830
2015-09-08 11:41:16
--------------------------
285079
9302
Đây là một trong những cuốn sách hay nhất mà mình đọc. Nó làm suy nghĩ của mình thay đổi rất nhiều: yêu thương mọi người nhiều hơn, cảm thông với mọi người nhiều hơn và đi mua sắm cũng ít bị dụ dỗ hơn. Cuốn sách cũng giúp mình biết được làm thế nào để đạt được mục tiêu, làm thế nào để uống thuốc đúng giờ mà không bị quên (Cái ni mình quên hoài). Bởi vậy sau khi mình đọc xong cuốn sách, mẹ còn vui hơn cả mình vì mình thay đổi rất nhiều. Một cuốn sách rất hay.
5
308365
2015-08-31 15:52:59
--------------------------
282616
9302
Phi Lý Trí (Tái Bản 2014) của Dan Ariely là cuốn sách cho dân kinh tế , người không học cũng có thể đọc vì nó mang đến lăng kính nhìn mới lạ về kinh tế , một cuốn sách rất thú vị . Bìa sách và giấy làm từ chất liệu tốt, bản in không mắc nhiều lỗi nên đáng để mua một cuốn về suy ngẫm , bạn sẽ thấy đáng đồng tiền bát gạo . Cám ơn tiki đã cho mình cơ hội được đọc cuốn sách này , mình sẽ giới thiệu nó cho bạn bè minh để họ cũng có cơ hội tiếp thu kiến thức như mình.
5
729944
2015-08-29 11:55:07
--------------------------
282450
9302
Những kiến thức trong sách khiến tôi "vỡ òa" vì trước nay có những sự thật rành rành trước mặt mà tôi không để ý, đến khi đọc và nghe phân tích của tác giả tôi mới thấm thía. Tôi nghĩ những nhà kinh doanh nên đọc quyển sách để có những phân tích và cách ra quyết định cho giá thành sản phẩm cũng như chiến lược kinh doanh hợp lý, để từ đó thúc đẩy kinh doanh. Tôi xin cảm ơn tác giả vì những nghiên cứu và chia sẻ của ông, đây là một trong những quyến sách mà tôi yêu thích nhất từ trước tới nay
5
169373
2015-08-29 10:02:48
--------------------------
279774
9302
Phi lý trí mình đã tìm mua và đọc cách đây 2 năm, nhờ sự giới thiệu của một giảng viên dạy môn Xác suất thống kê. Lần này mua nó, để tặng cho một đứa em, với rất nhiều hi vọng, cuốn sách có thể thay đổi điều gì đó ở đứa em này.
Lúc vừa thấy cuốn sách, nói thật mình không ưng bìa sách lắm, sách tái bản lần này  bìa bóng, chứ không mịn kiểu như cuốn Phi lý trí trước mình mua ^^ - tự cười 1 phát, lại là một tư duy phi lý đây. Mình nghĩ đây là một cuốn sách hay, xứng đáng là một trong những cuốn gối đầu giường. Nhưng cái hay thường đi kèm với sự khó hiểu, có những đoạn thí nghiệm của Dan Ariely mà mình phải đọc đi đọc lại mấy lần mới hiểu ông thí nghiệm như vậy để làm gì đó, cái này chắc do tư duy hơi chậm của mình thôi =))) Một lời khuyên cho bạn nào hay đi mua sắm hay muốn mua bất kì một thứ gì, hãy đọc Phi lý trí, để có thể đưa ra 1 quyết định chính xác nhất nhé

5
770273
2015-08-27 09:07:10
--------------------------
277991
9302
   Qủa thật sau khi đọc Phi lý trí mình mới nhận ra. Những điều mà Dan Ariely viết hoàn toàn chính xác. Ông đã chỉ ra những điều mà luôn xuất hiện trong cuộc sống nhưng lại luôn không để ý đến. Quyển sách này có thể giúp bạn tiêu dùng một cách thông minh hơn. Mình cảm thấy ông cực kì có nghị lực và làm việc một cách thật sự và có trách nhiệm. Những thí nghiệm của ông được thực hành và tạo sự tin cậy cao chứ. Quyển sách rất dễ hiểu và lôi cuốn. Mình cũng không biết nó thuộc lĩnh vực kinh tế hay tâm lý học nữa. Một lựa chọn tuyệt vời cho những ai học 2 ngành này và những người tiêu dùng.
5
505714
2015-08-25 15:08:07
--------------------------
272378
9302
Mình biết cuốn này khá lâu khoảng 1 năm về trước nhưng mới mua gần đây, có thể nói là rất đáng, khi đọc nó mình ngỡ ra rất nhiều điều, tưởng như bình thường mà lại vô cùng lý thú, chúng ta đôi khi hành động khác với những gì chúng ta nên làm nhưng chúng ta lại không biết điều đó cứ hành động theo lối mòn, hơn nữa nó còn cho chúng ta biết rõ hơn về điều bí ẩn đằng sau những hành vi của con người. Phi lý trí là một quyển sách thuộc lĩnh vực kinh tế học tuy nhiên với ngôn ngữ và cách diễn đạt dễ hiểu không khó để những ai không thuộc lĩnh vực này có thể tiếp thu một cách dễ dàng.
4
504208
2015-08-19 23:39:02
--------------------------
266660
9302
Đã mua quyển này vào tháng trước. Phải nói đây là quyển sách cần thiết cho mọi đối tượng chứ không chỉ dành riêng cho những người có chuyên môn về kinh tế học, mặc dù có những kiến thức liên quan đến chuyên môn hơi khó hiểu một chút. Thế nhưng những gì mà quyển sách truyền đạt, từng chút một sẽ khiến bạn bất ngờ, vì những ví dụ rất thực tế, rất đời thường mà ẩn sau đó là cả quá trình nghiên cứu và giải đoán kinh tế học hành vi rất thuyết phục. 
4
70065
2015-08-14 21:26:33
--------------------------
257339
9302
Với ai đã từng biết đến tác giả Dan Ariely thì sẽ không khỏi thán phục con người tài ba này. Là một tấm gương của niềm tin không đầu hàng số phận. Cũng như quyển sách này - Phi Lý Trí là quyển sách tác giả đã trải nghiệm chính cuộc sống của mình và viết nên nó. Rất nhiều thí nghiệm được ông và các đồng nghiệp của mình trong cuốn sách sẽ làm tăng thêm sự thuyết phục với người đọc. Giúp đọc giả hiều được phần nào sự thật ẩn sau mọi quyết định trong cuộc sống của con người. 
5
709574
2015-08-07 16:23:10
--------------------------
248031
9302
Lấy cảm hứng từ câu chuyện của chính mình (thời gian điều trị bỏng trong bệnh viện), Dan Ariely đã tiến hành hàng loạt những cuộc thí nghiệm về hành vi con người; ông đã đúc kết được một loạt những kiến thức thú vị về trí não con người: Phi lý trí. Không nhiều người biết đến khái niệm Phi lý trí, song nó xuất hiện trong mọi lĩnh vực: chiêu trò marketing, họp mặt gia đình, mua kẹo, hưng phấn tình dục, tính tương đối... Tất cả đều được gói gọn trong 339 trang sách, được Ariely trau chuốt, gợi mở với những ví dụ cụ thể được phân tích kỹ lưỡng, cũng như những bài học sâu sắc, khúc chiết. Thời gian bạn bỏ ra để đọc 339 trang sách Phi lý trí chưa bao giờ là lãng phí!
5
575572
2015-07-30 17:09:00
--------------------------
242090
9302
Cuốn sách “Phi Lý Trí” của tác giả Dan Ariely có lời đề tựa kích thích sự tò mò của tôi. Khái niệm “lý trí” cũng như “phi lý trí” không được giải thích theo cách giải mã ngôn ngữ bình thường, tôi nhận ra ý nghĩa thật sự của chúng sau các câu chuyện và bài học cuộc sống từ cuốn sách. Dan Ariely là giáo sư kinh tế học hành vi nên những vấn đề được ông giải thích đôi lúc khó hiểu với lượng kiến thức mới lạ. Đọc sách, tôi hiểu thêm về trí não hành vi bản thân, những gì phi lý trí theo một cách có quy luật bởi những sự khôn ngoan của người kinh doanh. 
4
276840
2015-07-26 08:16:29
--------------------------
241615
9302
Bạn có bao giờ được nghe đến Ngọc trai đen. Tôi nghĩ là có, tất nhiên rồi, đây là một sản phẩm quý mà tạo hóa ban tặng cho con người. Thế nhưng, bạn có biết rằng ban đầu những viên ngọc trai đen đầy sự huyền bí và quý phái này hầu như không có chỗ đứng trên thị trường. Nhưng sau đó thì sao? Sau đó là những gì bạn được nhìn thấy ngay trong thị trường ngày nay: Ngọc trai đen có giá cao ngất ngưỡng. Lý giải cho điều này, Dan Ariely đã lấy ví dụ về đàn ngỗng. Tại sao lại là những chú ngỗng ư? Vì ông Ariely cho rằng, hành vi con ngỗng đi theo sinh vật đầu tiên mà chúng thấy khi chào đời có liên hệ mật thiết với hiện tượng “khắc sâu” của con người. Lấy ví dụ về ngọc trai đen, về đàn ngỗng và còn rất rất nhiều những thí nghiệm thú vị khác, tác giả đã cho ta thấy rằng con người đã phi lý trí như thế nào. 
Bản thân chúng ta không đôi khi “phi lý trí” hơn chúng ta tưởng. Đó là những gì mà tôi rút ra được sau khi gấp lại quyển sách này. 
“Phi lý trí” là một quyển sách về Kinh tế học hành vi, nhưng lại rất gần gũi và dễ tiếp thu đối với mọi người, kể cả những người không am tường về kinh tế. Tin tôi đi, quyển sách sẽ làm bạn mở mang rất nhiều. Và sau khi đọc xong, tôi chắc rằng, bạn sẽ mất vài giây để suy nghĩ về một món hàng khuyến mãi hoặc một món hàng được tặng miễn phí mà so với trước kia, bạn sẽ chọn nó mà không chần chừ dù chỉ một giây.

5
43136
2015-07-25 17:30:03
--------------------------
231949
9302
Đây là cuốn sách nên đọc dù có người sẽ thích đọc kỹ/đọc sâu và có người sẽ chỉ đọc lướt qua. Lý do: Đây là cuốn sách lý giải tâm lý học hành vi con người khá khoa học. Không chỉ khi đọc xong gấp sách lại  mà ngay cả khi đang đọc chúng ta cũng phát hiện ra mình "phi lý trí": điên cuồng mua sắm các sản phẩm giảm giá dù có khi bỏ xó, nhầm lẫn tai hại giữa sự giúp đỡ xã hội và quan hệ mua/bán...
Đọc xong cuốn sách rồi mình đoán người đọc sẽ hiểu về tâm lý học hành vi để điều chỉnh lại cách sống một cách "lý trí hơn". Điều mình không thích ở cuốn sách này là bài trắc nghiệm về sex với các sinh viên. Nó có vẻ không thuyết phục lắm!
5
291573
2015-07-18 14:30:36
--------------------------
226549
9302
Đây là một cuốn sách tuyệt vời về hành vi con người. Cuốn sách giải thích tất cả các hành vi, quyết định của con người dựa vào các động lực vô hình với lối viết cuốn hút, dễ hiểu, và đầy những nghiên cứu khoa học thực nghiệm để minh họa.
Lối bố cục 1. Nêu vấn đề, 2. Giải thích, 3. Nêu ví dụ, liên tưởng, những câu truyện ta được nghe từ bé... để dẫn dụ cho lý thuyết, 4. Thực nghiệm, 5. Kết quả, 6. Phương pháp khắc phục.
Mỗi chương, mỗi câu chữ đều không hề thừa. Quyển sách này nói đến hành vi con người và đặc biệt là cuốn sách mở đường cho ngành Kinh tế học hành vi (Nếu ai cảm thấy cái sự hay của Tài chính hành vi thì đây, một quyển sách tuyệt vời để đọc ;)).
5
590813
2015-07-12 13:42:22
--------------------------
214682
9302
Ariely là một trong những tác giả mình yêu thích nhất. Đọc phi lý trí không khác gì bạn đang được chiêm ngưỡng những tác phẩm nghệ thuật, đó chính là các công trình nghiên cứu về hành vi ứng xử của con người trong những việc rất đời thường và hàng ngày.

Khi đọc và cảm nhận xong, có lẽ bạn sẽ hiểu được phần nào tại sao con người chúng ta lại hành động phi lý trí như vậy, qua đó sẽ giúp chúng ta tránh được một vài tình huống không mong muốn cũng như vô cùng khó xử trong công việc và cuộc sống.
4
140922
2015-06-25 13:04:20
--------------------------
207216
9302
Qủa thật con người đôi khi lý trí không thắng nỗii con tim, tức là hành động theo bản năng, ít khi suy xét cẩn thận. Sau khi đọc xong cuốn sách này tôi nghiệm ra nhiều điều, rằng bất cứ thứ gì trên đời đôi khi không hẳn như những gì mình nghĩ, mình chỉ hành động theo phán đoán chủ quan. 
Tôi rất ấn tượng sau khi đọc xong cuốn sách này và sau khi đọc xong tôi chợt nhận ra mình khá là hành động phi lý trí trong cuộc sống, trở thành người "dễ bị dụ", dễ thoả hiệp với người khác. 
Sách trình bày tương đối rõ ràng dễ hiểu, bìa đẹp tuy nhiên đôi chỗ chữ hơi mờ, khó đọc.
Nói chung cuốn sách này rất hay, Cảm ơn tác giả, cuốn sách rất thú vị.
4
462666
2015-06-11 18:29:36
--------------------------
204939
9302
Quyển sách này thực sự thú vị khi bạn vừa đọc, vừa để ý đến những hành động của mình và mọi người chung quanh trong cuộc sống hằng ngày. Và chính những công ty bán hàng đang là những người đang lợi dụng sự phi lý trí của chúng ta để marketing và đẩy mạnh doanh số. Nhiều khi bạn sẽ thích thú khi mua hàng có những sản phẩm khuyến mãi mà bạn cần hơn là mua những sản phẩm đó, nên mình nghĩ cuốn này thực sự có ích cho những ai muốn kiềm chế bản thân khi đi mua hàng, như mình chẳng hạn.
4
140014
2015-06-05 12:05:28
--------------------------
204682
9302
Cá nhân tôi rất ghét những kẻ suốt ngày cứ mở mồm ra là bảo người này sống lý trí, kẻ kia cái gì cũng lý trí,... Tôi nghĩ những kẻ đó hoàn toàn mù tịt về tâm lý học. Sau khi đọc Phi lý trí, tôi càng nghĩ rằng mình đã đúng. Ngoài ra, tôi còn thu nhặt được những kiến thức rất quý giá mà mình chưa bao giờ biết đến. Chẳng hạn như ngoài cảm xúc, tác giả còn cho rằng mong đợi, quy chuẩn xã hội và các lực lượng vô hình cũng làm méo mó năng lực lý trí và làm cho chúng ta có những hành động sai trái. Những lúc đó, tôi thường không thể làm chủ được đầu óc của mình. Tác giả đã phân tích rất cụ thể về những tác nhân này và tôi tin tưởng rằng những kiến thức này sẽ giúp chúng ta hành động đúng đắn hơn, kiên định hơn.
5
313837
2015-06-04 17:40:33
--------------------------
167369
9302
Tôi phải công nhận một điều đây là quyển sách viết ngắn gọn và cô đọng nhất về thể loại kĩ năng, khoa học & phát triển con người. Tác giả Dan Ariely rất xuất sắc ở điểm lồng ghép những ví dụ ngắn gọn nhưng đầy hàm ý. 

Sách có 13 chương với 13 nội dung khác nhau nhưng lại liên kết với nhau, tôi khá ấn tượng với chương 7 : "Cái giá cao của sự sở hữu". Chương 7 nói rất rõ và phân tích đậm nét lí do vì sao các hot brand dù giá cả rất xa xỉ nhưng lượng hàng bán rất chạy, tôi thích cách viết thẳng thắn của ông Dan, đi thẳng vào vấn đề, rất chi tiết. 

Về hình thức thì không có gì để bàn, bìa nhìn ngoài nổi hơn & bóng sáng hơn, chữ in rõ, đậm nhưng khổ sách phân bố chưa hợp lí nên nhiều lúc đọc phải ấn mạng sách vì chữ bị khuất. Còn lại là rất tuyệt, 9,75/10đ ^^!
5
50283
2015-03-14 17:27:27
--------------------------
142643
9302
Phi lý trí là một trong những quyển sách về tâm lý học hành vi hay nhất mà mình từng đọc. Nội dung chủ chốt của quyển sách nhằm chứng minh đa số quyết định của mỗi người chúng ta trong cuộc sống thường ngày (như mua bán, so sánh lựa chọn giữa các loại sản phẩm, giá cả...) đều không dùng đến lý trí. Rất nhiều ví dụ lý thú được đưa ra như quyết định mua báo của sinh viên MIT hay chiến lược nâng giá ngọc trai đen của Assael đưa người đọc đi từ bất ngờ này đến bất ngờ khác. Sau khi đọc xong quyển sách, mình nhận ra rằng chính bản thân đôi lúc cũng hành động một cách phi lý trí, trở thành "miếng mồi ngon" cho những người bán hàng hay đơn vị truyền thông ;) Quyển sách viết về đề tài khó ( tâm lý học hành vi) nhưng lại rất dễ đọc, dễ hiểu. Nếu bạn là sinh viên hay người hoạt động trong ngành Sales - Marketing - Truyền thông - Quảng cáo bạn đừng nên bỏ qua quyển sách này.
4
398179
2014-12-19 13:30:01
--------------------------
136114
9302
Tôi đã mua cuốn sách này và tôi cảm thấy nó rất thú vị. Tuy nhiên, đầu tiên, để dẫn đề, có vài người bạn đã nói một cách rất hài như thế này khi thấy tôi đọc cuốn sách này: "mày đã không lý trí rồi mà còn đọc sách này thì coi chừng mất lý trí!?" - chẳng biết nói hài hay nghiêm túc?. Tôi xin trả lời: "Vâng! Tôi luôn biết rõ tôi không lý trí và không lý trí đến mức nào khi ra quyết định và cuốn sách này giúp tôi thấy rõ sự không lý trí của TẤT CẢ mọi người khi ra quyết định" - theo đó, kể cả một người cho mình lý trí nhất thì cũng sẽ lâm vào tình huống "phi lý trí" khi ra quyết định.

Tuy nhiên, trước hết thì cần giới thiệu một chút. Cuốn sách này được viết bởi Giáo sư kinh tế học hành vi Dan Ariely của Học viện Công nghệ Massachusettes (MIT). Tôi tin rằng ông là một giáo sư tài ba về kinh tế học hành vi (lĩnh vực tôi yêu thích) và rất muốn gặp ông một lần. Nói chung là tôi rất khâm phục ông này về mảng kinh tế học hành vi.

Bây giờ vào đề chính. Nếu bạn đã học qua Kinh tế học và còn nhớ nó thì sẽ biết rằng Kinh tế học được xây dựng trên một giả định rất quan trọng rằng con người là "duy lý" - nghĩa là con người sẽ luôn đưa ra quyết định một cách rất lý trí. Tuy nhiên, thật sự có như vậy? "Phi lý trí" trả lời rằng KHÔNG! Những tình huống (case) trong sách được đưa ra rất thực tế và từ đó cho thấy được con người "phi lý trí" như thế nào và "phi lý trí"một cách có hệ thống khi ra quyết định dù họ luôn cho rằng họ đang hành động một cách rất lý trí.

Hãy lấy vài ví dụ thú vị sau: "bạn có tin rằng người bạn đời mà bạn chọn là thật sự phù hợp với bạn? liệu có thứ gì ảnh hưởng đến quyết định của bạn?", hoặc "tại sao cò nhà đất thường dẫn bạn đi coi những ngôi nhà khá tốt trước rồi mới đưa bạn đến một hai ngôi nhà tốt thật sự sau đó và bạn quyết định chọn mua căn nhà này?", hoặc "tại sao chỉ nên miễn phí vận chuyển với một đơn hàng có giá trị nhất định trở lên mà không phải là miễn phí với tất cả các đơn hàng?", và rất nhiều tình huống khác nữa.

Vậy bạn có thật sự tin là bạn luôn rất lý trí khi đưa ra những quyết định trong đời? "Phi lý trí" sẽ cho bạn thấy câu trả lời dựa trên những nghiên cứu thực nghiệm nghiêm túc và khoa học. Còn bạn có rảnh như tôi để đọc nó không là chuyện khác. Good luck!
5
152358
2014-11-19 08:19:45
--------------------------
134161
9302
Tôi mua cuốn sách này trong ngày hội xả hàng gần đây. Và đọc một lèo từ đầu đến cuối. Dù không phải là dân kinh tế nhưng nó đối với tôi không phải là không hữu ích. Cuốn sách không vẽ ra những lí thuyết đơn thuần, tất cả đều được minh họa rõ ràng, chi tiết qua các nghiên cứu của chính tác giả, đầy ví dụ. Nó cho ta một cái nhìn rõ ràng hơn về những quyết định hằng ngày - vô lí một cách khó tưởng tượng. Là sinh viên Y Dược, dù có nhận thức về vai trò của giả dược và những liệu pháp tâm lí trong điều trị, nhưng tôi cũng không ngờ nó có tác động mạnh mẽ đến vậy. Thực sự đây là một cuốn sách rất bổ ích.
5
205720
2014-11-08 16:11:54
--------------------------
133954
9302
Mình đã đọc một lượt quyển sách này, phải nói là những công trình nghiên cứu và thí nghiệm trong sách khá đơn giản nhưng có quy mô rộng lớn và thực tế, mình phần nào tin tưởng những dẫn chứng của tác giả qua những thí nghiệm đó.

Cứ mỗi 1 luận điểm là tác giả lại sử dụng không dưới 5 thí nghiệm để chứng minh cho nó, đủ thấy ông tâm huyết cỡ nào. Những luận điểm, câu hỏi của từng chương đều là những điều rất có ích trong cuộc sống, cứ cuối mỗi chương ông đều trả lời hết. Có một điều đó là ông không chính thức hướng dẫn chúng ta cách sử dụng luận điểm ấy, mình dù biết là "Không nên mua hàng miễn phí" nhưng cũng không thể áp dụng nguyên lý này triệt để.

Cuối sách có phần phụ lục, phần này theo mình là hữu dụng nhất tuy nó ngắn nhất. Những chương cuối này trình bày cụ thể cách sử dụng những luận điểm ở phần I, nhưng vẫn ngắn quá. Có lẽ ông chia công trình của mình thành 2 phần, phần 1 là Phi Lý Trí (thượng), và phần 2 - cái mình cần nhất là Lẽ Phải của Phi Lý Trí (hạ).
4
418158
2014-11-07 19:13:13
--------------------------
85507
9302
Mọi người thường hành động theo PHI LÝ TRÍ hơn là LÝ TRÍ. Nếu bạn là marketer,là người kinh doanh,có hứng thú tìm hiểu về tâm lý,hành vi của con người thì PHI LÝ TRÍ là một cuốn sách có ích cho bạn. Thấu hiểu "Hành vi khách hàng" là một việc không dễ,nhất là đối với các young marketer. PHI LÝ TRÍ giúp hiểu rõ hơn về cách thức con người hành động theo phi lý trí và các ví dụ thực nghiệm về các hành động đó. Đọc PHI LÝ TRÍ có thể giúp cho các bạn tạo ra các "bẫy" PHI LÝ TRÍ để có thể tăng doanh số,lợi nhuận cho bạn hoặc ít nhất nó cũng giúp bạn tránh các "bẫy" PHI LÝ TRÍ mà người khác đang giăng ra.
5
132942
2013-07-05 16:02:33
--------------------------
80749
9302
Đây là cuốn sách khá hay, nó mở ra cho tôi một thế giới mới về phi lý trí, mới đầu đọc tựa đề tôi thấy thật khó hiểu nhưng khi đọc xong cuốn sách tôi cảm thấy nó cũng không khó hiểu như tôi tưởng. Nhờ cuốn sách mà tôi hiểu về những quyết định, những sự lựa chọn của con người và của chính bản thân là từ đâu, vì đâu và tại sao ta lại lựa chọn như vậy?
Phi lý trí, cuốn sách giúp tôi thấu hiểu quy luật của những sự lựa chọn, những cách thức gây dấu ấn trong kinh doanh, quảng bá sản phẩm, những mánh khóe mà những người bán hàng sử dụng để đưa chúng ta vào phi lý trí.
Đây thật sự là cuốn sách thú vị cho những người muốn khám phá về một thế giới mới lạ, thế giới của PHI LÝ TRÍ
5
105935
2013-06-14 00:46:21
--------------------------
70280
9302
Cuốn sách này mang lại cho tôi cái nhìn rất mới về cái-được-gọi-là-lý-trí. 
Tôi cứ nghĩ mình là không ngoan khi mua những thứ được gắn cái mác giảm giá, khuyến mãi để có thế tiết kiệm ít tiền mà không hề nghĩ tới rằng những món đó không thật sự cần thiết cho tôi. Và những thủ thuật mà những người kinh doanh đưa ra cho chúng ta chọn lựa phần lớn đều rơi vào trúng cái họ muốn bán cho chúng ta bằng cách trưng ra vài thứ mồi nhử để chúng ta so sánh.
Ở một chương khác trong cuốn “Phi lý trí” đã giúp tôi nhìn ra mối quy chuẩn xã hội và quy chuẩn thị trường, bạn thử đặt mình vào hai trường hợp sau: một người nhờ bạn phụ họ bưng một vài món đồ và người khác đưa tiền ra và nhờ bạn bưng đồ giống như khi nãy? Bạn sẽ nhiệt tình bưng đồ trong trường hợp nào hơn? Mức lương quyết định sự trung thành của bạn hay các chế độ đãi ngộ của công ty?
Chúng ta cứ nghĩ rằng chúng ta hành động một cách có lý trí và hệ thống nhưng khi cảm xúc thống trị thì bạn sẽ không còn nhận ra bạn là bạn của thường ngày nữa.
Đây là một cuốn sách mà tôi cho rằng nó rất thiết thực và hữu dụng cho đời sống mỗi người. Tác giả Dan Ariely viết cuốn sách này dựa trên các nghiên cứu, thí nghiệm về khoa học hành vi và tôi chắc rằng ai trong chúng ta cũng “phi lý trí”.

5
11309
2013-04-19 23:03:38
--------------------------
