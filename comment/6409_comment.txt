565961
6409
Cuốn này rất hay, lời văn đã lôi cuốn mình từ những dòng chữ đầu tiên. Lần đầu đọc giới thiệu chị này người Việt, mình không nghĩ cách hành văn của chị lại hay như thế. Nội dung cũng rất hay nữa, cảm ơn chị Tiểu Châu đã viết ra cuốn sách này, vô cùng cảm ơn chị.
5
2354605
2017-04-06 13:25:54
--------------------------
375027
6409
Người không nhớ tôi sao lẳng lặng nhẹ nhàng khắc sâu vào tâm trí của tôi . Đây là một câu chuyện ngọt ngào thấm sâu vào suy nghĩ của tôi nếu như nhân vật chính nói rằng hạnh phúc nhất của cô ấy là lúc ngủ thì với tôi hạnh phúc nhất là được bên cạnh những người mình yêu thương . Xoay quanh cô ấy là đau thương và mệt mỏi chỉ muốn đắm mình vào giấc ngủ để quên đi cái cuộc sống hiên thực đầy phũ phàng chỉ mang đến cho cô ấy những tổn thương . Xuyên suốt câu chuyện nói về tình bạn , tình yêu trai gái và tình yêu múa bale của nhân vật nữ chính . Tôi thấy cô ấy rất tương đồng với một nhân vật nữ chính trong truyện vũ khúc thiên nga mà tôi đac đọc qua hồi cấp 2 . Cả 2 đều có niềm đam mê bất tận với bale và khao khát được múa đến cháy bỏng
4
1074301
2016-01-27 17:15:06
--------------------------
323361
6409
Giữa thị trường sách tràn lan như hiện nay, sách của Tiểu Châu với mình rất đặc biệt, rất giá trị, rất đáng suy ngẫm. Không phải chỉ là những lời tình cảm bình thường mà sâu hơn cả là cốt truyện, cách xây dựng nhân vật, tâm lí nhân vật, những triết lí được đúc kết, bộc lộ bản chất con người, bản chất của tình yêu, bản chất của cuộc sống này. 

Văn phong của Tiểu Châu nhẹ nhàng, sâu lắng, mơ màng, như gió như mây vậy. Chỉ muốn đọc mãi đọc mãi không ngừng, cầm sách lên là không bỏ xuống được. Mình đã đọc đi đọc lại cuốn này đến mấy lần, mỗi lần lại nhận ra nhiều phần ý nghĩa bên trong. 

Theo dõi fb và page của Tiểu Châu càng thích Tiểu Châu nhiều lần hơn nữa, viết rất hay vẽ lại rất đẹp, tính tình trầm tĩnh, an nhàn, rất khiêm tốn, rất có khí chất. 
Vào đây viết dài dòng vì rất thích sách này và Tiểu Châu. 
Mong rằng sẽ được đọc nhiều sách hơn nữa của Tiểu Châu, làm fan đợi lâu quá, cả năm rồi vẫn chưa ra cuốn tiếp theo. Mà chờ đợi có cái giá của chờ đợi, tin tưởng tác phẩm của Tiểu Châu vô cùng - Author of Sweet
5
884948
2015-10-18 15:16:52
--------------------------
283261
6409
Mình mua quyển sách này bởi quyển này có tựa nghe mông lung, xa vời, gợi nên nhiều cảm xúc.

Tiểu Châu có lối cảm nhận cuộc sống một cách rất thực, không xa hoa, không lãng mạn, không có cầu kì trau chuốt.

Một câu chuyện đầy cảm xúc, nhiều tình yêu, nhưng lại man mác một nỗi buồn, đem cho ta nhiều suy nghĩ. Những nhân vật như được xây dựng để bổ sung cho nhau, để kết hợp với nhau tạo ra những con người, những cuộc đời.

Bìa sách đẹp, nhưng mà vẫn còn bị tình trạng loang màu :'( nhưng chất lượng thì vẫn ok.

Một quyển sách hay
5
621744
2015-08-29 20:52:52
--------------------------
271676
6409
Mình rất thích Tiểu Châu, bởi sự kiên cường của chị ấy. Khi biết chị ấy viết truyện Người không nhớ tôi sao mình đã tìm mua ngay, lối viết của chị ấy vẫn vậy nhẹ nhàng nhưng cũng thật sắc sảo, sâu lắng. Amaya có mang dáng dấp của chị, 1 cô gái theo đuổi cùng cực 1 chàng trai nhưng chàng trai đó không đồng ý, đến khi sự chờ đợi của cô đạt đến giới hạn và trái tim của cô đã dành cho người khác thì chàng trai ngày nào lại đồng ý. Rồi cô quyết định ở bên cạnh thương yêu của mình, thế nhưng cô lại phát hiện ra mình mắc bệnh, cô rời xa anh không nói 1 câu nào. Kết thúc truyện là cảnh anh tìm thấy cô. 1 kết thúc mở, cá nhân mình không thích kết thúc này cho lắm. Nhưng mình nhớ mãi 1 câu "Càng ảo tưởng thì càng đau lòng, lừa gạt ai cũng không đáng sợ bằng việc tự dối gạt chính mình"
5
120071
2015-08-19 12:45:20
--------------------------
174846
6409
Cuốn sách hay đến từng câu, mượt mà, đầy ý nghĩa. Đọc kĩ đọc chậm sẽ thấy rất nhiều điều ẩn ý bên trong. Tình tiết cuốn hút, cách chuyển mạch khéo léo, dẫn dắt người đọc.
Mình đã đọc hết một lần trong hơn 4 giờ, và đang đọc lại lần nữa, nhiều lần nữa, vì rất đồng cảm, rất đáng suy ngẫm.
Rất mong chờ cuốn tiểu thuyết tiếp theo của chị Tiểu Châu, cuốn sách thật sự hay và đáng giá. Những câu chị Tiểu Châu viết đều rất thấm đẫm tình người, đầy trăn trở nhưng nhẹ nhàng, không gây áp lực cho người đọc. 
Thật sự rất thích chị và sách của chị.
5
434005
2015-03-28 22:56:13
--------------------------
174551
6409
phải thừa nhận là mình chưa gặp một cuốn sách nào mà tác giả có một lối viết nhẹ nhàng như thế này.
câu chuyện k kể về các con người Việt Nam mà nói về những con người...(mình cũng không biết nước nào nữa, chắc là nhật bản)... nói chung là xa xôi.
ballet, một môn nghệ thuật mà mik chưa bao giờ biết đến nhưng qua NKNTS mình đã biết đc rất nhiều điều thú vị về nó, như là vở Giselle, các vũ công bale được nhắc tới là nhớ tới ngay bộ đồ váy trắng là vì lí do gì.
một câu chuyện nhẹ nhàng và vô cùng, vô cùng mượt mà, y như là lụa, mình khá kết cuốn này.
tuy nhiên mình vẫn mong ở lần tái bản sau tiểu châu sẽ chỉnh sửa lại đôi chỗ còn chưa hay và bị lủng củng, như thế sẽ rất tuyệt. và tranh nữa, cho thêm mấy bức minh họa ngọt ngào gì đó nữa đi.
3
166271
2015-03-28 12:09:34
--------------------------
123535
6409
"Những câu chuyện buồn chỉ được kể lại khi lòng đã thôi rớm máu, khi một người đang rất buồn hãy tin rằng họ sẽ chẵng bao giờ kể ra đâu. Nhưng khi trải lòng ra được rồi, cũng không hẳn là họ đã hết buồn, mà chỉ đang trải qua một thứ cảm giác khác, một cung bậc khác. Nhưng cũng không phải vui vẻ trở lại, nếu đã vui rồi, nhất định họ chẳng bao giờ nhắc lại chuyện đó nữa." 

Biết Tiểu Châu - Biết cuốn sách này. Nhưng chưa bao giờ có ý định sẽ mua nó. Bởi trước giờ đọc qua khá nhiều sách của các tác giả của VN. Cảm nhận để lại sau những cuốn sách ấy không nhiều. Chỉ 1 số ít trong đó có ấn tượng sâu sắc. Và nghĩ Người không nhớ tôi sao có thể cũng sẽ là 1 trong số phần nhiều kia. Nhưng vì tò mò - thế là ngỏ lời mượn cô bạn thân đọc thử xem thế nào. Khá ấn tượng vì truyện bắt đầu bằng những nhân vật không phải ở VN mà ở 1 đất nước mà mình rất thích ^^ 

Một tác phẩm nhẹ nhàng và sâu lắng. Ta bắt gặp được những tính cách của con người thường nhật. Dư vị tình yêu, thái độ, biểu cảm của người đang yêu được miêu tả tinh tế. 

Thường thì nhân vật chính của truyên cô gái nào cũng hoàn hảo cả, và Amaya cũng không ngoại trừ nhỉ. Mình thích cách tác giả miêu tả những suy nghĩ và hành động của cô gái đó. Chân thật và rất tự nhiên Không biết Tác giả có giống mình thường bị ấn tượng bởi người đầu tiên gặp bởi mùi hương không mà thường Miêu tả về Ichi gắn với mùi bạc hà. - Một chàng trai - niềm ao ước của tất cả các cô gái. Một chàng trai khá tâm lý nhưng khi vào tình yêu rồi cũng đôi lúc hơi ngây ngô 1 tí :D Về Hana - thấy đâu đó có bóng dáng của bản thân mình. Cũng từng trải qua một nỗi đau kinh khủng , dằn vặt - nhưng mình đã chọn cách trải qua khác cô gái này. Ở nhân vật này, các chi tiết xoay quanh nhân vật khá bí ẩn, gây ấn tượng và trăn trở khá nhiều cho mình.

 Và có 1 chút thắc mắc nho nhỏ khi đọc xong tác phẩm bởi nhân vật này. Cũng phải nói thêm về Ken nữa chứ nhỉ - thường thì mình không thích kiểu con trai thế này - nhưng đôi khi vì tình yêu, người ta sẽ trở nên mù quáng.... 

Đó là cảm nhận của mình về tác phẩm này. Nhẹ nhàng nhưng sâu lắng. Rất ấn tượng. Và mình sẽ thêm nó vào bộ sưu tập của mình ^^
4
70874
2014-09-02 13:00:59
--------------------------
