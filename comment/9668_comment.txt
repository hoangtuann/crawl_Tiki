327256
9668
đây là một trong những cuốn sách mà tôi yêu thích nhất trong thư viện của mình và đã đọc rất nhiều lần - một cuốn sách thật sự rất bổ ích cho tất cả chúng ta làm việc. Sách viết rất lôi cuốn, tôi đã đọc liên tục từ trang đầu đến trang cuối và không thể ngưng lại giữa chừng. Cuốn sách giúp tôi rất nhiều trong việc giải quyết những vấn đề rắc rối và căng thẳng trong công việc, cũng như những điều gây khó chịu nơi công sở, còn cho tôi rút ra được một kinh nghiệm "thái độ của bạn đối với những rắc rối sẽ thay đổi khi bạn đã học cách vượt qua chúng". 
5
294223
2015-10-27 14:14:15
--------------------------
