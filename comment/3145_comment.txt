496313
3145
Rất lâu rồi sau "dế mèn phiêu lưu kí" mình mới đọc được tác phẩm văn học Việt Nam cho trẻ em hay đến vậy.
Chú Tiến viết thật đến từng chi tiết về cuộc sống của loài mèo, tập quán, thói quen cực kì chính xác. "Cháu ông" là tên chú mèo thoạt nghe thấy kì lạ, nhưng   lại chứa đựng biết bao yêu thương của người chủ dành cho chú mèo nhỏ. Không phải quan hệ chủ tớ, mà đó là quan hệ ruột thịt. Con mèo/ con chó nếu nhìn với con mắt của bà chủ Đốm Hoa, chỉ đơn thuần là công cụ đuổi chuột, chẳng đáng đối xử tử tế, nói gì đến nâng niu, yêu chiều. Nhưng Ông yêu "cháu ông" vì nó không đẹp bằng Đốm Hoa, lại không được khỏe mạnh. Đó mới chính là tình yêu thực sự. Cuộc sông của chúng không hề dễ dàng chỉ cho ăn, ngủ và vuốt ve. Đứa nào may mắn lọt vô nhà được thương yêu như ông thì mới sung sướng như vậy. Còn lại là cuộc chiến khốc liệt sinh tồn. Và chính con người lại là kẻ thù đe dọa mạng sống của chúng nhiều nhất. Bả, bẫy, đánh đập, sẵn sàng ruồng bỏ khi bọn nhỏ còn chưa dứt sữa mẹ.Tất cả những chi tiết đó chú Tiến không hề hư cấu một chút nào, thực tế còn khốc liệt hơn cả những chi tiết ấy. Câu chuyện của chú gợi lại rất nhiều kỉ niệm của mình về những chú mèo đã và đang ở với mình. Cái chết của mèo mẹ gần giống với cái chết của chú mèo như thành viên trong gia đình mình nhiều năm về trước. sự vứt bỏ mèo con vẫn diễn ra nhan nhản hàng ngày. Có đứa không may mắn thì yểu mạng, có đứa được cứu thì cũng bị tâm lý giống cô mèo mẹ, có đứa sống lang thang như đám mèo giang hồ bị xua đuổi, đánh chém, dội nước sôi,...
Câu chuyện đã lấy nước mắt của mình từ đầu đến cuối.
Và hi vọng rằng chú mèo "Cháu ông" của chú đã hoặc sẽ trở về với chú.
Câu chuyện này không phải là cái kết trọn vẹn như các câu chuyện dành cho trẻ em, nhưng mình nghĩ đây là câu chuyện mọi trẻ em cần đọc, để hiểu, để cảm thông và thay đổi ý thức hệ về cách cư xử với loài vật nuôi vốn tồn tại quá lâu và gây ra quá nhiều hệ lụy.
Cảm ơn chú Tiến đã viết câu chuyện này cho con, cho những bạn yêu quý loài vật và cho các bé thiếu nhi
5
895234
2016-12-17 01:47:11
--------------------------
496262
3145
Dưới con mắt của chú mèo có tên gọi Cháu Ông, mọi sự vật đều hiện lên một cách trong trẻo, hồn nhiên và đáng yêu. Dung lượng sách vừa đủ, không dàn trải. Là 1 quyển sách đáng sở hữu trên kệ sách.
4
462944
2016-12-16 20:26:47
--------------------------
372469
3145
Làm mèo - câu chuyện kể về chú mèo được hết mực cưng chiều. chú mèo đặc biệt ở tâm hồn và suy nghĩ khác lạ so với đồng loại. Chú có một người mẹ và ông bác bị chia cắt từ nhỏ. Câu chuyện thiếu nhi này mở ra cho ta một thế giới khác của động vật, một tình thương khác loài. Đây là một tác phẩm vừa phù hợp với thiếu nhi vừa phù hợp với người lớn, để lại cho ta một chuỗi suy nghĩ về sự mở rộng tâm hồn <3 Các bạn yêu động vật nên tìm đọc
4
940024
2016-01-21 18:05:14
--------------------------
333321
3145
Câu chuyện kể về chú mèo Cháu Ông và quá trình học để làm mèo của cậu. Tuy chỉ là những chuyện xảy ra rất hàng ngày đến nỗi không thể hàng ngày hơn. Từ việc kiếm ăn, đi lang thang, phiêu lưu, bắt chuột nhưng lại cuốn hút đến lạ kì. Ở "Làm Mèo", mình cảm nhận được một chút cao thượng, một chút hy sinh, một chút trắc ẩn, giữa những sinh vật cùng loài cũng như khác loài. Đồng thời ta cũng thấy đâu đó cái sự khắc nghiệt giữa mèo nhà và mèo hoang, mạnh được yếu thua, như chính xã hội loài người vậy. Nhưng tác giả lại cho ta một kết thúc không trọn vẹn khiến mình bị hụt hẫng. Tuy vậy đây là một tác thẩm hay, đáng để đọc
3
39471
2015-11-07 19:57:02
--------------------------
