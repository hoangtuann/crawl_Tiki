366342
10980
"Tư vấn tâm lí học đường là một cuốn sách bổ ích cho giới trẻ ngày ngày nay đặc biệt là học sinh còn ngồi trên ghế nhà trường như học sinh THPT như chúng em. Cuốn sách đã cho em hiểu được nhiều điều về những vấn nạn xã hội hiện nay....Ngoài ra nó còn thật sự hữu ích đối với những học sinh có thể đọc và tìm hiểu nó sau này có thể theo ngành tâm lí học như bản thân em.....Bên cạnh đó tác giả cầm xem lại vì nó còn một số nội dung không phù hợp với học sinh THPT vào lứa tuổi 15.16 hiên nay nữa , vì lứa tuổi này đã phát triển vượt bậc hơn nữa rồi
4
856421
2016-01-09 19:26:42
--------------------------
270470
10980
Tư Vấn Tâm Lý Học Đường (Tác giả Nguyễn Thị Oanh) là tập hợp rất nhiều câu chuyện, rất nhiều câu chuyện về tâm lý với những lời tư vấn thăm tình, cởi mở giúp chúng ta hiểu và có thể áp dụng cho bản thân hay người thân thông qua các lời tư vấn của tác giả. Nói dụng rất hay, dễ áp dụng thực tế.
Nhưng theo tôi, sách không có phần mục lục hơi khó để có thể tham khảo nhanh lại phần mình muốn đọc lại. Theo tôi có thêm mục lục thì cuốn sách sẽ hoàn hảo hơn.
4
504087
2015-08-18 11:44:54
--------------------------
248167
10980
Mình nghĩ để viết một cuốn sách như thế này thì chắc hản tác giả đã phải bỏ ra rất nhiều công sức nghiên cứu. Tư vấn tâm lý học đường đã nêu ra đúng những vấn đề mà các bạn học sinh - sinh viên thường gặp phải, một cách cởi mở và thoải mái nhất có thể. Cuốn sách không chỉ giúp những bạn đọc trong lứa tuổi học đường đọc và hiểu hơn về bản thân, mà còn giúp các bậc phụ huynh, các thầy cô giáo hiểu hơn về con em và học sinh của mình.
5
727120
2015-07-30 18:26:32
--------------------------
242170
10980
Mình nghĩ để viết một cuốn sách như thế này thì chắc hản tác giả đã phải bỏ ra rất nhiều công sức nghiên cứu. Tư vấn tâm lý học đường đã nêu ra đúng những vấn đề mà các bạn học sinh - sinh viên thường gặp phải, một cách cởi mở và thoải mái nhất có thể. Cuốn sách không chỉ giúp những bạn đọc trong lứa tuổi học đường đọc và hiểu hơn về bản thân, mà còn giúp các bậc phụ huynh, các thầy cô giáo hiểu hơn về con em và học sinh của mình.
4
557111
2015-07-26 10:40:16
--------------------------
211132
10980
Hiện nay, "tư vấn tâm lí học đường" là một hoạt động mới được manh nha xây dựng và hình thành ở một số trường học với mục đích hỗ trợ, giúp đỡ và định hướng cho các em học sinh. Và lĩnh lực này đang được toàn xã hội quan tâm, bởi lẽ hiện nay các em học sinh có quá nhiều sự tác động từ bên ngoài mà chính bản thân các em lại còn quá kém cõi về các kĩ năng sống cần thiết, nhiều bậc phụ huynh phải đau khổ vì không biết tại sao con mình nó ngoan ngoãn mà giờ lại hư hỏng đến thế... Cuốn sách với nhiều câu hỏi, nhiều tình huống đặt ra gần như có thể giúp các thầy cô, các bậc cha mẹ hiểu hết tâm tư nguyện vọng, những khó khăn mà các em đang trong giai đoạn tuổi mới lớn gặp phải. Xin cảm ơn tác giả rất nhiều và đã cho ra đời cuốn sách quý giá này và mong rằng tác giả sẽ có thêm nhiều công trình khác hỗ trợ cho các thầy cô, các bậc phụ huynh trong việc thấu hiểu những mầm non tương lai của đất nước.
5
455308
2015-06-20 09:24:21
--------------------------
79566
10980
" Tư vấn tâm lý học đường" thật sự là một cuốn sách bổ ích. Cuốn sách này hầu như đã giải đáp gần hết được những câu hỏi mà mình đã thắc mắc từ bấy lâu nay. Nó đã giúp mình hiểu ra và thông suốt được nhiều điều. Cuốn sách này không chỉ thực sự bổ ích đối với lứa tuổi còn cấp sách đến trường như mình mà còn quý báu cho các bậc cha mẹ, phụ huynh. Thông qua cuốn sách " Tư vấn tâm lý học đường", các bậc phụ huynh sẽ hiểu hơn về con cái của họ, những vấn đề mà con em họ có thể thắc mắc hoặc gặp phải trong giai đoạn tuổi mới lớn như vậy. Đây thật sự là một cuốn sách rất bổ ích và ý nghĩa.
5
121026
2013-06-07 22:17:46
--------------------------
