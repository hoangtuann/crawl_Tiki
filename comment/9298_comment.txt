115306
9298
Cô giáo Rachel chính là định mệnh kéo Johnny ra khỏi đống bùn lầy mà tuổi thơ khắc nghiệt và định kiến khắt khe của những con người nơi thị trấn nhỏ gây ra. Johnny của 10 năm sau không còn là cậu học sinh nhỏ ngày nào mà cô che chở. Anh đã thành một người đàn ông bụi bặm, ngang tàng, ăn nói sỗ sàng và đầy cay độc. Nhưng Rachel vẫn tin anh, tin rằng đằng sau lớp vỏ xù xì, gai góc đó là một tâm hồn trong sạch, lương thiện. Còn Johnny, khoảng thời gian 10 năm không thể bào mòn tình cảm anh dành cho cô giáo tốt bụng của mình. Anh coi cô là điều đẹp đẽ duy nhất còn sót lại trong đời anh, bám lấy tia sáng ấy để tiếp tục tồn tại, vươn lên trước miệng lưỡi người đời. Mình đã có rất nhiều cảm xúc khi theo dõi diễn biến chuyện tình cảm và yêu cả cách họ vượt lên rào cản xã hội. Cả hai đã rất dũng cảm. Rất thương nhân vật John Harris và hạnh phúc khi anh đã có một cái kết có hậu xứng đáng (mặc dù nếu kết truyện anh vẫn ở lại thị trấn và được mọi ng dần chấp nhận sẽ có hậu hơn, nhưng cái kết của Karen có vẻ hợp lý và thực tế hơn thì phải, hì).
Một câu chuyện lãng mạn pha chất trinh thám nên rất hấp dẫn, hồi hộp. Mình chấm cuốn sách 5 sao nhưng trừ 1 cho cái bìa. Haizzz, sao lại chọn 1 cái bìa đơn điệu, thiếu lôi cuốn như vậy chứ??? Cũng may mà mình có đọc lời tựa, nếu không đã "nhìn mặt mà bắt hình dong", sẽ bỏ qua 1 cuốn sách hay như vậy. Hihi
4
308069
2014-06-26 13:01:26
--------------------------
113729
9298
Tôi không chọn được từ nào ngắn gọn để làm tiêu đề cho câu chuyện này nên lấy tên sách vậy, cái tên hợp nhất, mùa hè định mệnh, mùa hè đem hai con người đến với nhau bởi tình yêu bất chấp mọi rào cản, bất chấp thời gian, bất chấp tuổi tác, bất chấp địa vị... Nói truyện lãng mạn thì không hẳn chỉ như vậy, truyện mang phong cách phương Tây đặc trưng, có lãng mạn, có mãnh liệt, có sức hút cơ thể và tâm hồn. 
Truyện được kể với diễn biến nhanh gọn, Rachel có những lo nghĩ của người phụ chín chắn trước quyết định lâu dài với Johnny và những lo nghĩ ấy là điều thực tế. Nhưng rồi họ đã cùng cố gắng để thích hợp với nhau, để đến với nhau mà không màng đến những khác biệt hay rào cản.
Yếu tố trinh thám đơn giản nhẹ nhàng khiến cho truyện thu hút hơn, là kiểu trinh thám lồng ghép trong truyện lãng mạn phương Tây mà có thể bạn đã gặp trong khá nhiều truyện.
4
49203
2014-06-04 20:16:04
--------------------------
103281
9298
Đây là quyển sách bóp nghẹt ta trong từng khoảnh khắc
Ngay từ lần gặp đầu tiên, cậu học trò năm xưa đã đem đến cho ta một cảm giác về 1 sự thay đổi lớn về cuộc sống ở thị trấn này
Môtíp tình yêu cô- trò quen thuộc nhưng cách dẫn dắt, cách miêu tả của tác giả thật tuyệt vời. Ta như kiệt sức để theo kịp cái ngọn lửa tình yêu cuồng nhiệt của Johnny. Cách anh thầm yêu Rachel trong suốt quãng thời gian qua, những suy nghĩ đen tối, những khao khát của anh dành cho cô thật mãnh liệt
Rachel bị nhiều rào cản, rào cản gia đình, dư luận xã hội, tuổi tác, địa vị nhưng sau cùng bất chấp mọi thứ, cô vẫn tin Johnny và yêu hết mình.
Tình yêu của 2 người không sến súa mà cuồng nhiệt, quá nhiều rung động
Kết thúc truyện có hậu tuy nhiên vẫn hơi hụt hẫng vì mình thích một cái kết lãng mạng hơn. Việc hai người rời khỏi thị trấn nhỏ đó nơi xảy ra biết bao chuyện, biết bao hiểu lầm. Quả thật bỏ đi là lựa chọn đúng đắn vì định kiến con người khó lòng thay đổi dù Johnny được chứng minh trong sạch. Đến đây ta nhìn thấy một mặt khác của đời sống thực : con người là sinh vật ích kỉ và dĩ nhiên ta phải lo cho chính mình, cho gia đình mình trước, ta sẵn sàng chà đạp người khác để bảo vệ mình và gia đình. Johnny là một nạn nhân của việc bị oan nhưng không có tiếng nói, không ai che chở
Mình rất thích khúc Johhny bộc bạch hết lòng mình cho Rachel, sau cái chết của người cha tàn nhẫn mà anh vẫn động lại trong mình tình cảm phụ tử. Thật nghẹn ngào
Đây là một quyển sách hay tuy nhiên sách in lỗi nhiều quá: lặp lại đoạn nhiều lần, sai chính tả. Khi đọc gây cảm giác khá khó chịu
5
129848
2013-12-28 23:07:51
--------------------------
90629
9298
Mùa hè định mệnh giống như là một tuyên ngôn khẳng định chân lý trong tình yêu: tình yêu giữa người với người là không phân biệt tuổi tác, xuất thân sang hèn, hay học thức. Nó chỉ đơn giản từ nỗi cảm thông sâu sắc và những xúc cảm trọn vẹn dành cho nhau. Một cô giáo nhà giàu gia giáo, học rộng hiểu cao, có địa vị cao trong xã hội lại yêu một tên lưu manh, nhà nghèo, sống trong khu ổ chuột,thường xuyên chửi thề, và mang danh tù tội? Liệu câu chuyện đó có hợp lí không cơ chứ? 
Những tình tiết câu chuyện từng chút một kéo hai người lại với nhau, không chỉ là cái e ngại nhìn chằm chằm vào một thân hình vạm vỡ quyến rũ mà đó còn là những sẻ chia cảm thông vì một quãng đời khó khăn. Anh hiểu cô muốn gì và cô biết anh đã trải qua những gì. Lần đầu tiên mình đọc được một truyện mà tình yêu giữa hai người là xuất phát từ những chi tiết nhỏ nhặt và nó là cả một quá trình dài: tìm hiểu, sẻ chia và đến được với nhau bằng rụt rè, đam mê, rồi lại dè dặt vì xã hội. 
Có thể xã hội không chấp nhận mối quan hệ đó, nhưng cái quan trọng là tình yêu mãnh liệt đã chắp cánh cho họ can đảm để có thể bên nhau.
Câu chuyện còn xen vào một số yếu tố trinh thám nhẹ, làm cho câu chuyện không nhàm chán mà khá thú vị.
*Một số nhận xét khác:
- Bìa cuốn sách đơn giản nhưng rất thanh lịch
- Một số từ ngữ còn sai chính tả, thậm chí có một đoạn văn dài được lặp lại 2 lần.
- Nội dung lúc yên bình cuốn hút, lúc nghẹt thở đến hồi hộp
- Lối viết nhẹ nhàng nhưng cũng rất hấp dẫn
Mình cho cuốn sách này 9/10 điểm
5
15919
2013-08-12 03:13:41
--------------------------
82163
9298
“ Mùa hè định mệnh” có một phần giới thiệu không quá hấp dẫn nhưng tôi vẫn mua nó bởi tôi muốn xem kết quả của một tình yêu.Tác giả mang lại cho người đọc vô vàn những cảm xúc ngọt ngào và mãnh liệt của tình yêu, tò mò một thế giới tâm linh và những đau đớn của một cuộc đời không hoàn chỉnh. Johnny một chàng trai bị bắt vì tội cưỡng dâm 10 năm trước đã trở nên sỗ sàng, ngang bướng và bụi bặm của 10 năm sau nhưng tình yêu của Johnny giành cho cô giáo của mình chưa bao giờ thay đổi. Cuộc gặp gỡ định mệnh của 10 năm sau khiến hai con người hai trái tim thổn thức tìm đến nhau. Dù cách nhau 5 tuổi, dù vấp phải những phản đối của gia đình nhưng họ vẫn đến với nhau với một trái tim cuồng nhiệt đầy yêu thương. Tình yêu với họ đã vượt qua tuổi tác và rào cản của tất cả. Có bao người đã tìm được mùa hè định mệnh của riêng mình. 
4
97873
2013-06-20 08:40:14
--------------------------
80856
9298
Việc "Mùa hè định mệnh" được đưa vào danh sách 100 tiểu thuyết lãng mạn hay nhất thế kỷ đã khẳng định được những giá trị tuyệt vời của tác phẩm này. Đây thực sự là một cuốn sách hay và lôi cuốn. Nhà văn Karen Robards với một giọng kể đầy mê hoặc có thể khiến bạn cười, khiến bạn ngập tràn hạnh phúc, và cũng có thể khiến bạn bật khóc. Câu chuyện tình yêu giữa Johnny Harris và Rachel Grant có thể không quá mới mẻ hay đặc biệt, nhưng bằng những khắc họa đầy sắc sảo, lối dẫn dắt tinh tế cùng một ngòi bút biến hóa bất ngờ, tác giả đã đem đến cho câu chuyện ấy một vẻ tươi mới và cuốn hút đến tận cùng. "Mùa hè định mệnh", mùa hè của 10 năm sau khi Rachel gặp lại Johnny - người học trò cũ, một tình yêu đã chớm nở, một cảm xúc mãnh liệt đã trỗi dậy bên trong trái tim, khiến hai con người ấy đến với nhau bất chấp biết bao thử thách khắc nghiệt của số phận.
Cuốn sách đã cho chúng ta thấy một điều thật ý nghĩa rằng, tình yêu luôn là điều tuyệt diệu nhất, mạnh mẽ nhất trong cuộc đời mỗi chúng ta.
5
109067
2013-06-14 14:04:23
--------------------------
80383
9298
Ban đầu đọc review mình cũng không định mua truyện này vì cảm thấy không được hấp dẫn lắm. Nhưng sau khi được tặng thì mình cảm thấy đây là một cuốn sách hay. Tuy không đặc sắc nhưng mình thích cách tác giả đan xen câu chuyện giữa trinh thám và lãng mạn. 
Một Jonny cố gắng vượt qua số phận nghiệt ngã để đến với tình yêu cả đời mình, một Rachel gia giáo, nề nếp nhưng cuối cùng vẫn bị tình yêu đích thực cuốn đi. Câu chuyện là bức tranh đẹp về tình cảm con người, về một thị trấn bé tí hà khắc, phong kiến. Một bức tranh sống động, chân thực, tuy nhiên mình không thích cách Karen để cho 2 nhân vật chính phải ra đi, phải bỏ lại nguồn gốc, bỏ lại gia đình, bỏ lại nơi mình sinh ra, lớn lên. Câu chuyện tuy kết thúc có hậu nhưng vẫn không khiến người đọc thật sự thỏa mãn với cái kết cần có của một cuốn tiểu thuyết lãng mạn.
5
125574
2013-06-12 16:53:49
--------------------------
74714
9298
Thứ nhất không hiểu sao tôi không thích cách trình bày của bìa sách, cũng không hiểu vì sao, có lẽ là bởi với một cuốn sách được cho là tuyệt như thế này thì có vẻ cách trình bày bìa được cho là đơn giản đến khó chịu.

Thứ hai, tôi thấy được nội dung của câu chuyện, khá hay đấy chứ, bạn có thể dễ dàng thấy được tình tiết của câu chuyện sẽ ngày càng hấp dẫn khiến bạn không thể bỏ cuốn sách được khi mà chưa đọc được cho kì hết. Tuy nhiên tôi vẫn thấy cuốn sách đôi chỗ vẫn khá nhạt. Tôi không thích cách xây dựng hình tượng nhân vật theo cách của tác giả, có vẻ là hơi thiếu logic thì phải?

Thứ 3, lần này là khen, tôi khá hài lòng về những câu văn của tác giả, tuy không hài lòng lắm về nhân vật mà bà xây dựng nhưng tôi thích cách viết của bà, câu văn được chau chuốt rất tỉ mỉ, nhiều cảnh lồng ghép khá ăn ý đến lôi cuốn, cách nhìn của bà khá hay.

Thứ 4, không thể phủ nhận dù thế nào thì đây cũng là một cuốn sách đáng để đọc♥
3
104997
2013-05-14 19:09:36
--------------------------
72470
9298
lúc đầu thấy tên truyện thì mình không có hứng thú cho lắm, tên truyện đối với mình không cuốn hút, và đúng như lời giới thiệu về truyện cũng không làm mình hứng thú , cô giáo và học trò thể loại mình cũng không hứng thú . Và mình đọc nó hoàn toàn là do ngẫu nhiên , bạn mình mua và mình mượn đọc ( do rảnh ý mà) nhưng khi đọc xong thì thái độ mình thay đổi hoàn toàn. tình yêu cô giáo hay j` j` đi nữa cũng là tình yêu, nhưng nổi bật là câu chuyện đã xãy ra 11 năm trước được làm rõ , tình mẹ con, và có 1 chút tình huống tâm linh ...... đọc sẽ biết thêm chi tiết =]]
5
51336
2013-05-02 20:04:38
--------------------------
61703
9298
"Mùa hè đình mệnh" là tác phẩm đầu tiên mà mình được đọc của nhà văn Karen và cũng là một trong những cuốn mình thích gần đây. Tác giả đã tạo nên được một câu chuyện khá đặc sắc và mới lạ, nhưng vẫn giữ được những nét quen thuộc của dòng văn chương lãng mạn. Mình thích nhất cách bà miêu tả sự thay đổi của nhân vật nam chính, từ một cậu con trai nghịch ngợm trở thành một người đàn ông "với vẻ ngoài nguy hiểm". Chính sự thay đổi này đã làm đảo lộn mọi thứ, cuốn cả hai nhân vật chính vào một vòng xoáy mãnh liệt của tình yêu, nơi mọi định kiến và giới hạn bị xóa bỏ. Tất cả đã làm nên một cuốn truyền đầy hấp dẫn, truyển tải những thông điệp nhân văn, đồng thời ẩn chứa những khắc họa tinh tế về nghệ thuật.
5
20073
2013-03-03 15:56:06
--------------------------
58959
9298
One summer (Mùa hè định mệnh) của Karen Robards là tác phẩm đầu tiên tôi đọc của nữ văn sĩ này mặc dù tôi có đến hai tác phẩm của bà trong tay - Julia yêu dấu nữa.
Nhìn chung, cuốn tiểu thuyết cuốn hút tôi và buộc tôi phải có bằng được bởi cốt truyện hấp dẫn và rất thực của nó. Tuy có một số  rối rắm nhưng cuốn tiểu thuyết vẫn đáng để bỏ thời gian ra đọc.
Thường thi bước vào một cuốn tiểu thuyết lãng mạn đa phần tôi đều thích và mến nhân vật nữ hơn nam (trừ một số cuốn nhưng tôi không tiện nói ra) và ở chuyện tình này thì tôi lại thích nhân vật nam chính hơn - John Wayne Harris. Bởi nỗi đâu anh đã chịu đựng, sự gian khổ anh phải gánh chịu và sự bất công số phận mang đến cho một chàng trai tốt bụng. Johnny có lẽ không đến nỗi biến thành một tên ma mãnh, ăn nói sổ sàng nếu không có sự kì thị vô cớ hay sự nhầm lẫn điên rồ của cái gọi là đám đông. Còn Rachel Elisabeth Grant là một cô giáo tốt nhưng suy nghĩ của cô dường như chưa được diễn đặt rõ. Tôi không biết tình yêu cuồng nhiệt Rachel cảm nhận là tình yêu (như tác giả viết) hay chỉ đơn thuần là cảm giác thương hại. Nhưng khi tác giả đã đặt chữ "yêu" vào những diễn biến sau thì câu chuyên dễ thở hơn và dần đi theo chiều hướng trinh thám pha chút lãng mạn hơn. Tác giả không hề giấu diếm kẻ sát nhân mà đưa ra từng manh mối nho nhỏ để người đọc cũng có thể cùng nhân vật mà điều tra kẻ giết người (điều đáng tiếc là tôi để ý quá trễ - vì tôi là một độc giả lãng mạn - những chi tiết ấy để có thể truy tìm hung thủ cùng hai nhân vật) và khi đã làm được như vậy thì cái tài của tác giả chính là nằm ở đó. Có lẽ tôi thấy Robards tài hơn những cây viết khác là ở chỗ tạo cơ hôi cho người đọc đoán ra kẻ sát nhân rồi đánh lạc hướng bằng nhiều chương nói lên suy nghĩ của hắn, cấu hình nên chất trinh thám nhưng lại không lươm thượm chút nào khi xây dựng một tình yêu sâu đậm (ở đây là chất lãng mạn nhé) và nhiều yếu tố nhân văn mà đôi khi ta lại tìm thấy trong vài dòng tự sự.
Bạn đọc nhận xét trước có vẻ không thích Rachel bởi "sự thích thú khi chia tay bạn trai cũ" nhưng theo tôi thì ít ra khi cô đã xác định rõ tình cảm của mình đối với Johnny, kết quả phải đến là chia tay Rob dù cho có thế nào. Sao lại gọi là "bắt cá" mặc dù cô chia tay Rob khi biết chắc mình yêu Johnny? Và, Rachel không hận Michelle Henessy vì bỏ cô mà cưới Becky Grant, chỉ là đau buốn vì một tình yêu đã mất - một tình yêu cướp đi của cô những mộng mị của tuổi trẻ. Đồng ý là có vấn đề với suy nghĩ của Rachel nhưng cô cũng không đến nỗi xấu tính lắm.
Như đã nói trên thì câu truyện mang đến nhiều ý nghĩa sâu xa : bất chấp địa vị, tuổi tác, tầng lớp, nghề nghiệp, quá khứ - tình yêu mang hai con người đã được định sẵn với nhau bằng một sự sắp đặt nhỏ, tài tình quá phải không? Nó chứng minh cho tôi thấy suy nghĩ, đánh giá của bản thân của ta về quá khứ của người khác có ra sao cũng sẽ là sai lầm lớn khi ta chưa tìm hiểu mà mở lòng ra với họ.
Về cuốn tiểu thuyết thì theo tôi thấy có một đoạn bị lặp, một số chỗ bị sai chính tả và lỗi viết tắt do biên tập không kĩ lưỡng nhưng dù sau cũng không đáng kể.
Mong hãng Thái Uyên sẽ xuất bản thêm nhiều cuốn tiểu thuyết khác có nội dung mới và hay hơn nữa.
5
19049
2013-02-08 17:13:57
--------------------------
58910
9298
Cuốn sách này đã cuốn hút tôi ngay từ khi đọc qua lời giới thiệu và quả thật là rất hay. Khi đọc tôi cảm thấy nó gần giống như "Núi tình" của L.Howard, cũng là nhân vật nam chính bị kết tội hiếp dâm và chịu nhiều thương tổn trong tâm hồn -> trở nên cay nghiệt với mọi người xung quanh, điểm giống thứ 2 là người phụ nữ đem đến tình yêu, hạnh phúc cho cả hai nhân vật nam chính đều là cô giáo. Cả 2 tác phẩm tôi đều đã đọc và nhận định chung của tôi là đều hay, mỗi tác phẩm đều mang một nét đặc trưng, một phong cách riêng. Đối với "Mùa Hè Định Mệnh", cuốn sách tuy lãng mạn nhưng cũng pha chút trinh thám, đặc biệt là cách dùng từ theo tôi là phóng khoáng nhất trong tất cả những quyển sách đã đọc, nhưng có như vậy thì nó mới thể hiện được tất cả những mặt trong con người Johnny, kẻ đã bị kết án, đã phải ngồi tù cách oan uổng suốt 10 năm, đã chịu nhiều đau khổ và có một tuổi thơ bất hạnh nên việc anh trở nên cay nghiệt và có những lời lẽ phóng túng để đánh vào những kẻ thù ghét xa lánh anh là không có gì ngạc nhiên nhưng cái hay của cuốn sách ở đây là tuy thấy thô tục như vậy nhưng không hề thô chút nào mà ngược lại trở nên rất tinh tế và vô cùng sâu sắc. Tình yêu mà Johnny giành cho cô giáo hơn mình 5 tuổi cũng là thứ tình yêu mà rất nhiều người khao khát, nó quá chung thủy, quá cảm động, khi đã giành được trái tim của cô rồi anh đã hoàn toàn thay đổi con người mình, đã yêu cô một cách chân thành và giành tất cả lòng quan tâm của mình cho cô. Đối với Rachel cũng thế, cô cũng rất tuyệt vời khi không ruồng bỏ cậu học trò năm xưa nhưng vẫn tin tưởng, vẫn quan tâm và tìm cách giúp đỡ Johnny mặc cho những lời xầm, nói xấu Rachel vẫn hết mình với Johnny và đã vô cùng dũng cảm để đón nhận tình yêu của anh.
Mua cuốn sách này thật sự không uổng, tuy còn nhiều thiếu sót về chính tả, lỗi in ấn nhưng nhìn chung Karen Robards đã thành công trong việc viết nên tác phẩm. ngoài hai nhân vật chính, diễn tiến cảnh rượt đuổi, truy sát của kẻ giết người điên loạn cũng khiến câu chuyện trở nên hồi hộp, gay cấn. Johnny & Rachel, một hình ảnh đẹp của tình yêu chung thủy tuy khác biệt nhau về tuổi tác, về địa vị xã hội nhưng không hề có sự khác biệt trong tình yêu.
5
41370
2013-02-08 10:23:39
--------------------------
57627
9298
Cốt truyện cũng khá thú vị, diễn biến về sau đọc khá cuốn hút, hấp dẫn, tạo cảm giác ly kỳ. Ba sao là dành cho nội dung cốt truyện.

Tuy nhiên, hai nhân vật chính khá chán. Không thấy có sự logic trong diễn biến tâm lý của nữ chính, mà hầu như chỉ thấy nhân vật lao theo cảm tính của bản thân, ko suy nghĩ đến bất kỳ ai khác ngoài mình và nam chính. Đặc biệt không hiểu sao tác giả lại xây dựng nữ chính bắt cá hai tay nhưng không một lần có cảm giác áy náy hay hối hận với anh chàng người yêu cũ, cả khi đã ngủ với nam chính rồi mà nữ chính vẫn tiếp tục lợi dụng và đi chơi với anh người yêu kia, không một lần có ý định nói sự thật để chia tay quách cho xong mà chỉ săm soi vào những điểm "xấu" của anh ta; thậm chí còn cắm cho anh ta một cái sừng to oạch ngay giữa đông đảo người dân trong thị trấn rồi lấy làm rất hả hê về hành động ấy của mình, trong khi anh ta không phải là mẫu nhân vật tệ hại chút nào, mà chỉ là ko hợp với cô Rachel ấy. Đọc đến đoạn ấy mình cứ nghĩ, 10 năm trời Rachel oán hận anh chàng Michael vì đã lừa dối tình yêu của mình để chạy theo cô em gái xinh đẹp nhưng hời hợt, nhưng chính bản thân Rachel cũng lừa dối tình yêu của anh chàng người yêu cũ kia  để chạy theo kẻ mà cả thị trấn coi là tội đồ, vậy thì Rachel đâu có khác gì mấy so với cái gã Michael bội bạc, vô trách nhiệm kia???

Nam chính cũng ko phải là mẫu người nổi bật. Đồng ý anh ta bị coi thường cả thời thơ ấu nên trở thành con nhím, xù lông lên với bất kỳ ai đến gần. Thế nhưng trong mối quan hệ đặc biệt với cô giáo Rachel, vẫn có cảm giác gì đó là anh ta chỉ muốn làm theo ý mình mà ko hề quan tâm xem đối phương nghĩ gì. Quá trình họ đến với nhau vẫn cứ cảm giác có gì đó ép buộc từ phía Johnny. Nhưng đến đoạn cuối truyện thì nhân vật này dễ chịu hơn.
3
91042
2013-01-31 12:40:57
--------------------------
