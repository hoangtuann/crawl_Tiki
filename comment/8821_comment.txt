396760
8821
Nhân Danh Học Việt Nam do Phó giáo sư, tiến sĩ Lê Trung Hoa viết thật sự tôi thấy khá hay. Chỉ là cái tên thôi mà cũng cả là những công trình nghiên cứu công phu. Cuốn sách tuy mỏng nhưng nó khái quát được những gì phổ biến và sơ lược nhất. Cuốn sách được chia thành các phần chính như họ, tên đệm, tên chính v.v... sách còn nêu những quy tắc khi đặt tên, bảng khái quát tên, lược sử, những điều nên tránh. Mình thấy sách phù hợp cho những ai muốn tìm hiểu hơn về vấn đề nhân danh học. Vì mình thấy nhiều người đặt tên  cho con cái rõ hay và kêu nhưng cũng không rành ý nghĩa về tên cho lắm. 
4
854660
2016-03-14 00:06:47
--------------------------
244800
8821
Sách mỏng hơn mình tưởng tượng rất nhiều, thành ra cũng có đôi chút hụt hẫng. Tuy nhiên nội dung trong sách lại khá chi tiết về ý nghĩa của tên, họ... ở Việt Nam, đồng thời cũng đưa ra những bảng thống kê để mọi người có thể tham khảo. Tài liệu tham khảo đưa ra cuối sách khá nhiều, nếu tìm đọc hết số tài liệu đó thì có lẽ... *cười* Thực ra thì nói chuyện về tên họ có khi nói cả ngày cũng chẳng hết nữa, nếu nói tỉ mỉ chắc cuốn sách sẽ dày và bự cỡ quyển từ điển mất.

Điểm trừ cho sách là chất lượng in ấn không tốt lắm, không hiểu vì giấy mỏng hay mực đậm mà nhiều trang gần như nhìn xuyên thấu được cả mặt sau. Tuy nhiên đây là một cuốn sách hay, nhưng vẫn mong nếu được tái bản thì cuốn sách sẽ được bổ sung nhiều hơn về nội dung, trau chuốt hơn trong trình bày, tóm lại là hoàn thiện hơn bản hiện hành.
4
128054
2015-07-28 14:37:36
--------------------------
174398
8821
Cuốn sách bao gồm rất đầy đủ các mảng mà người đọc nên biết về tên họ người Việt Nam, bao gồm cả cách đặt tên từ trước đến nay, ý nghĩa của tên gọi... Sách được kết cấu khoa học, dễ đọc, dễ nhớ, là cuốn sách tốt cho những ai mới bắt đầu tìm hiểu về lĩnh vực này. Riêng cá nhân mình thì muốn đào sâu về lịch sử hơn một chút, ví dụ như vấn đề phạm húy, nhưng cũng may là cuốn sách có liệt kê tài liệu tham khảo, sau này có thời gian cũng dễ tra cứu.
4
513440
2015-03-27 22:02:34
--------------------------
