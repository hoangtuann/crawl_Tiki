285116
6675
Một quyển sách độc đáo mà mình đã đọc qua. Mark V. Hansen là một trong những tác giả viết sách mình yêu thích nhất. Quyển sách này mình nghĩ cần đổi tên là "Ai muốn là triệu phú thực sự". Tác giả đã dung hòa được yếu tố làm giàu và cung cấp các giá trị đích thực cho xã hội và những người xung quanh. Một quyển sách làm giàu kinh điển, bền vững và rất nhân văn. Một quyển sách dạy làm giàu từ chuyên gia tâm lý, tác giả của bộ "Quà tặng cuộc sống". Mình rất thích định nghĩa về "Doanh nhân khai sáng" - một triệu phú đô la thực sự là một người biết định hướng đến sau cùng là những giá trị cho đi, những phúc lợi xã hội, vượt lên trên định nghĩa làm giàu bằng lợi nhuận và thặng dư lao động. Một quyển sách rất hay, rất đáng để mỗi người đọc lại nhiều lần...
4
138711
2015-08-31 16:18:41
--------------------------
216711
6675
Một cuốn sách hay truyền cảm hứng cho bạn làm giàu. Ở mỗi chương của cuốn sách, bạn sẽ được chia sẻ các câu chuyện có thực về những nhân vật hoàn toàn bình thường, nhưng lại nắm được cách giải mã mật khẩu của sự giàu có và làm nên một sự nghiệp đáng nể từ chính những ý tưởng của mình. Họ làm ra hàng triệu đô mỗi năm và cũng sẵn sàng dùng hàng triệu đô đó để kiếm lại nhiều hơn, mọi thứ đều được quyết định bởi tác phong và mật mã làm giàu. Một cuốn sách đáng đọc.
4
13083
2015-06-28 09:54:53
--------------------------
172199
6675
Làm giàu là một việc quan trọng trong cuộc đời. Hãy làm giàu ngay từ bây giờ nhưng đừng nên đánh đổi sức khỏe, gia đình, bạn bè để đạt được chúng. Nhưng làm sao để đạt được mà không bất chấp tất cả. Tôi thấy những người thầy mà tôi ngưỡng mộ dạy cực kì giỏi, học sinh cực kì ái mộ, tiền kiếm được nhiều vô kể nhưng một ngày chỉ ngủ được 3 tiếng. Donald Trump, tỷ phú người Mỹ cũng ngủ một ngày có 3 tiếng. Nhưng trong quyển sách này, Mark Victor Hansen sẽ chỉ cho chúng ta cách làm được điều đó bằng 4 mật khẩu để làm giàu bền vững. Hãy đọc và tìm hiểu để giàu có trong tất cả mọi mặt chứ không chỉ là tiền bạc.
5
387632
2015-03-23 15:32:27
--------------------------
118108
6675
không phải ai sinh ra cũng được may mắn sống một cuộc sống giầu sang.Vậy làm thế nào để thay đổi được số phận của mình?Hơn ai hết,chúng ta phải phát triển được các kỹ năng: xây dựng sự tự tin, cách quản lý thời gian hiệu quả và hành động theo cảm tính. Quyển sách này viết ra chính là để giúp bạn và tôi giải mã mật khẩu giầu sang của mình. Những câu chuyện cụ thể,chân thực tạo cảm hứng cho những ai đã từng đọc nó và giúp ta vẽ ra được con đường nhanh nhất,chắc chắn nhất cũng như an toàn nhất để đến được cái đích mà chính ta đặt ra.
           Đây chính là cuốn sách gối đầu giường cho những ai luôn ấp ủ ý định thay đổi cuộc sống của mình!
           Hãy bỏ chút thời gian ra để đoc quyển sách ý nghĩa này vì có thể nó sẽ làm thay đổi cuộc đời mỗi chúng ta!
4
381173
2014-07-26 09:04:46
--------------------------
