554085
8248
Cảm ơn những giá trị đạo đức và nhân văn từ cuốn sách quý. Cuốn sách giáo khoa của mọi thời đại!
5
2130638
2017-03-25 15:40:58
--------------------------
472473
8248
Mình thích quyển sách này ngay từ khi nhìn thấy nó, bìa sách đẹp, giấy in cũng đẹp, đặc biệt cái tên của nó "Quốc Văn giáo khoa thư" như đưa mình về lại quá khứ, cái thời mà vật chất chưa lên ngôi như bây giờ, người ta chú trọng đến đạo đức, và hiển nhiên, trẻ em là những thành phần được ưu tiên giáo dục đạo đức. Những câu chuyện nhẹ nhàng, đơn giản, gần gũi, đọc xong thấm liền, không hề có lời lẽ sáo rỗng, và cao xa, gây khó hiểu cho lứa tuổi nhi đồng.
5
273998
2016-07-09 17:06:08
--------------------------
461006
8248
"Luân lý giáo khoa thư" và "Quốc văn giáo khoa thư" là 2 cuốn sách Dượng Tony giới thiệu mua.
Đây là những bài học đạo đức ngắn gọn súc tích, có ví dụ cụ thể rõ ràng, và tương tác với học trò bằng câu hỏi cuối bài. 
Sách được sử dụng trong giảng dạy từ đầu thế kỷ XX nhưng cho đến bây giờ vẫn nguyên giá trị.
Tuy nhiên vẫn còn một số điều không còn phù hợp lắm trong thời buổi hiện đại.
Ví dụ như: sách viết con cái phải vâng lời cha mẹ, cha mẹ lớn tuổi kinh nghiệm sống nhiều nên lời cha mẹ nói là đúng, cần phải nghe theo, nghe theo lời cha mẹ là hiếu thuận. Thiết nghĩ, cha mẹ bây giờ chưa hẳn nói gì cũng đúng, nên dạy cho trẻ cách đưa ra ý kiến riêng của mình nếu cha mẹ sai, trên tinh thần tôn kính cha mẹ.
Cha mẹ vẫn có thể dạy con những bài học này và nói với chúng rằng, đó là tiêu chuẩn đạo đức trong thời đại xưa, và hỏi con xem nghe xong câu chuyện này thì con thấy có còn đúng trong xã hội bây giờ không. Gia đình cũng có thể cùng nhau thảo luận xem nên thế nào, không nên thế nào, trên tinh thần tôn trọng ý kiến con trẻ. Không nên áp đặt rằng: mình nói thì luôn đúng, con cái không biết gì.
Như vậy sau mỗi câu chuyện, gia đình sẽ có những cuộc thảo luận rất rôm rả, để những thành viên hiểu nhau hơn.
4
438736
2016-06-27 14:35:59
--------------------------
460633
8248
Quyển Quốc văn giáo khoa thư này mình mua kèm chung với quyển Luân lý giáo khoa thư và đều rất hài lòng về cả hai quyển sách. Riêng với cuốn Quốc văn giáo khoa thư, khi cầm sách lên cảm giác rất hài lòng vì cách trình bày bìa sách, chất liệu giấy in, bố cục bài học, font chữ in và hình minh họa, tất cả đều rất cổ điển, tựa như sách xưa vậy. Về nội dung thì sẽ rất bất ngờ về lối dùng từ ngữ, cách hành văn, nội dung các phần bài học, đều rất lạ lẫm.
5
171151
2016-06-27 10:16:25
--------------------------
449966
8248
Tính dân tộc của cuốn sách nằm ngay trên tiêu đề của nó - Quốc văn giáo khoa thư. Đọc sách này mà thấy nhẹ nhàng chứ không nặng nề như những cuốn sách giáo khoa bây giờ. Văn phong nhẹ nhàng, cẩn thận, trong sáng chứ không đánh đố, hai nghĩa. Tuy còn hạn chế về cách dùng chữ quốc ngữ nhưng vào thời đó mà viết được cuốn sách như vậy quả là một học giả tài năng hiếm có. Giấy in đẹp, mà hơi vàng, đọc không chói mắt. Sách bìa cứng dễ bảo quản. Cám ơn tiki.vn.
5
634855
2016-06-18 11:28:25
--------------------------
449768
8248
Mình biết đến cuốn sách Quốc văn giáo khoa thư này và cuốn Luân lí giáo khoa thư thông qua page Tony buổi sáng. Vì tò mò nên mua về đọc thử. Nội dung sách rất hay tuy là những bài học nhỏ đơn giản như dạy trẻ nhỏ lễ phép, kính trọng ông bà cha mẹ, chăm chỉ học tập,.. rất thích hợp để giáo dục em. Mình mua về thường hay đọc cùng em nhỏ rất bổ ích. Còn về hình thức sách dày dặn bìa chắc chắn mực và giấy in tốt. Rất cảm ơn tác giả và tiki cả dượng Tony nữa.
5
712474
2016-06-17 23:25:44
--------------------------
418607
8248
Mình đã suýt xoa khi nhận được sách từ tiki, bìa cứng, giấy đẹp và còn đẹp hơn bởi nội dung cuốn sách.
Một cuốn sách không đao to búa lớn, không giáo điều nặng nề mà nhẹ nhàng xây dựng nhân cách trẻ em từ sự trân trọng và yêu thương. Những bài học về làm người, hiếu thảo, lễ phép với ông bà cha mẹ, tôn trọng mọi người, dạy cách ăn, cách ở, cách mặc... khi đọc thấy rất ấm lòng.  Tinh thần coi trọng đạo đức của người xưa chạy xuyên suốt tác phẩm
Những câu chuyện nhỏ, những lời khuyên nhẹ nhàng mà thấm thía. Thật may mắn cho thế hệ cha chú đã được học một cuốn sách như thế này.

5
41486
2016-04-19 16:04:06
--------------------------
403071
8248
Ấn tượng đầu tiên về sách là về cách bài trí rất đẹp, chắc chắn; chất lượng giấy tốt, mực in rõ ràng. Về Nội dung: Quốc văn giáo khoa thư là một quyển sách hay và ý nghĩa. Các bài viết tuy ngắn gọn nhưng hàm chứa bài học sâu sắc về cuộc sống, về đạo đức ẩn chứa nhiều thông điệp về điều hay, lẽ phải mà sau bao nhiêu năm vẫn còn nguyên tính giá trị và tính giáo dục. Mang tư duy cuốn sách áp dụng vào cuộc sống giúp chúng ta có cái nhìn nhân sinh quan phong phú. Văn phong đơn giản, dễ chịu, xúc tích dễ hiểu để giảng dạy cho con trẻ. Cám ơn NXB đã tái bản cuốn sách hay và ý nghĩa này!
5
773453
2016-03-23 10:04:41
--------------------------
383169
8248
Tôi vốn thích những gì được chứng thực bởi thời gian. Quyển sách này mang đậm hơi hướng xưa khiến tôi có cảm giác như mình được trở về những năm 95-96, khi tôi lật giở từng trang của sách tiếng Việt lớp 1. Thật sự rất khó tả cảm giác ấy, những bài học ấy mang đậm chất nhân văn mà ấn tượng mãi đối với tôi đến tận bây giờ như "Truyện bó đũa" hay mấy đoạn trích "Gọi là Tý xíu/Mà chẳng bé đâu/ Tý biết nấu nướng/ Hai bữa cơm canh/ Tý còn nhờ ông/ Pha thanh tre cật/ Tý ngồi Tý vót/ Được mười cây chông/ Gửi đồn biên phòng/ Đánh quân cướp nước"... Đến bây giờ tôi vẫn không thể nào quên. Đến bây giờ k còn giữ những cuốn sách giao khoa đấy nữa tôi cứ tiếc mãi, thì may thay gặp "Quốc văn giáo khoa thư". May mắn thay cho tôi lại được gặp lại những cảm xúc của thời đồng ấu. Từng trang sách cũng mang hơi hướng xưa cũ, những đoạn văn ngắn gọn mà súc tích ý nghĩa, k đao to búa lớn mà vẫn đi vào lòng người. Chưa kể còn có giải nghĩa từ phía dưới, câu hỏi đúc kết. Rất hay!
Tôi thiết nghĩ đây mới xứng đáng là quyển sách được đưa vào chương trình phổ thông. 
Cảm ơn Tony Buổi Sáng đã giới thiệu quyển sách này. Cảm ơn NXB trẻ đã phát hành 1 "tác phẩm" tuyệt vời: giấy đẹp, bìa đẹp, nội dung không có gì để phàn nàn và cảm ơn Tiki đã đưa sách đến với tôi.
5
1119846
2016-02-20 11:46:33
--------------------------
378067
8248
Hóng mãi mới mua được sách này từ tiki , sách được bọc cẩn thận , hình thức sách thực sự rất ấn tượng . Nội dung của Quốc văn dạy về những lẽ ứng xử ở đời , những bài thuộc lòng về tình nghiã luân lí , sách giới thiệu về các nhân vật lịch sử như Hai bà Trưng, Tô Hiến thành  . Sách còn giới thiệu về các phong tục và địa danh nước ta . Quốc văn giáo khoa thư thực sự là cuốn sách đầy giá trị để làm các bài đọc cho trẻ , hơn thế chính người lớn cũng cần đọc để nhận ra cái thiếu sót trong kiến thức và cả trong ngôn ngữ của mình .
5
829115
2016-02-04 11:02:13
--------------------------
372395
8248
Hành văn nhẹ nhàng, đơn giản dễ hiểu. Đặc biệt thích hợp với gia đình có trẻ nhỏ hay bố mẹ muốn mua cùng đọc, cùng ngẫm và cùng học với con bởi những câu chuyện ví dụ vô cùng sinh động, dung dị nhưng đầy giá trị giáo dưỡng tâm hồn cũng như nhân cách sống của không chỉ trẻ nhỏ mà cả người lớn cũng vậy. "quốc văn giáo khoa thư" cùng với "Luân lý giáo khoa thư" là hai cuốn đặc biệt nên có trong tủ sách nhà bạn. 
(Tony buổi sáng  đã đưa tôi đến với cuốn sách tuyệt vời này!)
P/S: Cảm ơn tiki.vn. Vì một cuốn sách giá trị khoa tìm thấy ở bất cứ hiệu sách nào lại có thể mua đuoc trên tiki
Thân!
4
672710
2016-01-21 15:59:56
--------------------------
319988
8248
Thật lòng mà nói, mình mua cuốn sách này cùng cuốn Lý luận giáo khoa thư, vì tò mò thôi...và cuốn này mình không hiểu lắm...
Nhìn cuốn sách này khi mới nhận được, mình thấy hình thức cuốn sách rất tốt, dày dặn, bìa đẹp...đến khi nhìn vào trong mình thích hình ảnh minh hoạ và những từ diễn giải dưới đó, có nhiều từ đôi khi mình nghe nhắc đến chứ chưa chắc đã hiểu hết ý nghĩa
Rất cảm ơn tác giả đã biên soạn lại cuốn sách này và cũng cảm ơn tiki đã đưa cuốn sách đến với độc giả chúng tôi.
3
728441
2015-10-10 09:19:28
--------------------------
307583
8248
Đầu tiên, hình thức cuốn sách rất đẹp, cuốn sách có bìa cứng ép nhũ xanh bóng đẹp, trang kế tiếp màu cam có vân nổi. Hình thức quá đẹp khiến tôi nghĩ đến sự trân trọng của ban tác giả dành cho kiến thức của cha ông chúng ta đúc kết lại, trang sách vàng chữ đen gợi lại cho tôi kỷ niệm lúc đi học ngày bé. Các hình ảnh đem lại kiến thức tuyệt vời hơn về cuộc sống ngày xưa của cha ông. Các bài học trong sách rất hay và phù hợp để dạy cho các cháu về đạo đức, lễ phép, lối sống kính trọng ông bà cha mẹ và người lớn tuổi hơn. Tôi hay đọc cho con tôi nghe trước khi cháu đi ngủ thay cho truyện cổ tích. Tôi nghĩ cuốn sách này nên đưa vào chương trình giáo dục đại trà cho con cháu chúng ta thay cho cuốn giáo dục công dân lỗi thời như hiện nay.
5
452607
2015-09-18 10:21:10
--------------------------
304945
8248
Bản thân tôi mua cuốn sách một phần vì tò mò một phần vì thấy trong danh mục sách của dượng Tony khuyên đọc. Lời đầu tiên thì cá nhân tôi thấy cuốn sách được biên soạn rất tốt. Trang trí bìa cứng và loại giấy phù hợp. Các bài học đạo lý tuy đơn giản nhưng lại vô cùng sâu sắc. Lời văn phù hợp cho cả trẻ em và người lớn. Phần hay nhất của cuốn sách chính là phần giải thích từ ngữ. Bản thân tôi xem nó như một cuốn từ điển tiếng Việt vậy. Những từ chúng ta hay dùng thường ngày nhưng chưa chắc đã hiểu hết ý nghĩa. Cám ơn những tác giả đã dày công biên soạn cuốn sách.
5
768773
2015-09-16 19:00:35
--------------------------
303464
8248
Sách bìa cứng, trong mạ chữ xanh thật long lanh, rất đáng để ở tủ sách trong nhà, có lẽ đây cũng là một sự trân trọng với những tác giả đã dày công nghiên cứu cuốn này. Sách viết xen kẽ những bài răn dạy đạo đức, dạy văn hóa, dạy lịch sử, hướng học sinh ý thức đúng đắn, biết nhân lễ hiếu tiết nghĩa....Có lẽ sách hướng đến học sinh nhỏ tuổi, nên không giải thích nhiều, mà thiên về răn dạy lối sống, thành ra đọc không thấy hấp dẫn lắm. Tuy nhiên, sách này dùng để hỗ trợ dạy tiếng Việt cho bé xem ra rất ổn, những từ khó được tác giả giải thích rất cụ thể dưới mỗi bài, bài đọc cũng ngắn mà rất trực quan.
4
588698
2015-09-15 21:05:43
--------------------------
302866
8248
Cuốn sách này cùng với "Quốc văn giáo khoa thư" từng được sử dụng như cuốn sách đạo đức dành cho học sinh tiểu học ngày xưa. Sách bao gồm những câu chuyện nhỏ, giáo dục các hành xử đúng đắn trong những tình huống xuất hiện trong cuộc sống hàng ngày. Nội dung được trình bày đơn giản, dễ hiểu và rất dễ ứng dụng, liên hệ với thực tế. Phù hợp đểđọc cho trẻ tầm 5-6 tuổi trở lên.
Bìa sách cứng, đẹp, giấy khá tốt. Tuy nhiên, hình minh họa hơi nhỏ, chỉ có màu trắng - đen và không được sinh động lắm.

4
538
2015-09-15 15:07:54
--------------------------
302845
8248
Cảm xúc đầu tiên khi cầm sách lên là bất ngờ vì không ngờ sách là loại bìa cứng đẹp và chắc chắn như vậy, thiết kế từ hình vẽ đến từng họa tiết đều mang một màu xa xưa hoài cổ, lật ra bên trong thì thấy màu giấy ngả vàng của những ngày xa xưa, từng bài học đều có hình minh họa, cảm thấy như mình được cầm trên tay lại cuốn sách Đạo Đức của những năm tiểu học vậy. Mỗi bài học ngắn gọn, đơn giản nhưng ý nghĩa thì muôn đời còn đó, dạy con người ta về những giá trị đạo đức cơ bản nhất của một con người. Sách thích hợp cho mọi lứa tuổi, nhất là trẻ nhỏ để giáo dục nhân cách các bé từ khi còn non. Ba mẹ mình đầu 2 thứ tóc rồi mà đọc sách này vẫn khen lắm. 
5
199482
2015-09-15 14:56:47
--------------------------
301597
8248
Những cuốn sách giáo khoa ngày nay phải viết là không bằng quốc văn giáo thư. Những bài học về làm người, hiếu thảo, lễ phép với ông bà cha mẹ, tôn trọng mọi người, dạy cách ăn, cách ở, cách mặc... khi đọc thấy rất ấm lòng, không thấy áp đặt chút nào, dễ hiểu, dễ nhớ ( Những câu chuyện này ngắn lắm, không qua một trang đâu.)  Sao những người biên soạn SGK không lấy những câu chuyện này để dạy chúng mình nhỉ? Mình thấy ngôn từ ở đây không cổ lỗ chút nào cả rất đẹp đấy chứ. Cảm ơn nhà xuất bản trẻ đã xuất bản Quốc Văn Giáo Khoa Thư vì cuốn sách là một điều tuyệt vời của Việt Nam ta!!
5
508336
2015-09-14 19:11:05
--------------------------
297987
8248
cuộc sống hiện tại vốn đầy bon chen, giả dối cũng nhiều, càng ngày con người ta càng đánh mất mình vì những thứ vật chất phồn hoa bình thường. quyển sách dạy ta cách làm người, từ chữ tín, hiếu, đạo, nhân, bồi dưỡng đạo đức của mỗi chúng ta. vì thế đây là quyển sách vô cùng bổ ích giúp chúng ta sống ra hồn một con người, biết yêu thương và giúp đỡ người khác, biết trân trọng những điều bình thường giản dị nhưng đong đầy tình người, dạy ta cách sống tốt. sống tốt cho bản thân và tốt cho người khác, biết yêu thương con người
4
399161
2015-09-12 12:36:37
--------------------------
297654
8248
Chiều nay mua 3 cuốn sách từ Tiki, nhận hàng xong đọc đến tối là xong 1 quyển và một nửa Quốc Văn Giáo Khoa Thư.

Phải nói răng sách hay, viết đã lâu nhưng không hề mất đi giá trị giáo dục và hình thành con người. Những bài học có vẻ đơn giản như đối xử với vật nuôi, cách ăn uống, cách hiếu kính cha mẹ... dù viết cách đây hơn nửa thế kỷ nhưng khi đọc lại vẫn khiến ta gật gù tán thưởng hoặc giật mình vì nhận ra mình vẫn còn nhiều thiếu sót.

Các bài học tuy ngắn nhưng rất thực tế, rất con người, "áp dụng ngay và luôn" khiến ta thực sự thắc mắc sao những kỹ năng cơ bản đầu đời, văn hóa ứng xử với mọi người được soạn ra dễ hiểu như vậy nhưng hiện tại lại không được áp dụng cho các học sinh đang trong tuổi thiếu niên, nhi đồng. Rất hi vọng Bộ Giáo dục xem xét xây dựng một cuốn giáo trình tương tự như vậy.

Tuy nhiên, nói đi cũng phải nói lại, vì hoàn cảnh lịch sử lúc đó, đang chịu ảnh hưởng chính trị từ Pháp nên có một số bài học mang tính ca ngợi "mẫu quốc". Tỷ như, bài học ca ngợi người Pháp giúp Miền Bắc dẹp giặc cờ đen, ca ngợi tuyến đường xe lửa Pháp xây giúp dân ta nâng cao trình độ phát triển. Dù sao, biên soạn cuốn sách trong giai đoạn như vậy, việc có một số bài học mang hơi hướng như trên là không thể tránh khỏi.

Dù sao bỏ qua một số vấn đề nhỏ, theo tôi đây là một cuốn sách hay, giúp trẻ hình thành nhân cách cũng như giúp người trưởng thành xem lại mình...
4
805861
2015-09-12 00:04:09
--------------------------
297627
8248
“Quốc Văn Giáo Khoa Thư” và “Luân Lý Giáo Khoa Thư” là hai bộ sách giáo khoa tiếng Việt được dạy song hành ở các trường Tiểu học Việt Nam trong suốt những thập niên thuộc nửa đầu thế kỷ XX. Trong đó, cuốn “Quốc Văn Giáo Khoa Thư” là những mẩu chuyện nhỏ hay như lời khuyên bổ ích trong cuộc sống chẳng hạn như đừng để móng tay, chớ tắm rửa nước bẩn… Hay giới thiệu về các nhân vật lịch sử như Ông Phan Thanh Giản.. Tất cả đều là những bài học nhỏ về cuộc sống xunh quanh chúng ta..
4
654655
2015-09-11 23:39:35
--------------------------
295835
8248
Từ cái nhìn đầu tiên đã rất thích cuốn Quốc văn giáo khoa thư và Luân lý giao khoa thư. Lúc nhận được sách thì càng thích hơn. Bìa đẹp, cứng cáp, bên trong rất đẹp. Nội dung thì không có gì phải bàn, rất ý nghĩa và bổ ích. Cuốn sách này dành cho mọi lứa tuổi, trẻ con cực kì phù hợp đọc, người lớn như mình đây cũng nên đọc để có thêm kiến thức. Những thứ liên quan đến giáo dục thì không bao giờ lỗi thời. Thiết nghĩ nhà nào cũng nên mua 1 bộ về để cả nhà cũng nghiên cứu. 
5
52328
2015-09-10 17:50:55
--------------------------
293962
8248
Mình mua cuốn Luân lý giáo khoa thư xong qua mua cuốn này ngay. Những bài học trong cuốn sách giống với Luân lý giáo khoa thư: rất nhẹ nhàng và dễ hiểu, cũng như rất thấm. Khuyến khích người lớn cho trẻ trong độ tuổi phát triển đọc sách này, vì theo mình cảm nhận thì đây là một cách giảng dạy rất khoa học của giáo dục ngày xưa. Mình rất hài lòng với nội dung cũng như cách trình bày của quyển sách. Bậc cha mẹ cũng có thể tìm hiểu như một nguồn tốt để dạy dỗ con cái mình.
5
4940
2015-09-08 21:47:32
--------------------------
293176
8248
Tôi tìm mua quyển này vì đã từng đọc các bài báo, bài viết của tác giả Trần Trọng Kim, một học giả lớn của Việt Nam nên muốn đọc các công trình của ông và các đồng sự. Khi đọc thì quả thật hài lòng, không một chút thất vọng. Quyển sách trình bày đơn giản, rõ ràng, xúc tích. Sách sử dụng từ Hán Việt nhiều, khiến tôi cảm thấy rất thích vì từ Hán Việt hay và tạo cảm giác nghiêm túc. Sau này nhất định quyển này tôi sẽ giữ gìn và mang cho các con đọc, những bài học đơn giản thế này tốt hơn nhiều so với những quyển sách bây giờ và các thông tin tràn lan trên internet. Cám ơn các tác giả đã hết lòng vì Giáo dục Việt Nam
5
15037
2015-09-08 07:02:12
--------------------------
292402
8248
Tôi đã đặt mua tại tiki 2 quyển Quốc văn giáo khoa thư và Luân lý giáo khoa thư. Tiki bán 2 quyển này với giá khuyến mãi cực kì tốt. Dịch vụ giao hàng nhanh, có gọi điện thoại xác nhận và giao hàng tận nơi, miễn phí.
Sách được bọc cẩn thận và bảo quản tốt.
Đây là 2 quyển sách rất hợp với trẻ em lẫn phụ huynh trong việc giáo dục đạo đức thông qua các bài học đạo đức ngắn gọn, ý nghĩa và đầy nhân văn.Các bậc phụ huyng, giáo viên tiểu học nên đọc để hiểu thêm về giáo dục ngày xưa và có thể giúp ích trong dạy và hướng dẫn em nhỏ trong những năm đầu đi học.
Tôi rất hài lòng về tất cả.

4
775749
2015-09-07 11:51:28
--------------------------
291347
8248
Tôi đã tìm mua quyển sách này vào năm ngoái khi biết thông tin Nhà xuất bản Trẻ tiến hành xuất bản Quốc văn giáo khoa thư nhưng rất tiếc vào thời điểm đó trên Tiki đã hết. Tôi đã chờ và đã mua được. Cảm ơn Tiki đã đem đến thông tin tái bản và phát hành sách này. Quốc văn giáo khoa thư với những bài học ngắn gọn dễ hiểu và nhẹ nhàng dạy cho trẻ nhỏ về các vấn đề về gia đình, nhà trường và xã hội. Đặt ra những câu hỏi nhỏ và thông qua mỗi bài học khuyên em nhỏ thực hiện. Không phải là những bài học giáo điều hay bắt buộc các em phải làm theo mà thông qua những lời giải thích nhỏ để các em tự nhận thức và thực hành. Các bậc phụ huyng, giáo viên tiểu học nên đọc để hiểu thêm về giáo dục ngày xưa và có thể giúp ích trong dạy và hướng dẫn em nhỏ trong những năm đầu đi học.
5
355080
2015-09-06 10:50:39
--------------------------
290932
8248
Quốc văn giáo khoa thư cùng với Luân lý giáo khoa thư là hai cuốn sách không những người lớn nên đọc mà các bé học sinh, thiếu nhi cũng nên đọc. Nếu ai có con nhỏ chưa đọc được, họ có thể dùng sách thay cho chuyển kể để đọc cho các bé, vì sách dạy làm người, đức tính cần thiết để phát triển trí óc và tâm hồn trẻ thơ. Sau khi được dượng Tony Buổi Sáng giới thiệu cho đọc, mình đã quyết định mua hai cuốn này. Quả thực sách làm mình rất hài lòng. Mặc dù đã qua thời học sinh tiều học từ rất lâu, nhưng càng đọc mình càng thấm thía những lời dạy trong sách. Hai cuốn sách này xứng đáng nằm trong kệ sách nhà bạn.
5
277975
2015-09-05 21:59:54
--------------------------
289438
8248
Ngay từ khi Tiki mở order cho cuốn sách này, mình đã rất ấn tượng. Bìa sách màu nâu, với những hình ảnh xưa cũ, tạo cảm giác hoài cổ, rất đẹp và trang nhã. Hôm nay nhận được sách, cảm giác thật tuyệt vời khi cầm cuốn sách trên tay. Tiki bọc sách rất đẹp, vừa vặn. Ruột sách in đều, đẹp, nội dung thì khỏi cần phải nói, tất cả đều là những kiến thức gần gũi với đời sống và vô cùng bổ ích, nhất định sau này mình sẽ đọc cho con mình nghe, dạy con mình học mỗi ngày một bài. Cảm ơn NXB Trẻ rất nhiều vì đã tái bản cuốn sách tuyệt vời này!
5
36066
2015-09-04 14:46:14
--------------------------
288543
8248
Được biết đến là sách giáo khoa của bậc cha ông chúng ta từ thời trước, cuốn sách QUỐC VĂN GIÁO KHOA THƯ thực sự đã làm tôi tò mò. Cho đến khi cầm được cuốn sách trên tay tôi mới hiểu ngày trước cha ông ta đã được hưởng một nên giáo dục như thế nào. Cuốn sách có từ đầu thế kỷ 20, quá xa so với tuổi của tôi nhưng khi đọc lại thấy vô cùng gần gũi. Có lẽ vì lời văn sử dụng những từ ngữ rất thân quen, nội dung thì đề cập những sự kiện hết sức quen thuộc với trẻ em hằng ngày như tôi đi học, tôi tập đọc, ... Đặc biệt là các bài văn đều dùng đại từ "tôi" để kể chuyện, không như sách giáo khoa bây giờ dùng từ "em". Việc dùng từ "tôi" giúp trẻ em khi học khẳng định vị trí và trách nhiệm của mình như một cá nhân trong xã hội. Một điểm đáng lưu ý là luật bằng trắc trong các câu văn rất hài hòa, nên khi đọc lên tạo một cảm giác du dương, dễ chịu. Đọc lớn 1 đoạn văn tôi thấy yêu tiếng Việt biết bao. Đọc lên bồi hồi như nghe lời dạy của người xưa vậy.
5
726143
2015-09-03 17:25:10
--------------------------
287686
8248
Mình biết tới cuốn sách này thông qua sự giới thiệu của Dượng Tony. Cuốn sách cùng với Luân lý giáo khoa thư đã làm nên một bộ sách tuyệt vời về giáo dục đạo làm người cho trẻ nhỏ. Nhung mình nghĩ không chỉ trẻ nhỏ mà ngay kể cả người lớn khi đọc cuốn sách này cũng thấy thật gần gũi với đời sống. Và thông qua cuốn sách này, mình đã hiểu biết thêm về sự giáo dục của ông cha ta ngày xưa, qua những mẩu chuyện rất ngắn nhưng rút ra được những bài học hết sức bổ ích trong cuộc sống. Cảm ơn Tiki vì một lần nữa giao hàng rất nhanh! Chắc chắn mình sẽ còn mua sắm tiếp ở Tiki!
5
772163
2015-09-02 22:33:14
--------------------------
286921
8248
Mình biết đến Quốc văn giáo khoa thư qua lời giới thiệu của bác Tony buổi sáng. Là quyển giáo khoa mà cha ông ta học cách đây 100 năm. Đây cũng là món quà mà nhiều Việt Kiều mua làm quà hay đọc cho trẻ nhỏ mỗi tối. Nội dung mỗi bài học tuy ngắn nhưng lại chứa đựng ý nghĩa sâu sắc về học làm người. Là điều cơ bản của giáo dục.
Bìa dày đẹp, giấy tốt. Mua ở Tiki được giảm đến 30%. Đóng gói cẩn thận và giao hàng nhanh. Vì thế mình lúc nào cũng mua sách ở Tiki.
5
282976
2015-09-02 08:32:59
--------------------------
286211
8248
Cuốn sách có từ đầu thế kỷ 20, quá xa so với tuổi của tôi nhưng khi đọc lại thấy vô cùng gần gũi. Có lẽ vì lời văn sử dụng những từ ngữ rất thân quen, nội dung thì đề cập những sự kiện hết sức quen thuộc với trẻ em hằng ngày như tôi đi học, tôi tập đọc, ... Đặc biệt là các bài văn đều dùng đại từ "tôi" để kể chuyện, không như sách giáo khoa bây giờ dùng từ "em". Việc dùng từ "tôi" giúp trẻ em khi học khẳng định vị trí và trách nhiệm của mình như một cá nhân trong xã hội. Một điểm đáng lưu ý là luật bằng trắc trong các câu văn rất hài hòa, nên khi đọc lên tạo một cảm giác du dương, dễ chịu. Đọc lớn 1 đoạn văn tôi thấy yêu tiếng Việt biết bao.
5
465332
2015-09-01 15:20:15
--------------------------
168194
8248
Lời văn nhẹ nhàng,trân trọng trẻ nhỏ ( các câu hỏi luôn gọi các em bằng Anh - anh học lớp mấy,xung quanh nhà anh có gì .v.v. ) 
Bài học ngắn gọn, gần gũi ,dễ hiểu phù hợp với các em - các em hãy còn nhỏ ,mâm cao cỗ đầy văn chương dài dằng dặc làm chi - không hề mang tính áp đặt các em phải trả lời như khuôn mẫu hơn hẳn sách Đạo Đức của em mình.
Cho thấy tinh thần cọi trọng đạo đức của người xưa điều này chạy xuyên suốt tác phẩm
Rất tiếc là không học bằng sách này nữa.
Mình đưa cho một số giáo viên là bạn học của mình coi và họ thật sự đánh giá tốt nội dung của nó.
Chất lượng bản in :
1,Bìa : Đẹp,in bìa cứng và dày cầm rất thích :D
2,Trang giấy : Tốt,dai,mực in tốt không nhòe.
5
107694
2015-03-16 10:09:14
--------------------------
