556501
10460
Không quá hào nhoáng như những tiểu thuyết ngôn tình khác. Những chương đầu đọc khá là cuốn hút ... về sau thì hơi chán nhẹ. Túm lại là tương đối ổn
3
1364651
2017-03-28 13:43:35
--------------------------
248389
10460
"Mọi nỗi đau rồi sẽ qua đi. Thời gian sẽ xóa nhòa mọi thứ. Cái chúng ta cần làm là phải yêu quý hiện tại hơn.Quên đi quá khứ thì chúng ta mới được cứu rỗi. Nắm bắt hiện tại thì chúng ta mới có thể yên lành...!" - Trích Luyện yêu. Từ Luyện yêu, tôi học được nhiều triết lý sống của tác giả, một cuốn sách khá dày nhưng tương xứng với nội dung của nó (hài hước, tình cảm, lãng mạn và đầy triết lý!) Tình yêu không bao giờ có phân biệt tuổi tác, chỉ cần ta luôn có một trái tim rộng mở và sẵn sàng trao nhau yêu thương!
4
705146
2015-07-31 09:53:39
--------------------------
111839
10460
Cách viết của Hoàng Mặc Kỳ chính là thông qua cử chỉ và hành động để nói lên tính cách nhân vật. Trong câu chuyện này, có Thẩm Đình độc mồm nhưng không kém phần lương thiện, tính cách mạnh mẽ giống một cây xương rồng. Có Nhân Kiệt tài giỏi, lạnh lùng nhưng bên trong có rất nhiều nỗi đau. 
Đây là một câu chuyện hay, càng đọc càng lôi cuốn.
Mỗi người phụ nữ trên thế giới này đều có thể là Thẩm Đình, chỉ là có gặp may vớ đc 1 Nhân Kiệt hay không thôi.
Tôi yêu câu chuyện này, yêu tác giả, yêu từng nhân vật.
4
339564
2014-05-04 12:42:54
--------------------------
105853
10460
Luyện Yêu là một quyển sách mà chỉ đọc vài trang đầu tiên đảm bảo sẽ cuốn hút bạn ngay nhưng cái sự cuốn hút đó đến giữa sách sẽ bị trầm xuống. Nó trầm xuống không phải vì nội dung không còn hay nữa hay là tiết tình đã hết hấp dẫn mà là tính cách của nhân vật nữ hơi cực đoan nên khiến tôi đọc hơi bị ức chế. Nhưng cũng may là càng về cuối thì nhân vật nữ có thể thấu hiểu cho nam chính nên tôi cũng phần nào bớt đi sự bức bối trong mình.
Nhưng trách nữ chính thì cũng không đúng lắm. Nếu đứng ở lập trường của một nhân viên, đồng nghiệp với nhau mà nói thì suy nghĩ của Thẩm Đình không có gì là sai trái cả. Nhưng đứng ở lập trường của một người lãnh đạo mà nói thì việc làm của Nhân Kiệt là đúng đắn hợp tình hợp lý cũng không sai trái gì. Một lãnh đạo giỏi là phải quyết đoán tuy biết sa thải nhân viên trong lúc kinh tế khó khăn là một vấn đề rất nam giải nhưng không làm thế thì doanh nghiệp làm sao đổi mới được. Tình cảm là tình cảm nhưng công việc là công việc không thể vì tình cảm mà làm ảnh hưởng đến công ty đến công việc. Thà là một vài người phải chết còn hơn là chết cả lũ. Nên mới nói lúc làm việc phải làm hết mình, chú tâm vào nó, tâm huyết với nó thì dù có chuyện gì xảy ra cũng không sợ hãi, người tài đi đâu cũng sống được. Nên tôi rất đồng ý với cách làm của Nhân Kiệt.
Ngoài  nội dung hay, tính cách nhân vật đặc biệt và tính thực tế của chuyện cũng rất cao. Bây giờ ra đời đi làm, ai chẳng có học vị, bằng cấp một lô, tài năng cũng đâu có thiếu nhưng không phải cứ đâm đầu ngụp lặn hết sức làm việc là thành công, tiền tài địa vị có đủ mà chỉ thấy thời gian thấm thoát trôi nhìn đi nhìn lại cũng chẳng được gì chỉ thấy tuổi càng ngày càng chồng chất còn hoài bão ước mơ xem ra cũng chỉ là bốc đồng thời quá khứ có mấy ai thực hiện được. Truyện cho ta thấy thực tế rất tàn khốc nhưng nó cũng cổ vũ chúng ta đừng vì hoàn cảnh trước mắt mà quên đi hoài bão của mình, còn nước còn tác thì biết đâu chừng ước mơ đó cũng sẽ thực hiện được đối với những ai có niềm tin và sự cố gắng.
3
180185
2014-02-08 20:15:36
--------------------------
77402
10460
Đối với sách, mình chỉ nhận xét một từ ''ổn'', với thể loại tình yêu không phân biệt tuổi tác thì truyện cũng rất đáng để quan tâm. Truyện không có chiều sâu nên cũng không để lại ấn tượng gì cho mình nhiều, mình nghĩ nó rất phù hợp để giải trí hay thư giãn lúc rảnh rỗi thì được. Chuyện tình của 2 nhân vật chính cũng khá bình thường, khá dễ thương nhưng được cái là tính hài hước, đọc truyện mình không khỏi sặc cười :)). Mình cũng đánh giá cao truyện về điều này. Bìa truyện thì rất dễ thương, sách nhỏ gọn dễ đọc, dịch giả rất khéo léo trong việc tạo tiếng cười cho người đọc. Mình cho truyện 3 sao :)
3
57869
2013-05-27 18:55:52
--------------------------
77283
10460
Truyện xây dựng những tình tiết khá hài hước, khiến  mình bật cười sặc sụa :)) Nói chung truyện cũng không có chiều sâu, cũng khá bình thường nhưng yếu tố hài hước thì rất ổn. Mình tuy không thích thể loại nv nữ chính già hơn nam chính nhưng với ngòi bút của Hoàng Mặc Kỳ đầy sống động và chân thực thì mình cũng có lúc bị cuốn hút vào. Điểm cộng nữa là nhân vật được khắc họa rõ nét, bản dịch của truyện rất ổn, không có lỗi sai, dịch giả cũng dịch rất sát nghĩa, với văn phong dí dỏm, dịch giả đã cho thấy cái hay như bản gốc của truyện. Tuy nhiên, thiết nghĩ truyện chỉ hợp để thư giãn giải trí là chính, vì nó không để lại trong mình ấn tượng gì nhiều.
3
57869
2013-05-27 00:57:23
--------------------------
69250
10460
"Luyện yêu" là cuốn tiểu thuyết mà tớ mua ngay không chần chừ vì nội dung rất lôi cuốn. Đọc mấy chương đầu trên mạng mà cười ầm ĩ không ngừng được. Tớ khá kết những câu nói đầy triết lý và nhân văn trong truyện. "Không phải không đủ yêu mà chỉ yêu thôi thì không đủ". Chẳng cần những yếu tố hút khách như sex, hành động,... "Luyện yêu" vẫn là một cuốn sách "hot" theo ý kiến cá nhân của tớ. Và đặc biệt nhất là cuốn sách tuy dày nhưng rất nhẹ, cầm thích quá chừng. Tớ có mấy cuốn ngôn tình vừa dày vừa nặng cầm muốn trẹo cả tay. 5 sao nhé.
5
27232
2013-04-15 14:38:13
--------------------------
59267
10460
Truyện này  mình thấy cũng được, đọc hài. Mở đầu khá ấn tượng: Gái già Thẩm Đình và Thẩm Nhân Kiệt oan gia cùng tranh mua một đôi giày, vì không ai chịu ai nên mỗi người mua một cái :)) Hai người lại sống trong cùng tòa nhà nên gặp nhau rồi người này độc miệng với  người kia cũng là điều bình thường. Nhưng mà tác giả cho nhiều tình tiết hài quá nên đọc thỉnh thoảng cũng thấy chán. Kết thúc thì dễ đoán rồi, Thẩm Đình đã hơn 30, gặp hết thất bại này đến thất bại khác, nhưng mà lại được cậu phi công trẻ Thẩm Nhân Kiệt yêu. Môtíp truyện quá quen rồi còn gì. Nhưng mà chêm vào trong truyện cũng có khá nhiều câu triết lí hay và sâu sắc. Truyện này để nên để đọc dần dần, chứ liền luôn một mạch cũng hơi nhàm...
3
92868
2013-02-12 17:07:09
--------------------------
53736
10460
"Luyện yêu" là một cuốn tiểu thuyết hay, lôi cuốn qua từng trang sách. Với cách viết nhẹ nhàng, hóm hỉnh, cách dẫn dắt truyện nhịp nhàng, tác giả đưa người đọc qua những cung bậc cảm xúc khác nhau trong tình yêu. Câu chuyện tình được kể trong sách cũng khá thú vị và có nhiều điểm khác biệt, với những tình tiết hấp dẫn và bất ngờ, để lại nhiều dư vị ngọt ngào cho người đọc. Tuy nhiên, có thể ngôn ngữ của tác giả chưa được chau chuốt tinh tế lắm, nên cuốn sách chủ yếu tính giải trí, chứ chưa thực sự để lại những chiêm nghiệm sâu sắc. Nhưng dù sao đây cũng là một cuốn sách đáng yêu, trẻ trung và tươi mới, làm cho người đọc cảm thấy thích thú và hào hứng.
3
20073
2013-01-04 18:42:57
--------------------------
40169
10460
Đọc đoạn cuối mà mình cảm thấy hụt hẫng nhiều lắm, bởi vì nhân vật trong tưởng tượng của mình luôn xù gai với nhau, vậy mà chỉ nhờ một vài sự việc, lại trở nên dịu ngọt với nhau quá đỗi. Có thể tình yêu sẽ khiến con người ta thay đổi, nhưng cái gì cũng có giới hạn, không thể một bước từ địa ngục lên thiên đường được, cứ y như là từ hai cây xương rồng sẵn sàng chỉa gai vào nhau, chỉ trong thoáng chốc lại biến thành hai nhánh hoa thược dược mềm mỏng và yếu đuối.
Nhìn chung thì cốt truyện thu hút, trong giả có thật, trong thật có giả. Truyện cũn mà mới, ngôn từ sắc xảo. Nếu nhìn một cách khách quan thì Luyện yêu vẫn là một tuyệt phẩm
4
46330
2012-09-18 23:50:49
--------------------------
37419
10460
  Đây là cuốn sách mà tôi thực sự rất thích.Khi thấy cái tên tựa đề là  "Luyện yêu",tôi bất giác mỉm cười..cười vì mình chưa có người yêu nênbiết đâu trong cuốn sách lại có vài chiêu hay ho để mình  tìm đc người yêu nhỉ??....Và sau khi đọc xong nó,tôi phát hiện ra rằng mình k luyện đc chiêu nào để tìm người yêu nhưng lại luyện được cơ hàm và dây thần kinh cười của mình lên 1 giới hạn mới.
 Cuốn sách  thực sự rất hài hước,dễ thương và đâu đó trong truyện,trong từng nhân vật,tôi nhìn thấy suy nghĩ  và vướng mắc của bản thân mình trong đó,và cũng từ đó mà tìm đc câu trả lời cho mình.Hài hước nhưng đôi lúc thực sự rất sâu sắc.
 Hoàng Mặc Kì là một tác giả mà tôi chưa đọc qua một tác phẩm nào hết nên khi quyết định bỏ tiền ra mua cuốn sách đó,thật sự tôi đã rất đắn đo.Nhưng khi cầm trên tay và đọc nó tôi nghĩ rằng tác  giả và dịch giả đã k làm tôi thất vọng.Nếu bạn là người thích ngôn tình hài hước thì đây là một lựa chọn rất phù hợp.
5
33691
2012-08-22 21:22:20
--------------------------
34518
10460
Tôi phải nói là rất rất thích cuốn sách này.
Cốt truyện được xây dựng khá công phu, đặc biệt là khúc cuối, tình huống tạo ra rất cuốn hút.
Không chỉ có thể, lời văn rất sắc sảo, có những triết lí mà tôi đã từng trải nghiệm nhưng chưa đúc kết được, tác giả đã giúp tôi kết luận.Có một số câu tôi rất tâm đắc.
Tôi đã có một lựa chọn đúng đắn khi đọc cuốn sách này, rất hài lòng!
5
44069
2012-07-22 16:51:20
--------------------------
26729
10460
Luyện Yêu chỉ đơn thuần là một tiểu thuyết đọc trong lúc rảnh rổi, để tìm đến tiếng cười qua từng trang sách chứ thực sự không phải là cuốn sách "gối đầu giường".Truyện có nhiều tình tiết khiến tôi bật cười vì độ hài hước, đáng yêu, nhưng đôi khi cũng khiến tôi suy ngẫm về cuộc đời này. Về những người trẻ giàu nhiệt huyết, tài năng nhưng không gặp thời. Nhưng họ vẫn lạc quan, vẫn sống, vẫn yêu đời. Nếu bạn yêu thích thể loại truyện tình cảm hài hước thì bạn đừng bỏ qua nhé! Dịch giả dịch rất khéo, rất vui, nên mình tin rằng các bạn sẽ không hối hận vì đã mua nó đâu.Sách khá dày nên đọc lướt qua trước để hiểu sơ sơ cốt truyện chứ không nên đọc từng trang từng chữ mất thời gian. Đó kinh nghiệm đọc mà mình tiếp thu của khá nhiều người.
4
38001
2012-05-12 16:22:53
--------------------------
22091
10460
Tôi rất thích quyển sách này. Hoàng Mặc Kỳ có khả năng khắc họa tính cách nhân vật vô cùng tài tình và đặc sắc. Khi nhắc đến tên bất cứ ai  trong " Luyện Yêu", tôi lập tức liền có thể mường tượng ra tính khí của nhân vật ấy một cách rõ  nét. Đây là điều không phải nhà văn nào cũng có thể làm được. Đặc biệt là hình tượng nam - nữ chính. Bọc trong vẻ ngoài tưởng chừng xù xì, gai góc là những trái tim luôn khao khát yêu thương, khao khát vươn lên. Họ sống, đối mặt với cuộc sống cùng nhiều va vấp, trăn trở. Giữa dòng đời vồ vập, vốn không có gì hoàn hảo, họ khó khắn mới hiểu, và đến được với nhau. Truyện có nhiều tình tiết khiến tôi bật cười vì độ hài hước, đáng yêu, nhưng đôi khi cũng khiến tôi suy ngẫm về cuộc đời này. Về những người trẻ giàu nhiệt huyết, tài năng nhưng không gặp thời. Nhưng họ vẫn lạc quan, vẫn sống, vẫn yêu đời. Rất vô tình, tôi bắt gặp hình bóng mình trong họ. Đó chính là nguyên nhân làm quyển sách ảnh hưởng nhiều tới tôi.
5
24968
2012-03-19 23:47:23
--------------------------
20495
10460
Đây là một câu chuyện khá dí dỏm về đề tài " phi công trẻ lái máy bay bà già ". Truyện xây dựng rất sinh động hình ảnh của Thẩm Đình, 1 phụ nữ 30 tuổi không sự nghiệp, không tình yêu.
Cách suy nghĩ, hành xử của cô rất "thật", như những gì đang diễn ra trong cuộc sống thường ngày. Tình yêu của Thẩm Đình-Nhân Kiệt đến rất tự nhiên. Họ gặp nhau, cãi nhau nhưng rồi sự thấu hiểu, chia sẻ trong công việc lại đưa họ đến gần nhau. Tuy sẽ còn nhiều khó khăn nhưng chỉ họ tin tưởng nhau, cảm thấy hạnh phúc khi bên nhau, vậy là đủ.
Văn phong dịch giả khá hay, một câu truyện đáng đọc.
5
28683
2012-03-02 11:39:08
--------------------------
16081
10460
Đây là tiểu thuyết nói về đề tài "phi công trẻ lái máy bay bà già" mà mình đánh giá là khá hài hước. Đọc rất vui tuy nhiên theo mình thấy thì không được hay cho lắm. Bởi lẽ nó không sâu sắc, không lột tả hết được những khó khăn thực sự mà hai người yêu nhau phải trải qua. Luyện Yêu chỉ đơn thuần là một tiểu thuyết đọc trong lúc rảnh rổi, để tìm đến tiếng cười qua từng trang sách chứ thực sự không phải là cuốn sách "gối đầu giường" mà mình lựa chọn.

Tuy nhiên, nếu bạn yêu thích thể loại truyện tình cảm hài hước thì bạn đừng bỏ qua nhé! Dịch giả dịch rất khéo, rất vui, nên mình tin rằng các bạn sẽ không hối hận vì đã mua nó đâu.
3
7892
2011-12-16 02:28:30
--------------------------
