444966
5858
Phía sau những mặt tốt hào nhoáng của nước Nhật tráng lệ, là những mặt tối mà sau khi đọc cuốn sách ta mới cảm nhận được... Yakuza không hề đơn giản như ta nghĩ một chút nào... theo lối viết truyện pha chút hài hước của Jake, ta thấy được bộ mặt thế giới ngầm một cách chân thật và trần trụi nhất. Yakuza hiện nay không hẳn là những tên đầu gấu mặt chuột xăm trổ đầy mình, chúng có thể ngụy trang thành nhiều tầng lớp xã hội khác nhau. Chẳng hạn núp bóng trong vẻ ngoài mộ công ty bất động sản mà ta không hề hay biết? Tóm lại, nếu bạn đọc nó bạn sẽ thấy suy nghĩ của bạn về yakuza được thay đổi rõ rệt
5
606550
2016-06-09 11:30:32
--------------------------
415314
5858
Nghĩ đến Tokyo có lẽ ai trong chúng ta cũng sẽ nghĩ đến tháp truyền hình Tokyo kỳ vĩ, những chuyến tàu điện ngầm đúng giờ đến kinh ngạc, tàu siêu tốc làm thế giới ngạc nhiên, những ánh đèn hào nhoáng, những phố xá tấp nập người qua lại, những nhà hàng sushi cao cấp, những trung tâm thương mại hoành tráng. Nhưng biết đâu đằng sau những thứ đó là 1 thế giới ngầm đáng sợ, nơi đó có những dằn xé, bệnh hoạn, uẩn ức, tội ác. Cuộc sống đó như 1 mặt trái thô ráp của Tokyo, của Nhật Bản.
5
465332
2016-04-13 10:17:31
--------------------------
410350
5858
Cuốn sách dành cho những bạn đọc muốn trải nghiệm về thực trạng thế giới ngầm ở thủ đô Tokyo nước Nhật nhưng lại muốn bảo toàn tính mạng, vậy nên đây là cuốn sách nên có trong tủ sách nhà bạn, từng trang sách được viết dưới con mắt trải nghiệm thực tế của một nhà báo nước ngoài. Tác phẩm là câu chuyện hay, hồi hộp và gay cấn, xứng đáng bỏ túi tiền ra mua. Người đọc sẽ có những trải nghiệm gián tiếp dưới góc nhìn của một nhà báo nước ngoài về một Nhật Bản thật khác, một Nhật Bản không có Hoa anh đào, không Hello Kitty, không trà đạo... mà là một Nhật Bản với những nhóm mafia khét tiếng, với những thành viên các băng đảng mặc vest đen, đeo kính đen lịch sự, tự do đi lại giữa đường phố Tokyo
5
816965
2016-04-03 22:49:24
--------------------------
397683
5858
Lần đầu đặt mua sách ở tiki và mình đã không thất vọng. Chất lượng sách rất tốt.
Cuốn sách cho ta thấy mặt tối của xứ sở hoa anh đào đúng với cái tên của nó. Cho những ai thấy vẻ đẹp của đất nước này qua những trang Manga hay Anime và cả qua tranh ảnh sách báo, mọi người hãy đọc cuốn sách này để có thể biết được Nhật Bản - đất nước tưởng chừng hoàn mĩ về mọi mặt: truyền thống lâu đời, ý thức con người, khoa học kĩ thuật... lại không hề như vậy...
Thương xót, đồng cảm, hồi hộp, căng thẳng, nhẹ nhõm, boăn khoăn, suy nghĩ... cuốn sách đã tất cả những cảm xúc trên một cách rất thực.
5
387938
2016-03-15 12:14:23
--------------------------
330779
5858
Là cuốn sách đáng đọc cho những ai muốn nhìn thế giới xung quanh như đúng bản chất của nó. Cuốn sách là một câu chuyện trần trụi về một thế giới không mấy tốt đẹp bên dưới hào quang của sự phát triển vượt bậc được thể hiện trên nhiều phương diện như kinh tế, văn hóa cũng như xã hội ở đất nước Nhật Bản. Cuốn sách còn phô diễn những gốc khuất, những sự thật bên trong ngành công nghiệp tình dục được cấp phép ở Nhật Bản. Và cũng cho thấy con người gần như bắt đầu trở nên cô đơn trong thế giới hiện đại, khi nhu cầu của ngành công nghiệp tình dục lại không chỉ thiên về phục vụ cho nam giới. 
5
823779
2015-11-03 11:20:38
--------------------------
265765
5858
Với những ai đã từng say mê Bố già thì Thế giới ngầm Tokyo sẽ là một đề xuất không tệ cho lựa chọn tiếp theo :))
Tác giả có cách nói chuyện rất tỉnh, đầy hài hước và trào phúng cùng với một vốn kiến thức sâu rộng về Nhật bản đã giúp đưa cuốn sách lên một tầm cao và mang trong mình phong cách khác hẳn với những cuốn sách còn lại. Khi đọc ắt hẳn các bạn sẽ không khỏi nổi da gà về cái thế giới ngầm Tokyo ấy, nhiều khi còn thấy nó khá bệnh hoạn - theo đúng nghĩa đen luôn ấy, vì nó được tác giả miêu tả rất chân thật và sống động. Nhưng chịu thôi, mặt trái thì có bao giờ làm người ta thuận mắt :)). Nó phơi bày khá nhiều mặt trái đấy vì vậy nên cực kì đáng đọc cho những ai muốn nhìn thế giới theo đúng bản chất bên trong của nó.


5
464538
2015-08-14 09:52:31
--------------------------
231593
5858
Thực trạng thế giới ngầm ở thủ đô Tokyo nước Nhật dưới con mắt một nhà báo nước ngoài.
Tác phẩm là một câu chuyện hay và xứng đáng để đọc dưới góc nhìn của một nhà báo người ngoại quốc về xã hôi, con người và văn hóa của đất nước Nhật Bản không chỉ ở những khía cạnh tốt đẹp mà còn ở cả các mặt trái của xã hội nước Nhật.
Qua ngòi bút trào phúng và giọng văn hài hước của tác giả - Jake Adelstein, một người ngoại quốc sống, học tập và làm việc nhiều năm ở Nhật Bản - câu chuyện đã dẫn dắt người đọc thâm nhập vào thế giới ngầm của thủ đô nước Nhật, một thế giới tội phạm bao gồm nạn mại dâm, buôn người, cho vay nặng lại, trộm cướp… được miêu tả chân thật, sống động, phơi bày những thực trạng nhức nhói của một quốc gia phát triển bậc nhất khu vực châu Á cũng như của cả thế giới.
5
618799
2015-07-18 10:55:11
--------------------------
228754
5858
Tôi đã mua cuốn sách này và đọc liên tục trong 2 ngày. Phải nói cuốn hồi ký này làm tôi xúc động trào dâng đến nghẹt thở vì nó quá chân thực, quá tê tái và nghẹn ngào trước những mặt nổi tôi từng biết về xã hội Nhật Bản phồn vinh. Bạn đừng hy vọng đây là cuốn sách nhí nhảnh vui tươi về cuộc sống hồn nhiên vì những điều đó không nằm trong đây. Những tình tiết, quan hệ giữa những người có địa vị phức tạp và đặc thù công việc của một nhà báo điều tra tội phạm người Mỹ Jake Adelstein dần hé mở những góc khuất bất ngờ và đầy kịch tính sẽ không thể khiến bạn đặt cuốn sách xuống được.
4
496400
2015-07-15 21:24:31
--------------------------
204426
5858
Đặt cuốn sách xuống cũng là lúc mà cảm giác nuối tiếc trào dâng. Một cái gì đó gọi là trống rỗng và buồn đến tận cùng. Càng cảm thấy nuối tiếc hơn khi đây là một câu chuyện có thực, viên cảnh sát đã cống hiến cả đời của mình cho sự nghiệp nhưng cuối cùng lại bị đồng nghiệp ganh ghét mà hãm hại rồi phải chết trong ung thư, cô gái điếm chua ngoa, tục tĩu muốn làm một điều gì đó tốt đẹp lại có một cái kết không rõ ràng mà gần mười phần là chết trong đau đớn, tay trùm xã hội đen ranh ma, xảo quyệt mà biết bao người phải đổ công, đổ sức đánh đỗ lại kết thúc bằng việc đi tu ... Vẫn biết là mọi xã hội luôn có những góc khuất nhưng thực sự vẫn thấy thật bàng hoàng khi đọc xong câu chuyện này, thấy ghét những quan điểm cứng đầu, cổ hủ, và bơ đi cái quyền con người ở những cô gái điếm hay Yakuza, thấy ghét khi mọi thứ cứ phải làm theo rập khuôn để rồi biết bao con người đã phải cam chịu số phận bị chèn ép, ức hiếp. Mong rằng mọi thứ dần sẽ được thay đổi...
4
140014
2015-06-03 23:02:29
--------------------------
