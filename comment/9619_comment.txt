304497
9619
Đối với những ai ai yêu thích công việc kinh doanh thì không thể không ít nhất trong cuộc đời kinh doanh đầy thăng trầm sẽ luôn gặp một cuộc khủng hoảng không ảnh hưởng lớn thì cũng phải có ảnh hưởng nhỏ. Cẩm Nang Kinh Doanh - Quản Lý Khủng Hoảng đã mang đến cho ta những ví dụ điển hình của những rủi ro hay phải biết đoán trước được những gì sắp xảy ra để phòng tránh, phải biết sắp xếp, quản lí một cách thông minh và logic để giảm đi những thiệt hại cũng như rủi ro từ khủng hoảng.
5
136569
2015-09-16 14:13:50
--------------------------
127261
9619
Khủng hoảng làm đau đầu nhiều tổ chức có nguy cơ rủi ro cao như Nasa. Mọi tổ chức đều chịu khủng hoảng, cho dù họ có nhận ra điều đó hay không. Đối với 1 công ty, khách hàng có thể gây ra thiệt hại đột ngột và nghiêm trọng cho nhân viên, danh tiếng và doanh thu của họ. Một cuộc khủng hoảng lớn sẽ ảnh hưởng đến toàn bộ tổ chức và trong 1 số trường hợp như vụ Enron và ngân hàng Barings có đề cập đến trong sách. Khi lâm vào khủng hoảng, các nhà quản lý cần phải hành động nhanh chóng để tổ chức lại nguồn lực, kiềm chế khủng hoảng và cuối cùng giải quyết khủng hoảng với thiệt hại ít nhất. Cuốn sách quản lý khủng hoảng này được viết ra nhằm khắc phục tình trạng đó bằng cách giải thích những vấn đề thiết yếu của việc quản lý khủng hoảng; hướng dẫn ta những giải pháp thực tiễn để khống chế và làm chủ khủng hoảng gây thiệt hại ngoài kế hoạch và không dự báo trước được.
Rất bổ ích cho mọi tổ chức.
4
381173
2014-09-23 14:59:44
--------------------------
