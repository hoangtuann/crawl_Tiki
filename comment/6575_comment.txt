435085
6575
Khát vọng đổi đời đọc cách đây 4 năm g đọc lại , phải nói là nó ám ảnh rất lâu . Đọc ngôn tình ngôn lù đam mỹ đọc thì đọc nhiều xong quên hếch , mà đọc quyển này đã thấy rất dằn vặt  . Lỡ đọc rồi là không thể ngừng lại và đem luôn nỗi bế tắc ra ngoài ㅋㅋㅋ Khi không còn gì để mất screw it, let's do it . Nchung là thic style này quá nên đọc luôn Bức thư của người đàn bà không quen và tiếp tục buồn mệt đấu tranh khi đọc
5
1380304
2016-05-24 13:50:41
--------------------------
368109
6575
Tôi nghe một người bạn giới thiệu về cuốn sách này, ngay lập tức tôi đã đặt mua tại Tiki. Thêm một điểm thú vị nữa là tác giả Stefan Zweig mà tôi đã rất thích khi đọc tác phẩm Bức thư của người đàn bà không quen của ông. Lối viết truyện lôi cuốn, cuốn hút theo từng chặng đường và tâm lý của cô gái trẻ từ nơi nghèo khổ tới cuộc sống thượng lưu hào nhoáng. Cô đã bị choáng ngợp giữa cuộc sống phồn hoa đó một cách quá dễ dàng và bị cuốn theo. Cuốn truyện cho người đọc thấy sức hút của sự sa hoa và không làm chủ được bản thân khi không biết nắm bắt lấy cơ hội của mình.
Từ cuôc sống giàu sang trở lại cuộc sống nghèo khổ, bế tắc thật không dễ dàng. Khi người ta không còn gì để mất, người ta dễ dàng làm những việc liều lĩnh mà trước đây chưa từng có trong suy nghĩ.
Câu chuyện là cả một quá trình mô tả diễn biến tâm lý của nhân vật chính, thật hết sức hứng thú khi đọc cuốn sách này
4
431662
2016-01-13 12:55:06
--------------------------
253398
6575
 Sau Bức thư của người đàn bà không quen, tôi rất mê mẩn lối văn chương của Stefan Zweig nên ngay khi Tiki nhập Khát vọng đổi đời về tôi đã đặt mua tức thì. Phải nói là nếu chỉ xét riêng về hình thức thì tôi không hài lòng với cái bìa, hình cô gái minh hoạ dễ làm người ta lầm tưởng đây là tiểu thuyết tình cảm lãng mạn. Về nội dung mình nghĩ tác giả đang muốn đưa ra lời cảnh tỉnh chúng ta, những kẻ dễ bị choáng ngợp bởi giàu sang hoa lệ mà đánh mất bản ngã của chính mình.
4
167283
2015-08-04 13:48:35
--------------------------
