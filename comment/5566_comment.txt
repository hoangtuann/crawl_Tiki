403078
5566
Khi đi tìm truyện cho hai bé nhà mình cách đây gần 1 tháng, mình được gợi ý mua "Truyện cổ Grim". Đây là tổng hợp những câu chuyện dân gian. Mỗi câu chuyện mang một ý nghĩa sâu sắc về đạo lý nhân văn, về tình người. Tuy nhiên có nhiều truyện nội dung bị trùng lặp và có những truyện nội dung khó hiểu đối với bé 5-6 tuổi, mẹ đọc và phải giải thích cho con nhiều hơn. Dù sao đây cũng là một trong số những bộ truyện gối đầu trước khi đi ngủ cho các bé!
4
776117
2016-03-23 10:17:02
--------------------------
377158
5566
Bìa sách với trang sách có màu in trang nhã, mang phong cách cổ điển rất hợp với phong thái "truyện cổ". Chữ in rõ ràng không bị dính nét. Tuy nhiên các mẫu truyện bị lặp lại nội dung khá nhiều tạo cảm giác khá nhàm chán. Nhưng cũng không thể phủ định hai anh em nhà Grimm đã cùng nhau viết nên những câu truyện để đời. Những câu chuyện khá bình dị và gần gũi, tuy vẫn đan xen các yếu tố kì ảo như bà tiên và phép màu. Chắc chắn những bé gái, bé trai sẽ thích mê với bộ truyện này.
3
755854
2016-02-01 19:37:25
--------------------------
330692
5566
Mình mới nhận cuốn sách này hôm qua (02/11/15), sách có hình thức đơn giản nhưng trang nhã, nhiều tựa truyện Grim, có đan xen một vài truyện cổ khác nhưng cũng cùng thể loại (và các tác giả đã nói rõ chuyện này). Giấy vàng nhạt, mặc dù giấy không tốt lắm nhưng cũng chấp nhận được. Đọc qua một vài truyện thì thấy khá hay. So với những cuốn khác về truyện cổ Grim thì mình nghĩ cuốn này có số lượng tựa truyện và giá cả cạnh tranh nhất. Nếu có điều kiện thì có thể sưu tầm đủ bộ 4 tập để dành cho con cháu đọc.
4
86027
2015-11-03 08:22:14
--------------------------
