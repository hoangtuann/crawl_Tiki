488295
5465
Mình mua cuốn sách này trong đợt giảm giá tiki và không hề hối hận, nó thật sự rất thú vị.

Khổ giấy hợp ý, bìa sách đẹp mà đơn giản, mình hơi tiếc khi truyện này không có bookmark. Tựa truyện cũng khá độc đáo và hợp với bìa truyện.

Về phần nội dung, nó thật sự đã thu hút mình ngay từ những trang đầu tiên. Nó có đôi nét tương đồng với Harry Potter những cũng có những nét riêng biệt. Tuy mình không thích nhân vật nữ chính trong truyện, lập dị, quái đản chỉ quan tâm tới tình yêu của mình, điều đó khá ích kỷ khiến bộ truyện này chỉ xoay quanh ba nhân vật khiến mình thấy hơi thiếu vắng một điều gì đó. Kết thúc cũng không quá bất ngờ, nhưng mình không mong đợi gì hơn ở một cuốn tiểu thuyết giả tưởng.
4
1376143
2016-11-01 16:49:41
--------------------------
390473
5465
Ấn tượng đầu tiên của tôi về quyển sách chính là bìa sách
Quả nhiên,đúng với cái tên "Bóng tối và xương trắng" bìa sách được thiết kế khá đơn giản với gam màu tối, tiêu đề màu bạc nhìn vô cùng nổi bật, tạo cho người nhìn một cảm giác hấp dẫn, kì bí... Chất liệu giấy rất tốt, có độ xốp và dày, ngả màu vàng nhạt nên nhìn rất dịu mắt,
Về nội dung, thì câu chuyện khá hấp dẫn, lôi cuốn người đọc. Một thế giới phép thuật bí ẩn, nơi có những pháp sư mang trong mình sức mạnh vô biên và những con quái vật ghê rợn - luôn là mối đe dọa của con người. Một cô gái trẻ mang trong mình sứ mệnh giải cứu đất nước. Được đưa về cung điện để rèn luyện, cô có được mọi thứ khi trước chỉ dám ước mơ: Vật chất, sắc đẹp, quyền năng… và cả sự ưu ái của Hắc Y, người nắm trong tay quyền lực tối cao. Nhưng đổi lại chính là sự xa cách của cậu bạn thanh mai trúc mã Mal. Tôi thật sự mong chờ tập tiếp theo của câu chuyện.
5
426871
2016-03-03 22:29:50
--------------------------
357311
5465
Vốn là người mê tiểu thuyết giả tưởng, huyền bí, ngay khi quyển sách này xuất bản tôi đã nhanh tay đặt ngay không chút do dự. Và như những gì tôi hình dung, tác phẩm thật sự không làm tôi thất vọng, tôi đã đọc một mạch từ đầu đến cuối chỉ trong một ngày, không dứt ra được. Về phần nột dung, tác giả đã vẽ nên một câu chuyện hoàn toàn mới lạ, lôi cuốn, và đầy bất ngờ, tôi thích cách tác giả lồng ghép miêu tả nột tâm nhân vật cùng diễn biến câu chuyện làm cho người đọc không bị phân tâm, hay quá dài dòng đến nhàm chán như một số tiểu thuyết giả tưởng khác. Người biên dịch đã làm tốt công việc của mình, câu chữ được dịch mạch lạc,suông sẻ. Bìa sách được thiết kế ấn tượng và chất lượng in cùng giấy in rất tốt. 5 sao cho một tác phẩm không xem sẽ phí cuộc đời!
5
104927
2015-12-23 12:35:50
--------------------------
345570
5465
Bìa sách được thiết kế rất đẹp mang theo một hơi hướng huyền bí rất bắt mắt. Trọng lượng của sách rất nhẹ, khi cầm trên tay rất thích. Giấy dày, front chữ to, rõ, khi đọc không làm mình bị mỏi mắt, nhức mắt.
Nội dung trong sách thì không cần bàn cãi. Thế giới được tác giả tạo ra trong cuốn sách đầy sức hút, lãng mạn và đặc biết quyến rũ mặc cho sự tàn khốc vùng Hư Hải đen tối, chế chóc và những con volcra man rợ có thể đánh hơi con người qua mùi máu. Alina trong truyện là một cô bé dũng cảm đám xả thân cứu bạn của mình, dám chống lại Hắc Y đầy tham vọng, nham hiểm để giải thoát bản thân mình.
Câu chuyện trong sách đã để lại cho mình một ấn tượng rất sâu sắc và mình rất nóng lòng được mang về tủ sách của mình tập tiếp theo!
5
636002
2015-11-30 20:33:13
--------------------------
319076
5465
Mình vừa đọc cuốn này xong, rất hay. Bạn nào chưa mua thì nên mua cuốn này về mà đọc. Cốt truyện hấp dẫn, vô cùng lôi cuốn, phải nói là gây nghiện. Mình cũng vô cùng thích cách tác giả xây dựng nhân vật, thích nhất là Alina Starkov, lúc đầu mình không hề thích nhân vật này nhưng càng về sau lại càng thích, rất mạnh mẽ, cá tính. Mình cũng rất thích bìa của sách này, lột tả được nhiều, thể hiện rõ nội dung truyện. Đang mong chờ nhà xuất bản trẻ cho ra tập hai, lâu quá :((
5
755209
2015-10-07 20:24:52
--------------------------
296132
5465
Đọc mấy cái trích đoạn trên fanpage của Trẻ từ hồi sách còn chưa được in nên mình háo hức lắm. Quả thật là khi cầm cuốn sách trên tay không khiến mình thật vọng một tẹo nào. Cực kỳ lôi cuốn, cực kỳ hấp dẫn, phải gọi là kích thích đến nghẹt thở. 
Điểm cộng: Bìa rất là cute a, cái tựa cũng dịch rất là sát nghĩa. Thật sự là mua xong cảm thấy chỉ muốn mua tiếp tập 2 để còn biết tiếp kết cục. Tác giả này thật mình không hề biết đến, nhưng nhờ Trẻ ắt hẳn mình sẽ trở thành fa của tác giả này.
Điểm trừ: Mau mau ra tập 2 nào.

5
195704
2015-09-10 21:44:45
--------------------------
258031
5465
Mình nghe nói cuốn này còn tập 2 nữa, nên mình thấy cũng hơi buồn nhưng khi xem các nhận xét của các bạn khác mình thấy các bạn ấy nói nên đọc nên mình quyết định mua nó. Theo như mình nhận xét thì cốt truyện viễn tưởng rất lôi cuốn và gay cấn. Ngay từ trang bìa mình đã có cảm tình rồi nhưng khi đọc thì càng thích thú với nó hơn. Mình hôi hộp đọc đến trang cuối cùng và cảnh tượng thiên nhiên ấy khép lại trong lòng người đọc. Kết lại là các bạn nên đọc một cuốn sách hay này. Chất lượng sách tốt, giấy đẹp và sách của NXB Trẻ nên là hàng thật ko lo :3
4
429585
2015-08-08 10:20:40
--------------------------
230456
5465
Mình chờ bộ này được dịch từ rất lâu rồi, có sách ra là mua về ngay và đọc ngấu nghiến rồi lại phải khổ sở chờ đợi tập tiếp theo. Câu chuyện hấp dẫn ngay từ đầu, không gian có phần lạnh lẽo,u ám, tuyến nhân vật có vẻ lạnh lùng lãnh đạm lắm. Nửa đầu truyện mình không có ấn tượng gì với Mal mà bị thu hút bởi Hắc Y hơn nhiều, cảm giác nhân vật này có nhiều bí ẩn để khám phá hơn.
Càng đọc mình càng bị lôi cuốn, có lúc còn vừa muốn đọc nhanh để biết tiếp theo thế nào vừa sợ đọc hết không còn gì để đọc, lại phải chờ đơi :))
Mong NXB Trẻ sớm ra tiếp bộ này cho mình đỡ phải mong như mong mẹ đi chợ về :))
5
459083
2015-07-17 14:56:06
--------------------------
218345
5465
Bộ tiểu thuyết Grisha nói về những người có năng lực siêu nhiên, tuy không phài mới lạ nhưng mang một chất riêng, tác giả đã tạo dựng một thế giới mà những con người đó có vai trò rất quan trọng. Truyện khắc sâu được tâm lí nhân vật diễn biến qua từng giai đoạn, những suy nghĩ biến đổi không ngừng khi biết được sự thật. Thành công của truyện là tạo ra 1 bất ngờ lớn về tính cách và bản chất thật của Hắc Y. Những diễn biến sau đó khiến cho người ta không ngờ tới. Mình rất hy vọng cặp đôi Alina và Mal sẽ hạnh phúc bên nhau.
Bìa sách khá là đẹp, giấy cũng rất tốt, chất lượng in ấn làm mình khá hài lòng.
5
315293
2015-06-30 15:39:20
--------------------------
217293
5465
Nhìn chung thì đây là 1 quyển fantasy ổn từ mặt hình thức cho đến nội dung. Cái bìa mình cũng khá ưng vì nó toát lên được tinh thần của truyện, thấy rất hợp. Cốt truyện của quyển này nói lạ thì cũng chưa tới mức lạ, nhưng cũng không phải là quen. Nhân vật nữ xây dựng khá tốt, duy có tính cách là chưa được nhất quán lắm, thú thực thì mình cũng không ưa tính cách ấy lắm, nhưng xét ra thì cũng thực tế. Có thể 1 phần do đọc thấy mấy nhân vật nữ kiểu "chính diện" quá quen rồi chăng =))
4
469377
2015-06-29 09:07:47
--------------------------
180695
5465
Tôi đọc tất cả nhận xét về truyện này mà tôi có thể kiếm được và quyết định mua ngay về vì có quá nhiều lời khen.

Về nội dung: nhìn chung thì hay. Suốt 2/3 câu chuyện tôi không hề thấy thích nổi nhân vật nữ chính. Tôi không biết cô bao nhiêu tuổi, nhưng chắc chắn hơn 15 tuổi, và nếu tác giả muốn miêu tả cô ấy là một cô bé đang tuổi dậy thì với tính khí thất thường, dở dở ương ương, lúc nóng lúc lạnh thì tác giả đã rất thành công. Tần suất thay đổi giữa việc Alina nổi giận với mọi người, ương ngạnh chống đối, rồi dịu lại, rồi co rúm sợ hãi chấp nhận số phận mới của mình dày đặc đến nỗi tôi cứ muốn hét lên "Cô ta bị cái quái gì vậy?" Có những lúc cô ấy thích thú chìm đắm trong cuộc sống nhung lụa dù chỉ 1 giây trước đó cô còn chỉ trích sự xa hoa quá đáng của cung điện so với những vùng đất nghèo nàn khác của đất nước. Và nhiều điều khác nữa thể hiện rõ hai thái cực này làm tôi còn nghi ngờ liệu Alina có bị nhân cách phân liệt hay không.
Nhưng 1/3 truyện còn lại thì tôi mới thấy được sự kiên cường thực sự, thấy được một người trưởng thành, biết nhẫn nhịn, biết nghĩ cho người khác.

Đây rõ ràng là một truyện thuộc thể loại fantasy nhưng thú thật những gì mang chất fantasy lại ít ỏi đến đáng thương, khi tác giả hầu như chẳng miêu ta gì mấy về sức mạnh của các Grisha, chỉ nói qua loa. Cảnh đất nước lầm than cũng chỉ được nhắc rải rác trong vài câu suy tưởng của nhân vật chính. Và hơn hết, sức mạnh của các tiết độ sư cũng không phải là điều gì mới mẻ, vì khi đọc truyện tôi liên tưởng được ngay đến bộ phim "Tiết khí sư cuối cùng". Những câu chữ miêu tả cảnh cung vàng điện ngọc, quần là áo lụa, nữ trang vàng bạc còn nhiều hơn những bài học và những tiến bộ trong năng lực của Alina.
Có lẽ tôi đã quá kỳ vọng vào yếu tố fantasy của truyện, hy vọng nó phải đồ sộ hơn, chi tiết hơn nữa.

Về hình thức: tôi thích bìa truyện, thích dòng chữ Bóng tối xương trắng màu bạc. Giấy ngà vàng dịu mắt, tuy hơi nhẹ hơn tưởng tượng do giấy rất xốp.

Về trình bày: nửa đầu chỉ có vài lỗi đánh máy, nhưng nửa sau thì mât độ dày hơn và nhiều chỗ bị bay mất chữ. Khi viết các nhận xét, tôi những mong các nhà xuất bản/cty phát hành có thể đọc được để cải thiện các lỗi và hoàn thiện hơn các quyển sách trong những lần tái bản sau.
3
281707
2015-04-09 22:17:04
--------------------------
166704
5465
Đắn đo rất lâu mới quyết định đọc, vì những series hiện giờ thường có cốt truyện không lạ cho lắm. Nhưng với quyển này thì phải nói là hay. Tôi rất thích cách tác giả xây dựng ngoại hình, tính cách cho cô gái Alina, rất lạ và độc đáo, khiến người đọc có cảm tình và thấy chân thực hơn so với những nhân vật nữa vượt trội cả về ngoại hình, tính cách lẫn năng lực.  Bối cảnh truyện có phần u ám, giọng văn của tác giả lôi cuốn, khiến người đọc không dễ đoán. Mong tập tiếp theo ra sớm sớm ghê.
4
198257
2015-03-13 11:22:19
--------------------------
165986
5465
đây chỉ là 1 trong 3 tập của series nhưng tác giả đã mở ra 1 câu truyện tuyêt
quả thật đây là 1 cuốn sách không giống bất kì cuốn sách nào tôi từng đọc
một alina k hoàn hảo gầy gò nhưng lại  là 1 nhân vật tuyệt vời đối với tôi. Cô ấy dần trưởng thành hơn, mạnh mẽ hơn. Hắc Y mạnh mẽ, bí ẩn & quyến rũ. Mal, cậu bạn thanh mai trúc mã của Alina, chung thủy, đáng tin cậy, thích đôi này lém. cộng với đó tác giả đã gợi lên 1 cuộc sống bóng tôi bởi sụ đố kị xương trắng là cái gì đó bí ẩn mà có lẽ 2 tập nữa mới biết đc. hóng quá
5
540816
2015-03-11 18:24:05
--------------------------
158883
5465
Khi tôi được biết về cuốn này, thì nó còn đang trong giai đoạn in ấn. Điều làm tôi ấn tượng nhất chính là tên và bìa. Cái tên khá lạ, có phần kì dị và bí ẩn, cộng với bìa rất đẹp và quyến rũ, Bóng tối và Xương trắng đã ghi được 2 sao. 
Hành văn mềm mại (của dịch giả và tác giả), cốt truyện khá hay, mạch truyện và tình tiết có diễn biến li kì. Đôi bạn thanh mai trúc mã được khắc họa rõ nét, rất chân thực. Hắc Y thì lạnh lùng, thần kì và quyến rũ, tuy nhiên thì...
Mong rằng nhà xuất bản sớm ra phần 2, hay thật.
4
435897
2015-02-14 11:01:36
--------------------------
158146
5465
Thế giới trong truyện Bóng tối tối và xương trắng là 1 thế giới đầy rẫy những nguy hiểm, không chỉ là những con quái vật ghê rợn mà bên cạnh đó còn có sự đố kị, lòng tham lam của con người.  Đây là câu truyện mình không tài nào đoán được phần kết sẽ ra sao. 
Tạo hình Alina trong truyện không phải là 1 cô gái hoàn hảo. Cô là trẻ mồ côi, gầy gò, yếu ớt, không có gì đặc biệt ngoài quyền năng mạnh mẽ được ẩn giấu trong con người cô. Mình lại thích nhân vật chính như vậy, không màu mè, cảm giác đọc gần gũi hơn. 

Mal và Alina là 2 người bạn thân từ thuở nhỏ, đến khi lớn lên tình cảm giữa họ ngày càng trở nên khăng khít hơn.,nhưng biến cố xảy ra khiến cho tình bạn của họ nhiều lần bị thử thách.Hắc Y là 1 nhân vật gây cho mình nhiều bất ngờ , nhất là những trang cuối của câu truyện.

Thật sự là mình mong NXB Trẻ nhanh nhanh dịch xong 2 phần còn lại của bộ truyện này để mình có thể biết được kết thúc của Alina, Mal và Hắc Y. Hóng phần kết :)))))
5
516346
2015-02-11 17:37:44
--------------------------
154020
5465
Mới đầu mình mua quyển này là vì bìa sách quá đẹp, qua phần giới thiệu nội dung thì nghĩ có lẽ sẽ giống với I am Number Four. Nhưng khi đọc thì mình biết mình sẽ cực kỳ tiếc nếu không mua Bóng tối và Xương trắng. Câu chuyện hấp dẫn ngay từ đầu, không gian có phần âm u, ta dễ bị nhầm lẫn đâu là thiện đâu là ác, đâu là thật, đâu là giả dối.. Nửa đầu truyện mình không có ấn tượng gì với Mal mà bị thu hút bởi Hắc Y, lạnh lùng nhưng đầy bí ẩn và cuốn hút (có ai như mình không ta? =)) ), luôn nghĩ đây là anh chàng tốt vì những biểu cảm của Hắc Y quá thật, nhưng đến cuối truyện thì hình tượng đã sụp đổ.
Trong thời gian chờ NXB Trẻ làm quyển 2 chịu khó cày bản tiếng anh vậy :)
5
43144
2015-01-28 08:50:09
--------------------------
137373
5465
Series này có 3 tập, và đây là tập 1.
Lối hành văn, các nhân vật, & cốt truyện đã cuốn hút tôi vào từng trang sách. 
Alina là 1 nhân vật tuyệt vời đối với tôi. Cô ấy dần trưởng thành hơn, học hỏi được nhiều điều hơn qua từng diễn biến của câu chuyện. Hắc Y mạnh mẽ, bí ẩn & quyến rũ. Mal, cậu bạn thanh mai trúc mã của Alina, chung thủy, đáng tin cậy, có thể là chỗ dựa tốt.
Thế giới mà tác giả tạo dựng trong câu chuyện này thật kỳ ảo, sống động, khác lạ & độc đáo. Diễn biến truyện lúc thăng lúc trầm, có những đoạn mà sự gay cấn lên tới đỉnh điểm ko thể đoán trước được. 
Nội dung tôi cho 4,5 sao & hình thức tôi cho 0,5 sao (bìa sách quá đẹp!).
5
75959
2014-11-26 10:06:39
--------------------------
