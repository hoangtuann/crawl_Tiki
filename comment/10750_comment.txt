360927
10750
Thiết kế bìa bắt mắt , nổi bật thì câu truyện lại chưa có sức hút mạnh mẽ lên người đọc với nhân vật sam và lexie giản dị và thân thương cũng chưa thực sự là đam mê lớn với người đọc bởi tính khí chưa có khác nhau thì sau này ít đủ điều kiện làm nên tác phẩm đáng nhớ , cách miêu tả hời hợt , chưa đầu tư chất liệu đặc thù tả cách trình bày nhạt nhòa , tối tăm , ánh sáng bao phủ chậm rãi làm mau quên bản chất vui vẻ vốn sẵn có .
3
402468
2015-12-30 03:02:52
--------------------------
309966
10750
Nghe nhiều người khen tiểu thuyết này hay, mình cũng mua để xem thử. Mình rất thích thiết kế bìa này, hình ảnh đẹp, màu sắc ổn, hài hòa, điểm trừ là cả quyển sách khá nặng so với các quyển khác có số trang tương tự. Nội dung ổn, cuốn hút, mình thích Sam, thích Lexie, thích chuyện tình cảm của họ, thích sự đưa đẩy của số phận để họ đến bên nhau, thích cái cách họ ở bên nhau cứ tự nhiên như thế, nhưng có đôi chỗ câu văn tối nghĩa làm mình không hiểu được tác giả muốn nói gì, đây có lẽ cũng làm giảm phần nào hứng thú của mình với truyện.
3
39798
2015-09-19 11:13:46
--------------------------
290096
10750
Nội dung cuốn sách mô tả tình dục quá nhiều, quá tỉ mỉ.còn dẫn truyện thì rời rạc, thiếu lôi cuốn. một cuốn sách không đáng đọc. Tôi cũng chưa đọc sách của Carly nên không nhận xét do bản dịch hay tác giả nhưng cảm giác khi đọc xong cuốn sách này là khá tệ. Mặc dù được xây dựng trên một cốt truyện khá tốt,chuyện tình trong mơ của hai con người từng bị tổn thương bởi tình yêu,chàng và nàng đối diện với tình cảm của mình như chim bị tên sợ cành cong , nhưng rồi họ đã vượt qua mọi trở ngại để đến với nhau. Nhưng với cách diễn đạt thế này thì thật đáng thất vọng.
1
480142
2015-09-05 08:39:06
--------------------------
265514
10750
Mình nghe bạn bè nói tác phẩm này rất đáng đọc nên đã lên tiki đặt mua về. Đây giống như một câu chuyện ngôn tình phiên bản Mỹ: chàng thì một bước lên tiên khi từ người tầm thường được vô số cô theo đuổi; nàng thì lạnh lùng nhưng cuối cùng vẫn bị chàng khuất phục - một chuyện tình đẹp lãng mạn không tưởng. Tuy nhiên trong quá trình đọc sách mình cảm thấy rất thất vọng. Bản dịch tồi tệ không thể tả được. Câu văn rất tối nghĩa và không thể hiện được ý của tác giả. Cuối cùng là sức hút của câu chuyện bị giảm đi hẳn, người đọc cũng tụt hứng chỉ vì dịch thuật. Chẳng biết Chibooks có thể chọn được dịch thuật viên và biên tập viên tốt hơn không? Thất vọng!!!
2
100797
2015-08-13 23:33:03
--------------------------
204709
10750
Nội dung của "Hãy hôn em nếu anh có thể" không có gì cần phải chê, bởi cốt truyện hấp dẫn, diễn biến hợp lý, chuyện tình của Sam và Davis rất thú vị và bất ngờ; tuy nhiên mình cảm thấy thật khó để chấp nhận bản dịch này, một bản dịch vô cùng tồi tệ không khác gì đưa đoạn văn bản vào Google dịch. Câu cú lủng củng và tối nghĩa, không có chút nào gọi là mượt mà, có nhiều câu cụt ngủn nên khi đọc thấy mất hứng vô cùng, dịch thuật quá chán làm ảnh hưởng đến giọng văn, giảm đi sức hút của truyện, biên tập cũng tồi, thật thất vọng quá.
3
393748
2015-06-04 19:01:14
--------------------------
100522
10750
Thật sự mình đã đọc một vài tác phẩm của Carly Phillips và thấy tác giả có môt giọng văn khá lôi cuốn, nhưng sau khi đọc bản dịch cuốn "Hãy hôn em nếu anh có thể" của Chibooks mình cảm thấy rất thất vọng, sự thật là bản dịch không toát lên được văn phong đặc trưng của tác giả và câu chữ lủng củng, cẩu thả, biên dịch và biên tập sơ sài khiến cho câu chuyện trở nên khó hiểu, đôi chỗ như sử dụng google translate vậy. Bên cạnh đó, thiết kế bìa và chất liệu giấy của Chibook không đổi mới, lúc nào cũng y như nhau. Nếu vẫn cứ lối dịch và biên tập thế này mình nghĩ sẽ không tiếp tục ủng hộ những tác phẩm khác của nxb được.
1
125574
2013-11-26 09:58:48
--------------------------
99593
10750
Mình biết đến tác giả Carly Phillips lần đầu qua cuốn "Đố dám yêu em" và khá ân tượng nên đã quyết định mua ngay cuốn này của bà. Đúng là duyên phận luôn được định đoạt, nó đã mang hai con người xa lạ Sam và Lexie tưởng chừng như trên dòng đời sẽ không bao giờ chạm nhau được đến với nhau với một cái duyên khá tình cờ. Tình yêu của hai người đầy những thử thách, buộc họ phải nhận ra, nhìn về tương lai của mình đâu sẽ là thứ mình cần, đâu là những gì sẽ luôn bên mình để lấp đầy cho trái tim cô đơn. Và khi trái tim lên tiếng mách bảo hai người đã hướng về nhau sẵn sàng chia sẻ cuộc sống mà trước đây chỉ dành riêng cho mỗi người cũng chính là thử thách cuối cùng được đặt ra. Gấp cuốn truyện lại người đọc ắc hẳn sẽ hài lòng.
Về phần bản dịch của tác phầm này mình thấy khá ổn.
Rất mong chờ những tác phẩm tiếp theo của bà!
4
54192
2013-11-16 14:53:59
--------------------------
80711
10750
Đây là câu chuyện bình thường nhưng lại không bình thường. Thực ra trong cuộc sống những mối tình như vậy, kiểu nhân vật như vậy, kiểu tình huống và hoạt cảnh như vậy, ta vẫn có thể thấy được, tuy nhiên không phải câu chuyện nào cũng có thể tồn tại và hình thành đầy hấp dẫn như vậy được. Câu chuyện tình khá thú vị và cốt truyện khá lôi cuốn, bìa sách và chất lượng in cũng rất tốt. Tuy nhiên có một điều mình nghĩ quyển sách chưa thật sự làm tốt đó là phần dịch. Thật sự có nhiều câu tối nghĩa và quá cụt, làm cho trong tình huống câu tự dưng thấy hụt hẫng và thiếu ý. Ngoại trừ khuyết điểm này thì tập sách rất thú vị.
5
28320
2013-06-13 22:00:56
--------------------------
16834
10750
Đây là một tác phẩm hay nhưng bạn dịch quá tệ đã phá hỏng hết nội dung, ý nghĩa của truyện, dịch truyện chả khác nào đưa vào google dịch. Thật sự Chibooks làm tôi quá thất vọng về bản dịch này.
3
14924
2011-12-27 22:38:35
--------------------------
8769
10750
 Đây là một cuốn sách mà theo tôi đánh giá là khá sâu sắc. Các mẫu nhân vật không có gì đặc biệt. Họ rất bình thường. Còn về cách tạo dựng và xử lí tình huống khá ổn. Không có nhiều chi tiết cao trào mang tính đột phá cho truyện. Nội dung truyện khá sâu sắc và có ý nghĩa đối với cuộc sống và tình yêu của con người. Cuộc sống của mỗi người có thể hơn nhau ở sự may mắn. Coop là một trong những điển hình đó. Từ một anh chàng độc thân bình thường trở thành người chồng lí tưởng của mọi cô gái. Và tình yêu là không thể đoán và điều khiển được. Lexie lúc đầu không hề chú ý coop mà là chiếc nhẫn thế nhưng cuối cùng lại đánh mấ trái tim mình lúc nào không hay. Lời văn trau chuốt, giàu cảm xúc đã khiến cho câu chuyện hay hơn.
3
7233
2011-07-24 09:30:03
--------------------------
