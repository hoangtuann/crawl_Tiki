437188
6003
Tôi không hiểu tại sao 1 quyển sách như thế này lại được xuất bản :))
Cũng may là tôi nhận quyển sách này trong một đợt khuyến mãi của tiki, nếu mua nó thì chắc tôi tiếc tiền đứt ruột mất.
Nam chính được tôi hình dung qua 2 từ "biến thái". H trong quyển này thật quá "thẳng thắn" và "dâm tục" đến ngỡ ngàng.
Nữ chính quá "tóc vàng hoe", tôi từng đọc rất nhiều quyển về tiểu bạch thỏ, trong sáng , đáng yêu, ngờ nghệch nhưng quyển này thì thật là... Tôi không nghĩ cô ấy sẽ lấy nam chính làm chồng luôn ấy chứ :))
Hẹn hò từ những điều dung tục, cuộc sống vợ chồng nhạt nhẽo, cuối cùng li dị và đường ai nấy đi.
Nói chung là nhảm vô cùng. Thật tệ.

1
532031
2016-05-27 20:39:05
--------------------------
384204
6003
Em đã gặp được chàng trai vì tình yêu mà bất chấp tất cả chưa? Anh chính là chàng trai vì tình yêu mà bất chấp tất cả..
Câu nói ngọt ngào ấy làm mình xao xuyến. Nữ chính thật khổ sở cho người ta cảm giác thương chị vì tình yêu mà đau khổ. Có lẽ ai cũng phải một lần vì tình yêu mà đau? Nhưng nũ chính thật may mắn vì có nắng sớm ấm ấp chiéu xuống chị sau đêm tối có người vì chị bất chấp tất cả. 
Văn phong nhẹ nhàng đặc biệt của Ngãi Mễ đem mình hiểu rõ hơn về tình yêu và hy vọng.
4
1061830
2016-02-21 22:07:37
--------------------------
368023
6003
Truyện rất dày, gần 2 đốt ngón tay nhưng lại nhẹ hơn so với những truyện cùng cỡ khác rất nhiều vì dùng giấy siêu nhẹ. Loại giấy này nhẹ và đẹp lúc ban đầu thôi chứ để lâu thì lại bị ố vàng. Sách có bookmark nhưng bookmark hơi đơn điệu và mỏng.
Thiết kế bìa đẹp khiến mình có cảm giác khá dịu nhẹ, thoải mái.
Về nội dung: mình cảm thấy truyện này dày thì đã sao, nội dung đâu có hay lắm đâu, chỉ có thể nói là ở mức tạm ổn thôi. đã thế mua lại còn đắt, ai thích mua thì nên đọc qua trước ở trên mạng đã, rồi hẵng quyết định có mua hay không.
4
800850
2016-01-13 09:57:01
--------------------------
358929
6003
Lúc đầu mình chọn mua cuốn sách này vì tựa đề có chút gì đó rất nhẹ nhàng, và Ngải Mễ cũng là tác giả mà mình thích từ cuốn " Cùng anh ngắm hoa sơn trà" . Phải nói là truyện này khá bình thường và không tạo được xúc động như những cuốn trước đó của tác giả . Nhiều khi đọc truyện thấy ghét nữ chính kinh khủng , vì sự ấu trĩ  và tình yêu thái quá của chị này . Ghét luôn cả anh chồng luôn , vì ngoại tình mà . Mình đọc được có 2/3 cuốn rồi để lại luôn , có lẽ vì truyện chưa thu hút được mình .
4
743273
2015-12-26 09:58:55
--------------------------
317012
6003
Tôi mua bản Ebook truyện này qua MIKI, tôi đã ngấu nghiến nó trong vòng 1 ngày thứ bảy. Tôi không nghĩ truyện nhảm nhí như một số nhận xét. Nhìn từ góc cạnh khác, thì Vương Quân là cô gái thực sự hết lòng vì yêu. Có điều, tôi thất vọng về người mà cô yêu, sau này làm chồng cô. Đừng nói nhân vật này không có thực ngoài đời, mà có nhiều là khác. Ích kỷ, tham lam, hời hợt và cơ hội - đó là Vương Thế Vỹ. Cho nên, Vương Quân dám thay đổi để làm lại với một người kém cô về cả tuổi đời, về cả than phận ở nước Mỹ. Có điều, tôi không thích chuyện cô ấy và Kevin phát sinh quan hệ tình cảm trên mức, trước khi chấm dứt cuộc hôn nhân trước. Có lẽ, do cô ấy đang sống ở Mỹ, nên suy nghĩ cũng thoáng hơn? 

4
572177
2015-10-02 10:52:04
--------------------------
309719
6003
Giá tiền của sách thì khá cao nhưng nội dung thì lại không chất lượng như mình nghĩ. Ban đầu mua sách chỉ vì ấn tượng với nhan đề Cô gái tháng sáu (do mình cũng sinh tháng sáu). Nhưng khi nhận được sách và bắt đầu thưởng thức nó thì mình lại gần như không muốn đọc nó nữa. Cách xây dựng hình tượng nhân vật không thực tế một chút nào. Cốt truyện được sắp xếp một cách lộn xộn, thiếu sự cân đối và hài hòa khiến mình phải đọc đi đọc lại hai, ba lần trang đầu tiên mới bắt kịp nhịp của Ngải Mễ. Và mình không có dấu hiệu muốn đọc tiếp khi hiểu được ý của cô.
2
534860
2015-09-19 08:58:51
--------------------------
244614
6003
Mình mua sách về từ nhà sách, điều đầu tiên thấy đó là cái bìa của nó trông khá thích mắt. Khi đọc rồi mới thấy vừa hối hận vừa thất vọng. Chính là cái cách xây dựng hình tượng nhân vật đã làm người ta ngán chẳng muốn đọc nữa. Nữ chính thì ảo tưởng và quá ích kỉ, nói chung là tính cách làm người ta cảm thấy rất chán ghét. Còn nam chính lại là một tên quá... dâm đãng, tơ tưởng bệnh hoạn. Còn nữa, mình đọc từ đầu đến cuối quyển truyện thì không hiểu Cô gái tháng sáu là ai.
1
676659
2015-07-28 12:27:47
--------------------------
185685
6003
Thực sự là thất vọng, bản thân ban đầu (khi chưa mua) tôi rất thích cuốn này vì tên sách tuyệt vời (chắc do tôi sinh tháng 6), bìa đẹp, đọc review ổn , mặc dù thấy nhận xét trên tiki (lúc tôi quyết định mua) thì tệ hại, nhưng khi lên instagram thấy ai mua cũng bảo hay và ổn, mà dù sao thì tôi vẫn và đã quyết định mua sách này rồi.
Chờ mãi thì sách cũng đến tay, và rồi hăm hở đi đọc. Cuối cùng là đã mấy tháng trôi qua tôi chỉ đọc chưa được nửa quyển! Bản thân thấy có lẽ có mỗi 2-3 chương đầu lúc Vương Quân yêu thầm rồi tưởng tượng là hay, sau đó là lúc "hẹn hò" tôi thấy thật tệ luôn, mặc dù H trong ngôn tình không phải không có nhưng ở đây nó quá thẳng thắn, Vương Thế Vĩ quả thật là... rất dâm tục. Cố gắng lắm đọc tiếp nhưng chỉ thấy NHÀM và NHÀM, có lẽ phần sau sẽ hay hơn nhưng tôi không thể đọc tiếp được và có lẽ tuổi của tôi vẫn chưa thể hiểu được một cuộc sống, cái hay của truyện.
2
178502
2015-04-19 19:22:55
--------------------------
178775
6003
Đây là lần đầu tiên tôi đọc truyện của Ngải Mễ. Tôi rất bất ngờ với cốt truyện. Ban đầu đọc, tôi thấy nhân vật nam thật biến thái và... dâm đãng, cứ nghĩ Vương Quân sẽ không chọn một người như thế làm chồng, nhưng nó lại diễn ra không như tôi muốn. 
Một cuộc sống thật nhàm chán, tẻ nhạt diễn ra với vợ chồng Vương Quân và Vương Thế Vĩ. Nhưng, từ trong câu chuyện đó, một câu chuyện mới được mở ra. Đó là khi Vương Quân nhận ra cô không hề thật lòng yêu chồng mình và anh ta cũng thế. Cô đã gặp được người mà cô thật lòng yêu-Kevin. Từ những lần  cùng làm việc, cùng ăn, cùng cười nói. Mọi thứ diễn ra thật chậm để người đọc cảm nhận được tình yêu đích thực.
Tuy nhiên, Vương Quân đã mất quá nhiều thời gian để nhận ra cuộc sống vô nghĩa mà mình đang sống, và Kevin lại là một người có quá nhiều khuyết điểm và sai lầm
4
526066
2015-04-05 17:24:14
--------------------------
163168
6003
Lúc đầu khi đọc preview của cuốn sách này, tôi đã đặt kì vọng là nó sẽ rất hay! Vì đọc cmt trên mạng và instagram ai cũng nói nhẹ nhàng và lôi cuốn! 
Nhưng đến khi cầm trên tay và đọc nó! Tôi đã bỏ hết gần nữa cuốn!!! Nếu có thể dùng 2 từ để miêu tả về cuốn sách này tôi sẽ dùng 2 từ: NHẢM NHÍ! 
Cả nam chính và nữ chính đều mang lại cho tôi cảm giác chán ghét! Nữ chính quá ích kỷ, nam chính quá ham mê tình dục! Hoang tưởng, hoặc có thể là cố tình hoang tưởng...
Nhạt, thật sự rất nhạt!
2
79420
2015-03-04 11:59:24
--------------------------
158034
6003
Theo mình có thể tác giả đã đưa câu chuyện của chính mình vào chuyện như đưa ví dụ về Quỳnh Dao. Mình có cảm giác anh nam chính không thật sự yêu chị Vương Quân. Chị Vương Quân thì quá mù quáng. Anh Kevin hình như cũng không thật sự yêu chị Vương Quân. Nếu hôm gặp anh Kevin chị ấy mặc áo lót và quần áo bình thường thì chưa chắc anh ấy đã yêu chị í. Nói chung các anh mình thấy đều không yêu chị Vương Quân thật lòng và mình không thích sự ly hôn. Dù sao cũng cảm ơn Ngải Mễ.
3
558708
2015-02-11 08:40:01
--------------------------
157397
6003
Thật sự mà nói mình cảm thấy rất chán khi đọc cuốn sách này. Chắc vì từ trước giờ chỉ thích những tình cảm lãng mạn của tuổi trẻ nên khi đọc sách này mình thấy khá hụt hẫng. Nữ chính là người mà mình thấy giống như là người ích kỉ, thích gì làm đó đôi khi thiếu suy nghĩ, chỉ nghĩ tới cảm nhận của mình. Điều này có thể chấp nhận khi Quân còn ở tuổi 20 mấy, nhưng đến khi gần 40 tuổi rồi mà vẫn như vậy khiến mình thấy rất khó chịu. Không những vậy lại còn một mực cho rằng chồng mình ngoại tình rồi sau đó đi rêu rao cho mọi người biết trong khi chỉ dựa vào phỏng đoán của bản thân mà chưa có bằng chứng gì (dù sự thật thì ông chồng đó cuối cùng đúng là ngoại tình)... Nói chung đây chỉ là ý kiến cá nhân của riêng mình, vì mình thật sự rất bực nữ chính cho nên phải lên đây viết nhận xét. Tuy nhiên, cũng có cái hay đó là luôn hết mình vì tình yêu, nhưng đôi khi hơi lố!!!
2
460759
2015-02-08 19:56:24
--------------------------
147763
6003
Đã từng đọc và yêu thích câu chuyện tình cảm động trong Cùng anh ngắm hoa sơn tra của Ngải Mễ nên mình khá kỳ vọng vào cuốn sách này, nhưng Cô gái tháng Sáu thực sự không gây được ấn tượng lắm với mình. Nội dung truyện thì cũng được nhưng cái mình không thích là tính cách, lối suy nghĩ và những hành động của nữ chính, minh có cảm giác cô ấy khá là ích kỷ và sống quá theo bản năng, thậm chí còn hơi có chút hoang tưởng (hay cố tình hoang tưởng???) khi cứ đổ riết tội ngoại tình cho chồng trong khi chính bản thân mình mới là người muốn ly hôn và có tình cảm với người khác. (Mặc dù ông chồng cũng chả ra gì). Có lẽ mình còn quá trẻ để hiểu về cuộc sống hôn nhân gia đình và những mối quan hệ phức tạp kiểu này nên cuốn truyện này thực sự không phù hợp với mình chăng nhưng thực sự mình không thích câu chuyện này lắm.
2
17740
2015-01-08 20:05:41
--------------------------
128972
6003
Nữ chính trong truyện đã từng bất chấp hết tất cả hy sinh mọi thứ để yêu một người nhưng khi bị phản bội cô chợt nhận ra rằng . Phụ nữ nghĩ tình yêu là tất cả đối với họ còn đàn ông nghĩ tình yêu chỉ là một phần của cuộc sống mà thôi ! Trái tim này đã bị tổn thương rất nhiều vì người đó nên cô không dám yêu bất kì ai nữa . Số phận của nữ chính quả thật rất éo le và bi thương cách tác giả miêu tả số phận của nữ chính đã khiến cho độc giả thương xót cho nữ chính và mong muốn cô gặp được tình yêu đích thực của mình . Nam chính giống như tia sáng chiếu xuống cuộc đời tối tăm của nữ chính . Anh không quá tài giỏi cũng không dịu dàng nhưng trái tim của anh đã trao cho cô từ lâu rồi . Đôi khi điều tốt đẹp nhất rất khó tìm nhưng sẽ khiến ta trân trọng không bao giờ quên
4
337423
2014-10-06 08:24:46
--------------------------
