380148
7834
Truyện nhạt , kết thúc dang dở , không hay , diễn biến hơi dài dòng , biểu cảm nhân vật chưa sâu , thảm họa vẫn còn chờ đợi một vài điều không hay , nhân vật đan xen rất nhiều , không nhớ ra đây là người có điều kiện gì với câu truyện , chuyển thể từ các số phận không mấy độc đáo , xuất hiện đều đều nên không phải làm đa dạng đến đặc biệt quá mới nhớ tới , ngôn từ chưa có ghi dấu ấn về nữ chính , tình tiết giống nhau .
3
402468
2016-02-14 00:26:15
--------------------------
333903
7834
không sai,ảo vọng đấy.Bởi nếu là hiện thực,nó hẳn phải nghiệt ngã biết chừng nào.Bởi nều là ảo vong,thì đó chính là thứ để chúng ta mơ mộng và hướng tới.
Giấc mơ truyện vẽ ra là một cuộc sống bình yên mà Nhược Hy(Bộ bộ kinh tâm) không bao giờ nắm giữ nổi,không bao giờ có được.Nhan Tử La không thể coi là yêu Tứ gia,vì nàng chỉ biết mỗi bản thân mình,nàng chẳng thèm để trận chiến 'Cửu long đoạt vị' vào trong mắt.Nói thế không có nghĩa là nàng có quyền lực bàng quan,thờ ơ,mà là bởi nàng không có giá trị gì hêt,nàng chỉ là một thiếp thất bé nhỏ không có tính uy hiếp,ông trời thương nàng,cho nàng đúng dưới tán cây cổ thụ có mênh đế vương-Dận Chân,hay Hoàng đế Ung Chính của dòng họ Ái Tân Giác La.
Nàng sống vô âu vô lo trong ảo vọng ấy,những gì đối với nữ tử thời xưa là khổ sở,thì với cách nhìn nhận của người hiện đại,nàng xem đó là cuộc sống 'ăn sung mặc sướng'.Dĩ nhiên còn nhờ vào 'bàn tay vàng' của tác giả nữa.
Có một điều tôi không hiểu,'tối chân tâm' mà tác giả muốn biểu lộ là gi?Nộ dung truyện chỉ rặt một màu sữa của những giấc mơ xuyên không,chả lẽ chân tâm là tình cảm,niềm hi vọng ước ao của các cô gái về một giấc mơ không thực?Nếu thế thì xin chia buồn,vì tâm ấy đắng như tâm sen,thực tế xin mời đọc Bộ bộ kinh tâm để tìm thêm
1
487743
2015-11-08 15:05:47
--------------------------
291937
7834
Bất kể Nhan Tử La nói gì, làm gì đều dễ dàng trở thành "hiện tượng" và gây ấn tượng với tất cả mọi người từ Khang Hy cho đến người dưới. Đặc biệt là con trai và 2 con gái của Nhan Tử La, cứ như thiên tài vậy, vừa sinh ra được mấy ngày mà phản ứng như con người ta 3 tháng. Điểm này làm tôi càng đọc càng thấy truyện xàm, đúng là tác phẩm mang tính giải trí cao. Tập 2 này tôi thấy còn không hay bằng tập 1, tuy cũng có những phân đoạn làm độc giả phải bật cười nhưng tình tiết vô lý dường như nhiều hơn và "vô lý" hơn rất nhiều. Tóm lại, truyện này chỉ có 1 điểm tôi thấy hài lòng chính là hình thức, đặc biệt là chất giấy, không hề ố vàng dù để rất lâu trong tủ.
2
31339
2015-09-06 21:20:03
--------------------------
259142
7834
Chưa bao giờ đọc bộ xuyên không đời nhà Thanh nào chán và nhảm như bộ Tối Chân Tâm này.Từ các tuyến nhân vật, tuyến nội dung đều dài dòng, đến hình tượng nhân vật đều rất tệ. Tứ gia, Nhan Tử La và các nhân vật khác đều gây thất vọng. Tôi đã cố gắng để đọc hết xem có gì nổi bật không, nhưng thật đáng thất vọng. Nếu đọc để giết thời gian và chỉ để giải trí thôi thì không có gì đáng bàn, nhưng nếu tìm một bộ truyện nội dung thật hay thì tôi khuyên tốt nhất không nên mua. 
1
13202
2015-08-09 09:39:41
--------------------------
245710
7834
Phần 2 vẫn như mô tuýp hài hước nhưng nhiều chuyện thú vị hơn đã xảy ra . Chỉ tội Bát gia , cũng yêu Nhan Tử La mà không làm gì được ngoại trừ để vết thương lòng gặm nhấm nổi đau. cũng tội Thương My , yêu Bát gia mà trong tim ngài không hề có hình bóng của mình . Cực kì ấn tượng câu nói của Bát Gia " Nếu không là người đó , là ai cũng vậy thôi "
Lúc Cách cách và thập bát hoàng tử mất , tôi cực kì đau lòng và khóc rất nhiều . Tác giả ác quá . Truyện toàn những hình ảnh hài hước , vậy mà để hai nhân vật dễ thương mất .
5
90456
2015-07-29 00:07:58
--------------------------
163504
7834
”Tối chân tâm” cũng là một bộ truyện xuyên không khiến tôi phát ngán vì độ nhàm và nhảm của nó. Truyện với dàn nhân vật phụ khá khủng, nhưng chả có tác dụng gì, đọc xong vẫn không nhớ nổi tên mấy nhân vật ấy. Nam chính không ấn tượng, chẳng nhìn ra Tứ Gia lạnh lùng ở đâu, tính khí thì dở dở ương ương, mà đã vậy, là a ca triều Thanh mà không biết cưỡi ngựa thì tôi cũng bó tay với tác giả luôn -_-. Nữ chính thì lại thuộc dạng may mắn đến mức không tin nổi, chả có gì đặc biệt nhưng hát dăm câu là được chú ý, có đứa con đáng yêu thì được sủng ái,…Đọc truyện mà buồn ngủ kinh.
1
449880
2015-03-05 08:25:36
--------------------------
132565
7834
Vừa viết review cho “Kim ốc hận”, chợt nhớ ra một quyển khác cùng thể loại và cũng lãng xẹt không kém nên bay qua luôn. Vâng, “Tối chân tâm” cũng là một truyện xuyên không mà tôi cho rằng nhạt nhẽo và ngớ ngẩn thậm tệ.
Nội dung truyện hoàn toàn không mới, cũng giống như vô vàn truyện xuyên không khác. Tình tiết thì lòng vòng, luẩn quẩn mang tính câu giờ, lắm chỗ lại chả ăn nhập gì, đọc mà hẫng lên hẫng xuống, nếu đem so với “Bộ bộ kinh tâm” (truyện có chung bối cảnh lịch sử) thì thật là đáng buồn.
Văn phong của tác giả nhẹ nhàng nhưng không đủ sức lắng, không gây được ấn tượng lại càng không đủ sức đi vào lòng người.
Nhân vật phụ trong truyện này nhiều quá, mà có để lại ấn tượng gì đâu, ví dụ như Mẫn Chỉ, Uyển Như, Dận Tự, Dận Đường,… Nữ chính thì mờ nhạt, đứa em tôi cứ khen Nhan Tử La đáng yêu chứ tôi thấy cô nàng này quá là bình thường, chủ yếu là số quá may, vì mấy cái lý do vu vơ mà được cả một đám mỹ nam yêu chết lên chết xuống. Nữ chính đã nhạt mà nam chính lẫn nam phụ còn nhạt hơn, từ Tứ Gia, Bát Gia đến mấy vị a ca khác. Tính cách nhân vật thì cứ na ná nhau, chả có gì đặc biệt.
Đọc truyện này tôi không thể không gật gù…vì quá buồn ngủ.
1
393748
2014-11-01 12:29:29
--------------------------
105362
7834
Với tôi, đây là một bộ truyện rất hóm hỉnh, hài hước, nhiều chi tiết dễ thương, cốt truyện lạ so với nhiều ngôn tình lâm li bi đát trên thị trường hiện nay. Câu chuyện xoay quanh về cuộc sống, tình yêu hạnh phúc của nhân vật chính trong thế giới mà cô bị tình cờ rơi vào, chứ không hề đề cập đến vấn đề quay lại thế giới cũ. Tác giả khéo léo gợi lại hoàn cảnh lịch sử của thời vua Ung Chính khi đó, nhưng nhẹ nhàng lược bớt những giai thoại đau lòng hay bạo lực, không phù hợp. Kết thúc mĩ mãn, tuyệt vời. Rất "mãn nhãn" người đọc vì tình yêu đẹp, nhưng không kém phần hài hước, cười đau cả bụng! Cách trình bày súc tích,  chia nhiều chương, đọc không thấy "ngán". Tôi cảm thấy rất tiếc vì truyện chưa được nhiều người biết đến.
5
138965
2014-01-28 21:17:57
--------------------------
