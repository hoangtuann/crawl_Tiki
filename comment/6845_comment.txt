234720
6845
Trong tất cả các cuốn của Fre'deric đã xuất bản tại VN thì đây là cuốn sách gây thất vọng nhất, nhưng tôi không hiểu rằng đây là một thất bại của nhà văn hay là thất bại của người dịch?

Nghệ thuật trào lộng mỉa mai giễu cợt chính là nét hấp dẫn của văn chương Frederic thì văn phong trong cuốn sách này không lột tả được, vì thế sách chỉ còn những cảnh sex và câu chuyện tâm tình khiên cưỡng giữa tay chơi và cha xứ. Sách mà chỉ có sến và sex thì lại đâm ra rẻ tiền. 
3
390673
2015-07-20 15:34:07
--------------------------
224600
6845
Các tác phẩm gần đây của Frédéric Beigbeder thật sự khiến tôi rất thất vọng, nhất là sau Kẻ ích kỉ lãng mạn tôi mong chờ một tác phẩm có thể khá hơn nhưng lại hoàn thất vọng. Câu chuyện vẫn mang nét dí dỏm nhưng không còn có ý nghĩa như các tác phẩm trước đây của tác giả nữa. Một người đàn ông ra đi tìm kiếm những phụ nữ đẹp nhất thế giới để kiếm lợi nhuận trong ngành giaỉ trí rồi phải lòng phải cô nàng ấy với đủ sự xa hoa, lãng mạn và đau khổ nhưng lại chẳng khiến tôi cảm động tí nào. Có lẽ tác giả hiện giờ cũng giống như những gì đã viết trong Kẻ ích kỉ lãng mạn, chỉ là một nhà văn nổi tiếng xoay quanh tiệc tùng và mỹ nữ chứ không còn đặt tâm hồn mình vào tác phẩm nữa.
2
129176
2015-07-08 22:35:43
--------------------------
115628
6845
Một tác phẩm Pháp khá quen thuộc với bạn đọc Nhã Nam. Frédéric Beigbeder có số lượng sách bán chạy. Nhưng với cuốn sách này, mình chỉ đọc được 1/3 cuốn thì không đọc được nữa. Nó khó đọc và khó tiếp thu quá. 
Nhân vật chính là một người đàn ông đi tìm cô gái xinh đẹp nhất thế giới với đủ thứ tiêu chuẩn trên trời dưới đất. Hành trình gian truân ấy được kể lại với vị cha xứ (!). Khá sex, khá xa hoa và lãng mạn nhưng khiến mình chóng mặt nhiều hơn là hấp dẫn. Giọng văn tác giả rất hài hước nhưng mình thấy cốt truyện không giàu tư tưởng sâu sắc. 
Mình đang mong chờ một điều khác, vượt bậc hơn nữa từ các nhà văn Pháp.
2
308416
2014-06-30 13:07:42
--------------------------
