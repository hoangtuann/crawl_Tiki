347473
10283
Cuốn sách không chỉ là một tài liệu quý báu cho những bạn nào muốn tham dự cuộc thi viết thư quốc tế UPU mà nó còn cho chúng ta thêm rất nhiều kiến thức bổ ích ,những nhận thức và hành động đúng đắn trong hành trình bảo vệ lá phổi xanh của trái đất..Sách có chất lượng giấy khá tốt,bìa có hình minh họa đẹp,giàu ý nghĩa ,cuốn hút.Nói chung cuốn sách này rất hữu ích với tất cả mọi người nhất là các bạn học và yêu thích thì nên ít nhất một lần đọc và cảm thụ nó !!!

5
543894
2015-12-04 14:45:02
--------------------------
287909
10283
Sách gồm các bài viết đạt giải trong cuộc thi viết thư với chủ đề đề cao việc bảo vệ rừng, nhằm chung tay ngăn chặn nạn tàn phá rừng, bảo vệ cuộc sống trên trái đất. Những bài viết của các em rất hồn nhiên, trong sáng, nhưng không kém phần sâu sắc và sáng tạo. Tôi nghĩ cuốn này rất thích hợp cho các em học sinh đọc, vừa nâng cao kiến thức về bảo vệ rừng, bảo vệ môi trường sống, vừa nâng cao khả năng cảm thụ và viết văn của các em. Sách rất bổ ích, giá cả hợp lý, tuy các con tôi còn nhỏ nhưng tôi đã mua… để dành đến khi bé vào vào tiểu học, trung học tôi tặng cho các bé đọc. 
4
332766
2015-09-03 09:21:41
--------------------------
