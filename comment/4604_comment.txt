413872
4604
Đã mua quyển sách này ở nhà sách cách đây khá lâu và thật sự ấn tượng với những gì mà 2 tác giả Abhijit V. Banerjee và Esther Duflo đã chia sẻ. Quyển sách là những kinh nghiệm được hai tác giả đúc kết từ những trải nghiệm thực tế và tham khảo những nghiên cứu của các đồng nghiệp thực hiện tại nhiều quốc gia trên thế giới. Hai tác giả không nghiên về trường phái nào mà có những nhận định rất sâu sắc về cái nghèo và tâm lý người nghèo. Khổ sách vừa để dễ mang theo, sách dày nhưng lại rất nhẹ nên rất dễ cầm đọc. 
4
984574
2016-04-10 14:53:25
--------------------------
264607
4604
Với tôi, có lẽ đây là một cuốn sách viết về Kinh Tế mà dễ hiểu và khơi gợi được nhiều cảm xúc khi đọc nhất. Không chỉ đơn thuần là liệt kê ra hàng loạt những vấn đề thực trạng, nói về giải pháp một cách máy móc và lý thuyết. Hai tác giả đã dẫn dắt người đọc khám phá những ngóc ngách của cuộc sống của những người nghèo, vì sao họ vẫn nghèo, và vì sao những chính sách hỗ trợ vẫn không hiệu quả; tất cả đều được thể hiện bằng một thứ văn phong bình dân, dễ hiểu. Và quan trọng nhất, đây không phải là những nhận định cá nhân tác giả, mà là kết quả của nghiên cứu khoa học thực nghiệm. Cuốn sách có thể sẽ làm thay đổi quan điểm của chúng ta về người nghèo, cảm thông hơn, và có khi sẽ có những sáng kiến thiết thực hơn để giúp họ thoát nghèo.
Đọc xong cuốn sách, tôi ước ao, những nhà lãnh đạo trong nước ta, đặc biệt những người đang công tác trong chương trình xóa đói giảm nghèo cũng được đọc… 

5
389
2015-08-13 11:55:29
--------------------------
213056
4604
Về hình thức: rất ổn, giấy màu vàng đỡ hại mắt, lại khá nhẹ nên cầm chắc tay. Ngoài ra còn có bookmark đi kèm, thuận tiên cho việc nghiền ngẫm cuốn này trong thời gian dài.
Về nội dung: đây thực sụ là gọi ý quy giá cho các nhà hoạch định chích sách đặc biệt đối với các quốc gia đang phát triển như Việt Nam. Tác giả đã giải thích trên cơ sở nghiên cứu khoa học nhiểu vấn đề mà trước đây ta vẫn lầm tưởng như liêu người nghèo khi có nhiều tiền sẽ ăn nhiều hơn, có phải thực sự những gia đình đông con sẽ chi tiêu ít cho giáo dục ?
Cuốn sách giải quyết những vấn đề hóc búa dựa trên những nghiên cứu chuẩn xác từ đó đưa ra các giải pháp khả quan hơn cho các quốc gia kém phát triển !
5
182890
2015-06-23 10:57:03
--------------------------
212788
4604
Đây là lần đầu tiên mình đọc một quyển sách về chủ đề xoay quanh người nghèo. Quả thực, quyển sách đã giúp mình mở mắt.
Trước đây cũng đã có nhiều nghiên cứu, kết luận về việc vì sao người ta nghèo; nhưng đến 2 tác giả của quyển sách, họ đã tập hợp rất nhiều dữ liệu cũng như tiến hành nghiên cứu thực tế để đưa ra những kết luận riêng (và thậm chí chứng minh được tư tưởng của những người đi trước là chưa đúng).
Mình ấn tượng nhất là phần 1 của quyển sách, mỗi chương bàn về những "bẫy nghèo" cho người nghèo như dinh dưỡng, y tế, giáo dục, kế hoạch hóa gia đình,...với rất nhiều dẫn chứng, số liệu, và những lời giải thích rất thuyết phục. Mình đã "à, ồ thì ra vậy" rất nhiều lần vì những thông tin mà quyển sách mang lại.
Quyển sách nghiêng về kinh tế học một ít (nguyên bản tiếng Anh: Poor Economics), nên có đôi chỗ cũng thách thức sự kiên nhẫn của mình. 
Mình cho 4 sao.
4
650466
2015-06-22 21:32:32
--------------------------
