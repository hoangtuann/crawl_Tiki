224583
7884
Các tình huống sách nêu ra khá phong phú và cũng là những vấn đề thường gặp của cha mẹ khi có con đi mẫu giáo. Tuy nhiên vì hoàn cảnh môi trường mầm non của Trung Quốc và Việt Nam không giống nhau, nên một số tình huống mình thấy không phù hợp. Ví dụ như quan điểm của giáo viên không phải lúc nào cũng giống nhau, điều kiện nhà trường cũng vậy. Thực tế tùy thuộc vào rất nhiều yếu tố trong mỗi trường hợp cụ thể mà cha mẹ cần ứng xử cho khéo léo. Vì vậy, cuốn sách này chỉ mang tính chất tham khảo thôi.
4
82774
2015-07-08 22:09:56
--------------------------
115725
7884
Từ khi có con, thói quen đọc sách của mình cũng thay đổi, tủ sách cá nhân mình ngày càng nhiều những quyển sách về chăm con, hiểu con, dạy con, .. Trước khi cho con đi nhà trẻ mình đã cố gắng trang bị nhiều nhất có thể về thể chất, thói quen ăn uống ngủ nghỉ, tiêm phòng, ... nhưng muôn vàn nỗi lo cứ ùa về dù mình đã cẩn thận tham khảo rất nhiều từ anh chị em, bạn bè, đồng nghiệp về ngôi trường mình cho bé nhà mình học, về cô giáo phụ trách chăm sóc bé. Có phải phụ nữ thì lo quá không nhỉ??? Mình cứ lo sao con mình đi học về "mặt buồn quá!", hay khóc không chịu đến lớp, vào lớp ít chơi với bạn, cô giáo có chăm con mình kỹ càng không, con có ăn no không, có uống đủ sữa, nước không? Làm sao mình biết con mình không bị bạn bè ăn hiếp, đánh nhau với bạn, ... Ôi! sao nhiều thứ phải lo khi xuất hiện 1 thành viên nhí trong gia đình thế nhỉ! Làm sao để có thông tin từ cô giáo mà không làm tổn thương nhau...
May mắn mình phát hiện quyển sách này ở Tiki, "em" này đã giúp mình yên tâm hơn khi gửi con bởi có vài chuyện do mình quá lo mà hù dọa chính mình thôi, hay rất đơn giản đế giao tiếp và nắm thông tin từ cô giáo mà chẳng phức tạp hay nhạy cảm như mình nghĩ.
Tuy bé của mình đã chịu chơi với bạn, chịu đi học nhưng mình nghĩ quyển sách này vẫn nên là quyển sách gối đầu giường cho những bà mẹ trẻ để khi vướng mắc có thể đọc lại và có cách giải quyết phù hợp nhất.
5
355461
2014-07-01 10:16:54
--------------------------
