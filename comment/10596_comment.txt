374171
10596
Đây là phần thứ 6 của tập sách và vẫn hết sức hấp dẫn đối với mình, dù là một bộ sách khá dài hơi, cuộc phiêu lưu của 2 chị em đã được xuất bản hàng chục tập trong tiếng Anh nhưng tiếng Việt thì mới có 6 tập này hơi bị buồn. Dù câu chuyện khá dài nhưng mỗi tập thì khá vừa dài khoảng 300 trang vừa đọc không bị mệt mỏi đủ để độc giả háo hức đón đọc tập sau, nếu bản tiếng Việt không được xuất bản tiếp chắc mình phải đọc bản tiếng Anh thôi truyện này quá hay mà.
4
306081
2016-01-25 18:26:17
--------------------------
291805
10596
Trong tập sau này, những khoảnh khắc của trận hỏa hoạn đã tái hiện lại trong trí óc của Amy, và cô bé đã day dứt khi nghĩ cái chết của bố mẹ là do lỗi của mình. Đồng thời họ cũng lờ mờ nhận ra rằng cô nàng au-pair của họ có điều gì đó thật đáng ngờ. Trải qua 6 tập truyện là những đất nước, con người, doanh nhân, nhân vật tiêu biểu khác nhau trong suốt chiều dài lịch sử, càng viết dường như tác giả càng thuần thục hơn, văn phong cũng ngày càng mượt mà hơn, nội dung cũng ngày càng lôi cuốn hơn, và lượng kiến thức chứa trong từng cuốn truyện thì càng ngày lại càng rộng lớn hơn. Tập 6 này có vẻ như có phần nổi bật hơn những tập trước khi nhiều bí mật dần được hé lộ ra. Tuy cho đến nay tập 7 vẫn chưa được phát hành nhưng hình hy vọng tập tiếp theo này sẽ được phát hành trong thời gian tới
4
39471
2015-09-06 18:50:58
--------------------------
226793
10596
Mình đọc liên tục 6 tập truyện trong 2 ngày. Và rồi phát hiện ra phần 7 vẫn chưa được  xuất bản ở Việt Nam :((( Tập 6 diễn biến khá hồi hộp. Đặc biệt là khi Amy dần nhớ ra những ký ức trong đêm diễn ra trận hỏa hoạn. Cô bé day dứt khi nghĩ cha mẹ chết là lỗi của mình và không dám kể cho Dan. Ngoài ra , hai chị em đã bắt đầu nghi ngờ Nellie - nàng au pair vì những khả năng lái máy bay và những bức email bí ẩn. Kết thúc truyện là sự hi sinh cao cả của Irina - một người mẹ đáng thương và sự thành lập liên minh một-lần-nữa giữa hai chị em Amy và Dan với người bạn già Alistair Oh. Mình đánh giá đây là tập truyện gay cấn nhất trong 6 tập vì nó đã mở ra những bí mật và cả những nghi ngờ quá ngạc nhiên. Chắc mình phải cầm từ điển mà đọc tập tiếp theo bản tiếng Anh mất. T^T
4
271828
2015-07-12 22:20:33
--------------------------
71532
10596
Đây là tập truyện mình mong chờ nhất, giải đáp hầu hết những thắc mắc bỏ lửng trong những phần trước. Army và Dan đến Úc theo sát chuyến hành trình của cha mẹ chúng, tìm lại quá khứ về cái chết của họ. Army trở thành nhân vật trung tâm, cô bé phải đối diện trước những nghi ngờ về kẻ thù ám hại cha mẹ và sự truy sát của họ.hàng. Một kẻ thù đã được loại bỏ khỏi cuộc chiến: Irina. Sự thật về con người này hầu như đã được hé lộ: 1 phụ nữ sắt đá, máu lạnh độc ác nhưng đầy lòng trắc ẩn thầm kín, chính bà hi sinh để cứu 2 chị em, quyển sách dậy lên trong lòng người đọc sự thương cảm của con người và lòng dũng cảm của 2 đứa trẻ. Nhiều tình huống hấp dẫn và cách chuyển cảnh khéo léo. Tập truyện mở ra nhiều nghi vấn: Nellie là ai và có thực sự đang giúp chúng ko? Hy vọng phần 7 sẽ tiếp tục lôi cuốn như vậy
4
91158
2013-04-27 15:55:47
--------------------------
29649
10596
Khác với bộ seri Percy Jackson, 39 Manh mối là sự phối hợp nhiều tay viết khác nhau trong 1 câu chuyện với nội dung thống nhất. Sự phối hợp này góp phần làm thay đổi góc nhìn về câu chuyện qua mỗi tập, tạo sự hứng khởi cũng như tò mò cho người đọc. 
Tập 6 có mở đầu khá phức tạp so với 6 tập trước, tuy nhiên câu chuyện vẫn giữ được nét cuốn hút riêng do lối viết tường thuật hành động và dàn nhân vật đông đảo. 
Tiếp theo tâp 5 diễn biến hơi đơn điệu, tập 6 có thể nói đã giúp người đọc tìm lại cảm giác hồi hộp theo từng hành động của nhân vật, dù sự kéo dài tình tiết có phần hơi gương gạo ở khoản giữa sách khiến người đọc cảm thấy hụt hẫng.
Tuy nhiên, đây vẫn là một quyển sách đáng đọc trong hè 2012 này.
2
41922
2012-06-07 16:37:03
--------------------------
