373647
9998
Một cuốn sách vô cùng chất lượng cũng như cả trong lẫn ngoài. Mình thì không giỏi tiếng anh lắn, nhưng khi mua cuốn này về thì 360 động từ được chia một cách rõ ràng, không rắc rối kèm theo hình ảnh minh họa nên dễ học, khả năng tiếng anh được nâng cao hơn. 
Cuốn sách giống như một cuốn sổ nên tiện mang theo. Về hình thức thì giấy rất đẹp, tốt, font chữ rõ ràng dễ đọc. 
Nói chung mình rất hài lòng khi mua cuốn sách này về nha. Cực đẹp mà không hề tốn nhiều tiền.

5
257278
2016-01-24 11:04:21
--------------------------
317647
9998
Khác xa với tưởng tượng, khi nhìn hình mình nghĩ chắc cuốn sách này phải to to như bao cuốn từ điển khác nhưng không ngờ khi mình mở hộp hàng ra thì đập vào mắt mình là một cuốn sách nhỏ, gọn, có bao bìa đẹp, bắt mắt, giấy dày, cứng. Cuốn sách đem đến những kiến thức về Tiếng Anh như Động từ Bất qui tắc, 12 thì cơ bản giúp cho người học Tiếng Anh học tập dễ dàng và tiện lợi. Đặc biệt là giá thành cực kỳ rẻ, hợp với túi tiền bọn học sinh chúng em. Nhưng hình như là hơi ít từ ví dụ như phần A chỉ có 4 từ, phần N chỉ có 2 từ hay phần V chỉ vỏn vẹn 1 từ,... nên cũng có một số từ không có trong sách. Nhưng nói tóm lại là em rất thích cuốn sách này.
4
570715
2015-10-03 22:20:34
--------------------------
298697
9998
Cuốn sách này khi nhìn qua mình tưởng là khổ phải như cuốn sổ tay đẹp nhưng khi mua thì lại nhỏ gọn thực sự rất tiện khi đút túi mang đi. Cuốn sách nhìn khá đẹp, giá cả cũng phù hợp với các bạn học sinh mà lại đem được đến những kiến thức cơ bản nhất mà mội người cần biết khi học tiếng Anh. Phân làm hai phần và cuốn sách cung cấp cho người đọc những mảng khác nhau. Mình thích giấy của cuốn sách, rất sáng và phù hợp với mắt của học sinh. Cuốn sách là một cuốn sách rất chất lượng và có ích.
4
419803
2015-09-12 20:33:36
--------------------------
260999
9998
Mình phải nói là mua quyển này chất lượng ngoài mong đợi luôn. Mình không nghỉ chỉ với giá 14k mà lại có một quyển sách học tiếng Anh tốt thế này. 360 động từ bất qui tắc được trình bày rõ ràng, có ví dụ minh hoạn, hình vẽ...rất dễ học dễ nhớ. 12 thì được trình bày ngắn gọn ở cả dạng chủ động và bị động. Quyển sách nhỏ gọn, dễ mang theo. Chất lượng giấy tốt, chữ rõ ràng dễ đọc...Nói chung là quá truyệt vời. Và hơi thất vọng là tại sao mình không biết đến quyển này sớm hơn >< !!!!! 
5
400930
2015-08-10 20:18:59
--------------------------
