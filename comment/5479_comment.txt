307647
5479
Con trai tôi sau khi hoàn thành tô nét cuốn này, thì giờ con đã viết thành thạo các chữ số từ 0 đến 10 và còn làm toán rất tốt do con đã viết và đếm, đọc thành thạo. 
Lúc trước chưa có cuốn vở tập tô số, dạy con viết số còn phức tạp do mẹ cứ cầm tay con viết theo nét của mẹ, nhưng từ khi có cuốn vở này, con tô theo nét đã được in sẵn, theo trình tự trái phải, trên dưới mà mẹ đã dạy. 
Những cuốn sách vở được giới thiệu ở TIKI tôi tin là đã được lựa chon sơ tuyển trước khi người mua là chúng tôi lựa chọn lần nữa. Tôi sẽ mua thêm nhiều loại sách giáo dục khác cho con mình học ở trên Tiki. 
5
572177
2015-09-18 10:53:46
--------------------------
