476495
9558
Mình vừa mua thành công sản phẩm cuốn sách Lịch sử Việt Nam bằng tranh tập truyện Xây đắp nhà Lý. Hồi còn đi học, mình sợ môn lịch sử kinh khủng vì quá dài. Từ ngày mình biết bộ truyện này, mình yêu lịch sử hơn. Đọc các tập, mình hiểu hơn về Việt Nam. Ở tập này, mình có cơ hội học được thêm kiến thức về nhà Lý. Nhà Lý rất phức tạp do qua nhiều đời vua. Sự hình thành nhà Lý thật gian nan, bão táp. Vì vậy,mình hiểu hơn về vua nhà Lý. Mình cho 4 sao.
3
1468522
2016-07-20 21:34:57
--------------------------
162619
9558
Bộ sách Lịch sử Việt Nam bằng tranh giới thiệu những tiến trình lịch sử, nhân vật lịch sử một cách ngắn gọn, chuẩn xác, rất dễ tiếp thu. Phần tranh minh hoạ được đầu tư giúp người đọc dễ dàng hình dung các khung cảnh lịch sử, đặc biệt là đối tượng độc giả trẻ tuổi. Lúc bé tôi thường mê mẫn vùi đầu vào những quyển sách này, nhờ vậy mà kiến thức lịch sử cũng tích góp được kha khá. Đến nay đọc lại vẫn thấy hay, thấy thích, vừa ôn lại kiến thức cũ, vừa như gặp gỡ lại một phần tuổi thơ của mình.
4
515736
2015-03-02 20:56:51
--------------------------
