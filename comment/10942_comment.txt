474670
10942
Tôi đọc tập 1 "Mắt rồng" cũng đã hơn 7 năm, và giờ khi đã lớn, tôi mới được cầm trên tay tập 2 "Quái vật xứ khác". Lời văn rất dễ thương, hóm hỉnh, cách tưởng tượng ra một thế giới phép thuật của những người Lắm Phép cũng rất ngộ nghĩnh chứ không như các tác phẩm khác. Nút thắt của câu chuyện được giải quyết rất tinh tế, phù hợp với văn phong dành cho trẻ con, rất nhẹ nhàng, dễ hiểu, và cũng thật đáng yêu. Khi bộ truyện được dịch hoàn chỉnh tại Việt Nam, chắc lúc ấy tôi cũng rời xa sân ga tuổi thơ cũng khá lâu, tôi vẫn sẽ tiếp tục đọc nó, như để đi tiếp một chặng đường dài của bộ truyện này trên đất nước mình, một bộ truyện kỳ ảo rất dễ thương dành cho thiếu nhi.
5
307132
2016-07-11 21:27:51
--------------------------
459100
10942
Đối với mình thì truyện rất hay , hấp dẫn . Các tình huống , diễn biến li kì , không thể đoán trước được . Chính những cuộc thi kì quái mà Erec phải vượt qua cùng với tình bạn , đoàn kết với nhau đã làm nên những nét hấp dẫn cho truyện . Mình rất thích và mong chờ các tập tiếp theo , tuy nhiên , ở Việt Nam chỉ mới dịch có 2 quyển và mình đã đợi lâu rồi mà vẫn chưa ra quyển tiếp theo . Đây chính là một trong những tác phẩm ưa thích nhất của mình . 
5
379591
2016-06-25 20:00:33
--------------------------
411712
10942
Phần hai của bộ truyện có vẻ chặt chẽ hơn, nội dung vừa phải, hợp lý. Song đến cuối thì tình tiết gay cấn có phần quá dồn dập, nhưng các tình huống khó khăn lại được giải quyết quá đơn giản, gọn lẹ; nhất là ở cuộc sát hạch thử hai của Erec Rex, cao trào cuộc chiến đang được đẩy lên cao thì Erex nghĩ ra Cuộn Giấy Sự Thật, và thế là giải quyết xong luôn. Các diễn biến sau cũng thế. Trong khi phần cái lỗ hổng hư vô thì được miêu tả khá chi tiết. Về phần thông điệp thì tác giả truyền đạt cũng khá tốt, ở tập này Erec Rex đã phải đối mặt với rất nhiều khó khăn, đặc biệt là đấu tranh với chính ham muốn của bản thân cũng như học cách nhận thức về lẽ phải; ngoài ra truyện cũng thể hiện ý rằng không phải cứ mang vẻ ngoài xấu xí thì bản chất cũng xấu, và ngược lại, sự thật không phải lúc nào cũng như người ta tưởng. Điểm này tôi đã nắm rõ dụng ý tác giả ngay từ đầu, nên với tôi không có gì bất ngờ cả. Cuốn sách thiếu niên này có chỗ hay và có chỗ chưa hay, dù vậy tôi vẫn ưu ái cho 4 sao.
4
386342
2016-04-06 14:56:18
--------------------------
258086
10942
Khi biết cuốn Erec Red có tập 2 mình đã đặt mua. Mình thật sự buồn vì cuốn 2 không hay bằng cuốn 1. Câu chuyện bắt đầu lằn nhằn khó hỉu, rồi tình huống chưa được gay cấn cho lắm. Vì đây là truyện kì ảo nên mình cũng thấy thích ráng coi cho hết. Mình nghe nói bộ này có trọn bộ 5 tập còn ở Việt Nam mới dịch có 2 tập thôi nên các bạn cân nhắc kỉ lưỡng trước khi mua. Bộ này cũng mắc tiền nên suy nghĩ cho kĩ rồi hẳn mua nhé.
4
429585
2015-08-08 10:46:54
--------------------------
139582
10942
Câu chuyện kể về hành trình của cậu bé có một liên hệ mật thiết với loài rồng đến Xứ Khác! Ở đấy, cậu gặp những chiến binh đẹp, và rất đẹp!!! Xuyên suốt cuộc hành trình đã cho cậu bài học quý giá!
Dịch giả đã làm tốt hết khả năng của mình, dịch với một giọng văn sát với lời gốc nhất, truyền tải được gần hết ý của tác giả
Tình tiết vô cùng hấp dẫn, nhưng không đủ lôi cuốn người đọc cầm nó trên tay không rời và chăm chú đọc hàng giờ liền như quyển đầu của bộ truyện là Mắt Rồng!
Mọi sự việc xảy ra hoàn toàn khác với ý bạn nghĩ!
Đến kết cuộc, một cái kết mà tôi chắc rằng 99,9% người đọc đều có thể suy ra được nhưng vẫn mong mỏi chờ quyển tiếp theo của bộ truyện để đọc nốt!
btw, bộ truyện này tác giả định diếm luôn hay sao mà cả 2 trời rồi vẫn chưa ra quyển tiếp
4
503349
2014-12-07 21:32:41
--------------------------
