430025
4112
Mình đã đặt mua cuốn sách này cho đứa cháu đang học lớp 5 để ôn tập từ vựng và luyện phát âm qua lời giới thiệu sách của đứa bạn. Nghe bạn nói sách diễn đạt đơn giản, có kèm CD nên cũng rất tiện cho việc luyện phát âm, rất phù hợp với những người mới học tiếng anh, thấy hay hay nên đặt mua về cho cháu. Sau một thời gian dùng thử, cháu mình rất thích, cháu nói sách phiên âm rất rõ ràng, đơn giản, dễ hiểu, dễ đọc lại có kèm CD rất ổn, nghe rất rõ, luyện phát âm tốt. Nói chung rất ưng ý khi đã đặt mua.
4
929376
2016-05-14 12:12:27
--------------------------
425423
4112
Chất lượng giấy tốt, chữ dễ đọc, các phần được chia rõ ràng giúp người học dễ học hơn.Sách bao gồm các mẫu câu, cụm từ trong giao tiếp cơ bản. Tất cả đều có phiên âm bằng tiếng việt cho nên rất dễ dàng cho những người mới học tiếng anh. Phát âm trong đĩa CD kèm theo cũng rất dễ nghe. Sách tạo cảm giác hứng thú hơn trong việc học tiếng anh. Việc học từ vựng cũng dễ dàng hơn nhiều. Tự học nghe nói tiếng anh căn bản là cuốn rất tốt cho những người bắt đầu, những người tự học tiếng anh.
4
1232633
2016-05-04 17:40:26
--------------------------
422834
4112
Tự Học Nghe - Nói Tiếng Anh Căn Bản (Kèm CD) là cuốn sách khá hay dành cho những bạn tự học và chưa biết nhiều về anh văn giao tiếp. Cuốn sách chỉ phù hợp cho những bạn nào giao tiếp còn yếu và chưa biết cách đọc một số từ căn bản thôi. Điểm trừ ở cuốn sách này ở chỗ là nó ghi ra cả phiên âm tiếng việt, cách học này về lâu dài dễ bị đọc sai lắm nên mình không thích cho lắm. Dù sao cũng cám ơn tác giả của cuốn sách này. Hy vọng sau khi học hết quyển sách này vốn tiếng anh của mình sẽ tiến bộ hơn.
3
1273667
2016-04-28 10:19:05
--------------------------
410917
4112
Tự Học Nghe - Nói Tiếng Anh Căn Bản (Kèm CD) là cuốn sách khá hay dành cho những bạn tự học và vốn kiến thức tiếng anh chưa nhiều, nếu như bạn giao tiếp tiếng anh khá và biết cách đọc phiên âm thì không nên dầu tư vào cuốn sách này. Cuốn sách chỉ dành cho những bạn nào giao tiếp còn yếu và chưa biết cách đọc một số từ căn bản thôi. Mình khongo thích cuốn sách này ở chỗ là nó ghi ra cả phiên âm tiếng việt, về lâu dài dễ bị đọc sai lắm nên mình vẫn thích các cuốn ghi theo phiên âm quốc tế luôn
3
653217
2016-04-05 03:08:54
--------------------------
338908
4112
Mình tìm hiểu những cuốn sách giúp tự học tiếng anh và thấy cuốn sách này.Đọc thử thấy khá hay và tin tưởng vào MC book nên đã quyết định mua qua tiki.mình đã nhận được sách nhanh chóng va thấy rất hài lòng.sách rất rõ ràng dễ hiểu lại có kèm cả CD nên rất thuận tiện để học.hy vọng mình sẽ cải thiện được trình độ tiếng anh thật nhiều nhờ cuốn sách này.cảm ơn đội ngũ tác giả MC Book và tiki thật nhiều.Mong được gắn bó cùng cả 2 nhiều hơn nữa.chúc tiki và MC book ngày càng phát triển hơn nữa.
5
940357
2015-11-17 06:04:16
--------------------------
338907
4112
Mình tìm hiểu những cuốn sách giúp tự học tiếng anh và thấy cuốn sách này.Đọc thử thấy khá hay và tin tưởng vào MC book nên đã quyết định mua qua tiki.mình đã nhận được sách nhanh chóng va thấy rất hài lòng.sách rất rõ ràng dễ hiểu lại có kèm cả CD nên rất thuận tiện để học.hy vọng mình sẽ cải thiện được trình độ tiếng anh thật nhiều nhờ cuốn sách này.cảm ơn đội ngũ tác giả MC Book và tiki thật nhiều.Mong được gắn bó cùng cả 2 nhiều hơn nữa.chúc tiki và MC book ngày càng phát triển hơn nữa.
5
940357
2015-11-17 06:04:05
--------------------------
298764
4112
Hiện giờ mình đang cần tìm đọc những cuốn sách tự học tiếng anh vì mình sắp gần đi định cư.Mình đã tìm hiểu và cảm thấy cuốn sách này phù hợp với mình và rồi đã đặt trên tiki.vn.Khi nhận được sách mình rất ưng ý.Từ bìa sách cho tới giấy sách được in bằng màu.Đặc biệt hơn nữa là cách trình bày rất logic,dễ hiểu,rõ ràng.Mình nghĩ nó rất phù hợp với tất cả những ai đang cần học anh văn như mình.May mắn hơn với cách trình bày như vậy mình có thể dễ dàng giúp ba mẹ mình cùng học tiếng anh.Cảm ơn tác giả cuốn sách này và đồng thời cũng cảm ơn tiki.vn.
5
83467
2015-09-12 21:03:13
--------------------------
282826
4112
Mình vừa nhận được sách thì đã vội mở ra xem và nghe cả phần CD nữa. Sách giúp cho mình phát âm tiếng anh chuẩn hơn, làm cho mình có hứng thú với mỗi tiết học tiếng anh trên lớp, với lại sách được xuất bản từ công ty cổ phần sách MC Books nên mình rất an tâm về nội dung cũng như chất lượng của sách :)))  cầm sách trên tay mình cảm thấy rất đáng đồng tiền, giấy tốt, chữ rõ, chia ra từng phần rõ ràng. Tóm lại mình rất thích quyển sách này, đối với mình nó thực sự bổ ích ^^
5
714041
2015-08-29 14:23:46
--------------------------
