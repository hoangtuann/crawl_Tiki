411594
10832
Mình mua cuốn sách này khá lâu trước đây, tưởng đã viết nhận xét cho tác phẩm này nhưng hôm nay vào xem thử thì chưa hề viết. Thật lạ lùng.
Dù sao thì, khi mình còn rất nhỏ, người lớn và những bộ phim phát sóng khi ấy nếu có nhắc đến tuổi trẻ, tuổi thanh xuân, tuổi xuân thì đều nói rằng đó là một tuổi đẹp nhất đời người, ta nên gìn giữ và trân trọng nó lâu nhất chúng ta có thể. Đặc biệt đối với con gái vì thanh xuân chỉ có. Chúng ta viễn tưởng cho tuổi đó những mong ước đẹp nhất, những giấc mơ, khát khao, nhiệt huyết...Thế nhưng, tác giả Xuân Thụ với tác phẩm này chỉ viết lại sự thật mà không hề màu hồng như chúng ta nghĩ. Một sự thật trần trụi đến đau lòng. Nhưng sự thật nào cũng đau lòng, vì đó đơn giản là sự thật.
5
383240
2016-04-06 11:25:29
--------------------------
410033
10832
Xuân Thụ không trưởng thành hơn gì mấy so với Búp bê Bắc Kinh. Nhân vật Xuân Vô Lực cũng y như trước, sống rất bạt mạng. Có lẽ mẫu người như cô ấy không có gì xấu, ít ra không có trộm cướp hay lừa tình, chỉ là sống cho cảm giác của bản thân mà thôi. Nhưng quan điểm của mình thì không chấp nhận kiểu trụy lạc như vậy: ai cũng lên giường được, dù là mới quen biết. Cách viết của tác giả cũng như tính nhân vật, không chọn lọc, thích gì viết đó, kể chuyện vì muốn kể, không cần biết nó có ý nghĩa gì không. Chỉ có một số câu, thể hiện suy nghĩ của cô ấy, thì đúng là đáng học hỏi và suy ngẫm lắm. 
3
535536
2016-04-03 13:38:33
--------------------------
386262
10832
Phải trải qua cái giai đoạn điên rồ, sẵn sàng đâm vào bất cứ thứ gì bản thân thấy thích hoặc là ham muốn. Bất đồng, cuồng nhiệt trong tình yêu có phải là thứ mà tuổi trẻ rong ruổi tìm kiếm. Những câu chuyện trong tác phẩm này kể về những con người sẵn sàng làm mọi thứ ngốc nghếch chỉ vì họ muốn làm như thế. Nếu không như thế, có lẽ họ đã không trưởng thành. Tuổi trẻ, ít hay nhiều đều nên có một sự ngông cuồng nào đó để khi ta già vẫn còn thứ để ta nhớ nhung.
Đó chỉ là ý kiến, quan điểm riêng của tôi. Mỗi người mỗi suy nghĩ, mỗi sự lựa chọn và tôi chọn cuốn sách này.
5
95468
2016-02-25 13:12:44
--------------------------
360634
10832
Nói về tuổi trẻ của mỗi con người luôn đi qua với ngập tràn niềm vui , mang lại tiếng cười mới mẻ về điều đặc biệt nào đó , với câu truyện vẫn là hơi hướng đưa người ta quay trở về thời đại của chiến đấu không ngừng cho tương lai , mục đích cao thượng đáng trân trọng làm không thể nào quên đi với cả thời gian sau này , nếu truyện làm ta ôn lại kỷ niệm thì vẫn cần đến lời văn hết đỗi bình dị và trẻ trung theo quãng đường dài .
3
402468
2015-12-29 15:29:21
--------------------------
