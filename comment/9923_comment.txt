454739
9923
“Cô bé lọ lem và nàng công chúa ngủ trong rừng” phiên bản Happy reader 350 từ gồm hai câu chuyện cổ tích mình đã được đọc từ lâu bằng tiếng Việt, nay đọc phiên bản tiếng Anh mình thấy có nhiều cảm xúc đặc biệt; đặc biệt vì mình có thể vừa học tiếng Anh vừa thưởng thức những câu chuyện đã đi vào thế giới trẻ thơ nhiều thế hệ. Mình thường vừa đọc sách vừa nghe đĩa CD để luyện cách đọc – cách nhấn chữ - nhả chữ - học ngữ điệu lên xuống một cách tự nhiên và cách nhấn trọng âm trong một đoạn văn.
5
301718
2016-06-22 08:49:54
--------------------------
403689
9923
Tôi đã mua “ Happy Reader - Cinder & Công Chúa Ngủ Trong Rừng “ trên tiki về cho con gái của tôi để vừa tập dịch sách tiếng anh và nghe tiếng anh vào buổi tối trước khi đi ngủ. Bé nhà tôi rất thích thú với “Happy Reader - Cinder & Công Chúa Ngủ Trong Rừng “. Cuốn sách này đã giúp con gái của tôi được nâng cao trình độ nghe và đọc hiểu tiếng Anh. Giọng đọc trong CD chuẩn, lại có âm nhạc nhẹ nhàng phù hợp với ngữ cảnh nên bé nhà tôi, tối nào cũng thích nghe. Với những từ mới có giải thích ở phần cuối mỗi trang sách đã giúp con tôi dễ dàng đọc hiểu các đoạn văn trong sách. Đây là cuốn phụ huynh nên mua cho con của mình học.

5
730635
2016-03-24 07:34:35
--------------------------
348756
9923
Mình rất thích rất đam mê “tip” chuyện 350 word. Và cuốn Happy Reader - Cinder và Công Chúa Ngủ Trong Rừng cũng không nằm ngoài lệ đó bởi câu chuyện thật hay thật ý nghĩa, dù đọc đi đọc lại nhiều lần vẫn không cảm thấy ngán. Bìa sách thì bắt mắt, dễ thương. Bên trong cũng thật sự bắt mắt, sinh động từ những hình ảnh hoạt hình. Ngoài sách thì khi mua còn được tặng kèm 1 CD chất lượng. Về phía bán hàng – công ty Tiki thì giao hàng khá nhanh, đóng gói khá cẩn thận. 
5
702247
2015-12-06 18:51:42
--------------------------
336091
9923
Mình siêu siêu thích bộ sách này của first news. Vừa có phần truyện kể bằng tiếng anh, liệt kê các từ mới + nghĩa tiếng việt ở mỗi trang, cuối trang lại có những câu quan trọng và cả câu hỏi nho nhỏ nữa. Nói chung là rất bổ ích trong việc trau dồi tiếng anh. Giọng đọc trong CD rất truyền cảm, chuẩn, nghe mãi ko chán như kiểu được mẹ kể chuyện cho mỗi đêm giống trong phim :)))) Bạn nào có trình độ tiengs anh sứt mẻ như mình thì đọc từ quyển 350 từ trước nhá, ko khó đâu.
5
35740
2015-11-11 21:46:36
--------------------------
200802
9923
Đây là quyển thứ hai trong bộ Happy Reader của First News. Phải nói là mỗi câu chuyện trong bộ này đều hay và thú vị. Tuy những câu chuyện này được rất lại một cách ngắn gọn bằng những từ ngữ đơn giản nhưng vẫn truyền tải được nội dung của câu chuyện. Sách kèm với CD mang lại rất nhiều lợi ích và tiện lợi cho tôi. Tôi vừa có thể đọc truyện, học từ mới vừa nghe được cách phát âm cũng như luyện cách phát âm sao cho đúng cho chuẩn. Một cách học tiếng Anh vừa học cũng như vừa chơi. Vậy mà mang lại hiệu quả cao lắm đó.
5
321923
2015-05-25 22:42:33
--------------------------
73468
9923
Mình nghĩ rằng cuốn sách này rất thú vị và bổ ích cho những ai muốn học tiếng Anh. Trước tiên là việc học từ mới, sau những đoạn văn bằng tiếng anh thì bạn sẽ được nhận ngay nghĩa của những từ mới và cụm từ ở cuối trang. Hơn nữa ngoài việc học từ mới, sách còn tặng kèm đĩa CD giúp bạn học nghe và có ích trong cả cách phát âm nếu bạn đọc theo đĩa. Giọng đọc trong đĩa rất dễ nghe và cảm xúc, rõ ràng. Đĩa được chia thành nhiều Chapter nhỏ giúp bạn rất dễ học. Nhưng điều làm tôi thấy thú vị hơn cả là việc học tiếng Anh trong qua các câu truyện cổ tích quen thuộc mà tôi nghĩ rằng ai trong chúng ta cũng thuộc, vậy nên rất dễ để đoán từ. Hình minh họa trong truyện cũng rất đẹp. Tùy từng mức độ còn có thể chọn từ 350 đến 1000 từ. Tôi nghĩ rằng đây là một cuốn truyện rất hay dành cho mọi lứa tuổi muốn học tiếng Anh.
5
99442
2013-05-08 17:27:32
--------------------------
