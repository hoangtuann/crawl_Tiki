278801
9158
Tác phẩm kể về cuộc đời bị phủ sau bóng tối của các bạn trẻ khiếm thị bởi nhiều nguyên nhân khác nhau ( bẩm sinh, bệnh về mắt hay do hậu quả chiến tranh), cùng sống dưới một mái nhà là ngôi trường dành cho trẻ khuyết tật. Tuy mất đi ánh sáng, nhưng mỗi bạn đều là một tấm gương về sự lạc quan, tinh thần hiếu học, biết yêu thương bảo bọc lẫn nhau. Các bạn cũng thích ca hát, thích vận động, muốn được giao lưu trò chuyện với mọi người. Đọc để cảm thấy chúng ta thật may mắn vì còn có thể nhìn ngắm được vạn vật, đọc để yêu nhiều hơn cuộc sống tươi đẹp xung quanh mình!
4
109583
2015-08-26 10:38:02
--------------------------
