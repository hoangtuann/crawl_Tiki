387277
9274
"Doraemon bóng chày" – tác giả Mugiwara Shintaro và Fujio Fujiko là một bộ truyện có nội dung nói về môn thể thao khá phổ biến, thường được chơi ở Nhật Bản – môn bóng chày. Bộ truyện tranh này nói về các trận đấu rất gay cấn và ly kỳ ủa đội Doras do chú mèo máy Kuroemon làm đội trưởng. Đội bóng Doras là một đội có tinh thần đoàn kết rất cao, là hiện thân của tình bạn cao đẹp. Truyện còn có những nhân vật phụ như Shiroemon,... Đây là một bộ truyện tranh mình rất thích. =)))))
5
1142307
2016-02-26 21:15:07
--------------------------
254481
9274
Trận chiến giữa " senpai " và " đàn em " đã diễn ra với lợi thế lúc đầu nghiêng về đội Great Doras . Để được trở nên mạnh như vậy , họ đã phải lao vào điên cuồng luyện tập với tinh thần thi đầu " chỉ cần thắng là được " và quên mất cái gọi là niềm vui khi chơi bóng chày . Đội Doras cũng đã như vậy nhưng nhờ sự trở lại của Hyoro , tinh thần đã vực dậy và họ đã tìm được cảm giác ngày xưa : không cần quan trọng thắng thua , chỉ chơi bóng chày vì niềm vui . Đọc tập này mình không thể dừng ngón tay lật sách , hấp dẫn không tả được .
5
617834
2015-08-05 11:28:42
--------------------------
229122
9274
Xem tập này mà tức ông huấn luyện viên trời đánh của đội Great Doras (vốn dĩ chỉ là đội dự bị Doras 2) bất chấp mọi thủ đoạn và toàn chơi xấu để giành chiến thắng mà thôi. Nhưng dĩ nhiên đã là dự bị rồi thì làm sao bằng đội Doras chính thức được. Trong trận đấu trước Hyoroemon vì lí do cá nhân nên đã vắng mặt và nhò thành viên cũ là Guriemon chơi thay vị trí của mình, nhưng rốt cuộc trong trận đấu quái gở này Hyoroemon - thủ lĩnh tinh thần của đội Doras đã trở lại và sát cánh cùng các đồng đội để mở màn cho chiến thắng ngoạn mục. Tập này hấp dẫn hơn nữa khi cả đối thủ Monta cũng ra tay giúp sức cho đội của Kuroemon có thể giành chiến thắng để họ đối mặt nhau trong trận chung kết. Càng đọc càng gây cấn không thể dừng được
4
471112
2015-07-16 14:55:40
--------------------------
