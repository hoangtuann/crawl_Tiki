496544
3858
Hạnh phúc đôi khi thật giản dị như chính trong những sinh hoạt hàng ngày thôi, nhưng chúng ta nhiều khi vẫn mong ước thứ gì đó lớn lao hơn, xa vời hơn, để rồi khi mất những tình cảm gia đình đơn giản thường ngày ta mới hiểu điều gì là quan trọng là đáng giá trong cuộc đời. Mình rất thích cuốn sách này, quả ko hổ danh là 1 giọng văn Pháp luôn mang sắc màu lãng mạn, hóm hỉnh mà ẩn chứa nhiều ý nghĩa thâm sâu. Truyện cũng một phần cho ta thấy mặt tham lam, lạnh lùng và vô tình của 1 người đàn ông mờ mắt trước đồng tiền mà quên đi tấm lòng chân thành của người vợ. Nhưng kết cục người vợ đã có thể giải thoát cho mình khỏi con người xấu xa đó mà tìm được bến bờ mới bù đắp cho trái tim mình.
5
249745
2016-12-19 12:16:10
--------------------------
477778
3858
Điểm cộng đầu tiên là mình thấy bìa sách đẹp và tựa sách cũng hay, gợi nhiều suy nghĩ sau khi đọc xong. Nội dung ổn và thực tế, kể về một cô nàng có cuộc sống vui vẻ bên gia đình như bao cô gái bình thường khác cho đến khi cô trúng giải độc đắc. Cách xây dựng tâm lí nhân vật chính khá gần gũi. Mình thích nhất là các thứ mà cô nữ chính liệt kê muốn làm và muốn mua sau khi trúng số. Nó tạo cảm giác rất thật. Tóm lại đây là một cuốn sách đáng để đọc và ngẫm
4
561291
2016-07-28 20:42:42
--------------------------
475350
3858
Mình đã đọc xong cuốn sách này trong vòng 3 đêm trước khi đi ngủ,và đêm nào mình cũng khóc...
Lời tâm tư cuối tác phẩm của chính tác giả thậm chí còn tuyệt vời hơn, với ước mơ lay động con tim và truyền cảm hứng thay đổi số mệnh bằng câu chữ,ông có một niềm tin vô cùng đẹp rằng một câu văn cũng có thể thay đổi thế giới, mình không biết thế giới của các bạn đã đổi thay hay chưa,nhưng tác phẩm này đã thực sự dạy mình trở thành con người tốt hơn :)
5
351245
2016-07-14 09:18:30
--------------------------
455073
3858
Chỉ là mơ ước thôi đề cập đến một đề tài thường thấy trong cuộc sống hiện nay, đó chính là sự đấu tranh gay gắt giữa tiền tài và số phận. Cách tác giả Grégoire Delacourt miêu tả nội tâm của nhân vật Jo rất gần gũi, chân thực và cuốn hút. Các tình tiết trong câu chuyện hợp lí, đôi khi có hơi nhanh, gây khó hiểu nhưng nhìn chung vẫn tạm ổn. Về vấn đề dịch thuật vẫn chưa được ổn lắm, đôi chỗ hơi lủng củng, bìa sách thiết kế không mấy nổi bật, giấy sách tạm ổn.
4
182813
2016-06-22 12:57:26
--------------------------
437836
3858
Một cuốn sách khá hay với bìa sách đẹp. Đó là câu chuyện về cuộc sống của một nhân vật nữ với những ước mơ của mình và sự biến đổi cuộc sống của cô khi cô nhận được số tiền lớn, nhưng lại cảm thấy bất an...... Qua đó, ta thấy được ý nghĩa của câu chuyện, tiền có thể làm xa rồi những thứ mà ta tưởng đã gắn bó rất lâu, khi mất đi rồi ta mới biết nó quan trọng với mình đến chừng nào..."Chỉ là ước mơ thôi" là một bài học rằng hãy trân trọng những gì bạn tạo dựng nên, bởi trong đó có tấm lòng, có tình yêu, có giá trị vĩnh cửu... mang lại cho bạn hạnh phúc giản dị mà bền vững theo thời gian!
4
1042981
2016-05-28 22:27:45
--------------------------
423270
3858
Một câu chuyện bình dị mang đến nhiều suy nghĩ miên man. 47 tuổi, cô ấy vẫn không ngừng mơ ước về những vật dụng trong nhà, những chuyến du lịch cùng chồng. Cuộc sống không có gì đặc biệt và cứ thế yên ả trôi qua. Nhưng đâu đó, cô ấy vẫn muốn có thứ gì đó thay đổi, nhiều ước muốn vẫn cứ hiện lên trong suy nghĩ. Rồi đùng một cái, 18 triệu ơ-rô đến khi cô không hề mong muốn. Có nhiều thay đổi tích cực đến cùng 18 triệu đó, đến nỗi cô ấy cảm thấy bất an, đâu là thật đâu là giả? Là một sự miêu tả tâm lí chân thực đến từng cảm xúc để cuối cùng nhận ra, đâu phải tiền là tất cả. Đôi khi, tiền lại là thứ đẩy những yêu thương gắn kết chệch mối nối và rồi rời nhau mãi mãi.
Nó là một cuôn sách hay, hãy xem một người phụ nữ trung niên làm gì khi những niềm tin bị đạp đổ bởi những điều phù phiếm. Đọc và cảm nhận nhé mọi người!

5
504952
2016-04-28 22:40:13
--------------------------
406657
3858
Có những khi đọc xong một tác phẩm hay, ta không bíêt phải thốt lên lời nào cho trọn vẹn, tròn đầy nhất. Cảm xúc thật khó diễn tả, lời lẽ ngắn gọn, dễ hiểu, tình tiết dẫn dắt bất ngờ khéo léo, văn phong mượt mà, gợi hình gợi cảm xúc. Có những giấc mơ dang dở, những ứơc mơ thời xuân son sắc mà ta hằng ao ứơc chạm tới nhưng chẳng thể nào thực hiện. Đọc xong tác phẩm, tôi cũng tự hỏi mình, nếu sở hữu 1 số tiền lớn trong tay minh sẽ làm gì? Và có hàng  trăm sự lựa chọn. Nhân vật chính chơi vơi chông chênh trong những gíâc mơ, níu giữ hạnh phúc từ những điều bình thường giản dị để rồi bị chính người mình yêu thương làm mọi thứ vỡ vụn. Mình đọc đựơc một câu đâu đó đại lọai là: "Nếu muốn hạnh phúc vơí tiền bạc, trứơc tiên phải bíêt hạnh phúc khi không có tiền". Sẽ có những thứ mà tiền không mua đựơc, đừng để tham muốn giết dần giết mòn những thứ đáng giá "tầm thường" mà vô giá. Gấp sách lại mà cảm xúc vẫn xuyến xao, sách quý dạy cho ta những bài học hay.
5
857944
2016-03-28 18:21:54
--------------------------
392949
3858
Truyện mang nét giản dị của cuộc sống đời thường những biến cố cuộc đời của cuộc dời người phụ nữ được tác gói gọn trong cuốn sách có lẽ tình yêu đến với cô chỉ là khát khao một mái ấm hạnh phúc chứ không phải cảm giác lãng mạng của những con người yêu nhau tha thiết đề rồi khi nhan sắc tàn phai thứ cô còn lại là một gia đình với cái vỏ bọc bình yên nhưng nó cũng chẳng kéo dài được bao lâu lần trúng xổ số ấy đã làm thay đổi cuộc đời cô cô bị phản bội cô lại tìm về tình yêu nhưng bây giờ thứ tình cảm ấy lại thăng trầm hơn nhưng đủ để làm cô nở một nụ cười nói chung mình nghĩ cuốn này phù hợp hơn với những bạn trên 20 tuổi vì để cảm nhận được cái cám dỗ của đồng tiền sự dối trá lừa lọc và cảm giác bị một người đã đi với bạn nửa cuộc dời thì bạn cần một độ chín chắn nhất định
2
916104
2016-03-07 21:54:59
--------------------------
376743
3858
Điểm cộng đầu tiên đối với mình có lẽ là bìa sách, mà trong khoản này thì lúc nào Nhã Nam cũng làm tốt hết :D
Đầu tiên mình cũng chỉ định tìm mua một quyển truyện ngắn để đọc vì không có nhiều thời gian thôi, nhưng đọc xong quyển truyện này thì chỉ muốn đọc lại vài lần nữa, có lẽ tại vì nó khá hợp với mình. 
"Chỉ là mơ ước thôi" là câu chuyện giản dị nói về những điều đời thường nhất, nhỏ bé nhất và cũng gần gũi nhất. Và nội dung truyện khá thực tế nữa, nên mình rất thích. Có thể thấy tác giả dành nhiều sự trân trọng dành cho những người phụ nữ vô danh, nhỏ bé, và mình nghĩ mọi người phụ nữ đều rất đáng để trân trọng. Ước mơ cũng vậy. Thoạt đầu rất bức xúc vì những gì Jo đã phải trải qua, nhưng cuối cùng cũng có thể thở phào nhẹ nhõm vì ước mơ nhỏ bé của Jo đã được thực hiện. 
“Bởi vì những gì ta cần lại là những giấc mơ nhỏ thường nhật của chúng ta. Đó là những thứ lặt vặt mà chúng ta thường phải làm, theo chúng ta đến ngày mai, ngày kia, đến tương lai, những thứ linh tinh mà chúng ta sẽ mua vào tuần kế tiếp và cho phép chúng ta nghĩ rằng tuần kế tiếp ấy, chúng ta vẫn còn sống.”
4
24255
2016-01-31 19:09:16
--------------------------
365822
3858
Cuốn sách không theo kịch bản vẫn thường thấy dù hình ảnh người mẹ, người vợ rất gần gũi với người việt nam: tảo tần và lo lắng cho gia đình, yêu thương chồng dù người chồng rất đỗi bình thường. Bà ấy cân nhắc, đấu tranh tư tưởng rất nhiều trong việc quyết định lấy hay không lấy tiền trúng số. Bà xem cuộc sống hiện tại là hạnh phúc và e ngại số tiền đó sẽ làm xáo trộn cuộc sống này. Kết cuộc thì chính chồng bà phát hiện ra tờ vé số và bỏ bà ra đi... Câu chuyện làm ta suy nghĩ, cuối cùng thì hạnh phúc gì?  Phải chăng chính là những thứ ta đang có. Dù nó là gì đi nữa thì tiền không thể mua được. 
2
612857
2016-01-08 19:57:12
--------------------------
350429
3858
Chúng ta luôn mơ ước: phải chi mình có đủ tiền để mua cho bản thân 1 cái xe thật xịn, mua cho chồng/vợ món quà mà anh ấy/chị ấy hằng khao khát, mua cho con 1 máy tính tốt để học, mua cho cha mẹ 1 mảnh vườn nhỏ vui thú điền viên, mua bảo hiểm y tế tốt nhất cho mọi người trong gia đình, mua 1 tủ sách thật to cho quỹ A, mua hàng bao áo ấm cho những đứa trẻ nghèo vùng cao,  mua lương thực dành tặng các bếp ăn tình thương, .... vì chúng ta nghĩ rằng mình sẽ hạnh phúc, chồng/vợ mình sẽ hạnh phúc, con mình sẽ hạnh phúc, cha mẹ mình sẽ hạnh phúc, mọi người xung quanh mình sẽ hạnh phúc khi có những thứ đó.
Thế nhưng, khi bất chợt có thật nhiều tiền trong tay, chúng ta có còn nhớ cái danh sách dài ngoằn ở trên? Hay cái danh sách đó sẽ nhanh chóng thay đổi? Ngắn hơn hoặc dài hơn?
Mãi cuốn theo tiền bạc và mớ danh sách đó mà đôi khi chúng ta quên đi những thứ mang lại hạnh phúc, niềm vui cho chính chúng ta, người thân, bạn bè xung quanh mình từ những điều hóa ra rất vụn vặt... là cái hôn yêu thương cho chồng/vợ mỗi sớm mai thức dậy, là buổi chiều chờ đón con ở trường để thấy con cười rạng rỡ, là bữa cơm đầm ấm bên cha bên mẹ, là gói sách truyện cũ bao bọc cẩn thận trao cho nhóm quỹ A,... là rất nhiều khoảnh khắc yêu thương mà bạn đã lâu rồi không làm hay vẫn làm nhưng vội vã đến độ chẳng còn kịp nhận ra...
"Chỉ là ước mơ thôi" như 1 lời nhắc nhở gửi đến mọi người, rằng: hãy trân quý những gì bạn tạo dựng nên, bởi trong đó có tấm lòng, có tình yêu, có giá trị vĩnh cửu... mang lại cho bạn hạnh phúc giản dị mà bền vững theo thời gian!
5
463360
2015-12-10 16:14:04
--------------------------
338594
3858
mình rất thích quyển này, thật sự rất lôi cuốn dù văn phong nhẹ nhàng, đoạn cuối cao trào thì hồi hộp kịch tính. tựa gốc là "danh sách những điều tôi khao khát", xác với nội dung câu chuyện hơn. có lẽ sau khi đọc mình cũng sẽ lên những danh sách như vậy và từ từ thực hiện chúng. cũng mong đó là những ước mơ bình dị mà không cần phải có quá nhiều tiền mới thực hiện được. vấn đề của truyện cũng thật đáng suy ngẫm, một món tiền từ trên trời rơi xuống chưa chắc là niềm vui hay bất hạnh.
4
16313
2015-11-16 15:36:35
--------------------------
324886
3858
Bìa sách thiết kế đẹp. Thể hiện rõ ngành nghề của nữ chính Jo.
Cuộc hành trình của Jo nhẹ nhàng và tinh tế. Tác giả thể hiện rất rõ nội tâm của người phụ nữ trong gia đình. Dù họ trở nên thế nào thì họ vẫn luôn nghĩ về gia đình, nhưng không phải lúc nào cuộc sống cũng viên mãn như họ muốn. Hài lòng với hạnh phúc hiện tại sẽ giúp hạnh phúc đó kéo dài mãi...nhưng may mắn đã mỉm cười với Jo hay đó lại là chuỗi ngày bất hạnh đối với Jo và gia đình cô ấy. Ranh giới đó thật mong manh trước 1 tấm séc... nhưng đến cuối kết cục dành cho Jo cũng khiến tôi hài lòng... dẫu cho hạnh phúc của Jo bây giờ không còn trọn vẹn nhưng khởi đầu mới mẻ này của cô phần nào cũng an ủi được nỗi đau trước đó.
5
776615
2015-10-22 11:51:51
--------------------------
322887
3858
Sách ổn, bìa khá ổn, giấy đẹp . nội dung hay.
 Khi mất đi một thứ gì đó mới biết nó quan trọng với mình như thế nào.
Cho mình hiểu rõ về giá trị của đồng tiền, sự ham muốn đối với "nó". Cái quyết định bồng bột để rồi hối hận đã muộn. "Chỉ là ước mơ thôi" nghe khá đơn giản nhưng cũng khi "ước mơ" đó thành hiện thực thì mọi chuyện sẽ không như ta nghĩ, "nó" sẽ làm thay đổi gần như là "mọi thứ",.. Nhưng cái kết khá buồn.
 Câu truyện lôi cuốn, nói chung HAY. 
4
816148
2015-10-17 11:07:41
--------------------------
319551
3858
Mình thích cách mở đầu tác phẩm của tác giả, một mụ béo ụ ngắm mình trong gương và cảm thấy mình rất đẹp.

Mình cũng rất thích cách tác giả miêu tả tình cảm, trạng thái tâm lý nhân vật và các thói quen của các cô nàng sinh đôi, khiến cho mình tưởng đâu... tác giả thật sự là người phụ nữ. Ông diễn tả sinh động cách người chồng lừa vợ mình, lấy tiền và chết dần chết mòn ra sao. Sự đan xen giữa quá khứ và hiện tại làm cho tác phẩm thêm phần sinh động nhưng lại đầy tính nhân văn !
4
90063
2015-10-08 22:53:09
--------------------------
310147
3858
Thoáng qua thì những tưởng ước mơ thì không thành hiện thực được. Nhưng không, quyển sách rất thuyết phục khi đề cập tới chủ đề này. Nó khuyên ta không ngừng từ bỏ ước mơ, tin tưởng vào bản thân, đặt ra mục tiêu của cuộc sống và đừng ngần ngại vươn lên. Để dẫn chứng thì câu chuyện trong sách là ví dụ rất tài tình về một người phụ nữ thành đạt trong cuộc sống. Tuy nhiên cũng không phải ai cũng ăn may được như nhân vật chính. Nhưng nói chung, câu chuyện rất hấp dẫn và tuyệt hay, mọi người nên đọc để nắm thêm các bí quyết nuôi dưỡng ước mơ.
5
534452
2015-09-19 12:43:37
--------------------------
307533
3858
Mình thấy cuốn này từ lúc Nhã Nam đăng trên fanpage facebook hỏi ý kiến về bìa sách, đọc sơ nội dung là mình thấy thích và có vẻ hợp với bạn mình (vì nó thích văn học Pháp).
Cốt truyện không có gì xa vời, phức tạp, mà đơn giản xoay quoanh những gì giản dị, hết sức bình thường, những vấn để nhan nhản trong gia đình, xã hội nhưng có vẻ như ít ai để ý, nhưng nó có ảnh hưởng rất lớn đến cuộc sống hạnh phúc của ta.
Và đặc biệt là về người phụ nữ, khi mà gia đình, con cái tiền bạc chiếm hết quỹ thời gian của họ, thì có vẻ những mơ ước, những thói quen, sở thích của một người phụ nữ như mua một cái váy mới, làm kiểu tóc mới, tán gẫu với bạn bè...cũng có vẻ xa vời, khó khăn.
Bạn hãy đọc để thấy trân trọng hơn những người phụ nữ ở quanh mình: Mẹ bạn, vợ bạn, chị em bạn, cô bạn của bạn...
Và tuy rất nhẹ nhàng, không gay cấn, không hồi hộp nhưng nó rất thấm, thấm dần dần khiến mình thấy một chút chầm chậm, buồn dai dẳng.
5
421650
2015-09-18 09:49:40
--------------------------
303373
3858
Đầu tiên là mình rất thích bìa sách, hình ảnh nhã nhặn và đầy nữ tính, một điểm cộng.
Về nội dung, dường như tác giả rất trân trọng và yêu thương những người phụ nữ bên cạnh mình. Tuy là người phụ nữ trong tác phẩm của ông cũng bình thường thôi, không có gì nổi bậc và đáng chú ý, nếu không nói là xuề xòa, qua loa nhưng giọng văn của ông vẫn toát lên sự trân quý dành cho nhân vật. Câu chuyện không mới nhưng cảm giác gần gũi và tiếp thêm hy vọng cho những ước mơ nho nhỏ đời thường.
5
380464
2015-09-15 20:26:12
--------------------------
300653
3858
Chỉ là mơ ước thôi, nghe qua có vẻ mọi chuyện chỉ là ước mơ không thể thực hiện được, không cách nào hoàn thành được. Nếu không đọc sách, ai biết được cần thành công thì cần đeo đuổi, bảo vệ, phấn đấu mà hoàn thành ước mơ đó, ai biết được những thăng trầm trong cuộc sống đem lại cho người ta những bài học kinh nghiệm đáng giá ra sao. Ngay từ khi Nhã Nam đưa ra cuộc bình chọn bìa sách cho cuốn này, tôi đã bị đánh gục bởi cái tiêu đề sách. Tuy không thể mua liền nhưng những cuốn sách tôi muốn, nhất định tôi sẽ mua được. Tất cả không chỉ là mơ ước đâu.
4
479365
2015-09-14 08:56:36
--------------------------
300489
3858
Đề tài bình thường, không hẳn có gì quá gay cấn, nhưng giọng văn nhẹ nhàng dẫn dắt người đọc. Thật ra, đâu chỉ riêng phụ nữ, mà con người chúng ta nói chung, luôn luôn dằn vặt mình giữa hai luồng tranh đấu, là mơ ước, đam mê và nỗ lực sống cuộc sống mình mong mỏi hay đơn giản, yên bình một gia đình mà ai ai cũng hy vọng, bình thường đến tầm thường - và để khi có "sự cố" xảy ra, chúng ta lại ngơ ngác không biết nên xử lý thế nào mới phải. Từ cuốn sách suy ra trong cuộc sống cũng vậy thôi, luôn là những lựa chọn, đôi khi cái chúng ta được chẳng đáng bằng thứ hy sinh để có được cái-được kia, nhưng ai nói trước được chữ "ngờ", rằng "nếu" biết trước, thì chẳng đã... Nói chung, đây là cuốn sách đọc để giải trí, vì thực ra những cái nêu lên trong cuốn sách, đều là những sự thật chúng ta đã biết, cuộc sống của mình phải do mình quyết định và làm chủ.
3
131913
2015-09-13 22:57:57
--------------------------
298029
3858
Chỉ là mơ ước thôi có lẽ là câu chuyện dành cho tất cả mọi người về cảm giác biết "đủ" trong cuộc sống. Giọng văn nhẹ nhàng, những tình tiết đưa đẩy hợp lý và khéo léo khắc họa toàn bộ được cuộcsống của nhân vật. Ở dậy, đôi khi một cuộc sống giản dị vẫn đáng trân quý hơn rất nhiều so với vô vàn ước mơ xa xỉ Channel mà cái giá phải trả chính là hạnh phúc thường nhật hàng ngày. Đây là câu chuyện phù hợp với người trẻ và tất nhiên, cả những người đang mơ ước đổi đời nhờ vé số :)
5
512436
2015-09-12 13:10:17
--------------------------
291191
3858
Truyện đem đến những cảm xúc khó tả, khiến tôi muốn đọc lại để hiểu hơn. Phụ nữ chúng ta thật sự mong muốn điều gì? Chúng ta mơ đổi đời nhưng cũng muốn giữ gìn cuộc sống như hiện có, chúng ta mơ đến những chàng đẹp trai trong truyền thuyết nhưng thật ra có thể chỉ cần một người đàn ông dù thô lỗ nhưng sẽ cùng mình có một cuộc sống giản dị và bình yên, chúng ta mơ giàu sang nhưng cũng sợ đồng tiền sẽ phá hủy mọi thứ...

Sự nữ tính của câu chuyện, kể cả sự dài dòng và chút ủy mị của nó, đã cứ thế dẫn dắt tôi như vậy.
3
80143
2015-09-06 07:42:26
--------------------------
284534
3858
Mình đặt mua cuốn sách vì lời quảng cáo hấp dẫn rằng cuốn sách nằm trong top best seller, đã phát hành cả triệu bản. Mình đã nghi ngờ quảng cáo thật lố khi cầm trên tay cuốn sách. Mình nhớ có bạn đã khen bìa sách đẹp trong nhận xét, mình thì ngược lại, trang bìa chẳng gây xúc động cho mình ngoài việc mình thấy nó khá ... tầm thường. Và mình hy vọng thể loại tiểu thuyết thì cuốn sách sẽ dày hơn tí ti. Đoạn đầu truyện khiến mình thấy nhàm chán. Một cô Jo béo, bị ảo tưởng sức mạnh, thích ngắm mình trần trụi trước gương và thấy mình đẹp đẽ với tưng ấy những thớ thịt thừa. Mình đã đánh giá vội vàng rằng chả có gì thú vị khi đọc về một người phụ nữ béo sống nhờ 1 công việc bình thường tại một khu chẳng có tiếng tăm. Thế rồi cách hành văn từng đoạn ngắn, cấu trúc câu dài - ngắn bắt đầu khiến nhịp văn dồn dập, cuộc đời của cô Jo rơi vào những tình huống mình không thể đoán biết. Nỗi đau của cô trở nên cao trào thì cũng là lúc ý nghĩa nhân văn của câu chuyện được bộc lộ rõ rệt. Hạnh phúc là có một người để yêu, bằng tất cả trái tim mình, vượt qua và tha thứ. Hạnh phúc là chăm lo cho gia đình bé nhỏ của mình. Hạnh phúc là được sống với đam mê, đem đam mê ấy trở thành nguồn cảm hứng và sự thích thú cho rất nhiều người. Hạnh phúc bên bữa cơm cùng những câu chuyện phiếm.
Hạnh phúc ko mua đc bằng tiền. Hạnh phúc là có mọi điều để mơ ước, để cói gắng.
Cuốn sách bỗng trở nên kì diệu, toả sáng rực rỡ trong một buổi chiều mưa tí tách rơi.
4
89432
2015-08-31 00:50:06
--------------------------
281269
3858
Cuốn sách có bìa đẹp, nếu chưa đọc nội dung cuốn sách thì có thể đây là một cuốn sách tình yêu lãng mạn hoặc một cuốn sách tâm lý nhẹ nhàng cho tuổi mới lớn. Nhưng khi đọc hết cuốn sách rồi mới biết nó mang một ý nghĩa khác. Chỉ là ước mơ viết về một người phụ nữ đã có gia đình với những ước mơ, rồi một ngày cô trúng số. Việc này đã thay đổi cuộc sống của cô nhưng không phải nhờ vào việc dùng số tiền này mà vì chồng cô đã đánh cắp số tiền đó. Nhưng nhờ vậy cô đã thay đổi cuộc sống của mình và đưa đến cho người đọc ý nghĩa của hạnh phúc là phải phải đến từ một trái tim biết yêu cuộc sống và trân trọng chính bản thân mình!
4
56881
2015-08-28 11:53:54
--------------------------
271016
3858
Là câu chuyện về Jo một người phụ nữ bình thường nhưng cũng chính là câu chuyện của rất nhiều những người phụ nữ khác trong cuộc sống này.Jo chính là nhân vật đại diện cho những người phụ nữ ngày nay,bận bịu vất vả lo lắng cho cuộc sống mưu sinh hằng ngày, lo cho gia đình,chồng con mà phải chôn vùi đi những ước mơ riêng của mình nhưng vẫn luôn hướng tới và làm những điều tốt cho bản thân và mọi người xung quanh.Thế nhưng cuộc đời của người phụ nữ ấy thực sự thay đổi khi cô trúng tờ vé số độc đắc.Sở hữu trong tay số tiền khổng lồ mà trước giờ Jo chưa từng nghĩ đến cô băn khoăn lo lắng về việc sử dụng nó cũng như những tác động của số tiền ấy đến gia đình cô.Trong khi Jo lên một danh sách những thứ cần mua sắm trong gia đình và những món quà cho bản thân cho chồng con thì chồng cô lại nhẫn tâm trộm hết số tiền đó bỏ trốn khiến Jo như sụp đổ hoàn toàn.Kết truyện là khi Jo đã bình phục về tâm lý và tìm được một người khác yêu thương mình nhưng có lẽ'cô giờ đã được yêu nhưng cô sẽ không bao giờ yêu được nữa'.Có lẽ hạnh phúc là thứ gì đó quá mong manh chăng... 
3
490297
2015-08-18 19:57:08
--------------------------
270179
3858
Mình mua cuốn này vì khá ấn tượng với cái tên. "Chỉ Là Mơ Ước Thôi, đúng như dụng đoán của L'Express, đã thành công vang dội, mang về " bạc tỷ" cho tác giả. Chỉ hơn một năm sau khi ra mắt, sách đã bán hết hơn 1 triệu bản in, được dịch ra 33 thứ tiếng, được chuyển thể thành phim, đưa tên tuổi của Grégoire Delacourt ra toàn thế giới." Quả đúng như những thành công cuốn sách đạt được, nó hoàn toàn không làm mình thất vọng. Từ một cuộc sống nhàm chán, nhạt nhẽo đời thường, có những ước mơ mà sẽ chỉ mãi là ước mơ, Jo đã rẽ sang bước ngoặt của đời mình khi trúng tấm séc 18 triệu đô. Tác phẩm cho ta ấn tượng về cách Jo vượt qua nỗi đau, cho ta những bài học sâu sắc và thiết thực.

4
624143
2015-08-18 01:15:13
--------------------------
262959
3858
Một cuốn sách rất dung dị và gần gũi. Một người phụ nữ có cuộc sống bao người, có một gia đình không hoàn hảo và một công việc đều đều lặp lại qua bao ngày. Một ngày, có một biến cố làm thay đổi cuộc đời của cô đó là tấm vé số trúng thưởng có giá trị cao, nó làm biến động về nội tâm, xáo trộn cuộc sống của những người trong cuộc và cách xử sự của hai người với vận may đó, liệu tình yêu sau bao năm có thể bị đánh đổi bởi tiền bạc?... Đây là một tác phẩm đáng xem và suy ngẫm.
5
727180
2015-08-12 11:24:41
--------------------------
255152
3858
Cuốn sách mỏng và đầy lôi cuốn. Mình thích cách tác giả đưa Jo vào những tình huống éo le, một cuộc sống nhàm nhán rồi cách Jo lên kế hoạch, những ước mơ đời thường khi trúng sổ số và cả cách Jo vượt qua nỗi đau bị người chồng trộm tấm séc trị giá 18 triệu đô và tìm được hạnh phúc mới. Có những quyển sách hay những blog ảnh hưởng tới hàng vạn người và quyển sách này là một trong những quyển sách như vậy. Nó là liều thuốc tinh thần cho những ai đang còn chơi vơi, đang giằng xé tình yêu và hạnh phúc.
5
627
2015-08-05 19:59:43
--------------------------
248916
3858
Với cốt truyện rất thực tế cùng với lối viết đời thường cùng với quan điểm tiền không thể mua được hạnh phúc của nhà văn Gregoire Delacourt, " chỉ là mơ ước thôi " là một cuốn sách rất hay đáng để cho các bạn suy ngẫm về cuộc sống.
Có những điều chúng ta có thể tha thứ, những những gì mà chồng của chi Jo đã làm thì anh ta có đáng được tha thứ? Bất ngờ ở cuối chuyện mình rất thích. Phụ nữ đáng có được hạnh phúc của riêng mình dù cho họ xấu hay đẹp về ngoại hinh.
5
567316
2015-07-31 11:11:50
--------------------------
247643
3858
Tôi được nghe giới thiệu "Chỉ là mơ ước thôi" bởi một người bạn và tôi đã tìm mua nó ngay vì khá tò mò về người phụ nữ Jo. Quả thật, quyển sách này không làm tôi thất vọng khi kể về câu chuyện của một người phụ nữ bình thường nhưng lại có kì tích đặc biệt. Người phụ nữ tên Jo, 47 tuổi có nhiều ước mơ từ xa hoa đến bình dị đời thường. Rồi cô trúng tờ vé số đủ để cô biến mọi ước mơ của cô thành sự thật. Nhưng mọi chuyện sau đó có thật sự khiến cô hạnh phúc? Thật khó đoán.
4
89637
2015-07-30 11:58:26
--------------------------
247384
3858
Tôi đa đọc tác phẩm này liền mạch suốt 1 đêm. Thật khó dứt ra với câu truyện hấp dẫn như vậy. Tôi nhìn thấy trong Jocelyne Guerbette có chút gì đó của tôi, của vô vàn những bà nội trợ trên thế giới. Những ước mơ từ nhỏ nhoi, thực tế đời thường, đến những mơ ước xa vời vợi biết là phù phiếm, ảo vọng nhưng mình vẫn ước mơ, cho đời nó bướt nhạt. Rồi, bất ngờ đến với Jocelyne Guerbette. Cuộc sống cô rẽ qua 1 ngả khác, giống và cũng thật khác với ước mơ của cô. Cô có những gì cô mơ ước. Và cô cũng mất rất nhiều.
Có lẽ, những ai vẫn tôn thờ triết lý tôn thờ kim tiền, cần suy nghĩ lại. Bởi hạnh phúc không phải là 1 thứ có thể mua được bằng tiền.
5
293704
2015-07-30 10:15:21
--------------------------
241703
3858
Jocelyne - một người phụ nữ rất bình thường giữa lòng nước Pháp, với một người chồng cũng bình thường không kém. Tác giả đã khai thác được những ý nghĩa thật sự của hạnh phúc từ cuộc sống bình dị của họ. Ai bảo có tiền mua tiên cũng được thì hãy đọc và cảm nhận cuốn sách này. Cùng với những ước mơ có thể đạt được nhờ số tiền triệu đô có được do trúng xổ số là cái giá phải trả: chồng cô "cướp" số tiền đó và cao chạy xa bay với những ước mơ của cô. Cô tưởng như vậy là hết thật rồi nhưng hạnh phúc thật sự của cô mới chỉ bắt đầu.
5
528434
2015-07-25 19:32:02
--------------------------
232444
3858
Guerbette là một phụ nữ 47 tuổi, như bao người khác, cô có nhiều mơ ước từ nhỏ đến lớn, từ xa hoa tới đời thường, những thứ vụn vặt. Cuộc đời cô thay đổi khi ngày đẹp trời đến, cô trúng nhiều triệu đôla. vì thế, cô có thể trả phí học cho con gái, chữa bệnh cho người bố đã già. Tuy vậy, đời đâu đơn giản như thế. Một khi trúng số, cô phải đánh đổi nhiều thứ, sự thoải mái có, sung sướng có, nhưng lo âu băn khoăn cũng nhiều. Vậy một khi những điều ước của chúng ta thành sự thật, liệu chúng ta có thực sự hạnh phúc không. Cùng đọc nào.
5
393693
2015-07-18 20:19:52
--------------------------
232315
3858
Đây là một cuốn sách hay, bởi nó đem lại cho mình một cái nhìn khác về hạnh phúc.
Trong sách tác giả đã khai thác được những khía cạnh, những định nghĩa tưởng chừng như quen thuộc của hạnh phúc với mỗi người.
Một cuốn sách rất thực tế và chân thực, khi đọc mình cảm thấy hòa mình vào được với các nhân vật, và chính mình cũng cảm thấy thấu hiểu được phần nào suy nghĩ của họ.
Bên cạnh đó, tác giả cũng đã cho người đọc thấy rằng, Tiền không đem lại hạnh phúc trọn vẹn cho tất cả mọi người.
Ý nghĩa.
4
459083
2015-07-18 18:40:31
--------------------------
231575
3858
Cuốn sách phản ánh thực tại của cuộc sống, khi mà con người ta đặt đồng tiền lên trên mọi thứ. Có thể cuộc sống sẽ đầy đủ hơn khi bạn có nhiều tiền, nhưng chưa chắc nó sẽ làm cho bạn và những người xung quanh bạn cảm thấy hạnh phúc nếu cách bạn cư xử với đồng tiền không đúng. Nếu như mở đầu cuốn sách là một bức tranh không quá sáng nhưng lại ngập tràn hạnh phúc, những hạnh phúc giản đơn từ cuộc sống. Nhưng kể từ khi tờ vé số xuất hiện nó làm cho con người ta trải qua quá nhiều cảm xúc, để rồi khi đến kết thúc, ta lại ngậm ngùi với sự thật. Nếu bản thân không vượt qua được sức hút của đồng tiền thì sẽ dễ khiến chính bản thân ta và những người xung quang đau khổ. 
Đây là một cuốn sách khá thực tế, hay và đầy ý nghĩa! 
5
172185
2015-07-18 10:54:53
--------------------------
230156
3858
Mình không có ý định mua quyển này nhưng vì nó nói về phụ nữ nên quyết định đọc thử :D nhân vật chính lớn chắc gần gấp đôi tuổi mình luôn nhưng chính sự bình dị, đời thường của cô và câu chuyện này khiến mình đồng cảm, thậm chí là nghĩ về tương lai của mình một chút. Có cái gì đó trong người phụ nữ này làm mình xúc động nhưng cũng hơi tức giận: ước mơ chính đáng, khao khát thầm kín, nhưng lại quá an phận, chấp nhận những gì đang có, rồi chút may mắn trong cuộc sống giúp cô trúng số nhưng điều này không thay đổi được tính cam chịu trong cô. Cuối cùng vẫn không bứt ra được khỏi "số phận". Đọc đoạn kết hơi bứt rứt một tí, nhưng có lẽ nó phù hợp với nhân vật như người phụ nữ này, nên cũng không làm mình ngạc nhiên lắm.
3
5412
2015-07-17 11:49:42
--------------------------
228616
3858
Một tác phẩm hay, nhưng lại không hợp tuổi với mình :v. Dù vậy nhưng đây vẫn là một tác phẩm vô cùng ý nghĩa, đem lại một cảm xúc xao động khó tả. Tác phẩm nói về những người mẹ, những người phụ nữ tuổi trung niên với niềm mơ ước chân thành,  đầy thực tế. Thật khâm phục khi tác giả đã viết một cách đủ đầy về tâm hồn người phụ nữ, về sự cân bằng giữa mình và mọi người, về những điều giản dị trong cuộc sống. Tác phẩm giống như một nốt trầm về cuộc đời của những người phụ nữ, giữa những điều khát khao và nơi chốn thực tại. Đơn giản rằng: Bạn nên đọc. :)
5
595874
2015-07-15 17:56:22
--------------------------
228152
3858
Có lẽ tiêu đề sẽ khiến mọi người bảo là mình chê, nhưng thật sự mình thích kiểu truyện như vậy, rất đời thường, rất thực tế, nhưng qua lời miêu tả của tác giả nó cũng là một cuốn đáng để đọc một mạch đến hết.
Đồng tiền cũng chỉ là một phương tiện, nó không làm con người ta hạnh phúc hay đau khổ, nó chỉ làm những cảm xúc đó mãnh liệt hơn thôi.
Khi bạn hài lòng với những gì bạn đang có, bạn tìm được một góc bình yên trong tâm hồn, khi có tiền hay không, bạn vẫn có thể đạt được trạng thái ấy.... và ngược lại, không có số tiền nào có thể đủ cho lòng tham của con người.
5
196599
2015-07-15 07:13:32
--------------------------
222698
3858
Thật sự "Chỉ là mơ ước thôi" --- quá tuyệt, tiền không có lỗi, lỗi ở việc tham tiền. Mình đã đọc một mạch hết luôn vì sợ dòng cảm xúc bị đứt quãng. Quả thật tác phẩm dẫn dắt ta qua nhiều cung bậc cảm xúc khác nhau: mơ ước, đơn giản, dằn vặt, đắn do, tuyệt vọng và cuối cùng là bình yên... Khi gập quyển sách lại mình đã thở dài trong nhẹ nhõm. Người vợ Jo đã sẵn sàng vứt bỏ cả 1 khối tài sản khổng lồ mà hàng triệu người mong muốn để có thể giữ gìn hạnh phúc gia đình bình yên, để bảo vệ hạnh phúc đó trước ma lực của đồng tiền. Nhưng Jo đã nhầm. Người chồng đã vì ước mơ ích kỷ của hẳn, đã lừa phỉnh và giết chết tâm hồn vợ mình  để ôm đi đống tiền kia. Nhưng cái ích kỉ của hắn, khi đạt được những thứ hắn muốn: Nhà lầu, xe hơi, đồng hồ xịn...thì lương tâm lại bị dằn vặt bởi tội lỗi, bởi đau khổ, hắn đã bị héo khô vì sự tham lam của mình mà đánh mất đi tình yêu đích thực của cuộc đời....Tình yêu đích thực có thể ko chiến thắng được sự ham muốn của con người, nhưng cuộc sống luôn công bằng ở một mặt nào đó. Dù ta có bị tổn thương, bị dẫm đạp thì những kẻ gây tội vẫn sẽ chịu đớn đau nhiều hơn. Hay! * các bạn nữ nên đọc * =)). boy thì càng nên =))
5
309333
2015-07-06 07:37:26
--------------------------
219732
3858
Đọc cuốn sách này đã khiến tôi đã nghĩ rất nhiều về cái gọi là số phận.
       Nhớ ngày xưa có câu chuyện ông vua được tiên tri sẽ bị con trai mình giết chết nên  đã đuổi con trai mình ra khỏi vương quốc, cho đến một ngày chiến tranh nổ ra, hai cha con ở hai bên chiến tuyến , ông vua đã bị chính chàng trai không biết đó là cha mình bắn chết. Số phận luôn giữ đúng lời hứa của mình.
        Khi số tiền trúng số ập đến với Jo thì chúng ta đã dự cảm trong đầu bao nhiêu là bất hạnh, công ty xố số cũng đã cảnh báo , Jo trăn trở, ai cũng  biết  tiền có thể đem đến hạnh phúc nhưng còn tàn phá nó nhiều hơn.  Jo đã  quyết định vứt bỏ tiền bạc đểcó thể giữ gìn hạnh phúc và tình yêu mà cô đang có. . Nhưng móng vuốt của số phận đã sờ đến và nó không dễ buông tha cho ai, Jo phải nhận lấy sự đỗ vỡ còn khủng khiếp hơn so với dự đoán ban đầu nếu cô sử dụng số tiền đó.  Đối với tôi, đây là cái mà tác giả đã rất thành công trong  câu chuyện này: liệu tình yêu có thể nào chiến thắng nổi số phận ?
5
129176
2015-07-01 21:25:24
--------------------------
211369
3858
Tựa gốc của cuốn sách :"La liste de mes envies" nghĩa là danh sách những điều tôi khao khát.
Một cuốn sách tâm lý xã hội nước Pháp, lời văn nhẹ nhàng, da diết, viết về những chuyện đời thường xung quanh người phụ nữ trung niên không có gì nổi bật về ngoại hình. Tình cờ trúng số với một món tiền kếch xù, là may mắn hay bất hạnh với gia đình cô đây ? 
Đã có lúc tôi bực mình vì tính cách cam chịu của cô. Phụ nữ pháp cũng giống phụ nữ Việt đó, cả đời chỉ lo lắng cho chồng con mà chẳng để ý đến bản thân. Nhưng bạn yên tâm, đây là cuốn sách pháp, có cái kết kiểu Pháp. Và chả hiểu sao tôi không thích cái kết này nên chỉ cho 4 sao :v
4
405585
2015-06-20 15:17:48
--------------------------
211314
3858
Làm sao để nói hết sự yêu thích về quyển sách mỏng mảnh nhưng sâu sắc này? 
Truyện mở đầu và xuyên suốt bằng giọng văn dịu dàng, tình cảm nhưng cũng đầy hài hước, chua cay của tác giả dưới cái nhìn của một người phụ nữ luống tuổi và sồ sề. Thông qua nhân vật tôi, những chuyện đời ngẫu nhiên của những người xung quanh được gợi lên, hoặc vui, hoặc buồn. Nhưng nổi bật nhất, vẫn chính là cuộc đời và những cảm nghĩ của nhân vật chính, người phụ nữ bình thường với những ước mơ giản dị, về ngôi nhà, mảnh vườn, cửa hàng xén, người chồng và các con.
Có những nỗi đau tồn đọng trong lòng người phụ nữ như tro than có thể bùng cháy bất cứ lúc nào, nhưng luôn được kiềm chế và đè nén. Có những ước mơ thời thiếu nữ ngỡ đã quên nhưng khi ở một mình lại miên man nhớ tới, để rồi cảm thấy nuối tiếc và buồn bã, vì không thể hoàn thành hoặc không dám hoàn thành. Có những cú ngã, một lần trở thành nỗi ám ảnh suốt đời.
Nữ nhân vật chính đã vô cùng dịu dàng và tinh tế với từng suy nghĩ về những điều diễn ra xung quanh mình, thậm chí là với từng người ghé thăm trang blog của cô. Vào một ngày định mệnh đến, cô phải đối mặt và lựa chọn mỗi hành động của mình với nó, thật đáng yêu làm sao khi cô chọn cách im lặng và từ bỏ món tiền thưởng. Nhưng ngờ đâu, đấy chính là lối vào sự vỡ mộng. Thật cay đắng. Khi lựa chọn từ bỏ phù phiếm để lấy những gì thân thuộc, song lại chỉ nhận được sự bội phản từ người yêu thương nhất. Để rồi từ đó trở nên mất niềm tin và tình yêu và cuộc sống...
Có thể nói, gói gọn lại câu truyện hài hước nhưng buồn bã này chính là, một sáng thức dậy, nhận ra rằng, tôi đơn giản chỉ là vỡ mộng bởi những điều chỉ là mơ ước thôi.
4
30924
2015-06-20 13:38:32
--------------------------
205406
3858
Sẽ rất tiếc nếu bỏ qua quyển sách này, lúc đầu tớ mua quyển sách này là được bạn giới thiệu và cho mượn đọc rồi thích luôn từ lúc nào không hay nên quyết định mua một quyển.
Lãng mạn, hiện thực chính là văn phong của Marc Levy, đầy tính nhân văn. Ước mơ chỉ mãi là ước mơ nếu ta không dám thử thách và thực hiện. Nhưng nếu không thực hiện được mà vẫn đặt nó làm mục tiêu thì đó là điều thật đáng quý.
Bìa sách rất đẹp và dễ thương, mình rất thích các họa tiết minh họa :3
Một cuốn sách rất hay
5
621744
2015-06-06 17:45:47
--------------------------
199442
3858
"Chỉ Là Mơ Ước Thôi" là một cuốn sách đáng đọc. Văn phong của tác giả rất thực tế. Tôi là một phụ nữ không thành công trong sự nghiệp, hiện vẫn đang thất nghiệp và tôi có rất nhiều mơ ước.... nhưng chẳng có mơ ước nào thực tế hết. Đơn giản vì có ai đánh thuế ước mơ đâu. Nhưng khi đọc "Chỉ Là Mơ Ước Thôi" tôi lại ngộ ra rất nhiều điều trong cuộc sống. Cuộc sống này cái gì cũng có chỉ không có hai từ " Nếu như".... Vì vậy nhân vật Jocelyne Guerbette là một người tôi thấy rất giống mình và đa số phụ nữ trưởng thành nói chung.... Tôi rất yêu quá hình tượng Jocelyne Guerbette này........ Mọi thứ khi nằm trong tầm tay thì ta không bao giờ nhận ra sự tồn tại của nó chỉ khi mất đi rồi ta mới biết nó quý giá đến nhường nào!
5
640962
2015-05-22 11:33:45
--------------------------
194268
3858
Nếu nhắc đến văn học lãng mạn mà chỉ biết Marc Levy, hẳn cũng giống như nói về món ăn Việt Nam mà chỉ biết phở hoặc bánh mì. Còn có nhiều thứ khác mà.

Và đây là một trong những thứ như vậy.

Hòa mình cùng nhân vật. Cùng dự đoán cốt truyện. Không dễ để khiến một cô gái 21 tuổi như mình thực sự hiểu được tâm tư của một phụ nữ trung niên, nhưng tác giả đã làm được, một cách xuất sắc.

Về ước mơ. Nhưng không động viên người ta theo đuổi hay gì gì đó. Bởi vì lúc ước mơ (sắp) được thực hiện rồi, vẫn có một câu chuyện đáng nghe.

Rất muốn đọc lại, để một lần nữa sống lại cảm giác tuyệt vời ấy.
5
618004
2015-05-09 23:19:57
--------------------------
