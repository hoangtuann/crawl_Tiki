449102
3117
Mình rất thích nhân vật Fliss - chị gái của nhân vật chính - cô dâu Lottie, cô ấy vừa có sự nghiệp, yêu thương em gái, yêu thương con trai, nhưng mất mát một cuộc hôn nhân "thảm hoạ". Cô ấy độc lập, kiểm soát, bản năng và rất hóm hỉnh.

Còn Lottie, cô ấy sống bản năng tình cảm hơn là lý trí, chỉ đọc tình sử những "Lựa Chọn Không May" hậu thất tình của Lottie là đủ biết cuộc sống phong phú của cô ấy rồi! Và mặc dù 33 tuổi, cần lấy chồng, muốn một đàn con, nhưng Lottie vẫn suy nghĩ một gia đình vẫn dựa trên nền tảng tình yêu của hai người. Điều đó nghe thật cổ tích một-cách-hơi-ngốc-nghếch, nhưng ai quan tâm chứ, ai cũng cần được yêu mà!

Mình thích cách dịch của dịch giả "Tờ Nhờ", những câu chửi thề, tiếng lóng hay câu thoại kinh điển "đút lạp xường vào cái bánh bao" hay những suy nghĩ "con điên", "thằng khốn", "thằng dở hơi"..... Ôi thôi đọc đau cả ruột!

Sophie Kinsella đã không làm độc giả thất vọng khi vận dụng linh hoạt đổi ngôi giữa các nhân vật, mỗi nhân vật đều có suy nghĩ, hành động, lý tưởng, và đặc biệt đều cho rằng mình đúng!

Các nhân vật nam Richard, Lorcan hay Ben đều hài hước không kém, các tuyến nhân vật phụ khi mỗi tình huống xảy ra cũng hài hước không kém.

Câu chuyện đánh giá cao sự lựa chọn, có tính thực tế rất cao của người phương Tây đối với hôn nhân gia đình, trước lựa chọn người bạn đời mà (cứ trước mắt cái đã) sẽ cùng đi với mình trên đường đời, họ không thể nhắm mắt buông xuôi, thích lấy là lấy, thích bỏ là bỏ, đó là tinh thần trách nhiệm mà mỗi một người nên chuẩn bị khi đứng trước cánh cửa hôn nhân.
4
170711
2016-06-16 23:19:21
--------------------------
448860
3117
Mình chưa đọc nhiều các tác phẩm của Sophie nên đối với mình tác phẩm này cũng chiếm nhiều tình cảm của mình
Phần bìa của Nhã Nam đợt này làm mình khá hài lòng. gáy đóng vừa vặn. giấy và phông chữ tốt
Điều thú vị ở tác phẩm của Sophie là tác giả luôn viết tác phẩm dưới góc nhìn của nhân vật tôi. Cách viết này chiếm rất nhiều tình cảm của mình bởi nó đem lại cho mình những cảm xúc thích thú về dễ cảm nhận khi đọc
Điều thú vị tiếp theo là nhân vật nữ của Sophie có tính cách rất hài hước và thẳng thắn. Hơn nữa tuyến nhân vật mà tác giả tạo ra rất phù hợp với cốt truyện và làm mọi chuyện diễn ra một cách hợp lý. Ở riêng tác phẩm này cảe hai chị em Fliss và Lottie đều là những cô gái có suy nghĩ lạc quan và điên rồ. 
Điều thú vị cuối cũng là sự kết thúc có hậu. Khi mà ở tác phầm của Sophie cũng như tác phẩm này đều làm mình mỉm cười ở mỗi phần cuối. Và ở mỗi câu truyện thì đều có sự thay đổi ở cách nhìn nhận của mỗi nhân vật.
5
856660
2016-06-16 16:31:15
--------------------------
437570
3117
Sofie Kinsella là một tác giả viết thể loại chick-flick rất thành công. Mình rất thích giọng văn hài hước, hóm hỉnh thông minh của cô. Cô luôn đặt ra những tình huống oái ăm, có một không hai và cô luôn thành công trong việc tháo cởi  nút thắt cách tài tình. Nhân vật của rất thực tế, không bị tô vẽ bằng những từ ngữ hoa mĩ. Họ rất đời thường, đôi khi khá hậu đậu nhưng tất cả đều rất tốt bụng và luôn thông minh, sáng tạo, luôn biết nghĩ cho người khác.
 Sau seri Tín Đồ Shopping, Hồn Ma sành Điệu, Anh có thể giữ bí mật, ... cô lại tiếp tục thành công với Đêm Tân Hôn này. 
Nếu bạn là Fan hâm mộ của Sofie thì bạn không thể bỏ qua tác phẩm này.
5
389
2016-05-28 14:16:46
--------------------------
394052
3117
Tôi là fan hâm mộ truyện của Sophie Kinsella và quyển này cũng cùng mô típ với những quyển truyện khác của cô. Nhân vật chính là một cô gái vừa rơi vào tình trạng không may khi chàng trai cô yêu không cầu hôn cô như mong đợi. Trong trạng thái mất cân bằng nhất về mặt tình cảm thì cô được một người mà cô đã không gặp 15 năm nay cầu hôn và cô chẳng ngần ngại gì mà nói "vâng". Thế là một lô một lốc chuyện buồn cười, bất ngờ kéo đến, đưa cô và chị gái của cô vào một cuộc phiêu lưu lộn xộn. Cuối cùng thì, như những cái kết trong các truyện khác của Kinsella, đâu lại vào đấy, châu lại về hợp phố. Mặc dù theo những mô típ cũ, tài kể truyện của Kinsella vẫn khiến bạn bị cuốn vào các tình tiết thú vị và hài hước. Theo tôi thì đây là một chick-lit điển hình để bạn tiêu khiển.
4
33461
2016-03-09 20:59:47
--------------------------
384185
3117
Mình rất thích truyện của Sophie Kinsella và cũng từng đọc qua một vài tác phẩm của cô ấy nên khi Nhã Nam phát hành đã mua ngay lập tức. Cá nhân mình thấy "Đêm tân hôn" không hay bằng những quyển trước. Tuy nội dung khá mới lạ và vẫn hấp dẫn như phong cách viết của cô nhưng khi đọc mình cảm giác khá bình thường, không quá thu hút. Như khi đọc quyển "Điện thoại này dùng chung nhé" hay "Anh có thể giữ bí mật" thực sự mình không cưỡng lại được, cứ muốn đọc mãi nhưng với "Đêm tân hôn" thì trầy trật mãi mới đọc xong. Nửa đầu sách không hay lắm nhưng nửa sau khá ok. Mình rất thích cách kết truyện của Sophie Kinsella, bao giờ cũng thế , luôn tốt đẹp cả ;))
Một điểm cộng cho cuốn sách này đó là bìa khá đẹp, không hẳn bắt mắt nhưng khá tinh tế và nhẹ nhàng nhưng cũng không kém hấp dẫn.
4
445157
2016-02-21 21:45:38
--------------------------
374732
3117
Hai điều đưa mình đến với cuốn sách này, thứ nhất là truyện của Sophie, luôn là những câu chuyện nhẹ nhàng nhưng hấp dẫn, pha thêm chút hài hước nên không bị nhàm chán, và hai là sách của Nhã Nam với thiết kế bìa ấn tượng, thu hút, chất lượng giấy thì quá ổn. Lottie là nhân vật chính của câu chuyện, nhưng càng đọc lại càng bị thu hút bởi câu chuyện của Fliss, một cô chị với mục đích để bảo vệ em gái mà nghĩ ra đủ mọi cách quái chiêu để ngăn cản đêm tân hôn, những mối quan hệ làm thay đổi dần suy nghĩ của cô ấy...
4
359073
2016-01-26 22:47:16
--------------------------
343775
3117
Sophie Kinsella lại tiếp tục đem tới cho độc giả một tác phẩm lãng mạn hài hước đầy lôi cuốn. 
Vẫn theo lối viết người kể chuyện là nhân vật chính, nhưng khác biệt so với các tác phẩm cũ là viết theo lời của 2 nhân vật. Điểm nhìn thay đổi liên tục khiến người đọc bị cuốn theo, chỉ muốn đọc liền một mạch để không bỏ sót chi tiết nào. 
Nếu đã từng đọc các tác phẩm trước như series Tín đồ shopping, thì nhất định phải đọc cuốn này. 

Điểm trừ 1 chút là đôi khi đọc nếu kỹ tính sẽ thấy lỗi dịch thuật hoặc biên tập. 
4
17249
2015-11-27 09:46:30
--------------------------
339147
3117
Thật sự ban đầu mình mua sách này chỉ vì bìa sách. Tình cờ lướt qua cuốn sách này khiến mình gợi nhớ đến Santorini, địa điểm trong mơ mình luôn mong được đặt chân đến 1 lần, và mình đã ấn nút đặt mua mà không do dự
Khi nhận sách, mình ngấu nghiến đọc hết trong vòng 1 ngày và quả thật là không hề hối hận với quyết định của mình. Khi chỉ vừa đọc 2 chương của quyển sách mình đã không rời mắt được. Sự hài hước, lãng mạn ngập tràn, mình dễ dàng tưởng tượng ra mọi tình huống như đang xem 1 bộ phim hài tình cảm mà không kém phần ý nghĩa. thỉnh thoảng bắt gặp mình đâu đó như Lottie, thích mơ mộng và tỏ ra cứng rắn. Mình sẽ không nói nhiều về nội dung sách để các bạn còn tìm đọc :) quả thât sau quyển sách này mình sẽ phải tìm mua thêm sách của Sophie vì quá ưng giọng văn của cô ấy
5
894283
2015-11-17 15:12:16
--------------------------
294294
3117
Đây là cuốn thứ hai mình đọc của Sophie Kinsella, cuốn trước là Anh có thể giữ bí mật không?. Nội dung câu chuyện phù hợp với những bạn thích chút hài hước, chút bất ngờ và lãng mạn. Mình thích nhất ở cuốn này chính là cách sắp xếp theo lời kể từng nhân vật, rất chân thực và có cái nhìn đa chiều. Mặc dù là đêm tân hôn là của Lottie nhưng thật sự mình thích thú khi đọc phần của Fliss và Lorcan hơn. Mấy trò can dự của Lliss và cách cô chị giải quyết mọi vấn đề trong cuộc sống của cô ấy rất thú vị và sáng tạo. Cuốn sách 412 trang, dày dặn, cầm thích tay nên bạn sẽ hứng thú đọc tới tận cuối cùng. 
4
565162
2015-09-09 10:03:55
--------------------------
280610
3117
Nếu ai đã quen thuộc với những tác phẩm của nữ tác giả này thì hẳn là sẽ thấy quyển này có đôi chút quen thuộc trong văn phong và cách xây dựng tình huống. Tuy cũng thú vị nhưng mình chưa thấy thoát ra được những nét cá tính trước kia của tác giả, đôi chỗ hơi rối vì có quá nhiều tình tiết, nhưng nhìn chung đây là một quyển sách ổn, khá hay cho những bạn yêu thích tiểu thuyết lãng mạn hiện đại có chút hài hước.
4
476360
2015-08-27 20:33:55
--------------------------
259324
3117
Tự hỏi Lottie sẽ ra sao nếu không có chị của mình. Mình thích nhân vật Fliss hơn cả nữ chính, thích mấy suy nghĩ tưng tửng của chị ấy và cả tình yêu của cô dành cho em gái của mình. Còn Lottie theo cảm nhận của mình thì khác là bốc đồng khi gặp cú sốc chia tay. Thật may là có Fliss, nếu không thì không biết cô nàng sẽ ra sao. Nhân vật Richard thì khá mờ nhạt, nếu có thêm một vài chương về Richard có lẽ sẽ thú vị hơn. Đến kết truyện mình lại ước giá mà có thêm chương về tình yêu của Fliss và Lorcan nữa thì quá tuyệt.
4
111393
2015-08-09 12:35:38
--------------------------
257145
3117
Mình đọc nhiều tác phẩm của Sophie Kinsella nên mua luôn khi nhìn thấy tác phẩm này. Giọng văn của Sophie thì vẫn như thế. Vui nhộn, hài hước, bất ngờ. Nhân vật chính của truyện là Lottie có một cái kết hợp lý, không bất ngờ. Nhưng mình vẫn cảm thấy là truyện không hấp dẫn như các tác phẩm khác, cứ thiếu thiếu một điều gì đó. 
Câu chuyện kết thúc mở cho tuyến nhân vật phụ. Cũng khá là tò mò cho những nhân vật này. 
Bìa sách đẹp, giấy đẹp, cầm chắc tay. Giấy không bị chói mắt. 


3
463104
2015-08-07 14:21:14
--------------------------
220764
3117
Ngay khi thấy truyện mới của Sophie, mắt mình đã sáng rực và lập tức mua luôn. Truyện của Sophie vẫn thế, cực kỳ vui nhộn với những nhân vật cá tính kèm suy nghĩ hơi lập dj mà nhiều người sẽ thấy rất "khùng", song lại không thể khiến độc giả thấy ức chế hay bực bội, Có lẽ đó là tài của Sophie. Lúc đầu đọc giới thiệu, mình đã nghĩ Lottie là tuyến truyện chính mà mình muốn theo dõi hơn tuyến truyện của cô chị gái, và chuyện xảy ra hoàn toàn trái ngược. Càng đọc càng bị thu hút hoàn toàn vào các chương của cô chị, nhiều khi chỉ muốn đừng có chương nào của Lottie nữa. 
1 điều phải nói, các nhân vật nam trong truyện của Sophie quá tuyệt vời, yêu khủng khiếp ấy, nữ chính thật quá may mắn vì có họ trong đời!
4
135788
2015-07-02 22:46:22
--------------------------
212996
3117
Mình đã tặng cuốn này cho Bạn mình và bạn bảo sao không tặng đêm đầy sao hay gì khác mà lại tặng đêm tân hôn , nói thế chứ nhìn  vui lắm luôn. Xuyên suốt câu chuyện, thì  trải qua nhiều cung bậc cảm xúc. Câu chuyện hay, tinh tế. Là một câu chuyện khá nhẹ nhàng nhưng được nêm một chút lãng mạn, gây cấn và tình cảm của tác giả, có lẽ vậy. Bìa sách thì cũng khá là tinh tế và nội dung của truyện thì lại hay hơn nữa.Mình rất thích truyện của Nhã Nam phát hành khá hay và lý thú
Nên đọc.
4
671691
2015-06-23 09:50:36
--------------------------
