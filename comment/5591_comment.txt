454947
5591
Hồi mình học lớp 7 toàn để đanh tiền vì mục tiêu là mua trọn bộ danh tác thế giới này... Truyện tuy chưa truyền tải 1 cách hiệu quả nhất về tác phẩm " thằng gì ở nhà thờ Đức Bà" nhưng cũng khá đầy đủ... Dành cho những ai không đọc truyện chữ hoặc không có thời gian để đọc tác phẩm này thì quyển sách này sẽ là lựa chọn đúng nhất...  Giúp mọi người hiểu hơn về tác phẩm và thông điệp quý giá mà tác giả truyền đạt cho người đọc... Sẽ tiếp tục úng hộ tiki 
5
729595
2016-06-22 11:33:12
--------------------------
364497
5591
  Bộ truyện " Danh tác thế giới" là một bộ sách tóm tắt các tác phẩm nổi tiếng thế giới bằng tranh vẽ, giúp người đọc tiếp cận tác phẩm một cách trọn vẹn hơn.Tác phẩm " Thằng gù nhà thờ Đức Bà" cũng là một tác phẩm rất thành công trong việc dùng tranh vẽ lột tả toàn bộ tác phẩm nguyên bản. Một mặt nào đó tác phẩm miêu tả chân thực, theo sát cốt truyện, nắm bắt trọn vẹn các tình tiết chính trong tác phẩm gốc, dùng những tranh vẽ cực kì dễ thương tả lại những chi tiết đấy, khiến cho người đọc cảm thấy gần gũi hơn, dễ tiếp cận hơn với tác phẩm, nắm bắt nội dung tác phẩm một cách bao quát hơn so với việc đọc nguyên bản nhiều tình tiết. Truyện này thích hợp với nhiều lứa tuổi, kể cả trẻ em.
5
893157
2016-01-06 12:15:17
--------------------------
260493
5591
Truyện Thằng gù nhà thờ đức bà đã ca ngợi tình yêu vô cùng trong sáng của một người       với những khiếm khuyết và một cô gái Ai cập vô cùng xinh đẹp và kiêu kì. Đồng thời truyện cũng phê phán tầng lớp quý tộc bấy giờ luôn lộng hành với mọi người. Truyện là sự kết hợp hoàn hảo giữa truyện tranh và truyện viết giúp cho không chỉ người lớn mà trẻ em đều có hứng thú say mê với câu truyện. Hình ảnh bìa nhiều màu sắc hấp dẫn giá cả lại vừa phải phù hợp với tất cả mọi người

5
59512
2015-08-10 14:32:12
--------------------------
