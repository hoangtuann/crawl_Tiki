470009
8274
Trong 4 kĩ năng thì viết và đọc là 2 kĩ năng khá quan trọng và khó, chiếm nhiều điểm trong IELTS, vì vậy mình quyết định mua quyển sách này. Quyển sách khá chuyên sâu về các đề tài đọc và viết. Các bài đọc tuy dài nhưng rất thú vị, không gây cho mình cảm giác khô khan, buồn chán. Quyển sách có kèm đáp án tiện lợi cho mình trong việc tự kiểm tra. Nhưng đọc mình thấy có 1 trang bị rách, chắc do việc in ấn có sai sót, mình cũng thông cảm. Phần viết có các chủ đề hay, phù hợp với mình, bạn có thể tham khảo gợi ý ở cuối sách.
4
635412
2016-07-07 08:09:31
--------------------------
