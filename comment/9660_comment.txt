222905
9660
Ai cũng có cho mình một thời được gọi là tuổi "teen" nhưng cái khoảng thời gian ấy có đẹp, có đáng nhớ hay không? Tôi chọn cho mình cuốn sách này làm bạn đồng hành và tôi hoàn toàn không hối hận. Vì sao ư? Vì nó giúp tôi quá nhiều điều bổ ích. Ngay từ bài viết đầu tiên, cuốn sách giúp tôi biết trân trọng cuộc sống mà tôi đang có, biết yêu cuộc sống, yêu bản thân của mình. Rồi kế tiếp đó là rất rất nhiều những bài học cần thiết. Tôi thấy mình trưởng thành hơn, quyết đoán hơn, chững chạc hơn, khóe léo hơn, sống tốt hơn, đẹp hơn, biết dấn thân, biết suy ngẫm và đặt ra mục tiêu rõ ràng cho bản thân. Cuốn sách này sẽ là người bạn tuyệt vời cho các bạn trẻ tuổi teen đấy! <3
5
292005
2015-07-06 13:09:02
--------------------------
78847
9660
Chia tay cái thời ngô nghê khờ dại,ta bước vào trang mới-mà người ta vẫn gọi là "teen".Mặc dù ai rồi cũng phải qua cái thời tẻ tuổi nhưng không phải ai cũng có thể chuẩn bị cho mình một hành trang vững chắc để bước lên con đường ấy.Sự lựa chọn người bạn đồng hành của tôi là Blog Thành Công Của Teen.Bởi đó là cuốn sách thật sự hữu ích cho những ai bước vào teen.Cuốn sách giúp tôi hiểu được giá trị cuộc sống,có niềm tin và hi vọng,quyết đoán và biết dấn thân.Điều đó đã hình thành nên tính cách của một con người đến với ngưỡng trưởng thành.Những bài học đơn giản mà sâu sắc đã khiến mỗi chúng ta khi trải nghiện trên từng trang viết thực sự thích thú và hài lòng.
4
121947
2013-06-03 21:10:55
--------------------------
