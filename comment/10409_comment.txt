342763
10409
Chủ đề của cuốn sách về khoa học, có lối viết và nội dung khá phức tạp, người đọc muốn đọc và thẩm thấu hết nội dung thì về cơ bản cũng cần phải có kiến thức một chút xíu để có thể hiểu. Tuy nhiên cuốn sách cũng khá phổ thông và không quá kén người đọc.
Nhận xét của mình thấy các vấn đề được đề cập tới khá thú vị, tiếp cận và  lý giải khá hợp lý, dễ tiếp nhận.
Về chất lượng giấy cũng như nội dung sách thì không hiểu vì sao, sách của mình giấy nhiều trang bị in lỗi, đè, nhăn rất phản cảm. Sách không dày lắm nhưng số trang bị như vậy cũng đến cả chục trang làm mình khá khó chịu.
3
155788
2015-11-25 00:07:56
--------------------------
239760
10409
Là một cuốn sách khoa học nhưng có sức lôi cuốn đặc biệt bởi ngôn ngữ, văn phong và cấu trúc không tuân theo yêu cầu của những văn bản khoa học, có lẽ đó là lý do khiến "Một triệu năm sắp tới" nói riêng và những vấn đề khoa học  nói chung đến được với người đọc một cách dễ dàng và thú vị nhất. 
Tôi đặc biệt thú vị với chương "Dân số", "Con người - một động vật hoang dã" và "Mưu cầu hạnh phúc". Cách tiếp cận và lí giải vấn đề của nhân loại trong những chương ấy rất gần gũi, dễ đọc, dễ hiểu và khiến tôi dễ dàng liên tưởng đến nhiều vấn đề khác của nhân loại và trong cuộc sống của chính mình 
5
401570
2015-07-24 08:19:05
--------------------------
