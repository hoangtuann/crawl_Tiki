494272
10634
Khá giống với 1 lít nước mắt nhưng hay hơn và đẫm lệ hơn. Những dòng tâm sự của 1 cô gái còn rất trẻ (15 tuổi ) sẽ khiến tâm hồn bạn đổ lệ.
5
1943608
2016-12-05 23:56:18
--------------------------
483921
10634
Đọc cuốn sách này, tôi liên tưởng tới ngay Một lít nước mắt, bởi sự tương đồng giữa những dòng nhật kí đáng yêu của hai cô gái mới lớn, những suy tư, trăn trở và cả những điều diễn ra hàng ngày rất đỗi bình thường, giàn dị. Nếu như Aya Kito phải đối diện với căn bệnh quái ác, một bất hạnh do tự nhiên thì Anne Frank lại trở thành một trong những vô vàn nạn nhân của chiến tranh, điều chính do bàn tay con người gây ra. Và trong cuộc chiến vô nghĩa đó, những điều thật khủng khiếp đã diễn ra, đẩy Anne và gia đình vào những tình thế hiểm nghèo. Mỗi một lời văn đều lột tả hết sự tàn bạo của chiến tranh và sự vô giá của hòa bình. Tuy rằng hoàn cảnh khắc nghiệt đến đâu thì tinh thần lạc quan của Anne chính là ngọn nến hi vọng soi sáng trong bóng tối của đau thương, buồn khổ. Một cuốn sách tuyệt vời và đáng được trân trọng, dù ở thời đại nào đi chăng nữa.
5
258584
2016-09-18 19:38:39
--------------------------
480386
10634
400 trang mà tôi chỉ đọc mất 5 giờ đồng hồ (bình thường tôi đọc chậm hơn nhiều). 
Cũng khá giống với Một lít nước mắt của Kito Aya, với hình thức một quyển nhật ký như thế này, và cũng cho thấy sự tuyệt vọng khốn khổ của một con người bị đánh mất tự do.
Tuy nhiên, quyển sách này đậm tính nhân văn, ẩn chứa cái nhìn sâu sắc về các khía cạnh con người đến từ một cô bé 15 tuổi đang bị giam cầm bởi sự phân biệt chủng tộc đáng lên án của Đức quốc xã. Tuy vẫn còn 1 ít chủ quan, 1 ít đỏng đảnh của con gái mới lớn, nhưng từng trang giấy, từng chữ viết đều được tái hiện chân thật, sống động nhất có thể, bất kể hoàn cảnh ngặt nghèo thế nào, đói khổ ra sao, Anne vẫn duy trì ngòi bút của mình, mặc dù cô không muốn ai đọc nó, cô vẫn viết với những cảm xúc mãnh liệt nhất. 
Tôi gần như đã khóc ở những trang giấy cuối cùng, những trang giấy tiếc thương cho số phận một con người trẻ tuổi đầy hoài bão và tràn đầy nhựa sống... 

5
47187
2016-08-17 15:19:58
--------------------------
460543
10634
Cảm động đến những trang cuối cùng của nhật ký. Thấy được sự tàn bạo của phát xít. Nhật ký Anne Frank là một bằng chứng sống đã tố cáo tội ác khủng khiếp của Hitler. Anne là một cô bé thông minh, láu lỉnh và khá bướng bỉnh, cuộc sống hàng ngày sống cùng với những con người mà mỗi người 1 tính cách trong một nơi trật trội nhưng vẫn phải chịu đựng và chấp nhận. Có bé 12 tuổi với những lời văn đanh thép và rất thật và hồn nhiên của một đứa trẻ, nhiều đoạn mô tả về những người hàng xóm "thân thiết" mà phải bật cười. Sự sống động của cuốn nhật ký này vẫn y nguyên, sức mạnh của nó vẫn tồn tại trên những trang giấy. Hiện thực của chiến tranh thật sự quá tàn khốc.
5
309333
2016-06-27 08:58:00
--------------------------
434414
10634
Nếu bàn luận về cuốn sách này thì thật sự không có gì để nói hay bàn luận,phê bình gì cả vì sở dĩ quyển sách này đã quá nổi tiếng rồi.Chi tiết và nội dung thì thật sự không chê vào đâu được,như vậy mới xứng đáng nằm vào danh sách bán chạy thuộc hàng kinh điển của thế giới.Tôi dám chắc là nếu như bạn chỉ đọc sơ qua và không chú ý đến tên tác giả thì bạn sẽ không thể nào ngờ được đây là một quyển nhật kí chân tình,giản dị nhưng hay đến nổi ta tưởng chừng đây là quyển sách của một nhà văn lừng danh nhiều kinh nghiệm.Thật sự bổ ích và đây là quyển sách đã làm nên tên tuổi của một bé gái 13 tuổi.Phải,một cô bé 13 tuổi-thật đáng khâm phục!
5
999425
2016-05-23 09:09:03
--------------------------
394189
10634
Đúng như nhận xét trên trang bìa cuốn sách đây là một cuốn sách thật sự đặc biệt. Đọc nó mình càng thêm hiểu hơn về sự khắc nghiệt và tàn bạo của chiến tranh và về tinh thần của  loài người. Và hơn nữa mình cũng thấy Anne Frank có điểm gì đó rất giống mình một cô gái nhạy cảm và cô đơn. Cuốn nhật ký của Anne cũng cho tôi nhiều bài học đáng suy ngẫm. Cô gái Do Thái ấy thật dũng cảm và tôi ngưỡng mộ cô gái ấy rất nhiều_ một cô bé mười ba tuổi đã phải bỏ trốn và sống bí mật và chết khi  chưa đầy 16 tuổi.
5
580169
2016-03-09 23:09:22
--------------------------
390602
10634
Trong “Bộ quần áo mới của hoàng đế”, chẳng ai dám nói mình không nhìn thấy bộ quần áo của hoàng đế, vì sợ bị cho là kẻ ngu ngốc. Cũng tương tự như vậy, chẳng ai dám chê những cuốn nhật-ký-của-người-chết như Nhật ký Anne Frank, Nhật ký Đặng Thùy Trâm, Một lít nước mắt v.v. là dở, vì sợ bị cho là kẻ vô cảm, độc ác, tàn nhẫn, máu lạnh!?
Tôi không nói rằng những cuốn sách ấy không có chút giá trị gì, nhưng coi chúng như thể là những cuốn sách vĩ đại, thậm chí vĩ đại nhất mọi thời đại thì… thật là quá đáng.
1
293599
2016-03-04 09:12:09
--------------------------
388208
10634
Thật sự khi quyết định mua quyển sách này mình chưa hề đọc trước tóm tắt nội dung hay bất kì nhận xét nào về nó, chắc có lẽ vì thể loại hồi kí hay nhật kí không phải là thể loại sách mà mình quan tâm. Nhưng quyển sách này đã thay đổi suy nghĩ của mình. Những trang đầu khá nhàm chán vì chỉ là những trang nhật kí ghi lại những cảm xúc của một cô bé đang tuổi dậy thì như bao cô bé khác, nhưng càng về sau, tình cảnh chiến tranh và nạn diệt chủng đã đưa cuộc đời của Anne bước vào một giai đoạn tàn khốc. Thực sự họ là những con người mạnh mẽ, đặc biệt là Anne, rất thông minh và tài năng. Nhưng tiếc thay số phận của họ quá ngắn ngủi...Đọc để thấy được sự thật ác nghiệt của chiến tranh, sự đồng cảm với những con người trong tình cảnh đó, để thấy được tình người thật ấm áp...
5
22978
2016-02-28 13:23:07
--------------------------
384347
10634
Về hình thức: bìa đơn giản, tạo nên vẻ đẹp về hình thức khiến người đọc tò mò. Kích thước vừa vặn, khi cầm đọc rất chất tay, tạo cho người đọc vẻ thoải mái.
Về nội dung: Nhật Ký Anna Frank đã làm cho tôi lặng và bất chợt mường tượng đến một cô gái vô cùng hài hước, trong sáng, rất đỗi bình thường như bao cô gái khác, cô có tình bạn, tình yêu, gia đình và người thân. Nhưng, tất cả lại diễn ra trong thời chiến. Những dòng nhật ký trong Nhật Ký Anna Frank là những ghi chép thường nhật về cuộc sống và cách cô cảm nhận về thế giới khách quan cũng như về những quan điểm, sự việc tàn ác do chiến tranh mà cô chứng kiến. Mà nó còn lên án một sự thật rùng rợn, tội lỗi do chiến tranh, hậu quả, kết thúc. 
Về giá: rẩt rẻ.
5
1106939
2016-02-22 10:34:19
--------------------------
380630
10634
Tôi lại tìm được những giây phút tuổi thơ khi đọc tác phẩm này
Cuốn sách là nhật ký của bà Anna lúc bà mới 14 tuổi. Được viết trong vòng 2 năm từ ngày 14 tháng 6 năm 1942 kể về suy nghĩ, những chuyện thường nhật dưới ngôi nhà bí mật tại Hà Lan, câu chuyện tình yêu, suy nghĩ của tuổi dậy thì, hiểu tính cách của bố mẹ theo những hướng hoàn toàn ngây thơ. 
Kết thúc cuốn nhật ký là ngày 1 tháng 8 năm 1944 khi bà và gia đình bị bắt đi dưới thời Hittler. 
Sẽ là thiếu xót lớn nếu bạn không đọc
" Xúc động đến nghẹn lòng. Tôi đã tìm được trong nhật ký của bà đâu đó tuổi thơ tôi. Những cảm xúc yêu thương đầu đời cho tới những suy nghĩ từ việc người khác đối xử với mình. Ôi! cuộc đời" - Tôi đã ghi cảm nhận như vậy trong cuốn sách

Đánh giá giấy in và bản dịch: Tốt
5
919665
2016-02-15 14:23:43
--------------------------
370463
10634
Cuốn Nhật ký này thì nổi tiếng rồi, khỏi bàn. Cái tôi muốn nói đến là chất lượng bản Việt.
Chính tả tiếng Việt sai lỗ chỗ, chắc biên tập nội dung làm ẩu, tên địa danh gõ sai như Frankfurt thì thành Frrankort, nếu là do Anne viết sai thì có thể chú thích cho người đọc hiểu.
Dịch giả có vẻ lười tìm hiểu và chú thích, dường như chỉ dịch để vậy thôi. Tôi vừa đọc như phải gõ Google: SS trong này là gì? Đơn vị đo lường "bảng" của Hà Lan bằng nhiêu gram? v.v...
Tôi thấy thất vọng khi cầm bản dịch trên tay, dù quan điểm của tôi luôn là Ủng hộ sách bản quyền, nhưng bỏ tiền ra mua bản dịch không ok lắm thế này thì cũng tiếc
2
1116442
2016-01-17 22:11:03
--------------------------
363989
10634
Dù có bao nhiêu chuyện xảy ra nhưng Anne vẫn luôn lạc quan và khao khát tìm được niềm vui cho mình. Tôi như thấy mình đồng cảm với Anne qua một vài điểm mà cô ấy viết trong nhật kí, một vài điểm đôi khi tôi rất bực vì cách suy nghĩ nông cạn của Anne và vỡ òa khi biết cô ấy thay đổi từng ngày rồi cho tôi nhiều bài học từ một cô gái mới lớn đã trải qua rất nhiều câu chuyện...Quyển nhật ký rất đáng đọc, đọc để hiểu tâm tư của một cô bé, một đại diện của khao khát hòa bình và tìm kiếm sự cảm thông, đồng cảm của những con người chung sống với nhau
4
88923
2016-01-05 11:56:38
--------------------------
354972
10634
Có những thứ quá đổi bình thường lại hoá khác biệt.
Một cuốn nhật ký với nhiều điều được một cô gái bộc bạch qua trang giấy để nói về mỗi ngày trong chuỗi sự sống của mình nhưng lại chứa đựng nhiều diêud hấp dẫn.
Giữa thế chiến, với những thứ tàn khốc huỷ hoại mọi thứ ấy thế mà Anne một cô bé nhỏ đầy hồn nhiên cứ nhìn rồi lại viết lên mọ thứ. Khốc liệt có, đau buồn có nhưng đâu đó như vẫn tồn tại sự vui tưoi có phần lạc quan yêu đời của một cô gái ở tuổi mới lớn.
4
809815
2015-12-18 22:54:46
--------------------------
354792
10634
Mình biết đến cuốn sách này vì nó hay được so sánh với Nhật kí Đặng Thùy Trâm. Đọc những dòng chân thật được viết bới một cô bé gái là nạn nhân của chiến tranh mình mới thấy rõ hết được sự tàn khốc và cái cách mà chiến tranh giết chết cuộc sống của con người ta. 
Cái mà mình thích còn là ở lời bạt cuối cuốn nhật kí và kể tiếp về cuộc đời sau đó của Anne và các thành viên khác trong gia đình. 
Bìa sách rất đẹp, bố cục trình bày sách cũng rất hay.
Còn để so sánh ra thì cũng khá khập khiễng nhưng cá nhân mình thì mình thấy đọc cuốn Nhật kí Đặng Thùy Trâm mình có gì đó đồng cảm hơn nhiều. Không phải do mình là người Việt mà mình nói vậy. Mà là bởi chiến trường hiện lên rõ nét hơn bao giờ hết, và tâm tư tình cảm của người Việt thì hẳn là phải có nét gì đó tương đồng.
Nếu đã đọc cuốn Nhật kí Anna Frank này thì nên đọc cả Nhật kí Đặng Thùy Trâm và ngược lại. Hai cuốn sách này sẽ cho chúng ta những bài học về nhiều điều trong cuộc sống tùy vào cách cảm nhận của mỗi người.
3
919635
2015-12-18 17:56:24
--------------------------
331730
10634
Tôi đã tìm kiếm cuốn sách này từ rất lâu, và mừng rơn khi vô tình thấy nó xuất hiện trên tiki nên đã đặt mua ngay.
Đọc "Nhật kí của Anne Frank", nếu không biết về cô gái này có lẽ tôi không thể tin được đó là những dòng nhật kí của một cô gái mới 13 tuổi. Những ghi chép của cô về cuộc sống bí mật đầy hiểm nguy và khó khăn, nhưng lại đầy vui tươi, với những tình cảm trong sáng, những suy nghĩ lạc quan, những cách nhìn hài hước làm tôi vô cùng xúc động và nể phục. Tôi có thể hình dung ra được một cô gái Anne nhỏ nhắn, thông minh, tinh nghịch và nhạy cảm. Đáng lẽ ra cô gái ấy có thể phát triển được hết tài năng của mình nếu được sống trong hòa bình và tự do. Thế nhưng cuộc chiến đã đẩy cô gái ấy và cả gia đình vào một cuộc sống đầy khắc nghiệt, phải tự tách biệt mình với thế giới bên ngoài. Thế nhưng điều ấy không ngăn được tinh thần hướng sáng, hướng thiện của cô gái Anne. 2 năm phải sống trong bí mật ấy, với Anne vẫn là những ngày tháng đầy lạc quan bên gia đình, bên bố mẹ và chị gái. Tinh thần của cô như một ngọn lửa duy trì niềm tin cho những người trong gia đình, và là mình chứng cho tội ác của phát xít Đức. Và ngọn lửa ấy vẫn cháy đến tận bây giờ để đốt lên tinh thần lạc quan, ý chí bền bỉ trong những độc giả của cô.
5
551313
2015-11-05 00:16:45
--------------------------
325130
10634
Nếu bạn muốn biết cuộc sống của một người Do Thái trong chiến tranh thế giới thứ hai dưới sự bành trướng của Đức Quốc Xã và cuộc diệt chủng Holocaust là như thế nào thì bạn nên đọc cuốn sách này.
Bên cạnh đó thì bạn cũng nên đọc thêm một cuốn sách khác nữa, cũng nói về chiến tranh thế giới thứ hai, nhưng bối cảnh là ở nước Đức, phố Munic - đất nước của chính  Adolf Hitler, đó là Kẻ trộm sách. Tôi đã đọc hai cuốn này - Kẻ trộm sách và Nhật Ký Anne Frank, và sau đó tôi đã quyết định tìm đọc cuốn Adolf Hitler - chân dung một trùm phát xít  của John Tolan, và hiểu hơn nữa về cuộc sống địa ngục của dân Do Thái thời đó. 
3
220900
2015-10-22 21:14:31
--------------------------
308943
10634
Một trong những cuốn nhật ký kinh điển , cuốn nhật ký của  cô bé  người Do Thái Anne Frank chỉ mới 13 tuổi  viết trong một hoàn cảnh khó khăn và bất lợi. Anne Frank viết về những điều giản dị xung quanh cuộc sống của mình và gia đình ,cô viết về những cuộc tranh luận, về thức ăn,những suy nghĩ, những rung động của bản thân, về những nỗi lo lắng, sợ hãi,về tình hình chiến sự  và về mỗi con người trong cái “Chái nhà bí mật” nơi cô và gia đình đang ở để trốn tránh sự truy bắt của Đức quốc xã trong chiến dịch bài trừ người Do Thái. Tuy rằng cô mất khi mới 16 tuổi nhưng cô đã để lại một tượng đài về nghị lực và lòng dũng cảm.
5
28807
2015-09-18 21:40:32
--------------------------
302802
10634
Đây là một cuốn sách rất hay mà mọi người nên đọc, sách nằm trong danh sách mười cuốn sách được đọc nhiều nhất trên thế giới. Cuốn sách đã chia sẻ về thế giới quan của một cô bé sống giữa thời chiến đầy chân thật và xúc động, đi xuyên cả cuốn sách bạn sẽ cảm nhận những nỗi khốn khổ đau thương của cô bé Anne, tuy còn nhỏ nhưng Anne đã có một trái tim mạnh mẽ kiên cường, vẫn lạc quan về cuộc sống ngày mai sẽ tốt đẹp hơn hôm nay.Đây là một tác phẩm nên đọc.
5
471238
2015-09-15 14:39:00
--------------------------
301325
10634
Tôi đã phải chờ rất lâu mới có thể sở hữu cuốn sách đặc biệt này, vì cứ luôn trong tình trạng không còn hàng. Và cuốn sách thì vượt qua kỳ vọng của tôi về một cuốn nhật ký. Nó thật sự đặc biệt và gây tác động rất lớn tới tôi. Những trải nghiệm của cô gái nhỏ bé nhưng khát vọng sống luôn rực lên trong trái tim cô giữa những hiểm nguy luôn rình rập làm cho cô trở nên vĩ đại thật sự. Bản thân cuốn sách và yếu tố lịch sử của nó làm người ta cảm thấy rùng mình và cảm phục những con người dũng cảm không chịu khuất phục trước tội ác. Cuốn sách đã trở thành kinh thánh sống của nhiều người, chỉ đơn giản với những khát vọng mà nó thể hiện về khao khát được sống, được yêu thương một cách đúng nghĩa. Với tôi, đây là cuốn sách đặc biệt nên trân trọng và phải đọc ít nhất một lần trong đời. Tôi cảm thấy rất biết ơn…
5
370558
2015-09-14 16:27:25
--------------------------
297958
10634
Cuốn sách là tập hợp những trang nhật ký của Anne Frank - cô bé mười ba tuổi người Do Thái cùng gia đình trốn chạy và phải sống lén lút trong căn áp mái để tránh nạn bài Do Thái của Đức Quốc xã. Lịch sử được tái hiện dưới cái nhìn của cô bé 13 tuổi một cách chân thực và khốc liệt. Anne Frank là một nhân vật, một nhân chứng của những tháng năm chiến tranh và diệt chủng khốc liệt ấy. Tuy phải sống một cuộc sống lén lút, mất mát, đau thương, ngoài kia bủa vây bởi chết chóc, nhưng cô bé vẫn không thôi lạc quan và hy vọng về một ngày tươi sáng. Xuyên suốt câu chuyện là lời tự sự, lời tâm sự về lòng nhân ái và tình yêu thương. Bên cạnh giá trị lịch sử, đây còn là cuốn sách để nuôi dưỡng tâm hồn.
5
24486
2015-09-12 12:10:54
--------------------------
283987
10634
Mình biết đến Nhật ký Anne Frank qua bộ phim The fault in our stars (đoạn Hazel vào thăm ngôi nhà Anne Frank đó). Đây quả thực là một cuốn sách kinh điển, sách là những trang nhật ký của cô gái 13 tuổi Anne Frank về cuộc sống thường ngày của cô cùng những cảm xúc không thể nói với ai trong lòng cô gái, tất cả những điều đó đã được viết lên những trang sách và làm nên một cuốn sách vô cùng đặc biệt. Nhật ký Anne Frank chi mình thấy những suy tư, mơ mộng rất con gái của Anne như bao cô gái tuổi mới lớn bình thường khác, nhưng cũng là nơi cô gái viết nên những suy nghĩ, cảm xúc thật sâu sắc, chững chạc của mình. Càng đọc sách mình càng thấy Anne là một cô gái đặc biệt, và mình không thể tin nổi những câu chữ ấy thực sự là của một cô gái tuổi mới lớn. 
Sách dịch cũng được, nội dung hay, đây là một cuốn sách mọi người nên đọc để hiểu thêm về những năm tháng tàn khốc mà con người phải trải qua (Chiến tranh thế giới thứ hai) và để ngợi ca những con người tốt bụng, giàu lòng nhân ái, đã chấp nhận mạo hiểm mà bao bọc cho gia đình Frank suốt hai năm trời cho đến khi tất cả bảy người trong Chái nhà bí mật bị Gestapo bắt đi
5
415536
2015-08-30 15:15:13
--------------------------
283020
10634
Sau khi biết được cuốn sách này nằm trong top 10 cuốn sách mà ai cũng nên đọc qua một lần trong đời, mình cũng tò mò và đã mua về. Cuốn sách này chứa đựng rất nhiều bài học qua cuộc sống trong chiến tranh của gia đình Frank. Dù chiến tranh có khốc liệt, có tàn nhẫn như thế nào thì nó cũng không thể nào giết chết được tâm hồn mạnh mẽ, trong sáng của Anne cùng khát vọng sống mãnh liệt của cô. Sau khi đọc cuốn sách này, mình mới hiểu tại sao đây là cuốn sách được khuyên đọc. Thấm đẫm tính nhân văn, giá trị hiện thực. Sách in khá tốt, đây là một sản phẩm khá tốt.
5
548542
2015-08-29 17:15:15
--------------------------
272490
10634
Mình biết đến Nhật ký Anne Frank sau khi đọc xong danh sách 10 cuốn sách được đọc nhiều nhất thế giới. Phải có điều gì đó đặc biệt lắm mới khiến Nhật ký Anne Frank thu hút sự chú ý của cả thế giới. Và sau khi đọc xong thì mình mới hiểu vì sao. Cuốn nhật ký này thể hiện sự tàn bạo và những đau thương mất mát do chiến tranh gây ra, nhưng bên cạnh đó cũng thể hiện tinh thần mạnh mẽ của một cô gái luôn hy vọng về một tương lai tự do và hạnh phúc. Điều mình không thích về cuốn sách là chất lượng giấy, nhiều trang in rất tối, khó nhìn, nhất là những trang có ảnh chụp.
4
307488
2015-08-20 08:56:08
--------------------------
246597
10634
Quyển nhật ký đã mang chúng ta đến với thế giới nội tâm đầy phức tạp của cô bé Anne Frank trong quãng thời gian gia đình cô phải chạy trốn sự truy bắt của Đức Quốc xã và cuộc sống đầy tăm tối, đáng sợ dưới "Chái nhà bí mật". Ở nơi đó, tuy lúc nào cũng trong tâm trạng lo âu, thấp thỏm, đôi lúc tâm trí bị nỗi buồn lấn át, thậm chí là bị đe dọa bởi cái chết nhưng Anne vẫn giữ được nét vô tư, trong sáng của một cô bé mười ba tuổi và luôn nhìn cuộc đời qua ánh mắt lạc quan, tràn đầy khát vọng sống và tình yêu. Quyển sách cho ta thấy sự lạnh lùng, tàn ác của chiến tranh, đã trút bao đau khổ, bất hạnh lên những con người vô tội và nhấn chìm họ vào cảnh mất mát, lầm than. Thế nhưng, đâu đó giữa cuộc chiến khốc liệt vẫn sáng lên những tinh thần thật mạnh mẽ, dũng cảm và thông minh, đó không chỉ là cô bé Anne Frank mà còn là vô số con người luôn ấp ủ trong trái tim mình khát vọng sống tự do và hạnh phúc. 
5
387532
2015-07-29 18:17:10
--------------------------
246373
10634
Rất khó khăn tôi mới mua được cuốn Nhật ký Anne Frank nên vừa nhận được nó là tôi cắm đầu cắm cổ vào đọc ngay tức khắc. Và tôi không thể không thừa nhận rằng đây quả là một cuốn nhật ký phi thường. Làm thế nào mà một cô bé mới mười ba tuổi lại có thể viết ra những dòng chữ giản dị mà lại sâu sắc và lay động tâm can đến thế. Điều đáng quý là dù ở trong những giờ phút tăm tối nhất thì Anne Frank cũng chưa bao giờ đánh mất hy vọng. Cả cuốn nhật ký là những ước vọng tha thiết và đẹp đẽ nhất của một cô bé yêu hoà bình và căm ghét chiến tranh.
5
167283
2015-07-29 17:58:14
--------------------------
235521
10634
Anne, một cô bé 13 tuổi nhưng từ suy nghĩ đến hành động, cách hành xử của cô bé đều rất cá tính, dứt khoát và có phần lạ lùng so với lứa tuổi của cô bé. Đôi khi người đọc sẽ thấy giọng văn của cô bé như "bất cần" hòa quyện trong tính cách ngây thơ, trong sáng. Anne coi cuốn Nhật ký như một người bạn thân, lắng nghe tất cả tâm tư, niềm vui, nỗi buồn, nỗi oan ức trẻ con của cô bé. Đọc tác phẩm này mình mới thấy cuộc sống của Anne và các gia đình Do Thái quá sức khổ cực, hiểm nguy thì luôn cận kề. Nhưng ở họ vẫn toát nên sự lạc quan, niềm tin mạnh mẽ vào một thế giới hòa bình, không còn chiến tranh, không còn nạn phân biệt chủng tộc.
4
29775
2015-07-21 09:43:49
--------------------------
230262
10634
Anne chỉ là một cô bé mới 13 tuổi, chuẩn bị làm người lớn và có suy nghĩ riêng của mình. Quyển nhật kí đã miêu tả rất sâu sắc cảm nhận của một cô bé mới lớn về một cuộc sống chui lủi, mất tự do. Đọc quyển sách này, ta mới thực sự hiểu được nạn phân biệt chủng tộc tàn bạo dưới thời Đức quốc xã.  Tôi thực sự cảm phục về lạc quan sống của cô gái bé nhỏ đó. Mặc dù đã mượn đọc cuốn nhật kí này từ rất lâu rồi nhưng tôi vẫn quyết định mua lại nó để bổ sung vào tủ sách của mình.
5
477402
2015-07-17 13:01:40
--------------------------
227576
10634
Anne là người một cô gái kiêu hãnh. 
Em mới chỉ 13 tuổi khi viết những trang nhật kí ấy, còn tôi, một người đọc 19 tuổi tự thấy mình không thể so sánh với em. Cách suy nghĩ của em rất trưởng thành và chân thật.  
Anne trong hình dung của tôi là một cô gái quyết liệt, yêu ghét rõ ràng, luôn chắc chắn về bản thân và luôn nỗ lực vượt qua chính mình. Cô bé Do Thái ấy là một minh chứng chân thực cho những con người Do Thái với những phẩm chất đẹp đẽ và đáng khâm phục như bất cứ dân tộc nào trên thế giới này.
Khi ta đọc Anne Frank, thấu hiểu những tâm tư của cô gái sinh ra trong thời chiến, sinh ra trong sự ruồng rẫy của người đời với gia đình em, những người mang dòng máu  Do Thái như em, ta sẽ hiểu sẽ thông cảm, sẽ cùng họ lên án chiến tranh và nhận ra điều kì diệu mang tên Hòa Bình mà chúng ta đang sống.  Ta sẽ xót thương cho em mãi, ngay đã khi gấp cuốn nhật kí lại.
Cô gái Do Thái sinh đẹp và kiêu hãnh ấy, tâm hồn trẻ trung biết yêu thương, rung động của em hay chính là bản tố cáo đanh thép cho một giai đoạn lịch sử khó khăn và đầy rẫy bất công của nhân loại mang tên: Phát xít
5
104493
2015-07-14 13:22:16
--------------------------
223364
10634
Tuy còn nhỏ nhưng cô bé Anna đã có thể miêu tả rất chân thật về cuộc sống trốn chạy của người Do Thái dưới sự truy bắt của bọn Pháp xít Đức. Mặc dù có thể thấy là cô bé đang ở trong giai đoạn tuổi mới lớn có những suy nghĩ trầu tre, nhưng đâu đó trong tiềm thức cô bé có đc những sngi mà chắc gì người lớn có thể hiểu được. Kết thúc của cuốn nhật ký khiến mình phải sngi rằng Anna đã biết trước được cái chết đang tới với cô bé chăng? 
5
540471
2015-07-06 22:53:15
--------------------------
220635
10634
Thật sự khi đọc xong cuốn này tôi đã khóc. Có lẽ khi nhắc đến A. Hitler người ta sẽ nhớ đến cuộc diệt chủng người Do Thái tàn bạo của ông, tôi vẫn nghe thấy là rất kinh khủng nhưng đến khi đọc cuốn sách này tôi mới thật sự biết đến cái tàn bạo đó. Chiến tranh thật tàn khốc khiến cho nhiều người chịu sự bất hạnh trong đó có cô bé Anne. Cô bé là một người thông minh, dũng cảm, quan sát tinh tế đã ghi lại toàn cảnh xã hội thời đó. Qua từng ngòi bút chúng ra hình dung rõ hơn về chiến tranh và quý trọng thời khắc chúng ta đang sống.
5
328024
2015-07-02 20:31:48
--------------------------
208611
10634
Tôi là thế hệ trẻ 9X và tôi là một trong hàng trăm triệu đứa trẻ sinh ra vô cùng may mắn trong một đất nước khi mà tiếng bom đã lùi xa hơn 40 năm.Tôi rất khâm phục trí tuệ người Do thái,ở nhà cũng có mấy quyển tìm hiểu về họ và cũng thương xót cho những con người Do thái bị tàn sát trong chế độ của Hitler bạo tàn.Đặc biệt là cuốn Đi tìm lẽ sống của tiến sĩ Victokl Frank mà tôi đã mua,và tôi đã khóc rất nhiều.Và với cuốn sách này,mà tác giả là một cô bé 13 tuổi ngây thơ nhưng rất mạnh mẽ kiên cường đến cùng sự sống.Cuốn nhật ký đã ám ảnh không ít độc giả vì thấy thương thật nhiều cho Anne Frank khi tuổi thơ của cô bé phải chịu đựng sự khốc liệt của chiến tranh.
5
456366
2015-06-15 15:29:10
--------------------------
203076
10634
Đây là quyển nhật kí để lại trong tôi nhiều suy ngẫm nhất, đầy ám ảnh về hiện thực xã hội cũng như con người. Anne Frank đã thể hiện được những suy nghĩ, tâm tư, tình cảm của mình thông qua những dòng tự sự kể lại cuộc đời của em - 1 cô gái giàu lòng yêu thương, can đảm và mạnh mẽ ngay trong những giờ phút sinh tử khốc liệt nhất. Thật đáng cảm phục! Tác phẩm "Nhật ký Anne Frank" là một bài học về sự kiên cường, ý chí nghị lực cho các bạn trẻ. Tuy viết trong hoàn cảnh lịch sử, nhưng giá trị của nó còn sống mãi đến tận bây giờ.
4
131327
2015-05-31 10:49:33
--------------------------
199135
10634
Anna có lẽ phải được trao tặng huân chương Anh Dũng khi sống giữa không gian nguy hiểm, thiếu thốn đủ bề nhưng sự yêu đời, tự tin, lạc quan không bao giờ mất đi trong cô. Những trang nhật ký hết sức mượt mà đến mức mình nghĩ sẽ phải mua bản tiếng Anh để học cách viết giản dị nhưng đầy sắc màu này. Cô bé chỉ 13 tuổi nhưng có nhiều cái nhìn rất sâu sắc về cuộc sống, có lẽ do hoàn cảnh đưa đẩy. Trái tim đầy tình yêu thương, cũng biết yêu ghét khiến một khi đã đọc thì mắt không thể rời khỏi bất cứ chữ nào trong cuốn hồi ký nữa.
5
162924
2015-05-21 16:20:27
--------------------------
158765
10634
Một cuốn nhật ký của cô bé giá trạc tuổi tôi nhưng trong từng câu chữ và lời văn của Anne lại mang sự vô tư hồn nhiên đến bất ngờ trong cái hoàn cảnh đó. Đọc đến đoạn cuối tôi đã khóc rất nhiều. Cớ sao một đứa trẻ lại phải chịu cuộc sống như vậy vì nó là người Do Thái, vì nó sinh ra trong cái thế giới đó? 
Sự ám ảnh của chiến tranh không thể rõ hơn được khi đọc cuốn sách này. Nó  phản ảnh tội ác của những kẻ độc tài, phản ánh cái sự bất công mà những đứa trẻ ở thời kì này phải chịu đựng. 
Đọc mỗi trang sách mà cảm thấy mình thực sự vô cùng may mắn vì không phải sinh ra trong cái thời kì kinh khủng đó.
5
531559
2015-02-13 21:31:15
--------------------------
157658
10634
Đây quả là một cuốn sách đặc biệt bởi lẽ bản thân cuốn nhật kí là một minh chứng rõ nhất cho chiến tranh và cuộc sống khó khăn của người Do Thái.Anne là một cô gái thật sự rất tài năng và khi đọc bạn sẽ thấy cô bé ấy trưởng thành qua từng trang nhật kí ấy.Cô ây đã bộc lộ con người thể hiện suy nghĩ của chính mình qua những trang giấy đó để ra nó trổ thành một bằng chứng chẳng thể chối cãi về tội ác của chiến tranh.
Từng trang nhật kí chúng ta lại càng hiểu hơn về cuộc sống khi phải sống chui rúc trong gần 2 năm cuối đời của cô.Những sự gò mò bứt rứt đều được người dịch dịch rất rõ ràng nên mình không có gì phải chê!!!!
Một tác phẩm làm thế giới cảm động.
5
406836
2015-02-09 20:38:31
--------------------------
109193
10634
Lúc đầu, tôi nghĩ rằng đây ắt hẳn phải là một cuốn sách viết về nội dung gì đó khá la "cao siêu" ! Nhưng ko, bất ngờ hơn cả bất ngờ, nó vô cùng nhẹ nhàng, cuộc sống ngày thường như bao ngày khác. Tuy nhiên là cuốn nhật ký được viết trong thời chiến, cái "nhật" ấy chắc chắn cũng phải khác với thời bình hiện nay. Sống trong hiểm nguy rình rập, cô bé ấy đã làm tôi bât ngờ hết lần này đến lần khác với tinh thần lạc quan hiếm có. Ko chịu thua số phận, Anne Frank chính là hình ảnh tiêu biểu cho phẩm chất đáng quý của người Do Thái, sự ham học hỏi còn thể hiện xuyên suốt tác phẩm. Bất kể hoàn cảnh nào, ý chí kiên cường ấy đã khiến bao thế hệ theo sau phải khâm phục. Đó là bài học mà có lẽ trong thời bình khó ai có thể học được.
5
33512
2014-03-28 21:40:47
--------------------------
90612
10634
Đọc của Đặng Thùy Trâm rồi lại đọc quyển này. Thực sự khác một trời vực. Tất nhiên cả hai quyển đều hay, nhưng một đứa con gái 16 tuổi trong thời bình như tôi, quyển Anne Frank còn là gì của vùng trời con gái. Là những cảm xúc vô tư, hồn nhiên, như những hạt bụi sáng lấp lánh lẩn khuất giữa màu đen ngút ngàn của bom lửa và khói đạn. Quyển nhật ký thực sự có giá trị nhắc nhở về hòa bình, là một chứng nhận lịch sử vĩ đại, là một tâm hồn đồng điệu dễ cảm thông dành cho các bạn trẻ và ai từng là bạn trẻ 
3
79845
2013-08-11 21:36:28
--------------------------
82802
10634
Tôi biết đến cuốn sách này khi xem nó trên mạng xã hội của một người quen. Trước đây, tôi đã đừng đọc "Nhật kí Đặng Thùy Trâm" nên tôi rất xúc động và ấn tượng với những gì gọi là"nhật kí" được ghi chép một cách chân thực từ cuộc đời, số phận của con người. Đọc "Nhật kí Anne Frank", tôi như thấy mình sống lại những cảm xúc khi đọc "Nhật ký đặng thùy trâm" : đồng cảm, thương xót, và hơn hết là khâm phục cách sống, suy nghĩ của những con người phi thường này. Cuốn sách đã khiến tôi cảm thấy phẫn nộ, bức xúc vì hậu quả do chiến trnah gây ra: con người không được sống một cách thoải mái, không thể theo đuổi những ước mơ và tình cảm, gia đình họ phải bị chia cắt bởi những thứ vô nghĩa như thế. Và tôi cũng trân trọng hơn cuộc sống mà mình đang được sống: một cuộc sống bình yên, không có chiến tranh. "Nhật ký anne frank" thật sự đã đem lại cho tôi những xúc cảm mãnh liệt qua những dòng chữ chân thật của Anne. Tôi thực sự thích cuốn sách này, đây sẽ là cuốn sách nhắc nhở cho tôi rất nhiều điều: về chiến tranh và hòa bình, về con người và về ước mơ
5
24380
2013-06-23 12:24:33
--------------------------
75342
10634
Lúc đầu đọc bản thân tôi thật sự không có chút cảm xúc nào cả. Thật đáng buồn là vậy. May mắn được sinh ra trong thời hòa bình, khi mà những dấu tích chiến tranh tôi biết đến dường như chỉ qua báo chí, ảnh và sách. Tất nhiên tôi cũng đã có cảm động nhưng để nói là bản thân đồng cảm với nỗi đau đó thì chưa. Đọc cuốn nhật kí trên , ban đầu tôi chỉ thấy được đó là những câu chuyện thường nhật, hết sức thường nhật của một cô thiếu nữ, để rồi một ngày tôi đọc lại và chợt nhận ra sự khủng khiếp của chế độ phát xít rõ nhất từ trước tới giờ và Anne Frank đã dũng cảm và kiên cường biết bao. Thế Gioi đã trải qua 2 cuộc chiến tranh lớn và biết bao cuộc chiến nhỏ khác nhưng dù lớn hay nhỏ, những đau thương và mất mát chẳng bao giờ là nhỏ cả. Cuốn sách như nhắc chúng ta nhớ nhân loại sợ chiến tranh như thế nào và ta cần làm những gì, đấu tranh vì điều gì!   
5
33999
2013-05-17 22:53:53
--------------------------
36437
10634
Mình biết đến cuốn sách này khi nó được nhắc đến trong phần giới thiệu mở đầu của "Nhật Ký Đặng Thùy Trâm", và thật tình cờ khi người dịch tp này là Đặng Kim Trâm, em gái của Thùy!
Đây là một cuốn nhật ký chứa đựng tâm hồn của 1 cô gái nhỏ bé, một tâm hồn lắm góc cạnh, bao la như bầu trời xanh, rực rỡ sắc màu như cầu vồng. Trong mỗi con chữ, cảm xúc được đong đầy, tưởng chừng như chỉ cần chạm nhẹ vào là chúng có thể tràn ra. Mỗi trang sách như một nốt nhạc du dương về tình yêu, cuộc sống, tuổi trẻ, về cả những nỗi buồn không nguôi trong lòng. Cuốn sách chân thật, đại diện cho cả một thời đại, và sống mãi trong lòng mọi người dù thời gian có đi qua bao lâu. Đọc xong cuốn nhật ký, Anne Frank như trở thành một người bạn thân thiết, một người bạn với sự đồng cảm sâu sắc!
Rất thích cuốn nhật ký này!
5
20073
2012-08-10 14:53:47
--------------------------
29991
10634
Tôi thích cuốn sách này. Không hiểu sao khi đọc những bức thư mà Anne viết cho Kitty, tôi thấy mình trong đó. Tôi thấy đồng cảm với những suy nghĩ, những tình cảm của Anne. Từ việc cô ấy cảm thấy không hòa hợp được với mẹ, sự cô đơn của cô ấy khi sống giữa gia đình, nhưng rung động đầu đời, những suy nghĩ, những thắc mắc của một cô bé đang tuổi dậy thì... tất cả, tôi như thấy chính suy nghĩ của bản thân trong đó.

Cô ấy không được hạnh phúc như tôi. Cuộc sống của cô ấy và những người mà cô yêu thương bị chiến tranh tàn phá. Có lẽ một người sống trong thời bình như tôi nói đồng cảm với cô thật khó. Nỗi đau và nỗi sợ mà chiến tranh mang lại cho cuộc sống của Anne, bản thân tôi khó mà tưởng tượng được. Nhưng trong những điều bình dị và nhỏ nhoi, tôi thấy mình thấu hiểu, thấy mình giống và thấy mình thành tri kỉ với Anne. 

Đây là một cuốn sách đáng đọc. Một cuốn sách của một cô gái với tâm hồn thật đẹp và một tài năng thật tuyệt vời. Thật buồn khi chiến tranh đã cướp cô ấy đi khỏi thế giới này.
5
35822
2012-06-10 22:41:23
--------------------------
12107
10634
Cuốn sách này tôi đã đọc được từ 3 năm về trước và còn cẩn thận viết những suy nghĩ của mình vào cuốn sách mỗi khi đọc xong.
Câu chuyện này thực ra là 1 chuỗi những hoạt động cực kỳ bình thường hàng ngày của 1 cô bé đang trong giai đoạn mới lớn. Nhưng bạn biết không, những hoạt động ngỡ như bình thường đó lại được đặt trong hoàn cảnh chiến tranh, khi mà sự đe dọa mạng sống của từng người cứ treo lơ lửng trên đầu họ.
Cuộc càn quét và tiêu diệt người Do Thái của phát xít Đức đã dồn những con người này vào 1 cuộc sống luôn - sẵn - sàng để chạy trốn. Từng người từng người một bị đánh dấu lên người để phân biệt và để truy kích.Họ không hơn gì những vật thí nghiệm mặc dù những điều họ làm chỉ là muốn sống cùng gia đình và người thân.
Thế nhưng, trong hoàn cảnh khủng khiếp ấy, cô bé Anne vẫn rất lạc quan về 1 tương lai mới. Những lời trong nhật ký của cô bé cứ tuôn ra đều đặn hàng ngày: về những rung động của tuổi mới lớn, về ba mẹ, về tình hình chiến sự, về những dự định học hành của cô bé....Điều mà tôi thích nhất, cảm phục nhất là trong những trang nhật ký ấy, dù cô bé có miêu tả tình hình khốc liệt và tiêu cực như thế nào đi chăng nữa thì cuối mỗi câu kết là 1 điều gì đó lạc quan hơn, tin tưởng vào 1 ngày được tự do cùng mọi người.
Nhưng rồi, những trang cuối cùng Anne lại được viết trong khu giam giữ những người Do Thái. Người ta chỉ biết là chỉ mấy ngày sau ngày cuối cùng được viết trên trang nhật ký, người ta cũng không còn thấy cô bé nữa.
Cuối cùng, những người Do Thái vẫn không tránh khói cuộc càn quét của phát xít Đức, cô bé Anne cũng vậy. Nhưng những trang nhật ký của cô bé vẫn còn sống mãi đến tận bây giờ.
Nếu bạn đang muốn có 1 động lực gì để sống hoặc làm việc, hay đơn giản là bạn muốn con mình làm quen với sách thì hãy tìm đến "Nhật ký Anne Frank" - lạc quan vào cuộc đời ở những giây phút cuối cùng.
4
9559
2011-10-02 12:11:53
--------------------------
