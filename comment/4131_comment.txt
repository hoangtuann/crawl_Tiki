464255
4131
Lúc đầu cũng không để ý đến cuốn này lắm . Nhưng có đợt vào nhà sách thì thấy "Mái ấm của Chi" , đọc ké thì thấy hay , thế là bắt đầu cày từ tập 1 . Phải nói là càng đọc thì càng thấy thích . Đây là tập truyện rất chân thực , kể về cuộc sống của Chi , rất đáng yêu và dễ thương . Tính cách của loài mèo nói chung và mỗi con mèo nói riêng được thể hiện đầy đủ qua từng nhân vật mèo trẻ con . Và đặc biệt mỗi quyển được tặng một tấm photo card nhỏ nhỏ xinh xinh , rất đẹp 
5
1118399
2016-06-30 00:06:29
--------------------------
407319
4131
Ôi !Sao mà Chi đáng yêu quá !Sau khi đọc câu chuyện này ,em thấy rất yêu những chú mèo vì vẻ đáng yêu ,sự tinh ngịch dóm dỉnh,có tâm hồn như con người.Em rất xúc động trước tình cảm giữa con người với động vật.Sau khi cho các bạn trong lớp mượn đọc,các bạn đều nhận xét là chuyện rất hay.Em rất cảm ơn tác giả Konami Kanata vì đã sáng tác ra loạt chuyện này!Nhưng những hình vẽ cần có sự đột phá về cách vẽ bìa sách rất đẹp nhưng cần thú vị và màu đậm hơn.
5
586686
2016-03-29 20:54:04
--------------------------
338368
4131
Chi vốn là chú mèo mà mình thích nhất, đặc biệt là sự đáng yêu tinh nghịch của chú ở trong câu chuyện. Thông qua câu chuyện, tôi cảm thấy giống như tác giả muốn giới thiệu cho người đọc về những con vật bé nhỏ khác, mà không phải riêng Chi hay chỉ là một loài mèo.
Tôi là một người rất yêu thương động vật và rất mong mọi người có thể đọc cuống truyện "Mái ấm của Chi này , suy ngẫm để thay đổi cách nhìn nhận và cách ứng xử đối với động vật trên trái đất
5
931227
2015-11-15 23:43:45
--------------------------
261325
4131
Từ xưa đến nay mình vốn là người không hề thích bọn mèo. Nhưng sau khi đọc "Mái ấm của Chi" từ tập 1 thì cuốn truyện này đã đốn tim mình với hình ảnh bé mèo nhỏ nhắn tinh nghịch quá đáng yêu luôn. Săm soi từng chi tiết, từng cử chỉ hành động của mèo con ấy mà mình không thể không bị cuốn theo từng tập truyện này. Bên cạnh đó, mình thấy không chỉ đọc để nhìn ngắm mèo con đáng yêu mà mình còn có thể học được cách yêu thương thú cưng của mình, học cách chơi đùa và thấu hiểu được một người bạn đặc biệt này, cũng có những điều mình cũng nên học từ mèo con đáng yêu Chi.
5
618282
2015-08-11 00:17:54
--------------------------
255032
4131
Truyện con mèo đúng nghĩa là truyện về một con mèo. Mà cái nhìn trong mắt của một con mèo, thế giới thật to lớn, đầy kì bí mà vẫn đáng yêu vô cùng. Những mẫu chuyện đơn giản mà lại vô cùng chính xác cuộc sống của mèo, những gì Chi đã làm trong truyện, con mèo nhà tôi cũng làm hệt như vậy. Chân thật, xúc tích và dễ thương, hiếm có cuốn tranh truyện nào, mà lại là một cuốn truyện con mèo, lại khiến tôi thấy yêu đến vậy.
Mong hoài trọn bộ sẽ sớm về đến tay. 
5
148922
2015-08-05 18:20:28
--------------------------
220303
4131
Bé mèo này là bé mèo dễ thương nhất mà mình từng thấy đấy! Bé Chi dễ thương quá đi , nét vẻ của tác giả này làm mình thích lắm , từng cử chi , hành động trẻ con mà dễ thương chủa Chi làm mình thích lắm. Tập nào cũng phải mua bé Chi về ngay mới được. Nhưng mình buồn khi đành phải chia tay bé Chi ở tập 10. Mong rằng tác giả của bộ này sẽ vẽ thêm thật nhiều những cuốn truyện bổ ích và hay giống như thế này nhé ^^
5
590161
2015-07-02 14:32:01
--------------------------
194265
4131
Mỗi lần đọc Mái ấm của Chi là cảm xúc trong tôi cứ dâng trào. Rất hay, rất bổ ích. Thông qua mái ấm của Chi mà con tôi đã biết yêu những loài động vật nhỏ hơn.
Cảm ơn tác giả Konami Kanata vì đã giúp tôi dạy con mình những bài học bổ ích mà không cần phải quá nhiều lời. Với Chi tôi thấy cuôc sống này rất dễ thương. Đọc đi đọc lại nhiều lần mà vẫn thấy rất thích. Nét vẽ rất đẹp, ấn tượng. Hai mẹ con tôi đều quyết tâm giữ gìn sách cẩn thận và đọc lại nhiều lần. Chúng tôi vui buồn với từng cảm xúc của Chi.
5
628391
2015-05-09 22:53:22
--------------------------
193715
4131
Nhân vật chính là một chú mèo, đúng nghĩa luôn chứ không hề có siêu năng lực nào, những suy nghĩ của Chi đều được thể hiện qua đôi mắt, chúng lúc nào cũng mở to tròn như luôn kinh ngạc háo hức với mọi thứ mà nó bắt gặp, thật đáng yêu kinh khủng luôn! Truyện là những trải nghiệm của Chi, rất đời thường nhưng thể hiện dưới con mắt của mèo lại trở nên vô cùng vui tươi và tươi sáng, thú vị. Tác giả vẽ mèo Chii đáng yêu và thật sự am hiểu về mèo đó, đọc truyện không những có thể thư giãn mà còn có thể hiểu được “anh mèo” ở nhà mình nữa, rất hay!
5
249905
2015-05-08 15:52:36
--------------------------
193569
4131
Mình bắt gặp Chi trong một lần tình cờ vào nhà sách, nhìn lên giá truyện, "Mái ấm của Chi" đã đập vào mắt mình vs giá cao nhất ngưỡng: 35k :v Mình tò mò nên định chỉ đọc vài trang thôi, ai dè lại lỡ kết mê bé ấy mất tiêu, hix.  Lần đầu đặt Chi trên Tiki, mình sợ bị mất bookcard lắm. Vì trong truyện hay có đẹp nên mình rất sợ bị mất trong quá trình vận chuyển. Đến khi truyện về, mình vô cùng hài lòng khi thấy em nó còn nguyên, không những thế, gáy và bìa truyện cũng rất tốt, mới cóng! Nhân vật Chi và những câu chuyện của nhóc đã hay rồi, quyển truyện mua về lại đẹp, có bookcard đầy đủ và mới nên mình rất thích. Hy vọng những sản phẩm tiếp theo mình đặt cũng được như thế này, Tiki nhé ^^~
5
321692
2015-05-08 00:01:10
--------------------------
192750
4131
Quá dễ thương luôn! Ngay từ khi đọc phần giới thiệu ở trên mạng về một “chú mèo con trắng sọc xám” và nhìn hình chú mèo ở bìa thì một người yêu mèo như mình đã mê tít rồi! Ban đầu nghĩ giá hơi chát cho một cuốn truyện tranh nhưng truyện màu, giấy dày và quan trọng là chú mèo con quá dễ thương nên rất đáng tiền. Tính cách của loài mèo nói chung và mỗi con mèo nói riêng được thể hiện đầy đủ qua từng nhân vật mèo trẻ con, “cô” mèo, “chú” mèo. Đúng là chuẩn không cần chỉnh. Chỉ người yêu và gắn bó với mèo mới hiểu những điều này.
5
404606
2015-05-05 16:15:16
--------------------------
191095
4131
Bé mèo ngây ngô này làm siêu lòng ngay những độc giả khó tính nhất bởi cái vẻ ngoài quá kute, những cử chỉ đáng yêu, suy nghĩ quá dễ thương. Không chỉ những độc giả yêu mèo mà cả những ngườì không thích hay chưa từng nuôi cũng sẽ có chút ấn tượng và cuốn hút bởi em mèo dễ thương này. Đến tập này thì bé Chi đã có những thay đổi trong suy nghĩ, cô mèo non nớt ngây thơ ngày nào đã dần hiểu hơn về thế giới xung quanh. Đó cũng là khi khoảng cách với mẹ mèo ngày càng gần hơn còn Nhà Yamada đã phát hiện ra 1 sự thật khiến họ băt đầu lo lắng...
5
166935
2015-04-30 22:39:41
--------------------------
183820
4131
Mình rất vui khi bộ truyện này được xuất bản và đặc biệt còn là Kim xuất bản . Truyện được in ấn rất đẹp , về phần dịch thuật thì miễn chê . 
Truyện kể về chú mèo Chi lạc mẹ và rồi được một gia đình mang về nuôi , họ đối với Chi rất tốt . Ở này chi đang dần nhận ra mình khác loài với gia đình của mình , cô bé đang dần nhận ra cô là một con " mèo " .Chi cũng đã gặp được anh em của mình nhưng lại không nhận ra .  Truyện được vẽ vô cùng dễ thương , tạo cảm giác gần gũi cho người đọc . 
Mình là một người yêu mèo nên bộ truyện này mình không thể bỏ qua ♥ .
5
335856
2015-04-16 18:56:02
--------------------------
183500
4131
nếu có ai đó hỏi tôi "tại sao bạn mua cuốn truyện này ?" tôi sẽ trả lời ngay "vì bìa của truyện" .tôi cái  bị cuốn hút từ cái hình bé mèo Chi với những cử chỉ đáng yêu trong bìa lần đầu nhìn thấy .khi đươc cầm trên tay quyển này, tôi bắt đầu khai thác sâu hơn về nội dung của nó. Và nó đã không làm tôi thất vọng, các tình tiết trong truyện quá gần gũi,dễ thương như đã từng thấy ở các chú mèo nhà tôi vậy . tôi ước chi những lời thoại của Chi có thể dài hơn và sâu sắc hơn. nhìn chung , đây là quyển truyện tranh đáng đọc cho các bạn yêu mèo .
4
602566
2015-04-16 10:47:45
--------------------------
