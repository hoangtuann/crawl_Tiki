81998
9370
mình đã từng đọc qua quyển chính sử Trung Quốc viết về 350 vị hoàng đế. Tuy nhiên do số lượng quá nhiều chính sử chỉ cho mình một cái nhìn khái quát nhất. Đọc quyển sách 12 đại hoàng đế này của Huyền Cơ thật sự là 1 tài liệu tuyệt vời. Lịch sử được diễn giải bằng ngòi bút tiểu thuyết nên tuy dày nhưng không ngán, không chỉ có các sự kiện chính trong cuộc đời vị vua đó mà ngay cả các truyền thuyết dân gian cũng được tác giả đề cập làm mình thấy rất thích thú. Đặc biệt là những ai yêu mến Tần Thuỷ Hoàng và Ung Chính thì không nên bỏ qua. 
5
111463
2013-06-19 15:24:17
--------------------------
