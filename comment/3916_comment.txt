411163
3916
Bé nhà mình rất thích truyện cổ tích nên mình mua về bộ này cho bé.Khi nhận được hàng mình thấy rất hài lòng và bé cũng rất vui.Các câu truyện mang ý nghĩa nhân văn sâu sắc và khá đơn giản để các bé có thể hiểu.Hình ảnh cũng khá được .Bởi các bé thường bị lôi cuốn bởi hình ảnh hơn là chữ viết.Bộ truyện gồm 10 câu truyện rất gần gũi với thế giới của bé.Giúp bé kích thích được trí tưởng tượng và học cách đối xử với mọi người thông qua các câu chuyện .Cảm ơn tiki rất nhiều vì đã có rất nhiều chương trình giảm giá,tạo điều kiện cho khách hàng.

4
543671
2016-04-05 17:12:31
--------------------------
345916
3916
Ai có con nhỏ chắc cũng muốn mua thật nhiều truyện cổ tích về cùng đọc với con để giúp các bé xây dựng những những ước mơ, phát triển ngôn ngữ và trí tưởng tượng. Bộ truyện này gồm 10 câu chuyện rất đỗi quen thuộc mà chắc hẳn ai cũng đã từng được nghe hoặc kể từ tuổi thơ của mình. Về nội dung, các câu chuyện đều được viết lại một cách rút ngọn, vắn tắt, ít chi tiết so với phiên bản gốc dẫn đến hình thức là các quyển sách khá mỏng và bé bé. Hình vẽ minh hoạ tương đối ổn, có gam màu trầm đậm tính cổ tích. Nhưng thực sự mình vẫn thích những quyển sách dành cho thiếu nhi nên tươi sáng và khổ in lớn hơn như thế này thì sẽ giúp các bé dễ đọc và thích thú hơn.
3
460351
2015-12-01 15:34:06
--------------------------
232242
3916
Đọc truyện cổ tích,nhất là truyện về những nàng công chúa xinh đẹp,những chàng hòang tử tài ba,...mà được ngắm thêm tranh vẽ thì còn gì thú vị bằng.Bộ túi 10 quyển này dù khá ít truyện,nhưng đều là những truyện hay của cổ tích Grimm hay Andecxen.Chất lượng sách tốt,màu sắc khá bắt mắt,hình vẽ khá đẹp,hầu hết các hình vẽ có độ giáo dục và tinh tế,phù hợp với thiếu nhi.Nếu những quyển sách này được biên tập trong khổ sách lớn,bìa cứng,nhiều truyện trong cùng một quyển, chắc hẳn sẽ rất dễ dàng để chúng tôi dùng đọc cho con nghe,vừa giải trí vừa giáo dục.
4
551735
2015-07-18 18:02:11
--------------------------
