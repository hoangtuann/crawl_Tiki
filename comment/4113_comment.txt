358848
4113
Tôi đã mua sách và rất hài lòng, cuốn sách hệ thống những chủ đề giao tiếp thiết thực và phong phú, rất dễ học và dễ nhớ. Sách còn kèm CD hỗ trợ học rất tốt, đọc theo những đoạn hội thoại khiến khả năng nói tiếng anh của tôi khá lên rất nhều, tự tin hơn với cách  đọc chính xác của mình.
Cách học giao tiếp theo từng chủ đề này giúp học rất hiệu quả, đặc biệt đối với những  người mới bắt đầu . Sách có 29 đơn vị bài học tương đương với 29 chủ đề, rất là đầy đủ. Với 1 quyển sách này bạn có thể giao tiếp tiếng anh được tương đối. Nếu muốn tìm một cuốn sách tự học giao tiếp để có thể giao tiếp được cơ bản thì đây là 1 cuốn tuyệt vời để tự học.

5
1004174
2015-12-25 23:42:41
--------------------------
355085
4113
Sách phân loại theo chủ đề, làm người đọc nhìn vào dễ hiểu, ngắn gọn và súc tích. Bìa sách độ trắng tốt ưa nhìn, bản thân tôi thích quyển sách này. Sách có kèm theo đĩa CD, đĩa tốt không trầy khi xem không bị giật. Đĩa CD tốt khi nghe giúp người học dễ hiểu tiếp thu nhanh hơn. Với quyển sách này, trình độ học của tôi sẽ tốt hơn trước. Tiki giao hàng rất nhanh, nhân viên cũng tốt. Làm tôi giật mình vì độ giao sách nhanh. Mong tiki sẽ phát huy. Tôi sẽ luôn ủng hộ
5
891033
2015-12-19 10:10:53
--------------------------
221282
4113
- Cuốn sách chia làm 29 chủ đề và mỗi chủ đề đi sát với các giao tiếp hàng ngày của chúng ta.
- Ex: chủ đề về chào hỏi " Greetings" hoặc chủa đề về mua sắm"Shopping ", chủ đề về điện thoại" Telephoning" đi sát với đời sống hàng ngày chúng ta gặp, ở mỗi chủ đề Tác giả phân chia rất rõ ràng từng mục " Vocabulary" nói về từ vựng trong bài, và cách phiên âm các từ cho chúng ta dễ phát âm đúng, ngoài ra còn có phần hội thoại "Dialogues" đi cùng với CD giúp chúng ta vừa nghe vừa học từ vựng trong các mẫu đối thoại dễ dàng nhớ từ hơn.
- Nói chung cuốn sách hữu ích cho người mới bắt đầu học Tiếng Anh và làm quen với đàm thoại từ dễ đến khó hơn.
5
657350
2015-07-03 17:38:19
--------------------------
