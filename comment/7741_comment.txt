252129
7741
Bìa sách đẹp, bắt mắt, phần in bên trong rất rõ ràng, dễ nhìn, trình bày logic, không làm tắt các bước quá mức dễ gây khó hiểu. Một cuốn sách đa dạng các loại bài, các bài tập của nhiều dạng bài toán nguyên hàm khác nhau. Mỗi bài, loại bài đều được giải bằng nhiều cách một cách chi tiết, ngoài ra còn có cách giải nhanh để tham khảo thêm (điều mà không phải cuốn sách hướng dẫn học nào cũng có), trong sách còn có nhiều dạng toán mới lạ. Tóm lại nó phù hợp cho những ai muốn am hiểu tường tận về nguyên hàm-tích phân.
3
112336
2015-08-03 13:54:40
--------------------------
