162543
8997
Về bìa của Tam Quốc Diễn Nghĩa - Tập 7 thì vẫn vậy: màu bìa đẹp, hợp với nội dung sách, bền, nhẹ, sáng sủa. Chất liệu giấy tốt, sáng, nhẹ, bền nên cầm đọc rất là dễ chịu và thoải mái. Bố cục chữ minh họa thì rất là chặt chẽ và hợp lý với cách trình bày tranh.
Nét vẽ đẹp, chân thực, sống động và tinh xảo nhưng màu vẽ thì mình thấy ổn bởi vì khi mình cầm đọc cuốn truyện lại bị thấy màu nhem vào tay nên hơi khó chịu một tí ( điểm này thì mình vẫn thông cảm được) .Nội dung cuốn này vẽ cảnh giao chiến rất là đẹp, chân thực.
5
525063
2015-03-02 18:07:43
--------------------------
