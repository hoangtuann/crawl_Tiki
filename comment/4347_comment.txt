423008
4347
Hòa giải cơ sở là một trong những chính sách hay và độc đáo của pháp luật Việt Nam so với hệ thống pháp luật các nước trên thế giới. Quan trò của Hòa giải gần như là điều kiện tiên quyết để giải quyết trước khi vào công việc tố tụng chính thức. Cuốn sách tuy nhỏ nhưng đã cung cấp một nguồn kiến thức quan trọng đặc biệt là cho những người làm nghề luật như tôi. Thiết nghĩ NXB nên tập hợp nhiều nhiều văn bản liên quan vào cuốn sách để quá trình tra cứu thêm dễ dàng hơn nữa.
4
105202
2016-04-28 16:02:36
--------------------------
