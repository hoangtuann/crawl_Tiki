450268
7841
Khi mình mua và quay lại nhận xét một chút thì giá giảm khá mạnh, đây là một cơ hội tốt cho các bạn teen mua và trải nghiệm
Mình mua quyển này cho bạn gái mình và có đọc qua ^^ 
Bìa đẹp dễ thương, giấy tốt
Mỗi câu chuyện đều mang lại cho mình cảm xúc riêng và đây cũng thế. Có lẽ Kẹo Bạc Hà cho tình đầu có âm hưởng chung là nhẹ nhàng. Viết về mối tình đầu rất dễ thương ngây ngô như viên kẹo bạc hà đang tan chảy, 25 viên kẹo bạc hà đang tan chảy trong tâm hồn người đọc. Sự hờn ghen, đố kị, tình cảm đơn phương ... đâu đó  có thể bắt gặp chính bản thân mình 
5
1427380
2016-06-18 17:56:56
--------------------------
290014
7841
Là tuyển tập 25 truyện ngắn về chủ đề tình yêu, vui có, buồn có như chính viên kẹo bac hà, ngọt ngào đấy nhưng cũng the thé cay. Những truyện ngắn đạt gải cao đều rất xứng đáng bởi nội dung hay và có sự trau chuốt về mặt ngôn ngữ. Sách tập hợp những truyện ngắn lọt vào chung khảo của cuộc thi viết Kẹo bạc hà cho tình đầu trên tạp chí Áo trắng nên chất lượng khá đảm bảo. Bìa minh họa đẹp, lại được in nổi nữa nên rất thích. Một vài truyện cũng khiến mình chưa thích thú lắm.
4
431055
2015-09-05 00:44:13
--------------------------
257550
7841
Kẹo bạc hà cho tình đầu là cuốn sách tổng hợp nhiều truyện ngắn viết về tình yêu tình bạn của người trẻ. Cuốn sách mang một hơi thở mát lạnh nồng nàn của bạc hà và sự ngọt ngào của kẹo. Tôi không thích giấy in của sách vì nó nhám tạo nên cái vẻ khó chịu, đôi khi mất đi cảm giác đồng cảm với từng câu truyện. Đúng là những người vừa mới chia tay người yêu hay có mối tình đầu đẹp thì sẽ đồng cảm với từng nhân vật. Rất hay, rất thú vị, quá chất
3
255023
2015-08-07 19:20:13
--------------------------
228674
7841
Đây là tuyển tập những truyện ngắn lọt vào vòng chung khảo của cuộc thi Kẹo bạc hà cho tình đầu nên hẳn là yên tâm về chất lượng vì đã được ban giám khảo của cuộc thi chọn lọc kĩ lưỡng. Giống như tên gọi của cuộc thi, đâu đó trong những truyện ngắn này, ta bắt gặp được các cung bậc của tình yêu, là ngọt ngào hạnh phúc hay là cay xè của sự chia ly. Tuy vẫn còn một vài truyện mình không thích nhưng nhìn chung đây là một tác phẩm đáng để đọc và suy ngẫm. 
4
330563
2015-07-15 19:36:48
--------------------------
185091
7841
.Truyện đặc biệt hay đối những ai vừa trải qua mối tình đầu,tình đơn phương. Nó cứ như vị the the của viên kẹo vậy.Tình đầu ít có khi nào thành! Tôi không thích một số kết thúc trong truyện nhưng đó là những kết thúc cho tình đầu là những kỉ niệm đẹp và nếu là tôi dù không thích nhưng tôi vẫn sẽ chọn một cái kết theo hướng như vậy. Đọc truyện tôi cảm thấy mình vừa hiểu ra một cái gì đó trong lúc tôi tồi tệ nhất.Ở đây có những mẫu truyện làm người ta phải rơi cả nước mắt.Khi đọc truyện này kết hợp 1 bản nhạc không lời thì đúng là cực "chất" luôn.
4
475077
2015-04-18 21:13:33
--------------------------
119083
7841
Đây là một tuyển tập từ cuộc thi "Kẹo bạc hà cho tình đầu", những tác phẩm được chọn xuất bản đánh giá tính cẩn thận và chọn lọc trong cuộc thi khá cao. Những câu truyện đơn giản, nhẹ nhàng nhưng mang nhiều màu sắc và thể hiện sự sáng tạo của các tác giả. Mỗi tác phẩm đều có một thông điệp riêng và gợi nhớ cho người ta nhiều cảm xúc về tuổi thơ, về mối tình đầu. "Vị bạc hà" mà cuốn sách mang đến tạo cho ta nhiều suy ngẫm, nhiều dư vị và nhiều đồng cảm. Đây là một cuốn sách mà bạn nên tìm đọc để thấy nhiều dư vị trong cuộc sống này :)
4
248457
2014-08-02 11:58:44
--------------------------
