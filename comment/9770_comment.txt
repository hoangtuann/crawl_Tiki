575401
9770
Bộ sách Minano quá nổi tiếng dành cho những người có mong muốn học thêm 1 ngôn ngữ là tiếng Nhật.
Tuy nhiên, nếu chỉ dùng cuốn này không thì rất khó khăn cho việc học, vì người mới chưa quen mặt chữ. Cần phải mua thêm bản dịch nghĩa.
4
285522
2017-04-17 11:23:33
--------------------------
464216
9770
Ngoài học ở trung tâm ra mình mua thêm cuốn này để ôn luyện.
Cuốn sách này khá tốt vì nguyên bản từ Nhật nên sẽ chuẩn xác để ôn luyện hơn. Tuy nhiên bản này trình độ sơ cấp 1 mà không dịch tiếng việt nên không dành cho đối tượng bắt đầu chưa biết gì tự học, 
Cuốn sách này được in giấy xốp nên việc học cũng cần phải cẩn thận chứ không quăng quật thoải mái được như những giáo trình khác
Cần mua thêm cuốn có dịch tiếng việt và giải thích với cuốn bài tập chủ điểm để bổ sung chứ một cuốn này thì không đủ
4
74132
2016-06-29 23:34:46
--------------------------
419189
9770
Phù hợp cho người mới học, tuy nhiên chữ khá nhỏ mà nét đậm khiến cho việc đọc rất khó khăn. Biết lí do này chắc là loiix của nhà in ấn, nhưng cũng đủ để đánh giá một thành phần. Tiki bọc sách rất đẹp, vừa vặn. Nhưng mong tiki bán riêng bookcare hoặc bán kèm sách với bookcare rồi thêm giá cũng không sao cả. 
Chẳng còn gì để viets nữa. Nhưng mong tiki chuyển phát nhanh thì tốt hơn nhiều. Mình nhớ lần trước tiki vận chuyển tận 9 ngày, mà lần này chỉ 2 ngày. Ahi hi hi
4
725054
2016-04-20 19:55:14
--------------------------
408628
9770
Sp này thật sự rất có ích cho những ai đang theo và học tiếng nhật..các bài học này phù hợp cho những ai thích tự học tự nghiên cứu. Giá cả phù hợp chữ rõ có phiên qua kanji nhưng khá nhỏ. Không chỉ có từ vựng mà còn có những nét văn hóa rất đáng yêu của người nhật. Và khi học bộ giáo trình này bạn cần phải bền bỉ và kiên trì để đật được kết quả cao.  Nói chung đây là một cuốn sách hay và bổ ích.hết văn rồi sao mà phải tới 100 từ mới cho đăng
5
954203
2016-03-31 23:41:57
--------------------------
405590
9770
Tôi mới đặt cuốn sách này vào mấy ngày hôm trước. Phải nói là khi nhận được hàng mà tôi vui cỡ nào. Mong ngóng sách từng ngày. Bởi với tôi, tiếng Nhật như đã ăn sâu vào máu của tôi, tôi quý sách tiếng Nhật mà còn quý hơn cả vàng nữa, hehe. Đợt sách này tôi mua là sách tái bản năm 2015, cực thích luôn. Chất giấy cũng khá ok, mà hơi vàng vàng nên sẽ không cảm giác bị lóa mắt. Cỡ chữ cũng tạm ổn, tuy nhiên có vài chữ kạni đọc không ra. Nhưng không sao, vì tôi rất ưng cuốn sách này và cả chất lượng phục vụ của tiki nữa.
5
658966
2016-03-26 22:30:03
--------------------------
404607
9770
Chỉ là mua giúp người bạn yêu thích nước Nhật và mê phong tục, phim hoạt hình Nhật thôi. Đưa sách cho nó học vài bữa mà nó khen sách quá trời.
Thấy nó bảo học cũng được, sách chi tiết tỉ mỉ trình bày bố cục rõ ràng cẩn thận lại đầy đủ các phần cho nên rất là dễ học, dễ tiếp thu đối với những người mà mới bắt đầu học tiếng Nhật Bản như nó vậy
Sách dày, giá tương đối, tiki bọc hàng cẩn thận và giao hàng cũng rất là nhanh. Sau này sẽ còn đặt nhiều đồ trên tiki nữa
5
694162
2016-03-25 14:09:07
--------------------------
389295
9770
Cuốn Tiếng Nhật Cho Mọi Người - Trình Độ Sơ Cấp 1 (Bản Tiếng Nhật) này rất phù hợp cho những bạn nào vừa bắt đầu học tiếng nhật. Bạn nào muốn học TN thì nên mua cuốn này cùng với cuốn Tiếng Nhật. Sách Tiếng Nhật Cho Mọi Người - Trình Độ Sơ Cấp 1 (Bản Dịch Nghĩa và Giải Thích Ngữ Pháp) sẽ hỗ trợ rất đắc lực cho các bạn trong quá trình tiếp cận với tiếng Nhật. Điều đáng tiếc là chất lượng giấy không được tốt cho lắm. Mình cảm thấy khá hài lòng với bộ giáo trình này.
5
642968
2016-03-01 19:39:39
--------------------------
387170
9770
Chắc hản bạn nào đang học tiếng nhật thì không xa lạ gì với quyển này rồi. Đây là quyển sách cung cấp những kiến thức cơ bản về ngữ pháp, mẫu câu cần thiết cho những ai học tiếng Nhật.Mình có một vài nhận xét như sau:
Về hình thức: bìa đẹp,có hỗ trợ bookcare nữa là tuyệt vời.Giấy ngã vàng, in đẹp không bị nhòe.
Về nội dung:Cung cấp đủ ngữ pháp, mẫu câu cần thiết.một điểm trừ là sách không có CD nên không thể hỗ trợ cho phần nghe có trong sách .
Ngoài ra còn có quyển Bản Dịch Nghĩa và Giải Thích Ngữ Pháp" các bạn nên mua hai quyển nà song song với nhau sẽ thuận tiện hơn rất nhiều.
5
183979
2016-02-26 18:38:48
--------------------------
362900
9770
Tiếng Nhật Cho Mọi Người - Trình Độ Sơ Cấp 1 rất hữu ích đối với người mới học tiếng Nhật như mình. Cuốn sách trình bày dễ hiểu, logic và khoa học. Sách có bản dịch chưa đầy đủ cho lắm. Giấy sách thì hơi mỏng nên khá là nhẹ. SÁch có một số chỗ tiếng Nhật in chữ khổ hơi bé nên khó nhìn mặt chữ và hơi khó đọc . Nói chung cuốn sách này rất bổ ích, tiki bọc sách cũng rất cẩn thận nên không lo bị hư sách vì giấy của sách khá là mỏng. 
4
998787
2016-01-03 01:00:12
--------------------------
355603
9770
Đây là một trong hai cuốn sách nên mua để bắt đầu học tiếng Nhật. Nội dung trình bày khá rõ ràng, chất liệu giấy tạm chấp nhận. Để học tốt hơn thì nên mua kèm theo cuốn Bản Dịch Nghĩa và Giải Thích Ngữ Pháp tập một để có thể dễ dàng cho người tự học. Sách gồm 25 bài cơ bản giới thiệu về những mẫu câu cơ bản, cách giao tiếp cũng như văn hóa của Nhật bản. Về phần nghe của sách có thể dễ dàng kiếm được trên mang. Một cuốn sách tốt cho ai mới bắt đầu tự học.
5
151541
2015-12-20 12:18:18
--------------------------
352943
9770
Đối với những ai đang muốn học tiếng Nhật nhanh chóng và hiệu quả thì đây là sự lựa chọn của bạn. Với cách phiên âm cùng cách dịch thuật chính xác mang đến cho người học sự thân thuộc cũng như dễ dàng trong việc học tiếng Nhật. Phần minh hoạ cùng các nhân vật và ví dụ vô cùng dễ hiểu. Bìa sách đẹp và chắc chắn. Gáy sách được kẹp rất cẩn thận. Đây là quyển sách thiết yếu cho bạn khi muốn học tiếng Nhật. Còn chần chừ gì nữa, mua ngay và học thôi nào. Tiki giao hàng rất chính xác và đúng giờ. 
5
982878
2015-12-15 11:59:17
--------------------------
349348
9770
Đây là cuốn sách cơ bản cho mọi người bắt đầu học và làm quen với tiếng Nhật với các cấp độ từ cơ bản đến nâng cao. 
Từ vựng từng bài cũng từ dễ đến khó. Nhiều trung tâm cũng dùng bộ giáo trình này để giảng dạy.
Mình cũng vừa bắt đầu tìm hiểu về giáo trình này trong thời gian gần đây, hi vọng sau khi hoàn thành giáo trình mình có thể biết được một phần về tiếng Nhật.
Nhưng trước khi bắt đầu thì mình khuyên các bạn nên học thuộc2 bộ bản chữ cơ bản trước tiên của tiếng Nhật là Higarana và Katakana
5
630802
2015-12-08 10:51:21
--------------------------
347398
9770
Quyển này viết đầy đử mà dẽ hiêu cho người bắt đầu học. Mọi người mua về mà học kèm theo quyển dịch và giải thích ngữ pháp nữa là ôkê. Tiki giao hàng cũng nhanh và thuận tiện nữa. Quyển sách này mình thấy trung tâm nào dạy tiếng nhật cũng dung để giảng dạy hết. Giáo viên dạy tốt nữa thì tuyệt vời. Bài tập cũng gần gũi mà đa dạng nữa. Chỉ có cái là k thấy địa nghe ở đâu để luyện tập hết trơn à. Không biết tiki có không? Luyện nghe cũng quan trọng mà!
5
1017932
2015-12-04 12:58:45
--------------------------
331593
9770
Nếu bạn tính học tiếng nhật mà không biết nên học từ đâu, học theo giáo trình nào thì đây là sự lựa chọn của bạn. Bộ giáo trình Minna no nihongo này gồm có trình độ sơ cấp 1 (bản tiếng nhật, bản dịch và giải thích ngữ pháp, tổng hợp các bài tập chủ điểm, kanji) và trình độ sơ cấp 2 cũng giống như vậy. Sách được biên soạn khá logic và đầy đủ các kiến thức cơ bản. Với một bộ như vậy thì kì thi năng lực nhật ngữ với trình độ N5 và N4 sẽ không còn khó đối với bạn nữa.
5
416217
2015-11-04 20:34:46
--------------------------
330663
9770
Mình đã mua cuốn này và rất thích nó, ấn tượng ban đầu là bìa rất đẹp, giấy xốp nhẹ và thơm cực, hình ảnh dễ thương, các ví vụ minh họa thực tế rất sống động, dễ dàng hiểu được các cách dùng từ phù hợp cho từng ngữ cảnh (tuy nhiên tất cả đều là Japanese nhé). Quyển này có cả phiên bản tiếng Việt, nội dung hoàn toàn giống nhau. Tùy theo nhu cầu và trình độ mà các bạn có thế chọn một trong hai, nếu có điều kiện thì nên sở hữu cả 2 cuốn để kết hợp với nhau - sẽ dễ dàng hơn với tiếng Việt và nâng trình với bản tiếng Nhật đó :D
5
612696
2015-11-03 00:43:45
--------------------------
320662
9770
Ưu điểm: Sách in mực rõ nét, không nhòe mặc dù chữ nhỏ. Về nội dung thì không cần bàn cãi: Nội dung đầy đủ từng phần, còn bao gồm bài tập. Mọi thứ được sắp xếp rất tốt từ căn bản đến nâng cao. 
Nhược điểm: Tuy nhiên bản in này chữ nhỏ quá, mình mong muốn chữ được in to hơn để có thể nhìn rõ chữ cái và Hán Tự ( Hán tự được in nhỏ quá nên có nhiều chữ không nhìn ra được nét.
Nhìn chung về giá cả và nội dung thì quyển sách vẫn rất đáng để mua!
Nếu không có CD kèm theo thì vẫn có thể dễ dàng tìm được trên mạng.
4
751220
2015-10-12 00:15:33
--------------------------
305266
9770
Về bộ sách Minano Nihongo, những ai bắt đầu học tiếng Nhật đều biết đến. Đây là bộ sách cung cấp những kiến thức cơ bản về ngữ pháp, mẫu câu cần thiết cho những ai có ý định học tiếng Nhật. Nhưng vì sách toàn là tiếng Nhật nên chúng ta cần mua thêm quyển Giải thích ngữ pháp, và các bài tập chủ điểm cùng một bộ để có thể học tốt hơn. Từ đó, chúng ta cũng có thể bắt đầu tự học tiếng Nhật. Về hình thức, giấy hơi mỏng và vàng, chữ in hơi mờ, phần tiếng Nhật hơi nhỏ, nên nhìn hơi nhức mắt.  
4
3243
2015-09-16 21:37:44
--------------------------
297849
9770
Là bản viết tiếng Nhật của bộ sách Minano Nihongo, bộ sách cơ bản, phương pháp học tốt nhất cho bất cứ ai trong chúng ta muốn bước chân vào tiếng Nhật. Tuy là viết toàn bộ tiếng nhật nhưng đi từ cơ bản đến nâng cao nên không làm khó chúng ta. Bạn có thể tự học một cách dễ dàng. Sách giới thiệu các mẫu câu cơ bản, bài thoại gắn liền với thực tiễn. Các bài tập ngữ pháp để thực hành giúp chúng ta nhớ lâu và phản xạ tốt hơn. Hình thức sách khá nhưng vẫn thiếu xót, giấy hơi ngả vàng và xơ, chữ tiếng nhật hơi nhỏ để đọc.
4
252046
2015-09-12 10:44:37
--------------------------
292940
9770
Quyển Tiếng Nhật cho mọi người từ bộ giáo trình Minna no Nihongo nhưng mình vẫn thích giáo trình gốc của nó hơn vì khổ sách này hơi nhỏ gây rối cho những bạn mới học và chủ yếu tự học như mình. Giấy sách màu vàng, ưu điểm là nhẹ, dễ lật nhưng cũng nhanh cũ và có mùi nên mình không thích chất liệu này lắm. Về nội dung, chi tiết và rõ ràng nhưng để tự học thì chưa đủ, bộ sách này cần bổ sung thêm sách luyện nghe và đọc hiểu có trong bộ giáo trình cũ để hoàn thiện các kỹ năng cho người mới học.
4
405959
2015-09-07 20:28:56
--------------------------
286539
9770
Cuốn ''Tiếng Nhật Cho Mọi Người - Trình Độ Sơ Cấp 1'' này rất cần thiết cho những bạn đã biết một chút về tiếng Nhật. Những bạn chưa biết gì về tiếng Nhật thì học quyển sách này khá khó khăn vì nó được viết hoàn toàn bằng Nhật ngữ. Sách này là sách bài tập cho giáo trình Minna no Nihongo. Sách giúp làm quen chữ, học từ vựng nên khá bổ ích. Làm xong quyển sách này có thể tự tin đi thi trình độ N5. Tóm lại đây là quyển sách bổ ích cho các bạn học mới học tiếng Nhật.
4
644308
2015-09-01 20:46:59
--------------------------
284991
9770
Quyển sách này rất nổi tiếng và phổ biến, rất hữu ích với những ai mới bắt đầu học tiếng nhật, có đi kèm chung với CD nhưng không thấy tiki có kèm CD, trình bày rất rõ ràng, giấy vàng đọc không hại mắt, mực in thì rõ nhưng chữ nhỏ quá, đọc mỏi mắt, mình mua cuốn sách này vì có phần từ vựng mở rộng có kèm hình nhìn rất thích, nhìn hình rất dễ học. Từ vựng đầy đủ, chỉ cần học hết thi N5 là ok. Nói chung là một cuốn sách bổ ích cho những ai muốn học tiếng nhật
5
80823
2015-08-31 14:40:18
--------------------------
267137
9770
Mình mới bắt đầu học tiếng Nhật cách đây vài tuần và được GV tiếng nhật ở trung tâm mình học có giới thiệu cho bộ sách này gồm có bản tiếng nhật và bản dịch tiếng việt, giải thích ngữ pháp. Cuốn sách rất hữu ích đối với những bạn mới bắt đầu học tiêng Nhật như mình, sách trình bày khá dễ hiểu, chữ in rõ nhưng có vài khuyết điểm là chữ in hơi nhỏ và giấy khá mỏng nên mỗi khi cần ghi chú gì vào sách lại có vết hằn ở trang phía sau. Nói chung mình khá hài lòng về bộ sách này của NXB Trẻ
4
721387
2015-08-15 11:20:26
--------------------------
264561
9770
Quyển này thì rất phổ biến không chỉ cho người nước ngoài học tiếng Nhật mà còn cho người du học ở Nhật nữa. Quyển sách này đã rất phổ biến rồi nên bạn có thể mua được dễ dàng, trình bày đẹp. Quyển này cần dùng chung với bản dịch và phiên âm tiếng Việt thì tốt hơn. Bản gốc này bạn có thể dùng để làm bài tập, làm quen mặt chữ, học từ vựng thì dùng bản dịch và phiên âm sẽ giúp ích cho người mới bắt đầu. Nói chung thì bạn kết hợp 2 quyển lại với nhau nếu mới bắt đầu học. Để học hết N5 thì đây là những quyển rất hợp lý!
5
59633
2015-08-13 11:26:52
--------------------------
258946
9770
Theo mình biết thì Minna no Nihongo là giáo trình phổ biến không chỉ dành cho người học tiếng Nhật tại Việt Nam mà ở Nhật cũng được du học sinh Việt sử dụng khá nhiều. Trước đây, khi mình mới học, rất khó để tìm những cuốn sách như thế này. Vì một là sách gốc, hai là sách trường photo. Sách gốc tính ra đến vài trăm nghìn một quyển, còn sách photo thì nhìn không rõ. May sao lùng được trên Tiki quyển này, giá rất phù hợp với sinh viên, lại có cảm giác dùng sách “đường đường chính chính”, vì thầy mình nói dùng sách photo cũng là vi phạm luật bản quyền. Có điều không hài lòng về chất lượng giấy: giấy khá mỏng, nếu bạn xài bút lông hay bút nước mà viết sẽ bị thấm qua trang sau, cảm giác hơi khó chịu. Nhưng hoàn toàn có thể bỏ qua được vì nội dung và hình thức sách quá tốt, điều đó mình không đề cập đến nữa vì đã nhiều bạn bình luận về nội dung rồi.
4
205174
2015-08-08 22:40:26
--------------------------
258394
9770
Mình luôn muốn được học tiếng Nhật chủ yếu để xem anime và manga. Trong trường có chị lớp trên mở câu lạc bộ tiếng Nhật thế là mình tham gia. Chị ấy khuyên mình nên mua cuốn sách này vì nó rất hay và bổ ích. Thế là mình mua. Quả thật rất hay, mình thích lắm. Cũng cảm ơn tiki nhiều vì nơi mình ở không có bán sách này, mình kiếm rất lâu. Mình lên tiki thấy có bán là mình đặt ngay. Chất lượng sách rất tốt chữ rõ ràng không thấy có chỗ bị lên. Mình cảm ơn tiki nhiều lắm
5
698872
2015-08-08 15:06:35
--------------------------
257849
9770
Tôi hiện là sinh viên muốn tìm hiểu học hỏi văn hóa Nhật vừa chuẩn bị phục vụ đi làm sau này vừa là sở thích . Tôi đã mua quyển sách và cảm thấy rất hài lòng . Chữ trong sách được in rõ ràng, nội dung từng bài được thể hiện rõ , dễ hiểu. Rất thích hợp cho người mới lần đầu học tiếng Nhật như tôi. 25 bài trong sách hoàn thiện nội dung chương trình tiếng nhật sơ cấp 1. Học hết 25 bài sẽ có đầy đủ kiến thức để thi N5. Ngoài ra nên mua thêm những quyển sơ cấp 1 còn lại để nắm vững hơn kiến thức về cả tiếng nhật và nội dung biên dịch 
5
197034
2015-08-08 00:38:28
--------------------------
255532
9770
Trước đây mình đã từng học tiếng Nhật hơn 1 năm và cũng đã dùng giáo trình Minna no Nihongo khi học tại trung tâm ngoại ngữ ĐH KHXH&NV nhưng lúc đó chưa có bản dịch tiếng Việt mà là bản song ngữ Nhật - Anh. Giờ mình muốn ôn lại để thi lấy bằng và có xem thử vài giáo trình khác nhưng vẫn thích Minna nhất. Sách trình bày dễ hiểu và logic, mỗi bài sẽ phân chia thành các phần rõ rệt: từ vựng - ví dụ - ngữ pháp - luyện tập ngữ pháp - hội thoại và bài tập luyện nghe. Có những bài đọc nhỏ cũng rất hay và dễ thương. Mình chỉ có chút ý kiến riêng là sách do nxb Trẻ in khổ hơi nhỏ nên chữ nhìn không rõ lắm và phân biệt thành các cuốn riêng bản Nhật - bản dịch ngữ pháp - bản bài tập thì...hơi bất tiện 1 chút do khi học bạn cần đối chiếu giữa bản dịch và bản Nhật ngữ. Nếu có cách nào kết hợp cả 2 lại thì thật là tuyệt. Chất lượng in thì rất tốt và chữ in rõ ràng.
4
254939
2015-08-06 09:24:26
--------------------------
252961
9770
Mình mua quyển này để học tiếng Nhật theo lời giới thiệu của nhiều người. Kết cấu bài học trong sách như sau:
1. Mẫu câu cơ bàn
2. Ví dụ
3. Hội thoại ( chúng ta nên học thuộc luôn đoạn hội thoại mỗi bài).
4. Luyện tập
5. Bài tập
6. Ôn tập
Các phần trên là sự kết hợp của nghe, nói, đọc, viết nên sau khi học xong bạn sẽ ghi nhớ được nội dung bài học.
Về hình thức, quyển giáo trình này in rõ ràng. Sách dày hơn 300 trang và rất dễ đọc. Nhưng do mình chưa học hán tự nên có nhiều kí hiệu hán tự in cỡ chữ như trong sách thì mình không nhìn ra.
4
215163
2015-08-04 06:17:23
--------------------------
251148
9770
Khi mình bắt đầu học tiếng nhật mình đã tìm rất nhiều nguồn tài liệu tham khảo và mình thấy bộ Minna No Nihongo đc rất nhiều người đánh giá cao. Khi đó chỉ có bản scan nhìn rất khó nên khi biết tiki bán bộ giáo trình này mình đã đặt mua ngay. Tuy nhiên trc khi học bộ giáo trình này thì cũng cần có trình độ nhất định về tiếng nhật vì quyển sách này đc viết hoàn toàn bằng tiếng nhật. cuốn sách có chất lượng giấy in tốt. Nội dung trình bày rõ ràng các bài cơ bản rồi nâng cao dần phù hợp với người học
4
448305
2015-08-02 14:38:14
--------------------------
242735
9770
Có người nói là giáo trình phải có trịnh độ chút mới đọc được cái này, thiệt ra là không đúng. Nếu bạn muốn có thể đọc được chữ trong giáo trình, chỉ một bước duy nhất là học thuộc bảng chữ cái, việc này khá đơn giản, vì chỉ cần mua một cuốn tập viết chữ cái, chuyện này là đương nhiên đối với người mới học đúng không. 
Về văn phạm thì có một cuốn bản dịch và giải thích ngữ pháp giải thích khá chi tiết các điểm văn phạm căn bản trong giáo trình này.
Một phần nữa, nếu bạn muốn luyện tập nhiều hơn, thì có giáo trình tổng hợp các bài tập chủ điểm, với nội dung các bài tập đi sát với các bài học của giáo trình này.
Học xong cuốn này, các bạn có thể tiếp tục với bộ 3 cuốn minna 2 đó

Và việc các bạn cần chú ý với giáo trình này là phải xem trước nhà xuất bản. Hiện tại, mình chỉ thấy ưng ý với nhà xuất bản trẻ. Chữ rõ, giấy dày, nhìn cũng đẹp hơn nữa. 
Chúc mọi người học tốt nhé
4
373698
2015-07-26 21:50:11
--------------------------
218301
9770
Mình cũng mới bắt đầu học tiếng Nhật, nghe mọi người giới thiệu quyển này hay, học tốt, thế là mua. Nhưng xin nói trước với mọi người là sách này chỉ dành cho những bạn nào đã thuộc bảng chữ cái tiếng Nhật nhé, nếu không thì không biết gì đâu, vì sách này chỉ có tiếng Nhật thôi, không có phần phiên âm cũng như phần dịch nghĩa. Với những bạn mới bắt đầu làm quen mà lại tự học như mình sẽ rất khó khăn. Đành phải mua quyển khác, có đầy đủ phiên âm và dịch nghĩa về học thêm, quyển này để dành khi nào trình độ cao hơn một chút về từ vựng sẽ lấy ra học thì sẽ rất hữu dụng đấy ạ.
3
121517
2015-06-30 15:18:56
--------------------------
217742
9770
Mình đã học gần xong quyển 1 của Minna no Nihongo rồi. Cuốn này được trình bày rất tốt cho người bắt đầu học cũng như tự học, và ở các trung tâm cũng sử dụng cuốn giáo trình này. Sách trình bày từng phần rõ ràng. Đầu sách có giới thiệu qua cách học tiếng cho người bắt đầu. Có đủ các mẫu câu trong bài, các tình huống đối thoại ngắn áp dụng, một bài đối thoại giữa các nhân vật và bài đọc cũng như bài tập để luyện ngữ pháp. Tài liệu này dùng kèm cuốn Bản Dịch Nghĩa và Giải Thích Ngữ Pháp thì tự học không phải là quá khó.
4
139133
2015-06-29 21:23:12
--------------------------
217148
9770
Mình đang bắt đầu học tiếng nhật nên được một người chị học khoa Nhật giới thiệu cuốn này. Cuốn sách giúp mình làm quen với tiếng nhật khá tốt, vì nó được làm theo chương trình bên Nhật nên mình rất yên tâm về nội dung.
 Mình nghĩ nếu mua riêng cuốn này thì sẽ rất khó khăn cho các bạn tự học ở nhà, các bạn nên dùng chung với cuốn "Tiếng Nhật Cho Mọi Người - Trình Độ Sơ Cấp 1: Bản Dịch Nghĩa và Giải Thích Ngữ Pháp" sẽ thuận tiện hơn rất nhiều.
Và trong sách có khá nhiều bài nghe nên bạn nào muốn thì cứ trả lời ở dưới mình sẽ để lại link cho cách bạn sử dụng.
4
37970
2015-06-28 22:11:21
--------------------------
193277
9770
Đây là một quyển sách rất thích hợp cho những người muốn bắt đầu học tiếng Nhật. Đặc biệt là tự học.
Cái bài được chia theo trình tự hợp lý. Nguồn kiến thức theo thứ tự logic. 
Từ vựng khó dần theo từng bài. Có nhiều từ để sử dụng cho cuộc sống hội thoại hằng ngày.
Sách cũng là một nguồn kiến thức thích hợp để dự thi kì thi năng lực Nhật ngữ JLPT.
Tuy nhiên quyển sách này vẫn có một số lỗi sai nhỏ, tuy nhiên nó vần là một lựa chọn tuyệt vời cho việc học tiếng Nhật.
Hiện nay có nhiều trung tâm Nhật ngữ uy tính đã sử dụng quyển này là giáo trình này để giảng dạy.
5
612374
2015-05-06 23:29:02
--------------------------
190603
9770
Đây là quyển bài tập 1 trong bộ minna no nihong go - giáo trình tiếng nhật phổ biến nhất được giảng dạy tại các trung tâm và cho người tự học. Cuốn sách này gồm 4 phần chính đó là : tập dịch câu tiếng việt sang tiếng nhật, tập đặt câu với cấu trúc ngữ pháp và từ vựng trong bài, làm các bài tập để củng cố ngữ pháp và từ vựng, nghe để quen với cách phát âm và tình huống giao tiếp của người Nhật. Khi sử dụng quyển sách này các bạn không nên bỏ qua phần bài tập nào, vì mỗi bài tập có một mục đích riêng, giúp bạn nhớ ngữ pháp và từ vựng lâu hơn. Mình học theo quyển sách này và cảm thấy rất hài lòng.
5
615616
2015-04-29 20:41:55
--------------------------
142292
9770
Đây chính là cuốn sách cần thiết cho những người học tiếng Nhật khi mới bắt đầu. 
+Điểm cộng :  -Cách trình bày sách rất logic và phân bài học cũng rất hợp lý, khiến người học không bị rối.
                     -Đằng sau có sẵn đáp án các bài tập để người học không phải gặp khó khăn khi tự học.
                    -Phần phụ lục có những trợ từ cũng như từ vựng của các bài học được tổng hợp lại, để tiện cho việc ôn tập.
+Điểm trừ : Bản dịch có một số chỗ bị sai và chưa đầy đủ. 
Nhưng tổng thể thì đây là một cuốn sách tuyệt vời cho những ai muốn bắt đầu học tiếng Nhật,đặc biệt là tự học.
4
39397
2014-12-18 10:20:35
--------------------------
109784
9770
Chắc hẳn các bạn học Tiếng Nhật đang không biết phải mua cho mình cuốn sách học Tiếng Nhật nào thật hữu ích và dễ sử dụng ! Cuốn sách do chính công ty 3A Corporation phát hành ( Công ty Nhật Bản ) và được NXB Trẻ mua bản quyền.
+ Điểm tốt
1. Có 2 cuốn ( 1 bản dịch + 1 bản Tiếng Nhật ) dễ tra cứu.
2. Bài học logic, khoa học
3. Trình bày dễ hiễu, có hiragana đi kèm Kanji
+ Điểm chưa tốt
1. Bản dịch chưa được đầy đủ lắm
Các bạn hãy cố gắng đọc nó, nó rất hữu ích !
5
298441
2014-04-07 13:02:21
--------------------------
