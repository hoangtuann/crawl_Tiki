428304
8600
Lần đầu tiên mình được thưởng thức một giá trị văn hoá bất hủ một cách vui nhộn đến như vậy. Có nhiều đoạn ôm bụng mà cười. Có lẽ chính điều đó đã góp phần tạo nên thành công cho cuốn sách. Nhờ tính dí dỏm như vậy mà những kiến thức sẽ dễ tiếp thu hơn, người đọc có thể nhớ lâu hơn những dữ kiện liên quan. Đặc biệt hơn, tác giả còn có những bài tập nho nhỏ để giúp độc giả kiểm tra lại việc tiếp thu những kiến thức đã đọc. Tuy rằng mình cũng có rất nhiều cuốn viết về HY Lạp nhưng vẫn thấy không thất vọng khi rinh em này về.
5
93234
2016-05-10 23:43:51
--------------------------
421507
8600
Tớ đọc thần thoại Hi Lạp từ bé, thích nó. Tớ cũng đã mua 2 bộ Thần thoại từ 2 dịch giả khác nhau, tớ còn xem phim, tìm kiếm thêm thông tin trên mạng. Tóm lại là từ rất nhiều nguồn, cóp nhặt kha khá những thứ mà tớ cho là hay ho, làm dày thêm cái sở thích và thú vui của tớ.
Thực sự thì cuốn sách này là tớ tình cờ mua, ban đầu còn chẳng có thiện cảm mấy với cái bìa không đẹp đẽ gì của nó. Cơ mà nội dung của nó khá chất, tớ đọc được tác giả đã đề cập đến những điều trước kia tớ xem nhưng không để ý, nhìn nó bằng 1 con mắt hóm hỉnh và khác lạ hơn. Cách viết cuốn hút, trình bày đẹp, đọc thực vui vẻ, cũng thu hoạch được kha khá kiến thức.
Tớ đọc được, bạn bè của tớ - những người chẳng biết gì về Hi Lạp và thần thoại Hi Lạp đọc được, em nhỏ bé của tớ đọc được, bác tớ - nhà nghiên cứu văn hóa đọc được.
Đề cử~
4
1017745
2016-04-25 11:37:40
--------------------------
383268
8600
Tôi rất thích những câu chuyện về hy lạp. Nên lần đầu thấy nó, tối đã nhanh tay mang nó về bên mình. Nó quả là những câu chuyện đầy lí thú và hấp dẫn. Bên cạnh đó còn cho tôi thêm một kiến thức mới. Khi đọc từng trang trong cuốn sách này tôi như là một người đang phiêu lưu trong từng câu chuyện. Thật là tuyệt vời, tôi rất thích nó.Nếu bạn cũng như tôi hãy đặt nó nhé! Nó sẽ không nhàmchán như bạn nghĩ đâu. Hihi cảm ơn tác giả và tiki đã mang đến một sản phẩm như thế này. Mong rằng tiki sẽ có thêm nhiều cuốn sách hay và bổ ích nhé.
5
719986
2016-02-20 13:58:31
--------------------------
340894
8600
Sách đọc rất vui với nhiều hình minh hoạ ngộ nghĩnh. Có 10 câu chuyện về các vị thần trong thần thoại Hy Lạp được tác giả chọn lọc để kể lại dưới nhiều hình thức phong phú như đối thoại, thư từ, truyện tranh, v.v khiến mình có cảm giác như đang đọc truyện cười. Ngoài ra các phần dữ liệu kỳ thú cũng cung cấp nhiều thông tin thú vị về các nhân vật trong thần thoại Hy Lạp. Tác giả Teary Deary là tác giả nổi tiếng của loạt sách Lịch Sử Kinh Dị (Horrible Histories) nên những ai đã đọc qua loạt sách đó sẽ nhận ra lối hành văn dí dỏm vốn có của ông. 
5
470937
2015-11-20 19:15:20
--------------------------
322811
8600
Mình mua cuốn sách này nhằm cho cháu đọc để giúp cháu mình biết thêm về nhiều chuyện thần thoại, cổ tich trên thế giới. Mình cũng đắn đo nhiều giữ nhiều cuốn cùng thể loại thần thoại nhưng vẫn quyết định mua cuốn này và nó hoàn toàn phù hợp với tiêu chí của mình. Viết đơn giản, đọc dễ hiểu và chữ thì vừa đủ không quá nhỏ, rất hợp với các bé khoảng độ tiểu học khi đọc những câu văn được lược đơn giản như vầy.
Lại mua vào đợt giảm giá nên sách rẻ nữa, nên mình thích cực.
Ưng ý vô cùng luôn.
5
38943
2015-10-17 01:20:47
--------------------------
290209
8600
Mình thuộc lòng hầu hết mọi câu chuyện của Thần thoại Hy Lạp, thế nhưng mình vẫn mua cuốn này về đọc vì mình thấy nó quá hay và lạ. Những câu chuyện quen thuộc qua bàn tay của tác giả trở nên vô cùng hài hước và được biến tấu đa dạng theo nhiều hình thức: comics, lá thư, chuyển kể, hồi ký.. làm người đọc rất dễ đọc và dễ nhớ. Thế nhưng, có một lời khuyên là bạn nên đọc trước Thần thoại Hy lạp bản chính thống trước khi đọc quyển này để có một kiến thức toàn diện hơn.
4
198504
2015-09-05 10:12:29
--------------------------
284086
8600
Tôi là một giáo viên tiểu học và trong dịp đầu năm có mua một số quyển sách dạng về lịch sử, khám phá khoa học cũng như các dạng sách như thế này để cho HS tìm đọc trong tủ sách của lớp. Đây là một cuốn sách có thể nói là một cuốn sách khá thú vị và vô cùng hấp dẫn đối với các em học sinh của tôi vì ngoài cách viết khá hài hước , sách còn có những hình vẽ minh họa hấp dẫn. Các vị phụ huynh có thể cho con em mình tìm đọc quyển sách này để khơi gợi trí tò mò, lòng ham hiểu biết cũng như luyện cho các bé niềm ham mê đọc sách
4
14979
2015-08-30 16:42:25
--------------------------
