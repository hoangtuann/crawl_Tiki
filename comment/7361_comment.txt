487989
7361
Những trang sách mở ra cùng những câu chuyện nhỏ xinh, gần gụi của những năm tháng đón đọc Hoa Học Trò mỗi đầu tuần.
Những câu chuyện được chọn lọc có chung một tone màu vàng nắng chiều như thiết kế bìa, hay chính màu nắng vàng ấy chiếu xuyên suốt tới tận những trang sách cuối cùng? Có lẽ cả hai :) và cũng bởi điều này, cuốn truyện có phần nhàn nhạt và hơi đơn điệu.
Nếu là một cuốn truyện mua tặng cho con cái mình, các quý vị phụ huynh nên cân nhắc kỹ vì rất nhiều nhân vật chính trong các câu chuyện có xu hướng sống nhanh, sống gấp và sẵn sàng "cháy sáng một lần trong đời".
3
755159
2016-10-29 10:46:43
--------------------------
418647
7361
Hoàng Anh Tú được biết đến là anh Chánh Văn rất nổi tiếng của Hoa Học Trò một thời nên mình cũng có ít nhiều háo hức. Nhưng cuốn sách này thực sự chán  hơn mình tưởng. Những câu chuyện bình thường, cũng không toát lên chất Sài Gòn là mấy. Câu văn nhiều khi bị cụt lủn, không gây được cảm xúc. Mình nghĩ có lẽ cuốn này hợp với các em học cấp 2 hơn là người đã lớn hẳn như mình chăng? Bởi mọi thứ quá ngây ngô và đơn giản. Mình vẫn chờ đợi và hi vọng những gì ý nghĩa, cảm xúc sâu sắc hơn trong cuốn sách này. 
Điểm cộng lớn nhất là bìa sách đẹp, tên sách hay!
3
281738
2016-04-19 17:53:07
--------------------------
366501
7361
Cuốn này mình đã đọc được của nhà văn nhiều lần trước khi có cuốn sách bởi sài gòn nhiều nắng , được chọn lọc và in lại với nội dung không khác gì , để không bị nhàm chán thì nên lựa chọn những tác phẩm hay hơn thì sẽ ấn tượng với nhiều độc giả hơn , viết từ rất lâu rồi đưa vào thì cũng đơn giản và dễ gây hiểu lầm bởi 1 tác giả mới lạ , sách cũng có nhiều truyện ngắn nên thay đổi sắc màu của nhân vật có tính cách biệt lập .
3
402468
2016-01-10 05:58:51
--------------------------
303213
7361
Bị hấp dẫn bởi bìa sách, tựa sách, chắc hợp tâm trạng lúc đó, mình đã đặt mua. Không biết sao, mình thích cái gì có "Nắng". Chưa từng đọc truyện của Hoàng Anh Tú nên cũng muốn trải nghiệm thử xem sao. Nội dung khá ổn, những tình huống các nhân vật gặp nhau, yêu nhau, cũng rất mới mẻ. Nhưng giọng văn nhanh, tình tiết cứ lướt qua  như sẽ rồi và cảm xúc để lại trong mình không nhiều. Đọc xong phải như suy tư, trầm ngâm kết cục nhưng mình lại không thích vậy. Vì vậy, cuốn này tạm ổn với chất "Nắng" mình nhận được từ nó.
3
108421
2015-09-15 18:37:17
--------------------------
264129
7361
Bìa sách rất đẹp, dễ giữ gìn cả giấy nhìn cũng rất thích. Nhưng nội dung thì hơi nhàm chán. Là những câu chuyện ngắn về con người Sài Gòn và Hà Nội. Những mẩu chuyện tình yêu nho nhỏ với những kết thúc khó hiểu. Sau mỗi câu chuyện  Hoàng Anh Tú để lại cho người đọc những suy nghĩ riêng tuy nhiên lại nhanh chóng, chớp nhoáng khiến cho người đọc có phần hụt hẫng. Nhưng cững rất thực tế, chân thật và sống động. Nếu thích những cái kết do tự mình viết thì sẽ là 1 lựa chọn đúng cho bạn.
3
742205
2015-08-13 00:47:42
--------------------------
228071
7361
Biết đến tác giả Hoàng Anh Tú là một nhân vật nổi tiếng nhưng khi đọc quyển sách này mình lại hơi thất vọng. Bìa sách rất đẹp, bìa cứng dễ cầm và bảo quản sách, nhưng nội dung và những câu chuyện trong sách này hơi khó hiểu đối với tuổi teen. Đây đa số là những câu chuyện về tuổi đôi mươi trở lên, khi con người đã lao vào cuộc sống để bươn chải và đi tìm hạnh phúc cho đời mình. Họ yêu chớp nhoáng, chưa kịp suy nghĩ, và lại chia tay khi chưa nghĩ suy gì cả. Tuy nhiên nếu ai vừa đọc vừa suy gẫm kĩ càng thì sẽ cảm thấy cuốn sách này cũng khá hay với những hoàn cảnh và bài học tác giả gửi gắm trong đó.
2
199936
2015-07-14 22:04:08
--------------------------
198927
7361
Mình thấy cái tên Hoàng Anh Tú khá nổi, nên mình đã thử mua sách của anh. Nhưng nói thật là nó làm mình khá thất vọng.
Những truyện ngắn bên trong không có điểm nhấn. Đặc biệt là truyện "Bởi Sài Gòn nhiều nắng" được tác giả ưu ái lấy đặt tên cho sách thì lại càng làm mình không hiểu. Chắc cũng do mình không đủ trình độ để hiểu ý nghĩa của những cái kết đó. Nhưng tâm trạng chung mà mình có khi đọc quyển sách này là hụt hẫng. 
Khổ sách hình vuông khá dễ thương, bìa dày và giấy chất lượng. Nhưng có lẽ nội dung cần được tác giả đầu tư hơn nữa. Lần sau mình sẽ xem xét kĩ hơn trước khi quyết định mua sách của anh!
3
195693
2015-05-21 08:47:48
--------------------------
156094
7361
Quyển này bạn mình mua vì bìa cứng quá đẹp, tên sách thì lãng mạn, tác giả cũng nổi tiếng, nhưng đến lúc đọc thì quá thất vọng. 
Sách là tuyển tập truyện ngắn mang phong cách đặc trưng của Hoàng Anh Tú, nhiều tình tiết và dồn dập cảm xúc. Trong quyển này các truyện đa phần  viết về những mối gặp gỡ bất chợt để rồi làm người trong cuộc dùng dằng không biết phải làm sao. Đọc một, hai truyện còn thấy hay, nhưng xâu chuỗi tất cả vào một quyển thì đọc khá nhàm, không lôi cuốn. Điều không thích là kết truyện nhiều lúc làm chưng hửng người đọc, khá tụt cảm xúc.
Các bạn nên cân nhắc kỹ khi mua quyển này.
2
201502
2015-02-03 09:25:04
--------------------------
105337
7361
Nói thật nễu Bởi Sài Gòn nhiều nắng không là tác phẩm của anh Chánh Văn tôi sẽ không nài nỉ thằng bạn gãy cả lưỡi để mượn quyển sách mới toanh mà nó vừa nhận được từ tiki. Bìa cứng, ảnh xinh - cảm nhận đầu tiên và cũng là nguyên làm tôi hí hửng với quyển sách cả buổi. Mở sách ra tôi lật ngay Bởi Sài Gòn nhiều nắng, à sao nhỉ? quá chưng hửng, khéo đây không phải là truyện của anh Chánh Văn hay sao ấy. Giống như chuyện tình cấp tốc, tự dưng nhờ thử áo, nói chuyện tí sau đó hôn...có quá dễ dãi không vậy? rồi 2 hay 3 truyện kế tiếp cũng như vậy, làm tôi thất vọng liên tục. Tình tiết thì nhảy vèo vèo, và không có tí gì logic. Ngôn ngữ anh Chánh Văn dùng thì rất ok, nhẹ nhàng nhưng cũng triết lí, chỉ mỗi truyện thì không ...như mơ ạ :))) Đây chỉ là nhận xét riêng của tôi, và tôi cũng mong anh Chánh Văn có thể đọc được để có cho ra những tác phẩm hoàn hảo hơn. Thân!
1
84364
2014-01-28 07:54:30
--------------------------
104754
7361
Thoạt đầu mua quyển sách này trên Tiki, mình nghĩ sẽ được đọc những truyện ngắn mới nhất của Hoàng Anh Tú. Nhưng sau khi mua về, mình thấy đa số là những truyện cũ đã từng đăng tải trên các bộ sách "Hãy nói yêu thôi, đừng nói yêu mãi mãi" của báo Hoa Học Trò. Thậm chí có truyện đăng cách đây đã khá lâu như "Đồng nghiệp lãng mạn" (năm 2004). Những truyện ngắn này ai đã từng theo dõi các bộ sách của báo Hoa thì mình khuyên không nên mua vì bị trùng lặp khá nhiều.
3
33861
2014-01-17 21:02:50
--------------------------
