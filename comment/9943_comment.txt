239916
9943
Mình rất thích quyển sách này. Trước đây nhìn đến hình học không gian là muốn bỏ qua luôn cho rồi. Từ khi đọc quyển sách này thấy ghép trục toạ độ cũng không khó, thử làm bài tập thì không có bài nào bị sai, dần dần làm bài trôi chảy hẳn, nhìn bài tập không còn thấy sợ vì mình không biết làm nữa. Thật tuyệt khi ban đầu mình chỉ có ý định mua thử xem sao nhưng mang lại kết quả bất ngờ như vậy. Thi đại học cũng có phần này nên mình nghĩ mua quyển này ôn tốt nhé!
4
7304
2015-07-24 10:14:06
--------------------------
171179
9943
Mình rất thích học hình học nhưng rất dỡ phần tọa độ không gian. Nhưng nói thật, khi mua quyển sách này về nó đã giúp mình rất nhiều trong việc tự học và tự ôn bài phần tọa độ không gian. Lời giải khá tỉ mỉ và giải thích khá rõ ràng giúp mình áp dụng được nhiều thủ thuật giải toán. Đặc biệt còn có những công thức đặc biệt giúp mình vận dụng nhanh mà trong sách giáo khoa không có những công thức đó.
Có điều trang giấy hơi tối màu một chút, nếu không thì là một quyển sách hoàn hảo.
4
39173
2015-03-21 13:25:28
--------------------------
