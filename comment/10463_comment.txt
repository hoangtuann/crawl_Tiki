483928
10463
10 câu nói tưởng chừng như đơn giản nhưng trong cuộc sống chúng ta lại quên đi và ít khi sử dụng đến. Một cuốn sách với tựa đề khá giản dị nhưng bên trong lại đầy quyền lực. Đúng như tựa đề của cuốn sách, khi chúng ta có được thói quen sử dụng thường xuyên những câu nói đấy thì cuộc sống của chúng ta sẽ trở nên tươi đẹp, nhẹ nhàng và đáng yêu hơn. Sau khi đọc xong cuốn sách này mình đã thực hành, vận dụng 10 câu nói này vào cuộc sống và kết quả thật tuyệt vời khi cuộc sống của mình trở nên đầy niềm vui và hạnh phúc. Cảm ơn ngài Rich DeVos vì một cuốn sách rất ý nghĩa.
5
422969
2016-09-18 20:00:55
--------------------------
466215
10463
Những câu nói tưởng chừng như phải phổ biến nhất lại ít được sử dụng nhất. Những câu nói tưởng chừng như đơn giản nhất lại vô cùng ý nghĩa.
Vấn đề ở đây là tại sao bạn không sử dụng chúng, bạn ngại, bạn không dám nói, bạn nghĩ sẽ hơi xấu hổ khi nói ra. Nhưng nếu bạn không nói ra bạn chẳng là gì cả. 10 câu nói vạn năng, thay đổi được cả cuộc đời. Một trong nhưng số ít cuốn sách hay về thể loài self-help (ở tiki là sách kinh tế nhưng theo mình nó cũng là self-help).
5
256405
2016-07-01 22:45:50
--------------------------
444819
10463
Mình mua cuốn sách này lâu rồi, và hôm nay mình mới lấy ra đọc. Mình vốn trc giờ ko thích đọc sách, nhưng ko biết tại sao quyển sách này như cuốn mình vào những dòng chữ đó. Khoan nói tới nội dung, chỉ cần nhìn mục lục thôi thì mình cũng rất tâm đắc rồi. Từ tôi sai rồi đến tối xin lỗi, rồi cuối cùng là tôi yêu bạn, nó đi theo 1 trình tự nhất định. Một cuốn sách dành cho mọi lứa tuổi, mọi ngành nghề. Càng đọc mình càng đắm ìm vào nó. Cảm ơn The Secret, cảm ơn ông ngoại đã cho con biết về quyển sách này
5
1060371
2016-06-09 00:53:58
--------------------------
271132
10463
1 cuốn sách thích hợp cho tất cả chúng ta, đặc biệt là những bạn nào chưa tự tin vào bản thân mình. 10 câu nói vạn năng, 10 câu nói khích lệ tinh thần. Tuy thoạt, đọc vào tưởng chừng nó đơn giản nhưng nếu chúng ta thực hiện được nó, ta sẽ trở thành 1 người tích cực và tự tin. Trong hoàn cảnh khó khăn, hãy dùng  10 câu nói vạn năng như công cụ để giúp bạn vượt qua khó khăn đó. Đọc cuốn sách này xong, ắt hẳn, bạn đọc sẽ cảm thấy bất ngờ và cảm thấy những gì nó mang lại sẽ thật tuyệt vời.
Hãy đọc và cảm nhận.

5
100658
2015-08-18 21:24:12
--------------------------
141841
10463
Cuộc sống bây giờ gấp gáp xô bồ,nhiều lúc ta cảm thấy hoang mang với những nhiễu liệu,những cá nhân bất chấp luân lí đạo người ,những thiên tai khủng khiếp diễn ra trên thế giới.Nhưng khi đọc cuốn sách này,tâm hồn ta dịu lại ,nhẹ nhàng,và thanh thản vô ngần.Những câu nói tuy ngắn,tuy tưởng chừng đơn giản như vậy ấy mà nhiều người trong số chúng ta khó làm được thường xuyên.Cuốn sách đã đưa ta đến gốc rễ của Cuộc sống này,đó chính là tình yêu thương đồng loại,Khiến ta suy nghĩ soi chiếu thân tâm,chậm lại một chút để cảm nhận rằng cuộc sống này sẽ tuyệt vời làm sao khi ai ai cũng sống với nhau bằng một tình thương cảm mến.Thật hạnh phúc khi có cuốn sách này :)
5
456366
2014-12-16 20:59:00
--------------------------
141164
10463
Cuộc sống quá bận rộn và đôi khi cứ mải mê chạy theo nó mà ta đã vô tình đánh mất những niềm hạnh phúc nhỏ nhoi xung quanh. Vì quá bận rộn mà bạn quên khen ngợi đứa con bé bỏng của mình, quên khen ngợi những cố gắng của đồng nghiệp, quên cả lời cảm ơn cho những hi sinh vất vả của cha mẹ,...... Rồi vì áp lực, cạnh tranh cuộc sống mang lại bạn ngày một trở nên khô khan và cộc cằn hơn, ngay cả một câu khen ngợi, khuyến khích ai đó cũng tiết kiệm.........
Cuốn sách này sẽ thay đổi suy nghĩ của bạn, giúp bạn cải thiện các mối quan hệ của mình, biết trân trọng những người xung quanh hơn, yêu thương nhiều hơn. Nhưng để phát huy tác dụng của 10 câu nói vạn năng này, bạn phải nói bằng cả tấm chân tình và phải thật lòng (không nên học thuộc rồi áp dụng một cách máy móc vì người khác sẽ biết bạn đang giả vờ như vậy thì thật là tai hại)
5
381380
2014-12-14 09:48:17
--------------------------
109012
10463
Với phương châm: "Nói ra những lời đơn giản sẽ giúp bạn giảm bớt gánh nặng mà bạn phải mang trong lòng nếu cứ giữ thái độ im lặng", Rich DeVos đã đem đến người đọc một cuốn sách kỳ diệu, người đọc có thể dễ dàng cảm nhận được sự tích cực cũng như những sức mạnh mãnh liệt mà 10 Câu Nói Vạn Năng đem đến. Với hiểu biết tích lũy được từ những trải nghiệm sống và đặc biệt là lối suy nghĩ tích cực, đầy lạc quan Rich DeVos đã giới thiệu đến người đọc những lời khuyên đơn giản nhưng chứa đựng những thông điệp ý nghĩa có khả năng làm thay đổi cuộc sống.
5
6260
2014-03-26 15:29:58
--------------------------
28800
10463
Rất hiếm khi tôi tìm được một quyển sách về tâm lý ưng ý! Vì đơn giản suy nghĩ của bản thân rất khó bị tác động bởi yếu tố bên ngoài. Tuy nhiên, tôi đã phải thay đổi quan điểm ngay khi đọc quyển sách này. 10 Câu Nói Vạn Năng là quyển sách hay nói về cách ứng xử. Quyển sách đã sẽ chỉ ra cho bạn biết 10 Câu Nói Vàng trong giao tiếp với mọi người. Từ những câu nói tưởng chừng đơn giản nhưng dường như rất ít người dám thốt ra như "Tôi đã sai" hay "Tôi xin lỗi", đến những câu nói tuy ngắn gọn nhưng có một sức mạnh to lớn như "Tôi cần bạn" và "Cảm ơn".
Bạn muốn suy nghĩ và cách ứng xử tốt đẹp hơn? Hãy đọc 10 Câu Nói Vạn Năng, và "bạn có thể làm được"!
3
16713
2012-05-31 21:18:50
--------------------------
26409
10463
Mình mua quyển sách này với một hy vọng là mình có thể theo đuổi 1 cái nhìn tích cực hơn về cuộc sống bởi vì lúc đó, mình nhận ra rằng mình đã đi quá nhanh, đã quá tiêu cực, bi quan. Và khi đọc quyển sách này, từng phần, từng mặt chữ đều thấm đượm vào con người mình và những suy nghĩ, hành động cũng như cách nói chuyện của mình đã dần thay đổi.Tôi đã phải thay đổi quan điểm ngay khi đọc quyển sách này. 10 Câu Nói Vạn Năng là quyển sách hay nói về cách ứng xử. Quyển sách đã sẽ chỉ ra cho bạn biết 10 Câu Nói Vàng trong giao tiếp với mọi người. Từ những câu nói tưởng chừng đơn giản nhưng dường như rất ít người dám thốt ra như "Tôi đã sai" hay "Tôi xin lỗi", đến những câu nói tuy ngắn gọn nhưng có một sức mạnh to lớn như "Tôi cần bạn" và "Cảm ơn".
5
38001
2012-05-08 18:42:52
--------------------------
19023
10463
- Mình nhận ra ngay quyển sách này vì 1 người bạn ở bên Mỹ đã khuyên mình nên đọc vì nó sẽ giúp ích mình rất nhiều. Mặc dù vậy, mình vẫn quên đi mất. Đã lướt qua Tiki, đặt 1 vài đơn hàng mà vẫn không nhớ đến quyển sách người bạn khuyên đọc. Bất chợt vào 1 ngày đẹp trời, mình đã nhìn thấy quyển sách và tự nhiên có cảm giác rằng, mình phải mua quyển sách này ngay lập tức.

- Rich DeVos không chỉ là người đồng sáng lập công ty Amway mà là một trong những người có ảnh hưởng nhất ở miền Tây Michian. Mình không rõ lắm về đời tư hay tiểu sử của Rich DeVos, nhưng nhờ người bạn của mình, mình biết được những ảnh hưởng của ông ở Grand Rapids và thấy rằng ông là 1 nhà lãnh đạo tài ba. Mình đã thử mua quyển sách này để xem mình có thể áp dụng được điều gì vào trong cuộc sống hằng ngày của mình không.

- Mình mua quyển sách này với một hy vọng là mình có thể theo đuổi 1 cái nhìn tích cực hơn về cuộc sống bởi vì lúc đó, mình nhận ra rằng mình đã đi quá nhanh, đã quá tiêu cực, bi quan. Và khi đọc quyển sách này, từng phần, từng mặt chữ đều thấm đượm vào con người mình và những suy nghĩ, hành động cũng như cách nói chuyện của mình đã dần thay đổi.

- Mình thực sự tin rằng DeVos đã thực sự chạm vào cuộc sống của mình thông qua quyển sách này. Mình luôn mang theo quyển sách này bên cạnh để có thể tham khảo một cách thường xuyên và để chắc chắn rằng mình đang sử dụng 10 câu nói trong quyển sách này một cách đúng đắn nhất đối với mỗi người, mỗi việc…

- Và mình muốn nói rằng:
          “Cám ơn” DeVos (câu nói số 6)
          “Tôi cần bạn” quyển sách này (câu nói số 7)
và
          “Tôi yêu bạn” Tiki (câu nói số 8)

5
17933
2012-02-10 20:14:59
--------------------------
16135
10463
Rất hiếm khi tôi tìm được một quyển sách về tâm lý ưng ý! Vì đơn giản suy nghĩ của bản thân rất khó bị tác động bởi yếu tố bên ngoài. Tuy nhiên, tôi đã phải thay đổi quan điểm ngay khi đọc quyển sách này. 10 Câu Nói Vạn Năng là quyển sách hay nói về cách ứng xử. Quyển sách đã sẽ chỉ ra cho bạn biết 10 Câu Nói Vàng trong giao tiếp với mọi người. Từ những câu nói tưởng chừng đơn giản nhưng dường như rất ít người dám thốt ra như "Tôi đã sai" hay "Tôi xin lỗi", đến những câu nói tuy ngắn gọn nhưng có một sức mạnh to lớn như "Tôi cần bạn" và "Cảm ơn".
Bạn muốn suy nghĩ và cách ứng xử tốt đẹp hơn? Hãy đọc 10 Câu Nói Vạn Năng, và "bạn có thể làm được"!
4
16713
2011-12-17 11:09:44
--------------------------
