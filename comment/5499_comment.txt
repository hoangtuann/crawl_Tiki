541149
5499
Bìa sách cầm trên tay mới thấy là siêu dễ thương nhé, nhờ bookcare của tiki mà càng xinh xắn hơn ý. Câu chuyện bên trong của hai nhân vật cũng siêu dễ thương, nàng xinh đẹp còn chàng hào hoa, lại có chút sủng, mấy ngày nghỉ rảnh rỗi được nằm ôm sách xơi bánh nữa thì còn gì bằng.
5
829200
2017-03-14 01:49:46
--------------------------
539797
5499
Cá nhân mình cảm thấy " độc dược phòng vé" này rất hay... truyện không phải ngược nhưng mỗi lần đọc những cảm xúc của nữ chính lại thấy hơi đau lòng.Nam chính thì có vẻ phải làm cho thể thảm hơn xíu nữa mới thấy thỏa mãn được kakaka 😂😂😂
5
1426067
2017-03-11 19:42:11
--------------------------
528145
5499
Mình thích cuốn này lắm, bởi giọng văn hài hước của tác giả và tính tình của nữ chính phải nói là bá đạo. Còn về phần giao hàng thì khỏi chê, Tiki bọc sách kiến cẩn thận đóng gói, giao hàng lại đúng hẹn.
4
1214779
2017-02-19 21:48:57
--------------------------
492585
5499
Mình biết đến tác giả Ức Cẩm qua truyện Có cần lấy chồng không, Con thỏ bắt nạt cỏ gần hang nên đã mua thử bộ Độc dược phòng vé về đọc. Cốt truyện thì cũng khá đơn giản. Thế giới showbiz ở đây không quá phức tạp như các truyện khác, không đề cập nhiều đến các vấn đề nguời thứ 3. Phong cách đơn giản, nhẹ nhàng.
4
1409645
2016-11-25 12:03:34
--------------------------
469257
5499
Đây là một câu chuyện khá hay và hài hước. Tôi thấy nữ chính khá thú vị và có tài trong việc diễn xuất đấy chứ, nhiều đoạn còn khiến tôi lăn ra cười. Bên cạnh đó, tôi cũng hiểu rõ hơn về các mặt sáng tối trong ngành giải trí, thú thật, tôi cũng hơi bất ngờ bởi bình thường tôi cũng không quá quan tâm đến ngành này, nó thật sự là một môi trường cạnh tranh khốc liệt. Tuy nhiên, cái bí mật ở phần sau câu chuyện khiến tôi không mấy hài lòng bởi nó có chút đột ngột và không hợp logic lắm.
4
685968
2016-07-06 10:46:50
--------------------------
397556
5499
Truyện Ức Cẩm lúc nào cũng nhẹ nhàng và dịu dàng như thế...
Tình yêu trong truyện Ức Cẩm là thứ tình cảm ổn định, không có ngược tâm hay tiểu tam các thứ...
Nhưng truyện của Ức Cẩm luôn rất thu hút người đọc.
Có thể là do văn phong trong sáng, cốt truyện nhẹ nhàng nhưng không gây nhàm chán.
Như cuốn sách này, câu chuyện tình yêu của anh chàng tổng tài ít nói với cô diễn viên vô tư hết lòng vì người thân.
Dù cho đến cuối mình vẫn không hiểu nam chính thích nữ chính khi nào, dù cái kết có chút nhanh, không có ngoại truyện về sau nhưng nó vẫn là một câu chuyện hay...đáng để đọc!!!
4
768377
2016-03-15 08:54:11
--------------------------
396245
5499
Độc dược phòng vé của tác giả Ức Cẩm là câu chuyện kể về nữ diễn viên Bạch Mạch Nhiên và cả những bí mật đằng sau sự hào nhoáng của ánh hào quang trong giới showbiz. Thẩm Lâm Kỳ là bạn trai của cô, nhưng thái độ của anh với cô lại có phần hơi xa cách. Trong một buổi tiệc, cô trả đũa một ông chủ sản xuất phim bằng cách gọi cho vợ ông ta đến đánh ghen rồi sau đó thản nhiên ném điện thoại của lão xuống sân. Thật không may, hành động này của cô lại có người nhìn thấy, hơn nữa đó lại là chủ của chiếc xe- Kiều Minh Dương. Ba người họ rốt cuộc là ai đến với ai?
4
632796
2016-03-13 08:39:46
--------------------------
394241
5499
Trong giới giải trí đầy cám dỗ nhưng cũng rất đơn giản, những cô diễn viên xinh đẹp không có tài lực được giới truyền thông ví von như những bình hoa di động thăng trầm là do người đứng sau cô ta có đủ lớn hay to hay không, vai diễn hay thứ bậc phù dù gì đó phải xem hầu bao của các đại gia có lớn không. Đây không còn là đề tài quá mới về sự thăng trầm nhờ tuổi trẻ trong làng giải trí. Nhưng với giọng văn hài hước, câu chuyện đời của nữ chính cũng như cách cô ta chuyên tâm tự nhấp mình bước ra khỏi cái danh hiệu chẳng mấy mĩ miều. Mối quan hệ yêu thương chẳng vào đâu của ông to đằng sau cô ta cho bạn đọc những phút giải trí thú vị
3
902274
2016-03-10 00:46:45
--------------------------
389509
5499
Truyện hay và hài hước, tình tiết phát triển vô cùng hợp lí. Mình đã đọc xong cuốn này trong 1 đêm, và thực sự không thể rời mắt khỏi dù chỉ một giây. Truyện thực sự vô cùng hấp dẫn, gây cười cho độc giả, cũng hơi thực tế một chút. Mình nghĩ nếu ai đọc ngôn tình thì nhất định phải thử đọc qua truyện này. Ức Cẩm là một tác giả có giọng văn vô cùng mượt, và nói thật là ít có ai viết được ngôn tình hiện đại mà thu hút độc giả đến từng nét chữ, câu văn thế này. Còn về phần giao hàng thì chất lươngh khá tốt, giao đến tận nhà, sách còn bọc mới tinh, không hề có nếp gấp giở sách, chữ in đẹp, không bị mờ. Nói chung là không có gì phải chê :)
5
1175991
2016-03-02 09:41:00
--------------------------
388451
5499
Bìa truyện thiết kế theo kiểu hình vẽ chibi ngộ nghĩnh và đáng yêu. Nội dung truyện cũng khá ổn, tuy không mới lạ hay quá đặc sắc gì nhưng cũng coi như dễ thương. Tình tiết nhẹ nhàng dù xoay quanh chủ đề showbiz, giọng văn cực kỳ hài hước ngọt ngào, sủng cực nhiều, cũng có nhiều tình tiết gây cười. Nữ chính thuộc kiểu tiểu bạch ngốc nghếch ngây ngô, bình thương mình vốn không thích kiểu con gái này lắm đâu nhưng đặt vào hoàn cảnh truyện này lại không thấy phản cảm chút nào hết. 
4
630600
2016-02-28 23:35:28
--------------------------
348349
5499
Mình rất thích đọc truyện của Ức Cẩm vì nó nhẹ nhàng lôi cuốn nhưng không thiếu phần cao trào. Độc dược phòng bán vé khiến mình cảm thấy hơi nhạt, lúc đầu khi đọc mình có rất nhiều thắc mắc và hơi vô lý, nhưng cái kết làm mình rất hài lòng!  Thẩm Lâm Kỳ luôn che chở, bảo vệ và giúp đỡ " độc dược ", một cô gái hy sinh bản thân vì gia đình, vì em trai để rồi bước chân vào showbiz đầy phức tạp- Bạch Mạnh Nhiên. Cô hơi ngốc nhưng nhờ tình cảm của chính bản thân và sự cổ vũ không ngừng của Lâm Kỳ, nó đã khiến cô dần thay đổi bản thân để có thể sánh bước cùng anh, mặc cho những sự dị nghị của thế giới showbiz đầy thị phi này. Kết thúc truyện bộc lộ hết những thắc mắc về mối quan hệ phức tạp của cặp đôi này, một cái kết hay và đẹp.
5
949408
2015-12-05 23:12:40
--------------------------
338699
5499
Độc Dược Phòng Vé của Ức Cẩm là một cuốn tiểu thuyết khá hài hước nhưng cũng đầy sức động có tính chât thật trong đời thường của giới giải trí hiện nay văn phong khá vui nhộn và hài hước khiến cười bò câu truyện kể về cuộc đời của một diễn viên không nổi nhưng khi có hậu thuẫn là người có chức có quyền trong giới giải trí đã khiến cuộc đời của Bạch Mai Nhiên thay đổi hoàn toàn còn về nam chính soái ca thì không cần bàn cãi là có nhà có xe chưa vợ nói tóm lại đây là một cuốn tiểu thuyết hay
5
673238
2015-11-16 19:11:40
--------------------------
333911
5499
Nữ chính biết thân biết phận,không có chút thành tựu,về cơ bản thì...bình thường đến không thể bình thường hơn.Ấy vậy mà đời cô vô cùng rực rỡ:cô được một 'mặt trời' chiếu sáng.
'Mặt trời' là ai?Xin thưa:Thẩm Lâm Kỳ.Đúng là số trời cho,Mạn Nhiên có duyên với anh,được anh bao che,yêu thương giúp đỡ.Nói về thực lực thì cô này chẳng bằng một góc của anh chàng ngoài đời thực,may sao đây là ngôn tình,đọc những suy nghĩ bựa đến không thể bựa hơn của cô nàng cũng làm tôi phải bật cười.Vả lại,'anh không phải cá,sao biết niềm vui của cá',mạn Nhiên từ chỗ lạ thành quen với Lâm Kì,chắc hẳn,cô chính là người mà anh cần,cô chính là độc dược của lòng anh.
Nội dung khá đơn giản,giọng văn dễ thương,dễ hiểu,gây cười cho người đọc.Dù truyện không khai thác sâu về chuyện nghề nghiệp của Mạn Nhiên nhưng nó cũng đã mô tả khá chân thực về giới showbiz đầy rẫy những thị phi.Các tình huống truyện khá là đa dạng,cách kể linh hoạt,diễn tả nội tâm nhân vật rất tốt,đặc biệt là nữ chính.
Đọc xong truyện này chỉ muốn phát biểu cảm tưởng bằng một câu:sao người ta lấy được ông chồng hoàn mỹ thế hở trời!

3
487743
2015-11-08 15:21:52
--------------------------
321239
5499
Dù không quá hay nhưng Ức Cẩm ,cô ấy thực sự đã viết nên một câu chuyện đáng yêu chết đi được.Dù người ta có ngán ngẩm mô tuýp tiểu bạch-tổng tài đi nữa thì tôi vẫn thích.Chẳng ai may mắn bằng nữ chính đâu,gặp một soái ca hoàn hảo thì mình có không bằng ai cũng vẫn được yêu thương.Tôi đã mua độc dược phòng vé để tặng sinh nhật bạn,nó thích lắm bởi vì nó cũng có chút tiểu bạch.Nhưng mà nó cũng khá là may mắn giống nữ chính khi gặp được một anh khóa trên như soái luôn.
5
349523
2015-10-13 15:32:14
--------------------------
303434
5499
Có ai mua về rồi đọc mà thấy buồn cười như mình không vậy? Trời ơi truyện đáng yêu vật vã =_= Dù dạo này chuyển qua đam mỹ nhiều hơn ngôn tình rồi nhưng đọc truyện này mình vẫn thấy rất được. Truyện có bìa ngoài khá dễ thương cũng như nội  dung của truyện vậy. Đặc biệt là Thẩm Lâm Kỳ và Bạch Mạnh Nhiên cũng rất xứng đôi, cả mấy cp phụ  nữa, cũng rất hài hước. Nói chung là mình khá ưng ý với cuốn sách. Hơi buồn là sách của mình bị rách một ít ở bên trong nhé T^T.
5
578513
2015-09-15 20:54:17
--------------------------
272613
5499
Lúc đầu mình thấy ấn tượng nhất chính là tên truyện “Độc dược phòng bán vé” và thiết kế bìa sách theo kiểu chibi rất đáng yêu. Nhưng khi đọc thì nội dung của truyện này chỉ ở tầm trung thôi, không hay nhưng cũng không dở. Giọng văn của tác giả rất mượt mà, chau chuốt và khá hay, còn motip truyện lại không mới mẻ lắm; tình tiết nhẹ nhàng quá nên thiếu những cao trào cần có và truyện này nói về giới showbiz nhưng rõ ràng là không đủ sâu. Truyện này chỉ hợp để đọc giải trí thôi.
4
562431
2015-08-20 10:37:31
--------------------------
251817
5499
Mình order sách cũng khá lâu nhưng bây giờ rãnh rỗi mới ngồi lấy ra đọc. Bìa có hình vẽ Thẩm Lâm Kỳ và Mạch Nhiên rất đáng yêu, tựa truyện "Độc dươc phòng vé" khá thú vị và có tí châm biếm, vừa nhìn vào đã biết trong cuốn truyện này không có yếu tố "ngược" mà toàn "ngọt" thôi. Một diễn viên tiểu Bạch trẻ trung, khôn lõi, ranh mãnh và đáng yêu chỉ xếp hạng Trung trên màn ảnh nhỏ sánh đôi cùng một Thẩm ca kiệm lời, khó hiểu, giàu có nhưng lại có tài trí và hết lòng "hậu thuẫn" cô tình nhân tinh nghịch. Mô típ cũ : nữ tiểu bạch và nam băng lãnh. Truyện nhẹ nhàng, thích hợp cho những sắc nữ mê ngọt ngào và sủng ái. Đọc trên tinh thần giải trí là chính.              
3
606118
2015-08-03 10:35:29
--------------------------
245664
5499
Truyện của Ức Cẩm đa số đều HE , đề dễ thương như vậy . Nhưng mỗi cuốn đề có nét thú vị riêng . Cực kì thích Tiểu Bạch , dù có hơi ngây thơ một tí nhưng lại mạnh mẽ bên trong khi quyết định bỏ đi . May mà soái ca Kỳ ca ca đã chịu gian khổ để dụ được Tiểu Bạch về . Mô tuýp truyện tuy quen thuộc kiểu đại gia - gái ngố không tài cán gì nhưng lại không hề nhàm chán
Thêm một điểm cộng nữa là bìa đẹp và dễ thuông . Nên đọc 
5
90456
2015-07-28 23:31:22
--------------------------
229032
5499
Cuốn truyện đọc để giải trí thì rất hay, mình bị cuốn hút với tên truyện "Độc dược phòng vé" nghe nó vui vui nên quyết định rinh em nó về. Đọc truyện mình luôn mang theo cảm giác thoải mái, truyện không có cao trào gì nhiều, nút thắt trong truyện cũng giải quyết đơn giản, nhưng mình rất thích hành động và đối thoại của nam/ nữ chính, hai người họ dễ thương quá! Cuối truyện mình cũng hơi ngạc nhiên bởi em của nữ chính không phải em ruột. Chất lượng truyện thì khá được, bìa trơn láng in hình và chữ nổi, dù bìa hơi mỏng, thích hình chibi ngay bìa, dễ thương.
3
522674
2015-07-16 13:10:44
--------------------------
228812
5499
Mình mua truyện vì tên tác giả, hình bìa dễ thương, nhưng mà cỡ chữ, font size chữ hình như có vấn đề, huhuhu, hay tại mắt mình dạo này đọc truyện nên căng ra ta ? Cỡ chữ khá nhỏ, cách chữ hẹp, mình đọc mà mắt cứ hoa lên hết đi.
Nội dung truyện nhẹ nhàng, dễ thương. Nữ chính tươi trẻ, nam chính thâm trầm. Nhưng thật sự là giọng văn của tác giả (hay lỗi do dịch giả) rời rạc. Cách diễn tả câu chữ gây khó hiểu lắm. Vài đoạn mình vừa đọc qua, lại phải đọc và ngẫm mới hiểu. Nói chung tạo hình nhân vật, tính cách nhân vật, cốt truyện đều không lạ ạ. Quyển truyện đọc cho vui, đọc để GIẢI TRÍ đơn thuần thì cũng được. Không đến nỗi tệ hại đâu ạ.
4
489524
2015-07-15 23:44:26
--------------------------
221785
5499
Giọng văn nhẹ nhàng , tình tiết chậm thích hợp để giải trí . Độc dược phòng vé cùng với hai nhận vật chính là Bạch Mạnh Nhiên - là diễn viên chuyên được đóng vai chính nhưng diễn xuất thì cực kì tệ , vì sao ? Vì sao cô ấy lại chuyên đóng vai chính ? Có một bí quyết đó chính là cô có người chống đỡ , có hậu thuẫn . Mà hậu thuẫn là ai ? Chính là nam chính Thẩm Lâm Kỳ . Hai nhân vật này đã phản ánh được chút ít thế giới hậu trường sau màn bạc của diễn viên . Có điều tình tiết hơi rời rạc , nam chính quá lạnh nhạt ở khúc đầu , mình bực nhất là lúc nữ chính bị lợi dụng mà nam chính vẫn đứng đó xem như không có gì xảy ra , cũng may có Kiều thiếu cứu . Nhưng tóm lại truyện cũng tạm , có nhiều khúc gây cười thích hợp để giải trí , xả stress !
4
351263
2015-07-04 13:37:32
--------------------------
217684
5499
Diễn biến chậm, nhẹ nhàng phù hợp đọc giải trí cuối tuần, phần nào phản ảnh hậu trường của giới diễn viên cũng lắm chiêu trò nhưng cũng nhiều gian khổ, nếu không phấn đấu thì sẽ bị đào thải và quan trọng là cần có chỗ dựa làm hậu thuẫn vững chắc. tuy nhiên nội dung thiên về yếu tố tích cực.
Có một tình tiết làm mình không đồng ý là em trai của nữ chính trở thành em ruột của nam chính thiếu tính hợp lý, nhưng chi tiết này lại được tác giả đẩy lên thành cao trào khiến nam nữ chính phải xa nhau. Nhưng không sao, cũng có thể cười nhẹ nhàng khi đọc truyện.
3
583423
2015-06-29 19:50:53
--------------------------
213555
5499
Tình tiết không mới mẻ lắm nếu không phải nó là quá bình thường. Nữ nhân vật chính xinh đẹp, nam chính luôn tìm cách bảo vệ, giải quyết mọi rắc rối cho nữ chính. Nội tâm của nhân vật không được khai thác triệt để nên đọc mà nhiều lúc chả hiểu mô tê gì,lời văn không đủ sức thuyết phục, mạch truyện rời rạc không súc tính lắm. Đọc để giải trí thì ok nhưng mình đã đọc quá nhiều ngôn tình và thể  loại như thế này thì thực sự quá nhàm nên không để lại cảm xúc mấy, cao trào và đột phá cũng không có mấy. Thực sự hơi thất vọng.
3
255987
2015-06-23 21:43:56
--------------------------
202472
5499
Cốt truyện cũng khá hay nhưng lời văn của tác giả thì chưa được hay lắm, mình thấy nó cứ dài dòng thế nào ấy. truyện cũng có vài phần kịch tính nhưng cũng được giải quyết mau lẹ, chưa gây sâu sắc lắm. Nhưng hai nhân vật nam nữ chính thì cũng khá dễ thương, cũng vẫn có một vài tình tiết thú vị. Nói tóm lại thì đây là một câu chuyện nhẹ nhàng vừa đủ, thích hợp cho các bạn mới đọc ngôn tình, chứ với những người đã đọc nhiều thì truyện hơi bị nhạt. Bìa truyện vẽ rất dễ thương và đáng yêu, thu hút người đọc
4
507583
2015-05-29 17:42:51
--------------------------
198811
5499
Theo ý kiến cá nhân mình thì tác phẩm Độc dược phòng vé này của Ức Cẩm cũng khá là được, nhưng truyện này lại chỉ mang lại tính chất giải trí cho người đọc thôi. giọng văn khá rời rạc, tình tiết khá nhàm chán và lối viết chưa mới mẻ, chưa mang lại sức hút cao cho người đọc, nhất là truyện cũng chưa có những nút thắt cố định để tạo điểm nhấn cho câu truyện trong lòng người đọc. Sự miêu tả của tác giả cũng như logic về tâm lí của các nhân vật không được hợp lí cho lắm, nhất là kiểu thay đổi cảm xúc của nam chính dành cho nữ chính. Lúc đầu thì lạnh lùng, không quan tâm, thậm chí thờ ơ vậy mà đùng phát một lúc sau lại đối xử nhẹ nhàng kiểu thích rồi khiến mình không thích lắm. Nói chung truyện này chỉ bình thường thôi!!!
3
573527
2015-05-20 21:09:58
--------------------------
184839
5499
Độc dược phòng vé là một tiểu thuyết có tính giải trí. Từ đầu đến cuối truyện luôn duy trì cảm xúc nhẹ nhàng, không có gì nổi bật. Nhân vật nam chính cũng giống như các nam thần trong truyện khác: đẹp trai, lạnh lùng, tài giỏi. Nhân vật nữ chính thì xinh xắn, ngây thơ. Tuy nhiên mình cảm thấy giọng văn của Ức Cẩm trong truyện này không được tốt lắm. Rõ ràng phần đầu nam chính đối xử với nữ chính rất lạnh nhạt, thậm chí còn đứng nhìn khi nữ chính bị lợi dụng. Nhưng chưa có sự chuyển biến gì đã đùng một cái đối xử với nữ chính nhẹ nhàng hơn. Cách viết không trôi chảy, các tình tiết rời rạc không có sự liên kết khiến cho người đọc thấy nhàm. Nhất là bí mật về nam chính và em trai nữ chính tự nhiên phần cuối lòi ra mà không có dự báo trước càng làm cho ta không thích. Tóm lại truyện chỉ thích hợp để giải trí thôi.
3
484991
2015-04-18 13:13:28
--------------------------
183814
5499
Hay ,nhiều đoạn rất vui ,làm tôi cười ha hả ,nhưng có vài khúc tình tiết khá rời rạc ,không ăn khớp với nhau lắm và làm cho người đọc cảm thấy khá là nhảm ! (Ý kiến riêng thôi) . Bìa đẹp ,hình in nổi ,lúc đầu thấy bìa sách tôi cứ tưởng lă nó sến súa lắm ,rồi tôi chú ý đến vết gân "tức giận" trên mặt cô nàng ,và thế là hốt luôn không do dự =]] bookmark còn khá mỏng nhưng cũng dài nên vẫn khá là tốt (so vớ vài cuốn khác) . À ,cái kết thì không đặc sắc gì mấy ,HE nhưng mà tôi nghĩ nên "đột phá" thêm một chút nữa thì sẽ thú vị hơn ,phần kết và phần mở đầu nữa . Hợp với giải trí .
4
538448
2015-04-16 18:18:51
--------------------------
181538
5499
Đây là một cuốn truyện rất hay với kiểu nội dung nhẹ nhàng nhưng vẫn lôi cuốn người đọc. Vẫn trong thế giới showbiz, trong cuộc chiến của các người nổi tiếng, người có hậu thuẫn mạnh hơn luôn luôn thắng. Nhưng điều làm câu truyện này đặc biệt hơn hết là họ yêu nhau. Nữ nhân vật chính hài hước và dí dỏm, luôn gây rắc rối rồi lại để anh phải dọn dẹp cho mình. Thỉnh thoảng khi hai người cãi nhau là tim mình như muốn rơi ra luôn ý. Còn những khúc họ đấu khẩu với nhau là đọc mà cười đau cả bụng. Là một cuốn sách rất hay để giải trí.
5
591187
2015-04-12 01:03:00
--------------------------
177753
5499
Phong cách của cuốn sách này là một tình yêu nhẹ nhàng trong một thế giới hỗn tạp. Có thể tư duy của tác giả về giới giải trí còn đơn giản, nhưng mình thực sự thích câu nói của chị Bạch khi đứng trước sự dồn ép của giới truyền thông - một sự phản kháng tất yếu.  Nói đến tình yêu của nhân vật chính, có thể nói là lâu ngày sinh tình. Định mệnh kéo hai người lại gần nhau, và họ đã quyết định giao trái tim đến gần nhau. Phải nói một điều là tư duy của nữ chính có thiên hướng... khụ khụ.... biến thái!!! Chị hay có những suy nghĩ như tên lửa phóng. Vì vậy lời khuyên là đừng ăn uống gì trong lúc đọc truyện nhé! =))
4
209069
2015-04-03 19:56:45
--------------------------
176088
5499
Lúc đầu nhìn cái bìa là mình đoán truyện sẽ được đi theo chiều hướng nào rồi :3 Quả thật mình đoán đúng truyện có lối văn rất nhẹ nhàng, hài hài, lâu lâu có những câu thoại khiến mình không khỏi bật cười. Cả nam nữ chính đâu không phải chịu gì nhiều cái ngược, tâm trạng đa số đều rất vui vẻ nên khi đọc cảm thấy không quá đai lòng mà cảm thấy thoải mái thư giãn. Nhưng cả hai vẫn không có gì mới so vơi các nhân vật ngôn tình khác. 
Một điểm không cuốn hút của truyện chình là mình cảm thấy không có cao trào mạch truyện cứ đều đều, nói về giới showbiz nhưng có vẻ gì đó quá đơn giản dễ dàng nên mình không thích lắm . Mà thực ra truyện này mình yêu anh nam phụ hơn thích Kiều Minh Dương ! Không hiểu sao nam chính không gây ấn tượng cho mình :)) 
Nhưng đây cũng là một câu chuyện thú vị xem giải trí rất phù hợp !
3
103513
2015-03-31 17:01:48
--------------------------
173545
5499
Độc Dược Phòng Bán Vé là một câu chuyện khá hay và đáng yêu, có tính giải trí cao. Nhiều khúc khá hài hước, lại có nhiều đoạn vui vui ngố ngố và ngọt không thể tả được.
Tình yêu của đôi nam nữ chính khá đáng yêu và nhẹ nhàng, giống như một cây hoa lặng lẽ, âm thầm nở rộ cho đến khi tỏa hương khoe sắc, ngọt ngào không thể chê vào đâu.
Mình khá thích nữ chính Mạch Nhiên, tự lập và lý trí, biết thân biết phận, biết vươn lên, biết mạnh mẽ. Nam chính si tình và đáng yêu, tình yêu của anh sâu đậm vô cùng... Một bộ truyện khá, giải trí tốt.
4
382812
2015-03-26 13:46:34
--------------------------
170495
5499
Cốt truyện hay, khá độc đáo khi viết về thế giới showbiz, xen lẫn yếu tố hài hước, văn phong tác giả Ức Cẩm trau chuốt. Mọi thứ kết hợp hài hòa làm nên câu chuyện đặc sắc.
Nữ chính Bạch Mạch Nhiên là diễn viên, nhìn thì có vẻ ngây thơ nhưng thật sự là 1 người rất tinh quái, khiến mọi người điêu đứng nhiều phen :v Cô có Thẩm Lâm Kỳ - giám đốc công ti giải trí - làm hậu thuẫn. 
Tình yêu của nam chính và nữ chính thật nhẹ nhàng, sâu lắng nhưng lại có phần hơi ảo, không được chân thật cho lắm. Đọc giải trí thì lại rất ok nhé :))
3
487638
2015-03-20 07:56:55
--------------------------
163922
5499
Truyện rất vui nhộn, tình cảm của 2 người lúc đầu rất đáng yêu, mình rất thích những nhân vật có tính cách như Lâm Kỳ, ngoài lạnh lùng, trong ấm áp. Mẫu nam chính điển hình của ngôn tình nhỉ!!!
Tuy nhiên có 1 điểm trừ mình rất ko hài lòng là ở cuối truyện lại cho xảy ra 1 sự cố và mình thấy cách xử sự của Mạch Nhiên rất vô lý, hình như là biết rằng đó ko phải em trai ruột của mình nên tức quá ko biết làm gì thế là đổ hết lên đầu Lâm Kỳ, rõ ràng trước đây đã biết đó ko phải em trai mình, thì lúc sau này biết cũng chẳng có lý do gì để bực tức và hận Lâm Kỳ như vậy khi chính anh là người đã chăm lo cho cuộc sống của 2 chị em quá hoàn hảo rồi. Thử hỏi nếu lúc đầu ko có Lâm Kỳ bao bọc thì cuộc sống của Mạch Nhiên có được sung sướng như bây giờ!!!
Có nhiều nét tương đồng với phim Thiên Kim Nữ Tặc nhỉ!! ^^
4
460759
2015-03-06 10:05:22
--------------------------
159672
5499
Đọc truyện này để xả stress rất tốt, truyện hài hước và có nhiều tình tiết khá lạ , truyện nói về giới showbiz , nam chính đúng hình tượng soái ca đẹp trai, nhà giàu, bá đạo. Còn nữ chính Bạch Mạch Nhiên cũng không phải dạng tiểu bạch thường thấy mà có phần tinh quái, tuy nhiên có cái mình không thích là ở phần cuối truyện nữ chính có vẻ yếu đuối hơn đoạn đầu. Ức Cẩm xây dựng các tình tiết truyện khá tốt nhưng về phần quan trọng là tình yêu của nam nữ chính có phần gượng gạo, không hợp lý cho lắm. Về hình thức, bìa rất đẹp nhưng vì nền màu trắng nên dễ bị bẩn.
4
561573
2015-02-18 10:33:02
--------------------------
158793
5499
Độc dược phòng vé có thể nói là tác phẩm hội tụ đầy đủ nhất phong cách Ức Cẩm: nhẹ nhàng, đáng yêu, lãng mạn và hài hước không đỡ được. Mượn một đề tài quen thuộc "thế giới showbiz", thế giới của ánh hào quang và nhiều khoảng tối nhưng Ức Cẩm vẫn khiến tác phẩm hấp dẫn.Tôi ấn tượng nhất là nữ chính Mạch Nhiên và anh chàng tài tử Kiều Minh Dương. Bạch Mạch Nhiên đúng thật là một bình hoa nhưng là một bình hoa có suy nghĩ và ra sức nỗ lực để khẳng định mình qua từng vai diễn. Tôi lại không ấn tượng lắm với nam chính Lâm Kỳ bằng anh nam phụ Kiều thiếu. Anh rất dễ thương và hài hước, chỉ tiếc là không thấy tác giả ghép đôi anh với ai hết ngoài mối tình như có như không với Mạch Nhiên (không biết có phải không hay do tôi tưởng tưởng, tôi vẫn thấy Kiều thiếu thích Mạch Nhiên). Bìa truyện của Amun quá dễ thương rất phù hợp với nội dung câu chuyện.
3
114409
2015-02-13 22:39:04
--------------------------
157575
5499
Đây là cuốn sách đầu tiên mà tôi đọc của Ức Cẩm. Không biệt những truyện khác của chị như nào nhưng đây là một cuốn tiểu thuyết khiến tôi rất hài lòng. Từ bia sách đến nội dung đều rất hài hước, đáng yêu. Tuy nhân vật nam chính vẫn là hình mẫu soái ca quen thuộc của ngôn tình nhưng nữ chính lại khá đặc biệt.
 Diễn biến truyện có cao trào gây hứng thú cho người đọc. 
Những suy nghĩ của nữ chính rất thú vị và...dị. Văn phong của tác giả rất trôi chảy.
 Tuy vậy nhưng nhiều chỗ có lẽ là do tác giả quá trau chuốt, tỉ mỉ thành ra lại dài dòng quá :v
4
445522
2015-02-09 14:32:13
--------------------------
152072
5499
Một quyển sách tạo được điểm nhấn từ phần bìa, nhìn vào đã tạo ra một cảm giác rất đáng yêu và hài hước!
Và câu chuyện mà Độc Dược Phòng Vé mang đến không làm mình thất vọng! Kết hợp giữa tình cảm và hài hước, Ức Cẩm đã vẽ nên một tình yêu thật đẹp nhưng không kém phần hài hước, "bá đạo" của Thẩm Lâm và Mạch Nhiên!
Chuyện tình diễn ra giữa cô nàng ngây thơ nhưng cũng đầy lém lỉnh , giữa Thẩm Lâm có vẻ bề ngoài khá lạnh lùng nhưng lại mang trái tim yêu thương nồng nhiệt :)
Chuyện tình với những tình huống rất buồn cười và không kém phần khó đỡ đã làm mình cười muốn lộn ruột ^^
Yêu thích cách viết này của Ức Cẩm! Vừa được xả stress mà lại vừa đáp ứng nhu cầu đọc truyện tình cảm của mình!
4
303773
2015-01-21 22:57:10
--------------------------
149728
5499
Mình biết đến và thích Ức Cẩm từ truyện “Con thỏ bắt nạt cỏ gần hang” của cô, và độc dược phòng vé cũng không làm mình thất vọng. Truyện về chủ đề giới showbiz, mình rất thích thú, hơn nữa giọng văn của Ức Cẩm lại rất hài hước, vui vẻ, đọc thấy rất thoải mái. Mạch Nhiên vừa ngây thơ lại vừa lém lỉnh, đáng yêu; giám đốc Thẩm lạnh lùng nhưng vẫn khiến người ta phải yêu mến, lại được bà mẹ dễ thương hết biết. Truyện có lắm tình tiết hài không đỡ nổi, cũng có nút thắt, nút mở khá hợp lý, nhưng cái mình thích nhất lại là cách Ức Cẩm hướng nhân vật xử lý tình huống, cứ ngỡ họ sẽ làm thế này, đến khi có phản ứng thật sự thì lại hoàn toàn bất ngờ bởi nó không hề như mình đoán.
4
393748
2015-01-14 16:48:35
--------------------------
147495
5499
Lần đầu đọc truyện của Ức Cẩm. Nghe tên là lạ, đọc khá hài hước. Các tình huống truyện được xây dựng khá là hấp dẫn và tình cảm. Vẫn là tình yêu bá đạo của nam chính, nữ chính ngây thơ, lém lĩnh và không dễ bị ức hiếp! Tuy nhiên, truyện vẫn có nhiều tình tiết không rõ ràng, đọc xong cũng không biết vì sao Thẩm Lâm lại yêu Mạch Nhiên, còn cô nàng là yêu anh thật sự sâu đậm hay không, những chuyện trong quá khứ vẫn chưa được giải đáp hết.
Bìa truyện dễ thương. Ngôn ngữ đơn giản nhưng tinh tế!
3
468300
2015-01-07 23:19:04
--------------------------
147463
5499
Độc dược phòng vé là cuốn truyện thể hiện được cái nhìn đa chiều mà Ức Cẩm mong muốn mỗi người trong chúng ta hướng đến, để mỗi người có cái nhìn tích cực đối với cuộc sống hằng ngày. Văn phong của Ức Cẩm chủ yếu mang tư tưởng tích cực cho dù phải đương đấu với những khó khăn, điều này đã phần nào thể hiện một cách chân thực qua tính cách của cô gái nữ chinh. Truyện có cao trào, nhẹ nhàng, đem lại sự cuốn hút cho người đọc. Cuốn sách với chất liệu giấy tốt còn giúp chúng ta dễ dàng trong việc bảo quản.
4
500425
2015-01-07 22:10:24
--------------------------
147283
5499
Với giọng văn hài hước, truyện đã hấp dân tôi ngay từ những dòng đầu tiên. Tính cách của các nhân vật chính cũng rất dị chứ không phải kiểu nữ chính ngây thơ hoặc mạnh mẽ, nam chính lịch lãm hoặc ngang tàng như trong các tác phẩm khác. Tuy hài hước như vậy nhưng truyện không phải là không có chiều sâu, chỉ có một điểm trừ nhỏ là phần kết hơi nhạt và hơi vô lý quá, gây nên cảm giác hơi hụt hẫng một chút, nhưng dù sao cái kết vẫn là happy ending nên điều đó cũng không ảnh hưởng lắm đến cảm giác hài lòng của tôi đối với cuốn sách này.
5
17740
2015-01-07 14:14:40
--------------------------
143899
5499
Truyện này rất hay và hấp dẫn. Không như những truyện khác, truyện này không chỉ nói về tình yêu nam nữ lãng mạn mà còn về cuộc sống chân thực ngoài đời.Xây dựng nhận vật chính rất tốt nhưng đôi khi tác giả lại đi sâu vaò suy nghĩ của nhân vật nữ chính nhưng truyện này giúp giảm bớt căng thẳng và đọc cho vui. Và lúc đầu mình không định mua cuốn này nhưng nhìn thấy tiki bán rẻ hơn hiệu sách rất nhiều nên đã quyết định sẽ mua. Thứ ấn tượng đầu tiên là hình chibi bìa cuốn sách trông vui nhộn và giúp tăng cảm hứng đọc
4
520095
2014-12-24 20:27:38
--------------------------
143327
5499
Ức Cẩm là cái tên  khá xa lạ đối với tôi,đây là lần đầu tiên tôi đọc tiểu thuyết do tác giả này sáng tác.Điều đầu tiên tôi cảm thấy thích khi cầm trên tay quyển Độc Dược Phòng Vé đó là trang bìa của quyển,khi cầm khá thích tay,do chất giấy đẹp,ngoài ra,nội  dung và tính cách nhân vật được khắc hoạ  rõ ràng chỉ với một hình vẽ chibi vui nhộn  và đơn giản.Nhưng,quan trọng hơn là phần nội dung và cốt truyện,miêu tả rất thực tế về thế giới showbiz hiện nay,bên cạnh đó là chuyện tình của cô nàng có tính cách hài hước Bạch Mạch Nhiên và chàng giám đốc lạnh lùng Thẩm Lâm Kỳ.Diễn biến truyện rất thú vị,có lúc cao trào,cũng có lúc lại nhẹ nhàng ,êm đềm,khiến tôi không thể bỏ lỡ được những chương tiếp theo.Phải nói là tác giả rất cao tay khi đưa độc giả đi từ bất ngờ này đến bất ngờ khác,có những tình tiết căng thẳng cũng không quên gây cười cho người đọc.Tuy nhiên,ngoại hình của nhân vật được miêu tả không rõ ràng khiến độc giả khó mường tượng ra được ,điển hình là cô nàng nhân vật chính Bạch Mạch Nhiên.Ngoài ra , một chương truyện trở nên dài dòng,một số chi tiết hơi dư thừa là do  tác giả quá chăm chút về những câu độc thoại nội tâm và những màn đấu tranh tư tưởng của nữ chính lại quên đi truyện phải có nhiều tình tiết gây cấn và những sự kiện làm cho độc giả không quên được,sai sót nhỏ này của tác giả làm bộ truyện mất chiều sâu hơn.Nhưng dù sao,đây vẫn là một tác phẩm hay và gây ấn tượng mạnh với nhiều độc giả,trong đó có cả tôi !
5
497057
2014-12-22 16:43:58
--------------------------
143081
5499
Truyện được kể bằng ngôi thứ nhất, hài hước nhưng rất có chiều sâu và cũng thực tế. Vẫn là mô tuýp nam lạnh lùng, nữ ngốc nghếch, nhưng lại cho ta cái nhìn khác về mọi thứ. Thấy rõ sự bất công trên cuộc, truyện của Ức Cẩm rất hài hước và có một số chi tiết ấn tượng như những lúc chúng ta cứ nghĩ nữ chính sẽ nổi giận thì không truyện đi hoàn toàn theo một chiều hướng chúng ta không tưởng. Truyện vô cùng thực, nói rõ hiện trạng của ngày nay tạo cho chúng ta cái nhìn đa chiều không chỉ bị thu hẹp trong phạm vi nhỏ của truyện. Và trong giới giải trí cũng có thể xuất hiện tình yêu chân thành, truyện đã bộc lộ hoàn toàn điều đó. Độc dược phòng vé là cuốn truyện rất đáng đọc và tôi rất thích.
4
501442
2014-12-21 14:03:37
--------------------------
139770
5499
Với mọi tác phẩm của Ức Cẩm mà tôi từng đọc qua đều có những nét hay riêng của nó. Trong "Độc dược phòng vé" Ức Cẩm kể câu chuyện bằng ngôi thứ nhất. Thể hiện cái nhìn đơn diện trong mọi tình huống trong truyện. Vẫn là đề tài kiểu trai giàu lạnh lùng và nữ ngốc nghếch, Ức Cẩm lại lột xác theo giống như một ngòi bút khác vậy. Trong những truyện trước đây, mỗi ý niệm đều biểu lộ rõ trong từng câu chữ, nhưng trong tác phẩm này lại khác. Ức Cẩm mượn nhận thức của Bạch Mạnh Nhiên mà nói ra một số biểu hiện của giới showbiz ngày nay. Chuyện cặp kè với đại gia, tạo scandal để nhanh nổi tiếng,...Hay là việc báo chí lá cải nở rộ, còn có cả tình trạng fan cuồng,...
"Độc dược phòng vé" quả thật là một tác phẩm đáng để bạn bỏ thời gian đọc
5
469780
2014-12-08 20:30:12
--------------------------
134373
5499
Mới đọc tóm tắt nội dung thôi nhưng tôi đã cảm thấy khá thú vị rồi . Tôi rất tò mò không biết một cô gái được gới thiệu là Về nữ diễn viên Bạch Mạnh Nhiên, nếu nói cô ta có kỹ năng diễn xuất sẽ không ai tin, nói cô ta có tác phong thần tượng lại cảm thấy thiếu sót. Phim cô ta đóng vừa không có chiều sâu vừa không bán được vé. Nói cô ta là bình hoa thì đã coi trọng cô ta lắm rồi. Đúng là độc dược phòng vé!"sẽ có tính cách như thế nào nhỉ ? Nhất là trong giới nghệ thuật nếu không có tài năng thì sẽ bị chìm nghỉm , Truyện miêu tả chi tiết những hào quang và những góc tối đằng sau những người làm giải trí . Lúc nào họ cũng phải đeo trên mình nụ cười giả tạo trước bất kì chuyện gì . Nhưng kể cả tình huống như vậy cũng không ngăn được tình yêu chân thành xuất hiện . Kết thúc của truyện rất thỏa mãn ý tôi
5
337423
2014-11-09 11:37:16
--------------------------
134016
5499
Lúc đầu nhào vô đọc vì thấy cái văn án có vẻ hay, văn phong vui nhộn, thêm chút bựa. Nhưng vào đọc lại thấy hơi thất vọng.
Quả thật chẳng biết cái lý do gì mà Thẩm Lâm kỳ lại thích Bạch Mạnh Nhiên rồi bao che, giúp đỡ, yêu thương (ngầm). Còn nữ chính thì có vẽ giống bình bông di động nhưng cuối cùng cũng chứng tỏ được thực lực của mình, cũng không làm mình quá thất vọng.
Nhưng phải công nhận cách tác giả dùng câu chữ rất dễ thương, nhiều suy nghĩ của nữ chính quả thật ta không đỡ nổi, hành văn trôi chảy. Nhiều lúc làm mình cười lăn cười bò.
còn nam chính đủ soái, đủ kiêu, đủ nhà, xe, công ty... tặng kèm bà mẹ dễ thương, hiền lành.
Chị mà không lấy anh này thì quá có lỗi với nhân dân rồi.
3
469907
2014-11-07 22:10:32
--------------------------
