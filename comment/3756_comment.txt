494092
3756
Cuốn sách viết dễ đọc, nhưng mình không thấy bổ ích lắm. Có lẽ viết thuộc dạng năng khiếu của riêng từng người rồi.

4
759399
2016-12-04 17:42:27
--------------------------
488300
3756
Một cuốn sách cần thiết cho mọi người. Sách có nội dung ngắn gọn, đọc nhanh. Nhưng cũng cần dành nhiều suy nghĩ, đặc biệt là lúc ta áp dụng vào cuộc sống. Tôi rất ân ý với nội dung lẫn hình thức. Mong tiki tiếp tục mang đến những cuốn sách tương tự.
4
1327930
2016-11-01 18:07:34
--------------------------
479781
3756
Bấy lâu nay, mình luôn tìm kiếm 1 nguồn kiến thức kiến mình có thể sử dụng tư duy linh hoạt hơn, sắc sảo hơn. Mình thấy cực kì may mắn khi  được sở hữu những cuốn sách về tư duy trong bộ cẩm nang này. Dù có 1 chút xíu tiếc nuối do dịch giả miền Nam dịch, một số từ vựng sử dụng không phải từ phổ thông nên gây cảm giác khó hiểu. Đánh giá 5 sao cho bộ cẩm nang và hi vọng tác giả tiếp tục dịch những cuốn sách còn lại trong bộ cảm nang này!!!
5
1443714
2016-08-12 10:52:56
--------------------------
403272
3756
Cuốn sách  “ Cẩm Nang Tư Duy Viết “ đã giúp tôi hiểu rõ bản chất và khái niệm của việc viết có mục đích và phải có tư duy. Nội dung sách rất cô đọng, đi thẳng vào hướng dẫn để viết có thực chất. Phần lý thuyết và thực hành tương đối rõ ràng. Bìa thiết kế cũng tạm được, màu sách hơi buồn. Cách trình bày rất khoa học, chia làm hai phần chính: lý thuyết và thực hành. Hai màu mực xanh dương - đen giúp người đọc  dễ dàng theo dõi hơn.Sách này có thể áp dụng được cho mọi lứa tuổi, nhưng đặc biệt rất hữu ích cho các bạn sinh viên. Tuy nhiên sách hơi mỏng.
3
730635
2016-03-23 15:20:31
--------------------------
310231
3756
Viết là một cách rèn luyện trí não, tư duy logic, học cách sắp xếp và tổ chức trong từng câu chữ. Nhiều người cho rằng kỹ năng viết thiên về xã hội, nhưng không, viết vừa là hoạt động của bán cầu não trái và bán cầu não phải, chúng ta phải biết kết hợp và vận dụng từ tư duy cho đến cách viết như thế nào.

Sách được viết bởi chuyên gia nên tính lý thuyết và thực hành song hành nhau, nhưng nội dung các bài tập thực hành chưa đa dạng mà phần lớn thuộc về triết học - chính trị, và vốn dĩ để viết tốt và viết hay, viết sâu, chúng ta cần có kiến thức chuyên sâu trong lĩnh vực đó. Vì vậy, bài tập ví dụ đa dạng lĩnh vực hơn nữa thì mình sẽ có nhiều cái nhìn, góc nhận xét khác nhau hơn. 
Về hình thức:
-Mình không thích màu mực xanh lắm dù sách in rất tốt.
-Bìa hơi mỏng, sơ ý thôi là để quăng góc liền.
-Giá tiền hợp túi tiền cho một cuốn cẩm nang đáng có trên kệ sách thế này. 
3
2531
2015-09-19 13:30:04
--------------------------
281315
3756
Về hình thức: 
- Bìa sách được thiết kế khá dày và đẹp.
- Tuy là một cẩm nang ít trang nhưng nội dung bên trong được trình bày khá bắt mắt. Màu mực in rõ, chất lượng giấy tốt.
- Cách trình bày cũng rât khoa học khi chia làm hai phần chính: lý thuyết và thực hành. Hai màu mực xanh dương - đen giúp mình theo dõi dễ dàng hơn.
Nội dung:
- Sách tuy ít trang nhưng nội dung rât cô đọng, đi thẳng vào vấn đề "viết có thực chất" chứ không lan man dài dòng. 
- Cả phần lý thuyết và hướng dẫn thực hành tương đối rõ ràng nhưng những bài tập thực hành mình nhận thấy tương đối khó vì chủ yếu liên quan đến lĩnh vực triết học - chính trị. Các bạn cần có những hiểu biết cơ bản mới viết tốt được.
- Sách thích hợp cho các bạn sinh viên khối ngành xã hội và những ai yêu thích viết những bài viết có liên quan đến học thuật.
4
398179
2015-08-28 12:32:13
--------------------------
264552
3756
Bìa sách: trình bày bìa khá hấp dẫn, kích cỡ sách nhỏ hơn một chút sẽ thuận tiện hơn khi đọc với độ dày của sách. Chất lượng giấy khá tốt, có sử dụng 2 màu chữ và kích thước chữ nên khá dễ theo dõi. 
Về nội dung: Đề cập đầy đủ tới cách viết mà mỗi người nên biết để viết được tốt hơn, mình thấy cách tư duy khi viết hiện tại của mình chưa thật sự đúng đắn, chưa suy nghĩ kĩ càng khi viết ra những nội dung, câu cú để chống đỡ ý cho đoạn văn của mình được mạch lạc và có sức thuyết phục. Cuốn sách đem lại giá trị rất tuyệt vời cho mình. Tuy nhiên cách hành văn chưa thật sự thoát ý và lôi cuốn người đọc hơn. 
4
636713
2015-08-13 11:23:53
--------------------------
213271
3756
Sách có dung lượng mỏng nhưng nội dung đầy đủ, rõ ràng và súc tích, chỉ ra những quy tắc rõ ràng trong cách viết. Viết thế nào cho có mục đích và viết thế nào cho đúng. Mình nghĩ cuốn sách này đã được dịch lại một lần sang tiếng việt rồi nên sẽ làm mát đi cái hay ban đầu của bản gốc. Dù sao đây cũng là một cuốn sách đáng để mua, tuy mỏng hơn mình nghĩ. Giấy dày và xịn, đọc không bị lóa mắt, chữ rõ ràng, không nhòe mờ, lộn xộn. Bìa bóng, dày và đẹp. Mình khá là hài lòng.
5
114793
2015-06-23 16:33:30
--------------------------
