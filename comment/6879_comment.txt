128636
6879
Không ai trong chúng ta là không muốn thành công. Để tới được đích cuối cùng, ta phải vượt qua nhiều thác ghềnh, cám dỗ khiến ta đôi khi lạc lối. Hầu hết kinh nghiệm trong cuộc sống là từ những người đi trước. Nghệ thuạt học từ những người thành đạt đã được lưu lại trong rất nhiều cuốn sách và phương pháp này cho đến nay vẫn rất hữu ích. Nếu việc chinh phục thành công là mục tiêu của bạn thic bạn nên đọc cuốn sách này bởi những định hướng cơ bản đó xuất phát từ những người đã thành công trong cuộc sống. Với lối viết đơn giản, dễ hiểu sẽ giúp ta khám phá câu chuyện thành công của những doanh nhân thành đạt nổi tiếng thế giới, những vận động viên sáng giá, những con người dũng cảm đương đầu với nghịch cảnh để chiến thắng chính mình như Joe và Norma, những người coi việc giúp người khác thành công là thành công của chính bản thân; đồng thời cải thiện, trau dồi khả năng lập kế hoạch và xây dựng thành công của chính chúng ta. Mỗi câu chuyện trong sách chính là nấc thang đưa ta đến gần thành công của mình hơn.
3
381173
2014-10-03 20:51:52
--------------------------
