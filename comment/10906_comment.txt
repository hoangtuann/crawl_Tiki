417920
10906
Mua sách đọc vì tò mò là chính! Nhưng khi đọc qua lời giới thiệu của nhà thơ Lê Minh Quốc, chỉ muốn vào đọc ngay để xem giọng văn trào phúng của nhà văn như thế nào, có như những gì nhà thơ Lê Minh Quốc đã viết hay không. Ngay từ truyện ngắn đầu tiên đã cảm giác được sự trào phúng nhẹ nhàng, dí dỏm, sáng tạo, giàu cảm xúc và khá tinh tế. Cứ thế đọc xong trong nữa buổi là hết 22 truyện ngắn hài. "Trăng mật ở đảo" của nhà văn Bích Ngân là quyển sách nên đọc và suy ngẫm!
4
54458
2016-04-18 09:56:45
--------------------------
56037
10906
Mình thích những tác phẩm của nhà văn Bích Ngân. Mỗi tác phẩm đều có một dáng vẻ, âm hưởng khác nhau nhưng đều lôi cuốn và thu hút. Tác giả luôn biết làm mới mình bằng những câu chuyện đậm chất sáng tạo, độc đáo và khác biệt, nhưng vẫn giàu cảm xúc và tinh tế. "Trăng mật ở đảo" cũng vậy. Đây là một tập truyện thú vị, với những truyện ngắn diễn tả sinh động và chân thực về đời sống, tình yêu và con người. Mỗi truyện được viết ra một cách nhẹ nhàng, đơn giản, nhưng chạm đến cảm xúc của người đọc bằng ngôn từ mới lạ. Giống như lời giới thiệu, cuốn sách là một sự kết hợp bất ngờ đầy thú vị giữa yếu tố trữ tình, nhẹ nhàng với yếu tố hóm hình, hài hước. Đây là một cuốn sách không nên bỏ qua.
4
20073
2013-01-18 18:47:28
--------------------------
