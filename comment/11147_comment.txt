490964
11147
Đây là quyển sách tôi hài lòng nhất trong số các sách tôi mua trên TIKI.
Chất lượng giấy quá tốt,hình vẽ rõ ràng và đẹp.
Mô tả nội dung khá chi tiết, ai cũng có thể tập được.
Tôi rất hài lòng với sản phẩm này.
5
257481
2016-11-15 15:26:38
--------------------------
462076
11147
Tôi mua quyển sách này dành tặng bạn gái trong nhận dịp ngày quốc tế phụ nữ.Khi vừa nhận được món quà cô ấy rất vui và phấn khích.Vừa về nhà là cô ấy bắt đầu làm theo ngay.Tính đến nay cô ấy cũng tập được nữa năm.Thay đổi được một chút , mặt xinh hơn tý hì hì.MÌnh nghe mọi người nói tập yoga cần thời gian dài chứ không phải nay mai là thay đổi được liền. Nhưng theo mình thấy nửa năm mà thay đổi được thế cũng là tốt lắm rồi. Nói tóm lại mình khuyên các bạn trai nên mua quyển này dnàh tặng bạn gái hay là người thân cũng đều tốt cả.
5
983329
2016-06-28 11:15:07
--------------------------
454484
11147
Yoga Cho Khuôn Mặt Trẻ Đẹp cuốn sách hướng dẫn chi tiết liệu pháp yoga thư giãn làm đẹp cho gương mặt, hầu hết ai cũng biết yoga là bộ môn thể dục nhẹ nhàng được rất nhiều phụ nữ yêu thích và có tính ứng dụng cao, nhưng mình cũng bất ngờ là có cả yoga cho khuôn mặt, nếu chăm chỉ luyện tập, kết hợp ăn uống và sống một lối sống lành mạnh là chìa khóa vàng cho sắc đẹp tươi trẻ của mọi phụ nữ mà không có mỹ phẩm nào có thể so sánh được.
4
730322
2016-06-21 23:09:39
--------------------------
403016
11147
- "Nhất dáng nhì da", cơ thể cần tập luyện để có vóc dáng đẹp, cân đối thì khuôn mặt cũng vậy. Sách này dạy các bài tập yoga cho khuôn mặt (tất nhiên bao gồm tất tần tật những gì thuộc về khuôn mặt như mắt, mũi, miệng,...), dễ dàng thực hiện không cần máy móc phức tạp. 
- Mình tin rằng nếu duy trì thói quen luyện tập yoga như sách hướng dẫn với thói quen chăm sóc da bằng mỹ phẩm phù hợp, an toàn, nguồn gốc xuất xứ rõ ràng và với chế độ sinh hoạt (ăn, uống, ngủ nghỉ, giải trí...) thì da sẽ khỏe mạnh, sự lão hóa tới chậm hơn.
- Sách bìa cứng, cả quyển không nhiều trang nên cũng không dày. Dù là bìa cứng, nhưng khi vận chuyển tới tay mình thì ngay góc vuông của bìa sách đã bị móp, gãy, mềm nhũn. Điều này mình thường xuyên gặp phải khi mua sách ở Tiki do đối tác GHN vận chuyển. Cứ là đơn hàng sách mình nhận được thì phần lớn toàn bị móp hộp/rách hộp/te tua hộp, rách bìa ngay gáy sách, móp cạnh bìa ... dù mình ở HCM. Mong Tiki xem lại vấn đề này.
4
571973
2016-03-23 08:19:06
--------------------------
377968
11147
Về hình thức: Sách bìa cứng, giấy dày, trơn láng, in màu, nhìn vào là thấy hứng thú đọc ngay.
Về nội dung: Những động tác được minh họa cụ thể, nêu rõ tập cho bộ phận nào, mục đích nào. Tất cả mọi người đều dễ dàng thực hiện theo.
Cá nhân mình mới tập được tầm 1 tuần, nhưng thấy da dẻ hồng hào hẳn, nhìn nét mặt cũng tươi tắn hơn. Mình tin nếu duy trì những bài tập này trong thời gian dài, sẽ mang đến nhiều cải thiện rõ rệt hơn trên gương mặt. Đây thực sự là quyển sách cần phải có của chị em phụ nữ. Thay vì nghĩ đến việc duy trì sắc đẹp bằng dao kéo khi lớn tuổi, thì tập Face Yoga từ bây giờ sẽ có ích hơn nhiều.
3
710872
2016-02-03 22:24:04
--------------------------
307711
11147
Thật ra khuôn mặt mình có nhiều khuyết điểm, mặt mình nhìn rất béo, mắt thì nhiều bọng mỡ nên sau vô tình biết được cuốn sách này mình đã không ngần ngại mua về thử nghiệm. Sau 1 thời gian dài nghiên cứu và thử nghiệm cộng với việc chăm sóc da mặt cẩn thận mình nhận lấy kết quả vô cùng tốt! Cá nhân mình thấy sau 1 thời gian mặt mình bớt béo hẳn, bọng mắt cũng giảm dần, da dẻ có vẻ hồng hào hơn, các vết nhăn cũng giảm bớt vài phần. Mình thấy đây là một cuốn sách không thể thiếu đối với chị em phụ nữ chúng mình!
5
143101
2015-09-18 11:17:44
--------------------------
272377
11147
Trước đó đã  nghe nói về yoga cho mặt ( facial workout ) mà tìm mãi không thấy tài liệu nào hướng dẫn , may sao mình lại tìm thấy cuốn sách này nên không chần chừ rước em ý về luôn .Mặt cũng như các bộ phận khác trong cơ thể cần luôn được luyện tập để giữ được sự tươi trẻ . Mình dùng sách để tập mặt kết hợp với chăm sóc da bằng sản phẩm thiên nhiên . Cá nhân mình thấy khá hiệu quả , mặt hồng hào và đường nhăn nhỏ xíu ở mắt thấy mờ đi . Để chăm sóc sắc đẹp thì đây là một quyển sách không thể thiếu được .
5
124682
2015-08-19 23:38:54
--------------------------
263485
11147
Trước đây học Yoga cổ điển cô mình thường dạy các bài tập mắt để có đôi mắt sáng và khỏe mạnh, các bài tập hít thở nữa, sau khi tập các Asana sẽ tập co rút và căng giãn cơ mặt kiểu như tư thế con sư tử, và cho đến giờ ở nhà mình vẫn chỉ luyện tập như vậy. 
Đến khi đọc sách này mình mới thực sự biết Face Yoga cũng có khá nhiều phần để luyện tập, không phải chỉ có tự căng giãn cơ mặt như mình nghĩ mà có thêm phần tự massage các bộ phận trên khuôn mặt nữa. Mình nghĩ chỉ cần chú trọng đến sự thả lỏng và kiên trì massage nhẹ nhàng theo các động trong sách này chắc chắn khuôn mặt sẽ nhẹ nhàng và thanh thoát hơn! Khỏi phải mất công đi spa nhiều.^^
Tuy nhiên, để thực hiện mất kha khá thời gian và phải vệ sinh tay sạch sẽ chứ không massage xong mặt đầy mụn, và mình nghĩ nếu có tinh dầu để massage nữa thì càng tốt hơn hoặc nếu ở vùng mắt thì dùng các tinh chất hay kem dưỡng mắt của bạn để massage hiệu quả lắm luôn.hihi
5
699481
2015-08-12 16:14:31
--------------------------
216978
11147
Theo mình nghĩ : "Muốn tự tin thì trước hết phải đẹp!". Bản thân mình có một khuôn mặt không hoàn hảo, nhiều mỡ mặt và nếp nhăn do không biết cách chăm sóc nên mình cảm giác khác tự ti. Vì thế, mình mua cuốn sách này với hy vọng cải thiện khuôn mặt mà không cần dùng những hình thức phẫu thuật thẫm mỹ. 
       Nội dung sách hay: Các bài tập rõ ràng, chi tiết theo từng phần để bạn dễ dàng lựa chọn cho các vùng cần cải thiện trện gương mặt từ mắt, mũi, miệng cho tới trán, cằm, cổ. 
       Mình đã thực hiện các động tác giảm mỡ mặt trong vòng 2 tuần, tuy chưa có kết quả rõ ràng về giảm mỡ nhưng mình cảm thấy gương mặt tươi tắn hẳn ra. 
       Mình tin rằng nếu kiên trì thực hiện trong thời gian dài thì mặt mình sẽ thon gọn và tươi trẻ như mình mong muốn mà không cần đọng tới dao kéo. Đẹp tự nhiên vẫn là tốt hơn!
5
642428
2015-06-28 15:53:48
--------------------------
212060
11147
Ưu điểm: bìa sách cứng cáp, giấy in đẹp, sắp xếp các nội dung thành tunwgf chương hợp lý, trình bày minh họa hình ảnh rất dễ hiểu, các bài tập đa dạng cho nhiều mục đích khác nhau như giải tỏa mệt mỏi, chăm sóc da mặt, tóc...
Nhược điểm: (cũng không hoàn toàn là nhược điểm) khi áp dụng các bài tập yoga đòi hỏi thời gian và sự kiên nhẫn rất lớn từ người tập.
Cuốn sách là món quà ý nghĩa và thực tế dành cho mẹ và những bạn gái quan tâm chăm lo tới sắc đẹp và sức khỏe.
4
114324
2015-06-21 17:29:46
--------------------------
187285
11147
Về nội dung, quyển sách này hướng dẫn rất rõ ràng, chi tiết các phương pháp yoga sử dụng cho khuôn mặt. Các phương pháp được đề cập trong quyển sách này không chỉ là cho da mặt không, mà còn cho toàn bộ các chi tiết khác của gương mặt, như là: mắt, mũi, miệng, trán và cả lông mày. Một điều rất tuyệt vời nữa của quyển sách này là nó hướng dẫn cho người đọc cách để có được gương mặt cười nếu kiên trì tập luyện. Về hình thức thì chất lượng giấy và bìa đều rất tốt. Được in màu nên dễ dàng thực hành theo.
4
329218
2015-04-22 20:23:05
--------------------------
179127
11147
Mình có vài nhận xét về sách này như sau:
Về hình thức: Sách có bìa cứng khá chắc chắn, khi mình nhận được sách thì bên ngoài sách vẫn còn lớp giấy bóng bao chống bụi nên mình rất hài lòng. Ngoài ra sách in màu và có hình ảnh minh họa nên rất dễ hiểu và dễ thực hành theo.
Về nội dung: có rất nhiều bài tập tương ứng cho từng vùng trên khuôn mặt, nội dung khá đa dạng, mình rất thích nội dung của sách này. Tuy nhiên để luyện tập các bài yoga này thì chắc chắn bạn phải kiên nhẫn.
4
538881
2015-04-06 15:30:22
--------------------------
