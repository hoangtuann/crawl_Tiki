468235
7230
Trước tiên là về chất lượng sách: giấy trắng, nét chữ in rõ nét dễ  đọc và không bị nhòe. Nội dung các phần nghe khá phong phú, đây là điều rất cần thiết cho 1  quyển sách luyện thi N2 bởi vì độ khó của đề thi N2 nằm ở chỗ chủ đề quá rộng, bạn phải trang bị cho mình một lượng kiến thức xã hội nhất định mới có thể làm bài tốt được. Quyển sách này sẽ giúp bạn phần nào ở chỗ đó. 

Về phần giao hàng thì Tiki làm rất tốt. Sách được đóng gói cẩn thận, nhân việc giao hàng lại tốt bụng và nhiệt tình. 3 lần mình đặt mua sách đều thế cả nên mình rất hài lòng.
5
205252
2016-07-04 21:21:36
--------------------------
463937
7230
Cuốn sách: "Luyện thi năng lực Nhật ngữ N1" này phù hợp cho những ai đã có một nền kiến thức vững chắc về Nhật ngữ mới có thể hiểu được. Sách được biên soạn 100% tiếng Nhật. Sách còn kèm theo đĩa CD giúp cho mình rèn luyện kĩ năng nghe. Chất liêu giấy in tốt vì vậy mà những câu chữ hiển thị rõ ràng. Cấu trúc trình bày mạch lạc. Để hiểu sâu được nội dung kiến thức cần phải kiên trì rất nhiều vì trình độ N1 rất khó. 
Cảm ơn Tiki đã tạo điều kiện cho mình được tiếp cận đến cuốn Luyện thi năng lực Nhật ngữ N1. Cuốn sách giúp mình rất nhiều trong việc học.
4
791063
2016-06-29 19:58:20
--------------------------
415696
7230
Giá quá tốt cho một cuốn shinkanzen master kèm CD. Chất lượng giấy rất tốt. Nếu như mua sách này ở Nhật thì tốn tầm 250 ngàn cơ. Hi vọng sẽ có các phần còn lại. Chúc các bạn thi JLPT tháng 7 này thành công.

Trong những bộ sách hay được dùng để ôn thi JLPT thì soumatome là dễ nhất, khó hơn là speed master và khó nhất là shinkanzen  master. Việc có tiếng Việt giúp cho việc ôn luyện n1 trở nên dễ dàng hơn. Các bạn cũng có thể tham khảo cuốn 500 câu hỏi ôn n1 nữa nhé.

5
1007144
2016-04-13 21:31:50
--------------------------
296803
7230
Tài Liệu Luyện Thi Năng Lực Nhật Ngữ N1 choukai là một tài liệu luyện nghe rất hay, sát với đề thi Năng Lực Nhật Ngữ trong thực thế. Sách được thiết kế theo phần nghe của kỳ thi JLPT nên rất phù hợp cho các bạn đang có nhu cầu luyện thi. Sách có mục lục rõ ràng dễ tra cứu, có phần chú thích bằng tiếng Việt nên thuận tiện theo dõi. Mỗi phần nghe đều có phần kaitou và trancript để người học có thể xác định lại mức độ nghe hiểu của mình. Nếu bạn đang muốn luyện thi JLPT thì đây là một quyển sách không thể thiếu trong kệ sách của bạn.
5
415494
2015-09-11 12:15:23
--------------------------
224427
7230
Đây là cuốn sách gồm tài liệu luyện thi tiếng Nhật, trong sách rất ít các ghi chú bằng tiếng Việt, để đọc hiểu thành thục yêu cầu, có lẽ người học cần phải có thêm cuốn từ điển vì trình độ trong cuốn sách luyện thi này quá cao.
Sách khổ lớn, to bản, tương đối mỏng, giống như các bài trắc nghiệm được trình bày dễ hiểu thông thoáng cho người luyện thi Nhật ngữ Shin Kanzen Maste.
Tôi hi vọng bộ sách luyện thi năng lực Nhật ngữ Shin Kanzen Master sẽ sớm phát hành đầy đủ ở Việt Nam để thuận lợi cho việc ôn luyện tiếng Nhật những người trẻ như chúng tôi.
4
75153
2015-07-08 17:24:44
--------------------------
218453
7230
Đây là bộ sách luyện thi năng lực Nhật ngữ Shin Kanzen Master rất nổi tiếng. Có rất nhiều bộ sách luyện thi JLPT nhưng theo đánh giá của nhiều người thì đây là một trong những bộ tốt nhất. Tập sách luyện nghe N1 là trình độ cao nhất trong các trình JLPT nên rất khó, tốc độ đọc nhanh. Bạn mình người nước ngoài nhờ mình mua giùm vì giá cả rất tốt so với giá ở nước ngoài. Sách hầu như có rất ít ghi chú Tiếng Việt mà chủ yếu toàn là Tiếng Nhật. Tiếc là bộ này ở Việt Nam mới chỉ có cuốn luỵện nghe hiểu, hy vọng sẽ sớm ra thêm các cuốn luyện đọc hiểu và ngữ pháp...
5
470937
2015-06-30 16:31:32
--------------------------
