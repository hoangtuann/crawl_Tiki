451225
5937
Thần Thuyết Của Người Chim là một cuốn tiểu thuyết sử thi rất đáng đọc. Truyện lấy bối cảnh cuối thời Hùng Vương, cuốn hút ngay từ những dòng đầu tiên với những miêu tả sâu sát về đời sống, văn hóa, phong tục, tập quán,... của người Việt cổ - người Lạc. Văn phong của tác giả Văn Lê rất đẹp, rất nhịp nhàng và sinh động nên cuốn sách càng có giá trị hơn. Đây là một tác phẩm thực sự ý nghĩa, giúp cho tôi hiểu biết thêm về nguồn cội, về lịch sử con người và dân tộc mình. Tôi đọc mà vừa tự hào, vừa cảm thương, lại cũng vừa nuối tiếc cho người Lạc - Việt mình. Qua đó cũng thấu hiểu tâm tư và khát vọng của tác giả với quê hương, đất nước. Mặc dù có đôi đoạn hơi đều đều nhưng sách vẫn rất hay, như đoạn chiến trận binh đạo với quân Triệu Đà rất có cao trào. Và phần kết khi nước mất, nhà tan tôi cũng không kìm được nỗi xúc động dâng trào...
4
386342
2016-06-19 15:25:59
--------------------------
389222
5937
Truyện được kể rất mạch lạc và phảng phất chất phiêu lưu rất thú vị. Chỉ tiếc là tính văn học của đề tài này nói chung và tác phẩm này nói riêng chưa đủ để thu hút người đọc phổ thông (trừ mấy đứa hoài cổ như mình) như các dòng tiểu thuyết giả sử hay phiêu lưu. Có thể không được biết đến nhiều trong thời điểm hiện tại, nhưng với quá trình phát triển của nền văn học trong nước, tác phẩm này sẽ trở nên rất cần thiết. Xin dành sự kính trọng cho bác Văn Lê vì tình yêu tổ quốc và lịch sử đầy sức sống của bác. ^^
4
598425
2016-03-01 17:31:46
--------------------------
205859
5937
Chỉ có thể nói: hay! 
Nội dung: cốt truyện hay, truyền được tinh thần, văn hóa việt. Nét đẹp nhân văn của người Chim, tổ tiên của người Việt Nam. Lời văn có nhiều từ hơi khó hiểu, có thể là do dùng từ cổ, nhưng đọc và suy nghĩ thì lại như thấy được cái thần của người xưa. Trong các nhân vật mình thích nhân vật T'rưng Chiêm, Mi Thoại, những người phụ nữ Chim mạnh mẽ, tự tin nhưng không hề kém về duyên dáng, dịu dàng. Nhân vật nam chính thì không thích lắm vì thấy hơi hoa đào dù Lang Kôông tài trí hơn người, có nhiều phát kiến; thấy tiếc cho những người tài, có tâm với nước nhưng chưa thực hiện được như Đô Lỗ, Dĩnh Toán, Lang Kôông. Hơi nhiều cảnh "h" nhưng lời văn không "trần trụi, thô lỗ" lắm.
Hình thức: bìa đẹp, màu sắc ấn tượng.
Hay, đáng để đọc và suy nghĩ.
5
137304
2015-06-07 21:27:20
--------------------------
