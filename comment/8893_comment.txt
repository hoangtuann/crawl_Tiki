474209
8893
Lịch sử Việt Nam bằng tranh là một bộ sách khá tốt, đáp ứng được nhu cầu học lịch sử nước nhà. Có nhiều bộ sách lịch sử, nhưng đây là bộ sách minh thấy tương đối hay nên đã mua thử vài cuốn cho con đọc. Bé nhà mình đã rất thích, nên mua thêm cho đủ bộ (y). Bộ sách chọn cách tiếp cận lịch sử bằng hình thức truyện tranh là một cách hay, giúp trẻ có hứng thú đọc. Bản thân mình đọc cũng thấy hứng thú. 
Tuy nhiên, không biết như thế nào, nhưng mình thấy có vài trang hình ảnh in hơi mờ.
5
1497182
2016-07-11 08:32:18
--------------------------
171308
8893
Bộ sách này có từng tập truyện rất rõ ràng giúp mình có thể nhớ lại từng giai đoạn trong lịch sử mà không bị nhầm lẫn. Mấy quyển này cũng khá mỏng, đọc không bị chán mà cũng rất đầy đủ thông tin. Rất tiện khi có thể đọc từng quyển một cách nhanh chóng mà vẫn nắm bắt, hiểu được những diễn biến, sự việc trong từng trận chiến, giai đoạn lịch sử. Cùng với nhiều hình ảnh và những lời văn khá liên kết thì bộ sách này rất phù hợp cho các bạn học sinh và cả người lớn nữa.
4
540461
2015-03-21 17:46:28
--------------------------
80344
8893
Tôi đã mua 50 cuốn Lịch sử Việt Nam bằng tranh. Bộ sách này dễ đọc với trẻ em và cũng hấp dẫn cả người lớn. Tranh minh hoạ thể hiện sinh động cuộc sống của người Việt từng giai đoạn lịch sử, đúng của người Việt. Ngoài việc chống ngoại xâm, bộ sách còn đề cập nhiều hơn đến những vấn đề kinh tế, xã hội....Tuy nhiên, chất lượng bản in còn không đồng đều, nhiều trang in còn mờ, chữ đứt nét. Với giá bán như vậy mà được in màu thì bộ sách càng có giá trị và hấp dẫn hơn nữa. Tôi đang chờ những cuốn tiếp theo của Nhà xuất bản TRẺ.
4
38335
2013-06-12 14:06:13
--------------------------
