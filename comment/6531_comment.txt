223259
6531
Cách trình bày bắt mắt, cấu trúc các Task hợp lí. Bạn sẽ được làm quen với từng loại đề, từ dễ đến khó. Dựa theo hướng dẫn, rèn luyện hằng ngày, bạn sẽ thấy mình tiến bộ rõ. 
Với các bạn đã từng học qua các lớp luyện IELTS, quyến sách này ko hữu ích lắm đâu. Mình nghĩ nó phù hợp với những bạn mới bắt đầu làm quen hoặc tự học ở nhà hơn. 
5
39317
2015-07-06 20:46:25
--------------------------
159115
6531
Sách cho ta những phương pháp làm task 1 và task 2 có hệ thống và khá dễ theo dõi. Các cấu trúc sử dụng không hề nâng cao, thích hợp với những người mới bắt đầu, nhưng đối với những người đã từng học IELTS thì quyển sách này không cần thiết.
Mặc dù vậy, đối với bạn nào mới bắt đầu tiếp cận với IELTS thì quyển sách này vô cùng phù hợp. Nó sẽ cho bạn những chỉ dẫn ở từng biểu đồ, bản thân có thể tự luyện tập và cuối cùng ta có thể tham khảo qua bài mẫu sau cùng. Các bài mẫu đều có chọn lọc kỹ càng tuy có hơi dài so với quy định.
2
406884
2015-02-14 22:23:45
--------------------------
