449001
5012
Cá nhân tôi chưa biết đến tác giả Tình Không Lam Hề trước khi đọc cuốn sách này. Bìa khá đẹp, lại có tên tác giả cũng hơi lạ nên tôi quyết định mua. Cá nhân tôi thấy cách hành văn khá ổn, tuy nhiên cốt truyện lại làm cho tôi có chút cảm giác hơi lỏng lẻo, các tình tiết cũng không được mạch lạc lắm. Có vẻ như cứ dính tới mafia là đa số các tác giả chẳng thể thoát khỏi cái bóng để làm thoát lên cái mình muốn thể hiện vậy. Một sao cho bìa, một sao cho văn phong, một sao cho tên tác giả.
3
435897
2016-06-16 20:41:40
--------------------------
391343
5012
Truyện có nội dung rất hay . Lúc đầu mình đọc mới vào thì là lúc án thừa ảnh bị tai nạn và thẩm trì nói chuyện với cô ấy . Mới đọc vô mình không hiểu gì từ từ về sau án thừa ảnh nới chuyện với lâm liên thành về chuyện đó thì mình mới hiểu . Hồi đầu mình còn tưởng là tiền tiểu phi là nhân vật chính ai ngờ thừa ảnh mới đúng là nhân vật chính . Thẩm trì thì là giống như một xã hội đen vậy nhưng luôn bảo vệ hết mình cho thừa ảnh . Tóm gọn lại truyện rất hay buồn cũng có chiến tranh lạnh cũng có mà vui ân ái nhau cũng có . Có lúc sau thừa ảnh bị mất trí nhớ thì thẩm trì đã bồi đắp cho cô ấy như bắt đầu lại cuộc sống mới và nói rõ về nghề nghiệp của anh ấy mà cô luôn thắc mắc bấy lâu . Truyện này thuộc dạng OE . cuối cùng thì kết thúc là thừa ảnh vẫn đang tìm lại trí nhớ và bắt đầu cuộc sống mới với thẩm trì thôi . Truyện hay lắm nhưng cái mình thất vọng là bìa quá mỏng và bookmark cũng quá mỏng . Còn sai một số lỗi chính tả và in sách thì hình như ít keo quá hay sao ý mà mình lỡ kéo qua một cái thì nó bị sứt cả trang nhưng cũng có thể là lỗi do mình mà sách này hình như từ hồi 2015 ý vì mình mua nó còn bìa chúc mừng năm mới 2015 nói chung truyện hay nhưng mình k thích bìa và bookmark !
4
1095528
2016-03-05 10:12:58
--------------------------
340518
5012
Truyện này nếu nói xuất sắc thì không tới nhưng về cơ bản thì có thể đọc giải trí tạm, nội dung truyện thường thường, mảng dịch thuật rất châu chuốt, bìa sách cũng được đầu tư cẩn thận. Văn phong của Tinh Không Lam Hề được thể hiện rất rõ trong truyện này, ai đã đọc truyện của chị sẽ thấy, nhẹ nhàng, bình lặng và khá sâu lắng, ít sóng gió, tính cách nam nữ chính Thừa Ảnh và Thẩm Trì khá thú vị,có mấy chỗ  ngược tâm chẳng qua cũng vì hiểu lầm các kiểu thôi, kết truyện thì mình thấy không hay lắm đâu
4
630600
2015-11-20 02:01:35
--------------------------
318919
5012
Lần đầu tiên đọc tác phẩm của tác giả Tình Không Lâm Hề,cảm thấy tác giả không quen viết về thể loại hắc bang này nên có phần vẫn con non tay,tập trung vào tình cảm là chính.Mới đầu truyện mình có hơi lơ mơ không rõ ràng lắm,nhất là quá khứ của Thẩm Trì và Thừa Ảnh,cả tình cảm của họ đối với nhau cũng mờ nhạt như người dưng,nhưng càng về sau khi gặp nguy hiểm khó khăn,hai vợ chồng cũng chịu đối mặt với nhau,dù cảm xúc vẫn chưa tới,thật sự mình muốn tác giả kể thêm về quá khứ cả hai,vì chuyện khá ngắn nên nam phụ không nhiều đất diễn lắm,còn mờ nhạt,đoạn cuối cùng lơ lửng,có cảm giác hơi đuối ở đoạn kết.
3
560863
2015-10-07 11:58:04
--------------------------
300202
5012
Bìa sách đẹp bắt mắt vì ấn tượng đầu đó cho nên mình mới mua quyển truyện này nhưng khi mở ra thì thấy bìa truyện quá mỏng so với số trang sách, bookmark cũng mỏng và không được đẹp cho lắm. Tuy vậy cốt truyện khá hay nói về tình cảm của thẩm trì và thừa ảnh họ gặp nhau khi anh là ông trùm có thế lực còn cô là 1 cô học sinh, từ đó tình cảm của họ phát sinh dù phải trải qua nhiều sóng gió họ vẫn luôn ở bên nhau đi hết cuối con đường. Nếu kết truyện là OE thì sẽ hợp lí hơn
3
625161
2015-09-13 20:16:50
--------------------------
221366
5012
Đây là lần đâu tiên mình đọc truyện của Tình Không Lam Hề.
Ấn tượng đầu tiên của mình là về hình thức cuốn sách: Bìa sách được in khá đẹp, trông nhẹ nhàng nhưng lại bắt mắt. Là một sự kết hợp ton sur ton. 
Về nội dung:Khi đọc cuốn truyện này, mình khá ấn tượng với văn phong của Lam Hề, thích cái nội dung được truyền tải sau hình thức đẹp ấy. Đọc Tháng ngày ta đã qua để nhận ra, tình yêu không nhất thiết phải là cả hai người có cùng sở thích, cùng quan điểm, cùng suy nghĩ. Biết đâu như vậy sẽ rất nhàm chán. Lam hề cho tôi thấy “Em yêu anh vì những điều trái ngược”. Chính cái trái ngược ấy lại làm nên sự thu hút. Đó chính là tình yêu giữa Thừa Ảnh và Thẩm Trì.
4
290680
2015-07-03 19:55:18
--------------------------
220639
5012
Câu chuyện được viết theo kiểu viết thịnh hành về tình yêu của đại ca xã hội đen với một cô gái trong sáng thánh thiện nhưng được vị đại ca này hết lòng yêu thương.
Trong truyện rất ít những trường đoạn gây giật mình, nếu có thì cũng tương đối nhẹ nhàng, không mãnh liệt như trong Đạo tình cũng chẳng thấm đẫm tình cảm như Từ bi thành. Câu chuyện chủ yếu nói về tình yêu, về sự tin tưởng giữa hai con người yêu nhau.
Thẩm Trì mạnh mẽ bá đạo, Thừa Ảnh nhẹ nhàng nhưng cứng rắn...
Những hiểu lầm giữa hai người đươc tác giả khai thác khá nhiều nhưng đều không được đẩy lên đỉnh điểm, mà cả hai luôn giải quyết bằng những chi tiết ân ái. Tác giả miêu tả chú yếu sự lạnh lùng của nhân vật nữ và nhan sắc của nhân vật nam làm mình cảm thấy hơi ngồ ngộ.
Nhờ dịch giả cẩn thận mà mình cảm nhận được văn phong của tác giả, nhiều đoạn dí dỏm, đặc sắc làm xua tan bớt không khí nặng nề ở một số đoạn.
Nói chung tác phẩm đơn giản nói về tình yêu nhẹ nhàng, đơn giản không cao trào lắm.
Đoc giải trí thì khá tốt, nhiều đoạn văn hay.
Hi vọng lần sau tác giả khai thác câu chuyện sâu sắc hơn.
4
101005
2015-07-02 20:34:32
--------------------------
171228
5012
Mình biết đến Tình Không Lam Hề qua cuốn truyện Mưa gió thoáng qua ,tôi yêu em ,thật sự thì quyển truyện ấy đã để lại trong mình rất nhiều ý tưởng nhưng sau khí đọc xong cuốn Tháng ngày ta đã qua thì mình khá thất vọng .Nội  dung không được thu hút mình lắm ,các mối quan hệ trong truyện vô cùng phức tạp =='' Và vì cũng chưa đọc hết bộ truyện ,nên cũng không biết kết thúc của các nhân vật ra sao TT^TT 
Chất lượng : Bìa sách khá đẹp ,nhẹ nhàng kết hợp tông vàng cùng với những cánh hoa rẻ quạt trông rất hay nhưng bookmark lại khá mỏng ( phải nói là cực kì mỏng ) ,cầm không có cảm giác chắn chắc ,tựa như chỉ cần 1 cơn gió là bay đi luôn cái bookmark vậy =))) Nunabook cần khắc phục điểm trừ này nhé !
3
107142
2015-03-21 14:40:57
--------------------------
171004
5012
Sự thật là tôi đã đọc truyện này lâu rồi, được dịch bên kites với tên "Từ bắt đầu đến hiện tại", thích truyện ngay khi vừa đọc văn án. Truyện rất hay, văn phong của Tình Không Lam Hề chưa từng làm tôi thất vọng. Thẩm Trì và Thừa Ảnh hai con người khác biệt tưởng như đối lập đó qua bao gian nan, cách trở cuối cùng cũng về được bên nhau. Tuy là kết thúc chưa thực sự thỏa mãn được cảm xúc của tôi nhưng như vậy có lẽ sẽ hợp lý hơn. Tất cả mọi chuyện nếu kết thúc quá viên mãn thì sẽ cảm thấy không thực. Tóm lại đây là cuốn sách đáng đọc.
4
363721
2015-03-21 01:01:29
--------------------------
160526
5012
Khi tôi đọc cuốn truyện này, tôi đã yêu ngay lối văn phong của Lam Hề, những cuốn chuyện chị viết đều là những cuốn truyện hay và sâu sắc, và Tháng ngày ta đã qua cũng là một trong những cuốn truyện để lại nhiều dư âm...Tình yêu không nhất thiết phải là trắng và trắng, đen và đen mà có thể đôi khi sự dung hòa giữa trắng và đen cũng tạo nên một tình yêu vĩnh cửu. Thừa Ảnh và Thẩm Trì chính là sự giao hòa đẹp đẽ đó... Cho dù giữa họ có sự khác biệt, người bóng tối, kẻ ánh sáng nhưng không có nghĩa là không đến được với nhau, tình yêu của họ vốn là tạo hóa mà ông trời ban tặng giữa đường giao nhau của bức tranh đen trắng, mặc cho có những lúc sóng gió, có những lúc cãi nhau to nhỏ, nhưng như vậy lại càng khiến tình yêu thêm khẳng định hơn. Một người đàn ông cho dù nhúng chàm đến mức độ nào thì cũng vẫn khao khát có một gia đình đầm ấm, hạnh phúc... Mà Thừa Ảnh lại chính là người mang lại cho anh cảm giác đó.... loại cảm giác yên bình, êm đềm cho dù trải qua bao sóng gió.
5
111548
2015-02-24 17:19:43
--------------------------
