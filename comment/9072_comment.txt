456604
9072
Khi còn nhỏ tôi thường được bố mẹ mua cho rất nhiều truyện cổ tích để đọc, so với các bản được xuất bản ngày nay thì các bản trước đây có nội dung có chiều sâu, ngôn từ phong phú, trong sáng hơn nhiều, chính vì vậy khi mua tặng các bé sách truyện tôi thường rất chú trọng nội dung, ngôn từ của truyện. Khi mua Tô Màu Truyện Cổ Tích Việt Nam - Sự Tích Bánh Chưng Bánh Dày tôi đã đọc thử trước, quả thấy truyện đã được chọn lọc kỹ lưỡng nên mới quyết định mua. Hơn nữa, các bé còn được tô màu, biết thêm trang phục người xưa. Hi vọng nhà xuất bản tiếp tục xuất bản thêm nhiều câu chuyện cổ tích hay ý nghĩa như này đến các em nhỏ.
4
667726
2016-06-23 15:58:33
--------------------------
377128
9072
Tô màu lịch sử cũ đã giúp hình thành nên màu sắc về trang sử dựng nước yên bình khi có người gặp được cơ may đã đứng ra chỉ gọi cho dân chúng biết hình dạng của chiếc bánh mang tên gọi của vũ trụ , vạn vật gắn bó với người dân trong đất nước đã cùng làm để tưởng nhớ tới tên của các người có công ơn như thế chẳng hạn như nói thật khi đứng ra bỏ tội cuộc cho chất liệu quý gía bản chất lại thân thiết với ngày nay , nhớ tới công trình bỏ ra hàng năm .
4
402468
2016-02-01 18:08:22
--------------------------
