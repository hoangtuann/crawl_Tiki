257281
9057
Sách chia nội dung để thực hiện trong vòng 30 ngày, rất sáng tạo và hữu ích. Bản thân tôi đã tìm hiểu về lĩnh vực marketing khá lâu và thật thiếu sót khi tôi mới được biết về cuốn sách này thông qua người bạn của mình. Sách đọc và áp dụng ngay được, sách được dịch với ngôn từ dễ hiểu và chi tiết. Đây là quyển sách rất rất hay dành cho các bạn đang tìm hiểu hay làm về marketing như tôi, thậm chí bạn nào chưa hiểu gì cũng có thể đọc để tham khảo.
4
565594
2015-08-07 15:40:49
--------------------------
251078
9057
Với nội dung, câu từ và cách viết dễ hiểu, cuốn sách này là một bản chỉ dẫn marketing dành cho mọi người - từ những người chưa có khái niệm gì về tiếp thị (đọc sách để có cái nhìn chung về công việc marketing) cho đến những chuyên gia về marketing (đọc để phản ánh lại những kinh nghiệm hiện có). Cuốn sách được viết theo 30 ngày, mỗi ngày là 1 bước cần thiết để đạt được  chiến dịch marketing hiệu quả, Hi vọng sắp tới Tiki sẽ có thêm nhiều sách về marketing du kích vì đây hiện đang là xu hướng marketing của thế giới
4
363405
2015-08-02 13:19:48
--------------------------
203594
9057
Tôi mới làm quen với Content Marketing và cảm thấy loại hình Marketing này không hề đơn giản như nhiều người vẫn nghĩ. Trong quá trình tìm kiếm tài liệu tôi đã nhận thấy quyển sách Marketing du kích trong 30 ngày là một nguồn kiến thức rất thích hợp để tham khảo trong quá trình nghiên cứu và thực hành về Content Marketing. Jay Conrad Levinson đã phân tích những yếu tố quan trọng trong Marketing rất ngắn gọn và chúng ta có thể vận dụng trong vòng 30 ngày. Nhưng theo tôi nghĩ quyển sách phù hợp hơn với người làm Content đơn lẻ hơn là một doanh nghiệp lớn. Có thể coi quyển sách này là một khóa học ngắn hạn và hiệu quả lớn.
5
313837
2015-06-01 19:09:21
--------------------------
161637
9057
Quyển sách thực sự cần thiết và không thể thiếu cho những doanh nghiệp vừa và nhỏ, những doanh nghiệp có chi phí tiếp thị thấp nhưng muốn đat hiệu quả cao. Nội dung quyển trình bày ngắn gọn, các đề mục được sắp xếp theo trình tự hợp lý, đồng thời có ví dụ minh họa giúp cho ngược đọc dễ tiếp thu và có thể ứng dụng trong thực tế. Đặc biệt, là cuối một phần đều có phần cô đọng lý thuyết và cách thức thực hiện. Tất cả mọi đối tượng muốn làm tiếp thị đều có thể đọc và ứng dụng ngay.
4
356759
2015-02-28 16:43:26
--------------------------
