571649
4118
Nó nhó hơn mình nghĩ, nhưng đối vs học sinh thì nó tương đối đầy đủ. Tuy nhiên khi giao hàng thì sách hơ có vấn đề về bìa. Nhưng nhìn chung mọi thứ đều ổn. Great jobs
5
1743709
2017-04-12 21:18:53
--------------------------
541829
4118
Sách nhỏ gọn như quyển sổ tay rất đẹp mắt...
Giải thích cách dùng rõ ràng và có cả phần ví dụ để giúp hiểu rõ hơn...khá tốt
4
1310322
2017-03-14 19:48:50
--------------------------
491534
4118
Mình học ôn thi ielts và được cô giới thiệu mua cho quyển này. Phải nói là trên cả ưng ý. Quyển sách có đầy đủ phrasal verb thông dụng. Bên cạnh đó còn có giải thích và đặt câu tiếng anh ( có dịch tiếng việt) giúp mình nhớ lâu hơn. Đây là quyển sách rất đáng mua
4
514267
2016-11-18 21:25:32
--------------------------
473613
4118
Ấn tượng đầu tiên của mình về sách là nó nhỏ, vuông cạnh rất đẹp, chất lượng giấy tốt, bền. Về nội dung thì trình bày khoa học, dễ hiểu, cung cấp thêm cho mình kiến thức bổ ích, lấy ví dụ như cụm từ "get up" ngày xưa mình chỉ biết đến nghĩa là thức giấc, nhưng nhờ cuốn sách mình biết thêm được 5 nghĩa nữa như đứng dậy, tăng tốc tự cảm thấy, ... Nói chung là với cá nhân mình  cuốn sách cực kì có ích ;uôn, các cụm từ này thường không có trong từ điển.
5
394626
2016-07-10 19:19:36
--------------------------
471247
4118
Mình đã dùng cuốn này hơn 2 tháng và mình rất thích về sản phẩm . Sách được thiết kế nhỏ nhưng giải thích đầy đủ , dễ tra , cứ như cuốn từ điển mini vậy . Gía cả cũng rẻ , mua trên tiki còn được giảm giá nữa Bạn mình cũng có 1 cuốn nhưng nó là cuốn phô bằng giấy a4 nên nó toàn sài chung với mình , hihi. Tuy nhiên , sản phẩm hay hết hàng nên mấy lần mình định đặt dùm bạn mà toàn hết à , mong tiki nhập hàng thường xuyên hơn
5
1237322
2016-07-08 12:45:02
--------------------------
466824
4118
Mình đã mua cuốn phrasal verbs này để tra cụm động từ cho nhanh hơn và dễ dàng hơn khi không phải cầm cuốn từ điển nặng gần kí. Cuốn phrasal verbs này có cách sắp xếp logic, hợp lí, dễ tra cứu, mỗi cụm động từ có giải thích nghĩa và ví dụ minh họa nên dễ nhận biết. Nhưng cũng có bị thiếu một vài cụm thôi, không đáng kể. Mình nghĩ các bạn nào mà từ điển có đầy đủ phrasal verbs rồi thì nên cân nhắc kĩ trước khi mua vì cuốn này cũng hơi mắc và bạn nào từ điển chưa có hay không có đủ các phrasal verbs thì nên mua cuốn này vì nó rất cần thiết và quan trọng trong môn học tiếng anh đặc biệt là phần ngữ pháp.
4
1299217
2016-07-02 22:42:25
--------------------------
465434
4118
Trong khi học tiếng Anh mình rất hay nhầm lẫn giữa các phrasal verb. Có những từ có rất nhiều giới từ kèm theo mang nhiều nghĩa khác nhau làm mình không thể nào nhớ nổi. Vì vậy quyển sách này là một cứu cánh cho mình. Sách được trình bày như một cuốn từ điển, mỗi phrasal verb có giải nghĩa tiếng Việt và câu ví dụ minh họa. Mình thường dùng khi cần tra cứu nhanh hoặc đọc dần dần để ôn tập. Tuy nhiên nếu bạn nào cần hiểu cặn kẽ hơn thì nên tham khảo thêm trong các từ điển Anh-Anh uy tín vì dung lượng quyển sách này có hạn, nó chỉ có các từ thông dụng cũng như nghĩa thông dụng thôi.
4
32513
2016-07-01 01:00:23
--------------------------
455419
4118
Trước đây cụm động từ là điều khó khăn đối với mình vì thường thì trong từ điển Anh-Việt dạng cầm tay hay từ điển trên điện thoại ko có các cụm động từ. Nhưng từ khi có cuốn này thì việc tra nghĩa cũng trở nên dễ hơn. Nó như một quyển từ điển riêng về cụm động từ vậy. Tuy nhiên quyển này còn thiếu rất nhiều từ thông dụng mà mình nghĩ nếu bổ sung thêm thì sẽ tốt hơn. Thiết kế cũng khá nhỏ gọn vừa tầm tay, dễ mang đi học. Nói chung là khá ok
4
633743
2016-06-22 15:44:23
--------------------------
447193
4118
Vừa mua cuốn sách này, tôi thích kích cở của nó là gọn, bìa mềm, dễ cầm nắm (tôi không thích bìa cứng rất khó cầm trên tay để lật ra). Sách có phần diễn giải cả nghĩa tiếng anh và tiếng việt, (tiếng anh màu đỏ, tiếng việt màu đen không bị rối).
Nhưng với ý kiến riêng của tôi, để dễ nhìn hơn nữa. Sau phần diễn giải, Tác giả nên xuống dòng ở mỗi câu ví dụ.  Có thể điều này làm cuốn sách dày hơn, nhưng sẽ dễ nhìn hơn,
Nhưng tôi rất thích cuốn sách này. Cảm ơn tác giả.

5
41266
2016-06-13 17:05:26
--------------------------
435107
4118
Phrasal verb là phần ngữ pháp Tiếng Anh khá khó đối với những bạn có ý định thi vào chuyên anh. Nhưng nhờ có quyển này mà mình đã có thể ghi nhớ được nhiều từ hơn, vì sách nhỏ gọn và có thể mang đi bất cứ đâu, như là một quyển từ điển thu nhỏ vậy. Mặc dù vậy, sách cũng có những điểm trừ nho nhỏ. Đó là các phrasal verb chưa được phong phú cho lắm, vì lúc mình cần tra từ nào đó thì sách lại không có. Vì vậy, mình mong bên nxb sẽ hoàn thiện hơn cho những bản in tiếp theo.
4
945772
2016-05-24 14:11:27
--------------------------
422815
4118
Trước hết là giá thành rẻ hơn nữa Minh mua ở nhà sách vì mình mua khi Tiki đang có đợt khuyến mãi . Thành ngữ trong tiếng Anh rất nhiều và khó nhớ nhưng quỷên sách này đã giúp mình rất nhiều trong việc luyện viết. Quỷên sách nhỏ nhưng có khá đầy đủ những thành ngữ thông dụng và tiện lợi cho việc mang theo trong túi xách. Giấy cũng khá dày , mực in tốt không bị lem, bìa sách đẹp mắt . Nói chung mình rất hài lòng về sản phẩm  này . Tiếp tục mua hàng Tiki  vì chất lượng và phát chuyển nhanh .
5
650348
2016-04-28 09:39:28
--------------------------
419717
4118
Sách trình bày ngắn gọn, dễ hiểu, ví dụ cho kèm cũng rất dễ nhớ. Màu sắc cũng rất bắt mắt, khiến mình lúc nào cũng muốn nhìn, đọc và học và giúp mình trau dồi thêm số lượng cụm động từ mình có - cái mà trước đây, khi thi đại học mình đã từng rất sợ. Kích thước nhỏ gọn, nên mình có thể dễ dàng mang theo mọi nơi. Tuy nhiên, theo mình thì nxb nên đưa thêm nhiều phrasal verb hơn nữa, bởi vì có nhiều từ mình cần tra, nhưng khi mở sách ra để kiếm thì lại ko có... cảm giác cứ hụt hẫng thế nào ấy. Nhưng tóm lại, mình cũng khá thích cuốn sách này.
4
88399
2016-04-21 21:03:01
--------------------------
411306
4118
Mình thấy, việc sử dụng cụm động từ trong văn nói và cả văn viết rất cần thiết. vì không thể cứ dịch riêng lẻ như vậy sẽ rất khó hiểu nên mình quyết định phải mua quyển cụm động từ Phrasal verbs. Thấy trên tiki có nên đặt hàng ngay. Khi nhận hàng, sách được đóng gói khá cẩn thận, sách nhỏ gọn rất thuận tiên cho việc mang theo. Sách sử dụng nền trắng, kết hợp phông chữ đỏ và đen cảm thấy rất thích thú khi học. Sách giải thích nghĩa rõ ràng, phân đúng loại từ nữa. Thật đáng để đem cuốn này về nhà.
4
480717
2016-04-05 20:38:27
--------------------------
407516
4118
Mới có được em này về. Mở ra nhìn bất ngờ dễ sợ. Do cứ nghĩ là quyển sách chắc hẳn lớn lắm. Thế mà mở ra là quyển sách có khổ khá nhỏ. Nhưng vậy mình mới thích. Rất dễ đem theo mỗi khi đi học mà không thấy nặng. Sách rất hay. Mỗi từ sẽ được dịch nghĩa và cho ví dụ rất gần gũi và dễ hiểu. Tuy nhỏ nhưng mà có nhiều  từ lắm nha. Nói chung với trình độ bây giờ bấy nhiêu đó đã đủ rồi. Bìa sách thì màu không được đẹp lắm.
5
665919
2016-03-30 09:52:13
--------------------------
407019
4118
Mình trước giờ rất yếu về cụm động từ, đã vậy trong đề thi đại học phần này thường chiếm rất nhiều điểm. Vậy nên mình đã đặt mua cuốn này để về nhà tự học, tự cải thiện bản thân.

Học hết những cấu trúc trong này thì chắc không thể rồi, nhưng khi làm bài mình cứ tra cấu trúc rồi dần dần cũng nhớ được chút ít. Cuốn này nhỏ nhắn, dễ cho vào cặp học chính hoặc cả cặp học thêm. Nhỏ như cuốn sổ tay ý, rất tiện ích. Mặc dù nhỏ nhưng lượng kiến thức lại không nhỏ chút nào. Rất hài lòng khi mua cuốn này.
4
616425
2016-03-29 12:23:01
--------------------------
401041
4118
Sách giấy đẹp, chữ in màu bắt mắt, tránh gây sự nhàm chán, nhỏ gọn dễ cầm  tay. Tuy nhiên, cách trình bày giữa các từ hơi xít vào nhau, nên hơi nhiều. Các từ vựng được trình bày trong sách rất phong phú, để giúp người học tra cứu và có thể tự đọc, học trước trau dồi thêm vốn từ vựng.Tuy nhiên giá thành gốc thì có vẻ chát, 72k cho một cuốn sách nhỏ. Tuy nhiên mình thấy Cuốn sách Cụm Động Từ (Phrasal Verbs) này thật sự hay và bổ ích. Vì cụm động từ trong tiếng anh là một phần khó học.
5
854660
2016-03-20 08:30:21
--------------------------
400698
4118
Mở sách ra đọc vài trang đầu mình rất thích vì phần giải thích rất rõ ràng, chi tiết và dễ hiểu. Về phần nội dung thì được chia theo thứ tự ABC, rất thuận tiện cho việc tra cứu. Nói đây là một cuốn từ điển cũng không hẳn. Nó nhỏ gọn hơn, phù hợp để mang theo bên mình. Phrasal Verbs là một phần rất quan trọng nhất là trong văn nói tiếng Anh. Sách hai màu và giấy rất đẹp. Giá 72000 đồng, còn lại được tiki giảm giá. Mình đặt hàng chiều hôm trước thì ngày mai đã nhận được rồi trong khi mình không chọn hình thức giao hàng nhanh. =)))
5
746262
2016-03-19 19:11:50
--------------------------
364781
4118
Khi học tiếng Anh, học các cụm từ là cái khó muôn thuở của tôi. Khi dò những từ điển bình thường hoặc là Lạc Việt thì đôi khi cụm từ đó hoặc là thiếu nghĩa hoặc là không có ví dụ minh họa thậm chí còn không có trong từ điển khiến cho việc học cụm từ trở nên vô cùng khó khăn. Từ khi có quyển sách này thì điều đó đã trở nên dễ dàng hơn nhờ bố cục trình bày rõ ràng, bắt mắt, nhìn là muốn học ngay.Nội dung cũng rất đầy đủ và có ví dụ rõ ràng cho từng nghĩa cụ thể nên rất dễ học.Cảm ơn vị cứu tinh cho Phrasal verb.
5
739994
2016-01-06 20:57:36
--------------------------
364692
4118
Với học sinh như mình, việc học các cụm động từ thực sự rất khó khăn vì ta không thể cắt nghĩa của nó bằng cách ghép nghĩa của động từ với nghĩa của giới từ, hoặc có nhiều cụm động từ có rất nhiều nghĩa có khi đến chục nghĩa, hay nhiều cụm có nghĩa giống nhau nhưng dùng trong các hoàn cảnh khác nhau...Thế nhưng, tất cả đã trở nên dễ dàng nhờ cuốn sách này. Nó như một cuốn từ điển nhỏ gọn tiện dụng với các cụm từ được sắp xếp theo thứ tự bảng chữ cái A B C và một cụm từ còn được giải nghĩa tiếng Việt rõ ràng dễ hiểu, có ví dụ sinh động. Thật tuyệt!
5
1096195
2016-01-06 18:11:12
--------------------------
361876
4118
Vì gặp rắc rối trong việc tra cứu cụm động từ nên mình đã quyết định mua sản phẩm này. Sách nhỏ gọn cầm vừa lòng bàn tay quan trọng là nội dung của sách rất hay.
Tác giả đã cho cách nhận biết từ nào là nội động từ hay ngoại động từ, từ nào có giới từ đi kèm hay không. Mỗi cụm động từ là nhiều nghĩa khác nhau, mỗi nghĩa lấy một ví dụ cụ thể dễ hiểu rất có ích cho những người học cụm động từ tiếng anh. Vì vậy sản phẩm là một lựa chọn tốt cho những ai muốn học cụm động từ.
5
646511
2015-12-31 20:16:26
--------------------------
333571
4118
Cuốn sách có kích thước nhỏ gọn bỏ túi được, khá dày, in màu đẹp, chất lượng như vầy mà giá này thì rất hợp lý. Giống như một cuốn từ điển mini, mỗi từ có giải nghĩa bằng tiếng Anh/ Việt và kèm ví dụ. Phrasal verb là một trong những thứ rắc rối nhất trong tiếng Anh, hầu như kỳ thi nào cũng có nên quyển sách này rất cần thiết cho người học tiếng Anh. Nhưng mà vì gần như là từ điển nên sách không có bài tập, nếu có một số bài tập ngắn giúp ghi nhớ phrasal verb tốt hơn thì đây là cuốn sách hoàn hảo.
4
225938
2015-11-08 06:41:52
--------------------------
327464
4118
Cụm động từ là một phần rất khó trong tiếng anh, vì vậy quyển này rất hữu ích không chỉ cho học sinh mà còn giáo viên chuyên ngành nữa.. giấy in tốt, ví dụ lấy rất thực tế không mông lung.  Quyển sách rất nhỏ gọn như từ điển thu nhỏ vậy rất tiện cho việc mang đi khắp nơi. Sách được sắp xếp theo A--Z rất dễ để tra nhưng có điều mình thấy sách nên bổ sung hình ảnh minh họa để cho dễ thuộc từ hơn. Nhiều lúc đọc mà hoa hết cả mắt vì 1 từ phrasal verb rất nhiều nghĩa nên mình mong tác giả cho hình ảnh minh họa vào.Nhưng mình nghĩ cuốn này như thế là rất hay  rồi. Cám ơn tác giả............
4
869938
2015-10-27 21:53:18
--------------------------
312528
4118
Sách không có phần đọc thử nên mua rồi mới biết được nội dung là như thế nào. Nói chung là giống như 1 cuốn từ điển về các cụm động từ vậy, kích thước sách nhỏ gọn loại sách bỏ túi để có thể tra bất cứ lúc nào. Bố cục tra từ được đánh thứ tự Alphabet gồm rất nhiều từ tra mà hoa cả mắt. Không có hình ảnh minh họa, mỗi cụm động từ như vậy sẽ có vài câu ví dụ minh họa cách dùng và ngữ nghĩa. Nếu để tra từ thì ok, còn nếu như để học từ mới thì hơi khó nhớ. Nếu có thêm phần giải thích nguồn gốc từ và có hình ảnh sinh động thì sẽ tốt hơn. Nhưng in general là cũng đáng mua để tra những cụm từ lạ hay để làm bài tập.
4
770071
2015-09-21 15:31:56
--------------------------
310233
4118
sách giống như 1 quyển từ điển thu nhỏ rất tiện trong việc tra từ và mang theo. còn có các ghi chú như ngoại động từ hay nội động từ, tách hay không tách,... sách giải thích theo cả tiếng việt lẫn tiếng anh. còn có các ví dụ để mình hiểu rõ nghĩa hơn. màu chủ đạo của sách là màu đỏ đô nên không bị chói mắt, dễ chú ý hơn. nhưng cũng có một số từ thông dụng để làm bài tập lại không có ghi trong sách nên mình phải tra ở sách khác. mình thấy đây là quyển sách bạn không thể thiếu khi làm bài tập tiếng anh và muốn học thêm nhiều từ mới ngoài sách
4
656157
2015-09-19 13:32:07
--------------------------
299660
4118
Thật tuyệt ! Cuốn sách nhìn như 1 cuốn từ điển bỏ túi vậy đó . Các cụm động từ đc sắp xếp theo bảng chữ cái từ A --> Z nên rất là dễ tìm kiếm . Giấy in màu tốt và đẹp . Mỗi cụm động từ đều có 1  ví du đi kèm và được dịch sang tiếng Việt ngay dòng bên cạnh. Trước đây mình rất khó xác định nghĩa của động từ với tính từ đi kèm , thật khó vì chỉ thay đổi giới từ hay phó từ thì nghĩa sẽ thay đổi ngay . Bây giờ cầm được sách trên tay nên lại có động lực học tập tiếp rồi ^^ .
5
490142
2015-09-13 14:04:14
--------------------------
297106
4118
Mình học ngoại ngữ nên phần phrasal verbs là phần khá quan trọng đối với mình, nhưng hầu như không có cuốn sách nào bao gồm đầy đủ tất cả kiến thức về phần này. Nếu nói đã đầy đủ tất cả các cụm động từ thì cuốn sách này chưa đủ, nhưng phần nội dung trong sách được trình bày rất rõ ràng, dễ dàng tra cứu và giúp người học hiểu tường tận về các phrasal verbs, không những vậy còn có ví dụ để dễ dàng . ghi nhớ, giấy in sách cũng rất đẹp. Nói chung mình rất hài lòng về sản phẩm này, nếu muốn nâng cao trình độ về tiếng anh, nên mua cuốn này
5
115397
2015-09-11 16:22:11
--------------------------
291999
4118
Vốn là 1 người nắm khá chắc ngữ pháp tiếng Anh nhưng một điều đáng buồn mà tôi phải thành thật thú nhận, đó là tôi luôn gặp rắc rối với các Cụm Động Từ (Phrasal Verbs). Động từ trong tiếng Anh đơn thuần đã có tới hàng trăm, nhưng khi chúng kết hợp với các giới từ đi kèm thì thật sự là một nỗi ám ảnh lớn bởi với mỗi sự kết hợp với nhau chúng lại tạo ra các ý nghĩa khác nhau, và để làm chủ được chúng thật sự là 1 điều không hề dễ dàng gì. Với cuốn sách được thiết kế nhỏ gọn, tiện dụng, linh động dễ mang đi bất cứ đâu và 1 hệ thống Cụm Động Từ (Phrasal Verbs) đầy đủ được thiết kế có khoa học chính là 1 vị cứu tinh giúp tôi xóa bỏ nỗi lo âu bấy lâu nay.
5
769896
2015-09-06 22:18:21
--------------------------
291210
4118
Quyển sách này đã giúp mình có thêm kiến thức về các cụm động từ tiếng anh. Lúc đầu khi được cô giáo cho bài tập về Phrasal verbs thì mình khá là lo lắng và băn khoăn rất nhiều. Mình chả hiểu được ý nghĩa của mỗi động từ. Mình hơi thất vọng nhưng khi về nhà mình lên ngay Tiki và đã tìm được một quyển cụm động từ này. Mình hết sức ưng ý sách, sách tốt về mặt nội dung cả chất lượng. Nội dung sách hay, đưa ra những ví dụ cụ thể. Chất lượng của giấy tốt, bìa đẹp. 
5
714041
2015-09-06 08:33:05
--------------------------
287690
4118
Đây là một quyển sách khiến mình khá hài lòng về cách trình bày lẫn nội dung :) Về cách trình bày, cuốn sách được thiết kế tối giản, dễ tra cứu và dễ sử dụng. Sách có hình dạng thuôn dài, kích thước khá gọn, dễ mang theo. Các cụm từ được sắp xếp theo thứ tự abc như bảng chữ cái khiến việc tra cứu dễ dàng hơn. Về nội dung, các cụm từ được giải thích, lấy ví dụ rất rõ ràng, thực tế. Mình thích học tiếng Anh, tuy nhiên, có đôi khi sẽ gặp phải các cụm động từ có nghĩa khác hoàn toàn so với nghĩa của hai từ ghép tạo thành nó, nên đây cũng là một cuốn sách rất có ích khi làm các bài tập ngữ pháp hay áp dụng vào giao tiếp tiếng Anh.
5
562067
2015-09-02 22:35:59
--------------------------
269906
4118
Cuốn sách này là một sự lựa chọn rất tốt dành cho những bạn muốn học về các cụm động từ Tiếng anh. Từng cụm động từ được sách chú giải rất chi tiết về nghĩa , cách dùng và đặc biệt là ở về phần ví dụ, rất thực tế và dễ hiểu. Và các cụm động từ được sắp xếp theo vần abc nên rất dễ tra cứu. Phần in ấn của sách cũng rất tốt, nhất là giấy rất dày và dễ nhìn. Nói chung đây là một cuốn sách ngữ pháp tiếng anh rất bổ ích, phù hợp với tất cả mọi người.
5
431082
2015-08-17 20:41:28
--------------------------
237731
4118
Tác giải An Bình đã đưa ra quyển sách tham khảo này cũng với Nhà Xuất Bản Hồng Đức. Cuốn sách bao gồm gần 300 trang giới thiệu cụm động từ, giải thích nghĩa của nó, cũng như việc đưa ra các ví dụ dùng chúng trong thực tế. Mình đã mua cuốn sách này khi đang học khóa học TOEIC và phát âm tại trung tâm Share của anh Trần Thanh Sơn - một người mình ngưỡng mộ và cũng là bạn của mình. Cuốn sách rất hữu ích cho mọi hoạt động về Tiếng Anh như ôn luyện thi, trau dồi thêm, ... của mọi lứa tuổi.
Sách được thiết kế đầy sáng tạo và logic, dễ dàng nhất cho người học tập.
5
334680
2015-07-22 17:57:55
--------------------------
228389
4118
Mình nhận thấy đấy là một cuốn sách khá là hữu ích, cuốn sách này không chỉ phù hợp cho những bạn  ôn luyên Toeic mà còn phù hợp cho những bạn  ôn luyện thi đai hoc, thi vào lớp 10 chuyên...v..v... Lúc mình ôn thì Toeic có nhiều câu sử dụng cụm động không chỉ ở phần nghe mà còn cả ở phần đọc, mình tìm trên mạng thì đôi lúc có cụm từ đó nhưng đội lúc lại không, mà không thì nhiều hơn. Những lúc như vậy, mình cảm thấy rất ứng chế, thế là mình tìm trên mạng hiên nay có cuốn từ điển nào về cụm động từ không. Và mình đã tìm được cuốn sách này. Mình xin kể một số điểm cộng của sách như sau:
1. Sách có giải thích nghĩa bằng tiếng việt và đặc biệt là tiếng anh
2. Sách thích rõ chi tiết đây là ngoại động từ  hay nội động từ, có thể tách ra hoặc bắt buộc viết liền (những cái này trên mạng không hề có)
3. Ví dụ có dịch nghĩa (đây cũng là điều mình rất thích)
5
694519
2015-07-15 13:19:25
--------------------------
210592
4118
Cuốn sách này thật sự là sự lựa chọn hoàn hảo cho những người mới bắt đầu học tiếng anh cần nắm chắc cụm từ để có thể hoàn chỉnh kĩ năng viết đúng câu đúng ngữ pháp ngữ nghĩa. Nó phân loại theo thứ tự alphabet như cuốn từ điển thu nhỏ nên rất dễ tìm từ cần tìm hơn nữa mỗi từ đều có ví dụ minh họa cho từng cụm từ có dịch ra tiếng việt nên rất dễ để nắm bắt. Thật sự mà nói đây là một sản phẩm giúp ích rất nhiều cho việc học tiếng anh của mình.
5
126394
2015-06-19 17:14:16
--------------------------
194748
4118
Phrasal verb là một phần khó và quan trọng trong tiếng Anh. Khi có quyển sách này mình cảm thấy học cụm động từ trong tiếng anh dễ đang hơn rất nhiều. Sách trình bày các cụm động từ giống như cuốn từ điển, theo vần abc. Mỗi Phrasal Verb đều có đưa ra một ví dụ minh họa tương ứng với từng nghĩa khác nhau của nó. Về kích cỡ cuốn sách khá nhỏ và tiện dụng khi mang theo nên có thể dễ đang học ở mọi lúc mọi nơi. Tuy vậy cuốn sách này không có bài tập minh họa và có nhiều từ chưa được đề cập tới trong sách. Nhưng đối với mình thì đây vẫn là cuốn sách bổ ích.
4
466113
2015-05-11 18:33:42
--------------------------
188126
4118
Trước đây, mình học không được chắc cho lắm về các cụm động từ, vì vậy mình đã mua cuốn Cụm Động Từ (Phrasal Verbs) và thấy rất hiệu quả. Cuốn sách có bố cục rất logic, các loại cụm động từ theo từng chủ đề: cụm động từ cho các hoạt động buổi sáng, cụm động từ cho việc di chuyển, các hoạt động giải trí... Và trong từng chủ đề, có ví dụ cụ thể cho mình hiểu và nắm chắc, nhớ rõ hơn. Nhờ vậy mà mỗi khi làm bài tập trắc nghiệm tiếng anh, mình không còn đắn đo nên chọn phương án nào vì cuốn sách đã giúp mình củng cố rất nhiều vốn kiến thức. Mình rất hài lòng khi sử dụng cuốn Cụm Động Từ (Phrasal Verbs) ^^
4
611652
2015-04-24 19:23:02
--------------------------
