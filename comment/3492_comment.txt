330763
3492
Tôi đã tìm mua quyển sách này từ rất lâu. Và 1 dịp tình cờ tôi thấy quyển sách này bán ở tiki. Và tôi đã không do dự, mua quyển sách này. Tôi là người theo đạo thiên chúa, vì thế, tôi muốn con tôi tìm hiểu thêm về tôn giáo này. Quyển sách có cách viết dễ hiểu, thu hút, có hình ảnh màu minh họa, sống động. Những chi tiết lịch sử tôn giáo được lồng vào những câu chuyên gần như dạng dân gian. Vì thế, trẻ em rất hứng thú, vừa đọc chữ, vừa xem hình, ít chữ, nhiều hình minh họa. Lối  kể chuyện dễ hiểu, sinh động. Mỗi ngày tôi dùng quyển sách này để kể chuyện cho con tôi nghe. Tôi rất hài lòng với quyển sách này.
5
404105
2015-11-03 10:47:37
--------------------------
