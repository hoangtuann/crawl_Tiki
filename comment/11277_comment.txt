472227
11277
Mỗi khi mình cảm thấy mệt mỏi, chán nản, hay áp lực học hành, cuốn truyện như người bạn thân của mình í. Nó ko cần phải chia sẻ gì với mình, chỉ cần mình giở ra đọc lúc buồn, thì cỡ nào mình cũng phải cười. Truyện dùng để xả stress rất hữu ích. À trong tập này cu Shin vào lớp 1 nè, cu cậu mà cứ chậm chạp như hồi mẫu giáo vậy là không được đâu nhé. Hehe. Nhà cu Shin bị nổ, vậy là phải chuyển đến căn hộ khác nhỏ hơn, tuy vậy mà tình cảm gia đình vẫn không rạn nứt, có khi lại nhiều hơn. Tuy không gian nhỏ nhưng lúc nào cũng ấm cúng và tràn ngập tiếng cười ^^.
5
163977
2016-07-09 12:21:48
--------------------------
404090
11277
Biết shin hồi học cấp 2 đên giờ mình đã đọc gần hết 50 tập shin truyện ngắn. Có thể nói là mình rất thích shin, mình rất quý mến cậu bé này. Những trò quậy phá đau đầu, nhức óc với shin như trò ngoáy mông, đóng giả,....đều làm chúng ta rộ lên tiếng cười vui nhộn. Bạn sẽ vô cùng hạnh phúc khi sống lại tuổi thơ với shin, cả người lớn và trẻ con ai ai cũng thích đọc shin nhỉ. Shin không chỉ mang lại tiếng cười mà trong đó còn chứa cả tình cảm gia đình và bạn bè
5
1203012
2016-03-24 17:53:19
--------------------------
348927
11277
Biết truyện Shin-Cậu bé bút chì qua đứa bạn, lúc đầu cũng đọc cho vui thôi nhưng giờ ghiền luôn ạ, thể hiện rõ sự tinh nghịch của Shin, đứa trẻ hiếu động, hơi quậy phá nhưng vẫn thể hiện bản thân là người dũng cảm giúp bảo vệ những người xung quanh :))) chất lượng sách khỏi chê,in màu rõ ràng, bìa cứng nhìn bắt mắt. Đưa cho cháu đọc thế là mỗi lần có điểm cao là cứ đòi mua :)
Sách giá mềm nên mua tặng cho cháu thì còn gì bằng :))) 
Tiki gói hàng cẩn thận,giao hàng nhanh chóng, Thank Tiki 
5
783648
2015-12-07 11:35:10
--------------------------
206592
11277
Thế giới trẻ con với bao điều lí thú mà người lớn cũng chưa khám phá hết, đến với Shin - Cậu Bé Bút Chì thì mình cảm thấy thật diệu kì. Đặc biệt Shin - Cậu Bé Bút Chì (Tập 29) mình ấn tượng nhất là  chuyện Masao cảm thấy xấu hổ khi đi vệ sinh trong nhà trường do bị bạn bè trêu trọc nhưng khi cu shin xuất hiện đã làm cho mọi người có cái nhìn khách quan hơn trong điều ấy.Cậu bé ấy thật dũng cảm khi có lên tiếng trong khi đi toilet, góp phần làm cho học sinh ở trường không ngại khi đi vệ sinh nữa.
Bìa truyện hấp dẫn, sáng tạo, độc đáo, hấp dẫn. @@
5
355857
2015-06-10 09:55:31
--------------------------
