434157
9237
9 năm học ròng rã trôi qua trong chớp mắt mà ngoại trừ những kiến thức cơ bản,... thì còn lại là: SẠCH SÀNH SANH
ĐIỀU gì đã xảy ra trong não tôi khiến nó "quên" nhanh đến thế, hay câu hỏi chính xác hơn là : TẠI SAO???
Tại vì tôi chưa áp dụng "đủ nhiều" phương pháp hay tại quá lười? KHÔNG, tôi đã thử nhiều cách đấy, nào là đứng ,nằm ,ngồi , thuộc bài tới nỗi đọc không sót 1 dấu chấm, phẩy...và "chỉ" học 5,6 tiếng 1 ngày !
Có 1 cách giải thích và cũng là duy nhất: tôi chưa thực sự học đúng cách!
và câu trả lời đó tôi đã tìm được trong cuốn sách này,mang tên:SƠ ĐỒ TƯ DUY.
Bằng cách học tiên tiến nhất, hiệu quả nhất, tôi sẽ hiểu bài theo cách tuyệt vời và ngẫu hứng mà tôi chưa từng được biết.
Phương pháp này có thể nói vượt trội nhiều lần so với cách truyền thống cổ hữu vì
kết hợp được cả hai bán cầu não trái ,thỏa sức sáng tạo kích hoạt tiềm năng để hiểu được vấn đề 1 cách mạch lạc nhất có thể.
Và khi đã hiểu được vấn đề thì chúng ta sẽ nhớ nó
5
1156058
2016-05-22 13:53:18
--------------------------
347239
9237
Với những ví dụ hài hước, ngôn từ thông minh, bài tập gây "kích thích"... Sơ Đồ Tư Duy là món quà tuyệt vời nhất mà bạn nên dành tặng cho não bộ của mình.

Về giá thì cuốn này khá đắt, 360K nhưng hoàn toàn đáng để bỏ ra.
Về bố cục, tác giả dùng những hình ảnh và bố cục cực kỳ kích thích não bộ, bạn sẽ đầy hứng thú mỗi khi đọc quyển sách này.
Phần bài tập hay thực hành, mới đầu bạn sẽ gặp chút khó khăn, chỉ cần kiên trì làm theo những phương pháp mà Tony Buzan hướng dẫn, tư duy của bạn sẽ phát triển không giới hạn.
5
1019157
2015-12-04 08:49:00
--------------------------
306646
9237
Tôi biết đến Mind Map từ khá lâu nhưng chưa thực sự tìm hiểu và áp dụng vào công việc
Cho tới khi tôi đọc cuốn "Tôi tài giỏi, bạn cũng thế" cách đây khoảng 3 tháng, mặc dù trong đó chỉ đề cập 1 phần nhỏ về mind map nhưng nó đã khiến tôi cảm thấy "Phải tìm hiểu"
Tìm trên mạng có rất nhiều sách nhưng khi đọc phần nhận xét của mọi người, tôi đã chọn cuốn này
Tôi tin cuốn này không phải chỉ để đọc 1 lần mà nó sẽ còn được xem đi xem lại nhiều lần. Quả thật đây là 1 phát minh vĩ đại cho nhân loại
Tôi đang mong chờ được khám phá và sử dụng cuốn sách này
5
777678
2015-09-17 18:17:19
--------------------------
274108
9237
Đã đọc quyển này lâu nhưng phải tới giờ mới ghi nhận xét. 
Sau 1 khoảng thời gian đọc, học và áp dụng, phương pháp học này thực sự quá tuyệt, cực kì thích hợp với mình, nhất là năm nay khi mình học cuối cấp, chuẩn bị vào đại học, cảm thấy vô cùng may mắn khi đã mua cuốn sách này.
Tốn 1 khoảng thời gian để quen dần và say mê nó ! 
Trình bày thì không có gì chê trách, quá đẹp, tuy nhiên cuốn của mình thiếu mười mấy trang cuối , khiến minh hơi ức chế nên mới cho 4 sao, cũng không đổi trả vì mình ở xa, mong lần sau Tiki chú ý hơn trước khi giao hàng .
4
186614
2015-08-21 17:31:01
--------------------------
210981
9237
Đây là cuốn sách hoàn hảo nhất về sơ đồ tư duy tôi từng đọc. Tony Buzan có lẽ không còn qua xa lạ gì đối với chúng ta. Khi cầm được cuốn sách trên tay t đã đọc nó liên tục trong 3 ngày liên tiếp nhưng chưa đủ, tôi phải đọc đi đọc lại nó để có thể hiểu được hết nó nên việc đó tốn hơn 2 tháng của tôi. Nếu như bạn mua về, bạn đọc vẫn còn thấy mơ hồ thì đừng lo lắng, bởi cuốn sách như một bản hoàn chỉnh về sơ đố tư duy. Càng đọc bạn sẽ càng thấy sơ đồ tư duy là một công cụ rất mạnh và vô cùng thú vị !
5
554890
2015-06-19 23:04:17
--------------------------
191510
9237
Quyển sách Sơ đồ tư duy này của hai anh em tác giả Tony & Barry Buzan là quyển sách mà tôi ưa thích nhất trong giá sách của mình. Thứ nhất do nó được in màu và làm bằng giấy rất tốt. Thứ hai sơ sồ tư duy là một công cụ hoàn hảo để sáng tạo, đưa ra những giải pháp không tưởng có thể tháo các nút thắt trong tất cả những vấn đề của cuộc sống. Từ lịch sử hình thành và phát triển cho tới những cách lập sơ đồ tư duy mang dấu ấn của riêng chúng ta được các tác giả trình bày rất dễ hiểu. Quan trọng nhất là chúng ta vừa đọc vừa áp dụng các kiến thức trong sách vào thực tiễn thì chẳng mấy chốc chúng ta sẽ nhận ra sự tiến bộ của mình.
5
387632
2015-05-01 22:01:03
--------------------------
168652
9237
Không chỉ riêng cuốn sách này mà những cuốn sách khác của Tony Buzan đều rất hữu ích và ý nghĩa. Sơ đồ tư duy - một phương tiện học tập hiện đại đã được Tony Buzan sáng tạo nên. Ông đã khai thác được chức năng của sơ đồ tư duy trong học tập, lập kế hoạch và còn cho nhiều hoạt động khác nữa. Chỉ với vài bước, mình đã có thể hoàn thành một sơ đồ tư duy với các hình ảnh, màu sắc, ý chính. Vừa tiện lợi, cho mình có thể tự do cách trang trí, vừa có thể đảm bào được các ý chính trong bài, kế hoạch,.. mà ta có thể nhớ lâu. 
 Hãy đọc thử quyển sách này, bạn sẽ biết về sơ đồ tư duy - một công cụ học tập mới, tiện lợi!
5
540461
2015-03-16 23:20:53
--------------------------
153690
9237
Sơ đồ tư duy có lẽ là sáng kiến quan trọng nhất trong lĩnh vực giáo dục thời hiện đại. Một công cụ đơn giản, dễ sử dụng mà vô cùng hiệu quả. Dùng sơ đồ tư duy khi học giúp kết hợp hai bán cầu não trái và phải, đảm bảo công suất làm việc của não đạt tối đa. Sơ đồ tư duy cho phép phát huy tính sáng tạo, tự do phá vỡ những rào cản thông thường của giáo dục cổ điển. Mình bắt đầu sử dụng sơ đồ tư duy bốn năm trước, và công cụ này trở thành một phần quan trọng giúp mình học tốt, học ngôn ngoan mà không gian nan. Không chỉ dùng cho việc học, có thể áp dụng sơ đồ tư duy để lên kế hoạch làm việc, sắp xếp công việc khoa học, kĩ lưỡng.
5
515736
2015-01-27 09:14:17
--------------------------
77606
9237
đây không phải là tác phẩm đầu tiên mình đọc của Tony Buzzan nhưng có lẽ đây là tác phẩm mình thích nhất của tác giả.
Có lẽ nó được coi là phần chi tiết của lập sơ đồ tư duy đã được nói đến trong cuốn "sử dụng trí tuệ của bạn" của Tony Buzzan. Lúc đọc cuốn sách "sử dụng trí tuệ của bạn" mình đã rất thích phần này. Nhưng thật tiếc trong cuốn sách" sử dụng trí tuệ của bạn" nó chỉ bao quát về cách lập sơ đồ tư duy chứ không nói rõ ràng. Và rồi đến khi biết được có cuốn sách này của tác giả mình đã không ngần ngại mua ngay nó.
Và quả thật nó đã không làm mình thất vọng. Cuốn sách khá là hay. Nó nêu chi tiết các bước, cách thưc hiện một sơ đồ tư duy hoàn chỉnh. Sơ đỒ tư duy không chỉ có thể áp dụng trong việc học tập mà nó còn có thể áp dung trong thứ khác đấy: như sinh hoạt hằng ngày, công việc, lịch trình,.....
Và nó quả là công cụ tiện ích, cần thiết trong cuộc sống ngày nay của mọi người.
P/S: Mình rất thích cuốn này, cực thích luôn nó.
5
60584
2013-05-28 20:41:57
--------------------------
64852
9237
Sơ đồ tư duy – Nếu ai đó hỏi t điều gì cần học đầu tiên, tôi luôn khuyên bằng 1 từ khóa “ sơ đồ tư duy “.
Trong thế giới quá nhiều điều để học, bạn bị “ loạn  thông tin “. Sơ đồ tư duy – mindmap là 1 cách tốt nhất – nhanh nhất – hiệu quả nhất tiếp cận được với kiến thức và hệ thống hóa .
Kiến thức , áp dụng nhanh, nhớ lâu, tự do, không bó buộc tư tưởng truyền thống. Nhiều màu sắc, hài hước… 
Điểm khó – là dành cho người nếu đọc từ đầu – cần kiên nhẫn mới có thể hiểu hết và áp dụng đúng . 
Đọc 1 lần và áp dụng ngay – cả đời.

5
37796
2013-03-22 08:58:17
--------------------------
64280
9237
Cách chúng ta suy nghĩ ảnh hưởng đến mọi kết quả. Thật vậy, tôi chỉ nhận được điều này sau khi rơi vào bế tắc. 12 năm học phổ thông tôi đều đạt điểm giỏi, nếu không muốn nói là gần như 9 chấm tất cả các môn. Ừ, vì giáo dục Việt Nam tạm thời hơi đáng buồn là học thuộc lòng, vì vậy được điểm cao đâu khó.

Nhưng khi lên đại học là lúc tôi bắt đầu thay đổi! Không phải là vì áp lực điểm số nữa, mà là vì tôi lớn lên. Tôi bắt đầu nghĩ mình sẽ học được gì giữa một biển kiến thức đại học. Tôi bắt đầu phải tìm ra ý nghĩa ứng dụng cho những kiến thức mình học. Điều trước tiên cần làm là nhớ. Hiển nhiên không phải là thuộc và đọc lại ro ro như còn bé, mà là nhớ và có thể gọi lại trong kí ức một cách sinh động, đầy thú vị!

Thật hay, tôi tìm thấy Sơ đồ tư duy trong Hội sách. Tôi thích màu của sách, cực kì đẹp và gợi cảm giác ngay khi xem. Và cả cách lập sơ đồ tư duy nữa, nó giúp não bạn phá vỡ mọi khuôn khổ, lại được tự do nguệch ngoạc nhưng dần sắp xếp ý tưởng cho rõ ràng, nhờ thế mà những ý nghĩ điên rồ thoát ra khỏi mọi giới hạn, giúp tôi đạt được kết quả cao hơn. 

Thỉnh thoảng tôi vẫn thích viết dông dài, nhấn nhá câu văn hơn, nhưng quả thật Sơ đồ tư duy là công cụ suy nghĩ tuyệt vời nhất mà tôi từng biết.
5
7800
2013-03-19 14:28:16
--------------------------
