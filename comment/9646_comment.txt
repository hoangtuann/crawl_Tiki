400712
9646
Mình đã có khá nhiều những quyển truyện tiếng anh như quyển này rồi nhưng càng đọc mình càng thấy hay. Lev Tolstoy thì chắc nhiều bạn đi học cũng phải nhớ rồi, có "Chiến tranh và hoà bình" rất hay và nổi tiếng. Truyện ngắn của ông không thể nào chê được, mang đầy tính nhân văn, ý nghĩa và tất nhiên mua quyển sách này thì mục đích là để học tiếng anh nên cuốn sách này rất hữu dụng. Mình cũng không biết nhận xét thế nào nhưng hãy mua những quyển sách như thế này, đặc biệt là bộ Happy Reader í. Hay và hữu ích lắm luôn.
5
1217475
2016-03-19 19:56:42
--------------------------
311852
9646
Mình không để ý đến chuyện "Tolstoy" là ai? Mình chủ yếu chỉ muốn rèn luyện thêm tiếng anh thôi. Đặc biệt là cách phát âm chuẩn, cách nhấn nhá, giọng đọc sao cho truyền cảm là được hihi :). Giọng đọc trong CD thì quá là tuyệt rồi nhưng có điều mình muốn sao chép vào laptop để dành lỡ có hư hay mất CD thì còn có cái để nghe lại mà không thể chép CD đươc ( chắc là do có bản quyền :( ). Vì vậy mỗi lần nghe xong là mình phải cất cho kĩ càng. 
4
675686
2015-09-20 11:37:03
--------------------------
224220
9646
Ấm áp tình người và đầy tính nhân văn. Đã từng theo dõi bước chân của cậu bé Nguyễn Ngọc Ký thì càng không thể bỏ qua cuốn sách khi Ký trở thành cử nhân, lớn lên với những bài học sâu sắc hơn, đi xa gia đình để lên thành phố, rời xa những con đường quê để đến những nẻo đường đông đúc hơn, khó khăn hơn. Thế nhưng nghị lực và sức mạnh cũng như những tấm lòng tốt đẹp chưa bao giờ ngưng rọi sáng trong suốt hành trình ấy. Đọc sách mình rất xúc động và đã nhiều lần rơi nước mắt. Hi vọng một ngày nào đó có thể đọc cuốn “Tôi đi dạy” nữa
4
162924
2015-07-08 12:00:41
--------------------------
160664
9646
Lev Nikolayevich Tolstoy một nhà văn Nga không những nổi tiếng với Chiến tranh và Hòa bình mà còn nổi tiếng với những câu chuyện viết cho thiếu nhi. Trong cuốn sách vừa luyện nghe học tiếng Anh này ta lần lượt sẽ gặp anh thợ đóng giày Simon nghèo khó nhưng tốt bụng hay chàng ngốc Ivan ngây thơ với tấm lòng biết sẻ chia nên luôn luôn khiến chàng vượt qua mọi thử thách. Cuối cùng thì chàng ngốc Ivan ngây thơ, nhân hậu đã trở thành người đứng đầu trong vương quốc của chính mình....Giọng kể truyền cảm, lôi cuốn khiến người nghe dễ tiếp nhận thông tin. Cho trẻ đọc sách và nghe đĩa cũng là một cách luyện tập tiếng Anh vô cùng hiệu quả qua những câu chuyện hấp dẫn như thế này
5
534227
2015-02-25 10:14:16
--------------------------
