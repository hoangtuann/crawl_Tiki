460362
10811
Nhắc đến Ohenri, người ta nghĩ ngay đến Chiếc lá cuối cùng, một câu chuyện chất chứa tình người và ngợi ca những điều giản dị nhất từ cuộc sống xung quanh ấy lại làm nên những điều kì diệu. Đọc những tác phẩm của nhà văn Ohenri, chúng ta lại chất chứa những tình cảm yêu thương, đồng cảm cho số phận của mỗi nhân vật. 
Cuốn sách là tập hợp những truyện ngắn hay và cảm động mà nhà văn đã làm rung động con tim của bao tâm hồn lãng mạn như chúng ta. 
Mình rất thích cuốn sách này cũng như yêu chính tác giả của nó vậy. 
Cảm ơn Tiki, mong Tiki có nhiều sách hay hơn nữa nhé!
5
791063
2016-06-26 23:17:13
--------------------------
390768
10811
Mình đã đọc bản tiếng anh nên thất vọng bản dịch thuật cực kỳ. Nhìn vào mấy trang đầu là thấy lỗi chính tả đầy rẫy luôn. Có nhiều chỗ phiên âm tên tiếng anh sang tiếng Việt đọc bực bội vô cùng, làm cho cậu chuyện chẳng có liên kết gì cả. Câu chữ cũng lủng củng, dài dòng đọc chẳng hiểu gì hết.
Thiết kế cái bìa mà nhìn tưởng mua nhầm cuốn Chiếc lá cuối cùng.
Nội dung mình đọc trong bản tiếng Anh khá dễ hiểu nhưng sang bản tiếng Việt thì nó trở thành một mớ hỗn độn, không liên kết và làm người đọc mất hứng vô cùng.
Hy vọng khi tái bản thì biên tập kĩ hơn và thiết kế lại cái bìa.
1
280007
2016-03-04 13:51:09
--------------------------
359601
10811
Truyện ngắn của O.Henry có thể xem là những tác phẩm khó nuốt không chỉ bởi sự khó hiểu mà còn ở nhưng triết lý sâu xa của tác giả. Bởi thế nếu chỉ đọc một lần mà không đọc lại và đọc kĩ, không đặt hồn mình vào trang sách thì khó có thể hiểu hết được. Và đặc biệt là bản dịch phải thật sự sát với nguyên tác và phải có sự uyển chuyển trong từng câu chữ để tránh dài dòng lê thê mà khó hiểu. Và thực sự bản dịch này đã khiến mình và có lẽ là nhiều bạn đọc khác thất vọng
4
556997
2015-12-27 14:38:32
--------------------------
282513
10811
Thật sự mình cảm thấy rất thất vọng sau khi mua cuốn này! Về hình thức thì ổn, tạm chấp nhận, in dễ đọc. Nhưng nội dung thì... Tác giả O-Henry không chú trọng câu chữ mượt mà, cái hay nằm ở nội dung câu chuyện. Thế mà bản dịch lại quá rối rắm, dịch theo ngôn ngữ kiểu Tây, có nhiều câu "bởi", "được làm...", rồi lặp từ,... làm cho câu chuyện đọc vào cứ thấy khó chịu, không mượt. Có chỗ còn không hiểu gì cả! Với cả mình không phiên âm tên nước ngoài ra tiếng Việt, làm như vậy không hay. Mong NXB chú trọng và rút kinh nghiệm!
3
539652
2015-08-29 10:38:03
--------------------------
244821
10811
Truyện ngắn của O.Henry không chú trọng vào những con chữ mượt mà, những lời văn bay bổng, mà tất cả chỉ là sự sáng tạo táo  bạo trong cốt truyện với những tình tiết đời thường mang lại những cái kết bất ngờ. Nội dung truyện thì hay khỏi bàn rồi. Nhưng cái mình thất vọng chính là dịch thuật! Dịch thuật tuy thống nhất và không có lỗi chính tả nhưng, theo quan điểm cá nhân, mình không thích tác giả sử dụng phiên âm tiếng Việt cho những cái tên ngoại quốc như trong truyện. Phần dịch nội dung truyện còn đôi chỗ khó hiểu, chưa làm bộc lên nét O.Henry. Thiết kế bìa không đẹp và quá khách quan vì O.Henry không chỉ viết mỗi  truyện "Chiếc lá cuối cùng" không đâu.
2
100155
2015-07-28 14:56:40
--------------------------
202769
10811
Đọc văn học nước ngoài mình không hy vọng lắm ở phần dịch thuật vì thật sự nếu dịch giỏi đến đâu cũng không truyền đạt được nguyên văn tác giả muốn nói. Do đó mình rất chú tâm đến nội dung. Truyện ngắn của O. Henry đặc sắc nhất ở phần xây dựng tình huống truyện, rất nhiều bất ngờ và khá lôi cuốn. Theo mình cảm thấy thì nhiều đoạn kết ông thường viết theo lối lơ lửng, vừa dứt lại vừa không dứt (không biết miêu tả như thế nào nữa) khiến người đọc suy ngẫm nhiều sau khi khép lại một câu chuyện.
5
593346
2015-05-30 14:42:08
--------------------------
196833
10811
Những cuốn sách của O.henri thực sự rất dễ hiểu, không khó đọc như những nhà văn nhiều thế kỉ trước đây (Jane Austen, Charlet Dicken...) Điều khiến những sáng tác của nhà văn này trở nên nổi tiếng đó là những câu chuyện ngắn nhưng giàu ý nghĩa sâu sắc. Không quá màu mè, không hoa lá trong ngôn từ nhưng những câu chuyện của ông lại rất được yêu mến. Đặc biệt là tác phẩm chiếc lá cuối cùng rất nổi tiếng và quen thuộc với hầu hết chúng ta khi nó đã được trích vào học trong chương trình ngữ văn trung học cơ sở. Mình thấy quyển sách này có nhược điểm là bìa chưa được đẹp lắm.
4
614265
2015-05-16 15:40:44
--------------------------
162253
10811
Mình đã đọc nhiều tác phẩm truyện ngắn của O.Henry và cái ấn tượng đầu tiên của mình về chúng là ngôn từ vô cùng dễ hiểu. Những mẩu chuyện tuy ngắn mà chứa đựng những giá trị cao cả. Những số phận con người, tình yêu thương, lòng vị tha, sự trắc ẩn, bao dung... Cho dù chúng ta chỉ đọc những câu chuyện đó một lần trong đời nhưng giá trị thông điệp sâu sắc, những bài học đắt giá mà nó gửi gắm tới bạn đọc cho tới sau này ta vẫn thật khó có thể quên được
5
540359
2015-03-01 20:40:47
--------------------------
137872
10811
Tôi đã mua quyển truyện này sau khi học bài chiếc là cuối cùng năm lớp 9, cô giáo đã giới thiệu cho chúng tối quyển truyện này. Cốt truyện rất hay, vẽ lên bối cảnh xã hôi nước Mĩ thế kỉ XIV, tôi rất thích các câu truyện trong tác phẩm, nhưng có lẽ quyển truyện chưa đủ đắt để có phần dịch hay hơn thì phải, có lẽ vì giá cả chưa cao, bởi lẽ phần dịch khá là không ổn, cứng nhắc trong khi câu chuyện lại đầy tình cảm như vậy. Nhưng dù sao tôi cũng thích tác phẩm, và phâm phục tài năng của tác giả O. Henry, các câu chuyện với nhiều nỗi bi kịch quả thực là cảm động, khiến tôi xót xa cho hoàn cảnh của biết bao người dân nghèo. đồng thời rất yêu mến và khâm phục nghị lực sống của họ, cốt truyện đã thể hiện được tư tưởng nhân văn của tác giả, cho ta thấy giá trị hiện thực và giá trị nhân đâọ của từng câu chuyện
4
412050
2014-11-29 09:58:22
--------------------------
137871
10811
Tôi đã mua quyển truyện này sau khi học bài chiếc là cuối cùng năm lớp 9, cô giáo đã giới thiệu cho chúng tối quyển truyện này. Cốt truyện rất hay, vẽ lên bối cảnh xã hôi nước Mĩ thế kỉ XIV, tôi rất thích các câu truyện trong tác phẩm, nhưng có lẽ quyển truyện chưa đủ đắt để có phần dịch hay hơn thì phải, có lẽ vì giá cả chưa cao, bởi lẽ phần dịch khá là không ổn, cứng nhắc trong khi câu chuyện lại đầy tình cảm như vậy. Nhưng dù sao tôi cũng thích tác phẩm, và phâm phục tài năng của tác giả O. Henry, các câu chuyện với nhiều nỗi bi kịch quả thực là cảm động, khiến tôi xót xa cho hoàn cảnh của biết bao người dân nghèo. đồng thời rất yêu mến và khâm phục nghị lực sống của họ, cốt truyện đã thể hiện được tư tưởng nhân văn của tác giả, cho ta thấy giá trị hiện thực và giá trị nhân đâọ của từng câu chuyện
4
412050
2014-11-29 09:58:13
--------------------------
137305
10811
Có một số điểm khá ức chế khi đọc truyện này, đặc biệt là về phần dịch thuật. Truyện dịch có vẻ khá khô cứng, đặc biệt ghét cái kiểu phiên âm tên tiếng anh sang tiếng việt của dịch giả, khiến người đọc khá ức chế khi theo dõi nội dung truyện. Chất lượng giấy tầm trung, không gây được ấn tượng. Tuy nhiên, mình rất thích bìa truyện, thực sự rất đẹp và gợi cảm xúc, là nguyên nhân chính thúc đẩy mình mua cuốn truyện này. Nội dung truyện hay, rất nhân văn, ý nghĩa và những cái kết bất ngờ. Tuy vài truyện hơi khó hiểu, yêu cầu sự nghiền ngẫm, suy nghĩ của đọc giả. Trừ một số điểm gây ức chế về dịch thuật, truyện ngắn của O.Henry rất đáng đọc. Nhìn chung là tạm ổn.
3
372541
2014-11-25 20:30:26
--------------------------
125023
10811
Truyện ngắn của O. Henry luôn có những nét rất nhân văn, ý nghĩa, và đặc biệt là kết thúc bất ngờ - tôi chưa bao giờ đoán đúng cái kết của một truyện nào cả. Và không phải lúc nào người đọc cũng có thể hiểu hết ý nghĩa của câu chuyện ngay lập tức, bạn phải lật giở và nghiền ngẫm nó lại lần nữa mới hiểu rõ được ý đồ của tác giả. Với ngôn từ gần gũi và có chút gì đó trào phúng, những tác phẩm của O. Henry bao phủ nhiều mảng màu khác nhau của cuộc sống, từ tình bạn, tình yêu đôi lứa, tình cảm vợ chồng cho đến sự vất vả khổ cực của những người dân nghèo nơi thành thị. 
Song với những ấn bản đã xuất bản ở Việt Nam, tôi chưa thấy hài lòng với bản dịch ở đâu cả. Bản dịch này cũng vậy. Không kể đến những lỗi nhỏ nhặt của cuốn sách, điểm trừ lớn nhất là văn phong khô cứng của dịch giả. Phần là do ngôn từ của O. Henry cũng khó dịch, phần khác có lẽ vì dịch giả không đủ mạnh dạn để dịch thoát ý hơn. Điểm cộng của cuốn sách là bìa đẹp, gợi lên nỗi lòng buồn man mác trong "Chiếc lá cuối cùng."
4
387413
2014-09-10 19:01:18
--------------------------
104442
10811
Cũng như nhiều bạn khác, tôi biết đến O.Henry lần đầu tiên qua truyện ngắn Chiếc lá cuối cùng trong sách ngữ văn. Thoạt tiên rất khó hiểu, khó nuốt, và cảm giác bị đánh lừa bởi lối văn khác lạ của ông. Nhưng mãi đến khi đọc phần kết của câu chuyện, thì chỉ có hai từ thôi, tuyệt vời, cái kết quá tuyệt vời, cách viết hấp dẫn ngay đến những dòng cuối cùng của câu chuyện.
Với ấn tượng đó, tôi đã mua ngay khi thấy cuốn sách này trên kệ sách và những truyện ngắn như Chị em bạn vàng, Cánh cửa màu lục, Cây xương rồng, Căn phòng đầy đủ tiện nghi, Khi ta yêu...lại tiếp tục là những chiếc lá cuối cùng nữa, mang đến cho tôi sự bất ngờ, bí mật luôn được giữ kín cho đến chi tiết cuối cùng. Mỗi truyện ngắn đều để lại cho tôi một bài học ý nghĩa, dù it dù nhiều cũng đã làm thay đổi rất nhiều cách suy nghĩ xưa củ của tôi.
Tuy thế cũng có những truyện ngắn khó hiểu, tôi phải đọc đi đọc lại tới 6 -7 lần. Nhưng với tôi, đó cũng là cái hay mà người đọc xứng đáng được thưởng thứ!
4
257144
2014-01-13 09:57:19
--------------------------
88552
10811
Truyện ngắn của O.Henry có thể xem là những tác phẩm khó nuốt không chỉ bởi sự khó hiểu mà còn ở nhưng triết lý sâu xa của tác giả. Bởi thế nếu chỉ đọc một lần mà không đọc lại và đọc kĩ, không đặt hồn mình vào trang sách thì khó có thể hiểu hết được. Và đặc biệt là bản dịch phải thật sự sát với nguyên tác và phải có sự uyển chuyển trong từng câu chữ để tránh dài dòng lê thê mà khó hiểu. Và thực sự bản dịch này đã khiến mình và có lẽ là nhiều bạn đọc khác thất vọng:

- thứ nhất là lỗi đánh máy, lỗi chính tả đầy rẫy, hầu như trang nào cũng phải có vài lỗi.
-thứ 2 là cách phiên âm tên tiếng anh sang tiếng việt khiến mình hết sức khó chịu vì khó nhớ khó đọc và nhiều khi là rất buồn cười
- cuối cùng là phần dịch thuật. nhiều chỗ dịch không đầu không đuôi, chỗ thì lại dài dòng lê thê mà đọc vô lại không hiểu gì cả, phải đọc đi đọc lại 2 3 lần để đoán coi thật ra bản gốc ở đoạn đó tác giả viết cái gì. Thực sự khó hiểu.

Mong rằng ở những tác phẩm sau nhà xuất bản sẽ hạn chế được những khuyết điểm này!
2
107839
2013-07-27 00:46:14
--------------------------
77237
10811
Tôi tình cờ đến với tập truyện này bởi tác phẩm "Chiếc lá cuối cùng" nổi tiếng trên toàn thế giới của ông. Tác phẩm của ông thật hay mà đầy ý nghĩa, với những cái kết bất ngờ làm rung động hàng triệu con tim.
Chắc các bạn không ai không biết đến cái chiếc lá vàng úa, già cỗi đã đem đến linh hồn cho 1 cô gái đang gần như tuyệt vọng trong chính cuộc sống của mình. Để rồi từ đó, tình yêu và niềm hy vọng từ 1 lão hoạ sĩ già đã được đã được dâng cao và bùng lên rực rỡ, dù lão đã ra đi mãi mãi nhưng chính niềm tin và sự hy sinh của mình đã giúp cô gái ấy tìm được mục đích sống cho mình...
Đến với tập truyện này, chúng ta không chỉ đến với những câu chuyện đầy cảm xúc như vậy mà còn hiểu thêm được về cuộc sống muôn màu, về những mảnh đời đa dạng, những nút thắt, những sự thật không khỏi khiến ta chạnh lòng..., như câu chuyện về 1 chàng trai đến 1 phòng trọ nhỏ, rồi tình cờ hay tình yêu cháy bỏng của mình khiến anh phát hiện có những dấu tích của người con gái mà anh yêu thương nhất đã từng ở đây, nhưng cô ấy đã không còn, quá đau khổ anh từ biệt cõi đời nơi chính căn phòng mà cô gái xinh đẹp ấy đã ra đi...

4
46758
2013-05-26 18:58:33
--------------------------
76420
10811
Đọc truyện ngắn O Henry, ta thấy những tác phẩm của ông đặc sắc từ nội dung cho đến nghệ thuật. Ở đây tôi xin đề cập đến mảng nghệ thuật trong những sáng tác của ông. Có thể nói, trong rất nhiều đặc điểm nổi bật của nghệ thuật truyện ngắn O Henry như phong cách cổ điển hay là những cái kết cấu độc đáo thì ngôn từ nghệ thuật cũng đóng vai trò quan trọng. Là bậc thầy trong việc sử dụng tiếng lóng, tiếng địa phương, khẩu ngữ...kèm theo việc sử dụng linh hoạt những kiểu ngôn ngữ này tạo nên một thứ ngôn từ nghệ thuật  hấp dẫn và tràn đầy sức sống đến lạ thường trong những truyện của ông. Những truyện ngắn của ông như " Chiếc lá cuối cùng", " Ái tình theo khẩu phần", " Xuân trên thực đơn" hay " Qùa tặng của các thầy pháp"... nhờ đó mà lôi cuốn độc giả đến tận trang sách cuối cùng!
5
72874
2013-05-22 20:55:56
--------------------------
72598
10811
về phần nội dung của các truyện ngắn thì không bàn , nhưng có 1 số điểm của cuốn truyện này làm tôi bức xúc quá phải lên đây gửi nhận xét (trước giờ đọc truyện cũng nhiều nhưng chưa bao giờ viết nhận xét vì lười , nhưng cuốn truyện này đã buộc tôi làm điều này ).

- thứ nhất là lỗi đánh máy , lỗi chính tả đầy rẫy , hầu như trang nào cũng phải có vài lỗi , không biết kiểm duyệt kiểu gì mà lại để 1 đống lỗi như vậy , hay là không có kiểm cũng k biết chừng.
- kế đến là cái kiểu phiên âm tên riêng ra tiếng việt , đọc kiểu này làm tôi cảm thấy rất khó nhớ tên nhân vật , có nhiều tên phiên âm đọc muốn méo cả mỏ , nhiều tên chả biết fải đọc làm sao luôn.
- cuối cùng là phần dịch thuật . nhiều chỗ dịch không đầu không đuôi , chỗ thì lại dài dòng lê thê mà đọc vô lại không hiểu gì cả , fải đọc đi đọc lại 2 3 lần để đoán coi thật ra bản gốc ở đoạn đó tác giả viết cái gì , hên xui có khi đoán được , có khi dẹp luôn .

thật lòng mà nói những điểm kể trên đã làm cho cuốn sách mất đi độ hay chắc cũng 70 - 70% chứ không ít , vậy mà thật không hiểu sao những người ở trên lại khen nức nở không thấy cảm thấy khó chịu 1 tí ti nào . 
chắc toàn là viết nhận xét để kiếm tiki xu chứ thật ra chẳng có đọc lấy 1 trang .
1
12751
2013-05-03 20:08:32
--------------------------
69265
10811
Cuộc đời đầy thăng trầm và sóng gió đã tạo nên một O.Henry dày dạn và bản lĩnh. Ông thể hiện những điều đó trong mỗi truyện ngắn của mình. Ông gửi cả vào trong mỗi tác phẩm một góc trong tâm hồn mình, trái tim mình. Những truyện của ông thật nhẹ nhàng, sâu lắng, đôi khi thật giản dị và chân thành, nhưng nó vẫn có sức lôi cuốn mãnh liệt với người đọc bởi những ý nghĩa nhân văn cao đẹp. Có lẽ chẳng ai có thể quên câu chuyện về chiếc lá cuối cùng được vẽ trên bức tường, một chiếc lá minh chứng cho tình yêu thương giữa người với người, một chiếc là cứu sống cả một tâm hồn đau buồn và ủ rũ. Và khi đọc tập truyện ngắn này, bạn sẽ nhận ra, trong mỗi câu chuyện của O.Henry, đều có "một chiếc lá" như vậy!
5
109067
2013-04-15 15:38:53
--------------------------
62289
10811
Tôi biết đến O.Henry đầu tiên qua truyện ngắn: Chiếc lá cuối cùng. Và từ đó, tôi có thêm một thần tượng văn học mới. O.Henry là con người của nghệ thuật, ông lao động sáng tạo không biết mệt mỏi và mang đến cho đời biết bao tác phẩm văn chương đáng giá. 
Ở Chiếc lá cuối cùng, tôi như thấy cụ Bermen mang dáng dấp của tác giả. Cho đến khi chết, ông vẫn sáng tạo và cứu giúp người khác bằng nghệ thuật chân chính của mình. 
O.Henry viết truyện ngắn cho những lớp người dưới đáy xã hội, cho thấy ở ông sự nhân văn, sâu sắc và một tấm lòng nhân đạo. 
5
54418
2013-03-08 10:04:34
--------------------------
45726
10811
Mình không phải là người yêu thích thể loại truyện ngắn. Nhưng với những câu chuyện của O.Henry thì là một ngoại lệ. Nhà văn có cuộc đời đầy sóng gió này là một con người rất tài năng, những câu chuyện của ông thực sự đặc biêt, có kết cấu độc đáo, mang nhiều ý nghĩa rất sâu sắc. Tình yêu thương, sự vị tha, sự sẻ chia, tất cả được O.Henry dùng làm chất liệu viết nên những bản tình ca về cuộc sống, về đời người. Những truyện của ông lấp lánh, sắc sảo, mang một nỗi buồn nhẹ nhàng mà ngọt ngào, với những nhân vật gần gũi, những hành trình mới mẻ về tình yêu, những kết thúc đầy cảm xúc và bất ngờ. Khi đọc cả tập truyện ngắn, điều còn lại cho mỗi độc giả là những tình cảm và thông điệp quý báu, đầy tính nhân văn.
Mình thực sự xúc động khi đọc các tp của O.Henry!
5
20073
2012-11-10 09:30:19
--------------------------
18199
10811
Tôi thích đọc truyện ngắn của O.Henry. Truyện ngắn của ông cốt truyện không quá cầu kỳ, không mang những tầm tư tưởng lớn lao, vĩ đại nhưng trong mỗi truyện ngắn lại luôn chan chứa một niềm tin lớn lao vào con người, vào cuộc sống với cái nhìn vui vẻ, yêu đời của ông trước những thăng trầm của số phận con người, đặc biệt là những con người nghèo khổ, thất thế, bất hạnh. 
Tôi đặc biệt thú vị với truyện ngắn “Món quà của các đạo sĩ” khi hai vợ chồng nghèo nhân ngày lễ giáng sinh đã bán đi tất cả những gì gọi là quý giá nhất, có giá trị nhất của mình để có tiền mua một món quà nhỏ cho người kia. Người chồng đã bán đi chiếc đồng hồ vàng quý giá để mua chiếc lược đồi mồi nạm ngọc tặng cho vợ, bởi vợ anh có một mái tóc dài mượt mà như suối thiên thanh, chiếc lược này mà cài lên mái tóc đó chắc chắn sẽ vô cùng tuyệt đẹp. Còn người vợ, cô đã lựa chọn mua chiếc dây đeo đồng hồ bằng bạch kim cho người chồng của mình, cô biết rằng nó sẽ rất thích hợp với chiếc đồng hồ của anh, anh có thể đàng hoàng xem giờ trước bất kỳ ai, vì thế cô đã bán đi mái tóc dài của mình. Sự thực quả là quá trớ trêu đối với hai con người “ngốc nghếch” đó. Câu chuyện thoạt đầu ta nghe có vẻ hơi buồn cười nhưng nó lại khiến cho trái tim chúng ta rưng rưng thương cảm cho hai con người đáng thương đó, và ta cùng thầm mỉm cười ngưỡng mộ cho tình yêu, sự hi sinh hết mình cho nhau của hai con người bé nhỏ đó.
Hay như câu chuyện “Tên cớm và bản thánh ca” vì muốn tránh cái rét cắt da cắt thịt, không muốn ngủ bờ ngủ bụi giữa mùa đông buốt giá, hắn đã nghĩ ra đủ mọi cách ăn trộm, ăn cắp, quỵt tiền, phá hoại của công, ghẹo “gái”, trêu ngươi cảnh sát,… để mong được bắt vào tù, ít nhất cũng được ăn no, ngủ ấm trong tù, không phải lang bạt ngoài đường giữa tiết trời lạnh giá của mùa đông. Thế nhưng ước nguyện của hắn đâu có dễ dàng mà thành, hắn càng nghênh ngang làm những trò “chướng tai gai mắt” trước mặt cảnh sát thì người ta lại chỉ nghĩ rằng hắn bị điên, dỗi việc mà thôi. Thế rồi trong lúc hắn đang nghĩ kế tiếp theo để có cơ hội vào tù thì hắn nghe được những giai điệu du dương của một bản thánh ca văng vẳng từ một ngôi nhà thờ cổ kính. Trong chốc lát trái tim hắn rung động, sự xúc động mãnh liệt đó đã thôi thúc hắn, kéo hắn ra khỏi vũng bùn tuyệt vọng, làm sống lại những mơ ước thiện lương của hắn. Hắn mong muốn được làm lại cuộc đời. Thế nhưng bỗng dưng lúc đó có 1 bàn tay cảnh sát nắm chặt lấy hắn và rồi tòa án xử hắn 3 tháng tù ở nhà khám… Cuộc đời là vậy, chẳng phải lúc nào cũng theo như ý muốn của con người, nhưng tôi tin rằng đối với tên cớm đó 3 tháng tù sẽ là 3 tháng giúp hắn tránh cái rét mướt của mùa đông, để hắn có thể trả giá cho những gì trước đây hắn đã vi phạm, để rồi khi bước ra khỏi nhà khám hắn sẽ giữ bỏ được quá khứ không mấy tốt đẹp của mình để bắt đầu một cuộc sống lao động chân chính.
Từng truyện ngắn, từng nhân vật của O.Henry đều luôn thống thiết một tình thương yêu bao la, một tinh thần nhân đạo sâu sắc. Mỗi câu chuyện ông viết, mỗi nhân vật không phải là những hình tượng cao sang, đẹp đẽ, kì vĩ, ngược lại, đó lại là những con người ở tận cùng cái đáy của xã hội, đó là cô thư ký đánh máy, cô bán hàng tiền lương quá thấp nhưng tâm hồn giàu mơ ước và lãng mạn; những người lao động nghèo khổ trong khu chung cư tồi tàn; hay những gã lang thang trong công viên, trên hè phố, đầu óc luôn luôn bận bịu với những mưu kế để tồn tại và cả những tên lừa đảo, bịp bợm đi trên phố như những con thú săn mồi. Tuy nhiên trong những con người xấu số, bất hạnh đó vẫn luôn giữ được một phần lương tri, một phần bản chất vốn dĩ là tốt đẹp của con người, vẫn lấp lánh ánh sáng của tình người. Có lẽ chính vì vậy mà suốt thế kỷ qua tên tuổi và tác phẩm của ông vẫn luôn tồn tại trong sự ưa thích và mến chuộng của người đọc khắp nơi trên thế giới.

5
16668
2012-01-27 20:28:30
--------------------------
6906
10811
truyện ngắn của O.Henry mang đậm tình người .Nó cho ta thấy được những tình cảmđược coi là xa hoa trong cuộc sống thị trường ngày nay .
riêng bản thân tôi có lẽ sẽ không bao giờ quên đươc câu chuyện mang tựa đề "chiếc lá " ,một câu chuyện để lại niềm xúc động sâu sắc trong tôi,dạy cho tôi rằng sống là cần biết cố gắng để vượt qua khó khăn ,không bao giờ được phép đầu hàng số phận .mong rằng các bạn sau khi đọc cuốn sách này sẽ biết trân trọng cuộc sống hơn

5
6200
2011-06-24 16:54:05
--------------------------
5545
10811
Truyện ngắn của O.Henry lúc nào cũng vậy, giọng văn nhẹ nhàng, dí dỏm, rất bình dị mà sâu sắc và luôn để lại cho người đọc ấn tượng bởi những cái kết bất ngờ.
Những trải nghiệm phong phú của O.Henry đã đem lại cho các câu chuyện của ông sự sinh động và nhiều màu sắc của XH Mỹ đương thời, với cuộc sống của rất nhiều các tầng lớp nhân dân lao động, thương lưu, trí thức.
Một tập truyện rất đáng đọc, không những để giúp tâm hồn phong phú hơn, mà còn cho người đọc thấy một bức tranh sinh động về xã hội và một giai đoạn lịch sử của nước Mỹ.
5
5768
2011-06-03 10:28:41
--------------------------
