357411
8553
Quyển sách này hay, bổ ích, hình ảnh sống động, bố cục cũng đẹp nữa.

Khi đặt mua, tôi cứ ngỡ là sách chỉ toàn chữ như các quyển sách "Vì sao" khác. Khi nhận sách, tôi khá bất ngờ. Hinh ảnh minh họa trong sách quá sống động và đẹp.

Sách rất phù hợp cho các cháu thiếu nhi ham hiểu biết, cũng phù hợp cho các bậc bố mẹ đọc để trả lời Một vạn câu hỏi vì sao của các con mình.

Một quyển sách đáng mua và đáng đọc.

Tôi sẽ tìm mua thêm cho đủ bộ 20 quyển của bộ sách này.
5
778630
2015-12-23 14:50:22
--------------------------
277591
8553
Cũng như các quyển sách cùng series Em Muốn Biết Vì Sao. Khổ sách và hình ảnh rất phù hợp cho các bé thiếu nhi. Nội dung thì rất thú vị và hữu ích. Cách dẫn dắt và so sánh trong sách cũng khá dí dỏm. "Em Muốn Biết Vì Sao: Lạc Đà Có Bướu" là một quyển sách phù hợp cho các bé từ 3 tuổi trở lên. Tranh ảnh và thông tin trong sách mang đến cho các bé kiến thức hữu ích về thế giới động vật và những giây phút vui chơi, thư giãn cùng sách.
5
352823
2015-08-25 08:58:46
--------------------------
