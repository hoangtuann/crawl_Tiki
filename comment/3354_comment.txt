445500
3354
Thích cách tư duy của tác giả. Từ việc chia ra trò chơi hữu hạn và vô hạn mà mở ra cho người đọc cách ứng xử trong cuộc sống thường nhật. Ví dụ hôn nhân là một trò chơi hữu hạn, trong đó vợ - chồng được thừa nhận bởi những người chứng kiến. Tình yêu không hôn nhân là một trò chơi vô hạn, người chơi trong đó có thể được thay thế bất kì lúc nào và không giới hạn người chơi :) . Lựa chọn coi đó là hữu hạn hay vô hạn là cách nhìn của mỗi người nhưng tất cả đều có quy luật, một khi hiểu rõ quy luật đó thì trong lòng sẽ không vướng mắc bất kì phiền muộn nào nữa!

p.s: mua cuốn này từ một hiệu sách thông thường và đã đọc được hơn 2/3 :)
5
141648
2016-06-10 12:55:22
--------------------------
374967
3354
Nếu bạn không phải là 1 người yêu triết học, yêu lý luận tư duy, yêu những gì trừu tượng được thể hiện qua ngôn từ thì tôi khuyên bạn không nên mua quyển sách này. Vì quyển sách tuy nhỏ, mỏng thôi, nhưng quả là rất khó hiểu. Có thể phải đọc vài lần và suy ngẫm thì mới may ra hiểu chút ít. Tác giả phân tích 2 thuộc tính của 2 loại trò chơi hữu hạn mọi người chơi để thắng, và trò chơi vô hạn mọi người chơi để chơi tiếp. Từ đó tác giả liên kết với các vấn đề khác trong cuộc sống.
4
465332
2016-01-27 15:38:49
--------------------------
325795
3354
Ban đầu trước khi mua quyển sách, với tựa đề cộng với lời giới thiệu tác phẩm, quyển sách htaajt sự gây cho mình sự tò mò, mình cũng không thật sự nghĩ nó khó nuốt đến thế. Tuy nhiên đây quả thực là quyển sách khó đọc, đọc qua một lần, có thể bạn sẽ nghĩ mình đã hiểu nhưng bạn có thể vẫn chưa hiểu được những gì sâu xa hơn ở cuốn sách cho tới khi bạn đọc lại lần nữa. Dù có thể sách hay, nhưng vì chưa thấu hiểu được toàn bộ nên tạm đánh giá ba sao.
3
746610
2015-10-24 11:14:34
--------------------------
