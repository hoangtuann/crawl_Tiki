404115
5212
Tập 48 siêu siêu hài hước, có đoạn mình đọc cười chảy nước mắt, như đoạn Shin học làm vệ sĩ sau đó chỉ vì tham ăn mà vô tình đỡ cho tiểu thư Ai khỏi bị nút sâm panh bắn vào đầu. Đó chỉ là một trong vô vàn những tình tiết khôi hài không đỡ nổi. Đến tập này độc giả cảm nhận được rằng Shin đã lớn, có vẻ trưởng thành hơn, vì Shin có em rồi mà, tuy nhiên độ dễ thương và tính hay gây rắc rối vẫn không hề giảm. Truyện hay và đặc sắc lắm, nhưng mẩu truyện cuối về em gái Shin, việc cô bé vô tư nói đến cái ấy của anh trai mình thấy khá là phản cảm, nó không phù hợp để trẻ em nói và trẻ em đọc. Dù vậy, do tập truyện hay nên tôi vẫn dành cho 5 sao.
5
386342
2016-03-24 18:41:51
--------------------------
369136
5212
Mình không thường xuyên đọc Shin-cậu bé bút chì, chỉ thỉnh thoảng thấy đứa bạn có nên mượn đọc thôi. Mới đọc được một số tập nhưng cũng khá thích, cậu bé bút chì này cực kì dễ thương, mà truyện lại có nhiều tình huống hài hước,  rất phù hợp cho việc đọc giải trí. Mấy nhóc em họ của mình mê truyện này lắm, kể cũng phải thôi, bìa sách minh họa thì ngộ nghĩnh, truyện thì hay và vui nhộn. Chẳng qua là không có thời gian để đọc thôi chứ nếu có nhiều thời gian khéo mình cũng "nghiện" truyện này mất.
5
475058
2016-01-15 10:02:37
--------------------------
365913
5212
Em trai mình thích đọc truyện tranh Shin - Cậu Bé Bút Chì cho nên mình hay tìm nhưng cuốn em chưa có để mua cho em. Điều làm mình ấn tượng đầu tiên chính là bìa truyện rất sinh động, đáng yêu, lại có nhân vật ''hiệp sĩ lợn'' tính tình cẩu thả, thấy khó khăn liền bỏ chạy. Cái điệu ngoáy mông '' vô đối'' , cùng những trò chơi khác người của Shin cũng khiến cho người đọc buồn cười. Truyện rất hay, không chỉ dành riêng cho trẻ con mà cả người lớn cũng rất yêu thích.
5
1096195
2016-01-08 23:07:39
--------------------------
348928
5212
Đã mua rất nhiều tập của Shin_Cậu bé bút chì nhưng khi thấy Tiki bán nên mua ủng hộ tiki :)))
Sách truyện khỏi chê, in màu sống động, nội dung truyện hài hước của cậu bé Shin hiếu động, nghịch ngợm, hay pha trò, nhiều lúc cũng thấy bản thân trong đó, :))) Hài nhất là cái trò hay khoe mông của cu Shin. vô đối.. tuy có hài nhưng vẫn thể hiện tính tốt của cu Shin tốt bụng, ai cũng yêu mến, sách rất hay ngay cả ba mẹ mình cũng thích nữa.. thank Tiki vì giao hàng nhanh chóng 
5
783648
2015-12-07 11:40:40
--------------------------
324276
5212
Wow , tập truyện này không kém gì những tập truyện khác , cu vẫn hay bày trò , nghịch ngợm , và trò " khoe mông " của Shin , coi vậy chứ cu Shin cũng người lớn lắm nha , lúc những cảnh chia tay hay khúc hấp dẫn , hồi hộp , cu Shin tỏ ra thái độ rất là người lớn , dễ thương , làm cho người ta rất buồn cười và đến nổi phải " xúc động " . Lý do mà mình mua cuốn này là không chỉ như thế , thứ nhất là cái bìa rất dễ thương và đẹp , thứ hai là những nhà sách ở đây không có quyển này . Truyện này rất hay!!!
5
603156
2015-10-20 20:56:36
--------------------------
210459
5212
Khi tình cờ mua và đọc được được quyển truyện Shin - Cậu Bé Bút Chì tập 49 thì mình đã mua ngay quyển này. Tuy đọc lộn xộn nhưng mỗi quyển là một câu chuyện hoàn toàn khác nhau. Mình đã bắt đầu thích seri truyện này rồi đó, rất là hay. Biết bao nhiêu kỷ niệm hồi nhỏ, hình ảnh cậu bé trong câu chuyện cũng rất giống với mình hồi nhỏ, nghịch ngợm lắm. Cơ mà sản phẩm này hết hàng hơn một tuần rồi, đợi lâu quá, mình định mua dùm tụi bạn ở lớp mình nữa đấy
5
603704
2015-06-19 15:11:04
--------------------------
