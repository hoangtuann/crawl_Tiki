207571
4409
Đây là một cuốn sách hữu ích với rất nhiều đề thi thử được tập hợp từ các trường điểm, trường chuyên trên khắp cả nước. Mọi HS sắp chuẩn bị bước vào kì thi hoặc các GV đang dạy luyện thi đều có thể tham khảo.
- Ưu điểm: Mở đầu sách là phần tóm tắt các phương pháp giải bài tập, phần này giúp cho HS có cái nhìn lại một cách tổng quát kiến thức trước khi giải đề. Với mỗi đề thì đều có kèm theo hướng dẫn giải rất chi tiết cho từng câu, điều này càng nâng cao hiệu quả tự học của HS.
- Khuyết điểm: Cấu trúc của mỗi đề thi trong sách gồm có 60 câu theo chương trình phân ban, điều này chưa hợp lí cho thực tế hiện nay vì hiện nay cấu trúc đề của Bộ Giáo dục và Đào tạo chỉ có 50 câu và không phân ban.
Nói chung, đây vẫn là một cuốn sách có giá trị tham khảo cao cho HS và GV.
5
527557
2015-06-12 17:25:25
--------------------------
