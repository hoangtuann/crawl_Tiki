365696
6038
Bà Tám Tiền - mẹ Dần Béo cũng là một trong những nhân vật mà mình thích nhất nên truyện có bà Tám mình mới đọc đã thấy thú vị rồi. Hấp dẫn hơn nữa là truyện còn đề cập đến bao nhiêu món ăn thơm ngon, phong phú và độc đáo khiến mình vừa đọc vừa đói bụng. Và điều hấp dẫn thứ hai đó là truyện có cuộc chiến tranh giành, lần này là khách hàng của hai nhà hàng ở đất kinh kỳ, đúng thực vô cùng hài hước luôn. Chủ đề tập này nói về ẩm thực xứ kinh kỳ, qua đó chúng ta được làm quen với những món ăn rất tinh túy và mang hồn Việt, đọc liền thấy yêu thêm nền ẩm thực và văn hóa Việt Nam.
5
386342
2016-01-08 15:55:56
--------------------------
322582
6038
Truyện nói về cuộc chiến giữa nhà hàng ở huyện của bà Tám tiền, tên là "Hương 8" và nhà hàng của Tào Lao, tên là "Dzách Lầu". Cuộc chiến như thế nào thì các bạn đọc rồi biết, truyện ngắn mà nói ra thì hết hấp dẫn mất rồi. Đương nhiên là trong truyện vẫn có sợ góp mặt của Tí Sửu Dần Mẹo, 4 bạn nhỏ này cũng đã giúp bà Tám biết bao là việc đấy. Kết thúc rất vui nhộn. Phía sau truyện có phần "Đo chỉ số IQ", với mình thì nó chẳng hấp dẫn gì cả :))
4
367488
2015-10-16 16:51:18
--------------------------
