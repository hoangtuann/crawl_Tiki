210688
5897
Đây là một quyển sách lịch sử không hề khô khan mà trái lại, khi đọc tôi thấy rất hứng thú. Bởi lối kể chuyện sử thêm vào một ít hư cấu về cuộc đời Phan Đình Phùng, người lãnh đạo cuộc khởi nghĩa Hương Khê trong phong trào Cần Vương. Đọc sách này tôi thấy giống đọc một quyển tiểu thuyết giả sử nhiều hơn là một sách sử thuần túy. Bởi tác giả kể thêm vài giai thoại về những nhân vật trong cuộc khởi nghĩa thời đó như anh hùng Cao Thắng, tài ba nhưng yểu mệnh. Và tôi lại thấy bùi ngùi luyến tiếc khi cụ Phan Đình Phùng sau cùng mộng lớn không thành, bị lâm bệnh và chết giữa rừng. Tóm lại, quyển sách này tuy là sách xưa tái bản lại nhưng đọc thật hấp dẫn và đọng lại trong tôi rất nhiều cảm xúc.
4
277587
2015-06-19 18:44:31
--------------------------
