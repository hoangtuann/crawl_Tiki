413218
7135
Khi Mình đọc trích dẫn một câu trong cuốn sách này, nó làm mình thôi thúc phải mua ngay mà không chần chừ. Một cuốn sách hay cần có trong tủ sách. Những lúc cần suy nghĩ về những vấn đề khó giải quyết nên đọc những mẩu truyển nhỏ để suy ngẫm thêm, làm đẹp cuộc sống, và sống tích cực hơn. Sách thiết kế lại rất đẹp và nhẹ, dễ dàng cầm trên tay nên không thấy mỏi. Mình hy vọng những lớp trẻ bây giờ cũng đọc những cuốn này bên cạch những sách ngôn tình bình thường.
4
313417
2016-04-09 10:27:21
--------------------------
403863
7135
Mình đã đọc quyển sách này từ lâu lắm rồi, bây giờ sách được tái bản với chất lượng giấy tốt hơn, mỏng nhẹ hơn, trình bày đẹp mắt hơn vì vậy mà mình mua lại để thay thế quyển sách cũ của mình. Nội dung cuốn sách kể về rất nhiều tấm gương trong học tập và cuộc sống mà khi đọc mình sẽ có ý chí và can đảm hơn khi đối mặt với những thử thách trong cuộc sống. Đây là một quyển sách rất đáng để đọc, mỗi ngày đọc những câu chuyện nhỏ ở đây và suy ngẫm những bài học mà nó mang lại- rất thú vị.
4
715487
2016-03-24 12:47:37
--------------------------
403811
7135
Mỗi câu chuyện luôn để lại sau nó những bài học làm người đắt giá. Con người khi sinh ra ai cũng giống nhau nhưng để sống thật với chính mình thì lại rất khó với mỗi người.Vậy sự can đảm tồn tại trong cái thế giới này để làm gì ? Đó là để khẳng định mình , để vượt lên trên nỗi sợ hãi thường trực trong bản thân , để vươn tới mục đích ước mơ của mình...Quyển sách này đã đem lại cho tôi những suy ngẫm về cuộc đời mình , nó đã nhắc nhở tôi rằng sự can đảm đến từ đâu và tôi cần nó đến mức nào. Rất hay , rất ý nghĩa. 
5
408074
2016-03-24 11:42:16
--------------------------
203790
7135
Tôi đã suy ngẫm thật nhiều đằng sau mỗi câu chuyện. Và, có câu chuyện khiến tôi cố gắng kiềm chế cảm xúc đang dâng lên trong lòng. Câu chuyện cuối cùng làm cho tôi bung ra tất cả. Tôi  - tại sao sao lại giống cậu bé đó như thế!
Tôi bây giờ đã can đảm hơn rất nhiều. Nhưng, sự can đảm mà tôi có được ấy đã phải đánh đổi rất lớn: sự ra đi của ba tôi.
Tôi hy vọng bạn đọc quyển sách này. Quyển sách không dạy bạn hãy can đảm và tử tế nhưng những câu chuyện ấy trong quyển sách đã làm nhiều điều hơn thế.
Và. . . cám ơn bạn đã hiểu ý của tôi. . .
5
131439
2015-06-02 12:29:21
--------------------------
