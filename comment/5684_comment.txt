298218
5684
Qua lời kể của Danny, thế giới bé nhỏ của hai cha con hiện ra rất ấm cúng. Và bới đây là chuyện viết, kể cho thiếu nhi nên giọng văn mang hơi hướng kể chuyện hoặc nói hơn là văn viết. Nó giản dị, nhẹ nhàng nhưng rất ý nghĩa và để lại nhiều bài học hay. Không chỉ cho các bé mà cho cả những người trẻ, những bậc làm cha làm mẹ. "Danny - Nhà vô địch thế giới" không chỉ mang đến những câu chuyện lý thú, bé nhỏ trong cuộc sống của Danny và những thứ xung quanh cậu. Mà sâu xa hơn nó truyền nhiều cảm hứng rất nhiều, đặc biệt là cách dạy con và những điều mà một đứa trẻ thực sự cần từ cha mẹ chúng. Một cuốn sách bé nhưng giàu ý nghĩa.
4
474670
2015-09-12 15:37:43
--------------------------
