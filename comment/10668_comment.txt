76116
10668
Truyện "hoàng tử ếch" là một câu truyện khá phổ biến ở các đất nước. Đặc biệt là cả ở phương Tây và Đông.
Truyện mang đến cho các bạn nhỏ rất nhiều hiểu biết. Và hiểu rõ hơn là khi nhìn một con người thì không chỉ nhìn vào bề ngoài của họ. Giống như hoàng tử trong truyện là một anh chàng đẹp trai, tài giỏi nhưng bị mắc phải một lời nguyền và mang một hình hài xấu xí. Nhưng rồi chàng cũng tìm  được cho mình một nàng công chúa dành cho chàng tình yêu chân thành không để để ý khi chàng đang mang một hình hài xấu xí. Và chính tình yêu và nụ hôn chân thành của nàng công chúa ấy đã hóa giải được lời nguyền ấy. Giúp hoàng tử trở về được với hình hài thật sự của mình,
Truyện rất hay, mang đến cho người đọc nhiều cảm xúc thật. Đây là một câu truyện tốt để dạy cho các bé biết không nhìn bề ngoài của một con người để phán xét về mọi thứ của người ấy.
5
118096
2013-05-21 12:46:27
--------------------------
