567997
5224
ncl hay hay cực kì, xem cả chục lần, đọc truyện trên web rồi cả của đứa bạn vẫn không thích bằng mua về rồi nghiền ngẫm. Nếu buồn thì cứ đem ra đọc xem như relaxing, conan đối với mình là best cứu mood luôn đó . Tập này thì best rồi
5
1948278
2017-04-09 09:00:55
--------------------------
455675
5224
Ran nhớ lại việc cô đã theo Shinichi và mẹ cậu ấy tới New York và giải được vụ án Quả Táo Vàng. Ấn tượng nhất là câu nói của Shinichi khi đối diện với tên ác quỷ đường phố trong tòa nhà bỏ hoang: "Tôi chẳng biết vì sao người ta lại giết lẫn nhau, nhưng, đâu cần một lý do chính đáng, để cứu một ai đó." 
Những tập mà Conan trở lại thành Shinichi không bị teo nhỏ thật là hấp dẫn và cuốn hút. Thích nét vẽ của Aoyama vẽ Shinichi học sinh cấp 3 hơn.
5
23847
2016-06-22 19:37:38
--------------------------
410069
5224
Mình vừa mượn cuốn sách tập 35 của chị mình xong.Cuốn sách này Shinichi lại xuất hiện cùng Ran ở Mỹ,nhưng chuyện đó đã rất lâu rồi.Vụ án xảy ra rất kỳ lạ,Shinichi lại phá vụ án một cách tài tình.Nhưng khi trở về,Ran làm rơi mất chiếc khăn tay mà cô đã được người nổi tiếng cho mượn vì xước tay(quên tên cô ấy rồi)Shinichi lên nhặt hộ trong một tòa nhà.Đúng lúc đó,cô đã gặp Akai Shuhuchi,thành viên của FBI.Các bạn phải đọc mới biết.Nhưng Ran không biết anh ta,cô tưởng đó là tên giết người hàng loạt nên đã rất sợ hãi,nhưng khi Akai hỏi cô có phải người Nhật không thì cô lại không sợ hãi nữa.
5
1172849
2016-04-03 14:21:09
--------------------------
401553
5224
Tập 35 là phần tiếp của câu truyện Ran-Shinichi đi Mỹ ở tập 21,,, Cái cách mà Shinichi bảo vệ Ran thật đáng ngưỡng mộ. Tuy vụ án ở Mỹ có phần bi thảm về tình yêu nhưng lại cũng cho chúng ta một cách nhìn mới về tình cảm giữa người với người. Sự nhí nhảnh láu cá của Yukiko cũng là một tác nhân gây thú vị, gần như đây là nhân vật luôn luôn được chào đón như đúng cái danh là đại minh tinh
      Sự xuất hiện của Akai Shuichi với mái tóc dài cũng tạo ra sự hấp dẫn cho tập này,,,,,,,,
5
445062
2016-03-20 22:09:54
--------------------------
377946
5224
 Trong vụ án khu phố tàu Yokohama, Ran bị ngất. Dần dần, Ran nhớ lại những kí ức khi còn cùng với Shinichi, tại chuyến đi tới New York, Ran đã bắt gặp một vụ án khiến cô day dứt mãi không thôi:
-Thank you, sweet angel, you helped me to make it.-Rose-hung thủ vụ án tại New York.
-Đâu cần lí do chứ? Tôi chẳng biết vì sao người ta lại giết lẫn nhau...nhưng đậu cần có lí do chính đáng để cứu một ai đó?-Shinichi 
Ran lại ngất đi, ngày mai, tên quỷ đường phố đã chết, cảnh sát cho rằng hắn tự tử, nhưng với những li luận của Shinichi, lẽ nào lại là người mà Ran gặp trước căn nhà hoang đã giết hắn?
Trở lại hiện tại, những tin đồn về khu tập thể bị ma ám đã được khép lại công với vụ giết người ở công viên gần đó xảy ra đã lâu dưới những suy luận logic và tài tình của Conan-Shinichi trong lốt thám tử ngủ gật Kogoro.
Mitsuhiko bị lạc trong rừng khi còn Numabuchi ở trong? Càng đọc càng thấy hay !
5
97553
2016-02-03 21:14:26
--------------------------
362118
5224
Conan tập 35 khiến tôi vô cùng thích thú. Tôi thích nhất cảnh Shinichi ôm Ran qua khung cửa ô tô trên đường New York. Một vụ án nơi xứ người xảy ra trước mặt Shinichi và cậu đã lao vào phá án hết sức tuyệt vời! Phải nói đây là tập truyện rất hay vì có sự xuất hiện của Shinichi và cả Akai - những nhân vật mà độc giả rất yêu mến. Tại vụ án này, những diễn viên nổi tiếng tầm cỡ Hollywood xuất hiện thật tuyệt! Trong đó có cả người phụ nữ bí ẩn dường như biết rõ mọi chuyện Sharon... thật tuyệt!
5
362041
2016-01-01 12:23:36
--------------------------
354893
5224
Vụ án căn nhà ma có hai tên cướp. Một tên bày mưu tính kế nhát ma khách nhà trọ để họ bỏ đi còn mình thì chăm sóc người bạn. Lẽ ra hắn không cần mệt nhọc vậy, chỉ cần giết tên kia đi rồi hưởng trọn số tiền cướp một mình vì có bà con gì với hắn đâu mà phải thế. Nhiều lần thấy tác giả dựng truyện rất là "thiên đường", cái gì cũng đẹp như mơ vậy. Xa rời thực tế quá có hay không ? Có lẽ dành cho thanh thiếu niên nên vẽ vời một chút cho các cô cậu ấy lạc quan về cuộc đời. 
5
535536
2015-12-18 21:00:08
--------------------------
351831
5224
 trong số những tập truyện conan, mình thích tập 35 nhất. Có lẽ bởi mình có cảm tình với Shinichi lớn, thêm vào đó là tập này có các vụ án bí ẩn mang màu sắc kì dị ma quái. Còn nhớ lần đầu mình đọc vào buổi tối, lúc đang ngồi một mình, nhiều đoạn cảm thấy khá sợ hãi, cứ như đọc truyện ma vậy. Đây cũng là tập đầu tiên Akai, nhân vật yêu thích của mình xuất hiện. Tạo hình của anh rất ngầu, lại còn mang thân phận là FBI, kết nhất là cái nón chụp đầu và biểu cảm lạnh lùng quyết đoán của Akai
5
30531
2015-12-12 23:31:23
--------------------------
285648
5224
Trong tập này nhân vật Shinichi lại xuất hiện! Mặc dù đó chỉ là ký ức của Ran về khoảng thời gian qua Mỹ cùng Shinichi nhưng vụ án đã xảy ra lúc đó cực kỳ cực kỳ hấp dẫn.Khả năng suy luận tài tình của Shinichi tiếp tục được toả sáng. Ran đã gặp Shuichi Akai, ở thời điểm này vẫn chưa biết được thân phận thật sự của người này, chỉ biết anh ta có mái tóc đen dài và một gương mặt lạnh như tiền (nhưng Ran đã nghĩ rằng Shuu là người tốt vì cô đã thoáng thấy được dòng chữ đó) Nữ diễn viên nổi tiếng Sharon và Vermouth rốt cuộc có quan hệ như thế nào? Càng đọc càng hấp dẫn!
5
471112
2015-09-01 01:27:43
--------------------------
196612
5224
Mình đến với Conan cũng là tình cờ. Vì quá chán hết truyện đọc nên bữa ấy mình mới mượn nhỏ bạn ngồi trước mình về nhà đọc. Và đến khi đọc xong thì ghiền luôn tới giờ. Lúc ấy mình học lớp 3, kí ức về Conan tập 35 không bao giờ phai nhạt trong tâm trí. Và nó cũng trở thành một trong những tập truyện trong bộ Conan mà mình thích nhất. Trong tập 35 này thì mình thích nhất là vụ án tại Mĩ được Ran nhớ lại. Ran cùng Shinichi được đi xem kịch tại Mĩ và gặp phải một vụ án. Nhờ trí thông minh và khả năng suy luận của mình, Shinichi đã giúp mẹ thể hiện trước mọi người. Có 3 điều mình thích nhất trong vụ án này là Shinichi và Ran tới Mĩ và gặp mẹ của Shin; sự xuất hiện của Sharon tức Vermouth trong bọn áo đen và sự gặp gỡ của Ran và Shuichi Akai.
5
45656
2015-05-16 09:51:12
--------------------------
196559
5224
Mình không thích lần tái bản 2014 này, tiền mắc hơn, giấy in không tốt nữa mà nhiều khi cầm đọc đen cả tay luôn. Tập này Akai xuất hiện với một mái tóc dài, trông bảnh thiệt, hình như đây là lần đầu tiên Ran gặp Akai thì phải. Hóa ra ma không đáng sợ như mình nghĩ, chỉ là do con người tạo dựng nên thôi. Nói chung là tập này cũng không có gì đặc biệt, những vụ án cũng có thể đoán được nhưng phải cho một điểm cộng là nét vẽ của tác giả rất đẹp mắt, thể hiện từng nét mặt rất sống động, nhất là Akai, một điều nữa là lâu lâu Shinichi lộ diện thật là tuyệt.
4
13723
2015-05-16 07:43:06
--------------------------
191138
5224
Câu chuyện mở đầu là vụ án được hồi tưởng trong tâm trí của Ran về một lần được đi xem kịch cùng với Shin và mẹ anh tại Mĩ. Vụ giết người ngay trên sân khấu lớn, trước mặt bao nhiêu con người. Shin cũng chưa thực sự thành thạo trong việc phá án. Nhưng bằng trí thông minh, khả năng qua sát và sự suy luận nhanh nhạy, sắc bén của mình, Shin đã cho mẹ mình một cơ hội thể hiện tài năng phá án trước đám đông. Điều mình gây ấn tượng nhất chính là câu nói của Shin sau khi hung thủ tìm ra lại chính là người Ran cứu trước đó: "Tôi chẳng biết vỉ sao người ta lại giết lẫn nhau nhưng đâu cần một lí do chính đáng để cứu một ai đó". Mình rất thích tập truyện này.
5
466113
2015-05-01 08:26:52
--------------------------
