443393
3422
Mình thấy cuốn sách rất ý nghĩa, có tính giáo dục, rất phù hợp với trẻ em. Sách hình khá đẹp , bắt mắt,  gồm các mẫu chuyện ngắn gọn nhưng chứa đầy ý nghĩa, tình thương, bài học làm người, giá cũng hợp lí, tuy nhiên khổ sách khó để bọc bằng bao vở bình thường. Trừ điểm đó ra thì đều tốt cả.. Sách rất phù hợp cho trẻ nhỏ trong giai đoạn phát triển về thể chất lẫn tâm hồn vì sách có nhiều bài học làm người rất hay, nhưng người lớn cũng có thể đọc để hiểu được tâm hồn, tính cách trẻ em ngây thơ, trong sáng, đẹp đẽ đến nhường nào và cũng ó những bài học người lớn cũng cần suy ngẫm.
5
948285
2016-06-06 20:24:59
--------------------------
