377169
5132
Tập 1 nhạt nhòa, tập 2 cũng chẳng khá hơn. Cái sự dây dưa không dứt của Dạ Lạc với các nhân vật nữ thật sự rất khó chịu. Ngoài U Minh sứ giả và Dạ Ly với Doãn Kiếm, tôi thực sự không thích bất kỳ nhân vật nào khác. Dạ Lạc thì tàm tạm. Tôi kỳ vọng nhiều hơn vào nhân vật Phong Linh nhưng cuối cùng chỉ là hời hợt. Tập 2 này tôi phải nhảy cóc rất nhiều, không thể đọc liền mạch. Đông nhân vật, ít đất diễn, khiến tác phẩm có chút gượng ép. Tác giả nên tập trung nhiều hơn vào Dạ Lạc thay vì chia ra cho đầy đủ các phe phái khác, do sự thực là tác giả chưa đạt đến trình độ đó. Câu chuyện khá nhạt nhòa. Chỉ tiếc cho một cốt truyện hay.
3
926764
2016-02-01 19:55:19
--------------------------
375251
5132
Thiết kế màu sắc và hình vẽ đẹp , ấn tượng , tạo ra những không gian u ám , ảm đạm và đầy bí ẩn đưa cho người đọc thấy được sự thay đổi hết sức ngẫu nhiên và có chủ ý từ trước , đan xen giữa hiện thực và mối liên hệ giữa hai người bạn quỷ quái với con người , tranh đấu quyền lợi là mục đích tối cao , cần thiết đến với 2 người như bản chất nó luôn biểu hiện ra đúng ý của tác giả nên thấy điều làm ta nghĩ đến bước đầu tiên chính là sự giúp sức từ các thế lực siêu nhiên .
4
402468
2016-01-28 01:21:50
--------------------------
305445
5132
Dạo này hay đào sách cũ sách đọc nửa vời dang dở lên đọc cho hết để còn tiễn các em ra đi và quyển 2 Trăng Đêm của Tích Lan Thành là một trong số nhưng lựa chọn của mình. Phải nói, Quyển 1 mình thích bao nhiêu thì quyển này mình thất vọng bấy nhiêu. Mình mong chờ thật nhiều ở nhân vật Phong Linh nhưng rốt cuộc tất cả cũng chỉ là nửa vời. Thậm chí đoạn mà cho là có vẻ đau thương nhất là lúc Phong Linh chết trong vòng tay của Doãn Kiếm cũng không gợi được xúc động trong mình. Truyện thì nhiều nhân vật mà không nhân vật nào đi tới đâu, nhân vật cũng giống như nội dung truyện cứ ỡm ờ, ngay cả kết thúc cũng ờ ỡm. Cá nhân tôi cảm thấy Dạ Lạc khá vô trách nhiệm với Dạ Ly nên đã cho điểm trừ rồi lại càng muốn trừ thêm.
3
180185
2015-09-16 22:59:09
--------------------------
157985
5132
Trăng Đêm tạo được ấn tượng cho mình từ khâu thiết kế bìa đến nội dung truyện
Phần bìa được thiết kế rất đẹp mắt, mang màu sắc ghê rợn đầy u ám. Tạo nên một cảm giác bí hiểm mà mình chờ đợi để tìm hiểu :D
Phần nội dung thì mình khó có thể chê được. Cốt truyện được xây dựng có nội dung, có sự đan xen giữa hiện thực và những yếu tố kỳ ảo như yêu ma - con người - tà thần. Cuộc đấu tranh giữa yêu ma và con người với sự giúp đỡ của tà thần thật hấp dẫn và đầy màu sắc cuốn hút. Rất thích những thể loại truyện theo thể loại này :D
Cách dẫn dắt truyện tùy theo từng phân đoạn. Có chậm rãi, có dồn dập và những tình tiết kịch tính cũng được đẩy lên cao trào :)
Theo mình thấy thì 2 tập truyện Trăng Đêm rất đáng để đọc!
4
303773
2015-02-10 23:04:52
--------------------------
150014
5132
Thực sự thì khi cầm cuốn này đọc từ trang đầu tiên là lập tức không muốn bỏ xuống rồi, độc đáo nhất ở chỗ là tác giả đã xây dựng lên một thế giới giữa con người và yêu ma, tà thần sống để bảo vệ loài của mình. Tập 1 để lại cho bản thân mình nhiều ấn tượng về cốt truyện lẫn tính cách của từng nhân vật khi đọc xong mình mong là ra tập 2 thật nhanh nhưng phải công nhận rằng tập 1 để lại cho mình ấn tượng thì tập 2 lại để cho mình cảm xúc khó quên về Dạ Lạc lẫn Dạ Ly, nói chung là được
5
533277
2015-01-15 14:48:05
--------------------------
146941
5132
Tậpj1 mình đã thấy nó hay rồi nào ngờ sang tập 2 nó lại ngày càng hấp dẫn hơn . Mạch truyện được tác giả đẩy lên cao , các mâu thuẫn giữa các nhân vật thì không ngừng xuất hiện . Các yếu tố mới được xây dựng nhằm làm tăng độ hấp dẫn cho câu chuyện . Kết thúc phải nói là mình rất vừa lòng . Tuy không phải ai cũng được hạnh phúc nhưng dù sao cũng khá viên mãn . Người tốt thì sẽ được hưởng bình yêu kẻ ác sẽ mãi mãi chìm ngập trong oán hận và dằn vặt không thể nào dứt ra được . Sang tập 2 phải nói một điều rằng về chất lượng thì chả khả quan hơn tập 1 là mấy . Có cảm giác như nhà xuất bản làm qua loa đại khái đánh lừa độc giả vậy .
4
337423
2015-01-06 08:42:21
--------------------------
