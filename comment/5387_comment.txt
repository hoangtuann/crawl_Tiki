408736
5387
Mình mua cuốn Bé tập tô màu này cùng với bộ sáp màu cho đứa cháu 3 tuổi của mình và bé rất thích. Ngày nào bé cũng đòi lấy ra để tô màu và hào hứng khoe với mọi người tác phẩm của mình. Các tranh trong sách rất đẹp, ngộ nghĩnh, lại thuộc nhiều chủ đề nên bé không chán. Có phần tranh màu để cho bé có thể nhìn theo để tô, nhưng bé nhà mình không chịu theo tranh mẫu mà lúc nào cũng tự tô mầu theo ý mình. Mình nghĩ đây cũng là cách hay để kích thích trí sáng tạo của bé khi bé chưa tự mình vẽ được.
5
647767
2016-04-01 10:01:28
--------------------------
