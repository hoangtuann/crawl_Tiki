445690
11221
Tập cuối cùng của bộ truyện Doraemon dài, không ấn tượng như lúc ban đầu, tập cuối lại tiếp bước hai tập kết cuố là 22 và 23 giảm nhiệt, nói thật là vẫn biết Số Một bị mất trí nhớ nên mới quên Nobita nhưng cái cách suy nghĩ, thái độ và hành động của cậu thật khiến mình không tài nào chấp nhận nổi. Tập này cũng thương cho Doraemon, vì quá yêu mà đã phạm phải sai lầm, tên Hắc Miu cũng khiến mình khó chịu vô cùng. Nhưng khép lại tất cả, tình bạn vẫn mãi chiến thắng.
4
1420970
2016-06-10 18:46:08
--------------------------
370966
11221
Trong số truyện đôremon tập dài thì mình thích nhất là tập này mình rất thích ichi đặc biệt là Ichi lúc nhỏ nhìn vô cùng dễ thương và tinh nghịch tình cảm giữa nobita và Ichi thật cảm động Ichi đã làm hẳn một cỗ máy thời gian để đến thăm nobita nhưng lại gặp sự cố may mà cuối cùng họ vẫn gặp được nhau mình buồn nhất là lúc nhóm đôrêmon chia tay với nhóm Ichi thật buồn mình không thích chia tay một chút nào. Truyện này thật sự rất hay và ý nghĩa nữa mình đọc nhiều lần rồi mà vẫn không thấy chán
5
1102669
2016-01-18 19:09:21
--------------------------
185260
11221
Đây thật sự là một câu chuyện cảm động về tình bạn chân thành, trong sáng. Xuyên suốt câu chuyện kể về tình bạn cao đẹp giữa Nôbita và chú chó" Số Một". Mình thật sự xúc động khi "Số một" nhận ra Nôbita. Dù cho mười năm, một trăm, một ngàn năm hay bao nhiêu năm đi chăng nữa thì tình bạn của họ vẫn mãi trường tồn, không bao giờ nhạt phai. Câu chuyện gieo vào lòng người đọc nhiều suy nghĩ về giá trị đích thực của tình bạn mà ta đã từng lãng quên. Truyện có chất lượng tốt, giấy khá cứng, khó rách, bền bỉ với thời gian.
5
558345
2015-04-19 09:40:22
--------------------------
182584
11221
Câu chuyện luôn làm mình xúc động mỗi khi đọc lại. Truyện Doraemon tập dài luôn nói về tình bạn những câu truyện này đã khẳng định tình cảm thieng lieng do. Dù qua 1000 năm nhưng tình bạn của Nobita và Ichi không hề thay đổi. Những câu hát mà Nobita dạy cho Ichi khi mà còn ở hiện tại như những câu hát kết nối tình bạn xuyên thời gian, tạo nên kỉ niệm giữa 2 người. Họ chia tay nhau trong niềm tin, niềm hi vọng về một ngày nào đó dù không gặp lại nhau, những con cháu họ vẫn có thể gặp được nhau và cùng kết nối 2 dân tộc...............
5
564406
2015-04-14 13:26:54
--------------------------
168489
11221
Chú chó số một do nobita cứu đã trở thành người bạn tốt của nobita. Đúng thật, nobita có một tấm lòng nhân hậu vô cùng, tuy cậu bé hậu đậu và vụng về, song, cậu vẫn rất tốt bụng.
Tình bạn giữa nobita và số một thật khiến người ta ngưỡng mộ, dù một bên là người, một bên là con vật. Nhưng giữa họ, còn hơn cả con người.
Nhóm bạn nobita nhờ sự giúp đỡ của doraemon đã tìm được số một và cứu chúng ra khỏi con cháu của mèo ướt sủng.
Thật là một tình bạn thiêng liêng.
5
482828
2015-03-16 19:40:09
--------------------------
167714
11221
Cuộc phiêu lưu đến vương quốc chó mèo là tập truyện dài cuối cùng, kết thúc 24 tập truyện dài Doraemon của tác giả Fujiko.F.Fujio. Có chút nuối tiếc vì là tập cuối cùng nên hình như tôi đã đọc hơi lâu thì phải. Tình bạn giữa Nobita và " Số Một" quả thực rất chân thật và cảm đọc. Trải qua biết bao nhiêu là thế kỉ họ gặp lại nhau và nhận ra nhau. Họ bắt tay nhau và chúc nhau hạnh phúc, Bao nhiêu vui buồn xảy ra những người bạn tốt như Nobita, Như Doraemon, Như " Số một" Như Xuka, như XeKo, như Chaien vẫn luôn bên nhau, sát cánh cùng nhau, hoạn nạn vẫn luôn có nhau.
5
186455
2015-03-15 11:50:24
--------------------------
159473
11221
Mỗi cuốn Doraemon truyện dài là một chuyến phiêu lưu thú vị của các người bạn nhỏ và chú mèo máy Doraemon, qua nhiều vùng đất khác nhau, nhiều thế giới khác nhau, gặp gỡ và giúp đỡ những người bạn khác nhau ...... Nobita ở vương quốc chó mèo có thể nói là tập cuối của series Doraemon truyện dài, hơi buồn và tiếc một chút vì rất thích đọc truyện dài của Doraemon bởi cuộc phiêu lưu dài hơn, hấp dẫn hơn và có tính nhân văn rất nhiều. Tình bạn của Nobita và chú chó Số Một - Ichi không khỏi khiến người khác phải ngưỡng mộ, xa cách 1000 năm nhưng hai người bạn này vẫn nhớ và giữ những kỷ niệm của nhau.
4
471112
2015-02-17 09:29:16
--------------------------
