457529
12115
Cuốn sách này là sự giác ngộ chơn tánh dành cho những ai mong muốn giác ngộ Phật tâm. Những điều viết trong sách tuy không mới nhưng đó là sự hạnh ngộ của những người mong muốn tìm về thiện căn trong con người bản thể của chính mình. Có Phật tâm ắt sẽ có Phật quả, bắt nguồn tự sự thấu hiểu quy luật luân hồi và vòng sinh tử. Sách có dung lượng nhỏ, cô đọng, súc tích dễ nắm bắt, dễ hiểu. Tuy nhiên, đối với những ai mới vừa tiếp cận, có thể sẽ khá mất nhiều thời gian để đọc và hiểu.
5
24486
2016-06-24 12:25:50
--------------------------
