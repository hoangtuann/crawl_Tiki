542481
6331
Thank vì giao hàng nhanh, sách in ấn tốt. Đọc chưa xong hết nhưng phải nói đây là 1 tác phẩm hay.
4
49020
2017-03-15 08:00:23
--------------------------
237826
6331
Những người nuôi giữ bồ câu quả không hổ là tiểu thuyết đáng kinh ngạc nhất của Alice Hoffman. Trên cái nền của lịch sử của Israel cổ đại, Alice Hoffman đã xây dựng một kiệt tác văn học, nơi trí tưởng tượng và những cảm xúc dữ dội của bà bùng nổ và thăng hoa hơn bao giờ hết. Dù ở đâu, dù thời đại nào, phụ nữ bao giờ cũng là những người chịu nhiều tổn thương nhất, yếu ớt nhất, bị khinh rẻ nhất. Nhưng cũng chính họ, ẩn sau tấm thân mềm mại yếu ớt lại là sức sống mãnh liệt đáng kinh ngạc. 
Cuốn tiểu thuyết cũng có thể coi là bài ca hùng tráng về loài người trong chiến tranh. Tôi rất ấn tượng về cái bìa của cuốn sách, tuy đơn giản nhưng lại rất giàu tính biểu tượng. 
5
167283
2015-07-22 19:57:37
--------------------------
223788
6331
Cuốn tiểu thuyết lịch sử hạng nhất, rất dày, tôi đọc bền bỉ trong vòng nửa tháng trời mới kết thúc câu chuyện, phần vì dày, dài, phần vì tiểu thuyết được viết vào thế kỉ trước nên không phải cứ đọc trôi tuồn tuột là hiểu vấn đề.
Quả thực, truyện rất khóc đọc, cốt truyện mới mẻ, không bị lẫn vào thế giới tiểu thuyết lịch sử chung chung. Biểu tượng chim bồ câu trắng gần như thành huyền thoại về sự hòa bình, tình yêu thương, niềm hi vọng vượt lên khỏi chiến tranh tàn khốc.
Một cuốn tiểu thuyết lịch sử suất sắc.
5
75153
2015-07-07 16:28:54
--------------------------
155318
6331
Đây là một trong những cuốn tiểu thuyết châu Âu được viết vào những năm 60 của thế kỉ trước. Vì nó phản ánh một cách trung thực về xã hội và cuộc sống con người một cách chân thành dù hơi khó đọc một chút. Ở đó, ta tìm được những giá trị của cuộc sống, để biết yêu thương, biết trân trọng những gì đẹp đẽ đang xảy ra xung quanh ta.Mình đã tìm được những điều đẹp đẽ ấy, như tình yêu trong những hoàn cảnh khốc liệt nhất và như những người nuôi giữ những sứ giả hòa bình .....
5
192934
2015-01-31 19:51:46
--------------------------
136633
6331
Phải nói là mình đã lưỡng lự khá nhiều trước khi chọn mua cuốn sách này. Qua những lời giới thiệu ban đầu thì có thể thấy đây là một thể loại khá mới mẻ với một chủ đề khó tiếp cận với độc giả ngày nay. Nhưng những suy nghĩ đó đã hoàn toàn bị đánh bay khi mình đọc và chìm đắm vào những trang sách tuyệt vời của "Những người nuôi giữ bồ câu". Một tác phẩm khá đồ sộ nhưng có sức mạnh như thôi miên khiến mình không thể đặt xuống giữa chừng. Bối cảnh của câu chuyện là ở thời cổ đại, khi xã hội lúc đó vẫn còn rất sơ khai, nhưng qua những lời kể đầy chân thực thì câu chuyện đã trở nên vô cùng gần gũi và tươi mới. Tác giả tập trung đặc tả sự mạnh mẽ của bốn người phụ nữ giữa lòng pháo đài cổ đại ở Israel: Yael - cô gái tóc đỏ dám đương đầu với những với những nguy hiểm để đến với tình yêu và trở thành một người mẹ, Revka - người đàn bà với cuộc đời đầy những đau thương và mất mát giờ một mình phải trống trọi tất cả, Aziza - cô gái với vẻ ngoài yếu đuối sẵn sàng giả trai để tham gia chiến trận thay cho em mình, và Shirah - một người bị cộng đồng của mình coi là phù thủy xấu xa phải trải qua biết bao đày đọa. Từng lời tự sự của mỗi nhân vật tràn đầy cảm xúc, diễn tả được sự xót xa, tủi hờn, và cả những phút giây sôi sục đầy kiên cường trong con người họ. Khi cộng đồng bị xâm chiếm và đứng trước nguy cơ diệt vong, họ đã trở thành những nữ anh hùng thực thụ. Với đoạn kết kịch tính và giàu chất bi tráng của sử thi, hình ảnh những người phụ nữ "nuôi giữ bồ câu" này lại càng được khắc họa đậm nét và sâu sắc hơn.
Hình ảnh những chú bồ câu trắng ở những tháp nuôi cũng mang ý nghĩa ẩn dụ cao đẹp về tự do, về tình yêu thương, về niềm hy vọng vượt lên trên sự ác liệt của chiến tranh tàn khốc.
"Những người nuôi giữ bồ câu" hoàn toàn là một tác phẩm đáng đọc, đáng nhớ, đầy sức mạnh, với hình tượng nhân vật được miêu tả xuất sắc, và một cốt truyện mới mẻ, giàu sức gợi, cùng những thông điệp nhân văn ý nghĩa.
5
109067
2014-11-22 15:48:00
--------------------------
