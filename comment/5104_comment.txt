428965
5104
- Cách viết và nội dung rất teen, điều này không bàn cãi. Tuy nhiên, có vẻ như teen quá đâm ra... ảo quá chăng? Ngay từ đầu, việc không cần biết tên thầy giáo là đã hơi ảo rồi. Teen bay giờ không chỉ biết tên thôi đâu, mà sẽ còn xin luôn cả tên Facebook của thầy nữa kìa. Tiếp đó, thầy đã có bạn gái rồi, vậy cớ gì lại bắn hint cho nữ chính nhiều thế? Dắt học sinh đi bar tới sáng mà không cần xin phép phụ huynh học sinh? Ba của nữ chính không bực mình khi một thầy giáo lại dắt con gái mình đi thâu đêm mà nói dối ông ư? Còn kết thúc, việc thầy gặp tai nạn có ý nghĩa gì? Chưa kể, ngôi xưng của truyện hơi bị nhập nhằng rắc rối. Ban đầu là "tôi", sau đó đổi thành "Ly Cún", "nó", sau đó lại "tôi",... Điều đó làm mạch cảm xúc của mình bị đứt đoạn. Mình bị phân vân giữa 2 và 3 sao, nhưng nghĩ tới ý tưởng và lối viết táo bạo của tác giả, cộng với chủ nhiệm của mình là một thầy giáo sử cute phô mai que và rất teen nên quyết định cho 3 sao :3.
3
411530
2016-05-12 10:04:42
--------------------------
426825
5104
Mình thích cách viết của tác giả quá đi mất,rất lôi cuốn dường như ổng rất thấu hiểu tâm lý của tụi con gái thì phải :3.Cũng thích nội dung truyện nữa,cách học Sử trong này rất độc và lạ,lại phù hợp với quan điểm của mềnh.Không hiểu sao tác giả lại có thể vẫn còn viết chân thực và gần gũi tới vậy.Làm mình đọc liền mạch luôn.Hình minh họa trong sách được in màu rất đẹp.Nhưng mình thấy tính cách của thầy giáo thì như tâm thần phân liệt ấy,không biết tính cách ảnh ra sao nữa và càng về sau truyện dường như chẳng đi đâu về đâu ấy.

4
616034
2016-05-07 19:54:52
--------------------------
388333
5104
Đây là lần đầu mình đọc sách của tác giả Lê Hoàng. Quyển sách này bìa ngoài rất dễ thương, phiên bản sách hơi lớn. Sách này nội dung nói về những tâm tư, suy nghĩ của Ly Cún- Ki Ki. Cô nàng xinh đẹp, thông minh, kiêu kì và được rất nhiều anh chàng mê.Một cô gái teen và coi bọn con trai trong lớp chả ra gì. Bỗng một ngày gặp được thầy giáo dạy môn Sử vừa cao ráo, đẹp trai và còn vừa rất teen teen. Cả hai đã nhiều lần đi chơi chung và trải qua nhiều điều thú vị. Trên lớp thầy dạy Sử cho Ki ki, còn khi lúc đi bơi thì Ki Ki lại là người dạy thầy cách bơi. Thầy là người đã thay đổi cách dạy môn Sử và khiến cho cả lớp 11A phải háo hức mong chờ khi có tiết Sử đến. Nhưng một ngày thầy đã rời xa và để lại cho bọn chúng nỗi buồn và tuyệt vọng vô cùng. Rồi Ly Cún và thầy đã gặp lại nhau thật bất ngờ. Đọc câu chuyện này sẽ khiến ta ngặc nhiên bởi bất ngờ này sang bất ngờ khác.
4
639171
2016-02-28 19:22:50
--------------------------
343885
5104
Đã đọc quyển này từ cuối năm 2014, và từ đó tới nay đã mua rất nhiều quyển để tặng rất nhiều người, và đa phần ai cũng thích. Một quyển sách đáng đọc và có rất nhiều tầng ý nghĩa, thích hợp cả với những ai thích thể loại tiểu thuyết nhẹ nhàng lãng mạn hay suy ngẫm. Lời đề tựa của nhà sử học Dương Trung Quốc "cá nhân tôi cảm ơn quyển sách này" thật sự kích thích sự tò mò của tôi. Làm sao mà một nhà sử học lại đi cám ơn một quyển sách thể loại này. Đọc hết mới hiểu tại sao lại như thế. Một quyển sách nhỏ, nhưng ẩn chứa nhiều điều đáng để suy ngẫm.
4
270700
2015-11-27 13:24:26
--------------------------
335949
5104
Thật sự không thể ngờ đây lại là một tác phẩm được viết bởi một tác giả nam, mà không phải là nam trẻ mà lại là một người đã qua cái tuổi nhất quỷ nhì ma thứ ba học trò này đã rất rất lâu rồi. Đọc xong lại tiếc cái thời nổi loạn ngày xưa. Một câu chuyện cho teen, mà theo mình đoán chủ yếu các teen girls, nhưng với các bạn gái đã mãn teen đọc cũng mê mẩn lắm đấy, cũng chỉ ươcs gì em có thể quay lại ngày xưa để có thể gặp được thầy mà thôi :-))). Tuy nhiên mình thấy kết thúc vẫn còn hơi hụt.
5
105502
2015-11-11 19:03:39
--------------------------
322323
5104
Đây là một cuốn sách rất thú vị.
Câu chuyện thật hài hước về Ly Cún, Mai Tồ, Chích Choè, thầy giáo,... Đạo diễn Lê Hoàng viết bằng ngòi bút hài hước pha chút dí dỏm :)) đọc truyện ta như thấy chính mình trong mỗi nhân vật vậy. Lứa tuổi học trò thật vui vẻ, hồn nhiên, vô tư. Tình yêu tuổi học trò cũng thật trong sáng, vui vẻ. Tôi thấy ấn tượng về cách thầy giáo teen dạy học trò, phải vừa học vừa thực hành, học không gò bí trong sách vở mới thực là học. Tôi cũng từng ước có những buổi học thầy trò cùng trò chuyện rôm rả sinh động như thế. Cũng ấn tượng với thầy hiệu trưởng tuy già nhưng không cổ hủ.
4
764379
2015-10-15 23:49:27
--------------------------
321828
5104
Đây là một trong nhiều bức tranh, mảng ghép gắn liền với thực tế xã hội được Lê Hoàng chuyển tải truyện và chia sẻ cùng mọi người. Sách không chỉ dành cho lứa tuổi teen mà dành hẳn cho những ai đã trải qua lửa tuổi này. Tác giả có nói chơi với bạn của mình là: mày ngu lắm mày mới không đọc truyện của tao viết đấy. Rất có thể người đọc sẽ phải hình dung lại lứa tuổi của mình và so ra trước sự thay đổi và khác biệt với lứa tuổi teen ngày nay,
Không đơn giản là truyện mà còn là những châm ngôn đây đó, vụn vặt một lần nữa cho thấy Lê Hoàng nổi lên như một ánh sao trong làng cầm viết.
5
390132
2015-10-14 22:53:05
--------------------------
321041
5104
Đây là cuốn sách khiến tôi cảm thấy thú vị nhất từ trước đến giờ. Tác giả Lê Hoàng thực sự rất tài năng và thành công khi lồng những baifhocj học lịch sử lí thú vào câu chuyện. Với tác giả cái gì, điều gì cũng có thể suy ra lịch sử, còn có cả những châm ngôn vô cùng hùng hồn của Ly Cún và đám bạn. Tất cả đã tạo nên cái hay, cái độc cho cuốn sách. Nó còn thể hiện những chiều sâu, những cung bậc cảm xúc về tình cảm của các nhân vật, những tình cảm được thể hiện rất teen.
4
570046
2015-10-12 23:29:15
--------------------------
300005
5104
Mình đã lựa chọn bỏ 100k để mua cuốn "Sao thầy không mãi teen teen" bản bìa cứng này. Khi nhận được sách mình hết sức hài lòng. Trước tiên là bìa sách quá dày dặn, lại còn nguyên màng nilong ở bên ngoài. Mới hết sức. Thật bất ngờ khi chú Lê Hoàng là tác giả của cuốn sách này. Hình thức sách lung linh, không có gì để chê cả, nội dung cũng hấp dẫn. Ngôn ngữ dí dỏm, gần gũi. Đúng như cái tựa đề, tràn ngập cuốn sách là một màu teen teen, hài hước và vô cùng sáng tạo. 
5
340213
2015-09-13 17:19:27
--------------------------
298392
5104
Tôi đã mua cuốn truyện Sao thầy không mãi teen teen quyển bìa cứng và thật sự nó không hề khiến tôi thất vọng. Vào ngày tôi nhận được cuốn sách hận không thể la hét lên hết ngày về vẻ ngoài bìa đầy lung linh kia, một màu xanh hi vọng bóng loáng nằm gọn được bao bì cẩ thận bằng plastic sợi dây màu đỏ giúp ta đánh dấu trang để khi rãnh có thể đọc, tặng postcard in lời bài hát, giấy và màu in hoàn mỹ, nội dung thì dễ thương khỏi nói tràn ngập thời tuổi thơ ngọt ngào. Đối với tôi truyện thực sự khá hoàn hảo và không thể bỏ sót được.
4
338898
2015-09-12 18:00:52
--------------------------
283025
5104
Đúng như tên gọi! Sao thầy không mãi teen teen quá là chất luôn nha... Mình đọc đi đọc lại mà không thấy chán mặc dù mua đã lâu :) Cái hôm thấy chú Lê Hoàng giới thiệu cứ thèm mãi, tới tận tháng sau mới mua, hiuhiu >< Nhưng mà ấn tượng là tranh vẽ đẹp lắm luôn với lại ngôn ngữ rất là dễ thương! Sách thơm ơi là thơm :'p Mà còn sai sót một tí trong mấy trang in nha Tiki ^^ Chẳng hạn như ở chỗ rượu thì bản in thành rượt @@ Hơi buồn tí =)) nhưng không sao
4
492342
2015-08-29 17:19:26
--------------------------
269142
5104
Qủa thật ... cuốn sách rất hay ... tặng bạn bè cũng dễ vì đây là chủ đề ai cũng đọc được, ai cũng từng yêu quý Thầy của mình ..... mình cũng từng làm thầy cho một trung tâm, nên được học viên tặng quyển sách này ... có một phần của mình trong đó !! ^^
Lời văn vui tươi, Teen cực !!! Làm cho mình như sống lại một thời ngịch ngợm á !!!!
Cám ơn anh ,  anh Lê Hoàng !!! Cám ơn tác phẩm "Sao thầy không mãi teen teen?" 
4
52084
2015-08-17 09:33:11
--------------------------
265696
5104
Khi mua quyển sách này mình mới biết chú Lê Hoàng viết sách. Mình rất thích tính cách của chú thông qua những chương trình mà mình đã theo dõi trên truyền hình. Thật sự bất ngờ ! Lời văn dí dỏm và đôi lúc là ngẩn ngơ giống như tâm hồn của tuổi teen. Hàm ý mà chú Lê Hoàng gửi vào trong quyển sách này thực tại vào cuộc sống hiện tại ngày nay. Cám ơn chú với bài viết mang ý nghĩa sâu sắc này. Tác phẩm trình bài cẩn thận, tinh tế. Mình nghĩ đây là đứa con tâm huyết của tác giả khi cho ra đời.
4
618799
2015-08-14 09:01:58
--------------------------
244002
5104
Tác phẩm "Sao thầy không mãi teen teen?" của Lê Hoàng thực sự rất tuyệt vời! Truyện có những suy nghĩ rất độc đáo, hay ho, những suy nghĩ mà tôi-một teen- cũng chưa từng nghĩ tới. Truyện được diễn tả bằng những ngôn từ dí dỏm, dễ thương. Bìa sách đẹp, nội dung hài hước và sáng tạo quá trời =))) Văn của Lê Hoàng có sự nổi loạn và rất táo bạo. Ước gì tôi được học thầy sử đó nhỉ? <3 Truyện này thật sự là vô cùng tuyệt vời! Mong rằng sẽ có thêm những quyển truyện táo bạo như thế này!
5
641393
2015-07-27 21:57:43
--------------------------
201655
5104
Đọc từ đầu đến cuối, chợt nhận ra mình...chưa đúng teen :3
Ấn tượng đầu tiên, bìa đẹp quá trời quá đất ♥
Đọc lại lần hai sau vài tuần sửa đổi, mình thấy mình...teen hơn hẳn ♥
Táo bạo. Bất ngờ. Độc đáo. Và còn rất nhiều điều không thể diễn tả bằng ngôn ngữ. Mọi người thì mình không rõ, nhưng với mình thì nó quá tuyệt vời. 
Đọc xong cảm thấy tủi thân kinh khủng. Giá như...mình có ngưòi cha như cha của Ly Cún. Giá như...cô giáo dạy Sử của mình cũng giống nhân vật Thầy. Giá như...rất nhiều điều nữa.
Tuy nhiên vẫn rất thích, rất rất thích cuốn này ♥
5
547934
2015-05-27 22:26:02
--------------------------
201391
5104
Truyện cũng khá dí dỏm, dễ thương, chuyện trong lớp Ly Cún cùng thầy giáo dạy sử nhiều lúc làm mình cười rất thoải mái; giọng văn cũng đúng chất teen, có chút nhí nhố, có chút đáng yêu. Chỉ tiếc là nhiều tình huống bị cường điệu quá thành ra hơi lố, có cảm giác cố làm màu. Mình cực ghét việc tác giả đan xen mấy lời bình luận bên lề của bản thân vào, làm cảm xúc bị gãy hết, mình nghĩ lần sau tác giả nên hạn chế việc đan xen này vào, nó sẽ làm độc giả thấy khó chịu.
3
393748
2015-05-27 11:25:24
--------------------------
187765
5104
Từ bìa sách cho tới nội dung, tất cả đều rất sáng tạo, ngộ nghĩnh và hấp dẫn =))) Mình thật k ngờ một Lê Hoàng nghiêm túc, thường viết truyện cho lứa tuổi trung niên, nay lại cho ra đời một tác phẩm hóm hình như thế cho teen =))) Lời văn thì bá đạo, k thể chê vào đâu được. Suy nghĩ của nhân vật Ly Cún rất hài hước, dễ thương, rồi để mỗi chúng ta cũng thấy bản thân mình trong đó ^^ Truyện chúng ta một chuyến đi người dòng thời gian để trở về cái thời học trò mà ai cũng cho là trong sáng, hồn nhiên, đáng trân trọng nhất ♥ Thực sự rất đáng để đọc, để cảm nhận bằng trái tim ^^ Cảm ơn tác giả TEEN Lê Hoàng nhé ♥ XD
5
472746
2015-04-23 22:33:53
--------------------------
184135
5104
Mình chỉ biết đến Lê Hoàng với cương vị là một nhà sáng tác nhạc nhưng không ngờ chú lại viết ra một quyển sách teen đến như thế .ý tưởng phải nói là hoàn hảo đến từng chữ . thầy teen, trò cũng teen, rồi khiến cho người đọc cũng trở thành teen . chú đã khéo léo trong việc phản ánh đời sống showbiz việt trong thực tế và cũng mang cho ta nhiều kiến thức bổ ích nữa . quyển sách đã khiến cho ta nhận ra được tình yêu đầu đời của tuổi học trò hồn nhiên vui tươi nếu ta tìm được một nửa còn lại của mình . có thể nói tình yêu không hề phân biệt tuổi tác chỉ cần hai phía đón nhận nhau..........
5
311372
2015-04-17 11:13:40
--------------------------
176355
5104
Bìa sách rất đáng yêu, nhí nhảnh và gần gũi, giống như tiêu đề vậy, Sao Thầy Không Mãi Teen teen (với bọn em?) :)
Một quyển sách đúng chất teen nổi loạn, từ ngôn ngữ cho đến nội dung và hình thức rất táo bạo, quái quái và cũng... ngây thơ không kém. Nhiều tình huống dở khóc dở cười nhưng rất gần gũi với việc dạy và học ở trường lớp, có những môn khoai ơi là ngán cùng "anh" thầy giáo thì đẹp trai mà dễ tính không để đâu cho hết. Tình yêu đầy mơ mộng của cô bạn Ly Cún 17 tuổi cũng là điều mình rất thích, có đứa con gái mới lớn nào lại chống cự được sức hút của thầy này được cơ chứ.
Tóm lại, kết sách hai chữ thôi: tuyệt vời!
4
382812
2015-04-01 06:34:10
--------------------------
173576
5104
Trước khi đưa ra lời nhận xét, mình xin giới thiệu, mình là học sinh lớp 10 và chỉ trong 4 tháng nữa mình sẽ học cùng khối với Ly Cún! Đọc cuốn sách này, mình cảm nhận được sự cố gắng của một tác giả ở cái tuổi 60 viết cho tuổi teen như thế nào. Quả thật rất teen, rất "hợp thời". Từ ngôn từ, lời nói đến hành động đều toát lên sự ngây thơ nhưng đầy kiêu hãnh , nổi loạn nhưng cũng đầy nỗi sợ vì những ranh giới mà người lớn đã vạch ra cho những đứa trẻ mới lớn! Còn sự phù phiếm, hồi hộp, choáng váng? Bạn đọc đi rồi biết!^^!  Vì sao tức giận và  bất lực? Tức bạn sẽ tức nhà trường, ngành giáo dục. Nhưng giận , sẽ là giận chính bạn. LỐi tư duy của tác giả Lê Hoàn sẽ khiến cho bạn quay cuồng trong vòng tròn những điều phi lý mà từ lúc sinh ra chính là thứ bảo bọc bạn, nhưng sẽ là bức tường ngăn cản bạn....Đó là mình. Nhưng  bạn sẽ có cảm nhận khác! Mình là HỌC SINH! Mình căm thù môn VĂN vì nó luôn bắt mình phải phân tích giống tác giả SGK. BẠn sẽ càng hiểu điều này hơn sau khi đọc cuốn này!
4
278147
2015-03-26 14:24:36
--------------------------
147801
5104
Tớ đã mua quyển này ở tiệm sách. Không giảm giá. Lúc nãy lướt comment thì lại nhận ra: không tặng kèm. Có khi là người ta cố tình ăn chặn??? Nói chung rất bức bách. Nhưng mà truyện hay quá. Ngôn ngữ cứ như mới chui từ lớp học ra vậy, còn nội dung thì táo bạo cực kì: nữ sinh yêu thầy giáo. Có khi người lớn đọc được lại la bai bải cho coi. Nhưng mà tớ thông cảm cho họ, người lớn lúc nào cũng nặng nề tư tưởng lo âu suy tính, không "teen teen" được. Tớ mang quyển này khoe khắp lớp, kết cục là bị quần mấy vòng rồi vào tay thầy chủ nhiệm. May là thầy nhìn cái tựa, rồi cười, trả lại cho tớ. Thầy còn trẻ mà. Suýt nữa tớ đã nói "thầy cầm về đọc thử". Mà thấy không cần thiết, lại thôi. Thầy tớ teen chán. Tớ chỉ mong ước ai cũng như thầy. Còn nữa, tớ cũng mong tổ Sử mua 7 quyển này về để giáo viên nghiên cứu. Hơi lố nhỉ?
5
532207
2015-01-08 21:15:29
--------------------------
145787
5104
Mua cuốn sách đã lâu rồi mà hôm qua mới đến tay để đọc, Mấy bạn động nghiệp, Học sinh tranh nhau đọc. Ai cũng rút ra nhận xét: không chỉ học sinh mà thầy cô giáo cũng nên đọc, đặc biệt là các thầy cô dạy lịch sử, Tôi thích cuốn sách từ chất liệu giấy rất đẹp, cách trình bày và in ấn không thể tuyệt vời hơn, Ngôn ngữ trong cuốn sách đó đơn giản, lôi cuốn, dễ tiếp cận, nhiều tình huống lí thú, như chính trải nghiệm của tác giả vậy. Mỗi người đều bắt gặp hình ảnh của mình trong đó, như thân thương lắm. Đặc biệt là các bạn nữ.
Mỗi bậc làm cha mẹ, thầy cô cũng nên đọc để hiểu hơn về giới trẻ.
5
480417
2015-01-01 01:17:29
--------------------------
145517
5104
Mình được tặng cuốn sách này trong dịp sinh nhật. Thứ nhất thì cuốn sách khá bắt mắt, màu sắc và hình vẽ đều đẹp và sinh động. Mình cũng rất thích chất liệu giấy nữa. Mình thấy hay vì phần nhiều ở lối hành văn của tác giả, mình cảm nhận ở lối hành văn đó có chút gì đó giống mình nên mình thấy khá là thú vị, mộc mạc có, kiêu kì có, ... nhưng hơn hết đó là một cuốn sách về tuổi "áo trắng" ngây thơ, hồn nhiên và đáng yêu hết sức. Cuốn sách khiến tôi thấy buồn cười và phải suy ngẫm về những định nghĩa hết sức là thú vị. Nói chung thì tôi rất thích cuốn sách này.
4
296749
2014-12-30 20:42:05
--------------------------
145320
5104
Tò mò đặt sách từ Tiki, đọc 1 lèo trong 2 tiếng, thấy quyết định đặt sách là hoàn toàn tuyệt vời.
Khá lạ khi tác phẩm đề cập đến tình yêu kẹo ngọt giữa nữ sinh 17 và thầy giáo, hơn nữa lại là thầy giáo dạy Sử - môn học chán nhất trong thời còn mài quần trên ghế nhà trường. Điều lạ thứ 2, sách là lời tâm sự của nữ sinh 17 tuổi. Tâm lý, hành động của nữ sinh đều được thể hiện đúng "bản chất teen", nổi loạn, nhưng cũng rất ngây thơ, trong sáng. Mình như đang được bước vào lịch sử, lịch sử của Việt Nam, của dân tộc, và cũng của chính mình, của 1 thời học sinh yêu ghét trắng đen rõ ràng.
Thử cầm cuốn sách lên, đọc và tận hưởng, chắc chắn bạn sẽ thấy nhiều khía cạnh thú vị của cuộc sống :)
5
164946
2014-12-30 01:01:35
--------------------------
144598
5104
Mình là một người rất khó tính trong việc đọc sách, nhất là đối với tác phẩm của nhà văn trẻ Việt. Lần đầu tiên thấy cuốn sách trên tiki ;à bản bìa mềm mình đã thấy rất mắc cười vì tên sách khá là "quê quê". Nhưng mà nhỏ em nhờ mình mua nên mình đã đăng kí mua dùm con bé. Lúc sách về mình cũng không nghĩ là sẽ đọc nhưng cuối cùng cũng bị nhỏ em dụ dỗ. 
Thật sự câu văn viết vẫn hơi cứng, nếu mềm hơn như sự thanh mảnh của Cún thì mạch cảm xúc sẽ đi sâu hơn nữa. Tuy nhiên sự dí dỏm, tâm lí và nội dung đặc biệt và tư tưởng "đầu tiên trên trang sách" đã khiến mình bị cuốn theo Ly Cún. Và hơn hết, như Ly Cún mình đã hâm mộ và rất yêu quý thầy giáo. Mình tin rằng, nếu bạn đang và đã là học sinh, bạn sẽ tìm thấy cho mình những kỉ niệm lúc áo trắng tuyệt vời. Cuốn sách còn thay đổi cách nhìn nhận của chúng ta về giáo dục. Nếu bạn không phải học sinh mà là giáo viên thì càng nên đọc cuốn sách. Nó sẽ giúp bạn gần hơn với học trò.
5
215772
2014-12-27 12:28:59
--------------------------
143315
5104
Quyển sách là tâm sự của bạn Ly Cún, một bạn nữ sinh có vẻ hơi 'nổi loạn' với lắm trò nhưng cũng rất dễ thương, với những suy nghĩ về thầy giáo đẹp trai, phóng khoáng, rất hiểu ý học sinh. Tính cách của Ly Cún khá dễ thương, gần gũi và rất thật, đúng chất của một cô nữ sinh ở độ tuổi này. Còn thầy giáo 'đẹp trai như tài tử' có cách dạy học rất hay, mới lạ không đụng hàng.
Lối viết của Lê Hoàng rất dí dỏm, cách so sánh trong truyện vô cùng Việt Nam xD đọc mà mình cứ khúc khích cười với mấy câu như "đẹp trai như Ưng Hoàng Phúc" hay "đẹp đôi như Thủy Tiên với Công Vinh" ...

Quyển sách rất đáng tiền, có tính giải trí cao, dễ thương, minh họa bằng những trang màu rất đẹp, bookcard tặng kèm cũng đẹp luôn!!!
4
464558
2014-12-22 16:14:53
--------------------------
143175
5104
Thời gian trước, Lê Hoàng có về trường mình diễn thuyết đi kèm với việc giới thiệu cuốn sách "Sao thầy không mãi teen teen". Chú ấy chia sẻ cuốn sách được viết trong vòng 1 tháng rưỡi và chú viết chẳng phải bởi mục đích gì to lớn mà chỉ là "Tôi viết vì tôi cáu, tôi cáu bởi những bạn trẻ ngay nay không được biết những cái cần biết". Vì tò mò, mình đã tìm đến với cuốn sách. Nó đã đem lại cho mình khá nhiều bất ngờ và cảm thấy thích thú với một cuốn sách có văn phong không thể "Lê Hoàn" hơn nữa. Mình tự đặt ra câu hỏi : Sao chú có thể năm bắt tâm lý của những bạn nữ một cách tài tình đến thế? Tuy nhiên, điểm trừ duy nhất của cuốn sách chính là Lê Hoàn sử dụng lối viết mạnh, câu văn hay bị ngắt quãng nên có thể sẽ làm gián đoạc mạch cảm xúc của bạn đọc. Lời khuyên duy nhất của mình là hãy kiên nhẫn đọc cuốn sách này, bạn sẽ phát hiện ra nhiều điều bất ngờ...
4
432104
2014-12-22 00:16:24
--------------------------
