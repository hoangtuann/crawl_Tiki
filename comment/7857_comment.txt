515709
7857
Đây là tác giả viết truyện ngắn hay nhất tôi từng đọc. Một cuốn sách không thể bỏ qua đối với những người mê truyện ngắn!
5
168560
2017-01-26 10:56:41
--------------------------
478207
7857
Đôi lúc khiến ta suy ngẫm, đôi lúc khiến ta bất ngờ, đôi lúc khiến ta bàng hoàng, đôi lúc khiến ta chột dạ , lại đôi lúc khiến ta thở phào.
Bản dịch có nhiều lỗi sai cùng vài cách dùng từ không thích hợp, một ví dụ là:
Nhầm giới tính hai nhân vật chính của tác phẩm điện ảnh "Hiroshima tình yêu của tôi" đươc nhắc đến trong truyện. 
Tuy tình tiết truyện khá lôi cuốn nhưng cách dịch của bản phát hành này không khiến người đọc thấm được, Nó cứ khiên cưỡng một cách kì lạ.
 Nói chung là 1 tác phẩm hay, đáng đọc nhưng bản dịch chưa xứng tầm.
3
58720
2016-07-31 22:24:53
--------------------------
472746
7857
Đây là cuốn sách khó hiểu phải công nhận là thế, trước mình cũng có đọc tác phẩm Cuộc đời yêu dấu do Alice Munro sáng tác. Mình giờ vẫn thấy lơ mơ, mơ hồ về những gì mình đã đọc, tác phẩm này cũng thế, ngay từ đầu đọc Trốn chạy cũng đã thấy khó hiểu rồi! Flora biến mất liệu có phải gia đình của Carla sẽ mất đi cái gọi là hòa khí, sự yên bình trong gia đình, có phải vậy không? Mất nó chồng cô sẽ đối xử với cô như thế nào? Tại sao và liệu có phải rằng anh ta giết nó không? Mình phải đọc chầm chậm lại, cố gắng thấu hiểu, suy ngẫm cuốn sách này thôi!!
4
508336
2016-07-09 22:37:36
--------------------------
456366
7857
Chỉ mới đọc lần đầu, nên đôi khi cá nhân mình chưa hiểu rõ dụng ý của tác giả ở một số câu chuyện khi tạo ra tình huống cho các nhân vật. Điều mình thích nhất, là yếu tố bất ngờ, gây tò mò thích thú đến mức ngạc nhiên cho xuyên suốt cả câu chuyện. Câu chuyện mình nhớ nhất là về Robin, khi còn là con gái, sau một lần hết sức tình cờ gặp một con người con trai mà cô xem là định mệnh, cô và anh hẹn gặp nhau 1 năm sau đó. Để rồi cô trở về nhà với nỗi thất vọng, là anh không nhận ra cô. Và khi trở thành một quý bà tầm U60, hết sức tình cơ là Robin phát hiện người đàn ông mà đóng sập cửa trước mặt bà hôm ấy là anh em sinh đôi của người mà bà đã hẹn. Bà đã nghẹn lại, ấy thế nhưng, bà biết lý do, rằng ngay từ đầu ở buổi hẹn thứ 2, bà đã mặc sai chiếc váy xanh.
4
42088
2016-06-23 13:35:58
--------------------------
285965
7857
Đọc tác phẩm, tôi thấy được phần nào chính mình cũng như những người quanh tôi trong đó. Trong một số hoàn cảnh nào đó, sẽ có lúc con người ta muốn được trốn chạy, muốn bứt ra khỏi thực tại, thoát khỏi công việc nhàm chán, cuộc sống nhàm chán, cuộc hôn nhân đã không còn tình yêu hiện hữu ... Những trốn chạy không phải để trốn tránh thực tế, mà để một lần được nhìn lại mình, nhìn vào tận sâu trong lòng xem bản thân cần gì và muốn gì. Tôi nghĩ tác giả đã rất tuyệt vời khi mô tả nội tâm đầy mâu thuẫn của nhân vật !
4
24682
2015-09-01 12:23:10
--------------------------
274860
7857
Alice Munro là nhà văn người Canada đoạt giải Nobel văn học vào năm 2013 sau Mạc Ngôn của Trung Quốc. Bà mang phong cách văn học mới được thể hiện trong 7 tâp truyện ngắn của bà đã xuất bản. Người ta vẫn thường ví bà là Chekhov của nền văn học Canada. Trong tuyển tập truyện ngắn này đều hướng tới một câu chuyện chung là trốn chạy tuy nhiên thì các câu chuyện không bị rập khuôn mà có những chi tiết khác biệt nổi bật. Kết cấu các câu chuyện tuy không lôi cuốn nhưng những cuộc trốn chạy trong truyện trinh thám nhưng tâm lý nhân vật qua các cuộc trốn chạy đó đã được bộc lộ. Cuốn sách này cũng là một kiệt tác truyện ngắn sau các truyện ngắn của Chekhov và James Joyce.
5
623509
2015-08-22 12:40:42
--------------------------
271019
7857
Cuốn sách bao gồm những câu truyện ngắn, đều xoay quanh hai chữ "Trốn chạy". Mỗi nhân vật trong từng câu truyện đều có những hoàn cảnh khác nhau, cách sống khác nhau. Tuy vậy, điểm chung ở họ đều là muốn được "trốn chạy" khỏi bế tắc trước mắt, và họ đều nghĩ tới "trốn chạy" như một cách để tiếp tục cuộc sống.
Không có nhiều gay cấn, nhưng lối viết của tác giả đã mang lại cho mình rất nhiều cảm xúc mà không phải chỉ đọc một lần là thấm. Tuy cuốn sách có đôi chỗ diễn đạt hơi khó hiểu, nhưng đây là một cuốn sách tuyệt vời, rất đáng đọc!
4
48662
2015-08-18 19:59:38
--------------------------
243589
7857
Đây là một tập những câu chuyện ngắn riêng lẻ của tác giả. Mỗi câu chuyện đưa người đọc đến với những xúc cảm khác nhau của những cuộc trốn chạy nhưng không hề gây ra sự nhàm chán chút nào. Tác giả đã khai thác một cách triệt để vào thế giới nội tâm của người đọc. Không cần dài dòng và lãng mạn hay quỵ lụy như một cuốn tiểu thuyết nhưng lại khiến cho người đọc không khỏi thán phục trước ngòi bút kiệt xuất của tác giả đã được giải Nobel, quả thật rất xứng tầm!
5
112376
2015-07-27 15:43:54
--------------------------
233586
7857
Quyển sách là một tập hợp những câu truyện ngắn liên quan đến tên tác phẩm "Trốn chạy". Mỗi câu truyện là cái nhìn thú vị về người phụ nữ. Nội dung sách rất hay khi được viết bởi một bậc thầy về truyện ngắn đương đại đạt giải Nobel. Hình thức cũng rất được, chất lượng giấy in tốt. Chỉ có chút lỗi về dịch thuật khiến người đọc đôi lúc thấy khó chịu và phải khựng lại. Hi vọng những lần xuất bản sau sẽ tốt hơn. Sách thích hợp cho các bạn nam đọc để hiểu thêm về tâm lí phụ nữ vốn rất khó hiểu, phức tạp.
3
125876
2015-07-19 19:36:35
--------------------------
210432
7857
Mỗi người có một cuộc đời, có người đang hạnh phúc yên ấm thì bên cạnh đó cũng có người đang phải chịu đựng những sự đau khổ cùng cực, bế tắc tuyệt vọng mà phải trốn chạy khỏi thế giới họ đang sống. Một loạt những mảnh đời được khắc hoạ dưới ngòi bút của Alice, giúp ta hiểu thêm được vốn cuộc sống không chỉ toàn màu hồng.
Chất lượng sách khá tốt vì là của Nhã Nam nên mình cũng yên tâm mua. Nhưng thực sự một số đoạn dịch hơi kì, có lẽ khâu biên tập nên rút kinh nghiệm trong những lần tái bản sau.
5
26110
2015-06-19 14:27:13
--------------------------
159158
7857
Nếu chỉ nói về nội dung thì đây thực sự là một tác phẩm đáng đọc vì nó đọng lại trong lòng người đọc một số cảm xúc nhất định, nó giúp cho ta định nghĩa về những thứ đơn giản trong cuộc sống qua từng mẫu truyện nhưng không biết do cố tình hay vô tình mà ở bản dịch có một số từ còn sai chính tả và chưa thực sự hay, phù hợp với tình tiết của chuyện.
Bìa sách và chất lượng sách tốt và đẹp, rõ ràng, ngay thẳng, mong nhà xuất bản sẽ khắc phục một số lỗi để bản dịch được hoàn thiện hơn.
4
548542
2015-02-15 11:33:22
--------------------------
150288
7857
"Trốn chạy" là một tác phẩm đoạt giải Nobel văn học, với 8 truyện ngắn rất đáng đọc. Những truyện ngắn tuy về những nhân vật khác nhau trong những hoàn cảnh, bối cảnh khác nhau, nhưng đều là những cuộc trốn chạy. Người vợ trốn chạy khỏi người chồng độc ác, con thú cưng trốn chạy khỏi chủ của nó, người mẹ bỏ đứa con để chạy theo những cơn mê điên dại của mình...."Trốn chạy" không hề có những tình tiết gay cấn, nhưng lại làm nhói đau lòng người theo một cách riêng biệt. Khi con người trong cuộc sống thường nhật chịu quá nhiều đau đớn và buồn tủi, họ phải tìm cách trốn chạy. Đó là những gì mà "Trốn chạy" nói với chúng ta. Văn phong cuốn này quá đẹp, cách viết bình thản, nhẹ nhàng mà vẫn giàu cảm xúc. Đây đích thực là một cuốn sách đoạt giải Nobel!
5
98372
2015-01-16 11:23:58
--------------------------
102466
7857
Sách của nhà văn đạt giải Nobel, hẳn sẽ là cuốn sách đáng đọc, đáng quan tâm. Tôi đã chờ đợi trên Tiki từ bữa hết hàng. Rồi cũng mừng vì mua được.
Sách Nhã Nam làm tốt. Chất lượng in không chê được. Chỉ có điều bản dịch sang tiếng Việt chưa thật sự hay nên đọc thì nắm được cốt truyện nhưng tinh thần và văn phong chưa thoát. Chẳng hạn ngay truyện đầu tiên của tập là Trốn Chạy, vẫn bị trúc trắc nhiều đoạn, đọc có cảm giác dịch chữ tây ra chữ ta. Dù sao cũng cảm ơn dịch giả đã có công giới thiệu với bạn đọc Việt Nam. Hy vọng sẽ có những bản dịch mới sách của Alice Munro hoàn thiện hơn.
3
116273
2013-12-19 13:34:23
--------------------------
