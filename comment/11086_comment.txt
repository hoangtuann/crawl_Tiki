329066
11086
Đây là một trong những cuốn sách yêu thích của mình trong bộ sách giáo dục trẻ mà mình đã mua. Tác giả giới thiệu rõ ràng ngắn gọn từng giai đoạn để chúng ta có thể biết sự phát triển của trẻ qua từng giai đoạn. Qua đó, chúng ta có thể theo kịp sự phát triển của trẻ, phát huy trí tuệ tiềm ẩn của trẻ. Mình thích các trò chơi tác giả giới thiệu theo độ tuổi, rất dễ để thực hành với con mình. Một cuốn sách nên có trong bộ sách giáo dục sớm cho con.
5
729620
2015-10-30 22:40:41
--------------------------
