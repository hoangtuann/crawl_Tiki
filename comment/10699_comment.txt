379588
10699
Mình đến với quyển sách này vì sự tò mò về 1 thiên tài như Bill Gates đã được nuôi dưỡng như thế nào để trở nên xuất chúng. Quả thực, thông qua quyển sách, mình hiểu được nền văn hoá khuyến khích con trẻ tự khám phá, tự làm chủ và tự chịu trách nhiệm đã đóng 1 vai trò quan trọng đối với việc hình thành nên nhân cách của những bậc vĩ nhân. Lòng nhân ái và sự cống hiến với đời, vời người của cha mẹ Bill Gates đã cho ông thêm niềm tin và sức mạnh để sau này lớn lên và tiếp tục nhân rộng lòng bác ái đó cho cả nhân loại. Sách viết về những kỷ niệm tuổi thơ của nhà Gates với 1 người cha nhân hậu, 1 người mẹ quảng giao và 1 người bà thông tuệ. Những đứa trẻ lớn lên trong nền văn hoá ấy cũng thừa hưỡng những đặc tính tốt đẹp và biết san sẻ, yêu thương nhau. Sách rất đáng đọc và suy nghĩ, nhất là đối với các bậc phụ huynh!
4
66279
2016-02-11 16:30:28
--------------------------
266565
10699
Đây là cuốn sách chân thật mà đầy suy ngẫm cho người đọc. Cuốn sách đã hấp dẫn tôi ngay từ những trang đầu tiên. Khi đọc tác phẩm này bạn sẽ nhận ra hình ảnh của một người cha mẫu mực là tấm gương để người con noi theo, bên cạnh đó là người mẹ tâm lý và những đứa con tuyệt vời. Cuốn sách như tựa đề của nó "góp mặt cho đời - suy ngẫm về những món quà từ cuộc sống". Món quà ở đây phải chăng là những điều tác giả đã có được khi ông là một người cha tuyệt vời trong mắt đứa con, là một người chồng tốt, là một vị luật sư tài ba.
Đọc xong tác phẩm, bạn sẽ biết mình nên suy nghĩ, hành động như thế nào để có thể trở thành một "tấm gương" như Bill Gates cha đã làm được.
5
140053
2015-08-14 20:08:10
--------------------------
