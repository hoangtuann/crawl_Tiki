393932
4772
"Doraemon" – là một bộ truyện tranh của tác giả người Nhật Bản Fujio Fujiko mà mình rất thích vì bộ truyện tranh này gắn liền với tuổi thơ của mình. Truyện có nội dung xoay quanh cuộc sống hàng ngày của Nobita – một cậu bé lười biếng, học kém, yếu thể thao... đọc bộ truyện tranh này ta sẽ rút ra được nhiều bài học ý nghĩa và bổ ích qua những cuộc phiêu lưu của nhóm bạn Nobita như: lòng dũng cảm, tình yêu thương... nhưng trên tất cả, "Doraemon" là hiện thân của tình bạn cao đẹp. Một bộ truyện bổ ích.
5
1142307
2016-03-09 16:57:25
--------------------------
