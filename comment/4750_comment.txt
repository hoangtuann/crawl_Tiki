547513
4750
Mặc cho nội dung rất ngắn, nhưng lượng tình tiết và thông điệp truyền tải của tác phẩm là vô cùng lớn, cảm giác như sóng biển đánh vào lồng ngực bạn, hết đợt này liền đến đợt khác. Không chỉ là tình yêu, mà còn là tình cảm gia đình, cách sống giữa xã hội đầy vô cảm như hiện nay. Khả năng lớn bạn sẽ ám ảnh, sẽ buồn mỗi khi nhớ đến tác phẩm. 
Bìa đẹp, dịch hay.
5
690542
2017-03-19 12:08:18
--------------------------
352403
4750
Bi kịch tình yêu một lần nữa đã được Pilai tái hiện lại và đưa lên một tầm cao mới, với những mâu thẫn, dằn xé nội tâm của hai con người nơi làng chài ở Ấn Độ. Các hủ tục, giáo điều cổ hủ ở nơi đây như những cơn cuồng phong, lướt qua cuộc đời cặp đội này, chia cắt tình yêu chỉ chực chớm nở của họ, đẩy họ ra xa nhau, và dồn ép họ đến bước dường cùng, Ở nơi đó, tình yêu đối với hai con người ấy dường như là thứ xa xỉ, không thể được mọi người chấp nhận, để rồi họ buộc phải lựa chọn con đường đớn đau nhu7gn cũng thanh thản nhất để được ở bên nhau: cái chết. Một bi kịch tình yêu thấm đẫm nước mắt!
5
570187
2015-12-14 13:59:56
--------------------------
285893
4750
Đây là một tác phẩm kinh điển của Ấn Độ nên cũng không phải dạng dễ đọc.
Mùa tôm là câu chuyện về một làng chài nhỏ. Ở đó có những con người trong vòng lẩn quẩn của số phận và những hủ tục, phân chia giai cấp, tôn giáo. Những điều này đã trực tiếp chi cắt tình yêu đẹp va thuần khiết từ lúc nhỏ của một đôi trai gái. Để rồi kết cục họ cũng được ở bên nhau, nhưng là ở dưới biển đại dương sâu thẳm kia.
Có thể đây không chỉ là câu chuyện diễn ra ở một làng chài Ấn Độ mà còn ở rất nhiều nơi khác nữa.

4
376152
2015-09-01 11:31:08
--------------------------
269591
4750
Nói chung mình vẫn thích những cuốn sách có kết thúc có hậu một cách viên mãn, với 2 nhân vật trong truyện này, mình thấy cuộc sống của họ khổ quá, tôn giáo, tập quán khác biệt, những định kiến, miệng lỡi của người đời...cả hai đều đã hết lòng vì người kia, đều nghĩ cho người kia, tình yêu của họ thực sự mình thấy cao thượng, cũng đã cố gắng hết sức để không có gì phải hổ thẹn vì tình yêu đó...Họ sống trọn vẹn vì tình yêu đó, để ngay cả khi họ có không đến được với nhau, họ vẫn đi tiếp trong cuộc đời mà không có gì phải hối tiếc hay dằn vặt nhau. Dù văn phong của cuốn sách này có thể không hợp với nhiều độc giả Việt ( tôn giáo, cuộc sống của một làng chài Ấn Độ), theo mình nghĩ nó vẫn là một cuốn sách hay.
4
104897
2015-08-17 16:12:27
--------------------------
255512
4750
Đây là quyển sách mà mình mong đợi từ rất lâu. Như một bạn đã nhận xét ở phía dưới, bạn chờ 7 năm để đọc lại được quyển sách này, còn mình thì phải chờ đến 15 năm mới tìm được nó. Ngày trước mình đọc ké sách của mẹ, khi đó vẫn còn là một cô bé, chưa hiểu nhiều về tình yêu và cuộc sống gia đình, nhưng đã khóc rất nhiều khi đọc quyển sách này. Mà thời đó mình chỉ đọc được phân nửa truyện thôi, vì tập hai không có. Mối tình trong quyển truyện luôn ám ảnh mình và mình cứ mong mãi để được đọc một quyển hoàn chỉnh. Tiki đã mang niềm vui đó đến cho mình, mình đã đọc một lèo cho đến hết quyển truyện ngay khi nhận được nó.. 

5
210330
2015-08-06 08:57:40
--------------------------
242341
4750
Mình đã tìm rất lâu để tìm được cuốn này. Lần đầu đọc mình chỉ được đọc bản tóm tắt nhưng nó đã thu hút mình. Cuốn sách này rất hay, đọc để hiểu, để cảm thông. Không chỉ phụ nữ khi yêu cũng khổ, mà đàn ông cũng khổ. Cuộc đời không như là mơ, số phận thì mãi là số phận. Nhân vật chính dù sao cũng đã sống là chính mình. Dù không cưới được người mình yêu nhưng với chồng mình, cô đã hết lòng. Đúng thật là khi là của nhau sẽ tim về với nhau. Mùa tôm đã đẩy hai người yêu nhau phải xa nhau, nhưng chính mùa tôm lại hút họ về với nhau với tất cả vị mặn nồng của biển, của tình yêu.
5
77484
2015-07-26 15:01:58
--------------------------
200661
4750
vì trước đây chẳng thấy công ty nào tái bản, phải đọc bản cũ những năm 80-90 giấy đen thui.... Cuốn sách văn học Ấn Độ này rất hay, đơn giản là nên đọc, đọc để biết được một nét văn hoá ấn độ, để cảm nhận được cuộc đời ko như mong muốn của nhân vật nữ, không thể cưới người mình yêu nhưng khi làm vợ người khác vẫn toàn tâm chăm sóc chồng. những câu văn trong đây rất dễ tiếp nhận, không hề khó đọc, diễn biến hợp lý, không lan man, không xến súa mà vừa phải, cuốn sách là một câu chuyện tình chứ không phải nói về mùa tôm khô khan đâu, mùa tôm ở đây là ẩn dụ cho cuộc sống bao quanh các nhân vật, có cái hồn lạnh lạnh của biển, có cái nóng nóng của tình yêu con người. Mình đã đọc 7 năm trước và bây giờ sẽ lại đọc nữa
5
532979
2015-05-25 17:58:08
--------------------------
