308479
10362
"Bí ẩn mãi mãi là bí ẩn", thật đúng với nội dung mà cuốn sách truyền tải. Cuốn sách thật sự hữu ích đối với một học sinh như mình. Hàng loạt các sự kiện cũng như các bí ẩn vẫn chưa có lời giải đáp đã mở ra một chân trời kiến thức mới cho người đọc. Mỗi bí ẩn đều có cái hay của nó và hay hơn là cách người viết mang nó đến cho người đọc.Một cuốn sách thật sự hay !!!! Thêm vào đó, sách có bố cục cũng như cách trinh bày rất hấp dẫn, giấy tốt. Cảm ơn tiki rất nhiều !!!
4
635906
2015-09-18 18:37:30
--------------------------
255770
10362
Bộ sách " Bí ẩn mãi mãi là bí ẩn" đã mở ra một thế giới thật lạ kỳ và mới mẻ. Những đề tài trong sách trải dài rộng khắp, về thiên nhiên, về con người, cả về vũ trụ bao la. Sự vật bên ngoài trái đất luôn là những ẩn số đòi hỏi nhân loài phải đổ ra rất nhiều tài lực để tìm hiểu khám phá. Các hiện tượng tự nhiên, các vùng đất bí hiểm, các nền văn minh từ ngàn xưa cũng được đào sâu nghiên cứu tìm tòi, một số sự vật hiện tượng đã có lời giải, nhưng vẫn còn rất nhiều bí ẩn mà trình độ khoa học kỹ thuật hiện nay chưa thể giải đáp. Ngay cả con người cũng có những khả năng, năng lực siêu nhiên, vượt ra khỏi những gì mà chúng ta từng được biết từ trước đến nay.
Sách có nhiều hình ảnh minh họa, nội dung đa dạng, phong phú, tuy nhiên một số đề tài còn bị trùng lặp.
5
109583
2015-08-06 12:57:16
--------------------------
225851
10362
Mình đã đặt mua trọn bộ "Bí ẩn mãi mãi là bí ẩn" trong một lần. Đây là bộ sách chứa đựng nhiều kiến thức, mang lại nhiều giá trị trí tuệ, đặc biệt là cho những người ưa thích khám phá và tìm hiểu về các nền văn hóa cổ xưa. Những hiện tượng được đề cập đến cũng đều là những hiện tượng bí ẩn, khiến cho người đọc đã tò mò lại càng háo hức muốn đi tìm lời giải. Các tập trong bộ sách được bố cục hợp lý. Sách có số trang phân bố phù hợp, in trên giấy tốt. Chỉ có điểm trừ là một vài nội dung bị lặp lại ở các cuốn trước và sau.
5
24486
2015-07-11 00:05:51
--------------------------
211448
10362
bộ sách Bí Ẩn Mãi Mãi Là Bí Ẩn theo khuynh hướng khoa học giải thích về các hiện tượng xảy ra gần với cuộc sống trên khắp thế giới . Ngoài ra còn đề cập về các sinh vật trong huyền thoại vẫn chưa được chứng minh đang và đã tồn tại , lẫn các địa danh huyền bí nổi tiếng luôn được chú ý tới. Điểm cộng của bộ sách này là đánh vào các bí ẩn luôn làm cho tâm trí con người tò mò về chúng ngoài ra còn có các hình ảnh minh họa cụ thể cho từng đề tài tạo sự hấp dẫn cho người đọc tránh nhàm chán, điểm trừ là vẫn có vài đề tài bị trùng lặp với nhau và vài bí ẩn hiện nay đã được giải thích nên tạo sự đối lập trong tư tưởng người đọc .
5
104474
2015-06-20 16:26:08
--------------------------
179837
10362
Bộ sách này hay lắm. Thực sự làm mình bị thu hút khi vửa mới đọc . Quyển này cho mình biết rất nhiều về những hiện tượng lạ. Chủ đề rất đa dạng nhưng mỗi chủ đề thì luôn có rất nhiều dẫn chứng được nêu rất chi tiết và nhiều sự việc xảy ra ngoài đời làm mình bị thuyết phục ngay. Những hiện tượng được nhắc đến hiện tượng nào cũng rất kì lạ và bí ẩn khiến mình rất tò mò và chóng đọc hết. Ngoài những chủ đề khá nổi như UFO, đĩa bay thì bộ sách này còn khai thác cả những vấn đề rất ít được nhắc nhưng rất bí ẩn và hết sức thú vị.
Tuy từng cuốn sách không dày, mỗi bài viết được viết ngắn ngọn trong một hoặc vài trang, kèm theo đó là những hình ảnh minh họa cụ thể, bộ "Bí ẩn mãi mãi là bí ẩn" này sẽ chỉ ra cho ta thấy xung quanh chúng ta luôn tồn lại những điều mà khoa học không thể chạm tới, cho chúng ta cảm giác thế giới chúng ta đang nhìn thấy đây luôn tồn lại một sức mạnh vô hình, và đôi mắt đôi khi không thể giúp chúng ta nhìn thấu tất cả.
5
602569
2015-04-08 07:11:24
--------------------------
