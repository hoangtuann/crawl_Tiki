376256
10527
Truyện rất hay. Đúng là sách bộ giáo dục Anh khuyên đọc. Nhắc đến truyện ma cà rồng người ta thường nghĩ đến những chuyện kinh dị khủng khiếp, ma cà rồng hút máu người. Nhưng Darren Shan hoàn toàn không phải như vậy. Chuyện vẫn là cuộc sống hằng ngày của chúng ta, về những tình bạn, tình yêu của Darren. Đặc biệt là nó còn mang đến những giây phút hết sức xúc động khi một con người phải từ bỏ quyền làm người của mình. Một bộ truyện thật tuyệt vời. Mọi người sẽ không hối hận khi mua nó đâu :)))
5
1101166
2016-01-30 13:56:58
--------------------------
325418
10527
Chất lượng trang in lẫn bìa đều ở mức tuyệt vời.Ngay từ tập 1 đã cuốn hút người đọc bởi những nhân vật trong truyện, vừa thể hiện sự quái dị thường có trong các tiểu thuyết kinh dị, vừa giúp người đọc tạo ra nhân vật cho riêng mình, Lồng ghép các yếu tố lại khiến câu chuyện này cực kì hấp dẫn và lôi cuốn người xem qua từng trang, đọc tập 1 rồi, tập 2 càng không khiến mình thất vọng.. Đọc xong tập 2, mong ước tiếp theo của mình là sở hữu trọn bộ của tác giả. Một Ngàn Like cho tác phẩm của tác giả
5
878283
2015-10-23 13:59:11
--------------------------
195169
10527
Có thể nói đây là thể loại truyện kì bí, viễn tưởng đầu tiên mà mình đọc. Thật sự là truyện không để người đọc thất vọng mà! Mình bị cuốn hút vào truyện từ những tập đầu tiên, theo dõi, hồi hợp, sống cùng với những cảnh, những tình tiết trong truyện đó. "Những câu truyện kì lạ của Darren Shan" hình như có bản phim điện ảnh, nhưng mà mình thấy đọc truyện hay hơn gấp trăm lần ấy! Bạn có thể tự tưởng tưởng ra hình ảnh của nhân vật, rồi cảnh vật theo ý thích. Thật sự đến bây giờ hình ảnh núi ma cà rồng, rồi trận đánh nhau ở sân vận động, đám ma cà chớp, Darren Shan, thầy Cresley, những người trong gánh xiếc quái dị vẫn còn như in trong đầu. Hoàn thành bộ truyện mà vẫn cứ có cái gì đó nuối tiếc, đọc xong ngẩn người luôn. Kết thúc nó quá là ảo, vô định, mình cũng không biết diễn tả thế nào cho chính xác nhưng mà cái kết kiểu như là đảo ngược tình hình, cốt truyện từ tập đầu. Kiểu như cái mở nút hoành tráng mà mình đọc xong mấy ngày sau mới hoàn hồn lại được. Đây, thật sự là một bộ truyện đáng mua và đọc đi đọc lại!
5
91309
2015-05-12 21:45:54
--------------------------
191153
10527
Kể về quá trình tiếp theo kể từ khi Darren được truyền máu và trở thành Ma cà rồng nửa mùa. Nó theo chân ông Bradley trên cuộc hành trình vô định để rồi trở về với nơi định mệnh, Gánh xiếc quái dị. Lần này, Darren chính thức trở thành một thành viên trong đại gia đình quái dị, làm quen thêm nhiều bạn mới và nhiều khi tự tạo cho mình những kẻ thù mới, những bi kịch mới. Đằng sau mỗi câu chuyện của Darren luôn ẩn chứa những thông điệp đơn giản mà sâu lắng về tình người , cuộc đời: Trân trọng những tình cảm trước mắt và hãy sống cho ngày hôm nay.
4
166935
2015-05-01 09:06:12
--------------------------
15951
10527
Nếu tập 1 của bộ truyện " Những câu chuyện kì lạ của Darren Shan " nói về cái "định mệnh" đã đẩy cậu bé Darren bước vào thế giới của sinh vật đêm tối thì đến tập 2 này, bạn sẽ được chứng kiến cuộc phiêu lưu đầu tiên của cậu bé.
 Vẫn là lối viết nửa úp nửa mở cùng giọng văn hài hước, tác giả đã tạo nên những tình huống rất logic và hấp dẫn khiến bạn khó lòng mà bỏ cuốn truyện xuống một khi đã cầm lên. Tình bạn giản dị giữa Darren, cậu bé rắn cùng một chú nhóc loài người và cuộc chiến không cân sức với người sói sẽ làm bạn hồi hộp đến thót tim. Qua đó, truyện không chỉ đơn thuần là giải trí mà còn gửi đến ta thông điệp đầy ý nghĩa :  trân trọng tình bạn, nâng niu và giữ gìn nó và hãy giữ lòng trung thành.
Đừng bỏ lỡ tập truyện hấp dẫn này nhé !
5
15657
2011-12-14 14:32:08
--------------------------
