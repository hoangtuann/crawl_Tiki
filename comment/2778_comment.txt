379481
2778
Dế rô bốt- một cuốn truyện tranh hài hước và vui nhộn. Bìa sách cũng khá dễ thương. Những hình vẽ trong truyện đều ngộ nghĩnh và đáng yêu. Câu chuyện xoay quanh những cô cậu bé  Út Đa, Ù La, Bom Hô, Chi Hu cùng với nhiều món bảo bối vô cùng đặc biệt của Dế Rô bốt. Mỗi lần đọc từng câu chuyện nhỏ trong sách khiến cho mình cười rất sảng khoái và vui. Ngoài ra cuối mỗi cuốn truyện còn có phần giới thiệu lại kĩ càng của từng món bảo bối của dế rô bốt và các truyện cười khác.
4
639171
2016-02-11 11:51:19
--------------------------
309266
2778
Mặc dù mình đã là sinh viên và đã qua rồi cái thời đọc truyện tranh nhưng mình vẫn rất thích truyện tranh vì những hình vẽ ngộ nghĩnh, đáng yêu trong truyện. Vì vậy mà mình vẫn mua bộ truyện Derotbo về đọc tuy rằng Dếrôbốt - Nhân Tài Ảo Thuật là bộ truyện tranh khoa học giả tưởng dành cho thiếu nhi từ 7 - 11 tuổi. Những câu chuyện vui nhộn kèm theo đó là những bảo bối thần kỳ đem lại những tiếng cười sảng khoái sau một ngày học tập mệt mỏi. Cuối mỗi tập truyện đều có phần giới thiệu lại các bảo bối giúp người đọc theo dõi dễ dàng hơn.
4
408969
2015-09-18 23:00:09
--------------------------
