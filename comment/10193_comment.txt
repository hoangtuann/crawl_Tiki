379784
10193
Một câu chuyện tình yêu đầy lãng mạn giữa anh chàng  siêu sao khúc côn cầu Sam và cô nàng lên kế hoạch đám cưới Autumn . Sau bao nhiêu thăng trằm, họ cuối cùng cũng đã học được cách tha thứ cho nhau, Câu chuyện với kết thúc có hậu làm ấm lòng trái tim của độc giả. Anh chàng đào hoa đẹp trai Sam lại một lần nữa bị khuất phục trước cô nàng quyến rũ ương bướng Autumn.  Văn phong hài hước dễ thương mang lại tiếng cười cho độc giả. Đây là một tác phẩm hay, rất đáng đọc. 
4
653344
2016-02-12 16:49:15
--------------------------
373863
10193
Cùng mô tip yêu nhau có con và sau đó yêu lại nhưng không giống như Tình yêu trở lại của Rachel Gibson tác phẩm này khá hơn nhiều vì hiểu lầm lúc trước của họ do họ còn quá trẻ và không quen biết nhau lâu, lúc trước chỉ là hấp dẫn về thể xác và sau này mới là tình yêu và rất nhiều chi tiết dễ thương đã giúp cho câu chuyện nhẹ nhàng êm dịu như lúc Sam phê thuốc giảm đau hay nhân vật con trai hai người cũng dễ thương và là chất xúc tác quan trọng cho tình yêu của hai người. Đúng với tâm lý là những người yêu nhau sẽ đến với nhau.
3
306081
2016-01-24 21:51:37
--------------------------
360733
10193
Trái tim mở rộng khiến con người đi sâu vào nỗi nhớ cần thể hiện ra cho ta một sức sống mạnh mẽ cần tôn cao , trong truyện người đàn ông có thế lực , đào hoa với các cô gái yếu ớt , chân yếu tay mềm song lại đến với nhau như sự sắp đặt ngẫu nhiên , tình cờ giữa thế giới mong manh ngoài thiên hà , với cá tính trái ngược nhưng lại hấp dẫn nhau tạo ra sự độc đáo , lạ lùng với cả 2 lẫn người đọc , diễn biến khá khôi hài .
3
402468
2015-12-29 18:32:39
--------------------------
240014
10193
Câu chuyện tình yêu của anh chàng Sam và cô nàng Autumn khiến tôi bật cười. Anh chàng đào hoa Sam cuối cùng cũng chịu khuất phục trước cô nàng Autumn "ngang tài ngang sức". Đoạn đối thoại giữa hai nhân vật này luôn làm tôi cảm thấy "khó đỡ". Cảnh đêm Halloween khi Autumn mặc chiếc áo Sydney Crosby Penguins bị Sam bắt gặp là một trong những cảnh khó quên nhất :)) Dù cốt truyện không quá mới lạ nhưng lời văn cùng cách xây dựng các tình huống truyện, lời thoại giữa hai nhân vật chính của Rachel đã làm cho truyện không hề nhàm chán chút nào. Một quyển sách hay.
4
35746
2015-07-24 10:58:53
--------------------------
82660
10193
Với văn phong của Rachel Gibson thì không có gì mới mẻ, nhưng mình thích trong mỗi tác phẩm lại là một câu chuyện rất mới mẻ và hài hước. 
Với "Người đàn ông của tôi" mình thích những sự hằm hè, những cuộc đối đầu giữa Sam và Vince - anh trai của Autumn. Tuy hơi khó chấp nhận một anh chàng lăng nhăng như Sam có thể hoàn lương nhanh đến như vậy nhưng mỗi tình tiết trong truyện khiến mình cảm thấy rất thú vị, mỗi đoạn đối thoại, những cuộc gặp gỡ của Sam và Autumn, đặc biệt là khi anh bắt gặp cô mặc chiếc áo "chim cánh cụt" - một biểu tượng của đối thủ của mình, dù không hề có ý định chọc tức Sam nhưng chiếc áo đã làm anh phát điên, đọc đến đoạn này mình không thể không phì cười. 
"Người đàn ông của tôi" còn cho mình thấy một khía cạnh mới của việc tha thứ và lãng quên, cho dù điều đó có vẻ như khó khăn và bất khả thi đi nữa, nhưng nếu đủ dũng cảm và lòng tin thì không có điều gì là không thể.
4
125574
2013-06-22 12:54:28
--------------------------
77424
10193
khi đọc tác phẩm của  Rachel Gibson dường như người đọc luôn bị cuốn hút bởi lối viết văn bay bổng nhưng không kém phần dí dỏm với nội dung quen thuộc là tình yêu và môn khúc côn cầu. Và cũng với lối văn đó bà đã xây dựng nên hình ảnh của Autumn - một nhà lên kế hoạch đám cưới thành công và Sam LeClaire - một thành viên của đội Chinooks thông qua tác phẩm "Người đàn ông của tôi". Tưởng chừng như cuộc hôn nhân đã đổ vỡ không hàn gắn lại được thì sau nhiều năm gặp lại tình cảm của lại nảy nỡ và sâu sắc hơn vì trước đó họ là cha mẹ của một cậu con trai khẩu khỉnh. Và chính trách nhiệm của cha mẹ đã giúp 2 người hiểu được hạnh phúc của gia đình là tất cả. Đây là một tác phẩm rất sâu sắc và có giá trị cao !!!!

5
51106
2013-05-27 21:01:37
--------------------------
50725
10193
Cũng vẫn là motip quen thuộc của Rachel là tình dục và môn khúc côn cầu. Nhưng cuốn sách này như những cuốn sách khác với lối dẫn dắt người đọc vào câu chuyện một cách khéo léo và cuốn hút. 
Một kết thúc truyện có hậu củng cố thêm sự tin tưởng vào tình yêu , tình yêu thương gia đình cho người đọc về một cuộc sống đầy sóng gió và thực dụng hiện nay.
Một cuốn truyện đáng để đọc !
3
13536
2012-12-16 11:23:19
--------------------------
49519
10193
Tác phẩm này đem đến những xúc cảm hoàn toàn mới mẻ và lý thú cho mình. Tác giả đã kể một câu chuyện sinh động trong nhưng chi tiết phức tạp đan xen, lột tả quá chân thực và hấp dẫn tâm lý và tính cách nhân vật, tạo nên một câu chuyện tình yêu đúng chất của "Richel Gibson". Rõ ràng là câu chuyện của Autunm và Sam mang lại nhiều cung bậc khác nhau, vui vẻ, hài hước, hồi hộp và lãng mạn. Diễn tiến tình yêu của họ được nhà văn khai thác và xử lý rất triệt để, với ngôn từ sâu sắc, tinh tế, diễn tả được mọi rung động dù nhỏ nhất ở mỗi nhân vật. Cuốn sách này, hay một câu chuyện tình yêu kỳ diệu, không khỏi làm mọi người xao xuyến và bồi hồi.
4
20073
2012-12-08 09:39:29
--------------------------
33666
10193
Đôi khi, chúng ta phải học cách: let it go, nhất là những chuyện xảy ra trong quá khứ lại mang đến "kết quả tốt đẹp trong tương lai". Sam và Autumn đã có những điều-nên-hối-hận trong quá khứ nhưng lại có thể bỏ qua. Suốt quyển truyện là giọng văn nhẹ nhàng và dí dỏm, cả những tiêu đề của từng chương như một danh sách của Rachel Gibson. Nếu bạn cần học cách chấp nhận quá khứ và xoá bỏ ám ảnh, nghĩ về điều tốt đẹp hơn ở tương lại, bạn nên dừng lại ở truyện này: Any man of mine
4
3271
2012-07-15 18:28:37
--------------------------
24723
10193
Mình đã đọc khá nhiều cuốn của Rachel Gibson và rất thích cách tác giả này đưa người đọc vào thế giới nội tâm, suy nghĩ của các nhân vật và sống cùng họ. Mình cũng khá thích thú với mô típ truyện: yêu-bỏ-bỏ-yêu thế này :)) và có vẻ như Rachel đã tạo nên tình huống này rất tốt. Cách 2 nhân vật chính - kẻ tám lạng người nửa cân- đối đầu với nhau rất hài hước, đáng yêu và trung thực. Chất dí dỏm của câu chuyện cũng là một điểm sáng không thể bỏ qua. Cảnh Sam bắt gặp Autumn mặc bộ đồ kì cục Sydney Crosby Penguins tại bữa tiệc Halloween là một trong những cảnh khiến mình bò lăn bò cười. Thêm vào đó, sức hút lẫn nhau của 2 nhân vật chính sẽ tạo nên những cảnh hot xịt máu mũi. Cuốn sách nằm trong series Chinooks hockey này sẽ khiến các bạn mê thể loại chick-lit hài lòng :))
4
4058
2012-04-15 20:51:09
--------------------------
