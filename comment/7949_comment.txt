404292
7949
Cuộc sống tội nghiệp của một cậu bé “chưa hề biết thế nào là mái ấm gia đình”, một cậu bé “ từ nhỏ đã phải lê la đầu đường xó chợ kiếm sống” đã được tái hiện qua ngòi bút của tác giả Nguyễn Trí Công. Đó là cuộc đời đầy bất hạnh của những trẻ em cơ nhở, luôn dựa vào sự giúp đỡ của mỗi người nhưng cũng có những em không có số phận may mắn như vậy,điều đó đã khiến em bước đi trên con đường tội lỗi. Dù là gì đi chăng nữa , tác phẩm đã phản ánh một thực trạng đang xảy ra trong xã hội ngày nay và hơn hết Nguyễn Trí Công đang thổi vào tác phẩm của mình một sự đồng cảm và khơi gợi sự đồng cảm ấy trong lòng người đọc.
4
634723
2016-03-24 22:44:17
--------------------------
400943
7949
Truyện này tác giả viết vào năm 1994. Khá lâu rồi. Tuy nhiên tên các con đường vẫn như thế. Vì khá gần nhà tôi (tphcm) nên dễ dàng tưởng tượng từng con đường tác giả nói đến. Khá thú vị. Truyện nói về những đứa trẻ bụi đời. Truyện với từ ngữ đậm chất nam bộ. Tôi rất thích. Tuy nhiên tôi thấy truyện hơi dài dòng. Hay nhưng chưa đủ lôi cuốn tôi đọc 1 mạch dù truyện khá ngắn. So với tác phẩm Nhật ký buồn của hải âu mà tác giả viết. Truyện kia hay hơn và lôi cuốn hơn. Tuy nhiên truyện này thích hợp với thiếu nhi như bìa sách có đề cập.
4
418779
2016-03-19 23:39:21
--------------------------
