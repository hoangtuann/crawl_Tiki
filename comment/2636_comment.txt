346343
2636
Mình thường xuyên mua mấy cuốn tô màu cho bé nhà mình ở các nhà sách. Hôm nay mua quyển này trên Tiki cho chẳn tiền đơn hàng. Nói về sách thì bé rất thích, giá lại rẻ hơn so với mua ngoài. Các hình ảnh trong sách tô màu có mẫu nên có thể giúp các bé tự tô hoặc cũng có thể sáng tạo màu mới. Mà mấy loại tô màu này trẻ con tô rất nhanh, nên chắc chắn trong đơn hàng tiếp theo mình sẽ mua nhiều hơn cho  cháu tô. Rất giúp các cháu thoả sức sáng tạo và cảm nhận màu sắc tốt!
5
724772
2015-12-02 13:57:04
--------------------------
