391740
5602
Mình chờ tập cáp treo và bà chúa Tuyết này lâu lắm rồi. Thủ pháp giết người độc đáo và nghe có vẻ ma quái, rùng rợn. Nhật Bản có nhiều truyền thuyết nhỉ ! Vụ án của tay nhà báo tự do có phần rắc rối mà những đứa trẻ mới học lớp 1 lại xử lý được nhưng không ai thắc mắc ! Đó là vụ án hóc búa và trí tuệ và mình chịu thua, khong suy luận như thế được. Hồi nào ranh xem và nghiên cứu lại. 
Tác giả càng ngày càng bớt sáng tạo nén mấy vụ sau này không hấp dẫn lắm. 
5
535536
2016-03-05 19:43:05
--------------------------
375355
5602
Vụ bắt cóc ở mấy file đầu thật hơi nhảm một chút nhưng mình cảm thấy anh chàng Takagi ngốc nhất trên đời. Anh chàng làm gì cũng nghĩ đến Sato,lúc nào anh ấy cũng làm tổn thương mình hơn làm hại đến người khác, một anh chàng tốt bụng như thế nhất định phải giữ lấy nhé Sato. Thì ra ngay từ lúc nhỏ hai anh chàng Hattori và Shinichi đã "đụng" nhau rồi, hai cái đầu nhạy bén này quả là thiên tài mà, nhìn nụ cười của hai người lúc hình ảnh cuối truyện làm mình tò mò nhất, nhất định là hai anh chàng đã tìm ra thủ phạm rồi.
5
13723
2016-01-28 12:27:08
--------------------------
371979
5602
Conan là một series truyện tranh mà mình thích nhất từ trước đến nay. Mỗi lần ra sản phẩm mới là đều phải ra nhà sách mua ngay. Bình thường là mình ra nhà sách mua nhưng nhân tiện đợt này mua hàng trên tiki nên đặt luôn. Truyện nhận về rất đẹp, giống như khi mình mau trên nhà sách, chất lượng khá tốt. Mình chỉ thắc mắc không biết vì sao lại tăng giá lên từ 16k lên 18k vậy, mình thấy nội dung cũng đâu có khác gì cũ, thấy hơi đắt nhỉ. Dù sao thì conan vẫn rất tuyệt.
5
709418
2016-01-20 20:46:13
--------------------------
371518
5602
Trong bộ sưu tập Conan thì cõ lẽ đây là tập mà mình yêu thích nhất đặc biệt là vụ án ở trượt tuyết khi được kể lại. Phải nói là nó cực lì hấp dẫn bởi đó là cuộc so tài giữa hai thám tử diễn ra trong quá khứ Heiji và Shinichi khi cả hai cậu thám tử còn đang là học sinh cấp 2. Và chính vì sự so tài đó đã cho họ biết đến nhau hiểu nhau hơn và trở thành bạn thân phá án trong các vụ án. Ở tập nay nó hay nữa là có sự xuất hiện của hai nhân vật cực kì dễ thương là bố mẹ của Conan .
5
1062396
2016-01-19 18:55:51
--------------------------
361425
5602
Conan là bộ truyện tôi đọc từ hồi bé xíu. Truyện cứ tái bản hết lần này đến lần khác nhưng hồi đó nghèo khó không mua đủ bộ được, và rồi bây giờ, tôi dành dụm để đón nhận người bạn mới vào tủ truyện của mình. Conan thông minh nên tôi rất thích. Tập 50, tôi thích nhất là tập bố mẹ Shinichi và Heiji gợi ý cho đứa con. Cả hai dù chỉ mới cấp hai nhưng những lập luận là vô cùng sắc bén. Bên cạnh con đường dài của thám tử phía trước là những tâm trạng đáng yêu của Kazuha và Ran khi quan tâm đến Shinichi và Heiji. Câu chuyện thật hấp dẫn và tuyệt vời.
5
362041
2015-12-30 23:34:07
--------------------------
350597
5602
Mở đầu là cuộc hẹn hò tập thể giữa các nhóm bạn của Sato, Takagi,Chiba,Yumi. Vui hơn là khi Sato và Takagi tình cờ gặp nhau và cả hai đều ghen vì hiểu lầm đối phương và còn chọc tức nhau nữa. Nhưng khúc cuối thì lại kết thúc bằng màn nói chuyện tình cảm trong xe.Điểm thu hút hơn hết thì lại là vụ án của Shin và Hattori khi hai nguời học cấp hai .Khung cảnh dưới trời tuyết làm cho truyện càng hấp dẫn và không kém phần lãng mạn của hai coupe Shin-Ran,Hattori- Kazuha .Bị cuồng tập này
5
849191
2015-12-10 21:58:53
--------------------------
306740
5602
Mình tìm mua quyển truyện này tại một hiệu sách gần nhà.Quyển truyện này mình thích nhất là vụ án trên núi tuyết, cuộc so tài giữa hai thám tử diễn ra trong quá khứ Heiji và Shinichi khi cả hai cậu thám tử còn đang là học sinh cấp 2. Ngoài ra vụ án nhà báo có hẹn để phỏng vấn bọn nhóc thám tử lớp 1B cũng rất hấp dẫn. Nhưng có một điều mình không thích là mực in mỗi khi cầm sách đọc lại vấy bẩn cả tay và trang sách có những trang định lại mình phải dùng kéo tách ra nếu dùng tay sẽ bị rách. Mong nhà xuất bản chú ý hơn trong những lần tái bản sau.
3
559023
2015-09-17 19:27:04
--------------------------
278070
5602
Ở tập truyện này, mình thích nhất là vụ án có liên quan đến một nhà báo có ý định phỏng vấn nhóm thám tử lớp 1B. Với sự có mặt của cô giáo, conan phải cố gắng tỏ ra "ngây thơ" và trẻ con đúng nghĩa để tránh sự nghi ngờ của cô. Chính điều đó đã làm cho nhân vật conan trở nên vô cùng dễ thương và hồn nhiên. Bên cạnh đó, vụ án bắt cóc trẻ con ở đầu tập và cuộc tranh tài giữa Shinichi và Henji trong cùng vụ án mạng xảy ra trên vùng núi tuyết cũng rất hấp dẫn. Nhờ đọc tập truyện này mà mình đã biết thêm rất nhiều về câu truyện "bà chúa tuyết" nổi tiếng tại Nhật...
4
302048
2015-08-25 16:22:55
--------------------------
242728
5602
Quyển truyện này mình thích nhất là phần phá án giữa Shinichi va Heiji thời trung học. Phong thái của 2 người khá là giống nhau, màn xử án thì tài tình, lúc nào đọc xong mình đều cảm thấy bất ngờ cả. Bác Aoyama vẽ tranh thì đẹp hết chỗ chê rồi, tỉ mỉ từng chi tiết nữa. Vụ án lần sau có thêm mấy anh chị cảnh sát cũng dễ thương thật, mình thích nhất là chị Sato với anh Takagi. Nói chung mình rất thích quyển truyện này vừa li kì, hấp dẫn lại không kém phẩn dễ thương nữa.
5
704114
2015-07-26 21:46:21
--------------------------
197216
5602
Vụ án bà chúa tuyết trong tập này xảy ra khi cả Shinichi Kudo và Heiji Hattori đều đang học trung học và trường học của cả hai đều đang đi du lịch với trường học ở một khu trượt tuyết. Vụ án này rất tò mò, hấp dẫn. Nhưng điều mình thích hơn cả là sự thể hiện khả năng suy luận của Shinichi và Heiji. Và tsự xuất hiện của bố mẹ của cả hai đều rất dễ thương. Hình ảnh trong tập này được chăm chút rất đẹp, các nhân vật đều rất rõ nét và đáng yêu. Bìa sách cũng đẫ thể hiện được vụ án trung tâm của tập truyện là ở khu trượt tuyết. Đây là một trong những tập truyện của bộ Conan mà mình thích nhất và rất vui vì được đọc.
5
45656
2015-05-17 10:16:52
--------------------------
196869
5602
Vụ án nào trong tập này cũng hấp dẫn hết á. Nhưng hấp dẫn phải nói đến vụ án chủ đạo, tức là vụ án ở bìa đấy, vụ án mang tên bà chúa tuyết đem lại cho chúng ta quá khứ của cả Conan và Heiji khi hai người đang còn là học sinh cấp 2. Thật sự là đọc đi đọc lại vẫn thấy hấp dẫn quá trời quá đất. Và còn cả vụ án liên quan đến các cô chú cảnh sát Sato, Chiba, Takagi quen thuộc nữa chứ, cũng hấp dẫn không kém đâu nhé. Một tập truyện không thể bỏ qua trong bộ sưu tập Conan nhá.
5
530790
2015-05-16 16:39:18
--------------------------
179760
5602
thích vụ án bà chúa tuyết của tập 50.là vụ án hồi tưởng lại quá khứ khi mà shin và heiji vẫn đang là học sinh trung học.một vụ án khó với 2 chàng thám tử và phải nhờ đến sự trợ giúp của những bậc phụ huynh :) 
một người bị bắn chết trên cáp treo bên cạnh là một chiếc túi đựng đầy tuyết,y hệt như vụ án nhiều năm về trước. dựa trên gợi ý từ cha mẹ của mình mà shin và heiji đã tìm ra thủ phạm.heiji đến tận cơ quan điều tra để phá án trực tiếp còn shin thì gọi qua điện thoại.2 người ngẫu nhiên đã cùng giải được vụ án hóc búa.kỉ niệm đầu giữa 2 chàng thám tử đông tây !
4
586877
2015-04-07 22:22:37
--------------------------
