538977
3137
Mặc dù xem phim qua rồi và vẫn đang chờ đợi những tập tiếp theo, nhưng tôi vẫn thấy sức hấp dẫn của đọc truyện.. phải nói truyện hay quá hay.. k kém gì phim 1 chút nào.. vẫn những tình tiết hài hước lẫn kịch tính.. ai cũng nên xem 1 lần trong đời 
5
2019783
2017-03-10 16:33:52
--------------------------
322769
3137
 Mình biết đến One piece từ anime chứ không phải manga. Sau khi bị anime hấp dẫn, mình tìm mua manga trên tiki. Đọc truyện cảm thấy rất hấp dẫn, hồi hộp. Tình tiết phát triển nhanh hơn phim, nét vẽ đẹp và chăm chút hơn. Ngoài ra manga được nxb Kim Đồng in bằng giấy xốp, nhẹ, mềm, thơm, thích hợp cầm đi mọi nơi cũng như nằm đọc mà không ngại mỏi tay. Bìa rời, màu sắc đẹp, tranh bìa được chọn kĩ lưỡng rất bắt mắt. Nói chung khi cầm cuốn truyện trên tay cảm thấy rất hài lòng dù giá có hơi chát khi theo 1 bộ hơn 70 cuốn.
5
30531
2015-10-16 22:40:24
--------------------------
255080
3137
Khả năng Mantra của thần Enel đã không tính toán đúng. Băng Mũ Rơm vẫn tiếp tục chiến đầu dù có bị thương rất nặng. Enel đã khởi động con tàu bay của mình nhằm phá hủy đảo Skypiea và đẩy hết người dân trên đảo xuống biển xanh. Người dân của đảo trên trời - Skypiea đang cố gắng "di cư" để tránh nguy cơ bị hủy diệt cùng với hòn đảo. Băng Mũ Rơm thì lại cố gắng đánh bại Enel để bảo vệ đồng đội của mình và hòn đảo xinh đẹp này. Luffy sẽ hạ Enel như thế nào?
5
730702
2015-08-05 19:09:06
--------------------------
220706
3137
Nói chung lần tái bản này của Onepiece khiến mình khá hào hứng. Giấy xốp, bìa rời được chỉnh theo gần với bản gốc nhất giúp những fan cuồng và cả những ai đã bỏ lỡ sẽ cho mình cơ hội để sở hữu 1 tuyệt đỉnh manga. Trong tập này, những giây phút giẫy chết của skypiea khi kẻ tự xưng là "thần" Enel khởi động con tàu bay và âm mưu tống toang bộ người dân trên trời xuống biển xanh. Nhưng thứ mà hắn không ngờ, một thiên địch của hắn đã xuất hiện. Cuộc chiến không cân sức này đang diễn ra không theo đúng những gì mà khả năng Mantra của Enel có thể tính ra. Thật sự không thể dứt ra được mà cứ muốn đọc mãi không thôi!
5
166935
2015-07-02 21:41:14
--------------------------
