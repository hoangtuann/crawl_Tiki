288839
10167
Nửa đầu truyện cứ như ta đang đọc luận văn về xe máy, cung cấp đầy đủ thông tin về sự thay thế của các đời xe máy qua các năm, nêu bật các lợi ích và tiện lợi của xe máy so với các phương tiện đường bộ khác, lại còn có hẳn một bài cho việc thống kê sự phát triển mật độ xe máy ở các đô thị lớn, tăng dần số lượng các thương hiệu xe máy lắp đặt nhà máy ở Việt Nam, công suất sản xuất xe máy đặc biệt là của Honda Việt Nam. Còn nửa sau của sách lại không liền mạch với tựa sách, không liên quan đến xe máy mà lại nói về lối sống, phố phường, ngõ ngách ở Hà Nội.
3
583423
2015-09-03 22:19:28
--------------------------
198640
10167
Ngay từ tên truyện ta đã cảm nhận được sự phóng khoáng cũng như hài hước của câu truyện. Cách tác giả vào đề, cũng như cung cấp thông tin về xe máy- một phương tiện phổ biến, vô cùng quen thuộc với mỗi người chúng ta rất thú vị. Cuộc sống hàng ngày của chúng ta gắn với chiếc xe máy rất nhiều và khó có thể có 1 phương tiện nào thay thế được nó. Cách nhìn của tác giả về sự gắn bó của người VIệt Nam rất thú vị. Tuy nhiên phần cuối truyện  mình cảm thấy hơi lan man, ko cuốn hút mình như phần đầu.
3
288211
2015-05-20 13:33:53
--------------------------
80976
10167
Với Xe máy tiếu ngạo, tác giả thật sự rất am hiểu về các đời xe máy, về những hiện tượng xã hội xung quanh những con xe. Gắn liền với nó là những cái nhìn lúc châm biếm, lúc ngưỡng mộ, đôi khi ghen tị với những người sở hữu nó. Những áng văn như dòng suy nghĩ, lời tâm tình, trò chuyện lúc uống trà, ăn cơm hay lúc nhâm nhi tách cà phê. Tuy nhiên, hạn chế của tác giả là do sử dụng nhiều từ ngữ quá đắt, có giá trị nên làm câu văn hơi dài dòng, khó hiểu, nhập nhằn câu chữ khiến người đọc dễ nhàm chán. Tuy nhiên, đó là những cái nhìn rất thi vị về đời sống.
3
100289
2013-06-14 20:25:57
--------------------------
71652
10167
Tản văn khá hay, thể hiện vốn sống, vốn hiểu biết phong phú và cả phong thái tự nhiên, nhẹ nhàng mà tinh tế của người viết, dù giọng văn không thực sự là gu của mình. Bối cảnh rộng lớn, dày, sâu, trải dài từ mấy chục năm trước đến nay. Chiếc xe máy hiện lên với nhiều câu chuyện, giai thoại, hình ảnh và góc độ thú vị. Đối với một người chừng 20 tuổi đầu như mình thì cuốn sách này quả là một vựa kiến thức cần thiết, đáng đọc.

Chỉ tiếc là bối cảnh tác phẩm tập trung ở Hà Nội nên đối với một người Sài Gòn có chút không thân thuộc, gần gũi. 

3
96489
2013-04-27 23:57:10
--------------------------
48038
10167
Tản văn này khá độc đáo và đem đến nhiều chiêm nghiệm thú vị cho mọi người. Chỉ với chiếc xe máy đã quá đối quen thuộc với chúng ta, nhưng tác giả đã nghĩ ra, đã sáng tạo dựa trên chính trải nghiệm của bản thân biết bao câu chuyện hài hước, lôi cuốn và hấp dẫn.  Từ những cảm nhận về chiếc xe, đến những cách nhìn cuộc sống thông qua chiếc xe, và cả những bật mí khá ngộ nghĩnh nhiều người chưa biết đến. Tác giả đặt con người vào một tổng thể gắn bó với xe máy, làm những bài viết, những tâm sự càng gần gũi và thuyết phục hơn.
Đây là một tản văn hay với những cách diễn đạt, cách dùng từ rất Tây, rất hài hước và sinh động!
4
20073
2012-11-28 16:27:19
--------------------------
28780
10167
Đọc xong cuốn này cảm nhận của mình tuy đọc cả cuốn sách thấy dài hơi vì bị cuốn vào mỗi chiếc xe máy và hằm bà lằng xung quanh nó. Nhưng qua đó tác giả cũng tóm tắt lại quá trình từ xe đạp lên xe máy rồi cả ô tô nhưng chú trọng vẫn là cuộc sống quanh xe máy.

Tản văn này có nhiều triết lý khá hay, ngộ nghĩnh mà khi đọc chính mình cũng Ngộ ra được nhiều điều.
3
28013
2012-05-31 18:32:46
--------------------------
