423380
5865
Đã mua rất nhiều sách của windy- mcbook và rất hài lòng ,về cuốn sách trắc nghiệm trình độ A này mình có nhận xét như sau : sách dày khoảng 300-400 trang  , bìa đẹp chắc chắn , nội dung khá đầy đủ , bài test nhiều loại và dạng bài phong phú , có cả đáp án phía sau rất tiện cho người làm bài kiểm tra có thể dò đáp án và sửa sai được , giá thành mua trên tiki lại rẻ hơn so với trên thị trường nên mình hay mua trên tiki , sẽ ủng hộ tiki dài dài dài dài dài
4
405051
2016-04-29 10:30:36
--------------------------
385498
5865
Về cuốn sách Tổng Hợp Trắc Nghiệm Tiếng Anh Trình Độ A của The Windy - MC Books - Nhà xuất bản đại học quốc gia Hà Nội mình có nhận xét như sau:
Ưu điểm:
- Sách dày hơn 400 trang
- Nội dung phong phú
+ Phần 1 70 đề thi 
+ Phần 2 có 600 câu hỏi trắc nghiệm tổng hợp
+ Phần 3 Key
=> Rất phong phú
- Chất lượng giấy oke,bìa cứng cũng đẹp, mặc dù nhiều trang nhưng cầm không nặng
Nhược điểm chỉ có là sách của MC Book lúc nào giá lúc nào cũng chát mặc dù chất lượng tốt, nếu mua số lượng nhiều quyển thì tổng cũng nhiều tiền thật đấy. Tuy nhiên mua trên tiki được giảm giá nên cũng đỡ đi phần nào :)) Nên mình mua khá nhiều sách ở trên Tiki
5
854660
2016-02-24 06:05:43
--------------------------
336556
5865
Mình đã đặt mua một số sách luyện Tiếng Anh (nghe) của The Windy nên rất tin tưởng vào nhóm tác giả này. Nhân dịp khuyến mãi của Tiki mình đặt cuốn sách trắc nghiệm trình độ A để trau dồi Anh ngữ.
Sách in rõ ràng, bìa quen thuộc với tên nhóm biên soạn The Windy. Giấy dày, chữ đậm rõ. 
Nội dung đa dạng với trắc nghiệm là hình thức chính, có in sẵn đáp án.
Nội dung kĩ tuy nhiên vẫn còn vài sai sót. Mong các tác phẩm khác từ The Windy sẽ hay hơn và ít lỗi sai hơn.
4
477889
2015-11-12 17:32:12
--------------------------
330619
5865
Sách có các câu trắc nghiệm được phân chia thành từng bài rõ ràng, không khó lắm, ở mức cơ bản nên mình có thể làm được. Sách dùng để củng cố kiến thức rất tốt, mình rất thích. 
Bìa thì mình không thích cho lắm vì mình ghét màu vàng. Giấy bên trong tốt, không bị len màu, trăng, dễ nhìn.Mình sẽ mua thêm 1 quyển trình độ b để ôn luyện tiếp. Giá quyển này cũng khá rẻ, hợp túi tiến. Nói chung mình khá ưng ý với sản phẩm này, sẽ tiếp tục ủng hộ tiki nữa.
4
379843
2015-11-02 22:38:38
--------------------------
230522
5865
Mình mua quyển sách này để luyện đề thêm. Nhân xét chung là:
Về hình thức: Cũng như các sách khác của The Windy ; Giấy tốt, không lem màu. Sách trình bày từng phần một khá rõ ràng. Cuối sách có đáp án, mình rất hài lòng.
Về nội dung: Sách trình độ A, nên các bài tập khá cơ bản, sát với đề trên lớp và đề thi trình độ A, nắm chắc kiến thức có thể làm được. Nên làm đi làm lại nhiều lần.
Tóm lại, mình khá thích quyển sách này, đã mua cùng với quyển trình độ B.
4
382812
2015-07-17 15:25:36
--------------------------
200248
5865
Đây là 1 quyển sách tốt về mặt nội dung lẫn hình thức.Sách chỉ gồm các câu trắc nghiệm nhưng được hệ thống đi từ dễ đến khó, nội dung câu hỏi phong phú, không có sự trùng lặp lại như các sách tham khảo khác hiện nay trên thị trường.
Chất lượng giấy thì khá tốt, giấy có độ dày tốt màu chữ in rõ ràng.
Tất cả đáp án của các bộ đề đều được trình bày rõ ràng.Có thể nói đối với những bạn đang trong quá trình ôn luyện thi bằng A Tiếng Anh quốc gia thì nên mua quyển sách này để được làm quen với cấu trúc nội dung cũng như yêu cầu của đề.
với mình đây là 1 quyển sách hoàn hảo
5
368991
2015-05-24 12:52:43
--------------------------
171199
5865
Nghe tên thì đương nhiên đây là một cuốn sách những tuyển tập những đề thi Tiếng Anh trình độ A. Cuốn sách gồm có đề thi Tiếng Anh trình độ A và dường như có nâng cao hơn level A một chút. Tất cả phần đề và đáp án đều được trình bày rõ ràng, đẹp mắt. Mình khuyên các bạn nên mua cuốn này nếu muốn ôn thi chứng chỉ vì nó là cuốn sách đầy đủ duy nhất về các đề tiếng anh trình độ A mình biết và còn có đáp án rất chính xác đầy đủ nữa !
4
501445
2015-03-21 13:51:19
--------------------------
