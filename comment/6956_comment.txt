485679
6956
Bộ sách này gồm 6 cuốn. Mỗi cuốn dạy về các kỹ năng khác nhau cho bé. Ở tập 2 này gồm những thói quen hằng ngày; những lúc ở nhà và ở trường rồi ở nơi công cộng. Dạy cho bé biết tư thế ngồi, đi, đứng, tắm gội, cắt móng tay. Đi học về thì chào hỏi ra sao, cách xưng hô, đồ chơi chơi xong cất thế nào, cách giao tiếp với bạn, đi học đúng giờ....nói chung mình thấy rất bổ ích cho các bé trong độ tuổi mẫu giáo. Hình vẽ đẹp và rõ nét. Các bài thơ dễ thuộc. Bé được trực tiếp tương tác với sách khi cầm bút chọn ra các tình huống đúng. Tiki giao hàng rất nhanh và cẩn thận. 
4
1114461
2016-10-05 11:03:05
--------------------------
356141
6956
Bộ sách Con là bé ngoan gồm 6 tập do Nhà xuất bản Dân Trí phát hành. Tập 2 trong bộ sách này bao gồm 36 trang, có các hình minh họa rất dễ thương. Nội dung của cuốn 2 bao gồm : thói quen hằng ngày (tắm gội, cắt móng tay, tư thế đứng, tư thế ngồi, tư thế đi), ở nhà (đi học và về nhà, cách xưng hô, dùng từ “Mời”, không kén ăn), ở trường (cất đồ chơi gọn gang, giờ ngủ trưa, đi vệ sinh, giao tiếp với bạn, đi học đúng giờ), ở nơi công cộng (bảo vệ hoa cỏ, dạo chơi trong sở thú, ra vào cửa xoay). Mình thích phần Góc trò chơi của sách, giúp bé và bố mẹ tương tác với nhau và giúp bé khắc sâu hơn những phép tắc vừa học.
3
15022
2015-12-21 13:04:06
--------------------------
346872
6956
Mình muốn tìm những quyển sách giáo dục cho bé những việc hàng ngày ở nhà ở trường. Bộ sách "Con là bé ngoan" gồm 6 tập đã thỏa mãn được yêu cầu này.
- Kích thước sách phù hợp với việc cầm đọc
- Giấy trắng
- Màu sắc tươi sáng
- Hình ảnh ngộ nghĩnh thu hút trẻ em
- Các bài học đơn giản, nhẹ nhàng phù hợp với trẻ em nhiều lứa tuổi
Tập 2 này cũng như các tập khác được chia ra thành mấy mục chính, ở đây chia ra 4 mục chính: Thói quen hàng ngày, Ở nhà, Ở trường, Ở nơi công cộng. Trong đó lại được chia làm nhiều mục nhỏ, gần gũi với cuộc sống hàng ngày như: Tắm gội, Cách xưng hô, Giờ ngủ trưa, Bảo vệ hoa cỏ...
Trong từng mục nhỏ đều có câu hỏi và gợi ý giúp bố mẹ, thầy cô giáo đưa ra những câu hỏi liên quan để bé có thể hiểu hơn về nội dung đó.
Bộ sách này rất hữu ích cho gia đình có trẻ nhỏ
5
241211
2015-12-03 15:03:49
--------------------------
260532
6956
Mẹ có thể dùng cuốn sách này để dạy cho bé về những sinh hoạt hằng ngày, việc vệ sinh cá nhân của bé. Giúp bé biết cách ứng xử ở nhà, ở trường và ở nơi công cộng thì nên như thế nào. Với các hình ảnh minh họa rất dễ thương, trực quan. Bé có thể vừa xem tranh, vừa nghe mẹ đọc, và dễ dàng để ghi nhớ các bài học này. Bé được tự do hỏi tại sao phải thế này, tại sao phải thế kia, và hai mẹ con có thể trao đổi với nhau về những thắc mắc của bé. Sách rất hữu ích.
5
476955
2015-08-10 14:59:26
--------------------------
