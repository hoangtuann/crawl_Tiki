484172
3787
Cuốn sách hay, chi tiết, là cuốn cẩm nang tốt để học hỏi.
Trong sách có nêu chi tiết 1 số tình huống cụ thể
Tuy nhiên, sách thiên về đọc vị khách hàng nhiều hơn (tức là nhìn ngôn ngữ cơ thể để đoán xem khách đang có suy nghĩ gì), để tổng hợp lại thì mình đã gạch đầu dòng và ghi những ý chính ra 1 cuốn vở (cũng khá nhiều ý)
Ở 1 số trường hợp thực tế mình đã gặp thì có 1 số điểm không giống hoặc nhìn vậy mà không phải vậy.
Vì thế theo ý kiến của mình, bên cạnh việc kết hợp với đọc sách thì các bạn nên tự tạo 1 cuốn cẩm nang riêng cho mình dựa trên thực tế bạn đã gặp và quan sát được, như vậy sẽ hay và thú vị hơn ^_^
4
557823
2016-09-21 13:53:48
--------------------------
461312
3787
Đối với mình cuốn “Ai hiểu được khách hàng – người đó bán được hàng” – Lý Kiện Lâm là cuốn cẩm nang về tìm hiểu tâm lý khách hàng và bán hàng. Cuốn sách đưa ra những tình huống cụ thể (thường gặp trong đời sống hàng ngày), phân tích yếu tố tâm lý khách hàng trong đó và gợi ý cách để thuyết phục khách hàng mua sản phẩm – dịch vụ của mình. 
Sách được in trên khổ giấy to – dày, chất lượng giấy tốt – chất giấy nhẹ - xốp mà vẫn cứng cáp, giấy màu vàng ngà không hại mắt khi đọc lâu.
4
301718
2016-06-27 18:31:48
--------------------------
447782
3787
Cuốn sách là tập hợp những kinh nghiệm, những chỉ dẫn căn bản về tư duy bán hàng và cách tiếp cận,nắm bắt tâm lý khách hàng. Sách đưa ra nhiều trường hợp cụ thể, gần gũi với đời sống, do đó rất dễ hiểu. Bất kỳ một bạn trẻ nào cũng có thể dễ dàng ứng dụng, không chỉ trong công cuộc kinh doanh nho nhỏ của mình mà còn rút ra kinh nghiệm ứng xử khéo léo trong cuộc sống. Kỹ năng quan sát, thấu hiểu là rất quan trọng. Cuốn sách cần thiết cho bất kỳ ai đang muốn " sống đẹp" trong lòng mọi người.
4
815063
2016-06-14 21:05:40
--------------------------
244991
3787
Đọc và ứng dụng ngay nhé mọi người! Đảm bảo mọi người sẽ thích và ứng dụng ngay. Sách viết ngắn gọn, dễ hiểu. Khách hàng sẽ hài lòng về bạn hơn khi bạn đọc và ứng dụng nó. Tôi cam đoan là như vậy. Sách đi thẳng vào vấn đề, ít lang man. Tôi còn thích thêm 1 cuốn "đừng bao giờ đi ăn một mình". Bạn hãy tìm và đọc nó đi sẽ có ích cho bạn! Tôi húc mọi người 1 buổi tốt tốt lành bên gia đình và người thân! nhâm nhi sách thật sảng khoái nhé!
4
507809
2015-07-28 17:05:38
--------------------------
206560
3787
CUốn sách giúp các bạn có những ứng dụng ngay lập tức, bằng những tình huống cụ thể trong nhiều ngành nghề giúp cho việc tìm hiểu về các đặc tính bán hàng được dễ dàng hơn, tuy nhiên cũng có những phần đọc hơi chán vì những câu đối thoại khá là khoa trương, không có được tự nhiên (kiểu như nịnh nọt) không phù hợp với thục tế cho lắm cũng có thể là do người dịch dùng từ ngữ không chính xác. Càng về sau thì cuốn sách càng hay, để bán được hàng không nhất thiết phải chèo kéo mà phải tạo sự tin tưởng cho khách hàng, rất nhiều tình huống được đưa ra ngắn gọn xúc tích đọc là cảm thấy hưng phấn liền....
4
643960
2015-06-10 08:30:57
--------------------------
200555
3787
Cuốn sách không chỉ cung cấp cho bạn những kĩ năng bán hàng mà nó nói rất sâu về việc hiểu rõ tâm lý khách hàng để bán hàng cho đúng đối tượng, giúp những người bán hàng sẽ không phải lãng phí sức lực vào những người không muốn dùng sản phẩm bạn bán.
Cuốn sách này còn giúp bạn nhìn bề ngoài của người nào đó để biết họ có thể trở thành khách hàng tiềm năng hay không. 
Cuốn sách còn giúp bạn đoán được ai là người có quyền quyết định đến việc mua sản phẩm trong một gia đình đông thành viên.
Thực sự thì cuốn sách khá hữu ích, không viết dài dòng, đi thẳng vào vấn đề qua những tình huống ngắn và thực tế.
4
280007
2015-05-25 12:45:29
--------------------------
