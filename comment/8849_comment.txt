425342
8849
Mua quyển sách này để tặng con trai đầu lòng, cảm giác đầu tiên của tôi khi mở ra là rất đáng yêu! Có những thông tin cơ bản về bé và gia đình để điền vào, còn có chỗ dán ảnh nữa. Hình ảnh minh họa siêu xinh. Tuy nhiên theo tôi thấy thì nhật kí cho bé như vậy là hơi ít. Những sự kiện trọng đại của bé thì đủ rồi nhưng sẽ còn những điều thú vị trong suốt quá trình bé lớn lên nữa. Tôi nghĩ sách nên thêm nhiều trang trống có hình ảnh in chìm để ba mẹ và người thân bé viết thêm.
4
430974
2016-05-04 13:57:38
--------------------------
417921
8849
Mình mới mua cuốn này xong. Mua dùm cho bạn nên được xem ké. Cuốn sách thật nổi bật và đáng yêu, nội dụng thì dễ hiểu và đẹp mắt nữa. Kích thước sách to nên dán hình quá dễ dàng, có điều muốn có nhiều hình thì phải chụp rồi rửa chắc hơi tốn kém phần rửa hình. Nhưng mà ai có điều kiện thì sẽ hoàn thành cuốn này dễ dàng thôi. Sau này bé con lớn lên xem lại chắc sẽ thích và yêu thương ba mẹ của chúng lắm vì đã dày công ghi lại những khoảnh khắc lớn dần lên cảu con như thế. Quả là thời bây giờ có đủ mọi thứ để ghi lại những kỷ niệm của cuộc đời. Thật tiện lợi.
4
169455
2016-04-18 09:57:43
--------------------------
394897
8849
Tôi đang băn khoăn không biết chọn kiểu nhật ký nào dễ thương và không nghĩ ra cách viết nào sinh động cho con lưu lại quãng thời gian ấu thơ. Thì tôi search được cuốn sách này thỏa mãn các yêu cầu tôi đang tìm kiếm. Cuốn sách gợi ý cho tôi lưu lại những khoảnh khắc gì của bé, có cả chỗ để lưu lại những tấm hình đáng yêu của bé, vừa đẹp về hình thức để sau này bé con đọc lại như 1 cuốn truyện nhiều màu sắc về mình. 
Cuốn sách giá cả hợp lý, nội dung hay và hình thức đẹp.
4
616843
2016-03-11 08:09:54
--------------------------
346729
8849
Bắt gặp quyển nhật ký này tôi liền đặt 2 quyển, một cho bé bi nhà tôi và một làm quà tặng cho chị đồng nghiệp cũng mới sinh em bé. Khi nhận hàng tôi rất hài lòng vì nét vẽ đẹp, màu sắc tươi sáng. Khi lật đến trang cuối tôi vẫn còn tiếc vì cuốn nhât ký không dày hơn nữa. Ngoài ra, cuốn nhật ký phù hợp hơn để ghi thông tin trong năm đầu tiên của bé. Nhưng không sao, ngoài cuốn nhật ký này còn khá nhiều cuốn khác để tôi dán thêm ảnh, viết những lời nhắn nhủ hay ghi chép lại cuộc sống của con trong những năm đầu đời :)
4
82987
2015-12-03 11:01:09
--------------------------
299598
8849
Tôi rất hài lòng khi mua sản phẩm này. Sách có bìa dầy, đẹp, chất lượng giấy tốt. Phần nội dung được biên tập kỹ, font và kích thước chữ to, dễ đọc. Hình ảnh minh họa phong phú, dễ thương, phù hợp với trẻ nhỏ. Quyển nhật ký này giúp tôi ghi lại một số thông tin quan trọng trong những năm đầu đời của con, đi kèm là những hình ảnh đáng yêu của bé.  Đây giống như một quyển album, một món quà thật đặc biệt dành riêng cho con. Mỗi dòng được ghi, mỗi tấm ảnh được dán vào, tôi thấy mình như càng gần con hơn, càng yêu con nhiều hơn.
5
109583
2015-09-13 13:19:24
--------------------------
237464
8849
Có lẽ đối với bố mẹ, nhất là những người lần đầu như mình, sẽ có rất nhiều kỉ niệm muốn lưu giữ cho con ở những năm tháng đầu đời. Cuốn sách này là một cuốn nhật kí được hệ thống khá đẩy đủ các thông tin cho bố mẹ điền vào (bao gồm cả ảnh) sẽ giúp bố mẹ lưu giữ được những kỉ niệm thơ ấu của bé cũng như những người xung quanh bé.
Một cuốn sách hay, và mở, vì đối với mỗi cặp bố mẹ, cuốn sách sẽ biến thành một phiên bản riêng cho mỗi bé
5
705833
2015-07-22 14:40:36
--------------------------
213821
8849
Quyển này mình mua hộ chị dâu  thôi những bả cũng đem khoe rồi, đẹp lắm!
Trang nào cũng đẹp ấy mà giấy sờ rất thích, quyển này được biên tập từ nước ngoài cho nên mình thấy nội dung rất được, chi tiết từng giai đoạn một của đứa bé mới lớn, thiết nghĩ sau này bé mà xem lại chắc chắn sẽ rất vui cả tự hào vì bố mẹ tỉ mẩn như vậy.
Vì là vợ chồng mới cưới nên a/c rất quan tâm đến từng chút một sự thay đổi của đứa đầu lòng, quyển này đã được điền một nửa rồi, vừa có dấu ấn của bé, của bố mẹ lại cả ông bà cô chú nữa. 
Kiểu nhật kí này không phải mới nữa nhưng theo mình thấy thì quyển này rất ổn, về cả mặt hình thức, chất lượng lẫn nội dung
5
496784
2015-06-24 10:11:59
--------------------------
