458857
7809
Mình mua "Hẹn" trong một bộ nhưng cảm thấy "Hẹn" lại thiếu thú vị so với các quyển khác. Nội dung truyện cũng thường không để lại ấn tượng vì mình đã đọc rất nhiều rất nhiều truyện ngắn được viết theo kiểu này rồi. Mình nghĩ tác giả đã quá đi vào khuôn khổ của các truyện ngắn hiện nay, vì vậy tác giả nên có một cái nhìn khác lạ hoặc đổi mới cốt truyện.
Điểm cộng là bìa sách đẹp, nhỏ nhắn, dễ thương với lại giá cả rất mềm nên mình mua về làm quà tặng bạn. 
3
1157252
2016-06-25 15:46:58
--------------------------
402715
7809
Ngay từ đầu mình rất ấn tượng với bìa sách và tựa đề nên đã quyết định mua về đọc, Câu chuyện rất hợp với tình cảnh tâm trạng của mình lúc này, mình thực sự rất muốn mua đủ trọn bộ nhưng rất tiếc lại hết hàng không biết lúc nào mới có, nội dung sách thực sự rất hấp dẫn và đáng yêu... Rất thích hợp để làm quà tặng cho bạn bè, vừa hay dễ thương, lại đáng yêu nữa, mong tác giả sẽ cho ra đời nhiều cuốn sách hay như thế này nữa.Cảm ơn tiki rất nhiều
5
900340
2016-03-22 20:22:10
--------------------------
399298
7809
Một cuốn truyện khá là hay. Cuốn sách được thiết kế vô cùng đẹp mắt, với bìa ngoài của cuốn sách  là một cậu bé trên tay cầm bó hoa và khuôn mặt của cậu ta thì vô cùng tâm trạng. Giá của cuốn sách này rất phù hợp với túi tiền của tôi, có lẽ đây là cuốn truyện rẻ nhất trong giá truện của tôi ở nhà. Nhưng không vì thế mà câu truyện này không hay đâu nhá. Nội dug của cuốn sách vô cùng thú vị, nó đem lại cho tôi nhiều cung bậc cảm xúc khác nhau, để tôi được trải mình cùng với câu chuyện. Có vẻ như câu truyện cũng làm cho tôi có một chú gì đó thất vọng. Nhưng nó làm tôi cảm thấy thú vị nhiều hơn . Đây là một câu truyện hay và ý nghĩa.
3
568619
2016-03-17 15:43:20
--------------------------
395664
7809
Cuốn sách ngàn lẻ một độ hẹn trong bộ sách ngàn lẻ một độ nói về những câu chuyện ngắn về những cuộc hẹn, trong tình yêu thì những cuộc hẹn hò làm cho hai người yêu nhau thêm khắn khít, có những cuộc hẹn vui vẻ ngọt ngào, cũng có những cuộc hẹn buồn bã cãi vả nhưng tất cả các cảm xúc đó đều tạo nên những gia vị khác nhau trong tình yêu. Sách nhỏ gọn dễ thương, bìa thì đẹp nhìn là yêu ngay. Giấy tốt và đóng chắc chắn, giá cả thì rẻ vô cùng.
3
851304
2016-03-12 11:05:20
--------------------------
336682
7809
Một bộ sách nhỏ gọn, xinh xắn và ảnh bìa thì rất bắt mắt độc giả. Nếu xếp cả 4 cuốn sách chồng lên nhau và nhìn vào thanh ngang, chắc chắn ai cũng sẽ nhận ra một từ tiếng anh khá quen thuộc "LOVE", đã nói lên hầu hết tất cả các mẩu chuyện ở trong đó. Hẹn hò thật sự từ hai phía hay chỉ từ một phía, hẹn giả vờ hay giống như giả vờ Hẹn như đã từng mong đợi hoặc hoàn toàn khác với "kịch bản" đã vẽ ra...... Không khác gì cuốn Cảm, cuốn Hẹn cũng vậy nói hết lên những câu chuyện ngỡ như ở đời thực cũng có. Những nét vẽ khá đơn thuần thậm chí chẳng quá đặc sắc, mà nó lại nói lên tâm tình, cảm xúc của những cặp đôi mới yêu hay đang cảm nắng của lứa "tuổi teen" ngày nay.
3
922089
2015-11-12 20:12:49
--------------------------
310464
7809
Một cuốn sách được thiết kế vô cùng dễ thương, bìa đẹp và bắt mắt, khổ sách nhỏ nhỏ xinh xinh. Cộng thêm giá mềm, hợp túi tiền của mình. Không vì thế mà nó không hay. Nội dung cuốn sách rất thú vị. Mỗi câu chuyện lại đem cho mình những cảm xúc khác nhau. Đôi lúc là vui, thỉnh thoảng lại man mác buồn, và thậm chí cũng có một ít thất vọng. Một cuốn sách giàu cảm xúc. Không chỉ có cuốn sách này thôi đâu. Mà cả bộ sách Ngản Lẻ Một Độ cũng đều vô cùng là thú vị, hợp với phong cách teen của chúng mình. 
4
365697
2015-09-19 16:12:29
--------------------------
295903
7809
Mình đã quyết định mua bộ sách "ngàn lẻ một độ" này vì mình thấy bìa sách cũng rất dễ thương và cuốn sách cũng nhỏ gọn. Giá sách cũng rất mềm và rất phù hợp túi tiền. Quyển sách 'hẹn' này kể về các câu truyện teen rất đáng yêu, có những câu truyện rất vui, nhưng cũng có những câu truyện mình cảm thấy hơi tiếc. Nhưng có những câu truyện có kết mở thì mình không thích lắm. Nhưng nói chung là mình cũng rất thích quyển sách này vì nó đã mang lại cho mình rất nhiều cảm xúc.

5
729854
2015-09-10 18:52:12
--------------------------
247529
7809
Mình quyết định mua " Bộ sách ngàn lẻ một độ - Hẹn" này ngay từ cái nhìn đầu tiên bởi vì bìa sách được thiết kế cực kì đẹp và dễ thương, giá cả cũng mềm nữa . Nhưng đến lúc mua về thì mình thấy rằng mình đã không lãng phí tiền bạc chỉ để ngắm bìa thôi. Nội dung của cuốn sách này còn thực sự hấp dẫn nữa!  Về cuốn Ngàn lẻ một độ Hẹn này thì sách đã mang đến cho mình mẩu truyện tuy không dài nhưng rất lôi cuốn và rất phù hợp với tuổi teen. Có những truyện vui nhưng cũng có những truyện buồn man mác. Nhưng tất cả đều mang đến cho mình những cảm xúc như khi đang hẹn hò vậy! Mình thích nhất  mẩu truyện " Anh em song sinh", một câu chuyện nhẹ nhàng nhưng thu hút và kết thúc truyện mình cũng rất  hài lòng. Nói chung là ình không chê vào đâu được!
5
306288
2015-07-30 10:36:00
--------------------------
240159
7809
Khi mà chưa đọc cuốn này điều ấn tượng đầu tiên là bìa sách, rất đẹp, phải nói là cực đẹp. Không chỉ bìa của "hẹn" mới đẹp đâu, mà cả bộ chuyện ngàn lẻ một độ, bìa cuốn nào cũng đẹp hết à. Sách lôi cuốn mình bởi những câu chuyện teen teen vô cùng đáng yêu. "Anh em song sinh" thật mềm, man mác buồn. "Nhỏ ngốc" hay hay, thú vị... mỗi chuyện một vẻ, một cảm xúc. Nhưng điểm chung là rất hay và vừa ý mình. "Hẹn như đã từng mong đợi hoặc hoàn toàn khác với "kịch bản" đã vẽ ra..." thật thú vị...
5
543425
2015-07-24 12:56:48
--------------------------
219948
7809
Ngàn lẻ một độ nhớ thật là một cuốn sách hay và dễ thương. Điều đầu tiên mà mình thích nhất là bìa sách vẽ rất là đẹp nhé tên sách thật đặc biệt và cuốn sách này nhỏ xíu à bé bé xinh xinh có thể bỏ túi mang đi khắp nơi. Những truyện ngắn trong này đều hay và mang thông điệp ý nghĩa vì do chị Dung Keil chắp bút mà. Từng lời văn nhẹ nhàng, đôi khi dậy sóng nhưng dễ dàng thấu hiểu và tìm ra trong đó hình ảnh của mình, mang lại cho người đọc rất nhiều cảm xúc. Hay!
5
418341
2015-07-02 08:59:35
--------------------------
198436
7809
Cuốn "Hẹn" của chị Nguyệt là một cuốn sách rất hay.  Sách nhỏ nhắn nên trông rất...dễ thương !!! Chị Nguyệt viết truyện ngắn với nội dung rất rất rất phong phú. Và trong cuốn này cũng vậy !
Từ câu chuyện đầu tiên là "Anh em song sinh", tớ đã thấy rất rất có cảm xúc, nó nhẹ nhàng, cái kết mở (đúng chuẩn chị Nguyệt) làm tốn nơ-ron tưởng tượng của tớ cực TvT
Ba câu chuyện tiếp theo cũng thế.
Nhưng khi đến với truyện "Người mẫu ảnh", "Mảnh vỡ thinh lặng" thì lại mang đến một cảm giác buồn, hụt hẫng. Nhưng rất sâu, rất thấm...
Còn những câu chuyện tiếp nữa thì...đáng yêu hết sức !
"Hẹn" lần lượt mang đến cho tớ nhiều cảm xúc khác nhau, vui có, buồn có. Rất hay, rất thú vị !
...
Một điều rất đặc biệt, chị Nguyệt cũng đáng yêu y như truyện của chị ấy vậy ! Không tin bạn cứ thử nói chuyện với chị ấy đi =)))
5
547934
2015-05-19 22:06:37
--------------------------
165098
7809
Hẹn hò là giai đoạn quan trong nhất trong tình yêu. Đây là khoảng thời gian để hai con người gần gũi, tiếp xúc và hiểu nhau nhiều hơn. Cảm xúc thăng hoa cũng chính từ những cuộc hẹn hò. Tác giả thật khéo léo khi đưa vào đây những câu chuyện hẹn hò của tuổi học ngây ngô, trong sáng. Những vụng dại, những sai lầm, những thấu hiểu đã khiến người đọc không ngừng xúc động. Thật tuyệt khi nhớ lại những kỉ niệm đó, ngốc nghếch vô cùng. Hi vọng trong tương lai, những bộ sách như vậy sẽ nhiều hơn và phổ biến hơn.
Bìa sách rất đẹp, xinh xắn nhưng khổ sách lại hơi nhỏ.
4
198460
2015-03-09 18:38:27
--------------------------
