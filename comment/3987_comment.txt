293722
3987
Lúc nhận được sách mình khá bất ngờ vì khổ to hơn mình nghĩ. Bìa sách thiết kế đẹp mắt, hình minh hoạ các trò chơi bên trong khá sinh động, dễ thương kích thích bé ham học hỏi. Sách có nội dung giáo dục, giúp trẻ phát triển khả năng tư duy, quan sát, đánh giá xem xét các tình huống được đưa ra. Bé nhà mình rất thích và chăm mở sách đọc hơn hẳn dù mới 5 tuổi. Giá cả hợp lí. Sách thích hợp để phụ huynh cùng bé học tập, đồng hành cùng con cái trong quá trình phát triển của bé.
4
484159
2015-09-08 16:27:57
--------------------------
