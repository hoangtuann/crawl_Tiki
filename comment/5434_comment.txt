212472
5434
Hiện nay, rèn kĩ năng sống cho các em học sinh là nhiệm vụ không kém phần quan trọng và được toàn xã hội quan tâm. Bởi so với thời trước thì có lẽ hiện nay đạo đức của các em đang đi thụt lùi dần, sự yêu thương kính trọng cha mẹ, thầy cô, bạn bè, văn hóa lịch sự hòa nhã trong giao tiếp, đi thưa về trình... dường như rất hiếm thấy ở các em thay vào đó là luôn đặt cái tôi của mình lên trên hết, hễ đụng chuyện chút xíu là gây gỗ, đánh nhau. Bộ sách rèn kĩ năng sống của tác giả Nguyễn Khánh Hà ra đời đã hỗ trợ rất lớn cho cha mẹ, thầy cô trong việc giáo dục nhân cách cho các em, mỗi tựa sách rèn một kĩ năng cần thiết cho các em. Những câu chuyện gần gũi mang tính giáo dục cao dễ đi vào lòng các em giúp các em từ từ thay đổi mình.
5
455308
2015-06-22 14:33:37
--------------------------
