491546
4674
+ Hình thức: ngay lập tức cuốn hút mình. Đơn giản, dễ thương và truyền tải được thông điệp
+ Nội dung: logic, dẫn dắt và đem lại một cái nhìn tổng quát (từ quá khứ - hiện tại - tương lai cũng như mặt lợi hại và cái nhìn đa chiều) về hiện tượng nóng lên toàn cầu. Cuốn sách đem đến rất nhiều kiến thức hay và bổ ích. Bên cạnh đó cuốn sách cũng đưa ra các từ khóa để người đọc có thể tự tìm hiểu thêm. Cuốn sách như một hồi chuông cảnh tỉnh. Mình rất thích nó, mặc dù trước đây cũng biết về hiện tượng nóng lên toàn cầu nhưng kiến thức còn mơ hồ thì nay nó làm mình canh cánh trong lòng về việc Trái Đất ngày một nóng hơn và thôi thúc mình có một thói quen xanh hơn.
+ Một điểm mình mong muốn thay đổi là phải chi cuốn sách có thể chi tiết hơn nữa thì thật tuyệt biết bao.
5
1455677
2016-11-18 22:28:49
--------------------------
331186
4674
Theo tình trạng hiện nay thì Trái Đất đang bắt đầu nóng lên một cách đột ngột. Nguyên nhân là do khí cacbon đioxit bắt đầu gia tăng và tích tụ lại trên bầu không khí. Theo ta biết , bức xạ nhiệt của mặt trời là một bức xạ có sống ngắn nên dễ dàng xuyên qua tầng ozone và CO2 đến Trái Đất. Còn bức xạ của Trái Đất ra vũ trụ thì là một bức xạ có sóng dài nên dễ dáng bị hấp thụ lại tầng CO2. Vì thế mà Trái Đất bắt đầu nóng lên. Đồng thời , do khí hậu nóng gia tăng, băng bắt đầu tan ra dẫn đến nước dâng gây ngập lụt. Những con ngươi không có ý thức còn chặt phá cây ,trong khi đó thì cây là nguồn chuyển đổi CO2 thành O2 duy nhất trên Trái Đất.
Cuốn sách này sẽ giúp chúng ta hiểu thêm và biết ý thức không góp phần làm Trái Đất ngày càng nóng hơn.
HÃY CHUNG TAY BẢO VỆ TRÁI ĐẤT ĐỂ CUỘC SỐNG TA THÊM TƯƠI ĐẸP HƠN !!!
4
816167
2015-11-04 07:00:56
--------------------------
223122
4674
Hàng ngày trên các phương tiện truyền thông, người ta hay đề cập đến "sự ấm lên toàn cầu", và có lẽ it có ai biết rõ vấn đề này như thế nào và sự ảnh hưởng của nó nghiêm trọng ra sao.
Quyển sách nhỏ nhắn này cho chúng ta cái nhìn toàn diện về sự nóng lên của trái đất, nguyên nhân hình thành, lịch sử phát triển của nó và các nguy cơ mà các loài sinh vật trên trái đất trong đó có loài người đang phải đối mặt. Một trong các tác động rõ ràng nhất của sự ấm lên toàn cầu là mực nước biển dâng cao, trong 10 thành phố có nguy cơ cao nhất bị nước biển nhấn chìm thì Việt Nam có 2 thành phố là Hải Phòng và TPHCM!
Hãy hiểu biết và hành động trước khi quá muộn.
5
462641
2015-07-06 16:48:24
--------------------------
