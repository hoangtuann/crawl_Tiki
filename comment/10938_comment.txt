456895
10938
Mình mua quyển này cùng lúc với quyển Thám tử Freddy, và được sale còn 30.000. Thật sự thì đối với mình, quyết định mua nó là không sai lầm một tí nào. Nội dung rất hay, có những tình tiết rất vui nhộn. Hình minh họa trong truyện rất đẹp (theo quan điểm cá nhân của mình là vậy), bìa nhám, cầm rất êm tay. Trong tập này, Freddy đã phải làm rõ chân tướng của một kẻ được đồn là Kẻ mặt mịt, nhưng thật sự lại là Simon và bè lũ của lão. Thích nhất là lúc đánh nhau ở gần cuối, cười không ngậm được mồm luôn.
5
502455
2016-06-23 20:58:06
--------------------------
318385
10938
Tiếp nối thành công của tập 1, tập 2: "Freddy và Kẻ Mặt Mịt" lại đem đến cho độc giả những chuyện phá án thú vị của chú heo thám tử Freddy. Truyện mở đầu dí dỏm theo cách nhẹ nhàng, tinh tế rồi dần cuốn hút bạn đọc vào những tình tiết hồi hộp, bí ẩn nhưng lại chẳng hề nặng nề, căng thẳng mà diễn ra hết sức tự nhiên, thơ ngây, sinh động. Tác phẩm còn truyền tải nhiều thông điệp ý nghĩa về tình bạn, lòng can đảm và sự đoàn kết. "Freddy và Kẻ Mặt Mịt" thật hay và chẳng kém phần nhân văn.
5
6502
2015-10-05 21:12:57
--------------------------
234055
10938
Một câu chuyện trinh thám dành cho thiếu nhi rất dễ thương nhưng cũng thú vị đối với người lớn. Thế giới trong câu chuyện được miêu  tả hóm hỉnh, sinh động và hấp dẫn. Gà Charles,bò Wiggins, chuột cống Simon, mèo Jinx được nhân cách hóa sinh động như con người. Chú lợn Freddy phải trổ tài thám tử của mình để điều tra vụ án về kẻ mặt mịt bí ẩn dù chưa có ai nhìn thấy hình thù của kẻ dấu mặt này. Cả nông trại phải đoàn kết để chống lại kẻ thù bí ẩn xấu xa. Cuốn sách là cuốn tiểu thuyết phưu lưu phù hợp cho lứa tuổi thiếu nhi.
4
198406
2015-07-20 08:41:54
--------------------------
