519373
6288
Không biết có phải do mình đọc nhiều sách viết về nội dung tương tự nên làm mình hụt hẫng hay không. Những lời khuyên đưa ra hầu như mình đều được biết đến, lời văn chưa chạm được đến cái tôi của mình. Những trang đầu làm mình vô cùng hứng thú, nhưng từ từ cũng mất dần cảm xúc vì nội dung dàn trãi. Ưu điểm duy nhất mình thấy được là việc tác giả phân chia chủ đề, phương pháp một cách rõ ràng và khá chi tiết, hướng dẫn cách làm cụ thể hơn các sách khác. Nhưng cũng cảm ơn tác giả đã giúp mình xác định lại 1 lần nữa về ước mơ là điều có thể thực hiện. Cảm ơn
2
772352
2017-02-05 17:30:36
--------------------------
496356
6288
Cuốn này hơi dài dòng. mà đưa dẫn chứng về gia đình của ông ấy khá là nhiều @@
3
768753
2016-12-17 13:15:49
--------------------------
474901
6288
Mình đã đọc vài quyển sách của TMG rồi.
Và mình nhận xét quyển này như sau ( có lẽ hơi khác hơn các bạn ở dưới)
Ưu điểm: Sách đẹp, giấy dày, trình bày đẹp, có nhiều câu chuyện minh họa, thể hiện nhiều cảm hứng của tác giả mong muốn đem lại thành công cho mọi người, các phương pháp và các bước dễ hiểu, áp dụng được.
Nhược điểm : Nếu như đọc nhiều sách kỹ năng của TMG thì có lẽ quyển sách này chưa tạo nhiều ấn tượng cho mình, vì mình thấy dài và hơi lan man. Mình thích quyển Tôi tài giỏi và Làm chủ tư duy hơn :D Các bạn có thể đọc bản đọc thử hay xem online trước khi mua nhá. Đây là ý kiến các nhân của mình.
3
455233
2016-07-12 08:54:25
--------------------------
463752
6288
Những Bước Đơn Giản Đến Ước Mơ. Một cuốn sách dành cho những ai đang mong muốn cũng như đang trên bước đường chinh phục những ước mơ hoài bão của mình. Steven K. Scott đã đưa ra những dẫn chứng đơn giản giúp cho chúng ta hiểu được những sợi dây xích ngăn cản chúng ta đến với thành công và những phương pháp cắt đứt sợi dây xích đó, kích hoạt những tiềm năng to lớn trong chính chúng ta để từng bước thực hiện ước mơ của mình, bìa sách đẹp, chất lượng giấy tốt không chê vào đâu được.
5
582479
2016-06-29 16:18:36
--------------------------
459943
6288
Một cuốn sách có bìa đẹp mắt và ấn tượng chứa đựng nhiều bí quyết thành công  của nhiều người trên thé giới được tác giả gắn kết lại với giọng văn hiện đại và lôi cuốn. Đây là cuốn sách dạy kỹ năng sống với nhiều kiến thức rất, với loại sách này, chúng ta nên dành thời gian để dọc và chim nghiệm, thậm chí đọc lại nhiều lần để có thể hiểu và áp dụng hiệu quả trong cuộc sống. Qua cuốn sách tuyệt vời này, tác giả Steven K. Scott đã truyền tải đến chúng ta một thông điệp rất ý nghĩa: hãy cứ mơ ước và kiên trì với ước mơ của mình, tất cả đều không quá khó để biến ước mơ thành hiện thực thậm chí là đơn giản nếu ta biết thực hiện đúng cách.
5
310872
2016-06-26 17:40:18
--------------------------
441052
6288
Mình là một người đang khát khao thực hiện ước mơ và cuốn sách này gãi đúng cỗ ngứa của mình. Không chỉ biết kích hoạt những động cơ cần thiết để hiện thực hóa điều mình muốn mà còn chuẩn bị đầy đủ trước cả khi hành động như vượt qua những quan niệm rằng bản thân tầm thường, thiếu tầm nhìn rõ ràng, thiếu kiến thức, điều kiện.
Mình có biết về luật hấp dẫn và những tác hại do tiềm thức ngầm sai lầm gây ra. Phần đầu sách theo mình cực kì hữu dụng và nên nghiêm túc trước khi tiến vào các phần hiện thực hóa.
Cuốn sách sẽ là cẩm nang hướng dẫn mỗi khi mình muốn thực hiện điều gì đó mà không ràng buộc bởi những quan niệm hạn chế của xã hội.
5
153783
2016-06-02 18:52:36
--------------------------
434064
6288
Cuốn sách này đến với tôi ngay lúc cấp thiết nhất "Tôi đang chuẩn bị thi chuyển cấp và chẳng còn chút hi vọng nào để học hành cả". Steven đã cho tôi thêm năng lực, niềm tin. Tôi đã áp dụng tất cả các Nghệ Thuật Hiện Thực Hóa Ước Mơ của ông không những vào việc học tập mà còn trong các mối quan hệ gia đình, bạn bè. Những phương pháp của ông đã khiến nhiều người thay đổi hoàn toàn. Chúng ta ai cũng là chiếc Saturn V, với các bộ thiết bị siêu việt ( bộ não) nhưng chúng ta lại tự kiềm hãm khả năng tuyệt vời trong mỗi chúng ta, hãy loại bỏ xiềng xích như Sten K.Scott.
Hãy thay đổi cuộc đời!
5
1209601
2016-05-22 10:51:29
--------------------------
432962
6288
Mình chưa từng thất vọng khi mua những sách do TGM xuất bản vì các đầu sách đều được chọn lọc kỹ càng, nội dung chất lượng, trình bày rõ ràng và đẹp mắt nội dung rất sát thực tế có kèm bài tập thực hành vì vậy cuốn sách rất hữu ích cho những ai đã ấp ủ những giấc mơ mà chưa từng thực hiện được, sách tạo nhiều động lực cho người đọc, quan trọng nhất vẫn là  cách chúng ta tư duy từ th đó dẫn đến thành công hay thất bại, trong số những trang web mua hàng trực tuyến thì tiki.vn là nơi mình cảm thấy hài lòng nhất. 
4
713046
2016-05-20 09:12:30
--------------------------
420692
6288
Từng đọc một cuốn sách của bác Trần Đăng Khoa và thấy nhắc đến cuốn sách này nên tôi đã mua nó. Quả thực là đáng tiền vô cùng vì đây là một cuốn sách hay và thiết thực cho cuộc sống thực tế của mỗi người. 
Cuốn sách chỉ ra những điểm chưa đúng trong cách suy nghĩ, cách làm việc từ những điều nhỏ nhất trong cuộc sống. Tôi đã thay đổi suy nghĩ rất nhiều sau khi đọc được cuốn sách này. Không nói đến những gì quá to lớn và mang tầm vĩ mô, cuốn sách tập trung vào những tiểu tiết nhỏ nhặt trong cuộc sống hàng ngày hơn. 
Mọi người và đặc biệt là những người trẻ nên tìm đọc cuốn sách này.
4
281738
2016-04-23 18:28:26
--------------------------
407664
6288
Đây là một quyển sách cực kì hay. Có những lúc lòng ta chông chênh không biết nên làm gì và đi về đâu. Đọc quyển sách này giúp ta nhận ra rằng ta luôn mắc phải sợi dây xích tầm thường, tầm thường cả trong suy nghĩ lẫn hành động vì ta luôn sợ phải thất bại và đối mặt với thất bại. Cuốn sách bên ta giúp ta từng bước bộc phá ra khỏi sự hèn nhát của chính bản thân. Không chỉ vậy nó còn dạy cho ta biết phấn đấu đối mặt và quản lí thời gian một cách chi li. Có nói bao nhiêu cũng không thể tả được động lực mà cuốn sách mang lại chỉ tóm tắt lại là sách "hay phết nhở"
4
858908
2016-03-30 15:37:46
--------------------------
396101
6288
Tôi đọc Sống và Khát vọng của bác Trần Đăng Khoa thấy nói về quyển này nên có ấn tượng khá tốt. Rồi tình cờ bạn tôi đọc nên tôi quyết định rinh ngay ẻm về.

Bìa em siêu đẹp, nội dung trình bày đẹp mắt, dịu. 

Nhân nhi cùng tách cà phê, thấy được những điều mới lạ. Về những khát khao, ước mơ đã bị chôn vùi trong tiềm thức bởi xã hội xung quanh, bởi sự thiếu tự tin về bản thân, vễ những giới hạn mà mình tự đặt ra. Tôi bỗng thấy thì ra tôi từng ước mơ như vậy! Và, sẽ cố.
4
943099
2016-03-12 22:16:54
--------------------------
385457
6288
mình mua cuốn sách này được 2 tháng rồi. trong những tháng thất nghiệp ở nhà, mình rất chán nản, không muốn tiếp xúc với bất kỳ ai. Mình suốt ngày chỉ ru rú ở nhà, lướt web, xem phim,...và đọc sách. Mình tìm đến tiki với mong muốn tìm được một cuốn sách có thể thay đổi được suy nghĩ tiêu cực của bản thân. Và mình đã tìm thấy cuốn sách này. Đây là cuốn sách chứa đựng một kho tàng quý giá về ước mơ và các bước đạt được ước mơ. Nó đem đến niềm tin, khích lệ tinh thần thực hiện ước mơ của mình. Cám ơn Tiki, cám ơn TGM.
5
393766
2016-02-23 23:53:29
--------------------------
383293
6288
Ước mơ cũng là thứ mà nhiều chỉ dám mong ước, chỉ dám nhìn nó từ đằng xa như một cái gì đó đẹp đẽ mà không vùng lên chạy hết sức đến nó. Khi lớn lên, ước mơ đẽ bị gác lại một góc để tập trung vào những mối lo khác trong cuộc sống, chúng ta có thể quên mất chúng, hoặc bị buộc phải quên, vô số trải nghiệm mới đem chúng ta vào thử thách về độ cao của những khó khăn của chúng ta. Vì thế đừng để nó bay đi, hãy học cách đến với nó. Cuốn sách này sẽ giúp bạn.
5
828378
2016-02-20 14:42:09
--------------------------
379883
6288
Là cuốn sách hữu ích và hay nhất trong những cuốn sách tôi từng đọc! Nó giúp tôi thay đổi suy nghĩ và cách học tập cũng như làm việc trong cuộc sống. Những phương pháp mà STEVEN K.SCOTT đưa ra thật không khó để thực hiện nó nếu bạn có niềm tin và kiên trì theo đuổi. Cảm ơn dịch giả Trần Đăng Khoa và Uông Xuân Vy đã đem đến cho thế hệ trẻ Việt Nam cuốn sách ý nghĩa này! Cảm ơn tập đoàn TGM BOOKS và Nhà xuất bản Phụ Nữ đã đồng hành cùng bạn đọc trong thời gian qua !
5
750331
2016-02-12 23:45:44
--------------------------
376014
6288
Thật tuyệt vời khi cầm trên tay một cuốn sách chỉ cho bạn biết những điều tưởng chừng như đơn giản nhưng lại giúp bạn từng bước từng bước đến với những mơ ước của mình! Đó là những kinh nghiệm  hay giống như là một cuốn kim chỉ nam chỉ cho bạn cách để khơi dậy và nuôi dưỡng niềm đam mê, cháy hết mình từng giây từng phút một. Ước mơ chính là động lực giúp con người ta vươn tới đỉnh cao, cũng giúp tiếp thêm cho chúng ta nguồn năng lượng cần thiết để sống cuộc sống một cách thực sự ý nghĩa va hãy chân trọng những ước mơ của mình và đừng bao giờ từ bỏ nó!
5
733791
2016-01-29 20:05:00
--------------------------
345088
6288
Thực sự đây là một cuốn sách có nội dung hay, dễ hiểu. Sách nói về những điều nên làm và nên tránh để đạt được ước mơ của mình.
Đâu đó trong cuộc sống thì có lẽ chúng ta cũng đã thực hiện một số điều như sách đề cập. Sau khi đọc sách này tôi đã hệ thống lại cách làm của mình. 
Hiện tại thì chỉ có thể nói là sách hay thôi, còn để nói sách tuyệt vời thì phải đánh giá về kết quả khi áp dụng theo sách này.Tôi sẽ áp dụng sau 1 năm nữa sau khi áp dụng nhé!
5
293383
2015-11-29 19:38:22
--------------------------
339208
6288
Quyển sách không đưa bạn đến ước mơ hão huyền mà là đến ước mơ thực sự. Chắc chắn rằng bạn phải áp dụng công thức cho sự thành công vào cuộc sống mới đem lại thành công :). Lối viết lấy vd đơn giản, phân tích dễ hiểu, dễ thực hiện, dễ áp dụng. Chất lượng sách tốt, có hình ảnh dễ mường tượng. Phải áp dụng vào thực tế từ từ mới được. Nói chung, đây là cuốn sách khó lòng bỏ qua được nếu bạn muốn chạm vào ước mơ - dù là phi thực tế ở hiện tại :D
4
936607
2015-11-17 16:37:12
--------------------------
337632
6288
Cuốn sách mang đến những gì thực tế và cực kỳ gần gũi với đời sống thường ngày của chúng ta, nghe thì nhưu là câu chuyện cổ tích nhưng những hướng dẫn trong quyển sách Những Bước Đơn Giản Đến Ước Mơ sẽ giúp bạn biến ước mơ của mình thành hiện thực!
Nếu bạn chưa rõ lối đi đến thành công thì cuốn sách theo mình là lựa chọn hoàn hảo, mỗi cuốn sách tiki mình mua mình đắn đo suy nghĩ rất nhiều và đây là 1 trong những cuốn dẫn mình đến thành công
một quyển sách nhìn rất đơn giản bên ngoài, nhưng bên trong là một thế giới hoàn toàn khác, đọc cuốn sách bạn sẽ có định hướng rõ ràng cho tương lai
5
604569
2015-11-14 13:22:27
--------------------------
310319
6288
Mình rất ngưỡng mộ 2 dịch giả Trần Đăng Khoa và Uông Xuân Vy, đặc biệt rất thích đọc các sách của TGM, vì các đầu sách đều được chọn lọc kỹ càng, nội dung chất lượng, trình bày rõ ràng và đẹp mắt, chỉ có 1 điều khiến mình không hài lòng đó là sách hơi nặng nhưng cũng không sao. Cám ơn TGM và các dịch giả đã mang đến 1 cuốn sách hay, nội dung của sách tuy chỉ là những phương pháp cực kỳ đơn giản nhưng rất hiệu quả, giúp cho mình vượt qua được những nỗi sợ hãi, sợ thất bại và thêm tự tin hơn vào sự lựa chọn của mình. Nhờ đó mà mình cũng biết thêm về tác giả Steven K. Scott, khiến mình vô cùng ngưỡng mộ và khâm phục ý chí của ông.
5
515497
2015-09-19 14:33:50
--------------------------
283900
6288
Phải nói là những cuốn sách từ TGM BOOKS luôn là những cuốn sách tuyệt vời, từ cách trình bày đến nội dung. Một cuốn sách với nội dung hay là chưa đủ mà phải đi kèm với hình thức tốt, về khoảng này TGM Books luôn là số một.
Quả thật mình bị cuốn hút ngay từ bìa sách, nên mình đã quyết định mua ngay nó. Ai trong chúng ta cũng đều có 1 mơ ước, dù lớn hay nhỏ, và để sống với ước mơ đó thật không dễ dàng gì, liệu chúng ta có dám đánh đổi, có dám hi sinh những thứ hiện tại để đến với ước mơ của mình. Hãy nhìn vào những con người thành công nhất, ở đó chúng ta sẽ thấy được những thành quả đáng ngưỡng mộ mà họ đã phải đấu tranh, phải giành giật, làm việc không mệt mỏi một cách kiên trì mới có được.
"Thành Công" đối với mỗi người là khác nhau, và con đường để đến với nó cũng khác nhau, gian nan vô cùng. Và khi bạn có một mơ ước, thì việc đến được với mơ ước đó là bạn đã thật sự thành công. Cuốn sách là tập hợp những kinh nghiệm quý báu, những bí quyết giúp bạn giữ được lửa trong mình để soi sáng con đường phía trước. Hãy một lần đọc nó và cảm nhận
5
246712
2015-08-30 14:02:40
--------------------------
276891
6288
Cảm ơn cuốn sách những bước đơn giản đến ước mơ, trong chúng ta ai cũng có những mơ ước hoài bão của cuộc đời.Chúng ta luôn thắc mắc tại sao họ lại thành công, tại sao chúng ta lại thất  bại.Chúng ta thất bại chẳng qua chưa tìm cho mình con đường đi đúng đắn.Trong cuốn sách này hội tụ đầy đủ những cách khác nhau của nhiều người thành công trên thế giới áp dụng.Họ là những người đi trước đã trải qua những bài học khác nhau, giờ đây họ rút  kinh nghiệm và đúc kết lại cuốn sách giúp chúng ta dễ dàng tìm ra con đường mà mình sẽ phải trải qua
5
139912
2015-08-24 14:27:15
--------------------------
273126
6288
Phải nói trong những cuốn sách mình có thì đây là cuốn sách đẹp nhất. Khi nhận được mình thực sự là cực kì ngạc nhiên bởi bìa đơn giản mà vô cùng thu hút của nó. Từ màu sắc đến bố cục phải nói là thực sự rất rất đẹp. Mình đã bị cuốn hút bởi cuốn sách ngay từ lần đầu tiên nghe thấy tựa đề. Bản thân mình là một người sống cùng với những ước mơ và rất tin tưởng vào việc chỉ cần giữ ước mơ thì mình sẽ làm được những điều vượt qua hẳn suy nghĩ bình thường của mọi người. Mình vẫn đang từ từ nghiên cứu quyển sách này và mong rằng nội dung sẽ ấn tượng như những ấn tượng lần đầu tiên của mình về nó.
5
702178
2015-08-20 17:45:59
--------------------------
271023
6288
Nếu bạn thật sự khao khát điều gì đó, trở thành tỷ phú hay người nổi tiếng, mặc cho mọi người có cười nhạo bạn, hãy bảo vệ giấc mơ của mình ! Sống một đời nghèo khó nhất chính là không có lấy một giấc mơ.Con đường đến mơ ước rất nhiều chông gai, thử thách nhưng khi đọc quyển sách này tôi tin rằng bạn sẽ như tôi, sẽ vẽ lên được kế hoạch thực hiện lý tưởng của mình.Sách viết rất rõ, rất cụ thể từng phương pháp tiến hành kế hoạch ấy. Bạn còn trẻ, bạn có lý tưởng sống cao đẹp nhưng không biết thế nào để đạt được thì đây chính là chìa khóa cho bạn đấy ! Bản in đẹp chắc chắn bạn sẽ hài lòng.Tôi thấy thật may mắn được đọc quyển sách này.
4
665322
2015-08-18 20:02:29
--------------------------
264816
6288
Mới đầu nhìn bìa sách và tựa đề sách mình đã muốn mua ngay, chưa kể đó là sách của Adam Khoo, bìa thật sự rất đẹp mắt, màu phối hài hoà, tựa đề lôi cuốn chỉ muốn rinh về nhà ngay khi mới lần đầu thấy, mình rất thích sách của tác giả Adma Khoo,sách nào của chú mình cũng đón đọc hết. Chưa kể nội dung thì vô cũng hoàn hảo, đọc vài trang lại muốn đọc tiếp, đọc mãi chẳng muốn dừng, rất lôi cuốn người đọc. Chữ in rõ ràng, dễ đọc, dễ hiểu, giá cả cũng hợp lí,rẻ hơn thị trường rất nhiều,thật sự mua sách trên tiki rất lợi,mình rất hài lòng về sản phẩm này.
5
621499
2015-08-13 14:43:48
--------------------------
262071
6288
tôi rất thích các cuốn sách của bên TGM và cuốn sách này cũng không ngoại lệ
về phần thiết kế bìa thì tôi thấy hợp với tiêu đề và đep
sách được in bằng giấy binhg thường nên khá nặng nếu là loại giấy nhẹ thì sẽ tốt hơn
xét đến nội dung tôi thích cách trình bày khoa học,tác giả chia ra thành nhiều mắt xích tuy cũng nhiều nhưng được cô đọng lại nên dễ đọc, nhưng vẫn có những phần tác giả viết rất chi tiết rất tâm lí và hiểu biết về những lí do, phương pháp để đến với thành công tuy không phải là dễ để ứng dụng nhưng dù sao nhờ có cuốn sách này ta có thêm bài học phương pháp để tiến tới thành công
và đặc biệt có phần ông đưa ra trách nghiệm để giúp ta hiểu rõ về ưu nhược điểm từ đó ta được nhìn nhận rõ hơn về chính mình
5
540816
2015-08-11 16:58:15
--------------------------
259379
6288
"Những bước đơn giản đến ước mơ".Chẳng lẽ chỉ cần những bước đơn giản mà nếu ta áp dụng là đạt được ước mơ sao?Và quả thực quyển sách là những bí quyết hiệu nghiệm đơn giản và dễ hiểu của những người đã thành công.Còn việc áp dụng thuần thục bí quyết đó để gặt hái được thành công thì ắt hẳn không phải là dễ.Nhưng gì có thành công nào có được mà không trải qua rèn luyện cơ chứ.Tôi vẫn đang vận dụng theo những bí quyết trong sách cho con đường tới ước mơ của mình .
5
409358
2015-08-09 13:28:53
--------------------------
256003
6288
Mơ ước thì ai cũng có, nhưng cách để biến ước mơ thành hiện thực lại là 1 vấn đề khác. Cuốn sách hướng dẫn rất chi tiết từng bước để bạn có thể thực hiện ước mơ của mình, biến cái không tưởng thành hiện thực (dù bạn mơ trở thành tỉ phú hay phi hành gia thì đều có cách để thực hiện nó), đây có thể nói là cuốn hay hay nhất, hữu ích nhất và dễ thực hiện nhất trong tất cả những cuốn sách làm giàu mà mình đã đọc. Chỉ cần kiên nhẫn thực hiện từng bước theo chỉ dẫn trong sách và mỗi ngày ghi lại những thành tựu mà bạn đạt được, sau 1 năm kỳ tích sẽ xuất hiện và bạn sẽ bất ngờ. Rất đáng để gối đầu giường của mỗi người!
5
99983
2015-08-06 15:28:09
--------------------------
253032
6288
Sách của TGM luôn hấp dẫn tôi bởi kích thước chuẩn cũng như chất lượng giấy tốt. Cuốn sách không chỉ là những bước đơn giản đến ước mơ như cái tựa của nó, mà ẩn sau trong đó còn là cả một thông điệp - dù đơn giản đến đâu mà bạn không dám ước mơ dù chỉ một lần thì mọi thứ sẽ luôn xa vời. Có một câu nói mà tôi rất thích trong quyển sách: "Cứ làm đi, tạo nên một ngày của bạn." Đúng vậy, có thể hôm nay ta vẫn chưa đến đích, nhưng ta đã gần đến đích hơn ngày hôm qua.
5
187067
2015-08-04 08:56:30
--------------------------
247025
6288
Những Bước Đơn Giản Đến Ước Mơ là tập hợp những những bí quyết hiệu nghiệm của những người thành công trên thế giới được tác giả Steven K.Scoot cô đọng lại một cách đơn giản và hiệu quả nhất. Về mặt hình thức, sách có bìa đẹp và bắt mắt phù hợp với tiêu đề của sách. Về nội dung, tác giả nêu ra những sợi xích đang trói chúng ta lại khiến chúng ta không thế tiến đến ước mơ. Bên cạnh đó, còn nêu được những "động cơ"" mạnh nhất có thể giúp chúng ta tăng tốc và lao tới mục tiêu của mình. Đây là một quyến sách sẽ giúp bạn hành động và biến ước mơ thành hiện thực.
5
204567
2015-07-30 09:42:51
--------------------------
242219
6288
Mình rất thích sách của TGM bởi nội dung luôn được chọn lọc kĩ càng và hơn nữa được các dịch giả hàng đầu dịch lại nên vẫn đảm bảo đầy đủ nội dung và câu chữ mượt mà.

Nội dung sách không phải là quá mới lạ với bất kì ai trong chúng ta nhưng cũng có thể nói là không cũ, một lời khuyên cho các bạn là các bạn nên làm theo y như những gì tác giả yêu cầu và nhớ hoàn thành đủ bài tập bởi chắc chắn các bạn sẽ nhìn thấy sự khác biệt thần tốc.

Tuy nhiên, cá nhân mình cho rằng đây không phải là sách để đọc nhanh và cũng đừng vội vã ôm đồm cho bằng hết kiến thức bởi rất dễ quên, không nắm rõ được ý tác giả và không tiêu hóa hết " kiến thức" có 1 điều đáng tiếc là đôi khi tác giả không quá nhấn mạnh và xoáy sâu vào từng mục nào :)
5
90063
2015-07-26 11:30:02
--------------------------
228707
6288
Mình mua cuốn sách này được vài tháng nhưng mình đã dành thời gian để đọc đi đọc lại những suy nghĩ cũng như những cách mà tác giả đã giới thiệu,mình đã cố gắng đọc và học theo những phương pháp trong cuốn sách.Theo mình thì cuốn sách có một số điểm giống với cuốn sách " Cách nghĩ để thành công" của tác giả Napoleon Hill nhưng được diễn giải theo kiểu hiện đại hơn. Nhìn bề ngoài, sách khá bắt mắt, giấy in cũng khá tốt và rõ ràng. Nói chung đây là một cuốn sách nên đọc cho các bạn nhất là sinh viên.
5
483530
2015-07-15 20:31:32
--------------------------
202789
6288
Khi mua cuốn sách này được Tiki giảm giá lại bọc bóng bên ngoài thấy đẹp quá nên không nỡ xé ra đọc. Nên mua hơn một tháng mà giờ mới đọc xong. Trong “Những bước đơn giản đến ước mơ”, Steven K.Scott đã trình bày logic, rõ ràng từng luận điểm, vấn đề. Lối viết cuốn hút, câu chữ chau chuốt, được dịch sang tiếng Việt khá mượt. Quyển sách có nội dung vô cùng sâu sắc, nó giúp chúng ta hiện thực hóa ước mơ bằng những bước hết sức đơn giản. Nhưng bản thân mình nghĩ cũng chẳng dễ gì mà thực hiện.Cũng hơi tiếc vì biết đến cuốn sách này khá muộn. Nói chung rất có ích cho mọi người nhất là sinh viên, giúp mình định hướng rõ ràng tương lai hơn.
5
466046
2015-05-30 16:05:27
--------------------------
197997
6288
Trong “Những bước đơn giản đến ước mơ”, Steven K.Scott đã trình bày logic, dẫn dắt độc giả từ khía cạnh này sang khía cạnh khác của vấn đề, qua những câu chuyện thực tế, qua chính cuộc đời của ông làm người đọc không thể rời mắt khỏi sách. Quyển sách có nội dung vô cùng sâu sắc, nó giúp chúng ta hiện thực hóa ước mơ bằng những bước hết sức đơn giản, dễ áp dụng trong thực tế.
Nếu bạn cũng giống như tôi, chưa từng biết ước mơ của mình là gì hay đã để nó chết theo cùng năm tháng thì các bạn cũng sẽ tìm ra được nó sau khi đọc cuốn sách này. Nếu bạn sợ hãi hay lo lắng thì hãy áp dụng những nguyên tắc chặt bỏ sợi xích sợ hãi, nó giúp chúng ta tự tin hơn rất nhiều trong công việc cũng như cuộc sống hàng ngày. Nếu bạn chưa hiểu được bản thân, chưa biết cách phát huy hết khả năng tiềm ẩn bên trong mình thì Steven K.Scott sẽ hướng dẫn bạn làm điều đó – Kích hoạt 7 động cơ cực mạnh để bạn vươn tới ước mơ không tưởng của cuộc đời mình.
5
510184
2015-05-18 22:04:32
--------------------------
193596
6288
Những Bước Đơn Giản Đến Ước Mơ (Tái Bản 2014), tác giả Steven K. Scott đã đưa ra 15 bước đơn giản giúp bạn và tôi có thể hiện thực hóa ước mơ của mình một cách dễ dàng nhất (nếu bạn biết cố gắng và kiên trì theo hướng dẫn của Steven K. Scott).
Nội dung, rất hay, rất OK, đó là nguồn động sức tiếp thêm sức mạnh có bạn tiến bước đến với ước mơ. 
Hình thức: bìa nhìn đơn giản nhưng đẹp tôi rất ấn tượng. Cách trình bày trong sách rất OK, làm nổi lên được ý chính, có tóm nội dung chính ở cuối chương, và có hình ảnh minh họa ở đầu chương. Tôi rất thích những cuốn sách nội dung tốt mang động lực và có hình thức như vậy! SÁCH HAY!
5
504087
2015-05-08 08:11:57
--------------------------
160749
6288
Tôi tìm đến Những Bước Đơn Giản Đến Ước Mơ từ cái lúc tôi đọc xong quyển Sống và khát vọng của bác Trần Đăng Khoa, một quyển sách nhìn rất đơn giản bên ngoài, nhưng bên trong là một thế giới hoàn toàn khác, Steven K. Scott người tác giả đã dung lời lẽ của mình thuyết phục đọc giả rằng ước mơ không đơn giản nhưng cũng không hề khó nắm lấy. Chính lời lẽ chân thực, rất thường ấy đã chia sẽ tất cả những chông chênh và bí quyết đến với ước mơ. Úp sách lại và thực hiện ước mơ đã ấp ủ, đã chon vùi bấy lâu nay, cám ơn Những Bước Đơn Giản Đến Ước Mơ.
5
538722
2015-02-25 14:12:49
--------------------------
156207
6288
Ai cũng có ước mơ của mình nhưng không biết cách nào để thực hiện nó thì quyển sách này sẽ giúp ích rất nhiều trong việc biến ước mơ của bạn thành sự thật.Tôi rất thích quyển sách này bởi vì tác giả đã giảng giải, trình bày những bí quyết thành công của mình một cách chân thành, giản dị nhưng vô cùng ý nghĩa.   Theo tôi, mọi người nên vừa đọc vừa ngẫm nghĩ để tìm thấy con đường dẫn đến thành công tuy khó khăn nhưng cũng đầy hạnh phúc.Tác giả Steven K. Scott cũng là người bình thường như bao người khác chính vì những mục tiêu phi thường dẫn đến ông là người khác biệt,một con người thành công.Mọi người nên đọc tác phẩm này để tìm thấy những ước mơ cao đẹp của mình.
5
167268
2015-02-03 18:05:15
--------------------------
147199
6288
Ba tháng cuối năm 2014 là một quãng thời gian tuyệt vời của tôi, và cả gia đình tôi nữa. Bạn có thể tưởng tượng được trong vòng chưa đầy 1 tháng thu nhập của bố mẹ tôi đã hơn 30 triệu đồng so với trước đó mức lương ổn định mỗi tháng của cả gia đình chỉ hơn 10 triệu đồng, vâng, con số đã tăng lên gấp 3 lần. Bên cạnh đó là sự tăng vọt số điểm tổng kết học kỳ kì I (lớp 11) của tôi.
Làm thế nào mà chúng tôi có sự đột phá lớn đến như vậy? Xin phép tôi không muốn đề cập chi tiết nhưng tôi có thể khẳng định rằng, đó là nhờ chúng tôi đã áp dụng những nguyên tắc thành công đặc biệt mà bất cứ ai cũng có thể tìm thấy, nó nằm ngay ở những cuốn sách kỳ diệu, và cuốn gần đây nhất mà tôi đã mua chính là Những Bước Đơn Giản Đến Ước Mơ của Steven. Tôi đã đọc nhiều loại sách bí quyết thành công nhưng có lẽ đây là cuốn hay nhất tôi từng đọc. So với Think and Grow Rich, cuốn sách này có lối viết hiện đại hơn, sát thực tế hơn và "thân thiện" hơn. Cái cách Steven chia sẻ kinh nghiệm của ông thật đơn giản, mộc mạc mà lại chạm đến và thức những gì sâu xa nhất trong mỗi chúng ta, thật cuốn hút! Cái cách mà ông hướng dẫn cho ta từng bước đến với nấc thang cuối cùng của ước mơ thật chi tiết và hiệu quả. 
Tôi có thể dùng cụm từ để nói về cuốn sách: Sự Kết Tinh Hoàn Hảo!
5
36378
2015-01-07 09:18:09
--------------------------
135579
6288
Thật hay là mình đã kịp mua cuốn này trước khi quá muộn. Cuốn sách như một cẩm nang giúp bạn tìm được động lực và xây dựng hệ thống niềm tin giúp bạn hiện thực hoá những ước mơ của mình, với bìa ngoài đẹp, tươi sáng, hấp dẫn người xem, cuốn sách còn có tác động sâu với người đọc bằng lập luật chặn chẽ, lô-gic, nội dung rõ ràng, bố cục hợp lý. Cuối mỗi chương mỗi phần đều có phần tổng kết chung, tiện cho việc theo dõi và tiếp nhận những kiến thức. Tác giả còn chỉ ra những ví dụ hết sức cụ thể,... Mình rất thích cuốn sách này.
Hơn nữa, sách được TGM phát hành và bán trên Tiki nên bạn không cần phải lăn tăn về chất lượng cũng như giá cả :))))
5
414919
2014-11-16 10:07:23
--------------------------
