470449
8974
Đây là tác phẩm đầy lôi cuốn với nhiều tình tiết thú vị: yêu giới, ma, cương thi, tình yêu...Tình yêu của hai nhân vật chính phải trải qua nhiều khó khăn, ngăn cách mới đến được với nhau.Minh Nguyệt Thính Phong đã rất thành công trong việc xây dựng được  diễn biến truyện đầy thú vị và hấp dẫn qua từng trang truyện với lối văn hài hước thoải mái  và chân thực đặc trưng.Heo con và Diêm vương là tác phẩm mình đặc biệt thích và là mẫu hình truyện hiện đại xen dị giới mà mình muốn tìm đọc thêm.Hãy đọc để cảm nhận!
5
482613
2016-07-07 15:27:00
--------------------------
462366
8974
Sang tập 2, Heo Con Chúc Tiểu Tiểu với thân phận là 1 nhân viên trong công ty hàng ma sư của Nghiêm Lạc, dần được rèn luyện trở nên nhanh nhẹn, mạnh mẽ, bản lĩnh và trưởng thành hơn, tình cảm giữa cô và Nghiêm Lạc cũng ngày càng có tiến triển tốt đẹp. Mấy hàng ma sư khác trong công ty Nghiêm Lạc mình thấy rất thú vị, nhất là Cao Lôi với Thư Đồng. Đại nạn 600 năm lại sắp sửa lặp lại, tất cả đều trở nên căng thẳng hơn, cao trào thắt nút của truyện sắp đến rồi. Điểm cộng của truyện này là dịch rất tốt, mượt và có cảm xúc
5
1324806
2016-06-28 15:28:28
--------------------------
396064
8974
Khi đọc xong tập 2 tôi cảm thấy thật sự rất ấm áp trong tình cảm của 2 nhân vật chính Heo Con và Nghiêm Lạc. Và thật bất ngờ khi lại có sự xuất hiện của 2 nhân vật phụ không kém phần quan trọng đó là Nghiêm La và A Mặc-một cặp đôi xuất sắc. Tưởng như câu chuyện sẽ có những phút giây ngọt ngào về sau nhưng phần cuối thật là một hồi gay cấn khi Heo Con nghe tin rằng Nghiêm Lạc bị trúng độc. Vậy thì liệu họ sẽ ra sao? Liệu Nghiêm Lạc có trở lại bình thường được hay không?...Tôi thật sự rất mong chờ vào tập tiếp theo <3
4
1073608
2016-03-12 21:44:15
--------------------------
378720
8974
Tập hai của Heo yêu Diêm vương là sự chuyển biến tình cảm giữa hai nhân vật chính - Nghiêm Lạc và Chúc Tiểu Tiểu. Văn phong vẫn hài hước nhưng vì đi theo mô tuýp cũ nên theo ý kiến riêng là hơi chán. Điểm sáng trong tập này là có nhiều tình huống tưởng tượng diệt trừ yêu ma thú vị. Tác phẩm không chú trọng vào người thứ ba trong quan hệ tình cảm giữa hai nhân vật chính. Mong rằng tập ba sẽ hấp dẫn hơn. Tác phẩm phù hợp cho các bạn yêu thích sự nhẹ nhàng, dễ thương.
3
297890
2016-02-07 01:02:15
--------------------------
377995
8974
 Mình vừa đọc xong tập 1 liền nhảy vào đọc tập 2 luôn . Tập một đang dừng ngay chỗ gay cấn nhất . Sang tập hai cũng không làm mình thất vọng với nhiều tình tiết đan xen và những thế lực hắc ám như chỉ đang chờ để hại Tiểu Tiểu ( hay heo con ) . Vẫn lối viết hài hước từ tập một , trong tập hai giữa heo con và diêm vương Nghiêm Lạc đã có thêm bước tiến mới làm mình rất chờ mong ở tập ba cho hai bạn này đến với nhau đi . Ngoài ra truyện cũng có những cặp đôi phụ rất đáng yêu !
4
743273
2016-02-03 23:41:13
--------------------------
363783
8974
Đây là lần đầu tiên mình đọc tác phẩm của Minh Nguyệt Thính Phong và phải nói là vô cùng thích thú, càng đọc càng thấy ghiền. Lối văn của tác giả hài hước và lôi cuốn người đọc, càng về sau truyện càng có nhiều chi tiết đặc sắc và hấp dẫn người đọc hơn. "Heo yêu Diêm Vương" không chỉ nói về tình yêu giữa Heo Con và Nghiêm Lạc mà còn về những cuộc chiến ly kỳ giữa các tộc. Cốt truyện có nội dung mới mẻ, có những chi tiết kịch tính, kinh dị, ngọt ngào và đau khổ nhưng cũng không kém phần hài hước và lãng mạn. Giọng văn rất hay, thể hiện một phong cách rất riêng của tác giả.
5
628395
2016-01-04 21:56:32
--------------------------
346000
8974
một cuốn truyện đáng để đọc. Cho dù tôi ghét tình yêu chờ đợi nhưng tiểu thuyết này đã thuyết phục tôi. Đợi chờ hàng ngàn năm chỉ để đổi lại một câu nói. Sau đó còn bị cản trở bởi những ác linh, ma cà rồng, quái thú....sự tưởng tượng của tác giả đúng là vô đối, xây dựng các chi tiết nhỏ đến lớn rất chặt chẽ. Tuy là ngôn tình nhưng mỗi một câu chuyện về ác linh, về nhân cách con người được bộc lộ khiến ta phải suy nghĩ, cũng tạo được yếu tố sợ hãi cần có. cảm ơn tác giả đã viết nên truyện này
5
979895
2015-12-01 18:43:10
--------------------------
341244
8974
Nếu như ở nội dung tập 1 không có nhiều đặc sắc thì đối với mình, đến tập 2 đã xuất hiện nhiều tình tiết tạo tình huống gay cấn cuốn hút người đọc. Mình vốn không đánh giá quá cao truyện nhưng đọc đến đây thì mình đã bắt đầu cảm nhận được câu chuyện và bị nó cuốn theo từ lúc nào không hay. Điều mình đặc biệt không thích ở bộ truyện này chính là cái bìa. Bìa 3 tập truyện giống nhau và chỉ thay đổi màu tạo cảm giác rất nhàn mắt. Hình vẽ chibi không hề phù hợp và những dòng chữ nhỏ lại càng tạo cảm giác rối mắt. Màu chọn cho các bìa cũng xấu và nhìn bị xỉn. Hi vọng nếu có tái  bản nhà sách sẽ cân nhắc dùng bìa khác.
4
192908
2015-11-21 16:42:00
--------------------------
325433
8974
Nói thật thí tôi thâý truyện hơi dài dòng hơi kéo dài nhưng quả thật phải nói cũng khá hay tất cả tình huống đều được dàn dựng 1 cách công phu khiến người đọc á khẩu vô ngôn không còn từ để nói .....Chú heo đó quả thật là "hơi" ngốc còn đại boss thì lúc đầu tưởng là 1 anh hùng chính quân lúc sau thì "vô" cùng mặc dày vô sỉ khiến người đọc tâm tư rối bời thật sự rất nôn nóng muốn qua quyển ba thấp thổn không ngừng nhẹ nhàng nhưng không kém phần mạnh mẽ, lôi cuốn .
3
624207
2015-10-23 14:29:41
--------------------------
320522
8974
Mấy pha đánh nhau là chủ yếu thôi!Bên cạnh đó chuyện này nhấn mạnh yếu tố khoảng-cách-chênh -lệch,nên đọc truyện để theo dõi mấy pha 'choảng' nhau là chính.Vậy nên....tôi rất thích những nhân vật phụ.Người nào cũng cá tính,cũng độc đáo hết.
Heo con không yếu đuối.Tình yêu làm người ta kiên cường,làm người ta sẵn sàng bất chấp hiểm nguy để đạt được ước nguyện,khiến con người ta trở nên tốt đẹp hơn,trở nên mạnh mẽ hơn.Tác giả như muốn hướng đến một thông điệp rằng;dù là người hay là tiên,đã yêu,thì đều có quyền thay đổi,đều có tư cách để đến với nhau
Truyện đầu tiên của Minh Nguyệt Thính Phong mà tôi đọc là chạy đâu cho thoát.Có lẽ tác giả hợp với truyện hiện đại.Nhưng không có nghĩa là tác phẩm này không hay đâu nhá!Mỗi tội dài quá thể.


4
487743
2015-10-11 15:33:13
--------------------------
310303
8974
Biết đến Heo Yêu Diêm Vương cũng là nhờ cậu con trai quý tử_ Nghiêm Cẩn của họ trong (BHYCTMV). Sau khi quá phấn khích về văn phong và cốt truyện lẫn nội dung của MNTP thì mình quyết định mua trọn bộ 3 quyển của HYDV. Có thể nói, lựa chọn mua HYDV chưa bào giờ là quyết định sai lầm của mình cả, trái lại còn rất cảm ơn vì đã cho mình biết đến 1 tác phẩm hay như vậy. Theo mình, HYDV không đơn thuần là xoay quanh tình yêu giữa Heo Con và Boss mà còn là những cuộc chiến ly kỳ diễn ra giữa Ma Tộc và Thần Tộc. MNTP đã vô cùng thành công khi xây dựng 1 cốt truyện như vậy, một nội dung mới mẻ, có kinh dị, có huyền bí, có hài hước, có ngọt ngào và chua xót, tất cả nhân vật đều có 1 dấu ấn riêng cho mình. Đặc biệt, những tình tiết được tạo nên luôn phù hợp vs nhau, rất logic, mọi thứ đều có 1 mắc xích chung, không rời gạc, giọng văn rất nhạy, sắc bén không bị gò bó trong ngôn từ văn chương. 
5
459343
2015-09-19 14:23:53
--------------------------
306670
8974
Đây là lần đầu tiên mình đọc tác phẩm của Minh Nguyệt Thính Phong và phải nói là vô cùng ưng ý. Văn phong của tác giả dí dỏm và vô cùng lôi cuốn người đọc, càng về sau truyện càng có những chi tiết đặc sắc, kịch tính và hấp dẫn người đọc hơn. Mong là tập 3 sẽ có một kết thúc trọn vẹn và hạnh phúc cho hai nhân vật chính. Mình rất thích tác phẩm này, trong tương lai mong rằng Minh Nguyệt Thính Phong sẽ cho ra đời nhiều tác phẩm hay, hài hước và nhẹ nhàng như vậy nữa. 
5
115397
2015-09-17 18:38:40
--------------------------
302772
8974
Vẫn với cách viết truyện đáng yêu, tinh nghịch ấy nhưng truyện về sau càng kịch tính và ngọt ngào hơn. Heo con luôn mạnh mẽ cùng anh chàng Diêm Vương chung tình,luôn biết bảo vệ, yêu thương heo con thật khiến người ta ghen tị quá mà ^^. Tình yêu của cặp đôi chính rất kiên cố, không thay đổi dù những thử thách có khó khăn cách mấy. Cùng với nhân vật chính hoàn hảo thì cũng không thể thiếu những nhân vật phụ góp sức vào truyện. Truyện luôn mang đến những tình huống hồi hộp, thót tim khi anh và cô chống chọi với muôn trùng cách trở. Ở tập sau tôi chỉ mong muốn họ không uổng phí với những hi sinh mà họ bỏ ra
5
536908
2015-09-15 14:23:14
--------------------------
295844
8974
Đúng chất văn chương của Minh Nguyệt Thính Phong, hài hước, đáng yêu, truyện càng về sau càng kịch tính và hấp dẫn. Tình yêu của heo con và diêm vương được đẩy lên đến cao trào, heo con thì mạnh mẽ, diêm vương lại là người rất chung tình và có sức mạnh bảo vệ heo con, làm người đọc như mình đây không thể ngưng đọc được. Mình sẽ cày sang tập tiếp theo, mong một cái kết có hậu và trọn vẹn ở tập ba. Sẽ tiếp tục ủng hộ các tác phẩm khác của Minh Nguyệt Thính Phong. :3
5
688967
2015-09-10 17:58:51
--------------------------
290432
8974
Tập hai của bộ truyện "Heo Yêu Viêm Vương" lại càng hấp dẫn hơn tập một! phải khen một lần nữa về văn phong của Minh Nguyệt Thính Phong, phải nói là dí dỏm nhưng không làm người đọc nhàm chán! những cuộc đụng độ giữa ma và pháp sư được miêu tả vô cùng hấp dẫn, cao trào! Tập này có nhiều tình tiết lãng mạn hơn tập một khi Heo Con đã nhận ra tình cảm của Nghiêm Lạc! Nam chính quan tâm, chăm sóc, bảo vệ cho nữ chính hết mình! Tôi mua truyện do tựa truyện và hình ảnh bìa rất lạ và dễ thương, và càng đọc thì lại càng thích bộ truyện này! "Hơn sáu trăm năm trước, em dạy anh biết đến tình yêu, bây giờ sáu trăm năm sau,giữa hai chúng ta, tất cả quyền chủ động cũng đều ở em" thật mong chờ một cái kết đẹp ở tập kế tiếp! 
5
544616
2015-09-05 13:02:57
--------------------------
273728
8974
Cái tên của truyện khá hài hước và bìa sách cũng nổi bật nên khi nhìn thấy sách mình cực kì muốn mua. Khi hàng được chuyển đến tận nhà mình đã rất vui vì sách được gói rất cẩn thận không bị gãy mép. Đọc sách mình thấy thật không phí tiền khi mua cuốn sách về. Cốt truyện có phần mới lạ và độc đáo nên rất cuốn hút người đọc. Mình đã đọc tập một thấy khá hay nên quyết định mua tập hai. Câu chuyện tình cảm được nói đến có chút hài hước và cũng có chút sợ hãi, xúc động, chân thành. Mình rất mong chờ tập 3.
4
386212
2015-08-21 11:56:39
--------------------------
269644
8974
Tập tiếp theo của " heo yêu diêm vương" càng thêm hấp dẫn khi Chúc Tiểu Tiểu nhận ra boss chính là chân mệnh thiên tử của mình. Nữ chính mạnh mẽ gan dạ, nam chính anh hùng cái thế. Dàn nhân vật phụ hùng hậu nhưng đều có cá tính riêng. Mỗi chương mỗi cảnh trong truyện lại mang đến cho người đọc những cảm xúc khác nhau: hào hứng có, sợ hãi có, mà xúc động cũng có. Độ hấp dẫn tăng lên khi yêu ma quỷ quái xuất hiện ngày càng nhiều, con sau quỷ quyệt hơn con trước. Trông chờ tập ba với kết cuộc không thể viên mãn hơn.
5
109583
2015-08-17 16:59:17
--------------------------
265646
8974
Bằng những chi tiết miêu tả gợi cảm, những yếu tố hiện thực đan xen với yếu tố kì ảo và những tình huống hài hước, "Heo Yêu Diêm Vương" của Minh Nguyệt Thính Phong đã, đang và sẽ luôn để lại ấn tượng sâu sắc trong tim người đọc về tình yêu chung thủy của hai nhân vật trong truyện: Heo và Diêm Vương Nghiêm Lạc. Tình yêu của Heo đối với Diêm Vương chính là một tình cảm mãnh liệt, sôi nổi, sẵn sàng hy sinh vì người trong lòng như chính tính cách của cô vậy. Còn Diêm Vương đối với Heo chính là một tình cảm lặng thầm mà nóng bỏng trái ngược với vẻ ngoài băng lãnh của anh. Heo Yêu Diêm Vương là một bộ sách rất hay và đáng yêu, tôi không hề hối hận khi đã mua quyển sách này.
5
472087
2015-08-14 08:22:01
--------------------------
264484
8974
Trước giờ mình rất hiếm khi đọc truyện về yêu tinh ma quỷ nhưng vô tình thấy cái tựa rất lạ " Heo diêm vương" nên đắn đo lắm mới quyết định mua 1 tập về đọc tử. Ai ngờ ngay từ tập 1 mình đã mê tít truyện này rồi. Tuy là nói về những chuyện trừ yêu diệt quỷ, nhưng cách viết của tác giả làm cho mình có cảm giác chả khác nào đang xem một câu chuyện về cảnh sát bắt cướp cả, rất nhiều tình tiết hấp dẫn và lôi cuốn người đọc. Ngoài ra câu chuyện còn đan xen các tình tiết yêu đương khiến cho tác phẩm không bị khô cứng, vừa thích hợp cho những ai thích đọc truyện ngôn tình cũng như truyện trinh thám.
4
310891
2015-08-13 10:45:19
--------------------------
261123
8974
Heo yêu Diêm vương là tác phẩm mình đọc đầu tiên của về thế giới ma .Thoạt đầu đầu cứ như đang đọc truyện ma vậy nhưng càng về sau như ở tập 2 mình cảm thấy rất thú vị và lãng mạn . Nhiều cảnh khôi hài đan xen nhau đọc không hề bị nhàm chán . Heo con thật sự tinh nghịch , tình tiết vui nhất trong cuốn tập 2 này là sự ghen tuông của Nghiêm Lạc ,vừa lạnh lùng vừa lãng mạn ...thật sự quá tuyệt !!! càng đọc mình càng thấy hứng thú với cặp đôi này !
5
678718
2015-08-10 21:31:24
--------------------------
248482
8974
Hàng ma sư hóa ra chẳng phải một công việc oai nghi, lẫy lừng như người ta tưởng, tham gia mới biết chỉ toàn nguy hiểm và nguy hiểm. Tác giả viết những đoạn đánh yêu quái, ma sói, thu phục ác linh thực sự rất hấp dẫn. Còn phát hiện ra tên Vu Lạc Ngôn bấy lâu nay thích Tiểu Tiểu cũng sở hữu năng lực lớn, một hàng ma  sư  có năng lực. Nhưng hài nhất là  cái sự ghen tuông của Đại ác ma Nghiêm Lạc. Uầy. Diêm Vương nhưng chả có tí rộng lượng chút nào. Có cái đọc  lúc anh  bị thương cũng thấy đau lòng giùm nữa. Soái ca à.
4
139133
2015-07-31 10:01:11
--------------------------
229549
8974
''Heo yêu Diêm Vương '' là bộ truyện đầu tiên mình đã đọc về thế giới Diêm vương.Truyện rất hay nhưng lúc đầu đọc tớ vẫn rất sợ vì truyện có xem lẫn nhiều chi tiết nói về linh hồn, ác linh.Tuy vậy nhưng rất hấp dẫn và thú vị.Chuyện tình yêu trắc trở khó khăn nhưng vẫn có thể vượt qua ,ngay cả lời nguyền dành cho Heo con và Diêm vương cũng không thể làm gì được hai người.Đến cuối cùng hai người có một cuộc không trọn vẹn và hạnh phúc cùng bảo bảo.Truyện đọc rất hay.Quả không uổng tiền mua .
5
700931
2015-07-16 21:33:55
--------------------------
195197
8974
"Heo yêu Diêm Vương", một cái tên rất lạ và một câu chuyện tương đối thú vị. Những đoạn đánh quái, thu thập ác linh trong truyện rất hay, gay cấn và kịch tính, đọc mà không rời mắt nổi luôn. Thật ra trong truyện này không có nhiều đất diễn cho tình cảm lắm, nhưng chỉ cần qua những lần chiến đấu, qua việc đại boss trơ mắt nhìn bé heo lấy người khác mà bất lực suốt mấy trăm năm nhưng vẫn không thay lòng là đủ biết tình cảm boss dành cho Heo con lớn thế nào rồi. Truyện đọc rất hấp dẫn, mình coi liền 3 tập mà không chán.
5
449880
2015-05-12 22:55:02
--------------------------
194407
8974
Tập 2 này mạch truyện gay cấn hơn khi cuộc chiến bắt đầu khơi màu, xen lẫn với những phút diệt ma là những tình cảm sâu sắc của nhân vật, tập này Heo Con và Boss chính thức yêu nhau và kết hôn, trải qua 600 năm thì cuối cùng nhân vật chính lại được bên nhau; nhưng đây cũng là sự khởi đầu cho cuộc chiến khốc liệt sắp diễn ra ở tập sau khi thế lực ma đạo ngày càng mạnh cùng với sự bãi chức của Boss nhằm lui về âm thầm thực hiện kế hoạch tìm kiếm nhân tài và chống lại cuộc đại chiến
5
581978
2015-05-10 13:55:30
--------------------------
159488
8974
Càng đọc mình càng thấy hay và ghiền, mình quyết định mua tập 2. Ở tập này mình lại thích nhất Thư Đồng và Vu Lạc Ngôn. Hai người này làm mình cười ngất không thôi. Tuy Thư Đồng hơi cọc cằn, thô lỗ nhưng cô ấy rất thấu hiểu, thực tế. Ở tập này thì tình cảm của Nghiêm Lạc và Chúc tiểu Tiểu cũng đang bồi đắp thêm. Nhiều lúc dở khóc dở cười với Tiểu Tiểu. Nhưng công nhận lối viết văn của MNTP làm mình có cảm giác đang đọc truyện kinh dị. Rùng rợn đến ớn lạnh. Nhưng mà hay là ok rồi.
5
519052
2015-02-17 10:51:15
--------------------------
128087
8974
Mới đầu tôi bị tên truyện và văn án đánh lừa, cứ tưởng đây là truyện cổ đại, ai dè lúc mua về mới biết đấy là truyện hiện đại. Tôi cho rằng truyện này chỉ ở mức trung bình, không đặc sắc cũng chưa đến mức thảm họa.
Nội dung truyện ổn, có sự sáng tạo so với phần đông sách ngôn tình hiện nay. Cách tác giả đan xen chuyện hiện tại với quá khứ, cài thêm các tình tiết “trừ yêu diệt ma”rất thú vị. Hình tượng chàng Diêm Vương si tình được xây dựng khá ổn. Văn phong cũng dí dỏm, hài hước. Tuy nhiên, mạch truyện diễn biến quá chậm, lắm lúc biến thành dài dòng, nội tâm nhân vật (cả người lẫn ác linh) cũng chưa sâu sắc cho lắm, dẫn đến nhàm chán.
3
393748
2014-09-28 22:41:57
--------------------------
93999
8974
Tập 2 này hình như không được hay bằng tập 1, đó là theo ý kiến chủ quan của mình thôi các bạn không đồng ý thì cũng đừng giận mình nhé. Tập 2 này chủ yếu xoay quanh chuyện tình giữa Heo con và Boss đại nhân mà mình thì không phải người theo chủ nghĩa lãng mạn nên tập 2 có phần hơi chán so với mình. Có lẽ vui nhất vẫn là những cảnh có Thư Đồng. Mình thích nhân vật này hơn nhân vật nữ chính vì cô ấy khá lạc quan và vui tính không giống như Tiểu Tiểu lúc nào cũng chỉ biết khóc và dựa dẫm vào Nghiêm Lạc, như thế thật không có phong cách của 1 hàng ma sư. Mình cũng muốn có thiên nhãn của Vu Lạc Ngôn để có thể nhìn rõ con người trên thế giới này. Dù sao cũng vote cho tập này 5*.
5
57470
2013-09-08 21:55:49
--------------------------
86041
8974

Sang đến tập 2 tôi thấy truyện đã hay hơn đáng kể so với tập 1 Heo con cuối cúng cũng hiểu được truyện xảy ra giữa mình và Diêm vương.Nhờ có nhân vật Vu Lạc Ngôn mà cuối cùng Diêm Vương và Heo con cũng trở thành 1 đôi những chi tiết ngọt ngào cũng khiến tôi thấy thích truyện hơn.Tập 2 chủ yếu nói về việc bắt quái cùng với những nhân vật như Tiết Phi Hà,Thư Đồng,Tư Mã Cần,Ray....Heo con cũng bộc lộ bản chất hơn người của mình mà hoàn thành xuất sắc nhiệm vụ.Đến cuối cùng Vu Lạc Ngôn cũng hiểu ra thiên chức của mình để gia nhập vào tập đoàn hang ma phục quái.

2
111183
2013-07-08 21:07:13
--------------------------
81766
8974
Trong truyện này ngoài cặp đôi hai nhân chính Tiểu Tiểu-Nghiêm Lạc(hay Heo con với Diêm Vương). Heo con rõ là ngốc nghếch, đáng yêu còn Diêm Vương lạnh lùng nhưng tình cảm. Mình thật sự cặp đôi phụ Thư Đồng-Lạc Ngôn. Hai bọn họ cứ như oan gia vậy, gặp nhau là cãi nhau(cười chảy nước mắt). Chắc chắn kết thúc sẽ yêu nhau cho coi. Rõ hợp mà.
Tình tiếp truyện ngày càng lôi cuốn cứ tưởng chỉ có hai tập nhưng không phải. Nhưng mà càng tốt truyện hay vậy kết thúc uổng lắm ~.~. Rõ ghét bà Cửu Thiên Huyền Nữ ấy, tự nhiên lại đuổi Nghiêm Lac không cho làm viêc địa phủ, lại còn tưởng mình giỏi nữa chứ. Rõ ghép ấy. Nói chung là thích tập này.
5
60584
2013-06-18 15:33:38
--------------------------
79264
8974
Nối tiếp sau tập 1 diêm vương va heo con đến được với nhau nhưng k hề dễ dàng đạt được càng ngày càng nhiều chuyện xảy ra họ không được ở bên nhau thuận lợi từ việc diêm vương bị ép từ chức đến các ác linh xác sống ...ngày càng hoành hành hại người một chuỗi các sự việc gây cấn luôn khiến chúng ta chăm chú theo dõi như chính chúng ta là 1 nhân vật đang cùng họ trải qua các sự việc ấy đồng thời cũng không thiếu những lúc hài hước trong các cuộc đối thoại giữa các nhân vật như giữa Thư Đồng cùng Vu lạc Ngôn , Tư Mã Cần hay giữa Ray vs Boss . Kết cuốn 2 cũng giống cuốn 1 kết mở làm ta hồi hộp mong chờ cuốp tiếp theo . Tuy nhiên một số chổ cần viết rõ thì TG k lột tả hết được nhưng dù sao cũng mong mỏi được đọc cuốn 3 nhanh nhanh ra truyện đi thôiiiii 
5
97418
2013-06-06 11:45:33
--------------------------
