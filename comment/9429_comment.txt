53725
9429
Từ lâu mình luôn muốn tìm hiểu nhiều hơn về thuật phong thủy cũng như những liên hệ của nó với đời sống, nhưng vì những tài liệu phần nhiều mang tính trình bày nặng nề và khô cứng. Chỉ khi có cuốn sách này, mình mới có thể thỏa mãn nhu cầu tìm hiểu bấy lâu. Cuốn sách chứa đựng rất nhiều điều thú vị về phong thủy thực vật, về những kiến thức từ cơ bản đến chuyên sâu giúp mọi người tự áp dụng trong đời sống. Đọc cuốn sách mới có thêm nhiều hiểu biết bổ ích và hữu dụng về phong thủy, cả những khám phá bất ngờ về lĩnh vực này. Chỉ cần để ý một chút thôi, bạn có thể ứng dụng phong thủy để làm cuộc sống của mình thêm phong phú và màu sắc.
4
20073
2013-01-04 18:20:31
--------------------------
