438224
5613
tuy chưa nhận được tập 25 từ tiki, nhưng mình có thể nhớ lại tập 24 với nhiều màu sắc khó phai, với nhiều tình tiết hấp dẫn. mình còn nhớ trong tập 24 trước đó có bìa là hình conan mặc áo khoác tím, có kí hiệu chữ K bên góc trái, tập 24 có tổng cộng 11 file tất cả, trong các file ấy, mình thích nhất là phần khám nghiệm hiện trường, tức file 1, lí do mình thích là vì có chú cảnh sát takagi và cô Sato, mặc dù họ vẫn chưa thổ lộ tình cảm nhưng chắc chắn trong tương lai sẽ có. à nhưng mà file cuối mình đọc nhưng thấy chưa được hiểu lắm, lúc mà bé Ai bị bọn áo đen tóm á, sao có cảnh bé Ai đang ở trong lớp , còn có Azumi nữa, hk biết có phải bé Ai mơ mình đang ở trong lớp không nhỉ ^^. mình chỉ nhận xét tập 24 để những ai chưa đọc thì cũng biết được chút ít. 
5
1388711
2016-05-29 14:30:13
--------------------------
346949
5613
Sản phẩm tập truyện Thám Tử Lừng Danh Conan Tập 25 (Tái Bản 2014) quả đúng thật là rất hay , hấp dẫn và sáng tạo , không kém gì những tập truyện trước đây . Bìa thì không chê vào đâu được , chất lượng rất là tốt , giấy thì thì cũng vậy , chất lượng cũng rất là tốt , nhưng mực in có trang thì đậm có trang thì nhạt nên không hài lòng lắm . Nội dung câu chuyện thì cũng chẳng kém gì giấy và bìa hết , cũng rất là hay , mình rất là hài lòng . Nói chung tất cả mình rất lòng riêng mực in thì mình không!
4
603156
2015-12-03 17:40:45
--------------------------
346699
5613
Cứ kiếm nhiều tiền từ việc đơn giản là phải nghi có bán ma tuý không. 
Hung thủ chưa hiểu rõ câu chuyện đã ra tay là hồ đồ nhưng tác giả dựng truyện vô lý quá. Từ tiếng Anh bị hiểu nhầm sang tiếng Nhật, dịch theo tiếng Nhật rồi tự sát có vẻ miễn cưỡng. 
Lẽ ra nên báo cảnh sát là hại được ông cha rồi, không cần đích thân giết người để trả thù. Phải chăng con người ta thích giết người cho hả giận ? Hay nếu không có án mạng thì không có trinh thám !
Agatha dựng truyện hợp lý hơn Aoyama. 
5
535536
2015-12-03 10:08:29
--------------------------
196229
5613
Tập này cuốn hút mình ngay lập tức vì mình vốn là một fan của cặp đôi Heiji - Kazuha, đã vậy lần này còn có những đoạn thể hiện tình cảm của Heiji đối với Kazuha nữa chứ :)) Vì như đoạn Heiji tức giận vì Kazuha bị tổn thương ở vụ nhà nhện ấy, đọc xong vụ ấy cứ làm mình ám ảnh hoài á, có lẽ một phần do mình vốn sợ cái lũ 8 chân ấy. Một vụ án cũng hay không kém là đội thám tử nhí đi vào hang sâu thám hiểm và gặp những kẻ xấu, nhưng không lo, đã có Conan, mọi chuyện sẽ ổn thôi.
5
530790
2015-05-15 16:03:21
--------------------------
177426
5613
Điểm nhấn của tập này chính là khi Hattori, Kazuha và gia đình ông râu kẽm quyết định đi vào một ngôi nhà nhện trong rừng. Tất nhiên một vụ án mạng đã xảy ra nhưng suýt nữa đã làm hại đến Kazuha, điều này đã làm cho Hattori giận đến điên người. Cuối truyện là chuyến thám hiểm vào hang sâu của bọn thám tử nhí, cực kì nguy hiểm nhưng không kém phần thú vị. Tập này mình rất thích nên mình cho 5 sao, hình thức và nội dung đều tuyệt vời. Không những vậy, hình vẽ và nhân vật đều rất sống động và chân thật. Quả là một quyển truyện hay.
5
13723
2015-04-03 09:31:51
--------------------------
