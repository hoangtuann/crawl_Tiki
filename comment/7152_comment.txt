393169
7152
Trước đó tôi không có cái nhìn "đa cảm" với các tiểu thuyết Phương Tây nhưng sau khi đọc qua Jen Ero tôi đã hoàn toàn bị đánh gục.
Một tình yêu cháy bỏng của một người con gái đã chịu nhiều đau thương, mất mát nhưng vẫn tin yêu vào thứ mà cô gọi đó là tình yêu. Cái khát khao, nhiệt huyết của Jen Ero đã làm lung động tái tim thô lỗ của quý ông Rochester. Sự ngăn cách về độ tuổi không khiến cô gục ngã, hai người vẫn tìm và đến với nhau dù có bao nhiêu trắc trở, khó khăn (tiêu biểu là sự ngăn cấm từ phía gia đình và xã hội lúc mà người ta vẫn có cái nhìn khó chịu về tình yêu ông cháu).
Sách bìa cứng rất tuyệt cho việc lưu trữ, sắp xếp trên kệ sách cũng đẹp hơn. Đáng mua
5
95468
2016-03-08 12:30:12
--------------------------
380586
7152
Nói về 1 cô bé Jane Eyro sinh ra khi ba mẹ mất sớm ( họ đã cố tình đến với nhau khi không có sự đồng ý của cha mẹ) Cô bé được gửu cho 1 người họ hàng ngược đãi, rồi sau đó vào trại mồ côi. Năm 18 tuổi cô đi làm gia sư và yêu người bố nuôi của cậu học trò tên Thorn Field người hơn cô 20 tuổi. Đến lúc họ chuẩn bị cưới nhau thì Jane Eyro phát hiện ra người chồng tương lai đã và đang có vợ bên cạnh- Người phụ nữ điên. Cô dứt áo ra đi tình cờ tìm được họ hàng của mình. 1 năm sau cô quay lại tìm Rochester. Lúc này bà vợ điên của ông đã chết, ông thì bị mù và cụt tay do bà vợ. Jane Eyro vẫn quyết định yêu ông dù ông tàn phế. Họ bắt đầu cs gia đình với những đứa con
Là tác phẩm sâu sắc được sánh ngang với tác phẩm của người em Emily Bronte : Đồi Gió Hú
Rất xứng đáng để đọc tác phẩm này
++Đánh giá về giấy in: KHÔNG BIẾT CÓ PHẢI VÌ VẬN CHUYỂN XA KHÔNG MÀ LÚC MÌNH NHẬN SÁCH THÌ NÓ ĐÃ BỊ LONG GÁY- NHƯNG MÌNH ĐOÁN RẰNG ĐÓ LÀ DO CHẤT LƯỢNG SÁCH KÉM. GIẤY MÀU TRẮNG PHAU   
++Đánh giá về bản dịch: Bản dịch dễ hiểu và sâu sắc nhưng mình không thích phiên âm nhân vật sang TIẾNG VIỆT
+ BẠN NÊN ĐỌC TÁC PHẨM NHƯNG NẾU BẠN YÊU CẦU CHẤT LƯỢNG SÁCH CAO THÌ KO NÊN MUA CUỐN NÀY
5
919665
2016-02-15 13:07:44
--------------------------
367675
7152
Nàng Jane Eyre là người đã gây dựng nên cuộc đời đầy bi thương , trống rỗng trước sự phân biệt đầy đủ với cả xã hội khắc nghiệt làm cho nàng phải rời bỏ để đi tìm sự thân thiết gắn với thứ tình yêu sâu sắc với cô , không có nhiều tình tiết quá quan trọng song nhà văn đã miêu tả rõ nét về những con người như cô để một lần nữa phải quả quyết rằng trên đời này không có gì phủ nhận tầm cao lớn của những người phụ nữ ra tay cứu giúp .
4
402468
2016-01-12 16:08:49
--------------------------
222499
7152
Ban đầu nhịp điệu câu chuyện chậm làm tôi thấy khá nhàm chán, nhưng sau khi đọc đến đoạn sau tôi đã bị thuyết phục.
Cô bé Jane Eyre thật đáng thương lớn dần trong cảnh thiếu thốn nhưng trái tim vẫn thật trong sáng. Để rồi khi trưởng thành trở thành nàng Eyre mảnh mai với lý trí và tình cảm mạnh mẽ đáng ngưỡng mộ. Tình yêu của nàng lớn hơn bất cứ điều gì nhưng lý trí của nàng lại không cho phép một điều trái đạo đức, dù cho điều ấy không hại tới ai. Chôn chặt ngọn lửa tình, Jane bỏ đi, đầy đau đớn rời xa ông Rôchextơ. Nhưng ở nơi xa xôi, dù đã tìm thấy người thân, nàng vẫn không nguôi nhớ về ông. Tình yêu, tâm hồn, một nghị lực đáng nể ở người thiếu nữ mảnh dẻ đã chiến thắng. Nàng chiến thắng những con người muốn đày đọa nàng để sống và có được tình yêu. Cuối cùng nàng đã trở về bên Êđua thân yêu của mình, tàn tật nhưng tự do, một cái cảm động đến rơi lệ. 
Là câu chuyện tình yêu nhưng Jane Eyre đồng thời cũng nói lên hiện trạng xã hội lúc bấy giờ gây cho con người bao đau khổ. Nhưng hạnh phuc sẽ đến với những trái tim chân thành và trong sáng.
Chất lượng sách tốt, giấy không quá mỏng, và may mắn là khi mình nhận hàng sách vẫn nguyên vẹn không một chút hư hỏng.
Yêu Tiki !
4
547586
2015-07-05 19:03:18
--------------------------
195630
7152
Một tuổi thơ bất hạnh, nghèo khổ, đầy tủi nhục . Sự bất công khi sống cùng gia đình mợ Sarah Reed , sự khắc nghiệt khi ở trại trẻ Lowood , đã không làm cho Jen gục ngã .Jen đã không cam chịu số phận, mà vươn lên chống lại số phận khắt nghiệt đó .
Cũng như bao cô gái khác ,cô cũng muốn có được tình yêu .Và tình yêu chân thành của cô đã làm cho Rochester  , một người đàn ông vốn khó tính thô lỗ cộc cằn, thay đổi. Dù rằng tình yêu của họ, không phải là tình yêu giữa 1 nàng công chúa xinh đẹp, kiều diễm, và chàng hoàng tử đẹp như tranh vẽ..nhưng cũng thật cảm động và ấm áp . Dù rằng khi gặp lại nhau  Rochester đã già, đà mù lòa , nhưng Jen vẫn yêu và nguyện ở cạnh ông . 
Jen cô gái nghị lực, chân thành, lương thiện và mạnh mẽ .
Khi đọc nó tôi tin bạn sẽ yêu Jen như tôi đã yêu cô ấy vậy .
5
616860
2015-05-14 10:05:24
--------------------------
