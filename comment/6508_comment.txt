494360
6508
Giấy xấu, xấu nhất trong những quyển mà mình đã đọc, quote tặng kém cũng xấu. Các bài được tuyển chọn thì có 3/10 bài hay con lại cũng bình thường và chán. Rất hối hận khi mua quyển này!
1
1188641
2016-12-06 17:15:59
--------------------------
438220
6508
những câu chuyện trong cuốn sách này không quá hàm xúc và xuất sắc nhưng tôi rất thích vì giọng văn nhẹ nhàng đó và còn có lẽ cả những cảm xúc thật như tự truyện của chính người viết. Đó là những mối tình vừa ngô nghê, vừa khờ dại. Chúng khiến ta thổn thức và gợi lên nhiều mối tình ô mai đã trở thành kí ức trong mỗi người. Tình yêu, hay ít  nhất là thích. Để được hạnh phúc cũng cần phải xuất phát từ hai phía. Tuổi trẻ là lúc để chúng ta yêu nồng nhiệt, để nông nổi, Bỏ qua, dẽ để lại tiếc nuối.
4
692220
2016-05-29 14:26:00
--------------------------
406824
6508
Mình mới mua quyển sách Tớ Từng Thích Cậu Như Thế Đấy! cách đây cũng một vài hôm. Quyển sách này phải nói là tác giả viết rất hay, rất tuyệt vời, cô đọng, súc tích luôn. Quyển sách này đã đưa tôi vào một thế giới - thế giới của nhân vật, tôi cứ như là hòa vào họ để cảm nhận những cảm xúc, những tâm trạng của họ, nó ngộ nghĩnh, đáng yêu, buồn cười như thế nào ấy và đôi khi còn là những khoảng lặng sâu trong đáy lòng. Tình đầu sáng trong, tươi đẹp vô cùng, dù cho mình có trải qua bao nhiêu biến cố trong đời thì  vẫn là những ký ức rất đẹp, những khoảnh khắc  khó quên nhất trong mỗi con người của chúng ta
5
526223
2016-03-28 22:49:29
--------------------------
377826
6508
Tớ từng thích cậu như thế là một cuốn sách mà trong đó gồm nhiều câu chuyện ngắn viết về tình cảm trong sáng của các cô cậu học trò.. Truyện có nội dung tình yêu tuổi học trò trong sáng, hồn nhiên, ngây thơ, đáng yêu.. câu chuyện tình yêu giữa những người bạn cùng lớp.. câu chuyện tình yêu của con trai lớp văn và con gái lớp toán.. những tình huống thật đáng yêu, những lời nói ngây thơ, những trò đùa vui, những lần đuổi nhau xung quanh sân trường.. Nói chung là cuốn sách này khá hay
4
1092116
2016-02-03 14:32:14
--------------------------
376172
6508
Cuốn sách là những câu chuyện ngắn về tình yêu tuổi học trò với những gì giản dị nhất, trong sáng nhất và đẹp nhất. Đôi câu chuyện cũng là những nỗi buồn, những tiếc nuối về tình yêu đơn phương hay mối tình đầu. Tất cả tạo nên một cuốn sách thật tuyệt. Tất cả mọi người đều đã từng trải qua một thời như vậy nên khi đọc cuốn sách này sẽ có được một cảm xúc bồi hồi, xúc động. Đâu đó cũng sẽ bắt gặp hình ảnh của mình trong đó. Tóm lại cuốn sách rất ý nghĩa. Mua không phí tiền
5
1101166
2016-01-30 10:25:05
--------------------------
329877
6508
Tôi đã có trong tay một quyển " tớ từng thích cậu như thế đấy!", tôi biết đến quyển tuyển tập này từ blog radio, ngày nào tôi cùng vào web ấy để nghe những câu chuyện, những tâm sự khá là hay ho và tôi đã tình cơ nghe được số blog có chứa 1 truyện ngắn trong tuyển tập này. Những câu chuyện ngắn trong tuyển tập là những cảm xúc đầu đời trong veo mà hẳn ai trong chúng ta đều đã từng trải qua, tình đầu ngây ngô hay bước sang mối tình cấp 3 đầy thơ mộng... và cả tình yêu đơn phương chỉ dám giữ riêng cho mình mà chẳng dám nói ra. Có những tiếc nuối cũng có những mối tình đẹp.. nhưng dẫu sao nó cũng làm tôi sống lại một thời từng ôm ấp mối tình đầu học trò của mình. Cảm ơn tiki
5
508805
2015-11-01 15:46:41
--------------------------
281297
6508
Về những câu chuyện, chúng hoàn toàn thuyết phục được mình. Mỗi khi rảnh rỗi, mình cũng hay đọc lại. Có cảm giác rằng những câu từ ấy mang một sức hút rất đặc biệt, và kéo cảm xúc của mình đi theo nhân vật. Chất lượng của chúng thì tuyệt rồi, không cần phải bàn cãi.
Nhưng về phần giấy, font chữ và bìa thì mình không hài lòng lắm. Giấy có màu hơi tối, bìa (và cả giấy) cũng khá mỏng nữa. Bên trong mình có bắt gặp một số kha khá lỗi chính tả. Mình có cảm giác như nó được in một cách khá sơ sài, qua loa và khác xa so với những gì mình hình dung khi chọn mua.
Bù lại, bộ photo quote tặng kèm rất dễ thương và ý nghĩa. Mình hi vọng những sản phẩm sau sẽ được cải thiện tốt hơn. Chứ như thế này thì thật là tiếc.
3
528046
2015-08-28 12:17:41
--------------------------
274537
6508
Thời học sinh, ai ai cũng từng có những rung động, xuyến xao ngây ngô, trong sáng. Thích chỉ vì thích, yêu chỉ vì yêu, chân thành mà nông nổi. "Tớ từng thích cậu như thế đấy!" - một lời confess mạnh mẽ, đáng yêu, làm người ta bật cười thú vị kèm ngưỡng mộ. Những câu chuyện trong tuyển tập cũng đều đáng yêu như tựa sách: chút yêu thương len lỏi cái thời "quần xanh áo trắng", chút nhớ nhung, chút hờn giận, chút tiếc nuối...lại ngọt vị ô mai :)) Tình đầu sáng trong, tươi đẹp và dù cho có trải qua bao nhiêu biến cố trong đời thì đó vẫn là những ký ức, những khoảnh khắc kỳ diệu, khó quên nhất! Thế mà...rất mong manh...Thành hay không không quan trọng, quan trọng chính là chúng ta đã có một thời tuổi trẻ sống hết mình, không hối hận. Sách đã giúp độc giả như ngồi lên Cỗ máy thời gian, sống lại cái thời trong trẻo bất tử đó trong những tích tắc ngọt ngào...Đẹp...
4
247818
2015-08-21 23:46:59
--------------------------
237246
6508
Cứ như môt nụ hồng e ấp mới chớm, “Tớ từng thích cậu như thế đấy!” đưa ta quay về cái buổi ban sơ ô mai của tuổi học trò. Ở cái tuổi ấy, người ta có nhiều rung động rất thoáng qua, nhưng đẹp, và trong trẻo. Cả một thời áo trắng đã qua được gói trong những hạt giống khác nhau và quyển sách này là một trong những hạt giống như thế, để khi người ta mở nó ra thì cả một cái cây xum xuê tự dưng đâm chồi trước mặt chỉ trong vài chương truyện. Thoáng nghĩ về một thời ấy, hình như là đánh mất, mà vì vậy nên tìm về. :)))
4
152579
2015-07-22 12:32:58
--------------------------
210123
6508
Đọc xong mỗi câu chuyện là lại có một trải nghiệm cảm giác khác nhau, cứ như kí ức của cải tuổi ngây ngô ùa về vậy. Quyển sách này tuyển tập những câu chuyện khá hay, nhưng mình vẫn thích mấy truyện ở đầu sách hơn vì chúng có kết khá là happy. Thích nhất là truyện 5cm/s, đọc cái tên thì tưởng là giống truyện cùng tên của Nhật, nhưng không ngờ tác giả lại có thể nghĩ ra câu chuyện tạo cho mình cảm giác mới lạ như vậy. 
Tóm đi tom,s lại, quyển sách này rất rất đáng mua nếu ai đó quan tâm tới tình yêu của tuổi học trò và muốn được lần nữa đắm mình trong cảm xúc ngây ngô thời đó.
4
369458
2015-06-18 22:34:25
--------------------------
187170
6508
Mình nghĩ về nó vêd một chút tuổi học trò, mình tìm về nó tìm 1 chút mình của ngày xưa. Đọc cuốn sách này giống như những gì mình trải qua, nhẹ nhàng ngây ngô là 2 từ mình dùng để nói về cuốn sách này. Bởi nó là sự kết hợp của những câu chuyện học trò đáng yêu, là sự tinh hoa của những cây bút với lối viết tuy khác nhau nhưng ở họ có 1 góc khuất tâm hồn để nghĩ về một gì đó của chút hoài niệm. Mình như được trẻ lại là mình của 3 năm trước vậy. Mình hài lòng với sự nhẹ nhàng mà cuốn sách mang lại, mình thấy thích thú với sự trẻ con trong veo của những câu chuyênh này. Mình thực sự hài lòng về cuốn sách
4
346097
2015-04-22 16:38:49
--------------------------
173725
6508
"Tớ đã từng thích cậu như thế đấy" tuyển tập những truyện ngắn thể loại tình cảm của tuổi teen. Thực lòng từ trước đến nay tôi không thích đọc mấy loại truyện này lắm, nhưng đọc quyển này tôi lại có 1 suy nghĩ khác. Truyện ngắn đa số trong tập truyện đều mang phong cách nhẹ nhàng. Cách dẫn dắt váo tập truyện khiến tôi khá hứng thú nhưng càng về sau nhất là mấy câu chuyện ở cuối cùng lại là kết thúc SE nên tôi không thích lắm. Điểm trừ duy nhất ở tập truyện là còn in sai lỗi chính tả
3
561532
2015-03-26 17:45:46
--------------------------
156596
6508
Tôi đã tia quyển này từ hè rồi và bây giờ mới mua được. Mỗi một câu chuyện trong "Tớ từng thích cậu như thế đấy!" đưa tôi vào một thế giới - thế giới của nhân vật, tôi như hòa vào họ để cảm nhận những cảm xúc của họ, nó ngộ nghĩnh, đáng yêu, buồn cười và đôi khi còn là những khoảng lặng. Để rồi sau đó gấp quyển sách lại, tâm hồn tôi lâng lâng khó tả. Và tôi thắc mắc - tại sao phía sau quyển sách lại ghi "Đây là truyện ngắn đặc biệt dành riêng cho những ai đã bước qua một thời áo trắng" mà không phải là "Dành cho tất cả những ai đã và đang trải qua thời áo trắng". Nói chung sách rất hay bởi vì ở đâu đó trong những câu chuyện tôi có thấy bóng dáng mình thấp thoáng trong đó :)
4
548377
2015-02-05 13:49:58
--------------------------
155385
6508
Vừa đọc xong quyển sách này. Nó bao gồm nhiều truyện ngắn khác nhau về chủ đề lứa tuổi học trò. Mỗi truyện có một cảm xúc riêng nhưng đều nhẹ nhàng đi vào lòng người với những cảm xúc sâu lắng. Những rung động đầu tiên của thời áo trắng cắp sách đến trường, những tình bạn thật tốt, thật chân thành...ta dễ dàng tìm thấy phần nào bản thân mình trong đó. Tuy nhiên, có một lỗi khá khó chịu là in bị mất tổng cộng 6 trang sách (không liên tục nhau). Thật tiếc nhưng cũng hài lòng. Hơn nữa bộ trích dẫn hay được tặng kèm rất tuyệt, có thể sử dụng đễ tặng bạn bè.
4
198858
2015-01-31 21:07:06
--------------------------
120660
6508
Cuốn sách này mình không mua mà được tặng. Ban đầu mình chẳng hứng thú lắm vì tựa đề sách khá trẻ con, cũng vì mình nghĩ truyện của tuổi teen viết chắc cũng không hay đâu. Nhưng mình đã nhầm. Cuốn sách khá hay, khá nhẹ nhàng, phù hợp với lứa tuổi mới lớn.

Những truyện đầu tiên nói thật là khá dở. Nội dung mờ nhạt, câu cú lủng củng, chấm phẩy loạn cả lên không rõ đầu đuôi, lại còn sai chính tả. Mình khá thất vọng với phần ấy. Nhưng càng về sau cuốn sách lại càng hay. Các truyện gần cuối ấn tượng hơn hẳn. Nội dung có bình thường, gần gũi, có mới lạ, độc đáo, cách hành văn của mỗi tác giả lại có một nét riêng. Trong đó mình thích nhất "Tình đầu bọ xít" và "Có một mùa thu rất lạ trong tranh". Có lẽ vì mình già trước tuổi cho nên cô bé cũng già trước tuổi trong "Có một mùa thu rất lạ trong tranh" đã làm rung động trái tim mình. Văn phong của truyện cũng rất êm đềm, tình tiết mới lạ.

Có lẽ cuốn sách này hợp để đọc giải trí và để nhớ về tuổi học trò hơn là để tìm kiếm một cái gì đó sâu xa, cao siêu. Rất nhẹ nhàng! Mình khá thích cuốn sách!
3
24661
2014-08-14 11:52:45
--------------------------
