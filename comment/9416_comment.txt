319354
9416
Mình mua cuốn sách này được 4 tháng rồi, nhưng công việc bận quá không có thời gian đọc, bây giờ mới đọc xong. Cảm nhận đầu tiên là tác giả có một nền tảng về kinh doanh đầu tư mạo hiểm từ người cha đi trước dẫn dắt và các người đồng nghiệp tốt. Thứ hai là giúp cho người đọc hiểu được bản chất của đầu tư mạo hiểm là giúp ươm mầm các công ty tài năng khởi nghiệp. Tuy nhiên, lợi nhuận vẫn là trên hết. Đọc cuốn này, mình thấy tác giả đề cập không đầu tư tại Việt Nam. Có lẽ người Tây tế nhị nên họ chỉ nói vậy thôi chứ không đề cập chi tiết vì sao. Còn vì sao thì mình nội suy là do các công ty khởi nghiệp của mình không hấp dẫn, môi trường phát triển không tốt, nhân sự...
4
196421
2015-10-08 15:26:47
--------------------------
269120
9416
Nội dung về khởi nghiệp chính là lý do khiến tôi phải đặt mua quyển sách ”cuộc chơi khởi nghiệp” này trên Tiki. Và khi nhận được quyển sách này trong tay, tôi thấy quyển sách này bìa sách đẹp, giấy in tốt, nội dung trong quyển sách này đề cập đến việc khởi nghiệp. Đã có rất nhiều sách viết về chủ đề  nhưng hầu như đều nội dung khó ứng dụng ở Việt Nam, rất ít sách nội dung phù hợp với thị trường ở Việt Nam, và quyển sách này đã giải quyết được vấn đề đó. Nhìn chung, theo tôi đây thật sự là một quyển sách rất đáng để chúng ta mua về đọc.
3
42985
2015-08-17 09:17:52
--------------------------
