503169
3798
Quyển sách ngữ pháp kinh điển, rất bổ ích. Có một số lỗi dịch nên chỉ được 4 sao.
4
1384584
2016-12-31 16:18:26
--------------------------
394642
3798
Sách rất hay, cụ thể, dễ hiểu.Có các đề mục bằng tiếng việt. Nội dung sách dễ tiếp thu theo từng chủ đề . Cuối mỗi đề mục có các bài tập thực hành rèn luyện khả năng và đánh giá được mức độ của bản thân khi làm bài tập. 
Sách có giấy tốt, màu mực đẹp mắt, bìa sách dày và khó bung ra như các loại sách khác .
Điểm chưa tốt của sách là còn ít bài tập ở cuối mỗi mục, và chưa có bài tập tổng hợp của tất cả các bài. 
Nhưng với mình cuốn sách này rất hay, rất hữu ích trong việc học của mình ở trường.
4
1163950
2016-03-10 19:03:41
--------------------------
349993
3798
Sách cung cấp đa dạng và phong phú các dạng kiến thức cơ bản về Tiếng anh cho mình. Phù hợp cho những bạn học lại, bắt đầu học hay đơn giản là bổ sung và củng cố nền tảng cơ bản môn Anh. Điểm chưa hài lòng là bài tập hơi ít và chưa có độ khó. Chủ yếu chỉ áp dụng chứ chưa thực hành được nhiều. Phần key nằm hơi....gần bt nên nhiều khi chưa tự giác lắm. Sách khổ nhỏ mà dày trang nên lật giở để học cũng hơi...bất tiện. Bù lại sách in chữ rõ ràng, giấy giày và tốt, bìa dày. 
4
123769
2015-12-09 17:04:27
--------------------------
291165
3798
Mình đã học được vài đề mục của cuốn sách và thấy rất hay và khoa học. Sách có ví dụ từng trường hợp cụ thể của các đề mục, ngoài ra còn có liên hệ từ đề mục này sang đề mục khác, giúp cho người học dễ dàng trong việc tiếp cận mỗi đề mục liên quan tới nhau. Sau mỗi mục đều có bài tập riêng để giúp bạn luyện cho đề mục đấy và đặc biệt phần key nằm ngay cuối mỗi đề mục nên không cần phải dở qua lại ở cuối sách để kiểm tra đáp án như những cuốn sách khác mình đã học trước đây. 
Điểm chưa tốt của cuốn sách là phần giải thích mỗi đầu đề mục được viết bằng tiếng việt, nếu sử dụng tiếng anh có vẻ sẽ tốt hơn. Phần bài tập hơi ít cho mỗi đề mục chưa giúp mình luyện được nhiều. Tuy nhiên với một cuốn sách hay và dày thế này lại có giá rất học sinh/sinh viên nên mình rất thích. 
4
53222
2015-09-06 02:16:14
--------------------------
269072
3798
đây là cuốn sách rất có ích trong việc học anh văn đối với mình, nó chia ra 130 unit , mỗi unit sau khi hướng dẫn lý thuyết đều kèm theo bài tập rõ ràng và đều có phần key để dễ dàng so sánh kết quả điều này khiến mình dễ dàng tìm ra lỗi hay sai của mình trong ngữ pháp, còn đối với từ vựng thì rất quen thuộc với học sinh trung học cơ ở và trung học phổ thông . là cuốn sách giúp người đọc củng cố ngữ pháp , hoàn toàn dễ hiểu ,dễ tiếp thu, cách trình bày ổn, tuy nhiên chữ màu xanh gây hơi đau mắt đối với mình
4
641447
2015-08-17 08:00:10
--------------------------
256305
3798
Bản thân mình thấy cuốn sách này khá hữu ích, các đề mục tiếng anh được nêu đầy đủ, phù hợp với chương trình. Mỗi bài đều có các tình huống cụ thể giúp chúng ta dễ hiểu hơn. Cuốn sách khá dày, chất lượng giấy in tốt, chữ rõ nhìn, không nhập nhèm hay sai lệch. Tuy nhiên các cấu trúc, các form được nêu chưa được cụ thể lắm, và lượng bài tập còn khá ít. Mỗi unit chỉ có từ 4 đến 5 bài luyện ngắn. Cuốn sách chỉ phù hợp với những người đã được học tiếng anh cơ bản, người chưa biết gì về tiếng anh khi đọc sẽ khá khó hiểu. Dù sao đối với bản thân mình thì cuốn sách này khá hữu ích.
4
557090
2015-08-06 19:19:57
--------------------------
