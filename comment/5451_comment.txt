488912
5451
Khoan nói đến nội dung của sách, đầu tiên khi nhận hàng từ tiki mình cảm thấy hơi thắc mắc. Mình đặt mua sách bỏ túi của nhà xuất bản Đống Đa nhưng khi mình lại nhận được sách (không phải là bỏ túi) của Đinh Tị. Cùng một nội dung giống nhau nhưng mình vẫn thích bỏ túi hơn, vì nó gọn, dễ mang theo. Nhưng mình sẽ bỏ qua vì giá tiền không bị thay đổi. 
Về nội dung, có lẽ mình chưa có một tâm hồn cảm thụ sâu sắc để nhận ra được giá trị của nó. Mình thấy cốt truyện khá đơn giản, phần lớn xoay quaanh chuyến đánh cá của ông lão, chuyện ông lão gặp cá mập và phải chống chọi với nó. Có lẽ đây không phải thể loại sách mình yêu thích.
3
1672241
2016-11-05 13:50:10
--------------------------
373494
5451
Mỗi tác phẩm của Ernest Hermingway là một sản phẩm văn chương xuất sắc. Nếu Tuyết trên đỉnh Kilimanjaro cho người đọc thấy được cảnh đẹp và văn hóa ở Châu Phi, Chuông nguyện hồn ai cho thấy nét đẹp của tình yêu và khát vọng trong thời chiến thì Ông già và biển cả là một cuộc chiến của ông lão đánh cá và những đàn cá biển. Trong cuộc chiến đó, ý chí và phẩm chất của con người được khắc họa rõ nét qua những lời độc thoại của ông lão đánh cá. Ông già và biển cả là một tác phẩm phù hợp cho mọi lứa tuổi. 
5
984574
2016-01-23 21:55:42
--------------------------
277415
5451
Một tác phẩm văn học kinh điển của thế giới và cũng là một tuyệt tác của Hemingway. Khi trước mình chỉ được đọc một trích đoạn của tác phẩm này trong sách giáo khoa ngữ văn cấp 3 chứ chưa đọc hết toàn bộ. Cuốn sách phản ánh hiện thực cuộc sống về xã hội thời bấy giờ nên rất đáng đọc. Tác phẩm cũng đã tạo nên một nguyên lý trong văn học : nguyên lý tảng băng trôi nói về tính hai mặt của vấn đề. Đây cũng chính là điểm thu hút của tác phẩm khiến nó trở thành một tác phẩm văn học của thời đại.  Mình cũng rất thích những kiểu sách bỏ túi như quyển này, nhỏ gọn và nhẹ nhàng nên mình có thể mang đi khắp nơi.

5
137953
2015-08-24 22:18:35
--------------------------
245379
5451
Đọc xong cuốn sách gợi cho mình khá nhiều suy ngẫm 
nhất là về tính kiên trì trong cuộc sống ..mình đã tự hỏi "sao một ông già lụ khụ vs chẳng có một chút gia sản gì đáng quý ..lại có thể lạc quan..tin tưởng vào một tương lai tốt đẹp ,về những con cá to ..dù cho hơn hai tháng ròng dài đằng đẵng trôi qua như vậy ??
và tại sao ..phải chăng sự đầy đủ , cuộc sống nhanh , tấp nập của hiện tại làm cho con người luôn bực dọc , mất kiên nhẫn để rồi phải hối tiếc ?? "
là một người trẻ trong xã hội ..phải biết nỗ lực .luôn phấn đấu vươn lên..ko nản chí trước khó khăn ..thử thách ..biết "hết mình"
Chữ trong sách hơi nhỏ ..lời thoại của nhân vật cũng không rõ ràng ..nên phải đọc thật tập trung...để hiểu..để cảm nhận !!
4
614518
2015-07-28 21:31:02
--------------------------
242827
5451
Ối mình hơi tiếc là lúc mình mua cuốn sách nó quá nhỏ và mình đã không đọc kỹ nó nên cứ nghĩ là khổ bình thường nên khi mua về hơi hối hận bởi bìa sách, khổ sách nhỏ, chữ cũng nhỏ theo hơi khó đọc, dù font và size cũng khá rõ ràng, Tuy nhiên bìa sách thì rất ổn, mình khá thích. 

Về nội dung thì không còn gì để bàn cãi rồi, đây đúng là một tác phẩm bất hủ mà ai cũng nên xem một lần. Câu chữ mượt mà mà gần gũi,hình ảnh nhân vật được xây dựng rất tốt. Tạo hình nhân vật  " ông già" mạnh mẽ, hào hùng như số phận của những ngư dân lênh đênh chênh biển vậy ^^
5
90063
2015-07-26 23:37:07
--------------------------
204459
5451
Những câu văn, câu chữ vô cùng giản dị, chủ yếu là những đoạn độc thoại của nhân vật nhưng với Hemingway thì đã tạo nên những cung bậc suy nghĩ đậm nét, rất nhân văn và rất cao thượng.
Với những nỗ lực của mình, với những ước vọng chinh phục ông lão đã câu được con cá kiếm lớn nhất, oai hùng nhất. Nhưng nhiều khi ông lại đấu tranh với chính bản thân mình bởi ông đã đi quá xa: có thể trận chiến này làm hại cả hai khi con cá sẽ không thể toàn vẹn về đất liền với đầy đủ giá trị vật chất của nó, khi ông già đã sức cùng lực kiệt và để đối thủ lớn nhất bị bọn cá mật sâu xé. Những hình ảnh đặc sắc trong lối văn của mình giản tiện tới mức tối đa và chính vậy ông đúng là bậc thầy trong nguyên lý "Tảng băng trôi".
5
105692
2015-06-04 08:17:01
--------------------------
204443
5451
Ông già và biển cả thực sự là tác phẩm văn học đáng đọc, phù hợp cho mọi lứa tuổi. Đây là tác phẩm đã giúp Hemingway đặt được Nôben Văn học
Bạn có thể tìm đến tác phâm này, với mục đích kiếm tìm câu chuyện thật thú vị, đầy hóm hỉnh và hài hước.
Nhưng ở một cái nhìn sâu hơn, ta sẽ bắt gặp những triết lý, đáng để suy ngẫm ở đời.
Tác phẩm như minh chứng rõ ràng cho nguyên lý Tảng băng trôi mà Hemingway đã đề ra vậy. Phần nổi mà ta nhìn thấy, chỉ chiếm một phần rất nhỏ của tảng băng.

Mình vẫn thích cuốn sách bỏ túi của NXB văn học này.. Nó nhỏ gọn, nằm lòng bàn tay, dễ thương .
5
323143
2015-06-04 00:26:43
--------------------------
198189
5451
Mình khâm phục ông lão ngư dân quá, ông đã không từ bỏ con cá kiếm mặc dù bị cá mập rượt theo cho đến khi chỉ còn lại bộ xương của con cá, điều ấy có nghĩa là: hãy theo đuổi ước mơ của mình, đừng nản chí cứ tiến về phía trước như ông lão đánh cá. Cuốn sách này ca ngợi con người, sức lao động và khát vọng của con người. Đây đúng là một tác phẩm hay, có ý nghĩa đối với mọi người trên Trái Đất. Kết thúc câu truyện, ông lão không phàn nàn gì về bọn cá mập gì cả mà ông chỉ sống bình yên như xưa thôi! Thật là một tác phẩm hay!
5
508336
2015-05-19 13:23:28
--------------------------
188969
5451
Không dài, không hề tốn nhiều thời gian để đọc quyển nhày. Nhưng ta sẽ tốn thời gian để có thể hiểu hết ý nghĩa mà tác giả muốn truyền tải. Một ông già gắn bó với biển cả, ông nuôi hy vọng câu được một con cá kiếm khổng lồ. Nhưng đâu hề dễ dàng, ông phải trải qua biết bao gian khổ để đi đến được thành công. Đúng là tác phẩm thuộc hàng kinh điển của thế giới. Nó cho ta thấy được số phận của những con người gian khổ, nhỏ bé nhưng kiên trì chống chọi với sức mạnh to lớn của thiên nhiên. Không gục ngã. Một bài học đáng giá cho nhận loại.
5
400930
2015-04-26 11:40:33
--------------------------
183127
5451
Quá hay! Thật sự xứng đáng là một tác phẩm kinh điển, mình đã đọc liền một mạch mà không thể dứt ra. Chuyện không chỉ dành cho trẻ em mà triết lí thì dành cho người lớn. Với nối viết tự sự, * Ông già* Ernest Hemingway đã đưa người đọc tham gia chuyến đánh cá cùng ông lão, sự kiên trì bền bỉ, sức mạnh của con người luôn chiến thắng được mọi nghịch cảnh. Có niềm tin thì sẽ thành công. Hơn thế nữa là tính nhân văn đối với chính những con cá, đại diện cho thiên nhiên mà ở đó con người cũng là 1 phần trong đó....
Hình ảnh bộ xương của con cá khổng lồ ở bến thuyền cùng với những cái nhìn của những người đi qua nó, là hình ảnh và thông điệp sẽ chẳng bao giờ quên đối với mình. 
"Ông già và biển cả" thật sự là kinh điển. Mình sẽ để dành sau này cho con mình đọc. :D.
Cảm ơn tiki!
5
309333
2015-04-15 15:10:19
--------------------------
173115
5451
Cách xây dựng tình huống truyện của nhà văn rất độc đáo và lôi cuốn, câu văn cô đọng, hàm súc mang nhiều ý nghĩa biểu tượng. Giá trị về mặt nội dung cảu tác phẩm này có lẽ là điều không cần bàn đến, tôi đã đọc rất nhanh vì bản thân cứ như bị cuốn theo từng đợt sóng của câu chuyện vậy. 
_ Về mặt hình thức
+ bìa sách khá đơn giản, không quá cầu kì mà rất đẹp mắt
+ chất lượng giấy tốtm giáy siêu nhẹ cầm rất sướng tay
+ đôi chỗ dịch bị lỗi về mặt cú pháp.
5
538945
2015-03-25 17:24:48
--------------------------
164011
5451
Ông già và biển cả là tác phẩm đáng phải đọc trong số các kiệt tác văn học. Tác phẩm này tiêu biểu cho văn phong Ernest Hemingway, rất cô đọng, súc tích. Lối viết của ông theo nguyên lí tảng băng trôi, theo đó, tâm thức con người như tảng băng, ba phần nổi, bảy phần chìm sâu. Tác giả chắt lọc, chỉ chừa lại những gì súc tích nhất, chừa ra khoảng trống để người đọc tự mình chiêm nghiệm, suy tư. Tác phẩm ông ngắn nhưng bao hàm nhiều tầng ý nghĩa. Đọc ông già và biển cả để thấy cuộc sống tuy khó khăn nhưng thật đẹp, đầy tin yêu và cả chất thơ trong đó.
5
515736
2015-03-06 14:50:19
--------------------------
148324
5451
Đây là một trong những tác phẩm nổi tiếng bậc nhất, là đỉnh cao của Hemingway. Nó đã đi vào sách giáo khoa nhiều nước trên thế giới với nguyên lý "tảng băng trôi" quen thuộc với tất cả học sinh. 
Truyện ca ngợi tinh thần lao động, chiến đấu chống lại sức mạnh của biển khơi. Nhiều lúc như thấy ta đơn độc và nhỏ bé trước những thế lực thiên nhiên hùng mạnh. Cuối cùng, người đi đánh cá là người đi săn hay con mồi? Liên tục chúng ta phải đảo ngược 2 vị trí đó với nhau. 
Câu chuyện kết thúc bất ngờ, như ta vừa được xem một phim ngắn Oscar hay đọc một truyện ngụ ngôn hiện đại vậy.
5
174257
2015-01-10 15:18:08
--------------------------
