530873
5474
Nhịp truyện từ từ, chậm rãi. Mặc dù truyện có nhiều nút thắc, nhưng mình cảm thấy truyện không tập trung sâu vào việc mở nút thắc nhiều mà thay vào đó là hơi lan man về những chuyện ngoài lề. Cảm thấy không hay bằng những tác phẩm khác của tác giả Musso như Hẹn em ngày đó, Và sau đó...
3
619426
2017-02-24 07:20:21
--------------------------
513433
5474
Mình rất thích Marc Levy. Có lẽ bởi vậy nên mình cũng mê mẩn các tác phẩm của Guillaume Musso. Truyện đầu tiên mình đọc là "Ngày mai" và mình chết mệt với nó. Đọc tác phẩm ấy làm mình liên tưởng đến phim The Lake House, cũng như phong cách của Marc Levy.
3
1132079
2017-01-20 10:29:33
--------------------------
364757
5474
Truyện của Guillaume luôn hay như vậy. Dù không có lời giới thiệu cho cuốn sách này nhưng tôi vẫn đặt hàng và thật sự không hề thấy thất vọng.
Sam là một bác sĩ vừa mất vợ còn Juliette là một diễn viên thất bại chán nản muốn quay về quê hương. Hai người gặp nhau tình cờ và phải lòng nhau trong vòng một tiếng. Hạnh phúc trong hai ngày để rồi Juliette phải lên máy bay, nhưng bất hạnh máy bay rơi và tưởng như Sam sẽ rơi vào đau đớn thì tình cờ một người phụ nữ tự xưng là cảnh sát gặp anh và nói rằng cô ấy chưa chết, nhưng vài ngày nữa thôi, chính anh phải dẫn cô ấy đến với thần chết.
Cốt truyện hấp dẫn, tình yêu ngọt ngào, cũng rất ý nghĩa. Đây là một quyển sách cuốn hút!
5
622800
2016-01-06 20:32:04
--------------------------
343017
5474
Quyển sách thứ 2 của Guillaume Musso mà mình đọc_sau cuốn "hẹn em ngày đó"
Hơi bị thất vọng nếu so với cuốn sách đầu tiên mình đọc_nhưng nói z không có nghĩa là nó dở nha :)
Mở đầu là:    
"Khi nghĩ đến anh, trái tim em đập nhanh hơn
 Và điều đó là điều duy nhất có ý nghĩa với em"
<nghe dễ thương hết sức :>
chuyên kể về tình yêu của Sam và Juliette. Cũng giống với "hẹn em ngày đó" ở điểm là cả 2 anh chàng trong chuyện đều cố gắng cứu tình yêu của đời mình ;)))) và cả 2 đều rất tốt ... xứng đáng có được hạnh phúc :)
Bên cạnh đó kết thúc cũng rất bất ngờ nha :> 
3
428279
2015-11-25 16:44:15
--------------------------
337065
5474
Đầu tiên phải nói là mình rất ấn tượng với bìa của quyển sách này. Bìa đẹp, bóng, không dễ bám bẩn. Buồn cười vì lúc đầu mình mua vì nghĩ nó là truyện tình cảm lãng mạn cơ, sau khi đọc mới thấy may quá vì nó không phải truyện tình cảm lãng mạn =)) Nội dung truyện hấp dẫn và có nhiều chi tiết bất ngờ khiến đọc giả đọc xong một trang lại tò mò muốn đọc thêm một trang nữa, cứ như thế, khó có thể dứt ra được cho đến khi đọc đến trang cuối cùng.
4
889506
2015-11-13 15:12:55
--------------------------
298886
5474
Nếu như mọi người thích Marc Levy với những câu chuyện tình cảm động, xướt mướt thì Musso cũng là một nhà văn người Pháp nổi tiếng với thể loại tiểu thuyết tình cảm, nhưng có phần gay cấn hơn. Các tác phẩm của ông thôi thúc chúng ta đọc để tìm hiểu chuyện gì sẽ xảy ra tiếp theo. Nhiều tình tiết thú vị mà tôi dám chắc bạn sẽ không thể nào lường trước được. Và đó chính là điều tạo nên sự đăc biệt, cái hay của những tác phẩm của Musso. Cuốn sách này, lại một lần nữa, "thắng" tôi vì đã tạo ra những quyết định rất bất ngờ nhưng cũng thật đúng đắn.
5
295124
2015-09-12 22:04:49
--------------------------
292033
5474
Một lối hành văn quen thuộc của nhà văn Guillaume Musso, vẫn lôi cuốn, tình cảm, gây gấn xen kẽ chút giả tưởng. Trong truyện này ta như cảm nhận được một tình cảm chân thành, tuyệt vời, một tình yêu mà 2 người đã không vô tình mà đi lướt qua nhau. Hai con người, vô tình gặp, rồi chợt yêu, điều gì đã khiến họ tìm thấy nhau, tin tưởng đó là tình yêu của mình? Nếu như ngày mai sẽ chết trong khi cả cuộc đời bây giờ mới cảm thấy được thật sự là sống thì sẽ ra sao- giằng co, đấu tranh. So với nhiều tác phẩm của Guillaume Musso thì tác phẩm này có thể đoán biết được điều gì đó vào phút cuối, nhưng thật không thể dễ dàng mà chỉ vài trang là có thể hình dung tất cả.
4
475842
2015-09-06 22:38:16
--------------------------
261604
5474
Nếu như so với những tác phẩm khác của Musso mà tôi đã từng đọc thì cuốn sách này chỉ ở mức tạm ổn. Nội dung vẫn ấn tượng và gây bất ngờ, tuy nhiên có một vài điểm không hợp lý cho lắm. Và sự xuất hiện của nhân vật "cậu bé Mile" làm tôi cảm thấy không cần thiết. Nhưng không thể phủ nhận rang tình yêu của Sam Galloway và Julliet that sự đẹp. Lừa dối nhưng chân that và sự hy sinh của cả hai đã chứng tỏ điều đó. Nhìn chung thì tình tiết vẫn lôi cuốn, thu hút người đọc và kết thúc happy ending làm tôi rất hài lòng.
4
82925
2015-08-11 11:23:09
--------------------------
253564
5474
Một đặc điểm rất dễ bắt gặp với các tác phẩm của G. Musso đó là bạn sẽ luôn bị cuốn vào từng trang sách, các tình tiết của câu chuyện liền mạch, thôi thúc ta đi tìm điều gì sẽ xảy ra trong cái thế giới thực thực ảo ảo của ông. Đối với cuốn " Hãy cứu em" này, nó đã mang đến cho mình câu chuyện thật tốt đẹp về tình yêu giữa Sam và Juliette, về một tình bạn trong mơ giữa Sam và Grace.Nhưng " Một giây để làm chao đảo một cuộc tình, một giây để tách rời hai mặt của một đồng xu..." Sam đã nói như vậy khi nhận thấy quyết định sai lầm của mình. Nó nhắc ta hãy trân trọng lấy từng giây từng phút cuộc đời khi ta được ở cạnh người mình yêu, ở bên những người bạn và gia đình.
5
461791
2015-08-04 15:46:16
--------------------------
244480
5474
Mình đọc hãy cứu em sau khi đã đọc hai tác phẩm khác của Musso là: "Central Park" và "Cuộc gọi từ thiên thần" Về vẻ ngoài thì thiết kế bìa chưa đẹp lắm, dù đây là lần tái bản, chưa hiện đại và tinh tế thiết kế của bản gốc. Về nội dung phải nói là hơi hụt hẫn một chút ở những chương đầu, bởi vì mình hâm mộ Musso qua lối viết hiện thực nhưng trong truyện lại xuất hiện nhân vật siêu nhiên. Lúc đầu mình nghĩ rằng nhân vật này sẽ hoặc là thừa hoặc là sẽ đẩy mạch truyện về mô-típ quen thuộc không mới lạ. Nhưng không, Musso thực sự làm mình ấn tượng với cách xây dựng nhân vật không những hợp logic mà còn rất thú vị và khó đoán. Cá nhân mình đánh giá rất cao tác phẩm này.   Điểm cộng thêm nữa là phần miêu tả nội tâm nhân vật dồi dào phong phú nhưng không nhàm chán! Đây là một cuốn sách đáng để bạn thêm vào tủ sách của mình!
5
100155
2015-07-28 11:09:20
--------------------------
222078
5474
Mình đã đọc khá nhiều tiểu thuyết nhưng của các tác giả Trung Quốc . Đây là cuốn truyện nước ngoài đầu tiên mà mình đọc.
 Văn phong của câu chuyện khác hẳn so với những tiểu thuyết ngôn tình mà mình đã từng đọc. 
 Nếu như ở những tiểu thuyết ngôn tình, kết cấu truyện khá quen thuộc thì ở đây, truyện mang một nội dung đặc sắc và sâu lắng.
 Mới đầu mình đọc cũng không quen đâu, thấy nó cứ cứng cứng kiểu j :v nhưng đến trang thứ 10 thì những thứ diễn ra trong cuốn truyện ấy cứ ngấm dần, ngấm dần vào mình.
 Đến khi đọc đến giữa quyển :) Trời ơi, không thể dời mắt được, tình tiết thực sữ thú vị, nó khiến mình tò mò, phải phán đoán.
 Thay vì biết trước được những gì sẽ đến trong những truyện ngôn tình trước thì trong " Hãy cứu em" mình lại có được sự bất ngờ :)
5
688016
2015-07-04 22:29:16
--------------------------
205552
5474
Có lẽ, do cuốn sách này vẫn là một trong những tác phẩm "lót nền" cho những câu truyện đặc sắc được viết sau này nên khi đọc mình cảm giác không thỏa lòng cho lắm. Ngay khi đọc phần giới thiệu của cuốn sách thì mình nghĩ người viết đoạn giới thiệu đó có lẽ cũng cùng cảm xúc với mình.

 Chuyện tình giữa hai nhân vật chính không dữ dội, gay cấn hay hồi hộp mà chỉ là tình cảm dịu dàng thuần túy (nếu không muốn nói là hơi có tý "thiếu muối". Không bằng được "Ngày mai" hay "Cô gái trong trang sách của tác giả". Một cô gái muốn trở thành diễn viên gặp chàng bác sĩ góa vợ khi cô đang diễn vở diễn để đời nhất của mình. Như phim Hàn Quốc, tông vào nhau rồi yêu nhau cả thể. Rồi cô gái bị buộc tội khủng bố đánh bom máy bay chỉ vì cô yêu anh và muốn ở bên anh. ==" Thật sự cảm giác thiếu muối mà. 

Nhưng khi đọc thì vẫn cảm nhận được cách viết, cách dẫn truyện đặc trưng không giống ai của nhà văn.  Trong cuốn này, mình nhặt nhạnh lại được vài câu văn lẫn câu trích ở đầu chương khá hay. Sách dễ đọc, mang tính giải trí hơn là nhặt kiến thức nên cũng không đến nỗi tệ đâu. Dù sao cũng quyết định mua hết 1 bộ toàn Musso về ngâm cứu văn phong nên cũng không đến nỗi đau lòng.
Dịch vụ của tiki thì khỏi bàn rồi. Sách mình được bọc rất vừa. Nhưng đừng ai dại như mình thấy cái bìa nó thít sát vào bìa nilong mà ngồi gỡ ra nha. TT_TT bụi nó chen đầy vô khoảng không giữa hai cái bìa.
2
285978
2015-06-07 06:17:34
--------------------------
162034
5474
Có lẽ mình không hợp với những cuốn sách kết hợp nửa trinh thám nửa tình cảm như của Guillaume Musso, dù nó được mọi người khen ngợi rất nhiều.
Sam, nhân vật chính trong truyện, dù sau bao năm cũng không thể quên được người vợ đã tự tử của mình, anh vùi đầu vào công việc. Vậy mà chỉ qua một lần suýt đụng xe trúng Juliette, nói chuyện với cô, anh đã phải lòng cô ngay lập tức, Juliette cũng vậy. Liệu như thế có nhanh quá chăng? Đó là về phương diện tình cảm. Còn trinh thám thì sao? Một cái kết quá dễ đoán. Nhưng điều này hãy để các bạn tự suy luận khi đọc truyện ^^. Với mình, truyện khá êm đềm dù có thêm yếu tố trinh thám. Sách chỉ dừng ở mức độ bình thường chứ chưa thể đạt tới đặc sắc. Mình cũng đang đọc dở Nếu đời anh vắng em nhưng chuyển sang quyển khác rồi, đọc nhiều sách của Guillaume Musso sẽ thấy khá ngán. Điểm cộng của tác phẩm là lời lẽ mượt mà, câu văn giản dị khiến bạn có thể đọc một lèo từ đầu đến cuối tác phẩm mà không cần nghỉ giữa chừng. Sẽ đáng mua dành cho những bạn nào không đặt nặng quá vấn đề về tình cảm hay trinh thám.
3
92868
2015-03-01 13:26:54
--------------------------
