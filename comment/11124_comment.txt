482668
11124
Nội dung không đặc sắc lắm nhưng những tình huống có sự bất ngờ, nói về hiện thực sống của tầng lớp đa số là thanh thiếu niên hiện nay, dành phần lớn thời gian cho thế giới ảo, chơi game và đắm chìm vào các nhân vật ảo, đôi khi họ lại không phân biệt đc đâu là ảo đâu là hiện thực, những người lớn hơn lại coi việc câu view và danh vọng trong thế giới online lại là thành công trong thực tế. Những nút thắt và mở trong tập truyện này tuy ko kịch tính và hấp dẫn nhưng cũng đáng để đọc qua và rút ra nhiều bài học cho chúng ta trong cách hành xử lẫn lời nói trong cuộc sống hay cả vs thế giới online.
3
329159
2016-09-08 07:41:39
--------------------------
421337
11124
Đây là cuốn sách tiếp theo sau cuốn "Búp bê đang ngủ" trong series về nữ đặc vụ Kathryn Dance. Vẫn là lối viết quen thuộc của Deaver, hấp dẫn, các tình tiết đan xen lên nhau làm độc giả khó lòng đoán biết được đâu mới là chân tướng thực của sự việc. Cuốn sách làm bạn hồi hộp ngay ở trang đầu tiên và bạn khó lòng có thể đặt cuốn sách xuống nếu chưa đọc hết. Về hình thức thì sách rất đẹp, giấy in rõ ràng, rất ít lỗi chính tả, đặc biệt mình rất thích hình minh họa, rất phù hợp với nội dung của quyển sách.
4
122042
2016-04-24 21:58:32
--------------------------
413750
11124
Sau khi đọc xong quyển Búp bê đang ngủ mình rất ấn tượng với dòng truyện trinh thám của tác giả Jeffry Deaver. Mình dự định sẽ sưu tập toàn bộ trọn bộ truyện của ông vì mình rất mê thể loại hình sự-trinh thám này. Hôm nay mình quyết định mua quyển Cây thập tự ven đường nhìn bìa sách rất ấn tượng không biết nội dung như thế nào. Sau vài ngày đọc xong mình bị cuốn hút theo từng trang sách gây cấn đến nghẹt thở, giá mà truyên này có thể làm phim thì hay biết mấy.
5
509425
2016-04-10 11:17:42
--------------------------
400193
11124
Cùng seri về nhà phân tích tâm lý học tội phạm Kathryn Dance, so với "Búp bê đang ngủ" thì mình ấn tượng về cuốn này hơn. Một phần vì cách mở đầu tác phẩm, lẫn các vụ án xen lẫn vào nhau làm cốt truyện trở nên hấp dẫn, khó đoán hơn.
Phần thứ hai, cuộc sống trong thế giới ảo, những kiến thức về Internet của tác giả khá hay. Tuy kết truyện thủ phạm khó ai ngờ tới, nhưng nếu đọc và để ý kỹ càng các chi tiết và diễn biến thì cũng không khó để đoán ra kẻ đứng đằng sau vụ án.
4
182482
2016-03-18 21:55:07
--------------------------
382718
11124
Một cuốn trinh thám hình sự về "cỗ máy nhận biết nói dối" Dance. Mình thích nhân vật này từ khi đọc Trăng Lạnh, và cảm thấy đây là một nữ cảnh sát tuyệt vời.
Không còn dựa quá nhiều vào công nghệ, phân tích chứng cứ như Lincoln Rhyme, Dance đưa đến làn sóng mới.
Cuốn sách nói lên vấn đề hiện nay của giới trẻ: công khai quá nhiều thông tin cá nhân lên mạng xã hội, và chính điều đó có khả năng gây hại cho họ.
Cũng có một số tình tiết hơi thót tim, và cái kết cũng khá bất ngờ khiến cho câu truyện dồn dập và kịch tính.
So với bản cũ, cuốn Cây Thập Tự Ven Đường này khiến mình vừa lòng.
5
712991
2016-02-19 15:35:38
--------------------------
293886
11124
Mình vốn dĩ là 1 fan của thể loại truyện trinh sát hình sự và cũng là 1 fan trung thành của Jeffery Deaver nên khi Tiki cho ra tác phẩm này thì mình đã không ngần ngại mà mua ngay nó về. Ấn tượng ngay với tiêu đề của tác phẩm cũng như hình ảnh những cây thập tự đã đem lại cho mình cảm giác kịch tính rồi. Khi đọc xong nó thì mình cảm thấy đây là 1 tác phẩm thành công tuy rằng chưa được xuất sắc như các tác phẩm khác của Jeffery Deaver. Phải nói rằng ông là bậc thầy trong việc dẫn dắt đọc giả từ bất ngờ này đến bất ngờ khác. Mọi tình tiết cứ úp úp, mở mở và khiến người đọc nghĩ đến nhiều khả năng nhưng thật ra mọi chuyện lại không phải vậy. Về kết thúc câu chuyện thì cũng như mong đợi nhưng mình cần 1 điều gì đó kịch tính hơn, ly kì hơn nữa nhưng dù sao theo mình thì đây cũng là 1 tác phẩm hay
4
39383
2015-09-08 20:29:40
--------------------------
283738
11124
Người dịch:		Lê Đình Chi
NXB:			Bách Việt – Thời Đại
Thể loại:		Tâm lý hình sự
Bìa:			Đẹp
Chất lượng in, giấy: Tốt
Chất lượng dịch:	Tốt
Nhận xét:		 Nếu bạn thích tâm lý hình sự thì nên chọn cuốn sách này. Phần đầu dẫn nhập nhẹ nhàng, đôi lúc có cảm giác mất nhịp, rồi tiết tấu tăng dần, hồi hộp, nghẹt thở, và tràn ngập bất ngờ thú vị. Người đọc bị đánh lừa hết lần này đến lần khác khi nhập vai Kathryn Dance cùng truy tìm thủ phạm các vụ giết người. Tác giả quá thông minh.
Khuyên: 		Hay, nên đọc.

5
129879
2015-08-30 11:24:19
--------------------------
248596
11124
Bì sách, tựa sách và hình những "cây thập tự ven đường" gợi lên cảm giác chết chóc, ớn lạnh và con đường dài dẫn vào nghĩa địa. Tuy nhiên, cốt truyện không miêu tả cái chết, cũng không mang âm hưởng chết chóc mà còn đưa người đọc vào cuộc phiêu lưu với những vụ ám sát bí ẩn, huyễn hoặc, càng đọc càng bị kích động. Với riêng tôi, khi đọc tác phẩm tôi còn có cảm giác ớn lạnh và đề phòng như thể chính mình đang đi trên con đường đầy những cây thập tự và còn sắp trở thành nạn nhân của vụ ám sát vậy. Tuy rằng, truyện còn khá mờ nhạt ở việc miêu tả tâm lý nhân vật, tuy nhiên tác giả đã khá thành công ở thể loại trinh thám. Cuốn sách này đã cầm lên thì không thể bỏ xuống được!
5
24486
2015-07-31 10:06:38
--------------------------
238732
11124
Đọc quyển này bạn phải đọc một mạch, nếu ngưng một hay hai ngày là sẽ quên sạch và khó theo kịp tình tiết. Nội dung ban đầu thì có vẻ rất gay cấn, nhưng đến đoạn cuối thì hơi đuối. Có rất nhiều đoạn khó hiểu và dư thừa. Cho tới đoạn theo dõi cậu bé chơi game, truyện vẫn hấp dẫn. Chỉ có sau đó thì tình tiết khá rắc rối một tí và hết đáng sợ như trước. Đoạn đầu vừa đọc vừa run vừa hồi hộp, đoạn sau chỉ là tò mò không biết vụ án kết thúc ra sao. Ai mới đọc trinh thám thì quyển này sẽ chấp nhận được, còn với các fans truyện trinh thám lâu ngày thì quyển này hơi nhạt nhẽo.
3
55364
2015-07-23 13:30:23
--------------------------
227376
11124
Về vụ án cây thập tự ven đường, có thể chia thành hai luồng truyện: một luồng về cậu bé mê điện tử được xem là nghi can chính, và một luồng về hung thủ thực sự. Ban đầu khi mới đọc thì hai luồng này có vẻ khớp nhau, song thực ra  không đơn giản thế. ĐIều khiến mình thất vọng là luồng về cậu bé thì được miêu tả chi tiết , tỉ mỉ, phân tích kĩ càng, trong khi luồng truyện nói về hung thủ lại khá hời hợt. Nhất là đoạn sau khi nói về động cơ gây án và hung thủ dần lộ diện, thì hành động của hung thủ lại càng lủng củng và khó hiểu. Dù vậy, mình vẫn khá thích truyện này. Đoạn nói về cậu bé mê chơi game khá đáng sợ, như một lời nhắc nhở đến những người lớn hãy quan tâm hơn việc con cái mình đang chơi game gì, nó có thể thực sự trở thành một cái màn đen bao phủ tâm trí. 
4
82774
2015-07-14 08:26:23
--------------------------
224625
11124
Tôi thường thích những cuốn sách của Jeffery Deaver, nhưng Cây thập tự ven đường là một ngoại lệ, nó hoàn toàn không tạo cho tôi chút ấn tượng nào cả. Diễn biến câu chuyện thay đổi thất thường, chuyển cảnh không tốt. Ở nhiều chỗ, ngôn ngữ được sử dụng có một chút nhạt nhẽo và dài dòng, khiến cho người đọc dễ xao nhãng. Tôi vẫn không hoàn toàn chắc chắn lý do tại sao thủ phạm lại giết rất nhiều nạn nhân như vậy. Động cơ giết nạn nhân chính của hắn thì rõ ràng, nhưng các nạn nhân còn lại hầu hết có vẻ là ngẫu nhiên. 
Tóm lại, đây là một cuốn sách khá dành cho các kỳ nghỉ và một ví dụ thú vị về một cách tiếp cận giúp người đọc tương tác nhiều hơn bằng cách kết nối những câu chuyện với nội dung trên internet :)
3
223142
2015-07-08 23:10:37
--------------------------
214631
11124
Thật sự mà nói truyện này không như mình mong đợi, mình được nghe giới thiệu rất nhiều về truyện này nhưng khi đọc xong mình hơi thất vọng.Câu chuyện xoay quanh những tình tiết gây cấn trong vụ cây thập tự, nhưng kết cấu truyện quá dài, đôi lúc mình cảm thấy mệt mỏi, diễn biến tâm lý thì quá phức tạp và rắc rối, không có nhiều tình huống gợi tò mò, suy nghĩ cho người đọc.
Về nội dung là như thế, nhưng về chất lượng giấy của sách thì khá tốt, chữ in rõ ràng.Nói chung tuy thất vọng 1 chút nhưng dù sao mình cũng cho 3 sao
3
368991
2015-06-25 11:20:40
--------------------------
211000
11124
Với những độc giả trung thành với truyện trinh thám của Jeffery Deaver, hẳn các bạn có thể dễ dàng nhận ra phong cách viết truyện của ông là vô cùng mạnh mẽ và cuốn hút với Sát nhân mạng, Giọt lệ quỷ, lá bài thứ 12 ... các tình tiết trong truyện gay cấn, hồi hộp, ly kỳ, dẫn dắt người đọc đi hết từ bất ngờ này sang bất ngờ khác. Có lẽ nói về nội dung và kết thúc của một câu chuyện trinh thám là không nên, văn phong của "Cây thập tự ven đường" có phần nhẹ nhàng hơn nhiều so với những truyện khác của ông, nội dung của nó tập trung nhiều vào điều tra phá án, có lẽ vì vậy mà mình đọc cả nửa quyển mà cứ cảm thấy hơi nhạt nhẽo vô vị, nhưng dù sao thì nó cũng khiến mình không thể bỏ xuống khi mà chưa biết kết cục ra sao. Đây có lẽ là điểm độc đáo trong lối viết của J.Deaver
3
420160
2015-06-19 23:35:06
--------------------------
147894
11124
Thực sự thì với cảm nhận bản thân về truyện của J. Deaver nói riêng và trinh thám nói chung thì tác phẩm này chỉ dừng ở mức tạm chấp nhận được. Thực tế thì trông tác phẩm giống một vụ điều tra phá án như 1 bộ phim Hollywood hơn là một tiểu thuyết trinh thám đúng nghĩa.
Xuyên suốt vụ án bạn rất ít gặp những trường hợp suy luận độc đáo một cách Logic tài tình mà chỉ phỏng đoán mơ hồ dựa trên nhiều tình tiết một cách chậm rãi sau mỗi vụ án, khi mà mở màn và đến trường đoạn cuối vẫn rất ít sự bất ngờ xảy ra, cho đến cuối tác phẩm mới dần được hé lộ thì tuy có bất ngờ nhưng chắc ai cũng đoán được một phần rồi. Nếu ai từng đọc Sát Nhân Mạng sẽ thấy những yếu tố bất ngờ và dồn dập như thế nào cho đến cuối cùng khi mà mọi nghi vẫn đều vào ngõ cụt rồi hướng sang sự bất ngờ mới.
Việc miêu tả nội tâm nhân vật cũng không thực sự sâu sắc khi quá mức tập trung vào Dance mà quên đi hầu hết các nhân vật khác, nếu có cũng chỉ ở mức khái quát bên ngoài là chính, hầu như bạn sẽ chẳng ấn tượng quá về các nhân vật phụ xung quanh cô. Thực tế thì cả quyển mình chỉ hơi ấn tượng mỗi anh chàng Giáo Sư máy tính và Game thủ Travis mà thôi, tuy đất diễn không nhiều và không có gì nổi bật thật sự.
Một vài điểm nhấn như cách phân tích hành vi cơ thể và suy nghĩ của Dance thực sự mới mẻ và ấn tượng, dù chỉ giúp việc phá án vài ba lần, còn lại chủ yếu là mang tính cá nhân. Hành động thầm lặng của người đồng nghiệp M cho đến cuối vụ án để giảm sự mờ nhạt của anh, dù mất mát bản thân quá nhiều về tính yêu để rồi cuối cùng bị chàng Giáo Sư nẫng tay trên 1 cách khá buồn - nhưng mình thích bác Giáo Sư ở điểm này.
3
451219
2015-01-09 09:06:58
--------------------------
139558
11124
Thực sự thì mình rất ghiền tiểu thuyết của J. Deaver, nhưng một số cuốn gần đây mất đi sự cuốn hút, gay cấn đến nghẹt thở thường thấy trong những tác phẩm trc mình đã đọc của ông như: Kẻ tầm xương, Vũ điệu của thần chết, Trăng lạnh, Chiếc ghế trống, v.v..
Tuy rằng Cây thập tự ven đường nằm trong serie truyện về nữ đặc vụ K. Dance, khác hẳn với những suy đoán từ bằng chứng hiện trường mà chúng ta làm quen được với L. Rhyme, bằng chứng thu thập được không rõ ràng, ko có các dấu vết vi mô tìm được. Vì vậy, cuốn "Cây thập tự ven đường" làm cho người đọc (như riêng mình) cảm thấy rất bức bối khi những manh mối không dẫn đến đâu.
Truyện đọc khá cuốn hút, không làm chúng ta nhàm chán nhưng không được gây cấn đến phút cuối. Tuy rằng kết cục về thủ phạm chính xác thường là những người chúng ta không nghĩ đến (và là phong cách viết của J. Deaver), nhưng cũng cảm thấy không quá bất ngờ.
Nói chung đây là một cuốn đáng mua, đáng đọc về serie nữ đặc vụ Dance, chúc các bạn đọc sách vui vẻ :)
4
474652
2014-12-07 19:04:19
--------------------------
123152
11124
Nếu đã từng đọc tác phẩm "Búp bê đang ngủ" chắc chắn bạn cũng sẽ không thể bỏ qua một tác phẩm nổi tiếng khác của nhà văn Jeffery Deaver về nữ đặc vụ thông minh Kathryn Dance - một chuyên gia về ngôn ngữ cơ thể. Một đề tài mới lạ về cây thập tự ven đường. Cây thập tự ven đường không chỉ gợi lại nỗi ám ảnh về những tai nạn thương tâm trên các xa lộ mà còn là lời gợi ý về các vụ án sắp diễn ra.
Điểm cộng của tác phẩm đầu tiên phải nói đến là Bìa truyện rất thu hút, hình ảnh cây thập tự đứng trơ trọi trên tuyến đường dài như vô tận; dịch thuật hay, hiếm khi gặp lỗi chính tả; các nhân vật được lột tả một cách chi tiết; diễn biến câu chuyện thu hút, ai cũng có thể trở thành nạn nhân, mục tiêu của kẻ giết người và hung thủ thực sự lại chính là người bạn không thể ngờ tới nhất.
điểm trừ của tác phẩm theo mình thì không có. 
Đây thật sự là một tác phẩm dành cho các fan truyện trinh thám!
4
296992
2014-08-30 14:23:53
--------------------------
122042
11124
Thực sự mình mới biết tên tác giả này gần đây thôi. Và đây cũng là quyển đầu tiên mình mua của nhà văn này.
Rất lôi cuốn! Vừa qua 1,2 chương là mình thấy nội dung hứa hẹn nhiều điều bất ngờ chờ mình khám phá. Kathryn Dance, một nữ đặc vụ goá chồng, luôn là một tấm gương tốt đẹp cho mọi người trong nghề. Cái cách cô điều tra vụ án cây thập tự ven đường rất chi tiết, tỉ mỉ, thể hiện sự chuyên nghiệp lâu năm. 
Xen lẫn những tình tiết hồi hộp, bí ẩn, cũng là chuyện tình cảm nhẹ nhàng giữa các nhân vật. Không thể phủ định chuyện đó nhưng thực sự rất tuyệt vời. 
Bất ngờ nhất là khi biết được thủ phạm thực sự của vụ án là ông chủ blog Chilton chứ không phải là cậu bé Travis. Sự bất ngờ đó cũng đem lại phần nào thành công cho tác phẩm. Mình không hề thấy thất vọng chút gì, thay vào đó là niềm yêu thích các tác phẩm như vậy.
5
340113
2014-08-22 21:19:14
--------------------------
118636
11124
Tiểu thuyết của Jeffery Deaver vốn rất ăn khách nên mình khá tò mò. Và quyết định chọn quyển sách này đã không làm mình thất vọng.

Tình tiết rất lôi cuốn, các đoạn miêu tả cũng khá sống động khiến nhiều lúc mình không thể buông xuống được. Mình đặc biệt thích các đoạn mà đặc vụ Dance lấy khẩu cung của nhân chứng. Việc thu thập chứng cứ qua ngôn ngữ cơ thể cũng như quá trình phân tích được nêu ra khá chi tiết giúp người đọc hình dung rõ ràng. Mình còn nghĩ có thể tự áp dụng ngoài đời nữa :p

Những vụ án lồng vào nhau làm phân tán sự chú ý của người đọc để đưa đến một kết thúc hết sức bất ngờ. Mình cũng thử đoán hết người này tới người khác mà rốt cuộc lại ra một người hoàn toàn không nghĩ đến... (nhưng không hiểu sao mình vẫn thấy chưa được thuyết phục lắm)  

Không chỉ có những vụ án cần xử lý mà những chi tiết tình cảm cũng được xen vào rất khéo léo giúp giảm bớt căng thẳng ^^. 

Tuy nhiên, có lẽ chưa theo dõi các tập trước nên mình hơi bị lẫn lộn vì có khá nhiều nhân vật. Đôi khi quên mất anh này là ai làm mạch truyện bị đứt đoạn.

Mình có xem qua nhận xét mấy quyển trước có khá nhiều lỗi chính tả nên để ý kỹ. Nguyên quyển dày cộm này chỉ khoảng 10 lỗi chính tả thôi :p 

Một quyển sách đáng đọc với cái nhìn về máy tính, mạng xã hội và tác động của chúng đến những người trẻ tuổi. Những thông tin đưa lên mạng thật sự nguy hiểm hơn chúng ta tưởng đó!
4
32394
2014-07-30 11:27:57
--------------------------
