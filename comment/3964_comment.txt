543217
3964
Sách Phụ Nữ Tuổi 20 Để Thành Công là quyển sách đáng đọc dù bạn là phụ nữ tuổi 19 hay phụ nữ tuổi 22,26.
5
592026
2017-03-15 12:45:17
--------------------------
451345
3964
Phụ Nữ Tuổi 20 Thay Đổi Để Thành Công là một cuốn sách cho những bạn gái như chúng ta. Trong sách đề cập đến mọi mặt trong cuộc sống, từng mẫu chuyện là những bài học và đưa ra những kinh nghiệm cho bản thân mình. Để hoàn thiện mình hơn qua từng trang sách, sách này không phải hoàn hảo nhất nhưng nó giúp ta có một sự mạnh mẽ hơn và mạnh dạn thay đổi để thành công hơn. Mọi chuyện sẽ trở nên hấp dẫn và thú vị hơn khi chúng ta thay đổi, lựa chọn một cách khôn ngoan
4
821492
2016-06-19 17:34:41
--------------------------
442636
3964
“Phụ nữ tuổi 20 – Thay đổi để thành công” là một quyển sách hay dành cho các bạn gái. Mình đã mua quyển sách này để tặng cho em gái cuả mình.
Sách bàn về những kỹ năng sống , cách ứng xử mà phụ nữ tuổi 20 nên rèn luyện.
Chia sẻ những câu chuyện nhỏ giúp bạn đọc dễ hiểu , dễ hình dung.
Giúp bạn có cuộc sống vui vẻ , hạnh phúc , thành công :)
Điểm trừ là trong quá trình vận chuyển sách đã hư mất mép sách điều này khiến mình không hài lòng lắm. Hi vọng những lần đặt hàng tới không còn xảy ra tình trạng này
3
632740
2016-06-05 13:02:22
--------------------------
415340
3964
Mình muốn mua một quyển sách dạy kỹ năng cho cô em gái nhỏ ở tuổi 20. Sau khi suy nghĩ, mình quyết định chọn một tác giả người Việt để mở đầu cho quá trình thay đổi tư duy của em vì tác giả Việt Nam chắc chắn sẽ có suy nghĩ và định hướng phù hợp hơn người nước ngoài. Mình thấy em gái cũng thích đọc và hay đọc đi đọc lại, cũng có thay đổi tích cực, trưởng thành hơn. Mình nghĩ quyển sách như vậy là hữu ích rồi. Phần hình thức cũng ổn. Giá có cao hơn so với các quyển sách dịch cùng loại một tí nhưng chấp nhận được.
3
255882
2016-04-13 10:59:10
--------------------------
368764
3964
Em mua cuốn sách này để tặng chị gái nhân sinh nhật 20 tuổi, Mới tặng nên cũng j biết chị ấy thấy như thế nào nhưng về giấy và cách thiết kế em thấy rất ổn ạ
em cảm thấy mình đã lựa chọn đúng
Cảm ơn Tiki ạ! Luôn ủng hộ Tiki, giảm giá thường xuyên, thái độ phục vỵ và gói hàng tốt, vì mấy lần gọi tổng đài và đều được đáp lại rất tốt ạ
Yêu Tiki ah
mong chị em sẽ vui khi nhận món quà này! Chúc Tiki luôn thành công như thế này
5
523572
2016-01-14 16:09:55
--------------------------
295443
3964
Mình rất thích quyển sách ngay từ những trang đầu viết về nghệ thuật trong tình yêu. Thật sự rất hay và hữu ích. Bên cạnh đó, tác giả còn chia sẻ về những mẫu chuyện nhỏ giúp người đọc dễ hiểu và dễ hình dung. Đặc biệt, mình thích quyển sách này vì tác giả cũng đề cập đến một số thuật ngữ liên quan đến kinh tế như "Lý thuyết trò chơi", "Tổng bằng 0", "Ngục tù song nhân",.. nhưng lại đưa ra ví dụ liên quan rất sinh động. Mình nghĩ quyển sách rất phù hợp với mình và đặc biệt là các bạn nữ đang học chuyên ngành kinh tế.
5
74603
2015-09-10 11:19:08
--------------------------
293001
3964
Phụ nữ 20, thay đổi để thành công. Mình rất ấn tượng với tên cuốn sách này nên đã quyết định mua nó. Bìa sách cũng khá đẹp, nhưng hơi mỏng. Cuốn sách nói về các kĩ năng sống, các cách nghĩ, cách chọn lựa về việc định hướng, làm thêm. Sách này rất hữu ích với mình, mình cảm thấy như vậy, vì kiến thức xã hội của mình còn hơi kém! Tuổi 20 là tuổi xảy ra nhiều chuyện, từ tình bạn, tình yêu đến công việc đều dồn vào, nên việc chọn, định hướng là rất quan trọng và hữu ích. 
5
451462
2015-09-07 21:27:43
--------------------------
249455
3964
Nhìn chung thì nội dung sách khá cần thiết đối với các bạn nữ. Sách bàn về những kĩ năng sống, cách ứng xử mà phụ nữ chúng ta nên rèn luyện mỗi ngày để trở nên trưởng thành và thành công trong công việc cũng như cuộc sống. Tuy nhiên, đối với người đã đọc nhiều sách về kĩ năng sống như mình thì thấy cách trình bày của tác giả vẫn còn chung chung, phần tổng kết cuối không được rõ ràng lắm, vẫn có một số điểm không thể hiện được tính nhất quán và thuyết phục.
4
594849
2015-07-31 16:41:09
--------------------------
246073
3964
Thiết kế của quyển sách rất đẹp, màu in rõ, có hình minh họa sinh động, khiến mình cảm thấy rất thích thú khi đọc. Theo mình nghĩ, đây sẽ là quyển sách phù hợp cho những bạn mới vào trường đại học vì nó hữu ích cho việc 4 năm đại học của bạn, nó bao gồm chuyện định hướng, tình cảm, làm thêm hoặc cho những mối quan hệ xã giao. Giúp cho bản thân chúng ta có thêm kinh nghiệm để lựa chọn người đàn ông đời mình, quản lý tài chính phù hợp cũng như cách tiết kiệm. Vậy hành trang tuổi 20 bạn cần chuẩn bị là cái gì, hãy cho đọc ngay nào.
5
111377
2015-07-29 17:33:13
--------------------------
226840
3964
Chất liệu giấy không tốt , khá mỏng nhưng nội dung thì cũng tạm ổn. Một số câu chuyện hay đáng để học hỏi và rút kinh nghiệm nhưng cũng có một số chưa giải đáp hoàn toàn câu chuyện làm cụt hứng... Cộng thêm có lý thuyết trò chơi để phân tích tình huống trong câu chuyện hơi chán và khó hiểu ... Hay tại vì mình dốt toán quá nên thế ! Nói chung thì cư mua về tham khảo qua và biết đau nó giúp ích cho một số bạn gái cần giải quyết vấn đề mà không thể tâm sự cùng ai ... :)
3
689328
2015-07-13 01:32:18
--------------------------
213622
3964
“Phụ nữ tuổi 20 – Thay đổi để thành công” là một quyển sách hay dành cho các bạn gái. Mình đã mua tặng quyển sách này cho bạn gái mình và cô ấy rất thích nó. Con gái bước qua tuổi 20 sẽ có nhiều bước ngoặt trong cuộc sống, đôi khi không tránh khỏi cảm giác chông chênh, bất an. Quyển sách này sẽ giúp bạn vững vàng hơn, khôn ngoan hơn trước những sự thay đổi, lựa chọn trong cuộc sống. Sách nói về mọi tất cả các vấn đề mà các bạn nữ hay quan tâm; từ tình yêu, sự nghiệp, quản lý tài chính, xã giao,....Các bạn nữ nên đọc để học hỏi và hoàn thiện mình hơn nữa :)
5
554150
2015-06-23 22:49:37
--------------------------
