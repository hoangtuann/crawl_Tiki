435142
7053
Nằm trong chuỗi sách Mùa hè ngọt ngào, Quyển 3 có nâng dần cấp độ khó của từ ngữ, không còn là những vần thơ, thay vào đó là một câu chuyện, giúp bé phát triển kỹ năng đọc ở cấp độ cao hơn. 
Ngoài ra, sách còn chứa đựng bài học sâu sắc về tình bạn và việc cân nhắc có nên quá ôm đồm công việc vào bản thân hay không?
Hình vẽ dễ thương, dạy bé về các con vật trong rừng, tuy nhiên có vẻ khá rối để bé nhỏ có thể phân biệt được các con vật.

3
1017785
2016-05-24 15:01:09
--------------------------
229225
7053
Một câu chuyện rất dễ thương về bé lừa Xi mê ôn tốt bụng, chú không bao giờ ngần ngại khi có ai cần giúp đỡ, hàng ngày chú thường đưa báo cho bác Lửng, mua đồ cho cô chuột chũi và cõng bác Cáo già đi chơi. Tuy nhiên đến một ngày khi cậu bị ốm thì khi đó mọi người từ tức giận đã chuyển thành lo lắng và tất cả đêu đến chăm sóc Xi mê ôn giúp cho cậu cảm nhận được tình bạn bè rất đáng giá.
Một điểm cộng của chuyện là hình ảnh rất đẹp, nhiều màu sắc và cực kỳ dễ thương
5
153008
2015-07-16 16:49:42
--------------------------
