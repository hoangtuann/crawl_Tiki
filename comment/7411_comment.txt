256113
7411
Cuốn sách được in trên chất liệu giấy tốt. Câu chuyện có nội dung ca ngợi cậu bé có trí thông minh và lòng dũng cảm giúp cho các anh trai thoát khỏi nguy hiểm. Tuy nhiên nhiều tình tiết trong câu chuyện có lẽ không phù hợp cho trẻ nhỏ như tính tiết cha mẹ bỏ con vào rừng hay tinh tiết lão quỷ cắt cổ con. Những tình tiết như vậy cần được cha mẹ giải thích và rút ra những bài học mang tính tích cực cho trẻ, tránh trường hợp để trẻ tự đọc và tự hiểu.
1
415825
2015-08-06 16:33:08
--------------------------
