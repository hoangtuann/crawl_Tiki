542781
3678
Tiki đóng gói kỹ và giao hàng nhanh. sách rất nhẹ, in rõ ràng, tuy nhiên chất giấy ko được trắng cho lắm. Mình mua được lúc giảm giá còn 46k. nội dung sách bổ ích, có cả tiếng anh và tiếng việt, CD kèm theo ko có hộp đựng nên có vẻ dễ vỡ vì sách rất mỏng.
5
1361298
2017-03-15 09:34:32
--------------------------
488504
3678
Mình mua quyển sách này để luyện thi N3. Nghe là kỹ năng mình không tự tin nhất, tuy nhiên nhờ quyển sách này, kỹ năng nghe của mình cải thiện hơn rất nhiều, tuy nhiên có cảm giác nghe CD kèm theo sách tốc độ đọc nhanh hơn khi đi thì thì phải, nhưng điều đó lại tốt hơn để mình rèn luyện. Sách được trình bày rất dễ thương, tam ngữ luôn (nhật, anh, việt) nên bên cạnh tiếng Nhật thì tiếng anh cũng được rèn luyện chút chút. Mình cũng mua luyên cả bộ gồm kanji, reading, grammar nữa để bổ trợ thêm. Bổ ích lắm nha!!! 
4
678614
2016-11-03 11:37:12
--------------------------
455076
3678
Mình mua cả bộ somatome này để tự ôn thi N3 nhưng lúc mình mua Tiki hết quyển ngữ pháp nên mua được có 4 quyển thôi. Bộ sách này thì được rất nhiều người nhắc đến khi cần luyện thi năng lực tiếng Nhật nên mình cũng lựa chọn bộ sách này. Sách thiết kế rất dễ hiểu, tiện dụng mỗi bài nghe đều có đáp án kèm các bài nghe để mình tự xem lại. Sách có hình minh họa khá dễ thương, cùng với chủ đề đa dạng nữa nên khi tự ôn cũng không quá nhàm chán.
4
165179
2016-06-22 12:59:03
--------------------------
451907
3678
Tiki giao hàng rất nhanh, may mà mình mua được trọn bộ 5 cuốn. Sách thiết kế học mỗi ngày 2 trang nên học không bị quá tải và mình có thể phân bổ thời gian học mỗi cuốn một ít. Sau mỗi tuần học còn có bài test như kì thi năng lực nhật ngữ để kiểm tra xem mình tiếp thu được tới đâu nhiều hay ít nên mình rất hài lòng với bộ sách này. Tuy vậy có 1 điểm trừ là chất lượng giấy không được tốt, màu giấy hơi tối và nhám. Mong là nếu tiki có somatome N2 thì chất lượng giấy được cải thiện hơn.
5
285656
2016-06-20 10:55:32
--------------------------
447040
3678
Mình rất hài lòng khi đặt mua sách online tại Tiki.vn , luôn có những đầu sách giảm giá tốt, xử lý đơn hàng mau lẹ, giao hàng nhanh chóng, chất lượng sách tốt, mới y như những cuốn sách bán ngoài nhà sách. Mình biết cuốn sách này thông qua bạn bè! Một đứa học tiếng nhật thì luôn có niềm đam mê với sách như tớ! Cầm cuốn sách trên tay mới được giao tới, lòng rất vui như được quà! Sách yên trên giấy tốt, bìa đẹp, chữ nghĩa rõ ràng, có giải thích, đáp án, các bạn nên đặt thêm bookcare để bảo vệ sách!
5
639443
2016-06-13 13:26:33
--------------------------
396503
3678
Soumatome nghe hiểu n3 là một trong 5 cuốn trong bộ giáo trình nổi tiếng học thi n3. Nghe hiểu là một kĩ năng cần rèn luyện trong thời gian dài nên cuốn sách này phù hợp với các bạn chuẩn bị cho kì thi một cách nghiêm túc. Điểm hài lòng là ở cuốn sách này  là có kèm CD theo sách, rất tiện mà không mất công tìm kiếm trên mạng như giáo trình minna no nihongo. Ngoài ra , chất lượng giấy in khá ổn so với giá tiền phải bỏ ra, một cuốn sách không thể thiếu khi ôn n3
4
151541
2016-03-13 16:32:58
--------------------------
385632
3678
Nằm trong bộ sách luyện thi năng lực nhật ngữ n3, đây là sách không thể thiếu đối với những người học tiếng nhật.  Sách được biên soạn theo dạng gần giống  bài thi, giúp người học dễ dàng tiếp cận, làm quen với kỳ thi năng lực nhật ngư . Bài học dễ hiêu,sinh động. Đĩa không bị trầy xướt.  Nhưng mà giấy thì không được đẹp lắm thôi nha các bạn. Giá thành rất rẻ nhưng nội dung sách lại rất ok đó. Mình mua cả thẩy 4 cuốn luôn rồi. Tiki luôn giao hàng nhanh và luôn có khuyến mãi,rẻ hơn bên ngoài
5
564937
2016-02-24 12:08:12
--------------------------
365601
3678
Còn một số phần chưa được dịch, hơi khó hiểu một chút. Ngoài ra sách được chia thành các phần rất hợp lý, phù hợp để ôn thi trong thời gian1-2 tháng. Các phần nghe sử dụng những ví dụ rất gần gũi. Rất tốt. Sách có màu giấy hơi vàng, nếu trắng hơn một chút thì tốt. Chữ cái cũng được in to, rõ ràng. Là một quyển sách luyện thi tốt. Mình rất thích cuốn sách này.
Tiki bao bọc sách rất cẩn thận, rất đẹp. Mình rất hài lòng. Giao hàng đúng hẹn, cái này mình thích nhất. Cám ơn tiki.
4
625880
2016-01-08 12:34:49
--------------------------
347952
3678
sách này nội dung rấtt hay cần thiết để ôn thi hiệu quả.Sách luyện nghe này theo mình thấy rất hữu ích, nó hỗ trợ mình rất nhiều trong việc luyện nghe. Sách có các bài nghe với các chủ đề đa dạng phong phú cùng hình ảnh minh họa nên giúp mình có được vốn từ rộng hơn ở mỗi bài học. Sách có phần đáp án và chi tiết các đoạn văn nghe phía sau nên rất tiện để mình tra đáp án và xem lại những bài nghe mà mình nghe không rõ. Tiếc là sách ít quá phải chi sách có nhiều bài nghe hơn nữa thì thật là tuyệt vì mình thấy những bài nghe trong sách này rất là hay và thú vị.
5
757144
2015-12-05 11:40:36
--------------------------
344973
3678
Mình đã mua cuốn sách này từ khi có thông tin NXB trẻ phát hành bản tiếng Việt và mình cảm thấy rất thích khi nhân được sách. Đây thực sự là cuốn sách hữu ích cho mình và những bạn sắp luyện thi n3. Cuốn sách có thiết kế gọn, nhỏ, nhẹ, hình ảnh ngoài bìa màu xanh lá cây rất dễ thương. Bên cạnh chứ kanji còn có hiragana và tiếng anh, còn có cả phần bài tập nữa. Mình cảm thấy rất hài lòng về cuốn sách và cả giá thành của nó. Tuy nhiên mình thấy phần bài tập còn ít, một số từ vựng ngoài hình ảnh thì nên có những câu ví dụ minh họa về cách sử dụng trong trường hợp, ngữ cảnh như thế nào. Bên cạnh đó theo mình thì chất lượng giấy cũng nên nâng cao.
4
208076
2015-11-29 15:15:13
--------------------------
309131
3678
Sách luyện nghe này theo mình thấy rất hữu ích, nó hỗ trợ mình rất nhiều trong việc luyện nghe. Sách có các bài nghe với các chủ đề đa dạng phong phú cùng hình ảnh minh họa nên giúp mình có được vốn từ rộng hơn ở mỗi bài học. Sách có phần đáp án và chi tiết các đoạn văn nghe phía sau nên rất tiện để mình tra đáp án và xem lại những bài nghe mà mình nghe không rõ. Tiếc là sách ít quá phải chi sách có nhiều bài nghe hơn nữa thì thật là tuyệt vì mình thấy những bài nghe trong sách này rất là hay và thú vị. 
4
324101
2015-09-18 22:23:13
--------------------------
305671
3678
Mình mua luôn cả một lốc mấy cuốn học tiếng Nhật trên Tiki. Vì đang cần tự học lắm. Riêng về cuốn sách này, mình thấy đây là một cuốn sách hay, hữu ích cho những ai đang luyện thi Nhật ngữ. Vì chắc chắn chúng ta sẽ hơi khó nuốt hơn với phần nghe. Được nghe nhiều, nghe quen thì sẽ bớt bị bỡ ngỡ, thành thạo hơn. Sách có đáp án rõ ràng để sau khi nghe xong có thể kiểm tra lại đáp án. Có các phần chia theo từng buổi, từng tuần. Nói chung là thích cuốn sách này lắm ý!
5
290680
2015-09-17 07:29:32
--------------------------
298298
3678
Khi học tiếng Nhật, phần mình nghĩ khó nhất là phần nghe, ở những kì thi năng lực Nhật ngữ, chỉ được nghe 1 lần, không được nghe 2 lần như thi tiếng Anh đâu. Thêm vào đó, theo mình thấy thì thời gian học nghe ở những trung tâm khá ít, chủ yếu là tập trung phần ngữ pháp. Mà nghe lại là phần khó vì hội thoại thông thường của người Nhật nói rất nhanh ( ở trung tâm các thầy cô kể cả giáo viên nước ngoài thường nói chậm cho bạn dễ nghe ). Mua cuốn này về. chịu khó mỗi ngày 2 trang và bạn sẽ thấy hiệu quả thấy rõ. cuốn này cực thích hợp cho những bạn luyện thi mà cảm nhận phần nghe mình còn yếu
5
93717
2015-09-12 16:34:25
--------------------------
288827
3678
Nếu bạn nào muốn luyện thi Năng lực nhật ngữ thì việc luyện nghe là không thể thiếu  trong quá trình học tập và ôn luyện. Vì mình tự học ở nhà nên không biết ngưỡng N3 thì trình độ nghe ở mức nào, chính vì vậy mà mình đặt mua sách này trên Tiki để về luyện nghe cho chắc. Nói chung, vẫn giống như những cuốn trong series, mình thích bìa ngoài dễ thương, và không thích chất liệu giấy vàng bên trong. Nội dung sách thì cũng chia ra làm 4 phần: Chuẩn bị, Làm quen nhiều dạng đề khác nhau, Nghe ở nhiều bối cảnh khác nhau và Nghe nhiều nội dung khác nhau. Cuối sách là các đáp án của bài nghe.
4
443629
2015-09-03 22:09:57
--------------------------
282136
3678
Sách khá hay, nội dung phong phú, rất tốt cho việc tự luyện nghe ở nhà! Sách gồm 4 tuần mỗi tuần khoảng 5 đến 6 ngày. Đặc biệt ở ngày 1 tuần 1 có phần luyện nghe cách phát âm của các âm tắt và hay sai trong tiếng Nhật rất hay. Cuối mỗi tuần có phần matome để ôn lại kiến thức. Mỗi ngày đều có phần từ vựng bổ sung trước khi nghe giúp tăng vốn từ vựng giúp nghe tốt hơn. Các bài nghe được chia theo chủ đề, thường là cuộc sống hằng ngày. Cuối sách có phần Kaitou dành cho bạn nào chưa nghe được hết hoặc muốn kiểm tra lại. Sách dùng tự luyện rất ổn. Nhất là luyện thi Năng lực Nhật ngữ.
5
415494
2015-08-28 22:24:40
--------------------------
278179
3678
Với mục đích nâng cao khả năng tiếng Nhật, đặc biệt là kỹ năng nghe hiểu. Đó là kỹ năng mà mình đang còn yếu, và cần luyện rất nhiều, đặc biệt dành cho kỳ thi năng lục Nhật ngữ N3. Trong danh sách những quyển sách đã từng luyện, thì đây cũng là một quyển sách hay, giọng phát âm tốt. Cái hay nữa là các chữ kanji khó thì còn có thêm phiên âm bằng hiragana, giúp người học cũng dễ dàng theo dõi và đọc trong sách. Một quyển sách hay và tốt cho việc tự luyện tiếng Nhật!
5
575705
2015-08-25 18:05:04
--------------------------
258947
3678
Để ôn thi N3 thì có rất nhiều sự lựa chọn cho mọi người, nhưng lựa chọn tốt nhất và phổ biến nhất có lẽ là soumatome. Chất lượng sách ổn nhưng giấy lại hơi mỏng mốt chút nếu viết bút chì thì sẽ hợp hơn, bên trong có những minh họa thú vị giúp cuốn sách đỡ khô khan. Một cuốn sách được Nhà xuất bản Trẻ biên tập lại để dễ dùng hơn với người Việt vì vậy nếu dùng đủ cả 5 cuốn sẽ đem lại kết quả rất tốt nhưng quan trọng nhất vẫn là ở bản thân người học.
5
401493
2015-08-08 22:41:24
--------------------------
255520
3678
Chỉ vừa cầm cuốn sách trên tay là đã thấy thích rồi. Bìa sách được thiết kế dễ thương, nhiều hình ảnh minh hoạ vui nhộn, cách trình bày logic tạo cảm giác thích thú chứ không nhàm chán và nhiều chữ như các sách khác. Tiki lại giao hàng rất nhanh nữa - đặt hôm trước là hôm sau có ngay nên mình rất hài lòng. Đặt mua rồi mới để ý thấy sách có cả bộ luyện nhiều phần khác nhau - đáng tiếc là tiki lại hết hàng. Mình thấy rất hữu ích cho những ai đang muốn luyện thi N3 như mình. 
5
254939
2015-08-06 09:07:40
--------------------------
250074
3678
Mình nghĩ chắc chắn ai đang luyện thi năng lực N3 cũng đang phải lo lắng, vất vả suy nghĩ chọn sách. Nhưng mình thấy chị của mình đã mua nguyên cả bộ sách này, không những chị ấy mà cả bạn của chị ấy cũng mua bộ sách này, ra các nhà sách để hỏi mua bộ này thì không biết bộ này có hot không mà chỉ biết đi đâu cũng báo hết hàng. Cũng may là Tiki còn hàng và còn kịp tranh thủ mua 3 bộ, với giá rất hời so với các nhà sách khác, giúp nhiều người tiết kiệm được thêm chi phí. Mình nghĩ trong 5 cuốn bây giờ chỉ còn mỗi cuốn này là còn hàng mà thôi :) may cho ai đã kịp mua hihi
5
90063
2015-08-01 09:30:10
--------------------------
236489
3678
Somatome từ lâu đã được đánh giá cao để ôn luyện N4 và N3 ( lên cao nữa thì mình không chắc nhé). Nói chung là lúc mình ôn luyện xen kẽ Đọc hiểu, Hán tự và Nghe hiểu. Đương nhiên là các bạn cần có vốn từ vựng hàng ngày để có thể nghe được người Nhật trong CD nói gì nữa.
Cuốn sách này khá hay vì có nhiều hình minh họa. Mình mới nghe được đến trang 14. Nói chung nghe xong nhiều lúc tự kỷ cười một mình vì có những mẩu chuyện khá hài. Ai không biết lại tưởng mình...khùng. Mình thích nhất là lặp lại theo đĩa CD. Hy vọng là mình sẽ ôn tập cấp tốc trong hai tuần. Vì mình thấy thời gian mỗi ngày 2 trang chỉ là mức tối thiểu mà người viết sách yêu cầu tính kỷ luật của người học.
4
17544
2015-07-21 20:29:36
--------------------------
225342
3678
Các bài ôn được sắp xếp theo chủ đề nhằm giúp những ai đang ôn luyện thi năng lực nhật ngữ JLPT trình độ N3 có thể làm quen với nội dung thi. Có 5 chương, 4 chương đầu để luyện nghe trong 4 tuần, mỗi chương có khoảng 5 đến 6 bài nghe, chương cuối cùng là ôn luyện tổng hợp. Mình rất thích giáo trình ôn này vì có nhiều hình minh hoạ vui, cách trình bày cũng rõ ràng mạch lạc. Vì là sách luyện nghe nên ở cuối có phần văn bản của các bài nghe nên rất tiện tra cứu. Tuy nhiên chất lượng giấy thì mình không ưng cho lắm vì hơi mỏng lại tối màu nhìn giống giấy in báo.
5
470937
2015-07-10 11:08:26
--------------------------
