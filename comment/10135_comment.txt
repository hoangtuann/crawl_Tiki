448306
10135
Mình đã cười lăn lộn với văn phong của tác giả khi kể về những suy nghĩ và trò của Tom. Nó như hệt mình ngày bé, mình phải thốt lên rằng các bạn nam đọc thì sẽ thấy hình ảnh của mình hiện rõ lên như thế nào nữa?

Rất dễ thương với những trò ngây ngô ma quái. Giọng văn rất tinh tế và thanh lịch nhưng mang lại tiếng cười vui nhộn. 	Mark Twain thật sự xứng đáng với danh tiếng của ông.
Một điều mình ko thích là chất lượng sách của nhà xuất bản này khá tệ.
5
493161
2016-06-15 20:03:33
--------------------------
403605
10135
Đầu tiên phải nói rằng rất vui khi nhận được sách sớm hơn dự kiến. Sách đẹp. Giấy đẹp. Giá thì được khuyến mãi nên rẻ lắm luôn. 
Cuốn sách phù hợp cho nhiều lứa tuổi. Đặc biệt là thiếu niên. Với những khát vọng được bay nhảy, tung cánh, đi trên con đường riêng của mình đôi khi là những sự ngỗ nghịch nhưng đầy hoài bão của sự non nớt muốn khám phá bởi sự tò mò k giới hạn. 
Đọc nó khiến ta như lạc vào thế giới mới, cùng sống lại những ước mơ, những thú vui, cùng phiêu lưu, đồng hành với cậu bé tom.
3
1177873
2016-03-23 23:19:03
--------------------------
396646
10135
Đây là một cuốn truyện rất thú vị. Phần dịch cũng khá hay so với một vài bản khác! Cuốn sách này mình đã nghe nói đến từ rất lâu rồi, giờ mua về rất thích thú! đúng như mọi người đã nhận xét, đây là một cuộc phưu lưu có thể nói là hấp dẫn nhất mình từng đọc! tình tiết truyện nhẹ nhàng nhưng cuốn hút, đưa mình về với những cảm xúc của một thời thơ ấu! thực sự rất hay!
Tiki giao hàng cẩn thận, đóng gói kĩ càng. mỗi món hàng được để riêng biệt và không bị dính vào nhau! :) Mình rất hài lòng và sẽ tiếp tục mua sản phẩm cuả Tiki, nhất là sách !!!!


5
1202596
2016-03-13 20:58:10
--------------------------
390765
10135
Tom Sawyer là một cậu bé cực kì hiếu động, tinh nghịch, luôn nghĩ ra đủ thứ trò ranh ma, tinh quái nhưng thực chất cậu bé Tom cũng chỉ như bao cậu bé khác, cậu có một tâm hồn trong sáng, luôn tò mò, khao khát được khám phá thế giới quanh mình.
Tom Sawyer chắc hẳn là hình mẫu lý tưởng của các bé trai muốn hướng tới. Một đứa trẻ không thích nằm trong vùng an toàn mà cha mẹ đã vạch sẵn mà chỉ muốn phiêu lưu, khám phá và mạo hiểm thật nhiều. nhưng cuối cùng cậu nhận ra rằng gia đình vẫn là nơi cậu thích nhất.

5
280007
2016-03-04 13:43:16
--------------------------
353537
10135
Tác phẩm Những cuộc phiêu lưu của Tom Sawyer là tác phẩm tôi chọn trong những tác phẩm kinh điển dành cho thiếu nhi. Tôi khá hứng thú với các tác phẩm thuộc thể loại trinh thám, phiêu lưu, với những chuyến phiêu lưu đầy thú vị và đậm chất thơ. Đọc tác phẩm này đều thỏa mãn những điều trên. Tôi sẽ mua thêm cả combo sách cho tiết kiệm để să thêm những cuốn sách văn học kinh điển hay nữa để thỏa mãn niềm đam mê đọc. Cám ơn tiki tôi rất hài lòng về dịch vụ của các bạn
5
833612
2015-12-16 13:26:51
--------------------------
352014
10135
Ngày còn đi học chỉ được đọc 1 trích đoạn nên đã rất muốn đọc tác phẩm này rất lâu rùi. Tiki giảm giá nên rất mún mua. Hay. Và sau này sẽ đọc cho con nghe nữa. Giao hàng nhanh. Sẽ ủng hộ tiki hơn nữa. Tuy nhiên chất lượng giấy không được đẹp lắm. Hic. Hy vọng tiki chọn nhà cung cấp hàng có chất lượng vì mua sách về để đọc nhưng cũng là tài sản vô giá phải lưu giữ nữa. Hy vọng tiki sẽ có những chương trình khuyến mại nữa để mình có thể có được những quyển sách ưa thích. Cảm ơn tiki rất nhìu.
4
337568
2015-12-13 14:02:53
--------------------------
347071
10135
Mình vừa mua cuốn sách này và mình đã ngấu nghiến nó và cảm thấy sức hấp dẫn tuyệt vời của cuốn sách rất ý nghĩa này. Đọc Những cuộc phiêu lưu của Tomsawyer mình như đươc sống lại tuổi thơ, mình tin là ai đọc cuốn sách này đều cảm nhận được một điều là: À, khi tuổi đó mình cũng có những cảm xúc giống như cậu bé Tom... với giong văn hài hước mà sâu sắc, đã có lúc mình bật cười với những trò chơi và những suy nghĩ trẻ thơ, hồn nhiên của các nhân vật trong truyện như việc đổi cái răng của Tom lấy con mèo chết của Huck... Rồi cảm giác hồi hộp theo từng chi tiết của truyện, những cuộc phiêu lưu rất thú vị và đầy ý nghĩa ở trong đó.
Tóm lại, cuốn sách này là một cuốn sách hay và rất đáng đọc không chỉ với các em thiếu nhi mà ngay cả người lớn....

5
604411
2015-12-03 20:48:02
--------------------------
340064
10135
Với những trò nghịch ngợm của chú bé Tom đã đem lại cho người đọc những trận cười thoải mái, những giây phút thư giãn vui vẻ. Các em nhỏ và những người có tâm hồn tươi trẻ chắc chắn sẽ rất thích những cuộc phiêu lưu của Tom. Cùng với phong cảnh đồng quê nước Mỹ được tác giả vẽ ra, Tom được xây dựng lên nổi bật trên cái bức tranh có vẻ đìu hiu ấy. Những trò đùa của Tom thực sự gây cười cho người đọc, vừa ngây thơ lại có chút trưởng thành. Càng đọc càng khiến người đọc say mê
4
845629
2015-11-19 09:28:09
--------------------------
327069
10135
Đây là một quyển sách hay, nội dung truyện làm mình phải phì cười khi đọc những suy nghĩ cũng như những trò tinh nghịch mà cậu bé Tom này nghĩ ra. Rồi những cuộc phiêu lưu của cậu bé này làm mình thấy happy sao ý, nói chung làm mình hòa nhập vào nhân vật, sự hồn nhiên của tuổi nhỏ cùng với những trò đùa tai quái làm mình không khép nổi miệng. nhưng về phần hình thức quyển sách thì bìa quá mỏng, đây là cái mình không ưng ý nhất. Nhưng nói chung nội dung truyện làm mình đỡ buồn hơn
5
464516
2015-10-27 08:54:32
--------------------------
324679
10135
đây là một tác phẩm dành cho rất nhiều lứa tuổi. câu chuyện phiêu lưu của tom rất thú vị , nhiều chi tiết gây cười nhưng qua câu chuyện ta còn thấy sự châm biếm xã hội dưới ngòi bút độc đáo của tác giả. Tom là một chú bé rất hiếu dộng và luôn luôn nghĩ ra được những chiêu trò tinh quái tuy nhiên cũng trong những cuộc phiêu lưu ấy , Tom và bạn bè đã khám phá ra được một vụ giết người, phá tan âm mưu độc ác của toán cướp và cứu mạng được cô bạn gái becky. đây thực sự là một tác phẩm thiếu nhi kinh điển.
4
626659
2015-10-21 18:57:14
--------------------------
300812
10135
Độc giả sẽ phải phì cười với những hành động tinh nghịch của các cậu bé trong truyện. Mình không thể quên được hình ảnh cậu bé Tôm giả vờ đau bụng để không phải đi học. "Không phải con giả vờ đau bụng để trốn học đâu" . Đúng là trẻ con. Thế khác gì lạy ông tôi ở bụi này. Giống y như lũ trẻ con nhà mình. Trẻ em như búp trên cành biết ăn biết ngủ biết học hành là ngoan. Dù chúng có nghịch ngợm làm người lớn buồn hay giận nhưng sâu trong tâm hồn chúng là những tình cảm trong sáng không che giấu.
4
329267
2015-09-14 11:07:02
--------------------------
296619
10135
Quả là một chuyến phiêu lưu đầy kì thú của Tom Sawyer. Mình đọc mà cứ nghĩ mình chính là một nhân vật trong truyện, đi từ miến đất này đến miền đất khác mà không phải suy nghĩ, không phải lo sợ. Tuy nhiên, mình vẫn chưa thích bản in này lắm, giấy khá mỏng và rất dễ rách, dường như nếu lật quá mạnh thì tưởng chừng sẽ rách mất. Đổi lại thì bản in này khá đẹp và rất bắt mắt, ấn tượng đầu tiên chính là bìa sách, hai nhân vật mặc một bộ quần áo giản dị nhưng họ rất vui vẻ, khuôn mặt tươi cười, hạnh phúc. Mình tin chắc rằng cuốn sách sẽ mang đến cho các bạn những trạng thái cảm xúc khác nhau, từ hấp dẫn đến li kì, hài hước.
5
13723
2015-09-11 10:33:40
--------------------------
296149
10135
Một trong các cuốn Classic của Đông A. Bìa đẹp, lấp lánh còn có có cả tranh minh họa rất dễ thương. Những trò nghịch ngợm của Tom, những suy nghĩ của cậu và cuộc phiêu lưu thú vị của cậu sẽ khiến bạn phải mỉm cười trước cậu bé vô cùng hiếu động và tinh quái. Mình thấy buồn cười nhất vẫn là lúc Tom tán Becky, có thể học hỏi Tom được nhiều điều. :) Một cuốn sách thú vị bạn không nên bỏ lỡ. Nếu đã đọc Tom bạn không nên bỏ qua cuốn sách Những cuốn phiêu lưu của Huck Finn của cùng tác giả.
4
491545
2015-09-10 21:55:06
--------------------------
285002
10135
Lần trước mình đã mua cuốn Những Cuộc Phiêu Lưu Của Tom Sawyer rồi, nhưng tiếc là tên nhân vật bị phiên âm tiếng Việt, nên mình quyết tâm mua một cuốn khác vừa ý hơn. Và bản này rất vừa ý mình.
Tác phẩm ngoài việc kể về những cuộc phiêu lưu, những trò nghịch phá ranh mãnh của cậu bé Tom mà còn mô tả một vùng thôn quê nước Mỹ điển hình của thế kỷ 19. Nơi mà những đứa trẻ ngoan ngoãn được người lớn đúc thành một khuôn mẫu tẻ ngắt và khô khan, còn những thằng bé bày đủ trò chọc phá mọi người, phớt lờ mọi nguyên tắc của người lớn đặt ra như Tom, Huck thì được xem là ngỗ nghịch.
Văn phong hài hước pha chút châm biếm, theo mình sẽ phù hợp với những ai đã từng là trẻ con hơn là trẻ con thực sự.
5
707382
2015-08-31 14:51:51
--------------------------
283859
10135
Những Cuộc Phiêu Lưu Của Tom Sawyer xứng đáng là một trong những tác phẩm tiêu biểu của Mark Twain, cũng như của nền văn học Mỹ. Tác giả không chỉ khắc họa hình ảnh một cậu bé tinh nghịch với đủ trò quỷ quái cùng những chuyến "phiêu lưu" thú vị của cậu, mà còn họa lại một vùng thôn quê sinh động của nước Mỹ thế kỷ 19. Cậu bé Tom trong truyện được miêu tả rất thật, với những suy nghĩ, hành động đúng chất một đứa trẻ nghịch ngợm nhưng cũng rất đỗi hồn nhiên. Cuốn truyện là một bảng màu sắc rực rỡ, có ly kỳ hồi hộp, có hài hước dí dỏm, mà cũng có những điểm châm biếm sâu sắc xã hội Mỹ thời bấy giờ, khiến người đọc phải suy ngẫm.
5
57459
2015-08-30 13:28:31
--------------------------
258468
10135
Lại một tác phẩm tuyệt vời nữa của Văn học thiếu nhi nước ngoài được tái bản và quả thật đó là một điều đáng mừng. Câu chuyện này đã trở thành một phần rất đẹp của tuổi thơ những ai đã trưởng thành và sẽ là món quà vô giá cho những bạn trẻ đang tiến bước vào đời.
Từ khi ra đời cho đến nay, Tom Sawyer đã trở thành người bạn thân thiết của các thế hệ trẻ em trên khắp thế giới và Việt Nam. Rất mong các bạn trẻ lựa chọn tìm đọc cuốn sách này!
4
608340
2015-08-08 16:00:14
--------------------------
250983
10135
Câu chuyện kể về những cuộc phiêu lưu của cậu bé Tom Sawyer tinh nghịch và những người bạn thân. Qua những câu văn của tác giả Mark Twain, tôi đã được cảm nhận sâu sắc hơn về cậu bé đó. Và tôi đã bị cuốn hút vào đấy từ lúc nào mà không hề hay biết. Tôi ngày càng say sưa hơn trong những trang sách và đưa mình vào những chuyến phiêu lưu đầy thú vị, vui nhộn và kịch tính của các nhân vật trong truyện. Có những lúc, tôi tưởng như câu chuyện đã đến hồi kết.
5
711358
2015-08-02 11:33:35
--------------------------
218698
10135
Tôi đã đọc Những cuộc phiêu lưu của Huck Finn trước khi đọc Những cuộc phiêu lưu của Tom Sawyer. Hai tác phẩm này đã đem lại sự nổi tiếng khắp năm châu cho Mark Twain và chúng được xếp vào hàng những cuốn sách thiếu nhi hay nhất mọi thời đại.

Cái tên “những cuộc phiêu lưu” dễ làm chúng ta liên tưởng đến những chuyến đi dài ngày, đầy hiểm nguy và bão táp. Tuy nhiên, anh chàng Sawyer của chúng ta lại là con người thật bình dị, những chuyến phiêu lưu của cậu có thể gọi là “giang hồ vặt” như chữ của Nguyễn Tuân. Nhưng không vì thế mà cuốn sách trở nên nhạt nhẽo, trái lại, nó hấp dẫn và đầy lôi cuốn.

Tâm hồn trẻ thơ của Sawyer, với những suy nghĩ cũng rất trẻ thơ và nhiều trò đùa nghịch vừa buồn cười vừa thú vị đã chinh phục độc giả bao thế hệ nay.

Bạn còn chần chừ gì nữa mà không tham gia vào chuyến phiêu lưu của Tom Sawyer nhỉ?
4
598102
2015-06-30 20:10:55
--------------------------
214518
10135
Cuốn sách về cậu bé Tom Sawyer là quyển đầu tiên để mở ra những hành trình về sau này, những trò nghịch ngợm, quật phá đậm chất trẻ con của Tom khiến mình cười không ngớt bởi đôi khi mình cũng thấy sự nghịch ngợm của mình trong nhân vật Tom. Trong những chương đầu tác giả viết về cuộc sống hàng ngày của Tom ở vùng quê thanh bình làm người đọc dễ dàng nắm bắt được tính cách riêng Tom, để rồi càng về sau khi dấn thân vào cuộc phiêu lưu của Tom cùng nhóm bạn, người đọc sẽ lại càng hồi hợp và bất ngờ bởi những tình huống kịch tính trong cuộc phiêu lưu, và không khỏi ngạc nhiên khi Tom nghịch ngợm phá phách lại trở  thành anh hùng của thị trấn.
5
471112
2015-06-25 08:40:56
--------------------------
86461
10135
Mình rất thích truyện của Mark Twain, trong đó đặc biệt thích hai quyển sách thiếu nhi hấp dẫn về các cậu bé Tom Sawyer và Huck Finn. Cuốn sách về cậu bé Tom Sawyer là cuốn đầu tiên giới thiệu cho chúng ta bối cảnh về một miền quê thanh bình bên dòng Missisipi nơi hai cậu bé tinh nghịch kia đã trải qua thật nhiều cuộc phiêu lưu li kì, hấp dẫn từ những trò chơi tai quái bắt chước theo sách vở và truyện kể dân gian. Thích nhất ở dòng sách thiếu nhi của Mark Twain là giọng điệu rất hồn nhiên, dù có dùng những từ ngữ rất đao to búa lớn, tất cả toát lên sự giáo dục nhồi nhét cứng nhắc của một thời , chế giễu một cách nhẹ nhàng mà chẳng kém phần sâu cay về mọi mặt của xã hội đương thời, chuộng bề mặt, hình thức bên ngoài hơn cái cốt lõi bên trong mỗi con người.
Những chuyến phiêu lưu đầy hấp dẫn và mối tình trẻ con của Tom hứa hẹn sẽ cho bạn những trận cười vui vẻ và cả những giây phút gay cấn nghẹt thở. Một cuốn sách thiếu nhi kinh điển không thể bỏ qua
5
71367
2013-07-10 18:47:44
--------------------------
84433
10135
Mình lựa một đống sách về đọc, và đây là cuốn mình mở ra cuối cùng. Thật sự mình không háo hức lắm khi cầm trên tay cuốn sách này, phần nài ép bản thân nhất định phải đọc cho hết. Mục đích mình lựa chọn mua cuốn sách này, mặc dù đã qua cái lứa tuổi thiếu niên nhi đồng, vì mình chưa từng thưởng thức một tác phẩm nào của Mark Twain, nhiều khi người ta nhắc đến tác giả kinh điển này mà không biết thì quả thực rất ngại.  

	Đọc những chương đầu tác giả tập trung kể về cuộc sống hằng ngày của Tom, nhằm cho người đọc hiểu được những tính cách rất riêng của Tom. Tính cách của Tom rất đặc biêt; là đứa trẻ nghịch ngợm, có những hành động vô cùng trẻ con, ngây thơ, non nớt; nhưng nhiều khi suy nghĩ và hành động rất dũng cảm, rất trưởng thành, rất đáng yêu và đáng khen. Một số đoạn đầu truyện cảm thấy “nhạt nhạt” nhưng càng đọc càng có nhiều tình tiết thú vị vô cùng. Nhiều chi tiết truyện gây cười khôn tả. (Điển hình như cách chữa mụn cóc của Tom và Huck: “…cắt mụn cóc lấy cho được một ít máu, rồi cho máu lên nửa quả đậu, xong đào một cái lỗ và chôn nửa quả đậu đó xuống... quả đậu có máu sẽ kéo sẽ rút, cố hút phần kia về với nó và như thế nó giúp cho cục máu lôi bật mụn cóc và không lâu mụn cóc sẽ biến mất”; bài thơ: “Quỷ theo xác chết/ Mèo theo quỷ/ Mụn cóc theo mèo/ Thế là ta sạch mụn nhé!”

Nói thực nghe cái tên “Cuộc phiêu lưu của Tom Sawyer” mình hình dung ngay đến cuộc phiêu lưu kiểu như “Trên sa mạc và trong rừng thẳm” hay “Cuộc phiêu lưu kì lạ của Carich và Valia” đã đọc từ thời tấm bé; kiểu những hành trình dài, trải qua nhiều nơi, phải chống trọi với thiên nhiên, rời nhà đi sau bao năm tháng mới quay trở về. Vì vậy mình tò mò, mình chờ đợi ngày chú bé Tom xách ba lô lên đường, biệt tăm đi khám phá chân trời mới. 
Hóa ra cuộc phiêu lưu của cậu bé Tom và những đứa bạn chỉ là những cuộc phiêu lưu ngắn, nho nhỏ nhưng thú vị, rất gây tò mò, rất cuốn hút. Phong cách rất trẻ con rất đáng yêu! Tác gỉa quả thực rất thấu hiểu tâm lý trẻ thơ, cách tác giả miêu tả, cách kể rất tinh tế, đọc mà cảm thấy ngộ nghĩnh vô cùng. Những cuộc phiêu lưu của của đứa trẻ đầy dũng cảm, đầy gan góc, đầy quyết tâm với một lý tưởng trong sáng, đã mang lại một thành quả vô cùng xứng đáng. Chú bé dũng cảm đã mang về chiến tích lớn và trở thành người hùng của làng nhỏ. Chú trở thành “thần tượng”, trở thành “khao khát” của bao đứa trẻ trong ngôi làng St. Peterburg; và chắc hẳn cũng đã và đang là niềm mơ ước của bao trẻ em trên thế giới. 

	Mình đọc bản truyện của Nhà xuất bản Văn học và Trung tâm Văn hóa Ngôn ngữ Đông Tây, tuy màu sắc bìa khá đẹp, bắt mắt nhưng bản dịch không chuẩn xác, nhiều đoạn chưa thống nhất cách xưng hô “ cậu- tớ”, “mày-tao”; nhiều chỗ lỗi đánh văn bản, chấm câu khiến cảm giác rất khó chịu khi đọc. Vì vậy mọi người không nên mua bản truyện này. 

	Cuộc phiêu lưu của Tom Sawyer là một câu chuyên đáng đọc. Tiếc rằng đến bây giờ mình mới thưởng thức. Mong cuốn truyện sẽ đến được tay nhiều em nhỏ hơn!  

4
54209
2013-06-30 14:19:23
--------------------------
76929
10135
Nói chung câu đầu tiên mà tôi phải nói ở đây là :KHÔNG ĐỌC LÀ THIẾU XÓT LỚN ĐÂY: .Thật ra quyển truyên này tôi được thầy tiếng anh giới thiệu, ban đầu hỏi thì cứ ngơ ngơ, nhân vật kinh điển mà không biết. Thế là quyết định đọc thử, phải nói là cười tẹt ga. Còn trẻ, ai cũng mong muốn thoát ly khỏi bố mẹ, sống một cuộc sống tự do tự tại, thích làm gì thì làm, ăn lúc nào, ăn cái gì cũng chẳng có ai quản, Tom tất nhiên không thuộc ngoại lệ. Thông minh, gan dạ, bay vào một thế giới mới mẻ với những cuộc phiêu lưu kỳ thú Nhưng cuối cùng em vẫn trở về với gia đình, bởi đó luôn là tổ ấm bao bọc sau suốt quãng đường dài dằng dặc. Đây là một tác phẩm đã lâu, nhưng nội dung thì không hề cũ chút nào, ngoài trong sáng và hấp dẫn ra, tôi chẳng biết có từ gì thích hợp hơn nữa.  Tuy nhiên có một số chỗ tôi vẫn cảm thấy sao đó, không biết là do cách dịch hay lối ngôn ngữ từ hai văn hóa khác nhau, nhưng cuốn sách này vẫn là 1 trong những quyển tôi thích đọc nhất :)
4
74568
2013-05-24 20:58:38
--------------------------
71841
10135
Lại thêm một cuốn sách thiếu nhi kinh điển được ra đời trong một diện mạo mới để đến gần hơn những bạn đọc nhỏ tuổi bây giờ. Tôi tự hỏi không biết bao giờ mới có thêm những tác phẩm thiếu nhi được xếp vào hàng kinh điển như thế này. "Những Cuộc Phiêu Lưu Của Tom Sawyer" chẳng phải một cái tên xa lạ gì với những ai đã từng một thời thiếu nhi đọc sách, xem phim hoạt hình. Từ cốt truyện đến hình tượng, tâm lý nhân vật đều vui tươi hóm hỉnh đúng tinh thần thiếu nhi mà vẫn rất tinh tế, sâu sắc. Độc giả một khi đã lạc vào những chuyến phiêu lưu của cậu bé Tôm nghịch ngợm cùng nhóm bạn thì sẽ không khỏi ngạc nhiên về những tình huống kịch tính, những cái lên án xã hội rất hồn nhiên mà sâu sắc, tinh tế và hơn hết là sự hồi hộp, ngạc nhiên về cậu bé đã thành anh hùng của thị trấn. Đây thực sự là cuốn sách hay vô cùng mà không ai có thể bỏ lỡ.
5
29716
2013-04-28 23:07:57
--------------------------
69822
10135
Mình biết đến truyện của Mark Twain đó là nhờ được cô tặng cho cuốn sách này " Những cuộc phiêu lưu của Tom Sawyer" năm rồi ! Phải nói là ngay từ khi đọc những dòng đầu thì cảm thấy hồi hộp và câu chuyện nó thú vị dần dần ! Những trò nghịch mà hoá ra lại dẫn đến 1 cuộc phiêu lưu chưa từng có , rồi những lần cậu ta yêu thầm phải 1 cô tiểu thư cùng trường rồi những lần thoát chết trong gang tấc . Dù quyển sách khá dày và mình đọc cũng hơi ngán nhưng nhờ những tình tiết hồi hộp gay cấn mà mình lại tò mò đọc tiếp ! Cũng cần phải chỉnh lại 1 vài chỗ , từ ngữ ko dc rõ ràng lắm , hơi bị mẫu thuẫn ! Tôi rất thích Tom , thật tuyệt
5
58192
2013-04-17 22:17:19
--------------------------
55242
10135
Cuốn sách này đưa ta đi hết những bất ngờ thú vị này đến bất ngờ thú vị khác. Về chuyện tình của cậu Tom và cô Becky, hay là cuộc bỏ nhà ra đi đầy ngoạn mục của 3 người bạn để rồi trở về trong sự hân hoan vui mừng của bà cô và mọi người trong xóm, lại còn trở thành anh hùng nữa chứ. Thật đúng là phiêu lưu thú vị. Rồi còn tên Joe da đỏ và tội ác của hắn bị phanh phui, và rồi chuỗi ngày lo âu căng thẳng sợ bị trả thù đã kết thúc bằng cái chết của tên sát nhân và một kho báu kếch xù mà cậu vớ được. 

Những cuộc phiêu lưu kích thích trí tò mò cực kì!

5
46290
2013-01-12 22:40:01
--------------------------
50357
10135
Tác phẩm này (tôi xin phép được dùng 1 từ ngữ trang trọng hơn thay cho từ câu chuyện) thật sự là một cuốn sách thiếu nhi kinh điển.
Điểm cộng đầu tiên chính là nội dung cốt truyện. Một câu chuyện vui tươi, tuổi thơ hiện ra đầy màu sắc, những trò nghịch ngợm của chú bé Tom, những cuộc phiêu lưu kì thú, với bộ óc bình thường như chúng ta thì thật khó để tượng tưởng ra. Nhưng Mark Twain đã làm được tất cả.
Điểm cộng thứ hai chính là miêu tả tâm lý và tạo hình nhân vật. Tâm lý từ những đứa trẻ cho đến người lớn tuổi đều hiện ra rõ nét như 1 bức tranh sinh động, vô cùng thú vị. Tạo hình nhân vật Tom hiện ra là 1 chú bé nghịch ngơm, trái ngược hẳn với cậu em họ ngoan ngoãn.
Điểm cộng thứ ba chính là cách phản ánh xã hội của tác giả qua cái nhìn trẻ thơ ví dụ như nạn phân biệt chủng tộc, tuy nhiên sự châm biếm, mỉa mai với xã hội không sâu cay và thể hiện rõ ràng như trong cuốn "những cuộc phiêu lưu của Huck Fin".
Tóm lại, đây là một tác phẩm rất xuất sắc, đáng đọc, đáng nghiên cứu.

5
79385
2012-12-13 22:51:08
--------------------------
49294
10135
Mark Twain có lẽ là một trong những nhà văn viết truyện thiếu nhi hay nhất, tài năng nhất trong lịch sử. Cuốn truyện về cậu bạn Tom Sawyer  dù đã ra đời từ lâu nhưng luôn nhận được sự yêu mến của mọi độc giả. Chuyến hành trình của cậu bạn này với nhóm bạn đưa độc giả từ những cảm xúc này đến những cảm xúc khác, buồn có, vui có, hồi hộp có, cảm động có. Tất cả được nhà văn Mark Twain viết hết sức tài tình, hấp dẫn, với bút pháp cổ điển mà gần gũi, ngôn ngữ tươi mới mà tinh tế đến không ngờ. Bây giờ được đọc cuốn truyện này trong một diên mạo mới với sự biên tập khá tốt của công ty Đông A càng làm mình cảm thấy thích thú hơn nữa.
Cảm ơn Mark Twain vì một cuốn sách tuyệt vời đến vậy!
4
20073
2012-12-06 20:22:42
--------------------------
44478
10135
Mình vốn mê phiêu lưu khám phá, có lẽ cũng vì chú bé Tom nghịch ngợm này. Thế đấy, cái thế giới này bao la thật, nhưng đôi lúc chẳng cần đi đâu xa, trải nghiệm cuộc sống xung quanh như Tom, như một đứa trẻ, chẳng phiền hà, tự do bay nhảy. Cứ mỗi lần đọc quyển sách nào về phiêu lưu, là y như rằng mình cứ muốn chạy đi luôn, đi tới những nơi mình muốn đến. Quyển sách này cũng không ngoại lệ. Không hẳn là có ích cho những ai yêu du lịch, nhưng chắc chắn một điều, bạn sẽ vỡ ra cả khối điều khi tìm hiểu về chú bé Tom cũng như cuộc sống muôn màu xung quanh chú, và có khi, cái tình yêu muốn được bay nhảy lại lớn dần lên trong bạn đấy.
5
65545
2012-11-02 20:54:31
--------------------------
41582
10135
Những cuộc phiêu lưu của Tom Sawyer thú vị đầy những trò chơi tinh nghịch nhưng cũng đầy thú vị tuổi trẻ.
Tom là một cậu bé tinh nghịch, thông minh, ham hiểu biết, giàu trí tưởng tượng nhưng cũng giàu lòng nhân hậu và yêu thương, với tâm hồn dũng cảm đáng yêu. Những trang sách sinh động, đầy kịch tính, hình ảnh của những đứa trẻ không chỉ biết đùa vui, phiêu lưu khám phá mà còn nhận thức đúng đắn về các thói xấu, những kẻ tham lam. Lật mở từng trang sách là từng bước tiến vào cuộc phiêu lưu đầy màu sắc của chú nhóc Tom tinh nghịch đáng yêu, những cuộc khám phá mới lạ, những tiếng cười hồn nhiên, trong trẻo của hai nhân vật chính trong truyện trước sự thắng lợi của cuộc phiêu lưu tìm đến tự do.
Sống như thế mới là tuổi thơ. Dữ dội, trong trẻo và đầy bùng nổ. Đó mới là thế giới tuổi thơ đầy màu sắc rực rỡ và đúng nghĩa của tuổi thơ. 
Tuy nhiên các bản dịch về tác phẩm này ngôn từ có hơi gượng gạo, không được tự nhiên cho lắm, chưa lột tả được cái nét ngây thơ trong sáng của tác phẩm.


5
49592
2012-09-30 22:48:56
--------------------------
41026
10135
Được đánh giá là một trong những tác phẩm kinh điển dành cho thiếu niên với lối sống phóng khoáng, những suy nghĩ trong veo mà tinh nghịch, Những cuộc phiêu lưu của Tom Sawyer chắc chắn sẽ làm các em nhỏ say mê với bao cuộc phiêu lưu kỳ thú của cậu bé thông minh và ngỗ nghịch Tom Sawyer, đó cũng là ước mơ của chúng, được thoát ra vòng tay của các ông bố bà mẹ để tự do tung cánh bay cao thật cao nhưng rồi cuối cùng cũng vẫn nhận ra Gia đình mình vẫn là số 1. Tuy nhiên có thể do dịch giả nên câu chữ của tác phẩm làm tôi quá chán ngán, chỉ có thể nhấm nháp dần dần với cốc cà phê trên tay hoặc có thể ý tưởng của tác phẩm là của một thời ấu thơ xa xăm, của một thập niên xưa cũ!
3
58343
2012-09-26 15:55:34
--------------------------
