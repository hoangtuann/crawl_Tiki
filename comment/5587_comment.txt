447140
5587
Mình đã đọc một lèo cuốn Thám tử lừng danh Conan tập 55 này. Tập này là một tập khá đặc biệt khi đưa độc giả trở về thời quá khứ. Ngược dòng thời gian khoảng hơn 10 năm trước, khi Shinichi và Ran còn học lớp 1... đột nhiên một ngày họ nhận được ám hiệu "hãy ngăn sự tức giận của Hyde!" từ một nhân vật tự xưng là "em trai của Shinichi".Thế là hai cô cậu bước vào cuộc điều tra đầy những điều li kỳ. Cách suy đoán như thần đồng của Conan khi còn nhỏ rất đáng nể. Ngoài ra, trong tập 55 này còn có câu chuyện về con miu cưng của Kisaki Eri tên là Goro và sân thi Koshien dành cho các thám tử...Tiki giao hàng rất nhanh chóng. 
5
531312
2016-06-13 15:59:45
--------------------------
397541
5587
Nếu không tính vụ cuối cùng thì tập này khá hay, bởi vì vụ cuối rất nhảm, về bệnh của con mèo của Eri và tình cảm của vợ chồng Mori, y như là sau những vụ gay cấn thì đưa vào một vụ án vui nhộn để thư giãn đầu óc vậy. Vụ án hồi Shinichi còn nhỏ thì phù hợp với con nít Nhật, chứ không biết tiếng Nhật thì thấy nó nhạt nhẽo lắm, chỉ được mấy hình ảnh hồi trẻ của các nhân vật rất đẹp, dễ thương, và ngộ khi Ran tóc ngắn. Vụ ông người Đức khá đơn giản với lời ám chỉ hung thủ của nạn nhân nếu ai biết tiếng Đức. Hay ! Lâu lâu đưa nước ngoài vào cho truyện đỡ nhàm chán.
5
535536
2016-03-15 08:14:04
--------------------------
382513
5587
Nội dung tập này đa số là hoàn toàn kể về tuổi thơ của Shinichi và Ran (có cả Kaito Kid nữa). Ôi chồi ôi tên ngốc Shinichi suốt ngày rủ Ran con nhà người ta đi tập tành phá án cùng hắn, nhờ ơn phước của cậu ta mà cả 2 đều ăn mắng như cơm bữa của Eri mẹ Ran. Nhưng nhờ vậy mà chúng ta đã có một Shinichi như ngày hôm nay. Tập này mình rất thích vì có nhiều tình cảnh vui nhộn, tiếc là mất tiêu cuốn đó rồi......huhu....ai ban phước cho mình xin cuốn đó nha! (đùa thôi). Hihi...
5
1034042
2016-02-19 12:23:40
--------------------------
231477
5587
Trong tập này nổi bật nhất chắc chắn là vụ án kể về 10 năm trước khi Shinichi và Ran chỉ mới là cậu học sinh lớp 1. Khi nhận được ám hiệu "hãy ngăn sự tức giận của Hyde!" và ám hiệu được gửi từ một nhân vật tự xưng là "em trai của Shinichi" , chàng thám thử nhí đã cùng cô bạn học của mình bước vào cuộc điều tra bằng cách giải các mật thư. Shinichi lúc đó mới chỉ là học sinh tiểu học thôi vậy mà đã có bản lĩnh và những suy luận hết sức nhạy bén rồi, thiệt là ngưỡng mộ quá đi mất. Ran hồi nhỏ cũng dễ thương cực kỳ luôn. 
5
471112
2015-07-18 08:27:53
--------------------------
199204
5587
Tiêu điểm của tập này phải nói là vụ án khi Conan chỉ mới lớp 1, nhưng đã suy đoán một cách như thần đồng, lần tìm những mật mã mà "bóng ma" trong thư viện để lại như một lời thách đấu. Conan đã giải rất trọn vẹn tất cả những mật thư, có điều mật thư cuối cùng lại giải nhầm một chỗ, đến tận bây giờ mới phát hiện ra thì đã bị bố nẫng mất tay trên :)) Nói chung tập này hay cực, đọc hoài không chán, với lại, nhờ tập này mới biết, Shinichi và Kid là anh em một nhà đó nha :))
5
530790
2015-05-21 18:41:00
--------------------------
