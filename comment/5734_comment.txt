554389
5734
Đoán là người dịch không có kiến thức về luật (cả luật VN lẫn luật của Hoa Kỳ) nên nhiều thuật ngữ chuyên môn dịch không được, sai bét về nội hàm.
Mua quyển này vì thấy tiêu đề hấp dẫn, nhưng đọc được 2-3 trang là đem cho bạn :D, mua bản gốc tiếng Anh trên kindle đọc mới hiểu được.
1
91661
2017-03-25 22:04:44
--------------------------
457216
5734
Đọc nhận xét của bạn đọc mua sách trước mình có chút do dự nhưng vẫn quyết định mua bởi mình muốn kiểm chứng cũng như tìm hiểu nhiều thêm về Luật pháp nước ngoài do mình cũng đang học thêm về Luật.
Học Luật Việt Nam nhưng theo mình quyển Luật 101 này cũng rất tốt cho việc nhìn nhận sự tương quan giữa Luật Việt Nam với Luật nước ngoài như thế nào!
"Đọc đọc đọc" là mục tiêu mình mong muốn để có thể dung nạp thật nhiều kiến thức hữu ích cho ngành nghề mình chuẩn bị theo đuổi!
Rất vui vì mình không chọn sai, quyển sách rất hữu ích đối với mình!!!
4
355461
2016-06-24 08:28:32
--------------------------
343618
5734
Tôi mua cuốn sách này khoảng 1 tháng đến nay mới đọc được hơn 1/2 cuốn. Nhìn chung, đây là 1 cuốn sách khá kén chọn người đọc. Người đọc đòi hỏi phải có kiến thức nhất định về pháp luật mới hiểu được và so sánh Luật VN và Luật Hoa Kỳ. Khâu dịch thuật vẫn chưa khúc chiết, mạch lạch. Một vài đoạn viết rất hàn lâm khó hiểu... Tóm lại, chỉ đọc cuốn sách này thôi bạn chưa thể trở thành luật sư và chưa hiểu hết pháp luật Hoa Kỳ nhưng là tài liệu tham khảo bổ ích cho người học và nghiên cứu pháp luật.
3
852363
2015-11-26 21:15:43
--------------------------
250837
5734
Tôi đọc xong cuốn sách này vào tháng trước và không thể phủ nhận sự hấp dẫn của nó dù sách này có một chút gì đó khá "cứng nhắc". Việc hiểu biết thêm về hệ thống pháp luật Mỹ khá thú vị, nó có thể giúp ta biết thêm những chính sách mới của chính quyền tiểu bang và liên bang đã ban hành. Ngoài ra tôi còn có thêm một số kiến thức thực tế về pháp luật. 
Dù cuốn sách này chắc chắn sẽ không thể làm cho bất cứ ai trở thành một luật sư nhưng nó cung cấp một nền tảng khá vững chắc về các bộ phận, khía cạnh khác nhau của luật pháp Hoa Kì và cách họ ứng dụng nó vào thực tiễn. Một cuốn sách đáng để đọc nếu bạn có hứng thú với ngành luật, nó cũng rất bổ ích cho những người đang học luật tại Việt Nam để so sánh đối chiếu sơ bộ.
4
352140
2015-08-02 10:56:18
--------------------------
