440613
2949
Truyện hay rất dễ nhớ và mang tính giáo dục cao. Giọng văn xúc tích, ngắn gọn khiến các bé vui vẻ lắng nghe và ghi nhớ các từ ngữ tiếng anh trong truyện. Hình vẽ trong truyện rất sinh động, màu sắc bắt mắt con trẻ. Truyện khắc hoạ hai nhân vật chính là dê mẹ và chó sói mưu mô. Qua câu truyện ta thấy được hình tượng dê mẹ dũng cảm bảo vệ đàn con thân yêu của mình dù phải đối mặt với chó sói hung ác. Một câu truyện mang tính nhân văn sâu sắc và kết thúc có hậu dạy dỗ cho các bé phải luôn ghi nhớ theo lời mẹ vì mẹ luôn làm mọi thứ tốt nhất cho các con yêu dấu của mình!
4
1313582
2016-06-02 00:27:12
--------------------------
439806
2949
Mình mua quyển này với một lốc truyện song ngữ khác để khuyến khích thằng em học tiếng anh, mang về mình còn mê nhiều hơn nó, nội dung truyện thì ok rồi, vì mình vốn thích những truyện này, đơn giản không phức tạp, lại gọi nhớ tuổi thơ đọc truyện dữ dội của mình, cảm xúc có khác nhưng lại thấy vui, em mình cũng rất thích, vừa khuyến khích nó đọc sách vừa gián tiếp thúc đẩy nó học tiếng anh, mình thấy cũng ok lắm, mình còn định mua nhiều quyển như vầy cho nó về học.
4
457599
2016-05-31 20:17:54
--------------------------
407328
2949
Mình mua sách này cho hai đứa cháu, chúng vốn rất thích chơi các trò chơi game trên điện thoại và các chương trình trên mạng, nhưng khi mình mua sách về và gợi ý kể chuyện cho chúng, thì chúng liền bỏ điện thoại và say sưa nghe kể chuyện. Như vậy, kể chuyện cho chúng nghe không chỉ giúp chúng phát triển khả năng tư duy ngôn ngữ mà còn giảm được ham mê trò chơi game của trẻ nhỏ, bên cạnh đó còn hun đúc thêm những đức tính tốt cho trẻ.

Sách song ngữ nên trẻ có thể học thêm tiếng anh khi nghe truyện. Hình ảnh dễ thương khiến trẻ thích thú và nhớ lâu. Nội dung truyện hay, lời văn dễ hiểu. Mình đánh giá đây là một quyển sách tốt cho trẻ con.
4
36756
2016-03-29 21:06:04
--------------------------
400945
2949
Nếu yêu cầu một quyển truyện màu vừa dày, vừa đẹp, vừa có nội dung giáo dục, vừa có giá phải chăng thì sẽ như mò kim đáy bể, chắc vì chúng ta còn phải học hỏi công nghệ in ấn sách dành cho thiếu nhi của những nhà xuất bản nước ngoài và trả tiền dịch thuật. Cuốn sách này không có hình minh họa đẹp bằng những cuốn sách trong cùng bộ sách, nhưng nội dung thì được. Nói chung là phù hợp cho lứa tuổi mẫu giáo và cấp 1, khi những cái đầu còn cực kỳ trong sáng.
4
63996
2016-03-19 23:41:06
--------------------------
233043
2949
Quyển sách Tủ Sách Truyện Cổ Tích - Goofy Can Đảm (Song Ngữ Anh - Việt), một câu truyện rất hay và cảm động về tình yêu thương của dê mẹ Goofy dành cho ba chú dê con của mình. Câu truyện mang tính giáo dục cao ngoài ra bé còn được thực hành Tiếng anh với những câu đối thoại rất gần gũi với cuộc sống hàng ngày. Hai bé nhà mình rất thích khi được Mẹ tặng cho quyển sách Truyện Cổ Tích - Goofy Can Đảm (Song Ngữ Anh - Việt) này. Đây là quyển sách hay. các mẹ có con nhỏ nên mua nhé! 
5
460604
2015-07-19 14:28:17
--------------------------
232865
2949
Quyển sách Tủ Sách Truyện Cổ Tích - Goofy Can Đảm (Song Ngữ Anh - Việt), một câu truyện rất hay và cảm động về tình yêu thương của dê mẹ Goofy dành cho ba chú dê con của mình. Câu truyện mang tính giáo dục cao ngoài ra bé còn được thực hành Tiếng anh với những câu đối thoại rất gần gũi với cuộc sống hàng ngày. Hai bé nhà mình rất thích khi được Mẹ tặng cho quyển sách Truyện Cổ Tích - Goofy Can Đảm (Song Ngữ Anh - Việt) này. Đây là quyển sách hay. các mẹ có con nhỏ nên mua nhé!
5
460604
2015-07-19 13:53:39
--------------------------
