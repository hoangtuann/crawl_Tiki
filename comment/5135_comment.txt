420838
5135
Mình khá là hối hận khi mua quyển sách này vì mua nó về chỉ xem được mấy cái hình linh tinh, chữ thì khó đọc, chỗ lên chỗ xuống. Mình cũng đã xem bản tiếng anh của Tom Gates trước đó. So với quyển này, nó được dịch cũng chưa sát nghĩa lắm. Nhưng mà nói chung, đối với những ai thích xem hình ảnh thì nên mua. Cũng vui đó. Thi thoảng mình có thấy mấy cái hình cười đau bụng luôn. Một điểm các bạn cần khắc phục nữa là chất liệu bìa sách, khá mỏng và dễ cong.
2
634478
2016-04-23 22:08:35
--------------------------
419943
5135
Tom Gates - Hầu hết những ý tưởng thiên tài là một quyển sách theo mình khá thú vị, mỗi trang sách là những hình ảnh minh hoạ hay những ý tưởng nhí nhố nào đó của anh chàng Tom không thể điên hơn được nữa, về khoản này khi đọc sách mình thấy khá giống mình, cứ như với cậu ý mình là một thằng bạn ấy, ha ha. Cầm cuốn sách trên thấy vui một, đọc nội dung và hình ảnh thấy vui mười luôn, lúc nào mình cũng nghĩ là có một người bựa và bá đạo đến thế sao.
3
993142
2016-04-22 12:05:30
--------------------------
407710
5135
Mình rất ấn tượng với những hình vẽ ngộ nghĩnh ở trang bìa. Sách nhẹ, có nhiều hình ảnh minh họa vui nhộn, vô cùng sáng tạo.
Thật không ngoài sự mong đợi, "Tom Gates - Hầu hết những ý tưởng thiên tài" mang cho mình những giây phút thực sự thoải mái sau khi trải qua những giờ học căng thẳng. Những ý tưởng đó không chỉ "thiên tài" mà còn rất "điên rồ".Nội dung truyện dí dỏm, hài hước. Sách phù hợp với nhiều độ tuổi . Mình rất thích thú khi đọc. Rất thích cuốn sách này.  
4
921299
2016-03-30 16:40:14
--------------------------
397843
5135
Mình thấy nhận xét của mọi người tích cực quá nên mới mua cuốn này về. Tại phần đọc thử không hiện ra chứ không thì không mua đâu. Đọc được một chút thì chán ngắt chán ngơ không đọc tiếp nổi nữa. Nội dung hơi bị tào lao, không có ý nghĩa gì hết. Biết là dành cho học sinh nhưng mà mình cũng không nghĩ học sinh thích đâu. Thua cả cuốn Nhật ký của ỉn. Mình hay mua truyện dành cho thiếu niên nhi đồng lắm nhưng mà đây là cuốn thất vọng nhất mình từng chọn.
3
143507
2016-03-15 15:04:31
--------------------------
329776
5135
cuốn truyện mang lại cho người đọc sự vui vẻ khi đọc câu chuyện về một học sinh lớp năm tên là Tom Gates. Cậu có một cuốn nhật ký hàng ngày để viết và vẽ về kinh nghiệm học của mình và cuộc phiêu lưu của gia đình. Tom thường cảm thấy chán ở trường và bịa ra những bào chữa vô cùng sáng tạo vì không làm bài tập về nhà của mình.Tôi cảm thấy người đọc ở tất cả các lứa tuổi đều sẽ tận hưởng các cuộc hành trình, hình ảnh minh họa và trí tưởng tượng của Tom Gates.
5
60839
2015-11-01 12:55:59
--------------------------
326879
5135
đây là sách thuộc thể loại doodle với nhiều hình ảnh minh họa, trang trí dễ thương nói là dành cho con nít nhưng người lớn đọc cũng được. Vậy nên, nếu ai là fan của thể loại như Diary of Wimpy Kids thì nên đọc tác phẩm này. ở Việt Nam cũng có bán bản tiếng anh nhưng mắc hơn bản Việt nhiều, không thích hợp cho ai luyện tiếng anh vì hình nhiều chữ ít nên mua bản việt là một cách để tiết kiệm mà lại còn sở hữu quyển sách tuyệt vời, siêu cấp đáng yêu. Hơi tiếc là các tác phẩm trong cùng bộ còn lại của Tom Gates thì lại luôn trong trạng thái hết hàng dài kỳ nên chưa mua được mà bản tiếng anh thì lại mắc. Mong tiki nhanh chóng có hang để mình có thể mua được trọn bộ.
5
55927
2015-10-26 19:19:54
--------------------------
247663
5135
Mình siêu thích những quyển truyện nhiều hình như thế này, kiểu như Nhật ký ngốc xít hay Nhật kí chú bé nhút nhát, nhưng Tom Gates còn hơn cả thế, hình minh họa siêu nhắng nhít dễ thương theo phong cách doodle, phải nói là cực tuyệt luôn í >< Nội dung hài hước dí dỏm vô cùng tận, lần nào đi học về mà mệt mỏi là cứ lăn vào đọc, đọc đi đọc lại vẫn thấy thích. Hiện tại Tom Gates có 4 tập mình đều đọc cả và cảm thấy là quyển nào cũng hay :3
5
385556
2015-07-30 12:12:06
--------------------------
211897
5135
Ấn tượng của mình đầu tiên về quyển sách này là những hình ảnh ngộ nghĩnh và dễ thương ở bìa sách , khi mở sách ra sẽ có một chú gián (mình thấy khá giống gián :D ) nhãy múa suốt các trang sách .
Ấn tương tiếp theo là về những hình vẻ đầy sáng tạo và dễ thương của cậu bé về những suy nghĩ của mình . Vì hầu có hình và chữ đan xen nhau nên bạn sẽ không thấy chán khi đọc .
Mình khá thích loại giấy của quyển sách , dễ đọc dưới đèn bàn và nhẹ khi cầm .
Xin cảm ơn .
5
554150
2015-06-21 11:31:29
--------------------------
170843
5135
Mình cực thích cuốn sách này, thích nhất là bìa sách, nhìn hơi rối nhưng vô cùng sáng tạo. Nét vẽ vui nhộn, những câu chuyện hài hước của cậu bé cùng với những hình vẽ đơn giản nhưng thú vị giống như một cuốn nhật kí. Đặc biệt hơn, mình rất ấn tượng với hình động của chú bọ đang nhảy nhót ở mép sách. Có thể nói đây là một cuốn sách khác biệt hoàn toàn với những cuốn sách khác bởi cách trình bày bằng những hình vẽ. Cuốn sách phù hợp với mọi lứa tuổi bởi những tình tiết dí dỏm và nét vẽ vô cùng dễ thương. Mình nhất định sẽ mua tập tiếp theo của bộ sách.
5
409715
2015-03-20 20:38:21
--------------------------
