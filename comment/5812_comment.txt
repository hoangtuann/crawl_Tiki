469146
5812
Bản thân nhan đề quyển sách một cách đơn giản đã nói lên được nội dung của quyển sách này. "Trẻ em như búp trên cành/ Biết ăn biết ngủ biết học hành là ngoan". Muốn trẻ ngoan thì từ khi còn nhỏ đã phải rèn giũa. Đặc biệt là lòng yêu thương con người. Và mình nghĩ bộ sách này là một sản phẩm thích hợp để các ông bố bà mẹ có thể chọn để có thể giáo dục con mình một cách đơn giản, sáng tạo, sinh động bằng hình ảnh. Nếu có thể nên thu thập nguyên bộ.
5
679926
2016-07-06 02:13:01
--------------------------
466844
5812
Cuốn sách là những mẫu chuyện nhỏ kể về quy tắc ứng xử trong cuộc sống hằng ngày. Cuốn sách không chỉ dành riêng dành riêng cho trẻ nhỏ mà nó còn dành cho tất cả mọi người. Nó dạy cho mọi người phải biết sống sao cho phải, cư xử đúng mực. các bậc phụ huynh nên sắm cho mình những cuốn sách như vậy để kể cho các bé nghe trước khi ngủ.
Mỗi câu chuyện mang tính chất giải trí nhẹ nhàng nhưng đầy tính nhân văn nếu chúng ta biết đào sâu, suy ngẫm về nó

5
338653
2016-07-02 22:57:55
--------------------------
454677
5812
Gieo mầm tính cách là một bộ truyện rất thú vị . Mỗi tập là một hạt giống gieo vào trẻ tính cách yêu thương . Tha thứ . Biết đối xử tốt với những người thân xung quanh mình . 
Sách in bằng giấy không được cứng lắm làm bé nhà mình lật dễ bị cong . Nhưng như vậy thì sẽ in được nhiều câu chuyện hơn mà không tốn thêm nhiều chi phí . 
Cuốn sách này thật sự rất bổ ích cho các bé. Các mẹ hãy sắm trọn bộ quyển này cho các bé nhà mình nhé.
5
593209
2016-06-22 04:41:37
--------------------------
440860
5812
Tiki có rất nhiều sách cho thiếu nhi với nhiều thể loại phong phú. Lựa chọn cho bé nhà mình cuốn sách này, vì yêu thương là đức tính không thể thiếu đối với mỗi người. Dạy cho bé biết cách yêu thương bản thân, yêu thương mọi người và cả những thứ xung quanh nữa. Mỗi một câu chuyện sẽ giúp cho sự yêu thương trong bé mỗi ngày một lớn lên. Biết cách yêu thương bé cũng sẽ nhận lại sự yêu thương. 
Sách được bọc rất cẩn thận. Giấy sáng, hình minh họa đáng yêu. Ngôn ngữ dễ hiểu, phù hợp với lứa tuổi các bé. Là một cuốn sách nên có trong tủ sách của gia đình.
4
606122
2016-06-02 13:41:23
--------------------------
432145
5812
Bìa sách hợp chủ đề, màu sắc rất hợp với hai từ "yêu thương". Cho nên em mình đã chọn ngay từ lần đầu thấy. 

Với những mẫu truyện ngắn, kích cỡ chữ khá to, quyển sách này rất phù hợp cho các bạn nhỏ. Các bạn nhỏ sẽ được học những bài học bổ ích về lòng yêu thương, giúp nuôi ấm tình cảm của chúng. Không những thế, sách còn khuyến khích việc đọc của các bé. Từ đó vừa hình thành cảm xúc, tích cách trong trẻ nhỏ, vừa trau dồi thêm khả năng đọc sách của các em.
4
1336690
2016-05-18 16:58:05
--------------------------
421290
5812
Có gì đẹp trên đời hơn thế. 
Người yêu người sống để yêu nhau. 
Hai câu thơ của Tố Hữu cũng giống như điều mà những câu chuyện nhỏ trong Yêu thương muốn hướng tới. Yêu thương giúp con người mạnh mẽ vượt qua khó khăn, là liều thuốc vực dậy với người cần được giúp đỡ. Sống yêu thương là cách sống tốt đẹp nhất mà ai cũng nên sống. Và một ý kiến thế này, cuốn truyện hình ảnh minh họa rất đáng yêu! Mình ấn tượng nhất với câu chuyện Hai vết sẹo, nó làm mình thật xúc động về tình cảm người mẹ dành cho con của mình!
3
505896
2016-04-24 20:16:35
--------------------------
386494
5812
Cuốn “Gieo mầm tính cách – Yêu thương” nằm trong bộ 6 cuốn do Nhà xuất bản trẻ phát hành. Cuốn này có các câu chuyện như : Trắng và đen, Tình thương, Giàu sang và Thành đạt, Chiếc hộp yêu thương, Chuyện an hem họ Điền, Lớn lên trong tình yêu thương, Thương chị, Hai vết sẹo, Thuận hòa, Câu chuyện quả cam, Miếng băng gạc, Ngôn ngữ bàn tay, Yêu thương không mệt mỏi.Mình mua bộ truyện này lâu rồi, do đặt mua trên Tiki nên giá giảm nhiều so với giá bìa. Truyện có hình minh họa dễ thương.
2
15022
2016-02-25 19:35:12
--------------------------
255529
5812
Sức mạnh của lòng yêu thương thật phi thường và mãnh liệt, giúp con người vượt qua được mọi khó khăn, thử thách trong cuộc sống. Phải đầy ắp yêu thương mới có thể trao yêu thương được cho người khác. Trong thời đại ngày nay, cần lắm những tấm lòng, cần lắm những bài học đạo đức sâu sắc để gieo vào lòng người những hạt giống yêu thương, gieo mầm nhân cách tốt. Quyển sách nhỏ gọn, có hình ảnh minh họa dễ thương, cùng nhiều câu chuyện nhỏ lồng ghép các bài học trong đó, rất bổ ích và thú vị. Cám ơn tiki.
5
110777
2015-08-06 09:21:42
--------------------------
185729
5812
Người ta nói giang sơn dễ đổi, bản tính khó dời. Vì vậy ngay từ lúc đầu hình thành tính cách, thói quen tốt cho trẻ là một điều tất yếu và quan trọng. Ngay khi còn bé, các em còn non nớt và đăc biệt là hay bắt chước theo hành động của người lớn từ lời nói, hành động và cách cư xử..... Chúng ta có thể giáo dục trẻ qua các câu chuyện kèm theo hình minh họa để trẻ dễ dàng nhận thức. Cuốn sách Gieo mầm tính cách - Yêu thương thật bổ ích, từ việc đưa ra những câu chuyện hàng ngày kèm theo đó là cách xử lý tình huống và những bài học rút ra từ câu chuyện giúp trẻ học tập nhanh chóng và đặc biệt là giúp người đọc phát triển lối tư duy, áp dụng vào thực tế.
5
519052
2015-04-19 20:38:20
--------------------------
