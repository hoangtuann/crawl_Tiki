283611
6778
Đọc "Cuộc cách mạng Công nghiệp lần III" của Jeremy Rifkin, tôi nhìn thấy một tương lai rõ ràng cho dù "chúng ta mới đang ở giai đoạn chuyển tiếp" - như tác giả đã nhận định. Tác giả đã đề xuất 5 trụ cột để thực hiện thành công cuộc cách mạng công nghiệp lần III để đưa nhân loại ra khỏi sự phụ thuộc và nhiên liệu có nguồn gốc CARBON (dầu mỏ, than...) và tiến tới một nền sản xuất bền vững, thân thiện môi trường dựa trên các nguồn năng lượng tái tạo được. Năm trụ cột đó là:
1. Sự chuyển dịch sang năng lượng tái tạo
2. Chuyển hóa các công trình xây dựng ở mọi lục địa thành các nhà máy điện mini để thu gom năng lượng tái tạo tại chỗ,
3. Áp dụng công nghệ Pin nhiên liệu Hydro và các công nghệ lưu trữ khác trong mọi công trình và xuyên suốt cơ sở hạ tầng để lưu trữ năng lượng gián đoạn
4. Sử dụng công nghệ Internet để chuyển đổi lưới điện truyền thống thành mạng điện thông minh (Smart Grid) có thể chia sẻ năng lượng từ các nguồn năng lượng phân tán,
5. Chuyển các phương tiện vận tải sang các phương tiện chạy điện và pin nhiên liệu có thể mua và bán điện thông qua một lưới điện thông minh ở cấp châu lục.

Tương lai năng lượng như thế nào sẽ quyết định đến toàn bộ cách con người sống và vận hành cuộc sống ra sao. 

Có thể hình dung ra tương lai qua đoạn trích dẫn từ cuốn sách: "Sự ra đời một cơ chế năng lượng tái tạo (Renewable Energy) được cung cấp bởi các tòa nhà (Mini Power Factory), được lưu trữ một phần ở dạng Hydro (Hydro Battery), được phân phối thông qua mạng lưới thông minh (Smart Grid) và kết nối với các phương tiện vận tải chạy điện không có khí thải (Electric Vehicle) sẽ mở cửa cho cuộc Cách mạng Công nghiệp lần III."
5
1196
2015-08-30 08:49:28
--------------------------
