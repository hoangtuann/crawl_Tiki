393257
8583
Khi mua quyển sách này là bởi vì mình thấy cách trình bày quyển sách rất hay nên mua thôi. Khi về nhà mở sách ra thì thấy hơi bất ngờ bởi quyển sách không những trình bày hay mà nội dung cũng khá tốt. Sách in đẹp thâm chí là ở từng nhân vật có thay đổi màu giấy nữa nhìn như là cuốn sổ tay vậy! Hình vẽ thì khá nhiều nhưng nội dung thì có lẽ những em bé nhỏ sẽ không thích đâu có lẽ các em cấp 2 sẽ thích hơn vì các em hiểu nhiều hơn bởi vì ở đây có mộ số thuật ngữ chuyên ngành. Nếu ai có ý định tặng quà cho các em cấp 2,3 mình xin góp ý là mua những quyển như thế này đẹp, sang trọng, ý nghĩa!
4
1085969
2016-03-08 14:30:02
--------------------------
356738
8583
Cuốn sách được trang trí bắt mắt với những hình ảnh minh hoạ nhiều màu sắc, ngộ nghĩnh và dễ thương.
Bìa sách khá đẹp mắt, chất lượng giấy tốt. 
Cuốn sách nói về các danh nhân có tầm ảnh hưởng đến hậu thế, đó là các nhà phát minh khoa học rất nổi tiếng trên thế giới. Với đủ các lĩnh vực. 
Sách chỉ tóm tắt nhưng khá rõ ràng dễ hiểu. 
Có thể làm quà tặng cho các em nhỏ kích thích các em có tinh thần đọc sách, ham học hỏi. 
Mình rất thích bộ sách Trường Học Danh Nhân Thế Giới
5
754539
2015-12-22 12:17:48
--------------------------
288587
8583
Quyển sách này nói về những danh nhân đã tạo nên những ảnh hưởng lớn đến hậu thế. Những danh nhân nổi tiếng đó là Edison nhà phát minh, Van Gogh họa sĩ tài năng, Conan Doyle tiểu thuyết gia trinh thám nổi tiếng với bộ tiểu thuyết Sherlock Holmes, anh em nhà Wright phát minh ra máy bay, Gandhi lãnh tụ Ấn Độ, Winston Churchill lãnh tụ Anh, Einstein nhà vật lý học, Helen Keller nữ văn sĩ Mỹ, Pablo Picasso họa sĩ Tây Ba Nha nổi tiếng, Patton tướng quân Mỹ tài ba, Montgomery đại tướng Anh bách chiến bách thắng.
5
698434
2015-09-03 18:01:47
--------------------------
287022
8583
Tôi yêu mến Henry Ford nên đọc ngay nội dung về nhân vật này khi đọc sách này. Nội dung và cách trình bày, cũng như hình ảnh minh hoạ hài hước thực sự khiến tôi hài lòng. Khi so sánh với nội dung của một cuốn sách dày khác về cuộc đời của Henry Ford thì tôi thấy nội dung sách tuy ngắn nhưng lại đầy đủ những nét quan trọng, nổi bật, những đóng góp quan trọng của nhân vật này. Và một phần thực sự làm tôi thích thú và yêu quý bộ sách này là các bài học rút ra qua cuộc đời của các vị danh nhân.
Nội dung sách có tính giáo dục tốt và cả tính giải trí, thực sự thích hợp cho tủ sách gia đình.
4
292190
2015-09-02 10:25:22
--------------------------
270753
8583
Cảm nhận của mình về quyển "Trường Học Danh Nhân Thế Giới - Danh Nhân Ảnh Hưởng Tới Hậu Thế"  là sách hay, đẹp, giá tưởng đắt so với một quyển sách dành cho thiều nhi nhưng khi nhận sẽ thấy rất hợp lý vì sách khá dày, bên trong được in màu rất đẹp, nội dung cũng rất thú vị chắc chắn các bé sẽ rất thích tuy nhiên bìa sách tiki giao không giống so với hình minh họa, màu sắc khác, không có hình nền đằng sau hay không có đánh số tập nói chung là không đẹp bằng tuy nhiên về tổng thể có thể nói là ổn, một quyển sách đáng mua
4
153008
2015-08-18 16:20:24
--------------------------
