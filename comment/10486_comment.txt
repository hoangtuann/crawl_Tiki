369967
10486
Cuốn này trước tiên là rất buồn vì có quá nhiều người nằm xuống vì trận chiến, ra đi vì sự điên khùng của Steve. Càng đọc càng thấy nhân vật Steve không nên có sự đồng cảm. Cho dù tuổi thơ ấu của Steve thiếu thốn tình thương, nhưng chỉ vì nhìn mọi chuyện từ 1 mặt rồi không nghe lời giải thích từ Darren, sắp xếp mọi kế hoạch để giết Darren, người bạn thân nhất và là người hi sinh tuổi thơ của chính bản thân để cứu mạng mình. Thật không thể chấp nhận, nhưng nhờ như vậy mà câu chuyện mới có thể kéo dài và càng lúc càng gay cấn và hồi hộp hơn
5
84723
2016-01-16 20:38:58
--------------------------
273958
10486
Đây là một trong những bộ truyện dài mà mình rất thích, cốt truyện hấp dẫn và bất ngờ, lối kể chuyện tự nhiên, gần gũi, tạo được cảm xúc cho người đọc. Tập 11 này mình không thích lắm, vì nó là tập kế cuối, mà mọi chuyện đang diễn ra một cách mơ hồ và khó đoán.Mình thích những tập trước của bộ truyện hơn vì khi đó Darren sống trong những ngày không có hận thù và chém giết
5
649505
2015-08-21 15:10:44
--------------------------
213782
10486
"Darren Shan trở về nhà. Thế giới sắp thành đại ngục. Kẻ thù cũ đang trực chờ. Hình như định mệnh quyết tâm hủy diệt Darren và thế giới sẽ được an bài với sự thống trị của Chúa Tể Bóng Tối"
nhưng chúa tể bóng tối rút cục là ai có thật là Darren hay là Steve không,hay có thật là chúa tể bóng tối có tồn tại không
các số phận nhân vật dường như đang được mở ra rõ ràng nhưng cuối cùng lại càng mông lung,áp chót tập cuối nhưng vẫn không biết các ý định của tác giả như thế nào
chờ đợi tập cuối cùng.
hi vọng kết thúc sẽ giải đáp tất cả thắc mắc đó
đây là bộ truyện nếu nói là kinh dị thì chưa đủ tầm,nhưng thực sự hấp dẫn và lôi cuốn theo phong cách kể ngôi thứ nhất của tác giả
chỉ còn tập cuối cùng,bộ truyện kì ảo này sẽ kết thúc
5
70555
2015-06-24 09:28:51
--------------------------
191188
10486
Ai cũng đều có 1 phần đen tối, xấu xa. Như lời của bà phù thủy thì không cần biết ai Steve hay Darren thì 1 khi đã trở thành chúa tể (chắc chắn như định mệnh :))) thì kẻ đó chắc chắn sẽ hủy diệt thế giới đúng theo ý của Lão Tiny. Tập này thì có nhiều cái chết rất đau lòng. Và nếu theo 1 logic nào đó người đọc sẽ nghĩ ngay đến vị trí Dark Lord chắc chắn sẽ thuộc về Darren khi nỗi căm thù đã lên đến tột độ! Bánh răng của định mệnh đã bắt đầu quay và không gì có thể dừng nó lại nữa.
4
166935
2015-05-01 10:05:28
--------------------------
