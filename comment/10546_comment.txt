198896
10546
"Tây du ký" là tác phẩm nổi tiếng của Ngô Thừa Ân. Với khả năng viết truyện tuyệt vời, ông đã minh họa những nhân vật hư cấu như có hồn, có sức sống, với phiên bản này được nhà xuất bản Mỹ Thuật chuyển thể lại càng sinh động, rõ ràng và phù hợp với các em nhỏ. Ẩn sâu trong mỗi phần của cuộc hành trình đi thỉnh kinh của bốn thầy trò đường tăng là những bài học rất giống với đời sống mà chúng ta cần chú ý. Thiết kế bìa với hình Tôn Ngộ Không ngộ nghĩnh, dễ thương.
5
351857
2015-05-20 23:06:16
--------------------------
73913
10546
Tây du ký - bộ phim nổi tiếng của thế kí đã gắn liền với tuổi thơ của bao người. Xem phim đã hay, nay đọc truyện còn hay hơn. Truyện miêu tả rõ ràng hơn về hoàn cảnh cũng như nội tâm nhân vật, khiến ta như hóa thân vào nhân vật đồng thời khiến ta hiểu thêm về truyện. Đặc biệt NXB Mỹ Thuật đã từ tác phẩm nổi tiếng trong tứ đại danh tác ấy đã chuyển thể sang một phiên bản khác để bạn đọc, đặc biệt là trẻ em dễ hiểu hơn về ý nghĩa, nội dung cũng như thông điệp mà tác giả muốn truyền đạt. Bìa sách được thiết kế rất đẹp, rất dễ thương, phù hợp với các em nhỏ.
4
57869
2013-05-10 17:34:05
--------------------------
65756
10546
Đây là một tác phẩm không thể đọc 1 lần!
Bởi đơn giản nó chứa triết lí nhân sinh khổng lồ trong từng câu chữ (điều mà phim ảnh chưa thể chuyển tải hết).
Theo Tỳ kheo Thích Chơn Thiện trong quyền "Bàn Về Tây Du Ký Của Ngô Thừa Ân", 5 nhân vật chính là năm thành tố trong mỗi con người, từng con số, từng hình ảnh đều có ý nghĩa của riêng nó và toàn bộ kinh Bát nhã được Ngô Thừa Ân chuyển vào đấy một cách tài tình.
Đọc để suy ngẫm, đọc để sống và đọc để giải thoát.
Nhưng hơi tiết rằng quyển "Bàn Về Tây Du Ký Của Ngô Thừa Ân" hiện khá khó kiếm, nó như con đò để đưa ta sang sông, nơi " kho báu thật sự đang cư ngụ.

5
107173
2013-03-27 15:17:39
--------------------------
