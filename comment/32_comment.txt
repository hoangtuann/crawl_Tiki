542589
32
Một cuốn sách ko thể thiếu đối với những bạn muốn giỏi tiếng anh.
5
2085306
2017-03-15 08:37:34
--------------------------
494500
32
Cuốn sách đáp ứng được nội dung mà mình yêu cầu, rất tiện lợi trong việc bỏ túi. Mình học được kha khá từ mời, từ đồng nghĩa từ cuốn này. Chất giấy thì khỏi bàn rồi, quá tuyệt vời.
5
118789
2016-12-07 09:37:38
--------------------------
485834
32
Quyển từ điển Anh - Anh đầu tiên của tôi. Một quyển sách thật sự rất hữu dụng, nhất là với những bạn học tiếng anh nâng cao hoặc ôn thi Ielts, Toefl... Tăng khả năng sử dụng và mở rộng vốn từ. Học tiếng Anh khó nhất là khi mình muốn diễn tả điều gì đó khi nói hoặc viết mà không biết nhiều từ mới, đều phải dùng lặp đi lặp lại các common words, nhưng từ khi mua được em Dic này về, mình chẳng phải lo lắng gì nữa. Từ được giải thích ngắn gọn, dễ hiểu, kèm thêm cách phân biệt khi nào dùng từ này, khi nào dùng từ kia cho phù hợp với quy tắc và ngữ cảnh, như kiểu phân biệt sự khác nhau giữa wicked và evil ấy, làm khả năng học và vốn hiểu biết tăng lên rõ rệt, đồng thời bổ sung khá nhiều cho mình về phần collocations và patterns, các ví dụ đúng sai rất cụ thể. Từ điển lại khá nhỏ, gọn, mình toàn cho vào túi áo khi đi học, giấy khá dày, đẹp và mịn, mua chẳng nuối tiếc tý nào các bạn ạ.
5
519996
2016-10-06 14:46:48
--------------------------
485307
32
Tuy rằng mang tiếng là từ điển, nhưng cuốn này rất nhỏ, đúng chuẩn bỏ túi. Cuốn sách nhỏ nên chữ cũng nhỏ, chú thích cũng rút gọn lại so với cuốn thesaurus cỡ lớn. Dù vậy mình thấy cuốn này phù hợp để học hơn, vì nó gọn và dễ đem theo vào lớp. Cuốn lớn cũng không phải là quá nặng đến mức không mang đi nổi, chỉ là cuốn nhỏ thế này mới không làm bạn ngán khi phải thường xuyên cho nó vào cặp. Với lại, mình thấy số từ đồng nghĩa trong cuốn này đã đủ nhiều rồi.
5
303694
2016-10-01 13:29:17
--------------------------
471805
32
Sách này sẽ tốt tùy theo cách sử dụng của từng người. Như vài bạn chăm học tiếng anh sách này giúp rất nhiều. Còn với đứa hí hửng mua như mình về trưng bày cho đẹp vì quá lười tra :v
Có điện thoại rồi từ điển đi vào dĩ vãng.
Dù biết phát âm với nghĩa của cuốn này chuẩn hơn nhiều trên mạng nhưng lười nên cũng không tra.
Tóm lại khuyến khích cho các bạn chăm chỉ mua ạ, bạn nào quen dùng điện thoại tra rồi cẩn thận mua về bỏ xó (như mình) * huhu xót tiền*
5
513917
2016-07-08 22:22:32
--------------------------
468693
32
Khi mới mua về, mình hơi bất ngờ vì không nghĩ từ điển lại bé như thế, nhưng lại vô cùng thích vì nó rất đáng yêu :)) Mình cầm rất vừa tay và cũng rất tiện lợi khi mang đi học nữa. Oxford Thesaurus pocket này được trình bày rất tỉ mỉ, và từ đồng nghĩa trái nghĩa cũng rất chuẩn xác, mình thấy rất hữu ích. Nhưng có một điều là sách này không có phần phiên âm, làm mình hơi khó khăn trong việc tìm cách đọc. Nếu bổ sung thêm điều này nữa thì quá tuyệt rồi. Với cả vì là pocket nên sách hơi ít từ, nhưng cũng không trách được vì nó ít từ mới nhỏ gọn được. Nói chung mình rất thích sản phẩm này.
5
597043
2016-07-05 12:22:03
--------------------------
433863
32
Khi luyện viết mình thường bị lặp từ rất nhiều khiến bài văn không hay nên rất cần những từ đồng nghĩa. Quyển từ điển này là từ điển bỏ túi nên khi mang đi học khá tiện vì nhẹ và không cồng kềnh. Các từ trong đây tuy không quá nhiều nhưng là những từ thông dụng mình hay cần dùng đến. Thêm nữa ngoài chỉ ra cho mình các từ đồng nghĩa, từ điển còn phân biệt các sắc thái nghĩa của từ và chỉ ra khi nào thì nên dùng một từ để thay thế. Nói chung là tuyệt!!! Luyện viết tiếng Anh mà không có quyển này thì không ổn chút nào. Học từ đồng nghĩa cũng nâng vốn từ của mình lên nữa!
5
129322
2016-05-21 21:53:19
--------------------------
431003
32
Ưu điểm đầu tiên mà ai cũng có thể thấy được là cuốn từ điển rất nhỏ, gọn, nhẹ, mình có thể mang đi học thêm hay đi bất kì đâu một cách dễ dàng. Cuốn từ điển rất phù hợp cho những ai cần nâng cao, mở rộng vốn từ vựng, luyện thi đại học, hoặc luyện thi chứng chỉ IELTS. Nhất là phần Writing task 2 trong IELTS yêu cầu người đọc cần lượng từ vựng kha khá để viết bài. Đối với học sinh như mình thì giá thành như thế này cũng khá là đắt, tuy nhiên so với những gì cuốn từ điển này đem lại thì hoàn toàn đáng với đồng tiền đã bỏ ra.
5
178929
2016-05-16 14:00:34
--------------------------
381247
32
Một cuốn sách nhỏ gọn tiện lợi khi mang theo bất kỳ nơi nào, rất phù hợp làm quà tặng cho bạn bè trong lớp luyện thi IELTS. Trình bày đẹp, logic, diễn đạt cách so sánh, cách dùng từ đồng nghĩa sao cho thật phù hợp với ngữ cảnh, từ đó làm phong phú và chính xác hơn cho câu trong phần speaking và writing. Mình đã giới thiệu cuốn sách này đến nhiều bạn bè, thầy cô và họ rất hào hứng khi có một cuốn sách nhỏ gọn, hữu ích. Nếu có phần mềm để cài đặt vào máy để việc tra cứu được nhanh và tiện lợi hơn nữa thì sẽ rất hay. Lúc mở bưu phẩm ra bản thân mìnn rất ưng ý với sản phẩm đặt, sau khi mình giới thiệu cuốn sách đến mọi người, rất nhiều bạn đã đặt mua hoặc nhờ mình mua dùm, thật vui vì đã giới thiệu được một cuốn sách bổ ích đến với mọi người để có thể nâng cao điểm số IELTS, nhanh chóng đạt được mục tiêu đặt ra. Hy vọng Oxford sẽ thêm được nhiều cuốn sách hay như thế này nữa. Cảm ơn Tiki đã đem được những cuốn sách hay đến với độc giả.
5
597129
2016-02-16 21:26:33
--------------------------
380813
32
Vì là pocket nên rất nhỏ gọn và dễ dàng sử dụng, rất thích hợp cho giới văn phòng, sinh viên, học sinh khi không tiện mang theo quyển từ điển lớn. Sản phẩm này rất đáng đồng tiền bát gạo, của nxb Oxford nên chất lượng cũng tương xứng với đồng tiền. Ngoài quyển này ra, mình còn mua verbs and tenses và cuốn pocket grammar của cùng nxb Oxford. Do cuốn từ điển này khá nhỏ, giấy mỏng nên dễ bị rách nên mình sử dụng bìa plastic của Tiki bọc lại bảo vệ cover của cuốn sách này
4
59160
2016-02-15 21:54:48
--------------------------
379702
32
Tôi đã từng cảm thấy vốn từ vựng của mình quá hạn hẹp trước những bài writting mà mình viết ra, thậm chí nhiều khi chính mình viết xong mà chẳng muốn đọc lại vì ôi thôi, nó quá nhàm chán. Cũng từng đó tôi cũng chẳng còn hứng thú gì với việc luyện viết. Nhưng cuốn sách này đã thay đổi hoàn toàn suy nghĩ của tôi, những từ ngữ trong đây được giải thích rất ngắn gọn và dễ hiểu, việc học từ cũng trở nên đơn giản hơn. Tuy chỉ là một cuốn từ điển nhỏ nhưng thực sự nó còn hữu ích hơn nhiều cuốn sách từ vựng mà tôi đã từng đọc qua. Thật sự đây là một cuốn sách tuyệt vời!
5
464390
2016-02-12 11:05:54
--------------------------
377119
32
Em đang luyện thi IELTS , cuốn từ điển này thực sự đã giúp đỡ em rất nhiều trong phần Writing . Nhờ nó mà bài viết của em đỡ bị gạch đỏ chi chít hơn nữa còn giúp em mở rộng vốn từ ngữ, biết cách sử dụng từ đúng đắn trong các trường hợp khác nhau. Trình bày nội dung rất ổn, rất khoa học nên dù chữ khá nhỏ nhưng vẫn dễ đọc dễ hiểu. Cuốn từ điển nhỏ gọn nên em mang đi xem ngay cả trên xe bus, tiết kiệm được rất nhiều thời gian học . Em rất thích sản phẩm này !
5
52762
2016-02-01 17:55:21
--------------------------
365559
32
Mình cần mua quyển này đã lâu, nay nhân dịp thấy trên Tiki giảm giá mình mua ngay. Đây là quyển từ điển nhỏ gọn nhưng rất hữu ích và tiện lợi. Nó giúp bạn mở rộng vốn từ rất tốt, nhất là khi làm bài writing mà xài kèm quyển này thì sẽ viết rất hiệu quả. Quyển sách vừa tay, cất vừa túi, in đẹp, trình bày rõ ràng, dễ hiểu, dễ đọc. Mỗi ngày xem một ít cũng rất tốt. Với những ai yêu thích học anh văn cũng như ôn luyện tiếng anh thì mình nghĩ đều nên mua quyển này. 
5
72266
2016-01-08 11:10:51
--------------------------
354097
32
Dù mình khá tiếc là mình mua cuốn này mà không được bọc book care do khổ nhỏ nên sách để lâu dễ bị quăn góc bìa cũng như bìa nhanh cũ, hay bị xước và bị bạc màu (dân giữ sách như mình không thích điều này).
Nhưng nhìn chung thì chất lượng từ điển của Oxford là quá tuyệt vời luôn rồi. Giấy trắng, mỏng vừa phải nên lật qua lật lại không sợ dễ rách nhưng 2 trang kế tiếp hay bị chồng lên nhau, lật nhanh thì hơi khó, lại phải ngồi tỉ mẩn tách ra. Chữ nhỏ nhưng rõ ràng, dễ nhìn.
Về nội dung thì "tuy nhỏ nhưng võ đầy mình". Ngoài cung cấp từ đồng nghĩa thì còn có ví dụ rất dễ hiểu ở từng mục từ.
Đây sẽ là trợ thủ đắc lực cho việc luyện tập writing của mình.
5
768261
2015-12-17 12:27:15
--------------------------
350746
32
sách từ điển bỏ túi này trình bày ngắn gọn , súc tích, nhiều từ cùng nghĩa (có cả trái nghĩa), ví dụ sáng ý. rất phù hợp cho bạn nào muốn nâng cao vốn từ cả văn nói và văn viết. mình đã dùng và thấy rất tốt. Sách nhỏ gọn có thể mang theo bên mình bất kỳ lúc nào cũng có thể lấy ra xem lại. chất liệu giấy tốt, bền. Mình thường hay dùng cuốn này cho môn viết, để vốn từ phong phú không bị lặp lại. Hi vọng cuốn sách này cũng là sự lựa chọn tốt cho các bạn.
5
157108
2015-12-11 07:18:48
--------------------------
340038
32
Mình mới mua và đọc cuốn này trong một ngày thôi, cảm nhận đầu tiên của mình là chất lượng giấy in và cách bố trí từ khá là bắt mắt, không như những cuốn từ điển bình thường.Cuốn sách có bo tròn 2 góc nữa, dễ dàng bỏ túi mà không lo bị gấp mép sách. Rất nhỏ gọn và tiện lợi, tuy nhiên, với góc độ của một người học tiếng anh beginer như mình thì nó chưa thực sự đầy đủ, sách không có phần phiên âm quốc tế, nên việc tra từ và phát âm của nó thì gặp khá là nhiều khó khăn khi vẫn phải dùng từ điển khác để tìm cách phát âm. Trong sách trình bày theo cấu trúc : từ - ví dụ- từ đồng nghĩa- từ trái nghĩa. Ví dụ minh họa thì khá ít. Theo mình nghĩ thì cuốn này thích hợp hơn với những bạn nào có ý định học và viết tiếng anh nhiều vì nó có nhiều từ đồng nghĩa và trái nghĩa, có thể giúp mình đa dạng hóa từ trong đoạn văn.
3
462003
2015-11-19 07:58:48
--------------------------
338510
32
Cuốn này thật sự rất hữu ích cho những ai muốn trau dồi kĩ năng writting của mình. Trước giờ khi làm.bài viết mình rata hay băn khoăn về cách dùng từ có chính xác ngữ cảnh không? Nhưng nhờ cuốn này giúp sức kĩ năng viết của mình được cải thiện rất nhiều. Hơn nữa khi tra một từ lại biết thêm nhiều từ nên từ vựng cũng tăng lên đáng kể .
Tuy nhiên vì cái này theasurs nên sẽ không có phần nghĩa đâu mà chỉ đưa ra những từ cùng nghĩa với cách phân biệt thôi nên mấy bạn nên cân nhắc khi mua kẻo nhầm với cuốn advanced nhé.
Với lại vì là pocket nên cũng không nhiều từ lắm, nếu chỉ để tra nhanh hoặc muốn tiện dụng cầm mọi lúc mọi nơi cho việc học thì nên mua, còn nếu muốn học dịch thuật thì nên mua bản đầy đủ nhew
4
857825
2015-11-16 12:20:25
--------------------------
325431
32
Mình vừa nhận được cuốn sách này ngày hôm qua. Mình rất bất ngờ! Thú thực khi xem sản phẩm này trên tiki mình nghĩ nó thiên về 1 quyển từ điển mà khi cần từ nào thì tra từ đó, chứ không thể đọc hay nghiên cứu như sách được. Thế nhưng khi bắt đầu đọc thì mình vô cùng hứng thú, cách diễn giải cô đọng, rất chi tiết, rõ ràng. Mình thậm chí có thể ngồi đọc nó mà không chán. Cuốn sách này làm mình liên tưởng đến cách mà cô giáo tiếng anh dạy vậy, kiến thức đã được tổng hợp và đang được truyền đạt lại cho người đọc vô cùng dễ hiểu.
Sách nhỏ gọn, có thể mang theo trong túi và dùng mọi nơi mọi lúc. Giấy đẹp. Mình rất ưng và cảm thấy rất xứng đáng khi bỏ tiền mua. 
5
887088
2015-10-23 14:27:53
--------------------------
302107
32
Mình đang trong quá trình học môn Anh văn để thi đại học . Có rất nhiều sách để lo toan và lên kế hoạch để mua và cuốn từ đồng nghĩa tiếng anh là một trong những cuốn rất quan trọng . Mình phát hiện và qua tìm hiểu thấy giá ở tiki rất vừa nên quyết định mua . Nó thật sự rất bổ ích , trang sách rất tốt , cách sắp xếp từ ngữ rất rõ ràng , sáng sủa dễ nhớ , dễ tìm . Hi vọng là cuốn sách này giúp ích cho mọi người cũng như nó đã giúp ích cho mình.
5
781966
2015-09-14 23:40:17
--------------------------
291102
32
Theo mình, đây là một quyển sách bổ trợ tìm hiểu chuyên sâu về từ vựng tiếng anh rất tiện lợi và hiệu quả. Nó không những giúp nâng cao vốn từ mà còn giúp mình phân loại được cách dùng những từ đồng nghĩa một cách đúng đắn. Thông qua việc tìm hiểu loại từ như vậy, vốn từ vựng và hiểu biết về tiếng anh sẽ tăng lên đáng kể, giúp ích rất nhiều trong việc hiểu ngữ nghĩa của tiếng anh đặc biệt là làm cho bài viết được hay và chuẩn xác hơn nhiều. Quyển sách phù hợp với những bạn khá tiếng anh và muốn tìm hiều chuyên sâu hơn về môn học này.
5
123775
2015-09-06 00:05:24
--------------------------
290745
32
Điểm cộng đầu tiên của quyển này chính là thiất kế gọn, nhẹ, tiện lợi để dễ dàng cho vào túi hoặc cặp xách mà không tốn kém nhiều diện tích và cũng không gây cảm giác nặng khi mang theo. Từ vựng được sắp xếp theo đúng thứ tự từ A đến Z nên rất thuận lợi cho việc tra cú nghĩa của từ. Những từ đồng nghĩa được liệt kê ra khá là đầy đủ. Ngoài ra từ điển còn hướng dẫn chúng ta cách sử dụng từ trong trường hợp nào cho hợp lí và đúng nghĩa nhất. Mình rất thích quyển này.
5
408083
2015-09-05 18:55:20
--------------------------
281006
32
Chất lượng giấy tốt. Nhỏ gọn, nhẹ, tiện lợi khi di chuyển xa. Tuy nhiên, do nhỏ gọn nên số từ trình bày không được nhiều nên nhiều từ mình tìm hoải chả thấy, nếu có điều kiện nên mua bản to ấy thì đầy đủ hơn. Từ điển trình bày khá rõ ràng, có ví dụ cụ thể rất dễ hình dung dùng trong ngữ cảnh nào, từ đồng nghĩa, trái nghĩa trình bày rành mạch thuận tiện cho việc học. Ai học tiếng Anh thì nên đầu tư một quyển để nắm vững từ vựng hơn, tự tin trong giao tiếp cũng như viết essay.
5
135002
2015-08-28 02:53:25
--------------------------
271953
32
Những người học chuyên sâu tiếng Anh hoặc tiếng Anh học thuật sẽ rất cần những quyển từ điển như thế này. Lúc mình chưa mua thì phải tra trên điện thoại hoặc Google, còn phải tìm xem từ đó nên và không nên dùng trong ngữ cảnh nào nên sẽ rất cực. Sau khi thấy Tiki đang giảm giá mình mua về liền, quả thật rất là tiện lợi. Nó giúp cho bài viết của mình phong phú và hay hơn nhờ biết được nhiều từ đồng nghĩa trái nghĩ. Các bạn học tiếng Anh khá nên mua quyển này về tham khảo.
5
62926
2015-08-19 16:44:13
--------------------------
268903
32
Mình mua quyển từ điển này để nâng cao vốn từ vựng để cải thiện khả năng tiếng anh của mình. Sách nhỏ gọn in rõ ràng không lem bẩn , tuy nhỏ nhưng rất rõ ràng và dễ đọc. Lúc đầu mình còn chưa quen xài từ điển Anh-Anh, những dùng quyển này mình dần dần quen và cảm thấy rất dễ và hứng thú, khi tra ngoài biết nghĩa của từ mình còn biết được nhiều thứ khác liên quan để vận dụng tốt từ đó hơn. Rất cần cho những người học tiếng anh, một người bạn đồng hành tốt!
5
163292
2015-08-16 22:15:08
--------------------------
265685
32
Hiện nay môn tiếng anh thật sự khiến nhiều bạn học sinh phải gặp rất nhiều khó khăn về vốn từ vựng. Quyển từ điển này sẽ giúp các bạn hiểu rõ hơn về nghĩa của các từ, những trường hợp đồng nghĩa và trái nghĩa của từ. Giúp các bạn dễ dàng hơn trong việc xác định các từ để viết bài luận. Đặc biệt với thiết kế nhỏ gọn, bạn có thể dễ dàng mang theo bên mình khi để trong balo hoặc túi quần mà không sợ về vấn đề trọng lượng mà những quyển từ điển kia hay mắc phải.
5
474983
2015-08-14 08:55:13
--------------------------
259504
32
Từ điển Oxford nổi tiếng về chất lượng, rất hữu ích cho người học tiếng Anh. Kỹ năng writing đòi hỏi phải có lượng từ vựng lớn cùng khả năng sử dụng từ đồng nghĩa linh hoạt. Cuốn từ điển này là một công cụ cần thiết để có thể viết luận tốt, từ điển giải thích rất chi tiết và dễ hiểu về từ đồng nghĩa, trái nghĩa và cách dùng cho đúng ngữ cảnh. Từ điển pocket size nên rất tiện lợi, có thể mang theo mọi lúc mọi nơi nhưng cũng vì là cỡ nhỏ nên lượng từ vựng không nhiều lắm.
5
177708
2015-08-09 15:18:04
--------------------------
251546
32
Hiện nay bộ giáo dục có thêm phần viết luận vào bài thi đại học môn tiếng anh nên có thể nói cuốn sách bỏ túi này rất hữu ích đối với những đứa học sinh như tui. Cuốn sách tổng hợp những từ đồng nghĩa, trái nghĩa với lối trình bày rất rõ ràng và dễ hiểu, có đưa ra ví dụ cụ thể để phân biệt những từ dễ gây nhầm lẫn nên khi tra sách tui khỏi phải băn khoăn liệu dùng từ này đã đúng với văn phạm hay chưa. Tuy nhiên, sách cũng có một vài hạn chế, ví dụ như việc thiết kế 1 cuốn sách bỏ túi nên từ vựng trong cuốn sách không nhiều lắm, có những từ tui cần tra nhưng tra mãi không ra, tui nghĩ nếu có điều kiện thì các bạn hãy mua kết hợp cuốn pocket này với cuốn Oxford Learner's Thesaurus(bản đủ chứ không phải bỏ túi) để sử dụng, như vậy sẽ hiệu quả hơn. Nhưng nói chung, cuốn từ điển bỏ túi này rất hữu dụng và là một người bạn đáng tin cậy trong con đường chinh phục tiếng Anh của những bạn học Anh văn.
5
5131
2015-08-02 22:21:15
--------------------------
251531
32
Trong ba cuốn từ điển cỡ nhỏ mình mua, mình thích cuốn này nhất. Lý do đầu tiên vì màu sắc của cuốn từ điển làm mình khá dễ chịu khi tra từ. Thứ hai, mình đọc và thấy được sự da dạng của từ, cách dùng từ, chúng đã giúp mình cải thiện vốn từ và làm tăng sự liên kết giữa các từ mình biết và từ mới, giúp mình dễ nhớ hơn. Sự tiện lợi của kích thước từ điển là không cần phải bàn. Lúc nào mình cũng mang theo chúng dễ dàng. Một người bạn nhỏ luôn hữu ích.
5
32038
2015-08-02 22:06:11
--------------------------
249131
32
Mình đã mua và rất hài lòng với quyển từ điển này, từ điển oxford luôn là lựa chọn hàng đầu với những người học tiếng anh. Trong quá trình viết writing thì quyển từ điển hỗ trợ mình rất nhiều trong việc sử dụng từ đồng nghĩa, giúp tránh sử dụng trùng lặp từ. Bên cạnh đó khi học reading sử dụng từ điển này giúp mình có thể học nhiều từ một lúc, học một cụm các từ đồng nghĩa giúp vốn từ của bạn tăng lên một cách nhanh chóng. mình thấy đây là một cuốn sách cực kì hữu dụng cho những người học anh văn
5
300269
2015-07-31 14:07:17
--------------------------
229239
32
Thêm một ấn bản tốt nữa từ NXB Oxford. Từ điển được thiết kế theo dạng pocket size nên thật sự rất vừa vặn để cho vào ba lô để tôi luôn mang theo bên mình và lấy ra học những lúc rảnh rỗi. Giấy hơi mỏng nhưng rất mịn, lật từng trang có cảm giác rất thích. Vì ấn phẩm này tập trung bổ sung về từ đồng nghĩa, trái nghĩa cũng như giải thích cụ thể trường hợp nào cần phải sử dụng từ nào (VD: achievement hay accomplishment ?) nên sẽ phù hợp cho những bạn đã có một trình độ Anh văn căn bản, và muốn mở rộng thêm vốn từ. Phục vụ cho bản thân trong việc Viết và Nói đa dạng hơn, tránh lặp ý; cũng như khi Nghe và Đọc sẽ không bỡ ngỡ nhiều khi găp một từ mới vì: "À, từ này là từ đồng nghĩa với từ ... mà!".
5
172485
2015-07-16 17:14:45
--------------------------
223637
32
Đối với một người thường xuyên sử dụng tiếng Anh thì cuốn sách này rất hữu dụng và tiện lợi khi mang theo bên người. Mình rất thích sử dụng các cuốn từ điển từ Oxford bởi vì cách giải thích đơn giản nhưng dễ hiểu. Có các ví dụ cụ thể để người đọc dễ hình dung và liên tưởng tới. Mặc dù là cuốn pocket nhưng size chữ đủ lớn để người cận như mình đọc. Đợt này mình đặt 2 cuốn vừa là cuốn này và cuốn Business. Dùng kèm theo 2 cuốn rất phù hợp với người chuyên làm việc và giao tiếp bằng tiếng Anh với các đối tác nước ngoài.
5
540471
2015-07-07 13:16:02
--------------------------
222845
32
Mình tự nghiên cứu và trao dồi thêm tiếng Anh ở nhà nên khi mua quyển này về mình rất thích, sách in rất rõ ràng, giấy đẹp và nội dung rất hay, mình đã tìm kiếm rất lâu 1 quyển sách hệ thống lại cho mình từ đồng nghĩa và trái nghĩa đáng tin cậy và khi phát hiện quyển này mình cảm thấy rất tự tin khi sự dụng thêm được kho từ vựng phong phú này cho công việc học tiếng anh của mình. Một điểm cộng nữa cho sách là sự nhỏ gọn, nó giúp mình có thể mang theo đi bất kì đâu! ^^
5
351847
2015-07-06 11:34:10
--------------------------
218595
32
Khi nói hay viết tiếng anh, mình vẫn hay mắc lỗi lặp từ quá nhiều vì không tìm được từ đồng nghĩ để diễn đạt. Sau khi mua cuốn từ điển này, mình cảm thấy việc dùng từ linh hoạt hơn, khi viết luật hay đặt câu cũng dễ dàng và hiệu quả hơn rất nhiều.
Theo mình, cuốn từ điển này cực kì cần thiết đối với tất cả những người học tiếng Anh, đặc biệt là những ai muốn thông thạo ngôn ngữ này. Số lượng từ đồng nghĩa và trái nghĩa vô cùng phong phú, được trình bày rõ ràng, dễ hiểu cộng với kích thước nhỏ gọn là những ưu điểm mà mình rất thích ở cuốn từ điên này. Chất lượng giấy tốt và cách thiết kế gáy sách hình vòng cung khiến sách không bị quăng mép khiên mình cảm thấy rất thoải mái và dễ dàng khi tra từ.
5
138880
2015-06-30 18:13:26
--------------------------
217791
32
Mình khá hài lòng với quyển từ điển này, bìa sách đẹp, nhò gọn, tiện dụng khi mang đi học, làm việc hay bỏ túi. nội dung về nhừng từ đồng nghĩa rất hữu dụng cho những ai muốn làm việc với tiếng anh và những bạn học tiếng anh. riêng về tính chính xác thì mình thấy oxford dường như được xem là lựa chọn hàng đầu. tuy nhiên, đây là những từ đồng nghĩa, trái nghĩa và chỉ toàn bằng tiếng anh, nên những bạn chỉ mới học cơ bản thì khó có thể sử dụng hết được sách. nên mình có lời khuyên cho các bạn nên lựa chọn kỹ trước khi mua để tránh khó khăn khi sử dụng. còn với những bạn khá tốt về tiếng anh thì nó thật sự hữu ích.
5
605801
2015-06-29 23:05:25
--------------------------
211524
32
Mình đã đặt 2 cuốn này từ Tiki cho mình và thằng bạn, nhưng cả 2 đứa đều chẳng dùng được. Từ điển này gồm những từ đồng nghĩa, trái nghĩa và giải thích nghĩa của từ bằng những key word gợi ý. Nhìn tổng quan thì nó rất hữu ích. Tuy vậy, đối với một người tiếng Anh chỉ tầm cơ bản và một người không chú trọng vào kĩ năng Writting thì dường như nó hoàn toàn vô ích. Vì thế, lời khuyên của mình là, nếu bạn muốn nâng cao trình độ viết hoặc trau dồi vốn từ thì hãy mua nó ngay. Còn không thì hãy dùng loại Oxford Student sẽ tốt hơn đấy!!
5
95911
2015-06-20 18:36:59
--------------------------
208109
32
Cuốn sách thực sự rất hữu ích cho việc sử dụng Tiếng Anh một cách chính xác!
Các từ cùng trường nghĩa được sắp xếp cùng 1 nhóm với nhau, phân biệt sắc thái nghĩa và cách sử dụng chuẩn xác. 
Ta cũng có được những từ đồng nghĩa để sử dụng thay thế khi bí từ (để tránh hiện tượng lặp từ trong tiếng anh, đặc biệt là khi viết luận)
Có ví dụ đầy dủ. Cuốn sách với chất lượng giấy tiêu chuẩn, kích thước bỏ túi đêm lại cảm giác tiện dụng và yêu thích.

Dù vậy, mình vẫn khuyên các bạn có nhu cầu tiếng Anh cao nên mua cuốn sách to kia hơn. Vì đầy đủ và chi tiết hơn rắt nhiều!
5
323143
2015-06-14 09:52:39
--------------------------
191691
32
*Điểm nổi bật của cuốn từ điển này là “độ đáng tin cậy” bởi vì đây là từ điển Oxford nên nội dung các bạn khá yên tâm nhé. Nếu các bạn ở trình độ mới bắt đầu thì hãy tận dụng cuốn từ điển này khi đi học nhé vì nó sẽ kích thích bạn sử dụng tiếng Anh trong mọi tình huống. Điểm đặc biệt là cuốn sách sẽ cho bạn biết các từ đồng nghĩa và cách dùng qua các cụm từ, ví dụ, so sánh, có cả từ trái nghĩa nữa. Điều này cực kì hữu ích với các bạn học chuyên ngành tiếng Anh và cả các bạn muốn nâng cao trình độ hay cấp độ của mình, đặc biệt là những bạn muốn thi các kì thi quốc tế. Nếu nhìn sơ, chúng ta sẽ thấy trước mắt là cuốn từ điển này sẽ hữu ích cho Nói và Viết những nhìn sâu và suy nghĩ kĩ thì cuốn từ điển giúp ta cả 4 kỹ năng Nghe – Nói – Đọc – Viết vì dù sao mục đích chính của sách là cung cấp từ vựng. Nếu bạn biết những từ đồng nghĩa nhiều thì bạn sẽ thể hiện được bài viết, bài nói rất mới mẻ mà không bị lặp lại từ và tránh bị gài bẫy bởi các từ đồng nghĩa, trái nghĩa trong các bài đọc và nghe.
*Điểm mà mình hối tiếc là sách không có phiên âm (vì đâu phải ai cũng biết cách đọc hết đâu những chúng ta vẫn có thể tra cứu cách đọc theo nhiều cách khác nhau, chắc là tác giả muốn giản tiện để sách gọn nhẹ đó mà).
4
45667
2015-05-02 13:55:13
--------------------------
189845
32
Đây thật sự là một bảo bối đối với những người học tiếng Anh, đặc biệt đối với những người luyện viết ở trình độ từ B1 trở đi.Nó nhỏ gọn, chuẩn mực từ nhà xuất bản nổi tiếng thế giới. Nội dung được biên tập một cách rõ ràng, khoa học và rất đầy đủ. Kích cỡ nhỏ gọn, tính di động rất cao, rất tiện lợi đem theo bên người. Chất lượng giấy thì phải nói là tuyệt vời. Giấy bóng và mịn, sờ rất thích. Mặc dù bây giờ việc tra từ điển bây giờ đã tiện lợi hên rất nhiều cùng với sự phát triển của smart phone và internet, nhưng đối với việc học tiếng Anh nghiêm túc thì cuốn từ điển này vẫn không có gì địch lại được.
5
594760
2015-04-28 09:47:41
--------------------------
155946
32
trong các bài viết Academic English như Writing của TOEFL hay IETLS, việc sử dụng các từ đồng nghĩa để lặp đi lặp lại các từ khóa là rất cần thiết. Điều này không chỉ tăng thiện chí của giám khảo đến bài viết vì sự đa dạng của từ vựng và cách sử dụng từ, nó còn giúp bạn tăng vốn từ vựng và sự linh động trong khả năn sử dụng tiếng Anh. Không chỉ có vậy, ta cũng có thể trở nên nhanh nhẹn hơn trong việc dùng từ dưới dạng trang trọng và không trang trọng. Tóm lại, nhờ cuốn từ điển này, việc học từ sẽ trở nên thật dễ và thú vị.
4
41811
2015-02-02 17:56:14
--------------------------
155621
32
Khi đặt mua tiki sản phẩm này, mình cực kì ưng ý luôn, Sách trông rất nhỏ gọn, dễ dàng bỏ vào túi, bìa đẹp lắm. Nhưng cái mà mình thích nhất là khi mở cuốn từ điển ra, sách trình bày nội dung kiến thức rất khoa học ( từ đồng /trái nghĩa, ex,...) . Bìa chất lượng rất tốt, thôm, trơn, giấy dày và nhìn bắt mắt bởi ba màu trắng, xanh và đen. Vì là từ điển pocket nên chữ nhỏ nhưng mực in chữ rất đậm. Một cái hay của quyển sách là có sách giúp ta phân biệt cái mà ta hay nhầm lẫn trong cách dùng. Và trong một số test chuyên anh thì lượng từ của dictionary này sẽ không bao giờ thiếu. Tại sao không mua thử một cuốn để dùng? Chắc hẳn bạn sẽ thích.
5
529121
2015-02-01 15:51:16
--------------------------
152633
32
Mình biết cuốn này nhờ một em gái giới thiệu cho. Ấn tượng đầu tiên của mình từ lúc nhìn sách trên tiki chính là bìa cuốn sách, cực kì đẹp và có cảm giác của một cuốn sách chất lượng. Tới khi tiki giao sách tới nhà thì ấn tượng tiếp theo chính là cuốn sách được trình bày rất khoa học, chi tiết với các phần ví dụ, các từ đồng/trái nghĩa, collocation và thích nhất chỗ cách dùng; bìa cứng, trơn, giấy trắng & khá dày, dùng 3 màu xanh - trắng - đen cộng với chữ tuy cỡ nhỏ nhưng mực in rõ nên không quá khó khăn khi tra cứu (mang lên lớp học mà mấy cô bạn cũng khen ầm ĩ vì sách đẹp quá =))). Nhưng điểm cộng nổi bật của sách chính là phần nội dung. Quả thật quá trình làm bài tập cloze test và writing của mình đã dễ dàng hơn rất nhiều khi có sách. Trước giờ mỗi khi làm bài tập mình đều phải lên google search cách dùng của các từ, rất mất thời gian và phụ thuộc (nhất là khi ở trường wifi rất yếu thì coi như không làm bài tập được), nhưng bây giờ khi thấy phân vân về cách dùng các từ thì chỉ cần mở sách ra là có ngay trong tay đáp án đúng, cực kì vui và tạo cho mình hứng thú làm bài tập. Một điểm trừ nhỏ xíu có lẽ là do cuốn sách là bản bỏ túi nên có một vài từ không có phần phân biệt với các từ gần nghĩa khác mà chỉ có phần opp với collocation nên đôi lúc mình cũng hơi hụt hẫng. Tuy nhiên mình nghĩ đây là một cuốn sách mà các bạn học TA ở trình độ khoảng B1 trở lên rất nên có, nhỏ gọn, tiện ích và quan trọng nữa là chất lượng, tại sao lại không sở hữu ngay cho mình một cuốn? Mua cuốn này rồi mình cũng đang mong cuốn Oxford Idioms mà tiếc là tiki lại hết hàng mất tiêu rồi :'(
5
502810
2015-01-23 18:30:58
--------------------------
144168
32
Mình vừa mới mua quyển sách vào ngày 5/12 trong đợt tiki giảm giá. Tiki giao sách đúng hẹn và bên trong sách vẫn còn mới, không bị nát. Sách có bìa đẹp vì cũng là sách của Oxford University Press rất uy tín. Bên trong các từ cần tra in màu xanh, dễ nhìn, đẹp mắt, có ví dụ sử dụng từ, các từ đồng nghĩa, trái nghĩa ở dưới rất tiện lợi cho việc tra cứu. Hơn nữa còn có phần phân biệt các từ dễ nhầm lẫn. Vì vậy mỗi lần học từ ta có thể biết thêm các từ đồng nghĩa và trái nghĩa của nó. Trong bài cloze test đối với 1 học sinh chuyên Anh như mình thì việc biết nghĩa Tiếng Việt của từ cần điền không đủ mà còn cần chọn từ giữa các từ đồng nghĩa sao cho phù hợp với ngữ cảnh nữa. Bài Writing và Speaking IELTS hay các bài kiểm tra trình độ khác cũng đòi hỏi vốn từ phong phú và vì thế quyển Oxford Learner's Pocket Thesaurus Dictionary rất cần thiết với những bạn đam mê và chuyên sâu môn Tiếng Anh. Nó còn rất nhỏ gọn và dễ đem theo. Nên mua 1 cuốn để dùng !
5
501445
2014-12-25 19:22:45
--------------------------
120378
32
Trong quá tình học writing, đau đớn sau phần cấu trúc và ngữ pháp chính là từ vựng. Nhiều khi viết xong, ngồi đọc lại, bỗng thấy mình lặp từ quá mức và dùng từ rất... không sáng tạo. Vò đầu bứt tai đi tra từ điển thì thiệt tình là rất biếng, vì một là ngồi nhìn mớ chú thích dài cả cây số, hai là phải tra việt-anh (không biết mọi người thế nào, chứ mình rất ghét phải tra việt-anh...) Đến khi mua cuốn này, mình cảm thấy như đang bước lên khỏi địa ngục. Giải thích của từ điển rất ngắn gọn, dễ hiểu. Ngoài cung cấp từ đồng nghĩa, nó còn cung cấp từ trái nghĩa thế nên tha hồ ngồi viết không lo lặp từ. 

Từ điển nhỏ gọn, giấy tốt, chữ có hơi nhỏ một chút nhưng mà trình bày đẹp, không rối mắt lắm đâu. Hoàn toàn đáng đồng tiền bát gạo các bạn ạ.
5
332975
2014-08-11 20:28:32
--------------------------
118640
32
Đây là cuốn sách hơn 25000 từ đồng nghĩa, trái nghĩa và đây là ấn bản mới nữa nên mình rất thích và liền mua nó. Quả thật không hối hận khi mua bởi vì bìa sách màu xanh dương dịu, giấy tốt, chữ thfi hơi nhỏ. Cách sử dụng sách này được hướng dẫn không kĩ lắm nên mình hưoi bối rối khi sử dụng và đặc biệt làm mình hụt hẫng là khi tra 1 từ nào đó, lại cho ví dụ mà không nêu rõ cáhc sử dụng từ ấy. Nhìn chung, đây là cuốn sách bỏ túi tiện dụng, tra khi nào mình cần và phân biệt 2 từ với nhau.Vfa chắc chắn rằng đây là cuốn sách không thể thiếu trong cặp các bạn học sinh chuyên anh như mình. Yêu sách này lắm!
4
367544
2014-07-30 11:51:42
--------------------------
92298
32
1 cuốn từ điển nhỏ gọn nhưng lại rất đầy đủ và hữu ích. Không chỉ đơn giản là 1 cuốn từ điển để tra cứu những từ đồng nghĩa, trái nghĩa, mà hơn thế, nó còn hỗ trợ rất nhiều trong việc phát triển kĩ năng viết: không bị lặp từ, tránh dùng sai từ... Bên cạnh đó, từ điển còn đưa ra những ví dụ, những giải thích cụ thể cho những cụm từ đồng nghĩa, phân biệt cách dùng cho phù hợp với từng ngữ cảnh, bởi 1 trong những khó khăn khi học ngoại ngữ đó là dùng đúng từ, đặc biệt là những từ có cùng nét nghĩa....
5
5592
2013-08-26 14:47:25
--------------------------
80974
32
Đây là cuốn từ điển "không thể thiếu" trong cặp sách của mình. NHỏ gọn, đầy đủ, hữu ích. Đặc biệt khi học writing, có quyển này mình  không lo việc bị lặp từ, thiếu vốn từ nữa. Trong đấy có luôn cả những hướng dẫn, phân biệt cách dùng của các từ nữa ( nhiều từ đồng nghĩa mà cách dùng không giống nhau đâu).
Nói chung là ai học tiếng anh thì rất nên có 1 quyển từ điển từ đồng nghĩa. Mà quyển to thì hơi bất tiện ở khoản linh động, quyển bỏ túi như thế này vừa đủ những từ mình cần, lại vừa gọn nhẹ dễ mang đi.
5
53085
2013-06-14 20:14:29
--------------------------
79377
32
Trong quá trình học anh văn, có rất nhiều dạng bài tập đòi hỏi cần phải biết cách phân biệt những từ gần nghĩa, đồng nghĩa và cách dùng chúng trong những trường hợp khác nhau. Và sẽ rất mất thời gian nếu như bạn dùng những từ điển thông thường. Với cuốn từ điển này, ta sẽ nhanh chóng hoàn thành những dạng bài tập trên và thu về cho mình rất nhiều kiến thức bổ ích.
Với kích thước nhỏ gọn và bìa bọc plastic của tiki, bạn sẽ dễ dàng bảo quản và mang theo bên mình mọi lúc mọi nơi để cải thiện vốn từ vựng cũng như vốn kiến thức của mình.
5
46582
2013-06-06 21:05:26
--------------------------
66949
32
TiKi ơi, mình đợi mua cuốn sách này lâu rồi nhưng giờ vẫn để hết hàng.Mình có nhập chọn vào "thông báo cho tôi khi có hàng" nhưng vẫn không thấy phản hồi.
Mình muốn đạt 2 cuốn nhưng vẫn chưa thấy có, có thể cho  mình biết khi nào có được không?
Pocket Thesaurus là cuốn sách mà các bạn đang theo học ngành anh ngữ như mình rất cần có, nó sẽ giúp ích rất nhiều trong các môn học đọc, viết và cả biên phiên dịch hay phân tích văn học nữa.Không những đang học mà những ai có sở thích học tiếng Anh hay chuẩn bị du học và làm việc trong môi trường tiếng Anh cũng cần nên có. Đó là lời mà mình được thầy dạy dịch truyền lại.
Nếu được sử dụng hình thức đặt sách trước hay sách sắp về lại, mong TiKi sớm gửi thông báo cho mình biết nhé!
Chúc TiKi sẽ có thêm nhiều sách hay để giới thiệu cho giới trẻ Việt Nam như cuốn sách này.
3
74122
2013-04-03 16:36:43
--------------------------
54474
32
Trước hết cảm ơn Tiki về bìa sách được bao sạch sẽ! 
Cuốn sách nhỏ gọn có thể đem đi bất kỳ nơi nào. Nội dung thì không thể chê được. Đây là cuốn sách mình kiếm từ lâu! Cuối cùng cũng có nó trong tay. 
Cách hướng dẫn sử dụng từ đồng nghĩa và cả trái nghĩa . sử dụng từ chính xác trong từng tình huống khác nhau trong cả văn nói và văn viết, với những ví dụ cụ thể. Giúp tránh lỗi lặp từ trong các bài essays.  Cuốn sách hữu ích không chỉ cho các bạn chuyên ngành mà còn giúp cho các bạn mới bắt (tránh nhầm lẫn về lỗi dùng từ) ! Tuy nhiên vì là cuốn từ điển bỏ túi nên chữ hơi nhỏ... nhưng không hối tiếc khi mua cuốn này!
4
38875
2013-01-09 13:21:25
--------------------------
47357
32
Cuốn sách này đã giúp mình rất nhiều. Từ khi mua cuốn sách này ở tiki mình hứng thú trau dồi từ mới hơn bao giờ hết. Đây không phải là cuốn từ điển thông thường chỉ nêu ra nghĩ tiếng Việt cho mỗi từ mới mà nó như là người thầy dạy tiếng Anh vậy. Nó chỉ ra cho mình những từ đồng nghĩa trái nghĩa và còn cho nhiều ví dụ minh họa dễ hiểu. Cuốn sách này còn dạy cho mình từ nào dùng trong hoàn cảnh nào chứ không đơn thuần chỉ ra những từ đồng nghĩa trái nghĩa. Nhiều từ đồng nghĩa nhưng có từ sử dụng ở hoàn cảnh này còn ở hoàn cảnh khác thì đôi khi lại phải sử dụng từ đống nghĩa khác. Vừa nhỏ gọn (kích thước) vừa nhiều kiến thức. Nếu muốn trau dồi vốn kiến thức về Tiếng Anh nhiều hơn nữa thì cuốn sách này sẽ là người bạn đồng hành với bạn. 
5
70498
2012-11-23 14:04:22
--------------------------
43434
32
Sau thời gian dùng cuốn này, mình rút được những điều sau:
- Nâng cao khả năng dùng từ, tránh dùng lặp từ đặc biệt rất hữu dụng khi viết các bài report, essay.
- Bên cạnh những từ đồng nghĩa còn có những từ trái nghĩa nên giúp chúng ta có thể hệ thống và nhớ từ dễ dàng hơn.
- Những từ đồng nghĩa được phân biệt cách dùng cho phù hợp với những ngữ cảnh khác nhau (đưa ra ví dụng cách dùng đúng và cách dùng sai)
- Những từ trong từ điển là những từ được dùng phổ biến.
-  Giấy và mực in đẹp tuy nhiên cách trình bày hơi rối mắt.
4
63508
2012-10-21 23:38:30
--------------------------
36239
32
cuốn sách thuận tiện đem đi bất kì nơi đâu , thông tin cung cấp đầy đủ và bổ ích . tôi rất thích phần nhấn âm vì nó giúp tôi làm bài tập dễ hơn . dịch nghĩa từng từ khá dễ hiểu và số lượng từ khá mới , nhiều , chính xác . cuốn từ điển này giúp tôi cải thiện nhiều việc như : nhớ từ  nhanh hơn , gọn nhẹ trong việc đem theo , tra từ nhanh hơn ,... học tiếng anh cũng tốt hơn và thích thú hơn đối với tôi . từ điển có nhiều tiến bộ vế hình thức lẫn nội dung . Hơn nữa lại xuất xứ từ đại học oxford nên rất đáng tin tưởng . đọc và thấy chất lượng , đó chính là những gì tôi muốn nói với các bạn 
5
50774
2012-08-08 11:14:23
--------------------------
35923
32
Điểm khiến tôi thích thú nhất khi vừa nhận được quyển từ điển này là nó rất nhỏ gọn, kích thước bé hơn máy tính f(x) mà học sinh cấp hai, ba thường dùng. Chất lượng giấy tốt, tuy nhỏ nhưng hệ thống từ ngữ được phân bố hợp lý, rõ ràng, mỗi từ ngữ đều có câu ví dụ cách dùng, từ trái nghĩa, những điều cần lưu ý và cả những câu sai ngữ pháp để ta phân biệt nữa. Đối với những ai đang học ngoại ngữ nói chung, để có thể giao tiếp với người khác, đòi hỏi không chỉ vốn từ rộng, mà phải am hiểu cách sử dụng chúng hiệu quả, quyển từ điển này là công cụ hỗ trợ hữu ích để giúp ta đạt được điều đó.
5
29256
2012-08-04 17:30:13
--------------------------
31933
32
Oxford Learners Pocket Thesaurus là một cuốn từ điển rất tuyệt vời.

Điểm cộng đầu tiên cho cuốn từ điển này  là hết sức nhỏ gọn. Thứ hai là nó có những cách thức trình bài hết sức khoa học, dễ tìm kiếm từ ngữ tra cứu, cho nên có tính linh động rất cao. Thứ ba, cuốn từ điển này giải quyết một khó khăn lớn trong việc học tiếng Anh cho giới học sinh, sinh viên chúng ta, đó là phân biệt những từ gần nghĩa.

Trong lúc học tiếng Anh, có những từ gần nghĩa mà chúng ta có tưởng chúng đồng nghĩa hoàn toàn, nên rất dễ bị sai trong những bài tập phân biệt nghĩa của từ (theo mình dạng này cực kì "khó chịu").

Cuốn từ điển Oxford Learners Pocket Thesaurus là một sản phẩm rất đáng mua đấy, các bạn ạ !

5
23953
2012-06-30 18:28:07
--------------------------
27323
32
Khi nhận được sách mình rất  ngạc nhiên vì sách quá nhỏ gọn. 510 trang nhưng không dày tí nào. Chất lượng giấy tốt, cách bố trí , trình bày từ ngữ rất dễ xem! 
Và tin chắc rằng mình sẽ biết được nhiều từ vựng hơn vì tính năng đặc biệt của cuôn 1 từ điển này, bên cạnh từ gốc có thêm những từ đồng nghĩa ,cộng thêm , quyển sách rất hay khi chỉ ra những điểm khác nhau giữa những từ gần nghĩa !!! 
5
8690
2012-05-18 13:40:54
--------------------------
25784
32
Những bài tập phân biệt giữa các từ gần giống nhau về mặt ngữ nghĩa nhưng lại có những cách dùng hoàn tòan khác nhau thường khiến học sinh bối rối và lúng túng. 
Nhưng nay với cuốn Oxford Learner's pocket thesaurus việc đó sẽ trở nên vô cùng đơn giản. 
Trình bày đẹp, khoa học, ví dụ ngắn gọn và dễ hiểu, cuốn từ điển này sẽ giúp ích cho bạn rất nhiều không chỉ ở việc phân biệt cách dùng của từ mà còn đem lại cho bạn vốn từ phong phú. Ví dụ, khi tra từ EFFECTIVE bạn sẽ biết thêm về từ PRODUCTIVE và từ trái nghĩa của nó là INEFFECTIVE. 
Sách của nhà xuất bản Oxford University luôn luôn đảm bảo về mặt chất lượng. Và 1 lần nữa, họ đã ko làm cho người học Tiếng anh phải thất vọng.
5
21347
2012-04-29 18:46:44
--------------------------
12533
32
Cuốn từ điển này mình đã được thầy giáo dạy tiếng anh dưới thiệu mua về học và mình nhận thấy rằng: nó thực sự rất tuyệt vời.

Đây là trong những cuốn sách không thể thiếu cho việc học tiếng anh. Cuốn sách hỗ trợ rất nhiều cho việc học  và mở rộng vốn từ vựng cực kỳ hữu ích đặc biệt áp dụng trong các kỳ thi quốc tế đòi hỏi tính học thuật cao như IELTS.

Với tập hợp các từ đồng nghĩa, hỗ trợ rất nhiều cho người học trong việc viết sử dụng các từ học thuật, không lặp lại các từ đã sử dụng.

Dưới dạng bỏ túi nên rất thuận tiện khi mang bên mình mọi lúc mọi nơi.

Rất đáng để mua đấy các bạn ạ ^^

4
13281
2011-10-09 10:12:47
--------------------------
