127335
10945
Đầu tiên tôi đặt mua cuốn sách này chỉ vì nghe cái tên của nó, "Phố của làng" nghe dịu dàng và khắc khoải xa xăm. Tác giả Dạ Ngân tôi cũng chưa đọc sách nào của bà viết cả, quyết định mua sách này vì chỉ nghĩ đơn giản là biết đâu lại tìm được một điểm chung. Nhưng khi cầm nó trên tay, lật giở từng trang tôi bàng hoàng và thầm cảm ơn cho cái quyết định của mình. Đó là may mắn của tôi, may mắn là tôi được đọc nó. "Phố của làng" mang về cho tôi bao kỉ niệm, những kí ức tuổi thơ không tên, trong veo, quen thuộc mà trong cuộc sống bận rộn ai nấy đã vô tình quên đi. Sách là một sự trải dài tâm sự, đọc của tác giả ta tìm ra cái bấy lâu mình lãng quên mất, những điều rất quen trong lòng ai cũng có nhưng phải bon chen, bận rộn với đời khiến ta bẵng đi cũng bởi không ai nhắc lại. "Phố của làng" làm được điều đó, lật giở từng trang sách là từng điều rất đổi thân quen hiện về, là đâu đó tiếng tuổi thơ đồng vọng, những kí niệm thân quen mà chan chứa. Đọc sách tôi biết quý hơn những gì đã qua và yêu hơn những gì đang cùng tôi sống. 
Tôi muốn cảm ơn tác giả, cám ơn quyển sách này. Quyển sách mang về biết bao cảm xúc tha thiết ngọt ngào. Tôi sẽ giữ gìn sau này cho con mình đọc để cho nó biết trân quý những gì đã đang và sẽ cùng ta chung sống.
4
120702
2014-09-23 22:18:49
--------------------------
