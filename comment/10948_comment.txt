118951
10948
"Kẻ săn người" là quyển truyện kinh dị đầu tiên tôi mua. Truyện có những tình tiết rất lôi cuốn và gây ra nhiều bất ngờ cho người đọc. Chánh thanh tra Piere Saint Hilaire lại vướng vào 1 vụ án rắc rối. Vợ anh chết, cấp dưới của anh bị cho là giết vợ anh cũng đã bị giết và chính anh đã bị dính vào bẫy của hung thủ, người hung thủ này khiến chúng ta không ngờ tới đó chính là Sarras - đồng nghiệp của anh. Các tình tiết của truyện khiến ta phải theo dõi liên tiếp không thể bỏ sót tình huống nào được. Ngoài ra, nhân vật Wuenheim - giám đốc Ủy ban thanh tra cũng là nhân vật được tôi chú ý. Anh là 1 người tài tuy nhiên sự phán đoán của anh lại không chính xác. Anh cũng là người làm cho Eva mất niềm tin ở anh để Sarras có cơ hội tiếp cận. Cuối cùng, anh lại phải hi sinh trước mũi dao của Sarras, làm tôi cũng thấy đáng tiếc. Tuy nhiên anh cũng đã hoàn thành tốt nhiệm vụ của 1 chánh thanh tra rồi :) Bố cục của truyện khá tốt, phông chữ đọc khá rõ.
Tôi không thích kết thúc của truyện lắm. Việc bắt Sarras là quá nhanh. Các tình tiết không được hấp dẫn như những chương trước. Nhưng nói chung truyện là 1 tác phẩm đáng để đọc. Không liên quan lắm nhưng tôi thấy vụ án này tựa tựa vụ án trong phim "Bằng chứng thép" :D
4
285606
2014-08-01 12:36:27
--------------------------
