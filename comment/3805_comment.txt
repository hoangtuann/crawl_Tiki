264836
3805
Soạn văn trước khi vào học là vấn đề muôn thưở nên mình đã không ngàn ngại khi quyết định cuốn Học tốt Ngữ văn 12 để giúp việc soạn bài diễn ra nhanh chóng và đầy đủ hơn. Sách có bìa đẹp, giấy bên trong tuy khá mỏng nhưng tạm ổn. Nhưng quan trọng hơn là nội dung sách, nội dung được trình bày rất ư là chi tiết nên làm ta dễ hiểu, bố cục cũng có trình như sách giáo khoa nên rất dễ tìm kiếm. Chất lượng in tốt, rõ ràng. Học tốt Ngữ văn là quyển sách thiết yếu cho bạn nào muốn học tốt ngữ văn hơn!
4
415181
2015-08-13 14:58:33
--------------------------
