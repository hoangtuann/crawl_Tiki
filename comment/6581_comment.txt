459428
6581
 Sống có mục đích - điều mà nhiều người đang mải mê tìm kiếm, nhưng bạn ơi, hãy tìm nó ở một chỗ khác nhé, quyển sách này, theo mình không giúp cho bạn tìm được mục đích đâu. Mình mua vì cũng đang muốn tìm mục đích cho bản thân nhưng sách thì cứ nói chung chung, đại thể, rồi còn nói chưa tới. Đọc không khéo còn bị mất phương hướng hơn. Bìa sách có cái cây đa sắc màu đẹp. Câu Biết điều gì là quan trong để có cuộc sống mong ước ư?, vậy mà bên trong không trả lời được câu hỏi đã đặt ra một chút nào. 
1
605606
2016-06-26 08:05:12
--------------------------
204090
6581
Mục đích sống rất quan trọng, đây là điều mình đã nhìn thấy được sau nhiều năm sống bị chệch hướng và kết quả là không đi được đến đâu cả. Nhà cho dù có muốn xây đẹp bao nhiêu đi nữa cũng cần một bản vẽ trước để từ đó mà triển khai cũng vì vậy mà muốn có một tương lai ổn định thì trước hết mình phải xác định mục đích của mình ở hiện tại là mình muốn mình sẽ là người như thế nào để từ đó mà cố gắng. Quyển sách này sẽ chỉ bạn điều đó. Nhưng sau khi đọc mình có nhận xét rằng vì là quyển sách do các giáo sư, tiến sĩ người Mĩ viết nên có ngôn ngữ rất khó tiếp nhận cũng vì thế mà khó tận dụng được hết các điểm mạnh mà tác giả đề ra nhưng mình vẫn cho rằng đây là một quyển rất đáng để đọc qua một lần nếu chúng ta là người sống vô định và đang muốn tìm một mục đích sống thật rõ ràng.
3
274995
2015-06-03 08:28:32
--------------------------
191152
6581
Đây là một quyển sách kỹ năng sống điển hình của các tác giả Mỹ.
Đánh vào tâm lý những người chưa có mục đích, lý tưởng sống rõ ràng.
Sách có nội dung dàn trải, đôi khi cảm thấy mệt mỏi vì đọc những điều "luôn luôn đúng" lặp đi lặp lại. Sau đó dẫn chứng được đưa ra từ những nguồn chưa xác thực nhầm củng cố thêm lập luận của tác giả. 
Một khía cạnh nào đó, quyển sách khiến những người thiếu trải nghiệm trong cuộc sống cảm thấy cuộc sống đẹp đẽ hơn vì những vấn đề được giải quyết tương đối dễ dàng. 
Dù sao nếu là một người đang đi tìm câu trả lời cho câu hỏi "mục đích sống của bản thân là gì?", thì đây có thể là một quyển sách để tham khảo.
2
602581
2015-05-01 09:03:00
--------------------------
137624
6581
"Sống có mục đích" là một cuốn sách khá hay, rất nên có trong hành trang đến thành công của mỗi người. Nhìn qua đã thấy, cuốn sách có bìa ngoài đẹp, hấp dẫn, sinh động. Chất lượng giấy in không thể chê vào đâu được, biên dịch cũng rất tốt, rất hay. Cuốn sách có nội dung không mới những vẫn chưa hề hết nóng, đề cập đến vấn đề quan trọng quyết định sự thành công hay thất bại, đó là mục đích của cuộc sống, khi đã xác định được mục đích rồi, bạn sẽ nhanh chóng đạt được những thành công trong cuộc sống, được tiếp thêm sức mạnh vượt qua khó khăn hay giúp bạn cảm nhận được cuộc sống này ý nghĩa biết nhường nào...
Nhược điểm của cuốn sách: các ví dụ đưa ra chưa sát với thực tế lắm,....
Nhưng dù sao, mình vẫn đánh giá rất cao cuốn sách này.
5
414919
2014-11-27 15:56:45
--------------------------
