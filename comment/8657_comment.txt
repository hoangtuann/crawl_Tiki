333024
8657
Có vẻ như cuốn sách này phù hợp với các bạn sinh viên hơn là các bạn học sinh vì đa phần các bí quyết trong sách đều hướng đến thành phần sinh viên. Sau khi đọc qua một số sách kỹ năng thì mình nghĩ cuốn sách này chưa thật sự sâu sắc và hữu ích các phương pháp trong sách được đề cập một cách sơ sài và không lôi cuốn, bên cạnh đó giá cả của sách cũng không tương xứng với nội dung, đây thật sự không phải là sự lựa chọn tốt đối với bản thân mình
2
83343
2015-11-07 12:17:25
--------------------------
294714
8657
Mình nghĩ đây là cuốn sách mà tất cả các bạn học sinh hay sinh viên đều nên sở hữu. Đúng như tên của nó "Vượt qua kỳ thi một cách hoàn hảo" cuốn sách sẽ trang bị cho bạn một kỹ năng hoàn hảo nhất để đối phó với các kỳ thi. Từ phương pháp học tập, ghi chép hiệu quả, đến cách ôn tập, chuẩn bị cho kỳ thi, cách làm bài thi như thế nào để đạt được kết quả cao nhất tất cả đều được tác giả nhắc đến trong cuốn sách này.
Cuốn sách nhỏ nhưng có võ này rất tiện dụng, bạn có thể mang nó theo bên mình đến nhiều nơi và chia sẻ các bí kíp ấy với những người bạn của mình nữa.
5
145385
2015-09-09 16:54:39
--------------------------
294675
8657
Đây là cuốn kỹ năng sống mà mình thấy có trang bìa dễ thương nhất!Mình mua cuốn này khi mình sắp thi đại học ,mình mới thi xong,với hy vọng là cuốn sách sẽ giúp ích cho mình!Cuốn sách tuy không phải là hoàn hảo nhưng những kiến thức ở cuốn sách này rất bổ ích cho các bạn sắp thi đai học,cuốn sách hội tụ những điều mà bạn cần phải chú ý để đạt được kết quả cao trong kỳ thi!Nhưng mình nghĩ ngoài cuốn sách này ra chủ yếu là tự lực bản thân của các bạn chứ đừng phụ thuộc vào sách mà không học bài thì cũng như không!!
4
413637
2015-09-09 16:12:19
--------------------------
258712
8657
Mình đã mua quyển sách này gần một năm trước để chuẩn bị cho việc đi thi đại học, cũng thấy được khá nhiều thứ hay ho để áp dụng. Bây giờ đọc lại, mặc dù thấy cuốn này hơi sơ sài một chút bởi chỉ đơn thuần tổng hợp lại những kiến thức có khá nhiều ở những quyển khác nhưng khá đầy đủ những điều cần chú ý để đi thi được điểm cao. Nói chung là mình thấy nó khá phù hợp cho các em học sinh đang lên kế hoạch chuẩn bị cho những kì thi quan trọng sắp tới.
4
192952
2015-08-08 19:58:01
--------------------------
257041
8657
Tôi mua quyển sách này khi mà cả mẹ và con đều vật lộn cho kỳ thi học kỳ của bé. Nói chung bé mới bước vào năm đầu tiên đi học nên còn lạ lẫm (với lại sao mà trẻ con bây giờ học lớp 1 mà học quá trời thứ). Cho nên tôi tìm đến quyển sách này.
Nói chung theo nhận xét của tôi là tạm ổn, trùng lặp ý tưởng đâu đó ở sách này sách kia, đơn thuần chỉ là tổng hợp lại các bước tuần tự từ khi bắt đầu bắt tay vào học cho đến khi 'xả tâm lý' sau khi thi xong. Nói chung xem quyển sách này mới thấy tâm lý nặng thi cử ở Việt Nam (có cả đoán trước đề và hiểu cách chấm điểm của giáo viên nữa) tác động đến cả cách định hướng và hướng dẫn chuẩn bị thi cử của những người viết sách.
3
520793
2015-08-07 13:06:36
--------------------------
243739
8657
Khi mua về mình cảm thấy sự lựa chọn của mình là đúng. khi mua về nhìn thấy quyển sách này được bọc lại rất bất mắt. và khi mở ra thì mình cảm thấy rất hài lòng về chất lượng màu sắc của từng trang sách cũng như bìa sách. và một phần quan trọng không thể không nhắc đến đó chính là nội dung. nội dung được nhiều tác giả biên soạn rất hay và ý nghĩa. phải nói quyển sách này đã giúp cho tôi rút ra được những kinh nghiệm và những bài học quý giá trong cuộc sống hằng ngày của bản thân mình. thật lòng mà nói đây không phải là lựa chọn sai của tôi khi quyết định mua nó.
5
485605
2015-07-27 17:41:19
--------------------------
240607
8657
Mình mua cuốn sách này lúc gần ngày thi, thật sự thì cũng hơi khó hiểu chút nhưng cũng giúp ích cho mình. Cuốn sách dạy chúng ta các kĩ năng ôn luyện cũng như thi cử, nhờ vậy mà mình tự tin để đi thi hơn, nếu bạn nào muốn thay đổi hay học thêm các phương pháp học thì chọn cuốn sách này rất thích hợp, cuốn sách nhỏ gọn có thể bỏ túi và mang đi bất cứ đâu. Chất lượng giấy khá ổn, bìa cũng khá đẹp. Thật sự mình rất hài lòng về cuốn sách này.
4
477877
2015-07-24 21:06:27
--------------------------
211124
8657
Theo mình, cuốn sách khá hữu ích. Nó chứa đầy đủ nội dung để vượt qua kỳ thi một cách hoàn hảo.  Khi đọc hết quyển sách này, mình đã đúc kết ra được nhiều kinh nghiệm quý báu trong việc thi cử và mình cảm thấy tự tin hơn để bước vào những kì thi sắp tới. . Cách viết logic, dễ hiểu. Trình này bìa khá ngộ nghĩnh. Chất lượng giấy in khá tốt tuy nhiên màu giấy hơi tối. Và theo mình nên có thêm bookmark để thuận tiện khi đọc. 
Mình nghĩ đây là cuốn sách nên đọc .
4
386757
2015-06-20 09:19:49
--------------------------
204457
8657
Cuốn '' Vượt qua kỳ thi một cách hoàn hảo" chứa đựng tất cả những kỹ năng cần thiết để hoàn thành tốt một bài thi. Khi đọc hết quyển sách này, mình đã đúc kết ra được nhiều kinh nghiệm quý báu trong việc thi cử và mình cảm thấy tự tin hơn để bước vào những kì thi sắp tới.
Bìa sách có hình ảnh rất ngộ nghĩnh, đáng yêu, bắt mắt, chất lượng giấy in rất tốt, nội dung được trình bày rất rõ ràng, dễ hiểu và dễ nhớ, giá cả như vậy là vừa phải.
Mình nghĩ mua cuốn sách này là một sự lựa chọn đúng đắn.
5
603877
2015-06-04 07:53:45
--------------------------
196008
8657
Quyển sách này chỉ đơn thuần tóm tắt lại những phương pháp trong những quyển sách rất hay như Để luôn đạt điểm 10, Học khôn ngoan mà không gian nan vv (đều do Alphabooks ấn hành). Nên mình nghĩ nếu các bạn đã từng đọc qua nhiều sách chia sẻ về các kỹ năng học tập thì không cần đọc quyển này. Khi đọc nó, mình chỉ cần đọc câu đầu và câu cuối là có thể đoán ra phần nào nội dung này đã được đề cập trong quyển nào mà mình đã từng đọc trước đây. Nên cần cân nhắc khi chọn mua.
3
178374
2015-05-15 06:24:06
--------------------------
159960
8657
Quyển sách là tất cả những kỹ năng cần thiết để hoàn thành tốt một bài kiểm tra. Sau khi đọc xong nó, mình đã rút ra nhiều kinh nghiệm hơn trong việc thi cử và góp phần nâng cao điểm số của bản thân. Bìa sách khá dễ thương, giấy in tốt, nội dung được trình bày rất rõ ràng, đặc sắc. Dù là kỳ thi nào đi nữa thì mình tin là nó sẽ thích hợp với mọi người, đặc biệt là đối với những bạn chuẩn bị bước vào những kỳ thi quan trọng như mình. Điểm trừ duy nhất là sách không có tặng bookcard nên hơn bất tiện trong quá trình đọc.
5
492104
2015-02-20 22:01:54
--------------------------
