447024
10248
Nội dung của cuốn sách này cực hay, cực hấp dẫn và lôi cuốn. Nêu như trong "Tôi là số Bốn" chỉ nói về cuốc sống lẩn trốn của số Bốn và trận chiến tàn khốc của cậu với kẻ thù mà không nói đến biệt năng đặc biệt nhiều, thì ở "Sức mạnh của số Sáu" phần biệt năng đặc biệt cùng với những cuộc chiến chống lại sự rượt đuổi của kẻ thù được khắc họa nhiều hơn. Những bí ẩn về những chiếc hộp mà mỗi người giữ cũng được bật mí, xuất hiện thêm ba thành viên mới với những biệt năng ấn tượng: số Bảy, số Chín, số Mười. Và mình nào rất háo hức để được đọc phần tiếp theo của loạt truyện này.
5
703243
2016-06-13 12:22:30
--------------------------
416397
10248
Mình xin đánh giá sơ qua về sách như sau : về hình thức bên ngoài, Sức mạnh của số sáu được bọc ni lông bên ngoài trông rất mới, bìa không được đẹp cho lắm :( truyện được dịch rất chuyên nghiệp và dễ hiểu. Về nội dung, truyện có nhiều tình tiết hành động vô cùng lôi cuốn và hấp dẫn, các nhân vật trong truyện để lại cho mình nhiều ấn tượng rất sâu sắc và đặc biệt nhất là ' cô gái số sáu ' - nhân vật chính của truyện, tuy truyện rất hay nhưng nếu so sánh với phần trước - Tôi là số 4 thì chắc ko hay bằng :v 
5
1262273
2016-04-14 23:49:07
--------------------------
161006
10248
Sức mạnh của số sáu là phần tiếp theo trong series tiểu thuyết giả tưởng, nếu ai đã đọc qua tập trước tôi là số bốn thì chắc chắn không thể bỏ qua phần tiếp theo này. Nhân vật chính lần này là cô nàng số sáu xinh đẹp, tài giỏi và mạnh mẽ và cũng là người khiến trái tim hai anh chàng số bốn và Sam bị rung rinh. Đây là một tập truyện đầy những pha hành động, rượt đuổi đến nghẹt thở, người đọc không thể rời mắt khỏi trang sách. Kết thúc truyện mở khiến người đọc tò mò chờ đợi xem ở tập tiếp theo sẽ có thêm những người bạn mới đặc biệt nào.
5
313795
2015-02-26 12:20:18
--------------------------
155833
10248
Sau khi hội ngộ với số sáu , cả hai Số Bốn và Số Sáu cùng nhau tiếp tục hành trình tìm kiếm và hội tụ cùng các thành viên khác. Sam là bạn đồng hành mới của hai người. Cả hai đã khám phá ra sức mạnh và tìm được thêm biệt năng mới. Cả hai chàng trai trẻ đều có những rung động ngây thơ với cô bản Số Sáu gợi cảm, tài năng của mình. Tác giả rất thành công trong cách miêu tả các cuộc tấn công của bọn Mogadore và sự phản công của các bạn trẻ. Những lúc ấy đọc mà mình như nghẹt thở, không đặt sách suốt được, đọc hết một lượt không ngơi nghỉ. Quá hấp dẫn. Chờ đợi tập tiếp theo
4
120193
2015-02-02 11:43:39
--------------------------
141175
10248
Cô nàng Số Sáu siêu giỏi! Xa Cepan của mình từ sớm, cô gái mạnh mẽ biết tự lập, tự luyện tập và phát triển biệt năng của mình, ở tập mới, cô nàng này còn khiến hai anh chàng Sam và Số Bốn lép về hoàn toàn về sức mạnh! 
Những cuộc chiến bắt đầu, mọi chi tiết truyện trở nên dồn dập, gấp gáp hơn, dưới ngòi bút của tác giả và khả năng dịch thuật khá tốt của dịch giả (phải nói vậy vì không phải truyện nào cũng được dịch ra hợp lý và hấp dẫn như thế này :)) ) thì sự cuốn hút của mạch truyện lại càng được nhân lên. 
Ở tập này, bên cạnh những cuộc chiến bất ngờ và mới mẻ, kịch tính giữa Số Bốn, Số Sáu và Sam với bọn Mogadore, còn có những rắc rối mới nảy sinh khi hai anh chàng số Bốn và Sam hình như "trót" cảm nắng người bạn đồng hành đầy cá tính và sức mạnh của mình :)) 
Kết thúc gợi  cảm giác chờ đợi, không biết ở tập sau, không biết những người bạn mới sẽ còn có biệt năng đặc biệt và cá tính quái dị nào nữa :))
4
310296
2014-12-14 10:44:07
--------------------------
113190
10248
Tập 1 "Tôi là số 4" có vẻ hơi nhàm chán, thiên về kể lể về tiểu sử của nhân vật chính, thì "Sức mạnh của số sáu" đã mang đến một cục diện hoàn toàn khác. Trận chiến nảy lửa bắt đầu thật sự, những viên đạn, những đao kiếm, những quái vật, những vết thương dày đặc, và đặc biệt là sự xuất hiện của Số 7, số 9 và số 10. Ba nhân vật mới với biệt năng siêu phàm của họ chắc chắn sẽ kích thích tận cùng trí tưởng tượng của bạn với những pha hành động hấp dẫn đến nghẹt thở. Họ đã mạnh lên, kẻ thù cũng đông thêm và cẩn thận thêm. Xen kẽ những phép màu của Biệt Phẩm, Biệt Năng, Garde, Cepan, ta vẫn đọc được những dòng văn ngọt ngào miêu tả rung động của số 4, số 6 và Sam. Đặc biệt, liệu lúc này Sarah còn chung thuỷ và tin tưởng vào Số 4? Liệu việc số 4 bị FBI bắt giữ có liên quan tới cô? Phần tiếp tuyệt vời này hoàn toàn đáng đọc!
5
113650
2014-05-27 17:25:43
--------------------------
84770
10248
“ Sức mạnh của số sáu” đã cho tôi biết rằng mỗi con người đều có những sức mạnh riêng chỉ thuộc về bạn và căn bản là bạn sẽ khơi dậy nó thế nào. Một câu chuyện sẽ khiến bạn như xem một bộ phim hành động với những pha rượt đuổi và tẩu thoát lôi cuốn đến nghẹt thở. Mình cũng có nhận xét như bạn Yến Nhi là chuyện được kể theo hai mạch nên nhiều lúc không được đồng nhất và thiếu đi tính liên kết cảm giác chúng cứ rời rạc và chắp vá kiểu gì ý nhưng quả thật thì đó chỉ là khiếm khuyết nhỏ thôi chuyện quả thật rất hấp dẫn
4
97873
2013-07-02 08:48:22
--------------------------
39043
10248
Sau khi đọc xong mình phải công nhận là Số Sáu quả là một cô nàng sexy, ngổ ngáo, mạnh mẽ và cũng rất dịu dàng. Câu chuyện tình của John và "Maren Elizabeth" là điểm sáng trong cuốn sách này. Mình thấy hứng thú và bị lôi cuốn bởi diễn biến tâm trạng của John khi ở bên cô bạn đồng hành của anh. Điểm sáng thứ 2 chính là những pha hành động lôi cuốn, pha tẩu thoát nghẹt thở. Đấy là ưu điểm còn khuyết điểm thì nhiều vô số. Tác giả kể câu chuyện theo 2 mạch: John và Marina. Thực ra thì sự kết nối giữa 2 nhân vật hay mạch truyện chỉ là những chiếc Hộp chứa đựng Biệt phẩm. Còn nhìn nhận theo một cách khách quan thì câu chuyện khá rời rạc, có nhiều đoạn kể của Marina làm mình thấy nhàm chán. Tuy có khuyết điểm nhưng có thể nói rằng sự lôi cuốn của Sức mạnh của Số Sáu hoàn toàn lật đổ đế chế Tôi là Số Bốn rồi! Mình rất mong chờ một bộ phim bom tấn với cô nàng Số Sáu yêu thích của mình!
3
16882
2012-09-07 23:21:27
--------------------------
24473
10248
1,2,3,4,5,6,7,8,9 những con số chúng ta thường nhìn thấy mỗi ngày . Nhưng trong tác phẩm này thì nó lại mạng một ý nghĩa khác.
Đó là số thứ tự của chín đứa trẻ mang sức mạnh phi thường, đang bị truy sát theo thứ tự đó. Và 1,2,3 đã chết, nhưng không vì thế mà các đứa trẻ khác bỏ cuộc, họ sẽ phải hành động để cứu lấy bản thân.
Có sức mạnh không là chưa đủ, cần trí tuệ , sự đoàn kết, cố gắng hết mình để được sống với mọi người mà mình yêu mến.
Truyện rất hay , từ truyện rút ra được nhiều bài học hữu ít về sức mạnh của bản thân. Và đây là quyển thứ 2 trong seri mà mình chờ khá lâu mới ra.
 
5
35521
2012-04-12 09:34:11
--------------------------
20805
10248
"Tôi là số 4" là 1 chuyển thể điện ảnh vô cùng thành công của bộ sách cùng tên, nếu bạn đã ghiền phim, không thể nào bỏ qua truyện, và nếu đã đọc xong tập 1, chắc chắn bạn phải nhanh tay đặt ngay cuốn 2. Số phận của những vị anh hùng đến từ hành tinh khác sẽ như thế nào? Những vị anh hùng còn lại có biệt tài gì? Thiện hay bị biến chất theo phe ác? Chỉ có thể chờ đợi tập 2 mà thôi.

Đọc truyện, tôi cứ có cảm giác như được quay về thời ấu thơ, được sống với thế giới "Những anh em siêu nhân", lúc nào cũng mơ được trở thành 1 siêu nhân,... nhưng bộ sách này hay hơn hẳn ở chỗ tác giả đã khéo léo lồng ghép thế giới hiện thực vào, và đậm tính nhân bản!
4
23636
2012-03-05 20:00:06
--------------------------
