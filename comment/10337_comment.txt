472443
10337
Đọc Miko mình như tìm về ngày xưa. Một cảm giác rất con nít, chỉ cần người khác làm tốt với mình chút xíu là thấy người ta là người siêu tốt bụng, Miko đã cảm thấy ông ở tiệm sách không cho cô bé coi cọp như vậy. Miko có dịp về quê thăm bà nội và cảm thấy mọi thứ đều mới mẻ đối với một cô bé từ thành phố đến, cô bé trở nên thân thiết với người em họ mà lúc đầu đã cảm thấy mình phiền phức. Trong mẩu truyện khi cả hai cùng đến cổng đền đã bị phá hủy một nửa khi ở Nagasaki, mình cảm thấy rất giàu tính nhân văn, một cô bé chỉ mới học lớp 5 nhưng rất hiều chuyện, dù không có nhiều tiền nhưng cô bé đã mua hoa để cúng 4 cô gái trẻ là nạn nhân trong cuộc chiến tranh ấy
5
80823
2016-07-09 16:24:39
--------------------------
463100
10337
Hồi cắm trại, cứ rảnh rảnh là mượn của nhỏ bạn đọc. Bình thường mình không hay đọc nhiều truyện tranh Nhật Bản cho lắm, nhưng cuốn này thực sự rất đáng để đọc.
Nội dung vô cùng trong sáng, hồn nhiên, xoay quanh cuộc sống của một cô bé lớp 5 nhưng không nhàm chán. Truyện có nhiều điểm vui, hài ước nhưng cũng không kém phần cảm động lấy đi nước mắt của độc giả. 
Truyện cũng làm cho người ta nhớ lại về một thời thơ ấu- khoảng thời gian đẹp vô cùng khi nhìn thấy sự hồn nhiên vô tư của nhóc Miko.
5
975584
2016-06-29 07:49:25
--------------------------
462991
10337
Vì trong bộ truyện Miko mình thiếu mất mấy tập đầu :< mà trên tiki lại đang bán tập 2 thế là vội vàng bếch em nó về. Mấy tập đầu chả hiểu sao mình thấy tác giả Ono Eriko vẽ Miko ngộ ngộ hay hay. Kiểu tóc cứ vểnh hết cả lên :v phần điều ước của Miko rất là ý nghĩa này nên mình cực thích phần này luôn :< đọc phần 1 mà mình thấy quý ông bán sách dễ sợ luôn :3 tập này cái mặt bà Miko nó buồn cười dễ sợ. Đeo cái túi tiền mà mặt cứ ngược lên trời.n
Nói chung là rất hay
4
1286291
2016-06-28 23:53:12
--------------------------
457752
10337
" Nhóc Miko : cô bé nhí nhảnh - Tập 2 " là quyển duy nhất trong series Nhóc Miko có bìa màu đỏ tươi nên mình cảm thấy khá là thích cover lần này : màu sắc tươi sáng,bắt mắt,hình ảnh tranh sinh động lại đáng yêu.Ngay từ câu truyện đầu tiên của tập 2 mình đã cảm thấy thích thú rồi vì sao nó giống mình quá trời >_<  sau đó là đến phần " Điều ước của miko" cực kì cảm động,đọc bao nhiêu lần vẫn thấy xúc động ( dù tranh vẽ đơn giản nhưng ý nghĩa lớn lắm đó nhé).
5
1419914
2016-06-24 15:38:38
--------------------------
447811
10337
Mình rất thích đọc truyện Miko và Miko rất dễ thương, tinh nghịch. các bạn của Miko rất đáng yêu, đặc biệt là Tappie cậu bạn lúc nào cũng bị ghép đôi với Miko. Gia đình của Miko cũng rất thú vị, bố mẹ và em trai Mamoru ai cũng đáng yêu, có những tính cách riêng, mang đến cho truyện những tình huống cười vỡ bụng. Truyện vẽ rất đẹp, giấy in của Nhà xuất bản Trẻ tốt. Tiếc là truyện này xuất bản lâu rồi nên bìa gập, nếu truyện bìa rời thì còn gì bằng. Miko đáng được 5 sao!!
5
400930
2016-06-14 21:30:51
--------------------------
439239
10337
Truyện rất thích hợp cho trẻ lớp 5 trở lên, người lớn đọc cũng rất thích, thấy nhớ thời thơ ấu với những chuyện ngộ nghĩnh đáng yêu của trẻ con.. đặc biệt là mỗi truyện đều có 1 bài học đạo đức nho nhỏ về cách sống, cách cư xử hướng trẻ em đến những điều tốt đẹp, lương thiện. Tranh vẽ đậm phong cách truyện tranh Nhật bản từ lâu đã được thiếu nhi Việt Nam rất yêu thích: biểu cảm dễ thương, gần gũi, sinh động. Truyện dành cho mọi lứa tuổi. Có thể mua làm quà tặng cho các bạn nhỏ cũng rất ý nghĩa
4
648948
2016-05-30 23:12:33
--------------------------
436319
10337
Tập này hay thiệt đó ^^. Tụi miko mới học lớp 5 thôi mà đã học giáo dục giới tính kìa :3, chả bù cho mình ngày xưa, mai sau này lên cấp 2 mới học. Mà tập này thấy tội miko ghê, thích người ta còn bày đặt nhường :3, xong rồi lại buồn!!! Tự dưng đọc tập nayg thấy ghét Miho quá xá ( tội nghiệp Miho ), ai biểu cứ tranh tappei của Miko cơ!! Yosiki ga lăng thiệt ^^ mới lớp 5 thôi đã hiểu ra chân lý yêu là chỉ cần làm người mình yêu hạnh phúc :3. Mong ONo ra Miko nhiều nhiều để đọc nữa ^^
5
648608
2016-05-26 16:02:17
--------------------------
425656
10337
Tập truyện Miko này làm tui khóc quá trời lun đó, vì có câu chuyện Miko đi Nagasaki và hồi tưởng lại mấy đứa trẻ bị chết trong quả bom nguyên tử mà Mĩ thả xuống,  còn phần tác giả đi chụp ảnh phần cổng ngôi đền về nhà thì có cảm giác như bị ám rụng rợn quá trời.  Tập này vui nhưng hơi buồn,  câu chuyện vẫn vậy,  xoay quanh cuộc sống gia đình,  thầy cô,  bạn bè của Miko nhà ta,  rồi lần Miko về nhà bà Nội, rồi chuyện về chiếc áo khoác của mẹ,vân vân và ...
Hay thật!
4
1085299
2016-05-05 09:59:27
--------------------------
360523
10337
Miko _ cô bé nhí nhảnh tập 2 là một trong những miko mà mình rất thích Đọc xong tập 2 xen lẫn những mẩu truyện vui đó là những mẩu truyện làm mình suýt nữa khóc như "điều ước của Miko" khi cô bé đến Nagasaki chơi thấy cái cổng di tích còn lại của chiến tranh Bìa đẹp vẽ ổn nội dung hay Ôi, nhóc Miko, càng đọc càng ghiền mà không thấy chán. Cô bé Miko sao mà dễ thương và nhân hậu quá đi!  Tình cảm gia đình , trường lớp , bạn bè tới chuyện tình cảm người lớn được thể hiện qua những câu chuyện vô cùng gần gũi xoay quanh nhân vật chính là một cô nhóc bình thường , trong sáng , nhí nhảnh , yêu đời và tính cách lúc thì vô cùng trẻ con lúc lại suy nghĩ như một bà cụ non Nội dung truyện có tập rất cảm động " miko special 
5
1065005
2015-12-29 12:29:40
--------------------------
321330
10337
Miko _ cô bé nhí nhảnh tập 2 là một trong những miko mà mình rất thích . Trong này gồm có những tập " Cấm coi cọp , bí mật của con gái , trò chơi thử thách lòng can đảm , kỷ niệm ở oita ... , điều ước của miko , ba chú heo con , lên tivi , lời nói dối trắng trợn , miko special trở về nhà ,. 
Nội dung truyện có tập rất cảm động " miko special " . Truyện có vài trang bị rách làm mình không thích xíu nào . Nhưng mà mình vẫn hài lòng với cuốn truyện này nhìu nhìu . Truyện vẽ không đẹp lắm , nhưng vui nhộn và sinh động , có tính giải trí cao . 
4
635786
2015-10-13 18:08:58
--------------------------
294580
10337
Đọc xong tập 2 xen lẫn những mẩu truyện vui đó là những mẩu truyện làm mình suýt nữa khóc như "điều ước của Miko" khi cô bé đến Nagasaki chơi thấy cái cổng di tích còn lại của chiến tranh... đoạn cuối truyện có viết 'đừng để trẻ con trên thế gian phải chịu đựng điều này thêm một lần nào nữa '..hay câu truyện 'miko special trở về nhà ' thực sự rất cảm động làm mình gợi nhiều suy nghĩ hơn .Quyển sách thực sự rất hay và đáng để đọc, chờ để đọc nhưng tập tiếp theo.
5
165748
2015-09-09 14:49:53
--------------------------
289147
10337
Có một sự thật là  mình rất thích tập này . Lí do vì sao ư ? Trong các quyển mình mua đây là tập có nội dung thú vị ,  hài hước , vô cùng gần gũi nữa . Mình thích nhất là chương đầu là nói về việc coi cọp của Miko . Khi đọc đến đoạn này tự dưng mình thấy sao mà giống mình thế . Các mẩu truyện khác cũng hay và cuốn truyện này cũng đáng để mua lắm đấy . Bìa đẹp vẽ ổn nội dung hay . Quá là tuyệt vời luôn .
5
726610
2015-09-04 10:12:11
--------------------------
257761
10337
Mình đọc nhó Miko từ cái hồi hàng ngày phải ra quán thuê truyện để thuê, khi trên tiki có mình quyết đinh mua luôn. Phải nói đây là một bộ truyện dễ thương kinh khủng. Cho dù cốt truyện chì xoay quanh cuộc sống hàng ngày của cô bé Miko nhưng lại không hề nhàm chán, ngược lại lúc nào cũng hồn nhiên, vui tươi và gây được nhiều tiếng cười cho người đọc. Lúc nhỏ đọc vì thấy Miko giống mình quá, bây giờ thì đọc để tìm lại tuổi thơ. Lúc nào Miko cũng khiến mình cảm thấy vui vẻ và yêu đời hơn.
4
138880
2015-08-07 22:42:24
--------------------------
254455
10337
Mấy hôm trước mua hộ nhỏ em mình , rảnh không có gì làm mở ra đọc rồi mê luôn .Tình cảm gia đình , trường lớp , bạn bè tới chuyện  tình cảm người lớn được thể hiện qua những câu chuyện vô cùng gần gũi xoay quanh nhân vật chính là một cô nhóc bình thường , trong sáng , nhí nhảnh , yêu đời và tính cách lúc thì vô cùng trẻ con lúc lại suy nghĩ như một bà cụ non . Đọc Miko , mình như tìm lại được một phần tuổi thơ đã mất .
5
617834
2015-08-05 11:13:17
--------------------------
229362
10337
Nếu ai mà không được đọc Miiko trong cuộc đời chắc là đã mất đi một phần tuổi thơ rồi :< mình tí thì mất, gần mãn teen thì Miko đến với mình :v nhưng ai đọc muộn cũng không cần phải lo, vì thật sự Miiko phù hợp với tất cả độ tuổi. Miko luôn nhí nhảnh, yêu đời, lười biếng và không thật sự hứng thú với học tập, nhưng lại có ý chí quyết tâm rất lớn và rất biết suy nghĩ, yêu thương gia đình. một cô bé con lúc thì thấy quá trẻ con so với tuổi, lúc lại thấy vài phần trưởng thành hơn, khiến Miiko trở nên đa chiều và gợi nhiều suy nghĩ hơn. Quyển sách này rất hay. :3
5
364412
2015-07-16 19:37:35
--------------------------
168060
10337
khi cầm cuốn chuyện này trên tay mình thấy rất thích vì sau mấy ngày mưa chờ đợi ship hàng. Mình thực sự bị ấn tượng với tính cách của cô bé Miko trong chuyện. Trong sáng vô cùng. Kiểu như được quay lại về với tuổi thơ vậy. Bây giờ là sinh viên đại học nhưng thực sự khi đọc truyện này mình thấy rất thích. Có thể nói nó là nguồn giải trí giúp mình giảm bớt căng thẳng. Chuyện chỉ đơn giản là những tình tiết rất quen thuộc về những đứa trẻ nhưng cũng cho người đọc thật nhiều suy ngẫm.
5
199477
2015-03-15 21:23:47
--------------------------
165125
10337
Không phải câu chuyện về một cô bé siêu nhân hay một cô bé có năng lực phi thường, Miko chỉ đơn giản xoay quanh câu chuyện về một cô bé hết - sức - bình thường như bao cô cậu học sinh trên Trái Đất này với những buồn vui, giận hờn. Nhưng chính điều đó lại làm nên nét hấp dẫn của Miko với những câu chuyện về gia đình, tình bạn hết sức đáng yêu mà theo tôi chưa có bộ truyện tranh nào có thể làm được như vậy. Đến nỗi câu chuyện lắm lúc khiến tôi ghen tị với Miko vì có những người bạn và một gia đình tuyệt với đến như vậy!
4
78605
2015-03-09 19:54:32
--------------------------
150389
10337
Miko mình biết đến khá muộn nhưng thật sự bị nghiền từ lúc nào không hay luôn. Miko dễ thương, nghịch ngợm, hồn nhiên, mập mập đáng yêu lắm. Truyện dành cho thiếu nhi nên tính giáo dục được mình đánh giá cao. Những bài học qua những câu chuyện đời thường vụn vặt xoay quanh Miko bỗng nhiên trở nên thú vị và sâu sắc. Thực sự đây là một cách giáo dục trẻ rất tốt vì trẻ em thích đọc truyện tranh lắm mà! Từ gia đình, trường lớp, bạn bè,....tới chuyện tình củm siêu cấp dễ thương của Miko với Tappei làm mình đổ liêu xiêu. Theo mình thì khổ truyện hơi nhỏ một chút nhưng không sao, bìa đẹp, màu sắc tươi sáng mà nét vẽ của tác giả dễ thương vô đối. ♥ Miko luôn rồi.
5
123769
2015-01-16 16:29:08
--------------------------
144761
10337
Ôi, nhóc Miko, càng đọc càng ghiền mà không thấy chán. Cô bé Miko sao mà dễ thương và nhân hậu quá đi! Mặc dù là truyện tranh dành cho thiếu thi nhưng "Nhóc Miko" có tính giáo dục rất cao. Mình tuy lớn rồi nhưng khi đọc Miko vẫn nhận được nhiều bài học ý nghĩa và sâu sắc. Những câu chuyện tình cảm "con nít" giữa Miko và Tappei cũng rất đáng yêu ( mình thích đôi này nhất á ). Chất liệu giấy xốp làm cho cuốn truyện trở nên khá nhẹ. Nét vẽ chibi của tác giả thì dễ thương thôi rồi. Yêu Miko quá đi thôi ♥
5
178929
2014-12-27 22:52:10
--------------------------
130739
10337
Nhóc Miko là bộ truyện tranh thiếu nhi cực gần gũi và có tính giáo dục, mình vui mình đã mua được những tập truyện dễ thương và bổ ích cho em mình. 
Ngoài những tình tiết thú vị, nét vẽ dễ thương thì nhóc Miko còn mang lại những bài học nhỏ về cuộc sống cho trẻ em tiểu học.
(May là truyện in theo kiểu cũ, dở đúng không phải dở ngược, em mình thích dở đúng, vì Doremon bản mới dở ngược mà nó không thích đọc :( ).
5
49203
2014-10-19 19:39:32
--------------------------
119752
10337
Mình tình cờ đọc truyện Miko khi nhỏ em mua về. mình thật sự bị lôi cuốn từ những truyện đầu tiên. Nét vẽ của tác giả rất dễ thương, nhất là những tình huống hài hước thì hình ảnh làm mình cười không ngớt được :) Nội dung thì rất dễ thương, về chuyện bạn bè, gia đình, lại còn mang tính giáo dục rất cao. Mình rất vui vì có được 1 bộ truyện hay, dễ thương mà ý nghĩa như vậy cho trẻ con, mà người lớn cũng bị lôi cuốn, có thể đọc để giải trí rất phù hợp
5
83159
2014-08-07 11:24:10
--------------------------
71468
10337
Mình thật sự rất ấn tượng về cuốn truyện này. Một cô bé Miko nhí nhảnh đáng yêu và nghịch ngợm đã trở thành điểm nhấn cho bộ truyện. Tình cảm gia đình, tình cảm bạn bè được hiện lên rất rõ nét làm cho người đọc cảm nhận được sự ấm áp thông qua những câu chuyện ngắn của Miko. Thành công của bộ truyện là những tình tiết hài hước của Miko và những nhân vật khác. Trong một cuốn truyện có những phần truyện ngắn riêng biệt nhưng lại rất liên kết với nhau khiến người đọc không thể đặt cuốn truyện xuống. Nhóc Miko thật sự là một bộ truyện tranh rất lôi cuốn và tôi rất thích!!!
5
93778
2013-04-27 10:42:57
--------------------------
43052
10337
mình đọc cái này ở nhà em họ mình mà bây giờ mình "nghiện" luôn rồi đó!!! câu chuyện rất là hài nhưng có có những phần cũng rất cảm động> cậu nhóc Mamoru và Miko luôn luôn cãi vã nhưng có những lúc lại rất hòa hợp, đầy tình yêu thương , che chở. Đặc biệt trong câu truyện này có 2 nhân vật mà mình thích nhất là Tappel và Miko. 2 người này có tình cảm với nhau nhưng  lại luôn luôn cãi nhau để che dấu cảm xúc của mình hoặc họ đúng là như vậy. nói chung tổng thể câu chuyện ngay từ tập đầu đã có ấn tượng sâu sắc với người đọc, ko thể phai mờ
5
62619
2012-10-17 18:28:40
--------------------------
29615
10337
Đây là cuốn Miko đầu tiên mà mình mua, lúc đó mình mới học cấp 1 thôi ^^. Đọc xong cuốn 2 là mình tức tốc tìm mua lại cuốn 1 liền bởi vì bộ truyện này quá sức dễ thương, đáng yêu, ngôn ngữ không quá trau chuốt, trái lại rất gần gũi, hóm hỉnh, vui tươi,... 
"Nhóc Miko" xoay quanh cuộc sống hằng ngày của cô bé Miko nhí nhảnh, tinh nghịch cùng với bạn bè, gia đình...
Câu chuyện còn có lúc đan xen vào những cảm tình, những rung động trong sáng, nhẹ nhàng của cô bé..... Tác giả đôi khi còn thêm thắt phần ngoại truyện đôi khi hơi "kinh dị" một tí làm chho câu chuyện thêm phần sinh động và hấp dẫn hơn :x...
Đọc cuốn truyện này mà mình cảm thấy yêu đời hơn rất nhiều, cũng biết cách ứng xử thật tốt với bạn bè xung quanh....
Dù cho đã là học sinh cấp 3 rồi :"> nhưng tuần nào mình cũng phải rinh 1 cuốn Miko về nhà cho bằng được.... Yêu quá đi mất thôi! Các bạn cũng đọc thử đi nhé! Ghiền luôn đấy! ^^
5
40122
2012-06-07 13:07:57
--------------------------
19340
10337
 Những ai thích thú với những tình huống hài hước vui nhộn cùng nét vẽ dễ thương không thể bỏ qua truyện '' Nhóc Miko''. ONO Eriko -tác giả của truyện tranh nói trên (cùng tác giả với truyện ''Cô Nhóc Tinh Nghịch '' chắc hẳn không lạ lẫm gì với bạn trẻ thời nay. Cốt truyện cực vui nhộn. Nhân vật trong truyện không thể hiện trực tiếp cho nhau nhưng họ thực sự rất yêu thương và luôn bảo vệ nhau. Mối quan hệ gia đình luôn khăng khít và vui vẻ (dù vẫn hay cãi vã). Truyện không chỉ dành cho lứa tuổ thiếu nhi mà phù hợp với tất cả những ai vẫn còn ngồi trên ghế nhà trường. Một khi cầm trên tay cuốn truyện này, bạn sẽ không thể nào bỏ xuống nếu chưa đọc hết bởi cốt truyện cực kì hấp dẫn. Các tình tiết trong mỗi phần của truyện đều liên quan chặt chẽ với nhau khiến cho ta đã đọc trang này là muốn đọc tiếp trang sau, trang sau nữa. Tạo hứng thú cho người đọc và không gây nhàm chán chính là đặc điểm truyện của ONO Eriko. Bạn sẽ có những phút giây thư giãn thật sự khi cầm trên tay cuốn truyện này.
5
23564
2012-02-15 13:14:59
--------------------------
