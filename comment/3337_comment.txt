485660
3337
Mình rất ngưỡng mộ gia đình bé Nhật Nam kg vì bé là thần đồng mà vì tình cảm gia đình này thật ấm áp yêu thương. Những lời văn kể về những kỷ niệm của Nam và gia đình chị Điệp dù rất giản dị nhưng luôn mang cho ta cảm giác thân quen gần gũi và ấm áp. Tác giả còn hướng dẫn rất cụ thể cách dạy giỗ con trên tinh thần vui học để con hào hứng khám phá mọi điều trong cuộc sống mà kg phải theo kiểu "ép cung" nặng nề làm con sợ hãi việc học. Cảm ơn những tình cảm chân thành mộc mạc mà vô cùng ấm áp của chị dành cho các độc giả. 
3
1114461
2016-10-05 10:06:24
--------------------------
469264
3337
Mình đã theo dõi facebook của chị Phan Thị Hồ Điệp từ lâu và nhận thấy rõ ràng rằng chị là một người mẹ tuyệt vời,một người bạn lớn sâu sắc nhưng không kém phần tinh tế,hóm hỉnh của cậu bé thần đồng Đỗ Nhật Nam. Do vậy khi nhìn thấy cuốn sách này mình đã không do dự mà nhấn nút mua luôn.Sách viết theo kiểu tản văn,nhật ký.không có trình tự thời gian nhất định,mỗi câu chuyện đều xoay quanh một việc thường ngày nào đó của 2 mẹ con chị Điệp và bé Nam,nhưng với người đã và đang làm mẹ như mình,đó lại là những câu chuyện khơi gợi cảm hứng tuyệt vời! Mình có thể tìm và lọc được trong đó rất nhiều thông tin,cách nói chuyện,nuôi dưỡng tâm hồn con phong phú và hữu ích
5
657859
2016-07-06 10:51:59
--------------------------
461872
3337
Sách của cô Điệp viết rất hay, đây là 1 cuốn sách hữu ích bổ sung vào giá sách của phụ huynh để dạy con cái. Mình rất thích cuốn sách này. Tuy nhiên, điểm trừ duy nhất là cách bố cục chữ sắp xếp ở mỗi trang sách theo dạng hình tròn. Có lẽ mình không thích cách bài trí này cho lắm. Mình thích kiểu bài trí theo dạng cổ điển hơn. Nếu giữa các chương có trang trí thêm hình ảnh màu, minh họa dễ thương thì mình nghĩ sách còn hấp dẫn hơn nữa.
Nhưng dù sao sách khá hay. Cảm ơn cô Điệp đã có cuốn sách hay như thế. Tiki chuyển hàng nhanh, đóng gói cẩn thận, mình rất ưng.

4
836377
2016-06-28 08:32:30
--------------------------
456397
3337
Nhận sách từ Tiki. Mình lại tìm đến các mẫu chuyện nhẹ nhàng...để cảm thấy mọi thứ thật nhẹ nhàng!
Hiếm có quyển nào lôi cuốn mình như cuốn này. Bằng chứng là mình cố gắng làm nhanh công việc để đọc sách, đi đâu cũng muốn nhanh về nhà để xem tiếp chị Điệp kể chuyện gì. Thậm chí mình đã cắt giấc ngủ trưa từ 30-45 phút còn 15 phút hoặc "hy sinh" luôn giấc ngủ trưa. Tối về con nài nỉ anh yêu "anh chơi với con nhé! Ưu tiên e đọc hết sách ...hihi"
Mặc dù đã đọc nhiều bài ở facebook của chị nhưng đọc lại thấy thấm hơn nhiều.
Mình không có ý định đọc 1 lần, sẽ đọc thêm 1 vài lần nữa...
Thank chị tác giả đã chia sẻ các câu chuyện vô cùng nhẹ nhàng mà đầy ý nghĩa.
5
850880
2016-06-23 13:56:45
--------------------------
447784
3337
Khổ sách to ngang, in giấy dày dặn, bìa trơn nhẵn, sờ rất thích. Cá nhân mình thấy thì nhìn cuốn sách ở trên tay đẹp hơn trong ảnh rất nhiều. Sách là tập hợp những kinh nghiệm, ghi chép của một bà mẹ về cách nuôi dạy con, cách dõi theo con từng bước lớn lên. Không chỉ đưa ra những kinh nghiệm quý giá cho các bà mẹ khác, cuốn sách còn ghi dấu ấn trong lòng người đọc bởi sự truyền tải nhẹ nhàng, sâu sắc về tình yêu thương vô bờ của những bà mẹ đối với con trẻ.
4
815063
2016-06-14 21:09:41
--------------------------
436501
3337
Tôi mua sách đã khá lâu nhưng "để quên" chưa đọc. Tôi mãi mê đọc sách dạy con kiểu này kiểu khác. Nhưng khi đọc sách này, Tôi đọc vèo hết quyển sách. Nếu bạn muốn một quyển sách chỉ rõ cách dạy con nên thế nào, không nên thế nào thì không nên mua sách. Sách gồm những mẫu chuyện nhỏ, tất cả đều thấm đượm tình yêu. Tôi hiểu vì sao con của chị lại là một cậu bé tình cảm đến vậy. Đọc sách và cảm nhận tình yêu của một người mẹ, đơn giản, nhẹ nhàng nhưng sâu rộng vô biên.
5
322000
2016-05-26 20:23:35
--------------------------
435700
3337
Mình luôn theo dõi face của cô Điệp và rất ngưỡng mộ cách nuôi dạy con của cô, cũng như ngưỡng mộ cái gia đình nhỏ bé nhưng đầy ắp yêu thương ấy. Nuôi con với nhiều người như cuộc chiến vậy mà thấy với cô nó nhẹ nhàng và ngọt ngào thế. Mình nghĩ đấy cũng là một trong những lí do làm nên một Đỗ Nhật Nam. Quyển sách rất có ích cho những ông bố bà mẹ muốn những đứa con biết sống yêu thương. Từ ngày biết TiKi mình rất tin tưởng mua sách tại đây, chiết khấu cao, giao hàng nhanh và bọc gói rất cẩn thận. 
5
559809
2016-05-25 16:14:50
--------------------------
385366
3337
Cuốn sách là những câu chuyện hết sức nhẹ nhàng, dung dị về tình yêu của tác giả đối với từng sự vật sự việc và con người. Từng trang sách, từng câu chuyện làm lay động trái tim tôi, đọc nó mà nghe tim thổn thức vì những tình cảm tuyệt vời tràn đầy trong cuốn sách. Đọc xong cuốn sách thật sự khiến tôi thêm yêu thương gia đình, yêu thương cuộc sống, yêu thương con người và yêu thương chính bản thân tôi. Cám ơn chị Hồ Điệp, một con người bình dị, một bà mẹ tuyệt vời. Cảm ơn tiki vì sự nhanh chóng trong việc xử lý đơn hàng, đóng gói cẩn thận, và những ưu đãi dành cho mặt hàng sách. Chúc tiki luôn dẫn đầu và thành công hơn nữa trong mọi lĩnh vực. Hy vọng sẽ có thêm những khuyến mại lớn dành cho sách để nhẽng người không có điều kiện nhiều có thể mua được sách tốt hơn.
4
373267
2016-02-23 22:00:16
--------------------------
362522
3337
tôi luôn hâm mộ chú bé thần đồng Đỗ Nhật Nam và tự hỏi làm thế nào để nuôi dưỡng một thiên tài như thế ngay từ lúc nhỏ? Vì vậy mà tôi tìm đến cuốn Yêu thương mẹ kể này, ở đây, tôi bắt gặp được những dòng văn vô cùng giản dị, không phải là 1 cuốn cẩm nang hướng dẫn nuôi dạy con theo phương pháp mà là những sẻ chia ngọt ngào, tràn đầy ấm áp của tình thân của mẹ, tôi nghĩ, có lẽ chính vì sự thấu đáo, hiểu và yêu thương của của chị Điệp mà Đỗ Nhật Nam mới có điều kiện phát triển về sức lực lẫn tinh thần như thế. Chị đúng là tấm gương cho các bà mẹ cùng học hỏi để có nuôi dạy con thật tốt. 
5
547523
2016-01-02 09:14:17
--------------------------
354665
3337
Một cuốn sách chan chứa tình yêu thương mà mẹ dành cho con. Đây là một cuốn sách rất nên đọc, từ một câu chuyện nhỏ cho một ý nghĩa lớn! Mình
học được nhiều bài học giản dị mà bổ ích, thiết thực từ nhưng câu chuyện của chị Điệp! 
Cả cuốn sách là những dòng tâm sự, những mẫu chuyện nhỏ của tác giả và người thân trong cuộc sống, qua những câu chuyện mình thấy toát lên một tình yêu rất nhẹ nhàng, rất ngọt ngào và ấm áp, thân thương mà gần gũi. 
Tiki giao hàng nhanh, sách bọc rất đẹp!

5
821288
2015-12-18 12:48:26
--------------------------
331144
3337
Sau khi dành 4 tiếng liên tục đọc xong sách của anh Thảo, mình bắt đầu đọc quyển sách này. Nhưng mình chỉ đọc xong lời giới thiệu rồi thôi. Mình quyết định để ngày hôm sau đọc cho nó liên tục vì mình sợ đã quá muộn rồi đọc ko thể dứt ra đc. Và đúng như vậy, hôm sau mình mất 3 tiếng để đọc hết cuốn sách với nhiều cung bậc cảm xúc khác nhau, khi thì cười như đứa trẻ, khi thì phải bỏ sách xuống để chạy vào nhà vệ sinh giống như e Nam vậy. Đọc xong sách của 2 anh chị e thấy rằng đây là câu trả lời đầy đủ nhất cho câu hỏi: Tại sao e Nam lại giỏi như vậy ?
E xin đóng góp 1 ý nữa với anh chị cái tên Men của e Nam, Men ở đây chắc còn có nghĩa là MEN RƯỢU nữa. Vì thế mà anh Thảo ko uống rượu bia mà vẫn say bên người tình Nam, vì thế mà chị suốt ngày say sưa bên Nam với những trò chơi rất tuyệt với.
Cám ơn anh chị, chúc ca mổ của anh Thảo thành công tốt đẹp. Chúc cho gia đình chị luôn AN
5
893827
2015-11-04 00:09:25
--------------------------
325506
3337
Làm mẹ không phải dễ, vậy mà đọc sách Yêu thương mẹ kể của tác giả, lại thấy tác giả rất chu toàn trong việc nuôi nấng con cái và bảo bọc chúng trong tình yêu thương rất tinh tế, có thể đã hiểu được phần nào vì sao một đứa trẻ thiên tài phát triển rất nhanh nhạy trong thời đại vẫn còn có những nét bé thơ của độ tuổi thật, thì ra, có một người mẹ tuyệt vời như chị Điệp thực sự là một may mắn trong đời của Nhật  Nam! Đây là 1 quyển sách hay về cách nuôi dạy con trưởng thành, ko khuôn sáo hoa mỹ, mà chỉ là những dòng chia sẻ từ tận tâm can của người mẹ! 
5
516261
2015-10-23 18:20:10
--------------------------
324849
3337
Chị Điệp là người mẹ tuyệt! Mình đang có 2 cháu nhỏ. Một cháu 3,5 tuổi, một cháu 5 tuổi. Mình đang mất phương hướng trong việc dạy con cái cụ thể như chúng biếng ăn, hay tranh giành, cãi cọ, mè nheo... nhức hết cả đầu. Mình đã biết đến và đọc cuốn sách của Chị Điệp thấy rất hay và rất có ích đặc biệt là giúp mình có thêm động lực để bắt đầu lại trong việc nuôi dạy con theo một hướng khác. Yêu thương là không có đòn doi, yêu thương là không có quát mắng, yêu thương là hướng con đến những tri thức để con có hành trang vững bước trong cuộc đời. Cảm ơn Chị, cảm ơn Tiki.
5
882736
2015-10-22 09:12:22
--------------------------
304119
3337
Chờ mãi mới có thể mua được cuốn này.^^. Nội dung sách thì như tiêu đề rồi đó, rất là hay, cuốn sách nói lên những tâm tư tình cảm của một người mẹ dành cho người con được thể hiện một cách vừa có phần dí dỏm vừa ngọt ngào khôn tả. Những chuyện trong sách đôi khi không chỉ đơn giản là 1 câu chuyện mà từ đó người đọc còn có thể học hỏi về cách dạy dỗ con cái, cách đối xử với cha mẹ, ...
Ưu điểm: Truyện trình bày rất dễ thương, dễ đọc. Hình bìa đẹp.
Khuyết điểm: Mình thấy sách có giấy hơi mỏng một chút, vì là khổ ngang mà giấy mỏng, bìa mềm thì nhiều người đọc sẽ phải rất cẩn thận khi bảo quản sách.
5
96853
2015-09-16 10:44:01
--------------------------
301281
3337
Người mẹ này thật dể thương, cách thể hiện của chị cứ như là bạn của con trai bé nhỏ chứ không phải là hình ảnh của một người mẹ cao siêu nào khác... Vậy mà thấm đẫm và chan chứa tình thương yêu... Chị dạy cho mình cách làm một người mẹ như một người bạn với những đứa trẻ con dễ thương ngày nay... Và thật sự mình không ngạc nhiên khi chị và chồng chị lại có thể dạy dỗ Nhật Nam để Nam giỏi và tình cảm chứa chan như thế. Yêu gia đình nhỏ này nhiều lắm, và thực sự mình học được rất nhiều qua những cuốn sách của Nam và của cả gia đình em. Cám ơn em 
5
593166
2015-09-14 16:06:19
--------------------------
292599
3337
Cuốn sách này thực sự viết ra để dành cho những người phụ nữ đã đang và sắp làm mẹ! Mình mua cuốn sách hôm thứ 7, ngay ngày chủ nhật đã hoàn thành. Đọc xong mình cảm thấy thật sự khâm phục tình yêu thương vô bờ bến của người mẹ Hồ Điệp dành cho con trai của mình. Để trở thành một thần đồng Đỗ Nhật Nam như ngày hôm nay, ngoài những khả năng thiên bẩm thì bé Nam đã rất may mắn khi có được người mẹ như chị Điệp. Chị dạy dỗ dìu dắt con ngay từ những năm tháng đầu đời, kiên trì nhẫn lại, ko đặt nặng những kỳ vọng của người lớn lên con trẻ. Chính điều đó đã thể hiện tình mẫu tử thiêng liêng vô tư trong sáng và chưa bao giờ cần đáp lại của người mẹ! Cảm ơn chị Điệp, chúc gia đình bé Nam luôn mạnh khỏe hạnh phúc!
5
469674
2015-09-07 14:38:49
--------------------------
289466
3337
Chị ấy là 1 phụ nữ giỏi. những câu chuyện chị viết rất hay, rất dễ thực hiện, mình sẽ cố gắng ghi nhớ tất cả và sẽ truyền đạt lại cho con mình sau này, sách là những bài học chị đã đúc kết để viết ra cho mọi người cùng đọc, cùng tìm hiểu và cùng thực hiện để lớp trẻ mai sau toàn là những người tài năng hy vọng chị sẽ viết thêm nhiều sách để giúp những người mẹ như chúng em còn bỡ ngỡ trong cách dạy con để học tập. Đay là quyển sách về cách dạy con, yêu thương con hay nhất mà e từng đọc
5
396128
2015-09-04 15:07:00
--------------------------
286928
3337
Con đã đọc những chia sẻ của cô trên facebook. Cô là một người mẹ tuyệt vời. Cô yêu Nam bằng một tình yêu rất đặc biệt, cô làm bạn với Nam, nuôi dưỡng ước mơ của Nam. Quyển sách này thật sự rất hữu ích cho những ai đã, đang và sẽ làm mẹ. Không phải là sự giáo huấn mà trong đấy chỉ chất chứa tình yêu vô bờ của mẹ dành cho đứa con bé bỏng nhưng cũng là những bài học đắt giá. Mỗi đứa trẻ sinh ra có một sứ mệnh riêng, có những người bạn đồng hành là bố mẹ, bố mẹ sẽ là người bạn định hướng chúng đi đúng đường!
4
790661
2015-09-02 08:45:26
--------------------------
282379
3337
Quyển sách này do một người bạn nhờ mình mua hộ. Qua đọc quyển sách này, mình thấy rằng để nuôi dạy con thành thần đồng ngoài tố chất của đứa trẻ còn cần một công trình với nhiều tâm huyết và công sức của người mẹ dành cho con. Với tình yêu thương dạt dào, tác giả đã lan truyền được tình yêu thương và cung cấp những kinh nghiệm bổ ích giúp mỗi người mẹ có thể làm bạn với con.
Đôi lúc trên những trang văn, có những câu chuyện khiến mình bật khóc, đặc biệt là câu chuyện về tình yêu thương không đúng cách. Một cuốn tản văn đáng để dành thời gian đọc.
4
736925
2015-08-29 09:12:35
--------------------------
274111
3337
Vô tình rảnh rỗi, được đọc cuốn sách này ở bàn làm việc của chị đồng nghiệp, mình chưa có gia đình, nhưng ngay khi đọc được những trang đầu tiên, mình đã muốn có nó. Đó là tâm tư của một bà mẹ vô cùng yêu thương con, chị là người bạn đồng hành của con trong suốt hành trình phát triển của bé Nhật Nam. Mình vừa đọc vừa nhớ đến mẹ, đến gia đình và tuổi thơ của mình. Có lẽ bà mẹ nào cũng có một tấm lòng yêu thương và hy sinh tất cả vì con như thế. 
Mình tin rằng khi ai đọc cuốn này cũng sẽ khóc, cũng sẽ nhận ra được nhiều phương pháp để giáo dục cho con cái. 
thật là muốn sau này khi mình có con cũng sẽ viết nhật ký lại như thế này, để ghi lại cảm xúc, và cũng để con mình sau này đọc được sẽ hiểu được nỗi lòng cha mẹ! <3
Cảm ơn chị Phan Thị Hồ Điệp, cảm ơn nhà xuất bản đã đưa đến cho người đọc một cuốn sách tuyệt vời như thế! 
5
457640
2015-08-21 17:35:57
--------------------------
266200
3337
Cảm ơn mẹ Phan Thị Hồ Điệp đã chia sẻ tình cảm và phương pháp nuôi dạy bé Nhật Nam. Mình nghĩ đây là cuốn sách gối đầu giường của những bà mẹ trẻ mong muốn có những đứa con biết sống yêu thương, sống có trách nhiệm và tình cảm với gia đình, bản thân! Quan sát cuộc sống xung quanh, tôi thấy hiện nay, một số gia đình trẻ thật sự chưa chú trọng, quan tâm đến việc giáo dục cho trẻ, nhất là trẻ từ 0 – 3 tuổi. Hầu hết là để các em bé đó tự chơi với đống đồ chơi lớn trước mặt, còn cha, mẹ thì lo làm việc riêng của mình. Chủ yếu trông coi không để bé gặp nguy hiểm như: tiếp xúc với nước sôi, lửa, điện...  Như vậy sẽ bỏ phí một khoảng thời gian mà cha mẹ có thể định hướng, giáo dục nhân cách, xu hướng phát triển của con mình. Mà giai đoạn này là rất dễ giáo dục vì các em chưa tiếp xúc nhiều với thế giới bên ngoài, dễ vâng lời và tiếp thu nhanh.
5
739185
2015-08-14 14:38:39
--------------------------
237350
3337
Tôi từng lục tìm trên google tất cả những thông tin về cách nuôi dạy con của bố mẹ thần đồng Đỗ Nhật Nam. Và nay tôi đã được thỏa mãn vì sở hữu cuốn sách này. Mặc dù chỉ là những ghi chép theo kiểu tản văn, nhật ký nhưng người đọc có thể tìm thấy rất nhiều thông tin bổ ích. Và đương nhiên, giọng văn bình dị tràn ngập yêu thương mà hấp dẫn đến lạ,xao xuyến đến nao lòng. Cảm ơn mẹ Điệp rất nhiều bởi chị không chỉ đã nuôi dạy một "thần đồng" về tri thức mà còn là một người Mẹ truyền cho con đầy ắp yêu thương và tâm hồn nhân hậu! Bạn hãy đọc sách đi, tôi chắc bạn sẽ khóc vì hạnh phúc!
5
331353
2015-07-22 13:44:45
--------------------------
236062
3337
Thông qua cuốn sách này, ngoài việc hiểu được tình thương, cảm xúc, tâm tư của một người mẹ dành cho con, mình còn rút ra được cách ứng xử văn minh hơn dành cho con trẻ, đặc biệt trong việc dạy dỗ em trai của mình. Với em út, mình học được cách tiếp cận như 1 người bạn, người lắng nghe và đặt mình vào em. 
Cuốn sách cũng giúp vun đắp thêm tình cảm gia đình. Trẻ nhỏ đọc, nghe sẽ hiểu hơn về tình thương yêu dạt dào của người mẹ, dù mẹ mình có bày tỏ được như cô Điệp hay không.
4
325527
2015-07-21 15:17:19
--------------------------
233592
3337
Mình đã ngưỡng mộ chị Phan Thị Hồ Điệp từ lâu và thường xuyên theo dõi các bài viết mới của chị trên Facebook. Đọc sách của chị viết mình càng hiểu rõ hơn phần nào vì sao Nhật Nam lại trở thành một cậu bé thần đồng như vậy- vì bố mẹ của cậu cũng thật tuyệt :). Những trang sách tràn ngập tình yêu thương và mang đến những  phương pháp giáo dục rất hiện đại: Làm bạn với con, cùng học với con, viết văn vui vẻ, cùng khám phá thế giới, dạy con biết yêu thương, chia sẻ kiến thức, tự đi không lo lắng...Những câu chữ chân thật mà gần gũi, vừa mộc mạc vừa cuốn hút tạo nên một cuốn sách thật tuyệt và rất nhiều điều đáng học tập.

5
648692
2015-07-19 19:40:43
--------------------------
233456
3337
Chị Điệp là một tấm gương tuyệt vời cho không chỉ tất cả những Người Mẹ trên trái đất này mà còn cho tất cả những người được gọi là phụ nữ nói chung về công - dung - ngôn - hạnh. Riêng về dạy Con, Chị là một người thầy tuyệt vời hơn tất cả. Em vẫn thường vào fb của Chị để học lỏm, để theo dõi những cảm xúc, những đối thoại của Mẹ và Con, để cảm thông và chia sẻ những niềm vui bên cạnh bạn Nam. Hai Mẹ con như hai người bạn thân thiết, bên nhau trên mọi nẻo đường. Những gì Chị chia sẻ về dạy Con thật quý báu và (dĩ nhiên) rất đáng để học tập. Những cảm xúc của Chị được chia sẻ thông qua những bài thơ, những mẫu chuyện cũng là tài sản vô giá cho mình. Cảm ơn Chị, người Mẹ tuyệt vời!
5
91621
2015-07-19 17:26:49
--------------------------
226095
3337
Mình đã đọc những bài viết của chị Phan thị Hồ Điệp trên face book, nhưng vẫn mua yêu thương mẹ kể để đọc lại, để trên giá sách để tự nhắc nhở chính mình, dù không thể làm 1 người mẹ tuyệt vời như chị Hồ Điệp nhưng luôn cố gắng để kiên trì, nhẫn nại, lắng nghe và hiểu con hơn nữa. Để làm được như chị Hồ Điệp quả thật là quá khó, phải vừa có hiểu biết, tình yêu, thời gian, kiên trì....Nhật Nam quả thật là rất xuất sắc, nhưng nếu nói cậu ý là thần đồng thì không phải, cậu ấy là kết quả của 1 quá trình nuôi dậy tốt!
4
5269
2015-07-11 14:00:09
--------------------------
223031
3337
Đúng như cái tên, cuốn sách này là những Yêu thương mẹ kể. Từ những hoài niệm thuở ấu thơ với anh trai, chị gái, bố mẹ... đến những chuyện mới chuyện cũ với gia đình, với con. Mình nhìn thấy bóng dáng Nhật Nam qua những câu văn của chị, đúng là con mẹ Điệp không sai. Con được nuôi dưỡng tâm hồn bằng những yêu thương, những thơ mộng trong tâm hồn mẹ, nên Nam cũng nhạy cảm, quan tâm và tình cảm y như mẹ Điệp vậy. Nhẹ nhàng và giản di, lúc nào cũng dịu dàng ấm áp như thế với con, tình yêu của mẹ như cái nôi êm ái , một nơi bình yên cho con để trở về, để con thêm tự tin bước vào thế giới rộng lớn ngoài kia.
5
82774
2015-07-06 14:53:05
--------------------------
222351
3337
Khi biết facebook của cô Phan Hồ Điệp, tôi đã thức tới 3 giờ sáng để đọc trọn vẹn những dòng thủ thỉ của cô rồi ngồi khóc ngon lành. Hóa ra, đó là tình mẹ vô bờ. Hóa ra đó là tình cảm gia đình vẹn tròn ấm áp. Hóa ra, giữa cuộc sống bộn bề bon chen nhiều khi đến mức nghiệt ngã này, có "những" tình yêu  trong sáng, tinh khôi đến nhường ấy. Những con chữ như dòng chảy suối nhẹ nhàng, róc  rách chảy, như những hạt mưa phùn âm ấm đầu xuân . Chúng thì thầm những câu chuyện, những lời tâm tình, chúng chở nặng những nỗi nhớ thương, nỗi khắc khoải, tình yêu thương giữa những người trong gia đình. 
Những ai từng tò mò, những năm tháng tuổi thơ của Nhật Nam và cách nuôi dạy Nam thì có lẽ sẽ tìm được câu trả lời trong cuốn sách này. Cuốn sách gồm nhiều câu chuyện nhỏ, về cuộc sống gia đình, về cách mẹ Hồ Điệp " cùng con yêu Tiếng Việt" ...
Tôi đã từng dành cả ngày lưu tất cả các bài viết của cô Hồ Điệp,  nhưng khi cầm cuốn sách này trên tay, đó vẫn là một cảm giác rất tuyệt  vời! 

5
23924
2015-07-05 13:16:44
--------------------------
