466805
3688
Cũng giống như tập 1, Chúa tể những chiếc nhẫn tập 2 vẫn đi theo format cũ, dài dòng, lê thê. Nhưng lần này câu chuyện đã có những chi tiết mới, gợi ra nhiều sự tò mò, hào hứng hơn cho tôi. Ngoài ra, trong tập này, những trận chiến bắt đầu xuất hiện, đó chính là điều khiến tôi thích thú với tập này. Tập 2 khép lại đã tạo ra cho tôi một chút tò mò, hứng khởi hơn để tìm hiểu những gì xảy ra tiếp theo, điều gì sẽ xảy đến với Frodo, Sam và cả đoàn hộ nhẫn. Hi vọng ngọn lửa nhỏ nhoi ấy không bị dập tắt đi ở tập cuối.
3
307132
2016-07-02 22:23:04
--------------------------
461286
3688
Về phần bìa, bìa rời và có phần gáy mà khi ghép lại cả 3 cuốn sẽ cho ra một biểu tượng, nói chung là rất đẹp, nhưng theo mình thấy thì so với lần tái bản trước, hình minh họa bìa không đẹp bằng và không được hấp dẫn cho lắm. Nếu không vì Chúa Tể Những Chiếc Nhẫn đã trở nên quá nổi tiếng thì khi chọn sách mình đã bỏ qua cuốn sách này rồi, vì bìa cũng là một yếu tố quan trọng để thu hút độc giả ngay cái nhìn đầu tiên mà. Còn về nội dung thì không phải bàn cãi nữa rồi, truyện đầy hấp dẫn, lôi cuốn với những pha hành động đầy nghẹt thở, hồi hộp tuy có hơi dài dòng quá mức ở phần miêu tả. Nói chung, đây vẫn là một cuốn sách đáng mua.
4
776787
2016-06-27 18:14:41
--------------------------
408599
3688
Bộ ba"The Lords of The Rings" của nhà văn J.J.R. Tolkien đã nhận được quá nhiều lời khen ngợi từ mọi đối tượng ở khắp nơi trên thế giới. Có lẽ nó chẳng thêm một lời giới thiệu, một câu nhận xét nào nữa để có được sự chú ý của độc giả. Tuy nhiên, ở Việt Nam, tôi thấy nhiều người quan tâm đến chất lượng bản dịch và quyển sách như thế nào, có xứng đáng để bỏ tiền ra mua hay không. Và sau khi đọc, tôi có thể nói rằng các dịch giả và Nhà xuất bản Văn học, công ty Nhã Nam đã làm khá tốt trong việc truyền tải tinh thần một kiệt tác thế giới.
Bên cạnh đó, tiki làm việc rất tốt, giao hàng khá nhanh và chu đáo. bạn nào đang phân vân thì mua ngay đi nhé, đảm bảo không hối hận.
PS: Mong tiki cập nhật thêm quyển 1 "The Fellowship of The RIng".
5
322004
2016-03-31 22:20:42
--------------------------
375076
3688
Là phần tiếp theo của tập 1 - Đoàn hộ nhẫn, phần 2 này có kết cấu khá lạ, phần mở và phần kết của nó rất đột ngột, nếu không đọc phần 1 chắc hẳn người đọc khó lòng theo dõi và hiểu được mạch truyện. Nội dung truyện tạo cho mình cảm giác hồi hộp và mơ hồ về các nhân vật trong truyện. Mình rất ấn tượng với Ents - những cái cây có khả năng nhìn, nghe, nói và di chuyển, đặc biệt là thủ lĩnh người cây Treebeard. Đọc truyện và vẽ ra trong đầu một thế giới tưởng tượng quả thật rất thú vị.
4
662390
2016-01-27 19:01:46
--------------------------
352946
3688
Ba phần của chúa nhẫn, mỗi phần mang một đặc tính khác nhau. Nếu phần một mang đến cho người ta một cảm giác của sự rắn rỏi, nghệ thuật mang đậm những đánh dấu về sự tồn tại và công trình của các Elf thì phần hai này diễn ra trong không khí của những cuộc chia ly, những màn rượt đuổi nghẹt thở trong bóng tối cùng với việc hé lộ những phần quan trọng trong âm mưu hiểm độc nhằm thống trị Middle Earth của Sauron. Nó có nhiều điểm tương đồng với bộ The Hobbit và là sự nối tiếp đầy ấn tượng báo trước cuộc trở về của nhà vua.

Tuy nhiên, tôi vẫn không thể cho cuốn sách này 5 điểm vì lý do... đóng gói, rất cảm ơn tiki vì đã xử lý đơn hàng của tôi một cách nhanh chóng. Nhưng thực tình tôi không thể chấp nhận nổi việc dùng bìa plastic bọc thẳng bên ngoài dust jacket như vậy. Việc làm này đã khiến bìa chống bụi bị gãy nhiều chỗ ở mép trước và sau khiến tôi phải tốn nhiều thời gian để khắc phục. Đồng thời, tôi cũng đã phải bỏ bookcare đi để bọc lại sách một lần nữa trước khi gác lên kệ. Hy vọng các bạn sẽ để ý hơn vào lần bọc sách tiếp theo.
4
455391
2015-12-15 12:05:52
--------------------------
324736
3688
Hai Tòa Tháp đọc hay, lối nói văn vẻ tạo sự thích thú, do phân ra từng tập nên những đoạn tả nhiên nhiên con người hoa lá cành dài quá, mà lại cứ dịch hết tên riêng ra tiếng Việt nên mới đọc khó hiểu, Vì truyện thì đã nhiều thuật ngữ sinh vật rồi mà để nguyên thì chỉ lướt qua thôi còn dịch ra thì thấy hơi kỳ, có phần gây khó chịu vì thuộc tên gốc hơn. Bìa đẹp nhưng mỏng quá, dễ rách, quăn, rộp nước, giấy bên trong cũng dễ ngấm nước nữa. Nhưng nếu giữ gìn cẩn thận thì cũng được.
5
679335
2015-10-21 20:42:15
--------------------------
306719
3688
Hai Tòa Tháp là quyển thứ hai trong bộ Chúa Tể Những Chiếc Nhẫn. Hình thức thì tương đối đẹp nhưng hình như không đẹp bằng lần tái bản trước của Nhã Nam. Hôm bữa rửa mặt xong, cầm quyển này lên đọc thì ướt sũng, để khô thì bị rộp lên, thiệt là buồn. Nội dung lôi cuốn, hơi hài hước, dí dỏm, nhưng có phần cũng dài lê thê, không hiểu rõ nội dung cho lắm. Cũng như quyển đầu tiên, quyển này vẫn gặp vấn đề về dịch thuật những tên riêng, làm người đọc như mình rất bực mình. Hi vọng dịch giả sẽ chú ý hơn cho những lần dịch sau.
4
198858
2015-09-17 19:11:30
--------------------------
303864
3688
Tiếp nối phần một của loạt truyện Chúa tể những chiếc nhẫn, phần hai - Hai Toà Tháp - cũng hấp dẫn không kém. Mình mê quá đọc một mạch cho hết truyện gần như quên ăn quên ngủ! Câu chuyện li kì hấp dẫn, cách dẫn dắt lôi cuốn, mở ra một thế giới sống động và hoành tráng. Dịch thuật rất ổn, nhưng chỉ một điều là dịch tên nhân vật và địa danh theo phiên âm Tiếng Việt đã gây khó chịu cho khá nhiều người đọc. Nhưng dù sao với nội dung hấp dẫn như vậy, đây là một quyển sách không thể chối từ. Sách đẹp, giấy cũng đẹp và nhẹ, nhưng bìa hơi xấu so với đợt truyện trước của Nhã Nam.
4
118307
2015-09-16 00:41:42
--------------------------
301629
3688
Về hình thức thì sách có bìa khá đẹp, tuy nhiên giấy thì là kiểu giấy xốp, bột và rất dễ thấm nước, hay đụng vào thì cũng rất dễ rách. Dịch thuật thì tương đối ổn, nhưng đưa cách dịch tên địa danh và vật hay người như kiểu phiên âm ra tiếng Việt như này hơi kỳ. Ở bên Harry Potter cũng có như vầy nhưng nhìn chung là vẫn dễ hiểu và ăn rơ được với cách dịch giọng miền Nam của Lý Lan, còn trong quyển này mình nghĩ nên để nguyên bản thì hơn. Vì truyện thì đã nhiều thuật ngữ sinh vật rồi mà để nguyên thì chỉ lướt qua thôi còn dịch ra thì thấy hơi kỳ.
3
91802
2015-09-14 19:35:30
--------------------------
