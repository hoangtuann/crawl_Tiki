324532
9294
Tony Buzan quá nổi tiếng với sơ đồ tư duy, mình đã rất thích thú với những sơ đồ tư duy nhiều màu sắc của ông đã giúp mình trong việc ghi nhớ và tổng hợp kiến thức trong học tập nên mình đã tìm đến các đầu sách của ông, “Trí tuệ sáng tạo”, ai cũng biết sáng tạo là điều rất quang trọng trong của sống nó quyết định sự thành công của một con người trong công việc, Tony Buzan phần nào đã làm sáng tỏ những tư duy sáng tạo qua các kinh nghiệm của người đi trước ngoài ra ông còn nêu ra một số cách để cải thiện khả năng sáng tạo của bộ não
5
145118
2015-10-21 13:50:50
--------------------------
323049
9294
Tự nhận mình là một người thiếu sáng tạo và chỉ biết làm theo khuôn khổ nên em gặp khá nhiều khó khăn trong học tập và trong đời sống khi em rất kém trong khả năng ứng biến linh hoạt. Chính vì lí do đó nên em mua cuốn sách này và quả thực, nó không uổng tiền một chút nào. Trong sách có những cách hướng dẫn cũng như khá nhiều bài tập ứng dụng, thực hành rất thú vị. Đồng thời, tác giả còn cung cấp cho chúng ta cách lập ra một "Sơ đồ tư duy" như một công cụ để tăng khả năng làm việc sáng tạo của bản thân.
5
647002
2015-10-17 18:14:35
--------------------------
308859
9294
Mình biết tác giả Tony Buzan thông qua cuốn sách “ Tôi tài giỏi bạn cũng thế “ của tác giả Adam Khoo. Những chia sẻ về Sơ Đồ Tư Duy làm mình thấy thật sự thích thú.Từ đó mình đã tìm hiểu về tác giả Tony Buzan và thật sự ngưỡng mộ ông về những cuốn sách mà ông đã viết về não bộ của con người trong đó có cuốn “ Sức mạnh của trí tuệ sáng tạo”.Trong cuộc sống hiện nay khi IQ không còn là thước đo về độ thành công của con người nữa thì chỉ số sáng tạo CQ lại là thước đo độ thành công trong cuộc sống hiện nay.Cuốn sách chứa những phương pháp và những bài tập để rèn luyện chỉ số CQ mà ai trong chúng ta đều cũng có thể thực hiện được!
5
483530
2015-09-18 21:20:28
--------------------------
303457
9294
Bạn bị bó buộc trong khuôn mẫu và thói quen. Đôi khi bạn đánh giá thấp năng lực của bạn thân. Bạn luôn nghĩ rằng mình không thể làm được một việc gì đó. Tất cả đều chỉ là 1 cái bẫy để nhốt tâm trí của bạn lại. Hãy mở nó ra và luôn tâm niệm bạn sẽ làm được, vậy thì bạn sẽ làm được. Đây là điều mà tôi học được từ quyển sách này. Có lẽ cần thời gian để thẩm thấu và chiêm nghiệm hết những điều trong sách, nhưng có thể nói đây là 1 cuốn sách tuyệt vời và rất đáng để đọc.
5
724204
2015-09-15 21:04:05
--------------------------
295606
9294
Những quyển sách của Tony Buzan rất đáng xem. Quyển sách giới thiệu cho tôi những cách tuyệt vời để phát triển sự sáng tạo của mình, giúp cho tôi có những trải nghiệm thú vị, rèn luyện cho tôi kỹ năng suy nghĩ ra những điều thú vị về thế giới xung quanh một cách nhanh chóng. Giúp cho tôi tự tin hơn về bản thân, không còn cảm giác tự ti, mặc cảm. Quyển sách cho tôi biết rằng không ai trong chúng ta sinh ra mà không có được một sức mạnh tiềm tàng trong cơ thể. Đây là một quyển sách bổ ích, thú vị. Tôi thật sự rất yêu quyển sách này.
5
644674
2015-09-10 14:47:50
--------------------------
290720
9294
Sự sáng tạo là một yếu tố rất cần thiết trong công việc và mình luôn nghĩ đó là bẩm sinh có được. Nhưng Tony Buzan đã chỉ ra rằng mình đã sai hoàn toàn, với những dẫn chứng cụ thể đã giúp mình nhận ra điều ấy là sai lầm. Nó là mình thay đổi suy nghĩ luôn bó hẹp lâu nay của mình. Quyển sách sẽ chỉ ra những phương pháp cũng như bài luyện tập để bạn phát triển kỹ năng sáng tạo của mình cũng như phát triển tư duy. Cuốn sách rất đáng đọc với những ai muốn hoàn thiện bản thân của mỉnh và muốn phát triển trí tuệ, tư duy.
5
74657
2015-09-05 18:13:49
--------------------------
258249
9294
Quyển sách này đã thực sự đánh thức những tiềm năng trong tôi. Có những kỹ năng mình nghĩ rằng sẽ không bao giờ giỏi được nhưng quyển sách đã chỉ cho mình làm như nào để cải thiện và phát triển nó. Những kỹ năng ấy rất tốt và bổ ích trong công việc cũng như đời sống hằng ngày. Bạn sẽ bất ngờ khi áp dụng chúng vào những lĩnh vực khác và phát hiện rằng sự sáng tạo đã thay đổi cuộc sống của chúng ta một cách tuyệt vời như thế nào.
5
468726
2015-08-08 12:48:21
--------------------------
243281
9294
Quyển sách này hoàn toàn có khả năng làm thay đổi suy nghĩ của một người. Bằng những dẫn chứng cụ thể từ kết quả nghiên cứu của mình và những câu chuyện có thật từ các nhà thiên tài nổi tiếng mọi thời đại, Tony Buzan đã đưa người đọc trải nghiệm và khám phá ra những điều hoàn toàn mới. Hơn nữa, ông còn giúp người đọc tin tưởng hoàn toàn vào nhận định đó bằng cách đưa ra những bài tập vô cùng đơn giản nhưng mang lại kết quả không ngờ. Vì vậy theo mình, quyển sách này rất đáng để mọi người thêm vào tủ sách tại nhà. Điểm trừ duy nhất, theo mình, là không có lời mở đầu hay giới thiệu về tác giả của quyển sách, còn lại thì rất tuyệt vời.
4
651055
2015-07-27 12:53:36
--------------------------
155596
9294
Chắc ai cũng biết Tony Buzan là một chuyên gia hàng đầu thế giới về khả năng  tư duy của não bộ, sơ đồ tư duy và các phương pháp tư duy sáng tạo.Và quyển sách nhỏ này của ông đã chứng minh điều đó. Qua nội dung chia sẻ trong quyển sách, tác giả đã chứng minh rằng tư duy sáng tạo không của riêng ai và ai cũng có khả năng tư duy sáng tạo bằng cách đưa ra những bằng chứng cực kỳ thuyết phục, nêu ra cách học hót của loài chim, sự rèn luyện của các nhạc sĩ, họa sĩ thiên tài trong lịch sử, đồng thời chỉ rõ khả năng tư duy sáng tạo của chúng ta đã bị mai một do cách học tập khi còn bé .Tác giả đã cung cấp những kiến thức và các bài tập nhằm đánh thức khả năng tư duy sáng tạo trong mỗi con người. Theo nhận xét chủ quan của mình, tôi thấy đây là một quyển sách cực kỳ hữu ích  .
5
387632
2015-02-01 14:21:38
--------------------------
114203
9294
Mình chỉ mua cuốn sách này ở nhà sách một cách tình cờ. nhưng sau khi đọc xong nó, mình cảm thấy bản thân học thêm được rất nhiều điều. Cuốn sách này giúp mình khám phá ra được những khả năng tiềm ẩn bên trong mình: khả năng ghi nớ, khả năng sáng tạo ra những điều mới mẻ... Mình đã ứng dụng khá nhiều điều học được trong sách này vào thực tế, có những phương pháp mang lại kết qảu tốt, có nhiều phương pháp thì ko mang lại kết quả gì. Nhưng quan trọng nhất, sau khi đọc xong cuốn sách này, mình cảm thấy tư tin vào bản thân hơn rất nhiều, không còn mặc cảm và tự ti như trước nữa. Đối với riêng cá nhân mình, mình cảm thấy đây là một cuốn sách rất đáng đọc và rất bổ ích!
4
138880
2014-06-10 10:19:53
--------------------------
68467
9294
Tony Buzan là một tác giả mà mình cực kì yêu thích, các tác phẩm của ông như: sơ đồ tư duy, Sử dụng trí tuệ của bạn,...làm mình thật sự thích thú. Đây là một quyển sách mới nữa của Buzan mà mình đọc, tác giả đã biết kết hợp các năng lực bản thân của mỗi người để giúp họ khơi dậy khả năng sáng tạo. Tác giả cũng đã đưa ra các giải thích, các lập luận vì sao sự sáng tạo lại quan trọng dường nào, mình thật sự rất thích các lập luận này của ông, rất chặt chẽ. Đây là một cuốn sách thích hợp cho giới trẻ như chúng ta để có thể gợi mở thế giới khám phá thêm những điều mới lạ, một cuốn sách thật sự bổ ích.
3
60663
2013-04-11 22:59:19
--------------------------
65405
9294
Cụm từ "trí tuệ sáng tạo" đã để lại rất nhiều ấn tượng cho bản thân mình. Đúng là chỉ khi con người biết kết hợp những hiểu biết và tri thức với sự sáng tạo mới có thể tạo nên những thành công đột phá cho bản thân. Điều này cũng đã được chỉ ra trong cuốn sác. Bằng những dẫn chứng cụ thể, cùng những phân tích tài tình, tác giả đem đến một cái nhìn mới cho người đọc, thuyết phục họ bằng những trang viết đầy sức lôi cuốn. Tác giả đã chỉ ra tầm quan trọng của sự sáng tạo, trí tưởng tượng và những ý tưởng táo bạo, đây là những điều rất cần thiết nếu muốn có một sự thay đổi mạnh mẽ. Cuốn sách này rất phù hợp cho những bạn đọc trẻ, những người nắm giữ chìa khóa cho cánh cửa tương lai phía trước.
4
20073
2013-03-25 18:10:04
--------------------------
