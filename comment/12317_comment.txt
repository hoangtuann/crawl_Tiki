504713
12317
Mình cực thích ăn lẩu, bất kể đông hay hè, nên khi liếc thấy cuốn sách này đã cảm thấy rất thích thú rồi. "Câu chuyện bên nồi lẩu", cảm giác có sự quây quần, ấm áp, giản dị, rất bình yên, và thực sự từng câu chuyện trong cuốn sách cũng đều đem lại cho mình những xúc cảm như vậy. Đọc để cảm nhận và tận hưởng. 
5
2034317
2017-01-04 08:26:19
--------------------------
502453
12317
Ngôn từ giản dị, mỗi câu chuyện đều rất ngắn, tưởng như chỉ là những chuyện bình thường trong cuộc sống, nhưng lại khiến người đọc cảm thấy trong mỗi câu chuyện chất chứa biết bao tâm sự, rất trầm lắng. Hy vọng có ngày được gặp tác giả.
5
2018880
2016-12-30 09:46:50
--------------------------
477865
12317
Đã quá lâu rồi tôi mới đọc một cuốn sách hấp dẫn như vậy. Đây thực sự là 1 cuốn sách đậm chất Việt Nam, như cái thời mà tôi yêu thích văn học hiện thực của các cụ Nguyễn Tuân, Nguyễn Công Hoan... vậy. Tất nhiên nó không cổ điển khô khan, nhưng "chất" mà nó đem lại khiến tôi cảm giác muốn nâng niu và đọc đi đọc lại cuốn sách này nhiều lần.
Những bài viết làm tôi nhớ đầu những năm 90, dù kí ức không còn nhiều. Là những câu chuyện nhỏ từ nhà ra ngõ, những căn nhà tập thể cấp 4, những người hàng xóm đúng thật là hàng xóm ngày xưa, ngay cả những con mèo cũng hiện lên vô cùng thân thương.
Tập tản văn vô cùng ấn tượng. Tôi không biết nói thêm điều gì để diễn tả cảm xúc của mình khi đọc cuốn sách này nữa. Được biết tác giả đến từ Mann Up, tưởng sẽ lại đọc thứ tình yêu ngôn tình, nhưng hóa ra là chất rất hay. Xứng đáng có trong tủ sách của mỗi người.
5
4397
2016-07-29 11:26:48
--------------------------
