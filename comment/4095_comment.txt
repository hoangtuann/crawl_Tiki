364510
4095
Sự "lạ đời" trong quyển sách kỹ năng này nằm ở chổ tác giả không chỉ cung cấp cho người đọc các mẹo A,B,C... thông thường mà ta có thể bắt gặp đầy rẫy trên mạng hay các cuốn dạy tìm việc phổ thông khác, mà tác giả dường như đã cố gắng chỉ cách ta nên tư duy về "việc tìm việc hay tự tạo việc" của chính bản thân. Vì trong hầu hết trường hợp, chỉ có tư duy đúng thì việc làm mới đúng được. 
Đặc biệt mình thích nhất 2 quan điểm của tác giả :
  + "Cá bơi trong nước, người bơi trong xã hội", kỹ năng tìm việc đã trở thành kỹ năng sinh tồn trong thế kỷ 21
   + " Mỗi người dạy một người"vì đó là khả năng sinh tồn nên chúng ta cần phải chia sẽ cho nhau để mỗi người đều có thể tự tìm được việc làm phù hợp cho chính bản thân mình. 

5
520494
2016-01-06 12:41:36
--------------------------
352139
4095
Thật sự trước giờ mình rất ngán đọc sách, vì dù có đọc nhiều đến cỡ nào cũng không hiểu, không tiếp thu được bao nhiêu. Vậy mà lại thấy thích cuốn sách này mới lạ chứ, mọi thứ đều có quy luật của nó, chỉ cần chúng ta sáng suốt sẽ tìm được lối đi đúng đắn. Cuốn sách đã cô đọng mọi thứ cần thiết cho những người trẻ, đặc biệt là những người đang mắc kẹt trong cái vòng lẩn quẩn xin việc - phỏng vấn - thất bại - .... 
Học hành tuy là chìa khóa quan trọng nhưng vẫn chưa đủ, muốn biết cách mở chìa khóa ấy sao cho vẹn toàn, hãy đọc sách ^^
4
918978
2015-12-13 21:02:56
--------------------------
301912
4095
Một cuốn sách hay dành cho những ai đang bế tắc trong công việc và muốn chuyển sang một công việc khác phù hợp với mình hơn; cũng như những người đang thất nghiệp. Thay vì ngồi buồn chán, than vãn thì thời gian thất nghiệp là khoảng thời gian bạn làm được rất nhiều việc ý nghĩa như: học một cái gì đó, sửa chữa, làm từ thiện .... Cách tìm việc thành công nhất là tự vấn bản thân rồi làm sơ đồ bông hoa ... Rất hay và thú vị. Mình mất gần 1 tháng để đọc xong cuốn sách này (mỗi ngày 2-3 tiếng) và bây giờ dành thời gian để làm theo sách.
5
205349
2015-09-14 21:35:55
--------------------------
281876
4095
Có thể nói đây là cuốn sách tiếp thêm sức mạnh cho  những người khao khát tìm việc, và cũng mang những đồng cảm, niềm tin tới cho những người thất nghiệp. Sách đưa ra những rào cản về mặt kĩ năng, những bài tập định hướng lại bản thân, để tìm ra mình, rồi đến tìm việc, Về qui trình xin việc, tác giả chỉ định hướng qua về cách viết cv, vài câu hỏi phỏng vấn có thể gặp, cách đàm phán lương, nội dung tập trung của sách theo mình là thiên về khám phá bản thân nhiều hơn. Sách được trình bày khá cẩn thận, giấy đẹp, dễ nhìn, có bọc bìa cẩn thận.
4
588698
2015-08-28 19:32:26
--------------------------
277058
4095
Vào thời kỳ hội nhập thì quả thực đây là một cuốn sách không thể thiếu cho mỗi chúng ta. 
Cuốn sách hướng dẫn rõ ràng từ cách tìm việc bình thường đến chuyên nghiệp, viết CV như thế nào, cách thỏa thuận lương và rất nhiều điều mà trước đây chúng ta chưa biết...
Và quan trọng hơn hết là Tác giả đã đưa ra các bài tập lựa chọn để biết ta đang cần những gì để hiểu rõ được bản thân mỗi người.

5
761628
2015-08-24 16:38:50
--------------------------
