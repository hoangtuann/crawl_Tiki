255289
10225
Theo tôi, cuốn sách viết khá đầy đủ về cuộc đời của nữ bác học vĩ đại Marie Curie. Từ xuất thân, bối cảnh gia đình, tình hình xã hội Ba Lan lúc bấy giờ, thành tích học tập và tài năng thiên phú của bà, những chuỗi ngày vất vả, thiếu thốn, gian nan mà bà đã trải qua đến những ngày bà học  ở Pháp, sự nghiệp theo đuổi khoa học của bà, cho đến những sóng gió ba đối mặt sau cái chết của chồng mình và cả những năm tháng cuối đời của nữ khoa học này. Tôi thích quyển sách này ở chỗ tác giả không chỉ viết lên câu chuyện về Marie Curie dựa trên những sự kiện, tài tiệu mà còn đưa một phần những bức thư mà thân gửi cho bà  ( hoặc ngược lại), vài đoạn ngắn trong tác phẩm viết về bà mà tác giả chính là con gái bà Eve Curie  vào làm cho tác phẩm thêm phần khách quan tăng tính xác thực. Ngoài ra ở cuối sách còn có các gia thoại khá thú vị xoay quanh nữ khoa học này cùng với phần chú thích khá kỹ lưỡng, dễ hiểu. Độ dày  của sách vừa phải. Tóm lại đây là một quyển sách đáng để đọc.
5
64972
2015-08-05 21:52:25
--------------------------
254360
10225
Cuốn sách đã giúp ta hiểu thêm về người nữ bác học tài ba đã đạt được 2 giải Nobel về Vật lí và Hóa học: Marie Curie. Cuộc đời của bà đã được tác giả kể lại thật chi tiết sâu sắc giúp người đọc có thể hiểu thêm về người phụ nữ tài giỏi nhưng đầy giản dị. Bà là một con người thiên tài,một nhà hóa học hàng đầu của thế giới. Các cống hiến của bà cho ngành hạt nhân quả là to lớn và vĩ đại.Bên cạnh đó,sách còn giúp ta hiểu thêm Marie là một người phụ nữ tuyệt vời.Một con người yêu thương con cái,chồng và tất cả mọi người xung quanh.Nếu có yêu thích và muốn tìm hiểu về các nhà khoa học thì bạn nên đọc nó!!!! 
5
713380
2015-08-05 10:22:06
--------------------------
233657
10225
Cuốn sách có thể xem như một câu chuyện khá đầy đủ về cuộc đời nhà nữ khoa học Marie Curie từ xuất thân, bối cảnh gia đình, những năm tháng tuổi thơ đến cả một chặng đường dài từ ngày đầu gian nan, vất vả, thiếu thốn, khắc nghiệt đến những thành công mà bà đạt được trong sự nghiệp nghiên cứu khoa học của bà, những trăn trở, những hoài bão, những hi sinh to lớn của bà cho sự nghiệp khoa học, cho nhân loại, đến cả tính cách, những tình cảm lớn lao trong bà cho gia đình, chồng con, người thân, tổ quốc, sự nghiệp khoa học. Cùng quá đó là những diễn biến xã hội, chính trị khá đầy đủ, cụ thể ở thời cuộc của bà làm ta dễ dàng hình dung được phần nào những gì bà trải qua, cũng như có thể ôn lại được kiến thức lịch sử. Ngoài ra ở phần cuối sách còn cả những giai thoại về bà, những kiến thức cơ bản về phóng xạ, và cả 1 bảng đối chiếu những danh từ kỹ thuật (Việt -Pháp-Anh). Ngoài ra còn được cung cấp kiến thức về các nhà khoa học ở cùng thời đại của bà. Chúng ta sẽ càng hiểu và khâm phục hơn những tính cách quý giá của những khoa học chân chính. Đọc cuốn sách ta sẽ càng thêm khâm phục nhà nữ khoa học Marie Curie cũng như hiểu được không phải tự nhiên mà người ta có thể thành công, họ phải hi sinh, đánh đổi ra rất nhiều cũng như một niềm tin, một nghị lực phi thường mới có thể đạt được những thành công dù ở lĩnh vực nào đi nữa. Một quyển sách rất đáng để đọc.
5
544102
2015-07-19 20:24:27
--------------------------
168767
10225
Mình rất yêu thích khoa học nên đã chọn mua nó để đọc thử. Đây là cuốn sách viết rất rõ về bà Marie Curie, một người phụ nữ thông minh , quyết đoán, có ước mơ chinh phục khoa học. Ngoài ra bà là người hòa thuận với chồng, với gia đình và đặc biệt luôn gắn bó với họ ngay cả khi khó khăn nhất. Nếu có đam mê khoa học thì các bạn hãy mua nó, không chỉ hiểu biết về cuộc đời của bà Curie mà hẳn các bạn sẽ nhận được nhiều bài học từ bà, từ đó yêu cuộc sống và phấn đấu nhiều hơn trong xã hội phát triển ngày nay
5
465628
2015-03-17 09:17:38
--------------------------
157216
10225
Cuốn sách đã cho ta hiểu rõ về nhà khoa học vĩ đại Marie Quire.Bà là một con người thiên tài,một nhà hóa học hàng đầu của thế giới.Các cống hiến của bà cho ngành hạt nhân quả là to lớn và vĩ đại.Bên cạnh đó,sách còn giúp ta hiểu thêm Marie là một người phụ nữ tuyệt vời.Một con người yêu thương con cái,chồng và tất cả mọi người xung quanh.Ngay cả trong những hoàn cảnh khốn cùng nhất,bà cũng không nghĩ cho mình mà luôn luôn nghĩ cho mọi người,cho đất nước,cho thế giới.Tất cả đã làm nên một nhà khoa học đại tài.Bà là tấm gương cho chúng ta học tập !!!
5
363312
2015-02-07 22:38:06
--------------------------
