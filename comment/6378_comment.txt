377848
6378
Mình mới vừa ôn thi Ielts và đặt cuốn này để tự học, cảm thấy sách phần nghe cũng vừa với khả năng, không quá khó, sách nhiều chủ đề rất thực tế và nhiều từ vựng mới. Bìa sách thiết kế khá tốt, giấy tốt, không quá mỏng, nội dụng thì khỏi bàn cãi vì sách Collins hầu như là rất hay. Một cuốn sách rất đáng mua cho những ai có niềm đam mê anh văn và có khả năng tự học tốt.
Tiki giao hàng lại rất nhanh, uy tín , sách không bị hư hỏng chổ nào. Mình rất vừa ý và đang chuẩn bị mua thêm cuốn khác của Collins để luyện thi

4
313333
2016-02-03 15:24:37
--------------------------
358298
6378
Đây là một quyển sách với mình là cần thiết đối với những ai đang và muốn học Ielts để thi lấy chứng chỉ. Trong quyển sách này,chúng ta sẽ học đc rất nhiều điều bổ ích về cách làm sao để đạt điểm kĩ năng nghe như mong muốn hay là những mẹo nhỏ giúp mình làm tốt bài thi Ielts. Thêm vào đó, quyển sách này giúp các bạn củng cố kĩ nắng khi làm bài nghe, trau dồi kiến thức tiếng Anh để thi Ielts. Ngoài ra,các bạn nên tìm đọc thêm nhiều sách khác của Cambridge, Collin, Oxford...
4
808833
2015-12-25 08:37:11
--------------------------
340641
6378
Sau khi quyết định tự học ielts tại nhà. Thì mình đã đào cả internet để kiếm những tài liệu hay. Cuối cùng thì kiếm được cuốn này. Bộ Collins này dành cho những ai đang ở 6- 7.5 bạn nào có tham vọng muốn lấy điểm cao hơn. Các bài tập trong sách hướng dẫn rất rõ ràng, đặc biệt là phần vocabularies, mình rất thích phần này, có giúp mình có vốn từ nhiều để áp dụng cho phần thi speaking. Chỉ có một khuyết điểm là số unit hơi ít. chỉ có khoảng 12 unit.Chúc các bạn thành công trong thời gian ôn luyện ielts tại nhà.
4
58391
2015-11-20 11:06:53
--------------------------
318098
6378
Mình đã từng đi ôn thi IELTS và nhận ra mình kém phần Nghe nhất nên quyết định mua sách này để ôn luyện khả năng nghe tiếng Anh của bản thân.
Sách này khá hay. Học xong thấy rất thích vì nhờ quyển sách này mà khả năng nghe tiếng Anh của mình được cải thiện. Hơn nữa vốn từ vựng cũng tăng lên và ngữ pháp cũng được bổ sung không ít.
Sách hướng dẫn cụ thể và dễ nắm bắt nội dung khiến người học không bị nản vì chương trình quá sức.
Bên cạnh đó sách còn kèm đĩa nên rất thuận tiện cho việc luyện nghe.
Nên mua!
4
679399
2015-10-05 00:05:22
--------------------------
300561
6378
Collins là một trong những tên tuổi quen thuộc với những người luyện thi IELTS. Trang sách dày, giấy tốt. Có nhiều chủ đề khác nhau, các bài tập cụ thể, chi tiết, từ dễ đến nâng cao, đa dạng hình thức nghe. Sách cũng có thể giúp bạn biết thêm một số từ vựng hay xuất hiện trong đề thi IELTS. Nếu bạn muốn cải thiện kĩ năng nghe hoặc đang trong quá trình luyện thi thì đây là một sự lựa chọn tốt. Sách phù hợp cho những bạn muốn luyện band 6 - 7.5+. Chỉ có điều sách không được giảm giá trên Tiki. 
5
594849
2015-09-14 00:14:57
--------------------------
287514
6378
Đây là cuốn sách cuối cùng mình mua trong bộ 4 IELTS của Collins. Mình rất hài lòng về bộ sách này. Theo mình thì Listening là kỹ năng dễ nhất trong IELTS, tuy nhiên khi làm bài nếu không cẩn thận sẽ có thể mắc bẫy và bị mất điểm. Cuốn sách này chia thành 12 unit, tập trung vào các chủ đề thường gặp trong bài thi, được trình bày rất dễ hiểu. Sách giúp bổ sung một lượng lớn từ vựng, cung cấp các mẹo làm bài để tiết kiệm thời gian và đạt kết quả cao.
5
177708
2015-09-02 20:24:37
--------------------------
243774
6378
Cuốn sách này phù hợp với những người tự học vì nội dung trong sách tương đối nhẹ nhàng, dễ nghiên cứu. Trong sách còn có cả những bí quyết, mẹo làm bài giúp người học có thể hoàn thành bài thi tốt nhất. Nhược điểm của sách là cỡ chữ hơi nhỏ (theo đánh giá chủ quan của mình) nên khi tra từ mới mà viết lên trên cho tiện thì nhìn sẽ khó hơn. Mình thấy khá tiếc khi không ôn quyển này trước khi làm bài tập listening test trong IELTS Cambridge vì bài tập trong đây có vẻ dễ hơn nhưng cũng rất sát với đề thi.
4
134196
2015-07-27 18:18:50
--------------------------
176411
6378
Việc ôn thi ielts nói chung là rất khó, đặc biệt là kĩ năng listening vì mấy năm đi học toàn học ngữ pháp xuông nên khi ôn listening mình cảm thấy rất khó vì họ nói rất nhanh, lượng từ mới nhiều,nhiều cấu trúc lạ có thể nghe không ra nhưng mình tin rằng với quyển sách này, các bạn có thể tự học và tự nâng cao khả năng listening nhờ các tips hay các cách học của bạn cũng như vốn từ vựng cần thiết cho việc ôn thi ielts của bạn vì nó vừa có bài tập để luyện tập nhưng cũng vừa có lời giải rất hay.
4
594873
2015-04-01 09:36:06
--------------------------
156388
6378
Mình thấy quyển này cùng với mấy quyển ôn IELTS khác chung bộ của Collins như Speaking, Reading, Writing đều đi theo cấu trúc là vừa luyện kĩ năng, vừa học thêm từ mới trong mỗi Unit và có bài Practice theo sau. Dạng bài tập thì rất đa dạng, có dễ, có khó và trong phần Transcript luôn gạch chân sẵn phần giải đáp cho Answer key nên đọc rất dễ hiểu. Ngoài ra, mình rất thích cách chọn chủ đề của sách, rất gần gũi và sát với đề thi hiện hành, độ khó của quyển này nằm ở mức Intermediate nên cũng phù hợp với nhiều bạn.
5
66279
2015-02-04 16:32:52
--------------------------
153465
6378
Những quyển sách học tiếng Anh này của Collins không hề làm tôi thất vọng.
Nó rất đầy đủ và chi tiết dành cho những người tự học. Có phần Audio Scrip . Answer Key để mọi người có thể tham khảo thêm và ôn lại những chỗ mình sai. Trong sách có vài phần Tips dành riêng cho luyện Ielts.
 Chỉ cần siêng năng luyện tập thì nhưng quyển sách thuộc loạt luyện thi này rất hiện quả. Bám sát chương trình thi hiện nay.
Bài tập từ đơn giản tới nâng cao. Tôi mua luôn cả 4 quyển Lis-Red-Wri-Spe và luôn Vocabulary nữa. Thấy rất hữu ích.
3
92139
2015-01-26 13:38:39
--------------------------
