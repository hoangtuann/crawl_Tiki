397359
5411
Ấn tượng ban đầu của tôi đó là tiêu đề của cuốn sách. Một cuốn sách với cái tên " mọi chuyện rồi sẽ ổn cả thôi " rất thích hợp cho những ai có tâm trạng rối bời và cần bình tĩnh giải quyết vấn đề. Ấn tượng tiếp theo là ở bìa của cuốn sách, nó được thiết kế với màu sắc hài hòa, hình ảnh dễ thương. 
Trong cuộc sống tấp nập và vội vã như thế này, mỗi chúng ta đã bỏ lỡ những khoảnh khắc tươi đẹp của cuộc đời. Chính vì lẽ đó, chúng ta, hãy "sống chậm lại" mà trân trọng những điều tốt đẹp ở thế giới ngoài kia, mà bình tĩnh vui sống ! 
4
518457
2016-03-14 20:53:09
--------------------------
396772
5411
Mình chưa thấy nội dung ăn nhập gì với tựa đề lắm, cơ mà những bài học mà tác giả chia sẻ hay và thực tế. Sách khá mỏng cơ mà cũng hết 2 buổi mình mới đọc xong. Thích nhất ở sách cái phần định nghĩa về chữ "thiền". "Giờ ăn thì ăn, giờ ngủ thì ngủ gọi là thiền" nhớ câu đó mà thích nhất khúc đó trong sách. Thiền là chuyên tâm làm bất kỳ việc gì (ăn uống, ngủ nghỉ, học tập, làm việc, vui chơi...) và mang lại cảm giác thoải mái cho bản thân. Cơ bản là mọi hoạt động trong cuộc sống đều mang lại niềm vui cho bạn nếu bạn "thiền" ha ha ha.
3
204540
2016-03-14 00:35:07
--------------------------
374988
5411
Đập vào mắt là bìa sách, mình nghĩ ngay đến 1 quyển truyện anime hoặc là tiểu thuyết ngôn tình gì đấy. Được bạn giới thiệu nên quyết định mua thử về đọc. Một từ là tuyệt vời. Cuốn sách là động lực lớn để mình quyết tâm hơn, giúp mình có một mục tiêu rõ ràng cho cuộc sống và tương lai, chứ không phải sống buông thả như bây giờ. Cuốn sách khuyên mình làm những điều có ích không lãng phí thời gian cho những việc vô bổ. Đọc thấy có chút gì của bản thân trong đó. Mong là sau cuốn sách này mình sẽ sửa đổi được bản thân
5
112691
2016-01-27 16:10:43
--------------------------
361465
5411
Không biết có ai có cùng suy nghĩ như tôi không nhưng tôi thực sự cảm thấy quyển sách này rất đáng yêu. Qua lời kể chuyện của cô gái, những bài học mà sách muốn nêu ra không còn cứng nhắc, mà ngược lại rất dễ đi sâu vào lòng người. Qua quyển sách, tôi nhận ra được những việc mình cần và phải làm để xác định cho tương lai của mình, khi mà chỉ còn 2 năm nữa tôi sẽ tốt nghiệp, trong khi tôi còn chưa xác định được ước mơ, mục tiêu cũng như tương lai của mình. Cuốn sách đã đưa ra những lời khuyên rất chân thực, đúng đắn, khiến ta phải suy ngẫm nhưng lại qua những lời kể rất bình dị, gần gũi và có một chút gì đó hóm hỉnh của một cô gái. Nó giúp ta nhận ra được những giá trị thực của cuộc sống, của mục tiêu mình hướng tới, và của những hạnh phúc đơn giản xung quanh mình. 
5
942923
2015-12-31 02:24:42
--------------------------
295336
5411
Nhiều khi trong cuộc sống này, bạn sẽ giật mình nhận ra hình như mình đã bỏ lỡ điều gì  rồi. Chúng ta dường như bị cuốn vào cuộc sống này một cách vô thức, mọi chuyện trôi qua quá nhanh mà chúng ta không kịp nhận ra.
Rồi đến khi tôi đọc cuốn sách này, tôi chợt hiểu ra rằng mình cần phải sống chậm lại. Sống chậm lại để biết được những vẻ đẹp xung quanh mình, sống chậm lại để hiểu bản thân mình hơn và sống chậm lại  để sống hạnh phúc hơn. Nếu bạn gặp quá nhiều khó khăn, áp lực trong cuộc sống này hãy thử sống chậm lại xem sao. Cuối cùng bạn sẽ nhận ra được rằng, tất cả mọi chuyện rồi sẽ ổn thôi.
5
145385
2015-09-10 09:50:14
--------------------------
261120
5411
Khi tôi đọc thơ Xuân Diệu, mọi thứ đều vội vàng, nhưng khi tôi đọc những tác phẩm khác, cả tác phẩm này nữa, mọi thứ đều như khuyên tôi hãy sống chậm lại, đời sẽ đẹp hơn, sẽ thoáng hơn, sẽ bao dung hơn. Rốt cục thì sống nhanh sống vội cũng cái hay riêng, sống chậm lại cũng có cái tốt riêng. Hóa ra cũng là chuyện của mỗi người, trầm tư suy nghĩ chuyện này, chắc mình sẽ sống chậm hơn 1 chút rồi, và bất giác cảm thấy, mọi thứ xung quanh đều ý nghĩa biết bao. Nhưng ở thời đại này, thời đại tên lửa thì cuộc sống xô bồ, chưa kịp nghĩ xong hôm nay làm gì thì ngày mai đã phải bươn chải, tôi nghĩ, quyển sách này có thể làm ta kịp thời nhận ra vòng xoáy xung quanh nhanh chậm gì cũng là do bản thân mình mà thôi.
5
516261
2015-08-10 21:29:52
--------------------------
255386
5411
Khi tôi đặt mua cuốn sách này là lúc chỉ còn vài tháng nữa là bước vào kì thi đại học, một trong những kì thi quan trọng nhất của cuộc đời mỗi con người, quả thực cuốn sách đã tiếp thêm sức mạnh cho tôi để có thể vững tin thêm vào bản thân mình. Đây thực sự là 1 cuốn sách hay, giàu ý nghĩa, đáng đọc, đáng suy ngẫm. Mỗi trang sách giống như những bài học nhỏ, nhưng mang ý nghĩa vô cùng sâu sắc. Khi đọc những trang sách này điều đầu tiên tôi cảm nhận được chính là sự ấm áp. Từng câu từng chữ như khắc sâu vào trong tim. Phải rồi, cứ sống chậm lại, đừng vội vã, hãy cảm nhận cuộc sống xung quanh, hãy cứ sống là chính mình, không sao hết, mọi chuyện rồi sẽ ổn thôi.. Yêu cuốn sách này thật nhiều...
Tuy nhiên bìa sách không thực sự nổi bật, màu giấy tối, hơi khó nhìn...
5
527418
2015-08-05 23:39:11
--------------------------
198070
5411
Một cuốn sách với cái tựa đề thật hay và cái bìa sách thật bắt mắt.Đã từ lâu mình đã rất ấn tượng và quý mến công ty sách Alphabook vì công ty luôn cung cấp và biên soạn những cuốn sách rất chất cho đọc giả Việt.Người trẻ tụi mình mang nhiều mộng ước dệt tương lai lắm nhưng đôi khi trên hành trình chinh phục ước mơ gặp nhiều trở ngại cũng chỉ vì một số tính cách của mình,đặc biệt là tính nóng vội hấp tấp.Nhiều bạn trẻ còn dễ bị cuốn vào lối sống nhanh gấp gáp ngày thường đến nỗi bị căng thẳng stress nên quả thực cuốn sách là một cuốn sách hay đáng mua để đặt lên kệ.Mình cứ mong cuốn này dày hơn chút nữa ^^ đọc cho đã nhưng những chia sẻ trong sách là rất quý rồi.
5
456366
2015-05-19 08:12:41
--------------------------
179814
5411
Ngay tần đầu tiên mình nhìn thấy cuốn sách này mình đã quyết định sẽ mua nó. Qua bao ngày chờ đợi hết hàng rồi thì mình cũng rước được em ý về. Từ nội dung cho tới hình thức mình đều tuyệt vời. Tên sách giống như một lời khuyên khi ta mệt mỏi, nội dung thì giống như những liều thuốc khi tâm hồn ta bệnh. Cuốn sách đề cập tới những vấn đề sâu thẳm trong tâm hồn mỗi người, có những thứ ta chỉ muốn giấu giếm và lảng tránh nó, nhưng càng giấu càng lảng thì nó lại càng âm ỉ và lớn dần. Vậy nên cách duy nhất là đối mặt.
Cám ơn AlphaBooks và Tiki nhiều vì một cuốn sách tuyệt vời!
5
137031
2015-04-08 00:04:14
--------------------------
169879
5411
Cuốn sách này được trang trí ở bìa rất bắt mắt và đẹp . Có thể nói nó rất thu hút sự chú ý đến tất cả mọi người . Cuốn sách này luôn mang lại sự gần gủi và những lời động viên cho các giới trẻ hiện nay cũng như là những bài học bổ ích . Cuốn sách này thậ sự hay và bổ ích . Cuốn sách này làm cho nhiều người bị cuốn hút vào nó kể cả mẹ tôi cũng thích đọc nó , nên khuyên chúng ta xác định một tương lai và mục tiêu rõ ràng . Nó luôn khuyên ta là không bao giờ từ bỏ đam mê và ước mơ của mình trước những khó khăn . 
Cảm ơn Tiki đã mang lại nhiều cuốn sách thật nhiều nhân văn và bổ ích :)
5
534439
2015-03-18 23:40:33
--------------------------
155931
5411
Điều đầu tiên làm tôi chú ý tới cuốn sách này bìa ngoài của cuốn sách với hình bìa khá đẹp mắt với màu sắc được phối hợp hài hòa hơn nữa tôi nhìn thấy cuốn sách trong tâm trạng cực kì tồi tệ và cái tên của no : " sống chậm lại mọi chuyện sẽ ổn thôi" như một lời an ủi tôi trong lúc đó, nó cũng khơi gợi trí tò mò của bản thân tôi, tôi muốn xem trong đó tác giả viết những gì. chính những điều ấy đã thúc giục tôi mua cuốn sách này. Tôi cảm thấy nó thực sự rất hay và bổ ích. Cuộc sống đôi khi trôi quá nhanh mà chúng ta thì cứ như những vật vô tri bị cuốn trôi theo những cuồn quay ấy. Bạn hay tôi đã đôi lần quên đi giá trị của cuộc ống đích thực và đã đến lúc chúng ta lên thay đổi, nên xác định mục đính sống cho bản thân, sống tích cực và đừng bao giờ từ bỏ hay gục ngã trước khó khăn.
5
450785
2015-02-02 17:24:57
--------------------------
149566
5411
Ngày ngày tôi vẫn hộc tốc đạp xe trên con đường chảy dài những đường bánh lăn tròn sao cho kịp giờ học.
Ngày ngày tôi vẫn quơ quýt cơm rau đủ để cơn đói không ảnh hưởng đến công việc còn đang dang dở.
Tôi yêu cầu mình hoàn hảo, tốt đẹp.
Tôi mong mình đứng trên ánh hào quang chói mắt người đời.
Nhưng rồi khi nhìn thấy người anh vừa tốt nghiệp đại học nằm nhà chơi game cả ngày, rồi lại ngủ, tự dưng giật mình nhìn lại xem tôi đã có gì.
Nothing!!!
Tôi hoang mang vật lộn trong cuộc sống hàng ngày, đôi khi đơn giản nghĩ chỉ cần cố gắng đuổi kịp là được. Cố gắng đạp xe đạp để bắt kịp đuôi xe máy.
Tôi không nghĩ được rằng mình có thể đạp xe chậm lại để nhắc một xe máy khác chưa gạt chân chống, hay để chào cô lao công đang mệt nhọc đẩy xe vừa đi qua trên con đường bạc màu.
Có lẽ "sống chậm lại rồi mọi chuyện sẽ ổn thôi" chính là chìa khóa cho tôi, để tôi nhìn nhận lại cuộc sống này theo cách khác, cái cách "chậm mà chắc" ông tôi hay thủ thỉ. theo cái cách mà kim giờ không phải quay để đuổi kịp kim phút, mà quay để sự vận động của kim phút không là vô nghĩa.
Xin được cảm ơn Tiki vì nhờ Tiki mà mình cũng mới tìm được cuốn này! ^_^
5
535631
2015-01-13 23:35:42
--------------------------
