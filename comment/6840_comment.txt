309595
6840
Quyển sách là một  trong bộ 3 quyển luyện kỹ năng Nghe Tiếng Anh nổi tiếng. Đây là quyển thứ 3 - dành cho trình độ nâng cao. 
+ Ưu điểm: Các bài học được sắp xếp theo trình tự nâng dần độ khó, chỉ dẫn rõ ràng, bài tập đa dạng. Các bài thực hành, hội thoại được tăng cường độ khó về ngữ pháp và từ vựng vì là quyển nâng cao.
+ Khuyết điểm: Sách in trắng đen nên không thể làm tốt các bài học về màu sắc. Khổ sách được thu nhỏ từ sách gốc có màu nên chữ và hình ảnh hơi  mờ.
Mình mua quyển không kèm CD nên không biết ấn bản lần này có thay đổi gì về giọng đọc hay không. Nhưng nội dung vẫn được giữ nguyên nên mình nghĩ cũng là giọng đọc bản ngữ dễ nghe.
4
276944
2015-09-19 01:38:19
--------------------------
282235
6840
Đây là tập thứ 3 trong bộ sách Luyện nghe tiếng Anh, tập này đã là trình độ nâng cao rồi (Expanding). Cuốn sách dành cho những người đã hiểu biết về tiếng Anh trước đó nhưng vẫn muốn thực hành nhiều hơn, đặc biệt là về phần nghe, sau đó thông qua các đoạn hội thoại, các bài tập trong sách sẽ giúp ích cho việc giao tiếp bằng tiếng Anh. Một cuốn sách hay với nhiều bài học, mỗi bài nói về 1 đề tài liên quan đến cuộc sống thường ngày, các đề tài này được chọn lọc kỹ càng và gây hứng thú cho người học, bạn sẽ học hỏi được nhiều kinh nghiệm hơn nếu chịu học kỹ cuốn sách. Nhưng mà sách không đi kèm CD nên mình phải tự lên mạng kiếm tài liệu nghe (cần kết hợp luyện nghe nhé vì đó mới là tiêu chí của bộ sách này mà), sau nhiều bài thực hành nghe nói thì mình thấy đỡ sợ hơn khi phải giao tiếp tiếng Anh. Một cuốn sách bổ ích, thiết thực và rẻ nữa chứ ^^ 
5
75959
2015-08-28 23:49:58
--------------------------
243714
6840
Đây là một cuốn sách rất hay và hữu ích đối với mình. Cuốn sách giúp được mình rất nhiều trong việc luyện kỹ năng nghe Tiếng Anh. Hiện tại ở trường mình các bạn học chuyên đều cần dùng quyển sách này, nhưng ở có mình lại không mua được, nên khi biết ở trên tiki có mình cùng bạn mình đã đặt mua về để khỏi phải sử dụng sách photo. Sách mua về vừa in rõ nét lại có giá thành rẻ ngang với đi photo bởi vậy mình cảm thấy cuốn sách này vô cùng hữu ích với mình.
5
604794
2015-07-27 17:24:35
--------------------------
