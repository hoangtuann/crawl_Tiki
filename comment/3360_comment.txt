491846
3360
Mình thích sự sáng tạo của quyển sách này, có một sự tương tác rất tốt. Sách truyện phương tây thường hay có những kiểu như vậy, giúp bọn trẻ thấy thú vị hơn với việc đọc sách và yêu sách hơn.
Quyển này dĩ nhiên là không xem được ở phiên bản ebook rồi, tại sao à, bạn mua đi rồi biết hehe
5
821333
2016-11-21 13:22:29
--------------------------
453003
3360
Lúc đọc thông tin sản phẩm và đọc bình luận của các bạn thì mình cũng tò mò nên mua về thử . Nhưng mua về rồi cầm nó trên tay rồi thì mình lại không thích lắm vì nó pHù hợp với các em nhỏ hơn mình . Sách chỉ có vài trang giấy cứng có màu nên mình thấy hơi mắc . Sách ít chữ . Ít hình. Chủ yếu là gây sự tò mò cho các em nếu mẹ nào biết cách kể chuyện . Cũng chẳng biết ghi gì cho đủ 100 từ. Cảm ơn tiki .
4
593209
2016-06-20 22:42:51
--------------------------
442009
3360
Lâu lắm rồi mới có 1 cuốn picture book làm tôi cảm thấy bất ngờ thế này. Mặc dù ý tưởng không quá cao siêu nhưng lại rất thú vị khi dựa vào gáy sách để hình tượng hóa thành một cái - gì - đó - lưng - chừng nuốt chửng hết mọi thứ.
Ngoài ra, cuốn sách còn tạo sự tương tác đối với trẻ em, một điều rất ít cuốn sách có thể làm được, để kích thích sự hứng thú và tăng cảm giác tò mò cho trẻ. 
Sáng tạo vô cùng!
Tôi rất mong tìm đọc được nhiều cuốn sách đáng yêu như vậy hơn nữa.
5
190232
2016-06-04 10:30:07
--------------------------
337853
3360
Đây là một cuốn sách mà khiến tôi phải thốt lên "What? Cái gì thế này?" :)) Nhưng, chỉ cần một vài câu nói ngây ngô và hình ảnh minh họa hết sức đáng yêu của tác giả, đã dẫn chúng ta vào một câu chuyện ngắn rất dễ thương! Thật ra đọc xong cuốn sách này giống như bị lừa vậy! Nhân vật vô cùng ngay thơ... Chúng ta đơn giản không chỉ là đọc mà còn làm theo lời nói của nhân vật, cầm cuốn sách, nào xoay, nào lắc ... khiến chúng ta như hòa vào câu chuyện! Cuốn sách này viết cho thiếu nhi, tôi vẫn đọc ... Và tôi 16 tuổi rồi!! :)) Vẫn bị lừa như những đứa trẻ vậy! Vì cuốn sách dễ lôi cuốn người đọc quá mà!!! :))
5
641117
2015-11-14 21:13:49
--------------------------
306312
3360
Thực sự đây là một cuốn sách đã gây ấn tượng mạnh với mình bởi sự độc đáo, sáng tạo và mới lạ của tác giả. Mình rất thích trẻ con đọc những cuốn sách thế này vì sẽ khơi gợi, kích thích trí tò mò, khả năng tưởng tượng của các bé, điều mà mình nghĩ rất cần thiết cho sự phát triển của trẻ. À, cái hay nữa là quyển sách này không chỉ phải đọc một chiều như sách thông thường mà còn phải xoay, phải lắc nữa. Đảm bảo không chỉ các bé mà người lớn cũng thích.
5
785886
2015-09-17 14:58:53
--------------------------
279863
3360
Tôi đã chờ đợi khá lâu để mua được cuốn sách này. Tựa đề cuốn sách thật sự gợi sự tò mò cho người đọc: “Cuốn sách này vừa ăn mất con chó của tớ”. Sách vẽ ra một thế giới tưởng tượng: thông minh, tinh quái, đáng yêu, ngộ nghĩnh, thú vị, dễ thương… Hình minh họa trong sách rất dễ thương. Đây là cuốn sách dành cho trẻ nhỏ nhưng người lớn cũng có thể đọc được. Đọc cuốn sách này cảm giá tâm hồn mình trẻ ra bao nhiêu :)
4
332766
2015-08-27 10:25:54
--------------------------
256124
3360
Tôi cực kì thích những cuốn sách sáng tạo như thế này. Có lẽ nó bắt nguồn từ một ý tưởng trẻ con ngồ ngộ, và cách thể hiện thì đáng yêu kinh khủng. Chỉ là một cuốn sách  đã nuốt chửng mọi thứ vào thế giới của nó thôi mà. Tôi và bạn, hẳn cũng đã có lần cảm thấy mình rơi vào một cuốn sách, một câu chuyện, một thế giới tưởng tượng. Tác giả đã cụ thể hóa điều đó, và vẽ ra một cuốn sách tinh quái đáng yêu. Cuốn sách ấn tượng từ cái tên, mà mình tin rằng mọi em bé đọc xong sẽ đều thích thú mà nhắc lại câu nói đó: Cuốn sách này vừa ăn mất con chó của tớ!
5
82774
2015-08-06 16:39:11
--------------------------
254724
3360
"Cuốn Sách Này Vừa Ăn Mất Con Chó Của Tớ"- Richard Byrne là một cuốn sách hấp dẫn ngay từ tên gọi. Nó khiến mình lùng tìm ngay từ khi biết tên và luôn mong ngóng từng ngày sách có trên tiki vì lần nào mình tìm cũng hết hàng.Một cuốn sách làm mình thích mê. Tranh vẽ rất ngộ nghĩnh. Lúc đầu đọc cảm thấy thật điên rồ nhưng khi đọc xong lại cảm thấy buồn cười. Chính cái hồn nhiên của trẻ nhỏ mà tác giả đem vào làm cuốn sách trở nên nổi bật. Đây là một cuốn sách phù hợp cho mọi lứa tuổi.
5
294594
2015-08-05 14:25:49
--------------------------
254485
3360
Cuốn sách kỳ lạ nhất mà tôi có. Con tôi cũng rất thích cuốn này. Sau khi đọc cho con, con tôi luôn hỏi, làm thế nào mà cuốn sách này ăn mất con chó nhỉ, vậy con chó đã đi đâu, rồi Ben, Bella và cả xe cảnh sát... đã biến mất đường nào. Cách giải cứu ra sao? Tôi hiểu rằng cuốn sách ko chỉ mang lại sự kỳ lạ vốn có của nó mà còn mở ra cả 1 chân trời tưởng tượng cho trẻ con. Khi còn bé nếu các bạn cũng đã từng mơ hồ nghĩ ngợi rằng quyển sách rộng lớn thế nào mà sao cả 1 con rồng to đùng hay 1 chiếc xe cứu hỏa có thể chui vào trong đó. Thì giờ đây quyển sách này cũng cho trẻ con 1 ý niệm như vậy, rằng sau trang sách kia còn một khoảng không vô tận nào con chưa khám phá. Để từ đó đưa con trẻ bay bổng, tưởng tượng trong giấc mơ về một thế giới mà chính trang sách là cánh cửa thần kỳ
5
478786
2015-08-05 11:30:46
--------------------------
252122
3360
Quyển sách này mới xem tiêu đề thôi chưa gì đã làm người mua tò mò rồi. Vậy mà cầm lên tay rồi mới thấy "Một cuốn sách tinh quái! Quá tinh quái!" mà đúng tinh quái thật, làm thích ghê. Bé nhà mình và cả mình đều rất tò mò sau từng trang sách, không hiểu tại sao 1 quyển sách có thể ăn mất con chó và biết bao nhiêu thứ vào nữa. Vậy mà điều bất ngờ thú vị ấy cực kỳ hay, kết thúc hài hước lắm. Người lớn còn thấy kỳ lạ thì trẻ em còn thích thú hơn.
5
431238
2015-08-03 13:49:22
--------------------------
214234
3360
Quyển sách này được tôi bắt gặp lúc Nhã Nam giới thiệu.Tôi thấy vô cùng thú vị bởi chính cái tên kì cục của nó làm tôi đã thích thú. Khi lật vào trong, chỉ có vài ba dòng chữ còn lại là tranh nhưng nó đã diễn tả được hết nội dung. Lúc đầu đọc cảm thấy thật điên rồ nhưng khi đọc xong lại cảm thấy buồn cười. Chính cái hồn nhiên của trẻ nhỏ mà tác giả đem vào làm cuốn sách trở nên nổi bật, nó phù hợp cho thiếu nhi và còn cả vị thành niên. Bên Ngoài bìa sách cả bên trong tranh minh họa được vẽ rất tinh tế, và bắt mắt cho người đọc.
5
411363
2015-06-24 18:23:20
--------------------------
208005
3360
cuốn sách chứa đựng những tưởng tượng điên rồ nhất được biết thành hiện thực. Ngày nhỏ (cho đến giờ) mình vẫn có những tưởng về một con quái vật nào đó trong đầu: quái vật chăn, gầm giường,... bất cứ thứ gì thân thuộc nhất, mình tin rằng những bạn khác cũng vậy bởi mọi thứ đều là bí ẩn với trẻ nhỏ mà. Richard biến những suy nghĩ ngớ ngẩn đấy thành sự thật, mình nghĩ rằng ông sẽ rất "trẻ thơ" để có thể làm được thế. Tuy thế nét vẽ của Richard hơi gợi cho mình đến nhóc Nicole.
5
20175
2015-06-13 21:32:43
--------------------------
