571773
9857
Trước đây mình vẫn hay "nhận rác" từ người khác để rồi khiến bản thân mình thành một chiếc xe rác.  Sau khi đọc cuốn sách này mình đã nhìn nhận lại mọi sự việc diễn ra một cách bình tĩnh hơn, biết kiềm chế khi gặp những sự việc không mong muốn. Và quan trọng là phải cần phải rèn luyện thường xuyên, để không chỉ bản thân mình không "xả rác"nữa, mà có thể lan truyền đến mọi người xung quanh.
5
706054
2017-04-12 22:39:42
--------------------------
494667
9857
Bản thân mình có tính khá nóng và hay bị ảnh hưởng bởi sự tiêu cực từ người khác.
Rất may mắn khi mình tình cờ mua được quyển này trong dịp thẻ TPbank giảm giá 50%.
Mình luôn bỏ trong cặp và mỗi khi gần như nóng nảy những lời trong sách đã nhắc nhở mình phải bình tĩnh và không để mình trở thành nơi chứa rác của người khác.
Hảy mỉm  cười và bỏ qua.
Mình thích nhất đoạn này "“Có những người giống như “chiếc xe rác” vậy: họ chứa trong mình đầy “rác rưởi” - sự thất vọng, tức giận và chán nản. Và tất nhiên, họ phải tìm chỗ để trút bỏ mớ rác rưởi đó. Nếu thấy họ trút lên bạn thì bạn đừng đón nhận. Hãy mỉm cười, vẫy chào, chúc họ vui, rồi tiếp tục công việc của mình. Cứ tin tôi đi, rồi bạn sẽ thấy hạnh phúc hơn.”
5
353475
2016-12-08 11:28:22
--------------------------
466582
9857
Bài Học Diệu Kỳ Từ Chiếc Xe Rác là cuốn sách nhỏ như sách bỏ túi, dĩ nhiên chữ cũng nhỏ rồi, vậy mà First news không chịu ghi chú là sách bỏ túi, khi nhận được cuốn sách thì khá bất ngờ vì nó quá nhỏ so với tưởng tượng.
Nội dụng của quyển sách này chỉ xoay quanh việc dọn sạch sẽ tư tưởng tiêu cực như là dọn rác hằng ngày trong cuộc sống vậy, cho nên đọc hết quyển sách cũng không có gì mới mẻ thêm, tuy nhiên những câu chuyện vẫn mang ý nghĩa ở một mức độ nào đó.
3
74132
2016-07-02 14:25:04
--------------------------
463458
9857
Một quyển sách nhỏ, có lối ví von ẩn dụ thú vị. Những nguyên lí để sống một cuộc sống không bị ảnh hưởng bởi ngoại cảnh, nhiều người đã giác ngộ nhưng ít người có thể áp dụng thật sự và hoàn toàn vào cuộc sống, vì nó khó. Đúng, thật sự khó. Chính bản thân mình đã biết đến những nguyên tắc về bình an nội tâm khá lâu, tưởng như mình sẽ thực hiện đúng nhưng khi gặp phải vấn đề thực sự: bị người khác xả "rác" vào người thì việc để " mỉm cười" cho qua như bác lái xe trong quyển sách thực sự cần một nội tâm vững chãi và rèn luyện nhiều.
4
1128470
2016-06-29 12:20:59
--------------------------
400880
9857
Cuốn sách nhỏ gọn,dễ thương,không quá dày luôn làm cho tôi có cảm giác hứng thú khi đọc và không chán.Bạn có thể đọc ở mọi lúc mọi nơi mà không cần phải lo lắng xem liệu mình có đọc hết được cuốn sách này.Về nội dung,cuốn sách giúp ta thay đổi được câu chuyện trong đầu mình,giúp mình có tinh thần lạc quan khi gặp những 'chiếc xe rác'luôn xung quanh bạn.Đưa ra những phương pháp để bạn không gom quá nhiều điều phiền muộn trong cuộc sống.Khi đó bạn thấy cuộc sống của mình sẽ dễ chịu và yêu đời hơn.Xin cảm ơn tiki đã gói hàng rất cẩn thận và phục vụ nhiệt tình.
4
761874
2016-03-19 22:55:00
--------------------------
400735
9857
Đây là một cuốn sách nhỏ và mỏng nhưng chứa đựng nhiều bài học lớn.
 Cuốn sách dạy ta biết kiên nhẫn , nhẫn lại và biết mỉm cười bỏ ngoài tai những gì không đáng nghe, những gì xấu xa. Dạy ta biết cách làm thế nào  để hành vi tiêu cực của người khác không ảnh hưởng đến ta. Đây là bài học lí thú , bổ ích khiến tâm hồn ta trong sáng hơn , thanh thản hơn tránh được những bực tức... mà không phải chi li tính toán , nhỏ nhen ích kỉ... Đừng mất công tranh cãi vì trong lúc đôi co cãi vã bạn sẽ lãng phí đi khoảng thời gian mà bạn có thể làm được nhiều việc bổ ích hơn.
4
872351
2016-03-19 20:18:52
--------------------------
388035
9857
Cách đây khoảng 1 năm, mình tình cờ đọc được câu chuyện về chiếc xe rác qua trang Awake Your Power. Chỉ là một câu chuyện nhỏ được trích từ cuốn sách mà nó đã tác động và thay đổi suy nghĩ của mình rất nhiều. Cách hành xử của người lái taxi khi chứng kiến người khác "xả rác" đã làm mình ấn tượng và mình cũng muốn được sống thoải mái, rộng lượng như vậy. Rồi mình cũng quên dần câu chuyện này đi, mặc dù nhiều lúc gặp khó khăn, bực bội mình vẫn nhớ đến nó và kiềm chế bản thân lại, nhưng chẳng hiểu tại sao lúc đó không tìm cuốn sách này để mua. Gần đây, mình muốn mua sách để thư thả đầu óc, hy vọng sẽ có cuốn sách nào khai thông được cái não này, dĩ nhiên có cuốn "Bài học diệu kỳ từ chiếc xe rác" ("The law of the garbage truck") mà mình đang review :)). Thiệt ra là mình hay mua sách theo mức độ thịnh hành lắm, kiểu người ta khen là tò mò muốn mua. Bản thân cũng hay mua mấy cuốn sách triết lí để thay đổi bản thân, hoàn thiện nhân cách nhưng đọc xong, một thời gian sau lại đâu hoàn đấy (tính cứng đầu khó thay đổi :))). Lúc nhận sách, vì sách rất rẻ, chỉ 22k và nhỏ, mỏng như cuốn sổ tay nên dù có thích câu truyện trích đoạn đọc trên mạng trước đó bao nhiêu, mình cũng thấy tiếc. Tiếc vì mình nghĩ sách mỏng như vậy thì lhó mà khai thác sâu vào vấn đề, đọc không thấm hết nội dung tinh túy tác giả truyền đạt á :((. Đọc khoảng 40 trang đầu thì kiểu vẫn chưa thỏa mãn ấy, cảm giác như tác giả khai thác chưa sâu vấn đề, làm cho mình vẫn còn nhiều câu hỏi thắc mắc mỗi cuối mục. Nhưng bắt đầu từ mục số 6, mình cũng bị cuốn hút dần và biết đây là cuốn sách mình cần, và khi đọc hết cuốn sách (nhỏ nhắn) này, mình tin rằng nó sẽ thay đổi cách sống, lối suy nghĩ mình một thời gian dài kể từ bây giờ. Mình thấy những vấn đề mình gặp trong sách và học được cách giải quyết những vấn đề khó khăn đó. Sau khi đọc hết cuốn sách thì những câu hỏi trước đó mình thắc mắc cũng đã không còn quan trọng nữa, vì mình nghĩ bản thân đã học được một điều gì đó to lớn hơn. Kết thúc sách, tự hứa với bản thân sẽ cố gắng để thực hiện cam kết - "nói không với xe rác" của tác giả và mong muốn mọi người xung quanh cũng sẽ thực hiện nó hihi!!
Ps: "Xả rác" ở đây không phải là vứt rác ra ngoài đường đâu nha, mà là xả những cơn giận, sự bất mãn của bạn với những người xung quanh á!! Nếu tò mò thì mua sách đọc thử nha ;))
5
1135962
2016-02-27 23:50:40
--------------------------
374851
9857
Bằng những ngôn từ dễ hiểu, câu chuyện thực tế và ngôn ngữ phong phú cuốn sách đã truyền đến cho người đọc những thông điệp ý nghĩa trong cuộc sống. Cuốn sách giúp chúng ta nhìn có lăng kính về cuộc sống này một tích cực và lạc quan hơn. Ai trong chúng ta chắc chắn cũng sẽ gặp ít nhiều những rắc rối trong cuộc sống hàng ngày, chúng được ví như " những chiếc xe rác" nhưng cách ta nhìn nhận bản chất sự việc cũng như thái độ của chúng ta đối với từng sự việc ấy sẽ quyết định kết quả mà chúng ta nhận được. Nếu ai cũng biết kiềm chế những nóng giận tức và bỏ qua những vấn để nhỏ nhặt của mình thì cuộc sống này sẽ đẹp hơn rất nhiều!

5
100658
2016-01-27 12:53:56
--------------------------
368808
9857
Lúc mới mở hộp ra mình hơi thất vọng vì quyển sách có vẻ mỏng hơn mình nghĩ. Nhưng khi đọc trang đầu tiên mình thật sự bị thu hút. Chúng ta hay bực mình vì những chuyện nhỏ nhặt xung quanh, vô tình "gom rác" giúp người khác, xong rồi ta lại quăng chúng cho những người ta yêu quý. Thái độ vô lý đó có thể làm ta đánh mất một mối quan hệ, một người bạn, hình ảnh cá nhân trong mắt mọi người. Cuộc đời là hữu hạn, hãy sống thật vui vẻ cho xứng đáng một đời người, đừng phí thời gian để nhặt rác bên đường, hoàn toàn không đáng.
4
537271
2016-01-14 17:51:48
--------------------------
304123
9857
Ban đầu tôi cũng không hiểu vì sao tác giả lại đặt tên tác phẩm là như thế. Nhưng sau khi đọc hết mới hiểu được nó ý nghĩa như thế nào. Mọi thứ muộn phiền, những con người hay chữi rủa làm ta phiền muộn, những điều luôn khiến ta ưu sầu thì cứ xem nó như một chiếc xe rác. Mà chúng ta chẳng ai muốn để ý đến chiếc xe rác để làm gì, vì nó rất sê làm ảnh hưởng đến chúng ta. Cũng chỉ cần suy nghĩ đơn giản như thế mà tôi có thể giảm được rất nhiều muộn phiền không đáng có trong cuộc sống.
5
225624
2015-09-16 10:45:54
--------------------------
284373
9857
Tựa sách mang đến cho tôi một sự tò mò nên quyết định mua. Quyển sách mang đến một cái nhìn thoáng hơn về những vấn đề chúng ta thường xuyên gặp phải trong cuộc sống để tạo nên một tinh thần lạc quan. Tuy nhiên tác giả mở và đóng vấn đề vẫn chưa thật sự thuyết phục người đọc. Sau khi khép lại quyển sách này tôi nhận ra hình như tác giả cũng không lạc quan lắm khi xem mọi thứ khó khăn xảy ra đều là rác rưởi. Khi lấy ví dụ về chuyện bỏ lỡ buổi diễn của con mình thì lấy lý do là anh ta nhận "rác" từ đồng nghiệp và phát tán. Cá nhân tôi thấy góc nhìn này hơi vô trách nhiệm. 
3
15187
2015-08-30 21:31:46
--------------------------
206954
9857
Bài học từ chiếc xe rác đưa ra 7 cam kết vì  một cuộc sống không còn những chiếc xe rác. Những chiếc xe rác là những cảm xúc tiêu cực có thể do người khác mang đến cho ta nhưng cũng có thể do chính chúng ta làm vấy bẩn cuộc sống của mình. Để chống lại những chiếc xe rác, ta không chỉ cần dựa vào bản thân mà còn cần sự giúp đỡ, ủng hộ của người khác khi những chiếc xe rác lên đến hàng trăm hàng triệu như trường hợp của cầu thủ bóng chày Robinson. Cuốn sách có những câu rất hay : Tôi không chịu đựng những chiếc xe rác, tôi chỉ mỉm cười, vẫy chào và bỏ qua chúng. Hay là để ghét ai đó cũng cần phải học vậy sao không học để yêu thương họ? Một cuốn sách mỏng nhưng rất ý nghĩa, rất đáng đọc.
5
491545
2015-06-11 08:12:23
--------------------------
159083
9857
Đập vào mắt là một cái tên sách rất độc và lạ, "Bài học về chiếc xe rác". Mọi người ai cũng biết xe rác, nhưng xe rác mà có bài học để chúng ta học tập thì quả là một chuyện không bình thường chút nào.  Vì thế mình quyết định mua nó vào ngày 10/1/2015.
Mình là một người có thể nói là khá nhạy cảm, mọi sự thay đổi xung quanh cũng có thể ảnh hưởng đến tâm trạng của mình. do đó khi cầm cuốn sách cũng khá là thất vọng, vì trước nay toàn thích những cuốn sách dày và to, trong khi sách khá nhỏ và mỏng, nhưng không vì thế mà mình không đọc nó, từ lúc đọc hết trang đầu tiên đến trang thứ hai, từ đó, không thể nào cưỡng lại  sự thu hút, hấp dẫn từ cuốn sách. Quả là có rất nhiều bài học để mình học, trong đó cần kiểm soát cảm xúc của mình.
5
499844
2015-02-14 20:46:23
--------------------------
147049
9857
Tôi là một người cực kì hay nóng giận vì những thứ nhỏ nhặt, và cũng gặp một số vấn đề trong cuộc sống nữa! Tình cờ một hôm cô cho tôi mượn quyển sách này, nó đã giúp tôi thay đổi được phần nào suy nghĩ và hành động của mình!
Thiết kế bìa đầu tiên khá đơn giản và hơn hết là nêu lên cái chính mà quyển sách sẽ đề cập đến >>> "Chiếc xe rác". Chữ viết được trình bày rõ ràng và dễ hiểu, nội dung sách thì dựa trên các tình huống có thể thường ngày ta sẽ gặp, đưa ra tình huống để rồi cho chúng ta thấy cách giải quyết như thế nào là hợp lí, vừa giúp chúng ta cảm thấy nhẹ nhõm hơn, đỡ phải bực tức,.... Với việc trình bày sách dễ hiểu, rõ ràng, cộng với phần thông tin muốn truyền đạt tới người nghe rất là ý nghĩ, sẽ phần nào giúp chúng ta cải thiện cách ứng xử của mình. Tuy nhiên, do là sách dịch nên vẫn còn vài chỗ tôi cảm thấy chưa thoả đáng lắm (Đa phần sách dịch là thế) và các cách giải quyết vấn đề vẫn còn hơi chung chung chưa cụ thể! Nhưng với số tiền 18k bạn bỏ ra thì đây vẫn là một lựa chọn hoàn hảo cho bạn: vừa tiện lợi, vừa có thể giúp chúng ta thay đổi cách sống!
5
410224
2015-01-06 16:40:22
--------------------------
144021
9857
Mình mua cuốn sách này vì thấy lời giới thiệu đang là thứ mình cần, hơn nữa các bạn nhận xét khá chi tiết và đánh giá cuốn này hay nên mình thử trải nghiệm xem sao.
Về hình thức :
Sách nhỏ gọn, bìa láng đẹp, chữ rõ ràng, giấy bên trong màu hơi ngả vàng. Không quá trắng gây hại mắt.
Về nội dung:
Nội dung phải nói là hay, các tình huống từ đầu câu truyện là những gì mà hàng ngày cuộc sống ta đang trải qua.
Tuy nhiên, ở đây khi đọc sách các vấn đề tác giả nêu lên hầu như mình đều đã biết, ví dụ đưa ra rõ ràng nhưng cách giải quyết các tình huống không cụ thể lắm mà chỉ chung chung.
Đánh giá tổng thể 7/10
3
45157
2014-12-25 09:48:13
--------------------------
75105
9857
Tôi đọc quyến sách này chỉ trọn trong 1 tiếng đồng hồ. Thời gian rất ngắn, nhưng những gì quyển sách này mang lại cho tôi là vô cùng lớn và hữu ích.

Phần đầu thì nội dung khá quen thuộc, chủ yếu là bàn về lối hành xử sao cho đúng mực để chúng ta luôn là người văn minh, một người không dính "rác" ^^

Theo tôi thì phần sau chính là phần hay nhất trong nội dung toàn quyển sách. Cách chúng ta sống, giao tiếp được sách chỉ dạy rất kĩ. Học được cách vứt bỏ "rác rưởi" và trở thành một người "sạch sẽ" :)

Phải công nhận sách của FirstNew rất hay về giá trị sống. Và quyển sách này là một điển hình.
5
50283
2013-05-16 21:48:14
--------------------------
69954
9857
Cuốn sách được mở đầu bằng những câu chuyện đơn giản mà ai cũng phải gặp trong cuộc sống hàng ngày: bị vượt ẩu, nhân viên hàng quán khó chịu, đồng nghiệp không tốt… Thật dễ dàng để thấy chính mình trong những câu chuyên đó. Bạn sẽ làm gì? Bực tức để rồi bỏ dở công việc của chính mình, hay làm hỏng cả một ngày đẹp trời. 7 cam kết của “Bài học diệu kỳ từ chiếc xe rác” là 7 bài học giúp bạn dần vượt qua những điều khó chịu mà môi trường mang đến cho bạn trong cuộc sống: từ việc mặc kệ đi những “chiếc xe rác” đến việc tự mình xây dựng một môi trường không có “xe rác”. Tác giả muốn nhấn mạnh việc tập trung và những mục tiêu quan trông và bỏ qua những rắc rối vụn vặt sẽ giúp bạn thành công hơn.
Cuối cùng là hình ảnh mà mình tâm đắc nhất từ cuốn sách này: “Có những ngươi giống như “chiếc xe rác” vậy: Họ chứa trong mình đầy “rác rưởi” – sự thất vọng, tức giận và chán nản. Và tất nhiên họ phải tìm chỗ để trút bỏ mớ rác rưởi đó. Nếu thấy họ trút lên bạn thì bạn đừng đón nhận. Hãy mỉm cười, vẫy chào, chúc họ vui, rồi tiếp tục công việc của mình.” 
Một cuốn sách nhỏ nhưng những bài học từ đó thì không nhỏ một chút nào.

5
22494
2013-04-18 17:22:22
--------------------------
68320
9857
Bài Học Diệu Kỳ Từ Chiếc Xe Rác là một cuốn sách nhỏ bé, giản dị vô cùng nhưng đầy ý nghĩa. Thú vị từ tựa đề sách, chúng ta đã phần nào hiểu được, thông điệp nho nhỏ, rất đời thường mà rất thiết thực của cuốn sách.
Sự vật, sự việc nào quanh ta vốn dĩ đều mang những ý nghĩa nhất đinh, một chiều sâu của triết lý mà cuộc sống mang đến. Một chiếc xe rác thô kệch, lầm lũi đi thu dọn rác mỗi sáng sớm, cũng mang đến cho chúng ta những bài học hết sức đáng quý.
Cũng như sự đúng giờ của một cái xe rác, những bộn bề, stress, đủ những thứ vặt vãnh đến với chúng ta mỗi ngày, khiến chúng ta mệt mỏi, ta khó chịu, ta bực bội, ta chán nản, ta chán ghết... vô vàn những thứ cảm xúc tiêu cực, ảnh hưởng nặng nề tới tâm lý và các công việc khác, đó chính là rác rưởi của cuộc sống.
Làm thế nào để chúng ta tự biết bảo về cho mình, không để bản thân bị ảnh hưởng quá nhiều bởi hành vi và cách cư xử của người khác. Có khó không? để chúng ta có thể bỏ qua những thái độ của người khác không như chúng ta mong muốn. Câu trả lời là hoàn toàn có thể.
Phân tích một cách dễ hiểu, dẫn dắt đầy thuyết phục, David J.Pollay khẳng định việc chúng ta hoàn toàn có thể thay đổi suy nghĩ, thay đổi bản thân cho thích hợp, phù hợp với những gì chúng ta gặp phải mỗi ngày, trong mỗi hoàn cảnh và thời điểm. Để mỗi chúng ta chẳng còn là một chiếc xe rác đi nhặt nhạnh những buồn bực, khó chịu , gây cho mình thêm khó chịu và rắc rối. Hãy cứ tự tin , tập trung vào công việc của chính mình. Nếu có ai đó trút những thứ chẳng mấy hay ho  lên bạn, hãy mỉm cười và bỏ qua tất cả :) Đâu sẽ laị vào đó cả thôi.
Giá mà chúng ta xả rác ít hơn ^^
5
10984
2013-04-11 13:25:36
--------------------------
60909
9857
Trước hết phải nói là mình khá bất ngờ khi nhìn thấy cuốn sách, nó quá bé so với những gì mình nghĩ, cuốn sách bỏ túi đầu tiên của mình. May mắn, nó đã mang đến nhiều điều thú vị.
Trong cuốn sách cực gọn này là vô số những bài học mới mẻ về cách nhìn cuộc sống, qua hình ảnh chiếc xe rác. Một loạt quy tắc, cách xử lí những "khối rác" quá tải được mang đến một cách gần gũi trong các câu chuyện đời thường. Dễ hiểu và dễ áp dụng.
Nói chung, đây là một quyển sách bổ ích và tiện mang theo bên mình. Đọc đi đọc lại nhiều lần chẳng hạn.
4
65810
2013-02-26 19:40:41
--------------------------
44124
9857
Một cuốn sách nhỏ gọn bạn luôn có thể mang theo bên mình, rất hợp để bạn lấy ra nghiền ngẫm mối khi tâm trạng như "một chiếc xe rác". Mang trong mình những quy tắc, bạn dễ dàng thực hiện theo chỉ dẫn của cuốn sách, không màu mè, kiểu cách, chúng là những quy tắc đơn giản ngay lập tức giúp bạn thả lỏng tâm trạng lại và bắt đầu tiếp một ngày tuyệt vời. Mình rất ưng ý cuốn này vì tính chất mang đi bất cứ đâu cũng dễ dàng và sự dễ chịu, không kén người đọc của nó, áp dụng được cho mọi người. Hy vọng mọi người cũng sử dụng được cuốn sách này.
4
16614
2012-10-31 12:56:07
--------------------------
43333
9857
Có lẽ trong số chúng ta đã không ít thì nhiều đều đã từng trải qua cảm giác "bị ám" khi bước ra khỏi cửa..và chúng ta hầu như đều để cho cảm xúc của cảm giác tiêu cực đó ảm ảnh mình suốt ngày hôm đó..nó ảnh hưởng rất lớn đến tinh thần, chất lượng công việc của bạn thân và mọi người xung quanh ta nữa! 

Vậy câu hỏi đặt ra là làm sao "chuyển hóa" cảm giác đó?! Bằng chính sự từng trải và kinh nghiệm của mình tác giả David J.Pollay đã đưa ra những bài học vô cùng hữu ích, những cảm xúc tiêu cực đó mà ông gọi là những chiếc xe rác!  Ông sẽ chỉ cho chúng ta cách thấy cách vượt qua chúng để có được mỗi ngày đẹp tươi tràn đầy năng lượng trong cuộc sống của bạn..không có xe rác, không tự biến mình thành chiếc xe rác..và giúp mọi người thôi xả rác!
Nào bạn đã SẴN SÀNG chưa?
5
48640
2012-10-20 22:38:08
--------------------------
