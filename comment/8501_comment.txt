373236
8501
Qua phần 1 của series này người đọc có ấn tượng về nhân vật nam chính độc thân quyến rũ và hài hước nhưng sang đến phần 2 này thì mình không thấy được những phẩm chất đó mà câu chuyện cũng khá là khiên cưỡng mình cũng không thích nhân vật Isabel lắm vì nhìn chung những nhân vật nữ mà gia đình sa sút phải cố gắng sống trong nghèo khổ mà vẫn giữ được danh dự gia đình thường trở nên mạnh mẽ hoặc ương bướng quá mức, ở một mức độ nào đó sẽ làm cho câu truyện hấp dẫn hơn nhưng mặt khác lại làm cho tình yêu của 2 nhân vật thiếu tình cảm, nồng nàn, dù sao cũng nên đọc series này theo thứ tự để biết về câu chuyện của 3 anh em nhà St John
2
306081
2016-01-23 10:34:17
--------------------------
360664
8501
Chưa có sâu sắc , bí ẩn làm nên sức hút lớn dành cho mỗi người đọc , nhận ra được cái suy nghĩ sâu xa chỉ vẻn vẹn có vài điều hết sức nhỏ lẻ , không có cá tính trong lời thoại , diễn đạt tẻ nhạt chỉ có vài câu truyện để lưu giữ hồn truyện cho người đọc , không muốn đọc thêm nhân vật dù truyện có kết thúc ra sao đi nữa , mấy tác phẩm không nổi tiếng thì chỉ để thêm diện tích cho tủ sách mà không thuộc về phần giữ gìn .
2
402468
2015-12-29 16:21:46
--------------------------
278799
8501
Tôi thấy câu chuyện khá hấp dẫn đấy chứ! Tôi thích tính cách mạnh mẽ kiên cường bên trong con người Isabel nhưng ẩn giấu sau đó là khát khao muốn được yêu thương, bảo vệ. Và cái cách mà Nicholas quan tâm đến cô khiến ta trái tim ta cũng cảm thấy ấm áp, và dường như ta cũng hiểu được những giây phút đấu tranh trong suy nghĩ của Isabel khi một bên là tình yêu còn một bên là trách nhiệm với ngôi nhà Minera. Câu chuyện tình yêu nhẹ nhàng và dịu dàng ư? k , tôi không nghĩ vậy, mà hoàn toàn ngược lại, nó có phần nóng bóng và quyến rũ hơn nhiều so với những cuốn tiểu thuyết khác mà tôi từng đọc. Cuốn sách khiến tôi thích thú vì những cuộc trò chuyện giữa Nick và Isabel, tôi có thể cảm nhận được mong muốn khám phá bản chất và cuộc sống mà mỗi người dành cho đối phương. Nói chung đây là cuốn sách đáng đọc.
4
230601
2015-08-26 10:37:14
--------------------------
250719
8501
Trong 1 lần rời khỏi London để đi tìm em gái của ngài công tước Leighton. Quý ngài Nicholas St.John đã gặp cô tiểu thư Isabel Townsend, cô con gái lớn của một quý tộc đầy tai tiếng - Một cô gái dịu dàng, ấm áp. Cô đang tìm mọi cách để duy trì cuộc sống của những người sống trong ngôi nhà của mình và cô cũng tìm mọi cách để bảo vệ quyền thừa kế của em trai nhỏ của mình. Không những vậy cô còn lập ngôi nhà để chăm sóc những cô gái đang gặp khó khăn trong cuộc sống như Georgiana - Cô em gái của công tước Leighton. Những việc cô làm khiến tôi yêu thương hơn cô gái nhỏ nhắn này. Cô nhân hậu, mạnh mẽ và đầy tình yêu. Chỉ cần điều đó thôi, Quý ngài Nicholas đã không thể nào không yêu cô gái này. Và chỉ cần những điều cô làm thôi cũng đã chinh phục được trái tim của anh. Cám ơn Bách Việt đã mang đến cho tôi cảm nhận về tình yêu nhẹ nhàng và dịu dàng đến vậy. 
4
131519
2015-08-02 10:46:31
--------------------------
225114
8501
Có thể nói trong 3 cuốn của series này thì quyển này là quyển nhẹ nhàng nhất, không quá nhiều những tình huống gay cấn, cũng không có tình tiết nào gọi là phức tạp. Câu chuyện của Nicholas và Isabel diễn ra một cách ngọt ngào bởi sự đồng điệu từ hai trái tim của họ, có thể ban đầu Isabel không thích Nicholas mấy và tìm cách tránh anh nhưng bản thân cô lại không thể thoát khỏi sức hút từ anh, cả anh cũng như thế. Có thể nói đây là cặp đôi mà tôi thích nhất và đáng yêu nhất trong số 3 cặp đôi của bộ này, đơn giản vì họ đến với nhau một cách tự nguyện, dựa trên trái tim của mình, họ không đối đầu nhau nhiều, cũng không quá ngang bướng, quá lý trí. Nicholas là một chàng trai dễ thương, tốt bụng và khi đã yêu thì anh không bao giờ từ bỏ. 

Tôi thích giọng văn của Sara Maclean, bà chưa bao giờ khiến tôi phải chán nản mỗi khi đọc những tác phẩm của bà, mỗi một cuốn sách đều là một nội dung khác nhau nhưng lời văn thì luôn hấp dẫn, hài hước và lối kể chuyện rất sinh động của bà. Quả thật là thỉnh thoảng khi đọc những câu chuyện quý tộc Anh như thế này khiến tôi cảm thấy thư giãn rất nhiều.

4
41370
2015-07-09 21:00:08
--------------------------
218415
8501
Đối với cá nhân tôi, truyện 10 bí quyết chinh phục trái tim của Sarah Maclean không hay, giọng văn được, nhưng kết cấu lỏng lẻo không thuyết phục. Thật lạ lùng khi Isabel đã có thể giữ được bí mật của ngôi nhà lâu như vậy thì Nicolas lại có thể phát hiện dễ dàng trong 1-2 ngày, dân làng đó mắt có vấn đề hết sao :). Dẫu biết truyện thì phải hư cấu nhưng nó chẳng hấp dẫn mình chút nào, đến nỗi đọc được nửa cuốn thì cân nhắc có nên tốn thời gian để đọc hết truyện hay không :).  
Trước đây tôi cũng có đọc nhiều truyện pha thêm chút sex để câu khách, kích thích cảm xúc người đọc. Tuy nhiên trong truyện này, pha sex hơi đậm mà tôi chẳng cảm thấy hỗ trợ chút hiệu ứng nào cho câu chuyện, chỉ thấy nó là truyện trở nên rẻ tiền thôi. Nói chung, tôi thấy truyện này không đáng đọc và tiếc vì đã mua nó :(
2
219702
2015-06-30 16:15:53
--------------------------
96612
8501
Không biết là do tôi đọc nhiều hay thật sự theo nhận xét cá nhân của tôi, so với các tác giả dòng tiểu thuyết lãng mạn lịch sử thì giọng văn của Sarah MacLean không mấy thuyết phục được tôi. Phải công nhận nội dung tác phẩm khá thu hút nhưng khi đọc xong lại để lại rất ít dư vị.

Tuy nhiên, sau khi đọc '9 tuyệt chiêu tóm kẻ phóng đãng' thì Sarah MacLean cũng dẫn dắt người đọc tới luôn cuốn tiếp theo, cái hay của tác giả là dẫn dắt câu chuyện khá logic, dù luôn biết trước kết thúc nhưng người đọc vẫn phải tò mò theo dõi từng diễn biến tiếp theo. Tôi thích việc Nicholas phải tự đấu tranh tư tưởng với bản thân mình giữa việc giữ bí mật mục đích thật sự phải tiếp cậu Isabel và việc anh bị cô thu hút, tôi cũng rất thích tính cách mạnh mẽ và giàu lòng nhân ái của Isabel khi quyết tâm bảo vệ 'những cô gái của cô' 
'10 bí quyết chinh phục trái tim' có phần nổi trội hơn cuốn trước đó, và kết thúc của tác phẩm cũng khiến tôi rất mong chờ cuốn tiếp theo của tác giả.
3
125574
2013-10-12 20:40:58
--------------------------
96594
8501
Tác giả khắc họa tính cách nhân vật khá cá tính, mạnh mẽ một cách đáng yêu. Ngay mở đầu truyện đã lấy được lòng  đồng cảm của mình bởi nỗi tổn thương mà một tiểu thư quý tộc không nên có: bị cha ruột xem như một món tiền cược, phải gánh vác chuyện sinh tồn của gia đình, không có cơ hội cho một tình yêu, một cuộc hôn nhân đẹp. Cái cách mà Isabel xử lý tình huống khác với cách nàng nghĩ về tình huống đó, làm mình liên tưởng Isabel luôn phải tỏ ra mạnh mẽ như hổ trước mặt mọi người, nhưng thực tế nàng mỏng manh, sợ hãi như mèo nhỏ.

Truyện có phong văn mạch lạc, tình tiết lôi cuốn không gây nhàm chán, rườm rà. Điều mình thấy thú vị là những bài học nhỏ được xen kẽ ở đầu chương truyện rất tinh tế mà nếu ngẫm kỹ thì cũng đúng lắm, áp dụng được ^^, đồng thời cái cách đan xen những dòng chữ nghiêng nhỏ diễn đạt suy nghĩ của các nhân vật khiến ta hiểu và cảm nhận được nội tâm của họ ẩn sau những lời nói, lời kể bình thản giản dị.
3
168270
2013-10-12 14:03:03
--------------------------
