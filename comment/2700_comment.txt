404168
2700
Đọc xong cuốn sách mình vẫn muốn đọc lại lần nữa. Mạch truyện dễ theo dõi, khắc hoạ số phận của một người con gái xinh đẹp, giỏi giang, hiền lành lấy phải một người chồng già nua, xấu xí, có mẹ chồng và em chồng ác độc, cay nghiệt. Nội dung lôi cuốn, nhiều tiết tấu hấp dẫn, cho người đọc trải nghiệm nhiều cung bậc cảm xúc.  
Mình đã học hỏi được rất nhiều điều từ cuốn sách này, nhất là đức tính hy sinh hết mực vì chồng, vì con của nhân vật chính. Cô đã cảm hoá được gia đình chồng và nhận được nhiều sự nể phục từ gia đình, họ hàng, đồng nghiệp.
Sách đẹp, độ dày vừa phải, giá tốt.
5
275547
2016-03-24 19:50:18
--------------------------
