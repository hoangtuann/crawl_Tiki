514921
4937
Tiki giao hàng nhanh chóng, đóng hộp cẩn thận. Nhưng khi nhận hàng thì hơi bị thất vọng chút vì góc trên gáy sách bị sờn.
3
27943
2017-01-24 09:22:14
--------------------------
463091
4937
Tôi là thày tướng số, một quyển sách lạ lùng nhất mình từng đọc từ trước tới giờ, mình đọc hết hai phần ba tập một trên mạng mới quyết định mua, vì thấy hay quá. Đọc sách, mình cũng hiểu rõ hơn về cái gọi là Bát tự rồi đến những ngón nghề tinh vi của các a bảo. Nhưng cái hay ở chỗ tác giả đã khéo léo lồng ghép nhưng suy ngẫm, những lời khuyên về việc ăn ở thiện đức ở đời, cũng là cả một thời kì Trung Hoa đầy biến động, thật giả khó phân, nhân dân khổ cực. Mình vẫn thích truyện có lồng ghép yếu tố lịch sử như thế này.
5
716061
2016-06-29 07:37:25
--------------------------
419895
4937
Tập truyện này xoay quanh cuộc đời nhân vật Tổ Gia, một ông trùm chuyên dùng thuật bói toán, phong thủy để lừa đảo, không chỉ là với dân đen mà còn có cả những quan chức cấp cao. Thật sự khâm phục người xưa, nghĩ được những chiêu trò trát phi hay để lừa người.
Tập 1 chủ yếu giới thiệu về hoàn cảnh xuất thân, cũng như những sự kiện tiêu biểu trong lúc Tổ Gia lên năm quyền. Thông qua những câu chuyện, tác giả cũng cho chúng ta một số lời khuyên về thuật quản trị, thuật lấy lòng người. 

5
125876
2016-04-22 10:33:19
--------------------------
403146
4937
Đây là cuốn sách mà mình đã thích ngay từ lời tựa. Đã đọc cuốn này trong suốt 1 năm (thực ra thì đọc đi đọc lại), có nhiều quan niệm nhân sinh quan rất hay. Bói toán, tướng số hay phong thủy cũng đều giống nhau ở chỗ dựa vào tâm lý của người muốn xem mà bói, người càng tham càng dễ bị lừa, càng mang trong mình những sân si đố kị thì càng dễ bị kẻ lừa bịp dắt mũi. 
Là câu chuyện có soái ca nhưng lại không có tình yêu nào cả, là một người vĩ đại, đứng đầu và gánh trên mình biết bao trách nhiệm - những trưởng môn của các phái đều ít nhiều phải dẹp đi tình cảm nam nữ, chỉ một phút yếu lòng của họ có thể đưa cả môn phái bị diệt vong. Những con người phi thường thì hình như không bao giờ có được một cuộc đời bình thường.
Cuốn sách nên đọc đối với những bạn thích xem bói, hoặc những bạn tò mò về tướng số, dịch chi...
5
318230
2016-03-23 11:57:22
--------------------------
394359
4937
Nhìn sơ qua tựa đề " Tôi là thầy tướng số" thì không ít người đọc nghĩ rằng đây là 1 cuốn sách dạy xem tướng, bói toán...Nhưng hãy thử cầm trong tay cuốn sách này, đọc chừng vài trang là bạn sẽ có cái nhìn khác về nó.
Nội dung xoay quanh phái Giang Tướng, chuyên sử dụng các thủ thuật, tâm lý, kỹ xảo ... để lừa người xem với các mục đích khác nhau. Bên cạnh đó quyển sách này còn giúp bạn hiểu thêm về các giai đoạn lịch sử, những biến cố thăng trầm, ...Tuy là Phái Giang Tướng nhưng nó cũng có những quy định, tổ chức, quy mô mà người trong cuộc phải tuân theo.
Nhược: Gần cuối cuốn 1 có những lỗi chính tả, chữ bì nhòe
Đọc thử đi bạn nhé, không thấy phí đâu.
4
1153584
2016-03-10 11:15:22
--------------------------
373579
4937
Tôi hiếm khi đọc dạng truyện hoặc tiểu thuyết, nhưng cuốn sách này lôi cuốn ngay từ những chương đầu tiên. Những câu chuyện thủ đoạn "trát phi", "dàn cục" trong cuốn sách tưởng như chỉ trong truyện mà nhiều người không biết chính mình cũng đang bị lừa. Đọc xong cuốn sách chắc hẳn nhiều người sẽ biết được tại sao các thầy bói đoán trúng vậy. Căn bản là chúng ta bị bắt bài, chính vì tò mò về tương lai hoặc đang gặp chuyện mới đi xem bói. Đây chính là cơ hội cho bọn lừa đảo kiếm tiền. Nhưng cũng có những thầy tướng số thực sự, vì Kinh Dịch, Ngũ Hành là một môn khoa học cổ xưa được đúc kết qua hàng ngàn năm. Nếu nó nhảm nhí thì không tồn tại đến tận thế kỷ 21 này.
5
762054
2016-01-24 08:13:20
--------------------------
326222
4937
Lúc đầu mới mua về, mình nghĩ cuốn sách sẽ nói nhiều về tướng số, nhưng không, đây là cuốn sách nói về triết lý nhân sinh. Một cuốn sách chứa đựng rất nhiều bài học rút ra từ kinh nghiệm sống của tác giả, của những nhà triết học bình dân phương Đông. Những bài học về đối nhân xử thế, về cách làm người được lồng ghép khéo léo trong những lời kể gần gũi của nhân vật.
Một điểm trừ nhỏ cho tác phẩm này là tác giả đôi lúc động vào chính trị không đáng có. Những đoạn văn ca ngợi Đảng Cộng sản TQ và những đoạn văn viết như sách lịch sử TQ hầu như không cần thiết. Bởi khi bỏ những đoạn văn này ra không những không làm truyện mất hay mà ngược lại, chúng làm cho mạch truyện bị ngắt quãng không đáng có. Nhưng nói chung là mình vẫn thích! :))
5
80332
2015-10-25 09:15:55
--------------------------
281149
4937
Mình mua quyển này khá lâu rồi, nhưng mới đọc xong, vì quá nhiều việc chứ không phải quyển này không cuốn hút. Đầu tiên phải nói là bìa sách rất đẹp, có hồn, lâu lâu hơi giật mình vì nhìn thực quá :). Sách nhẹ, trang giấy mịn, nói chung là cầm tay rất tiện, vì không nặng. Nội dung sách rất hay, nhiều đoạn rất dí dỏm, mình thích cách kể chuyện của tác giả, cảm giác rất gần gũi, như là ai đó đang kể lại cho nghe vậy. Giữa một trời sách Trung Quốc toàn ngôn tình và đam mỹ thì quyển sách này như một món ăn vừa lạ vừa quen. Rất đáng đọc.
4
311797
2015-08-28 10:20:08
--------------------------
263989
4937
Phàm là mang chuyện đi xem bói , nhất định chuyện đó không suôn sẻ.
Chủ đề thực sự mới, không phải nói về cách bói toán nhân quả, mà là một loại lừa bịp, hiệu quả đến nỗi người ta chỉ có thể tin tưởng vì được đúc kết bởi kinh nghiệm nghìn năm, cách thu thập dữ liệu, phương pháp nghiên cứu, chuẩn bị kỹ càng. 
Nhưng không phải là phê phán, mà đọc để thấy trong đó, trong cái thế giới đầy mưu mô lừa bịp, vẫn có tình người, tình huynh đệ và trí tuệ sâu sắc.
4
85618
2015-08-12 22:48:42
--------------------------
260975
4937
Đó là điều mà mình và mẹ mình phải thốt lên khi đọc quỷên này. Mẹ mình nói: Sống hơn 45 tuổi rồi mà mới biết được trên đời này họ mưu mô, lừa lọc nhau đến thế. Còn đối với mình, đó là những bài học đầu đời để mình đi trên con đường này. Sống trên đời là phải có các mối quan hệ và "sự ngu dốt đều phải trả giá bằng tiền". Các bạn mua và đọc đi, mình đảm bảo sẽ hay và cuốn hút, bạn sẽ không bỏ sót 1 trang nào đâu. :). Hy vọng là tập 3 sẽ sớm ra mắt và mình tin là Lão Tổ còn sống. :)
5
319501
2015-08-10 20:08:07
--------------------------
248023
4937
Mình đọc xong bộ "Tôi là thầy tướng số" và rất yêu thích nội dung tác phẩm. Sách là tổng hợp giữa những dữ kiện khoa học và điều thần bí. Có những chi tiết rất đắt về nhân văn, về nghệ thuật sử dụng từ ngữ, xây dựng tính cách nhân vật và sẽ giải thích cho bạn một số điều bí ẩn như "Cương thi", "Mèo đen chạy qua xác chết". Đồng thời, sách cũng mang những điều khiến bản thân mình phải suy ngẫm về cuộc sống, cách nhìn nhận vấn đề. Kết thúc tập 2 hơi lửng lơ, không biết sẽ có tập 3 không. Nếu có, mình chắc chắn sẽ mua. 
5
169228
2015-07-30 17:08:20
--------------------------
224786
4937
Có thể nói cái nghề coi tướng số manh nha này không hề xa lạ gì với mỗi người chúng ta, và đặc biệt là ở Trung Hoa xưa. Đọc Tôi Là Thầy Tướng Số, bản thân tôi lại không trách những người thầy tướng số chỉ vì miếng cơm manh áo, kiếm kế sinh nhai mà đi làm công việc lừa người dối ta, mà tôi lại cảm thấy chính vì có những con người tin vào những điều mê tín dị đoan ấy, tin vào lời người khác mà lại nghi ngờ chính cuộc sống của mình. Thực chất, đã là con người, ai mà tham, sân, si nhưng lại không nên vì nó mà lừa gạt kẻ khác, đổi trắng thay đen như thế. Có lẽ, cũng chỉ vì hai chữ MƯU SINH.
Nói chung là bìa, giấy khá bắt mắt và đẹp. Ban đầu nhìn bìa cứ nghĩ truyện thiên về tính hài hước, châm chước nhưng đọc rồi thì thấy khá là nhiều suy tư, tự ngẫm về cuộc đời mình. 
4
183389
2015-07-09 11:01:17
--------------------------
209389
4937
Sách là câu chuyện có thực hay không mình không biết, tuy nhiên đọc nhiều khi thấy nó quái dị và kinh hơn cả truyện ma.
Đọc mới thấy người sống đáng sợ, cũng vì tiền vì kế sinh nhai mà không từ thủ đoạn để lừa đảo.
Đọc mới thấy cái khổ tâm của người trong cuộc, nhiều khi muốn dứt ra mà không dứt được
Chỉ bằng những mẩu truyện nhỏ được kể lại mà ta thấy được các loại chiêu trò các thầy tướng số sử dụng hồi xưa khi đầu óc con người còn chưa được văn minh như bây giờ. Một quyển sách đặc biệt hay
5
324739
2015-06-17 14:26:54
--------------------------
201941
4937
Tôi rất ít đọc sách dạng truyện kể hay tiểu thuyết, tuy nhiên tôi đã bị thuyết phục mua cuốn sách này khi chỉ đọc được chừng 5 trang. Tác giả viết rất lôi cuốn, kể về một Trung Hoa Dân Quốc đầy tệ nạn mà nổi nhất là tệ mê tín dị đoan. Phái Giang Tướng tụ họp những tay lừa đảo hành nghề Thầy tướng số, tuy nhiên đằng sau những gương mặt vẻ như đạo mạo hiền lành đó là cả một bầu trời các chiêu đòn tâm lý, các âm mưu, toan tính. Một cậu ấm danh gia thế phiệt bỗng chốc phải lưu lạc phương trời rồi duyên cớ lại trở thành Trưởng môn phái Mộc Tử Liên trong Giang Tướng. Bìa sách đẹp, giấy tốt, dịch hay và "trơn tru". Đó là cảm nhận của mình.
5
564333
2015-05-28 15:48:52
--------------------------
163140
4937
Bìa của cuốn sách này đẹp ( ý kiến của mình thôi nhé: vẽ mặt ông thầy nhìn láu quá), nhẹ, mềm và khá là bền. Chất liệu giấy: khi cầm thì mình thấy cứng nhưng mà rất nhẹ, màu giấy sáng, bền và ít bị ẩm nên dễ dùng, dễ đọc và dễ bảo quản nữa.
Nội dung nói về cuộc đời thấy tướng số tên Tổ Gia- người Trung Quốc từ nhỏ đã biết cách bói toán, xem tinh tượng, dự đoán được tương lai nên mọi người ai cũng kính nể, 15 tuổi thì bắt đầu đi phiêu bạt giang hồ khắp đây để gặp người tài, tìm kẻ sĩ và bói như là thần cơ diệu toán nhưng lại không bói được cho chính bản thân, nhờ điểm này cuốn sách vừa hay vừa thể hiện cái châm biếm sâu sắc xã hội bấy giờ.
5
525063
2015-03-04 10:50:57
--------------------------
