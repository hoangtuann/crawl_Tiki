566029
4736
1 cuốn sách rất hay, người tổng hợp viết nên cuốn sách này có kiến thức cả đông y lẫn tây y. Đã giải thích được nguyên nhân của căn bệnh huyết áp, huyết áp cao dễ đứt mạch máu; còn huyết áp thấp thì dễ dẫn tới các căn bệnh ung thư, nặng nhất là ung thư máu, cách cấp cứu kịp thời đối với người bị huyết áp đột nhiên tăng vọt....
Cách giải thích rõ ràng, dễ hiểu, một lượng kiến thức bổ ích.
Cách chữa của thầy mở ra hướng chữa mới cho các bệnh nhân bị ung thư, theo phương châm "mọi người tự cứu lấy mình, đừng trông chờ vào người khác, đừng trông chờ vào những tha lực bên ngoài".
Với thầy Đỗ Đức Ngọc "ung thư" không còn là một điều gì ghê gớm, và thầy chữa một cách rất nhẹ nhàng, đơn giản, chữa vào đúng gốc của bệnh, ko đau đớn, nhưng đòi hỏi người bệnh phải kiên trì, và đến với môn y học bổ sung này cũng phải có một cái duyên nữa.
Cách chữa ko tốn tiền, ko gây đau đớn, nhưng đòi hỏi phải có sự "kiên trì". THẾ THÌ TẠI SAO LẠI KHÔNG THỬ?
Con xin tri ân tài năng và nhất là tấm lòng của thầy. Một cuốn sách rất đáng để đọc.
5
1995897
2017-04-06 15:45:53
--------------------------
