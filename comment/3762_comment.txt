379791
3762
Vì tựa sách là " tuyển tập 162 Bài Văn chọn lọc" nên mình cứ nghĩ là nó sẽ có những bài văn mẫu. Nhưng sau khi mở sách ra và đọc thì không phải trong nó là những bài văn  mà là các bài tập liên quan đến các bài học trong sách giáo khoa (bài tập trắc nghiệm) và bên cạnh cũng có phần Tự Luận là những đề văn cho học sinh tự làm, phía sau là phần đáp án giúp cho chúng ta có thể kiểm tra lại kết quả bài làm của mình,có thể tìm những thiếu sót trong bài văn mình làm để bổ sung thêm từ phần "Yêu cầu và một số định hướng khi làm bài". Đây là một quyển sách khá hay, nó giúp cho mình học Ngữ văn tốt hơn. 1 ưu điểm nữa của sách là giấy màu sẫm giúp bảo vệ mắt, không bị chóa mắt dưới ánh sáng đèn.  
5
736788
2016-02-12 17:15:41
--------------------------
283132
3762
Lúc đầu, đọc qua tên sách, mình cứ tưởng bên trong là những bài làm văn mẫu, nhưng khi cầm quyển sách trên tay, lật ra mới thấy bên trong được chia thành 2 phần, phần đầu là bài tập dạng đề trắc nghiệm, phần sau là đề làm văn. Cuối sách có phần đáp án và hướng dẫn làm văn cho từng đề. Ban đầu mình cũng hơi hoang mang, nhưng sau một thời gian đọc cũng thấy rất hay, phù hợp với chương trình học, ngoài ra mình còn được rèn luyện khả năng tư duy khi viết văn.
4
635412
2015-08-29 18:56:40
--------------------------
