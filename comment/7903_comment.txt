577921
7903
Cho dù chưa đọc hay đã đọc thì ai cũng sẽ cho tiki điểm cooingj vì bao bọc cẩn thận, sách đẹp, mới, giao hàng còn hơi lục đục một tí nhưng nhìn chung tốt cực, hộp đựng cũng lịch sự, thông tin đầy đủ. Nội dung thì dễ thương, cảm động. Các bạn nên mua sách tại tiki
5
5195941
2017-04-19 21:05:35
--------------------------
535869
7903
Đây là cuốn sách đã làm tôi bật khóc. Từng dòng văn câu chữ của chị GG đều mang đến sự cuốn hút kì lạ. Có một cảm giác mà tôi luôn tìm kiếm bấy lâu nay: được cảm nhận sự bình yên của tuổi thơ bé. Tôi tìm thấy hình ảnh của mình trong "Con gái có điều bí mật". Tôi nhận ra bấy lâu nay tôi đã quá vô tâm. Tôi dường như không biết rằng mỗi ngày trôi qua cha mẹ lại ngày một già đi. Tôi chợt thấy mình đã lấy đi của mẹ, của ba rất nhiều, rất nhiều. Tôi hối hận vì những lần không nghe lời. Tôi tiếc nuối vì đã không dám nói: Ba mẹ ơi, con yêu ba mẹ nhiều lắm!  Cảm ơn chị GG vì " Con gái có điều bí mật", cảm ơn chị vì đã dạy em trân trọng từng giây phút ấm áp bên gia đình thân yêu!                                                                                                                                                         
5
2030422
2017-03-05 13:41:44
--------------------------
480554
7903
Bìa sách và nội dung đều rất  dễ thương nè. Có những câu chuyện ta đọc thấy được bản thân mình trong đó. Chợt nhớ về ba mẹ rồi bật khóc. Những lúc lớn tiếng với ba mẹ hay những lần ba mẹ lo lắng khi bị ốm nặng. Kí ức và kỉ niệm như ùa về. Mình đã sống thật quá ích kỉ. Khi đọc xong bỗng rùng mình một cái, nổi hết cả da gà trong cái thời nắng nóng này. Rồi im lặng nghĩ vẫn vơ về ba mẹ, tự trách bản thân. Liệu bây giờ có quá muộn không nhỉ?
5
1552684
2016-08-18 18:22:26
--------------------------
470597
7903
Con Gái có nhiều bí mật của tác giả Đỗ Nguyễn Phương Giao , tác giả dường như không tên tuổi mấy ! mới đầu đọc tựa chỉ hiểu sao hiểu nhầm từ CON GÁI thành GÁI- TRAI , sau này mua mới biết là CON gái tức con Gái bố :) ! thật! người ta bảo " con gái là tình nhân kiếp trước của cha" nên kiếp này được cưng chiều hết mực! thú thực là chưa đọc hết quyển nhưng vì Tiki sắp hết hạng được tikixu nên đành cmt hehe , nhưng quyển sách thực sự ý nghĩa , mình và ba mình rất nhiều xích mích ,đụng tí là cãi , nhưng ba thương mình lắm , mình cảm nhận được , đòi gì ,muốn gì ,miệng tuy chửi nhưng lại tìm cách mua cho mình :) còn nhớ lúc mua máy tính ,khóc nằng nặc , sau nghe mẹ khuyên giải ko đòi nữa , nhưng khoảng 2 tuần sau ,máy tính từ đâu vác về , ba lại còn bảo đc ngta cho ! đâu mà trùng hợp vậy ? mẹ mình sau này mới bảo đó là ba mượn tiền mua cho mình :) mình đấy gia đình thực sự rất khó khăn ,10tr không phải món tiền nhỏ , đến 1 ly cà phê ba còn tiếc ,thế mà....... thôi , không nói nữa :)) mua đi ,hay lắm !! 
5
1158712
2016-07-07 18:26:11
--------------------------
466729
7903
Trước tiên mình rất ấn tượng với sách bởi bề ngoài sách có bìa rất đẹp nhìn rất dễ thương và bắt mắt,chất lượng giấy tốt,chữ in đẹp,nói chung mình rất ưng ý,cũng rất hài lòng về dịch vụ giao hàng và chất lượng sách của Tiki.Về nội dung,cuốn sách như một cuốn nhật kí ,như lời giãi bày tâm sự sâu lắng thầm kín nhất của con gái tới ba mẹ.Mình đọc mà cảm nhận như chính cảm xúc và hình ảnh của chính bản thân mình trong đó,thấy hiểu và thương bố mẹ nhiều hơn.Cuốn sách khơi gợi lại những điều tốt đẹp trong cuộc sống ,những kỉ niệm mà ở đó luôn có ba mẹ gắn bó với chúng ta.Để rồi đọc mỗi trang sách như có dịp dành thời gian để bản thân ngẫm nghĩ nhiều hơn,,,thấu hiểu nhiều hơn,,,
5
1234963
2016-07-02 20:24:12
--------------------------
466090
7903
''Con gái có điều bí mật" khiến tối ấn tượng với trang bìa rất nhã nhặn, dễ thương. Cuốn sách không quá dài nhưng mang trong mình những thông điệp hết sức ý nghĩa với người đọc. Qua quyển sách làm ta có động lực nói lên những câu yêu thương mà đáng lẽ ta phải nói từ lâu. Cuộc sống không phải là quá dài để t có thể mãi bên cạnh những người ta yêu thương thế nên không có thời gian để ta lãng phí nhưng có rất nhiều khoảnh khắc cần ta trân trọng biết nhường nào. Tuy nội dung và ý tưởng khá hay nhưng lối viết lại kém thu hút, khiến người đọc dễ nhàm chán nhưng bù lại thiết kế trang sách rất bắt mắt.
3
376139
2016-07-01 21:00:59
--------------------------
455813
7903
Quyển sách được in bìa rất đẹp và bắt mắt. Cảm ơn tác giả rất nhiều vì Con Gái Có Điều Bí Mật!  Không hiểu sao khi đọc luôn có cảm giác bồi hồi và xúc động.Một thứ cảm xúc mới mẻ. Cuốn sách này tuy được xuất bản khá lâu nhưng vẫn không bao giờ bị quên lãng bởi thứ cảm xúc mà nó mang lại. Những tình cảm mộc mạc, chân thành, một tình yêu thương vô bờ bến được gửi gắm trong từng câu từng chữ đã chạm đến trái tim người đọc như chính cô tác giả luôn tràn đầy sự yêu thương vậy Và nhờ đó mà  tôi đã can đảm hơn để nói: "Con yêu ba mẹ. 
5
1210491
2016-06-22 21:32:14
--------------------------
436720
7903
Một quyển sách rất hay và cảm động. Cầm quyển sách trên tay, ấn tượng đầu tiên là bìa đơn giản và đẹp. Xuyên suốt quyển sách là những mẩu chuyện nho nhỏ thấm đẫm tình yêu thương thắm thiết của gia đình chị Nguyễn Đỗ Phương Giao. Lối viết văn chân thật dễ chạm đến được cảm xúc của người đọc. Đọc xong mới thấy bản thân vô tâm quá chừng, chưa bao giờ làm được gì cho ba mẹ cả, một lời yêu thương chưa dám nói. Quyển sách đã cho em nhiều bài học. Cảm ơn chị Nguyễn Đỗ Phương Giao nhé! 
Muốn nói yêu thương nhưng sao khó quá! 
4
1154836
2016-05-27 08:24:14
--------------------------
432298
7903
Sách rất hay và đáng được mua ạ.Những câu chuyện trong sách rất giản dị,gần gũi nhưng lại vô cùng  cảm động Tác giả viết hay như vậy có lẽ vì đây là những câu văn xuất phát từ tình cảm chân thật của chị ấy.Cách viết của tác giả rất nhẹ nhàng,dễ đi vào lòng người,làm tôi thấy rất yên bình.Thật sự thì tôi không mong đợi nhiều,nhưng cuốn sách này hay thật.Tui không bao giờ hối tiếc vì đã mua nó luôn.Tuy nhiên bìa sách còn khá mỏng,với hình minh họa trong sách rất mờ và cũng không cần thiết cho lắm.Mong tác giả nếu còn tái bản sẽ chú ý hơn.Rất mong chờ những tác phẩm tiếp theo của chị !
4
616034
2016-05-18 21:44:21
--------------------------
429891
7903
Cuốn sách "Cọn gái có điều bí mật"của tác giả Nguyễn Đỗ Phương Giao rất hay và cảm động.Nội dung quyển sách nói về một bức thư và những kỉ niệm ngày xưa  của cô con gái nhỏ với bố.Nội dung thì khá hay.Tuy nhiên bìa sách còn mỏng dễ bị bẩn và cong.Chất liệu giay rất tốt.Trong sách còn có những hình ảnh rất đẹp.Cảm ơn tác giả Nguyễn Đỗ Phương Giao đã cho ra đời một quyển sách rất hay.Cảm ơn tiki đã bày bán trên trang web của mình một cuốn sách rất hay và ý nghĩa.
5
1294315
2016-05-14 08:06:34
--------------------------
427176
7903
Mình không quá ấn tượng về quyển sách này nên không hề trông mong nhưng nội dung mình nhận được lại thật sự quá sức tưởng tượng! Rất hay và cảm động!

Đó là những câu chuyện giản dị, đơn giản trong một gia đình nhỏ. Là tình cảm của con người ruột thịt máu mủ với nhau, là những tình cảm chân thành nhất họ dành cho nhau và thật đáng trân trọng. Đọc mà tôi không ngờ rằng nước mắt mình đã rơi từ khi nào, tôi nhớ đến những lần ba mẹ chăm sóc và cưng chiều tôi,.. Quyển sách chân thật và gần gũi đến mức tôi đã hoàn toàn cuốn vào nó!

Đây là một quyển sách cảm động, đáng để đọc và hiểu rõ hơn về những tình cảm trong gia đình.
5
256661
2016-05-08 15:39:16
--------------------------
423507
7903
Có những chuyện người ta cứ để mãi trong lòng không dám nói ra. Ước gì bé mãi không lớn, tâm hồn mãi là của một đứa trẻ vô tư, vui sẽ cười, buồn sẽ khóc, thoải mái nói những lời yêu thương. Lớn dần, có những khoảng cách vô hình mà dù có muốn xóa bỏ đi cũng không thể được, lời yêu thương chỉ muốn thổ lộ ra nhưng cứ bị tắc đường ở ngã con tim. Mỗi người có một cách thể hiện tình cảm khác nhau. Mình chưa đủ dũng cảm để nói trực tiếp lời thương với ba mẹ, nhưng sẽ cố gắng để ba mẹ luôn mỉm cười.
4
362023
2016-04-29 14:34:03
--------------------------
402648
7903
Tiki mới giao hàng chiều hôm qua và mình đã dành nguyện một buổi tối để đọc hết cuốn sách. Đọc mà xúc động ,bồi hồi quá. Con gái có điều bí mật làm mình vừa đọc mà vừa nhớ về gia đình, nhớ ba mẹ, anh hai và cũng nhớ thật nhiều về tuổi thơ trong trẻo, về những ngày tháng cũ quá dỗi bình yên ấy.
Cảm ơn chị Giao Giao. Chị đã giúp em luôn nhớ rằng mình có một gia đình tuyệt vời như thế nào, yêu thương, tin tưởng mình như thế nào. Em thấy mình may mắn và hạnh phúc quá.
5
715918
2016-03-22 18:16:47
--------------------------
386561
7903
Tôi là một cô gái, và khi đọc tựa sách này, tôi đã rất tò mò, liệu những gì trong sách viết có giống mình không? Mà vốn là, con gái có hàng tỉ tỉ bí mất, chưa chắc là tác giả đã nói đúng. Nhưng khi đọc sách, tất cả không như tôi tưởng tượng, Một cuốn sách khiến tôi phải trầm mình suy nghĩ, và đôi khi là chợt bật khóc. Một cuốn sách khiến bạn phải dừng tất cả mọi thứ để suy nghĩ rằng "Mình đã hiếu thảo vói ba mẹ mình chưa?". Không chỉ có con gái, mà chắc hẳn con trai cũng sẽ có. Hãy mua quyển sách này, và bạn sẽ biết được bạn yêu ba mẹ đến nhường nào.
4
211114
2016-02-25 21:09:21
--------------------------
364516
7903
Điều mình thích nhất trong quyển sách này là  tác giả Phương Giao đã sử dụng cách xưng hô và  câu chữ rất nhẹ nhàng, gần gũi, và tình cảm. Những bài viết của tác giả tuy chỉ xoay quanh những điều gần gũi, giản đơn trong gia đình, giữa bố và con gái, giữa chị và em trai, nhưng khiến mình vỡ ra khá nhiều điều và tìm thấy chính mình trong những cảm xúc đẹp đẽ nhưng khó diễn tả ấy. Bỡi lẽ có những yêu thương rất khó diễn tả thành lời, đặc biệt là yêu thương dành cho những người xung quanh và rất gần gũi với mình. 
4
520494
2016-01-06 12:49:53
--------------------------
360314
7903
Cuộc sống xô bồ tấp nập không dừng lại cho đến khi tôi cầm trên tay quyển sách này. Nước mắt tôi đã rơi từ lời tựa đầu tiên với tất cả kỷ niệm dành cho cha mình.
Một cuốn sách giá trị để con người ta sống chậm lại, nhớ và nghĩ nhiều hơn về những người thương yêu, và cảm ơn cuộc sống vì họ vẫn còn ngay bên mình!
Nhìn qua lăng kính "con gái có điều bí mật", thương lắm Giao Giao và từng câu chuyện của chị. Những hình ảnh nhỏ xíu, ngộ nghĩnh và ngây ngô trong sách cũng đủ đánh thức cái ngôn ngữ yêu thương chưa đong đầy trong lòng!
Cảm ơn, cảm ơn, cảm ơn <3  
5
917759
2015-12-28 22:45:57
--------------------------
337802
7903
Cuốn sách thật sự rất cảm động, cảm ơn chị - Nguyễn Đỗ Phương Giao rất nhiều ạ. (Cho phép em được xưng hô như vậy hihi)

Thật sự lúc mua cuốn sách này em chưa hề đọc trước nội dung mà là thông qua lời giới thiệu của một người bạn, và lúc đọc tiêu đề em cũng không hề nghĩ được rằng cuốn sách thật sự ý nghĩa như vậy, em cứ nghĩ trong đầu chắc cuốn sách này viết về những điều mà bọn con gái vẫn thường nói với nhau như những cuốn sách viết cho tuổi teen khác, nhưng ngay từ câu chuyện đầu tiên em biết là ý nghĩ của em hoàn toàn ấu trĩ, trẻ con.

Cám ơn chị đã cho em những giây phút được trở về với tuổi thơ của mình, cho em nhận ra rằng lâu nay em cũng đã chưa nói ra được những câu yêu thương với Bố, Mẹ và Gia đình mình, hy vọng sau khi đọc xong cuốn sách của chị em có thể có đủ dũng cảm để nói ra những điều nhỏ bé đó.
5
862765
2015-11-14 19:19:14
--------------------------
330687
7903
lúc đặt mua sách, cứ nghĩ trong lòng đây chắc là quyển tản văn về tình cảm nam nữ, về những suy nghĩ của những cô gái biết yêu. Cầm quyển sách khá mỏng, mình cũng hơi hụt hẫng. Nhưng lật những trang sách đầu tiên, mình biết mình không mua nhầm tí nào :) đó là từng câu chuyện nhỏ rất nhỏ, nhưng ý nghĩ lại to thật là to. Cảm giác như câu chuyện kể về cuộc đời của chính mình, từ khi sinh ra đến lúc trưởng thành, trang sách nào cũng có ba mẹ ở đó, có em trai ở đó. Ngẫm kỹ lại, nhiều người đã đến và đi trong đời, duy chỉ có gia đình là không bao giờ quay lưng, dù cho bất kỳ điều gì xảy ra đi nữa. Cảm ơn tác giả, đã giúp mình tìm lại những cảm xúc của lần đầu xa nhà, mà bấy lâu trong cuộc sống bận rộn mình bỏ quên mất!
5
463997
2015-11-03 07:37:44
--------------------------
308268
7903
Sách gồm những mẩu truyện nhỏ kể những ký ức nho nhỏ của tác giả về gia đình. Những kỷ niệm nhỏ về chương trình riêng dành tặng sinh nhật của bố, về bữa cơm đầu tiên con gái nấu cho gia đình ăn sau hai năm đi học xa tràn đầy tình cảm. Những việc nho nhỏ xảy ra, những suy tư của con gái về bố mẹ được cô gái nhỏ viết ra mộc mạc nhưng ngọt ngào. Đọc sách cảm nhận được tình yêu lan tỏa tràn ngập trong gia đình nhỏ đáng yêu đó. PGiao thật may mắn đã có và được sống trong gia đình như vậy. Sách mỏng thôi nên đọc rất nhanh. Đọc xong rồi mà những cảm xúc ngọt ngào vẫn vướng vít theo người đọc. Nếu sách in chữ to hơn thì rất hoàn hảo.
4
43129
2015-09-18 16:18:13
--------------------------
306074
7903
Ban đầu mình còn tưởng "Con gái có điều bí mật" là sách về tình nam nữ bình thường. Nhưng khi đọc xong lại thấy hóa ra cuốn sách nói về tình cảm cha con, về đứa con gái và những điều bí mật mà người bố không sao có thế biết được. Hay có thể nói đúng hơn là tình của cô con gái dành cho người bố nhưng không thể hiện qua lời nói mà giữ thành những bí mật nho nhỏ của riêng mình. Nó cũng rất đúng trong xã hội hiện nay. Ai cũng yêu thương bố mẹ của mình, nhưng để thổ lộ ra ngoài thật khó và còn ngại ngùng. Tác giả thật sự đã viết ra được một tác phẩm quá tuyệt vời, từng câu từng chữ đều chứa đựng yêu thương đối với gia đình mình. Cuốn sách rất hay, sâu sắc và ý nghĩa. 
4
46651
2015-09-17 12:35:31
--------------------------
275777
7903
Ban đầu mình nhầm đây là một quyển sách viết về chuyện con gái – con trai, sau đó đọc giới thiệu mới biết. Và vì mình cũng là con gái nên đọc sách thấy đồng cảm vô cùng! Từ những kỷ niệm lúc bé đến những trăn trở của cô con gái khi thấy cha mẹ đầu lấm tấm sợi bạc... Cách viết không cầu kỳ, trái lại rất đơn giản nhưng cô gái chắc chắn rất yêu gia đình mới có thể đem tình cảm ẩn vào mỗi câu từ như thế. Mỗi bức thư nhỏ là một trang ký ức của người viết, cũng là một đoạn tuổi thơ của hầu hết những đứa con trong gia đình, vui vẻ, đầm ấm, hạnh phúc… Nếu bạn là người hay ngần ngại nói lên những lời yêu thương với cha mẹ, hãy thử tặng cha mẹ bạn một quyển sách này xem, chắc chắn đó sẽ là một món quà đầy ý nghĩa!
5
118852
2015-08-23 10:53:37
--------------------------
273229
7903
"Con gái có điều bí mật" là cuốn sách đầu tiên của nhà văn Nguyễn Đỗ Phương Giao mà mình từng đọc. Đây thực sự là cuốn sách rất hay. Giọng văn rất chân thực và giản dị. Cuốn sách nói về lòng biết ơn sự trân trọng về những điều nhỏ nhặt trong gia đình. Đó là nơi chúng ta sinh ra và lớn lên trong tình yêu thương đùm bọc của cha mẹ. Đôi khi trong cuộc sống bận rộn chúng ta quên mất gia đình, cuốn sách nhỏ này như một lời nhắc nhở giúp ta biết yêu thương và chân trọng những điều mình có. Cảm ơn tác giả đã viết nên một tác phẩm đầy xúc động và ý nghĩa như vậy.
5
419658
2015-08-20 20:03:33
--------------------------
271593
7903
Đọc lần 1, cảm giác ấm áp, dễ thương của người con gái nhỏ đối với gia đình cũng làm mình ấm lòng theo, nhưng cảm xúc chỉ dừng lại ở đó, rồi cuốn sách cũng nằm yên trên góc kệ. Cho đến những ngày sau, mình xảy ra mâu thuẫn với chị gái, và điều làm mình ấm ức, tức đến phát khóc là dù mình đúng, cái sai rõ rành rành là của chị, nhưng mẹ lại la mắng mình, đến độ quỳ lạy mình, cái cảm giác lúc đó, thật sự, chỉ muốn biến khỏi nhà, không muốn tồn tại trong gia đình nữa,tại sao cái gì cũng là lỗi tại mình, tại sao lại như vậy.
Tìm lại cuốn này đọc để mong cái tình trong đó làm mình yên tĩnh, nhưng làm tôi lại khóc nhiều hơn, khóc khi nhận ra đúng sai không quan trọng, quan trọng là tình cảm trong gia đình, có thể là chị sai, nhưng cách tôi phản bác lại chỉ khiến mọi thứ thêm tồi tệ, là mẹ không muốn thấy chị em tôi như vậy, là tôi đã quá nóng giận chỉ biết bảo vệ bản thân mình mà không nghĩ đến ai. Một lời xin lỗi khó nói ra thành lời.....


5
124733
2015-08-19 11:15:36
--------------------------
260342
7903
Với những câu chuyện đơn giản cùng với những minh họa lý thú cùng với cách kể chuyện đầy dí dỏm của tác giả, thật khó để không dõi theo câu chuyện. Với tiêu đề cuốn hút: Con gái có điều bí mật đã tạo cho độc giả một sự tò mò và muốn lật từng trang sách để đọc. Với chất lượng sách cũng như giấy in tốt đã tạo ra được cảm giác thoải mái cho người đọc và nâng niu cuốn sách hay. Dịch vụ giao hàng của tiki rất tốt và đơn hàng của tiki được bọc rất cẩn thận.
4
730145
2015-08-10 12:24:55
--------------------------
259054
7903
"con gái có điều bí mật" của Nguyễn Đỗ Phương Giao đọc lên khiến người ta như có một luồng điện chạy ngược vào tim, tình thân, tình phụ tử, tình ruột thịt ấm áp vô ngần. Những mẩu chuyện gia đình nhỏ xinh được kể qua giọng văn hết mực trong trẻo, đầy tình cảm khiến người đọc khó thể dời mắt khỏi những trang sách, những yêu thương. Phương Giao có một người ba hết sức tuyệt vời mà bất kì đứa trẻ nào cũng từng ao ước còn ba cô ấy lại có một đứa con không chỉ ấm áp mà còn giỏi giang. Một cuốn sách mỏng nhưng lại chan chứa yêu thương :)
5
248341
2015-08-09 06:59:01
--------------------------
258225
7903
Mình định mua tặng nhỏ bạn nhưng có vô duyên quá, không nhận nên mình đọc luôn. Quyển sách chứa đựng những tình cảm gia đình rất dễ thương, những tâm sự thầm kín nhưng đầy yêu thương mà cha con vốn rất ít thổ lộ với nhau. Đối với những bạn như mình, tuýp người thích gắn bó với gia đình, chắc chắn sẽ rất ưng ý với quyển sách trên. Nguyễn Đỗ Phương Giao là một nhà văn trẻ đang lên, mình khá thích văn của chị ấy và có lẽ đây là quyển hay nhất. Sách mỏng nhưng chất liệu giấy tốt lắm, bìa có màu nhẹ nhàng, phù hợp nội sung sách lắm luôn.
4
614800
2015-08-08 12:27:14
--------------------------
222999
7903
Cuốn sách này mình khá thích, về cả hình thức bên ngoài và nội dung bên trong. Về hình thức thì khỏi phải chê, bìa đẹp, giấy láng mịn, cầm đọc lướt qua từng trang giấy cảm giác rất thích thú. Về nội dung, tuy mình không phải là người thích những mẩu truyện ngắn nhưng với cuốn "Con Gái Có Điều Bí Mật" mình khá là thích, những mẩu truyện về cuộc sống bình thường trong gia đình và những điều thầm kín của một cô con gái được viết ra rất thú vị, lí thú và chân thực ^^.
4
246340
2015-07-06 14:22:47
--------------------------
220716
7903
Những điều mà ngày thường, tôi không bao giờ có thể nói ra với mẹ đã được tác giả thổ lộ hết trong quyển sách này. Dường như, bao yêu thương, những giọt nước mắt thương cha, thương mẹ đã được ghi lại, viết lại hết trong cuốn sách ấy. Mỗi câu chuyện đều nhỏ bé, bình thường và giản dị, xảy ra xung quanh ta. Thế nhưng khi đưa vào sách, tác giả đã vẽ lên bức kiệt tác tuyệt vời. Tác giả đã thành công khơi gợi trong tim mỗi con người cái tình yêu gia đình, yêu bố, yêu mẹ, khiến nó bùng lên, mãnh liệt hơn.
3
94487
2015-07-02 21:49:20
--------------------------
218592
7903
Một cuốn sách về cuộc sống tình cảm trong gia đình, ai cũng cần có những giây phút được ngồi bên những người thân yêu trong gia đình của mình, và chính tác giả cũng thể. Nhất là một người con gái lại càng cần gia đình của mình hơn nữa, tác giả đã cho thấy được những tình yêu thương và dường như đã rất thấu hiểu tâm lí của con gái vối lối viết rất trong trẻo và hồn nhiên. Tuy nhiên, khi xét về một khía cạnh nào đó thì nội dung thực sự vẫn chưa được cuốn hút cho lắm.
4
105935
2015-06-30 18:10:37
--------------------------
213080
7903
"Con gái có điều bí mật" là sách đầu tiên của Nguyễn Đỗ Phương Giao mà mình đọc. Ngay khi nhìn thấy tựa sách này mình đã cảm nhận được sức hút từ nó đối với mình. Chính vì vậy mình đã quyết định mua nó. Khi đọc xong cuốn sách mình cảm nhận được trong cuốn sách có rất nhiều hình ảnh trong đó giống với tuổi thơ của mình và khiến mình thêm hiểu về tình cảm của ba đối với chị em chúng mình. Trước kia mình không để quan tâm ba mẹ được nhiều, nhưng sau khi đọc xong thì mình quan sát kĩ hơn hai người và nhận ra nhiều điều trước kia mình chưa cảm nhận được. Cách viết văn của Phương Giao cũng thực sự cuốn hút mình. Mình rất cảm ơn chị đã viết ra cuốn sách này để mình yêu ba nhiều hơn trước! Hy vọng chị Phương Gian sẽ có nhiều tác phẩm ý nghĩa như vậy nữa.
4
558633
2015-06-23 11:23:01
--------------------------
208787
7903
lần đầu tiên mình đọc sách do Nguyễn Đỗ Phương Giao viết,và mình đã bị yêu nàng.Thực sự là sách rất hay,dễ thương,tình cảm chân thật,giàu cảm xúc.Lối viết không quá đặc sắc, chân thật dễ đi vào lòng người. Nhiều lúc cứ tưởng Lux viết truyện cho riêng mình vậy.Đọc xong truyện, mình đã khóc, vì trước đây, mình đã vô tâm, không quan tâm đến bố mẹ,làm bố mẹ buồn.Bây giờ, mình đã quan tâm, yêu thương bố mẹ nhiều hơn rồi.Đây là quyển sách rất đáng đọc, nó giúp nhiều người tìm lại những giây phút đẹp đẽ nhất của mỗi con người. Bìa sách thì lại rất đáng yêu.Xứng đáng 5 sao. Cảm ơn Lux nhiều lắm, yêu Lux
5
665206
2015-06-16 09:28:06
--------------------------
193059
7903
Hiện nay nhiều người vì công việc mà quên đi gia đình - nơi ta có thể về bất cứ lúc nào và luôn có người chờ đón ta. Đọc quyển này để yêu gia đình mình hơn, cảm nhận lại tuổi thơ, những ký ức tươi đẹp bên cạnh ba và mẹ. Khi đọc quyển này mình như bắt gặp chính bản thân xuất hiện trong từng trang sách. Lối văn rất dễ thương, chân thật, đi sâu vào lòng người. Về hình thức sách cũng rất đẹp, giấy tốt, trắng. Nói chung dây là một quyển sách hay, xứng đáng 5 sao.
5
400930
2015-05-06 13:32:19
--------------------------
180468
7903
“Ba ơi, con gái có điều bí mật…” nghe mới dễ thương làm sao. Đây là lần đầu tiên tôi đọc sách của Đỗ Nguyễn Phương Giao và ấn tượng của tôi là khá tốt. Thật ngưỡng mộ với gia đình hạnh phúc của bạn ấy. Về ưu điểm, quyển sách này có lối nói chuyện mộc mạc, chân thành và nhẹ nhàng khiến nó dễ dàng được cảm thụ. Ngôn từ dùng không quá phức tạp, dễ hiểu. Hình bìa cũng khá là dễ thương. Tuy nhiên, màu giấy sáng quá, cũng như là vỏ bìa mềm quá, mình thấy nếu như màu giấy tối lại nữa thì tuyệt hơn và sẽ hợp với nội dung quyển sách hơn. Ngoài ra mình thấy cách mở đầu của tác giả chưa thực sự hấp dẫn người đọc.
3
329218
2015-04-09 15:09:32
--------------------------
177132
7903
"Con gái có điều bí mật" một cuốn sách về gia đình hết sức trong trẻo, để lại trong lòng người đọc nhiều dư vị ngọt ngào của tinh thân. Nguyễn Đỗ Phương Giao quả là một cô gái may mắn khi được lớn lên trong gia đình vô vàn ấm áp ấy. Một ông bố tuyệt vời, trên cả tuyệt vời ấy chứ. Một tuổi thơ êm dịu bên bố như thế quả khiến người đọc như tôi không khỏi ghen tị. Tình cảm gia đình bền chặt như sợi dây vô hình ấy luôn buộc chặt mọi thành viên với nhau, yêu thương nhau rất nhiều. Cuốn sách cũng làm tôi nhớ lại tuổi thơ của mình, với bao kỉ niệm đẹp. Cảm ơn Giao, cảm ơn cô gái nhỏ
5
578865
2015-04-02 17:19:21
--------------------------
167237
7903
Đúng !! Con gái có rất nhiều những điều bí mật . Cuốn sách cho ta thấy được tình cảm gia đình là chính , biết bao điều khó nói mà không nói được . Bí mật của con gái thì vẫn luôn là bí mật thôi ( đó là phần nhận xét nội dung )
Về phần ngoại hình : Như các bạn thấy đấy , cuốn sách này thiết kế bìa đẹp và các trang giấy bên trong cuốn sách cũng được trang trí cẩn thận xen các hình ảnh minh họa , trang giấy được kết hợp giữa màu đen và màu cam ( và màu trắng của giấy ) 
Về phần chất lượng của hàng thì cũng rất tốt , nói chung là mình luôn luôn cho 5* về chất lượng sách của tiki . Đây là cuốn sách rất đáng mua để biết quý trọng và yêu thương gia đình hơn ^^
5
562472
2015-03-14 12:30:48
--------------------------
157214
7903
Mình thích quyển sách này ngay từ đoạn trích giới thiệu của tiki. Đây thật sự là một cuốn sách rất hay. Thường thì con gái không "thân" với cha bằng mẹ thế nhưng tình phụ tử không thua kém gì tình mẫu tử, nó được thể hiện qua những hành động nhỏ nhất. Những câu chuyện trong "Con gái có điều bí mật" được viết  như nhật ký, những bức thư dễ thương vô cùng, đôi lúc còn khiến mình rơi nước mắt. Mình khóc không phải vì buồn mà vì thương quá nhiều, mà tình thương đó đã lâu rồi mình chưa thể hiện. Tình cảm cha con trong tác phẩm này, qua ngòi bút bút của Phương Giao đã mang đến những cảm xúc ngọt ngào và bình yên, những nụ cười nhẹ nhàng và hết sức ý nghĩa cho đọc giả. Mong chờ thêm những tác phẩm của chị.
5
91968
2015-02-07 22:34:44
--------------------------
152333
7903
Rất nhiều bạn trẻ hiện nay luôn xem tình yêu đôi lứa là trước nhất, là không gì có thể thay thế hay bác bỏ, có lẽ nên đọc cuốn sách này để thấy, tình cảm gia đình cũng là một điều thiêng liêng, chẳng dễ gì từ bỏ hay thờ ơ. Những câu chuyện cô gái này viết rất chân thật, rất dễ rung động, bởi hầu như ai cũng có những tình cảm giấu kín với ba mẹ mình. Cả những ngọt ngào tuổi thơ khi ba luôn là người hùng và mỗi ngày thì cô gái nhỏ lại mơ một điều khác nhau. Chắc chắn khi đọc cuốn sách này, bạn sẽ thấy bình yên, nhẹ nhõm trong lòng, bởi tình cảm gia đình, là thứ duy nhất bạn chẳng phải tranh giành với ai để có được nó.
5
201502
2015-01-22 20:30:21
--------------------------
147730
7903
"CON GÁI CÓ ĐIỀU BÍ MẬT"- tôi thực sự đã bị ấn tượng ngay từ khi nhìn thấy tiêu đề xinh xinh của cuốn sách. Có lẽ tình cảm cha con là một tình cảm vốn không xa lạ gì trong cuộc sống của mỗi chúng ta... nhưng dường như không phải ai trong chúng ta cũng nhận ra một điều vô cùng quan trọng, đặc biệt của thứ tình cảm ấy. Và tôi cũng vậy. Tôi vẫn thường lầm tưởng rằng tình cha con là thứ tình cảm rất đỗi cao quý, thiêng liêng, nhưng đọc qua từng trang truyện, tôi mới chợt nhận ra rằng... Thì ra tình cha vốn giản dị và ấm áp đến như vậy. Tình yêu thương đơn giản chỉ là sự quan tâm, thấu hiểu và trân trọng lẫn nhau mà thôi.
5
522320
2015-01-08 17:42:22
--------------------------
133514
7903
"Con gái có điều bí mật" của Nguyễn Đỗ Phương Giao là quyển sách đầu tiên Anh tặng tôi, tôi đã thật sự biết ơn Anh bởi cuốn sách này đã gợi lên trong tôi vô vàn cảm xúc yêu thương quá đỗi ngọt ngào và vô cùng an yên. Chẳng có gì bằng tình cảm gia đình tròn đầy, chảng có gì tốt đẹp, sáng trong và đáng sống bằng những ngày tháng tuổi thơ tươi đẹp bên những người thương yêu nhất... Hãy luôn yêu thương, trân trọng gia đình mình bởi đó là nơi lưu trữ biết bao thanh âm trong trẻo kể từ khi ta chào đời, là nơi ta học được cách trao đi yêu thương và nhận lại vô vàn thương yêu từ ba, từ mẹ... Đọc sách của GIao đã có những lúc tôi lặng người đi trước những tình cảm của tình phụ tử ấm áp, thiêng liêng và bất diện của hai cha con họ...
5
417795
2014-11-05 19:31:18
--------------------------
113012
7903
Đây là 1 cuốn sách về lòng biết ơn, sự trân trọng những điều nhỏ nhặt, giáo dục về tình yêu gia đình một cách hoàn toàn vô tình mà có chủ ý. Khi đọc cuốn sách này, tôi chợt nhận ra mình cũng có 1 gia đình và tuổi thơ hạnh phúc như thế, tuy là mỗi nhà mỗi cảnh , nhưng điều quan trọng nhất là tác giả đã khiến tôi nhận ra những điều quý giá thiêng liêng và nhỏ nhặt trong từng gia đình khiến cho mỗi chúng ta trở thành những con người rất riêng.

Cám ơn Giao Giao và cuốn sách nhỏ của em, mặc dù đã học được cách yêu thương và trân trọng gia đình mình, nhưng đôi khi những lo toan, căng thẳng trong cuộc sống hàng ngày khiến chị quên đi những điều tuyệt vời trong cuộc đời chị, cuốn sách nhỏ này sẽ luôn như một lời nhắc nhở, để biết yêu thương gia đình mình hơn, để biết trân trọng và tận hưởng hạnh phúc trong từng khoảnh khắc nhỏ của gia đình mình hơn. Đây chắc chắn là món quà chị sẽ dành tặng cho những đứa em họ, hay bất cứ ai đang có xích mích với gia đình họ, để họ hiểu ra rằng : Gia đình luôn luôn là số một !
4
22174
2014-05-24 22:21:47
--------------------------
112207
7903
Tình cảm gia đình vẫn luôn là thứ tình thiêng liêng nhất mà mỗi người chúng ta có được trong đời. Càng đi xa, lời nhắn nhủ ấy lại càng thiết tha hơn bao giờ hết. Suốt dọc hành trình những câu chuyện Phương Giao chia sẻ, ắt hẳn nhiều bạn chúng ta sẽ thấy ẩn hiện trong đó nhiều tâm tư vốn giấu kín trong lòng mình. Từng lời của tác giả như từng dòng chảy cảm xúc tuôn trào cuồn cuộn như sóng xô bờ. Cuốn sách như lời khơi gợi và nhắc nhớ “bất cứ lúc nào con ngoái đầu nhìn lại, Ba vẫn ở đó, ánh mắt không ngừng hướng về phía con.”, hay những lần con vô tâm làm ba mẹ buồn, lo; những quan tâm nho nhỏ nhưng luôn cần gìn giữ... Bản thân thực sự biết ơn khi tìm thấy những đồng điệu với từng lời tác giả viết ra, để biết rằng mình cần trân trọng hơn nữa những thứ quý giá bên cạnh mình.
5
82864
2014-05-10 12:05:15
--------------------------
108548
7903
Quyển sách được in bìa rất đẹp và bắt mắt. Nó đơn giản nhưng đầy dễ thương, cuốn hút với 1 đứa con gái tuổi teen như tôi! Khi tôi cầm quyển sách lên trường đọc, ai cũng khen và rất thích!
Cảm ơn chị! Chị đã truyền sức mạnh cho tôi, để tôi trân trọng cái gia đình này hơn rất nhiều. Những câu chuyện đơn giản nhưng nó khía sâu vào suy nghĩ của tôi. Nó làm tôi xúc động vì đôi lúc hình ảnh gia đình tôi ẩn hiện trong đấy. Thực sự rất sâu sắc, cô đọng! 
Nhìn chung là những câu chuyện về gia đình rất xúc động, nhưng trong đó, tôi lại cảm thấy mỗi câu chuyện cứ như dạy tôi 1 bài học, 1 cách ứng xử văn hóa trong gia đình. Nó chỉ tôi biết phải làm thế nào khi chưa từng ai chỉ tôi làm điều ấy. Nó vực dậy trong tôi 1 cái gì đó gọi là trách nhiệm. Tôi phải quan tâm, chăm sóc gia đình ấy nhiều hơn. Vì mấy ai có được trọn vẹn 1 gia đình hạnh phúc!
Cảm ơn chị 1 lần nữa! Vì tất cả những gì chị viết trong sách! Và tôi đã can đảm hơn để nói: "Con yêu ba mẹ, yêu hai, yêu gia đình nhí nhố của mình lắm!"
5
253068
2014-03-19 18:33:25
--------------------------
106218
7903
Đọc cuốn sách này mình đã như đọc được chính tâm tư tình cảm của mình vậy. Chị Giao mặc dù chỉ viết những tản văn ngăn ngắn, cũng không hẳn là một bài viết hoàn chỉnh nhưng lại đong đầy yêu thương. Có những lúc đọc, mình khóc, khóc vì mình không có nhiều dũng cảm như chị Giao, không thể nói với ba mẹ mình rằng mình cũng yêu thương ba mẹ nhiều lắm. Khóc xong, mình lại thấy bình an. Thật vui vì chị Giao có một gia đình hạnh phúc, thật vui vì hoá ra, đâu phải một mình mình bối rối trước tình yêu thầm lặng của ba mẹ. Và giờ đây, mình biết rằng, dù là lúc nào đi chăng nữa, thì cũng chẳng lúc nào muộn để nói lời yêu thương.
5
53623
2014-02-16 13:19:41
--------------------------
103644
7903
Mới xem phần giới thiệu về sách trên tiki thôi nhưng nó cũng đã cuốn hút mình rất nhiều, khi được cầm sách trên tay mình đã giành cả buổi trưa không ngủ để ngấu nghiến nó, đọc đi, đọc lại nhưng cảm xúc vẫn không thay đổi, thực sự rất xúc động. Mỗi câu chuyện mà chị Phương Giao viết rất giản dị và chân thực, càng đọc càng thấm thía, đọc đến trang cuối rồi chỉ ước sách dài hơn tí nữa để lại tiếp tục thêm những cảm xúc mới.  Mình đã khóc rất nhiều khi đọc những dòng tâm sự chị gửi đến gia đình và nhất là về người ba của chị. Chị có một gia đình thật hạnh phúc. Nhìn lại mình em thấy mình thật vô tâm , em cũng rất thương ba mẹ nhưng chưa bao giờ em trực tiếp nói cả. Thực sự em rất ngưỡng mộ chị. Chị là động lực để em có thể thực hiện ước mơ của mình. Nhất định em sẽ thi đỗ vào Đại học Luật thành Phố Hồ chí Minh để đươc gặp chị. Thần tượng của em.Mong rằng chị sẽ sớm xuất bản thật nhiều sách hay như thế nữa. Em sẽ là độc giả trung thành của chị.
5
223174
2014-01-02 09:04:19
--------------------------
99594
7903
Em đã đủ dũng cảm để có thể nói nên lời:
"Con yêu Ba Mẹ nhiều lắm! 
Mẹ đã khóc vì điều đó còn Ba thì cười và im lặng... 
 Thực sự chỉ biết nói cảm ơn "Con gái có điều bí mật" nhiều lắm!
Cầm quyển sách trên tay, cứ đọc đi đọc lại...đọc mãi mà không thấy chán vì mỗi lần đọc lại là một cảm xúc khác nhau...ừ thì mới cười đó bởi những câu chuyện dễ thương nhưng lát sau thì má lại bị ướt bởi những giọt nước mắt không kìm được mà lăn dài...Con tim không thể không thấy ấm vì những yêu thương và tình cảm mộc mạc bao trùm cả cuốn sách...
Em cảm ơn tác giả vì đã viết nên cuốn sách này để con bé như em lần đầu tiên làm Ba Mẹ nở nụ cười hạnh phúc nhất!
Gửi lời đến tác giả: em không PR cho chị đâu mà là con tim em làm điều đó đấy!
5
103890
2013-11-16 15:10:21
--------------------------
98636
7903
Nghe tin quyển sách "con gái" này xuất bản, tôi háo hức định bụng đi mua tặng cho em gái của mình đọc. Nhưng khi cầm nó trên tay rồi, đọc rồi thì định bao tử lại cho nó đọc sau vậy. Quyển sách này của con gái viết nhưng không chỉ viết về con gái....

Đọc mà thấy rợn người, như muốn chợt òa khóc vì nhớ những lúc mình hư với ba, hỗn với má, vòi cái này vòi cái kia từ ba má mà không để ý đến những cảm xúc của ba má. Một ngày dành quá nhiều thời gian với công việc, học hành,...mà cuốn theo nó, quên đi ba má mình đang già yếu từng ngày, cần lắm sự yêu thương, cần lắm sự chăm sóc và quan tâm.

Tôi cảm thấy mình thật bất hiếu, cảm ơn tác giả vì quyển sách này, nó đã giúp tôi tỉnh ngộ rằng nếu bây giờ không yêu thương, thì chắc gì ngày mai còn cơ hội. Và qua "con gái cũng có điều bí mật này", tôi cảm thấy rằng không chỉ con gái mới có điều bí mật, mà con trai cũng có điều bí mật nên nói với ba mạ của mình. ^^
5
99059
2013-11-05 22:51:49
--------------------------
98575
7903
Cảm ơn tác giả rất nhiều vì Con Gái Có Điều Bí Mật!

Không hiểu sao đọc lại cứ thấy bồi hồi, xúc động, cứ "nổi da gà", cứ "rợn người". Có một cảm xúc rất lạ cứ lâng lâng trong lòng.

Cuốn sách hay không phải vì nội dung quá mới mẻ, không phải vì cách tiếp cận quá đặc biệt. Mà chính những tình cảm mộc mạc, chân thành, một tình yêu thương vô bờ bến được gửi gắm trong từng câu từng chữ đã chạm đến trái tim người đọc như chính cô tác giả luôn tràn đầy sự yêu thương vậy. Và tât cả những điều đó, đã khiến cho người đọc phải bồi hồi xúc động nhớ lại những kỉ niệm về Ba, về Mẹ, về Gia Đình của họ.

Cảm ơn Nguyễn Đỗ Phương Giao nhiều lắm
5
195154
2013-11-05 13:20:23
--------------------------
