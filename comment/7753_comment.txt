393568
7753
Series "Ngôi nhà nhỏ trên thảo nguyên" đã đến hồi kết thúc !   Ở tập này, Laura đã trở thành cô giáo, cô phải sống xa nhà, gặp nhiều khó khăn... Một bóng hình mới dần bước vào cuộc đời cô- Almanzo wilder ! Laura ingalls wilder bắt đầu viết bộ sách này khi bà đã 65 tuổi. Nó là một tư liệu chân thực, phản ánh cuộc sống của những nông dân Mĩ cuối thế kỉ 19.  Trình bày đẹp: giấy xốp Thuỵ Điển màu ngà vàng, có nhiều ảnh minh họa thú vị,... Xứng đáng nằm trên tủ sách của mỗi người !!!
5
803518
2016-03-08 22:22:09
--------------------------
232985
7753
Bộ truyện "Ngôi nhà nhỏ trên thảo nguyên" đã có một cái kết đẹp. Không quá dài và cũng không quá ngắn, nhưng đã đem đến cho bạn đọc nhiều cung bậc cảm xúc khác nhau về tình cảm gia đình, tình yêu, vất vả, hy sinh, hạnh phúc,...
Những ai từng biết đến cái tên "Ngôi nhà nhỏ trên thảo nguyên" chắc hẳn cũng từng một lần mong ước được sống trên một thảo nguyên xanh mênh mông, thơm mùi đồng nội. Mỗi trang sách đưa tâm hồn mình lạc vào thế giới xa xôi ấy.
Tác phẩm sẽ hoàn hảo hơn nếu tranh minh họa phù hợp, riêng đối với mình thì không thích kiểu vẽ minh họa này.
5
53390
2015-07-19 14:19:06
--------------------------
