450121
10327
Đọc qua tác phẩm thế hệ sau như mình mới thấy được những sự hi sinh thầm lặng của các nữ biệt động Sài Gòn. Các cô, các chị khi được giao nhiệm vụ thì luôn hăng hái, chấp nhận hị sinh để hoàn thành nhiệm vụ. Họ đã dùng những kinh nghiệm cá nhân, thăm dò, tự chế vũ khí để hoàn thành những trận đánh tuy nhỏ nhưng để lại tiếng vang trên chính căn cứ địch. Có những đoạn người đọc không khỏi xúc động khi tác giả thông qua lời kể của các cô, các chị kể về những lần bị địch bắt và dùng nhiều thủ đoạn tra tấn khác nhau, nhưng họ vẫn không khai nửa lời. Chiến đấu anh dũng như thế nhưng đến ngày hòa bình lặp lại họ vẫn là những người phụ nữ như bao người phụ nữ khác trở về với cuộc sống đời thường của mình. Với họ được sống trong hòa bình đã may mắn gấp vạn lần hơn các đồng đội đã ngã xuống.
5
1163581
2016-06-18 15:08:25
--------------------------
200829
10327
Tôi xem phim "Biệt động Sài Gòn" rất nhiều lần và lần nào cũng cùng một cảm giác vừa khâm phục, vừa biết ơn, và cả thương xót. Khi đọc "Những thiên thần đường phố" tôi mới được biết đến những câu chuyện khốc liệt hơn. Họ là những người phụ nữ gan góc và mưu trí, len lỏi vào tận hang ổ của giặc, những nơi nguy hiểm nhất, tìm hiểu, điều nghiên đến từng chi tiết nhỏ nhất, không ngại hy sinh thân mình, không nghĩ đến hạnh phúc riêng tư, bất chấp hiểm nguy ngay trước mặt, góp công lớn làm nên chiến thắng. Lỗi hành văn mộc mạc của Mã Thiện Đồng làm cho người đọc có cảm giác không phải đang đọc sách mà đang trò chuyện cùng với những nhân chứng lịch sử.
5
24486
2015-05-25 23:35:02
--------------------------
61709
10327
Mình thích cách tác giả gọi những nhân vật trong cuốn sách là nhưng "thiên thần". Họ là những người có thật trong lịch sử, những người phụ nữ can đảm, bề ngoài có thể mảnh mai nhưng bên trong lại chứa đựng những ý chí kiên cường, mạnh mẽ. Chính họ đã góp phần vào công cuộc đấu tranh của dân tộc ta, của nhân dân Sài Gòn. Những khắc họa, những lời kể trong sách không quá đi vào chi tiết, nhưng có sức gợi mạnh mẽ, khiến người đọc phải cảm phục những tấm lòng, những tâm hồn cao cả đó. "Những thiên thần đường phố" là một cuốn sách mang đến nhiều cảm xúc, dù có đọc đến lần thứ mấy đi chăng nữa.
5
20073
2013-03-03 16:12:34
--------------------------
