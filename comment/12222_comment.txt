447065
12222
Ngày xưa mình rất thích Thần đồng đất Việt, trong truyện lúc nào cũng có những câu chuyện lịch sử thú vị, được đưa về gần gũi với các bạn nhỏ. Cũng đã lâu rồi mình không còn theo dõi nữa. Đợt vừa rồi mình tình cờ mua một cuốn cho em, nội dung thay đổi khá nhiều, mỗi cuốn về một lĩnh vực, mình mua cuốn này nói về con thạch sùng, mẩu chuyện ngắn dí dỏm của Tí và các nhân vật xung quanh chú thạch sùng được in màu đẹp mắt. Tiếp đến là những thông tin, kiến thức về các loài bò sát, giúp trẻ có cái nhìn rộng hơn. Cuối cùng là mẩu chuyện về danh nhân. So với số tiền bỏ ra thì cuốn truyện cung cấp được khá nhiều kiến thức cho trẻ. Ổn.
3
141646
2016-06-13 14:04:46
--------------------------
