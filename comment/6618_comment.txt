463154
6618
Điều đầu tiên mình muốn nói là khoản dịch thuật chưa được trơn tru lắm, thành ra nhiều lúc cảm thấy chưa truyền tải hết được cái hay của tác phẩm. 

Chất lượng sản phẩm thì rất ok rồi, giấy tốt, bìa đẹp, có kèm cả bookmark nữa. Cầm trên tay thấy nhẹ chứ không nặng. Rất ưng ý.

Cốt truyện thì rất hay rồi. Khi Chloe biết mình chỉ là sản phẩm lỗi nhưng đầy nguy hiểm của một thí nghiệm khoa học, cô đã lên kế hoạch trốn chạy khỏi những kẻ tạo ra mình và giờ đang tìm cách giết mình để đảm bảo an toàn cho bản thân. Cuộc trốn chạy vô cùng kịch tính khiến người đọc không thể dời mắt. Liệu Chloe có thể tin tưởng bất kỳ ai khác hay không, khi mà bên cô là những kẻ nhiều mặt chẳng biết ai bạn ai thù.

4
401462
2016-06-29 08:41:56
--------------------------
441572
6618
Mình đặc biệt thích thể loại Fantasy nên bộ nào ra là phải mua ngay. Series Sức mạnh hắc ám không làm mình thất vọng tẹo nào. Cuốn này mình cực kì ưng trong 3 bộ vì trong sách còn có thêm những họa tiết nữa.
Chloe cùng với những người bạn luôn gặp rắc rối và phải đối đầu với người ở nhà mở LiLy. Còn diễn biến xoay quanh chuyện tình cảm của Chloe và cậu bạn Derek.
Sách được tiki bọc rất cẩn thận vào sạch sẽ không bị hư ở góc sách. Mình rất vui và rất mong những cuốn khác cũng được tiki bọc như thế.
4
1209811
2016-06-03 14:44:54
--------------------------
427749
6618
Đó là những gì mình có thể miêu tả về tập này, nó là sự chuyển giao của tập mở đầu và kết thúc lửng cho tập cuối! Quá nhiều tình tiết trong một tập truyện khiến bản thân cảm giác ngốn không nổi, và vì tác giả dùng ngôi kể thứ nhất là của nữ nhân vật chính nên khiến mình thấy khá hoang mang theo tâm trạng của nhân vật luôn. Rất mâu thuẫn và phải luôn đưa ra kế hoạch hay quyết định sống còn, đối với một cô gái tuổi teen thì điều đó càng khó.
4
505121
2016-05-10 00:15:53
--------------------------
303379
6618
Có thể nói Phần 2 - Thức Tỉnh của "Series Sức Mạnh Hắc Ám" là phần mình thích nhất trong 3 phần. Ở phần 2 này câu chuyện dần đến cao trào khi Chloe biết được sự thật rằng cô là một thí nghiệm khoa học biết đi và quyết định bỏ trốn với ba người bạn khỏi Nhà Mở Lyle. Nội dung truyện hài dước, hấp dẫn, xen lẫn nhiều bất ngờ với những bí ẩn liên tục xuất hiện đã lôi cuốn tôi cho tới trang cuối cùng. Tuy vậy truyện lại mất điểm ở phần dịch thuật và lỗi chính tả còn nhiều, mặc dù biết đây là lỗi thường thấy ở sách của Chibooks nhưng mình mong là Chibooks sẽ khắc phục tốt hơn.
4
44933
2015-09-15 20:28:16
--------------------------
283017
6618
Đầu tiên phải nói là bìa tái bản này đẹp hơn rất nhiều so với bìa đợt đầu, chất liệu giấy khá ổn, có đi kèm bookmark. Về nội dung, Thức Tỉnh bắt đầu cuộc trốn chạy của những đứa trẻ siêu năng lực khỏi nhóm nhà khoa học độc ác (khi mà ở tập 1 những đứa trẻ này vẫn đang bị giam hãm trong Nhà Mở Lyle). Điều mình thích nhất ở tập này là có nhiều đoạn riêng giữa chàng người sói Derek to khoẻ lạnh lùng và cô nàng Chloe nhỏ nhắn đáng yêu ♥ Họ thật dũng cảm khi dám tự lập kế hoạch bỏ trốn khỏi Hội Edison mà không cần người lớn trợ giúp. Mỗi ngày là mỗi nguy hiểm, người tưởng là bạn lại hoá thù, nhiều bí ẩn động trời được khám phá, những cuộc tấn công bất ngờ, nhiều cảnh hành động hơn, nút thắt mới lần lượt xuất hiện, tất cả điều đó sẽ cuốn bạn vào câu chuyện teen giả tưởng thú vị mà vẫn hài hước, ngọt ngào này ! Có điều là khoản dịch thuật khá tệ, chưa được thanh thoát, chưa được linh hoạt, lỗi chính tả vẫn còn, mực in trang đậm trang nhạt, nhưng vì thích series này quá nên mình có thể nhắm mắt cho qua (có để đọc còn hơn không), hy vọng Chibooks rút kinh nghiệm cho tập sau.
5
75959
2015-08-29 17:13:56
--------------------------
130779
6618
Mình đã bị em í hút ngay vào tập đầu thật sự là cuộc phiêu lưu đầy hấp dẫn và như mình mong đợi một kết thúc tuyệt vời cho cuốn sách.Nếu đọc từ tập đầy đến giờ chắc mọi người sẽ nhận thấy nó na ná giống phim x-men í nhưng nội dung thì hấp dẫn hơn.Cuốn là những trải nghiệm tuyệt vời khi Chloe gọi hồn cả một nghĩa trang wow tưởng tượng thôi cũng thấy thú vị.Một chút phiêu lưu, một chút phép thuật đã tạo nên cuốn sách tháng 10 của tôi !!!!
5
406836
2014-10-19 22:58:27
--------------------------
121456
6618
Mặc dù truyện thuộc vào thể lọai khá ăn khách hiện nay nhưng mình thấy chưa thực sự đặc sắc. Cốt truyện hay, nhưng cách diễn đạt khá dài dòng làm cho mình không thể hiểu rõ được chi tiết truyện, nhất là mình phải đọc đi đọc lại nhiều lần nội dung mới nắm rõ được những tình tiết tác giả viết. Tuy nhiên có một số đoạn khá gây cấn và gây thu hút làm cho người đọc hồi hộp ngóng trông diễn biến của câu truyện. Mình thích sự tự nhiên ở các nhân vật và suy nghĩ của họ.
3
284845
2014-08-19 18:58:09
--------------------------
117214
6618
Chloe Saunders từng là một cô gái tuổi vị thành niên bình thường - hoặc cô nghĩ vậy. Sau đó Chloe biết được sự thật gây choáng váng - cô là một thí nghiệm khoa học biết đi. Bị một nhóm các nhà khoa học độc ác, Hội Edison, biến đổi gien từ lúc mới sinh. Chloe là phế phẩm của họ - một người gọi hồn đầy uy lực có thể nhìn thấy hồn ma và thậm chí là làm người chết sống lại, điều thường gây ra những hậu quả kinh khủng. Tồi tệ hơn, sức mạnh ngày càng tăng tiến của Chloe đã khiến cô trở thành mối đe dọa với những thành viên còn sống sót trong Hội Edison, thế là họ quyết định, đã đến lúc kết thúc thí nghiệm này - vĩnh viễn…

Giờ đây, Chloe đang bỏ trốn với ba người bạn cùng sở hữu các siêu năng lực khác nhau - một pháp sư điển trai, một người sói hay bực bội và một phù thủy trẻ tuổi tính khí thất thường. Đồng hành cùng nhau, họ có cơ hội được tự do - nhưng liệu Chloe có thể tin tưởng các bạn mới của mình hay không?
tôi thích cốt truyện này
5
379843
2014-07-16 10:44:04
--------------------------
