468433
5440
Quyển sách nhỏ nhắn nhưng tóm gọn tình hình ngoại giao của ta với TQ từ bao ngàn đời. Nội dung sách phong phú tử hành trình, cống phẩm đến mưu tài trí lược của các nhà ngoại giao đầu tuên của VN. Quc đó mới thấy được hành trình gian khổ của những người đi sứ khó khăn biết nhường nào. Đọc xong quyển sách mình cảm nhận được sự uất ức của một nước nhỏ khi phải thuần phục nước lớn và các vị vua VN mặt dù ở các triều đại khác nhau nhưng vẫn chất chứa chung một nỗi niềm muốn chấm dứt những đòi hỏi và luôn muốn VN là một quốc gia tự trị không bị ràng buộc bởi TQ
4
1163581
2016-07-05 00:58:41
--------------------------
414072
5440
Tôi thật sự ấn tượng với những gì mà cuốn sách này mang lại. Việt Hoa thông sứ sử lược giúp người đọc có được một cái nhìn tổng quát về nghệ thuật ngoại giao của nước ta với anh hàng xóm to con. Nước ta vốn yếu hơn về thế và lực nên việc quan hệ với Trung Hoa thật sự là một vấn đề mang tính sống còn đối với sự tồn vong của dân tộc. Một mặt ta vẫn phải nêu cao tinh thần độc lập tự cường, giữ vững bản sắc dân tộc, một mặt cũng phải tránh làm mếch lòng "Thiên tử Trung Hoa" để giữ được bình yên cho đất nước. 
Phần đầu của sách nói về căn nguyên của việc đi sứ, cũng như miêu tả đầy đủ thủ tục và cống phẩm trong những chuyến đi này. Phần sau cuốn sách nêu lên một vài ví dụ điển hình về những vị chánh sứ, phó sứ của nước ta khi sang bên đất bạn và được nước bạn kiêng nể ra làm sao.
Cuốn sách tuy ngắn nhưng chắc chắn sẽ giúp ích được nhiều cho những ai muốn tìm hiểu. Khuyến nghị mọi người nên tìm đọc.
4
1177538
2016-04-10 22:26:28
--------------------------
338160
5440
Thiết-tưởng ai từng thấy quyển sách này cũng sẽ nghĩ là khó đọc. Vì nghe đến sử-lược mà lại là biên chép chánh-sử, nghĩ theo lối viết sử đương-thời nên có phần ngao-ngán. Nhưng thực sự lại không, lối viết ngắn gọn nhưng đầy thông-tin của tác-giả chính là một điểm mạnh của quyển sách này. Tác-giả dẫn ra rất nhiều sử-liệu và mô-tả tỉ-mỉ các cuộc bang-giao của nước ta với Bắc-triều, lại nói rõ công-cuộc và thâm-ý của tiền-nhơn chứ không suy xét thiển-cận mà biên vào hư-di cho hậu-thế. Tuy nhiên, nhơn vô thập toàn, việc thiếu sót thông-tin thời Lý Trần cũng là một điều đáng tiếc. Tuy vậy, tập sách này vẫn là loại sách sưu-khảo hay và ích-lợi cho những ai ham thích sử-học và muốn tường-tận lịch-sử quốc-gia. 
5
34195
2015-11-15 14:56:43
--------------------------
331846
5440
Khi nhắc tới sang sứ, chúng ta thường biết nhiều tới những cuộc đối đáp tài tình, thể hiện trí tuệ của con người Việt Nam khi đi sang Trung Quốc, nhưng lại không biết nhiều về những bi kịch của người đi sứ, không biết khi Trung Quốc của sứ sang nước ta thì nhân dân phải tốn kém thế nào, họ đi lại với đường lối ra sao, chuẩn bị những gì, cống nạp vất vả và uất ức bao nhiêu.

Những điều bạn không biết sẽ được giải đáp trong Việt Hoa thông sử sử lược. Mình dành cho cuốn sách này khá nhiều tình cảm, có lẽ trong cuốn sách có nhắc tới quê mình_đó là lộ trình mà sứ thần phải đi qua nên mình thấy tự hào.
5
403246
2015-11-05 10:21:04
--------------------------
302425
5440
Nếu ai đã thích lịch sử thì không thể bỏ qua quyển sách này. 
Nội dung cuốn sách tưởng chừng khô khan nhưng lại mang nhiều nội dung xoay quanh mối quan hệ bang giao giữa 2 quốc gia láng giềng. 
Những lần đi sứ gây tiếng vang và cả những lần đi sứ với mục đích cầu hòa sau những chiến thắng vang dội như của Lê Lợi hay Quang Trung, có chút đau xót khi chúng ta chiến thắng nhưng vẫn phải đúc tượng vàng Liễu Thăng để cống nạp cho bên bại trận.
Tất cả gói gọn trong hơn 100 trang sách. Đây là một trong những quyển sách thích nhất trong bộ Góc nhìn sử Việt của Alphabook ^^
5
7166
2015-09-15 10:56:56
--------------------------
