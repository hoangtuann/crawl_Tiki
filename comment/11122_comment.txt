408336
11122
Đọc "Cỏ đồi phương đông" xong tôi quyết định mua thêm 3 tác phẩm khác của Quyên nữa để đọc cho đã ^^ (trong đó có quyển "Cỏ lau vạn dặm" là cùng phong cách ma mị với quyển Cỏ đồi này)
*
Sách cũng ngắn thôi, dễ đọc, không dài dòng, không sáo rỗng.
Quyên viết khá chắc tay, cốt truyện lạ nên thu hút tôi, đọc thích lắm chứ không bị chán như nhiều quyển cọp mác "truyện ngắn" của các tác giả trẻ khác đang bày bán nhan nhản hiện nay. Giữa thời đại mà ai ai cũng có thể viết dăm ba câu chuyện rồi PR rầm rộ để gắn mác “nhà văn” nhưng mua về đọc chẳng đâu ra đâu, thì tôi thấy văn Quyên đằm thắm như bông hoa lặng thầm tỏa hương, không ồn ào, không lên gân, rất đáng trân trọng.

4
12693
2016-03-31 15:37:11
--------------------------
