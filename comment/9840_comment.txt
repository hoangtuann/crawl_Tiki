415899
9840
Mình thích đọc những câu truyện để cái kết mở như thế này nhưng lại không thích những cái kết không có hậu. Một số cây viết trẻ Cẩm Ly, Hoàng Nhật, Bích Khoa…lần đầu mình đọc. Nói chung, cơ bản không có ấn tượng mấy về tập truyện, cách viết còn đơn giản và cảm thấy quen thuộc như đọc ở đâu đó. Có lẽ do mạng xã hội quá phát triển, người ta trích từng đoạn đăng lên facebook, nên lướt qua và đọc lại thấy quen. Nói chung đây là tiếng nói của những cây viết trẻ về xã hội, tình yêu, con người, cuộc đời.
3
553752
2016-04-14 09:22:01
--------------------------
