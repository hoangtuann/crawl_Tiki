439279
9117
Câu chuyện về nhà động vật học người Anh Jane Goodall chuyên nghiên cứu về loài tinh tinh, Ngay từ nhỏ, Jane Goodall đã là một đứa trẻ lập dị, cô bé rất quan tâm đến động vật, sau này khi trở thành thư kí cho nhà khảo cổ học lừng danh Louis Leakey, Jane Goodall đã thực sự trở thành một nhà nghiên cứu động vật nổi tiếng thế giới nhất là về tinh tinh. Đọc về cuộc đời và sự nghiệp của bà mới thấy tình yêu bà dành cho động vật hoang dã và môi trường sống to lớn biết bao. Thật đáng khâm phục!
4
535853
2016-05-31 00:39:32
--------------------------
417141
9117
Biết đến Jane Goodall qua một lần xem trên tivi. Tình cờ đi nhà sách mình tìm thấy bộ truyện who này. Hôm nay mình định lên Tiki mua về đọc. Sau khi đọc xong quyển sách này mình có vài nhận xét sau. Thứ nhất bìa sách được thiết kế công phu tỉ mĩ sách được in màu rất đẹp mắt. Thứ hai là về nội dung cho ta biết tiểu sử Jane Goodall từ nhỏ cho đến lớn. Bà là người yêu thương động vật đặc biệt là các loài tinh tinh. Ngoài ra trong sách còn có những phần phụ lục cung cấp cho chúng ta nhiều kiến thức thú vị.
4
509425
2016-04-16 12:17:07
--------------------------
160761
9117
Tấm lòng yêu động vật đã thúc đẩy ước mơ được đến Châu Phi để nghiên cứu động vật của Jane Goodall. Câu chuyện đưa đến cho người đọc rằng nếu mong muốn điều gì hãy cứ mơ ước đi, hãy lập, bám sát và thực hiện mục tiêu đã đề ra chắc chắn một ngày sẽ đạt được ước mơ đó. Truyện in màu, chữ to rất dễ đọc. Cuối mỗi chương đều có sự giải thích nhằm cung cấp thêm thông tin cho người đọc. Trong truyện có đề cập đến chìa khóa thành công và nuôi dưỡng ước mơ của Jane rất đáng để cho các bạn nhỏ học tập theo.
4
534227
2015-02-25 14:58:05
--------------------------
152217
9117
Sự yêu quý và tình cảm chân thành dành cho những người bạn động vật của Jane đã biểu hiện từ khi bà còn bé, hiện nay dù đã gần 80 tuổi nhưng bà vẫn đi vòng quanh thế giới để tuyên truyền bảo vệ động vật.
   Nét vẽ đẹp, dễ thương, in màu hết, giấy tốt và thơm. Đọc truyện mới thấy có 1 cô bé vì muốn biết gà đẻ trứng bằng cách nào đã ngồi rình ở chuồng gà suốt 5 tiếng liền, còn mang giun, sâu vào phòng riêng để tiện ngắm. Sau khi đọc một cuốn sách về những con vật, Jane ước ao được sang châu Phi thăm chúng. Đến một ngày, mong ước của cô đã thành sự thật, cô đã có cơ hội gặp rất nhiều loài động vật, đặc biệt là tinh tinh...
4
285794
2015-01-22 16:23:24
--------------------------
65360
9117
Trước đây mình không hề biết về Jane Goodalll cũng như những công việc của bà, nhưng qua cuốn sách mình đã có được một cái nhìn rõ nét cũng những hiểu biết mới mẻ về người doanh nhân tài năng này. Bà quả thực là một người phụ nữ mạnh mẽ, dám dấn thân và tiến bước, dám đi theo con đường riêng của mình. Công việc nghiên cứu động vật của bà thực sự rất thú vị, vì nó giúp bà cũng như mọi người hiểu hơn về thế giới tự nhiên, cũng như tìm được nhiều sự đồng cảm bất ngờ giữa con người và loài vật. Jane là một tấm gương cho mọi người, về sự đam mê, sự nỗ lực cũng như những cống hiến cho cộng đồng.
4
20073
2013-03-25 15:03:55
--------------------------
