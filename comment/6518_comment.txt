495188
6518
Một câu chuyện rất hay và ý nghĩa về những tình cảm giữa người với người. Dù cuộc sống vẫn còn rất nhiều khó khăn nhưng con người vẫn luôn gìn giữ và dành cho nhau những chân tình đáng quý
4
917248
2016-12-11 09:19:06
--------------------------
326495
6518
tập hai cũng đã đem lại kết thúc trọn vẹn cho cậu bé Remi khi cậu đã tìm được gia đình thực sự - và giúp đỡ bạn bè sau những ngày tháng phiêu bạt nghèo khổ... cái lúc cậu được nhận về một gia đình mà toàn những thành viên rất lạ như ông bố, bà mẹ và anh chị em chẳng tỏ ra thân thiết gì thậm chí còn sai vặt và mượn chú chó Ca-pi để ăn cắp nữa nhưng Remi vẫn nói với bạn rằng mình vẫn yêu gia đình mình lắm - điều đó cho thấy cậu khát khao một mái nhà thực sự và sẵn sàng làm hết tất cả để có được điều đó.. cảm ơn tác giả vì câu chuyện hết sức tuyệt vời.
4
568747
2015-10-25 22:58:44
--------------------------
188329
6518
Tiếp nối phần một, phần hai của tác phẩm viết về chặng đường tiếp theo của Rémi đến khi tìm được người mẹ ruột thân thương của mình và sau đó là báo đáp những người bạn, nhưng người lao động nghèo tiền bạc nhưng giàu tình người đã giúp đỡ cậu không mảy may có một suy nghĩ vụ lợi nào.
Tác phẩm thể hiện giá trị nhân đạo sâu sắc, thể hiện khát khao của Hector Malot về một xã hội không có sự bóc lột giữa người với người, nơi giới quý tộc và người lao động yêu thương nhau, nơi kẻ xấu bị trường phạt và người lương thiện được tưởng thưởng.
4
594118
2015-04-25 08:50:33
--------------------------
