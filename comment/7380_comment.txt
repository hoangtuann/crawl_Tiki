410175
7380
Mình đã mua và đọc cuốc sách này cách đây 3-4 năm rồi, mình không nhớ rõ lắm. Nay muốn tìm mua tặng bạn bè nhưng từ giá 78.000 đồng giờ lại tăng lên 198.000 đồng. Mình cho là giá hơi cao. Nhưng những ai có điều kiện để đọc thì nên đọc qua quyển sách này. Bổ ích lắm đấy. Còn không thì đọc 2 quyển Đắc Nhân Tâm và Quẳng Gánh Lo Đi Mà Vui Sống của cùng tác giả Dale Carnegie thì cũng hay và ý nghĩa. 
Quyển sách hay và có ý nghĩa rất nhiều cho cuộc sống. Đọc và suy ngẫm giúp ta có thêm kỹ năng sống để hoàn thiện cuộc sống tốt hơn.
5
1281981
2016-04-03 17:38:55
--------------------------
342882
7380
Sách đưa ra các bước giúp cho mỗi người biết cách cân bằng cuộc sống và công việc. Ngoài ra, sách còn cung cấp cho người đọc những kỹ năng cần thiết trong cuộc sống phù hợp với mọi người đọc điển hình nhất là sinh viên đại học, người đi làm,...

Bên cạnh đó, sách cũng đưa ra cho người đọc những phương pháp nhìn nhận tích cực hơn ở trong hoàn cảnh khó khăn của cuộc sống hay công việc. Bìa sách và giấy rất đẹp, nội dung dịch chuẩn với bản gốc nhưng giá sách thì hơi cao.
3
88523
2015-11-25 11:26:23
--------------------------
267623
7380
mình rất hâm mộ tác giả viết sách này, ông ấy viết rất nhiều sách về kỹ năng sống rất hay, phù hợp cho mọi thời đại. nhưng sách này hay, ý nghĩa đấy nhưng quá đắt 198.000vnd luôn mà không được giảm giá gì cả.các sản phẩm cùng loại của tác giả này vừa rẻ, hay mà lại được giảm giá nhiều nữa. sách này mà hạ giá sản phẩm xuống 1 xíu thì chắc sẽ nhiều người mua, để mọi người đều được đọc sách. mà mình thấy mấy thể loại sách này nay trên audio book rất nhiều mà lại miễn phí nữa.
nói chung mình mua rồi nên sẽ trân trọng nó. 
3
744062
2015-08-15 18:28:34
--------------------------
