166001
9063
Đã lâu lắm rồi mới lại có một tác phẩm khiến tôi phải say mê đến quên ăn, quên ngủ, hồi hộp theo từng dòng chữ và suy nghĩ về nó mỗi khi đi ngủ. Tác giả bắt đầu viết văn ở tuổi 70 nên tác phẩm của bà, dù đầu tay cũng không thể xem thường.

Chắc hẳn bạn sẽ không ngờ rằng câu chuyện của tác giả chỉ là câu chuyện của một người phụ nữ kể lại cuộc đời đi du học của mình, rồi lấy chồng, có con và định cư ở nước ngoài. Vậy mà khiến tôi đảo lộn cả sinh hoạt vì đọc sách? Bởi vì câu chuyện được bà kể một cách rất tự nhiên, bình thường lại ẩn chứa những điều lớn lao, phi thường.

Là một du học sinh nhưng nhân vật chính là một trong những người Việt nam đầu tiên được cử đi du học tại một nước Xã hội Chủ nghĩa anh em: Ba Lan vào năm 1955. Cô bé An chỉ là một học sinh phổ thông, không biết gì về chính trị, chỉ biết cố gắng vì lý tưởng mà cả nước cùng đồng lòng. Cho đến tận lúc lớn tuổi, bà vẫn là một người không quan tâm đến chính trị nhưng cuối cùng cuộc đời bà lại biến động khủng khiếp vì chính trị và liên quan mật thiết đến chính trị.

Chuyện đi học của du học sinh, sau năm mươi năm mà vẫn có những điều  ngày nay còn nguyên giá trị. Chuyện học hành, chuyện sinh hoạt và đặc biệt là chuyện yêu đương. Du học sinh Việt Nam thời đó không được phép yêu nhau và đặc biệt là yêu người ngoại quốc. An đã phạm vào điều cấm kỵ. Cô bị giằng xé giữa cuộc đấu tranh cho tình yêu, lý tưởng, Tổ quốc và lẽ phải. Kỷ luật cực kỳ nghiêm khắc và nhiều phần cứng nhắc của chính phủ thời kỳ đó khiến cho sự xung đột lên đến đỉnh điểm và An bị áp giải về nước như một tội phạm. Phải có sức mạnh ý chí và tinh thần mạnh mẽ lắm, một cô gái mới dám đương đầu với những thử thách lớn lao đến vậy. May mắn thay bên cạnh cô là người chồng tương lai mà đến bây giờ cô vẫn thỏa mãn là đã không chọn nhầm, là những người Ba Lan tốt bụng vô hạn vì họ cũng hành động cho quyền con người. An đã đấu tranh và chấp nhận "mọi giá" để đạt được điều mình khát khao (mà tưởng như không thể thực hiện được) ra sao bạn đọc phải đọc từng câu mới thấy thấm!

Tác giả chọn thể loại tiểu thuyết là rất hợp lý, vì nếu viết là tự truyện hay hồi ký có lẽ người ta sẽ không tin vào một câu chuyện kỳ diệu như vậy. Bà đã phải tự học lại tiếng Việt để viết sách, giữ gìn ngôn ngữ mẹ đẻ bằng cách nói và hát một mình. Bà nói và viết thành thạo ba thứ tiếng thuộc hàng khó nhất thế giới: tiếng Việt Nam, tiếng Na Uy, tiếng Ba Lan, là một trong những người Việt đầu tiên định cư ở Ba Lan, và một trong những người Ba Lan đầu tiên định cư ở Na Uy. Nội những điều đó đã khiến câu chuyện của bà rất đáng để đọc. Tuy nhiên nhà xuất bản biên tập còn nhiều thiếu sót khi để lại rất nhiều lỗi chính tả, với một người sống 40 năm liên tục ở nước ngoài, viết được như vậy là quá giỏi nhưng những lỗi cơ bản thì người hiệu đính lẽ ra phải sửa, dù tác phẩm vẫn hoàn toàn chấp nhận được.

Cuốn sách không đơn thuần là cuộc đời một cá nhân, nó là một phần của lịch sử đất nước, ẩn chứa trong đó những bí mật về cả một dân tộc.
5
114275
2015-03-11 18:50:57
--------------------------
69931
9063
Ngã ba đường là một câu chuyện hết sức sống động và chân thực kể về mối tình "xuyên biên giới". Ở họ có một nghị lực hết sức phi thường, chính tình yêu đã mang đến cho họ động lực sống, họ cố gắng, cố gắng chỉ mong có được một hạnh phúc giản đơn.Nhân vật An đã cho ta cảm nhận về cuộc sống những năm thế chiến thứ hai, chúng thật khắc khổ, để vượt qua tất cả nỗi đau, vươn lên sống tiếp quả thật không phải là một điều đơn giản chút nào.Thế nhưng An đã làm được!Câu chuyện còn cho ta hiểu thêm về hành trình du học của những du học sinh miền bắc Việt Nam, cho ta hiểu thêm về hành trình nổ lực để cứu Đất Nước thoát khỏi nguy cơ bị xâm chiếm... 
4
101421
2013-04-18 14:40:24
--------------------------
