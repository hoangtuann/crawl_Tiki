473916
7352
Doremon là bộ truyện tranh hay nhất trước giờ mà tôi từng đọc. Tiếp nối "Doraemon - Đôi thám hiểm tại sao - tập 1" rất hay và thú vị thì tập 2 cũng hay và thú vị không kém. Những kiến thức bổ ích về cuộc sống và khoa học rất có ích và lý thú nhưng đôi khi chúng ta lại không để ý đến thì bây giờ đã được thể hiện trên chất liệu truyện tranh với những hình vẽ thú vị và đi cùng đó là những câu chuyện phiêu lưu của năm người bạn thân quen: Doraemon, Nobita, Chaien, Suneo và Shizuka.
5
60352
2016-07-10 22:38:43
--------------------------
456203
7352
Nội dung không có hấp dẫn như những câu chuyện ngắn trước đây . Nếu là trẻ em đọc hẳn sẽ dễ thấy chán , nhưng bên cạnh đó thì nó cũng rất bổ ích cho những kiến thức cơ bản của lịch sử như tên , năm sinh , sáng chế của các nhà khoa học . Cốt truyện mang tính học tập hơn là vui chơi giải trí . Tiki đóng hàng kỹ , giao hàng nhanh chống tiện lợi . Về chất lượng sách cũng bình thường không tệ như cái cmt khác nói . Nói chung mọi thích cũng ổn giá cả cũng không quá mắc. 
4
1088194
2016-06-23 11:45:42
--------------------------
442523
7352
Nội dung truyện rất ổn, dịch thuật tốt, nét vẽ mô phỏng tác giả Fujiko còn khá cứng, phản ứng biểu cảm của nhân vật tuy có phần phóng đại nhưng về mặt bằng chung nét vẽ mô phỏng này có thể chấp nhận được! Tôi rất hài lòng về cách xây dựng cốt truyện khi nhìn thấy các bảo bối quen thuộc ở các bộ truyện gốc được xuất hiện và áp dụng rất phù hợp với hoàn cảnh! Tôi thích nhất vụ án Conan Doyle trong tập 2, rất hài hước! 
Tuy nhiên điểm trừ lớn là cách đóng bìa sách quá tệ, sách cứ như bị mở banh ra quá nhiều lần khiến cho các trang giấy gần như sắp tách ra khỏi cuốn đến nơi! Riêng tập 2 thôi mà sách đã bị tróc ra 2 trang truyện! Tôi cực kỳ thất vọng ở mảng đóng sách!
3
516261
2016-06-05 09:51:01
--------------------------
392330
7352
Mình mua 2 quyển "Doraemon - Đội thám hiểm tại sao" về làm quà cho em. Thật may cho mình vì lúc mình đặt hàng chỉ còn lại 1 sản phẩm. Nội dung truyện thì quá tuyệt, cung cấp cho chúng ta rất nhiều kiến thức bổ ích về các vĩ nhân mà không kém phần hài hước, hấp dẫn. Tiki giao hàng rất nhanh.Thế nhưng mình khá khó chịu vì khi mua về mới lật ra thì phát hiện giấy bị tách khỏi mép hơn 1/3 quyển. cảm giác từng tờ từng tờ đang đọc mà cứ bị sứt ra thật chẳng vui tí nào. mong rằng nhược điểm này sẽ được khắc phục !
4
1156779
2016-03-06 18:58:31
--------------------------
389466
7352
Tập 2 dường như có sự tiến bộ hơn về nội dung, cách thức giải quyết tình huống cũng đỡ gượng ép hơn so với tập 1. Tuy nhiên, điểm trừ ở đây vẫn là việc nét vẽ của tác giả khiến tạo cảm giác nhân vật biểu hiện cảm xúc thái quá, có lẽ do mình hơi khắt khe chứ nhiều đoạn đọc thấy rất khó chịu. Nhưng dù sao cũng không thể đòi hỏi nhiều quá vì dù sao tác giả đâu phải Fujiko. Tóm lại thì đã có tập 1 nên mình mua nốt tập 2 cho đủ bộ, mình cũng không cảm thấy tiếc tiền khi mua bộ này vì dù sao mình cũng có ý định sưu tầm tất cả các sản phẩm liên quan đến Doraemon. Nếu bạn là fan của Doraemon thì cũng nên cân nhắc việc cho sản phẩm này vào bộ sưu tập của mình.
3
711206
2016-03-02 06:39:46
--------------------------
373521
7352
Cốt truyện khá hay nói về những vĩ nhân thế giới,nhóm doraemon xuất hiện và phá rối những cuối truyện thì sự thành công của phát minh hoặc phát kiến trùng khớp với cuộc đời của vĩ nhân,bề ngoài truyện khá đẹp nhưng khi mở truyện thì ngược lại.Nếu lật sách tí xíu thôi ở khe giữa thì trang truyện sẽ tách ra do keo kém chất lượng hoặc làm không kĩ  quá kém chất lượng mà đây là truyện tranh nói về vĩ nhân nên mình mới mua nhưng thật thất vọng vì khi lật trang truyện thì tranh đã tách ra khỏi truyện
2
1073793
2016-01-23 22:31:54
--------------------------
299778
7352
Tập 2 chất lượng cũng vẫn như tập 1 vậy. Tiếp tục chuyến hành trình từ tập 1, Nobita và Mèo Ú cùng chú robot giúp đỡ lại được gặp gỡ thêm nhiều nhân vật vĩ đại trong lịch sử. Qua những chuyến phiêu lưu vượt thời gian và không gian, từ quá khứ đến tương lai đầy thú vị đó, chúng ta sẽ vừa được cười vui vẻ bên những tình huống hài hước đáng yêu của Nobita, vừa có thêm những kiến thức về danh nhân, những người nổi tiếng trong lịch sử. Một tập truyện hết sức ý nghĩa và thú vị. 
5
340213
2015-09-13 15:10:40
--------------------------
