454016
3307
Với lời văn tự nhiên, đơn giản mà gần gũi. Anh đã viết nhưng câu chuyện về mình và truyền cho người đọc cảm hứng vượt qua chính mình. Vượt qua nhưng chướng ngại trong cuộc sống để tiến đến thành công.
Mình đã mua 5 quyển để tặng cho bạn bè và đồng nghiệp (những người mình rất trân trọng :) ).
Quyển sách giá chị có 70k nhưng giá trị của nó thì vô biên.
Có lẻ nhiều bạn không tin, nhưng hay thử 1 lần tìm hiểu về bầu Thuận là ai trước khi quyết định mua một quyển sách cho mình. :) Anh là một người nhiệt tình, năng động  luôn cống hiến mình cho tuổi trẻ!!! 
4
966237
2016-06-21 17:44:26
--------------------------
381232
3307
Đây là một cuốn sách đáng để bạn đọc và chiêm nghiệm khi bạn đang hoang mang cho con đường đi tương lai của chính bản thân bạn. Bạn sẽ học được nhiều bài học đáng giá, nghiền ngẫm với những câu chuyện rất đời của một tác giả rất chất.  Khó có thể tìm được quyển sách vừa ngắn gọn, vừa súc tích như thế trong hàng trăm đầu sách truyền động lực và hay hơn nữa nó do một tác giả Việt Nam viết, nó phù hợp với thời cuộc hiện đại này. Tin tôi đi nếu bạn đang lạc lối , bạn sẽ biết mình sẽ phải đi đâu, sẽ phải làm gì khi đọc cuốn sách này.
5
1136694
2016-02-16 20:54:04
--------------------------
346606
3307
"Vui chơi để kiếm sống"
Tình cờ được người bạn cho mượn quyển sách này- được mua ở Tiki, suy nghĩ ban đầu của mình là tò mò, không biết tác giả sẽ viết về điều gì.
Bỏ qua phần thông tin tác giả, mình dường như bị cuốn theo: từng lời văn, từng câu chữ- hết sức nhẹ nhàng, mộc mạc và thân quen.
Đọc quyển sách để chiêm nghiệm về cuộc sống.
Đọc quyển sách để thấy yêu công việc của mình.
Và đọc quyển sách để tìm cách hài lòng với cuộc sống cũng như cách để kiếm tiền từ công việc hiện tại.
Suy cho cùng: hãy làm công việc mình yêu thích rồi sao đó kiếm tiền từ sở thích đó!
3
836327
2015-12-03 00:25:19
--------------------------
319088
3307
Một cuốn sách để lại ăn tượng đặc biệt, thực tế, những kỉ năng sống không thể học được ở trường lớp, kể cả bạn học ở đường đời thì mấy ai đủ để sâu sắc nhận thấy được. Từng bỏ một công việc ổn định để đi tìm ước mơ, có thể đường phía trước khó khăn. Sống là sống cho mình, làm những thứ mình muốn tìm một tình yêu đích thực ... Là công việc. Làm việc mình muốn sẽ phát huy được tất cả nhiệt huyết tuổi trẻ. Cảm ơn tác giả đã cho thêm động lực để bước tiếp. 
4
453748
2015-10-07 21:01:59
--------------------------
313874
3307
Một tác phẩm mà ngiều lỗi chính tả thì mình thấy độc giả không được xem trọng. Trên hết nội dung nói về việc nếu bạn làm công việc bạn đam mê thì sẽ thành công nhưng quá chung. Điều mà ai cũng có thể nghe nhan nhản trên cái nài báo hay trang web nói về thành công. Cách kể chuyện không thu hút lắm, nội dung câu chuyện hơi nhàm. Tóm lại là mua rồi nên cố đọc hết xem vớt được gì không. Cuối cùng là đọc xong và thất vọng nhiều so vớinhững gì tác giả và báo chí pr. 
2
668069
2015-09-24 20:48:31
--------------------------
302997
3307
Cùng với sự tò mò của mình cộng với sự hấp dẫn của tựa sách nên mình chọn mua "Vui chơi để kiếm sống". Đọc phần một mình rất có cảm tình nhưng khi chuyển qua phần tiếp thì mình khá chán vì cứ đều đều giống phần trước. Vì cuốn sách này được một người tài hoa là MC Thanh Bạch đánh giá nên mình đổi cách đọc. Đọc ngược. Đọc từ bài sau cùng đọc lại. Đọc rồi lại không dứt ra được. Thầy sẻ chia những bài học nhưng không áp đặt phải làm như thế này hay phải thực hiện như thế kia. Thầy điềm đạm chia sẻ những trải nghiệm của chính thầy. Từ đó giúp mình ngẫm ra những bài học thật sự bổ ích. Thật sự biết ơn những sẻ chia từ thầy.
4
247455
2015-09-15 16:26:13
--------------------------
294279
3307
"Vui chơi để kiếm sống" là một quyển sách hay và ý nghĩa.Nó mang đến cho người đọc nhiều giá trị trong cuộc sống,chia sẻ những bí quyết trong cuộc sống thường ngày dưới cách viết hóm hỉnh của Bầu Thuận.Và đúng như lời nhận xét của MC Thanh Bạch về cuốn sách này "Có thể nói quyển sách này là một người bạn đồng hành.Bất cứ khi nào bạn gặp bế tắc,chán nản hay mất động lực,mất niềm tin trong cuộc sống,hãy lật quyển sách ra,đọc bất cứ trang nào,bạn sẽ tìm được câu trả lời cho chính mình"
5
632144
2015-09-09 09:55:37
--------------------------
293094
3307
Không biết nhiều về tác giả, do thấy tựa sách thú vị nên tìm đọc thử. Sách quả thật rất thú vị với 1 nghề tưởng chừng khô khan: quản lý người nổi tiếng. Tác giả có văn phong hài hước với lối sống cháy hết mình cho đam mê rất đáng để học hỏi tinh thần làm việc. Cuốn sách cho tôi nhiều niềm tin hơn trong cuộc sống, tìm ra cho bản thân điều mình đam mê nhất và sẽ cố gắng với tất cả khả năng của mình... vì như 1 câu nói trong sách: Làm được việc mình yêu thích là cả đời không cần phải làm việc!
5
801108
2015-09-07 23:02:11
--------------------------
289459
3307
Đợi Thầy ra sách mà cũng lâu lắc lâu lơ, nhưng giờ mua về đọc rồi mới thấy đợi chờ là hạnh phúc mà. Sách của Bầu Thuận không qua hào nhoán, hoa mĩ; ngôn ngữ rất dân giả, và thân quen; dôi khi mình thấy một ít bản thân ở trong đó, rất sinh viên, và cũng rất trưởng thành. Sách tuy không dài, nhưng có đủ cung bậc cảm xúc. Từ những trải nghiệm cuộc sống của tác giả. Đúng chất vui chơi để kiếm sống, tuy không mang tính học thuật gì nhiều, nhưng đây là câu chuyện về cuộc đời, rất đáng để suy ngẫm.
Sách nhẹ và mỏng, nên đọc cũng nhanh lắm.
5
420871
2015-09-04 15:03:08
--------------------------
281259
3307
Quyển sách "VUI CHƠI ĐỂ KIẾM SỐNG" của tác giả Huỳnh Minh Thuận đã góp phần tạo thêm niềm vui cho tôi. Bỡi lẽ, nó có một cái duyên gì kinh khủng lắm thôi thúc tôi đọc ngấu nghiến từ trang này sang trang khác, mà quên hết ưu phiền cuộc sống,...
Đây là một quyển sách hay, hay ở chỗ nó tập hợp những mẫu chuyện ngắn với một lối hành văn giản dị nhưng lôi cuốn đến lạ lùng, đằng sau những câu chuyện đó là những thông điệp mà tác giả muốn gửi gắm đến chúng ta. Tuỳ từng độ tuổi mà có những suy nghĩ và chiêm nghiệm khác nhau, rồi rút ra những bài học riêng cho mình. 
Đọc sách này tôi thích nhất câu "HẠNH PHÚC LÀ DÁM SỐNG VỚI NIỀM ĐAM MÊ - THÀNH CÔNG LÀ BIẾN ĐAM MÊ THÀNH THU NHẬP BỀN VỮNG" ❤️❤️❤️
"Bạn đang trăn trở với cuộc sống này, bạn đang mất phương hướng, bạn đang gặp nhiều áp lực công việc và cuộc sống, bạn không biết đam mê của mình là gì,.. Bạn sẽ tìm được câu trả lời qua quyển sách này".
Hãy lắng nghe câu chuyện của TÁC GIẢ, bạn sẽ hiểu thôi mà! 
Cuối cùng,
Cho tôi xin gửi một lời cám ơn chân thành nhất đến với tác giả, tác giả đã chia sẻ những lời hay ý đẹp cho đời, và lời cám ơn tới nhà xuất bản đã cho ra một ấn phẩm hay đến với mọi người! 
5
319540
2015-08-28 11:47:18
--------------------------
270442
3307
Tôi biết đến tác giả là một diễn giả, khi biết mới ra sách Vui Chơi Để Kiếm Sống (Tác giả Huỳnh Minh Thuận) và tôi đã mua và đọc. "Khi làm những điều mình đam mê thì bạn không phải làm việc, mà là 'thưởng thức công việc'. khi đó bạn không phải sống, mà là: VUI CHƠI ĐỂ KIẾM SỐNG" khi đọc câu này ở bookmark tôi đã thực sự ấn tượng với cuốn sách này.
HAY!
Những câu chuyện của tác giả rất có sức hút và ảnh hưởng đến suy nghĩ rất nhiều người và cả tôi. Những bài học kinh nghiệm này đã tạo cho tôi thêm động lực về niềm đam mê của mình để tôi có "VUI CHƠI ĐỂ KIẾM SỐNG"
5
504087
2015-08-18 11:26:28
--------------------------
269089
3307
Anh Huỳnh Minh Thuận là cựu sinh viên trường mình. Mình đã theo dõi Anh trên mạng xã hội facebook cũng như những bài báo của Anh và tất nhiên sẽ không thể bỏ qua cuốn sách này. Đặc biệt với những bạn mới ra trường giống mình, phải biết vui chơi, tụ tập bạn bè, tham gia các hoạt động xã hội...và nhất là đi du lịch để có một tinh thần thoải mải, tự do sau đó là những thành quả trong công việc. Làm việc và tận hưởng cuộc sống là hai khái niệm luôn song hành với nhau. Một cuốn sách đáng đọc cho các bạn trẻ.
5
736284
2015-08-17 08:41:46
--------------------------
252607
3307
Rất bất ngờ vì quyển sách khá mỏng,không như mong đợi của mình.Tuy nhiên,không vì thế mà mất đi cái hay của nó.Với lối viết gần gũi,dí dỏm,của một người từng kinh qua thất bại lẫn thành công,Bầu Thuận đã cho mọi người những câu chuyện đời thường rất đáng chiêm nghiệm,những lời khuyên rất hữu ích cho giới trẻ,đó là:Bạn đam mê cái gì,nghiện cái gì,thì cũng có thể kiếm tiền từ cái đó,do đó,hãy cứ mạnh dạn thử nghiệm,sống với đam mê của mình,làm cũng như chơi ,mà chơi cũng như làm,khi đó mới chính là Vui chơi để kiếm sống!
5
612956
2015-08-03 20:06:59
--------------------------
242517
3307
Cuốn sách không thật thú vị như bìa sách và những lời tựa. Tác giả Huỳnh Minh Thuận chia sẻ những dòng cảm xúc cũng như phương châm sống của anh - một người trẻ, tuy nhiên đâu đó ta thấy có những điều quen thuộc đã gặp trên mạng, trong những cuốn sách hạt giống tâm hồn... Bên cạnh đó, những bài viết ý tứ khá ngắn cùng lối hành văn không đặc sắc, cuốn hút khiến cho độc giả mất đi sự hào hứng để lật trang. 
Thêm một điều để phàn nàn, là sách quá mỏng so với giá tiền của nó, với 70 nghìn đồng, người đọc xứng đáng nhận được nhiều hơn :)
2
18937
2015-07-26 19:12:39
--------------------------
238781
3307
Mới đầu khi mua hàng cứ ngỡ nó sẽ to bành ky hoặc chí ít cũng cả 200 trang nhưng mua về cầm trên tay......choáng bởi độ mòng và độ bé của ẻm. Nhưng khi bắt đầu đọc rồi thì thôi! Lướt lướt không ngừng nghĩ....nó mang một sức ảnh hưởng khá lớn. Lần đầu tiên đọc được nữa cuốn tôi lại muốn đọc thêm vài lần nữa. Phong cách dí dỏm, hài hước, không nặng câu từ khiến bạn khó có thể bỏ nó xuống khi đã lướt được vài trang. Nếu bạn sợ tốn thời gian hoặc dài đối với những quyển sách mang tín làm giàu, làm việc thì hãy đọc sách của chú Thựn này đi! Tuy nhiên có vài lỗi sai chính tả cũng như câu từ phía sau sách khiến bạn đang cao hứng thi khá khó chịu. Mong rằng những lần tái bản sau hoặc những sản phẩm mới của chú Thựn không có....Rất vui vì cùng quê với tác giả! 
Nói tóm lại bạn đọc bạn sẽ thấy nó ý nghĩa và hay đến mức nào!
5
442742
2015-07-23 14:04:09
--------------------------
230103
3307
Tôi không dám khẳng định mình đã đọc nhiều sách. Một số sách tôi đọc nói về  cách kiếm tiền, tạo thói quen, động lực làm việc cho chính mình,.. như Ngày làm việc 4h, 7 thói quen để thành đạt, Quẳng đi gánh lo và vui sống, Dạy con làm giàu,... Những cuốn sách này nếu áp dụng tốt đơn giản chỉ 1 cuốn thôi tôi có thể kiếm được tiền nhưng liệu nó có làm cho tôi vui không. Thì "Vui chơi để kiếm sống" mới chính là cuốn sách giúp tôi xác định hướng đi đúng với khả năng cũng như đam mê của chính mình. Nếu văn phong của người nước ngoài thường là một câu chuyện dài xuyên suốt cho một nhân vật trong quá trình đi đến ước mơ, ước muốn của mình như "Nhà Giả Kim" hay " Suối Nguồn" thì cuốn sách "Vui chơi để kiếm sống" của tác giả Huỳnh Minh Thuận lại theo một văn phong cực kì vui nhộn, thích hợp cho những bạn trẻ. Những mẩu chuyện ngắn của anh tưởng chừng như chỉ kể lại việc mà anh đã trải qua, vô thưởng vô phạt nhưng lại chứa đựng nhiều ý nghĩa làm người đọc phải nghiền ngẫm rồi rút ra bài học cho riêng mình. 
5
469653
2015-07-17 11:21:01
--------------------------
223957
3307
"Vui chơi để kiếm sống" rất khác với các cuốn sách khác, bởi nó thực sự mỏng và đặc biệt rất nhẹ, cầm trên tay không nghĩ là 1 cuốn sách. Nhưng bù lại đây là 1 cuốn sách khá thú vị, nói về những trải nghiệm thực tế của tác giả, và một góc nhìn tiến bộ về cuộc sống, cũng như tình người ấm áp với những câu chuyện về những việc làm tưởng chừng nhỏ bé nhưng không mấy người làm được. Cảm ơn tác giả, và Tiki đã cho mình có thêm những cái nhìn khác về cuộc sống.
4
634712
2015-07-07 21:07:25
--------------------------
213090
3307
Thật sự khi nhận được sách mình cảm thấy rất bất ngờ, "Vui chơi để kiếm sống" rất mỏng so với mình tưởng tượng.
Đọc hết cuốn sách trong 1 buổi chiều cảm giác cứ lên lên xuống xuống bất chợt theo từng sự việc mà tác giả trải qua, những kinh nghiệm thực tế mà anh đã đúc kết. 
Thật sự quyển sách này bổ ích cho những ai đang muốn tìm phương hướng đúng đắn cho lối đi của bản thân, nhưng để làm động lực thúc đẩy như giới thiệu thì thật sự theo mình là không nhiều.
Còn về phần cuối quyển sách thì thật sự mình không thể hiểu nỗi dụng ý của tác giả khi chèn thêm những mẫu truyện ngắn mà theo mình thấy thì nó không hề liên quan gì hết ?
4
60542
2015-06-23 11:44:16
--------------------------
209856
3307
Cuốn sách đầu tiên của anh Bầu Thuận.
Vẫn thường xuyên theo dõi và đọc các bài post của anh trên face book. Nó thực sự thú vị và giúp em được nhiều lắm.
Còn cuốn sách này đó chính là kinh nghiệm thực tế của anh.
Ngôn ngữ anh sử dụng hết sức gần gũi với tụi em, đọc mà học hỏi được nhiều lắm,
Chờ đón cuốn sách tiếp theo của anh.
Hy vọng sẽ sớm có.
Mua ở tiki rẻ hơn nữa, hehe. Hôm bữa thấy anh ưu tiên cho bao nhiêu bạn đó sẽ có chữ kí và lời chúc của anh mà tiếc là đã trễ rồi.
5
100385
2015-06-18 12:02:42
--------------------------
