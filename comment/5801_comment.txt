297160
5801
nếu bạn đang tìm một cuốn sách giúp bạn vừa luyện phát âm, luyện nghe và cả luyện nói thì bạn không thể bỏ qua Listen In 2 của David Nunan.Trang bìa được thiết kế rất đẹp và bắt mắt người đọc.về phần nội dung, cuốn sách này được viết theo 20 unit khác nhau, trong mỗi unit được chia ra từng phần nhỏ giúp người đọc dễ dàng nắm bắt được nội dung.Bên cạnh cuốn sách này còn được trang bị 4 CD giúp bạn luyện nghe một cách dễ dàng.Trong mỗi CD có từng task rất chi tiết.Tóm lại nếu bạn đang muốn tìm một cuốn sách giúp bạn nghe nói phát âm tốt thì không nên bỏ qua Listen In 2.
5
803125
2015-09-11 16:48:25
--------------------------
289334
5801
Hình thức: Sách được in đẹp, bìa cứng, màu sắc đẹp. Giấy bên trong trắng đẹp, chữ in rõ ràng

Bộ sách listen in này được viết và trình bày ngắn ngọn dễ hiểu, theo từng chủ đề nhỏ. File nghe trong CD rất rõ ràng, đúng giọng chuẩn, dễ nghe. Giúp cải thiện được đáng kể khả năng nghe và nói, giúp phản xạ nhanh, nói chuẩn.

Bộ sách này gồm 3 quyển, mình đã học hết, bây giờ mua lại để tặng cho một em khóa dưới. Hy vọng em ấy cũng sẽ cải thiện tốt khả năng nghe nói tiếng anh như mình.
5
409134
2015-09-04 13:15:51
--------------------------
