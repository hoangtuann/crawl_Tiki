293158
5396
Hình thức sách đẹp, bìa đẹp, giấy tốt, trình bày ngắn gọn bắt mắt. Nhẹ nhàng dẫn lối cho tư duy tích cực. Lối viết súc tích về nhiều khía cạnh trong cuộc sống đầy tính chiêm nghiệm, giúp người đọc suy ngẫm và tìm ra hướng suy nghĩ, hành động hữu ích cho bản thân và những người xung quanh. Mẹ mình cũng khá thích cuốn sách này và khá phù hợp với những người thích chiêm nghiệm như Mẹ. Song nếu đọc nhiều có thể thấy những điều này ta đã biết, cái mới và tính đột phá khó bắt gặp trong tác phẩm, cá nhân mình coi đây là một cuốn sách có tính nhắc nhở nhiều hơn.
1
492988
2015-09-08 01:23:00
--------------------------
220095
5396
Cuốn sách này nói về nhiều khía cạnh, cảm xúc, tính cách và ý nghĩa của cuộc sống. Mỗi khía cạnh dù không diễn giải nhiều nhưng cũng làm cho bạn cảm thấy phải suy nghĩ lại về bản thân, về cuộc sống. 
Cuốn sách này chất lượng giấy tốt, trình bày ngắn gọn dễ hiểu, giá thành tốt.
Sau mỗi chủ đề là một câu danh ngôn để chốt lại vấn đề đó, nếu chẳng may bạn không thể hiểu hết ý mà tác giả muốn truyền đạt thì những câu danh ngôn là một sự lựa chọn dành cho bạn.
Cảm ơn tác giả và cảm ơn Tiki.
4
448771
2015-07-02 11:27:43
--------------------------
199023
5396
Tôi từng là một người có những suy nghĩ bi quan, chán chường trước cuộc sống tẻ nhạt của mình. Và rồi tình cờ, tôi đọc được quyển sách này. Tôi thực sự như đã tìm thấy được chính mình, đã có dịp tự chiêm nghiệm lại chính bản thân cũng như thấy bước trưởng thành lớn trong cuộc đời mình.Tác giả đã đưa ra những cái nhìn hết sức chân thực và đầy nhân văn về thái độ, cách sống của con người. Đọc nó, tôi đã có những cái nhìn khác biệt, cách sống khác biệt, có lẽ là theo hướng tích cực hơn. Tôi nhận ra qúa khứ dù tươi đẹp hay đau thương thì con người vẫn không thể sống mãi với nó. Hãy trân trọng quá khứ như một bài học, cất giữ nó thật kĩ trong ngăn kí ức của riêng mình. Sống thật tốt với những gì đang diễn ra ở hiện tại và tương lai. Luôn hi vọng và tin tưởng bạn sẽ nhận ra nhiều phép màu trong cuộc sống.
4
635412
2015-05-21 12:43:00
--------------------------
170286
5396
Với quyển sách "Quên Hôm Qua - Sống cho ngày mai" bạn đọc sẽ có thêm nhiều nghị lực trong cuộc sống... 
Ngay từ tựa đề quyển sách chúng ta đã thấy được một thông điệp hết sức đơn giản là "quá khứ là cái đã mất đi - không bao giờ lấy lại được, còn tương lai là cái đang đến"...
 Mỗi con người chúng ta đều có quyền sống cũng như chúng ta có quyền có được một tương lai tươi sáng! Quá khứ chỉ đơn giản là những bậc thang trải đầy khó khăn...có qua rồi thì chúng ta mới có thể đến được một khỏang trời tươi sáng... Hãy nhớ là "khi một cánh cửa đóng lại, sẽ có một cánh cửa khác được mở ra" và nó không dành cho ai khác, đó chính là bạn!
Đây là quyển sách đáng đọc cho những ai đang bế tắc trong cuộc sống!
5
558557
2015-03-19 20:10:00
--------------------------
