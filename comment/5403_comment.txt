217792
5403
Ấn tượng đầu tiên về quyển sách chính là bút danh vừa quen vừa lạ của tác giả. Sách trình bày bìa rất đẹp nổi bật với hình biếm họa của tác giả rất dễ thường và đáng yêu. Nội dung quyển sách gồm 58 câu chuyện thú vị được tác giả kể lại một cách chân thực và rất sáng tạo, từ chuyện trong nhà đến việc ngoài ngõ, chuyện gì cũng có, chuyện gì cũng có thể "chém gió" được. Tất cả đã làm nên một "xóm nhà lá" nhộn nhịp, đúng như với tên của quyển sách "Ngồi lê chém gió". Quyển sách cũng đã đem lại cho mọi người những tiếng cười nhẹ nhàng sau những giờ làm việc căng thẳng. 
5
408969
2015-06-29 23:06:54
--------------------------
