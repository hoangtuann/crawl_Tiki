468180
5095
Một truyện tình cảm động và mang lại nhiều giá trị sâu sắc là những từ dành cho tác phẩm này. Một sản phẩm văn học Thái Lan mà mình lần đầu tiên trải nghiệm khiến mình vô cùng bất ngờ. Lời văn được chau chuốt, không hoa mĩ nhưng chân thực và trong sáng đến nao lòng. Đây thật sự là một tác phẩm đáng đọc, chất lượng giấy và in ấn đều tốt, cuốn sách có bìa ấn tượng và trông rất lãng mạn. Cảm ơn Tiki đã mang đến cho độc giả tác phẩm ý nghĩa này với mức giá tốt.
5
280989
2016-07-04 20:43:44
--------------------------
448037
5095
Mình chưa bao giờ đọc văn học Thái Lan (thường đọc văn học phương Tây và Trung Quốc hơn). 
Nhưng khi đọc giới thiệu của Tiki thì cảm giác đây sẽ là một câu chuyện hấp dẫn.
Và quả thật, khi nhận sách, rồi cuốn theo những trang sách nhẹ nhàng, sâu lắng, để đến khi gấp lại rồi, dư âm và những cảm xúc trong đó vẫn còn đọng lại.
Sau này có xem một bộ phim hoạt hình của Nhật (Garden of Words) thì những dư âm của Đằng sau Bức tranh lại tìm về.
Một cuốn sách đáng đọc.
5
441712
2016-06-15 11:25:34
--------------------------
422719
5095
Khi định viết review tôi nghĩ tôi sẽ viết nhiều lắm, nhưng lúc bắt đầu viết thì lại chẳng biết viết như thế nào, viết từ đâu. 
Đây là một chuyện tình đẹp nhưng buồn. Một tình yêu đến rất đơn giản và đi như lẽ tất nhiên. Tình yêu Noporn dành cho công nương Kirati là tình yêu mãnh liệt, cuồng si của tuổi trẻ; còn tình yêu công nương Kirati dành cho Nopporn lại là một tình yêu âm ỉ và lặng lẽ. Hai người có tình nhưng chẳng thể đến với nhau. Một người quên còn một người vĩnh viễn ghi nhớ. Kirati đã mong mỏi được một lần yêu và được yêu, dù không có kết quả, nhưng cô đã hạnh phúc.
"Ta chết mà không có người yêu ta, nhưng ta mãn nguyện vì tìm được người ta yêu". 
5
291719
2016-04-27 21:54:54
--------------------------
418407
5095
Toàn bộ câu chuyện chỉ xoay quanh tình cảm của hai nhân vật chính: Nopporn (22 tuổi) và công nương Kirati (35 tuổi). Mặc dù có nhan sắc trẻ đẹp và trí tuệ hơn người, nhưng bị thần tình yêu bỏ quên nên đến năm 34 tuổi công nương Kirati mới lập gia đình lần đầu với 1 hầu tước vừa mới mât vợ (lớn hơn nàng khoảng 15 tuổi). Chênh lệch tuổi tác giữa hai con người cùng lớn tuổi, cùng suy nghĩ chính chắn, cùng lối sống yêu sự bình yên không đem đến cái gọi là tình yêu đúng nghĩa cho nàng, bản thân nàng chấp nhận cuộc sống đó và cảm thấy hạnh phúc. Đến một ngày, cuộc sống tưởng như bình lặng đó bỗng chốc trở nên xáo trộn vì tình yêu cuồng nhiệt của chàng trai trẻ dành cho nàng, bằng sự chính chắn của một người trưởng thành đằm thắm, công nương Kirati đã từng bước dìu dắt chàng trai trẻ hướng cuộc đời mình vào học hành để đảm bảo cho sự nghiệp tương lai, từ đó chàng trưởng thành trong công việc và cuộc sống mà không có tình yêu. Để lại công nương 1 mình âm thầm đau khổ trong mối tình câm lặng. May mắn là, cuối cùng chàng đã nhận ra chân tình của nàng trước khi nàng lìa đời qua lời nhắn:
"Ta chết mà không có được người yêu ta
Nhưng ta mãn nguyện vì đã tìm được người ta yêu".
Cách tác giả diễn đạt thì thấy mọi chuyện có vẽ nhẹ nhàng, nhưng khi hiểu kỹ thì thấy vô cùng sâu sắc.
5
357674
2016-04-19 09:49:35
--------------------------
417525
5095
Sự phân biệt giai cấp trong xã hội phong kiến ở Thái Lan, những luật lệ hà khắc theo quan niệm lỗi thời đã gây ra bao đau khổ cho người phụ nữ, cho cuộc đời họ, tình yêu của họ,dồn họ vào chân tường của sự cô đơn. Cũng giống như Nghiệt duyên, cô yêu nhưng phủ nhận thứ tình yêu ấy, cho đến lúc cuối đời khi nằm trên giường bệnh trong vòng tay của người mình yêu. "Ta chết mà không có được người ta yêu. Nhưng ta mãn nguyện vì có được người ta yêu." Tuyệt vời quá.
5
578193
2016-04-17 09:23:54
--------------------------
411064
5095
Chuyện tình đầy tiếc nuối giũa công nương Kirati và chàng trai trẻ Nopporn, nếu họ gặp nhau ở một thời điểm khác có lẽ sẽ có một cái kết tốt đẹp hơn cho cả hai. 
"..cậu chưa từng hiểu ta kể từ ngày đầu tiên chúng ta quen biết nhau". Mình nghĩ đúng thật, nếu như Nopporn hiểu thì không cần phải nhiều lần hỏi :" công nương có yêu tôi không?" mà phải cảm nhận được câu trả lời, hoặc cũng có thể do công nương Kirati che đậy rất kĩ, do khoảng cách địa vị, tuổi tác. nhưng những phút cuối của cuộc đời có lẽ tình cảm này quá lớn mà câu nói cuối cùng như bùng nổ để đáp lại câu hỏi mà Nopporn muốn có.
P/s: do tiki khuyến mãi, mình thấy giá mềm và thích tựa truyện nên mua ngay mà không đọc trước nội dung, nên có hơi sốc khi tuổi chênh lệch nhiều.:)
3
785604
2016-04-05 14:08:48
--------------------------
404463
5095
Đây là lần đầu tiên tôi đọc truyện của Thái Lan, chắc do không hợp cách viết như vậy ,nên khúc đầu có hơi hoang mang, không theo kịp. Càng về sau tôi dần thích ứng với truyện hơn ,nên cảm thấy khá cuốn hút, hấp dẫn , và đầy kịch tính, truyện kể về hai người yêu nhau nhưng có sự chênh lệch về địa vị về tuổi tác, đó chính là khoảng cách rất lớn mà ở thời bấy giờ hai người không thể vượt qua được, và cái kết gây ra nhiều tiết nuối cho cả nhân vật và đọc giả ủng hộ tình cảm hai người.
4
313953
2016-03-25 10:25:58
--------------------------
361473
5095
Con người luôn khao khát được sống theo bản năng tâm lý của mình cho nên nếu như phải rời bỏ một số những phong cách bỏ đi và di dời sắc màu của chúng thì sẽ mất đi ý nghĩa chúng xuất hiện bởi thế không phải ai cũng hiểu hết được điều mà Kirati đã cống hiến cho cuộc sống bằng cách cho đi sự vĩ đại cao cường với tâm huyết trong lời nói liên quan sự bất tử với tấm lòng mà con người luôn chờ đợi mình nhất , thật sự có sức hút đặc biệt .
4
402468
2015-12-31 03:05:34
--------------------------
347774
5095
Chuyện kể về 1 người con trai yêu 1 cô gái tuy đã qua ngưỡng 30 nhưng vẫn xinh đẹp rạng ngời. Một cô gái sắc sảo, thông minh, dịu dàng, khiêm tốn, thùy mị và nết na luôn phải sống 1 cuộc đời tù túng, bất hạnh, không có được tình yêu. Nhưng đến khi cô cảm nhận được yêu là gì, cô lại phải kiếm chế tình yêu đó, bởi, cô yêu người không phải là chồng mình. Giữa 2 người chênh lệch quá nhiều về tuổi tác, không thể có tình yêu nảy sinh - cô quan niệm như thế. Để rồi cuối cùng, đến lúc chết, cô vẫn mãn nguyện mà nói rằng: Ta chết mà không có được người yêu ta, nhưng ta mãn nguyện vì tìm được người ta yêu."

Cuốn sách có nhiều đạo lý tuy dài dòng nhưng khá hay, như: "Cậu đừng quên rằng, có 1 con chim trong tay còn tốt hơn có 2 con chim đậu trên cành cây. Việc có được niềm hạnh phúc mà không có tình yêu có lẽ sẽ tốt hơn việc mơ màng đến một tình yêu mà không có được niềm hạnh phúc."
4
285794
2015-12-04 22:55:15
--------------------------
347388
5095
Chỉ là tình cờ đọc được quyển này trong nhà sách, cảm thấy rất hay nên mình quyết định mua quyển này. Yêu cái cách tác giả mở đầu câu chuyện, thật là hấp dẫn, khiến tôi tò mò cứ cầm quyển sách mãi không buông. Đọc rồi mới thấy thương cho tình cảnh nhân vật. Cả hai đều có số phận quá nghiệt ngã. Nhưng chỉ có tình yêu lại mang đến họ gần nhau hơn. Nhưng trong mỗi người lại có cái tôi , cái lòng tự trọng quá lớn. Người thì chỉ mới là sinh viên, còn người kia thì đã trở thành quí bà sang trọng. Và thật nể phục tình yêu thầm lặng của công nương đối với cậu sinh viên , cho đến khi cuối của cuộc đời mình. nàng vẫn muốn 1 lần ôm trọn tình yêu của minh
4
557999
2015-12-04 12:49:38
--------------------------
321989
5095
Ban đầu đọc quyển sách này cảm thấy như không được hấp dẫn lắm nhưng sau đó càng đọc càng thấy bị thu hút. Câu chuyện ngày càng được đẩy lên cao trào đặc biệt là nơi núi Mitake. Ở nơi đó tình yêu chân thật của chàng Nopporn được thổ lộ cho công nương Kirati. Kết thúc chuyện cho ta thấy đây là một câu chuyện tình buồn thảm giữa hai người có sự chênh lệch về địa vị và về tuổi tác quá lớn. Tiểu thuyết này bên thái đã được chuyển thể nhiều lần thành phim 1 lần thành nhạc kịch và được tái bản nhiều lần. Vở nhạc kịch WaterFall có cốt chuyện dựa trên tiểu thuyết này. Hãy mua nó về đọc thử và tự mình cảm nhận nó bạn nhé.
5
829918
2015-10-15 12:54:11
--------------------------
320343
5095
Mình rất ngưỡng mộ tính cách, phẩm hạnh và vẻ đẹp ngoại hình cũng như tâm hồn của công nương Kirati. Chắc chỉ có trong truyện mới có được con người hoàn hảo đến thế. Nhưng đúng là hồng nhan bạc mệnh,  cả tuổi trẻ, vẻ đẹp, tài hoa của nàng đã bị chôn vùi trong chính ngôi nhà của mình theo năm tháng. Một cuộc đời nhạt nhẽo, cô quạnh. Nhưng cũng thật may mắn vì cuối cùng nàng cũng tìm được tri kỷ hay cũng chính là tình yêu của đời mình, người có thể hiểu được nỗi khổ của nàng và nàng có thể dốc hết bầu tâm sự, những điều mà bấy lâu nàng chỉ giữ cho riêng mình. Mình cũng rất tâm đắc những điều mà công nương chia sẻ với Nopporn, đối với mình đó đều là những triết lý sâu sắc
4
180872
2015-10-10 23:38:35
--------------------------
314996
5095
Hầu hết với các sách nhạt thì khúc đầu sẽ hay và lôi cuốn nhưng đối với sách này thì ngược lại lúc đầu khá nhạt nhưng khúc sau lại hấp dẫn đến bất ngờ . Khắc cốt ghi tâm câu'' Ta chết mà không có được người yêu ta nhưng ta mãn nguyện vì đã tìm được người ta yêu  '' của công nương Kirati . Tình yêu có thể vượt qua rào cản của xã hội phong kiến là 1 điều vô cùng quý giá của công nương Kirati và chàng trai trẻ Nopporn., có thể vượt qua nhưng cái kết lại không toàn vẹn như ta nghĩ 
Mọi vật như yên bình vẫn trôi trầm trọng nhưng kết thúc lại buồn man mác cho người đọc 
4
538253
2015-09-27 21:49:16
--------------------------
267423
5095
Theo mình, đây trước hết là một cuốn sách hay. Sau khi đọc xong cuốn sách này, mình đã cho rằng mình sẽ viết được rất nhiều điều về nó nhưng khi bắt đầu viết thì không biết diễn tả sao cả. (Vì mình chỉ muốn nêu cảm xúc chứ không muốn tóm lại nội dung sẽ làm mất hay cách trình bày của tác giả). Nghe tên cũng đủ thấy nhẹ nhàng, nội dung tưởng chửng nhẹ nhàng nhưng lại không hẳn như vậy. Cuốn sách này rất xứng đáng để được nhiều bạn đọc tìm đến bởi nó đẹp lắm nhưng cũng rất là "khó hiểu"...Tình yêu là gì?...""Ta mãn nguyện vì tìm được người yêu ta". Văn phòng ấn tượng lắm.

5
292005
2015-08-15 15:18:56
--------------------------
254868
5095
"Đằng sau bức tranh" đã để lại ấn tượng cho tôi không chỉ riêng tình cảm giữa Nopporn và Kirati mà còn ở câu nói của nàng công nương: "Ta chết mà không có được người yêu ta, / Nhưng ta mãn nguyện vì đã tìm được người ta yêu ". Câu nói này đã ám ảnh tôi suốt một khoảng thời gian sau khi gấp sách lại. Nó khiến tôi cần phải suy nghĩ nhiều về tình yêu. Mỗi người có quyền được yêu theo cách của chính mình, nhưng có mấy ai chọn cách yêu như nàng Kirati chứ? Tình yêu của nàng sao mà đau khổ đến thế? Chỉ giữ riêng trong lòng mãi không nói, thế có được gọi là cao cả không? Có ích kỷ không? Hay là ngu ngốc?
5
122381
2015-08-05 15:50:04
--------------------------
251837
5095
Khi đọc cuốn sách này, một lần nữa tôi tự hỏi: Rốt cục tình yêu là gì. Trong tình yêu, không thể nói người nào yêu nhiều hơn.Tư đầu tới cuối, tôi cứ ngỡ Nopporn là người yêu sâu đậm bởi sự day dứt cùng nỗi nhớ và biết bao tình thương mà Nopporn đã dành cho công  nương Kirati trong suốt 6 năm trời. Biết bao tình yêu đong đầy trong những dòng thư mà Nopporn gửi cho Kirati. Nhưng ở đời, ai biết được điều gì. Trong câu chuyện tình yêu này, người đâu đớn hơn có lẽ là công nương Kirati_một người yêu mà không thể nào nói lên được tình yêu của mình, phải chôn giấu nó sâu kín tận trong đáy lòng cho tới giây phút cuối cùng. Cô thậm chí còn không được phép thể hiện tình yêu. Biết bao nhiêu điều cất giấu đằng sau đôi mắt phủ đầy nỗi buồn ấy.
Tình yêu của hai người đã vượt qua biết bao rang giới: tuổi tác, địa vị, biên giới. Nhưng tiếc rằng, số phận thật trớ trêu khi cho hai người có cơ hội gặp nhau nhưng không được đến bên nhau. Thế nhưng, đó cũng là một điều may mắn. Gặp được nhau rồi yêu nhau, đã là hạnh phúc . Như lời công nương Kiarat nói ở cuối truyện "Ta mãn nguyện vì tìm được ngươi yêu ta".
5
62056
2015-08-03 10:37:17
--------------------------
165994
5095
Đó là một câu chuyện tình trong sáng và cảm động.Nhân vật là những con người điềm đạm, hiểu biết và đầy chín chắn.Họ yêu nhau một cách tự nhiên mà không bị áp lực bởi hoàn cảnh hay dục vọng! Câu chuyện kết thúc nhưng để lại trong lòng người đọc nhiều ám ảnh.Tình yêu đôi lúc đến muộn những cũng đủ làm người ta mãn nguyện.Ấy là thứ tình yêu không màng đến sự chiếm hữu.tưởng hư đơn giản nhưng lại vô cùng sâu sắc! Sâu sắc hơn bất cứ thứ tình yêu nào trên thế giới này! Tôi yêu thích văn phong của tác giả và sự tinh túy trong ngôn ngữ của người dịch truyện.Thật là một cuốn truyện đáng để ta tìm hiểu!
5
382517
2015-03-11 18:39:32
--------------------------
