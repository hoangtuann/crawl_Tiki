218564
5130
Đây là cuốn thứ 3 trong bộ 4 cuốn 345 Câu khẩu ngữ tiếng Hán. Trong tập 3 có thêm khoảng 100 câu nói tiếng Trung thông dụng được giới thiệu qua 16 bài học từ bài 33 đến bài 48. Sách có mục luyện tập cho mỗi bài học, có danh sách hệ thống lại từ vựng đã dùng trong sách rất tiện cho việc tra cứu. Một số bài tập được phỏng theo kỳ thi HSK để giúp bạn làm quen dần với kỳ thi nếu bạn có ý định thi. Ở tập này các chủ đề, nội dung phức tạp hơn so với 2 tập đầu.
4
470937
2015-06-30 17:49:36
--------------------------
