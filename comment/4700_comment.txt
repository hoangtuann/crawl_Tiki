324577
4700
Cuốn “ ” là một trong 12 cuốn trong bộ sách Khám phá thế giới của Nhà xuất bản Kim Đồng. Truyện có tổng cộng là 25 trang, các câu thoại được dịch rất khéo léo, đọc rất có vần với nhau. Tuy nét vẽ không được đẹp lắm nhưng mình thấy nội dung truyện rất có ý nghĩa. Truyện kể về một cậu bé nói chuyện với bố mình về những sự vật, hiện tượng xung quanh, bố cậu nghe thấy và kể về thời xưa – lúc bố cậu còn bé. Khi nghe bố cậu kể vậy, cậu đã có ước muốn được “ Bố ơi, bố ơi, con năn nỉ, cho con về hồi bé của bố đi!” 
3
15022
2015-10-21 14:57:20
--------------------------
