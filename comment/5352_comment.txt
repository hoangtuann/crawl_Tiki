497197
5352
Sách hay, bổ ích, kiểu truyền cảm hứng động lực ý, :)) mua tại Tiki cũng khá lâu r nhưng mới đọc hết gần đây vì thi cử lu bu quá, bn nào chưa đọc thì nên đọc, Tiki giao hàng nhanh, book care nên bảo quản sách rất okie hehe, giảm giá nữa chứ :))
5
1025522
2016-12-20 22:07:42
--------------------------
475882
5352
Riêng bản thân mình, mình cảm thấy đây là một quyển sách rất hay, rất đáng đọc. Khi đọc về những gì Trí Bác đã trải qua khi còn bé, thật sự mình cũng chẳng thích thú gì cho lắm nhưng dần dần quyển sách bỗng dưng thu hút mình từ lúc nào không hay. Có lẽ là do mình tìm thấy được bản thân trong từng trang sách, có lẽ là do những suy nghĩ của Vu Trí Bác đã khích lệ động viên mình như một người bạn tâm hồn. Quyển sách nghe tên có vẻ "cao siêu" và thiết nghĩ sẽ toàn những phương pháp học tập như những quyển sách khác,  nhưng không, quyển sách này đơn giản chỉ là kể lại những gì Trí Bác đã trải qua và sự tiến bộ hằng ngày trong suy nghĩ của anh. Đây là quyển sách nuôi dưỡng ước mơ và hi vọng, quyển sách này giúp mình tự tin hơn đặc biệt khi nhớ đến câu nói đã trở thành kim chỉ nam của Trí Bác: "Trời sinh thân ta, tất có dùng."  Nói chung, theo mình, đây là một quyển sách rất đáng mua  và đáng đọc đi đọc lại nghiền ngẫm nhiều lần.  
5
264308
2016-07-17 08:37:37
--------------------------
431572
5352
Đây là một trong những quyển sách mình thích nhất. Hành trình của Vu Trí Bác trải qua bao khó khăn được tôi luyện cuối cùng cũng thành công. Mình cũng mắc phải sai lầm là không biết cách thể hiện bản thân như Trí Bác, bây giờ ngẫm lại mới thấy đúng. Kĩ năng mềm thật sự là rất cần thiết nhưng Việt Nam mình lại chỉ chú tâm đào tạo kiến thức mà sau khi ra trường lại quên rất nhiều. Tự nhận thấy cuốn sách đọc rất dễ hiểu dùng để giải trí rất tốt và mang tính suy ngẫm cao, phù hợp với mục đích của mình.
3
562102
2016-05-17 17:02:51
--------------------------
430778
5352
Mình cảm thấy thích cuốn sách này. Có lẽ nó gây ấn tượng với mình ngay từ cái tên có chút kì lạ của nó. Lúc đầu mình có hơi tò mò và thắc mắc ý nghĩa thật sự của cái tên đó nhưng mình vẫn mua. Quyển sách giống như một cuốn tự truyện của một chàng trai trẻ đã đi qua quãng đường đầu đời rất đặc biệt. Với lối viết gần gũi, xưng hô thân mật, từng trang sách được viết nên từ chính kinh nghiệm của  Vu Trí Bác. Mình nghĩ đây là một cuốn  sách hay và khuyến khích mọi người nên mua.
4
815542
2016-05-15 21:17:32
--------------------------
360520
5352
Mình mua cuốn sách này để tặng cậu bạn thân
Thấy bạn ấy khen nhiều lắm
Nó rất có hữu ích đối với cuộc sống chúng ta
Nó giúp chúng ta có nghị lực hơn trong cuộc sống, Mỗi chúng ta không có ai là hoàn hảo, mỗi người có những điểm mạnh riêng
Chúng ta nên biết phát huy những điểm mạnh đó
Mỗi điểm mạnh sẽ có một lợi ích riêng trong cuộc sống
Mình nghĩ đây là một trong những cuốn sách đáng để mua và đọc
Chất lượng giấy tốt, chữ in rõ ràng dễ đọc
Dịch vụ ship hàng vs gói hàng của Tiki làm mình thích lắm ạ
Yêu Tiki lắm lắm luôn ạ
Sẽ ủng hộ Tiki nhiều hơn ạ
5
523572
2015-12-29 12:25:54
--------------------------
336840
5352
Bạn mình khá thích quyển này nên mình đã mua tặng và cũng có mượn đọc ké. Theo khách quan mà nói thì quyển sách đánh trúng tâm lí người đọc khi kể về quá trình "một người học dốt" trở thành "một sinh viên trường đại học danh tiếng", tạo động lực ở người đọc. Vu Trí Bác tuy rằng nhà có điều kiện nhưng chủ yếu dựa vào thực lực là chính, vượt mọi khó khăn để có thực hiện ước mơ. Theo đó là những kinh nghiệm được chia sẽ cùng với sự trải nghiệm về thế giới xung quanh cũng rất là hay. Nhưng nếu xét theo phương diện chủ quan thì liệu quyển sách áp dụng vào trường hợp của mỗi chúng ta liệu có thành công không, xác suất thành công cũng khó nói. Vậy nên theo mình đây là quyển sách đọc tham khảo hay giải trí thì hay hơn.
4
391713
2015-11-12 23:47:40
--------------------------
329510
5352
Một quyển sách rất hay với những kinh nghiệm thực tế. Mình cảm thấy thích những quyển tự truyện kiểu như thế này, cho khi thấy quyển sách lần đầu trên mạng, dù lúc đó sách chưa xuất bản, đã cảm thấy rất ấn tượng cho tiêu đề quyển sách. Và thật nó không làm mình thất vọng vì đây chính là quyển sách mà mình cảm thấy cho mình rất nhiều động lực sau khi đọc xong. Hơn hết, với những nỗ lực mà Vu Trí Bác đã bỏ ra, hy vọng anh sẽ đạt nhiều thành công hơn nữa.
4
263786
2015-10-31 22:41:11
--------------------------
321530
5352
Đầu tiên là giao hàng cực nhanh, mình order 1 ngày sau có liền :D
Sách thu hút từ những trang đầu tiên luôn, đọc rất đúng thực trang hiện giờ, nói chung là 1 cuốn sách nên đọc, sau này biết tâm lý dạy con cũng ok. 
Cuốn này dành cho cấp 2 và cấp 3 thì được nhất, để mấy em nhận thức sớm. biết được tương lai. 
Ban đầu nhìn tựa sách thì mình tưởng là thua từ trường Harvard, nhưng thực ra là thua từ trước đó nữa. thật đáng khâm phục. Cần có những cuốn sách như thế này để khai sáng tri thức hơn (y)
5
395923
2015-10-14 09:53:54
--------------------------
307172
5352
Với tình trạng ưa chuộng thành tích học tập cao hiện nay thì cuốn sách này sẽ làm chúng ta thức tỉnh và tự tin vào tương lai hơn. Qua tấm gương của Vu Trí Bác chúng ta thấy rõ các kỹ năng mềm rất quan trọng và nên là những điều trẻ em ưu tiên học khi còn nhỏ. Khi được trang bị đày đủ các kỹ năng mềm chúng ta có thể vượt qua khó khăn để đạt được thành tựu trong tương lai hơn là kiểu học chạy theo thành tích. Kiểu sống vì thành tích như vậy sẽ khiến chúng ta dễ bị tổn thương chính bản than và khó mà vuon lên sau vấp ngã. VTB cũng rất may mắn đã có gia đình như thế và mong rằng ngày càng nhiều trẻ em cũng may mắn như vậy. Sách hay tuy nhiên chữ hơi nhỏ và giấy dày thêm chút sẽ tuyệt.
5
43129
2015-09-17 23:23:42
--------------------------
306086
5352
Cuốn sách rất thực tại. Gần gũi và dễ gây cho người đọc cảm thấy dễ chịu. Cuốn sách là nguồn động lực của những người đang ở trong vực thẳm của sự thất vọng. Mình nói thật là bạn sẽ thay đổi cách nhìn về bản thân khác ngay. Sẽ có đủ tự tin bước đi tiếp con đường mà bản thân mình chọn. Sắc thái cuốn sách nhẹ nhàng mà triết lí, học giỏi hay không giỏi cũng dựa vào kỹ năng mềm. Không phải cứ cố gắng bù đắp kỹ năng cứng thì chúng ta cũng khó bước lên khỏi khó khăn. Cứ bỏ sách vào giỏ hàng đi mấy bạn ơi, mình thấy đúng lắm!!
5
504577
2015-09-17 12:42:47
--------------------------
296212
5352
Có thể nói, nhận thức về con đường học vấn của mình được thay đổi chính là nhờ vào quyển sách này. Tác giả viết sách gần như nói rất đúng tất cả các thực trạng học đường hiện tại khiến độc giả cảm thấy gần gũi và dễ cảm nhận nội dung của sách. Mình đã được tiếp sức và niềm tin rất nhiều khi đọc xong quyển sách này, cảm giác như "Tác giả có một điểm xuất phát như thế mà lại làm nên điều kì diệu, tại sao mình thì không?". Bằng một cách nào đó, mình giảm được áp lực học tập và nghiệm ra không phải cứ học giỏi thì sẽ có tất cả. Tóm lại là mình cảm thấy sách này rất đáng có trong tủ sách của mỗi học sinh. (:
5
507050
2015-09-10 22:25:14
--------------------------
290324
5352
Đây là cuốn kỹ năng sống tôi thấy thú vị nhất!Những câu văn rất dễ hiểu và gần gũi với mỗi người!Tác giả kể về cuộc hành trình của chàng trai Vu Trí Bác rất thành công tại Mĩ!nhưng để được thành công như ngày hôm nay anh đã phải bỏ qua rất nhiều thứ mà mình thích(vì ôn thi mà không thể xem trận bóng mà mình thích)ở đây chúng ta sẽ bắt gặp một Vu Trí Bác với tốc độ thay đổi rất nhanh nhưng cũng không kém phần cực khổ và nổ lực của chính bản thân!Các bậc phụ huynh và các bạn trẻ nên đọc cuốn sách này để giáo dục con cái tốt hơn và để chính bản thân mỗi bạn trẻ có thể noi gương Vu Trí Bác!!!
5
413637
2015-09-05 11:31:37
--------------------------
287558
5352
"Chàng trai Harvard thua tại vạch xuất phát" đã thay đổi hoàn toàn quan niệm của tôi về học hành, trường lớp. Mặc dù chưa biết được tương lai tôi thực sự mong muốn điều gì, nhưng tôi có một niềm tin chắc chắn rằng, chỉ cần phát triển các kỹ năng mềm một cách toàn diện như Vu Trí Bác thì bất kì khó khăn gì cũng không thể ngăn cản được tôi trên con đường tiến đến thành công. Sách giấy tốt, chữ dễ nhìn, tôi đã đọc liền mạch trong hai buổi tối là hết. Dù là bậc cha mẹ, hay học sinh đều có thể đọc cuốn sách để xác định hướng phát triển tương lai cho con cái hoặc bản thân.
5
407394
2015-09-02 20:58:20
--------------------------
283067
5352
Mình mua sách này cũng khá lâu rồi nhưng nay mới có thời gian để đọc. Quyển sách thật sự rất có ý nghĩa với mình trong giờ phút này - những tháng cuối của đời sinh viên, với những nỗi lo không nguôi về việc làm, về con đường thực hiện mơ ước tương lai. Vu Trí Bác ngay từ những ngày còn nhỏ đã có xuất phát điểm thấp hơn người khác nhưng ai mà chẳng gặp khó khăn trên đường đời. Dù vậy, với sự quan tâm của gia đình, sự kiên trì của bản thân cậu đã làm ra được điều không tưởng là trở thành sinh viên của Harvard. Đó không chỉ là một kết quả ngay trước mắt mà là cả một quá trình phấn đấu không ngừng, trao dồi không chỉ kiến thức mà còn cả những kĩ năng mềm qua các kinh nghiệm trong cuộc sống. Lối kể chuyện của tác giả cũng khá gần gũi tuy một vài chỗ còn hơi dài dòng. Đây là một quyển sách hay, đáng đọc và chắc chắn sẽ tiếp thêm năng lượng cho những ai đang gặp khó khăn, đang chùn bước trước ước mơ của mình.
4
63487
2015-08-29 17:59:37
--------------------------
255571
5352
Cuốn " Chàng trai Harvard thua tại vạch xuất phát" là một cuốn sách hay. Ngôn ngữ kể chuyện của Vu Trí Bác, gần gũi dễ hiểu. Đọc sách này tôi nhận thấy rằng, trong quá trình học tập ko phải chỉ chú trọng học kiến thức trên ghế nhà trường, chúng ta cần học hỏi qua quá trình giao tiếp xã hội, qua cuộc sống. Không chạy theo những giá trị ảo, hãy làm và cống hiến hết mình vì đam mê thì cuối cùng cuộc đời sẽ mỉm cười với bạn. Và đặc biệt hãy học Vu Trí Bác luôn suy nghĩ tích cực, tìm thấy điểm tích cực trong thất bại.
5
511851
2015-08-06 10:04:39
--------------------------
255431
5352
Mình mua cho cháu mình quyển sách này nhưng cũng đọc ké, cơ mà không hấp dẫn lắm. Thấy cháu mình cũng đọc dang dở mãi chưa xong. Điều mình không thích là giọng kể của nhân vật được dịch lại khá cứng, nhiều chỗ nghe không tự nhiên. Câu chuyện kể dài dòng, có lẽ chắc chỉ cần đọc qua tóm tắt chi tiết là đủ, nhiều đoạn mình phải nhảy cóc vì lan man quá. Dù sao cũng là một tấm gương cho các bạn trẻ không ngừng nỗ lực và ước mơ vượt lên trên hoàn cảnh.
2
585544
2015-08-06 00:55:31
--------------------------
249963
5352
Quyển sách nhắc cho ta biết ngoài vấn đề học tập để trao dồi kiến thức thì cũng không nên quên những hoạt động xã hội, những công việc gần gũi bên chúng ta hàng ngày, chăm chỉ rèn luyện sức khỏe, mỗi người nên chọn cho mình 1 môn thể thao để chơi. Với căn bệnh thành tích thì dần dần trẻ con bị kéo theo, rồi tuổi thơ của chúng chỉ có học, học, và học. Qua quyển sách ta thấy được Vu Trí Bác, một học sinh với kết quả học tập thuộc hạng chốt của lớp nhưng cậu lại may mắn được sống trong một gia đình với sự quan tâm, chia sẻ của cha và ông bà nội cho cậu được học những môn cậu thích, cho cậu những trải nghiệm thế giới xung quanh. Sau một thời gian thì cậu cũng nhận ra được việc mình cần phải làm, cùng với sự phát triển các kỹ năng mềm, nổ lực của bản thân cậu đã đạt được ước mơ của mình là vào Harvard. Một học sinh luôn đứng chốt lớp với ước mơ Harvard quả là một điều khó tưởng tượng ra
4
443436
2015-08-01 00:38:49
--------------------------
235081
5352
đầu tiên về hình thức thì mình thấy sách tên sách rất hấp dẫn nhưng bìa không được hấp dẫn cho lắm, nếu đầu tư được bìa sách đẹp hơn thì thật tuyệt. còn về nội dung, sách viết về quá trình khi còn học ở trong nước (Trung Quốc) của Vu Trí Bác, một học sinh với mức học tầm trung nhưng sau khi sang Mĩ thì cố gắng phấn đấu và sau này đạt được thành công. Cá nhân mình thích cách kể chuyện của Vu Trí Bác hơn những lời của chu Dịch sau mỗi câu chuyện, khá dài dòng vì qua câu chuyện trên mình có thể tự đúc kết được nhiều bài học. Sách khá hay, nhưng không hay như mình tưởng tượng, nhìn chung vẫn là cuốn sách đáng để đọc
4
495463
2015-07-20 20:34:41
--------------------------
234304
5352
Tôi đã từng nghĩ rằng nếu mình đã thua tại những điểm ban đầu của cuộc đua thì mình khó có khả năng thắng cuộc. Nhưng sau khi đọc xong câu chuyện của Vu Trí Bác mình đã hoàn toàn thay đổi quan điểm. Chẳng ai có thể ngờ một chàng trai như tác giả cấp 1 ở lại lớp, cấp 2 đội sổ, cấp 3 qua Mỹ học nhưng lại học trong một  ngôi trường vùng ven, không hề có danh tiếng lại có thể đậu vào trường đại học Havard -  1 trong những ngôi trường danh giá nhất thế giới. Không phải là một điều dễ dàng để một người "thua tại vạch xuất phát" như Bác để đạt đc thành tựu như vây. Cần phải có môi trường, thái độ trong gia đình đặc biệt là ông bà, bố mẹ của Bác, ngoài ra cần phải có đc tinh thần rèn luyện, những kĩ năng dần dần đc hoàn thiện. Dù biết rất khó để để làm đc như Bác nhưng cuốn sách này sẽ giúp bạn có thêm niềm tin khi bạn có những vấp ngã thủa ban đầu.
4
267843
2015-07-20 11:27:58
--------------------------
230722
5352
"Chàng trai Harvard thua tại vạch xuất phát" là tự truyện về sự trưởng thành của  chàng trai Vu Trí Bác học trường không tên tuổi cấp 3 và đại học cho đến khi mở được cánh cửa Harvard.
Cuốn sách mang đến cho người đọc những kỹ năng mềm cần có trong một xã hội mà thế hệ trẻ càng ngày càng thụ động.
Đọc cuốn sách ta sẽ thấy được trong lòng chàng trai trẻ người châu Á luôn có tổ quốc ở trong lòng để luôn giữ bước chân tiến tới, không ngần ngại khó khăn để trở thành một công dân tốt, một người đàn ông trưởng thành, và là một con người sống có ích.
Một trong những câu nói "đắt" nhất mà cuốn sách mang lại chính là: 
" Để tích luỹ tri thức hãy bước vào cánh cổng đại học, để phụng sự tổ quốc và đồng bào, hãy cố gắng hết sức khi được đi ra" 


5
473660
2015-07-17 18:01:33
--------------------------
222967
5352
như các nhận xét bên dưới, mình rất hài lòng với cuốn sách này, cuốn sách là một cẩm nang để chúng ta rèn luyện và học tập theo.
đọc xong cuốn sách này, mình có rất nhiều động lực để học tập và ý chí quyết tâm. từ một học sinh bình thường như MR.Khoo mà tác giả đã vượt lên để học tập tại havard và trở thành một sinh viễn tài năng, một người thành công.
có bạn nói cuốn sách này cũng đi theo lối mòn như bao cuốn sách khác nhưng thật sự thì nếu bạn học tập theo các phương pháp mà tác giả đã làm thì bạn sẽ thật sự thành công. Phương pháp học nào cũng thế, chỉ có quyết tâm là sẽ thành công.
4
166271
2015-07-06 13:52:50
--------------------------
213057
5352
Cuốn sách này đã làm thay đổi suy nghĩ của tôi rất nhiều. Cuốn sách như một phép màu giúp tăng lên gấp bội phần ý chí và niềm tin vượt qua thử thách. Mọi thành công trên đời cần có ước mơ không ngừng được vun đắp, niềm tin rộng mở tiếp nối những mục tiêu và kế hoạch rõ ràng. Ngoài ra cần có một tinh thần tỉnh táo, biết vận dụng chiến thuật phù hợp để cán đích thành công.
Vu Trí Bác tự tin, hòa đồng, tiết kiệm, yêu thể thao, tham gia hoạt động ngoài trời và luôn phấn đấu, đối mặt với thử thách từ dễ đến khó,... tất cả chỉ là một câu chuyện nhưng ý nghĩa trong đó lại vô cùng lớn lao, vô cùng chân thực.
Từng câu chuyện nhỏ, từng bài học rút ra song song với nhau, đọc rất thích, đánh trúng điểm yếu mỗi người.
5
386757
2015-06-23 10:57:06
--------------------------
212289
5352
Quyển sách này không phải là một cẩm nang hướng dẫn cho các bạn, nó là mọt câu chuyện có thật về một cậu sinh viên Vu Trí Bác. Anh đã thành công với con đường học vấn của mình và viết lại câu chuyện về hành trình ấy. Xuất phát từ gia đình khá giả, anh hoàn toàn có đủ điều kiện để đi học thêm nhiều thầy cô như bạn bè. Nhưng không, gia đình anh có cách nuôi dưỡng con cái hoàn toàn khác với đa số các bậc cha mẹ hiện này. Họ đào tạo kĩ năng mềm cho anh, từ đó anh hoàn thiện bản thân. Quyển sách thật sự là một nguồn động viên lớn cho học sinh như chúng ta, dạy chúng ta điều gì mới là cần thiết trong xã hội.
4
374756
2015-06-22 10:25:38
--------------------------
192610
5352
Nhìn chung thì mootip chẳng có gì mới. Làm thoạt đầu mình còn tưởng anh chàng này có điều gì đó đặc biệc thú vị nữa. Chủ yếu anh ta chỉ nói về sự thành công chứ phương pháp thì nếu áp dụng cho người khác thì không mấy khả quan vì không phải gia đình nào cũng có khả năng mà quan tâm con cái kiểu như thế. không phải cứ có cái khuôn thì ai cũng đặt mình vào đấy được đâu. Cho nên anh ta cũng được xem là hơn hẳn người bình thường vì có điều kiện kinh tế gia đình rồi, còn người bình thường mà muốn có được học bổng để học Havard thì có cố gắng gấp 10 lần như thế chả biết có tương lai gì không!
3
548377
2015-05-05 09:46:06
--------------------------
176877
5352
Khi nhắc đến Harvard làm tôi nghĩ ngay đến nơi đã đào tạo ra những nhân tài kiệt xuất như tổng thống Mỹ Barack Obama hay Bill Gates, nhưng tiêu đề sách lại nói anh chàng thua ngay tại vạch xuất phát .Tôi nghĩ như thế thì có gì đáng xem. Thế nhưng , cuốn sách lại nói một vấn đề sâu rộng hơn, một người học dốt lại có thể là sinh viên trường danh tiếng này, đã làm tôi vô cùng hứng thú.
Sau khi đọc tôi nhận thấy đây cũng chỉ là một câu chuyện vẫn thường hay nghe đến, đâu phải cái gì cứ cố gắng là được, câu không gì là không thể chỉ là một cách nói khuyên con người đừng bỏ cuộc dễ dàng thôi, nếu không có điều kiện trợ giúp thì dù có cố gắng đến mấy cũng chỉ vô vọng mà thôi
3
539768
2015-04-02 00:20:51
--------------------------
173802
5352
Khi đọc tựa đề và xem view ở Tiki mình tưởng rằng anh chàng này tuy học kém nhưng sau nỗ lực vươn lên giành được học bổng đi du học, nên rất kì vọng nó sẽ sốc lại tinh thần học tập cho bản thân. Tuy nhiên câu chuyện của Vũ Trí Bác lại khiến mình hơi thất vọng, không được như kì vọng. Nói chung anh này cũng rất may mắn khi có gia đình tâm lí và tạo điều kiện cho đi Mỹ du học. Cảm thấy trong cuốn sách này cuộc đời Vu Trí Bác cũng ít nhiều được tâng bốc lên.
3
466046
2015-03-26 20:28:22
--------------------------
172203
5352
Khi mình mua cuốn sách này mình đã kì vọng khá nhiều vào đó, nhưng câu chuyện sự phấn đấu của Vu Trí Bác lại không như những gì mình mong muốn đọc.
Ban đầu mình tưởng là anh ấy tự xin học học bổng để đi học nhưng thực ra do học kém nên được bố mẹ cho đi sang nước ngoài từ cấp 3 để có môi trường học khác với mong muốn con mình học tốt hơn. Sự chênh lệch kiến thức từ Trung Quốc sang Mỹ, với các môn như Toán, Hoá khiến Trí Bác tự tin và giỏi hơn. Rồi bla...bla thành công. Nói chung mình không thấy thoả mãn khi đọc cuốn sách này.
Nhưng rõ ràng không thể phủ nhận những điểm tốt của cuốn sách. Khuyên bạn nên Hoà đồng, vui tươi, yêu thể thao, ... Và đem lại nhiều kiến thức mới về những trường học tại Mỹ, mở mang đầu óc và biết nhiều hơn.  Mình cũng không biết nói thế nào nhung mình cảm thấy cuốn sách được thổi phồng nhiều quá. Đọc lên hơi thất vọng,
4
58089
2015-03-23 15:37:41
--------------------------
143697
5352
Đây là cuốn sách vô cùng hay và cực kỳ bổ ích cho những ai có ước mơ ra nước ngoài học tập. Nó không hề nói bạn phải học thế nào, cày cuốc sách vở ra sao, bạn phải học bao nhiêu giờ một ngày,... mà chỉ đơn thuần là kể chuyện. Vâng, kể lại toàn bộ cuộc sống của một du học sinh khi anh ấy lần đầu đặt chân đến, chưa sõi tiếng Anh cho đến khi anh ấy tốt nghiệp Harvard - trường đại học danh giá của nước Mỹ.
Tự tin, hòa đồng, tiết kiệm, yêu thể thao, tham gia hoạt động ngoài trời và luôn phấn đấu, đối mặt với thử thách từ dễ đến khó,... tất cả chỉ là một câu chuyện nhưng ý nghĩa trong đó lại vô cùng lớn lao, vô cùng chân thực.
Từng câu chuyện nhỏ, từng bài học rút ra song song với nhau, đọc rất thích, đánh trúng điểm yếu mỗi người.
5
382812
2014-12-24 07:43:29
--------------------------
143641
5352
Không biết mọi người như thế nào nhưng đối với mình cuốn sách này thực sự có tác động rất lớn đến mình. Đọc xong mình chỉ muốn mau đứng lên để hành động thôi ^^ Rất nhiều điều bổ ích cho các bạn trẻ trên con đường lập nghiệp và cả cho các bậc phụ huynh nữa.

Thứ nhất: khái niệm về "Thua tại vạch xuất phát". Lần đầu tiên mình nghe nói về khái niệm này nhưng đây thực sự là vấn đề nóng hổi cần bàn đến trong xã hội hiện nay, đặc biệt là ở phương Đông. Những hệ thống thi cử và con số, bảng điểm chán ngắt - chúng ta đã quá quan tâm đến chúng, thậm chí còn muốn dùng phương pháp ấy để nuôi  dưỡng, đào tạo thế hệ tương lai. Bạn sẽ thấy chúng sai đến nhường nào khi đọc cuốn sách này.

Thứ hai: "Trời sinh ta, ắt có lúc dùng" câu nói này đã khích lệ mình rất nhiều. Mình vốn đang chìm đắm trong sự thất vọng về chính bản thân, nay được tấm gương như Vu Trí Bác cổ động, mình tự tin hơn rất nhiều, không còn sợ dư luận múa máy xung quanh mình.

Thứ ba: "Trời giáng trọng trách cho tôi, tất cần phải vất vả bền chí" Mình rất khâm phục anh Trí Bác về điểm này. Ba lần thi GMAT cực kì vất vả và cũng đầy thất bại bi thảm nhưng cuối cùng anh cũng bền chí đến cùng hoàn thành mục tiêu. Xem ra những chờ đợi vô vọng của mình vẫn chưa là gì so với anh ấy.

Thứ tư: "Giấc mơ nước Mĩ" mình đã hiểu tại sao nhiều người lại muốn đi du học Mĩ như thế. Những trải nghiệm của anh Trí Bác tại Mĩ đã khiến tôi được mở rộng tầm nhìn hơn rất nhiều.

Thật ra còn rất nhiều điều tuyệt vời mà mình học được từ cuốn sách nhưng mình chỉ có thể tóm gọn vậy thôi. Con đường đến Harvard của anh ấy khiến mình rất rất ngưỡng mộ.

Mình mong bìa cuốn sách thiết kế đẹp hơn, cuốn sách rất hay nếu có cái bìa xứng tầm nữa thì khỏi bàn luôn ^^
5
113747
2014-12-23 22:09:37
--------------------------
140478
5352
Cuốn sách thực sự hấp dẫn với những ai đàng vật lộn trên con đường tìm kiếm thành công. Bằng những kinh nghiệm thực tế, vốn sống phong phú, tác giả đã chỉ ra những phương pháp vô cùng hữu hiệu dành cho các bạn học sinh, sinh viên, những người đang tìm kiếm cho mình một con đường tương lai phía trước. Cuốn sách như một phép màu giúp tăng lên gấp bội phần ý chí và niềm tin vượt qua thử thách. Mọi thành công trên đời cần có ước mơ không ngừng được vun đắp, niềm tin rộng mở tiếp nối những mục tiêu và kế hoạch rõ ràng. Ngoài ra cần có một tinh thần tỉnh táo, biết vận dụng chiến thuật phù hợp để cán đích thành công.
Cuốn sách có tính giáo dục cao, giàu tính nhân văn sâu sắc, là công cụ hữu hiệu với những ai muốn học cách tư duy và thay đổi bản thân.
5
414919
2014-12-11 15:09:15
--------------------------
