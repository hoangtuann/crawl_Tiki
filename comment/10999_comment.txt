487772
10999
Nội dung về cơ bản là được, tuyến nhân vật phong phú, cách hành văn cũng được. Nhưng có nhiều điểm không ưng là Cảm giác hụt hẫng mỗi khi kết thúc 1 phần do nội dung bị cụt, những tập đầu như Thâu thiên hay Hoán nhật hay các phần lẻ Phá lãng, Thiết hồn thì tình tiết chưởng không nổi bật, toàn kiểu chưa đánh đã thắng, chưa yêu đã chết. Lại còn bị drop nữa chứ. Đang đọc Tuyệt đỉnh, tự nhiên ngừng cái phụp giữa chừng, chưa kịp đánh nhau, chưa kịp thi hành mưu kế, Tiểu Huyền chưa kịp học được võ gì nổi bật để có hy vọng thành danh. Cảm giác lúc đấy Tiểu Huyền sẽ giống Hứa Mạc Dương, được truyền cho Thiên Mệnh Bảo Điển rồi sẽ truyền lại cho thằng khác thế hệ sau và chết. Hic. Hoán nhật tiễn vừa xuất hiện đã hỏng, mà chẳng thấy xuất hiện lại theo như Thiên mệnh. Có lẽ tác giả vẫn đang viết tiếp nhưng hy vọng tác giả đừng chết trước khi xong truyện giống như mấy bộ truyện tranh. Đọc xong gần chục phần mà vẫn thấy lơ lửng ko có dấu hiệu giải đáp các nghi vấn nảy sinh trong truyện. Đừng drop.
3
1521849
2016-10-27 08:36:35
--------------------------
367916
10999
Đem lại cảm tưởng về sự khai thác trong nhân vật được làm mới hơn , thời vị hàn  có cách giới thiệu sự việc rất ngắn gọn nhưng có sức suy diễn rất xa , phù hợp với nhiều người đọc khó tính , cởi mở với những hành động trượng nghĩa của những vị anh hùng hảo hán làm ta hài lòng với chính mình ở hiện tại , không quá đòi hỏi gì nhiều mà chỉ là sự nổi bật về tình người nhiều hơn , sống thật tốt rồi sau đó gây dựng nên không ngờ .
4
402468
2016-01-12 23:19:10
--------------------------
305587
10999
So với những tên tuổi tác giả đã thành danh ở mảng truyện kiếm hiệp này mà mình quen thuộc như Lương Vũ Sinh, Kim Dung, Cổ Long, thì Thời Vị Hàn là tác giả khá xa lạ với mình. Không nổi bật, hoành tráng như những tác phẩm khác, " Thâu Thiên Cung" ngắn gọn hơn, súc tích hơn, không thiếu những cảnh chiến đấu của các anh hùng, các nhân vặt mỗi người một tính cách, mỗi người một sở trường, sở đoản riêng, mạch truyện liền lạc, lôi cuốn, tuy nhiên, mình phải đọc thật chậm, thật kỹ mới cảm thấy thấm được văn phong của Thời Vị Hàn, mới thấy hết cái hay của truyện.
4
39798
2015-09-17 00:23:28
--------------------------
292034
10999
Mình mua trọn bộ Minh tướng quân hệ liệt và Thâu Thiên Cung là tập đầu tiên trong bộ truyện này. Ấn tượng đầu tiên là sách in khá đẹp, mình khá ấn tượng với bìa sách. Khi mới đọc, nếu không quen với truyện kiếm hiệp sẽ cảm thấy hơi chán, nhưng thực ra càng đọc về sau sẽ cảm nhận được sự hấp dẫn và nét đặc sắc riêng trong văn phong của Thời Vị Hàn. Đối với mình, Thâu Thiên Cung nhìn chung khá ổn, mong rằng tác giả sẽ cho ra đời nhiều tác phẩm như thế này trong tương lai.
3
115397
2015-09-06 22:38:31
--------------------------
281630
10999
Bìa:			Đẹp
Chất lượng in, giấy: Tốt
Chất lượng dịch:	Tốt
Nhận xét:		 Thâu Thiên Cung chủ yếu là cuốn khởi đầu trong Minh Tướng Quân hệ liệt. Tác giả lần lượt giới thiệu các tuyến nhân vật nên đôi lúc cứ lặp đi lặp lại, làm mạch văn thiếu hấp dẫn. Các nhân vật được khắc họa khá tốt với tính cách và võ công của từng người. Theo tôi, có lẽ nhân vật Minh Tướng Quân được miêu tả hay nhất với võ công thượng thừa, tư duy cẩn mật tinh tế, hành sự quyết đoán và cái hay là khó nói được đây là nhân vật chính hay tà.
Khuyên: 		Cũng được, có thể đọc.

3
129879
2015-08-28 15:51:43
--------------------------
266939
10999
Có thể nói đây là 1 tác phẩm xuất sắc, không thua kém so với những tiểu thuyết kiếm hiệp của nhà văn Kim Dung. Khi mới đọc, mọi người sẽ có cảm giác không quen lắm với cách dùng từ, câu chữ hành văn của tác giả, tuy nhiên khi đã quen rồi thì đó lại là 1 phong cách rất riêng và mang màu sắc võ hiệp lôi cuốn và hấp dẫn, những tích cách đại hiệp, quân tử được khắc họa rất rõ nét. Sách in khá đẹp, chữ rõ ràng tuy nhiên trang bìa hơi bị nhạt màu thì phải?
4
230629
2015-08-15 04:04:35
--------------------------
227698
10999
Đây là lần đầu tiên tôi mua một cuốn truyện kiếm hiệp Trung Hoa như thế này. Mặc dù tôi đã từng đọc khá nhiều truyện ngôn tình nhưng đây là lần đầu tiên tôi đọc kiểu tiểu thuyết này. Tôi mua nó vì được tặng một quyển trong hệ liệt này nhưng lại là tập 2 cho nên tôi quyết định mua để đủ bộ. Lúc đầu đọc nó sợ là sẽ nhàm chán khó hiểu nhưng không ngờ văn phong của tác giả lại vô cùng ngắn gọn nhưng cũng không hề thiếu tinh tế. Tôi cảm thấy Thời Vị Hàn là một tác giả không thua kém gì các bậc tiền bối Kim Dung, Cổ Long...
5
320254
2015-07-14 14:22:48
--------------------------
165739
10999
Thực sự thì tôi chỉ biết được cuốn này qua việc mua sách và được tặng free gift là cuốn này. Lúc đầu tưởng rằng đây chỉ là một cuốn sách không có ai rating nên không đọc, nhưng khi đọc rồi thì sẽ thấy rất hay, câu từng ngắn gọn, dịch giả và tác giả chú thích rất kĩ lưỡng khiến người đọc cảm thấy dễ hiểu về nội dung câu chuyện. Cốt truyện về thời kiếm hiện và thần công thời xưa thì tạo cho người đọc cảm giác mạnh mẻ và lôi cuốn. Tôi mong rằng có thể đọc được hơn nữa nhiều cuốn sách như thế.
5
344167
2015-03-10 22:41:26
--------------------------
