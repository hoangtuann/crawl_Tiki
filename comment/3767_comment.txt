494375
3767
Sách mới bọc cẩn thận, giấy thơm, giao hành nhanh! 
5
1854655
2016-12-06 18:32:37
--------------------------
468913
3767
Truyện này là mình mua tặng bạn vì biết cậu ấy thích tác giả của truyện này, sẵn dịp mình cũng được đọc ké sau khi cậu ấy đọc xong luôn :"> nhìn chung thì truyện khá hay và ấm áp, bìa đẹp, bên trong lại còn có thêm nhiều hình ảnh xen kẽ, rất mới lạ và độc đáo. Dù sao thì mình cũng khá hài lòng về các tác phẩm của Kim nên cũng không có băn khoăn gì. Tiki thì vẫn giao hàng đúng hẹn như vậy. Tóm lại là mình vô cùng  hài lòng về sản phẩm.
4
882749
2016-07-05 19:36:13
--------------------------
432228
3767
Từ trước đến giờ mình luôn thích đọc truyện của Girlne Ya, tất nhiên "Nữ hiệp quái chiêu" cũng không phải ngoại lệ.
Về chất lượng sách thì không cần phải bàn, có thể nói là quá ổn.
Về nội dung thì mới đọc đến tập 3 nên mình cũng chưa biết kết thúc của truyện sẽ như thế nào. Nhưng 2 tập đầu tiên đã lôi cuốn mình với nội dung lãng mạn. Đến tập thứ 3 này mình lại biết thêm nhiều điều hơn nữa.
Nói chung, NXB Kim Đồng và Tiki chưa bao giờ làm mình thất vọng cả!
5
387535
2016-05-18 19:55:32
--------------------------
311426
3767
Tập 1, 2 khiến mình khá hài lòng. Thế là lục tục rinh em tập 3 về nhà. Vẫn là cảm giác hấp dẫn và hài không đỡ nổi của chị Quách Ni! ^▽^ Thật tuyệt vời! Những tình tiết trong tập 3 này bắt đầu tăng độ hấp dẫn và kịch tính. Mình khá yêu thích hai anh em Thánh Dạ và Thánh Y ! ♡.♡  Không có chỗ nào để chê, quả là viên kẹo ngọt khi ta vừa bơi ra khỏi bể ngược lóp ngóp! =.= Chờ đợi vào tập tiếp theo của Quách Ni để gặp lại hai anh em mỹ nam của mình. :v
5
453691
2015-09-19 23:10:38
--------------------------
258253
3767
Nữ Hiệp Quái Chiêu của nữ tác giả Girlne Ya - Quách Ni  thật sự rất lôi cuốn và hấp dẫn. Ở tập 2, sự thật về  đa tính cách của Thánh Dạ đã được gỡ bỏ ở tập 3 này. Sau đó tiếp tục là những tình huống bá đạo giữa Lâm Hy và anh em Thánh Dạ và Thánh Y. Câu chuyện trở nên hấp dẫn hơn khi tác giả đã cho nhân vật bí ẩn Ác Ma D. Sự thật hiểu lầm của Lâm Hy làm mình càng hồi hộp muốn biết đến tập 4. 
  Lần này NXB Kim Đồng đã làm cho cuốn sách có hình thức rất đẹp và bắt mắt. Mình thực sự không hối tiếc khi đã mua cuốn sách tuyệt vời này :D
4
257278
2015-08-08 12:50:50
--------------------------
222863
3767
Mình khá thích thú với bộ truyên Nữ hiệp quái chiêu của chị Girlne Ya. Truyện có nhiều chi tiết hài hước. Có nhiều truyện đọc rất dễ thấy kết thúc nên hơi nhàm chán. Khi đọc, mình không thể đoán được chi tiết tiếp theo sẽ như thế nào và câu chuyện kết thúc ra sao. Tuy nhiên, cách vẽ các nhân vật của chị hơi bị cứng, không được uyển chuyển cho lắm, người này thì giống người kia, người nữ thì hơi giống người nam,... Ở mỗi tập truyện còn được tặng kèm bookmark từng nhân vật. Nội dung truyên xoay chuyển tốt, phong phú, lãng mạn và đầy khiếu hài hước.
4
635331
2015-07-06 11:54:50
--------------------------
195831
3767
Đây là một bộ truyện hay nhất mình từng đọc.

Mình vô cùng yêu thích bộ truyện này vì phần nội dung rất cuốn hút và lãng mạn,thêm nữa vô cùng hài hước.Nội dung truyện kể về một cô gái bình dân,ước mơ to lớn nhất chính là được hành hiệp trượng nghĩa cứu giúp mọi người khi khó khăn,cô đụng độ hai chàng chai đặc biệt,một là hoàng tử trường Phong Lâm (nơi cô đang học),hai là tên bạn khá thân của cô,đầu củ hành.Trong quá trình thực hiện ước mơ,cô vô tình phải lòng hoàng tử trường Phong Lâm và biết được một bí mật động trời của anh ta,sau đó giữa họ xảy ra nhiều biến cố.

Nội dung hài hước và thu hút,thêm một điều vô cùng tuyệt vời đó chính là nhà xuất bản còn tặng kèm bookmark có hình nhân vật.Mình thật sự không thể bỏ lỡ các tập tiếp theo được.
5
412058
2015-05-14 18:56:09
--------------------------
