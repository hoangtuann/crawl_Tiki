506029
8087
Mình mới mua cuốn này cùng với cuốn Dã Tràng và Thánh Gióng. Mình thích ý tưởng truyện cho trẻ em kết hợp với ghép hình. Cuốn sách giúp bé biết về những câu truyện dân gian của VN đồng thời rèn kỹ năng kiên nhẫn, ghi nhớ khi sắp xếp các miếng ghép. Điều mình không hài lòng là chất lượng của các miếng ghép không khớp với lại khuôn trang. Hơi bất tiện ở chỗ mình phải cho bé ghép các miếng ghép trên mặt bàn và cất vào trong hộp riêng. Chắc là các phụ huynh chỉ chuộng mua sách truyện cổ tích nước ngoài cho các cháu, các truyện dân gian VN này ít người mua nên Tiki giao cho mình cả 3 cuốn đều là sách cũ. Sách có bọc nylong cẩn thận bên ngoài mà bọc nylong cũng cũ. Mình muốn trả lại sách cho Tiki nhưng ngại phiền phức và bé muốn chơi ghép hình ngay, nên thôi. Mình phải đem đi bọc sách vì mới đọc truyện có 1 lần mà bung 1 góc của gáy sách ra rồi. Mình luôn muốn ủng hộ truyện dân gian VN nhưng chắc lần sau cẩn thận ghi chú khi mua hàng là KHÔNG MUA SÁCH CŨ.
3
1731780
2017-01-06 05:52:51
--------------------------
395537
8087
Sách này rất hay, vừa tích hợp các câu truyện cổ tích với các mảnh ghép hình. Mọi người trong gia đình có thể vưà kể truyện vừa xếp hình với các bé. Thông qua việc xếp hình sẽ giúp trẻ có thể phát triển tư duy và nhạy bén của não bộ. Hình ảnh trong sách rất sinh động và phong phú. Rất thích hợp với việc mua và làm quà tặng cho trẻ sắp vào lớp một. Trẻ có thể vừa học chữ vừa chơi. Sách được Tiki đóng gói rất cẩn thận. Cảm ơn Tiki rất nhiều.
3
488605
2016-03-11 23:45:16
--------------------------
