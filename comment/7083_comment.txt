419208
7083
Thấy cuốn này dạo gần đây khá hot nên mình quyết định mua cả bộ về đọc xem dư nào. Lúc đầu đọc thì ôi trời ơi, mạch truyện chậm ơi là chậm, lết gần hết nửa quyển mà vẫn chưa hiểu mô tê gì về  truyện hết ấy. Bìa truyện thiết kế rất đẹp, mang đậm nét không khí của truyện. Bìa thiết kế đẹp, khổ sách nhỏ thành ra sách hơi dày, điểm này mình không thích cho lắm. Chất giấy xốp nhẹ, hơi mỏng, sờ khá thô ráp và không được mịn, giấy có mùi công nghiệp khá nặng, bìa hơi mỏng, dễ nát.
3
902137
2016-04-20 20:19:15
--------------------------
378984
7083
Bìa truyện không được đẹp, có hơi cụt hứng.
Lần đầu tiên đọc truyện về thần thoại Bắc Âu do người châu Á viết, cảm giác khá mới lạ.
Nhìn chung thì giọng văn của Thiên Lại Chỉ Thiên khá ổn, dễ nắm bắt và tương đối mạch lạc, tuy có vài đoạn hơi sến, motip na ná như ngôn tình. Ngôn tình ở đây không phải lỗi gì xấu hay trầm trọng, điều mình muốn nhấn mạnh là  - đối với một câu chuyện viết về các vị thần Bắc Âu - yếu tố này không nên lạm dụng nhiều, cách hành xử của các nhân vật cũng không nên quá đi theo lối mòn hoặc làm tình tiết trở nên dễ đoán.
Truyện mô tả Ena là thợ rèn nhưng mấy đoạn cô rèn búa, gươm, thần khí lại không thuyết phục – lắp vào gõ ra - có cảm giác rất đơn giản, chỉ cần khéo tay một chút và đầu óc nhanh nhạy thì ai cũng làm được.
Nhân vật Ena cũng, nói sao nhỉ, tương đối dễ dãi và ngô nghê, đồng ý là mạnh mẽ đấy, nhưng thật sự tính cách nhân vật này làm mạch truyện chùng chình khá nhiều.
Truyện vẫn còn một số lỗi in không đáng có.
3
198930
2016-02-08 20:41:28
--------------------------
358450
7083
Lúc đầu còn tưởng loki là nhân vật chính của tác phẩm. Ban đầu mình thấy mạch truyện đi rất chậm, mãi mà chưa thấy chút chuyển biến gì. Ngay cả khi ena thích lando cuzng k thể hiện rõ sự nhug nhớ của mình. Cho đến khi loki thức tỉnh, sự bá đạo của hắn làm tôi xóa hết sự bức xúc khi mới đọc. Đây thực sự là một câu chuyện tình rất rất đẹp nếu không đọc quyển sau - không có quá khứ đau buồn giữa odin,frigg và loki. Vì tôi đều yêu họ, yêu cả loki
5
979895
2015-12-25 11:50:37
--------------------------
343916
7083
Tác giả Thiên Lại Chỉ Diên là một cái tên khá lạ lẫm đối với mình, đọc xong truyện thì thấy tác giả viết cũng tương đối chắc tay. Có thể nói đây là một cuốn huyền huyễn lạ với bối cảnh thần thoại Bắc Âu, cuộc chiến của những vị thần, tình yêu thần thoại giữa người và thần thánh. Mình từng đọc thần thoại nên không thấy có vấn đề lắm, nhưng ai chưa từng đọc thì chắc sẽ phải nghiền ngẫm kỹ mới hiểu được. Tình yêu của Ena và Lando trong truyện được khắc họa đẹp và chân thành vô cùng, giọng văn cũng khá hấp dẫn.
4
630600
2015-11-27 14:01:25
--------------------------
283804
7083
Giọng văn của tác giả khá tình cảm, mạch kể khá tốt nhưng có một số chỗ nhà xuất bản còn in quá nhiều lỗi. Tuy nhiên, cách mạch truyện được hình thành xung quanh câu chuyện tình cảm của Ena và Loki rất thuyết phục người đọc, cách Loki trân trọng Ena, những chi tiết vô cùng dễ thương khi họ cùng nhau đến cuộc thi để lấy quả táo vàng của Iris. Cho đến cuối tập 1 có một cái gì đó khiến mình phải đặt mua tiếp tập 2 để xem những điều gì sẽ diễn ra đối với cặp Ena và Loki. 
4
271677
2015-08-30 12:40:24
--------------------------
266541
7083
Chất liệu giấy tuy không được tốt nhưng về nội dung thì hay.... Cách tác giả viết cũng hay, nhất là những đoạn miêu tả phong cảnh hay tâm trạng nhân vật.

Ban đầu mình đọc thì cảm thấy nhẹ nhàng, có chút tình cảm nhẹ nhàng nhưng càng đọc thì càng ngược, ngược đến mức đau tim. 
Loki yêu Frigg từ kiếp trước, nhưng trong kiếp tái sinh thì lại dần dần yêu Ena (tái sinh của Frigg).... nhưng Loki vẫn cố chấp, luôn giữ chấp niệm đối với Frigg không buông xuống được. Làm tổn thương Ena - người con gái yêu anh đến mức chấp nhận hy sinh. Đến khi Loki nhận ra mình đã yêu Ena chứ không phải là tái sinh của Frigg thì đã quá muộn, nàng đã không còn bên anh nữa.

Về Ena, đầu truyện có thể thấy nàng là người con gái quá mức bình thường, không đẹp lộng lẫy, không tài năng khiến người khác hâm mộ. Nhưng sau thì tài năng tiềm ẩn trong nàng dần được lộ ra, và nàng cũng là một cô gái cá tính. Cảm thấy tiếc cho Ena, nàng và Loki không thể ở bên nhau.
4
504684
2015-08-14 19:46:27
--------------------------
261007
7083
Mình không hiểu sao cuốn này lại không có bookmark, bìa truyện cũng khá cũ, chất lượng giấy cũng tàm tạm. Tóm lại, về hình thức, truyện không đạt tiêu chuẩn. Về nội dung, đây là tuýp huyền huyễn lạ, bối cảnh là thần thoại Bắc Âu, do đó, mình gặp nhiều khó khăn trong việc nhớ được tên của các vị thần cũng như mối quan hệ giữa họ.
Đọc tập 1, mình nghĩ rằng nhân vật chính là Loki và Ena (Frigg). Xuyên suốt tập này là sự phát triển tình cảm của họ. Thành thật mà nói thì mình không thích cả Loki lẫn Ena. Lí do là vì Ena thì quá bình thường, còn về Loki, đó là người đàn ông bá đạo một cách đáng ghét. Ngay từ đầu truyện, mình đã thích người trong giấc mơ của Ena hơn tất cả các nhân vật nam khác, và vô cùng thích thú khi cuối cùng cũng biết được đó chính là Shuuji, và cũng là Odin.
Mình không cảm thấy hứng thú khi đọc tập này lắm, không phải tác giả viết quá dở, chỉ là mình vô cùng không thích cách tác giả xây dựng tuyến tình cảm của các nhân vật, cứ cảm thấy "nửa Tây nửa Tàu", khó chịu vô cùng. 
Mong là tập 2 sẽ hay hơn.
3
323629
2015-08-10 20:25:58
--------------------------
258321
7083
Truyện Lời Chúc Phúc Của Odin - Tập 1 của Tác giả Thiên Lại Chỉ Diên là một tác phẩm hay. Mình mua hộ bạn mình cả bộ 3 tập và cũng đã đọc qua. Nhận được truyện của tiki thấy tiki bọc cả 3 truyện Lời chúc phúc của Odin đều đẹp và cẩn thận.Câu truyện kể về những cuộc chiến của các vị thần. Đan xen vào đó là chuyện tình yêu đầy kịch tính và lãng mạn. Phải nói đây là một bộ truyện hay của Tác giả Thiên Lại Chỉ Diên. Mình rất hài lòng với bộ truyện
5
483857
2015-08-08 13:57:10
--------------------------
246614
7083
Ở tập một này, Thiên Lại Chỉ Diên kể về tình yêu của Ena ( Frigg) và Loki (Lando).Câu chuyện vừa mang yếu tố tình cảm xen lẫn hài hước và giả tưởng. Phần đầu nói về tình yêu sâu đậm của Ena và Loki dành cho nhau, đến phần cuối của quyển truyện thì nói dến lúc Ena và Loki cãi nhau và Ena thức tỉnh để trở thành Frigg trở về bên Odin. Mình thấy rất tội nghiệp Loki, rõ ràng là rất yêu Ena nhưng rất nhiều lần khiến chị đau khổ. Nhưng đến cuối cùng thì câu chuyện vẫn rất hay.
5
712296
2015-07-29 18:18:35
--------------------------
222279
7083
Giọng văn ngọt ngào, sâu lắng của tác giả đã dẫn ta đến một câu chuyện tình yêu đầy sắc màu giữa 1 người con gái tầm thường vs 1 người anh hùng toàn năng của cả dân tộc.
Ngay từ khoảnh khắc nhìn thấy Ena, Lando đã trao chọn tình yêu của mình cho cô. Tình yêu của a mãnh liệt, nồng nàn. Anh giành thời gian cho cô, giúp cô tìm kiếm vật liệu quý giá để chế tạp ra cậy quyền trượng toàn năng, giúp cô có được danh hiệu cũng như sự nể phục từ tất cả mọi người.
Ena cũng đáp trả tình yêu nống nàn của Lando bằng sự chân thành, đằm thắm của mình. Vào cái ngày Lando thức tỉnh trở thành chủ thần Loki, Trước sự bá đạo trong tính cách của Loki, mặc dù biết anh không còn là 1 Lando dịu dàng từng nâng niu cô. Nhưng cô vẫn cảm nhận tình yêu anh dành cho cô và đáp trả lại bằng tất cả tình yêu của mình.

Tác phẩm đưa ta vào một khung cảnh thần tiên của vùng đất Vanaheim, nơi câu chuyện tình yêu ngọt ngào xuất hiện và dần dần ngày càng trở nên sâu đậm.
những khung cảnh choáng ngợp, tuyệt sắc trong tác phẩm giúp ta thấy tác giả thật sự tài năng trong việc miêu tả khung cảnh hòa hợp cùng tình yêu của Lndo và Ena.
Thật tuyệt diệu!
5
8593
2015-07-05 11:34:24
--------------------------
