297128
3644
Nhìn chung các tác phẩm của tác giả Hương Giang viết trong sáng, dễ hiểu, phù hợp với trẻ em và giá cả rất mềm. “Lão cáo quỷ quyệt và cô gà mái đỏ” là câu chuyện xoay xung quanh con cáo muốn ăn thịt cô gà mái tơ nên tìm mọi cách để bắt cho được cô. Tuy nhiên, cô gà mái tơ với tính cẩn thận và sự khôn ngoan, dũng cảm của mình đã thoát khỏi lão cáo, khiến cho lão không bao giờ còn dám tìm cách bắt cô nữa. Đây là một trong những truyện mà con tôi rất thích. Câu chuyện dễ thương, hình minh họa đẹp, khổ in xinh xắn, dễ cầm đọc.
4
332766
2015-09-11 16:34:37
--------------------------
