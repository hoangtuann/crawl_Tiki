268316
10144
Mình đã mua đủ bộ 3 cuốn "30 thói quen học sinh tiểu học cần phải rèn luyện", "30 thói quen học sinh tiểu học cần chú ý","30 thói quen học sinh tiểu học cần làm"  cho em trai, sách gồm  các câu chuyện, thông qua những câu chuyện này, các em học sinh tiểu học có thể phân biệt được những thói quen tốt, xấu, rất gần gũi và thiết thực với các em ngày nay trong thời kỳ phát triển nhanh chóng mắt mà các giá trị dần bị mất đi và đã thay đổi nhiều so với thế hệ ba mẹ các em, từ đó phù hợp với tâm lý của trẻ, giúp trẻ có nếp sinh hoạt khoa học, không chỉ hữu ích cho bản thân mình mà còn cho cả những người xung quanh. Mình tâm đắc nhất là sau mỗi câu chuyện lại có thêm mục "Suy nghĩ" và "Làm thử" để trẻ học cách suy nghĩ, thực hàn.

4
467685
2015-08-16 11:24:22
--------------------------
207347
10144
Đây là cuốn sách hay có tác dụng hình thành kĩ năng sống cho các em học sinh. Sách được trình bày đẹp, mỗi câu chuyện đều có hình vẽ minh họa sinh động làm tăng thêm hứng thú cho trẻ khi đọc. Nội dung rõ ràng, dễ hiểu, ngôn ngữ trong sáng mang tính giáo dục cao, sau mỗi câu chuyện còn có thêm mục"suy ngẫm". "làm thử" để trẻ học thêm cách suy nghĩ và thực hành làm theo.Sách bao gồm 5 chương, mỗi chương có nhiều câu chuyện nhỏ rèn cho các em những thói quen tốt trong sinh hoạt, cư xử, thói quen sức khỏe, xây dựng cho các em thói quen đọc sách và những thói quen đạo đức chuẩn mực trong xã hội để dần hình thành nên tính cách, đạo đức cho các em. Đây là cuốn sách quý, các phụ huynh nên mua cho con em mình.
5
455308
2015-06-12 08:25:03
--------------------------
