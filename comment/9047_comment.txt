474792
9047
Tôi vừa nhận được cuốn sách này từ nhân viên giao hàng của Tiki, tôi nhận thấy nội dung cuốn sách rất phong phú, bìa sách cứng, màu sắc và hình ảnh rất đẹp. Cuốn sách giúp bé nhà tôi có được sự khéo léo và sáng tạo, có thể làm được những vật dụng hằng ngày như vương miện, đồng hồ . . . mà không cần phải sử dụng đến kéo vì đã được cắt dán sẵn. Nói chung là tôi rất thích và rất hài lòng vì đã lựa chọn cho con một cuốn sách hay. Cám ơn Tiki rất nhiều.
3
1120397
2016-07-11 22:46:56
--------------------------
426053
9047
Nói chung là mình không thích bộ sản phẩm làm thủ công bé khéo tay của Đông A này lắm, sản phẩm gồm các mảnh rời, không phải đóng thành quyển nên sau khi chơi xong có khá nhiều rác phải dọn, bộ sản phẩm đồ chơi ngộ ngĩnh này có lẽ là khá nhất trong các bộ nhưng điểm trừ mình vẫn thấy là giấy bìa không đủ cứng sản phẩm làm ra có kích thứơc cái thì vừa cái thì chật, mà dính hồ dán thì khá bẩn lem nhem, mà nói chung là cũng không chơi được lâu hơi phí tiền
3
153008
2016-05-06 09:09:36
--------------------------
121303
9047
Mình đã mua tập Đồ chơi ngộ nghĩnh này tặng cho cháu, phải nói là rất tuyệt vời. Sản phẩm làm từ giấy bìa cứng dễ sử dụng và cũng khá bền nếu chơi cẩn thận, in màu rõ nét, tươi sáng, rất sáng tạo, dễ tách không cần dùng tới kéo và phù hợp với các bé hơn. Trong bốn tập thì có tập này đặc biệt nhất vì có thể làm được đồng hồ đeo tay, mắt kính, mũ... thỏa sức làm điệu. Không những vậy sau khi chơi xong, cháu mình còn dùng lại bộ khung để "nhân bản" mô hình nữa. Mình mua ngay lúc giảm giá, giá rất phải chăng nữa chứ.Tóm lại hài lòng lắm lắm!
4
92071
2014-08-18 21:27:42
--------------------------
