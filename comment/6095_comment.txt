126657
6095
Đây là một trong những quyển truyện kể hàng đêm cho thiếu nhi mà mình (khi đã lớn) vẫn cảm thấy thích thú nhất. Điều làm mình thích thú đầu tiên là cái cách trình bày sách rất thiếu nhi, màu sắc cũng như những hình minh họa vô cùng ngộ nghĩnh đáng yêu đính kèm. Bên cạnh đó, nội dung các câu chuyện lại vô cùng gần gũi và ngắn gọn với những nhân vật mà trẻ em nào cũng yêu mến như chú Gà trống hay quên, cô mèo tốt bụng hay bạn heo tham ăn,.. Mình thật sự bị cuốn hút bởi những câu chuyện lý thú và vui nhộn trong quyển truyện này.
3
45959
2014-09-19 19:49:03
--------------------------
