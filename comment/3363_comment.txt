468869
3363
Nguyễn Phú Khánh là một tác giả viết sách tham khảo Toán khá quen thuộc. Mình đã mua những cuốn sách phân dạng và hướng dẫn giải toán lớp 10,12 do Nguyễn Phú Khánh viết cùng các tác giả khác thấy dễ sử dụng. Cuốn này mình mua về tham khảo thấy tác giả viết cũng khá dễ hiểu và chất lượng. Nhưng vì chỉ trong moit cuốn sách mà tác giả bao quát hết cả mười câu trong đề thi quốc gia nên các dạng bài chỉ ở mức tổng quát, nếu muốn đi sâu kiếm điểm 9,10 thì nên tham khảo sách chuyên đề về từng dạng như bđt, pt, bpt,...
4
1186917
2016-07-05 18:00:39
--------------------------
280905
3363
" Cấp tốc giải 10 chuyên đề 10 điểm thi môn toán" của tác giả Nguyễn Phú Khánh là một cuốn sách tự học toán 12 hay và bổ ích. Sách được biên soạn đầy đủ kiến thức toán 12 với lí thuyết cơ bản được tóm tắt dễ hiểu, phương pháp giải rõ ràng, phân dạng khá chi tiết, ví dụ giải cụ thể kèm lời giải được trình bày lô-gic và dễ hiểu. Có lời giải nhưng tác giả vẫn khuyến khích học sinh cố gắng tìm tòi, suy nghĩ theo lối riêng bản thân giúp tìm ra cái mới, sự so sánh tìm ra con đường thuận tiện hơn. Về hình thức, cuốn sách này làm tôi rất ưng ý với hình thức đẹp, chất giấy và chất lượng in tốt, dễ dàng mở sách ra mà không phải đè mạnh để mở giữ cho sách cố định. " Cấp tốc giải 10 chuyên đề 10 điểm thi môn toán" và các tác phẩm khác cùng tác giả thể hiện tài viết sách hay của Nguyễn Phú Khánh.
5
661175
2015-08-27 23:20:45
--------------------------
