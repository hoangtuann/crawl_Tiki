351049
4072
Một cuốn sách hay. 
Một cuốn sách lối kể giản dị, câu chuyện ý nghĩa. Sách rất phù hợp với trẻ nhỏ. Dễ nhớ, dễ gần, rất thích hợp rèn luyện tính cách, và giáo dục trẻ nhỏ. 
Sách minh họa rõ ràng, hình ảnh sinh động, đẹp mắt. Các câu chuyện đều thú vị, ý nghĩa.
Các bé từ 4 tuổi trở lên là phù hợp nhất.
Thanks Tác Giả và Nhà Xuất Bản đã cho ra một cuốn sách cần thiết cho các bậc bố mẹ hiện nay.
Tóm lại là một cuốn sách nên mua. Giá cả lại khá mềm.
5
160916
2015-12-11 16:39:19
--------------------------
318568
4072
Mỗi câu chuyện trong " Chuyện kể trước giờ đi ngủ" là một bài học quý giá cho bé. Các câu chuyện với chủ đề và ngôn từ gần gủi dễ chạm vào trái tim thơ ngây và nhận thức non nớt của bé. Bé nghe và hiểu và tự nguyện làm theo. Con tôi năm nay 4 tuổi, bé đã nghe tôi đọc một mạch hết cả quyển sách. Bé thích nhất chuyện "quả cam là của tớ" và " Bé Tí học theo chim vành khuyên". Nhờ "chuyện kể trước giờ đi ngủ" mà tôi có thể dạy bé lễ giáo và biết chia sẻ cũng như cách giao tiếp với những người xung quanh một cách rất nhẹ nhàng mà bé hiểu và thực hiện theo một cách rất tự nguyện.
4
134875
2015-10-06 13:33:00
--------------------------
290139
4072
Sách "Chuyện kể trước giờ đi ngủ" là các câu truyện xoay quanh cuộc sồng của các bạn thú vật trong gia đình của bé Tí. Các con vật thân thuộc, gần gũi với người Việt Nam: bác bò, bác gà, heo, cô mèo. anh gà trống, bạn bó. Mỗi câu chuyện đều có hình ảnh minh họa rất dễ thương và màu sắc. Mình quyết định mua sách đầu tiên là vì thương hiệu Alpha Book, sau khi đọc thử cũng thấy câu chuyện dễ thương nên mua luôn. Hihi. Mặc dù con gái còn nhỏ nhưng cũng rất thích nghe mẹ kể chuyện, nhưng đọc 3, 4 lần vẫn chỉ nhớ được nhà bạn Tí có những con gì thôi. Chắc do bé còn nhỏ, nhưng dù sao cũng cám ơn Tiki đã mang lại món quà này cho 2 mẹ con mình
4
227811
2015-09-05 09:24:12
--------------------------
240200
4072
Cuốn sách nói về những câu chuyện xung quanh cuộc sống hàng ngày rồi qua đó dạy bé các bài học trong cuộc sống. Mình đọc cho bé  câu chuyện trong sách, mỗi câu chuyện mình đều đọc đi đọc lại nhiều lần cho bé nhớ và yêu cầu bé trả lời câu hỏi về các nhân vật trong truyện, hành động của các nhân vật đúng hay sai, yêu cầu bé phân tích và giải thích tại sao. Sau đó mình nói bé kể lại toàn bộ câu chuyện trên theo cách hiểu của bé. Mình cảm thấy các câu chuyện trong cuốn sách rất phù hợp với các bé độ tuổi 4-6 vì độ tuổi này bé sẽ hiểu các câu chuyện rõ ràng và đầy đủ hơn, cuốn sách cũng giúp các bà mẹ dạy cho các bé những bài học mỗi tối trước khi đi ngủ :)
5
622473
2015-07-24 13:30:48
--------------------------
229187
4072
Sách "Chuyện kể trước giờ đi ngủ" của Alphabook gồm 10 câu truyện xoay quanh cuộc sồng của các bạn thú vật trong gia đình của bé Tí, mối bạn sẽ làm nhân vật chính của một chuyện gồm một chú cún con, 1 cô mèo, một bac bò, 1 bác heo và 1 anh gà chống. Các câu chuyện đều có hình ảnh minh họa rất dễ thương và màu sắc.
Mình mua sách một phần cũng vì thấy là do Alphabook xuất bản nên về chất lượng mình không thấy có gì bàn cãi cả, tuy nhiên do bé nhà mình còn bé, một câu chuyện lại hơi dài nên chưa thích hợp lắm
4
153008
2015-07-16 16:02:00
--------------------------
