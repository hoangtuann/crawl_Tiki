436485
4718
Đúng là không làm mình thất vọng. Quyển sách không chỉ gây ấn tượng với mình ở bìa sách dễ thương, cái tựa rất hút mà điều quan trọng là những câu truyện bên trong như nhắc nhở mình: Có phải thời gian đã khiến mình vô tình quên đi điều gì đó rất đáng trân trọng không. Đọc xong bỗng nhớ về khoảnh khắc của ngày xưa, cái ngày trước khi mình được làm bạn với cô đơn. Cũng đã từng yêu thương hết mình. Có lẽ đã đến lúc bước qua mùa nhớ để tìm lại mùa thương
5
947368
2016-05-26 19:53:52
--------------------------
345141
4718
Mình không hẳn là người đọc sách khó tính và kén chọn. Nhưng câu chuyện đầu tiên mình không thích cho lắm. Có điều gì đó khó hiểu, cô gái này bắt đầu thích chàng trai này từ khi nào khi chỉ gặp có 1 lần? Chàng trai đó có gì cuốn hút cô gái ấy? Mình không nói ngoại hình nhưng tính cách của chàng trai này ra sao mình còn chưa nắm được thì cô gái đã thích chàng trai này rồi. Truyện bị ngắt quãng nhiều đâm ra chán đọc. Mình đã định không đọc nữa. Nhưng có điều, suýt nữa thì mình phải hối hận, vì những câu chuyện sau đều hay, nhẹ nhàng, và như là ta có thể bắt gặp đâu đó trong cuộc sống.
Có 1 câu chuyện mình rất thích "Ngày bão dịu dàng", ngày bão mà dịu dàng sao? Dùng từ lạ quá, nhân vật chính trong câu chuyện cũng nghĩ như mình vậy. Nhưng đó lại là câu chuyện đẹp, không phải tình yêu đôi lứa, tình cảm rất thật giữa người với người. 1 Cô bé có tâm hồn đẹp, lạc quan nhưng căn bệnh quái ác đã cướp em đi. Mình đã bật khóc, thật đó, mình đã khóc khi câu chuyện kết thúc. 
Nói chung, bìa đẹp, có bookmark nho nhỏ, truyện hay, mình cũng không thấy tiếc khi mua nó :3
4
1014490
2015-11-29 21:21:22
--------------------------
341784
4718
Mỗi câu chuyện đem đến một cảm xúc khác nhau...co khi là bình yên nhẹ nhàng... hay mỉm cười khi nhận ra mình đâu đó trong từng câu chuyện... những câu chuyện đã tiếp thêm niềm tin cuộc sống...làm lòng bình yên giữa những ngày nổi bão...từng câu từng chữ... đã viết nên cảm xúc của tôi... giữa những ngày thế này... quyển sách duyên phận này đã đến bên tôi... giúp tôi ghi lại cảm xúc ngổn ngang trong đầu mình...đã tôi đi qua những ngày quá nhiều những phiền muộn...  tôi cũng mong rằng QUA MÙA NHỚ... tôi sẽ GẶP MÙA THƯƠNG.... I LOVE BOOK.... and TIKI...
5
951430
2015-11-22 23:30:52
--------------------------
316546
4718
Tiêu đề ấn tượng, bìa sách khá đẹp, nổi bật. "Qua mùa nhớ gặp mùa thương" mang lại cho người đọc những suy ngẫm, khoảng khắc yêu thương và nhớ nhung mà ta đã vô tình bỏ qua nó trong cuộc đời. Đọc nó khiến bạn suy nghĩ, chắc rằng, trong khoảng thời gian nào đó, bạn đã vô tình quên nhớ một ai đó. Và có đôi khi, bạn cảm thấy mình lạc lõng giữa cuộc đời. Truyện ngắn đã đem lại cho bạn những khoảnh khắc yêu thương, lấp đầy khoảng trống tâm hồn, làm chúng ta quên đi cảm giác lạc lõng, bước ra khỏi thế giới cô đơn mà mình tự tạo. Một quyển sách sẽ không làm bạn thất vọng khi mua nó và đọc nó.
4
823967
2015-10-01 08:04:39
--------------------------
302656
4718
"Qua mùa nhớ gặp mùa thương"

Thời gian trôi qua nhanh, từng ngày từng ngay với những công việc quen thuộc, một ngày tình cờ lướt trên tiki và thấy tựa sách này, những cảm xúc trong tôi tràn về. 

Đã rất lâu, rất lâu rồi tôi không còn thương, không còn nhớ một ai đó.

Cầm được quyển sách trên tay, tôi rất thích vì sách được in rất đẹp, ảnh bìa thật dễ thương.  Những bài thơ, những cây chuyện thật nhẹ nhàng, sâu lắng, dẫn dắt tôi về với những kí ức xưa, rung động xao xuyến, ước mong được thương, được nhớ...
5
409134
2015-09-15 13:12:48
--------------------------
274636
4718
"Qua mùa nhớ gặp mùa thương" mình đã ấn tượng cái tên này khi thấy nó, quyển sách còn rất đẹp, rất dễ thương. Mới đầu mình tưởng chỉ có truyện ngắn, nhưng khi mua về mới biết thì ra còn có phần thơ. Mình thực sự rất thích quyển sách này, những câu chuyện, những dòng thơ thật thơ mộng, nhẹ nhàng và sâu lắng, từng câu chữ được trau chuốt tỉ mỉ, tinh tế và cẩn thận, khiến một đứa còn "FA" như mình cũng thấy rung động xao xuyến giống như chính mình đã từng yêu, từng được yêu thương vậy.
5
697477
2015-08-22 08:59:35
--------------------------
259844
4718
Đọc Qua mùa nhớ gặp mùa thương chợt thấy lại những ngày mà cảm xúc còn chưa biết gọi thành tên, cái thời mà cậu bạn cùng lớp là cả một thế giới giỏi giang và đầy ngưỡng mộ, rồi đến cái thời mà cảm xúc là điều trở đi trở lại trong những trang nhật ký, để rồi sau đó dòng chảy của cuộc sống kéo ta vào những lo toan, những bộn bề, chẳng còn thời gian cho những điều đã xa xôi ấy nữa. Bất chợt gặp lại những cảm xúc ấy,tưởng chừng như ta mới chỉ mười tám đôi mươi, trong trẻo với những xúc cảm đầu tiên.
4
415825
2015-08-09 21:33:01
--------------------------
238216
4718
Từng theo dõi trên page của Tôi Thích Viết nên khi sách phát hành mình đã quyết định mua nó. Chưa biết đến nội dung ra sao nhưng chỉ cần xem qua bìa sách thôi cũng đã khiến mình thích thú. Sách được chia thành 2 phần truyện ngắn và thơ với cách viết nhẹ nhàng, tinh tế và sâu sắc với nhiều câu chữ được chăm chút kĩ càng. Mình đặc biệt thích các bài thơ của tác giả September Rain với lối viết rất giàu cảm xúc. Đây xứng đáng là tác phẩm nên có trên giá sách nhà bạn đấy.
4
330563
2015-07-23 02:20:35
--------------------------
223039
4718
Cuối tháng trước mình lướt Tiki và gặp được nó - Qua mùa nhớ gặp mùa thương. Thấy bìa sách đẹp, vào đọc thử thì thấy khá là hay, nhiều cảm xúc. Mình phân vân có nên mua em nó hay không và cuối cùng là chọn mua vì thấy sách hay như này thì sợ hết hàng mất.Công nhận là bìa sách rất đẹp, đẹp nhất trên giá sách của mình. Đọc xong thì trong lòng trỗi dậy nhiều cảm xúc, đan xen nhau về những tình yêu trong cuộc đời. Mình đã không sai khi chọn mua em này về. Bây giờ vào viết nhận xét thì thấy hết hàng rồi. Mong rằng Tiki sẽ sớm đưa hàng về cho những bạn chưa được đọc em này nhé.
                                                                   Yêu thương 
5
673028
2015-07-06 15:04:48
--------------------------
214288
4718
Đôi khi tôi thật sự nhận ra rằng đã lâu lắm rồi mình chẳng nhớ, chẳng thương hay để ý một ai. Thả tâm trạng khốn đốn ngày qua từng trang sách, từng mãu chuyện ngắn, từng bài thơ thậm chí không chắc là mình có hiểu hết được hay không. Khẽ rơi nước mắt về nhân vật và về cả những chuyện mình đã trãi qua, khẽ mỉm cười và chợt nghĩ ngày ấy sao mình vô tư quá. Biết bao giờ cho cái cảm giác tình yêu đầu vụn dại và thơ mộng ấy có thể quay lại, à, chắc là chẳng bao giờ. Hãy thử bước qua mùa nhớ, đằng sau đó là cả một mùa thương.
4
328816
2015-06-24 19:56:10
--------------------------
205066
4718
"Qua Mùa Nhớ Gặp Mùa Thương" là một tác phẩm đặc biệt, bởi không chỉ bản thân nó đặc biệt mà còn đặc biệt bởi tác giả - Tôi thích viết.
Một bút danh rất hay, rất ý nghĩa, chắc vì thế mà quyển sách này cũng đầy ý nghĩa.
Lối viết nhẹ nhàng mà sâu sắc, những câu từ ấn tượng, để lại cho người đọc những ấn tượng trong tâm hồn, tìm lại những cảm xúc đâu đó của chính mình qua câu chuyện.
Rất hay, rất nhẹ nhàng mà đầy cảm xúc!
Ngoài ra, bìa sách được minh họa rất dễ thương, rất đẹp, mình rất thích.
5
621744
2015-06-05 17:30:14
--------------------------
195965
4718
Điều mình thích nhất ở tác phẩm này là vì nó là sự kết hợp của hài hoà của những mẩu truyện ngắn và cả các bài thơ. 
Những truyện ngắn trong cuốn sách này rất nhẹ nhàng thôi nhưng để lại ấn tượng sâu sắc trong tôi. Đương nhiên tôi không ấn tượng với tất cả, nhưng quả thực tôi đã tìm thấy ở đâu đó hình bóng mình trong truyện. Có những hình ảnh, chi tiết trong truyện, và cả những câu thơ, vẫn cứ ám ảnh trong tâm trí kể cả khi mình đã đóng sách lại rồi. Và nói lại một lần nữa, những bài thơ trong này rất hay và rất lạ, rất mới, các bạn nên thử tự mình trải nghiệm.
Xét về hình thức, bìa cuốn sách cũng rất đáng yêu, gây thiện cảm với tôi ngay từ cái nhìn đầu tiên.
Một cuốn sách đáng đọc.
4
40293
2015-05-14 23:19:39
--------------------------
192683
4718
Qua mùa nhờ gặp mùa thương là tác phẩm gồm truyện ngắn và thơ bao gồm nhiều tác giả trẻ đã có mặt trong nhiều sách mới ra. Những câu chuyện hài hòa nhẹ nhàng mà mới lạ bởi cốt truyện mang đến cho tôi cảm giác mới lạ bởi tác phẩm này. Đây là quyển sách dễ thương tù tên sách cho tới nội dung, nhưng mối tình đầu khắc cốt ghi tâm những bài thơ tự do không chỉ là tình yêu mà còn cả tình cảm gia đình. Chất lương giấy tốt bìa ngoài đẹp và mới la không gây nhàm chán cho người đọc người xem
4
464921
2015-05-05 12:48:22
--------------------------
189329
4718
Đây là quyển sách mà có phần minh họa trang bìa làm mình thích nhất trong tất cả trên kệ sách của mình, rất dễ thương, đáng yêu. "Qua mùa nhớ gặp mùa thương" là sự kết hợp thơ và truyện ngắn, giúp mình bớt nhàm chán hơn so với khi cầm trên tay quyển sách chỉ toàn 1 thể loại. Những tác phẩm trong đây rất hay và ý nghĩa, thỉnh thoảng mình bắt gặp được hình ảnh của chính mình qua từng dòng văn, câu thơ. Để rồi đóng sách lại, những cảm xúc như còn vẹn nguyên, thấy nhớ và thương...Cảm ơn 1 quyển sách tuyệt vời, rất phù hợp cho các bạn trẻ.
4
131327
2015-04-27 07:45:55
--------------------------
181695
4718
Đây là một trong những quyển sách mà tôi thích nhất.Quyển sách gồm những mẫu chuyện và những bài thơ rất hay.Đọc qua mỗi bài thơ mỗi câu chuyện ta sẽ rút ra được những bài học vô cùng quý giá và bổ ích.Đây là quyển sách rất thích hợp cho các bạn đặc biệt là các bạn tuổi teen.Quyển sách này rất bổ ích.Hiện tại mình đang đọc quyển sách này.Mình biết được quyển sách này là do một người bạn đã giới thiệu và cho mình mượn đọc.Chỉ mới đọc mấy trang thôi mà mình đã thấy thích thú.Mỗi câu chuyện mỗi bài thơ đều rất bổ ích.Mình rất thích quyển sách này
4
501467
2015-04-12 11:36:44
--------------------------
