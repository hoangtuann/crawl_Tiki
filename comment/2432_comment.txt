37823
2432
Đúng là vốn từ vựng mà cuốn sách này cung cấp không nhiều, còn lặp đi lặp lại một số từ, sai chính tả một số chỗ. Tuy nhiên trường ĐH ngoại ngữ sử dụng nó làm giáo trình và là một SV của trường tôi thấy cuốn sách rất hữu dụng trong việc giúp các SV tương tác vs nhau cũng như tổ chức hoạt động nhóm rất hiệu quả.Các ví dụ sát với thực tế, dễ hiểu và phân tích. Các bài tập dễ đối với phần lớn sinh viên. Điều ta học được ở đây là làm quen với việc sử dụng tiếng anh trong kinh doanh, đơn giản, thông dụng và rất căn bản. Một lựa chọn chấp nhận được với những ai muốn tự học tiếng anh thương mại.
3
40676
2012-08-27 20:05:18
--------------------------
37713
2432
Sách này thì mình nghĩ là những ai học về khối ngành kinh tế chắc không xa lạ gì. Mình cũng đang học giáo trình này và nói thật là giáo trình này rất chán. Ngoại trừ những bài đọc được trích từ những bài báo của các tạp chí nổi tiếng như FT hoặc là Guardian thì mình nghĩ sách này còn lại chỉ toàn điểm trừ. Thứ nhất sách này rất chú trong vào thảo luận cho nên nó chỉ hay khi bạn học tập trong một môi trường tranh luận sôi nổi, còn về từ vựng dù rằng co cung cấp thêm kiến thức cho bạn nhưng nó lại không nhiều, vì vậy những ai muốn có thêm vốn từ vựng phong phú thì không nên mua quyển sách này vì mục đích của nó không phải chỉ là cho bạn ngồi đó ôn luyện không mà là phải tranh luận và nêu lên lập trường. 
3
17940
2012-08-26 11:06:41
--------------------------
