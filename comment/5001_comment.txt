433279
5001
Mới vừa đọc thật sự truyện rất khó hiểu, hai nhân vật cứ làm mình nhầm lẫn với nhau hoài luôn, nhưng càng đọc càng bị lôi cuốn theo nhân vật luôn á hihi. Từ những lần xa gia đình, những chuyến đi đến tận sáng như thế thật thích và có thể thực hiện được những sở thích của mình. Mình cũng rất muốn được thử một lần như vậy.. Quả thật mình chỉ mới đọc truyện gần đây thôi nên chưa biết nhiều tác giả. Nhưng "thành phố ngày ta yêu nhau" thì mình đã đọc lại ba bốn lần rồi vẫn thấy hay và kết thúc rất tuyệt...
4
590169
2016-05-20 18:46:44
--------------------------
414377
5001
Mình rất thích cuốn truyện này! Bìa rất đẹp. Lâm mạnh mẽ, rất độc lập dù là một tiểu thư nhà giàu. Du lại là một người con gái rất cá tính, lạnh lùng. Dù bị Thùy Chi chơi xấu nhưng Du rất bảo vệ lâm và Lâm đã rất thân thiện và họ đã trở thành bạn. Lời văn của tác giả hay, nhẹ nhàng và thật sự đã toát lên cá tính của những người trẻ ở Hà Nội. Truyện vẽ lên một cuộc sống tươi đẹp, đầy màu sắc và cũng không hẳn là yên bình của giới trẻ bây giờ. 
5
533552
2016-04-11 16:35:51
--------------------------
390712
5001
Tiki giao hàng nhanh hơn cả hẹn luôn cơ, rất cảm kích!!! Tôi thích cả bìa sách lẫn nội dung bên trong. Tôi học được cái tính tự lập của Lâm, tôi biết quý trọng những phút giây của cuộc sống hơn. Tôi mạnh mẽ chống lại thử thách hơn. Tôi còn được trải nghiệm về con phố Hà Nội cũng như được cảm nhận được tất tần tật không khí Hà Nội nữa cơ. Bạn hãy đọc và cảm nhận trong những chiều hoàng hôn và con người bạn sẽ cảm giác man mác rất lạ mà không thể diễn tả bằng mọi ngôn từ...
Cảm ơn Tiki rất nhiều!!!!!
5
1033381
2016-03-04 12:36:46
--------------------------
168257
5001
Tôi rất thích cuốn sách này cả bìa sách lẫn nội dung bên trong. Nghe tựa cuốn sách và nhìn bìa thôi, tôi đã rất muốn đọc, và tôi biết mình không lầm. Tôi khâm phục tính tự lập của Lâm, dù là tiểu thư, được sống trong nhung lụa giữa Sài Gòn cùng bố mẹ nhưng vẫn quyết tâm về lại Hà Nội để bắt đầu cuộc sống do mình làm chủ. Khi bị chơi xấu bởi Thùy Nhi vẫn mạnh mẽ vượt qua và trở thành bạn của nhau. Tôi nhìn thấy được trong đó một Hà Nội gần gũi, đáng yêu, thân thuộc bởi những người trẻ sống và làm những điều mình thích, giúp ích cho xã hội. Những người bạn của Lâm từ Du, Minh, Đức đến Thùy Nhi và Nguyên-chàng trai của Lâm, tất cả vẽ ra một bức tranh muôn màu phong phú về tình yêu, tình bạn, về Hà Nội thân yêu. Tôi rất muốn được đến Hà Nội một lần sau khi đọc xong cuốn sách này.
4
439061
2015-03-16 11:41:08
--------------------------
