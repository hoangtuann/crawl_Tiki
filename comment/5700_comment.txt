479594
5700
Quyển sách có nội dung nhẹ nhàng nhưng sâu lắng. Truyện có cốt truyện trải dài theo trình tự 12 tháng trong năm, từng tháng từng tháng mang theo biết bao nỗi ưu tư tuổi học trò. Bất cứ bạn là ai, ở đâu, làm gì thì cũng bắt gặp chút gì rất riêng tư của mình ở trong đó. Một quyển sách nhắc nhớ về thời học sinh nhiều kỷ niệm! 
Bìa sách thiết kế đơn giản nhưng bắt mắt, tông màu ấn tượng. Bìa dày, nếu bạn kèm theo gói bọc sách tikicare thì tuyệt, bảo quản sách tốt hơn (dù lớp bọc sách che mất phần bìa dùng để chặn sách)
Chú Tiki giao hàng dễ thương, đúng hạn, rất hài lòng !
4
1199770
2016-08-10 21:55:34
--------------------------
375830
5700
Đọc về miền Bắc. đọc về cuộc sống của miền Ngoài mà không đọc về Thương nhớ mười hai của Vũ Bằng thì không được. Mình thích nhất là đoạn văn miêu tả về ngày tết. Lúc đó không khí tết, những món ăn tết như bánh chưng, dưa chả, mỡ hành, những câu đối đỏ, cây nêu xanh được dựng lên... và khi mà giai đoạn Tết quá đi, tất cả mọi người trở về nhịp sống hối hả thường ngày, và món ăn" bát canh trứng cua ăn mát như quạt vào lòng" khiến độc giả như mình khoái vô cùng. Vũ Bằng đúng là tác giả cho nhiều vùng quê của Việt Nam,
5
568747
2016-01-29 12:07:49
--------------------------
321135
5700
Đã từng nghe nhiều về tác phẩm nổi tiếng Thương nhớ Mười hai của nhà văn Vũ Bằng. Tuy vậy, đây là lần đầu tiên mua sách về ngồi nghiền ngẫm. Một phần cũng vì thích thú cái bìa sách giàu tính hình tượng của ấn bản mà Nhà xuất bản Kim Đồng thực hiện. Cuốn sách này không phải là một cuốn dễ đọc với đại đa số độc giả trẻ được, vì văn phong đậm tính cổ điển và những dòng chảy cám xúc của tác giả cử mênh mông tràn trề. Nhưng một khi đã đọc thì sẽ bị cuốn hút. Câu chữ, lời văn đẹp đẽ nhẹ nhàng. Thật xứng danh là một trong những tác phẩm văn học bất hủ của nền văn chương Việt Nam hiện đại.
5
531312
2015-10-13 11:48:47
--------------------------
