406193
10042
Tôi mua Doraemon Bóng Chày (Phiên Bản Mới) - Tập 10 cách đây khoảng 1 năm để tặng cho em trai học lớp 2. Hy vọng cậu bé sẽ vui và sẽ tăng khả năng đọc, viết, chính tả qua những trang truyện tranh lý thú, không khô khan nhàm chán như sách giáo khoa. 
Doreamon đối với tôi như một phần tuổi thơ dữ dội của mình. Tuổi thơ không máy tính, không điện thoại thông minh, không tivi internet. Tuổi thơ chỉ mỗi ngày đi học, đi chơi với chúng bạn, dắt díu nhau ra hàng truyện thuê về đọc rồi đổi cho nhau.
5
110566
2016-03-27 21:26:12
--------------------------
223121
10042
Trong tập lần này, đội Doras đã chính thức bước vào trận đấu Big Dome Cup và gặp Whitners thuộc loại "không phải dạng vừa" với cú ném ba lần zíc zắc WWW huyền thoại của Shiroemon . Thoạt đầu mình cứ nghĩ đội Doras sẽ không thể vượt qua cú ném quá ảo diệu và không-thể-chế-ngự này, nhưng mà bất ngờ đã xảy ra, bằng sự cố gắng và quyết tâm, đội Doras và đặc biệt là nhờ có Kuroemon "thần thánh" đã làm nên được một kì tích. Thật sự rất hồi hộp và gay cấn, không chỉ vậy, mỗi cú ném, mỗi trận đấu đều mang những chi tiết hài hước khó đỡ. Thật sự rất thích tập này nói riêng và cả bộ truyện Doraemon bóng chày nói chung, giống như một phần của tuổi thơ vậy á. 
4
174330
2015-07-06 16:47:36
--------------------------
51635
10042
Để chân thành nhận xét thì tôi thích Doraemon bóng chày hơn là  các tập Doraemon truyện ngắn. Vì Doraemon bóng chày vừa hài hước lại vừa có kịch tính. Nói chung hỉ nộ ái ố cũng được tái hiện đầy đủ qua các trận bóng. Đọc hết tập này sang tập khác vì muốn biết được kết quả của các trận bóng chày. Những vụ chơi xấu của đội bạn hay thỉnh thoảng hay có những bất đồng quan điểm trong chính đội mình. Nói chung thì tôi vẫn thích Doraemon Bóng chày hơn vì nó cho tôi cả cảm giác hồi hộp lẫn sảng khoái vì những chi tiết hài hước và kịch tính qua từng câu chuyện, từng trận bóng. Hơn thế nữa nó phù hợp cho mọi lứa tuổi và đọc hoài đọc đi đọc lại cũng vẫn không cảm thấy nhàm chán! Một bộ truyện tuyệt vời:x
4
66838
2012-12-22 01:02:50
--------------------------
