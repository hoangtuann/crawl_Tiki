355210
4106
Quyển sách tham khảo này đáp ứng nhu cầu của 1 phụ huynh có con học lớp 2 như tôi trong việc mỗi tối, học bài cùng con. Làm các bài tập môn tiếng việt, từ tập đọc, trả lời các câu hỏi trong bài, viết chính tả, tập làm văn, làm các bài tìm vần, từ thích hợp. Cuốn sách hướng dẫn cách phụ huynh có thể học cùng con mỗi ngày. Tôi nghĩ, khi hiểu được nội dung của 1 bài học. Con trẻ sẽ thích thú với môn Tiếng việt và dễ dàng tiếp thu môn học hơn. Tôi hài lòng khi có trong tay 1 công cụ học tốt cùng con mình. Sẽ ủng hộ tiki trong mảng sách tham khảo bên cạnh sách giáo khoa.
3
645116
2015-12-19 14:26:31
--------------------------
