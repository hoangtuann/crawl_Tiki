29856
47
A imaginary noble who believes that he still live in the past with knight the dragon and the lady. It is a funny story, a kind of relaxing book for the one who just want laugters. But it is a also a sad and emotional book for who want to understand more about the sunk ideas and reveal the true message of the author.
Yes, this is not a normal funny story, it is a bell to wake you up, to notify you that this is the time to refresh your mind and your mind and your thinking. Do not emerge yourself into the past, face up with the present, accept it and bravely live with it. 
However, I know the author does not want to make fun of the kind of people metioned above, he just sees them as pitiful ones and really want to help them to get through their fears and obsessions. This is a story that is suitable for everyone, it will give you refreshment as well as meaningful thoughts. Read it and stay with it
5
17940
2012-06-09 07:31:42
--------------------------
11070
47
Tuy là tiểu thuyết viết về một nhà quý tộc hay mơ mộng làm hiệp sĩ nhưng 'Don Quixote' lại mang tính thực tế và trào phúng đậm đặc. Tác giả Cervantes đã thật tinh tế khi lồng những sự việc có vẻ ngớ ngẩn với những biểu hiện của vẻ đẹp tâm hồn của các nhân vật trong tác phẩm, đặc biệt là nhân vật Don Quixote. Từ đó, các nhân vật của tác phẩm rất 'thực', không bị lí tưởng hóa một cách thái quá, là những con người tế nhị, biết thương yêu đồng loại, yêu quý tự do, biết trọng đạo lý và ghét thói xa hoa ăn bám của bọn quý tộc đương thời. Cũng từ đó, Cervantes đã nêu lên được sự chế giễu của mình dành cho những tư tưởng phong kiến đã lỗi thời, cần được xóa bỏ để có tể xây dựng một xã hội mới tiến bộ hơn.
5
2851
2011-09-09 14:14:40
--------------------------
