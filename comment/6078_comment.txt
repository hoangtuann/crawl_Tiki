450425
6078
Nam Âm là nhân vật nữ tôi yêu thích nhất trong bộ Hồi ức Long Thành. Cô ấy cũng là nữ chính trong tập 3 của bộ sách này.
Một Nam Âm tiểu thư đáng yêu được ba mẹ, anh chị quan tâm chiều chuộng ngày nào đã dần trưởng thành qua năm tháng. 
Từ một cô bé con ngây thơ trong sáng trở thành cô nữ sinh tinh nghịch rồi sinh viên đại học. Trong bối cảnh đại gia đình họ Trịnh với nhiều biến cố, Nam Âm cũng lặng lẽ hấp thụ và thấu hiểu những góc tối tăm trong gia đình. Những va vấp trong tình yêu thời thanh xuân tuổi trẻ đã tạo nên một Nam Âm trưởng thành đầy màu sắc. Cô đã trở thành một người đàn bà thực thụ. 
Cảm ơn tác giả đã viết một bộ sách hay như vậy. 
Mình hơi thất vọng một chút vì sách của Tiki đến tay mình hơi lem nhem và cũ nữa. Nhưng vì sách hay nên mình bỏ qua lần này. Hy vọng các bạn cẩn thận hơn trong những lần giao sách sau.
4
41486
2016-06-18 20:56:26
--------------------------
136884
6078
Hồi Ức Long Thành Tập 3 - Tôi Muốn Tin Tình Yêu Là Mãi Mãi làm tôi trải qua biết bao nhiêu cung bậc cảm xúc rốt cuộc cũng cho tôi được một cái kết mãn nguyện . Tuy cái kết đó chứa đựng nhiều bi thương và nước mắt nhưng tổng thể các nhân vật người tốt kẻ ác gì đều nhận được kết quả xứng đáng . Đọc truyện tác giả dẫn dắt tôi vào một thế giới ngôn tình màu sắc đẹp rự rỡ nhưng cũng có phần bi thương . Tôi rất thích nhân vật nữ chính trong cuốn truyện này . Cô luôn tỏ vẻ cao ngạo kiên cường để che giấu trái tim tan nát bên trong . Có một câu như thế này người nào càng tỏ ra mạnh mẽ người đó càng muốn được quan tâm và nam chính đã làm được điều đó
4
337423
2014-11-23 21:03:09
--------------------------
