485994
6357
- Huhu, may quá vừa mua xong là quyển này hết hàng, ngẫm lại thấy mình may mắn thật.
- Đây là quyển luyện viết mà minh chọn ra giữa hàng ngàn quyển, bìa đẹp, bên tron chất lượng cũng đẹp. Các mẫu luyện viết thì không nói đến, chỉ bàn về những đoạn thơ văn hay được trích dẫn có chọn lọc. Rồi có hẳn khá nhiều bài nhưng thật ra quyển nhẹ, bìa cầm chắc tay. Nói chung lá săn phẩm này mình cảm thấy rất ưng ý. Ok, đáng mua cho nhưng em nhỏ học tiểu học hoặc nhưng bạn trung học mà chữ còn xấu như mình.
5
286362
2016-10-07 22:04:41
--------------------------
419585
6357
Vở này không phải tập viết theo thứ tự bảng chữ cái mà tập viết theo giáo trình sách giáo khoa thì phải? Mình thấy thường tập viết các nội dung học trong sách giáo khoa, như vậy cũng tốt, vừa luyện chữ vừa ôn bài. Vì lớp một vẫn viết kiểu chữ to nên rất tốn vở, tuy nhiên theo mình thấy thì cũng viết đầy vở, không lãng phí. Vả lại lớp một cũng chưa viết chữ hoa. Đoạn cuối tập viết các đoạn văn mà viết vừa to vừa không viết hoa nên mình nhìn không quen, thấy rất buồn cười.
3
806293
2016-04-21 15:59:48
--------------------------
413949
6357
Cảm nhận đầu tiên khi tôi nhận được cuốn luyện chữ đẹp này
1) Bìa sách rất đẹp phù hợp với lứa tuổi Tiểu học nên khi tôi cho học sinh xem các bé đều rất thích.
2) Cách đóng gói của tiki làm tôi rất hài lòng. Bên ngoài là hộp các - tông nhỏ, bên trong là các miếng mút để giữ tập không bị cong, gấp bìa.
 Vì thế tôi quyết định mua số lượng lớn về cho các bé Thiếu nhi luyện chữ. Mong quý công ty nhập về nhiều bộ sách này hơn để tôi kịp trao thưởng cho các bé cuối năm.
5
1277048
2016-04-10 17:00:47
--------------------------
