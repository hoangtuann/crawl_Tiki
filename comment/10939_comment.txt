460103
10939
Mình quyết định đặt mua cuốn này trên tiki khi được đám bạn học thêm cùng trường giới thiệu. Và thật không uổng công tụi nó giới thiệu. Khi mình nhận được hàng và háo hức mở ra đọc ngay lập tức. Giọng văn hay hóm hỉnh làm mình cứ liên tưởng đến một bộ phin trên kênh disney channel. Những câu chuyện tuổi teen của con gái chúng ta được tác giả kể lại vô cùng hài hước và không kém phần lãng mạn. Sau khi đọc xong mình rất thích giọng văn này nên quyết đinh mua thêm một cuốn tặng cho em gái của mìnhn
5
393298
2016-06-26 20:00:30
--------------------------
371886
10939
Nếu được, mình chỉ muốn đánh giá 3 sao rưỡi thôi, vì tập này không lôi cuốn và duyên dáng bằng những tập trước. Ở các tập đầu, rắc rối sẽ được giải quyết bởi các nhân vật trong truyện. Còn tập này thì Jess thoát khỏi rắc rối hết lần này đến lần khác nhờ hoàn cảnh. Rắc rối của Jess không được giải quyết, nó tự mất đi. Một trong những lí do mình đọc bộ sách này là vì biết đâu, một ngày nào đó, mình sẽ rơi vào những tình huống tương tự và sẽ biết cách thoát nạn nhờ "quen biết" Jess Jordan. Ngoài ra, mình cũng không thích đọc một quyển truyện mà nữ chính là một cô nàng hoàn hảo được nhiều chàng theo đuổi, bao gồm cả chàng trai 'hot' nhất trường. Cho nên khi tác giả để nhan sắc Jess bị lu mờ bởi Flora, mình đã xác định đây là quyển sách dành cho mình. Thế mà trong tập này,  một đẹp thất bại của tập này, Ben Jones phải lòng Jess. Nếu ở những tập trước, mình có thể dễ dàng tưởng tượng bản thân là Jess Jordan để hoà vào câu chuyện, thì trong tập này, mình thất bại với điều đó.
4
411530
2016-01-20 16:50:27
--------------------------
366339
10939
Mình mua cuốn này vì rất thích tập trước nhưng tập này thật quá chán giống như cố kéo dài câu chuyện thôi vậy, tập trước thì hai bạn trẻ bên nhau vui vẻ nhưng đến tập này lại muốn chia tay chỉ để kéo dài truyện, những tình huống trong chuyện xảy ra cũng hơi bị vô duyên và không hài hước, không duyên dáng như trong phần trước như chi tiết Jess đi vệ sinh trong vườn, và cuối cùng mọi chuyện lại được giải quyết ổn thỏa nhưng có vẻ hơi khiên cưỡng.
Mình không thích phần này bằng phần trước.
3
306081
2016-01-09 19:13:46
--------------------------
258713
10939
Tôi thích series dành cho tuổi teen này của Sue Limb bởi lối viết khá hài hước và tự nhiên. Có nhiều loại sách kiểu vậy đọc rất khiên cưỡng, cứ làm quá rắc rối lên và tính cách nhân vật thì ẩm ương một cách quá lố. Loạt truyện "Con gái 15, 16" thì khác, cũng những rắc rối, những tình huống bất ngờ cùng bao nỗi lo toan vô cớ của tuổi mới lớn nhưng được thể hiện một cách nhẹ nhàng, dí dỏm. Ở tập "Bom nổ chậm" này Jess hoàn toàn bị "sock" khi cậu bạn trai Fred bất ngờ tuyên bố cậu không muốn công khai chuyện tình cảm ở trường. Lại thêm việc mẹ cô bé có bạn trai người Nhật. Từ đây đã nảy sinh mâu thuẫn, giận hờn cùng bao rắc rối oái oăm mà với phong cách viết của Sue Limb thì nó diễn ra thật đáng yêu và thú vị.
4
6502
2015-08-08 19:58:12
--------------------------
213568
10939
Mình đã mua một lúc 4 cuốn sách của series loạt truyện con gái do tác giả Sue Limb viết, quả thực đến tập cuối cùng mình vẫn không hề thấy thất vọng một chút nào hết. Có lẽ vì đây là tập cuối nên có nhiều tình tiết “gay cấn” hơn những tập khác, nào là bỗng dưng sau kì nghỉ hè yên vui hoành tráng, Fred lại bỗng nói không muốn hẹn hò nữa, hay là những tình huống éo le dở khóc dở cười khác. Mình thích nhất là đoạn rắc rối giữa Jess và thầy Powell hay cô Thorn cũng như vở kịch của nhóm Jess cuối năm. Giá như mà có thêm nhiều tập nữa, đọc sách xong mà cứ muốn đọc thêm mãi. Cảm ơn tác giả Sue Limb và các dịch giả đã mang đến một ấn phẩm tuyệt vời như thế này cho tuổi teen!
5
618015
2015-06-23 21:56:08
--------------------------
211091
10939
Quyển sách "Con gái 16 - như quả bom nổ chậm "xoanh quanh Jess , đây là phần cuối của tập truyện con gái cực vui của tác giả Sue Limb , tôi đã đọc hết tất cả các phần trước , câu chuyện rất hay, dí dỏm có tính nhân văn và kèm theo những bài học và kinh nghiệm dành cho tuổi mới lớn mà tác giả muốn truyền đến, phần cuối này có những tình huống rất bất ngờ nhưng lại được giải quyết một cách hợp lí . Vì thế tôi đã muốn cuốn sách này để đọc trong những lúc rãnh rỗi thay vì chơi game, nó rất hay và hợp với tâm lí các bạn tuổi mới lớn như tôi
4
662816
2015-06-20 08:37:52
--------------------------
210127
10939
Mình chẳng thấy truyện này hay tí nào, có thể là do tư duy của mình không phù hợp với motip truyện này. Truyện chỉ loanh quanh những rắc rối của cô bạn Jess rồi thật may mắn khi tất cả lại được giải quyết vào cùng một thời điểm. Có những chi tiết hết sức vô duyên khi cô bạn Jess đi vệ sinh ngay trong vườn và không mặc quần nhỏ mặc váy ngắn đi đến chỗ có phần "trang nghiêm" như phòng khám với bà. Mọi thứ đều rối tung lên và sau đó thì tự những rắc rối đó biến mất. Chi tiết duy nhất mình thích là khi cô bạn diễn kịch và đóng giả làm cô giáo khó tính của Jess. mình nghĩ đới với những bạn có tư duiy khá khắt khe thì không nên mua quyển sách này.
2
369458
2015-06-18 22:42:29
--------------------------
156205
10939
Ôi, mình đã đọc liên tục cả bộ truyện này và cảm thấy rất thích. Bộ truyện vết về cô bạn Jess "thông minh", về tình cảm gia đình, tình bạn, tình yêu xoanh quang Jess. Có những tình tiết rất hấp dẫn! Trang bìa bắt mắt, chất liệu giấy đẹp, các hình vẽ minh hoạ cho mỗi phần trong cuốn sách đơn giản nhưng lại cuốn hút đến lạ! Mình đã giới thiệu cho cô bạn, bạn của mình mê cực và đã đặt mua ngay một bộ mặc dù cô ý đã mượn của mình và đọc hết rồi ^^ Nói chung các bạn teen nên có trong giá sách bộ truyện "siêu hài" này.
5
261478
2015-02-03 18:00:56
--------------------------
147676
10939
Quyển sách được viết một cách hài hước, văn phong hợp với tuổi teen, nội dung cũng rất tươi mới và thú vị! Nhận vật chính Jess gần gũi như bao cô nàng khác, cũng có những lo âu về chuyện tình cảm của mình, chuyện tình cảm của mẹ, bà giáo hắc ám, chuyện trường lớp, rồi cả chuyện xem phim của bà ngoại. Thêm vào đó là những chuyện ngoài lề khi  Jess đến phòng y tế, hay bài văn tả gia đình bạn ấy, cực kỳ buồn cười luôn! Với mình thì cuốn sách có giá trị giải trí rất tốt, không phô trương hay đánh bóng, mà nội dung thì cực gần gũi với các bạn gái trong đô tuổi ẩm ương. Sau khi đọc sách đến lần thứ 2, thì mình quyết định sẽ mua luôn cả series làm quà cho em gái!
4
201502
2015-01-08 16:27:19
--------------------------
