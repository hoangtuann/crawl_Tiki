469711
5124
Khi cầm cuốn sách trên tay , mình đầu tiên không muốn đọc vì nhìn bìa hơi xấu , nhưng sau khi đọc xong mình mới biết rằng đây thực sự là một quyển sách tuyệt vời , chuyện kề về cô bé Hetty trong trại trẻ mồ côi luôn ao ước được gặp mẹ của mình , qua bao nhiêu thử thách cuối cũng cô cũng tìm thấy mẹ , nhưng mẹ cô không đẹp như cô nghĩ mà chỉ là một người phụ nữ bình thường , không nổi tiếng , nhưng đó chính là người yêu thương cô nhất , và thế là đủ
5
1475910
2016-07-06 18:25:15
--------------------------
396249
5124
Cuốn truyện không có hồi hộp, không có kịch tính, không có lo âu, nó đơn thuần như chính cô bé Hetty vậy. Một cô nhóc tinh nghịch, quậy phá và đối lúc là hỗn xược. Nhưng chính những điều đó đã tạo cho cô bé một điều rất quan trọng cho cuộc sống quá khoa khăn cho đứa trẻ này chính là sự mạnh mẽ. Ngỗ nghịch để che đi lo lắng sợ hãi bên trong mình, có đôi khi cô bé đám thương này làm tôi không kìm được nước mắt. Kết thúc tìm lại được mẹ của Hetty đã rất hạnh phúc rồi, chuyện sau này không quan trọng nữa. Một đứa trẻ sẽ vượt qua tất cả khó khăn nếu nó còn có mẹ. Nếu con bạn trong độ tuổi từ 10 đến 13 thì cuốn truyện rất phù hợp để tặng con.
4
622800
2016-03-13 08:47:56
--------------------------
368178
5124
Đây là cuốn sách mình đặt ở tiki cách đây không lâu. Đây là một cuốn sách rất hay và ý nghĩa . Cốt truyện phong phú , bìa sách đẹp , tiki bọc sách khiến cho mình có cảm giác quý mến quyển  sách hơn . Câu chuyện kể về cô bé Hetty Feather là một cô nhi. Sau những hoàn cảnh cảm động , hồi hộp và lôi cuốn ấy , cô bé đã trở thành một anh hùng thực thụ trong lòng người đọc. Đặc biệt , phần kết của chuyến hành trình ấy làm mình thực sự cảm động . Không phải là đứa trẻ nào cũng xinh xắn , dễ thương và biết ăn nói lễ phép nhưng đứa trẻ nào cũng cần mẹ của nó. Nói chung là cuốn sách đầy ý nghĩa cho dịp sinh nhật mình . Mình sẽ ủng hộ Nhã Nam cũng như Tiki dài lâu
5
866431
2016-01-13 14:48:07
--------------------------
340898
5124
Cảm ơn cô 'Lưu Chi'-người đã tặng con, cũng là người dịch quyển sách này và là người đã mang cho con những điều về cuộc sống mỗi khi con đến lớp. 
Bìa sách_ màu sắc nhạt nhoà,  đơn giản,  không ấn tượng, nói ngắn lại là_chưa đẹp_. "Tôi đã sai"  Khi được tặng tôi đoán nội dung sẽ chẳng có gì đặc sắc, bình thường. Tôi không đọc nó vì trong lòng không có cảm xúc gì và cất nó lên kệ. Tôi bắt đầu mở quyển sách này ra vào một ngày: trong đầu tôi cứ hiện lên hình ảnh cô gái trên bìa sách. 
Từng trang giấy trên tay tôi được mở ra. Bỗng trên khoé mắt có vị mặn. Tự lấy tay như một thìa đường xoẹt qua vị mặn kia. Cái cảm giác ấy đó là lần đầu tiên. Sang những lần đọc sách sau tôi phải tự biết cất mình vào một nơi nhỏ bé riêng tôi để tự nhũ lòng với mình. Tôi đã phải gập quyển sách lại rất nhiều lần bởi tôi bắt giữ hình ảnh của mình trong đó, không phải việc mình không có mẹ làm tôi khóc mà chính niềm ước mơ khi bé của tôi được nhìn lại làm tôi thấy mặn, đắng, cay, ngọt,...
Tôi không coi đây là một câu chuyện ý nghĩa mà là câu chuyện cho ta biết: ta được gì, điều ta nghĩ ta chưa có so với người khác và hơn hết đó là mẹ nói riêng và gia đình nói chung. Tôi rất yêu mẹ và tôi yêu người nhiều hơn nữa sau khi đọc sách. Chính vì quyển sách cho tôi thấy được thiếu mẹ sẽ như thế nào hay chính mẹ một người rất yêu ta, dõi nhìn ta trên đường đời. Lần nữa tôi muốn nói lời cảm ơn đến tác giả quyển sách, 'Lưu Chi' của con và công ty Nhã Nam đã cho thế hệ chúng ta một quyển sách tuyệt như vậy. Cũng tự xin lỗi và trách bản thân mình trước giờ luôn chú ý tới vẻ đẹp bìa sách. Cũng muốn gửi lời khuyên đến các bạn chưa có trên tay quyển sách rằng hãy rước một quyển về, bạn sẽ không hối hận đâu. Từ đầu đến cuối vẫn là #no.1
4
885633
2015-11-20 19:23:58
--------------------------
338515
5124
Đề tài về tình mẫu tử luôn khiến mình rất cảm động. Đọc cuốn truyện này mình đã khóc kha khá lần! Tình tiết truyện rất hay và lôi cuốn, nhiều đoạn thực sự gây xúc động mạnh. Những đứa trẻ viện cô nhi quả thật đáng thương, mong ước nhỏ nho về tình thương của mẹ cũng không thể có! Từ Hetty, Gideon đến Saul, Eliza đều khiến mình rơi nước mắt! Mình rất thích nhân vật Hetty, em tuy có phần ngang ngạnh nhưng là một đứa trẻ mạnh mẽ và rất giàu tình cảm, nhất là tình cảm của em dành cho những người thân tuy không phải ruột thịt của mình. Như lời tựa cuốn sách đã viết “không phải đứa trẻ nào cũng xinh xắn, dễ thương và biết ăn nói lễ phép, nhưng đứa trẻ nào cũng cần có mẹ.” Và một cô bé tốt như Hetty rất xứng đáng có được hạnh phúc, tác giả Jacqueline đã viết cho em một cái kết đẹp bên mẹ ruột của mình, cái kết tuy hơi đơn giản so với kỳ vọng của mình nhưng nhìn chung là một cái kết nhân văn cho cả câu chuyện.
5
135597
2015-11-16 12:51:16
--------------------------
338166
5124
Cảm động. Đó là cảm xúc đầu tiên bật lên trong tôi khi đọc những dòng đầu tiên của "Hetty đi tìm mẹ". Và cứ thế tôi không ngừng nghẹn ngào theo từng con chữ, theo chuỗi dài những năm tháng cuộc đời Hetty.
Hetty Feather là cô bé ương ngạnh, nóng nảy nhưng cũng thật thông minh, can đảm và giàu tình cảm. Hetty nuôi dưỡng trong mình những mộng ước từ thuở ấu thơ để có thể chống chọi và tồn tại trước sự khắc nghiệt của hiện thực, nhưng rồi khi ước mơ tan vỡ một cách phũ phàng, sự thật ập xuống đôi vai gầy nhỏ bé, thật khó để chấp nhận, cô bé hoàn toàn bất lực và bơ vơ. Khi đọc đến đoạn ấy dường như tôi cũng nghe trái tim mình vụn vỡ.
Câu chuyện buồn với bối cảnh u ám song tác phẩm vẫn toát lên sự ấm áp, ngọt ngào qua lời văn sâu lắng, giàu biểu cảm, giàu sức sống của Jacqueline Wilson. Lời văn mà đã gieo vào lòng người quá nhiều cảm xúc, và buộc tôi xót thương trước sự khổ cực, tổn thương, cô đơn, mất mát, chia ly của Hetty và những đứa trẻ bị bỏ rơi giống cô bé; cũng thật tự nhiên khiến tôi trân trọng tình yêu thương, sự bao dung và đồng cảm vẫn luôn hiện diện giữa cõi đời lạnh lùng, nghiệt ngã.
Tác phẩm đã tái hiện hiện thực đầy sống động, và cực kỳ xuất sắc.
Hơi tiếc là do không cầm lòng nổi tôi đã trót đọc đoạn cuối nên biết trước mẹ Hetty là ai. Nếu kiên nhẫn hẳn tôi sẽ nhận được điều bất ngờ. Lời khuyên cho các bạn độc giả khác là đừng bao giờ nên đọc trước diễn biến, sẽ làm mất đi ít nhiều sự hứng thú.
"Hetty đi tìm mẹ" hay theo như tên gốc là "Hetty Feather" là cuốn sách quý trong kho tàng văn học nhân loại,
5
386342
2015-11-15 15:09:19
--------------------------
322388
5124
Đầu tiên phải nói đến là bìa sách, nó được trang trí rất đẹp. Truyện rất cảm động. Xoay quanh về cô bé Hetty, tuy bị mẹ bỏ vào cô nhi viện nhưng cô vẫn không oán hận mẹ. Với ý chí và lòng yêu thương mẹ, cô đã trốn khỏi cô nhi viện để lên đường đi tìm mẹ đầy nhọc nhằn. Mình thật sự bị lôi cuốn vào truyện qua lời kể của tác giả. Kết thúc chuyện rất cảm động và ý nghĩa, Hetty đã tìm được mẹ. Người mà cô bé đã sống cùng trong cô nhi viện trong nhiều năm qua. Nói chung là sách rất hoàn hảo, xứng đáng 5 sao .  
5
548073
2015-10-16 10:00:36
--------------------------
317470
5124
Truyện về một cô bé mồ côi nhưng không bi thương như một số truyện khác. Xuyên suốt câu truyện là nhiệt huyết hừng hực của Hetty. Mình thực sự bị cuốn theo giọng kể của tác giả. Cô bé Hetty tuy bị mẹ bỏ vào Cô nhi viện, chịu những khổ sở nhưng không khi nào cô bé oán trách mẹ hay cuộc đời. Cô bé có một ý chí mãnh liệt, một niềm tin kiên định. Một điểm mình cũng rất thích ở truyện là truyện khá thực tế, chứ không phải hư ảo như một câu chuyện cổ tích. Theo mạch câu chuyện, mình đã thực sự tin anh Jem sẽ tìm Hetty và hai người sẽ sống hạnh phúc như câu chuyện của 2 anh em, hay mình cũng thực sự tin quý cô Andelline là mẹ của Hetty. Nhưng không phải như vậy, chị gái Martha hầu như đã quên Hetty, anh Jem cũng kể 1 câu chuyện y như vậy với Eliza và tóc của quý cô Andelline không hề màu đỏ.
Truyện thực sự đã đem đến cho mình nhiều cảm xúc.
5
198939
2015-10-03 15:20:03
--------------------------
308684
5124
Bià sách dầy dặn, thiết kế đẹp mắt, mực in rõ nét.Nói chung về thiết kế của sách thì khỏi bàn cãi. Nội dung thì miễn chê. Một cô bé Hetty với mái tóc đỏ, khuôn mặt cau có, ương ngạnh và nổi loạn, nhưng sâu bên trong cô bé lại là sự mềm yếu, dễ tổn thương. Cô bé mang trong mình tình yêu thương mẹ mãnh liệt, cô đã quyết định bỏ trốn khỏi cô nhi viện để bắt đầu hành trình đi tìm mẹ đầy nhọc nhằn. Kết thúc đầy bất ngờ và cảm động, cuối cùng cô bé cũng tìm được mẹ, người mà em đã sông cùng trong suốt thời gian ở cô nhi viện.
5
455025
2015-09-18 20:21:15
--------------------------
304639
5124
Một cô bé Hetty yếu mềm, dễ tổn thương mặc dù bề ngoài ương ngạnh, cau có và khó gần. Một cô bé yêu thương mẹ, khát khao mẹ và cần mẹ đã bỏ trốn khỏi cô nhi viện, tiến hành một cuộc hành trình nhọc nhằn đi tìm mẹ. Nhiều tình tiết hồi hộp, lôi cuốn, cảm động lấy được nước mắt của độc giả trong hành trình khát khao đi tìm hạnh phúc của Hetty. Một kết thúc quá đẹp và quá mãn nguyện. Quyển sách làm rung động lòng người. Rất cám ơn tiki đã chia sẻ.
5
110777
2015-09-16 15:46:32
--------------------------
300803
5124
Cũng như những câu chuyện về các bé mồ côi sống trong cô nhi viện nhưng với Hetty tìm mẹ thì có tiết tấu khác đi một chút. Đầu tiên về khả năng ghi nhớ các chuyện lúc nhỏ của Hetty khi còn là trẻ sơ sinh: kí ức về mẹ, chuyện với cô bảo mẫu tốt bụng của mình, với đứa trẻ cùng đưa đi với mình… Hetty cũng thật may mắn khi cô nhi viện gửi nuôi em bên ngoài trong gia đình nghèo đông đúc. Em không phải chịu những hạn chế trong khuôn khổ cô nhi viện. Tại đây em được gia đình nuôi dưỡng yêu thương che chở, được tận hưởng tình cảm gia đình ấm cúng, được thỏa sức vui chơi, tinh nghịch hồn nhiên. Và rồi khi đến tuổi Hetty phải quay trở về cô nhi viện đối mặt với không khí khác hẳn ở nhà, với nhiều luật lệ quy định và cả người quản lý hung dữ. Hetty vẫn không quên gia đình mẹ nuôi và luôn nhớ đến người mẹ ruột của mình. Dù sống trong khó khăn nhưng em không hề oán giận mẹ ruột đã bỏ rơi mình. Cuối cùng em đã tìm được mẹ ruột cũng chính là người đã yêu thương và sống cùng em trong cô nhi viện. Một câu chuyện hay, cảm động. Bìa và các hình minh họa trong sách đều đẹp. 
4
43129
2015-09-14 11:00:20
--------------------------
289469
5124
 Cuốn sách là câu chuyện về hành trình đi tìm mẹ của Hetty bé bỏng. Mình rất thích nhân vật Hetty, cô bé mồ côi đầy nghị lực. Những tháng ngày Hetty lớn lên ở nhà cha mẹ nuôi, tình cảm của em với ba mẹ, với các anh chị, với anh Jem, và với người em được đón về cùng ngày với Hetty, ta thấy được em là một cô bé giàu tình cảm, tuy có hơi tinh nghịch, và thích tưởng tượng. Mình rất xúc động với hình ảnh một cô bé mới 5 tuổi, phải rời gia đình thân yêu, để trở lại trại trẻ mồ côi, một nơi lạ lẵm, với bà người quản lý hung dữ, và rất nhiều qui định. Nhưng em không thôi nghĩ về gia đình, đặc biệt là ước mơ đi tìm mẹ, một quí cô trong rạp xiếc. Câu chuyện kết thúc một cách bất ngờ, Hetty cuối cùng cũng tìm được mẹ, người mà em đã sống cùng bấy lâu ở cô nhi viện. Một câu chuyện hay, cảm động.
5
476955
2015-09-04 15:08:10
--------------------------
250399
5124
Quả thật điều đầu tiên phải nói là mình phải khâm phục đến tinh thần và nghị lực của Hetty trước đã. Một cô bé Hetty lớn lên trong một trại mồ côi vậy mà lại mạnh mẽ và kiên cường đến vậy. Hetty luôn hướng về tương lai và hơn hết là không oán giận mẹ mình và đã đi tìm kiếm lại người mẹ thất lạc bấy lâu. Mạch truyện diễn ra liên tiếp nên người đọc sẽ không bị nhàm. Hơn nữa, bìa đẹp nè,hình minh họa vẽ cũng đẹp. Chất lượng dịch thì khỏi chê, mượt. Sách tuy cầm hơi nặng tay nhưng mà dễ bảo quản hơn giấy Hà Lan.
5
120193
2015-08-02 10:19:23
--------------------------
241150
5124
Hetty một đứa trẻ lúc nào cũng tỏ ra mạnh mẽ nhưng đó chỉ là cái mẽ ngoài của cô bé, thật sự cô bé rất yếu đuối và cũng như bao đứa trẻ cô nhi khác luôn ước ao có mẹ. Ngay từ khi cuốn sách này vừa được trưng bày trên tiki, tôi đã liền nhanh tay nhấn nút đặt hàng, thật sự cuốn sách đã không làm tôi thật vọng, đúng là một cuốn sách hay và đáng đọc. Hetty mặc dù sống trong cô nhi viện nhưng cô bé vẫn tìm được những người bạn tốt ở trong đó, có nhiều khúc rất cảm động nhưng tôi rất hài lòng với cái kết có hậu. Về phần chất lượng giấy thì rất tốt.
5
477877
2015-07-25 11:11:52
--------------------------
232305
5124
Thoạt đầu mình hơi bối rối với tính cách của Hetty, nhưng càng đi sâu vào câu chuyện của cuộc đời cô bé, mình càng thấy Hetty thực sự rất đáng yêu. Em nhớ rõ tất cả mọi thứ đầu tiên: ngày đầu cuộc đời, cảm giác đầu tiên với mẹ, với cô bảo mẫu dịu hiền, với đứa em trai không máu mủ, với quý cô cưỡi ngựa, với cô giáo và người bạn gái thân thiết. Ở Hetty có một ý chí và sức mạnh tinh thần tuyệt vời, không chịu dễ dàng bị nghịch cảnh đánh bại.
5
129889
2015-07-18 18:30:53
--------------------------
173514
5124
Tất cả trẻ em trên thế giới này đều cần có mẹ và ngay cả mình cũng thế, Hetty là một cô nhi điều đó không có nghĩa là cô bé không nhớ và yêu thương mẹ
Nhưng dĩ nhiên cô cũng có tính cách bướng bỉnh và cứng đầu. 
Ngày qua ngày, cô khao khát được thấy mẹ, có mẹ, được mẹ chăm sóc như biết bao đứa trẻ khác, dần dần, nỗi khao khát ấy lớn đến nỗi cô nảy ra một ý định điên rồ là rời khỏi cô nhi viện và đi tìm mẹ.
Vẫn là sách Nhã Nam, chất lượng in và giấy rất tốt, giấy ngả vàng thì mình cực thích. 
Câu chuyện giàu cảm xúc, lôi cuốn và hồi hộp đã làm nức lòng biết bao nhiêu người. Câu chuyện khiến mình cảm thấy yêu mẹ hơn bao giờ hết!
5
542401
2015-03-26 13:02:52
--------------------------
143770
5124
Mình đọc 1 lèo cả cuốn trong 1 ngày và bật khóc hạnh phúc khi vừa hết truyện. Tưởng tượng về 1 cô bé con kiên cường mà bay bổng được thể hiện qua văn phong tự nhiên và rất trẻ thơ khiến mình cảm thấy thật ấm áp. Truyện thú vị với nhiều tình huống diễn ra liên tục, cả vui lẫn buồn nhưng cuối cùng Hetty cũng đã tìm được mẹ của em và có quyền hi vọng về 1 viễn cảnh tươi sáng. Ko rập khuôn giáo điều, trẻ em ko phải là 1 cỗ máy trong tay người lớn, hãy để chúng tự do khám phá và bộc lộ bản thân mình - 1 lý do nữa để mình thấy đồng cảm với tác phẩm này. Hãy đọc và cảm nhận về 1 Hetty cho riêng mình!
5
474473
2014-12-24 13:14:28
--------------------------
