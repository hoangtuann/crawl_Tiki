422050
4091
Mình vốn rất thích chơi và khám phá mấy trò kiểu này, thực sự rất hay! Người Việt Nam theo mình hay tư duy theo lối mòn, ít sáng tạo, độc đáo, mà cuốn sách này giúp mình nhận ra khá nhiều điều hay ho khác, không chỉ theo cách thông thường mà là sáng tạo, rất tốt! Chỉ với những câu hỏi ngắn gọn mà mình thấy rất bổ ích, phù hợp với những bạn đang tìm kiếm những khám phá mới lạ. Khổ sách khá nhỏ, tương đối mỏng, dễ cầm tay, nói chung là ưng và mình sẽ mua thêm những quyển cùng bộ.

3
560592
2016-04-26 13:19:33
--------------------------
411099
4091
Cuốn sách dành cho các bạn thích thử thách, tư duy sáng. Sự thích thú khi bạn tự mình giải ra câu đố tư duy. Tạo khả năng tư duy tốt, phá cách hơn. Cuốn sách khơi dậy lại tư duy sáng tạo của bản thân sau thời gian dài " chậm tiêu " do sự lười biếng của bản thân không suy nghĩ. Giúp khả năng tư duy sáng tạo phá cách hơn trong công việc thiết kế của mình. Hay giúp ích cho bạn nhìu trong nghành suy nghĩ nhiều. Thư giãn cho các bạn thích thách thức giải đố. ^^
5
1272124
2016-04-05 15:25:00
--------------------------
410440
4091
Mình tình cờ biết được bộ sách tư duy đúng cách này trên tiki bán, nên đã mua gần trọn bộ để về giải câu đố và giải trí. Sách có nhiều kiểu dạng bài đố hay từ dễ đến khó. Trong bộ sách thì tôi thích cuốn tư duy phá cách và tư duy thị giác. Bằng việc giải những câu đố này mà thấy cách tư duy của mình cũng tốt hơn và tập trung vào một vấn đề hơn trước rất nhiều. Sách cũng tốt trong việc dạy cho con cái tư duy nếu như cha mẹ nào cũng giải với chúng.
5
854660
2016-04-04 09:52:46
--------------------------
249092
4091
Quyển sách này quả là một thách thức với những ai đam mê thử thách bản thân để vượt qua lối suy nghĩ thông thường đến với lối suy nghĩ mới mẻ độc đáo. Những cách tư duy mới lạ, dưới nhiều góc nhìn khác nhau được giới thiệu lần lượt qua các câu hỏi ở mức độ Dễ-Trung bình -Khó, và các dang đề bài khác (tìm ảnh đúng, ghép ảnh, phán đoán). Đúng như tên gọi của quyển sách, nó giúp tôi phát triển tư duy linh hoạt hơn, độc đáo hơn.Không những trong việc giải các câu đố trong sách mà còn những vấn đề khác trong cuộc sống.
4
640957
2015-07-31 12:48:35
--------------------------
195772
4091
Cuốn sách này cùng những cuốn sách khác trong bộ sách Tư duy đúng cách đã tạo nên một bộ sách khá hay và bổ ích để rèn luyện sự tư duy của bản thân một cách hệ thống, có hiệu quả. Những câu hỏi, bài tập trong đây sẽ giúp ta dần làm quen với các lối tư duy mới lạ, linh hoạt. Sau khi đọc và làm xong tất cả các câu hỏi trong sách, tôi đã có thể dễ dàng trả lời các câu hỏi IQ trong chương trình Đường lên đỉnh Olympia, có thể giải một bài toán khó một cách nhanh chóng hơn rất nhiều. Đây là một bộ sách hay mà bạn không nên bỏ qua!
3
102306
2015-05-14 16:35:40
--------------------------
