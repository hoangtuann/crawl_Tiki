330905
9465
Là người Việt không ai là không biết đến hai từ "gia cam". nhưng để hiểu rõ hơn, tỉ mỉ chi tiết hơn thì chưa hẳn ai cũng biết.
Cuốn sách là cái nhìn tổng quát về cái gọi là "da cam". Không chỉ là một phía mà còn là cả thế giới đang nhìn nhận vấn đề này. Nó đưa đến cho người đọc những mất mát và đau thương mà da cam để lại. những hệ lụy của da cam cho tương lai.
Đọc xong cuốn sách tôi thấy vấn đề gia cam không chỉ là vấn đề mà riêng Việt Nam gặp phải mà đó là vấn đề của cả thế giới.
4
853546
2015-11-03 16:20:06
--------------------------
316806
9465
Khi tôi học cấp 2, có một cậu học sinh trong trường có khuôn mặt vàng vọt , tóc tai lỏm chỏm, rất khác thường, chỉ cần nhìn qua là thấy ngay có liên quan đến vấn đề di truyền, sau đó tôi mới phong phanh được, đó là một chứng nhân sống cho tội ác của những kẻ đã xâm lược nước ta trước đây... . Khi đọc cuốn sách, tôi càng thấm thía đau đớn hơn, làm sao có thể chịu đựng được, cái chúng đối phó với người dân Việt Nam là sử dụng thứ độc hủy hoại các thế hệ con người, làm sao có tội ác nào lớn hơn thế nữa!? Quyển sách này có sức thuyết phục cao về cách giải thích những vấn đề liên quan đến chất độc da cam mà lứa trẻ thời nay không được nhắc đến nhiều, nhưng nếu có những hình ảnh và số liệu kỹ hơn, càng tăng thêm giá trị cho tác phẩm! Chân thành cám ơn người viết nên cuốn sách này! 
5
516261
2015-10-01 20:04:52
--------------------------
281073
9465
Chiến tranh đã lùi xa mấy chục năm, nhưng nhữn gì nó còn xót lại thì vẫn chưa thể mờ hết. Một người Mỹ viết về chiến tranh Việt Nam thì quả thực không gì khách quan hơn.Con đường da cam như một bản tố cáo về tội ác của Mỹ trong chiến tranh Việt Nam, chỉ để đạt được mục đích của mình mà quân đội Mỹ đã dùng thủ đoạn tàn độc nhất đối với con người Việt Nam ta. Những chiến sĩ, những đứa trẻ, người thân của họ là những người gánh chịu trực tiếp hậu quả Da cam này. Dù đã có những sự bù đắp nhưng chưa bao giờ là đủ cho những số phận không may mắn bị nhiễm chất đốc đó. Họ không may mắn vì họ chọn con đường hi sinh cho tổ quốc, hi sinh cho độc lập của đất nước...Một quyển sách nên đọc để  hiểu rõ và chia sẻ hơn với những anh hùng da cam.
5
309333
2015-08-28 08:39:30
--------------------------
274927
9465
Đọc "Alain bàn về hạnh phúc" ưng quá thế là lên Tiki rinh gần hết bộ sách nằm trong Tủ sách cánh cửa mở rộng về. Trời đất hỡi, phải nói là quyển nào cũng đáng đọc luôn í. Quyển này cũng không phải là ngoại lệ đâu. Vì tác giả là nhà nghiên cứu lại còn là người nước ngoài nữa nên có thể cung cấp cho người đọc một cái nhìn khách quan nhất về chiến tranh da cam của Mỹ đối với Việt Nam, nhưng như vậy không có nghĩa là bênh vực hay thương hại bên nào, tác giả nói lên quan điểm của chính tác giả, không hướng người đọc vào kết luận nào tiêu cực hết nên rất đáng để đọc. Ngoài ra với sự hiểu biết uyên thâm cùng vốn từ phong phú của tác giả, người đọc sẽ không cảm thấy khô khan khi đọc những tác phẩm mang tính nghiên cứu và chính sự thế này
4
464538
2015-08-22 13:47:56
--------------------------
177330
9465
Cuốn sách đưa ra một góc nhìn mới mẻ về cuộc chiến. Không phải từ góc nhìn của hai bên tham chiến, mà từ góc nhìn của người làm khoa học. Những tranh cãi giữa các nhà khoa học và cuộc đấu tranh của họ trong nỗ lực bảo vệ môi trường. Sự sa lầy của Mỹ trong chiến dịch Ranch Hand, thảm kịch môi trường ở Việt Nam, và đau xót hơn là di chứng ở các thế hệ tương lai là bài học đắt giá và là một tiền lệ để thế giới ngăn chặn một thảm họa tương tự trong tương lai.

Nhiều khía cạnh được nêu ra trong sách của cuộc chiến khá mới mẻ với người Việt Nam (liên quan đến các quyết định về chiến lược của Nhà Trắng). Điểm trừ của cuốn sách có lẽ là ở khâu dịch thuật. Các chiến lược chiến tranh của Mỹ ở Việt Nam ít nhiều đã quen thuộc với người Việt, khi dịch tên các chiến lược, trận đánh nên bám sát với cách gọi tên trong Lịch Sử (đã quá quen thuộc), hoặc có chú thích ở bên dưới để dễ theo dõi.
3
104850
2015-04-02 22:36:17
--------------------------
149193
9465
Quyển sách Con Đường Da Cam mang nhiều màu sắc của một cuộc chiến tranh với nhiều mất mát, đau thương và cả sự hừng hực khí thế đấu tranh của nhân dân ta.
Quyển sách là bằng chứng xác thực nhất mà tội ác của Mỹ gây ra cho Việt Nam chúng ta.
Qua quyển sách mình có nhiều góc nhìn hơn về cuộc chiến tranh đầy vẻ vang ở Việt Nam, về chính quyền của Mỹ với quyết định có nên dùng chất hóa học này trên Việt Nam hay không?  Nhưng cuối cùng cái quyết định tàn nhẫn ấy vẫn xảy ra với Việt Nam!
Quyển sách cung cấp cho mình nhiều tư liệu về chất hóa học màu da cam, về những đau thương, tang tốc mà nó gây ra trên mảnh đất Việt Nam!
Một quyển sách hay và rất đáng để đọc!
5
303773
2015-01-12 21:27:14
--------------------------
132166
9465
Chiến tranh Mỹ tiến hành ở Việt Nam trong những năm 54-75 có sử dụng chất hủy diệt sinh thái, mà hậu quả của nó cho đến bây giờ vẫn vô cùng đau thương. Ngoài cuốn sách này, mình ít tìm thấy tài liệu nào nói về chất độc màu da cam. Cuốn sách cho mình biết thêm nhiều thứ, ví như trong nội các Mỹ thời kỳ chiến tranh, có nhiều luồn ý kiến khác nhau về việc có hay không nên sử dụng chất hủy diệt sinh thái? Chúng sử dụng ntn? Hậu quả như thế nào? Nhưng theo mình, cuốn sách chưa đưa ra được những số liệu và hình ảnh xác đáng về hậu quả mà chất độc da cam để lại cho con người Việt Nam. Mình đã đọc sách, nhưng thấy chưa thỏa mãn về điều này. Nếu tác giả viết thêm được như thế, mình sẽ thấy thuyết phục hơn.
3
408079
2014-10-30 07:58:43
--------------------------
78287
9465
Những nỗi đau mà chất độc màu da cam mang lại cho biết bao thế hệ không may mắn ngày hôm nay ở Việt Nam vẫn luôn là vấn đề nóng bỏng, có tính thời sự nhất. Mấy ai hiểu được nỗi đau của những người đã và đang đối diện từng ngày cùng cái chất độc tai quái ấy mà chiến tranh mang lại chứ? Cuốn sách sẽ một phần giúp mỗi người hiểu thêm về nó, giúp mỗi người nhìn nhận đúng hơn về vấn đề chất độc màu da cam mà quân đội Mỹ đã rải lên đất nước chúng ta. Qua đó, bày tỏ sự tố cáo, phẩn nộ đối với những gì mà chất độc này mang lại. Đồng thời bộc lộ sự cảm thông sâu sắc đối với những mảnh đời không nay mắn, động viên để họ có niềm tin trong cuộc sống. 
Con đường da cam - con đường của tội ác, thật đúng và có ý nghĩa to lớn!
5
72874
2013-05-31 21:14:02
--------------------------
69929
9465
Điều mình ngạc nhiên nhất về quyển sách này không phải là tựa sách, tiêu đề hay hình ảnh con đường màu cam với cây lá rụng đầy bắt mắt kia...mà đó chính là tác giả. Một người ngoại quốc.
Chiến tranh đã qua đi trên mảnh đất Việt Nam hơn 40 năm nhưng nhưng dư vị của nó vẫn còn hiện diện trong tâm trí bao thế hệ trẻ. Chất độc màu da cam cũng vậy. Nó là minh chứng cho sự tàn bạo của quân Mỹ tại các chiến trường miền Nam. Nhưng tác giả David Zierler đã viết nên cuốn sách với một trái tim rất chân thật. Quyển sách này không nhằm mục đích giải trí nhẹ nhàng vì ẩn chưa bên trong đó là bao nỗi xúc cảm sâu sắc đến người đọc. Mình nghĩ rằng đây giống như một sự đồng cảm và chia sẻ của các thế hệ người Mỹ sau này. Vì dù gì, chiến tranh không còn nữa, chúng ta cần hướng đến tương lai, đó là hòa bình, là khát vọng khẳng định mình. Điều đó tốt hơn việc "ném đá" hay thiêu tôn trọng lẫn nhau giữa các nước hàng trăm lần.
Đây thật sự là một quyển sách đáng đọc !
4
112629
2013-04-18 14:05:59
--------------------------
67291
9465
Đã qua hơn 40 năm kể từ ngày thống nhất đất nước, nhưng những hệ lụy, hậu quả mà chiến tranh mang lại chưa một ngày ngừng lại. Trong đó, chất độc da cam là con đường đây đau khổ, âm thầm lặng lẽ nhưng đầy tàn khốc và kéo dài đến tận ngày hôm nay và cả những năm sau này nữa. Đọc Con đường da cam để tôi hiểu rõ hơn về hành trình hủy diệt của nó đối với dân tộc Việt Nam ta đã diễn ra như thế nào. Những con người vô cảm, độc ác, độc tài đó, những kẻ gieo rắt cái chết dần chết mòn ấy chẳng phải trả giá bao nhiêu trong khi những người dân vô tội, những con người yêu nước lại phải gánh chịu những sự đau đớn, những số phận nghiệt ngã, những sự day dứt lương tâm dù mình không phải là người gây ra cái tội ác ấy. Đọc Con đường da cam để tôi biết được trước khi làm điều gì thì hãy suy nghĩ đến những hậu quả mà những người khác phải gánh chịu cho việc làm của mình.
5
63376
2013-04-06 09:54:21
--------------------------
65499
9465
Không thể gọi là hay! Vì đây hoàn toàn không phải là tác phẩm nghệ thuật, hoàn toàn không mang hơi hướng của sự hư cấu hấp dẫn người đọc. Làm nên gía trị lớn nhất của tác phẩm, đó là tính chân thực từ những câu chuyện có thật, về tội ác, về những cảnh đời phải chịu hậu quả tàn khốc của chiến tranh, về di chứng quái ác của chất độc màu da cam để lại từ mấy chục năm trước.
Mình đã đặt mua nhiều cuốn Con đường da cam và gửi tặng bạn bè, không chỉ đơn thuần là gửi tặng một cuốn sách, mà còn là lời nhắn nhủ cùng sẻ chia với những số phận, những con người phải gánh chịu hậu quả chiến tranh, là gửi gắm một thông điệp hãy nhìn thẳng vào sự thật của lịch sử!
5
24486
2013-03-26 09:48:16
--------------------------
52693
9465
Những hậu quả mà chiến tranh để lại thực sự rất nặng nề và đau thương. Đặc biệt vấn đề về chất độc da cam luôn làm nhức nhối bao thế hệ người dân Việt Nam. Nó không chỉ để lại những thương tổn thể xác mà còn khắc sâu vào tâm hồn những vết thương khó lành lại được. Cuốn sách "Con đường da cam" này phần nào sẽ khắc họa lại giai đoạn lịch sử khủng khiếp đó, khi quân Mỹ dùng đi-ô-xin để tàn sát đất nước ta. Từng trang sách đều được viết nên một cách chân thực và giàu cảm xúc, đều diễn tả được đúng nhất những gì đã xảy ra, đồng thời bật mí những sự thật có thể làm nhiều người giật mình. Với lối viết sâu sắc và tinh tế, tác giả không chỉ kể một câu chuyện lịch sử mà còn nói lên một bài học về đạo đức con người.
4
20073
2012-12-28 10:27:51
--------------------------
