495982
3910
Mình đã mua sách trên Tiki rất nhiều lần, và lần nào cũng rất rất hài lòng. Tiki đóng gói rất cẩn thận. Sách giảm giá nhưng mới cứng. Quyển sách này mình thật sự rất bất ngờ, ngoài sức tưởng tượng của mình. Sách rất đẹp về hình thức, nội dung cũng rất hay, vừa nhận được hàng là mình đã tranh thủ đọc ngay. Rất vui.
4
1229298
2016-12-15 12:13:42
--------------------------
485814
3910
Tiki luôn giao hàng nhanh và đảm bảo. Hàng được đóng hộp đẹp đẽ. Được khuyến mãi Bookcare nên nhìn bìa sách đẹp hơn hẳn. Thích mua sách ở Tiki nhất ở điểm có thể tìm kiếm đầu mục sách dễ dàng và có thể còn đọc thử được.Truyện thì mình không thích lắm. Vì mình nghĩ đây là truyện thiếu nhi, nên chắc sẽ có nhiều tranh ảnh, tạo niềm hứng thú cho các bé. Nhưng truyện lại quá nhiều chữ và ít tranh ảnh, nên sẽ phù hợp với các bé lớn hẳn, tập học cấp I, cấp II. 
3
1104702
2016-10-06 13:43:11
--------------------------
461817
3910
Truyện này của chị Nguyệt Nguyệt phải nói là quá hay và hấp dẫn. Lúc nhận được sách này mình cũng bị bất ngờ vì không ngờ khổ sách lại to như vậy như cầm trên tay một cuốn truyện cổ tích vậy. Bìa sách thì siêu cấp dễ thương, các tranh minh họa được vẽ bằng tay nên trông cũng rất thích. Truyện này chắc không phải chỉ dành riêng cho thiếu nhi đâu nhỉ vì em là người lớn đọc còn thấy truyện hay nữa thì chắc mấy em thiếu nhi sẽ thích lắm đây. Cảm ơn chị vì một câu chuyện hay.
4
1246695
2016-06-28 01:20:56
--------------------------
365664
3910
Truyện là người vẽ nên hình ảnh thân thuộc với trẻ em , làm con người thấy sức hút rất lớn từ giá trị được chọn lọc , làm cho ai cũng bỏ qua chúng khi chưa hiểu hết lý do nó xuất hiện mà ở chính mọi người suy diễn ra còn câu truyện lại kể chi tiết hơn về sự liên kết giữa thiếu nhi với các bạn bè , hồn nhiên với sắc màu tươi sáng , rực rỡ , hợp với các con người bận rộn , muốn tạo ra môi trường học tập có nhiều sinh động .
5
402468
2016-01-08 14:47:15
--------------------------
264861
3910
Từ cách dẫn chuyện, lời thoại của các nhân vật cho đến các bức tranh minh họa đều rất dễ thương. Tôi thực sự rất thích cách tác giả miêu tả về thế giới thần tiên kỳ diệu ở xứ sở Pha Lê, nơi có rừng Kim Cương hay đầm Chết... Thế giới ấy hiện lên thật sống động qua ngòi bút linh hoạt của tác giả. Tôi cảm thấy rất thú vị khi đọc tới đoạn bộ tứ các bạn nhỏ đi vào Hang Nước Đen để lấy lại viên ngọc của vương quốc Thủy Tinh đã bị mụ Nữ Vương Chuồn Chuồn đánh cắp và phù phép rồi cảnh Hoàng Tử Cáy đấu với con quái vật canh giữ cửa hang. Truyện không chỉ dành cho độc giả nhỏ tuổi mà còn rất thích hợp cho cả người lớn nữa. Cuốn sách rất hay. Chúc mừng Nguyệt Nguyệt.
5
27232
2015-08-13 15:21:25
--------------------------
223649
3910
Mình mới đọc xong truyện này trưa nay, lần đầu tiên đọc truyện của tác giả nên đánh giá khá khách quan, không thể so sánh cuốn sách này với các cuốn sách trước được.
Sách sử dụng ngôn từ khá phù hợp, có chọn lọc, thích hợp với con trẻ.
Tên nhân vật ngộ nghĩnh, dễ thương, dạy cho con trẻ những bài học đắt giá về tình yêu thương gia đình, sự đoàn kết giúp đỡ giữa bạn bè khi gặp khó khăn, lòng hiếu thảo đối với đấng sinh thành.
Một cuốn sách khá thú vị.
4
566059
2015-07-07 13:40:27
--------------------------
207085
3910
Đúng như lời tác giả, "Chuyến phiêu lưu đến bên kia xứ sở thác Mây Mù" là cuốn truyện không chỉ giành cho thiếu nhi, mà người lớn chúng ta có thể cùng bé Tròn, Tũn, Phệ, Bờm phiêu lưu tìm đến "thế giới tươi đẹp" để giảm bớt những mệt mỏi, buồn phiền trong cuộc sống hàng ngày. 
Mặc dù là truyện giành cho thiếu nhi, nhưng xuyên suốt truyện chính là những bài học đắt giá như lòng hiếu thảo, tình cảm yêu thương, đoàn kết giúp đỡ bạn bè, sự lạc quan, dũng cảm...
 Với ngôn ngữ phong phú, câu văn được trau chuốt đến hoàn hảo :))) nội dung mới lạ, hấp dẫn, trang sách được minh họa bắt mắt tôi tin cuốn truyện sẽ làm hài lòng các bạn.
Cảm ơn Nguyệt Nguyệt và chúc bà sẽ luôn thành công trong sự nghiệp viết lách của mình. Yêu bà :*
5
76936
2015-06-11 13:47:22
--------------------------
200408
3910
Khi cầm cuốn sách này và đọc, tôi cảm thấy như không còn tồn tại ở thế giới này.Câu văn lôi cuốn, ngôn từ sinh động, cốt truyện thú vị của Nguyệt Nguyệt đã đưa tôi lạc vào thế giới thu nhỏ trong cuốn sách kia, cùng tham gia vào cuộc phiêu lưu của những người bạn nhỏ, cùng trải qua bao nhiêu chuyện thú vị.Ở cuốn sách này, bạn có thể thấy được cuộc sống hiện hữu với những gam màu đặc sắc.Vui buồn, hạnh phúc, khổ đau, năng động, nhiệt huyết và tinh thần quyết thắng của những người bạn nhỏ.Cùng với đó, bạn nhận được những bài học sâu sắc về tình yêu thương, sự ích kỷ, lòng tham lam của con người với con người.Cuốn sách là một điểm mới nổi bật trong kho sách truyện Việt Nam.Nguyệt Nguyệt đã rất thành công khi mượn hình ảnh trẻ thơ để thể hiện một câu truyện cảm động, sâu sắc trong lòng người đọc.Đó chính là yếu tố để cuốn sách thu hút người đọc ở mọi lứa tuổi.Cảm ơn Nguyệt Nguyệt đã mang lại cho chúng ta một câu chuyện chân thành và sây sắc hệt như cô gái nhỏ viết ra  cuốn sách này vậy.Còn bạn, bạn đã bị " quyến rũ " chưa?
5
473556
2015-05-24 21:49:20
--------------------------
197972
3910
Tôi được biết đến tác giả ở tác phẩm Em vẫn chờ anh, khác với tiểu thuyết đầu tay, đây là cuốn truyện viết cho thiếu nhi, điều mà ít và hầu như không có tác giả trẻ nào chọn. Có lẽ đây cũng chính là 1 điểm mạnh của cuốn sách khi mà đa phần các tác giả bây giờ viết về tình yêu. Lấn sang một phong cách mới nhưng dường như tác giả lại càng phát triển được khả năng văn chương của mình trong việc sử dụng ngôn từ phong phú rất phù hợp cho thiếu nhi. Trẻ con, trong sáng, ngây ngô mà cũng rất thật, rất chân thành. Đến với chuyến phiêu lưu cùng các bạn, tôi cảm thấy tự hào vì mọi thứ đậm chất việt nam.  Tên nhân vật, bối cảnh, câu chuyện đều gần gũi, thân quen, giống như mọi thứ tôi vẫn thấy hàng ngày. 
Cốt truyện đơn giản nhưng mang đầy tính nhân văn và giáo dục. Chuyến phiêu lưu đến xử sở bên kia thác Mây Mù thực sự là một làn gió mới, khiến người đọc dễ chịu, sảng khoái, nhưng cũng không kém phần lôi cuốn và đọng lại những bài học sâu sắc về tình cảm bạn bè, gia đình, lòng dũng cảm cũng như đề cao các giá trị khác của con người.
5
54418
2015-05-18 21:06:20
--------------------------
197377
3910
Văn phong của chị Nguyệt rất hay, mình cảm thấy mỗi một từ, một câu chị đã phải suy nghĩ rất kĩ mới chọn được những từ ngữ rất phù hợp, mượt mà như vậy. Nội dung truyện cũng thế, nội dung mới mẻ nhưng gần gũi, nó không chỉ dành cho thiếu nhi mà còn dành cho mọi lứa tuổi, đưa mỗi chúng ta trở về thế giới tuổi thơ - khi chúng ta vẫn còn nhỏ, được ông bà hay ba mẹ kể cho những câu chuyện cổ tích với những phép tiên nhiệm màu. Các bạn có thể vừa đọc, vừa ngẫm nghĩ rồi lại bật cười vì sự đáng yêu, hài hước của Tròn , Phệ, Tũn, cũng có thể trầm mặc nhớ lại tuổi thơ của mình. Chúc mọi người đọc sách vui vẻ. Chúc chị Nguyệt luôn vui vẻ và thành công trong sự nghiệp văn chương của mình!
5
174831
2015-05-17 14:44:34
--------------------------
190393
3910
"Chuyến Phiêu Lưu Đến Xứ Sở Bên Kia Thác Mây Mù" là cuốn sách thiếu nhi viết cho tình bạn và tình cảm gia đình, được hoàn thành bởi trí tưởng tượng phong phú cùng ngòi bút sắc nét của tác giả Nguyệt Nguyệt. Cốt truyện mới mẻ, tình tiết có cao trào, lại gần gũi với trẻ em Việt Nam và cả người lớn nữa. Những khung cảnh kỳ ảo được tác giả miêu tả rõ nét đến mức người đọc có thể tưởng tượng ra được một cách hoàn hảo. Văn của Nguyệt Nguyệt rất đẹp, linh hoạt, vốn từ phong phú và sinh động.
5
621808
2015-04-29 14:25:12
--------------------------
189620
3910
Văn phong tác giả khá cầu kì, hoa mỹ. Có những đoạn sử dụng câu cú phức tạp, không dễ đọc mà phải đọc từ từ mới thấm. Nhưng chính vì thế lại có gì đó khác lạ, tạo nên một phong cách riêng. Điểm mạnh của tác giả là có ngôn từ phong phú, nhiều màu sắc. Cốt truyện có nhiều tình tiết, hợp với những người thích phiêu lưu và tưởng tượng. Càng về sau càng kịch tính hơn. Các chi tiết hài hước cũng thú vị. Nhìn chung mình không rõ trẻ em đọc thấy thích hay không nhưng lứa tuổi thanh thiếu niên đọc được, cũng có thể coi là một làn gió mới giữa nhan nhản các thể loại quen thuộc hiện nay.
5
54232
2015-04-27 18:56:12
--------------------------
189056
3910
Văn của Nguyệt Nguyệt rất linh hoạt, và được bổ trợ thêm bằng trí tưởng tượng phong phú, nhưng lại không xa rời thực tế. Hình ảnh trong sách đủ gợi mở lên những khung cảnh thần kỳ, nhưng vẫn gần gũi với trẻ em Việt Nam. Trẻ nhỏ đọc sẽ cảm thấy thế giới thật diệu kỳ, người lớn đọc lại như quay trở lại tuổi thơ. Lối hành văn của Nguyệt không bó hẹp, mà mở rộng và hài hòa, lúc cao trào, lúc lại tinh nghịch. 
Đây là một cuốn truyện thiếu nhi đáng để người lớn tìm đọc, và mua tặng con em của mình.
5
381189
2015-04-26 14:07:05
--------------------------
187897
3910
Hồi nhỏ mình rất thích đọc truyện thiếu nhi, từ thiếu nhi thế giới đến cổ tích Việt Nam. Cho đến tận bây giờ, truyện cổ tích cho thiếu nhi vẫn là cuốn sách mà mình muốn mua nhất. Giờ thấy cái tên truyện mình đã muốn mua ngay. 
Nhân vật trong truyện có những cái tên ngộ nghĩnh  như : Tròn , Phệ, Tũn, đều là những cái tên quen thuộc hồi nhỏ của trẻ em Việt Nam, chuyến phiêu lưu trong câu chuyện chắc chắn rất hấp dẫn lôi kéo khiến mình háo hức chào đón cuốn sách của tác giả.
Hơn nữa, mình cũng rất khâm phục tinh thần của tác giả. Chúc tác giả và chuyến phiêu lưu đến xứ sở sương mù thành công
5
51712
2015-04-24 11:26:43
--------------------------
187883
3910
Bình thường người ta chỉ nói là "văn hay" thôi. Nhưng đọc truyện của chị Nguyệt thì phải thốt lên "văn rất đẹp". Trí tưởng tượng của chị vô cùng phong phú, câu văn mượt mà, gợi mở và cứ nổ rộ ra như pháo hoa vậy. Truyện thiếu nhi nhưng không chỉ thiếu nhi muốn đọc.  Cốt truyện được đầu tư rất kĩ, càng đọc càng thấy gay cấn, li kì, có cao trào và có những đoạn hài hước. Đây là thu hoạch đáng giá nhất của tôi trong hội sách lần vừa rồi. Mặc dù không thích bìa lắm, nhưng đọc xong là tôi đặt lên phần kệ những cuốn sách tôi ưng nhất liền.
5
473388
2015-04-24 11:13:53
--------------------------
