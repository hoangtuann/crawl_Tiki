296535
11261
Mình rất thích đọc cuốn truyện tranh này(mình cũng thích đọc doremon và conan nữa)!Mình thấy truyện không được hay như doremon nhưng công nhận cũng chẳng thua kém gì những câu chuyện nổi tiếng của Nhật Bản,ban đầu mình mua là vì mình thấy rất nhiều đứa con nít đọc truyện này nên mình mua cho em mình!Thế là buồn buồn lấy đọc thử ai ngờ vui phết,em mình nó rất thích,nó đọc đi đọc lại hoài à!Công nhận chuyện hài thật, một câu chuyện thiếu nhi mà đảm bảo ai cũng thích!!Mình mong sẽ có nhiều tập truyện hay hơn nữa!!
5
413637
2015-09-11 09:45:36
--------------------------
205696
11261
Cuốn truyện này hôm nay tôi đọc trộm của thằng bạn thân hàng xóm bên cạnh,phải nói tôi đã cố gắng hết sức để nhịn cười chứ nếu không họ mà biết thì lại cho rằng tôi có vấn đề đầu óc.Tôi đã đọc rất nhiều các truyện hài hước khác nhưng có lẽ bộ truyện này vẫn là hay nhất.Tác giả quả là xuất sắc khi tạo ra một nhân vật vừa ngây thơ hồn nhiên lại vừa nguy hiểm ranh ma như vậy.Còn có những đồ vật có chức năng kì quái trong truyện cũng làm người đọc cảm thấy vui vẻ.
Phần mở đầu truyện là đi đến thế giới song song không được hay cho lắm vì nội dung khá đơn giản không có nhiều kịch tính như các truyện khác.Các mẩu truyện về sau thì khá hơn khi nói về cuộc sống thường ngày đơn giản mà vui nhộn,có nhiều chi tiết khiến người đọc cười nhiều hơn.
Mong rằng tập tiếp theo của truyện sẽ hay hơn nữa.
5
650298
2015-06-07 12:29:17
--------------------------
