545113
5123
Vì con mình mê quyển Chuyện Con Gà của tác giả Thụ Nho nên mình mua quyển này vì cùng tác giả. 
Tuy nhiên mình thì thấy nội dung không bằng quyển kia. Ấy thế mà anh cu vẫn thích lắm :). Sở thích của trẻ con chưa bao giờ giống mình  
3
65595
2017-03-16 18:58:48
--------------------------
405092
5123
Mình mua cuốn này vì thấy cùng tác giả (Thụ Nho viết lời) với cuốn Chuyện con gà nhưng cuốn này chỉ được 3/5 điểm so với cuốn kia. Vì là sách dạy học đếm nên nội dung rất đơn giản, chỉ là 1 con này, 2 con kia, 3 con nọ...Hình vẽ rất được chăm chút nhưng lại theo kiểu chibi quá nên đến người lớn xem còn không đoán được đấy là con gì nếu không có phần chữ. Điểm cộng là sách thiết kế đẹp, giấy dày, tốt; bìa cũng rất cứng, không dễ bị rách trong lúc bé xem.
3
473814
2016-03-26 05:23:50
--------------------------
404554
5123
Sách dày dặn đẹp. Tuy nhiên, ý tưởng của tác giả có vẻ muốn tạo vui nhộn, ngộ nghĩnh cho bé nhưng bé thì lại thắc mắc " sao con nào cũng mập ú nụ cái bụng vậy mẹ?" "Mẹ ơi, hổng giống nai Bambi. Hổng giống nai ở sở thú" thế đấy, mua sách cho chàng khó lắm á.
 
Từ ngữ của tác giả sử dụng ở đây bé hổng thích lắm "mẹ ơi, sao lại là CON CHẤY?, CON CHÓ CÚN- chó tên CÚN hả mẹ? Sao mấy con khác không có tên?". "Con CẦY là thịt cầy ở quán hả mẹ?" Mới có hơn 2 tuổi mà bé chị đọc cho nghe mà không hài lòng rồi la ỏm tỏi vậy đó.

Quyển sách đơn giản chỉ muốn truyền tải để bé tập đếm nhưng xem ra hơi bị khó với mấy em lý sự rồi.
3
575359
2016-03-25 12:37:35
--------------------------
284944
5123
Truyện bìa cứng chắc chắn, thích hợp bỏ vali du lịch hay bé năng động thích tự chủ cầm sách đi lăng quăng. Giở ra từ màu sắc đến hình ảnh đều chỉ có thể nói là dễ thương hết sức. Từ mỗi trang có thể mô tả cho bé các hình ảnh, giúp bé gọi tên, tập đếm. Có nhiều con vật nó hơi xa lạ với thể loại cuộc sống, như con rệp chẳng hạn, nếu lồng vào các con vật gần gũi hơn nữa thì sẽ không chỉ là sách tập đếm mà thôi, nhưng nói chung là khá hài lòng. 
5
666583
2015-08-31 14:07:36
--------------------------
247357
5123
Mình đợi những sản phẩm đơn giản mà chất lượng như này đã lâu rồi, con mình 20 tháng, bắt đầu thích đếm các số, lúc cầm bàn tay bé xiu xíu của con, chỉ vào từng con một rồi đếm cũng mẹ rất đáng yêu (tuy chỉ đếm đến số 10)
Tuy nhiên một số con vật nhìn hơi khó hiểu với con vì vẽ theo phong cách hoạt hình, con chưa biết đấy là con gì, cũng hơi khó liên hệ đến thực tế, nhưng tương lai không xa con sẽ hiểu và có lẽ trí tưởng tượng của con cũng sẽ dần phong phú hơn.
4
102292
2015-07-30 10:12:52
--------------------------
246069
5123
Một quyển sách quá đẹp, cả hình thức lẫn nội dung. Nếu không nhìn tên tác giả có lẽ sẽ dễ dàng nhầm lẫn đây là sách của nước ngoài do VN dịch lại. Nội dung sách đơn giản, dễ hiểu, tập cho bé cách đếm từ 1 đến 20, với các minh họa bằng các con thú rất sống động. Thực tế thì mình chỉ cần có thế cho bé nhà mình!
Rất cảm ơn Nhã Nam đã nỗ lực đưa ra những tựa sách tranh thiếu nhi rất Việt Nam, giữa muôn ngàn tựa sách ngoại hiện nay.
5
462641
2015-07-29 17:32:52
--------------------------
238895
5123
Cuốn truyện này thoạt nhìn có vẻ đơn giản vì hình nhiều, chữ rất ít mà nội dung cũng rất đơn giản. Nội dung chính của cuốn truyện là để cho bé tập đếm từ 1 đến 20 và nhận biết được cái loài vật. Sau vài lần đọc truyện cho con nghe, mình phát hiện ra cuốn sách này có thêm chức năng khác nữa. Ứng với mỗi bức tranh ở mỗi trang, bạn có thể sáng tác thành một câu chuyện riêng lẻ, tùy theo trí tượng tưởng và sức sáng tạo của bạn. Quyển truyện này rất phù hợp cho những bạn có con nhỏ.
5
210330
2015-07-23 14:57:36
--------------------------
194880
5123
Mình mua cuốn sách cho con gái 9 tháng tuổi. Mình nghĩ nên nuôi dưỡng thói quen đọc sách cho các em bé từ khi con nhỏ. Dù chưa biết đọc nhưng bé rất thích thú với các hình ảnh. Đây là cuốn sách dễ thương, kể về nai con trên đường gặp các con vật khác. Vừa nói đến các con vật vừa xen kẽ dạy bé các số đếm từ 1 đến 20. Các câu vần nhau giúp mẹ và bé dễ thuộc. Sách khổ to, bìa cứng, hình dễ thương, giấy đẹp. Tuy nhiên sách khá nặng so với các em nhỏ. Nội dung đơn giản phù hợp các em. HI vọng các bạn nhỏ khác cũng yêu thích cuốn sách này.
4
520514
2015-05-12 08:43:04
--------------------------
173554
5123
Đây là một cuốn sách rất tốt dành cho các em nhỏ. Tôi đã mua cuốn sách này mừng sinh nhật lần thứ 3 cho đứa cháu, nó thích mê cuốn sách vì có những hình ảnh minh họa vui nhộn. Sách có bìa cứng vì thế rất nặng nên luôn để dưới nệm cho bé ngồi đọc. Chất lượng sách có Nhã Nam vẫn rất tốt, đặc biệt cuốn sách này được in bằng giấy Couche bóng, mịn và sáng và vì sách có rất nhiều hình ảnh nên in trên giấy trở nên vô cùng bắt mắt. 
Luôn ủng hộ nhiệt tình cho Nhã Nam xuất bản loại sách này vì con em có thể tập đếm số và tập đọc, bên cạnh đó sẽ tạo được thói quen chăm đọc sách cho con em mình ngay từ nhỏ.
4
542401
2015-03-26 14:00:48
--------------------------
151892
5123
Cuốn sách với nội dung giới thiệu đến các bé số đếm từ 1 đến 20, được trình bày rất đẹp. Ít chữ, chữ to, hình được vẽ rất dễ thương, phối màu hài hòa. Đầu sách và cuối sách, họa sĩ còn vẽ minh họa các con số từ 1 đến 20, chia theo dãy số chẵn, dãy số lẻ để các bé có thể học số chẵn, số lẻ.Ngoài ra, các câu trong sách được viết có vần với nhau nên cũng rất dễ nhớ.Mình nghĩ đây là cuốn sách các phụ huynh nên có để hướng dẫn con tập đếm từ những con số đầu tiên
4
15022
2015-01-21 13:45:59
--------------------------
151653
5123
Tôi mua cuốn sách này để đọc cho con nghe mỗi khi con vui vẻ. Cháu mới 3.5 tháng tuổi nhưng tỏ ra rất chăm chú và thích thú với những hình vẽ nhiều màu sắc. Đây là một sách hay để giúp trẻ học đếm và phát triển các giác quan, óc sáng tạo. Tôi rất vui và tự hào khi có những cuốn sách truyện tranh sáng tạo và sinh động dành cho trẻ em mà do chính người Việt sáng tác và minh họa. Chất liệu giấy tốt, bìa cứng (riêng tôi không thích bìa cứng lắm vì khá nặng), và tôi hài lòng vì lần này Bookcare đã bọc luôn cả những cuốn sách "ngoại cỡ" như thế này.
4
490746
2015-01-20 17:06:41
--------------------------
