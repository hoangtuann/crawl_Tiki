464235
7356
Lữ đã ươm ánh mặt trời để tặng cho mọi người một quyển sách ấm áp bởi ánh dương này. 
Mình rất thích đọc giọng văn của Lữ, nó đơn giản nhưng ẩn chứa nhiều suy tư, đọc một lần, hai lần, rồi ba lần, và cứ thế, đọc mãi như nghe Lữ kể một câu chuyện kể đi kể lại vẫn muốn nghe vậy, giọng văn lời văn như thấm đẫm vào trái tim người đọc, khơi gợi những kỷ niệm cũng như mở rộng tư tưởng thoải mái bay lên giữa bầu trời một cách ung dung nhất.
4
74132
2016-06-29 23:50:13
--------------------------
446735
7356
Mình cũng chỉ đọc review trên tiki và rồi mua cuốn này vào đợt giảm giá 50% của NXB Trẻ. Mình không kì vọng nhiều, nên ngay khi mở ra đọc nhưng trang đầu tiên, mình đã thấy rất hài lòng. Cuốn sách được viết với giọng văn chậm đều, gần như là một lời thủ thỉ tâm sự của tác giả với chúng ta. chân chất, mộc mạc, gần gũi và giàu tình cảm là những gì mình cảm nhận được qua những dòng viết của tác giả Lữ.

Và những ai bi quan sầu đời, thiếu niềm tin vào cuộc sống, mình nghĩ đây là một cuốn sách mà bạn nên dành chút thời gian, ngừng lại, đọc và cảm nhận, mình nghĩ nó sẽ giúp được gì đó.

Có thể những bạn không quen hay không thích cách hành văn chậm rãi đều đều, không đầu không cuối, không cao trào kịch tính sẽ không thích cuốn này.
4
500847
2016-06-12 20:35:14
--------------------------
419676
7356
Đùa thôi. Chứ tôi ươm ánh mặt trời là cuốn sách nhẹ nhàng như trời thu nhưng ẩn sau đó có chút gay gắt đốt lê khát vọng của con người.
Nó làm khao khát sống của mỗi người mạnh mẽ hơn
Văn phong nhẹ nhàng, đôi khi như lời thủ thỉ hay vài ba câu xúc cảm của tác giả. Nhất là những nỗi niềm những hy vọng của tác giả thật đẹp
Mình rất vui và không hối hận khi đã đặt mua cuốn sách này
Chần chừ gì mà không nhanh tay đặt mua nào các bạn :)))
5
1182056
2016-04-21 19:09:13
--------------------------
258614
7356
Đây là một quyển sách mà theo mình nghĩ là rất thích hợp để đọc trong những ngày thời tiết âm u, cũng như đem tặng cho bạn bè. Theo cảm nhận của mình thì quyển sách này rực rỡ từ nội dung cho đến cái tên của nó luôn. Nội dung thì hay, ở mỗi tản văn, chúng ta như tìm thấy hay cảm nhận được một cái gì đó ấm áp, nhẹ nhàng và bên cạnh đó cũng có những điều rất thuyết phục.
Với mình, mình cho rằng,  quyển sách có thể chữa lành những vết thương trong tim đấy. Vì ở nó luôn mang đến cho ta sự sâu lắng trong tâm hồn. Nếu xét về con tim, thì quyển sách đã sưởi ấm được con tim. Còn xét về lý trí thì có thể đâu đó khi ta gấp quyển sách lại, ta có thể nghiệm ra rằng "À..ra là cuộc sống cũng tươi đẹp lắm đấy chứ"
4
308302
2015-08-08 17:50:50
--------------------------
257317
7356
Tản văn Tôi Ươm Ánh Mặt Trời là một cuốn sách thích hợp để đọc trong những ngày mưa và cũng hợp đem tặng cho bạn bè. Cuốn sách như toả ra ánh sáng rực rỡ, rực rỡ từ cái tên tác phẩm đến tên tác giả (đơn giản mà rất vang), đến cả cái bìa (sáng bừng đến từng nét vẽ). Nội dung của tác phẩm thì quá tuyệt, mỗi tản văn đều đượm hương vị mặt trời, đem đến cho người đọc cảm giác sáng sủa, hưng phấn. Tuy nhiên giấy in của Trẻ ban đầu thì rất nhẹ rất đẹp nhưng hình như hơi nhạy cảm với thời tiết, dễ ố quá!
5
167283
2015-08-07 16:06:21
--------------------------
223067
7356
Ai đọc cuốn sách này, hẳn cũng sẽ có cảm giác tác giả như viết cho riêng mình. Đáng yêu là thế đấy, từng lời từng lời thủ thỉ, như đang nói chuyện với đứa em gái nhỏ. Về tin yêu, về cuộc sống rực rỡ sắc màu và kỳ diệu. Đọc, và bật cười vì cuốn sách đáng yêu quá, nếu có một người đáng yêu như thế nói chuyện với mình, cứ nhẹ nhàng như thế, chỉ ngồi nghe thôi mà không thể tắt được nụ cười trên môi ấy nhỉ... Ờ, mỗi ngày mặt trời lên là một món quà ai đó dành riêng cho mình, thế giới này đẹp tươi bởi vì có người ươm mặt trời dành tặng riêng cho bạn. Thấy không, thế giới này là quà tặng của bạn đấy. 
4
82774
2015-07-06 15:42:29
--------------------------
198629
7356
Thoạt đầu bị nhầm Phạm Lữ Ân với Lữ chứ. Giờ thì hết rồi. ^^ Nhờ  bạn giới thiệu mà mình đã biết đến văn phong và tác phẩm khá hay "Tôi ươm ánh mặt trời" của Lữ này. Giữa lúc văn chương đang có quá nhiều sách khiến người ta có thể u sầu hơn thì "tôi ươm ánh mặt trời" như một liều thuốc chữa vết thương hữu hiệu với những dòng tản văn không mang đậm tính triết lý nhưng vẫn đầy tính thuyết phục.Văn phong nhẹ nhàng , sâu lắng, rất hợp với "gu" của mình ^^  Đọc tản văn của Lữ thấy đời nhẹ nhàng hơn hẳn.
4
75031
2015-05-20 13:04:06
--------------------------
134180
7356
Đây là tác phẩm đầu tiên mình đọc của Lữ. Và thật sự rất thích.
Phong cách viết nhẹ nhàng, nhẹ đến mức khi đọc những dòng viết về sự chán đời, k muốn sống cũng nhẹ nhàng, dịu dàng và êm ả lạ.
Không phải là những tản văn dạy bạn phải sống thế này, nên thế kia. Chỉ đơn giản là những chia sẻ của anh và em, về những mùa rạng rỡ trong năm, về những kỷ niệm ngọt ngào xưa kia, về những triết lý sống đơn giản. Để từ đó ta nhận ra rằng hạnh phúc ở quanh mình, hạnh phúc đơn giản như là ánh mặt trời mỗi sáng - một món quá mà Lữ đã cất công gieo trồng dành tặng cho ta
Khi gấp quyển sách lại, mình cảm thấy trái tim thật ấm áp, vui vẻ và nhận ra rằng, ờ thì... Cuộc sống này hoá ra cũng thật hạnh phúc nhỉ
5
75271
2014-11-08 18:01:14
--------------------------
