307935
10473
Đây là bộ sách túi đầu tiên tích hợp được rất nhiều yếu tố mà tôi biết. Mỗi cuốn bao gồm nội dung câu chuyện, những câu hỏi xung quanh câu chuyện để bé trả lời (rất hay để rèn luyện khả năng nhận thức và trí nhớ cho bé). Bên cạnh đó là các mục nhỏ như "Trò chơi" (bé có thể chơi cùng bạn để tăng cường khả năng vận động); "Góc khéo tay" và "Công thức nấu ăn" (rất thích hợp cho các bé gái để rèn luyện sự khéo léo và tỉ mỉ). Cuối cùng là một "Lời khuyên" đáng yêu của Camille giúp cho các bé biết cách suy nghĩ và hành động trong các tình huống tương tự. Quả là những cuốn sách thú vị, bổ ích!
5
479122
2015-09-18 13:12:25
--------------------------
