312426
4575
Cuốn sách trình bày, theo tôi đánh giá, khá là khoa học và phù hợp với lứa tuổi của các con từ 5-6 tuổi. Con tôi hàng tuần, với sự hướng dẫn của mẹ, đã và đang làm khá tốt các bài trắc nghiệm trong cuốn vở này. Con rất hào hứng với các bài toán đố trong cuốn vở. Và điều vui nhất của mẹ con tôi là khi anh lớn làm bài tập với mẹ, thì em nhỏ cũng ngồi nghe ké và tỏ ra hào hứng không kém gì anh mình, mặc dù có thể bé chưa hiểu lắm. Tôi rất thú vị khi con nhỏ của mình sau thời gian nghe ké, thì đếm vanh vách từ 1 tới 10 bằng cả tiếng Việt và tiếng Anh. Cuốn sách giúp con vừa học vừa chơi với toán! 
5
572177
2015-09-21 11:10:47
--------------------------
