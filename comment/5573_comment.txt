256352
5573
Một bộ văn học dân gian Đức như truyện cổ Grim đã xuất ở Việt Nam từ rất lâu, lẽ ra phải được chăm chút kĩ lưỡng hơn từ một NXB lớn. Đơn cử như quyển số 3 & 4 có nhiều truyện tương tự nhau, chất lượng giấy khá thấp, ngoài ra ban biên tập chưa thực sự chọn lọc kĩ càng các câu chuyện - một số súc tích tới mức người đọc có thể đọc hết truyện mà vẫn chưa hết hơi - kể cả trẻ em?! Phần tốt nhất là bìa sách, được thiết kế đẹp mắt, và truyện có lồng vào một số tranh minh họa phù hợp với các câu truyện.
3
621799
2015-08-06 20:01:07
--------------------------
