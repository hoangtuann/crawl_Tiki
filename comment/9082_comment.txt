421842
9082
Sách viết dựa trên các nghiên cứu khoa học để từ đó đưa ra cách thức hoạt động của thói quen con người, cơ cấu hoạt động để từ đó giúp người đọc hiểu rõ & tự tạo thói quen tốt cho mình hoặc thay đổi thói quen xấu 
Bố cục sách chặt chẽ, logic tuy nhiên vì đưa ra nhiều nghiên cứu nên đôi lúc đọc hơi hoang mang. Ví dụ tác giả đưa ra 3 bước để hình thành thói quen nhưng dựa trên nghiên cứu 1 thì chỉ thấy 2 bước đầu, để biết được bước thứ 3 phải đọc tiếp qua 1 nghiên cứu khác nên đôi lúc khiến người đọc quên mất phần nội dụng cốt lõi đã đọc trước đó
Nếu sách có thêm phần tóm tắt ở mỗi cuối chương thì sẽ tiện hơn cho người đọc cũng như ghi nhớ. Như đã nói ở trên, tác giả không đưa ra 1 cách thức để tạo ra 1 thói quen cụ thể nào mà chỉ đưa ra cách thức hình thành thói quen tổng quát của con người, việc vận dung để tạo thành thói quen là dựa trên sự đúc kết & hành động của mỗi người sau khi đọc và hiểu sách này. 
4
6253
2016-04-25 22:40:40
--------------------------
416060
9082
Với ngôn từ dễ hiểu, cuốn sách giải thích một cách khoa học nhưng cũng rất đơn giản về cách hình thành thói quen và nguyên lý hoạt động của nó. Bên cạnh đó, những ví dụ được đưa ra rất sinh động, gây hứng thú cho người đọc. Tuy nhiên mình thấy "Vòng lặp thói quen" được lặp đi lặp lại hơi nhiều, dù biết rằng tác giả muốn nhấn mạnh về vòng lặp này, nhưng việc đưa ra một cách quá liên tục khiến đôc giả bị ngợp.
Nếu bạn đang muốn hình thành những thói quen mới, vất vả tìm cách gạt bỏ những thói quen xấu xí thì đây là một cuốn sách không thể thiếu trong quá trình bạn thực hiện ý định của mình.
4
61691
2016-04-14 13:24:30
--------------------------
360379
9082
Thói quen tạo lập tính cách quả không sai. Luyện tập thói quen tốt mang giúp cuộc sống chúng ta thêm thuận lợi, đơn giản, thành công. Với cuốn sách này tôi có thể biết được cơ chế của thói quen từ đó chỉnh sửa những thói quen xấu của mình, tạo lập nhiều thói quen tốt trong cuộc sống. Sau khi đọc cuốn sách tôi nhận ra rằng không có gì là không thể thay đổi được, thói quen xấu cũng vậy. Sách đẹp, chất liệu giaya tốt, in rõ ràng. Giao hàng nhanh. Cảm ơn tiki. Sẽ tiếp tục ủng họ tiki
4
732928
2015-12-29 08:18:26
--------------------------
343347
9082
Mình thật sự tốn nhiều thời gian để đọc cuốn sách này, nhiều khi phải đọc đi đọc lại nhiều lần mới hiểu được hết ý của tác giả, một phần vì dịch giả dịch chưa hay lắm, câu cú khó hiểu, phần vì kiến thức trong sách tương đối chuyên môn.
Tuy vậy, theo mình đánh giá, cuốn này là một cuốn nên đọc, không chỉ trong lĩnh vực kinh tế hay marketing mà còn rất ữu ích trong đời sống. Áp dụng các nguyên lý trong sách, mình đã tạo được nhiều thói quen tích cực để “đè” hết các thói quen tiêu cực. ^^
4
780900
2015-11-26 10:58:51
--------------------------
331075
9082
Có rất ít cuốn sách viết về thói quen của cá nhân và tổ chức xuất sắc như Sức mạnh Thói quen của Charles Duhigg. Mình xin có một vài nhận xét:
- Nội dung cuốn sách rất hay và chặt chẽ, giúp ta biết được một thói quen hình thành như thế nào, thói quen xấu, thói quen tốt, làm sao để thay đổi thói quen đó,...
- Hình thức sách đẹp, khá sát với bản gốc tiếng Anh, giấy in rõ nét,.. Đây là điểm mình thích ở alphabooks.
- Giá thành hơi cao, nhưng rất hợp lý với kiến thức mà nó đem lại. 
4
62086
2015-11-03 22:25:09
--------------------------
314374
9082
Bản thân của mình là đứa có xu hướng dễ bị sa ngã, và có nhiều thói quen xấu. Không phải dễ khi mình mua sách bởi một phần mình thích mua quần áo, giày dép hơn mua sách. Nhưng cuối cùng thì mình đã không hối hận khi sở hữu cuốn sách này vì hầu như những gì mình đang lấn vào, những gì đang khiến mình lo sợ, những điều mà mình muốn cố gắng bị đẩy ra,... tất cả đều nêu rõ mồn một. 
Bây giờ mình đang cảm thấy mạnh mẽ hơn, lý trí hơn. Cuốn sách này nếu đọc một lần thì không bao giờ thấm, mà phải đọc đi, đọc lại, viết ra những câu mình yêu thích dán lên tường thì mới thực sự đáng giá.
5
485195
2015-09-25 21:55:53
--------------------------
307325
9082
Ai trong chúng ta cũng đều có những thói quen. Và chúng ta để nó hiện hữu như một điều hiển nhiên trong cuộc sống của mình. Chúng ta không hề biết rằng, nó có một sức mạnh khủng khiếp. Nhưng thói uen mang lại hiêu quả tốt xấu như thế nào còn phụ thuộc bản chất và hành vi của từng người. Thông qua tác phẩm, chúng ta biết được ảnh hưởng của một số thói quen đến chính mình, và điều quan trọng hơn, chúng ta biết được giá tri và sức mạnh vô hình của nó để tạo động lực cho bản thân điều chỉnh theo một hướng tích cực hơn.
5
482216
2015-09-18 01:13:39
--------------------------
302337
9082
Mình đọc được quyển này khi vào nhà sách, thích quá mà hơi đắt với sinh viên nên thấy tiki bán là mình mua ngay. Sách mở đầu bằng câu hỏi vì sao chúng ta lại có những thói quen xấu, biết là xấu nhưng khó bỏ rồi đi vào phân tích thói quen cá nhân sau đó là thói quen của tổ chức. Cáng đọc càng thấy hay. Thói quen ảnh hưởng đến chúng ta rất nhiều., bỏ được thói quen xấu và học hỏi những thói quen tốt sẽ giúp chúng ta tốt hơn rất nhiều. Ai đang có những thói quen k tốt thì nên đọc nhé. 
5
61311
2015-09-15 10:04:59
--------------------------
283725
9082
Đây không chỉ là cuốn sách mà Business Insider khuyên đọc mà còn là cuốn sách mà rất nhiều tỉ phú nổi tiếng đã từng đọc từ khi họ còn rất trẻ. Thiết nghĩ thói quen cũng làm nên cuộc đời mỗi người, có những điều đơn giản diễn ra hằng ngày xung quanh bản thân mà ta không để ý đến. Tất cả những điều đó đều có thể ảnh hưởng trực tiếp đến cuộc sống mỗi người.
Chính vì vậy, cuốn sách này cho ta nhìn nhận lại bản thân, những thói quen mà ta cần thay đổi để trở nên tốt hơn, ít ra là chiến thắng chính bản thân mình.
5
723262
2015-08-30 10:55:12
--------------------------
282647
9082
Cuốn sách đem đến cho chúng ta một cái nhìn sâu về cách để hình thành thói quen cho mọi sự vật, cách chú khỉ làm để có được thứ mình muốn là nước hoa quả, cách lập chiến dịch quảng cáo để hướng khách hàng hành động để thỏa mãn được một nhu cầu sở thích nào đó. Nếu áp dụng các kiến thức này trong việc nắm bắt tâm lý mua hàng của khách hàng thì sẽ thành công. Một cuốn sách chuyên về tâm lý thói quen rất đáng đọc cho những ai muốn tìm hiểu về lĩnh vực này.
4
93002
2015-08-29 12:18:48
--------------------------
277120
9082
Một cuốn sách hay cho tất cả những người trẻ tuổi đang loay hoay tìm một lối thoát cho cuộc sống đơn điệu, cá nhân và lòng vị kỷ của bản thân mình. bạn sẽ tìm thấy được phần lớn các tính xấu của mình được miêu tả một cách khá là tường tận và "thực tế một cách đau lòng" đến mức bạn chỉ muốn đọc ngay cho xong quyển sách và bắt tay vào thay đổi cuộc đời mình! Rất lâu rồi mình không đọc được một quyển sách có sức bật như vậy đến tâm hồn nên có thể đánh giá đây là một cuốn sách rất đáng cho các bạn trẻ đọc, nhất là các bạn sinh viên hằng ngày đang tự giết thời gian và chính mình bằng những thói quen xấu
4
14979
2015-08-24 17:50:41
--------------------------
276918
9082
Theo tôi biết đây chính là một quyển sách xuất sắc về thói quen, đó chính là lý do khiến tôi phải đặt mua quyển sách ”sức mạnh của thói quen” này trên Tiki. Và khi nhận được quyển sách này trong tay, tôi thấy quyển sách này bìa sách rất đẹp, giấy in rất tốt, nội dung trong quyển sách này đề cập đến những nghiên cứu về thói quen. Đã có rất nhiều sách viết về chủ đề nói nhưng hầu như đều chưa có quyển sách nào phân tích nó một cách tỉ mỉ và đưa ra các hướng áp dụng rõ ràng, và quyển sách này đã giải quyết được vấn đề đó. Nhìn chung, theo tôi đây thật sự là một quyển sách rất đáng để chúng ta mua về đọc và nghiền ngẫm.
5
42985
2015-08-24 14:50:33
--------------------------
266526
9082
Mình rất thích quyển sách này “Sức mạnh của thói quen”- Charles Duhigg. Đây là quyển sách mình đã tìm kiếm từ  lâu, giúp phát triển kỹ năng cá nhân. Thói quen quyết định thành công hay thất bại ở mỗi người. Quyến sách giúp cho chúng ta có cái nhìn toàn diện hơn về thói quen, thói quen cá nhân hay thói quen tổ chức/ xã hội; lợi ích của thói quen, phân biệt thói quen tốt & xấu như thế nào? Ngoài ra còn có lời khuyên về các cách vận dụng thói quen, các nguyên tác để thay đổi thói quen,… Sách rất hay và bổ ích.
Xin cám ơn.
5
554150
2015-08-14 19:33:17
--------------------------
205254
9082
Nội dung sách giúp chúng ta phát triển kỹ năng cá nhân. Giúp chúng ta tìm ra đâu là thói quen xấu cần bỏ đi, đâu là thói quen tốt cần phát huy. 
Nhìn chung chủ đề sách khá hay, nhưng viết còn hơi lang mang theo kiểu tự truyện. Chỉ cần đọc 1/4 sách có  thể hiểu hết nội dung, nhưng sách lại viết khá dài dòng, lại thiếu ví dụ ,bài học thực tế . Không biết có phải là sách được dịch lại nên cách viết không diễn đạt được hết ý hay không.
Mặc dù vậy mình cảm thấy giá thành so với nội dung sách thì hợp lý, đáng để bạn tham khảo
4
251819
2015-06-06 10:32:29
--------------------------
171140
9082
Con người chúng ta ai cũng có những thói quen cố hữu được hình thành từ ngày này qua tháng nọ. Trong số đó có những thói quen tốt và cả những thói quen xấu. Vấn đề quan trọng là đa số chúng ta có thói quen xấu nhiều hơn. Tôi dám chắc như vậy. Nếu không chúng ta đã thành công như số ít những người giàu có trên trái đất này. Nhưng chúng ta cần phải thay đổi những thói quen xấu và thay vào đó là những thói quen tốt. Quyển sách này sẽ giúp cho chúng ta thực hiện điều đó. Nội dung thực tế, dễ hiểu và chất lượng tốt. Hãy thay đổi thói quen và chúng ta sẽ sống tốt hơn qua từng ngày.
5
387632
2015-03-21 12:25:57
--------------------------
154594
9082
Đây là một cuốn sách kết hợp hai thể loại kỹ năng và sách kinh tế, nó không chỉ có những lý luận về sự hình thành thói quen, vòng lặp thói quen, mà phần chính của cuốn sách là rất nhiều dẫn chứng cụ thể về sử dụng thói quen trong xã hội học và kinh tế học, như hệ thống nghiên cứu thói quen người mua hàng của các công ty bán lẻ, hay cuộc biểu tình phản đối phân biệt chủng tộc, những dẫn chứng này rất hay, nhưng mang tính kể lể hơi nhiều, dẫn đến cuốn sách có phần dài dòng và lan man. Dù sao thì đây cũng là một cuốn sách đáng đọc.
4
472173
2015-01-29 20:16:31
--------------------------
141995
9082
Điều nhận thấy đầu tiền ở Sức Mạnh Của Thói Quen chính là sách khá dày. Bìa được thiết kế đơn giản mang ý nghĩa vượt thoát ra khỏi những thói quen thường ngày. Về nội dung, quyển sách là công trình nghiên cứu quy mô của Charles Duhigg về thói quen, sự hình thành thói quen cũng như tác động của thói quen đến thành công của con người. Đồng thời tác giả cũng đưa ra những giải pháp để người đọc từ bỏ thói quen cũ, thay vào đó là những thói quen mới tốt hơn. Những lời khuyên, giải pháp mà tác giả đưa ra thật sự rất dễ hiểu và dễ dàng áp dụng. Mình đã thử và thành công :) Tuy nhiên, giá như tác giả cô đọng nội dung quyển sách hơn nữa và đưa thêm nhiều ví dụ minh họa thì quyển sách sẽ tuyệt vời hơn.
3
398179
2014-12-17 12:53:58
--------------------------
127423
9082
Mình vẫn đang đọc quyển sách này nhưng mình thấy một số nhận xét sau:
- nội dung chỉ xoay quanh việc thay đổi thói quen thì mình nên làm thế nào để thay đổi thói quen, quá nhiều ví dụ minh họa trong khi đó phần cuối cuốn sách tác giả đã tổng hợp lại cụ thể vì vậy chỉ cần nêu một vài ví dụ là được. 
- Một số trường hợp gần gũi với cá nhân được tác giả  phân tích cụ thể làm cho người đọc dễ áp dụng.
Nói chung bạn chỉ cần đọc lướt qua và tổng hợp 1/3 cuốn sách là đã hiểu được toàn bộ vấn đề cần nắm bắt. Tuy nhiên nếu bạn có thời gian bạn đọc kỹ thì thấu hiểu được nhiều nội dung hơn.
2
429415
2014-09-24 10:57:47
--------------------------
114545
9082
Minh có tình cờ biết bản gốc Tiếng Anh của cuốn sách trước khi nó xuất bản bằng tiếng Việt. Theo mình thấy nhận xét chung về sách khá tốt và các trang ebook nước ngoài đều có giới thiệu qua cuốn này như một cuốn sách hay để đọc. Tất nhiên là với những lời giới thiệu có cánh thì ngay khi có bản tiếng Việt ra mắt, mình đã mua ngay. Tuy nhiên, khi đọc xong thì mình nhận thấy có một số vấn đề sau:
- Nội dung hay, thiết thực được dẫn dắt bằng nhiều câu chuyện. Chính cách viết này khiến sách đỡ khô khi đọc và tiếp cận vấn đề nhưng lại khiến sách rơi vào cảnh viết lan man. "Vòng lặp thói quen" bị lặp đi lặp lại khá nhiều lần, tuy lần nhắc lại sau có bổ sung thêm vài ý nhưng nhìn chung phần lớn nội dung bị lặp lại và quá dàn trải. Mình nghĩ nếu không có thời gian bạn chỉ cần đọc chương cuối phần 1 là cũng đã đủ nắm 70% ý của tác giả.
- Sách viết làm 3 phần; cá nhân, tổ chức và cộng đồng. Mình thấy 2 phần sau viết quá dài, chiếm phần lớn nội dung sách nhưng cũng lại rơi vào cảnh viết lan man và chỉ nêu bật thêm vài ý nữa thôi.
- Cuối cùng là phần biên tập sách. Bề ngoài, sách được design khá sát bản tiếng Anh, nhưng nội dung thì bị lược bỏ một số phần. Thêm vào đó, văn phong dịch không được hay, nhiều đoạn đọc lủng củng và tối nghĩa. Ngoài ra, mình đánh giá cao tư tưởng cũng như kiến thức mà tác giả chắt lọc mang lại. Không phải đơn giản mà cuốn sách lại được nhiều đánh giá tích cực. Tuy nhiên giá tiền sách so với nội dung thu được từ nó chưa thật sự hợp lý (nhất là đối với các bạn học sinh, sinh viên).
3
4955
2014-06-15 02:48:57
--------------------------
111853
9082
Khi mua cuốn sách này mình đã nghĩ nó sẽ cho mình biết thêm nhiều thứ về thói quen cũng như cách để tạo lập/ thay đổi thói quen. Quả thật cuốn sách đã đáp ứng được điều đó nhưng không nhiều như mình nghĩ. Những thông tin hữu ích dường như chỉ tập trung trong vài chương đầu trong khi phần lớn quyển sách khá lan man, quá nhiều ví dụ, câu chuyện mà chẳng làm rõ thêm được vấn đề. Thiết nghĩ độ dày cũng như giá tiền của quyển sách nên giảm đi khoảng 1 nửa thì sẽ phù hợp hơn với lượng thông tin nó mang lại
3
256757
2014-05-04 20:18:04
--------------------------
99653
9082
Cuốn sách này mình biết từ năm ngoái khi lên Amazon xem bản tiếng Anh của nó thì rất nhiều người đã chọn mua và đánh giá cao. Tác giả đã phân tích từng các bước hình thành thói quen thông qua các thói quen của cá nhân, công đồng và tổ chức từ đó đưa ra mô hình chung để thay đổi chúng. Bạn có thể thấy ở cuối sách tác giả đã tổng hợp lại tất cả các luận điểm chính để hướng dẫn thay đổi một thói quen như thế. Đây là một cuốn sách hay nhưng phần bìa sách Alphabooks dánh keo sách quá chặt làm mình hơi khó chịu, điểm khác là Alphabooks đã lược bỏ hết tất cả phần chỉ mục (index) để người dùng tham khảo thấy khá là thất vọng. 4 sao cho cuốn sách này
4
12414
2013-11-17 07:28:33
--------------------------
96180
9082
Vấn đề sách nêu ra rất hay và bổ ích. Tác giả đã phân tích quy luật hình thành thói quen của chúng ta cả tốt và xấu. Dẫn chứng nhiều trường hợp điển hình nổi tiếng từ cá nhân đến các tổ chức trên thế giới trong việc hình thành các thói quen.
Đây là cuốn sách giúp bạn nâng cao kiến thức  cho bản thân, sau khi đọc sách bạn có thể có sáng kiến để thay đổi bản thân mình, cơ quan hay tập thể loại bỏ những thói quen xấu và tạo dựng những thói quen tốt.
- Sách được in bằng chất liệu giấy tốt, bìa mềm, kết cấu đề mục rõ ràng, hình thức trông gọn gàng tạo cảm hứng cho người đọc. Nhưng phần dịch ra tiếng việt tôi thấy một đôi chỗ chưa được xuôi cho lắm, làm phải đọc đi đọc lại để rõ nghĩa.
4
175449
2013-10-06 10:31:10
--------------------------
78629
9082
Những năm 1900, nước Mỹ từng khẳng định vệ sinh răng miệng không tốt là hiểm hoạ quốc gia. Nhưng một quảng cáo của Claude Hopkins đã thay đổi được "hiểm hoạ" đó. Từ con số 7% số người Mỹ có 1 tuýp kem đánh răng trong nhà, sau 10 năm, con số này là 65%. Hopkins đã quảng cáo điều gì? tại sao 1 sản phẩm có thể tạo được sự thay đổi to lớn như vậy? Và như chúng ta biết hiện nay đánh răng mỗi ngày là một việc làm không thể thiếu, nhưng tại sao nó lại không thể thiếu? 
Đọc Power of habit đi các bạn trẻ. Và các bạn sẽ hiểu chúng ta đơn giản như thế nào, đã bị các nhà kinh doanh dắt mũi ra sao.... 
Ps: Riêng mình đã li giải được (rất khoa học đấy nhé) là vì sao kem đánh răng ps không đươc người tiêu dùng ưa chuộng, hâhhahah
5
15043
2013-06-02 14:58:00
--------------------------
73383
9082
Đọc quyển sách này, tôi mới nhận ra những thứ mình làm hằng ngày, vào cùng 1 khung giờ luôn lặp lại những hành động giống nhau: 19h học bài... 22h mở máy tính hoặc xem phim... Bây giờ tôi mới hiểu vì sao lại làm những việc đó gần như một thói quen và không xem đồng hồ tôi vẫn đến đúng giờ là không làm gì khác được. Thật sự thì giờ tôi đã biết chính bản thân đã tạo cho mình những thói quen lâu dài, và khó thay đổi trong ngày một ngày hai.
Tôi thường hay vừa học vừa nghe nhạc, không có nhạc là không học bài được. Nhưng sau khi đọc quyển sách này, tôi biết được đó là thói quen không hề tốt. Với những dẫn chứng xác thực được nêu ra trong sách, tôi đã xác định được cái gì tốt cái gì không tốt cho bản thân, và dần dần thay đổi thói quen của mình. Giờ thì tôi không cần nghe nhạc mà vẫn học được bình thường.
Để biết những thói quen của bản thân là tốt hay không, có phù hợp với môi trường sống hay không, bạn nên đọc quyển sách này và biết đâu bạn sẽ thay đổi một số thói quen hằng ngày để tạo cho mình một cách sống khoa học và tốt nhất để có thể làm việc một cách thành công hơn. Thử xem thế nào nhé.
5
34754
2013-05-07 22:58:51
--------------------------
70794
9082
Đây là một quyển sách khá hay. Tôi phải công nhận rằng đoạn đầu đến phần giữa, tác giả viết rất khoa học và có những dẫn chứng khiến tôi phải ngả mũ khâm phục.
Đặc biệt là ví dụ về loài khỉ ăn socola hay thói quen của một người đã bị mất trí nhớ, Starbucks...
Song cho đến khoảng gần cuối quyển sách, nội dung có vẻ hơi thiên về mặt kinh tế chứ không hề có trong đời sống xã hội. Chấp nhận đây là một quyển sách về kinh tế, nhưng Sức mạnh của Thói quen : có nghĩa là phải đảm bảo chu toàn về cả mặt đời sống. Thế mới đủ đầy và làm hài lòng độc giả.
Đây là quyển sách hay về thay đổi hành vi con người, nhưng theo quan điểm của tôi, nó chưa phải hay nhất.
Cảm ơn Tiki đã mang đến cho tôi một sự nhìn nhận rất mới mẻ  trong định nghĩa của hai từ "Thói quen".
3
50283
2013-04-23 13:03:16
--------------------------
70613
9082
Nhiều người không để tâm đến thói quen thường ngày của mình và cũng chẳng hề thấy được sức mạnh tiềm ẩn đằng sau những thói quen đó. Ông bà ta chẳng nói "giang sơn dễ đổi, bản tính khó dời" là gì! Một khi thói quen đã ăn sâu vào tiềm thức, nó khó lòng rứt ra khỏi con người. Nó lôi khiến con người đi theo những lối đi, hoặc đúng, hoặc sai, không ai luờng trước được. Đọc "Sức mạnh của thói quen" bạn sẽ tìm thấy sự nhẹ nhàng, cũng như thú vị đến từ ngay cả những thói quen hằng ngày của bản thân mình. Quyển sách là món quả chân thành và sâu sắc dành tặng cho những người thân, bè bạn hoặc cho chính mình để cùng nhau suy ngẫm về cuộc sống và về những điều đang diễn ra hằng ngày ở xung quanh ta.
4
92965
2013-04-22 11:13:14
--------------------------
70254
9082
Charles Duhigg-một tác giả khá lạ mà mình mà mình mới nghe thấy, nhưng cuốn sách của ông thì thật sự hay "Sức mạnh của thói quen". Theo mình thấy thì ngày nay các bạn trẻ thường bị sa đà vào những thói quen vô bổ mà họ vẫn thường làm hàng ngày, như lướt web hàng giờ đồng hồ, chơi game mải miết mà họ không dành thời gian đó vào những việc có ích cho cuộc sống hơn.
Theo mình đây chính là vấn đề của việc hình thành thói quen, bởi họ đã quá quen với những thói quen xấu và vô bổ đó mà nhiều người đã đánh mất tuổi trẻ của mình, đây chính là một cuốn sách sẽ giúp bạn đi đúng hướng trong cuộc sống. Việc hướng dẫn cách hình thành những thói quen tốt, từ chối sa đà vào những thói quen xấu, điều đó sẽ dần giúp bạn cải thiện được bản thân mình. Một cuốn sách có ý nghĩa.
5
60663
2013-04-19 21:53:05
--------------------------
69668
9082
Đúng như những lời nhận xét, về cơ bản thì người lớn và trẻ em không khác nhau là mấy. Như ta đã biết: "Gieo thói quen thì gặt tính cách". Tất cả những tâm tư suy nghĩ cho đến tính cách trong mỗi con người chúng ta đều được phản ánh qua những thói quen hằng ngày. Nhưng với xã hội hiện nay, việc những thói quen xấu đang dần lấn át tất cả những thói quen tốt đẹp mà chúng ta hình thành từ khi còn là đứa trẻ, điều nay thật sự rất đáng lo ngại. Cuốn sách xuất hiện như một món quà của cuộc sống, hi vọng nó có thể đến tay tất cả mọi người đọc để chúng ta có thể cùng cảm nhận và học hỏi những điều mà nó mang lại.
4
112629
2013-04-17 13:47:55
--------------------------
69638
9082
Vấn đề thói quen ảnh hưởng đến sự thành công của bạn trong cuộc sống không còn mới mẻ nữa. Tuy nhiên, để tập hợp lại và phân tích chúng thì có lẽ là ít. Và "Sức mạnh của thói quen" là một trong số ít đó. 
Cuốn sách giúp cho người đọc có cái nhìn khái quát về bản thân mình hơn, như nhìn chính mình trong gương vậy. Từ đấy có thể thấy được những điểm mạnh, điểm yếu của bản thân mà phát huy hoặc khắc phục. 
Việc từ bỏ thói quen xấu đã khó, nuôi dưỡng và duy trì được thói quen tốt lại càng khó hơn. Làm sao để những thói quen tốt đó trở thành một trong những đức tính của bạn thì nó đòi hỏi phải trải qua một thời gian rèn luyện dài. Đơn giản như thói quen đúng giờ chẳng hạn. Để có thói quen đúng giờ, tức là bạn phải có kế hoạch cụ thể và khoa học. Bạn phải biết tính toán và xử lý công việc như thế nào tốt nhất để cán đích như lịch đã hẹn. Như vậy, để có thói quen đúng giờ, bạn phải thiết lập những thói quen khác liên quan đến nó và phải được làm liên tục đến khi nó thuộc về bạn. Điều đó đòi hỏi sự kiên nhẫn. Đúng vậy, không có thành công nào mà không có dấu chân của người bạn kiên nhẫn cả. THÓI QUEN TẠO NÊN TÍCH CÁCH, TÍNH CÁCH QUYẾT ĐỊNH SỐ PHẬN.
4
63376
2013-04-17 11:59:30
--------------------------
68673
9082
Như nhiều người đều hiểu, thói quen đóng vai trò quan trọng trong thành công của mỗi cá nhân, doanh nghiệp và cả cộng đồng. Có lẽ cũng chính vì điều đó, không ít các khóa học phát triển bản thân chú trọng vào việc tập cho học viên các thói quen tốt. Đã và đang tham gia một số khóa học như thế, tôi nhận rõ được những bài học ấy có thể nói đã được cô đọng một cách khá đầy đủ trong quyển Sức mạnh của thói quen. Nội dung sách ngắn gọn, rõ ràng, phân tích một cách khoa học các vấn đề trong cuộc sống. Các thói quen và "hệ quả" của thói quen nêu trong sách đều khá gần gũi, quen thuộc và dễ dàng gặp trong cuộc sống của bất kỳ một người nào, khiến đôi lúc tôi vừa đọc, vừa ngạc nhiên "sao giống mình quá nhỉ" rồi tự cười. Điều đó giúp những thông điệp và bài học trong sách đi vào người đọc một cách nhẹ nhàng hơn. Tuy nhiên, để có thể bỏ một thói quen xấu là một việc rất khó, và việc thành lập một thói quen tốt cũng không hề đơn giản. Và điều đó khiến dù cho nội dung sách không kém gì các khóa học phát triển bản thân, hiệu quả của nó vẫn không thể so sánh được khi không trực tiếp đưa người đọc vào những khuôn khổ nhất định để hình thành một thói quen. Vì vậy, để cuốn sách thật sự có giá trị đối với mỗi người, điều thiết yếu đó chính là sự nhận thức và sự tự giác của bản thân người đọc.
4
14095
2013-04-12 20:33:07
--------------------------
68503
9082
    Cuốn sách rất thiết thực với đời sống của chúng ta. Nếu để ý, ta sẽ nhận ra mỗi người chúng ta đều có rất nhiều thói quen, tốt có, xấu cũng có. Có thể từ trước đến nay chúng ta đã vô tình bỏ qua những thói quen của mình, ta xem nó như là những điều tất yếu trong cuộc sống, do vậy chúng ta cứ thực hiện chúng một cách đều đều, không nghĩ ngợi. Khi đọc cuốn sách, tôi vô tình đã nhận ra, thì ra mình lại có nhiều thói quen đến như vậy. 
    Vì thế, mỗi ngày tôi để tâm đến những hành động, việc làm của mình hơn, để có thể tìm ra những thói quên tốt và phát huy, và đương nhiên là sẽ cố gắng loại bỏ những thói quen xấu nếu tôi nhận ra điều đó.
4
68435
2013-04-12 08:20:17
--------------------------
68454
9082
con người có rất nhiều bản chất mà thậm chí chính con người cũng không nhận ra.
và một trong số đó, là sự ảnh hưởng của thói quen tới cuộc sống hàng ngày. thói quen là một loại phản xạ có điều kiện có tầm ảnh hưởng ghê gớm đến mọi quyết định trong đời của mỗi con người. chúng ta có thể trở nên tốt hơn, tệ hơn, ích kỉ hơn,rộng lượng hơn,... Tất cả đều được hình thành từ những thói quen.
thoạt đầu tôi cứ nghĩ cuốn sách này cũng chỉ là một loại văn học triết lí khô khan cứng nhắc. thế nhưng dưới ngòi but của Charles Duhigg, mọi thứ lại trở nên chân thực, rõ ràng, hiển nhiên như đúng bản chất của nó vậy
quả là một cuốn sách đáng để đọc, đáng để suy ngẫm, đáng để học tập.
mong là tôi không có quá nhiều thói quen xấu ^^

4
101433
2013-04-11 22:32:07
--------------------------
68271
9082
Đây là 1 cuốn sách tuyệt vời, và tôi nghĩ bạn cần phải đọc nó. Điều gì đã khiến tôi đưa ra một lời khẳng định dứt khoát như vậy? Lý do tôi nghĩ đây là một cuốn sách tốt vì nó qua các nghiên cứu trong cuốn sách đã cho chúng ta thấy được  cách những thói quen được hình thành và thay đổi như thế nào. Chúng ta từng biết ai đó đã từng vượt qua khỏi những giới hạn, hoặc người đó từng hút thuốc, và bất ngờ, chỉ qua 1 đêm thức dậy, họ đã có thể thay đổi bản thân họ chỉ trong 1 thời gian ngắn. Bạn có thắc mắc rằng họ đã làm điều gì để thay đổi mình như vậy không? Họ hình thành thói quen mới và thay đổi những cái cũ, đó là như thế nào.

Bạn hãy làm điều gì đó vừa đủ và rồi nó sẽ trở thành cái mà ta gọi là thói quen, nó có thể tốt mà cũng có thể xấu. Điều này đã được nói đến trong cuốn sách qua các nghiên cứu về triệu chứng "mất trí nhớ". Ví dụ: các nghiên cứu đã chỉ ra rằng những người bị mất trí nhớ không thể chỉ ra đâu là nhà bếp khi được hỏi đến, thế nhưng khi bản thân họ cảm thấy đói, họ sẽ tự động đứng dậy và đi đến nhà bếp 1 cách tự động.

Điều mà bản thân tôi quan tâm nhất khi mua cuốn sách chính là làm cách nào để thay đổi thói quen, và cuốn sách đã thành công khi mô tả rất kỹ lưỡng cách để bạn và tôi có thể thay đổi những thói quen của mình. Hãy dũng cảm đối mặt với nó, tất cả chúng ta có những thói quen chúng ta muốn thay đổi. Để thực hiện điều này chúng ta cần phải ghi nhớ những tín hiệu và cảm xúc khi thực hiện điều đó , nhưng hãy thay đổi những thói quen hàng ngày. Tôi sẽ sử dụng một ví dụ từ cuộc sống riêng của tôi để minh họa cho điều này. Tôi là người làm việc văn phòng và stress dường như là người bạn đồng hành với tôi, và tín hiệu sau đó là tôi sẽ lấy hộp thuốc ra và hút đến khi nào cảm thấy thoải mái mới thôi, thoải mái chính là cảm giác mà tôi có được, nó như là 1 phần thưởng vậy. Bây giờ để thay đổi nó, khi thấy tín hiệu stress, tôi sẽ dừng công việc lại và bước ra khỏi văn phòng, đi dạo, nói chuyện với 1 ai đó trên đường, hoặc đơn giản là chạy bộ, cũng cái cảm giác thoải mái đó nhưng tôi thấy bản thân mình khỏe hơn rất nhiều.

Nói về thói quen, bản thân tôi đã nghe rất nhiều lời phàn nàn như là: tôi không thể làm được nó, tôi đã cố gắng rất nhiều rồi nhưng tôi không thể làm được. Và còn hàng tá các lời biện minh khác. Tôi biết rằng thay đổi thói quen là 1 điều khó, bởi vì phần nào chúng ta đã quen với nó rồi, nhưng hãy tin tôi, điều đó là hoàn toàn có thể và tôi cam đoan với bạn rằng cuốn sách này sẽ cho bạn thấy điều kì diệu đó.
5
13944
2013-04-11 09:12:49
--------------------------
68234
9082
Mình được tặng cuốn sách nhỏ này vào dịp sinh nhật, đối với mình, đó là một món quà rất ý nghĩa, mọt sách mà ^^, Tuy đã là học sinh cuối cấp rồi, nhưng mình vẫn dành chút thời gian cho việc đọc sách, nhất là những cuốn sách giúp mình rất nhiều trong suy nghĩ, cách sống và sự thành công trước ngưỡng cửa cuộc đời  như "Sức Mạnh Của Thói Quen".
Thói quen - một danh từ hết sức gần gũi với mỗi chúng ta, trẻ nhỏ, lớn hơn, về già, ai cũng có cho mình những thói quen.  Có những thói quen bắt đầu khi ngày mới bắt đầu, và cũng có những  thói quen cả khi một ngày kết thúc... Có thói quen tốt, mà cũng có thói quen xấu. Ừ thì đó là những điều nho nhỏ mà bất cứ ai trong chúng ta đều có thể nhận ra. Thế nhưng,  được bao nhiêu người trong chúng ta hiểu hết được giá trị thật sự của một thói quen, sức mạnh vô hình nhưng lớn lao mà nó đem lại?
Charles Duhigg với một cách kể, một cách phân tích và dẫn dắt đầy lôi cuốn, ông đã khai sáng trong lòng độc giả những ý tưởng ngỡ thông thường mà đầy quý giá. Từ đó, ta biết sống chậm lại để nhìn về bản thân, biết phân biệt thế nào là một thói quen tốt, thế nào là một thói quen xấu, và chúng ta hoàn toàn có thể kiểm soát những thói quen, hành động của mình.
Chúng ta cần phải sống có nguyên tắc hơn, không thể cứ để những năm tháng cuộc đời ngỡ dông dài mà hoài phí, sống theo cảm hứng và cảm xúc sẽ chẳng mang lại hiệu quả và sự thành công lâu dài. Và, ta phải biết tạo cho mình những thói quen, trong học tập, làm việc, và cuộc sống, cho chính mình, cho tập thể, và cho cả xã hội. Và chỉ cần như vậy, ta sẽ đạt được những thành công đầy mĩ mãn, với những kết quả không ngờ. Như một lời thách thức, cuốn sách gợi những cảm giác khám khá và trải nghiệm trong lòng người đọc " hãy thử dành tặng cho cuộc sống của mình những thói quen, chắc hắn, cuộc sống sẽ tặng lại bạn thành công, chiến thắng và hạnh phúc"
Lại là một cuốn sách khiến mình rung động, chẳng phải vì nó mang lại những cảm xúc thường nhật của con người, cũng chẳng phải vì nó lấy đi nước mắt và gợi những xót xa, mất mát... đơn giản, nó mang lại cho mình trải nghiệm, sự hiểu biết và ý thức mạnh mẽ với ước mơ được thay đổi bản thân, được sống tốt hơn và hoàn thiện mình hơn mỗi ngày.
Vậy là đủ, cho ý nghĩa của một cuốn sách được xuất bản, được ra đời, và đến tay bạn đọc.

4
10984
2013-04-10 22:40:44
--------------------------
68177
9082
Như chúng ta đã biết :" Gieo thói quen thì gặt tính cách"... Bản chất con người chúng ta như thế nào được thể hiện qua thói quen hàng ngày của họ... Cuốn sách đã mang lại nhiều lời khuyên bổ ích, nhiều bài học đáng để suy nghĩ giúp chúng ta dần dần hoàn thiện, hình thành những thói quen tốt cho mình để  có thể trở thành những con người thành công trong cuộc sống này... Cuốn sách là 1 quà tặng cuộc sống, hi vọng sẽ được đến tay nhiều bạn đọc để chúng ta dần hoàn thiện bản thân mình nhé!!! 
5
110814
2013-04-10 18:49:21
--------------------------
67759
9082
Cuốn sách đã mở ra một chân trời mới trong tôi.Nó đã giúp tôi thay đổi thói quen xấu thành tốt bằng cách rèn luyện mỗi ngày.Mỗi ngày bây giờ đối với tôi là một niềm vui mặc dù phải dậy sớm và loại bỏ những thói quen xấu.Chính thói quen xấu đã đẩy ta vào những điều tưởng chừng vô hại nhưng dần dần theo thời gian nó chính là một con sâu, con sâu ăn vào tính cách và công việc hàng ngày, nó sẽ làm hư tất cả mọi chuyện.Hãy tin tôi đi, thói quen trong cuộc sống đóng một vai trò rất quan trọng, nó giúp tôi dần hòa nhập với những gì chúng ta đặt ra và những gì chúng ta muốn.
4
101421
2013-04-08 19:52:45
--------------------------
