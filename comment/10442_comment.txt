53702
10442
Tập 29 là tập đầu tiên mình được đọc của bộ truyện về chàng cao bồi Lucky Lucke. Ban đầu, mình mua cuốn sách này do bị ấn tượng với bộ phim hoạt hình chuyển thể, cũng như tên tuổi của tác giả Gosciny, một trong hai tác giả đã làm nên tác phẩm "Nhóc Nicolas" kinh điển. Và mình đã hoàn toàn hài lòng và thích thú với cuốn sách này. Một câu chuyện vô cùng thú vị, hài hước, dí dỏm, nhưng cũng đầy chất phiêu lưu đến những vùng đất xa xôi, rộng lớn. Chàng cao bồi là hình tượng nổi bật, được khắc họa rất chân thực và sống động, đầy màu sắc. Đây là một trong những tập truyện hay nhất mà mình được đọc.
4
20073
2013-01-04 16:37:12
--------------------------
