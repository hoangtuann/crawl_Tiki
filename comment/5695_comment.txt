375730
5695
Có nhiều tình tiết còn thiếu điều kiện được thay thế bởi giác quan còn chia sẻ đầy đủ những ước mơ thiếu thời mong có thể được chạm vào hay cảm nhận với nó hơn là biết trả lời cho bao nhiêu vốn giả định đã được hướng tới hiện thực hóa về khả năng và nhu cầu của độc giả song nó lại phải nhường chỗ cho sức mạnh tối cao mà phủ nhận nó càng làm cho nó trở nên song hàng cùng chúng ta đến khi đạt được ý muốn , truyện hơi dài dòng .
3
402468
2016-01-29 03:32:55
--------------------------
132015
5695
Ai đã là con người thì trong thân tâm luôn có những mong ước cho bản thân của mình dù nó rất nhỏ nhoi và không đáng kể . Tôi ấn tượng nhất ở trong truyện có một câu nói như thế này hạnh phúc là thứ mà ta luôn ao ước mặc dù trong mắt người khác nó chả có gì quan trọng cả . Tác giả biết cách xây dựng cốt truyện rất linh hoạt . Nghệ thuật tương phản đối lập giữa phần đầu và phần cuối của truyện . Tôi chưa thực sự hài lòng lắm về kết thúc của truyện này . Tác giả vẫn chưa giải đáp được hết thắc mắc của tôi . Có nhiều chit iết hay hấp dẫn nhưng cũng có nhiều chi tiết tẻ nhạt và nhàm chán . Nói chung truyện này chỉ dừng ở mức độ đọc cho vui chứ không xuất sắc lắm
3
337423
2014-10-29 08:39:30
--------------------------
