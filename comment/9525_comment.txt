476697
9525
Tôi đã biết đến nguyên lý 80/20 trước đó và tôi nhìn tựa đề này với mong muốn áp dụng nguyên lý 80/20 hiệu quả cho cuộc sống và công việc hằng ngày. Một số thông tin hữu ích được tác giả truyền tải - mà có thể áp dụng được, còn lại phần lớn thông tin là dẫn giải các vấn đề liên quan quá nhiều đến ngành tư vấn và tài chính; mà tôi cho rằng nó không thực sự hữu ích đến cá nhân. Những bài dẫn giải tương đối gần giống nhau với bối cảnh của thị trường nước Mỹ của 1-2 thập kỷ trước mà cá nhân tôi cho rằng không mấy phù hợp cho hiện tại. 
2
1271299
2016-07-22 10:15:20
--------------------------
371091
9525
Mình mua cuốn sách này với mong muốn được học hỏi những gì hay ho từ nguyên lý 80/20 để áp dụng vào công việc và khi đọc đến trang 200 thì những việc sách viết chỉ là nhai lại cái  nguyên lý 80/20 . Tất cả ví dụ hay phương thức một là vô cùng sơ sài hai là vô cùng lý thuyết khô khan và chỉ rọc khuôn cho việc kinh doanh và văn phòng rất là khá thiếu thực tế trong tất cả các lĩnh vực còn lại. Điều rút ra bổ ích từ cuốn sách chỉ dừng lại ở phần một . Đây là ý kiến cá nhân của mình xin cám ơn các bạn đã xem qua .
1
10605
2016-01-18 22:37:38
--------------------------
336522
9525
Đây là một quyển sách hay về Kinh tế, quyển sách này cung cấp cho chúng ta những kiến thức, "gen kinh doanh" thông qua ví dụ điển hình về những con người 80/20 và quá trình xây dựng sự nghiệp của họ. 
Tuy nhiên, bạn sẽ dễ dàng nắm nội dung quyển sách này nếu bạn đã có sẵn đôi chút kiến thức về kinh tế vì theo cảm nhận của mình, lời dịch của quyển này còn hơi khó hiểu.
Về hình thức, vừa nhìn vào đã thích. Bìa sách dày, bóng, rất đẹp, chất lượng trang in cũng rất tốt. Rất hài lòng về hình thức của quyển Con người 80/20 này. Đây là một quyển sách đáng được sở hữu và ngâm cứu. 
4
725822
2015-11-12 16:24:43
--------------------------
309140
9525
Nhìn qua tựa đề có lẽ nhiều người cũng hiểu được cuốn sách này đề cập tới vấn đề gì, đó là quy luật 80/20: 80% kết quả bắt nguồn từ 20% công việc thực sự hiệu quả. Tác giả khuyên người đọc nên đầu tư thời gian và công sức vào 20% công việc tạo ra 80% kết quả, tập trung vào công việc mình làm tốt thay vì lan man làm mỗi việc một ít. Tuy nhiên ví dụ trong sách lặp đi lặp lại hơi nhiều, nhiều lúc đọc hơi khó hiểu. Nhưng dù gì đây là cuốn sách rất đáng đọc, hướng người ta về làm việc hiệu quả để có kết quả tốt chứ không chỉ cần cù bù thông minh là được.
4
460557
2015-09-18 22:26:43
--------------------------
288093
9525
Mình đã mua cả bộ sách về nguyên lý 80 / 20 trong khi biết rằng nguyên lý này có thể chỉ viết ra 1 vài dòng. Tuy nhiên, biết và áp dụng được nguyên lý lại là một khoảng thời gian cần rèn luyện, tuy nhiên nó thực tế và đáng để bỏ công sức học tập.
Cuốn sách tập trung vào sự nổi trội của các cá nhân. Timothy Feriss đã giới thiệu phương pháp này trong cuốn Tuần làm việc 4 giờ cùng với nguyên lý Parkinson, tối ưu và giản tiện thời gian làm việc.
Nghĩa là thông minh hơn chứ không phải chăm chỉ hơn. Chăm chỉ là một hình thức lười biếng, lười suy nghĩ. Tập trung vào kết quả quan trọng là điều cuốn sách muốn nhấn mạnh.
4
37796
2015-09-03 12:08:03
--------------------------
287631
9525
Tôi quyết định mua quyển sách này sau khi đã đọc quyển "Nguyên lý 80/20", so với quyển sách trước đó thì quyển sách này viết chi tiết và thiết thực hơn rất nhiều khi tác giả đã tập trung đi sâu vào các khía cạnh của 80/20. Richard Koch nhấn mạnh về quan điểm của hầu hết người thành công đó là tối đa hóa lợi nhuận trong khi bỏ ra tối thiểu chi phí, ông mở rộng triết lí đó ra hầu hết các khía cạnh trong kinh doanh - từ việc phát triển ý tưởng cho đến sử dụng con người hay quản lí tài chính. Xin cảm ơn tác giả và Tiki vì quyển sách hay. 
4
644564
2015-09-02 21:55:25
--------------------------
268869
9525
Trong cái thời đại mà người ta đề cao sự "cần cù bù thông minh", " làm nhiều được nhiều", hầu hết chúng ta đều đã, đang và sẽ coi đó chân lí. Có lẽ bởi vì nó nghe có vẻ đúng và công bằng? Và thế là có lẽ chúng ta đang đánh mất dần sự hạnh phúc và tự do trong cuộc sống bởi những "chân lí" đó. Tôi không phải là ngoại lệ. Nhưng đó chỉ là trước đây thôi. Sau khi đọc quyển sách này, tôi đã rút ra được những cốt lõi thật sự cần thiết cho cuộc đời mình. Những người nghe qua thì có thể nghĩ đây là sự làm biếng. Không, đây là sự sáng tạo và biết điều khiển cuộc sống đích thực của mình. Làm ít được nhiều. Nghe thì có vẻ vô lí, nhưng đối với tôi và có lẽ với cả nhiều người khác nữa, đó chính là chân lí. 
5
620881
2015-08-16 21:47:24
--------------------------
222728
9525
Trước đây khi tìm hiểu về cách quản lý thời gian hiệu quả trên mạng internet, tôi cũng đã vô tình đọc được một câu ngắn gọn về "nguyên lý 80/20" nhưng lúc đó tôi vẫn chưa nhận thức được tầm quan trọng của nguyên lý này.
Mãi đến hơn 1 năm sau, khi tôi tìm được quyển sách này, tôi đã khám phá ra được nhiều điều thú vị về nguyên lý 80/20. Richard viết 3 quyển về nguyên lý này trong đó quyển "Con người 80/20" tập trung nói về những con người đã áp dụng thành công nguyên lý tuyệt vời này vào cuộc sống, một cách vô tình hay hữu ý, và đem lại những kết quả vượt ngoài mong đợi.
Qua những câu chuyện của họ, bạn sẽ hiểu hơn về nguyên lý 80/20 cũng như cách áp dụng chúng vào mọi mặt cuộc sống. Bạn sẽ thấy rõ sự thay đổi về hiệu quả cũng như năng suất làm việc của bản thân. Có thể nói việc phát hiện ra nguyên lý này đúng là một điều tuyệt vời.
4
541710
2015-07-06 09:07:42
--------------------------
198830
9525
Nội dung cuốn sách là những câu chuyện về những con người thành công nhờ áp dụng nguyên lí 80/20 một cách vô tình hoặc có thông qua nghiên cứu kỹ lưỡng cách thức vận động của thế giời. Những người thành công này biết những gì là quan trọng đối với họ, những gì đang làm mất năng lượng của họ mà họ vất chúng đi. Nhờ áp dụng nguyên kí 80/20 mà những con người này biết tập trung vào những gì đem lại có giá trị nhất và tốn ít thời gian và công sức nhất, đó là những người nắm được nguyên lí. Những người ngoài kia không có những suy nghĩ này thì hàng ngày vẫn khó khăn để kiếm sống, họ chỉ khác nhau ở một chỗ, đó là suy nghĩ và vận dụng nó vào cuộc sống của bản thân mình. Những bài học, những câu chuyện bổ ích, những tấm gương rất đáng để bản thân tôi học tập và vận dụng phương pháp này vào ngay chính cuộc sống của mình một cách phù hợp nhất với bối cảnh của đất nước ta! cám ơn :D
4
636713
2015-05-20 21:34:15
--------------------------
171799
9525
Con người 80/20 là quyển sách mà chúng ta nên đọc sau khi đọc xong nguyên lý 80/20 nhằm tìm hiểu thêm về nguyên lý này. Bạn sẽ trở thành một con người 80/20 sau khi đọc xong quyển sách này và luôn thực hiện theo nguyên lý đơn giản nhưng dễ hiểu này. Một con người sẽ tập trung vào 20% công việc trong hệ thống, những công việc sẽ mang lại 80% thành công cho công ty, tổ chức. Quyển sách in trên nền giấy ố vàng, làm tôi có cảm tưởng đang cầm trong tay một quyển sách cũ lâu năm. Mà đối với những người yêu sách thì sách càng cũ càng có giá trị.
5
387632
2015-03-22 16:42:58
--------------------------
74025
9525
Trước đây tôi được nghe nhiều về nguyên lý 80/20. Nhưng khi tìm hiểu về nó, đặc biệt khi tôi gắn nhãn quan 80/20 lên mọi vấn đề trong đời sống hằng ngày của tôi, tôi thấy quả là vậy. Đặc biệt, nếu bạn làm trong lĩnh vực kinh doanh bạn sẽ thấy rõ điều đó. Cuộc sống của chúng ta cũng vậy, và con người chúng ta cũng thế. Vận động theo quy luật này. 
Trong cuốn sách này, chỉ rõ chín yếu tố tạo nên 20% nỗ lực để tạo ra 80% thành công của chúng ta. Cuốn sách chỉ dẫn từ những ví dụ thực tế, sinh động trong công việc của bạn để dẫn thới thành công. Đây là cuốn sách nên có của mọi người, vì nó chính là nguyên lý cuộc sống!
4
5246
2013-05-11 10:22:02
--------------------------
