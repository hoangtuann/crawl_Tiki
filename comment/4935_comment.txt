493432
4935
Về nội dung và hình thức trình bày mình rất tốt, đẹp nhưng điểm trừ của nó là tên các địa danh và tên người được phiên âm ra tiếng việt.
3
94191
2016-11-30 11:36:36
--------------------------
459257
4935
Quả nhiên là một cuốn tiểu thuyết kinh điển, tác giả đã mang đến cho người đọc một Paris vừa xa hoa tráng lệ vừa tối tăm, man rợ, một câu chuyện đầy bi kịch, một xã hội toàn những con người đáng sợ, lấy nỗi đau của người khác làm niềm vui. Đoạn đầu miêu tả khá kỹ về kiến trúc nên mình cảm thấy khá khó đọc vì vừa đọc vừa cố tưởng tượng. Mỗi nhân vật trong truyện đều có một tâm hồn méo mó, cho dù có vì tình yêu hay không (trừ cô gái thì lại quá ngây thơ, đến mức ngu ngốc, nhưng cũng dễ hiểu bởi cô mới 16 tuổi). Nội dung tuyệt vời, bìa giấy cứng, thiết kế đẹp nhưng không rõ vì lỗi in hay sao đó mà thỉnh thoảng lại có đoạn bị lặp lại trong sách, cũng có vài lỗi chính tả nên là dù sách rất hay nhưng chỉ 4* thôi   
4
782605
2016-06-25 22:31:04
--------------------------
450676
4935
Cùng với "Những người khốn khổ", "Chín mươi ba", "Lao động và biển cả"; "Nhà thờ Đức Bà Paris" đã góp phần làm nên tên tuổi và tài năng của Victor Hugo trong mảng sáng tác tiểu thuyết, bên cạnh thơ và kịch. Tiểu thuyết có cốt truyện đơn giản, dễ hiểu với những môtip quen thuộc của văn học lãng mạn. Điểm đặc sắc nhất của tác phẩm chính là chất Grotesque (cái thô kệch) cùng thủ pháp tương pháp sử dụng rất thành công. Đó là sự đối nghịch giữa cái đẹp và cái xấu, giữa cái cao cả và cái nhỏ mọn, giữa cái thiện và cái ác. Đó là câu chuyện tình khá rắc rối giữa 4 nhân vật: Quazimodo - người có trái tim nhân hậu, thánh thiện dù bên ngoài là một thằng gù xấu xí; Frollot - tên phó giáo chủ từ cái thiện đã bị những cám dỗ tha hóa thành "con quỷ" với bản chất xấu xa; Foebus - gã kỵ sĩ đẹp mã nhưng là một tay lưu manh, đàng điếm, thô bạo và nàng Esmeralda xinh đẹp, có trái tim nhân hậu, đã cảm hóa được Quazimodo. Một câu chuyện bi kịch nhưng không bi kịch, những trái ngang cuộc đời, bất công xã hội được Victor Hugo đề cập đến rất rõ thông qua tiểu thuyết này. Đồng thời, bạn đọc sẽ phải trầm trồ trước tài miêu tả bậc thầy của Victor Hugo từ nhà thờ Đức Bà Paris cho đến không khí của những buổi lễ hội, thiên nhiên,... Tóm lại, đây là một tác phẩm rất đáng đọc.
5
60352
2016-06-19 02:47:24
--------------------------
449847
4935
Tôi rất thích những tác phẩm của nhà văn Victor Hugo bởi vì nó như tái hiện lại cho tôi 1 khung cảnh lịch sử. Nhiều khi tôi đọc nó lôi cuốn đến nỗi là tôi tưởng những câu chuyện này đang diễn ra trong đầu. Khi đọc sách trong đầu của tôi hiện ra một nền kiến trúc độc đáo, và những cái bi kịch của con người trong xã hội phong kiến. Nó dường như chạm vào trái tim người đọc. Ngoài ra bìa sách cứng, được in rất đẹp mắt. Và  theo tôi thì tôi rất thích cuốn sách này
5
1171886
2016-06-18 08:08:14
--------------------------
419001
4935
Về hình thức: cuốn sách rất đẹp, bìa cứng chắc chắn, chất liệu giấy khá tốt. Nói chung mình không tiếc khi mua cuốn sách này.
Về nội dung: như chúng ta đã biết sách của ông không hề dễ đọc và cuốn Nhà thờ đức bà Pari cũng vậy. Tôi ấn tượng với nhân vật nữ chính, yêu sự trong sáng thánh thiện của cô nhưng đồng thời không tán thành sự mù quán, yêu Poebus một cách ngu ngơ như vậy. Còn anh gù tôi trân trọng anh vì sự lặng thầm luôn bên cạnh bảo vệ người mình yêu và trả thù cho cô... Các đoạn tả cảnh trong truyện thật đặc sắc, quả không xứng danh ngòi bút của một đại văn hào.
5
1131432
2016-04-20 13:21:11
--------------------------
294707
4935
Tôi chưa bao giờ có ý định sẽ đọc "Nhà thờ Đức Bà Paris", vì nghe người ta bảo rất khó đọc. Cho đến khi, trong một buổi giảng, thầy tôi đã kể cho chúng tôi nghe về tác gia Victor Hugo, và về Quasimodo của ông. Tôi bắt đầu có những tò mò về một chàng gù hèn mọn, xấu xí, nhưng có tình yêu trong sáng, cao đẹp hơn rất nhiều những kẻ đẹp đẽ sang quý trên đời. Tôi lần tìm lên mạng nghe bài hát "Bella", bị hớp hồn bởi giai điệu và cảm xúc. Cuối cùng, tôi mới đọc "Nhà thờ Đức Bà Paris". Ấn tượng đầu tiên của tôi khi mở quyển sách ra là khá nặng và dày. Với một đứa quen cầm mấy quyển ngôn tình dù dày đến đâu vẫn nhẹ tênh như tôi mà nói, quả thực có chút khó khăn. Cốt truyện lại không liền mạch, nhiều khi hơi sa đà vào miêu tả kiến trúc, nên tôi đọc những chương đầu khá chậm. Dần dà, tôi lại bị hút vào câu chuyện từ bao giờ không hay. Tôi thương Quasimodo, tôi trách Frollo, tôi khinh Phillip ... Rất nhiều cảm xúc dâng lên trong tôi. Tác phẩm này thật sự đã đưa tôi đến với xã hội đen tối của Paris thời Trung Cổ đen tối. Tôi nhìn thấy những con người, họ bằng xương bằng thịt, họ xấu xa có, ích kỷ có, nhưng vượt lên trên tất cả, đó là tình người và trái tim cao thượng mà người dành cho người.
5
453691
2015-09-09 16:45:41
--------------------------
282683
4935
Tác phẩm xứng đáng với hai chữ "kinh điển". Đọc "Nhà thờ Đức bà Paris" bạn không chỉ thán phục kiến thức về kiến trúc, văn hóa, lịch sử của tác giả mà còn vì tính nhân văn của tác phẩm. Bằng ngòi bút điêu luyện, Victor Hugo đã miêu tả chân thực một xã hội Pháp thời trung cổ đầy giả tạo, dối trá, khinh bỉ, sự phân biệt giai cấp đã đẩy một con người xuống tận đáy của xã hội, một bi kịch về tình yêu tay ba và kết thúc là cái chết của người con gái. Sách có bìa cứng, khổ lớn, gáy sách chắc chắn,  giấy cứng để dán bìa bên trong màu đỏ rất nổi và hợp với thiết kế tông tối của bìa. Cuốn sách khá dày mà bìa cứng nữa nên với giá tiền này mình thấy rất phải chăng.
5
719449
2015-08-29 12:43:06
--------------------------
279424
4935
Cuốn tiểu thuyết lãng mạn Victor Hugo của những niềm đam mê bóng tối và tình yêu đơn phương. Nhà thờ đức bà Paris là một tác phẩm kinh điển hoàn mỹ. Trong tháp Gothic hình vòm của Nhà thờ tuyệt đẹp nằm ở kinh đô Paris sầm uất lại có một người đánh chuông bị gù lưng. Bị chế nhạo và xa lánh với sự xuất hiện của mình, ông là kẻ đáng thương. Esmerelda, một vũ công xinh đẹp đã thu hút được sự chú ý của người đánh chuông gù lưng. Tuy nhiên, cũng đã thu hút được sự chú ý của các phó giám mục Claude Frollo nham hiểm. Cuốn tiểu thuyết Victor Hugo mang lại sự sống cho Paris thời trung cổ mà ông yêu thương, và thương tiếc khi nó đi qua khoảng thời gian ấy. Nhà thờ đức bà Paris là một trong những tiểu thuyết lãng mạn lịch sử vĩ đại nhất của thế kỷ XIX. Cuốn tiểu thuyết là niềm đam mê của những ý tưởng, miêu tả rõ ràng về kiến ​​trúc Gothic và của một nền dân chủ đang phát triển, và chứng minh rằng bên ngoài xấu xí không có nghĩa là họ không có vẻ đẹp đạo đức ở sâu bên trong.
5
424154
2015-08-26 19:55:41
--------------------------
241267
4935
Mình còn nhớ lần đầu tiên được tiếp xúc với tác phẩm này là qua bộ phim hoạt hình:" Thằng gù ở nhà thờ Đức Bà". Và có lẽ đúng như người ta đã nói:" Xem phim mãi mãi không hay bằng đọc truyện". Nếu như qua bộ phim ta có thể cảm nhận được sự đau khổ của thằng gù bị xã hội ruồng bỏ thì khi đọc truyện ta càng thấm thía hơn nỗi đau thật sự của 1 sinh linh bị chối bỏ. Về bản chất Thì Quasimodo là 1 người tốt, đã hi sinh hết mình cho tình yêu thầm lặng của mình. Tuy nhiên chỉ vì được sinh ra với hình thù không giống ai mà anh đã phải suốt đời chịu sự khinh rẻ của người đời trong khi anh vẫn là 1 con người. Có lẽ ngoài thông điệp phản ánh xã hội Pháp thời bấy giờ thì Hugo vẫn muốn con người chúng ta đối xử bình đẳng với nhau. Cho dù ta có thể khác biệt về màu tóc, nước da, quan điểm xã hội nhưng vẫn là con người và không thể phân biệt họ vì 1 điều mà họ không muốn bởi không có nỗi đau nào hơn nỗi đau không được đồng loại chấp nhận cả. Đọc xong mình cảm thấy thật may mắn khi đã được sinh ra đầy đủ và hoàn thiện và cũng phần nào cảm thấy xót thương cho những số phận không may.
5
39383
2015-07-25 12:29:15
--------------------------
224687
4935
Tiểu thuyết của Hugo luôn đặc biệt vì lượng triết lý nó bao hàm, văn phong hoàn mỹ và những nhân vật khó quên trong lòng độc giả. Thằng gù và cô gái Esmeralda xinh đẹp chỉ là một trong vô vàn mảnh ghép của bức tranh kiến trúc, xã hội, chính trị và công lý của Paris trong thời kỳ đầy biến động được lột tả xung quanh một hình tượng chủ đạo: Nhà thờ Đức Bà. Có nhiều trường đoạn và tình tiết truyện có thể làm người đọc phổ thông bối rối - vì độ dài và sự không liền mạch. Nhưng nếu kiên nhẫn, độc giả sẽ được thưởng thức một trong những tác phẩm văn học đặc sắc nhất, khó quên nhất của đại văn hào người Pháp: Victor Hugo.
5
598425
2015-07-09 06:31:38
--------------------------
210933
4935
Đây là cuốn sách văn học nước ngoài đầu tiên mình đọc. Hình như lúc đầu đọc chưa quen nên thấy rất khó hiểu, và khó hình dung được hình ảnh mà tác giả miêu tả trong truyện. Tuy nhiên, càng về sau càng hay, cốt truyện rất tuyệt vời. Lột được bản mặt của xã hội Pháp trước Cách Mạng. Mình không thích kể nội dung truyện ra vì nó sẽ làm giảm sức hấp dẫn. Nhưng cũng muốn nói một chút về nó. Nội dung không được liền mạch lắm nên hơi khó hiểu, tự dưng đang kể về Gringoa ở quyển một, rồi Gringoa biến mất trong mấy quyển sau, rồi lại xuất hiện tiếp nữa, chen vào những cái đó là một bối cảnh hoàn toàn khác, nhân vật hoàn toàn mới, tuy nhiên cuối cùng kết thúc thì lại xuất hiện với nhau. Đó là một chi tiết khá đặc biệt trong cách viết văn của Hugo. Mình thấy sách rất hay, các bạn nên mua về đọc
5
394591
2015-06-19 22:25:08
--------------------------
188547
4935
Rất thích các tác phẩm văn học nước ngoài và đặc biệt là văn học Pháp. Ngoài quyển Ba chàng lính ngự lâm nổi tiếng lẫy lừng thì Nhà thờ đức bà Paris cũng nổi không kém. Tôi đã từng rất thích tác phẩm Những người khốn khổ của Victor Hugo và giờ là đến Nhà thờ đức bà Paris. Tác phẩm là một cuốn tiểu thuyết lãng mạng nhưng có sự liên kết rất thực tế với hiện thực chứ không phải là một cuốn tiểu thuyết cổ tích. Tóm lại tác phẩm này là một minh chứng cho những mảnh đời phải chịu sự khổ cực của sự thống trị tàn bạo, giúp ta biết thêm về lịch sử, con người nơi đất Pháp xa xôi, nhưng lại gần ngay trước mắt khi ta khám phá tác phẩm. Bìa giấy cứng nên rất chắc chắn và thiết kế rất đẹp tạo cho cuốn sách một vẻ huyền bí lạ lùng. 
5
602873
2015-04-25 17:31:22
--------------------------
168472
4935
Cũng như tác phẩm Những người khốn khổ, Victor Hugo một lần nữa lột tả được hết cái cùng cực ,cái bi kịch của người dân Pháp. Tuy vẫn có những nét ấm áp trong cái bi kịch ấy, nhưng thật sự tôi đã sợ hãi và đau xót khi đọc Nhà Thờ Đức Bà Paris, những cái chết rùng rợn, đau đớn, xót xa khiến tôi như sống trong cái xa hội thối nát, tàn nhẫn ấy.
Đây cũng là một tác phẩm nên đọc đối với những bạn muốn tìm hiểu lịch sử nước Pháp. Hugo vừa nêu nên cuộc sống nhân dân, vừa nếu nên lịch sử Pháp. Bìa sách cũng rất đẹp, giấy in tốt
5
525123
2015-03-16 18:58:04
--------------------------
157865
4935
Một quyển tiểu thuyết đặc sắc không chỉ về nội dung mà còn về "hình thức" của nó. Victor Hugo đã lột tả hoàn toàn khác biệt về đời sống nhân dân nước Pháp lúc bấy giờ. Cuốn này theo tôi thì nó hoàn toàn là bi kịch nhưng thỉnh thoảng có pha 1 chút hài hước không đến nỗi rút cạn nước mắt của độc giả như Những người khốn khổ. Tuy viết theo chủ nghĩa văn học lãng mạn thế nhưng trong lãng mạn lại xen lẫn yếu tố hiện thực. Dựa vào lịch sử để viết một cuốn tiểu thuyết lãng mạn mà vẫn cho độc giả thấy được thực chất khốn cùng của xã hội nước Pháp, chỉ có Victor Hugo mới có đảm lược đó. đây là một cuốn tiểu thuyết kinh điển đáng để đọc và nghiên cứu. cá nhân tôi vừa đọc vừa suy ngẫm và tôi đã cho ra kha khá bài học hay. Mọi người nên mua và đọc thử đi, lựa chọn không tồi chút nào
4
471002
2015-02-10 16:35:26
--------------------------
