406545
3168
Thoạt đầu đọc tập "Bí kíp xuyên tạc chuyện rồng" tôi những tưởng tập này có phần đuối hơn các tập trước, nhưng không, đoạn cao trào đã bứt phá một cách ngoạn mục, đỉnh của đỉnh luôn. Cái đoạn mà Nấc Cụt bị dung nham đuổi sau đó rơi xuống biển và con Rồng Lửa nở ra ấy, đúng là quá hoành tráng luôn. Và Hăng Rết tập này cũng hay phá bĩnh rồi gây rắc rối các kiểu nhưng cuối cùng thì cũng lập được công. Một tập truyện thật hết sảy, có đủ mọi cung bậc cảm xúc. Tôi thiết nghĩ Bí Kíp Luyện Rồng xứng đáng là một trong những bộ fantasy hay nhất mọi thời đại.
5
386342
2016-03-28 15:37:20
--------------------------
297461
3168
"Bí Kíp Xuyên Tạc Chuyện Rồng" là cuốn thứ 5 trong seri 9 tập Bí Kíp Luyện Rồng, cũng như những tập trước, tập truyện lần này rất vui nhộn nhưng không kém phần kịch tính, cuốn truyện hấp dẫn, làm rung động cả những độc giả khó tính nhất vì những tràng cười vô cùng sảng khooái. Trong khi hòn đá lửa bị đánh cắp, núi lửa sắp phun trào đến nơi và cả những con rồng Hỏa Diệt đang lăm le đe dọa nữa (minh họa ngay trang bìa là em rồng khủng bố này mà sao nhìn điệu đà và dễ thương quá luôn!??), trong lúc nước sôi lửa bỏng Hăng Rết đã lập được công lớn, Nấc Cụt rất cam đảm và kiên quyết cho dù có lúc rơi vào thế bí. Nấc Cụt là một người Viking vĩ đại!
Truyện ngày càng hay, nhưng giá ngày càng đắt, mà mình mua xong 2 hôm sau Tiki giảm giá 20% xuống 25%, thiệt là!!!
5
68108
2015-09-11 21:24:32
--------------------------
276929
3168
Wow! Cuốn sách này được xuất bản đúng sinh nhật mình nè.
Càng những tập về sau thì cuộc phiêu lưu của Hiccup càng trở nên khó khăn và nguy hiểm hơn. Những lúc cứ tưởng là đã kết thúc trong tuyệt vọng thì bất ngờ lại xuất hiện. Thiệt đau cả tim. Trong tập này Toothless của chúng ta lập được chiến công cực lớn nha. Toothless trong truyện đáng yêu ở chỗ bình thường rất cứng đầu và ích kỷ, và nhát nữa, nhưng khi Hiccup bị nạn hoặc cần được giúp đỡ thì Toothless không bao giờ bỏ rơi người bạn đồng hành của mình (có lẽ một phần lý do là vì không ai cưng chiều Toothless được như Hiccup cả ^ ^).
5
707382
2015-08-24 15:06:09
--------------------------
242640
3168
Lần thứ năm gặp gỡ nhân vật nồi tiếng Nâc Cụt Horendous đệ Tam- Kiếm sĩ thần sầu và người nói tiếng rồng của bộ tộc Ngang Tàng Lông Lá thời thần Thor, tôi vẫn không khỏi bất ngờ về sự tháo vát và lòng nhiệt thành, dũng cảm hơn người của cậu. Tác phẩm  gây cười trên từng trang truyện cùng những minh họa hài hước của chính tác giả đã đem đến cho tôi những giây phút vui vẻ, bất ngờ, cười ngặt nghẽo mà cũng không khỏi cảm động rớt nước mắt. Một series rất đáng đọc.
5
292668
2015-07-26 20:41:24
--------------------------
213135
3168
Đọc nghe qua cái tên có vẻ gì đó rất cuốn hút, mạnh mẽ. Quyển sách nay tạo được điểm nhấn đầu tiên khá ấn tượng đối với đọc giả.
Con người sinh ra là để phiêu lưu, khám phá, chinh phục những khó khăn, thử thách. Quyển sách như tiếp thêm một ngọn lửa nhiệt huyết, say mê cho người đọc!
Nội dung quyển này khá hấp dẫn, văn phong cũng hết sức lôi cuốn, tạo được nhiều cảm xúc.
Thuộc thể loại phiêu lưu, viễn tưởng nhưng cầm trên tay quyển sách cả thế giới ảo đó sẽ từ từ hiện ra như một cuộc sống thật!
4
309747
2015-06-23 13:49:54
--------------------------
