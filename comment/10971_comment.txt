568949
10971
Đại dương khó thương, đọc rồi sẽ thương. Sách có cách viết hài hước, dí dỏm, hình minh họa vui nhộn. giấy màu ngà vàng tương đối tốt, sách không quá dày, vừa khổ tay nhưng có thể bao quát được kiến thức căn bản về đại dương và các sinh vật dưới biển. Mình cảm thấy cuốn sách này rất đáng để xem vì cách viết văn độc đáo, hài hước, không nhàm chán. Sách viết về những sự kiện chấn động trong lịch sử, những sinh vật ở dưới biển những lí giải khoa học cho các hiện tượng và một số điều lí thú về đại dương mà chúng ta chưa biết.
5
1790965
2017-04-10 10:44:06
--------------------------
456936
10971
Sách này rất hay và thú vị cách diễn đạt ngôn ngư đan xen với các hình ảnh ngộ nghĩnh giúp các bé nhà tốt rất thích thú và tiếp thu rất nhanh .Mang lại cho các bé một khoảng kiến thức mới về thế giới xung quanh . Ngoài đại dương khó thương thì tôi cũng đã mua cho con mình cuốn sinh học gì đó của  cùng một tác giả . Cuốn này cũng rất hay và hấp dẫn hình như series các cuốn khoa học này có tới mấy phần phần nào tôi cũng thấy hay và bổ ích tôi khuyên các bạn nên mua
5
661909
2016-06-23 21:30:27
--------------------------
436213
10971
Đọc cuốn sách này ta sẽ biết thêm về thứ " không có điểm bắt đầu cũng không có điểm kết thúc nhưng chạm tới mọi lục địa " về những tên hải tặc đáng sợ với những luật lệ lạ đời và hơn thế nữa còn có cả " hồ sơ mật " với những vụ cướp táo tợn. Về sông nước khắp 7 biển mênh mông rộng lớn. Về những vụ mất tích bí ẩn của tàu thuyền chưa có lời giải thích tại tam giác quỷ huyền bí nơi đại dương rộng lớn và những lí giải khoa học hay thuyết âm mưu xung quanh những vụ mất tích. Về chuyến đi định mệnh đầu tiên và cũng là cuối cùng của con tàu Titanic huyền thoại và những nghi vấn xung quanh xác ướp có biệt danh " Đắm Tàu " thứ được cho rằng đã xuất hiện trên tàu Titanic khi đắm.... Tóm lại biển cả thật mênh mông, bao la và rộng lớn và những điều ta biết về nó cũng chỉ nhưng vài chiếc vỏ sò trên bãi biển trải dài vô tận
5
1337568
2016-05-26 13:11:11
--------------------------
378776
10971
Biển cả, đại dương luôn là một chủ đề thú vị và không bao giờ hết chuyện để nói, hiểu biết của chúng ta về biển cả có khi còn ít hơn những gì chúng ta hiểu được về vũ trụ và khả năng thám hiểm biển cả của chúng ta cũng vẫn còn có hạn nhưng mình rất yêu thích các bộ phim tài liệu về biển và cũng mua thêm cuốn này để tìm hiểu thêm và rất thích, qua cuốn sách mình biết thêm được rất nhiều về đại dương không chỉ các số liệu nhàm chán thông thường khó nhớ mà còn có thuyết minh hài hước và những hình minh họa thông minh dễ hiểu, rất thích cuốn sách này.
5
306081
2016-02-07 14:03:09
--------------------------
325174
10971
KTHD - Đại Dương Khó Thương là quyển sách viết về đại dương, những tên cướp biển, những sinh vật sống trong biển cả bao la. Nhất là khi đọc quyển sách này mình biết được thêm về vụ đắm tàu Titanic trong lịch sử, mình có nghe nói về bộ phim này nhưng chưa xem, giờ đọc quyển sách này mình mới hiểu rõ. Đã vậy sách còn cho mình biết thêm về những con tàu bị mất tích bí ẩn trong tam giác quỷ nổi tiếng thế giới, thật đáng sợ. Quyển sách thật sự giúp mình mở mang thêm đầu óc
5
402581
2015-10-22 22:31:40
--------------------------
309447
10971
Từ nhỏ khi được bố mẹ dẫn đi biển chơi thì lúc nào cũng chỉ nghĩ là :"Biển chỉ có nước và cát vậy thôi à? ".Lớn lên thì mới biết được biển chỉ là một phần của cả đại dương bao la.Rồi tình cờ bắt gặp cuốn sách này, cũng vì muốn tìm hiểu để hiểu biết nhiều hơn nữa về đại dương bao la kia, mình đã bỏ ngay cuốn sách vào giỏ hàng.Và không uổng công chờ đợi,cuốn sách đã mở ra cho mình một cuộc thám hiểm vào đại dương xanh thẳm.Đại dương không chỉ có nước và cát, nó còn là một khối tài nguyên tôm,cá,...khổng lồ và những bí mật sâu thẳm dưới tận sâu đáy đại dương mà các nhà thám hiểm vẫn chưa khám phá hết được.Đây là cuốn sách thật sự bổ ích.  .
5
57884
2015-09-18 23:43:52
--------------------------
298750
10971
Đến với cuốn sách này bạn sẽ được tìm hiểu mọi thứ về đại dương sân thẳm với những sinh vật có cả những con lạ hoắc, đáng sợ mà bạn không thể biết dưới đáy biển sâu. Ngoài ra cuốn sách còn nói về những con tàu bí ẩn biến mất ở vùng tam giác quỷ nổi tiếng, rồi đến những kẻ cướp biển hung tợn nữa. Mình cực kì thích phần  nói về chuyến tàu titanic nổi tiếng ngày xưa. Phải nói là bộ sách Horrible geography cực kì bổ ích, tìm hiểu về mọi thứ trên đời ừu toán học, sinh học, hóa học, vật lý đến những đề tài như thiên nhiên, vut trụ. Cực kì bổ ích
4
381629
2015-09-12 20:57:55
--------------------------
284676
10971
Đọc quyển sách này giống như mình đang có một chuyến du lịch dưới lòng biển cả vậy. Từ những loài hiền lành đến những loài hung dữ, những tên cướp biển ... đủ thứ về biển cả. Như lạc trên biển thật vậy, truyện còn có những tranh ảnh minh hoạ thú vị nữa, tuy nét vẽ đơn giản nhưng lại vui nhộn bổ sung rất nhiều cho nội dung, làm nội dung không nhàm chán mà còn sinh động nữa. Mình thật sự khâm phục tác giả về lượng kiến thức của ông, ông biết nhiều thứ và cũng mang lại cho bạn những kiến thức mà ông biết đó.
4
153585
2015-08-31 10:01:46
--------------------------
279400
10971
Khi nhận đươc sách về, mình xem thử sản phẩm ngay, bìa sách đẹp, bắt mắt người đọc, giấy được in bằng chất liệu tốt nên cảm thấy rất thích.
Đọc ''Đại dương khó thương'' quả thật rất hay, cho bạn cảm giác giống như đang tự mình khám phá mọi thứ dưới lòng biển. Những thông tin bổ ích cùng lời kể tự nhiên, dí dỏm khiến chúng ta dường như bị cuốn hút vào những câu chuyện. Mình sẽ tiếp tục ủng hộ series ''Horrible... "

5
613975
2015-08-26 19:26:44
--------------------------
254304
10971
Đại dương không phải là nơi cho những kẻ nhát gan mà là nơi cho những chiến binh dũng cảm đang giành giật sự sống từng giây, từng khoảnh khắc.Ranh giới của sự sống và cái chết ư, chỉ cần tợp một phát, tất cả sẽ chấm dứt.Đây không chỉ là một cuốn sách khoa học, nó còn là một cuốn truyện li kì, hấp dẫn đến từng chi tiết. Học mà chơi, chơi mà học, rùng rợn nhưng không kém phần dí dỏm, cuốn sách này đã đem lại một cái nhìn mới về cuộc sống trong đại dương bao la rộng lớn mà chúng ta chưa từng biết đến.
5
280592
2015-08-05 09:55:53
--------------------------
249871
10971
Đại dương khó thương, đọc rồi sẽ thương. Sách có cách viết hài hước, dí dỏm, hình minh họa vui nhộn. giấy màu ngà vàng tương đối tốt, sách không quá dày, vừa khổ tay nhưng có thể bao quát được kiến thức căn bản về đại dương và các sinh vật dưới biển. Mình cảm thấy cuốn sách này rất đáng để xem vì cách viết văn độc đáo, hài hước, không nhàm chán. Sách viết về những sự kiện chấn động trong lịch sử, những sinh vật ở dưới biển những lí giải khoa học cho các hiện tượng và một số điều lí thú về đại dương mà chúng ta chưa biết.
4
295212
2015-07-31 23:08:14
--------------------------
241045
10971
Mình cảm thấy hài lòng về bộ sách "Horrible Geography" này. Nó đã cung cấp cho mình rất nhiều những thông tin khóa học thú vị, tuy là sách khoa học nhưng không hề nhàm chán, khô khan với hình ảnh minh họa hài hước và cả lối hành văn của tác giả cũng như vậy, rất dễ tiếp thu. Những tên cướp biển hung tợn, những con tàu huyền thoại, những chú cá độc đến tắt thở và những con cá mập trắng khổng lồ rùng rợn, rong biển không chỉ dùng để ăn mà còn có rất nhiều công dụng bất ngờ khác. Mình đã định "rinh" bộ sách này về nhà từ lâu rồi.
5
649807
2015-07-25 09:25:01
--------------------------
240745
10971
Đúng là rất khó thương - đại dương với những bí ẩn kinh hoàng, những loài vật ghê gớm, những vụ đắm tàu mất tích chấn động địa cầu. Thế nhưng, đại dương cũng rất dễ thương với bạt ngàn hải sản ngon lành, rất dễ mến với những con sóng đưa chuyến tàu du hành đến những vùng đất mới. Cùng với lòng biển bao la là khao khát chinh phục thiên nhiên của con người. Nhưng bên cạnh đó, con người cũng đang phá hoại biển cả, làm tuyệt chủng nhiều loài sinh vật biển quý hiếm. Bằng ngôn ngữ vui nhộn và hình ảnh sinh động, quyển sách đem đến cái nhìn mới mẻ về biển cả, một phần rất lớn của trái đất. Tất cả những lí thuyết khô khan bị đánh bay, thay vào đó là một hình ảnh biển khác lạ, vui vẻ, gần gũi với chúng ta.
5
120276
2015-07-24 21:47:44
--------------------------
230468
10971
Mình khá thích bộ sách Địa Lý Rùng Rợn này, hết lên núi tuyết, thì lại lên núi lửa, hết đến đảo hoang lại lại chạy về hoang mạc,... rất rất nhiều nơi, mà nơi nào cũng độc và lạ. 
Đại Dương Khó Thương cũng là một hành trình lạ và độc, mặc dù trước đó, ít nhiều mình cũng được biết qua sách vở, báo tin,...
Lặn xuống đại dương ta thấy nhìn? Những loài cá đẹp mê cùng những loài thực vật kỳ lạ. Những cuộc săn đuổi dưới biển.
Hay nếu chán thì ngoi lên bờ để rồi giật mình khi bị trôi đến vùng Tam giác quỷ hay nếu may mắn leo được lên thuyền thì lại... đụng phải cướp biển?
Tóm lại, nếu bạn ưa khám phá, thích địa lý và muốn giải trí thì quyển sách nhỏ này sẽ là một lựa chọn không tồi đâu. 
5
382812
2015-07-17 15:01:12
--------------------------
217786
10971
Đại dương nhìn ở phía trên thì trông thật hiền hòa nhưng có mấy ai biết rằng ở dưới mặt nước êm ả đó là một cuộc sống dữ dội đến thế nào! Qua những trang sách với lời văn dí dỏm, dễ hiểu , hình ảnh minh họa sinh động , ngộ nghĩnh, những thông tin bí ẩn đến bất ngờ ,… sẽ đưa chúng ta đi xuống dưới lòng đại dương sâu thẳm và đầy rẫy nguy hiểm ấy. Cuốn sách này sẽ giúp cho chúng ta hiểu một cách đơn giản nhưng cũng thật rõ ràng về đại dương khó lòng thương được. Một cuốn sách bổ ích nhưng cũng không kém phần vui nhộn sẽ khiến bạn phải cười khi đọc qua nó.
5
554214
2015-06-29 22:47:48
--------------------------
199540
10971
Tuy rằng là một cuốn sách khoa học, nhưng cách diễn giải nội dung lại thiên về phong cách dỉ dỏm, ngay cả những hình vẽ trong sách cũng rất buồn cười. Điều thú vị nhất là cuốn sách đã chọn lọc đưa ra những nội dung gây chú ý, gây tò mò  như về tam giác quỷ - nên tuy rằng cuốn sách khá là nhiều chữ nhưng đọc không hề chán. Cách dùng từ cũng phổ thông, nên trẻ tiểu học cũng có thể đọc được (chỉ cần có người lớn đọc giúp những tên địa danh tiếng Anh là được!). Một thiếu sót nho nhỏ là cách xưng hô trong lời thoại của những nhân vật minh họa không được lịch sự cho lắm (xưng hô là mày, tao) nên nếu để trẻ nhỏ đọc một mình thì cũng không ổn lắm!
4
303694
2015-05-22 14:17:23
--------------------------
197981
10971
Đây là cuốn sách khoa học nhưng thông dụng trong xã hội và thích hợp cho lứa tuổi thiếu nhi mới lớn muốn nạp thật nhiếu kiến thức. Và khi đọc nó bạn sẽ không thể căng thẳng nổi bởi tính hài hước của tác giả viết sách và vẽ tranh, hơn nữa từ ngữ diễn đạt rất dễ hiểu. Nếu bạn có ý định ra biển hoặc muốn tìm hiểu kĩ về biển thì nên chọn quyển sách này bởi khi đọc sách rồi chuyến đi của bạn sẽ trở nên lí thú hơn vì biển có bao la thứ độc đáo và rất hay ho. Đóng sách lại rồi mà miệng tớ cứ cười mãi bởi sự hài hước của quyển sách và cảm thấy có rất nhiều điều mà mọi người chưa hiểu rõ về đại dương nên càng tìm hiểu sẽ càng thấy thú vị. Còn rất nhiều quyển sách mang tiêu đề  ‘ Horrible Geography ‘ viết về các chủ đề địa lí rất lí thú của cùng một tác giả. Còn tớ nhờ mấy cuốn Horrible ‘ khủng khiếp ‘ này mà trở nên yêu sách hơn đấy. Đọc thử rồi biết !! .
5
605634
2015-05-18 21:19:54
--------------------------
132252
10971
Tôi rất thích tiêu đề của sách, hài hước giống như chính cuốn sách. 
Cung cấp rất nhiều thông tin về đại dương, bao gồm cả các sinh vật biển, các dòng hải lưu, cách đại dương hình thành...cho đến cướp biển, Titanic bị chìm ra sao, dưa chuột biển thì ăn được nhưng chanh biển thì không...
Rất nhiều rất nhiều thông tin, nhưng không hề loạn.
Nội dung rất thú vị, vui nhộn.
Nhiều hình vẽ minh hoạ dễ hiểu.
So với bản in trước, bản in 2014 chuyển sang giấy xốp, nhẹ và không loá mắt  nhưng dễ ẩm mốc. Nhưng nội dung đảm bảo vẫn hay cực kỳ.
4
187467
2014-10-30 15:36:21
--------------------------
13686
10971
Một cuốn sách vui nhộn và kì thú bổ sung cho ta những kiến thức rùng mình về những con tàu đắm mất tích trong tam giác quỷ, những tên hải tặc, khủng bố trên biển từ xưa đến nay và cả những con cá mập trắng khổng lồ, những loài cá độc đáo, đầy màu sắc. Và cả những truyền thuyết, huyền thoại về tất tần tật mọi thứ từ trên mặt biển xuống đến những thứ dưới đáy biển, các xác tàu đắm, các quái vật biển, và nhiều thứ khác. Phóng viên báo trong cuốn sách sẽ đưa ta đi khám phá đại dương khó thương của chúng ta.
3
9013
2011-10-28 10:57:07
--------------------------
