545881
5823
Không phải là một câu chuyện lãng mạn nhẹ nhàng như "giai điệu cho những trái tim tan vỡ", mà tràn đầy tuổi trẻ với những nỗi lo lắng, trưởng thành, đau khổ và tràn đầy tình yêu non nớt nhưng không phải không mãnh liệt.
Đọc cuốn sách như trải qua hết bao nỗi buồn, xong sống như chính mình để cảm thấy vui vẻ và trọn vẹn nhứt
4
163788
2017-03-17 16:14:35
--------------------------
459168
5823
Đây là một cuốn sách tình yêu lãng mạn với lối viết hơi nhàm chán. Tuy nhiên không phải vì thế mà cuốn sách không hay hoặc không có chỗ nào đặc sắc. Nội dung không đến nỗi hấp dẫn và lôi cuốn nhưng có một khía cạnh nhân văn rất cao ở cốt truyện đó là "ai cũng có quyền mắc sai lầm" - nhưng không có nghĩa bạn sẽ sai lầm suốt cả đời. Bạn có quyền đứng lên nhìn nhận, đấu tranh và giải quyết mọi vấn đề do sai lầm đó gây ra chỉ cần bạn có niềm tin, động lực hay nói rõ là tình yêu và sự tin tưởng của Micha dành cho Ella. Cuốn sách nhẹ nhàng, một vài chi tiết thật sự hay nhưng kết thúc hơi vội khi chuyện của Micha và bố cậu chưa được giải quyết thỏa đáng.
3
82925
2016-06-25 21:01:26
--------------------------
382341
5823
Bí mật của Ella và Micha là một câu chuyện hay, một tình yêu đẹp và mang một bài học to lớn. Dù bạn thay đổi như thế nào đi nữa thì cốt lõi của bạn không bao giờ thay đổi, trái tim thổn thức vì một người vẫn rung liên hồi khi gặp người ấy. Tình yêu cháy bỏng có thể vực con người dậy sau tất cả những khó khăn, mang họ về với nhau và về với con người vốn có. Một câu chuyện đậm chất Mỹ, phóng khoáng, không kiêu dè và sâu sắc, không quá gây cấn hồi hộp nhưng lại mang đến cho tôi một cảm xúc bôi hồi khó tả, cách dẫn truyện cũng đặ biệt khôn kém khiến tôi hiểu thêm về suy nghĩ của mỗi nhân vật trước mỗi sự việc xảy ra trong truyện. Tôi đọc truyện trong một dịp tình cờ và đây là một sự tình cờ tuyệt vời!
5
569292
2016-02-18 23:27:25
--------------------------
282413
5823
Mình mua cuốn sách này vì thấy bìa khá đẹp và được đánh giá là "The New York Times Bestseller". Thế nhưng khi đọc thì mình cảm thấy không được như mong đợi. Câu chuyện kể về cô nàng Ella, thay đổi mình hoàn toàn, bỏ đi một nơi thật xa, để lãng quên cuộc tình với anh bạn thân từ thời thơ bé tên là Micha, sau một biến cố lớn xảy ra với Ella. Trong khi đó, Micha ở quê nhà liên tục tìm kiếm thông tin về cô, nhưng vô vọng. Đến mùa hè, Ella buộc phải về nhà, và đương nhiên, những kí ức của cô đối với Micha và quá khứ của mình sống lại, cô không thể chạy trốn được nữa... Đây là một nội dung quen thuộc, và cách kể chuyện của tác giả không thể lôi cuốn được mình vào mạch câu chuyện.
2
311551
2015-08-29 09:35:23
--------------------------
216739
5823
Cuốn truyện này khá là hấp dẫn với mình vì nó rất phù hợp với tuổi trẻ, ngôn ngữ đầy năng động và tươi vui. Nội dung của câu chuyện rằng Ella và Micha là hai nhân vật đang chìm đắm trong thế giới hiện tại và đang cố quên quá khứ, tuổi thơ đau buồn của họ. Hai người bắt đầu nảy sinh tình cảm với nhau, sau nhiều sóng gió giữa cuộc tình của họ, cuối cùng họ đã nhận được tình cảm chân chính của đối phương. Ban đầu truyện đi hơi nhanh nhưng sau đó truyện nhẹ nhàng, chậm rãi pha chút sâu lắng cho người đọc như tôi cảm thấy cần phải suy nghĩ về cuộc sống hiện tại, hãy cho đi yêu thương nhiều hơn như hai nhân vật trong truyện, đặc biệt chính là suy nghĩ về tình yêu - một cảm xúc của những người mới lớn như tôi
5
465628
2015-06-28 10:44:25
--------------------------
161425
5823
Bí mật của Ella & Micha hợp với lứa tuổi teen, truyện mang cảm xúc đậm chất teen Mỹ, có nét giống những bộ phim truyền hình Mỹ mà mình từng xem, có lẽ mình đã qua tuổi teen nên khá kén truyện kiểu này.
Đoạn giới thiệu không nhiều, đơn giản xúc tích khiến mình bỗng có hứng hay là mua thử xem biết đâu lại thích truyện có không khí trẻ trung ấy.  Tuy với mình truyện không hợp gu nhưng mình tin rằng sẽ có nhiều bạn thích nó bởi sự chân thực, giọng văn phù hợp của tác giả.
3
49203
2015-02-27 21:05:42
--------------------------
141940
5823
Điều đầu tiên khi đọc cuốn sách này là tôi cảm thấy nội dung khá giống với một cuốn sách tôi đã từng đọc đó là "Ba mét phía trên bầu trời", tuy truyện của Jessica không xuất sắc bằng nhưng giọng văn của cô vẫn có một cái gì đó rất lôi cuốn tôi. Ella & Micha những người trẻ đang sống trong một thế giới hiện đại nhưng phía sau cuộc sống của mỗi người là một tuổi thơ, một quá khứ đau lòng, chính vì sự đồng cảm ấy mà họ nhận ra cảm xúc của trái tim mình. Chia ly rồi lại tái hợp, tình yêu sau còn mãnh liệt hơn cả ban đầu, sự chăm sóc dịu dàng của chàng trai trẻ dành cho cô gái của anh ta khiến tôi cảm thấy rất thích.
Đây chỉ là một câu chuyện nhẹ nhàng theo đỗi thông thường, có thể nhân định rằng cuốn sách chỉ phù hợp với lứa tuổi mộng mơ nhưng sâu xa trong tác phẩm tôi vẫn cảm nhận được một cái gì đó rất ý nghĩa và ngọt ngào của một mối tình thời trẻ, những cảm xúc mà khó ai quên được nếu như đã từng trải qua trong quá khứ.
4
41370
2014-12-17 09:27:53
--------------------------
129753
5823
Mình là một người yêu thích những tác phẩm văn học lãng mạn, và càng đọc nhiều thể loại này thì mình càng mong muốn tìm được trong đó những điểm mới mẻ, táo bạo và không lặp lại. Nhưng "Bí mật của Ella và Micha" lại gần như không có những điều đấy, có thể nói nó làm cho mình khá thất vọng. Độ nổi tiếng của cuốn sách chính là lý do ban đầu mình chọn mua và kỳ vọng vào nó, nhưng sau khi đọc thì chẳng có mấy chi tiết để lại ấn tượng cho mình. Hình tượng nhân vật trong tác phẩm quen thuộc đến mức nhàm chán: một cô gái xinh xắn giàu tình cảm cùng một anh chàng đẹp trai cuốn hút, thích đua xe, tiệc tùng và có phần ngổ ngáo, những nhân vật kiểu này mình đã thấy rất nhiều rồi. Nội dung câu chuyện cũng diễn biến theo kiểu lối mòn, thiếu điểm nhấn, từ đầu đến cuối chỉ thấy hai nhân vật chính nói với nhau những lời âu yếm, rồi say đắm nhau đến mức điên cuồng. Thực chất là trong chuyện có vài chi tiết thú vị như quá khứ đầy đau thương của Ella và sự đấu tranh nội tâm của cô, nhưng tác giả lại không khai thác sâu thêm, mà viết một cách có phần hời hợt, sơ sài. Chính vì vậy mà tác phẩm thiếu chiều sâu, và nó rất dễ bị lãng quên sau khi đọc vì không có gì  đặc sắc cả.
2
109067
2014-10-12 14:59:04
--------------------------
