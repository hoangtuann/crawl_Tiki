473209
6673
Mình tình cờ thấy cuốn này trong hội sách thường niên, thế là mua luôn. Phải nói cuốn sách khá nhỏ và mỏng, nhưng những gì được viết trong cuốn sách thật sự rất đáng giá. Cuốn sách nêu ra những tipps, kinh nghiệm khi bán hàng,giúp khách hàng tìm ra nhu cầu của mình chứ không phải chỉ chăm chăm để bán được hàng, ngoài ra còn có các ví dụ thực tiễn rất sinh động và bổ ích. Nếu bạn muốn trở thành một seller giỏi, đừng bỏ qua cuốn sách này, nó sẽ giúp ích cho bạn.
5
381211
2016-07-10 12:56:16
--------------------------
445958
6673
Nhìn chung sách cũng khá hữu ích, giúp bổ sung kiến thức, kỹ năng xử lý tình huống với khách hàng. Đưa ra những vấn đề mà một nhân viên bán hàng hay gặp phải với khách hàng và những việc mà nhân viên bán hàng nên làm khi không có khách hàng trong cửa hàng. Tuy nhiên, những tình huống đưa thì cách giải quyết hay bị lặp lại và có những cách giải quyết theo cá nhân mình vẫn chưa hợp lý lắm. Rất mong Alphabooks có những cải thiện và cho độc giả có những sản phẩm tốt hơn nữa. 
3
808631
2016-06-11 10:40:26
--------------------------
323255
6673
Đây là 1 quyển sách mỏng, nhẹ, nhưng tổng hợp được những điều CĂN BẢN đối với việc bán hàng, cách giúp đỡ khách hàng, dẫn dắt khách hàng, không quá vội bán hàng, cần phải nêu được giá trị món hàng trước khi nói đến giá cả. Tuy nhiên quyển sách này lấy ví dụ về các mặt hàng quần áo, giày dép thời trang là chính, tôi có cảm giác là người viết nghiêng về các sản phẩm có giá tiền không quá cao, nhưng về cơ bản là có thể áp dụng trong nhiều trường hợp. Với chất lượng in tốt, lý thuyết cơ bản và giá tiền của quyển sách này là phù hợp.
4
471751
2015-10-18 10:47:20
--------------------------
306588
6673
Mình đọc và học qua nhiều khóa bán hàng rồi, nhưng đọc xong cuốn này vẫn thấm thía nhiều điều. Nhất là tâm thế BÁN chứ không đặt mình vào tâm thế MUA. 
Rất rất rất khó. Đối với các bạn dang bị áp chỉ tiêu doanh thu, chỉ tiêu bán hàng thì còn khó hơn vì bạn hiểu, bạn ghi nhớ, bạn đặt mục tiêu là BÁN  ... càng nhiều càng tốt, nên vô hình chung bạn quên mất tâm thế của người mua là như thế nào. Người mua càng được mời chào, càng thấy như bị ép lại càng không thích mua. 
Thì đọc xong cuốn này bạn sẽ hiểu phần nào cách "Giúp khách hàng mua", mà hiệu quả của việc này đã được nhiều cửa hàng có doanh thu cao khẳng định :) 
Mình thích cuốn này :)
4
100682
2015-09-17 17:40:12
--------------------------
146230
6673
Cuốn sách theo đuổi 1 tôn chỉ duy nhất: đừng vội bán hàng, hãy giúp khách hàng mua hàng, luôn đặt mình ở vị trí khách hàng. Ở sách, ta học hỏi được những bí kíp đơn giản, hữu hiệu: tạo ấn tượng tốt với KH, cách đặt câu hỏi cho KH khó tính, giới thiệu sản phẩm hấp dẫn, xử lý ổn thoả mọi tình huống, biến khách vãng lai thành khách ruột. Các vấn đề được cụ thể hóa trong quá trình bán lẻ từ khi KH vào cửa hàng đến lúc ra. Tác giả chỉ ra việc cần làm của cửa hàng khi vắng khách là luôn phải tỏ ra bận rộn: dọn dẹp sạch sẽ, sắp xếp bài trí lại cửa hàng nhưng chỉ đưa ra số liệu khẳng định những cửa hàng thực hiện như thế đều đạt kết quả cao chứ không giải thích nguyên nhân. Còn nhiều chỗ khác nữa, người đọc rất cần lời giải thích mà không có.
4
381173
2015-01-03 08:51:17
--------------------------
