115512
8637
Mình thấy truyện này khá hay, mặc dù là truyện dành cho thiếu nhi nhưng người lớn đọc cũng sẽ thấy hấp dẫn. Phần tóm tắt của Tiki cũng đầy đủ rồi nên mình chỉ đưa ra vài ý kiến cá nhân thôi: điểm cộng là miêu tả rất đặc sắc (mình rất thích những đoạn về con bé con của ông chủ lâu đài đối thủ của ông George và bà Emily), cốt truyện tưởng chừng như đơn giản nhưng đọc đến gần cuối sách, khi bọn trẻ và lũ ma phát hiện ra bí mật trên hòn đảo, nơi đàn thú đang bị giam giữ bí mật, chắc chắn bạn sẽ rất ngạc nhiên; điểm trừ: phần xuất hiện của những con ma không được "nuột" cho lắm, khá thiếu tự nhiên. Tóm lại thì đây là cuốn sách giải trí khá hay!
3
307488
2014-06-29 16:00:19
--------------------------
