340084
3664
Sách trình bày theo dạng truyện tranh, hình vẽ sinh động nên rất hấp dẫn các bạn nhỏ. Trang giấy dày cà màu sắc rõ nét.
Nhưng vì mình mua cho bé 5 tuổi nên nhiều thông tin hơi khó hiểu, có lẽ các bé tiểu học và lớn hơn sẽ phù hợp hơn. Sách này sẽ giúp các bé hình dung các loại thể khí một cách cụ thể và dễ nhớ hơn.
Tuy nhiên, chữ hơi nhỏ, nhiều khung hình nên khi đọc cho bé nghe mình cũng thấy mỏi mắt và có vẻ bé hơi khó theo dõi nhịp truyện.
3
715022
2015-11-19 10:10:40
--------------------------
