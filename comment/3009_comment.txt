418781
3009
Từ trước đến nay khi mình tìm tư liệu về ngành du lịch thường mình chỉ tìm ngay đến những quyển cẩm nang du lịch chứ chưa bao giờ tìm hiểu du lịch qua cách viết của một nhà văn. Được biết nhà văn Sơn Nam là một trong những cây bút viết văn tài ba về phong tục lối sống của người dân Nam Bộ. Đây là quyển sách đầu tiên mình tìm đọc của Sơn Nam. Vùng đất Hậu Giang và An Giang vẫn còn những góc khuất mà ngày nay ít có sách vỡ nào đề cập đến. Qua ngòi bút của Sơn Nam mình đã khám phá ra còn nhiều điều hay ở vùng đất này.
5
509425
2016-04-19 22:35:49
--------------------------
