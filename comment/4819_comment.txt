402716
4819
Khi cầm trên tay cuốn sách này, mình rất vừa ý. Bìa sách giản đơn, tinh tế và đẹp. Chất lượng giấy theo mình là khá tốt. Chữ rõ ràng. Mình đã đọc được vài truyện và cảm thấy lắng đọng cảm xúc  trước những nhân vật mà Jack London khắc họa. Đặc biệt ấn tượng nhất là tác phẩm "Tình yêu cuộc sống". Dù có gian khó, hiểm nguy  cách mấy, dường như nhân vật nhận thấy cái chết đang đến rất gần với mình nhưng bằng sự khao khát được sống đã thôi thúc con người ta phải vươn lên nghịch cảnh bằng mọi giá. Câu chuyện chan chứa về khát vọng sống của con người luôn còn mãi. 
Cảm ơn Tiki nhé!
5
791063
2016-03-22 20:23:34
--------------------------
391420
4819
Mình biết đến Jack qua tác phẩm nổi tiếng viết về loài vật là cuốn Tiếng gọi nơi hoang dã. Trong tuyển tập truyện của Jack này thì chỉ tiếc mỗi khôg có tác phẩm tiếng gọi... và cuốn nanh trắng. Mình nghĩ đây là 1 thiếu sót. Nhưng bên cạnh đó mình biết thêm được vài truyện qua cuốn sách này của ông như là tình yêu cuộc sống, .... bìa đẹp giấy mềm trắng chống lóa mắt. Cốt truyện tuy dài, khó đọc nhưng vẫn chuyên chở tư tưởng tình cảm sâu kín mà nhà văn muốn gửi gắm vào trong đó.
5
634295
2016-03-05 12:58:52
--------------------------
360128
4819
Mới nhận được sách. Cảm giác đầu tiên là hụt hẫng. Mình nghe nhiều bạn khen hay nên cũng mong chờ lắm, nhưng khi giở trang sách đầu tiên, nhìn danh từ riêng bị viết phiên âm khiến mình cực kỳ khó chịu. Whisky thì bị gọi là uýtki, Scotland thì là Xcốtlen, San Francisco thì là Xan Phranxixco,..blah blah blah.  Mình khuyên bạn nào không thích phiên âm tiếng Việt thì đừng nên chọn mua cuốn này. Mình thật tiếc khi mặc dù có chức năng đọc thử nhưng click vào thì không xem được gì cả. Mong tiki kiểm tra lại cái chức năng đọc thử của cuốn này để các bạn khác không gặp phải sự khó chịu như mình.
3
905014
2015-12-28 18:59:32
--------------------------
352649
4819
Một trong những nhà văn Mỹ mà mình thích nhất vì đơn giản tác phẩm của ông viết đều liên quan đến các con vật với nhiều cung bậc cảm xúc với con người. bên cạnh đó là tính cách nhân vật, hình ảnh tầng lớp, địa vị và lối sống miền hoang dã của nước Mỹ cũng hiện ra cho ta thấy một mong ước sống gần gũi thiên nhiên của không nhiều người thay vì sự xô bồ bon chen mệt mỏi nơi đô thị. Nanh trắng rồi tiếng gọi nơi hoang dã đều là những tác phẩm như vậy, rất hay và nhiều ẩn ý cho ngừơi đọc cảm nhận..
5
568747
2015-12-14 20:58:08
--------------------------
253800
4819
Cái cảm giác khi đọc xong quyển sách này là cực kỳ thú vị. Sau khi đọc qua tác phẩm Nanh Trắng của Jack London, tôi đã quyết đinh tiếp tục làm mọt sách với cuốn sách này. Câu chuyện của Jack London rất đa dạng, từ câu chuyện của những người Anh điêng da đỏ, câu chuyện của những người da trắng trong cuộc phiêu lưu tìm vàng hay tìm vùng đất mới đến câu chuyện ở nhũng hòn đảo xa xôi nơi những người thổ dân vẫn còn ngự trị ! Bên cạnh đó bài học cuộc sống trong mỗi câu chuyện vẫn có giá trị đến ngầy hôm nay. Đây thật sự là 1 cuốn sách hay xứng đáng có trên bất kì kệ sách nào !!!
5
105202
2015-08-04 19:24:45
--------------------------
201363
4819
Mình đã từng đọc truyện ngắn Tiếng gọi hoang dã của Jack London, sau đó quyết định mua cuốn sách này để có cơ hội đọc nhiều hơn các tác phẩm của ông. Ngòi bút của Jack London rất mãnh liệt, quyết đoán, ông luôn đẩy nhân vật của mình vào bước đường cùng, bóc trần hiện thực xã hội lúc bấy giờ, để qua đó thể hiện tính nhân văn. Dù là người hay vật, trong nghịch cảnh, luôn phải tìm cách vươn lên, vươn đến khát vọng 1 ngày tươi đẹp hơn. Cảm ơn 1 quyển sách hay và ý nghĩa.
4
131327
2015-05-27 10:10:16
--------------------------
174887
4819
Mình mất vài tuần để đọc xong các mẫu chuyện chọn lọc rất hay trong cuốn này.
 Truyện được kể khá đa dạng và cả những bài học được rút ra cũng phong phú không kém.
 Ở một số câu chuyện, Jack London đặt nhân vặt mình vào vị trí ngoặt nghoèo, gian nan và khiến người ta chỉ muốn chết cho xong. Nhưng ngay sau đó Jack London lại để nhân vật của mình trả lời cho tất cả những cố gắng vùng dậy trong băng tuyết lại rát như lửa và ngày đói dài :"càng đẩy con người vào bờ vực cái chết thì con người càng muốn thách thức sự sống"
 Tôi còn thích cả những bản tình ca anh hùng trong vài mẫu truyện, mối tình vượt giai cấp, sự định kiến và lòng tự tôn. Và đôi khi đi với tình yêu là sự lạm dung.
 Bên cạnh mặt trái tình yêu, là rượu, thuốc, lòng tham xâm chiếm, ... tất cả vẽ lên một bức tranh về bộ mặt con người da trắng thối nát, và đồng thời gợi một thoáng lịch sử oai hùng lẫn bi thưỡng của các thổ dân Mỹ.
5
523230
2015-03-28 23:23:36
--------------------------
