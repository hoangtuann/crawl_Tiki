384878
4727
Một túi yêu thường là một cuốn sách hay, mình mới đọc sơ qua thôi mà đã rất thích, không chỉ mình và cả gia đình mình ai cũng thích từ ông xã mình và cha mẹ chồng mình cũng thích. Một cuốn sách chứa đựng những yêu thương, những nấc thang phát triển của bé những cung bậc của cảm xúc, giúp cho người lớn học được rất nhiều điều. Một túi yêu thương không chỉ dành riêng cho những người lần đầu làm cha mẹ, một túi yêu thương còn dành cho ông bà và cả những gia đình đã có và đang chuẩn bị đón chào bé thứ 2. Một cuốn sách rất hay và ý nghĩa
5
604547
2016-02-23 09:14:23
--------------------------
349406
4727
Cuốn sách đã đem lại cho mình những bài học bổ ích, quý giá trong cuộc sống. Nó như một người thầy ôn tồn nâng đỡ, bảo ban ta để ta trưởng thành hơn trong cách nói, cách nghĩ, cách ứng xử giao tiếp thành công. Từ những bước đi đầu tiên đó. Ta cùng quyển sách đi đến những thành công trong đường đời, sự nghiệp, các mối quan hệ mật thiết cũng nhue xã giao. Với ngôn từ lành mạnh. Nội dung hữu ích, hấp dẫn, mang laih người đọc sự thích thú và tập suy ngẫm, soi xét bản thân. Sách là mặt trời của mỗi con người
4
1003449
2015-12-08 12:53:21
--------------------------
302196
4727
Mình mua sách này cũng nhiều loại sách giáo dục con trẻ khác như Nuôi dạy con kiểu Nhật, Chờ đến mẫu giáo thì đã muộn. Cũng như các loại sách hiện nay, Một túi yêu thương là cẩm nang bỏ túi cho các bậc cha mẹ trẻ hiện nay, ủng hộ việc giáo dục con một cách nghiêm khắc, đồng thời vẫn cho con được quyền quyết định và lựa chọn trong pham vi cho phép. Lời văn mộc mạc và giản dị nhưng lại rất dễ hiểu, đi thẳng vào những vấn đề các ông bố bà mẹ thường xuyên đối mặt với con như giải quyết thế nào khi con ăn vạ, vô phép với người lớn....
4
671150
2015-09-15 02:06:25
--------------------------
269422
4727
Mình mua cuốn này tại Tiki và có thử tìm cuốn nguyên bản bằng tiếng Anh. Cuốn tiếng Anh có tên là “The pocket parent” và được thiết kế với size nhỏ, bỏ túi, giống như một cuốn từ điển cho các bậc cha mẹ dễ dàng mang theo và tra cứu.Nội dung cuốn sách xoay quanh các vấn đề mà cha mẹ thường phải đối mặt khi có con từ 2 đến 5 tuổi, sách tiếng Anh chia các vấn đề và sắp xếp theo thứ tự bảng chữ cái nên khi dịch sang tiếng Việt người đọc có thể chưa hiểu rõ tại sao mục lục lại được sắp xếp như vậy. Mình thấy các vấn đề mà tác giả nên lên rất phổ biến và tác giả có đưa ra gợi ý để giải quyết. Đây là một cuốn sách hay, các bậc cha mẹ nên tham khảo.
3
15022
2015-08-17 13:08:33
--------------------------
254418
4727
Cuốn sách khẳng định một chân lý cho việc dạy con trẻ là luôn cần một tình yêu đầy đặn, sẵn sàng và vô bờ bến từ các bậc phụ huynh. Tuy vậy để vận dụng vào cuộc sống hàng ngày các ông bố bà mẹ chắc chắn sẽ thấy khó khăn khi thực sự thấy con bạn ăn vạ, mè nheo, bướng bỉnh, nói hỗn. Rất nhiều ví dụ tình huống được đưa ra để bạn đọc hình dung, suy nghĩ, thay đổi cho phù hợp và áp dụng. Cuốn sách hỗ trợ bạn một phần trong phương pháp ứng xử với con cái và hơn thế nữa là cách yêu thương con cái đúng mực. 
4
232089
2015-08-05 10:54:17
--------------------------
254097
4727
Hẳn là những bạn trẻ lập gia đình, có con đầu lòng luôn bối rối không biết khi phải chăm sóc, chiều con thế nào cho phải? để con không hư, không xấu tính đi, và theo mình, đây là cuốn sách mà bạn nên đọc, nên tham khảo để chăm sóc, dạy dỗ con, để bé có thể phát triển, lớn lên tốt về cả thể chất lẫn tâm lí. Đây là cuốn sách mình mua tặng chị dâu, và chị đã khen mình rất nhiều cũng như rất quý cuốn sách này, đó là niềm vui rất lớn với mình khi được biết cháu của mình được một bà mẹ hiểu biết chăm sóc, nuôi dưỡng. Một Túi Yêu Thương xứng đáng được đặt tại vị trí trân trọng của tủ sách nhỏ của mỗi gia đình và là món quà quý cho những người thân của bạn.
5
304901
2015-08-04 23:49:00
--------------------------
206740
4727
Không một ai trên đời này lại không yêu thương con cái của mình, thế nhưng mỗi người lại thương con một kiểu, có người thì luôn chìu con, đáp ứng mọi yêu cầu mà con đòi hỏi, có người thì lại quá nghiêm khắc với con, lúc nào cũng mắng chửi các con, không cho con giao du với bạn hay tham gia bất cứ một cuộc chơi nào trong lớp, sợ con hư, sợ con bị ảnh hưởng bởi bạn bè... Vậy làm thế nào để thể hiện tình thương với con cái một cách khoa học vừa mang tính giáo dục con, uốn nắn con nên người vừa cho trẻ thấy rằng cha mẹ luôn thương yêu và tôn trọng các con? Cuốn sách quý này sẽ cung cấp cho các bạn nhiều bí quyết, nhiều lời khuyên, với những tình huống cụ thể khi bạn đối mặt với nó bạn nên làm gì. Hãy kiên nhẫn đọc từng trang sách để tích lũy những bí quyết tuyệt vời mà tác giả mang tặng cho các bậc cha mẹ chúng ta nhé!
5
455308
2015-06-10 15:13:31
--------------------------
169925
4727
Tuy văn phong của quyển sách không bay bổng và mượt mà như nhiều quyển sách nuôi dạy con khác mà tôi từng đọc nhưng giọng văn rất gần gũi, thân thiện cho tôi cảm giác như tác giả đang ngồi cạnh trò chuyện với tôi vậy.
Tác giả đã trình bày quyển sách rất ngắn gọn và dễ hiểu, không nói lan man, không lạc chủ đề, tình huống kèm theo mỗi vấn đề cũng vậy, các tình huống rất gần với thực tế thường thấy khi nuôi dạy trẻ, từ các vấn đề thường xuyên gặp phải đến các vấn đề mà đôi khi các bé quái chiêu nghĩ ra thường khiến các bậc phụ huynh phải đau đầu, và các cách để giải quyết tình huống rất hiệu quả, và là những cách thức rất đơn giản không đòi hỏi các bậc phụ huynh phải có kỹ năng gì ngoài sự kiên nhẫn và phải cố gắng giữ bình tĩnh vốn là 2 điều mà các bậc phụ huynh luôn nên có, nhưng nếu chưa quen cũng không sao, trong sách có vài đoạn giúp các bậc phu huynh vô tình mất kiên nhẫn, nổi nóng với trẻ biết cách xử lý vấn đề như thế nào rồi. :)
Đây thật sự là một quyển sách rất hữu ích cho những ai chưa có kinh nghiệm nào trong việc nuôi dạy trẻ mà nhất là những bé lắm chiêu lắm trò.
5
96853
2015-03-19 06:22:40
--------------------------
165209
4727
Quyển sách dành tặng cho tất cả các phụ huynh. Lật mở mỗi trang sách, chúng ta đều cảm nhận được vị yêu thương mà tác giả đã dành tặng cho chúng ta. Đó không chỉ là những bài học dạy con đơn thuần, mà chứa đựng trong đó tình yêu mà tác giả dành tặng cho trẻ thơ. Qua đây, tác giả muốn gửi cho chúng ta một thông điệp :" đừng tạo khoảng cách với con mà hãy luôn luôn dang rộng vòng tay để chở che và trò chuyện cùng chúng. Hãy là những người bạn thân của con mình"
4
93425
2015-03-10 00:08:43
--------------------------
165182
4727
TUy tôi ko phải là 1 bậc cha mẹ và dù tuổi tôi vẫn còn rất nhỏ thì khi đọc cuốn sách này lên, tôi mới hiểu sự vất vả của cha me đối với việc nuôi dạy con trẻ khó khăn và nhọc nhằn tới nhường nào, cuốn sách đã cho tôi biết những điều thực sự rất bổ ích và tôi rất thích câu nói của tác giả: có rất nhiều sách dạy bạn về cách nuôi dạy trẻ nhưng chỉ có 1 vài quyển mới đáng để bạn bỏ tiền ra. Tác giả sự thực đã đánh trúng tim đen của tôi. Tôi sẽ ủng hộ tác phẩm này thật nhiều
5
575641
2015-03-09 22:36:51
--------------------------
164082
4727
Một túi yêu thương- một gia đình hạnh phúc
Không đơn thuần là những dòng chữ cộc lốc mà đây là tác phẩm rất có ích cho các bậc phụ huynh đang có con cái và muốn chúng thành những người công dân tốt
Hẳn đã viết về cách nuôi dạy con cái do đó văn phong không được suôn sẻ  cho lắm mà có tí cứng rắn, dứt khoác trong đó nhưng vẫn làm cho người đọc là những bậc cha mẹ khá là hài lòng
Tuy nhiên, những bạn trẻ cũng có thể đọc như mình vì nó có thể làm con người bạn trở nên tốt hơn, yêu thương mái ấm gia đình hơn, yêu thương tổ quốc mình hơn nữa
Đây là sản phẩm cần mua và trân trọng nó
5
465628
2015-03-06 19:13:15
--------------------------
163942
4727
Mỗi quyển sách dạy con cái là một trải nghiệm và một bài học thật riêng biệt.
Một Túi Yêu Thương không đơn giản chỉ là một quyển sách lý thuyết suôn về cách dạy con cái mà nó còn chứa đựng cả những yêu thương trìu mến của cha mẹ dành cho con của mình :)
Về những bài học dạy con: theo mình thấy là có thể áp dụng được. Cách viết của tác giả thực tế và chân thật. Cách trình bày logic và mạch lạc. Văn phong tuy không được mượt mà cho lắm nhưng vẫn không quá khô khan, giáo điều :D Đó chính là những điểm mà mình thấy thích và đánh giá cao quyển sách này hơn những quyển sách dạy con khác!
Về phần tình cảm: Quyển sách giúp mình có thể thả tình cảm vào để dạy dỗ con cái , nó giúp những đứa trẻ thấu hiểu được tình cảm của đấng sinh thành hơn. 
Nói tóm lại: MÌnh thấy quyển sách này hữu dụng và thực tế - có thể áp dụng được cho con trẻ của mình. Giúp giải quyết những tình huống "khó xử" đối với trẻ. Giúp thấu hiểu tâm lý của trẻ hơn để giải quyết :)
5
303773
2015-03-06 10:55:37
--------------------------
163883
4727
Trên thị trường hiện nay thì vô cùng nhiều sách dạy con cái, như  sách dạy con tự lập, dạy con ngoan, dạy con kiểu Nhật, kiểu Mỹ. Tất nhiên làm mẹ ai cũng muốn con mình trở thành người hoàn thiện như trong những cuốn sách đó không mấy khi nhắc đến tình cảm mẹ con. Cầm Một túi yêu thương lên mình thấy vô cùng hài lòng đây là cuốn sách vừa giúp mẹ dạy con vừa giúp mẹ có thể làm bạn với con nâng cao tình cảm gia đình, chắc hiếm có cuốn sách dạy trẻ làm được. 
  Thiết kế bìa cũng rất đáng yêu, bắt mắt. Một túi yêu thương chắc chắn sẽ dành được sự quan tâm lớn của các bà mẹ.
5
526401
2015-03-06 07:31:29
--------------------------
