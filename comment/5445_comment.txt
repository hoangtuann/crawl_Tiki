484436
5445
Là người khá kiệm lời và ít khi nêu ra nhận xét về một cuốn sách khi vẫn còn đang đọc dở. Nhưng thực sự cuốn sách 18 phút này tuyệt vời hơn cả mong đợi về hình thức lẫn nội dung. Thời gian vốn là thứ đắt đỏ, sử dụng sao cho hợp lý, làm cách nào để tối ưu và đạt hiệu quả cao...cuốn sách sẽ hướng dẫn đầy đủ cho bạn! Tiếc là mình đã không mua cuốn này sớm hơn. Sẽ mua thêm vài cuốn nữa để tặng bạn bè vì thật sự rất nhiều thứ cần được đọc từ cuốn sách này.
5
1304316
2016-09-23 22:02:29
--------------------------
470397
5445
Tìm ra đâu là những hạn chế trong suy nghĩ,hành động của mình trước đâMình quyết định lên Tiki mua cuốn này sau một hồi lượn lờ ở gian hàng TGMBOOKS ở hội chợ sách :))))
Thật sự cuốn sách đã đem đến cho mình một cách nhìn rất mới,và cũng giúp mình y.Nói chung là cuốn này rất hữu ích,rất đáng đọc.
Dịch giả dịch rất mượt,rất tự nhiên.
Chất lượng giấy tốt,trình bày đẹp,chữ in rõ ràng.Rất tuyệt.
Chỉ có cái là khổ sách hơi to,nếu có khổ nhỏ để bỏ túi nhâm nhi lúc chờ xe buýt thì sẽ tuyệt hơn,theo ý kiến của mình là vậy :)
4
437930
2016-07-07 14:29:56
--------------------------
453268
5445
Mình luôn gặp khó khăn trong việc quản lý thời gian và sắp xếp công việc hàng ngày. Mình muốn tìm một cuốn sách giúp quản lí thời gian hiệu quả và mình đã tìm thấy. 18 phút không chỉ đơn giản để biết giá trị của thời gian, nó là một công cụ cùng các phương pháp cụ thể giúp ta làm chủ được thời gian, xác định mục tiêu, lên kế hoạch cho cuộc sống. Bên cạnh đó, còn có những phương pháp rất hữu ích giúp ta làm chủ sự phân tâm, chần chừ của bản thân, tập trung, kiên trì theo đuổi đam mê và thành công.
4
873902
2016-06-21 09:52:59
--------------------------
444088
5445
Quyển này hồi đầu mình cũng không định mua, vì mấy quyển kiểu dạy quản lý thời gian thì nhiều quá rồi, cái nào cũng như nhau, đọc riết thấy chán, nhưng đọc thử mục lục thì thấy cái này có vẻ hay nên mua thử. Nó hay thật đó các bạn ạ, đọc vô xong ghiền luôn không dứt ra được. Nói rất đúng các khó khăn trong việc quản lý thời gian, không phải kiểu chung chung như mấy quyển khác, quyển sách này đưa ra cách quản lý thời gian rất cụ thể và dễ thực hiện nhưng cũng hiệu quả lắm. 
5
68491
2016-06-07 21:05:18
--------------------------
429396
5445
Sách được in trên giấy đẹp, bố cục rõ ràng dễ đọc, hợp lí và rất thực tế. Cuối mỗi chương còn có các khung tóm tắt nội dung giúp cho độc giả dễ dàng nắm bắt nội dung của chương hơn. 18 phút là quyển sách phát về những phương pháp để triển bản thân và cũng là cuốn sách về kinh doanh cực kì thú vị và đem lại tính hiệu quả rất cao. Những lời khuyên trong quyển sách giúp chúng ta làm chủ thời gian và cuộc sống của mình. Cuốn sách này thực sự rất bổ ích với tất cả mọi người.
5
1227898
2016-05-13 09:45:09
--------------------------
408116
5445
Trước hết về kỹ thuật thiết kế, bố cục sách rõ ràng, hợp lý tạo cảm giác thư thái khi đọc và giúp người đọc dễ hiểu hơn. Chất lượng giấy rất tốt, màu đẹp. Ngôn từ dễ hiểu, gần gũi.
Còn về nội dung thì đây không phải là quyển sách mà bạn đọc rồi để lại lên giá sách mà không đụng đến nữa. Mà bạn cần phải đọc thường xuyên để luyện tập thói quen tốt và không từ bỏ nữa chừng hành động thay đổi thói quen. Đọc quyển sách này bạn sẽ thấy mình đâu đó trong những câu chuyện. Nói chung là sách hay đó!
4
1243937
2016-03-31 09:52:29
--------------------------
390444
5445
Tác giả trình bày sách theo một bố cục rất dễ đọc, lối văn hay, nhẹ nhàng và để lại khá nhiều ấn tượng. Từng đọc một số cuốn sách về thiền nên những ý tưởng trong phần I không quá mới mẻ đối với tôi. Tuy nhiên, tổng thể cả cuốn sách là những bài học rất hay và thú vị, giúp người đọc có những hướng hành động rất cụ thể để tiếp cận với thành công.
Một điểm trừ mà tôi không thích lắm, đó là việc trình bày cuốn sách. Cách dòng khá rộng, lề rộng, làm tăng khổ sách, tốn giấy và sách nặng. 
4
953009
2016-03-03 21:35:49
--------------------------
382098
5445
* Chất lượng sách rất tốt, bìa dày, giấy từng trang dày, trình bày đẹp, dễ theo dõi. 2 người dịch cũng rất hay. 
* Nội dung thì ổn, đem lại cho tôi nhiều điều, cách nhìn nhận cuộc sống, cách để dung hòa giữa cong việc, gia đình, xã hội,...
* Chắc có lẽ đậy là sản phẩm cùa TGM nên độ hoàn thiện rất cao từ hình thức đến nội dung. 
Đọc một cách chậm rãi, ngấm dần từng lời khuyên của tác giả bạn sẽ thấy rất dễ để điều khiển mọi thứ xung quanh theo ý mình.....
4
954684
2016-02-18 16:11:02
--------------------------
379584
5445
Đầu tiên, thích cách viết của tác giả, tác giả kể một câu chuyện của bản thân, rồi liên hệ với mạch chủ đề đang nói. 
Thứ hai, cách tác giả phân chia phần và chương, khá chi tiết và phù hợp.
Cuối cùng, tác giả có đưa ra một điều mà những ai đọc dạng sách này cũng cảm thấy là đọc xong muốn áp dụng rất nhiều nhưng thật ra, chỉ cần chọn một thứ mình cảm thấy thích, phù hợp và muốn làm ngay để thực hiện, rồi dần dần mọi thứ sẽ đi vào guồng của nó. 
Chỉ một điều, và đó là điều mình thích nhất.
5
32038
2016-02-11 16:14:06
--------------------------
373798
5445
Tôi mua quyển sách này vì đọc sơ qua tóm tắt là lập kế hoạch, làm chủ sự phân tâm, vì tôi luôn vướng lại ở quá trình tập trung vào công việc. Nhờ có quyển này tôi đã tập trung đúng đắn vào những trọng tâm cụ thể trong năm, nhìn về tương lai xa hơn và sống trọn từng giờ trong mục tiêu mình đã đề ra!
Quyển sách này đối với tôi phần giá trị nhất là cách thức kỉ luật từng ngày nhằm theo sát kế hoạch. Phần đầu có đề cập cách thức đi tìm điểm mạnh, chấp nhận điểm yếu, phát huy khả năng nhưng theo tôi là đối với những ai đang ở bước đầu xác lập mục tiêu và tầm nhìn thì quyển này không thích hợp.
4
153783
2016-01-24 18:08:39
--------------------------
366493
5445
Mình vừa đọc xong quyển sách này.
Sách in rất đẹp, chất lượng giấy quá tốt.
 Trái với suy nghĩ ban đầu về các loại sách nói về kế hoạch làm việc hay bị khô khan, khuôn mẫu thì quyển sách này tạo cho mình sự thích thú và vui vẻ khi đọc. Bên cạnh đó là những lời khuyên, gợi ý, và các câu chuyện thực tế, gần gũi giúp chúng ta sắp xếp, quy hoạch lại thời gian để đạt hiệu quả cao trong công việc cũng như trong cuộc sống.
Bằng cách ngắt nhỏ từng chủ đề, từng câu chuyện, người đọc sẽ cảm thấy dễ đọc và dàng tiếp thu.
Đây sẽ là quyển sách mà bạn nên đọc đi đọc lại nhiều lần và áp dụng tất cả các nguyên tắc, các lời khuyên của tác giả vào thực tế.
4
916249
2016-01-10 03:51:50
--------------------------
364500
5445
Lần đầu tiên mình đặt hàng ở Tiki, đó là 2 quyển sách "18 phút" và "đừng bao giờ đi ăn một mình". Phải nói là chất lượng giấy tốt được bao bì khá cẩn thận. 18 phút mình đã đọc và thấy rằng nó thật sự rất hay, qua quyển sách này tác giả chỉ cho mình cách làm chủ thời gian của mình chỉ với 18 phút. Bên cạnh đó là những giá trị sống quý giá và những câu truyện giản dị mà vô cùng ý nghĩa giúp t vỡ ra nhiều điều. Quyển sách thực sự thích hợp cho những ai muốn làm chủ cuộc đời của mình.
4
865873
2016-01-06 12:22:51
--------------------------
347755
5445
Cầm quyển sách thì công nhận ấn tượng về chất lượng của giấy in rất tốt. Nội dung khá thú vị và bổ ích. Không quá thiên về giảng giải đạo lý như các sách dạy kỹ năng mình hay đọc. Sách đan xen những câu truyện ngắn để minh họa về những nội dung muốn truyền đạt, nên đọc không cảm thấy nhàm chán. Mỗi ngày nên đọc vài phần nhỏ để suy nghĩ và rút ra kinh nghiệm cho bản thân và xác định mục tiêu, cách chuẩn bị cho tương lai. Mình thấy đây là cuốn sách khá hay cho mọi người.
5
996441
2015-12-04 22:26:01
--------------------------
337476
5445
Cuốn sách in rất đẹp, rõ ràng chất lượng giấy tốt. Nội dung thì vô cùng tuyệt vời, đây thực sự là một cuốn sách hoàn thiện bản thân ai cũng nên có. Bạn có thể kiểm soát được nội dung sách một cách dễ dàng qua các phần tóm lượt và hệ thống ở mỗi chương, mỗi bài. Ngoài ra có rất nhiều bài học kinh nghiệm dễ dàng được đúc rút từ những ví dụ dễ hiểu. Tuy tóm lượt nội dung khiến bạn có thể bỏ qua các câu chuyện mà vẫn hiệu quả nhưng theo tôi các bạn nên đọc kĩ, các câu chuyện vẫn có giá trị riêng của nó, nhất là ở những phần chữ màu đỏ, đó là các công thức tuyệt vời.
4 sao, đáng ra là 5 vì nội dung quá tốt, tuy nhiên tôi không thích 2 trang quảng cáo đính kèm ở đầu và cuối sách cho lắm, tôi muốn xé nó ra mà sợ xấu sách. Theo tôi nên lựa chọn hình thức thông minh hơn để quảng cáo như kẹp cùng sách để người mua cảm thấy được tôn trọng.
4
467203
2015-11-14 08:14:51
--------------------------
310849
5445
18 phút đối với bạn nhiều hay ít? 
Trước đây đối với mình thì đó là một lượng khá ít, nhưng sau khi đọc cuốn sách này, nhận ra trong 18 phút mình có thể làm được bao nhiêu thứ, thời gian là một thứ tài sản vô giá mà mình không thể để nó phí phạm một giây nào. Cuốn sách đã khiến mình biết coi trọng thời gian, biết quản lý quỹ thời gian eo hẹp sao cho thật hợp lý để tạo ra những lợi ích không chỉ cho bản thân. Một cuốn sách tuyệt vời để bạn nhận ra bạn giàu có như thế nào với 24 giờ 1 ngày.
4
6274
2015-09-19 20:11:30
--------------------------
282258
5445
Tiêu đề sách khá đơn giản với số 18 nổi bật, thú thật là nhìn cái bìa mình đã bỏ qua mấy lần nhưng một lần dừng lại, đọc lại thông tin sách thì đã thấy có vẻ thú vị đây. Sách trình bày hết sức ngắn gọn, dễ đọc trải dài với 45 bí kíp, mỗi bí kíp là một câu chuyện kèm một lời khuyên nho nhỏ.  Có nhiều lời khuyên cũng không mới mẻ cho lắm, nhưng mình cũng chẳng mấy khi để ý mà làm đọc sách cũng là một lần xem lại và trải nghiệm.
3
588698
2015-08-29 00:25:25
--------------------------
260405
5445
Cảm nhận của mình ở những trang đầu tiên đây là một cuốn sách nhàm chán, cứng nhắc vì tác giả luôn mở đầu bằng một câu chuyện và kết thúc bằng một lời nhận xét khá chủ quan của mình nên không thú vị  lắm. Đọc đến khoảng 1/3 cuốn sách thì thấy có sự đột phá mới trong cách viết, tác giả đưa ra nhiều lời khuyên, lý giải vì sao và cách tiến hành như thế nào. Tiêu đề 18 phút không có nghĩa là mỗi ngày chỉ làm việc trong vòng 18 phút mà đó là 18 phút để tập trung nhìn nhận lại mình đã làm được những việc gì trong một ngày đó. Cuốn sách gợi được cảm giác tò mò và thú vị cho người đọc, rất đáng để có trong tủ sách nhà bạn.
5
593209
2015-08-10 13:09:36
--------------------------
259342
5445
Hiện tại trên thị trường sách về kỹ năng sống có rất rất nhiều đề tài, tôi đã đọc qua hầu như toàn bộ các cuốn sách này, cuốn thì nói về nghị lực, sự tích cực trong suy nghĩ, sáng tạo, bla bla bla... Những điều ấy nếu bạn lĩnh hội và nhập tâm được thì nó rất có ích nhưng hầu như chúng ta đang sống quá vội để có thể dừng lại và suy nghĩ về các giá trị này. 
Đây là cuốn sách viết về sự dung hợp giữa mục đích sống và các vai trò của một con người trong xã hội.  RẤT THỰC TẾ, không chứa đựng những giáo điều sáo rỗng. Cuốn sách đáng để đọc nhất trong năm.
4
21413
2015-08-09 12:54:55
--------------------------
258662
5445
Cuốn sách này chủ đề hao hao giống với cuốn 80/20 nhưng nó còn giúp chúng ta dừng lại và sống chậm hơn, biết tập trung vào những thứ quan trọng với chúng ta (có thể là gia đình, bạn bè, sở thích, ước mơ...) thay vì mỗi ngày bận rộn với công việc. Đọc và chậm rãi thực hành theo chỉ dẫn, bạn sẽ bất ngờ khi thấy cuộc sống mình thay đổi rõ rệt từ lúc nào. Tốt nhất là nên đọc 1 lượt sơ qua hết cuốn sách, rồi sau đó đọc kỹ lại từng chương và ứng dụng nó.
5
99983
2015-08-08 18:55:30
--------------------------
255622
5445
Bìa sách ấn tượng và con số 18 gây tò mò, đó chính là lý do khiến tôi phải đạt mua quyển sách ”18 phút” này trên Tiki. Và khi nhận được quyển sách này trong tay, tôi thấy quyển sách này bìa sách thật sự rất đẹp, trình bày nội dung trong sách cũng đẹp, giấy rất tốt, nội dung trong quyển sách này đề cập đến phương thức quản lý thời gian. Đã có rất nhiều sách viết về chủ đề quản lý thời gian nhưng đều tương đối giống nhau và đi theo lối mòn, kh1o áp dụng , và quyển sách này đã giải quyết được vấn đề đó, quyển sách này đưa ra một phương thức rất hay đó là dành ra thời gian 10 phút để chúng ta lập kế hoạch và 8 phút để mỗi giờ chúng ta cảnh tỉnh lại bản thân để sử dụng thời gian hiệu quả hơn. Nhìn chung, theo tôi đây thật sự là một quyển sách rất đáng đển chúng ta mua về đọc và suy ngẫm.
4
42985
2015-08-06 10:48:56
--------------------------
254981
5445
Mình đã đọc khá nhiều sách của TGM nên cuốn nào mình mua cũng đều rất hài lòng,18 phút nghe có vẻ thời gian rất ít nhưng với 18 phút này bạn sẽ có thể thành công trong cuộc sống,18 phút bạn có thể làm được rất nhiều việc nếu biết cách sử dụng chúng.Mình đã đọc và áp dụng một số phương pháp của tác giả nên mình có nhiều thời gian hơn cho gia đình và bạn bè,thời gian là thứ quý giá và không thể nào trở lại vì vậy chúng ta nên biết cách sử dụng nó cho hiệu quả.Sách có mùi thơm và được bọc bằng một lớp kiếng bên ngoài nên không sợ rách.Mình rất thích cuốn sách này
5
48148
2015-08-05 17:23:16
--------------------------
225377
5445
Nội dung sách hay, thiết thực, khả năng viết của tác giả cũng lôi cuốn, không làm chán hay buồn ngủ cho người đọc. Tác giả đã chỉ cho ta cách xoay sở mọi thứ bằng cách kể và phân tích những câu chuyện, từ đó người đó rút ra được bài học và nhận thức cho chính mình. Bìa sách và màu trong trang sách in đẹp, chất lượng, đó là một điểm mạnh của sách TGM books, tuy nhiên TGM thường in những quyển kĩ năng sống hơi dài, cá nhân tôi không thích hình dạng cuốn sách mình trông hao hao sách giáo khoa. Tuy vậy, về cả hình thức và tổng thể, 18 phút xứng đáng 8/10.
5
109789
2015-07-10 11:47:30
--------------------------
215049
5445
Tôi mua cuốn sách này khi tiki chưa áp dụng chương trình giảm giá nên khá tiếc. Chất lượng giấy dày và đẹp khiến tôi rất hài lòng. Bìa được thiết kế khá sáng tạo, với con số 18 được phóng lớn trên trang bìa. Tôi đọc cuốn sách này cũng không được nhiều lắm nhưng cá nhân tôi thấy đây là một cuốn sách kinh tế khá là hay và hữu ích. Bản thân tôi cũng cảm thấy tin tưởng bản thân hơn trong việc điều hành công việc kinh doanh, năng suất của mình làm được. Dù vậy, tôi cảm thấy chưa thực sự thỏa mãn với cuốn sách này như một số cuốn sách cùng loại khác.
5
114793
2015-06-25 19:54:38
--------------------------
211248
5445
Đúng như lời nhận xét của một bạn phía trên, đây không phải là một quyển sách nhồi nhét các phương pháp quản lý thời gian. Quyển sách hướng người đọc tới việc hiểu được những thói quen sử dụng thời gian, yếu tố sao nhãng và đặc biệt đây là quyển sách giúp người đọc định hình được mục tiêu đích cuộc đời và những giá trị quan trọng trong cuộc sống, để rồi mọi nguyên tắc quản lý thời gian tác giả đưa ra đều xoáy sâu và đưa người đọc đến gần với mục đích đó.

Đây không phải là quyển sách chỉ cho bạn cách hoàn thành tất cả công việc. Quyển sách này giúp bạn sống cân bằng trong cuộc sống, sử dụng thời gian của mình với sự tập trung và hiệu quả tốt nhất. Những phương pháp trong sách liên kết chặt chẽ với nhau, và liên kết chặt chẽ với mục đích cuộc đời mỗi người. Và cuối cùng là cô đọng những nguyên tắc đó trong 18' mỗi ngày để có một ngày làm việc hiệu quả mà không xa rời giá trị bản thân.
5
332519
2015-06-20 11:27:22
--------------------------
191933
5445
Gretchen Rubin nhận xét quyển sách "không kém phần hài hước", nhưng khi đọc lời văn của 2 dịch giả không cho thấy độ hài hước tí nào.
Quyển sách là 1 sự trải nghiệm của chính tác giả , 1 người với sự nghiệp từng lên như diều gặp gió rồi đột ngột sụp đổ nhanh chóng. Qua sự thất bại tạm thời đó, tác giả đã điềm tĩnh trở lại, định vị lại bản thân mình và khởi động 1 quá trình mới. Sách khá hay và cần thiết cho ai muốn tìm lại, và tái khám phá bản thân mình !!!
3
594852
2015-05-03 10:21:08
--------------------------
185727
5445
Ấn tượng đầu tiên là sách khá đẹp, chất lượng giấy tốt và chữ in dễ nhìn. Hơn thế nữa, với giọng văn khá hài hước của tác giả, cuốn sách đem đến cho người đọc những phương pháp hữu ích để có thể tập trung vào những việc quan trọng cần làm, tránh bị sao nhãng, tiết kiệm được thời gian và công sức trong công việc và cuộc sống.
Càng đọc mình càng thấy cuốn hút, không bị buồn ngủ như một số quyển khác có cùng chủ đề.
Mình đã không hối hận với sự lựa chọn này. Cảm ơn Tiki!
5
186614
2015-04-19 20:35:41
--------------------------
181229
5445
Đây là cuốn sách rất hài hước và bổ ích, mình đã đọc say mê để tìm lại con người mình, tự xoay chậm lại guồng quay cuộc đời để sắp xếp lại khối tài sản thời gian cho hợp lý. Con người dù nhiều ý chí và cố gắng đến đâu nếu không biết sử dụng thời gian đúng đắn thì một là lãng phí, hai là tự thiêu mình ra tro. Mình còn bắt gặp một câu nói rất hay thế này: Hòa nhập còn có tác dụng ngược, nó khiến bạn không quan trọng, nếu bạn cũng giống như người khác thì bạn còn cần thiết cho công việc này đến mức nào? Bạn có thể bị thay thế bất kỳ lúc nào! Một cuốn sách lấy lại niềm tin và thời gian tuyệt vời cho tất cả mọi người!
5
382812
2015-04-11 13:03:02
--------------------------
153713
5445
Quyển sách này mình thấy rất thú vị, kết hợp giữa kinh doanh và kỹ năng sống, dễ đọc nhưng nội dung không cũ, tác giả viết và  phân tích rất hợp lí, khiến mình cứ gật đầu không ngớt. Sách được xuất bản bởi TGM nên rất đẹp và chắn chắn, màu sắc tươi sáng, phông nổi, chữ chuẩn.
Nhiều bạn quen với dòng sách kỹ năng sống, có thể thử đọc quyển này để làm mới tư duy. Mỗi bài viết ngắn gọn (không có nghĩa là cụt đâu), không lê thê, nội dung sâu sắc. Rất đáng đọc!
5
201502
2015-01-27 10:48:26
--------------------------
146356
5445
Sách được in trên giấy đẹp, trình bày rõ ràng dễ đọc. Trong những chương đầu, tác giả giúp ta xác định vị trí hiện tại của bản thân, nhận ra tiềm năng của mình. Sau đó lần lượt quản lý tốt thời gian hơn bằng cách xác định mục tiêu của năm nay, hoàn thành những việc trong ngày, hạn chế sự phân tâm. Theo tác giả, nên dành ít nhất 80% thời gian vào 5 việc quan trọng của đời mình. Bên cạnh đó, "18 phút" không phải là quyển sách đưa ra các tips chạy đua với thời gian, nhồi nhét nhiều việc cùng lúc, mà khuyên ta hãy biết sống chậm lại để tăng tốc tốt hơn.
4
43888
2015-01-03 14:52:47
--------------------------
140251
5445
Cuốn sách có bìa ngoài đẹp, bắt mắt người xem. Chất lượng giấy in và in ấn rất tốt, 2 dịch giả dịch rất hay, dễ hiểu. Cuốn sách chỉ ra tầm quan trọng của 18 phút chuẩn bị ngắn ngủi thôi nhưng nó có thể quyết định toàn bộ đến cơ hội, tương lai và cuộc sống của bạn. Tác giả cũng chỉ ra những phương pháp rất hay giúp bạn chuẩn bị 18 phút một cách có giá trị nhất. Sách có tính thực tế cao, phù hợp với nhiều lứa tuổi. Cuốn sách do TGM group phát hành và được Trần Đăng Khoa và Uông Xuôn Vi biên dịch nên các bạn không cần phải lăn tăn về nội dung và chất lượng, còn được bán trên TIKI nữa, giá cả rất hời, chắc chắc sẽ không làm bạn thất vọng.
5
414919
2014-12-10 17:05:12
--------------------------
