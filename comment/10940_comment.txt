548021
10940
quyển này nằm trong bộ "Câu chuyện thời trang" nên đọc theo thứ tự:
- Những cô nàng Gucci
- Những thiên thần Armani
- Những công chúa Prada
Đọc truyện này mình nhớ đến bộ truyện tranh "Mắt lưu ly" mình cực thích hồi bé, cũng nói về các sự việc hàng ngày xung quanh ba cô gái xinh đẹp, mình cực thích kiểu truyện nói về cuộc sống của mấy cô quyến rũ, ăn chơi, am hiểu thời trang nên tìm được bộ này quá hợp gu luôn, đọc vui vui mà cứ ước mình cũng có cuộc sống như thế, thư thái, vui vẻ mà lúc nào cũng có những cô bạn hợp cạ sát cánh bên cạnh, tiếc là lúc đặt mua k tìm được hết nên đặt thiếu quyển prada :( trong 1 đêm đọc hết 2 quyển trong bộ này rồi thích kiểu truyện thế bày ghê
5
2116866
2017-03-20 02:36:17
--------------------------
36906
10940
Một cuốn sách khá thú vị dành cho những ai yêu thời trang và say mê những câu chuyện tình yêu lãng mạn.
Cuốn sách ca ngợi lòng nhiệt huyết của tuổi trẻ, hết mình vì ước mơ và đam mê cũng như trong sáng tạo nghệ thuật. 
Đường đời cũng không thể thiếu những sóng gió để các bạn trẻ mạnh mẽ vượt qua và trưởng thành, nhận ra mình trong tình cảm gia đình ấm áp, tình bạn và tình yêu.
Trong bộ ba xinh đẹp của trường ĐH thời trang tôi thích cô em út   Frankie nhất, một cô gái chân dài, ngây thơ, bốc đồng mà đáng yêu, trong sáng. Còn cặp đôi mà tôi thích lại là cặp Marina -  Rob, tình yêu của họ sexy, hài hước, đẹp và chân thành.
3
37817
2012-08-16 21:18:12
--------------------------
