458846
9763
Mình là một người yêu lịch sử, về Trung Quốc có tới 5000 năm lịch sử..., nên một số sự kiện của Trung Quốc mình cũng hay bị nhầm lẫn, thế là nay quyết định mua quyển sách này về đọc để nắm rõ hơn, thường thì review sách hạn chế nói về nội dung, nên mình chỉ nó về chất liệu sách thôi, quyển này cũng không dày lắm đâu, nhưng nó chứa đầy đủ những gì bạn cần biết đấy, ở mỗi trang luôn có hình minh họa, giấy rất mềm và đẹp, mình mua được giảm giá nhiều nên thấy rất thích 
4
448539
2016-06-25 15:29:02
--------------------------
372849
9763
Ấn tượng ban đầu đối với sách là sách khá đẹp, giấy được làm bằng chất liệu trơn nhìn rất hợp mắt và hơn hết hình ảnh bên trong khá bắt mắt màu sắc hài hòa gây hứng thú cho người đọc. Tuy nhiên không biết có phải mình am hiểu chưa tới sử hay không mà có nhiều chi tiết trong sách khác với nhiều cuốn sách còn lại mình từng đọc nên mình hơi lo ngại. Phần viết của cuốn sách khá hấp dẫn lôi cuốn người đọc gây hứng thú và cảm thấy thích học sử hơn.
2
366662
2016-01-22 13:03:31
--------------------------
370380
9763
Từ lâu mình đã luôn bị thu hút bởi lịch sử và văn hoá Trung Quốc nên đây thực sự là một cuốn sách đáp ứng và thoả mãn nhu cầu của mình.
Khi nhận được sách, mình khá bất ngờ vì sách được in màu, giấy cứng cáp và mỏng hơn so với mình tưởng tượng.
Nội dung không đi quá sâu vào nghiên cứu phân tích hàn lâm, cách viết đơn giản, dễ hiểu đảm bảo cung cấp đầy đủ và khái quát nhất cho người đọc về các thời kì và triều đại lịch sử Trung Quốc.
Cá nhân mình thấy đây là cuốn sách phù hợp cho tất cả mọi người muốn mở rộng thêm kiến thức về đất nước Trung Quốc. 
5
1012941
2016-01-17 17:33:42
--------------------------
325381
9763
Từ bé mình đã rất tò mò và thích thú khám phá về Trung Quốc bởi đây là 1 đất nước có rất nhiều điều thú vị, từ các triều đại, văn hoá cho đến con người nên mình rất vui vì tìm được bộ sách này. 
Sách được trình bày đẹp, bìa hơi cứng cầm rất chắc tay, bên trong được biên tập cẩn thận, có nhiều thông tin bổ ích mà không bị quá tải, dễ đọc. Đọc sách là thêm được bao nhiêu kiến thức đấy. 
Nói chung là rất ưng và đang chuẩn bị tiếp tục đọc những cuốn tiếp theo trong bộ này.
4
52328
2015-10-23 12:57:31
--------------------------
243779
9763
Trung Quốc là một quốc gia có lịch sử, nền văn minh lâu đời. Quá trình lịch sử lâu dài, dựng nước, giữ nước, phát triển của họ quả thực đáng khâm phục. Một đất nước trải dài với hơn 5000 năm lịch sử có thể tự hào về những phát minh, những công trình, thành quả mà mình đã tạo dựng ra. "Lịch sử Trung Quốc", với gần 200 trang sách, không nhiều nhưng đã mô phỏng lại, lột tả toàn bộ quá trình từ thời sơ khai lập quốc của "con cháu Rồng Thiên đến thời hiện đại, trong công cuộc giữ và phát triển đất nước. Sách nêu khái quát nhưng hầu như đầy đủ thông tin cần thiết cho những ai cần cái nhìn tổng quan về lịch sử Trung Hoa. Sách viết vừa đủ, k nhồi nhét quá nhiều thông tin thừa hay sâu xa, lại được đầu tư giấy tốt, in màu, khiến người đọc có cảm giấc dễ chịu và thích đọc, thích khám phá. Đây là một trong những quyển quan trọng, có giá tri.nhất trong bộ sách 12 quyển về Trung Quốc này. 
5
164758
2015-07-27 18:25:39
--------------------------
215923
9763
Mình vốn thích lịch sử Trung quốc vì có nhiều điểm tích câu chuyện liên quan đến từ các triều đại cho đến cuộc sống đời thường của người dân... Bàn về cách xử thế cách ứng đối và nhiều phát minh vĩ đại... Một nền văn minh to lớn đồ sộ và có sức ảnh hưởng to lớn đến nhiều nước xung quanh trong đó có Việt Nam... Dù gì với nền văn minh hơn mấy nghìn năm lịch sử với nhiều nhân vật tài ba... Gắn với nhiều biến độ g nhất là có liên quan đến Việt nam vì gần như triều đại nào bên họ cũng xâm lược nước ta cả... Dù vậy muốn hiểu Trung Quốc phải biết sử của họ... Một cuốn sách đáng để đọc...
5
568747
2015-06-26 23:19:40
--------------------------
151244
9763
Quyển sách mang đến cho mình một cái nhìn toàn diện và khách quan về lịch sử Trung Quốc. Cách viết của tác giả thật sự gây hứng thú cho người đọc, từng chặng, từng mốc lịch sử được viết đầy dễ hiểu, dễ nhớ nhưng cũng mang tính khách quan rất cao. Nhờ quyển sách mình hiểu thêm nhiều điều, biết thêm nhiều điều thú vị hơn về lịch sử Trung Quốc. Theo mình quyển sách này thực sự hay và bổ ích cho những bạn nào yêu thích, muốn tìm hiểu hay học chuyên ngành về Trung Quốc. Rất hay và bổ ích, rất đáng để đọc!
4
442739
2015-01-19 08:06:11
--------------------------
