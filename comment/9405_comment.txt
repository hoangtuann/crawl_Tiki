420265
9405
Giống như các tác phẩm khác của Meg Cabot tiểu thuyết tình yêu lãng mạn đan xen trinh thám khiến người đọc bị cuốn vào mối tình của họ. Điểm nhấn của truyện này là tác giả duy trì câu chuyện dưới các đoạn email nhân vật cho nhau khiến cho mạch văn mang tính trần thuật, gây bất ngờ cho người đọc. Cốt truyện cũng rất kịch tính, tạo cho người đọc tò mò về kẻ đã tấn công bà hàng xóm, cách mà nàng Mel rơi vào lưới tình thật bất ngờ với chàng trai kia, liệu xem sự thật gia thế giàu có của chàng hàng xóm "gỉa mạo" có bị bại lộ không,..
4
186012
2016-04-22 22:15:30
--------------------------
286850
9405
Sau khi đọc bộ Nhật kí công chúa, mình rất thích Meg Cabot nên tìm truyện cùng tác giả để đọc, và Chàng trai nhà bên không làm mình thất vọng. Có ai dám khẳng định đã hiểu hết người hàng xóm của mình chưa? Hàng xóm- một người thân thiết, quen thuộc với mình nhưng vẫn còn ẩn chứa những bí ẩn, góc khuất. Bị cuốn vào tình huống trớ trêu, giả dối của hàng xóm "thật", cô nàng Melissa đã phải lòng người hàng xóm "giả". Mel sẽ đối mặt như thế nào với sự thật và làm gì với tình yêu của mình. Một câu chuyện thú vị, hấp dẫn và cái kết như mơ sẽ thu hút bạn đọc.
4
361463
2015-09-02 00:56:57
--------------------------
221347
9405
Tác giả Meg Cabot quả thật đã trở lại đầy ngoạn mục.Tôi thấy đây cũng là một tiểu thuyết có chiều sâu.Tuy nhiên,tuyến nhân vật hơi cũ,yếu tố hài hước làm át đi tính chân thực của tình yêu đôi lứa.Mặc dù cốt truyện khá lạ nhưng mình cảm thấy vẫn hơi nhạt.Trong văn đàn Trung quốc hiện nay,ngôn tình đang chiếm trọn trái tim và lấy đi biết bao nước mắt của người đọc thì sự xuất hiện của Chàng trai nhà bên đã xóa tan đi cảm giác buồn,tiếc nuối của mình trong suốt nhiều lần đọc truyên ngôn tình.Một tác phẩm bạn nên đọc để trải nghiệm một cảm giác lạ thường nhưng cũng đầy sự thú vị
3
684744
2015-07-03 19:08:11
--------------------------
184508
9405
Nếu bạn còn trẻ (hoặc muốn nhớ về tuổi trẻ), đã đọc (hoặc kể cả chưa) bộ truyện đình đám Nhật ký công chúa, muốn đọc một cái gì đó nhẹ nhàng, hài hước nhưng vẫn có chút ý nghĩa nhân văn, hãy mua cuốn sách này.
Chỉ là một chuỗi những mẩu email ngắn (và cả dài) của các nhân vật, tưởng chừng như khập khiễng lộn xộn, khhó hiểu, nhưng lại không hề! Qua những bức thư điện tử ấy, người đọc có thể hòa mình vào cuộc hội thoại của các nhân vật, thay vì chỉ "nghe kể lại từ ngôi thứ ba". Một cách viết rất sáng tạo đấy chứ!
Về nội dung, như đã nói, mình tthấy đây là một câu chuyện tình cảm nhẹ nhàng và hài hước, có thể khiến người đọc nó có tâm trạng vui vẻ yêu đời hơn, tin vào tình yêu hơn. Nhưng không phải kiểu ngôn tình toàn màu hồng đâu nhé! Sách vẫn mang đến những mặt tối của cuộc đời, như việc những chàng trai con nhà "đại gia", chẳng thèm học hành phấn đấu mà chỉ chhờ ăn sẵn từ gia sản kếch sù của gia đình, những cô người mẫu ham mê thời trang và mù tịt về những thứ khác, những đứa cháu  chẳng thèm quan tâm tới ông bà,.. Nó khiến người ta mơ mộng nhưng không ảo tưởng về cuộc đời.
Chỉ có duy nhất 1 điêrm trừ là bao bì không được bắt mắt lắm. Có lẽ vì vậy mà cuốn sách chưa được nhiều người biết đến cho lắm. Hy vọng nhận xét của mình sẽ phần nào giúp ọi người.
5
9511
2015-04-17 21:07:07
--------------------------
99547
9405
Không như nhiều cuốn tiểu thuyết khác. truyện có kết cấu khá lạ, không phân ra từng chương hay liền mạch như vài truyện ngắn khác. "Chàng trai nhà bên" kết cấu theo kiểu từng đoạn hội thoại, ở đây là mỗi bức mail của từng người mà câu chuyện ở đây xoay quanh cô nàng Mel chuyên gia đi làm muộn. Mở đầu truyện có thể khiến người đọc cảm thấy lạ lẫm và khá khó hiểu, nhưng sau khi đọc được 1 đoạn thì câu truyện trở nên thú vị hơn. Truyện có nội dung bình thường, cụ thể là motip vẫn cũ nhưng điểm nổi bật chính là cách viết của tác giả khiến chuyện tình của Mel và anh chàng hàng xóm trở nên thú vị hơn. 
Điểm trừ chính là thiết kế bìa truyện, rất tẻ nhạt và không có điểm nhấn. Thật sự là cuốn truyện sẽ không được lựa chọn dựa trên hình thức.
3
125574
2013-11-15 22:29:18
--------------------------
