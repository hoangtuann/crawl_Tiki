528357
2832
Một cuốn sách mà mình phải đợi rất lâu mới mua được (sách mới tái xuất bản). Cuốn sách kể về một a chàng với một tấm bằng đại học loại ưu, một gia đình bề thế và một công việc ổn định phía trước. Nhưng cậu ta lại từ bỏ tất cả để dấn thân vào một cuộc hành trình vào miền hoang dã. Cuốn sách có nội dung khá lạ vs ng đọc nhưng lại cổ vũ khát khao dấn thân của ng trẻ. Và điều quan trọng nhất mà mình học được qua cuốn sách này đó là tuổi trẻ cần gạt bỏ định kiến, khát khao, theo đuổi đến cùng lẽ sống của riêng mình và(miễn là k vượt quá phạm vi đạo đức). Một cuốn sách dành cho người trẻ! 
4
1642349
2017-02-20 10:49:05
--------------------------
521353
2832
Nội dung tuyệt. Những câu chuyện trong sách như lời tự sự của tác giả về hành chình của chính mình. Cảm xúc!
4
896569
2017-02-08 14:51:48
--------------------------
483351
2832
Vào trong hoang dã của Jon Krakauer - đây là một cuốn sách có chiều dày và chiều sâu ấn tượng, mang tính chất nhân văn. Khi một con người quyết định từ bỏ tất cả để đi tìm tự do thật sự, để khám phá cả con người mình. Vượt qua những thử thách khó khăn, anh khao khát tìm được cái "hoang dã" trong mình. Cuốn sách là chuyến phiêu lưu với những trải nghiệm thần kì của Christopher McCandless. Lúc đầu mình không cảm thấy hay nhưng từ từ cuốn sách làm cho mình thay đổi cách suy nghĩ về nó.
3
1082676
2016-09-13 15:24:22
--------------------------
464244
2832
Phải nói đây là một trong những cuốn sách hay nhất mà tôi từng được đọc hay nói cách khác là cuốn sách gối đầu giường của tôi. Tôi biết đến tác phẩm này từ phim rồi mới mua trên tiki về đọc ( săn bao nhiêu lâu mới có hàng :p). Nói về nội dung, tôi thực sự thích thú ngay từ đầu bởi một cốt truyện hấp dẫn, tôi bị cuốn theo nó, cuộc phiêu lưu của anh chàng Siêu Xì Trum vô cung ly kì nhưng cũng đầy gian nan và thử thách. Anh vượt qua hết nơi này đến nơi khác, không một đồng tiền trong người, nhưng sau bao ngày tháng anh đã tới điểm đến, và ngục ngã nơi ngưỡng cửa thiên đường Alaska. Có người khâm phục anh vì ý chí mạnh mẽ dám từ bỏ ánh hào quang để đi theo tiếng gọi của trái tim, số khác lại gọi anh là một kẻ ích kỉ chỉ biết đến bản thân mình. Anh chết, nhưng đối với tôi mấy ai được như anh - chết nhưng nhận ra là mình đã từng sống, sống tự do, sống độc lập, sống có trách nhiệm với bản thân và sống như những gì mình muốn. Tuy nhiên điều quý báu nhất mà tôi học được từ Christopher McCandless, cũng như điều cuối cùng mà theo cách hiểu của tôi, anh đã hối hận đó là "Hạnh phúc chỉ có thực sự khi được chia sẻ".
5
256405
2016-06-29 23:56:22
--------------------------
463176
2832
Sách hợp với những bạn có máu đam mê xê dịch như mình. Một cuộc hành trình đúng chất hoang dã. Cuốn sách này sẽ khơi lên trong các bạn một ham muốn muốn trải nghiệm cuộc đời, muốn thử thách bản thân, từ bỏ những mệt mỏi của trường lớp của công việc để thực hiện một chuyến đi. Một câu chuyện ngắn nhưng đầy gay cấn và hồi hộp. Một cuốn sách nhỏ nhưng chứa đựng niềm thôi thúc lớn. Đặc biệt thích bìa sách này, nó có cái gì đó phong trần của một đời ngao du.
5
132597
2016-06-29 08:56:01
--------------------------
457678
2832
Mình đã từng mua quyển này bên Nhã Nam, nhưng vẫn quý và hay mua sách của Tiki, nên vào để lại vài lời nhận xét cho quyển sách mình để gối đầu mỗi ngày này.
Mình là một người sống theo chủ nghĩa xê dịch, nên quyển sách này rất thích hợp cho những ai mang tư tưởng giống mình. Cuộc phiêu lưu mạo hiểm của Chris như tiếp thêm có dũng khí cho những ai muốn sống theo tư tưởng của riêng mình, làm những việc mình cho là đúng,thoát khỏi guồng quay muôn thuở những gì xã hội cho là đúng, học hết thpt, học lên đại học, ra trường tìm việc làm, lập gia đình, cơm áo gạo tiền, rồi già đi và chết khi chưa làm được những gì bản thân muốn làm.
5
634230
2016-06-24 14:39:44
--------------------------
430555
2832
Mình không tiếc lời dành cho quyển sách này 2 chữ "tuyệt vời". Mất gần 2 ngày để mình đọc nó từ đầu đến cuối vào phải nói rằng mình cực kì tâm đắc với quyển sách này. Hoài bão, khác vọng của con người trước một thiên nhiên hoang dại được tác giả khắc họa nên không chỉ bằng câu chuyện của Chris McCandless mà còn của nhiều thanh niên đầy đam mê khác mà trong đó Chris nổi bật lên với nét cá tính mạnh mẽ khác người, phong trần và nhiệt huyết. Mình ấn tượng nhất với bức thư của Chris gửi ông già Ron, đọc đi đọc lại hoài. Nếu bạn là một tâm hồn tự do, hãy mua ngay quyển sách này khi bạn nhìn thấy nó!
5
151503
2016-05-15 13:12:47
--------------------------
406663
2832
Sau khi xem xong bộ phim này thì cảm xúc của mình thật sự đã dâng tới đỉnh điểm rồi. Có lẽ vì quá đồng cảm với anh chàng mang trái tim bíêt yêu thương rung động vơí thiên nhiên hoang dã. Nếu có thể một lần, nhất định mình cũng sẽ bở lại tất cả để sống một cuộc đời anh ấy. Vậy nên đã quyết tâm mua sách bằng đựơc. Tiếc là chờ tiki lâu quá, nên nóng lòng tìm em ấy ở hiệu sách. Phim ảnh sẽ không thể đem lại cho ta cảm xúc trọn vẹn như khi ta lật qua lật lại từng trang sách. Chắc chắn đây là quyển sách yêu thích nhất của mình, khi mà từng câu chữ như thổn thức, thầm vẽ ra những gì mình đang suy nghĩ. Cuốn sách gửi gắm bíêt bao là bài học với những thông  địêp đẹp đẽ.
5
857944
2016-03-28 18:31:27
--------------------------
319234
2832
Tôi đã đọc quyển sách này ngay sau khi xem bộ phim "Into the wild". Nội dung sách không hoàn toàn giống như nội dung phim, tuy nhiên tinh thần khao khát tự do đầy phóng khoáng thì vẫn còn nguyên vẹn. "Vào trong hoang dã" đã truyền cảm hứng cho tôi rất nhiều, khi đọc xong tôi đã cảm thấy cực kỳ đồng cảm với nhân vật chính, một cảm xúc rất khác lạ... Chỉ mong có thể từ bỏ tất cả để đi theo ước mơ, làm tất cả những điều mình thèm muốn và khao khát, sẵn sàng đương đầu với mọi khó khăn, trở ngại trên đời. Dù cái giá phải trả có lớn đến thế nào đi nữa :) thì cũng còn hơn là sống cả một đời u buồn, tẻ nhạt, lùi lũi như con rùa sau xó cửa :))
Bìa sách đẹp, chất lượng giấy in cũng rất tốt. Trình bày gọn gàng, hợp lý.
4
293599
2015-10-08 11:00:39
--------------------------
312655
2832
Anh chàng Chris McCandless đã bị xem là một gã điên cuồng và thiếu hiểu biết khi tự mình lao vào vùng hoang vu mà không một chút chuẩn bị, để rồi phải bỏ mạng tại chốn hoang vu. Cái chết của anh ấy đã để lại những tranh cãi gay gắt về cái gọi là lòng dũng cảm. Vừa sợ hãi, vừa hồi hộp xen lẫn phấn khích và cuối cùng là đi đến cảm thông, thán phục... đó là những cảm xúc mà tôi có được khi đọc quyển sách này. Lúc đầu tôi cho rằng không có lí do gì để khuyến khích những người trẻ tuổi lao vào cuộc sống bất cần như anh chàng này, nhưng rồi tôi bị thuyết phục bởi cái triết lí giản dị mà tác giả đã chỉ ra. Rằng chúng ta không sống để làm hài lòng mọi người xung quanh mình, không thể ở lì trong một nơi chốn như một cái lồng mà người ta gọi là "chỗ an toàn",  ta phải sống và chết như cái cách mà bản thân muốn, mạo hiểm là một trong những cách để ta tìm ra bản ngã của mình.Chris chán ngấy với sự giả tạo và thực dụng của những người xung quanh, anh ta thực hiện hành trình của riêng mình, hành trình tìm lấy sự tự do và bản ngã của mình.
4
644100
2015-09-21 19:49:36
--------------------------
312239
2832
Quả thật là mình được biết đến tác phẩm này qua chính bộ phim cùng tên "into the wild" (dịch tiếng Việt là vào trong hoang dã). Mình đã quyết tâm tìm đến tác phẩm này để chiêm nghiệm hết những bài học của tác giả gửi gắm. Khi xem phim này mình đã rất thích, và được truyền cảm hứng vô cùng, nhưng khi đọc thì mình lại còn thích và yêu hơn nữa. Nhà văn đã khắc hoạ rất rõ nét hình ảnh anh nhân vật chính sống những năm tháng tuổi trẻ của mình trong những chuyến đi. Rất khuyên đọc!
4
114883
2015-09-20 23:06:15
--------------------------
309795
2832
Mình đã tìm mua ngay quyển sách này sau khi xem được bộ phim này trên mạng. Cuốn sách truyền cảm hứng cho mình rất nhiều, đọc xong cảm thấy rất đồng cảm với nhân vật trong quyển sách, và nó cũng mang lại một cảm xúc rất khác lạ...

Mình thật sự rất ngưỡng mộ những người có thể từ bỏ tất cả để đi theo ước mơ, làm tất cả những điều họ mong muốn và khao khát, sẵn sàng đương đầu với bất cứ khó khăn, trở ngại nào. Nhờ quyển sách này mà những ước mơ trong mình sẽ được nuôi dưỡng và chờ đến ngày sẽ thực hiện nó!

Cuốn sách này là một trong những quyển sách mình yêu thích nhất!! Bạn nên đọc thử nó :)
5
132887
2015-09-19 09:48:20
--------------------------
301022
2832
Một cuốn sách truyền cảm hứng tuyệt vời. Cuốn sách khiến người đọc nhận ra chính bản thân mình trong từng bước chân lang thang của 'Supertramp", phần nào gợi lên trong mỗi người khát khao được một lần trải nghiệm, được sống trong hoang dã, hay ít nhất, truyền cho người đọc sự can đảm muốn dấn thân trải nghiệm những điều mới mẻ trong cuộc sống. Cái chết của Chris không làm cho người đọc nhụt chí, mà ngược lại, thấy cảm động vì một người sẵn sàng sống vì lý tưởng của mình, và chết trong lý tưởng ấy.
4
44934
2015-09-14 13:45:59
--------------------------
296384
2832
Bìa sách cùng lời dẫn ấn tượng là điểm đầu tiên thu hút tôi đến với quyển sách này: rất hoang dã và mang một chút bất cần. Tôi ao ước bản thân có đủ sự dũng cảm như nhân vật chính Christopher, dám vứt bỏ tất cả: công việc, cuộc sống tiện nghi để lao mình vào chuyến hành trình khám phá vùng đất hoang dã, xa xôi và khắc nghiệt. Có lẽ đây chính là minh chứng hùng hồn nhất cho câu nói "Tuổi trẻ là những chuyến đi, đi để sống và để trải nghiệm". Ở tuổi 20, quyển sách đã truyền cảm hứng cho tôi rất nhiều. Một quyển sách tuyệt vời.
5
35746
2015-09-11 02:41:44
--------------------------
293999
2832
Tôi tình cờ xem được bộ film trên truyền hình, mê mẩn đến nỗi tìm đọc luôn cuốn sách này. Ko mua được sách, phải kiếm ebook mà đọc. Cảm thấy một sự đồng cảm đến lạ lùng. Ko có gì hối tiếc cho một quyết định sống như vậy. Cá nhân tôi, nhờ cảm hứng từ cuốn sách, đã tự mình thực hiện chuyến hành trình xuyên Việt 1 mình bằng xe máy, ăn bờ ngủ bụi, trèo đèo lội suối. Hạn chế tất cả các tiện nghi, công nghệ có thể. Giao hòa với thiên nhiên, những khoảnh khắc cảm xúc thăng hoa đến tột đỉnh. Tôi nhớ trong film có cảnh anh chàng ăn trái táo, nghẹn ngào thốt lên "đây là quả táo ngon nhất tao từng ăn". Tôi cũng vậy, với 1 trái lê, vừa chạy xe vừa ngấu nghiến ...thơm ngon lạ lùng. Cả cuộc đời tôi, cho đến hiện tại, sống với thiên nhiên chính là những giây phút tuyệt vời nhất...
5
800261
2015-09-08 22:26:17
--------------------------
286037
2832
Tôi đã xem bộ phim  Into the wild của Sean Penn chuyển thể từ cuốn sách cùng tên của Jon Krakauer nên sẽ mà bỏ qua tác phẩm này, khi đọc lại cuốn sách này thì gợi lại cho tôi hình ảnh đơn độc nhưng thú vị của Christopher McCandless trong hành trình tìm kiếm ''hạnh phúc'' cho riêng mình mặc dù anh có trong tay mình mọi điều kiện tốt nhất về công việc , gia đình . Tôi rất thích sách của Nhã Nam nhưng cuốn sách tôi mua hoàn thiện không được tốt lắm, không biết do Tiki bảo quản sách hay do Nhã Nam nhưng cuốn sách tôi mua bị vài vệt ố vàng ở trong sách.
5
28807
2015-09-01 13:07:03
--------------------------
275231
2832
Thật sự tôi đã chờ phiên bản tiếng Việt của quyển này rất lâu. Sau khi xem bộ phim được chuyển thể từ bản tiếng Anh - Into The Wild, tôi rất mong mỏi được cầm trên tay ấn bản được Việt hóa. 
Đây là câu chuyện có thật được tác giả thuật lại - cuộc hành trình của tuổi trẻ mà có người sẽ nghĩ là nông nổi, ngông cuồng, người khác lại thần tượng nó như một triết lý sống. 
Bỏ qua cuộc đời "sẽ" thành công, người con trai ấy mang theo mình một khát vọng sống cùng thiên nhiên đã đơn độc hành trình dọc nước Mỹ để đến Alaska. Dù kết thúc sẽ khiến nhiều người buồn lắm, nhưng nó luôn đọng lại trong tâm tư độc giả một thông điệp ngầm đầy ý nghĩa!
5
364576
2015-08-22 18:55:17
--------------------------
274738
2832
Cũng là 1 backpacker nên luôn có một sự ngưỡng mộ và tôn trọng với những người như Alexander Supertramp. Và mình tin là trong sâu thẳm tâm hồn của bất cứ một kẻ yêu thích lang thang nào cũng đều có ước mơ thực hiện được giống như anh, chỉ là hoàn cảnh mỗi người thế này hay thế khác, họ thực hiện ước mơ của mình theo những cách khác nhau. Trong một thế giới có vẻ như ngày càng hỗn độn và mong manh, tìm về với thiên nhiên hoang dã giống như một sự giải thoát cho tâm hồn của chính mình, để biết so với tự nhiên, mình thật nhỏ bé như thế nào, để sống với trọn vẹn những niềm đam mê của minh. Supertramp không trở về, anh ở lại mãi với The Wild, nhưng anh đã là niềm cảm hứng cho vô vàn những traveller vẫn hàng ngày rong ruổi trên đường, hòa mình vào sự vĩ đại của tự nhiên.
5
631
2015-08-22 10:55:29
--------------------------
271339
2832
Khi biết Nhã Nam tái bản lại cuốn này mình rất mừng, vì Thaihabook đã không còn sản phẩm này nữa, dù đã xem film rồi nhưng đọc sách vẫn thích hơn. 
Hình thức sách thì mình không thích lắm, cả về màu giấy lẫn mực in lẫn bìa, nó quá xấu đi :((. Nhận sách mà hơi thất vọng. Nhưng nội dung thì tất nhiên rồi - tuyệt vời, tác phẩm viết về một người trẻ dám nghĩ, dám làm, bỏ hết những thói thường và luật lệ của cuộc sống, đọc mà rất phấn khích vì gặp được nhân vật ấy, cảm thấy cái phần ấy ở trong mình nhưng mình lại không dám làm như họ. Tuy phải nhận lấy một kết cục đáng buồn nhưng chốt lại, đó lại là một cuộc đời đáng sống và rất đáng ngưỡng mộ.
Cho tác phẩm 5 sao luôn!!!! 
5
464538
2015-08-19 07:52:20
--------------------------
269359
2832
Tôi chờ đợi rất nhiều để có được cuốn sách này khi nhà xuất bản Thái Hà không còn bán cuốn này nữa, nhưng khi biết được Nhã Nam biên tập lại tôi rất vui mừng. Tôi vẫn thích bìa sách cũ hơn nó gần với tác phẩm hơn là bìa mới của tác phẩm này được lấy hình ảnh từ nam nhân vật chính của bộ phim được làm từ tác phẩm. Tôi chỉ dành hai từ ngưỡng mộ đối với Christopher Johnson McCandless. Rồi tôi nghĩ có phải xã hội Mỹ đi trước mình nhiều quá để những tinh thần như của Chris sống ở thập kỷ 90 của thế kỷ 20 đã cảm thấy: "...Chưa từng mãn nguyện với cuộc đời như hầu hết mọi người đang sống. Tôi luôn muốn được sống mãnh liệt hơn và phong phú hơn..." khát khao chinh phục sự tự do, phiêu lưu về thiên nhiên hoang dã. Tôi khóc khi đọc đến lời từ biệt của Chris: "Tôi đã có một cuộc sống hạnh phúc và xin cảm ơn chúa. Tạm biệt và cầu chúa phù hộ cho tất cả mọi người".
5
256925
2015-08-17 12:24:57
--------------------------
260920
2832
Đây là một câu chuyện đầy ám ảnh và cảm động về khát khao được tự do sống và trải nghiệm của người trẻ. Đọc để cảm nhận những đam mê, khát vọng mãnh liệt của nhân vật, dám buông bỏ tất cả và chấp nhận cả cái chết để được sống đúng với tâm hồn mình. Tuy có chút tiếc nuối và buồn bã khi kết thúc, nhưng đây vẫn là một câu chuyện đẹp và truyền động lực mạnh mẽ đến người khác. Câu chuyện cũng được chuyển thể thành phim điện ảnh khá hay, bạn cũng nên xem qua để cảm nhận rõ hơn.
5
253168
2015-08-10 19:26:05
--------------------------
257491
2832
Đọc cuốn sách này tôi cảm nhận được một cách suy nghĩ khác về cuộc sống và tự do, nhất là khi nhận ra cuộc sống của mình cứ đơn điệu lặp lại hằng ngày. Chris đã bảo rằng cuộc sống chỉ có ý nghĩa khi mỗi ngày là một sự khám phá và trải nghiệm mới mẻ. Tôi thấy có nhiều người chỉ trích, cho rằng lối sống của Chris là ngu xuẩn, làm tổn thương những người thân yêu của cậu nhưng theo tôi Chris có một ước mơ và anh can đảm thực hiện nó- đó là một điều tuyệt vời, cậu cũng đã nghiêm túc thực hiện 4 năm đại học một cách xuất sắc theo nguyện vọng bố mẹ rồi mới thực hiện ước mơ của mình. Cái chết của Chris thực sự là một điều đáng tiếc, vì như cậu nói đây là chuyến du hành cuối cùng và có lẽ cậu sẽ trở về nhà. Tuy nhiên, tinh thần và con người Chris đã được thể hiện và ghi nhớ trong những con người biết đến cậu như những người đọc chúng ta.
5
129176
2015-08-07 18:18:31
--------------------------
256364
2832
Sự cô độc, sự thiếu thốn, cũng như sự sợ hãi đã nhường chỗ cho tự nhiên. Giờ đây, khi vào trong hoang dã anh ấy mới được là chính mình, dù cho có nỗi khó khăn nào, tất cả đều biến mất để hòa mình vào thiên nhiên. Anh ấy thật sự mạnh mẽ, mạnh mẽ khi đấu tranh với cái xã hội vô cảm kia, đấu tranh để thực hiện niềm đam mê của chính mình. Mình nghĩ khó có ai có đủ can đảm để bỏ tất cả cuộc sống sung sướng để thực hiện niềm ước mơ đó.
4
730250
2015-08-06 20:17:59
--------------------------
255502
2832
Đọc xong cuốn này, bạn thực sự thấy ham muốn của con người là vô hạn. Có tài năng, có học thức, giàu có, có sức thu hút mọi người và được yêu thương, thế là quá nhiều cho một người để sống tốt trong xã hội. Nhưng Chris hướng đến những điều hoàn toàn khác, cái anh ham muốn là những trải nghiệm hoàn toàn mới mẻ, là phiêu lưu mạo hiểm. Và Chris cực đoan từ bỏ hết những thứ mà theo anh là thuộc về một cuộc sống ổn định, đơn điệu, giả tạo để hiến mình cho cuộc sống anh khao khát. Cuối cùng anh phải trả giá cho đam mê anh theo đuổi bằng mạng sống của mình.
Tác giả không có nhiều tư liệu để viết về những điều mà Chris trải qua, nhưng ông phân tích nhiều con người với hành động tương tự như Chris, say mê lao vào Alaska để thử thách bản thân mình trước hiểm nguy, viết về những con người đã đi qua cuộc đời anh trong hai năm phiêu bạt, từ đó rọi chiếu vào Chris: Anh sống hết mình, dũng cảm đi theo đam mê, tiếng nói của trái tim mình. Cái chết của anh là một tai nạn như trong nhiều kiểu tai nạn mà mỗi người đều có thể gặp phải trong cuộc sống, chứ không phải là hậu quả đương nhiên của một lối sống bốc đồng, vô trách nhiệm, thiếu hiểu biết như nhiều người nói về anh.
Tác giả cũng phân tích những suy tư, tình cảm, lý luận của Chris dẫn anh đến sự say mê cuộc sống hoang dã để chỉ ra những nông nổi của tuổi trẻ trong chính những suy tư đó. Và thấp thoáng trong tác phẩm, ông cho rằng: "ngay cả lời biện hộ hùng hồn nhất dành cho những hành động liều lĩnh nhất đều có vẻ ngu ngốc và rỗng tuếch" nếu bạn chứng kiến nỗi đau của những người yêu bạn bị bạn bỏ lại khi bạn tận hiến cho cuộc sống đam mê của riêng mình.
Nội dung cuốn sách thật tuyệt diệu, và hình thức cũng tuyệt không kém, không cái bìa sách nào thể hiện tác phẩm đẹp hơn thế.

5
404264
2015-08-06 08:41:36
--------------------------
247532
2832
Mua quyển này vì xem bộ phim Into the wild (2007), xem phim đã thấy mê cái sự tự do và cuộc sống hoang dã của Alex nhưng đọc truyện thực sự mới cảm thấy hết được ý nghĩa của những việc Alex làm. Bỏ lại tất cả để trở về với thiên nhiên hoang dã, anh ý đã chọn một cuộc sống tự do, không bị chi phối bởi bất kỳ ai, không bị xoay quanh đồng tiền. Đây mới thực sự là một cuộc sống ý nghĩa, tự mình làm chủ, thoải mái, phiêu du, tự do tự tại.
5
364243
2015-07-30 10:38:40
--------------------------
246440
2832
Đây là một câu chuyện sâu sắc, hấp dẫn về Chris McCandless, một người đàn ông trẻ tuổi thông minh, mạnh mẽ, và duy tâm, người đã cắt đứt mọi quan hệ và rằng buộc với gia đình ở tầng lớp trung lưu của mình. Anh sau đó tái sinh  thành Alexander Supertramp, một kẻ sống lang thang  với một chiếc ba lô, du ngoạn khắp nước Mỹ. McCandless đã kết thúc cuộc hành trình của mình vào năm 1992 tại Alaska, khi ông đi một mình vào nơi hoang dã phía bắc của Denali. Anh không bao giờ quay trở lại. Anh đã chấp nhận hy sinh để đạt được sự tự do tuyệt đối và vẻ đẹp hoang sơ mà anh tìm kiếm bấy lâu chính là ở nơi khắc nghiệt nhất trái đất .
5
65620
2015-07-29 18:04:18
--------------------------
246323
2832
Quyển sách nên đọc sau khi xem bộ phim chứ không phải đọc nó trước khi xem phim. Bởi quyển sách là sự nối dài tiếp diễn những câu hỏi, khúc mắc còn lại trong bộ phim.
Quyển sách là phần còn lại của người sống. Alex đã để lại yêu thương ở những nơi anh đi qua trong suốt cuộc hành trình hoang dã đó. Nếu bộ phim là một tông lạnh và hoang vắng thì quyển sách là chiều sâu cảm xúc và ấm áp.
Những đoạn trích đến từ những quyển sách khác cũng là một phần thú vị trong quyển sách này.
5
581635
2015-07-29 17:54:20
--------------------------
243398
2832
Đã đọc phiên bản gốc tiếng anh của quyển này, qua lời văn của tác của tác giả về nhân vật Chris thật sự quá hay, cảm xúc qua từng lời văn thật sự khó tả. Khi đóng quyển sách này lại thì trong lòng như có một cảm giác gì đó nghèn nghẹn, một con người - một nơi hoang dã, Chris đã chọn cho mình một cách sống không có ai có thể tưởng tượng nổi. Dù chỉ mấy tháng sau xác của anh được tìm thấy nhưng khó có ai có thể quên nổi Chris. Khi vừa thấy phiên bản tiếng Việt này mình đã mua ngay và hy vọng nó vẫn sẽ cho mình những cảm xúc tuyệt vời như phiên bản tiếng anh.
5
136299
2015-07-27 14:00:57
--------------------------
239958
2832
Trong xã hội văn minh và hiện đại này, ai lại không muốn tận hưởng những thứ đồ xa xỉ và một cuộc sống no đủ cơ chứ. Thế nhưng,  Chris McCandless lại không nghĩ như vậy. Anh đã chọn cách sống lạ thường, đó là "Vào trong hoang dã". Anh đã dấn thân vào một cuộc hành trình đầy hiểm nguy với những điều không thể đoán định. Đây xứng đáng là một tác phẩm kiệt xuất bởi sự kịch tính và mãnh liệt. Đọc xong cuốn sách này, ắt hẳn chúng ta cũng sẽ bắt đầu có những ý định điên rồ và dũng cảm như Chris.
5
112376
2015-07-24 10:32:10
--------------------------
237943
2832
Nhiều người xem Chris McCandless là một tên ngớ ngẩn, hoang tưởng khi một mình tiến vào vùng Alaska hoang dã đầy tuyết trắng để " dựa vào đất đai mà sống", cũng có nhiều người coi anh là một con người đầy dũng cảm thoát ra được khỏi cuộc sống tầm thường để tìm đến lý tưởng của riêng mình. Dẫu cho chỉ vài tháng sau hành trình ấy, xác của anh được tìm thấy trong hoang dã, thì tên tuổi của anh sẽ không dể dàng bị quên đi như những con người " tầm thường" theo cách nói của Anh.
- một cuốn sách ai cũng nên đọc -
5
678932
2015-07-22 21:18:34
--------------------------
