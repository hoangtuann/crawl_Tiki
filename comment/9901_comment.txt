367680
9901
Cả series này rất hay mà chưa được dịch mấy, mình đọc cuốn này trên miki thấy cũng khá hay nên cũng cân nhắc mua sách giấy.
Cốt truyện rất hay, nhiều tình huống hấp dẫn gay cấn và hồi hộp nhất là đoạn giải cứu 2 cô cháu 
Đây là cuốn sách không chỉ có tình cảm yêu đương nam nữ mà còn có tình cảm gia đình, tình đồng đội, tình bạn và lòng dũng cảm vượt lên chính những mặc cảm của bản thân mình. Đọc sách giúp mình yêu đời hơn và trân trọng những gì mình có.
3
306081
2016-01-12 16:18:54
--------------------------
319904
9901
Thiên thần của anh là một quyển tiểu thuyết đặc sắc: sinh động, thực tế, hấp dẫn và lãng mạn với nội dung , cốt truyện độc đáo, cũng khá là mới mẻ. Câu chuyện xoay quanh cô giáo trẻ Mia và người thương binh SEAL- Frisco. Tác giả đặt nam  nhân vật chính của mình vào hoàn cảnh bị tàn tật, mất khả năng đi, khiến Frisco tuyệt vọng. Và trong hoàn cảnh đó bà mang Mia vào cuộc sống của anh. Theo dõi dòng chảy êm đềm của câu chuyện, nỗi đau, sự gắng gượng lẫn buông xuôi được lột tả chân thực, ý thức vượt qua nghịch cảnh và nỗ lực thoát khỏi  hoàn cảnh tồi tệ thể hiện rõ trong tiểu thuyết. Bên cạnh tình yêu của hai nhân vật chính có tình đồng đội đáng quý và tình cảm thuần khuyết dễ thương của Tash và Thomas. Chuyện không có nhiều nhân vật, đơn giản nhưng vẫn là một quyển tiể thuyết hay.
3
64972
2015-10-09 22:03:23
--------------------------
309485
9901
Nói thật là tại vì nhìn hình bìa có con bé trắng trắng tròn tròn dễ thương quá, không cầm lòng nổi nên mình mới mua nhưng chưa đọc liền. Mình cũng chưa từng đọc truyện của Suzanne bao giờ. Mà bẵng đi 1 thời gian mới sực nhớ ra là mình cũng có cuốn này mới lôi nó ra nghiên cứu. Đây quả là 1 câu chuyện cảm động về tình người, tình yêu cũng như đề cao tinh thần vượt lên chính mình dù cho hoàn cảnh có đánh gục và nghiệt ngã như thế nào. Từ 1 chàng lính Seal tài giỏi bỗng Alan trở thành người không còn khả năng đi lại, thấy tội nghiệp anh ghê, anh tự tạo vỏ bọc vô tình cho bản thân mình nhằm trốn tránh tất cả mọi thứ. Và lúc ấy, chính Mia đã xuất hiện và kéo anh trở về thực tại, thắp lên niềm tin tưởng chừng như đã vụt tắt trong anh, giúp anh vực dậy, vượt qua sự mặc cảm và theo lẽ tất yếu của cuộc sống, họ yêu nhau. Mình thấy Mia đúng thật là thiên thần dành riêng cho Alan. Truyện này tuy lãng mạn nhưng không sến rện đâu nha, có pha thêm chút hài hước lẫn niềm đau, là đau lòng ấy. Nhưng nói chung thì truyện hay, mọi người nên thử đọc, giá cả cũng ở mức chấp nhận được. 
4
42247
2015-09-18 23:58:05
--------------------------
232889
9901
Có người từng nói "Kẻ thù lớn nhất cuộc đời chính là bản thân bạn". Qủa thật vậy, khi Alan - một người lính SEAL - dũng cảm mất đi khả năng đi lại, phải từ bỏ công việc yêu thích, đắm chìm trong rượu và cảm giác mất mác, dường như hình ảnh người lính dũng cảm trước đây đã bị xóa bỏ. Nhưng cô nàng hàng xóm vừa chuyển đến Mia cùng đứa cháu mà anh bắt buộc phải nuôi dưỡng Tash đã giúp Alan hiểu ra rằng anh vẫn "hoàn thiện". Alan đã vượt qua mặc cảm, tuyệt vọng để mở rộng lòng mình với cuộc sống xung quanh. Truyện rất lãng mạn và nhiều tình tiết hài hước như chiếc sofa màu hồng mà Tash nằng nặt đòi chú mình phải mua để rồi chính Alan bị đồng đội trêu chọc vì chiếc sofa này. 
4
35746
2015-07-19 13:59:34
--------------------------
126483
9901
Luôn luôn, truyện của Suzanne Brockmann chạm tới nỗi lòng sâu thẳm trong con tim độc giả. Truyện của bà quá đỗi tự nhiên, như đang xem một thươc phim điện ảnh, nhưng cũng không thiếu hồn của trang sách. Trách sao cư đọc truyện của bà là không dứt ra được, dù có là 1,2 giờ sáng (haha )
Thiên thần của anh là truyện về một ng đàn ông không hoàn hảo, anh ta tự dìm mình trong sự hối tiếc, trong sự bất lực vì sự kiêu hãnh của một con người không bao giờ biết bỏ cuộc. 
Và của một người con gái cũng không bao giờ biết bỏ cuộc, nhưng là không biết bỏ cuộc để ngừng yêu thương. 
Thật đẹp biết mấy và dũng cảm biết bao, nhưng cũng đau lòng tới nghẹt thở
Vâng, những thiên thần, " Liệu họ có thực sự tồn tại? " :)
4
177150
2014-09-19 08:26:27
--------------------------
113922
9901
Alan Francisco, một lính SEAL, đã gần như mất hết hy vọng khi một bên đầu gối bị thương và làm anh không có khả năng chạy hay chiến đấu như một người lính được nữa. Diễn biến tâm lý, sự suy sụp và tuyệt vọng của Alan được mô tả rất chân thực. 
Thế rồi Mia và Tash xuất hiện, mang theo tình yêu và tình ruột thịt, một lần nữa giúp anh hoàn thiện. Mình rất thích Tash – cháu gái của Alan, một cô bé hồn nhiên, thông minh và rất mạnh mẽ. Cả Mia, một giáo viên lịch sử đầy nhiệt huyết và nhân hậu, hai người đều yêu thương và cho Alan sức mạnh để một nữa bảo vệ và chiến đấu vì những gì từng, đang và sẽ thuộc về mình. Văn phong của Suzanne Brockmann rất lôi cuốn, tính cách nhân vật được khắc hoạ rõ nét, nội dung sâu sắc và để lại nhiều dư âm. Mình cũng rất ấn tượng với đoạn miêu tả Alan lắng nghe tiếng của màn đêm, một việc bình thường với người lính nhưng lại rất lạ thường và đầy chất thơ với những người khác. Có lẽ Suzanne Brockmann hiểu rất rõ cuộc đời của những người lính, mới có thể diễn tả được sinh động như vậy.
Tash, và Mia đều là những thiên thần của Alan.
Và có, những thiên thần có tồn tại, giữa chúng ta.
5
198930
2014-06-06 20:41:17
--------------------------
105886
9901
Lại thêm một cuốn sách của  Suzanne Brockmann mà mình rất thích. Ở  Suzanne Brockmann không có những tác phẩm có nội dung tương tự nhau, “thiên thần của anh” ta bắt gặp những trải nghiệm đau thương, những nỗi dằn vặt dày vò đau đớn đến oán hận bản thân mình của một anh chàng thương binh đang rất có tiềm năng phát triển của đội quân SEAL- tổ chức quân đội tinh nhuệ nhất của Mỹ. Có đôi lúc ta dường như bị nhốt trong cái thế giới nội tâm của Alan Frisco: đầy bất lực, mệt mỏi và tức giận. Mỗi một tình huống đưa ra, dù ít dù nhiều  Suzanne Brockmann vẫn đưa anh về vấn những bất lực và oán hận cuộc sống: anh chảng thể nào đem số đồ ăn đưa vào nhà, chẳng thể nào tự mình vác những vật dụng đã mua lên cầu thang, chẳng thể nào đưa cô cháu gái nhỏ vào bệnh viện khi con bé sốt cao mà không nhờ đến Mia, chẳng thể nào đánh lại một gã to xác để bảo vệ Mia và đứa cháu gái, cũng chẳng thể nào thực thi cái kế hoạch đột nhập cứu họ mà phải đứng ở ngoài làm nhiệm vụ quan sát. Thế nhưng, từng bước một cái oán hận và bất lực ấy đưa anh về với một cuộc sống thực tại, một cuộc sống mà anh vẫn sống với niềm tự hào của mình, vẫn sống như là một người có ích cho xã hội như cái ước mơ bay cao của anh và mở rộng vòng tay đón nhận sự giúp đỡ của những người xung quanh.
Mia đến với anh như một thiên thần, giúp đỡ anh từng bước một bước ra cái ngột ngạt và đau thương giày vò anh. Bản thân mình rất thích cách dịch tựa truyện của cuốn sách mình, có thể nói nó hay hơn cả tựa đề gốc (Frisco’s kid) vì dù là Mia hay cháu gái của anh, thì họ đều là những thiên thần đưa anh về đúng con mình, một anh chàng vui vẻ, cuồng nhiệt và ngập tràn niềm tin.
*Một số nhận xét khác:
- Bìa cuốn sách khá dễ thương.
- Nội dung hay và mô tả nội tâm sâu sắc.
- Tình tiết đơn giản nhưng đều có những ẩn ý riêng biệt
Mình cho cuốn sách này 9/10 điểm
5
15919
2014-02-09 16:12:36
--------------------------
80410
9901
Đây là cuốn sách hấp dẫn nhất của Suzanne mà mình được đọc. Một Mia dịu dàng, ngọt ngào, đầy tình thương; một Alan Frisco giàu cảm xúc luôn được che giấu dưới vẻ ngoài khắc nghiệt. Câu chuyện tuy không mới nhưng khiến mình rất xúc động và khó có thể nào quên được. Câu chuyện là sức mạnh của nghị lực, của tình yêu có thể chiến thắng nỗi sợ hãi, sự đau khổ của quá khứ. Tình yêu thật sự có sức mạnh chữa lành những vết thương tưởng chừng là mãi mãi trong tâm hồn. Số phận đã mang Mia đến như ánh sáng phía cuối đường cho một Alan Frisco tưởng chừng đã buông xuôi và quay lưng lại với cuộc sống, kéo anh ra khỏi vực sâu tăm tối của tâm hồn và thắp sáng lại niềm tin nơi anh. 
5
125574
2013-06-12 18:37:29
--------------------------
76954
9901
Truyện của Suzanne Brockmann luôn gây cho người đọc một cảm giác mới mẻ, truyện của bà cốt truyện hầu như không tương tự hay na ná nhau, điều này làm mình rất hâm mộ bà. Truyện Thiên Thần của anh lần này cũng vậy, mình đặc biệt thích tình yêu này. Một tình yêu vượt qua những rào cản, một tình yêu thương chân thành nhất, khi nhân vật nam chính Alan đã tưởng chừng như tuyệt vọng thì Mia xuất hiện, như một thiên thần đưa anh ra cõi tăm tối tuyệt vọng. Cách viết của tác giả không làm mình thất vọng bao giờ, một cách viết lôi cuốn thu hút người đọc trong từng trang truyện. Tuy nhiên, mình không thích bìa truyện lắm, mình mong đợi hơn nhiều so với hình ảnh của một bé gái ngồi trên bãi cát.
4
57869
2013-05-25 01:20:17
--------------------------
72649
9901
Đây là một tác phẩm được xây dưng cốt truyện hoàn toàn khác so với những tác phẩm trước đây của Suzanne Brockmann. Một tình yêu được dựa trên sự thấu hiểu, đồng cảm giữa hai nhân vật nam và nữ chính, chứ không hề còn kiểu tình yêu nam nữ giữa chàng và nàng thanh mai trúc mã yêu nhau từ lâu. Đây là quyển mình hứng thú đọc nhất của bà, vì cốt truyện lạ, hấp dẫn và cuốn hút. Tình yêu giữa Frisco và Mia có vẻ khá khó khăn, khi mà anh, đã từng là một người lính kiên cường, dũng mãnh và đầy gan dạ, trở thành kẻ tàn tật và không còn can đảm để nhận tình yêu của Mia, anh đã làm cô tổn thương. Anh đã nghĩ chỉ có cách đó mới khiến Mia đi xa anh, không phá hủy cuộc đời cô với kẻ tàn tật như anh nữa. Nhưng anh đã nhầm, anh rất yêu cô....
Suzanne một lần nữa khẳng định tên tuổi của mình trong làng văn học lãng mạn. Dưới ngòi bút của bà, những câu chuyện tình yêu được thêu dệt một cách sống động và ngọt ngào, dù có đôi khi tình tiết truyện cũng khá giống nhau, nhưng lại không hề nhàm chán tí nào. Một tác phẩm đáng đọc nữa của bà dành cho những ai luôn tin vào tình yêu đẹp, lãng mạn và chân thành.
Nhận xét ngoài lề: Bìa sách lần này có hình ảnh rất dễ thương: một cô bé ngồi chơi trên bãi biển, rất hợp với tiêu đề "Thien thần của anh". Lời dịch cũng mượt mà, sống động tuy có một số chỗ lỗi đánh máy. Mình chấm 5 sao nhé :)
5
23656
2013-05-03 23:25:58
--------------------------
60692
9901
Một câu chuyện, một trải nghiệm tình yêu đầy cảm động, sâu lắng và lắng đọng trong lòng người đọc. Bằng tất cả những am hiểu, sâu lắng và tự mình lắng nghe trái tim và tâm hồn mình, tâm hôn người mà tác giả S.B đã viết lên 1 câu chuyện vô cùng tuyệt vời này. Sức mạnh tình yêu được nâng lên 1 cảm súc mới nhiều thi vị và những ý nghĩa đầy sâu sắc, lắng đọng.
A.F, nam chính, anh đã trải qua 1 cuộc đời đầy cay đắng, đầy những vết thương tâm hồn, một tuổi thơ đầy dữ dội, trưởng thành trong một môi trường đầy những vất vả, gian nan đã khiến anh bị thương tổn, phải từ bỏ cuộc đời của 1 quân nhân, anh đã buông xuôi tất cả cho đến tận lúc anh gặp được Mia- người phụ nữ đã vad vẫn sẽ làm thay đổi cuộc đời anh, kéo anh đứng dậy, bỏ lại sau lưng 1 vũng bùn lầy tuyệt vọng, đớn đau, thương tổn.
Thiên thần của anh, cô ấy đúng là  1 thiên thần mang tình yêu đến cho anh, khi chính bản thân anh không biết sẽ chìm sâu bao nhiêu nữa xuống vũng bùn lầy tuyệt vọng ấy. Và anh yêu cô ấy rất nhiều <3
4
9481
2013-02-24 22:14:03
--------------------------
57390
9901
Trong những sách đã đọc của Suzanne Bockmann thì đây là cuôn tôi thích nhất, qua Alan tác giả đã chuyển được nỗi đau đớn mà 1 người lính phải chịu đựng cả về thể xác lẫn tâm hồn, Alan rất kiên cường khi phải đối mặt với những tàn tật của mình nhưng nỗi đau khi anh mãi mãi sẽ không còn là 1 lính seal khiến anh tan nát. Đối với Mia, cô là 1 cô gái xinh đẹp và giản dị, cô bước vào cuộc đời của Alan và từng bước thay đổi con người anh, cô mang đến cho anh tình yêu, sự đồng cảm, quan trọng hơn hết cô giúp anh nhận ra giá trị cuộc sống này, anh còn cả tương lai phía trước, anh vẫn sẽ là 1 lính Seal nhưng với vai trò khác cao cả và tuyệt vời hơn thế. 
Tình yêu khỏa lấp mọi nỗi buồn, tình yêu cứu con người ra khỏi bờ vực của sự tuyệt vọng, tình yêu mang con người đến 1 ánh sáng mới. Đó là tất cả những gì đã thể hiện trong "Thiên Thần Của Anh". 1 câu chuyện đầy cảm động. Tôi yêu Alan, yêu Mia và yêu cả Natasha cô cháu gái đáng yêu, cả 3 như 1 gia đình gắn bó trong đó Mia chính là thiên thần đã mang đến tình yêu, hạnh phúc mới cho cả Alan & Natasha.
5
41370
2013-01-29 18:04:46
--------------------------
54016
9901
đang say sưa, đắm chìm trong dòng tiểu thuyết ngôn tình của Trung Quốc, thật khó để bước chân vào thế giới của các tác phẩm Âu Mỹ. Vô tình tìm đọc được cuốn sách này, tôi đã thay đổi quan điểm. Câu chuyện mở ra thật giản đơn với thông tin một anh lính SEAL bị thương do hậu quả của chiến tranh và ngày ngày phải đối mặt với đôi nặng như khắc sâu thêm vết  thương của anh. Và rồi thiên thần của anh đã đến, giải cứu ấy khỏi nỗi đau khổ, tuyệt vọng và chỉ cho anh thấy vẻ đẹp của cuộc sống,
Tưởng chừng mô tuýp là quen thuộc nhưng cách dẫn dắt, khai triển tác giả lại mang phần cuốn hút, đặc biệt là miêu tả tâm lí nhân vật. Đó là sự băn khoăn, trăn trở, mâu thuẩn trong nội tâm chàng lính, là sự thấu hiểu, ân cần và hy vọng của cô gái...kể cả những lúc thân thiết, gần gũi nhất với nhau, giữa họ vẫn tồn tại những dòng suy nghĩ về hiện tại, về tương lai mà khó lòng bất chấp tất cả để bên nhau. Nhưng cuối cùng kết thúc có hậu của truyện đã khép lại để tất cả chúng ta cùng mỉm cười..
Có thể bạn thích một kết thúc mở đầy hứa hẹn, một kết thúc buồn đầy day dứt, vấn vương.. Nhưng tôi lại thích một kết thúc có hậu và ấm áp, cũng coi như thỏa tấm lòng và quá trình theo đuổi một câu chuyện dài. Hết buổi trà chiều và tôi không thấy vị đắng chát, nghèn nghẹn, tách trà ngọt dịu vẫn còn đong đầy hương thơm....
3
68208
2013-01-06 18:32:07
--------------------------
51367
9901
Đang trong giai đoạn thi học kì một nhưng vẫn còn đủ thời gian rãnh mà đọc hết cuốn tiểu thuyết này.
Nhìn chung thì Frisco's kid (Thiên thần của anh) của Suzanne Brockmann là một tác phẩm hay, cốt truyện không mới nhưng lối viết thì rất khác so với nhiều tác giả tôi đã đọc.
Câu truyện kể về người lính SEAL - Alan Francisco đã bị chấn thương sau một chiến dịch giải cứu. Năm năm tích cực chữa trị là năm năm luyện tập gian khổ để anh có thể thoát ra khỏi chấn thương nơi đầu gối để quay về bên những người lính khác và trở thành một anh cháng SEAL vui tính như cũ. Nhưng vết thương quá nghiêm trọng, bằng hết nỗ lực của bản thân Frisco chỉ có thể đứng và đi được bằng cây ba-toong. Natasha Francisco và Mia Summerton bước vào đời anh, thay đổi mọi thứ từ kéo anh dưới vực thẩm lên đến tận bến bờ của hạnh phúc mãi mãi về sau.
Tác giả diễn tả tâm trạng đau khổ ấy bằng thứ ngôn từ giản dị, không hoa mĩ, không tha thiết. Ấy vậy mà hầu như mọi nỗi đau đều đến được với độc giả. Tuy nhiên, theo bản thân tôi vẫn thấy thiếu một thứ gì đó. Hình như, do tác phẩm quá ngắn (chỉ khoảng ba trăm mấy trang) nên nhiều khuất mắc, nhiều suy nghĩ, nhiều hoàn cảnh chưa ngấm được vào người đọc thì đã chuyển sáng một cảnh mới, một chi tiết khác.
Về nhân vật, lúc đầu tôi thấy thích Frisco và Mia nhất (vì là nhân vật chính mà) nhưng về sau lại thấy thấm thía được cái tên Frisco's kid bởi lẽ mọi vấn đề của câu truyện, mọi thắc nút đều được Tasha kết nối. Đó là một cô bé hồn nhiên, dễ thương và đáng yêu, đặc biết là rất tinh ranh nữa chứ. Cái hay của Suzanne Brockmann ở đây là biến được những dóng chữ bình dị trên trang giấy thành một cô bé sắc sảo và rất thực.
Cuối cùng, câu truyện nói lên rất rõ rằng hãy vượt qua những khó khăn phía trước. Khi vượt qua rồi, có thể tương lại sẽ không như cũ nhưng nó vẫn có thể tươi sáng và sẽ luôn có người ở đó để giúp bạn đi tiếp con đường phía trước dù người đó là ai. Đừng từ bỏ, bởi vì một khi buông tay, quá khứ sẽ tiến đến hay có lẽ là một tương lai có màu như hiện tại bạn đang phải gánh chịu và gánh chịu mãi.
Cảm ơn những ai đã khiến tiểu thuyết này đến với tôi. 
4
19049
2012-12-20 08:20:08
--------------------------
50007
9901
Sau khi đọc xong tác phẩm, tôi nghĩ rằng cuốn tiểu thuyết này không đơn thuần chỉ là 1 cuốn tiểu thuyết lãng mạn. Nó chứa đựng tình yêu, tình cảm gia đình, những gì thiêng liêng và quý giá. Alan đã sống trong mặc cảm trước khi Mia bước vào cuộc đời anh. Cô như một thiên thần đã giúp vượt qua sự khó khăn tàn tật, tin tưởng vào chính bản thân mình, quên đi bóng tối tuyệt vọng đã từng vây hãm anh. Không chỉ vậy, cô còn giúp anh làm quen với một vai trò mới, người cậu của của 1 cô cháu gái bé nhỏ. Ban đầu là sự vụng về, nhưng dần dần anh đã học cách đón nhận cô cháu gái vào cuộc sống của mình, cô bé có mẹ là 1 phụ nữ nghiện rượu cũng đã dần dần học cách sống như bao đứa trẻ bình thường khác. Alan ko chỉ nhận được những món quà tuyệt vời ấy mà anh còn nhận được tình cảm của chính thiên thần đã cứu vớt cuộc đời mình.
5
38477
2012-12-11 16:54:23
--------------------------
49787
9901
Cuốn sách này của nhà văn Suzanne Brockmann thật nhẹ nhàng, chân tình, giản dị, nhưng cũng có sức mạnh có thể làm xao xuyến mọi trái tim. Cách dẫn truyện cũng như lời văn của tác giả phải nói rằng rất hấp dẫn và sắc sảo, lôi cuốn độc giả vào một miền cảm xúc tinh khôi nhất, trong sáng nhất, dung dị nhất của tình yêu. Đúng như những gì cuốn sách này kể, tình yêu có thể làm sống dậy cả những linh hồn cằn cỗi nhất, có thể vực mọi con người, làm họ có thể vượt lên những giới hạn để nắm bắt lấy cuộc sống của mình. Hai nhân vật chính trong truyện, những con người bình thường, nhưng cũng đẹp đến lạ lùng, họ chính là những thiên thần đã đến trong đời nhau, đến vì tình yêu và chỉ vậy mà thôi!
5
20073
2012-12-09 21:03:26
--------------------------
48946
9901
Tôi luôn dành một sự quan tâm đặc biệt đến những cuốn tiểu thuyết tình cảm lãng mạn. Thiên Thần Của Anh- một tựa sách nhẹ nhàng nhưng cũng đầy bay bổng khiến tôi ấn tượng từ lần đầu bắt gặp. Tôi không tin rằng một cô gái xinh đẹp như vậy lại sẵn sàng ở bên một chàng lính tàn tật. Nhưng khi đọc câu chuyện này thì tôi phải tin. Tin vào tình yêu kì diệu giữa người và người.
Alan Francisco- người lính may mắn ấy đã gặp được một nửa của mình: Đó là cô gái mà trong mắt anh tựa như thiên thần. Nếu là bạn, bạn có tin rằng một ai đó thật sự là thiên thần không khi mà những lúc gặp khó khăn người đó luôn ở bên bạn ? Cuốn sách này đã gửi gắm thông điệp: Thiên thần không tồn tại trong thực tế nhưng trong tim mỗi người hẳn sẽ lưu giữ hình ảnh một thiên thần.
4
76230
2012-12-04 10:06:34
--------------------------
39389
9901
Cuốn Thiên Thần Của Anh của Suzanne Bockmann là một trải nghiệm tình yêu đầy sâu lắng và cảm động. Tác giả đã viết nên câu chuyện bằng sự am hiểu sâu sắng về trái tim và tâm hồn. Sức mạnh tình yêu trong Thiên Thần Của Anh đã được nâng lên một cảm xúc mới nhiều thi vị và ý nghĩa.
Nam chính Alan Francisco đã trải qua một cuộc đời quá nhiều đắng cay, tuổi thơ qua đi để lại vết thương sâu trong tâm hồn. Quá trình trưởng thành gian nan và vất vả để tạo dựng một người lính mãnh mẽ. Nhưng số phận một lần nữa đẩy anh vào bước đường cùng, trong một trận chiến anh đã bị thương, đành phải từ bỏ cuộc đời quân nhân. Alan đã buông xuôi tất cả cho đến khi anh gặp được Mia. Cô gái đã làm thay đổi cuộc đời anh, là một thiên thần mang tình yêu đến để kéo anh ra khỏi vũng lầy của sự tuyệt vọng...
4
6502
2012-09-11 16:40:28
--------------------------
