501013
8931
Cuốn sách "Lẽ phải của phi lí trí" phân tích, giải đáp thắc mắc về 10 vấn đề thuộc mọi lĩnh vực trong cuộc sống hàng ngày. Những vấn đề tưởng như mặc địch, nhưng được phân tích, mổ xẻ dựa vào những thí nghiệm của chính tác giả và cộng sự. Cuốn sách này sẽ giải đáp cho chúng ta những thắc mắc như tại sao chúng ta thường thiên vị những ý tưởng, đồ vật do chúng ta tự tạo ra hoặc là con của minh? tại sao chúng ta thường dễ đồng cảm với một cá nhân bất hạnh hơn là một cộng đồng đớn đau
Cuốn sách có vẻ khó nuốt với những người không đam mê về tâm lí học xã hội; tuy nhiên nếu kiên nhẫn và hứng thú, bạn đọc sẽ khám phá ra được nhiều điều thú vị
4
772952
2016-12-27 17:48:38
--------------------------
488036
8931
 Sách rất hay đặc biệt dành cho những người đam me kinh doanh thì là quá tuyệt!
Đây là cuốn sách kĩ năng đầu tiên mik thấy lối trình bày rất sáng tạo, từ hình vẽ và lập luận giải thích đều rất OK luôn! 
 Vậy nên cũng đangs để mua lắm ấy, rất đáng đồng tiền( đã thế tiki còn ưu đãi giảm giá nữa).
Nhưng lần này thì mik vẫn trừ điểm tiki đó là bọc plastic của mik ko chắc lắm, đọc được chút lật qua lật lại đã bong rồi( cái chỗ mép sách í) , mà mấy cuốn trong đơn hàng mik cx vậy nữa, hi vog lần sau sẽ OK hơn...
5
607435
2016-10-30 01:34:32
--------------------------
444944
8931
Đây là một cuốn sách hay.Nội dung xoay quanh những thực nghiệm của tác giả Dan Ariely nhằm giải thích một vài hành vi của con người chúng ta.
Những thực của ông đơn giản cùng với những kết luận thực tế khiến cuốn sách rất dễ tiếp thu chứ không hề phức tạp.
Lời văn gọn gàng,từ ngữ cũng đơn giản,cách diễn giải của tác giả cũng ngắn gọn nhưng đầy đủ ý nghĩa làm cho người đọc không bị chán và ngộp bởi kiến thức khoa học.
Đây là một cuốn sách hay,đáng đọc dành cho tất cả mọi người,không chỉ dành riêng cho những ai làm việc trong lĩnh vực kinh tế,nếu muốn khám phá ra cách thức vận hành của não bộ sau những hành vi đời thường.
4
1067554
2016-06-09 11:10:53
--------------------------
397092
8931
Mình thực tế chưa đọc cuốn sách Phi Lý Trí của tác giả, nhưng cuốn sách này nằm trong chuỗi những cuốn sách mình đặt mua để tìm hiểu về trí truệ trong cảm xúc và tư duy của con người.
Nói đúng hơn, cuốn sách đã đạt được điều mà tôi mong muốn là giải thích một số hiện tượng trong cuộc sống ở hành vi con người và những tác động làm thay đổi suy nghĩ/hành vi. Cuốn sách thiên về nghiên cứu hơn là đưa ra những gợi ý, nhưng chúng ta hoàn toàn có thể dựa vào những phát hiện từ các nghiên cứu của tác giả để có thể lên những kế hoạch cho chính mình để đạt được hiệu quả cao nhất.
5
358928
2016-03-14 14:22:12
--------------------------
379703
8931
Ai mà đã đọc cuốn Phi Lý Trí thì chắc chắn sẽ muốn mua cuốn này vì cuốn trước quá hay và xuất sắc và mình cũng vậy, mình thấy cuốn này cũng khá ổn không hay hơn cuốn trước, giống như hai phần của một cuốn sách vậy.
Cuốn sách này dành cho những bạn muốn tìm hiểu về tâm lý học hành vi của con người và một chút về kinh tế học, rất bổ ích cho những người bán hàng muốn tìm hiểu về những động cơ mua hàng của khách hàng.
Phần hình thức thì khá là tốt bìa gập in đẹp, giấy trắng mịn.
4
306081
2016-02-12 11:18:28
--------------------------
365599
8931
Mình có đọc qua cuốn "Phi Lý Trí" của Dan Ariely và rất thích nên đã đặt mua cuốn "Lẽ Phải Của Phi Lý Trí" trên tiki, mình vừa nhận được sách sáng nay, về phần nội dung, mình cho rằng đây là một cuốn sách hay và bổ ích, tuy nhiên, mình cảm thấy rất thất vọng với chất lượng sách, trang giấy mỏng, chữ trang sau nổi rõ ở trang trước gây cảm giác rất khó chịu khi đọc, nếu vì giá rẻ mà không đảm bảo được chất lượng thì mình thấy không nên giảm giá làm gì.
3
900648
2016-01-08 12:33:21
--------------------------
360716
8931
Kinh tế học hành vi thì có daniel kahneman trong tư duy nhanh và chậm có lẽ là cuốn sách đồ sộ nhất trong lĩnh vực này. Vì đã đọc cuốn đó nên khi đọc lẽ phải của phi lý trí tuy cũng thú vị nhưng nằm ở mức vừa phải, những quy mô quan sát cũng nằm ở phạm vi nhỏ hơn. Tuy nhiên ở bộ óc quan sát tuyệt vời cùng những trải nghiệm riêng tác giả đưa đến cho người đọc những điều đánh giá. Theo mình thấy cuốn sách này giá cũng khá cao so với kiến thức của nó mang đến, giá bằng nửa cuốn tư duy nhanh và chậm nhưng giá trị của nó khoảng bằng 2 chương nghiên cứu trong tư duy nhanh và chậm.
3
74132
2015-12-29 18:00:12
--------------------------
341764
8931
Một trong những tác phẩm trong seri của tác giả Dan Ariely nói về phương pháp lập trình ngôn ngữ tư duy và kinh tế học hành vi thông qua những thí nghiệm trực tiếp của ông và các sinh viên để đưa ra kết luận về ảnh hưởng của những mặt trái mà chúng ta vẫn ngộ nhận từ trước đến nay.
Bạn sẽ nhận ra những lầm tưởng trước giờ của mình đúng như tên gọi quyển sách "Phi lý trí" và gần như ảnh hưởng trái chiều trong nhiều trường hợp, giống như việc tăng lương cao có thực sự tăng hiệu suất việc làm hay không và ảnh hưởng nó như thế nào. Quyển sách này sẽ cho bạn bài học về rất nhiều thứ và biết như thế nào là "đủ" để chúng ta có những phương pháp tốt nhất và hoàn hảo hơn.
4
451219
2015-11-22 22:44:24
--------------------------
332633
8931
Tôi đã đọc phi lý lí của  Dan Ariely, và ngộ ra một sự thật phũ phàng. Tôi thấy nó rất hay, vì vậy sự ra đời của Lẽ Phải Của Phi Lý Trí lại thúc đẩy tôi đọc hết cuốn sách này. Không nên tuân theo một cách mù quáng quá mức lý trí, hãy nhìn lại con người mình. Đọc cuốn sách này bạn sẽ biết được lợi ích bất ngờ của việc đó, hãy đọc và ngẫm nghĩ, biết đâu nó sẽ giúp bạn trong công viêc. Và cuốn sách này đặt biệt hữu ích cho những ai có công việc về kinh doanh. Chúc các bạn may mắn
5
828378
2015-11-06 18:51:05
--------------------------
311012
8931
Đã đọc qua  và rất thích cuốn Phi lý trí nên mình quyết định mua cuốn Lẽ phải của phi lý trí này của Dan Ariely. Với cuốn sách này thì tác giả lật ngược lại vấn đề so với cuốn trước, đó là đưa ra lợi ích của phi lý trí. Ông vẫn dùng rất nhiều thí nghiệm để kiểm nghiệm các giả thuyết như đối với cuốn trước. Mình thấy thú vị nhất là phần ông lý giải về việc tại sao con người lại coi lao động là vinh quang và việc các mức độ của áp lực có lợi và có hại cho kết quả lao động như thế nào. Nói chung đây là một cuốn sách hay nên đọc nếu bạn muốn hiểu hơn về tâm lý học hành vi.
5
105510
2015-09-19 21:01:44
--------------------------
307096
8931
“Lẽ phải của phi lý trí” là cuốn sách nối tiếp “Phi lý trí” trong series 3 tập sách về kinh tế học hành vi của tác giả Dan Ariely. Cũng tiếp tục nói về vấn đề phi lý trí trong các quyết định của con người như phần trước nhưng tập sách này lại đi sâu vào phân tích, trình bày các kết quả thực nghiệm từ những thí nghiệm về hành vi của con người. Đây sẽ là một cuốn sách vô cùng hữu ích cho những ai có công việc trong các lĩnh vực tiếp xúc nhiều đến con người như bán hàng, marketing, … khi muốn tìm hiểu thêm về tâm lý và hành vi khách hàng. 
5
199482
2015-09-17 22:45:27
--------------------------
306717
8931
Mình quyết định mua sách này là từ ấn tượng cuốn Phi Lý Trí của cùng tác giả Dan Ariely. Sách đưa ra nhiều nghiên cứu khá thú vị, chi tiết về những vấn đề trong cuộc sống nhưng lại không kém phần thuyết phục và sâu sắc. Nhiều điều đúc kết được giúp chúng ta ra những quyết định sáng suốt hơn trong cuộc sống hằng ngày. Vì vậy, mình nghĩ sách phù hợp với nhiều người, không chỉ riêng các bạn sinh viên kinh tế hay bất kì nhóm độc giả nào cả. Bản dịch cũng khá dễ hiểu và gần gũi với người đọc.
4
315772
2015-09-17 19:10:09
--------------------------
282583
8931
Lẽ Phải Của Phi Lý Trí có quan điểm khác cuốn sách đồng tác giả Phi lý trí , nó suy diễn và diễn giải nguyên nhân dẫn đến những hành động kì quặc của con người và giúp chúng ta tháo nút nó và về sau có thể nhìn nhận nó với một con mắt của người hài hước . Một cuốn sách đáng đọc cho những người hướng nội . Cám ơn tiki đã cho mình cơ hội được đọc cuốn sách này , mình sẽ giới thiệu nó cho bạn bè minh để họ cũng có cơ hội tiếp thu kiến thức như mình.
5
729882
2015-08-29 11:31:26
--------------------------
250124
8931
Nếu bạn đã từng là độc giả yêu thích quyển sách tiền đề ' Phi lý trí ' thì bạn sẽ dễ dàng nhận ra cuốn sách này dựa trên những kết luận rút ra từ những thực nghiệm tinh vi, khoa học được trình bày trong quyển sách trước, phân tích chi tiết hơn trong từng lĩnh vực cụ thể của đời sống vi mô và vĩ mô một cách đáng ngạc nhiên và thuyết phục. Không chỉ mang lại kiến thức phổ quát trong một lĩnh vực khá thú vị mà những kết luận đó còn như những kinh nghiệm thu được giúp ta ra quyết định tốt hơn trong đời sống thường ngày.
4
171151
2015-08-01 10:25:14
--------------------------
217273
8931
Trong cuốn sách phi lý trí, Dan Ariely đã giải thích vì sao con nguời lại phi lý trí trong những truờng hợp đáng ra con người phải lý trí. Ở trong cuốn sách này, dựa vào những nghiên cứu của mình, Dan lại chứng minh được rằng, trong những trường hợp phi lý trí đấy, con người vẫn có những lý do và lẽ phải của riêng mình, chính những điều đó đã khiến họ hành động ngược hoàn toàn với quỹ đạo đúng đắn của con người.

Mình nghĩ rằng cuốn sách này không chỉ hữu ích đối với những nhà kinh tế, những người làm trong ngành kinh tế, mà còn có ích đối với tất cả mọi người, vì cuốn sách này sẽ giúp các bạn hiểu rõ hơn về hành vi của mọi người, giúp các bạn có một cách nhìn và cách hiểu mọi người thấu đáo hơn.
4
140922
2015-06-29 08:15:46
--------------------------
215900
8931
Sau thành công của quyển Phi Lý Trí, tác giả Dan Ariely đã tiếp tục dẫn dắt người đọc đến với nhiều khía cạnh khác của cuộc sống cũng như hành vi của mỗi cá nhân trong các mối quan hệ xã hội, từ đó phân tích từ đâu mà mỗi chúng ta đã có những hành vi đó.
Vẫn với phong cách đưa ra những ví dụ thực tế rất hấp dẫn người đọc để làm rõ các hành vi của cá nhân, đồng thời kết luận vì sao có hành động đó.
Sách thật sự hữu ích cho những ai muốn tìm hiểu về hành vi con người, đặc biệt là các bạn đang học tập và làm việc trong môi trường tiếp xúc nhiều như kinh tế, marketing,..
5
315066
2015-06-26 22:52:34
--------------------------
