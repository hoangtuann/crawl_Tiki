480621
6425
Truyện bắt đầu từ khi Tần Chiêu Chiêu còn nhỏ. Trung Quốc là nước phát triển công nghiệp sớm hơn Việt Nam nên có lẽ có nhiều điều trong tuổi thơ của nữ chính không giống với chúng ta, nhưng từng câu từng chữ miêu tả tâm trạng cảm xúc của nữ chính khi bắt đầu gặp rồi yêu nam chính như thể chính tuổi thơ của tôi vậy, thích một người từ rất lâu rất lâu trước, và phải cả chục năm sau, người ta mới biết được tình cảm của mình. Nhưng biết được rồi thì sao, yêu đơn phương thì mãi mãi chỉ là yêu đơn phương. Tần Chiêu Chiêu là cô gái tốt, yêu đến nhiệt thành, yêu hết bản thân mình, vì người mình yêu mà bỏ quên cả chính bản thân mình. Truyện không như nhiều motip khác, nữ chính yêu thầm nam phụ, rồi cho đến khi nam chính xuất hiện, nam phụ mới nhận ra và đáp lại tình cảm của nữ chính thì đã quá muộn. Truyện như kể về mối tình đơn phương của Chiêu Chiêu và Kiều Mục, đông thời trong quá trình đó lại chậm chạp nảy sinh tình cảm với Mộc Mộc. Tuy còn nhiều tiếc nuối với mối tình của nữ chính, nhưng lại khiến người đọc vô cùng thỏa mãn với cái kết, một câu chuyện đáng để đọc
5
1614627
2016-08-19 11:03:09
--------------------------
393132
6425
Không cao trào cũng không quá tầm thường, Tuyết Ảnh Sương Hồn đã mang đến cho người đọc một Những tháng năm hổ phách (tập 1) bình lặng an yên, kể về cuộc đời của Tần Chiêu Chiêu, một cô bé con của gia đình công nhân nghèo. Bắt đầu từ lúc còn chạy nhảy ngoài đường chơi với con hàng xóm cho đến lúc TCC đỗ vào một trường đại học có tiếng ở Thượng Hải, nhưng lại không hề nhàm chán, trái lại TASH đã thổi vào đó một làn gió thanh xuân tươi trẻ tràn đầy. Tuổi thơ của TCC gắn với những cột mốc thay đổi của xã hội Trung Quốc, cho ta hiểu thêm về những hậu quả của chính sách cải cách Trung Hoa và những con người trong giai đoạn đó. Những câu chuyện xảy ra với TCC trong thời học sinh dường như mang ta trở về với những mảng ký ức con con vụn vặt về tuổi thơ của mình, mọi thứ đều rất thân quen. Có lẽ nhiều bạn đọc sẽ thấy đâu đó hình ảnh của chính mình được cất giấu trong TCC, một cô bé bình thường về gia cảnh lẫn nhan sắc nhưng lại mang một phần nội tâm muốn vươn lên mãnh liệt, cô học vì gia đình và cả vì người mình thương, đôi khi lại hơi ích kỷ vì chính tình cảm của mình.
TASH có một lối viết khá thú vị đó là miêu tả luân phiên nội tâm của cả hai nhân vật nam & nữ đang thầm thích nhau, khi đọc nội tâm của nhân vật nam (Lâm Sâm) lại có cảm giác ấm áp hơn. TCC hay nghĩ ước gì cô được sinh ra trong một gia đình khá giả hơn, nhưng mình lại ước được như TCC, cô học rất giỏi, dù trước kỳ thi đại học có lo lắng tới đâu thì vẫn đỗ với số điểm cao như thường, còn có một người thầm thích cô thật lâu.
Đọc xong tập 1 rồi, nhất định sẽ mua thêm tập 2, vì 1 tập hơi mắc nên chia ra mua 2 lần ^^
5
732155
2016-03-08 11:09:05
--------------------------
384751
6425
Những năm tháng hổ phách khi đọc tên thì nghĩ có lẽ là một câu truyện sẽ hấp dẫn và kịch tính thế nhưng bìa sách lại dịu dàng và êm ái. Nội dung lại như bìa sách. Một cấu truyện ghi lại sự lớn lên, bao gồm tình yêu cuộc sống gia đình. Và nổi bậc về tình yêu của hai người trong sáng đến tất cả các bạn phải khát khao và ganh tỵ. Có lẽ ai trong đời cũng nên yêu một lần khi ta còn dại khờ dù nó có dang dỡ thì cũng sẽ là một hồi ức đẹp để ta nhớ lại sau này.
4
1061830
2016-02-22 22:19:42
--------------------------
370012
6425
Lúc đầu mình không thích thể loại truyện kiểu tự sự từ bé đến lớn của một nhân vật lắm , nhưng mình đã thích cuốn sách này ngay từ những trang đầu tiên . Tác giả đã dẫn dắt người đọc cùng trải qua quá trình trưởng thành của Trần Chiêu Chiêu - một cô gái bình thường có bố mẹ là công nhân sống trong xã hội đang phát triển . Tác giả kể từng chi tiết rất thực làm mình cũng bồi hồi nhớ lại thời thơ ấu : cũng thích những viên kẹo đắt tiền mà đẹp ( như kẹo thỏ trắng ) , cũng cứng đầu và lại nhút nhát , cũng thầm mến một cậu nam sinh vì cậu mà đã cố gắng không ngừng ... Đây thực sự là một tác phẩm của Tuyết Ảnh Sương Hồn mà bạn không nên bỏ qua .
5
743273
2016-01-16 22:30:00
--------------------------
368812
6425
Tần chiêu chiêu có nhiều cá tính đặc biệt là sự yêu thương và dễ hòa hợp với mọi người , cô bé có thể làm nên độc đáo từ chính xã hội mà cô sinh sống , có thể còn rất nhiều điều tất yếu xảy ra cho cô nhưng cô không sống trong gượng ép mà chung với toàn cảnh của sự xô bồ , thay đổi cách nghĩ rất phong kiến và cổ hủ nhưng tinh thần cứng như thép làm ta thấy phảng phất đâu đấy chính là sức trẻ và trí tuệ thông minh từ nhỏ đến lớn .
4
402468
2016-01-14 17:55:40
--------------------------
349628
6425
Câu truyện bắt đầu từ cảm nhận của một đứa bé con nhà nghèo đối với một cậu bé nhà giàu. Có lẽ vì ở một khoản cách chênh lệch đó mà tình yêu của cô bé thật ngây ngô và trong sáng nhưng cũng có phần tự hạ thấp bản thân. Nó kéo dài từ thời hoa niên đến lúc trưởng thành - một mối tình đơn phương. Đọc truyện cảm thấy rất tiếc cho Chiêu Chiêu khi mà không thể có một kết thúc viên mãn với người mình thích bao nhiêu năm. Nhưng cũng may là nhân vật nữ chính đã không bỏ lỡ một mối tình khác cũng kéo dài bao nhiêu năm như thế. Cố gắng kiên trì một tình yêu không phải lúc nào cũng kết thúc hoàn mỹ. Có lẽ còn cần có duyên phận. Một câu truyện với tiết tấu nhẹ nhàng.
3
365997
2015-12-08 20:14:28
--------------------------
181213
6425
Mình rất thích cái cách mà Tuyết Ảnh Sương Hồn miêu tả khoảng thời gian thanh xuân vô ưu vô lo trong những tác phẩm của mình. Tác phẩm này cũng vậy, nếu bạn nào đang ở tuổi này hoặc đã qua rồi thì cũng nên đọc, tặng cho tuổi thanh xuân của mình chút hoài niệm. Tình yêu của hai nhân vật chính rất trong sáng, đúng như những gì tuổi thanh xuân nên có, cái cách họ dần yêu nhau rồi đến với nhau làm mình có cảm giác đang thật sự chứng kiến câu chuyện tình của họ vậy, cái này chị Ảnh rất thành công nhé. Khoảng thời gian đọc cái này mình hoàn toàn rất thư giãn luôn, như được trở lại, những năm tháng cắp sách một lần nữa vậy. Triết lý nhân sinh trong truyện của chị cũng rất hay, ai thích càng không nên bỏ qua.
4
82786
2015-04-11 12:07:40
--------------------------
144005
6425
Mở đầu bằng những câu văn rất nhẹ nhàng tác giả dẫn dắt chúng ta vào mối tình trong sáng êm đềm của hai nhân vật chính . Lúc đó tình yêu mà họ giành cho nhau không pha lẫn chút tạp chất nào cả không có địa vị,danh dự , tiền bạc họ yêu nhau chỉ vì trái tim mình mách bảo . Điều đáng buốn cho tình yêu này là họ không thể thấu hiểu được nỗi lòng của đối phương có lẽ là bởi vì còn trẻ nên mới dẫn đến đổ vỡ . Khi đã trưởng thành tất nhiêu suy nghĩ cũng khác đi , thỉnh thoảng khi nhớ lại mối tình năm xưa thì bất giác mỉm cười . Kí ức đó thật là ấm áp làm sao . Đọc truyện mình như hòa cả vào truyện tác giả dùng rất nhiều biện pháp nghệ thuật làm truyện rất nổi bật và sinh động . Về mặt nội dung tuy không mới lạ nhưng cách hai nhân vật chính trong truyện nối lại tình yêu khiến ta phải suy nghĩ
4
337423
2014-12-25 09:13:09
--------------------------
135005
6425
Câu chuyện kể về Tần Chiêu Chiêu từ nhỏ cho tới lúc thi đỗ đại học. Chiêu Chiêu là 1 cô gái con nhà công nhân tại nhà máy Trường Cơ. Tuổi thơ cô gắn liền với chế độ bao cấp của Nhà Nước. Xuyên suốt câu chuyện là quá trình cô lớn lên, những thay đổi trong suy nghĩ, tính cách cùng biến đổi của Đất Nước. Quá trình cô thầm mến và theo chân cậu bạn Kiều Mục nho nhã, tài hoa khác biệt những đứa trẻ như cô bên nhà hàng xóm và cậu bạn Lâm Sâm cùng lớp cấp 3 nghịch như quỷ thích cô ra sao. Bạn sẽ thấy được những loại kẹo, những bộ phim, những lối sống, sinh hoạt của con người trong thời điểm đó. Đây là 1 câu chuyện nhẹ nhàng, tự sự theo dòng thời gian từ bé tới lớn. Điểm trừ của tác phẩm này chính là tác giả miêu tả quá nhiều về yếu tố bên ngoài như các loại kẹo, loại thiếp, loại quần áo, nhạc đĩa... đã ra đời như thế nào khiến cho người đọc có cảm giác hơi nhàm chán còn các tình tiết trong truyện không có gì quá đặc sắc, nội tâm nhân vật cũng không nổi bật, thậm chí cô bé Tần Chiêu Chiêu giữa hồi nhỏ và lớn lên quá khác biệt, không có gì đặc biệt, cô trầm lặng, tự ti, thiếu cá tính, đôi khi hơi ích kỷ. Thế nên đây là 1 câu chuyện nhẹ nhàng, thích hợp cho những ai muốn tìm kiếm lại thời thơ ấu, cảm giác yêu thầm.
4
262942
2014-11-12 16:05:03
--------------------------
