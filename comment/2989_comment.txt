309362
2989
Sách tuy nhỏ nhưng xinh xắn, dễ thương. Sách được thiết kế bìa cứng nên trẻ em đọc sẽ không dễ bị nhàu sách. Mỗi trang là một lời khuyên để các bé hình thành thói quen tốt, và kèm theo đó là hình ảnh minh hoạ. Sách tuy đơn giản, câu văn ngắn gọn, hình ảnh minh hoạ đẹp, sống động nhưng rất có ích để các bậc phụ huynh rèn luyện thơi quen tốt cho trẻ. Tuy nhiên, số trang hơi ít, vừa đọc qua là đã hết. Sách thiết kế đặc biệt, nhỏ gọn nên không áp dụng bọc sách được.
4
428098
2015-09-18 23:23:55
--------------------------
