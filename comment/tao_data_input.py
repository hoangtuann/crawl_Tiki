
import datetime
import os
import re
#!/usr/bin/env python
# encoding=utf8  
import sys
reload(sys)  
sys.setdefaultencoding('utf8')
import os
import time
from itertools import islice
import numpy as np
import pandas as pd
from BeautifulSoup import BeautifulSoup
def IsNumber(text):
	try:
		int(text)
		return True
	except ValueError:
		return False
def time_diff_str(t1, t2):
	"""
	Calculates time durations.
	"""
	diff = t2 - t1
	mins = int(diff / 60)
	secs = round(diff % 60, 2)
	return str(mins) + " mins and " + str(secs) + " seconds"
 
def clean_sentence(sentence):
	review_text = BeautifulSoup(sentence).text
	review_text=review_text.strip()
	return review_text
 
def convert_plain_to_csv(plain_name):
	t0 = time.time()
	if os.stat(plain_name).st_size == 0:
		print 'Emty file:'+plain_name
		return
	#s = tsv_name
	#x = s.split("_")
	with open(plain_name, "r") as f1, open("input_comment_v2.csv", "a+") as f2:
		i = 3
		flag_text = 0 #variable to check text has 1 or more sentences
		#f2.write("commentId,score,text\n")
		#while True:
			# next_n_lines = list(islice(f1, 6))
			# if not next_n_lines:
				# break
 
			# process next_n_lines: get productId,score,summary,text info
			# remove special characters from summary and text
		output_line = ""
		for line in f1:
			if line.startswith('-------'):
				#f2.write('\n')
				continue
				
			if IsNumber(line):
				if flag_text==1:
					output_line+=','
					flag_text=0
				if i>0:
					#f2.write(line)
					
					if i==1:
						output_line=output_line+line.strip('\n')
						i=i-1
					else:
						output_line=output_line+line.strip('\n')+','
						i=i-1
				else:
					i=3
					output_line+='\n'
					continue
			else:
				if line.startswith('20'):
					continue
				else:
					text = clean_sentence(line.strip('\n')).encode('utf8')
					output_line += text.replace(',','')
					flag_text=1
		f2.write(output_line)
		output_line = ""
 
	print " %s - Converting completed %s" % (datetime.datetime.now(), time_diff_str(t0, time.time()))
	
def get_reviews_data(file_name):
	"""Get reviews data, from local csv."""
	if os.path.exists(file_name):
		print("-- " + file_name + " found locally")
		df = pd.read_csv(file_name)

	return df
 
print 'Begin'
file = open("input_comment_v2.csv", "a+")
file.write("commentId,bookID,text,score\n")
file.close()
number = 1
while (number<=12496):
	plain_name= str(number)+'_comment.txt'
	convert_plain_to_csv(plain_name)
	number=number+1
print 'End'