494976
4111
Quyển sách kèm cd rất dễ học. Giọng đọc như người kể chuyện làm ta thấy thoải mái dễ tiếp thu bài học
4
297249
2016-12-10 03:19:43
--------------------------
466476
4111
Mình mới mua được 3 quyển sách học tiếng anh của tác giả Woo Bo Huyn mà quyển nào cũng ưng vô cùng. Nội dung thì tuyệt khỏi bàn rồi: Có mẫu câu, có ví dụ, có phần giải thích chung, có cách dùng từ và cụm từ, có phiên âm của từ, có CD để nghe. Một cuốn sách thật sự đáng để mua. Mỗi tội cái tem McBooks trên bìa của sách bị bong và mờ một nửa cộng với viền bìa bị rách một chút. Nhưng xét tổng quát thì mình vẫn tăng 5 sao cho sản phẩm.
5
712963
2016-07-02 12:10:15
--------------------------
461167
4111
Sách phù hợp để học ngữ pháp cho giao tiếp. Cấu trúc quyển sách được trình bày một cách thoải mái, không bó buộc theo khuôn mẫu. Từng phần có ví dụ rõ ràng, và nhấn mạnh chỗ cần phải học. Sách có kèm theo CD. Đặc biệt là CD này như là một quyển sách nói vậy, không đơn thuần chỉ là đoạn hội thoại như những quyển sách khác. Đôi lúc mình chép vào điện thoại rồi tối bật nghe khỏi cần đọc sách luôn. Tuy nhiên giọng bản xứ trong CD theo mình hơi lạ lạ, hơi khó nghe so với giọng bản xứ của những quyển sách nước ngoài khác. Tóm lại sản phẩm khá ổn, dùng để tra cứu ngữ pháp khi chém tiếng Anh là tuyệt vời.
4
470835
2016-06-27 16:34:24
--------------------------
460728
4111
Mình rất yếu tiếng anh, nhưng lại ngán với mấy công thức nữ pháp khô khan, mình chỉ muốn biết đọc và học những câu giao tiếp thông dụng, khi khi nhìn thấy quyển này trên tiki mình quyết định thử mua nó. Quyển sách này như một cuốn sổ tay, trong sách ghi rất nhiều câu nói thông dụng, kèm với các cấu trúc sử dụng, rất tiện lợi. Nhưng có một điểm trừ là cách trình bày chưa logic, diễn tả hơi dài dòng. Nên chỉ đọc được một lúc là thấy chán, nhưng nó cũng không phải là một cuốn sách tệ.
3
764462
2016-06-27 11:29:11
--------------------------
406050
4111
Như tên gọi. Sách chủ yếu nhấn mạnh vào 200 mẫu câu tiếng Anh cơ bản và thông dụng trong giao tiếp. Giúp bạn dễ dàng áp dụng nhanh vào thực tế nhờ hình thức trình bày đơn giản, dễ hiểu. Sách được in bằng giấy tốt và màu sắc thì rất rõ ràng. Tuy nhiên, mình không thích CD kèm theo sách cho lắm vì CD được người Việt Nam thu âm nên giọng sẽ không chuẩn và có phần dài dòng, rườm ra khi đề cập qua nhiều vào phần giới thiệu. Còn lại thì sách rất bổ ích.
5
746262
2016-03-27 17:17:01
--------------------------
385516
4111
Sách của chú Woo Bo Huyn rất khác với những cuốn sách tiếng Anh tràn lan trên thị trường. Một phong cách đặc biệt, rất riêng và đậm chất. Không những thú vị mà còn đem lại hiệu quả cao. Sách được dịch bởi cử nhân và giảng viên của khoa tiếng Anh trường Đại học ngoại ngữ Quốc gia và là đầu sách do The Windy MCBooks mang đến nữa thì thật sự rất yên tâm cả về hình thức lẫn nội dung. Tự Học Đàm Thoại Tiếng Anh Phá Cách có kèm CD giúp người học được luyện tập thực tiễn. Thật sự đây là một cuốn sách hay !
5
854660
2016-02-24 07:24:38
--------------------------
374497
4111
Cuốn sach này thì rất hay. Cách viết sách của tác giả giúp người học tiếng Anh có thể ghi nhớ các mẫu câu vô cùng thuận tiện vì có ví dụ, cách sử dụng thực tế kế bên. Mà học tiếng Anh dễ thuộc nhất theo mình là học ví dụ. Cuốn sách này không những đáp ứng nhu cầu học ngữ pháp tiếng Anh của mình mà còn đáp ứng cả nhu cầu học giao tiếp.
Về hình thức thì chất lượng giấy khá tốt, màu sắc cũng khá ổn, hình ảnh minh họa cũng khá dễ thương, tạo hứng thú cho đọc
4
427570
2016-01-26 14:20:59
--------------------------
350495
4111
Mình là một người tự học tiếng Anh qua sách vì không có thời gian đến lớp hay các câu lạc bộ tiếng Anh ngoài trung tâm dạy thêm. Và mình cực tin tưởng sách của Woo Bo Hyun luôn. Mình mua một lượt ba cuốn và cảm thấy trong ba cuốn này thì ĐÀM THOẠI TIẾNG ANH PHÁ CÁCH là quyển sách mà mình thích nhất. Lý do? 
Thứ nhất, sách được biên tập rất tỉ mỉ về mặt nội dung. Từng mẫu câu đàm thoại được trình bày rất rành mạch và theo cấp độ, đằng sau đó còn có cả những ví dụ để người học dễ dàng tham khảo thực hành. Điều đặc biệt là nó giải thích cặn kẽ hoàn cảnh sử dụng những mẫu câu đó, dạy phát âm và cả ngữ pháp. 
Thứ hai, sách được trình bày cực đẹp với bìa đẹp, giấy tốt và in sắc nét. 
Thứ ba, sách kèm CD nên nếu học một mình sẽ dễ dàng theo kịp bài. 
Có điều là lúc mình đặt sách này, khi nhận được không có CD. Mình đã phản hồi lên Tiki và hôm sau Tiki đã không ngại đường xá xa xôi đến giao thêm cho mình dù chỉ là một cái CD nhỏ xíu. Yêu Tiki quá! Thực sự mình rất cảm động. Trong quá trình giao hàng không tránh khỏi sơ sót nhưng Tiki đã nhiệt tình giải quyết một cách rất tốt đẹp, cảm ơn Tiki nhiều lắm luôn...
4
39173
2015-12-10 19:03:49
--------------------------
349540
4111
Tự Học Đàm Thoại Tiếng Anh Phá Cách (Kèm CD) của tác giả  Woo Bo Hyun thật sự rất hay, cách trình bày của tác giả rất hay, hầu hết trong các tình huống đời thường nên rất dễ đọc và tiếp thu. Nếu bạn nào trình độ tiếng anh không khá lắm thì nên mua cuốn sách này về tự học để cải thiện, còn bạn nào tốt thì nên đầu tư cuốn sách khó hơn. Về hình thức thì chất lượng giấy khá tốt, màu sắc cũng khá ổn, hình ảnh minh họa cũng khá dễ thương, tạo hứng thú cho đọc

4
653217
2015-12-08 17:29:37
--------------------------
336661
4111
Mua quyển này chung với quyển tiếng anh ma thuật,
Về màu sắc sách chỉ có hai màu xanh và đen chứ hông đẹp như quyển tiếng anh ma thuật
về nội dung thì gồm 200 mẫu câu cơ bản nhìn hơi nản nhưng mỗi mẫu câu cho công thức và ví dụ dễ hiểu       Tầm 5 câu mỗi ví dụ và sẽ có 1 đoạn hội thoại ngắn 2-3 câu về mẫu đó .
Nếu bạn có lượng từ khá nên mua về xài sẽ dễ áp dụng. 
Chưa nghe cd nên chưa đánh giá 
4
679396
2015-11-12 19:41:50
--------------------------
335317
4111
Thật ra thì với sách của woo bo hyun mình đều cố gắng mua đủ bộ sách của Tác giả. Kiểu như người sắp chết đuối vớ được phao cứu sinh vậy. Mình thì mù tịt tiếng anh, mà cũng ghét ghét học tiếng anh thật. Mà khi đọc cuốn sách này mình chỉ muốn đọc và học luôn và ngay. Cuốn sách này như một cuốn ngữ pháp tiếng nhưng mà nó không nhàm chán như những cuốn sách dạy ngữ pháp tiếng anh của một số tác giả khác toàn thấy chữ là chữ, nhìn xong đã thấy đau đầu. Mà tác giả Woo Bo Hyun đã lồng ghép ngữ pháp vào các câu đàm thoại hàng ngày giúp chúng ta tiếp thu nhanh. Hơn nữa lại được in trên giấy tốt, màu sắc đẹp, có hình ảnh minh họa hài hước. Vì vậy mà mọi người gọi ông là phù thủy tiếng anh quả không sai tí nào.
4
325302
2015-11-10 21:21:15
--------------------------
333878
4111
Tự Học Đàm Thoại Tiếng Anh Phá Cách (Kèm CD), Tác giả Woo Bo Hyun cuốn sách này phải nói canh rất lâu, nhiều lần có hàng quay lại lại hết hàng, lần này có dịp có lại mình mua ngay, tác giả Woo Bo Huyn khỏi phải nói nếu ai đã từng đọc sách của ông của thích lối viết sách của ông rồi, sách luôn in màu sách rõ ràng, ghi chú chi tiết, dễ nhớ, dễ sử dụng và nhỏ gọn dễ mang theo nữa, rất phù hợp cho người tự học nếu cố gắng trình độ sẽ tăng lên nhanh chóng.
4
728931
2015-11-08 14:40:24
--------------------------
312786
4111
Quyển sách Tự học đàm thoại tiếng anh phá cách là một cuốn sách hay của wooo bo hyun. Sách đưa ra rất nhiều mẫu câu hay và bổ ích rất hay được sử dụng trong tiếng anh của người bản xứ. Ngoài ra sách còn giải thích rất linh động và dễ dàng để người đọc hiểu và sử dụng. Không những thế, phần trình bày rất đẹp, dễ hiểu, có thêm phần tranh minh họa sinh động dễ tạo hứng thú cho người học. Bên cạnh dó, sách còn kèm theo đĩa giúp người học có thể nghe và luyện nói dễ dàng.
5
491823
2015-09-22 07:44:39
--------------------------
299775
4111
Về hình thức , sách có trang bìa rất bắt mắt , còn bên trong thì chất liệu giấy rất tốt . Về nội dung , giao tiếp tiếng anh luôn là một mục tiêu lớn đối với người học tiếng anh , nhưng để thật sự có được sự tự tin trong giao tiếp , ta cần nắm vững và thành thạo các tình huống và cấu trúc thông dụng , và quyển sách này giúp ta làm được điều đó  . Sách rất thích hợp cho các bạn học sinh , sinh viên như mình . Giá ngoài nhà sách khá cao , nhưng nếu mua ở tiki thì mức giá rất mềm . Cảm ơn Tiki vì quyển sách này . Mình sẽ tiếp tục ủng hộ tiki . 


5
650543
2015-09-13 15:09:30
--------------------------
286576
4111
Sách Tự học đàm thoại tiếng anh phá cách của Woo Bo Hyun rất hay, mình cũng đọc được vài cuốn của tác giả này. Thật sự cuốn sách không hề gây thất vọng. Khi nhận sách mình rất thích với cách trình bày của nó, bìa sách đẹp, giấy mịn, chữ rõ. Sách có 200 mẫu câu thông dụng, dễ dàng thuận tiện. Mỗi thành ngữ có những ví dụ về mẫu câu, giải thích từ vựng, mỗi chủ đề đều có những từ cùng nghĩa, đoạn hội thoại theo từng ngữ cảnh. Cuốn sách này giúp chúng ta thuận tiện hơn trong việc học tiếng anh.
5
619956
2015-09-01 21:17:49
--------------------------
253986
4111
Phần lớn giáo trình tiếng Anh hiện nay đều rất máy móc, đọc thôi còn gây buồn ngủ nói chi học thuộc. Với mình cuốn Tự Học Đàm Thoại Tiếng Anh Phá Cách là 1 giải pháp hay. Nội dung rõ ràng, dễ hiểu, bố cục đẹp mắt. Điều ghi điểm nhất chính là các cấu trúc có độ thực tế, tính ứng dụng cao. Sách còn có CD kèm theo để luyện đọc chính xác và cả luyện nghe nữa. Mình chỉ không thích việc bao đĩa CD bị đóng vào gáy sách, bọc rời và dán kèm bên ngoài thì tốt hơn.
5
141394
2015-08-04 22:00:57
--------------------------
239898
4111
Nội dung của cuốn này thì rất tuyệt vời, mình thích ngay từ khi đọc thử trên web. Cách viết sách của tác giả giúp người học tiếng Anh có thể ghi nhớ các mẫu câu vô cùng thuận tiện vì có ví dụ, cách sử dụng thực tế kế bên. Mà học tiếng Anh dễ thuộc nhất theo mình là học ví dụ. Cuốn sách này không những đáp ứng nhu cầu học ngữ pháp tiếng Anh của mình mà còn đáp ứng cả nhu cầu học giao tiếp. Tuy nhiên, mình rất khó chịu khi mở sách ra thấy khoảng mười mấy trang bị nhăn từ trên xuống dưới ở nguyên cáp mép ngoài. 
4
57860
2015-07-24 09:59:58
--------------------------
239408
4111
Mình đang loay hoay không biết mua quyển sách nào để phục vụ tốt cho việc học Tiếng anh của mình. Tình cờ thấy trên Tiki có quyển Tự Học Đàm Thoại Tiếng Anh Phá Cách (Kèm CD) được các bạn khen hay nên mình đặt mua thử. Khi nhận hàng từ Tiki mình thấy rất ưng ý về quyển sách. Tác giả trình bày những mẩu đàm thoại rất thông dụng, thực tế, sau mỗi bài học thì có từ vựng đẻ cho người học tham khảo. Dĩa CD được hướng dẫn cách học rất chi tiết. Sách in trên giấy khá đẹp nhưng so với giấy in trên quyển Ma thuật của Woo Bo Hyun thì không bằng thôi. 
5
460604
2015-07-23 21:26:35
--------------------------
238864
4111
Mình đang loay hoay không biết mua quyển sách nào để phục vụ tốt cho việc học Tiếng anh của mình. Tình cờ thấy trên Tiki có quyển Tự Học Đàm Thoại Tiếng Anh Phá Cách (Kèm CD) được các bạn khen hay nên mình đặt mua thử. Khi nhận hàng từ Tiki mình thấy rất ưng ý về quyển sách. Tác giả trình bày những mẩu đàm thoại rất thông dụng, thực tế, sau mỗi bài học thì có từ vựng đẻ cho người học tham khảo. Dĩa CD được hướng dẫn cách học rất chi tiết. Sách in trên giấy khá đẹp nhưng so với giấy in trên quyển Ma thuật của Woo Bo Hyun thì không bằng thôi.
5
460604
2015-07-23 14:45:39
--------------------------
238427
4111
Mình đang loay hoay không biết mua quyển sách nào để phục vụ tốt cho việc học Tiếng anh của mình. Tình cờ thấy trên Tiki có quyển Tự Học Đàm Thoại Tiếng Anh Phá Cách (Kèm CD) được các bạn khen hay nên mình đặt mua thử. Khi nhận hàng từ Tiki mình thấy rất ưng ý về quyển sách. Tác giả trình bày những mẩu đàm thoại rất thông dụng, thực tế, sau mỗi bài học thì có từ vựng để cho người học tham khảo. Dĩa CD được hướng dẫn cách học rất chi tiết. Sách in trên giấy khá đẹp nhưng so với giấy in trên quyển Ma thuật của Woo Bo Hyun thì không bằng thôi.
5
460604
2015-07-23 10:31:59
--------------------------
235440
4111
tất cả quyển sách của tác giả Woo Bo Huyn mình đều mua trên tiki do nhà xuất bản MCBook phát hành. Về nội dung thì khá hay, từ nội dung sách, tích hợp nhiều câu văn thông dụng trong giao tiếp, có ví dụ, từ vựng khó trong bài và một số lưu ý rất hay, và CD cũng thú vị. tuy nhiên đây là quyển thứ 3 mà mình không hài lòng về cách đóng sách. Vấn đề của quyển này là nó đóng 25 tờ đầu bị xẹo ra so với các trang còn lại, làm mình vô cùng khó chịu. Mong là MCBook có thể khắc phục nhược điểm đóng sách của mình. Nhưng nếu bạn may mắn thì có thể mua được quyển sách được đóng tốt hơn.
3
299660
2015-07-21 08:22:08
--------------------------
223445
4111
Một cuốn sách phù hợp cho việc học tiếng Anh. "Tự Học Đàm Thoại Tiếng Anh Phá Cách" sẽ giúp bạn học được các mẫu câu một cách tự nhiên nhất. Không giống như phương pháp học tập chúng ta được học ở trường phổ thông không mấy hiệu quả cuốn sách này đã đưa ra một phương pháp mới đúng như cái tên của nó.Mỗi ngày bạn đọc vài câu tiếng Anh và bắt chước theo cộng với sự kiên trì chắc chắn bạn sẽ có thể nói được tiếng Anh. Sách trình bày đẹp, chất lượng giấy tốt.
4
405346
2015-07-07 08:55:32
--------------------------
220625
4111
Nhìn tổng thể cuốn sách được thiết kế đẹp mắt, dày dặn, chất liệu giấy tốt, kiểu chữ, cỡ chữ phù hợp, kèm theo 1 số hình ảnh vui nhộn tránh nhàm chán cho người học.
Nội dung được thiết kế rất hay, với lượng cấu trúc và từ vựng khá phong phú. Với mỗi cấu trúc sẽ gồm phần giải thích hoàn cảnh sử dụng cấu trúc rõ ràng bằng tiếng Việt, kèm theo 3 ví dụ minh họa cụ thể. Tiếp theo sẽ đến từ cần chú ý phát âm cho chuẩn, và cuối cùng là phần chú ý các trường hợp tương tự, đặc biệt so với cấu trúc. Với nội dung mỗi cấu trúc không dài, và lượng kiến thức trọng tâm giúp người học dễ nắm bắt và học hiệu quả. Phần đĩa CD kèm theo được ghi âm to, rõ và tốc độ đọc phù hợp. Cuốn sách có phương pháp học thực sự hữu dụng cho mọi đối tượng học tiếng Anh.
5
634712
2015-07-02 20:17:03
--------------------------
220523
4111
- Sách của tác giả Woo Bo Hyun luôn là những cuốn sách hay các phần trong sách rất rõ ràng, mình đã mua đủ 1 bộ sách của Tác giả trên TiKi và mình đánh giá như sau:
- Cuốn sách này như một cuốn ngữ pháp tiếng anh theo lối học rất riêng của tác giả, nó không nhàm chán như những cuốn sách dạy ngữ pháp tiếng anh của một số tác giả khác học theo giáo trình rất khó nhớ, tác giả Woo Bo Hyun đã lồng ghép ngữ pháp vào các câu đàm thoại giúp chúng ta tiếp thu nhanh, dễ dàng nhớ vì áp dụng trong thực tế hàng ngày chúng ta sống.
- Bộ sách của tác giả Woo Bo Hyun là một bộ sách hay nhất mà mình được cầm trên tay, lối dạy rất thực tế và thông dụng hàng ngày cách ông học không ép chúng ta cố nhồi nhét các thì, các câu ngữ pháp như giáo trình mình học trong sách giáo khoa mà dạy theo một cách khác cái cách mà tác giả đã từng học và đem lại hiệu quả, ông đúng là một bậc thầy trong cách luyện Tiếng Anh
- Hai từ " Best Saller" mà mọi người ban tặng ông quả không sai tý nào, và mình rất rất thích cuốn " Tự học giao tiếp Tiếng Anh thành thạo" một cuốn trên cả tuyệt vời.
- Nói tóm lại mình không thấy ân hận khi sưu tập đủ 1 bộ sách của tác giả Woo Bo Hyun và Tiki là nơi mình tin tưởng đặt mua sách vì TiKi có dịch vụ sau bán hàng rất tốt, nếu sách bị nhăn nheo không như ý muốn đều được TiKi đổi một cách nhiệt tình nên trong thời gian tới khi có nhu cầu mua sách, TiKi luôn là sự lựa chọn đầu tiên.
- Cám ơn tác giả đã cho ra mắt cuốn sách hay và cũng cám ơn TiKi đã bán những cuốn sách chất lượng.
5
657350
2015-07-02 17:30:03
--------------------------
200518
4111
Đây là 1 quyển sách khá hay,sách giúp cho người học ( đặc biệt là những người tự học ) nắm được các mẫu câu căn bản trong giao tiếp tiếng Anh, khiến cho việc học và ghi nhớ tiếng Anh không còn máy móc mà rất tự nhiên và linh hoạt.
Với hình thức đưa ra các mẫu câu căn bản và lượng từ phong phú, sách giúp người học mau chóng nắm được tiếng anh căn bản
Chất lượng giấy tốt, dày, chữ in rõ là những phầm điểm cộng thêm cho sách, 
Đây thật sự là 1 quyển hữu dụng cho trình độ người học căn bản
5
368991
2015-05-25 11:14:52
--------------------------
