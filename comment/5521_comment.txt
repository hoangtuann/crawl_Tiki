545557
5521
Với những nét vẽ hiện đại và chân thực, những câu chuyện về Holmes được thể hiện thật rõ ràng và dễ hiểu. Có thể bạn đã đọc những mẩu chuyện này dưới dạng tiểu thuyết, nhưng tin tôi đi, cảm giác sẽ thật khác khi bạn cầm trên tay một trong những quyển truyện tranh về Holmes này. Những điều trừu tượng được sáng tỏ, những cảm xúc thể hiện trên khuôn mặt, những đắn đo, những âm mưu...tất cả đều rất rõ ràng.
5
301861
2017-03-17 09:44:59
--------------------------
269548
5521
Mình là một fan của truyện Sherlock Homes nên khi thấy combo này mình quyết định mua ngay. Cách tiki gói quà cũng rất cẩn thận. Khi cầm cuốn truyện trên tay mình thấy rất thích, chất liệu giấy tốt, màu sắc hơi u ám một xíu nhưng vậy mới giống truyện trinh thám. Nội dung truyện của Conan Doyle nói chung rất ổn, tình tiết lôi cuốn, mình đã đọc đến lần thứ 3. Nói chung mình rất ưng ý
5
621642
2015-08-17 15:22:13
--------------------------
161835
5521
Truyện sherlock homes có lẽ là truyện về trinh thám phá án hay nhất. Nếu như thời nhà Đường có Địch Nhân Kiệt là thần thám nổi tiếng thì ở phương tây, sherlock homes được xem là  nổi tiếng nhất. Bộ truyện bao gồm nhiều truyện ngắn với nội dung xoay quanh các vụ án ly kỳ, hấp dẫn và gây cấn đồng thời với lối phá án cực kỳ logic và thuyết phục người đọc. Ngoài việc có nội dung hay truyện còn lôi kéo người đọc tham gia suy luận đề giải quyết các vụ án. Nếu ai đam mê về truyện trinh thám thì đây là bộ truyện phải đọc.
5
356759
2015-02-28 23:51:16
--------------------------
