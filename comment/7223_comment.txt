577245
7223
Mình đã mua cuốn sách này, và cảm thấy rất hài lòng, đẹp dễ nhìn,
5
931021
2017-04-19 09:49:03
--------------------------
375775
7223
Tiếng Anh Dành Cho Thiếu Nhi là một cuốn sách có nội dung hay (các bài học được biên soạn rất phù hợp, gần gũi với trẻ) nhưng về phần CD của sách, khi mở CD lên thì mình khá thất vọng vì ngoại trừ các phần Vocabulary và Check it có bài nghe chuẩn do người bản ngữ đọc thì phần Practice có bài nghe không hay cho lắm (hình như là do người Việt đọc). Mình mong rằng nhóm biên soạn sách sẽ sửa chữa bài nghe của phần Practice để Tiếng Anh Dành Cho Thiếu Nhi sẽ mãi là một cuốn sách hay cho trẻ.
3
784709
2016-01-29 09:32:19
--------------------------
