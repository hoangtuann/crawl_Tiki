435351
9881
Mình chọn truyện này vì khá ấn tượng với tựa sách và hy vọng sẽ đọc được 1 truyện tình cảm động , khắc cốt ghi tâm. Phần đầu truyện xây dựng tính cách nhân vật khá ổn nhưng càng về sau lại càng nhạt dần, đến nỗi đọc xong mình dường như quên gần hết nội dung câu chuyện. Truyện không có cao trao, miêu tả tình cảm giữa 2 nhân vật khá nhẹ nhàng (Có thể do mình không thích hợp với thể loại truyện tình cảm nhẹ nhàng quá). Sự day dứt của truyện cũng chỉ đến từ sự lo lắng sợ hãi của bản thân 2 nhân vật. Nói chung là 1 lần bị rắn cắn cả đời sợ dây thừng vậy. Sự dùng dằng qua lại của 2 bên thiệt khiến người theo dõi mệt mỏi mà. Tóm lại nếu ai chỉ thích đọc giải trí không hại não thì truyện này cũng tạm chấp nhận được, nhưng nếu yêu cầu 1 thể loại sâu sắc đọc để ngẫm thì có lẽ là không nên. 
3
6253
2016-05-24 23:07:37
--------------------------
430773
9881
Một tình yêu đẹp thời sinh viên làm quãng thời gian đại học của Tô Cẩn và Nhan Bác thêm tươi đẹp ngọt ngào. Tô Cẩn là một cô gái dám yêu dám tỏ tỉnh dám thể hiện ra chứ không nhút nhát giữ trong lòng. Tôi ngưỡng mộ điều này. Thời đại này chuyện cọc đi tìm trâu cũng không còn là chuyện lạ nữa. Nhan Bác cũng nhờ chiến thuật đặc biệt của Tô Cẩn mà yêu cô. Nhưng vì hiểu lầm mà hai người xa cách nhau tận sáu năm. Tuy nhiên đến cuối cùng mọi hiểu lầm đều được hoá giải. Nhan Bác và Tô Cẩn lại quay về với nhau. Cốt truyện khá quen thuộc.
4
610589
2016-05-15 21:05:12
--------------------------
382401
9881
Cuốn sách rất hấp dẫn. Nó kể về câu chuyện nói về cuộc tình của hai nhân vật chính. Lúc đầu yêu nhau sau đó chia tay. Khi hai người gặp lại nhau thì là lúc cô gái chuẩn bị lấy chồng. Liệu cô gái có từ bỏ người yêu hiện tại - người sắp là chồng của cô để quay trở lại với chàng trai người đã là mối tình đầu của cô? Liệu hai người có được hạnh phúc? Cuốn sách với tựa đề rất hay và ý nghĩa. Bìa sách thì đẹp mê li khỏi chê. Hãy đọc và cảm nhận nhé các bạn
5
1101166
2016-02-19 08:02:00
--------------------------
369067
9881
Vốn từ cách hành văn mềm mại , dịu dàng những câu từ , lời nói nhân nhượng từ giới phái khác nhau , trích dẫn cho ta hiểu thế nào là nhìn nhận đúng ý kiến từ vẻ bề ngoài như phán đoán nữa mà chính là sự thiếu tự tin trong giao tiếp mà thôi , chưa chắc đã biết hẳn về tài năng , sự khám phá ra những thiếu sót trong suy nghĩ nữa mà thôi , nụ cười luôn gắn với mỗi người phụ nữ sinh ra không phải là vẻ đẹp mà chính là sự thôi thúc , nguồn khát vọng .
4
402468
2016-01-15 01:16:27
--------------------------
300674
9881
Mình chưa bao giờ đọc các tác phẩm của nhà văn Mộc Cẩn Thiên Lam này cả,đây là cuốn sách đầu tiên mà mình đọc của tác giả này,cuốn sách có trang bìa đẹp,nghe cái tên "Hãy cứ yêu như chưa từng tổn thương" làm mình liên tưởng ngay đến một nhân vật nữ vô cùng mạnh mẽ và mình sẽ rất thích!Nhưng mình thật thất vọng(có lẽ đã hy vọng nhiều quá),câu chuyện không có gì là hấp dẫn,không có chi tiết nhấn mạnh nào để gây sự chú ý hay gây sự cuốn hút để khiến người đọc thích cả!!
3
413637
2015-09-14 09:20:39
--------------------------
274844
9881
Tác phẩm này nói về hai câu chuyện tình của Tô Cẩn và Ngô Tiểu Đồng. Tình yêu lúc còn trẻ đầy mãnh liệt, đầy bất chấp, có chút ngây ngô, có chút hồn nhiên, bướng bỉnh nhưng trong đó cũng có những lo sợ trong tình yêu, những khó khăn do vấn đề cơm áo gạo tiền. Mình rất thích tính cách của Tô Cẩn, dám yêu, dám bất chấp cả, đồng thời cũng dám buông tay. Trong cuộc đời, được mấy ai có được một tình yêu lúc còn trẻ đẹp như vậy. Và cũng có mấy ai có thể giữ được tình yêu có đến cuối cuộc đời. Câu chuyện của Tô Cẩn - Nhan Bác, Ngô Tiểu Đồng - Thẩm Gia Ngôn, họ từng yêu, từng tổn thương nhưng họ đã bỏ qua tất cả để đến bên nhau. Đây là một tác phẩm hay, mình thích lắm. 
4
321923
2015-08-22 12:27:59
--------------------------
271426
9881
Đến với cuốn sách này trong lúc bỡ ngỡ, hoang mang với tình yêu, với những rung động đầu đời, tôi nào có ngờ bản thân lại bị cuốn hút, si mê như vậy. Hai câu chuyện tình yêu, Tô Cẩn và Nhan Bác, Ngô Tiểu Đồng và Thẩm Gia Ngôn, họ là đại diện cho những năm tháng thanh xuân rực rỡ, huy hoàng dẫu có vụng dại, ngốc nghếch của mỗi chúng ta. Tìm thấy ở các nhân vật là sự kiên quyết, cố chấp, bướng bỉnh trong tình yêu, vì yêu một người mà bất chấp tôn nghiêm bản thân như Tô Cẩn, lo sợ cho tương lai mù mịt mà thiếu vững tin như Nhan Bác; dằn vặt giữa gia đình và tình yêu mà buông tay như Thẩm Gia Ngôn, hay là sự mạnh mẽ dẫu có chông gai như Ngô Tiểu Đồng. Trên hết thảy mâu thuẫn nội tâm ấy, cuối cùng họ vẫn tìm lại nhau giữa biển người mênh mang, giữa bàn tay vô tình của thời gian. Xa cách không đáng sợ, đáng sợ là anh và em không còn yêu nhau nữa.
Có thể trong cuộc sống, mỗi người rồi sẽ bị tình cảm tổn thương, dày vò. Nhưng không phải vì thế mà lo sợ không dám yêu lần nữa. Có thể ai trong chúng ta không có cơ hội gặp lại người mình từng yêu như trong sách truyện, thế nhưng ngã rẽ phía trước, biết đâu người tiếp theo lại là người mà ta chờ đợi.
Bởi đời người dễ được mấy lần yêu, cho nên Hãy cứ yêu như chưa từng tổn thương, bạn nhé !
Bìa sách rất đáng yêu, nét mực in đều, không thiếu nét, và tôi đặc biệt thích cái màu ngà ngà của giấy. 

5
478050
2015-08-19 09:23:09
--------------------------
271260
9881
Tác phẩm thu hút tôi từ chính cái tên của nó "Hãy Cứ Yêu Như Chưa Từng Tổn Thương". Cùng với đó là trang bìa sách, được thiết kế với tông màu trầm, mang đến cái cảm giác xa xăm và cô độc. Mộc Cẩn Thiên Lam xây dựng nên nhân vật nữ chính khác hẳn với tôi, Tô Cẩn - người con gái mạnh mẽ, kiên cường và ngạo mạn, cô dám yêu, dám nói dẫu cho phải "cọc đi tìm trâu". Cái cách tác giả dẫn dắt câu chuyện đã làm tôi nhớ về một thời thanh xuân của mình, cái thời mà "chúng ta đã vứt bỏ sự tôn nghiêm, cá tính và cả sự cố chấp của mình, cũng là vì không thể từ bỏ được một người". Đây là một tác phẩm rất hay, đáng đọc nhưng có một điểm khiến tôi không thích lắm là sách cầm khá nặng tay.
4
44933
2015-08-18 23:57:02
--------------------------
205160
9881
Ngôn tình không hẳn lúc nào cũng sến sẩm, sướt mướt và đầy sự bi lụy. Đọc "Hãy cứ yêu như chưa từng từng tổn thương" tôi cảm thấy đâu đó có bóng dáng của mình trong những tháng ngày sinh viên giờ đã xa. Thực ra tôi cảm thấy mình khá giống Tô Cẩn là cũng gặp và thích một chàng trai từ cái nhìn đầu tiên. Nhưng khác với cô, tôi không tự tin tiếp cận anh, không tự tin tạo cho mình cơ hội để có kế hoạch tấn công anh giống như Tô Cận với Nhan Bác. Chính vì thế, tôi cảm thấy vô cùng ngưỡng mộ cô gái này bởi sự mạnh mẽ, chân thành, dám yêu dám nói. Cuộc đời có những thứ đôi khi cũng không thể lí giải được, Tô Cẩn và Nhan Bạc cũng thế,họ yêu nhau bằng một tình yêu chân thành, lãng mạn, nhưng rồi trước những sóng gió, trước những lo lắng, toan tính cho tương lai, họ đã buông tay nhau. Những tưởng hạnh phúc của cặp đôi này không còn kéo dài nhưng trước nỗi đau mà họ đã đi qua, may mắn là tình yêu chưa thực sự rời xa họ, để rồi bất chợt nhận ra rằng họ không thể sống thiếu nhau và dù rằng hai trong số họ có đi đến bất cứ nơi nào vẫn sẽ có người kia đứng đợi nơi cuối con đường...Mộc Cẩm Thiên Lam với giọng văn ngọt ngào, lãng mạn đã mang lại một tác phẩm hay và ý nghĩa. Để rồi chúng ta nhận ra một điều rằng "chúng ta đã vứt bỏ sự tôn nghiêm, cá tính và cả sự cố chấp của mình, cũng là vì không thể từ bỏ được một người". Vì thế "Hãy cứ yêu như chưa từng tổn thương".
4
450212
2015-06-05 23:16:43
--------------------------
197394
9881
Mình hiếm khi đọc mấy thể loại sách ngôn tình của Trung Quốc vì cảm giác cốt truyện nào cũng giống nhau còn ngôn từ quá bi lụy nhiều khi sến sẩm. Nhưng không hiếu sao nhìn thấy cuốn sách nay lại đặt mua. Chắc tại cái tựa :) Nhưng cuốn sách cũng không làm mình thất vọng. Mình thích cái cốt truyện , nhân vật cả cái thông điệp mà cuốn sách này mang lại. Tuổi trẻ là như vậy , dẫu có biết bao lần tổn thương nhưng một khi đã vẫn còn yêu sẽ luôn luôn bao dung và tha thứ. Dẫu cho có từng bị chia cắt, nhưng chỉ cần biết có người quay đầu lại, thì vẫn có một người luôn sẵn sàng đợi ở chốn cũ, để nắm tay đi tiếp con đường còn lại...
1
75031
2015-05-17 15:26:34
--------------------------
177483
9881
Chia tay rồi còn có thể làm lại từ đầu không? Không thể, vết thương ngày xưa dẫu có lành cũng vẫn còn sẹo mà. Nhưng Tô Cẩn và Nhan Bác lại không như vậy. Tình yêu thời thanh xuân đẹp đẽ của họ không thể vượt qua ngưỡng cửa vào đời, nhưng đến khi trùng phùng, họ đã phát hiện ra, bản thân không thể từ bỏ tình yêu này, dù không dám chắc bản thân có còn yêu đối phương được nữa hay không. Rốt cuộc là yêu đối phương của thời hiện tại, hay là luyến tiếc mối tình đẹp ngày xưa? Tô Cẩn không biết, cô chỉ biết cô không thể mất anh một lần nữa. Đi qua nỗi đau, mới thấy thì ra người ấy vẫn đang chờ đợi. Vì vậy, đừng sợ tổn thương.
4
57459
2015-04-03 11:05:33
--------------------------
138849
9881
Có lẽ điều ấn tượng mình nhất đối với quyển sách này là ở phần thiết kế bìa sách. Vừa nhìn vào hẳn ai cũng sẽ khó mà rời mắt được nó. Những hình ảnh đẹp nhưng lại cô độc và thoáng nỗi buồn sâu sắc. Những nỗi buồn do tình yêu, do tổn thương trong tình yêu.
Câu chuyện là hành trình tìm tình yêu và sống vì tình yêu của cô gái Tô Cẩn - một cô gái dám sống vì tình yêu, vượt qua được những tổn thương trong tình yêu và luôn làm những điều mà cô cho nó là đúng. Đôi khi cảm thấy thương cho Tô Cẩn khi tình yêu của cô dang dở, những đau buồn ấy khắc sâu vào trong tim...Nhưng rồi, khi Nhan Bác quay trở lại, cô đã thứ tha vì tình yêu, vượt qua những tổn thương trong tình yêu để được hạnh phúc. Có lẽ cái kết đẹp này đều làm hài lòng đọc giả :)
Câu chuyện tuy không nhiều cao trào, không nhiều kịch tính nhưng không làm người đọc nhàm chán. Đó là cái hay của tác giả, chỉ với những câu chữ bình dị, mộc mạc nhưng vẫn đủ sức lôi cuốn người đọc đến những trang sách cuối cùng!
4
303773
2014-12-04 22:19:47
--------------------------
130880
9881
Mua cuốn này về vì thấy tên hay, bìa sách cũng đẹp, gợi chút gì đó buồn man mác, nhìn chung là vừa mắt mình (lúc ấy còn tưởng đây là một tập tản văn cơ). Truyện không có tình huống cao trào, không kịch tính, giọng văn cũng bình dị, nhẹ nhàng, có thể nhiều người cho là nhạt nhưng mình lại rất thích. Mình khá thích Tô Cẩn, thích sự tự tin của cô, thích cái cách cô yêu – chân thành, sâu sắc, thích cái cách cô kiên trì theo đuổi những gì mình cho là đúng, dám yêu lại ngay cả khi đã từng bị tổn thương. Kết truyện, Tô Cẩn và Nhan Bác quay lại với nhau, âu cũng là một kết thúc đẹp. Truyện cho mình thấy một điều: dù đã từng đau, dù đã từng tổn thương, tình yêu thương vẫn sẽ là liều thuốc hữu hiệu nhất xoa dịu nỗi đau trong tâm hồn mỗi con người.
4
393748
2014-10-20 19:48:02
--------------------------
106296
9881
Tôi ấn tượng nhất trong truyện chính là câu nói của bố Tô Cẩn khi khuyên nhủ cô đừng đi Mỹ. Những lời nói của bố Tô Cẩn như giải được mối tơ  vò trong lòng cô, như khiến cô phải vỡ lẽ những gì mình đã làm , đã suy nghĩ như thế nào trước đây. Trong lòng cô dấy lên sự bối rối, cô vẫn yêu anh nhưng không chắc có sự ngược lại nơi anh. Tô Cẩn dần dần hiểu ra, Nhan Bác là một vị trí nào đó rất quan trọng với cô, dù cô có chối bỏ đến mấy, đứng trước anh, trái tim cô vẫn lỗi nhịp đập. Câu chuyện kết thúc nhẹ nhàng ấm áp vừa lòng người đọc, rất thích hợp để nhâm nhi quyển sách này trong những buổi trà chiều :)
4
286556
2014-02-17 20:20:18
--------------------------
103442
9881
Yêu, đâu phải ai cũng dám đương đầu chấp nhận sự thật. Đôi khi họ sợ hãi, sợ tổn thương, họ chọn cách rời xa. 
Nhưng cuối cùng, chính họ lại làm tổn thương họ.
Nỗi đau của Tô Cẩn .... một cô gái từ nhỏ đã không khóc, vậy mà cuối cùng cô đã đau lòng rất nhiều.
Nhan Bác, tình yêu của anh dần dần... từ từ và rồi ngoại truyện đã chứng minh , anh yêu Tô Cẩn còn nhiều hơn cô yêu anh. 
Thực sự mình đã rất xúc động khi đọc cuốn này, miêu tả TÌNH YÊU, vị tình yêu rất ĐẬM, cả cái miêu tả cảm xúc , miêu tả tình yêu của 2 người cũng rất THẬT, và khiến người đọc cảm nhận được!
Hơn nữa, tình yêu này tự nhiên chứ không như những cuốn sau này mình đọc - không tự nhiên mà cũng không thật.
Nói chung mình rất thích cuốn này. Và đây cũng là cuốn truyện tình cảm Trung Quốc đầu tiên mình đọc.
5
256028
2013-12-30 20:57:03
--------------------------
91595
9881
Tôi thực sự rất thích cuốn sách này. Thích từ tên truyện cho đến những câu văn đáng để ngẫm nghĩ và đặc biệt là tính cách của Tô Cẩn. Là con gái, mấy ai lại có thể vượt lên những quy tắc, lòng tự trọng và sự cố chấp của bản thân để yêu một người, mấy ai có thể dũng cảm bày tỏ tình cảm với người mình thích. Sau khi phải hứng chịu biết bao nhiêu tổn thương mà Tô Cẩn vẫn có thể quay lại và sống hạnh phúc với Nhan Bác. Điều này làm tôi rất xúc động. Không phải vì tình tiết câu chuyện mà vì sự dũng cảm của hai nhân vật chính. Họ đã dùng tình yêu của mình để lấp đầy khoảng trống của trái tim. Đó là điều mà tôi không thể làm được. Rất ngưỡng mộ Tô Cẩn
5
117965
2013-08-20 22:44:09
--------------------------
86007
9881
Hãy cứ yêu như chưa từng tổn thương - tựa đề của cuốn sách đã nói lên nội dung của nó. Nội dung của truyện không có nhiều đột phá hay nói cách khá là rất nhẹ nhàng. Nhưng tôi lại thích sự nhẹ nhàng đó. Đây là một cuốn tiểu thuyết nhưng tôi thấy nó dường như giống như thước phim hồi ức của một người. Cô đã kể về những kỉ niệm và trải nghiệm của cô từ lúc đại học đến hiện tại. Câu chuyện tái hiện theo trình tự thời gian một cách rõ ràng. Nhờ đó mà người đọc rõ ràng trong từng tình tiết truyện. Có thể nhiều người cho rằng cuốn sách này nhạt nhưng tôi lại không cho là như thế. Tôi thích những gì nhẹ nhàng. Cuốn tiểu thuyết này mang lại cho tôi cảm giác bình an ấy, kể cả khi hai nhân vật chính đã không còn ở bên nhau nhưng tôi vẫn tin chắc rằng họ sẽ trở về bên nhau. Và cuối cùng, họ đã trở về bên nhau. Hãy đọc cuốn tiểu thuyết này thật chậm để cảm nhận được cái hay và ý nghĩa của nó nhé! Tôi rất thích " Hãy cứ yêu như chưa từng tổn thương".
4
59233
2013-07-08 17:43:17
--------------------------
82953
9881
Cuốn sách này thu hút tôi bởi tên truyện và bìa sách khá bắt mắt nhưng sau khi mang về nhà đọc thì mới biết câu nói "đừng đánh giá cuốn sách qua bìa" thật là đúng! Cốt truyện ổn nhưng lời văn quá sến và nội dung nhàm chán, giọng văn của loại truyện cổ trang cũng không quá nhàm chán và sến súa như cuốn này. Tình tiết của truyện cũng quá nhẹ nhàng, không kịch tính nên thành ra đọc rất buồn ngủ. Câu chuyện tình yêu của các nhân vật cũng bình thường, không đặc sắc. Chắc hẳn là do tôi không hợp với thể loại truyện tình cảm nhẹ nhàng. Cuốn sách này không để lại cho tôi nhiều ấn tượng. Thật sự tôi không thích cuốn sách này tí nào!
2
116740
2013-06-24 12:44:20
--------------------------
78247
9881
Tất cả những nhân vật trong câu chuyện này đều đã từng yêu, và cũng đã từng một lần làm tổn thương nhau
Trong tình yêu, không tránh khỏi những lúc bạn cảm thấy mệt mỏi, chông chênh, không biết trước mắt mình con đường sẽ thế nào?
Có đôi khi, tổn thương nhau, buông tay nhau không có nghĩa là hết yêu nhau, nhưng đã buông tay nhau, liệu còn có tương lai nữa hay không?
Không có nhiều cặp đôi yêu nhau, chia tay, xa cách vài năm mà vẫn quay lại được
Một phần vì tổn thương trong quá khứ sâu đậm, 1 phần là chẳng ai trong số họ chắc chắn về phần tình cảm của mình
Những nhân vật trong truyện cũng vậy, họ cũng trải qua các cung bậc khác nhau của tình yêu, nhưng cuối cùng ai trong số họ cũng tìm được hạnh phúc riêng cho mình, cho dù có đau đi chăng nữa, cho dù bạn có bị tổn thương đi chăng nữa, nhưng tình yêu lại 1 lần nữa có thể chữa lành vết thương của bạn.
3
1331
2013-05-31 19:18:51
--------------------------
77734
9881
Mình ấn tượng ban đầu Tô Cẩn dưới sự miêu tả của tác giả là cô gái mạnh mẽ, tài giỏi, và rất bản lĩnh, những gì cô muốn thì chỉ cần kiên trì lại có thể đạt được. Nhưng càng về sau truyện lại càng nhạt dần, đến mức mình chỉ mong đọc qua cho xong. 2 nhân vật chính ko thật lòng với nhau, ko đủ dũng cảm đối diện với tình cảm của bản thân, Tô Cẩn trở thành cô gái yếu đuối, lệ thuộc, và hay khóc, cô chẳng còn giữ được cái bản chất vốn có của mình. Nhan Bác thì yêu thương nhưng lại quá bi quan, có sá gì khó khăn nếu anh chịu cùng cô bước tiếp. Từ chỗ nghĩ cho nhau, 2 người đã trở nên quá ích kỷ, chỉ sợ bản thân mình tổn thương và đã chia tay nhau theo cách ấy, bỏ lỡ 6 năm thanh xuân mà theo mình là rất phung phí, cũng ko phải là lý do gì quá to lớn để 2 người phải lạc mất nhau một cách đáng tiếc như vậy. Mình đánh giá ko cao quyển sách này, nếu bạn mong đợi một câu chuyện tình khắc cốt ghi tâm thì mình nghĩ quyển sách này ko dành cho bạn.
2
34200
2013-05-29 13:05:54
--------------------------
77072
9881
Tô Cẩn là 1 cô gái luôn rất tự tin vào bản thân, cô luôn cho rằng chỉ cần bản thân mình không từ bỏ thì sớm muộn gì cũng đạt được mục đích. Tình yêu của cô cũng vậy, cô luôn liên trì theo đuổi Nhan Bác. 1 anh chàng thông minh, lạnh lùng, luôn che giấu cảm xúc của bản thân nhưng nhờ sự cuồng nhiệt, kiên trì của Tô Cẩn trái tim anh đã dần hé mở, hướng về cô.
Những ngày đầu, tình yêu của họ đầy ấp sự nhẹ nhàng, hạnh phúc. Nhưng không gì là mãi mãi, giá như Nhan Bác bộc lộ bản thân nhiều hơn, chia sẻ với Tô Cẩn nhiều hơn, và Tô Cẩn giá như không đánh mất sự tự tin của mình như trước kia, luôn tin tưởng Nhan Bác thì họ đã không phải chia tay mặc dù vẫn còn rất yêu thương nhau.
Sau nhiều năm gặp lại, họ đã trưởng thành hơn, tình cảm của họ vẫn luôn hướng về nhau.
Tình yêu của họ tuy gặp bao trắc trở nhưng nó giúp tình cảm của họ càng thêm vững chắc.
3
54371
2013-05-25 17:42:02
--------------------------
76384
9881
"em chỉ hy vọng anh đừng lúc nào cũng tiến lên phía trước, em sợ em sẽ không đuổi kịp anh, càng sợ anh sẽ bỏ lỡ những phong cảnh đẹp trên đường. Trên thế gian này, những gì đẹp nhất, tốt nhất, em đều hy vọng có thể cùng anh tận hưởng. Nhưng không phải chỉ có thế, dù đau khổ em cũng muốn chia sẻ cùng anh, có thể cùng anh đi hết con đường núi càng lúc càng khó khăn này. Nếu bên cạnh anh không có một người bầu bạn thì cho dù anh có leo được lên đỉnh núi cũng có nghĩa lý gì? Anh chắc chắn sẽ cô đơn, sẽ buồn chán, em không nhẫn tâm để anh lại một mình. Em muốn ở bên anh, chúng ta cùng nhau leo tới đỉnh núi và ngắm nhìn thế giới. Nhưng em hy vọng thỉnh thoảng anh có thể dừng lại, nhìn ngắm cảnh đẹp bên đường. Có thể đó là điều anh không muốn, nhưng biết đâu, sẽ thấy nó đẹp hơn?"
Tôi phải công nhận tình cảm của Tô Cẩn rất chân thành, đọc cả câu chuyện, tấm chân tình của của đói với Nhan Bác khiến tôi vừa khâm phúc vừa yêu quý. Tôi tự hỏi" tại sao yêu lại không dám bày tỏ như cô ấy? Khi yêu sao cứ phải tỏ ra đỏng đảnh, yêu sách với người yêu mà không thực lòng quan tâm đến người mình yêu như cô ấy?" Tô Cẩn dùng cái tình tuy còn con nít chưa đủ chín chắn nhưng nó rất chân thành điều đó khiến chúng ta những người đang yêu đã yêu hãy nhìn tình yêu bằng một con mắt khác đi. Tôi thích tính cách của Tô Cẩn yếu là dám hành động, đã yêu là dám dùng tâm để yêu, dùng trái tim để cảm nhận chứ không phải chỉ là những lời nói xuông sáo rỗng. Tôi rất vui khi hai người đến với nhau. Tuy vậy, Mộc Cẩn Thiên Lam đã đi quá nhanh trong câu chuyện, dù nó nhẹ nhàng nhưng vẫn chưa quấ sâu sắc để một người thích đọc tiểu thuyết có thể xem đi xem lại nhiều lần. Mong cô sẽ cố gắng hơn trong những tác phẩm khác.
3
107240
2013-05-22 16:58:43
--------------------------
67140
9881
Tôi chưa một lần mua thử bất cứ cuốn tiểu thuyết nào , hầu hết thời gian tôi đều đọc tiểu thuyết trên mang , nhưng khi vừa thấy cuốn sách này trong hội chợ hàng Việt , tôi đã không ngần ngại mua nó . Đây là một cuốn sách hay  tính cách của Tô Cẩn , sự dũng cảm trong tình yêu của cô đã làm tôi tin tưởng hơn về tình yêu . Có những lúc họ hiểu lầm nhau , để rồi bỏ qua nhau trong 6 năm dài , sau đó lại nhờ có tình yêu chân thành mà quay về với nhau . Còn về câu chuyện của Ngô Tiểu Đồng và Thẩm Gia Ngôn , về mặt cá nhân tôi nghĩ tác giả nên dành thêm một ít thời gian để nói về chuyện tình của họ , họ đã từng tổn thương nhau nhưng sau 4 năm gặp lại , họ đã chọn nắm giữ hạnh phúc cho riêng mình. 
5
109927
2013-04-04 21:19:37
--------------------------
64369
9881
Mình không phải là một người yêu thích thể loại văn học tình cảm lãng mạn, không phải là người có nhiều trải nghiệm về tình yêu. Nhưng mình vẫn chọn cuốn sách vì tiêu đề của nó rất hay và ý nghĩa, gợi lên trong mình rất nhiều cảm xúc khác nhau. Và nó đã làm mình hài lòng và yêu mến. Câu chuyện của cuốn sách này có lẽ không có nhiều điều mới và độc đáo, chỉ là một chuyện tình đơn giản giữa hai con người với tính cách khác nhau, hoàn cảnh khác nhau và suy nghĩ cũng khác nhau, . Nhưng cách tác giả diễn tả tâm lý nhân vật, khắc họa hình ảnh tính cách nhân vật, những biến chuyển nhỏ nhất của tình cảm, cùng những miêu tả rất đẹp về mối tình này, đã cuốn hút mình từ đầu đến cuối. Đọc để hiểu thêm về tình yêu, không chỉ toàn màu hồng, không phải lúc nào cũng bền chặt và khăng khít.

3
104066
2013-03-19 21:07:37
--------------------------
58199
9881
So với những quyển tiểu thuyết trước đó tớ từng đọc thì tớ không thấy nội dung quyển sách này đặc sắc mấy. Nhưng không thể không nhắc đến yếu tố hài hước mà các nhân vật xây dựng nên, đoạn đầu rất rất vui. Lúc nào tớ cũng thấy tình yêu thời sinh viên đẹp cả, lãng mạn đến từng cen-ti-mét luôn á. Nhưng phần sau thì tớ không thích mấy, không có gì mới mẻ vẫn là cái kiểu sau bao nhiêu năm những người yêu nhau sẽ quay về bên nhau. Tớ đồng ý là ai cũng muốn kết thúc có hậu nhưng không phải theo kiểu đơn giản như thế này ... 
3
16017
2013-02-03 08:37:07
--------------------------
56515
9881
Trong tình yêu, có đôi lần tôi cứ tự hỏi rằng tại sao người ta yêu nhau nhưng lại phải chia tay, tôi hiểu đó là có quá nhiều lý do tạo nên rào cản giữa họ, không thể vượt qua nên họ đành chấp nhận chia tay. Hai nhân vật chính trong truyện cũng giống như vậy, họ có những suy nghĩ riêng, những dằn vặt riêng, cứ sợ bản thân mình ko làm người mình yêu hạnh phúc, thế nên đành chấp nhận xa nhau. Chẳng hiểu sao tôi cứ không thích một cái kết cấu như vậy, với tôi, tình yêu đẹp chính là biết cách đấu tranh vì nó, gìn giữ nó chứ không phải im lặng chờ đợi hay rời xa nhau. Tình yêu chính là cố gắng để được ở bên nhau, tình yêu chính là hành động để khiến người mình yêu hạnh phúc chứ không phải chúc người mình yêu hạnh phúc trong tay kẻ khác. 
Giữa dòng đời như thế, liệu những trái tim lạc bước còn có thể tìm thấy được nhau. 
4
6502
2013-01-22 09:31:51
--------------------------
54462
9881
Mình chọn cuốn sách này là bởi vì nó đã cuốn hút mình ở cái bìa đẹp có tính nghệ thuật cùng với tiêu đề rất ý nghĩa "Hãy cứ yêu như chưa từng tổn thương". Đây sẽ là 1 cuốn sách giúp xoa dịu tâm hồn và mang lại niềm tin yêu cho những ai từng trải qua nỗi đau khổ trong tình yêu. Như người xưa từng nói, có trải qua khổ đau mới biết trân trọng niềm hạnh phúc thực sự. Chỉ cần mở rộng lòng mình đón nhận tình yêu 1 lần nữa, vết thương cũ sẽ lành và còn lại như 1 kỉ niệm đáng nhớ.
Tuy nhiên, một điểm trừ cho cuốn sách này là nhiều tình tiết trùng lắp so với các tác phẩm của tác giả khác, làm mình có cảm tưởng tiểu thuyết ngôn tình Trung Quốc bây giờ đã bão hòa, tác giả chưa có sự bức phá mới trong cách tạo ra tình huống.
3 sao là mình vote cho ý nghĩa cuốn sách!
3
33346
2013-01-09 12:33:56
--------------------------
54050
9881
Mình chắc ai cũng giống mình, lựa chọn câu chuyện này đầu tiên bởi tiêu đề của nó: Hãy cứ yêu như chưa từng tổn thương. Trong tình yêu, dù có ngọt ngào và sâu đậm đến đâu vẫn để lại cho nhau những tổn thương, có thể nhạt mà cũng có thể đậm. Chẳng mấy ai có thể không đau, không hận vì điều đó, và chẳng mấy ai có thể bỏ qua tất cả để lại yêu, yêu cuồng say như chưa từng bị tổn thương cả. Tô Cẩn và Bác Nhan là minh chứng cho điều đó. Có tổn thương, có xa nhau, nhưng vì tình yêu sâu sắc dành cho nhau, vẫn trở lại bên nhau, vẫn yêu nhau như lúc đầu tiên. Những tổn thương có lẽ chẳng thể xóa nhòa nhưng chẳng thể che lấp được một tình yêu sâu sắc. Và nếu như bạn tim thấy mình trong câu chuyện này, thì hãy lại yêu, hãy yêu như lúc đầu, như bản thân mình chưa bao giờ tổn thương vậy. ^^
5
9481
2013-01-07 00:19:38
--------------------------
50669
9881
Mình không phải là một người yêu thích thể loại văn học tình cảm lãng mạn, không phải là người có nhiều trải nghiệm về tình yêu. Nhưng mình vẫn chọn cuốn sách vì tiêu đề của nó rất hay và ý nghĩa, gợi lên trong mình rất nhiều cảm xúc khác nhau. Và nó đã làm mình hài lòng và yêu mến. Câu chuyện của cuốn sách này có lẽ không có nhiều điều mới và độc đáo, chỉ là một chuyện tình đơn giản giữa hai con người với tính cách khác nhau, hoàn cảnh khác nhau và suy nghĩ cũng khác nhau, . Nhưng cách tác giả diễn tả tâm lý nhân vật, khắc họa hình ảnh tính cách nhân vật, những biến chuyển nhỏ nhất của tình cảm, cùng những miêu tả rất đẹp về mối tình này, đã cuốn hút mình từ đầu đến cuối. Đọc để hiểu thêm về tình yêu, không chỉ toàn màu hồng, không phải lúc nào cũng bền chặt và khăng khít.
4
20073
2012-12-16 09:00:52
--------------------------
44470
9881
Rất ấn tượng với tiêu đề của quyển sách. nó làm suy nghỉ khá nhiều.có lẽ do không có nhiều từng trải trong chuyện tình yêu nên việc nhận xét về yêu tôi cũng không thật sự sành sỏi cho lắm.nhưng cũng có lẽ vì vậy mà khi tổn thương không nhất thiết đó là tình yêu mà cũng có thể là tình bạn thì người ta sẽ dễ trở nên khép mình lại với thế giới, với những người mới đi qua mình.Nếu từng ít nhất một lần giống như tôi thì bạn nên đọc quyển sách này để hiểu vì sao tình yêu là thứ được nắm giữ bằng chính con tim chân thành và cả một chút cố gắng trân trọng.Hãy thử đọc ... cảm nhận có khi một nửa của bạn đang ở rất gần bạn đấy và "Hãy cứ yêu như chưa từng tổn thương"

4
66865
2012-11-02 20:35:28
--------------------------
43134
9881
mình mua sách vì trước tiên là ấn tượng với tên truyện và bìa sách. nhưng sau khi đọc xong mới nhận thấy nội dung truyện rất hay, dù nhẹ nhàng nhưng rất triết lý. Tô Cẩn với gia cảnh tốt, có điều kiện sống thuận lợi từ bé đến lớn, chưa bao giờ muốn cái gì là không được, đã thích là phải đạt được. Nhan Bác lạnh nhạt,   trầm tĩnh, là mẫu con trai điển hình của ngôn tình hiện đại, dễ dàng bị Tô Cẩn thu hút bởi những rắc rối mà cô đem lại chưa bao giờ xuất hiện trong cuộc sống tĩnh lặng của anh. Đơn giản là có thử thách, có khó khăn, có xa nhau nhưng cuối cùng vẫn quay về với nhau, đó chính là kết cục viên mãn nhất. Truyện có những triết lý tình yêu đúng đắn, ngọt ngào làm ta phải nghiền ngẫm. Dù tình tiết truyện có nhiều lúc khúc mắc, vẫn chưa rõ ràng hết các mối quan hệ của nhân vật nhưng mình rất hài lòng và không hối hận khi đặt mua cuốn sách này!
5
58464
2012-10-18 15:56:58
--------------------------
42560
9881
Tôi đã từng đọc một câu như thế này: "Lòng tin cũng giống như cục tẩy, sau mỗi sai lầm thì sẽ lại nhỏ đi". Nhưng câu nói này có lẽ không hề đúng với câu chuyện tình yêu trong tác phẩm "Hãy Cứ Yêu Như Chưa Từng Tổn Thương". Tôi đã từng nghĩ câu chuyện tình yêu giữa Tô Cẩn và Nhan Bác chỉ có thể là trong tiểu thuyết, nhưng cũng lại có lúc thầm ước rằng trong thực tế sẽ tồn tại một tình yêu như vậy. Một tình yêu quá sức chân thành, quá sức kỳ lạ... khiến cho tôi không thể rời mắt khỏi cuốn sách cho đến khi đọc xong.
Sáu năm trôi qua, vẫn không thể khiến Tô Cẩn ngừng tin vào Nhan Bác để rồi cuối cùng nhận ra một sự thật "cuối cùng em cũng biết, cho đến lúc này em vẫn muốn tin anh". Chỉ cần chúng ta còn tin nhau, còn dốc lòng và can đảm để yêu thêm lần nữa, chắc chắn chúng ta sẽ có được hạnh phúc.
Tôi không chỉ ngưỡng mộ mối tình giữa Tô Cẩn - Nhan Bác, mà còn ngưỡng mộ tác giá Mộc Cẩn Thiên Lam, ngưỡng mộ vì cách dẫn dắt vào tác phẩm của cô, đặc biệt là cách khiến cho người đọc có cảm giác "tôi thêm có lòng tin vào tình yêu" như tôi đang có.
4
50505
2012-10-11 12:12:41
--------------------------
41549
9881
Cuốn sách với 1 tiêu đề dành cho những người đã từng tổn thương vì tình yêu, đang sống khép mình, đóng cửa trái tim vì sợ tổn thương lần nữa....đọc và mỉm cười vì biết ở đâu đó còn có người đang chờ đợi bạn.....Nếu cố chấp thì bạn sẽ mất tất cả nhưng nếu tự tin đón nhận bạn sẽ có tất cả hoặc ít nhất là thứ bạn cần. Cuốn sách này sẽ chỉ ta cách yêu dù đã tổn thương để ta nhận ra rằng tổn thương cũng không phải là hoàn toàn xấu. Đừng bao giờ nghĩ rằng chỉ có mình mình đã chịu tổn thương trong tình yêu. Có lẽ chính sự tự tin và bạo dạn của Tô Cẩn đã khiến Nhan Bác không đao gươm mà đành chịu trói. Tôi yêu quyển sách này, yêu sự dè dặt đắn đo khi chọn mua và yêu quy luật tình cảm là "Không quy luật"................!
4
52885
2012-09-30 18:29:24
--------------------------
40862
9881
Có ai đó đã từng nói rằng: Trong tình yêu, ai bắt đầu trước đó là người thua cuộc. Nhưng câu nói này có lẽ không còn đúng trong tình huống của Tô Cẩn. Gặp và thích Nhan Bác từ cái nhìn đầu tiên nhưng cô gái thông minh ấy khiến người khác phải khen ngợi vì kế hoạch tác chiến của mình. Cô có một sự tự tin hiếm có, dẫu là “cọc đi tìm trâu” nhưng chưa từng thể hiện như một kẻ yếu thế. 
 Mình tin điều này là có thật! tự tin, thông minh, sâu sắc là những thứ mà 1 cô gái hiện đại cần và  “cọc đi tìm trâu” không phải là một khái niệm mới!
Sự tự tin của Tô Cẩn không phải xuất phát từ thói ngạo mạn của một cô tiểu thư muốn gì được nấy mà có lẽ từ bản chất kiên cường, cô tin mình gieo hạt cuối cùng cũng có ngày nở hoa. Có lẽ chính sự tự tin và bạo dạn của Tô Cẩn đã khiến Nhan Bác không đao gươm mà đành chịu trói.
             Chỉ cần cố gắng thì không gì là không thể!
Nhưng cuộc đời có biết bao ngã rẽ, đôi chân bước lạc liệu trái tim có còn tìm thấy nhau?
                 Sẽ, nếu như thật sự yêu thì điều gì cũng có thể!
 Đây là một câu truyện hay, khiến mình thích từ lần đầu tiên, mình sẽ cố gắng có được nó!
3
58218
2012-09-25 13:44:36
--------------------------
40722
9881
Bạn nên nhớ,vết thương nào không hạ gục được ta,nó sẽ làm ta trờ nên mạnh mẽ hơn.Hãy luôn cho trái tim mình một cơ hội mở cửa đón nhận yêu thương.Nếu Tô Cẩn và Nhan Bác không mở lòng mình.Chắc có lẽ họ đã lạc mất nhau mãi mãi trong cuộc đời.Bạn cũng vậy,cơ hội nào cũng chỉ đi qua đời ta một lần,và khi đã vuột mất sẽ không bao giờ tìm lại được nữa.Đừng bao giờ nghĩ rằng chỉ có mình mình đã chịu tổn thương trong tình yêu.Đó thật sự là một suy nghĩ sai lầm.Bất cứ ai cũng từng chịu những vết cứa,nhưng sau khi giúp con tim mình lành lại,họ sẽ yêu hết mình và xem vết thương cũ là một bài học đáng giá giúp họ tìm được hạnh phúc thật sự
5
12103
2012-09-23 22:34:01
--------------------------
40049
9881
vết thương dẫu lành thì cũng để lại sẹo, trái tim một khi đã tổn thương thì khó lành và lại thêm "phản ứng phụ" với nhân tố gây ra vết thương. Tổn thương vì tình yêu thì đau thật và trái tim lại có xu hướng "đóng băng" dày hơn, lẩn trốn sâu hơn, ích kỉ hơn. Với trái tim như vậy thì làm sao mà yêu thương, không yêu thương thì làm sao hạnh phúc? cuốn sách có thể là liều thuốc hữu hiệu với những ai đã và đang bị tổn thương. hãy dũng cảm yêu như chưa từng tổn thương để tinh yêu luôn như lần đầu tiên, luôn ngọt ngào, luôn rộng mở và ấm áp. chỉ có yêu thương mới làm lành tổn thương. và cuốn sách này sẽ chỉ ta cách yêu dù đã tổn thương để ta nhận ra rằng tổn thương cũng không phải là hoàn toàn xấu. hãy xem như đó là một cách rèn luyện sự kiên cường và hãy xem như là sư trả giá để nhận được yêu thương lớn hơn vì cuộc sống có luật nhân-quả!
4
45942
2012-09-18 02:49:25
--------------------------
39403
9881
Tên của quyển sách này khiền người ta phải đắn đo khi lựa chọn mua nó. Thật sự trong tình yêu sự ngạo mạn không bao giờ mang lại kết quả tốt đẹp nhất nếu họ không nhận ra nó, đây là 1 ví dụ.  Sự tự ti trong tình cảm bao giờ cũng có cái hậu quả của nó, quy luật là vậy và câu chuyện này là 1 quy luật. Người ta nói rằng :"Trong tình cảm thì không có gì là không thể chỉ có người không thể là cái gì." Nếu cố chấp thì bạn sẽ mất tất cả nhưng nếu tự tin đón nhận bạn sẽ có tất cả hoặc ít nhất là thứ bạn cần. 
Tôi yêu quyển sách này, yêu sự dè dặt đắn đo khi chọn mua và yêu quy luật tình cảm là "Không quy luật".
Chân thành chúc phúc đến những người đã hoặc đang yêu và gửi một lời chúc mừng rằng họ đã nếm được mùi vị của 1 phần cuộc đời !
4
55514
2012-09-11 19:56:42
--------------------------
39357
9881
khi tổn thương chúng ta thường sống thu hẹp lại...sống dè dặt hơn với tình yêu...trong tâm hồn và trái tim luôn sợ 1 điều gì đó....một nỗi ám ảnh của quá khứ áp đặt lên tương lai, vì vậy chúng ta thường che giấu tình cảm và quên rằng con người luôn cần có tình yêu thương.....nhưng khi 1 cánh cửa này đóng lại sẽ có 1 cánh cửa khác mở ra...khi mất đi 1 điều gì đó thì sẽ luôn có điều khác bù đắp cho bạn....nếu có 1 người làm bạn khóc thì ở đâu đó sẽ có 1 người làm bạn cười....nên hãy yêu như chưa từng tổn thương...hãy mỉm cười với người luôn làm bạn cười hạnh phúc.....
Cuốn sách với 1 tiêu đề dành cho những người đã từng tổn thương vì tình yêu, đang sống khép mình, đóng cửa trái tim vì sợ tổn thương lần nữa....đọc và mỉm cười vì biết ở đâu đó còn có người đang chờ đợi bạn.....
4
47370
2012-09-11 10:29:02
--------------------------
