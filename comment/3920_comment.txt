323586
3920
Lần đầu biết đến tác phẩm Biên bản chiến tranh qua mỗi buổi Đọc truyện đêm khuya trên Đài Tiếng nói Việt nam. Từ đấy luôn mong chờ mỗi đêm để được nghe tác phẩm này. Tác phẩm đã tái hiện một giai đoạn lịch sử hào hùng của lịch sử nước nhà. Những biến cố dồn dập dẫn đến sự sụp đổ của Việt nam Cộng hòa, những toan tính vô vọng để hòng ngăn sự sụp đổ đó. Tác phẩm cũng cho chúng ta biết thêm thông tin về những người của phía bên kia, những người có liên hệ trực tiếp tới những biến cố thời điểm đó. Một tác phẩm đầy ắp tư liệu, rất tuyệt vời để đọc.
5
809033
2015-10-19 10:46:58
--------------------------
296631
3920
Đã từng nghe câu chuyện này trên đài tiếng nói Việt Nam - mục đọc truyện đêm khuya. Tâm trạng lúc đó rất háo hức vì tác giả đã cho chúng ta một góc nhìn rất khác về cuộc chiến. Lúc đó sách chưa xuất bản, mãi đến hôm nay tôi mới có dịp cầm trên tay quyển sách này. 
Một bức tranh sống động về những ngày tháng tư lịch sử. Giúp chúng ta thêm một góc nhìn khác về những con người đã thuộc về lịch sử. Đọc quyển sách này chúng ta có thể hiểu được bối cảnh lịch sử lúc đó, cảm nhận được những hơi thở gấp gáp của cuộc chiến cũng như những phản ứng tuyệt vọng để cứu lấy tình hình. 
5
123445
2015-09-11 10:42:45
--------------------------
221887
3920
Đây là cuốn sách phải nói là ít người trẻ hiện nay biết và tôi cũng chỉ biết nó qua lời giới thiệu của báo vnexpress mà thôi. Tôi đã quyết định mua, đọc để biết thêm về một giai đoạn oanh liệt và hào hùng của dân tộc và thật may mắn, cuốn. Sách đã không làm tôi thất vọng. Đọc xong cuốn sách bạn đọc đã có thể hình dung ra một khung cảnh miền Nam trước năm 1975 ồn ào và xô bồ với muôn ngàn khuôn mặt các chính khách của chế độ cũ, nhừng góc khuất, những tâm tư,  những toan tính, những biến động của lịch sử. Mặc dù là tiểu thuyết, nhưng với kho tư liệu đồ sộ mà tác giả có được, việc tái tạo nên một bức tranh toàn cảnh về chế độ ngụy Sài gòn trước giờ sụp đổ thật sống động và giúp chúng ta có góc nhìn toàn cảnh hơn về cuộc đấu tranh giải phóng dân tộc hào hùng. Một cuốn sách đáng đọc.
5
352314
2015-07-04 16:26:28
--------------------------
196513
3920
Nhân dịp 30-4, tôi đã đặt mua cuốn sách qua lời giới thiệu trên đài báo. Thực sự, đây là một tác phẩm tuyệt vời: là một cuốn tiểu thuyết, nhưng đây là tiểu thuyết lịch sử, mọi chi tiết được viết ra đều đi kèm dẫn chứng, bằng chứng có giá trị đến từ không chỉ ở trong nước mà còn từ phía Hoa Kỳ, từ những tài liệu của VNCH mà tác giả có dịp may mắn tiếp xúc. Từ đó dựng nên một bức tranh lịch sử chân thực, sinh động ở phía bên kia, từ đó thấy được sự sụp đổ tất yếu của VNCH. Nhà báo Trần Mai Hạnh là một trong những người đã có mặt trong thời khắc lịch sử 30-4 năm ấy. Cuốn sách hay ở chỗ nó không phê phán, lên án, chỉ trích người bại trận, hay nâng cao giá trị của kẻ thắng trận, nó chỉ mô tả sự thật hiển nhiên phơi bày trước mắt, không ý kiến riêng. 40 năm trôi qua đất nước đã thống nhất, sông núi nước non có thể liền lại ngay, nhưng lòng người có thể không như vậy, Biên Bản Chiến Tranh 1 2 3 4-75 cho ta nhiều điểm nhìn mới qua sự thật được công bố, làm sao để lịch sử không đè nặng vào tương lai, để mọi người cùng chung một lòng nhìn nhận lịch sử như bài học đã đi qua để tiến lên và đoàn kết.
5
170179
2015-05-15 23:39:58
--------------------------
