39556
10350
Đất nước ngày càng phát triển, hội nhập với thế giới. Là một người trẻ, tôi cảm thấy bản thân mình cũng bị ảnh hưởng rất nhiều từ xu hướng thời đại. Cũng như tôi,nhiều bạn trẻ tiếp thu với những sản phẩm công nghệ mới rất nhanh. Nhưng chính văn hóa, lịch sử lâu đời của dân tộc mình lại không hiểu rõ hay quên lãng đi ít nhiều. Tự cảm thấy bản thân mình cần tìm hiểu nhiều hơn về lịch sử nước nhà. Phải hiểu rõ văn hóa nước mình mới có thể tìm hiểu các nền văn hóa khác trên thế giới. Càng đi sâu tìm hiểu, càng cảm thấy kiến thức bản thân ít ỏi, lịch sử Việt Nam thật phong phú. Tôi tự hào về đất nước Việt Nam giàu truyền thống, thực sự rất hạnh phúc vì là một người Viêt Nam. Những cuốn sách viết về lịch sử, về những điển tích , điển cố, giai thoại, các nét truyền thống của dân tộc như thế này thực sự rất bổ ích, đã giúp tôi cũng như nhiều người trẻ Việt khác hiểu rõ hơn về lịch sử dân tộc mình.
4
10984
2012-09-13 15:24:13
--------------------------
