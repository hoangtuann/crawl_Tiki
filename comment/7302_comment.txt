108172
7302
Những mẫu chuyện nhỏ do Phạm Vũ Ngọc Nga viết đã truyền tải đến tôi một cảm xúc vui có, buồn có, yêu có.....tôi yêu những nhân vật trong mỗi câu chuyện. Đó là những chuyện về tình bạn thuở thơ ấu, gợi một phần ký ức trong tôi trỗi dậy - thời "trẻ trâu", ham chơi. Lối kể chuyện dí dỏm, những tình tiết gây cười, gây xúc động. Bên cạnh đó, sự thành công của cuốn sách này đó là hình ảnh minh họa của Nguyễn Thanh Nhàn. Phải nói vừa đọc truyện vừa xem hình, vừa tưởng tượng ra....rất dễ thương!
4
277885
2014-03-15 18:16:45
--------------------------
