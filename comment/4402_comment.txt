340931
4402
Nếu người xưa dạy con bằng cách "Thương cho roi cho vọt, ghét cho ngọt cho bùi" thì trong thế giới ngày nay, điều đó được cho là phản giáo dục.
Không ai, không người nào có thể xúc phạm đến danh dự, tinh thần, thể xác của một đứa trẻ dù cho nó có là con mình.
Vậy, phải làm sao những người cha người mẹ hay người thầy người cô giáo của những thập niên trước đây có thể quay 180 độ để thây đổi tư dạy trẻ? Hẳn nhiên sẽ là một câu hỏi quá khó chứ không còn là khó nữa. Thay đổi tư duy, thay đổi tính nết một con người không dễ dàng, mà đi đòi hỏi các bậc "trưởng lão " gần như là không tưởng. Nhưng qua cẩm nang này, những ai muốn điều tốt đẹp đến với bọn trẻ sẽ nhận được câu trả lời thích đáng nhất có thể.

Bên cạnh đó, sự thay đổi công nghệ, đặc biệt là internet và gần đây là facebook luôn là mối bận tâm hàng đầu của thầy cô giáo và cha mẹ những đứa trẻ. Chứng nghiện mạng xã hội và gây nên nhiều hệ lũy khôn lường ở bọn trẻ thường xuất phát từ lối tiếp cận giáo dục áp đặt, cấm đoán. Theo tác giả, bậc làm cha làm mẹ cần phải có niềm tin vào con cái mình, hãy cùng con cái thảo ra nguyên tắc và cùng chúng thực hiện. Tin vào con thì con sẽ thấy trách nhiệm hơn, ý thức hơn và trưởng thành hơn.

Ngoài ra, những câu chuyện về các ca tư vấn tâm tại trường trung học cũng là bài học quý cho giáo viên và phụ huynh tham khảo nhằm cởi mở hơn với con cái trong lối sống hiện đại, bao dung hơn với những sai lỗi của trẻ.
3
317483
2015-11-20 21:00:37
--------------------------
