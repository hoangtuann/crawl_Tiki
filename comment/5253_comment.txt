572809
5253
Tôi đã đọc  QS này trong vòng 1 tuần, dù thời gian không có nhiều thời gian như vậy nhưng khi cầm lên những hình ảnh về cuộc đời 1 con người hiện ra trước mắt. 
Những con người đầy sự bất ngờ, lạc quan và luôn cho nhau những giá trị.
1 ông già 70 tuổi và 1 ca ghép tim ko thể tin được, 1 ý chí và 1 niềm tin chưa bao giờ cạn.
Tiếc là phải chi tôi được biết QS này sớm một tí. Amazing.
5
547897
2017-04-14 09:51:23
--------------------------
483871
5253
Cuốn sách kể về cuộc đời của một con người vĩ đại và một tình bạn tuyệt vời giữa Ngài Rich DeVos và Ngài Jay van Andel.
Cầm cuốn sách trên tay, đọc hết chương 1, chương 2 và rồi thật sự không muốn bỏ cuốn sách xuống. Câu chuyện về cuộc đời người sáng lập ra một tập đoàn dẫn đầu thế giới trong ngành kinh doanh tiếp thị trực tiếp thật sự rất lôi cuốn và hấp dẫn. Những biến cố, những khó khăn mà ông đã phải trải qua để xây dựng được tập đoàn như ngày hôm nay thật quá lớn lao.
Tóm lại, cuốn sách này thật sự là một cuốn sách tuyệt vời nhất trong tủ sách của mình.
5
422969
2016-09-18 07:31:23
--------------------------
478934
5253
Khoảng nửa năm trc đây mình đã được một người quen giới thiệu về cuốn sách này, người đó nói là cuốn sách này có thể cho mình thấy được tương lai của bản thân. Bản thân mình trc giờ rất làm biếng, nhất là về đọc sách, lời người bạn đó của mình mình cũng ko quan tâm lắm. Cho đến gần đây mình đang gặp 1 vấn đề rất lớn, và người bạn đó lại tiếp tục kêu mình đọc lại cuốn sách này. Mình chỉ mới đọc đc khoảng mấy ngày thôi, nhưng ko hiểu sao khi mình đọc cảm giác rất thân thuộc, như mình đc sống trong cuộc đời ngài Rich Devos vậy. Mình thấy đc hình ảnh của Rich và Jay, mình thấy đc điều kì diệu từ 1 chai loc, mình thấy đc khát vọng và tình yêu cháy bỏng của họ vào con người. Thật tuyệt vời!!!!!! 
5
1060371
2016-08-06 04:56:54
--------------------------
437287
5253
Rất hay và ý nghĩa, giàu tính nhân Văn. Là nguồn cảm hứng, động lực cho tinh thần Khởi nghiệp. Là người sáng lập ra một công ty tuyệt vời. Đem một cơ hội công bằng, bình đẳng đến cho tất cả mọi người trên thế giới này. Một doanh nhân tuyệt vời với tầm nhìn xa. Cuốn sách quá hay cho những người muốn  nghiên cứu về lịch sử của một công ty tiên phong cho Xuan thế của thế kỷ hai mốt. Đọc và suy ngẫm một cuốn sách tốt cũng giống như đi cả đọan đường. Nice
5
1390388
2016-05-27 23:01:43
--------------------------
378390
5253
Cuốn sách "Hồi Ký Rich Devos - Con Đường Tỷ Phú" về hình thức rất đẹp, bìa sách dày và sách tuy dày nhiều trang nhưng nhờ chất lượng giấy tốt nên cầm rất nhẹ, khác hẵn với những cuốn sách dày khác mà tôi đang có.
Về nội dung, ngay trang đầu tiên đã cho tôi 1 tình cảm tốt với Rich Devos, ông đã cám ơn người vợ mình góp phần làm nên thành công trong sự nghiệp của ông. Con đường tỷ phú của cuộc đời Rich Devos thật sự đầy gian nan suy tính và cũng cần 1 nghị lực phi thường mà ông đã có giúp  người đọc sẽ có thêm những bài học vô cùng bổ ích và có thêm nghị lực phi thường để thực hiện vươn lên dù có khó khăn và thử thách trong cuộc sống.
5
358906
2016-02-05 12:31:43
--------------------------
298048
5253
Cuốn sách này không phải là một tập nhật ký thông thường, cũng không phải là tự truyện, càng không phải là một tác phẩm. Cuốn sách này giống như một tài liệu tham khảo hữu ích cho những ai đang ấp ủ giấc mơ lớn vậy. Nhưng cuốn sách lại có văn phong rất gần gũi, nhẹ nhàng như lối kể chuyện thông thường. Câu chuyện trở thành một tỷ phú trên thế giới của Rich DeVos thật sự đáng suy ngẫm và học hỏi. Thêm một điểm cộng là nhà làm sách đã có thiết kế hình thức sách rất đẹp, sang trọng và cuốn hút. 
5
531312
2015-09-12 13:21:34
--------------------------
244985
5253
Nhiều người rất muốn thành công nhưng sự thật thì nó rất là đau đớn, áp lực. Đặc biệt là sáng tạo ra một ngành kinh doanh mới sẽ gặp phải rất nhiều ý kiến tiêu cực, nhưng Rich DeVos vẫn vững tin vào kinh doanh của mình và niềm tin ấy đã giúp ông thành công trên con đường giúp đỡ mọi người trên thế giới. Chẳng những thế, với vô vàn khó khăn và thử thách đến với mình, nghị lực vượt qua khó khăn mãnh liệt của nhà đồng sáng lập tập đoàn Amway thật đáng để chúng ta ngưỡng mộ! Trở thành một tỷ phú trên thế giới hoàn toàn xứng đáng với Rich DeVos và sự nghiệp ấy chắc chắn sẽ thay đổi  thế giới của rất rất nhiều người.
5
141197
2015-07-28 17:03:25
--------------------------
219749
5253
cuốn sách này rất hay và rất tuyệt vời nói về chặng đường thành thành công của Rich Devos đã trãi qua rất nhiều khó nhưng sau những khó khăn đó lại là những cơ hội tìm ẩn và rất tuyệt vời. Rich Devos và người bạn của ông đã thành lập nên một tập đoàn vô cùng lớn mạnh như ngày hôm nay. từ đó rút ra được một bài học rằng mọi khó khăn gian khổ trong cuộc sống thường ẩn chứa một cơ hội tìm ẩn sau lưng nó vì vậy chúng ta đừng nản chí và tiếp tục hoàn thành tốt và vượt qua những khó khăn thử thách của mình và sau đó nhận được những phần thưởng xứng đáng với những công sức mình đã bỏ ra
5
171796
2015-07-01 21:47:58
--------------------------
161776
5253
Cuốn sách này là cả một chặng đường dài của tỷ phú Rich Devos. Thành công nào cũng cần có khó khăn và ẩn sau một khó khăn luôn luôn là một cơ hội. Mỗi chương kết thúc luôn chứa đựng những điều ý nghĩa trong đó. Không có một con đường  thành công nào trải đầy hoa hông cả, cái gì cũng có khó khăn của nó quan trọng là ta đối mặt và giải quyết khó khăn đó như thế nào. Niềm tin vào một tương lai tươi sáng đó là điều không thể thiếu cho những nhà lãnh đạo tài năng muốn thành công.
5
100658
2015-02-28 21:55:26
--------------------------
159558
5253
Mặc dù cái tên Amway gây cho tôi một ấn tượng không tốt ở Việt Nam nhưng sau khi đọc hồi ký của ông chủ, đồng thời cũng là nhà sáng lập tập đoàn Rich Devos, tôi có lẽ nên có cái nhìn khác. Cuộc đời của Rich là tấm gương tuyệt vời cho những doanh nhân đang mang trong mình khát vọng khởi nghiệp và đang run sợ trước những khó khăn, chông gai trên đường. Kinh doanh trên một đất nước tự do về kinh tế, một thị trường cực kỳ khốc liệt, ông đã xây dựng vào phát triển Amway thành tập đoàn hàng đầu thế giới. Quyển hồi ký này chứa đựng những điều mà ông muốn chia sẻ với tất cả mọi người về hành trình tới thành công của mình.
5
387632
2015-02-17 17:31:59
--------------------------
159070
5253
..."Tôi không thể" là lời khẳng định thất bại. "Tôi có thể" mới là tuyên ngôn của lòng tin và sức mạnh"....
"Khi cầm lên rồi, bạn sẽ không muốn đặt cuốn sách xuống. Một cuộc đời sống động hiện ra trước mắt với những lời khuyên, những bài học và truyền lại bao cảm hứng cho những ai đang ước mơ về một cuộc sống tốt đẹp hơn, cũng như những ai đã sẵn sàng lao động chăm chỉ để chinh phục ước nguyện đó."
Quả đúng như vậy. Dù có rất nhiều việc để làm nhưng khi bạn cầm cuốn sách này trên tay, bạn sẽ không thể nào bỏ nó xuống mà làm việc khác. Cuốn sách này có một sức cuốn hút lạ lùng đối với mình, và mình nghĩ mọi người cũng sẽ như vậy nếu đã đọc ít nhất một trang của cuốn sách.
Khi mình mua sách vào ngày 26/1/2015, đó cũng là lần đầu tiên mình biết nó, lúc đó chỉ mua với giá là 69.000 đồng, trong khi giá bìa nó là 158.000 đồng. Vì vậy nên mình đã gọi điện cho tiki và đặt hàng lại, dù đã đặt hàng.
Rồi sau đó, được nghe bạn nói là một trong những nhà phân phối của Amway, mình mới bật mình lại và lục lại đống sách hôm trước đã mua về. 
Tuy vậy, cái quan trọng không chỉ ở đó mà ở bản chất, những kinh nghiệm truyền lại cảm hứng từ Rich Devos, khá có ích cho những ai có ước mơ về một cuộc sống tốt đẹp. 
Xét về bề ngoài, sách có bìa cứng, khá đẹp, cỡ chữ là loại cỡ chữ mà mình thích, nhìn khá rõ. Ở bìa trước, cái chữ "Con Đường Tỷ Phú" được viết chữ nổi, sờ tay vào có cảm giác khác lạ, rất thú vị.Ngoài ra, sách còn có một cái dây vàng để đánh dấu những trang mình đã đọc qua, khá tiện ích.
Sau khi đọc cuốn sách này, mình nên suy nghĩ lại về loại hình BÁN HÀNG ĐA CẤP, không hẳn nó lúc nào cũng xấu, cũng lừa đảo và mình cũng mong muốn những ai có đam mê về bán hàng và muốn thay đổi cuộc đời mình thì nên đọc cuốn sách này. .
5
499844
2015-02-14 20:25:10
--------------------------
156822
5253
Ở VN khi nghe đến đa cấp là thấy sợ, AmwayVN cũng không ngoại lệ. mình cũng từng tìm hiểu học hỏi amway một thời gian và thấy nó như một tín ngưỡng tôn giáo vậy, mọi người trong đó tin tưởng và nồng nhiệt một cách thái quá... Thôi không nói đến amway nữa, về cuốn sách này mình thấy trên fahasha nhưng thấy chữ amway thì hết muốn mua, nhưng khi về nhà lên mạng đọc bản trích thì thấy khá thú vị, nhân dịp Tiki giảm giá thì mình đã tậu về được. Một hành trình của rich devos và những bài học đã cho mình thêm niềm tin và những kiến thức vô cùng quý giá mà mình rút ra được. Giờ thì mình đã hiểu vì sao first news lại ưu ái trau chuốt bề ngoài của quyển sách này như vậy. Một quyền sách đẹp.
5
74132
2015-02-06 15:38:30
--------------------------
142796
5253
Quyển sách đẹp về hình thức và sâu sắc về nội dung.

Khi cầm trên tay quyển sách, người đọc sẽ cảm thấy trân trọng và nâng niu bởi khổ sách to và bìa cứng dày khắc chữ nổi.
 
Tôi đã yêu thích tác giả Rich Devos từ hai quyển sách trước đã xuất bản ở Việt Nam và cũng được bạn bè giới thiệu quyển sách nổi tiếng khác của Rich Devos là Simply Rich (chính là bản tiếng Anh của "Con đường tỷ phú") nhưng chưa có cơ hội sở hữu. Qua từng trang sách, người đọc sẽ được  chiêm nghiệm một cuộc đời đầy sống động với ước mơ xây dựng một công việc kinh doanh có thể giúp tất cả mọi con người trên thế giới có cuộc sống tốt đẹp hơn, và tập đoàn bán hàng trực tiếp lớn nhất - Amway cũng ra đời từ ước mơ đó!

Qua cuộc đời của Rich Devos, hẳn người đọc sẽ có thêm những bài học vô cùng bổ ích và có thêm nghị lực phi thường để thực hiện được ước mơ của bản thân và vươn lên mọi khắc nghiệt trong cuộc sống.

Vô cùng yêu mến và cảm phục ngài Rich Devos vì đã cho đi câu chuyện của cuộc đời mình!
5
138751
2014-12-19 22:45:52
--------------------------
