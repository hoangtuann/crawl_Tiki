405331
4073
Vừa nhận sách là mình lấy ra xem ngay.
Bìa sách màu hồng thật đáng yêu với hình cô bé chải tóc ra vẻ dịu dàng lắm!
Cuốn sách thật hay và bổ ích đối với mình, cho mình hiểu biết thêm về cách ứng xử với mọi người xung quanh ,biết yêu thương bản thân nhiều hơn ,hiểu và thông cảm với ba mẹ hơn.Và đặc biệt quyển sách đã giúp mình vượt qua thời kì khủng hoảng tâm lý tuổi dậy thì.Cuốn sách sẽ là người bạn đồng hành với mình trong suốt khoảng thời gian này và cả lúc sau nữa . Mình đọc xong thì sẽ để lại cho em mình đọc vì nó cũng sắp đến tuổi dậy thì rồi .Cảm ơn tiki nhiều lắm !!!
5
791443
2016-03-26 16:18:23
--------------------------
290499
4073
Sách viết gần gũi với tuổi mới lớn, như một người bạn trò chuyện với bạn vậy. Sách rất thú vị, có nhiều tranh in màu nội dung vui nhộn kèm theo một số trang cho bạn có thể ghi chép tuỳ ý thích vào. Đó là cách đơn giản để bạn tự khám phá bản thân mình cũng như các bậc cha mẹ dựa vào đó có thể hiểu được tâm lý của con mình. Dựa vào đó có thể xử lý được nhiều tình huống khác nhau. Một điểm trừ là giấy hơi mỏng tý, do đó nếu bạn lật trang sách mạnh quá nó có thể rách hoặc sút gáy sách. Nên nhẹ nhàng khi đọc.
4
153585
2015-09-05 14:18:33
--------------------------
248171
4073
Mình đã mua sách này cách đây chắc khoảng 3 tháng, sau khi mình nhận được sách thì mình rất vui, sách thiết kế khá đẹp nhưng giấy có vẻ hơi mỏng, dễ rách nhưng ngược lại sách được in màu rất bắt mắt, hình ảnh sinh động. Tiếp đến là nội dung, về nội dung thì sách mô tả khá đúng về lứa tuổi dậy thì hay lứa tuổi học sinh chúng ta, tâm lí tuổi mới lớn nên chưa chững chạc, tuy nhiên sau khi đọc cuốn sách này, thì mình đã ra dáng người lớn một chút, hiểu rõ hơn về kiến thức tuổi mới lớn, biết suy nghĩ trong những hành động của mình. Nói chung sách khá hay và bổ ích, mong là sau này sẽ phát triển hơn về những loại sách giáo dục như thế này.
5
619059
2015-07-30 18:29:41
--------------------------
191235
4073
Tuổi mới lớn được xem là tuổi nổi loạn, khi mà tâm sinh lí còn chưa phát triển hoàn thiện, nửa người lớn, nửa trẻ con. Lúc ấy cơ thể chúng ta có vô vàn điều mà chúng ta cũng chẳng thể lí giải nổi. Đọc cuốn sách này cho ta thêm nhiều kiến thức bổ ích về tuổi mới lớn, hiểu rõ bản thân mình để "nổi loạn" đúng cách. Các bậc phụ huynh cũng nên đọc cuốn sách này để hiểu thêm về con cái, để làm bạn với chúng trên chặng đường phát triển và cả bề ngoài lẫn nhân cách.
Đây thực sự là một cuốn sách đáng đọc
4
606127
2015-05-01 11:16:45
--------------------------
