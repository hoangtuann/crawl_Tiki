466646
10242
Đối với bản thân mình thì cuốn sách này thật sự rất tuyệt vời. Lúc nhận hàng mình có hơi đắn đo vì sách mỏng quá, không biết nội dung có được bao nhiêu. Nhưng khi đọc xong thì mình cảm thấy cái giá 15.000Đ ( mình mua đợt giảm giá của tiki :D) là quá rẻ cho cuốn sách này. Cảm nhận đầu tiên là bìa sách rất đẹp, thiết kế đơn giản, giấy chống lóa mắt. Còn về nội dung, tác giả tập trung hướng người đọc vào việc tự "quán chiếu nội tâm để nhận rõ chân tính của mình", mà theo mình hiểu một cách đơn giản là tự lắng nghe tiếng lòng mình để hiểu được chính bản thân mình. Điều mà mình ấn tượng nhất là lối tư duy logic và giọng văn của cụ Thu Giang Nguyễn Duy Cần. Khi đọc sách, mình có cảm tưởng như đang trò chuyện gần gũi với ông bà mình vậy. Tuy nhiên các từ ngữ trong sách có thể khiến bạn khó hiểu vì cụ viết sách này lâu rồi( cụ là học giả nổi tiếng của Việt Nam vào khoảng những năm 50-60 cuả thế kỷ trước).Sách mỏng nhưng bài học có giá trị rất lớn, người đọc cần suy ngẫm và đọc lại nhiều lần may ra mới lĩnh hội được ít nhiều!
5
579087
2016-07-02 17:05:23
--------------------------
459507
10242
Cuốn sách là một góc nhìn nhận khác về cuộc sống mà chúng ta chưa từng, hoặc ít khi nghĩ đến. Cuộc sống hối hả như ngày nay, một cuốn sách như vầy là rất cần thiết. Mình đọc nhiều lần mà vẫn thấy hay, càng đọc càng học càng ra ý mới. Sách cụ Cần hay ở chỗ này. Đọc sách cần phải suy nghĩ, phản biện. Không đọc sách mà để sách suy nghĩ giúp cho mình. Một cuốn sách nên đọc và đọc nhiều lần nữa cơ, nhất là những người trẻ như mình. Sách đẹp giá tốt so với thị trường. Bản thân mình khá hài lòng !
5
106095
2016-06-26 10:10:59
--------------------------
456568
10242
Do mình rất thích cụ Thu Giang nên quyết định mua và sưu tầm đủ bộ sách của cụ do Nhà xuất bản Trẻ ấn hành. Quyển sách này tuy nhỏ, ngắn nhưng để hiểu được nó thì phải trải nghiệm rất nhiều và có kiến thức rất rộng. Quyển này có thể nói là tổng hợp lại những gì cụ đã viết từ trước, nên nếu bạn đọc không hiểu và có phần không đồng ý thì nên đọc những tác phẩm khác của cụ, như Tinh Hoa đạo học phương đông, bộ sách đạo đức kinh, Nam Hoa Kinh để đọc quyển sách này dễ thấm vào người hơn. 
5
125876
2016-06-23 15:33:46
--------------------------
455918
10242
Tủ sách của Thu Giang Nguyễn Duy Cần như một seri sách vậy. Đọc cuốn cái dũng của thánh nhân ,cái cười thánh nhân, thuật ứng xử của người xưa mà không đọc cuốn  một nghệ thuật sống này quả là thiếu sót lớn đối với tín đồ của thu giang nguyễn duy cần, cũng như những người bạn trẻ đầy nhiệt huyết hay những con người muốn thay đổi cuộc sống, cuộc đời nhìn nhận mình. Thế nên dưới danh nghĩa tín đồ của cụ Duy Cần mình khuyên các bạn hãy nên tẩu em nó về mà đọc
4
719986
2016-06-22 22:20:38
--------------------------
455081
10242
Để muốn có một đức tính, một lối sống đạo đực, hợp trượng nghĩa. Nghệ thuật sống là một lựa chọn không thể bỏ qua đâu nhé. Noa cho ta biết cách ứng xử, cách nói chuyện, cách suy nghĩ, cách đối đáp đền ơn như thế nào cho phải phép, phải đạo lý. À mà muốn toàn diện hơn ý. Thì nên mua cái dũng của thánh nhân, cái cười của thánh nhân, thuật yêu đương nữa vì cái này chỉ là một phần nhỏ trong tập sách muốn rèn luyện nhân cách, rèn luyện cách sống mà thôi
5
719986
2016-06-22 13:01:28
--------------------------
404602
10242
Theo mình thì đây cũng là một quyển sách mang tính chất khai sáng trong Tủ sách Nguyễn Duy Cần. Qua quyển sách, tác giả đã chuyển tải những triết lý cuộc sống sâu sắc dường như là chân lý cốt lõi để khơi gợi nơi lòng đọc giả tìm về cái sống của mình. Đây là quyển sách đầu tiên mình tiếp cận khi biết đến học giả Nguyễn Duy Cần. Thật may mắn là mình đã hiểu được những quan điểm, những triết lý, ngôn từ của quyển sách. Đó là căn cơ đã đủ, là hoa tới ngày để nở và là duyên nữa. Quyển sách như một cơ sở lý luận cho cuộc sống của mình. Bạn trẻ đọc quyển này mà không hiểu thì đừng bỏ nó đi nhé. Hãy vẫn để nó nơi gác sách của bạn. Rồi một lúc nào đó bạn hãy đọc lại, bạn sẽ thấy khác lần đọc trước nhiều lắm. Và cứ như thế. Nên đọc sách này trong những khi thanh vắng, yên tĩnh để vừa đọc vừa chiêm nghiệm, suy ngẫm, bạn sẽ nhận thấy những điều sâu sắc lắm.
5
197516
2016-03-25 13:58:32
--------------------------
332733
10242
Cuốn sách này đã đem đến cho tôi một cách nhìn cuộc sống mới chân thực hơn. Chỉ vỏn vẹn hơn 200 trang sách mà cô đọng được cả một nghệ thuật sống của bậc cao nhân: đó là phải vượt lên trên cái cặp mắt nhị nguyên thiện ác tốt xấu mà điều hòa hai mặt mâu thuẫn trong lòng mình, khi ấy tâm mới thanh tịnh, đi đến cái sống chí thiện của mình được. Xã hội người ta vẫn luôn đi tìm cách dạy người dạy đời nhưng chính vấn đề nội tại sâu xa của bản thân lại không giải quyết được ma vẫn loanh quanh trong cái vòng mê lầm đau khổ của bản ngã. Sách hay!
5
867590
2015-11-06 21:20:04
--------------------------
299084
10242
Các cuốn sách của cụ Thu Giang- Nguyễn Duy Cần như Cái cười của thánh nhân, cái dũng của thánh nhân, Một nghệ thuật sống, Thuật xử thế của người xưa, Thuật yêu đương có những giá  trị riêng của mình. Sách được viết súc tích, cô đọng. Lúc đầu đọc, mình phải đọc thật chậm để hiểu được ý tác giả muốn gửi gắm qua từng câu chữ. Tuy còn một số ít hạn chế do sách đã được viết khá lâu nhưng mình nghĩ cuốn sách vẫn còn nguyên giá trị cho đến hiện tại và sau này. Bìa sách đẹp, mỗi cuốn sách được có bìa khác nhau- đây là lý do mình thích mua từng cuốn chứ không mua nguyên một cuốn dày gồm nhiều cuốn gộp lại.
4
277975
2015-09-12 23:49:16
--------------------------
276438
10242
Cụ Nguyễn Duy Cần nổi tiếng về các tác phẩm học thuật, nghiên cứu viết rất nhiều sách dạy kỹ năng và nghệ thuật sống. Tác phẩm của ông ra đời những năm giữa thế kỷ thứ 20, phần nhiều về giáo dục con người, đạo học thật sự khiến tôi khâm phục về kiến thức uyên thâm và kinh nghiệm sống của ông đã để lại cho đời. Đối với tác phẩm "một nghệ thuật sống", thật sự khó có thể hiểu được nhiều ý tứ của ông chỉ sau một lần đọc, nhiều từ ngữ ngày xưa tôi phải hỏi những người lớn tuổi và tra cứu thêm mới hiểu được ý nghĩa mà Cụ muốn truyền tải. Tuy nhiên đây cũng là một tài liệu quý và đáng trân trọng.
3
310872
2015-08-23 23:42:22
--------------------------
223371
10242
Đối với tôi, những bạn nào đánh giá cao sách này và nói hay, thì tôi thật sự khâm phục…
Trong các sách nội dung ngắn của Cụ Nguyễn Duy Cần mà tôi từng đặt mua và đọc, thì cá nhân tôi nhận thấy đây là cuốn sách khó hiểu nhất và cũng có thể là cuốn sách có nội dung nhàm chán nhất mà Cụ viết. Bên cạnh, lối viết nặng tiếng hán việt, các câu châm ngôn của Cụ dẫn dắt thật sự rất khó để cảm. Cũng không thể trách được vì Cụ thuộc lớp tri thức xưa, nên ngôn ngữ rất Nho học, điều đó làm thế hệ trẻ ngày nay khó mà tiếp cận. Hoặc nếu có… cũng sẽ thấy đôi chỗ hơi nặng “giáo điều” và không phù hợp.
Đọc sách này của Cụ mà tôi có cảm tưởng như đang đọc sách Triết học, môn học mà tôi cực kỳ ớn thời sinh viên. Nếu các bạn trẻ muốn tìm hiểu sách của Cụ, tốt nhất là nên đọc “cái dũng của Thánh nhân”, tôi thấy đây là tác phẩm nhỏ của Cụ mà tôi ấn tượng và thích thú nhất!

1
285004
2015-07-06 23:02:06
--------------------------
222683
10242
Mình hơi ngu khi mua bộ sách của Cụ về chọn cuốn mỏng nhất để đọc, sau khi đọc cái Dũng của thánh nhân, mình đọc cuốn "Một nghệ thuật sống" này, chỉ vì đơn giản là nó mỏng. Và nói thật sự cuốn sách này mỏng nhưng được đề cập quá nhiều, cũng như những khái niệm và những tư tưởng quá to lớn và ngắn gọn, nếu chưa đọc những cuốn Óc Sáng Suốt hay Thuật Tư Tưởng trước thì khó mà lãnh hội. Sau khi mình đọc xong 2 cuốn đó rồi xem lại cuốn này thì mới thấy được giá trị của nó mà Cụ Cần truyền tải. Cái này Cụ chỉ dám nói là "Một nghệ thuật sống", chứ không phải "nghệ thuật sống", vì nó viết trên quan điểm của Cụ cho rằng điều đó nên như vậy nhưng không phải khuyên mọi người đều theo như vậy. Phải biết chắt lọc những cái giá trị cho riêng mình để viết nên nghệ thuật sống của chính bản thân chứ không phải mà bắt chước rập khuôn máy móc.
5
74132
2015-07-06 00:33:45
--------------------------
218111
10242
Với lối viết khá cổ cùng với tâm thế là một người chỉ dạy, cuốn sách không phải là một tác phẩm dễ đọc như các cuốn self-help trên thị trường. Tác giả không làm bạn của người đọc như các tác giả, diễn giả khác mà tác giả viết dưới vị thế một người thầy, một người dẫn đường. Vì thế, nếu muốn hiểu cuốn sách, cần phải tạm gác cái tôi sang một bên, để mặc cho tác giả dẫn dắt từ đầu đến cuối, thành tâm học hỏi. Có thế mới thấu được ý, cảm được tâm của tác giả. Có thể mới thu được thành tựu khi đọc cuốn này. Khi đọc xong rồi, thực sự hiểu rồi, lúc đó bạn hoàn toàn tự do để xem xét lại quan điểm của tác giả, thậm chí là phê phán. Tuy nhiên, trước khi hoàn thành tác phẩm, hãy chịu khó kìm nén cái tôi lại một chút. 
Về ngôn ngữ, có thể nói là bình thường với thời của tác giả, tuy nhiên có lẽ người đọc cũng cần một trình độ tương đối để hiểu được tất cả các ý tứ, ngôn từ của tác giả.
5
155141
2015-06-30 14:02:47
--------------------------
202328
10242
Đây không phải là quyển sách chỉ dẫn từng ly từng tí, hay chỉ ra cách nào đó để bạn có thể chạy trốn cuộc sống. Không hề, ở đây, Thu Giang chỉ đơn giản, đề xuất ra chỉ một trong vô vàn cách sống đẹp, đó là chúng ta cải tạo bản thân để nhắm đến cải tạo xã hội. Ông không khuyên nhủ chúng ta hãy vui vẻ hay quên đi lo âu một cách sáo rỗng như những câu chuyện không đầu không đuôi, ông chỉ phân tích, tạo khoảng lắng, rồi lại chia sẻ, và đúc kết. Đó là cả quá trình. Đích đến của quá trình là một tinh thần vững mạnh - đó mới chính là căn cốt của vấn đề. Chúng ta sẽ tạo ra một tâm hồn đẹp hơn, bền hơn, sinh động hơn và thú vị nhất là sẽ được độc lập hơn với hoàn cảnh xã hội. Sách trình bày đẹp, nội dung ngắn gọn, súc tích. Lời văn nhẹ nhàng, trau chuốt.
5
564333
2015-05-29 10:59:09
--------------------------
198704
10242
Một nghệ thuật sống là cuốn sách tu bổ tinh thần giúp cho con người ta vững vàng hơn phát triển hơn như một cây cổ thụ vững chắc phải có bộ rễ lan rộng và sâu và bộ rễ đây chính là sự to bổ bản thân tinh thần đạt đến tầm cao mới. Qua tác phẩm mình có thêm một góc nhìn một quan điểm mới về cách sống, lẽ sống. Có thêm tự tin để bước vào và đứng vững vàng trước sóng gió cuộc đời.
Về hình thức: chất lượng sách mới tái bản khá tốt, bìa và màu bìa đẹp, phông chữ cũng dễ đọc, giá cả sách cũng hợp lí.
Bạn nên đọc các tác phẩm của Thu Giang - Nguyễn Duy Cần các tác phẩm của ông rất hữu ích dù đã viết cách đây hơn nửa thế kỉ.
Chúc các bạn đọc sách vui vẻ!
"Đọc sách hay cũng giống như trò chuyện với các bộ óc tuyệt vời nhất của những thế kỷ đã trôi qua." - Rene Descartes
4
528389
2015-05-20 16:17:57
--------------------------
173618
10242
Sách bàn đến 1 lối sống khác, một nghệ thuật sống để con người tìm được cho mình 1 lối thoát nhẹ nhàng nhất. Cuốn sách khá mỏng nhưng khá là triết lý. Bên cạnh đó cuốn sách này khá là kén người đọc , vì nó được viết từ những năm 60 nên văn phong khá là cổ, người đời sau rất khó để hiểu những gì mà người viết gửi gắm. Những người thích đọc sách với 1 style gần gũi, hiện đại hơn thì mình nghĩ là không nên mua. Cá nhân mình thấy giấy của NXB trẻ khôngđược chất lượng lắm vì xốp quá. Nhưng bù lại, cuốn sách chỉ có 20k nên ta có thể chấp nhận được
4
114793
2015-03-26 15:02:15
--------------------------
51253
10242
Thứ nhất, cuốn sách này được thiết kế đẹp mắt, với sự trình bày rõ ràng, đơn giản, tạo nên cảm hứng cho người đọc.
Thứ hai, cuốn sách này chứa đựng rất nhiều bài học bổ ích khó có thể tìm kiếm trong một cuốn sách nào khác. Có lẽ nghệ thuật sống không phải là chủ đề mới, nhưng được tác giả khai thác ở những khía cạnh khác biệt, độc đáo, góp thêm những hiểu biết cho độc giả. Lối viết nhẹ nhàng, dễ hiểu, không đưa ra bất cứ lời giáo huấn khô khan nào mà chỉ có những sẻ chia, tâm tình, giúp người đọc khám phá được nhiều hơn về cuộc sống, con người xung quanh mình.
Đây là một cuốn sách mà ai cũng nên đọc và suy ngẫm!
4
20073
2012-12-19 12:55:51
--------------------------
