454720
3516
Quyển sách tô màu này chỉ bìa là đẹp. Bên trong rất ít trang tô, hình vẽ thì rất đơn giản, hơn nữa các hình rau củ quả lại không được giống với thực tế lắm, củ cà rốt tròn ủng như củ khoai vậy. Bé nhà mình tô rất nhanh, 1 ngày tô được đến mấy quyển. Nói chung mình cũng không ưng ý lắm so với các quyển tô màu khác mình mua cùng đợt, tuy nhiên giá cũng rẻ, lại không mất phí ship vì mình mua cùng sản phẩm khác nên mình thấy cũng tạm chấp nhận được.
3
1213403
2016-06-22 08:28:47
--------------------------
439513
3516
Sách tô màu rau củ quả là quyển sách tôi rất thích. Sách in hình rau củ quả rất đẹp, rất giống với hình ảnh các rau củ quả thực tế. Với giá 6 ngàn đồng 1 quyển tôi mua mấy quyển để cho bé tập tô làm quen với hình ảnh, tập cho bé nhận biết một số đồ vật đơn giản cũng như màu sắc. Nói chung rất hay mua sách trên tiki, giá cả lúc nào cũng được ưu đãi tốt hơn bên ngoài, lại không phải mất công ra nhà sách. Tiki lại hay có nhiều chương trình khuyến mãi sách giảm giá nhiều lại kèm quà tặng, giao hàng nhanh chóng nên rất thích. Sẽ tiếp tục ủng hộ tiki.
4
927612
2016-05-31 13:09:21
--------------------------
