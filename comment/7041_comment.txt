349530
7041
Mình đã đọc xong tập này và thấy nội dung của tập có hơi giống với một câu chuyện mà mình đã đọc. Usopp chỉ vì lời nói dối mà đã khiến dân làng không thể tin tưởng được nữa khi cậu nói sự thật. Cùng sự yêu thương Kaya mãnh liệt của Usopp, anh chàng vẫn cố gắng giải thích, muốn bảo vệ Kaya khỏi mối đe dọa và âm mưu của tên quản gia Klahadore mà trước kia là hải tặc, một người mà Kaya luôn tin tưởng, đã chuộc nhà thôi miên để giết cô.
Hơn nữa, Tiki giao hàng rất nhanh. Mình đặt mua từ thứ 6 tuần trước, trong đơn hàng ghi là khoảng thứ 5- thứ 6 tuần kế sẽ có hàng nhưng thứ 2 đã có hàng rồi :P
5
1026242
2015-12-08 17:13:28
--------------------------
274228
7041
Đây là một trong những tập truyện mà mình thích nhất, Usopp tuy hay nói dối, lúc đầu hơi yếu một chút nhưng rất tuyệt vời, dũng cảm chiến đấu để bảo vệ Kaya. Thêm vào đó thì đoạn Luffy bị thôi miên, la hét, đấm đá lung tung rất thú vị và không kém phần hài hước. Thật ra thì mình xem anime rồi mới quyết định theo bộ này. Không chỉ chứa đựng các yếu tố giải trí, bên cạnh đó One Piece cũng truyền đạt những bài học ý nghĩa và đầy tính nhân văn sâu sắc. Rất hay.
5
111007
2015-08-21 19:47:23
--------------------------
253968
7041
Đọc tập này mọi người hiểu được lý do tại sao Usopp hay nói dối như vậy, cũng xuất phát từ ý tốt và thỏa mãn ý muốn làm hải tặc của mình. Tuy rằng Usopp yếu thật nhưng yêu thương Kaya thật lòng nên rất dũng cảm chiến đấu bỏ qua nỗi sợ hãi của mình. Kaya thật đáng thương khi người quản gia mà mình tin tưởng, coi như người thân lại là một người xấu, muốn chiếm đoạt tài sản của gia đình. Việc gì liên quan đến Luffy đều thú vị, hài hước hết. Luffy bị thôi miên rồi la hét, đánh đấm loạn lên. Đọc truyện rất hay!
5
730702
2015-08-04 21:46:42
--------------------------
216879
7041
Ban đầu khi gặp nhân vật Usop tôi rất ghét cậu ấy nhưng đến tập này tôi mới biết dù con người ta có yếu đến đâu nhưng trước những người yêu thương thì họ đều ra sức bảo vệ. Hiểu hơn về hoàn cảnh của Usop, lí do tại sao cậu ấy lại hay nói dối mới cảm thấy tuy nhát gan nhưng là một con người rất tuyệt. 
Tội nhất là tiểu thư Kaya, bạn của Usop..Cô đã coi tên quản gia hải tặc là người thân bao nhiêu năm để khi sự tật sáng tỏ chắc hẳn đau đớn lắm.
Còn tên hâm Luffy, chắc chỉ có cậu ấy ngốc nhất trên đời mới dễ dàng dính bẫy thôi miên kiểu đấy nhỉ.
4
139133
2015-06-28 13:11:48
--------------------------
198588
7041
Tập này quá hay luôn, tuy hơi nguy hiểm nhưng như vậy mới thấy được sự đoàn kết trong nhóm Luffy. Có nhiều tình tiết làm mình buồn cười lắm, điển hình nhất là đoạn Luffy bị thôi miên, la hét om sòm, đánh điên cuồng luôn (bạn nào xem phim đến khúc này không cười mới là lạ). Tội cho tiểu thư Kaya, bị lừa dối suốt bao nhiêu năm trời, biết được sự thật, hẳn cô nàng phải shock lắm. Ấn tượng nhất là anh chàng Usopp, hoi yếu một chút nhưng lại rất dũng cảm, cậu bảo vệ “người mình thương” mà không ngại nguy hiểm. Thấy truyện hay và hấp dẫn lắm, bìa in cũng đẹp nữa, mình thấy hài lòng.
5
13723
2015-05-20 12:04:53
--------------------------
