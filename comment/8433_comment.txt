150747
8433
Cuốn sách cho chúng ta biết rất nhiều lợi ích cũng như công dụng va cách sử dụng rau sao cho đúng.Cuốn sách còn cho ta biết làm thế nào để chế biến cho các loai rau củ đó và dùng nó sao cho phù hơp với từng người.Nó còn giúp những bà mẹ thuyết phục con cái mình,nhất là những đứa trẻ nhỏ nó sẽ thích ăn rau.Vì rau rất tốt cho hệ tiêu hóa của tât cả chúng ta...nếu có cuốn sách trong tay tôi tin nó sẽ giúp ích rất nhiều cho các bạn và các ban sẽ yêu quý nó mỗi ngày.
4
536745
2015-01-17 16:49:23
--------------------------
135898
8433
Sách trình bày về cách sử dụng, bảo quản,...của khá nhiều loại rau củ ta dùng trong cuộc sống hằng ngày, còn có cách ăn uống của từng loại phù hợp với những tạng người nào theo quan điểm của cả đông và tây y. Bên cạnh đó, ở mỗi loại còn có giới thiệu một số bài thuốc chữa bệnh, tuy nhiên có vài bài còn phức tạp, khó sử dụng. Tôi thích nhất là các hướng dẫn làm đẹp và giảm cân với rau củ của sách. Sách in giấy tốt, có điều hình minh họa là đen trắng, nhưng nếu in màu chắc giá sẽ cao hơn nhiều. Bìa không phải là bìa gấp nên cần bọc lại nếu không muốn bị tróc góc.
3
57860
2014-11-17 21:20:21
--------------------------
