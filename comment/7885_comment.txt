527937
7885
Phải nói là Rick riordan là thần tượng của tôi. Tôi là người thích đọc truyện những thể loại như thế này. Tôi mong Tiki có thể cập nhật thêm truyện của Rick Riordan. Các tác phẩm của ông mang tính lịch sử khá cao khiến nhiều người yêu lịch sử như tôi phải tò mò về các tình tiết câu chuyện của hai anh em KANE. Câu chuyện đầy thú vị và đầy thử thách của hai anh em  nhà KANE. 
5
1948131
2017-02-19 15:29:20
--------------------------
376381
7885
Mình biết Rick Riordan qua bộ truyện Các anh hùng của đỉnh Olympus và mình thấy ông viết rất hay.Tác phầm Biên niên sử nhà Kane chỉ có ba cuốn nhưng độ hay và cuốn hút các độc giả thì không kém cạnh.Mình mới mua được mấy ngày vẫn chưa đọc nhưng tổng quan thì bìa của cuốn sách này rất đẹp, phần giới thiệu cũng rất hay và cuốn hút người mua. Bộ truyện này nói về hành trình của Carter và Sadie trong hành trình cứu thế giới. Liệu Carter và Sadie có cứu được thế giới hay ko?, gặp những khó khăn gì thì phải theo dõi truyện đọc sẽ rõ

5
900397
2016-01-30 19:34:22
--------------------------
365152
7885
Ngai vàng lửa là cuốn thứ hai trong bộ ba Biên Niên Sử nhà Kane, nơi hai anh em sinh đôi tiếp tục các chuyến phiêu lưu với độ nguy hiểm tăng dần. Có thể nói ngai vàng lửa là tập hợp của những chuyện đáng tò mò và kịch tính nhất, vì ở tập này xuất hiện những bước chuyển quan trọng, hé mở vai trò của các nhân vật quan trọng và nhất là cường độ phiêu lưu và chuỗi sự kiện đều kịch tính hơn cuốn 1.
Cuốn 2 này là một cầu nối rất hữu hiệu giữa hai cuốn 1 và 3, bạn không thể đọc cuốn 3 mà chưa đọc Ngai vàng lửa, vì các sự kiện của Ngai vàng lửa chính là đặt một bước đệm, khơi gợi nên niềm hứng thú muốn tìm đến kết cục ở độc giả, nếu không đọc Ngai vàng lửa hẳn niềm hứng thú khi đọc tập cuối sẽ vơi giảm rất nhiều.
Theo suy nghĩ của mình, Ngai vàng lửa có một điểm trội hơn hai tập 1 và 3 chính là ở yếu tố tình cảm, ở Ngai vàng lửa, tình anh em giữa Sadie và Carter đã dần khăng khít sau khi trải qua thử thách, ta chứng kiến sự trưởng thành trong suy nghĩ của hai anh em, đặc biệt trong cuốn này khắc họa tình bạn giữa Bes và hai anh em rất cảm động, và lòng quyết tâm của Carter phải cứu được Zia cũng chính là điểm nhấn cho yếu tố biết xét đoán và tài lãnh đạo bẩm sinh trong cậu. Vì bộ ba này là truyện phiêu lưu, cho nên yếu tố tình cảm thường được kể một cách dí dỏm, nhưng xét ra tập 2 này yếu tố tình cảm cảm xúc khá đậm nét và cũng khiến độc giả rất cảm động !
Cho nên, nếu đã lỡ đọc Kim tự tháp đỏ thì đừng bỏ qua Ngai vàng lửa, tin rằng cuốn sách này sẽ khiến bạn hài lòng hơn cả cuốn trước !
4
473539
2016-01-07 15:19:24
--------------------------
336089
7885
Tập 2 là phần tiếp nối hoàn hảo của kim tự tháp đỏ. Sự xuyên suốt, logic được liền mạch. Tinh thần khám phá pha lẫn hồi hộp xuyên suốt tập truyện. Trong phần hai đầy lý thú của bộ ba cuốn sách này, Carter và Sadie, những người con của Dr. Julius Kane, nhà nghiên cứu về Ai Cập xuất sắc, đã bước vào cuộc tìm kiếm Cuốn Sách của Ra ở khắp nơi trên thế giới, nhưng Ngôi Nhà Sự Sống và các vị thần quyết tâm ngăn bước họ. Rick Riordan là tác giả có sách bán chạy nhất do tờ New York Times bình chọn cho Series truyện dành cho trẻ em Percy Jackson và các vị thần trên đỉnh Olympus và Series Tiểu thuyết trinh thám dành cho người lớn Tres Navarre. Ông có 15 năm giảng dạy môn tiếng Anh và lịch sử ở các trường trung học cơ sở công và tư ở San Francisco Bay Area ở California và Texas, từng nhận giải thưởng Giáo viên Ưu tú đầu tiên của trường vào năm 2002 do Saint Mary’s Hall trao tặng. Ông hiện đang sống ở San Antonio, Texas cùng vợ và hai con trai, dành toàn bộ thời gian cho sáng tác.
4
492574
2015-11-11 21:45:43
--------------------------
312026
7885
Series này chỉ có ba tập nên độ hoành tráng không thể bằng bộ Percy Jackson, nhưng nội dung vẫn rất thu hút và gây tò mò. Ưu điểm của bác Rick là sự lồng ghép khéo léo giữa những suy nghĩ hiện đại và thần thoại cổ xưa - lấy nền là những truyền thuyết và những chuyến phiêu lưu kỳ bí. Trước đây mình chỉ biết sơ sơ về những vị thần Ai Cập, nhưng sau khi đọc bộ này thì đã biết một số chữ tượng hình và những điển tích liên quan đến nền văn hoá cổ xưa này, rất thú vị. Truyện được chia làm hai ngôi kể - của Sadie và cả Carter, tạo cảm giác tâm lý nhân vật xoay chuyển và sinh động. Tình tiết được xử lý khá tốt, mạch truyện nhanh gọn, đặc biệt là phong cách viết hài hước, dí dỏm của tác giả trong những tình huống căng thẳng đã làm mình lăn ra cười. Trong chuyến đi tìm quyển sách của thần Ra nhằm đối đầu với Set, hai anh em nhà Kane đã gặp không ít khó khăn, nhưng họ đã vượt qua nhờ tình anh em và sự kiên cường của mình. Mình cũng đặc biệt chú ý tới cặp đôi Anubis/ Walt và Sadie, phải nói là bác Rick viết mấy đoạn tình cảm cứ hài mà hay không tả được. Bìa sách cũng rất bắt mắt, chữ in rõ ràng, mình rất hài lòng.
5
198930
2015-09-20 16:45:05
--------------------------
310761
7885
Đối với những ai hâm mộ những câu chuyện phiêu liêu huyễn tưởng thì không thể bỏ qua series Biên Niên Sử Nhà Kane được! Đây là một câu chuyện hay về những chuyến phiêu liêu hấp dẫn với lối kể chuyện vô cùng liền mạch và độc đáo của tác giả! Phần hai này chủ yếu nói về tình cảm anh em giữa  Carter và Sadie trong cuộc truy tìm cuốn sách của Ra với những tình tiết xảy ra vô cùng kịch tính trên con đường nguy hiểm với âm mưu ngăn cản của các vị thần và Ngôi nhà sự sống! 
5
143101
2015-09-19 19:36:02
--------------------------
309532
7885
"Ngai vàng lửa" là một trong ba quyển sách nằm trong series Biên niên sử nhà Kane. Các tác phẩm của Rick Riordan đều rất đặc biệt, với lối kể chuyện đầy lôi cuốn của mình, Rick đã đưa người đọc trở về Ai Cập, giúp họ mở mang kiến thức cũng như tầm hiểu biết về các vị thần, các huyền thoại ở nơi đây. Về kết cuộc của bộ sách này thì cuối cùng cái thiện đã chiến thắng cái ác. Truyện bao gồm những tình tiết hồi hộp, thu hút người đọc, khiến mình không thể ngừng lại được. Nói chung đây là một bộ truyện khá xuất sắc, rất đáng để đọc.
5
115397
2015-09-19 00:21:33
--------------------------
306141
7885
Phải nói rằng mình rất thích các tác phẩm của Rick Riordan bởi vì mỗi cuốn sách của ông đều mở ra cho người đọc về 1 thế giới huyền bí, xa xưa. Và ở tập 2 trong biên niên sử nhà Kane này thì mình lại được biết thêm về các vị thần theo quan điểm của người Ai Cập cũng như những phong tục huyền bí của họ. Và ở tập 2 này thì 2 anh em phải dấn thân trên con đường nguy hiểm với hi vọng tìm được quyển sách thần ra để có thể cứu thế giới này. Các diễn biến trong câu chuyện rất hồi hộp và mình rất mong chờ kết thúc của câu chuyện trong tập cuối "Bóng rắn"
5
39383
2015-09-17 13:17:44
--------------------------
245825
7885
Trong cuốn sách đầu tiên, tôi cảm thấy có một chút xa cách giữa hai anh em Carter và Sadie, mặc dù tôi rất thích họ. Nhưng khi đi sâu vào các cuốn sách thứ hai, được biết thêm nhiều điều về họ, tôi cảm thấy gần gũi với họ hơn. Và theo mạch chuyện tiến triển, tôi nhận thấy mối quan hệ anh em giữa cả hai thật cảm động và thu hút.

Bên cạnh đó, Rick Riordan cũng thêm vào đây một số nhân vật thú vị. Ví dụ như Bes, một vị thần có quyền năng chuyên đi dọa người với sự xấu xí của mình. Và người yêu mới cho Sadie, Walt. Có một số đứa trẻ khác được Sadie và Carter huấn luyện, nhưng có vẻ chúng chỉ được ném vào câu chuyện cho đủ mà không được khai thác nhiều. Nhưng cũng uh, hy vọng rằng chúng sẽ trở nên thú vị hơn trong phần còn lại của loạt serie này.


4
223142
2015-07-29 02:04:19
--------------------------
197422
7885
Biên Niên Sử Nhà Kane gồm có 3 phần : Kim Tự Tháp Đỏ  , Ngai Vàng Lửa , Bóng Rắn ,cốt truyện liền mạch với nhau . Nói về chuyến hình trình  của Carter và Sadie trong việc tìm kiếm Cuốn Sách của Ra để ngăn chặn âm mưu của Apophis - quỷ rắn Hỗn Mang đang đe dọa sẽ nhận chìm cả thế giới vào bóng tối vĩnh hằng , đây cũng là sứ mệnh mà nhà KANE phải gánh vách trên vai cùng với đó là sự can thiệp của các pháp sư trong Ngôi Nhà Sự Sống và các vị thần có ý định thù địch. Đây là một tác phẩm hay rất xứng đáng có trong thư viện tại gia của những người yêu thích về xứ sở Ai Cập và các vị thần.
5
104474
2015-05-17 16:43:06
--------------------------
