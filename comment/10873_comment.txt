5835
10873
Với tôi thì ngay tựa đề đã cho thấy tổng quát những điều mà bạn sẽ tìm được trong quyển sách này. Với những bậc phụ huynh, nhất là những người có trẻ đang trong tuổi sắp là teen hay là teen thì những quyển sách như thế này là thứ nên đọc và tiếp nhận như một cách thay đổi trong thái độ của cha mẹ đối với con cái. 

Đấy là cái tuổi mà rất nhiều biến cố xảy ra, khi không biết cách lắng nghe và giúp trẻ nhận ra mình được yêu thương và trân trọng thì sẽ xảy ra những chuyện khó lường. Bạn cần nhớ rằng khi đọc quyển sách thế này đồng nghĩa với việc nhìn nhận trẻ một cách nghiêm túc chứ không phải cứ là trẻ nhỏ mãi nữa mà nuông chiều hay quá khắt khe. Chính quyển sách này cũng như gì bạn trải nghiệm thực tế sẽ là thứ giúp bạn trở thành một bậc phụ huynh tâm lí trong mắt trẻ sau này.
3
4717
2011-06-07 13:55:11
--------------------------
