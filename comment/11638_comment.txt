358135
11638
Quyển sách này là phần tiếp theo của quyển "Total Toeic - Basic To Intermediate Toeic Skill-Building Guidebook" của cùng tác giả. Nếu như quyển kia trình độ từ 400 - 550 thì quyển này đòi hỏi cao hơn, nằm trong trình độ từ 500 - 750. Sách cũng được chia làm 2 phần reading và listening với từng unit nhỏ, giống với cấu trúc của đề thi TOEIC. Đĩa CD phần này khó nghe hơn vì yêu cầu trình độ cao mới hiểu được hết được. Sách này nên dành cho những bạn đã nắm vững kiến thức căn bản, tương đương với trình độ Intermediate
5
297258
2015-12-24 20:52:16
--------------------------
