462579
8878
Dù có hơi máu me một chút mỗi khi Ngộ Không xuống tay đánh chết mấy con yêu quái, nhưng phải công nhận hoạ sỹ của Tây Du Kí đã khắc hoạ vô cùng sinh động và hoàng tráng những cảnh hành động giữa Ngộ Không và kẻ địch, đến cả vẻ mặt ở những cảnh xa cũng được chăm chút tỉ mỉ, vô cùng đẹp mắt! Ngoài ra ... hai vị yêu quái đại vương trong truyện cũng được vẽ rất đẹp trai, đến phút cuối bị thu phục trở thành tiểu hài tử lại có nét đáng yêu. Một điểm lạ lẫm nữa là thay vì đoạn kết trừ gian diệt bạo lòng ai phóng khoáng thì ở tập 8 thầy trò Đường Tăng đều mang tâm trạng rất tồi tệ khi biết được mấy con yêu quái này "được" bồ Bát cử xuống thử lòng thầy trò Tam Tạng. 
5
998926
2016-06-28 18:28:10
--------------------------
