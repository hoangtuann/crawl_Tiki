304299
9888
Tôi được 1 người bạn tặng cho tôi sách này khi tôi sắp làm mẹ. Nội dung sách rất hay và hữu ích cho các bà mẹ trẻ.Sách đề cập đến những kiến thức quan trọng như cách chăm sóc mẹ sau khi sinh con, cách chăm em bé....Sách cũng giải thích những sai lầm mà các cụ vẫn áp dụng cho con cái mình như: bà đẻ phải kiêng tắm, kiêng gió, kiêng đánh răng...Sách như 1 cẩm nang dành cho các  bà mẹ. Tôi đã mua  sách tặng cho em gái,hy vọng mhữn kiến thức bổ ích trong sách sẽ giúp em gái tôi không bị bỡ ngỡ khi lần đầu tiên làm mẹ
4
512701
2015-09-16 12:23:45
--------------------------
90375
9888
Quyển sách gối đầu giường cho các mẹ trẻ, hiện đại nhưng thiếu kinh nghiệm. Sách hướng dẫn mình chi tiết cách kiêng cữ, ăn uống, chăm bé..đến từng ngày luôn, ngày đầu tiên như nào, ngày thứ 2 ra sao... Có cả phần dặn dò các ông bố nữa, cũng cụ thể và tỉ mỉ không kém. Các món ăn còn có hướng dẫn cách làm kèm ảnh chụp rõ ràng, mỗi tội đây là sách dịch của Trung quốc, nên các vị thuốc nam hơi khó tìm, hay tại mình không quan tâm đến thuốc bắc với chả thuốc nam nên thấy thế không biết. Mà nhé, sách còn giải thích cạn kẽ nhưng sai lầm mà các cụ nhà mình cứ bắt gái đẻ phải kiêng. Đọc xong mà mình thấy tự tin hơn hẳn, mình thấy những cái mà các cụ bắt kiêng vô lý không chịu được, nhờ có quyển này mà mình cãi lại được đấy. Chỉ kiêng cái gì cần phải kiêng, tại sao lại kiêng thôi, chứ không kiểu kiêng gió, tắm, đánh răng...các kiểu. Giờ bé nhà mình được 2 tháng rùi, nhờ áp dụng phương pháp trong sách mà bé nhà mình được chăm rất khoa học và nhẹ nhàng, hihi! 

5
44150
2013-08-09 19:05:03
--------------------------
67671
9888
Tôi sắp làm mẹ, thời gian chờ đợi đứa con đầu lòng còn gần 2 tháng nữa chào đời luôn khiến tôi hồi hộp và lo lắng không biết mình sẽ chăm sóc bé yêu như thế nào là tốt nhất và tự chăm sóc bản thân như thế nào cho khoa học để mẹ khỏe, con khỏe, cả nhà đều vui :)
Cuốn sách này giúp tôi biết được cách ở cữ và chăm bé trong tháng đầu (tháng quan trọng nhất) sau khi sinh bé sao cho thật an toàn và hợp lý.
Cám ơn tác giả rất nhiều.
4
107946
2013-04-08 09:42:46
--------------------------
