221495
4129
Canh là một món ăn không thể thiếu đối với một bữa ăn của người Việt Nam nói chung và gia đình mình nói riêng, nấu gì thì nấu trong bữa ăn nhất định phải có 1 tô canh. Vì vậy cuốn sách này rất hữu ích đối với mình nên cho luôn 5 sao ^^.
Thiết kế bìa chưa được đẹp lắm theo quan điểm của mình. Về nội dung của sách thì mình rất thích vì trình bày dễ hiểu, có những món canh chế biến đơn giản nhưng cũng có những món canh đòi hỏi sự cầu kỳ trong các chế biến nguyên liệu và cách nấu, mình chỉ tiếc hùi hụi là không được nếm thử hết các loại canh trước khi bắt tay vào thực hành
5
572238
2015-07-03 22:38:36
--------------------------
