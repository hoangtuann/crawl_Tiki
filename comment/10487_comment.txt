434408
10487
Người tình trẻ trong Tử Cấm Thành là tiểu thuyết giả tưởng. Tiểu thuyết mô tả khá chi tiết về cuộc sống của người Âu ở triều đại cuối của Trung Hoa, bên cạnh đó là sự mô tả khá chi tiết về kinh thành Bắc Kinh. Đọc tiểu thuyết để có cái nhìn mới về triều đại phong kiến dưới con mắt của người Âu. Nội dung tiểu thuyết không quá hấp dẫn, nhưng có sự dẫn dắt nhẹ nhàng, tương đối lôi cuốn, với cái kết bất ngờ, tác phẩm sẽ để lại dư vị khó quên. Tiểu thuyết được trình bày với văn phong giản dị, đôi chỗ khá hài hước. Nói chung đây là tiểu thuyết khá hay. 
4
554079
2016-05-23 08:50:22
--------------------------
268844
10487
Đọc cuốn sách chúng ta có thể thấy được tác giả rất hâm mộ (đến mức ám ảnh) Bắc Kinh, văn hóa Trung Quốc và Tử Cấm Thành. Được trình bày dưới dạng một hồi kí của một người Âu Châu đến Tử Cấm Thành làm việc và ước mong được một lần diện kiến nhà vua dưới bối cảnh triều đình phong kiến Mãn Thanh đã đến hồi kết của nó. Không được toại nguyện nhưng ông dã cố gắng thâm nhập vào chốn thâm cung đó qua lời kể của một người thầy nước ngoài dạy tiếng Trung Hoa. Chúng ta theo dỏi những sự kiện qua tâm sự và lời kể của 2 nhân vật này. Cách hành văn không hiểu là của tác giả hay do dịch giả nhưng đôi chỗ khá khó hiểu, lủng củng. 
3
371014
2015-08-16 21:26:04
--------------------------
50679
10487
Mình rất thích cách tác giả viết nên cuốn sách này. Dưới hình thức của một cuốn nhật ký hư cấu, tác giả đã kể cho chúng ta nghe một câu chuyện lịch sử thú vị, đặc biệt, thấy được bức tranh xã hội phong kiến trong thời kỳ suy tàn. Mỗi câu văn, mỗi trang sách đều có cách diễn đạt hay, ngôn từ tinh tế và giàu cảm xúc, không khô khan mà tình cảm, nhẹ nhàng. Cuốn sách đem lại một chuyến phiêu lưu ngược thời gian, nơi lịch sử được sáng tỏ và nhìn dưới góc độ mới mẻ. Cách kể của tác giả cũng đơn giản thôi nhưng lại có một sức hút kỳ lạ, làm người đọc thích thú và không thể ngừng lại trong suốt cuộc hành trình này.
Tuy có đôi chỗ chưa hay lắm nhưng cuốn sách này cũng rất đáng đọc!
3
20073
2012-12-16 09:30:14
--------------------------
36421
10487
Lối văn viết và cách dùng từ ngữ còn mập mờ,đôi chỗ vô lí.Nội dung tạm ổn,hoàn thành và Bắc Kinh được miêu tả khá sắc nét,nhưng ý tưởng ghép giữa thực tại với những điều ghi chép trên lịch sử chưa hẳn là hay.Có thể gây khó hiểu đôi nét.Nhưng nhìn chung,tác phẩm cũng đã hoàn tất được sự thành công sơ lược cho một cuốn sách lịch sử đương thời kèm theo nhật kí.
2
41838
2012-08-10 12:40:32
--------------------------
