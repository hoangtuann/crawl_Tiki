392584
10965
Mua sách "Ý Chí Ngoài Đường Đua - Hành Trình Trở Về Từ Cõi Chết"  về vì khá nhiều người review hay về ý chí của con người khi đối mặt với những vấn đề khó khăn của cuộc sống, nhưng mình chưa có thời gian để đọc
Bây giờ vẫn gối đầu giường chưa có thời gian để đọc, nhưng thấy bìa sách hơi cũ không mới cứng lắm, có một vài vết nhũ không biết do thiết kế sách vậy hay có gì đó vô tình bị tô lên nói chung là thấy hình thức hơi bị thất vọng.
4
104932
2016-03-07 11:36:13
--------------------------
323690
10965
Tôi không biết còn ai sẽ mua cuốn sách này hay không vì bây giờ khi nhắc đến Lance Amstrong người ta sẽ không còn nhớ đó là người hùng chiến thắng ung thư, là nhà vô địch 7 lần Tour de france. Hiện giờ người ta chỉ biết đó là 1 tay nói dối vĩ đại, sử dụng doping để giành chiến thắng và cũng không lấy đó làm điều hối hận vì anh nói ai cũng dùng doping. Giờ đây có lẽ anh ta chỉ còn là 1 tấm gương đen, âu đó cũng là bài học cho bất kỳ ai đừng nên để sự hào nhoáng và vĩ đại che mờ lý trí của mình.
3
144957
2015-10-19 14:43:43
--------------------------
9006
10965
Theo cảm nhận của mình giá trị cốt lõi mà cuốn sách muốn gửi gắm đó là ý chí của con người khi đối mặt với định mệnh khắc nghiêt mình, của niềm tin và hi vọng khi ta đứng trước vực thẳm của cuộc đời, cụ thể ở đây là với căn bệnh ung thư, căn bệnh mà khi nghe đến ta sẽ nghĩ ngay đến 3 từ" Tôi chắc chắn sẽ chết sớm". Cái đáng sợ nhất của căn bệnh ung thư đó là:

1/ Nó mang đến sự đau đớn khủng khiếp và tăng cấp. Nó kéo dài, hành hạ người bị mắc bệnh về cả thể xác và tinh thần.
2/ Quá trình điều trị đòi hỏi sự kiên trì, sử dụng các phương pháp chữa trị đẩy mình đến giới hạn của sự chịu đựng. 

Chính điều này dẫn đến có những người không thể qua nổi đợt điều trị với 3 phần chủ yếu: phẫu thuật, xạ  trị, hoá trị. Amstrong có 1 tính cách rất mạnh mẽ nhưng anh vẫn là 1 con người và khi ta là con người ta có thể gục ngã khi mình không chịu đựng được nữa. Nhưng chính vào lúc ấy, tình yêu thương, niềm tin của những người thân yêu nhất đã góp phần  vào vực anh dậy: mẹ anh với đầy tình thương dành cho con, những đồng đội thân cận nhất, người bạn gái mà anh yêu mến, vị bác sĩ hiền hoà mang lại niềm tin ràng căn bệnh của anh sẽ chữa được dù thật sự nó chỉ dưới 5% cơ hội. Họ đã ở đấy bên cạnh anh, vì anh. Khi đã thoát khỏi lưỡi hái tử thần, nó mang lại cho anh 1 nhận thức mới về cuộc sống, Amstrong không còn là 1 câu trai bồng bột háo thắng ngày nào, anh trở nên dày dạn hơn, sâu sắc hơn. Điều gì mang Amstrong trở lại đường đua? Nó có phải vì anh yêu thích chạy xe đạp và ham muốn được nắm trong tay chiếc cúp Tour De France danh giá? Câu trả lời là: KHÔNG. Điều anh muốn chứng minh là anh làm được, anh không những có lại cuộc sống bình thường mà còn phải là 1 nhà vô địch, 1 con người phi thường không  khuất phục trước những nghịch cảnh hiện tại ( sau phẫu thuật thể trạng anh cực kì yếu) liên tục rèn luyện, học hỏi, lao lên phía trước. Anh không những là được mà còn làm nó xuất sắc với 1 chuỗi vô địch liên tục thiết lập nên kì tích mới của giải Tour de France (điều này chứng minh thành quả của anh không hề do sự may mắn mang lại). Điều kì diệu ấy chỉ có thể có nhờ ý chí phi thường của 1 con người và sự hỗ trợ của những người bên cạnh mình những người không được nêu tên khi ta đứng ở đỉnh thành công nhưng trong thâm tâm ta biết họ đã ở đó, hỗ trợ mình, bên cạnh mình lúc khó khăn nhất và có niềm tin cháy bỏng là mình làm được.

4
7875
2011-07-28 12:30:23
--------------------------
3502
10965
Đọc xong quyển sách này, tôi chợt nhớ lại một câu thoại trong bộ phim Võ sĩ giác đấu - Gladiator. 
Maximus đã nói rằng: "Thần Chết rồi sẽ mỉm cười với tất cả chúng ta. Tất cả những gì mà một người có thể làm, là mỉm cười chào đón y."

Đó chính là thái độ của Lance Armstrong trong cuộc chiến giành lấy lại cuộc sống, sinh mạng của mình từ căn bệnh ung thư quái ác. Một cách rất chân thực, cuốn sách đã khắc họa lại quá trình, diễn biến những cuộc đua hằng ngày của anh với Thần Chết. Những khó khăn, đau đớn, chán nản rất con người, hành trình tìm lại cuộc sống và mục tiêu của cuộc đời đã khắc họa nên một Armstrong không khuất phục. Cùng với tình mẫu tử, tình bằng hữu, và cả tình yêu của người bạn đời, anh đã tìm thấy lại cuộc sống trước căn bệnh, nhưng với một cách nhìn khác. Nếu có thể, bạn hãy tìm xem chân dung của Lance Armstrong qua ống kính của Annie Leibovitz. Bức chân dung đó, cộng với quyển sách này sẽ giúp bạn hiểu thêm về một tinh thần không bao giờ khuất phục của nhà vô địch Tour de France 7 lần liên tiếp theo một cách nhìn rất khác. 

5
399
2011-04-22 14:42:49
--------------------------
