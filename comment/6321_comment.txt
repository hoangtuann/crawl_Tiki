489686
6321
Cuốn sách là những cuộc phiêu lưu kì diệu của nhân vật George. Hài hước nhất là chú lợn Freddy chính là nhân tố làm thay đổi cuộc đời George - trở thành một cậu bé đam mê khoa học.
Nhìn tổng quan thì đây là một cuốn sách thú vị của nhà thiên văn học Stephen Hawking với con gái Lucy Hawking. Bìa sách đẹp, chất lượng in khá tốt, có hình ảnh minh hoạ. Những ảnh chụp về vũ trụ rõ nét, nhìn rất đã mắt.
Ngoài ra cuốn sách còn có 2 phần tiếp theo: Kẻ Giấu Mặt Ngoài Hành Tinh, George Và Vụ Nổ Big Bang.
4
356898
2016-11-09 12:18:27
--------------------------
407132
6321
Mình đặt mua quyển sách này cho đứa em trai ngay khi nhìn thấy cái tên tác giả. Dù là quyển sách cho thiếu nhin nhưng tri thức trong quyển sách thì ngay cả người lớn đôi khi cũng chưa biết. 
Dù là những nội dung khó nhằn nhưng dưới cách viết của Lucy & Stephen Hawking thì đơn giản và dễ hiểu. Vũ trụ là một thế giới huyền bí với bao nhiêu bí mật, khơi gợi trí tò mò, nhất là với những đứa trẻ. Quyển sách giúp trẻ em tiến gần hơn đến những khái niệm khoa học khô khan. 
5
150859
2016-03-29 15:19:34
--------------------------
397115
6321
Khi tôi cầm được cuốn sách này trên tay, tôi đã bị cuốn hút bởi bìa sách. Màu sắc nhìn rất lôi cuống người đọc. Càng lôi cuốn hơn vì tác giả của nó không ai khác là Stephen Hawking - một nhà vật lý đại tài, người tạo ra thuyết vạn vật. Cuốn sách đưa tôi vào cuộc khám phá đầy hấp dẫn của cậu bé George và những người bạn hàng xóm . Những điều về vũ trụ thần kỳ đều mở ra trong từng trang sách. Đến giữa quyển sách có kèm theo tập ảnh vũ trụ mang nhiều màu sắc huyền ảo mà chúng ta chưa từng được thấy. Đây là quyển sách không những bổ ích cho người lớn mà còn là quyển sách rất thú vị cho trẻ nhỏ .
4
1209601
2016-03-14 15:02:14
--------------------------
375238
6321
Đã từng có dịp đọc quyển sách này cách đây 5 năm nhưng bây giờ mới có dịp mua được sách. Sách do Stephen Hawking viết nên mình rất tin tưởng về chất lượng sách. Tuy bìa sách được tái bản không được đẹp như trước nhưng vẫn rất hài lòng về sách. Về hình thức: những trang màu trong sách về các hành tinh rất đẹp, giấy tốt và chữ rất dễ đọc. Nội dung: tuy viết về thiên văn học nhưng rất dễ hiểu cho những người không chuyên. Do đó, sách đã cung cấp rất nhiều thông tin về hệ mặt trời cho người đọc theo một cách dễ hiểu.
5
702566
2016-01-28 00:05:33
--------------------------
369705
6321
Sách hay, nội dung hấp dẫn minh họa đẹp, sách in cũng đẹp chất lượng giấy cũng tốt.
Không cần phải bàn cãi về nội dung khoa học trong cuốn sách này vì có sự đóng góp của nhà vật lý vĩ đại Stephen Hawking, tất cả các nội dung khoa học trong cuốn sách đều hay, dễ hiểu và kết hợp nhuần nhuyễn với nội dung câu truyện làm cho cuốn sách giúp trẻ em tìm hiểu khoa học nhưng không khô khan mà dễ hiểu, dễ nhớ giúp cho các khái niệm khoa học khó hiểu mà người lớn chưa chắc đã hiểu như hố đen dễ hiểu hơn đối với tất cả mọi người, thậm chí trẻ em.
Một cuốn sách cần thiết trong tủ sách gia đình.
5
306081
2016-01-16 11:49:25
--------------------------
335536
6321
Trẻ em thời nay có nhiều sách hay để đọc quá. Mình lớn tuổi rồi mà gặp cuốn sách này cũng mê. Con mình còn nhỏ, chưa đọc được, nên mình mua để dành sau này đọc lại cho con nghe. Sách rất hay, hấp dẫn, hình ảnh minh họa rất trung thực, rõ nét và sống động. Khi nhìn thấy sách, thấy tên tác giả và đọc lướt qua nội dung là mình đã bị cuốn hút ngay từ đầu vào rồi. Đây là một cuốn sách hay dành cho các em thiếu nhi. Sách hay đáng có trong tủ sách nhà bạn.
5
362966
2015-11-11 10:04:34
--------------------------
322372
6321
Tôi mua cuốn sách này chủ yếu vì thấy cái tên của Hawking mà tôi đang đọc 1 số tác phẩm của ông, thật bất ngờ là con gái tôi ngay khi về đã đọc rất say mê một mạch tới mức thức khuya quá giờ. Nếu không buộc cháu đi ngủ có lẽ cháu sẽ đọc một mạch.
Sau đó tôi có bảo cháu kể lại tóm tắt cho nghe và quyết định đọc thử. Một cuốn sách thú vị, đặc biệt hấp dẫn cho những người yêu thích những thứ mới lạ và những câu truyện kì lạ về vũ trụ bao la.
Tôi còn tặng cho con một người bạn và khuyên vài bạn bè mua cuốn này cho con họ đọc, tất cả đều phản hồi là các cháu rất thích và đọc say mê ngay khi bắt đầu.

5
599210
2015-10-16 09:00:13
--------------------------
300186
6321
Mình thấy dù dịch còn nhiều chỗ hơi rối, đôi chỗ sai lỗi chính tả (như khi viết về con gái chú Erik nhìn chú lợn Freddy "âu yếm" thành "âu yêm", ghi nhầm tên 1 nguyên tử hoá học thành nguyên tử hiđro) thì sách khá hay, hình minh hoạ sinh động, dễ thương, khiến người đọc có cái nhìn rõ nét về các nhân vật. Chưa kể mỗi khi chú Erik giải thích 1 khái niệm cho George thì sách lại có phần riêng giải thích chi tiết nằm ở ngay trang sau hoặc cuối chương nữa. Ở giữa sách thì là hình ảnh về vũ trụ rất đẹp :3
Nói chung đây là 1 cuốn sách hay cho bất kỳ ai mới bắt đầu yêu thiên văn học hay đơn giản là muốn tìm đọc 1 tác phẩm thiếu nhi thú vị ^O^~

4
287158
2015-09-13 19:55:29
--------------------------
277144
6321
Yêu thích và muốn tìm hiểu và thiên văn học nhưng tôi không biết phải bắt đầu từ đâu và bắt đầu như thế nào. Vô tình bắt gặp trên Tiki, Chìa Khóa Vũ Trụ Của George đã giúp tôi thỏa mãn mong ước này. Với cốt truyện là một cuộc phiêu lưu nghịch ngợm mà sáng tạo được lồng ghép các bài học vật lý vũ trụ căn bản cộng với hình minh họa màu sống động dễ hiểu . Đây xứng đáng là quyển sách nhập môn cho các tín đồ mọt sách và có đam mê về vũ trụ thiên văn như mình.
4
105202
2015-08-24 18:24:13
--------------------------
257324
6321
Nhắc đến thiên văn học thì Stephen Hawking là một cái tên không thể bỏ qua, mà nhắc đến sách thiên văn "nhập môn" dành cho trẻ em thì quyển này lại càng nên có trên tủ sách nhà bạn. Những khái niệm khoa học cực kỳ thử thách như lỗ đen vũ trụ đã được ông trình bày rất thú vị và đầy cảm hứng, khiến câu chuyện này giống như Harry Potter lọt vào không gian. Lẽ ra quyển sách này đã được 5 sao nhưng theo ý mình, trừ đi 1 sao là vì khoảng 5-6 chương đầu của quyển sách được viết khá vụn và vụng về, tương đối chênh lệch với độ hấp dẫn và thông tin của những chương còn lại. Đây thực sự là một cuộc phiêu lưu hấp dẫn vào không gian dành cho trẻ em. Sách có rất nhiều hình minh họa đẹp, cùng với các thông số, biểu đồ và đồ thị không quá chi tiết, rườm rà để giữ vững sự hài hước, nghịch ngợm và gây tò mò cho độc giả : )
4
5412
2015-08-07 16:12:06
--------------------------
251055
6321
Mình rất thích ngắm những vì sao nên muốn tìm hiểu một chút về vũ trụ và mình đã tìm đến Chìa Khóa Vũ Trụ Của George - sách được các anh chị trong group Thiên Văn Học giới thiệu là dành cho những người mới bắt đầu . Thật vậy , giọng văn nhẹ nhàng , hài hước , kiến thức được lồng ghép vào những tình huống cộng với tranh minh họa rất đẹp làm một đứa dốt đặc Vật Lí như mình cũng có thể dễ dàng hiểu được . Mình rất thích các trang màu , muốn cắt ra để dán khắp phòng nhưng sợ vậy sẽ làm sách mất đẹp :'( . Dù sao đây cũng là một quyển về khoa học RẤT ĐÁNG ĐỂ ĐỌC .
5
617834
2015-08-02 12:56:13
--------------------------
238821
6321
Đây là một cuốn sách rất hấp dẫn, lôi cuốn. Lối hành văn của tác giả vô cùng hài hước, nội dung sáng tạo Những kiến thức tưởng chừng vô cùng khô khan, "khó nuốt" trong sách giáo khoa, khi được  cho vào cuốn sách này bỗng chốc trở nên dễ hiểu, thú vị. Cuốn sách phù hợp với mọi nhóm độc giả, đặc biệt là các bạn nhỏ. Nó cung cấp những kiến thức rất cơ bản của bộ môn vật lí thiên văn, thông qua cuộc hành trình của George vào vũ trụ. Các hình minh họa rất dễ thương, đáng yêu. Mình đặc biệt thích tập tranh ở giữa truyện, với những hình ảnh đẹp mê hoặc của vũ trụ bao la, rộng lớn này. 
5
570187
2015-07-23 14:23:41
--------------------------
226289
6321
Một câu chuyện khoa học nhẹ nhàng dí dỏm nhưng không kém phần gay cấn và nghẹt thở (khi Geogre gặp nạn trên sao chổi hay khi cậu bé thăm dò nhà tiến sĩ Reeper....).Mình thích nhất ở quyển sách này là những bức ảnh minh họa chân thực các hành tinh,mặt trăng,sao... trong vũ trụ được lồng ghép theo các chương truyện,tức là người đọc có thể vừa đọc vừa cùng lúc hình dung theo gợi ý từ các bức ảnh ấy.Cá nhân mình cho rằng quyển sách này phù hợp với cả trẻ em lẫn người lớn-những ai muốn tìm hiểu kiến thức về vũ trụ,vì nó giải thích,giới thiệu,... các hiện tượng,sự vật,.. một cách đơn giản nhưng vô cùng dễ hiểu và gây sức lôi cuốn.Nó mang thông tin,hơn cả là niềm say mê đối với vũ trụ của ngài  Stephen Hawking và khao khát truyền say mê ấy cho thế hệ trẻ
5
545253
2015-07-11 21:40:37
--------------------------
219260
6321
Lôi cuốn và thú vị- Đó là lời nhận xét của bé con nhà mình. Thú thật mình chọn mua quyển này dựa vào tên của tác giả. Khoa học về vũ trụ được lồng trong 1 câu chuyện được truyền tải nhẹ nhàng kèm những trang in màu trên giấy láng rõ đẹp giúp trẻ hứng thú hơn trong quá trình đọc.Tuy nhiên sách sẽ hơi khó đọc với trẻ nhỏ hơn 12 tuổi.Sau khi đọc quyển này bạn ấy mơ có 1 cái kính viễn vọng đấy Tiki.
3
126160
2015-07-01 12:10:58
--------------------------
207157
6321
Lúc đầu mua cuốn sách chỉ vì thấy tác giả của nó là Stephen, chưa đọc sách của ông bao giờ nay lại thấy ông viết truyện cho trẻ con nên mua về đọc, hy vọng sẽ dễ hiểu hơn các loại sách cho người lớn, và đúng là thế thật. Thông qua câu chuyện của một cậu bé với một nhà nghiên cứu, tác giả đã đua ra rất nhiều thông tin khoa học nghiêm túc, có rất nhiều chi tiết mà trong các sách vật lý thông thường mình cũng chưa từng đọc qua. Cách dẫn dắt và diễn giải của tác giả rất dễ hiểu, phù hợp với một cuốn sách dành cho trẻ em nhưng vẫn đảm bảo tính nghiêm túc, tỉ mỉ của một nhà khoa học. Sách cũng có nhiều hình ảnh minh họa và chú thích giúp làm sáng tỏ hơn những vấn đề khoa học được đề cập. Tuy nhiên đây không phải là một cuốn sách để đọc qua cho vui mà phải thực sự nghiền ngẫm nó mới có thể thật sự hiểu rõ những gì tác giả thể hiện
4
507583
2015-06-11 16:18:02
--------------------------
205732
6321
Bạn không thể nào kể chuyện về vũ trụ cho con/em/ cháu của bạn với hàng lô hàng lốc những thuật ngữ chuyên ngành, những công thức, những giải thuyết khô khan được. Nhưng kỳ lạ là 2 tác giả lại làm được điều này. Tác giả lấy chuyện của trẻ em để truyền tải những gì trẻ em cần - một cách có hệ thống, tuy khái quát nhưng không hề qua loa vụn vặt. Quyển sách chính vì thế sẽ rất gần gũi với trẻ em, tạo tiền đề cho các em nghiêm túc với một lĩnh vực học thuật nào đó - có thể là vì đam mê chẳng hạn, như vậy cuộc sống các em sẽ có động lực hơn.
5
564333
2015-06-07 14:33:57
--------------------------
197149
6321
Stephen Hawking luôn là thần tượng của tôi về một vĩ nhân không chỉ xuất sắc bởi tri thức uyên bác mà còn ở cách ông thể hiện trong từng tác phẩm của mình. Chìa Khóa Vũ Trụ Của George - là cuốn sách ông viết cho thiếu nhi cùng với con gái mình, viết cho những đứa trẻ mơ mộng và ham học hỏi, những đứa trẻ bị bó buộc trong khuôn khổ quy tắc - phụ - huynh như Geogre, những đứa trẻ thích tưởng tượng và hoạt bát như Annie,... nhưng tụ chung lại đều là những đứa trẻ ham hiểu biết, thích khám phá những chân trời mới lạ.Từ ngữ được sử dụng trong sách rất dung dị, mộc mạc nhưng chứa đầy cả tình yêu thương của gia đình và tri thức vũ trụ học, hình thức trình bày rất dễ thương khi chú Eric viết tay bản thảo cuốn sách về Lỗ Đen của chú có cả phần dành cho George và Annie với những ghi chú bên lề sinh động. Bên cạnh mạch truyện còn có cả những trang ghi chú hiểu biết khái quát về vũ trụ được ghi nhận cụ thể và dễ hiểu. Thêm vào đó, Stephen Hawking còn ưu ái cung cấp cho cuốn sách phát hiện mới nhất của ông về cách thoát ra khỏi lỗ đen như thế nào thông qua hành trình giải cứu chú Eric của Geogre. Bạn đọc cũng sẽ vô cùng yêu mến Cosmos - một siêu máy tính thích trò chuyện cùng con người, nói nhiều và thân thiện. Nếu có một đứa trẻ ở nhà và muốn kích thích chúng tìm tòi thì đây là một lựa chọn tuyệt vời cho bạn.
5
364576
2015-05-17 03:41:15
--------------------------
182337
6321
Đây thực sự là một cuốn sách tuyệt vời cho những đứa trẻ ham học hỏi và tò mò muốn hiểu biết về vũ trụ bao la. Câu chuyện được dẫn dắt rất thú vị, nhẹ nhàng như chính sự quen biết tình cờ giữa Geogre và Annie. Với yếu tố kì diệu là chiếc siêu máy tính Cosmos có thể nói chuyện được và giúp mở cánh cửa đi vào vũ trụ, từng câu chuyện khám phá mạo hiểm giữa Geogre và cô bé hàng xóm Annie ươm mầm sự thích thú tìm hiểu và trí tưởng tượng cùng khả năng sáng tạo vô biên của thiếu nhi đối với thế giới xung quanh mình. Lồng ghép vào những câu chuyện còn có các bức ảnh màu về vũ trụ cùng rất nhiều những kiến thức bổ ích về thiên hà, tinh vân hay các ngôi sao trong hệ Mặt Trời... Thực sự tôi đã bị cuốn hút bởi sự kì diệu của Cosmos cùng lối viết truyện nhẹ nhàng dành cho lứa tuổi thiếu niên. Đây quả là một cuốn sách rất hay dành cho thiếu niên và đặc biệt là những em nhỏ ưa thích khám phá và tìm hiểu!
5
314122
2015-04-13 20:58:19
--------------------------
128440
6321
Đây có lẽ không phải là một câu chuyện cuốn hút , các tình tiết và nhân vật đều được dàn dựng một cách rất đơn giản, dễ hiểu nhưng như thế đã quá đủ đối với một cuốn sách dành cho thiếu nhi. 

Bởi lẽ đây là một cuốn sách thật sự thú vị và cuốn hút ! 

Điều đầu tiên cuốn hút mình ngay từ đầu có lẽ là cách trình bày sách , hình ảnh minh họa , hình ảnh kèm theo và cả font chữ nữa nhưng xuất sắc nhất là hình ảnh minh họa - sống động, thú vị , cực kì hấp dẫn. 

Stephen Hawking thật sự đã viết một câu chuyện khoa học dành cho thiếu nhi, mình cứ nghĩ rằng lượng kiến thức trong cuốn sách này có lẽ quá lớn, sẽ rất khó hiểu , nhưng mà không phải như vậy , bất kì đứa trẻ nào cũng có thể hiểu được và bất kì đứa trẻ nào cũng sẽ thích thú với quyển sách này, nào là vừa có truyện để đọc, lại thêm kiến thức được lồng vào khéo léo trong các tình tiết , nào là có chú thích rõ ràng , hình minh họa tuyệt đẹp , nó gần như khác hoàn toàn với sách giáo khoa của bọn trẻ - với đủ thứ lý thuyết khô khan , nhàm chán đến tận cổ ! Stephen hawking thật sữ đã cho chúng ta thấy vẻ đẹp của khoa học , rằng vật lý gần gũi với cuộc sống như thế nào , rằng trái đất xinh đẹp của chúng ta hình thành như thế nào , chịu hiểm họa ra sao , và cả vũ trụ ngoài kia có những gì , ông đã dần dần đưa " khoa học - vũ trụ " vào gần hơn với trẻ nhỏ , khi mà có quá ít những tài liệu thực sự bổ ích , dễ hiểu dành cho thiếu nhi , mọi thứ  phức tạp được ông viết ra trông thực sự đơn giản :) 

Quyển sách thật sự đã khơi dậy được trí tò mò , tưởng tượng ham học hỏi của trẻ nhỏ , ít nhất đối với em gái mình : con bé đã không ngừng hỏi mình  vì sao  thế này , vì sao thế kia , liệu điều đó có thật không , con bé đã kiên nhẫn tìm hiểu thêm trên mạng và cả xụ mặt xuống khi không vào được trang web " gia nhập cùng George " 

P.s : Có một số chi tiết khoa học mà ngay cả ngày nay chương trình  Phổ thông của chúng ta cũng chưa cập nhật vào :))))
4
36336
2014-10-02 01:02:29
--------------------------
