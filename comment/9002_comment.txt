470322
9002
Chỉ với quyển sách nhỏ và những câu chuyện ngắn, các bạn nhỏ đã dành thời gian vào việc đọc sách - một việc vô cùng hứng thú và bổ ích, là bước đệm để các bạn nhỏ yêu quý, trân trọng việc đọc sách về sau. Những quyển sách như vậy là món quà quý giá mà bậc phụ huynh nào cũng nên dành tặng con em mình, nhất là lứa tuổi nhi đồng. Kho tàng truyện cổ tích Việt Nam vô cùng phong phú, đa dạng, nhiều thể loại, biến thể, dạy cho các bạn nhỏ nhiều bài học bổ ích.
5
169341
2016-07-07 13:22:00
--------------------------
163157
9002
Chỉ vì ghen tuông vô lối, Vạn Lịch đã đuổi vợ ở ngay bãi biển. Trải qua nhiều biến cố, Vạn Lịch làm ăn thua lỗ còn người vợ hắn đuổi đi thuở nào trở thành quan tuần ty, đảm nhận việc thu thuế ở sông. Gặp lại vợ khiến cho Vạn Lịch vừa thẹn vừa uất nên trao lại tài sản cho vợ rồi tự tử. Những đồng tiền đó được đem đúc thành một thứ tiền gọi là Vạn Lịch. Câu chuyện là xuất xứ của đồng Vạn Lịch và qua đó đề cao đạo nghĩa vợ chồng. Truyện đơn giản, ngắn nhưng súc tích. Mình đặc biệt thích chất liệu giấy in truyện này của Đông A.
4
534227
2015-03-04 11:34:00
--------------------------
