432889
9986
Cuốn sách Học Tiếng Anh Qua Tin Tức này mình thấy khá hay, mặc dù sách tiếng Anh bây giờ bán trên thị trường giờ không thiếu. Nhưng nhiều cái thiên về và đặt nặng vào mảng ngữ pháp quá. Cuốn này mình thấy nó thiết thực và gần gũi với đời sống này, sách có 3 chủ đề chính trị, kinh tế và văn hóa. Mỗi chủ đề gồm 16 bài báo khác nhau. Mỗi bài báo đều có phần từ vựng hay cấu trúc ngữ pháp sẽ được giải thích ở sau bài đọc và phần cuối là dịch bài báo sang tiếng Việt. Mình thấy nó giúp người học có thể tìm hiểu thêm tin tức đã xảy ra trong quá khứ, cũng như giúp người học rèn luyện kỹ năng dịch thuật. Nhưng mình vẫn muốn là cuốn sách cần thêm nhiều nội dung hơn nữa !
4
1302823
2016-05-19 23:34:08
--------------------------
358517
9986
Nội dung sách phong phú khi tập hợp nhiều bài đọc tiếng Anh ở các lĩnh vực : kinh tế - chính trị-văn hóa... Tương đối nhẹ và gọn, thích hợp để bỏ túi xách mang theo đọc giải trí và rèn luyện khả năng đọc hiểu tiếng Anh. Trước mỗi bài đọc đều có phần liệt kê từ mới, tuy nhiên sẽ tốt hơn nếu bên cạnh từ mới có thêm phần phiên âm - trọng âm để người đọc phát âm đúng ngay, không phải mất công dừng lại tra cứu trên từ điển. Sách chú trọng kỹ năng đọc hiểu nhưng sẽ hoàn hảo hơn nếu có bán kèm CD nghe.    
3
289786
2015-12-25 13:47:53
--------------------------
304522
9986
Mình thấy sách anh văn dạo này khá nhiều, đủ thứ nội dung và chủ đề tùy theo nhu cầu của mỗi người.Riêng cuốn sách này mình thấy tập trung vào chủ đề thông tin qua tin tức. Cũng là một chủ đề hay cho tất cả mọi người tham khảo. Nhưng nó cũng thật sự chưa hay lắm vì sách áp dụng cách học cũ nên rất khó áp dụng, nếu sách áp dụng một số phương pháp học dễ nhớ hơn chắc sẽ có nhiều người ủng hộ hơn. Dù sao cũng cám ơn tác giả đã chia sẻ một cuốn sách khá hay.
4
225624
2015-09-16 14:24:34
--------------------------
240503
9986
Khi đọc qua cuốn sách này, mình thấy tất cả sự hiểu biết ùa về trong não của mình. Giống như là kí ức từ thuở xa xưa vậy đó. Mình rất mê học tiếng anh, tìm hết cái này qua cái nọ để có thể mà học tiếng anh một cách tốt nhất nữa chứ! Thế là mình đã quyết định xin lời tham khảo của bạn bè lẫn thầy cô, cha mẹ và anh chị em của mình về một quyển sách tiếng anh tốt nhất có thể giúp chúng ta có thêm nhiều kiến thức cho hôm nay lẫn mai sau. Sao các bạn không mua thử, hãy cảm nhận nó và xem nó là một người bạn thân của mình nha
4
544045
2015-07-24 17:26:55
--------------------------
