274812
4679
Mình vốn hâm mộ Vũ Hùng từ rất lâu vì ông là tác giả truyện thiếu nhi và đã có những quyển sách viết rất hay về rừng. Từ giữ lấy bầu mật, sao sao, sống giữa bầy voi,... Bây giờ là Mùa săn trên núi. Cuốn sách viết về một thời rất xưa nhưng cũng rất tươi đẹp của ông cha ta. Tươi đẹp ở đây chính là cuộc sống với bầu không khí và không gian núi rừng rộng lớn chưa bị thế giới hiện đại phá hoại. Tuy phải đối mặt với thú dữ, với thời tiết khắc nghiệt nhưng ông cha ta thời xưa đã sống và cho ta thế giới ngày nay đấy thôi. Quyển sách nhỏ, cau chuyện ngắn gọn nhưng sâu sắc. Bạn có thể mang quyển sách đi du lịch nhẹ nhàng. Giấy tốt, màu vàng làm mắt không bị mỏi.
5
614800
2015-08-22 11:45:09
--------------------------
