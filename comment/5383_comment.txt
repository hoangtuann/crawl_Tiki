288086
5383
Mình mua bộ sách này để dạy cho em mình gần 4 tuổi, mình thấy sách rất hay, có các bài từ cơ bản đến nâng cao, mỗi bài ngoài các bài tập viết còn có phần cho bé tô màu với nhiều hình khác nhau, có phần nối số bé rất thích. Mình vừa có thể kết hợp dạy bé phân biệt màu sác, vừa dạy bé tô chữ, viết chữ. Tuy nhiên, cả bộ chỉ có vài cuốn có phần ôn tập cuối sách, mong NXB cho phần ôn tập vào tất cả các sách để mình có thể ôn lại cho bé nhớ hơn.
5
73436
2015-09-03 11:55:59
--------------------------
