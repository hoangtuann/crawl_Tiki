210437
9850
Cuốn sách này phù hợp cho những bạn đã đọc trước truyện Doraemon, nếu chưa đọc trước mà đọc quyển này thì thôi khỏi hiểu gì luôn. Những câu đố vui cũng khá là hay, nó đố về các loại bảo bối của Doraemon.Mà mình thấy dễ quá, mình giải được hết, không cần xem kết quả ở trang phía sau luôn. Đọc quyển này cảm giác như mình đang ôn lại kỉ niệm vậy đó. Đọc là lại nhớ đến hình ảnh cậu bé Nobita hậu đậu vụng về và chú mèo máy đáng yêu Doraemon. Thấy rất là hay. Mình thích quyển sách này này lắm luôn.
4
603704
2015-06-19 14:43:54
--------------------------
71566
9850
Mê đo re mon từ nhỏ nhưng mình lại không thích thể loại truyện này của đô rê mon, bởi nếu mình thích thì mình thà mua cuốn truyện trọn bộ còn hay hơn là đọc cuốn này chỉ nói đứt đoạn về một số phần kí ức, nói thật chứ nếu mình chưa đọc trước thì coi chẳng hiểu gì cả, và không cảm thấy thú vị.
Nhưng mà, nói gì thì nói , mình vẫn rất ủng hộ hết mình truyện đo rê mon :)) tình bạn của mon với noobi ta thật dễ thương và đôi khi làm ta rất xúc động , và những món bảo bối thú vị cứ lần lượt mà xuất hiện .. thể hiện những ước mơ nhỏ nhoi dí dỏm của học trò hiện đại, thật vô dùng đáng yêu !
3
93722
2013-04-27 19:32:29
--------------------------
51431
9850
Mình rất thích đọc đôrêmon từ khi còn nhỏ đến bây giờ dù đã lớn mình vẫn còn thói quen đọc truyện này những lúc rãnh rỗi. Mình thích nôbita bởi cậu vụng về hậu đậu nhưng lại rất tốt bụng luôn sẵn sàng giúp đỡ bạn bè, còn Mon thì lúc nào cũng ủng hộ nôbita, luôn hướng nôbita siêng năng học hành hơn đừng ham chơi. Mình thích tình bạn giữa họ, chaien, xêkô và xuka. Khi có khó khăn gì thì cả nhóm đều chung tay giúp đỡ nhau để vượt qua.
Truyện đôrêmon đố vui này cũng có những thú vị riêng của nó, giúp ta ôn lại những truyện đorêmon trước đây mà không cần đọc lại, ngoài ra truyện có màu nên cũng khá đẹp. Bạn nào thích mèo Mon thì thử khám phá truyện này xem
3
60663
2012-12-20 17:47:19
--------------------------
