277108
3434
Lần đầu tiên mua một cuốn sách về sáng tạo mà không thể hàn lâm hơn được nữa. Thật sự mình rất thích đọc những cuốn sách hướng dẫn và khai mở sự sáng tạo, vì mình đang làm mảng này. Nhưng mua cuốn này về mình chỉ hy vọng rằng mình không hấp tấp mua đến vậy, vì rõ ràng kiểu cách hướng dẫn sáng tạo trong cuốn sách này quá khô cứng, dài dòng và không thể dùng cho những bạn yêu thích sáng tạo theo hướng nghệ thuật. Trong cuốn sách này, những ví dụ và những lời biện giải chỉ đơn thuần là lặp lại những thành tựu và rút ra những kết luận... không thể chung chung hơn. Và tựa đề sách với cuốn sách cũng chẳng liên quan gì, thậm chí nội dung sách còn đập tan thiện cảm với tựa đề của mình nữa :( Đây là góc nhìn từ người làm trong ngành quảng cáo, tiếp xúc với sự sáng tạo mỗi ngày nên chắc yêu cầu của mình cao hơn những gì cuốn sách này mang lại :(
2
37779
2015-08-24 17:31:45
--------------------------
233115
3434
Cho đến hiện nay thì quan điểm về sáng tạo thường được nhìn nhận theo 2 trào lưu, trào lưu 1 là trào lưu đã phổ biến chính là sáng tạo là một sự đổi mới, một sự phá cách, ngẫu nhiên không có quy luật, sáng tạo phải dựa vào các nguyên liệu đầu vào mới hơn, lạ hơn và phương pháp phổ biến là cần tạo ra thật nhiều ý tưởng sau đó mới xem xét lại như phương pháp Brainstorm. Trào lưu thứ 2 là như trong quyển sách này trình bày, sáng tạo cũng có những quy luật để sáng tạo, thực tế nếu chúng ta quan sát sự phát triển của các sản phẩm điện tử ta sẽ thấy rằng rõ ràng nó có quy luật chứ không phải là ngẫu nhiên. Và 5 quy luật trong quyển sách này theo tôi biết cũng chưa phải là toàn bộ mà còn có nhiều quy luật khác nữa như làm ngược lại, tách rời,...Nhưng tôi nghĩ tác giả chỉ trình bày 5 quy luật có lẽ vì đây là các quy luật phổ biến nhất. Bản thân tôi cho rằng không có trào lưu nào đúng trào lưu nào sai mà cả 2 trào lưu đều tạo ra các sản phẩm sáng tạo trong đời sống, tuy nhiên sáng tạo theo quy luật có lẽ chiếm đa số. Hình dung theo quy luật 80/20 thì sẽ như thế này: 80% là sáng tạo theo quy luật, 20% là đổi mới đột phá, và trong 80% sáng tạo theo quy luật thì có 80% là sáng tạo theo 5 quy luật trong sách trình bày còn 20% là các quy luật khác. Do đó quyển sách này cung cấp cho chúng ta 64% ý tưởng sáng tạo, chỉ với 1 quyển sách tôi thấy như vậy là rất tốt, xứng đáng để mua và nghiền ngẫm.
5
42985
2015-07-19 15:04:05
--------------------------
207125
3434
Quyển này mình không mua mà được đọc từ đứa bạn, lúc lại nhà nó chơi, Khi thấy tựa sách thì mình cũng không thích lắm, chỉ được cái tò mò thôi, sau khi đọc hết thì mình thấy nội dung cũng chẳng đột phá lắm nếu gọi là sách về tư duy mới. Tuy nhiên không phải sách không hay, tác giả viết rất ngắn gọn, súc tích, cũng khá nhiều ví dụ và những câu hỏi thú vị. Tuy nhiên nếu độc giả là người khắt khe, ắt hẳn sẽ nhận ra đôi chỗ còn phạm lỗi nguỵ biện. Tuy vậy, nếu bạn là người mới bắt đầu với loại sách tư duy như thế này, quyển này cũng đáng đọc. Bìa đẹp, chất lượng giấy in tốt.
3
564333
2015-06-11 15:13:56
--------------------------
205635
3434
review quyển sách này vài trang đầu thì thấy nó thật tuyệt, đặc biệt là những người làm công việc sáng tạo ngày nay, nếu không có đột phà thì sẽ tụt hậu. Quyển sách đưa người ta vào tri thức với câu văn không quá dài và dễ hiểu so với các tác phẩm tri thức khác. Đây là một quyển sách thú vị, từ cái bìa đến nội dung, thật sự bổ ích và cần thiết, dễ nhớ, dễ hiểu, ngắn gọn và xúc tích. Với tôi, đây là cuốn một ngày nào đó tôi sẽ mua và đọc nó, không khó để đưa ra quyết định này.
5
277478
2015-06-07 11:11:40
--------------------------
