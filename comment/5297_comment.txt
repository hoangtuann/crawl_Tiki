310402
5297
Đây là một cuốn sách thú vị dạy tôi những kiến thức để khám phá thế giới trong lòng đại dương. Nó giúp tôi tìm hiểu những loài sinh vật biển, sự khác nhau giữa biển và đại dương, những hiện tượng kỳ thú nơi đáy biển. Sau khi đọc xong cuốn sách này, tôi hy vọng một ngày nào đó mình được lặn xuống biển, được thám hiểm đáy đại dương, tận mắt chứng kiến những cảnh đẹp, những loài sinh vật kỳ lạ... những thứ mà tôi chỉ được biết thông qua ti vi, internet và sách vở.
4
578993
2015-09-19 15:38:15
--------------------------
289499
5297
Mình đã mua cuốn một cho bé, và thấy rất hài lòng, nên mua luôn cuốn 2 này. Bé của mình rất thích thú tìm hiểu về các bạn nhỏ ở đại dương, biết thêm được tập tính sống của các loài như cá heo, cá mập, cua, cá, mực, sứa... Bé chưa đọc được, nhưng luôn miệng hỏi mẹ "con gì vậy mẹ", và xem rất say mê. Sách dạng truyện tranh, có hình vẽ minh họa, được in màu, nôi dung thì rất đơn giản, dễ hiểu. Nên mẹ có thể cùng đọc sách, cho bé xem tranh và giải thích cho bé. 
5
476955
2015-09-04 15:32:49
--------------------------
