537129
10835
Nhìn sách mà nhớ trường ghê, quyển này mình vô tình đọc đc trong thư viện trường cấp 2. Hình ảnh minh họa sống động, cung cấp nhiều kiến thức bổ ích về ong, kiến :)
5
2006156
2017-03-07 14:23:10
--------------------------
378009
10835
Tính đoàn kết , hợp nhất mỗi khi con người phải sống trong cái sự cổ điển , nhanh chóng đối với sự khẳng khái luyện tập , tầm với cao hơn , mong mọi bí ẩn ngoài tự nhiên không làm chúng gục ngã , lùi bước , giống như sự trân trọng , cao quý , gợi ý nhớ về đời người nhầm lẫn , có lúc giống như thiếu đi sự kiên trì của toàn bộ thực tế , trở thành cái suy xét thực tại lẫn lộn cái sức mạnh mới chứng minh khoa học gần với suy nghĩ của chúng ta .
4
402468
2016-02-04 01:25:06
--------------------------
253330
10835
Con Ong Cái Kiến là quyển sách thứ ba tôi sở hữu trong Bộ Sách Giữ Gìn Thiên Nhiên , sau cuốn Côn Trùng Là Thế Đó và quyển Chuyện Ruồi Bọ . Nói về chủ đề ong và kiến thì chúng ta hay liên tưởng đến những con người chăm chỉ, các đội quân cần mẫn. Nhưng bạn có biết thế giới của chúng là một kiến trúc xã hội với các tầng lớp trong cộng đồng và vị trí của từng thành viên được tổ chức quy cũ và chặt chẽ, mọi công việc được lên kế hoạch và thực hiện chuyên môn hóa không? Ngoài ra sách còn giới thiệu về loài giun, một sinh vật nhỏ bé nhưng có vai trò vô cùng quan trọng là cân bằng sinh thái. Cùng với lượng thông tin phong phú của đề tài này, các điều thú vị của thế giới ong kiến giun này sẽ được chia sẻ cuối mỗi phần trong sách, bạn sẽ thích thú khi đọc đấy
5
544151
2015-08-04 12:58:19
--------------------------
168190
10835
Tôi mua vì giá sách khá mềm. Khi nhận được sách, tôi thật sự sự bất ngờ vì  nội dung sách rất phong phú, hình thức trình bày đẹp mắt. Hình minh họa dù không được in màu nhưng vẫn rất dễ nhìn, to, rõ và được trình bày ngẫu hứng, không gây cảm giác nhàm chán cho người đọc.  Sách không chỉ giới thiệu về một số chủng loại ong, kiến mà còn giới thiệu về các loại giun  để người đọc tham khảo. Sách còn có những mục đóng khung nho nhỏ để bật mí cho người đọc thêm những thông tin vô cùng thú vị về chủ đề chính.
5
57860
2015-03-16 09:59:30
--------------------------
