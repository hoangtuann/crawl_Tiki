303088
8552
Ban đầu mình mua vì thấy sách rẻ, hơn nữa mình cũng có hứng thú với chủ đề tìm hiểu các dân tộc, sau khi nhận được sách từ tiki và lật ra xem thử thì cảm thấy mình đã quá sáng suốt khi "rước" em nó về. Khổ sách to, in rõ nét từ bìa tới ruột, cắt giấy gọn gàng, toàn bộ sách được in màu rất đẹp. Trình bày bên trong sách rõ ràng và bắt mắt. Hình ảnh nhiều vô số (không phải kiểu hình chụp mà chỉ là hình vẽ thôi, nhưng rất là sinh động nhé). Thông tin trong sách vô cùng đa dạng, ngoại trừ các thông tin để trả lời câu hỏi cho phần mục lục thì còn có các thông tin bên lề, xoay quanh vấn đề dân tộc-đất nước (văn hoá, phong tục, con người, đặc trưng...). Bộ sách này gồm 20 cuốn và có lẽ mình sẽ sưu tầm cho bằng hết !
5
75959
2015-09-15 17:28:28
--------------------------
