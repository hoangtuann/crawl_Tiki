476816
5547
Tìm sách trên tiki tình cờ thấy quyển Giáng sinh yêu thương, đọc nhận xét của các bạn khác thì nghe nói sách này rất hay và ý nghĩa nên mình cũng muốn tìm đọc thử. Cuối cùng phát hiện được quyển tiếng Anh này nên quyết định mua luôn. Mình thấy những quyển Happy Reader này tiki bán khá rẻ, phù hợp với  các bạn học sinh, sinh viên muốn rèn thêm tiếng Anh như mình. Có khá nhiều quyển kể những câu chuyện quen thuộc. Khi nào đọc xong 4 quyển vừa mua mình sẽ mua tiếp về đọc. Rất có ích.
5
1296172
2016-07-22 18:26:42
--------------------------
132431
5547
Những sách như thế này rất bổ ích cho việc đọc và tăng từ vựng tiếng Anh. Nó có bảng từ vựng khó để hiểu nghĩa. Nó vừa giúp đọc hiểu về các tác phẩm văn học vừa tăng khả nang đọc hiểu tiếng Anh. Có nhiều cấp độ để lựa chọn. 
Tôi đã mua rất nhiều quyển như thế này và thấy rất hữu ích. Đây là cách học tiếng Anh rất hiệu quả và thú vị, không gây nhàm chán như đọc các quyển sách tiếng Anh dày cộm. Nội dung sách vừa phải lại có cốt truyện nên rất hấp dẫn người đọc
5
365079
2014-10-31 16:25:36
--------------------------
