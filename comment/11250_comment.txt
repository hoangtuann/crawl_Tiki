458558
11250
Một cuốn sách khá dài nhưng rất viết rất tường tận về sự thành công của nền kinh tế Thụy Sĩ. Bản thân mình từng làm việc cho công ty Nestle Vietnam nên rất có hứng thú với cách làm ăn của người Thụy Sĩ. Quả thật quốc gia nhỏ bé này khiến cho mọi người đáng ngưỡng mộ. Từ ngành công nghiệp sữa, đồng hồ, đến du lịch, ngân hàng, dược phẩm, kiến trúc...  Lịch sử đã cho thấy chính tài nguyên về con người mới là quan trọng nhất, như Nhật Bản, hàn Quốc và Thụy Sĩ những đất nước có nguồn tài nguyên ít ỏi nhưng đều vươn lên thành những con rồng kinh tế. Một cuốn sách đáng đọc cho những ai quan tâm đến marketing và nghiên cứu kinh tế quốc gia. Tuy nhiên vẫn còn một số điểm mình chưa vừa ý là bản dịch không được hấp dẫn lắm, nội dung còn hơi dài dòng, giáo điều như thể mình đang đọc một nội dung trên Wikipedia.
4
88741
2016-06-25 11:30:22
--------------------------
437437
11250
Sách viết hay nhưng đôi chỗ còn lỗi chính tả. Sách trình bày rõ ràng, chất lượng in tốt, hơi nặng. Sách trình bày chi tiết các câu chuyện thành công của các công ty Thụy Sĩ . Trước đây chỉ biết Thụy Sĩ nổi tiếng về ngân hàng và đồng hồ. Đọc xong thật sự khâm phục người Thụy Sĩ, dù nước nhỏ dân số ít, thiếu thống tài nguyên nhưng lại sở hữu rất nhiều công ty danh tiếng hàng đầu thế giới. Ngẫm lại thấy nước mình còn phải cố gắng rất nhiều mới bằng các nước phát triền.
4
149584
2016-05-28 10:02:07
--------------------------
428373
11250
Cuốn sách này viết khá chi tiết và đầy đủ về các ngành công nghiệp của Thụy Sĩ. Văn phong viết hơi khó đọc và dễ buồn ngủ đối với các bạn ghét đọc những số liệu. Tuy nhiên, với việc kể về những điều thần kỳ mà Thụy Sĩ đã đạt được thì mình nghĩ các bạn sẽ có thêm nhiều thông tin bổ ích. Về ngoại hình sách thì mình thấy cũng khá tốt. Sách này mình mua về tặng một người bạn. Bạn mình review lại và khen rất nhiều. Mình thì thấy đọc chỉ cung cấp thêm thông tin thôi. 3 sao cho cuốn sách
3
493617
2016-05-11 09:32:41
--------------------------
407001
11250
Nội dung sách bàn nhiều về các công ty của Thuỵ Sĩ và những bài học kinh doanh của các công ty này nhằm lý giải cho thành công của đất nước Thuỵ Sĩ. Văn hoá, lịch sử và tính cách của Thuỵ Sĩ đóng vai trò không nhỏ trong thành công này tuy nhiên không được đề cập khai thác kỹ. Sách có nhiều dữ liệu bảng biểu dài dòng gây nhàm chán khi đọc. Về hình thức, mực in hơi mờ còn nhiều lỗi chính tả, cuối trang 533 hình như in thiếu cả một đoạn văn, trang sách kết thúc ở "rằng nguyên lý này sẽ tiếp tục làm nên các" và khi sang đến trang 534 thì chuyển hẳn sang một đoạn mới. Mình hơi thất vọng vì hình thức của sách.
2
470937
2016-03-29 11:40:04
--------------------------
406965
11250
Swiss made là một cuốn sách hay ,hấp dẫn. Cách diễn đạt mạch lạc, từ ngữ dùng dễ hiểu. Cuốn sách đã giúp tôi mở rộng kiến thức về một đất nước Thuỵ Sĩ xa xôi, mà từ trước tôi chưa có kiến thức gì khác ngoài cái thương hiệu đồng hồ. Cuốn sách như mở ra chân trời mới với những quan điểm sống rất khác biệt, rất riêng, rất Thuỵ Sĩ. Mặc dù những nhân vật trong cuốn sách có những người tôi biết và cũng có những người với tôi rất xa lạ nhưng cuốn sách vẫn để lại cho tôi một ấn tượng sâu sắc.
5
702741
2016-03-29 11:07:48
--------------------------
372267
11250
Swiss Made thực sực là một quyển cẩm nang khá đầy đủ và bao quát về nền kinh tế Thụy Sĩ, bao gồm các lĩnh vực: thương mại, du lịch, sản xuất đồng hồ, ngân hàng, y dược, nông nghiệp... Sách đồng thời cũng cung cấp các câu chuyện về sự thành công của các thương hiệu của Thụy Sĩ đã nổi tiếng trên toàn thế giới như: Nestle, Rolex, DKSH, UBS, Holcim. Tuy nhiên, quyển sách cũng khá khó đọc, khi nó đòi hỏi bạn đọc phải có một vốn kiến thức nhất định về lịch sử, chính trị và văn hóa của Thụy Sĩ nói riêng và châu Âu nói chung.
4
143116
2016-01-21 12:25:35
--------------------------
340733
11250
Dạo gần đây mình hay đọc sách về các Quốc gia trên thế giới: Made in Japan, Quốc gia khởi nghiệp và giờ là Swiss Made. Quyển này mình đã đọc ebook được 1/3 rồi mới quyết định tậu để giữ làm của riêng.
Sách khổ to, dày và nặng nhưng người đọc như bị cuốn hút ngay từ những trang đầu tiên. Tác giả phải nói là rất kỳ công, phân tích một cách tỉ mỉ và sâu sắc về đất nước Thụy Sĩ. Chúng ta không chỉ biết đến những thành công, làm nên danh tiếng như khi người ta nhắc tới: ngân hàng, đồng hồ,... mà còn là một loạt những thấy bại.
Cuốn sách tuyệt vời cho những bạn trẻ đam mê khám phá những điều mới mẻ.
5
512469
2015-11-20 13:59:27
--------------------------
320807
11250
Tôi đã được bạn giới thiệu về cuốn sách và quyết định đặt ở tiki mua ngay. Sách rất dày và giấy rất đẹp. Tuy đã biết Thụy Sĩ nổi tiếng về đồng hồ và chocolate nhưng tôi thực sự rất bất ngờ khi được biết Nestle cũng là một thương hiệu nổi tiếng của Thụy Sĩ. Cuốn sách đã cung cấp rất nhiều thông tin bổ ích về con người, đất nước, văn hóa, địa lý, kinh tế Thụy Sĩ. Cách mà họ vượt qua khó khăn, từ một nước không hề giàu có về thiên nhiên, khoáng sản nhưng tự thân vùng lên và trở thành một trong những cường quốc Châu Âu hùng mạnh. Đây thực sự là một cuốn sách rất bổ ích dành cho các bạn sinh viên nghiên cứu, những người đi làm như tôi. 

5
665899
2015-10-12 13:22:51
--------------------------
296792
11250
Từ lâu Thụy Sĩ đã rất nổi tiếng với những cộng nghiệp liên quan đến sự chính xác tuyệt đối. Tuy nhiên qua quyển sách "Swiss Made - Chuyện Chưa Từng Được Kể Về Những Thành Công Phi Thường Của Đất Nước Thụy Sỹ" chúng ta còn biết thêm những thăng trầm và những thất bại mà ít ai biết đến .
Từ một nước chỉ có vài triệu dân , Thụy Sĩ đã đạt được những thành công vượt bật trong kinh tế như về sữa , công nghệ y khoa ... tất cả được nói rất rõ trong quyển sách này và bạn sẽ hiểu tại sao Thụy Sĩ nhỏ bé như vậy nhưng lại là một trong những nước có GDP bình quân đầu người cao nhất thế giới .
Một quyển sách hay và hấp dẫn .
5
554150
2015-09-11 12:08:27
--------------------------
280608
11250
Đây như là một bản tóm gọn một cách chi tiết và đầy đủ nhất lịch sử của Thụy Sĩ, nếu bạn muốn tìm hiểu về kinh tế và cả những ngành công nghiệp lâu đời nơi đây thì đây là quyển sách đáng và cần phải đọc.  Mình nghĩ văn phong mang hơi hướng của một bản báo cáo , chính xác là khá khô khan và kiểu như nó mất đi cái gì đó gọi là văn chương hay cuốn hút , nhưng nội dung tri thức đưa lại hấp dẫn  dành cho tất cả những ai đó đang khao khát tìm hiểu  hay mở rộng vốn hiểu biết của bản thân hay khám phá một cái gì đó mới mẻ hơn ở trong thế giới. 
4
36336
2015-08-27 20:32:47
--------------------------
216756
11250
Tựa sách thần kỳ về Thụy Sĩ này của R.James Breiding không chỉ nêu lên các câu chuyện thành công - cả thất bại - của những công ty trong nhiều lĩnh vực kinh tế khác nhau tại Thụy Sĩ, mà còn thẳng thắn nêu ra những kinh nghiệm và tư duy mà đất nước này đã áp dụng để làm nên những thành công này. Thành công của Thụy Sĩ không chỉ do riêng một người hoặc một tổ chức nào mà bạn sẽ thấy rằng, đây là công sức được đóng góp và chia sẻ bởi toàn dân tộc Thụy Sĩ. Đây là điều mà còn nhiều nước vẫn chưa thực hiện được
4
13083
2015-06-28 11:01:24
--------------------------
197397
11250
Nhắc tới Thụy Sỹ, chắc hẳn ai trong chúng ta cũng nghĩ tới những ngân hàng đảm bảo giữ bí mật của khách hàng hàng đầu thế giới. Nhưng sau khi đọc xong quyển Swiss Made này, tôi thấy được rất nhiều bài học mà chúng ta có thể học được từ sự thành công của đất nước Thụy Sỹ. Toàn bộ những thông tin quan trọng trong sự thành công của đất nước này đã được R. James Breiding đưa vào công trình nghiên cứu của mình, giúp cho độc giả có một cái nhìn toàn cảnh về sự thành công đáng ngưỡng mộ và học tập của đất nước Thụy Sỹ. Một quyển bách khoa toàn thư Thụy Sỹ rất đáng đọc.
5
387632
2015-05-17 15:40:06
--------------------------
124225
11250
Cuốn sách nêu lên một loạt khó khăn và một loạt thành công của Thụy Sĩ. Chỉ đến khi đọc xong bạn mới thực sự hiểu Thụy Sĩ là một đất nước như thế nào. Có những bí quyết của doanh nghiệp, doanh nhân và cả những chính trị gia Thụy Sĩ để truyền cảm hứng cho các bạn trẻ cũng như làm bài học cho những ai đang làm kinh tế và đặc biệt là muốn vươn ra thế giới.Phải nói rằng tác giả đã tìm hiểu và phân tích rất sâu từ những thói quen cho đến tư tưởng của con người nơi đây. Nhưng toát lên một điều xuyên suốt cuốn sách:" Thụy Sĩ là một đất nước không có gì ngoài con người, vì thế họ đi lên nhờ đầu tư vào con người, họ bán sô cô la, pho mát, đồng hồ và cả tuyết, những thứ ở đâu cũng có nhưng không ở đâu tốt bằng ở Thụy Sĩ. Sữa và cà phê có ở khắp toàn cầu nhưng việc phát minh ra cà phê sữa đóng gói đã khiến Nestle trở thành vĩ đại, những điều dó đáng cho một thế hệ doanh nhân Việt Nam phải ngẫm nghĩ. Ngẫm về giá trị của "Swiss made" và "Made in Vietnam". Cuốn sách đáng đồng tiền bát gạo với người đam mê
4
114275
2014-09-06 09:27:08
--------------------------
