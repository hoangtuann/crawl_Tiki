445516
6482
“Thế gia danh môn” đối với mình là 1 truyện xuyên không rất hay, dài nhưng vẫn không khiến mình muốn bỏ ngang. Truyện có cả cung đấu và gia đấu, tuy nhiên cung đấu rất ít, chủ yếu là gia đấu thôi. Nữ chính của truyện - Nhược Nam - rất mạnh mẽ kiên cường, cũng đủ bản lĩnh, nam chính ban đầu đáng ghét nhưng sau này hối cải sửa chữa nên cũng đáng tha thứ. Tình tiết truyện thú vị, nhiều nút thắt mở và diễn biến hợp lý lắm. Mình cho rằng cái hay nhất trong truyện này là tác giả đã khắc họa tâm lý các nhân vật cực kỳ tinh tế và nhạy bén.


5
1324806
2016-06-10 13:24:01
--------------------------
342423
6482
Đây là bộ truyện điền văn đầu tiên mà mình đã đọc, và cũng là một những tác phẩm xuyên không nằm trên kệ sách nhà mình. Lúc đầu nhìn bộ truyện mình ngán ngẫm vì quá là dài, nhưng bạn mình bảo bộ này rất hay. Cho nên mình đã đọc, đọc rất cuốn hút, không dứt ra được, càng đọc càng thích. Mình thích Cận Thiệu Khang và Tưởng Nhược Nam, mình ghét ông Cảnh Tuyên đế biến thái. Cảm xúc sau khi đọc xong 4 cuốn, lại lôi ra nghiền lại, mình là người viết văn cực dở, cho nên mình chỉ nói gọn lại: mình đã không hối hận khi mua bộ truyện này, nên đọc. 
5
694893
2015-11-24 13:34:35
--------------------------
329746
6482
Bộ truyện nên có của những nàng mê tiểu thuyết xuyên không, nữ chính không nhu nhược. Năm chính trước vô tình, sau chuyên sủng.
Nữ chính xuyên không về, có được tài lẻ ấn huyệt chữa bệnh bằng phương pháp cổ truyền, cộng với cái tính an phận nhưng không nhu nhược mà dần chiếm được cảm tình.. Mình chắc chắn bạn đọc sẽ rất thích tính cách của nữ chính. 
Phần cuối của truyện xuất hiện hai nhóc tì, làm cốt truyện dịu xuống và rất hài hước. 
Toàn truyện ghét nhất là ông vua "vô duyên". Đại loại là kết thúc làm mình rất thỏa mãn, ngược vừa đủ, sủng vừa đủ.
5
315966
2015-11-01 12:14:46
--------------------------
320442
6482
Truyện điền văn,cung đình hầu tước,một đôi,cổ đại.
Nói chung là không có gì để chê về tác phẩm này.Dù ấn tượng của mình hơi nhạt nhưng truyện rất hay.(Bạn cũng đừng e ngại vì câu mình nói là nhạt nhé,mình không thích điền văn đâu,đọc xong là quên luôn)
Cũng như bao truyện ngôn tình xuyên không khác,tôn sùng nữ chính lên quá,mừng là vẫn bám trọng tâm chủ đề truyện, Nhược Nam đã ứng xử rất khôn khéo để tồn tại trong thế gia vọng tộc.Nàng chỉ có một ý kiến duy nhất ‘khác người’ về thế giới này.Trượng phu của nàng,nàng không muốn nhứng chàm những người con gái khác.Thế là OK rồi nhỉ?
Còn về các nhân vật nam,mình thích sự chung tình của Thiệu Khang và sự thẳng thắn của chàng tướng quân máu lạnh.Xuyên không không có nơi nương tựa thì chàng tướng soái này là chọn lựa số một!
Anh hoàng thượng mình chỉ thấy ngao ngán.Thế quái nào lại rờ đến vợ thần tử kia chứ!Sao không thế như  Thịnh thế trà hương,đối thủ là một viên quan như Tể tướng khuynh quyền chẳng hạn!Điểm duy nhất mình hài lòng về con người này là lúc kết thúc truyện,cuối cùng vị cửu ngũ ấy cũng đã chịu buông tha cho 2 người(Phù!)
Ấn tượng nhất là 2 cậu bé sinh đôi .Dễ thương quá đi mất!là bộ truyện lý tưởng cho những fan điền văn nhá

4
487743
2015-10-11 10:56:35
--------------------------
317850
6482
Đây là một truyện thuộc thể loại điền văn, xuyên không, gia đấu khá hay. Tình tiết hấp dẫn, logic, nội tâm nhân vật sâu sắc, lối hành văn cũng trau chuốt tinh tế nữa. Mình đăc biệt thích cá tính nữ chính Nhược Nam, thông minh, bản lĩnh, dù có lập trường kiên định và nguyên tắc vững vàng. Còn nam chính Cận Thiệu Khang với cả hoàng thượng vừa nhạt nhòa lại vừa đáng ghét, nữ phụ còn đáng ghét hơn nữa. Dù sao cũng phải nói là khi xây dựng được những nhân vật phản diện khiến người đọc ngán ngẩm như vậy chứng tỏ tác giả đã thành công rồi.
5
630600
2015-10-04 14:22:07
--------------------------
300261
6482
Truyện thể loại điền văn và hơi dài, những 4 tập dày, nhiều người chê là dài dòng, nhưng cá nhân mình thấy truyện khá trọn vẹn và lôi cuốn, khiến mình đã say mê xem không ngừng được cho tới khi xem tập cuối cùng. 2 tập cuối là những mối xung đột gay gắt giữa Hoàng Thượng, Nhược Lan và Thiệu Khang, khiến cho cốt truyện cũng trở nên nhanh và hồi hộp hơn. Tuy cuối cùng mâu thuẫn cũng được giải quyết, kết cục viên mãn đại đoàn viên, nhưng mình thật sự hơi thất vọng tí về cái kết. Về giải quyết hiểu lầm, motip có vẻ cũ và đến cuối cùng Nhược Lan cũng không thể trao niềm tin trọn vẹn cho Thiệu Khang, dù anh xứng đáng được nhiều hơn thế. 
Bìa 2 tập cuối quả thật rất đẹp, 2 bảo bối song sinh quả thật rất đáng yêu.
5
41744
2015-09-13 20:49:38
--------------------------
291669
6482
Truyện có thể coi là dài,những 4 tập nhưng cốt truyện không bị nhàm chán,có độ cao trào,hấp dẫn khiến mình đọc không muốn ngừng. Tuy nhiên ở 2 tập 3,4 này khá nặng nề đối với mình. Hoàng thượng thật sự rất quá đáng,phá gia đình của người ta rồi còn cướp vợ của thần tử,áp đặt đủ điều,coi mà bị Hoàng thượng làm cho tức chết. Nhưng nhờ có những tình huống hiểu lầm mà ta càng thấy rõ Thiệu Khang - 1 người đàn ông với tư tưởng cổ đại rất chung tình với Nhược Lan,vượt qua tư tưởng tam thê tứ thiếp,một lòng một dạ với nàng!
5
127341
2015-09-06 16:22:23
--------------------------
287897
6482
Hai quyển 3 và 4 của thế gia danh môn có bìa rất đẹp, nói chung là mình đặc biệt thích bìa truyện. Khi đọc truyện mình thường rất thích những quyển thuộc thể loại xuyên không và thế gia danh môn cũng không ngoại lệ. Tuy là bộ truyện này khá dài nhưng không phải vì thế mà nội dung lại trở nên nhàm chán. Cách miêu tả tâm lí nhân vật vô cùng logic, diễn biến câu chuyện lại rất thú vị nhưng cũng không tránh khỏi những đoạn bi thương. Được cái là đoạn kết viên mãn  nên mình không có gì phải phàn nàn.
5
408083
2015-09-03 09:11:01
--------------------------
271343
6482
Tôi đặt mua ngay khi phần tiếp theo vừa được xuất bản. Sách đúng chất điền văn, rất dài nhưng nội dung hay và hấp dẫn vô cùng nên thật không uổng phí khi dành thời gian và công sức để đọc. Mối tình cưới trước yêu sau của cặp đôi Tưởng - Cận lại có thêm kẻ thứ ba đáng ghét, chính là Cảnh Tuyên Đế. Bằng khả năng hô phong hoán vũ, ông ta làm mọi cách để chia rẽ hai người. Tưởng Nhược Nam vốn là người phụ nữ thời hiện đại, không chấp nhận việc chia sẻ chồng mình với người khác, nên chọn cách xa rời chàng. Tôi đồng tình với cách làm này của cô ấy. Còn về Cận Thiệu Khang, tôi vừa giận, vừa thấy thương chàng. Cũng may kết cuộc đại đoàn viên. Văn phong của Thập Tam Xuân rất mượt, diễn biến nội tâm nhân vật hợp lý. Truyện có êm ả, có cao trào, dài nhưng không chán. Bìa sách hai tập cuối có hình ảnh minh họa đẹp.
5
109583
2015-08-19 07:59:20
--------------------------
250947
6482
Tình tiết dồn dập, hồi hộp, gay cấn nhưng không khiến người đọc cảm thấy quá nặng nề. Truyện dài nhưng các tình tiết đều rất hợp lý khiến  cho câu chuyện trở nên hấp dẫn. Một khi đã cầm quyển sách lên là khó có thể đặt xuống. Cảm giác sẽ như bị chìm đắm vào câu chuyện: ngưỡng mộ nữ chính, say mê nam chính, vừa giận vừa thương các nhân vật phụ khác. Thực ra mình đã đọc truyện này trên mạng nhưng vẫn bỏ tiền ra mua trọn bộ 4 quyển và cảm thấy số tiền bỏ ra quả thật không hề uổng phí. Một buổi tối, bên bàn học, một ly sữa ấm và một cuốn truyện trong bộ truyện này, đối với tôi thì đó là tất cả những gì tôi cần cho một buổi tối tuyệt vời! 
P/s: chỉ mong sao sau này khi lập gai đình mình có thể tỉnh táo, sáng suốt và kiên cường như nhân vật nữ chính...
5
722766
2015-08-02 11:05:40
--------------------------
149546
6482
Phải nói là truyện rất dài, ấy vậy mà mình mải miết đọc đến không dứt ra được, không thấy nhàm chán chút nào. Mình cực kỳ thích cách xây dựng hình tượng nữ chính trong truyện này. Tưởng Nhược Nam, một cô gái đến từ thế kỷ hai mốt, khi xuyên không về cổ đại đã “thừa kế” thân xác của Nhược Lan – cô gái nổi tiếng khắp kinh thành vì “xấu tính xấu nết” và phải ngậm ngùi “dọn dẹp hậu quả” thay cô ta. Tính cách của Nhược Nam hết sức thú vị, đủ mạnh mẽ, đủ thông minh, khéo léo, biết tiến biết lùi, cô đã dần dần cải thiện hình ảnh mình trong mắt mẹ chồng, em chồng, mọi người xung quanh,… và giành được trái tim của phu quân Thiệu Cận Khang. Truyện này là điền văn, chủ yếu chỉ xoay quanh gia đình nhà họ Thiệu thôi, nhưng văn phong của Thập Tam Xuân rất tuyệt, truyện vẫn rất đáng đọc
5
449880
2015-01-13 23:00:23
--------------------------
141390
6482
Đọc truyện này, đoạn đầu thấy khá là thú vị, nhất là cái đoạn Nhược Nam phải chịu cảnh “quýt làm cam chịu”, ngậm ngùi “dọn dẹp hậu quả” thay Nhược Lan của kiếp trước. Nữ chính Nhược Nam là một cô gái mạnh mẽ, bản lĩnh, biết lý lẽ lại có tư tưởng bình đẳng, khá thích tính cách này của cô ấy. “Tính cách quyết định số mệnh, đường là do mình đi mà nên”, đấy là tư tưởng mà tôi cực kỳ thích. Cũng chẳng có gì lạ khi cô gái ấy từng bước cải thiện hình ảnh, bước ra khỏi cái bóng nanh nọc đáng ghét trong quá khứ, chiếm được tình cảm của nam chính Thiệu Cận Khang. Mất tội đoạn sau hơi loằng ngoằng, tiểu tam tiểu tứ tùm lum, lại còn dính dáng đến cả chính trị nữa, càng đọc càng ghét anh hoàng thượng. Nhìn chung, cốt truyện tương đối ổn, xây dựng tình huống khá, lối viết chắc tay, chỉ khổ nỗi truyện dài quá, đọc mà thực sự rất mệt.
3
393748
2014-12-15 13:26:19
--------------------------
123388
6482
Mình rất khâm phục nhân vật Tưởng Nhược Lan . Bởi vì là một cô gái hiện đại xuyên không tới thời cổ đại nên cô vẫn giữ tư tưởng một vợ một chồng . Khi phát hiện ra rằng mình yêu Hầu gia Nhược Lan đã phải đấu tranh lí trí rất nhiều để đưa đến quyết định cuối cùng . Nếu ở tập 1 và tập 2 nhân vật Vũ Thu Nguyệt mưu mô xảo trá khiến ai cũng ghét thì tập 3 và tập 4 lại xuất hiện một nữ thứ vốn là sát thủ còn ghê gớm hơn Vũ Thu Nguyệt rất nhiều lần . Mình không thích nhân vật hoàng thượng cho lắm chỉ vì tình yêu của mình mà hắn sẵn sàng hủy bỏ hạnh phúc của Nhược Lan . Nhược Lan đã chứng minh rằng cô là người phụ nữ kiên cường có thể sống sót khi bị ruồng bỏ . Rất thích cặp sinh đôi của Nhược Lan và Hầu gia chúng rất đáng yêu
4
337423
2014-09-01 09:13:51
--------------------------
122966
6482
Mỗi người có quan điểm riêng và sở thích riêng, nên những nhận xét của tôi đây cũng chỉ mang tính chủ quan. Các bạn có thể tham khảo và quyết định có nên thử hay không tác phẩm này. Tôi thấy tác phẩm này tuy ko quá xuất sắc khiến người đọc cứ muốn đọc lại mãi nhưng vẫn khá ổn. Văn phong của Thập Tam Xuân mượt, ổn định. Cách xây dựng nhân vật mang hơi hướm hiện đại nhưng đôi khi Tưởng Nhược Nam vẫn biết suy nghĩ cho hoàn cảnh hiện tạ và xử lý tình huống khôn khéo, nên nữ chính tôi vẫn thấy là người suy nghĩ sâu sắc chứ ko hời hợt. Thập Tam Xuân giống như đang chơi 1 ván cờ với chính mình, cốt truyện xây dựng những cao trào tạo nên nút thắt, như thử thách cách ứng xử của nữ chính cũng như cách giải quyết tình huống của bản thân. Nhưng lần nào Tưởng Nhược Lan cũng khiến tôi hài lòng về cách giải quyết vấn đề ở Hầu phủ, tuy ko phải cách giải quyết hay nhất, nhưng luôn là cách tốt nhất trong tình huống hiện tại. Còn Cận Thiệu Khang là mẫu đàn ông phong kiến điển hình. Nhưng vì Nhược Lan mà người luôn luôn khuôn phép như hắn cũng phải phá vỡ nguyên tắc cổ hủ của mình. Nhược Lan vì hắn mà thay đổi, hắn luôn ghi nhớ điều đó và luôn trân trọng, cũng vì Nhược Lan mà thay đổi nhiều. Hai người bỏ lỡ nhau mấy năm trời bởi vì không thể có điều j tốt đẹp có thể đạt được dễ dàng được. Nên tôi coi đó là thử thách, là sóng gió để 2 người nếu có thể vượt qua lại được hưởng cái hạnh phúc mà họ xứng đáng được có. Nên Cận Thiệu Khang cũng ko phải người luôn bỏ lỡ, hắn đã luôn cố gắng để bù lại lỗi lầm trong quá khứ và để lấy lại những j đã đánh mất. Tôi không phủ nhận là cuối truyện hơi dài dòng, nhưng nhìn chung vẫn ok và đáng đọc.
4
255606
2014-08-29 09:59:41
--------------------------
122393
6482
Cá nhân mình khi đọc tác phẩm này thì cảm thấy rất thích. Mình không cảm thấy nó nhàm. Tuy độ dài thì hơi dài 1 chút, tất nhiên hơi tốn tiền mình 1 chút :) nhưng quả thật đáng đồng tiền. Mình cũng tương đối khó tính trong chuyện chọn tác phẩm để đọc, vì tuổi mình không nhỏ, cho nên nếu tác phẩm quá hời hợt thì chắc mình sẽ không để mắt đến. Mình không nghĩ cốt truyện gượng ép. Theo quan điểm của mình thì tính cách nhân vật khá ổn, chỉ có điều tác giả xây dựng nhân vật nữ ... hình như hơi nhiều may mắn trong cuộc sống cổ đại. Tưởng Nhược Lan là 1 người kiên cường, có tôn nghiêm trong cuộc sống, những điều cô ấy quyết định thì khó mà thay đổi. Đôi khi sống làm người nên cần có những điều như thế. Nếu điều chúng ta nghĩ là đúng thì nên kiên trì dù cho điều đó trái ngược với suy nghĩ của xã hội. Cho nên tôi không nghĩ cô ấy hời hợt. Còn Cận Thiệu Khang thì vì bảo vệ tình yêu của mình mà đi ngược với lễ giáo phong kiến, vì điều đó làm tổn thương tình yêu của họ. Chàng đã cố gắng hết sức để giữ lời hứa của mình, đến cái chết cũng không màng tới thì không thể gọi là bỏ lỡ được. Tất nhiên, không có gì là hoàn hảo ở đời, cho nên theo quan điểm cá nhân của tôi thì truyện này không thể bị đánh giá quá tệ. :) Có thể xem là tác phẩm hay. Các bạn có thử!!! :)
5
59876
2014-08-25 18:12:39
--------------------------
122034
6482
Đây là bộ truyện gây bức xúc nhất mình từng mua. Có bốn điểm mà mình vô cùng không hài lòng.

Thứ nhất, cốt truyện nhàm chán đến đáng sợ, không có một chút điểm nhấn tạo nên khác biệt nào y hệt những bộ xuyên không thị trường mình đọc trên mạng.

Thứ hai, nữ chính và nam chính đến với nhau một cách quá gượng ép, vừa không có tí sóng gió nào, vừa không phù hợp. Nếu nói thẳng ra, Tương Nhược Lan là người hời hợt, còn Cận Thiệu Khang là kẻ luôn bỏ lỡ.

Thứ ba, khắc họa nhân vật không có chút lôi cuốn nào, miêu tả tâm lí quá trần trụi, không gây được sự tò mò đối với diễn biến câu chuyện.

Thứ tư, là về độ dài. Một câu chuyện có cốt như thế này, mà được kéo dài đến bốn tập là điều không chấp nhận được. Mình cảm thấy, giống như người ta vẫn nói, nên để nó "chết sớm siêu sinh sớm" thì tốt hơn. Phần mở đầu thì dông dài, phần diễn biến thì loằng ngoằng, có khi mình đọc đi đọc lại mà chẳng hiểu gì, có lúc đọc lướt cách tới tận sáu bảy chương mà vẫn hiểu được nội dung chính, chẳng hiểu đấy là do mình quá thông minh hay do tác giả viết trần trụi quá, phần kết thúc thì có thể dùng một cụm từ thế này: Tràng - giang - đại - hải. Một đoạn kết thôi mà nhập nhà nhập nhằng, đọc vào khiến mình rất khó chịu.

Chưa kể đến mấy điểm mình chưa nêu ra cụ thể nữa.

Có lẽ những lời phê phán của mình hơi nặng nề, nhưng quả thực tác giả đã rất thất bại trong việc viết ra một câu chuyện đủ hay để lôi kéo độc giả khó tính là mình. Cảm giác sau khi mua và đọc xong có thể tóm gọn lại thế này: Tốn thời gian và tốn tiền.
1
137364
2014-08-22 20:06:58
--------------------------
