425188
2966
Nếu ai từng yêu thích phim hoạt hình masupilami trên màn ảnh nhỏ thì cũng nên xem thử quyển sách với phiên bản truyện này. Tiki giao hàng rất nhanh. Chất lượng giấy in tốt. Màu đẹp . Nhưng về phong cách dịch thuật thì mình không được hài lòng lắm. Cách nói của các nhân vật thì không tự nhiên nét vẽ có cảm giác không được trau chuốt cho lắm. Truyện đi hơi nhanh có chỗ còn khó hiểu. Nhưng đó chỉ là ý kiến khách quan của mình thôi. Cuối cùng thì đây vẫn là một quyển sách hay dành cho fan của chú marsu đuôi dài này.
4
1207993
2016-05-03 22:28:00
--------------------------
377076
2966
Sách được minh họa bằng những hình ảnh sống động của Marsupilami  qua từng trang sách và những câu thoại dễ thương. Thay vì đọc nhũng cuốn sách cổ tích ngày xưa cho con em của bạn thì bạn có thể đọc những cuốn sách về chú Marsupilami thông minh diệt trừ bọn xấu cho con em của bạn trước khi đi ngủ. Cũng là một cách rất tốt trong việc giúp bạn và con em gắn kết hơn, để con em bạn hiểu thêm về thời thơ ấu của bạn cùng chú Marsupilami thông minh, không phải khỉ, cũng không phải báo cũng chẳng phải vượn.
5
383240
2016-02-01 16:11:51
--------------------------
321398
2966
Được Franquin thai nghén từ serie truyện Spirou & Fantasio, con vật Marsupilami với nhiều đặc tính kỳ lạ, nhiều khi chiếm hết chú ý trong một số truyện của serie Spirou, xứng đáng có một serie riêng. Và tập 1 này đã minh chứng điều đó. Vẫn giữ được nét vẽ từ serie Spirou, với phong cách kể chuyện thông qua nét vẽ mô tả hành động, hình dáng đặc trưng trong các tình huống khác nhau của Marsupilami, tập truyện này đã giới thiệu thành công nhân vật với nhiều tình tiết vui nhộn. 
Hình thức trình bày bị một điểm trừ về font chữ khi so sánh một số từ có font chữ gốc trong truyện, không đúng phong cách truyện tranh Spirou. Màu sắc và giấy in tốt. Dịch thuật tốt, không gượng, nhưng có một điểm gây thắc mắc cho cá nhân về chi tiết chanh dây có độc, mặc dù cũng tìm kiếm thấy có những tác hại nhưng làm ngất liền cả con heo thì cũng hơi bị độc. Không rõ chi tiết này có chính xác về dịch thuật không
4
729620
2015-10-13 20:59:15
--------------------------
319161
2966
Mình mê phim hoạt hình này từ bé nên khi thấy truyện xuất bản thì không thể bỏ qua. Đúng như mong đợi là truyện vẫn hấp dẫn như phim, gia đình marsu vẫn đáng yêu như vậy, các câu chuyện liền mạch hơn phim hoạt hình nữa. Vừa đoc truyện mình vừa có thể tưởng tượng ra âm thanh trong phim hoạt hình, giống như ôn lại kí ức tuổi thơ. Truyện màu sắc bắt mắt, khổ sách cũng vừa (giống bộ lucky), in ấn tạm ổn, nhưng mình không thích font chữ này, nhiều chỗ chữ quá nhỏ nữa, nói chung mình vẫn khá hài lòng với cuốn truyện vui tươi này.
4
145252
2015-10-08 00:56:47
--------------------------
261595
2966
 Thường thì nếu đã trót yêu thích bộ phim hoạt hình trước rồi thì người ta thường so sánh với tác phẩm truyện, nhưng mình thấy ở mỗi thể loại đều có cái hay riêng. Lúc thấy thông báo xuất bản Marsupilami, mình đã quyết định mua ngay về đọc dù đã nằm lòng nội dung qua phim rồi. Và quả thật không phụ lòng mong mỏi, cuốn truyện in màu rất đẹp và rõ ràng, hình ảnh không bị nhòe. Cốt truyện cũng được thay đổi linh hoạt và phù hợp với khung hình, không bị quá gượng kiểu như cắt ghép từ bộ phim hoạt hình mà có thể đọc một cách tự nhiên, lôi cuốn, ngay cả với những người lần đầu đọc nó. Chỉ có chút không ưng ý là có một số khung hình với chú thích quá nhỏ nên không thể nhìn rõ ( một số thôi ), còn lại thì đều ổn ^^ 
4
133044
2015-08-11 11:22:25
--------------------------
260377
2966
Rất vui vì cuối cùng cũng Marsupilami đã được xuất bản và nhượng quyền ở VIệt Nam. Mình cũng vui khi truyện giữ nguyên khổ giấy và tên các nhân vật cũng như cách nói chuyện được giữ nguyên, được xem lại về gia đình Marsupilami, gặp những người thổ dân và gã thợ săn xấu tính, cảm giác như tuổi thơ cách đây hơn 10 năm sống lại.
Điểm trừ của cuốn truyện là in màu chưa được sắc nét lắm, màu còn bị nhòe và font chữ sử dụng thực sự là lạ lùng và khó đọc quá (hơi buồn 1 chút). Trang đầu tiên khung chữ đầu tiên còn sai chính tả c - t. Hi vọng tập sau sẽ được chăm chút hơn
3
162924
2015-08-10 12:50:56
--------------------------
259295
2966
Lúc cầm cuốn này trên tay mình thật sự rất thích thú. Bìa đẹp, bắt mắt. Hình ảnh marsu trong tuổi thơ như sống lại, làm mình cảm giác bản thân như trẻ hơn ^^. Hình vẽ sống động, màu sắc rất bắt mắt, đọc mà cảm tưởng như mình cũng đang hòa vào trang truyện, được tận hưởng tất cả cảm giác thoải mái nhất khi ở trong một khu rừng tươi xanh sống động như thế. Hình tượng marsupilami rất dễ thương. Chắc phải mau chóng mua thêm những cuốn khác để tiếp tục phiêu lưu cùng marsu thôi
5
529003
2015-08-09 12:04:36
--------------------------
248061
2966
Mình đặt trước cuốn này cả 2 tuần; hồi nhận được sách, mừng húm luôn ( chưa kể được tặng thêm cây quạt :3 hơi bị dễ thương ^^ ). Sách vẽ đẹp, không biết sao thấy nội dung hơi khác với phim hoạt hình, hình như kể từ nguồn gốc luôn thì phải; mà nhìn chung là hay. Mỗi tội là font chữ không được bắt mắt cho lắm, chỉ ở mức chấp nhận được ^^ . Đọc xong mình thấy như tuổi thơ quay về, dù ngót nghét cũng 19 - 20 rồi. Đang đợi đợt đặt sách tiếp theo hốt luôn cuốn 2 & cuốn 3 ( nếu có ) về nhà ^_^
5
575572
2015-07-30 17:11:37
--------------------------
236491
2966
Từ nhỏ mình đã thích coi phim Marsupilami rồi, nên thấy có truyện tranh được xuất bản là mình đặt mua liền. Lúc mình mua được tặng kèm một cây quạt, hình Marsupilami luôn, dung quạt thì không tốt, chủ yếu để trưng chơi thôi. Về truyện, sách khổ lớn, in màu, rất đẹp và rõ rang. Nội dung dễ thương, phù hợp cho trẻ nhỏ và để giải trí. Truyện khá mỏng, đọc chơi giải trí rất tốt. Điểm trừ duy nhất là phần giới thiệu lúc đầu chữ hơi nhỏ, lúc đọc phải đưa sát lại. Ngoài ra, truyện rất dễ thương, cả trẻ em lẫn người lớn đều có thể đọc.



5
10280
2015-07-21 20:30:59
--------------------------
228851
2966
Nhận xét đầu tiên là về hình thức:
- Tác giả vẽ rất đẹp, màu sắc rất bắt mắt
- Chất lượng giấy tốt
- Lúc mình mua có quà tặng là quạt nhựa Marsu nữa, rất thích 
Nhận xét thứ hai là về nội dung:
- Câu chuyện bắt đầu với những tình tiết nhẹ nhàng bằng lời dẫn hài hước của người dẫn chuyện. Càng về sau càng hồi hộp gay cấn. Và.... kết thúc lúc chuyện phần thắng lúc nào cũng thuộc về chú Marsu, làm lão thợ săn Backalive.thua tức tối.
Khi đọc truỵện mình cứ tưởng như đang được sống trong khu rừng đó luôn vậy, cảm giác thật sự rất tuyệt. Đối với những ai đã từng hâm mộ anh chàng Marsu thì  sẽ không thể nào bỏ qua tác phẩm này.
5
673600
2015-07-16 08:05:17
--------------------------
226106
2966
 Hồi còn học cấp hai, cứ mỗi chiều đi học về ăn cơm xong là mình lại bật tivi theo dõi từng tập phim hoạt hình Marsupilami.Dù đã xem bộ phim vui nhộn ấy rồi nhưng mình cũng không khỏi hào hứng và mong chờ khi biết tin Marsupilami phiên bản sách sắp phát hành .Quyển truyện tranh này không hề làm mình thất vọng , hình thức rất tốt , màu sắc phải nói là rất sinh động và tự nhiên. Nội dung truyện vẫn duyên như ngày nào ,cốt truyện diễn ra khá hợp lí ,  nhưng phần dịch còn chưa được tự nhiên và hấp dẫn . Mình đặc biệt rất kết cây quạt giấy , thiết kế rất đẹp và tiện dụng .Hi vong những tập truyện tiếp theo sẽ còn tốt hơn thế và có thêm nhừng quà tặng kèm hấp dẫn nữa .
4
635331
2015-07-11 14:25:21
--------------------------
223153
2966
Marsupilami là bộ phim hoạt hình rất nổi tiếng nhiều năm về trước, hình ảnh chú Marsu đáng yêu hầu như đã ăn sâu vào tâm trí của nhiều khán giả. Nhưng phải chăng vì bộ phim hoạt hình quá đỗi đáng yêu và thú vị ấy mà nó đã trở thành rào cản khi Marsupilami biến thành nhân vật truyện tranh ? Bản thân mình rất yêu thích những con Marsu, hơn nữa nó lại gắn bó với tuổi thơ của mình (1 thời phát cuồng mà) nên khi thấy có truyện mình đã lập tức mua ngay và vô cùng hào hứng, thế nhưng khi đọc dường như mình không có cảm giác gì cả, chú Marsu nghịch phá, tinh ranh, hiếu động trên màn ảnh nay đã trờ thành 1 nhân vật "tĩnh" nên hình như độ sinh động, hài hước đã giảm đi đáng kể, và cũng do bây giờ mình đã lớn rồi nên không còn quá hứng thú bởi 1 nhân vật hoạt hình thế này nữa chăng ?... Với lại mình nhớ hồi nhỏ cũng có đọc phiên bản comic của Marsupilami rồi, nhưng mà bản mình đọc thì lúc đó Marsu đực vẫn chưa có gia đình, sau này mới gặp Marsu cái và có 3 đứa bé Marsu thiệt dễ thương ! Nhưng phiên bản được mua bản quyền đợt này lại là 1 bản khác, và mình thấy bản này không hài hước bằng bản cũ... Bản dịch của tập Marsu này mình thấy còn hơi khô khan, cứng ngắc, kém phần hấp dẫn, kém mượt, hy vọng về sau dịch giả sẽ dịch vui hơn, phù hợp hơn, và còn 1,2 lỗi type thì phải ^^ Nhưng được cái bìa  thiết kế rất đẹp, giấy mịn và tốt, lên màu tốt, chỉ có những chỗ màu đậm quá như là màu đen hoặc màu đỏ thì bị in nhoè, nếu để ý kỹ các khung thoại có hình trái tim của Marsu cái và khung thoại có chữ màu đen in đậm (ví dụ như "khòoo" - tiếng ngủ ngáy của ông thợ săn) sẽ thấy màu hơi hơi nhoè. Điều mình thích nhất là tiki tặng quạt giấy hình Marsupilami cho 200 đơn hàng đầu tiên (mình đã nhận được), quạt rất xinh ! Tập 2 thì nghe nói được tặng kèm bookmark, chính những món quà nhỏ đi kèm này là nguyên nhân khiến mình thấy thích hơn cả ! Dù sao thì mình cũng sẽ mua ủng hộ T.A books, vì đây là 1 đơn vị phát hành truyện mới toanh, mà Việt Nam còn ít NXB mảng truyện tranh quá nên hy vọng T.A books có thể góp phần đem lại những sản phẩm tốt và hay đến với độc giả.
4
75959
2015-07-06 17:53:46
--------------------------
