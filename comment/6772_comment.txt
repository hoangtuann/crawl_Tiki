512073
6772
Lối viết rất hay và có nhiều nội dung phù hợp vs lứa tuổi 
Mọi người nên có quyển này trong tủ sách :)
5
1994074
2017-01-17 17:30:50
--------------------------
457827
6772
Những mẩu truyện nhẹ nhàng nhưng không có nhiều đặc sắc, nhìn chung là không quá ấn tượng! Mình hơi thất vọng vì nội dung sách, có nhiều mẩu chuyện khá hụt hẫng. Tâm lí nhân vật không có gì đặc biệt, hơi hời hợt, bộc lộ nội tâm không tốt, phù hợp với thiếu nhi hơn là người lớn.
Tác giả chỉ khắc họa lại những câu chuyện nhỏ trong cuộc sống hằng ngày của con gái, rất bình thường.
Bù lại phần hình thức bắt mắt, bìa sách đẹp, dày vừa phải, có phần mở đầu khá hay.
Mình thấy đây chỉ là một cuốn sách trung bình khá, không phải hay nhưng cũng không đến nỗi quá tệ.
3
1161050
2016-06-24 16:25:44
--------------------------
371468
6772
Cuốn nhật ký nữ sinh của Dương Hồng Anh nói về cô bé lớp 6 Nhiễm Đông Dương và những nỗi niềm của tuổi dậy thì, xoay quanh cuộc sống hằng ngày của cô bé lúc ở nhà cũng như lúc ở trường với giọng văn nhí nhảnh của một cô bé 12 tuổi làm mình cảm thấy cũng giống như mình đang chia sẻ nỗi niềm đó trên những trang sách ấy (mình cũng 12 tuổi) về những người bạn trong lớp về những thay đổi của tuổi dậy thì về gia đình. Các bạn nên mua đọc thử đi mình thấy cũng hay đó.
4
1043353
2016-01-19 17:02:00
--------------------------
355418
6772
Đây là lần đầu tiên mình được đọc một cuốn sách dưới dạng một quyển nhật kí. Mặc dù mình không thích văn học nước ngoài nhưng khi đọc quyển sách này thì mình lại có một suy nghĩ khác. Quyển nhật kí miêu tả chân thật về cuộc sống, tình cảm bạn bè, thầy cô của nhân vật chính Nhiễm Đông Dương ( là một cô bé cùng tranh lứa với mình, 1 cô bé hồn nhiên đang chuẩn bị cho cuộc hành trình thoát xác để hóa bướm). Bìa sách lại rất đẹp và teen nên mình nghĩ đây là một cuốn sách tuyệt vời !!!
5
637989
2015-12-19 21:47:48
--------------------------
352271
6772
Mình luôn muốn tìm những cuốn sách có nội dung về trường học, giới tính và mọi thứ xung quanh những cô gái mới lớn, thật may là sau lần dạo Tiki đã thấy Nhật ký nữ sinh.
Sách dày so với học sinh tiểu học nhưng lại có nội dung thu hút, mỗi chương truyện không dài, tâm sinh lý, bối cảnh khá hợp nên em mình không ngại dài mà đọc hết rất nhanh. 
Nhật ký nữ sinh rất hợp với lứa tuổi dậy thì, cuối tiểu học đầu trung học.
Mình rất muốn mua thêm những cuốn kiểu như thế này, không biết còn cuốn nào không nhỉ?
4
49203
2015-12-14 10:27:21
--------------------------
333197
6772
Dương Hồng Anh thật tài tình khi có thể viết ra 1 cuốn sách hay đến như thế. Những mẫu truyện ngắn được lồng dưới dạng nhật ký của cô bé Nhiễm Đông Dương với bạn bè, gia đình. Mỗi câu chuyện là 1 ý nghĩa hay và sâu lắng, có thể làm rung động với những người khó tính nhất. Bìa sách khá đẹp, láng, giấy cũng khá dày, hình minh họa dễ thương. Nếu bất cứ ai có 1 cô con gái đang ở độ tuổi mới lớn thì có lẽ cuốn sách này khá hợp để có mặt trong tủ sách.
5
290174
2015-11-07 16:30:09
--------------------------
300709
6772
Mình rất thích cuốn sách này,ban đầu minh mua vì mình thích những cuốn sách có liên quan đến học sinh,nhưng nội dung sách không hề làm mình thất vọng,cuốn sách này có chuyển thể thành phim,nhưng bất kỳ cuốn sách nào thì khi mình đọc đều sẽ hay hơn là xem,nên dù mình đã xem bộ phim này rồi nhưng mình vẫn thích đọc,phải nói tác giả của cuốn sách này rất hiểu tâm lý của nữ sinh và các cô cậu học trò!Pha lẫn với những câu chuyện về học đường là những câu chuyện cười rất hài và thú vị!!
4
413637
2015-09-14 09:49:51
--------------------------
215750
6772
Mình đã từng xem bộ phim được chuyển thể từ tác phẩm "Nhật kí nữ sinh" nhưng khi đọc nó vẫn cảm thấy rất hấp dẫn. Hình thức nhật kí khiến cho tác phẩm trở nên gần gũi với lứa tuổi học trò. Cùng với đó, những câu chuyện được cô bé nữ sinh lớp 6 Nhiễm Đông Dương ghi lại hết sức quen thuộc, gợi cho người đọc nhớ lại thời học sinh của mình. Từng dòng nhật kí được tác giả Dương Hồng Anh viết theo cách nhìn của các em thiếu nhi nhưng vẫn truyền tải được nhiều thông điệp sâu sắc về tình cảm bạn bè, thầy cô, gia đình.
5
470567
2015-06-26 19:31:11
--------------------------
160718
6772
Câu chuyện được viết dưới dạng Nhật Ký, có chút hài hước và thú vị nhưng cũng sâu sắc. Tác giả là một người tâm lý rát hiểu tâm trạng của tuổi cỡ khoảng lớp 5-6-7. Điều đó cũng để mọi người hiểu rõ hơn về tâm lý của tuổi này. Một tuổi hồn nhiên trong sáng, với nhiều ước mơ bay bổng. Giúp chúng ta tự tin hơn trong cuộc sống, hiểu rõ hơn về chính mình. Đồng thời cũng nói lên mặt xấu và tốt. Về phần giấy thì ổn, bìa đẹp, đơn giản không quá cầu kì.
5
477877
2015-02-25 12:11:57
--------------------------
132037
6772
Nhật Ký Nữ SInh là câu chuyện được kể lại nhẹ nhàng dưới dạng Nhật Ký. Nó không gây ra sự nhàm chán mà đem lại hứng thú cho người đọc.
Chuyện xoay quanh cuộc sống thường ngày của cô bé Nhiễm Đông Dương.
Tuy tác giả là người lớn nhưng đích thực rất hiểu tâm lý học trò, đem lại sự thoải mái và đồng cảm cho người đọc.
Nếu bạn có một đứa con gái đang tầm lớp 5-6-7, tin tôi đi quyển Nhật Ký Nữ Sinh thực sự là quyển con bạn nên có trong tủ sách
5
354399
2014-10-29 11:26:55
--------------------------
