395936
9277
"Doraemon bóng chày" – là một bộ truyện tranh rất hay của tác giả Mugiwara Shintaro & Fujio Fujiko. Bộ truyện này nói về những trận đấu bóng chày của đội Dora do chú mèo máy Kuroemon làm đội trưởng. Đội bóng Dora có tinh thần đoàn kết rất cao, luyện tập và thi đấu rất nghiêm túc nên cí tỉ lệ thắng rất cao. Đọc cuốn truyện tranh này các bạn sẽ được thưởng thức trận đấu bóng chày giữa 2 đội – Dora và Iriomote Mangroves. Đây là một bộ truyện rất hấp dẫn, ly kỳ trong từng chi tiết khiến các bạn không thể rời mắt. Mình rất thích bộ truyện này.
5
1142307
2016-03-12 17:42:23
--------------------------
220848
9277
Đang đến lúc gây cấn của mùa giải vô địch bóng chày toàn Nhật Bản, trong khi đội Doras đối đầu với đội Akita Orions của Oreemon thì Hyoroemon lại đột nhiên biến mất và nhờ Guriemon thay mình thì đấu. Anh chàng Hyoroemon  tuy nóng tính hay la lối nhưng lại chính là thủ lĩnh khích lệ của cả đội Doras, lại là một cầu thủ chủ lực nữa, khá là thích chú mèo máy này. Không biết Hyoroemon đang ở đâu, mong là tập sau cậu ta sẽ mau chóng trở về cùng Doras giành chức vô địch. Tập truyện rất hay!
5
471112
2015-07-03 07:30:51
--------------------------
216071
9277
doraemon thì quá quen thuộc với tất cả chúng ta rồi ,lúc đầu tôi nghĩ doraemon bóng chày là về truyện dài chắc chán ngắt nhưng sau khi đọc xong tôi có cảm nhận khác,Doraemon bóng chày mang lại cảm giác kịch tính giống như đang xem đấu bóng chày thật vậy,doraemon bóng chày xây dựng nhiều hình tượng nhân vật với những tính cách khác nhau,phân mảng nhận vật cũng rất đều,cả một đội bóng không có thành viên thừa,tất cả đều  cùng nhau cố gắng để đạt kết quả cao.Ngoài ra doraemon bóng chày còn mang lại cho tôi bài học về tình đồng đội,sự đoàn kết,ý chí vươn lên chịu khó không quảng ngại khó khăn của từng thành viên trong đội bóng.Đây là một cuốn truyện rất đáng đọc đối với trẻ em lẫn người lớn!
5
505094
2015-06-27 09:33:02
--------------------------
83215
9277
Câu truyện được bắt đầu bằng cuộc quyết đấu giữa hai đội ÊĐÔGAWA ĐÔRÊ và đội IRIOMOTE MANGROVES . Lúc đầu đội ĐÔRÊ dường như gặp rất nhiều khó khăn bởi sự phán đán và đôi mắt tinh tường của cầu thủ Iriemon và phải chơi trên sân của đối thủ - một sân bóng rất quái dị.Trong khi đó đội chùa                                                                                                                                                YAMAOKUYAMA với sự xuất sắc của thủ quân Monta đã chiếm ưu thế.Nhưng nhờ sự xuất sắc của HYOROEMON và sự may mắn , Đội ĐÔRÊ đã dành chiến thắng.Quả là một cuộc đấu rất gay cấn.Nhưng sau đó đội ĐÔRÊ lại gặp phải một đội rất mạnh là AKITA ORIONS lại còn có sự vắng mặt của HYOROEMON và phong độ đỉnh cao của cầu thủ ÔRÊÊMON.Và kết quả của trận đấu sinh tử này ra sao mời bạn đón đọc tập 19.Mình đọc truyện cũ nên tên các nhân vật không giống lắm.Mong các bạn thông cảm!!! : ))
5
129310
2013-06-25 21:35:54
--------------------------
