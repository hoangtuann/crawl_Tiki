466283
9406
Cuốn sách này đề cập những thương vụ thành công và thất bại rất hay và đặc sắc nhưng bị sự hời hợt của First news khi phát hành không chăm chút kỹ lưỡng cho nên cuốn sách này về font chữ cũng rất tệ cùng với sách tồn kho lâu nên bìa có vẻ lên mốc rồi, cho nên việc đọc một cuốn sách như vậy cũng khá là chán nản.
Bù lại đó thì nội dung cuốn sách quả là tuyệt vời, mình rất thích đọc những thương vụ về thất bại vì dường như những điều thất bại đều có nhiều bài học hơn là thành công thì phải.
3
74132
2016-07-02 00:54:17
--------------------------
266358
9406
 Về chất lượng sách được bao bìa tốt, trang trí đẹp. Trình bày đẹp mắt, logic. Chất lượng phục vụ cũng rất tốt. Với giá này thì quyển sách cung cấp những kiến thức như vậy không phí tiền. Quyển sách này thống kê lại những thương vụ mua bán sáp nhập khá nổi tiếng trên thế giới kể cả những thương vụ thành công và thất bại của các công ty lớn. Qua quyển sách cũng giúp bạn học tập được cách thức và những phương hướng, cách nhận định và quan sát để giúp bạn đưa ra quyết định đúng đắn khi đầu tư.
4
656164
2015-08-14 16:29:21
--------------------------
212695
9406
Thức ra quyển sách là tổng hợp về mười quy tắc cơ bản để thực hiện thành công một thương vụ .Trong mỗi qui tắc có những ví dụ cụ thể về những thương vụ thành công và thương vụ thấy bại . Sau mỗi thương vụ đều có một khung nho nhỏ tóm tắt ngắn gọn tại sao nó thành cồng hoặc thất bại .
Ngoài ra , cuối sách còn có bonus thêm 50 thương vụ khác nữa nhưng nói một cách ngắn gọn và không đi sâu vào phân tích .
Mình chỉ chưa hài lòng về chất liệu sách , có thể bạn sẽ thấy mõi thì đọc lâu , còn về nội dung thì khá ổn .
Xin cảm ơn .
4
554150
2015-06-22 19:23:07
--------------------------
73413
9406
Tuy mình không phải dân kinh tế nhưng đọc cuốn sách này cũng thấy hay hay-Những thương vụ thành công và thất bại, cuốn sách này sẽ đem đến cho bạn nhiều trải nghiệm hơn trên con đường kinh doanh, tuy việc kinh doanh rất đa dạng mỗi lúc mỗi khác không ai giống ai nhưng đây là một quyển sách hay sẽ giúp bạn học hỏi được từ những thất bại và thành công của những người nổi tiếng trên thế giới, chính nhờ những kinh nghiệm này mà bạn có thể hoàn thiện bản thân mình hơn và tránh đi vào những vết xe đổ như thế. 
Một cuốn sách khá hay để tham khảo :)
4
60663
2013-05-08 10:33:39
--------------------------
