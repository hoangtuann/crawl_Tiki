474666
6457
Trước đây mình cũng có mua cuốn sách này rồi, nhưng là mua ở cửa hàng Alphabooks. Mình mua tặng cho một bạn nữ vì bạn ấy cứ than là "sợ ế" hoài. Hồi đó mua chỉ có giảm giá 20%. Nhưng rồi mình lên Tiki kiếm thì thấy Tiki có bán, thậm chí bán rẻ hơn với chiết khấu 25% nên mình đã quyết định đặt mua cũng để tặng cho một bạn nữ khác cũng "sợ ế" luôn. Hiện tại mình đang chờ nhận hàng. Cuốn sách này mình cũng có đọc sơ qua rồi và nhận thấy nó rất hữu ích với những bạn nữ nào mà chưa thực sự tự tin vào bản thân mình và vẫn còn lo rằng mình không có khả năng thu hút phái mạnh. Nó sẽ giúp các bạn nâng cao giá trị bản thân và không còn tự ti nữa.
4
661322
2016-07-11 21:24:17
--------------------------
452002
6457
Được cô bạn giới thiệu cuốn này, mình cũng băn khoăn lắm. Vì dạo này lười đọc kinh khủng, chỉ thích cái gì dễ hiểu, nhẹ nhàng.
Vào Tiki thấy mọi người nhận xét tích cực quá nên cũng liều ôm về. Ai ngờ đọc cả tối không dứt ra được.
Sách mỏng thôi nên đọc khá nhanh, nhưng để ngấm được chắc mình phải đọc đi đọc lại nhiều lần.
Hoàn toàn không phải là những chiêu trò để bạn thoát ế, cuốn sách sẽ giúp bạn tận hưởng cuộc sống đầy màu sắc này. 
Sống cuồng nhiệt, thúc đẩy bản thân, sống hết mình. Mình nghĩ đây là tinh thần mà tác giả muốn gửi gắm.
4
20223
2016-06-20 12:38:10
--------------------------
404436
6457
Cuốn sách thích hợp cho những cô gái đang cảm thấy tự ti về bản thân mình. "Gái khôn không bao giờ sợ ế" cung cấp cho người đọc những lời khuyên về cách sống, cách suy nghĩ, tuyên bố những tuyên ngôn cần có của một người phụ nữ bản lĩnh trong thời đại ngày nay. 
Tuy nhiên, sách hơi lan man, dài dòng, chưa có cái nhìn tổng quát, đánh vào nội dung trọng tâm.
Sách hơi ít nội dung và chưa thể giúp các cô gái thoát ế khi không đề cập đến các khía cạnh khác ngoài tâm lý.
3
275547
2016-03-25 09:47:05
--------------------------
403806
6457
Tuyệt vời! Lúc nhìn thấy bìa sách mình đã rất ưng ý với cái tên và cách trang trí bìa rồi. Là 1 người có cá tính khá mạnh mẽ, văn phong cuốn sách này quả thực rất hợp với mình. Không rườm rà câu lệ, đi thẳng vào vấn đề và có cái nhìn toàn vẹn về những phẩm chất nên có của những " gái khôn" ngày nay. Nội dung có bẻ tương đồng với " con gái phải mạnh mẽ" của Ploy nhưng cũng mang những nét riêng không lẫn với bất kì ai. Những bạn nữ nào muốn hàon thiện tiềm năng của phái nữ hơn nữa hãy đọc thử nhé. Rất hay đấy.
5
675755
2016-03-24 11:37:09
--------------------------
401167
6457
Có những suy nghĩ lạc hướng đã hình thành trong trí não của các bạn gái, sau khi được đọc quyển sách này mình đã có một suy nghĩ tích cực hơn và không còn tự ti về bản thân mình nữa, một cô gái thật sự hấp dẫn là một cô gái biết hướng về những điều tốt đẹp và sự chân thành chính là yếu tố làm nên một tình yêu bền vững, các bạn gái ơi hãy sở hữu cho mình cuốn bí kíp này (với mình nó được gọi là cuốn bí kíp) để giúp các bạn gái tự tin hơn và nhận ra được các giá trị đích thực trong cuộc sống cũng như trong các mối quan hệ xung quanh.
5
1056178
2016-03-20 11:25:17
--------------------------
346158
6457
Mình đã mua quyển sách này cách đây 4 năm rồi, cho đến giờ không biết là mình đã đọc lại nó biết bao nhiêu lần rồi! Mỗi lần đọc lại có những cảm nhận khác nhau. Nó không phải là loại sách chỉ đưa ra " lý thuyết suôn" mà nó còn chỉ bạn cách để thực hiện. Từ ngày đọc và áp dụng những điều trong cuốn sách này, mình đã thay đổi rất nhiều, cuộc sống của mình trở nên tốt hơn. Cuốn sách này đã làm thay đổi hoàn toàn quan niệm sống của mình. Mình đã giới thiệu nó cho nhiều người bạn của mình đọc và nó đã trở thành 1 trong những quyển sách yêu thích của họ. 
5
14237
2015-12-02 00:36:22
--------------------------
341868
6457
Một cô gái sau khi chia tay bạn trai, thông thường thời gian đầu vô cùng đau khổ, hay mất niềm tin vào cuộc sống, kể cả niềm tin về đàn ông, sợ hãi không dám bước bước nữa. Và cho rằng ai cũng như ai. Đánh mất đi cơ hội để đi tìm một nửa của mình thì Gái khôn không bao giờ sợ ế có thể giúp chúng ta thoát khỏi cái mớ hỗn độn đó. Tự tin bước tiếp, cách làm thế nào để tạo cho mình một vẻ ngoài hấp dẫn, một suy nghĩ thoáng hơn về cuộc sống. Sẵn sàng bước qua những đau khổ của cuộc tình cũ, quyến rũ và đầy kinh nghiệm cho cuộc tình sau.
Sách rất hay,hãy đọc nhé.
4
326896
2015-11-23 10:51:20
--------------------------
306210
6457
Xét cho cùng tình yêu để đi đến hôn nhân vẫn là một giai đoạn cực kì quan trọng trong đời người con gái. Vậy tại sao các bạn nữ luôn để con tim hay sự thiếu hiểu biết để quen người không mang lại hành phúc cho bản thân mình. Đây là cuốn sách có thể phần nào giúp bạn lý giải những lý do mà bạn có thể mắc sai lầm và đâu là những điều cần lưu lý khi có ý định quen ai đó. Không phải ai cũng có những cái nhìn tỉnh táo và được gặp những người tử tể. Hãy trở thành cô gái hiểu biết để có được cuộc sống hạnh phúc và tránh mắc sai lầm và cho dù bạn có mắc sai lầm thì vẫn tự tin và bước tiếp ( nhất nhất đừng phạm phải chung sai lầm)
5
65619
2015-09-17 13:46:41
--------------------------
130975
6457
Quyển sách với tựa đề khá hài hước " Gái khôn không bao giờ sợ ế" nhưng nội dung lại ẩn chứ một triết lý sâu sắc: hãy là chính mình và bạn sẽ có tất cả những gì bạn muốn. Quyển sách không chỉ là lời tuyên ngôn về phong cách sống của phụ nữ thế kỷ 21 mà còn là sự khắng định vai trò của một nửa thế giới trong cuộc sống hiện đại. Cuốn sách có ưu điểm là được trình bày ngắn gọn, dễ hiểu (bạn có thể đọc xong hết quyển sách trong chưa đầy 2h). Tác giả xây dựng nội dung khá thông minh với những lời khuyên, những bài tập hành động, những câu hỏi cuối mỗi chương dùng để hệ thống lại kiến thức. Nhược điểm dễ nhận thấy nhất là chương đầu người dịch hơi lan man, dài dòng, không chắc và cuốn hút như những chương sau của quyển sách.
4
398179
2014-10-21 15:48:30
--------------------------
