569405
10983
Em mới mua và đọc hồi sáng .. sách hay lắm ạ :))  M.n nên mua về đọc
5
5139296
2017-04-10 18:20:10
--------------------------
547113
10983
Cuốn này hay mình sẽ sưu  tập bộ 6 quyển ở bìa sách này nhưng chỉ k thích phần tiếng anh lắm kể viết tiếng việt thì được  nhiều nội dung hơn
5
2120669
2017-03-18 22:17:58
--------------------------
536885
10983
Chỉ đọc qua comment của mọi người đã có thể thấy đây là một cuốn sách rất cảm động về tình cảm gia đình. Ý định tìm kiếm một món quà để tặng cho mẹ nhân ngày 8/3 nhưng lại tìm được cho riêng mình 1 quyển trong số đó. Chính tựa bìa đã khiến mình rất ấn tượng, quyết định mua không đắn đo gì cả.
4
208917
2017-03-07 02:24:13
--------------------------
531821
10983
Là cuốn sách hay. Nó làm tôi hiểu hơn về tình cảm cha mẹ dành cho mình từ đó yêu thương cha mẹ hơn. Và nhận ra bao lâu nay mình đã vô tư như thế nào, đã làm cha mẹ buồn bao nhiêu nhiêu lần...
Cảm ơn cuốn sách.
5
2187696
2017-02-26 10:17:14
--------------------------
469372
10983
Nếu được cha mẹ yêu thương thật sự, thì khi bạn đọc được những ngôn ngữ này bạn sẽ hiểu rõ họ hơn, cha mẹ có thể dùng bao nhiêu từ ngọt ngào dành cho con, cùng tấm lòng biển cả của mình để chở che cho con, dù chỉ trên trang giấy nhưng từng dòng chữ vẫn tràn ngập yêu thương khiến mình nhớ cha mẹ da diết. Mình là 1 người cũng may mắn vì được cha mẹ thương yêu, tuy biết rằng không phải ai cũng được như thế, nhưng đừng vì vậy mà buồn phiền vì có lúc chính các bạn cũng sẽ làm cha mẹ, đứa trẻ nào cũng mong muốn được yêu thương nên hãy yêu như các bạn từng mong muốn được như thế. 
5
1048390
2016-07-06 12:28:46
--------------------------
425637
10983
Cuốn sách là những tản văn ngắn, chỉ khoảng hai mặt giấy, được viết theo lối song ngữ. Cuốn sách là những dòng tâm sự của những bậc làm cha làm mẹ gửi đến những đứa con bé bỏng của họ. Cha mẹ luôn là những người yêu thương con cái vô điều kiện và không bao giờ họ cần phải nói ra điều đó, nhưng nhiều khi con cái lại là những người vô tâm, họ quên mất rằng cha mẹ cũng cần được con cái yêu thương. Bố mẹ chỉ được làm bố mẹ khi con cất tiếng khóc chào đời mà thôi.
5
816965
2016-05-05 09:12:33
--------------------------
410593
10983
Sách do Nhã Nam tuyển chọn gồm 127 thông điệp tràn đầy tình yêu thương của mẹ dành cho con, được diễn đạt bằng 2 ngôn ngữ:  tiếng Việt và tiếng Anh.
"Đừng sống quá vội đến mức quên đi không chỉ nơi con đã đi qua, mà còn quên nơi con đang đứng.
Cuộc sông không phải là cuộc đua, mà là 1 hành trình đầy hương vị trên mỗi chặng đường đi qua"
Còn nhiều, rât nhiều yêu thương được gửi gắm trong từng trang, từng trang sách.
Hãy đọc để cảm nhận những yêu thương bình dị, nhẹ nhàng thường xuyên đến cùng ta trong cuộc sống, nhưng chắc gì ta đã nhận ra và biết trân quý
4
357674
2016-04-04 14:26:23
--------------------------
403466
10983
Sau khi đọc cuốn sách này, mọi người hãy tự suy nghĩ lại bản thân mình và lắng động để cảm  ơn cuốn sách này...Hãy mua cuốn sách này  nếu bạn yêu bố , mẹ bạn và hãy suy nghĩ tại sao bà mẹ bạn lại luôn la mắng bạn vì bà mẹ các bạn luôn muốn các bạn hoàn hảo nhất, tuyệt với nhất với ba mẹ, bạn bè, người thân và mọi người. Nếu bạn là người biết suy nghĩ về cho bố mẹ bạn hãy mua cuốn sách này nó sẽ giúp bạn có những suy nghĩ sâu hơn về ba mẹ.
3
1224484
2016-03-23 21:01:31
--------------------------
388605
10983
Đọc xong Thông Điệp Yêu Thương - Con Có Biết mình có cảm giác mình trở nên trưởng thành hơn rất nhiều, suy nghĩ thấu đáo hơn, tinh tế hơn và có chiều sâu hơn. Những thứ hơn đó vô cùng quý giá mà trong cuộc sống chẳng ai đánh rơi để bạn nhặt cả. Mình như thêm yêu thương gia đình, thêm yêu thương bố mẹ, thêm trân trọng đấng sinh thành của mình hơn. Không chỉ có "hơn" mà còn có "bớt" nữa. Đó là bớt ích kỉ, bớt bốc đồng, bớt trách móc cha mẹ hơn. Đúng là không có gì khó bằng việc làm cha mẹ!!!
5
956031
2016-02-29 11:59:59
--------------------------
242537
10983
Ngay từ cái nhìn đầu tiên, tôi đã rất ấn tượng với cái tên "Con có biết", bìa sách rất đẹp, rất bắt mắt và hơn cả là được viết bằng song ngữ( rất giúp ích cho tôi khi trau dồi thêm Tiếng Anh). Ở trong đó chưa đựng những thông điệp, những lời nói từ tận đáy lòng mà những người làm cha làm mẹ muốn nói với con...Một vài trong số đó, có lẽ, tôi hay 1 vài người chưa biết đến hay chưa được nghe nhưng khi đọc xong tôi dường như thấy gần gũi, hiểu được cha mẹ nhiều hơn, biết được những lời mà cha mẹ muốn nói nhưng chưa có dịp hay thấy ngại khi nói với con. Và tôi nhận ra rằng sự tồn tại của tôi đối với người khác có lẽ không có gì đặc biệt nhưng chắc chắn rằng với cha mẹ tôi luôn là một sự kì diệu...
5
140608
2015-07-26 19:31:31
--------------------------
222283
10983
Những cuốn sách "Thông điệp yêu thương" tôi thấy đều có bìa sách rất bắt mắt và cách trình bày nội dung cũng rất hấp dẫn. Tôi đã đọc gần hết bộ thông điệp yêu thương và mỗi cuốn là một cảm xúc. Khi đọc đến cuốn "con có biết" cái cảm giác gần gũi lắm, tuyệt vời lắm và được yêu lắm. Tôi được hiểu thêm về tình cảm của gia đình, của người mẹ...niềm hạnh phúc của một đứa con như tôi. Giá như tất cả chúng ta đều tìm đến cuốn sách này thì sẽ tuyệt vời biết bao nhỉ?
5
292005
2015-07-05 11:38:31
--------------------------
199857
10983
Quyển sách này được thiết kế rất đẹp mắt với phần nội dung được viết song ngữ (gồm tiếng anh và tiếng việt) rất hấp dẫn.
Mỗi trang sách là mỗi dòng chảy tình yêu thương của mẹ dành cho con của mình... Đó là một tình yêu thương bao la, vô điều kiện và tồn tại mãi  mãi.
"Mẹ sẽ luôn ở đây với tình yêu thương dành cho con"...
Khi gấp trang sách lại, tôi hiểu đôi phần về tình cảm của mẹ mà bấy lâu nay tôi khó mà hiểu được...Cảm ơn mẹ đã tạo cho con 1 cuộc sống ngày hôm nay!
5
558557
2015-05-23 13:15:54
--------------------------
197234
10983
Cuốn sách này là quà tặng sinh nhật lần thứ 8 của mình từ mẹ. Bây giờ đọc lại, cảm xúc của mình vẫn vẹn nguyên như thế. “Con có biết mẹ đã yêu thương con rất lâu từ ngày con còn chưa chào đời? Ngay khoảnh khắc đầu tiên mẹ ôm con vào lòng, một trong những ước mơ hồi bé của mẹ đã thành hiện thực.” Khi lớn lên và đọc lại những dòng này, mình thấy thật kì diệu làm sao, sự ra đời của tôi trên cuộc đời này như một phép màu nhiệm với mẹ. Cuốn sách có thể đã phần nào nói được những thông điệp mà mẹ muốn tôi hiểu. Cách bố trí trang cũng như font chữ và giọng điệu của sách khiến mình như nghe thấy tiếng lòng dịu dàng của mẹ :’)
4
507349
2015-05-17 10:47:07
--------------------------
176543
10983
Đúng như tựa sách, cuốn sách này đã mang đến cho đời thông điệp của tình yêu thương. Và lần này thì tình yêu đó còn bao la hơn bao giờ hết - tình yêu của đấng sinh thành dành cho những đứa con bé bỏng của mình. Đọc cuốn sách, cứ như bản thân đang lắng nghe những lời tâm tình của chính cha mẹ mình vậy. Đọc nó để thấu hiểu mọi tâm tư của cha mẹ, đọc nó để yêu thương cha mẹ nhiều hơn.
Không chỉ đọc để tìm cảm xúc mà còn đọc để trau dồi từ ngữ tiếng Anh. Chính vì vậy mà những cuốn sách song ngữ luôn là một lựa chọn hoàn hảo cho mọi người.
5
433873
2015-04-01 13:30:16
--------------------------
157067
10983
đó là 1 trong số những cuốn sách nhỏ nhắn nhg đã đem lại cho tôi những suy ngẫm về mk 1 đứa con và cha mẹ. lời nhắn nhủ nhẹ nhàng của mẹ, 1 điều j đó mang nét mạnh mẽ từ ba, cảm thấy rằng bố mẹ luôn dõi theo mk, bố mẹ sẽ dành cho mk những tình cảm lớn lao mà sau này khi mọi người làm cha làm mẹ hãy học tập đó có lẽ cũng chính là điều tác giả muốn nhắn nhủ đến con mk nên đọc mk thấy nhưu đc bố mẹ thủ thỉ bên tai nhg cũng như thấy đc những vô tâm of mk hix. nhg mk thấy nhìu khi câu văn mang hơi hướng văn học mk thích sụ bình di hơn. nhg dú sao đây là cuốn sách viết cho những ai đã đang làm con
4
540816
2015-02-07 15:55:19
--------------------------
149145
10983
"Công cha như núi Thái Sơn-Nghĩa mẹ như nước trong nguồn chảy ra". Câu ca dao từ xa xưa đã nói lên tình cảm thiêng liêng mà cha mẹ dành cho con cái. Và giờ đây tôi lại một lần nữa được sống trong những cảm xúc chân thật, tuyệt diệu ấy thông qua những câu nói, mẩu truyện ngắn của "Con có biết". Cũng như những quyển khác của bộ sách "Thông điệp yêu thương", tôi rất hài lòng về mặt hình thức với cách trang trí bìa sách bắt mắt, thu hút. Bên cạnh đó sách còn là song ngữ, có thể giúp tôi trau dồi thêm vốn tiếng Anh của mình. Đặc biệt, với quyển sách này, tôi cực kì thích cách đặt nhan đề vì nó như một câu hỏi mở ra cho mỗi người những suy ngẫm khác nhau, giúp chúng ta nhìn lại chính mình, nghĩ về phận làm con phải sống sao để đền đáp lại công ơn nuôi dưỡng của cha mẹ. Một quyển sách hay và ý nghĩa cho tất cả những ai được sinh ra trên cõi đời này!
5
131327
2015-01-12 19:06:00
--------------------------
136369
10983
Ngay từ cái nhìn đầu tiên, tôi đã bị cuốn hút bởi màu sắc tươi sáng, trang nhã và bố cục trang trí khá dễ thương của quyển sách. Nhưng điều khiến tôi say mê và thích thú nhất là do nội dung cũng như thông điệp mà quyển sách này mang lại. Thông điệp yêu thương "Con có biết" là một cuốn sách rất hay nói về tình yêu thương vô bờ bến của cha mẹ.dành cho con cái, từ những cái nhỏ nhặt nhất đến những điều lớn lao, phi thường nhất. Quyển sách mang đến cho người đọc nhiều cảm xúc, vui có, buồn có, nhưng tất cả đều xoay quanh tấm lòng thương yêu, tình cảm dạt dào của cha mẹ.
Ngoài ra sách còn có sự song hành ngôn ngữ Anh-Việt nên rất thuận lợi cho các bạn muốn trau dồi thêm tiếng Anh. Nhất là đối với các bạn đang học xa quê, hãy đọc và cảm nhận để thấu hiểu rõ hơn tình yêu nồng nàn, vĩ đại của mẹ cha. Và hãy để cho tâm hồn mình lắng dịu vào những cung bậc cảm xúc ngọt ngào ấy ...
5
485130
2014-11-20 18:34:22
--------------------------
76181
10983
Con đã biết, ba mẹ yêu thương con như thế nào và dành cho con những gì. Có lẽ nhờ vào cuốn sách mà con biết được những hi sinh của ba mẹ đã dành cho con. Con hồn nhiên, vô tư , trong sáng đến vô tâm. Con không biết ba mẹ đã dành cho con nhiều như thế, nhiều hơn cả những gì con biết từ quyển sách này. Con cảm phục tình yêu thương của ba mẹ, không biết sau này khi lớn lên con có được tấm lòng nhân hậu và yêu thương như ba mẹ đã làm cho con không? Nhưng con dám chắc một điều rằng, không cần đợi đến lúc lập gia đình, sinh con thì con mới hiểu và yêu thương ba mẹ, mà ngay và luôn bây giờ con cũng đã hiểu được tình yêu ba mẹ dành cho con. Điều mà tôi có thể học từ cuốn sách này khi đọc nó xong là " Trân trọng những khoảnh khắc khi được cùng ba mẹ trãi qua những ngày tháng ngày im đẹp, rồi chạy ngay đến mẹ và nói rằng Mẹ Ơi Con Yêu Mẹ Nhiều Lắm".
5
39515
2013-05-21 16:55:18
--------------------------
49315
10983
Những gì cuốn sách này mang lại, rất đơn giản có thể gói gọn trong một từ: yêu thương. Tràn ngập trong những trang sách là tình cảm nồng cháy, sự ân cần, sự quan tâm của những bậc sinh thành dành đến cho những đứa con thân yêu. Mỗi tâm sự trong sách dù nhỏ thôi, nhưng đều chất chứa bao nỗi niềm của nhiều tháng năm, bao ước mơ tốt đẹp cho con cái. Độc giả sẽ hiểu thêm về cha mẹ mình khi đọc cuốn sách, cũng như tìm được cách làm sao để gần gũi với cha mẹ hơn, yêu thương cha mẹ nhiều hơn. Cuốn sách có sự song hành của 2 ngôn ngữ Việt và Anh, nên sẽ gây được sự thích thú lớn cho những độc giả trẻ tuổi. Vừa đọc sách, vừa có được những bài học bổ ích, vừa rèn luyện ngoại ngữ, quả là điều tuyệt vời!
4
20073
2012-12-06 20:52:03
--------------------------
29692
10983
"Thông điệp yêu thương-Con có biết" là một cuốn sách đầy ý nghĩa... Những mẩu truyện ngắn, những câu nói chất chứa tình yêu thương vô bờ của bậc làm cha, làm mẹ...
Đọc để cảm thấy trân trọng những gì mình đang có, đọc để thấu hiểu bao nỗi vất vả của cha mẹ, đọc để cảm nhận tình cảm yêu thương của cha mẹ là vô cùng lớn lao, là vô bờ, là vĩnh cửu.... Đọc để yêu thương cha mẹ nhiều hơn, để sửa sai những lỗi lầm đã mắc phải với cha mẹ, để không bao giờ phải làm cho cha mẹ buồn hay lo lắng!
Cuốn sách vừa có cả tiếng anh lẫn tiếng việt, giúp ích cho chúng ta rất nhiều trong việc cải thiện vốn từ tiếng anh của mình :)
Hãy để đây là một món quà sưởi ấm tâm hồn bạn, hãy để những mẩu truyện ngắn thức tỉnh con người bạn! Đây là 1 cuốn sách nên-có trong trong tủ sách của bạn :)
5
40122
2012-06-07 21:33:07
--------------------------
15471
10983
Đã là cha mẹ thì ai cũng mong muốn cho con mình những gì tốt nhất, đẹp nhất, hoàn hảo nhất. Chẳng màng đến bản thân, chỉ cần con được hạnh phúc, được đầy đủ là đã mãn nguyện lắm rồi. Yêu con và cầu nguyện cho con mỗi đêm từ khi con còn chưa ra đời... Có mắng, có rầy đấy nhưng những lúc như thế cha mẹ cũng có sung sướng gì!!! Cứ thử đặt mình vào vị trí của cha mẹ đi rồi bạn sẽ hiểu!!! Cuốn sách là song ngữ Anh- Việt nên cũng rất bổ ích cho những ai muốn nâng cao trình Tiếng Anh của mình qua những bài viết đầy cảm động.
5
16223
2011-12-06 12:42:56
--------------------------
9998
10983
Một cuốn sách rất cảm động. Đó là tất cả những gì mình có thể nói với quyển sách này. 
Quyển sách là một tổng hợp, là tất cả những gì tốt đẹp mà các bậc cha mẹ dành cho con cái. Là những cung bậc cảm xúc, thăng trầm, ngân vang nhưng luôn kết thúc ở nốt nhạc yêu thương. Là những gì những đứa con cần phải hiểu, rằng ba mẹ chúng đã vất vả như thế nào để nuôi chúng lên người. Là những lúc cãi vã, bất đồng quan điểm,...bởi vì, sở sĩ, gia đình là như vậy.
Bạn nên mua quyển sách này, nó sẽ là món quà tinh thần cho cả bố mẹ và chính bạn. Hãy để những thành viên trong gia đình nhận ra giá trị của nhau, đến một lúc nào đó, bạn sẽ nhận ra, gia đình là tất cả.
5
9348
2011-08-17 19:14:14
--------------------------
