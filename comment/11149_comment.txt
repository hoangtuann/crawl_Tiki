356229
11149
Tác giả đã miêu tả một xã hội thời nay qua ngòi bút sâu cay, đôi lúc chua chát, có khi hóm hỉnh, có khi lại có chút gì đó trần tục. Ngôn ngữ châm biếm cho một xã hội thực dụng, có tiền là có tất cả, có tiền mua tiên cũng được, nhưng nhân vật lại không làm độc giả ghét hay tức mà chỉ thấy buồn cười, một nụ cười mỉm vì đâu đó trong đời sống mà mình đang sống chúng ta cũng đã bắt gặp đâu đó những con người như thế, rất thực tế, rất rõ nét. 
3
324545
2015-12-21 15:06:53
--------------------------
261750
11149
Mình vốn thích truyện cười, nên khi thấy một cuốn truyện cười được quảng cáo là "cười lộn ruột" thì mình không ngần ngại mà mua. Bìa giấy tốt, giấy bên trong cũng tốt. Nội dung truyện không phải không hay đâu, nhưng thực sự đối với mình thì truyện nặng nề quá, câu chữ của tác giả thật sự trần trụi. Vẫn biết tác phẩm có ý nghĩ châm biếm nhưng mình không thể thích được. Câu từ, nội dung truyện thích hợp với bậc cha chú của mình đọc thì hơn, nói chung mình nhận thấy mình không hợp với tác phẩm này.
3
489524
2015-08-11 13:10:00
--------------------------
192909
11149
đời thế mà vui, lối kể chuyện thoạt nhìn chấm biếm nhưng sau khi đọc xong tôi cảm nhận ra nhiều bài học trong cuộc sống, nó được xen lẫn cả bi lẫn hài. Đời thế mà vui sẽ dễ dàng đi vào người đọc ở lứa tuổi tầm trung niên trở lên, bởi những câu chuyện được kể cũng hơi có chút tục xen lẫn thanh. Lê Minh Quốc vốn là 1 đạo diễn vì thế câu chuyện xuyên suốt từng vỡ từng võ 1 và đước sắp xếp 1 cách logic, các tình huống đời thường dân dã từ bàn nhậu đến việc muốn trở thành sao được ông châm biếm 1 cách đầy nghệ thuật với những từ ngữ không bóng bẫy không màu mẽ, đơn giãn và gần gũi tạo sự hứng thú và khơi gợi cho đọc giả. Đời thế mà vui sẽ là 1 trãi nghiệm thú vị cho những đọc giả trung niên!
4
603696
2015-05-05 22:57:24
--------------------------
