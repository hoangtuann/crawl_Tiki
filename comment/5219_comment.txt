455679
5219
Có thể nói đây là tập khá ly kỳ, nếu không nói là hay nhất cho tới giờ. Tập này chủ yếu là suy luận phá án, không có tình cảm hay cảnh lãng mạn giữa Ran và Shinichi. Nhưng tác giả cứ câu giờ vụ băng đảng áo đen hoài mà không đi vào mấu chốt. Mình không thích nhân vật Haibara trong tập này, có phần quá lạnh lùng, rườm rà.
Phần cuối tập này thì còn bỏ ngõ khi Hattori và Hazuka rơi vào thế ngàn cân treo sợi tóc, bị trói và nhốt trong tòa nhà.
4
23847
2016-06-22 19:45:32
--------------------------
395101
5219
"Thám tử lừng danh Conan" là một manga series rất đặc sắc và hấp dẫn của tác giả người Nhật Bản Gosho Aoyama. Cốt truyện là kể lại những vụ án của cậu thám tử học trò Kudo Shinichi, bằng tài phá án, suy luận sắc bén, cậu đã phá được rất nhiều vụ án, nhưng do một lần bám theo hai tên mặc đồ đen nên cậu đã bị chúng cho uống viên thuốc APTX4869 nên cậu bị teo nhỏ lại. Cốt truyện quen thuộc. =)))
Tác giả Aoyama Gosho có nét vẽ rất sinh động và đẹp, lời thoại hay. Mình rất thích bộ truyện này.
5
1142307
2016-03-11 13:47:30
--------------------------
379309
5219
Hình như lần này bọn áo đen hơi nương tay cho Connan thì phải, không thì Connan cũng tiêu rối. Vụ những con búp bê thật là hơi bị xàm một chút, phải nói là đơn giản quá mức tưởng tượng. Hay nhất là lúc Kazuha và Hattori gặp nạn nhưng lại không nói rõ làm sao mà bắt được cả hai nên mình cũng hơi thắc mắc. Khá hài lòng với tập này, nhất là khi người sói bỏ mặt nạ của mình, trong lúc đó, người sói như bị một mũi tên bắn hạ, thế rồi người sói bị gục ngã.
5
13723
2016-02-10 16:28:48
--------------------------
371508
5219
Tập này là chứa một trong những vụ ấn tượng nhất trong Conan . Mình rất thích vụ Hatori và Kazuha thoát chết trong gang tấc. Ở tập này thì conan cũng có rất nhiều câu nói hay khiến chúng ta phải suy nghĩ, bộ truyện này người ta thường nói là để giải trí nhưng mình hoàn toàn không thấy vậy ở đây nó còn có tính giáo dục con người. Và đến cuối kết của cuốn truyện thì thật sự rất hay khi hai chàng thám đã đã bắt được hung thủ nói chung đây là một tập khá hay.
5
1062396
2016-01-19 18:28:44
--------------------------
363672
5219
đối với bản thân mình, Conan như một phần sự sống. Từ hồi lớp năm tuổi thơ mình đã gắn liền với những cuốn truyện tranh và trong đó có Conan. Có thể nói là mình đã rất say mê sự tài giỏi và thông minh đáng nể của anh chàng thám tử này. Và trong tập truyện này cũng thế, sự tài ba của cậu thám tử vẫn càng lúc càng thể hiện rõ nét. Thật ngưỡng mộ. Bên cạnh đó, trong tập này Heiji và Kazuha càng làm cho người hâm mộ thêm thích thú khi cả hai sa vào nguy hiểm. Thật hồi hộp.
3
362041
2016-01-04 19:38:15
--------------------------
354925
5219
Mình mê conan lắm ,tập này thích cực kì . Mình đã có tập 38 này nhưng đã bị mất và bây giờ đã có lại .Chưa nói tới nội dung chỉ hình thức thôi là tiki giao hàng rất nhanh và truyện được bọc bookcare rất đẹp . Còn về nội dung thì siêu lãng mạng , Hejji xém chút đã tỏ tình với Kazuha nhưng khi thoát khỏi nguy hiểm lại chối nhìn yêu cực ,hài vô đối .Wolf Face cực hay nhìn kịch tính như đấu vật trong tivi nhờ vậy mà biết thêm về môn đấu vật và ngón đòn trong môn này . Hay
5
849191
2015-12-18 21:38:09
--------------------------
354857
5219
Đọc sách truyện nước ngoài hiểu biết thêm về văn hoá nước họ. Nhật Bản có cả lễ hội búp bê. Nhưng chơi mà cũng đòi hỏi nhiều kiểu sắp xếp truyền thống như thế thì có vẻ như người Nhật gò ép bản thân họ quá rồi. 
Bọn con nít tiểu học thôi, chưa gì đã yêu đương và nghĩ chuyện chồng vợ. Thật buồn cười nhưng cũng hơi lố. Đội thám tử con nít đó làm mình ngán bộ truyện kinh khủng. Phải chi đừng có tụi nhóc thì hay quá. Nhưng sau này hầu như luôn có một vụ liên quan tới bọn trẻ ở mỗi tập. 
5
535536
2015-12-18 20:07:47
--------------------------
327890
5219
Mình là một fan của Conan . Đã đọc từ rất lâu rồi nhưng giờ mới mua . Tiki có giảm giá nên rẻ hơn so với mua ở nhà sách .
Tập này hay thật đấy ! Thích nhất là câu nói của Conan : " Mặt nạ có thể che được khuôn mặt . Nhưng không thể che hết được tinh thần thượng võ bị mất ! " Tập này tập trung nói về đạo đức nghề nghiệp thì phải . Rất ý nghĩa ! Theo thời gian , con người dần thay đổi , không còn giữ nguyên được ý niệm tươi đẹp của trước đây . Dần nảy sinh lòng tham , làm nên những điều đáng tiếc .
Conan có rất nhiều câu nói rất hay . Bô truyện không chỉ để giải trí mà còn có tính giáo dục con người . Trong những câu chuyện tưởng chừng chư chỉ ở trên trang sách , có thể ta đã từng gặp hoặc từng rơi vào hoàn cảnh ấy .
Thật muốn truyện sớm kết thúc , vì đã quá dài và rất muốn biết những bí mật . Nhưng lại không nỡ , kết thúc rồi thì phải làm sao đây ? Có khi nào giống Doraemon không ? Không có cái kết luôn ? ( cười )
5
558702
2015-10-28 17:33:38
--------------------------
211680
5219
Người sói của Conan tập này quá đỉnh... đẹp trai tài giỏi mà còn khiêm tốn nữa, có một câu mà Ran hỏi anh khi đang che giấy mặt nạ :" anh làm như vậy có khiến bạn gái anh buồn không ?" lúc đó chỉ nghĩ tới Conan vì cũng là giống nhau trong hoàn cảnh bắt buộc phải giấu thân phận của mình. vui nhất là đoạn tìm ra kẻ lợi dụng người sói gây án rồi  cả một nhóm trong phòng bị hạ gục thì Ran xuất hiện dùng ngón võ thần sầu giáng cho tên này một cú kinh hoàng...  phát minh mới của tiến sĩ thì khỏi phải chê rồi... bác Ghosho có tài sáng tạo và liền mạch ghê đọc hoài vẫn không chán... 16000  là phù hợp cho những truyện manga thế này.
4
568747
2015-06-20 22:06:24
--------------------------
210501
5219
Nhiều lúc tự hỏi rằng sao Shinichi có thể tài thế? Mặc dù đã bị teo nhỏ nhưng Shinichi vẫn phá án một cách ngoạn mục. Sự nhạy bén, nhanh nhạy, ứng sử tình huống một cách thông minh và khả năng lập luận tài ba của cậu thám tử trung học này khiến mình rất khâm phục. Ở tập này, nhớ nhất là vụ Heiji và Kazuha bị bắt, gay cấn và hồi hộp đến ngộp thở. Mình lo lắng lắm, Heiji bị bọn xấu đánh ác quá! Thật may là hai chàng thám tử đã hiểu ý nhau và tóm được tội phạm, Heiji và Kazuha được cứu thoát, phù..!
5
543425
2015-06-19 16:14:52
--------------------------
204828
5219
Tập này khá hấp dẫn, mở đầu tập, Conan vẫn bất chấp nguy hiểm để thăm dò tung tích tổ chức áo đen, lúc cậu trốn trong tủ và cửa tủ mở ra, mình đã thót hết cả tim dù vẫn biết trước là cậu không sao cả. Vụ án giết đấu sĩ cũng hay, quá ấn tượng khi chứng kiến hành động của Ran ở đoạn cuối vụ án, đòn đánh quá tuyệt. Vụ án ở cuối truyện khá dễ thương, tuy Heiji và Kazuha bị bắt nhốt và đe dọa tính mạng nhưng như người ta vẫn nói, hoạn nạn thấy chân tình, đồng cam cộng khổ mới càng yêu mến nhau hơn, Heiji thậm chí còn định tỏ tình với Kaz nữa mà lại... thật tiếc quá.
5
393748
2015-06-05 02:45:00
--------------------------
198604
5219
Lúc đầu nhìn bìa cảm thấy bị cuốn hút vô cùng, cứ nghĩ là Conan sẽ làm cao bồi miền Tây nữa chứ. Tập này lại được vào danh sách yêu thích của mình vì có sự xuất hiện của cặp Heiji va Kazuhaaa. Thật là yêu lắm ý, thoát chết cùng nhau, suýt chút nữa Heiji còn nói lời tỏ tình nữa chứ, ông Gosho thật biết trêu người quá đi mà. Nhưng tập này hay nhất phải nói là phát minh của ông tiến sĩ Ausaga, giúp ích được ghê lắm luôn, muốn biết là gì thì tìm đọc và sau đó ôm bụng cười lăn giống mình nha :))
5
530790
2015-05-20 12:21:23
--------------------------
183054
5219
Tập này là chứa một trong những vụ ấn tượng nhất trong Conan. Cực thích vụ Hatori và Kazuha thoát chết trong gang tấc. Trong vụ án này  sẽ xuất một bà luật sư cùng nghề với bà Kisaki nhưng đầy độc ác, toan tính, cố tình hãm hãi, thậm chí định giết Hatori và Kazuha. với tài trí hơn người, một lần nữa bộ đôi Hatori và Conan lại tái xuất giang hồ.Tèn ten! Vụ án này còn pha lẫn một chút lãng mạng dễ thương khi suýt nữa Hatori đã tỏ tình với Kazuha! Ngoài ra, các vụ khác cũng khá hay. Nói chung là không uổng công chờ đợi.
5
558345
2015-04-15 13:06:20
--------------------------
