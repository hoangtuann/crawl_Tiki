210758
9309
Tôi đọc cuốn sách này sau khi đã xem rất nhiều sách và phim về chiến tranh. Và tôi thật sự bị ấn tượng bởi sách. Ngay từ tựa đề, tôi đã vô cùng thắc mắc: "Cát trọc đầu" là gì ? Và sau khi đọc nó, tôi mới càng hiểu thêm về chính vùng quê xứ Quảng đầy nắng gió của tác giả... 
Có 1 điều tôi rất nhớ khi đọc sách, đó là nhà văn yêu cát, nhớ cát đến tận cùng. Mỗi nhân vật ( Nụ, Bá,...) đều có những phút giây bồi hồi nhớ về quê hương, và dĩ nhiên, "Cát" luôn xuất hiện. Cũng như người con vùng miền Tây sẽ nghĩ về sông nước, hay người Bắc Bộ nghĩ về cánh đồng cò bay thẳng cánh với những trưa hè nóng oi ả, người con Quảng Bình nhớ về quê với đặc trưng "Cát".
Mọt điều nữa, đó là sách đã khắc họa chiến tranh dưới một góc nhìn khác hoàn toàn so với đại đa số nhưng cuốn khác - khắc họa nội tâm con người một cách đầy bản năng trần trụi. Đó là cái "Mới ".
4
457579
2015-06-19 20:04:52
--------------------------
165825
9309
Nhờ có chú Vinh mà tôi có niềm vui đọc sách. Đọc sách không biết chán và ham đọc sách. Tôi không còn thích chơi game, xem phim nữa mà hễ rảnh là đọc sách. Đọc xong cuốn Lời thề thấy đã quá rồi. Đọc tới Cát trọc đầu còn đã hơn. Sự thật lịch sử được phơi bày một cách trần trụi không giấu diếm. Thời nào cũng có người tốt và kẻ xấu. Người tốt thì ta cứ yêu quý, trân trọng còn kẻ xấu thì ta cứ tởm, cứ khinh, cứ coi thường. Những con người như Bá thời nào cũng có và phải sống chung với loại người này thật sự là một gánh nặng tinh thần với những con người chân chính như Nụ, Xuân, An. Đọc đến đoạn tiểu đội 4 của Nụ hi sinh tất cả trong buổi sáng đó và hoài niệm của Nụ về những cô gái trong tiểu đội mà tôi khóc ướt hết cả gối. Trân trọng và mãi mãi ghi nhớ công ơn của các chiến sĩ chân chính. "Xẻ dọc Trường Sơn đi cứu nước/ Mà lòng phơi phới dậy tương lai"
4
470181
2015-03-11 09:46:38
--------------------------
121677
9309
“Cát trọc đầu” của Nguyễn Quang Lập  đã bóc trần một phần sự thật đen tối của chiến tranh. Nhờ cuốn tiểu thuyết này tôi phần  nào biết được bên cạnh những người ra trận vì lòng yêu nước, vì quê hương còn có những kẻ như Bá – một con “sâu mọt” vì những toan tính, lợi ích của bản thân mà ra trận để rồi hết lần này đến lần khác  làm lien lụy đồng đội. Đây là cuốn sách viết về đề tài chiến tranh khá chân thật – Nghiệt ngã, tàn khốc, đau thương … Một cuốn sách nên đọc.
4
21730
2014-08-20 20:52:32
--------------------------
61678
9309
Đúng như những lời giới thiệu về cuốn sách, "Cát trọc đầu" có lẽ không thực sự nói về chiến tranh. Đó chỉ là một cái cớ, một bối cảnh được dựng lên để từ đó, tác giả có cơ hội khắc họa rõ nét hơn những tầng sâu cảm xúc của con người. Trong một xã hội loạn lạc, đầy rẫy những nguy hiểm khi cái chết luôn rình rập, mọi nỗi khổ đau, hạnh phúc của con người đều được đẩy lên đến cực điểm. Tác giả đã rất tài tình khi để những nhân vật tự do thể hiện tính cách, suy nghĩ, và chỉ thông qua những nét chấm phá đầy ý nhị, đã tạo nên một bức tranh hiện thực đầy sống động. Cuốn sách này là một lựa chọn thú vị cho những bạn đam mê văn học Việt Nam đương đại.
5
20073
2013-03-03 12:57:47
--------------------------
