288209
10244
Đọc tác phẩm này, tôi quả thật rất ấn tượng. Tác phẩm đã chỉ cho tôi thấy những khía cạnh của tình yêu trong cuộc sống này. Nó không hề đơn giản như chúng ta vẫn tưởng tượng. Không phải cứ mù quáng tìm kiếm là được đâu. Nó đòi hỏi sự hi sinh, lòng tốt, cả lòng dũng cảm nữa. Chỉ những phẩm chất tốt đẹp nhất của con người mới có thể giúp chúng ta tìm kiếm một tình yêu thật sự, một tình yêu sẽ gắn bó với chúng ta cho đến ngày rời xa thế gian này.
5
398353
2015-09-03 14:00:15
--------------------------
