256150
9629
Một tuyển tập các câu chuyện hay nhất về những tình cảm thiêng liêng trong cuộc sống: là tình bạn, tình thân hay cả tình yêu nồng cháy. Ở mỗi câu chuyện là một hoàn cảnh, một số phận, một cuộc đời khác nhau cho ta góc nhìn đa chiều về cuộc sống. Rồi cũng chính từ những câu chuyện ấy, ta nhận được những bài học vô cùng quý giá và quan trọng. Đọc cuốn sách,ta sẽ biết trân trọng hơn, nâng niu hơn những gì mình đang có. 
Cuốn sách xứng đáng được xếp trong bộ sách Hạt giống tâm hồn - một bộ sách hay mà First New Trí Việt dành tặng riêng cho độc giả Việt Nam.
4
413140
2015-08-06 16:56:14
--------------------------
139219
9629
"Nơi Đó Có Tình Yêu" là một cuốn sách rất hay, mang đậm ý nghĩa nhân văn sâu sắc. Cuốn sách có bìa ngoài đẹp, nhẹ nhàng mà cuốn hút, chất lượng giấy in và in ấn rất tốt, màu giấy đẹp, giấy lại còn thơm nữa ;))). Cuốn sách là tập hợp các câu chuyện rất đỗi giản dị, thiêng liêng về tình yêu thương, tình cảm gia đình, bạn bè và tình yêu đôi lứa,... Qua đó nhằm ca ngợi, biết ơn, thêm yêu và thêm trân trọng những người bạn, người yêu, người thân trong gia đình, luôn có một tình yêu đặc biệt với ta, mà chỉ họ mới có. Cuốn sách có tác động mạnh mẽ với người đọc, phù hợp với mọi lứa tuổi.
5
414919
2014-12-06 14:54:16
--------------------------
72992
9629
Mình đã mua cuốn sách này làm quà sinh nhật cho cậu bạn thân của mình nhưng mình cứ tưởng cuốn sách nói về tình cảm đôi lứa chứ! Khi mình hỏi cậu bạn mình đọc thấy hay không thì mặt cậu ấy tiu nghỉu: "Chỉ có tình cha, tình mẹ, tình anh chị em thôi hà, chẳng lãng mạn gì hết, chán chết!" Mình giận quá, giật lại mang về nhà đọc thế là mình có một cuốn sách hay ơi là hay! Mình đọc mà thấy rất cảm động. Vẫn là những câu chuyện về tình cha, nghĩa mẹ nhưng luôn để lại trong lòng chúng ta những cảm xúc sâu lắng, giúp ta thêm yêu những người thân yêu bên cạnh mình! Một cuốn sách rất đáng đọc! Mình thật may mắn khi giành lại cuốn sách hay như thế này từ tay thằng bạn thân!
4
104794
2013-05-05 18:29:25
--------------------------
63844
9629
Cuốn sách này giống như một khúc tình ca có giai điệu quen thuộc, nhưng một khi đã nghe người ta khó có thể nào quên lãng được. Những câu chuyện trong tập sách nhỏ này chạm đến những tình cảm sâu xa trong trái tim mỗi con người. Mỗi câu chuyện ấy mang một màu sắc khác nhau, nhưng tựu chung đều ca ngợi tình yêu thương, tình cảm gia đình, bạn bè và tình yêu đôi lứa. Tất cả đều chỉ nhằm một mục đích: cho chúng ta thấy rằng, tình cảm yêu thương giữa người với người là một tài sản quý giá, đáng được bảo vệ và trân trọng mỗi người, và được yêu thương đã là một điều vô cùng may mắn và hạnh phúc. "Nơi đó có tình yêu" không chỉ là một cuốn sách, nó còn như một hành trình tìm về những ý nghĩa đích thực trong cuộc đời con người.
4
20073
2013-03-17 14:46:55
--------------------------
