558273
6567
Mình không hài lòng lắm về chất lượng của sản phẩm lần này. Bìa sách bị lỗi, giấy mỏng và phần mực in trong sách chỗ thì mờ chỗ thì đậm, có những trang giấy bị nhăn khiến mình không đọc được nội dung. Mình đã định xin đổi lại sách nhưng mình thấy nó khá phiền phức và mất thời gian nên thôi. Mình đã từng đặt hàng bên Tiki 2 lần trước và cảm thấy rất hài lòng vì chất lượng sách tốt nên mình tiếp tục đặt thêm. Tuy nhiên với quyển "Mặt dày tấm đen" này mình không hài lòng chút nào nếu không muốn nói là thất vọng. Vì bản thân mình là người thích đọc sách và giữ sách nên mình hy vọng lần tới  Tiki sẽ cẩn thận hơn trong việc chọn sách và nhà xuất bản để mình để mình có thể tiếp tục đồng hành cùng Tiki.
2
2072240
2017-03-29 20:18:18
--------------------------
469780
6567
Mặt dày tâm đen tác giả Trương Khởi Thánh mình không biết có giống với cuốn của Chin - ning Chu không? hình như là khác nhau về cả nội dung nhưng sao lại đặt tiêu đề giống nhau ? mình được người bạn giới thiệu đọc cuốn này nhưng không biết là mình có mua đúng không nữa . 
Sách dạy về thuật làm người , về tâm lý con người phù hợp hơn với những ai đang đi làm . sách về nghệ thuật kinh doanh chứ không phải dạng triết lý như mình nghĩ . Dù sao thì với nội dung này cuốn sách cũng rất xứng đang có trong tủ sách của mọi gia đình .
5
593209
2016-07-06 20:08:54
--------------------------
439818
6567
Tôi ngỡ ngàng với nội dung của cuốn sách này. Tiêu đề thì rất bí ẩn. Tôi được một người bạn thân chuyên nghiên cứu về Trung Quốc giới thiệu cho cuốn sách này với hứa hẹn sẽ ảnh hướng lớn đến nhận thức về thế giới của tôi. Tò mò, tôi lên tiki đặt hàng ngay. Hôm sau tôi nhanh chóng nhận được sách và tôi ngấu nghiến ngay.
Ngay những trang sách đầu tiên tôi nhanh chóng ghiền cuốn sách này.
Đây là góc nhìn, phương diện rất thực tế đến ngỡ ngàng của cuộc sống. Đọc nó tôi cảm thấy rất xúc động vì nó chạm đến bên trong nỗi đau của mình và chỉ dẫn cho mình một lối đi để thành công. 
Bạn nên sở hữu cuốn sách này sớm.
4
39844
2016-05-31 20:29:53
--------------------------
322003
6567
Cuốn sách này trước đây thời học đại học, thầy giáo quản trị có nhắc đến 1 lần và cứ nhấn đi nhấn lại nội dung của nó sẽ cho ta rất nhiều kiến thức về quản trị. Vô tình lang thang tiki như một thói quen thì mình thấy cuốn sách này có hàng. Quyết định đặt mua và đọc thử thì thấy nội dung sách khá hay. Tuy nhiên do sử dụng khá nhiều cốt truyện ví dụ của Trung Quốc thời xưa nên có đôi chỗ chưa hiểu lắm. Những sách như thế này cần đọc chậm và tư duy, thậm chí đọc đi đọc lại nhiều lần kiểu như Đắc nhân tâm í.
Riêng về tiki, giao hành nhanh, chất lượng tốt, sách dùng bookcare nên được bao bọc sạch đẹp :)
4
650739
2015-10-15 13:08:33
--------------------------
270942
6567
Cuốn sách này tuyệt vời cho tất cả mọi người và đặc biệt là cho nhà lãnh đạo. Cuốn sách dạy cho mình có một phong thái của nhà lãnh đạo, cách đối nhân xử thế, đó là những nghệ thuật tiên quyết để thành công. Cuốn sách được trình bày rất khoa học, đi từ cái nhỏ đến cái lớn, giống như kiểu tu thân - trị quốc - bình thiên hạ vậy. Và biết cách khi nào thì "hậu" khi nào thì "hắc" - nghệ thuật là ở chỗ đó. Cuốn sách tuy được viết lâu rồi nhưng giá trị của nó quả thật khó bì được. 
Sách được xuất bản từ 2012 rồi nhưng mình thấy sách còn khá mới, giấy trắng hơi mỏng nhưng có thể chấp nhận được, mực in đậm rõ nét nên mình thấy khá ổn.
5
74132
2015-08-18 18:53:38
--------------------------
194601
6567
Ban đầu nhìn tựa sách mình có cảm giác cuốn sách này đem đến cho những mánh khóe kinh doanh kiểu bất chấp luật pháp, nhưng vì sự tò mò và sở thích nên đã mua đọc. Kết quả là mình có được cuốn sách tuyệt vời. 
Nó đem đến cho mình sự mạnh mẽ, quyết đoán trong suy nghĩ. Ko chỉ trong công việc mà còn đời sống thường ngày. 
Cuốn này mình ko chỉ đọc 1 lần, 2 lần mà còn hightlight những điểm thú vị trong quan điểm. Tiêu chí đọc sách của mình khá kén chọn vì mình đã đọc khá nhìu sách về kinh tế, mình ko bao giờ chọn những cuốn sách viết những quan điểm gần giống như nhau, ko thích những sách chỉ chăm chăm nói về những lý thuyết tẻ nhạt,..  
Nếu như bạn đã có mục tiêu, ước mơ và cũng có những thứ đang cản trở những ước mơ đó thì bạn nên đọc sách này.
5
485195
2015-05-11 10:23:05
--------------------------
