528805
4895
Mình cực không thích truyện bộ vì dài và tốn nhiều tiền nhưng đây là bộ truyện duy nhất mình sở hữu đầy đủ, Kim Kiền xuất hiện ở Khai Phong phủ khiến cho một Khai Phong đầy uy nghi trong phim trở nên sinh động hơn bao giờ hết, những vụ án kì bí và nhức óc nhưng có cô nàng thì lại trở thành tình huống hài hước mang lại tiếng cười cho đọc giả, và điều quan trọng là tác giả chưa bao giờ tùy tiện xào nấu và thay đổi tính nhân văn trong Bao Thanh Thiên nên không gây ra phản cảm khi đọc. Một bộ truyện hay để xả stress.
5
607767
2017-02-20 22:04:28
--------------------------
522177
4895
Sự thu hút của tác phẩm hoàn toàn là do cốt truyện. Cốt truyện này hoàn toàn khác với nội dung phim bộ Bao Công, sản xuất vào năm 1996 (mà ai cũng biết). Tác giả tài ở chỗ biết sáng tạo, mang làn gió mới vào tác phẩm nghệ thuật kinh điển này. Những vụ án được tác giả kể lại với góc nhìn hài hước, vui tươi hơn, "teen" hơn, không mang nặng những triết lý cuộc đời hay phản ánh 1 cách đa chiều về gia đình, xã hội, đất nước
Do đó giữ được chân độc giả. Nên khuyến khích 1 like cho điều này

Về nhân vật thì tác giả xây dựng quá "hiện đại", mang tính 1 chiều, đậm hơi hướng ngôn tình ngày nay. Nên hoàn toàn không có chiều sâu, hay tồn tài sự phức tạp/mâu thuẫn trong tính cách nhân vật

Nếu bạn đang tìm 1 cuốn sách là giải trí đơn thuần, đây là lựa chọn tốt. Một cách đốt thời gian rảnh hợp lý. Và đây cũng như là 1 sự khuyến khích người trẻ đầu tư vào văn hóa đọc
3
61861
2017-02-09 16:59:34
--------------------------
484112
4895
Truyện này phải công nhận tác giả có trí tưởng tượng phong phú, sáng tác có kết nối với cốt phim thật , truyện rất hài hước, nội dung hay và một dàn nhân vật ngộ nghĩnh độc đáo. Mặc dù chị nữ chính có vẻ hơi ngơ ngơ về tình yêu nhưng lại rất tài giỏi, rất nhiều lần" chó ngáp phải ruồi" mà giúp Bao đại nhân phá án thành công. Mình đọc mà không ngớt cười, cười đến rụng răng luôn. Một Bao đại nhân liêm khiết chính trực, một Công Tôn Sách đa mưu túc trí, “gian xảo” vô cùng, một Ngự Miêu Triển Chiêu đẹp trai ngời ngời, nghĩa khí đầy mình, là “Idol” của cả thành Biện Lương, một Kim Kiền miệng lưỡi dẻo quẹo, chuyên gia nịnh bợ, ham tiền, háo sắc, mê giai đẹp
5
1704947
2016-09-20 20:32:24
--------------------------
471975
4895
Một câu chuyện hài hước về cô nàng ham vui, hám tiền, một con người không kiên định gió theo chiều nào thì theo chiều ấy, rất thích nịnh bợ kẻ khác vô tình xuyên về thời cổ đại
Tại đây cô đã gặp được rất nhiều nhân vật mới Bao Thanh Thiên Bao Chửng, Triển đại nhân văn võ song toàn, Công Tôn ' gậy trúc' bụng dạ âm hiểm, Bạch thử ngũ gia nóng tính nói nhiều,...
Cô nàng chẳng có điểm nào tốt đẹp này không ngờ lại có thể khiến người tài giỏi như Triển đại nhân mê đắm.Câu chuyện này không chỉ xoay quanh vấn đề tình cảm mà còn xen kẽ sự xuất hiện của những vụ án gay cấn nghiêm túc dưới cái nhìn tếu táo của tác giả
Truyện được tặng postcard rất dễ thương, minh hoa bìa cũng đáng yêu, chất lượng giấy trắng của nanu thì miễn chê.Nói chung là một cuốn truyện hay đáng đọc
5
889851
2016-07-09 08:09:16
--------------------------
469125
4895
Mình vốn chẳng có mấy cảm tình với truyện xuyên không còn đồng nhân nhưng đây là một ngoại lệ. Truyện dài cơ mà hay không tả nổi. Truyện cực kỳ hài nhưng không phải kiểu hài hước gượng ép hay gây cười rẻ tiền mà là kiểu vừa hài vừa sâu sắc vừa kịch tính ấy. Triển Chiêu, Bao đại nhân, Công Tôn tiên sinh là những nhân vật rất quen thuộc với mỗi chúng ta nhưng qua truyện này lại rất mới mẻ, khác lạ, có phần đáng yêu nữa. Nữ chính Kim Kiền hơi dị tí nhưng mình vẫn thích kiểu cá tính và ham tiền như thế này.

5
1324806
2016-07-06 00:29:37
--------------------------
453544
4895
Mình rất mê bộ phim Bao thanh thiên, và ngay khi biết truyện này dựa trên các vụ án của Bao thanh thiên, mình đã mua luôn. Về hình thức, mình khá hài lòng, giấy của truyện rất mịn, bìa đẹp, bookmark dễ thương. Về nội dung thì không thể chê được, ngoài việc phá án, truyện còn đan xen những tình tiết hài hước, đan xen cả tình cảm nên truyện trở nên rất thú vị, sinh động chứ không chỉ có việc phá án khô khan. Đọc truyện mà mình cũng muốn xuyên không luôn, rất ghen tị với Kim Kiền, được anh chàng Triển Chiêu tài giỏi, anh tuấn bảo vệ, để ý. Bạch Ngọc Đường cũng được miêu tả khá tốt, các vụ án cũng được tái hiện chân thực, chính xác. Tóm lại, mình thực sự hài lòng với truyện này, hy vọng tác giả sẽ sáng tác thêm nhiều tác phẩm nữa.
5
562321
2016-06-21 12:06:17
--------------------------
452068
4895
Bộ truyện này chống chỉ định đọc khi đang ăn cơm, uống nước và ở chỗ đông người. Vì nó sẽ làm bạn mất hết hình tượng!!!
Nhân vật nữ thông minh, mưu mẹo, ham tiền, ham ăn ,nói chung là vô liêm sỉ
Nam chinh Triển Chiêu ngọc thụ lâm phong, tiêu sái anh hùng, soái ca của mọi thời đại, ấy mà lại thích cô nàng quỳ gối trước đồng tiền kia!!
Thể loại xuyên không thì đã quá quen thuộc nhưng bộ này đặc biệt độc lạ, nữ phẫn nam trang cũng không còn mới nhưng chưa từng thấy nữ chính nào phẫn nam trang lại giống như vậy!!!!
5
596518
2016-06-20 13:47:57
--------------------------
443606
4895
Nội dung hay, rất hài. Nữ chính bá đạo :3 Mỗi lần tới khúc Kim Kiền và Triển Chiêu đều rất buồn cười :)))) Có điều khúc cuối của Ô bồn án, mình không hiểu tại sao Bao Công biết Kim Kiền giả làm Lưu Thế Xương. Sách kèm thêm 4 cái postcard rất dễ thương :3 Tuy nhiên nghe nói tới 8 tập truyện mới hết nên hơi nản :((( Mình mua 1 lúc 5 tập luôn nhưng tập 3 có vài trang có mấy vết vàng vàng trông khó coi lắm cơ, thật sự không hài lòng chút nào. Và Tiki nên cho khách hàng hẹn giờ giao hàng chứ không phải giao theo tuyến nữa :((( Vì Tiki cứ giao hàng vào giờ mình bận nên mấy lần không nhận được hàng, phải nhờ người thân nhận dùm, theo mình thì tự tay nhận hàng sẽ vui hơn. Mong Tiki khắc phục 2 điều này.
Vốn là mình sẽ cho 5 sao nhưng do hình thức chưa được tốt nên chỉ cho 4 sao thôi.
4
1173119
2016-06-07 09:18:59
--------------------------
438385
4895
Hôm thứ 4 có ghé tiki mua hàng tình cờ nhìn thấy bộ này. Ngay lập tức bị hấp dẫn. Trang bìa vé hình chibi dễ thương thiệt đó. Nhìn qua là biết bộ này vui rồi. Mình lại là một người có niềm đam mê lớn với tiểu thuyết ngôn tình. Vì thế, đã gặp được bộ yêu thích là không thể bỏ qua. Liền vung tay đem "em" nó về nhà. Thật kì diệu, thật đúng là "tình yêu sét đánh" luôn đó. Nhận hàng xong, dành nguyên một buổi chiều để đọc. Đọc xong lại hối hận mình đọc nhanh quá. Mới đó mà xong mất rồi. Truyện rất hay, rất dễ thương. Và giờ mình chuẩn bị rinh thêm em tập 2 về. Đọc xong tập 2 sẽ cho nhận xét toàn tập luôn. "Hóng tập 2"
5
610391
2016-05-29 18:51:47
--------------------------
431152
4895
Truyện này nếu đọc giải trí thì cũng được, nhẹ nhàng, tình tiết dí dỏm. nhưng nếu nói về để lại ấn tượng trong đầu người đọc thì vẫn chưa, đối với mình thì mấy vụ án trong truyện vẫn chưa tạo được hấp dẫn lắm, nhưng tính ra nó vẫn không phải thiên về trinh thám nên không thể trách được. tóm lại có thể nói tập này chắc chỉ là mở đầu nên không có gì gây ấn tượng về tình tiết và tình cảm của 2 nhân vật chính, mong là về sau có thể thấy được các khía cạnh tính cách khác của kim hiền :)
4
780955
2016-05-16 20:08:43
--------------------------
430203
4895
Mình tình cờ biết được bộ truyện này là từ 1 con bạn học ngồi cùng bàn kế bên! Do cứ thắc mắc thế quái nào mỗi lần thấy tới giờ nó ôm cuốn này là y như rằng không biết trời trăng mây gió là gì... =))) lâu lâu còn cừoi khằng khặc kiểu thần kinh tự kỉ! Thế là mình cũng hóng hớt ngó qua :)) đập vào mắt bấy giờ là cái bìa sách nhân vật chibi siêu dễ thương luôn! Lúc đó ấn tượng thật thế là tìm mua đọc thử! Thật lòng mà nói đây là cuốn đầu tiên thể loại TQ ngôn tình mà mình đọc đến bấn loạn như vậy ( trước giờ chưa từng quan tâm đến ngôn tình TQ). Từ cách hành văn hài hước, tỉ mỉ, ngôn từ phong phú, nhân vật cá tính kinh điển,... Khỏi nói cho sách với cả tác giả, ngừoi dịch ngàn sao luôn! Tiki giao hàng nhanh đóng gói cẩn thận mua trên này còn đc giảm giá không mắc như ở ngoài! Cám ơn tiki nhiều lắm!
5
619354
2016-05-14 18:43:43
--------------------------
428356
4895
Mình nhận xét về phần nhược điểm trước :
- Về phần in có một số trang đầu mực in bị lem chút xíu, chữ thì vẫn đọc rõ nhưng thẩm mỹ không được như ý.
- Về phần nội dung: có một vài chỗ in thiếu chữ, tuy chỉ là hạt sạn nhỏ thôi nhưng mình cầu toàn nên cũng không hài lòng lắm
Về ưu điểm:
Truyện tuy bựa do nhân vật chính yêu tiền, ham sống, sợ chết đã vô tình ăn may trong mấy pha cứu người và được tôn làm nghĩa hiệp một cách vô cùng trái ngang. Nhưng truyện vẫn có kịch tính, các vụ án được Bao công xử rất hay xen lẫn với các tình tiết hài hước. Kinh nghiệm cho ai đọc truyện này là sẽ trao dồi được kỹ năng tóm tắt nội dung câu chuyện nhưng lại rất bao hàm, chi tiết và có điểm nhấn (học tập từ kỹ năng ngôn ngữ của nhân vật chính Kim Kiền). Có điều theo mình thấy thì có vài đoạn kể văn phong hơi lỏng lẻo, hơi nhàm nhưng dầu sao cũng là chấp nhận được trong tổng thể cuốn truyện.
Nói chung là hay và hài lòng!
4
845638
2016-05-11 07:51:06
--------------------------
417162
4895
      Truyện này hay, mình thích cách miêu tả nhân vật của tác giả nhưng mình không thích cách tác giả miêu tả Công Tôn Sách, mình cực thích Công Tôn, vậy mà qua đây Công Tôn bị miêu tả nhưng là ông già râu bạc vậy á. :((((
     NHưng mà ngoài Công Tôn ra thì những nhân vật khác đều được miêu tả rất ok ( ngoại trừ Kim KIền :). Nhất là Triển CHiêu, được miêu tả bao ngầu luôn. 
    Các tình tiết trong truyện rất thú vị, cực hài, nhưng mà khuyết điểm là truyện đôi khi miêu tả những cảnh phá án hay tìm kiếm thông tin này nọ rất khó hiểu, nhất là những lúc phá án, lộn tùng phèo hết luôn, phá án cũng không hợp lý lắm. 
    Nhưng mà truyện rất hay, thú vị.
4
790110
2016-04-16 13:17:56
--------------------------
377722
4895
Vốn đã đọc bộ truyện này từ lâu nhưng khi muốn mua mà sách lại hết hàng mất nên tôi cực kì vui mừng khi cuốn sách này tái bản . Đảm bảo với những ai chưa đọc cuốn sách này là một tác phẩm siêu gây cười . Tiếng cười đó có thể từ Kim Kiền cô gái tính tình y chang đàn ông yêu tiền hơn mạng sống . Tiếng cười đó có thể bắt nguồn từ Triển Chiêu chàng trai luôn bị kim kiền xoay như chong chóng . Các vụ án được tác giả miêu tả rất sống động không hề khô khan nhạt nhẽo tí nào
5
337423
2016-02-03 10:02:07
--------------------------
346333
4895
Mới nhìn cái tựa truyện là mình đã muốn nhào vô mua rồi. Coi phim Bao Công từ nhỏ với đủ loại version đã khiến mình trở thành fan trung thành của Bao Công, Chiển Chiêu và Công Tôn Sách. Mình rất hài lòng vì truyện rất vui, rất hay, cũng rất kịch tính. Với lại được tặng mấy cái bookmark rất đẹp, làm mình ôm truyện mà cười toét miệng. Đọc truyện này để xả sì trét thì hết sẩy, cười lăn ra đất, đau cả bụng. Mà mấy đoạn phá án thì đọc hồi hộp thấy sợ.  Khuyến khích bà con "nhảy hố" chung với mình nha!!!
5
860942
2015-12-02 13:32:24
--------------------------
334527
4895
Theo hành trình đi lên làm nhân viên công vụ của Kim Kiền- cô gái từ thế kỉ 21, Âu Dương Mặc Tâm đã rất thành công xây dựng hình ảnh Khai Phong phủ với các nhân vật quen thuộc:  Bao Thanh Thiên, Công Tôn tiên sinh và trên là Triển Chiêu (My love <3 <3) Truyện đầy đủ các án nổi tiếng của lão Bao nhưng lại theo một cách xử lí khác, hài hước và vui nhộn hơn. Tình tiết thú vị, nhân vật hài hước, câu văn hấp dẫn đã tạo nên sự thành công của DKPPLNVCV <3
5
443615
2015-11-09 18:05:18
--------------------------
316189
4895
Đến phủ Khai Phong làm nhân viên công vụ. Từ tên truyện đã biết có liên quan đến các vụ án của lão Bao mà ai cũng thuộc nằm lòng diễn biến và kết quả. Thế nhưng khi đọc truyện mình vẫn thấy vô cùng thú vị vì cách xây dựng nhân vật của tác giả. Mỗi nhân vật, mỗi tính cách, mỗi tình tiết luôn mang lại sự vui vẻ, thú vị mà khi đọc mình không bao giờ ngừng cười được.  Đây là một cuốn sách vô cùng tuyệt vời trong vô số những tiểu thuyết mà mình từng đọc.
5
845104
2015-09-30 13:09:39
--------------------------
312567
4895
Đây không phải là bộ tiểu thuyết đầu tiên mình đã đọc, nhưng lại là tiểu thuyết lần đầu tiên mình bị ấn tượng mạnh mẽ và mê mẫn quên sầu, nhất định quyết tâm mua về lưu trữ cho đủ bộ mới được. ^^
Nói về nữ chính, aiza, quả thật là một Kim Kiền hiếm có khó tìm. Bản thân là nữ nhi nhưng đọc cứ có cảm giác chẳng phải nữ, ngoại trừ một điều để nhận biết đó là nàng ta cực kì mê trai đẹp. Ngờ đâu nàng lại xuyên không về thời Tống, ngờ đâu lại lưu lạc trôi dạt được bước chân vào Khai Phong Phủ gặp được soái ca Triển Chiểu, ngày ngày điều được nhìn thấy dung nhan tráng lệ ấy thì còn gì bằng. 
Lại nói, đọc truyện này để biết thêm "mặt trái" của Công Tôn Trúc Tử và Lão Bao là như thế nào. (haha) Đảm bảo mọi người sẽ sock cho xem ^^ Còn về Tứ đại hộ vệ của Lão Bao thì mỗi người một vẻ, cơ mà đọc đến phần ngoại truyện của tập 2, lại thấy có mùi "đam mỹ" trá hình (haha) vì cả bốn người này đều rất "ái mô" Triển đại nhân, vì một bữa cơm tất niên với Triển Chiêu mà phải bỏ 40 lạng bạc để cầu cứu Kim Kiền, thì quả thật....
Tóm lại, bạn nào có ý định mua thì nên mua ngay kẻo xẩy, bạn nào không có ý định mua... thì cũng nên mua kẻo về sau hối hận không còn kịp =)))))))))
5
475548
2015-09-21 16:27:28
--------------------------
300144
4895
Mình biết tác phẩm này qua mấy đứa bạn trong lớp nghe chúng nó bảo là hay lắm lên facebook tra thì thấy khá là thú vị nhưng vì truyện có khá nhiều tập nên mình cũng hơi tiếc chút giờ thì ôi thôi rồi, mình bị nghiện nặng luôn đó!.Cách mà  Âu Dương Mặc Tâm xây dựng tính cách cho những nhân vật chính phải nói là siêu sáng tạo khác hoàn toàn với khai phong phủ bản nguyên tác truyền hình. Bìa sách thì nhìn rất vui mắt với sự xuất hiện của hai bạn nhỏ Miêu Miêu và Kiền Kiền thêm mấy tấm postcard đáng yêu nữa chứ ^^
5
536908
2015-09-13 19:16:38
--------------------------
291810
4895
Tập 1 của Đến phủ Khai Phong làm nhân viên công vụ rất hay. Cách viết rất hài hước, tính cách của nhân vật được khắc họa rất nhạy bén, thú vị và dễ thương. Các nhân vật trong Khai Phong phủ không còn vẻ nghiêm nghị khó gần nữa mà cũng trở nên rất dễ thương. Cốt truyện rất sáng tạo, nhân vật Kim Kiền cực đáng yêu và có những phát ngôn vô cùng "bá đạo". Tuy nhiên bên cạnh đó cũng có một số chi tiết hơi "phi logic" như Kim Kiền là người hiện đại bị rơi vào quá khứ rất xa xưa nhưng lại thích nghi khá tốt, hơn nữa khao khát trở về nhà dường như không có, 2 vị sư phụ nhận đồ đệ hơi dễ dãi, thậm chí chưa biết gì về Kim Kiền cả...
Tuy vật, mình vẫn rất thích bộ truyện này, mang tính giải trí rất cao. 

4
18866
2015-09-06 19:02:00
--------------------------
282684
4895
Bộ này mình đọc đi đọc lại bao lần mà chưa thấy chán, lần nào cũng cười ngoác mồm trước các tình tiết trong đây. Kim Kiền là một trong số ít nữ chính có tính bựa mà mình được biết, hám tiền lại còn nhát chết, nhưng nếu nhan sắc của Triển Chiêu gặp nguy hiểm là chả sợ gì nữa luôn. Còn lí do vì sao thì hài hết biết
Triển Chiêu đúng tính nam hiệp, yêu chính nghĩa, lại còn dễ đỏ mặt. Nam chính đáng yêu như này cũng hiếm hà, chứ các tác phẩm khác toàn nam thần thôi
Dàn nhân vật phụ, từ Bao Công, Công Tôn Sách, quần chúng nhân dân, cũng đều rất thú vị, rất bà tám

Nói chúng, rất thích tác phẩm này của má Tâm, mỗi tội sao má kéo dài thế, phần kết cứ xa xăm nơi đâu, ôi~


5
259894
2015-08-29 12:43:13
--------------------------
281729
4895
Một cuốn tiểu thuyết ngôn tình nhẹ nhàng nhưng không kém phần hài hước và thú vị. Nam chính và nữ chính đều rất đặc biệt. Kim Kiền dường như là một trong những nữ chính đặc biệt nhất trong ngôn tình. Một cô gái cực kì mê tiền và nhát gan. Triển Chiêu mang lại cảm giác ấm áp và có thể nói là "ngố". Một tác phẩm phù hợp cho những ai muốn thư giãn sau một ngày làm việc vất vả. Đọc mãi không thấy chán, thấy nhàm. Những vụ án li kì, cộng thêm các tình huống dở khóc dở cười khiến người đọc không thể dời mắt khỏi trang sách.
5
350206
2015-08-28 17:10:23
--------------------------
279795
4895
Dạo này toàn bị stress do học hành căng thẳng, muốn tìm 1 cuốn sách ngôn tình nhẹ nhàng mà hài hước thì được giới thiệu cho DPKPLNVCV, qua nhiên ko thể nào thất vọng được, rất hay, rất nhẹ nhàng và hài hước, thích nhất là cái cảnh Triển Chiêu lúc nào cũng quan tâm đến Kim Kiền ^^. Mình chưa từng xem bao thanh thiên nhưng khi đọc truyện này thì lập tức đi tìm hiểu xem ông là người thế nào, nhờ có bộ truyện này mà mình bắt đầu thích tìm hiểu lịch sử trung quốc.
5
462792
2015-08-27 09:27:49
--------------------------
279446
4895
Mình là một con mọt tiểu thuyết Trung Quốc, đọc qua khá nhiều thể loại, cực ghiền truyện xuyên không, cổ đại và sau khi đọc Đến phủ Khai Phong làm nhân viên công vụ, mình khẳng định luôn đây là tác phẩm mà mình thích nhất! Vốn mình vô tình nghe qua tiêu đề dài lê thê của truyện, cảm thấy tò mò và tìm đọc trên mạng để rồi lọt hố luôn, không dứt ra được, đọc rồi mà vẫn phài tìm mua ngay để rồi cứ lâu lâu lại giở ra đọc lại, có nhiều chỗ thuộc lòng luôn ấy. Thực sự rất rất ưng, đọc hoài không chán luôn ấy, bảo đảm với các bạn, đã đọc tập 1 là phải đọc tiếp cho tới hết các tập sau!
5
60677
2015-08-26 20:24:48
--------------------------
275203
4895
Kim Kiền, người như tên, quỳ gối trước đồng tiền, hehe, cơ mà mình thích ! Mà không chỉ Tiểu Kim, Tiểu Miêu, Công Tôn gậy trúc, Bao mặt đen hay Bạch Thử cũng cute quá mức. 
Đây là tập truyện trinh thám, hài hước, ngôn tình đầu tiên, và duy nhất mình đọc tới giờ ( kiếm hoài mà không còn nữa), mô típ xuyên không nhưng hoàn toàn không nhàm chán ! Bao Thanh Thiên thì mình mới xem phim, đọc truyện cũng là lần đầu mà hay quá chừng luôn ^_^, hóng - ing !
5
186614
2015-08-22 18:33:00
--------------------------
258480
4895
ban đầu mình không dám đọc bộ này vì thấy nó tận 7 tập, mỗi tập lại dài lê thê nên nghĩ sẽ rất chán. hơn nữa nếu đã muốn biết về bao thanh thiên thì thà xem phim còn dễ hiểu hơn. nhưng thật sự sau khi đọc hết 5 chương đầu, mình đã xác định khi nào chuyện được xuất bản thì sẽ mua đủ 7 tập đó. tuy xuyên không là mô típ chẳng mấy mới lạ, các án trong bao thanh thiên đều đã biết chước nhưng quả là tài nêm nếm của âu dương mặc tâm rất thần kì, làm cho mình đọc mãi không bỏ ra được. chuyện vừa hài hước dí dỏm vừa gay cấn kịch tính. mình nghĩ rằng ai sau khi cầm chuyện lên rồi cũng sẽ như mình, CUỒNG chuyện này luôn.
hóng tập 5-ing.
5
491865
2015-08-08 16:08:33
--------------------------
251981
4895
Truyện này nên xếp vào Đam mỹ thì đúng hơn ngôn tinh. Nội dung truyện không quá đặc sắc, chỉ là viết lại những vụ án kinh điển của phủ Khai Phong, vấn nhân vật cũ, tình tiết cũ nhưng cách viết lại có gì đó cuốn hút vô cùng . Văn phong của Âu Dương Mặc Tâm có chút tưng tửng, hiện đại, dễ đọc. Tuy biết trước thủ phạm mỗi vụ án nhưng mình vẫn thấy có chút hồi hộp, tò mò khi theo các hành trình phá án. 
Kim Kiền - nữ chính mang đủ tính xấu  rất đỗi thông thường ở thời hiện đại  nhưng vẫn rất dễ thương, cá tính. Bên cạnh nữ chính, các nhân vật phụ cũng khá đặc sắc, xuất hiện cũng vô cùng hợp lý.


5
625177
2015-08-03 11:50:29
--------------------------
248135
4895
Bìa truyện vẽ khá dễ thương. Những nhân vật và vụ án của Bao Thanh Thiên thì không hề xa lạ với chúng ta. Nhưng khi đọc bộ truyện này cho ta nhìn những nhân vật và câu chuyện ấy dưới một góc nhìn khác, góc nhìn hài hước hơn, mới mẻ hơn. Đó có lẽ là nhờ nữ chính Kim Kiền không có ưu điểm như nữ chính của những truyện xuyên không khác như: xinh đẹp, giỏi giang, tài giỏi, thông minh... chỉ được cá mê trai, mê tiền, lém lĩnh và những suy nghĩ khá tếu. Đôi khi đọc làm ta phải cười không ngừng được.
4
472239
2015-07-30 17:50:01
--------------------------
240870
4895
Lần đầu gặp nữ chính, đã không xinh đẹp, lại còn mê trai, tham sống sợ chết, mê tiền, nói thông minh thì không hẳn đúng mà gọi là tkhôn lỏi thì đúng hơn....Quả thật đọc mà không thể ngừng cười. 
Nhân vật Triển Chiêu thật siêu cấp đáng yêu, thêm một Công Tôn cao thâm khó dò, lại một Bao Chửng đường đường chính trực....kết hợp với một Kim Kiền chuyên dính vào rắc rối, tạo nên một phủ Khai Phong nhộn nhịp và sống động. 
Chắc chẳng ai như mình,mua tập 2, 3, 4 rồi mới quay lại mua tập 1, giờ thì ngóng dài cổ tập 5!!
5
291719
2015-07-24 23:22:03
--------------------------
219112
4895
Mình thấy Đến phủ này cũng lâu rồi, review cũng rất tốt luôn, nhưng mà ngại nỗi nhìu tập với lại cốt là Nữ giả nam ấy nên cũng không hứng thú lắm. Một ngày đẹp trời thì quyết định đặt mua, trời ơi, phải nói là không hề uổng phí luôn. Kim Kiền là nữ chính mà mình thích nhất, ấn tượng nhất từ hồi đọc ngôn tình tới giờ, siêu ham tiền, siêu trả giá, siêu nịnh bợ, siêu sợ chết.. toàn là những 'ưu điểm' mới lạ không. Mỗi tập chỉ nói về 1 vụ án , nhưng không vì thế mà kéo dài lê thê nhàm chán, đọc DPKPLNVCV giúp mình hiểu rõ hơn về những vụ án của Bao Công, mà trong đó cô mắt hí Kim Kiền này cũng góp phần không hề nhỏ. 
Đợt này mua tái bản có bookmark, dễ thương thôi rồi luôn, cảm ơn tiki nhiều lắm ^^
5
187302
2015-07-01 09:40:40
--------------------------
218253
4895
Truyện hay, tình tiết hấp dẫn, đọc rất vui, dù là viết trên những câu chuyện xử án đã biết rõ. Có nhiều trường đoạn không cần thiết và lặp đi lặp lại như mô tả Triển Chiêu. 
Đọc xong tập 1 sẽ mua liền tay tập 2. 
Nữ nhân vật chính đọc mà cảm tưởng giống giống như Tiểu Bảo vậy, ham tiền háo sắc nhưng có chính nghĩa. Đặc biệt là miêu tả Triển Chiêu như thiên thần vậy ai gặp cũng ngưỡng mộ. Còn Công Tôn Sách gian gian sao đó, hehehe. Mỗi người 1 vẻ làm nên bộ truyện hay.
4
632582
2015-06-30 14:54:49
--------------------------
217851
4895
Truyện này hay cực luôn í ! Mặc dù không thích đọc yếu tố xuyên không nhưng khi cầm sách trên tay thì không thể dứt ra được luôn T.T Bìa tái bản đẹp hơn rất là nhiều, hầu như không còn lỗi chính tả cũng như văn phong. Về nội dung thì hay thôi rồi, cứ tưởng nhân vật Kim Kiền sẽ bị dư thừa, nhưng không, sự xuất hiện này lại bổ sung cho truyện những tình tiết rất hài và thú vị, cũng như tạo sự mới mẻ cho cốt truyện Bao Thanh Thiên đã rất quen thuộc. Câu văn của tác giả cũng rất rất hài, đọc mà không ngậm lại được mồm luôn í. Mong các cuốn sau mau mau tái bản nhé !
5
295019
2015-06-30 09:07:09
--------------------------
212772
4895
Tôi cực kì yêu thích cuốn sách này,cả hình thức lẫn nội dung.Về hình thức Cuốn sách tái bản được phủ lên màu nâu nâu nhạt nhạt rất hợp(thức là mình không thích loại bìa màu trắng trước đây) các nhân vật Kim Kiền,Triển Chiêu,Công Tôn Sách,Bao Đại Nhân vẽ chibi rất dễ thương(nhất là Miêu ca ca ) .Về nôi dung thì khỏi bàn,dưới ngòi bút trào phúng của Âu Dương Mặc Tâm các nhân vật được khắc họa sống động,đúng chất lun(với lại khi đọc sách mà cứ nhớ lại bộ phim Bao Thanh Thiên hồi nhỏ từng coi :3 ) vì đã coi phim rùi nên tôi cứ nghĩ nó sẽ rất nhàm chán nếu đọc lại nhưng không,nó cực kì thu hút,những vụ án cứ tưởng chừng như đã cũ qua nhòi bút của Âu Dương Mặc Tâm lại rất mới mẻ,sự xuất hiện của Kim Kiền đã tao nên những tình tiết gây cười,làm náo loại Khai Phong Phủ tưởng chừng không có tiếng cười.Đọc mà cười quặng cả ruột,hãy mua ngay nếu bạn còn chần chừng,rất đáng đọc nhất là các bạn thích thể loại hài hước!
5
505094
2015-06-22 20:59:26
--------------------------
209217
4895
Dưới ngòi bút của Âu Dương Mặc Tâm, các nhân vật Bao Công, Công Tôn tiên sinh, Triển Chiêu chưa bao giờ trở nên sống động như thế. Một cái nhìn khác, một khía cạnh khác đã khiến mọi nhân vật trở nên gần gũi với tôi hơn. Coi phim, tôi chỉ thấy họ thật hoàn hảo, không gì không làm được, nhưng qua câu chuyện của tác giả, các nhân vật rất là thật, tự nhiên, ai ai cũng đều có tính xấu của riêng mình. Dù đã biết trước kết quả, nhưng vẫn không nhịn được mà hồi hộp, mà kích thích dõi theo từng bước chân qua từng vụ án, dần dần khám phá ra những bí mật đằng sau lớp vỏ bọc. Một quyển sách đầy kịch tính khiến bạn không ngừng lật từng trang cho đến tận trang cuối cùng dù bạn đã nắm rõ kết quả trong tay.
5
566587
2015-06-17 03:00:58
--------------------------
207758
4895
Đây là một bộ truyện khiến mình vô cùng yêu thích và vẫn mỏi mất trong ngóng từng tập truyện, chỉ có điều Nanu làm hơi lâu thì phải? Truyện này nhân vật chính không phải mới lạ, đều là những nhân vật quen thuộc với mình từ nhỏ như Bao Công, Triển Chiêu, Công Tôn Sách,… nhưng ở đây, qua ngòi bút của Âu Dương Mặc Tâm mình như nhìn thấy những khía cạnh khác của họ vô cùng thú vị. Những vụ án phần lớn là được xào lại nhưng vẫn hay. Mình đặc biệt thích nữ chính Kim Kiền, cô là một người con gái có cá tính thật sự đặc biệt, ấn tượng cực kỳ.
5
393748
2015-06-13 06:41:17
--------------------------
194649
4895
Chưa bao giờ mình đọc một tác phẩm ngôn tình có nữ chính đặc biệt như Đến phủ Khai Phong làm nhân viên công vụ, bạn Kim Kiền của chúng ta quả là một sáng tạo mới mẻ đầy ấn tượng của tác giả Âu Dương Mặc Tâm. Vẫn những vụ án đã quen thuộc với bao thế hệ khán giả của bộ phim Bao Thanh Thiên nhưng bằng ngôn từ của mình, tác giả đã cho chúng ta một cách nhìn khác vô cùng hài hước, đáng yêu của một cô gái đến từ thế kỉ 21, để từ đó chúng ta có những cảm xúc khác, mới lạ hơn, đặc biệt hơn về những câu chuyện tưởng chừng như xưa cũ.
5
374083
2015-05-11 13:28:35
--------------------------
194266
4895
Đã lâu lắm rồi mới có tác phẩm làm tôi trông ngóng đến vậy. Quả thật " Đến Phủ Khai Phong Làm Nhân Viên Công Vụ" của Âu Dương Mặc Tâm tính cho đến thời điểm này là tác phẩm tôi hay lấy ra đọc đi đọc lại nhiều nhất. Mỗi lần đọc là đều không thể không cười lăn lộn. Buồn gì cũng bay đi hết với một Kim Kiền tham tiền như mạng và có nhiều chủ ý phải gọi là kinh người. Đảm bảo đọc tập 1 rồi bạn không thể ngừng tiếp mà mua tập tiếp theo nên để đừng phải ngóng trông dài cổ có khi bạn nên mua hết một lần cho tiện. Các vụ án của tác phẩm đều quen thuộc hết, nào là Trát Mỹ Án, nào là Ô Bồn Án, nhưng qua tác phẩm thì mang một màu sắc khác. Làm tôi đọc mà không sao ngừng lại được.
5
628391
2015-05-09 23:08:29
--------------------------
192732
4895
Đến phủ khai phong làm nhân viên công vụ là cuốn truyện đầu tiên mình đọc lấy bối cảnh lịch sử. Qủa thật không khiến người đọc thất vọng! Kim Kiềm không phải là cô nàng tốt bụng gì cho cam nhưng nhờ có hai người sư phụ đã rèn luyện 36 bí kíp cho thì cô đã dùng nó để giúp cho Bao Thanh Thiên phá án thành công. Không ngờ được một ngày sẽ được đọc một cuốn truyện kiếm hiệp hài hước đến như vậy Tuy có hơi dùng nhiều từ khó hiểu nhưng khi đọc lâu rồi sẽ quen.
5
591187
2015-05-05 14:33:29
--------------------------
189803
4895
Đến với ĐKPPLNVCV tôi không thể không khen trí tưởng tượng phong phú của tác giả, nhờ trí tưởng tượng có một không 2 của ngài mà đã tạo nên một Kim Kiền tôi không còn gì để nói. Tôi thường đọc có nữ chính cao cường thông minh tuyệt đỉnh, đôi khi cũng có ngu ngốc làm tiểu bạch thỏ, nhưng chưa từng nghĩ một ngày sẽ xuất hiện Kim Kiền vô sỉ, tham tiền, nhát gan gặp nguy hiểm chạy nhanh hơn thỏ. Thật sự không ngờ từ câu truyện gốc Bao Thanh Thiên danh chấn một thời mà có thể tạo ra một câu truyện hài hước đan xen các tình tiết phá án hài đến như vậy. ...Mọi chuyện cứ diễn ra thật tự nhiên, hài nhưng không có tình tiết dư, mọi chuyện đều sắp xếp khá hợp lí. Tôi rất biết ơn tác giả A6DMT đã tạo ra câu truyện này để cho độc giả có nhiều tràng cười thoải mái đến vậy.
5
391362
2015-04-28 00:07:37
--------------------------
189188
4895
Không còn gì để nói nữa, sau khi đọc cuốn này thì Kim Kiền đã chính thức trở thành nữ nhân vật chính số 1 trong lòng tôi (dù tôi thấy 90% làm nhân vật nam vẫn thích hợp hơn). Tích cách quái lạ, thích nịnh bợ, ham tiền và mê trai của cô nàng rất hợp gu tôi (rất dễ thương). Quả thật, tui rất vui vì tác giả đã tạo ra một nhân vật tuyệt vời như vậy. Ồ, đượng nhiên là Triển Chiêu nhà tôi cũng rất ổn, rất là "tạcmao thụ" (chỉ là ý kiến riêng thui). Cốt truyện vui nhộn, dựa trên nhưng vụ án sẵn có nhưng không hề nhàm chán. Cách hành văn của tác giả rất hợp ý tui, đương nhiên cũng phải cảm ơn dịch giả. Nói chung tôi rất thích cuốn này, dù điểm trừ duy nhất là tôi thấy bìa sách chưa được ấn tượng như tôi mong chờ cho lắm. Tuy vậy, cái cốt lõi bên trong vẫn là đáng khen hơn.
5
146927
2015-04-26 19:10:20
--------------------------
186888
4895
Thực ra mình chưa bao giờ thực sự xem Bao Thanh Thiên , nhưng lăn lộn trên mạng nhiều mình cũng biết sơ sơ về các nhân vật trong phủ Khai Phong và cảm thấy vô cùng hứng thú .  Và giờ Âu Dương Mặc Tâm đã giúp mình tiếp cận thêm với Khai Phong phủ một cách cô cùng thú vị . 
Truyện kể về Kim Kiền- cô gái xuyên không chẳng có gì nổi bật và tham lam tiền bạc . Nhưng đã gọi là nữ chính tất nhiên sẽ có điểm làm người ta ưa thích . Mình rất phục tài viết văn của tác giả , ngôn từ trau chuốt phong phú , tượng tượng cũng vô cùng đa dạng .
Nhìn chung đây là cuốn sách vô cùng thú vị , ai thích Triển Triêu thì tuyệt đối không được bỏ qua đâu nhé ♥
5
335856
2015-04-22 01:33:34
--------------------------
185255
4895
Dù bạn đã từng hay chưa bao giờ xem Bao Thanh Thiên, bạn cũng sẽ bị cuốn hút khi đọc truyện này. Đến phủ Khai Phong làm nhân viên công vụ kể lại những vụ án kinh điển của phủ Khai Phong dưới thời Bao Thanh Thiên. Tập 1 với Trát mỹ án cũng chỉ đơn giản là kể chuyện điều tra phá án. Nhưng bên trong những tình tiết cũ, nhân vật cũ, Mặc Tâm đã tìm ra những con người mới, cách nghĩ mới, khiến độc giả cho dù biết trước thủ phạm hay kết cục, vẫn phải hồi hộp chăm chú dõi theo hành trình khám phá của các nhân vật, để rồi thở phào nhẹ nhõm khi thủ phạm cuối cùng cũng bị trừng phạt. Đối với một fan ruột xuyên không như mình thì đây chắc chắn là một quyển truyện không thể bỏ lỡ.
5
569928
2015-04-19 09:31:23
--------------------------
179095
4895
Đây là truyện xuyên không đầu tiên lấy được cảm tình của mình khi chỉ vừa đọc mấy trang đầu. Nói theo các tỷ muội yêu thích ngôn tình thì truyện này thuộc thể loại đồng nhân. Mạch truyện Bao Hắc Tử bị thay đổi không nhiều, nhưng điều đặc biệt là trong qua trình phá án nữ chính động chân động tay vào để dẫn đến kết quả khá nhiều. Sức tưởng tượng của tác giả rất phong phú, cộng với việc nắm bắt khá rõ tình tiết trong Bao Thanh Thiên đã làm nên một sự cộng hưởng tuyệt vời. Tập này nói về vụ Trát Mĩ Án, nhờ có bàn tay nữ chính mà những sự việc đáng tiếc tối thiểu không bị xảy ra. Mặc dù chị rất sợ phiền, nhưng chị vẫn theo giúp mẹ con Kim Liên. Nữ chính rất cá tính, trừ việc hám tiền quá thôi ^^! Mà, dù sao đấy cũng là một nét nổi bật rất độc đáo của chị mà.
5
209069
2015-04-06 14:18:28
--------------------------
179024
4895
Nghe giới thiệu bộ này đã lâu lắm rồi nhưng giờ mới có cơ hội đọc nó. Đây là một câu chuyện kể về các nhân vật quen thuộc đến không thể quen thuộc của Phủ Khai Phong và các vụ án mà lão Bao thụ lý dưới một góc nhìn mới mẻ pha lẫn hài hước của tác giả. Nữ chủ là người xuyên không nên cũng khá là YY, gương mặt đã không đẹp, thân hình cũng không có, lại còn háu sắc với tham tiền. Quả là tập hợp đủ mọi yếu tố làm người ta khinh bỉ. Tuy là vậy nhưng chị rất chi là hài hước và ma mãnh, thường thường chọc cho Tiểu Miêu (Triển Chiêu) giận dỗi tới đỏ mặt tía tai. Ngồi đọc mà tớ cứ cười không ngừng với lối suy nghĩ và cách ứng xử của nữ chính.
5
39118
2015-04-06 11:59:42
--------------------------
176931
4895
Đọc truyện, những vụ án quen thuộc trong Bao Thanh Thiên được tái hiện lại bằng một giọng văn hài hước, ly kì và gay cấn. Những Bao Công, Công Tôn tiên sinh, Triển Chiêu và đặc biệt là cô nàng Kim Kiền khó đỡ khiến người đọc không thể không bị thu hút. Lắm lúc đọc mà thấy tội nghiệp thay Miêu đại hiệp, vì tác giả đã đem anh quăng vào tay ma nữ Kim Kiền, có lẽ anh sẽ còn đau khổ dài dài. Điểm trừ lớn nhất của truyện là không biết cái hố này đến bao giờ mới lấp xong, kiên nhẫn chờ truyện hoàn thì chờ không nổi, mà trót nhảy vào rồi, mê rồi lại càng chờ không nổi, chỉ hi vọng tác giả viết nhanh nhanh lên một chút.
5
57459
2015-04-02 09:26:58
--------------------------
172135
4895
Đối với một đứa không kịp mua tập 1 cũ như tôi, thì việc tái bản là điều cực kì may mắn!
Đến phủ khai phong làm nhân viên công vụ có cốt truyện xuyên không kết hợp với đồng nhân Khai phong phủ, thể loại khá mới mẻ mà các truyện Trung Quốc hiện nay chưa có. Tác giả gây ấn tượng ngay với văn phong gây hài thu hút, đáng yêu mà vẫn rõ ràng, mạch lạc từng phân đoạn. Tôi không thích xem phim Bao Thanh Thiên, nhưng thích đọc truyện này bởi Âu Dương Mặc Tâm đã đơn giản hoá các chi tiết lịch sử đi, đan xen yếu tố gây hài và tình cảm giữa Triển Chiêu - Kim Kiền.
Bìa sách đẹp, màu tái bản giống nhưng vị trí tên truyện không giống các tập còn lại cho lắm. Được tặng thêm 4 postcard rất đẹp, rất đáng yêu!
5
468194
2015-03-23 14:18:50
--------------------------
169969
4895
Đầu tiên phải nói là mình rất không thích truyện xuyên không nhất là những truyện có liên quan đến lịch sử hay những gì đại loại như vạy. Đến khai phong phủ làm nhân viên công vụ lại hội tụ đủ cả hai yếu tố đó nhưng nó lại là ngoại lệ đối với mình. Truyện rất hài hước, vẫn là những vụ án quen thuộc nhưng dưới ngòi bút của Âu Dương Mặc Tâm lại trở nên rất thú vị. Một Khai phong phủ với Bác Bao thanh liêm, liêm khiết, một quân sư Công Tôn gậy trúc bụng đen phúc hắc, một Ngự tiền tứ phẩm hộ vệ Triển Chiêu ngay thẳng chính trực, đẹp trai, phóng khoáng là idol số 1 của thành Biện Lương, thêm cả một cô nàng Kim Kiền của thế kỉ 21 tham tiền, hám của tất cả đều rất dễ thương. Tình cảm của Triển Chiêu với Kim Kiền cũng dễ thương cực kỳ, đặc biệt là khi Triển Chiêu ghen. Hãy đọc Khai phong phủ để cảm nhận một phủ Khai Phong rất riêng biệt không có ở bất cứ nơi nào khác.
5
51313
2015-03-19 10:43:07
--------------------------
165163
4895
Truyện đọc rất hài, lúc mình đọc đã phải cười rung mạnh đến nỗi té khỏi cái ghế dựa vào tường có tay vịnh! Điểm cộng thứ nhất chính lá cho nội dung truyện. Nội dung hay, tình tiết hấp dẫn, cách dùng từ của dịch giả cũng rất chuẩn, khiến cho mình rất thỏa mãn với số tiền bỏ ra. Điểm cộng thứ hai chính là chất lượng của cuốn sách, bìa sách cùng bookmark rất đễ thương, giấy cùng sờ rất đã tay, sờ hoài vẫn ghiến! Điểm trừ duy nhất cho cuốn sách này là tác giả thật sự viết rất chậm! Đại loại là khoảng một năm hơn thì mới ra một cuốn đi, nhưng không sao, mấy cuốn của bộ này mình nhai khoảng trên dưới năm lần vẫn chưa thấy chán! Thích hợp cho những ai đang muốn sự hài hước và dí dõm trong từng trang sách!
5
363319
2015-03-09 21:57:28
--------------------------
161535
4895
Tập 1 của truyện khá thú vị. Cũng là mô tuýp xuyên không thông thường, nhưng nhờ các tiết hài hước nên truyện không gây nhàm chán. Tuy nhiên, đọc tập 1 này, mình cảm thấy truyện không có chiều sâu, vẫn là đọc để giải trí trí thì được, nhưng để gọi là nhớ lâu thì không thể. Mặt khác, mình cảm thấy chi tiết phi thực tế nhất chính là tâm lí của nữ chính khi xuyên qua, nó chỉ được miêu tả một cách qua loa. Thậm chí mình còn chẳng cảm nhận được sự lo lắng, đau buồn thực sự của Kim Kiền khi bản thân có khả năng phải xa rời mãi mãi gia đình, bạn bè của mình. Nói chung thì dù nữ chính là đầu mối của tất cả các tình tiết gây cười nhưng cái sự vô ưu, vô lo thái quá của  cô không gây thiện cảm với mình cho lắm.
4
323629
2015-02-28 10:50:24
--------------------------
161064
4895
Bắt đầu đọc Đến phủ khai phong làm nhân viên công vụ từ 2013, có thể nói đây là một trong những bộ truyện mình yêu thích nhất. Bao Công-Bao Thanh Thiên hẳn rất quen thuộc với nhiều người, mình coi phim riết cũng thuộc gần hết các vụ án trong đó, nhưng khi đọc tác phẩm này, chị Mạc Tâm đã mang đến một hơi thở mới cho câu chuyện. Các sự kiện, vụ án được diễn ra hài hước và thú vị hơn  nhờ sự góp mặt của Kim Kiền- cô nàng đến từ thế kỉ 21, với sự hiểu biết dù là nhất định của mình, Kim Kiền đã rất nhanh trí xử lí tài tình mọi tình huống xảy ra xung quanh, mang lại cho mình nhiều của cải cũng như giúp Bao đại nhân phá án và đặc biệt là chiếm được trái tim Miêu nhà ta. Ngoài ra mình cũng hơi bị kết dàn nhân vật trong truyện,có họ tình tiết không bị biến dạng mà mạch truyện còn trở nên vui nhộn hơn, hấp dẫn hơn (nhất là cặp sư phụ của Kim Kiền Y Tiên-Độc Thánh, họ chính là "đối thủ" xứng tầm của Công tôn).
Nói chung là tác phẩm này hài lắm, lại rất gây cấn, sáng tạo khiến mình một lần lọt hố là 2 năm vẫn chưa bò lên được :(( Chỉ ước tác giả viết xong mau mau đi thôi...
5
91968
2015-02-26 16:11:03
--------------------------
160561
4895
Bao Công là tên gọi rất quen thuộc với hầu hết mỗi người, Đến phủ khai phong làm nhân viên công vụ là câu chuyện kể về những vụ án trong phim Bao Thanh Thiên với những nhân vật quen thuộc như Triển Chiêu, Công Tôn tiên sinh, Bao Chửng,.. Và cái sáng tạo của câu truyện là nó được kể lại dưới con mắt của Kim Kiền - cô gái đến từ thế kỉ 21. Những vụ án, tình tiết mang tính trinh thám tưởng chừng mang tính hồi hộp, tò mò, bất ngờ,uy nghiêm nhưng trong câu chuyện này còn mang tính hài hước, dí dỏm bởi sự xuất hiện của Kim Kiền.
Tóm lại truyện sáng tạo, hài hước, mang tính giải trí, rất đáng đọc.
5
521157
2015-02-24 20:22:54
--------------------------
