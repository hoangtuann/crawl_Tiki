299306
8165
Với tựa đề sách " Bí Thuật Dưỡng Sinh Của Vua Càn Long" mình cảm thấy vô cùng tò mò mong là sẽ hiểu hơn về những bài dưỡng sinh, những liệu pháp mà vua Càn Long đã sử dụng, nên mình không ngần ngại mà rước ngay cuốn sách này về.
Nhưng khi đọc xong, mình cảm giác thật uổng tiền...
Cuốn sách chỉ dẫn về cung điện, nơi nằm, quần áo, ... mọi thứ đồ dùng sinh hoạt của vua mà người bình thường như mình không bào giờ với tới... 
Món ăn thì toàn những thứ " trên trời", người bình thường muốn ứng dụng thì không nổi, chắc chỉ dành cho các đại gia thôi

Nếu cuốn sách chỉ đề cập đến sự xa hoa của Vua thì nên đổi luôn tựa sách, chứ đừng ghi " Bí mật dưỡng sinh" nữa, dễ gây ra hiểu lầm cho những ai muốn tìm hiểu các liệu pháp dưỡng sinh thông thường.

Nói chung, so với tựa đề thì đây là một cuốn sách không phù hợp!!! 
2
34983
2015-09-13 09:35:32
--------------------------
