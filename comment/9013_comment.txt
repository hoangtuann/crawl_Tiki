428493
9013
Mình mua cuốn này khá lâu nhưng đến hôm nay mới đọc đến cuốn sách này. Mình thích quan điểm của tác giả về vấn đề tiền bạc. Trong khi đa phần chúng ta đều tránh nói về tiền bạc với trẻ. Nhưng quan điểm hiện đại thì cần phải cho trẻ biết và hiểu rõ về tiền vì tiền luôn hiện hữu trong đời sống. Thế giới ngày càng kết nối với những thẻ tín dụng mà ta không cần có sẵn tiền mặt và không cần phải lê bước ra khỏi nhà vẫn có thể có những thứ mình thích nhưng liệu đó có phải là thứ mình cần? Chúng ta cần phải chỉ cho chúng biết cách sử dụng như thế nào, và tốt nhất hãy dạy từ nhỏ. Cách chia tiền ra nhiều hủ đựng tiền, cách trẻ có thể kiếm thêm tiền, cách vay và cho vay ... Những chuyện không mới nhưng giúp mình định hình những việc cần phải dạy cho trẻ.

4
199100
2016-05-11 12:40:08
--------------------------
328012
9013
Tác giả là người Mĩ, và tất nhiên bà viết dành cho phụ huynh ở Mĩ hay những nước phương Tây phát triển.
Nội dung sách khá phong phú, rõ ràng. Những phương pháp của tác giả rất thực tế dù nó gây ra nhiều tranh cãi (như việc cho tiền trẻ khi trẻ làm việc nhà vẫn là vấn đề sôi nổi và còn được bàn tán). Ý tưởng của tác giả rất tiến bộ trong thời đại mới.
Ta sẽ dễ thấy, nhiều thứ bà giới thiệu, dù là đã quá quen thuộc ở quê hương bà nhưng Việt Nam còn phải chờ đợi những điều đó trong tương lai gần. 
Bởi vậy, bạn chỉ có thể áp dụng phần nào nội dung sách. Nhưng dù sao, dùng tiêu đề sách để giáo dục con bạn có lẽ cũng ổn: Tiền không mọc trên cây.
4
123188
2015-10-28 21:50:12
--------------------------
308264
9013
Mình mua quyển sách này do người bạn giới thiệu. Quả thật , không phí  tiền và thời gian chút nào khi đọc quyển sách này. Sách rất hữu ích cho phụ huynh để dạy cho trẻ về vấn đề tài chính. Cuốn sách đưa ra những chi dẫn rất rõ ràng, cụ thể về việc dạy con trẻ các kiến thức quản lý đồng tiền theo từng độ tuổi, như cách giúp trẻ tính tiền, tiết kiệm, lập ngân sách chi tiêu, cho đến khái niệm phức tạp như "ghi nợ" hay " thế    chấp" ... đến thế giới hoàn toàn mới như thẻ tín dụng, mua cổ phiếu...Bên cạnh đó tác giả còn đưa ra những ví dụ cụ thể, những bài học trực quan sinh động , những trò chơi đầy hào húng  giúp trẻ dễ tiếp thu, không bị nhàm chán . Ngoài ra sách còn đưa ra những lời khuyên cho phụ huynh về cách thức chi tiêu của chính mình - 1 điều sẽ ảnh hưởng không nhỏ tới con trẻ, vì trẻ thường có xu hướng lặp lại những thói quen của cha mẹ. Sách thật sự hữu ích cho người lớn và trẻ em.  
5
512701
2015-09-18 16:17:42
--------------------------
278756
9013
Một cẩm nang dành cho các bậc phụ huynh giúp con trẻ hiểu biết và sử dụng đúng giá trị của đồng tiền. Với nhiều bài thực hành và nhiều ví dụ cụ thể sát thực tế giúp giải toả nhiều vấn đề liên quan đến việc sử dụng tiền bạc đồng thời biết được cách chi tiêu hợp lý. Quyển sách còn hướng dẫn các ông bố bà mẹ cách lập ngân sách và cả cách làm từ thiện, giúp trẻ tự xây dựng khả năng tự lực của bản thân mình. Rất bổ ích và ý nghĩa. Cảm ơn tiki 

5
110777
2015-08-26 09:54:54
--------------------------
241872
9013
Đây thực sự là cẩm nang tương đối đầy đủ trong việc giáo dục tài chính cho trẻ em dành cho các bận phụ huynh. Cuốn sách có những hướng dẫn rất cụ thể cách làm. Còn việc áp dụng như thế nào cho phù hợp với văn hoá gia đình bạn hay văn hoá Việt Nam là tuỳ ở từng người đọc. Với kinh nghiệm hơn 30 năm làm việc trong lĩnh vực tài chính và là chuyên gia tài chính gia đình, trẻ em, chắc hẳn những chia sẻ của bà rất đáng để tham khảo và vận dụng một cách phù hợp.
Phần thuật ngữ cuối sách là một trợ giúp rất tốt vì có nhiều khái niệm trong sách mà nếu người đọc không làm trong lĩnh vực tài chính thì cũng khó lòng hiểu và giải thích chính xác cho con, 
5
292190
2015-07-25 21:45:27
--------------------------
124552
9013
Mình đã mua cuốn sách này về đọc qua và nhận thấy rằng cuốn sách này thật sự bổ ích đối với phụ huynh hiểu và dạy cho trẻ về vấn đề tài chính, được biết về tài chính khi còn nhỏ, dạy được con bạn chi tiêu một cách hợp lý và khoa học.là một nền tảng tốt cho tương lai của trẻ, đặc biệt ở độ tuổi teen. 
Cuốn sách viết cụ thể từng mảng, dễ hiểu, dễ đọc.
Tuy nhiên, vì cuốn sách lấy bố cục bên Mỹ nên còn có một số vấn đề hơi khó hiểu nhưng không ảnh hưởng nhiều.
4
411560
2014-09-07 23:43:15
--------------------------
76660
9013
Quản lý chi tiêu tưởng đơn giản nhưng hóa ra lại phức tạp với khá nhiều người. Việc quản lý đồng tiền của mình thật tốt không chỉ cần thiết khi bạn đã làm ra được đồng tiền mà nó quan trọng ngay từ khi bạn còn nhỏ. Chính vì thế mà bạn cần phải giảng dạy cho những đứa con của mình rằng: "Tiền không mọc trên cây" để chúng bước đầu hiểu về giá trị và biết quý trọng đồng tiền. Cuốn sách này cực kỳ hữu ích đối với các bậc phụ huynh khi dạy cho con cái những bài học hữu ích cho cuộc sống bây giờ và mai sau của chúng. Bạn sẽ biết phải cho con tiền tiêu vặt như nào là đủ và hợp lý, dạy chúng chi tiêu những đồng tiền đó vào đâu để không phí phạm và biết sự dụng tối đa giá trị của đồng tiền, giúp trẻ biết những khái niệm đầi tiên về ngân hàng... Việc dạy trẻ về những kiến thức tài chính sơ đẳng như thế này từ khi còn nhỏ không hề khó khăn và khô khan mà ngược lại rất nhẹ nhàng vui nhộn thông qua cuốn sách này.
3
29716
2013-05-23 23:02:25
--------------------------
