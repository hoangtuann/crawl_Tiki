489908
8903
cuốn sách nội dung hay và thực tế. Nhưng giấy rất mỏng, mực in lem, nhiều trang bị nhòe chữ không thấy gì luôn. Mình định đổi lấy cuốn khác nhưng tình hình chung là cuốn nào giấy cũng xấu.
2
596204
2016-11-10 14:35:27
--------------------------
466581
8903
Đã mua cuốn sách được 2 tháng, cảm nhận đầu tiên là khá dầy, nhưng sẽ giành thời gian để đọc để lướt qua hết để khi cần thiết sẽ tra cứu lại.

Cậu em đang học lớp 11 cũng thích thú và giành để học ôn thi.

Điểm mình chưa hài lòng là chất lượng giấy và chất lượng in, vì khi đọc chữ ko được rõ ràng. Và ở trang này thì bị thì nhin xuyên qua in chữ của mặt sau. 
Mình vẫn mong muốn chất liệu in tốt hơn và nâng giá bán lên 1 chút cũng ko vấn đề gì.
3
673104
2016-07-02 14:21:52
--------------------------
402503
8903
Mình quyết định mua quyển sách này khi được một người bạn giới thiệu trong lúc cần ôn thi Toeic. Thật sự mình có chút không hài lòng khi sách lần này không được đẹp như những lần trước mình mua ở Ti Ki, giấy thấy không được tốt lắm. Còn nội dung sách thì cũng tạm ổn, dù theo mình thấy là nó chưa được đầy đủ lắm. Sách trình bày khá kĩ và chi tiết, có ví dụ minh họa cụ thể và rõ ràng. Hệ thống sách rất logic và chặt chẽ, từ cơ bản đến nâng cao, giúp người đọc có thể nắm nội dung một cách dễ dàng. Để nắm vững về phần ngữ pháp thí nói chung quyển sách này là ok rồi.
4
409457
2016-03-22 13:21:23
--------------------------
244252
8903
Tình cờ thấy bạn mình có cuốn này lại thấy trên tiki cũng có bán nên mình quyết định  đặt mua .Nội dung của cuốn sách thì khá ổn, trình bày rõ ràng dễ tra cứu, nhưng mình rất k hài lòng về chất lượng giấy, phần đầu in rất mờ, khá khó nhìn, có những trang giấy còn chưa cắt hết k biết do mình đen đủi hay sao nữa! đây là bán mới hơn của first new k biết cuốn bên nxb Hồng đức thì thế nào ! Dù sao đây cũng là 1 cuốn sách bổ ích!
3
187773
2015-07-28 08:53:01
--------------------------
149479
8903
Quyển sách về ngữ pháp thực sự đầy đủ và phù hợp với các bạn có nhu cầu nâng cao về cách thức sử dụng trong bài thi đại học cũng như là các chứng chỉ quốc tế như IELTS,TOEFL,TOEIC... Quyển sách thực sự sẽ giúp ích trong việc làm các bài luận văn tiếng Anh hay cả trong việc giao tiếp với người nước ngoài vì sách cũng có rất nhiều ví dụ trong nhiều ngữ cảnh hay nhiều tình huống mà có thể ai trong chúng ta sẽ gặp phải. Cảm ơn tiki vì đã mang một quyển sách thực sự hữu ích đến các bạn học sinh sinh viên như tụi mình!
5
520876
2015-01-13 20:21:58
--------------------------
126166
8903
Cuốn sách này như là 1 cuốn tư điển về ngữ pháp từ những cái cơ bản nhấn đến nâng cao mà bất cứ ai khi học anh văn nên cần có nó. Ngữ pháp được chia ra thành từng mục rõ ràng ,ngoài ra còn có hướng dẫn cách sử dụng các từ ,cụm từ thông dụng và có những ví dụ chi tiết ,rõ ràng. Cuốn sách khá dày ,đầy đủ và không dài dòng . Qủa thật ,nếu muốn nắm vững ngữ pháp từ những bước đầu tiên thì theo mình đây là 1 cuốn sách đáng được chọn .
4
422805
2014-09-17 15:25:45
--------------------------
94441
8903
Về nội dung của cuốn sách:
Mình thực sự thích cách trình bày khoa học, ngắn gọn của cuốn sách. Cuốn sách giống như một cuốn từ điển khá đầy đủ về ngữ pháp tiếng Anh, rất tiện lợi cho mình mỗi khi muốn ôn lại một kiến thức ngữ pháp nào đó.
Về hình thức của cuốn sách:
Mình có một chút không hài lòng về hình thức của cuốn sách cho lắm. Mặc dù khi vừa nhìn thấy cuốn sách mình rất thích vì bìa sách được Tiki bọc rất đẹp nhưng khi mở sách thì mình thấy chất lượng in ấn không được tốt cho lắm, nhiều trang chữ in rất mờ, khó đọc và chất giấy cũng không được đẹp như cuốn sách mà mình đã từng mượn của bạn mình.

3
154426
2013-09-14 09:25:55
--------------------------
