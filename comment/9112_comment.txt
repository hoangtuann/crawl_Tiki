289956
9112
Nếu bạn là một ông bố bà mẹ bận rộn và không có nhiều thời gian cho con cái thì bạn sẽ khó đọc và thực hiện được theo quyển sách này. Tuy nhiên, nếu bạn có thể dành thời gian và sức lực bên con thì đây chính là 1 gợi ý vô cùng hay ho để cùng vừa học vừa chơi với con. Trong cuốn sách nhỏ này có vô số những thí nghiệm đơn giản, nguyên liệu cùng không khó để tìm kiếm và nguyên tắc thực hiện thì chỉ cần cẩn thận một chút là có thể thành công.
4
724204
2015-09-04 23:26:00
--------------------------
255517
9112
Cuốn sách này là tập hợp các ý tưởng, trò chơi khoa học dành cho bé. Rất đơn giản thôi, và cũng dễ làm, nguyên liệu thì dễ kiếm. Các trò chơi lý giải các hiện tượng xảy ra quanh bé, cùng với đó có lời giải thích. Bé vừa chơi vừa học được nhiều kiến thức thú vị, bổ ích, và mỗi giờ chơi cùng bé là bố mẹ và con có một khoảng thời gian vui vẻ cùng nhau. Mình sẽ thử bắt đầu vào các cuộc "thí nghiệm khoa học" nhỏ này cùng con xem sao, mới đọc qua đã thấy hấp dẫn rồi.
4
82774
2015-08-06 09:06:03
--------------------------
