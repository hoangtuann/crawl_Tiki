442105
3511
Trước khi mua cuốn sách này, tôi đã tham khảo nhận xét của nhiều bạn đọc khác và bị ấn tượng bởi những lời khen ngợi đó. Tuy nhiên, cuốn sách có cách tiếp cận  không mới khi nhìn những vấn đề người lớn qua lăng kính trẻ nhỏ. Nội dung sách dễ thương. Chữ to rõ nên đọc rất nhanh. 
Dù sao cũng cảm ơn tác giả đã giúp tôi cảm nhận được phần nào cuộc sống thời bao cấp của Hà Nội ngày ấy một cách trong trẻo và nhẹ nhàng nhất. Ngoài ra, một lời khen khác dành tặng cho bìa sách rất đáng yêu.
3
190232
2016-06-04 12:35:55
--------------------------
360877
3511
Cuốn sách dành cho ai luôn hoài niệm về những điều dĩ vãng về Hà Nội xưa. Bố mẹ ta vẫn luôn kể những điều hay về Hà Nội xưa mà đôi khi những thế hệ đi sau như chúng ta không thể biết hết được những chuyện từng ngõ ngách của nơi ta đã sinh ra và lớn lên.Cuộc sống hiện nay con người ta sống quá hối hả và quá nhanh nên có đôi khi ta lại thèm được thử cuộc sống xưa ấy.Đọc sách ta được nhìn về 1 Hà Nội giản dị,mộc mạc qua lăng kính của cô bé San San tinh nghịch.Một cuốn sách không chỉ dành cho thiếu nhi,cho học sinh mà còn dành cho cả những bậc phụ huynh - những người đã từng 1 thời sống trong hoàn cảnh bao cấp thiếu thốn.Chợt nhớ đến câu nói "Lý do duy nhất để con người níu giữ kỉ niệm bởi vì kỉ niệm là thứ duy nhất không bao giờ thay đổi, trong khi mọi thứ điều đổi thay".
5
494469
2015-12-29 23:25:11
--------------------------
293428
3511
Sinh ra và lớn lên  ở Hà Nội đã gần 20 chục năm nên mình dành cho Hà Nội 1 tình cảm rất đặc biệt. Mình luôn muốn tìm đọc các cuốn sách về Hà Nội xưa, Hà Nội cổ kính đẹp đẽ những năm 80 và San San chân to đi xốp là cuốn sách như vậy. Hà Nội trong đây bình dị và đẹp đẽ, Hà Nội hiện lên qua các mẩu truyện ngắn, qua những trò chơi, những bộ phim quen thuộc với đám trẻ thời ấy. Cuộc sống khi ấy không no đầy như bây giờ nhưng luôn làm cho tôi cảm thấy thật ấm áp và gần gũi, Sách còn có những trang hình minh hoạ rất đẹp và dễ thương.
4
52328
2015-09-08 12:34:27
--------------------------
228604
3511
Đọc cuốn sách, cảm thấy cô bé San san này có nét gì đó khá giống với Tottochan, nhưng cũng có những điểm đáng yêu riêng: thông minh, tinh nghịch và láu cá hơn. Một Hà nội xưa được hiện lên qua cái nhìn thơ ngây, hồn nhiên của một cô nhóc vì vậy nó gợi cho ta nhớ nhiều về Hà Nội, về tuổi thơ, cho ta cười và cho ta một chút nuối tiếc về quãng thời gian một đi không trở lại
Rất ít những cuốn sách hay như thế này cho thiếu nhi và có lẽ là cho cả những ai muốn nhìn lại về Hà Nội, về tuổi thơ của mình. Văn phong giản dị, mộc mạc đúng như suy nghĩ lời nói của trẻ em nên rất đáng yêu, dễ gây thiện cảm cho người đọc.
4
465921
2015-07-15 17:32:00
--------------------------
216118
3511
Mình rất thích những câu chuyện về Hà Nội ngày xưa, nhất là khi nó được kể lại dưới góc nhìn của các em bé (mà có khi mình phải gọi là cô bé hay bác bé luôn rồi) sống trong thời kì ấy.

Trước đây là Trẻ con phố hàng của tác giả Bạch Ngọc Hoa, giờ là San San chân to đi xốp của tác giả Quỳnh Lê Quynh Boissez. Ngay từ cái tên đã thấy "nhộn nhịp" rồi. "Chân to đi xốp" đơn thuần là một đôi bàn chân to quá khổ, tới độ chỉ đi được dép xốp, nhưng cũng là kiểu bắt chước vô cùng dễ thương những cái tên "kiểu Liên Xô" một thời.

Trong cuốn sách này, độc giả cứ thoải mái cười đi, khóc một chút cũng được, nhưng quan trọng nhất là vui, và cảm thấy bình yên trong lòng.

Như mình đã từng trải qua lúc đọc cuốn sách này.
5
102543
2015-06-27 10:37:36
--------------------------
