434672
3362
Mình mua bộ sách này cách đây 2 năm, theo dõi từ ngày Mer "bị" chọn đến ngày Eadlyn trưởng thành và mình phải công nhận Kiera đã hoàn toàn thuyết phục mình ngay từ những dòng trích bên ngoài bìa sách. Lúc đầu thấy bìa đẹp nên mình tò mò, sau khi tìm hiểu thêm và đọc vài trang đầu trên Amazon thì trong đầu chỉ có mỗi suy nghĩ là phải mua bộ sách này cho bằng được. Giọng văn rất bình dị, nhẹ nhàng và thu hút của Kiera đã cuốn mình theo từng câu chữ của câu truyện. Nếu như Harry Potter kể về thế giới phù thùy, cuộc tranh đấu chống lại bóng tối và những chuyến phiêu lưu kỳ thú của HP và bạn bè thì The Selection lại nói về một chuyện tình đượm chất cố tích trong một thế giới chính trị phức tạp. Nói là rắc rối thế thôi nhưng truyện đọc này khiến mình rất cảm động. Cảm động từ những cuộc tranh luận đầy hài hước giữa Max với Mer đến những lúc Max làm đủ mọi thứ để giành lấy trái tim Mer từ tay Asp,... Lần đầu tiên đọc mình rất bức xúc với cách đối xử Mer dành cho Max, sự thiếu dưt khoát trước mối tình đầu với Asp đặc biệt là ở cuốn 2 nhưng sau khi đọc 3 cuốn còn lại thì cảm giác ấy cũng biến mất dần. Bây giờ dù bộ truyện đã kết thúc nhưng mình vẫn đọc đi đọc lại nó rất nhiều lần, mỗi lần càng thấy yêu mến và ngưỡng mộ hơn cặp đôi này. Nhiều lúc mình thật sự ước rằng sau này cũng sẽ có được một chuyện tình đẹp như thế. Tình cảm mà 2 người dành cho nhau là không thể nhầm lẫn, đặc biệt khi Max bị thương hay khi Mer lên cơn đau tim. Và một điều lưu ý bé bé xinh xinh là truyện này hơi ngược nam nha mấy bạn :3 Có thể có những đoạn khiến bạn bực mình nhưng mà bạn cứ bình tĩnh dọc tiếp còn k thích thì ngưng từ từ đọc lại nhưng chắc chắn k thể bỏ qua bộ sách này. KHÔNG THỂ BỎ QUA!!! Chúc các bạn có một khoảng thời gian vui vẻ, hạnh phúc ở Illéa <3
P/s: Khuyến khích mua bản gốc tại vì bìa đẹp, viết hay hơn và... bìa siêu đẹp <3 <3 <3
5
478111
2016-05-23 16:47:47
--------------------------
430896
3362
Mình mua sách ban đầu do bị cuốn hút bởi thiết kế bìa sách, khá bắt mắt. Phần đầu của cuốn sách khá buồn tẻ, không cuốn hút được người đọc, chỉ đến phần Mer vào cung, gặp được Hoàng tử và giữa họ có sự liên hệ thì câu chuyện mới bắt đầu được coi là có hồn. Nhưng mối quan hệ giữa Hoàng tử, Mer và Aspen lại là mối quan hệ tay ba, mập mờ, cho tới cuối sách mình vẫn chưa hiểu rõ được cuối cùng Mer chọn ai? Đây có thể được coi là chuyện cổ tích giữa đời thường, mặc dù không có phù thủy :3 nhưng nữ chính cũng là một bước lên mây giống như Lọ Lem vậy :)) Dù vậy mình vẫn mong một cái kết có hậu giữa Maxon và Mer. Bản dịch ổn nhưng còn làm nhạt đi khá nhiều chi tiết đắt giá của tác phẩm ,mong ở những phần sau của series dịch giả sẽ cải thiện hơn =))
4
284930
2016-05-16 09:20:11
--------------------------
401331
3362
Vốn rất mê những câu chuyện lãng mạn về hoàng tử và công chúa nên mình không ngần ngại gì mà chọn mua ngay quyển này. Hơn 4 tiếng liên tục dán mắt vào từng trang sách, để bây giờ vương lại là một cảm giác nuối tiếc, cứ như được ăn ngon chưa đã thích thì lại không được ăn nữa vậy *haha*. Tuy nhiên mình biết bộ truyện này vẫn còn được tiếp tục, nên cứ cố gắng bình tĩnh mà chờ thôi :3 (thật ra mình chịu không nổi nên đã lên mạng tìm đọc hết rồi)

Motif truyện cũng không quá lạ, nhưng cái hay là tác giả đã xây dựng cho một vài cô gái Người Được Chọn (và cả các nàng hầu nữa) một thứ tình bạn dễ thương, sâu sắc chứ không phải là những màn ẩu đá tranh đấu kịch liệt, giúp câu chuyện được nhẹ nhàng và đỡ nặng nề hơn trong không khí tranh đua "giành chồng" khá căng thẳng *haha*. Qua đó, phần nào tác giả cũng cho ta thấy ở đâu thì cũng có người tốt, kẻ xấu, người thật lòng và kẻ giả dối. Ngoài ra, mình đặc biệt trân trọng cái cách Kiera Cass đề cao tình cảm gia đình thông qua 1 vài chi tiết nhỏ khi bà phác hoạ câu chuyện của America và Maxon.

Mình thật sự thích cách tác giả xây dựng hình mẫu vị hoàng tử Maxon nghiêm nghị, đẹp trai và rất đỗi dịu dàng. Cách anh đối xử với từng cô gái cho thấy anh là một người đàn ông lịch thiệp và biết tôn trọng người khác. America cũng chính là hình tượng mà mình muốn và đang cố gắng để trở thành: sống đúng, sống thật với bản thân, đối xử tốt với mọi người, và biết nghe theo tiếng nói của trái tim. Aspen cũng là một nhân vật khá thú vị, mạnh mẽ, can trường, có ý chí. Hi vọng anh sẽ tìm được tình yêu mới của mình ở chốn cung điện vì vốn mình trót quá yêu thích cặp đôi Maxon - America quá rồi.

Nhìn chung điểm công là thế, tuy nhiên bản dịch vẫn còn một số lỗi sai không đáng có, và mình cảm nhận nó chưa thật sự sâu sắc. Mình không biết đó là do nguyên tác từ bản gốc là thế, hay do bản dịch chưa truyền tải được hết nội dung. Dù sao đi nữa, mình vẫn sẽ tìm bản gốc đọc, để có một cảm nhận đầy đủ hơn về câu chuyện tình ngọt ngào đáng yêu quá đỗi này.
4
47349
2016-03-20 14:51:22
--------------------------
399461
3362
Mình bị hấp dẫn ngay từ lần đầu tiên nhìn thấy bìa sách. Mình đặt mua nó và đọc ngay từ lúc cầm sách trên tay
Đọc sách rồi bị hấp dẫn luôn
có hôm thức đến 1 giờ sáng để đọc nó
Truyện hay quá. cách tác giả Kiera Cass miêu tả nhân vật rất chi tiết và dể thương
nghe tin sách chuyển thành phim mà thích lắm
chỉ thích ngắm diễn viên rồi tưởng tượng ra cảnh phim
Nói chung là có thể xếp vào Best Books của mình
Thấy Nhã Nam bảo là đến giữa năm sẽ ra phần hai của truyện
The Elite <3
5
856660
2016-03-17 20:09:17
--------------------------
353216
3362
Mua cũng lâu rồi nhưng mình để quên luôn trên giá sách, mãi tới hôm qua mới bỏ ra đọc, và đến giờ vẫn còn ân hận vì sao lại đọc cuốn này muộn thế. Xét tổng thể thì đây là 1 kiểu truyện không mới, tình tiết khá là dễ đoán, các nhân vật cũng không quá độc lạ, nhưng cách kể chuyện vẫn khá lôi cuốn, tình tiết truyện không có quá nhiều gay cấn nhưng lại rất ngọt ngào, không phải theo kiểu quá sến súa nhưng vẫn khiến người đọc cảm thấy mơ mộng. Maxon và America là một cặp đôi kì lạ, họ đến với nhau theo cách không bình thường, địa vị của cả hai cũng không hề bình thường, nhưng cách họ yêu nhau cũng chẳng khác gì những cặp đôi khác, yêu nhau bằng sự sẻ chia, yêu bằng những tình cảm chân thật dần bồi đắp qua thời gian, một tình yêu đẹp và mộc mạc giữa chốn hoàng cung xa hoa và những cuộc đối đầu khắc nghiệt của những cô gái trẻ. Rất mong chờ 2 tập tiếp theo của series này, cũng sắp rồi, hi vọng 2 tập sau cũng sẽ mang lại cho mình những cảm xúc y như tập 1 này ^^
4
502810
2015-12-15 19:19:16
--------------------------
344877
3362
Truyện xoay quanh những cuộc đấu đá giữa những cô gái trẻ với nhau để lấy lòng hoàng tử Maxon trong cuộc tuyển chọn hoàng phi, những lần ẩn nấp vì quân phiến loạn tấn công, tình yêu giằn xé giữa Aspen và nhân vật nữ chính - America Singer. Ngoài phần bìa sách rất đẹp, thì tôi vẫn cảm thấy câu chuyện dường như quá nhạt so với mong đợi. Sự mập mờ tình yêu giữa 3 nhân vật Aspen, America Singer và hoàng tử Maxon khiến tôi cảm thấy rối rắm khi đọc. Cứ tưởng trái tim của America đã quên đi Aspen và bắt đầu mở ra vì Maxon, thì Aspen lại từ đâu xuất hiện khiến câu chuyện đi theo một chiều hướng khác. 
2
301353
2015-11-29 12:03:54
--------------------------
312047
3362
Nào giờ chỉ thích truyện ngôn tình nhưng nay đọc thấy nội dung nhân vật được xây dựng hay lắm Mình rất mong chờ được tiếp để biết quyết định của Mer. Cảm thấy buồn cho nhân vật khi phân biệt các giai cấp theo các số thứ tự. Tuy là nội dung vẫn thường hay gặp trong các phim các tiểu thuyết là đã có người yêu sau khi chia tay thì tìm được tình yêu mới. Mình thích một kết thúc có hậu cho cả 3 nhân vật chính. Cuốn sách có bìa đẹp, ấn tượng, màu sắc đặc biệt. mình mua ngay khi thấy sách 
4
20094
2015-09-20 17:29:14
--------------------------
304674
3362
Đọc tới cái lời cám ơn bà bùn dễ sợ :(. Cứ đợi xem ruốt cuộc kết quả thế nào. Ai dè bị bỏ ngỏ, xem ai muốn tưởng tượng và cho kết sao thì cho. Lên mạng search ngay "Tuyển chọn hoàng phi 2" :v. Hóa ra hok có hehe. Khi Mer gặp Aspen trong hoàng cung, mình cứ tưởng sau đó là những tình tiết cao trào được khai khác, để xử lý tình huống giằng co nội tâm của Mer mạnh mẽ hơn. Dù sao thì vẫn thix Maxon hihi... ấm áp, gần gũi, tuy chút vô tâm nhưng không phải cố ý... bị người khác hiểu lầm... vì họ chưa gần anh nên không biết!
4
159195
2015-09-16 16:12:01
--------------------------
289533
3362
Vừa xem xong Tuyển Chọn Hoàng Phi và cảm thấy rất là thất vọng, không như tôi mong đợi, vì nội dung khá là nhạt nhòa, không có gì đặc sắc cũng như mới lạ, để tạo ấn tượng đột phá trong lĩnh vực truyện lãng mạn Âu Mỹ.
Chắc có lẽ tôi xem truyện ngôn tình thể loại cổ trang cung đình khá nhiều rồi nên khi xem truyện này nó không gây được ấn tượng mạnh mẽ với tôi.
Nội dung thiếu cuốn hút đến nhân vật cũng không có gì để nhớ. Nữ chính America Singer cá nhân tôi thấy rất bình thường chưa đủ điểm nhấn, thậm chí Maxon đối với tôi cũng không gây được ấn tượng.
Bù lại nội dung không hấp dẫn mấy thì bìa sách quá ư tuyệt vời, đẹp long lanh, chính bìa sách đẹp mê hồn như thế này đã dẫn dụ tôi không suy xét mà mua ngay quyển sách.

2
180185
2015-09-04 16:06:01
--------------------------
289429
3362
Sau khi đọc hết cuốn sách, mình thấy rằng nội dung câu chuyện không mới. Bộ 3 nhân vật chính kiểu gì cũng sẽ rơi vào rắc rối tình cảm trong khung cảnh hoàng cung đầy thị phi, cạnh tranh và đấu đá.
Tuy nhiên, mình vẫn say sưa đọc, vì mình mong chờ cách giải quyết câu chuyện sẽ có gì đó mới mẻ và táo bạo hơn trong 2 tập sau, khi America phải lắng nghe con tim mình. 
Nhịp điệu tập đầu tiên này vẫn khá chậm và đều, nhưng cũng có bất ngờ. Tình bạn vẫn hình thành ngay trong lòng cuộc chạy đua giành lấy trái tim hoàng tử Maxon.
So với một tác phẩm lãng mạn thì cuốn này khá thực tế và thông minh. Ngoài ra, bìa sách đẹp, chất lượng giấy và bản in tốt, ít lỗi chính tả.
4
20161
2015-09-04 14:38:54
--------------------------
284763
3362
Bị thu hút bởi bìa và tựa sách, nên tôi đã mua cuốn này. Sách có thiết kế bìa đẹp, màu xanh bắt mắt. 
Đây là series tiêu thuyết tình cảm nhẹ nhàng, lãng mạn phù hợp với bạn đọc teen. Như là chuyện cổ tích giữa lọ lem và hoàng tử, chỉ khác là nàng lọ lem đã có người thương trước rồi. Hoàng tử cũng có vô số mỹ nữ bên cạnh để lựa chọn. 
Quyển 1 kết thúc với điểm nhấn là cô gái nhận ra tình cảm của mình. 
Nội dung thuộc dạng motip quen thuộc nhưng lại là series quá dài. Cá nhân tôi không theo dõi tiếp tục cuộc tranh giành này nữa.  

3
35521
2015-08-31 11:17:33
--------------------------
264333
3362
Tôi từng không thích đọc truyện nhiều tập thế nhưng sau khi đọc Tuyển chọn Hoàng Phi 1 tôi đã rất mong chờ quyển 2, 3. Đây thật sự là một tác phẩm rất đặc sắc. Bí ẩn, gai góc nhưng mãnh liệt đầy chất thơ là những gì tôi cảm nhận được khi đọc. Thiết nghĩ tình cảm con người cũng thật kì lạ, còn nhớ còn yêu tha thiết nhưng nếu bất chợt rung động thêm lần nữa vì một ai đó khác thì lại phân vân, tôi thật sự rất tò mò liệu rằng ai sẽ là người sau cùng cùng chung bước sánh đôi cùng America...
4
609523
2015-08-13 09:31:22
--------------------------
263254
3362
Tuyển chọn hoàng phi tập một khá hay, cốt truyện hấp dẫn tình tiết ly kỳ. Nhưng vẫn có một số lỗi chính tả, dịch không có nghĩa. Tuy nhiên, câu chuyện về cô gái America Singer và chàng trai Leger thì cũng khá hấp dẫn nhưng anh chàng lại là một tầng lớp thấp nên cô và anh lại không thể hạnh phúc. Về hoàng tử Maxon và America Singer thì cũng khá thú vị. Câu chuyện nói lên tình yêu của tuổi trẻ. Và pha chút phân biệt tầng lớp giai cấp. Tập một cũng chua rõ cho lắm, mong rằng tập hai sẽ sớm ra mắt và tập ba cũng thế, để giải đáp những thắc mắc bấy lâu của độc giả.
5
605343
2015-08-12 14:10:23
--------------------------
254160
3362
Một câu chuyện cổ tích đang xảy ra giữa 1 xã hội đầy u tối. Như 1 chuyện tình giữa Lọ Lem và hoàng tử, nhưng nhiều nút thắt, kịch tính, và phức tạp hơn, dĩ nhiên cái kết "hạnh phúc mãi mãi" có diễn ra được hay không, thật khó mà biết trước. Một câu chuyện lãng mạn vừa đủ, hấp dẫn vừa đủ và dĩ nhiên là dành cho những độc giả khó tính vừa đủ thôi :))
Hình thức, bìa khá đẹp và tao nhã. Chính mình cũng bị bìa cuốn hút nên mới mua cuốn sách này, và đó là sự thành công của Nhã Nam trong vấn đề thiết kế. Bên trong, trình bày khá đẹp mắt và rõ ràng tuy nhiên lại khá nhiều lỗi edit. Có vẻ Nhã Nam khá vội khi xuất bản cuốn sách này, hi vọng vấn đề này tập sau sẽ được nxb khắc phục.
4
41744
2015-08-05 01:32:21
--------------------------
252789
3362
Trước khi mình đặt mua cuốn sách này, mình cảm thấy cuốn bìa sách thiết kế khá đẹp, nội dung truyện tạm được. Khi mình mua đọc thì cảm thấy khá thất vọng vì cốt truyện ko hay như mình nghĩ. Truyện còn khá là mập mờ, ko rõ về mối quan hệ của 3 nhân vật lắm, khá là khó chịu vì mối quan hệ tay ba này. Đến cuối truyện mình vẫn chưa thật sự hiểu là nữ chính đang thích ai nữa; câu chuyện về nữ chính vẫn đang còn bị bỏ dở, khó hiểu và rắc rối.
2
258651
2015-08-03 22:12:31
--------------------------
250877
3362
Tôi đã từng đọc quyển sách này vì bìa có hình ảnh rất bắt mắt và rất tao nhã.Truyện kể về một xã hội có những tầng lớp quá gay gắt, cuộc sống của họ phụ thuộc theo nùa. Bên cạnh đó cũng có một số tình tiết hài hước và rất hấp dẫn.Ngay khi đọc xong quyển này tôi rất háo hức chờ đợi những tập truyện tiếp theo.Nhưng phần dịch vẫn còn khá nhiều lỗi sai. Mong rằng NXB Nhã Nam và Tiki phát hành tập tiếp theo cũng hấp dẫn như vậy và sẽ dịch tốt hơn.
5
733234
2015-08-02 10:59:41
--------------------------
248372
3362
Khi đọc giới thiệu và vài chương đầu trong truyện thì thực sự là mình không ấn tượng lắm với motif một cô gái nghèo mạnh mẽ và chàng hoàng tử đẹp trai, phong độ v.v
Lý do khiến mình mua cuốn sách là vì một nhận xét đây là phiên bản đấu trường sinh tử không máu thì mình lại nghĩ ngay tới một trận cung đấu nhưng cũng không có. Theo mình thì nội dung truyện không mới và vẫn theo motif của dòng tác phẩm lãng mạn. Tác giả có cố gắng thêm vào một số tình tiết kịch tính nhưng vẫn chưa tròn, có chút gượng ép và chưa thỏa đáng (vd như hoàng cung gì mà giặc vào phá liên tục và vua thì trốn dưới hầm cho lính gác đi dẹp loạn hay việc cái cô gái bị loại cũng mờ nhạt, không ấn tượng). Với cách xây dựng một đất nước đối diện với chiến tranh và với 35 cô gái giành nhau ngôi vị hoàng phi thì mình đã hi vọng là một câu chiện hồi hộp và gây cấn hơn nhiều. Mình thấy tác giả có ý tưởng hay nhưng chưa thực sự tận dụng và khai thác được để tạo kịch tính thu hút người đọc như sự khốc liệt trong sự tuyển chọn, những màn tranh tài hay đấu đá đầy mưu mô của các cô gái, những nguy hiểm mà họ phải đối mặt trước vấn đề chiến tranh (hay là do mình lậm cung đấu nhỉ :">) Nhưng do đây chỉ mới là cuốn đầu tiên trong series nên có lẽ những gây cấn lại nằm trong cuốn sau chăng? 
Mình thích cách xây dựng tính cách của ba nhân vật chính, nhất là America nhưng khi đến phần Aspen vào cung thì tình cảm của mình dành cho America vs Aspen suy giảm đáng kể vì America rất không dứt khoát về Aspen, cô hận anh làm tổn thương mình nhưng vẫn dễ dàng bỏ qua cho anh, cô ghen khi hoàng tử Maxon dành thời gian cho cô gái khác trong khi chính cô cũng lén lút với Aspen. Không biết chuyện sẽ đi tới đâu nhỉ?
3
20349
2015-07-31 09:52:11
--------------------------
242917
3362
Trước tiên mình muốn nhận xét về bối cảnh trong truyện, dù bối cảnh hiện đại nhưng việc phân chia cấp bậc trong đó lại quá gay gắt, đến mức chia ra thứ tự từ 1 đến 8 thì mình cũng thấy hơi quá, lại còn hệ thống luật lệ vô lý và nhiều tình tiết bất công đến độ mình thấy nếu là thời xưa 1 chút thì hợp lý hơn. Lý do hoàng tử phải lấy 1 thường dân trong khi các công chúa thì bị đem đi gả cho các nước khác nhằm thiết lập liên minh cũng còn mơ hồ, chưa hợp lý lắm. Và mình đã trông đợi 1 cuộc tuyển chọn "nhiều chất xám" hơn thay vì chỉ hời hợt về ngoại hình phần nhiều thế này. Công nhận nhân vật America có cá tính, và được xây dựng hay, nhưng nhân vật này lại có 1 đặc điểm rơi vào lối mòn, đó là nhập nhằng chuyện tình cảm. Cô có thể phân vân về tình cảm, nhưng mình ko thích cái cách cô thân mật với cả 2 người và thay đổi ý kiến quá nhanh. Mới mấy trang trước còn ôm hôn cuồng nhiệt anh này, mấy trang sau đã chủ động ôm hôn anh kia :P Cũng may là cô đã có lời khẳng định rằng cô chọn chính bản thân mình, và chưa thực sự chắc chắn về ai. Dẫu sao mình vẫn nghĩ trong khoảng thời gian suy xét phân vân đó, cô nên giữ khoảng cách với cả 2 người, chứ nếu cứ vượt mức như trong truyện thì lại thành ra bắt cá 2 tay. Chính điều này làm giảm cảm tình của mình với nhân vật. Còn điều nữa là cái hoàng cung trong truyện quá dễ bị tấn công, không hiểu nổi @@ Cung điện chẳng lẽ bèo thế ư?
Dù sao cũng trông chờ vào tập tiếp theo, hy vọng tác giả thêm vào nhiều màn đấu trí trí tuệ hơn thay vì chỉ miêu tả sự thay đổi ngoại hình của các cô gái.
4
126898
2015-07-27 07:57:19
--------------------------
241445
3362
Điều đầu tiên tôi muốn nhận xét là về nội dung: Nội dung truyện khá hấp dẫn và hay. Tôi không gợi ý nhiều về nội dung để các bạn có thể tự đọc và cảm nhận. Tuy nhiên đây là cuốn sách tuyệt vời, tình tiết khá lôi cuốn. Có những lúc câu chuyện lâm vào bế tắc nhưng cũng có những lúc lại đưa người đọc tới chân trời mới.
Điều tôi hi vọng những đợt tái bản sau sẽ chỉnh sửa ở cuốn truyện này là lỗi chính tả. Ở các trang 333, 292, 229, 261, 284, 195, 246 và còn rất nhiều trang khác đều sai lỗi đánh máy và cấu trúc câu. Điều này không làm tôi phiền lòng hay thất vọng vì đôi khi người dịch không để ý kĩ càng trước khi tung sản phẩm, tuy nhiên tôi muốn NXB sẽ chỉnh sửa lại chút cho câu chuyện thêm phần hay hơn.
Có một điều tôi muốn góp ý là thay vì trong truyện, người dịch dịch là Số 5, số 4, số 3 để chỉ cấp bậc, tại sao lại không thay bằng các từ ngữ như America - công dân hạng 5, Carlee - công dân hạng 3 chẳng hạn. Tôi có đọc qua The Selection phiên bản gốc thì thấy ý của tác giả để chỉ về cấp bậc. Dịch như sách đã xuất bản có phần cứng nhắc. Lúc đầu thậm chí tôi còn không hiểu số 5 là gì cho đến khi đọc tới 1/3 truyện. 
Một điều tôi thích về tácphẩm này là cách bài trí của sách khá đẹp, điều này làm tôi không ngần ngại bỏ tiền ra mua về sở hữu cho mình một bìa sách dễ thương như thế.
5
185178
2015-07-25 14:44:12
--------------------------
238597
3362
Mình biết đến " Tuyển chọn hoàng phi" cũng nhờ tiki. Một phần là mình ấn tượng bởi bìa sách quá đẹp, một phần thấy bình luận của các bạn về cuốn sách này khá tốt. Không biết các bạn đã coi phim " Được làm hoàng hậu " chưa, nhưng mình thấy cuốn sách này có diễn biến tâm trạng các nhân vật hơi giống giống í. Nhân vật chính America siêu dễ thương. Mình rất thích kiểu nhân vật chính như thế này nhưng có vẻ hơi đi theo lối mòn. Hai chàng trai chinh phục Mer thì có vẻ hơi giống giống nhau, mình chưa thể phân biệt được tính cách của họ. Kết thúc của truyện thì hơi mất hứng vì phải chờ để được biết những tình huống tiếp theo. Dù sao thì đây cũng là cuốn sách hay dành cho những cô gái đang yêu.
4
362309
2015-07-23 12:08:49
--------------------------
226344
3362
Điều tôi ấn tượng đầu tiên với "Tuyển chọn Hoàng Phi" chính là bìa quá đẹp. Đọc những đoạn đầu, hơi hụt hẫng vì cảm giác đây là mô típ có vẻ "cũ". Nhưng càng đọc về sau, tôi lại lại bị mạch truyện cuốn theo khi nào không hay. America Singer mang nét phóng khoáng, rạng rỡ của một cô gái tràn đầy năng lượng. Maxon thanh lịch, tao nhã, dịu dàng "chuẩn" Mr.right của các cô gái. Aspen quyến rũ, nhạy cảm. Bên cạnh chuyện tình tay 3, cuộc chống lại quân phiến loạn, tình bạn cũng như khung cảnh cung điện xa hoa cũng góp phần đưa mạch truyện thêm kịch tính và hấp dẫn. Tôi chưa từng đọc The Selection series ở bản gốc, nhưng theo tôi, bản dịch này cũng khá ổn. Gấp lại quyển 1, thật sự tôi muốn xem ngay 2 quyển còn lại. NXB Nhã Nam và Tiki mau mau nhé :)
4
605811
2015-07-11 23:09:22
--------------------------
225260
3362
Một tác phẩm hay với cốt truyện độc đáo và mới lạ, tình tiết diễn ra không quá nhanh cũng không quá chậm. Phần nội tâm nhân vật được khắc họa khá tốt, khá sâu với những mối quan hệ lãng mạn, đẹp đẽ trong Hoàng cung. Tuy bối cảnh diễn ra trong Hoàng cung nhưng không hề mang màu cổ tích hay lãng mạn, sến súa thái quá thay vào đó tình yêu trở nên chân thật trở nên chân thật hơn. Mình thích nhân vật nữ chính, có cá tính và mạnh mẽ, mong chờ ở tập tiếp theo sẽ vẫn hấp dẫn như thế này. Bên cạnh đó về phần dịch thuật thì chưa được tốt lắm, mong tập sau sẽ dịch tốt hơn. 
4
401466
2015-07-10 07:05:46
--------------------------
216618
3362
Quyển này được Nhã Nam giới thiệu PR khá nhiều, lại thêm bìa đẹp nên mình cũng có cảm tình nên rước về luôn, hơi hụt hẫng khi biết đây chỉ là tập đầu trong series 3 tập :((( Nhưng nói chung mình vẫn thích quyển này vì nó khai thác đề tài mới lạ, lãng mạn vẫn là tông màu chủ đạo nhưng được lồng ghép trong bối cảnh gần như là phiêu lưu đấu trí vậy. Mối quan hệ, tình cảm, cảm xúc các nhân vật được khai thác sâu. Về phần dịch thì mình không dám nói vì mình đâu có đọc bản gốc, nhưng mình thấy cũng ổn.
4
418163
2015-06-27 23:29:31
--------------------------
214388
3362
Cô gái America Singer chỉ vừa chạm ngưỡng một phần ba quảng đường của cuộc thi "Tuyển chọn Hoàng Phi", một cuộc thi mà cô không mảy may yêu thích hay chẳng có đến một tí thích thú nào đối với nó. Chỉ do tình cờ, nói cho đúng là do một sự ép thúc, bắt buộc từ người bạn trai của cô, Aspen, mà cô đã dấn thân vào trò chơi may rủi này. Và,... kết quả thật sự không như những gì cô từng nghĩ. Cô phải lòng chàng hoàng tử điển trai, Maxon, một người khá cứng nhắc nhưng lại có trái tim nồng hậu và thấu hiểu lòng người.
Tuyển chọn Hoàng phi có cốt truyện khá hay, đi sâu vào phân tích tâm trạng, tính cách của nhân vật. Nhưng phần dịch vẫn chưa phát huy hết hiệu quả trong việc truyền tải nội dung câu chuyện, đặc biệt là những chi tiết miêu tả cảm xúc, dòng tư duy của nhân vật. Nhìn chung, khung cảnh truyện đặc sắc, mang đến cho người đọc vẻ cổ kính của đất nước Illea xinh đẹp, với cung điện nguy nga nhưng không kém phần ấm áp, gần gũi.
4
670267
2015-06-24 21:44:33
--------------------------
212030
3362
Sách hay! The Selection là một series có ý tưởng khá mới mẻ. Kiera Cass thật tài tình khi đưa người đọc đến với một thế giới tưởng chừng như cổ tích nhưng lại không hề cổ tích chút nào. Ở đó vẫn có người đẹp, có hoàng tử nhưng ngoài việc chọn vợ tưởng chừng như đơn giản lại ẩn chứa trong đó biết bao bí mật, biết bao nguy hiểm. Bản dịch ổn nhưng còn làm nhạt đi khá nhiều chi tiết đắt giá của tập này. Hy vọng ở những tập sau, dịch giả sẽ truyền tải được cái hay của tác phẩm tốt hơn.
4
201534
2015-06-21 16:07:07
--------------------------
208632
3362
Tập một của bộ tiểu thuyết Lãng Mạn mang màu sắc Huyền ảo: Tuyển Phi của tác giả Kiera Cass mang tới cho người đọc một câu chuyện đầy màu sắc về nỗ lực "đổi đời" theo đúng nghĩa đen lẫn nghĩa bóng của cô nàng tiểu thư America Singer trong cuộc thi tuyển cung phi mà với nàng đó chẳng khác nào một cơn ác mộng vậy. Nàng bị buộc phải bỏ lại sau lưng tình yêu vụng trộm của mình với chàng Aspen nghèo khó, phải rời xa quê nhà để khoác lên mình lớp vỏ bọc xa hoa và đối diện với một cuộc đời đầy sóng gió bên hoàng tử Saxon - người đàn ông mà nàng thậm chí chẳng biết một tí gì trước đó.

Câu chuyện kể về cuộc tranh đấu không ngừng nghỉ của một cô gái đơn độc giữa những hiểm nguy chốn cung đình để thay đổi định kiến xã hội, thay đổi số phận được sắp đặt cho một tương lai vô định.

Về phần bản dịch, mặc dù cảm thấy rằng đây là một cuốn sách hay và mọi người nên đọc nhưng tôi vẫn có chút không hài lòng khi tập này vẫn chưa truyền tải được những gam trầm cũng là yếu tố làm nên sức hấp dẫn của bản gốc The Selection. Tuy nhiên, tiềm năng của nó vẫn đủ để khiến tôi cảm thấy trông đợi hơn vào tập sau.
4
455391
2015-06-15 16:04:00
--------------------------
