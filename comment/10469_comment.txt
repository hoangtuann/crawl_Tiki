147808
10469
Đến với tác phẩm vì nó thuộc thể loại linglei (lánh loại), và bởi lời giới thiệu "tác phẩm xuất sắc" trên bìa (dù biết điều đó không khẳng định được gì), tôi đã có kỳ vọng khá cao.
Tuy nhiên, tác phẩm khác khá nhiều so với những gì tôi tưởng tượng. Truyện lấy bối cảnh trước năm 2000, trong một xã hội mà xe đạp, tàu hoả và bốt điện thoại phổ biến hơn nhiều so với di động, Internet.
Nhân vật chính tự kể lại câu chuyện của mình - một câu chuyện không thực sự sâu sắc nhưng chân thực, mộc mạc, có những ký ức tuổi học trò vui tươi, nghịch ngợm cùng đám bạn ký túc; mối mình tay ba lắt léo; những giây phút cô độc không nơi nương tựa, phải trộm cắp để có cái ăn qua ngày.
18 tuổi, Thẩm Sinh Thiết có những suy nghĩ còn nông nổi, có lối sống tạm bợ không chí hướng, cũng không thể tránh được bởi quanh cậu là gia đình, nhà trường và xã hội còn lạc hậu, thờ ơ với tuổi trẻ cần được chăm lo, uốn nắn. Nhưng rất may, còn có 2 cô gái luôn yêu thương và quan tâm cậu, cùng biến cố cuối truyện có thể nói đã thay đổi rất nhiều suy nghĩ và con người cậu. Biến có được tạo dựng khá hay, tiếc là được giải quyết nhanh, làm giảm tính hồi hộp và cao trào của truyện.
Tác phẩm là lời tự bạch nên mang yếu tổ chủ quan là chính, tuy nhiên có chứa đựng những triết lý sống nhất định, là lời khuyên chân thành cho những con người đang ở hoàn cảnh tương tự, vướng mắc chưa tìm ra lối thoát.
Một số điểm trừ:
- Phần đầu tác phẩm, các suy nghĩ và dẫn dắt hơi lộn xộn, nhiều nhân vật mới xuất hiện, khiến người đọc khó theo dõi.
- Lỗi lặp từ hơi nhiều, có lẽ do người dịch không trau chuốt.
- Một số mốc thời gian trong truyện không hợp lý (nhân vật chính bị đuổi học đầu năm 1999, vậy mà thời gian tiếp theo khi cậu sống một mình lại được ghi là cuối 1998 đầu 1999).
3
445579
2015-01-08 21:32:58
--------------------------
