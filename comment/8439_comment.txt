312295
8439
Bộ sách tìm hiểu về các nền văn hoá  lớn tiêu biểu của Nhà xuất bản Trẻ gồm 4 tập chứa đựng nhiều nội dung phong phú, cô đọng súc tích sẽ làm hài lòng tất cả những ai yêu thích tìm hiểu về các nền văn hoá cổ đại. Bộ sách không chỉ ăn điểm ở nội dung mà còn ở cách trình bày thiết kế rất bắt mắt, ấn tượng. Sách in màu trên giấy trắng mịn, xen kẽ phần giải thích là những hình ảnh, tranh vẽ minh hoạ rất đẹp mắt. Bố cục có phần chính, phần phụ lại logic khiến người đọc rất dễ theo dõi. Giá cả lại rất phải chăng. Tóm lại đây là một trong những bộ sách mình rất thích.
5
93234
2015-09-21 00:19:37
--------------------------
