336651
9213
Wow! Phải nói là hình thức của sách quá đẹp quá đỉnh luôn ấy, bìa cứng, giấy bóng, màu sắc, cách trang trí, ảnh minh họa tuyệt vời luôn, nhìn rất khoa học. Mình thích nhất là phần bìa sách này. Sách có phần hơi mỏng so với mình nghĩ, giá hơi mắc nhưng lượng kiến thức thì rộng, qua cuốn sách này mình biết thêm rất nhiều nhà phát minh khoa học, vật lí, các thứ mà họ đã phát minh. Chúng ta phải biết ơn đến họ rất nhiều vì họ đã ban cho chúng ta một cuộc sống đầy đủ như bây giờ. :D
4
644911
2015-11-12 19:30:48
--------------------------
309897
9213
Tôi dùng từ tuyệt hay dành cho quyển sách này, thông qua sách, tôi biết được bề dày phát minh khoa học của toàn nhân loại. Những con người quá vĩ đại, đóng góp của họ vô cùng quý giá, làm nền tảng cho sự phát triển khoa học hiện nay. Nếu bạn đang muốn tìm hiểu về những cái nôi khoa học của nhân loại thì hãy đọc sách này, vì nó sẽ đưa bạn tìm đến những nhà bác học vĩ đại và những phát minh của họ. Bạn thắc mắc  về tại sao có nhiệt kế, có bóng đèn,..quyển sách sẽ trả lời tất cả.
5
388917
2015-09-19 10:35:15
--------------------------
305169
9213
Khi nhận được sách mình khá ngạc nhiên vì quyển sách khá là mỏng, bìa sách cực kỳ đẹp và chắc chắn, giấy in cũng khá là tốt và có nhiều hình minh họa đẹp, theo mình thấy thì nó giống sách dành cho thiếu nhi. Đọc sách để biết được về cuộc đời của những thiên tài và sự ra đời những phát minh của họ, cảm thấy thán phục vì những phát minh của họ góp phần thiết thực vào cuộc sống hiện tại của chúng ta. Sách hơi mắc nhưng lượng kiến thức cung cấp cho mọi người thì không hề ít xíu nào đâu
4
394433
2015-09-16 21:06:58
--------------------------
269787
9213
Mình đã mua cuốn sách Thiên Tài - Các Nhà Phát Minh Vĩ Đại Và Công Trình Của Họ   ,nay mình có đôi chút nhận xét về cuốn sách này. Theo mình giá như vậy là hơi mắc so với một cuốn sách khá nhỏ và mỏng.Sách làm bằng giấy bóng có màu cùng những hình vẽ rất đẹp.Đây được coi như là một cuốn sách kinh điển không thể thiếu trong tủ sách của mỗi người chúng ta.Mình đem cuốn sách này tặng cho em mình,nó thích lắm.Mình cũng rất hài lòng với cách giao hàng của Tiki
3
187271
2015-08-17 19:48:56
--------------------------
