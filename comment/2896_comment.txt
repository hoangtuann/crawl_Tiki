269378
2896
Sách khá logic, nội dung trình bày theo kiểu đưa ra các mẫu câu rồi sau đó đưa chúng vào tình huống giao tiếp trong các đoạn hội thoại nên làm cho mình dễ tiếp thu. Sách viết theo kiểu song ngữ nên cũng dễ hiểu. Nội dung sách thì theo mình là khá đầy đủ và chi tiết để có thể giao tiếp trong môi trường làm việc kinh doanh hay trong tình huống giao tiếp hằng ngày. Sách còn có nhiều thành ngữ thông dụng thường dùng trong giao tiếp khá hay. Nói chung, mình thấy cuốn sách này khá bổ ích đối với mình
5
653217
2015-08-17 12:45:16
--------------------------
