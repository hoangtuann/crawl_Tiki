538773
4390
Là sách kinh tế  đầu tiên đối với mình , làm cho chúng ta có thêm cái nhìn khác về những thứ xung quanh bản thân chúng ta. Có một sự khâm phục với tầm nhìn xa của tác giả . Lãnh hội được điều mua một mặt hàng rẻ nhất là như thế nào .Là một quyển  đáng đọc để trở thành 1nhà tiêu dùng thông thái cho mọi đối tượng 
5
549125
2017-03-10 12:13:01
--------------------------
482674
4390
Lần đầu tiên mình đọc một cuốn sách về chi phí cấu tạo thành sản phẩm. Cuốn sách viết khá chi tiết, người đọc sách hiểu được các chi phí phải bỏ ra khi sử dụng một sản phẩm nào đó. Từ đó sẽ trở thành người tiêu dùng thông minh. Mặc dù đọc cuốn sách không được cuốn hút và hấp dẫn lắm những qua cuốn sách mình cũng học được một số điều khá hữu ích cho quán cà phê đang hoạt động của mình.  Nhất định rảnh sẽ đọc lại để hiểu rõ thêm một số vấn đề.


2
440889
2016-09-08 09:43:41
--------------------------
440350
4390
"Kiếm bao nhiêu tiền không phải là vấn đề, vấn đề là ở ta tiêu nó ra sao"
Cuốn sách viết bởi tác giả Yoshimoto đem lại một góc nhìn mới mẻ từ những thứ tưởng như đã quá quen thuộc mà ta thấy hiện hữu hàng ngày. Từ đó tôi học hỏi được nhiều điều, có thể kể đến như chính sách giá của các công ty nổi tiếng thế giới, những yếu tố cơ bản làm nên giá sản phẩm và cách tốt nhất để chở thành một khách hàng thông minh, tiết kiệm được tiền và chí phí liên quan khác. Với những yêu kinh tế hay đơn giản muốn chi tiêu thông minh hơn, đây là một cuốn sách hay nên đọc
4
815063
2016-06-01 16:35:12
--------------------------
418097
4390
"Khai sáng", từ dùng để tả tác động của cuốn sách này tới bản thân tôi. Rất nhiều thắc mắc từ trước giờ của tôi gần như đều có trong cuốn sách này. Nó còn khiến tôi nhận ra nhiều nhận định sai lầm mang tính chủ quan của mình từ trước đến nay. 
Đọc những phân tích mà chợt giật mình về nước mình… Ngày nào những người làm trong lĩnh vực kinh tế ở VN có thể tinh tế, nhanh nhạy như người Nhật, lúc đó kinh tế VN mới chuyển mình mạnh mẽ được. 
Hy vọng tôi sẽ sớm trở thành "người tiêu dùng thông minh".

4
542131
2016-04-18 16:59:02
--------------------------
394452
4390
Ngày nay khi mà xã hội phát triển khách hàng ngày càng được phục vụ những sản phẩm đa dạng chủng loại mẫu mã kiểu dáng. Nhưng liệu chúng ta có thực sự thông thái trong quá trình tiêu dùng? Chúng ta hiểu được bao nhiêu về giá trị của sản phẩm liệu giá trị đó có tườn xứng với giá thành? Bằng những phân tích sâu sắc kỹ lưỡng rõ ràng, tác giả đã phân tích nhiều hiện tượng trong cuộc sống bằng những ví dụ cụ thể mà gần gũi. Hãy là một người tiêu dùng thông minh nhé!
5
833136
2016-03-10 13:26:30
--------------------------
377293
4390
Một quyển sách về những chi tiêu hằng ngày rất đời thường, thông qua góc nhìn của một nhà kinh tế học bình dân Yoshimoto Yoshio. Qua đó mà những lý thuyết khô khan như giá trị thặng dư, chi phí dịch vụ,... được chuyển tải đến người đọc một cách nhẹ nhàng, dễ hiểu. 
Từng ví dụ được tác giả phân tích vô cùng chi tiết ở nhiều khía cạnh, đem đến nhiều suy nghĩ khác nhau cho người đọc nhằm chọn ra sản phẩm phù hợp nhất cho bản thân mỗi khi đi mua sắm.
Một quyển sách hay về kinh tế mà bất kỳ ai cũng có thể cảm nhận được.
4
954906
2016-02-02 09:44:08
--------------------------
365417
4390
Quyển sách thực tế và phân tích rất cặn kẽ những vấn đề tưởng như cơ bản trong cuộc sống, tiêu dùng hằng ngày. Dù sách không to và dày nhưng cách bố trí số liệu khiến người đọc không thể lướt nhanh qua mà phải nhâm nhi từng vấn đề một. Có những điều tưởng như vô thưởng vô phạt trong cuộc sống, đến khi đọc quyển này chúng ta mới nhận ra:" Ồ thì ra là vậy !" . Tác giả khiến chúng ta sẽ tăng cường quan sát nhiều hơn sau khi hoàn thành quyển sách này. Rất hay !
4
891481
2016-01-07 23:39:38
--------------------------
362073
4390
Đến star bucks mua cà phê cốc lớn, nghe cái tên là đã thấy ấn tượng rồi, quyển sách như một bài mách xem chúng ta hằng ngày đã tiêu dùng thế nào, quả thật nó như một quyển sách dạy ta về cách hạch toán mọi thứ vậy. Tôi có thể hình dung cách mà các của hàng cạnh tranh nhau nhưng cùng hưởng lợi từ nhau, tại sao mình lại mua sản phẩm này đắt hơn, woaa, quả là những điều gần gũi hằng ngày mà đôi khi chính chúng ta cũng không để ý, đối với những người trong lĩnh vực kinh doanh thì nó càng hữu ích nữa
4
622540
2016-01-01 10:49:00
--------------------------
339107
4390
Là một người không biết cách chi tiêu sao cho đúng,nên sau khi đọc phần giới thiệu sách tôi đã quyết định mua nó. Tác giả đã phân tích rất kĩ về cách tiêu dùng,lí do vì sao một sản phẩm lại có nhiều giá,vì sao họ lại mua những sản phẩm giá cao mặc dù biết giá thật của nó... hay các vấn đề về kinh tế cũng được tác giả phân tích rất rõ và có ví dụ, biểu đồ minh họa. Vì là sách về kinh tế nên phải suy ngẫm và tập trung mới thấu hiểu hết được. Tuy nhiên có một số chỗ hơi bị lặp lại. Dù vậy đây vẫn là một quyển sách rất đáng đọc.
5
674572
2015-11-17 13:56:59
--------------------------
325508
4390
Đến Starbucks Mua Cà Phê Cốc Lớn là cuốn sách mình mua giùm chị, bà chị mình thích ống cafe ở Starbucks lắm, bản thân mình có uống thử 1 lần, không ngon lắm không biết do mình gọi loại chưa xứng tầm hay sao nhưng cũng hơn 100k mà uống thua các loại bình thường mình hay uống. Trước khi đưa sách cho chị, mình có đọc thử, đa số trong sách là những công thức, những kinh nghiệm về giá, những chiến lược về cách định giá sản phẩm, tuy nhiên sách vẫn chưa cụ thể vấn đề để có thể áp dụng ở VN vì không phải nước nào cũng có nền kinh tế giống nước nào, ví dụ như Nhật Bản rất khác VN
3
575567
2015-10-23 18:22:26
--------------------------
309286
4390
Mình mua cuốn "Đến Starbucks mua ly cà phê cốc lớn" này vì mình cũng đang có dự án mở quán CF, cuốn sách này phân tích những chiến lược về giá của Starbucks và đặt ra câu hỏi, đến Starbucks bạn sẽ gọi ly cà phê cỡ lớn hay cỡ nhỏ? Từ đó áp dụng vào cuộc sống chúng ta cũng giải đáp được những câu hỏi như là: Khi đi xem phim, bạn sẽ mua bịch bắp rang bơ cỡ to hay nhỏ? Lúc cần mua trà giải khát bạn sẽ  đến siêu thị cách xa hay đến tiệm tạp hóa cạnh nhà?? Trả lời được những câu hỏi này, chúng ta sẽ học được cách tiết kiệm, học cách tính toàn và quản lý chi phí từ hôm nay. Chỉ như vậy, chúng ta mới có thể trở thành người tiêu dùng sáng suốt!!
4
643634
2015-09-18 23:05:50
--------------------------
306036
4390
Người Nhật nổi tiếng với cách sống tôn trọng kỷ luật và cách chi tiêu kinh tế thông minh. Vì thế nước Nhật luôn có một thị trường nội địa hoạt động sôi động vào bậc nhất thế giới. Các chủng loại hàng hóa được bày bán ở Nhật cực kỳ đa dạng và phong phú, điều đó khiến cho người tiêu dùng phải luôn tỉnh táo và thông minh trước khi sử dụng đồng tiền của mình để mua một thứ gì đó.
Tích nhỏ thành lớn, trong 1 lần chi tiêu thông minh bạn sẽ tiết kiệm được nhiều chi phí, tích lũy dần sẽ thành một con số không nhỏ. Chúng ta khi đi mua sắm chủ yếu chỉ quan tâm đến chi phí nổi, tức giá sản phẩm, mà bỏ qua rất nhiều chi phí lớn khác như chi phí giao dịch, chi phí cơ hội. Cuốn sách này đã phân tích cho tôi thấy được đâu là những chi phí chìm to lớn mà chúng ta phải trả khi mua một sản phẩm/dịch vụ.
Cách giải thích bằng sơ đồ dễ hiểu, văn phong gần gũi. Là một cuốn sách hay trong lĩnh vực kinh tế. Nếu bạn nào học trong lĩnh vực kinh tế thì cuốn sách này xoay quanh các nội dung trong kinh tế vĩ mô và kinh tế vi mô như: chi phí cơ hội, tính kinh tế theo quy mô, các loại chi phí trong doanh nghiệp...
Tuy nhiên, theo mình, sách có nhược điểm:
+ Có một vài chỗ văn phong còn dài dòng.
+ Một số trường hợp chỉ áp dụng đúng tại Nhật.
+ Tác giả chỉ đề cập theo hướng của một nhà kinh tế học, mà bỏ qua các yếu tố ảnh hưởng khác như thương hiệu, sự ảnh hưởng của bạn bè, người thân...
4
341353
2015-09-17 12:18:52
--------------------------
287024
4390
Người ta nói tiền nào của nấy quả là không sai ! Giá sách rẻ và theo ý kiến chủ quan của mình là nội dung hơi bị dài dòng lan man khi tác giả gỉai thích mấy các khái niệm về mua bán hàng hóa ! Mình nghĩ cuốn sách này có thể khá hơn khi ít giải thích và đi sâu vào quá trình trung gian của một sản phẩm từ khi sản xuất cho đến khi được bày bán !Lời khuyên là không nên mua cuốn này! Tuy nhiên, đó chỉ là ý kiến của mình thôi vì mình thấy nhiều người khác có phản hồi tích cực về sách này! Thân :)
2
569415
2015-09-02 10:28:41
--------------------------
283011
4390
Đến Starbucks Mua Cà Phê Cốc Lớn là một cuốn sách rất hay. Cuốn sách giúp chúng ta hiểu rõ hơn về những vấn đề kinh tế hay tài chính rất nhỏ nhặt mà chúng ta thường hay bỏ qua chúng. Cuốn sách khiến bạn phải suy nghĩ lại về những khoản chi tiêu tưởng như nhỏ mà lại không hề nhỏ đó. Tác giả phân tích sâu sắc, kỹ lưỡng, chi tiết, sách lại có nhiều biểu đồ và hình ảnh minh họa nên rất dễ hiểu và thú vị.  Hi vọng tác giả Yoshimoto Yoshio sẽ có thêm nhiều tác phẩm có giá trị thiết thực như vậy nữa.
4
562431
2015-08-29 17:09:23
--------------------------
280733
4390
Có lẽ do tác giả là người Nhật, nên mình cảm thấy yêu thích cuốn sách ngay từ cái nhìn đầu tiên, đơn giản vì người Nhật quá nổi tiếng về sự thông thái trong tiêu dùng cũng như trong cuộc sống sinh hoạt hàng ngày rồi. 
Sách có nhiều biểu đồ, tranh minh hoạ, cực kì dễ hiểu, lại vô cùng sát với thực tế.
Nói chung là mình rất thích, vì một buổi chiều hôm nọ ghé Nhã Nam chơi, lang thang lại rước được em này về, phải nói là vô cùng ưng ý luôn!!!
5
31014
2015-08-27 21:41:28
--------------------------
236926
4390
Đây là một cuốn sách phân tích & lý giải về kinh tế tiêu dùng một cách khá đơn giản & dễ hiểu. Tuy nhiên cuốn sách viết theo các chương với những chủ đề khác nhau, có một số chương rất thú vị, nhưng một số chương lại không hấp dẫn lắm. Cách đề cập & giải thích vấn đề của tác giả cũng khá hay, nhưng vì viết thành nhiều chương lẻ, nên sự liên kết tổng quan của cả cuốn sách cũng không liền mạch, như chương kết thúc khá buồn tẻ. Cuốn sách này có thể mới mẻ đối với những người không học về kinh tế, còn với những người học kinh tế hoặc có quan tâm tìm hiểu từ trước thì cách viết khá chung & cơ bản, không quá đặc sắc.
Tuy nhiên, tổng quan thì cuốn sách viết cũng khá tốt để mọi người có cái nhìn mới mẻ hơn về việc tiêu dùng, về các loại "chi phí" đi kèm mà bình thường có thể không để ý :)
4
394497
2015-07-22 08:38:12
--------------------------
231109
4390
Quyển sách này đề cập đến những vấn đề kinh tế theo cách thú vị và dễ hiểu hơn rất nhiều so với những quyển sách giáo trình khô khan, mặc dù đôi khi có những chỗ từ ngữ trong quyển sách hơi khó hiểu, không biết có phải là do dịch thuật hay không. Đối với những người không học về kinh tế thì cũng có thể hiểu một cách dễ dàng. Những vấn đề được đề cập trong cuốn sách cũng rất thực tế và gần gũi với cuộc sống, giúp người đọc có thể hiểu rõ nguyên nhân sâu xa về mặt kinh tế của những sự việc mà chúng ta có thể biết, nhưng không hiểu nguyên nhân vì sao lại trở nên như vậy.
4
149727
2015-07-17 21:43:25
--------------------------
225520
4390
Quyển sách với cách kể chuyện, so sánh hay, dẫn chứng và phân tích thú vị. Đến Starbucks Mua Cà Phê Cốc Lớn thực sự là một cuốn sách khai sáng cho những vấn đề tưởng nhỏ mà không nhỏ- những vấn đề liên quan đến kinh tế hay tài chính. Cuốn sách khiến bạn đặt suy nghĩ sâu sắc cho những chi tiêu nhỏ hằng ngày cùng lúc tìm ra hướng đi cho khả năng kiểm soát tài chính của bản thân bạn. Và đúng như mục tiêu của tác giả, hướng bạn trở thành người tiêu dùng khôn ngoan.
4
109789
2015-07-10 15:40:18
--------------------------
211056
4390
Trước giờ khi vào một cửa hàng, mình luôn gọi những món giá tầm trung để dễ lựa chọn, nhưng một lát sau, nếu tinh ý nhìn thực đơn, mình luôn cảm thấy đã bỏ ra hơi nhiều tiền một chút để có được món đó. 

Hoặc khi bạn vào quán ăn, cả một rừng thông tin ăn uống sẽ khéo léo làm bạn loạn lên, để sau cùng bạn chọn những gì mà người bán muốn bạn mua.

Nếu đa phần trường hợp của bạn đều giống vậy thì đây là một quyển sách giành cho bạn. Bạn sẽ hiểu được nguyên lý của các mặt hàng, nếu bạn là người tiết kiệm hay muốn kiểm soát túi tiền thì lại càng nên đọc nó. Nó giúp ích khá nhiều.

Tuy là những số tiền nhỏ hàng ngày, nhưng theo tháng, theo năm bạn sẽ thấy đó là một con số rất lớn. Hãy thông minh trong cách xài tiền từ bây giờ. Nhặt sách lên và tìm hiểu thôi.
4
312168
2015-06-20 07:32:37
--------------------------
209396
4390
Tôi mua quyển sách này vì ấn tượng bởi những vấn đề tiêu dùng thường nhật mà tác giả tập trung giải đáp. Những câu hỏi về tiêu dùng tưởng như đơn giản nhưng dưới sự giải thích theo quan điểm kinh tế học, Yoshimoto Yoshio giúp tôi vỡ lẽ ra nhiều điều. Tuy nhiên, có những giải thích của tác giả nghiên về lập luận kinh tế nên muốn hiểu tường tận phải tốn thời gian đọc đi đọc lại. 
Đây là một quyển sách nhìn chung là thú vị nhưng tác giả đôi khi đưa vào những ví dụ hơi rườm rà.
3
382212
2015-06-17 14:44:10
--------------------------
200938
4390
đó là chủ đề xuyên suốt của cuốn sách này. Tác giả mở đầu bằng việc đặt ra so sánh về giá chai trà xanh ở các nơi bán khác nhau và phân tích vì sao có sự khác biệt về giá rồi từ từ dẫn dắt người đọc qua nhiều chương phân tích tiêu dùng, giúp người đọc nhìn rõ hình dung và hiểu rõ hơn động thái giá cả trong cuộc sống. Tôi nghĩ với những ai quan tâm đến chủ đề tiêu dùng thì đây là cuốn sách khá cần thiết , giúp ta thay đổi tư duy về việc sử dụng đồng tiền trong đời sống hằng ngày .
4
532979
2015-05-26 11:19:25
--------------------------
192308
4390
Với tôi đây là một cuốn sách rất hay. "Đến Starbucks mua cà phê cốc lớn" đã giúp tôi có một cái nhìn khá mới mẻ về những sản phẩm hằng ngày chúng ta sử dụng, về việc chúng ta đã là " một người tiêu dùng thông minh" hay chưa? Với triết học Mác, ta biết tới các giá trị thặng dư trong xã hội nhưng đó chỉ là những kiến thức suông, nhưng giờ đây ta hiểu nó, nhìn thấy nó ngay trong các sản phẩm thực tế hằng ngày. Cuốn sách này giúp ta thay đổi tư duy về việc tiêu tiền, về cách chúng ta tiêu dùng sáng suốt, thông minh. Cảm ơn Tác giả Yoshimoto Yoshio và Công ty cổ phần sách Alpha đã mang đến cho bạn đọc một tác phẩm có giá trị.
5
624079
2015-05-04 11:52:08
--------------------------
