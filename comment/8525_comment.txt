443521
8525
Bìa ngoài trông hết sức hấp dẫn , bắt mắt. Nhưng khi đọc thì nói thật mình mong đợi hơn thế. Có lẽ do đây là cuốn được sưu tầm và tuyển chọn lại,rồi mới được hiệu đính. Nội dung đôi chỗ bị lặp, thông tin hơi thiếu.Nhưng điểm cộng của sách là giới thiệu thêm 1 số gia vị  mới cùng những tính năng hữu ích như làm đẹp, chữa bệnh....;những mẹo vặt chữa cháy cho món canh bị mặn,giảm vị cay, bí quyết bảo quản gia vị đúng cách.
Một cuốn sách thực dụng, cũng đáng để bổ sung kiến thức nấu bếp cho các bà nội trợ
   

3
842197
2016-06-06 22:37:18
--------------------------
355502
8525
Tổng quan về cuốn sách: Bìa đẹp, chất lượng giấy tốt, trang trí chi tiết trang sách bắt mắt.
Về nội dung: sách cung cấp thêm những thông tin rất cần thiết cho người đứng bếp, phân tích từng loại gia vị rõ ràng, cụ thể. Cách sắp xếp chương mục khoa học, hợp lý, dễ tìm.
Với những người nấu ăn đơn giản, món ăn cơ bản chỉ bao gồm những gia vị chính thì cuốn sách đúng là kích thích sự tò mò, khám phá thêm những hương vị mới cho những món ăn đã cũ. Với những người đam mê nấu ăn, hay nấu ăn thường cầu kì, chú trọng khẩu vị, thì cuốn sách như một cẩm nang, cũng cấp thêm những bí quyết sử dụng từng loại gia vị, ngoài công dụng tạo mùi, tạo màu vốn có.
Tuy nhiên, khổ cuốn sách khá nhỏ (so với độ dày và cách đóng gáy sách) nên khi đọc thường chỉ mở được 60-90 độ.
Điểm trừ (hoặc có thể là ý đồ của tác giả): nhiều đoạn bị lặp đi lặp lại (vì viết chi tiết từng loại gia vị, nên mỗi gia vị có thể sẽ xuất hiện ở các chương mục khác nhau, với cùng một công dụng, một cách chế biến...)
Tóm lại, cuốn sách vẫn hay, vẫn đáng có trong nhà bếp để nghiên cứu và thực hành!
4
652769
2015-12-20 01:29:23
--------------------------
333882
8525
Bản Hợp Xướng Của Các Loại Gia Vị của Tác giả Đinh Công Bảy, từ trước đến nay món ăn ngon hay không quyết định nhiều ở cách nêm nếm và gia vị chất lượng, con người đã biết dùng gia vị từ rất sơ khai, món ăn thêm phần hấp dẫn đậm đà, chua, ngọt, đắng, mặn, cay mỗi thứ điều mang nhiều cảm xúc như bản giao hưởng ẩm thực và vì thế người đầu bếp như là người nghệ sĩ nếu thực thụ có thể sử dụng gia vị một cách thuần thục, chuyên nghiệp, cuốn sách nói lên nhiều điều bổ ích, công dụng, cách sử dụng sao cho đúng và cách bảo quản.
4
728931
2015-11-08 14:43:45
--------------------------
212869
8525
Thật khó có thể nhận biết và sử dụng được đúng cách, đúng công dụng của tất cả các loại gia vị trong cuộc sống. Tuy nhiên "Bản Hợp Xướng Của Các Loại Gia Vị " đã đưa ra những kiến thức thực dụng nhất về thế giới của các loại gia vị dùng trong ẩm thực, trong đời sống mà đôi khi ta đã bỏ qua công dụng tuyệt vời của những loại gia vị quen thuộc xung quanh mình.
Sách có chất lượng in tốt, kích thước gọn có thể dễ dàng mang theo tham khảo.
Sách không chỉ dành cho các bà nội trợ mà là một sách tham khảo rất hay dành cho tất cả mọi người.
4
183662
2015-06-22 23:14:24
--------------------------
