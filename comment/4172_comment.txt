459762
4172
MÌnh mua sách lâu rồi nhưng đến bây giờ mới bình luận .
Sách mang tính mỉa mai . Châm biếm . Nhưng mình thấy nó không áp dụng vào trong thực tế những câu nói dạng mỉa mai này được . Cái mình cần là cuốn nào có thể áp dụng được cơ .
Và mình thấy các bạn bình luận đọc cuốn này mà cười như bị điên thì mình cũng không thấy điều gì đáng để cười như vậy cả . Có chăng thì chỉ là cười mỉa mai thôi . 
Nhưng cuốn sách cũng khá thỏa mãn cho những bạn đang muốn đọc để giết thời hian . Kiểu như là đọc 1 câu chuyện cười vậy .
4
593209
2016-06-26 14:03:16
--------------------------
428309
4172
Cuốn sách này viết khá thực tế. Đọc dễ hiểu, và cũng khá dễ đọc, mua cuốn này về đọc một lèo là hết cuốn sách luôn. 
Nhưng hơi tiếc là thật sự cuốn này không được như kì vọng của mình.  Cứ tưởng đọc cuốn này là phải ngồi cười, còn chuẩn bị tinh thần cho mọi người kêu mình khình khi đọc cuốn này vì thấy có một anh review là đã bị chửi vì ngồi cười một mình như người điên. 

Túm lại, cuốn sách này cũng có thể góp phần cho tủ sách bạn thêm phong phú, nhưng đừng đặt kì vọng quá nhiều, hãy bình thường thôi để đọc rồi cảm nhận từ từ.
4
678540
2016-05-11 00:05:36
--------------------------
385194
4172
Quyển sách này của Nguyễn Thiện giúp mình rất nhiều. Về việc ứng xử trong cuộc sống, mình học hỏi áp dụng vào từ những mẫu truyện trong quyển sách này. Nó làm ta bớt ngượng ngùng hơn trong các tình huống khó xử, làm ta hoạt bát hơn, giúp ta giao tiếp tốt hơn.
Qua câu truyện mình tin chắc các bạn có thể bắt gặp  hình ảnh của mình đâu đó, vì các mẫu truyện đều nhẹ nhàng mà sâu sắc. Tự cười mình có lẽ nhiều lúc các bạn đều tự cười mình đi. Và cuốn sách này giúp bạn giải quyết điều đó.
4
1061830
2016-02-23 18:30:24
--------------------------
364925
4172
Truyện hài phiếm phản diện cá tính nổi loạn , đối lập các phong cách tạm thời của người có khả năng nhận xét các khía cạnh của vấn đề trở nên nổi cộm mà tạo thành các câu truyện đặc biệt với đa dạng con người khác nhau , sống mà mãi chỉ chạy theo những thứ nổi trội , bề ngoài vật chất mà bỏ qua sự ý nghĩa nằm ẩn sâu bên trong không ai cũng thấy được , để làm sao mà có được bao mối đe dọa hàm hoài nghi đến nhau giữa bộn bề .
4
402468
2016-01-07 01:51:47
--------------------------
335015
4172
“Ta tự cười mình”, bật cười vì những tình huống hóm hỉnh trong sách rồi giật mình nhìn lại chính bản thân, chợt phát hiện ra bấy lâu nay mình vẫn hay cười giễu người khác mà không tự đánh giá lại bản thân. Giọng văn của tác giả rất bình dị, gần gũi nhưng không hề cẩu thả, trái lại mình tin rằng từng câu từng chữ đều đã trải qua quá trình chọn lọc rất cẩn thận, kỹ lưỡng. Văn phong hài hước, nội dung ý nghĩa, đây là một cuốn sách hữu ích cho mỗi chúng ta để tự kiểm điểm lại bản thân.
5
393748
2015-11-10 13:17:31
--------------------------
319356
4172
Tôi là người thích nói chuyện tếu táo, hay kể chuyện hài nhưng cũng không ít khi lấy người khác ra làm trò hề. Chính vì vậy cũng không ít lần gây phiền toái cho người khác, cho mình. Đọc xong Ta tự cười mình của Nguyễn Thiện, thực là mang lại ý nghĩa rất lớn, giúp có những tiếng cười trọn vẹn, không làm phiền đến ai. Dân gian có câu: "một nụ cười bằng mười thang thuốc bổ". Hẳn là tác giả Nguyễn Thiện đã kê đơn cho độc giả liều thuốc bổ hạng nhất cho tâm hồn mỗi người, đặc biệt với cuộc sống áp lực, phức tạp như bây giờ. Cảm ơn tác giả Nguyễn Thiện!
4
754305
2015-10-08 15:35:25
--------------------------
305180
4172
Cầm quyển sách trên tay tôi cũng khá hào hứng nhưng tôi cũng như một số đọc giả nhầm tưởng đây là một quyển sách hài hước những không phải. Nói đúng hơn thì đây là quyển sách dạy người ta cách tạo nên hài hước bằng cách tự trào bản thân mình. Đây là một điều khá hay, cần thiết trong cuộc sống hiện nay. Những mẩu chuyện được trích dẫn trong sách cũng khá hay, những câu chuyện mang đến nhiều kiến thức bổ ích cho người đọc. Bìa sách trình bày bắt mắt, cách trình bày phân chia rõ ràng, dễ đọc. Tôi thường chú ý đến lỗi chính tả trong mỗi quyển sách, trước giờ rất hài lòng với NXB Trẻ vì sách rất ít sai lỗi chính tả. Ví dụ như tiêu đề Chương 3: Cá nhân tự trào (trang 55). Nếu có tái bản lại quyển này, mong biên tập chỉnh sửa cho hoàn thiện. Và hơn hết, có lẽ do tôi còn quá trẻ, chưa hiểu biết nhiều nên vẫn chưa hiểu hết được ẩn ý cũng như những điều quyển sách hướng tới. Mong có dịp hiểu sâu hơn.
3
151536
2015-09-16 21:10:58
--------------------------
292388
4172
Cuốn sách giúp tôi luyện tập khả năng hài hước, rất sâu sắc và văn minh. Điều quan trọng nhất mà cuốn sách mang lại cho tôi là sự phân biệt rõ Tự trào- Hài hước với cười trên khiếm khuyết người khác- vô duyên.
Cảm ơn tác giả đã tâm huyết viết nên cuốn sách hay!
Trước kia tôi cứ ngỡ hài hước là do cái khiếu của người đó, cũng như bạn cao còn tôi thấp vậy. Sau khi đọc sách tôi mới vỡ ra: Hài hước có thể luyện tập được, và không hề khó để làm điều đó, sau khi bạn đọc sách! Hãy bắt đầu ngay hôm nay, cười vào cái sự nói nhịu của tôi mà tôi đã phải nói với cô giáo chủ nhiệm: dạ ạ ạ.... emmmm viết iết iết iết... nhanh hơn ơn ơn ơn... nói cô ạ!
5
769411
2015-09-07 11:30:55
--------------------------
289563
4172
Trước khi mua sách mình có đọc nhận xét trên tiki thì cứ nghĩ đây là 1 cuốn sách hài hước, tức là cuốn sách viết ra nhằm mục đích gây cười bằng chính giọng văn của tác giả. 
Nhưng khi mua về đọc hết thì thật sự mình thấy chủ yếu là tác giả đang diễn giải hình thức tự trào, là việc chúng ta tự biến những khiếm khuyết của bản thân, hoặc những điều không tốt trở thành những câu chuyện vui, giúp mọi người xung quanh thư giãn. Mình thì không thấy giọng văn của tác giả buồn cười hay trào phúng lắm, chắc mọi người thấy buồn cười là nhờ những truyện cười ngắn mà tác giả sưu tầm và chèn vào sách. 
2
53085
2015-09-04 16:34:38
--------------------------
247213
4172
Cười người hôm trước hôm sau người cười. Đôi khi ta thật dễ thấy được sai lầm hay khiếm khuyến của người khác mà không thể thấy được điều tương tự ở bản thân mình. Đây là cuốn sách đặc biệt, không chỉ có sự hài hước đơn thuần mà còn chứa đựng sự tinh tế ẩn ý sâu xa, đem chúng ta đến những cung bậc cảm xúc khác nhau, giúp chúng ta nhìn lại mình và có cách nhìn rộng lớn hơn, bao dung hơn về những điều đang tồn tại xung quanh ta trong cuộc sống này.
4
310872
2015-07-30 09:59:58
--------------------------
247109
4172
Thật lòng mà nói, mình không hài lòng với bìa sách chút nào, nhưng về tất cả các phương diện khác như là chất lượng giấy, chất lượng in, số lỗi chính tả hay nội dung sách thì đều không có gì cần phải chê trách. Giọng văn của tác giả quả thật vô cùng hài hước, đem đến cho độc giả những giây phút vô cùng thoải mái, đọc sách mà cứ cười mãi thôi. Tuy nhiên, những gì tác giả mang đến không chỉ là tiếng cười mua vui mà còn là những bài học bổ ích, trợ giúp rất nhiều cho mỗi người trên bước đường hoàn thiện chính mình.
5
449880
2015-07-30 09:50:37
--------------------------
211526
4172
mở đầu cuốn sách là những câu chuyện cười nhỏ mà tác giả dẫn dắt bạn đọc tới cái ẩn ý phía sau. Cách viết hài hước khiến ai ngay từ lần đầu đọc cũng không khỏi bật cười nhưng sau đó suy ngẫm thì lại thấm thía cái bóng ẩn phía sau nó không chỉ là tiếng cười mà còn là bài học cuộc sống. Cuốn sách cũng giúp tôi thay đổi phần nào cái nhìn áp đặt trước đây và suy nghĩ rộng mở hơn về mọi thứ xung quanh biết ứng xử sao cho phù hợp trong lối sống đô thị đa mặt và đầy cám dỗ này.
4
126394
2015-06-20 18:38:16
--------------------------
205712
4172
Tác giả cuốn sách có nói khi nào đọc xong sách thì ghi đôi lời nhận xét vào đây. Tôi cũng đã vào đây sau khi nhận cuốn sách được mấy ngày. Nhưng tôi lại quay ra ko ghi gì cả.
Phải thú thật đến hôm nay tôi vẫn chưa đọc xong cuốn sách. Thói quen của tôi là khi có sách nhất định phải đọc một lèo từ đầu tới cuối mới hả. Nhưng với cuốn sách này, tiến độ vô cùng chậm.
Tôi lại trách cái anh tác giả. Viết chi mà viết khó rứa. Vì có hôm tôi đang nấu cơm rồi bật cười phá lên khi bất chợt nhớ ra một đoạn nào đó trong cuốn sách. Tôi cũng chả dại mang nó lên cơ quan vì sếp tôi là rất hay sang phòng, mà đọc thì khó kìm nén, chả nhẽ ngồi trước mặt ổng mà cười sằng sặc thì có mà tôi cắp cặp đi chỗ khác kiếm ăn mất. Tôi cũng chả dám đọc liên tục vì sợ mình bị tẩu hoả nhập ma hoặc lỡ có ai đến nhà chơi lại tưởng tôi dạo này suy nghĩ nhiều quá nên thần kinh có vấn đề vì miệng tôi luôn trong trạng thái mở to nhất... Vì vậy mà mỗi ngày tôi chỉ dám làm có... vài trang cho nó đỡ!
Nói như vậy để khẳng định rằng đây là cuốn sách đáng để đọc, nên đọc và cần thiết đọc. Đối tượng nào cũng nên đọc vì nó có tác dụng với tất cả.
Đôi dòng thế thôi, đến giờ mụ đàn bà vào bếp rồi.
5
655625
2015-06-07 13:00:16
--------------------------
204648
4172
Ban đầu mình chọn mua quyển sách này vì tựa sách nghe rất triết lý mình nghĩ sẽ học hỏi được rất nhiều từ quyển sách này và khi đọc quyển sách rồi thì mình rất vui vì đã lựa chọn mua nó. Đọc sách mình nhận ra rằng trước đây mình thật sai lầm cười khi nhìn thấy người khác mắc lỗi mà không nhận ra rằng khi mình mắc lỗi người khác cười nhạo mình thì mình sẽ cảm nhận ra sao đây quả thật đúng như tựa sách" Ta tự cười mình". Sách cũng cho mình trải nhiệm những cung bậc cảm xúc khác nhau bất chợt khóc rồi cũng lại bật cười theo từng lời kể của tác giả. Đây là một quyển sách đáng đọc giúp ta điều chỉnh lại bản thân xây dựng mối quan hệ tốt hơn giữa người và người.
4
324101
2015-06-04 16:33:58
--------------------------
202537
4172
Về hình thức bìa được thiết kế không quá đặc sắc nhưng lại thể hiện được một phần nội dung của tác phẩm . Về chất lượng giấy in tốt , cứng , hầu như không bị sai lỗi chính tả . Về nội dung đây là một tác phẩm rất đáng đọc . Văn phong của tác giả bình dị , đơn giản nhưng lại xoáy sâu vào những tình huống trong cuộc sông của chúng ta . Cho chúng ta hiểu rõ về các khía cạnh mới lạ của cuộc sống . Đọc truyện nhiều chỗ làm tôi không thể không bật cười nhưng cũng nhiều chỗ khiến tôi rất cảm động mà rơi nước mắt . Nhìn chung tác giả đã thành công trong việc truyền tải cảm xúc của mình đến độc giả
5
635463
2015-05-29 21:05:52
--------------------------
196372
4172
Đọc xong cuốn sách, tôi vừa cười vừa khóc... thật sự vậy. Cười là vì nét hóm hỉnh, hài hước được đưa vào một cách bất ngờ khiến người đọc không khỏi bật cười sảng khoái. Còn khóc là vì bấy lâu nay mình cứ nghĩ đem người khác ra làm trò cười là hay, là giỏi... nhưng khi đọc xong, ta biết lâu nay ta đã sai.. ta đã đem cái không tốt của người để châm chọc cười cợt, có khác gì kẻ mạnh hiếp đáp kẻ yếu đâu... Vì vậy ta khóc cho cái sai lầm, ấu trĩ của bản thân...
5
634371
2015-05-15 19:35:58
--------------------------
194662
4172
Mình vừa đọc xong quyển tự trào của chú Nguyễn Thiện. Quyển sách đem tới cho mình cách thức mới về việc tiếp xúc với những người xung quanh. Quả thực trước đây mình cũng thường lấy người khác ra làm trò cười, và họ thường nổi quạu. Bản thân mình cũng thế, cũng dễ nổi quạu khi bị người khác đem ra làm trò cười. Nhưng giờ đọc sách của chú, mình nghĩ là đã biết cách tạo cảm tình với những người xung quanh bằng tiếng cười. Mình cũng thay đổi quan niệm, đúng là tự lấy mình ra để tạo tiếng cười cho mọi người xung quanh thì mọi người ai cũng vui vẻ thoải mái. Mình cảm thấy tự tin hơn trước những lời chế giễu và biết cách ứng xử trong những trường hợp như vậy. Xin cảm ơn quyển sách của chú Nguyễn Thiện, nó rất bổ ích. Chúc chú có nhiều sức khỏe và không ngừng sáng tạo thêm nhiều mẫu tự trào hài hước cống hiến cho đời. :)
5
319787
2015-05-11 14:27:57
--------------------------
192791
4172
Đọc cuốn TA TỰ CƯỜI MÌNH của tác giả Nguyễn Thiện, cảm giác vui vẻ kéo dài với tâm trạng thật thoải mái. Tác giả không dùng ngôn ngữ dễ dãi bình dân mà chọn lọc từ ngữ rất cẩn thận, chính xác, nhưng lại không lạm dụng diễn đạt hàn lâm vì vậy giúp cho mọi người đọc dễ hiểu, dễ cảm nhận một cách tự nhiên, nhẹ nhàng.
     Cuốn sách nói về tự trào, nói về hài hước đương nhiên là vui vẻ và cười mệt nghỉ. Vậy mà cái được hơn cả (không biết vô tình hay cố ý của tác giả) đó là một thông điệp rất giản dị: Muốn thực sự vui vẻ bản thân và đem niềm vui đến với mọi người, ngoài các phương pháp luận, nghệ thuật từ ngữ, v.v...thì nhất thiết phải có tâm sáng (độ lượng, khiêm tốn, nhân hậu, bao dung,,...). Đơn giản thế thôi mà có lẽ phải tu dưỡng cả một quá trình trong suốt cuộc đời.
      Để thẩm thấu những điều này nếu ta tiếp xúc trực tiếp với các học giả để được lỉnh hội có lẽ khiêm tốn nhất với chi phí cafe, nước uống thôi thì chúng ta có thể mua từ vài chục đến vài trăm cuốn sách này. Tác giả giúp cho chúng ta tiết kiệm nhiều quá, hi hi hi. Nói đùa cho vui nhưng điều chính xác này thuyết phục các bà vợ hay hơn cả đấy nhé, ha ha ha... Vui quá đi thôi...
5
625034
2015-05-05 18:08:03
--------------------------
189673
4172
Tôi biết cuốn sách này từ rất sớm và theo dõi cho đến khi ra mắt. Thành thật mà nói, tôi không nghĩ việc hài hước, tự trào lại quan trọng đến như thế, cho đến khi tôi hoàn thành xong cuốn sách này chỉ 1 giờ sau khi đặt hàng. Sách hay và nhiều giá trị đối với tôi. Nó giúp tôi có thể hóa giải những tình huống căng thẳng một cách nhẹ nhàng, chủ động tạo ra bầu không khí gần gũi thân mật với đối tác ngay từ đầu. Tôi còn học được cách kiểm soát tình huống theo cách cho lợi cho cả đôi bên trong những lần đàm phán.
5
615067
2015-04-27 20:14:53
--------------------------
189500
4172
"Ta tự cười mình" không chỉ là "bí kíp" chỉ cho ta biết cách tạo ra tiếng cười một cách chân thực và không gây mất lòng (vì ta tự cười mình :p ) mà còn là một chiếc chìa khóa dẫn ta đi đến thành công. Sách đã vạch ra một hướng đi mới, một cách nhìn mới về tiếng cười bắt nguồn từ tự trào. Rất hay!
Tuy nhiên, sách cần có thêm những câu chuyện tự trào để làm thuyết phục hơn, sinh động hơn những lý thuyết mà tác giả muốn truyền tải. 
Tóm lại, với tôi đây là một quyển sách tuyệt vời. Xin cảm ơn tác giả.
5
620747
2015-04-27 15:18:34
--------------------------
188334
4172
Theo dõi facebook của tác giả Nguyễn Thiện nên biết tới cuốn sách này cũng khá lâu rồi. Một số ví dụ trong sách cũng được đọc qua satatus của tác giả nên khi biết Tiki bán sách là đặt mua ngay. Về cuốn sách mình có một số nhận xét như sau:
Một là: Chất lượng cuốn sách tốt, giấy in dày dặn khiến nhiều khi lật trang mình cứ phải sờ lại vì tưởng lật 2 trang liền. :D
Hai là: Bìa cuốn sách không ấn tượng và tên cuốn sách dài. Tìm sách tự trào không thấy ra, tìm tự trào - đỉnh cao của sự hài hước không thấy ra, tìm ta tự cười mình mới ra sách. 
Ba là: Nội dung cuốn sách thì mới, lạ nhưng không thật ấn tượng lắm. Phần bình về cuốn sách mình thấy có vẻ phô trương và hơi quá lời.
Dù sao để cuộc sống luôn vui tươi, sảng khoái và đầy ắp tiếng cười thì chúng ta nên học tự trào và cuốn sách là giáo trình đầu tiên.
Cảm ơn tác giả!
3
470181
2015-04-25 09:11:43
--------------------------
186494
4172
Mình đang đọc cuốn Tự trào- Ta tự cười mình của anh Nguyễn Thiện , đúng là đỉnh cao của sự hài hước. Chưa đọc đến trang cuối nhưng đã rất thích vì nó vừa lạ, vừa hấp dẫn, vừa hữu ích với mình. Vừa đọc vừa tủm tỉm, đọc chậm để học, để tập tự trảo để ngấm...Đây là cuốn sách rất có ý nghĩa cho mọi người, nhất là những giáo viên như mình. Giờ học có sự hài hước, giáo viên biết tự trào sự đem lại không khí thoải mái, cởi mở, gần gũi giữa người dạy và người học. Ai muốn cuộc sống tràn đầy niềm vui hãy đọc và thực hành nhé.
4
468165
2015-04-21 12:03:39
--------------------------
185361
4172
Bất ngờ , đầy thú vị và sảng khoái , đó là cảm nhận của tôi khi đọc cuốn sách này. Tất cả những ví dụ mà tác giả nêu trong sách rất đời thường và hoàn toàn có thể áp dụng trong cuộc sống hàng hàng để “ đặc nhân tâm” nhiều nhiều hơn. Sách này không chỉ đọc một lần mà phải đọc nhiều lần , vừa cười té ghế vừa tìm cách áp dụng khi gặp bạn bè để tạo ra không khí vui nhộn, thoái mái .
Trong lần tái bản đến, đề nghị tác giả bổ sung một chương nói về cách xử trí trong trường hợp độc giả cười mà không ngậm miệng lại được !  Xem như độc giả phải “ đọc kỹ hướng dẫn trước khi đọc sách
5
307528
2015-04-19 11:18:54
--------------------------
184370
4172
Lâu rồi mất thói quen đọc sách. Hôm trước tình cờ gặp cuốn Tự trào - Đỉnh cao của hài hước - Ta tự cười mình của Nguyễn Thiện và đọc một buổi chiều hết veo 180 trang. 
Không chỉ là những ví dụ rất sinh động về tự trào, mà tác giả còn phân tích khá kỹ kết cầu một câu chuyện cười, làm thế nào để tạo ra một câu chuyện cười cũng như làm thế nào để biến truyện vui ấy thành tự trào.
Túm lại đây là cuốn sách mình khá thích, ít ra là bỏ túi dăm ba câu nói hay hay, để thể hiện với các em người đẹp mỗi khi đi nhậu. Hehe... đời còn cần gì hơn.
5
612925
2015-04-17 16:47:53
--------------------------
