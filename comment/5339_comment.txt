401004
5339
100 Truyện Cổ Tích Việt Nam là một quyển sách rất hay và ý nghĩa. Mỗi câu chuyện trong tập đều mang một mà sắc riêng nhưng đều vô cùng hấp dẫn và có ý nghĩa giáo dục nhẹ nhàng cho trẻ em. Đọc những câu chuyện cổ tích này trước khi đi ngủ cho trẻ con sẽ giúp các em dễ dàng đi vào giấc ngủ êm đềm và chắc chắn sẽ có những giấc mơ đẹp. Mình rất thích quyển sách này . Đây quả là quyển truyện cổ tích vô cùng tuyệt vời dành cho thiếu nhi.
5
1174527
2016-03-20 01:25:24
--------------------------
299566
5339
Mình đã được tặng và đọc cho con gái nghe. Cháu mới một tuổi nhưng tối nào cũng thích được đọc truyện trước khi ngủ. Đây là cuốn sách hay mang ý nghĩa giáo dục. Giúp trẻ hình thành những nhân cách tốt ngay từ khi còn nhỏ tuổi. Dù không có hình ảnh minh họa nhưng mình nghĩ truyện văn cũng tốt. Để sau này cháu sẽ tự đọc lại khi lớn. Bản thân mình có kinh nghiệm: đọc nhiều truyện văn sẽ nâng cao khả năng viết, chuẩn về chính tả, và giỏi môn văn, nâng cao khả năng giao tiếp hơn rất nhiều so với việc chỉ đọc truyện tranh.
4
172345
2015-09-13 13:02:16
--------------------------
251979
5339
 Sách có rất nhiều mẩu truyện cổ tích hay, đọc cho trẻ con cũng khá được. Nhiều câu chuyện đã đọc từ rất lâu nay có cơ hội đọc lại vẫn cảm thấy thú vị như ban đầu mặc dù có rất nhiều chi tiết hư cấu nhưng dù sao cũng là cổ tích, nội dung hướng về bản tính lương thiện và khuyên con người nên ở hiền thì sẽ gặp lành, và các điển tích giải thích về sự ra đời hay xuất hiện của một số sự vật hiện tượng của đời sống một cách rất huyền ảo và kì bí. Còn về hình thức thì chưa thật sự đẹp, chất giấy khá mỏng, nếu giấy dày hơn thì có lẽ bắt mắt và nhìn sẽ sang hơn hẳn. 
4
113987
2015-08-03 11:49:33
--------------------------
149681
5339
mình mua quyển này cũng khá lâu rồi, lúc đầu đặt mua vì nghĩ có tranh minh họa nữa, tính đem tặng đưa em để nó biết về các câu chuyện cổ tích của nước mình, có hình minh họa thì tuyệt vì là trẻ con mà. nhưng đến lúc cầm quyển sách trên tay thì cuối cùng vẫn đành giữ lại tự mình đọc vì thấy nhiều cái hay quá :P
Các câu chuyện đều rất thú vị, có nhiều chuyện gắn với tuổi thơ, cũng cso nhiều chuyện khá lạ mà mình chưa từng nghe qua nhưng nhờ có quyển này mà mở mang tầm mắt. Có đôi chỗ lỗi đánh máy nhưng không đáng kể, tuy nhiên muốn góp ý là chất giấy hơi mỏng chút, cũng không phải vấn đề quá lớn nhưng cá nhân mình thích loại giấy cứng cáp chút sờ sẽ rất thích tay
4
496784
2015-01-14 13:43:29
--------------------------
