441860
11245
Cuộc kháng chiến thần thánh của dân tộc ta chống lại hai nước hiện đại có khả năng quân sự hàng đầu thế giới đã sản sinh ra rất nhiều tướng lĩnh tài giỏi nổi tiếng. Bên cạnh những tướng lĩnh đó là những người đóng góp thầm lặng hơn ở những lĩnh vực khác nhau tập hợp lại dưới sự lãnh đạo của Hồ Chí Minh. Ở đây ta thấy bất cứ hoàn cảnh nào Việt Nam cũng có rất nhiều nhân tài và tài nhìn người, thu phục và sử dụng người tài cực kỳ tài tình của bác Hồ. Làm một phép so sánh ta thấy là giáo dục thời đó xem ra có vẻ hiệu quả và thật hơn bây giờ. 
4
504882
2016-06-03 23:59:55
--------------------------
242394
11245
Mình đã mua cuốn sách này và thấy rằng nó rất đáng để mua. Về nội dung sách thì rất thú vị và bổ ích. Tác giả cung cấp cái nhìn nhiều khía cạnh về các tấm gương và có nhiều thông tin lý thú. Tuy nhiên cuốn sách làm mình khá khó chiu khi cầm đọc vì số trang thì quá nhiều mà không gian trang giấy chưa thích hợp. Ngoài ra một số trang có chữ khá mờ nhạt gây khó chịu cho mắt khi đọc. Tóm lại, nếu bạn muốn cuốn sách giúp bạn có thêm những động lực, tấm gương hiếu học và tài năng thì nó là cuốn sách bạn đang tìm...
3
453845
2015-07-26 16:32:10
--------------------------
201288
11245
Xét về kiến thức thì cuốn sách mô tả được những chân dung của tri thức Việt nam qua những thời kì,qua các thế hệ từ thế hệ trong chiến tranh đến thế hệ khi đất nước đã hòa bình.Qua đó những người trẻ như chúng tôi thật sự tự hào khi mà dân tộc Việt nam đã sản sinh ra những người con thật sự ưu tú đến như vậy,những nhà khoa học đã xây lên nền móng cho nền tri thức Việt nam trong tương lai.Nếu bạn đang là thanh niên thì hãy mua ngay cho mình một cuốn

Xét về chất liệu giấy thì có ưu và nhược điểm:
Thứ nhất sách được đầu tư khá kỹ từ bìa cứng cho tới chất liệu nhìn rất mượt.Giấy ngả vàng
Những cho số trang rất nhiều,sách rất dày mà giấy thì không được mềm,nên lật trang sách rất khó,nhiều khi muốn gạch chân những dòng quan trọng thì cực kì tốn sức..
Dù gì thì đây vẫn là một quyển sách không thể thiếu trong tủ sách của mỗi người
4
93846
2015-05-27 00:41:49
--------------------------
178036
11245
Mình tình cờ "phải" đọc cuốn này vì lần đó ko còn gì để đọc. Vẫn biết là thể loại này ko hấp dẫn mình được. Nhưng cũng cố đọc hết thì cũng thấy ý nghĩa. Đó là ở bút pháp đầy cảm xúc khi kể về những con người, lại chính xác, khoa học khi kể về những công trình, thành tựu. Hàm Châu đã miêu tả vô cùng rõ nét và chân thực về con đường của cha ông, trí tuệ của cha anh. Họ là những con người ở nhiều lĩnh vực, là những bậc cao nhân tài đức với đầy khao khát cống hiến, phụng sự xã hội nước nhà. Qua những quan sát kỹ lưỡng, tinh tế của mình, tác giả đã vẽ nên một bức tranh hoành tráng về giai thoại lịch sử Việt Nam thời đương đại
5
435267
2015-04-04 05:45:43
--------------------------
