325479
3887
Câu chuyện được viết trên chất liệu bìa cứng rất chắc chắn, màu sắc nổi bật. Cốt chuyện vừa có tính hài hước, vừa có tính châm biếm sâu sắc. Trong "Vụ kiện bát chè", cả hai vợ chồng đều "không phải dạng vừa", chồng thì "tham ăn" chè để cúng cha mà anh ta cũng ăn trước khi mang lên cúng, còn vợ cũng không để bản thân mình bị thiệt. Sự vô lí cũng như hài hước trong cách sử kiện của vị quan nọ, đã cứu vãn được "bộ mặt" của anh chồng, nếu để hở ra, mình là kẻ đã ăn vụng bát chè cúng cha thì anh ta sẽ rất xấu hổ, đồng thời sự vô lí cũng đã cứu vãn hạnh phúc của một gia đình, ấn tượng nhất là câu: "Hai bảy đâu nhất thiết phải bằng mười bốn đâu, đôi khi hai bảy cũng bằng mười ba mà...."
4
680594
2015-10-23 17:25:12
--------------------------
