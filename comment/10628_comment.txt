149537
10628
Điểm hay của tác phẩm có lẽ ai cũng sẽ nhận ra: đó là những nốt trầm, buồn mang theo cảm xúc khó tả. Những cảm xúc khá chân thật tạo nên mạch văn nhẹ nhàng và lắng đọng.
Điểm không hay của tác phẩm là ở những truyện ngắn có cảm xúc, có hồn nhưng chưa hẳn là một câu chuyện cụ thể, có chút gì đó lang man, có chút gì đó rời rạc giữa những yếu tố trong câu chuyện.
Mình thích phần thiết kế bìa rất nhiều. Rất đẹp, rất kỳ bí. Nhưng nội dung của một số câu chuyện lại làm mình hơi thất vọng vì nó! Câu chuyện làm mình ấn tượng nhất là Cây Quỳnh Không Ra Hoa! Đẹp, dung dị và trầm, buồn!
3
303773
2015-01-13 22:15:42
--------------------------
77176
10628
Điều khiến tôi tìm đến với "Giấc mơ" chắc có lẽ là bìa sách khá đẹp chứ không chắc là nội dung của nó. Với lối viết trầm buồn và nhẹ nhàng tác giả đem lại cho người đọc một cảm xúc lạ nhưng nó chưa đủ để làm lôi cuốn các độc giả. Tôi thấy những câu chuyện trong sách khá rời rạc, không liền mạch như những cuốn truyện ngắn mà tôi từng được đọc, cuốn sách này không mang một ý nghĩa gì xuyên suốt những câu truyện. Tôi nghĩ tác giả cần trau chuốt hơn nữa về văn phong và cốt truyện để cho ra đời một tác phẩm làm hài lòng các độc giả. Tôi không thực sự thích cuốn sách này!

3
93778
2013-05-26 11:51:34
--------------------------
16384
10628
Thật ra, cuốn sách này không hay lắm. Ban đầu tôi có tình cờ xem được truyện ngắn CÂY QUỲNH KHÔNG RA HOA, tôi thực sự bị ấn tượng bởi lối viết nhẹ nhàng trầm buồn của chị. Và có lẽ chỉ duy nhất một truyện ngắn đó khiến tôi cảm thấy hay. Khi đọc thêm một số truyện ngắn khác như Đơn Độc hay Đêm Và Mặt trời trên blog của chị, tôi đổi ý. Tôi thực sự không thích cách viết trong truyện ngắn Đơn Độc, nó chỉ như là một dòng tâm trạng mông lung như viết blog chứ không thực sự là một câu chuyện. Có lẽ văn phong của tác giả này không phù hợp với tôi.
Điểm duy nhất tôi yêu thích ở cuốn sách này là bìa sách rất đẹp, rất ngọt ngào.
2
7892
2011-12-20 00:49:00
--------------------------
