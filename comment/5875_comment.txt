308220
5875
mình mua quyển sách vì những gì tên sách gợi mở làm mình khá hứng thú tuy nhiên khi đọc sách thì thất vọng vì:
-Nội dung không hấp dẫn khi văn phong trình bày rối, mông lung
-Mỗi phần viết sơ xài
-Vì tác giả là 1 người lớn tuổi nên có lẽ không thích hợp với mình
Những điều hứng thú:
+Bìa đẹp
+Học hỏi được một vài mẹo vặt hữu ích 
+Những hình minh họa trong sách khá hay
tóm lại: sách này có lẽ mình sẽ chỉ mua một lần chứ không thể đem tặng hay chia sẻ nhiều. Đối với mình sự tinh tế mà tác giả mô tả nếu áp dụng sẽ là khập khiễng
3
735400
2015-09-18 15:54:13
--------------------------
290577
5875
Tự nhận là một người còn nhiều thiếu xót cần bổ sung, lựa chọn sách qua giới thiệu của Tiki và Nhận xét của độc giả. Nhưng quả thực mình cảm thấy thất vọng vì quyển sách, cũng chỉ đọc vài chương đầu rồi bỏ.
Ngôn từ, các mục khá chung chung, vô thưởng vô phạt. Tuy cũng có một số chỗ cung cấp thêm thông tin mới cho chúng ta nhưng nhìn chung cảm thấy hơi tiếc tiền khi mua quyển này vì không có những thông tin như mong đợi. Sách hơi mỏng nhưng cũng phù hợp với giá.
3
512469
2015-09-05 15:57:46
--------------------------
255338
5875
nội dung cuốn sách tuy là không gì mới lạ nhưng thật sự lại rất hữu ích... vì những điều ta thấy rất đỗi bình thường, ai cũng biết và cũng có thể làm nhưng mấy ai đã nghĩ tới và mấy người đã thực hiện chúng điều đó một cách nghiêm túc và khoa học... cuốn sách là lời khuyên, sự nhắc nhở nhẹ nhàng mà hữu ích về cách sống thực, sống giản dị mà vẫn thanh lịch, tinh tế thông qua sự chia sẻ về kinh nghiệm sống thực tế của bản thân và những người xung quanh, qua cảm nhận và sự trải nghiệm về cuộc sống... 
4
649474
2015-08-05 22:40:33
--------------------------
240472
5875
Mình quyết định mua sản phẩm này vì tiêu đề của nó và cách thiết kế sản phẩm ^^ Hình thức sản phẩm đẹp, bắt mắt, bìa dày, giấy trắng và dày. Còn được tặng kèm 1 bookmark rất tiện lợi.
Tuy nhiên sau khi đọc được 1/3 sách và đọc lướt nội dung của toàn quyển sách thì mình khá thất vọng vì mình những điều tác giả đề cập hơi chung chung và đó là những thông tin hầu như ai cũng nghĩ ra được, không có gì gọi là "đột phá" hay xuất sắc - điều mà mình mong mỏi khi mua quyển sách này.
2
278200
2015-07-24 17:00:31
--------------------------
167532
5875
về hình thức:siêu thích. thiết kế đẹp. giấy trắng và dày. có cả bookmark. giá sau khi giảm quá là hấp dẫn :D

về nội dung: mình cực khoái phần chỗ ở và thu nhập. nó đúng như những gì mình khát khao về một không gian sống, những lời khuyên về việc bài trí phòng ngủ, gian bếp, những chi tiết rất nhỏ từ mùi hương, các vật dụng trang trí, màu sắc...hoàn toàn phù hợp với những cô gái luôn ấm ủ một ngôi nhà thư thái trong tương lai

phần quần áo, có những quan điểm như rà trúng tim đen nên bản thân mình cảm giác như được...khai phá :)))). tuy nhiên, do tác giả là ng nước ngoài tha hf ra góc nhìn nhận và qan điểm đương nhiên ko thể nào đo ni đóng giày cho thị hiếu ng đọc Việt.

phần ăn uống: ôi mình lười phải động não cân nhắc ăn uống ntn là phù hợp lắm. nh kiểu gì mình cũng phải học vì bản thân mình cũng muốn vun vén cho gđ.

tóm lại là, đúng như nhận xét của các độc giả khác "nếu bạn chưa từng đọc về cách quản lý tiền và ngân sách, thì cuốn sách này dành cho bạn"

là ng trẻ, tại sao chúng mình ko cố gắng sống thông minh hơn mỗi ngày :)
4
128360
2015-03-14 22:25:55
--------------------------
165588
5875
Có lẽ vì tên cuốn sách mà mình mua cuốn này ^^

Nhưng mình nhận ra một vài thứ khi mua về. 

Điều mình thấy hữu ích cho mình nhất là một số gợi ý về du lịch và thực phẩm. 
- Mình thích ý tưởng đi du lịch chỉ với một chiếc ba lô, để chỉ mang đi những gì thật sự cần thiết. 
- Nói đến thực phẩm, cô ấy nói đến những thực phẩm tốt cho sức khoẻ nhưng kết hợp và chế biến theo một cách đầy sáng tạo và không nhàm chán. Mình khá thích suy nghĩ này của cô ấy và mình cũng muốn mình dùng ý tưởng này cho cuộc sống của mình.

Điều mình thấy không hợp với mình 
- Đây là sách của tác giả nước ngoài nên mình thấy có rất nhiều thứ không phù hợp với văn hoá cũng như cuộc sống của nước mình nên không dùng được.
3
32038
2015-03-10 18:31:02
--------------------------
134027
5875
“Không tốn kém mà vẫn thanh lịch, tinh tế" là cuốn sách khá hay, rất cần cho những ai quan tâm đến vẻ ngoài của mình, nhưng thường hay bị "viêm túi" . Cuốn sách có bìa ngoài khá hấp dẫn, chất lượng in cũng rất tốt, giấy in mịn, đẹp,... Trong thời buổi kinh tế còn khó khăn như hiện nay, việc giải quyết bài toán kinh tế cá nhân sao cho vừa đảm bảo nhu cầu cá nhân, vừa đảm bảo chi tiêu hợp lý,... là một bài toán khó. Nếu như có thể tiết kiệm một khoản trong việc mua sắm quần áo, trang trí nội thất,... mà vẫn đảm bảo giá trị thẩm mĩ và tinh thần, thì đó là một điều tuyệt vời,.... Với ngôn ngữ dễ hiểu và những ví dụ thực tế, cuốn sách là một cuốn cẩm nang hữu hiệu khi mua sắm quần áo, chọn mua nội thất, hay chuẩn bị cho một chuyết đi du lịch,... Đặc biệt, cuốn sách được viết bởi một nữ tác giả, nên với những ai còn độc thân, thì đây là một lựa chọn hoàn hảo :)))
5
414919
2014-11-07 22:26:50
--------------------------
