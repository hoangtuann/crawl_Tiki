420662
3512
Một cuốn sách của một con người đầy trải nghiệm quý báu. Tác phẩm cung cấp khá nhiều những thông tin du lịch rất hữu ích, cũng như đặt ra những câu hỏi mang đầy tính trách nhiệm đối với văn hóa, du lịch của nước nhà. Như Thái Lan, một người hàng xóm bé nhỏ bên cạnh chúng ta những cách họ làm du lịch thiệt khiến chúng ta ngưỡng mộ và học tập. Đọc sách của anh khiến bản thân học được khá nhiều điều hay, có ích, khuyến khích mọi người tìm đọc và trải nghiệm cảm xúc qua từng trang sách.
4
52094
2016-04-23 18:02:23
--------------------------
339689
3512
Hành trình du ký qua từng trang sách như dẫn người đọc qua các vùng đất, con người, văn hóa với nhiều nét rất riêng, rất đặc trưng. Tác giả có kiến thức khá rộng và sâu nên những minh họa, liên tưởng qua các địa danh làm người xem không khỏi bất ngờ, thú vị. Tác giả có óc quan sát tinh tế, ngòi bút sắc sảo nên những điều tưởng chừng bình thường ở những vùng đất khác nhau được dẫn dắt khéo léo đến những gì thật gần xung quanh ta, giúp người xem khám phá được vẻ đẹp rất dung dị ở đất nước mình, văn hóa nước mình. 
Cuốn sách rất phù hợp với những ai yêu du lịch, thích khám phá và tìm hiểu thế giới tươi đẹp xung quanh ta
5
408979
2015-11-18 15:14:27
--------------------------
339542
3512
Không hiểu sao tôi lại có thiện cảm đặc biệt với tên quyển sách này: "Trên đường về nhớ đầy"- một ấn tượng vừa phải để không bị lãng quên giữa "rừng" sách viết theo dạng du kí, một "níu kéo" vừa đủ để cầm trên tay là chỉ muốn đọc ngay.

Tác phẩm không mời gọi, cũng không phô trương. Đơn giản là những ghi chép theo đúng kiểu...ghi chép nhưng có một sự tỉ mẩn, nghiêm túc và sâu sắc. Đọc lời bạt lại càng thấy ngưỡng mộ và khâm phục thói quen này của người viết. Có lẽ vì thế mà quyển sách không khiến người ta xuýt xoa trước những câu văn bay bổng, gợi hình, gợi cảm; cũng không dào dạt tinh thần cá nhân mà ít nhiều thể loại này thường có; nó sống động theo cách riêng, đầy tâm hồn và tình cảm.

3
172604
2015-11-18 10:56:41
--------------------------
308910
3512
Đây là một quyển sách đậm chất du ký. Những ghi chép rất chân thực về những nơi tác giả từng đi qua, từng nhớ, từng cảm như một châu chuyện giúp ta hiểu và cảm hơn những văn hóa, những dân tộc.
Qua quyển sách này, bạn không chỉ thấy được những góc khuất của đất nước, con người, mà bạn còn nhận ra vẻ đẹp của người Việt, văn hóa rất đẹp của Việt Nam được tác giả khéo léo đưa vào qua những tình huống ngẫu nhiên.
Quyển sách vô cùng thích hợp cho những ai yêu thích du lịch, cũng như đam mê với văn hóa, con người. 
5
252682
2015-09-18 21:32:58
--------------------------
