557233
11329
Sách thường chia làm 2 loại: 1 loại bạn ko thể bỏ xuống và 1 loại lết mãi mới hết. Không may là TGBQA vs tôi lại là loại thứ 2, chủ yếu do diễn giải có phần khó hiểu và vài trích dẫn cũng tối nghĩa. Mối liên kết giữa các phần ko hấp dẫn như tôi mong đợi. Thật sự vì quá yêu thích Lược sử vạn vật và Carl Sagan là cái tên đk nhắc đến quá nhiều và đánh giá về sách tốt nên tôi đã thử. Đây có lẽ vẫn là cuốn sách bổ ích, nhưng văn phong ko phù hợp với khẩu vị của tôi. Phải chi có thêm sách của Bill Bryson thì tốt
2
1512857
2017-03-29 02:08:44
--------------------------
517153
11329
Theo cá nhân, quyển này còn hay và thực tiễn hơn cả quyển Vũ Trụ của cùng tác giả.
5
1724984
2017-02-01 14:32:11
--------------------------
467149
11329
Mình rất thích tác giả Carl Sagan qua cuốn Vũ trụ nên đã mua thêm cuốn này về đọc. Thế Giới Bị Quỷ Ám - Khoa Học Như Ngọn Nến Trong Đêm đã mang lại những hiểu biết về thế nào là khoa học? Thế nào là phản khoa học? Ông đã đưa ra những lời giải thích cho những thứ phản khoa học mà có lẽ trong chúng ta đôi khi đã tin vào nó.
Quyển sách mình nhận được có bìa bị xước và ở gáy sách cũng bị, mình nghĩ tiki nên lưu ý việc bảo quản và xem chất lượng của quyển sách trước khi giao cho khách hành.
5
1121287
2016-07-03 12:48:21
--------------------------
410635
11329
khi nhận được sách mình cảm thấy rất hài lòng.sách được bọc cẩn thận,không bị cong mép.trên thế giới vốn dĩ có nhiều điều mà khoa học hiện tại chưa lí giải hết được.bìa sách rất ấn tượng và tựa sách cũng vậy,gợi cho tôi một cảm giác mù mờ,không điểm tựa,không biết điều gì đúng,điều gì không đúng.sách của carl sagan khiến mình luôn cảm thấy hài lòng bởi lượng kiến thức khủng mà sách mang lại,bởi lời văn giản dị không cầu kì làm độc giả khó hiểu.mong rằng sau này tiki sẽ bán thật nhiều sách khoa học hay như thế nữa.cám ơn tiki!!
5
543671
2016-04-04 16:16:24
--------------------------
367555
11329
Vừa nhận được cuốn sách này va rất không hài lòng. Không phải nội dung sách mà do tình trạng sách rất tệ. Sách bị bong ngay cuối gáy, bìa bị trầy rất nhiều,khoảng 2/3 bìa, có nhiều chỗ hằn sâu vết trầy luôn. Lúc mới lấy ra sách còn một lớp bụi nữa. Không biết TIKI bảo quản thế nào mà sách cứ như sách cũ vậy. Bìa sau và gáy sách cũng bị trầy nhiều. Cầm cuốn sách trên tay mà mình không khỏi giật mình và thất vọng . Không biết những quyển khác như thể nào nhưng mình không hiểu sao TIKI có thể giao một quyển sách như vậy cho khách hàng. 
1
657779
2016-01-12 12:18:49
--------------------------
293293
11329
Mình rất thích Carl Sagan kể từ cuốn "Vũ Trụ", nên khi biết cuốn "Thế giới bị quỷ ám-Khoa học như ngọn nến trong đêm" được xuất bản thì mình đã không ngần ngại đặt mua ngay. Khi nhận được sách, về hình thức mình thấy bìa sách rất đẹp, chất lượng giấy in rất tốt (đọc không bị lóa mắt). Về nội dung, chủ đề xuyên suốt cuốn sách này là về UFO (vật thể bay không xác định) và các cuộc bắt cóc bởi sinh vật lạ, các phép lạ hay phù thủy, các tôn giáo .... Cuốn sách này chính là một cuộc hành trình khoa học thú vị, sáng tạo, giúp mở mang sự hiểu biết, khai sáng cho những vấn đề bấy lâu nay được xem là thần bí, được hiểu sai bởi sự ngô nghê của loài người, hay được dẫn dắt một cách tinh vi của giới truyền thông thậm chí là cả những nhà khoa học, bác sỹ.
5
80476
2015-09-08 10:05:06
--------------------------
255445
11329
Cuốn này mình đọc được khoảng 250/700 trang. Cuốn này thuộc thể loại khoa học, lúc mình mới đầu đọc thì cảm giác là chẳng hiểu j cả, về sau thì dễ hiểu hơn, Khi đọc bạn sẽ biết được nhiều sự thật: UFO có thật không, tên gọi "đĩa bay" bắt đầu từ đâu, có nói về tôn giáo, lí do ngày xưa bọn Tây tiêu diệt phù thủy,... (mình chưa đọc hết chưa biết phần sau nói về j). Tác giả ban đầu đưa ra những câu chuyện, dẫn chứng(của báo chí, người dân,..) làm cho bạn tin rằng có thể có UFO. và sau đó thì tác giả lại lí giải, tìm ra mấu chốt và nghịch lí của câu chuyện đó để giải thích được rằng UFO là có thật hay không! 
Những chương đầu nói về UFO khá nhiều.
Tóm lại cuốn sách này giúp bạn mở mang tầm hiểu biết, có cái để chém gió, một cuốn sách của sự thật, khá lôi cuốn người đọc và không dành cho ai nhìn thấy chữ nhiều thì ngán (700 trang và toàn là chữ nghịt cả trang). 
Mình chưa biết bản tiếng Anh như thế nào nên không nhận xét về việc dịch sách, cơ mà mình đọc thì vẫn có thể hiểu được ý chính  
"túm lợi lờ" nên đọc (ý kiến chủ quan thôi nha) 
4
367737
2015-08-06 01:24:00
--------------------------
254338
11329
Tôi mê mẩn Carl Sagan từ sau khi đọc cuốn Vũ trụ của ông do Nhã Nam phát hành nên khi Thế Giới Bị Quỷ Ám - Khoa Học Như Ngọn Nến Trong Đêm xuất hiện trên tiki trong mục sắp xuất bản tôi đã ngóng chờ mãi. Nhớ lại hồi ấy suýt phát điên vì chờ mãi không thấy sách ra. Mua được rồi thì lại thất vọng với cái bìa, đơn giản tới mức qua loa đại khái. Nhưng nội dung thì lại quá mức tuyệt vời. Biết bao nhiêu điều í ẩn về vũ trụ, về khoa học, cuộc sống được tác giả giải đáp cặn kẽ và hấp dẫn.
5
167283
2015-08-05 10:12:56
--------------------------
229827
11329
Đặc biệt thích những cuốn sách phổ biến khoa học như thế này. 1 cuộc hành trình vô cũng thú vị xuyên suốt lịch sử.
Rốt cục những tin tức giật gân trên báo đài hầu hết là những thứ bịa đặt, ngụy khoa học. Cuốn sách này làm mình cảnh giác hơn vs những thông tin mình xem đc, 
Những thứ thường chiếu trên TV hầu hết đều rất nông cạn, chỉ có những cuốn sách như thế này mới giúp chúng ta đến vs những kiến thức thật sự.
Cho dù thế giới có bị bao trùm bởi lũ quỷ thì vẫn còn đó khoa học chân chính như 1 ngọn nến hy vọng  :)
 ""Đâu đó ngoài kìa vẫn có những điều kỳ diệu đang chờ để được khám phá" ("Somewhere, something incredible is waiting to be known.")
5
359955
2015-07-17 08:54:30
--------------------------
226022
11329
Sagan chính là một trong những người đầu tiên khai sinh ra dòng sách khoa học cao cấp, những cuốn sách chứa đựng chất thơ đầy lãng mạn nhằm truyền tải những vấn đề khoa học tưởng chừng khô khan. Ông đã làm sáng tỏ những giá trị đích thực của khoa học và việc khoa học có thể thay đổi cách nhận thức của con người và giải thoát cho họ khỏi sự khiếp sợ trước thần linh.
5
112376
2015-07-11 11:41:15
--------------------------
216272
11329
Đây là một cuốn sách khai sáng tư duy loài người. Chỉ ra sự ám muội, tối tăm của con người suốt cả một quá trình lịch sử, bằng việc nêu lên sự sai lầm, cả tin, thụ động của loài người từ trong khoa học, những vấn đề nóng hổi của nhân loại như sự sống ngoài trái đất, những dấu hiệu bí ẩn trên hành tinh chúng ta, những hiện tượng quỷ ám,... đến những vấn đề về chính trị, xã hội từ cổ đại cho đến ngày nay, kiềm hãm sự phát triển của loài người trong hàng thế kỉ.
Hẳn các tín đồ thích những thể loại huyền bí sẽ thất vọng khi nhận ra nội dung trong sách không hoàn toàn huyền bí như tên gọi của nó - một cái tên nghe rất "tâm linh", có lẽ một lần nữa, ông đang thách thức sự cả tin của các độc giả.
Tôi đặc biệt rất thích cuốn "Vũ trụ" của Carl Sagan, và do đó tôi tin rằng cuốn sách này cũng tuyệt như vậy, nhưng tôi nghĩ ở đây có một chút vấn đề về việc dịch sách, có quá nhiều ý trong sách trở nên phức tạp và dài dòng trong khi theo tôi nó có thể đơn giản hơn thế. Nhìn chung, đây là một cuốn sách có giá trị to lớn khi đánh giá và nhìn thẳng vào những vấn đề tế nhị không chỉ của nước Mỹ nói riêng (quê hương tác giả) mà còn trên thế giới nói chung.
4
450704
2015-06-27 14:50:54
--------------------------
213336
11329
Mình thật sự bị thu hút với quyển sách ngay từ bìa sách và câu "Khoa học như ngọn nến trong đêm" gây sự tò mò vô cùng lớn.Giờ mình đã hiểu ý Carl Sagan muốn nói, khoa học và các phương pháp khoa học là sự cố gắng cao quý , nó như một ngọn nến không thể dập tắt của nhân loại trên con đường khám phá khoa học bao la .
Với lối văn gần gủi , Carl Sagan sẽ dắt chúng ta đi theo những UFO , nhữn vụ bắt cóc ngoài hành tinh , phù thuỳ, tôn giáo và còn nhiều những chủ đề hấp dẫn khác .
Đây là quyển sách không thể thiếu cho những bạn thích khoa học và luôn tìm đến sự khám phá .
4
554150
2015-06-23 17:53:36
--------------------------
128114
11329
Với giọng văn đầy lôi cuốn, tác giả đã cho chúng ta thấy một cách tổng quát về thế giới quan hiện tại và cách để nhìn nhận sự việc, cách ĐỌC thông tin không chỉ trong nghành thiên văn đầy bí ẩn mà chính trong cuộc sống hằng ngày. Muốn có được một cách nhìn tự do và thông suốt, chỉ có cách duy nhất đó là dựa vào khoa học, vì đó là con đường thẳng nhất, khách quan nhất và trong sáng nhất trong thế giới bị che mờ bởi bóng đêm của ảo vọng.
5
387692
2014-09-29 11:28:56
--------------------------
119347
11329
Lại thêm 1 cuốn nữa của Carl Sagan khiến mình vô cùng hài lòng. Vốn hâm mộ nhà khoa học này nên mình đã lập tức mua nó mặc dù đọc tựa đề cũng hơi khó hiểu. Với giọng văn thống nhất từ cuốn Vũ trụ tới Thế giới bị quỷ ám, rất gần gũi, không hề quá cao siêu hay hàn lâm. Thế giới bị quỷ ám - Khoa học như ngọn nến trong đêm nói đến tầm quan trọng của khoa học đối với tất cả lĩnh vực của sự sống. Tuy nhiên, nó lại bị che mờ bởi nhiều thông tin mang tính chủ quan, có phần ảo tưởng và mang hơi hướng "tự bẻ cong theo hướng con người muốn thấy". Carl Sagan đi vào phân tích một vài hiện tượng mà trước giờ chúng ta những tưởng nó phải là như vậy sau khi bị tung hỏa mù bởi quá nhiều thông tin mà chính chúng ta cũng không cần xác minh xem độ chính xác tới đâu. Với những lý lẽ thuyết phục, chúng ta không thể không đặt ra những câu hỏi đối với những sự kiện mà mình không hề nghi ngờ trước đây như "Thật sự UFO là như thế nào?", "Sự ảo tưởng đến với chúng ta có chăng là bình thường", để từ đó có được cái nhìn khách quan và chọn lọc hơn với mỗi thông tin mình thu nhận được. Như Carl Sagan nói, thế giới thì đang bị che mờ, thế mà khoa học chỉ như ngọn nến le lói trong đêm", và rồi "muốn không sợ bóng tối thì phải thắp lên ánh sáng".
4
9359
2014-08-04 14:59:51
--------------------------
