458484
9869
MÌNH biết đến cuốn sách này trong chuyến đi tham quan gian hàng sách ở nhã nam . Mở ra vài trang đọc thấy cũng thích nên quyết định lên tiki mua .
Cuốn sách chủ yếu đưa ra những lời khẳng định người giàu như thế nào ? Người nghèo ra sao . Sau đó đưa ra diễn giải cho những lời khẳng định đó theo ý kiến chủ quan của tác giả .
Khi đọc xong cuốn sách thì mình cảm giác khônh thích lắm vì cách diễn giải quá dài dòng những điều mình đã biết thông qua những bài viết trên mạng .
4
593209
2016-06-25 10:18:42
--------------------------
389555
9869
Cả 10 điều tác giả liệt kê ra trong quyển sách này đều thực sự rất đúng. Ai cũng mong muốn mình trở nên giàu có, nhưng chỉ là mong muốn mà họ không thành động và suy nghĩ của mình để đạt được mục tiêu đó. Có thể hiện tại bạn không giàu nhưng hãy tư duy và hành động như một người giàu có, thì có thể trong tương lai bạn sẽ trở thành chính họ.
Tin rằng sau khi đọc xong quyển sách này, bạn sẽ dễ dàng được biết mình đang nằm ở nhóm người nào, và bạn còn muốn mình tiếp tục ở trong nhóm người đó nữa hay không. Nếu không muốn tiếp tục ở nhóm người người đó, hãy thay đổi, ngay từ bây giờ.
5
739515
2016-03-02 12:01:07
--------------------------
368176
9869
Mình rất ấn tượng với bìa sách màu vàng bắt mắt, vì màu vàng thường khơi gợi sự sáng tạo và sức sống khi nhìn vào. (khi mệt mỏi, chán nản hãy tìm sách có bìa vàng đọc nhé ;) )

Cuốn sách như món quà vừa để dạy dỗ, vừa là lời tâm tình của người cha dành cho những đứa con của mình, như của hồi môn ấy :)).

 Lối viết vô cùng Dễ Đọc, Dễ Nhớ và Dễ Thấm. Mình rất thích sách nào có hình ảnh minh họa, và tuyệt vời là cuốn này vừa có hình ảnh sinh động vừa kèm những trích dẫn hay Phải Nhớ mình muốn highlight nữa. Perfect!

"Trên đời có 3 loại người. Những người làm nên, những người chứng kiến và những kẻ bàn luận về những gì đã diễn ra." Vậy bạn muốn là loại người nào? ;)

Hãy đọc cuốn sách vàng hấp dẫn này để tìm câu trả lời qua những bài học hay và thiết thực nhé! <3
4
579479
2016-01-13 14:44:56
--------------------------
336531
9869
10 Điều Khác Biệt Nhất Giữa Kẻ Giàu Và Người Nghèo, một quyển sách ngắn gọn như xúc tích, có những điều tôi rất thích như: Người giàu thì bàn về những ý tưởng - người nghèo thì thích buôn chuyện tào lao. Nó đã bàn đến những tư duy tích cực mình nên làm theo. Tác giả đã so sánh những tư duy khác nhau của người giàu và người nghèo từ những điều trong cuộc sống của tác giả. Khi đọc xong quyển sách này tôi nghĩ mình sẽ có những tư duy tích cực hơn để giúp cuộc sống của mình
5
952849
2015-11-12 16:48:13
--------------------------
335067
9869
Chưa đến 200 trang sách khổ nhỏ, mà tác giả đã mang đến cho mình một suy nghĩ mới, khác hẳn so với hững điều mình suy nghĩ trước đây, về người giàu. Bản thân mình cảm thấy cuốn sách thực sự hữu ích. Tác giả không chỉ hướng người đọc đến những suy nghĩ của người giàu, mà còn dẫn người đọc đến với những suy nghĩ đúng đắn, giúp mỗi người có thể tự định hướng cho bản thân một con đường làm giàu phù hợp nhất!
Mình nghĩ rằng đây là cuốn sách khá hay và nên đọc, phù hợp với những người từ 18 tuổi trở lên.
5
557217
2015-11-10 14:18:20
--------------------------
296408
9869
Một cuốn sách hay nói về lối sống của con người. Tác giả phân tích cũng như đưa ra những lời khuyên từ trải nghiệm của chính bản thân. Sách chỉ ra rõ rằng thái độ sống, thói quen hằng ngày và cách chúng ta tiếp nhận và xử lý những điều xung quanh đều góp phần không nhỏ tạo nên bản chất con người ta. Và chính những cách sống khác nhau tạo nên những cuộc đời khác nhau. Và những người giàu (hay người nghèo), họ đều có những điểm chung trong cách sống, những điều tạo nên cuộc đời của họ. Tác giả phân tích khá cụ thể và chi tiết, giúp độc giả, cụ thể là các bạn trẻ, có thể từng bước rèn luyện bản thân theo những thái độ sống tích cực của những người thành công, hay còn gọi là người giàu.
4
82495
2015-09-11 06:43:05
--------------------------
254763
9869
10 điều khác biệt nhất giữa kẻ giàu và người nghèo của Keith Cameron Smith viết lại những trải nghiệm của ông để cho mọi người có một cách nghĩ khác về cách làm thế nào để thành công. Mình nghĩ cuốn sách tuy ngắn gọn và không chỉ ra phương thức a,b,c nào đó rõ ràng cụ thể trên con đường đi tới thành công nhưng chúng ta vẫn có thể gián tiếp học được cách đi tới thành công qua 10 điều khác biệt như: cách suy nghĩ, cách sống, cách tiếp nhận xử lý thông tin, tầm nhìn chiến lược dài hạn... Mình nghĩ đây là cuốn sách ý nghĩa, góp phần giúp chúng ta nhìn nhận và thay đổi chính mình như tác giả đã nói "Tôi tin rằng mỗi chúng ta đều có một bài ca định mệnh và cuốn sách này chính là một trong những nốt nhạc trong bài ca đó".
4
164722
2015-08-05 14:45:58
--------------------------
245718
9869
Nội dung sách là ghi nhận của tác giả qua kinh nghiệm thực tiễn về những khác biệt trong thói quen, lối sống, quan điểm và cách suy nghĩ của người giàu và người nghèo. Cuốn sách này hướng chúng ta đến cách sống và quan điểm đúng đắn hơn, loại bỏ các suy nghĩ cản trở bước tiến trong cuộc sống và sự nghiệp. Nội dung sách ngắn gọn, súc tích nhưng chứa đọng nhiều thông tin bổ ích. Sách được tin khổ nhỏ, trang giấy mỏng giúp bạn dễ dàng mang theo cũng như dễ cất giữ. Sách cũng có thể hữu ích nếu được dùng làm quà tặng.
4
167542
2015-07-29 00:14:04
--------------------------
191692
9869
Các bạn biết đấy, “Giàu” luôn là điều mà chúng ta ao ước và mong muốn có được nhưng khi nhìn lại bản thân “Sao mình vẫn nghèo?” và rồi chúng ta lại từ bỏ và nghĩ rằng những người giàu là ăn may hay họ làm đủ mọi cách thậm chí bất chấp thủ đoạn để được giàu có, thế là ta lại dè bỉu và không cho mình có cơ hội tiếp xúc với sự thật đằng sau đó. Chúng ta đều có thể giàu bằng cách có đạo đức và hãy dẹp bỏ những ý niệm khiến chúng ta nghèo ngay từ bây giờ.
Tác giả khá tài tình trong việc đưa ra “10 sự khác biệt giữa người giàu và người nghèo”, để rồi so sánh, phân tích cho độc giả thấy được nguồn gốc của giàu và nghèo mà từ từ rèn luyện bản thân để thay đổi quan điểm và có cuộc sống tốt hơn cũng như giàu có hơn.
5
45667
2015-05-02 13:56:47
--------------------------
170522
9869
Trong xã hội, người giàu có chỉ chiếm số phần trăm rất nhỏ nhưng số người ấy lại nắm trong tay phần lớn của cải của loài người. Vậy đâu là thứ làm nên khác biệt lớn như vậy ? Có rất nhiều lý do khiến những đó giàu có hơn đại đa số cư dân trên địa cầu nhưng Keith Cameron Smith đã tóm gọn thành 10 lý do rõ ràng nhất và trình bày trong tác phẩm này. Tác phẩm này nhận được rất nhiều ý kiến trái chiều nhưng theo tôi nội dung trong sách rất thực tế chứ không trừu tượng. Hãy đọc 10 điều khác biệt giữa kẻ giàu và người nghèo để biết tại sao lại có sự khác biệt lớn đến thế.
5
387632
2015-03-20 09:24:49
--------------------------
144432
9869
Niềm đam mê kinh doanh đến với tôi khi tôi học lớp 7. Từ đó, tôi bắt đầu tìm đọc rất nhiều sách kinh doanh. Và đây là một trong những cuốn sách khiến tôi rất hài lòng vì cách viết ngắn gọn, súc tích (điều này giúp tôi có thể đọc lại cuốn sách này hàng tháng mà không cần mất nhiều thời gian :v), hình ảnh sinh động, gây ấn tượng. Nó giúp tôi biết được và hiểu rõ hơn những sự khác biệt của hai tầng lớp. Cảm ơn tiki đã mang đến cho chúng tôi những cuốn sách bổ ích!
5
445522
2014-12-26 20:23:48
--------------------------
86042
9869
Cách viết đơn giản, dễ hiểu của tác giả Keith Cameron Smith có lẽ chính là điều thu hút tôi đầu tiên. Ngôn từ và cách hành văn ngắn gọn giúp tôi nắm bắt vấn đề nhanh chóng. Có thể tác giả không cung cấp một số liệu rõ ràng về vấn đề bàn đến trong cách nhìn nhận sự khác biệt giữa “Giàu” và “Nghèo". Song thông qua chính kinh nghiệm từ bản thân ông, ông đã cho thấy những hiểu biết thú vị sâu sắc về sự khác biệt - cách suy nghĩ của con người, điều thực sự đã tạo nên khoảng cách giàu nghèo. Thông qua đó, tác giả đã tiếp thêm niềm tin vào bản thân cho người đọc, cũng như tiếp thêm sức mạnh để thực hiện những ước mơ trong cuộc đời mỗi con người.
4
6756
2013-07-08 21:11:52
--------------------------
68829
9869
Khoảng cách của phân giới giàu - nghèo đôi khi tưởng chừng như xa xôi, tít tắp, nhiều người sinh ra đã sống trong sung túc, nhiều người lại phải sống trong cơ cực và chính lúc ấy người ta lại đổ lỗi cho số phận. Vâng, đúng là số phận quyết định ta sinh ra như thế nào nhưng chính cách ta sống, cách ta suy nghĩ, hành động mới quyết định rằng ta nghèo hay nghèo. Đến với "10 điều khác biệt nhất giữa kẻ giàu và người nghèo" tôi lại thấy cái phân ranh đó chỉ là một dấu gạch ngang mà thôi , có thể bạn nghèo nhưng việc ý chí, suy nghĩ, hành động của bạn có đủ dũng cảm, liều lĩnh, khôn ngoan, nỗ lực dám dùng chính cái dấu gạch ngang ấy là cầu nối giúp bạn đến được chữ giàu hay không thì là một chuyện khác, và nếu cứ giữ mình trong phạm vị nhỏ hẹp của chữ nghèo, với những suy nghĩ tiêu cực, an phận rằng "ừ, vậy là đủ lắm rồi" thì dấu gạch ngang ấy cũng sẽ biến mất, đồng nghĩa với việc bạn không bao giờ có thể vươn đến sự giàu có được. Đấy chính là những gì tôi rút ra từ cuốn sách này. Tuyệt vời!
5
12219
2013-04-13 16:42:57
--------------------------
58636
9869
Trước hết mình có lời khen cho những cuốn sách như thế này đầy hoa mỹ, đủ thuyết phục người đọc có số liệu thống kê rõ ràng. Nhưng mình có quan điểm như thế này tác giả lập luận không có căn cứ rõ ràng Ví dụ như:
- Người giàu tư duy dài hạn, người nghèo tư duy ngắn hạn.
- Người giàu làm việc vì lợi nhuận, người nghèo làm việc vì lương.
- Người giàu tự hỏi những câu tích cực, người nghèo tự hỏi những câu tiêu cực.
….
Rồi các con số thống kê nữa. Nếu tác giả trình bày lập luận như thế thì cần phải có dẫn chứng, nguồn thông tin thư viện cho người đọc xác nhận đằng này lại lấy dẫn chứng một vài người đại diện cho tập thể.


1
12414
2013-02-06 12:02:16
--------------------------
50815
9869
Thật là thích khi được trải nghiệm trong quyển sách này cũng với tác giả Keith Cameron Smith. Một lối viết văn khá lạ, bởi dường như nó hơi ngắn gọn, xúc tích giống như liệt kê ấy. Nhưng điều đó không làm cho người đọc cảm thấy quá khô khan khi đọc. Bởi thật sự những điều mà người đọc hiểu được nó nhiều hơn những câu, những chữ mà tác giả đã viết. Tác giả đã so sánh người giàu với người nghèo rất hay, và đặc biệt rất đúng. Nhưng rồi thì kết luận rằng giàu và nghèo chỉ là những từ ngữ thông thường để xưng hô mà thôi. Vì mình nghĩ chẳng ai giàu hơn ai cả, những người giàu có biết đâu họ lại phải vượt qua rất nhiều thử thách mới có được những thành quả đó hay không. Còn người nghèo, thực sự số phận không ưu ái quá cho họ, nhưng thực sự nhiều người rất nghèo mà họ "giàu" về tình thương yêu giữa con người với nhau. Mình hiểu được rất nhiều từ cuốn sách này. Cám ơn tác giả Keith Cameron Smith đã cho một định nghĩa mới về sự giàu nghèo
5
64512
2012-12-16 18:56:54
--------------------------
50342
9869
Tôi thực sự thích thú khi đọc cuốn sách này. Những điều mà Keith Cameron Smith mang đến làm tôi hiểu ra phần nào sự thật sự khác biệt giữa kẻ giàu và người nghèo. Và tôi không nghĩ  quan niệm "kẻ giàu càng giàu thêm, người nghèo càng nghèo hơn" là đúng.
Và tôi cũng hiểu được rằng, sự khác nhau giữa giàu và nghèo cơ bản chính là suy nghĩ. Tôi nhận thấy rằng, những người giàu thường có suy nghĩ thoáng hơn và không đặt nặng vấn đề khi họ tham gia một điều gì đó, với họ chỉ giống như một trò chơi, do vậy tâm lý của họ thường  thoải mái hơn. Còn người nghèo, bởi họ đặt nặng vấn đề "gánh nặng cơm áo gạo tiền", vì thế chính gánh nặng này đã kéo họ xuống, không cho họ cảm giác an toàn, họ tham gia điều gì đó với tinh thần rất căng thẳng và lo lắng, có lẽ vì vậy họ không đạt được thành công như họ mong muốn.
Cám ơn Keith Cameron Smith đã giúp tôi hiểu rằng, giàu và nghèo chỉ là 2 khái niệm cơ bản, người giàu thực sự là người luôn mỉm cười khi đối mặt với thử thách khó khăn, còn người nghèo là những người người nghèo từ trong suy nghĩ của họ.
3
78790
2012-12-13 21:44:15
--------------------------
