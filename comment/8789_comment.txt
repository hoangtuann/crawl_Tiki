495477
8789
Bất kỳ một người yêu kiếm hiệp nào cũng biết Kim Dung chỉ có 16 tác phẩm và chẳng có tác phẩm nào mang tên Độc Cô Cửu Kiếm cả, Độc Cô Cửu Kiếm chỉ là một bộ kiếm pháp xuất hiện trong hai tác phẩm Tiếu Ngạo Giang Hồ và Thần Điêu Hiệp Lữ (thuộc Thần Điêu Tam Bộ Khúc) với vai trò là tuyệt thế võ học do Độc Cô Cầu Bại sáng tác trong thời gian đi ở ẩn và được Lệnh Hồ Xung, Dương Quá học lại (riêng Dương Quá còn được sở hữu thanh Trọng Kiếm và người bạn Thần Điêu của Độc Cô Cầu Bại). Theo mình, đây rõ ràng là một tác phẩm Tựa Kim Dung mà thôi.
1
1959012
2016-12-13 06:58:24
--------------------------
491855
8789
Độc cô cửu kiếm là tên nhân vật còn võ công của LHX là học từ bí kíp của ông ta mà ra. Dương quá còn lấy được cả kiếm nữa kìa. LHX sinh sau đẻ muộn. Cái này như kiểu dạng ngoại truyện nên ít tập, với lại độc cô quá bá rồi nên cũng chẳng cần viết truyện là gì nữa :)
2
1908658
2016-11-21 14:15:34
--------------------------
157846
8789
Không hiểu bên nhà xuất bản này làm ăn kiểu gì mà lại xuất bản kiểu này. Bất cứ ai biết về Kim Dung thì đều biết Kim Dung k có tác phẩm nào là Độc cô cửu kiếm, độc cô cửu kiếm thực ra chỉ là tên một bí kíp kiếm thuật của nv Lệnh Hồ Xung trong truyện Tiếu Ngạo Giang Hồ. Còn truyện Độc cô cửu kiếm này chỉ là của một người mạo danh Kim Dung viết ra, nội dung của truyện cũng không hề có một chút liên quan tới Độc cô cửu kiếm cả, đúng theo kiểu treo đầu dê bán thịt chó
1
352671
2015-02-10 15:46:45
--------------------------
