285719
6348
Mình đã mua cuốn này cho bản thân và sau khi đọc mình đã mua tặng cho 1 đứa bạn - đứa mà mình nghĩ có lẽ nó cũng có suy nghĩ giống nhân vật chính trong truyện.
Những suy nghĩ của Claudia không chỉ xuất phát từ quan điểm cá nhân của nàng mà là cả sự tác động từ những hoàn cảnh thực tế của chị em và bạn bè - những người chật vật với con cái, với sự ràng buộc của con cái trong hôn nhân thậm chí với cả việc hiếm muộn con.
Mọi câu hỏi của Claudia đều sẽ được giải đáp một cách thỏa đáng, mà bạn chính là người cảm nhận được sau khi đọc xong cuốn truyện
3
212038
2015-09-01 08:42:23
--------------------------
209688
6348
Nói thật lúc mua quyển sách này đọc là lúc mình đang mang bầu khoảng 3-4 tháng gì đó, xem review xong mình rất thắc mắc diễn biến câu chuyện nên quyết định mua. Có lẽ mình sống theo quan điểm người xưa nên tính cách và cách hành xử của nhân vật chính làm mình hơi khó chịu, ý là hơi buông thả và dễ dãi á. Nhưng nếu nhập tâm vào nhân vật mà suy nghĩ thì mình thấy rất hay ^^ vì bây giờ mình đã sinh con rồi, nên mình biết ý nghĩa quan trong của con cái đối với một người mẹ, mình đã lo sợ và tưởng tượng đủ điều tốt lẫn không tốt đối với baby của mình. Vì thế mình thấy được ở Claudia sự băn khoăn, lưỡng lự chính đáng. "Không lẽ mình cũng giống chị và bạn mình dùng đứa con để thỏa hiệp với chồng?!" ... Nhưng thật sự thì mình vẫn không thích Claudia lắm vì mình suy nghĩ đơn giản chứ không phức tạp như cô nàng >__
3
328614
2015-06-18 00:18:39
--------------------------
149707
6348
Em không muốn có Baby - Đây là cuốn tiểu thuyết thứ 5 tôi đọc của Emily Griffin bằng T.Việt.
Và cuốn này – độ sâu sắc, thâm thúy trong từng câu chữ có thể tương đương với cuốn “Khao Khát của em”. Tác giả rất tài tình thắt nút và tháo cởi từng tình huống tưởng chừng như đi vào ngõ cụt. Đó chính là điều tôi thích nhất ở các tác phẩm của Emily.
Trong tác phẩm này, tác giả bàn về 1 vấn đề rất “thời thượng” của các cặp mới cưới hiện nay, “có con/ khi nào có con/ hay không cần có con?”… Tất cả những ngóc ngách tâm lý của 1 người Phụ nữ đều được phơi bày trên từng trang.
Đây là 1 tác phẩm các bạn nên đọc – đặc biệt là các bạn nữ, không chỉ để giải trí đơn thuần, mà để hiểu hơn về chính mình – có những suy nghĩ của mình, mình không nói ra được thành lời, thì Emily đã nói hộ.
5
389
2015-01-14 15:31:17
--------------------------
130176
6348
Các tác phẩm của Emily Giffin xuất bản ở Việt Nam mình đều đã đọc và cực kỳ yêu thích, nên khi cuốn sách này ra lò là mình không chần chừ tìm đọc ngay. "Em không muốn có baby" cho thấy tài năng của tác giả trong việc khắc họa chân dung, tính cách, suy nghĩ của những con người bình thường một cách chân thực đến bất ngờ. Cuốn sách đề cập đến một vấn đề không hề mới: sự băn khoăn của một người phụ nữ trẻ trước quyết định sinh con. Biết bao trăn trở, lo lắng, và cả đấu tranh nội tâm cứ thế đan cài vào nhau, tạo nên một câu chuyện vừa cuốn hút, vừa lắng đọng. Ngòi bút tài hoa của Emily diễn giải mọi thứ một cách vô cùng tự nhiên, đơn giản và nhẹ nhàng, đôi khi cảm tưởng như tác giả đang chơi đùa với những con chữ của mình nhưng không hề để vuột mất cảm xúc của nhân vật. Tác phẩm này không có nhiều điểm đặc biệt, nó chỉ một bức tranh về cuộc sống, hôn nhân với những gam màu dịu dàng, trầm ấm. Emily thực sự đã trả lời được những câu hỏi tưởng chừng phức tạp về cuộc sống theo một cách đơn giản và duyên dáng.
Chỉ có điều mình không hài lòng là tựa sách. Tựa gốc của nó là "Baby Proof" thì nên tìm một cái tựa dịch đơn giản và ngắn gọn hơn, chứ cái tên "Em không muốn có baby" có vẻ hơi sến súa và không hợp với tác phẩm lắm.
3
109067
2014-10-15 19:38:43
--------------------------
129968
6348
Đây là một cuốn sách thú vị, dành cho những ai cần phút giây giải trí. Tôi không để đặt cuốn sách xuống mà tắt đèn đi ngủ bởi cuốn sách này. Thú vị, hào hứng và đầy lãng mạn. "Em không muốn có baby" có chủ đề rất mới mẻ mà lại rất quen thuộc. Quen thuộc vì đây là chủ đề tình yêu những mới mẻ vì tình yêu gắn với hôn nhân, đặc biệt là con cái. Tác phẩm không hề đơn điệu, không đề nhàm chán vì cách mà Emily so sánh hoàn cảnh của Claudia với 2 chị gái cùng cô bạn thân của mình thật quá khéo léo.
Tôi cho điểm 9/10 cho cuốn sách dễ thương, xúc động và nhẹ nhàng này.
4
308416
2014-10-13 22:34:37
--------------------------
