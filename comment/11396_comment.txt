541321
11396
Mình luôn yêu thích Thạch Lam, nhẹ nhàng và phiêu đãng. Từ lúc đọc Hai đứa trẻ ở trung học, mình đã luôn tìm kiếm các tác phẩm của Thạch Lam. Cuốn sách này cũng vậy. Chắc chắn mọi người sẽ không thất vọng. Giọng văn trong sách kéo ta về với một Hà Nội xưa với những thức quà vừa quen vừa lạ. Vì ở trong Nam nên đối với mình, Hà Nội băm sáu phố phường như một thế giới khác vậy.
5
289256
2017-03-14 11:02:05
--------------------------
482256
11396
Là người con của đất Hà Nội. Tuy nhiên lại lưu lạc đi nơi khác. Tuy nhiên những ký ức về một Hà Nội với những món ăn mộc mạc, giản dị nhưng không kém phần tinh tế, thanh cao luôn sống lại mỗi khi tôi cầm lên và thưởng thức tác phẩm kinh điển Hà Nội băm sáu phố phường của nhà văn Thạch Lam. Cuốn sách mang lại cho những người con xa xứ những cảm xúc chân thực về nỗi nhớ quê hương với những thức quà quen thuộc, bình dị ; thôi thúc người ta trở về cội nguồn và giới thiệu cho con người xứ khác những nét đẹp ẩm thực quê nhà.
4
566706
2016-09-04 22:33:19
--------------------------
463495
11396
"Hà Nội băm sáu phố phường" là tác phẩm đặc sắc của nhà văn Thạch Lam, thể hiện lòng yêu vùng đất đô thành này. Qua từng trang văn, ta dễ dàng nhận ra những cảm nhận tinh tế, sâu sắc của nhà văn dành cho những món ngon Hà Nội, mỗi món ăn là một giọt tinh túy của truyền thống lâu đời. Không đơn thuần là những món ăn dân dã, giản dị, mỗi ngóc ngách của "Hà Nội băm sáu phố phường" chứa đựng từng kỉ niệm, từng con người, từng món ngon mà phải có tâm hồn tinh tế mới cảm nhận hết được.
5
169341
2016-06-29 12:51:20
--------------------------
447802
11396
Hà Nội băm sáu phố phường là những bài đăng báo của Thạch Lam được tập hợp lại, nhưng nó chứa một giá trị sâu sắc về những nét văn hóa xưa cũ còn sót lại của Thủ đô đã trải qua ngàn năm văn hiến.
"Thật đáng tiếc khi chúng ta thấy những tục lệ tốt đẹp ấy mất dần, và những thức quý của đất mình thay dần bằng những thức bóng bẩy hào nháng và thô kệch bắt chước người nước ngoài..."
Khi nhịp sống hối hả của cuộc sống hiện đại đã ngày càng làm phai nhạt những giá trị văn hóa cổ xưa, thì cuốn sách này đáng để đọc, đọc để biết, để gìn giữ những giá trị tốt đẹp này!
5
917875
2016-06-14 21:21:59
--------------------------
391913
11396
Đương đọc cuốn "Hà Nội băm sáu phố phường" của Thạch Lam, thực tình do đây là những bài tản mạn đăng trên báo được gộp lại, nên có nhẽ độ "sâu" khiến mình hơi hụt hẫng. Ơ mà thực ra chắc tại mình bị ấn tượng với kiểu bài viết "nặng" kiến thức như Nguyễn Tuân quá =))).
Muốn mầy bài tùy bút kiểu Phở, Giò Lụa, của Nguyễn được gộp lại ghê, kiểu gì cũng phải hốt =))))
Dưng mà, giọng văn của Thạch Lam rất hay, cái thứ giọng thủ thỉ, êm êm, thích lắm. Nói chứ mình học hỏi cách viết, chủ yếu là học Thạch Lam chứ đâu. Người mình thích nhất, là Nam Cao; người mình bấn nhất, là Nguyễn Tuân, nhưng người ảnh hưởng tới mình nhiều nhất, là Thạch Lam cơ Biểu tượng cảm xúc colonthree
Mà hơi thắc mắc, sao tái bản lại không làm một bản chú thích ở đằng sau cho cái bài nói về việc Hà Nội có nhiều biển hiệu tiếng Pháp? Thú thực là mình không biết một chữ gì tiếng Pháp, nên không hiểu lắm về cái "mỉa mai" của Thạch Lam ở trong bài này.
Đoạn Thạch Lam bảo ổng vẫn trung thành với phở cũ chứ không muốn đổi mới, trời nhớ tới cái đoạn Tô Hoài kể là Nguyễn Tuân chỉ ăn độc một loại phở, đến lúc người ta ăn phở để no thì Nguyễn cũng dẹp ăn phở luôn trong "Cát bụi chân ai" ghê =)).
4
274508
2016-03-05 22:30:17
--------------------------
366747
11396
Thạch Lam là một nhà văn hay. Tác phẩm Hà Nội Băm Sáu Phố Phường xứng đáng nằm trong bộ sách Việt Nam danh tác.
Tác phẩm gồm những bài viết ngắn của Thạch Lam về Hà Nội được tổng hợp lại làm nên một quyển sách hay. 
Đọc văn của ông, nghe ông nói về những thức quà của Hà Nội, những món ăn ngon đúng nghĩa và cái hay cái dỡ của người Việt mà nhớ lại trong tâm trí cái thời bé thơ được ăn những thứ ngon và rẻ mà giờ đây không đâu còn bán nữa.
5
471257
2016-01-10 14:55:05
--------------------------
355293
11396
Hà nội cổ có nhiều góc phố nổi tiếng không làm cho nhiều người khỏi quên được giá trị đích thực toàn cảnh của xã hội buôn bán tấp nập , có nhiều giai đoạn phát triển của phồn hoa và văn hóa thẩm mỹ đa dạng , nhiều đồ đẹp , tiếng người nói chuyện giao hàng mang phong cách tĩnh lặng , sôi nổi , hoài niệm về ký ức không gian nhỏ bé gói gọn trong bức tranh , không làm ta buông xuôi mọi thứ , cảm nhận niềm vui khác biệt trong nhân gian .
4
402468
2015-12-19 18:22:45
--------------------------
333722
11396
"Thạch Lam là một cây bút thiên về tình cảm, hay ghi lại cảm xúc của mình trước số phận hẩm hiu của những người nghèo, nhất là những người phụ nữ trong xã hội cũ, sống vất vả, thầm lặng, chịu đựng, giàu lòng hi sinh ("Cô hàng xén"). Có truyện miêu tả với lòng cảm thông sâu sắc một gia đình đông con, sống cơ cực trong xóm chợ ("Nhà mẹ Lê"). Có truyện phân tích tỉ mỉ tâm lí phức tạp của con người ("Sợi tóc"). "Ngày mới" đi sâu vào nội tâm của một cặp vợ chồng trí thức nghèo. Chưa có truyện nào có ý nghĩa xã hội rõ nét như các tác phẩm của các nhà văn hiện thực phê phán. "Theo giòng" là một thiên tiểu luận viết kiểu tuỳ bút, ghi lại suy nghĩ của ông về nghệ thuật tiểu thuyết, có những ý kiến hay, nhưng chưa đi sâu vào khía cạnh nào.Và với cuốn "Hà Nội ba sáu phố phường" là phong vị đậm đà của quê hương xứ sở, lại rất gợi cảm. Văn Thạch Lam nhẹ nhàng, giàu chất thơ, sâu sắc, thâm trầm, thường để lại một ấn tượng ngậm ngùi, thương xót."
4
449829
2015-11-08 11:07:56
--------------------------
333620
11396
Không biết mình yêu thích Thạch Lam tự bao giờ, chỉ biết rằng mình từng lên google tìm hiểu về ông và tất cả các tác phẩm ông viết và tìm mua cho bằng được. Cái gì thuộc về thạch lam cũng nhẹ nhàng sâu lắng, cũng thấm đượm tình người, tình yêu quê hương đất nước. Đọc truyện thạch lam, cái hay của truyện đôi khi phải suy nghĩ, phải tìm hiểu nhiều lắm mới thấm được. Còn trong cuốn hà nội băm sáu phố phương này, mình thấy được ở đó những gì cổ xưa, chân thật nhất của hà nội, của nếp sống ông cha. Món ăn ngon, phố phường xưa, nếp sống, tất cả mọi thứ hiện lên sống động nhẹ nhàng và đong đầy cảm xúc  Tuy rằng mình không sống ở hà nội, nhưng mình luôn thích đọc về hà nội ( hà nội xưa ), dưới ngòi bút của thạch lam, mình càng thấy nó trở nên gần gũi, thân thương hơn hao giờ hết.
P/s: đọc nhận xét của mọi người mà mình thấy các bạn thật giống các nhà phê bình văn học, viết hay thế không biết, nên bản thân cũng tự thấy phải làm một đoạn văn, tuy rằng nó hơi dài dòng :))
5
642004
2015-11-08 09:34:03
--------------------------
333523
11396
Trong bối cảnh xã hội Tây Tàu nhố nhăng làm lạc mất những giá trị xưa cũ , cũng như những nhà văn cùng thời tìm về với những vẻ đẹp, với những giá trị chân- thiện- mĩ , quá Thạch Làm, ta thấy được một Hà Nội rất Hà Nội , từ những món ăn xưa cũ đậm chất dân tộc đến phong tục tập quán, thói quen thể hiện được cái đẹp của người Hà Nội trong Băm sáu phố phường. Đọc để cảm nhận, để thấm thía một Hà Nội xưa và để thêm yêu, thêm mê Hà Nội, quê hương đất nước mình 
5
915482
2015-11-07 23:23:42
--------------------------
333156
11396
Hà Nội của ai kia. Là Hồ Gươm. Là Văn Miếu. Là Hoàng Thành Thăng Long ngàn năm lịch sử. Là nền Văn Hiến lâu đời, niềm tự hào của biết bao thế hệ con Rồng cháu Lạc. 
Hà Nội của em. Không phải là Hà Nội xô bồ ồn ã. Không còn là ánh đèn hào nhoáng xa hoa. Mà là một góc nhỏ thật bình yên. Là nơi chưa xa đã nhớ. Là nơi chẳng nỡ rời lâu. Là nơi dung dưỡng tâm hồn sau ngày dài mệt mỏi.
Và em. Một cô gái Hà Nội. Thì rất thích đọc những câu chuyện về thú ăn chơi của người Hà Nội. 
Thạch Lam đã cực kỳ thành công khi làm cho em "say đắm " Hà Nội đến mức không dứt ra được.
Giọng văn cứ bình thản. Cứ thong dong nhưng nào có biết đâu chính thế đã làm cho độc giả say lòng.
Em đã từng đọc ở đâu đó. "Rượu hay, là rượu làm người ta say lúc nào chẳng biết. Say mà không nghĩ mình đã say."
Văn Thạch Lam cũng thế. Làm cho người ta sau khi đọc hết còn bồi hồi xao xuyến. Ngẩn ngơ nhìn lại. "Ồ, ra là mình đã say văn"
Yêu thích Thạch Lam từ khi đọc tập truyện ngắn "Gió lạnh đầu mùa."
"Hà Nội Ba sáu phố phường " có thể được gọi là tác phẩm mang cho em động lực đi quanh Hà Nội để thưởng thức từng miếng ngon của người sành ăn.
4
707418
2015-11-07 14:57:40
--------------------------
321092
11396
Yêu Hà Nội nên yêu luôn cả sách :) Tủ sách chật cứng những thứ được cộp mác "Hà Nội" rồi, từ sách cũ đến sách mới. 
Cái mình thích ở bộ Danh tác của Nhã Nam là chất lượng giấy, vẽ bìa, giấy in bìa. Phải dùng một từ là "chất". Bộ Danh tác này đang làm "điên đảo" túi tiền của mình. Chắc chắn là cứ phải mua dần từng quyển khi tiền lương về rồi. Nhiều bạn có nói là giấy mỏng quá, nhưng mình thích sách khổ như này và nhẹ như này, cầm đỡ mệt, đọc đỡ khổ.

5
4397
2015-10-13 09:43:26
--------------------------
312760
11396
Hà Nội ngày nay khác nhiều so với "Hà Nội băn sáu phố phường" của Thạch Lam. Cái cảm giác khi ta lật từng trang sách như là lật về quá khứ vậy. Kí của Thạch Lam thì hay rồi. Nhẹ nhàng nhẹ nhàng như "hương hoàng lan" bay vào tâm hồn người đọc vậy. Nhất là những lần ông miêu tả mấy món đồ ăn vặt của Hà Nội thì chao ôi, nó đã bay được vào "tâm hồn ăn uống" của mình rồi đó.
“Có ai buổi trưa vắng hay buổi đêm khuya, đi qua các nhà cô đào, và các chị em thanh lâu, thấy họ ăn cái quà ấy một cách chăm chú và tha thiết đến đâu không? Nước ốc chua làm nhăn các nét mặt tàn phấn và mệt lả, miếng ớt cay làm xoa xuýt những cặp môi héo hắt, và khiến đôi khi rỏ những giọt lệ thật thà hơn cả những giọt lệ tình.” 
Mình được bạn tặng sách vào sinh nhật năm ngoái. Sách của Tiki nhỏ nhỏ xinh xinh dễ mang theo bên mình. Đây là món quà mình thích nhất từ trước tới giờ
5
833144
2015-09-21 23:45:16
--------------------------
309935
11396
Từng đôi lần đến với Hà Nội nhưng đã để lại trong tôi những ấn tượng sâu sắc và khi thấy cuốn sách này tôi đã mua ngay. Do chưa tìm hiểu kỹ cứ ngỡ sách viết cảm nhận về từng con phố Hà Nội nên khi lật ra thì hơi buồn xíu. Nhưng vào đọc thì thấy sách đã không làm mình thất vọng. Lời văn của Thạch Lam thật nhẹ nhàng và đầy chất thơ, giọng văn như thủ thỉ rót vào tai người nghe, như khúc tự sự tâm tình về một Hà Nội thật đẹp và thơ mộng, về những ngón quà Hà Nội thật thân thương. Nhờ vậy mình lại thấy khát khao được khám phá Hà Nội hơn nữa. Dù là những nét còn sót lại của Hà Nội xưa
4
252046
2015-09-19 11:03:06
--------------------------
307099
11396
Tôi được được đọc ké quyển sách này từ rất lâu rồi, giờ chỉ muốn mua để sưu tập cho đủ bộ sách của Thạch Lam. Sách được bọc sẵn (bằng gói bookcare) trông khá đẹp. Bìa sách được trang trí bởi một bức vẽ cổ kính. Chất giấy bình thường không quá trắng, có thể đỡ làm hại mắt khi đọc. Nhưng giấy không được mịn và cứng lắm. Các trang giấy hơi mỏng. Nhìn chung quyển sách này chất lượng giấy tạm được. Khổ sách nhỏ gọn có thể cầm theo khi ra ngoài đọc rất tiện lợi. 

5
284203
2015-09-17 22:46:34
--------------------------
301838
11396
Cuốn sách này viết về 36 phố phường Hà Nội nhưng không miêu tả về sự hình thành và đặc điểm của 36 phố phường mà viết về ẩm thực đặc trưng của Hà Nội trong 36 phố phường cổ kính. Cùng với cuốn món ngon Hà Nội của Vũ Bằng thì đây là một trong những cuốn sách viết về ẩm thực một cách khác lạ, những món ăn ,hàng quán được thể hiện sống động qua ngòi bút của tác giả. Những câu chuyện vốn dĩ chỉ là những mẩu chuyện nhỏ  đã đăng báo nhưng khi đọc lại cho ta thêm nhung nhớ đến những nét đẹp giản dị,tinh túy của những món quà cũ mà chẳng cũ của một thời đại chưa hề xa.
5
28807
2015-09-14 21:08:52
--------------------------
286932
11396
Ngay từ bìa sách đã khiến tôi có cảm tình, với hình ảnh phố cổ Hà Nội trên nền màu giấy dó cổ kính. Đọc sách lại càng thích hơn. Thạch Lam viết về Hà Nội bằng tình yêu tha thiết, bằng những từ ngữ chân thực nhưng không kém phần trau chuốt, bằng vốn kiến thức được tích lũy qua bao năm tháng sống giữa phố phường Hà Nội. Tôi đặc biệt thích phần ông viết về những thức quà Hà Nội, đọc mà phát thèm chết đi được, ngòi bút miêu tả của tác giả làm người ta có cảm giác như món ăn đang hiển hiện trước mắt mình vậy. Có điều đôi chỗ tác giả viết theo lối hành văn ngày xưa nên cảm giác câu văn hơi trúc trắc một chút, dù vẫn có thể hiểu được.
5
57459
2015-09-02 08:55:58
--------------------------
281212
11396
Khi các bạn tôi đi du học, tôi đã phải suy nghĩ rất lâu để tìm ra món quà gì nhẹ nhàng, ý nghĩa tặng cho những con người xa xứ.
Dạo qua dạo lại một hồi, tôi nghĩ rằng những cuốn sách sẽ thật đáng yêu và dễ đem đi nữa.
Bộ sách Việt Nam danh tác là lựa chọn hàng đầu, và nhất là Hà Nội băm sáu phố phường. Những áng văn của Thạch Lam cứ ngọt ngào và êm đềm như thơ vậy. Cuốn sách này sẽ khiến người được tặng hạnh phúc, hẳn rồi phải không?
5
20223
2015-08-28 11:13:21
--------------------------
278337
11396
Qua bao nhiêu năm tháng, Hà Nội vẫn giữ được nét cổ kính của mình. Hà Nội với ba sáu phố phường, mỗi góc đều có một nét đặc biệt được thể hiện qua từng trang sách. Với những ai chưa có dịp thăm quan phố cổ thì từng dòng trong cuốn sách sẽ mở ra cho bạn một bức tranh thú vị. Hương vị của các món đặc sản như được ngấm vào từng câu chữ truyền tới độc giả. Ít cuốn sách nào viết về Thủ đô lại nhẹ nhàng và đầy cuốn hút như này. Cho dù bạn đã hay chưa có dịp dạo quanh phố cổ thì cuốn sách này cũng vẫn rất đáng để đọc.
5
485324
2015-08-25 20:48:02
--------------------------
276017
11396
Thật ra, với những người con không phải của Hà Nội, việc ngồi trước trang sách chiêm ngưỡng vẻ đẹp của ba mươi sáu con phố của vùng đất ngàn năm qua trải nghiệm của một người nồng nàn yêu Hà Nội là rất tuyệt. Không cần đi lòng vòng đâu xa, dõi mắt theo đôi dòng chữ của Thạch Lam, lắng nghe từng lời trái tim ông, tôi biết được vẻ đẹp của từng con phố cổ  quanh co, vị ngon của từng đặc sản và hương thơm của hoa sữa... Lời văn nhẹ nhàng man mác cuốn người đọc vào từng trang viết, đây là một cuốn sách rất tuyệt vời cho vùng đất thủ đô này. Sách in thơm, đẹp lắm
5
366416
2015-08-23 15:41:38
--------------------------
272094
11396
Tôi chưa đặt chân đến Hà Nội, nhưng tôi luôn muốn có thể đến đó và cảm nhận những gì mà Thạch Lam đã khắc họa, có điều đã bấy lâu rồi, chắc sẽ không còn nguyên vẹn như xưa! Nhưng đó đúng là một thế giới trong mơ, khi tôi có thể gần như cảm giác rằng có vị ngon của những món đặc sản Hà Nội, có âm thanh văng vẳng của những con phố đông vui và màu sắc! Bạn không cần là người quá đa cảm, nhưng bạn sẽ cảm thụ cuốn sách này như một chuyến đi gián tiếp qua mắt người, thèm thuồng và phấn khích, khao khát được được đến gần thế giới đó thêm nữa!
5
9289
2015-08-19 19:21:36
--------------------------
267963
11396
Thạch Lam là một tác giả mà mình khá thích, ngoài gia tập "Hà Nội Băm Sáu Phố Phường" cũng là một tuyển tập nổi tiếng đánh dấu tên tuổi Thạch Lam. Vì vậy mình đã tìm hiểu tuyển tập này và không thất vọng chút nào. Giọng văn Thạch Lam vẫn thế, nhẹ nhàng tâm tình, ông viết về những cái chướng tai gai mắt (biển hàng, ...) mà không giáo điều lên án gay gắt, như một lời thủ thỉ nhắc nhở khiến ta phải nhìn nhận một cách nghiêm túc. Thế và trong tuyển tập này chủ yếu còn ca ngợi Hà Nội với những món ăn giản dị thanh lành, ông yêu cái giản dị đầy đặn ấy và chẳng nồng nàn với cái pha tạp của cả món ăn. Đọc đến những món ăn khiến tôi chẳng dừng mình lại được và liền đọc mãi để theo dõi bước chân tìm đến với cái ngon của Thạch Lam. Hà Nội thật đẹp biết bao nhiêu dưới con mắt tinh tế Thạch Lam,...
4
299614
2015-08-15 22:55:39
--------------------------
258344
11396
Ấn tượng với Hà nội băm sáu phố phường từ những ngày tôi ngồi trên ghế nhà trường, với " Cốm, một thứ quà của lúa non". Nay, được cầm trên tay tác phẩm và đọc hết mới hiểu được cái dung dị và gần gũi, cái tròn đầy trong từng câu chữ của tác giả. Đọc tác phẩm mà như được hít hà từng món ngon của Hà Nội, như được đứng ở một góc nào đó,ngắm nhìn 36 phố phường nhộn nhịp, tươi xinh với từng hàng quán, từng món ăn đủ màu sắc. Chao ôi, còn cảm giác thèm thuồng nào bằng đọc sách mà tưởng tượng từng thứ quà ngon đang ở trước mắt, rồi đưa tay ra nhón một tiếng, rồi gật gù....tác giả khéo tả hay không còn gì bằng.Vâng, đọc Hà Nội 36 phố phường, tôi chỉ ước chi mình được ra thăm Hà Nội một lần, mà phải ra thăm vào mùa thu, mùa của cốm làng Vòng mới thỏa nỗi ước ao tuổi trẻ!
4
547523
2015-08-08 14:21:19
--------------------------
247297
11396
Hỡi ai mà đoi đói bụng, lấy quyển sách này lên mà đọc, thế nào cũng không chịu nỗi mà tìm gì bỏ bụng cho mà xem.
Những món ăn bình dị đến thế, mà qua trang viết của tác giả, cái bình dị đó thanh sạch một cách lạ thay.
Như món ăn được gói ủ trong lá sen, đọc đến đâu hé mở đến đó, để rồi hương vị món ăn như bày trước mặt. Không cần tác giả khen thêm vào chữ “Ngon” để bình phẩm, người đọc tự biết nó thơm ngon đến độ nào rồi.
Sách giới thiệu ẩm thực, hay đến vậy là cùng.

4
385588
2015-07-30 10:07:43
--------------------------
244505
11396
Nếu chứ từng tới Hà Nội bạn có thể cảm nhận vẻ đẹp của hà nội qua ngòi bút của nhà văn Thạch Lam. Những đặc trưng về ẩm thực của hà nội đều được nhà văn mô tả rất rõ nét và cụ thể. Ta có thể cảm nhận được mùi vị và hương thơm của chúng qua từng câu chữ của nhà văn. Thạch Lam tiếc cho cái vẻ đẹp xưa cũ đã bị thay thế theo xu hướng của thời đại. Đặc biệt là nét tinh tế, giản dị, mộc mạc trên từng món ăn hà nội.
5
484649
2015-07-28 11:24:47
--------------------------
230913
11396
Hà Nội đẹp hơn qua lối viết Thạch Lam. Những đặc trưng của ẩm thực Hà Thành một thời, cái nơi phồn hoa phố thị miền Bắc. Hà Nội chính là nét đặc trưng của cả vùng rồi trong đó nó cũng đặc trung của sự đa dạng trong ẩm thực và người thưởng thức. 
Trong tác phẩm, Thạch Lam chỉ cảm nhận những gì ông cho là hay, là ngon, là xấu nhưng qua đó ta lại thấy một nét mộc hơn và chân thật hơn những thức vị Hà Nội. Ông tiếc những nét xưa món ngon đã đổi để phù hợp theo "xu hướng" thời đại rồi ông lại yêu những cái chất vị cái mộc trong từng món bánh, món phở... và Cốm.
4
472508
2015-07-17 20:23:36
--------------------------
227647
11396
Khi đọc cuốn sách này, tôi cứ ngỡ mình đang ở phố cổ, hít hà những mùi ngói cũ kĩ, mùi quà bánh thơm ngon mới tinh, mùi cốm non thơm nồng trong chiếc lá sen. Là Hà Nội đấy, là Hà Nội rất Thạch Lam. Trong con mắt ông, Hà Nội đẹp và dịu như một cô gái mới lớn, nhưng cũng rất nồng, rất tình như kẻ đã sinh ra trong lòng Hà Nội từ bao năm qua. 
Chỉ có qua những lời văn, trang giấy, Thạch Lam mới giúp những con người hiện tại giữ được một Hà Nội trong tim theo cách riêng của mình. 
5
54418
2015-07-14 13:56:07
--------------------------
225045
11396
Mình còn nhớ, hồi thiếu niên, mình đã ấn tượng bao nhiêu với trích đoạn "Một thứ quà của lúa non - cốm" của Thạch Lam được học trong chương trình sách giáo khoa. Cũng chính vì thế mà mình quyết định đặt mua cuốn sách này, để thưởng thức lại những hương vị của một Hà Nội xưa mà mình chưa từng được tận hưởng.

Mình yêu một Hà Nội thật xưa như thế, một Hà Nội chỉ còn hiện lên qua những trang sách của các tác giả xưa. Thạch Lam có một lối văn thật mềm mại và dễ đi vào lòng người. Văn Thạch Lam trong cuốn "Hà Nội băm sáu phố phường" này giản dị mà êm như suối chảy, có cảm tưởng như ông không viết mà tự những câu, những chữ ấy cứ tuôn ra từ trong trái tim ông. Hẳn Thạch Lam phải yêu Hà Nội lắm. Vì những hương cốm làng Vòng, những vị chả cá Lã Vọng, vị bánh cuốn Thanh Trì, vị của bún chả, bún ốc, của một gánh phở ven đường hay gánh xôi tờ mờ sáng, cả vị của chén nước chè thật đậm... tất cả đều hiện lên thật tinh tế dưới ngòi bút nhà văn. Đọc một cuốn sách mà như thể đi trải nghiệm một vòng, trên chiếc xích lô ngày xưa, đi khắp các phố phường của Hà Nội cũ và nếm thử đủ các món xưa. Rất tuyệt! Cuốn sách quả xứng đáng là một "Việt Nam danh tác".
4
24661
2015-07-09 19:07:12
--------------------------
223173
11396
Hà Nội băm sáu phố phường là một trong những cuốn sách nổi tiếng bậc nhất viết về Hà Nội. Chúng ta có thể hiểu đây là “Hà Nội xưa”, chứ không phải Hà Nội xô bồ với đủ thứ “hổ lốn” hiện tại!

Tác giả đã đưa chúng ta đi qua từng con đường, từng góc phố, từng mái nhà “thâm nâu”, đầy chất nhạc họa và thi ca. Ở đó, bóng dáng người Hà Nội hiện lên thật đẹp: thanh lịch, nhẹ nhàng, chân thành, lễ phép. Những thức quà của Hà Nội cũng thật đặc biệt, đơn sơ, gói tròn kỉ niệm đối với những ai đi xa Hà Nội.

Càng đọc cuốn sách, chúng ta càng thấy buồn bâng khuâng… Đã từng có một Hà Nội đẹp đến thế!
5
598102
2015-07-06 18:26:52
--------------------------
219699
11396
Cuốn sách này thật sự làm tôi buồn và hụt hẫng khi thấy sự khác xa của Hà Nội ngày nay với Hà Nội của thế kỉ trước. Hà Nội xưa-một Hà Nội cổ kính trầm mặc của những con người hòa hòa thanh lịch, bằng sự quan sát tinh tế của một con người đa cảm cùng giọng văn đầy chất thơ Thạch Lam đã tìm ra nét đẹp của những điều nhỏ nhặt, giản dị đã làm nên Hà Nội đáng để yêu đáng để nhớ như những bảng hiệu, một cốc nước chè tươi, một cô bán hàng lễ phép xinh xắn và đặc biệt là những những món quà vặt rẻ tiền nhưng chất chứa cả tâm hồn và sự khéo léo của củ người làm ra nó được Thạch Lam coi như một công trình nghệ thuật.
4
507362
2015-07-01 21:00:06
--------------------------
210986
11396
Hà Nội Băm Sáu Phố Phường trong bộ Việt Nam Danh Tác...
Trong từng trang giấy có thể thấy rõ những ngõ ngách, góc khuất của Hà Nội, đời thường, sinh hoạt của Hà Nội. Nhà văn Thạch Lam đã diễn tả một cách chân thực, chi tiết, sống động với văn phong thật gần gũi... Tác phẩm chân thực đến nỗi làm cho những người con nơi đây yêu thêm đến độ "không thêm" được nữa quê hương thủ đô của mình, hay làm cho những người chưa từng đặt chân đến Hà Nội cũng có thể cảm nhận được cái đẹp nhất của người, của cảnh, của Hà Nội.
5
188750
2015-06-19 23:07:45
--------------------------
180124
11396
Cuốn sách này mang mùi cốm xanh,mang màu rực rỡ của những thức quà,những món ăn bình dị nhưng hết sức thanh nhã và cao đẹp.Và cả những biển hiệu nữa...Phố phương Hà Thành trong mắt Thạch Lam hiện lên như một bức trang mang dáng vẻ cao đẹp nhưng không hề hào nhoáng và hoành tráng như những thứ lai căng bắt chước,học đòi.Chỉ đôi mắt thanh cao mới có thể nhìn thấu sự thanh cao vào tao nhã ẩn dưới vỏ ngoài bình thường.Văn Thạch Lam luôn luôn là như vậy,nhẹ nhàng,ấm áp nhưng mang đầy tính nhân văn cao cả.
5
440861
2015-04-08 18:32:58
--------------------------
173755
11396
Vẻ đẹp xuyên thời gian của Hà Nội nên thơ nên văn không chỉ ở Việt Nam thôi mà còn được người ngoại quốc yêu mến. "Hà Nội băm sáu phố phường" của Thạch Lam đã khắc họa rất tốt vẻ đẹp này. Thạch Lam đã miêu tả rất chân thực về Hà Nội, về món cốm Vòng chỉ duy nhất tại Hà Nội, v...v.... Quà Hà nội mang đến vị ngon thuần thiết. Đây có thể xem như là một tập bút ký dành riêng cho vẻ đẹp Hà Nội. Chất lượng bìa và giấy in đều rất tốt, luôn ủng hộ Nhã Nam xuất bản bộ sách Việt Nam Danh Tác để gìn giữ và phát huy nền văn học Việt Nam.
5
542401
2015-03-26 19:17:57
--------------------------
169403
11396
Lời trước tiên cho những ai sắp đọc quyển sách này, bạn không thể dừng hình dung hóa những thức quà, những nét đẹp giản dị thật Hà Nội, mà chỉ Hà Nội mới có thôi ... Nhìn quà Hà Nội, thấy đâu dáng dấp con người Hà Nội gói tấm lòng trong ấy. Thạch Lam miêu tả thật nhẹ nhàng mà sao ra được hạt cốm tinh túy, món bún ốc mộc mạc hay hàng nước cô Dần bình thường nhưng khách vẫn thích dừng chân ... Có lẽ, phải yêu Hà Nội lắm, yêu con người nơi ấy lắm, cây bút kia mới phát huy được mọi ngóc ngách tâm hồn mình. Mà này bạn ơi, đọc Hà Nội băm sáu phố phường xong, đừng hỏi tại sao yêu Hà Nội xưa kia hơn nhé, và ... đang đói bụng thì hãy coi chừng đó ^^
5
542114
2015-03-18 09:18:26
--------------------------
168359
11396
Cái lý do đơn giản mà mình thích "Hà Nội băm sáu phố phường" bởi vì mình yêu Hà Nội, chỉ có thế thôi... Yêu những cái cổ xưa còn ghi lại biết bao nhiêu dấu ấn của cha ông, yêu những cái yên bình của phường phố ẩn hiện dáng dấp người Tràng An, yêu vẻ đẹp hào hùng của Thủ Đô lật đều theo từng trang Sử để tạo nên đất nước ngày nay Hà Nội thiêng liêng lắm, dù cho ai đó có không sinh ra và lớn lên ở Hà Nội cũng tự nhiên thấy sự thân thương và gần gũi của đất Thủ Đô. Bởi vì Hà Nội chính là hình ảnh của quá khứ, của tâm hồn, của Trái Tim con người Việt Nam.
5
466542
2015-03-16 15:06:39
--------------------------
152524
11396
Hà Nội băm sáu phố phường dưới con mắt của Thạch Lam như một bức tranh sơn mài tượng thanh mộc mạc, giản dị mà sâu sắc. Đọc đến đâu thấm ngay từng chi tiết nết ăn, nết ở gần gũi, thân thương đối với những con người sinh ra, lớn lên và gắn bó với mảnh đất Tràng An. Với những thực khách chưa từng đặt chân lên góc phố, con đường nơi đây, chắc sẽ khó cưỡng lại nét khơi gợi, phảng phất về ẩm thực và con người Hà Thành mà tìm đến, ít nhất một lần trong đời.
5
473130
2015-01-23 11:48:19
--------------------------
141239
11396
Hà Nội băm sáu phố phường của Thạch Lam được đánh giá là một cuốn bút ký dành riêng cho vẻ đẹp Hà Nội.Là những trang sách nói về những món ăn ẩm thực văn hóa của mảnh đất thiêng liêng lịch sử nghìn năm văn hiến, một mảnh đất chẳng phải ồn ào náo nhiệt mà tĩnh lặng và yên bình đến lạ.Hà Nội trong " Hà Nội băm sáu phố phường " cổ kính, thanh lịch, có chút gì đó quyến rũ và thân thương.Tác phẩm hính là sức quyến rũ mà ông muốn mang lại cho đọc giả, những người đã đang và sẽ yêu Thủ đô yêu dấu này.Cuốn sách là con đường ngắn nhất đưa nét đẹp nơi đây đến con tim của bạn :)
5
369772
2014-12-14 17:33:27
--------------------------
116910
11396
Trong số các nhà văn lãng mạn thời trước Cách mạng tháng 8, tôi thích nhất Thạch Lam. Tôi thích ông bởi giọng văn nhẹ nhàng mà hấp dẫn, cuốn hút người khác bởi một thứ gì đó buồn man mác lạ lùng. Tác phẩm "Hà Nội băm sáu phố phường này" cho tôi thấy được phần nào điều đó. Sự quyến rũ lạ lùng của giọng văn, cùng những chuyện không hề có cốt truyện trong nó nữa, gây cho tôi một sự bâng khuâng kì lạ. Vẫn là Hà Nội, vẫn là thủ đô yêu thương của tôi, nhưng sao Thạch Lam kể về nó với một nỗi buồn man mác hư vô đến lạ lùng. Để rồi, chợt thấy yêu sâu nặng thêm miền đất quê hương mình....
5
129990
2014-07-13 16:49:16
--------------------------
