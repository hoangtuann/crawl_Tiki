488543
6574
Đây là tác phẩm trinh thám mà tôi thích nhất ngay cả khi nó không phải là một tác phẩm trinh thám hay nhất. Ban đầu tôi không thích những tác phẩm về những tội phạm có tâm lý bất bình thường nhưng khi mượn đọc Đứa trẻ thứ 44 từ một người bạn tôi đã theo dõi liên tục chỉ trong vòng một đêm và quyết định đặt hàng trên Tiki ngay sau đó. Sau khi đọc Đứa trẻ thứ 44, cái cảm xúc hồi hộp, tiếc nuối, buồn bã đã "đi theo" tôi suốt một tuần hơn. Cái kết có thể nói là không thực sự có hậu, thậm chí là rất éo le nhưng nó lại chính là điểm cộng khi thành công tạo cho người đọc nhiều cảm xúc và đầy ám ảnh. Với cách gọi nhân vật chính là "gã" đã cho thấy được một điều rằng ngay cả một vị anh hùng cũng không thực sự là hoàng mĩ và tốt đẹp. Đứa trẻ thứ 44 là một tác phẩm rất hay và nên đọc, đảm bảo rằng sẽ không phí số tiền và thời gian bạn bỏ ra. Tôi cũng rất mong chờ phần 2 của truyện sớm được dịch và xuất bản.
5
680298
2016-11-03 16:04:50
--------------------------
473349
6574
Đứa trẻ thứ 44 là 1 cuốn sách rất thú vị và hấp dẫn voi tôi .tôi đã thích cuốn sách này tu rất lâu và bây giờ thông qua tiki tôi đã co đuoc nhung trang truyện thú vị và đầy xúc động .cảm ơn tiki rất nhiều !                        
Đọc truyện có nhiều đoạn ta nhu nghẹt thở bởi những tình tiết lý thú trọng truyện  và nhiều đoạn làm tôi thốt tim và quá cảm động .nhà văn đã khắc hoa cho ta thấy bối cảnh của nga những nam 1950 .một khung cảnh mờ tối và cuộc sống khó khăn ,đói nghèo của người dân ..Cuốn sách này có lẽ là cuốn để lại cho tôi nhiều cảm xúc ,long thương cảm nhất trong các cuốn trình thám tiểu thuyết mà tôi từng đọc.
5
1487962
2016-07-10 14:40:02
--------------------------
463745
6574
"Đứa trẻ thứ 44" truyện kể về những vụ sát hại những đứa trẻ trên khắp đất nước Xô Viết thời Stalin. Bối cảnh chính trị đặc biệt đó đã làm nên sức hấp dẫn của truyện. Hình tượng nhân vật chính vượt qua bao sự trở ngại ngăn cấm để tìm ra chân tướng vụ án chính là thể hiện cho niềm tin vào công lý sẽ chiến thắng bạo quyền. Sự thông minh của tác giả tài năng người Mỹ - Tom Rob Smith thể hiện ở bối cảnh và thời gian hết sức thuyết phục. Là những năm 1950 khi chính trị và xã hội Nga bất ổn. Đứa trẻ thứ 44 có giọng văn cuốn hút và người đọc sẽ lôi kéo bởi từng chi tiết. Ngoài ra còn có yếu tố kịch dị làm tác phẩm trở nên hoàn hảo hơn. 
Tội ác và truy tìm là thế, nhưng hầu như mỗi chương đều có sự xem lẫn cùng tình cảm gia đình!!! Và tìm kiếm nguồn gốc bản thân. Đó là một trong những điểm mạnh của tác phẩm.
5
30233
2016-06-29 16:13:43
--------------------------
414557
6574
Đọc xong cuốn sách này tôi vẫn không thể nào giải đáp được câu hỏi là: Tại sao một nhà văn người Anh, sinh ra và lớn lên ở Luân Đôn, lại còn khá trẻ ( sinh năm 1979) lại có thể viết về xã hội Liên Xô cũ vào những năm 1950 chân thực và sống động đến vậy?
Chắc chắn một điều rằng cũng không thể nào quên dịch giả Vũ Hồng Long. Dịch giả dịch câu chuyện này quá hay, đến từng câu chữ cũng không thể bỏ qua, không thể đọc lướt được. 
Đứa trẻ thứ 44 ly kỳ đến nghẹt thở, đầy cảm xúc, làm cho khi đọc ta luôn có cảm giác chờ đợi. Chờ đợi những câu chữ tiếp theo, chờ đợi chuyện gì sẽ xảy ra. Và càng về cuối lại thấy lo sợ. Sợ hết không còn gì để đọc.
Hãy đọc đi bạn, đừng chần chừ. Bởi bạn cũng sẽ có những cảm nhận như tôi! 
( Vài dòng cảm nhận sau khi đọc trang cuối  Đứa Trẻ Thứ 44)
5
724772
2016-04-11 23:04:18
--------------------------
413324
6574
Đây là quyển trinh thám mình thích nhất từ trước tới nay, truyện được tác giả lồng ghép vào các sự kiện lịch sự và bối cảnh thật vô cùng khéo léo, làm mình nhầm lẫn khi cố phân biệt các sự kiện thật và giả trong câu chuyện. Tác phẩm có rất nhiều chi tiết lấy cảm hứng từ sự kiện ngoài đời thật, nhưng bản thân câu chuyện này hoàn toàn là tiểu thuyết nhé. Các tình tiết và mạch chuyện mang tính liên kết và được sắp xếp rất logic. Diễn biến vô cùng bất ngờ, không theo một mô tuýp nào cả. Cuốn sách làm mình cực kỳ hài lòng, nhưng đáng tiếc mình đọc gần hết thì lại làm mất quyển sách. Nhất định sẽ tìm mua sách cũ hoặc ebook dể xem cái kết như thế nào.
5
411692
2016-04-09 13:21:25
--------------------------
398904
6574
Đứa trẻ thứ 44 mình đã từng xem phim này và khi thấy nó trên tiki mình đã đặt mua ngay. Một tác phẩm trinh thám xuất sắc lôi cuốn người đọc từng trang sách. Một cuốn sách hội tụ cả 2 yếu tố trinh thám và gia đình. Cách viết của tác giả rất lôi cuốn mạch lạc có nhiều tình tiết khá bất ngờ gây được hứng thú cho người đọc .Bìa sách khá đẹp dày chất lượng giấy in tốt. Đây cũng là phần mình thích nhất trong 3 phần của bộ truyện cũng mong nhà xuất bản sớm cho ra mắt phần tiết theo.
5
580169
2016-03-16 22:08:10
--------------------------
365018
6574
Mới đọc xong cuốn này phaỉ nói là hay kinh khủng luôn ạ. Đọc một mạch từ đầu tới cuối không thể bỏ xót một chữ nào, hấp dẫn ngay từ những trang đầu tiên. Đặc biệt nhất là câu chuyện phản ánh một thời kì lịch sử tăm tối của Xô Viết, một chế độ sai lầm, nới mà người dân luôn sống trong nghèo đói, lo sợ và phục tùng nó được bọc trong một vỏ bọc hào nhoáng và dối trá. Và chúng ta rất ít được biết đến..
 Đây là tác phẩm đầu tiên của tác giả mà viết hay quá mình muốn đọc thêm các tác phẩm khác của Tomrob Smith mong là sẽ sớm được dịch
5
1077508
2016-01-07 11:23:22
--------------------------
363527
6574
Mua cuốn này về, mà chị họ bảo sách có nhiều tình tiết không phù hợp lứa tuổi nên mẹ đem giấu luôn. Hôm đi tham quan với lớp, ráng nài mẹ cho đem theo đọc, hứa là sẽ đọc vài trang đầu thôi. Nhưng mà, mẹ ơi con xin lỗi! Đọc câu đầu tiên đã có mùi hấp dẫn rồi. Đọc nữa, đọc nữa, đọc nữa. Trong lòng cứ gào thét trả lai cho mẹ đi, nhưng mà cứ không thể nào ngừng đọc được. Một câu chuyện tuyệt vời! Không quá khó mà đoán được luôn thân phận của Leo và tên sát nhân, nhưng vẫn không thể dứt ra được. Đang rất mong chờ phần 2, The Secrect Speech!
5
411530
2016-01-04 14:36:47
--------------------------
346737
6574
Cuốn trinh thám đầu tiên mình đọc và nó đã ko làm mình thất vọg. Truyện kể về cuộc truy tìm kẻ đã giết 44 đứa trẻ của thanh tra Leo Demidov và vợ mình là Raisa. Đoạn đầu truyện viết lộn xộn và làm người đọc không hiểu. Nhưng càng về sau câu truyện dẫn dắt ta đến những tình tiết hấp dẫn và đầy bất ngờ. Khi đọc tới gần cuối, thân phận của Leo và kẻ giết người sẽ làm ta ngạc nhiên. Tuy là tác phẩm đầu tay nhưng tác giả đã viết 1 cốt truyện tuyệt vời. Khi đọc ta như cùng đồng hành vs Leo và Raisa đi tìm kiếm những thông tin, trải qua nhiều gian khổ và sự phản bội để bắt được kẻ giết người.
5
919879
2015-12-03 11:07:26
--------------------------
344061
6574
Mạch truyện khá nhanh và lôi cuốn. Dù kể theo lối song hành, nhưng các tình tiết kiểu đấu trí nghẹt thở hay hành đọng chớp nhoáng không nhiều. 

Điểm nổi bật nhất của truyện là tính chân thật. Xã hội Xô Viết được tả thật, và đôi khi làm mình cảm thấy sợ hãi. Những chi tiết khi điều tra viên tự động thêm tên người hắn ghét vào bản cung của tội phạm, lập tức người đó bị bắt, bị tra tấn không cần lý do, phảng phất giống những án oan,  những tranh cãi lùm xùm trên báo chí thời gian gần đây.

Và một điều nữa rất thực là loại nhân cách phản xã hội của nhân vật phản diện. Nhiều bạn sẽ không tin là loại tâm lý tội phạm đó dễ hình thành như vậy, nhưng nó lại có thật! 
4
114065
2015-11-27 18:46:40
--------------------------
339330
6574
ĐỨA TRẺ THỨ 44.
Tựa gốc: Child 44.
Tác giả: Tom Rob Smith.
Thể loại: trinh thám - kinh dị.
Vào năm 1953, khắp Liên Xô xảy ra liên tiếp nhiều vụ án mạng, nạn nhân là những đứa trẻ cả nam lẫn nữ, cùng một cách thức: bị lột truồng, miệng nhét đầy đất, bụng bị mổ moi lấy ruột, chân có cột một miếng vải. Leo là một thanh tra cấp cao của nhà nước bị cách chức xuống làm dân quân, tại đây anh chứng kiến cảnh bắt bớ người vô tội và vu cho họ tội giết những đứa trẻ kia để làm bịt miệng dư luận. Bất bình trước bộ máy thối nát, Leo đã ngầm thực hiện cuộc điều tra vụ án cùng với sự giúp đỡ của vợ anh và những người tốt bụng đang muốn tìm lại công lý.
Vợ anh bị chính phủ vu oan là gián điệp, Leo tin cô không phải là gián điệp nên đã đứng ra bảo vệ cô vì thế anh bị cách chức. Anh cứ tưởng rằng cô yêu anh nhưng hôn nhân của họ chỉ được xây dựng trên sự dối trá, cô lấy anh chỉ vì cô sợ anh - một thanh tra chuyên đi bắt bớ người dân theo lệnh của cấp trên. Hụt hẫng anh thấy cuộc sống mình thật vô nghĩa. Nhờ vụ án mạng, hai vợ chồng chung tay điều tra, mọi nghi ngờ, căm ghét, hiểu lầm đều tan biến, dần dần giữa họ nảy sinh tình yêu thật sự, xóa bỏ đi bức tường ngăn cách giữa hai người bấy lâu.
Càng dấn sâu vào cuộc điều tra, cuộc đời quá khứ và hiện tại của anh được phơi bày. Anh dần thay đổi, không còn là con rối máu lạnh, tàn nhẫn chỉ biết phục tùng chính phủ, thay vào đó anh trở nên thông minh, kiên cường và nhân ái. Anh quyết tâm chuộc lại tất cả lỗi lầm mà trước đây mình đã gây ra. Anh càng cố gắng thì ông trời càng trêu ngươi anh, để rồi kẻ giết người hàng loạt mà bấy lâu nay anh mạo hiểm cả tính mạng mình, tính mạng gia đình anh để truy lùng lại hoá ra là người em trai anh bị thất lạc lúc nhỏ. Động cơ giết người của em trai anh là gì? Và tại sao hai anh em lại thất lạc nhau? Đã có chuyện gì xảy ra đối với tuổi thơ của họ?
"Child 44" là sự kết hợp hoàn hảo giữa trinh thám và tâm lý. Tình tiết đan xen, câu chuyện này dẫn tới câu chuyện khác, tâm lý từng nhân vật được lột tả hoàn hảo. Truyện cũng cho thấy cái xã hội chủ nghĩa Liên Xô cũ đang phải gồng mình trở nên hoàn hảo bất chấp mọi thứ ngay cả lòng tin và cuộc sống của người dân, một xã hội tưởng chừng như tốt đẹp thực chất lại vô cùng thảm bại. Kết thúc vô cùng đau thương, người thì chết, kẻ còn sống thì mang nỗi đau thương suốt quãng đời còn lại. Kết thúc truyện cũng là cái kết cho vụ án giết trẻ em hàng loạt nhưng cũng là cái kết mở ra cho cuộc sống mới mang nặng nỗi đau quá khứ của những nạn nhân là hệ lụy cho những tội ác trên. Theo mình đây là một cái kết rất thông minh, mình mong nxb sẽ sớm xuất bản nốt hai cuốn "Agent 6" và " The secret speech"  của cùng tác giả cũng là phần tiếp theo của "Child 44". Tinh thần chung đây vẫn là một tác phẩm tuyệt vời.
5
93086
2015-11-17 20:41:27
--------------------------
325217
6574
Mình mua cuốn này là do bạn mình có. Lúc lướt qua thì bìa sách làm mình bị ấn tượng mạnh bởi hai gam màu đỏ và vàng nhạt. Chính bìa cũng đã lột tả được phần nào nội dung của sách rồi, gay cấn, bất ngờ và thú vị. Các nhân vật có tính cách rất khác nhau và hơn cả là có cái riêng của họ. Có lẽ đi từ đầu cho đến cuối truyện Leo là người phải trải qua nhiều thăng trầm nhất , từ công việc, gia đìg cho đến tình yêu. Mọi thứ bị đảo lộn bởi kẻ cấp dưới luôn ganh ghét, nhưng có lẽ một phần cũng bởi do Leo quá tin tưởng vào chế độ luật pháp nước nhà, tin tưởng vào thứ luật pháp mà không phải lúc nào cũng đúng. Cuối cùng thì anh đã nhận ra, và có lẽ cũng tìm ra con người thật của chính mình, cả về nghĩa đen lẫn nghĩa bóng. Dù đã đọc từ lâu nhưng mình vẫn khá thích truyện này và vẫn bị lôi cuốn bởi bìa sách :)
4
417101
2015-10-23 00:10:47
--------------------------
322672
6574
Nội dung truyện được viết rất hấp dẫn và lôi cuốn, cách tác giả miêu tả nội tâm, cảm xúc của nhân vật cũng rất tài tình, lối viết mạch lạc chi tiết sống động. Mình đặc biệt ấn tượng với những phân đoạn khi kẻ sát nhân thực hiện những thao tác thuần thục của hắn khi giết một đứa trẻ, thực sự đọc xong thì mình đã bị ám ảnh cách giết trẻ em của tên này. Và cũng rất bất ngờ khi biết đây là tác phẩm đầu tay của tác giả. Cuốn sách đầu tiên mà viết được như thế này thì quả thực rất nể. Bản dịch của Nhã Nam cũng rất mượt !
4
134401
2015-10-16 19:34:57
--------------------------
297230
6574
Tôi mua cuốn sách này vì mục đích sưu tầm là chính. Tôi cũng từng xem bản điện ảnh nhưng hình như bộ phim không lột tả hết dược nội dung của bộ phim.Nếu so sánh chắc là một trời một vực. Câu chuyện được tác giả miêu tả hoàn hảo và khiến cho người đọc phải say mê, tò mò, hồi hộp đến từng câu chữ. Dịch giả đã làm rất tốt để nêu hết ý của tác giả.  Đây là một cuốn sách đáng đọc nếu bạn là người thích những câu chuyện trinh thám. Đây chắc chắn là một trong những cuốn sách yêu thích của tôi. Must read!!!!!
5
798002
2015-09-11 17:43:13
--------------------------
275016
6574
Niềm yêu thích truyện trinh thám đã đưa tôi đến với cuốn sách này. Cuốn sách là hành trình đi tìm tên sát nhân tàn nhẫn đã cướp đi sinh mệnh vô tội của 44 đứa trẻ trên khắp nước Nga của Leo. Mỗi bước điều tra qua từng trang sách luôn lôi cuốn người đọc, tạo cảm giác hồi hộp, kịch tính, thắc mắc, tò mò. Nếu bạn đam mê trinh thám thì nên mua cuốn sách này ngay để đọc nếu không muốn bỏ lỡ câu chuyện trinh thám lôi cuốn này.
4
202527
2015-08-22 15:41:37
--------------------------
263258
6574
Một cuốn tiểu thuyết trinh thám dựa trên một sự kiện có thật, kể về hành trình của Leo, một quan chức cấp cao về chính trị trong bộ máy cũ của Nga thời hậu Stalin, đi truy lùng một kẻ sát nhân gần 50 đứa trẻ trên khắp một vùng nước Nga và cả những nước lân cận.

Cuốn truyện dựa trên một sát nhân hàng loạt có thật. Nội dung truyện trinh thám được xây dựng đầy kịch tính và luôn tạo bất ngờ, dẫu cho bạn nghĩ rằng bạn đã đoán được câu chuyện. Cách kể chuyện xen kẽ nhiều bối cảnh theo trật tự thời gian khiến câu chuyện mạch lạc, mà không ngừng cuốn hút.

Bên cạnh đó, giọng văn cũng không quá lạnh lùng khi miêu tả lại một thời kỳ nước Nga chìm trong chiến tranh lạnh, với một thể chế chính trị dễ bị lạm quyền, quá bạo lực, tàn nhẫn và vô lý. Nhân vật Leo và Raisa được xây dựng như những nạn nhân của nó, tìm đường sinh tồn theo những cách khác nhau, lạc lối trong tư tưởng, tha hóa và đi tìm lại lẽ sống cho chính mình.

Cá nhân mình thì kết thúc truyện vẫn còn chưa viên mãn, khi những mâu thuẫn xã hội vẫn còn đó, chưa giải quyết được.

Bộ phim của Tom Hardy, Noomi Rapace và Gary Oldman không hoàn toàn bám sát tác phẩm, đặc biệt là xuất thân của kẻ sát nhân.
4
41024
2015-08-12 14:10:46
--------------------------
258738
6574
Hay, ấn tượng, lôi cuốn. Truyện đã được dựng thành phim, mình cũng chưa để ý là có vietsub hay chưa nhưng mà mình đã coi trailer có vẻ rất ổn, rất có nét giống Matxcơva giống trong truyện. Kết thúc của truyện cũng khá bất ngờ. Về phần bìa thì rất đẹp, khổ dài nhìn dày dặn. Nói chung là các tác phẩm #1 The NYTimes thì khỏi phải lo về chất lượng rồi. Hơn nữa đây là tác phẩm đầu tay của tác giả, mình thấy như vậy cũng là xuất sắc rồi. Về nhược điểm thì mình cảm thấy vẫn chưa thấy gì hết. Tiếp tục cố gắng ^^.
5
366277
2015-08-08 20:14:12
--------------------------
258423
6574
Người dịch:		Võ Hồng Long
NXB:			Phụ Nữ
Thể loại:		Trinh thám Hình sự
Bìa:			Đẹp – Ấn Tượng
Chất lượng in, giấy: Tốt, chữ hơi nhỏ
Chất lượng dịch:	Tốt
Nhận xét:		“Một tiểu thuyết đầu tay đáng chú ý, sáng tạo, hồi hộp và không ngơi thu hút được sự chú ý của ta từ trang đầu đến tận trang cuối.”, một nhà văn Nga đã nhận xét như vậy. Tôi chỉ cần đồng ý 100%. Cái câu “quân tử 10 năm trả thù chưa muộn” không áp dụng được cho trường hợp của sát thủ trong truyện này. Chỉ vì thù hận lúc còn rất nhỏ, thế mà lớn lên đi giết đến 44 người khác chỉ để trả thù.
Khuyên: 		Hay, nên đọc.

4
129879
2015-08-08 15:31:28
--------------------------
254183
6574
Đây là cuốn tiểu thuyết trinh thám thứ 2 mà mình từng đọc nên để lại ấn tượng mạnh cho mình. Đây là một tác phẩm trinh thám giàu tính nhân văn và tình cảm, khiến người đọc phải đọc kĩ đến từng câu, từng chữ, không dám bỏ sót.  Mỗi phân đoạn trôi qua là một bức màn bí mật như được vén lên một cách từ từ, tạo cảm giác hồi hộp, kịch tính vô cùng. Cuốn sách lồng ghép được khéo léo nhiều loại cảm xúc, người đọc có thể sẽ nhận thấy sự bất công của xã hội lúc bấy giờ.
5
350051
2015-08-05 07:39:53
--------------------------
250163
6574
Là cuốn sách thứ 2 tôi đọc về thể loại trinh thám. Tác phẩm đưa người đọc hồi hộp theo từng diễn biến của câu chuyện và đặc biệt tôi ấn tượng với những lúc Leo hành động, một nhân vật chính hết sức tinh nhạy, lôi cuốn. Tôi thích tình yêu của Leo dành cho Raisa và thích những tình tiết anh đi tìm dấu vết của vụ giết các trẻ em. Là tác phẩm theo thể loại trinh thám không thể tránh khỏi những cảnh mô tả người chết hay lúc giết người và tưởng chừng tác phẩm sẽ không dành cho những ai yếu tim, nhưng không phải vậy nó vẫn đủ sức lôi cuốn ai đã đọc sẽ phải đọc hết câu chuyện. Một tác phẩm hay không thể bỏ qua
5
330142
2015-08-01 10:51:10
--------------------------
248997
6574
Mình không ngờ rằng có một ngày mình lại đọc được một tác phẩm trinh thám giàu tính nhân văn và tình cảm đến như vậy. Tác phẩm chứa đựng đầy đủ từ tình cảm vợ chồng, tình cảm bạn bè, tình cảm gia đình và cả tình người. Hồi hộp ngay từ đoạn bắt đầu đến đoạn cuối. Tác giả đã khiến mình có cái nhìn hoàn toàn khác về kẻ xác nhân khi mình tưởng rằng chính kẻ xác nhân là Leo bị chứng rối loạn nhân cách. Rồi lại nghi ngờ chính Andrei do đã tưởng rằng anh mình đã chết nên đã trở thành một nhân viên MGB với cái tên Leo Demidov để có thể điều tra ngược lại vụ án năm xưa. Và một cái kết bất ngờ ập xuống, mình hoàn toàn không ngờ tới nhân dạng thật sự của Leo lẫn tên xác nhân. Tác giả đã cho ta thấy một điều về MGb đó là "Ai bị bắt là phải có tội, và gần như bị bắt là phải chết". Một cuốn sách không chỉ hồi hộp mà giàu tính nhân văn, ý nghĩa sâu sắc chứa đựng nhiều thông điệp đối với xã hội ngày nay.
5
609835
2015-07-31 11:46:56
--------------------------
247168
6574
Đứa trẻ thứ 44 là cuốn trinh thám hay nhata mình từng đọc. Ở đố có những gam màu u tối nặng nề, chất nặng lên người đọc. Vụ án kì bí, tưởng chừng đã khép lại bỗng mở ra, chân dung nhân dạng méo mó giết người dần hiện ra, song song với đó là sự tìm lại quá khứ của thanh tra Leo. Truyện kịch tính, đủ làm người ta thót tim trong tích tắc, sau đó lại chìm sâu vào mạch truyện. Bên cạnh yếu tố trinh thám, truyện cũng hé mở những ý nghĩa nhân văn, dù lẩn khuất dưới câu chữ, vẫn sâu sắc đến lạ lùng.
4
256191
2015-07-30 09:55:50
--------------------------
245893
6574
"Cuốn trinh thám li kỳ hấp dẫn này sẽ bắt chúng ta thức quá giờ đi ngủ"- Boston Herald. Đó chính là lời nhận xét về đứa trẻ thứ 44 - cuốn sách chính là cuộc đi tìm lại công lý cho những đứa trẻ tội nghiệp bị hại chết - những cái chết đau đớn và vô cùng thương tâm - để hoàn thành ước nguyện được chơi bài cùng anh trai Pavel của tên sát nhân Andrei. Đó không hẳn là chuyến đi tìm lại công lí cho những đứa trẻ mà còn là chuyến đi tìm laị kí ức, qúa khứ của một người lính Xô Viết dũng cảm thông minh vén laị bức màn quá khứ của mình mà mình hằng chôn giấu bấy lâu nay. Một cuốn sách không chỉ hồi hộp, gay cấn mà còn đọng lại rất nhiều ý nghĩa sâu xa
5
417795
2015-07-29 17:17:40
--------------------------
241542
6574
Xô Viết trong ‘Đứa trẻ thứ 44’ được mô tả bằng những gam màu u ám, bất công và mọi người chìm trong không khí nghi kỵ và bài xích lẫn nhau. 
Định hướng xã hội chủ nghĩa vào thời kỳ này là tập trung vào công nghiệp nặng, trong khi điều kiện sống của người dân thì lại thê thảm, cái đói vẫn lén lén lủi vào bàn ăn của mọi gia đình, người dân xếp hàng hàng giờ ở cửa hàng thực phẩm nhưng vẫn không có đủ rau thịt tươi sống để mua; các gia đình được xếp chen chúc ở trong những ngôi nhà đã kịp xuống cấp trước cả khi người chủ đầu tư ký đơn vào hoàn thành công trình. 
Theo ghi nhận thời kỳ này số lượng tội phạm là 0, vậy tội ác của xã hội được xử lý như thế nào, thế nên nảy sinh ra một đội ngũ làm công việc chôn vùi sự thật, đội ngũ đó là MGB. 
Có một quy tắc được MGB xây dựng “Không được phép làm sai, người đã bị bắt thì nhất định có tội”, nên bằng mọi cách họ phải bắt được đối tượng tình nghi, đối tượng đã bị bắt thì nhất định phải kết án. Công bằng và dân chủ thiếu vắng hoàn toàn trong tâm trí người dân vì con người có thể bị xử bắn chỉ dựa trên những hồ nghi và lời đồn đại không chứng cứ. 
Gấp sách lại, nhớ nhất chính là khi Leo từ chối cùng Raisa bỏ trốn khỏi nước Nga, khi mà lý tưởng về tổ chức đã không còn, anh ta bị coi là phản bội, bị truy đuổi và nắm chắc cái chết, Leo vẫn chọn rời bỏ người yêu, quyết định cuộc đời của mình không phải ở nước ngoài – nơi những người chỉ muốn thông tin của anh ta – chứ không phải lý tưởng của anh.
4
38833
2015-07-25 16:09:09
--------------------------
240131
6574
   Tomrob Smith đã lấy tư liệu cho Đứa trẻ thứ 44 của mình từ câu chuyện có thật của kẻ giết người hàng loạt của Nga - Andrei Chikatilo - hắn đã sát hại trên 50 phụ nữ và trẻ em ở Nga trong những năm tám mươi. Mặc dù Smith đã đặt câu chuyện của mình vào khoảng thời gian trước đó, những năm năm mươi, ông chỉ đạt đến mức độ hồi hộp nào đó bằng cách chọn những năm tồi tệ nhất trong sự đàn áp của Liên Xô. Tomrob Smith lấy được sự ngưỡng vọng của tôi sau khi tạo nên được tác phẩm có sức nặng về mặt tâm lí. Cuốn sách như thể một thế giới đầy ác mộng.
5
529844
2015-07-24 12:29:13
--------------------------
234279
6574
Ban đầu đọc thì thấy những mẩu chuyên rời rạc, khó hiểu nhưng càng đọc lại càng thấy liên hệ chặt chẽ với nhau. Càng đọc mình càng bị thu hút, chỉ trong vòng một ngày mình đã đọc hết quyển sách. Câu chuyện kể những vụ án mạng liên tiếp xảy ra xung quanh khu vực Mat-Xco-va mà nạn nhân là những đứa trẻ. Điểm chung của những đứa trẻ sau khi giết là bị mổ bụng cắt mất dạ dày, miệng bị nhét đầy vỏ cây. Nhân vật chính là thanh tra Leo, anh đã tình cờ phát hiện ra hàng loạt vụ án man dợ như thế. Anh và vợ là Raisa đã cùng nhau đi tìm chân tướng thực sự, quá trình đó đầy gian nan nguy hiểm. Tìm được chân tướng cũng có nghĩa là anh phải đối mặt với quá khứ của mình đối mặt với người em trai đã luôn tôn thờ anh từ nhỏ. Câu chuyện đầy kịch tính nhưng cũng có nhiều tình tiết gây xúc động
5
597863
2015-07-20 11:13:35
--------------------------
233708
6574
Đây là một trong số ít tác phẩm viết về Liên Xô mà mình từng đọc và cảm thấy thật sự bị cuốn hút. Truyện mang đậm chất trinh thám khi xuyên suốt là những vụ án giết hại trẻ em đầy thương tâm, trong khi động cơ cũng như dấu vết của hung thủ đều là con số không. Và hành trình của nhân vật Leo đi tìm lời giải cho những cái chết bí ẩn càng trở nên hồi hộp và hấp dẫn hơn bao giờ hết. Đó không chỉ là một cuộc tìm kiếm chân dung kẻ giết người mà còn là con đường đi đến công lý, lẽ phải và cũng là lời tố cáo đối với cái xã hội lạnh lùng, đầy khắc nghiệt, nơi con người thậm chí không thể nắm giữ số phận của chính mình. Truyện có nội dung rất hay, lại chứa đựng nhiều ý nghĩa, thật sự là một tác phẩm rất đáng để đọc. 
5
387532
2015-07-19 21:00:55
--------------------------
219198
6574
Bối cảnh của truyện là Liên Xô những năm 50-60 thế kỉ 20, với một bộ máy hành chính và pháp luật cực kì quan liêu (chính cái này là cốt lõi gây nên mọi thứ), nên thực sự thì những bạn nào ngưỡng mộ Xô-viết sẽ cảm thấy bị hụt hẫng. Nhìn chung đây là một cuốn sách hay, xây dựng tính cách nhân vật chi tiết, các nhân vật đều có số phận rõ ràng, chuyển biến tâm lý nhân vật tốt, mình rất ấn tượng với cách miêu tả, lý giải cho những thay đổi trong mối quan hệ của 2 vợ chồng Leo và Raisa. Kết cấu truyện nhiều nút thắt mở, bất ngờ, mang tính hành động cao (cực kì phù hợp để chuyển thành kịch bản phim điện ảnh). Tuy nhiên về cuối không biết vì lí do gì nhưng theo quan điểm của mình tác giả xử lý chưa tới: thứ nhất là sự giúp đỡ vô điều kiện bất chấp tính mạng của những người cùng đi trên chuyến tàu tới trại Gulag, tiếp đến là sự giúp đỡ của dân làng (mình thấy hơi dễ dàng), bởi thực sự để giữ bí mật với một số lượng lớn người biết như thế là rất khó trong khi ở đây nếu đặt lên bàn cân 2 mặt lợi và hại thì sự chênh lệch là rất lớn (một bên nếu tố cáo thì thoát nghèo, đổi đời, một bên nếu bị phát hiện thì sẽ bị tử hình tất cả). Còn một chi tiết nữa mình lấn cấn: số phận 2 cô con gái của Andrei thì thế nào, hay tác giả sẽ có tiếp phần 2, nếu thực như vậy thì có lẽ sẽ rất thú vị.
4
92670
2015-07-01 11:01:27
--------------------------
217454
6574
Đứa trẻ thứ 44 là một cuốn sách khá nổi tiếng trong giới hâm mộ những người đọc truyện trinh thám, mình cũng chỉ mới bắt đàu chuyển sang đọc thể loại sách này và đây là cuốn đầu tiên mình chọn để đọc, Nội dung được giới thiệu rất kịch tính và u ám :( những cái chết trong truyện ám ảnh mình vô cùng. Càng đọc chuyện mình càng thấy lạnh, thậm chí là sợ, tuy nhiên cốt truyện lại không gây quá nhiều bất ngờ cho mình, có lẽ mình đã mong chờ ở nội dung quá nhiều chăng??
4
116557
2015-06-29 12:30:09
--------------------------
215071
6574
Truyện đọc rất hay nhưng không hiểu sao trong khi đọc đã lờ mờ nhận ra cái kết, có lẽ dạo này đọc nhiều truyện trinh thám quá nên khả năng đọc vị ngày càng tăng rồi. Cái thích nhất ở truyện này chắc là bối cảnh truyện, liên bang xô viết, những nỗi đau chiến tranh, những tranh chấp trong thời bình, thấp thoáng trong đó cái vị thời cải cách ruộng đất ở Việt Nam, cách mạng văn hóa ở Trung Quốc. Cứ thắc mắc nước chủ nghĩa xã hội nào cũng có cái thời nhập nhằng mắc cười mà không cười nổi thế á.
4
419116
2015-06-25 20:32:10
--------------------------
209250
6574
Khi đọc Đứa trẻ thứ 44, ta không chỉ cảm thấy hồi  hộp mà còn đôi phần nghẹt thở vì sợ hãi. Câu chuyện với nhiều bí ẩn xuyên suốt, đọc những chương đầu, ta không khỏi thấy tò mò và đôi chút khó hiểu vì những mẩu riêng rẽ, tưởng chừng như không gì liên quan tới nhau, nhưng càng đi sâu lại càng thấy hấp dẫn. Muốn đọc một lúc cho tới hết để xem cái kết cục của Leo, của số phận con mèo nhà bà Maria. Hy  vọng cuốn tiểu thuyết sẽ khởi  đầu cho nhiều cuốn sách tuyệt vời nữa.
5
491413
2015-06-17 09:48:56
--------------------------
203520
6574
cuốn sách này thực sự ấn tượng. Bạn sẽ bị cuốn vào câu chuyện ngay từ những đoạn đầu tiên, trang đầu tiên chứ không dài dòng và có phần thiếu sức hút như những cuốn sách khác thuộc thể loại trinh thám ở phần mở đầu. Sau khi đã bị cuốn vào thì có thể bạn sẽ đọc nó một cách miệt mài lúc nào không hay.
Về nội dung của cuốn sách thì tôi nghĩ nó khá táo bạo đặc biệt khi lấy bối cảnh xã hội Liên Xô những năm 50. Đó là thời kỳ tranh tối tranh sáng của chủ nghĩa xã hội mà nhìn qua thì người ta không thể cảm thấy được sự ngột ngạt trong nó
5
263240
2015-06-01 15:24:26
--------------------------
197175
6574
Đọc đứa trẻ thứ 44 mang lại cho ta những cảm xúc hồi hộp qua từng phân đoạn. Mỗi phân đoạn trôi qua là một bức màn bí mật như được vén lên một cách từ từ, tạo cảm giác hồi hộp, kịch tính vô cùng. Bắt đầu là về tên sát nhân sau đó là thân thế thực sự của Leo lại là anh trai của kẻ giết người tàn độc. Nhưng cuối cùng Leo đã cho ta thấy phẩm chất của người lính Xô Viết khiến người đọc cảm phục vô cùng. Ấn tượng nhất phải nhắc đến chính là tình tiết Leo đối mặt với em trai của mình nó mang tới cho chúng ta những cảm xúc lẫn lộn nhưng đáng trân trọng
5
578865
2015-05-17 08:09:34
--------------------------
177963
6574
Vừa đọc xong cuốn này cách đây 1 tuần. Phải nói là kịch tích đến nghẹt thở.
Rất khâm phục tác giả, dù là người Anh nhưng rất am hiểu địa hình, lịch sử, văn hóa thời Xô Viết.Tự nhiên thấy thêm yêu nước Anh và Nga.
Tiểu thuyết này đã được dựng phim và sẽ được công chiếu tại Việt Nam vào cuối năm 2015. Phim này chắc chắn là bộ phim mà mình sẽ lên lịch xem đầu tiên. 
Nếu bạn nào có ý định xem phim chiếu rạp này trong thời gian tới, bạn nên đọc xong sách này rồi xem phim, rất thú vị. (Với lại đọc truyện lúc nào cũng hay hơn xem phim - mình nghĩ vậy).
5
533454
2015-04-03 22:43:16
--------------------------
151912
6574
Mặc dù được giới thiệu là truyện trinh thám nhưng cuốn Đứa trẻ thứ 44 không quá tập trung vào những tình tiết ly kì, rùng rợn mà thay vào đó là những suy nghĩ của cá nhân, những điều mà một con người phải gánh chịu khi rơi vào cảnh cô đơn. Ngoài ra xã hội Liên Xô thời bấy giờ cũng được trình bày rất rõ nét, giúp ta hiểu hơn về cuộc sống và con người thời đó. Một con người tốt nhưng dưới sự tác động mạnh mẽ của xã hội sẽ trở nên như thế nào? Bạn hãy đọc tác phẩm này để biết được điều đó.
4
316903
2015-01-21 14:46:26
--------------------------
146743
6574
Trước hết cũng xin nói luôn là mình đọc quyển này không phải với tư cách truyện trinh thám, mà là một câu truyện về những dằn vặt của Leo Demidov, nhân vật chính.

Thật ra Đứa trẻ thứ 44 không có những câu hỏi khó khiến độc giả đoán mò vì tác giả đã tiết lộ hung thủ từ rất sớm, khó đoán chăng cũng chỉ là động cơ phạm tội của hắn. Cách tháo mở các tình tiết cũng rất tuyệt vời, nhưng vô cùng thích cách viết của Tom Rob Smith trong quyển này! Nhưng điều khiến mình thích ở tác phẩm này là bạn không thể ghét bất kì nhân vật nào, phản diện cũng vậy chính diện cũng vậy. Mình không nói rằng sau khi đọc xong mọi người phải yêu hết các nhân vật, chỉ là bạn không thể nào không thích tất cả nhân vật. Do tất cả bọn họ đều là những linh hồn tội nghiệp bị biến thái trong một xã hội quá chuyên chế đến mức cực đoan. Nhất là nhân vật chính, Leo Demidov. Nếu Nhã Nam có xuất bản hết bộ ba tập, chắc hẳn bạn sẽ cảm thấy yêu mến Leo như mình, bởi vì không yêu sao được một người đàn ông quyết làm tất cả vì gia đình mình, quyết làm điều đúng đắn theo tiếng gọi của lương tâm?
5
46351
2015-01-05 12:02:16
--------------------------
135279
6574
Đứa trẻ thứ 44 quả thật không chỉ là một câu truyện trinh thám đơn thuần. 
Nó là cả một thông điệp to lớn về thời đại mà nó muốn nói đến, về sự tồn tại cô đơn của con người có thể làm cho người ta trở nên thế nào.
Câu truyện trên cả nhưng tình tiết giết người, trên cả một kẻ sát nhân, với mình nó cho thấy tầm quan trọng của tuổi thơ một con người sẽ ảnh hưởng to lớn thế nào đến tương lai của đứa trẻ. 
Cộng với việc mô tả một cách thẳng thắng xã hội Liên Xô thời đó, không che giấu, phô bày tất cả những gì xấu xa và độc ác của những người cầm quyền hay giữ chức vụ cơ quan nhà nước nhưng không có lương tâm, đã đem lại kết quả xấu thế nào đến từng con người trong xã hội
Mình đã đọc truyện này liên tục chỉ trong 3 ngày. Và luôn cảm thấy thích thú với từng câu chữ của truyện. 
Nếu bạn tìm kiếm một câu truyện trinh thám thông thường, Đứa trẻ thứ 44 đáp ứng được bạn đấy. Và nếu bạn tìm kiếm gì đó ra ngoài một câu truyện trinh thám thông thường, bạn cũng hãy đọc nó, mình nghĩ sẽ chẳng ai thất vọng với nó cả.
5
35952
2014-11-14 12:55:22
--------------------------
131554
6574
Câu chuyện bắt đầu bằng những mẩu chuyện rời rạc, nhưng thực ra lại có quan hệ mật thiết với nhau. Nhân vật chính của tiểu thuyết này là một mật vụ MGB thuộc Liên Xô cũ, Leo (vì cái tên này mà suốt quá trình đọc tôi cứ tưởng tượng ra mặt của Messi). Anh này vốn là một mật vụ theo chiều hướng hơi cực đoan, không thể trách anh được bởi anh chỉ là một công cụ của bộ máy luật pháp lúc bấy giờ tại Liên Xô, nơi mà cái ác được che đậy, nơi mà công lý đồng nghĩa với việc bị tù giam, xử tử. Leo tình cờ phát hiện ra hàng loạt vụ án mạng mà nạn nhân hầu hết là trẻ em, bị giết theo cách thức man rợ: chúng bị lột trần, rạch bụng, mồm nhét đất. Leo cùng vợ là Raisa đã cùng nhau vượt qua nhiều thử thách nguy hiểm để từ đó tìm ra chân tướng gây sốc của sự việc, đồng thời chính anh cũng phải đối mặt với quá khứ mà anh đã muốn chôn vùi từ lâu.
Đây là một dạng tiểu thuyết mà bạn sẽ thích ngay từ trang đầu tiên, sau đó bạn sẽ muốn đọc một mạch cho đến hết. Câu chuyện gồm nhiều nhân vật phụ khác nhau, nhưng nhân vật nào cũng có cá tính và sắc màu riêng, khiến người đọc không hề cảm thấy bị rối loạn tuyến nhân vật mà thậm chí còn cảm giác thích thú khi đọc. Những nhân vật này đều đại diện cho hai bên thiện/ác, có cả những nhân vật nửa chính nửa tà mà đại diện là nhân vật chính Leo và ngài thanh tra Nesterov (đây cũng là 2 nhân vật mà tôi thích nhất). Ngoài ra, tác giả còn sáng tạo ra một cốt truyện cực kì li kì và mạch lạc, các sự kiện đều kết nối với nhau đầy sáng tạo và bất ngờ. Càng về cuối, câu chuyện càng trở nên hấp dẫn, các đầu dây đều cùng chạy về một điểm thắt cuối cùng khiến người đọc đi từ bất ngờ này tới bất ngờ khác. Đó là một đặc điểm cần phải có của một tiểu thuyết trinh thám hay: sự bất ngờ. Và tác giả Tom Rob Smith đã hoàn thành xuất sắc nhiệm vụ này. Bên cạnh cốt truyện ngoạn mục, tác giả cũng vẽ ra được bức tranh toàn cảnh của xã hội Liên Xô cũ, nơi mà sự thối nát của pháp luật lên ngôi nhưng vẫn không đủ sức mạnh để ngăn cản tình thương giữa con người với con người. Cuối cùng, câu chuyện này còn mang một thông điệp sâu sắc khác, rằng mọi sai lầm đều có thể được sửa chữa, miễn là ta sửa chữa kịp thời và đúng cách. Đó là cách nhân vật chính Leo tìm lại được bản chất hướng thiện của mình.
Một điều nữa khiến quyển sách này trở nên thu hút đó là cách dịch. Người dịch Võ Hồng Long đã sử dụng những từ ngữ tiếng Việt tuyệt hay và mạch lạc, khiến câu chuyện như được viết ra bởi một người Việt giỏi tiếng Việt chứ không phải là được dịch ra từ tiếng nước ngoài. Có những câu chữ được người dịch đưa vào rất sống động, tôi phải tự hỏi rằng anh/chị/cô/chú này phải học giỏi tiếng Việt vô cùng mới tìm ra được những cách dùng từ, cách đặt câu đầy hấp dẫn tới như vậy.
Sách khá dày, bìa làm theo nguyên mẫu nên khá đẹp, nhìn vào thấy tinh thần Xô viết lên cao.
5
148668
2014-10-25 21:05:05
--------------------------
126364
6574
Đây là một cuốn sách mượn vụ án để mô tả xã hội Liên Xô thời hậu Stalin đấy mà thôi. Dựa trên một vụ án có thật về tên giết người hàng loạt vùng sông Đông Andrei Chikalito, tác giả đã khéo léo lồng bối cảnh xã hội, khắc họa tâm lý nhân vật một cách rất sắc nét và chân thực. 
Về phần dịch , đây là một cuốn sách dịch tốt, không khí truyện ngột ngạt và căng thẳng đã được người dịch truyền tải sinh động. Tại sao là dịch là "gã"- vì bản thân Leo tuy là nhân vật chính, nhưng anh ta lại được dẫn dắt từ vai của một kẻ tay sai, kẻ săn người, đến lúc gặp biến cố và tỉnh ngộ , dịch là "gã" sẽ tạo nên cảm giác chênh vênh, hoài nghi về tâm lý và số phận của nhân vật này.
4
358488
2014-09-18 16:54:10
--------------------------
125849
6574
Nếu bạn nào yêu thích thể loại trinh thám nguyên gốc và mong chờ một tác phẩm đỉnh cao. Đây không phải là cuốn sách dành cho bạn. 
Tình tiết vụ án chán, dễ đoán, và không có gì đặc biệt.
Một điểm cộng là cuốn sách khắc họa được chế độ xã hội điên rồ, bức bách và bệnh hoạn thời bấy giờ, đồng thời miêu tả tâm lý nhân vật khá tốt. Bản thân mình đã đọc cuốn sách một mạch từ đầu đến cuối, không dừng lại được. Không phải vì tình tiết hấp dẫn, mà vì mong muốn được giải thoát khỏi cái xã hội trong truyện. Một xã hội đẻ ra toàn loại người đạo đức giả, chà đạp lên nhân quyền một cách không thương tiếc. Một xã hội cực đoan đến điên rồ. Bạn có thể thấy áp lực "phải đọc tiếp" khi giở từng trang sách, vì dường như cái áp lực ấy cũng thúc đẩy nhân vật chính đi tìm một kết thúc cho chính bản thân mình.
Như đã nói từ đầu, xét về khía cạnh trinh thám thì cuốn sách này rất rất rất dễ đoán. Còn nếu xét tổng thể, thì đây vẫn là một cuốn sách đáng đọc và cho bạn nhiều suy ngẫm.
3
354876
2014-09-15 22:15:48
--------------------------
123019
6574
Cốt truyện không phải khó đoán. Đọc mấy trang đầu, có thể đoán ngay ra Leo là ai !. Đọc đến 1/3 cuốn sách có thể đoán ra được hung thủ là kẻ nào, và mục đích là gì ?. Tuy nhiên không hiểu sao, cuốn sách lại có sức lôi cuốn hết sức đặc biệt với mình. Khiến mình đọc một mạch không thôi, không buông khi còn chưa xong. Phải nói, lâu lắm rồi mình mới đọc một cuốn sách tâm đắc như vậy.
Nhìn bìa sách, lúc đầu mình thấy nó thật xấu xí và cổ lỗ, nhưng sau khi đọc thì thấy rất hợp với nội dung câu truyện.
Tác giả rất tài tình trong câu chữ, và dịch giả đã cảm nhận được thông điệp đó để truyền đạt lại cho độc giả. Nên có thể đọc mãi mà không thấy nhàm chán. Đặc biệt, cuốn sách lồng ghép được khéo léo quá nhiều loại cảm xúc. Người đọc sẽ cảm nhận được sự bất công của xã hội bấy giờ, sự giả dối, sự lừa đảo, sự hối lận, tội lỗi, yêu thương, phản bội ở trong đó.
5
348024
2014-08-29 15:15:05
--------------------------
122482
6574
Theo mình thì cuốn sách này không có nhiều điểm mới hay nổi bật so với những tác phẩm trinh thám khác. Đúng là tác giả đã rất sáng tạo trong việc kết hợp trinh thám và lịch sử để tạo nên một câu chuyện hấp dẫn, nhưng vẫn còn khá nhiều chi tiết trong truyện chưa thực sự xuất sắc. Có nhiều đoạn miêu tả những cảnh rượt đuổi nghe có vẻ giống kịch bản trong một bô phim nào đó, lại có nhiều đoạn lại khá dài dòng, từ ngữ miêu tả cứ chồng đắp lên nhau gây cảm giác khó hiểu. Được cái là càng về cuối càng hấp dẫn hơn, khi Leo lần theo dấu vết của kẻ giết người hàng loạt, cái kết mang tính nhân văn cao, dù có thể nó sẽ mang lại tiếc nuối cho người đọc.
Một điều mình không thích ở tác phẩm là nhân vật chính - Leo lại bị gọi là "gã", nghe mất hết cả hình tượng một điều tra viên xuất sắc. Đáng lẽ nên dịch là anh thì đúng hơn. Nhưng dù sao đây cũng là một tác phẩm tạm ổn, đọc để giải trí và thử xem phán đoán của bạn đến đâu.
3
109067
2014-08-26 11:02:12
--------------------------
