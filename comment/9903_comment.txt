477781
9903
Mình biết đến tác phẩm này nhờ đọc ké của một người bạn, lúc đầu chỉ định đọc cho vui thôi nhưng không ngờ truyện lại hấp dẫn và đúng sở thích của mình nữa. Đọc đi đọc lại nhiều lần vẫn không thấy chán.  Mỗi khi có phần mới ra là mình liền tìm mua
Đây là phần cuối cùng của bộ truyện này. Đọc xong cảm thấy khá tiếc nuối vì không còn được tiếp tục đồng hành với cậu bé Eragon và cô rồng Saphira. Phần kết này khá hay, phần chiến đấu với nhà vua diễn ra nhanh và không gay cấn như tưởng tượng lắm. Tuy kết thúc không như mong đợi nhưng theo tôi thì khá ổn và biết đâu tác giả lại viết thêm phần tiếp theo :)
5
283530
2016-07-28 20:59:26
--------------------------
429025
9903
Đây là phần cuối của series truyện này!! Một cái kết khá hay!
Mình đã theo dõi bộ truyện này từ rất lâu rồi nên khi truyện kết thúc thì khá tiếc nuối. Kết truyện khá hay nhưng theo mình thì trận chiến giữa Eragon và vua diễn ra quá nhanh, thiếu kịch tính, không mhuw mình mong đợi. Chuyện tình cảm của Eragon cũng đã được rõ ràng.
Về hình thức thì không cần phải bàn cãi nữa, bìa đẹp, chắc chắn, tiki bọc sách rất đẹp vav giao hàng rất nhanh.
Mình nghĩ đây là cuốn sách mà nhưng bạn theo dõi series này nên mua!!
5
1293922
2016-05-12 12:14:25
--------------------------
343104
9903
mình biết được bộ sách này qua đọc ké của anh trai, và mình thấy quá hên khi đọc được bộ truyện này. Truyện này thật sự rất gay cấn, có nhiều pha hành động hiểm hóc, và cốt truyện rất kịch tính. Mình rất ấn tượng và đặt biệt thích cô rồng Saphira - 1 cô rồng với tính bướng bỉnh nhưng không kém phần đáng yêu. Nhưng dù sao mình vẫn cảm thấy hình như cuộc chiến khúc cuối diễn ra nhanh làm sao ấy... tập cuối để lại trong mình rất nhiều dư âm, trăn trở về Eragon và Saphira, và cũng đầy lắng đọng với kết thúc mở... cảm ơn tác giả nhiều vì đã cho mình được cảm nhận 1 câu chuyện hành động viễn tưởng hay như vậy.
5
774087
2015-11-25 19:40:30
--------------------------
309690
9903
Đây là cái kết hậu cho sự đấu tranh của Varden chống lại nhà vua. Nhưng trận chiến đấu của Eragon với vua dường như quá ngắn, không nhiều kịch tính và không làm thỏa mãn được fan hâm mộ lâu năm của nó. Chuyện tình cảm giữa Eragon và Arya cũng được công khai, nhưng cái kết của nó thật tiếc nuối và có lẽ không ai muốn vậy.   Một bất ngờ ( có thế đoán được ) là Arya cũng đã trở thành kỵ sĩ rồng.Cuối truyện, Eragon cùng Saphira và một số thần tiên bay đi tìm vùng đất mới cho rồng và kỵ sĩ. Chuyện gì sẽ xảy ra? Tôi rất mong chờ tập mới của bộ truyện từ Christopher Paolini.
4
508964
2015-09-19 08:37:39
--------------------------
304010
9903
Truyện này càng đọc càng hay. Không có vẻ kì diệu lung linh như Harry Poter nhưng eragon là cả một quá trình cố gắng của tất cả các nhận vật để đạt được thành công. Eragon thể hiện sự kiên trì, bền bỉ, không phải may mắn mà có thể thành công. Không chỉ eragon mà tính cách của các nhận vật khác cũng rất hay. Nasuda chỉ là một cô gái bình thường nhưng lại là một thủ lĩnh giỏi. Anh họ eragon chỉ là người thường cũng nhưng ý chí vô cùng kiên cường. Truyện rất đề cao sự có mặt của con người. Mình cũng rất thích khi tác giả đã cho Murtag thay đổi để đi đến cái kết bất ngờ. Nhưng mà mình cảm thấy kết thúc như vậy vẫn nhiều tiếc nuối quá. Cảm thấy các nhân vật vẫn rất cô đơn. 
5
30458
2015-09-16 09:27:45
--------------------------
279324
9903
Cái kết đậm dấu ấn quen thuộc, một trận sinh tử quyết định thành bại về sau và coi như hết, thế là xong! Y hệt cách kết thúc của các câu chuyện cũ ngày xưa của thế hệ 8x, thế nhưng tôi cũng rất ưng ý với đoạn kết với như vậy, có lẽ nó khiến tôi thấy gần gũi hơn chăng, tuy vẫn giữ được mạch truyện và các diễn biến hấp dẫn, thì đoạn kết chốt lại như vậy là đã ổn thỏa, đúng hơn là có hậu với các nhân vật trong câu chuyện đó! 
3
516261
2015-08-26 17:44:54
--------------------------
270152
9903
Thế là cuối cùng mình cũng đã chờ đợi và theo dõi xong toàn bộ diễn biến của cuộc hành trình phiêu lưu của cậu bé Eragon. Mới ngày nào cậu còn bỡ ngỡ với quả trứng rồng và cô rồng Saphira thì giờ đây cả 2 đều đã trưởng thành và sẵn sàng cho trận đánh cuối cùng với lão vua tàn ác thống trị vùng đất bấy lâu. Tuy nhiên thì cái kết không thật sự gây ấn tượng bởi mọi thứ quá bình thường không gay cấn, không hồi hộp mà chỉ đơn giản là 1 trận đấu. Tuy nhiên thì đây cũng là tác phẩm đầu tay của 1 cậu bé sinh viên thì cũng có thể xem là 1 tác phẩm thành công. Về cái kết thì mình chưa thật sự hài lòng khi mà mọi thứ không trọn vẹn hoàn toàn. Hi vọng rằng tác giả sẽ cho ra thêm nhiều bộ tác phẩm hay
4
39383
2015-08-17 23:33:44
--------------------------
266574
9903
Vậy là cuộc phiêu lưu của Eragon đã kết thúc.
Cậu bé nông dân ngày nào nay đã trưởng thành. Mang trên mình gánh nặng của cả đất nước, chàng vẫn tiếp tục tiến lên, không hề lùi bước, không hề sợ hãi! Dù có đôi khi do dự, đắn đo, đã tưởng chừng bỏ cuộc nhưng có đồng đội kề bên, chàng lại vững tin tiến về phía trước! :)

Một câu chuyện hay nhưng kết thúc thật hụt hẫng biết bao nhiêu! Hy vọng vào một khúc ca chiến thắng thật oai hùng để rồi chiến thắng lại đi kèm nối tiếc, khổ đâu!
Một kết thúc chưa thể thoả mãn được người đọc với những bí ẩn trùng trùng lớp lớp trong truyện... Thế nhưng, lời hứa Paolini cuối truyện lại cho ta thêm hy vọng một lần nữa lại thấy được vùng đất Alagaesia nhiệm màu! :D
3
274085
2015-08-14 20:16:12
--------------------------
262141
9903
Eragon 4 (Inheritance) - Di Sản Thừa Kế cũng là phần cuối của câu chuyện. Thú thật mình đã không thể dời mắt chỉ để biết được cái kết sau hành trình theo dõi từ tập 1 đến giờ. Và chắc hẳn nhiều bạn theo dõi Eragon đều chung ý nghĩ với mình.
Điểm cộng: Giấy tốt, mềm, xốp, cốt truyện hấp dẫn, đầy kịch tính và cao trào cũng như giàu tính nhân văn.
Điểm trừ: Vì mình mua 1 lúc 6 cuốn nên đọc liền tù tì từ tập 1 đến tập 4 mà không bị ngắt quãng. Tuy nhiên theo ý kiến của mình thì khúc cuối của truyện khá là chán, giết xong Boss cuối là mình chẳng muốn đọc tiếp. Mình không thích cách tác giả cứ lê thê kéo dài. Bìa sách trông cũng không đẹp.

4
195704
2015-08-11 18:00:25
--------------------------
250751
9903
Há há, cuối cùng thì cũng hết. Đang tuần bận rộn với bài tập lớn mà không rời được quyển truyện ra, phải nấn ná, cố gắng đọc cho xong. Giờ thì đọc xong rồi lại thấy tiếc vì đã hết rồi à??? :D
Cái kết cũng không tồi nhỉ??? Tự thấy là mọi thứ đều đẹp. Nhưng Galbatorix chết bất ngờ quá nhỉ, đọc qua đoạn đó mà cứ hi vọng là sẽ còn trận tái đấu quyết liệt hơn phía sau chứ. Cứ hi vọng là Galbatorix không chết nhanh thế để chờ thêm hồi gay cấn nữa chứ.
P/s: xong rồi, giờ tập trung vào làm bài tập nào.Yo!!!
À, mà thấy phần cảm ơn, anh tác giả có nói về những câu truyện khác, có ai biết tác phẩm nào của anh này thì giới thiệu cho với :D
5
140066
2015-08-02 10:49:10
--------------------------
231299
9903
kể từ khi đọc cuốn Eragon đầu tiên, tôi như bị cuốn vào một thế giới khác: thế giới nơi loài người, người lùn, tiên cùng nhau chống lại tên bạo chúa Galbatorix.Qua từng phần cuốn sách, tác giả đã cho ta thấy được sự trưởng thành của cậu bé Eragon cũng như sự lớn mạnh thêm của quân khởi nghĩa. Ở tập cuối này, mọi thứ đều diễn ra theo cái cách mà mọi người đều đoán trước được: cái ác bị tiêu diệt. Tuy nhiên nó không làm mất đi cái hay của bộ truyện. Vẫn còn đó những cảnh chiến đấu ( dù không hấp dẫn và dễ đoán hơn những phần trước), những suy nghĩ nội tâm về tình cảm giữa người và tiên của Eragon,... Tất cả đã tạo nên mọt phần kết thúc tuy không bất ngờ nhưng vẫn trọn vẹn cho những ai yêu mến bộ truyện.
3
53934
2015-07-17 23:12:57
--------------------------
219114
9903
Với những người đọc truyện lâu năm như mình thì cái kết này quả thật bình thường, cái ác bị tiêu diệt và cái tốt lên ngôi.
Mình thì vẫn mong tác giả chọn cái kết có hướng mở hơn, cái xấu bị tiêu diệt nhưng vẫn còn mầm mống, để các kỵ sĩ còn có ý chí quyết tâm để bảo vệ thế giới hơn. Tuy vậy vẫn còn khá nhiều chi tiết nhỏ vẫn chưa được tác giả viết hồi kết cho chúng. 
Nhưng dù gì thì cái kết như vậy cũng khiến đa số độc giả hài lòng rồi. Một kết thúc có hậu cho nhiều người. Hy vọng tác giả trẻ sẽ tiếp tục viết ra thật nhiều câu chuyện hay hơn nữa.
4
37970
2015-07-01 09:43:33
--------------------------
156769
9903
Di sản thừa kế kết thúc với cái kết mà tất cả chúng ta đều mong đợi: Galbatorix bị tiêu diệt, hòa bình trên khắp vùng đất Alagesia, sự trở lại của các kị sĩ rồng. Tập cuối không chỉ là cái kết đẹp mà còn khiến cho người đọc cảm nhận sâu sắc được sự mất mát to lớn của cuộc chiến khi nhiều người đã ngã xuống trong trận chiến cuối cùng: nữ hoàng Islanzadi, những người lính của phe Varden, tiên và người lùn. Arya đã trở thành kỵ sĩ rồng mới và thay Eragon trở thành người bảo vệ vùng đất thay cho Eragon. Eragon cùng với Saphira và Glaedr ra đi để trở thành người dạy dỗ cho thế hệ các kỵ sĩ rồng mới. Câu chuyện kết thúc với một cái kết đẹp nhưng vẫn còn để lại trong người đọc nhiều câu hỏi chưa có lời giải đáp, gây ra sự tò mò. Một kết thúc xứng đáng với bộ truyện kinh điển của Paolini.
5
291067
2015-02-06 12:10:09
--------------------------
128580
9903
Ở phần Di Sản Thừa Kế này mình cảm thấy chưa hài lòng lắm. 
So với những phần viết trước của series truyện này thì phần này hơi rườm rà trong cách viết. Nhiều chi tiết, cách miêu tả cảnh của tác giả tuy có văn phong nhưng làm mình đuối với mạch truyện của nó. Có thể đây là cảm nhận mang tính chủ quan của mình nên mình không thích những đoạn miêu tả quá chi tiết ấy :)
Thêm vào đó là ít các cảnh chiến đấu của Eragon hơn phần trước :) Thay vào đó là thiên về yếu tố chính trị hơn :)
Cái kết của nhân vật phản diện cũng chưa làm mình thấy hài lòng. Mình muốn hắn phải trả cái giá thật đắt cho những gì hắn đã gây ra :)
Điểm cộng cho tác phẩm là có cốt truyện khá hay, dẫn dắt tương đối tốt. Đan xen giữa tình cảm và chiến tranh. Ở phần này, đoạn chia tay của Eragon và anh em bạn bè rất xúc động :)
3
303773
2014-10-03 12:52:57
--------------------------
120852
9903
Truyện viết khá logic, mô tả sự trưởng thành từ từ của Eragon. Nhưng vẫn có một chút sạn nhỏ:

1. Thời gian kháng chiến quá ngắn. Căn cứ theo sự miêu tả của Paolini thì quân triều đình thuộc loại "quá đông và hung hãn", nhưng quá trình chiến đấu chiếm thành giết lính của quân cách mạng quá nhanh. Cứ cho là tinh thần chiến đấu cao, sự hỗ trợ của Eragon và thần tiên đi nữa thì vẫn khá vô lý. Bắt đầu từ phần 2 (có thể coi là lúc quân Varden lộ diện), Katrina có thai trong khoảng nửa cuối tập 1, cho đến lúc chiến tranh kết thúc (tức là tập 2 phần 4), diễn ra trong vòng chưa đầy 7 hoặc 8 tháng, vì chiến tranh xong 1 2 tháng Katrina mới sinh. =.=" Tôi thật sự chẳng hiểu đây là chiến tranh kiểu gì. Paolini xây dựng một quá trình phát triển quân đội, xây dựng liên minh đầy khó khăn nhưng lại đạt được kết quả nhanh đến bất ngờ. Kiểu đánh nhau thế này thì chuyện Varden lo sợ quân triều đình là dư thừa, chỉ cần tập trung lo đối phó Galbatorix thì đúng hơn.

2. Cái chết của Galbatorix chết quá đơn giản, nói trắng ra là sơ sài. Một kẻ tội ác tày trời, giết sạch loài rồng, hủy diệt tập đoàn kỵ sĩ, tàn phá Alageasia và góp phần xóa sạch nền văn minh nhân loại mà chỉ bị trừng phạt là chết vì...nhức đầu banh xác. =.=" Một sự khoan thứ không xứng đáng.

Cái tôi không hài lòng nhất đối với truyện xuất bản này, chính là phần dịch, vì nó mà tôi chỉ đánh giá 3 sao cho phần này. Không biết là vì làm vội để kịp xuất bản hay người dịch không hề xem trọng bản dịch của mình, mà dịch phần này rất tệ. Không kể đến sự khiên cưỡng gây cảm giác cứng ngắc khi đọc, chỉ nói riêng về lối xưng hô trong truyện.

Một Eragon sắp 18 tuổi, phần 3 vẫn còn gọi là "nó", ấy thế mà qua phần 4 đã thành "chàng", trong khi khoảng cách thời gian giữa 2 phần tính bằng...ngày. Đã thế, cách xưng này không đều, lúc "nó" đấy, vậy mà qua vài chữ thì lại sang "chàng" ngay. Tôi là người khó tính, đụng phải vết sạn này là đã mất hết hứng đọc rồi, tôi rất xét nét tiểu tiết. Trường hợp của Arya cũng tương tự, lúc "nàng" lúc "cô". Thêm vào đó, xưng hô trong hội thoại cũng lộn xộn, gây cảm giác rất khó chịu. Các phần trước dịch mượt bao nhiêu thì phần cuối này lại khiên cưỡng bấy nhiêu.

Điều hài lòng duy nhất về truyện xuất bản là bìa được và giấy tốt.
3
113966
2014-08-15 15:12:25
--------------------------
118219
9903
Vậy là đã 6 năm kể từ khi tôi đọc quyển Eragon đầu tiên.Cuộc hành trình của Eragon đã đến đích cùng tất cả bạn đọc.Nhưng có vẻ như phần cuối là phần mà tác giả hụt hơi sau từng ấy thời gian.Tôi đã rất kì vọng vào 1 cuộc chiến khủng khiếp giữa Eragon và Galbatorix.Nhưng nhân vật ác mà tôi mong đợi nhất lại chết quá dễ dàng và khó hiểu.Nhiều đoạn miêu tả cây rừng quá rườm rà cần cắt bớt.Có vẻ tập này thiên về chính trị nhiều hơn là các cảnh đánh nhau hồi hộp gay cấn,điển hình là cuộc họp để bầu ra vị vua mới sau khi Galbatorix bị giết.Cái kết thật buồn khi Eragon và Saphira phải xa những anh em,họ hàng,chiến hữu thân thiết để đến một phương trời xa.Thôi thì bữa tiệc nào cũng phải tàn,cuộc phiêu lưu nào cũng phải kết thúc.Cảm ơn Tác giả đã mang đến cho bạn đọc 1 bộ truyện tuyệt vời!!!
5
363312
2014-07-26 22:42:45
--------------------------
117545
9903
đây là tiểu thuyết viễn tưởng đầu tiên mà mình đọc từ nhỏ tới giờ. và có lẽ là hay nhất. tuy nhiên cái kết của Christopher Paolini có vẻ đã quá sợ việc mình sẽ viết dài dòng hay sao mà lại viết ra một kết thúc quá cụt. mình nghĩ nếu khai triển các tình tiết ra cho đầy đủ và sâu sắc thì có thể làm thêm 1 bộ nữa chứ không phải một tấp nữa đâu. làm mình đọc khá hụt hẫng khi Galbatorix bị giết quá nhanh và quá đơn giản. và có một số chi tiết không được nói đến trong truyện như làm sao Eragon biết được " Tên của các tên" và Murtagh giấu các eldunari ở đâu. Ứớc gì Christopher Paolini đã viết bộ cuối này một cách trọn vẹn hơn thì hay biết mấy. tuy nhiên khi đọc xong bộ truyện thì thấy như trong mình bị mất một thứ gì đấy mà bao lâu nay đi theo mình, lòng không muốn dứt ra. tình ra mình đọc hết bộ truyện này trong hơn 5 năm nên nó gắn bó với mình quá chặt rồi. dứt ra thì quyến luyến bứt rứt là phải. tuy nhiên đây có lẽ là tiều thuyết một trong những tiểu thuyết hay nhất thế giới
5
117973
2014-07-19 22:25:51
--------------------------
117287
9903
Sau khi đọc xong Inheritance,mình phải mất mấy phút để có thể tiếp nhận 1 phần những gì sách mang lại..quá nhiều bài học,tư tưởng,hành động đẹp và cao quý:Eragon dùng thần chú không lời cho Galbatorix thấy tất cả những gì mình làm,hy sinh tình yêu vì sứ mệnh của mình,Roran can trường dù yếu hơn nhiều vẫn cố giết Barst vì công lý,Murtagh vì Nasuada mà dần thay đổi tên thật mình..Đó là những bài học quý mà mình thấy ý nghĩa,riêng arya và eragon tuy không đến được với nhau vì nhiều lý do nhưng theo mình việc họ nói với nhau tên thật còn cao hơn cả 1 lời hẹn ước tuy mình cũng rất bất buồn,đây là 1 kết thúc hoàn hảo cho tất cả nhưng chưa đẹp cho những gì Eragon mang lại cho mọi người...
5
381503
2014-07-17 10:37:29
--------------------------
110697
9903
với những ai đã theo dõi Eragon ngay từ những tập đầu, đây là cái kết như mong đợi nhưng quá nhanh chóng. Có vẻ như tác giả đã đuối hơi và không có cách viết lôi cuốn như trước.
Đọc phần 1, 2 vô cùng hấp dẫn với các tình tiết mới lạ, có những đoạn đau buồn, có những đoạn vui tươi, gợi nhiều cảm xúc cho người đọc.
Tuy nhiên, phần cuối dường như chỉ gắng gượng kể nốt câu chuyện, một câu chuyện buồn.
Tôi hài lòng với cách cư xử của Eragon khi cậu ấy phân phát trứng rừng cho Kull, người lùn. Đó là sự tôn trọng kính nể đáng có. Tuy nhiên, việc phải lì xa người tgana yêu thật đau lòng!
2
98966
2014-04-20 08:19:29
--------------------------
101001
9903
Về tập cuối, có nhiều tình tiết lạ và khá bất ngờ: cách mà Eragon giết Galbatorix: một câu thần chú về thế giới, về cuộc sống con người mà lão gây ra; Eldunarí của con rồng già nhất trong số các tim-của-tim mà Eragon mang theo chỉ cất lời dạy dỗ chàng đúng 1 lần;... Nhưng có vẻ chỉ thế thôi. Những chương cuối hơi dài dòng và "lạc". Đặc biệt, có một chi tiết tôi tìm mãi không thấy: Eragon biết Từ-của-các-từ khi nào? Và có một vài bí ẩn chưa được giải đáp trong tập cuối, như bà lang phù thủy Angela có xuất thân như thế nào? Suy cho cùng, tập cuối đã thực hiện khá thành công nhiệm vụ khép lại câu chuyện dù có hơi gượng ép của tác giả và phần kết không mang nhiều mong muốn.
4
48268
2013-11-30 18:08:36
--------------------------
100978
9903
Có lẽ những ai đã theo chân cậu bé nông dân Eragon đến kị sĩ rồng Khắc-tinh-của-Tà-Thần, thì người anh hùng Eragon, rồng Saphira, nàng tiên Arya, công nương Nasuada, ... đã không còn xa lạ. Cuộc chiến đã gần với kết thúc. Nhưng rõ ràng kết chuyện hoàn toàn không thoả mãn lòng người đọc. Màu sắc thần thoại viễn tưởng đã bị hoà với màu sắc chiến tranh sử thi cổ đại, chỉ có máu và chết chóc. Tình người vẫn còn nhưng rõ ràng khuynh hướng tự nhiên vẫn đậm hơn. Mình không đánh giá cao phần kết của tiểu thuyết này, nhưng có lẽ cũng nên đón nhận cái kết tác giả đưa tới chúng ta, khép lại những trang viết của cây bút trẻ Christopher Paolini.
3
113650
2013-11-30 16:20:40
--------------------------
95574
9903
Một tác phẩm thực sự rất hay,tuy không sánh nỗi với Harry Poter  nhưng cũng xứng đáng để mình theo đuổi đén cùng.Mặc dù cuối cung Eragon cũng có thể tiêu diệt được lão Galbatorix nhưng dường như lão ta chết quá dễ dàng,chưa thật sự gay cấn và hồi hộp như các phần trước.Và cả  sự bí ẩn của nhân vật bà thầy lang phù thủy già kia vẫn còn là một ẩn số  chưa lời giải đáp.Nhưng dẫu sao cung có một cái kết có hậu,theo cách này hay cách khác...Tóm lại,mình rất thích truyện này nhưng vẫn hy vọng có một cái kết xuất sắc hơn...
4
167069
2013-09-27 20:58:37
--------------------------
91814
9903
Tôi theo bộ truyện này từ khi tôi lớp 6, và giờ tôi lớp 15 rồi, ngót nghét gần chục năm. Tôi công nhận 1 điều là trong vòng 10 năm đổ lại thì series về cậu Kỵ Sĩ Rồng này thuộc hàng top. Ko thể vào Top 3 chắc chắn rồi, nhưng Top 5 thì có thể. Cốt truyện rất hay, tác giả đã rất thành công khi xây dựng 1 thế giới đầy màu sắc fantasy. Những phần đầu viết rất tốt (trừ 3 chương đầu tiên của quyển 1 thì tôi không thích lắm) tuy nhiên phần cuối lại có sạn.
Sạn thứ nhất, khá nhiều series dài kì mắc phải là chi tiết sau cắn chi tiết trước, nhỏ thôi nhưng sẽ làm những người đọc kỹ tính như tôi khó chịu. Khó chịu vì nó phi logic quá. Cái lỗi này Harry Potter cũng đầy rẫy trong 2 cuốn cuối cùng.
Sạn thứ 2 bự hơn xíu, là do lòng tham của tác giả. Quá nhiều sự kiện bị "ép" vào 1 tập cuối. Những sự kiện tiềm năng đầy rẫy mà nếu cậu chịu khai thác thì có lẽ sẽ được 1 tới 2 tập nữa. Tuy nhiên do bị ép, tôi cảm thấy ngộp thở khi các sự kiện cứ ùn ùn kéo tới. Rất khó để xử lí kỹ càng toàn bộ.
Vồn trong chờ 1 cái kết hoàn hảo, nhưng vì những hạt sạn đó tập cuối của chuyến hành trình ngót nghét 10 năm lại thành 1 sản phẩm lỗi vô cùng đáng tiếc. Đáng tiếc hơn nữa khi đành rằng nó lỗi hoàn toàn ta có thể làm lơ mà vứt đi, đằng này nó lại quá tuyệt đến nỗi ta không nỡ, nhưng lại khó chịu khi thưởng thức
4
33497
2013-08-22 22:25:53
--------------------------
80374
9903
Vậy là Eragon đã đến hồi kết thúc. Một cảm giác vui buồn lẫn lộn khi tôi đọc xong cuốn sách này. Vui vì một cái kết (mà theo tôi nghĩ ) là trọn vẹn, là tốt đẹp và không thể nào hoàn hảo hơn. Nhưng buồn vì bộ sách tuyệt vời này đã chia tay đọc giả. Eragon 4- Di sản thừa kế vẫn tuyệt vời như các tập sách trước. Lôi cuốn. Thu hút. Không thể phủ nhận đây là một tong những bộ sách viễn tưởng hay nhất. Tác giả đã khiến đọc giả sống cùng các chuyens phiêu lưu của nhân vật, để giờ đây phải hối tiếc khi câu chuyenj đã đi đến kết thúc
Theo tôi nghĩ, đây là tập hay nhất và cũng tuyệt vời nhất trong bộ Eragon
5
24380
2013-06-12 16:28:11
--------------------------
77844
9903
Khi cầm trên tay cuốn thứ 1 trong tập Inheritance thì lật ra bìa cuối coi giới thiệu về Eragon và cô rồng Saphira và mình ngạc nhiên là tới đoạn Hai anh em kị sĩ rông . Lúc ấy cứ tưởng cả 2 anh em Eragon và Roran đều là kị sĩ rông làm mình muốn đọc nhanh hơn ( lúc ấy mình mới đọc đến Eldest tập 1 ) . Từng diễn biến tình tiết hấp dẫn và đầy lôi cuốn xuyên suốt 4 phần nhưng cũng có phần làm mình cảm thấy buồn là tình cảm của Eragon và Arya . Sự chênh lệch về tuổi tác là rào cản lớn giờ đây nàng Arya giờ đây lại kế ngôi nữ hoàng Islanzadí như lại thêm một lớp rào ngăn cản tình yêu cả hai . Cuộc chia li có thể là buồn với nhiều người nhưng cuộc phiêu lưu sẽ tiếp tục trong thời gian không xa     
5
123123
2013-05-29 22:15:29
--------------------------
76702
9903
Lần đầu tiên tôi đọc Eragon là qua sự giới thiệu của một người bạn.Câu chuyện về một kỵ sĩ xuất thân từ một cậu bé nông dân nghèo và con rồng của anh ấy đã thật sự cuốn hút tôi ngay từ tập đâu.Tuy trong quá trình theo dõi có một vài chi tiết làm tôi cảm thấy hơi "buồn" vì nó khá dài dòng nhưng cái kết cuối cùng của bộ truyện đã làm tôi hài lòng,tuy chuyện tình cảm giữa Eragon và Arya không mỹ mãn như tôi mong đợi.Phải nói đây là một bộ truyện tuyệt vời !
5
89851
2013-05-24 09:22:44
--------------------------
76699
9903
Lần đầu tiên tôi đọc Eragon là qua sự giới thiệu của một người bạn.Câu chuyện về một kỵ sĩ xuất thân từ một cậu bé nông dân nghèo và con rồng của anh ấy đã thật sự cuốn hút tôi ngay từ tập đâu.Tuy trong quá trình theo dõi có một vài chi tiết làm tôi cảm thấy hơi "buồn" vì nó khá dài dòng nhưng cái kết cuối cùng của bộ truyện đã làm tôi hài lòng,tuy chuyện tình cảm giữa Eragon và Arya không mỹ mãn như tôi mong đợi.Phải nói đây là một bộ truyện tuyệt vời !
5
89851
2013-05-24 09:21:58
--------------------------
72759
9903
Là một người đã theo dõi câu chuyện của cậu bé Eragon và cô nàng rồng Saphira từ những chặng đường đầu tiên, tôi thấy tác giả Paolini đã viết ra đoạn kết khá hay, ấn tượng nhưng không hoàn hảo. Tôi thấy đoạn Eragon dùng "thần chú không lời" để Galbatorix thấu hiểu những gì hắn đã gây ra cho tất cả mọi người và tan biến luôn từ đó, không hay cho lắm! Sao không cho Galbatorix được tiếp tục tồn tại trong vai trò 1 vị bạo chúa đã được cải tạo và trở nên lương thiện (theo kiểu kết thúc có hậu nhất có thể). Một chút ý kiến vậy thôi, chứ tôi vẫn tôn trọng ý tưởng của tác giả.
Cảm giác cầm trên tay cuốn sách mà mình trông đợi mấy năm trời quả thực khó mà diễn tả bằng lời. Đọc, đọc và chỉ biết đọc....ăn rồi đọc, đọc liên tù tì 2 ngày (tôi đọc sách hơi chậm) cũng xong. Phù, thoả mãn, cười thú vị, cứ như là mình vừa trải qua những gì như trong đó viết. Nhiều người cứ đem so sánh những tác phẩm nổi tiếng với nhau làm gì nhỉ? Tôi thấy mỗi tác phẩm có cái hay riêng, có sự đặc sắc và cách lôi cuốn độc giả khác nhau, hãy để những điều đó diễn ra tự nhiên, so sánh làm chi cho mệt!
Cảm ơn chân thành đến tác giả đã đem nhiều người đến với sách nói riêng và văn hoá đọc nói chung. Không có cuộc vui nào là mãi mãi, không có buổi tiệc nào mà không tàn, câu chuyện về 2 nhân vật anh hùng Eragon và cô rồng Saphira đã khép lại với những sự nuối tiếc, nhưng lúc này đây, hãy để trí tưởng tượng của chúng ta phát huy nhé, hãy viết lên trong suy nghĩ 1 cuộc phiêu lưu khác "made by me", hãy đem lại sự bay bổng cho thế giới đầy phép màu ấy!
5
97051
2013-05-04 16:40:31
--------------------------
71159
9903
Đọc bộ truyện Eragon từ những tập đầu tiên và cho đến hôm nay cuối cùng đã được đọc đến những trang cuối kết thúc bộ truyện dày công, nổi tiếng trên khắp thế giới này. Và quá thực đã đọng lại trong tôi vô vàn những cảm xúc. Hình ảnh chàng kỵ sĩ Eragon và nàng rồng Saphira đã in đậm trong trái tim tôi cũng như nhiều bạn đọc khác, vừa thân thương vừa gần gũi. Nay phải nói lời tạm biệt - không còn được đồng hành cùng Eragon và Saphira trong những cuộc phiêu lưu đầy hồi hộp và gay cấn nữa - trong lòng không khỏi cảm thấy nhiều nuối tiếc... Nhưng như tác giả Christopher Paolini đã nói, chính anh cũng cảm thấy không nỡ rời xa cái thế giới với những nhân vật anh đã tạo dựng trong bộ sách. Và một ngày nào đó anh sẽ trở lại với tất những gì mà anh đã gắn bó trong suốt nhiều năm trời. Chúng ta sẽ cùng hy vọng tác giả sẽ đem đến một câu chuyện hay và hấp dẫn nữa trong tương lai không xa.

Về tập cuối này không có quá nhiều điều cần bàn về nội dung, vì vốn dĩ nó luôn đầy đủ kịch tích, sự lí thú đầy cuốn hút như bộ truyện Eragon vốn vậy. Kết thúc êm đẹp cho tất cả. Mỗi nhân vật đều tìm được cho mình vị trí thích hợp để gây dựng và phát triển lại đế quốc Alagaesia. Việc ra đi của Eragon là hoàn toàn hợp lý cho một kết thúc tuy mang nhiều đau khổ về sự chia ly nhưng cũng mở ra một chân trời mới sáng rọi hơn. Phần cuối truyện chất nặng nỗi buồn nhưng lại chan chứa nhiều ân tình hơn bao giờ. Đoạn kết trong sự biệt ly đầy lưu luyến của Eragon và Arya đã thể hiện một tình yêu lớn lao, sâu sắc và tràn đầy ý nghĩa.

Một lời cuối xin nói lời tạm biệt với Eragon - Câu Bé Cưỡi Rồng - bộ truyện đã song hành cũng tôi trong suốt quãng thời niên thiếu. Cảm ơn vì đã mang đến cho tôi thật nhiều những rung động từ con tim.
5
6502
2013-04-25 13:42:44
--------------------------
66818
9903
Sau vài năm không thấy thong tin gì về những tập cuối của Eragon, tôi thực sự thất vọng khi một bộ truyện hay không được hoàn tất. Nhưng rất may là 2 tập cuối vẫn được xuất bản. 
Nhiều người vẫn nhận xét là kết thúc không có hậu nhưng với tôi, đây là cách kết thúc vượt ra ngoài dự đoán của tất cả mọi người, kể cả tôi và đó cũng là cách kết thúc hợp lí. Sự chênh lệch về tuổi tác giữa Arya và Eragon có thể là một lí do nhưng lớn hơn tất cả, trách nhiệm và nhiệm vụ nặng nề của cả mới là nguyên nhân chính khiến họ chọn con đường khác nhau. Với Arya là nhiệm vụ của một nữ hoàng, có nghĩa vụ phải bảo vệ, quản lí và dẫn dẵn đất nước. Tương tự là Eragon- kỵ sĩ còn tự do cuối cùng còn sót lại có nhiệm vụ xây dựng là lực lượng kỵ sĩ. Đó là những trách nhiệm nặng nề và không cho phép họ vì tình cảm riêng tư mà chối bỏ, cho dù tôi cũng nghĩ rằng nếu có cơ hội, Arya cũng muốn được ở cùng Eragon.
Nếu mỗi người vì cái chung, vì đại cục như Eragon và Arya thì thế giới sẽ tốt đẹp hơn nhiều.

5
61089
2013-04-02 14:13:22
--------------------------
64697
9903
cuộc chiến, đau đớn có, mất mát có, vui có, buồn có.. Thiết nghĩ với 1 người phải chịu đựng quá nhiều gian truân như Eragon thì anh phải là người xứng đáng được hưởng một niềm hạnh phúc bất tận khi cuộc chiến kết thúc.  Nhưng thật bất ngờ làm sao khi mà một tình yêu mới chớm nở đẹp tựa như huyền thoại của anh lại tan đi như mây khói. Tôi cứ ngỡ điều đó sẽ làm cho Eragon đau khổ khôn cùng nhưng điều bất ngờ lại nằm ở sự quyết định của cả hai.....
Và điều gì sẽ xảy ra thì không một ai biết được. Sự tuyệt vời ấy đang nằm trong cuốn sách cuối cùng . Sự mở đầu mới của cuộc đời Eragon bắt đầu từ cái kết không tưởng!
5
7589
2013-03-21 13:18:16
--------------------------
54992
9903
Sau bao gian khổ, hiểm nguy, Eragon cùng những người đồng đội, người bạn của mình tưởng chừng như không thể đánh bại Galbatorix vì lão quá mạnh. Nhưng, chỉ qua một chi tiết nhỏ, so với cả tác phẩm lớn và đồ sộ, Galbatorix đã bị tiêu diệt. Phần cuối này được tác giả đẩy cao những kịch tính, những tình tiết dồn dập làm cho tôi cảm thấy không thích lắm, không như văn phong nhẹ nhàng như những phần trước. Cái kết buồn của Eragon, đi đến một phương trời vô định, dù là cái kết mở nhưng quá ư đau buồn. Arya không thể đến bên Eragon cũng có thể là do chênh lệch về tuổi tác. Đó là một số điểm trừ nho nhỏ nhưng nhìn tổng quát Inheritance Cycle là một bộ truyện tâm huyết của C.Paolini, một bộ truyện rất đáng đọc. Nó có thể sánh ngang với tác phẩm giả tưởng nổi tiếng Harry Potter của J.K.Rowling.
4
66525
2013-01-11 18:01:51
--------------------------
52345
9903
Inheritance, hay Di sản kế thừa là phần cuối cùng trong bộ Inheritance Cycle của tác giả Christopher Paolini. Quả là một hành trình dài, từ khi cuốn sách đầu tiên được cậu bé Christopher viết dựa vào trí tưởng tượng của bản thân mình được xuất bản khi mới 15 tuổi. 4 cuốn sách, trong đó mỗi cuốn là một chặng đường trưởng thành của cậu bé Eragon từ vùng quê nghèo khó ở thung lũng Palancar để trở thành Kị sĩ rồng đánh bại kẻ bá vương độc đoán chốn xa hoa Galbatorix. Không có gì phải phàn nàn về mạch chuyện cùng những mô tả rất chi tiết của Paolini trong mỗi trận đánh, nhưng thực sự mà nói sự hấp dẫn của câu chuyện là ít hơn mong đợi của nhiều fan khi những mâu thuẫn và khó khăn trong chuyện lại được giải quyết tương đối chóng vánh. Kết thúc chuyện cũng sẽ đem lại rất nhiều nuối tiếc cho bạn đọc, tuy nhiên lại mở ra cơ hội cho những cuốn sách tiếp theo của Paolini ở xứ sở Alagaesia kì vĩ.
3
65273
2012-12-26 09:59:18
--------------------------
46501
9903
Mới đây mà đã 5 - 6 năm kể từ khi bắt đầu xuất hiện một câu chuyện về cậu bé vùng nông thôn - Eragon, có một cuộc đời đầy gian truân đã trở thành vị anh hùng luôn có lòng trắc ẩn cùng với con rồng ương ngạnh nhưng đậm sâu tình cảm với chủ, Saphira, con cái duy nhất còn lại của jống nòi, Eragon và Saphira đã cùng nhau vượt qua nhiều nỗi đau đớn từ việc mất đi gia đình, người thân, bạn bè, phát hiện ra những điều bí mật từ lâu được chôn giấu và khó khăn lớn nhất của 2 tâm hồn thuần khiết này chính là việc dẫn dắt toàn bộ loài người, người lùn và thần tiên chống lại tên ác vương độc tài Gabatorix. Trong cuộc phiêu lưu đó, một mối tình giữa con người và thần tiên cũng được bén lửa, Eragon và Arya đã tượng trưng cho sự tự do, trong sáng trong tình yêu bất kể nòi giống và thành kiến, dị nghị xung quanh. Bộ truyện này đã  mang lại cho tôi không biết bao nhiêu là cảm xúc, tôi có thể khẳng định rằng nó không thua kém, thậm chí còn hay hơn cả Harry Porter. Tôi rất tiếc vì hành trình của Eragon cũng đã đến hồi kết thúc và rất buồn vì phải chia tay với cái thế giới con người, thần tiên, người lùn, ma mèo, v.v... cùng chung sống, mà tôi đã hòa mình vào trong suốt những năm qua.
Tạm biệt một trong những tuyệt tác trong trí tưởng tượng của anh, Christopher Paolini
5
39721
2012-11-15 23:18:56
--------------------------
45127
9903
Sau bao lâu chờ đợi tập sách thứ hai của phần truyện cuối Inheritance từ những ngày đầu theo dõi truyện, tập cuối này giải đáp khá nhiều khúc mắc, cũng như tô đậm thêm vẻ đẹp huyền ảo của thế giới người và rồng của tác giả Christopher Paolini.
Khác với những tập trước đây, nhịp điệu, tiết tấu truyện diễn biến có phần nhanh hơn, không còn nhiều những đoạn đi sâu khắc họa thế giới Alagaesia, nhưng vẫn có những cung đoạn bộc lộ cảm xúc các tuyến nhân vật, góp phần xây dựng nội tâm nhân vật.
Tuy nhiên, có lẽ niềm kỳ vọng của mình quá lớn hay chăng nên ở một số cao trào, truyện vẫn chưa cho mình cảm giác đã. Mình vẫn nghĩ truyện cần có một điểm nhấn nào đó, tăng phần kịch tính cho câu truyện.
Nhìn chung đây vẫn là câu chuyện hay, tương đối tốt nếu nhìn nhận đây là một tác phẩm của một anh chàng chưa đầy tuổi băm. Phải nói là khâm phục anh chàng này vì năng lực viết lách thật dữ dội! Hy vọng sau này anh ấy sẽ có những tác phẩm hay không kém gì Eragon - Cậu bé cưỡi rồng!
4
41589
2012-11-06 11:42:12
--------------------------
44780
9903
viết theo chủ nghĩa anh hùng cá nhân , tác phẩm này đã mang đến cho mình một hình ảnh của vị anh hùng xuất thiếu niên với những điều ngẫu nhiên và đầy lý thú. mình nghĩ eragon và roran, 2 anh em, 2 tính cách nhưng đều có một nghị lực phi thường, có khao khát được bảo vệ quê hương, được bảo vệ những người mình thương yêu. eragon có sự hỗ trợ của những sức mạnh siêu nhiên, còn roran chỉ là một người thường, thế nhưng với ý chí và nỗ lựccủa mình, roản đã trở thành cây búa dũng mãnh... và mình khâm phục cả 2 người họ. mình yêu từng câu chữ trong tác phẩm và hồi hộp theo dõi tững diễn biến của câu chuyện. kết thúc rồi, một kết thúc mà không phải ai cũng hài lòng nhưng đó là cách mà Paolini đem đễn cho chúng ta. Ngay từ khi bắt đầu, chẳng phải cũng rất bất ngờ, thf khi kết thúc cũng sẽ rất đột ngột
5
67900
2012-11-04 09:22:44
--------------------------
39011
9903
Một tiểu thuyết mang tính tưởng tượng mà vẫn logic . Rất cuốn hút và hấp dẫn . Thật khâm phục tác giả , dù trẻ tuổi nhưng quyển sách không hề nhàm chán mà còn đầy thú vị , đáng để theo dõi  .Cốt  truyện hay nhưng hơi nhanh , mang tính phiêu lưu mạo hiểm  , và còn chút giả tưởng nữa . Qua từng tập sách ,một thế giới mới đầy bí ẩn mở ra .  những con rồng mạnh mẽ và quyền năng kì lạ cùng một kị sĩ ở Alagaesia , những cuộc chiến cam go , khốc liệt diễn ra liên tục , không để cho vùng đất này chút yên bình ...
 Cuộc chiến với  GALBATORIX  là cuộc chiến sống chết , được ăn cả , ngã về không . Thật hồi hộp để đón đọc bộ truyện này . Lối hành văn súc tích , ngắn gọn , dễ hiểu . Từng tình tiết đều được  ngòi bút của Christopher Paolini  trau chuốt kĩ càng .
5
46599
2012-09-07 19:04:42
--------------------------
38909
9903
Tập cuối cùng của Eragon được tôi mong ngóng từ rất lâu rồi .Những bí mật được hé lộ ở những tập trước quả thực đã gây cho bản thân tôi rất nhiều tò mò ,nào là tảng đá Kuthian hay Hầm mộ linh hồn .Cái hay của tác giả có lẽ chính là sự kiên kết chặt chẽ của các phần và đã thành xông rực rỡ .Có lẽ điều đáng tiếc nhất chắc chính là tình yêu của Eragon không được đáp lại .Arya phải chăng là không thể từ bỏ tất cả để cùng đi với Eragon được sao .Tuy nhiên phần kết cũng là khá ổn ,Paodini đã thành công trong việc để lại trong lòng độc giả 1 chút mơ mộng .Trận chiến với Gabatorix khá hấp dẫn nhưng không làm rõ từng hành động và chi tiết,có lẽ đây là điểm trừ duy nhất của cuốn truyện này .
4
33583
2012-09-06 19:48:46
--------------------------
38674
9903
Kì vọng càng cao thì thất vọng càng nhiều, 3 phần đầu quá tuyệt của Eragon đã đi sâu vào lòng độc giả: từ 1 chàng nông dân nghèo khó, 1 quả trứng rồng cho đến 1 kỵ sĩ trưởng thành với thanh kiếm Brisingr. Người đọc mong chờ một phần kết hoành tráng hé lộ những bí mật của game, 1 cuộc đối đầu nảy lửa giữa Eragon và tên độc tài Galbatorix, vậy mà cuộc đối đầu này không máu lửa như mình nghĩ. Duy có điểm cộng là kết cục game khá là hợp lý và hứa hẹn sẽ có đất cho nhà văn viết tiếp (nếu muốn viết vì Eragon quá thành công) những phần tiếp bước Eragon sau này.
3
52301
2012-09-04 14:41:47
--------------------------
38441
9903
Thực ra sau khi đọc tác phẩm này mình thất cái kết của tác giả rất sâu sắc và bất ngờ. Bởi ngay từ tập 1 ma mèo đã nói đến những bí mật về "cửa hầm, thanh kiếm, cái tên" thế nhưng dần dần từng chút một đến tận phần 4 thì tất cả mới sáng tỏ. Thêm nữa khoảng cách của Galbatorix với Eragon là quá xa nhưng không phải nhờ may mắn, hay sự thiếu cẩn thận của lão mà Eragon dành chiến thắng mà là nhờ trí tuệ thâm sâu của Eragon cùng những đau khổ mà cậu phải chịu. Ngoài ra tình hữu nghị giữa các dân tộc ở cuói truyện cũng rất đáng tán thưởng: Thay vì quy củ như cũ, chỉ có tộc Elf và người Human được hưởng quyền trở thành kị sĩ rồng mà ngay cả chủng tộ Dwaft, Urgal cũng được phép. Làm như vậy hoàn toàn tránh được khả năng Galbatorix thứ 2 xuất hiện vì khi các chủng tộc đều đoàn kết như vậy, giới kị sĩ rồng chỉ cần xuất hiện một phản đồ lập tức bị một đoàn quân khổng lồ hùng mạnh quét sạch. Ngoài ra việc để Arya và Eragon càng ngày càng giống nhau (Đều là tiên, có rồng, cùng giết chết tà thần...) thêm việc cho Eragon bay về phương Bắc (gần mảnh đất của thần tiên nơi Arya làm nữ hoàng) để xây dựng vương quốc kị sĩ rồng mới cũng là kết thúc mở của tác giả gợi ý cho việc về sau có thể Arya và Eragon sẽ đến với nhau.
5
48063
2012-09-02 09:28:24
--------------------------
38387
9903
Mới ngày nào ERAGON - Khắc Tinh của Tà Thần - chỉ là một cậu bé nông dân nghèo khổ và SAPHIRA - con rồng của cậu - chẳng gì khác hơn là một hòn đá xanh dương trong rừng. Vậy mà giờ đây, số phận của cả dân tộc đặt lên vai cả hai. Những tháng ngày được huấn luyện, những trận chiến chinh đã đem về vinh quang và hy vọng, nhưng ERAGON và SAPHIRA cũng phải gánh chịu những mất mát đau lòng. Tuy nhiên trận chiến thật sự vẫn còn phía trước: trận đối đầu với GALBATORIX. Hai anh em kỵ sĩ và rồng phải đủ mạnh để đánh bại lão. Nếu không thì sẽ không ai có thể làm được điều đó nữa. Vì sẽ không còn cơ hội thứ hai.
4
41190
2012-09-01 14:55:10
--------------------------
38095
9903
Thế là đã 9 năm trôi qua kể từ khi cầm trên tay tập 1 của bộ tiểu thuyết này. Quãng thời gian dài đằng đẳng nhưng tôi vẩn tiếp tục theo dõi...

Một câu chuyện phiêu lưu giả tưởng cực hay, một thế giới huyền bí với những con rồng khổng lồ, chàng nông dân ngày nào giờ đã trở thành một kị sĩ mạnh mẽ, nắm giữ trong tay trọng trách mang lại hòa bình cho vùng đất Alagaesia...

Từng câu chuyện được tiếp diễn tài tình, những xúc cảm của chàng thanh niên trẻ tuổi, những đau khổ, mất mát do chiến tranh, những cái chết đầy thương tâm, những âm mưu đen tối. Tất cả đều được miêu tả một cách thuần thục nhất qua ngòi bút của một tác giả chỉ mới 15 tuổi ( khi viết eragon tập 1)...

Và giờ đây, trận chiến cuối cùng với đức vua Galbatorix, những bí mật đen tối nhất sẽ được phơi bày, ai sẽ là người chiến thắng ???trứng rồng cuối cùng sẽ nở ra cho ai ???

...Một trong những quyến sách hay nhất mà tôi đã từng đọc
5
5566
2012-08-29 22:55:20
--------------------------
37792
9903
Thật sự mong đợi quyển truey65n này, 1 trong những quyển mà mình thấy hoàn toàn sánh ngang với Harry Porter, cách xây dựng nhân vật, tình tiết khiến người đọc hoàn toàn bất ngờ và ko  đoán trước được kết quả. Tập này sẽ là kết thúc cho chuỗi truyện mà mình đã theo dõi trong suốt nhiều năm qua, thật sự mong 1 kết cục có hậu và đẹp đẽ để kết thúc cho Eragon mà mình từng hâm mộ.
5
52301
2012-08-27 13:15:49
--------------------------
37320
9903
Mình đã theo dõi bộ truyện này lâu rồi, từng quyển một, từ Eragon đến Eldest, Brisingr, cuối cùng là Inheritance. Phải công nhận những ba quyển đầu rất hay, rất hấp dẫn làm cho mình phải say mê, trông ngóng liên tục. 
Nhưng ở phần cuối Inheritance làm cho mình không hài lòng lắm. Bởi nhịp điệu câu chuyện vội vã quá, sức hấp dẫn cũng kém đi, kết thúc lại thiếu hậu... 
Nhưng những ai đã theo dõi bộ truyện này ngay từ đầu rồi thì hãy cứ tiếp tục xem cho trọn vẹn, vì ít nhất mình cũng nên hoàn thành những gì mình đã bắt đầu. Dù sao đi nữa thì Christopher cũng đã tạo cho câu chuyện này một kết cục, dù không mỹ mãn như mong đợi.
4
9411
2012-08-21 16:21:35
--------------------------
