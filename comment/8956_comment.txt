473240
8956
Cuốn sách này như là một chén soup gà cho tâm hồn, bằng những câu chuyện cảm động và nghị lực về các số phận con người khác nhau, cuốn sách như tiếp thêm ý chí và khát vọng trong tâm hồn cho mỗi con người. Đây là 1 series sách đáng để đọc. Sách được viết bằng lối song ngữ, thích hợp để trau dồi vốn tiếng anh. Tuy nhiên có một vấn đề đó là mình đặt 2 cuốn trên tiki, nhưng tiki hình như không quan tâm đến chất lượng sách, sách của mình bị hết keo bung bìa hết cả. Mong tiki cải thiện thêm.
4
381211
2016-07-10 13:17:52
--------------------------
313529
8956
Mình đặc biệt rất thích bộ sách hạt giống tâm hồn trên Tiki vì không những giá rẻ hơn khi mua ngoài hiệu sách mà chất lượng cũng không hề thua kém chút nào. Cuốn sách đã trở nên gần gũi và quen thuộc với mỗi chúng ta. Với tôi, hạt giống tâm hồn như một người bạn hữu ích đồng hành với mình trong cuộc sống vậy. Từng mẩu truyện ngắn, mỗi câu danh ngôn đều chứa đựng những triết lí sâu xa mà dù có đọc nhiều lần vẫn thấy hay và ý nghĩa. Và ở cuốn sách này mang đến cho chúng ta niềm đồng cảm với những tâm hồn bất hạnh và những ai đang rơi vào hoàn cảnh này có thể lạc quan hơn và đứng dậy bước tiếp trước những sóng gió và kém may mắn trong cuộc đời.
5
636935
2015-09-23 22:26:36
--------------------------
174224
8956
Như mọi người đã biết,Chicken soup for the soul luôn được nhiều người đón nhận.Cả nội dung và hình thức đều tôt.Cuốn sách gây ấn tượng mạnh mẽ với tôi bởi nội dung rất thiết thực.Đây là những mẫu chuyện  có thực mà tác giả đã gom góp lại thành một quyển sách.Thông qua những mẫu chuyện ngắn này,tác giả muốn gửi gắm đến người đọc một thông điệp mang đầy triết lí nhân văn sâu sắc.Đời người không ai tránh khỏi những khó khăn,vấp ngã.Tuy nhiên,nếu chúng ta có một nghị lực phi thường,vượt qua mọi khổ đau trong cuộc đời thì chúng ta sẽ sớm ngày thành công trong cuộc sống.Với lối viết không cầu kì,hoa mỹ,chicken soup for the soul  mau chóng chiếm được cảm tình người đọc.Đây là quyển sách tuyệt vời mà mọi người cần nên đọc
4
592800
2015-03-27 16:55:20
--------------------------
138043
8956
Cuốn sách đã gây ấn tượng mạnh mẽ với mình, không chỉ về nội dung mà cách thể hiện rất mới, rất độc đáo, khiến người đọc cảm thấy hấp thích thú và hấp dẫn. Cuốn sách được viết dưới dạng song ngữ, vừa dễ hiểu vừa thuận tiện cho việc học và kiểm tra kiến thức tiếng anh. Bìa ngoài của cuốn sách rất đẹp, gây ấn tượng bởi cái tên Chicken Soup For The Soul. Hơn nữa, cuốn sách có nội dung rất thiết thực, nhẹ nhàng, dễ hiểu, kể về tình yêu thương giữa con người với con người, mang đậm tích nhân văn sâu sắc.
5
414919
2014-11-30 15:08:38
--------------------------
113415
8956
1 cuốn sách song ngữ khá hay. nội dung nói về tình yêu thương giữa con người với con người. mình thấy trên thị trường có khá nhiều loại sách kiểu này nhưng khi đọc Chicken Soup For The Soul 10 thì mình thấy nó nhẹ nhàng, đơn giản và dễ hiểu, không như 1 số sách khác hay dùng lối viết 1 cách quá văn vẻ. đối với những bạn nào muốn học tốt tiếng anh thì mình nghĩ là không nên bỏ qua cuốn sách này, vừa học được kiến thức, lại vừa học được về cuộc sống.
4
77978
2014-05-30 11:03:53
--------------------------
