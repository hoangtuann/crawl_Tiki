403534
5519
Mình mua chung với cuốn " Cẩm nang làm mẹ tuyệt vời", 1 quyển cho mình và 1 quyển tặng cho chồng, để hai vợ chồng cùng nhau chuẩn bị cho bé yêu. Thấy quyển sách này có vài mục khá giống với quyển " cẩm nang làm mẹ tuyệt vời" tuy nhiên mình nghĩ tác giả để vậy là đúng, để cho các ông bố biết chuyện chăm sóc con, dỗ dành và vui chơi với con không phải là chuyện của riêng các bà mẹ. Sách có khá nhiều hình minh họa màu, sắp xếp theo thứ tự rất khoa học, nên đọc dễ hiểu. Rất cần cho các ông bố trẻ Việt Nam, để các ông hiểu và chia sẻ cho các bà mẹ.
4
576276
2016-03-23 22:10:55
--------------------------
397860
5519
Cuốn sách với những thông tin hữu ích pha chút hài hước dưới các hình ảnh minh họa này sẽ mang đến một hình ảnh mới cho các ông bố hiên tại và tương lai. Đó là nếu như các anh chịu đọc những cuốn sách như thế này và có lòng muốn trở thành một người đàn ông văn minh trong mắt vợ và một siêu nhân trong mắt các con. Mong sao những người vợ Việt Nam sẽ có những ông chồng thích làm những hoạt động được hướng dẫn trong cuốn sách này để hành trình làm mẹ dễ chịu và hạnh phúc hơn trong tình yêu thương. 
5
143507
2016-03-15 15:29:10
--------------------------
230401
5519
Quyển này mình mua kèm với quyển Cẩm nang làm mẹ tuyệt với để tặng cho ông xã mình, quyển sách trình bày màu sắc rất bắt mắt và hình vẽ khá đẹp rất thích hợp cho những người lần đầu làm bố. Nội dung sách vừa đủ chứ không đi sâu và không nhiều như những quyển sách chữ nhưng đọc nó bạn cảm thấy nhẹ nhàng và không áp lực lắm, nó giúp bạn làm quen với một cuộc sống mới theo hướng nhìn dễ thương hơn. Phía cuối của quyển sách có hướng dẫn những ông bố cách tự chơi và làm đồ chơi cho con rất đáng để học hỏi và làm theo đó!
5
351847
2015-07-17 14:31:17
--------------------------
