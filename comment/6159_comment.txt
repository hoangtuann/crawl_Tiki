556957
6159
Mình đã đọc cuốn này đầu tiên khi mở hàng ra, tên tựa ấn tượng, văn phong đậm chất riêng. Mỗi chương 1 màu, mỗi kiểu. May rất thẳng thắn về chuyện "sex" mà chả ai dám nói về chuyện nhạy cảm mà chả ai dám thổ lộ. Đồng thời, cuốn sách còn giải đáp thắc mắc về các phái yếu và phái mạnh, họ cần gì và họ muốn gì.
5
5131186
2017-03-28 21:05:45
--------------------------
314766
6159
Mình vốn rất ít đọc truyện ngắn của các tác giả Việt Nam. Tuy nhiên khi đi nhà sách, tình cờ đọc lướt qua quyển sách này mình thấy rất thích cách viết của tác giả nên đã lên Tiki rinh về em ấy. Quả thực quyển sách không làm mình thất vọng. Mình như tìm thấy bản thân qua những bài viết đậm chất dịu dàng, mạnh mẽ của May. Thật sự ấn tượng với cách nhìn sâu sắc của May với các bài viết “Tri kỉ nơi đâu”, “Đàn ông không xấu, chỉ vì họ không yêu ta”, “Hãy cứ là đóa hồng kiêu hãnh”.
4
653344
2015-09-27 09:35:58
--------------------------
295034
6159
Văn phong của Blog của May không đơn thuần chỉ là những tản mạn của một cô gái, không thuộc về kiểu văn chương uỷ mị. Đừng chỉ hôn lên môi là một cuốn sách dành cho những ai có tâm hồn phụ nữ chính hiệu, một tâm hồn tràn đầy nữ tính và khát khao yêu thương. Đọc xong cuốn sách thấy lòng mình dịu hẳn ra, yêu thương nhiều hơn và biết đối đãi chân thành với người yêu của mình hơn. 

Những trải nghiệm của May cũng gần như chúng ta, có điều khác nhau ở cách cảm nhận, bạn sẽ bất chợt gọi tên được những cảm xúc khó tả khi đọc cuốn sách này. . 
4
53238
2015-09-09 22:22:21
--------------------------
294644
6159
Mình nghe tên tuổi của May cũng lâu rồi nhưng chưa từng đọc bài của cô!Tình cờ lớp mình có đứa bạn mua đọc và nó khen hay thế là mình cũng bê một cuốn về cho có cái gọi là phong trào chứ chẳng biết hay không nên chẳng hy vọng gì nhiều!Nhưng quả thật như nó nói đọc cảm thấy cũng rất thú vị !Sách nói về phụ nữ,mà mình cũng là con gái nên cảm thấy rất thích!Cảm thấy đây cũng là cuốn sách khá bổ ích cho chính bản thân,các bạn nữ nên đọc nhé(con trai cũng nên đọc đi để hiểu bạn gái mình hơn)!!!
5
413637
2015-09-09 15:44:19
--------------------------
259125
6159
Sách viết về phụ nữ, đưa ra những góc nhìn về đàn ông, lý giải những điều nhỏ nhỏ như tại sao phụ nữ luôn hỏi "Anh có yêu em không?", tại sao các cô gái muốn yêu nhưng sợ cưới, bàn tay đàn ông có gì đặc biệt, đàn ông 30 tuổi sẽ nghĩ gì... Tôi đọc nó để hiểu hơn những người phụ nữ xung quanh mình, trước tiên là người yêu. Thật lạ là hóa ra trong khi yêu phụ nữ cũng mãnh liệt và đầy xúc cảm như thế! MỘT cuốn sách hay, nên có trong tủ sách của mọi người đàn ông
4
473974
2015-08-09 09:21:24
--------------------------
125969
6159
Thích đọc bài của May từ những bài blog trên mạng, sách ra là phải rinh ngay về gối đầu giường. May viết về tình yêu bằng những triết lý rất dịu dàng mà lại cá tính. Kiểu gì cũng lý giải được, khiến người ta chẳng còn phán xét sai hay đúng, chỉ gật gù với cách lý giải của cô ấy thôi. Khoái mấy bài nóng bỏng trong sách, như là "Nếu "yêu", hãy "yêu" cho đàng hoàng", lúc đó thấy người phụ nữ trong May mãnh liệt thật, nhưng cũng có lúc lại dịu dàng ơi là dịu dàng như bài "Hãy để em dựa vào đời anh". Chắc phụ nữ cũng giống như May viết trong sách, phức tạp, phù phiếm nhưng mà phức tạp, phù phiếm một cách đáng yêu. À quên bộ bookmark tặng kèm thật đúng tinh thần "nóng bỏng", hehe.
5
418050
2014-09-16 14:08:24
--------------------------
125109
6159
Mua cuốn sách này đầu tiên là vì cái tiêu đề ấn tượng, sau đó đọc sách thì thấy quả thật không thất vọng. Tác giả sinh năm 88 mà viết chín chắn như phụ nữ 30, cá tính và thú vị. Vì ở ngoài bìa có đánh dấu 16+ nên tìm đọc ngay những bài có tiêu đề nóng bỏng như "Nếu "yêu", hãy "yêu" cho đàng hoàng", "Nếu đã lên giường, hãy đơn giản là yêu nhau đi"...Không biết tác giả trải nghiệm nhiều chưa, nhưng viết về sex thú vị và đúng quá. Viết về phụ nữ, về đàn ông, lý giải những điều nhỏ nhỏ như: tại sao phụ nữ luôn hỏi "Anh có yêu em không?", tại sao các cô gái muốn yêu nhưng sợ cưới, bàn tay đàn ông có gì đặc biệt, đàn ông 30 tuổi sẽ nghĩ gì...cũng đều rất chín, rất sâu. Đọc cuốn sách này có khi cười, khi lại trầm tư suy nghĩ. Nói chung, nó vừa là cẩm nang tâm lý học, vừa như cẩm nang tình dục học :D
5
418047
2014-09-11 11:30:38
--------------------------
