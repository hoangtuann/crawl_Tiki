457760
10422
PR hay còn gọi là giao tế cộng đồng (cũng có thể gọi là quan hệ cộng đồng là nhằm xây dựng quan hệ tốt đẹp giữa một công ty với người tiêu dùng, các tổ chức chính phủ và các tổ chức phi chính phủ. Việc hiểu rõ tầm quan trọng của PR sẽ giúp bạn xây dựng được các chiến lược chiêu thị trong tiếp thị rất hiệu quả. Quyển sách này sẽ cung cấp cho bạn các kiến thức về PR như cách lên một kế hoạch PR, cách làm việc với giới truyền thống,... một cách hiệu quả nhất. Đây là một quyển sách dành cho các bạn làm trong giới tiếp thị và truyền thông.
5
356759
2016-06-24 15:48:08
--------------------------
245750
10422
Quyển sách này sẽ đặc biệt phù hợp với các bạn sinh viên lần đầu tiếp cận với bộ môn PR. Thông thường, khi mới học bạn sẽ cần được biết bao quát, tổng thể về PR. Các chương đầu tiên trong các giáo trình sẽ hơi cứng nhắc và có đôi chút khó hiểu.

Với quyển sách này, bạn sẽ được giới thiệu một cách rất cơ bản về PR, những ví dụ, minh hoạ cũng được nêu ra song song với các định nghĩa giúp bạn có cái nhìn tổng thể hơn. Quyển sách này mình nghĩ nó hoàn toàn đúng với giá trị của nó. 4 sao mình cho ở đây là do mình đọc khi đã biết cơ bản về PR rồi nên thấy cũng hơi hụt hẫng khi đọc xong.
4
183286
2015-07-29 00:35:06
--------------------------
