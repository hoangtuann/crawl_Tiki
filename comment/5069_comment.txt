340480
5069
"Đầu đuôi câu chuyện dư lào, kể khúc giữa nghe coi" @_@
Đây là chiêu mà tác giả áp dụng cho truyện này. Đồng thời cũng là 1 cách tra tấn dã man lên người đọc. Vừa đọc vừa căng não đặt dấu chấm hỏi với các tình tiết bên trong chứ chẳng phải đọc để xả stress, để tìm niềm vui thay vào đó là sự hồi hộp có phần nghẹt thở. Càng đọc càng bị lôi vào như 1 cuộc hành trình phượt kết hợp trinh thám. (Mình đã leo Fan nên lại càng bị kích thích khi đọc)
Cuốn truyện hay nhất đc đọc từ trc đến giờ. Sau này mà gặp tác giả chắc mình phải gọi bằng "cụ" để tỏ lòng ngưỡng mộ quá :)))
5
876207
2015-11-19 22:27:41
--------------------------
321496
5069
Lôi cuốn không kém phần 1, phần 2 lấy anh chàng Quân trẻ tuổi, ngông nghênh làm nhân vật trung tâm. Nhưng không còn là chàng em rể với cái vẻ bất cần đời. Ở Quân giờ đây là một con người đứng đắn, sâu sắc, một chút trải đời nhưng vẫn rất hài hước và con tim tràn ngập yêu thương. Trong tập truyện này có một chút dài dòng, một chút lủng củng nhưng trên hết vẫn khắc họa được sự trưởng thành của Quân, đặc biệt là hành trình tìm thấy tình yêu của cuộc đời mình. Những trang sách còn là bài học về con đường đi của tuổi trẻ, tình yêu, tình cảm gia đình...Rất thích tác phẩm của Thu Thủy, nhẹ nhàng nhưng đầy ý nghĩa!
4
137718
2015-10-14 01:03:11
--------------------------
276559
5069
Chưa bao giờ thất vọng khi đọc những tác phẩm của Nguyễn Thu Thủy! Lối viết văn rất nhẹ nhàng, dí dỏm cùng với cách dẫn chuyện cuốn hút làm cho người đọc không thể rời mắt khỏi trang sách. Đặc biệt, câu chuyện được Thủy kể trong "Hot boy và Eo 58" lần này càng đáng để đọc hơn, bởi trong đó, tác giả không chỉ kể cho chúng ta nghe câu chuyện về tình yêu đôi lứa mà còn đem đến những suy tư về tình cảm gia đình, tình bạn, tình người... Gấp trang sách cuối cùng của Thủy lại, vẫn cảm thấy có chút bâng khuâng, tiếc nuối... Chờ những tác phẩm tiếp theo của Nguyễn Thu Thủy!
5
142327
2015-08-24 09:53:28
--------------------------
261437
5069
Cuốn sách “Hotboy Và Eo 58” của tác giả Nguyễn Thu Thủy có thể được xem như hệ liệt với tác phẩm "Gái già xì tin" đã được xuất bản và chuyển thể thành phim trước đó. Đó là câu chuyện tình yêu của nhân vật Quân đã trưởng thành. Không là một tình yêu sến, hay lâm li bi đát; đơn giản chỉ là mối qua hệ tình cảm giữa Quân và Thụy hết sức đời thường cùng tình cảm gia đình được lồng vào trong mang những giá trị tốt đẹp. Lối viết nhẹ nhàng nhưng vẫn tạo được kịch tính. Bìa sách thiết kế được, giấy cũng bình thường không bị lỗi gì.
4
276840
2015-08-11 09:04:27
--------------------------
170015
5069
đây là cuốn sách sau gái già xì tin kể về hành trình của hót boi quân tìm kiếm người đẹp eo 58 cm. phải nói là mình đã chờ đợi, chờ đợi mỏi mòn đến mỏi cả cổ mới có được cuốn sách này, nhưng nó lại làm mình hơi thất vọng tí xíu.
cách viết của nguyễn thu thủy trong cuốn này k sắc bàng trong gái già xì tin, nó có vẻ hơi rắc rối lủng củng và có chút cẩu thả, kiểu như tac giả viết và post lên mạng xong nhưng khi in lại k chỉnh sửa lại. nhưng nó vẫn rất hay, anh chàng quân làm mình mơ mộng và ao ước sao cho có một anh chàng như thế ngoài đường để mình hốt. thụy thiệt là hạnh phúc vì có một hot boi như thế yêu tha thiết, đặc biệt mình rất thik:
yêu của anh,
ngoan là của anh
5
166271
2015-03-19 12:19:37
--------------------------
167268
5069
Đây là 1 cuốn truyện thú vị, với những nhân vật thú vị, những tình huống thú vị. Hấp dẫn trong 1 câu chuyện tình yêu cuốn hút, nhưng lại sâu lắng trong những thân phận đầy nối niềm tâm sự riêng, và hài hước, đơn giản nhẹ nhõm trong 1 lối văn giàu cảm xúc.
Quân, anh chàng 27 mắt một mí, tính cách tưng tửng, quậy phá  đã từng làm xiêu lòng bao người trong Gái già xì tin, giờ này đã trở thành người đàn ông chững chạc, thành danh, hấp dẫn, đến mức Ái Thụy, cô gái vốn lãnh đạm và khép mình cũng không thể cưỡng được anh. Cuộc gặp gỡ trên dãy Hoàng Liên Sơn, trong hành trình leo Fan của cả hai thật thú vị, và mở ra một chuyện tình hấp dẫn.
Hành trình để Thụy, Quân đi đến tình yêu, hạnh phúc không hề đơn giản, nhưng điều tôi thích là câu chuyện thực sự không có nhân vật thứ 3 nào phá hoại. Mà chỉ là mỗi người, trong tình yêu, đều phải dẹp bỏ sự ích kỉ, cá nhân, đôi khi dẹp bỏ cả sự kiêu hãnh để có thể đến bên nhau. Cái kết của truyện, đã làm tôi hoàn toàn thỏa mãn.
Một cuốn truyện VN, mà nếu bỏ lỡ, chắc hẳn bạn sẽ tiếc.
5
582887
2015-03-14 14:18:13
--------------------------
