278886
9412
Mình mua quyển sách này với dự định là tự kiểm tra và tự nâng cao trình độ nghe tiếng Anh. Quả thật đây cũng là một cuốn sách hay đối với người muốn luyện nghe ở trình độ cao (advanced), cả đối với người tự học hoặc đối với học viên đang theo học tại các trung tâm ngoại ngữ (dùng với tư cách là tài liệu luyện nghe bổ sung).

Sách của Tiki giao tương đối mới (dù ko có bọc nylon), đĩa đầy đủ và nghe được. Nói chung là hài lòng với sách này.
5
64289
2015-08-26 11:45:52
--------------------------
271824
9412
Bạn nào đang muốn nâng trình độ nghe cao hơn nữa hoặc đang muốn luyện để tăng điểm TOEFL thì mình nghĩ đây là quyển sách dành cho các bạn. Sách chia thành các dạng câu hỏi thường gặp trong bài thi TOEFL theo từng chương, từ đó giúp các bạn dễ định hình và làm quen với cách thức thi nghe TOEFL ibt. Phần nghe khá dài và khó, nhưng phần trả lời và trích dẫn giải thích ngoài sau sách rất rõ ràng, nên mình nghĩ đây cũng là 1 trong những quyển sách hay mà các bạn nên chọn mua về học song song với việc luyện ở các trung tâm anh ngữ.
5
322571
2015-08-19 14:58:21
--------------------------
