431031
9053
Bị mê mấy quyển Lessons for Reading /Listening/ Writing ... của nhà xuất bản này rồi. Sách bìa dày, bên trong màu đẹp, trình bày bắt mắt. Nói chung là rất hữu ích. Chỉ sợ bản thân không có thời gian đầu tư ôn luyện thôi. Nếu bạn nào trình độ kha khá, trên trung bình rồi thì cứ ôm mấy quyển này luyện ngày luyện đêm, có thể luyện thêm speaking thực tế nữa, thì kết quả thế nào cũng đủ tốt. Chứ đến trung tâm chưa chắc đã hiệu quả bằng. "Có công mài sắt có ngày nên kim" mà
5
145583
2016-05-16 14:50:55
--------------------------
335325
9053
Lessons for IELTS Reading là một quyển sách vô cùng quý giá đối với mình. Mình rất thích màu sắc bắt mắt, cách bố cục theo từng unit riêng biệt và số lượng bài tập của nó. Đây là quyển sách không thể thiếu cho các bạn muốn nâng cao kĩ năng đọc tiếng anh đến tầm 5.5-6.5. Gía quyển sách rất phù hợp với chất lượng của nó. Tuy nhiên, có một điều mình không hài lòng lắm, mình nghĩ, sách nên cần có thêm những bài mock tests, ít ra là để người đọc có thể làm quen được với bài thi mấu của kid thi IELTS.
5
923880
2015-11-10 21:29:44
--------------------------
306078
9053
Sách này rất tuyệt! Sách luyện cho mình những bí kíp rất bổ ích cho kì thi IELTS Reading! Đưa ra những chủ đề thực sự rất hấp dẫn, gần gũi với bạn đọc! Màu sắc thì sinh động rất gây hứng thú với người đọc. Tuy vậy thì sách còn một số khuyết điểm như : cung cấp hơi ích các bài tập thực hành tập đọc  . Mình nghĩ sách nên đưa ra đáp án kèm theo những dòng hướng dẫn. Thay vì chỉ đơn thuần là A,B,C,D. Nếu các bạn luyện một cách thuần thục những kĩ năng này thi IELTS 7.5 Reading không phải là chuyện quá khó?
4
227063
2015-09-17 12:41:01
--------------------------
283380
9053
Theo mình thấy thì cuốn sách này hay và dễ sử dụng. Các bài reading theo mức độ từ dễ đến khó giúp việc luyện tập dễ dàng hơn. Trong sách còn có các tips giúp người học làm được bài tốt hơn. Giấy rất đẹp, hình ảnh và màu sắc sinh động giảm bớt nhàm chán. Các chủ đề trong sách cũng rất hay, gần gũi với thực tế. Đây là cuốn sách nên mua cho các bạn định thi IELTS, mình nghĩ nên mua kèm theo cuốn Soeaking nữa, vì có nhiều topic hay. Nếu thật sự làm theo hướng dẫn của sách thì bạn sẽ được điểm như ý.
4
127451
2015-08-29 22:38:53
--------------------------
274114
9053
cuốn này rất hay, và mình hài lòng nhất trong bộ lesson for ielts, sách viết các bài reading phù hợp với kì thi thực tế, bên cạnh còn đề cập các câu hỏi thường gặp trong đề thi, ngoài ra còn có các từ vựng phù hợp với từng chủ đề, sách in màu đẹp có kèm theo hình vẽ, mỗi chủ đề đều nêu ra các tips giúp tăng kĩ năng đọc, đọc nhanh và chọn được đáp án đúng nhanh nhất,hoàn thành cuốn sách này giúp mình tự tin hơn cho kì thi, mình sẽ giới thiệu bộ sách này cho bạn bè.
4
655306
2015-08-21 17:37:39
--------------------------
