304070
7976
Tôi rất háo hức khi mua cuốn sách này nhưng khi đọc thì thấy hơi thất vọng. Lời văn tuy hài hước hóm hỉnh nhưng đều đều giống nhau, những mẫu hội thoại hơi rườm rà không đọng lại nhiều trong tư tưởng của tôi, tác giả có vẻ như hơi vơ đũa cả nắm, khi tất cả trải nghiệm của tác giả chỉ là những trải nghiệm cá nhân, gần nửa cuốn sách là hỏi đáp thắc mắc bạn đọc, nên tôi nghĩ với mức giá như thế này thì cũng hơi mắc. tuy nhiên, cái được của tác phẩm theo tôi đó là những mẫu chuyện khá hài hước, mang tính giải trí cao.
2
36284
2015-09-16 10:18:21
--------------------------
280765
7976
Mình rất ít khi đọc mấy cuốn sách kiểu như này vì nghĩ chẳng có gì nhàm chán , cứ công thức này nọ, ý kiến chủ quan không gì hay ho, nhưng hôm bữa vô tình đọc ké của nhỏ bạn bị thu hút quá trời nên về đặt mua luôn. Lần nào buồn buồn lôi ra đọc lại cũng thấy mắc cười. Một cuốn sách viết về hôn nhân gia đình khá mới mẻ với cách viết chân thực và vô cùng hài hước, không nhiều quy tắc này nọ , mang lại cảm giác rất dễ chịu. Mình thích cuốn này đến nỗi, đám cưới của mấy đứa bạn thân là đứa nào cũng được mình tặng một cuốn như vậy hết. Môt cuốn sách hay , đọc để biết cách dung hòa cuộc sống vợ chồng sau này.Nói chung xứng đáng là "best seller" của 2013 ^^
4
75031
2015-08-27 21:53:53
--------------------------
230930
7976
Tác giả viết nên cuốn sách với lời văn hóm hỉnh và vui nhộn, cảm giác khi đọc rất vui vẻ nhưng thật sự đọng lại trong cá nhân mình lại không có gì mấy. Cuốn sách chỉ dừng lại ở mức độ giải trí chứ chưa có gì sâu sắc tuy rằng có thể bản thân độc giả có thể nhìn thấy chính bản thân mình ở một vài câu chuyện. Quan điểm của tác giả có vẻ đánh đồng quá, mình không nghĩ phụ nữ ai cũng thế và đàn ông ai cũng như tác giả nói vậy. Chính cái lối "áp đặt" này có lẽ làm cho mình cảm thấy "hơi" khó chịu trong một vài đoạn chăng?
3
146626
2015-07-17 20:31:53
--------------------------
215771
7976
Sách là những gom góp trải nghiệm của tác giả về những mối quan hệ, giữa đàn ông và phụ nữ, giữa chồng và vợ và tất cả chỉ từ trải nghiệm cá nhân, có lẽ vậy. Cách viết thấy khá là cảm tính, và mình cũng không thích cái sự gom bó đũa chọn cột cờ kiểu như thế này. nào là đàn ông thì như thế này, đàn bà thì như thế nọ, vợ phải cư xử như thế lọ, chồng là như thế chai. Trải nghiệm cá nhân thì không xấu, nếu tác giả chỉ đơn giản kể chuyện nhà mình thì tôi cũng khá thích, có điều cứ nâng cao quan điểm lên thấy mệt mỏi quá. Mà những bài viết thế này cũng tràn lan khắp các thể loại báo tạp chí rồi
2
419116
2015-06-26 19:57:50
--------------------------
179758
7976
những câu chuyện rất hay và rất vui nhộn, các bạn có thể tưởng tượng được cuộc sống của tác giả thông qua những lời kể hóm hỉnh, khắc họa rất chân thực về những gì rất nhỏ trong cuộc sống đôi lứa, hay chỉ đơn giản là những chi tiết rất dễ thương trong từng khoảnh khắc của cuộc sống!
Với tôi, cuốn sách đã mang đến thật nhiều niềm vui, tiếng cười và sự sảng khoái khi đọc từng dòng chữ, không những thế, tôi đã tìm thấy tôi trong chính từng tác phẩm của nhà văn, tìm được những ghen tuông tưởng như là chỉ có mình mình có ^^ và tìm thấy được cả anh bạn trai yêu thương phải chịu đựng mình và phát hiện ra cả việc anh ấy cũng sâu sắc không kém trong mỗi hành động hàng ngày, 
Nói chung, dù nam hay nữ đều nên đọc và trải nghiệm cùng quyển sách. Nó thật sự vui vẻ và hấp dẫn ngay từ những trang đầu tiên ^^
5
315009
2015-04-07 22:21:39
--------------------------
131338
7976
Cuốn sách kể lại những câu chuyện và trải nghiệm rất thật của tác giả. Bên cạnh đó, tác giả cũng là người có khiếu hài hước nên lời văn rất dễ thương. Phải công nhận một điều là những gì tác giả viết trong sách hầu như là chính xác. Ai cũng có thể dễ dàng bắt gặp hình ảnh của mình ở một trong hai mẩu chuyện của tác giả dù là đàn ông hay phụ nữ.

Nhưng cuốn sách cũng chỉ dừng lại ở mục đích giải trí bằng những câu chuyện hài hước. Không đọng lại gì nhiều sau khi đọc hết sách.
3
130747
2014-10-24 09:38:22
--------------------------
112206
7976
Khi cầm cuốn sách trên tay, đọc vài chương đầu tôi phì cười vì nhận ra chính mình đang được khắc hoạ rất đúng. Tôi như được đọc chính mình và cả người tôi yêu thương trong đấy. 
Anh tác giả này rất hay khi miêu tả sự hiểu biết tường tận của anh cề phụ nữ và đàn ông bằng lối viết dí dỏm và dễ thương đến lạ lùng. Anh nói đúng nhưng không làm bạn đọc nữ giới cảm thấy điên tiết mà làm họ thấy mình cần tiết chế điều gì và nên phát huy điểm mạnh nào rất cụ thể. 

Mong rằng tác giả sẽ phát hành nhiều cuốn sách chân thực hư thế này.
4
10784
2014-05-10 11:22:25
--------------------------
106893
7976
Quyển sách này chung quy  là kể và vd ra những thói quen, cảm xúc và hành vi rất rất  là quen thuộc và phổ biến đối với đàn ông cũng như phụ nữ trong các mối quan hệ tình cảm. Nó như thay lời muốn nói lên cảm nhận suy nghĩ mà đôi khi ta không biết tại sao mình lại có thái độ hay hành vi như vậy khi người đàn ông thế này hay người phụ nữ thế kia. Mặc dù cuốn này không phải thật sự là xuất sắc nhưng cũng cho tôi được một cái nhìn chân thật về lối sống, về hành vi cũa mình không phải là độc nhất vô nhị mà ai là con gái thì cũng sẽ như thế, ai là phụ nữ thì cũng sẽ như vậy khi đàn ông con trai nó cứ cho rằng 1 "cuộn chỉ " chỉ là 1 sợ chỉ dài thôi chứ không có gì lớn lao cả.
2
116498
2014-02-26 09:22:11
--------------------------
104711
7976
Hi mọi người! Mình vừa đọc xong quyển này (một số bài đọc lần thứ 2, như "Anh có yêu em không?""Vì sao anh yêu em?""Đàn ông tốt không nói chuyện đám cưới""Nàng Becky của tôi"...). Cứ vừa đọc vừa cười một mình như một đứa dở hơi :"> 
Mình thấy tác giả quyển này có một điểm rất giỏi, đấy là anh rất khéo trong việc đưa thông điệp của mình vào các câu chuyện và thể hiện nó một cách vô cùng vô cùng dí dỏm. XHDVTKVN không phải là một quyển sách đọc chỉ để gây cười, nó chỉ là vỏ bọc của một loạt những thông điệp, những khái niệm về phụ nữ qua góc nhìn của đàn ông (mà tác giả là đại diện). Nếu tinh ý, sẽ thấy tác giả Đức Long là một người đàn ông rất nhiều trải nghiệm. Có lẽ anh này phải yêu nhiều, thất tình nhiều hoặc đá con nhà người ta nhiều rồi mới hiểu rõ về phụ nữ thế. Mình cũng nhận ra là quyển này phù hợp với những ai có nhiều trải nghiệm tình trường (như mình, hehe). Nếu là một người khô cứng, cả đời chỉ có một mối tình duy nhất thì chưa chắc đã thấy thú vị. Thể nào cũng bảo tác giả chém gió :))
NGười ta nói đỉnh cao của hài hước là tự trào. Nếu đúng như thế, mình thấy Đức Long là người tự trào siêu giỏi.
4
273246
2014-01-17 10:50:46
--------------------------
103895
7976
Mỗi người có một gu riêng, và quyển sách này không thuộc gu của tôi. Thoạt đầu tôi tưởng nó là một quyển cẩm nang bí quyết thú vị về đàn ông- đàn bà. Đọc những nhận xét khen ngợi, tôi đã không ngần ngại mua ngay 1 quyển. Nhưng tiếc là, bìa sách rất đẹp, giấy tốt và cách trình bày khoa học không đủ làm tôi thỏa mãn số tiền mình bỏ ra. 

Nội dung nhàn nhàn, không có gì gọi là đáng gây cười, cũng không có gì gọi là suy nghĩ sâu sắc. Đơn thuần chỉ là liệt kê những câu chuyện rồi bình luận giọng điệu khá mỉa mai. Có lẽ nó hợp với mọi người, nhưng không hợp với tôi lắm, còn làm tôi hơi khó chịu khi đọc nó.

Khuyên những ai không thích kiểu văn châm biếm nên cân nhắc trước khi mua quyển này.
2
120276
2014-01-05 11:17:53
--------------------------
103493
7976
Thú vị quá! Tôi chưa từng đọc một cuốn sách dạng "nửa truyện nửa tâm lí" nào đặc biệt như vậy. Các câu chuyện về giới tính được tác giả viết rất khéo, và anh này cũng chứng tỏ được khả năng nắm bắt tâm lí chị em siêu đẳng (chả trách anh ta đã phải rào dậu cái câu "Tất cả các nhân vật đều có thật, trừ Tôi" ngay ngoài bìa ;)). Hài hước, dễ thương, nên chắc chắn chẳng người phụ nữ nào giận được Đức Long ngay cả khi anh cố tình trêu ngươi chị em bằng cách nói quá lên một số tính xấu của họ. 
Đây là một trong những cuốn sách tôi cho rằng đáng đọc nhất trong năm và nếu bạn đọc trong dịp nghỉ Tết sắp tới thì rất tuyệt. Mặc dù tôi chưa thích lắm cái bìa cũng như chất lượng in nhưng không hề gì. Chúc tác giả sớm tái bản tác phẩm này và xuất bản thêm nhiều cuốn sách khác cũng thú vị như thế. Tôi đã có thêm một tác giả yêu thích!!!
5
195222
2013-12-31 15:55:38
--------------------------
102614
7976
Nói không ngoa tí nào chứ, cuốn sách này khiến tôi cười bò lăn. Đức Long vẽ nên khá nhiều bức tranh biếm họa hay ho về phụ nữ - đàn ông, nhưng đặc biệt ở chỗ vẽ bằng chữ. Mà bạn biết tranh biếm họa rồi đấy, có hơi quá lên một tẹo, nhưng buồn cười. Những mẩu tản văn này cũng vậy. Tôi không thấy phụ nữ xấu xa tới mức như nhà văn này nói, nhưng tôi cũng chẳng trách được anh chút nào, ngược lại còn thấy rất thích thú với những gì anh nói. Nó thật ở một mức độ nào đó, và bạn phải soi gương nhìn lại mình xem có đúng là không bị anh "bắt bài" hết rồi hay không. Rất tuyệt vời, thật đó.
Chỉ có một chút không hài lòng về chất lượng in của cuốn sách mình sở hữu thôi, mực in không đều cho lắm.
4
102259
2013-12-21 17:37:52
--------------------------
100165
7976
Quả là một cuốn sách thú vị! Mỗi một tình huống, cảm xúc và tâm trạng của cả hai phe đều được lột tả chân thực bằng những ngôn từ hết sức đời thường nhưng không kém phần sâu sắc và hài hước lại đánh trúng tim đen của các cô gái khi yêu, nhiễm nhiên tôi không ngoại lệ. Những gì nhân vật tôi trong sách chia sẻ (dù có được hư cấu nhưng hoàn toàn dựa trên thực tế) là điều tôi cảm thấy rất hữu ích. Đó là điều mà cánh đàn ông chẳng bao giờ nói thẳng, khiến các cô gái đang yêu lúc nào cũng u u mê mê, mà có khi thế mới vướng vào lưới tình của các anh lâu dài được. Đọc tới đọc lui, đọc ngấu nghiến như món tủ của tinh thần vậy, cuối cùng tôi cũng chỉ khẳng định được điều mà trước giờ vẫn như là nguyên lý không thể tách rời với tình yêu, đó là "Chỉ cần tình yêu đủ lớn thì giới hạn nào cũng sẽ vượt qua".
4
190754
2013-11-22 14:43:09
--------------------------
99490
7976
Hôm nay vừa nhận được quyển này, đọc luôn tại công ty, vừa đọc cứ vừa cười rồi hỏi mình Ô sao đúng thế :))

Quyển sách rất đặc biệt, nội dung hầu như không quá mới ( yếu tố tâm lý phụ nữ trái gió trở trời đc khai thác khá nhiều ) nhưng không hề làm cho người đọc nhàm chán, bởi lời văn hài hước, miêu tả sinh động, câu chữ không hề văn hoa trau chuốt mà ngược lại, dùng lối "văn nói" làm tăng thêm tính gần gũi đời thường. Mình rất thích phong cách này của nhà văn Đức Long. Mỗi dòng mỗi chữ, mỗi câu chuyện, thậm chí tác giả đặt chính mình vào nhân vật để trải nghiệm ( tất nhiên, là nhân vật hư cấu :)) ) khiến cho mình thấy rất thú vị. Kiểu như được tâm sự, được trò chuyện với một người bạn.

Những câu truyện ngắn trong sách, hay đúng hơn là những "tản văn" trò chuyện đều khai thác được cái bản tính "sáng nắng chiều mưa, trưa buồn nổi bão" của phụ nữ, và đi sâu vào các khía cạnh khác trong tình yêu, cuộc sống theo cách rất đời thường, bình dị, nhiều khi đơn giản đến mức mình thấy ở ngoài phức tạp, lên sách sao lại "à, ra thế".

Một quyển sách rất đáng để xem, không chỉ vì nội dung, mà còn là vì một ngày thật đẹp. Xem, để hiểu và để cười :)
4
170765
2013-11-15 13:18:47
--------------------------
