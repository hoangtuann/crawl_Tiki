400591
8307
Thật ra là làm nhân viên khó hay làm nhà lãnh đạo khó? CEO là nhà quản lý hay nhà lãnh đạo, giữa hai thái cực này thì CEO sẽ đi theo thái cực nào. Hay là CEO phải là một người theo triết lý trung dung đi giữa 2 con đường để giữ được sự khách quan nhất từ đó mới có thể sáng suốt lèo lái công ty. Các phẩm chất nào một CEO cần phải có, đó có phải là phẩm chất của một vị tướng ra trận ngày xưa hay không. Tất cả những kiến thức sẽ được giải đáp tại sách này.
4
465332
2016-03-19 16:23:45
--------------------------
218117
8307
Quyển sách cẩm nang quản lý và CEO sẽ giúp chúng ta hiểu và biết được những gì mà ta phải chuẩn bị và thực hiện khi được chuyển đổi từ nhân viên thành cấp quản lý đồng thời giúp ta biết như thế nào là một nhà quản lý thực thục. Quyển sách sẽ trang bị cho ta những kiến thức nhằm giúp ta hoàn thiện bản thân hơn khi ta đã trở thành một nhà quản lý như: biết cách đối đầu với căng thẳng và cảm xúc, quản lý sự thay đổi, thi hành quyền hạn,... Đây là một quyển sách cần thiết cho những Nhà quản lý và cho những người muốn trở thành nhà quản lý trong tương lai.
5
356759
2015-06-30 14:04:20
--------------------------
197954
8307
Nhiều người thường nhầm lẫn giữa một nhà quản lý thực thụ và một "nhân viên làm quản lý". "Nhân viên làm quản lý" thường xen vào các công việc mang tầm vi mô của nhân viên mà quên mất vai trò của mình. Để trở thành một nhà quản lý thực thụ hoàn toàn không phải là một việc dễ dàng. Sau khi đọc xong quyển sách này, tôi thấy rằng những nội dung trong sách không chỉ là giúp cho "nhân viên làm quản lý" trở thành một nhà quản lý thực thụ mà còn cung cấp những kiến thức cho các nhà lãnh đạo cấp cao quản lý những nhà quản lý cấp trung dưới quyền của mình. Quyển sách rất đáng đọc với những ai đang công tác trong vai trò quản lý.
5
387632
2015-05-18 20:41:53
--------------------------
