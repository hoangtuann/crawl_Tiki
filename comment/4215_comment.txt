484243
4215
Hum nay đọc thấy tiki không tặng tiki xu cho nhận xét nên mới víêt cái này ^^. Mình mua cuốn này cách đây 1 năm, không phải ở tiki nhưng có 1 số nhận xét sau: sách hay nhưng ko dành cho nhiều đối tượng,nói chung tập trung cho các đối tượng nghiên cứu các ngành xã hội (ko chỉ xã hội là lịch sử, địa lý... gì đâu, ngành xã hội nghĩa chung chung ý)
Kiến thức đưa ra nhiều, có hệ thống tuy nhiên phần dịch hơi lan man, hix có nhiều chỗ đọc hơi bực mình vì cách dùng từ thay thế của người dịch làm cho nội dung đã mang tính khoa học hàn lâm đã nhức đầu lại còn hơi khó hiểu.
Nói chung là ai thích khám phá thì nên đọc, để có cái mà chém gió về thế giới, còn một số bạn chuyên ngành có thể làm tài liệu nghiên cứu.
Mình làm giảng viên 1 ngành xã hội nên thấy có thể khai thác được lượng kiến thức nhất định tùy vào yêu cầu của mỗi người;
4
132568
2016-09-22 09:18:48
--------------------------
360557
4215
Nội dung sách hay, đáng đọc. Sách được in khá đẹp, dễ đọc. 
Mình biết đến cuốn này qua tác phẩm "Súng, vi trùng và thép". Những thể loại này mình rất thích đọc. Quả thực, những nghiên cứu trong mỗi cuốn sách của các giáo sư nước ngoài thật tuyệt vời. Sự uyên thâm và tri thức được cung cấp trong đây thật đáng ngưỡng mộ. Thế giới con người và sự phát triển của nó tuy hiển hiện ra đó những cũng ẩn chứa nhiều thứ tuyệt vời. Tóm lại là bạn hãy chộp lấy 1 cuốn mà đọc nhé :)
5
139428
2015-12-29 13:26:01
--------------------------
354549
4215
Biết đến Jared Diamond không phải qua cuốn "súng vi trùng và thép" mà qua "sụp đổ", cuốn nào khi cầm trên tay cũng đọc một cách ngấu nghiến, tác giả có một kiến thức thật sự bao quát và cách viết cũng khiến người đọc dễ hiểu hơn. Những phân tích cho thấy không phải điều kiện tự nhiên đã hình thành nên đặc điểm xã hội mà con người sinh sống ở đó, những người truyền thống không phải là "mọi" mà chính là con người hiện đại cách đây hàng ngàn năm. Tuy nhiên, bản dịch cũng có những đoạn dịch hơi tối nghĩa. Nhưng đây là một cuôn sách hay đáng để đọc với những ai yêu thích nghiên cứu thế giới.
5
329102
2015-12-18 09:01:04
--------------------------
274038
4215
Mình được biết đến Jared Diamond lần đầu tiên khi đọc tác phẩm "Súng, vi trùng và thép" và ngay lập tức cảm thấy "choáng ngợp" với lượng kiến thức đồ sộ mà vị giáo sư này cung cấp. Đọc quyển sách này, chúng ta có thể hiểu thêm nhiều hơn về xã hội hiện đại mà mình đang sống, hiểu về sự thay đổi của kết cấu xã hội trải qua hàng triệu năm phát triển của nhân loại để rồi từ đó trả lời những câu hỏi hóc búa về xã hội loài người, về nền văn minh nhân loại. 
5
143116
2015-08-21 16:15:26
--------------------------
273220
4215
Bên cạnh việc phân tích các đặc trưng của xã hội xưa mà chúng ta còn phải học hỏi, cuốn sách này còn khái quát hóa được tổng quát xã hội ngày nay với những mặt tiêu cực mà xã hội xưa không bao giờ có, và quả thật phải công nhận rằng thế giới chúng ta đang sồng tuy thật dễ sống nhưng lại khắc nghiệt.
Ngoài ra, tác giả cũng nói sơ qua mà lại khá đầy đủ về khái niệm nhà nước và phi nhà nước. Tôi đăc biệt thích phần nói về tư pháp, nhất là tư pháp phục hồi - tội phạm và nạn nhân cùng gặp nhau trên tinh thần hòa giải. 
Đây quả là một cuốn sách hay cần được nghiên cứu kĩ lưỡng vì qua đó, con người có thể xác định được cách sống của mình về tôn giáo, về cách dạy con, về sự cẩn trọng trong cuộc sống,... các chính khách cũng có thể tham khảo cuốn sách này để xác định được các con đường tốt để xây dựng xã hội trở nên tốt đẹp hơn.
5
366877
2015-08-20 19:52:20
--------------------------
262238
4215
Từ việc so sánh xã hội hiện đại với các xã hội như New Guinea, chúng ta thấy được quá khứ từ hàng triệu năm trước. Sách cung cấp một lượng kiến thức sâu rộng về con người, xã hội, chiến tranh, kinh tế thị trường, các mối quan hệ trong một gia đình, tôn giáo, ngôn ngữ, sức khỏe, ... Qua việc xem xét những khác biệt về quá khứ và hiện tại, chúng ta có thể học hỏi được nhiều điều từ các xã hội truyền thống để tạo nên một thế giới tốt đẹp hơn. Đây là một quyển sách bổ ích.
5
192644
2015-08-11 19:24:45
--------------------------
258209
4215
Chúng ta đang sống trong thế giới nhà nước hiện đại được cho là văn minh, tiến bộ của nhân loại - một thế giới chỉ mới ra đời sau khi nông nghiệp xuất hiện 1500 năm, nhưng liệu thế giới ấy đã thật sự phát triển vượt trội đạt đến mọi thứ tốt đẹp nhất mà con người từng biết đến?
Ở tác phẩm kinh điển đầy tri thức này, ngài Jared Diamond bỏ ra hơn 30 năm nghiên cứu để trả lời cho câu hỏi lớn đó. Nghiên cứu về xã hội truyền thống để tiếp tục sự so sánh với xã hội hiện đại, tìm ra sự khiếm khuyết để khắc phục cũng như khuyến khích những tiến bộ. Người đọc từ đó, không chỉ dựa trên quan điểm của riêng tác giả, mà còn có thể tự mình xác định được lối sống phù hợp cho bản thân và từ đó trân trọng giá trị của truyền thống mà phần đông nhân loại đã bỏ quên phía sau.
5
364576
2015-08-08 12:14:30
--------------------------
208457
4215
Quyển này của GS Diamond hay, nói nhiều về cách hành xử của con người cổ và thực ra vẫn còn tồn tại nhiều ngày nay. Nói về vai trò của tổ chức và nhà nước. Nếu bạn thích quyển Súng vi trùng và thép thì cũng sẽ rất thích quyển này. Tuy nhiên là sách dịch nên đôi lúc đọc hơi dài dòng.
Sách của GS Diamond hay ở chỗ ông tiếp cận trực tiếp những nơi ông đề cập đến trong sách, cách viết chen lẫn kể những câu chuyện của ông làm người đọc không bị nhàm.
5
169828
2015-06-15 10:11:19
--------------------------
