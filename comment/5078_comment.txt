426245
5078
Những Cuộc Phiêu Lưu Của Pinocchio được xếp vào hàng kinh điển danh tác nên thiết nghĩ không có gì để bình luận về nội dung cả, và hẳn là tất cả độc giả đã quyết định mua quyển sách này đều đã sống với tuổi thơ có sự xuất hiện của cậu bé người gỗ mũi dài này qua các câu chuyện cổ tích hay phim hoạt hình rồi. Điểm nhấn của lần xuất bản này là một bản dịch chuyên nghiệp và chi tiết đến từng con chữ, bám rất sát nguyên bản phổ biến nhất nên cũng có khi gây khó hiểu cho độc giả nhỏ tuổi. Cá nhân tôi nghĩ ấn bản này sẽ có giá trị hơn với độc giả trưởng thành muốn nghiên cứu tác phẩm một cách chi tiết như một danh tác văn học hơn là truyện thiếu nhi.

Minh họa trong sách rất ấn tượng và đẹp mắt, rất dễ chiếm cảm tình và một lần nữa có lẽ thu hút độc giả trưởng thành hơn là trẻ em, vì phong cách hơi cổ điển và chân thực.
5
274545
2016-05-06 14:57:20
--------------------------
398848
5078
Những Cuộc Phiêu Lưu Của Pinocchio là một trong những cuốn sách nhiều tình tiết nhất mà tôi từng biết, cuộc phiêu lưu của chú bé người gỗ Pinocchio với bao sóng gió và gian khổ cứ liên hồi bất tận khiến độc giả được một bữa tiệc thưởng thức no nê. Và tính giáo dục thì cũng nhiều như thế. Trẻ em đọc cuốn này rất thích hợp vì từng câu từng chữ tác giả đều nêu lên những bài học, giúp trẻ hoàn thiện mình tốt hơn. Ngoài ra ông cũng thêm vào ẩn dụ phê phán thói hư, tật xấu của người lớn. Một cuốn sách thỏa mãn được độc giả ưa truyện phiêu lưu, thần kỳ và có ý nghĩa nên đáng được 5 sao.
5
386342
2016-03-16 20:48:13
--------------------------
377635
5078
- Theo hiểu biết của mình thì Pinocchio sách ra đời trước phim 29 năm, vậy nên nếu có chi tiết gì khác nhau giữa sản phẩm của Carlo và Disney thì hẳn phải là do bộ phim đã thay đổi chút ít bản gốc (vì có nhận xét của kha khá bạn hơi giống kiểu phim của Disney mới là bản gốc).

Về hình thức thì không còn gì để chê. Bìa cứng, đẹp, giấy tốt, bóng, màu sắc trang nhã, không gây nhức mắt, font và size chữ đẹp. Hình thì quá hoàn hảo. 10/10 điểm cho hình thức.

Về nội dung, mình cũng là một đứa xem phim rồi mới đọc nên khá bất ngờ trước những tình tiết Disney đã cắt bớt đi. Nhìn chung, đây đúng là một quyển sách mà mọi đứa trẻ đều nên đọc. Ẩn chứa trong từng chương truyện là những bài học đạo đức, dạy làm người. Ngoài ra, đầu sách còn có phần giới thiệu tác giả, giúp mình biết thêm về bệnh 'lười' của tác giả, cũng như Pinocchio là tác phẩm duy nhất để đời của ông. Điểm trừ là trong các câu nói của nhân vật có xen lẫn các ghi chú thường không có ngắt phù hợp mà lại để thẳng thành một hàng, hệt như lời của tác giả biến thành lời của nhân vật. Và có một chỗ đánh máy sai, mình không nhớ rõ trang nào, nhưng chỉ có một thôi.

Nhìn chung, quyển sách đạt 9/10 điểm đối với mình.
5
411530
2016-02-03 01:35:33
--------------------------
358244
5078
Cách xây dựng tuyến nhân vật cùng cốt truyện vô cùng hấp dẫn và lại giàu ý nghĩa. Có thể nói những tình tiết trong câu truyện tuy không có thật, nhưng nếu suy ngẫm kỹ thì nó vô cùng gần gũi với đời sống quanh ta. Từ câu chuyện bác thợ mộc già neo đơn, với bàn tay tài hoa đã tạo ra chú người gỗ Pinocchio và vì tấm lòng cùng sự mong mỏi của bác, mà cô tiên đã giúp đỡ biến cậu bé người gỗ từ vô tri vô giác mà trở thành con. Ấy chẳng phải là vì khi làm một điều gì đó, nếu bạn đặt hết tâm huyết và tình cảm vào, thì những cái đó sẽ "ngấm" vào sản phẩm và sẽ khiến cho nó trở nên vô giá. Và cậu bé người gỗ, vì chỉ là một chú bé gỗ có hoạt động và lời nói, chưa thực sự là con người, nên những việc chú làm chỉ là cảm tính, không thể nói rằng chú vì hư mà như vậy. Ai nói được rằng chính bản thân chúng ta, những con người hoàn thiện, đôi khi cũng không thể phân biệt thưc - hư, cũng như chú bé kia, làm sao biết được nên nghe theo chú dế lương tri của mình, hay những lời hoa mỹ bóng bẩy của những kẻ khác... Và còn rất rất nhiều tình tiết thú vị mà mang tính giáo dục sâu sắc khác trong câu chuyện này mà bạn có thể khám phá.
4
1060347
2015-12-25 00:09:50
--------------------------
358092
5078
: Đây là một cuốn sách đẹp về cả hình thức lẫn nội dung tôi đã được nghe mẹ kể về cậu bé Pinocchio Và Cô Tiên Xanh Tốt Bụng Tuy nhiên qua cuốn truyện này được biết thêm về chú Dế Biết Nói . Cuốn truyện này đã cho tôi biết thêm một bài học vô cùng ý nghĩa những đứa trẻ không biết vâng lời sẽ chẳng có cơ may nào thành công trong cuộc đời.Qua cách kể chuyện tự nhiên hấp dẫn của Carlo Collodi câu chuyện về cuộc đời của chú bé Pinocchio được hiện lên một cách tự nhiên sinh
 động đi sâu vào lòng người đọc. Về phần hình thức thì cuốn chuyện này thật sự tuyệt vời với bia chuyện cứng cáp và chắc chắn màu sắc hài hòa trang nhã kết hợp với những hình vẽ sinh động theo giọng kể của câu chuyện. Đây thực sự là một cuốn sách đáng đọc cho tuổi thơ.Điểm 9,5/10.
4
1074693
2015-12-24 19:21:00
--------------------------
337083
5078
Lời khen đầu tiên của mình là sách quá đẹp, khổ to, bìa cứng, giấy láng mịn, font chữ rõ ràng, tranh minh họa nước ngoài cực kỳ sống động. Song ngược lại sách cũng quá nặng, cầm muốn rụng tay luôn. Theo mình có lẽ Nhã Nam không nên làm nhiều ấn phẩm kiểu này, rất cồng kềnh, khó xếp lên giá sách và khi cầm sách đọc cũng mệt. Nội dung "Những cuộc phiêu lưu của Pinochio" hẳn không ít bạn đọc quá nắm rõ, bởi tác phẩm đã được chuyển thành phim và phim hoạt hình, nổi tiếng suốt nhiều thập kỷ qua. Câu chuyện thú vị về chú bé người gỗ không nghe lời, thế là chú vướng vào bao cuộc phiêu lưu nguy hiểm. Truyện không chỉ mang đến những giờ phút thư giãn cho các em nhỏ và cả người lớn mà còn truyền tải vào đó thông điệp giáo dục ý nghĩa.
4
6502
2015-11-13 15:49:18
--------------------------
306289
5078
Câu chuyện kinh điển về Pinocchio không chỉ hấp dẫn bởi những tình tiết vừa ly kỳ, vừa đan xen cổ tích mà còn bởi những bài học và tính nhân văn xuyên suốt. Đó là tình yêu vô bờ bến và vô điều kiện của cha mẹ đối với con cái, bất kể đứa con ấy đã phạm lỗi. Đó là những hậu quả đau khổ cuộc đời trả cho một đứa trẻ không vâng lời cha mẹ mà tin vào những mật ngọt, cám dỗ. Nhưng đó cũng là vòng tay yêu thương dang rộng đón những đứa con biết hối lỗi, biết sửa sai trở về. Không chỉ cho thiếu nhi, cuốn sách còn giành cho người lớn - những người là con của cha mẹ mình và là cha mẹ của con mình- suy ngẫm. Cuốn sách trình bày đẹp, từ bìa cho đến chữ, giấy và tranh minh họa. Tuy nhiên, mình vẫn muốn có một phiên bản đẹp nhưng với chất liệu bìa và giấy nhẹ hơn để con mình có thể dễ dàng mang đi mang lại đọc những lúc rỗi rãi hơn.
5
135645
2015-09-17 14:45:27
--------------------------
278522
5078
Cực đẹp! Cái này là nhận xét về hình thức. Thực sự cho dù giá truyện hơi cao nhưng chỉ nhận sách là cảm thấy xứng đáng. Sách to (khổ A4), bìa cứng, có hình minh họa rất đẹp. Bên trong sách sử dụng giấy láng bóng, cứng. Hình minh họa sinh động, màu khối hợp lý, tạo ấn tượng với không chỉ trẻ con mà cả người lớn. 
Về phần nội dung thì mình đã từng đọc truyện này rồi nên cũng không có gì bất ngờ lắm. Điểm cộng cho cuốn này là cũng có mở rộng thêm 1 số tình tiết ly kỳ.
Nói chung cuốn này rất thích hợp mua cho các bé vì chỉ riêng phần hình thức đã vô cùng thu hút rồi. Người lớn như mình thì mua để sưu tập ^^
4
51054
2015-08-25 22:31:41
--------------------------
273552
5078
Quyển sách khổ to, dài và khá ấn tượng. Hình ảnh minh họa đẹp, phối màu hợp lý, sinh động, phù hợp tạo cảm giác thích thú xen lẫn câu chuyện thú vị về cuộc phiêu lưu của chú bé người gỗ Pinocchio sẽ khiến các bé thích mê cho mà xem. Trãi qua biết bao nhiêu rắc rối, nguy hiểm và cả thú vị, cuối cùng chú bé Pinocchio của chúng ta cũng đã trở thành một chú bé người thật với nhiều bài học đáng quý. Và các bé cũng sẽ hiểu được rằng thành công sẽ đến với những ai biết vâng lời. Rất thú vị. Cám ơn tiki.
5
110777
2015-08-21 08:57:02
--------------------------
256708
5078
Lúc đầu mình đặt mua không nghĩ rằng nó sẽ ... to đến thế! Khổ sách rất to, như mấy cuốn truyện ngụ ngôn cho con nít ấy. Giấy láng cứng, bóng, hình minh họa cũng rất đẹp. Em mình khá thích quyển sách này. Bạn nào mua đọc cho bé thì rất hay đó nhé. Có mấy bài học trong đó mình "nhá" cu cậu cũng được, như phải nghe lời ba mẹ, hiếu thảo, không nói dối, không được đánh nhau với bạn bè,... là một tác phẩm kinh điển cho mọi đứa trẻ, thật đáng để mọi người cho vào bộ sưu tập của chính mình.
5
513433
2015-08-07 08:48:30
--------------------------
255752
5078
Tôi mua cuốn sách này khi nghe giới thiệu là truyện thiếu nhi dành cho người lớn. Nhưng sau khi đọc thì tôi thấy nó đúng là dành cho thiếu nhi. Quá nhiều chi tiết lặp lại   kể về việc mắc lỗi của Pinochio và sự tha thứ của những bậc đại diện cho cha mẹ bởi tình yêu vô hạn của họ dành cho con cái. Nên đọc khá là nhàm chán và tự đặt cho mình một câu hỏi là liệu cha mẹ trong truyện có quá chiều con hay không khi đã đáp ứng mọi yêu cầu của đứa trẻ.
3
140053
2015-08-06 12:36:55
--------------------------
250664
5078
 Mình rất thích những tranh minh họa của cuốn sách này, truyện trẻ con í, những hình ảnh có cách phối màu giản dị nhưng hài hòa tạo được ấn tượng thẩm mĩ sâu sắc. Nội dung truyện cũng được mở rộng và thêm vào nhiều chi tiết ly kì, hấp dẫn, cảm tưởng như cầm lên đọc rồi thì khó lòng nào mà bỏ cuốn sách xuống được. Chất liệu giầy bên trong là giấy bóng mịn và sáng lắm các bạn nha nên không phải lăn tăn liệu là 96k có đắt hay không đâu. mình rất hài lòng về cuốn sách này.
4
465834
2015-08-02 10:41:54
--------------------------
215604
5078
Mình thích sưu tầm những truyện thiếu nhi đẹp về cả nội dung và hình thức thế nên mình đã không thể cưỡng lại sức hấp dẫn của cuốn sách này. Nhã Nam in sách cực đẹp, giấy bóng dày dặn, minh hoạ màu, bìa cứng vừa bền vừa sang, tiếc là không có dây đánh dấu trang sách. Mong Nhã Nam xuất bản nhiều cuốn thiếu nhi long lanh như thế này ^^.

Tuy giá sách hơi cao so với những bản Pinocchio khác nhưng chất lượng thì tuyệt vời, rất đáng để sưu tầm cho những ai yêu sách hoặc muốn mua cho con em đọc.
4
49203
2015-06-26 15:23:48
--------------------------
212028
5078
Phải nói rằng, câu chuyện về chú bé Pinocchio rất quen thuộc với mọi người. 
Sau đây là ý kiến của mình về tác phẩm này:                                                             
        - Ưu điểm:                                                                                                           
   + Bìa cứng, đẹp                                                                                                       
   + Chữ to, rõ ràng                                                                                                      
   + Giấy bóng + cứng                                                                                              
   + Nội dung khá hay                                                                                              
   + Khổ to                                                                                                                
        - Nhược điểm
( theo mình thì không có)  :))
Ngoài ra, còn có hình ảnh phụ họa nữa (nhìn đẹp lắm). Một số chi tiết trong sách không hề có trong phim. Câu chuyện người gỗ chất chứa nhiều tình nhân văn. Nói tóm lại, mình rất thích sản phẩm này!!!
5
526577
2015-06-21 16:04:24
--------------------------
200648
5078
Mình nghĩ câu chuyện về chú bé Pinocchio thì ai cũng biết. Phải nói rằng đây là một cuốn sách hay dành cho thiếu nhi. Các chi tiết trong sách không hề có trong phim, khi đọc thì rất cuốn hút. Bìa đẹp, một số trang còn có hình ảnh phụ họa nữa (được in bằng màu). Ngoài ra, cuốn sách còn có khổ to, chữ to rõ ràng, giấy bóng + cứng, nội dung khá hay. Câu chuyện về chú bé người gỗ còn chất chứa nhiều bài học. Nếu có ai băn khoăn không biết tặng gì cho ngày sinh nhật của những đứa trẻ thì mình nghĩ hãy tặng cuốn sách này cho chúng vì đó có thể là một món quà sinh nhật tuyệt vời!!! :)
5
616880
2015-05-25 17:36:10
--------------------------
192564
5078
Về hình thức, mình cực kì ưng ý: bìa cứng khổ lớn, giấy bóng + in màu làm cho quyển sách lung linh vô cùng. Còn về nội dung, có lẽ đối với các fan Disney sẽ nghĩ là nó quá quen thuộc rồi, nhưng đọc truyện mới thấy có những chi tiết không hề có trên phim, những chi tiết đó bắt đầu ngay từ đầu truyện tạo cho người đọc cảm giác vừa quen thuộc vừa lạ lẫm và rất hứng thú đọc tiếp xem còn chi tiết nào khác biệt nữa hay không 
4
135436
2015-05-04 23:23:55
--------------------------
191403
5078
Trước hết nói về hình thức thì cuốn sách này được thiết kế rất đẹp, rất phù hợp cho lứa tuổi thiếu nhi, bìa tốt, khổ sách hơi to nhưng từng trang giấy lại chất lượng sáng bóng và có nhiều trang được minh họa bằng hình màu. Nội dung hay, phù hợp cho cả người lớn lẫn trẻ nhỏ vì câu chuyện về cậu bé người gỗ vốn nổi tiếng từ trước đến nay, mang lại nhiều bài học sâu sắc trong cách sống. 
Đây là một tác phẩm hay và giá trị, là cuốn sách mà các bậc phụ huynh nên mua để cho các em thiếu nhi được đọc và học hỏi nhiều điều hay từ cuốn sách.
4
41370
2015-05-01 17:49:56
--------------------------
191263
5078
Từ lúc còn học tiểu học tôi đã được xem qua bộ phim hoạt hình cùng tên với tác phẩm này: " Cậu bé người gỗ - Pinocchio" và nó đã để lại cho tôi một bài học sâu sắc và sau đó tôi đã tìm đến để đọc tác phẩm này, qua lời văn của tác giả, tôi thấy mình như đang cùng cậu bé Pinocchio trải qua những cuộc phiêu lưu đầy thú vị và thử thách đó. Sống trên đời là phải trung thực, nếu bạn nói dối mũi của bạn sẽ dài ngoằng ra, nếu bạn nói dối sẽ chẳng còn ai tin tưởng bạn nữa, đó là bài học sâu sắc mà khi còn là một cô bé con tôi đã được học. Đây là một tác phẩm rất hay và đầy ý nghĩa. Mong rằng sau này Nhã Nam sẽ còn mang lại cho các độc giả những tác phẩm hay như thế này nữa.
5
434144
2015-05-01 12:20:26
--------------------------
173448
5078
Tôi biết Pinocchio qua phim hoạt hình lúc nhỏ, đâu ngờ cũng có sách.
Lần trước tôi có ra nhà sách xem thử, quả thật bìa sách rất đẹp đúng với lời mọi người nói, mang đậm phong cách tác phẩm cổ điển.
Carlo quả thật là một tài năng khi đã tạo ra một nhân vật được hàng triệu người trên thế giới yêu mến như Pinocchio, câu chuyện ông kể cứ lôi cuốn mình theo từ khi đọc những trang đầu tiên.
Một quyển sách dành cho thiếu nhi vô cùng đặc sắc, nhẹ nhàng, lôi cuốn và giàu tính nhân văn.
Bìa của cuốn sách này rất đẹp, bìa cứng trông rất chất. Chưa hết, sách còn được in bằng loại giấy Couche trông rất bắt mắt và sáng.
Có điều tôi chưa hài lòng là sách có giá khá đắt so với mặt bằng chung, trọng lượng rất nặng, cầm đọc sẽ gây mỏi nên các bạn nên để sách trên bàn đọc, với lại sách rất to nên sẽ bất tiện khi mang theo di chuyển.
Nhưng đối với những người yêu thích văn học thiếu nhi thì đây là lựa chọn tốt và cần có trên kệ sách.
5
542401
2015-03-26 11:03:17
--------------------------
172512
5078
Ngày bé, mình chỉ biết Pinochio qua hoạt hình và rất yêu thích nhân vật này. Vì vậy khi cuốn sách này được bán trên tiki, mình không chần chừ mà mua ngay mặc dù giá còn khá cao. Bìa sách rất đẹp và cứng , có chút gì đó cổ điển .
Câu chuyện kể về chú bé người gỗ  Pinochino với những cuộc phiêu lưu vô cùng thú vị nhưng cũng không kém phần rắc rối. Để rồi kết lại, chú bé người gỗ năm xưa nay đã trở thành một cậu bé đã trở thành một cậu bé qua bàn tay mầu nhiệm của một cô tiên .
Truyện dạy cho ta triết lí sống trên đời rằng: '' Hãy luôn trung thực ''
Cám ơn Nhã Nam và Tiki đã đưa cuốn sách này đến với mình .
5
359810
2015-03-24 09:45:28
--------------------------
166654
5078
Mình chỉ mới biết đến chú Pinochio qua phim hoạt hình, và cũng khá tò mò khi thấy ra sách. Nhìn chung đây là một câu chuyện hay, nhẹ nhàng, giàu tính nhân văn và ý nghĩa, bìa sách cũng được thiết kế đẹp nữa (nhưng mình thấy nó hơi hao hao giống bìa Không gia đình của Đinh Tị) Giá thành quyển sách có hơi mắc, mặc dù chất lượng giấy, bìa rất tốt, và khổ sách cũng hơi to. Nhưng nếu bạn yêu thích văn học thiếu nhi thì có thể mua nó để làm một bản lưu giữ rất tốt đấy.
3
198257
2015-03-13 08:20:14
--------------------------
160629
5078
Trước giờ mình chỉ biết đến Pinocchio qua những phim hoạt hình chứ chưa từng được đọc truyện. Lối kể chuyện cũng khác xa với những tưởng tượng của mình dù cốt truyện không thay đổi. Câu chuyện về một đứa trẻ hư như Pinocchio lại được kể với một giọng văn rất nhẹ nhàng tràn đầy lòng nhân ái. 
Đây là một quyển sách không thể thiếu trong bộ sưu tầm sách thiếu nhi, nhưng cũng phù hợp với một số độc giả lớn tuổi hơn vì những bài học trong ấy không bao giờ quá muộn. Chỉ duy nhất mình không thích ở sách này là cách dịch tên không nhất quán, mình thích những tên trong sách được để nguyên như nguyên bản (nếu cần thì chú thích) hơn là dịch thẳng ra.
5
116202
2015-02-25 08:37:10
--------------------------
159698
5078
Ngày bé mình đã thích thú với bộ phim hoạt hình về chú bé người gỗ Pinocchio. Và nhiều năm sau, khi cầm trên tay cuốn sách gốc và đọc, thì những cảm xúc ấy vẫn không thay đổi. Một câu chuyện diệu kỳ, tràn đầy tình cảm, với màu sắc phiêu lưu ly kỳ, với trí tưởng tượng tuyệt vời của tác giả đã tạo nên cả một thế giới đầy biến đổi, muôn hình vạn trạng. Tác giả cứ để những tình tiết diễn ra thật tự nhiên bằng lời kể nhẹ nhàng, chân tình, chính từ đó những bài học và ý nghĩa sâu sắc được gửi gắm đến độc giả, đặc biệt là các em nhỏ. Một ý tưởng độc đáo, đã trở thành một câu chuyện kinh điển không thể quên.
Bản in bìa cứng mới này rất đẹp và đáng để những bạn yêu sách tìm đọc và lưu giữ.
3
109067
2015-02-18 13:15:20
--------------------------
