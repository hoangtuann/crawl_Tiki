448772
9055
Tôi đã mua đủ 4 bộ sách "Mẹ kể con nghe" từ tiki, những câu chuyện trong sách đơn giản, ít từ ngữ, hình ảnh dễ thương, mỗi câu chuyện có một thông điệp để mẹ có thể hướng cho bé áp dụng trong cuộc sống sau này.
Trang sách khá dày, bé nhỏ cầm chơi cũng khó bị rách, tôi đọc cho bé hàng ngày, lặp lại mỗi 02 lần cho mỗi truyện, bé nghe rất thích thú.
Truyện hay, chỉ là hơi nhỏ, tuy dễ dàng mang theo nhưng nếu khổ lớn hơn xíu nữa thì bé xem hình tốt hơn
4
1170850
2016-06-16 14:57:40
--------------------------
434588
9055
Tôi vừa mua xong bộ sách này, sau khi đọc xong cho nhóc nhà mình thì tôi thấy bộ sách có tính giáo dục cao . Có nhược điểm là hình vẽ chưa giống con vật ngoài đời thật. Bộ sách có sức thu hút đối với bọn trẻ. Mỗi câu chuyện luôn có dòng tổng kết nhưng nếu có thêm những câu hỏi sau mỗi câu chuyện để trẻ tự trả lời sẽ rất tuyệt. Mặc dù đã đọc 1 lần nhưng hầu như tối nào nhóc nhà tôi cũng muốn tôi đọc lại. Tôi khá hài lòng với bộ sách này. 
4
888451
2016-05-23 14:29:33
--------------------------
433602
9055
* Điều thấy hài lòng:
- Mỗi cuốn sách là một câu chuyện nhỏ về những con vật thân thuộc với cách sinh hoạt hàng ngày. Qua đó giáo dục trẻ bỏ đi các tật xấu như: Lười biếng, lấy trộm đồ, vứt đồ chơi lung tung...và rèn luyện các thói quen tốt: tự mặc quần áo, kiên trì, chăm chỉ, lễ phép...vân vân...
- Thiết kế bộ sách độc đáo, tiện lợi với chiếc vòng nhỏ gắn kết 10 cuốn truyện, không lo thất lạc
* Chưa hài lòng:
- Tranh vẽ chưa đẹp, hình ảnh các con vật trong truyện chưa giống thực tế lắm
4
400969
2016-05-21 13:32:05
--------------------------
417408
9055
Bé nhà mình mới hơn 5 tháng thôi. Mình cứ nghĩ là mua tập truyện này đọc cho bé nghe "cho có lệ" chứ bé chưa thẩm thấu được đâu. Nhưng ko ngờ bé rất thích thú khi mình đọc. Vì hình ảnh minh hoạ rất đặc sắc nên bé cứ giành giựt sách với mình. Bé càu cấu sách và thậm chí đưa sách vào miệng luôn
Mình ngưng đọc thì bé cựa quậy và bé cười khi mình tiếp tục đọc
Rõ ràng như các bác sĩ nói, trẻ sơ sinh vẫn có thể nghe và hiểu hết, chỉ là chưa biết nói mà thôi
2
262340
2016-04-16 22:31:50
--------------------------
337140
9055
Mình đã mua trọn 4 bộ sách mẹ kể con nghe. Mình đã đọc hết cả 4 bộ và thấy truyện rất bổ ích để nuôi dạy con. Cuối mỗi câu chuyện luôn có phần kết luận để bé hiểu được ý nghĩa của toàn truyện. Tranh được minh hoạ với màu sắc tươi sáng, bắt mắt, các nhân vật gần gũi giúp thu hút sự chú ý của bé. 
Sách thiết kế kiểu gắn vòng lò xo, vừa không lo thất lạc vừa giúp bé dễ sử dụng khi muốn xem cố định 1 truyện nào đó mà không phải cầm cả tập 10 cuốn trên tay.
Tóm lại là mình rất ưng bộ sách này.
5
204946
2015-11-13 18:04:31
--------------------------
294446
9055
Tôi đã mua trọn bộ 4 tập của bộ sách “Mẹ kể con nghe”. Con trai yêu của tôi đặc biệt thích thú tập truyện “rèn luyện thói quen tốt” này. 10 câu chuyện nhỏ của tập truyện đã giúp con tìm hiểu được nhiều hơn về thế giới xung quanh. Các nhân vật trong truyện là những con vật gần gũi với trẻ nhỏ, qua đó giúp con cảm nhận câu chuyện nhanh hơn.Cuối mỗi câu chuyện luôn có những dòng tổng kết ngắn gọn. Bộ sách này rất phù hợp với con trẻ và tôi thật sự hài lòng về bộ sản phẩm này.
4
445555
2015-09-09 12:40:17
--------------------------
189445
9055
Đây là một trong 4 tập của bộ sách “ Mẹ kể con nghe”. Một tập bao gồm 10 câu chuyện nhỏ. Ở tập “ Rèn luyện thói quen tốt”, có các câu chuyện : khỉ con nghịch ngợm, gấu không lạc đường, tự mặc quần áo, mèo con câu cá, gấu tìm đồ chơi, vẫn còn dùng được, mèo con ăn cá, lợn con xây nhà, hàng rào hoa hồng, hà mã ngủ lười. Cuối mỗi câu chuyện đều có phần tổng kết, qua đó, ba mẹ có thể giúp bé hiểu được bé cần rèn luyện thói quen tốt nào. Sách được thiết kế dưới dạng xâu chuỗi giúp tránh bị thất lạc.
3
15022
2015-04-27 14:10:10
--------------------------
