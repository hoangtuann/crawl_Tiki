354312
4758
Cuốn trình bày bố cục mạnh bạo , cuồng phong , nhanh nhạy biểu cảm sâu sắc của con người , khó hiểu về cảm xúc và suy nghĩ của con người hơn ,tuy đây là tập thơ nổi tiếng nhất nhưng cũng cần có mức độ hiểu biết nhất định mới có thể đưa ra quan điểm tuỳ cá nhân , phong thái biểu đạt kỳ lạ , hơi có hướng cổ xưa hoài niệm về ký ức sâu thẳm , có thể nói đại diện cho thể loại thơ tả thực đầy sự mới lạ , hướng văn xoay chuyển .
3
402468
2015-12-17 17:33:08
--------------------------
169112
4758
Đây là lần đầu mình đọc thơ Bích Khê. Tinh Huyết là tập thơ nổi tiếng nhất của Bích Khê. Nhưng bây giờ mình mới thử đọc và cảm nhận. Thơ ông lạ. Và với một trình độ cảm nhận thơ kém cỏi như mình thì đôi bài mình thấy hơi... khó cảm... nó huyền bí, nó... Nói chung là mình rất khó dể diễn đạt :(.
Mình mua sách vì sách có bìa đẹp, hình minh họa bên ngoài và ruột giấy bên trong rất đẹp. Giấy có chất lượng rất tốt, dày.  Bìa tốt. Có vẻ vì thế mà giá cả tập thơ có đắt đỏ hơn so với những tuyển tập trên thị trường.
4
114793
2015-03-17 18:52:03
--------------------------
