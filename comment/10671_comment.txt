159601
10671
Nếu ai đã từng đọc Mặt Phải chắc chắn sẽ rất tò mò khi đọc Mặt Trái để xem có gì khác hay trái ngược nhau hay không? Và sau khi đọc tôi nhận thấy có khác nhau và trái ngược nhau ở tên sách chứ nội dung thì không có gì đối lập. Mặt trái chủ yếu chỉ ra và hướng dẫn độc giả cách hạn chế những khuyết điểm của bản thân và cách để hoàn thiện bản thân, hoàn thiện nhân cách trong ta. Chắc chắn đây là một quyển sách phát triển bản thân mà ai cũng nên đọc dù có thể đã đọc rất nhiều sách phát triển bản phân khác. Đọc để biết được quan điểm của tác giả Flip Flippen và để biết tại sao Mặt Trái lại lấp đầy góc khuyết.
5
387632
2015-02-17 21:18:49
--------------------------
138619
10671
Mình được đứa bạn thân tặng cuốn sách này vào năm ngoái và mình đã dành gần một tuần để đọc hết được cuốn sách. 
Đây là một cuốn sách rất có ích, nó giúp chúng ta nhìn nhận được cái khuyết điểm của chính bản thân và còn có những biện pháp khắc phục chúng. 
Ngoài ra, sách này còn giúp chúng ta tìm ra được các lợi thế cá nhân để phát huy chúng một cách triệt để và hiệu quả.
Lời văn của tác giả khá là đơn giản và gần gũi, phân tích những khuyết điểm lẫn thế mạnh quá rõ ràng, rõ ràng đến mức đôi lúc mình thấy nó bị nhàm chán.
Nhưng nói tóm lại, cuốn sách này rất đáng đọc, nhất là với những ai đang có ước muốn thay đổi bản thân để trở nên hoàn thiện hơn - đương nhiên là có cả bản thân mình.
4
470237
2014-12-03 21:25:51
--------------------------
76588
10671
Đây là một quyển sách có ích, tôi nói  như vậy bởi vì nó đã giúp tôi nhìn lại chính bản thân mình, khắc phục những điểm cần hạn chế và giúp phát triển những điểm tôi làm tốt, đúng như lời tựa đề '' nhìn lại góc khuyết'' . Lời văn của tác giả chân thật mà giản dị, gần gũi, logic, phân tích tỉ mỉ những hạn chế khiến cho người đọc cũng từ đó mà hình dung rõ ràng. Đây là một quyển sách rất có ý nghĩa, giúp người đọc hoàn thiện lại bản thân mình, nhìn lại những khiếm khuyết lỗi sai của bản thân rồi từ đó sửa chữa.
4
57869
2013-05-23 19:33:45
--------------------------
76587
10671
Đây là một quyển sách có ích, tôi nói  như vậy bởi vì nó đã giúp tôi nhìn lại chính bản thân mình, khắc phục những điểm cần hạn chế và giúp phát triển những điểm tôi làm tốt, đúng như lời tựa đề '' nhìn lại góc khuyết'' . Lời văn của tác giả chân thật mà giản dị, gần gũi, logic, phân tích tỉ mỉ những hạn chế khiến cho người đọc cũng từ đó mà hình dung rõ ràng. Đây là một quyển sách rất có ý nghĩa, giúp người đọc hoàn thiện lại bản thân mình, nhìn lại những khiếm khuyết lỗi sai của bản thân rồi từ đó sửa chữa.
4
57869
2013-05-23 19:33:45
--------------------------
66240
10671
Nhiều khi tôi cảm thấy tự tin, làm nhiều việc, và rồi...cảm thấy việc đó là vô ích. Có những lúc tôi tự ti không dám thể hiện chính mình! Không phải tôi bôc đồng, nông nổi! Mà chính vì tôi chưa hiểu chính mình, chưa biết những góc khuyết trong mình, không biết biến mặt trái thành điểm mạnh để thành công!
Đọc cuốn sách của Flip Flippen, tôi đã hiểu ra mỗi người đều có những khả năng, năng lực của bản thân, nhưng để thể hiện hết những cái đó, ta phải tự tin thể hiện và hoàn thiện mình. 
Mặt Trái - Lấp Đầy Góc Khuyết là cuốn sách nói ra những hạn chế và đề ra cách khắc phục, giúp ta lạc quan, tự tin trên đường đời để từ đó THÀNH CÔNG!
5
105456
2013-03-30 09:54:24
--------------------------
61636
10671
Mình rất thích cuốn sách này ở tính chân thực và gần gũi của nó. Tác giả không đưa đến những tuyên bố hùng hồn, mạnh mẽ, không tô hồng cuộc sống mà chỉ đơn giản diễn tả nó như vốn có. Ngôn ngữ trong cuốn sách cũng nhẹ nhàng, dễ hiểu và dễ cảm nhận, từ đó giúp cho những kiến thức được truyển tải dễ dàng hơn. Tác giả đã chỉ ra những hạn chế của mỗi chúng ta, cách khắc phục chúng để làm cho cuộc sống và công việc trở nên tốt đẹp hơn. Những bài học được đem đến ắt sẽ là một hành trang quý giá cho mỗi chúng ta trên đường đời, vì nó ảnh hưởng rất nhiều đến mọi mặt của đời sống.
Đây là một cuốn sách nên đọc.
4
20073
2013-03-03 10:57:27
--------------------------
60247
10671
Mỗi chúng ta không ai là hoàn hảo cả, dù ít hay nhiều trong chúng ta đều có các hạn chế nhất định. Nó không chỉ kìm hãm con đường tới thành công của chúng ta mà còn làm cho chúng ta cảm thấy mình không hoàn hảo so với người khác. Cuốn sách chỉ rõ một cách cụ thể 10 hạn chế cá nhân mà con người hay mắc phải, thông qua đó giúp chúng ta cách thức để khắc phục nó, giúp ta có 1 cái nhìn lạc quan hơn và tôi tin chắc rằng nếu bạn rèn luyện giống với những gì mà cuốn sách nói thì con đường thành công của bạn ở mọi lĩnh vực đang rộng mở. Một cuốn sách hay và hữu ích !
4
63765
2013-02-21 17:37:42
--------------------------
45951
10671
Điều gì đã quyết định mức độ thành công của bạn, '' bạn sẽ trả lời ra sao''? Đó là tài nghệ của bạn, các kĩ năng hay tấm bằng đại học,
Ngoài tài nghệ, năng lực , tính cách, hay trình độ học vấn còn có yếu tố nào khác góp phần quyết định mức độ thành công của bạn trong cuộc sống. Phải chăng chính những hạn chế của bạn mới chính là nguyên nhân chủ yếu quyết định mức độ thàng công của bạn.
Một quyển sách giúp bạn phát hiện ra những hạn chế cá nhân dựa trên những câu hỏi và những biện pháp đã được nghiên cứu tìm tòi tỉ mĩ của 1 bác sĩ tâm lý tại tiểu bang texas( hoa kì). Dựa trên 5 nguyên lý và 10 hạn chế cá nhân nguy hiểm những. Tác giả đã giúp chúng ta phát hiện ra những hạn chế cá nhân kìm hãm sự phát triển cao hơn nữa của mỗi con người.Đưa ra những giải pháp khắc phục những hạn chế cá nhân đó. Cùng với lối viết không hoa mỹ. súc tích, rõ ràng lồng ghép những câu chuyện có thật, câu chuyện của những con người nổi tiếng trên mọi lĩnh vực kinh tế, xã hội, văn hóa,  tác giả đã làm cho tôi càng đọc càng thấy hứng thú và khao khát được biết những hạn chế của mình là gì và tìm cách khắc phục nó. Một quyển sách nên đọc.
3
64002
2012-11-11 16:03:33
--------------------------
