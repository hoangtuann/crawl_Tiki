424615
12285
Quyển sách này rất rất hữu ích đối với mình .Khi mua quyển này các bạn cần kèm theo một quyển ngữ pháp nữa .Qua mỗi bài exercises phần bài tập rất khác nhau giúp cho mình vừa học vừa làm rất tốt cho việc tiếng anh sau này .Sách phân chia các phần rất hợp lí.Nói chung sách cũng rất thú vị và hay nữa .Nhưng có 2 điểm buồn là sách TIKI đưa cho mình nó bị rách ở phần đầu .Và phần giải cũng có vài câu bị sai làm mình lẫn lộn .Đây là 2 điểm rất đáng tiếc mong TIKI chỉnh sửa lại .
3
599519
2016-05-02 11:59:12
--------------------------
402581
12285
Mình rất thích làm bài tập tiếng Anh để biết mình sai ở đâu, hổng kiến thức chỗ nào nên mình đã mua cuốn Bài Tập Đặt Câu Tiếng Anh  này về làm thử. Bìa sách rất đẹp, giấy cũng tốt, và chất lượng in ổn. Sách được in màu không chỉ đơn thuần chữ đen. Bài tập có số lượng nhiều cả trắc nghiệm lẫn tự luận. Với 3 dạng bài chính là viết lại câu với các từ đã cho và chọn đáp án sắp xếp câu đúng. Đáp án cũng đầy đủ ngay sau mỗi phần. Nói chung là sách hay !
5
854660
2016-03-22 16:42:11
--------------------------
361183
12285
Việc học tiếng anh đã rất khó khăn rồi việc đặt câu tiếng anh còn khó khắn hơn nữa, chắc chắn các bạn học tiếng anh rất lúc túng trong việc đặt câu hoi, mình cũng từng như vậy. Nhưng khi có cuốn sách này trên tay mình đã không còn lúng túng nữa, mình đã tu luyện theo cuốn sách này rất nhiều và đã thành công. Với cuốn sách này việc đặt câu hỏi trở nên dễ dàng hơn vì bài tập được chia làm 3 phân 
- Đặt câu với các từ gợi ý
- Viết thứ
- Chọn đáp án đúng
Với ba phần như vậy mà bây giờ mình đã đễ dàng đặt câu hỏi hơn.

5
1008047
2015-12-30 15:47:18
--------------------------
359626
12285
Trước đây tôi rất lúng tung trong những tình huống đặt câu tiếng anh, từ khi có cuốn sách này tôi đã rất tự tin về đặt câu tiếng anh của mình. Sách được chia ra các phần rõ ràng, cấu trúc phong phú, nội dung rất trực quan giúp các tôi không bị nhàm chán khi học. Cuốn sách này cũng rất bổ ích với các bạn trẻ đang ôn luyện môn tiếng Anh cho kì thi tốt nghiệp và đại học theo cách thức mới của Bộ giáo dục. Sách hay nhưng với giá hợp lý nên rất đáng để bổ sung cho công việc học hành, và mình đã chọn cuốn sách này.
5
1012731
2015-12-27 15:55:27
--------------------------
