397819
4908
Cũng giống như mẹ mình - Ngọc Cẩn, Hoắc Vân Ca đã từ từ tìm được bến đỗ thứ hai của lòng mình. Không phải là Mạnh Giác năm xưa mà là Lưu Phất Lăng - Hoàng đế Đại Hán. Chàng sẵn sàng bỏ lại cả ngôi vị mà người người ao ước chỉ để được nắm tay Vân Ca đi khắp thế gian, thưởng ngoạn mọi cảnh đẹp. Một chàng trai hết lòng yêu thương nàng như thế; sẵn sàng bỏ lại giấc mơ thiên trường địa cửu như thế, Vân Ca thật hạnh phúc xiết bao! Nhưng đời không như mong ước, cuối cùng kết thúc vẫn bi ai!
5
1103537
2016-03-15 14:33:11
--------------------------
365423
4908
Truyện của Đồng hoa lôi cuốn người xem vào những giai thoại buồn của nhiều cá nhân , chưa làm mất đi các yếu tố bản chất mà cũng lãng mạn , chiều hướng sâu xa vào vấn đề khắc họa bản tình ca cổ hủ nhưng cũng có gợi tả bất cứ điều lan man của sự tính toán rồi cũng có cả thất vọng khi không như ý mình mà tan vỡ , hay nhưng cũng bắt ta phải trải qua hàng điều khiến giật nảy mình lên mà vẫn bớt chút thời gian , hoài niệm về sự mất mát .
4
402468
2016-01-08 00:06:34
--------------------------
356493
4908
Vân Trung Ca quả không hổ là tuyệt phẩm của Đồng Hoa, quả là hay đến không còn gì để ca tụng. Thích nhất là tập 2 với sự xuất hiện của Lưu Phất Lăng hào hoa chung tình. Rất may là Vân Ca đã dứt khoát bỏ tối theo sáng, dù mình có hơi tội nghiệp Mạnh Giác một chút. Đoạn cuối sách buồn quá. Nếu theo lịch sử thì Lưu Phất Lăng phải chết không nghi ngờ.
Sách bìa cứng mà Tiki cũng bọc rất đẹp và cẩn thận. Trông vừa sang vừa tiện bảo quản. Dịch giả cũng dịch hay và mượt nữa.
5
167283
2015-12-21 22:50:01
--------------------------
290765
4908
Truyện của Đồng Hoa thì hay nhưng buồn vô cùng, Vân Trung Ca cũng thế! Vân Ca ban đầu nhận nhầm người, rồi lại trải qua bao chuyện mới gặp được Lăng ca ca, nhưng cuối cùng thì Lăng ca ca lại mất khá sớm! Nhưng mình không thích nhân vật Vân Ca lắm, bởi vì thấy hơi "đa tình" , đọc xong cũng không dám chắc Vân Ca yêu ai, Mạnh Giác hay Lưu Phất Lăng??? Truyện này cứ như là phũ và buồn kéo theo hình tròn, đôi khi thấy không thích Vân Ca vì cảm thấy tại Vân Ca mà nhiều mối tình cũng tan vỡ, làm cuộc sống của bản thân cũng như những người xung quanh cũng chẳng mấy sung sướng. 
Còn về sách, bìa rất đẹp, giấy xốp, nhẹ cầm vô cùng thích tay. Cầm đọc lâu tính ra cũng không thấy mỏi lắm, bìa cứng đẹp vô cùng, nhìn mấy em này ở trong nhà cứ muốn cầm lên đọc hoài! 
5
747082
2015-09-05 19:23:18
--------------------------
264131
4908
Truyện của Đồng Hoa thì chúng ta không cần phải bàn cãi. Quá tuyệt vời. Khắc họa hình ảnh nhân vật sống động biết bao. Chuyện tình của Vân và cả vị hoàng đế yểu mệnh làm cho mình rơi nước mắt không biết bao nhiêu mà kể. Tình yêu khắc cốt ghi tâm đến vậy mà lại vì những tranh đấu, những mưu mô, xảo quyệt bóp chết 1 cách không tiếc thương. Thương cho sự chờ đợi của vị hoàng đế cô đon, thương cho yêu mà sinh ly sử biệt. Và sẽ là 1 thiếu thốn nếu không nhắc đến Mạnh Giác. Là 1 người mâu thuẫn điển hình nên đến tận cuối truyện ta vẫn thương hơn là giận con người ấy. Bìa đẹp, giấy tốt, rất thích!
5
742205
2015-08-13 00:53:24
--------------------------
160169
4908
Ở phần 2 này, Vân Ca cũng đã được gặp lại Lưu Phất Lăng, nhưng phải mất một thời gian để nàng quên đi Mạnh Giác, thực sự muốn ở bên Phất Lăng từ tận đáy lòng. Tình yêu của họ là một tình yêu đẹp, nhưng Đồng Hoa đã quyện vào đó nhiều sầu bi. Cuối cùng họ cũng không thể ở bên nhau cho đến cuối đời được. Nhưng tôi thích cái cách mà hai người họ đón nhận điều đó. Họ không đau đớn, buồn bã mà cùng tận hưởng hạnh phúc từng giây từng phút một. Chẳng phải tình đẹp nhất vẫn là tình còn dang dở hay sao?
Và trong tập 2, dù Đồng Hoa cũng để Vân Ca chuyển tình cảm từ Mạnh Giác sang Phất Lăng, giống như Kim Ngọc từ yêu Mạnh Cửu sang Hoắc Khử Bệnh, nhưng tác giả đã có tiến bộ hơn trước. Tâm lý của Vân Ca được khắc học rất hợp lý. Còn tâm lý của Mạnh Giác thì vẫn có đoạn hơi gượng ép, nhưng cũng chỉ là lỗi nhỏ chứ không làm mất đi cái hay của truyện.
5
123326
2015-02-22 19:51:58
--------------------------
156620
4908
Bao giờ cũng thế, truyện của Đồng Hoa luôn khiến mình vừa yêu vừa hận, yêu vì giọng văn buồn man mác mà lắng sâu của cô, vì cốt truyện cuốn hút lại giàu giá trị nhân văn của cô, hận vì các nhân vật của cô chẳng mấy khi có được một kết cục tốt đẹp. "Vân Trung Ca" cũng thế. Đọc truyện, mình thương Vân Ca biết bao, một cô gái ngây thơ, trong sáng, lương thiện nhường ấy, chỉ vì một lời hứa thuở thiếu thời với Phất Lăng mà bị cuốn vào vòng xoáy tranh giành quyền lực không cách nào thoát được, đến tận cuối cùng vẫn chẳng thể tìm được hạnh phúc. Mình cũng vô cùng cảm phục nhân vật Hứa Bình Quân, cảm phục và cảm động trước tình yêu và lòng hi sinh cao cả của cô ấy.
5
393748
2015-02-05 15:42:58
--------------------------
