563637
6626
Mình vừa đọc hết 1 chương trong thuật xử thế những gì mình cảm nhận dc là sự điếm thúi, lấp liếm , không trung thực và cố gắng bao biện cho những khó khăn gặp phải . Những câu thành ngữ đã có từ 2000 năm trước của bọn trung quần què rất quen thuộc vì ai đã dc đi học đều đã nghe . Làm theo sách nói chẳng khác gì kẻ khờ .
4
1861990
2017-04-04 09:48:54
--------------------------
401137
6626
Tôi mua đủ bộ "Tứ thư lãnh đạo" và đây là cuốn đầu tiên tôi đọc. "Tiên học lễ - Hậu học văn". Xử thế luôn là điều cần thiết đầu tiên để có thể nổi bật trong cuộc sống xã hội hiện nay. Một người biết cách đôi nhân xử thế đúng đắn thì đã là thành công rồi. Cuốn sách này cũng đã thành công trong việc dẫn dắn người đọc đi theo những điều đúng đắn. Nội dung rất dễ đọc, không mang tính lí thuyết, chia thành từng phần nhỏ, đọc đến đâu hiểu đến đấy, áp dụng ngay lập tức. Rất thích.
5
211114
2016-03-20 10:59:41
--------------------------
324082
6626
Ấn tượng của mình đối với cuốn sách này nói riêng và bộ Tứ Thư Lãnh Đạo nói chung đó là rất tuyệt vời. Mình khoái mê tơi bìa sách ngay từ cái nhìn đâu tiên. Khi đọc thử vài trang của cuốn sách này ở nhà sách thì mình quyết định sẽ đặt mua một bộ trên tiki, tất nhiên là để tiết kiệm khá tiền. 
Thuật xử thế nằm trong combo mà mình mua. Tất nhiên hay dở đề là do quan điểm cá nhân của mỗi người, nhưng đối với mình đây là một cuốn sách đáng để đọc. Hai điều cơ bản của người đứng đầu nhà nước ngày xưa đó chính là việc đối nhân và xử thế. Điều đó chứng tỏ xử thế là một điều căn bản cần có của người có nguyện vọng làm lãnh đạo. Cuốn sách chắt lọc những tinh túy từ nhiều nguồn và tổng hợp lại trong khoảng 500 trang sách. 
Tất nhiên, nếu bạn muốn có một nền tảng lý thuyết để sau này có thể vận dụng trong nhiều tình huống thực tiễn thì cuốn sách này khá đáng đọc. 
4
759572
2015-10-20 13:06:44
--------------------------
299287
6626
Thuật Xử Thế, nằm trong bộ Tứ Thư Lãnh Đạo gồm 4 quyển cung cấp những kiến thức liên quan đến các kỹ năng cần thiết của một nhà lãnh đạo. Ấn tượng đầu tiên của quyển sách chính là bìa sách được trình bày đẹp mắt, dày. Chất lượng giấy sách rất tốt, chữ in rõ ràng và dễ đọc. Tuy nhiên, như điểm không thích mà mình đã đề cập trong Thuật Lãnh Đạo, những quyển sách dày như thế này nếu có thêm  dải lụa để đánh dấu trang thì sẽ hoàn hảo hơn. Đối với nội dung sách, các kiến thức chủ yếu tập trung vào các kiến thức liên quan đến xử thế như: cách giao tiếp, ứng xử với mọi người ở nhiều cấp bậc khác nhau; cách ăn mặc phụ hợp với nhiều tình huống....Mặc dù có nhiều kiến thức mình đã được tiếp cận trong nhiều sách khác, nhưng Thuật Xử Thế vẫn giúp ích cho mình nhờ những ví dụ thực tiễn được đề cập, và giúp mình ôn lại những gì trước đó
5
542195
2015-09-13 09:19:47
--------------------------
292696
6626
Tứ Thư Lãnh Đạo - Thuật Xử Thế là 1 trong số 4 quyển sách nằm trong combo Tứ Thư Lãnh Đạo gồm Thuật Xử Thế, Thuật Dụng Ngôn, Thuật Quản trị và Thuật Lãnh đạo. Đã gọi là thuật nghĩa là đây là những kỹ xảo, kỹ năng mà người sử dụng phải thành thạo và nhuần nhuyễn để có thể áp dụng vào mọi tình huống. Sách gồm các bài học về ứng xử, phục trang, nghi lễ, cách giao tiếp, cách ứng xử với các cấp. Góp ý là nên có 4 màu khác nhau cho 4 quyển để dễ phân biệt.


4
465332
2015-09-07 15:55:51
--------------------------
268575
6626
Thuật xử thế là cuốn sách nằm trong combo Tứ thư lãnh đạo, nội dung cuốn sách thể hiện rõ những bí quyết của một nhà lãnh đạo thành công, thông qua cuốn sách kèm theo là những ví dụ cụ thể, rất sinh động giúp chúng ta đọc cuốn sách dễ hiểu và dễ áp dụng vào thực tiễn hơn. Nó đã và đang giúp tôi dần dần nắm được những bí quyết và thay đổi bản thân tốt hơn, thiết nghĩ cuốn sách cũng rất cần cho những con người muốn thành công hơn trong cuộc sống.
4
562396
2015-08-16 16:51:48
--------------------------
239701
6626
Mình may mắn sở hữu đủ bộ tứ thư gồm: Thuật quản trị, Thuật lãnh đạo, Thuật dụng ngôn, Thuật xử thế. Bốn điều cơ bản tự tin làm nên một nhà kinh doanh thành công. Bạn cũng nhận thấy!
Thuật xử thế dạy bạn giao tiếp, lễ nghi, trang phục, cách ứng xử với các cấp, xây dựng uy tín, đối phó sai lầm,.. Vô số kiến thức kinh nghiệm hay, rất cần trong thực tế.
Quyển sách đẹp từ bìa, hình thức, trình bày, hay về nội dung, phải nói rất tuyệt vời như một vật báu gia truyền, tôn vinh vẻ đẹp cả cuốn sách. Xứng đáng giá trị đồng tiền! Tiki luôn mang lại những ưu đãi nhất cho khách hàng!
Rất hài lòng về bộ tứ thư lừng danh nay đã về tủ sách của mình!
Đóng góp: Nếu như thêm dây đỏ book mark đánh dấu sách nữa sẽ rất tuyệt!

5
107985
2015-07-24 01:37:37
--------------------------
226148
6626
Trước đây mình thấy một anh bạn mình tìm mua cuốn này trong bộ Tứ thư lãnh đạo nên mình đọc thử vài trang đầu thì thấy nó rất hay và thú vị nên quyết định mua quyển này ở tiki để đọc trọn vẹn quyển sách . Mình cảm thấy quyển sách rất hay và hữu ích, nói rất là chi tiết giúp ta ngộ ra nhiều điều trong quá trình giao tiếp hàng ngày trong cuộc sống và công việc. Tuy nhiên, mình cũng đồng tình với một bạn là sách k có dây bookmark là một thiếu sót lớn. 
4
642881
2015-07-11 16:04:31
--------------------------
198058
6626
Thuật xử thế dành cho nhà lãnh đạo, một tiêu đề có thể nói là rất hấp dẫn. Mình không biết đây là tiêu đề do tác giả hay do dịch giả đặt. Nhưng mình có phần lớn thất vọng về cuốn sách. Sách chỉ phù hợp cho sinh viên kinh tế năm 1, 2. Hoặc giả do tiêu chuẩn của mình khá khắt khe nên nội dung cuốn sách có phần quá đơn giản đối với mình. 

Có những chương trong sách có vẻ nên bỏ ra, cho vào 1 cuốn sách về cẩm nang thời trang công sở. Có những ví dụ trong sách hoàn toàn nằm trong Đắc Nhân Tâm của Dale Carnegie. Hình thức sách khá đẹp nhưng thiếu dây bookmark là điểm trừ ngay lập tức vì sách cũng khá dày.
3
104777
2015-05-19 03:30:31
--------------------------
159602
6626
"Một cây làm chẳng nên non, ba cây chụm lại nên hòn núi cao" hay "một cánh én không làm nên mùa xuân" đã cho chúng ta thấy một mình bản thân ta không thể tự mình làm tất cả mọi việc mà không cần tới sự giúp đỡ của người khác. Triệu phú tay trắng làm nên chứ không hề có triệu phú tự tay làm nên. Nhưng để người khác sẵn lòng giúp đỡ ta phải biết cách cư xử trong xã hội, trong cách giao thiệp với người khác mà ông cha ta gọi là đối nhân xử thế. Hãy học cách giao thiệp với người khác để thu phục nhân tâm và xa hơn là mọi người sẽ đồng lòng giúp đỡ chúng ta trong mọi việc của cuộc sống qua quyển sách bìa cứng tuyệt đẹp Thuật Xử Thế.
5
387632
2015-02-17 21:26:18
--------------------------
145101
6626
Cuốn sách này nên đọc cuối cùng trong bộ tứ thư lãnh đạo - Có thể nói là tầng cao nhất trong việc hoàn thiện tố chất một người lãnh đạo.
Đọc qua thấy được cuốn sách được cô đọng lại từ rất nhiều tài liệu khác, chắt lọc nhiều tinh hoa và được gom lại một cách đầy đủ. 
Nhìn qua thì cảm thấy thuật dụng ngôn và thuật xử thế nó có nhiều điểm tương đồng, nhưng thực chất thì chúng hoàn toàn khác nhau và bổ sung cho nhau, nếu thiếu một trong hai đều không được.
Đọc, học được toàn bộ 4 quyển sách tứ thư lãnh đạo sẽ trở thành một con người hoàn thiện - Nó không chi dành cho người lãnh đạo trong kinh doanh, mà dành cho tất cả mọi người.
4
74132
2014-12-29 11:03:34
--------------------------
