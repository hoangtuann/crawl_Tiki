375121
6829
Tiểu thuyết lãng mạn và có nhiều biểu cảm cao và khiến bao nhiêu người suy nghĩ rất lớn và làm cho con người trong nhân vật còn thấy được cái tình cảm khiến cảm phục ý chí lớn trong nhân cách của chàng trai làm cho cô gái lay động trái tim mình bởi các điều kiện bỏ đi thay đổi với nhiều thực tế của hành động trong thế giới hiện tại bởi vô số cái bố cục hình tượng dẫn đến điều mà người ngoài nhìn được đều minh xét và phô diễn ra cái đa diện .
4
402468
2016-01-27 19:56:19
--------------------------
373220
6829
Mình thấy series này của Maya Bank không hay lắm nhưng cuốn này thì tạm ổn
Khung cảnh diễn ra câu chuyện là một bãi biển thơ mộng rất lãng mạn và sau đó là Mexico rất kịch tính hồi hộp.
Truyện có đủ các yếu tố, lãng mạn lẫn điều tra tội phạm nhưng không bị quá ở yếu tố nào. Hai nhân vật chính cũng khá hay cuối cùng cũng cùng nhau vượt qua được hoàn cảnh khó khăn và đến với nhau bất chấp cả sự thù hằn giữa hai gia đình, xử lý kết thúc hợp lý.
3
306081
2016-01-23 09:53:18
--------------------------
359892
6829
Mình mua quyển này khoảng được ba tháng nay rồi....Chất lượng giấy, bìa đẹp miễn chê luôn..Vẫn nhớ như in lần đầu cầm quyển này lên đọc và đọc một lèo hơn 13 chương của truyện..:D...Vì truyện đọc rất li kì, hấp dẫn vừa đọc vừa tưởng tượng cảm giác như đang xem một bộ phim của Hollywood vậy...Mình rất thích cách tác giả xây dựng hình tượng Garrett..Trông anh thật mạnh mẽ, dũng cảm mà đầy cuốn hút..Tự nhiên đọc xong truyện mà mình muốn yêu một anh lính trong quân đội để được che chở, bảo vệ quá.:v..Và tình yêu trong truyện cũng rất đẹp...Chỉ ước tác giả cho Sarah "hành ha'' Garrett thêm một ít nữa để anh phải ''dùng chiêu trò'' dỗ dành cô thì tuyệt quá..Dù sao cũng rất rất rất thích quyển truyện này..Bạn nào thích đọc truyện lãng mạn chớ có bỏ qua nha..Haha..:)))
4
420354
2015-12-28 10:35:57
--------------------------
272301
6829
Tôi đã rất hào hứng khi truyện của Maya Banks được xuất bản, mặc dù biết rằng sách của bà có rất nhiều chi tiết được miêu tả rất “HOT” nhưng nội dung truyện thì không thể chê được, bên cạnh một tình yêu son sắt, lãng mạn thì các tình huống trinh thám luôn tạo cho tôi sự hấp dẫn, ly kì. Đối với các nhân vật trong truyện thì khốn nỗi tôi lại khá thích Marcus, anh trai của Sarah, mặc dù anh ta là một nhân vật phản diện nhưng tình yêu thương mà Marcus dành cho em gái mình thật đáng nể, đến khi anh ta chết tôi vẫn có một chút gì đó tiếc nuối và thương cảm cho Sarah khi điểm tựa của cuộc đời cô mất đi. Nhưng thay vào đó tình yêu ngọt ngào mà Garrett dành cho Sarah đã đủ để bù đắp tất cả. 
Truyện là một chuỗi những hành động khá ky kì nhưng lôi cuốn và hợp lý, tôi rất thích hình ảnh về một gia đình ấm cúng, hạnh phúc của Garrett, nơi mà cha mẹ, anh em luôn đoàn kết, hỗ trợ nhau, tôi cũng rất thích cách xây dựng từng nhân vật, đặc biệt là trong nội tâm của Garrett & Sarah, hai người đến với nhau như một điều kì diệu của cuộc sống và tình cảm mà Garrett trao gửi cho cô thật khiến biết bao nhiêu con tim phải thổn thức. Thể loại truyện mới này của Maya Banks thật sự không thể sánh được với dòng lãng mạn lịch sử của bà nhưng truyện vẫn hấp dẫn và lôi kéo được độc giả, series này khá dài và nhiều nhân vật trong truyện này cũng được tác giả dành cho một tác phẩm riêng biệt nói về mình, hy vọng mong manh rằng BV sẽ cho xuất bản hết những cuốn còn lại, nhất là quyển về Steele, nhân vật phụ trong “Trốn chạy và yêu thương”, là trưởng nhóm KGI, một người im lặng, ít nói, đầy bí ẩn nhưng thật sự tài năng.

4
41370
2015-08-19 22:12:00
--------------------------
224296
6829
Mình không bị ấn tượng khi lần đầu nhìn thấy cuốn sách này. Bởi vì thực sự bìa sách không bắt mắt, hơi thô..điều này có thể khiến cho nhiều bạn đọc có thể bỏ qua một câu chuyện hấp dẫn. Mình thích tác giả Maya Banks và lỗi viết của bà. Rất hấp dẫn và thu hút độc giả. Đặc biệt, cuốn sách này không làm mình thất vọng. Cũng như cái tên, cuốn sách là một câu chuyện về "cuộc trốn chạy xen lẫn yêu thương, sợ hãi và vô vàn cảm xúc đan xen. Và tình yêu chính là phép nhiệm màu kỳ diệu nhất". Cốt chuyện không quá mới nhưng với cách viết của bà nên truyện chiếm được nhiều tình cảm của bạn đọc!
4
292005
2015-07-08 14:10:14
--------------------------
222574
6829
Mình vẫn luôn thích phong cách của Maya Banks. Và trong tác phẩm này của bà, mình cũng vẫn thích. Bà có một ngòi bút với kiểu tình yêu lãng mạn nhưng không hề nông cạn, đam mê nhưng không phản cảm. Nó làm người ta tin vào khả năng có thật của 1 loại tình yêu mà ở đó, sự lãng mạn và đam mê có thể sóng đôi. Ở đó, con người ta có thể tìm thấy minh chứng cho cuộc sống cùng người mình yêu với sự say mê, cuồng nhiệt nhưng nó sẽ bình yên và tồn tại mãi mãi. Nhưng yêu tố làm mình không thích ở đây là nhân vật nữ chính. Sarah đã từng bị cha bỏ rơi, sống với cuộc sống mà không ai cho cô tình yêu thương từ khi mẹ chết cho đến khi Marcus tìm thấy cô, đúng. Cô thậm chí cũng đã bị hãm hiếp và nhục nhã, đúng. Nhưng chẳng phải cô đã trải gần hết tuổi thơ với những thứ mà người bình thường thậm chí chưa hề trải qua sao? Sắt qua tôi luyện thì sẽ thành thép. Nhưng Sarah là 1 thanh sắt đã qua tôi luyện nhưng thậm chí trở thành thứ gì đó yếu hơn cả sắt. Cô sợ hãi và cứ mãi chạy trốn. Cô muốn bảo vệ Marcus, người duy nhất cô yêu trên đời, nhưng thậm chí cách để chạy trốn cho ra hồn cô cũng không biết. Một cô gái đã bị đời vùi dập gần như hết cả tuổi thơ sao lại có thể quá mức yếu đuối, quá mức cả tin và quá mức ngây thơ như thế? 
4
258176
2015-07-05 21:36:12
--------------------------
215256
6829
Truyện có ý tưởng không mới nhưng cách viết của tác giả làm cho cốt truyện khá là hấp dẫn. Kịch tính cũng có nhưng theo mình cảm nhận chưa tới cao trào, chắc do bị giới hạn số chữ nên tác giả chỉ viết được thế thôi, nếu như thêm thắt vào chút ít thì sẽ hấp dẫn hơn nhiếu. Ví dụ như cái đoạn cuối đội Garett đi bắt Latimer ấy, bỏ bao công sức rình rập từ lúc đầu truyện tự nhiên tới cuối ông Latimer này bị cái thằng bắn tỉa nó hẫng tay trên, chi tiết này hơi bị lãng.
Nói chung truyện này chuyển thể thành phim chắc hay lắm, mình thích ông Garret, thấy tính ổng trong truyện hay hay lên phim chắc càng hấp dẫn.
4
474064
2015-06-26 07:59:30
--------------------------
212692
6829
Vẫn mô típ cũ - gã cảnh sát chìm giả dạng tiếp xúc với người tâan của tội phạm để điều tra án. Sau khi tiếp xúc hai nhân vật chính sẽ hiểu nhau dần dần thì yêu nhau. Nhưng cái hay là giọng văn của Maya Banks. Vô cùng nhẹ nhàng lại vừa nóng bỏng trong những đoạn mô tả sự lãng mạn của hai nhân vật, nhưng lại mạnh mẽ kịch tính trong phân đoạn đuổi bắt tội phạm. Maya Banks đã sử dụng cả bốn tính nhẹ nhàng - nóng bỏng - mạnh mẽ - kịch tính vô cùng uyển chuyển không hề bị dứt đoạn. Hơn nữa bản dịch cũng đã thể hiện được hết bốn tính chat này của tác phẩm.
Tuy nhiên bìa sách lại là nỗi thất vọng. Màu xám kết hợp với màu cam tạo cảm giác lạnh lẽo chảng mang một chút gì gọi là yêu thương ấm áp. Hình bìa cũng là về một đàn ông đang hì hụt trốn chạy không có chút gì mang một sự liên quan nào đến tình yêu. Nói chung thì với bìa sách này: Trốn Chạy thì thừa mà Yêu Thương thì chả thấy đâu.
4
108098
2015-06-22 19:20:50
--------------------------
126097
6829
Tôi khá nôn nao chờ đợi quyển này được xuất bản, vì yêu thích người dịch cũng như tác giả Maya Banks. Và nó không làm tôi thất vọng (ngoại trừ bìa sách, thật sự không hề phù hợp với tên và nội dung, làm mất hẳn sự ấm áp của tác phẩm).

Câu chuyện mở đầu với tình huống Sarah nhìn thấy người anh trai cùng cha khác mẹ với mình, Marcus giết chết Allen Cross - kẻ xấu xa đã cưỡng bức Sarah, lấy đi mọi lòng tin, niềm vui và chỉ mang lại nỗi hoảng sợ cho cô. Diễn biến tiếp theo là cuộc chạy trốn có vẻ kéo dài bất tận. Nhưng mọi thứ đã chuyển biến khác đi với sự xuất hiện của Garrett cùng cảnh biển mênh mông thơ mộng.

Với mô típ khá quen thuộc, nhưng qua lời văn của Maya Banks, tác phẩm mang một lôi cuốn khó diễn tả. Đó có thể là do những cảm xúc gần gũi, chứ không còn là tiểu thuyết tưởng tượng (tôi có cảm giác mình có thể tìm được một anh chàng Garrett hay cô nàng Sarah ngoài đời thực), tình cảm gia đình và tình đồng đội cũng là một điểm lôi cuốn tôi, khiến tôi thật sự rất muốn theo dõi tiếp câu chuyện của anh em nhà Kelly, nhất là cặp đôi P. J. và Cole.

Chất lượng in khá được, trình bày trang sách thoáng, bản dịch rất mượt không có lỗi chính tả. Thật sự rất mong chờ BV sẽ sớm xuất bản những tác phẩm tiếp theo của Maya Banks :)
5
202665
2014-09-17 08:36:29
--------------------------
125490
6829
Tôi biết đến Maya Banks từ cuốn Chiến binh của công chúa, bà viết rất chắc tay nên tôi cũng khá bất ngờ khi biết bà hay viết thể loại hiện đại hơn, đặc biệt là Romantic Suspense. Nhưng tôi thật sự thích cuốn này. Câu chuyện diễn biến chậm và sâu, nhưng tôi vẫn muốn có thêm nhiều hành động lãng mạn hơn nữa. Tình cảm của Garrett dành cho Sarah rất dịu dàng, ẩn chứa sự mãnh liệt nhưng chưa thật sự bùng nổ.
 Tôi cũng ko thích phần kết lắm. Sarah tha thứ cho Garrett hơi nhanh, dù Sarah hiểu anh làm thế vì sự an toàn của cô nhưng tôi vẫn muốn tác giả hành hạ Garrett thêm vài trang nữa :)). Sarah đã luôn phải sống trong sợ hãi và nghi ngờ sự phản bội từ quá lâu rồi, mà đây lại là người cô yêu, dù hiểu cho hoàn cảnh của Garrett đến bao nhiêu thì cô cũng ko thể tha thứ một cách nhanh chóng như vậy được.
Nhưng phải nói MB xậy dựng hình tượng nhân vật rất thực, đặc biệt là Hero Garrett, điều đó tạo cảm giác câu chuyện thật hơn và làm tôi tin biết đâu ngoài đời cũng có thể tìm được một người như thế...
 Về chất lượng in, có thể nói quyển này khá ổn, không như phần lớn các truyện phương Tây khác mà BV xuất bản, bản dịch rất mượt, hầu như không bị lỗi chính tả, chỉ duy có điểm trừ là bìa khá xấu,  có thể suy ra người làm bìa muốn nhấn mạnh tính cao trào hay những yếu tố hành động trong truyện (dù không thực sụ nhiều), có lẽ thế nên ấn tượng về một cuốn tiểu thuyết lãng mạn đã bị bỏ qua, phá vỡ những tưởng tượng của tôi về hình tượng nhân vật Garrett.
Dù vậy, tôi vẫn mong chờ BV sẽ xuất bản tiếp truyện của Maya Banks
4
68745
2014-09-13 17:56:26
--------------------------
123703
6829
Maya Banks hợp nhất với dòng tiểu thuyết lãng mạn lịch sử, và "trốn chạy và yêu thương" là một tác phẩm lãng mạn hiện đại. Tuy không hay bằng lịch sử những nói thật mình cực thích những tác phẩm của Maya Banks. 
Trốn chạy và yêu thương có nội dung không mới nhưng lại rất logic và lôi cuốn, lôi cuốn bởi tình tiết hợp lý, đan xen giữa lãng mạn và hình sự tạo cho tác phẩm một dấu ấn riêng dù motip cũ. Garrett và Sarah đến với nhau trong một tình cảnh ngặt nghèo và trắc trở, khi Sarah đang bỏ trốn và Garrett được trao nhiệm vụ 'canh gác' cô. Câu chuyện không có gì đáng nói nhiều, nhưng đây là lần đầu tiên mình không cảm thấy tức giận nhân vật phản diện - Marcus - anh trai Sarah. Dù anh ta có độc ác và tàn nhẫn nhưng điều cứu rỗi linh hồn Marcus chính là tình yêu vô bờ bến của anh đối với cô em gái mồ côi của mình. Đó chính là điểm mấu chốt để đến cuối cùng cái chết của Marcus lại vừa khiến người đọc cảm thông vừa khiến ngược đọc...nhẹ nhõm.
Dù là tác phẩm độc lập nhưng lại nằm trong series KGI của Maya Banks, và có đôi chút liên quan đến nhau nên việc đọc chỉ 1 tác phẩm và không đọc những tác phẩm còn lại khiến mình khá...bứt rứt. Hy vọng Bách Việt có thể xuất bản những cuốn tiếp theo trong series này của Maya Banks
4
125574
2014-09-03 09:41:48
--------------------------
