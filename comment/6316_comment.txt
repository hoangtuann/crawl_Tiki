199787
6316
Trước đây, khi học xong 12 thì tôi cứ ngộ nhận và ảo tưởng rằng vốn từ vựng tiếng Việt của mình đã đủ để "sống". Nhưng sau một thời gian học tập và đặc biệt là đọc các quyển sách về kinh tế học và bắt đầu tham gia viết bài, tôi mới nhận ra rằng vốn từ vựng tiếng Việt của mình còn quá ít ỏi. Quyển từ điển này tôi đã đọc qua (mặc dù chưa đọc hết) ở nhà sách Đà Nẵng và thấy nó khá tốt để chúng ta có thể tra cứu, nâng cao vốn từ và tăng thêm hiểu biết của mình về nghĩa của các từ đó. Từ điển được biên soạn nội dung rất cẩn thận, thiết kế trang bìa khá đẹp và được đóng bìa cứng nên rất dễ dàng bảo quản.
5
387632
2015-05-23 09:41:33
--------------------------
