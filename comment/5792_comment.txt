465981
5792
Các tác phẩm được tuyển chọn trong cánh cửa mở rộng đều ẩn chứa những giá trị sâu sắc.
Thác Loạn Ở Lasvegas đưa ta về vùng đất những năm 70. Những chi tiết cùng với lời văn của dịch giả sau chuyển ngữ đưa vào một thế giới rất chân thật mà nhiều khi nhập tâm quá khiến ta khó thở bởi sự thác loạn đó. Cuốn sách cũng gây cho ta một ảo giác tương tự như thuốc phiện vậy mặc dù tôi chưa hít thuộc phiện bao giờ.
Và những chi tiết hài hướng, lố lăng đó lại đưa đến những suy nghĩ sâu xa với nhiều bài học thấm đẫm của sự trải nghiệm

5
74132
2016-07-01 17:31:34
--------------------------
306790
5792
Khá ngạc nhiên khi tủ sách Cánh cửa rộng mở được nhà văn Phan Việt và giá sư Ngô Bảo Châu thành lập lại giới thiệu cuốn sách ngập ngụa tình dục, thác loạn và chửi thề này. Nhưng khi đọc xong cuốn sách, thấy lựa chọn của 2 người thật phù hợp. Hài hước thật cùng lời dịch không chê vào đâu được của Phủ Quỳ, nhưng cuốn sách lại châm biếm sâu cay chính trị Mỹ thập niên 70 với sự vùi dập tầng lớp Hippie chuộng hòa bình bởi Nixon và bè lũ đảng phái, cụ thể là cuộc chiến tranh phi nghĩa , xâm lược quốc gia viễn Đông là Việt Nam
4
292668
2015-09-17 20:02:08
--------------------------
223852
5792
Đọc Thác loạn ở Las Vegas khiến tôi có cảm giác như đang  thử một liều thuốc phiện cùng hai người đàn ông này rên con đường sa mạc, sợ hãi như thằng nhóc đi nhờ xe phải nhanh chóng bỏ chạy nhưng lại không thể cưỡng nổi cám dỗ tiếp tục cuộc hành trình. Một chuyến đi điên rồ trong ảo giác của thuốc , một Las Vegas hào nhoáng rực rỡ ánh đèn với những cuộc chơi thâu đêm cùng những suy nghĩ về thực tại nước Mỹ là những điều bạn sẽ được trải nghiệm khi đọc cuốn sách này.
5
129176
2015-07-07 18:07:46
--------------------------
158777
5792
Với mình thì "Thác loạn ở Las Vegas" của Hunter S.Thompson giống như phần hai của cuốn "Trên đường" nổi tiếng của nhà văn Jack Kerouac. Hai cuốn sách riêng biệt, hai hành trình riêng biệt, nhưng theo một cách kỳ diệu lại có rất nhiều điểm tương đồng lý thú. Có lẽ đó chính là nét đặc trưng của một thế hệ thanh niên Mỹ thời đó. Họ cuồng nhiệt, phóng khoang, hoang dại, dường như chẳng thèm sợ hãi bất cứ điều gì. "Giấc mơ Mỹ" có phần điên cuồng và những khát khao tuổi trẻ đã thôi thúc những chàng trai ấy dấn thân vào một cuộc đua bất tận. Hunter S.Thompson, với ngòi bút tuyệt vời, trong cuốn sách này đã lột tả chân thực những điều ấy, vẽ nên một nền văn hóa đầy màu sắc của Las Vegas. Những trang viết tràn đầy sức sống ấy, vừa ồn ào náo động, vừa sâu sắc và tràn ngập cảm xúc. Cuốn sách này giống như một "liều thuốc phiện" vậy, chỉ có điều thuốc phiện thật thì nguy hiểm, còn cuốn sách sẽ làm bạn say mê thích thú và rút ra được nhiều bài học quý giá.
4
109067
2015-02-13 21:59:19
--------------------------
