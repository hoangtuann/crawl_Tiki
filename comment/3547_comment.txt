418805
3547
Cuốn từ điển này nhỏ gọn, giá vừa túi tiền, phù hợp với mọi lứa tuổi. Em học chuyên Anh nên cũng cần nhiều đến từ điển nhưng thường thì từ điển dày và rất nặng, cồng kềnh. May sao trên Tiki có bán cuốn từ điển Việt - Anh nhỏ gọn này, em thấy rất hài lòng vì nó bỏ túi được. Chất lượng giấy đảm bảo, nội dung phong phú, chữ in nhỏ nhưng vì từ điển nhỏ nên cũng không sao.Tiki giao hàng nhanh, đóng gói chắc chắn nên em cũng rất hài lòng. Nói chung thì cuốn từ điển này rất tiện lợi và nhất là cho các bạn học sinh đang đi học
4
1284706
2016-04-20 00:13:50
--------------------------
