430983
5321
Đối với mình thì cuốn từ điển này khá hay. Những ai bắt đầu tập sử dụng từ điển anh - anh thì quả thật nên tậu ngay một em như thế này về, bởi vì sau phần dịch bằng tiếng anh em nó còn có cả dịch nghĩa bằng tiếng  việt cho những ai chưa hiểu nữa. Bìa cứng dày dặn, lại có thêm cả hộp bao bên ngoài nên rơi rớt cũng thấy an tâm phần nào. mình cũng rất thích cách trình bày, không chỉ in mực đen như từ điển thông thường mà những từ cần tra được in đậm bằng màu xanh nổi bật (hình như từ điển nào của MC books cũng thế). Tuy nhiên lượng từ vựng thì hơi ít, nhiều từ mình tra mà không có. Từ điển này chắc chỉ phù hợp với những bạn ở trình độ A, B thôi.
5
178929
2016-05-16 13:19:29
--------------------------
425864
5321
Học tiếng anh luôn cần phải có một người bạn từ điển ở bên cạnh vậy nên mình đã chọn cuốn từ điển này làm bạn vớii mình trên con đường tri thức. Giấy của từ điển rất đẹp, dày. Từ điển có một cái hộp bảo vệ nên từ điển không bị dơ. Nhưng khi sử dụng cần phải rất cẩn thận bởi từ điển rất dễ bị bung keo ra, đó là điểm trừ của cuốn từ điển. Còn về chất lượng in ấn và kiến thức thì tốt và dễ hiểu, phù hợp với mọi trình  độ.
5
825484
2016-05-05 16:48:05
--------------------------
406565
5321
Vì từ điển cũ bị bung xòe hết các trang nên mình phải đi tìm ngay một cuốn từ điển mới mà vẫn chất lượng để dùng đi học. Mua ở nhà sách thì khá đắt nên mình đã đặt trên Tiki. Cuốn từ điển này thật sự không phụ lòng mong đợi của mình. Bìa cứng, chắc chắn, từ điển còn được bỏ hộp rất cẩn thận. Cỡ chữ vừa đủ, khiến mình học thời gian dài và phải tra nhiều nhưng không nhức mắt. Tổ chức và sắp xếp hợp lí, khiến mình tra từ nhanh hơn, dễ hơn. Ví dụ đầy đủ và dễ hiểu, kèm theo các từ đồng nghĩa hoặc trái nghĩa, rất tiện cho việc mình làm bài tập. Phần đầu từ điển kèm theo nhiều các phụ lục bổ ích. 
5
123769
2016-03-28 16:15:33
--------------------------
404582
5321
Sách mình mới vừa nhận được.Về hình thức lẫn nội dung đều rất ok.Rất xứng đáng và nên mua.Đầy đủ và chi tiết.Giải thích trực quan sinh động.Có thêm các trang hình màu phía trước nhìn rất dễ thương.Ko có gì phải phàn nàn.Có điều chữ hơi nhỏ một chút,mình phải ráng mắt nhìn mới thấy giữa 1 rừng chữ như thế này,phải chi chữ nó to hơn một tý thì thoải mái hơn.Sách có hộp đàng hoàng.Không sợ hư sách hay rách mất.Tính mình khá kỹ ,nên thik chọn cuốn nào dày dặn cứng cáp để dùng.Cuốn này nên mua các bạn nhé!
4
435793
2016-03-25 13:34:36
--------------------------
383219
5321
Đây là lần đầu mình mua từ điển nên không biết phải chọn ra sao. Thấy mọi người nhận xét về sản phẩm khá tốt nên mua. Giá của cuốn từ điển phải chăng, bên ngoài là bìa cứng nên khó bị quăn giấy. Tuy nhiên thì khi mở ra hơi khó, cảm giác như bìa sách sắp rời ra luôn ấy.
Về phần nội dung thì cuốn từ điển có 350.000 mục từ, tha hồ tra. Phần đầu còn có cả hình minh hoạ, rất dễ hiểu. Nói chung là rất thích, thích cả cách giao hàng của Tiki

5
1153415
2016-02-20 12:44:17
--------------------------
377711
5321
Đây là một cuốn từ điển tốt. Ấn tượng đầu tiên của tôi là nó quá hấp dẫn ngay từ hình thức đến nội dung. Mở đầu cuốn sách là sự phân nhóm từ ngữ dùng trong từng ngữ cảnh, như từ ngữ về vật dụng trong nhà, từ ngữ về cây cối sông ngòi,.., kèm theo hình ảnh màu sắc rõ nét và đánh số thứ tự để người xem dễ dàng nhận biết. Chữ in trong giấy rất rõ nét và đẹp, giấy thì láng mịn, sờ rất thích, bìa cứng cộng với hộp cứng giúp bảo quản cuốm từ điển được lâu hơn. Mặc dù nếu học chuyên sâu thì đây không phải là cuốn từ điển hữu dụng lắm, nhưng nếu dành cho trẻ em thì đây là cuốn từ điển khá tốt.
4
63376
2016-02-03 09:53:57
--------------------------
355695
5321
Sách rất đẹp, cái hộp đựng sách mà cũng làm bìa cứng nữa, sách nhỏ gọn nhưng do dày quá nên hơi khó giở tí xíu. Các trang đầu sách in chất liệu giấy trơn bóng, có màu có hình ảnh minh họa và nói về các chủ đề tiếng anh, rất đẹp mà còn hữu dụng nữa. Quyển này dễ tra cứu mà lại đẹp, không gây nhàm chán, chữ in màu xanh dương rất bắt mắt. Từ vựng phong phú và được giải thích cặn kẽ, rõ ràng. Những từ đơn giản cũng được cho ví dụ cụ thể để mình họa, giúp người đọc khi tra có thể hiểu tường tận nghĩa và hoàn cảnh sử dụng từ đó. 
5
897666
2015-12-20 14:59:15
--------------------------
354738
5321
Ban đầu mình được người bạn giới thiệu và sau khi mua mình cảm thấy rất hài lòng. Từ vừng rất nhiều và nghĩa cũng được giải thích cụ thể, không chỉ vậy những trang đầu của quyển từ điển là những hình ảnh đồ vật trong cuộc sống thuận tiện cho việc học từ qua hình ảnh. Từ điển cũng được in bởi chất liệu giấy tốt và còn được bọc trong một chiếc hộp cứng. Mình cảm thấy rất vừa ý khi mua quyển từ điển này, càng giúp mình thích học Tiếng Anh nhiều hơn nữa. Hãy mua nó nhé !!!!
5
616523
2015-12-18 15:19:44
--------------------------
325646
5321
Về mặt nội dung, cuốn từ điển giải thích cơ bản dễ hiểu, kèm hình ảnh ví dụ sinh động, giúp ích cho việc ghi nhớ từ nhàm chán trở nên dễ dàng hơn.
Tuy vậy về mặt hình thức, mặc dù bìa cứng, lại có hộp bỏ thuận lợi cho việc cất giữ, nhưng khi lật mở lại gặp chút khó khăn, phải "nâng như nâng trứng" ấy, nếu không sẽ có cảm giác giấy và bìa sẽ bong ra sau khi mình dùng nhiều. 
Giá cả quá ổn so với một cuốn từ điển chất lượng như thế này!
5
869749
2015-10-23 22:30:38
--------------------------
305222
5321
Mình cũng muốn nhận xét để các bạn yên tâm cho sản phẩm này vào giỏ hàng nhé! mình phải công nhận tiền nào của nấy thật, đúng là cuốn từ điển này rất tốt đấy nhé! Từ điển nằm trong hộp nên rất dễ dàng để bạn cất vào tủ sách mà không bị quăn góc như mấy cuốn từ điển khác. Gồm 350.000 mục từ với những ví dụ hết sức dễ hiểu và tinh tế! Nếu chưa hiểu rõ các bạn có thể xem hình trong từ điển, nó sẽ giúp các bạn hiểu rõ hơn! Với vốn kiến thức của mình thì cần phải trau chuốt qua cuốn từ điển này. Cuốn sách này bìa cứng và được translate theo E-E-V nhé :) Chúc các bạn học tốt !
5
504577
2015-09-16 21:24:44
--------------------------
222958
5321
ồ, có vẻ sản phẩm này chưa có ai đánh giá nhỉ, vậy thì để mình đánh giá nó cho các bạn yên tâm cho vào giỏ hàng nhé. cuốn từ điển này thực sự rất đáng giá đồng tiền bởi vì nó chất lượng rất tốt,, sách dày, giấy đẹp còn có thêm các trang in màu giấy bóng mượt ở đầu cuốn minh học về các sự vật xung quanh chúng ta nữa. cuốn từ điển có bìa cứng, bên trong là các từ tiếng anh được dịch theo chuẩn anh- anh- việt kiểu oxfod. bạn có thể tra từ điển dễ dàng với số lượng từ phong phú, cách dịch nghĩ by english giúp bạn có thể đoán ra được nghĩa từ đó mà ko cần nhìn vào tiếng việt, còn giúp ích cho bạn trong việc diễn đạt từ nữa.
5
166271
2015-07-06 13:44:20
--------------------------
