435459
9466
Đúng như lời giới thiệu, tiểu thuyết là những lát cắt sinh động về đời sống của những Chí Phèo kiểu Mỹ. Với giọng văn nhẹ nhàng, pha lẫn sự hài hước tác giả dẫn dắt đến sự hình thành của hội bàn tròn gồm toàn những 'người yêu rượu'. Ban đầu có thể người đọc chưa thật sự có cảm tình với những paisano, nhưng dần dà những diễn biến, tình tiết dẫn dắt đến một loạt những suy nghĩ, hành động, tình cảm của các paisano càng làm người đọc hiểu hơn, cảm thông hơn với họ. Và vì thế mà bắt đầu yêu những anh chàng kiểu 'cao bồi vườn' này. Câu chuyện kết thúc đượm buồn nhưng như một lẽ đương nhiên của sự hợp tan, và bởi cái kết ấy mà khi đọc xong tiểu thuyết người đọc như mong ước được hiện diện cùng cái hội bạn ấy bên những galon rượu trong ánh nến mờ ảo để cùng hàn huyên tâm sự hay chỉ là để hát nghêu ngao vài câu khi đã bắt đầu sang chai thứ 2.
4
554079
2016-05-25 08:57:02
--------------------------
419535
9466
Đây là tác phẩm được biết đến nhiều nhất của tác giả John Steinbeck. Ông đã được trao giải Nobel Văn học. Dù đã được viết cách đây rất lâu rồi nhưng cũng rất may mắn được nhà xuất bản Trẻ mua bản quyền để phát hành sách để đem đến cho bạn đọc một tấm màn sương nhẹ về xã hội Mỹ bấy giờ. Tác phẩm đầu tay này của ông tập hợp nhiều mẫu chuyện viết về những người thuộc tầng lớp dưới đáy xã hội ở Monterey. Viết về những người đấu tranh không ngừng nghỉ bám trên đất của mình để sinh tồn.
5
383240
2016-04-21 14:30:06
--------------------------
303366
9466
Tác phẩm viết về tầng lớp nông dân Mỹ, tuy là châu Âu nhưng mình thấy nó có khá nhiều điểm tương đồng với nông dân châu Á, cùng là những người cùng khổ, cùng chia ngọt sẻ bùi, cũng vượt qua khó khăn và sống với nhau đầy tình nghĩa. Tác phẩm có văn phong giản dị, cách viết tự nhiên nhi nhiên không gượng ép khiến người đọc có cảm giác như mình đang được nghe kể chuyện chứ không phải đang phải đọc một câu chuyện khô khan, đây là một trong những tác phẩm nằm trong Tủ sách cánh cửa mở rộng mà mình khá ưng ý. Cho sách 5 sao nhe :))
5
464538
2015-09-15 20:21:48
--------------------------
227475
9466
Tôi đã từng đọc nhiều tác phẩm của "Cánh của mở rộng" giới thiệu nhưng tôi cảm thấy đây là cuốn sách hay nhất mà tôi từng đọc. Cuốn sách kể về cuộc sống đời thường của những người bạn cùng lớn lên với nhau trên mảnh đất Duyên Hải. Nội dung của truyện nói về cuộc sống khốn khó, nhưng tình cảm vượt lên trên sự khốn khó về tiền bạc. Cuốn sách như một câu chuyện sống có lúc thâm trầm buồn bã, có lúc niềm vui chiếm phần hơn; nhưng dù sao đi nữa cuốn sách như một bức tranh về những người bạn cực khổ, mà nói chung ra là của người dân trên đất Mỹ bấy giờ. Cuộc sống thì lầm thang, khốn khó, mỗi ngày đồ ăn của họ chỉ là những miếng đồ dư thừa từ những tiệm bán đồ ăn. Những từ ngữ rất ưa là đời thường. Nói chung quanh lại là một cuốn sách hay ý nghĩa về tình bạn. Tôi xin cảm ơn Tiki đã mang đến những đầu sách hay như vậy
5
435731
2015-07-14 11:20:03
--------------------------
222644
9466
Trong bộ "Tủ sách mở rộng", đây đã quyển mình ấn tượng nhất và mong muốn sở hữu nhất. Cốt truyện hay, sinh động, khai thác hết được những khía cạnh của cuộc sống. Như một xã hội Mỹ thu nhỏ qua các tầng lớp và giai cấp, lột tả hết được những thân phận và cảnh đời trong một mặt khác của vẻ hào nhoáng bên ngoài.Thị trấn Tortilla đem đến cho người đọc cái nhìn sâu sắc về mỗi con người, về xã hội đương đại, là một cái gì đó rất thật, rất riêng qua ngòi bút tuyệt vời của John Steinbeck giàu hình ảnh, đậm tính chân thực và nhân văn.
5
166935
2015-07-05 23:09:20
--------------------------
219501
9466
Cuốn sách viết về nước Mỹ và lấy bối cảnh xã hội Mỹ nhưng tôi thấy thấp thoáng đâu đó hình ảnh VN chúng ta.

Tôi chưa từng nghe đến cuốn sách này, cho đến khi thấy nó xuất hiện trong tủ sách Cánh cửa mở rộng của giáo sư Châu và chị Phan Việt. Quả thực tôi không hối tiếc khi mua nó.

Cuốn sách thể hiện một cái nhìn đầy cảm thông, thấu hiểu của tác giả đối với người nông dân nói riêng và rộng hơn là con người Mỹ nói chung. Mạch truyện hấp dẫn và càng về sau càng thú vị. Điều đáng khen là tác giả thể hiện một sự chân thật tuyệt vời trong suốt tác phẩm này. Hoàn toàn không ảo tưởng, “tô hồng”. Nhờ đó, nước Mỹ trở nên sống động hơn chứ không hoàn toàn là “American dream”.

Cám ơn giáo sư Châu và chị Phan Việt đã giới thiệu tác phẩm này.
4
598102
2015-07-01 15:49:48
--------------------------
50686
9466
Trong những cuốn sách thuộc tủ sách "Cánh cửa mở rộng" của nhà xuất bản Trẻ, thì đây chính là cuốn mình ấn tượng nhất, thu hút mình nhất. Một câu chuyện hoàn toàn xuất sắc, lôi cuốn trọn vẹn, đem đến những trải nghiệm có một không hai. Bối cảnh của truyện là nước Mỹ, với những người Mỹ có thân phận, địa vị khác nhau trong xã hội, nhưng rất gần gũi với độc giả Việt Nam vì có nhiều điểm tương đồng sâu sắc. Mỗi hình ảnh, mỗi nhân vật cùng những chi tiết về cuộc đời của họ hiện lên một cách nhẹ nhàng, chân thực và sắc nét, giúp người đọc cảm nhận được nhiều điều về con người và cuộc sống.
Một tác phẩm có giá trị nghệ thuật và nhân văn rất cao, rất xứng đáng với những lời khen ngợi của giới chuyên môn.
4
20073
2012-12-16 09:42:31
--------------------------
