332122
7684
công vật kỳ lân cả đời có ai nhìn thấy đâu. đến Tể tướng khi đưa con đó về cũng không thể nói được nó là giống gì mà cứ một hai gọi là kỳ lân rồi bắt bọn Trạng Tí phải nuôi hòng tìm cách gán tội. tất nhiên Tí thông minh nên hiểu ra mưu đồ của Tể tướng Tào Hống bèn chơi khăm lại nói rằng vì chính ông là người bắt nên hiểu rõ nhất về con " kỳ lân " này, nên để ông nuôi hợp hơn.. kết cục cuối cùng Tể tướng cũng đành bỏ qua con cống vật kì lạ để tránh phiền. câu chuyện ;là bài học về sự hoang đường nhưng ai nấy nấy vẫn tin để cảnh tỉnh chúng ta hiện giờ...
5
568747
2015-11-05 19:59:35
--------------------------
219165
7684
Đây là 1 trong những bộ truyện gắn liền với tuổi thơ của mình. Tí, Sửu, Dần, Mẹo.. gắn bó với mình không kém mấy nhân vật như Doraemon ấy ^^ Đặc biệt mình rất thích đọc mấy tập kiểu như tập này, phản ánh được trí thông minh của Tí và giúp mình phần nào hiểu thêm về lịch sử. Điều mình thích nhất là sau mỗi lần đọc xong, ở những trang cuối đều có ghi lại những thông tin bổ ích về lịch sử, nguồn gốc, điển tích trong truyện ấy, đọc mà thấy khâm phục ghê luôn.
4
476360
2015-07-01 10:33:39
--------------------------
117654
7684
Mình không biết đối với các bạn truyện "Thần đồng đất việt" như thế nào,nhưng đối với mình đây là 1 kho tàng kiến thức khiến mình rất thích thú.Hiện nay mình đã có 1 kho báu khổng lồ với hơn 100 cuốn và điều đó làm mình rất vui và tự hào.trong lớp mình được các bạn đặt biệt danh là Vân thư viện . Thư viện của trường toàn là sách mình đóng góp . Các bạn rất cám ơn mình vì đã cho các bạn biết đến 1 loại truyện tranh bổ ích . Trong truyện có các nhân vật cầm tinh con giáp như : Trạng Tí , Cái Sửu , Dần Béo , Cả Mẹo , Bá Hộ Mão , Công Chúa Phương Thìn , Tiểu Tị , Ngọ "Bà Chằn" , Mùi Mập , Thiên Thân Công Chúa , Dậu Rách , Anh Bá Tuất , Thái Tử Thiên Hợi...Mình rất yêu truyện Thần Đồng Đất Việt và sẽ tiếp tục nuôi kho báu của mình ngày một phình to.Em cảm ơn tác giả Lê Linh và công ty Phan Thị đã cùng chung sức tạo nên bộ truyện tranh này!
5
383388
2014-07-21 12:34:22
--------------------------
