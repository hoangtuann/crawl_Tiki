430790
10148
Em gái mình năm nay lớp 3. Để đọc một quyển tiểu thuyết "Tắt đèn' dày đặc những trang chữ, với một cô bé ham mê truyện tranh sâu sắc thì điều đó không hề đơn giản. Chính vì vậy khi cầm trên tay quyển truyện tranh "Tắt đèn", con bé đã vô cùng thích thú, và đọc một cách say mê. Nét vẽ vô cùng cuốn hút, tái hiện hoàn hảo bức tranh xã hội phong kiến lúc bấy giờ, một xã hội thối nát, mục ruỗng đến tận xương tủy. Mong rằng tác giả sẽ vẽ thêm nhiều những trang truyện dựa trên nội dung các tác phẩm văn học Việt Nam hơn nữa, để gần gũi với độc giả hơn, nhất là những độc giả nhỏ tuổi, giúp các em hiểu được thêm về cuộc đời này.
5
1239263
2016-05-15 21:33:58
--------------------------
221010
10148
Về nội dung truyện thì khỏi phải bàn rồi, một tác phẩm xuất sắc của nhà văn Nam Cao. Phải bàn ở đây là ngòi bút tạo nên nét vẽ trong cuốn truyện. Ba bạn trong nhóm B.R.O đã rất sáng tạo trong việc chuyển thể thành công từ truyện chữ sang truyện tranh, chắc hẳn rất khó để lột tả hết những điều tác giả muốn nói. Nét vẽ trong truyện rất đẹp, đồng thời lại rất có hồn; thể hiện sinh động và chân thực gần như trọn vẹn từng câu chữ; khắc hoạ những nhân vật và tình tiết rất đặc sắc, tiêu biểu là chị Dậu với "bầu trời tối đen như cái tiền đồ của chị". Cảm ơn nhóm tác giả và tiki đã mang đến một cuốn sách hay. 
5
668946
2015-07-03 11:33:23
--------------------------
216166
10148
Đọc tác phẩm của Ngô Tất Tố, người đọc như lọt thỏm vào một khoảng không vô định
Ta cứ thế mà rơi, rơi đến mức không thấy điểm dừng, chỉ biết oằn người gánh chịu lực Trái Đất cuốn dẫn mình . Nhân vật trong tác phẩm cũng vậy.Họ rơi trong một khoảng không mịt mùng, để rồi khi chạm đến một mốc, họ chuếnh choáng nhận ra rằng: lực hút của Trái Đất không có số đo cụ thể. Mọi thứ phụ thuộc vào thế giới họ đang sống, mà thế giới ấy -như Ngô Tất Tố đã đề cập - "bầu trời tối đen như cái tiền đồ của chị" - giữa một khoảng không ghê rợn như thế, đâu là sự sống, đâu là những sự vật có thể hiện hữu? Gai góc đến rợn người, lời văn viết ra như rỉ máu ở đầu ngọn bút, vừa đả kích chua ngoa thế lực đen tối và thối nát của xã hội phong kiến, vừa khắc họa những khắc nghiệt đến cùng cực của lực hút man rợ kia. Để rồi khi kết thúc, lực hút vẫn còn, không gian vẫn còn, sắc đen vẫn còn, chỉ có con người là dần dần thui chột. Đó, có hay chăng là một sự ám ảnh?
5
108594
2015-06-27 11:31:51
--------------------------
180946
10148
Xã hội năm 1945 đã hiện lên thật rõ nét và chân thực qua ngòi bút của Ngô Tất Tố, nay lại thêm sống động hơn qua những nét vẽ của nhóm BRO. Phận người nông dân trong xã hội ấy bị khinh thường, bị rẻ mạt, phải gánh trên lưng những thứ xiềng xích thuế má vô lý. Và khi bị dồn đến bước đường cùng, không còn đường lui thì người nông dân sẵn sàng vùng dậy để đấu tranh, để giành lại cho mình quyền sống như một con người chứ không phải là súc vật.
Truyện cũng đề cao người phụ nữ trong xã hội ấy : vị tha, thủy chung, thương chồng thương con và nhất là "giặc đến nhà đàn bà cũng đánh". Chị Dậu vì muốn cứu chồng mà đã liều mình đánh trả bọn quan lại, vì muốn kiếm tiền nộp sưu thuế mà chị chấp nhận làm vú em. Nhưng chị không cam tâm, không nhẫn nhục để bọn quan lại thối nát giẫm đạp lên nhân cách của mình. Tôi rất ấn tượng với cái kết cuối cùng của truyện khi chị Dậu trốn chạy giữa một bầu trời đêm đen tựa như một vùng tối không thể tìm ra lối thoát ....
5
10908
2015-04-10 17:03:34
--------------------------
67904
10148
Vốn dĩ Tắt đèn của Ngô Tất Tố đã hay , nay được vẽ lại bằng nét vẽ hết sức chân thực và bình dị của B.R.O lại làm cho truyện thêm sinh động và hấp dẫn hơn nữa . Mình đã từng đọc Chí Phèo và thấy nó thật hay , nay đọc Tắt đèn thấ còn hay hơn .Truyện là cả một tấn bi thảm giáng lên trên người phụ nữ chịu đựng nhiều đau khổ , truyện bộc lộ tâm lý xuất sắc cộng với chi tiết truyện chân thực , tinh tế , đem lai6 bài học cuộc sống thích đáng . Chỉ có một điều mình hơi thất vọng đó là đoạn kết của truyện là một kết thúc  mở , nhưng dù sao truyện rất hay và cảm ơn Tiki đã đem " Tắt đèn " đến cho mình
5
100974
2013-04-09 13:09:01
--------------------------
62586
10148
Biết đến " Tắt đèn "thông qua đoạn trích "Tức Nước vỡ bờ" trong Sách giáo khoa lớp 8 . Mình đã cực kì thích tác phầm này , tìm mua được tập 1 và cả 2 mình thấy nhóm B.R.O đã phác họa rất thành công nội dung của câu truyện , hình ảnh sống động , nhân vật có tính cách mạnh . Vốn dĩ nội dung của Ngô Tất Tố đã rất tuyệt nay công hưởng thêm lối truyện tranh càng làm cho bộ truyện này đã hấp dẫn lại thêm hấp dẫn . Điều duy nhất mình cảm thấy thất vọng đó là phần kết . Cảnh chị Dậu bị hãm hại sau đó bỏ chạy vào màn đêm u tối và ............ kết thúc . Vậy cuối cùng số Phận chi Dậu sẽ ra sao ? đến bây giờ mình vẫn chưa có câu trả lời . Nhưng dù sao đi nữa đó cũng là cốt truyện của Ngô Tất Tố , chỉ mong muốn B.R.O sẽ cho ra tiếp nhiều tác phẩm văn học Việt Nam dưới dạng truyện tranh thế này  . 
5
100974
2013-03-10 11:37:51
--------------------------
33238
10148
Kết thúc của Chị Dậu khiến cho người ta phải nghẹn ngào bởi vì "ngoài trời tối đen như cái tiền đồ nhà chị", một sự bế tắc và không lối thoát cho những người nông dân nước ta trước CMT8. Tuy nhiên, điều đó cũng không có nghĩa là trong truyện không hề có những điểm sáng, dù là nhỏ nhoi, rõ ràng nhất vẫn là cách xây dựng nhân vật, Chị Dậu là một phụ nữ mạnh mẽ, dám đứng lên chống lại bọn ngang tàn, ác bá, điều đó cho thấy người nông dân cũng đã phần nào biết phản kháng lại, không chịu bị bóc lột, áp bức. Ngay cả bìa của truyện B.R.O cũng đã làm rất tốt, thấy được nổi khổ đau của Chị Dậu nói riêng cũng như dân tộc ta nói chung vào thời kỳ đen tối đó
4
37817
2012-07-12 09:48:53
--------------------------
28151
10148
Trong năm học lớp 8, tôi đặc biệt thích tác phẩm “Tắt đèn”. Và tôi vui làm sao khi nó đã được B.R.O chuyển thể thành truyện tranh.
Với một người trẻ như tôi, turyện tranh dĩ nhiên là dễ đọc hơn truyện chữ. Thường, truyện chữ với tôi hơi khô khan, còn truyện tranh thì hấp dẫn hơn nhiều vì nó có hình ảnh minh hoạ cụ thể, rõ ràng và chân thật, sống động. Và tập truyện Tắt đèn của B.R.O đã làm được điều đó. Hình ảnh chị Dậu hiền hậu, nhân từ nhưng khắc khổ đã được khắc hoạ rõ nét. Những lúc dám đứng lên đấu tranh vì chồng, chị thật hiên ngang và đầy khí phách. Từng nét vẽ của B.R.O càng tô đậm tính cách đó của chị. Đồng thời, nhưng nghiên cứu kỹ càng của B.R.O về bối cảnh xã hội thời bấy giờ khiến phần khung nền được chăm chút cẩn thận, kỹ lưỡng, tạo cảm giác cổ xưa, phù hợp với tinh thần truyện và khiến người đọc dễ hình dung, mường tượng hơn.
Và vì thế, tôi lại càng thêm yêu thích tác phẩm Tắt đèn. Chợt nghĩ nếu Ngô Tất Tố được chứng kiến bản truyện tranh này, tôi nghĩ ông hẳn sẽ tự hào lắm.

5
33375
2012-05-24 21:33:02
--------------------------
26855
10148
Tớ đã đọc được tập 1 của cuốn truyện này, và đang chờ tập 2 đây. Lúc đầu tớ chỉ mua với ý định xem cho biết thôi, nhưng thật sự là hay quá. Tớ đã biết đến B.R.O từ những ngày B.R.O tham gia vẽ truyện trong những tập Thần Đồng Đất Việt Fan Club. Và tới giờ, B.R.O đã rất thành công trong việc truyền tải nội dung của những danh tác Việt Nam đến gần hơn với bạn trẻ. Chúc B.R.O thành công hơn nữa, và chuyển thể nhiều danh tác khác thành truyện tranh nhé!
5
25696
2012-05-13 19:34:00
--------------------------
