331138
11653
Thấy ích có sách nói về đạo giáo quá nên thấy cuốn này 100 Câu Chuyện Về Đạo Giáo là mình mua ngay, nhiều trong đó có nhiều truyện như ngụ ngôn trung quốc, tổng hợp nhiều cái nhìn về đạo giáo và nhận ra thực ra đạo giáo rất gần gũi với con người chúng ta, đạo làm người, cách sống, đối nhân sử thế, và ai được cho là thánh nhân hay những tấm gương đầy ngưỡng mộ luôn có cách đạo tuyệt vời, cuốn sách thú vị mang lại hiểu biết đạo giáo đến người đọc tham khảo.
3
730322
2015-11-03 23:58:46
--------------------------
285903
11653
Với niềm tin yêu cuộc sống, khi rằng cuộc đời này ngắn chẳng tày gang, con người vẫn chưa từng cam chịu nằm dưới bánh xe số phận làm kiếp phù dung sớm nở tối tàn. Con người không chịu thua cuộc, tự cố gắng kéo dài những viễn tượng, mờ mịt xa vời, thắt bện bằng vô vàng tư tưởng kỳ quặc. Không riêng gì Trung Hoa, những dân tộc khác vào buổi đầu lịch sử như Ấn Độ, Ai Cập, Ba Tư đều bị ám ảnh triền miên với khát vọng trường sinh bất lão, trẻ mãi không già. Họ cứ mãi đi tìm cái bất sinh bất diệt giữa cuộc đời sinh diệt diệt sinh.
4
715062
2015-09-01 11:37:00
--------------------------
