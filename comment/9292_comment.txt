144140
9292
Qua từng câu chữ Đặng Chương Ngạn mở ra cho chúng ta thấy một hiện thực quá đỗi đau lòng, khắc nghiệt. Các em sinh ra đều là những sinh lunh bé bỏng, trong trắng cần được nâng niu thế nhưng cuộc đời lại quá nhẫn tâm khi ném các em vào cuộc mưu sinh tàn ác, bất công. Các em sinh ra là người hưng chẳng thể làm người, bị bọn chăn dắt dùng làm công cụ kiếm tiền. Chúng sẵn sàng đánh đập các em chỉ vì thiếu chút tiền "chỉ tiêu" hay thậm chí đánh vì chúng thích đánh... Cánh cửa hạnh phúc mà cuộc đời dành cho các em chưa kịp mở ra thì đã đóng thật chặt. Qủa thật các em chẳng có lối thoát nào cả nếu có, nó cũng rất mong manh xa xôi. Khi đọc Kẻ chăn dắt tôi vô cùng căm phẫn trước hành động của bọn bất lương và đau đớn, xót xa cho số phận bi kịch của các em nhỏ.
5
93833
2014-12-25 17:25:25
--------------------------
76934
9292
Bạn xót xa cho những "bà mẹ" ngày ngày ẵm bồng đứa con đỏ hỏn ngồi ở góc ngả tư ngửa nón xin từng đồng bạc lẻ của người qua đường
Bạn khó chịu vì những đứa bé ăn xin cứ níu tay bạn, luôn miệng nài nỉ mua giúp chúng thứ này thứ kia
Bạn tội nghiệp cho những con người bất hạnh với khiếm khuyết trên thân thể mà ngày ngày vẫn lê lết trong các con chợ, khắp các ngả đường
...
Vậy bạn hãy đọc "Kẻ chăn dắt" để rồi tự hỏi
Liệu bạn còn xót xa khi biết người phụ nữ và đứa trẻ kia chẳng phải máu mủ. Người ta sẵn sàng đánh cắp, hành hạ những sinh linh bé nhỏ, yếu đuối nhất cho mục đích xấu xa của mình
Liệu bạn có thể thờ ơ khi biết nếu không xin đủ 1 số tiền, những em bé níu tay bạn sẽ lãnh chịu trận đòn roi tàn ác nhất mà chúng không thể bám víu vào ai
Liệu lòng thương hại của bạn có còn nếu tất cả vết trầy xước, máu me, đui mù... chỉ là 1 màn kịch

Tác phẩm đã hoàn toàn thay đổi độ của tôi về nhũng con người lạc bên lề xã hội. Tôi hiểu mình phải thương ai, thông cảm cho ai và tránh xa ai. 

Cám ơn tác giả Đặng Chương Ngạn rất nhiều!
5
71011
2013-05-24 21:52:49
--------------------------
69218
9292
Một câu chuyện khiến người đọc day dứt không yên. Câu chuyện về bé Hy, cha Quý, bé Châu, thằng Tý,.. - những phận người nhỏ bé trong vòng quay cuộc đời, bị nắm trong tay bọn Chăn Dắt hay những con thú đội lốt người.

Đọc từng trang sách, ta càng thêm căm phẫn hành vi tàn ác của bọn Thú Chăn Dắt. Chúng coi con người như những món "hàng" mua được bán được, thậm chí dù món "hàng" ấy hỏng, chúng cũng không hề thương tiếc mà vứt bỏ. Chúng không khác gì loài thú khi kiếm tiền trên thân xác con người. Đọc qua tác phẩm ta chỉ có thể thốt lên: "Bọn chăn dắt. Những con thú biết nói."

Đây thật sự là một tác phẩm hay và đáng đọc để ta có cái nhìn rõ nhất về tội ác của bọn Chăn Dắt, nhằm loại bỏ tội ác ra khỏi xã hội. Và quan trọng nhất là giúp những con người đã vướng vào vòng xoáy nghiệt ngã này tìm thấy ánh sáng nơi chân trời hy vọng.
5
51345
2013-04-15 12:44:22
--------------------------
