225466
9818
Sinh ra khi bác không còn nữa và đất nước đã hòa bình thống nhất nhưng những câu truyện kể về chủ tịch Hồ Chí Minh vĩ đại - Bác Hồ kính yêu của mọi người dân Việt Nam vẫn luôn có sức lôi cuốn mãnh liệt đối với tôi. Khi thấy quyển " Có bác mãi trong tim" tôi đã mua ngay. Quyển sách nhỏ với gần 200 trang được trình bày đơn giản trên nền giấy khá tốt là những câu truyện kể về Bác Hồ với muôn vàn kinh yêu và trân trọng của các tác giả mỗi câu truyện trong sách. Qua những câu truyện bình thường kể về những lần gặp Bác và những hành động giản dị của Bác nhưng để lại nhiều tình cảm  cho người đọc về Bác và những bài học, hành động đạo đức được rút ra làm ta thấy mình nhỏ bé.
5
504882
2015-07-10 14:32:43
--------------------------
