470849
5884
Hồi bé chắc ai cũng đã từng học, từng nghe qua truyện dế mèn phiêu lưu ký của nhà văn Tô Hoài. Bây giờ tuy đã lớn nhưng tôi vẫn muốn tìm lại những tác phẩm ngày xưa mình đã từng học. Nhưng đợi mãi chẳng thấy cuốn này tái bản nên mình quyết định mua bản tiếng anh đọc luôn. Trong sách được thiết kế và in màu rất đẹp, có nhiều hình ảnh khiến cho người đọc dễ hình dung hơn và làm cho câu chuyện thêm phần sinh động. Vả lại mua quyển này cũng giúp ích cho việc học tiếng Anh.
5
509425
2016-07-07 23:59:28
--------------------------
342471
5884
Mình đã mua cuốn Dế mèn phiêu lưu ký từ rất lâu rồi. Nhưng mình chỉ mua bản tiếng việt. Bây giờ vô tình gặp lại chú dế mèn năm nào trong một cuốn sách ngôn ngữ tiếng anh. Mình đã đặt mua ngay. Kích cỡ của cuốn sách rất to. Chất liệu giấy cực ổn. Sờ vào vừa láng vừa mịn, dày. Sách này là có tranh màu nhé. Rất hợp cho cả trẻ con học tiếng anh. Vì có hình ảnh minh hoạ mỗi trang nên việc đọc hiểu cũng dễ hơn nhiều. Màu sắc in đẹp lắm. Hình ảnh và chữ in to rõ ràng. Mình cũng chưa đọc hết cuốn sách nhưng nhìn qua mình thấy cuốn sách rất là tốt. Mình sẽ dựa vào vốn kiến thức về câu chuyện này trong việc đọc sách tiếng anh. Mình nghĩ nó vừa có tính chất truyện tranh vừa có những ý nghĩa câu chuyện sâu sắc và còn cả góp phần trau dồi tiéng anh nữa. Thật sự rất thích cuốn sách này. 
5
717149
2015-11-24 15:13:05
--------------------------
153764
5884
Tôi vẫn nhớ hồi nhỏ lúc học lớp ba được một người bạn cho mượn cuốn "Dế mèn phiêu lưu ký" và tôi rất thích truyện đó. Tôi đã mượn nó và đọc ngấu nghiến nó. Hiện nay tôi đã học lớp 11, thấy cuốn sách này trên tiki nên đã quyết định mua nó. Cuốn sách đặc biệt hữu ích đối với những bạn muốn trau dồi vốn tiếng anh của mình. Cách dịch của người dịch cũng khá lưu loát và ít lỗi. Về mặt hình thức thì cũng khá ổn. Nói chung là khá vừa ý với cuốn sách này.
4
316220
2015-01-27 12:57:03
--------------------------
