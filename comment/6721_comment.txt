462047
6721
Được bạn giới thiệu và đọc lời đề ban đầu của quyển sách tôi rất thích nên đã quyết định mua. Đúng là quyển sách viết về hành trình của chàng trai đi tìm đạo sống cho mình. Trải qua nhiều thử thách, nhiều trải nghiệm cuối cùng nhận ra bản thân mình là lí lẽ duy nhất. Sách được dịch trực tiếp từ tiếng Đức nên sát nghĩa hơn. Nhưng vẫn khá.... hoa mĩ và chuyên sâu về Đạo Phật. Với những người chưa từng tiếp xúc với Phật Pháp như mình thì vẫn hiểu ý nghĩa tác phẩm truyền tải nhưng cảm giác văn phong hơi khó thẩm thấu. Các bạn nên tìm đọc trước một đoạn trước khi mua. Về hình thức thì phải nói là Nhã Nam quá đỉnh trong việc thiết kế bìa sách đẹp ấn tượng :D
2
42014
2016-06-28 10:55:32
--------------------------
461801
6721
Khi đọc Siddhartha lần đầu tiên, với 2 trang đầu, mình đã bỏ xuống, mình không đọc nổi vì mình là một kẻ ngoại đạo với những từ ngữ đầy âm hưởng Phật giáo ấy. Lần thứ 2 cầm lên, mình đã không thể ngừng được, bởi vì mình đã đọc đến đoạn Siddhartha gặp Đức Phật. Mình đi đâu cũng nhìn thấy mọi người ca tụng Đức Phật vĩ đại, và họ làm theo ý Đức Phật trung thành không một chút nghi ngờ. Nhưng, Siddhartha, người đã hiểu thấu những lời giảng của Đức Phật hơn bất kỳ ai vẫn cảm thấy chưa đủ và quyết định tự đi tìm bản ngã của mình. Bản thân mình cảm thấy rằng không nên gắn mác cuốn sách này là một cuốn sách tôn giáo bởi vì cho đến lúc chết, Siddhartha trở thành một người vô thần và đây là một cuốn sách hay vô cùng. Nó cũng dễ đọc thôi, không không phải ai cũng đủ thấu cảm để hiểu, hiểu được niềm vui, hạnh phúc, nỗi buồn và sự đau đớn thật sự.So với "Nhà giả kim", mình thấy cuốn này hay hơn. Nếu ai tôn sùng vì "Hoàng tử Bé" như mình thì nên đừng nên để lỡ cuốn này.
5
94346
2016-06-28 00:56:43
--------------------------
458320
6721
"Siddhartha" là tác phẩm rất quen thuộc và là một trong những cuốn sách gối đầu giường của biết bao người  của thế hệ trước với bản dịch có tên gọi "Câu chuyện dòng sông". So với bản dịch trước dịch từ tiếng Anh sang, thì bản dịch lần này của Lê Chu Cầu hay hơn, lời văn chuẩn phong cách Hermann Hesse hơn vì dịch trực tiếp từ bản tiếng Đức. Nếu như "Sói thảo nguyên" là tác phẩm nổi tiếng nhất của Hesse, thuộc hàng bestseller thì "Siddhartha" được đánh giá là tác phẩm hay nhất của ông. Tác phẩm kể về chuyến hành trình cuộc đời của chàng trai trác việt Siddhartha đi tìm chân lý cuộc đời với nhiều trắc trở, cạm bẫy. Nhưng rồi chàng đã ngộ ra được chân lý bên cạnh một dòng sông. Chàng đã trở thành hiện thân của một vị bồ tác như người lái đò Vasudeva.Tác phẩm đã đặt ra rất nhiều những vấn đề về triết lý con người, cuộc đời và tôn giáo. Một trong những điểm thú vị nữa trong tiểu thuyết này chính là sự xuất hiện của các biểu tượng rất độc đáo. Đó cũng là yếu tố khiến cuốn sách trở nên khó đọc, nhưng càng đọc càng thấy hay và đọc lại càng nhiều càng ngộ được nhiều thứ. Tóm lại, đây là cuốn sách  rất đáng để đọc trong đời.
5
60352
2016-06-25 01:40:14
--------------------------
451253
6721
Là một tác phẩm phản ánh cuộc sống thông qua tôn giáo, tác phẩm đã mang đến những dư âm sống động cho người độc.
Về mặt hình thức, bìa Nhã Nam luôn khiến riêng cá nhân mình thích thú bởi lối trinh bày dễ nhìn, đẹp và trang trọng.
Về nội dung, tác phẩm xoay quanh nhân vật cùng tên với đức Phât, đang trên con đường tìm kiếm chân lí và đức tin.
Xét cho cùng, tác phẩm rất đáng đọc, đặc biệt với những bạn thích nghiên cứu về tôn giáo và tín ngưỡng. Đánh giá 7.5/10.
3
429966
2016-06-19 15:56:02
--------------------------
412173
6721
Đây chắc hẳn là một quyển sách không thể thiếu dành cho những ai có tín ngưỡng, bất kỳ tín ngưỡng nào hay nhưng ai đang đi tìm cho mình một niềm tin, một tín ngưỡng, đọc Siddhartha tôi thấy được chặng đường dài đi tìm chính mình của Ngài, từ con một người Bà la môn muốn ra đi để đắc đạo cho đến khi đánh mất bản thân vào những thói hư tật xấu rồi đến cuối cùng nhận ra không và tính không. Tôi cũng ngạc nhiên khi Siddhartha đánh mất mình vào nhục dục, tiền bạc và danh vọng nhưng cũng thở phào nhẹ nhõm khi đọc đến cuối quyển sách. Quyển sách nhìn chung viết về triết lý vô cùng sâu sắc cùng lối dịch gần gũi của Lê Chu Cầu ( đồng dịch tác phầm Nhà Gỉa Kim) 
4
740023
2016-04-07 10:46:37
--------------------------
404652
6721
Rất ít sách khiến mình có cảm giác như quyển sách này. Đọc xong gấp sách lại vẫn còn nhiều suy ngẫm nhẹ nhàng vấn vương. Chứ không kiểu hay nhạt nhẽo lúc đọc thấy hay, gấp sách lại trôi tuồn tuột hết. Sách này khiến mình thay đổi suy nghĩ về sách Phật giáo, nếu không phải bạn bè giới thiệu chắc chắn mình không bao giờ chạm vào sách tôn giáo ( vì thấy nó cư ảo diệu như thế nào ấy). 
Chất lượng giấy cũng như bản in khá tốt, về bản dịch tạm thời mình chưa tìm ra lỗi. :)
3
819257
2016-03-25 15:41:10
--------------------------
380576
6721
"Cái mục đích tối thượng của con người chính là hạnh phúc 
Không có tri thức bạn sẽ mãi trong cái vòng luẩn quẩn của hối tiếc và đến già bạn cũng không biết bạn là ai
Có tri thức, tôi kiêu ngạo, tôi muốn đi tìm tôi thực sự là ai. Để rồi trải qua ngàn ngàn khổ đau thì tôi mới nhận ra  tôi chính là giây phút này: không ưu tư, không mưu cầu"
Tôi đã vỡ lẽ ra nhiều nhiều sau khi đọc tác phẩm nay.Tôi đã hiểu Lý do tại sao có những người chìm sâu và rượu, cờ bạc và những trò chơi khác. Đó là bởi vì họ trống rỗng, họ không biết mình là ai
Không đọc bạn sẽ phí mất một đời
Chất lượng giấy in: 5 sao
Chất lượng bản dịch: Do bản dịch có liên quan đến vấn đề tôn giáo nên lúc đầu hơi khó hiểu, sau đó rất dễ
5
919665
2016-02-15 12:58:12
--------------------------
371683
6721
Mình theo Phật giáo nên mua tác phẩm "Siddhartha" cũng là vì sự tò mò. Mình thấy Hermann Hesse là nhà văn nước ngoài nhưng cũng đã chịu khó tìm tòi để viết quyển sách này. Sách kể về cuộc đời của một người đàn ông có tên Siddhartha (được gọi bằng cái tên khác là Đức Cồ Đàm). 
Tác phẩm mang đậm màu sắc triết lý Phương Đông, triết học Phật Giáo. Siddhathar là một cuộc hành trình tìm kiếm, con người cần phải trải nghiệm tận cùng mọi ngõ ngách của cuộc sống nếu muốn thấu hiểu bản chất của cuộc sống; để rồi nhận ra rằng trên tất thảy thì mọi người luôn tìm kiếm sự thanh thản, bình yên trong tâm hồn. Siddhartha là một tác phẩm xuất sắc về Phật Giáo, về niềm tin, về hành trình cuộc sống, và xứng đáng là tác phẩm kinh điển đạt giải Nobel văn chương. Một tác phẩm đáng để đọc nếu muốn hiểu hơn về cuộc sống này. :-)
5
126889
2016-01-20 10:47:34
--------------------------
354652
6721
Khi Govinda (Thiện Hữu) gặp Siddhartha (Tất Đạt) lần cuối, Govinda lẫn Siddhartha đều đã già. Siddhartha thì "đã nhận thấy tiếng dòng sông nói với ông, ông học từ tiếng nói ấy, nó dạy bảo ông, dòng sông như một vị thánh đối với ông" (trang 210). Trong khi Govinda thì "Cho tôi chút gì đấy làm hành trang trên đường đi. Siddhartha ơi, nó thường chông gai lắm, con đường của tôi ấy mà, nó thường tối tăm mờ mịt lắm" (trang 214).
Sự khôn ngoan không thể truyền cho kẻ khác. Trí tuệ mà một người hiền triết cố truyền lại luôn luôn nghe có vẻ điên rồ. Kiến thức có thể truyền được, nhưng trí tuệ thì không. Con người có thể tìm thấy trí tuệ, sống trong trí tuệ, được thêm sức mạnh nhờ trí tuệ, làm nên những phép lạ nhờ trí tuệ, nhưng người ta không thể truyền dạy trí tuệ được.
Mặc dù từ nhỏ hai ông là người bạn thân của nhau, cùng nhau tìm kiếm chân lý, tìm kiếm sự khôn ngoan, thông thái. Song, cuối con đường cuộc đời, Govinda-một kẻ tu hành uyên bác, suốt cả đời đi học nhân gian, kiếm tìm hạnh phúc để mang đến cho đời niềm an ủi-phải xin Siddhartha-một ông lái đò, sau cả một thời trai trẻ kiếm tìm chân lý, danh vọng, ra-vào cõi cực lạc và địa ngục của chính lòng mình-một lời khuyên cho cùng đích đời mình. 
Tìm kiếm quá nhiều sẽ không thể nào gặp được là vậy.
Hermann Hesse đoạt giải Nobel văn chương 1946 là xứng đáng. Thông qua Siddhartha, ta sẽ thấy hạnh phúc tận cùng của con người không phải là khoa học kỹ thuật, là thế giới số, không phải vật chất tiện nghi tầm thường, càng không phải là những thánh đường hay tu viện, mà chính là tình người.
5
317483
2015-12-18 12:27:01
--------------------------
322835
6721
Đây là quyển sách dù không dày nhưng làm khó tôi nhất từ trước đến giờ, vì những "từ chuyên ngành" như Đại Ngã, vô thường... vì những tư tưởng và triết lý Phật giáo. 

Có lẽ sẽ có nhiều bạn giống như tôi, cảm thấy xa lạ và khó hiểu nhưng đừng từ bỏ, cũng đừng cố gắng đọc đi đọc lại một đoạn nào đó cho kỳ hiểu mới thôi, hãy nhẹ nhàng lướt qua và đọc tiếp. Bạn hãy đọc - sống - rồi đọc lại, chắc chắn trong lần quay trở lại "dòng sông" đó bạn sẽ tĩnh tâm hơn, biết cách lắng nghe hơn, từ đó lĩnh hội được nhiều hơn ý nghĩa của tác phẩm cũng như của cuộc sống - giống như chàng Siddhartha đã làm ở dòng sông đời mình.
4
80143
2015-10-17 08:20:51
--------------------------
296969
6721
Lúc đầu cũng không chú ý lắm đến quyển này nhưng vì mình rất thích sách Lê Chu Cầu dịch, chỉ cần người dịch là Lê Chu Cầu thì mình sẽ mua ngay, chú dịch rất mượt, đọc rất thích, vì vậy nên mình đã mua Siddhartha của Herman Hesse. Nếu ai đã từng thích Nhà giả kim (cũng do Lê Chu Cầu dịch) thì có lẽ sẽ thích cuốn Siddhartha này, tuy sách mỏng nhưng khi đọc phải đọc thật chậm và nếu được nên đọc qua lại nhiều lần thì mới thấm được hết những điều mà tác giả viết.
5
543677
2015-09-11 14:21:17
--------------------------
282735
6721
Đây là cuốn sách không phải đọc một lần mà bạn có thể hiểu được ý nghĩa của nó. Càng đọc đi đọc lại nhiều lần bạn mới thấy thấm trong từng câu chữ. Về cuộc hành trình đi tìm chữ Tâm, chữ Tĩnh trong con người. Để nhìn lại bản thân của mình. Siddhartha đúng là một tác phẩm kinh điển của thế giới, đọc từng câu chữ trong đó. Giác ngộ được nó sẽ khiến tâm hồn mình thanh thản, hiểu được lòng mình thì mọi điều mới có thể đạt được. Mỗi lần đọc lại nó, lại có những suy nghĩ khác nhau
5
77305
2015-08-29 13:14:30
--------------------------
281132
6721
Một cuốn sách có quá nhiều ý nghĩa. Mình thấy cuốn này và "Nhà giả kim" là hai cuốn có thật nhiều ý nghĩa nhân sinh quan. Siddhartha là chuyến phiêu lưu của một chàng trai trẻ, cậu bỏ lại gia đình, người thân và lên đường quyết đi tìm giác ngộ. Có khó khăn, có gian khổ nhưng chưa bao giờ cậu chịu lùi bước. Lý tưởng "giác ngộ" luôn lơ lửng trên đầu cậu, là mục tiêu để cậu phấn đấu cả đời. Trong suốt cuộc hành trình, cậu đã trải qua tất cả những hỉ nộ ái ố của đời một con người để rồi cuối cùng nhận ra thứ mà cậu khao khát và hướng đến chính là "sự bình yên trong tâm hồn.", một nơi chốn dừng chân. Siddhartha xứng đáng là tác phẩm kinh điển đạt giải Nobel văn chương
4
388344
2015-08-28 10:01:51
--------------------------
279783
6721
Một cuốn sách về đề tài Phật giáo nhưng cũng có thể khiến người đọc bị cuốn theo những tình tiết của tác phẩm. Siddhartha thích hợp với những người thích đọc sách theo lối chiêm nghiệm, vừa đọc từng trang từng trang vừa nghiền ngẫm từng câu chữ. Cũng thật khó để diễn tả cái hay của cuốn sách. Mỗi người đọc sẽ có một cảm nhận khác nhau, nên cũng thật khó để đưa ra nhận xét cho cuốn sách này, nó là một cuốn sách để người ta đọc và từ từ cảm nhận. Sẽ có không ít người sẽ thấy mình đã , đang và sẽ là chàng trai Siddhartha trong câu chuyện.
4
56881
2015-08-27 09:15:54
--------------------------
267976
6721
Lần đầu tiên mình đọc cuốn "Câu chuyện dòng sông", mình như một con ếch rời khỏi giếng, cảm thấy ý nghĩa cuộc đời mình từ trước tới nay thật nông cạn làm sao. Giờ đây, cuốn sách đó đã được hiệu chỉnh một cách chính xác từ nguyên gốc tiếng Đức. Bản dịch chỉn chu, chú thích rõ ràng, cẩn trọng, lật từng trang sách có thể cảm nhận được tâm huyết của dịch giả dành cho nó. So với những cuốn sách về đạo Phật khác, như các tàng kinh và bài thuyết giảng, cuốn sách này đặc biệt dễ hiểu dễ ngấm. Vậy nên nếu bạn muốn một cuộc sống toàn vẹn cả về tâm hồn và vật chất, hãy cân nhấc!
5
292321
2015-08-15 23:03:32
--------------------------
267583
6721
Hermann Hesse là tác gia lớn và Siddhartha là tác phẩm chứa đựng tư tưởng lớn. Lần đầu đọc qua tác phẩm thật sự mình vẫn chưa thể lĩnh hội được cái ý nghĩa sâu xa mà tác giả muốn truyền tải - nó chỉ là một câu truyện thôi mà (mình nghĩ). Đúng thế phải đến lần thứ hai, thứ ba thì cái triết lý ấy đã thị hiển. Tác phẩm mang đậm màu sắc triết lý Phương Đông theo mình nghĩ gần nhất đó là triết học Phật Giáo. Theo Hermann Hesse thì con người cần phải trải nghiệm tận cùng mọi ngõ ngách của cuộc sống nếu muốn thấu hiểu bản chất của cuộc sống.
5
595446
2015-08-15 17:26:36
--------------------------
256853
6721
Tìm mua cuốn sách khi đã nghe qua lời giới thiệu và khen ngợi của rất nhiều người.
Siddhathar là một cuộc hành trình tìm kiếm để rồi trải qua những hương vị của cuộc sống con người nhận ra rằng trên tất thảy thì mọi người luôn tìm kiếm sự thanh thản, bình yên trong tâm hồn. 
Siddhartha là một tác phẩm xuất sắc về Phật Giáo, về niềm tin, về hành trình cuộc sống, và xứng đáng là tác phẩm kinh điển đạt giải Nobel văn chương. Một tác phẩm mà bạn cần phải đọc đi đọc lại nhiều lần.

4
136669
2015-08-07 10:44:22
--------------------------
254454
6721
Trong cuộc sống hiện tại, con người bận rộn với mọi thứ, đôi khi quên đi rằng mình vẫn còn một tâm hồn cần vun vén, con người luôn mâu thuẫn với chính mình, với bản ngã của mình, sống trong thực tại đầy rẫy những cám dỗ, ít khi chịu nhìn lại mình và xem xét mọi thứ, con người vẫn u mê khi chưa tìm ra ánh sáng, vẫn hằng ngày tìm đến cái khổ nhưng vô tình không biết. Cuốn sách là câu chuyện của một người tìm đến chân lí của cuộc sống, tìm đến sự giải thoát và vượt qua muốn vàn thử thách, để đi tìm về chính mình, để không u mê và có rất nhiều bài học trên đường anh đi.
4
132159
2015-08-05 11:12:48
--------------------------
251908
6721
Con người luôn luôn tìm kiếm. Chúng ta phải kiếm cái ăn, cái mặc, sự giàu sang, sự kính trọng, sự thõa mản... hằng hà sa số mong muốn. Trên con đường tìm kiếm đó, con sông trước mặt là một chướng ngại và chúng ta cần vượt qua nó. Khách đi thuyền trên sông chỉ chăm chăm đích đến là bờ bên kia, họ qua sông rồi lại vội vả chạy theo những mục tiêu đã định sẵn, chỉ có người lái đò ở lại, quay lại bến và tiếp tục đưa khách sang sông. Ông lái đò có thể cũng đã chạy theo những mục đích đời thường như bao nhiêu người khác, loay hoay trong vòng tục lụy nếu không đủ duyên để lắng nghe con sông trước mặt, đủ duyên để lắng nghe tiếng lòng mình, rọi vào đó để thấy cái Khổ, cái Luân Hồi, thấy Niết Bàn để từ đó tìm ra con đường Giải Thoát. Dòng sông chính là sự sống, chứa đựng hàng vạn âm thanh, chứa đựng sự vẹn nguyên trong vô lượng và vô ngã. Chính bởi thế nó cần phải chảy qua mọi thác ghềnh, mọi cửa bể, mọi địa hình, trải qua hết thảy những đau thương, vui thú, dục lạc và bi ai để trở về với Đại Ngã sau khi đã lìa xa và dứt bỏ tiểu ngã của mình. Mọi ngôn từ đều vô nghĩa nếu dùng để truyền đạt trí huệ của một người thế nên chúng ta hãy cảm thọ và thu nhận trí huệ bằng chính sự sống của mình.
"Siddhartha" là một tác phẩm xuất sắc về Phật Giáo. Ngay khi gấp sách lại mình biết cần phải đọc đi đọc lại nhiều lần nữa mới mong hiểu được những huyền diệu mà nhà văn Hermann Hesse đã nói đến, cũng như sự huyền diệu của sự chứng ngộ.
Mong rằng các bạn trẻ hãy đọc quyển sách này vì sự đơn giản mà vi diệu, nhẹ nhàng mà âm vang, sẽ đưa bạn bước gần hơn đến với Phật, đến với sự Toàn Thiện mà vốn đã ở sẵn tự bên trong bạn từ lâu lắm rồi.
5
378725
2015-08-03 10:44:16
--------------------------
240176
6721
Siddhartha cứ êm đềm như dòng chảy của dòng sông, dòng chảy của đời người, trải qua bao khổ ải để rồi nhận ra không gì quan trọng bằng sự thanh thản, bình yên trong tâm hồn. 

Đây là cuốn sách không chỉ để đọc 1 lần, phải đọc nhiều lần thì mới mong hiểu được 1 phần ít ỏi mà Herman Hesse muốn truyền đạt. Dù quyển sách khá mỏng nhưng từ ngữ tôn giáo sẽ làm khó chúng ta ngay từ đầu, có thể chúng ta sẽ đọc hết, nhưng ít có can đảm sẽ đọc lại lần hai. Nhưng dù sao thì hãy thử 1 lần, cũng như phụ nữ, Siddhartha, đọc là để thấm thía, không phải chỉ để hiểu, bạn nhé!
5
637201
2015-07-24 13:10:29
--------------------------
239432
6721
Cuốn sách này kể về cuộc đời của một người đàn ông có tên Siddhartha (tên của Phật Thích Ca Mâu Ni cũng là Siddhartha - nhưng người đàn ông có tên Siddhartha trong truyện không phải là Phật, Ngài được gọi bằng cái tên khác là Đức Cồ Đàm). Siddhartha vốn là con trai của một người Bà La Môn, sau khi sống một tuổi thơ yên ấm bên người thân, anh quyết định gia nhập vào đội ngũ sa môn (một kiểu khất sĩ) với người bạn thân Govinda. Lý do cho việc bỏ nhà ra đi này, là vì anh quá chán nản trước những lời răn dạy khô khan của các bậc tiền bối, Siddhartha muốn tự mình trải nghiệm những lý thuyết đã được dạy dỗ từ nhỏ ấy. Đây là một cuốn truyện với nội dung như một câu chuyện ngụ ngôn, cũng là một cuốn sách mà người đọc phải tự trải nghiệm qua từng câu chữ được tác giả viết ra. Chắc hẳn ai đọc "Siddhartha" đều dễ dàng thấy mình ở đấy, ít nhất là ở một thời điểm trong suốt cuộc đời của Siddhartha.Cũng giống như suy nghĩ của Siddhartha, hãy tự mình trải nghiệm quyển sách này - nó xứng đáng được trở thành một cuốn sách gối đầu giường cho bất cứ ai. Và có lẽ tôi sẽ còn phải đọc nó thêm nhiều lần nữa. 

5
501416
2015-07-23 21:33:04
--------------------------
228153
6721
Với mình có thể gọi đây là một cuộc hành trình vào bên trong bản thân mỗi người.
Như tác giả Ernie đã nói: "Kẻ tìm kiếm bên ngoài mơ mộng, người tìm kiếm bên trong tỉnh thức".
Cuốn sách như một cuộc hành trình từ những niềm tin tôn giáo khác nhau, niềm tin vào cuộc sống khác nhau sẽ thành nhiều nhánh, nhưng vấn đề là không có một con đường nào gọi là chung nhất cho mọi người, đến một lúc nào đó họ phải tự nhận ra và đi bằng con đường của mình.
Những cảm xúc yêu thương, sân hận, kỳ vọng, lòng tham đều được miêu tả chi tiết như vẽ lên bức tranh cuộc đời ngay cả trong thời kỳ này.
Sẽ đọc lại nhiều lần, vì mỗi lần sẽ thấy nó khác.
5
196599
2015-07-15 07:23:40
--------------------------
222900
6721
Theo như tìm hiểu thì sách còn có một cái tên gọi khác là "Câu chuyện dòng sông" do một nhà xuất bản khác phát hành cũng khá lâu rồi. Tựa đề kia hay tựa đề này, thực ra cảm thấy đều rất hay và ý nghĩa, đời người như một dòng sông trôi mãi lúc bình yên lúc dữ dội hay chẳng ai lại tắm hai lần trên cùng một dòng sông; còn Siddhartha là tên phiên âm của Tất Đạt Đa - người làm một cuộc hành trình giác ngộ.
Rất khó để bình phẩm về một câu chuyện tôn giáo, vì luôn có những ý kiến cũng như là nhận định trái chiều, đấy là tùy vào lịch duyệt của mỗi người. Nhưng với mình, đây là một quyển sách rất hay, được chuyển ngữ và biên tập thật chỉn chu tuyệt vời. Tuy còn rất nhiều điều trong sách mình chưa kịp ngộ ra, nhưng những gì gửi gắm trong đó thật sự quá tài tình.
5
30924
2015-07-06 12:59:28
--------------------------
221830
6721
Qua cuộc hành trình đi tìm sự giác ngộ của Tất Đạt Đa(Siddhartha) qua các các tôn giáo, các quan niệm sống và những cám dỗ của cuộc đời. Yếu tố tôn giáo chỉ làm nền cho tác phẩm này, không có việc quan điểm của tôn giáo này đúng hay sai so với tôn giáo khác, mà các tôn giáo đều có chung một đích đến chỉ khác nhau ở con đường thôi mà nhiều người không nhận ra. Qua ẩn dụ nhân vật chính Siddhartha cùng tên, sống cùng thời với đức Phật Như Lai và anh còn từng trò chuyện với Phật, đến cuối truyện khi Tất Đạt Đa ngộ ra chân lý, tuy cách diễn giải của anh khác với đạo Phật nhưng anh biết ngôn từ chỉ là cái vỏ bề ngoài, còn bên trong anh đã hoàn toàn giác ngộ như đức Phật. Mà điều Siddhartha ngộ ra hết sức đơn giản nhưng lại vô vàn sâu sắc rằng mọi vật đều có linh hồn, rằng còn người là một tiểu vụ trụ, ai hiểu được tâm hồn mình hiểu được cảm xúc của mình thì hiểu được vụ trụ. Và các vị thánh nhân đã từng ngộ ra chân lý này như đức Phật, Lão Tử, Trang Tử đều có nhắc đến điều này nhưng với ngôn ngữ và cách thể hiện khác nhau. Vì chủ đề tôn giáo chỉ làm nền nên cuốn sách này không hề có chút nặng nề hay tranh cãi tôn giáo nếu bạn đã say mê "Nhà giả kim" thì cũng nên đọc thêm tác phẩm này.
5
507362
2015-07-04 15:04:31
--------------------------
168910
6721
Nếu bạn chưa biết hay đã biết đến về quyển sách này, theo tôi, bạn nên mua nó. Thứ nhất, nó được nhà văn Nobel Hermann Hesse của Đức sáng tác. Thứ hai, với tâm thế của một người có tôn giáo (Phật) bạn có thể tìm tòi trong đó cái màu nhiệm ngay giữa cuộc đời của một con người như bao người khác. Thứ ba, nó được dịch giả Lê Chu Cầu dịch nên rất mượt :) Bạn nào đã đọc qua các tác phẩm mà bác đã dịch như "Nhà giả kim" (của Paulo Coelho) hay "sói thảo nguyên" (cũng của Hermann Hesse) sẽ nhận ra và thích ngay.

Cũng theo tôi, bạn có thể mua thêm quyển "Bên rặng tuyết sơn" (của Swami Amar Jyoti) do Nguyên Phong dịch vì nó cùng tông với quyển "Siddhartha" này. Đều nói về cuộc đời và số phận của 1 con người từ tấm bé đến khi trưởng thành và giác ngộ. Đặc biệt với các bạn theo Phật giáo sẽ rất tâm đắc nó như tôi.

Tôi thường để chung 4 quyển này với nhau. Có thể có chút khác biệt (tuy không nhiều) về tôn giáo, nhưng thiết nghĩ, bất kỳ ai trong chúng ta cũng đều có nhu cầu đi tìm bản ngã và sự giác ngộ cho riêng mình. Ta là ai? Tại sao ta sinh ra? Và ta phải làm gì?
5
108828
2015-03-17 12:54:15
--------------------------
135825
6721
Siddhartha là một trong số những quyển sách hay nhất về tôn giáo mà tôi đã từng đọc qua. Đây là quyển sách rất đáng đọc nếu bạn có theo một tôn giáo nào đó. Bản dịch này được dịch từ ngôn ngữ gốc là tiếng Đức do tác giả viết nên có lẽ bám sát nội dung gốc hơn. Hiện ở VN có 1 bản dịch khác của quyển này với tựa là "Câu chuyện dòng sông" được phổ biến rộng rãi hơn (có bản đọc audio rất hay trên internet). Sách hơi cao siêu nhưng nếu bạn đọc sang đến chương 3 thì chắc chắn bạn sẽ thấy rất thú vị và muốn đọc tiếp tục cho đến khi hết. Đọc quyển sách này đem lại cho tôi một cảm giác rất bình an và nhẹ nhàng. Đây thực sự là một tác phẩm rất tuyệt vời.
5
67603
2014-11-17 16:51:17
--------------------------
