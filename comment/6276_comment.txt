433295
6276
Mua vì thấy sách được giới thiệu và nhận xét hay quá, nên mình hi vọng lúc nhàn rỗi sẽ được đọc 1 tác phẩm li kì. Cơ mà chắc sách không hợp với mình nên mình không thích lắm. Đọc hoài mà phần đầu cứ nói mãi đến những ham muốn rồi chuyện giường chiếu. Mình cũng không anti gì chuyện đó, tuy nhiên mình hi vọng có nhiều đất hơn cho những nội dung khác Âm mưu hay tính toán của các nhân vật cũng không làm mình phải hồi hộp hay trầm trồ. Nói chung là mình cảm giác nội dung hơi nhàm chán
3
295534
2016-05-20 19:24:45
--------------------------
388768
6276
Mình mua cuốn này vì thích thể loại truyện có yếu tố kì ảo, phép thuật bla bla...
Và mình ưng nó từ câu giới thiệu này : "Với mỗi trang sách, độc giả lại tò mò liệu chàng cảnh sát đẹp trai Todd Blakely có đúng là hiệp sĩ mặc áo giáp mà Myra cần, hay phép thuật và dòng chảy thời gian sẽ chia lìa họ? ByBee đã làm được một việc hết sức phi thường là đưa cuốn tiểu thuyết lãng mạn thuộc thể loại Time Travel của cô chạm đến được trái tim độc giả. Trí thông minh và óc hài hước của những nhân vật cùng với cảnh đẹp như tranh vẽ là những yếu tố khiến độc giả mê mẩn. Từng trang sách là những tình tiết đan xen giữa quá khứ với tương lai, bằng bút pháp tài tình - đôi chút phép màu, ít gam lãng mạn và tí ti hài hước." Mình bị mê ngày từ khi đọc giới thiệu, lại nghe nói Catherine Bybee là độc giả nổi tiếng nữa. Nhưng điểm trừ là bìa của nó không đẹp cho lắm, nhìn hình ảnh cô gái hơi thô.
3
602517
2016-02-29 17:11:39
--------------------------
237976
6276
Mặc dù mới chỉ đọc được nửa cuốn nhưng mình thấy nội dung khá hấp dẫn, li kỳ, lãng mạn. Nhân vật nữ chính du hành từ quá khứ đến tương lai là một tình huống độc đáo, cuộc gặp gỡ như định mệnh của Todd và Myra kéo theo nhiều biến đổi về cảm xúc của hai người gây nhiều xúc động và cuộc chiến chống lại mụ phù thuỷ Grainna vừa hấp dẫn, kịch tính khiến cho cốt truyện đầy màu sắc và mang nét huyền bí. Hi vọng sau này truyện sẽ được tái bản một cách còn tốt hơn nữa và được nhiều người hưởng ứng.
4
435343
2015-07-22 21:37:58
--------------------------
216111
6276
Mình cũng chỉ mua cuốn sách này một cách ngẫu nhiên thôi và lí do vì sao mình cho nó 3 sao bởi vì :
 1. Nội dung được giới thiệu trong cuốn sách vô cùng hấp dẫn, khi mới mua mình  nghĩ sẽ có một cuộc chiến rất hoành tráng nổ ra trong này, tuy nhiên nội dung lại mình hơi thất vọng một chút.
2. Có lẽ nên gắn mác 18+ cho cuốn sách này, hoặc tại mình khó tính quá :) nhưng có một số phần trong cốt truyện khá là....
Tuy nhiên, nếu cuốn sách này có phim thì mình vẫn sẽ xem :) vì khung cảnh được tác giả  xây dựng lên khá là kích thích người đọc :)
3
116557
2015-06-27 10:30:03
--------------------------
215096
6276
Có 2 lí do mình để 4 sao cho truyện
Thứ nhất là mình không thích bìa truyện lắm, hình ảnh cô gái hơi thô @@~
Thứ 2 là như tiêu đề của mình, hơi khó hiểu. Mình không đọc nhiều văn học phương Tây vì thấy không hợp giọng văn và thường bỏ dở khi chưa đọc hết. Nhưng đây là lần đầu tiên mình đọc hết một quyển truyện trong một buổi tối. Nội dung truyện khá hấp dẫn, truyện ca ngợi tình yêu chân thành, sâu sắc, gắn bó vượt không gian và thời gian. Không chỉ với hai nhân vật chính, các cặp phụ cũng rất dễ thương, đáng yêu và thú vị. Có điều, cho tới cuối truyện, khi mà mụ Grainna biến thành con chim rồi bay mất, tiếp sau đó là cảnh mọi người từ chiến trường trở về  thì mình không hiểu lắm. Như vậy thì cuối cùng mụ Grainna có chết không vậy? Vẫn chưa phải không? vì câu nói cuối cùng của bố Myra
4
548594
2015-06-25 21:32:42
--------------------------
212717
6276
Cũng giống với cuốn đầu 'Phía sau một lời thề', cuốn thứ hai là về tình yêu gần như được gọi là tiếng sét ái tình. Tuy Todd chưa thật sự yêu Myra ngay nhưng nhìn sự ngây thơ, mong manh dễ vỡ của nàng, anh chỉ muốn chăm sóc bảo vệ nàng. Còn Myra nhìn anh lần đầu nhưng lại cảm thấy anh rất đáng tin tưởng và có thể dựa dẫm. Tình yêu của 2 người cứ thế tiếp tục nảy nở như vậy, vô cùng nhẹ nhàng và lãng mạn. Tuy cuốn sách không kịch tính, vẫn kết thúc như cuốn #1, tạo ra lối thoát cho mụ phù thủy Grainna để tiếp tục cho cuốn tiếp theo, nhưng cuốn sách vẫn cứ cuốn hút theo cách của nó đến lạ kỳ mà một khi đẫ cầm lên rồi thì không thể bỏ xuống và muốn đọc đi đọc lại  mà không thấy chán. hơn nữa mở dầu cuốn #2 này cũng không nhàm chán như cuốn #1, cũng có thể do hai nhân vật chính gặp gỡ nhau ngay từ chương đầu.
5
108098
2015-06-22 19:46:44
--------------------------
181442
6276
Mình đã mua quyển truyện này rồi và cảm thấy rất thích. Bìa truyện mang gam màu tối rất thích hợp với thể loại này, bìa truyện khá đẹp mắt. Không những bìa đẹp mà nội dung cũng hay và lôi cuốn. Nữ chính là một trinh nữ phải du hành vượt thời gian từ thế kỉ 16 để đến thế kỉ 21để trốn thoát bàn tay mụ phù thuỷ bị huyền rủa. Đến với thế kỉ 21 nàng may mắn gặp được anh chàng cảnh sát tốt bụng và sau cùng hai người trở về thế kỉ 16 rồi kết hôn. Tại đây đã xảy ra 1 cuộc chiến ác liệt với mụ phù thuỷ đáng nguyền rủa kia và rồi mụ ta đã trốn thoát. Mình đã mong đợi quyển tiếp theo từ mấy tháng nay, mong là sẽ sớm xuất hiện trên tiki.
5
408083
2015-04-11 21:40:22
--------------------------
161783
6276
So với “Phía sau một lời thề” thì cuốn 2 này khá hơn hẳn, nội dung hay và hấp dẫn hơn, bớt hẳn mấy cảnh dài dòng mà thay vào đó là đi sâu vào phần trọng tâm của truyện. Nhiều chuyện dở khóc dở cười xảy đến với hai nhân vật chính khi được khám phá thế giới thật mà người kia đang sống, tình yêu của họ cũng rất đẹp và dễ thương. Những tình huống gay cấn trong truyện khá hay, thích nhất là đoạn anh em trong nhà cùng nhau hợp sức để chiến đấu với Grainna nhưng cuối cùng thì cũng chỉ tạm xua đuổi mụ đi một thời gian ngắn, còn lại phải chờ BV xuất bản những cuốn tiếp theo trong series. Bên cạnh đó, tôi cũng rất thích tính cách mạnh mẽ của Myra, so với tuổi của mình thì Myra trưởng thành và chín chắn hơn rất nhiều, Todd cũng thế cái cách mà anh yêu chiều Myra và sự cố gắng để hòa nhập với cuộc sống nhà MacCoinnich của anh thật đáng yêu.

Nhìn chung thì tác phẩm này ổn hơn rất nhiều kể cả lối dịch, cách mô tả các nhân vật trong truyện có chiều sâu và có thể nhìn thấy rõ sự thâm hiểm trong những âm mưu của Grainna. Truyện hội tụ đủ các yếu tố lãng mạn, giả tưởng, gay cấn và cũng không ít những niềm vui, sự hài hước của từng nhân vật.
4
41370
2015-02-28 22:07:33
--------------------------
124935
6276
"Thề ước thầm lặng" thuộc thể loại Time Travel. Catherin Bybee đã rất tài tình khi kết hợp giữa yếu tố siêu thực và lãng mạn trong tác phẩm của mình. 
Myra dấn thân vào 'cuộc chiến' của chính mình khi cô buộc phải hoặc chạy trốn tới tương lai hoặc chết. Và câu chuyện tình yêu của Myra và anh chàng cảnh sát Todd bắt đầu.
Bên cạnh đó, có lẽ rút kinh nghiệm từ cuốn trước mà 'Thề ước thầm lặng' có lối dịch thuật hay hơn hẳn, không bị 'sến sẩm' như vài đoạn 'khủng khiếp' trong 'Phía sau một lời thề'. 
Rất hy vọng trong cuốn thứ 3 của series này sẽ được Bách Việt biên tập và cho ra mắt hoàn thiện hơn nữa.
4
125574
2014-09-10 08:29:32
--------------------------
