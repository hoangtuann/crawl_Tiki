197370
11248
Nhân tài của bạn - Họ là ai theo tôi sẽ là một cuốn cẩm nang hữu ích dành cho những người đã, đang và sẽ làm công tác tuyển dụng trong các doanh nghiệp. Quản trị nhân sự là một việc làm không hề dễ dàng. Chúng ta sẽ phải tìm ra nhân tài giữa muôn vàn những người bình thường, người thông minh, năng động giữa biển người lười biếng. Ngoài ra, quyển sách còn tư vấn và cung cấp cho chúng ta những cách để thực hiện chức năng của nhà quản trị nguồn nhân lực. Đây xứng đáng là một quyển sách gối đầu giường của các nhà quản trị nguồn nhân lực.
5
387632
2015-05-17 14:28:43
--------------------------
