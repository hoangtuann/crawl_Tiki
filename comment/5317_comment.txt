479066
5317
Đây là một cuốn sách có thể đem lại sự giác ngộ và thay đổi cuộc sống. Bạn sẽ sống đơn giản hơn, hạnh phúc hơn và có cái nhìn vị tha hơn với người khác.
Nhưng đây là một cuốn hoàn toàn không dễ đọc, bạn thậm chí cần đọc đi đọc lại 1 câu nhiều lần để ngấm được ý nghĩa của nó. Mình gợi ý nên highlight những ý nổi bật, vì chắc chắn bạn sẽ cần đọc lại nhiều lần trong đời.
Mình đã viết review rất chi tiết về cảm nhận sau khi mua sách từ Tiki, nhưng nó rất dài nên mình không thể post lên đây được. Nếu bạn cần lý do để đặt mua cuốn sách này thì hãy đọc thử review nhé: https://waterlemon.xyz/review-sach-mon-qua-cua-su-khong-hoan-hao/
5
68408
2016-08-07 10:37:38
--------------------------
470531
5317
Món quà của sự không hoàn hảo - giống như cái tên của tác giả đưa ra, nội dung của cuốn sách xoay quanh cách nhìn nhận vấn đề từ chính những khiếm quyết của bản thân. Thực ra, trước giờ mình ít khi đọc những thể loại sách này, vì theo mình nghĩ những loại sách này thường là những triết lý khó hiểu và khó đọc, rồi tình cờ qua một người bạn khuyên mình rằng nên đọc thử cuốn sách này, sẽ giúp bản thân nhận ra nhiều điều trong cuộc sống, đặc biệt là nhìn lại những khiếm quyết trong của bản thân. Thế rồi mình lên mạng tìm hiểu về cuốn sách, ghé qua Tiki thấy đang giảm giá, mình quyết định. Sau khi đọc xong cuốn sách, mình cảm thấy đây là một cuốn sách hay, đầy ý nghĩa, nội dung đầy tính nhân văn, cách hành văn dễ hiểu. Cảm ơn Tiki đã mang đến cho độc giả như mình những cuốn sách hay.
5
1187854
2016-07-07 17:12:04
--------------------------
430810
5317
Vâng, Món quà của sự không hoàn hảo là cuốn sách hay nhất mà tôi từng đọc. Có lẽ Brene Brown là một trong số ít những nhà nghiên cứu về một lĩnh vực tuy hiện diện trong mỗi con người và có tầm ảnh hưởng lớn nhưng lại ít được quan tâm và bàn luận. Bởi lẽ ai cũng biết nó nhưng lại sợ phải nói ra chính nó - sự Hỗ thẹn! Bạn ơi, hai chữ "Hỗ thẹn" này không phải chỉ đơn giản mà hiểu được một cách trọn vẹn ngay lập tức. Tôi đã đọc quyển này và một quyển nữa của bà là Sự liều lĩnh vĩ đại và chúng đã giúp tôi hiểu hiện tượng tâm lý này rõ ràng hơn. Nó đã góp phần thay đỗi cuộc sống của tôi. Cảm ơn Brene Brown!
4
815542
2016-05-15 21:51:20
--------------------------
316257
5317
Đây là một trong những cuốn sách cẩm nang cuộc sống hay nhất mà mình từng đọc. Sách không quá dày nhưng bên trong chứa đựng những nội dung vô cùng bổ ích, các câu chuyện, lời khuyên giúp cho mỗi người biết cách bằng lòng với sự không hoàn hảo trong cuộc sống của mình để từ đó nắm bắt được chiếc chìa khóa của hạnh phúc. Mình rất hài lòng về cuốn sách cũng như phong cách làm việc nhanh chóng của Tiki, mình ở Huế mà Tiki giao sách cho mình chỉ trong vòng 3 ngày thôi.
4
138880
2015-09-30 15:45:24
--------------------------
199027
5317
Đây là một cuốn sách khá hay và nội dung được tác giả gửi gắn cũng khá đặc biệt nữa. Từ một người cho mình là vô dụng, ít được quan tâm và cảm thấy mỗi ngày trôi qua đối với mình nó cứ nhàn nhạt một màu thì sau khi đọc quyển sách này tôi lại có cái nhìn hoàn toàn khác hẳn về cuộc sống này. Quyển sách đã chỉ ra rằng không phải gì riêng mình mà còn có rất nhiều người cảm thấy cuộc sống này đơn điệu, họ dễ bị tổn thương, dễ hổ thẹn và họ cố gắng dùng hết sức để khuất lắp những khuyết điểm của mình trong mắt người khác... Thay vì phải như vậy thì tại sao mình lại không tập đối diện với chính điều này, dũng cảm biểu hiện những sở thích của mình và bày tỏ những khuyết điểm của mình để mọi người cùng san sẻ nhỉ?
4
274995
2015-05-21 12:48:45
--------------------------
156493
5317
Cuốn sách này thật sự rất khó đọc, khó đọc ở đây nghĩa là rất khó để hiểu được, để theo kịp những ý tưởng, những suy nghĩ, những ví dụ của tác giả nhưng chắc chắn một khi bạn làm được những điều đó, nó sẽ giúp bạn thay đổi cuộc sống. 

Tác giả phân tích rất sâu và rất kỹ nguyên nhân ẩn chứa đằng sau những hành động của mỗi chúng ta. Cũng là một cuốn sách về phát triển bản thân thôi, tuy nhiên những vấn đề mà tác giả nhắc tới như sự dễ tổn thương,   sự hổ thẹn , sự che dấu khuyết điểm ... lại là những vấn đề vừa rất cũ vừa rất mới lạ. Qua các nghiên cứu của bà,  tác giả đã cho người đọc hiểu được rằng, nguồn gốc của hạnh phúc, của thành công , của  sự hài lòng về  cuộc sống chính là sự dũng cảm đón nhận và chấp nhận những tổn thương. Thật khó để chấp nhận sự thật này ngay cả với chính tác giả, thế nhưng một khi đã chấp nhận nó và thực sự  sống như  vậy,  chúng ta mới có thể hạnh phúc, thành công và thật sự tự hào , hài lòng về cuộc sống của chính mình.
5
22174
2015-02-04 23:19:35
--------------------------
142903
5317
Tựa đề của cuốn sách này không thu hút mình xíu nào. Điều mình quan tâm lại là tên tác giả Brené Brown - mình từng xem hai bài nói chuyện của cô ấy trên TED. Mình thật sự ấn tượng về điều cô ấy nghiên cứu. Và khi đọc thử, cô ấy nói về lòng dũng cảm - đây là điều mình quan tâm gần đây và thế là theo cuốn này luôn.

Về kết cấu của cuốn sách, mình thích sự đơn giản nhưng chính xác của câu chữ. 

Về cách cô ấy kể chuyện, mình nhìn ra đó cũng là cách mình và các bạn nữ xung quanh mình kể chuyện: nói ra những cảm xúc, nói trình tự câu chuyện ấy diễn ra như thế nào chứ không hẳn phải nhấn vào một điều gì đó và loại bỏ điều khác. 

Mình mong được đọc những tác phẩm tiếp theo của tác giả này và cám ơn chị Uông Xuân Vy và Vi Thảo Nguyễn đã mang đến cho mình cơ hội tiếp cận với tác giả và nghiên cứu của cô ấy.
5
32038
2014-12-20 15:39:21
--------------------------
