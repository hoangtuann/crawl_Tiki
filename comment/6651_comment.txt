329698
6651
Những câu chuyện cổ tích Việt Nam đặc sắc luôn mang tới cho bạn đọc những "tấm vé" để được trở lại với cái thời "ngày xửa, ngày xưa....". Và với câu chuyện "Đồng tiền vạn lịch" này cũng vậy, đọc truyện ta biết được nguồn gốc ra đời đồng tiền Vạn lịch của cha ông ta thời trước, đồng thời qua đó thấy được tình cảm, tình nghĩa vợ chồng của vợ chồng người lái buôn nọ. Giá trị con người, giá trị của cuộc sống được quyết định là do bản thân của mỗi người.
Hình ảnh cũng như câu từ trong truyện sinh động, vừa đọc vừa xem tranh rất thú vị.
4
680594
2015-11-01 10:55:08
--------------------------
