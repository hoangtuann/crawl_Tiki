289351
3094
Mình thích quyển luyện đề này vì phần Reading không chỉ có sao chép thông tin từ bài thành câu trả lời, mà đỏi hỏi phải tư duy cao hơn một tí. Mình dùng quyển này để dạy luyện đề IELTS các lớp trên trung cấp, phù hợp cho các bạn thích được thử thách và suy luận. Tiki cũng giao hàng rất nhanh, mình đã từng nhận đơn hàng có CDs bị gãy, nhưng Tiki giao hàng thì như mua tại tiệm gần nhà luôn, còn mới nguyên. Chỉ tiếc là mình chưa kịp biết đến bookcare để được gói sách sớm hơn, nên thấy mọi người khen, hơi tiếc.
4
730513
2015-09-04 13:36:10
--------------------------
