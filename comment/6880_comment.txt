388813
6880
Mình mua Tặng nhóc cháu dịp Sinh nhật, tiện đọc ké luôn. Đây là một cuốn sách hay, thú vị, sách thiếu nhi nhưng rất có ý nghĩa. Đọc sách này ta tìm thấy đâu đó hính ảnh của bản thân, như tìm lại được tuổi thơ của chính mình với những cuộc phiêu lưu thú vị, với những lần trốn đi chơi, tìm tòi khám phá những sự vật xung quanh, những điều mới lạ mà ta chưa từng được thấy, được biết đến. Dùng làm quà cho các bạn nhỏ khoảng lớp 4, lớp 5 trở lên rất hay. 
4
513404
2016-02-29 18:51:00
--------------------------
377829
6880
Lần đầu đặt sách ở Tiki. Công tác đặt sách nhanh gọn lẹ mà còn có thể xem tiến trình sản phẩm nữa. Sách giao tới nhanh, mới đặt hôm qua hôm nay đã có hàng và được đóng gói kĩ. Giá bìa được giảm nhiều nhưng chất lượng sách vẫn tốt. Mình đặt hai quyển Bù xù, tàn nhang và con vẹt nhại với quyển Đấu trường sinh tử. Bìa đẹp, giấy đẹp. Nội dung của ba quyển thì khỏi chê, chắc chắn hay. Mình mong là Tiki sẽ thêm nhiều sách của tác giả Enid Blyton và danh mục sản phẩm nha. ^^
4
1138511
2016-02-03 14:42:32
--------------------------
292579
6880
Enid Blyton đúng là tác giả kinh điển của dòng truyện thiếu nhi, mình vẫn thấy không khí phiêu lưu hấp dẫn của Bộ năm lừng danh. Những người bạn trẻ trải qua kỳ nghỉ hè đáng nhớ với một cuộc phiêu lưu trên đảo cùng người bạn đồng hành là cô vẹt thông minh thêm phần thú vị. 
Bìa và giấy in được, hình bìa đẹp bắt mắt nhưng mình thấy giá hơi mắc so với Bộ năm lừng danh hay những truyện thiếu nhi cùng khổ cùng số trang. Bộ này không biết có bao nhiêu tập, mình vẫn đợi Alphabooks xuất bản tiếp để sưu tầm.
4
49203
2015-09-07 14:28:18
--------------------------
