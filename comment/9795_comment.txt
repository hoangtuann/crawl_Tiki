320431
9795
30 năm cải cách kinh tế ở Trung Quốc cho chúng ta thấy những thành phố đại đô thị lớn như Thâm Quyến, Thượng Hải,... Đất nước ta cũng đang trên đà phát triển với nhiều khu kinh tế và đặc khu kinh tế được thành lập. Chúng ta cần có thể học hỏi gì từ những người đi trước, để đưa nước ta thành công với những mô hình mở cửa này? Một phần câu trả lời có trong chính cuốn sách " Kỳ Tích Phố Đông - 30 Năm Phát Triển Kinh Tế Ở Trung Quốc". Cuốn sách cũng cho chúng ta thấy chính những vấn đề mà chúng ta đang gặp phải trong công cuộc cải cách như xây dựng mang tính quy hoạch, bảo vệ quyền lợi của người dân bị mất đất,... Một quyển sách đáng để đọc và học hỏi
4
819572
2015-10-11 10:36:11
--------------------------
