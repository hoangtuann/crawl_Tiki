484413
11326
Mình mua sách để tặng.
Sách được gói và bao lại rất đẹp
Nhưng khi mở ra bên trong thì bị dính dơ và ố đốm vàng khá nhiều, ở cả phía trên sách nữa. Làm mình hơi thất vọng vì như vậy ng nhận sẽ có cảm giác sách cũ :(
Chỉ riêng cuốn 2 này là bị. Tập 1 và 3 thì ổn.
Sách thì mình chưa đọc nên k nhận xét. Màu giấy tập 1 và 2 đẹp, kem nhẹ. Dễ đọc và dễ chịu cho mắt. Riêng tập 3 không hiểu sao màu sách ngả vàng hơn. Đọc  không thích bằng. Vài nhận xét vậy
2
1654204
2016-09-23 19:13:46
--------------------------
367933
11326
Phần 2 của loạn thế anh hùng dần dần rõ ràng hơn về góc độ phản ứng của nhân vật  dành cho các tình tiết mới , nội tâm của Cảnh thương hoài và dịch bôi tửu có nét đi sâu vào khai thác song làm hơi ngao ngán sức tưởng tượng chưa có nhấn mạnh về cá tính của từng người mà làm quên đi ngay đặc điểm mấu chốt của nó , vậy thì câu truyện còn đề cập đến ai nữa hay phải tìm hiểu những gian nan mà họ đã từng di chuyển đến , phân tích chưa sâu .
4
402468
2016-01-13 00:10:29
--------------------------
338665
11326
Cầm đến quyển thứ 3 thì mình thực sự cảm thấy thất vọng. Vì sao ấy à? Đáng lẽ nhà xuất bản nên gộp cả 3 quyển vào 1 để mình có thể đọc một lèo luôn. Ai có thể hiểu cảm giác mang một tập đến lớp đọc hết rồi nhưng lại không mang theo quyển sau để đọc nữa chứ. Chưa kể mỗi tập đều khá mỏng, hoàn toàn phù hợp để ghép lại, vừa đỡ tiền vừa đỡ công sức mang đi mang lại. Đối với loại tiểu thuyết lần đầu tiên mình tiếp xúc này thì Loạn thế anh hùng đã để lại ấn tượng hoàn toàn tốt đẹp. Cầm trên tay tập cuối, mình cứ tiếc mãi vì truyện hết nhanh quá mà mình thì cứ muốn đọc mãi thôi ý. 
5
192908
2015-11-16 17:59:02
--------------------------
290352
11326
Đỡ hơn phần đầu thì tập 2 có vẻ có chuyển biến tốt về mặt sắp xếp bố cục và mạch truyện mặc dù lỗi này lỗi nọ và việc lộn xộn của truyện vẫn không làm lơ được. Chính vì như thế này nên tất cả mọi thứ trở nên nhạt nhòa, nhân vật cũng không khai thác được kĩ, cả bề ngoài lẫn bên trong đều giống kiểu bị bỏ lửng và làm cho xong chuyện như nhiều người cũng đã nói ở dưới. Nhưng dù sao mình cũng ghi nhận và trân trọng suy nghĩ và cách viết riêng của tác giả đã dồn tâm huyết viết ra bộ truyện này. 
4
91802
2015-09-05 11:57:19
--------------------------
283879
11326
Trong tập 2, bút pháp của tác giả thể hiện sự tiến bộ rõ nét so với tập 1. Các tình tiết được sắp xếp mạch lạc, rõ ràng và hợp lý hơn, không còn lan man lộn xộn như trước. Dù hệ thống nhân vật khá phức tạp nhưng đọc vẫn dễ hiểu (tập 1 có nhiều chỗ đọc thấy hơi mệt óc). Nội dung của tập 2 cũng thú vị hơn, một phần là mình cũng bắt đầu quen với văn phong người dịch nên thấy thôi thì tạm chấp nhận được. Hơi tiếc là nhiều nhân vật chưa được khai thác hết, đáng lý nên được cho thêm đất diễn thì tác giả lại miêu tả tâm lý qua loa. 
4
57459
2015-08-30 13:39:45
--------------------------
279498
11326
So với tập đầu tiên thì tập hai này có vẻ hay hơn, thực sự mình thấy khâu dịch sách vẫn chưa ưng ý lắm nhưng chỉ đôi chỗ thôi, nhìn chung vẫn phản ánh được phong cách viết tinh tế của tác giả. Truyện có chỗ không đi sâu, lửng lơ để người đọc suy nghĩ, tác giả xây dựng hệ thống nhân vật khá phức tạp nhưng chỉ gói gọn trong 3 tập sách, mình thấy hơi phí, hoàn toàn có thể mở rộng thêm ra được nhiều nữa. Đây là tác phẩm đầu tay của Tiểu Đoạn và mình đánh giá thế này là đã thành công rồi,
4
60677
2015-08-26 21:09:11
--------------------------
157065
11326
Mới vừa xem xong quyển 2 Loạn Thế Anh Hùng của Tiểu Đoạn. Nếu như ở quyển 1 nội dung tình tiết của truyện tản mát rời rạc toàn tập thì quyển 2 đã có sự tiến bộ, mọi thứ đã đi vào quỹ đạo, rõ ràng hơn nhiều.
Cách dẫn dắt câu chuyện của Tiểu Đoạn cũng rất mới lạ độc đáo, đặc sắc nhất chính là phần chuyển tiếp giữa nhân vật này qua nhân vật khác. Tuy kể về mỗi nhận vật độc lập nhưng nội dung của câu chuyện vẫn liền mạch, trơn tru. Cách kể này mặc dù độc lạ mới mẻ nhưng nhược điểm là không đi sâu vào nhân vật,chỉ thấy được sự dàn trãi mà không thấy được chiều sâu, nhất là tâm tư tình cảm suy nghĩ của nhân vật. Dù trong quyển 2 này có thể hiện nội tâm của Cảnh Thương Hoài, Dịch Bôi Tửu,….nhưng chỉ là lướt qua chứ không đào sâu. Tôi hứng thú muôn xem tiếp những nỗi niềm  những trăn trở trong lòng họ thì Tiểu Đoạn đột ngột kết thúc chuyển qua nhận vật khá làm tôi khá bất mãn kiểu như phim đang tới khúc hay bỗng dưng cúp điện, nóng muốn hộc máu.
Tóm lại, quyển 2 này từ bố cục, nội dung, tình tiết, thậm chí tình cảm trong mạch truyện đều hay hơn quyển 1.
4
180185
2015-02-07 15:21:40
--------------------------
