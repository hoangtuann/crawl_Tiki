474953
8916
Đây là lần đầu tiên mình đọc thể loại truyện này. Ban đầu đặt hàng vì được dịp tiki khuyến mãi giảm giá 70%, nghĩ là giá hạt dẻ quá nên mua đọc kèm cùng với đơn hàng. Tuy nhiên, khi đọc qua truyện mình không dứt ra được, cái cách đan xen và dẫn truyện của tác giả vô cùng tinh tế, qua đây mình cũng hiểu hơn phần nào cuộc sống trước đây, thời kỳ chiến tranh cũng như cuộc sống mà cha ông đã từng trải. Đúng như tên truyện "Hệ lụy" là tác phẩm nên đọc, mong là tiki sẽ có nhiều tác phẩm như này hơn nữa. Sẽ ủng hộ tiki trong tương lai nhiều !
4
914030
2016-07-12 11:28:17
--------------------------
