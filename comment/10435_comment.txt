53700
10435
Lucky Luke từ lâu đã là một tác phẩm kinh điển, được nhiều thế hệ độc giả yêu thích nồng nhiệt. Thực sự cuốn truyện có nhiều điểm thú vị có thể thu hút nhiều lứa tuổi khác nhau. Trong truyện có những hình vẽ đáng yêu, ngộ nghĩnh, nhưng cốt truyện chân thực, sáng tạo và bất ngờ, sẽ làm cho cả trẻ em và người lớn cảm thấy lôi cuốn và hào hứng. Những tình tiết trong truyện rất hấp dẫn, đôi khi gay cấn, hồi hộp. Ở đó, nổi bật lên là hình ảnh chàng cao bồi, cùng những khung cảnh của miền Tây viễn du hoang sơ mà kỳ thú. Đến với cuốn sách cũng như đến với một hành trình có một không hai, khám phá thêm nhiều điều diệu kỳ của cuộc sống và thế giới rộng lớn.
4
20073
2013-01-04 16:33:03
--------------------------
