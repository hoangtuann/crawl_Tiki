495401
10984
Người Phụ Nữ Giàu - Kiểm Soát Đồng Tiền Quản Lý Cuộc Đời đây là cuốn sách giúp có những suy nghĩ mới cho những người phụ nữ muốn độc lập tài chính, rất ý nghĩa, có điều nên đọc Dạy con làm giàu để hiểu sâu sắc hơn.

5
1000972
2016-12-12 18:34:54
--------------------------
462333
10984
Là một người tự chủ về tiền bạc nhưng đôi lúc thật sự tôi không biết kiểm soát ra sao cho đúng. Và cuốn sách này giúp cho người phụ nữ biết các kiểm soát đồng tiền, giúp cho hiểu thêm cách vận dụng đồng tiền cho đúng cách và sử dụng cho cuộc sống của bản thân người phụ nữ một các đúng đắn. Một kinh doanh đầu tư sinh lợi được xây dựng một cách hữu ích mà đôi khi người phụ nữ đã vô tình bỏ qua đã được hướng dẫn cũng như chia sẽ kinh nghiệm trong sách.
Bìa sách cũng như nội dung sách trình bày đẹp.
4
358906
2016-06-28 15:07:08
--------------------------
456163
10984
Người ta thường nói phụ nữ dù mạnh mẽ đến đâu cũng cần một bờ vai để dựa vào. Có lẽ điều này sẽ là mối bận tâm, lo lắng của những con người yêu tự do, ghét phiền não và theo chủ nghĩa độc thân nhỉ? Dù như thế nào thì việc độc lập về tài chính là yêu cầu thiết thực, thiết yếu nhất đối với nữ giới hiện đại. Quyển sách này sẽ là cuốn cẩm nang cho những người nữ đang muốn tự đứng vững trên đôi chân của mình. Một cuốn sách hữu ích về quản lý tài chính. Tại sao không thử đọc và chiêm nghiệm chứ?
4
855770
2016-06-23 10:47:57
--------------------------
437805
10984
Cuốn sách "Người phụ nữ giàu - kiểm soát đồng tiền - kiểm soát cuộc đời" đưa ra những lý do giải thích vì sao người phụ nữ cần sự độc lập về tài chính; và để làm được điều đó họ cần phải làm những gì, có thái độ ra sao, cần đầu tư vào những gì? Cuốn sách đưa ra những gợi ý về các danh mục đầu tư mà phụ nữ có thể lựa chọn để đạt đến đích là sự tự chủ về mặt tài chính - không phụ thuộc vào gia đình, chính phủ hay cộng đồng xã hội.
Điểm trừ: cách kể chuyện của Kim còn hơi lan man, dài dòng; và cuốn sách còn có vài lỗi về biên tập
3
301718
2016-05-28 21:20:21
--------------------------
428592
10984
Đã từng tìm hiểu tương đối về những cuốn sách về thể loại này nên khi thấy cuốn sách về đề tài tài chính này được viêt bởi một người phụ nữ đã hoàn toàn thu hút minh. Cả cuốn sách hoàn toàn không mang úa nhiều áp lực cho người đọc mà dưới dạng kể chuyện, chia sẻ kinh nghiệm khiến người đọc cảm giác gần gũi hơn và dễ hiểu... đương nhiên nếu nói tính ứng dụng thì đó vấn có một khoảng cách nhất định như văn hóa, môi trường tài chính hay chính những suy nghĩ của từng người. Cuốn sách cho bạn thêm kinh nghiệm đó chính là những gì cần nên có. 
4
1196547
2016-05-11 16:04:45
--------------------------
426323
10984
Có lẽ hiện nay những quyển sách dạy làm giàu không phải hiếm. Tuy nhiên quyển sách này lại nằm trong những số ít quyển sách dạy làm giàu cho nữ. Điều này làm mình khá ấn tượng. Quyển sách lấy những câu chuyện thực của tác giả đến cho chúng ta những kinh nghiệm làm giàu. Nó còn cho ta thấy cách nhìn mạnh mẽ, cá tính của người phụ nữ hiện đại muốn độc lập về tài chính. Tuy nhiên với mình thì phần đầu quyển sách hơi lan man và dài dòng. Mình đã đọc những quyển sách Dạy con làm giàu. Mình thích phong cách của người chồng hơn. Nhưng nhìn chung quyển sách này khá bổ ích, phù hợp với những người phụ nữ hiện đại ngày nay, tự tin, độc lập. Vì với cách nhìn của phụ nữ về làm giàu dễ cho nữ giới đồng cảm, gần gũi và khá thú vị
4
528600
2016-05-06 17:46:55
--------------------------
421822
10984
Từ lần đầu thấy tiêu đề quyển sách đã ấn tuợng, bởi trước giờ toàn sách về cách làm giàu, tư duy triệu phú...tuy không phải là người đam mê việc đọc sách nhưng rất hài lòng về chất lượng sách, bìa trang trí, cách dẫn chuyện của tác giả, bao sách tiki rất đẹp, quyển sách đẹp hơn nhiều và có thể lưu trữ lâu theo thời gian, sạch, đẹp hài lòng Bên ngoài và tạm ổn về nội dung, thích những quyển sách do tiki gợi ý mua. Hy vọng sẽ tìm dc nhiu sách hay hơn nữa cho phụ nữ, cám ơn tiki.
3
218242
2016-04-25 22:17:35
--------------------------
305407
10984
Mình mua quyển sách này cũng khá lâu r. Sách viết về mẫu người phụ nữ hiện đại. Như mình ao ước. Tự chủ. K bị kiểm soát. Sách thiên hướng về việc phụ nữ nên học cách đầu tư, kinh doanh. Để có thể có đc cs sau này k bị phụ thuộc. Sách đem lại niềm động lực cho mình. Tuy đôi chỗ còn đá sang tác phẩm của chồng bà ấy tựa Dạy Con Làm Giàu. Chắc là lí lẽ và suy nghĩ của 2 người giống nhau nên sợ lặp lại đây mà :3 nói chung là được. cũng đáng để học tập
5
540543
2015-09-16 22:46:33
--------------------------
292659
10984
hàu hết các cuốn sách viêt về tài chính mặc dù không nói ra nhưng phần lớn đều dành cho nam giới. Với góc nhìn hiện đại, phụ nữ vừa đẹp vừa giỏi kiếm tiền (và nhiều tiêu chí khác nữa), cuốn sách này phù hợp với bạn trẻ, đặc biệt là nữ, giúp cho các bạn hình dung ra được kiểm soát tiền trong cuộc sống của mình. Mình là một cô gái có mong muốn làm giàu rất cao nên khi bắt gặp cuốn sách này mình đã rất hứng thú và mua ngay không cần suy nghĩ. cuốn sách hướng dẫn và vẽ ra khá rõ nét các cách đầu tư mà một người phụ nữ có thể sp dụng. mạch kể chuyện rất lôi cuốn, thu hút người nghe đến với câu chuyện của cô, khắc họa rõ hơn về Kiểm soát đồng tiền. mình rất thíc cuốn sách này.
4
427788
2015-09-07 15:18:53
--------------------------
290984
10984
“Người phụ nữ giàu” – Kim Kiyosaki là một quyển sách hay dành riêng cho phụ nữ, đặc biệt là các bạn nữ đang quyết tâm thể hiện mình, sống bản lĩnh, quản lý đồng tiền, chu toàn công việc. Mở đầu sách, tác giả giới thiệu về cuộc đời mình, chuyện về những người bạn, hoàn cảnh của họ giúp thức tỉnh nhận thức chúng ta hiểu được giá trị cuộc sống. Những rắc rối và những quyết định của tác giả giúp ta có tầm nhìn về đồng tiền, không phụ thuộc vào đồng tiền. Quyển sách là các nhìn mới hơn về phụ nữ thời hiện đại, xinh đẹp và giỏi giang. Sách bổ ích.
Xin cảm ơn.

4
554150
2015-09-05 22:34:09
--------------------------
283606
10984
Với góc nhìn hiện đại, phụ nữ vừa đẹp vừa giỏi kiếm tiền (và nhiều tiêu chí khác nữa), cuốn sách này phù hợp với bạn trẻ, đặc biệt là nữ, giúp cho các bạn hình dung ra được kiểm soát tiền trong cuộc sống của mình. 
Mặc dù đoạn đầu có vẻ đậm chất cá tính của cô Kim, (nếu k ưa giọng văn là muốn gấp sách rồi) nhưng về sau mạch kể chuyện rất lôi cuốn, thu hút người nghe đến với câu chuyện của cô, khắc họa rõ hơn về Kiểm soát đồng tiền.
Có thể tính ứng dụng chưa cao đối với VN, nhưng đó là bài học tốt cho phái nữ muốn hoàn thiện về tài chính.!
5
617117
2015-08-30 08:45:03
--------------------------
267201
10984
Đây là một quyển sách hay. Giúp chị em phụ nữ có cách nhìn mới về việc đầu tư, mà sâu xa hơn đó chính là sự độc lập về tài chính. Chúng ta - những người phụ nữ phải làm chủ đồng tiền, đừng để đồng tiền biến mình thành nô lệ. Ắt hẳn mọi người sẽ có thêm niềm tin, thêm sức mạnh vào cuộc đời này sau khi đọc "Người Phụ Nữ Giàu". 
5
736337
2015-08-15 12:14:46
--------------------------
264357
10984
Do tôi đã đọc gần hết bộ Cha giàu, cha nghèo của tác giả Robert Kiyosaki nên khi lựa chọn mua cuốn sách này, tôi đặt hy vọng cao hơn rằng nó sẽ cô đọng hơn những nội dung dành cho phụ nữ làm giàu. Tuy nhiên, sau khi đọc xong tôi cảm thấy hơi hụt hẫng vì theo tôi cuốn sách chỉ là bản tổng hợp đơn giản các nội dung của bộ sách Cha Giàu Cha nghèo. Vậy nên, nếu bạn nào chưa đọc bộ sách trên thì tôi khuyên mới nên đọc cuốn sách này để hiểu rõ hơn về tư duy tài chính và cách làm giàu của người giàu. Ngược lại, bạn nên tập trung vào bộ sách trên thì tôi thấy sẽ có giá trị hơn.
3
78338
2015-08-13 09:44:46
--------------------------
241852
10984
Một cuốn sách hiếm hoi dạy làm giàu viết riêng cho phụ nữ. Đọc cuốn sách này, chắc chắn bạn sẽ nhìn thấy hình ảnh của mình hoặc vài nét trong đó giống mình thông qua các cô bạn gái của Kim Kiyosaki.
Cuốn sách giúp truyền động lực cho phụ nữ hướng đến hình mẫu phụ nữ tự tin, năng động, độc lập, tự chủ về tài chính.
Tuy vậy, những hướng dẫn trong sách vẫn chưa thật cụ thể, có những hướng dẫn có thể áp dụng ở Việt Nam và  có những hướng dẫn chưa phù hợp.
Một tài liệu tham khảo tốt của Kim Kiyosaki - người vợ, người bạn đồng hành của tác giả bộ sách nổi tiếng Dạy con làm giàu.
4
292190
2015-07-25 21:34:44
--------------------------
236210
10984
Đọc cuốn sách này, tôi như thấy hình ảnh của mình trong những người bạn của Kim. Tôi đã từng có lúc đặt ra những câu hỏi tương tự và cũng có những lo ngại mặc cảm tương tự của người phụ nữ khi nghĩ đến ước mơ tự do tài chính. Nội dung cuốn sách viết rất đơn giản và dễ hiểu, không quá phức tạp như bộ Dạy con làm giàu và phù hợp cho cả đối tượng có trình độ thấp.Tuy nhiên nội dung chỉ dừng lại ở việc thay đổi tư duy nhận thức là chính, chưa hướng dẫn cụ thể kế hoạch tài chính như thế nào. Tôi đang mong chờ các cuốn sách tiếp theo chia sẻ kinh nghiệm nhiều hơn trong con đường đạt đến sự tự do tài chính của tác giả.
4
32452
2015-07-21 16:49:06
--------------------------
229805
10984
Mình rất thích quyển này. Ít có quyển sách nào viết về tài chính mà dành riêng cho phái nữ, tác giả lại là một người thực sự giỏi. Mỗi chương trong quyển sách đều là những chia sẻ thật mà tác giả đúc kết lại. Mình thấy giống như đang đọc câu chuyện kể hơn là đọc sách về tài chính. Không khô khan mà rất thu hút. Cuốn sách cung cấp những kiến thức rất hay, dễ hiểu, cơ bản về tài chính mà mỗi người nên biết. Đây là cuốn sách mình thấy rất rất hay. Nó không chỉ phù hợp với phụ nữ mà mọi người đều nên đọc nó.
5
68491
2015-07-17 08:24:30
--------------------------
223160
10984
Người phụ nữ giàu là một quyển sách tuyệt vời dành cho phụ nữ.Kim Kiyosaki, vợ của tác giả bộ sách Dạy con làm giàu, chia sẻ với phụ nữ về đồng tiền, về đầu tư. Bà muốn giúp phụ nữ thay đổi tư duy về đồng tiền, kiểm soát đồng tiền tốt hơn để từ đó được độc lập về tài chính - không phụ thuộc vào đàn ông, gia đình, hay công ty, hay chính phủ, và quan trọng nhất là giúp phụ nữ quản lý cuộc đời mình tốt hơn.Thật không tiếc khi bỏ tiền mua quyển sách này.
5
474206
2015-07-06 18:04:41
--------------------------
220904
10984
Đây là quyển sách tuyệt vời để xây dựng hình ảnh người phụ nữ Độc lập, mạnh mẽ, tự do của Kim Kyzosaki. Vợ chồng Robert và Kim Kyzosaki là triệu phú về Bất động sản và họ đã tự do tài chính. Đây là quyển sách tuyệt vời dành cho các bạn nữ nhưng có ước mơ lớn, có hoài bão, có khao khát làm chủ và mong muốn cuộc sống tự do. Từ khi đọc quyển sách này tôi nhận thức được việc đầu tư đúng cách cho bản thân, tư duy, suy nghĩ của mình và hiểu được cuộc sống như thế nào là cuộc sống tuyệt vời. Đặc biệt tôi định hướng được mẫu người mà tôi muốn trở thành trong tương lai. Hãy sở hữu cho mình một quyển để thay đổi hình ảnh, tương lai của mình trong thời gian sắp tới
5
173621
2015-07-03 09:18:19
--------------------------
131759
10984
Đây là một quyển sách thiết thực và ý nghĩa mà mình nghĩ không chỉ phụ nữ mới cần phải đọc. Những kinh nghiệm quản lý chi tiêu của tác giả dưới ngòi bút của một triệu phú được đúc kết và giải thích rất rõ ràng, cặn kẽ, đem lại cho người đọc những hữu ích không nhỏ. Từ cách tiết kiệm vốn trong kinh doanh, cách đầu tư, chi - thu... đều được tác giả hết lòng ghi chú cẩn thận.

Mình thấy quyển sách dù chưa thể nào sánh được với Think And Grow Rich hay Người Giàu nhất thành Babylon nhưng những điều thực dụng trong sách này mình nghĩ cũng chưa có nhiều người biết và nhận ra. Nếu hiểu được một nửa những gì trong sách đề cập và áp dụng chúng vào cuộc sống thì cho dù không kinh doanh nhưng cuộc sống của người sau khi áp dụng nó cũng tươi đẹp hơn về mặt tài chính rất nhiều.
5
418158
2014-10-27 12:45:13
--------------------------
65154
10984
dường như mảng sách "làm giàu" gần như chỉ viết dành cho những quý ông, vì thế, khi tìm được quyển sách này tôi thực sự rất vui.

hầu hết tiền bạc trong gia đình Việt Nam đều do một người phụ nữ  quản lý, thế nhưng sách viết về mảng này lại quá ít. Các phương pháp trong sách không chỉ dành riêng cho phụ nữ, mà là cho những người có mong muốn làm chủ tiền bạc, làm chủ cuộc đời mình. Bằng chính kinh nghiệm xương máu mà tác giả đã từng trải qua, các phương thức trở nên thực tế hơn bao giờ hết.
4
40504
2013-03-23 22:32:10
--------------------------
46337
10984
Người Phụ Nữ Giàu - Kiểm Soát Đồng Tiền Quản Lý Cuộc Đời" là một cuốn sách tôi chờ đợi lâu nay. Tôi thực sự muốn học hỏi về lĩnh vực đầu tư, muốn làm giàu bằng đôi tay, trí óc, muốn làm chủ bản thân lẫn cuộc đời của chính mình. Người phụ nữ giàu là quyển sách viết về phụ nữ và đầu tư. Nó nói về phẩm chất, nói về lòng tự trọng của những người phụ nữ làm chủ cuộc đời mình. Đó là trải nghiệm của một người đã từng thất bại đến mức khánh kiệt, không còn một xu trong túi, không có nhà cửa và chỉ có đôi bàn tay trắng để gầy dựng sự nghiệp, để độc lập về tài chính và trở thành một phụ nữ giàu. sau khi đọc quyển này tôi ngộ ra rất nhiều điều về đau tư kinh doanh và làm giàu, quyeetr sách này với tôi giống như một người thầy dạy kinh dọah vậy!!!!!!!!
5
63207
2012-11-14 20:54:00
--------------------------
1724
10984
Bộ sách "Dạy con làm giàu" đã quá nổi tiếng, nhưng được viết qua một  người đàn ông là Kyosaki nên tôi thấy trong đó chủ yếu cũng nhành cho nam giới,  vẫn chưa có 1 cuốn nào giành riêng cho chị em phụ nữ. chị em thật sự rất thiệt thòi.
Tuy tôi là nam giới nhưng "Người Phụ Nữ Giàu - Kiểm Soát Đồng Tiền Quản Lý Cuộc Đời" là một cuốn sách tôi chờ đợi lâu nay.
Thanks Những tác giả của Bộ sách và Nxb trẻ.
5
0
2011-02-11 13:23:56
--------------------------
