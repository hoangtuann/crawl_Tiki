426624
7384
Sách là một câu chuyện tuyệt vời về một chú chó thông minh, nhân hậu. Đây là một món quà hay không những cho trẻ thơ mà còn cho cả người lớn. Mở trang sách ra thấy hình ảnh chú chó dễ thương nhí nhảnh, yêu thương người chủ của mình. Mặc dù mình không phải là một người yêu thương chó đặc biệt nhưng thật sự mình rất cả cảm tình với cuốn sách này. 

Một cuốn sách mang tính nhân văn cao cả, dạy chúng ta về lòng trung thành, dũng cảm và hi sinh cho nhau. Thật đáng yêu!
5
349914
2016-05-07 11:30:30
--------------------------
331457
7384
Câu chuyện về chú chó trison vừa nhẹ nhàng lại vừa cảm động.Xã hội loài người được miêu tả chân thực,hài hước qua góc nhìn của một chú chó.Hơn hết câu chuyện còn nói về sự khó khăn của người khiếm thị trong cuộc sống hằng ngày,vai trò của chó dẫn đường như đôi mắt sáng-người bạn tri  kỷ.Câu chuyện như một áng văn nhẹ nhàng khơi gợi lên trong ta những suy nghĩ chân thực về cuộc sống,sự tử tế ,lòng trắc ẩn để ta cảm thấy mình thật may mắn hơn biết bao con người kém may mắn khác...thức tỉnh những trái tim đang sống để thêm yêu cuộc đời,đối xử tốt với những người xung quanh và cả những chú chó- người bạn bốn chân thân thiết của chúng ta
4
857005
2015-11-04 15:13:29
--------------------------
240959
7384
Sau khi đọc Chú chó dẫn đường, đại gia và nhân quả, mình đã khâm phục tác giả Mikhail Samarsky này rồi. Và được trang tiki giới thiệu đề xuất đến truyện này của cùng tác giả, mình đã nhanh chóng đặt mua ngay. Cuộc sống nhân sinh lại được tiếp tục nhìn và cảm nhận qua trái tim của Trison, một chú chó dẫn đường thông minh, dễ thương, ngoan ngoãn mà còn  tốt bụng và rất mực trung thành. Cuộc hành trình cam go đầy thử thách để tìm đường về nhà với cậu chủ của Trison cũng đã dẫn dắt người xem bước qua nhiều miền sáng tối của cuộc sống, đã gợi ra nên những cảm xúc suy tư sâu sắc, sự cảm thông với những người “tối mắt nhưng sáng lòng”, và cuối cùng tác phẩm đã xây dựng thành công hình ảnh đẹp về tình bạn chân thành giữa người và chó.
5
521993
2015-07-25 02:07:25
--------------------------
191478
7384
Đây là câu chuyện thật ý nghĩa và sâu sắc, một bài học bổ ích vô cùng. Tác giả Mikhail Samarsky đã vô cùng tài ba khi đưa bài học đạo lí làm người vào truyện. Văn phong hóm hỉnh, hài hước theo lời tự sự của chú chó dẫn đường Trison nhưng lại có cách nhìn nhận và trái tim cao cả không khác gì loại người. Tác giả đã cho người đọc hiểu hơn về những nỗi đau, mặc cảm của người khiếm thị, cho họ nhiều sự cảm thông, tình cảm mà họ xứng đáng nhận được nhưng vẫn không mang lại cảm giác nặng nề cho cuốn sách.
5
580713
2015-05-01 20:58:35
--------------------------
145455
7384
Thú thật, mình biết đến nhà văn Mikhail Samarsky không phải do báo chí mà là khi mình tình cờ mua được quyển sách Cầu vồng trong đêm - Cho những trái tim đang sống này hồi còn học Tiểu học =w= . Cho đến tận bây giờ thì có lẽ đây là quyển sách mình ưng í nhất, Trison - một chú chó với trái tim và cách nhìn của một con người, đã cho chúng ta thấy được phần nào những mặt tốt và xấu của cuộc sống. Cuộc hành trình gian lao vất vả để tìm đường về nhà với cậu chủ của Trison cũng đã gợi ra nhiều điều mà chúng ta cần phải suy ngẫm, thật sự, Trison không chỉ là một chú chó thông minh, ngoan ngoãn dễ thương, mà còn là một "con người" tốt bụng, nhân hậu, trung thành, có cách nhìn và trái tim hoàn toàn khác biệt. Ánh sáng thật sự của quyển sách đã gợi mở cho những tâm hồn còn đang chìm khuất trong bóng tối. Truyện đồng thời cũng đề cao tình bạn đẹp và sự trung thành của loài chó đối với con người (có ai thấy thế không ạ ƠwƠ)
5
433269
2014-12-30 16:13:53
--------------------------
122838
7384
Bạn sẽ được trải nghiệm rất nhiều trạng thái cảm xúc khi đọc quyển sách này .Tác giả khéo léo đan cài vào câu chuyện những bài học nhân sinh cáo đẹp thông qua lời kể chuyện của một con chó -Trison .Một con chó lại là người dạy ta những đạo lí làm người khi mà con người đang tự đánh mất nó .Hơn cả ánh sáng của cầu vòng đó là ánh sáng của cái tình ấm áp .Mỗi lần đọc lại đem đến cho tôi một cảm xúc khác nhau .Đây thực sự là một cuốn sách ta nên có.
5
165044
2014-08-28 13:54:12
--------------------------
114213
7384
Một cốt truyện giản đơn mà lại sâu sắc qua những triết lý của cậu bé  14 tuổi . Đôi khi ngẫm lại , tôi thấy mình thật vô cảm , chưa bao giờ tôi nhận ra mình là người như thế ,  sống ích kỷ , hẹp hòi đến đáng ghét ! Tôi luôn mặc kệ mọi người xung quanh , dù họ có xảy ra chuyện gì đi chăng nữa nhưng khi bản thân tôi khi có một vết đau , vết ngã thì lại yếu hèn ,mất niềm tin ! Những con người ấy -qua lời kể Trison sao mà dũng cảm đối diện với sự thật đến vậy ? Không thấy mảng sáng cuộc đời , họ sống trong bóng tối thế mà vẫn lạc quan , yêu đời ..Càng khâm phục ý chí họ bao nhiêu tôi lại càng  trách bản thân mình bấy nhiêu ! Trách vì sự vô cảm, trách vì sự yếu hèn dù sinh ra với hình hài đầy đủ ! Cảm ơn nhé ! Cảm ơn Trison ! Cảm ơn Mikhail Samarsky ! Cậu đã tiếp thêm sức mạnh cho tôi lúc này và ngay bây giờ tôi hiểu mình nên làm gì !
5
312101
2014-06-10 12:37:45
--------------------------
104502
7384
Mikhail Samarsky viết về một chú chó Trison thật thú vị. Trison để lại ấn tượng sâu sắc trong tôi. Chú dũng cảm, thông minh vượt qua mọi thử thách. Những mẩu đối thoại giữa Trison và người bạn mù của mình rất tự nhiên, ăn ý với nhau, giữa họ là tình anh em, tình bạn bè chứ không phải chủ tớ. Đôi khi, tôi bật cười trước những suy nghĩ của Trison, Trison rất hay giận ai đó và rồi lại dùng từ số 10 trong những từ cậu ấy muốn nói đó là "xin lỗi". Trison như một đứa trẻ vậy. Nhưng lúc làm nhiệm vụ, cậu là một người dẫn đường thực thụ. Tôi cũng rất yêu quí hai người bạn của cậu ấy. Đọc câu chuyện này xong, tôi cảm thấy thương cảm cho những người tàn tật và thêm yêu quí chú chó của mình
5
116203
2014-01-14 07:49:18
--------------------------
104259
7384
Tôi biết đến Mikhail Samarsky cùng quyển Cầu vồng trong đêm này từ năm 2009, khi ấy quyển Cầu vồng trong đêm mới vừa được xuất bản, nhưng chưa được giới thiệu ở Việt Nam. Và hôm nay, khi cầm trên tay quyển sách này, tôi mới thấy được hết những gì độc đáo và ý nghĩa của nó. Qua lời kể của Trison, cuộc sống hiện ra trong tâm trưởng tôi, tuy là cuộc sống của những con người đã mất đi ánh sáng, nhưng thật lạ kì, lại sáng bừng hơn cả thứ ánh sáng thông thường mà tôi tiếp nhận hằng ngày từ thị giác. Đó là một bức tranh sinh động lạ kì, đầy đủ các mảng sáng, mảng tối... Quyển sách dạy tôi nhiều điều, đến nỗi tôi có suy nghĩ chủ nhân viết nó là một người thầy, tuy cậu ấy chỉ hơn tôi một tuổi. Đây là một quyển sách đáng có. Tôi nghĩ nó bổ ích không chỉ đối với tôi, mà với tất cả các bạn trẻ đang đi tìm lí tưởng sống.
5
129990
2014-01-10 15:28:54
--------------------------
