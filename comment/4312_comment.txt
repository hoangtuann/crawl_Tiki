454523
4312
Con Tằm là một quyển sách trinh thám tuyệt vời mà mình may mắn được đọc. Tuy việc giết người theo tình huống trong tiểu thuyết không phải là một đề tài mới lạ nhưng cách nhà văn diễn tả phân đoạn thám tử Strike phát hiện ra xác của nạn nhân thật quá sức tưởng tượng của mình, mình yêu thích đến từng chữ, từng chi tiết miêu tả tỉ mỉ cách dàn dựng hiện trường theo ý đồ của hung thủ, có thể thấy rõ sự tàn nhẫn đến độ biến thái không tưởng. Và điều đó lại làm cho mình hứng thú và hồi hộp gấp trăm lần để dự đoán tiếp các tình tiết có thể xảy ra tiếp theo trong câu truyện. Bạn phải trầm trồ gớm ghiếc tột bật về độ man rợ, cái cách mà kẻ thủ ác xử lý phần còn lại của thi thể nạn nhân.  Song song đó tác giả cũng đã lồng ghép một số người và sự việc liên quan đến đời tư của hai nhân vật chính trong truyện một cách vừa phải nhưng vẫn không làm xao nhãng đi nhịp điều tra. Và các Potterheads tất nhiên sẽ mỉm cười thích thú khi cô Rowling đã hữu ý theo kiểu vô tình nhắc qua bóng dáng của nàng Emma Watson ở trang nào đó của tác phẩm.  Tuy nhiên cũng có vài điều mình không hài lòng về nhà xuất bản Trẻ: trong cả hai cuốn Con Chim Khát Tổ và Con Tằm đều in sai khá nhiều lỗi chính tả, bìa sách thì dễ bị bong tróc phần keo so với những cuốn sách cũ mà mình đã mua trước đó. Hy vọng nhà xuất bản có thể khắc phục để các tác phẩm văn học thêm hoàn hảo.
4
88741
2016-06-21 23:31:53
--------------------------
452268
4312
Theo mình thì cuốn này tình tiết lẫn vụ án có hay hơn con chim khát tổ,mặc dù tác giả vẫn viết theo cách cũ giới thiệu vụ án,thẩm phấn nghi phạm xen lẫn vào các chi tiết về đời sống của ông thám tử nhưng độ hấp dẫn thì hơn hẳn cuốn trước.Mỗi một nghi phạm đều có khả năng là hung thủ qua các lời khai càng đọc càng rối vì người này khai kiểu này người kia nói kiểu khác không biết đâu là sự thật,cái kết theo mình là có gây ra sự bất ngờ và kịch tính không ngờ hung thủ có thể tỉnh bơ làm những việc ghê gớm đến vậy,bìa truyện rất đẹp,giấy sờ cũng rất đã,mình rất hài lòng khi mua quyển này.
4
526322
2016-06-20 15:39:58
--------------------------
443107
4312
Tác phẩm có quá nhiều chi tiết thừa, những nhân vật có cũng được chẳng có cũng không sao, chỉ có tác dụng tung hỏa mù làm cho người đọc bối rối, hoang mang. Tuy nhiên cách dẫn chuyện của J.K.Rowling vẫn có nét hấp dẫn riêng, khiến cho người đọc cứ phải lật hết trang này đến trang khác để xem tác giả đưa mình đến đâu. Điểm trừ của tác phẩm là không có cao trào, cách tình tiết từ đầu đến cuối cứ đều đều, không điểm nhấn và thủ phạm cũng không phải là quá bí ẩn, khó đoán.
3
464110
2016-06-06 11:11:04
--------------------------
434593
4312
Tuy là một cuốn sách dày nhưng không hề nhàm chán. Nội dung truyện cứ cuốn hút tôi muốn đọc thêm nữa, muốn biết thêm về hung thủ. Những nghi phạm không hề quá rõ ràng, chỉ chờ thám tử tìm bằng chứng. Trong quá trình tìm kiếm tội phạm, tác giả cũng đã cho vào một số tình tiết về đời sống, không quá nhiều, chỉ đủ để làm cho câu chuyện thêm thực tế.
Điều đặc biệt ấn tượng tôi đó là motif giết người, ghê sợ mà rất độc đáo. Bởi câu chuyện xoay quanh những nhà văn, những người nổi tiếng, nên cách giết người cũng văn chương lắm :)))) Và ở mỗi chương truyện, thay vì đặt tên cho nó, thì tác giả đã trích những câu văn trong các tác phẩm khác, để ta phải ngẫm nghĩ mới hiểu được nội dung, ẩn ý của chương truyện.
4
263175
2016-05-23 14:42:45
--------------------------
350357
4312
Tôi đã không ngần ngại đặt mua ngay quyển sách này bởi vì quá mê tác giả. Và đúng như mong đợi, quyển sách đã làm tôi thoả mãn từ đầu đến cuối. Cốt truyện mạch lạc, dẫn dắt tôi đi từ bất ngờ này đến bất ngờ khác, nhẹ nhàng nhưng vẫn lôi cuốn khiến tôi không thể dứt khỏi quyển sách. Phần bìa sách thiết kế nhưng quyển sách gốc của nhà xuất bản bên Anh làm tôi khá thích thú và chất lượng giấy in của nhà xuất bản Trẻ thì quá tuyệt. Hy vọng sẽ sớm được cầm trên tay quyển kế tiếp trong series trinh thám này.
5
104927
2015-12-10 13:34:32
--------------------------
339559
4312
Nếu là fan trung thành của thể loại truyện trinh thám, bạn sẽ không quá bất ngờ với nội dung quyển sách này. Án mạng được dựng nên theo tình tiết của một tác phẩm và nạn nhân bị giết một cách kinh tởm kia lại chính là tác giả của quyển sách đó. Thế nhưng điểm cuốn hút của quyển sách này nằm ở cách xây dựng hệ thống các nhân vật, cách buộc chặt và tháo gỡ tình tiết vào những thời điểm phù hợp làm ta ngỡ mình đã thu thập đủ bằng chứng nhưng vẫn phải chờ kết luận cuối cùng vào chương sau cuối. Kết hợp một vài yếu tố tình cảm và hành động rất mượt, nhẹ và sâu đã làm tăng hiệu ứng tích cực cho tác phẩm.
4
171151
2015-11-18 11:21:32
--------------------------
317621
4312
khi bắt đầu đọc truyện này mình hơi mất kiên nhẫn một chút vì cách mào đầu bình bình không mấy ấn tượng. Nhưng sau đó lại dần dị bị cuốn vào lúc nào không hay. Từ cốt truyện cho đến các tình tiết thắt mở đều được tác giả xử lý tốt từ đâuf đến cuối. Mình thích văn phong mạch lạc và cách mô tả tỉ mỉ, mang màu sắc gothic của trong tác phẩm. 
tuy nhiên mình có cảm giác là dịch giả cố gang dịch sao cho ra giọng văn giống y hệt trong Harry Potter,mà không phải giọng dịch that của dịch giả. cho nên có nhiều chỗ thấy từ dùng bị quá lố, nó cứ khiêng cưỡng mất tự nhiên sao đó. một điểm nữa là câu chuyện thì dài nhưng lại không có những bước tiến kích thích người đọc mà chỉ là chuổi sự kiện nối tiếp cho đến khi có được kết quả điều tra, tác giả cũng không đưa ra những " mồi nhử" để tạo hung khởi cho người đọc mà cố giữ đến cuối cùng mới thông báo kết quả luôn. và kết quả thì cũng không quá bất ngờ. Nếu bạn để ý kĩ các chi tiết nhất là chi tiết Owen thích bị trói thì sẽ sớm nhận ra ai là hung thủ. Kế đến là cách giải quyết thứ mà hung thủ mang khỏi hiện trường , cũng có thể đoán ra được luôn.
nói chung truyện hay, ai có kiên nhẫn, có thời gian và thích thể loại trinh thám không quá đánh đố thì nên đọc :)
4
9229
2015-10-03 21:18:58
--------------------------
285725
4312
Với một cuốn sách đóng mác J.K Rowling đã là quá đủ để đảm bảo cho sự thành công của cuốn sách. Không phải do tác giả đã quá thành công với series "Harry Potter" hay "Khoảng trống", mà câu chuyện về chàng thám tử Cormoran Strike và cô trợ lý trẻ trung Robin Ellacott thực sự hấp dẫn và lôi cuốn người đọc, ngay từ tập đầu tiên "Con chim khát tổ". Ở phần tiếp theo này, không đơn thuần chỉ là câu chuyện về một vụ án, mà còn là câu chuyện về tâm lý với một cách dẫn dắt kịch tính, lôi cuốn và có những nút thắt nghẹt thở. Đồng thời câu chuyện về chàng thám tử thất tình cùng cô trợ lý vừa mới đính hôn cũng xảy ra những tình huống thật hài hước với những cách nhìn thật độc đáo và lôi cuốn.
5
29716
2015-09-01 08:48:00
--------------------------
258900
4312
Ở tập 2 này , vụ điều tra trinh thám ko còn nhẹ nhàng như tập 1 nữa mà thay vào đó là những vụ điều tra có phần kịch tính hơn . Và vụ án ở tập 2 có phần kinh dị hơn về cuốn sách cuối cùng kì bí , dâm dục , kinh dị của tác giả mất tích Quine và về vụ án "giết người moi ruột" một cách tàn bạo và ghê tởm của tên sát nhân . Bà vợ , biên tập , người ở nhà xuất bản hay bà đại diện hay cô người yêu là hung thủ đây ?! Càng về sau càng kịch tính và nhiều tình tiết bất ngờ thú vị nữa . Đây là quyển sách nên tìm đọc sau Harry Potter nếu đã là fan của J.K.Rowling hay còn được biết tới với bút danh Robert Galbraith !!!
5
589566
2015-08-08 22:14:13
--------------------------
226024
4312
Mình cũng từng rất phân vân không biết có nên bỏ 200 nghìn ra mua cuốn sách không nhưng cuối cùng còn hơn cả kỳ vọng.

Robert Galbraith là bút danh khác của nữ nhà văn J.K.Rowling. Thực sự J.K.Rowling đã thoát khỏi cái bóng của Harry Potter để viết nên cuốn tiểu thuyết trinh thám tầm cỡ này. Có thể nói Con Tằm là một trong những cuốn trinh thám hay nhất mọi thời đại. 
Tác giả đưa người đọc qua những tưởng tượng, những phán đoán xem ai là kẻ đã giết nhà văn biến thái kia. Cái hay ở đây là câu chuyện không có đến một kẽ hở nào, tác giả rất thông minh trong việc xử lý và phán đoán tình huống. Khiến người đọc bị cuốn hút và mải mê vào từng chi tiết.

Kết thúc 700 trang truyện mình có thể nói đay là cuốn sách tên cả tuyệt vời
5
408981
2015-07-11 11:43:32
--------------------------
208042
4312
Lâu lắm rồi mới lại đọc cuốn sách trinh thám hấp dẫn như vậy, tuy là có nhiều tình tiết ghê rợn khi mô tả xác người thối rữa, bị nấu chín và bày ra ăn.... tuy là đoạn mô tả chưa thực sự lột tả sự ghê rợn thảm khốc lắm nhưng nội dung, tình tiết cùng mạch câu chuyện vẫn rất hấp dẫn và thu hút, lúc đầu mình đọc hơn của 200 trang, càng đọc càng hồi hộp hấp dẫn, ko biết kết cục ra sao, ai là người giết chết ông nhà văn biến thái trong số 7 người ăn thịt ông ta? ^^

Nếu so với con chim khát tổ, mình công nhận tác phẩm này dài hơn, cũng nói nhiều hơn 1 chút về cuộc sống xung quanh Strike and Robin nhưng mà cũng rất dễ thương mà. Tuy không phải là tuyệt vời như Nữ Hoàng Trinh thám nhưng cũng là một tác phẩm đáng đồng tiền bát gạo ^^
5
90063
2015-06-13 22:54:16
--------------------------
198079
4312
Đây là cuốn tiểu thuyết trinh thám, nhưng được viết theo cách riêng so với những cuốn trinh thám khác mình đã đọc. Truyện của Robert lấy điểm nhìn chỉ ở hai nhân vật duy nhất là Strike và Robin. Chính điểm nhìn hạn chế như vậy càng làm tác phẩm che giấu rất kỹ hung thủ. Cho nên muốn biết ai là kẻ thủ ác, độc giả buộc phải đọc đến cuối truyện.

Nội dung thì cũng chỉ là giết người vạch mặt hung thủ. Nhưng truyện có yếu tố hay ho ở việc xoáy vào đời tư của đôi nam nữ chính. Đi sâu vào tâm tư, nguyện vọng của họ. Cái hay là những tâm tư nguyện vọng này không dính đến yếu tố máu me. Mà là ở phía công việc, gia đình, người yêu. Nên người đọc sẽ có không gian để thoát ra nếu cảm thấy ngột ngạt với những trang truyện hình sự. 

Mình rất thích Robin. Cảm thấy nhân vật này làm truyện sống động thêm tại cổ đẹp. Để ý thấy 100% truyện trinh thám nam chính hành động thì thế nào cũng có một nữ đi theo song hành. Hi vọng Robert sẽ sớm có thêm vài cuốn nữa trong series này.
5
137500
2015-05-19 08:54:43
--------------------------
182819
4312
Bản thân mình không hẳn là một đứa đọc nhiều truyện trinh thám đến nỗi có tư cách viết cái tiêu đề như trên, nhưng quả thật Con Tằm là một trong những tác phẩm hiếm hoi có thể khiến tim mình đập mạnh mỗi khi lật sang trang mới, và mặc dù mình rất mong muốn giở ra ngay những trang cuối cùng để thỏa mãn trí tò mò bằng những đáp án cho những manh mối bí ẩn của câu chuyện, bản thân mình vẫn không muốn chuyến hành trình với thám tử Cormoran Strike kết thúc vì nó quá là tuyệt vời đi thôi.

Robert Galbraith, một bút danh khác của cô Rowling - tác giả nổi tiếng của bộ truyện Harry Potter, đã rất biết cách xây dựng một vụ án vừa đáng sợ vừa hóc búa và gay cấn đến phút cuối cùng, khiến cho người đọc tuy sợ nhưng  cũng phải mau chóng lia mắt đọc tới dòng tiếp theo vì họ hoàn toàn không muốn bỏ lỡ bất kì tình tiết nào trong chuyến hành trình của Cormoran Strike.

Mình đặc biệt thích trợ thủ Robin của Cormoran và mối quan hệ giữa hai người: rất bình đẳng và dễ chịu. Robin thật sự là hình mẫu nữ chính đáng mơ ước trong các tác phẩm văn học: cô thông minh và biết cách bày tỏ sự quan tâm đến người khác một cách hết sức tế nhị, tuy nhạy cảm nhưng đủ sáng suốt để tập trung vào chuyện lớn phía trước. Cormoran lúc đầu tuy không đánh giá cao Robin nhưng qua tập này rõ ràng vị thám tử đã trở nên tôn trọng  và tin tưởng cô hơn nhiều. 

Các manh mối của vụ án được rải đều khắp câu chuyện. Nếu mọi người chịu khó để ý kĩ, có khi sẽ giải ra được vấn đề ngay trước cả Cormoran Strike đấy thì sao? (Mình thì vốn không được thông minh đến vậy...)

Và lời cuối cùng: Gần 200 nghìn một quyển sách. Đáng không? Rất xứng đáng. 

À, cũng xin cảnh báo luôn rằng trong tác phẩm có đặc tả những chi tiết có liên quan đến xác người chết, máu me và nội tạng, những ai thật sự có vấn đề với những điều kể trên thì xin cân nhắc kĩ trước khi mua.
5
46351
2015-04-14 22:28:52
--------------------------
