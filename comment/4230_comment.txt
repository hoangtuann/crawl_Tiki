458600
4230
Tập 25 này là sự thông minh của Goku khi lừa Ginyu và lấy lại được thân thể khi bị Ginyu hoán đổi và tiêu diệt luôn được hắn, hay sự thông minh lanh lẹ của Krllin và Gohan khi lừa được Vegeta và gọi rồng thiêng giúp hồi sinh thượng đế và Piccolo và đưa Piccolo về hành tinh Namek quê hương của mình để cùng hợp lực tiêu diệt tên trùm vũ trụ Frieza. Một tập rất hay và lôi cuốn với sự thông minh lanh lẹ của Goku và Krillin.
Truyện mua ở Tiki mình rất yên tâm và hài lòng vì chất lượng rất tốt và sách luôn mới và đảm bảo.
5
766204
2016-06-25 12:11:57
--------------------------
299754
4230
Có một thời gian tụi con nít xóm tôi phát cuồng vì 7 viên ngọc rồng, sáng hôm nào ra truyện là hôm đó bỏ ăn sáng để dành tiền mua truyện tranh, không nhanh thì tới lượt mình lại chẳng còn cuốn nào, ấy vậy mà gần 20 năm, bộ truyện ấy lại tái xuất giang hồ, trong khoảng thời gian này truyện cũng được in 2 lần, nhưng bộ lần này mình đánh giá là hoàn hảo nhất. Truyện giữ lại nguyên dạng như bản gốc, từ bìa rời, bìa liền, trang truyện, cách đọc từ phải sang trái nữa, ngoài ra tên nhân vật, 1 số chiêu thức uýnh nhau được chỉnh lại cho đúng nên đối với những fan già như mình đọc cảm thấy không quen lắm! Vài tập đầu còn lỗi chính tả. Mong truyện sớm ra hết để mình sưu tầm cho đủ bộ.
5
68108
2015-09-13 15:01:25
--------------------------
258570
4230
Tập Dragon ball này kể về cuộc chiến với frieza và Ginzu của Songoku, thấy được sự thông minh của Songoku khi kịp ném con ếch khi Ginzu định hoán đổi với vegeta :3. Và khi krilin, gohan và Dende qua mặt vegeta và frieza để thực hiện 2 điều ước của rồng thần Namec làm piccolo sống lại. Đọc tập này thấy hồi hợp kinh khủng với những đoạn gay cấn- frieza xuất hiện sau khi rồng thần xuất hiện và tiếc nuối nhất là khi không ước được điều ước thứ 3. Cả nhóm điều đang gặp nguy hiểm và đành đợi sự xuất hiện của piccolo và songoku ở các tập sau thui :3 
5
204594
2015-08-08 17:07:54
--------------------------
218833
4230
Dragon Ball, cuốn truyện gắn liền với tuổi thơ của nhiều độc giả thế hệ 8x, 9x. Qua nhiều lần tái bản, cuốn truyện vẫn chiếm được lòng của nhiều độc giả nhí. Lần này, chuyện được xuất bản với chất lượng giấy tốt hơn, hình thức bìa đẹp, từng ô chuyện được in rõ nét cùng lời thoại trau chuốt hơn của các nhân vật. Trong tập 25 này tác giả Akira đã thực sự khiến chúng ta phải nể phục trước cốt truyện hay đến như vậy. Hành trình của chú khỉ con lần này sẽ thật sự cam go khi phải đối đầu với một kẻ thù mạnh như Frieza!
5
620158
2015-06-30 21:54:08
--------------------------
