472477
6390
Món lạ miền Nam của tác giả Vũ Bằng nằm trong bộ sách viết về văn hóa 3 miền của Việt Nam. Quyển này nói về miền Nam với các món ăn dân dã, ý tưởng của người làm ra những món ăn đó, và những đặc trưng không phải sách dạy nấu ăn. Tác giả người Bắc nhưng lại viết về miền Nam một cách yêu thương và mộc mạc, theo kiểu người miền Nam, làm cho những ai chưa yêu mến miền Nam, sẽ yêu thương hơn, và ai ở miền Nam sẽ hiểu thêm, yêu thêm xứ sở của mình !
5
273998
2016-07-09 17:10:47
--------------------------
375827
6390
Giống như món ngon miền bắc, Vũ Bằng viết về những món ăn của người dân miền Nam Bộ vừa ngon mà vừa giản dị, thấm đượm thêm những câu chuyện đi kèm với chúng, và cái tình cảm con người dành cho nhau qua những món ăn. rất bình dị, cây từ phòng khoáng, không cầu kì đúng như tính cách của người dân Nam Bộ. Đọc  cuốn sách này xong bên cạnh đất rừng phương Nam của Đoàn Giỏi, mới thấy được thiên nhiên Miền Nam trù phú đến dường nào. Thịt rùa, baba, lươn, các món vùng sông nước miêu tả vô cùng sống động, phục tài của Tác giả Vũ Bằng lắm.
4
568747
2016-01-29 12:00:52
--------------------------
354357
6390
Tôi đã đọc qua Miếng Ngon Hà Nội của Vũ Bằng, cảm thấy thích quyển sách này về hình thức cũng như nội dung. Sau đó tôi quyết định mua Món Lạ Miền Nam, để cả hai cùng sóng đôi trên kệ sách.
Nếu Miếng Ngon Hà Nội cho ta hiểu thêm về những thức quà dân dã nơi Hà thành, thì Món Lạ Miền Nam nói về những thứ lạ lẫm không phải ai cũng ăn được. Thật như vậy, tôi lớn lên ở miền Nam nhưng cũng chưa nếm hết 8 thứ lạ mà Vũ Bằng nêu ra. Nhất là về phần đuông dừa, nghĩ đến thôi cũng rợn da gà chứ đừng nói cho vào miệng. Nhưng qua sự miêu tả chân thực cùng cách hành văn tinh tế, sự ác cảm đối với món ăn này của tôi cũng dịu đi ít nhiều. Mặc dù vậy, tôi cũng không nghĩ mình sẽ ăn được nó, cũng như dơi huyết, bò kiến, canh rùa mà tác giả đã đề cập. Thậm chí trước khi đọc Món Lạ Miền Nam, tôi còn không nghĩ những thứ này có thể nấu thành món ăn khiến người ta vương vấn mãi.
Đọc quyển sách này, người miền Nam thấy yêu hơn nơi mình chôn nhau cắt rốn, còn người miền khác cũng cảm nhận được cái sự hồn nhiên chân chất, càng yêu thêm người miền Nam nước Việt.
Nói về chất lượng giấy cũng tốt, nhưng phần in thì có lỗi, có những chấm mực li ti cùng vài chữ bị lem. Ngoài ra thì tôi vẫn thấy sức tưởng tượng so sánh của Vũ Bằng ... quả là cao siêu, như đã nói ở phần nhận xét Miếng Ngon Hà Nội vậy.
3
967569
2015-12-17 19:34:30
--------------------------
300449
6390
Đọc quyển sách này khi đói bụng thật là quyến rũ. Tác giả miêu tả những món ngon, lạ ở miền Nam rất chi tiết, những cảm giác mà khi đọc, người đọc có thể tưởng tượng ra mình đang thưởng thức món ăn đó. Có một số món rất lạ, mình chưa từng nghe qua, mặc dù mình là người miền Nam. Vậy mới biết, tác giả đã có những sự trải nghiệm tuyệt vời như thế nào khi có thể thưởng thức hầu hết các món ngon Việt Nam. Mình chắc chắn sẽ mua thêm quyển Món Ngon Hà Nội để khám phá ẩm thực miền Bắc, ẩm thực Việt Nam thật đáng để khám phá. 
5
21422
2015-09-13 22:39:39
--------------------------
253316
6390
Sau "Món ngon Miền Bắc" giúp mình mở mang hiểu biết về các thức quà ngon Miền Bắc, Nhã Nam đã cho ra mắt cuốn "Món lạ Miền Nam", mình không ngần ngại rinh ngay về đọc. Vũ Bằng bằng giọng văn lai láng, bay bổng của mình đã viết nên 1 cuốn sách tuyệt hảo tổng hợp các món ăn Miền Nam: lạ miệng nhưng rất ngon. Sách được trình bày đẹp từ bìa cho đến nội dung, dung lượng từng bài viết vừa phải, nhẹ nhàng để con người ta từng chút từng chút khám phá những món ăn mà ông sẽ giới thiệu tiếp theo. Cứ vậy, đủng đỉnh đọc 1-2 chương. Gấp cuốn sách lại mà tự dưng thấy vị mằn mặn, cay cay đầu lưỡi.
5
112948
2015-08-04 12:50:24
--------------------------
252259
6390
Sau Thương nhớ mười hai và Món ngon miền Bắc, tôi háo hức đặt mua Món lạ miền Nam của Vũ Bằng. Tôi là dân Bắc chính cống trăm phần trăm, gần như chưa bao giờ thử món ăn miền Nam nên rất muốn biết các món miền Nam như thế nào. Tiki giao sách rất nhanh, vừa nhận được là tôi đọc ngay. Sách mỏng, giá cả phải chăng, bìa đơn giản nhưng đẹp là ấn tượng đầu tiên của tôi. Về nội dung, theo tôi thấy thì không bằng hai cuốn trước. Có lẽ vì tác giả là người Bắc nên không mặn mà với món Nam lắm chăng? Ngay cái tên cũng thấy rồi chỉ là "món lạ" chứ không ngon! Tuy nhiên ai muốn tìm hiểu về ẩm thực miền Nam thì nên mua cuốn này.
4
167283
2015-08-03 15:49:14
--------------------------
246606
6390
Được xem là anh em với “Miếng ngon Hà Nội”, “Món lạ Miền Nam” cũng là những câu chuyện về cuộc sống được nhà văn Vũ Bằng khéo léo lồng ghép vào từng món ăn bằng một cái nhìn chân thật, sống động và đầy cảm xúc. Tuy không sinh ra và lớn lên ở Miền Nam nhưng tác giả vẫn dành cho nơi đây một tình yêu thật mãnh liệt và sâu sắc, điều đó dễ dàng nhìn thấy qua cách ông viết về những trải nghiệm có buồn, có vui và sự thấu hiểu đến bất ngờ dành cho những món ăn đặc sản - những món mà ngay đến người Miền Nam chính gốc cũng chưa chắc đã nếm qua. Trong số đó, có những món khá gần gũi như khô tôm, khô cá, khô mực ,… gắn liền với truyền thuyết về nhân ngư; rồi món tóp mỡ ngào đường “ngon ngàn, ý nhị, đậm đà, bát ngát” và vẻ đẹp toát lên từ sự đảm đang, tần tảo của người phụ nữ. Một số món ăn khác lại khá lạ lùng và có chút đáng sợ với không ít người, đó là cháo cóc, khi ăn vào thì “ăn hương thơm đồng ruộng, ăn… những bản nhạc dân ca, ăn… bao nhiêu cuộc ân tình ‘ra rít’ vào lòng”; là món dơi huyết rất ngon và bổ, trị được nhiều bệnh cùng câu chuyện kỳ lạ về Bạch phu nhân; là món bò kiến thơm ngon, lạ miệng và câu chuyện về ích lợi của giống kiến vàng,… Mỗi một món ăn hay một câu chuyện đều chứa đựng tình yêu và sự gắn bó của tác giả đối với Miền Nam. Qua đó, ta thêm hiểu và cũng thêm phần yêu quý đối với mảnh đất quê hương mộc mạc nhưng rất đỗi chân thành này.
5
387532
2015-07-29 18:17:56
--------------------------
240966
6390
Vũ Bằng luôn có một giọng văn mượt mà, duyên dáng, gần như có thể nói ông là người một khi mở miệng là "lời vàng ngọc" tuôn như mưa. Nên việc sở hưũ một văn phong cá tính như vậy vừa là ưu điểm nhưng cũng có bất lợi. Đọc "Món lạ miền Nam", theo mình, việc cứ phải nhai đi nhai lại từng hồi tưởng một của ông về các cảm xúc vụn vặt đi kèm cùng với những món ăn không nổi trội (nổi trội có bao hàm ý nghĩa đã so sánh với hoàn cảnh xã hội thời đó), ta không thấy được sức quyến rũ khó cưỡng như khi nếm qua trong tưởng tượng những Phở, những Cốm của Vũ trước đó. Nhưng ai biết được, mình đọc tác phẩm nổi tiếng nhất của ông rồi mới đọc "Món Lạ Miền Nam" mình hoàn toàn có thể bị ảo giác "lần đầu" che mắt. Tin hay không tùy bạn, hay ta nói như một nhà phê bình ẩm thực sành sỏi (vì đây là cuốn sách của "quốc hồn quốc túy" nhá) "Ngon hay không tùy bạn!"
3
292321
2015-07-25 02:58:37
--------------------------
233806
6390
Mình là một người đam mê ẩm thực - muốn nhìn và thử các món ăn trên, trước tiên là Việt Nam đã, bởi thế cuốn này mình đã mua ngay để khám phá món mới rồi nhỡ biết đâu vào Nam mình sẽ thử. Những cũng vì cuốn này mà mình cảm thấy "ức chế" vì muốn thử ngay quá nhưng chắc chắn sẽ không được rồi. Mấy bạn miền Nam đọc tức sẽ chiêm nghiệm lại và biết thêm mấy món xứ sở của mình mà không biết còn mấy bạn miền Bắc - Trung nên đọc để khám phá những món ăn của một vùng đất dân dã "xứ" Nam. Vũ Bằng với lối dẫn truyện rất duyên, ông kể món ăn rồi cả những cái chuyện "tám dóc" để tránh sự nhàm chán. Cái cảm nhận của một nhà văn - một người như bao người khác với những người là chuyên gia ẩm thực sành ăn sẽ cho bạn cái cảm nhận khái quát nhất và đúng vị nhất. Nếu mà liên tưởng sinh động nữa thì đừng hỏi tại sao bạn nuốt nước bọt nhé.
4
472508
2015-07-19 22:03:05
--------------------------
217363
6390
Quá là lạ! Những món mà dường như tưởng không thể nào nuốt được vào bụng, thế mà vẫn được. Dù tôi là một người miền Nam nhưng chưa bao giờ biết đến và chưa bao giờ thử những món này. Tác giả xoáy sâu vào cách làm, cách thưởng thức, cái vị của món ăn mà tác giả cho là "lạ". Gắn thêm vào đó là những mảnh tình, những câu chuyện từ người nấu bếp hay người đánh bắt nguyên liệu chuẩn bị món ăn. Nếu có dịp, tôi sẽ thử vài món mà tác giả đã giới thiệu. Một cuốn sách thật hay và thú vị!
5
448654
2015-06-29 10:50:53
--------------------------
204056
6390
Bởi Vũ Bằng không phải con dân miền Nam, nên với ông món miền Nam là lạ, chứ không phải là ngon như Hà Nội. Phải nói là ông đặt tên tác phẩm đúng qúa. Không phải là người tinh tường món miền Nam thì sao mà có thể đánh gía ngon dở. Thôi thì ta nói về khoản lạ vậy, mà cái chính ăn không chỉ bằng miệng, mà còn phải dùng lòng để cảm. 
Ui chao! Cả cuốn sách nào đâu chỉ có nói về 8 món chính như tựa đề mỗi chương. Đọc sách, theo ông tám chuyện Đông Tây, hàng lô lốc món ngon được mô tả hồn nhiên mà chân chất, duyên cực, đến cái bao tử cũng xót xa, len lét nuốt nước bọt tới mấy lần. 
Đọc xong, càng thấy yêu sao cái mảnh đất của những con người thiệt là "thực thà như đếm, yêu ai thì lộ liễu,..."
4
104850
2015-06-02 23:57:07
--------------------------
133741
6390
Mình đang tìm hiểu về ẩm thực Việt Nam thì bắt gặp quyển sách này đang giảm giá trên tiki nên rinh ngay về luôn. Sách mỏng nhẹ, đọc nhanh hết. Đọc xong thì đúng là món lạ miền Nam thiệt. Sách làm phong phú thêm kiến thức không chỉ về món ăn mà còn về cách thức chế biến. Tuy nhiên, sách mang tính chất là tác phẩm văn học nhiều hơn. Tình cảm của tác giả được mô tả khá nhiều qua những trang viết về món ăn. Đối với bản thân mình, cuốn sách không đặc sắc lắm, bởi vì mình thích cách viết về ẩm thực của tác giả Giáng Uyên hay Dương Thụy nhiều hơn. Một điều làm mình ngạc nhiên là tác giả là người Bắc, lấy vợ miền Nam chứ không phải là một người Nam, nhưng cách dùng từ trong sách khá là gần gũi với phong cách một nhà văn Nam Bộ. Mình cho sách này 3 sao.
3
63149
2014-11-06 19:22:04
--------------------------
121044
6390
Quyển sách này thật đặc biệt.  Nó không phải là một quyển sách dạy nấu ăn như ta lầm tưởng khi đọc qua tựa đề, mà là sách nói về ẩm thực nhưng lại lồng trong đó là lòng yêu quê hương, yêu những con người mộc mạc chân tình. Qua ngòi bút của tác giả, những món ăn được làm ra đều thật là ngon,  vì đó là cả tấm lòng người nấu đều gửi trọn trong đó, làm những món lạ đó bỗng trở nên thật quen thuộc, làm ấm lòng du khách thưởng thức. Mà phải nhìn nhận một điều là tác giả đã khéo chọn những chi tiết rất độc đáo, đặc trưng của từng món lạ làm chúng ta khi đọc xong đều rất ấn tượng và muốn nếm thử liền. Dù tác giả cuốn sách này là người Hà Nội nhưng khi đọc ta lại thấy giọng văn rất mộc mạc, chân thật rất thích hợp để viết về miền Nam. Tôi tin rằng ai đọc qua sách này đều cảm thấy thêm yêu mến miền Nam với những món ăn thật lạ lẫm, kỳ cục nhưng đong đầy tình người.
5
277587
2014-08-16 20:34:06
--------------------------
121035
6390
Mới nhận được sách là mình ôm ngay luôn. Cuốn sách mỏng, ôm một buổi chiều là đọc xong hết. Mình gốc Bắc, nhưng từ khi sinh ra đã ở trong Nam, mẹ mình cũng là người Nam nên ăn món Nam cũng kha khá. Ấy vậy mà khi đọc cuốn sách này lại ngớ người ra: " Sao món gì lạ thế?". Toàn thịt rùa, thịt chuột rồi lại dơi. Nghe giống mấy món nhà hàng ghê, ấy mà qua cách viết của Vũ Bằng, nó lại hiện lên chân chất, dân dã (như nó vẫn là như thế), và khiến bụng ta lại đói cồn cào dù vừa mới xơi xong một phần thức ăn nhanh nào đó. Một cuốn sách mộc mạc, gợi cho người ta cảm giác gần gũi, thân thương với quê nhà, sao lại có thể thiếu trong tủ sách của một người con đã sống và yêu cái mảnh đất Nam Bộ này chứ?
4
90147
2014-08-16 19:33:35
--------------------------
