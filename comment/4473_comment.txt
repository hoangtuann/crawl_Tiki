483371
4473
Đánh liều mua thử quyển này do thấy mọi người review bảo hay. Hình thức thì ổn, bọc sách cẩn thận, không có gì phàn nàn. Vừa mới nhận được lúc trưa nên cũng chưa đọc được nhiều. Nhưng phải thừa nhận tác giả có ngòi bút cực kỳ đặc trưng rất khó lẫn lộn vào các tác phẩm khác. Văn phong chút đuộm buồn đúng kiểu mình thích, lời lẽ có phần khiêm nhường nhưng bật lên được những nội dung sâu lắng của tác phẩm. Nói chung, chỉ mới đọc được vài đoạn, nhưng chắc chẳn nó đủ sức thuyết phục mình rồi !!!
4
868799
2016-09-13 17:40:09
--------------------------
477184
4473
Câu chuyện này, nên nói thế nào đây. Ấn tượng ban đầu của nó là thật bí ẩn, sau đó, nó thật buồn. Nó không có những tình tiết gây cấn, những xung đột, nhưng vẫn có nỗi buồn ẩn trong đó. Nhân vật trong truyện không nhiều, nhưng hầu như ai cũng cho mình cảm giác kiểu: "Không chừng chương sau họ sẽ tự sát đấy". Truyện đã đọc xong rồi, nhưng những suy nghĩ về nó vẫn cứ lởn vởn bên mình. Chắc mình bị ám mất rồi haha.
Và mình nghĩ, tình cảm mà Sui dành cho Kazami, có lẽ không phải là tình bạn đâu nhỉ. Nghe giống tình yêu hơn ấy. Nếu là tình yêu, thì quả thực là một tình yêu ngọt ngào, luôn bên nhau những lúc đối phương cần nhất.
Trong truyện này thì mình vẫn thích nhất mối quan hệ giữa Sui và Kazami ấy. Cảm giác như giữa bao nhiêu chuyện, hai con người họ vẫn bình yên ở bên nhau, bình yên cùng cười, cùng nói, cùng nương tựa vào nhau. Thậm chí Sui còn có ý muốn chết cùng nhau luôn cơ mà.
Ban đầu mình cũng không định mua cuốn sách này đâu, nhưng cuối cùng vẫn quyết định mua, và giờ mình nghĩ, nếu ngày xưa mình không mua chắc giờ tiếc chết mất haha.
Ờm, và chắc là vì mình đọc nó lúc trời mưa nên giờ buồn quá à huhu
5
1459437
2016-07-25 15:43:11
--------------------------
419389
4473
Tôi giới thiệu cuốn sách này cho một đứa bạn. Nó bảo, "Cuốn sách này ám ảnh đến cùng cực mày ạ!". Ừ, tôi cũng cảm thấy y hệt như thế!
Banana đưa người đọc đi hết nỗi đau này đến nỗi đau khác, hết mất mát này đến mất mát khác. Dường như những sự việc xảy đến nằm ngoài khả năng chịu đựng và chấp nhận của con người bình thường. Những giằng xé sâu kín trong nội tâm của mỗi nhân vật, những khắc họa nặng nề và ám ảnh về cuộc sống thường nhật của mỗi nhân vật, những mối quan hệ rõ ràng xuất phát từ tình cảm chân chính mà lại rất phức tạp của mỗi nhân vật... tất cả đẩy người đọc men theo dòng chảy cảm xúc cuồn trào dữ dội.
Dường như, đọc "N.P" cũng như những tác phẩm khác của Banana, luôn buồn đến đau thương. Nhưng lại hấp dẫn và lôi cuốn đến khó cưỡng!
5
468633
2016-04-21 10:10:42
--------------------------
405432
4473
Thật sự nội dung cuốn sách là cái gì mình cũng không tài nào nắm bắt hay nói cho rõ được. Thế nhưng, cuốn sách lại cuốn hút mình một cách kì lạ. Đọc nó mình không thể nào ngừng lại giữa chừng. N.P đặc biệt vô cùng. Chưa một cuốn sách nào cho tôi một cảm giác như vậy. Mơ hồ, bay bổng và vô cùng cuốn hút. Nhân vật chính đưa ta vào thế giới được liên kết bởi một sợi dây vô hình của cô và Saki, Otohiko, Sui - 3 đứa con của ông nhà văn nổi tiếng. Và câu chuyện đầy niềm vui, nỗi buồn của họ đã mở ra....
5
746405
2016-03-26 18:40:01
--------------------------
400350
4473
Khắc khoải. Đó là từ đầu tiên hiện lên khi tôi đọc xong và suy nghĩ về N.P. Có quá nhiều cảm xúc gợn lên trong lòng nhưng không hiểu sao thật khó diễn tả, nó cứ khắc khoải khắc khoải đến đau đớn. Có lẽ dùng từ đau đớn thì hơi cường điệu nhưng quả thực tôi cũng cảm thấy đồng cảm với nỗi đau và sự bi thương của các nhân vật. Tình yêu đồng giới đã quá chênh vênh, mà tuyệt vọng hơn còn là tình yêu cùng huyết thống. Tác giả không chỉ miêu tả đó như một sự đau khổ mà còn là giới hạn để các nhân vật gắng gượng tìm cho mình một lẽ sống đích thực. Tác phẩm của Banana Yoshimoto là thế, buồn thảm, ám ảnh nhưng vẫn le lói hy vọng.
4
386342
2016-03-19 10:32:34
--------------------------
371662
4473
Dù trong N.P vốn có rất ít nhân vật nhưng tôi vẫn muốn lượt bỏ đi tất cả chỉ để nói về Kazami và Sui.
Từ khi người yêu của Kazami là Shoji tự sát, Kazami như bị chia làm đôi. Một cái “tôi” bị kẹt tại thời khắc ấy, và một cái “tôi” cho hiện tại. Sự ám ảnh về cái chết vẫn luôn bao trùm lên Kazami. 
Sui, một cô gái có quá khứ bi thương, bị cha mẹ bỏ rơi. Khi lớn lên, trớ trêu thay người cô yêu lại là cha ruột, rồi khi cả hai phát hiện ra việc loạn luân ấy, cha cô đã tự sát. Mối tình thứ hai lại là anh trai cùng cha khác mẹ...
Nếu Sui là đóa hoa cẩm tú trong cơn bão thì Kazami lại là mùa thu dịu dàng. Cô bị thu hút bởi nỗi đau của người khác, đó là lý do phần nào Kazami nhận ra đằng sau nụ cười của Sui là một vết thương sâu hoắc, đôi mắt Sui như một mặt nước đen thẵm dưới đáy giếng cũ. Tất cả những điều ấy là điềm báo của một người sẽ tự tử, Kazami biết mình yêu Sui từ lần đầu tiên gặp. Nhưng cô không bao giờ tỏ tình với Sui, vì Sui có khác nào một hư ảnh? Nhất định có lúc Sui sẽ biến mất. Trên hết, Kazami sợ lần nữa mất đi người mình yêu. Cô sợ sẽ lập lại bi kịch khi xưa với Shoji.
Cái gì đến cũng sẽ đến, Sui tự tử nhưng bất thành, điều cuối cùng cô để lại cho Kazami là một nụ hôn. Rồi Sui bỏ đi, không hẹn gặp lại. Tôi phải thét lên rằng thà cứ để Sui chết đi, còn hơn để Kazami cứ phải nuôi hy vọng. Nếu Sui giúp Kazami hàn gắn lại cái “tôi” bị lạc kia. Thì lần này chính Sui đã khiến Kazami đánh mất bản thân lần nữa, ngay ở giây phút họ hôn nhau. Đầu hạ họ gặp, cuối hạ kết thúc tất cả…
Sui tiếng Nhật nghĩa là Thủy (nước), không ai có thể bắt được nước. Kazami chỉ có thể cảm nhận. Nhưng mãi sẽ không thể có được Sui trọn vẹn, kể cả chính Sui. Cô cũng lẩn quẩn mãi trong hành trình tìm ra bản ngã.
Yoshimoto Banana đặt lời nguyền lên N.P. Buộc người đọc trăn trở theo số phận nhân vật. Hai tâm hồn không có gì giống nhau trừ nỗi đau, nhưng chính nỗi đau ấy lại là thứ gắn kết họ sâu sắc nhất. Dường như cả khi im lặng họ cũng cất lên nỗi buồn của riêng mình. 
Tuy đề cập về đồng tính, loạn luân, nhưng Yoshimoto Banana không hề nhấn mạnh vào những vấn đề gai góc để câu nước mắt độc giả. Trái lại bà xoa dịu nó bằng tình người, bằng sự cảm thông. Đến giờ tôi vẫn nghĩ nhà văn không viết cho tất cả, nhà văn chỉ viết cho ai có sự tương đồng với điều họ viết. Và N.P là một tác phẩm nhen nhón trong tôi sự đồng cảm, nhân vật trong N.P không phát rồ vì cô đơn, trái lại họ tự thấy vẻ đẹp của cô đơn và chấp nhận sống cùng với nó.
Kazami và Sui, hai con người ấy sống trong thế giới đa chiều riêng biệt của bản thân. Với họ thời gian như ngưng đọng, trong không gian nhỏ hẹp ấy mọi thứ trở nên thật tù túng. Đến cả hơi thở của họ cũng bị bóp nghẹn. Ký ức của họ không được phơi bày, họ chỉ trao cho người đọc từng mảnh rời rạc. Để ta tự chấp vá, và câu hỏi liệu Sui có tự tử lần nữa? Mãi không có đáp án. Nhưng biết đâu vì không có đáp án nên mới tạo ra hy vọng sống cho Kazami. Trên hết, sống chính là điều đẹp đẽ nhất.
4
43494
2016-01-20 09:29:51
--------------------------
326253
4473
Một cuốn sách ám ảnh và hay đến nghẹt thở. Cái hay đầu đến từ văn phong của tác giả ( hay người dịch) nhẹ nhàng, lơ lửng và yên ả. Dường như cái thế giới trong sách đã hoàn toàn lột tả được cái mùa hè sinh động rực rỡ mà ai cũng muốn yêu, ai cũng lưu luyến. Cuốn sách lấy đề tài là tình yêu đồng tính và đồng huyết nhưng lại khiến cho người ta cảm thấy một cái gì rất khả dĩ, rất hiển nhiên, rất bình thường. Đó là cái hay rất đặc trưng của Banana Yomoshito.
5
186092
2015-10-25 09:52:17
--------------------------
311860
4473
Lại một lần nữa Banana đưa người đọc vào một thế giới tâm lý phức tạp nhưng đầy rẫy những bài học về cuộc sống. Với những ai sợ đề tài đồng tính và loạn luân, đây có lẽ không phải lựa chọn tốt. Nhưng với những người mong muốn tìm kiếm cho mình một lời giải đáp về ý nghĩa của cuộc sống, về cách nhìn cuộc đời, con người và văn chương, "N.P" là một gợi ý tuyệt hay. Cũng giống như nhiều tác phẩm khác của Banana, "N.P" là những cái chết trở đi trở lại đầy ám ảnh và u buồn. Nhưng khép cuốn sách lại, chúng ta thấy lóe lên những hi vọng vào tương lai. Giấy in đẹp, tuy nhiên mình không thích bìa của cuốn sách này lắm.
4
475008
2015-09-20 11:49:23
--------------------------
294502
4473
Mua cùng lúc với "Tugumi", song N.P lại là câu chuyện khác khiến lòng nặng trĩu khi gấp cuốn sách lại. Mình đã không đọc kĩ review sách nên không hề biết cuốn sách viết về đề tài tình yêu đồng huyết và cả đồng tính nên khi đọc sách mình có đôi chút bất ngờ. Vì đây không phải là đề tài mình thực sự quan tâm. Không phải mình kì thị hay có ý gì khác chỉ là những đề tài như vậy nó vượt quá sức chịu đựng của mình. Quá bi thương và ám ảnh nên mình thường tránh đọc. Dẫu vậy vẫn cố lướt hết cuốn sách và rồi mình hiểu vì sao những ai dịch câu chuyện 98 ấy đều tự sát cả. N. P quả là một cuốn sách đầy những nỗi buồn u ám về tình yêu, cái chết. Quá bất hạnh và đau thương.
4
75031
2015-09-09 13:42:37
--------------------------
292079
4473
Nội dung quyển sách lôi cuốn người đọc vì chính bản thân nó. Truyện nhẹ nhàng nhưng thấm đượm những nỗi buồn về những mối tình khác biệt của những người đồng giới, đồng huyết. Thế nhưng một trong những điều cuốn hút còn lại trong tác phẩm là văn phong nhẹ nhàng, sâu lắng và trên hết là niềm tin, là sự lạc quan về cuộc sống được thể hiện trong câu chữ. Banana đã tạo nên một thế giới trong quyển sách này, một thế giới khác biệt, pha trộn giữa sự yên ả là chút lửa âm ỷ nổi loạn, và còn có ánh sáng của sự giải thoát
4
123775
2015-09-06 23:32:57
--------------------------
275780
4473
Banana luôn biết cách biến chuyển giọng văn đặc trưng của mình, khiến đọc giả càng thêm yêu thích những gì cô viết. "N.P" với số lượng nhân vật không nhiều nhưng lại nói đến nhiều vấn đề nhạy cảm và gai góc hơn ta tưởng: tình yêu đồng tính và đồng huyết. Thật lạ là những đề tài này tưởng khó khăn qua ngòi bút tinh tế và khiêm nhường của Banana lại đi vào lòng người 1 cách nhẹ nhàng như vậy. Câu chuyện "N.P" gắn chặt không ngờ những con người trẻ lại với nhau 1 cách kỳ lạ, họ gặp gỡ, chạm vào đời sống nhau và cũng như phần nào đó cứu rỗi cuộc đời có lẻ đã bế tắt quá lâu của nhau. Ngòi bút tác giả chậm rãi và từ tốn, thẳm sâu như 1 mặt hồ nhưng lại đưa ta đến 1 thế giới thật nhiều sắc màu và "N.P" dường như đã bừng sáng rực rỡ như thế ấy!
4
46905
2015-08-23 10:57:18
--------------------------
252755
4473
Đến với "N.P" trong qua tác phẩm "Vĩnh Biệt Tugumi", nhưng mình hơi bàng hoàng vì quyển N.P này thật khác, Sui thật sự không có một tý gì giống với cô bé Tugumi, nhưng khi đọc 2 tác phẩm cùng một lúc, bạn chắc chắn sẽ nghĩ rằng Banana Yoshimoto không hề viết theo kiểu lối mòn chút nào cả, cả 2 quyển sách đều khác, rất khác nhau, tuy nhiên văn phong của cô vẫn nhẹ nhàng, hơi u buồn và khắc khoải, nhưng không vì thế mà làm cho "N.P" trở nên nhàm chán chút nào, nó vẫn pha một chút kịch tính.
5
616945
2015-08-03 21:48:30
--------------------------
231993
4473
Vẫn với văn phong nhẹ nhàng, sâu lắng nhưng đầy khắc khoải, tác giả Banana Yoshimoto một lần nữa đưa ta đến với cái thế giới tưởng như êm ả, thanh bình nhưng luôn âm ỉ sự nổi loạn mà ta thường thấy trong các tác phẩm của cô. Trong thế giới đó, bằng mối giao cảm kỳ lạ, những con người với quá nhiều nỗi đau như Kazami, Sui, Otohiko gặp gỡ, gắn kết với nhau và mang đến cho nhau cả niềm hạnh phúc lẫn nỗi đau. Đó là mối tình bị ngăn cấm giữa hai anh em Otohiko và Sui; là sự thấu hiểu, gần gũi trên cả mức bạn bè giữa Kazami và Sui; là sự đồng cảm giữa Kazami và Otohiko. Thế nhưng, dù vui hay buồn thì ẩn sâu trong từng câu chữ của tác phẩm vẫn chứa đựng tình yêu, niềm tin và cái nhìn đầy lạc quan của tác giả về cuộc sống. Chính vì thế, đây thật sự là một tác phẩm rất tuyệt vời. 
4
387532
2015-07-18 15:11:06
--------------------------
