303192
3786
Sách tổng hợp những câu chuyện phá án khá li kỳ của nhiều nước. Có lẽ do viết cho trẻ em nên các câu chuyện được kể khá ngắn gọn cảm giác hơi thiếu hấp dẫn. thông qua các dữ kiện các em có thể cùng suy luận, phân tích và phá án với các nhân vật. Mỗi truyện còn được vẽ minh họa in màu đi kèm rất dễ thương. Đối với các bé thích suy luận, trinh thám thì đây là cuốn sách khá hấp dẫn tuy nhiên đối với trẻ đã lớn thì chúng sẽ thích những truyện dài có nhiều tình tiết hơn. Sách không quá dày, chất lượng giấy tốt cầm không mỏi tay rất phù hợp với thiếu nhi. 
4
43129
2015-09-15 18:28:25
--------------------------
260706
3786
Sách có nội dung hay, các câu truyện thật li kỳ và hấp dẫn, với giá trị như vậy mà lợi ích nhận được từ sách thì nhiều hơn, bạn đọc cho con nghe về các câu chuyện hấp dẫn. Từ đó tạo cho trẻ thói quen đọc sách rất tốt. Mình đã mua hai quyển của tác giả Vương Diễm Nga và rất thích, nội dung nhiều nên tha hồ mà đọc. Sách để giành cho trẻ kỹ năng luyên đọc là tuyệt vời luôn nhé.
Chất lượng giấy cũng Ok nha, cầm vừa gọn trong tay nên không nặng lắm đâu.
5
85410
2015-08-10 16:45:36
--------------------------
