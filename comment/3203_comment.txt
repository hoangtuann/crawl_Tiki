276323
3203
Robin Hood là nhân vật thân thuộc, gần gũi với những bé thích với thể loại truyện phiêu lưu. So với đọc sách toàn chữ, thì sách truyện minh họa bằng hình vẽ này sinh động hơn nhiều. Câu văn đơn giản, nhưng nhược điểm là sẽ không chi tiết cho cốt truyện.
Điểm cộng cho sản phẩm là có các trò chơi cho bé phát triển các kỹ năng. Một sản phẩm sách thông minh cho bé sắp, đã, và đang học chữ. 
Thiết kế bìa đẹp, chất lượng giấy tốt, in ấn rõ ràng. Tôi rất hài lòng.
3
478050
2015-08-23 21:33:07
--------------------------
247836
3203
Một cuốn truyện song ngữ 34 trang với hình thức đẹp mắt, cuốn hút, giấy in màu, bóng và cứng rất đẹp.
Về nội dung, ở trang 19 có sự trùng lắp 1 đoạn cuối với trang 18 gây khó hiểu. Phần dịch thuật, tên nhân vật Little John không thống nhất, lúc là John Nhỏ, lúc là Little John.
Do cuộc phiêu lưu của Robin Hood rất nhiều tình tiết, nhân vật và được gói gọn trong 1 cuốn truyện thiếu nhi nên nội dung truyện chỉ mang tính chất kể lại các tình tiết, đọc không thấy lôi cuốn và hấp dẫn lắm!
Phần cuối sách có một số trò chơi thú vị cho bé có liên quan đến câu chuyện: đếm số mũi tên và kiếm, tìm số lượng từ Robin Hood trong ma trận từ, sắp xếp các từ được xáo trộn để thành tên các nhân vật trong truyện. ghép hình với bóng, tìm đường đi, tìm điểm khác biệt, tô màu, đố vui.
Một cuốn sách song ngữ để các bé bồi dưỡng khả năng đọc tiếng Anh, xem hình đẹp và chơi các trò chơi thú vị kèm theo chấp nhận được.
3
292190
2015-07-30 16:51:52
--------------------------
