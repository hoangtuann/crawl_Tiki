262818
8526
Tôi vừa mua cho con gái quyển sách này cùng với bộ hai quyển Bách Khoa Tri Thức, dành tặng cho cháu sau một mùa hè chăm chỉ rèn chữ đạt được kết quả đáng khích lệ. Cả cháu và mẹ đều rất thích vì sách bìa cứng, giấy dày, láng và đặc biệt các hình ảnh minh họa in màu sắc rất đẹp. Nội dung sách được tác giả chia thành nhiều chủ đề từ cấp độ dễ đến khó tăng dần. Mở đầu quyển sách cháu có thể làm quen bảng chữ cái ABC, sau đó đến phần chữ số,... Phần màu sách rất ấn tượng vì ngoài cách học từ vựng thông thường, sách còn minh họa cách phối màu giúp các cháu có thể ứng dụng cách phối màu vào bài tập vẽ của mình. Ngoài ra sách còn có đĩa CD tặng kèm giúp cháu nghe và tập phát âm cho đúng giọng chuẩn. Tôi cảm thấy vui và hài lòng khi mua được cho cháu quyển sách hay.
5
326812
2015-08-12 09:48:13
--------------------------
