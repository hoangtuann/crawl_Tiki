466077
4494
Trước hết bìa đẹp, nội dung khá tốt. Tác phẩm đưa vô số thuật ngữ COD, DCDS...và thiết bị thử nghiệm, lấy dấu vết tân tiến: chỉ việc lấy dấu vân tay hoặc lấy máu đã có chục cách từ đơn giản đến phức tạp làm người đọc choáng ngợp nhưng không ngán mà tạo hứng thú. Từ ngữ miêu tả các xác chết thỏa mãn cho những ai thích sự kinh dị (mặc dù vs tôi chưa đủ :)) ) :ngón tay bị lóc thịt đến tận xương, mùi thịt luộc, tróc từng mảng, thân hình vặn xoắn...thêm vào đó là những pha đấu trí nhiều lần và đấu sức sau cùng giữa Lincoln và hung thủ gây sự hồi hộp và hấp dẫn. Điểm đắc của tác phẩm là làm độc giả nhiều lần lầm tưởng hết người này đến người khác là hung thủ. Kết, chủ mưu xuất hiện và biến mất ngay làm dân chúng bàng hoàng 2 tập :| Một tác phẩm đáng đọc
4
946012
2016-07-01 20:43:26
--------------------------
445300
4494
Là tập đầu trong loạt truyện trinh thám về nhà hình sự học Lincoln Rhyme, Kẻ tầm Xương đưa độc giả đến cuộc rượt đuổi để truy bắt gã sát nhân hàng loạt ở thành phố NewYork. Lincoln, cựu giám đốc pháp y của sở cảnh sát NewYork, tuy bị liệt cả 2 chân nhưng bằng  tài năng và kinh nghiệm của mình, đã phân tích và phán đoán tuyệt vời trong cuộc rượt đuổi này. các vụ án xảy ra liên tiếp, vô cùng hồi hộp và có 1 chút kinh dị trong đó. chỉ có 1 diều mình cảm thấy đáng tiếc đó là lời đề tựa ở bìa sau đã tiết lộ 1 chút nội dụng sách. 
4
577041
2016-06-09 22:44:17
--------------------------
424292
4494
Đây là cuốn trinh thám của Jeffery Deaver mình đọc, mình nghe nói khá hay nên quyết định mua.
Bìa sách không được đẹp cho lắm nhưng chất lương tốt, bao của tiki cũng rất đẹp.
Khi bắt đầu đọc truyện lê thê, dài dòng có những chi tiết không cần thiết nhưng càng về sau thì lại càng gay cấn hơn, nhất là ở những cuộc đối đầu của thám tử đại tài và tên giết người lọc xương, Deaver đã miêu tả được chính xác nôi tâm của tên giết người biến thái.
Mặc dù không phải tác phẩm trinh thám hay nhất của Deaver nhưng Kẻ tầm xương cũng là một cuốn sách đáng để đọc.
4
1293922
2016-05-01 12:26:30
--------------------------
388466
4494
Có lẻ tôi đã trông đợi quá nhiều về tác phẩm này nên đọc có cảm giác khá hụt hẫn vì: 
Thứ nhất: truyện là câu truyện phá án khá kịch tính nhưng trông chờ nhiều vào thiết bị 1 cách khuôn mẫu và trùng lập, như vậy làm cho biệt tài phá án của nhân vật chính giảm đi bội phần.
Thứ hai: truyện dông dài và không biết sao mà mình không vảm nhận được hết sự biến thái bệnh hoạn của tên sát nhân. Chắc là do bản dịch
Ngoài hai vấn đề trên thì truyện là 1 tác phẩm tạm được, phải dành lời khen cho tác giả vì đã nghiên cứu rất kĩ các thiết bị máy móc, thương tác giả 
Tác phẩm này kén người đọc.
2
931040
2016-02-29 00:06:41
--------------------------
387672
4494
Đây là cuốn thứ 2 mình đọc của tác giả Jeffery Deaver. Theo cảm nhận của mình thì cuốn này k kịch tính bằng cuốn Dữ liệu tử thần. Nhưng nội dung quyển này cũng rất hay. Nhất là những đoạn kể vê kẻ sát nhân cũng những miêu tả cách thức hắn ra tay vơi nạn nhân,làm mình rất ám ảnh. Quyển này cũng nói về cuộc sống đầy tuyệt vọng của Rhyme cũng như sự cố gắng của a đê vượt quá những đau
đớn sau tai nạn, và 1 chút nhen nhóm tình yêu giữa a va Amelia. Kết thúc truyện cũng rất bất ngờ về kẻ sát nhân. 
4
743912
2016-02-27 13:25:56
--------------------------
382713
4494
Cũng giống như các cuốn sách khác của JD, Kẻ Tầm Xương có nhiều yếu tố kịch tính, gay cấn, hồi hộp khiến người đọc khó có thể rời mắt
Mặc dù từ đầu đến cuối, các tình tiết hay đều và cho người đọc thấy được bộ óc ưu tú của Lincoln Rhyme, nhưng nếu những ai đã đọc nhiều truyện của JD rồi đề có thể thấy nhà hình sự học này dựa vào công nghệ quá nhiều, nhiều đến mức lúc nào cũng phải đợi tìm chứng cứ để phân tích. Tuy nhiên, việc đó không ảnh hưởng đến nội dungcủa câu truyện vì nhìn chung, đây vẫn là một cuốn trinh thám- hình sự hấp dẫn.
4
712991
2016-02-19 15:28:50
--------------------------
297550
4494
Mặc dù rằng đây là tác phẩm đầu tiên của Jeffery Deaver nhưng mình đã đọc các cuốn sau rồi mới tới cuốn này. Quả như tên gọi của nó "Kẻ tầm xương" cả câu chuyện là sự đấu trí giữa nhà thám tử tài danh Licoln Rhyme và tên sát thủ máu lạnh có sở thích lóc xương người chết. Mình sẽ không kể nội dung truyện ra đâu nhưng trong truyện có vài chi tiết hơi rùng rợn nên nghiêm cấm các bạn yếu bóng vía. Phải nói rằng Jeffery Deaver là 1 bậc thầy về thể loại truyện trinh thám khi ông có thể miêu tả chính xác tâm lý của tên sát nhân như vậy. Đúng là 1 tác phẩm trinh thám đáng đọc
4
39383
2015-09-11 22:27:54
--------------------------
297421
4494
Jeffery Deaver là một tác giả nổi bật của thể loại trinh thám. Những tác phẩm của ông đầy thú vị và kịch tính. Qua mỗi trang sách ta càng thêm hào hứng với mạch chuyện hay. Các yếu tố hành động cũng như suy luận luôn song song tồn tại với nhau tạo sự hứng thú cho người đọc. Không chỉ có vậy, các yếu tố suy luận cũng rất hay. Từng suy luận sắc bén của Lincoln được tác giả viết lên rất độc đáo và hấp dẫn. Trong The Bone Collector - tập truyện đầu tiên trong series Lincoln Rhyme tất cả dường như được tác giả xem xét rất tỉ mỉ đến từng chi tiết của vụ án. Những điều đó đã tạo nên những vụ án kinh điển với sự tài ba của nhà hình sự học cùng các cộng sự!
4
620158
2015-09-11 20:55:18
--------------------------
289686
4494
Mình rất thích cách kể chuyện của Jeffery Deaver, nó rất khó đoán về nội dung nhưng cái cách dẫn chuyện của ông thì không thể chê vào đâu được, có thể nói không thể nhầm lẫn giọng kể của ông với bất kì tác giả nào. Cách mà ông xây dựng nhân vật rất chân thực, mình rất thích hai nhân vật chính nhất là Lincoln Rhyme. Đọc Kẻ Tầm Xương mình luôn có cảm giác tò mò từ từng trang sách, không hiểu tại sao tên sát nhân luôn để lại những manh mối chỉ đường cho cảnh sát và nhất là tại sao hắn lại cứu Lincoln trong khi Lincoln cũng là một trong những mục tiêu của hắn.
5
564154
2015-09-04 19:35:11
--------------------------
288547
4494
Mình chỉ mới nhận sách hôm thứ ba và mình dành nguyên ngày lễ 2 / 9 để ngồi đọc cuốn sách này . Tuy chưa đọc xong cuốn sách , nhưng mình thấy sự gây cấn chính là ấn tượng đầu tiên của mình về tác phẩm này. Truyện thuộc thể loại kinh dị (  thể loại này mình rất ư là thích )  khá hay , súc tích , đầu đủ chi tiết với những tình tiết hết sức hấp dẫn . Tuy nhiên quá trình phá án bị lặp lại nên dễ gây chán cho người đọc . Nhưng ngược lại mình lại thích cách suy luận bởi thám tử , nó rất logic , làm người đọc dễ bị cuốn theo câu chuyện . Và tác giả từ từ lôi kéo người đọc đi từ bất ngờ này đến bất ngờ khác, người đọc tưởng chừng như đã biết hung thủ , nhưng rồi lại như chưa biết gì cả , khiến người đọc lại càng muốn tìm hiểu hơn xem xem ai là hung thủ . Xong nội dung, bây giờ mình sẽ nói đến hình thức . Truyện được minh họa rất đẹp, ma mị , màu sắc ổn , chất lượng giấy cũng được , nói chung là khá  ổn . 
4
619059
2015-09-03 17:26:01
--------------------------
251964
4494
Điều thú vị khi đọc Kẻ Tầm Xương là nhân vật trong truyện chỉ trải qua 1 2 ngày thôi và tác giả thâu tóm nó bởi hơn 500 trang sách. Xét về mặt nào đó thì đây là tác phẩm tác đọc, tuy nhiên theo mình không phải là hay đến mức nghẹt thở...

Jeferry viết truyện thường hay xoáy nhiều vào việc khám nghiệm, phân tích hóa học. Điều này làm rõ hơn cho tác phẩm. Song cá nhân mình thì không kết lắm vì theo dõi mà đôi khi chẳng hiểu mô tê gì vì mấy cái nhân vật làm là các thí nghiệm thuộc chuyên ngành điều tra nên không thể tự mình suy đoán được. Mặc khác bối cảnh thực sự không tạo được độ thoáng vì chỉ có nhà của Rhyme và hiện trường thôi. Có lẽ đánh giá này hơi chủ quan vì mình thích đọc kiểu trinh thám mà trong rừng hay trong lâu đài, có cây cối này nọ làm không khí sợ hơn.
4
137500
2015-08-03 11:34:38
--------------------------
242371
4494
Đây là quyển đầu tiên trong loạt truyện về thám tử Lincoln Rhyme. Trước tiên mình rất khâm phục Lincoln, mặc dù bị liệt hầu như toàn thân nhưng vẫn phán đoán bước đi và hành động của hung thủ một cách rất chính xác. Toàn bộ câu chuyện là quá trình rượt đuổi của phía cảnh sát và tên sát nhân, trước khi hắn bắt đầu vụ án mới. Truyện tập trung nhiều vào các bước phân tích hiện trường và vật chứng cùng với nhiều công nghệ, dụng cụ tiên tiến, hiện đại hỗ trợ, tất nhiên là không thể thiếu óc quan sát và suy luận của nhân vật thám tử. Mình đánh giá đây là một quyển trinh thám hay, diễn biến liên tục, hồi hộp. Nhất định mình sẽ tìm đọc các quyển khác của tác giả này.
4
582958
2015-07-26 16:03:07
--------------------------
223525
4494
Một cuốn sách trinh thám dành cho những ai mới bắt đầu. Bạn sẽ bị cuốn vào cuộc điều tra với hàng loạt những chứng cứ, dấu vết, những máy móc và trang thiết bị hiện đại, những kế hoạch tác chiến và liên kết giữa các đơn vị cảnh sát.. Cuốn sách giống như một bộ phim hình sự, gay cấn hồi hộp, có đủ các yếu tố của một tác phẩm đậm chất "Mĩ". Tuy nhiên việc phá án quá phụ thuộc vào khoa học công nghệ, quá trình phá án lặp đi lặp lại dễ khiến người đọc nhàm chán. Điểm hay của tác phẩm đó là lối viết dễ đọc, mạch lạc, các sự việc rõ ràng cụ thể, và đoạn kết tuy không mới nhưng cũng làm cho người đọc có được một chút bất ngờ, một cái kết mở bao giờ cũng đem lại sự chờ đợi và lôi cuốn!
4
637976
2015-07-07 10:52:00
--------------------------
214217
4494
Gay cấn chính là ấn tượng đầu tiên của mình về tác phẩm này. Truyện thuộc thể loại kinh dị (mình rất thích thể loại này á :3 ).

Cách miêu tả tỉ mỉ của tác giả về những vụ án cũng như nội tâm của các tội phạm. Và tất nhiên yếu tố kinh dị chính là điều hấp dẫn mình ở cuốn sách này :))).

Bối cảnh truyện cũng tốt, cách xây dựng nhân vật tuyệt vời, không quá nhàm chán cũng không hề tẻ nhạt chút nào.

Luôn đem đến nhiều điều bất ngờ chính là lối viết của nhà văn Jeffery Deaver. Một lối viết đầy mê hoặc cũng đầy những điều kì bí.

Truyện được minh họa rất đẹp, ma mị, màu sắc ổn, chất lượng giấy cũng không tồi.

Đây là một quyển sách hay, nếu là fan của truyện kinh dị có lẽ bạn không nên bỏ qua tác phẩm này!!!!
4
621744
2015-06-24 17:57:42
--------------------------
195253
4494
Thật ra mình xem phim trước rồi mới tìm đến tác phẩm để đọc. Vốn không mấy thích thể loại kinh dị hình sự điều tra nên mình có chút ít nhận xét chủ quan thế này. 
Truyện có bắt đầu khá kịch tính, gay cấn, bối cảnh câu chuyện xoay quanh một tên giết người hàng loạt, tâm lý tội phạm và người điều tra cũng được Jeffery Deaver miêu tả khá tỉ mỉ, đặc biệt là sự ám ảnh của hung thủ bởi những mảnh xương của nạn nhân khiến người đọc "dựng tóc gáy", mình cũng mắc bệnh sợ ma nữa nhưng thật sự ở nửa đầu câu chuyện khó mà dứt ra được dù lúc đọc "kẻ tầm xương" khi ấy cũng là đêm muộn rồi. 
Bối cảnh truyện trong thành phố NY nhưng cũng không khiến người đọc cảm thấy tẻ nhạt, điều tẻ nhạt trong truyện là nằm ở các vụ án kìa, đọc được đến 2 vụ án đầu thấy lôi cuốn lắm, nhưng bắt đầu đến vụ thứ 3 lại thấy nó tẻ tẻ, kiểu tình tiết lặp ý, không còn kịch tính như ban đầu nữa. Chỉ có đến đoạn gần kết và kết thì mình mới thấy thu hút trở lại. 
Nói chung đọc kẻ tầm xương của JD cũng khiến tâm lý người đọc khá "thăng trầm" nhé.
4
125574
2015-05-13 10:10:55
--------------------------
192765
4494
Đây là quyển đầu tiên mình đọc của tác giả Jeffery Deaver. Đọc được 2 trang đầu là mình đã thấy truyện rất hấp dẫn. Kịch tính ngay từ đầu truyện, lối viết nhanh và dồn dập làm mình khó bỏ được cuốn truyện xuống. Nhưng các tình tiết cứ thế lặp lại và cách thức điều tra, thu thập chứng cứ tuy tỉ mỉ nhưng quá phụ thuộc vào công nghệ làm mình nhàm chán dần. Bối cảnh xoay quanh cuộc điều tra ở New York nên đọc cũng hơi rối. Được cái là kết khá ổn. Một điểm cộng nữa là câu chuyện sẽ giúp ta có cái nhìn rõ hơn về việc điều tra hiện trường, về cách thức lần ra hung thủ từ những manh mối nhỏ nhất, những mảnh siêu vụn mà hung thủ vô tình để lại hiện trường. Nó thật sự vất vả, đòi hỏi nhiều kỹ năng, sự tỉ mỉ và đầu óc phán đoán chứ không phải chỉ nhờ vào máy móc và hóa chất.
4
494249
2015-05-05 16:47:15
--------------------------
181261
4494
Có những ám ảnh về quyền lực và sự khát khao được biết đến nặng nề đến nỗi, có kẻ dám tước đoạt mạng sống của người khác chỉ để chứng tỏ quyền lực của hắn. Lại cũng có những ám ảnh về sự bất lực và khổ đau khiến có người muốn từ bỏ mạng sống của chính mình chỉ để quên.

Rồi hai kẻ đó đối đầu nhau. Kẻ này quyết tiêu diệt kẻ kia cho thỏa cơn cuồng bạo, kẻ kia quyết vạch trần kẻ này để được chết nhẹ nhàng.

Chỉ có người đọc là thót tim.
5
125725
2015-04-11 14:53:16
--------------------------
174800
4494
Đúng như tên gọi của tác phẩm, kẻ giết người trong câu chuyện bị ám ảnh bị những chiếc xương của con người, hắn có thể giết một người đang sống để 'sưu tầm' 1 mảnh xương nhỏ nào đó.
Câu chuyện ảm ánh người đọc bởi những cái chết ghê rợn, những chi tiết mô tả trong câu chuyện làm cho người đọc như nghe thấy những âm thanh khi hung khí tiếp xúc với xương người, cảm thấy lạnh cả người như thể hung khí đó đang ở rất gần ta. 
Và tác giả từ từ lôi kéo người đọc đi từ bất ngờ này đến bất ngờ khác, người đọc tưởng chừng như đã biết hung thủ, nhưng rồi lại như chưa biết gì cả.
4
301260
2015-03-28 22:05:43
--------------------------
