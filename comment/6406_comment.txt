352055
6406
Sách trình bày đẹp, bắt mắt và đọc mau hết. Mình thấy cũng rất bổ ích cho những ông bố bà mẹ có con nhỏ. Quyển sách quy tụ những nội dung về việc dạy con theo khoa học, hiện đại. Sách trình bày khá chi tiết và đầy đủ về việc giáo dục trẻ, giáo dục về mọi phương diện liên quan đến trẻ. Theo mình thì mình khá hài lòng khi mua quyển sách này, giúp mình có cái nhìn rõ hơn về việc giáo dục sao để trẻ thông minh, và cũng có động lực hơn trong việc áp dụng khoa học dạy con.
4
259917
2015-12-13 15:40:33
--------------------------
252570
6406
Mình đã đọc cuốn sách này và thấy rất hay. giờ mua để tặng người bạn đang chuẩn bị sinh baby.Sách viết rất dễ hiểu, có ghi rõ từng mốc thời gian của bé những năm tháng đầu đời để mẹ theo dõi các hoạt động bé. Sách có nhiều màu làm nổi bật từng mục và tạo cảm giác thích thú cho người đọc. Ngoài ra, mình có mua thêm combo bọc sách 40 cuốn của Tiki, thấy được bọc rất đẹp. Sách lớn sách nhỏ gì cũng tính là 1 cuốn. Cảm ơn Tiki rất nhiều. Mình rất hài lòng.
5
661202
2015-08-03 19:40:06
--------------------------
136929
6406
Bạn đã có con ?
Bạn đang nghĩ đến việc có con ?
Nếu câu trả lời là có thì bạn hãy ngay lập tức mua cuốn sách này
Mặc dù tôi đã quên gần như toàn bộ nội dung của quyển sách này (tôi không ghi chú lại những điều tôi đã học được trong quyển sách vì dường như từng từ trong quyển sách tôi đều thấy quan trọng) nhưng quyển sách này truyền cảm hứng cho tôi rất nhiều. Cho nên tôi khuyên mọi người nên mua quyển sách này, đặc biệt là những cặp vợ chồng trẻ vừa có con.
Một cuốn sách tuyệt vời!!!
5
462453
2014-11-24 00:30:18
--------------------------
