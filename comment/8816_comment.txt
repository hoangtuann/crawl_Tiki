407630
8816
Tôi từng đọc nhiều bộ tiểu thuyết xuyên không, với "Kim ốc hận" tôi quả thật đã được nếm trải nhiều cung bậc cảm xúc, cảm nhận sâu sắc sự khác biệt so với các bộ xuyên không khác. Nữ chính vốn là cảnh sát từng chịu huấn luyện gian khổ, khốc liệt nên đối với tình cảm âm thầm sâu sắc của đế vương có phần trốn tránh, lãnh tình, hờ hững và có cả oán hận thưở ban đầu. Cung đấu khốc liệt, bản chất đế vương vô tình được khắc họa sắc nét qua ngòi bút Liễu Ký Giang. Nam chính tàn bạo, quyết đoán nhưng không hờ hững, lãnh tình với tất cả thân nhân, ngoại thích nhưng lại không thể phủ nhận tình cảm sâu sắc , bám rễ từ thời thơ ấu dành cho Trần A kiều. Vứt bỏ rồi tìm lại để đau đớn, để hối hận, quay đầu và cũng để hiểu được yêu là gì. Tác phẩm này là nét chấm phá không quá đặc sắc nhưng nhất định chẳng thể bỏ qua trong mảng tiểu thuyết xuyên không
4
1273634
2016-03-30 14:44:15
--------------------------
380438
8816
Kim ốc hận không những không có hận thù mà còn đảo ngược tình thế đang rất phóng túng , cảm nhận của người đọc không đi sâu vào tình tiết về mọi vấn đề quan trọng , yêu cầu chứa đựng một vài thói quen minh họa khôi phục lại nền văn minh cổ xưa của người trung hoa nhưng cũng khó tin về bậc trí hiền tài xuất hiện tại đôi lúc mở đầu câu truyện với mối hiểm họa khôn lường , khó nhìn thấy ngay lập tức , chưa thấy truyện lơ lửng , ngắt đoạn 
3
402468
2016-02-15 00:24:27
--------------------------
344750
8816
Mình đã đọc trên mạng nhiều lần, đợt này tiki sale nên quyết định mua.
Có thể lúc đầu truyện hơi dài dòng nhưng càng về sau mình càng thích. Thích từng nhân vật trong đó. 
Sự nhẫn tâm của những người quyền lực nhưng trong đó vẫn có sự yêu thương của những người thân.
Tác giả cho thấy sự mâu thuẫn của quyền lực và tinh yêu nhưng rồi cuối cùng chúng vẫn chung hòa và sống cùng nhau.Sự tranh đấu quyền lực của 2 bà mẹ nhưng rồi vẫn không bỏ được tình thương con. A Kiều thua Vệ Tử phu chỉ vì tình yêu chân thành của mình. Rồi sau đó Vệ Tử Phu thua cuãng vì tình yêu đó. Cuối cùng ai là kẻ đáng hận, ai là kẻ đáng thương.
Tôi vẫn ấn tượng nhất Lưu Triệt vị đế vương quyền uy. Tuy anh xuất hiện rất trễ nhưng tác giả vẫn cho thấy quyền uy và sự tàn nhẫn của bậc đế vương. Nói anh vô tình ko đúng muh nói anh có tình cũng không phải. A yêu A Kiều, bồi đắp cho những tháng ngày lỗi lầm nhưng anh ko dung túng cho người thân của A Kiều, thậm chí có thể nói là lợi dụng triệt để. A thương những đứa con của mình nhưng vẫn tự tay đẩy chúng vào vòng xoáy quyền lực. Sự mẫu thuẫn nội tâm giữa tình yêu và quyền lực sau khi gặp lại A Kiều.
5
104026
2015-11-29 00:08:58
--------------------------
343931
8816
Đây là câu chuyện xuyên không nhàm chán và vớ vẩn nhất mình từng đọc, dù Hán Vũ Đế và Trần A Kiều, Vệ Tử Phu đều là những nhân vật hết sức nổi tiếng trong lịch sử Trung Quốc và mình rất tò mò về câu chuyện cuộc đời, cuộc tình của họ. Chả hiểu tác giả nghĩ gì mà cho nhiều nhân vật xuyên không thế, đã vậy còn buff nữ chính quá đà dẫn đến cảm giác "lố", một nữ cảnh sát khi xuyên về lại thành đại tài nữ cái gì cũng biết cái gì cũng giỏi khiến mình thấy phản cảm. Tình tiết nhàm chán, mạch truyện lúc nhanh lúc chậm không phù hợp
1
630600
2015-11-27 14:27:32
--------------------------
337125
8816
Mới mua cuốn sách này vào hội sách vài ngày trước. Mình dựa vào nhận xét trên tiki để mua hai cuốn truyện này. Thật sự đọc vài dòng đầu thì rất khó hiểu vì quá nhiều nhân vật, quá nhiều tình tiết đan xen. Nhưng càng đọc càng thích. Một phế hậu Trần A Kiều đáng thương với tâm hồn bị tổn thương. Yêu phải bậc đế vương Lưu Triệt - hoàng đế với ngàn phi tần mĩ nữ để rồi bị y quay lưng không chút tiếc thương.

Một câm chuyện nhẹ nhàng, thấm vào tâm can của đọc giả. 
Một tình yêu bị phản bội để lại đầy đau thương và sinh mạng của hai đứa trẻ.
Một câu chuyện hay với một kết thúc đẹp.

4
936735
2015-11-13 17:10:47
--------------------------
322779
8816
Đầu tiên nhìn cái tên với bìa truyện thì nói thật là đã thu hút được sự chú ý của tôi nhưng mua về đọc rồi mới thấy nội dung truyện đúng là tỉ lệ nghịch với cái bìa.Cốt truyện dài lan man tình tiết rắc rối nhưng không gây hứng thú gì cho người đọc,nói chung là không hề có một tình tiết bất ngờ nào ờ thì cũng cung đấu đấy nhưng tôi thấy nó rất nhạt.Nhân vật thì cũng không có ai gây được ấn tượng,nữ chính xuyên không cái kiểu gì mà tôi thấy cái gì cô ta cũng biết,cũng uyên bác,cứ làm như ở thời hiện đại cô ta là siêu nhân vậy,thật là vô lý.Đọc truyện này chỉ tổ tốn thời gian chứ không lưu lại được gì.
3
526322
2015-10-16 23:14:28
--------------------------
318310
8816
Phải nói là mình cảm thấy rất may mắn khi chưa mua cuốn sách này. Mình mượn của bạn để đọc. Vừa đọc xong hôm nay, phải lên Tiki viết nhận xét luôn không thì quên nội dung truyện mất.
Tình tiết trong truyện nhiều, và rất rối. Khi chuyển cảnh tác giả viết rất đột ngột làm mình đọc thấy mơ hồ, khó hiểu. Nữ chính xuyên không quả là quá xuất sắc, việc gì cũng biết làm và việc gì cũng rất giỏi. Điều đáng tiếc sau khi gấp lại cuốn sách là tình cảm của Tiêu Phương dành cho Hàn Nhạn Thanh. 
Cuốn sách này có lẽ không phù hợp với mình, và chắc cũng không phù hợp với các bạn đọc truyện chỉ để giải trí như mình. Ưu điểm của truyện là bìa rất đẹp. Vì lúc trước mình nhìn bìa rất đẹp nên mới có ý định mua.
2
173372
2015-10-05 18:14:17
--------------------------
311435
8816
Đây là cuốn sách đầu tiên của Liễu Ký Giang mà mình đọc và mình khá hài lòng về tác phẩm này. Nội dung truyện khá hấp dẫn, cốt truyện mới lạ, nhiều chi tiết hấp đặc sắc, thu hút, khiến người đọc không thể ngừng lại. Hơn nữa, tác giả đã thành công trong việc xây dựng hình tượng và miêu tả cảm xúc nội tâm của các nhân vật trong truyện. Một điểm cộng nữa là bìa sách nhìn rất đẹp, giấy in cũng mỏng và nhẹ. Nói chung đây là một tác phẩm hay và đáng để đọc, mong rằng trong tương lai sẽ có cơ hội được đọc thêm những tác phẩm hay như vậy nữa.
4
115397
2015-09-19 23:11:58
--------------------------
299157
8816
Đọc xong câu chuyện ,mình bỗng có ý nghĩ "được một lần xuyên không như cô nàng Hàn Nhạn Thanh" để được sống trong bối cảnh cổ xưa. Từ cái tích " Kim ốc tàng kiều" trong lịch sử thời vua Hán Vũ Đế mà tác giả đã viết nên một tác phẩm hay đáng để đọc. Bối cảnh vả nhân vật lịch sử xa xưa nhưng Hàn Nhạn Thanh đã xuyên không và nhập vào thân xác phế hậu Trần A Kiều đã làm thay đổi câu chuyện tình của Hán Vũ Đế Lưu Triệt và biểu tỉ A Kiều. Họ đã yêu - phế - hận -xa nhau rồi lại trở về bên nhau , sống với nhau mấy chục năm cũng trong yêu -hận và yêu. Nếu nàng A Kiều thật của lịch sử được đắm chìm trong tình yêu của Lưu Triệt dành cho nàng như trong truyện thì chắc sẽ mãn nguyện và không còn mang trong lòng một mối hận vì " Kim ốc tàng kiều" Với văn phong mượt mà , ngôn từ tự nhiên , lồng được nhiều bài thơ cổ hay để diễn tả tâm tư nhân vật , tác giả đã khắc họa nên một câu chuyện tình tuy cũng có sóng gió nhưng cũng không kém phần lãng mạn. Kết thúc truyện đem đến cho ta một cảm giác nhẹ nhàng vì các nhân vật đã bằng lòng với số phận mình như thế.
4
41068
2015-09-13 01:59:24
--------------------------
293912
8816
Trước khi mua bộ tiểu thuyết này, mình đã đọc ở trên mạng một phần câu chuyện. Có thể với đa số độc giả khác thì bộ truyện này không hay, có phần dài dòng, ca tụng nữ chính quá đáng... nhưng với mình thì đây là một bộ tiểu thuyết hay. Nữ chính có vẻ là quá tài giỏi, nhưng bỏ qua việc nhỏ này thì toàn bộ các chi tiết khác đều hay cả. Diễn biến truyện khá chậm : từ lúc xuyên qua đến khi chính thức gặp mặt lần đầu tiên của nam nữ chính  - không kể những lần gặp thoáng qua - là mười năm ; Tình tiết truyện, diễn biến tình cảm của nhân vật logic. Một điểm khá mới lạ của truyện là nữ chính xuyên không là kiếp sau của A Kiều, khi xuyên không về thì hợp nhất hai nửa linh hồn, trí nhớ quay về, A Kiều bây giờ mang trong mình tình cảm và kiến thức ở cổ đại đồng thới cũng mang tư duy, kiến thức, lý trí của người phụ nữ hiện đại. Đó cũng là điểm làm Hán Vũ đế yêu A Kiều. Mình thích Hán Vũ Đế, là vua của một quốc gia và cũng là Triệt Nhi của A Kiều. Mình thích A Kiều, yêu hận rõ ràng, tình cảm và lý trí dung hoà. Nếu đánh giá về nội dung thì mình sẽ cho năm sao, nhưng phần dịch thuật lại làm tiểu thuyết mất hay, không diễn tả hết được không khí cổ xưa, ưu thương, mất đi chất thơ của truyện, mình không cảm nhận hết được tâm trạng của A Kiều và cả tình cảm của Hán Vũ Đế, không diễn tả được sự lưu luyến, day dứt trong tình cảm của họ nữa. Dù sao đi nữa thì Kim Ốc Hận -  tiểu thuyết đã thành công phá vỡ hình tượng A Kiều trong phim truyền hình mình đã xem và dung hợp với Trần A Kiều trong lịch sử, tình cảm của hai nhân vật chính đau buồn nhưng cũng đầy ngọt ngào đã ghi dấu với mình. 
4
39798
2015-09-08 20:52:21
--------------------------
277888
8816
Tôi mua truyện này của Tiki tại hội chợ sách! Bao bìa đẹp, hình ảnh bìa đẹp lung linh! in ấn trên nền giấy tốt, rất hài lòng với chất lượng sản phẩm! Về phần nội dung tạm được.  Cốt truyện xoay quanh đề tài cung đấu, còn dựa trên một số yếu tố lịch sử của Trung Quốc! tuy nhiên truyện hơi vài dòng, miêu tả đôi lúc quá chi tiết làm cho người đọc như tôi lại cảm thấy nhàm chán!  Nữ chính thì cứ như "siêu nhân" vậy! :) Tôi mua rất nhiều truyện tại hội chợ sách nên bộ truyện này tôi cứ đọc được vài trang là tạm ngưng để đổi sang truyện khác.  Truyện không tẹ nhưng lại không thu hút được sư kiên nhẫn của tôi :) tôi thích đọc truyện giải trí mà đọc truyện này thì lại rối lên, hehehe 
3
544616
2015-08-25 13:48:49
--------------------------
276858
8816
  Truyện dài dòng, tình tiết lại không đặc biệt. Câu chuyện cứ như sự nối dài của một chuỗi sự việc lê thê, không có hồi kết. Không có cao trào, có bi kịch nhưng không đau lòng. Nữ chính vừa buông vừa nắm, yêu không thừa nhận, cứ cố chấp giữ gìn thứ gì đó nhưng bản thân lại không biết đã trao người từ lúc nào rồi. Vốn dĩ nữ chính không quyết đoán, không lý trí như được miêu tả, mà chỉ là tự lừa mình, bởi nhút nhát nên không dám dấn thân mà khẳng định tình yêu, chỉ âm thầm chờ đợi, im lặng quan sát của Vũ Đế.
  Tuy nhiên, đọc Kim ốc hân, biết được nhiều bài thơ, bài phú rất hay. Có lẽ điểm cộng chỉ là vậy.
3
30531
2015-08-24 13:51:54
--------------------------
231255
8816
Bộ sách này mình được bạn tặng khá là lâu rồi dạo này rãnh rỗi lấy ra đọc và mình có vài nhận xét.
Đầu tiên là về hình thức:phải công nhận một điều rằng là bìa sách rất đẹp,đẹp lung linh luôn là đằng khác.Mình lại là một người luôn coi trọng bìa sách vì thế mình dành hẳn một sao cho phần bìa.
Còn về nội dung có lẽ vì ấn tượng về phần bìa quá lớn nên mình có hơi hụt hẫng vì phần nội dung.Mình cảm thấy cuốn sách có hơi dài dòng dẫn đến nhàm chán,nếu không vì quá rãnh rỗi mình thật sự nghĩ sẽ không có đủ kiên nhẫn để đọc hết cuốn sách.Còn một điều nữa tuy là xuyên không nữ chính có cần hoàn hảo đến thế không.
2
463303
2015-07-17 22:46:47
--------------------------
219652
8816
Tôi rất ấn tượng với mối tình của Hán Vũ Đế và hoàng hậu Trần A Kiều, đặc biệt là điển tích "Kim Ốc Tàng Kiều" nên mới mua quyển sách này. 

Tuy nhiên, tác giả đã không khai thác tốt điển tích này, thậm chí làm mất đi cái hay, cái bi, cái tình của điển tích qua việc cho nữ chính xuyên không vào A Kiều, việc xuyên không này không phải không hay, cái hay ở đây là A Kiều đã trở nên mạnh mẽ và quyết đoán hơn, không còn đi vào lối mòn uất hận như lúc trước, còn cái không hay là tác giả viết nữ chính xuyên không vào thân xác A Kiều quá xuất sắc, quá giỏi giang, việc này khiến mọi thứ trở nên không thật và vô cùng gượng gạo. Truyện quá dài và có nhiều tình tiết dư thừa, phải chi có thể khai thác sâu hơn về mặt tình cảm, cung đấu hoặc nhân tình, chứ không sa đà vào việc nữ chính xây dựng cơ ngơi riêng của mình thì đã hay hơn rồi. 

Tuy nhiên, nói đi thì phải nói lại, những mối tình, câu chuyện đan xen giữa 3 nhân vật Lưu Triệt, Trần A Kiều và Vệ Tử Phu cũng khiến tôi phải ngẫm nghĩ nhiều. Nào có mối tình Đế Vương nào mà không đẹp đẽ, không đau xót, không bi ai và không oán hận. Có điều Kim Ốc Hận chẳng phải tiếng ai oán giày vò của người thiếu phụ nơi cung cấm lạnh lẽo, mà là tiếng thở dài về cuộc đời về nhân tình thế thái.
3
80843
2015-07-01 19:35:50
--------------------------
178069
8816
Mình từng chần chừ khá lâu mới quyết định đọc bộ này, đơn giản vì trót thích Vệ Tử Phu và ghét Trần A Kiều ngay từ khi chưa đọc truyện. Nhưng đọc rồi thì không hề hối hận. Đoạn đầu tác giả viết còn non tay, nhưng về sau thì chắc tay hơn hẳn. Tình cảm của Lưu Triệt và A Kiều không thắm thiết nồng nàn, mà êm đềm nhưng sâu đậm. Chỉ hơi tội Lưu Triệt bị ngược lâu quá, phải đến gần cuối truyện mới lay động được A Kiều. Mình rất thích hai bài thơ trong truyện này, đặc biệt là phần dịch thơ của dịch giả, rất mượt mà.
4
57459
2015-04-04 10:06:07
--------------------------
170124
8816
Đây là một trong những truyện cổ đại – xuyên không tồi tệ nhất mà tôi từng đọc. Bìa truyện rất đẹp, quà tặng dễ thương nhưng nội dung quá nhảm. Xây dựng trên nền là mối tình giữa A Kiều và Hán Vũ Đế - mối tình vốn quen thuộc với  rất nhiều người – nhưng lại nhạt nhòa khiến người đọc muốn quên ngay sau khi gấp sách lại. Truyện dài mà diễn biến lúc quá gấp, lúc quá rườm rà, tình tiết và nội tâm thì hời hợt, đã thế lại còn phi logic. Dù nữ chính từ hiện đại nhập vào thân xác A Kiều thì cũng không cần phải giỏi đến mức đất trời biến sắc, từ nữ công gia chánh may vá thêu thùa đến trị nước an dân đều giỏi không giống người vậy chứ?
1
393748
2015-03-19 14:45:37
--------------------------
165883
8816
Đợt rồi bộ này được giảm tới 50% lại tặng kèm nhiều quà rất đẹp nên tôi quyết định mua, nhưng đọc xong mới thấy hối hận khủng khiếp. Mối tình giữa hoàng hậu A Kiều với Hán Vũ Đế vốn rất nổi tiếng, rất đẹp, nhưng khi lên sách lại chẳng đem lại cho tôi cảm giác đó. Truyện dài lê thê, văn phong nhạt nhẽo không có điểm nhấn, nhân vật quá nhiều nên bị rối. Nữ chính là người hiện đại xuyên vào thân xác A Kiều, nàng có thể có đôi chút khác biệt với mọi người thật, nhưng có cần “thần thánh” đến mức không gì không biết, không gì không giỏi vậy không? Dù sao ở thế kỉ 21, nàng cũng chỉ là cảnh sát chứ có phải thiên tài đâu.  Tác giả tung hô nữ chính ghê quá, đọc mà ức chế.
1
449880
2015-03-11 13:08:23
--------------------------
149754
8816
Tôi mua bộ truyện này trong hội sách giảm giá 50%, trước hết vì thấy bìa truyện rất đẹp và quà tặng kèm thì lung linh. Nhưng thất vọng hơn tôi nghĩ, tôi bỏ cuộc khi mới đọc được hai phần ba tập một và chán đến mức không thể đọc hết trọn bộ. Truyện quá dài, làm người đọc thấy nản và đuối. Nhân vật thì nhiều vô kể, không thể nhớ được ai với ai. Cốt truyện chưa ổn lắm, nhiều tình tiết vô lý, lại lằng nhằng, rối rắm. Truyện lấy bối cảnh lịch sử Trung Quốc, đan xen thể loại xuyên không. Nghe thì rất hấp dẫn nhưng cá nhân tôi thấy văn phong của tác giả còn chưa khéo léo. Nữ chính Trần A Kiều vốn là nữ cảnh sát ở thời hiện đại, chả hiểu sao đến khi xuyên không về quá khứ lại cứ như thánh thần vậy, từ ca hát, nấu nướng, pha trà, may vá, thiết kế, kể cả công việc chính sự đương triều,... cái gì cũng biết tuốt, thông thạo. Quá tài giỏi, quá may mắn, quá..phi lý. Tính cách cũng không ổn, không phù hợp với bối cảnh truyện. Nam chính Lưu Triệt đúng kiểu tình yêu hoàng tộc thời xưa, có mới nới cũ, phế hậu rồi lại lập hậu, phụ bạc tấm chân tình của A Kiều. 
Không biết có phải do tôi không hứng thú lắm với lịch sử, và cũng chưa đủ trải đời để hiểu hết những phức tạp trong lòng người hay không, nhưng tôi hoàn toàn thất vọng về bộ truyện này và cũng không có ý định đọc tiếp, không những tốn tiền tốn thời gian mà còn đau hết cả đầu.
1
64131
2015-01-14 18:12:32
--------------------------
133116
8816
Trong lịch sử, Trần A Kiều từng là hoàng hậu của vua Hán Vũ Đế. Chuyện tình yêu của hai người ban đầu rất hạnh phúc vì quen nhau từ thời thanh mai trúc mã. Thế nhưng đau đớn làm sao, lúc tôi mới đọc lịch sử khi viết về vua Vũ Đế và hoàng hậu A Kiều, ông đã phế nàng chỉ vì không có con. Lúc ấy, tôi càng cảm thấy thương cho A Kiều bao nhiêu thì lại ghét loại người bội bạc như Vũ Đế bấy nhiêu. Sau này, suy nghĩ ấy của tôi mới được bác bỏ từ khi đọc Kim Ốc Hận. Thì ra, trong mọi chuyện, chuyện nào cũng có góc khuất của nó. Và những góc khuất ấy trong tôi đã được tháo gỡ qua bộ truyện này. Vua Vũ Đế trong bộ truyện từ khi phế nàng không lúc nào nguôi ngoai nỗi nhớ về nàng, nỗi nhớ mong đó đã nói lên được tình yêu tha thiết của người dành cho người bạn thời thanh mai trúc mã.
Bộ truyện này thực sự là một bộ truyện hay về tình yêu đôi lứa thông qua lịch sử. Nó sâu lắng, êm dịu, lãng mạn pha lẫn bi thương, tiếc nuối như trong cuộc sống chúng ta vậy. Có lúc vui, lúc buồn, lúc cười, lúc khóc...
Tất nhiên, truyện cũng còn vài lỗi cần phải chỉnh sửa như cốt truyện hơi gấp gáp...
4
292697
2014-11-03 22:18:13
--------------------------
105098
8816
Cuốn sách này là một bản nhạc. Một bản nhạc đa cung bậc: có trầm buồn, có bi thương, có hạnh phúc. Liễu Ký Giang đã thực sự làm tôi bị cuốn hút, buồn với nỗi bi thương của nhân vật, vui với niềm hạnh phúc của họ. Tôi yêu Lưu Triệt, một vị quân vương sẵn sàng "Hậu cung ba ngàn chỉ chọn một", nhưng tôi lại tiếc cho Tiêu Phương, một Tiêu Phương dịu dàng chu đáo không nhiễm bụi trần. Ông mang trong mình một tình yêu sâu nặng, nhưng tình yêu ấy náu mình kĩ quá, để đến cuối cùng tôi chợt giật mình nhận ra, à, thì ra trái tim ông từ lâu đã thuộc về người học trò yêu kiều năm nao. Một tấm chân tình bị bỏ lỡ, một trái tim mãi im hơi lặng tiếng, đứng cạnh bên chúc phúc cho người mình yêu, mong nàng hạnh phúc.
Hiếm có một cuốn sách mà khi đã đóng lại, dư âm còn vang mãi trong tôi... có ngọt ngào, có dịu êm, cũng có bi thương chua xót...
4
276542
2014-01-23 19:18:06
--------------------------
104783
8816
Khi mình đọc lời giới thiệu, lời đề phía sau cuốn sách, mình đã nghĩ đây sẽ là một câu chuyện xuyên không về tình yêu và quyền lực đầy mưu mô, đau khổ,day dứt. Ai cũng biết Hán Vũ Đế là một vị vua rất nổi tiếng của Trung Quốc, và càng xót thương hơn cho Trần A Kiều, phế hậu của ngài. Vì vậy, khi được biết nữ chính sẽ nhật vào A Kiều, kể lại trang lịch sử hào hùng ấy thì mình rất nóng lòng.

Nhưng, truyện lại ngược lại với những gì mình đã đọc. Tình tiết vô lý lại gấp gáp, nhập nhằng. Vô lý rành rành khi một cô gái là cảnh sát ở thời hiện đại, véo một cái về quá khứ, rồi cứ như mr. biết tuốt, pha trà, nấu ăn, làm xưởng may, thiết kế nhà,...chưa kể còn tham gia chính sự! Mình đọc Bộ bộ kinh tâm, cũng là xuyên không, nữ chính cũng tài giỏi, nhưng ít ra là còn có cơ sở, nhưng với Kim Ốc Hận, mình chả tìm thấy có tí logic nào cả. 
Đã vầy, truyện còn diễn ra vô cùng gấp gáp. Tình tiết cứ hời hợt thế nào ấy. Mình nghĩ là đúng là tác giả muốn cho độc giả biết rằng A Kiều là 1 cô gái tài giỏi, nhưng mình nghĩ chỉ cần xoáy sâu vào 1 số chi tiết, làm rõ nó, để người đọc cảm nhận, chứ không cần thiết ôm một đống rồi cái nào cũng chả viết cho nên hồn.

Mình mới đọc được tập 1, và thật sự là rất rất chán. Trừ cái bìa, lời đề, quà tặng đặc biệt ra, mình chả có tí ấn tượng nào cả
1
48098
2014-01-18 10:21:23
--------------------------
104457
8816
Cảm nhận sau khi tôi đọc xong truyện này là đuối. Truyện quá dài và nhiều tuyến nhân vật. Nếu không đọc cẩn thận có thể giống như lạc vào mê cung, chẳng thể nhớ nổi ai là ai. Nhưng mà ngược lại, tôi lại rất thích ở cách tác giả đặt nhiều tâm huyết cho truyện. Tuy cũng là truyện xuyên không, cũng theo khuôn mẫu như các truyện khác về một nữ chính tài giỏi, giản dị, chiếm được lòng người thì cách đưa lịch sử vào tron truyện là điều ấn tượng với tôi nhất. Kết cấu truyện chặt chẽ và rõ ràng, liền mạch, có thắt nút mở nút một cách hợp lý. Điều đó không gây nhàm chán cho người đọc. Tuy tôi không thích Lưu Triệt, một người bội bạc, nhưng nhìn lại những gì y đã trải qua, đã làm cho đất nước, cho muôn dân, và cho A Kiều sau này, tôi nghĩ có lẽ đó âu cũng là nhược điểm của y. May mắn là y đã có thể khống chế và khắc phục được nhược điểm này. Một lòng một dạ với A Kiều. Hạnh phúc nắm tay đi đến cuối đời, sống cùng giường, chết cùng lăng mộ. Như vậy còn gì tiếc nuối nữa cho một bậc đế vương anh minh?!
4
63376
2014-01-13 15:08:34
--------------------------
102361
8816
Ngay từ khi tôi nhìn thấy bìa sách, tôi đã rất thích sự nhẹ nhàng của bìa truyện.
Tôi thích văn phong của tác giả, thích cách dẫn dắt, những bài thơ được chèn vào.
Nhưng càng đọc tôi càng cảm thấy tình tiết truyện quá mức phi lý.
Thứ nhất, quá nhiều nhân vật xuyên không. Họ gặp nhau ở cổ đại dưới những bộ mặt xa lạ nhưng lại nhận ra nhau, quá nhiều cái tình cờ trong những cuộc gặp gỡ tạo sự nhàm chán.
Thứ hai, nữ chính sau khi xuyên không lại quá mức may mắn, quá mức tài giỏi, phi thường. Nữ chính là một cảnh sát nhưng đành nhau cũng tàm tạm, còn ca hát, thiết kế, kinh doanh lại thông thạo vô cùng. Mặc dù là người hiện đại nhưng tính cách của nữ chính lại quá mức tự nhiên đến vô duyên.
Thứ ba, nam chính không hấp dẫn. Ham mê sắc đẹp, có mới nới cũ.
Có thể tôi chưa đủ trải nghiệm để hiểu được nội dung trong Kim Ốc Hận. Nhưng theo quan điểm cá nhân thì tôi thật sự thất vọng về bộ truyện này.
2
3386
2013-12-17 19:54:03
--------------------------
90804
8816
Một sai lầm của mình là tự tìm hiểu về lịch sử giai đoạn này trước khi đọc nên mình cũng gần như biết trước 1 số sự kiện trong truyện, mình thích kết thúc hoàn mỹ của truyện tuy rằng hơi trái với lịch sử.

Từng làm mẹ, làm vợ nên mình thấm lắm những nỗi đau mà A Kiều phải trải qua, những vui buồn nhìn con trưởng thành và niềm vui sướng khi con công thành danh toại. 

Tập 1 của Kim ốc hận là những cay đắng, vui buồn khi A Kiều lang thang, nhưng có lẽ là khoảng thời gian vui vẻ nhất của nàng. Trở về cung cấm, trở về vói những ràng buộc, với lồng son của quyền lực, nàng đã biến mất đi nụ cười, nhưng lại trưởng thành hơn so với nàng Kiều khi xưa. 

Chiếm được tình yêu, nàng còn chiếm được cả lòng tin và sự tín nhiệm của chồng, người mà đến gần khi mất đi, nàng mới dám mở lòng đón nhận tình cảm. 
5
3462
2013-08-13 13:57:28
--------------------------
90737
8816
Lưu Triệt phế hậu, lại lập hậu. Vòng tuần hoàn phế - lập xoay đổi giữa hai người con gái, cũng đồng thời thay đổi vận mệnh thịnh suy của hai nhà Vệ - Trần. Liệu trong lịch sử, có một giờ, một khắc nào, Lưu Triệt từng nhớ về người con gái đã yêu kiều gọi tên chàng là Triệt Nhi ngày ấy.
Mạch văn chuyện chậm rãi, nhẹ nhàng như nỗi lòng nàng A Kiều thổn thức. Nàng đã dâng cho chàng thứ tình yêu trong trẻo, đẹp đẽ nhất của cuộc đời, nhưng đổi lại, chàng có cho nàng được sự thủy chung của một vị quân vương với hậu cung hằng hà sa số mĩ nhân. Dựa trên lịch sử đau thương, Liễu Ký Giang đã một lần nữa làm sống lại thứ tình cảm tưởng chừng như không bao giờ lấy lại được của Triệt Nhi - A Kiều tỷ. Nàng Vệ Tệ Phu dung nhan mĩ lệ một bước hóa thành phượng hoàng, rốt cuộc vẫn không phải là người nắm giữ được trái tim Lưu lang. Trải qua cơn bĩ cực, rốt cuộc, tình đẹp cũng chỉ trong thơ văn.
4
109776
2013-08-13 07:15:02
--------------------------
88913
8816
Tôi biết đến Kim ốc hận khá muộn, tuy không biết truyện hay dở ra sao nhưng vì tôi vô cùng yêu thích ngôn tình và cũng có một sự tò mò khó lý giải đối với triều Hán, thêm nữa thấy nhiều bạn đánh giá truyện rất hay nên cũng muốn đọc thử xem sao và quả thật tôi đã không hối hận với lựa chọn đó của mình.
Tôi thấy nhiều người không thích Kim ốc hận bởi truyện lồng ghép khá nhiều sự kiện lịch sử trong khi ngày nay nhiều bạn chỉ muốn đọc những câu chuyện về tình yêu. Tuy nhiên, tôi lại thích cách tác giả dung hòa giữa lịch sử và tình yêu. Không có cái nền là lịch sử thì sao tình yêu có thể thăng hoa? Hơn nữa, Kim ốc hận là một câu chuyện có bối cảnh rộng và bao trùm, không chỉ đơn thuần là câu chuyện về tình yêu nhi nữ mà còn là câu chuyện về thiên hạ và tình người. Trong câu chuyện này, tôi được thấy một Hàn Nhạn Thanh thông minh xinh đẹp, một Lưu Triệt tài giỏi và đa tình nhưng khi yêu lại không kém phần nồng nhiệt và chung tình, một Vệ Tử Phu tưởng như đáng giận nhưng kỳ thực lại vô cùng đáng thương, một Lưu Sơ, Lưu Mạch đáng yêu và tràn đầy tình thương. Ngoài ra những nhân vật, những mảnh đời khác trong Kim ốc hận cũng rất đáng khâm phục.
Hành  văn của Kim ốc hận tương đối chậm rãi, nhẹ nhàng nhưng lại thấm dần và lưu lại dấu ấn lâu trong lòng người đọc.
Theo tôi, Kim ốc hận là một câu chuyện rất đáng đọc.
5
123606
2013-07-29 12:41:06
--------------------------
88217
8816
Một cái kết khác cho một câu chuyện bi thảm trong lịch sử. Cảm giác nhẹ nhõm khi gấp lại những trang sách. Đến tận cuối đời Lưu Triệt có thể nhắm mắt yên nghỉ khi một lần nữa được người con gái năm xưa y từng vứt bỏ gọi hai tiếng "Triệt nhi". Kiều Kiều vẫn là Kiều Kiều của y năm xưa. Một cái kết nhẹ nhàng, lãng đãng nhưng chạm đến cảm xúc của tôi. 
Tôi yêu sách, yêu tiểu thuyết nhưng rất ít đọc những quyển tiểu thuyết ngôn tình. Tôi đến với Kim Ốc Hận khi xem qua review của Tiki, một câu chuyện lịch sử được xây dựng với gốc nhìn khác sẽ như thế nào đã kích thích sự tò mò của tôi và khiến tôi hoàn toàn "bấn loạn" với nội dung truyện. Cảm ơn tiki đã giới thiệu một quyển sách hay, đáng để đọc như thế ^^!
4
123278
2013-07-24 10:41:23
--------------------------
85457
8816
Tôi không phải là người chuộng các tiểu thuyết Trung Quốc, cũng không phải người yêu thích lịch sử. Ấy vậy mà tôi lại mê mệt cuốn sách hội tụ đủ cả hai yếu tố mình không thích này. 

Câu chuyện là một cái kết khác, hạnh phúc và đền đáp xứng đáng cho những hy sinh của Trần A Kiều - người con gái cao quý hết lòng vì tình yêu, nhưng cuối cùng lại bị phản bội, để lời hứa "Kim ốc tàng Kiều" năm nào trở thành một tiếng thở dài tiếc nuối.

Lãng đãng, nhẹ nhàng mà thấm vào tận tâm can, đó chính là cảm nhận của tôi lúc gấp cuốn sách lại. Nếu có thêm cảm giác nào khác, thì chính là sự thỏa mãn, thỏa mãn khi thấy Lưu Triệt phải trả giá cho lỗi lầm năm nào, thỏa mãn khi thấy y hối hận vì đã gây tổn thương cho người con gái mình yêu. Dù biết câu chuyện này khó lòng mà xảy ra trong đời thực, nhưng đây là tiểu thuyết, và tôi là con gái mà, tiểu thuyết viết ra là để thỏa mãn những mơ mộng của con gái, không phải sao? :)


5
132
2013-07-05 11:42:44
--------------------------
