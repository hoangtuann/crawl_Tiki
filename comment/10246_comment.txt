474354
10246
Tìm lại chính mình là một quyển  sách hay  và cuốn hút. Tác giả của quyển sách  không đưa những triết lý sáo rỗng, khô khan, khó hiểu. Bằng cách dùng  một câu chuyện được xây dựng trên bối cảnh  xa xưa, kể về cuộc hành trình “ tìm ại chính mình” của một nàng công chúa, Marcia Grad đem người đọc đến với những miền cảm xúc  thật tươi đẹp. Tôi rất thích cuốn sách này.  Nó là một cuốn sách đầy ý nghĩa , đẹp cả về nội dung lẫn thiết kế bên ngoài. Bìa sách  được thiết kế đơn giản nhưng đẹp.
4
64972
2016-07-11 12:18:58
--------------------------
359170
10246
Cuốn như cuộc hành trình về ký ức xa xôi gắn với ý nghĩ mập mờ , giống như tạo ra sự giải thoát về lựa chọn lớn , thay đổi bước tiến mới trong cuộc đời còn nhiều lối mòn chỉ ra thành công còn ẩn hiện ở phía trước mắt , tìm lại chính mình là việc chỉ cho ta thấy sự thay đổi công bằng với xã hội , với con người và bản thân là yếu tố quyết định nên thành bại cần học hỏi nhiều ở tâm lý và chắc chắn trong hành vi .
4
402468
2015-12-26 16:35:05
--------------------------
323009
10246
Thông điệp của cuốn sách đến với chúng ta thông qua câu chuyện về một nàng công chúa vỡ mộng trong hôn nhân của mình. Cô đã thất bại, cô chịu đau khổ nhưng từng bước cô đã tìm được lẽ sống đích thực, tìm cho mình những giá tri cốt yếu và hơn hết tìm thấy chính bản thân mình. Không hề khuôn sáo với những lời dạy giáo điều, cuốn sách truyền đạt ý nghĩa theo một cách thoải mái, mang đến sự hấp dẫn như đọc một truyện kể. Chỉ hơi tiếc đôi chút về hình thức, bìa sách thực sự chưa cân xứng với nội dung.
4
6502
2015-10-17 16:10:20
--------------------------
313486
10246
Cuộc sống này chẳng ai biết trước được chữ "ngờ". Chính vì vậy mà bạn đôi khi cũng thật khó nắm bắt bản thân. Bạn tưởng mình vẫn là đứa con ngoan trong khi đang hờ hững với cha mẹ mình. Bạn nghĩ rằng mình vẫn ổn trong khi hao tổn ngày đêm lượn lờ trên các trang mạng xã hội. Vậy bạn là ai? Bạn đang ở đâu giữa dòng đời chảy xiết này? Đã đến lúc tìm lại chính mình rồi! Cuốn sách hạt giống tâm hồn này sẽ như mội trải nghiệm quý giá để chúng ta đi tìm lại bản thân. Mình đã đọc và ngộ ra rất nhiều điều. Có thể bạn cũng nên thử xem!
5
636935
2015-09-23 19:56:19
--------------------------
247757
10246
khi mới mua về, mình thấy cuốn sách khá dày nên cũng hơi nhát đọc. Nhưng khi đã đọc rồi thì không thể nào đặt quyển sách xuống được, cứ mỗi phần thì nó lại đem đến cho chúng ta những bài học ý nghĩ khác nhau, những điều rất hiển nhiên mà đôi khi trong cuộc sống chúng ta không hề nhận ra. Lồng ghép vào đó là câu chuyện về một nàng công chúa và tình yêu của nàng, điều này khiến cho những bài học đến tự nhiên và dễ dàng tiếp thu được. Hãy cùng thưởng thức câu chuyện nào
4
485138
2015-07-30 16:44:34
--------------------------
239588
10246
Lâu lắm rùi mình lại đọc thể loại này. Sách được viết dưới dạng truyện cổ tích, thông qua những trải nghiệm của Công Chúa Victoria, ta như thấy được câu chuyện cổ tích của chính mình, những thử thách trong cuộc sống sẽ giúp chúng ta trở nên mạnh mẽ hơn, hoàn thiện hơn. 
Toàn bộ câu chuyện hướng tới 1 ý nghĩa :" Hãy là chính bạn" , bạn hoàn hảo theo cách riêng của mình chứ không theo cách người khác mong muốn (kể cả những người bạn yêu thương và yêu thương bạn)
Be yourself, you will be happy on your way :)
4
87487
2015-07-23 23:42:04
--------------------------
187600
10246
"tìm lại chính mình", một câu nói mà hẳn ai trong chúng ta cũng một lần kiếm tìm, "mình là ai trong cuộc đời này, mình có nhiệm vụ gì khi có mặt trên cõi đời này?", ai đã tự hỏi những câu này thì hẳn nhiên nên đọc cuốn sách: Tìm lại chính mình này, một cuốn sách làm quà tặng cho tâm hồn mình, để mộ lúc nào đó tĩnh lặng ta đi tìm ta, tìm lại con người đích thực của chính mình. Tôi rất thích cuốn sách này, một cuốn sách hay cả về nội dung lẫn thiết kế bên ngoài, một cuốn hạt giống tâm hồn cho những ai cần một chốn bình yên trong trái tim mình.
5
425604
2015-04-23 16:42:28
--------------------------
139311
10246
Mình được một người thân tặng cuốn sách này nhân dịp Giáng Sinh, và mình đã dành hẳn cả một buổi dài để đọc trọn vẹn từ đầu đến cuối. Cuốn sách đã để lại cho mình rất nhiều bài học, thông điệp và kinh nghiệm tuyệt vời để áp dụng cho cuộc sống, mà cụ thể ở đây là cách để giúp bản thân đứng lên sau mỗi vấp ngã, cách để giúp cuộc sống bước sang một trang mới mẻ và tràn ngập hạnh phúc. Những bài học trong cuốn sách có lẽ không còn mới, những quan trọng là cách dẫn dắt và đưa đẩy những quan điểm, chủ đề rất độc đáo, đem lại nhiều sự thích thú cho người đọc. Đây là một cuốn sách đẹp, theo mọi nghĩa, một cuốn sách xứng đáng có trong tủ sách của bạn.
5
419658
2014-12-06 17:33:52
--------------------------
130103
10246
"Hạt giống tâm hồn" là một cuốn sách rất hay, một trong những cuốn sách có tác động mạnh mẽ nhất với tôi. Điều đầu tiên tôi ấn tượng ở cuốn sách là bìa ngoài được trang trí khá đẹp mắt và cái tên gọi của cuốn sách khiến người đọc cảm thấy tò mò. Thậy vậy, đó là nơi nuôi dưỡng tâm hồn bạn, bạn sẽ tìm lại mình, tìm thấy bình yên thực sự trong tâm hồn,... cảm nhận tình yêu thương từ mọi người xung quanh một cách sâu sắc và biết nhìn nhận vấn đề bằng một tâm hồn thanh cao, đẹp đẽ... Hạt giống tâm hồn giúp ta tránh xa với những cán dỗ bên ngoài xa hoa, phù phiếm bằng các câu chuyện nhỏ nhưng ý nghĩa, chạm đến sâu thẳm nhận thức của độc giả.
5
414919
2014-10-15 06:21:22
--------------------------
128170
10246
"Tôi từng rời khỏi nơi mà tôi quyến luyến nhất, bỏ lại sau lưng vô vàn sự níu kéo. Tôi đã trải qua một cơn lũ khủng khiếp và suýt bị nhấn chìm trong biển dữ ngoài khơi... Tôi đã từng cô đơn, trống trải, sợ hãi và lạc lõng biết bao. Thế nhưng, tôi đã vượt tất cả những thử thách đó". Câu nói ấy là một trong những câu nói tôi thích nhất trong cuốn sách. Cuốn sách không khô khan, nhàm chán mà chủ đề lại rất mới mẻ, hấp dẫn lôi cuốn người đọc. Nội dung trong cuốn sách là một câu chuyện cổ tích kể về nàng công chúa Victoria xinh đẹp nhưng không giống các câu chuyện cổ tích khác, Victoria không sống cùng với chàng hoàng tử đến suốt đời mà nàng lại dám từ bỏ chàng, rời bỏ cha mẹ để đi theo tiếng gọi của trái tim nàng, theo con đường mà trái tim nàng màch bảo. Những bài học cuộc sống đắt giá, những điều mà không phải ai cũng nhận ra, không phải ai cũng hiểu, đây là cuốn sách dành cho những ai còn đang chịu những đau khổ trong cuộc sống, cho những ai đang bơ vơ lạc lõng không hiểu mình là ai đang sống theo ý người khác, cho những ai đang tuyệt vọng. Mỗi người khi đọc cuốn sách này chắc hẳn sẽ rút ra cho bản thân một bài học vô giá nào đó.
3
369451
2014-09-29 19:48:02
--------------------------
118019
10246
Thoạt nghe tiêu đề, có thể bạn nghĩ đó chỉ là một câu chuyện cho bé con của con, nhưng ẩn chứa trong đó là cả kho tàng tri thức mà mỗi người ít nhất nên thử khám phá một lần! Một nàng công chúc lớn lên trong sự bảo bọc, vậy thì trách sao được khi cô mong được hoàng tử giải cứu- một hoàng tử lịch lãm, tưởng như hoàn hảo. Không phải vượt qua sông rộng núi cao, mà là hành trình đi tìm hạnh phúc thật sự. Không phải câu chuyện riêng của một nàng công chúa, đó là câu chuyện của bất cứ cô gái nào trong cuộc sống hàng ngày. Sẽ không có hoàng tử nào có thể mang lại cho bạn hạnh phúc, trừ khi đó là CHÍNH BẠN!  “Bạn bị lạc. Điều đó xảy ra khi bạn đi theo bản đồ của người khác”.Tin chắc rằng đây sẽ là một cuốn sách  mà khi đã cầm đọc thì bạn không thể bỏ xuống cho tới khi hoàn thành nó
5
295534
2014-07-25 16:24:33
--------------------------
73303
10246
Tên cuốn sách-tìm lại chính mình thực sự đã lôi cuốn mình đọc cuốn này. Tuy là một câu chuyện cổ tích không có thật nhưng nó đã để lại trong mình rất nhiều suy nghĩ. Chúng ta sẽ cùng cô công chúa Victoria bước qua những trang trogn cuộc đời của cô, rồi từ những câu chuyện của cô ta lại rút ra được những bài học cuộc sống cho riêng mình. 
Mình thích cuốn này bởi vì nó không phải là những cuốn sách lí thuyết mà là một câu chuyện nhẹ nhàng nhưng lại chứa đựng những bài học mà ta có thể ứng dụng vào trong cuộc sống của chính chúng ta. Một cuốn sách khá là thú vị đó :)
4
60663
2013-05-07 15:08:17
--------------------------
53707
10246
Tìm Lại Chính Mình là một câu chuyện cổ tích kể về nàng công chúa Victoria xinh đẹp nhưng không giống các câu chuyện cổ tích khác, Victoria không sống cùng với chàng hoàng tử đến suốt đời mà nàng lại dám từ bỏ chàng, rời bỏ cha mẹ để đi theo tiếng gọi của trái tim nàng, theo con đường mà trái tim àng màch bảo. Những aài học cuộc sống đắt giá, những điều mà không phải ai cũng nhận ra, không phải ai cũng hiểu, đây là cuốn sách dành cho những ai còn đang chịu những đau khổ trong cuộc sống, cho những ai đang bơ vơ lạc lõng không hiểu mình là ai đang sống theo ý người khác, cho những ai đang tuyệt vọng, Tìm Lại Chính Mình sẽ giúp bạn tìm được hạnh hpúc, niềm hạnh phác lâu dài.
5
23122
2013-01-04 16:50:00
--------------------------
53457
10246
Đây là một trong những cuốn sách mình ấn tượng nhất trong tập "Hạt giống tâm hồn". Bởi nó không có những triết lý sáo rỗng, khô khan, khó hiểu, mà tất cả chỉ là một câu chuyện cổ tích trong sáng, giàu cảm xúc, đem người đọc đến với những miền cảm xúc tươi đẹp nhất. Cuốn sách đưa người đọc đi theo bước chân của nàng công chúa Victoria, trên một hành trình bất tận của tình yêu, hạnh phúc. Quan trọng hơn hết, hành trình ấy là tất cả những gì cô cần làm để có được một cuộc sống tươi đẹp đúng nghĩa.  Người đọc hẳn sẽ bắt gặp hình ảnh của chính mình nơi nàng công chúa đó, và tự thu lượm cho bản thân nhiều bài học bổ ích để áp dụng vào cuộc sống của mình.
4
20073
2013-01-02 15:28:06
--------------------------
43113
10246
Tên cuốn sách đã thôi thúc trí tò mò của tôi để đọc nó. Qủa thật, việc ra đi, đến những chân trời mới để tìm lại mình, nhìn lại bản thân mình là một điều không dễ thực hiện, bởi ai cũng có bản ngã của mình, đã là bản ngã thì khó lòng thay đổi. Nàng công chúa trong cuốn sách này đã quyết tâm từ bỏ chốn vương cung xa hoa để ra đi theo tiếng gọi của tâm hồn, tiếng gọi của sự đổi thay để cuối cùng, cô nhận ra những điều hạnh phúc trong cuộc đời là những điều khá bình dị xung quanh chúng ta mà đôi khi vô tình hay cố ý, chúng ta đã vô tình lãng quên, chạy theo xa hoa phù phiếm, những cái trước mắt, tạm bợ chóng qua đi.
5
57175
2012-10-18 12:16:12
--------------------------
