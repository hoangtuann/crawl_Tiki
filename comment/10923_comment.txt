421901
10923
Cuốn này chủ yếu là đưa ra những câu chuyện và tình huống thực tế để rồi đúc kết ra những bài học và kinh nghiệm từ đó. So với đắc nhân tâm cũng tựa tựa như vậy. Nhưng cuốn này lại thiên về định hướng cho bạn cách làm sao để dựa vào chính mình đứng lên lãnh đạo người khác từ việc nhỏ nhặt như rèn luyện nhân cách đến làm những công việc lớn lao... Từ gia đình bạn bè đến xã hội để rồi hiểu ra cách mở khoá chiếc hộp kì bí của bản thân :)
4
619354
2016-04-26 07:40:25
--------------------------
301280
10923
Sách về lãnh đạo và phát triển bản thân, mình cũng hay đọc để định hướng bản thân. Sách giúp chúng ta bước cùng nhân vật chính đi qua những chặng hành trình và giải quyết những vấn đề về gia đình, về công việc, tìm ra nguyên nhân giải quyết nó. Giúp chúng hiểu những bài học cho bản thân. Bìa sách chưa hấp dẫn, chỉ đơn giản là hình ảnh một bàn cờ vua. Nội dung sách rất ổn, tuy nhiên mình nghĩ NXB cần đầu tư vào bìa sách nhiều hơn nữa để thu hút người đọc.
3
101774
2015-09-14 16:06:12
--------------------------
31980
10923
Một quyển sách khá, có cách dẫn dắt tốt, mới lạ, đi từ cái nhìn của một con người, xuyên suốt qua công việc, gia đình và những câu hỏi trong thâm tâm anh ta để đi đến những vấn đề trong quyển sách, tạo ra một thông đeịp gần gũi và dễ nhớ hơn là nêu ra những cách giải thích thông thường.
Tuy nhiên, điểm chưa tốt của sách cũng nằm chính trong việc giải quyết tình huống đó. Khiến sách trở nên dài dòng, đến các chương sau dễ nhàm chán, không thích hợp cho những người muốn đọc các luận điểm chính. Việc đưa đẩy theo góc nhìn cũng hạn chế khả năng suy luận và phản biện của người đọc.
3
25713
2012-06-30 22:08:48
--------------------------
9602
10923
Đây là cuốn sách kinh điển trong mọi quyển sách viết về lãnh đạo, kinh doanh và tự hoàn thiện bản thân.
Khi đọc cuốn sách này, tôi dần hiểu ra đâu chính là nguyên nhân khiến nhiều công ty được lặp ra chưa được 5 năm đã phá sản.
Nhưng điều đáng nói là tất cả họ đều đi cùng một con đường như nhau, và chắc chắn một điều là hệ quả cũng sẽ như nhau.
Rất nhiều người sẽ phải hối tiếc rằng tại sao mình lại chưa được đọc quyển sách này trước kia. Vì vậy tôi muốn khuyên bạn đừng nên là người bỏ lỡ cơ hội.
Cho dù bạn là nhân viên hay quản lí, hay làm bất kì một ngành nghề gì đi nữa thì cuốn sách nãy cũng có thể giúp bạn đạt được nhiều thành công hơn nữa, mà quan trọng nhất là nó có thể đem đến cho bạn sự hạnh phúc, vì khi bạn không còn "Tự lừa dối" nữa, thì bạn sẽ thấy mọi chuyện hoàn toàn khác.
Chúc các bạn ngày càng thành công và hạnh phúc trong cuộc sống, và hãy chia sẽ thông điệp của cuốn sách này đến những người bạn của bạn, vì ai trên cuôc đời này cũng xứng đáng sống một cuộc sống tốt hơn hiện tại.
5
9630
2011-08-09 10:51:34
--------------------------
