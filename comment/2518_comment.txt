516505
2518
Là một lối văn theo Tự lực văn đoàn, Thạch Lam đưa đến người đọc với bối cảnh xã hội trong tác phẩm của mình là xã hội thời kì đổi mới, mang hơi hướm hiện đại trong cuộc sống vật chất cho tới tinh thần, tình cảm con người. So với những nhà văn cùng thời kì này, như Tú Mỡ, Thế Lữ còn mang nặng chất văn của Nho giáo thì Thạch Lam tách biệt hẳn. Ông dùng lối văn tinh tế, gọn và gợi được thật là rành mạch bối cảnh, không gian, chi tiết đến mức thể hiện đc cả ánh đèn dầu hiu hắt, mùi thơm của loài hoa hoàng  lan..  và cả xúc cảm và tâm hồn của nhân vật! Là nỗi buồn sâu sắc về số phận người phụ nữ và trẻ em, nếu không thuộc lớp dưới đáy thì cũng là người ở cảnh bần hàn, hoặc đang rơi vào cảnh bần hàn, như trong Cô hàng xén, Hai đứa trẻ.. Là tình cảm đôi lứa nhẹ nhàng, không phô trương mà tinh tế, dịu dàng như Dưới bóng hoàng lan ...
Thật sự là qua văn của Thạch Lam ta càng cảm thấy sâu sắc hơn về tính nhân văn giữa người với người. Rất đáng để cho lên kệ sách! ^^
5
15659
2017-01-30 01:28:32
--------------------------
461400
2518
Quyển tuyển tập Thạch Lam này mình vô cùng thích. Qua quyển sách này, mình cũng cảm nhận được phần nào con người, tính cách của Thạch Lam. Và cũng khi đọc quyển sách này, mình đã có thêm rất nhiều kiến thức mới. Nào là những dẫn chứng để mình viết văn nghị luận, nào là những cảm xúc khi mình đọc từng dòng chữ trong quyển sách. Nói chung khi ta đọc sách, tâm hồn ta được thư giãn. Không những thế, nó sẽ đọng lại trong ta những cảm xúc mà có thể ta chưa từng có.
5
1181144
2016-06-27 19:51:39
--------------------------
457153
2518
Biết đến Thạch Lam qua tác phẩm Hai đứa trẻ, ông là một người viết văn rất đặc biệt. Ông viết thiên về những câu chuyện nhẹ nhàng sâu lắng một nhà văn không bao giờ viết cốt truyện . Những mảnh đời ông kể rất bình thường giản dị trong đời sống nhưng qua đó ta có thể thấy được sự yêu thương chân thành, những ước mơ nhỏ bé đáng được trân trọng. Vì là một lối văn không có tình huống và cốt truyện nên những tác phẩm của Thạch Lam rất kén người đọc và ít người có thể hiểu hết nội dung
4
827272
2016-06-24 06:48:12
--------------------------
445823
2518
Thạch Lam - một tác giả không hề xa lạ trong giới văn chương , kể cả người lớn tuổi hay những đứa trẻ cũng có thể biết đến những trang văn, những câu truyện của ông.
Cuốn sách này , theo tôi nghĩ , nó đẹp từ bìa sách cho đến chất liệu giấy , và đặc biệt nhất là những tác phẩm tiêu biểu nổi tiếng của Thạch Lam được in trong đây - những câu chuyện , những sáng tác nhẹ nhàng mà mang đầy ý nghĩa !
Có lẽ tôi nên mua cả tuyển tập Nam Cao và Vũ Trọng Phụng!!
4
1113682
2016-06-10 22:58:02
--------------------------
429348
2518
Từ khi đi học mình đã ấn tượng với "2 đứa trẻ" của ông. Nội dung bình dị, không kịch tính, như không có cốt truyện mà vẫn cuốn hút người đọc. Lời văn của ông nhẹ nhàng châm biếm 1 cá nhân, 1 xã hội thời đó một cách từng trải, lặng lẽ quan sát và suy nghĩ về cuộc sống. Ông đồng cảm với những con người bần cùng, nghèo khổ, văn của ông như thứ vũ khí nhã nhặn lên tiếng tố cáo một thế giới tàn nhẫn thời xưa. Cùng với những mặt xấu của xã hội thì ông cũng cho người đọc cả giác hạnh phúc nhẹ nhàng đơn giản của nhân vật dù hạnh phúc đó chỉ le lói một chút thôi.
5
1349581
2016-05-13 02:37:55
--------------------------
405618
2518
Nhắc đến tác giả Thạch Lam là hình ảnh đoàn tàu, khung cảnh buổi chiều tối trong truyện "Hai đứa trẻ" lại cứ ở trong đầu lẫn quẫn, mênh mang mà làm lòng ta xao xuyến đến lạ lùng. Truyện không có chuyện là cái đặc trưng nhất của truyện Thạch Lam. Không cần phải hoa mỹ, cũng không cần phải hấp dẫn, gay cấn, truyện của Thạch Lam cho ta thấy sự tinh tế trong nội tâm của con người, đây là cái mình cảm thấy thích nhất khi đọc. Bởi nội tâm là cái cơ bản cấu thành nên văn học
5
789990
2016-03-26 22:51:01
--------------------------
402741
2518
Những tác phẩm của Thạch Lam rất thu hút tôi bởi văn phong mượt mà, giản dị, thuần túy và đậm hồn quê của ông. Có thể tất cả những tác phẩm trên câu chữ k quá hoa mỹ, cầu kì nhưng lại rất gần gũi, chất chứa những nỗi niềm của tác giả về cuộc sống khó khăn của miền Bắc trong thời kì chiến tranh. Nó lột tả được những sự thực khiến người xem k khỏi bùi ngùi, xúc động, xót thương và đồng cảm cho những số phận nghèo khổ của dân ta lúc bấy giờ. Tôi đã được tiếp xúc với Thạch Lam qua tác phẩm Hai đứa trẻ trong ngữ văn 12 từ đó cũng khiến tôi say mê và tìm đọc các tác phẩm của ông. Tôi thích nhất là tác phẩm Ngày mới nó khiến cho tôi phải suy nghĩ nhiều về giá trị của đồng tiền lúc đó, vì đồng tiền mà nhiều người phải đánh đổi rất nhiều thứ, nó khiến con người ta thay đổi quá nhiều. Một lần nữa có thể nói rằng đây là quyển sách tôi rất thích và tôi đã say mê đọc nó suốt cả buổi, những tác phẩm trong sách thực sự khiến người ta phải suy ngẫm giá trị của nó
5
195428
2016-03-22 20:47:43
--------------------------
333533
2518
Tôi thích Thạch Lam bởi một chất văn rất nhẹ nhàng, đượm buồn và cơ hồ man mác những cảm xúc, sự nhạy cảm rất mực tinh tế tựa như một cánh bướm non phá kén chui ra. Ông viết nhiều thể loại nhưng với truyện ngắn, ông đặc biệt là một cây bút tài năng của nền văn học lãng mạn đương thời. Ở Thạch Lam ta bắt gặp một trái tim tâm hồn văn chương tinh tế và nhuần nhị , không khoa trương hay phô diễn. không gấp gáp , kịch tính hay đưa đẩy vội vàng và càng không lãng mạn viển vông để phục vụ thị hiếu 
5
915482
2015-11-07 23:34:08
--------------------------
329988
2518
Tôi là sinh viên chuyên ngành sư phạm văn nên rất cần những quyển sách như thế này. Dự định sẽ mua luôn 2 quyển tuyển tập Nam Cao & Vũ Trọng Phụng luôn cho trọn bộ. Sách viết khá đầy đủ những tác phẩm của Thạch Lam. Đặc biệt tôi rất thích tìm hiểu những tác phẩm văn học trước cách mạng và Thạch Lam là nhà văn tôi ấn tượng nhất khi học hai đứa trẻ thời phổ thông. Khi nhận sách Tiki giao hàng cảm thấy rất hài lòng, rất xứng đáng với đồng tiền bỏ ra và Tiki bọc sách rất đẹp. Sẽ luôn mua nhiều sách của Tiki.
5
509425
2015-11-01 20:30:14
--------------------------
293280
2518
Được biết đến Thạch Lam ở tác phẩm hai đứa trẻ khi còn học trung học và cũng là tác phẩm có trong đề thi Đại Học vào năm mình thi cho nên có thể nói nhắc đến Thạch Lam thì hẳn nhiên những lứa chúng mình sẽ nghĩ đến ngay đến hai đứa trẻ cùng những ký ức thời học trò. 
Khi được biết Minh Long phát hành cuốn sách tuyển tập này mình đã không ngần ngại mua ngay để có thể đọc thêm nhiều hơn tác phẩm của ông. Có thể nói cuốn tuyển tập này là cuốn đầy đủ nhất từ trước đến nay của Thạch Lam. Và mình khá hài lòng, sách cầm khá nhẹ tay, mùi giấy rất thơm.
5
74132
2015-09-08 09:56:36
--------------------------
266879
2518
Thạch Lam là một trong những nhà văn Viêt Nam thu hút nhiều đọc giả nhất trong thế kỷ 20. Tôi đã từng được học văn của ông từ những năm phổ thông, từ đó, tôi thật sự thích tìm tòi các tác phẩm khác của ông. Chất văn của ông nhẹ nhàng, nhưng sâu lắng trong từng câu chữ, làm cho mỗi người đọc chúng ta như thấy được mình trong mỗi tác phẩm của ông. Thật tiếc là hiện nay không nhiều bạn trẻ tìm đến văn của ông. Tôi may mắn khi tìm được quyển sách này, khi mà nó tuyển tập khá nhiều những tác phẩm cảu ông.
5
315066
2015-08-15 00:29:50
--------------------------
