412583
5059
Tôi không phải là một người mê truyện tranh nhưng cũng không thể nào ngừng được việc dời khỏi cuốn truyện đi được. Tôi dám cá với các bạn là các bạn không thể dời cuốn truyện được. Truyện viết rất hồn nhiên nói về cuộc sống hàng ngày của cu Shin, nhân vật chính trong truyện làm cho người đọc thấy buồn cười. Tôi thực sự rất yêu thích bộ truyện này. Nhưng truyện có cái là hơi bị bựa, trẻ em đọc nó cứ bắt chước nên phải hạn chế khi cho con em đọc vào trang cu Shin tụt quần khoe mông.
5
508445
2016-04-07 23:03:56
--------------------------
395987
5059
Đọc Shin, tôi có cảm giác như mình muốn bé lại. Bé lại để có thể vô tư cười đùa, hồn nhiên nhận xét về thế giới xung quanh, vô lo, vô nghĩ. Shin hay nói những câu ngộ nghĩnh, nhiều khi còn chả biết mình đang nói gì cơ. Nhưng tựu chung, Yoshito Usui đã tạo nên một nhân vật vô cùng dễ thương, gần gũi với tuổi thơ. Tạo hình ngây ngô, đơn giản, không cầu kì trau chuốt nhưng để lại cho người đọc nhiều ấn tượng đặc biệt. Cụ thể là mỗi lần nghĩ lại hành động của cu Shin, các bạn hay nhà Nohara,... Là mình vẫn mắc cười gần chết.
5
1108222
2016-03-12 19:48:34
--------------------------
310763
5059
Shin là một cậu nhóc lúc nào cũng có hành động gây cười, mà không chỉ Shin mà cả nhà shin, bạn bè của nhóc nữa. Tập nào của bộ truyện Shin cậu bé bút chì cũng mang lại tiếng cười cho người đọc, giúp giảm căng thẳng sau một ngày làm việc mệt mỏi. Hình ảnh đơn giản nhưng cũng rất hài hước, các đoạn đối thoại có nhiều đoạn đúng là không đỡ được. Cu shin lúc nào cũng ham ăn, thích các chị xinh đẹp, có mấy ý tưởng kỳ quái, thế mà vẫn đáng yêu vô cùng.
4
112477
2015-09-19 19:36:41
--------------------------
203591
5059
Truyện rất là hay và sinh động, phù hợp với lứa tuổi của trẻ con. Cảm giác như mình đang sống lại tuổi thơ của mình, thích chơi đùa , nghịch ngợm. Cốt truyện rất hay và sống động sau mỗi tập truyện thì mình đều có thể rút ra bài học rất là hay. Khi đọc truyện này thì mình cười rất là nhiều, chợt nhận ra bấy lâu mình đã không biết cảm nhận cái hay của những quyển sách, so với lúc trước thì giờ mình ham đọc sách hơn rất nhiều từ khi đọc truyện này. So với phim hoạt hình cùng tên mình xem thì thấy nhân vật trong truyện đẹp hơn nhiều so với trên tivi.
5
603704
2015-06-01 19:05:13
--------------------------
