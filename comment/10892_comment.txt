359229
10892
Cuốn sách của mình được in năm 2009, khi lấy hàng vẫn còn rất mới, không sứt mẻ gì, cho điểm 10 Tiki.vn về việc bảo quản rất tốt.
Những thông tin trong sách dường như vẫn rất nóng, một quá trình liên tiếp, dồn dập và nghẹt thở. Có những thông tin bạn chưa hề biết, và khoảng 20% là những gì công bố ra toàn thế giới.
 Sách rất dày, chữ đặc trang và cỡ nhỏ đến không thể nhỏ hơn. Nên đọc xong, bạn chắc chắn có một hiểu biết sâu sắc, rất sâu sắc, và cái nhìn đa chiều hơn. Nhưng dường như ít người mua quyển này, có lẽ chúng ta thích cái mới, nóng, mà không thích nhìn về quá khứ. 
Mình đánh giá cuốn này hay hơn quyển "Những thời khắc quyết định" do Alphabooks dịch.
5
849910
2015-12-26 19:25:15
--------------------------
293488
10892
Khi mình nhận được sách, ở trang cuối bị rách luôn một mảng, thật sự thấy rất tiếc cho một cuốn sách đẹp. Tuy nhiên cuốn sách này là một tài liệu đáng tham khảo nếu bạn muốn quan tâm về cách hành xử của nước Mĩ nói chung cũng như Tổng thống Bush nói riêng trên con đường chống khủng bố để từ đó ta thấy rõ quan điểm của vị tổng thống này trong cách vận hành siêu cường quốc. Cuốn sách là một câu chuyện chính trị mà nó vẽ ra cả về diễn biến, cách thức cũng như nội dung hành động từ cả hai phía: khủng bố và chống khùng bố.
4
600968
2015-09-08 13:21:23
--------------------------
265722
10892
Mình là một bạn đọc rất thích quan tâm và tìm hiểu về đất nước nổi tiếng này. Có thể nói trong các nhiệm kỳ thì ngài tổng thống George Walker Bush này được quan tâm nhiều nhất vì với nhiều sự kiện nổi bật mà cả thế giới luôn theo dõi trong  suốt thời gian nhiệm kỳ của  ông. Thông qua quyển sách này giúp cho mình càng hiểu thêm về con người cũng như tính cách riêng của ông trong cách đối phó, ứng xử với nhiều tình huống khác nhau mà tầm ảnh hưởng đến cả một quốc gia và thế hệ mai sau
5
618799
2015-08-14 09:20:06
--------------------------
249125
10892
Sách không chỉ có thông tin mà còn có cả hình ảnh thể hiện lại một cách khá đầy đủ tình hình chính trị nước Mỹ dưới thời tổng thống Bush, những quyết định quan trọng, những chính sách được Bush và những người đứng đầu nước Mỹ đưa ra..., đặc biệt là có khá nhiều thông tin liên quan đến vấn đề chống khủng bố. Đây là một cuốn sách chính trị nên không thể tránh khỏi đôi chỗ nhàm chán, tuy nhiên, nhìn một cách tổng quát, đây vẫn là một cuốn sách thú vị và có giá trị đối với những ai quan tâm đến chính trị nước Mỹ.

4
138880
2015-07-31 14:06:45
--------------------------
233640
10892
Mình đã mua cuốn sách này khoảng vài tuần trước,và cảm giác khi lấy sách raa là cảm thấy hơi thất vọng vì cuốn sách không phải bìa gấp như mình nghĩ,bìa khá dễ rách nhưng nội dung thì đúng thật là rất tuyệt vời, nó nói về Bush trong hành trình chống lại trùm khủng bố Osama bin Laden như thế nào,cách xử lý tình huống của ông ra sao trước những tình huống mà có thể gọi là không thể có cách nào giải quyết...Đối với những bạn nào đang định tìm hiểu về chính trị Mỹ thì đây là một cuốn sách hay,rất đáng đọc!
4
698788
2015-07-19 20:16:50
--------------------------
227470
10892
Bắt đầu từ ngày 11/9/2001, quyển sách điếm tới từng ngày, dựng lại bối cảnh hoang tàn ra sao, rồi những hối hả, những cuộc đối thoại căng thẳng của các quan chức.
Cũng như nhiều cuốn sách viết về đề tài chính trị, mỗi quyển phải có cái “tin mật” của mình. Đó là những thông tin mang nhãn mác “chỉ có duy nhất tại đây”. Quyển sách cũng có những thông tin như vậy. 
Nhưng cách viết của tác giả thì như là một quyển tiểu thuyết. Tôi đôi lúc lại cứ nghĩ như độc một quyển truyện nào đấy. Đọc tiểu thuyết thì có thể cuốn theo cách hành văn của tác giả, nhưng đọc một quyển sách về chính trị cái cần ở tác giả thì không phải như vậy. Tác giả lại cố xây dựng hình tượng các nhân vật, theo kiểu khuôn mẩu, kiểu như nghĩ đến Bush phải là thế này, thế này.. Và vì như vậy, cuốn sách đôi khi chất kịch chứ không giống là phơi bày lại. Có thể tác giả không viết sai, nhưng lại lượt bỏ cái mà tác giả thấy không hay, gọt dũa lại.
Nói chung là tôi cũng tìm được thông tin, những điều mới mẻ thông qua quyển sách. Nhưng cách viết của tác giả thì không ưng bụng.

1
385588
2015-07-14 11:16:27
--------------------------
159647
10892
Có thể nói trong lịch sử lập quốc của Mỹ, ít có tổng thống nào khiến cho nước Mỹ trở thành một đất nước khiến nhiều người vừa cảm thấy sợ vừa cảm thấy tin tưởng như tổng thống George Walker Bush. Ông là người thuộc đảng Cộng Hòa, do đó đường lối của ông mang hơi hướng cứng rắn và dứt khoát theo đúng kiểu của đảng Cộng Hòa. Dưới nhiệm kỳ của ông, nước Mỹ đã tham gia vào những cuộc chiến xa xỉ và tốn kém nhất, lần đầu tiên từ sau chiến tranh Việt Nam, và cũng là cuộc chiến tranh đổ máu nhiều nhất. Nhưng cũng chính ông đã nhận ra nguy cơ khủng bố, và cuộc chiến với khủng bố mà ông khởi xướng đã và sẽ tiếp tục bởi những người kế nhiệm ông, cùng với các đồng minh góp phần đánh bại chủ nghĩa khủng bố, bảo đảm an ninh thế giới. Một con người tài năng nhưng cũng có cá tính ương ngạnh không kém.
5
291067
2015-02-18 08:38:11
--------------------------
