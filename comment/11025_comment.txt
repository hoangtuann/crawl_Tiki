456436
11025
Cuốn sách “Tủ sách Sức khỏe gia đình – Cây hoa trị bệnh” cung cấp những thông tin chữa bệnh của các loài hoa quen thuộc thường gặp hàng ngày (hoa hồng, hoa cúc, hoa hòe, hoa sứ; hoa thiên lý; hoa atiso…)
Hình thức trình bày: có hình ảnh minh họa, thông tin được trình bày mạch lạc; sách được in trên giấy trắng nhẹ - dễ nhìn
Điểm trừ của cuốn sách là: chưa có phần tra cứu từ khóa và bảng chú giải thuật ngữ (index). Hy vọng trong lần xuất bản tới cuốn sách sẽ khắc phục được điều này
4
301718
2016-06-23 14:19:45
--------------------------
407695
11025
Nhìn sơ qua cứ tưởng là cuốn sổ tay nhỏ với giấy không tốt nhưng nhận hàng rồi mới vỡ lẽ giấy trắng tốt, bìa dày, cuốn sách dễ thương, hơn nữa hướng dẫn khá chi tiết cho từng loại hoa giúp giải đáp những thắc mắc của mình. Những người có đam mê thực vật cây cối như mình cũng có thêm nhiều kiến thức thường thức, sách trình bày cũng khá mạch lạc, dễ hiểu, dễ đọc. Mình chỉ cảm thấy không thích một điều  phải chăng là sưu tầm nên giá trị của sách cũng không cao.

5
198496
2016-03-30 16:10:36
--------------------------
371157
11025
Cuốn có nhiều lợi ích tìm ra loài hoa sống thực vật nhưng vô hại làm mất đi ánh sáng bao trùm lên thế giới cảnh quan của loài vật có ích , an toàn và sống đơn giản không quá cầu toàn làm cho người xem thấy thú vị , khỏe khoắn hơn sau khi học được bài thuốc chữa trị từ thiên nhiên , nguyên liệu có sẵn làm nên cuộc đời may mắn với lại đạt được yếu tố rẻ , dùng nhiều lần có thể thiết thực trong đa dạng hóa các sản phẩm chế biến ,
4
402468
2016-01-19 03:39:44
--------------------------
306449
11025
Đây là cuốn sách dành cho những ai thích tìm tòi những bài thuốc mới lạ từ thiên nhiên! Chỉ là những loài hoa đơn giản mà ta hay thấy ở mọi nơi nhưng nó có thể chế ra thành nhiều loại thuốc, dược liệu tốt cho làn da, ngoài ra nó còn có những bài thuốc món ăn làm đẹp cho cơ thể! Qủa thật đây là một cuốn sách mình thấy rất có ích cho mọi người. nếu bạn là một tín đồ về các loại mỹ phẩm thiên nhiên thì không thể bỏ lỡ cuốn sách này!
4
143101
2015-09-17 16:31:45
--------------------------
