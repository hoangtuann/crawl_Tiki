344306
10863
Qua tác phẩm thầy đã cho tôi cảm nhận sâu sắc và lĩnh hội được những bài học vô cùng quý giá. Trong cuộc sống, trong công việc  không phải lúc nào cũng màu hồng, nếu mình chỉ biết nhận mà không biết cho, chỉ biết trách người mà không nhìn lại bản thân, chỉ biết chạy theo danh vọng mà bất chấp tất cả thì cuối cùng cuộc sống của mình cũng sẽ không có ý nghĩa gì. Tu trong công việc dạy ta không những có trách nhiệm với công việc mà còn biết cách đối nhân xử thế với mọi người, biết làm cho mình tự tin, lạc quan hơn.
5
354887
2015-11-28 09:02:50
--------------------------
331152
10863
Mình mua cuốn Tu Trong Công Việc của Tác giả Hòa thượng Thích Thánh Nghiêm và đây lả đôi lời nhận xét, mình rất thích những quyển sách trong bộ sách phật pháp ứng dụng, thực ra quyển này mình đã thấy trên mạng nhưng mình thích đọc giấy hơn để bất cứ lúc nào cũng có thể mang ra đọc, nhưng nơi yên tĩnh tắt điện thoại và tĩnh tâm đọc sách quả thật rất thú vị... Sách của  Hòa thượng Thích Thánh Nghiêm luôn thấy đơn giản nhẹ nhàng, sâu lắng, đọc vào ngẫm nghĩ như trút được nhiều gánh nặng... Tu trong công việc sẽ thấy công việc nhẹ nhàng hơn...
4
730322
2015-11-04 00:20:38
--------------------------
302370
10863
Mình rất thích cách nói của thầy, vì cách thầy nói như đang ngồi đàm đạo với mình, uống một ly trà trong bàn khách nhà chùa vậy. Giọng điệu rất gần gũi, tự nhiên, có phần gần với văn nói hơn. Vì thể mà dễ tiếp thu, dễ thấm sâu ý tứ của thầy.
Mình đã có những cải thiện tốt trong thái độ và năng suất làm việc. Từ cách đón nhận những khó khăn, quan hệ với đồng nghiệp và cách để  an vui tự tại.
Tu trong công việc đúng là tiêu đề hoàn hảo cho cuốn sách này.
Cảm ơn thầy Thích Thánh Nghiêm nhiều lắm !
5
470823
2015-09-15 10:24:51
--------------------------
51624
10863
Lúc đầu, mình nghĩ đây sẽ là một tác phẩm khó nhằn và cần nhiều hiểu biết thông tuệ mới có thể cảm nhận được. Nhưng khi đọc, mình biết là mình đã nhầm. Đây là một cuốn sách tuyệt vời, một cẩm nang sống mà ai cũng nên có, được viết ra một cách nhẹ nhàng và giàu cảm xúc, không quá khó đọc mà đi vào lòng người như một khúc tình ca. Cuốn sách có nhiều trang viết sâu sắc và chi tiết, cho ta thấy được nhiều góc độ khác nhau của tâm linh và tín ngưỡng, cũng như sự liên hệ của Phật Giáo với đời sống, công việc mỗi người. Cuốn sách sẽ đem đến những khoảng lặng bình yên cho người đọc, đồng thời để lại nhiều chiêm nghiệm và bài học sâu sắc để áp dụng trong cuộc sống.
4
20073
2012-12-21 23:22:07
--------------------------
