380279
5655
Tập này hay lắm, nhất là những vụ án xảy ra xung quanh cây đàn vĩ cầm nổi tiếng. Lúc đầu mình cảm giác hơi sợ khi đọc đến đây và mình cũng rất bất ngờ khi biết được thủ phạm gây ra tất cả mọi chuyện. Thật tiếc cho anh chàng vì anh ấy không phải là người xấu, tuy nhiên, anh ấy cũng trả lời thắc mắc của Connan về một bài hát, hẳn ai chưa đọc cuốn này sẽ không tưởng tượng ra khuôn mặt của Connan khi đó, khuôn mặt lo lắng pha lẫn hoảng sợ. Nội dung quá hay, ngắn gọn nhưng tiết lộ nhiều điều.
5
13723
2016-02-14 14:38:20
--------------------------
379080
5655
mình thích thủ phạm trong vụ án giết người theo thứ tự các nốt nhạc, dù sao anh ta cũng là người có tài, chỉ do thù hận cho cha mình mà sinh lòng sát nhân, rất may còn cô cháu gái để kế tục sự nghiệp, thêm cả chi tiết bài nhạc "con quạ" mà Thám tử Conan tìm ra được từ các phím nhạc của điện thoại di động mà bọn áo đen sử dụng... Kid xuất hiện  cùng tranh tài với Thám tử Conan cũng khá hay, dù không có ông bác già Jurikichi nhà Suzuki như mọi khi , càng đọc càng không dứt ra được Shinichi quả là tài ghê...
4
568747
2016-02-09 09:55:38
--------------------------
378028
5655
Vụ án giết người theo nốt nhạc được hung thủ bùng lên theo những ngọn lửa, và thật không ngờ hung thủ lại lại người có đôi tai cảm nhận âm nhạc tuyệt vời, nhờ vụ án mà Conan-Shinichi đã có email của tổ chức áo đen. Nhưng tiếc thật đấy, chỉ vì có quá khứ tối tăm, hung thủ của vụ án đã làm những hành động dại dột, cũng may là Conan đã nhờ đội cảnh sát, nếu không thôi thì nước nhât đã mất đi một thiên tài âm nhạc rồi.
Kid đối đầu với Conan-Shinichi, đây thực sự mới là một cuộc chiến đầy lôi cuốn, thật sự vì Kaito Kid ùng có những tập phim khác làm cho anh, nên khi hai người chạm trán, sự hồi hộp của tôi tăng lên không ngừng!
5
97553
2016-02-04 06:53:26
--------------------------
377132
5655
Vụ án các khúc nhạc với cây đàn nổi tiếng hay nhưng dễ đoán ra thủ phạm. Cả tập này chỉ có nó là đáng chú ý còn mấy vụ còn lại đọc cho vui. 
Càng về sau Conan càng hết ý tưởng hay sao mà các cách giết người không còn độc đáo nữa. 
Tuy nhiên đọc để bổ sung kiến thức các lĩnh vực cũng được. 
Có tin đồn Agasa là trùm xã hội đen. Nếu đúng vậy để xem tác giả giải thích sao để hợp lý đây ! Cứ chờ xem. Mình nghi sẽ không có tập cuối cùng. 
5
535536
2016-02-01 18:18:42
--------------------------
371512
5655
Tại tập này xuất bản cũng đã rất lâu rồi nên lâu rồi mình chưa đọc lại nhưng lúc đọc lại rồi thì lại cảm thấy càng hay hơn vì phần nào đã hiểu rõ hơn được những suy luận trong vụ án này. Và cũng rất may nhờ chí thông minh của conan mà đã cứu được anh chàng thiên tài âm nhạc đó không thì chắc anh ấy đã tự tử. Và còn một điều nữa tập này mình thấy thú vị nhất vẫn là sự xuất hiện của siêu trộm Kid. Vì là fan của Kid lên cứ vụ án nào có Kid là y như rằng độ phấn khích lại tăng lên rất nhiều.
5
1062396
2016-01-19 18:40:27
--------------------------
363664
5655
Tiki giao hàng tuyệt lắm!Cuốn truyện rất mới như vừa xuất bản vậy. Đây là lần giao hàng mình rất thích vì cuốn truyện không hề bị nhăn. Không những thế, cuốn truyện này còn hết sức lôi cuốn. Địa chỉ của ông trùm áo đen được tiết lộ và con đường lôi tổ chức ra ánh sáng ngày càng gần. Cuốn truyện làm cho mình rất hồi hộp và ngưỡng mộ khả năng suy luận tài tình của Conan trước một thế lực đáng sợ giết người không ghê tay. Conan đang dần đi sâu vào cuộc sống tinh thần của mình.
5
362041
2016-01-04 19:28:15
--------------------------
337310
5655
Câu chuyện về gia đình âm nhạc rất hay . Mình rất thích Haga , anh ấy là một thiên tài âm nhạc , chỉ vì những sai lầm của quá khứ mà khiến anh ấy hủy hoại tương lại của mình . Mình thấy rất không đáng , rất tiếc cho anh ấy . Giá như anh ấy có thể bình tĩnh hơn .
Conan cũng đã tìm ra được địa chỉ mail của boss . Nhưng mọi chuyện lại đi vào ngõ cụt vì không thể tìm ra boss . " Quạ ơi quạ à , sao mày lại khóc ... " tổ chức áo đen bí ẩn rốt cuộc là một tổ chức như thế nào ? Tại sao ông trùm lại tạo một mail mang giai điệu một bài hát buồn như vậy ?
Vụ truy tìm kho báu lần này cũng có siêu trộm Kid góp vui . " Vật không thể chinh phục " , ông kichiemon đúng là tài giỏi thật .
Bí ẩn chiếc điện thoại ? Kịch tính quá !!!
5
558702
2015-11-13 21:47:39
--------------------------
298474
5655
Thám tử lừng danh conan 46 hay với rất nhiều tình tiết thú vị , vụ án giết người hàng loạt theo thứ tự các nốt nhạc theo tiếng Đức rất hay và đã làm cho Conan (Shin) đã phải tốm nhiều công để tìm hiểu và nhờ những kí ức trước với mẹ và tài chí của mình Conan đã phá giải được vụ án này , Conan cũng đoán trước được ý định tự sát của hung thủ nên kịp thời ngăn cản.Cũng bởi vụ án này mà Conan đã tìm ra được địa chỉ email của ông trùm tổ chức áo đen...và rồi mọi chúng sẽ ra sao đây...đọc mà không dứt được luôn ^^
5
416933
2015-09-12 19:02:28
--------------------------
291385
5655
Trong tập 46 này, phần mình thấy hấp dẫn nhất chính là sự xuất hiện của siêu trộm Kid. Vụ án nào có Kid là y như rằng độ phấn khích lại tăng lên. Hơn nữa lần này bên cạnh Conan còn có sự giúp sức của nhóm thám tử nhí nữa, 2 khắc tinh gặp nhau tạo nên một câu chuyện thật ly kỳ. Bên cạnh đó cũng trong tập này Conan đã tìm được lời giả cho câu đố mà Vermouth để lại, không biết từ đây Conan có thể tìm ra manh mối của bọn áo đen không. Đọc hết tập này là lại muốn đọc tập kế tiếp ngay!
4
471112
2015-09-06 11:11:30
--------------------------
251335
5655
Bản chất của conan là lôi cuốn người đọc , vụ này rất hay và hấp dẫn , cái chết tưởng chừng như là tai nạn kia lại do con người sắp đặt, và mình thích cái cách conan giữ lại mạng sống cho hung thủ , ngọn lửa thù hận có thể giết chết người khác lẫn mình ,nên sự vị tha lúc nào cũng là cần thiết. Rất ý nghĩa và mang 1 bài học sâu sắc . Conan lúc nào cũng mang đến cho người đọc trẻ sự hấp dẫn, cuốn hút, và ở cuốn này cũng không ngoại lệ.
4
634444
2015-08-02 17:52:22
--------------------------
178715
5655
lâu lâu không đọc bây giờ đọc lại vẫn thấy tập 46 thật là hay! khi mình nhìn bìa sách mình cứ nghĩ sẽ là một vụ án trong phòng hòa nhạc hóa ra không phải :) vụ án giết người hàng loạt theo thứ tự tên nốt nhạc.mình thấy thương anh thiên tài âm nhạc ấy mà.cũng may conan lường trước được sự việc nếu không anh ấy tự tử mất rồi.vụ án đấu trí giữa conan với kid cũng hay.không liên quan đến cảnh sát và ông mori như mọi khi,đây dường như chỉ là cuộc song mã giữa thám tử và siêu trộm.hấp dẫn và tài tình!
5
586877
2015-04-05 14:47:24
--------------------------
