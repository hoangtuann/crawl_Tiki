473256
4206
Mình đã biết đên Lý thuyết trò chơi từ khi học chương trình MBA, và cũng tìm đọc nhiều sách viết về chủ đề này nhưng đây mới là cuốn sách mình đang tìm kiếm. Sách viết rất chi tiết từ cái nền tảng đến những cái chuyên sâu, tuy nhiên nếu không phải là dân kinh tế thì bạn khó lòng mà hấp thụ hết nội dung cuốn sách. Bên cạnh, phần lý thuyết tác giả còn đưa ra những ví dụ thực tế rất sống động, giúp bạn đọc có thể tiếp cận dễ dang hơn với các lý thuyết trong sách.
5
14119
2016-07-10 13:28:06
--------------------------
458938
4206
Có lẽ nhưng ai học chuyên ngành tài chính ít nhất một lần đã từng nghe tới cái tên "Lý thuyết trò chơi". Giống như thuyết "bàn tay vô hình" của Adam Smith, lý thuyết trò chơi cũng đóng vai trò quan trọng, phổ biến trong thế giới kinh doanh nói chung và lĩnh vực tài chính nói riêng. Để hiểu biết một cách có bài bản và hệ thống thì có lẽ cuốn sách này là cuốn sách tốt nhất giúp bạn đạt được điều này. Bố cục sách trình bày rõ ràng từ cơ bản đến nâng cao, đơn giản đến phức tạp. Giấy sách tốt, bìa đẹp, chắc chắn. Một cuốn sách rất đáng mua.
3
475854
2016-06-25 16:59:35
--------------------------
443634
4206
Sách bổ ích với những người theo học ngành kinh tế muốn tham khảo thêm các kiến thức chung về kinh doanh, vì cách trình bày (dịch) còn hơi mô phạm và chung chung, thiên về lý thuyết (nhưng cũng hợp lý thôi vì tên cuốn sách là "lý thuyết" mà). 

Đối với những người đang tìm kiếm các câu chuyện cụ thể mang tính kinh nghiệm trong kinh doanh nhằm đáp ứng nhu cầu thực tiễn thì cuốn sách này chưa đáp ứng được. Nói chung, sách mang tính tham khảo, kiến thức chung nhiều hơn (thích hoe75p cho sinh viên)
4
1112105
2016-06-07 09:55:47
--------------------------
406642
4206
Ngoại hình: sách nặng, giấy thường, hơi khó để nằm đọc
Nội dung:
- Đánh giá chung: đây là một quyển sách thực sự rất tuyệt vời, nếu bạn là dân kinh tế thì sẽ là thiếu sót nếu bạn không tìm hiểu về Lý thuyết trò chơi.
- Quan niệm "thương trường là chiến trường" đã có từ lâu đời, nó vẫn đúng cho đến hiện tại ở một vài trường hợp. Tuy nhiên, để tối đa hóa lợi nhuận của công ty, đàn áp hoặc không hợp tác với các công ty khác quả thực không phải là cách tốt nhất. Cuốn sách sẽ cho bạn biết về sức mạnh của từ "Tranh hợp" và câu nói: "kinh doanh là sự hợp tác khi tạo ra chiếc bánh, nhưng sẽ là cạnh tranh khi chia phần chiếc bánh đó" sẽ phải là chân lí nằm lòng của những người bán hàng ngày nay
- Theo tôi đọc một vài thể loại sách khác, họ viết rất nhàm chán và toàn lí thuyết suông, ví dụ của họ cũng chỉ là số liệu ảo không ai kiểm chứng. Thế nhưng cuốn sách này, sẽ cho ta biết những ví dụ hoàn toàn thực tế của lịch sử nhân loại. Bên cạch đưa ra ví dụ, hai tác giả đồng thời cho thấy những phân tích kinh tế rất thực tế và dễ hiểu (điều mà nhiều tác giả không làm được do không có kiến thức chuyên sâu). 
- Những câu chuyện về những gã khổng lồ công nghệ, bạn cũng thấy là IBM là một tập đoàn máy tính lâu đời, nhưng tại sao chúng ta lại thấy nhan nhản trên thị trường nào Dell nào Nasus... vì sai lầm nào mà họ lại bị thụt lùi như vậy
- Ngày trước ở Nhật Bản, tập đoàn Nintendo đã thống trị ngành trò chơi điện tử, thống trị đến mức trong suốt nhiều thập kỉ, đã không có một nhà sản xuất nào có thể chen chân trong thị trường đó. Tuy nhiên, SAGA đã lật đổ đế chế Nintendo một cách rất ngoạn mục. Câu chuyện đó diễn ra như thế nào? Bí quyết của hai ông lớn này là gì?
Quả thực, có rất nhiều những thứ thú vị trong cuốn sách này. Là dân kinh tế thì đây là một tác phẩm không thể bỏ qua được. Hãy sắm ngay cho mình một cuốn để cùng thưởng thức nhé
Cảm ơn!
5
1178510
2016-03-28 17:55:17
--------------------------
404984
4206
Ban đầu nghe chữ "Lý thuyết" mình cũng thấy hơi ngại, nhưng đọc là bị cuốn hút ngay. Với nhiều ý tưởng, ví dụ và phân tích, tác giả đã cho chúng ta thấy kinh doanh, cũng như trong cuộc sống không phải lúc nào cũng là đối đầu, hay cạnh tranh. Mình thích nhất ý tưởng là "làm to cái bánh ra" bằng "giá trị gia tăng" của mỗi người hay mỗi công ty, chứ không phải lúc nào cũng cứ chăm chăm giành nhau miếng bánh nhỏ. Bằng cái nhìn đa chiều, phân tích sâu, tác giả cũng cho ta thấy mối quan hệ của ta với đối tác, hay nhà cung cấp đôi khi lại mang sự bất lợi cho ta. Một quyển sách rất bổ ích cho các nhà quản lý, các CEO, các GĐ kinh doanh hoặc những bạn đang khởi nghiệp.
4
916249
2016-03-25 22:15:29
--------------------------
360625
4206
Mình nghĩ kinh doanh đúng là không đơn giản. Sau khi đọc sách này, mình thấy hóa ra kinh cũng chỉ như một trò chơi thôi. Trò chơi về việc những người chơi tạo ra chiếc bánh và chia bánh. Liệu có cách nào làm chiếc bánh lớn hơn? Ai được chia nhiều, chia ít? Nội dung sách viết khách quan. Vì coi kinh doanh như là trò chơi để áp dụng các lý thuyết. Nên không có xen lẫn các yếu cảm xúc, thiên vị. Sau nhiều phần, sách có những phần ghi nhớ rất dễ đọc lướt qua và nắm được toàn bộ ý chính. Mình vẫn luôn thích tìm các cuốn sách phân tích về chiến lược kinh doanh. Ngoài cuốn này, mình còn mua thêm cả cuốn Tư duy chiến lược của cùng tác giả.
5
734210
2015-12-29 15:17:30
--------------------------
332988
4206
Dù mua đã lâu nhưng giờ mới có thời gian để đọc.Đây thực sự là một cuốn sách hay và có thực dụng cao.
Qua phần một có thể sơ lược qua 5 yếu tố cơ bản của kinh doanh: đối thủ cạnh tranh trực tiếp, nhà cung cấp, khách hàng,.....Đây là những yếu tố cơ bản để có thể  tạo nên những chiến lược kinh doanh trong từ cấp cơ sở cho đến toàn bộ công ty, tập đoàn.
Đây thực sự là một cuốn sách hay và bổ ích. Tuy cũng chỉ hiểu phần nào của cuốn sách, nhưng hy vọng đây sẽ là những kiến thức nền tảng làm tăng cường khả năng tư duy trong lĩnh vực kinh doanh.
5
441962
2015-11-07 11:34:16
--------------------------
321261
4206
Lý thuyết trò chơi, một đề tài nghiên cứu nổi tiếng về kinh tế học đã vinh dự nhận được giải Nobel kinh tế. Quyển sách Lý Thuyết Trò Chơi Trong Kinh Doanh của Tác giả Adam M. Brandenburger - Barry J. Nalebuff mở ra 1 góc nhìn mới trong lĩnh vực kinh doanh, câu hỏi trên thương trường có phải lợi ích của người này sẽ là bất lợi của người khác hay cả hai sẽ tự ghìm chân nhau tạo nên thế cân bằng vốn có của thế giới này? tất cả sẽ được trả lời sinh động trong sách.
5
144957
2015-10-13 16:02:02
--------------------------
296197
4206
“Lý thuyết trò chơi trong kinh doanh CO-OPETITION” của Adam M. Brandenburger - Barry J. Nalebuff là một quyển sách rất hay về kinh doanh, nó mang lại cho mình rất nhiều kiến thức mới mẻ, cho chúng ta cái nhìn mới hơn về kinh doanh. Sách chia bố cục rất rõ ràng và cụ thể, từ cạnh tranh và hợp tác; lý thuyết trò chơi trong kinh doanh; các yếu tố chiến lược gồm: người chơi, giá trị gia tang, các quy tắc, chiến lược, phạm vi,…. Mình cảm thấy đây là quyển sách rất hay, tác giả hướng dẫn ra nhiều kiến thức bổ ích mà các doanh cần biết.
5
554150
2015-09-10 22:21:02
--------------------------
291995
4206
Được giới thiệu cuốn sách này, mình thấy nó rất thực tế. Nhìn chung đây là cuốn sách bổ ích cho những bạn yêu thích nghiên cứu kinh tế. Về chất lượng sách được bao bìa tốt, trang trí đẹp. Trang giấy màu vàng đọc lâu cũng không sao cả.
Lý thuyết trong sách do  nhà toán hoc Jonash chứng minh. 
Kiến thức trong sách khá đầy đủ, và tổng quan. ó thể nói rất nhiều cuốn sách viết về lý thuyết trò chơi, nhưng để tìm một cuốn sách thực dụng nhất trong đó thì có lẽ đây là lựa chọn tốt nhất. 
5
525736
2015-09-06 22:15:50
--------------------------
266388
4206
Quyển sách “ Lý thuyết trò chơi trong kinh doanh ” này khá hay. Các bạn học kinh tế nên đọc sách vì có một số qui luật được áp dụng khá rộng rãi trong nhiều lĩnh vực phục vụ cho công việc sau này của mình. Nhìn chung sách rất đáng đồng tiền và đáng đọc. Về chất lượng sách được bao bìa tốt, trang trí đẹp.  Trang giấy màu vàng đọc lâu cũng không sao cả. Tuy nhiên cách trình bày có vẻ hơi không đẹp mắt, logic. Nhiều chữ quá trong một trang và lề lại rất sát. Có cảm giác ngộp khi đọc sách.
4
656164
2015-08-14 16:56:58
--------------------------
255086
4206
Học kinh tế nên có đôi lần được nghe giảng về lý thuyết trò chơi. Nhưng thời gian trên trường có hạn nên thầy cô không giảng nhiều lắm, điều này càng khiến mình tò mò. Trong một lần đi nhà sách, vô tình nhìn thấy cuốn sách này, mình mở ra đọc và bị thu hút ngay! Mặc dù là sách kinh tế nhưng lối viết không hề khô khan, nặng tính lý thuyết mà lại khiến người đọc tò mò và thích thú. Thật sự rất vui vì mình đã quýêt định mua nó và còn đang chuẩn bị mua luôn cuốn "Tư duy chiến lược" đây!
5
508249
2015-08-05 19:12:17
--------------------------
204894
4206
Mình biết đến lý thuyết trò chơi cũng khá lâu rồi - từ bộ phim A beautiful mind. Từ đó cũng tham khảo về một số sách về lĩnh vực này. Rồi sau đó quên bẽng đi, vừa qua có một sự kiện rất đáng tiếc là John Nash bất ngờ ra đi. Một lần nữa nhắc mình đến lĩnh vực này. Tìm đến cuốn sách như một sự tình cờ, và sự tình cờ đó đã không làm cho mình thất vọng. Có thể nói rất nhiều cuốn sách viết về lý thuyết trò chơi, nhưng để tìm một cuốn sách thực dụng nhất trong đó thì có lẽ đây là lựa chọn tốt nhất. Tác giả đi từ cái đơn giản đến phức tạp, lần lượt từ các chủ đề nhỏ đến lớn. Đọc sách không chỉ đọc những cái được viết trong đó mà phải tư duy thêm những vấn đề thực tế của chính bản thân, đó chính là cái hay của quyển sách. Một cuốn sách - lĩnh vực, rất đáng để nghiên cứu. 
Nếu là dân kinh tế sẽ biết đến bàn tay vô hình, thì lý thuyết trò chơi là cái giải đáp thắc mắc những thứ nằm ngoài quy luật bàn tay vô hình đó.
5
74132
2015-06-05 10:37:35
--------------------------
191529
4206
Mình học chuyên ngành kinh tế nên đặc biệt có hứng thú với những quyển sách phù hợp chuyên ngành. Trong quá trình học mình nhiều lần được nghe về khái niệm Lý thuyết trò chơi nhưng vẫn không rõ lắm về nội dung của Lý thuyết này. Mình chỉ biết nó rất hữu ích đối với những ai thích tìm hiểu về kinh tế học. Do đó vừa thấy quyển sách này mình đã vô cùng tò mò và muốn sở hữu ngay. Đối với mình, đây là một quyển sách rất đáng để mua. Giá cả quyển sách cũng khá ổn, hợp túi tiền sinh viên.
4
63487
2015-05-01 22:41:17
--------------------------
