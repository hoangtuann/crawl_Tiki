416064
8444
Toàn bộ tác phẩm gồm 5 chương, mới đọc bạn sẽ tưởng mỗi chương là một truyện ngắn riêng rẽ, nhưng thực ra chúng đều có liên quan, đều có chung một số nhân vật chính. Tuy nhiên lượng nhân vật chính này khá đông so với 1 cuốn truyện dài (5-7 nhân vật), đồng thời mỗi hồi lại là lời kể của một nhân vật, nhưng Miên Miên không chú thích cho ta biết đó là lời của nhân vật nào, mà để ta tự đoán biết qua các sự kiện nhân vật đó tham dự cũng như những còn người mà nhân vật đó gặp gỡ. Có lẽ phải đọc vài lần, người đọc mới xác định đúng được.
Người dịch mắc khá nhiều lỗi cơ bản, không dịch được thoát ý tác phẩm, có lẽ vì vậy mà nhiều bạn đọc thấy tác phẩm rời rạc, khó đọc. (VD: dịch heavy metal - tên một dòng nhạc rock, thành "kim loại nặng" ?!).
Cá nhân mình thích chương cuối cùng "Đêm của anh, ngày của em", câu chuyện rất chân thực và cảm động, về những cô gái Thượng Hải với mong ước một cuộc sống đầy đủ, một tình yêu đích thực, nhưng đã bị hiện thực xã hội tàn khốc huỷ hoại.
2
445579
2016-04-14 13:32:47
--------------------------
365265
8444
Mặc dù không thật xuất sắc nhưng đây lại là cuốn sách vượt quá kì vọng của mình. Mình mua cuốn sách này với lý do giá vừa đủ re để tròn đơn hàng được khuyến mại. 
Nghĩ là mua thôi chứ không hứng thú mấy. Nhưng khi đọc cuốn sách mình đã thấy mình sai. Nó quá là có giá trị so với giá thành của nó. 
Miên Miên - với lỗi viết chân thực, từng trải - đã mang tới cho người đọc cảm nhận một cách rõ nhật về thực trạng cuộc sống của thanh niên Thượng Hải về đêm. Rất chân thực. 
Chắc chắn sẽ còn mua thêm những cuốn sách của tác giả này :)
4
585214
2016-01-07 18:46:02
--------------------------
360917
8444
Truyện là văn hóa của những người trẻ như tại thành phố Thượng hải đầy sôi động , tấp nập lúc về đêm với thú vui ăn chơi sôi sục của giới cần mộ điệu , với sức lan tỏa rộng của sự hào nhoáng vật chất mà các cô gái thay đổi cách nghĩ bước chân lầm lạc làm mất đi giá trị thực tại của lứa tuổi , mất khả năng sống với cách nhìn thiếu đúng đắn , lộ rõ mảng tối u ám lẩn khuất phía sau căn phòng đóng chặt không những vậy thay đổi nhìn nhận về lối sống hiện nay ngày càng phóng khoáng .
3
402468
2015-12-30 02:20:03
--------------------------
360318
8444
Tiki giảm giá cuốn này và thấy nhận xét đánh giá cũng ổn nên mình mua đọc thử, nhưng có hơi chút thất vọng về nội dung. Cảm giác truyện rời rạc và khá nhạt, đọc xong không đọng lại nhiều, có đôi chỗ khó hiểu, mình không hiểu nổi ý đồ tác giả là gì luôn. Bên cạnh đó tác giả dùng nhiều tên tắt như J hay Z nên người dịch có vài chỗ dùng nhầm lẫn, đang người này lại dịch sang người kia.

Cuốn này mình nghĩ đọc giải trí thôi chứ không để lại ấn tượng mấy với mình. À hình bìa sách cũng là hình tác giả luôn , khá đặc biệt
3
43721
2015-12-28 22:51:25
--------------------------
315096
8444
Cuốn sách "Đêm của anh, ngày của em" của Miên Miên phản ánh cuộc sống của một bộ phận thanh niên Thượng Hải. Những chàng trai, cô gái trẻ tuổi sống không biết tới ánh sáng mặt trời, không có tương lai. Cuộc sống của họ chìm trong rượu, trong thuốc lắc, trong ma túy, và trong những cuộc vui quay cuồng, thâu đêm tại các quán bar, vũ trường. Đó cũng chính là mặt trái, là góc tối của Thượng Hải phồn hoa. Cuốn sách ngắn nhưng không dễ đọc, nhiều nhân vật và tình tiết đan xen, đôi khi khá phức tạp, rắc rối. 
3
29827
2015-09-28 08:43:10
--------------------------
282564
8444
Điểm mình thích nhất ở cuốn sách này chính là văn phong của Miên Miên, mình thật sự rất ngạc nhiên thật sự thì tại sao một cuốn sách giá rẻ như vậy lại có cách diễn đạt lôi cuốn người đọc đến vậy. Văn phong lại không quá nhẹ nhàng cũng chẳng gay cấn nhưng đủ nhấn sâu vào lòng người đọc cảm giác rất đỗi mới lạ. Một cô gái đã chọn lấy cách tự lập giữa lòng một thành phố hoa lệ nhưng ẩn sâu chẳng được đẹp đẽ như vẻ ngoài của nó. Cô mạnh mẽ, kiên cường để tồn tại trong một Thượng Hải sầm uất, tấp nập cũng là tự giúp mình không úa tàn tại chính nơi mà cô yêu.


4
368588
2015-08-29 11:11:54
--------------------------
231595
8444
"Đêm của anh, Ngày của em" quả là 1 cuốn sách hay. Tác giả Miên Miên thường kể các câu chuyện về tình yêu, con người của những cô gái Thượng Hải - những cô gái có gương mặt dịu dàng nhưng ẩn dưới đó là tính cách cứng rắn, cả quyết. Cô giúp cho người đọc hình dung ra một Thượng Hải sống động, tấp nập. Quả là một nữ nhà văn đầy tài năng, có khả năng khiến cho người đọc bị rung động.
Tuy sách có nhiều truyện ngắn hay, chất lượng giấy tốt, bìa đẹp nhưng giá lại rẻ bất ngờ. Thật tuyệt vời.
5
603877
2015-07-18 10:55:13
--------------------------
