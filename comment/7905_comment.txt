417186
7905
Nelson Mandela là cái tên không còn gì xa lạ trong mỗi ta, nhưng cuộc đời của ông đã trải qua những gì thì chưa chắc ai cũng hiểu rõ. Tình cờ lại thấy bộ truyện về danh nhân thế giới trong đó có Nelson Mandala mình liền háo hức mua ngay về xem. Truyện tranh bao giờ cũng dễ đọc hơn truyên chữ nhiều, quan trọng có nhiều hình mình họa không gạy nhàm chán và buồn ngủ. Ngòai ra trong tuyên còn có những phần phụ lục cung cấp cho chúng ta nhiều kiến thức bổ ích. Cảm ơn tác giả và Tiki
.
4
509425
2016-04-16 14:06:07
--------------------------
271944
7905
Cái tên Nelson Mandela có lẽ không hề xa lạ với chúng ta, có thể ví ông với đất nước Nam Phi như Bác Hồ với Việt Nam vậy. Cả cuộc đời ông đã dành để đấu tranh chấm dứt chính sách phân biệt chủng tộc tại Nam Phi nói riêng và Châu Phi nói chung. Đem lại quyền độc lập tự chủ cho người da đen tại chính quê hương họ.
Quyển sách này tuy dành cho trẻ con nhưng người lớn cũng vẫn tìm thấy những thông tin bổ ích cho mình. Bởi vì đây là truyên tranh nên đọc quyển truyện này giúp ta nắm được nhanh nhất tất cả các sự kiện xảy ra trong cuộc đời, thân thế và sự nghiệp của Nelson Mandela.

5
501684
2015-08-19 16:36:58
--------------------------
161240
7905
Những trang sách mở ra tái hiện lại cuộc đời, thân thế và sự nghiệp của Nelson Mandela-một người suốt một đời đấu tranh cho cho sự bình đẳng giữa người da đen và da trắng. Những hoài bão và khát vọng của ông thể hiện một ý chí kiên cường, một tâm hồn bác ái và nó được nuôi dưỡng trong Nelson từ khi còn rất nhỏ. Ông đã thành công trong việc đấu tranh để chấm dứt chính sách phân biệt chủng tộc tại Nam Phi nói riêng và Châu Phi nói chung và trở thành tổng thống da đen đầu tiên tại Nam Phi. Ngoài việc học hỏi về một tấm gương cao đep về tấm lòng bác ái, sự nuôi dưỡng ý chí của một danh nhân huyền thoại, ta còn được tìm hiểu thêm về Châu Phi tự nhiên xinh đẹp. Một góc nhỏ bồi dưỡng IQ ở cuối sách chắc chắn cũng sẽ làm bạn đọc trẻ thích thú. Đây là một cuốn sách cung cấp rất nhiều thông tin hữu ích về miền đất Châu Phi mà ắt hẳn rất nhiều người muốn tìm hiểu nên rât đáng để tìm đọc.
5
534227
2015-02-27 08:07:04
--------------------------
