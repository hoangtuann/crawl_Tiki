258839
10558
Trong bộ sách’ Văn học kinh điển dành cho thiếu nhi’ của nhà sách Đinh Tỵ, mình mua đầu tiên là cuốn: Hoàng tử bé và cũng đã rất lâu rồi. Rất thích thú với cách diễn đạt ngắn gọn xúc tích kèm những hình ảnh dễ thương này, mình đã đặt mua tiếp quyển ‘ Đảo giấu vàng’ này. Đây cũng là một cuốn tiểu thuyết phiêu lưu dành cho thiếu nhi, mô tả hành trình đi tìm kho báu trên báu trên biển. Trải qua nhiều biến cố, nhân vật chính -Jim- đã trở thành một thủy thủ thật sự, cũng là một kết thúc đẹp.
5
673477
2015-08-08 21:20:41
--------------------------
243117
10558
Dạo này mình ngán đọc tuyển thuyết quá nên chọn mua quyển này để thay đổi thú vui đọc sách. Quả thật truyện rất hay! Về tác phẩm kinh điển này thì nội dung khỏi bàn cãi luôn rồi, cực kỳ hấp dẫn. Bên cạnh đó tranh vẽ minh họa vô cùng dễ thương luôn, tô màu đẹp lung linh. Đúng là cho thiếu nhi nên hình thức rất đẹp. Sách của Đinh Tị nên chất lượng giấy, màu sắc đều ổn cả. Tuy nhiên sách cho trẻ em nên truyện cũng phần nào bị tóm gọn, lược bớt đi, không hay bằng đọc nguyên bản.
5
400930
2015-07-27 10:39:11
--------------------------
