478955
9659
Đây là quyển sách vô cùng ý nghĩa từ tựa sách đến nội dung. Hạt giống tâm hồn thì đã khá quen thuộc với chúng ta. Mình dùng quyển sách này làm món quà tặng cho chị mình nó thật ý nghĩa. Chúng ta có thể mua 1 quyển sách này với giá trị cao hẳn hơn giá tiền . Đọc để hiểu hơn về tình chị em, dù quyển sách kia đã tặng cho chị nhưng mình đang đợi tiki nhập thêm về rồi mua thêm 1 quyển nữa. Hãy mua về và đọc để gắn kết tình chị em nhé các bạn
5
725049
2016-08-06 10:11:01
--------------------------
56699
9659
Quyển sách này tôi đã đọc rất nhiều lần, thấy thật bình thường. thế nhưng, một ngày nọ, khi tôi ở nhà một mình, rất sợ, rất buồn, tôi tình cờ đọc nó một lần nữa, chị tôi, không bình thường như tôi từng nghĩ…
Chị là điều thiêng liêng vĩ đại, lớn lên cùng ta, che chở ta khi bị la mắng, chị khóc khi ta đau. Ngày tôi còn bé, hai chị của tôi luôn bảo vệ tôi, luôn yêu thương tôi, luôn biết tôi cần cái gì, muốn cái gì. Tình chị em, không có những cử chỉ nồng nhiệt như tình yêu đôi lứa, không cười đùa ầm ĩ như những người bạn, mỗi một người có một cuộc sống riêng, nhưng khi trở về nhà, chị em lại là người mà ta gần gũi nhất…
Quyển sách rất mỏng, mỗi câu chuyện rất ngắn, nhưng chứa đựng thứ tình cảm ngọt ngào thầm lặng mà đến một lúc nào đó ta mới nhận ra. Khi ta còn nhỏ, khi ta đã lớn, chị em cứ xa nhau dần, nhưng những tình yêu thời còn bé vẫn ở đó, khi chị cho ta một phần cơm trưa, khi dắt ta đi leo núi trốn bố mẹ, khi thức đến khuya với ta để gặp ông già Noel,… những câu chuyện như vậy đó, nhưng tôi thấy, chị tôi đã làm tròn sứ mệnh của nó – khiến ta rơi nước mắt, khiến ta chợt thấy nhớ chị biết bao, dù chị thật gần…

5
76823
2013-01-23 21:15:42
--------------------------
48610
9659
"Chị tôi" là một tập hợp rất nhiều mẩu chuyện nhỏ thú vị, mỗi chuyện mang trong mình những gam màu khác nhau nhưng đều chất chứa nhiều cảm xúc và gửi gắm những thông điệp về tình yêu thương. Mỗi câu chuyện là một lát cắt trong cuộc sống thường ngày của mỗi người, đi sâu vào khai thác tâm lý và tình cảm của chị-em gái. Qua mỗi câu chuyện, người đọc như tìm thấy hình ảnh của chính những người thân yêu bên mình, hiểu rõ thêm về giá trị của tình chị em, tình ruột thịt, về mối dây tương đồng hàn gắn những tâm hồn và trái tim. Với ngôn từ trẻ trung, dễ cảm nhận, và cách diễn đạt phù hợp với bạn đọc trẻ, cuốn sách thực sự là một món quà dành đến cho những người thân yêu bên bạn.
4
20073
2012-12-02 12:05:29
--------------------------
