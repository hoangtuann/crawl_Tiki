488919
5509
Truyện có nội dung chán, có cảm giác ở vấn đề nầy lại chuyển wa khác, không thấy chân thật, Gainr Huệ vì yêu đến tự sát? Nhảm! Nữ chính trẻ con, chán, thiếu thực tế, nam chính lại nhu nhược, bỏ đi là tốt nhất. Thấy thất vọng vì mua cuốn này



1
1359271
2016-11-05 14:49:38
--------------------------
408375
5509
Ngải Mễ , đó là một cái tên khá xa lạ đối với tôi . Tôi mua cuốn sách này cũng bởi vì bìa cuốn sách thực sự rất đẹp và tên truyện nghe cũng rất hay , ý nghĩa . Bình thường , khi nhắc đến thể loại truyện ngôn tình thì có lẽ người ta sẽ thường chọn những tác giả nổi tiếng và quen thuộc như : Diệp Lạc Vô Tâm , Phỉ Ngã Tư Tồn, Cửu Lộ Phi Hương ,... nhưng khi đọc tác phẩm này , tôi nghĩ rằng Ngải Mễ cũng rất xứng đáng nằm trong top đó . Tuy mô típ truyện khá quen thuộc nhưng qua đôi bàn tay của tác giả thì lại thành rất độc đáo , thú vị và lôi cuốn . Tôi nghĩ thế là đủ cho một câu chuyện ngôn tình .
3
1190243
2016-03-31 16:17:22
--------------------------
315632
5509
Tôi rất thích các tác phẩm của Ngải Mễ, và tôi đã mua tác phẩm này trong đợt hội chợ sách Xuân vừa rồi. Tôi thích nhân vật Ngải Mễ trong tác phẩm, cứ có cảm giác như chính cuộc tình của tác giả vậy. Một cô gái trong sáng, dám làm tất cả vì yêu! Tình yêu của Ngải Mễ và Allan/ Jason , trải qua nhiều cung bậc, từ rất hạnh phúc (Dù Ngải Mễ luôn nghi ngờ Allan có thật lòng với mình hay không, và cô luôn tự vẽ ra các tình huống nào đó mà Allan sẽ là nhân vật nam chính với một người không phải là cô) - khi Allan bị đi tù oan, cô là người hết lòng cứu cô, kể cả sẵn sàng đánh đổi với Trần Côn. Rồi họ xa nhau một thời gian tưởng chừng như tình yêu đó chấm dứt. Nhưng họ gặp lại, cả hai như trốn tìm trong vòng tròn tình yêu của mình. Và cuối cùng, cả hai đều vẫn mãi luôn là người yêu duy nhất trong lòng nhau! 
4
572177
2015-09-29 08:59:53
--------------------------
257369
5509
Theo mình thấy''Ngắm hoa nở trong sương'' nhận được khá nhiều ý kiến trái chiều.Có người khen hay,có người nói dở,nhưng đó là cảm nhận riêng của mỗi người.Theo riêng mình thì mình thích truyện này.Câu truyện xoay quanh tình yêu của Ngải Mễ và Allan. Hai con người gần như là đối lập nhau.Ngãi Mễ là cô gái thẳng thắn,mang theo suy nghĩ tình yêu phải cuồng nhiệt,yêu nhau phải như sông chết,đã thế còn hay suy nghĩ và hay suy diễn linh tinh khiến Allan mấy phen đau đầu.Và theo cảm nhận của một số người,đây còn là nhân vật gây ức chế số 1,nhưng đơn giản theo mình đó cũng là tâm lý phổ biến của các cô gái khi sở hữu một chàng trai gần như hoàn hảo như Allan mà thôi.Còn Allan lại là thái cực hoàn toàn khác,quan điểm về tình yêu của anh là chỉ cần khiến người mình yêu hạnh phúc là được.Mặc dù câu chuyện có thể khiến một người không kiên nhẫn lắm sẽ bực mình vì diễn biến hơi lê thê nhưng mình thấy trong truyện có một số câu khá hay và đáng suy ngẫm,Ngoài ra một điểm cộng cho truyện đó là bìa rất đẹp nữa.Nhìn là muốn mua ^^
5
153406
2015-08-07 16:48:26
--------------------------
239967
5509
Lúc đầu vì thấy bìa truyện khá đẹp nên mới mua , nhưng nội dung bên trong làm mình cả thấy rất thất vọng . Cách tác giả xây dựng tình tiết quá nhàm chán , dài dòng làm mình không muốn đọc nhưng vì sợ uổng tiền nên gắng gượng để đọc tiếp . Khúc sau thì đỡ hơn khúc đầu một chút , phải chi tác giả tập trung vào tình tiết  nội dung hơn thì sẽ hay hơn không . Nhân vật nữ chính - Ngải Mễ phải nó là quá nhu nhược làm mình cảm thấy ức chế . Nói chung thì quyển sách này làm mình quá thất vọng . Hi vọng sau này tác giả sẽ viết những tác phẩm khác hay hơn .
1
391948
2015-07-24 10:36:26
--------------------------
199957
5509
Hình thức: Bìa quá đẹp, khá hợp với tên truyện.
Đây là lần đầu mình đọc tác phẩm của Ngãi Mể và được viết như tự truyện của cô. Trước tiên nói về nhân vật nữ chính, cô cá tính, mạnh mẽ, thích nói lý lẽ với người khác, thích tranh luận. Nhưng cái mình thích nhất ở cô đó là sự thẳng thắn, yêu thì nói yêu, ghét thì nói ghét mà không cần tính toán vụ lợi gì. Cô yêu hết mình, sẵn sàng hy sinh cho người mình yêu nếu cô thấy người đó là xứng đán. Tuy cô có hơi trẻ con, tính tình nóng vội, giận một chút, nghi ngờ một chút lại suy diễn linh tinh, nghi ngờ người mình yêu, những điều đó chỉ xuất phát khi bạn quá yêu người khác, cô yêu Allan một cách cuồng nhiệt, khoảng thời gian tạm thời chia tay, dù bao nhiêu năm đi nữa cô vẫn chỉ yêu và nhớ nhung một mình anh.
Allan một chàng trai trẻ, đẹp trai, giỏi giang, rất nhiều cô gái theo đuổi thậm chí Jane thầm yêu anh 6 7 năm, tự tử vì anh. Ngải Mễ khá là may mắn khi được anh yêu, cô thường trách anh yêu không nhiệt tình, cô quá ngốc nghếch nghi ngờ anh hết lần này đến lần khác trong khi anh thật lòng chỉ yêu mình cô, không lừa dối cô bao giờ. Anh luôn dịu dàng , quan tâm, chăm sóc, lo lắng cho cô, anh dường như là một người cực kỳ tốt, hoàn hảo không thể tả.
Một  chuyện tình đẹp thì dĩ nhiên sẽ có một cái kết đẹp.
5
38403
2015-05-23 18:17:45
--------------------------
151670
5509
Khi tôi mua quyển sách này, cái đập vào mắt tôi là tên truyện "Ngắm hoa nở trong sương" nghe rất hay, rất có ý nghĩa, đọc nội dung ngoài bìa cũng cảm thấy rất được, nhưng đến khi đọc toàn bộ trong đó thì thấy khá thất vọng. Nữ chính Ngải Mễ thường rất trẻ con, thậm chí cũng gây cho mình cảm giác rất khó chịu, mồm thì bảo tin tưởng nam chính nhưng hết lần này đến lần khác đều đổ tội, tra khảo, trách móc anh. Người ta bị oan, phải vào đồn thu thẩm rất khổ cực nhưng vẫn lo lắng cho chị nữ chính, còn Ngải Mễ thì lại chỉ biết nghĩ đến bản thân, còn đang bận mải nghĩ rốt cuộc anh không yêu cô này nọ, sau đó khi tiểu Côn xuất hiện chiều chuộng nâng niu cô, Ngải Mễ còn có ý nghĩ ước gì tiểu Côn mãi theo đuổi cô, Allan thì mãi yêu mình còn cô thì cảm nhận cả hai thứ, không muốn bỏ đi bất cứ tình cảm nào, ích kỉ và hay tự khen bản thân mình. Không những thế còn xuất hiện nhân vật Giản Huệ, câu chuyện của người con gái này thật quá lâm li, chỉ vì sợ phân biệt tuổi tác mà giữ trong lòng rồi tự tử trong phòng Allan khiến anh bị dính nghi án suốt một thời gian. Chỉ có thể nói, Allan là nhân vật nam chính hiền lành nhất mà mình từng đọc, không thể hiểu sao tác giả có thể viết ra một câu chuyện kiểu khó chịu như vậy.
2
111548
2015-01-20 17:32:41
--------------------------
133757
5509
Ngải Mễ là một trong những tác giả mà tôi ưa thích nhất và rất tin tưởng vào tài viết văn của chị . Khác với những tác phẩm tiểu thuyết tình cảm lãng mạn được ra đời trước đó Ngắm hoa nở trong sương mang một cái gì đó mang mác buồn . Cuốn sách như chính là câu chuyện của cuộc đời tác giả vậy . Nữ chính trong truyện luôn tự ti về bản thân mình chỉ là một cô gái hết sức bình thường cô nghĩ mình không xứng với nam chính . Nhưng cô đâu có biết anh yêu cô bởi vì chính sự bình thường tưởng chừng như không có gì đặc biệt đó là của cô . Thay vì theo đuổi những thứ phù phiếm không thuộc về mình chi bằng hãy trân trọng những thứ mình có thể không phải tốt hơn sao
4
337423
2014-11-06 19:58:34
--------------------------
