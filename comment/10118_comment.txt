471657
10118
Tôi đã mua cuốn “Bé Vui Học Tiếng Anh Ở Nông Trại” vì vào thời điểm được khuyến mãi và cũng vì nhìn hình bìa chú bò được vắt sữa khá dễ thương. Ưu điểm: nổi bật đầu tiên là giấy cứng, và rất dày, hình dễ thương, từ vựng có phiên âm quốc tế giúp người dạy/ người học phát âm chuẩn hơn, tuy nhiên…. Khuyết: cuốn sách này có lẽ gần như chắc chắn do người Việt tự soạn, và biên tập, và có khá nhiều sạn: 1) phiên âm quốc tế sai: eraser BrE /ɪˈreɪzə(r)/; NAmE /ɪˈreɪsər/, thay vì /i’reize/ (trang 12), flower BrE /ˈflaʊə(r)/; NAmE /ˈflaʊər/, thay vì /flower/ (trang 14) ,… 2) dùng tiếng Anh sai (she goes ‘TO’ cluck cluck): không có chữ ‘TO’, … 3) tư duy dùng từ của người Việt: Jean (quần Jean không có S, vì văn hóa người Việt không có khái niệm MỘT cái quần giin phải là số NHIỀU trong tiếng Anh.)… Hy vọng ban biên tập Nhà xuất bản sẽ cẩn thận hơn trong việc xuất bản một quyển sách nhằm mục đích giáo dục như thế này.
2
966571
2016-07-08 20:54:54
--------------------------
