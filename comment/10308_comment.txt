267299
10308
Truyện khi mình nhận được đẹp, có cả bọc plastic khiến truyện đẹp và sáng hơn. Truyện được trang trí rất đẹp, in màu toàn bộ, chất lượng giấy in truyện cũng rất tốt, khá dày. Nội dung truyện, phần đầu là nói về đêm trước đám cưới của nobita và shizuka, rất hay cảm động. Nobita tuy rất hậu đậu, vụng về, làm đâu hư đấy,... nhưng cậu có một tấm lòng nhân hậu biết bao, rất yêu thương người, đến cả cọng cỏ, cành cây, đến cả động vật lang thang hay cả những con thú dữ, có gì vui cậu đều chia sẽ cho những người xung quanh, nhất là shizuka, mỗi khi Doraemon có bảo bối gì mới là cậu đều đem qua cho cô ngay !Chính vì vậy, Shizuka đã chọn Nobita một cách rất chân thành ! Phần hai nội dung truyện cũng hay không kém, kể về tình bà thiêng liêng, xúc động, kỉ niệm giữa bà ngoại và Nobita. Dù bà đã mất lâu rồi nhưng trong lòng nobita vẫn còn hình bóng của bà ngày nào, một hôm cậu bắt gặp lại chú gấu bông mà bà đã làm tặng cậu, khiến cậu càng nhớ về bà hơn, và cậu quyết định tìm lại bà bằng cỗ máy thời gian, khi trở về, rất nhiều chuyện đã xảy ra, có nhiều tình tiết xúc động khi về cuối truyện ! Nội dung câu truyện khuyên ta hãy trân trọng những thứ đang có, để rồi khi mất hối hận không kịp, thì đã muộn ! Đây quả là cuốn truyện hữu ích, các bạn hãy cùng tìm đọc nhé ! 
5
120141
2015-08-15 13:50:12
--------------------------
49026
10308
Đây chính là tập truyện về đôrêmon mà mình cảm thấy ấn tượng sâu sắc nhất, chỉ có thể nói hai từ "xúc động" mình đã không thể kềm được nước mắt khi đọc tập này, nào là nôbita được bà rất mực yêu thương che chở, bà luôn yêu quí cậu dù cậu nghịch ngợm phá phách hay làm bà buồn lòng. Đọc xong mình cảm thấy rất nhớ bà mình, trước đây bà mình cũng như thế, cũng luôn che chở cho mình. Nhưng từ khi mình đi học xa nhà không còn dịp gặp bà thường xuyên nữa nên đọc xong tập truyện này khiến mình thấy rất xúc động. Truyện không chỉ là giúp chúng ta vui vẻ sau giờ học căng thẳng mà tập truyện này còn chứa đựng những ý nghĩa vô cùng sâu sắc àm nhà văn muốn nhắn nhủ tới mọi người. Hãy trân trọng những thứ xung quanh mình, đó chính là những điều đáng quý nhất trong cuộc sống này. Đừng bỏ qua tập truyện này nhé, nó sẽ đem đến nhiều bài học bổ ích lắm đấy
5
60663
2012-12-04 19:54:37
--------------------------
46761
10308
Đây là tập truyện xúc động nhất mình đã từng xem. Có thể nói Doraemon đã gắn bó với tuổi thơ mình từ khi còn nhỏ và là tập truyện đầu tiên mình xem suốt quãng đời ấu thơ. Ngày trước tập "Nobita đêm trước ngày cưới" không đầy đủ như tập tái bản hiện giờ. Đến hôm nay khi đọc truyện này mình mới cảm nhận trọn vẹ n ý nghĩa của nó. Tình yêu thương, tình cảm vợ chồng, tình bà cháu thật thiêng liêng. Nobita tuy là chàng trai hậu đậu, học không giỏi, không đẹp trai nhưng là 1 người có trái tim nhân hậu, biết yêu thương mọi người, ngay cả 1 cọng cỏ, cành cây. Chính vì vậy mà Xuka đã chọn cậu làm người bạn đời vì cô tin rằng cậu sẽ mang lại cho cô 1 gia đình hạnh phúc.

Trong lúc đó truyện "Kỉ niệm về bà" cũng đầy ý nghĩa. Tình bà cháu trong truyện rất xúc động. Dù đã lớn nhưng Nobita luôn là đứa cháu bé bỏng, đáng yêu của bà ngoại. Dù bà đã mất rồi nhưng bà luôn sống mãi trong tim cậu bé thông qua hình ảnh chú gấu bông đáng yêu!
5
33861
2012-11-18 09:53:01
--------------------------
43662
10308
"Đêm trước đám cưới Nobita" cũng là 1 mẩu chuyện hay mà tôi rất thích chỉ sau "Kỉ niệm về bà". Nhiều lần tự hỏi rằng tại sao 1 cô gái thông minh, giỏi giang như Xuka lại chấp nhận lấy Nobita nhưng sau khi xem xong "ĐTĐCN" tôi đã hiểu. Nobita tuy hậu đậu nhưng Ở cậu có 1 thứ mà nhiều người khác không có, đó chính là tấm lòng. Cậu luôn yêu thương mọi thứ, từ cái cây bé nhỏ đến con mèo. Bất chấp khó khăn để đem con mèo trả lại cho chủ nó trước khi đi Mỹ; vộ tình đè phải một cái cây cậu cũng cố gắng kiếm 1 cành cây nhỏ để nó nương tựa... Và ắt hẳn Xuka cũng đã thấy được tấm lòng của Nô và tin tưởng cậu ấy sẽ là người có thể mang lại hạnh phúc cho mình. Và qua tập này có thể thấy được tình cảm của cha mẹ đối với con cái là như thế nào, diển hình là cuộc nói chuyện giữa cha và con gái (Xuka). Chỉ cần con mình đươc hạnh phúc thì đó chính là kho báu vô giá đối với bậc làm cha làm mẹ. Một mẩu chuyện tuy ngắn nhưng lại hàm chứa rất nhiều ý nghĩa để truyền tải đến với mọi người.
2
46651
2012-10-25 22:21:32
--------------------------
43661
10308
Đọc câu chuyện "Kỉ niệm về bà" làm tôi không khỏi xúc động và tôi đã khóc mỗi lần xem nó. Một câu chuyện rất hay về tình cảm giữa Nobita và người bà. Bà luôn là người che chở, bảo vệ Nô mỗi khi gặp chuyện, luôn nhường nhịn và nuông chiều Nô. Nhưng lúc ấy Nô còn quá nhỏ để hiểu tình cảm ấy. Đến lúc không còn mới thấy quý trọng. Chi tiết cảm động nhất phải nói đến là khi người bà may lại con gấu bông cho Nô và ước gì được thấy Nô cắp sách đến trường vì bà biết bà không còn nhiều thời gian nữa. Hãy biết trân trọng những thứ mình đang có. Đến lúc mất đi rồi mới thấy hối tiếc. Chúng ta sẽ không may mắn như Nobita vì cậu ấy có Doremon và cỗ máy thời gian có thể trở về quá khứ. Còn chúng ta thì vẫn cứ tiếp tục tiến đến tương lai và chỉ có thể bỏ lại phía sau những tiếc nuối.
2
46651
2012-10-25 21:49:35
--------------------------
