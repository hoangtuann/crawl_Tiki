562785
10133
Về thời gian giao hàng và giá cá thì khỏi phải nói nhiều nữa: rất rất ok.
Về sản phẩm này thì : phải nói là mình yêu từ cách bao sách cho tới từng chi tiết của cuốn sách. Quá ư là hài lòng
Có một điều mình cần gửi gắm là kiti nên nhập thêm nhiều tác phẩm hay mà được ấn bản bằng BÌA CỨNG ẤY vì cầm cuốn sách lên tay là không có chỗ nào phàn nàn hết ngược lại rất yêu nó nữa ấy chứ
Xin chân thành cảm ơn Đội Ngũ Của Tiki rất nhiều!
5
1431960
2017-04-03 12:16:59
--------------------------
473246
10133
Những gì bạn biết được về Adoft Hitler liệu đã đủ? Bạn có đang đánh giá một người thông qua quan điểm của kẻ khác? Chẳng phải ngẫu nhiên mà một kẻ bại trận trong thế chiến thứ nhất lại có thể leo lên vị trí lãnh đạo đất nước trong một thời gian ngắn, hãy dẹp cái thuyết âm mưu của bạn sang một bên đi và đọc cuốn sách này. Tôi cho rằng đây là một công trình nghiên cứu hơn là một cuốn tiểu sử thông thường. Theo tôi đây là cuốn sách đầy đủ và hoàn chỉnh nhất về cuộc đời của Adoft Hitler.
5
14119
2016-07-10 13:21:40
--------------------------
401430
10133
Viết về Adolf Hitler, không ít nhà nghiên cứu đã từng. Nhưng mình chỉ thực sự ấn tượng với cuốn tiểu sử đầy đủ, hoàn hảo, rõ ràng và chi tiết nhất này của John Toland. Ông có sự dày công nghiên cứu, có cả những tài liệu chỉ mình ông có, phỏng vấn khoảng 200 người đã từng tiếp xúc với Hitler để tạo ra cuốn tiểu sử dày hơn 1000 trang này.
Về Adolf Hitler, chúng ta thường chỉ nhận xét rằng ông là một trùm phát xít, một tên độc tài với dã tâm độc địa làm cả thế giới phải khiếp sợ, nhưng trên thực tế, ông là một nhà chính trị tài ba, lỗi lạc, có tài xoay chuyển tình thế và trên hết là một nhà dân tộc học, tư tưởng yêu nước đến cuồng loạn. Chẳng phải ngẫu nhiên mà ông tôn sùng nước Đức, chủng tộc Avaria thuần khiết; có tài thuyết giáo và lãnh đạo, biến một tổ chức Đảng tưởng chừng như một buổi tụ họp cafe với 7 thành viên thành Đảng NSDAP lớn mạnh, nắm chính quyền nước Đức thời bấy giờ.
Điều mình thích nhất trong cuốn sách đó là việc tác giả cố gắng viết một cách trung thực và khách quan nhất, dựa trên lời phỏng vấn với từng nhân chứng. Các câu chuyện đời tư của Hitler không bị đào bới sâu, chủ yếu chỉ là một vài phần ngắn gọn để có bức chân dung hoàn hảo nhất. Cả cuốn sách tập trung vào con đường chính trị, từ một kẻ bần cùng khốn khổ phải đi xin cháo thí đến người lãnh đạo Đức Quốc xã, gây nỗi kinh hoàng cho cả thế giới.
Vì cuốn sách đi theo chiều dài lịch sử cuộc đời Hitler lẫn bối cảnh lúc bấy giờ, nên khi đọc cuốn sách, bạn có thể tìm hiểu qua về hai cuộc Thế chiến ông đã đi qua để nắm rõ hơn về tư tưởng, suy nghĩ của Hitler.
5
182482
2016-03-20 18:41:21
--------------------------
338882
10133
Tôi thực sự thực sự ngưỡng mộ tác giả John Tolan vì những nỗ lực, sự cố gắng tuyệt vời của ông trong việc tìm hiểu về của đời của Adolf Hitler. Đọc cuốn sách, tôi có thể tưởng tượng được rằng ông đã dành hết bao tâm huyết để đưa đến cho độc giả một cái nhìn, một sự hiểu biết sâu sắc và cụ thể nhất cho người đọc về toàn bộ cuộc đời của tên trùm phát xít khét tiếng nhất trong Lịch sử Thế giới. Phải nói là cuốn sách quá sức tỉ mỉ, quá sức chi tiết, và nhiều lúc tôi phải tự hỏi bản thân, làm sao mà tác giả lại có thể biết hết những chi tiết như thế này. Phải, toàn bộ - toàn bộ cuộc đời Hitler từ khi được sinh ra, thời thơ ấu, lớn lên, những nền tảng tư tưởng chính trị ban đầu được hình thành như thế nào... cho đến khi trở thành tên Trùm của Đức Quốc xã và cái chết vào năm 1945, quá sức tỉ mỉ, quá sức tường tận. Khâm phục và rất cảm ơn tác giả vì điều đó.
Trước đây tôi chỉ được học một chút chút về Hitler trong các bài học lịch sử phổ thông, tôi chưa bao giờ tự đặt câu hỏi cho bản thân về sự ra đời của Hitler, thời niên thiếu cũng như lý do mà ông căm ghét người Do Thái đến mức như vậy. Tôi chỉ biết Hitler là cái ông mà bị cả thế giới ghê tởm, kinh sợ, diệt chủng một giống người nào đó trên Trái Đất ("hình như người Do Thái thì phải"). Tôi cứ nghĩ chắc ông cũng phải lớn lên trong điều kiện vật chất tốt lắm... Tôi chỉ nghĩ thế mà không thắc mắc gì thêm về nhân vật này, mà cũng không muốn thắc mắc, vì tôi chả có hứng thú gì với ông cả.
Cho đến khi đọc được hai cuốn Kẻ Trộm Sách và Nhật Ký Anne Frank, tôi mới vội vàng chạy vào thư viện lục tìm tài liệu viết về Hitler, và may mắn thay, tôi kiếm được cuốn sách nặng đến cả ký này. 
Đọc nó, tôi mới giật mình kinh ngạc rằng, Hitler thật ra không lớn lên trong sự giàu có như tôi từng tưởng, đã có lúc ông ta không còn một mẩu bánh mì mà nuốt, không có nhà mà trú thân, phải ngủ ngoài ghế công viên, ngủ dưới hiên nhà người khác trong mùa đông đầy tuyết... đến mức mà bản thân ông ta đã từng hằn học "Đây là cuộc sống của chó chứ không phải của người". Đọc đến đó thì cũng thấy hơi thương thương. Tôi cũng không ngờ là theo lời những người quen biết với Hitler thời đó (thời trước khi ông trở thành Quốc trưởng và chưa có ảnh hưởng gì đối với dư luận), ông ta lại được nhiều người quý mến như vậy. Thật sự không thể tưởng tượng nổi. Đọc cuốn sách, tôi cũng biết thêm về những lí do mà Hitler căm thù người Do Thái, tôi nhớ nhất là câu nói của ông "Hầu hết công chức (của nước Đức) đều là người Do Thái, hầu hết người Do Thái đều là công chức, nhưng đại diện của họ ở chiến trường (Chiến tranh thế giới thứ Nhất ) lại quá ít".
Xét về một phương diện nào đó thì Hitler là một người tài. Chỉ vì tư tưởng chính trị của ông bị lệch lạc. Thật tiếc là tài năng của ông lại đặt không đúng chỗ. Nếu hồi đó ông thi đậu vào Trường Nghệ thuật Áo thì cục diện thế giới đã khác, thực sự khác rồi. Có lẽ sẽ không có Thế chiến 2, không có hơn 50 triệu người chết, cũng không có 6 triệu người Do Thái bị diệt chủng. Chỉ một sự thay đổi nhỏ có thể xoay chuyển cả lịch sử.
P/S. Bình luận quá sức dài thế này mà chỉ cho có 50 tiki xu, keo kiệt quá!
5
220900
2015-11-17 00:25:49
--------------------------
