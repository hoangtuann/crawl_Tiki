384153
11255
Bắt gặp anh bạn này một lần đi nhà sách Fahasa. Mình vốn là người thích phiêu lưu và khám phá nên bị ấn tượng ngay bởi tiêu đề cuốn sách. Bè tre Việt Nam du kí. Vốn đã từng nghe tiếng về hành trình này trên báo nên khi tình cờ gặp được cuốn sách này mình thật sự rất vui mừng. Định sẽ rinh luôn em nó về nhưng nhìn giá thì hơi cao nên đành tiếc nuối tạm rời xa em nó để lên tiki đặt hàng. Mình là khách hàng ruột của tiki nên biết chắc rằng mua hàng ở trên tiki sẽ được giảm giá rất nhiều và tiki đã không làm mình thất vọng. Bìa sách rất đẹp, sách in trên giấy xốp phần lan nên rất nhẹ và đẹp. Mình rất thích. Sắp sửa đọc nó và hy vọng sẽ không làm mình thất vọng. Cho tiki 5 sao luôn.
5
132597
2016-02-21 21:12:30
--------------------------
283424
11255
Mình đã đọc rất nhiều cuốn sách về những chuyến phượt của các phượt thủ, nhưng đây là cuốn sách thật sự đặc biệt. Đặc biệt bởi chuyến phượt không phải trên đường bộ mà là trên biển, phương tiện không phải là xe, tàu lửa hay máy bay, thậm chí không phải là tàu thủy mà bằng một phương tiện tối giản của Việt Nam: bè tre! Đặc biệt hơn cả vì tác giả về chuyến phượt không phải là người Việt Nam mà là một tác giả nước ngoài. Đây không chỉ đơn thuần là một cuốn sách viết về một cuộc hành trình kỳ lạ mà còn chuyển tải khối lượng lớn kiến thức về lịch sử, địa lý, thiên văn, hàng hải. Qua cảm nhận của một người nước ngoài hoàn toàn lạ lẫm với một phương tiện đường thủy vô cùng thô sơ của Việt Nam, con người và cuộc hành trình cũng trở nên rất kỳ lạ và kiên cường, những con người bé nhở, chiếc bè tre bé nhỏ có thể vượt 5.500 dặm đại dương. Tác giả có công rất lớn trong việc đưa các giá trị về con ngwoif Việt Nam ra với thế giới!
5
24486
2015-08-29 23:26:34
--------------------------
270603
11255
Lần đầu tiên tôi đọc một cuốn sách do tác giả nước ngoài viết nhưng lại có cách viết như tác giả người Việt vậy. Cách hành văn gần gũi, nhẹ nhàng, đầy hình ảnh như có một sức cuốn hút mãnh liệt, làm tôi như đắm chìm vào cuộc phiêu lưu. Đây không chỉ là hồi kí của một đoàn thám hiểm về hành trình xuyên hải, nó còn dạy cho chúng ta nhiều điều về kiến thức hàng hải, về những kinh nghiệm đi biển, sự quan trọng của tình đồng đội và cả những thử thách khắc nghiệt của thiên nhiên cũng như vẻ đẹp của nó. Qua cuốn sách, tôi thấy được tinh hoa của chiếc bè tre Việt Nam, thấy được sự kiên cường của mảng Từ Phúc cũng như con người trên đó trước những khó khăn trong hành trình vượt biển.
5
259553
2015-08-18 13:49:40
--------------------------
264432
11255
Cuốn sách gợi mở một chân trời phiêu lưu mới mà tôi chưa bao giờ nghe đến, có những con người bình dị khám phá những tuyến đường khai mở của lịch sử loài người. Điều đặc biệt là có một con người Việt Nam nhỏ bé đã cùng những người bạn đi trên mảng tre vượt qua Thái Bình Dương bao la, tuy bất đồng ngôn ngữ nhưng sự kỉ luật, đoàn kết đã giúp cả đoàn vượt qua mọi khó khăn, đưa mảng tre đến gần bờ Tây châu Mỹ. Đóng góp đó đã giải thích một phần về sự tương đồng của các nền văn hóa khác châu lục, có thể đã có một con đường đi xuyên biển từ một vùng đất nào đó ở nước Việt Nam bây giờ. Thật khâm phục và đáng tự hào.
5
461808
2015-08-13 10:19:14
--------------------------
208221
11255
Cuốn sách ban đầu cuốn hút tôi vì dòng chữ "bè tre Việt Nam". Thật sự bè tre có thể làm được ư? Ba chương đầu của cuốn sách giúp tôi biết thêm về mảng tre, buồm dơi và cuộc sống ở làng chài Sầm Sơn. Mọi thứ đều rất mới mẻ. Ngoài ra, đây là lần đầu tiên tôi được đọc những miêu tả về Việt Nam ở thời kì sau đổi mới nhưng vẫn còn đang bị Mỹ cấm vận. Một góc nhìn rất khác về những điều tôi thường nghe. 
Bước vào cuộc phiêu lưu, tác giả mô tả rất chân thực những khó khăn trên suốt cuộc hành trình. Như tác giả nói, quan trọng nhất là bản thân cuộc hành trình, chứ không phải là điểm đến. Tuy không đến được điểm cuối cùng, nhưng chuyến đi đã thành công, khi một phần chứng minh thuyết xuyên dương là có thể, và tinh hoa của mảng tre Việt Nam.

Bìa sách và chất lượng giấy in đều rất đẹp
5
104850
2015-06-14 15:30:48
--------------------------
