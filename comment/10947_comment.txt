354849
10947
Giống cuốn Búp bê Bắc Kinh của Xuân Thụ, tác giả nói ra những suy nghĩ thực của mình về thế giới xung quanh cô ấy mà cũng có thể là suy nghĩ của nhiều người. 
Chúng ta đang sống trong một thế giới giả tạo như vậy do chính chúng ta tạo ra và mỗi người mang một chiếc mặt nạ, thậm chí là nhiều mặt nạ khác nhau. 
Đọc thì buồn cười lắm vì cô ấy nói chính xác nhiều chuyện như anh bạn của cô ấy tổ chức đám cưới ở nhà thờ vì người yêu thích chứ anh ấy không thích. Thực sự chúng ta làm rất nhiều chuyện vì người khác thích và mình thì muốn được người ấy chấp nhận. 
4
535536
2015-12-18 20:00:32
--------------------------
269989
10947
Với những ai không quen lối văn phong và suy nghĩ của các nhà văn châu Âu, như Pháp, Bỉ, Ba Lan, Tây Ban Nha,.. có lẽ sẽ gặp khó khăn khi tiếp cận cuốn sách nào. Những suy nghĩ rời rạc, nhưng câu chuyện ngắt quãng, nhưng câu văn dài, quá dài và những câu văn quá ngắn, cả những đoạn hội thoại cộc lốc... Tất cả trông thật ngớ ngẩn nhưng lại thật tinh tế! cảm xúc ở đây là thật, những người con người quá nhạy cảm, quá...tinh tế, quả bay bổng và..."trên tầm". Thật sự khó khăn khi xung quanh ta toàn kẻ ngốc, những người đơn giản,... Tôi có đọc một cuốn sách khác cũng của Ba lan, "Cô đơn trên mạng" được xuất bản cùng thời kì, và những nhân vật trong đó, phần nào đó cũng như cô Magda kia.
Nhìn chung đây là một cuốn sách hay, mỏng nhưng không dễ để đọc nhanh. Có một sự thu hút rất riêng. 
R-I-Ê-N-G...
4
61048
2015-08-17 21:19:21
--------------------------
120856
10947
Gấp trang sách lại mà sao tôi vẫn thấy mình quá mơ hồ...và tôi chưa bao giờ có một cảm giác như vậy khi đọc xong bất cứ một cuốn sách nào. Không phải mơ hồ vì câu chuyện có kết thúc mở mà bởi chính nội dung và ngôn từ hết sức lộn xộn trong "Ngoài vùng phủ sóng". Về ngôn từ, đối thoại 2 người tự dưng chuyển sang độc thoại, rồi chuyển sang suy nghĩ cá nhân rồi lại chuyển sang đối thoại, khiến cho người đọc thấy tràn lan và mệt mỏi. Về nội dung, một cô gái ko tìm được sự hòa nhập với mọi người xung quanh hoang moang loanh quanh tìm lối thoát tìm bộ mặt cho mình từ đầu truyện đến cuối truyện để kết thúc được gì? để tự ném mình theo vòng xoay của cuộc đời, muốn ra sao thì ra, để rồi vẫn mãi hoang mang ngơ ngẩn băn khoăn trăn trở đến tội nghiệp...
Kết luận của cá nhân tôi, thực sự đối với truyện ngắn này tôi nằm "ngoài vùng phủ sóng". Và nếu tác giả cứ giữ giọng văn này, và dịch giả cứ giữ văn ngôn như vậy, tôi dám chắc bất kì độc giả nào cũng "ngoài vùng phủ sóng" mà thôi.
1
97889
2014-08-15 15:44:54
--------------------------
