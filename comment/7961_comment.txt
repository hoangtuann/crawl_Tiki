284575
7961
Mình đặt mua trên Tiki quyển sách Bé Vào Lớp Một - Tập Tô Chữ (Tập 1) cho bé nhà mình vì khi đọc phần đọc thử mình rất ấn tượng với cách trình bày của quyển sách, rất sõ ràng và mạch lạc, gúp bé nhỏ dễ dàng yêu thích và tô tô vẽ vẽ trên những trang giấy này. Mỗi trang vở đều có những câu thơ thật ý nghĩa, ví dụ như trang đầu tiên khi bé mở ra được mẹ đọc cho câu thơ:
Ôi quyển vở mới tinh
Em viết cho sạch đẹp
Chữ đẹp là tính nết
Của những người trò ngoan
Nghe mẹ đọc xong câu thơ bé cũng biết ý thức giữ gìn quyển vở hơn. Mình rất hài lòng về sản phẩm
5
742275
2015-08-31 07:01:31
--------------------------
215019
7961
Giúp các bé chuẩn bị vào lớp 1 làm quen với chương trình học tiểu học.

Nội dung gần gũi với chương trình giáo khoa và hình minh họa sinh động, bộ sách sẽ giúp các bậc cha mẹ rèn luyện cho con trẻ những nét chữ đầu tiên một cách hiệu quả. Để chuẩn bị vào lớp một, các em cần làm quen và sử dụng thành thục với các chữ cái, các con số từ 1 đến 10, và bước đầu biết viết và ghép vần những chữ cơ bản. Hôm nay em mới mua cuốn này, em cảm thấy rất hạnh phúc và khá hài lòng vì chất lượng mực in rất rõ và giấy lại cực kỳ mướt tay luôn các bạn ơi. Nội dung sách bám sát vào Sách giáo khoa của các bé. Rất hữu ích nha
5
656021
2015-06-25 19:06:57
--------------------------
