401990
5488
Poster "Cây phẩm cách tích cực" phải nói là một sáng tạo hay.
Việc sắp xếp các tính cách theo gốc, cành và sang những nhánh nhỏ như là cách hiểu  tính cách này là điểm tựa cho tính cách khác.
Mình tâm đắc tính cách gốc rễ nhất chính " Yêu thương", và có yêu thượng thì có lòng "tự trọng" - tức là yêu thương chính bản thân mình,..
Nhưng mình hơi bị khó hiểu với một số tính cách trên cây, có lẽ do bản dịch tiếng Việt và tiếng Anh như tính cách "ngọt ngào" chẳng hạn. Mình cũng chưa hình dung được tính "ngọt ngào" là tính như thế nào.
Đề nghị Poster dành mặt sau để in cây phẩm cách nguyên bản bằng tiếng Anh hoặc kèm lời giải thích ngắn gọn cho các tính cách trên cây để người đọc dễ hình dung ra. Thêm phí cũng không vấn đề với người đọc đâu.
3
429557
2016-03-21 16:41:36
--------------------------
252929
5488
Phải thật sự công nhận rằng “Poster Cây Phẩm Cách Tích Cực” là một sản phẩm rất độc đáo. Mới đầu nhìn thoáng qua cứ tưởng sơ đồ tư duy gì đó, nhưng không phải chỉ có vậy. Nó đưa người xem quay lại với thế giới yên bình của chân tâm trong một xã hội còn nhiều thử thách của cuộc sống. Nhìn vào đó mỗi ngày như là một sự nhắc nhở nhẹ nhàng, là con người phải sửa những cái hạn chế, thường xuyên học hỏi điều hay, cố gắng sống hoà đồng và phát triển tinh thần nhân văn của mỗi cá nhân hơn. 

5
544151
2015-08-04 00:31:47
--------------------------
218532
5488
Tuy là một poster đơn giản, nhưng treo nó trong phòng, mỗi ngày nhìn vào ta lại phát hiện ra chính trong con người mình có những đức hạnh, những phẩm chất tích cực. Tôi dùng nó mỗi ngày để hành thiền, mỗi khi bước vào ngồi thiền, tôi lại quan sát một phẩm chất và quán tưởng, mỗi ngày một ít nhưng giá trị mà poster này mang lại rất lớn cho bản thân tôi.
5
204853
2015-06-30 17:32:54
--------------------------
