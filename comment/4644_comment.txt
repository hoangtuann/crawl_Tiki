546633
4644
Đây là lần đầu mình đọc truyện của Shannon McKenna. Truyện rất hay, kết hợp cả yếu tố trinh thám và tình cảm nên rất lãng mạn nhưng cũng kịch tính. Bìa sách thiết kế đẹp.
5
361461
2017-03-18 12:33:10
--------------------------
349025
4644
So với “Sau những cánh cửa đóng kín” thì cuốn này theo tôi hay hơn hẳn, là một sự khác biệt lớn so với cuốn trước. Mặc dù câu chuyện tình lãng mạn của Connor và Erin diễn ra cũng khá hấp dẫn, cũng có những cảnh HOT đặc trưng trong văn phong của McKenna nhưng truyện không quá đào sâu những cảnh nóng bỏng dày đặt khiến người đọc ngán ngẩm nữa mà tập trung chủ yếu vào trinh thám, vì thế giúp câu chuyện trở nên lôi cuốn, kịch tính hơn. Tâm lý tội phạm trong truyện được tác giả khắc họa rõ nét, trong mỗi một tên tội phạm đều hiển thị một suy nghĩ khác nhau và cách thức hành động cũng điên rồ không kém, điển hình là Novak, hắn không còn là người nữa mà như là hiện thân của một con quỷ. Novak là nỗi ám ảnh lớn của cả Erin lẫn Connor nhưng bằng mọi khả năng của mình, họ đã chiến thắng được kẻ thù đồng thời càng gia cố cho tình yêu của mình thêm vững chắc.

Là một sự kết hợp rất tuyệt giữa trinh thám và lãng mạn, theo tôi McKenna đã thật sự đột phá trong tác phẩm này, truyện nhiều tình huống gay cấn, hồi hộp và khó đoán biết trước được những gì sắp diễn ra. Nội tâm của cả Erin và Connor được xây dựng tốt với những tổn thương mà cả hai từng mắc phải và cả sự kìm nén trong tình yêu mỗi khi họ phải đối diện với nhau. Nhìn chung thì trừ bìa sách nhìn như truyện kinh dị, truyện ma ra thì tác phẩm này hoàn toàn đáng đọc.

4
41370
2015-12-07 15:28:11
--------------------------
323385
4644
Nói chung những tác phẩm của Shannon McKenna nội dung rất hay, không thể bàn cãi. 
Tuy nhiên có một số đọan tôi cảm thấy rất chưng hửng, thiếu một cái gì đó mà không giải thích được, mạch chuyện không hề mạch lạc ở những đoạn hai nhân vật thể hiện tình cảm với nhau, tôi khác thất bại vọng cho dù cuốn 2 này được nhiều trang web nước ngòai đánh giá cao là hay hơn và nóng bỏng hơn nữa cuốn 1 - Đằng sau những cách cửa đóng kín.
Sau này bạn tôi mới cho hay là Bách Việt đẫm cắt bỏ hầu hết những cảnh nóng của câu chuyện. Tôi cảm thấy làm thế là không tôn trọng người đọc và đặc biệt là người viết. Tác giả đã bỏ ra biết bao công sức mới có được những hàng chữ quý giá ấy, đó là đứa con của họ, là thành quả tư duy của họ vậy mà Bách Việt nói cắt là cắt. Nếu cuốn này quá nóng bỏng, BV có thể warning 18+ ở ngòai bìa chứ họ không nên cắt bỏ nội dung cuốn sách. Ở Việt Nam còn xuất bản sách khiêu dâm 50 shades, thì tại sao một cuốn sách hay như thế lại bị cắt xén mổ xẻ thành một tác phẩm bị xem là lãng mạn xẹt chả ra đâu vào đâu.
2
108098
2015-10-18 16:25:14
--------------------------
215357
4644
Đây là tác phẩm đầu tiên của McKenna mà tôi được đọc. Nội dung tình cảm pha hành động tội phạm, và không thiếu cảnh nóng bỏng. Mối tình đơn phương kéo dài 10 năm của hai người vốn có cảm tình với nhau, làm nhiều chuyện cho nhau nhưng không dám nói thật lòng mình. Phải trải qua nhiều chuyện mất mát mới đến được với nhau. Ngoài ra có nhiều cảnh hành động gay cấn làm cho truyện không nhàm chán. Sau cuốn này tôi đã tìm đọc lại Sau cánh cửa đóng kín cùng phong cách, cùng thể loại. Với fan tiểu thuyết lãng mạn như tôi thì McKenna đã nằm trong danh sách yêu thích rồi.
4
35521
2015-06-26 11:01:29
--------------------------
167223
4644
Nếu bạn là người thích những cuốn tiểu thuyết trinh thám, hình sự, xen lẫn lãng mạn thì Shannon McKenna sẽ không bao giờ khiến bạn thất vọng. 
MÌnh bị cuốn hút bởi tác giả này từ cuốn "sau những cánh cửa đóng kín", cùng series với "đứng trong bóng tối". 
Câu chuyện có mở đầu cực kỳ hợp lý và chi tiết, miêu tả nội tâm, tâm lý nhân vật chính diện cũng như phản diện rất chi tiết và thú vị, kể cả các nhân vật phản diện, tác giả cũng đi sâu vào khai thác tâm ký, kể cả sự "điên loạn" của   Novak và gã tay sai Georg.
Tuy nhiên, đến phần kết thúc có hơi "lãng xẹt" khiến mình có chút chưng hửng. Nhưng đây sẽ là một tác phẩm khiến bạn không thể rời mắt được cho đến những trang cuối cùng.
4
125574
2015-03-14 12:12:31
--------------------------
