165085
5692
Tuy học khối ngành xã hội nhưng tôi rất say mê khoa học, đặc biệt là sinh học và cổ sinh vật học. Quyển sách này rất ấn tượng với tôi ở lối trình bày. Bìa sách cứng, các trang được in màu nên rất hấp dẫn, sống động, phù hợp với các đối tượng độc giả là người lớn lẫn trẻ nhỏ. Thông tin trong sách rất bổ ích. Nét hấp dẫn lớn nhất của sách là các trang in 3D và kính 3 D tặng kèm, khiến người đọc thấy gần gũi, như chính mình được tiếp xúc với các quang cảnh trong sách vậy.
5
515736
2015-03-09 17:51:16
--------------------------
