521421
7798
Khi nhận được cuốn sách thì mình đã thấy nó vô cùng đáng yêu rồi, nhỏ nhắn , xinh xinh phù hợp để đem theo đọc khi ngồi xe bus. Nội dung thì cũng khá hay, các mẫu truyện ngắn vô cùng đáng yêu và thực tế.
5
1863081
2017-02-08 16:21:48
--------------------------
415681
7798
Sách khá là hay, lời văn dí dỏm. Câu từ quen thuộc.
Tuổi trẻ mơ mộng quá đáng nhưng rất đáng yêu. Đôi khi suy diễn quá đáng, tưởng tượng quá đáng, và cũng sĩ diện quá đáng vậy nên dễ khiến người đọc cảm thấy.. muốn đọc tiếp để xem như thế nào :P
Mình thích nhất là bìa sách, trơn bóng, hình ảnh lại đẹp rất là cute :))
Chữ lại in rõ ràng to và có màu ( kích thích việc đọc ) Và nội dung lại rất quen thuộc, tình yêu tuổi học trò trong sáng ngay thơ nhưng rất đẹp.
3
1171197
2016-04-13 21:19:00
--------------------------
399296
7798
Mua cuốn sách này là lựa chọ đúng đắn của mình. Tuy lúc đầu có hơi thất vọng vì cuốn sách này vừa mỏng, vừa bé nữa chứ. Lúc ấy mình đã rất sót cho cái túi tiền của mình . Nhưng không mình đã không cảm thấy hối hận khi mình đọc xong cuốn truyện này . bằng giọng văn lôi cuốn, tác giả đã cho mình cảm nhận được thật nhiều các cung bậc cảm xúc khác nhau của tình yêu tuổi mới lớn. thật sự câu truyện này rất phù hợp với độ tuổi của mình!Mình nghĩ các bạn còn đang ở độ tuổi teen thì lên mua cuốn truyện này để đọc. Cảm ơn!
4
568619
2016-03-17 15:34:19
--------------------------
395992
7798
Mình rất thích quyển sách này, bìa được thiết kế đơn giản với màu tím mộng mơ nhưng rất đẹp và dễ thương
 Tuổi 18 có tình yêu không?
Câu trả lời là...
Trời đang nắng...bỗng đổ mưa
Tình yêu tuổi học trò hồn nhiên và trong sáng là đề tài khá gần gũi với lứa tuổi của mình
 Mình thích nhất là truyện ngắn: Tình yêu 18 đặc biệt là sự hồn nhiên và lém lỉnh của cô gái đó
 Điều mình không thích ở cuốn sách này là khổ sách hơi nhỏ so với giá bìa nhưng mình mua đợt giảm giá nên cũng đỡ
4
1000744
2016-03-12 19:58:07
--------------------------
395045
7798
Yêu - một động từ, danh từ quen thuộc trong cuộc sống của chúng ta, ai cũng có những mối tình đẹp cho riêng mình. Cuốn sách ngàn lẻ một độ yêu với những câu chuyện ngắn về những tình yêu ngây thơ trong sách tuổi học trò, có những mối tình   ngây ngô nhưng nhiều lúc lại mau chóng chán để qua những câu chuyện ngắn đó người đọc có thể cảm nhận nhiều mối tình với nhiều khung bậc cảm xuc khác nhau. Cuốn sách có bìa đẹp, nho nhỏ dễ thương, giá thì hạt dẻ và hợp lí.
4
851304
2016-03-11 12:21:18
--------------------------
291875
7798
Bộ Sách Ngàn Lẻ Một Độ - Yêu của Tác giả Phạm Vũ Ngọc Nga đối với mình quả là cuốn sách rất đáng đọc. Đầu tiên mình ấn tượng với bìa sách, rất teen, rất long lanh và thể hiện được nội dung của cuốn sách. Nhưng mình mê nhất là những câu chuyện. Với giọng văn lôi cuốn, tác giả đưa mình đi hết những cung bậc Yêu của tuổi mới lớn. Mình nghĩ có lẽ không chỉ những câu chuyện xuất sắc mà tác giả đã đánh trúng tâm lí bạn đọc. Rất hấp dẫn! Đã là học trò, đặc biệt bạn nữ sẽ rất cưng cuốn này cho xem!
 

5
349220
2015-09-06 20:15:49
--------------------------
249373
7798
Cuốn Bộ Sách Ngàn Lẻ Một Độ - Yêu thực sự đã làm thỏa mãn mình! Cuốn sách có nhiều điểm mà mình rất thích. Thứ nhất là về thiết kế: Bìa của cuốn sách được thiết kế phải công nhận là rất đẹp, hình ảnh dễ thương và rất teen. Thứ hai là về nội dung. Đọc xong 160 trang sách mình như không muốn dứt ra vậy! Những mẩu truyện mà tác giả Phạm Vũ Ngọc Nga mang tới cho đọc giả thực sự rất thú vị. Đó là những câu chuyện tình yêu tuổi mới lớn thật trong sáng và cũng thật ngây ngô! Mình rất thích cách viết truyện của tác giả rất biết cách xây dựng hình tượng nhân vật ở lứa tuổi này, lối miêu tả ấn tượng, dễ hình dung và kích thích trí tưởng tượng của mình, cả về tâm lí về lứa tuổi mới lớn cũng rất ấn tượng! Mình chưa yêu bao giờ mà đọc xong cuốn sách này, mình cứa như vừa trải qua một cuộc tình ngây ngô vầy!
5
306288
2015-07-31 15:53:21
--------------------------
239823
7798
Tình yêu à? Mình chưa bao giờ yêu và chẳng biết nó ra sao. Nhưng những câu chuyện tình học trò trong trẻo lại đọc rất nhiều. "Ngàn lẻ một độ - Yêu" là cuốn sách mình thích nhất trong cả bộ. Những truyện về tuổi học trò nghịch ngợm, trong veo như nắng sớm, ngọt lịm như que kem. Nó có một cái gì đấy rất thân quen, phải chăng tuổi ấy bao giờ cũng vậy? Cảm thông với câu chuyện về cô nàng khuyết tật, con người ta đâu phải cứ hoàn hảo thì mới có một tình yêu đẹp, phải không?
5
543425
2015-07-24 09:18:35
--------------------------
217861
7798
Tình yêu của tuổi mới lớn. Rất trong sáng, hồn nhiên và ngây thơ. Những cuộc hẹn hò, những lời tỏ tình, những sự nhớ nhung, sự giận dỗi và cả những suy nghĩ của bản thân về người mình thích đều thể hiện sự trong sáng, ngây thơ và hồn nhiên của tuổi học trò. Tình yêu đầu thường không thành nhưng nó luôn là tình yêu đáng nhớ nhất của cuộc đời. Lời văn nhẹ nhàng, chân thật và tình cảm, cứ như tác giả chỉ mới bằng tuổi mình vậy. Về hình thức, khổ sách bé, dễ cầm và mang đi, chất liệu giấy cũng dày, mịn và sáng. Một cuốn sách dễ thương và nên đọc.
4
418341
2015-06-30 09:17:19
--------------------------
165095
7798
Sau bao lần cảm nắng, bao cuộc hẹn hò, bao nỗi nhớ nhung, hai người yêu nhau như đời vẫn thường như thế. Tình yêu là phút giây mầu nhiệm nhất và đẹp nhất trong đời người, nhất là khoảng thời gian đi học. Chưa bao giờ, tình yêu đi học lại ngập tràn hạnh phúc, bình yên và yêu thương nhiều đến như vậy. Có lẽ, nhà văn đã từng yêu thật nhiều nên thấu hiểu.
Lời văn nhẹ nhàng và mộc mạc nhưng cách xưng hô anh-em có phần hơi người lớn nên làm mất đi sự trong sáng của cả bộ truyện. Thật lòng mà nói, trong bộ bốn cuốn, cuốn này là tệ nhất.
3
198460
2015-03-09 18:31:59
--------------------------
