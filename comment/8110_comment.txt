566129
8110
Cuốn sách rất hay; sách tuy ngắn nhưng những gì tác giả gửi gắm là vô tận; nội dung xuyên suốt thống nhất, cách viết của tác giả hơi lạ vì mình đã quen đọc văn phong hiện đại cũng như có tìm hiểu về triết học phương Tây; vì thế khi đọc tác phẩm này cảm thấy những cái khó hiểu của triết học phương Tây được giải thích trong sách rất rõ ràng. Cách tác giả giải thích những vấn đề, triết lý không rõ ràng/mơ hồ bằng những ví dụ, điển tích khiến mình rất dễ hình dung. Tuy nhiên, có nhiều phần tiếng Pháp và Hán Việt nếu được nhà xuất bản chú thích rõ hơn thì càng tuyệt.
5
138110
2017-04-06 19:27:41
--------------------------
544601
8110
Sách có thể rất ít người đọc nhưng khi đọc rồi mới thấy được giá trị nội dung hơn hẳn nhiều loại sách hiện tại
5
34718
2017-03-16 11:46:14
--------------------------
477785
8110
Một cuốn không dễ đọc chút nào, có những trang sách tôi phải đọc đi đọc lại vài lần, có những câu tôi phải lên mạng tìm nghĩa của nó để dễ hiểu hơn. Song tôi thực sự rất thích, rất ấn tượng với cuốn sách này. Nó khiến ta suy ngẫm nhiều hơn, sự suy ngẫm đó phải gắn vào thực tế cuộc sống, ta phải gắn các quy luật vào tình huống cụ thể để kiểm chứng xem nó có đúng hay không. Cuốn sách bước đầu rèn cho tôi về cách mà ta nhìn nhận sự việc, hiện tượng - 1 cái nhìn tổng quát, đa chiều hơn. Thật may mắn khi biết đến cuốn "nhập môn triết học đông phương" và sở hữu nó.
3
913862
2016-07-28 21:17:37
--------------------------
456614
8110
Sách này nêu lên những điểm đặc trưng, cơ bản của nên Triết học Phương đông, nó khác xa với kiểu triết học phương Tây, Từ đó cũng hình thành nên những phương thức tiếp cận khác nhau. Phương Tây thì tập trung phân tích, chia nhỏ từng thứ ra mà quên mất sự tổng hợp và hài hòa của tự nhiên. Còn phương đông lại dùng hình thức Tổng hợp nên thường mang tính phổ quát, rất chung chung. Nếu như kết hợp và ứng dụng hai nền triết học này, thì sẽ có được một triết tuệ minh triết nhất.
5
125876
2016-06-23 16:05:27
--------------------------
349480
8110
Cuốn như là một hiền tài đem đến người đọc sự giải thích về nguồn gốc con người khai phá được tâm hồn , loại bỏ đi cái tôi cao ngạo để hòa thuận với đồng loại của mình , triết lý không thể tháo rời hiện thực , mình thấy cái mà tác giả thấy được khai thác từ chính con mắt toàn bộ kinh nghiệm , học hỏi từ cuộc sống nhiều , một trong những triết lý lâu đời của văn minh nhân loại hàng ngàn năm đưa lại sự ngưỡng mộ đến thế giới , chính xác cách biểu lộ .
5
402468
2015-12-08 15:54:04
--------------------------
268027
8110
Giống như tựa sách, "Nhập Môn Triết Học Đông Phương" giới thiệu một cách bao quát và hệ thống về tư tưởng chủ đạo của phương Đông. Ngoài ra còn có những chia sẻ rất bổ ích của tác giả (có thể tiết kiệm được rất nhiều thời gian cho những người đang bậm bõm tìm hiểu). Mình rất thích văn phong của cụ Thu Giang, khách quan và cẩn trọng, và nhất là thâm thúy, đọc càng nhiều lân càng thấy thấm thía và phát hiện ra nhiều triết lí mới. Bên cạnh đó, cụ Nguyễn Duy Cần là một trí thức tiến bộ thời đó nên dù qua một thế kí nhưng cách giảng dạy, chỉ dẫn rất hiện đại, không chỉ bưng lý thuyết mà gợi mở, thứ mà rất nhiều tác gia về ngành huyền học thiếu. Cuốn sách nhỏ xinh và không hề dễ nhai, nhưng xứng đáng.
5
292321
2015-08-15 23:46:04
--------------------------
248685
8110
Một quyển sách hay, giúp tóm tắt được những nội dung chính của các học thuyết triết học ở Phương Đông. Nếu bạn nào có ý định nghiêng cứu sâu về triết học thì mình nghĩ đọc cuốn này là thích hợp. 
Cuốn sách cung cấp những thông tin cơ bản nhưng khá đầy đủ. Đọc xong quyển này thì việc tiếp cận những quyển chuyên sâu sẽ dễ hơn, mà khi cần tra cứu thì cũng dễ dàng.
Chỉ một điểm là do sách được viết khá lâu rồi nên nhiều khi cách tác giả dùng từ hơi khó hiểu, nhưng nhận xét chung của mình thì quyển này hay, bổ ích và thú vị.
5
50322
2015-07-31 10:10:39
--------------------------
231220
8110
Mình đã tự hỏi tại sao cuốn sách này mỏng mà giá quá đắt như vậy? khi mua mình cũng đã đắn đo suy nghĩ nhiều. Nhưng giá trị là ở nội dung chứ không đo ở sự mỏng dày được, để viết một cuốn sách triết học nhập môn cho đại chúng đọc để mà hiểu mà thích thú không phải dễ, và Cụ Cần đã làm được điều đó. Nói thật sự mình không có thời gian để mà nghiền ngẫm hết cuốn lão trang nguyên bản. Biết rắng nó hay nhưng với thời gian ít ỏi mình chỉ mong hiểu được cái căn bản của triết học để mà nhìn cuộc sống tốt hơn thôi. Trước nay mình tôn sùng triết học phương Tây hơn, nhưng qua cuốn sách này mình mới thấy triết học Phương Đông hay đến dường nào. Thầm cảm ơn cụ đã để lại một cuốn sách hay và bổ ích.
5
74132
2015-07-17 22:32:21
--------------------------
185287
8110
Trước khi đọc cuốn sách này, tôi luôn luôn có tư duy “hâm mộ” tư duy Tây phương hơn là tư duy Đông phương bởi đơn giản, tôi thấy phương Tây họ đạt được nhiều thành tựu về khoa học hơn Đông phương. Những triết gia lỗi lạc mà tôi hâm mộ như Socrates, Platon, Kant, Bacon, Hegel... toàn là những triết gia phương Tây. Các nhà Toán học, vật lý học mà tôi hâm mộ cũng toàn là người Tây hết: Pascal, Einstein, Euclide, Pythagore, Galileo, Menden…Tuyệt đại không có một cái tên phương Đông nào hết trong tâm trí của tôi. Từ trước đến nay, tôi luôn thấy tư tưởng triết học Phương Đông như một cái gì đó già cỗi và lỗi thời, không được trẻ trung, sáng sủa và cách tân như tư tưởng triết học Tây phương. Nếu có cơ hội đi nước ngoài, tôi luôn luôn mong muốn được đến phương Tây, đến Mỹ. Nhưng cuốn sách này giống như một gáo nước lạnh táp vào mặt tôi, khiến tôi bừng tỉnh. Qua những phân tích hết sức thâm sâu của tác giả, tôi thấy kiến thức về triết học của tôi còn quá non kém và phiến diện. Cuốn sách buộc tôi phải nhìn nhận lại cũng như tìm hiểu sâu hơn về tư tưởng văn hóa Đông phương, cái nôi mà tôi được sinh ra. Cảm ơn tác giả, cảm ơn nhà xuất bản về cuốn sách tuyệt hay này.
5
594760
2015-04-19 10:17:32
--------------------------
