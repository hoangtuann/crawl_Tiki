190168
8426
Dưới bóng cây sồi là quyển sách văn học dân tộc tuyệt vời. Tôi tình cờ đọc được quyển sách này ở một thư viện và thật sự bị cuốn hút bởi lối văn mộc mạc, chân chất nhưng đầy tính sáng tạo của tác giả. Bằng lối văn miêu tả đặc sắc, tác giả tạo cho ta cảm giác đang sống giữa núi rừng, được mẹ thiên nhiên ôm lấy và che chở. Tác phẩm đưa ta đến vùng Lao Chải- một vùng xa xôi của tổ quốc, nơi đó có một tình yêu đẹp giữa chàng trưởng thôn - Phù và cô gái mang trong người "dòng máu màu đen" - Kim. Các tác phẩm của Đỗ Bích Thúy tập trung miêu tả người phụ nữ miền cao dù trải qua bao nhiêu bão táp, trải qua miệng lưỡi thế gian và qua gánh nặng của cơm áo gạo tiền, họ vẫn giữ trong mình sức sống mãnh liệt, vươn lên tìm hạnh phúc. Nếu bạn muốn tìm hiểu về những giá trị xưa cũ của người dân tộc vùng cao hay muốn sống trong khung cảnh tình yêu đơn sơ của người miền núi, thì đây chính là cuốn sách bạn đang tìm kiếm.
4
615616
2015-04-28 22:26:01
--------------------------
