560309
6408
Sách nhỏ gọn. Nội dung tương đối phong phú. Tuy nhiên không biết vì lí do gì mà sách của mình có đến cả chục trang bị rách, vài trang bị bẩn nữa 🙁🙁🙁
4
1206236
2017-03-31 16:50:35
--------------------------
506209
6408
Từ điển Anh-Việt nhằm đáp ứng nhu cầu học tiếng Anh
4
2036080
2017-01-06 13:10:29
--------------------------
490365
6408
cuốn từ điển 75 nghìn từ này nhỏ gọn, khá tiện lợi cho các bạn mang theo khi đi học, đi vui chơi hay muốn giao tiếp 
5
1886579
2016-11-12 11:25:51
--------------------------
479234
6408
Cuốn sách nhỏ gọn tiện lợi, có thể tra bất cứ lúc nào. Từ mới có đủ phiên âm nghĩa rất đầy đủ từ loại của chúng. Cuốn sách thực sự thích hợp cho những người học Tiếng Anh
Từ khi mua cuốn sách tôi đã cải thiện hơn nhiều về khả năng nhớ từ vựng bởi vì tra nhiều lần từ điển cũng là một cách để nhớ từ hơn. Hơn nưa quyển từ đuển còn nhỏ gọn có thể mang theo bất cứ nơi đâu, bất cứ lúc nào ta quên từ thì đều có thể tra . Tôi khuyên các bạn nên mua nó về dùng.
4
1472148
2016-08-08 12:58:00
--------------------------
373819
6408
Cuốn này chỉ có 75k từ, khá là ít, nó không đáp ứng đủ yêu cầu của các bạn theo chuyên anh nhưng ngược lại, nó không quá dày, chỉ gồm những từ quan trọng và hay gặp vì thế mà tiện mang đi lại và tra từ nhanh hơn. Màu giấy sáng, trình bày chia hai cột rõ ràng, cỡ chữ vừa đủ, các từ tiếng anh được in đậm nên đọc rất thuận mắt.
Mình rất thích cuốn từ điển này. Trên giá sách của mình chỉ có 2 cuốn từ điển, 1 là cuốn này, 2 là cuốn 350k từ. Nhưng cuốn này được mình sử dụng nhiều hơn vì nó nhỏ gọn nên dễ mang tới lớp học!

4
507835
2016-01-24 19:44:59
--------------------------
355078
6408
Trước khi mua cuốn từ điển này mỗi lần học tiếng anh không hiểu là phải lên mạng tra cứu, mà mỗi lần tra cứu như vậy mình rất lười. Nhưng sau khi mua cuốn từ điển này mình không lười khi phải kiếm từ tiếng anh khó nữa, chỉ cần lật sách ra dò dò là thấy từ mình cần tìm.
Cuốn từ điển này rất hữu ích đối với mình. Từ ngữ dễ nhìn, dịch nghĩa dễ hiểu, có nhiều từ mới và thật sự rất rất hay. Mình mua quyển từ điển này mẹ mình rất hài lòng vì tích cực học tập, nhờ nó mà mình cũng kha khá tiếng anh lên. Tuyệt vời lắm!
5
880750
2015-12-19 09:41:52
--------------------------
203046
6408
Cuốn từ điển này không quá dày, giấy màu trắng nhìn khá sáng. Các từ được trình bày theo bảng chữ cái nên dễ dàng cho việc tra cứu các từ. Cỡ chữ nhỏ, các từ tiếng anh được in đậm nên dễ nhìn hơn. Số lượng từ 75000 từ thì hơi ít, chắc là nhà xuất bản chưa cập nhật đủ các từ  mà thôi, ngoài định nghĩa tiếng việt ra thì các ví dụ hay collocations hay thành ngữ hầu như không có. Cuốn này chỉ phù hợp cho những bạn không học chuyên sâu về tiếng anh thôi.
4
131255
2015-05-31 09:15:40
--------------------------
