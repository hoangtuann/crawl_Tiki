501077
11360
Đây là quyển sách đầu tiên tôi đọc của ông hoàng tiểu thuyết lãng mạn Mỹ - Nicholas Sparks, tuy nhiên ấn tượng không được sâu sắc lắm.
Trước hết vẫn phải nói, giọng văn rất nuột, rất tinh tế, hơn nữa miêu tả rất chọn lọc và rất hay. Đây là điểm chung của văn học phương Tây, tuy nhiên ông đã làm rất xuất sắc mảng này. Chuyện tình của Trav và Gabby, nói trắng ra, chính là "ngoại tình" =))) nhưng ngoại tình ở đây lại là tình yêu đích thực, là tìm được nửa kia của cuộc đời. Những sự kiện trong câu chuyện xảy ra rất chặt chẽ, thể hiện hơn cả sự cuốn hút mà là một tình yêu nồng nàn mà hai nhân vật chính đã dành cho nhau tựa hồ rất rất lâu trước đó. Nhân vật phụ, cũng như mọi người, tôi đặc biệt thích Stephanie cùng tính cách vừa tưng tửng lại vừa thông minh, sắc sảo của cô nàng. Các tuyến nhân vật khác cũng thể hiện tốt được vai trò làm bàn đạp cho những phần mấu chốt của câu chuyện.
Điều tôi không hài lòng là, về đoạn cuối của câu chuyện - đoạn ở thời điểm hiện tại, 11 năm sau. Đó chính là phân đoạn chính, phân đoạn cao trào của LCCTT, nhưng lại không để lại cho mình ấn tượng lắm. Căn bản motif ấy hơi quen thuộc quá, dù phần miêu tả tâm lý nhân vật và trình bày diễn biến tâm lý, những sự cân nhắc, những sự lựa chọn của NS có xuất sắc và tuyệt vời đến thế nào đi nữa. Phần kết cũng có chút dễ đoán. Nhưng nhìn chung, tác phẩm cũng khá hay.
Một điều thêm nữa, không biết là do người dịch hay nguyên tác mà một số câu văn cứ hơi bị thô, không nhã. Có một khúc tôi nhớ mãi là vẫn trố mắt ra khi nhìn thấy đoạn nói về nội tâm của Gabby. Cô ấy xưng là "bản cô nương"! Tôi thật sự thấy chỗ này không được ổn cho lắm.
Bên cạnh đó, phim chuyển thể của bộ này thật sự tệ đấy, tôi nghĩ vậy. Những phần hay nhất như phần đi dù lại bị cắt đi hết, và các sự kiện được bóp méo một cách có phần quá tay mà những người đã đọc sách trước đó không thể không nhíu mày.
3
640394
2016-12-27 19:55:23
--------------------------
462207
11360
Tác phẩm quá hay và lôi cuốn, khi bắt đầu đọc thì ta khó dừng lại. Mạch truyện dẫn dắt người đọc từ hiện tại về quá khứ rồi lại quay về với thực tại để hiểu rõ hoàn cảnh cũng như quyết định của nhân vật chính. Travis là 1 người chồng, người cha hoàn hảo cho đến ngày định mệnh đã làm thay đổi cuộc sống của gia đình anh và đẩy anh vào việc phải lựa chọn theo lý trí và nguyện vọng của Gabby, vợ của anh hay theo tiếng nói của trái tim. Dù lựa chọn đó là gì thì nó vẫn phản ánh tình yêu mãnh liệt của Travis dành cho Gabby. Đoạn kết đầy xúc động và chứng minh trái tim luôn có lý lẻ riêng để dẫn ta đến thiên đường hạnh phúc.
5
1057114
2016-06-28 13:10:33
--------------------------
451071
11360
Quyển này hay hết hàng quá, hóng mãi trên Tiki mới mua được. Sở dĩ mình chọn Lựa chọn của trái tim vì các sách khác của Nicholas Sparks toàn kết thúc buồn (hình như ông thích kết thúc buồn hay sao ấy) nên muốn thử đọc một tác phẩm có kết có hậu của ông xem sao. Mình rất thích cái cách Nicholas Sparks tạo ra tình yêu, thật nhẹ nhàng, lãng mạn, từ từ chậm rãi làm sao, không hề giống các tiểu thuyết Mỹ khác. Ẩn chứa trong các tác phẩm của Sparks, dường như đâu đó đều ẩn chưa một chút định mệnh - đứa hai người đến với nhau một cách thật tình cờ, tự nhiên. Điển hình là Travis và Gabby trong tác phẩm này, có thể nói cuộc sống của họ đã thay đổi hoàn toàn khi gặp nhau... Dù bao nhiêu sóng gió đã xảy ra nhưng cuối cùng họ vẫn ở bên nhau, không hiểu sao khi đọc đến đoạn này cảm thấy thật ấm lòng. 
5
415536
2016-06-19 13:39:26
--------------------------
445545
11360
Đây là một trong những tác phẩm tôi thích nhất của nhà văn Nicholas Sparks. Cuốn sách vô cùng nhẹ nhàng, cảm động, ý nghĩa và đầy tính nhân văn. Bìa sách rất đẹp và lôi cuốn, giấy trắng chữ dễ nhìn không bị nhòe và có lỗi in ấn. Gấp trang sách lại có lẽ ta vẫn không thể quên được hình ảnh các nhân vật mà tác giả đã xây dựng. Cái kết vô cùng bất ngờ khiến người đọc phải tuôn trào nước mắt vì xúc động. Tuy cuốn sách được cho là nhẹ nhàng nhưng nó không hề kém phần lôi cuốn!
5
1075734
2016-06-10 14:02:51
--------------------------
425140
11360
Tình yêu đến với hai nhân vật một cách tình cờ. Họ cùng nhau tạo ra những khoảnh khắc tuyệt vời trong cuộc sống. Nhưng để giữ cho tình yêu đó tồn tại trước những biến cố của cuộc đời thì không phải ai cũng làm được. Sách rất hay, nhẹ nhàng miêu tả từng diễn biến trong tâm lý của nhân vật. Tình yêu không phải lúc nào cũng màu hồng,  Đây là một câu chuyện tình vượt qua khó khăn và sóng gió. Nó nhắc chúng ta nhớ rằng: Bằng cách nào đó tình yêu luôn tồn tại giữa cuộc đời này.
4
1224559
2016-05-03 20:22:37
--------------------------
412728
11360
Mình đã rất quen với lối viết của Nicholas Sparks, nhẹ nhàng nhưng tinh tế và sâu lắng. Chuyện tình của Travis và Gabby cũng vậy, vui cũng có mà buồn cũng không thiếu. Thích nhất là đoạn Gabby hôn mê gần 3 tháng trời. Trong khoảng thời gian ấy, Travis đã chứng minh được tình yêu và nghĩa vụ của dành cho Gabby. Anh vừa phải đảm nhận trách nhiệm làm cha và làm mẹ cho 2 đứa con, dành thời gian ở bên cạnh nói chuyện với Gabby và không ngừng cầu nguyện cho cô hồi phục lại. 
5
923880
2016-04-08 10:56:38
--------------------------
360297
11360
Đây quả thật là mội câu chuyện cảm động, sâu sắc. " chúng ta nên đi xa tới đâu nhân danh tình yêu. Chỉ riêng hy vọng thôi có đủ để kéo dài sự sống?" . Một câu hỏi ko phải ai cũng có thể trả lời đúng hay đủ. 
Một câu chuyện nhẹ nhàng, cảm động sâu xa. Truyện của Nicholas Sparks luôn có một chút gì đó như điềm báo, những dấu hiệu thể hiện tình yêu sẽ tạo ra những điều kỳ diệu. Mình đã đọc gần hết những tác phẩm của ông, xem phim được dựng lại từ sách. Thật sự thích sự nhẹ nhàng lôi cuốn trong văn của Nicholas Sparks
Bìa sách đẹp. Giấy đẹp. Không có lỗi chính tả.
5
469423
2015-12-28 22:26:50
--------------------------
325810
11360
Giọng văn thật nhẹ nhàng, nhưng cũng dễ dàng lôi cuốn bất cứ ai. Ấn tượng với một câu hỏi không dễ có câu trả lời của Travis " Người ta có thể đi xa đến đâu nhân danh tình yêu". Một câu chuyện thật sự theo đúng tựa đề của tác giả đã đặt nên " Lựa chọn của trái tim". Giữ đúng lời hứa với người phụ nữ anh yêu nhất, quan trọng nhất trong đời anh hay là làm trái lời Gabby vì dù sao anh vẫn cần cô với bất cứ hình thái nào?? Phải chăng khi cuộc sống đẩy chúng ta vào một tình thế khó khăn thì hãy để trái tim lên tiếng? Dù chỉ đọc sách mà vẫn có thể cảm nhận được khung cảnh tuyệt đẹp nơi Travis và Gabby đã xây nên tình yêu thật đẹp của họ (Nicholas quá tuyệt trong phong cách miêu tả cảnh như thế này ^^)
4
467885
2015-10-24 11:35:06
--------------------------
324418
11360
Cuốn sách này là một sự khác biệt của N.Sparks, khác ngay từ cái bìa không giống với đa phần những cuốn sách trước và khác ở nội dung, một cốt truyện hấp dẫn từ đầu đến cuối, mang nhiều nét hài hước dí dỏm và một cái kết hài lòng, có thể xem là hạnh phúc. Vẫn là lời văn nhẹ nhàng, sâu lắng, vẫn là vùng quê Carolina quen thuộc, câu chuyện diễn ra khá sôi nổi với hai hàng xóm cạnh nhà nhau Gabby & Travis, thực sự ban đầu thì tôi lại ấn tượng với hai con chó mà hai người nuôi hơn, chính nhờ chúng mà cả hai mới có dịp gặp gỡ nhau, cũng do chúng mà mới phát sinh rắc rối, tranh cãi giữa hai người và cuối cùng cũng nhờ chúng mà tình yêu mới đến với họ. 

Lúc đầu tôi tưởng rằng cuốn sách sẽ có kết thúc giống như bao truyện khác của N.Sparks, buồn và tiếc nuối vì khi đọc phần mở đầu truyện, tôi nhìn thấy nỗi tuyệt vọng trong con người của Travis, anh hoàn toàn bất lực trước căn bệnh của Gabby và chỉ cần một quyết định sai lầm anh sẽ mất cô mãi mãi, nhưng anh đã lựa chọn đúng, sự lựa chọn đã đưa Gabby trở lại với anh. Travis thật tuyệt vời, một người đàn ông hết lòng yêu thương vợ, đã hy sinh rất nhiều để ở bên cô. Cuốn sách này đưa tôi đi từ cảm xúc này đến cảm xúc khác, từ vui cười mà trở thành buồn thương lúc nào không hay, nhưng sau cùng thì lại là một sự ấm áp nhẹ nhõm trong tim, đây là một trong những quyển mà tôi hài lòng nhất của N.Sparks vì tôi thích một đoạn kết có hậu hơn, trong khi N.Sparks lại khá thích những cái kết buồn.

4
41370
2015-10-21 09:30:36
--------------------------
262078
11360
Trong sự lựa chọn của trái tim Nicholas Sparks quả thực không làm tôi thất vọng mà còn ngược lại. Nhẹ nhàng mà không kém phần lôi cuốn là những từ là tôi dành cho cuốn tiểu thuyết này. Nó thực sự chạm đến trái tim của người đọc như tôi qua từng trang giấy. Tiểu thuyết hiện đại phương tây bao giờ cũng chân thực và đó chính là điều tôi thích khi chọn những cuốn truyện để đọc. Phần đầu truyện kể về cuộc gặp gỡ của Travis và Gabby họ quen nhau rồi yêu nhau bất chấp cô đã có người yêu. Để rồi đến 11 năm sau anh phải đưa ra một sự lựa chọn thực sự khó khăn với cuộc đời anh. Ta nên đi xa tới đâu nhân danh tình yêu? Chỉ riêng hy vọng thôi có đủ kéo dài một sự sống? 
5
613896
2015-08-11 17:00:19
--------------------------
216960
11360
Lần đầu tiên đọc truyện của Nicholas Sparks tôi khá ấn tượng về cách viết của ông. Nó nhẹ nhàng và êm dịu như dòng suối từ từ rót vào người đọc những cảm xúc khác nhau rất kì diệu. Cuốn sách này cũng không phải là ngoại lệ với cái cách viết ây. Đó là tình yêu giữa hai con người xa lạ Travis và Gabby bằng cách nào đó họ đã đến với nhau và xây dựng nên mối tình thật đẹp nhưng cũng không ít trắc trở. Tuy nhiên những khó khăn ấy cũng sẽ qua đi một khi mà con người ta có niềm tin vào bản thân và vào tình yêu của chính mình.
4
133796
2015-06-28 15:35:30
--------------------------
210084
11360
Vẫn là văn phong nhẹ nhàng, giản dị mà lay động lòng người quen thuộc của Nicholas Sparks, ông hoàng tiểu thuyết lãng mạn người Mỹ lại dẫn dắt người đọc đến với một câu chuyện tình yêu khác, trong đó hai con người đã tìm đến nhau như một định mệnh, hai tâm hồn hòa quyện vào nhau trong một sự đồng điệu kỳ lạ đến bất ngờ, hai trái tim quấn quýt lấy nhau trong những cảm xúc thăng hoa và mãnh liệt nhất. Tình yêu giữa Travis và Gabby đã mang đến một sức mạnh phi thường, sức mạnh đã đưa Gabby trở lại với thế giới, và còn là niềm tin tưởng mãnh liệt vào một kết thúc có hậu cho cuộc đời của cả hai. Một cuốn sách hay và xúc động.
5
122350
2015-06-18 21:23:47
--------------------------
164180
11360
Tôi vẫn luôn bị thu hút bởi lối viết của Nicholas dù nó khá là đơn giản, nhẹ nhàng. Tình yêu trong tiểu thuyết của ông hầu hết chỉ xuất phát từ đời thường, những con người bằng một cách nào đó gặp nhau, để rồi nhận thấy người kia mới chính là người mình cần ở bên cạnh. Mỗi lựa chọn đều khiến chúng ta trăn trở băn khoăn, đặc biệt khi nó là lựa chọn từ trái tim.
Thực ra trái tim phức tạp mà cũng rất đơn giản, nó chỉ an yên bên cạnh người mà bạn yêu thôi.
Gabby đã để trái tim mình dẫn lối cho cô đến với Travis, và tôi tin cũng chính nó đã mang cô trở về với anh lúc cận kề cái chết.
Một câu chuyện đáng để đọc cho những trái tim đang yêu, đã yêu và sẽ yêu :)
5
28320
2015-03-06 23:50:35
--------------------------
100574
11360
Tình yêu - đôi khi là điều đến một cách tình cờ. Nó giống như một sự sắp đặt cố ý mà Thượng Đế chỉ chờ thời điểm cho một khởi đầu mới. 

Cuộc sống của Travis và Gabby dường như thay đổi trong ngày đầu tiên họ gặp nhau. Giống như định mệnh, bằng một sợi dây vô hình nào đó khiến người kia cảm nhận đươc sự cần thiết và quan trọng của người còn lại trong quãng đời của mình. 

Cuốn sách là một thiên tình cảm nhẹ nhàng, lãng mạn, lên xuống theo từng bước chân của những nhân vật trong đó. Đó là cách để nhìn nhận tuổi trẻ, để sống một cuộc sống trọn vẹn, cách để người ta tìm thấy hi vọng trong cơn tuyệt vọng bỉ cùng. Có những điều cần đến những sự dũng cảm và tình yêu chân thật. Đến cuối cùng, khi niềm hi vọng mang đến một tia sáng rõ ràng, người đọc có thể sẽ phải rớt nước mắt vì quá hạnh phúc cho Travis, cho Gabby. Sinh tử là điều đau lòng nhất trong một đời người. May mắn siết bao khi đến cuối cùng "Lựa chọn của trái tim" vẫn là một thiên truyện kết thúc có hậu.

Một quyển sách hay về tinh yêu.
4
18814
2013-11-26 14:42:07
--------------------------
