360543
3143
Trước khi đọc “Cuộc phiêu lưu của Ngón Cái” tôi không đánh giá cao tác phẩm. Cầm cuốn sách mỏng trên tay tôi chợt nghĩ, bìa dễ thương đấy nhưng dung lượng ngắn thế này chắc không đủ đặc sắc. Giống như một số truyện thiếu nhi Việt Nam tôi đọc, nội dung đều đều, không có điểm nhấn, thiếu sự sáng tạo,... Nhưng tôi đã nhầm. Cuốn sách văn học thiếu nhi kỳ ảo của Hàn Băng Vũ hay hơn những gì tôi kỳ vọng.

Ngay từ mở đầu truyện đã dễ thấm, bởi văn phong tác giả nhịp nhàng, trôi chảy và đầy sống động mặc dù câu từ chị sử dụng rất bình dị, rất chân thực. Chị viết chắc tay từ lối miêu tả đến cách diễn đạt, khiến cho tôi hòa mình vào cuộc sống của chú bé tí hon Ngón Cái một cách đầy đủ và trọn vẹn nhất.

Đề tài về người tí hon trong văn học thế giới đã được khai thác không ít, tuy nhiên qua sự thể hiện của Hàn Băng Vũ lại mang một phong cách rất riêng, rất thú vị và rất Việt Nam mà không phải ai đọc qua một lần cũng có thể cảm nhận hết.

Tôi bất ngờ là một quyển truyện nhỏ như vậy lại chứa đựng quá nhiều tình tiết hấp dẫn, có đủ nút thắt và cao trào. Từ việc Ngón Cái cứu các loài vật bị hà hiếp, phiêu lưu trong đêm ngăn chặn tội ác của bọn người xấu, giao chiến với rắn độc đến việc đánh đắm thuyền giặc giúp cha cứu làng,v.v... thảy đều cuốn hút tôi, buộc tôi đọc không ngừng nghỉ. Có lúc ngợp trước những tình huống gay cấn liên tiếp tôi không vừa lòng và nghĩ tác giả nên thay đổi, nhưng thực chất đối với một độc giả ưa truyện mạo hiểm như tôi thì những cuộc phiêu lưu của Ngón Cái có lẽ chẳng bao giờ là đủ cả, tôi còn muốn đọc nữa và đọc mãi.

Nếu một tác phẩm xuất sắc về mặt giải trí nhưng thiếu khuyết về thông điệp thì hẳn tác phẩm đó sẽ không đọng lâu trong lòng độc giả. Và tác giả Hàn Băng Vũ đã truyền tải quá tốt thông điệp qua tác phẩm của mình. Ngón Cái can đảm, mưu trí, giàu lòng nhân ái; chú bé luôn giúp đỡ và quan tâm bạn bè. Bài học về lòng tốt của Ngón Cái là rất bổ ích, sẽ nuôi dưỡng tâm hồn bạn đọc nhỏ tuổi. Còn với người lớn, những vấn đề hiện thực nhức nhối như chuyện hoa quả có chất bảo quản, hay to tát như chuyện biển đảo tổ quốc bị xâm phạm cũng được chị lồng ghép khéo léo, mở ra trước mắt chúng ta nhiều suy ngẫm...

“Cuộc phiêu lưu của Ngón Cái” tinh tế về nội dung, đẹp về hình thức, quý giá về thông điệp. Cuốn sách dành cho mọi lứa tuổi, đáng đọc và đáng trân trọng!
5
386342
2015-12-29 13:05:25
--------------------------
342999
3143
Câu chuyện dễ thương làm người đọc như phiêu lưu vào thế giới đầy màu sắc và thần thoại. Những tình tiết đơn giản, gần gũi nhưng qua lăng kính của nhân vật nhỏ lại như thú vị và đầy màu sắc hơn. Câu chuyện này có thể đọc kể cho con, cháu nghe vào những buổi tối. Giúp chúng phát triển khả năng tưởng tượng và ngôn ngữ. 
Bằng một giọng văn trong sáng và đáng yêu, phù hợp với các em nhỏ hy vọng tương lai, tác giả sẽ viết những câu chuyện đáng yêu như thế này để mình có thể mua cho các cháu.
5
481217
2015-11-25 16:17:49
--------------------------
295290
3143
"Cuộc phiêu lưu của Ngón Cái" mang âm hưởng cổ tích dân gian Việt Nam với nhiều tình tiết hấp dẫn, li kì của cậu bé Ngón Cái tí hon thông minh và can đảm. Giọng văn của Hàn Băng Vũ giản dị mà trong sáng và giàu biểu cảm, kết hợp hài hòa với nét minh họa mềm mại và tinh tế của Phan Miên Thảo, chắc chắn sẽ lôi cuốn các độc giả thiếu nhi ngay từ đầu truyện. Cùng đọc to với bé hay đọc cho bé trước giờ đi ngủ đều rất thích hợp. Hi vọng trong tương lại sẽ có thêm nhiều sách truyện như thế này nữa cho lứa tuổi nhi đồng. 
4
648847
2015-09-10 09:19:14
--------------------------
