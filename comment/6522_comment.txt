409338
6522
Nguyễn Công Hoan là một nhà văn mình vô cùng yêu thích. Quyển sách này tập hợp gần hết các tác phẩm của ông. Tuy nhiên tác phẩm Bước đường cùng thì lại không được in nên mình khá là tiếc. Nói chung khi nhận được sách mình rất ưng ý, bìa cứng nên không lo gãy gáy sách, giấy in dày dặn, chữ rõ ràng. Mỗi mẩu chuyện của ông đều rất ý nghĩa và mang nét trào phúng đặc trưng. Cách dẫn chuyện của ông rất dễ gây tò mò cho độc giả khiến ta phải đọc đến con chữ cuối cùng mới hiểu ý tứ sâu xa của nó
5
402348
2016-04-02 12:39:46
--------------------------
246204
6522
KHi nhận được hàng mình đã rất bất ngờ vì sách có bìa cứng mà tiki lại không ghi trong miêu tả. Với 1 cuốn sách dầy, in đẹp, giấy nhẹ, bìa cứng đẹp mà chỉ có từng này là giá quá rẻ cho sách mới. Sách có hầu như đủ hết mọi tác phẩm của Nguyễn Công Hoan, được sắp xếp thứ tự theo năm sáng tác (đây cũng là điều nữa khiến mình rất ưng). Nguyễn Công Hoan không ngừng đả kích những xấu xa, mặt tối của xã hội. Tuy ông không viết gì đau buồn nhưng người đọc vẫn cảm nhận được sự đau buồn của những nhân vật trong truyện
5
162924
2015-07-29 17:44:24
--------------------------
226932
6522
Quyển này tập hợp tương đối đầy đủ những truyện ngắn của nhà văn Nguyễn Công Hoan. Qua những câu chuyện, nhà văn đã lột tả vô cùng chân thật một xã hội đầy những bất công, mâu thuẫn giai cấp  trước cách mạng tháng tám mà chính ông cũng là một nạn nhân trong xã hội đó. Ngoài ra lối tả của ông cũng rất độc đáo, ông chỉ gợi tả, chỉ dùng những từ ngữ, hình ảnh bóng gió mà người đọc có thể đoán và hiểu ra ẩn ý bên trong, làm người đọc như đang tham gia vào câu chuyện. Điều đặc biệt nữa mà tôi thích ông hơn những tác giả khác là lối kết trong những mẫu truyện ngắn của ông luôn là một kết thúc mở hay kết thúc ở lưng chừng làm người đọc cứ phải thắc mắc, suy nghĩ, phỏng đoán hoài sau mỗi câu chuyện..
4
277587
2015-07-13 10:41:34
--------------------------
