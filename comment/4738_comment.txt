457568
4738
Đây là một trong những quyển sách mà mình rất thích. Bìa và chất lượng sách in khá ổn. Trình bày rất dễ hiểu, lời viết ngắn gọn, mỗi vấn đề mà tác giả đề cập lại được tóm tắt thành từng mục để bạn dễ dàng ghi ghi chép lại và thực hành. Nếu bạn có ý định khởi nghiệp thì nên đưa cuốn này vào trong danh sách những thứ cần đọc, nó sẽ giúp bạn tiết kiệm khá nhiều thời gian và công sức khi bắt đầu khởi nghiệp kinh doanh đấy. Hãy có ước mơ lớn và lên kế hoạch lớn!
5
551673
2016-06-24 12:56:57
--------------------------
319351
4738
Đây là cuốn sách rất bổ ích cho các bạn muốn xây dựng một sự nghiệp kinh doanh. Đọc xong cuốn này, bạn sẽ tự tin lập được một đề án kinh doanh tương đối hiệu quả. Cố gắng học hỏi thêm kinh nghiệm của những người đi trước nữa thì sẽ ổn! Tuy nhiên, ở điều kiện Việt Nam, yếu tố tâm lý con người là quan trọng nhất. Bạn cần đọc kỹ phần lên kế hoạch và tự xây dựng một kế hoạch kinh doanh cụ thể dựa trên ý tưởng kinh doanh của mình. Chúc các bạn thành công trên con đường mình đã chọn!
5
196421
2015-10-08 15:19:34
--------------------------
296965
4738
Mình thích những việc làm có đầy đủ lý luận và thực tiễn, đặc biệt là trong những việc quan trọng như khởi nghiệp chẳng hạn. Bạn sẽ không đủ thời gian và sức lưc nếu cứ đâm đầu vào đời làm đại làm càn để rồi chuốc lấy thất bại được. Dành cho những ai mong muốn khơi nghiệp, quyển sách này cung cấp những quy tắc "vàng" để làm hành trang lý luận, sẵn sàng làm tiền đề để hành động. Các quy tắc này được sắp xếp theo các việc từ "thấp" đến "cao". Tuy nhiên cuốn sách này không hẳn là "kinh thánh" trong việc khơi nghiệp được, dù sao đi nữa nó vẫn mang tính tham khảo vì đây chỉ là ý kiến riêng của tác giả, chúng ta cần phải nghiên cứu nhiều hơn và tự nghiền ngẫm ra các quy tắc khác. Sách in đẹp, tuy mình không thích trình bày bìa cho lắm.
4
564333
2015-09-11 14:16:43
--------------------------
278524
4738
Mình vừa mua quyển sách 8 Quy Tắc Vàng Khởi Nghiệp hôm 15/08, cảm nhận đầu tiên là bìa sách đẹp chất lượng giấy khá ổn. Sách trình bày rõ ràng, dễ nhìn, dễ đọc. Về nội dung, cũng giống như tựa đề sách, xoay quanh 8 quy tắc khởi nghiệp, giúp chúng ta có hướng đi, cách nhìn thiết thực để có thể bắt tay vào con đường khởi nghiệp. Và khởi nghiệp lúc nào cũng cần phải có những nguyên tắc nhất định. Tóm lại sách khá hay, đọng lại nhiều bài học
4
688667
2015-08-25 22:32:11
--------------------------
246797
4738
Mình vô tình nhìn thấy quyển sách này khi vào Nhà Sách, đọc thử và thấy sách rất hay, dự định là sẽ mua sau, không ngờ khi vào Tiki thì ngay lúc được giảm giá nên rước ngay em này về luôn. Cũng giống như tên sách, nội dung sách bao gồm 8 quy tắc vàng dành cho những ai sắp, đang và đã có ý định tự khởi nghiệp nhưng vẫn còn hoang mang chưa biết phải làm như thế nào. Mỗi quy tắc trong sách được xem như là một chương, trong mỗi chương lại chia nhỏ ra từng lời khuyên, cuối mỗi lời khuyên có tóm tắt lại để ghi nhớ và đưa ra một số câu hỏi để chúng ta suy ngẫm lại và cuối cùng là hướng dẫn chúng ta cách hành động phù hợp theo những lời khuyên đó.
4
443629
2015-07-30 09:22:32
--------------------------
