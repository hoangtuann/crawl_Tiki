286542
10181
Cảm nhận của tôi khi lần đầu đặt chân đến mảnh đất bình yên này là sự thơ mộng của dòng sông Hương êm ả chảy uốn lượn quanh thành phố hoà quyện vào sự cổ kính với công trình kiến trúc cung điện lăng tẩm của vua chúa xưa. Cảm xúc đó ám ảnh và thúc giục tôi mua quyển sách này ngay lần đầu nhìn thấy, bởi tôi muốn tìm lại chút lặng lẽ êm đềm của nơi đó. và quả thật nó không làm tôi thất vọng. Qua lời văn trôi chảy của tác giả tôi như một lần nữa được trở về đây và hiểu cặn kẽ hơn mảnh đất này. Và nếu bạn yêu thành phố này, đừng bỏ qua quyển sách hay này.
4
85575
2015-09-01 20:47:58
--------------------------
66718
10181
Tôi đã từng đến Huế, đã từng cảm nhận cái đẹp tuyệt vời của cố đô cổ kính mà thơ mộng ấy. Nhưng khi đọc qua cuốn sách này tôi mới thật sự thấy thấm thía hơn rất nhiều giá trị vẻ đẹp của Huế. Cuốn sách khai thác được rất nhiều chiều sâu những vẻ đẹp của cố đô này. Cuốn sách như một người hướng dẫn viên du lịch vậy lặng lẽ đưa chúng ta trải nghiệm từng nét đẹp, từng công trình kiến trúc uy nghi như lăng tẩm xưa của vua chúa...Ngoài ra chúng ta còn có thể cảm nhận được những vẻ đẹp vô cùng giản dị khác như chiều tím trên sông Hương thơ mộng, vẻ đẹp tà áo dài truyền thống của người con gái xứ Huế mộng mơ, thanh nhã hay vẻ đẹp cổ kính bình dị của mái chùa Thiên Mụ...Đây quả là một cuốn sách hay về Cố Đô Huế mộng mơ cổ kính
5
94962
2013-04-01 18:49:51
--------------------------
