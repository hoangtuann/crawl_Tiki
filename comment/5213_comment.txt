249012
5213
Nếu các bạn có ý tưởng luyện tiếng Anh nói chung và tiếng anh của bạn thực sự khá (ví dụ như có thể đọc tiểu thuyết tiếng Anh và xem TED talks không cần phụ đề) thì cuốn sách này sẽ hết sức bổ ích để bạn nâng cao vốn từ và làm đa dạng hóa khả năng sử dụng ngôn ngữ. Tuy nhiên nếu bạn chỉ có ý định luyện thi IELTS thì cuốn sách tỏ ra khá khó khăn, ít ra band 7 trở lên mới học được. Ngoài ra phần nghe thì chất lượng không tốt lắm, âm thanh của file hơi bị nhiễu.
3
190393
2015-07-31 11:57:00
--------------------------
