525589
4968
Ban đầu khi mới nhìn thấy cuốn sách mình rất muốn mua vì bìa sách rất dễ thương và tên cũng hay nữa. khi nhận dc hàng thì đúng là bìa đẹp và bookmark dễ thương thật. Nhưng khi mới vừa mở vài trang ra thì mình thất vọng vô cùng: giấy xấu, font chữ xấu, hình vẽ thì rất mất thẩm mỹ và nội dung nhàm chán vô cùng. Thiết kế bên trong rất tệ. Thêm vào đó không biết có phải do văn phong của tác giả không hợp với mình không nhưng mình cảm thấy sách rất dở, không muốn đọc tiếp. 
1
1399433
2017-02-15 12:57:19
--------------------------
524941
4968
Mình đã nhận được sách rồi, cảm ơn tiki giao hàng rất nhanh và tốt . Cảm ơn đến tác giả cuốn sách cũng rất ok , tuy nhiên thì chàng trai trong quyển sách làm tôi không thích lắm , và tôi là người không thích thơ nên cuốn sách chỉ dừng ở mức được không hài lòng tôi tuyệt đối . Khi nghe tựa quyển sách làm tôi rất cuốn hút tuy nhiên khi đọc thì cũng hay nhưng không được như bản thân mong đợi lắm . Bìa sách đẹp và mình rất hài lòng với tiki , thái độ giao hàng , thời gian giao hàng cũng như chất lượng sách . Thanks
4
2141519
2017-02-14 13:07:13
--------------------------
369330
4968
Cuốn có nhiều cách suy diễn về cách biểu đạt tình cảm khá nhẹ nhàng ,trong sáng lại hoàn toàn chứa đựng những điều khá bí ẩn quanh mối quan hệ đời thường cũng có trùng lặp về hoàn cảnh nhưng như các tác phẩm khác thì chưa hẳn bởi văn từ còn có nỗi niềm cảm thông , để tìm lại chính mình khi muốn thoát ra khỏi nơi thăm thẳm của đời sống thôi , trái tim là nơi nắm giữ sự sống quan trọng của con người mà nó chưa thể làm mất đi ý nghĩa thực .
4
402468
2016-01-15 15:41:13
--------------------------
243681
4968
Mình bị quyển sách này thu hút vì cái tựa nghe rất dễ thương-"Tim mình trong ngực người ta". Ban đầu mình cứ nghĩ nó là truyện ngắn- văn xuôi đơn giản. Nhưng khi đọc mình mới biết, mỗi truyện là một trang nhật kí được viết bằng những cảm xúc chân thật của tác gỉa. Đó là những bài thơ ngắn, những dòng văn xuôi được viết với câu chữ đơn giản, mộc mạc và đầy tình yêu. Đặc biệt, sách còn có chừa ra những trang trống để người đọc có thể viết những cảm xúc của chính mình vào đó.
Tuy nhiên sách có một điểm trừ nhỏ là phần gáy được đóng quá sát khiến cho việc mở sách hơi khó khăn và sách dễ bị bung trang.
Dù vậy, đây cũng  là một quyển sách hay và đáng đọc...
3
302048
2015-07-27 16:56:02
--------------------------
165029
4968
"Một quyển nhật ký giàu cảm xúc" - đó là ý nghĩ đầu tiên khi mình gấp cuốn sách này lại. Đúng như những lời tác giả nói đầu sách, đây đúng thật là một tác phẩm anh viết ra để "thêm niềm tin yêu và yên bình nơi trái tim", nhưng không những đối với anh mà còn cho những người đã dành thời gian đọc và cảm nhận nó, trong đó có cả mình. 
"Nhật ký tim mình trong ngực người ta" khiến nhiều người những tưởng là những suy nghĩ, cảm thán, nhớ nhung...của một chàng trai về mối tình đã qua. Tuy nhiên, càng đọc cuốn sách, mình càng bị cuốn trôi theo cảm xúc của nó xen giữa tình yêu, tình bạn và tình cảm gia đình. Đôi khi là một mẩu chuyện đẹp khiến ai đọc cũng thấy ngọt ngào, nhưng chợt nhận ra đó chẳng qua là khoảng thời gian trong hồi ức; là niềm tin tuyệt đối với bạn bè nhưng lại hụt hẫng vô cùng với sự kết thúc của mối quan hệ tóm gọn trong 2 chữ "phản bội"; là những tình cảm chân chất, ấm áp của những người thân gia đình xung quanh nhưng vẫn còn đó sự ân hận về những lỗi lầm thời nhỏ dại...những cảm xúc lẫn lộn đan xen...nhưng rất "đời" - đúng tính chất của một quyển nhật ký.
Tác giả chia trái tim bản thân mình thành 4 phần và đều diễn tả hết trong cuốn sách này. Tuy nhiên, đối với mình thì lại ấn tượng nhất 2 phần, đó là tình yêu và gia đình. Cảm xúc đọc 2 phần này thật sự là "Thêm niềm tin yêu và yên bình nơi trái tim". "Thêm niềm tin yêu" với tình yêu dù có là người thất bại và "yên bình nơi trái tim" trong vòng tay gia đình...
4
140964
2015-03-09 15:43:04
--------------------------
164921
4968
Tôi là một người kén đọc, đặc biệt, tôi bị "dị ứng" với thể loại thơ. Khi cầm trên tay cuốn sách, thấy tác giả là nam, tôi nghĩ ngay đến sự khô cứng trong từng câu chữ. 
Nhưng đó là lúc chưa đọc sách. Tôi cũng không tin vào bản thân mình rằng sau khi đọc những dòng thơ trong cuốn sách, tôi còn có thể chia sẻ chúng lên trang cá nhân, nghiền ngẫm chúng như kẻ nghiện thơ và chưa bao giờ ghét đọc thơ. Bấy nhiêu thôi cũng đủ cho tôi nhận ra cuốn sách thật sự đáng đọc. 
Những câu chuyện tình yêu của tác giả xen lẫn dòng suy nghĩ đầy lý trí về cuộc sống, về yêu thương con người khiến tôi nhìn lại mình. Câu chữ sâu sắc đến mức những gì tôi đã đọc cứ lởn vởn trong đầu tôi, rồi tôi tự hỏi: "Tim mình đã trong ngực ai kia rồi?"
Đáng đọc, đọc không chỉ để đọc mà để tìm lại chính mình, tìm lại trái tim của mình đã vô tình để quên đâu đó trong những cuộc tình xưa cũ.
5
251812
2015-03-09 09:32:42
--------------------------
161611
4968
Lúc đầu có hứng thú với quyển sách này ở bìa sách của nó, tranh vẽ rất đáng yêu và lạ. Tác giả lại mang tên con vật yêu thích của mình nữa. Song, mình thấy cuốn sách này quen quen, liếc vào phần tác giả Trung Giang thì mới phát hiện đây là phần tái bản của cuốn "Tim mình trong ngực người ta" đã để lại cho mình nhiều cảm xúc. Không biết có thêm cụm từ "nhật ký" thì nội dung có thay đổi gì không...Với nhiều người, Trung Giang là một cái tên lạ (mình cũng chỉ biết đến "TMTNNT" mà thôi) nhưng khá thích phong cách của tác giả này: không viết theo một khuôn khổ nào mà dường như chảy theo mạch cảm xúc. Đọc xong, thấy sao buồn buồn nhưng có vẻ cuộc đúng không phải tất cả đều mang màu hồng. Mặt khác mình cũng thấy thỏa mãn, bởi những dòng chữ in trên trang giấy đã từng là những suy nghĩ, những cảm xúc mình chỉ dám nghĩ thôi, không phải không muốn viết mà là không biết diễn đạt như thế nào. Chờ tác phẩm tiếp theo của tác giả!
4
91968
2015-02-28 15:29:55
--------------------------
