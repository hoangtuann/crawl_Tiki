340619
6185
Khổ sách bản to, in màu đẹp. Sách phù hợp với trẻ từ 4 tuổi trở lên vì nội dung được thiết kế giống truyện tranh, trẻ nhỏ sẽ không biết nên đọc theo thứ tự nào. Nội dung mang tính khoa học nên cũng phù hợp với trẻ lớn hơn. Hành trình đất sét biến đổi thành các sản phẩm được đơn giản hoá thành câu chuyện tranh về cuộc phiêu lưu của đất sét để trẻ dễ tiếp thu kiến thức. Mình nghĩ các mẹ có con lớn có thể mua cả bộ về để con đọc cũng rất hay. 
3
703221
2015-11-20 10:31:11
--------------------------
