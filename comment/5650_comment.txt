471468
5650
Ở phần đầu này chính là kể về chuyến đi chơi của những người bạn thân của ngài Mori, tưởng rằng chỉ là một chuyến đi họp mặt, nhưng không ngờ đó lại là một âm mưu có từ trước, giữa tội phạm và bạn thân, ngài Mori lần này sẽ chọn ai? Thật hồi hộp nha~ 
Ở phần sau thì, hihi, Conan không phải là không thể mắc sai lầm nào đâu nha, chắc tại máu thám tử nó ăn vào máu ấy mà :v 
Phải nói tập 9 này, phần nào cũng làm thật làm cho người ta bất ngờ mà =.=
4
459343
2016-07-08 15:59:38
--------------------------
422463
5650
Tập 9 của bộ Thám tử lừng danh Conan là tập đã đọc từ rất lâu lâu rồi và cũng đã làm thất lạc. May sao đã tìm thấy trên Tiki có bán. trong tập 9 thì vụ án khơi gợi nhiều suy nghĩ ray rứt nhất chính là vụ án ở suối nước nóng thì hung thủ cuối cùng lại chính là bạn bè của ông Mori và nạn nhân cũng chính là người ông quen biết. Tình tiết của vụ này tương đối bất ngờ, tạo được sự hồi hộp. Tuy nhiên, nét vẽ của tập này thì không sắt sảo lắm.
5
531312
2016-04-27 11:19:12
--------------------------
386501
5650
Thám tử̀ lừng danh Conan tập 9 (tái bản 2014) quá xuất sắc hấp dẫn lôi cuốn vô cùng, mỗi tập lại là những mẫu chuyện khác nhau với tình tiết hấp dẫn khác nhau nhưng không quá trùng lập không quá khô khan tác giả đã khắc họa được nét tính cách chững chạc của cậu học sinh cấp 3 trong thân xác của cậu bé học sinh tiểu học, sự thông minh, bình tỉnh sử lí tình huống của Conan đã góp phần làm nên sự xuất thần của nhân vật và sự hấp dẫn của truyện.Trong tập này vì mải chơi trốn tìm nên Yumi đã lên xe trốn và chẳng may bị bắt cóc.Đội thám tử nhí đã phải điều tra qua âm thanh và truy bắt bọn tội phạm trước khi quá muộn.Cuối cùng kết cục lại khiến mọi người thất vọng vì chỉ là nhầm lẫn . Conan bị hại rồi Ran khóc . Rất cảm ơn tác giả đã dày công để sáng tác ra một tuyệt phẩm như thế này!
5
853243
2016-02-25 19:44:17
--------------------------
375616
5650
Mấy tập đầu truyện thám tử Conan công nhận có nhiều chi tiết kinh dị ghê, Ayumi lúc chui vào xe ô tô, cầm cái gói lên mà thấy cái đầu người, may sau này biết là giả nhưng mình xem cũng thấy ghê quá trời, lại còn bình tĩnh bá hiệu cho đội thám tử nhí Conan biết nữa, phục thật. Vụ ở suối nước nóng cũng khá hay đặc biệt lại do chính ông thám tử gà mờ Mori phá án mới hay nhưng mình chỉ thích nhất màn rượt đuổi trong vụ chơi trốn tìm thôi, cuối cùng thì Ayumi cũng được giải thoát nhưng tội cho cả đội phải đóng thế thay diễn viên bị nhóc  Conan đá cho ngất xỉu vì bị lầm là hung thủ...
5
568747
2016-01-28 20:30:16
--------------------------
330866
5650
Gà mờ ư ? Ổng ấy giỏi Judo và cũng nghĩa khí lắm đấy chứ. 
Rõ ràng ông ấy đẹp trai, có tài và nhân cách tốt nên mới cưới được cô vợ luật sư tài giỏi và xinh đẹp. Chỉ có mỗi khuyết điểm là mê gái quá. 
Cha mẹ giỏi nên Ran cũng giỏi và thừa kế nét xinh xắn của cha mẹ. 
Tập này khá gay cấn. Từ tập này trở đi các vụ án trở nên hóc búa và khó phán đoán hơn rồi. Bởi thế mới yêu Conan nhiều vậy và không bỏ qua tập nào. 
5
535536
2015-11-03 14:22:38
--------------------------
250852
5650
Tập này rất hay, nhất là vụ án ở suối nước nóng khi nạn nhân và hung thủ lại là những người bạn cũ của ông chú Mori. Không như mọi lần bị Conan gây mê và phá án thay, lần này chú Mori đã không đứng ngoài cuộc mà muốn tự phá án. Với sự trợ giúp từ những lời gợi ý của Conan cuối cùng thủ phạm cũng bị lật mặt, và phải nói là ấn tượng với cái cách mà chú Mori đối đầu với tên hung thủ - cũng chính người bạn cũ của mình. Đọc tập này xong vừa bất ngờ mà còn nể chú thám tử râu kẽm ghê.
4
471112
2015-08-02 10:57:34
--------------------------
240390
5650
Nội dung truyện dĩ nhiên khỏi chê, tôi là một tín đồ trung thành của Thám tử lừng danh Conan, vì vậy bất cứ vụ án nào trong đây cũng làm tôi thấy thích thú. Tình tiết vụ án thú vị và đầy bất ngờ, đúng là bản chất của truyện trinh thám. Hơn nữa tập này còn có moojt số yếu tố hài hước làm cuốn truyện có phần nào nhẹ nhàng hơn, tôi thích điều đó. Tuy nhiên, tập 9 cũng như mấy tập đầu, hình vẽ còn chưa đẹp, thiếu cân đối và chưa thật sắc nét, kể cả Ran và ông bác Mori trông cũng có vẻ trẻ con.
4
412050
2015-07-24 16:09:09
--------------------------
206031
5650
Trong tập này vì mải chơi trốn tìm nên Yumi đã lên xe trốn và chẳng may bị bắt cóc.Đội thám tử nhí đã phải điều tra qua âm thanh và truy bắt bọn tội phạm trước khi quá muộn.Cuối cùng kết cục lại khiến mọi người thất vọng vì chỉ là nhầm lẫn
Trong tập này cũng là lần đầu tiên ông Mori tự phá án trong vụ án mạng liên quan đến mạng của bạn mình.Nhờ vào các gợi ý của Conan mà ông đã tìm ra thủ phạm nhưng đó lại là người bạn thân nên ông đã buồn bã rất nhiều.
Phần cuối là các vụ án mạng liên quan đến nước,trong đó Ran suýt bị hung thủ ám hại nếu không có Conan phát hiện kịp thời.Cuối cùng nhờ cào các vết nước mà thủ phạm đã bị phát hiện,thủ phạm sẽ là ai đây ?
Vì là mấy tập đầu tiên nên có vài chi tiết vô lí trong truyện như ba đứa trẻ cưỡi ván trượt lạ có thể đuổi kịp ô tô hay các gợi ý về cái chết bạn nhận xuất hiện ngay ở hiện trường vụ án không phù hợp với hoàn cảnh gì hết.
Tập tiếp theo sẽ có bao nhiêu vụ án xảy ra và bao nhiêu người phải chết đây ? Hung thủ đã dung cách thức gì để sát hại nạn nhân và các thám tử của chúng ta đã phá giải chúng như thế nào ? Mọi người tìm đón tập tiếp theo nhé.
5
650298
2015-06-08 14:42:41
--------------------------
175252
5650
Gặp lại những người bạn cũ hẳn là ông râu kẽm vui lắm nhưng không may một vụ án lại xảy ra. Giữa tình bạn và tội phạm hình như không có ranh giới, tội phạm thì vẫn là tội phạm phải không ông thám tử Mori? 
Bọn thám tử nhí lần này cũng có những kỉ niệm và khám phá thú vị, tưởng rằng Ayumi sẽ bị bắt cóc, ai ngờ đây chỉ là hiểu lầm. Trong tập này, hình như tình bạn được đề cao thì phải, khi chúng ta đoàn kết mọi khó khăn dường như bị thổi bay nhưng khi chia rẻ, rất khó khăn để đối mặt với thử thách. Vì vậy, đoàn kết là sống, chia rẻ là chết phải không các bạn?
4
13723
2015-03-29 19:45:02
--------------------------
170592
5650
trong mười tập đầu chắc tập 9 này hay nhất.mình ấn tượng vụ đầu tiên đọc xong mới thấy hóa ra conan cũng có lúc sai lầm nhưng sai lầm trong tập này thật hài hước.2 diễn viên kịch cho ngày hội ở trường teitan bị tình nghi là kẻ bắt cóc các cô gái.trong một lần chốn tìm yumi đã vô tình trốn vào xe của họ. 3 nhóc còn lại là conan,mitsuhiko và genta đã đuổi theo chiếc xe dựa vào huy hiệu thám tử,những gì 2 người đàn ông nói trên xe quả thực mờ ám khiến conan hiểu sai vấn đề.cái đoạn mà yumi nhìn thấy cái đầu giả đấy ghê thật,phát hoảng luôn.lúc chưa khám phá ra tưởng 2 gã để đầu thật lên xe ghê thấy mồ.
4
586877
2015-03-20 12:39:59
--------------------------
