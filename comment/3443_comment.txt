483280
3443
Mặc dù tác giả viết tác phẩm náy ở tuổi đã lơn nhưng vẫn giữ được cái nhìn của một đúa trẻ, những câu chuyện hết sức đời thường, của một cậu bé ngoan (cậu tự nhận mình như vậy) với những suy nghĩ về thế giới xung quanh. mình vừa đọc vừa nghĩ về hồi còn nhỏ của mình tuy hoàn cảnh khác nhau nhưng về suy nghĩ thì cũng ngây thơ như nhau, mình mua quyển này cùng với một vài quyển sách khác để cho mấy nhóc trong nhà, sau khi đọc mình nhận được những phản hồi rất thú vị từ cháu của mình, nên quyển này mình khuyên nên đọc cho mấy nhóc nhỏ ở nhà nghe, rất bổ ích.
4
457599
2016-09-13 00:15:47
--------------------------
449374
3443
Như mọi cuốn Nicolas, đây là một cuốn sách rất hay và thú vị, từng câu chuyện nhỏ của cậu bé Nicolas bên gia đình, trường lớp, bạn bè thật sự rất thú vị và đáng yêu. Đúng như lời nhận xét sau cuốn sách, đọc Nicolas, người lớn sẽ thấy sao mà nhớ quá, còn trẻ con thì thấy sao mà giống đến thế. Tôi thấy bản thân mình ngày bé và đôi khi là cả bây giờ trong cậu bé Nicolas, nghịch ngợm, trẻ con và vô lo vô nghĩ. Thật sự đây là một cuốn sách kinh điển  và nên đọc.
5
1121908
2016-06-17 13:12:57
--------------------------
428534
3443
Nhóc Nicolas này cực cực bá đạo. Đừng tưởng cậu là trường hợp cá biệt nhé! Quyển sách này chỉ là tổng hợp những câu chuyện của con trẻ thông qua 1 nhân vật là cậu nhóc Nicolas này thôi. Những câu chuyện kể, những tình huống trong gia đình, ở trường, chơi với bạn đều là những thứ chúng ta bắt gặp hàng ngày khi ta quan sát con trẻ.
Tác giả cực kì hài hước và hiểu tâm lý trẻ thơ mới sáng tác được bộ truyện trứ danh này!!!
Điểm cộng là người dịch sách. Cách diễn giải 1 câu thì khá là loằng ngoằng, 1 câu có thể dài lê thê nhưng đó lại là 1 thủ pháp nghệ thuật độc đáo trong bộ truyện này. Vì nó đúng với chất giọng của 1 đứa trẻ khi kể chuyện (bạn cứ thử lắng nghe đứa trẻ học tiểu học kể chuyện mà xem, chuyện này chưa dứt chuyển sang chuyện khác lúc nào mà nếu ta không chú ý lắng nghe rất dễ "lạc lối")
5
895234
2016-05-11 13:45:34
--------------------------
420303
3443
Mặc dù chưa đọc tập một mà nhảy sang tập hai đọc luôn nhưng những chuyện kể về cuộc sống thường ngày của cậu nhóc nicolas này thật sự rất thú vị và khôi hài. Đọc để thấy một thời mình từng điên khùng một cách thật dễ thương, vô tư và khó đỡ. "Mẹ đã bảo là mẹ có cười trước câu chuyện cười của con rồi" đó là cách người lớn đối xử với trẻ con bằng tư duy đã trưởng thành khi nghe hoài một câu chuyện cười, còn đối với nhóc nicolas , câu chuyện có kể trăm lần vẫn có thể đem lại nụ cười hồn nhiên như thuở ban đầu.Đó là một trong những điều khiến quyển sách này không chỉ dành cho trẻ em. Đọc mà cười hí hí như con điên vậy đó
4
897666
2016-04-22 23:12:34
--------------------------
406102
3443
Nhóc Nicolas không bao giờ nhàm chán cả. Cậu luôn làm người khác đau đầu vì những trò quỷ tinh ranh của cậu cùng bọn bạn. Đối với thiếu nhi, đây đích thực là một câu chuyện hài hước, dùng để giải trí, nhưng lại là một câu chuyện hài đầy lãng mạn cho người lớn. Qua Nicolas, họ cùng cười nhưng đồng thời cũng theo chân nhóc Nicolas trở lại những ngày xưa cũ, những trò quậy phá tinh ranh. Cùng cười xòa ồ lên: sao giống mình thế. Đó, nhóc Nicolas không chỉ dành cho thiếu nhi, trong đó còn chứa cả tuổi thơ của những người đã trưởng thành
4
1153632
2016-03-27 18:31:27
--------------------------
401705
3443
Mình đã sưu tầm đến 3 tập đầy đủ của nicolas và những chuyện chưa kể , phải nói cậu nhóc này rất láu cá , và có 1 đám bạn láu cá không kém , đôi đọc đễn nỗi cười ngất ngửa , em trái rất thích chú nhóc , nếu chắc hẳn có truyện tranh thì sẽ hay hơn nhiều nhỉ , một cậu nhóc có hàng tấn các trò nghịch ngợm làm thầy cô và ba mẹ đau đầu tôi lại nhớ tôi hồi bé , cũng tinh nghịch và ương ngạch chả kém , chỉ kém là tôi không có 1 đám bạn tinh nghịch như thế mà thôi 
5
796153
2016-03-21 09:31:28
--------------------------
384872
3443
Mình thấy đây không đơn giản là một cuốn sách kể chuyện của thiếu nhi, thông qua cách kể chuyện hóm hỉnh của tác giả, người lớn có thể nhận ra nhiều ẩn ý bên trong. Nói vậy bởi có những mẩu chuyện khi đọc lần đầu mình chỉ thấy hài hước, buồn cười nhưng khi đọc lại lại thấy ah, tác giả có ý muốn nói điều khác nữa. Lối hành văn trong sáng, những câu chuyện đơn giản nhưng được kể một sách hấp dẫn, hài hước. Sách phù hợp cho các bạn nhỏ, và cho cả các bậc phụ huynh muốn quay lại thời thơ ấu nghịch ngợm của mình :) 
4
675978
2016-02-23 08:51:47
--------------------------
382296
3443
Nicolas luôn mang đến cho người đọc những góc nhìn mới lạ của trẻ thơ cùng với những suy nghĩ lớn lên cùng chúng.Đọc Nicolas, ta mới hiểu thêm về trẻ thơ,để rồi gần gũi và thân thiết với chúng.Ta thấy tâm hồn ta trẻ ra qua từng trang sách, tìm lại được niềm vui con trẻ đã lãng quên trong ta bao năm nay.Chạm được đến đáy của câu chuyện mộc mạc là cả một chân lí về cuộc sống, những mảnh đời.Và rồi ta thêm trân trọng niềm hạnh phúc ta đang có
Là một trong những tác phẩm tiêu biểu của Nhã Nam,Nicolas có chất lượng giấy tốt và đẹp, không gây mỏi mắt
Cảm ơn các tác giả đã viết nên một tác phẩm để đời và cảm ơn Nhã Nam đã luôn đồng hành trong suốt thời gian qua
5
584588
2016-02-18 22:26:45
--------------------------
309191
3443
Bìa sách thiết kế đẹp, mực in rõ nét, chất giấy tốt. Hình vẽ minh họa tuy đơn giản nhưng rất cuốn hút người đọc. Nội dung thì khỏi bàn cãi, rất hay và hấp dẫn, mình đọc mãi không thấy chán. Cách viết tự nhiên,lôi cuốn người đọc. Từ chính sự ngây thơ và sự hiếu kì của mình, Nicolas đã tạo nên những tình huống dở khóc dở cười. Cùng với 7 người bạn, dường như ngày nào của cậu cũng rất vui tươi. Cả người lớn và trẻ em đều nên đọc cuốn này. Đối với những người đã trưởng thành, cuốn sách này sẽ khiến mọi người nhớ lại kỉ niệm một thời của bản thân mình
5
455025
2015-09-18 22:40:52
--------------------------
236140
3443
Mình có cả bộ truyện này, truyện đọc rất hay, rất hấp dẫn làm mình đọc mãi không muốn dừng. Hai tác giả Pháp đã thành công thực sự khi làm cho Nicholas có một nét cuốn hút phá cách. Từ sự ngây thơ, và tính hiếu kì của trẻ con, cậu bé đã làm nên rất nhiều chuyện dở khóc dở cười trong cuộc sống. Cậu bé tinh nghịch này sẽ khiến cho một ngày của mình trở nên vui hơn, và cùng với bảy cậu bạn khác, Nicholas đã trở thành tác phẩm kinh điển tại đất nước Pháp. 
5
676659
2015-07-21 16:00:36
--------------------------
