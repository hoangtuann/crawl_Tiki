473740
5945
Tự nhiên vừa nhảy vào truyện là thấy người quen liền hà, Rachel gặp ở đập Hoover đã cứu Percy thoát chết đã tái hiện ở tập này và đã trở thành một trong những vũ khí bí mật để Percy vượt qua mê cung. Tuy cuộc chiến đã đến hồi căng thẳng nhưng bác Rick vẫn không quên thêm vào những chi tiết hài hước, chẳng hạn như khi Tyson thú nhận là mình sợ Grover hay lúc Annabeth có cách xử sự kì lạ với Rachel và Percy. Fan của Percabeth nhất định phải đọc tập này mới được!!!
5
502455
2016-07-10 21:03:01
--------------------------
472283
5945
Lại là một cuộc phiêu lưu thú vị nữa của Percy Jackson và các anh hùng trẻ tuổi. Lần này những người bạn đã gặp phải nhiều mối nguy hiểm hơn, đấu tranh gian khổ hơn và vì vậy câu chuyện cũng hấp dẫn, nghẹt thở hơn. Cuộc phiêu lưu lần này vừa có những cuộc chiến nguy hiểm, vừa mang tính trí tuệ và bí ẩn khi chủ đề là cuộc tìm kiếm trong mê cung, khiến mình rất thích. Kết thúc truyện khá mỹ mãn mặc dù có vài người hy sinh. Vậy là chúa tể Kronos và cuộc nổi dậy của ông vẫn chưa kết thúc, chúng ta lại được chờ đón những cuộc phiêu lưu thú vị tiếp theo của Percy và những người bạn. Một cuốn sách rất hay và bổ ích^^.
3
699407
2016-07-09 13:23:30
--------------------------
463809
5945
Thật hết sức ngưỡng mộ daedalus, với những công trình tuyệt vời của ông. Và bên cạnh đó, lại là một câu chuyện, một chuyến phiêu lưu vô cũng nguy hiểm, gay cấn. Percy và bạn của cậu, người em trai Tyrson, tất cả đều hoàn thành vai trò một cách tuyệt vời. Percy thật là thông minh. Âm mưu của kronos, cậu đang đến gần hơn, bức màn đã gần được mở ra. Và đi cùng đó, lời sấm nguyền cũng đang đến sát hơn với cậu, với quyết định của cậu, với cái mốc 16 tuổi đã gần kề.
5
920424
2016-06-29 17:09:39
--------------------------
370941
5945
Ở tập 4 này, mạch truyện đã được đẩy lên nhanh hơn, nguy hiểm hơn. Mối nguy hiểm về đội quân của titan Kronos cũng bắt đầu hiện rõ ràng hơn. Nhân vật Rachel xuất hiện lại với vai trò người dẫn đường cho nhóm của Percy. Trong tập này, Rick Riordan đã cực kì tinh tế trong việc truyền tải tâm tư, tính cách của các nhân vật. Như khi Percy gặp một người anh em cùng cha khác mẹ của mình hay như khi anh gặp nữ thần Calipso kiều diễm, phải lựa chọn tiếp tục trách nhiệm hay trốn tránh nó. Mình còn ấn tượng mãi với câu nói "Các vị thần Olympus chắc gì đều tốt, anh đứng về phía họ không phải vì họ đúng mà vì họ là gia đình của anh"
5
859331
2016-01-18 18:24:49
--------------------------
363500
5945
Ngoài Edragon và thì đây là bộ truyện thứ 2 mình bỏ công sưu tầm. Truyện hay, hài hước, không chỉ là đọc giải trí, lồng trong truyện là tình thương cha mẹ, tình cảm gia đình và tình bạn đẹp đẽ. 
Ngoài nội dung truyện thì giấy in của tập 4 trở về sau rất xấu. Giấy xám xịt, bản in thì có chỗ mờ, không sắc nét và đẹp như các quyển trước. Thật không hiểu nổi khi nhà xuất bản càng tài bản thì giấy càng xấu và giá thì càng mắc! Chất lượng đi ngược với thời gian.
5
298824
2016-01-04 13:53:47
--------------------------
345995
5945
Một cuốn sách tuyệt vời lôi cuốn, không thể bỏ qua. Một vị anh hùng cam đảm, những người bạn tốt bụng đáng tin. Và một thế giới đã bị thay đổi. Đọc percy và thấy cuộc đời tươi đẹp hơn... trí tưởng tượng phong phú nội dung nhất quán. Anh hùng tuổi trẻ trí tuệ.... nhiều phẩm chất tốt đẹp hội tụ trong một con người. Ôi thế giới.. mọi chuyện sẽ khác theo mỗi các nhìn và cuộc sống sẽ thay đổi theo thời gian amen. Hay hay hay hay hay hay hay hay hay hay hay hay
5
934019
2015-12-01 18:25:36
--------------------------
265549
5945
Nhiệm vụ đầu tiên của Annabeth, một thử thách đáng gờm của con gái nữ thần Athena. Có vẻ như Annabeth không thích Rachel cho lắm, phải chăng là đang ghen??? Đến tập này tình cảm của Percy và Annabeth bộc lộ rõ hơn, hẳn là fan Percy-Annabeth thích điều này. Hơn nữa, cuộc phiêu lưu của các nhân vật qua ngòi bút của bác Rick thật sự rất lôi cuốn và thu hút. Hành trình xuyên qua mê cung là một hành trình vất vả và khó khăn. Thích nhất đoạn Percy dọn chuồng ngựa, càng đọc càng thấy yêu cậu bé này hơn. Bên cạnh đó, cách bác Rick kể về Nico càng làm tôi tò mò, không hiểu cậu bé này tốt xấu thế nào. Còn nữa, cuộc tìm kiếm của Grover và cái chết của thần Pan như một lời cảnh tỉnh về ý thức bảo vệ môi trường vậy. Thật hay và ý nghĩa
5
510105
2015-08-14 00:07:38
--------------------------
192503
5945
Mạch truyện càng được đẩy lên đến cao trào. Xuyên suốt ba tập truyện vừa rồi, Percy cùng những người bạn của mình đã vượt qua bao nhiêu thử thách. Các chi tiết xâu chuỗi rất hợp lý, logic. Phải nói rằng trí tưởng tượng và sáng tạo của Rick Riordan vô cùng phong phú. Bộ truyện này và Harry Potter là 2 tác phẩm văn học nước ngoài mà tôi thích nhất. Cách kể chuyện đặc sắc, ấn tượng. Còn đối với bìa truyện, tôi không thích cho lắm. Dù lấy màu đen huyền bí là chủ đạo, tôi vẫn thích bìa trước đây hơn.
5
598547
2015-05-04 19:05:32
--------------------------
153938
5945
Percy chuẩn bị cho cuộc chiến đối mặt với đạo quân hùng mạnh của titan Kronos. Trong khi đó, Trại con lai đối mặt với nguy cơ tấn công của Luke. Trong lúc chơi đánh trận, vô tình phát hiện ra một cánh cửa dẫn vào mê cung, Percy, Annabeth, Grover và Tyson phải lên đường tìm cách ngăn chặn không cho quân của Luke tiến vào mê cung. Trên đường đi họ đã gặp rất nhiều thứ, từ các vị thần cho đến các con quái vật, người trăm tay,... và cả kho luyện vũ khí của Kronos cùng với Typhoon đang bị giam giữ ở đó. Percy còn được gặp nữ thần xinh đẹp Calipso và đứng trước quyết định là tiếp tục hay trốn tránh nhiệm vụ của mình. Nhân vật từng xuất hiện trước đây là Rachel bây giờ quay lại với vai trò là người dẫn đường cho nhóm bạn ra khỏi mê cung. Kết thúc truyện là sự hy sinh và xám hối của Daedalus để phá hủy mê cung cùng đoàn quân của Kronos và sự hy sinh của nhiều thành viên Trại con lai. Lời tiên tri không thể tránh khỏi đang đến gần hơn với Percy. Câu chuyện vô cùng ky kỳ và hấp dẫn, tiếp nối được nội dung và hình thức của các tập trước.
5
291067
2015-01-27 21:25:35
--------------------------
