468287
10102
Cuốn sách là tập hợp những câu chuyện về các tấm gương hiếu thảo, mang lại giá trị nhân văn và đạo đức cùng các bài học vô cùng sâu sắc. Mình mua cho em mình học lớp 1 để nó tập đọc và trau dồi vốn từ cho tốt. Mỗi lần nó đọc xong hay hỏi nó cảm thấy thế nào, ban đầu cu cậu còn nhăn nhó nhưng mãi rồi cũng quen và nói trôi chảy. Đây là một cuốn sách rất đáng mua trong một rừng sách văn học cho thiếu nhi hiện nay, những giá trị cốt lõi thì không bao giờ cũ cả.
4
280989
2016-07-04 21:51:50
--------------------------
417647
10102
Cảm nhận đầu tiên của mình khi mua quyển sách này là Tiki giao hàng rất nhanh và cẩn thận, cho năm sao về mặt vận chuyển. Về chất lượng quyển sách được bọc sách cao cấp bookcare, cho năm sao luôn. Cuối cùng là phần quan trọng nhất làm nên một quyển sách chất lượng là mặt nội dung. Trong sách nêu ra những tấm gương hiếu thảo, mình vừa đọc vừa bậc cười khi thấy hình ảnh của chính mình đâu đó được lồng qua những nhân vật này. Quyển sách thực sự rất hữu ích cung cấp cho chúng ta những câu chuyện hay.
5
509425
2016-04-17 15:22:54
--------------------------
