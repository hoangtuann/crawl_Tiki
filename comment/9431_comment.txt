374780
9431
Trẻ em ấy mà, chỉ cần ba mẹ chịu ngồi chơi hoặc cùng đọc sách với chúng là chúng vui tưng tưng rồi. Do vậy nếu cha mẹ đọc và giải thích cho các em nghe những thắc mắc hay những câu hỏi ngô nghê, em sẽ dần yêu thích việc đọc sách bởi các em có trí tưởng tượng phong phú. Đảo hoang chính là cuốn sách giúp phát huy việc đó. Một cuốn sách địa lý được lồng ghép vào một câu chuyện phiêu lưu kì thú sẽ khiến cho tụi nhỏ mê tít. Những con vật, những hòn đảo, cuộc sống trên đảo,v.v... hoàn toàn mới lạ với các em. Cùng với giọng văn tưng tửng hài hước, tác giả đã phần nào giúp trẻ em tiếp cận được môn địa lý một cách tự nhiên và đầy yêu thích nhất.
4
63376
2016-01-27 09:47:16
--------------------------
287924
9431
Sách có cách tiếp cận kiến thức rất thú vị không giống như cách tiếp cận truyền thống dễ gây nhàm chán, lúc thì bắt đầu bằng một câu truyện, lúc thì bắt đầu bằng một câu hỏi hay đại loại thế, các kiến thức về đảo đưa ra rất phong phú và lạ thích hợp cho các bé thích tìm hiểu kiến thức mới, thích khám phá. Một điểm cộng của sách là có hình minh họa khá thú vị và hài hước khiến cho người đọc và đặc biệt là các bé thấy thích thú hơn, gây hứng thú trong việc đọc
4
153008
2015-09-03 09:36:17
--------------------------
286989
9431
Một cuốn sách địa lý hay và lý thú về các hòn đảo. Sách giúp giải đáp những thắc mắc về sự hình thành và biến mất của các hòn đảo, các loại đảo như đảo lục địa, hải đảo, quần đảo là gì, hệ động thực vật trên đảo, v.v. Mình thấy rất bất ngờ khi đọc thấy rằng ngọn núi cao nhất trái đất không phải đỉnh Everest trong dãy Himalaya mà chính là ngọn núi lửa Mauna Kea trên đảo Hawaii. Và còn rất nhiều điều ngạc nhiên khác. Sách in trên giấy xốp nhẹ nhưng vẫn còn vài lỗi sai chính tả.
4
470937
2015-09-02 09:51:36
--------------------------
254265
9431
Tôi chưa bao giờ cảm thấy thích thú với những hòn đảo trôi nổi ngoài biển khơi bao la đến như vậy.Cuốn sách này tuy không dày nhưng nó đã cho thấy chỗ đứng của mình trong giá sách của tôi.Câu chuyện của những thủy thủ xấu số, cuộc đời bất định của những hòn đảo, cả những loài thú mà tôi chưa bao giờ biết đến... tất cả đều gợi lên trong tôi sự tò mò khám phá.Nếu có dịp, chắc tôi sẽ ra đảo hoang sống thử như Robinson.Hì hì. Đọc xong tôi chỉ có một nỗi tiếc cho loài chim Dodo đáng thương.Đúng là sống hiền quá cũng không được mà!
5
280592
2015-08-05 09:35:08
--------------------------
248743
9431
Nếu bạn khoái Robinson Crusoe, bạn sẽ mê tít cuốn sách này. Nếu bạn chưa đọc Robinson Crusoe, đọc xong cuốn sách này bạn sẽ muốn tìm đọc. Nói chung đây là một cuốn sách hấp dẫn về đảo hoang - những hòn đảo sơ khai rậm rạp um tùm. Bạn tìm thấy trong sách này mọi thứ trên đảo, các loại động thực vật quen thuộc lẫn kì cục, thời tiết, thiên nhiên trên đảo... và sống quá lâu trên đảo thì bạn có thể biến thành "người rừng"!. Nói đùa thôi, không nghiêm trọng thế, chỉ là tôi đang cố bắt chước phong cách hài hước rất cà tưng của tác giả khi nói về khoa học. Đọc sách khoa học kiểu này vui lắm.
5
82774
2015-07-31 10:13:25
--------------------------
240808
9431
Đây có lẽ là quyển mỏng nhất trong series Horrible - nhưng nó chứa đựng tất cả những li kì, hấp dẫn, quay cuồng của những cuộc thám hiểm tìm kiếm những hòn đảo mới. Trên đó có những loài vật hay ho, những loài cây thú vị đậm chất đảo hoang bụi bờ, đầy tiềm năng để... phát triển du lịch biển đảo. Nhưng không phải đảo nào cũng tồn tại mãi mãi - nó xuất hiện và biến mất theo thời gian, giống như một trạm nghỉ chân tạm thời lí thú nhưng bất định. Tác giả lôi kéo ta đi đến những đảo hoang kì bí, bằng ngôn ngữ gần gũi và hình ảnh minh hoạ cực kì dễ thương. 
5
120276
2015-07-24 22:25:42
--------------------------
230400
9431
Bởi vì hồi bé yêu thích khoa học nên mình cố gắng sưu tập gần hết các quyển trong bộ sách "Khoa học Kinh Dị" này của nhà xuất bản trẻ. Mỗi quyển sách mặc dù nhỏ và khá mỏng, chỉ cần đọc trong một đêm thôi nhưng ngồn ngộn bao nhiêu kiến thức thực tế trong đấy.
"Đảo Hoang" là một nơi khá lạ đối với chúng ta, nhất là ở Việt Nam, còn đối với mình sau khi đọc xong cuốn Robinson Crusoe thì lại tò mò về nó hơn. Cách sinh tồn trong một thế giới hoang vu bốn bể nước muối, quan sát các loài động vật để chạy trốn cho nhanh,... hay tham gia vào những cuộc hành trình có thực trong lịch sử để trải nghiệm,... được trình bày ngắn gọn cũng rất chi tiết và thú vị. 
Một quyển sách địa lý khá hay dành cho những ai ưa khám phá và yêu bộ môn này.
5
382812
2015-07-17 14:30:13
--------------------------
217389
9431
Địa lí thật chán ngắt và buồn tẻ??? Có lẽ là bấy lâu nay ai cúng nghĩ thế nhưng thật ra là không phải vậy đâu. Chỉ cần đọc qua cuốn sách đảo hoang này bạn sẽ nhận ra rằng địa lí thật ra rất thụ vị. Với những thông tin ĐẢO điên, những sự thật bất ngờ, hình ảnh minh họa sinh động sẽ giúp chúng ta hiểu rõ hơn về sự hình thành đảo, đời sống nơi đảo hoang cũng vui không kém. Bạn sẽ thấy rằng hóa ra mọi điều bạn nghĩ về địa lí trước đây đều không đúng. Địa lí không những cực kì thú vị mà lại còn rất bổ ích . Một cuốn sách sẽ giúp bạn thay đổi suy nghĩ về địa lí và rất đáng đọc.
5
554214
2015-06-29 11:17:23
--------------------------
216307
9431
Mình mua quyển sách này hết sức tình cờ chỉ vì nhìn bìa quyển sách dễ thương quá nên mua mà thôi nhưng không ngờ chỉ với một trăm hai mươi tám trang sách thôi đã đem đến cho mình rất nhiều điều thú vị. Nhờ quyển sách này mình mới thấy kiến thức của mình quá là hạn hẹp. Hình vẽ minh họa trong sách rất là dễ thương. Nếu bạn nghĩ những kiến thức địa lý là khô khan thì bạn sẽ phải hoàn toàn thay đổi suy nghĩ của mình khi đọc quyển sách này, phong cách viết của Anita rất là hóm hỉnh làm cho những kiến thức ấy trở nên rất sống động. Đây là một quyển sách tuyệt vời để giúp bạn cải thiện kiến thức vật lý của mình đấy.
5
5317
2015-06-27 15:31:19
--------------------------
203297
9431
Cuốn sách địa lí ‘ kinh khủng ‘ nói về đảo hoang này là một chiếc thuyền gần như hoàn hảo dẫn bạn đến trước mắt một đảo hoang. Ở đó bạn được cảm nhận đầy đủ về vẻ đẹp cũng như rất nhiều điều bí hiểm của đảo hoang. Sách được trang trí bìa rất đẹp, còn được minh họa bằng nhiều tranh ảnh rất dễ thương, hài hước đến nỗi cười đau cả bụng.Nội dung là nói về sự phân bố các vùng đảo trên thế giới( có thể tham khảo nếu bạn muốn du lịch ở đảo ), sự hình thành mà thực ra đảo là cái đỉnh của một quả núi lửa, những thứ trôi dạt trên đảo,…Ngoài ra còn có nhiều cuốn sách của địa lí khủng khiếp nhưng thuộc lĩnh vực khác cũng rất lí thú, tham khảo để biết nhiều hơn về địa lí nha, không hề chán ngắt như bạn nghĩ đâu.
4
605634
2015-05-31 22:04:40
--------------------------
60536
9431
"Đảo hoang" thực sự là một cuốn sách hay. Hình vẽ minh hoạ trong sách rất đẹp, nội dung phong phú và từ ngữ cũng rất dễ hiểu đối với trẻ em. Lúc mới đọc, tôi nghĩ rằng cuốn này không hay bằng cuốn sách về thời tiết của cùng tác giả. Nhưng các con tôi lại phản đối ý kiến đó. Và quả thực càng đọc càng thấy nhiều điều thú vị. Không có nhiều thông tin về các phát minh khoa học, "Đảo hoang" hay theo một cách riêng, với rất nhiều câu chuyện kỳ lạ về sự xuất hiện và biến mất của các đảo "điên". Đặc biệt hơn, cuốn sách cung cấp cho các em nhỏ nhiều câu hỏi khó để “đố” giáo viên địa lý và điều đó giúp các em thêm yêu thích môn học này.
4
80813
2013-02-23 21:00:16
--------------------------
