554081
6081
Những giá trị đạo đức và lời dạy của cha ông sẽ cứu xã hội hiện đại này thoát khỏi sự động loạn và suy thoái. Sách để đọc và thực hành. Một cuốn sách quý!
5
2130638
2017-03-25 15:38:45
--------------------------
485550
6081
Phải nói là khi mua hai cuốn Luân Lý giáo khoa thư và Quốc văn giáo khoa thư, hai vợ chồng mình tranh nhau đọc vì quá thích, từ hình thức lẫn nội dung, sau này con lớn, dựa vào sách này mình sẽ dạy con từng chủ đề mỗi ngày, không thể tin được xã hội hiện đại ngày nay con người hầu như đã làm sai hết tất cả những gì sách này dạy rồi, giá trị con người suy đồi nặng nề. Cái mình thích ở sách này nữa là cách xưng hô, từ ngữ trong sách ngày nay không còn được dùng nhiều nữa, đọc rất thích, ngẫm thấy người xưa soạn sách cốt  mong muốn"một đứa con hiếu thảo trong gia đình, tất ở trường là học trò tốt, tất ngoài đường là đứa nết na"
5
385042
2016-10-04 09:01:25
--------------------------
475419
6081
Thời điểm phát hành cuốn sách này là khá lâu, nên ngôn ngữ đối với con trẻ bây giờ có thể hơi khó hiểu đôi chút, phải là những người sinh từ những năm 90 đổ lên thì còn thấy thân quen. Nhưng những bài học trong sách thì rất gần gũi, cách tiếp cận bằng những câu chuyện nhỏ, kết luận bằng thành ngữ ca dao rất dễ đi vào lòng người. Là người lớn rồi mà mình vẫn thấy những câu chuyện này vẫn đủ hấp dẫn, không giáo điều như sách ảnh bây giờ, tuy tràn lan nhưng toàn nói chuyện to tát. Mỗi nhà có trẻ con đều nên có cuốn sách này trong nhà, để trẻ đọc hiểu mà cũng là để lưu giữ giá trị lịch sử của cuốn sách.
Sách có bìa cứng, giấy mỏng nên nhẹ, nhưng cầm rất chắc tay. Là một cuốn sách tuyệt vời về nội dung và hình thức.
 
5
1280130
2016-07-14 14:41:32
--------------------------
467615
6081
Trong khi thị trường sách dành để giáo dục đạo đức của học sinh đang loạn lên với những câu chuyện như đi trên thủy tinh, chuyện cổ tích từ uống nước sọ dừa thành uống nước sọ người thì việc phát hành cuốn sách này thực sự là điểm sáng của Nhà Xuất Bản. 
Trong cuốn sách là tập hợp những bài học ngắn gọn, những câu hỏi cực kỳ dễ hiểu. Nó giúp cho các bậc phụ huynh rất nhiều trong việc giáo dục con cái trong những năm đầu đời của con. Thật tuyệt vời khi mà các nhà giáo dục của chúng ta ngày xưa đã biên soạn được ra những bài học như thế. 
Nhận được cuốn sách này mình rất háo hức vì đây là lần đầu tiên mình nhìn thấy được cuốn sách giáo khoa mà ngày xưa ông bà đã từng học. Chất lượng sách tốt, in rõ ràng, màu giấy không làm lóa mắt khi đọc.
5
312500
2016-07-04 07:40:24
--------------------------
460995
6081
Tôi mua cuốn sách này vì Dượng Tony giới thiệu.
Cuốn sách là những bài học ngắn định hướng sách suy nghĩ và hành động đúng đắn cho lứa tuổi thiếu nhi và thiếu niên.
Sách được sử dụng trong giảng dạy từ đầu thế kỷ XX nhưng cho đến bây giờ vẫn nguyên giá trị.
Tuy nhiên vẫn còn một số điều không còn phù hợp lắm trong thời buổi hiện đại, cha mẹ vẫn có thể dạy con những bài học này và nói với chúng rằng, đó là tiêu chuẩn đạo đức trong thời đại xưa, và hỏi con xem nghe xong câu chuyện này thì con thấy có còn đúng trong xã hội bây giờ không. Gia đình cũng có thể cùng nhau thảo luận xem nên thế nào, không nên thế nào trên tinh thần tôn trọng ý kiến con trẻ.
4
438736
2016-06-27 14:30:20
--------------------------
451871
6081
Cầm quyển sách lên đầu tiên mình cảm thấy cực kỳ ấn tượng về cách thiết kế trình bày từ bìa sách cho tới cách trình bày nội dung và hình ảnh minh họa. Cảm giác như thật sự được cẩm quyển sách của những năm đầu thế kỷ trước vậy. Cực kỳ ấn tượng. Nội dung thì tuyệt hay không có gì để bàn cãi, rất đơn sơ mà cực kỳ ấn tượng, nói về những chủ đề đạo đức  thiết thân trong cuộc sốn, được chia theo lứa tuổi: lớp đồng ấu và lớp sơ đẳng. Có cảm giác nuối tiếc là nếu chương trình giáo dục hiện nay sử dụng cuốn này thì hay quá.
5
171151
2016-06-20 10:25:52
--------------------------
449962
6081
Cuốn sách này không thể nhận xét chỉ bằng một vài từ. Tuy nhiên, mình xin chia sẻ một vài cảm nhận cá nhân của mình. Cuốn sách này tuy được viết cho trẻ con học vào những năm đầu thế kỷ 20 nhưng giá trị của nó đến giờ vẫn còn nguyên. Đôi lúc mình cảm thấy ghen tị với các em hồi đó vì chúng được học một giáo trình mang đầy tính nhân văn như vậy. Sách giáo khoa bây giờ cũng hay, cũng đẹp nhưng mang nặng tính chính trị quá. Đọc cuốn này mà thấy nhẹ nhàng, dễ hiểu, dễ nhớ chứ không thấy phải nặng nề để mà học xong trả bài cho thầy rồi quên luôn. Nên cho con trẻ bây giờ học những giáo trình như vậy.
5
634855
2016-06-18 11:24:04
--------------------------
449771
6081
Cuốn sách Luân lí giáo khoa thư này cũng như cuốn Quốc văn giáo khoa thư đều nêu ra những mẩu chuyện đơn giản nhưng ý nghĩa sâu sắc  mang tính giáo dục cao về đạo đức làm người. Không những đẹp về nội dung sách còn đẹp về hình thức bìa sách cứng màu đen nhũ hồng cam có bìa rời bọc ngoài nữa. Mực in tốt màu giấy hơi vàng. Tiki bảo quản cẩn thận giao hàng nhanh. Mình cảm thấy hoàn toàn hài lòng. Hi vọng sau này sẽ mua được nhiều sách hay ở tiki thêm nữa.
5
712474
2016-06-17 23:45:17
--------------------------
419862
6081
Cuốn Luân lý giáo khoa thư này không thể nhận xét chỉ bằng một từ. Cảm nhận của mình có khá nhiều  về nó. Bây giờ có ít sách từ ngày xưa còn và được biên tập lại như cuốn này. Cuốn sách này mình nghĩ nên có trong tủ sách của mỗi người, đặc biệt là những người làm cha làm mẹ. Nội dung sách rất nhân văn và tình người. Bằng những câu chuyện giản đơn để dẫn dắt và những câu hỏi ôn tập lại nội dung cũng như đúc kết, rút ra bài học đạo đức chứa đựng đầy tình thương bao la và nghĩa lý. Rất tốt cho việc dạy con trẻ. Tuy có một số từ sẽ khó hiểu đối với trẻ thơ. Sách đẹp bền và trình bày tốt !
5
1302823
2016-04-22 09:30:30
--------------------------
418614
6081
Biết đến “Quốc văn Giáo khoa thư” và “Luân lý giáo khoa thư” qua trang Tony Buổi Sáng, mình đã không ngần ngại đặt hai cuốn này qua Tiki. Đây là hai bộ sách giáo khoa tiếng việt được dạy song hành ở hệ thống trường tiểu học tại Việt Nam suốt thập niên nửa đầu thế kỷ XX.
Với riêng “Luân lý giáo khoa thư”, cuốn sách là những bài học nhẹ nhàng gần gũi, đề cao giáo dục đạo đức với trẻ nhỏ. Các em có thể dễ dàng học thuộc và ghi nhớ một cách dễ dàng, từ đó áp dụng vào đời sống và từ từ xây dựng nên nhân cách của các em.

5
41486
2016-04-19 16:24:33
--------------------------
386849
6081
Nội dung của sách là những câu chuyện ý nghĩa. Tuy nhiên là truyện rất ngắn nên mình nghĩ nó thích hợp để dạy cho trẻ em. Những ai có con nhỏ thì nên mua quyển sách này để hàng ngày dạy cho con những cách ứng xử trong cuộc sống, có ví dụ cụ thể nên các em nhỏ sẽ thích và học theo. Tuy nhiên đối với người lớn thì sách có vẻ nhàm chán vì những câu chuyện trong sách rất ngắn và ý nghĩa rất thực. Mỗi câu chuyện sẽ mang một ý nghĩa khác nhau.
2
782427
2016-02-26 11:09:07
--------------------------
373240
6081
Biết được tên quyển sách này và Quốc văn giáo khoa thư từ Tony buổi sáng và đến nay mới mua được cầm trên tay.
về hình thức: bìa cứng, chắc chắn, mang hơi hướng hoài cổ nên rất thích
về nội dung: đây là những bài học ngắn gọn nhưng giá trị to lớn. Trong thời buổi hiện nay, khi mà chúng ta ngày càng tất bật chạy theo những xô bồ của cuộc sống mà đôi lúc bỏ quên những giá trị nhân văn. Với bản thân, đây là những giá trị tốt đẹp để "tu thân", hướng dạy cho thế hệ con cháu sau này hiểu được con người với những giá trị tốt đẹp là như thế nào với cuộc sống phát triển văn minh.
4
502287
2016-01-23 10:43:17
--------------------------
330451
6081
Cả 2 cuốn: Quốc văn giáo khoa thư và Luân lý giáo khoa thư - Bìa cứng bên ngoài, lẫn chất lượng giấy bên trong thì miễn bàn, vì quá đẹp. Màu giấy không phản ánh sáng, dễ đọc và rất nhẹ. Nói tóm lại xứng đồng tiền bát gạo. 
Về nội dung, mình đọc trọn hết và ngẫm nghĩ theo cách riêng nên thấy được nhiều giá trị riêng, đưa ra nhận xét sơ bộ như sau:
- Không nên đưa cho con nít đọc trước khi mình lý giải, truyền đạt thấu đáo cho nó-tại sao? : Giá trị nhân văn thì lưu truyền mãi nhưng giá trị thời đại thì nên tự mình xem xét lại cho phù hợp ví thử như cách xưng hô trong sách mang tư tưởng "trọng nam quên nữ"( và còn nhiều nữa).
4
335356
2015-11-02 17:17:35
--------------------------
324567
6081
Trước hết mình muốn khen dịch vụ giao hàng và bọc sách của Tiki, sách bọc nilon rất đẹp, giao hàng cũng rất nhanh, mình rất hài lòng khi mở hộp sách ra. Sau là về nội dung cuốn sách, tất nhiên là đến thời điểm hiện tại thì nhiều điểm trong cuốn sách không còn thật sự phù hợp nữa, nhưng nó vẫn có giá trị tham khảo rất tốt, có thể đọc cho con nghe và giải thích thêm về sự khác biệt giữa thời cuốn sách được dùng để giảng dạy trong nhà trường với thời điểm hiện tại, để con hiểu và tự đúc rút được nhiều điều :)
4
36066
2015-10-21 14:45:06
--------------------------
321888
6081
Mình mua sách này khá lâu nhưng hôm nay mới viết nhận xét vì mỗi ngày mình tìm một câu chuyện trong sách để đọc cho con nghe. Con mình được 4 tuổi nên có chuyện hiểu, có chuyện không nhưng tối nào cũng nài mẹ đọc cho nghe. Vì vậy, mình chỉ chọn những câu chuyện đơn giản, dễ hiểu và đổi tên nhân vật từ Tí, Sửu, Mẹo... thành An, Tài, Tú...để bé dễ hiểu hơn. 
Sách dạy làm người, yêu thương ông bà cha mẹ, thầy cô bạn bè, quê hương đất nước. Nhưng câu văn theo lối viết xưa nên phải dạy luôn bé câu chữ ông cha ngày xưa. Khá hài lòng về sản phẩm
3
772003
2015-10-15 08:38:45
--------------------------
319985
6081
Khi mới nhìn thấy cuốn sách này trên tiki, mình nghĩ nó rất nặng nề..kiểu giống sách triết, loại lý luận không thể hiểu được...
Nhưng khi đọc mới thấy giá trị văn hoá nhân văn mà ông cha ta để lại...
Mình có 1 đứa cháu nhỏ đang tuổi ăn tuổi lớn, cuốn sách này hợp với nó nhất, có thể dạy nó hiếu thảo, kính trên nhường dứơi, giữ phép tắc luật lệ, yêu thương nhân loại...như một cách dạy đạo đức làm người...
Hy vọng mai sau cuốn sách này có thể đưa vào trừơng học, thay thế cho những cuốn sách phiên bản lỗi hiện nay...

4
728441
2015-10-10 09:11:17
--------------------------
308044
6081
Sách gồm những bài học nhẹ nhàng, không giáo điều, đề câp những bài học đạo đức của người Việt Nam. Tuy rằng việc học ngày nay đã khác xưa, nhưng dầu cho thế hệ nào, thời đại nào cũng cần phải biết tôn vinh những đạo lý muôn thưở : công cha, nghĩa mẹ, học trò biết ơn thầy...Sách trình bày ngắn gọn, xúc tích và dễ nhớ với trẻ nhỏ.Sách bìa cứng, ngoài bọc thêm bìa rời., giấy tốt.Tôi mong rằng sách này được dạy làm môn Đạo Đức ở bậc Tiểu học , thì hay biết mấy!
5
512701
2015-09-18 14:18:30
--------------------------
307608
6081
Tôi rất thích hai cuốn Luân Lý Giáo Khoa Thư và Quốc Văn Giáo Khoa Thư, nhìn hai cuốn sách tôi thấy nó thật đặc biệt, tôi thấy sự trân trọng với kiến thức và đạo đức của cha ông ta, tôi thấy tiếc vì bây giờ những kiến thức này mới xuất hiện lại trong xã hội chúng ta, giá như nó xuất hiện lại trong khoảng hơn 30 năm trước thì những điều xấu xa như bây giờ sẽ bị hạn chế rất nhiều. Những bài học về lòng hiếu thảo, kính trên nhường dưới, giữ phép tắc trong xã hội rất cần thiết trong hai cuốn sách này cần phải phổ biến hơn nữa cho con cháu chúng ta. Bìa sách cứng, ép nhũ hồng cam, font chữ đẹp, trang giấy vàng hoài cổ - Cuốn sách đẹp từ hình thức đến những bài học bên trong.
5
452607
2015-09-18 10:33:26
--------------------------
307035
6081
Các mẩu truyện chứa đựng những bài học đạo đức cơ bản nhất. Ngắn gọn mà hàm súc, không cần diễn đạt dài dòng vẫn truyền đạt những bài học đạo đức có giá trị một cách hiệu quả. Vì là sách giáo khoa của nửa đầu thế kỉ hai mươi nên có những tư tưởng đến giờ không đúng với thời đại hoàn toàn. Dù nhà xuất bản đã lược bỏ bớt nhưng để đảm bảo cho độc giả cảm nhận chân thực về cuốn sách, vẫn có những khía cạnh không phù hợp và ta nên đọc một cách chọn lọc
4
419096
2015-09-17 22:19:24
--------------------------
305857
6081
Thời gian vừa qua tôi hốt hoảng về những cuốn sách dạy kỹ năng mềm cho trẻ được mọi người lan truyền trên mạng. Phải nói là thật đáng sợ. Sách dạy kỹ năng mềm mà dạy trẻ đi trên thủy tinh vỡ rồi cưa bom,... Cũng lại phải nhờ tới Tony tôi mới biết tới cuốn sách này. Dù chưa có con nhưng tôi muốn dành cuốn sách này để dạy con về sau. Những bài học trong cuốn sách thật sự rất ngắn gọn, dễ hiểu và dễ dàng để dạy và hướng dẫn các con. Về chất lượng cuốn sách thật sự tôi rất ưng, sách bìa cứng dày dặn, nét mực rõ ràng dễ đọc. Mong rằng các thầy cô giáo hãy dùng cuốn này để dạy con trẻ.
4
470181
2015-09-17 10:37:12
--------------------------
302860
6081
Cuốn sách này từng được sử dụng như cuốn sách đạo đức dành cho học sinh tiểu học ngày xưa. Nếu bạn có con hoặc cháu nhỏ, đây là quyển sách đáng để mua. Từng câu chuyện nhỏ mang tính giáo dục cao được trình bày đơn giản, dễ hiểu và rất dễ ứng dụng, liên hệ với thực tế. Có thể đọc cho trẻ tầm 5-6 tuổi trở lên.
Bìa sách cứng, đẹp, giấy khá tốt. Tuy nhiên, hình minh họa hơi nhỏ, chỉ có màu trắng - đen và không được sinh động lắm.
Có thể tìm đọc thêm "Quốc văn giáo khoa thư" với cùng 1 thể loại.
4
538
2015-09-15 15:04:40
--------------------------
302837
6081
Vừa mới mua cuốn sách này cách đây 2 tuần, cảm xúc đầu tiên khi cầm sách lên là bất ngờ vì không ngờ sách là loại bìa cứng đẹp và chắc chắn như vậy. lật ra bên trong thì thấy màu giấy ngả vàng cảm giác rất hoài cổ, từng bài học đều có hình minh họa, cảm thấy như mình được cầm trên tay lại cuốn sách Đạo Đức những năm tiểu học vậy. Mỗi bài học ngắn gọn, đơn giản nhưng ý nghĩa thì muôn đời còn đó, dạy con người ta về những giá trị đạo đức cơ bản nhất mà một người cần có. Sách thích hợp cho mọi lứa tuổi, nhất là trẻ nhỏ để giáo dục nhân cách các bé từ khi còn non.
5
199482
2015-09-15 14:52:40
--------------------------
297980
6081
quyển sách này có từ rất lâu thời ông bà chúng ta, và được các tác giả biên soạn, xuất bản lại để phù hợp với thời đại mới. tuy nhiên những bài học, những luận lý vô cùng đơn giản nhưng đáng suy ngẫm, đáng lưu giữ để dạy lại cho các con cháu chúng ta sau này. từ việc tu thân là chính, quyển sách này giải nghĩa những từ ngữ một cách đơn giản nhất và dễ hiểu nhất, làm cho chúng ta có hứng thú khi đọc, những tình huống vỡ khóc vỡ cười làm tăng thêm sự lôi cuốn từ người đọc.
4
399161
2015-09-12 12:31:06
--------------------------
297631
6081
“Luân Lý Giáo Khoa Thư” và “Quốc Văn Giáo Khoa Thư” là hai bộ sách giáo khoa tiếng Việt được dạy song hành ở các trường Tiểu học Việt Nam trong suốt những thập niên thuộc nửa đầu thế kỷ XX. Trong đó, cuốn “Luân Lý Giáo Khoa Thư” là những lời khuyên về vấn đề luân lý, đạo đức và công dân giáo dục như phải tôn kính và vâng lời cha mẹ, anh chị em trong nhà nên hòa thuận, nhường nhịn lẫn nhau, đi học phải chuyên cần.. Điều đó thể hiện rất rõ trong phương châm giáo dục là “Tiên học lễ, hậu học văn” của nước ta..
4
654655
2015-09-11 23:45:07
--------------------------
295864
6081
Nếu bạn muốn tận dụng thời gian chờ trong ngày, bạn không thể giành thời gian 1-2 tiếng để đọc sách, thì quyển này là lựa chọn phù hợp vì mỗi đề mục của sách không quá dài, không phải là những câu chuyển kể dài dòng, không phải mất công làm dấu hoặc nhớ xem lần trước mình đọc đến phần nào rồi, trang nào rồi. Sách tuy giành cho học sinh tiêu học nhưng với tôi người lớn cũng nên đọc qua nó, đọc lại trong thời gian rỗi trong tháng. Những bài học đơn giản, thực dụng, dễ hiểu, nhưng để hiểu sâu sắc cần phải có thời gian luyện tập, kiên trì. Tôi mua sách này ở nhà sách Fahasa, bìa cứng, chắc chắn, dễ bảo quản.
4
419763
2015-09-10 18:15:12
--------------------------
295316
6081
Mình được biết tới cuốn "Luân Lý Giáo Khoa Thư" này qua giới thiệu của dượng Tony, nên mình đã tìm hiểu và đặt mua cho cháu mình. Sách có bìa cứng với chữ in nhũ trên bìa rất đẹp, mỗi tội giấy in thì không được đẹp lắm không hiểu có phải mục đích của sách là trông muốn cổ cổ nên nhà xuất bản đã lựa chọn loại giấy này không, về nội dung thì mình thấy đây là những bài học đạo đức thời xưa dạy các bé cách làm người và biết đối nhân xử thế, những thứ mà mình thấy sách giáo khoa bây giờ không làm nổi bật được, sách được chia làm 2 giai đoạn là đồng ấu và sơ đẳng.
5
153008
2015-09-10 09:37:08
--------------------------
293396
6081
Là cuốn sách mình tìm bấy lâu, vì ba mình cũng học quyển này khi còn nhỏ. Những gì ba mình dạy tụi mình đều giống như những gì có trong sách này. Cuốn này kết hợp với Quốc văn giáo khoa thư nữa trở thành một cặp đôi hoàn chỉnh. Mình sẽ dùng quyển này để dạy đạo đức và cách sống, đối nhận xử thế cho cháu của mình. Bây giờ tụi nhỏ quá thiếu những điều cần phải biết nhưng không được dạy bài bản ở trường. Ước gì cuốn này làm môn Đức dục ở tiểu học thì hay biết bao.
5
112788
2015-09-08 11:52:19
--------------------------
291361
6081
Đúng như tên gọi của quyển sách. Quyển sách dạy cho trẻ nhỏ những lẽ phải trong cuộc sống trong gia đình (ông bà, cha mẹ, anh chị em, người lớn), nhà trường (thầy cô và bạn bè), xã hội như truyền thống dạy học của Việt Nam “Tiên học Lễ, Hậu học Văn”. Con người văn có hay, chữ có đẹp đến đâu nhưng không có lễ giáo thì cũng không hoàn thiện được. Quốc văn giáo khoa thư và Luân lý giáo khoa thư và bộ sách không thể tách rời nhau trong giáo dục các em nhỏ. Các em là búp trên cành cần được nâng niu nhưng cũng cần được uốn nắn chỉ dạy để hình thành được nhân cách và đạo đức. Vì dạy học không chỉ có dãy kiến thức mà cần phải dạy cách làm người đúng nghĩa.
5
355080
2015-09-06 10:57:19
--------------------------
290930
6081
Luân lý giáo khoa thư cùng với Quốc văn giáo khoa thư là hai cuốn sách không những người lớn nên đọc mà các bé học sinh, thiếu nhi cũng nên đọc. Nếu ai có con nhỏ chưa đọc được, họ có thể dùng sách thay cho chuyển kể để đọc cho các bé, vì sách dạy làm người, đức tính cần thiết để phát triển trí óc và tâm hồn trẻ thơ. Sau khi được dượng Tony Buổi Sáng giới thiệu cho đọc, mình đã quyết định mua hai cuốn này. Quả thực sách làm mình rất hài lòng. Mặc dù đã qua thời học sinh tiều học từ rất lâu, nhưng càng đọc mình càng thấm thía những lời dạy trong sách. Hai cuốn sách này xứng đáng nằm trong kệ sách nhà bạn.
5
277975
2015-09-05 21:58:36
--------------------------
288662
6081
Sách có bìa dày, đẹp, làm như dạng những quyển sổ tay bìa cứng dày dặn, chắc chắn. Lật lớp bìa bao ngoài là bìa cứng có dòng chữ nhũ vàng rất sang trọng, tiếc là giấy không được đẹp lắm. Nội dung sách là những câu chuyện giáo dục nho nhỏ về tình cảm gia đình, bạn bè, thầy cô, cách lễ phép, kính trọng cha mẹ, người trên, biết kính trên nhường dưới, quý mến bạn bè,..... Một quyển sách quý về đạo đức cho các bạn nhỏ tiểu học. Giữa bộn bề vàng thau lẫn lộn, sách nội dung không phù hợp cho các bé hiện nay thì đây là một trong những quyển sách thật sự có tính giáo dục đạo đức cho thiếu nhi. Mình cũng sẽ đọc cho con mình những câu chuyện bổ ích trong sách. 
5
663152
2015-09-03 19:30:34
--------------------------
288549
6081
Với recomment của Tony, tôi đã biết đến và mua cuốn sách này, thật là một cuốn sách tuyệt vời. Quốc văn giáo khoa thư và Luân Lý Giáo Khoa Thư là hai bộ sách giáo khoa tiếng Việt được dạy song hành ở các trường Tiểu học Việt Nam trong suốt những thập niên thuộc nửa đầu thế kỷ XX. Đọc cuốn sách từ đầu thế kỷ 20 xa xôi mà tôi giật mình vì sao nó gần gũi và thân thương quá. Những bài học hết sức nhẹ nhàng, không giáo điều, không bề trên, đề cập những bài học đạo đức của người Việt Nam. Những điều này tôi không còn cảm nhận được ở các SGK hoặc giáo dục công dân hiện nay. Truyện dùng ví dụ hành động cụ thể để giáo dục, gợi mở đạo đức cho trẻ em.
5
726143
2015-09-03 17:26:41
--------------------------
288066
6081
Mình đã nhận quyển sách này và tối 2 mẹ con đã cùng xem. Mình chỉ dạy cho con 2 bài mỗi buổi tối thôi, cách mình làm là đọc câu truyện rồi hỏi con các câu hỏi theo sách, con trả lời xong thì mình nhận định và hướng dẫn thêm cho con.
Sách chia bố cục dễ hình dung, cách trình bày cũng dễ cho phụ huynh hướng dẫn con. Tuy là có một số chỗ câu từ không có phù hợp với hiện tại nữa nhưng mình có thể tùy biến để phù hợp. 
Nhìn chung tinh thần và phần hồn của sách rất đáng trân trọng.
Quyển sách hay để các Ba mẹ đọc và hướng dẫn con cái.
5
197056
2015-09-03 11:23:35
--------------------------
287693
6081
Mình biết cuốn sách này cũng thông qua sự giới thiệu trên facebook của Dượng Tony. Nó quả thực khiến mình rất hài lòng thông qua những câu chuyện ngắn gọn nhưng lại chứa đựng những bài học về luân lý làm người rất sâu sắc. Cuốn sách này rất đáng là một cuốn sách nên có trong mọi nhà để dạy dỗ con cháu. Thông qua cuốn sách này, mình cũng hiểu thêm được phần nào về giáo dục của ông cha ta thời xưa, hồi đầu thế kỉ XX. Hình thức sách cũng rất đẹp, bìa cứng có thêm một lớp bìa mềm ở ngoài. Và cũng rất hài lòng về sự giao hàng nhanh của Tiki, chắc chắn mình sẽ còn mua sắm trên Tiki nữa!
5
772163
2015-09-02 22:39:57
--------------------------
286925
6081
Mình mua cùng quyển Quốc Văn Giáo Khoa thư khi thấy bác Tony  buổi sáng giới thiệu bộ sách này. Một phần cũng vì tò mò muốn biết cách đây 100 năm cha ông ta được học gì thông qua 2 quyển giáo khoa trên. Cách trình bày sách rất khoa học, bài học là những mẩu chuyện ngắn về đời sống gia đình, về cách yêu thương, đối nhân xử thế với người lớn và với những người xung quanh. Ngắn gọn, xúc tích và dễ nhớ với trẻ nhỏ. 
Sách thiết kế dày đẹp, giá lại rẻ hơn khi mua ở Tiki. 
5
282976
2015-09-02 08:41:27
--------------------------
286394
6081
Cảm nhận ban đầu là rất tốt, rất tôn trọng với thiết kế thêm bìa mềm bao quanh ngoài bìa cứng, bìa mềm và bìa cứng làm rất chỉnh chu. Sách được đóng gói kỹ và được bao nilon tốt.
Chất lượng sách (theo ý kiến cá nhân mình thì mình thích chất liệu giấy như vậy, rất hoài cổ, nhưng phải cẩn thận, rất dễ rách nếu dính nước và bị bẩn) ok. Nội dung sách chủ yếu để đọc cho trẻ nhỏ nghe và mình giản giải cho trẻ hiểu, người lớn cũng nên đọc để tự nhắc lại bản thân &&.
Cho 10 điểm về sự hài lòng ^^
Thank tiki
5
123909
2015-09-01 18:03:09
--------------------------
286222
6081
Đọc cuốn sách từ đầu thế kỷ 20 xa xôi mà tôi giật mình vì sao nó gần gũi và thân thương quá. Những bài học hết sức nhẹ nhàng, không giáo điều, không bề trên, đề cập những bài học đạo đức của người Việt Nam. Những điều này tôi không còn cảm nhận được ở các SGK hoặc giáo dục công dân hiện nay. Truyện dùng ví dụ hành động cụ thể để giáo dục, gợi mở đạo đức cho trẻ em. Tôi nghĩ bộ giáo dục nên nghiên cứu đưa những bài học này vào chương trình cải cách giáo dục hiện nay.
5
465332
2015-09-01 15:24:48
--------------------------
264915
6081
Gọi là sách nên có trong nhà, vì cuốn sách là tập hợp những bài học dạy làm người, khuyên người ta biết hướng thiện, biết kính trên nhường dưới thông qua những câu chuyện nhỏ. Bài học trong sách thường gói gọn trong một trang giấy, bao gồm một câu chuyện ngắn dễ đọc dễ hiểu dễ nhớ, vài câu hỏi để từ đó người đọc có thể suy rộng trong những tình huống tương tự, mấy dòng chú thích, đôi khi còn có cả hình minh họa đi kèm, thực sự rất ngắn gọn xúc tích. 

Sách bìa cứng, ngoài bọc thêm bìa rời. Bỏ áo bìa ra thì bìa bên trong chữ ép nhũ khá đẹp, giấy tốt. Và vì bìa cứng nên không sợ cong vênh, quăn mép bìa, dễ bảo quản.
5
128054
2015-08-13 15:56:20
--------------------------
