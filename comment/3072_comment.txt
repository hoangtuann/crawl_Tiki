460140
3072
Cuốn sách không phải quá hay , vẫn là ngôn tình hơi mơ mộng và không thiết thực lắm về một cô gái ngốc tìm được chàng hoàng tử cho bản thân . Tuy nhiên đây là thể loại truyện võng du (chuyện tình trên game online nên thật sự không nên đòi hỏi quá nhiều . Nhưng dù sau câu chuyện vẫn rất dễ thương và cũng có tí rất thực ấy chứ . Câu chuyện tình giữa Kha Dĩ Mạc và Ninh Tâm cực kì đáng yêu , trong sáng và một cái kết thật đẹp là điều dĩ nhiên .
4
605740
2016-06-26 20:29:04
--------------------------
455867
3072
Tập 2 thì mình cũng chỉ mạn phép nói rằng hay không thua phần 1, tuy phần 2 có nhiều khúc rơi nước mắt hơn nhưng cũng không làm giảm độ hài hước và hay của nó.
Nhẹ nhàng, không rườm rà, đọc không dứt ra được, nhưng so với yêu em từ cái nhìn đầu tiên thì mình vẫn thích yêu em từ cái nhìn đầu tiên hơn, có lẽ vì mình yêu đại thần Tiêu Nại với Vy Vy mĩ nhân quá rồi :)) Nhưng theo đại đa số thích võng du thì mình thấy về phần này thì đại thần em nuôi anh đáp ứng đủ hơn
4
936775
2016-06-22 21:56:45
--------------------------
431552
3072
Truyện dịch rất khá, bìa siêu siêu dễ thương, chất lượng giấy cũng rất tốt, lại còn có cả bookmark và huy hiệu nữa chứ, thích chết đi được. <3 <3 <3 Phần cuối này trước đó rất lâu tôi xem qua trên mạng thì dịch rất tệ, dịch sai ý sai nghĩa tè le, đến nỗi tôi phải đi tìm bản convert đọc mới hiểu được đoạn đó có nghĩa gì. Khi cầm cuốn truyện trên tay tất nhiên tôi lật ra ngay khúc đó vì bị ấn tượng sâu sắc quá mà =)) nhưng thật sự không làm tôi thất vọng, đoạn đó dịch rất đúng, tuy là chưa được hay đến mức văn phong đẹp đẽ nhưng không sai là đã ổn rồi a. :v 5 sao cho bộ truyện này. <3
5
104893
2016-05-17 16:12:20
--------------------------
394794
3072
Đại thần từ trò chơi đến thực tế. Thật đáng để người ta ao ước. Anh nhẹ nhàng tinh tế, cô ngây thơ hồn nhiên, chuyện tình của họ có lẽ sẽ đẹp hơn nếu a k mắc bệnh. Cô hồn nhiên nhưng k có nghĩa vô tâm, cô từ bỏ sung sướng để theo anh chịu sự giày vò giữa sống và chết. Nhưng trời cao có mắt, những người có tình cuối cùng rồi cũng ở bên nhau.
Tác giả mới, tác phẩm mới và rất nhiều fan mới. Rất mong sớm đc gặp lại tác giả này.
4
767521
2016-03-10 22:23:43
--------------------------
378864
3072
1314520...một đời một kiếp anh yêu em là câu chuyện tình ngọt ngào nhưng cũng đầy trắc trở của đôi bạn trẻ Kha Dĩ Mặc và Ninh Tâm
Kha Dĩ Mặc-Mặc Thương đại thần Kiếm Khách tiêu sái đã cứu Ninh Tâm-tiểu Linh Xu bé nhỏ khi nhân vật khác mở hồng danh... Bọn họ vốn là 2 đường thẳng song song, nhưng sự giúp đỡ của Dĩ Mặc làm Ninh Tâm biết ơn và cô quyết định bao nuôi Mặc Thương đại thần-Kha Dĩ Mặc... Sau đó bao nhiêu tình huống dở khóc dở cười cùng với những tai hoạ liên tiếp tìm đến cô bạn nhỏ Ninh Tâm ngây thơ này
Kết thúc HE làm mk rất hài lòng, mk k muốn đọc trên mạng vì dịch k đc chính xác lắm, cuối cùng mk đặt mua hàng qua Tiki...cuốn Đại thần, em nuôi anh tập 2 hay lắm! Em cảm ơn Tiki nhiều ạ!!! ^^
5
1137497
2016-02-08 05:06:30
--------------------------
365282
3072
Đây là bộ võng du đầu tiên mình đọc, do đã xem hết trên mạng rồi chứ thực  không thích phong cách bìa sách lắm. 
Phải nói các nhân vật trong truyện đều có nét tính cách rất đặc sắc, từ cả gia đình Ninh Tâm đến Kha Dĩ Mặc cùng đám bạn. Tình cảm vì thế mà phát triển theo hướng khó lường luôn. Mình đặc biệt thích Ninh Tâm, chắc tại đây là nữ chính phải dùng cụm từ độc nhất vô nhị để tả. Nhiều khi thấy tội cho người xung quanh cô nàng này nhưng cũng khâm phục sự thẳng thắn và dũng cảm của Ninh Tâm. Tóm lại bạn hãy đọc thử, sẽ thấy cuộc sống có nhiều cách nhìn như thế nào!
4
358411
2016-01-07 19:15:54
--------------------------
363723
3072
Mình rất thích thể loại truyện võng du kể từ khi đọc " yêu em từ cái nhìn đầu tiên ". Thú thực là mình đã cố gắng tìm rất nhiều truyện võng du nhưng chưa có truyện nào làm mình hài lòng cả . " Đại thần em nuôi anh " là một ngoại lệ . Ngay từ tên truyện đã thể hiện được đây là một câu chuyện hài hước . Quả thật như vậy , với lối viết giữa 2 thế giới game - ngoài đời thực song song giúp người đọc theo được từng chương truyện khá rõ ràng . Chuyện tình đáng yêu giữa 1 Kha Dĩ Mặc ấm áp và một Ninh Tâm ngây thơ đã để lại nhiều ấn tượng cho người đọc .
4
743273
2016-01-04 21:03:37
--------------------------
335120
3072
Dù mua đã lâu nhưng bây giờ mình mới xem hết bộ truyện 2 cuốn này. Cực cưng nhân vật Ninh Tâm, cô nàng quá ngây thơ, đáng yêu, cà sống cực kì lạc quan và yêu đời. Một Mặc Thương toàn năng trong game, một Dĩ Mặc trầm lặng trong thực tế đều đổ gục bởi độ dễ thương cũng như ngây thơ của cô nàng Ninh Tâm này.
Vận xui cực điểm của cô nàng chỉ ở trong duy nhất 1 phụ bản, nhưng vận may thì toàn trò chơi không ai sánh bì lại cô. Chỉ là đuổi theo con hồ ly nhỏ, hay cho con rồng nhỏ ăn mà cũng nhận được nhiệm vụ ẩn và vật phẩm rơi ra thì toàn năng mà không ai có được.
Tuy thiết kế bìa cả hai cuốn đều giống nhau ( chỉ khác cái màu chử ) nhưng miêu tả đúng với tính vách của nhân vật trong đó :3
5
754714
2015-11-10 16:15:03
--------------------------
325190
3072
Đọc truyện này thấy vui dễ sợ. Nữ chính Ninh Tâm thật thà đến mức ngốc nghếch đáng yêu,nói câu nào là không ai đỡ được câu đó. Suốt từ đầu tập 1 đến nửa tập 2 là khung cảnh trong game Thần thoại,là nền để diễn ra mối tình online giữa Ninh Tâm và Đại Thần... Về đoạn kết thúc,sau khi 2 nhân vật chính gặp nhau ngoài đời thì diễn biến hơi nhanh. Gặp là đồng ý yêu luôn,không có những màn theo đuổi,không có xung đột kịch tính hay màn yêu đương thắm thiết. Thời gian 3 năm sau khi 2 người quyết định sang Mỹ cùng nhau chữa bệnh tim cho Kha Dĩ Mặc và phẫu thuật thành công thì tác giả viết quá ngắn,chỉ vài chục trang. Gây nên cảm giác hơi thiếu thiếu và hụt hẫng.
Tuy nhiên đây cũng là 1 tác phẩm đáng đọc,thư giãn tinh thần rất tốt ..
4
294512
2015-10-22 23:02:35
--------------------------
307245
3072
Mình đặt một lần cả hai quyển luôn nên đọc cũng không bị ngắt khúc đi. Nội dung của tập 2 này thì tất cả đều tập trung vào chuỵên tình cảm của Ninh Tâm và Dĩ Mặc ở ngoài đời, truyện khắc hoạ nội tâm nhiều hơn, giọng văn hài hước nhưng cũng không kém phần dễ thương. Đại thần cũng quá mức tuyệt vời đi chứ, vừa là hảo soái ca, tâm tính cũng quá tốt, Ninh Tâm thì đúng chuẩn là một "tiểu bạch thố" vừa ngây thơ vừa đáng yêu. Tuy nhiều lúc hành động ngớ ngẩn nhưng vẫn rất thẳng thắn với tình cảm của mình, một kiểu nhân vật rất dễ thương, tuy là mình không thích thể loại võng du cho lắm nhưng mình cũng bị tình tiết trong truyện cuốn theo lúc nào không hay.
4
680752
2015-09-17 23:59:26
--------------------------
298607
3072
Đây là một trong những cuốn tiểu thuyết võng du mình yêu thích nhất. Tình tiết truyện không nhàm chán, 2 nhân vật chính đến với nhau không phải quá hoa mỹ rực rỡ, nam chính không hoàn hảo và nữ chính cũng thể. Đều có ưu và khuyết điểm nhưng cả 2 đều rất đáng yêu, rất dũng cảm theo đuổi tình cảm của bản thân mình, nắm giữ nó trong tay và như để chứng minh điều đó thì người có tình rút cuộc cũng được về với nhau. Kết thúc trong sách xuất bản rõ ràng hơn khi đọc online.
5
106961
2015-09-12 19:56:06
--------------------------
280248
3072
Sau bao ngày chờ đợi mỏi mòn cuối cùng mình cũng đã có trong tay tập 2.Nếu tập 1 võng du chiếm hầu hết nội dung thì sang tập 2 lại xoay quanh chuyện Đại thần Kha Dĩ Mặc và Ninh Tâm gặp nhau ngoài đời.Gặp ở ngoài đời rồi Đại thần mới lộ rõ bản chất của mình:sinh nhật thì lừa lấy luôn con gái người ta,trước mặt tiểu tam không ngừng khoe ngọt ngào hạnh phúc,lúc kết hôn thì ’uy hiếp’tính mạng của bạn nhỏ Ninh Tâm :)...Nói chung truyện sủng thôi rồi, có chút ngược nhưng không đáng kể,độc giả được ăn kẹo sữa ngọt đến phát ngấy,kết thúc tất nhiên là happy ending rồi :).
P/S:Mình rất mong chờ ngoại truyện viết về anh chàng Lưu Thủy và công cuộc cưa vợ đầy khổ hạnh của chàng :) (cá nhân mình thấy Lưu Thủy và Mưa nhỏ khá đẹp đôi :))

5
123963
2015-08-27 15:29:20
--------------------------
278922
3072
Mình thấy cuốn tiểu thuyết này rất hay đặc biết là nhân vật ninh tâm, cuối truyện dù dĩ mặc có phải sang mĩ mổ tim nhưng ninh tâm vẫn "bỏ nhà theo giai" mà đi theo anh để giúp đỡ anh và sống tự lập dù từ nhở cô được ba mẹ bao bọc, đây là câu chuyện có kết thúc tuyệt nhất mình từng đọc những tình tiết lúc kết thúc có khúc thì lãng mạn có khú thì kịch tính khiến mình xem cũng vui buồn lẫn lộn không thể diễn tả hết được bằng lời .
5
644213
2015-08-26 12:11:43
--------------------------
268127
3072
Ở tập 2 cuối cùng hai nhân vật chính cũng có thể gặp nhau ở ngoài đời. Ninh Tâm vô cùng ngốc, lại còn thiên bẩm mù đường, nhưng cô ấy rất có tâm. Bỏ mặc nguy hiểm khi một mình đi quán net ban đêm, cũng cố gắng vào được game để chuẩn bị món quà mừng sinh nhật cho Mặc Thương thay cho cái gối thảm hoạ cô lỡ mang theo đi thi. Cô ấy vì đã xác định tình cảm của mình liền mạnh dạn tự mở lời làm người yêu, sau lại cũng là người mở lời cầu hôn, mình thấy cô ấy rất thẳng tính, rất đáng yêu, nhờ vậy mà bạn Dĩ Mặc có thể vênh váo trước bạn bè. Kết truyện cho thấy rõ hơn sự phấn đấu vì người mình yêu của Ninh Tâm rõ ràng hơn, cũng tạo tình huống cảm động về hộp chocolate, mỗi cái mình thắc mắc chocolate để bao nhiêu ngày không chảy sao?? Mà thôi, hai bạn quá dễ thương, cảm ơn nhà xuất bản đã mua bản quyền quyển này, thật sự rất thích rất hay
5
583898
2015-08-16 02:51:22
--------------------------
267531
3072
Nếu ở tập 1 tác giả tập trung khai thác nhiều về mảnh game thì ở tập 2 tác giả đã khai thác nhiều hơn về tỉnh cảm của hai nhân vật chính là gà mò Ninh Tâm và đại thần Mặc Thương, phải nói là hai người này vô cùng đáng yêu, tình yêu của hai nhân vật chính không đen tối như những tác phẩm ngôn tình khác mà rất trong sáng, chính chắn. Rất thích cách Thiền Đạm Ngữ xây dựng nhân vật Ninh Tâm, vừa chân thành đáng yêu vừa mạnh mẽ kiên quyết, Ninh Tâm luôn kiên trì ở bên Dĩ Mặc, tiếp thêm sức mạnh cho anh vượt qua căn bệnh tim của mình ^^
5
536908
2015-08-15 16:23:52
--------------------------
266854
3072
Tiếp theo phần 1 thì ở tập 2 này, tác giả không chỉ chú trọng vào việc khai thác mảng game nữa, mà quan tâm hơn đến tình cảm phát sinh vô cùng thuần túy của hai nhân vật. Mối quan hệ giữa Ninh Tâm cô nương thuần khiết, đáng yêu cùng đại thần Mặc Thương ngày càng thân thiết và được đẩy lên cao, họ nhận ra rằng mình càng ngày càng yêu đối phương nhiều hơn. Nhất là ở những chương cuối của truyện, khi bệnh tình của Dĩ Mặc ngày càng chuyển biến xấu đi và không mấy khả quan thì Ninh Tâm vẫn kiên trì ở bên anh, động viên chăm sóc anh. Truyện "Đại thần em nuôi anh" đã nói lên một phần nào đó khía cạnh tốt của một tình yêu online, thế giới ảo nhưng tình yêu là thật
5
295410
2015-08-15 00:03:07
--------------------------
247537
3072
Mình chọn mua cuốn này, đầu tiên vì bìa sách rất ngộ nghĩnh, đáng yêu, tiếp theo là tóm tắt nội dung nghe rất dễ thương. Sau khi mua và đọc quyển 1, mình đã ghiền luôn, mặc dù có truyện đã up trên mạng, nhưng vẫn quyết tâm không đọc trước, và chờ tiki bán, để rinh em nó về đây. Cuốn truyện kể rất hay, tuy là trong game nhưng tác giả lại viết rất hài hòa giữa hiện tại và thế giới ảo. Ở đó, mỗi nhân vật đều mang một tâm hồn rất khác nhau. Người thì ngây ngốc, đáng yêu. Người thì chính chắn, ân tình. Người thì xem trọng tình bằng hữu, tri giao. Kẻ thì nham hiểm, dối lừa. Kẻ lại đố kị, ghen ghét... Tất cả đã tạo nên một cuốn sách vừa hấp dẫn, vừa thú vị. Thế giới tuy là ảo, nhưng tình cảm của Ninh Tâm và Mặc Thương lại là thật, và cũng chính những tình cảm  đó đã giúp Kha Dĩ Mặc tìm được cuộc sống mới.... Một câu chuyện đầy xúc cảm
5
491902
2015-07-30 10:43:52
--------------------------
240266
3072
Đại thần em nuôi anh là cuốn tiểu thuyết hay và rất đáng đọc. Thuộc thể loại ngôn tình võng du, nhẹ nhàng, hài hước. Thực sự mình rất thích cách xây dựng nhân vật của tác giả, một Kha Dĩ Mặc nhẹ nhàng và một Ninh Tâm hoạt bát, thực sự rất hợp nhau. Tuy là Dĩ Mặc bị bệnh tim bẩm sinh nhưng Ninh Tâm vẫn kiên trì ở bên anh đến cùng. Cô bé Ngây thơ thế đấy, nhưng gặp chuyện cô ấy sẽ lột bỏ vẻ ngoài yếu đuối và trở nên mạnh mẽ không thua kém bất kìa ai.
---------- Tác phẩm này mình đã đọc đi đọc lại rất nhiều lần nhưng k hề thấy chán, ai thích cứ việc mua, bảo đảm cuốn truyện này sẽ không làm các bạn thất vọng -----------------
5
506971
2015-07-24 14:29:44
--------------------------
239314
3072
Mình thích tập 2 của tác phẩm này hơn tập 1 , vì tập 1 nói quá nhiều về game thì tập 2 nói về đời thực là đa số . Cách tác giả xây dựng tình tiết ở tập 2 rất hay . Tình cảm của hai nhân vật chính - Ninh Tâm và Mặc Thương rất trong sáng , đáng yêu . tuy nhiên kết thúc quá vội khiến mình hơi hụt hẫng nhưng không sao vì tác giả đã cho một kết thúc HE . Nói chung thì đây là một trong những quyển sách yêu thích của mình .
5
391948
2015-07-23 20:28:53
--------------------------
236205
3072
Mình rất thích thể loại ngôn tình võng vu.Bộ truyện Đại thần em nuôi anh với lối văn chương hài hước, nhẹ nhàng đan xen giữa hiện thực và trong game rất có sức hút đối với người đọc.với Kha Dĩ Mặc ấm áp, luôn luôn che chờ và bảo vệ cho cô nàng Ninh Tâm đã đốn tim hàng loạt đọc giả và một Ninh tâm nghốc nghếch nhưng cũng rất quan tâm đến người khác nên đã làm cho bao người có cảm giác thương yêu. nói chung mình rất thích tác phẩm cũng như những nhân vật trong bộ truyện này. ngoài ra bìa truyện in hình nhân vật chibi rất bắt mắt
5
471906
2015-07-21 16:46:21
--------------------------
216936
3072
Hôm qua mình mới nhận được hàng từ tiki . Vừa nhận được là mình đã mở hộp ra tìm cuốn Đại thần em nuôi anh tập hai để đọc ngay . Mình đã rất thích cuốn này từ tập một rồi , vì không muốn đọc trên mạng nên mình chờ xuất bản nốt tập hai. 
Mình có niềm yêu thích mạnh liệt đối với những cuốn ngôn tình thể loại võng du nên khi biết Nanubooks cho ra mắt cuốn sách này mình liền mua ngay . Truyện rất dễ thương , ở tập một nói về tình tiết trên game là chủ yếu thì ở tập hai là sự đan xen giữa game và hiện thực , tuy nhiên hiện thực có phần nhiều hơn một chút . Mình rất thích Ninh Tâm và Kha Dĩ Mặc . Mình thấy Ninh Tâm rất dễ thương , ngốc nghếch nhưng lại vô cùng chân thành . Cô tuy hiền lành nhưng thực ra không dễ bắt nạt đâu nhé , Cốc Lam không hiểu cô nên mới nói ra những lời cay nghiệt như thế . 
Còn về phần Kha Dĩ Mặc , mình rất thích tính cách dịu dàng , tình cảm của anh ấy . Luôn bao bọc và che chở cho Ninh Tâm hết mình . Tiếc rằng lại bị bệnh tim bẩm sinh , sống trong quá khứ đau buồn . Nhưng không sao cả , Tiểu Tâm đã đến và xóa hết đau buồn , mang lại hạnh phúc cho đại thần Mặc Thương rồi !
Mình rất thích bộ truyện này , có một điều khá đáng tiếc là không có ngoại truyện ,rất tò mò về cuộc sống sau này của Ninh Tâm và Dĩ Mặc . . .
5
335856
2015-06-28 15:07:07
--------------------------
215254
3072
Mình rất thích cuốn sách này. Bìa truyện là hình avatar các nhân vật trong game rất dễ thương. Nội dung rất hay và cuốn hút.Nó đã diễn tả rất thành công mối tình đầu tiên và duy nhất của cô nàng gà mờ Ninh Tâm và đại thần Mặc Thương. Cốt truyện hơi nghiêng về sự hóm hỉnh và ngu ngơ của hai nhân vật chính nhưng cũng có những chương rất cảm động làm mình rơi nước mắt. Như sự ra đi và phản bội bất đắc dĩ của Đồng Tưởng hay lúc ca phẫu thuật của Dĩ Mặc. Cốt truyện này rất đặc biệt vì nó mang một chiều hướng hơi phản với thực tế ví như một số người thường cho rằng chẳng thể nào có tình yêu online và nếu có thì đó cũng chỉ là nhất thời và kết cục là hai chữ "phản bội" nhưng cuốn truyện này đã phản bác lí lẽ đó bởi một tình yêu online vô cùng trong sáng và đầy xúc cảm. Đối với mình thì đây là một cuốn sách tuyệt vời về nội dung và ý nghĩa.
5
352650
2015-06-26 07:57:49
--------------------------
