430757
4931
Cuốn tiểu thuyết này của Âu dương mặc tâm thực sự để lại cho mình nhiều ấn tượng ! Ở tập này cô nàng Kim Kiền cùng đám Triển Chiêu lại tình cờ được hội ngộ với Độc Thánh, Y Tiên cùng nhau tìm bí phương giải độc cho thái hậu và thôn dân thôn Du Lâm. Nàng Kim Kiền thì lần này lúc nào cũng canh cánh lo sợ lộ phận đệ tử của nhị vị sư phụ lại còn đụng độ đám áo đen bí ẩn suýt bị bắt đi! May mắn thân phận của nàng ta vẫn được giữ bí mật! Chuyện tiếp theo sẽ như thế nào?!!! 
5
619354
2016-05-15 20:46:01
--------------------------
364947
4931
 Những nhân vật đã từng làm mưa làm gió nay lại bị tác giả hóa phép, yểm bùa trở thành những nhân vật tạo tiếng cười thâm túy. Lần đầu tiên đọc là mình đã bị lôi cuốn bởi nhân vật Kim Kiên "xui xẻo toàn tập" rồi. Haiz... Chỉ tại lúc trước mình lỡ dại đọc thử tập 1 kết cục...mua nguyên một bộ luôn.                                                             
  Nhưng cũng không thể không bái phục ngài tác giả bởi quyển tiêu thuyết này là quyển tiểu thuyết đầu tiên mình mua và cực kì đặc biệt với một người cực kì tiêc tiên như mình. haiz... Chắc minh cũng bị yểm bùa rồi quá. Thật tuyệt vời!!! Bái phục.
5
624445
2016-01-07 09:01:51
--------------------------
309163
4931
Đợi mãi cuối cùng Bách Việt cũng xuất bản tập 4, trước đó mình đã đọc trên mạng rồi nhưng vẫn nhất quyết phải mua cho đủ bộ vì quá thích bộ truyện này. Bạn nào đã đọc các tập trước thì cứ yên tâm là má Tâm vẫn giữ vững phong độ nhé, viết chỉ có lên tay thôi, hài thì thôi rồi, càng ngày càng không đỡ nổi, mình đọc lại mấy lần mà vẫn cười bò ra. Càng về sau này thì Triển đại nhân càng thể hiện (có tí xíu thôi nhưng mà vậy là cũng tiến bộ nhiều lắm rồi đó) tình cảm trên mức "lãnh đạo - nhân viên" với Kim Kiền, hóng coi má Tâm giải quyết vụ này thế nào ở mấy tập sau nha ^^
5
60677
2015-09-18 22:33:23
--------------------------
307868
4931
Mua đã lâu mà giờ mới có thời gian đọc ẻm, quả không uổng công , truyện rất hay , mình đặc biệt hứng thú với nhân vật với cái tên đặc biệt đó là Kim Kiền cùng tính cách đúng y như tên của cổ, cũng là xuyên không, nhưng sao truyện này mang lại rất nhiều ấn tượng với mình , từ bìa sách đến nội dung và cốt truyện từng chi tiết cũng hiện hữu đầy vẻ đáng yêu tinh nghịch, đọc truyện giúp mình xả stress và vui vẻ hẳn, về chất lượng thì giấy dày, bìa cứng và đẹp!
5
296749
2015-09-18 12:37:30
--------------------------
301539
4931
Nhận được quyển sách này trên tay thật sự rất vui, mà hình như dạo này tốc độ giao hàng của Tiki tăng nhanh đáng kể luôn. Cho Tiki một điểm cộng to đùng. Lần này mình xin được nhận xét về bố cục quyển sách  nhiều hơn. Trong những quyển của hệ liệt này, quyển 4A này là làm mình ưng ý nhất, chắc là bên Bách Việt đã rút kinh nghiệm từ nhưng lần trước. Vẫn là phong cách bìa cũ nhưng mình thấy giấy sách đợt này tốt hơn, mực in rõ ràng hơn nhưng lần trước. Gấy sách lần này rất đầy đặn, keo dán gáy cũng đều. Lúc mình mua có quà tặng kèm cũng rất chất lượng, nói chung là rất thích quyển 4A này, cảm giác rất xứng đáng với số tiền đã bỏ ra.
5
19933
2015-09-14 18:23:50
--------------------------
294895
4931
Đến Phủ Khai Phong Làm Nhân Viên Công Vụ ngày càng hay và dễ thương. Mức độ hài hước cũng ngày càng cao. Hihi
Mình rất thích phong cách của Kim Kiền từ cách xử lý tình huống cho đến tích cách, ngôn phong... rất hài, có hơi trẻ con nhưng lại rất đáng yêu và dễ thương.

Tập 4A thiết kế cũng rất đẹp, bookmark nhỏ xinh, lại còn tặng kèm lịch 2015 với những hình ảnh các nhân vật trong truyện. Tuy lịch có hơi nhỏ, giấy khá mỏng nhưng nhìn chung thì rất đẹp và sáng tạo.
5
18866
2015-09-09 20:28:15
--------------------------
274654
4931
Mình là mình cực thích bộ này luôn á rất hài hước lại rất dễ thương, gây cấn nữa, những vụ án kinh điển của phủ Khai Phong có một diện mạo vô cùng khác với những tình tiết phá án không quá căng thăng hay ngột ngạt mà thay vào đó là sự hài hước đáng yêu của dàng nhân vật chính rất đáng yêu nhưng cũng không kém phần nghiêm túc. Rất cảm ơn tác giả Âu Dương Mặc Tâm đã "chế" lại bộ truyện này để em "lột hố" và giờ không thể nào dứt được :-)) Rất mong chị Tâm mau mau viết tiếp để em cứ hóng mãi thế này chắc thành hươu mất thôi :-))
5
536908
2015-08-22 09:21:54
--------------------------
251480
4931
Lần này mình cảm thấy hài lòng lắm cơ, quyển sách được chăm chút từng li từng tí: bìa sách đẹp, giấy đẹp, bookmark hơi nhỏ nhưng xinh lung linh <3, còn được bọc lại cẩn thận nữa, đặc biệt  không mắc phải lỗi chính tả và đánh máy... 
Về phần nội dung càng ngày càng hấp dẫn, tình tiết lôi cuốn, lối hành văn hài hước; tuyến nhân vật được xây dựng rất đáng yêu, nào là Bạch Thử, Nhất Chi Mai... trước giờ đại danh đỉnh đỉnh, đẹp trai ngời ngời, nhưng qua ngòi bút của má Tâm thì không còn từ nào để diễn tả được =))), có lẽ chỉ còn Miêu đại nhân là vững phong độ thôi :3. Mình là fan của Hà Gia Kính Triển Chiêu, nên ban đầu anti bà Kiền dữ dội lắm nha, nhưng giờ thích bà ấy rồi :3, hóng đến ngày ra tập Triển Chiêu tỏ tình <thất bại> với Kim Kiền :))
5
78734
2015-08-02 20:57:21
--------------------------
249670
4931
Từ đầu bìa truyện đã làm cho ta có cảm giác hài hước và không khác gì bề ngoải của nó. Nội dung truyện xoay quanh những vụ án kinh điển trong phim Bao Thanh Thiên nhưng với 1 khía cạnh khác, phương diện khác truyện được lồng ghép những yếu tố hài hước giúp người đọc xả stress. Các tập trước đều hay đến tập này thì không ngoài mong đợi.Nàng Kim Kiền ham tiền, háo sắc và chàng Miêu Triển Chiêu có những pha hài khó đỡ.Mong những tập sau này sẽ vẫn giữ được lối hài hước và còn hài hước hơn
5
516977
2015-07-31 22:51:27
--------------------------
249549
4931
Truyện "Đến Phủ Khai Phong Làm Nhân Viên Công Vụ" là một trong số những tác phẩm yêu thích trong tủ sách bởi nó mang đến sự mới lạ độc đáo. Không phải là những tiểu thuyết tình yêu đầy nước mắt hay những cuộc phiêu lưu gây cấn đến ngạt thở nhưng truyện lại có một sức hút rất riêng! Ngay từ nội dung đã đủ khiến người đọc bất ngờ bởi những vụ án kinh điển tưởng chừng như quá đỗi quen thuộc lại được tái hiện hoàn toàn khác dưới góc nhìn có phần "hư cấu" nhưng lại hài hước đáng yêu! Truyện mang đến tiếng cười vô cùng tự nhiên với một Kim Kiền tuy mê tiền và có chút gian tà nhưng đôi khi lại rất được việc! Tập 4A này đưa người đọc gặp lại nhị vị sư phụ Y Tiên và Độc Thánh cùng hàng loạt tình huống cười ra nước mắt cùng cách xử trí vô cùng bá đạo mà có lẽ nếu không phải là Kim Kiền thì không ai có thể nghĩ ra được. Đây quả là tác phẩm rất đáng đọc và vô cùng mong chờ tập tiếp theo để xem sẽ còn chuyện gì xảy ra nữa đây. Vụ án nào lại được tác giả đem ra chế biến và tình cảnh sẽ còn trớ trêu lắm đây!!! 
5
134457
2015-07-31 22:40:32
--------------------------
241156
4931
Thuộc dòng Ngôn tình nhưng Đến phủ Khai Phong lại có một phong cách mới lạ, khác hẳn với những cuốn ngôn tình khác. Dàn nhân vật thì khỏi phải bàn cải gì, tất cả đề là cực phẩm. Từ lão Bao cho tới tiểu Trịnh, hay các nhân vật giang hồ khác đều rất thú vị. Án Thanh Long Châu này tuy hơi dài nhưng mà các tình tiết không sáo rỗng, rất cuốn hút. Nhị vị sự phụ của Kim Kiền phải nói là mặt rất dày a, Độc thánh với tiểu Miêu thì lúc nào cũng đấu mắt như mẹ chồng nàng dâu.Đọc xong tập 4 chỉ mong Nanubook mau mau xuất bản tập 5 TT_TT .
5
64755
2015-07-25 11:18:35
--------------------------
237178
4931
Đọc quyển này làm mình cười liên tục. Cốt truyện tuy xoay quanh những vụ án kinh điển mà những ai yêu thích phim Bao Công đều biết nhưng lại được biến tấu, lồng ghép vào các nhân vật nên tạo ra nét rất mới mẻ, nhiều phen khiến người đọc bậc cười. Tạo hình các nhân vật quen thuộc với mọi người cũng có chút biến tấu, như chàng Triển Chiêu bề ngoài thì lạnh lùng nhưng lại hay ghen tuông thầm, luôn theo sát Kim Hiệu Úy. Tập này thì nàng Kim Kiền ham tiền, háo sắc và chàng Miêu Triển Chiêu có những pha khó đỡ thật, nhất là lúc hai vị sư phụ của Kim Kiền xuất hiện làm Khai Phong Phủ lại một phen nhốn nháo :)) Hi vọng NXB sẽ ra tập tiếp theo sớm.
5
35746
2015-07-22 11:55:49
--------------------------
235893
4931
Đây là cuốn ngôn tình hài - trinh thám đầu tiên mình đọc. Đọc xong Đến phủ khai phong mình biết mình đã không lựa chọn sai lầm. Cũng là các vụ án trong Bao thanh thiên nhưng tác giả đã khéo viết lên thành một màu sắc khác khiến độc giả không thấy nhàm chán. Ngay từ trang đầu tiên, cuốn sách đã thu hút mình. Truyện rất hài hước về Kim Kiền nhát gan ham tiền, Triển Chiêu chính trực và còn rất nhiều nhân vật khác nữa. Mỗi nhân vật trong truyện đều có nét riêng để lại dấu ấn trong độc giả. Từ đây mình đã trở thành fan của tác giả Âu Dương Mặc Tâm. Đây là một cuốn sách đáng đọc giúp ta giải trí và thư giãn
5
328945
2015-07-21 13:49:28
--------------------------
225558
4931
Tập 4A này vẫn tiếp tục những tình tiết hài hước không đỡ nổi của Kim Kiến và những nhân vật khác. Cặp sư phụ của Kim Kiền sau bao ngày vắng bóng cuối cùng đã tái xuất giang hồ với màn ra mắt không thể ấn tượng hơn. Tình cảm của Tiểu Triển cũng tiến triển theo từng ngày nhưng nàng Kim thì vẫn còn mông lung lắm. Có vẻ như Bạch Ngọc Đường đang dần thích Tiểu Kim hơn rồi.Mong những tập sau sẽ được nhìn thấy những bước nhảy vọt về chuyện tình cảm giữa hai nhân vật chính.
4
167283
2015-07-10 16:45:40
--------------------------
222150
4931
Đây là truyện ngôn tình đầu tiên mà mình bỏ tiền mua sách về trưng trên giá sách, mà truyện còn chưa có viết xong nữa chứ, do quá quá quá ư là thích, cuồng Kim Kiền và couple Mèo - Chuột quá chịu không nổi :3 Mình đã đọc truyện, xem phim Bao Thanh Thiên không biết bao nhiêu lần, nhưng với truyện ĐPKPLNVCV này, dù biết trước cốt truyện vụ án rồi nhưng vẫn kinh ngạc và bất ngờ vô số lần trước mỗi tình tiết xảy ra, phục tác giả Âu Dương Mặc Tâm thật, viết lại chuyện đã có nhưng vẫn lôi cuốn độc giả đến từng chi tiết, phải chăm chú đọc từng câu từng chữ không bỏ sót chữ nào, vừa đọc vừa cười chảy nước mắt. Lúc buồn buồn lôi ra đọc lại là cười đến ngã ngửa không biết buồn nó bị vứt đến xó nào rồi luôn. Hóng tập tiếp theo  !!!!
5
42833
2015-07-05 02:05:11
--------------------------
214785
4931
Mình đã có 4 tập đến phủ khai phong làm nhân viên công vụ rồi hj:))))))))) Bìa truyện lúc nào cũng rất dễ thương. và cốt truyện rất hay nên mình đã chờ đợi rất lâu mới có một tập mới của tác giả Âu Dương Mặc Tâm. Mỗi lần đọc là mình lại mắc cười 2 nhân vật chính của chúng ta. Mỗi một vụ án cũng rất chân thật. Âu Dương Mặc Tâm đã dùng ngòi bút của mình viết nên tình yêu của Triển Chiêu nhưng cũng không làm mất đi phong cách phá án tài giỏi cùng sự anh minh của mọi người trong phủ khai phong. Thật sự là rất rất hay mình rất thích. Cảm ơn tác giả và tiki đã mang những cuốn truyện hay như vậy đến cho mình. Xin cảm ơn.
5
462393
2015-06-25 14:59:12
--------------------------
214354
4931
Có lẽ mình yêu Kim Kiền mất rồi. Có thể diễn tả truyện trong ba từ hài hước, thú vị và bất ngờ. Với những tình tiết xảy ra xung quanh Im Kiền cùng những vụ án đã tạo nên một bộ truyện vô cùng độc đáo. Đề tài về xuyên không không hiếm nhưng vẫn tạo nên những câu chuyện hay ho về chuyện tình chuyện án với những nhân vật đã quen và những nhân vật mới mẻ... Cơ mà là truyện có nhiều tập mà mỗi tập còn tốn kha khá tiền nên không biế là có nên sưu tầm đủ bộ truyện hay không nữa :(
5
310891
2015-06-24 21:12:07
--------------------------
194272
4931
Đọc quyển 4A này với vụ án Thanh Long Châu khiến mình vô cùng thích thú. Việc xuất hiện hai sư phụ Thánh Y, Độc Y của Kim Kiền làm đôi khi mình cười lăn. Nhất là cái đoạn đòi lấy máu "đệ tử duy nhất" để làm thuốc dẫn. Rồi lại tức điên cuồng khi Triển Chiêu bị tống giam. Sự xuất hiện của hai anh em Nhan Tra Tán - Nhan Tra Dật cùng Bạch Ngọc Đường, Nhất Chi Mai đã làm mình phải thức suốt đêm để đọc cho xong. Nửa đêm mình còn phải cười lăn cái việc Kim Kiền đi giấu tiền thưởng nữa chứ. Quá tuyệt
5
628391
2015-05-09 23:29:47
--------------------------
179483
4931
Tôi thấy tập này cũng không có chi tiết nào đặc sắc cả ngoại trừ việc gặp gỡ giữa Y Tiên, Độc Thánh và Kim Kiền. Truyện cũng mỏng hơn mấy tập trước mà giá tiền thì nhỉnh hơn, thường thì kết thúc truyện sẽ tìm ra hung thủ hoặc kẻ chủ mưu nhưng mà tập này khép lại mà để lại câu trả lời bỏ ngỏ. Tình cảm của Triển chiêu dành cho Kim Kiền cũng tăng lên một bậc, đó là sự bảo vệ một cách vô thức, đáng tiếc nàng Kim Kiền của chúng ta lại không nhận ra. Tôi không biết sau này khi Kim Kiền yêu Triển Chiêu thì có kìm hãm được sự nông nổi của nàng không. Trong truyện tôi thích nhân vật Nhất Chi Mai nhất vì mỗi tình huống dở khóc dở cười đều là do anh mà ra, thích cả biệt danh đen đủ đường mà Kim Kiền trao tặng cho anh nữa
4
80511
2015-04-07 12:53:17
--------------------------
176757
4931
Đây là một trong số ít truyện ngôn tình dài tập mà mình theo đuổi, và đến tập này thì độ hấp dẫn vẫn không hề suy giảm. Không có nhiều những tình tiết bi lụy, nặng nề - đó là lí do khi mình đọc cuốn này cảm thấy rất thoải mái nhưng không hề mất đi sự lôi cuốn.

Tập này thì mình càng "bái phục" độ "gian xảo" của Kim Kiền, nhất là kế "liên hoàn mĩ nhân". Sự xuất hiện của Thánh Y, Độc Y và góp mặt của Bạch Ngọc Đường, Nhất Chi Mai cũng khiến truyện trở nên hài hước và gây cấn hơn rất nhiều. Nói chung, đang hóng tập 4B, để xem vụ án giải quyết thế nào.
5
113747
2015-04-01 20:04:14
--------------------------
170689
4931
Hầu như truyện nào tui cũng chờ hoàn rời mới mua đọc, duy có truyện này là "lọt hố" không thương tiếc. Tập 4 vẫn lôi cuốn hấp dẫn với những vụ án vừa quen vừa lạ. Tình cảm của Tiểu Miêu dành cho Kim Kiền ngày càng rõ nét, mà chắc chính chủ nhân cũng chẳng nhận ra. Kim Kiền vẫn lươn lẹo như cũ, tuy nhiên vẫn ngây ngốc khi chẳng nhận ra tình cảm của Triển đại nhân. Đọc mà cười chảy nước mắt với "liên hoàn mỹ nhân kế"....
Và giờ lại chờ.......quyết tâm không đọc trước...nhưng.....nhà xuất bản xuất bản lẹ lên đi ạ T_T
5
291719
2015-03-20 15:24:04
--------------------------
167290
4931
Tôi đã mua cuốn sách này sau khi nó phát hành được 3 tiếng đồng hồ tại Bachvietstore. Thật tuyệt sau bao ngày mong chờ, cuối cùng tập 4A cũng xuất bản. Cũng như các tập trước, lần này tác giả Mặc Tâm đã không làm độc giả thất vọng! Vẫn lối viết hài hước như cũ, tác giả đã miêu tả vụ án Thanh Long Châu cực kì thú vị. Tình cảm của Triểu Chiêu đã được xác định rõ, còn Kim Kiền vẫn ngu ngơ như trước. Tập này cũng xuất hiện các nhân vật cũ như Bạch Ngọc Đường, Nhất Chi Mai... Ngoài ra có thêm Nhan Tra Tán và Tiểu Dật.
Bìa truyện khá đáng yêu, cách vẽ của chị Tứ Lộ vẫn tuyệt vời như trước. Được cái bìa đẹp và hợp nội dung truyện. Mỗi tội poster lịch tặng kèm bị gấp, và bookmark Triển Chiêu nhà sách bị in thiếu nên mình không có T.T

Hi vọng sẽ sớm có tập 4B.
5
468194
2015-03-14 15:38:50
--------------------------
163984
4931
Đã lỡ đọc truyện chị Mặc rồi nên không bỏ được chỉ đành ngóng chờ từng ngày đợi truyện ra thôi. Mới có tập 4A nên mong thêm 4B cho đủ bộ. Truyện rất hài hước, vui nhộn, nhân vật nào cũng cực kì đáng yêu mà thích nhất là Triển Chiêu á. Không chỉ nội công thâm hậu, danh tiếng nổi như cồn trong thành Biện Lương hay trong giang hồ mà còn cả độc chiêu '' Mĩ miêu kế '' có một không hai. Và đối với Kim Kiền rất khác biệt chỉ là chính anh cũng không nhận thấy. Nói chung truyện rất hay, hài hước và sáng tạo.
5
521157
2015-03-06 13:09:31
--------------------------
162186
4931
Đã quyết tâm chờ truyện hoàn mới đọc,vậy mà bị đứa bạn đạp xuống hố không thương tiếc.
Những vụ án tưởng chừng không thể quen thuộc hơn,qua ngòi bút của Âu Dương Mặc Tâm lại trở nên mới mẻ vô cùng.một Kim Kiền yêu tiền như mạng,tham ăn lười làm,háo sắc nhát gan(không hiểu tiểu miêu sao có thể thích nổi cô nàng này).một Triển Chiêu đẹp trai tốt bụng,võ công thượng thừa,là idod của thành Biện Lương.Chuyên bị Kim Kiền lôi đi thực hành "mỹ miêu kế".
Một Bao đại nhân chính trực liêm khiết,Công Tôn tiên sinh phúc hắc,tứ đại hộ vệ hâm mộ Triển đại nhân phát cuồng.
Tên trộm lười nhất quả đất Nhất Chi Mai,ngũ thử Bạch Ngọc Đường,hai vị sư phụ Y Tiên,Độc Thánh đáng yêu.vv...
Dưới ngòi bút của tác giả,Khai Phong phủ trở nên vui nhộn,sống động và hài hước hơn bao giờ hết.Phần đầu diễn biến hơi nhanh,nhưng không ảnh hưởng tới truyện mấy.Rấtttttt mong chờ tập 4b ra lò,nghe các bạn bảo năm nay hoàn truyện làm mình mừng quá.
5
472523
2015-03-01 18:57:15
--------------------------
161846
4931
Mình chỉ mới đọc chương một nhưng phải gấp sách lại lên đây viết nhận xét kẻo đọc xong lại quên mất. 
Về nội dung, đây là một trong những truyện ngôn tình hay nhất đối với mình, một trong những TÁC PHẨM NGÔN TÌNH THẬT SỰ chứ không phải là RÁC PHẨM đang được xuất bản ồ ạt ở Việt Nam. Tác giả có giọng văn hài hước, lạ và riêng không lẫn vào đâu được. Điều làm nên sự cuốn hút không thể cưỡng của truyện không chỉ là Kim Kiền hám tài, mà trong đó phải kể cả bộ ba Triển Chiêu - Bao Chửng - Công  Tôn Sách, cùng với bộ tứ hiệu úy, đám râu ria trong phủ Khai Phong, và toàn thể nhân vật quần chúng nữa, ai cũng trở nên hài hước và đáng yêu dưới ngòi bút của Âu Dương Mặc Tâm  Mới chương một thôi mà mình đã cười vỡ bụng rồi.
Nhưng không biết do mình khó tính hay gì mà có vài chỗ dịch mình không thể chấp nhận được. Trong truyện xuất hiện các từ như 'không khỏi', 'nhất thời'... khá khó chịu, ngoài ra còn kiểu AA sắc mặt âm u, BB sắc mặt lạnh lùng, CC lưng khom,... chủ vị đảo loạn cả lên, đảo không đúng chỗ,  đảo không cần thiết. Nói giống convert cao cấp thì quá đáng quá, nhưng không thể phủ nhận là nó cứ tựa tựa những bản edit hoặc cv (những bản thường bị người khác chê về văn phong như vậy ) trên mạng, làm cho câu chữ kém mượt mà hẳn đi. Dịch giả có thể sai sót, nhưng còn dàn biên tập thì đâu rồi? Tại sao lại thiếu trau chút như vậy? Tại sao lại sơ sài qua loa như vậy? Mình đọc các bộ ngôn tình xuất bản khác thì hầu như không gặp lỗi như vậy, như Thịt thần thiên chẳng hạn, căng mắt ra tìm cũng không thấy. Tất nhiên những hạt sạn này không nhiều, nhưng đối với truyện xuất bản thì không thể chấp nhận được. Mình từng dùng phần mềm QT đọc các truyện ngôn tình trung quốc, các từ như vậy xuất hiện nhiều lắm, không riêng gì 1 quyển mà hầu hết các truyện TQ đều như vậy, nhưng khi so với bản edit hoặc bản dịch xuất bản thì các từ ấy đã được chuyển thành từ khác mà  không làm mất nghĩa của câu, tạo cho câu văn thêm phong phú và mượt mà. 
Cái mình chê ở đây là bộ phận dịch truyện thiếu đầu tư quá thôi.
Xin hết!
5
122733
2015-03-01 00:31:05
--------------------------
161208
4931
Truyện hay, hài hước lại rất sâu sắc, các vụ án miêu tả rất cụ thể, gay cấn xen lẫn yếu tố hài hước khiến người đọc không thể rời mắt. Những vụ án ở Khai Phong phủ tưởng chừng như rất quen thuộc vậy mà dưới ngòi bút của tác giả lại trở nên rất hấp dẫn,chỉ với sự xuất hiện của một nhân vật tưởng chừng như không mấy quan trọng lại khiến cho câu chuyện vừa có chút quen quen vừa có chút mới lạ. Mình theo truyện này mấy năm rồi, chờ mòn mỏi. vậy mà tập 4a nhanh hết hàng quá vậy ta không kip order nữa, hic. bao giờ mới có hàng nữa vậy, thật buồn quá đi
5
87175
2015-02-26 22:41:33
--------------------------
161185
4931
Nếu bạn đã chán các kiểu nữ nhân vật nữ chính mít ướt, hiền lành quá mức hay thụ động đến chán, nếu bạn yêu thích những nhân vật trong Bao Thanh Thiên quen thuộc, và nếu bạn mong muốn đọc một câu chuyện tình cảm không quá dạt dào mạnh mẽ mà chỉ tinh tế, nhỏ giọt đáng chờ mong thì đây chính là cuốn ngôn tình dành cho bạn. Theo ý kiến của mình, nhân vật nữ trong bộ truyện rất đáng yêu dù ba lần bảy lượt bị tác giả dìm hàng, không hề bi lụy, lại hài hước, cốt truyện dễ đọc nhưng lại hấp dẫn, lời văn không hề sến súa. Tuy nhiên, đọc bộ này dễ nổi điên lắm nhé, vì hai nhân vật mãi không chịu nhận ra tình cảm của mình, cứ lăn tăn, lờ lớ lơ hoài à... cơ mà hay...
5
249905
2015-02-26 21:54:56
--------------------------
161079
4931
Đây là truyện duy nhất mình mong nó sẽ thật dài để được đọc thật nhiều, và không hề tiếc tiền khi rước cả bộ về nhà. Nhớ hồi đọc tập 1 thấy truyện cũng tàm tạm, đọc giải trí được, sau đó đọc tập 2, rồi tập 3 và thế là trở thành fan trung thành luôn. Ngóng từng ngày để cầm được tập 4 trên trên tay.
Đây là truyện siêu hài hước, nhân vật nào trong truyện cũng dễ thương, ai cũng có khả năng chọc người đọc cười ra nước mắt. Và mặc dù đây là truyện điều tra và hài hước là chính, nhưng tình cảm của cặp Tiểu Miêu và Tiểu Kim theo mình là nên được xếp vào loại dễ thương và hay nhất giới ngôn tình.
5
26010
2015-02-26 16:32:01
--------------------------
160942
4931
Bộ ngôn tình hay nhất mình từng đọc.
Không cẩu huyết, không ngược, chỉ có hài hước và đáng yêu xuyên suốt từ đầu đến cuối.
Trong tập Thanh Long Châu này, thân phận đệ tử Y tiên Độc thánh của Kim Kiền cũng đã được tiết lộ, tình cảm của Triển Chiêu dành cho Kim Kiền cũng ngày một sâu đậm (mỗi tội nàng ngây thơ không biết, sau này lại nghĩ Triển Chiêu yêu Bạch Ngọc Đường mới chết chứ!!!
Nhân vật Nhan Tra Tán và Tiểu Dật cũng xuất hiện (rất thích anh Nhan!!!!)
Điều hạnh phúc nhất là mẹ Mặc cũng đã sinh con xong, khả năng kết thúc DKPP năm nay là rất lớn.
Cảm ơn Tiki, Nanu và mẹ Mặc đã mang đến một bộ truyện đáng yêu như thế này! Hóng tập 4b
5
443615
2015-02-26 09:26:50
--------------------------
159470
4931
Truyện thật sự rất hay ;;___;; lọt hố của chị Mặc Tâm từ tận năm 2013 ;;__;; đến bây giờ cũng gọi là 2 năm rồi thì phải ngồi chờ trông ngóng cuốn 4 mà hiện tại đã là 17/2 rồi *lệ* rốt cuộc phải đợi tới bao giờ ;;___;; mà chị Mặc Tâm hiện còn đang viết tiếp ... đến bao giờ hoàn ;;__;; thôi ráng đu lỡ lọt hố rồi tỉnh sao. Thỉnh lược ngàn chữ than vãn cuối cùng truyện thật sự rất hay a không đọc là uổng ai chưa đọc mau đọc đi a ;;___;;
5
561846
2015-02-17 09:14:29
--------------------------
159396
4931
Kim Kiền - 1 trong những nữ 9 mà mình thích nhất. Hám tiền, thông minh, tính cách vui nhộn,.. mỗi tội EQ quá thấp, cực kì thấp. Cơ mà vì vậy mới dẫn đến những tình tiết dở khóc dở cười. ^^
Triển Chiêu - tài năng, trung thành, dễ thương. Mặt anh mỏng lắm, hơi tí là đỏ rồi ^^ mỗi lúc anh ăn dấm thì rất ba chấm nhé ^^
Anh chuột - xinh, giỏi, giàu.. cơ mà mình rất nghi anh có tình cảm với anh Mèo.Nếu 2 anh về với nhau cũng được, dù sao mình cũng là hủ mà =))
5
555204
2015-02-16 19:49:38
--------------------------
158771
4931
Đọc cái này bên một blog mạng mà cười đau bụng. Bạn Kim Kiền nhà ta cũng thật bá đạo quá đi nhưng mà vẫn phải gọi Triển Triêu công tử làm sư phụ a. Bao Đại nhân thì vô cùng công minh (cái này xem phim cũng biết rồi), Công Tôn sách rất đa mưu túc trí a.
Lúc đầu đọc còn tưởng nó là loại tiểu thuyết trinh thám "trá hình" cổ trang cơ nhưng mà càng đọc càng mê, càng đọc càng bị hút hồn. Đọc mà cố nhịn cười, đến khi cười mẹ lại kêu điên hay sao mà nửa đêm cầm điện thoại ngồi cười.
Chúc bạn này bị tác giả bỏ bùa sớm dinh quyển này về kẻo mọc nấm như mình.
5
531559
2015-02-13 21:43:57
--------------------------
156293
4931
Nói đến nước mắt phải chảy khi đọc truyện này thì lại không phải vì nội dung của câu chuyện mà là quãng thời gian đau khổ của những ai ”xui xẻo" bị tác giả bỏ bùa... Hu hu. Cuối cùng thì ngày này cũng đến a ta đã đợi 10 tháng rồi sắp không chịu nổi nữa rồi....
  Phần cảm thán đã xong. Đây là lần đầu tiên mình viết bình luận sách sau khi đọc không biết bao nhiêu quyển truyện ngôn tình. Mình rất tiếc tiền nên đa phần những cuốn sách mình đọc đều là trên mạng cả nhằm tiết kiệm chi phí nhưng đây là cuốn sách đầu tiên mà mình đã không do dự đặt mua trên tiki. Thật sự rất thú vị a nếu có thể mọi người nên đọc thử.
5
502903
2015-02-03 23:48:30
--------------------------
