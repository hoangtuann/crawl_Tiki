358253
7137
Lại một cuốn tản văn nữa về Sài Gòn. Không biết tại sao nhưng lúc nào mình cũng cực kỳ yêu thích các quyển sách viết về đề tài này. Đúng như tựa để cuốn sách, "Sài Gòn thương nhớ" là những nỗi niềm nhớ nhung, những kỷ niệm khó quên của các tác giả một thời gắn bó với thành phố Hồ Chí Minh ngày còn mang tên Sài Gòn, cũng là những trải lòng, cảm nghĩ chân thành của thế hệ ngày nay đối với thành phố mang tên Bác. Sách có bìa lụa màu tươi và rất đẹp, phù hợp cho những ai yêu mến vùng đất phía Nam vừa nhộn nhịp vừa cổ kính này của Tổ quốc.
5
433873
2015-12-25 00:43:41
--------------------------
