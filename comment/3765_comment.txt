569941
3765
Truyện hay, tình tiết lôi cuốn, hài hước, khá giống với tình yêu của mình nên đồng cảm sâu sắc. Đúng là khi yêu cần nhiều dũng cảm:)
5
1312334
2017-04-11 10:25:10
--------------------------
479544
3765
"Khi yêu cần nhiều dũng cảm" của tác giả Chetan Bhagat không đơn thuần chỉ là câu chuyện về tình yêu đôi lứa mà đó còn là khát vọng được gắn kết, được xoá bỏ những kì thị về con người trong xã hội Ấn Độ.

Chetan Bhagat đã vô cùng khéo léo khi lấy câu chuyện về tình yêu của hai con người sinh ra tại hai vùng đất khác nhau tại Ấn Độ. Tuy rằng, họ cùng sinh sống trong một lãnh thổ nhưng họ lại có những sự phân biệt đối xử, kì thị với nhau về làn da, về tiếng nói và về chủng tộc. Hai nhân vật yêu nhau phải vượt qua mọi định kiến, mọi sự bảo thủ để đến được với nhau. Đó cũng là lời là mà tác giả muốn nhắn nhủ về một tình yêu không gì ngăn cản được và mong muốn về một đất nước được thống nhất từ mặt lãnh thổ cho đến con người. 
5
372883
2016-08-10 14:45:08
--------------------------
462424
3765
Khi mới nhìn thấy bìa sách và chưa đọc lời giới thiệu, mình cứ nghĩ đây là một câu chuyện tình yêu đúng nghĩa và đơn giản, biết đâu tác giả "Ba chàng ngốc" đổi hướng sáng tác. Nhưng đúng là mình nhầm, vẫn là cốt truyện quen thuộc của Chetah, hiện thực, đôi khi khá cực đoan nhưng vẫn không kém phần lãng mạn. Dường như vẫn là nhân vật "tôi" quen thuộc trong "Ba chàng ngốc" nhưng câu chuyện hoàn toàn khác và thế giới quan của nhân vật cũng hoàn toàn khác. Mình thích mê nhân vật nữ chính, xinh đẹp (tác giả nói thế, "hoa khôi của trường IT"), thông minh, tự tin, mạnh mẽ, dám nghĩ dám làm,...Bên cạnh chuyện tình của họ là vấn đề tôn giáo, sắc tộc, có gây gắt, có bài trừ nhưng vẫn có phần hòa hợp và tôn trọng. Theo mình thì không nên so sánh cuốn này với "Ba chàng ngốc" (cuốn kia hết sức tuyệt với rồi, phim và truyện đã vào hàng bất hủ), riêng bản thân cuốn sách này thôi đã thật đáng để đọc.
4
41592
2016-06-28 16:05:56
--------------------------
435069
3765
Mình mua sách vì tiêu đề của nó gây cho tò mò và bản thân mình cũng trong giai đoạn cần nhiều dũng cảm để vượt qua khó khăn trong tình yêu mà chính mình tự đâm đầu vào.... Tình yêu không chỉ dựa trên tình cảm giữa trai và gái, mà còn cần rất nhiều sự dũng cảm để vượt qua sóng gió, "Khi yêu cần nhiều dũng cảm" là một ví dụ điển hình của định kiến xã hội về tôn giáo, con người, vùng miền.... mà bất cứ ai cũng có thể phải đối mặt trong đời, nó tác động đến tư tưởng, quan điểm của gia đình, của cha mẹ. Hẳn là sẽ có những tình yêu bị phản đối vì vô số lí do, nhưng chỉ cần bạn có niềm tin vào tình yêu của mình, có nhiều thật nhiều lòng dũng cảm thì bạn sẽ đạt đến đích của hạnh phúc.....
5
97829
2016-05-24 13:23:29
--------------------------
423410
3765
Tình cờ đọc ké của người bạn và không dừng được. Rồi sau đó phải về tìm lại phim để xem (mặc dù trước đó mình xem được 1 đoạn thì chê dở và không xem nữa). Truyện đúng là đã truyền cảm hứng và hướng người ta đến những giá trị gia đình và lạc quan trong tình yêu. Không phải tiểu thuyết lãng mạn, tình yêu không hề trơn tru, nhưng vẫn cuốn hút người đọc và mình thích cách 2 nhân vật chính ứng xử với hoàn cảnh. Rất dễ thương, rất đáng để học hỏi. Đọc truyện thích hơn phim ;)
4
42014
2016-04-29 11:00:53
--------------------------
409375
3765
Sách hay. Nội dung lạ mà quen. Ngôn từ câu văn rất hay và thú vị, dí dỏm, hài hước. Nói về hành trình 2 người yêu nhau vượt qua bao khó khăn, định kiến xã hội và đến với nhau bằng trái tim của tuổi trẻ mới, phóng khoáng và tự do. Nó tiếp thêm cho tôi động lực về tình yêu trong cuộc sống. Đáng để đọc. Ngoài ra chất lượng sách tốt. Bìa đẹp. Cốt truyện hấp dẫn, lôi cuốn từ chương này đến chương khác. Dù nó không ly kỳ nhưng sự nhẹ nhàng mà ý nhị truyện đã mang đến luồng gió mát lành đầy hy vọng cho tâm hồn người trẻ... Hay
5
692910
2016-04-02 13:35:56
--------------------------
394044
3765
Khi đọc quyển sách này tôi có cảm tưởng như xem một bộ phim truyền hình với đủ cảnh gia đình hai bên gây khó dễ cho cặp đôi trai gái yêu nhau. Giọng văn trào phúng của Chetan Bhagat giới thiệu với độc giả tính cách khác nhau của hai gia đình đến từ miền Nam và miền Bắc Ấn Độ với những đặc thù văn hoá khác nhau, đối chọi nhau. Truyện tập trung chính vào chủ đề gia đình và tầm quan trọng của việc có được sự ủng hộ từ gia đình hai bên khi kết hôn. Với tính cách của lớp trẻ hiện đại, đôi trai gái dễ dàng tự ý kết hôn bất chấp sự phản đối của đôi bên. Thế nhưng, nhờ vào tình yêu thương của những người con hiếu thảo đối với cha mẹ, đôi trai gái trong truyện đã nhẫn nại, nỗ lực phi thường để có được sự chấp thuận của hai bên gia đình và đi đến một đám cưới hạnh phúc.
5
33461
2016-03-09 20:50:46
--------------------------
371714
3765
Câu chuyện trong cuốn sách này đã được chuyển thể thành phim điện ảnh "2 States" rất nổi tiếng của Ấn Độ. Tôi đã xem đi xem lại phim không dưới 3 lần trước khi quyết định mua cuốn sách này. 
"Khi yêu cần nhiều dũng cảm" là câu chuyện tình yêu nhiều ý nghĩa của Krish và Ananya - hai người trẻ tuổi Ấn Độ đến từ hai vùng miền khác nhau, đem theo nhiều khác biệt về văn hóa, quan điểm mà ngay từ đầu, tình yêu là chưa đủ để giải quyết những khác biệt này.
Tôi thích cô gái Ananya ấy vô cùng. Cách cô ấy giải quyết tình huống trong đám cưới của em họ Krish, sự xinh đẹp, tự tin, thông minh của cô ấy khiến tôi nghĩ gia đình Krish đã hời nếu chấp nhận cô ấy làm con dâu. Trải qua bao khó khăn, hai người cũng đến được với nhau, tôi rất xúc động trước câu nói của mẹ Krish với Ananya trong lễ cưới, bà nuôi anh trở thành thạc sĩ và mong anh mang về cho bà một cô con dâu, nhưng anh đã mang về một cô con dâu thạc sĩ - vượt qua cả mong đợi của bà. 
Vì có tình yêu và trên hết là sự kiên trì và tấm lòng chân thành, những người yêu nhau đã đến được với nhau!
5
41486
2016-01-20 11:48:06
--------------------------
358280
3765
Bản thân đang có nhiều chuyện không vui, khó nghĩ, cách để mình giảm cảm giác khó chịu đó đi là đọc sách. Và việc chờ đợi khi mua sách từ Tiki cũng là một thứ gọi là hạnh phúc. hehe. 

Mình đọc một lèo là bay vèo, đọng lại nhiều điều, cái dũng cảm mà mỗi người khi yêu cần đối mặt đều được tác giả lột tả hết trong tác phẩm, hài hước là yếu tố không thể yếu ở tác giả này. Ý nghĩ của nhân vật nam khi độc thoại rất thực tế và những ý nghĩ thường trực suốt trong cuộc nói chuyện của họ. Để rồi, "Tha thứ không khiến người làm con tổn thương cảm thấy tốt hơn, mà nó làm cho con cảm thấy tốt hơn.". Có nhiều chuyện tha thứ không phải là chuyện dễ dàng nhưng quá trình đi đến để chấp nhận rồi nguôi ngoai rồi tha thứ rất rất rất mệt mỏi. Quan trọng có ai để chia sẻ được không thôi, quá cô độc khi bước trên con đường đời hơi dài này. 

Tóm lại, một cuốn sách đáng để đọc.


5
927374
2015-12-25 07:45:07
--------------------------
353903
3765
Nhiều ấn tượng về cách viết , không đi chung mà tìm hiểu rõ về cảm nhận của tác giả qua các nhân vật , thấy tình yêu không quá mềm mại mà cần nhiều tác động mạnh lên nó , không gian tràn ngập sâu sắc , đem đến tình yêu ai cũng mong ước có được , cô gái có nội tâm phong phú , vui vẻ , yêu đời nhưng lại cũng khó nắm bắt được con người cô có những hành động gì tiếp theo , thay đổi những gì , đọc truyện của nhà văn cho thấy sự thư giãn đến với thời gian cảm nhận .
4
402468
2015-12-17 05:10:51
--------------------------
347275
3765
Một câu chuyện nhẹ nhàng, rất thực nhưng chứa đựng sự đấu tranh quyết liệt, dám đấu tranh vì tình yêu của hai nhân vật chính. Có lẽ đã có nhiều bạn chia tay vì sự ngăn cản của gia đình nhưng chính cuốn sách này sẽ cho mình và các bạn những suy nghĩ tích cực hơn. Không gì là không thể hãy cố gắng hết sức vì cuộc sống này luôn có những thử thách để chính bạn tìm ra những cách giải quyết của riêng mình. Chắc hẳn những bạn thích và đam mê đọc sách như mình sẽ hiểu được cảm giác khi tìm và đọc được một cuốn sách hay và tâm đắc. Cảm ơn tác giả Chetan Bhagat với "Khi yêu cần nhiều dũng cảm" đã đem lại cho mình những trải nghiệm đó. Hy vọng sẽ có nhiều bạn cùng cảm nhận như mình qua cuốn sách này.  
5
691933
2015-12-04 09:55:22
--------------------------
338223
3765
Dù biết "3 chàng ngốc" là tác phẩm làm nên tên tuổi của Chetah Bhagat nhưng tôi chọn "Khi yêu cần nhiều dũng cảm" để đọc trước vì thích thú hơn với những câu chuyện lãng mạn, cũng như cảm thấy giọng dịch của tác phẩm này rất dễ chịu. Cuốn sách thật sự đáp ứng được những trông đợi ban đầu của tôi, khiến tôi "phải" đọc liền hết trong một ngày, và có lẽ cả nhiều ngày sau nữa dành để tìm hiểu thêm về những nét văn hóa thú vị được nhắc đến trong truyện. Quả thật tìm hiểu về một đất nước thông qua những tác phẩm văn học luôn là việc rất tự nhiên và dễ chịu!
4
80143
2015-11-15 17:59:05
--------------------------
328932
3765
Mình mua quyển này từ tầm hơn 2 tháng trước. Mình không hay đọc văn học Ấn Độ, chỉ mua quyển này khi biết tác giả là người sáng tác ra truyện "3 chàng ngốc" (mình được giới thiệu xem bộ phim chuyển thể từ truyện này). 
Ấn tượng để lại sau khi đọc tác phẩm là cả một văn hóa Ấn Độ đầy những hủ tục rườm rà và khác biệt từng vùng miền được hiện ra. Đằng sau một câu chuyện tình đầy nghị lực còn là cả một nền văn hóa, cách nghĩ, lối sống của người Ấn Độ. Từ đó triết lý nhân văn tác giả muốn gửi đến người đọc Ấn nói riêng và trên toàn thế giới nói chung được bộc lộ, gửi gắm. 
Đây là một tác phẩm hay, nên đọc với các bạn trẻ, người ham mê tìm hiểu những điều mới lạ. Và mình tin, sau khi đọc xong chuyện này, bạn sẽ có một cái nhìn khác, sâu sắc hơn về văn hóa Ấn Độ.
5
96361
2015-10-30 20:43:58
--------------------------
325557
3765
Một lần bất ngờ toi đọc được quyển sách này. Và thấy nó vừa hay vừa cần thiết nén tôi đã len tiki để đặt ngay. Sách nay với lối hành văn đơn giản nhưng vẫn thu hút được người đọc. Toi và tất cả những người ban của toi khi biết đến nó đều rất hứng thú. Hơn nữa sách in đẹp chữ đẹp và bìa cũng đẹp. Giấy cũng rất tốt. Toi cảm thấy rất hứng thú khi cầm nó. Cảm ơn tác giả. Và toi cũng hài lòng với sự nhanh nhẹn của tiki, rất đúng hẹn.
3
694564
2015-10-23 19:58:10
--------------------------
321130
3765
Xin chào Tiki!
1. Mình góp ý thêm 1 tính năng cho Tiki là ví dụ trường hợp khách hàng mua hàng làm quà tặng nhưng do khách hàng quá bận rộn, muốn đặt hàng ngay, nhưng quà tặng đó đến ngày sinh nhật mới có ý nghĩa nên khách hàng muốn đặt hẹn ngày nhận hàng thì Tiki sẽ thêm tính năng "ngày nhận hàng" đấy vào đơn hàng.
Tiki có hiểu ý tưởng này không ạ?
Đó có phải là 1 ý tưởng cho phương thức kinh doanh của Tiki không ạ?
2. Đường link của Tiki khi kiểm tra đơn hàng trên email, mình vào toàn thấy báo là " đơn hàng không tồn tại". Tiki xem lại giúp mình nhé. Mà thực tế khi vào trực tiếp trên web để kiểm tra đơn hàng thì ok.
Cảm ơn dịch vụ của Tiki rất tốt.
5
854800
2015-10-13 11:35:17
--------------------------
306750
3765
Chuyện tình yêu luôn là vấn đề vô cùng phức tạp, không chỉ ở Việt Nam mà ở tất cả mọi nơi trên thế giới.
Tuy nhiên, tại Ấn Độ - một nơi vô cùng phổ biến vấn đề "Hôn nhân được sắp đặt trước" thì câu chuyện này trở nên thú vị và mới mẻ hơn. Thầy giáo mình - một người Ấn Độ, đã kể rằng Thầy chỉ gặp vợ của mình có 30 phút và khi hai người gật đầu thì hôm sau là đính hôn và chờ ngày tốt là đám cưới luôn. Bởi hai gia đình vốn là bạn của nhau từ trước, cùng màu da, sống ở cùng một bang tại Ấn Độ nên mọi thứ diễn ra vô cùng nhanh chóng.
Nhưng cuốn sách này lại đề cập đến một câu chuyện hoàn toàn trái ngược, đã nói lên sự sự phân biệt chủng tộc rõ ràng ở Ấn Độ: hai vùng Nam - Bắc. Vừa khác nhau trong văn hóa, cách biệt trong ngôn ngữ, màu da và cả đẳng cấp nữa... đã tạo nên một câu chuyện hấp dẫn, thú vị và mang nhiều tầng ý nghĩa về tình yêu chân chính.
Một câu chuyện đa sắc màu và mang đến nhiều cung bậc cảm xúc, văn hóa mới lạ đã khiến cho cuốn sách được nhiều người yêu thích. Mình thực sự ngày càng thích văn học Ấn Độ bởi sự mới lạ và thú vị mà nền văn hóa này mang lại!!!

5
34983
2015-09-17 19:37:04
--------------------------
300882
3765
- Câu truyện mang ý nghĩa tầm sâu về văn hóa được lột tả qua lối hành văn hài hước, dí dỏm của tác giả: câu truyện kể về tình yêu của 2 bạn trẻ đang học thạc sĩ, truyện sẽ không có gì nếu 2 bạn không phải thuộc về 2 vùng khác nhau của Ấn Độ (1 người đến từ Bắc Ấn và 1 người đến từ Nam Ấn trong bối cảnh nạn phân biệt vùng miền dù trong cùng 1 nước của Ấn Độ còn mạnh mẽ hơn nạn phân biệt màu da ở Mỹ). Đọc xong mình thấy rất khâm phục 2 nhân vật chính, để đến được với nhau họ không những phải thuyết phục được bố mẹ 2 bên mà còn phải được sự đồng ý của cả họ hàng 2 bên nữa. Đọc xong truyện mình hiểu hơn về văn hóa Ấn Độ: họ cũng coi trong bằng bấp, địa vị xã hội, sự giàu nghèo như Việt Nam vậy.  Nếu truyện này được chuyển thể thành phim mình tin chắc nó cũng hấp dẫn như Ba chàng ngốc vậy.
5
8072
2015-09-14 11:58:10
--------------------------
293837
3765
Tất cả các cuốn sách của Chetah Bhagat mình đều đã đọc và thích lắm. Mấy câu chuyện trong sách của ông đều rất ý nghĩa với lôi cuốn nữa, nên khi thấy Nhã Nam mới xuất bản cuốn này đã không ngần ngại mà mua luôn! Đọc xong thì cũng thấy khá hay, không bị thất vọng thôi chứ thực ra cũng không quá hài lòng! Câu chuyện nói về hành trình tiến đến hôn nhân của Krish và Ananya, vô cùng khó khăn luôn vì sự phân biệt về vùng miền ở Ấn Độ, cũng có những tình huống khá hài hước, dí dỏm. Đọc xong thì hiểu hơn về văn hóa Ấn Độ nhiều, thích lắm vì có thêm kiến thức mới sau khi đọc xong ý. Với cốt truyện này khá lạ lẫm nên đọc có cảm giác mới mẻ không bị nhàm chán, còn khá ý nghĩa nữa. Đọc rồi mới biết không phải cứ yêu nhau là đến với nhau được, còn phải cần " nhiều dũng cảm" nữa! Khâm phục tình yêu của 2 nhân vật trong truyện nữa! 4 sao!
5
458906
2015-09-08 19:40:54
--------------------------
293611
3765
Đọc xong mới thấy thật sự thấu hiểu thế nào là yêu một ai đó. Giữa biết bao nhiêu người giỏi giang đeo bám trong một ngôi trường danh tiếng nhưng Ananya lại có tình cảm với Krish. Với sự khác biệt rõ rệt về văn hóa hai vùng miền - Nam Ấn và Bắc Ấn, họ đã phải đấu tranh rất nhiều với bố mẹ của cả hai để giành hạnh phúc cho nhau. Với cả lí trí, sự thông minh, lanh lẹ Krish và Ananya đã chiếm được sự yêu quí và tôn trọng của tất cả mọi người. 
Thật sự ngưỡng mộ!

4
577736
2015-09-08 14:50:01
--------------------------
277172
3765
Không như các cuộc hôn nhân trong cuộc sống, hai người yêu nhau rồi họ kết hôn, hành trình đến với cuộc hôn nhân của Krish và Ananya rất khó khăn, gian nan. Họ bị gia đình, họ hàng của mình phản đối ngăn cản cũng như những thành kiến giữa người Nam Ấn và người Bắc Ấn , mặc dù vậy hai người họ vẫn cố gắng bảo vệ, đấu tranh để mọi người có thể chấp nhận tình yêu của họ dành cho nhau. Tác giả đã thành công trong việc bộc lộ tâm tư, cảm xúc về nhân vật và quan điểm của chính mình trong tác phẩm " Khi yêu cần nhiều dũng cảm" này. Bìa sách đáng yêu, chất lượng cũng rất tốt. Tác phẩm là một tác phẩm hay mà tất cả mọi người nên đọc, nó sẽ không khiến phải hối hận khi đọc.
4
727431
2015-08-24 19:05:06
--------------------------
260074
3765
"Khi yêu cần nhiều dũng cảm" là một tác phẩm tương đối, nhẹ nhàng, đáng yêu. Một cậu chuyện tình kéo dài từ thuở còn đại học đến khi đã trưởng thành và bước ra cuộc đời, đối mặt với sóng gió của sự phản đối quyết liệt từ hai bên gia đình tách biệt hoàn toàn về văn hóa sống.

Ấn Độ là một đất nước đa dạng và phức tạp về văn hóa, vùng miền, ngôn ngữ, con người. Nơi mà da đen và da trắng hòa trộn, nơi những con người trong cùng một đất nước nhưng khác thành bang với định kiến và quan điểm khác nhau. Chính điều đó đã hình thành một rào cản lớn cho đôi tình nhân thuộc hai miền Nam-Bắc. Để đến được với nhau họ đã phải trải qua nhiều thử thách hòng chinh phục gia đình hai bên.

Nếu không vì sự can đảm và lòng quan tâm chân thật đối với người bạn đời tương lai, thì có lẽ Krish và Ananya đã không thể có một đám cưới trọn vẹn. Như thế mới biết, "Khi yêu cần nhiều dũng cảm" nếu không có được điều đó, tình yêu đã đỗ vỡ từ lâu. Chúc mừng cho Krish và Ananya.
3
52926
2015-08-11 08:31:20
--------------------------
257971
3765
"Nếu như lúc này, mưa nặng hạt hơn và gió cũng mạnh hơn... Thì có lẽ em,sẽ chẳng màng điều gì nữa mà bật khóc, thả lòng theo cùng cơn mưa..." Một tình yêu đẹp nhất là khi chàng trai và cô gái chỉ mới để ý đến nhau, chưa có mối quan hệ nào nhất định và ràng buộc. Thăng hoa nhất là khi họ trở thành một cặp. Hạnh phúc nhất là khi họ có thể vượt qua mọi rào cản và đến với nhau, để có thể sống đầu bạc răng long, đến cuối cuộc đời này!
4
733721
2015-08-08 09:28:07
--------------------------
253076
3765
Tôi cứ nghĩ đây sẽ là một câu chuyện tình gay cấn, nhiều sự lãng mạn và ngọt ngào như các thể loại tiểu thuyết tình cảm khác. Nhưng đây lại là một câu chuyện khác hẳn,cuộc đấu tranh bảo vệ tình yêu của Krish và Ananya, không chỉ là đấu tranh cho tình yêu, mà còn là đấu tranh chống lại những định kiến đã tồn tại từ lâu đời giữa 2 miền Nam Ấn và Bắc Ấn.Qua tác phẩm ta nhìn thấy được một Ấn Độ với nền văn hóa lâu đời, sự phức tạp, đa dạng về văn hóa từng vùng miền là không hề nhỏ,một đất nước thật sự có quá nhiều điều để ta khám phá .
Văn phong nhẹ nhàng ,đôi khi pha chút hài hước, khiến cho mạch câu chuyện trở nên dễ đọc và lôi cuốn.Bạn nên đọc nó, và bạn sẽ không phải hối tiếc đâu . 
5
616860
2015-08-04 09:35:46
--------------------------
250857
3765
Mình mua cuốn sách này một phần vì bị cuốn hút bởi nhan đề. Phần mở đầu có thể sẽ gây cho người đọc sự nhàm chán. Truyện không có những tình tiết quá lãng mạn như những câu chuyện tình yêu mà chúng ta vẫn thường biết đến qua các tiểu thuyết ,phim ảnh. Nếu bạn trông chờ một câu chuyện tình yêu với những tình tiết lâm ly, gay cấn thì có lẽ bạn không nên đọc cuốn sách này. Bởi vì cuốn sách này không chỉ là câu chuyện về tình yêu của đôi bạn trẻ mà còn là về tình cảm gia đình, sự bao dung, độ lương.
"Khi yêu cần nhiều dũng cảm" là hành trình bảo vệ tình yêu của Krish và Ananya. Hai người họ đã nắm tay nhau để vượt qua những thành kiến của người Nam Ấn và Bắc Ấn, vượt qua những bất đồng của hai gia đình. Câu chuyện còn đan xen những tình tiết hài hước những cũng đầy thấm thía. Mỗi người bố mẹ đều có những cách bảo vệ con cái của riêng mình, mỗi người một ý kiến những tựu trung lại đó là tình yêu thương con vô bờ bến.
Truyện càng đọc càng hấp dẫn. Sự thông minh của Ananya, sự chân thành của Krish, tình cảm gia đình, tình cảm cộng đồng...tất cả đã làm nên một câu chuyện hay và ý nghĩa.

4
62056
2015-08-02 10:58:00
--------------------------
242652
3765
Sản phẩm của Nhã Nam hiện nay về mẫu mã thì khỏi phải bàn- quá đẹp. Tác giả Chetah Bhagat sau 2 tác phẩm làm quen với độc giả Việt Nam là Ba chàng ngỗ và Ba sai lầm của đời tôi lần này lại có sự tái ngộ khác biệt và vô cùng thú vị là chủ đề về hôn nhân. Hoài bão của nhân vật trong tác phẩm không phải là một ngành nghề đúng với đam mê của mình. Mục tiêu của họ là đến được với nhau. Công việc của họ chỉ đáp ứng cho họ một thứ duy nhất- Tiền. Nhân vật trong tác phẩm được xoáy sâu thêm về nội tâm cùng nhiều chi tiết nói về gia đình hơn là các tác phẩm trước đây...
5
292668
2015-07-26 20:48:03
--------------------------
237164
3765
Mình kết tác phẩm này ngay từ những câu từ đầu tiên, không hiểu sao cách kể chuyện của tác giả khiến mình phải tủm tỉm cười mãi. Cứ nghĩ rằng sẽ là một kiểu tác phẩm nhẹ nhàng và đáng yêu, thích hợp đọc để giải trí thôi. Nhưng thực sự tác phẩm đem lại cho mình rất nhiều suy ngẫm. Với tư cách một người miền Bắc sống ở miền Nam, mình hiểu rất rõ những trờ ngại mà sự khác biệt văn hóa đem lại. Dù tự cảm thấy là một người khá hòa đồng, vậy mà đôi khi mình vẫn cảm sự ngăn cách nào đó với bạn bè và mọi người xung quanh, trong khi những khác biệt giữa hai miền cũng không thực sự quá lớn. Ấy vậy mà vấn đề của hai nhân vật chính và gia đình hai bên còn hơn thế rất nhiều. Họ khác nhau từ tín ngưỡng, dân tộc, lối sống, cách nghĩ, tới cả chuyện ngôn ngữ tưởng chừng như là điểm chung tất lẽ hóa ra cũng không phải tất lẽ như trong suy nghĩ. Thì ra xã hội Ấn Độ lại phức tạp tới vậy, thì ra một đất nước cũng có thế có nhiều khác biệt giữa người với người đến thế. Cuộc hành trình vượt qua định kiến và khác biệt của họ không hề vui như mình tưởng, cũng có những lúc rất trầm, rất mệt mỏi. Tuy nhiên, nó cũng không hề nặng nề, xen vào giữa những dấu trầm là những mảnh đối thoại đầy hài hước, một số nhân vật phụ đến không thể phụ hơn như ông sếp của Krish ở Citibank, ông chủ nhà trọ, mấy anh chàng cùng phòng... tất cả đều mang tới một chút thú vị và một chút hài hước. Một cuốn sách có thể khiến bạn cười rất nhiều nhưng đồng thời ẩn chứa rất nhiều thứ cần phải suy ngẫm. Còn gì tuyệt vời hơn thế!
5
420102
2015-07-22 11:36:01
--------------------------
221895
3765
Một cuốn sách hay nữa của Chetan Bhagat. Sau khi đọc cuốn này tôi đã trở thành một fan hâm mộ cuồng nhiệt của Chetan. Một câu chuyện rất rất rất thú vị về người Ấn Độ và cách họ yêu một người không cùng đẳng cấp. Mọi nhân vật trong truyện ai cũng đều ấn tượng hết. Câu chuyện tình yêu Ananya và Krish cứ khiến tâm hồn tôi lân lân mãi. Và có lẽ đặc sắc và thú vị nhất của cuốn sách có lẽ là những câu mà Krish tự nói với chính mình!

Cuốn sách là một lựa chọn "không phải dạng vừa" dành cho những trái tim đang yêu!!!!!!!!
4
7141
2015-07-04 16:39:53
--------------------------
206651
3765
Chuyện kể về một anh chàng người Punjab gặp, quen, tìm hiểu và bắt đầu yêu một cô gái người Tamil. Họ học MBA cùng nhau, ở xa gia đình trong thời gian yêu nhau. Sau khi ra trường, nàng chọn HHL ở Chennai, chàng thì đứng giữa vai trò là một người con, vai trò là một người yêu, đứng giữa lựa chọn Citibank ở Chennai và Delhi. Cuối cùng cả 2 đã có 6 tháng làm việc tại Chennai. Anh chàng đã chấp nhận sự hắt hủi của gia đình cô gái, sự ruồng bỏ, sự cấm đoán. Nhưng bỏ qua hết, vì tình yêu thương dành cho cô, anh chàng cố gắng để yêu thương cả gia đình cô dù họ có đối xử, hay xa lánh mình như thế nào. Cuối cùng thì anh ta cũng xong bước 1, có được sự chấp thuận từ gia đình cô gái. Sau đó là bước 2, cô gái cần có được sự chấp thuận từ họ hàng và gia đình chàng trai. Nhưng bù lại cô chỉ có 1 tuần để làm việc đó, thay vì 6 tháng, mọi thứ khó khăn nhiều hơn. Rồi những nghiệt ngã cũng đến, gia đình cô chấp nhận anh, gia đình anh chấp nhận cô, nhưng 2 bên gia đình vãn không thể chấp nhận nhau. Rồi cô và anh lại chia tay, xa nhau một thời gian vì những hiểu lầm, vì những trách nhiệm của một người con phải gánh vác. Sau hết thảy những điều đó, bố anh xuất hiện, là người hàn gắn tất cả, và mang lại một kết thúc hạnh phúc cho 2 bạn trẻ, cho gia đình 2 bên. 

Ông bố của anh là điểm nặng nề nhất trong cả câu chuyện, nhưng cũng là ánh sáng cuối đường hầm đó. 

Truyện có quá nhiều nút thắt, càng ngày càng chặt và khó gỡ hơn, nhưng chỉ cần một điểm nhấn duy nhất để gỡ bỏ được tất cả mọi thứ và người làm việc đó không ai khác là chàng trai. 
Và điểm nhấn đó cũng chính là điểm mà mình thích nhất trong câu chuyện của họ, là điều mình đọc và cảm thấy nhẹ nhõm, là lúc anh ta đi gặp một vị Guru.
Và đọc hết câu chuyện, mình đã hiểu ra tại sao "khi yêu cần nhiều dũng cảm".
5
65728
2015-06-10 11:13:07
--------------------------
204716
3765
Mình từng đọc và xem bộ phim ba chàng ngốc của tác giả Chetan Bhagat và mình nghĩ rằng, mọi tác phẩm của Chetan Bhagat đều mang một thông điệp vô cùng ý nghĩa. Cuốn tiểu thuyết Khi Yêu Cần Nhiều Dũng Cảm đã giúp mình chứng minh điều đó. Về nội dung, cốt truyện xoay quanh mối tình sâu đậm của chàng trai và cô gái Ấn độ, trải qua bao sóng gió, ngăn cản của gia đình nhưng kết thúc có hậu đã mang họ đến với nhau và chứng minh cho cả thế giới biết rằng, sức mạnh của tình yêu là một điều kì diệu. Về hình thức cuốn tiểu thuyết, mình thấy bìa tiểu thuyết khá ổn, giấy in đẹp. Mình rất ấn tượng với tiểu thuyết này!
4
611652
2015-06-04 19:29:50
--------------------------
201934
3765
Chetah Bahgat có cách viết tự nhiên và lôi cuốn, xây dựng nhân vật tốt. Truyện là sự đấu tranh cho tình yêu của 2 nhân vật cùng ở đất nước Ấn Độ, cùng trình độ nhưng lại thuộc về 2 thế giới văn hóa khác biệt. Thế mới thấy tình yêu đâu chỉ là màu hồng, nắm tay nhau đi hết cuộc đời mà phải luôn có sự nỗ lực từ 2 phía, nhất là khi đi đến hôn nhân thì không còn là chuyện chỉ của 2 người mà là của 2 gia đình, và suy cho cùng cha mẹ nào cũng mong con cái mình hạnh phúc. Sau khi đọc xong, mình tìm phim 2 states xem (được dựa trên truyện này) thấy dàn diễn viên đẹp, nhạc hay nhưng không thu hút như lúc đọc truyện (chắc là do đọc trước rồi).
5
54514
2015-05-28 15:36:53
--------------------------
201115
3765
Về hình thức tôi chỉ đánh giá tạm được , không quá xấu cũng không quá đẹp . Về chất lượng giấy in mềm , tốt , nhưng vẫn còn vài trang bị in lỗi không được rõ ràng lắm . Về nội dung khi yêu cần nhiều dũng cảm là một cuốn sách hay và rất đáng đọc . Mạch truyện nhẹ nhàng đằm thắm , nền tảng của câu chuyện được xây dựng khá tốt . Cách hành văn của tác giả không chỉ tài hoa mà còn rất có sức hút đối với tôi . Truyện truyền tải thông điệp mang tính nhân văn sâu sắc yêu hết mình , dũng cảm đấu tranh vì tình yêu . Tôi nghĩ cuốn sách này rất thích hợp cho những bạn trẻ đang bối rối trong tình yêu vì nó thực sự giống như một lời khuyên bổ ích vậy
5
635463
2015-05-26 17:55:29
--------------------------
197451
3765
Khi yêu cần nhiều dũng cảm là cuốn sách có lẽ hay và ý nghĩa nhất mà tôi từng đọc. trên đường đời mỗi con người sẽ có rất nhiều người đến với mình nhưng người mình yêu thì chỉ có một.Vậy nên cần dũng cảm để tiến tới , cảm nhận và đấu tranh cho tình yêu đó. Một tình yêu chân chính không bao giờ là một câu chuyện cổ tích chỉ có công chúa và hoàng tử nắm tay nhau hạnh phúc đến đầu bạc răng long. tình yêu chân chính cần phải trải qua sóng gió, khó khăn để chứng minh....... và tình yêu giữa một người con trai và một người con gái chỉ thực sự gắn bó khi họ cùng nắm tay nhau trải qua những sóng gió cuộc đời. Tôi nghĩ cuốn sách chính là một cuốn cẩm nang thú vị cho những bạn trẻ đang yêu và đang bối dối trước tình yêu của đời mình thì hãy đọc cuốn sách này để có cho mình những trải nghiệm thú vị .
5
624798
2015-05-17 17:59:09
--------------------------
