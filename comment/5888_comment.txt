560345
5888
Cực kì thích cách giao hàng của tiki - nhanh chóng, cẩn thận. 
Về quyển sách này m cũng thích nữa, viết chi tiết và tỉ mỉ lắm
4
5123549
2017-03-31 18:06:02
--------------------------
517405
5888
Đây là cuốn sách về mỹ phẫm handmade đầu tiên mình mua. Xem trên  youtube của granpa gardern rất thích. Và nhờ nó mà mình đã làm ra son dưỡng môi handmade đầu tiên. Son handmade Wonie Store của mình bắt đầu đến nay cũng được hơn 1 năm. và mình vẫn rất yêu quí cuốn sách này. Nó cho mình nhiều điều hơn là mình biết.Và chị Thư Đỗ cũng là tấm gương để mình noi theo. Học hỏi. Cuốn sách này rất tuyệt cho những ai yêu và đam mê handmade như mình.
5
1393491
2017-02-01 22:49:17
--------------------------
489221
5888
Mình mua cuốn này lâu r nhưng giờ mới viết nhận xét. Tiki làm việc rất nhanh, cẩn thận, chu đáo và tỉ mỉ. Khi mình đặt hàng dự kiến giao vào thứ 5 hoặc thứ 6 nhưng mà thứ 4 đã có sách r. Mình không dùng dịch vụ bookcare nhưng sách lại phẳng phiu, đựng trong hộp giấy nhìn rất chắc chắn. Sách rất đẹp chứ không phải như một số nhà sách để sách lung tung, giấy nhàu đi. Mình cũng rất thích cuốn này. Đọc nó mình tự làm được nhiều loại son, nước hoa... Bảng thành phần trình bày rất rõ ràng, mình có thể gia giảm nguyên vật liệu theo ý thích nữa. Bạn nào đọc qua rồi khắc biết nó hay như thế nào. Túm cái váy lại thì sách hay, tiki làm việc cực tốt. Ủng hộ tiki.
4
1028768
2016-11-07 06:28:03
--------------------------
478247
5888
Cuốn sách có sự đầu tư kĩ lưỡng về nội dung cũng như hình ảnh minh họa. Hướng dẫn làm mỹ phẩm khá chi tiết, đan xen những bài viết cảm nhận, đánh giá của tác giả. Có một phần hạn chế là cuốn sách chỉ hướng dẫn làm những sản phẩm handmade đơn giản. Mình mong sau khi tái bản, nội dung sách sẽ được bổ sung với những công thức làm mỹ phẩm phức tạp hơn một tí! 
Cuốn sách khá đẹp và hữu ích! 
Tiki gói hàng cẩn thận, giao hàng nhanh và giá cả rất ưu đãi! Cảm ơn Tiki rất nhiều!
3
712573
2016-08-01 11:03:58
--------------------------
476194
5888
Lần đầu tiên mua sách qua mạng rất hài lòng về cách giao hàng của Tiki nhanh gọn , đặc biệt đóng gói kĩ càng đẹp mắt . 
Rất hài lòng khi cầm cuốn sách này trên tay. Hình ảnh được chăm chút kỹ lưỡng kèm theo những công thức chia sẻ chi tiết, đó là điểm đặc biệt của cuốn sách. Những lời khuyên rất thực tế của hai tác giả chứng tỏ đã nắm rất vững kiến thức về các nguyên liệu tự nhiên và những kinh nghiệm ấy xuất phát từ thực tế  đã trải nghiệm chứ không phải tự nghĩ ra. Đọc sách xong mình rất muốn tự tay làm cho mình một thỏi son dưỡng ẩm hay một hộp kem ngay 
5
1511301
2016-07-19 08:16:44
--------------------------
473248
5888
Hồi cuốn sách này mới ra mình đã canh rất lâu mới có thể mua được nó trên tiki. Thực sự cuốn sách này rất bổ ích, nhất là cho chị em chúng ta. Ta cần có một hiểu biết nhất định về mỹ phẩm để có thể chăm sóc da một cách đúng đắn. Cuốn sách này hướng dẫn cách ta có thể dưỡng da bằng những thứ tưởng như rất bình thường, không phải bỏ tiền hằng trăm ra để mua những thứ mỹ phẩm mà chưa chắc có hợp với da ta hay không. Ngoài ra sách còn có những lưu ý rất thiết thực về các sản phầm làm đẹp.
5
381211
2016-07-10 13:22:32
--------------------------
460010
5888
Đầu tiên phải nói đến đó là bìa sách đẹp, rất thu hút. Giấy dày tốt. Nội dung thì tôi rất thích. Quyển sách hướng dẫn làm nhũng mỹ phẩm thông dụng. Hai chị tác giả hướng dẫn rất chi tiết nên tôi làm theo cũng khá dễ dàng. Tôi chỉ mới bắt đầu làm son thôi, vì tôi vừa hết cây son dưỡng được đứa bạn tặng nên mua sách về để tập làm son dưỡng. Tôi đã thử làm hết các loại son và kết quả thì rất là như ý. Tôi cũng sẽ thử làm các sản phẩm tiếp theo vì tôi thích mỹ phẩm thiên nhiên lắm. Vẫn mong chị Anh Thư và chị Hương Giang tiếp tục hợp tác để cho ra các sách về mỹ phẩm thiên nhiên nữa !
4
1193209
2016-06-26 18:39:35
--------------------------
450262
5888
Quyển sách này thích hợp cho những ai có đủ thời gian và vật lực để làm đẹp, vì có một số các thành phần nguyên liệu không phải muốn kiếm là kiếm được, lại đòi hỏi các thiết bị đong đếm, cho nên không phải dễ dàng làm nên sản phẩm, dù sách hướng dẫn khá chi tiết! Tuy nhiên, ngoài cách hướng dẫn thì việc sách trình bày những thành phần hoá học rất tốt cho người đọc, bởi có thể giúp cho người đọc biết những thành phần gây hại và tránh xa trước khi đặt niềm tin quá nhiều cho những lời quảng cáo.
4
516261
2016-06-18 17:50:01
--------------------------
441441
5888
Về Phần giao hàng nhận sách: khá là thích cách làm việc của Tiki , nhanh gọn , từ bao bì đóng gói gói luôn. Ấn tượng đầu tiên mở sách  từ bìa đến giấy ,hình ảnh đẹp , rõ ràng, giấy xịn :)) . Nội dụng dễ hiểu , giải thích chi tiết, từng nội dung phân chia rõ ràng.  Chưa từng đặt sách qua mạng và đây là lần đầu tiên nhưng cực kì hài lòng luôn. Ủng hộ tiki dài lâu . Bạn nào đang có những cầu muốn tìm hiểu về cách công thức đơn giản thì nhất định phải tham khảo cuốn bày.
5
870981
2016-06-03 10:41:33
--------------------------
435896
5888
1. Bìa cứng, ảnh bìa siêu đẹp. Bên trong ảnh màu, toàn ảnh được chọn lọc, giấy bóng, rất rất đã
2. Chi tiết. Tác giả thật sự rất tâm huyết, từng tấm ảnh, từng câu hướng dẫn, từng lời giải thích tất cả đều rất tuyệt. Hình ảnh thì được chọn lọc từng tấm (theo lời giới thiệu của tác giả), nên rất đẹp, rõ nét, chi tiết. Do là người trong nghề nên tác giả viết lời giới thiệu rất rõ ràng và chi tiết. Với người chưa biết nhiều về mỹ phẩm đọc xong quyển sách bạn được học hỏi thêm rất nhiều kiến thức, không chỉ cách làm mỹ phẩm mà về mỹ phẩm bạn sử dụng hàng ngày.
MUA ĐI CÁI GIÁ KIA KHÔNG ĐẮT
4
403461
2016-05-25 22:06:23
--------------------------
403736
5888
Trong các sách về trào lưu tự làm mỹ phẩm, mình nhận xét quyển này là phù hợp thực tế, dễ hiểu, dễ thực hành nhất. Sách chia làm nhiều cấp độ khác nhau cho từng sản phẩm. Hướng dẫn chi tiết đến từng bước một, thêm thắt vào những kinh nghiệm thực tế của tác giả, những điểm nhỏ nhặt, nếu không chú ý sẽ rất dễ làm hư mỹ phẩm. 

Các bạn nếu không có ý định thực hành thì cũng nên đọc để thêm kiến thức về mỹ phẩm, sau này đi mua thì còn biết là mỹ phẩm tốt chứa những gì. 
4
50359
2016-03-24 09:50:39
--------------------------
396883
5888
Trước giờ tôi là người rất thích mỹ phẩm,tôi may mắn được biết đến cuốn sách này thông qua người em họ.Nó đã giới thiệu và cho tôi mượn đọc.Ý kiến riêng của tôi thì  muốn nói sách rất hay và phù hợp cho những bạn kinh doanh sản phẩm handmade,ngoài ra cũng dành cho những bạn thích tìm tòi,nghiên cứu về những loại mỹ phẩm handmade.Ngoài ra thì tôi cũng muốn nhận xét thêm,cuốn sách nhìn rất bắt mắt,đẹp,trình bày logic,người đọc dễ hiểu.Tôi khuyên các bạn nên rinh ngay về cho bản thân mình một cuốn để khi làm đẹp cũng có thể sử dụng ngay.
5
83467
2016-03-14 10:19:32
--------------------------
388040
5888
Đã mua 1 cuốn sách này ở Nhã Nam. Mở ra cảm nhận là sách rất đẹp. Chất giấy dày, bóng, hình ảnh bắt mắt. Thực sự với 1 đứa thích làm đồ handmade như mình thì tự làm mỹ phẩm quả thực là 1 cuốn sách rất đáng đọc. Mở ra cho mình nhiều hiểu biết hơn về các loại nguyên liệu tưởng là xa nhưng thực ra cũng có thể tìm thấy được. Rất nhiều công thức : son, nước hoa, kem dưỡng...có thể xem thêm clip của GG trên mạng. Tuy nhiên, công thức làm nước hoa khô thì tỉ lệ có hơi khác với các clip phổ biến.
Dù sao cũng là cuốn sách rất hay và bổ ích!
5
696970
2016-02-28 00:20:30
--------------------------
384978
5888
Sách tự  làm mỹ phẩm  rất hay . Nó giúp cho người  đọc nhiều  kiến thức  mới  và rút ra nhiều  bài học hay cho người  đọc sách . Cảm ơn chị Thu Giang và chị Anh Thư  đã soạn ra cuốn sách này để  giúp mọi người  . Sách làm rất đẹp . Hình ảnh  rõ nét màu sắc tương đối làm cho hình ảnh sống động hơn . Mấy bạn yêu thích mỹ phẩm  thì nên mua sách này tham khảo nhé . Sách rất bổ  ích em cũng mong rằng sẽ có những  cuốn sách như  thế này thì sẽ giúp mọi người  am hiểu  về mỹ phẩm  hơn
4
1160299
2016-02-23 12:26:01
--------------------------
379737
5888
Mình là một người rất đam mê Mỹ Phẩm. Cùng với tình yêu đó, mình đã tự làm những thỏi son xinh xắn, đáng yêu và tự kinh doanh chúng. Những ngày đầu mày mò tập làm son, cuốn sách dễ thương với nhiều màu sắc sinh động này đã giúp mình hiểu hơn về một thế giới bơ, sáp, tinh dầu và những hương liệu, màu khoáng thiên nhiên. Cuốn sách đã tiếp thêm tình yêu mỹ phẩm và giúp mình vượt qua những khó khăn bước đầu làm son. Cám ơn các tác giả, các chị của Grandpa's Garden đã chia sẻ, truyền đạt những kiến thức phong phú về  mỹ phẩm Handmade :))))
5
536892
2016-02-12 13:34:56
--------------------------
363629
5888
Mình có cô bạn cuồng mỹ phẩm một hôm chia sẻ quyển này trên facebook và cô bạn chuyển sang tự làm mỹ phẩm luôn nên mình lục tìm quyển này ngay. Đặt mua bao lần mà cứ hết hàng nhưng cuối cùng mình cũng kịp rinh được ẻm về. Vừa nhận được là lật ra xem ngay. 
- Về hình thức thì chắc không cần phải bàn cãi vì bìa sách đầy màu sắc nên rất bắt mắt. Ở các mục đều có hình ảnh minh họa.
- Về nội dung: Tác giả đã dành hết tâm huyết của mình vào quyển sách dành tặng cho những tín đồ mỹ phẩm, những người đam mê làm đẹp và yêu cái đẹp. Thành phần dễ tận dụng từ trong góc bếp nhà mình và dễ tìm mua. Có trình bày cả những lỗi hay gặp phải và công dụng của từng loại mỹ phẩm. 
Tóm lại đây là một quyển sách tuyệt vời
5
22704
2016-01-04 17:51:23
--------------------------
360377
5888
Tôi bị cuốn hút bởi bề ngoài cuốn sách màu sắc rất đẹp,cộng với niềm đam mê về mỹ phẩm, tôi quyết định mua nó. Khi đọc nội dung tôi thật sự rất thích thú,những dòng chữ viết từ chính tâm tư người làm mỹ phẩm,tình yêu chân thật dành cho mỹ phẩm handmade, miêu tả rất cụ thể. Đọc nội dung và hình ảnh gợi cho tôi suy nghĩ để thực hiện cuốn sách này người viết đã rất đầu tư, mà những gì xuất phát từ lòng chân thành và tận tâm đều xứng đáng được trân trọng
5
472437
2015-12-29 07:21:49
--------------------------
355757
5888
Đây là cuốn sách chỉ về làm mỹ phẩm rất chi tiết, rõ ràng, bản thân mình rất cảm ơn 2 tác giả đã rất tâm huyết với sách. Chỉ dẫn đầy đủ về từng sản phẩm, kèm theo hướng dẫn làm, và các chú ý cần thiết, đặc biệt là các lưu ý về nguyên liệu thiên nhiên cùng với các phẩm màu, chất hoá học nếu cần khi cho vào, giúp người đọc không mù mờ khi chế biến.
Về hình thức, sách được in màu, giấy dày, đẹp, rõ ràng.
Điểm trừ là các sản phẩm được hướng dẫn trong sách tập trung về mảng dưỡng, chống nắng, chứ chưa có chỉ dẫn cũng như cung cấp kiến thức về liệu trình chăm sóc da cho phù hợp với từng sản phẩm, nếu có thể mong 2 tác giả bổ sung hoặc làm thêm 1 cuốn về kiến thức chăm sóc da.
4
153783
2015-12-20 19:16:51
--------------------------
351593
5888
Mình rất có hứng thú với mỹ phẩm, đặc biệt là mỹ phẩm handmade nên quyết định mua sách này về để nghiên cứu. Hình ảnh trong sách rất đẹp, chất lượng giấy lại tốt làm mình có cảm tưởng giống như đang xem catalogue mỹ phẩm chứ không phải đọc sách. Nội dung sách thì khỏi bàn, có khá nhiều thông tin bổ ích cũng như lưu ý, kinh nghiệm cho đứa chuẩn bị bắt đầu làm mỹ phẩm handmade như mình. Hy vọng với cuốn sách này, mình sẽ cho ra lò những mẻ mỹ phẩm đầu tay thật chất lượng. Cảm ơn tác giả!
4
116540
2015-12-12 15:14:19
--------------------------
341484
5888
Cuốn sách có hình ảnh đẹp, minh họa rõ ràng, hướng dẫn chi tiết và nguyên liệu cụ thể, có thể ứng dụng và tạo cảm hứng cho người đọc.
Cuốn sách hướng dẫn nhiều về cách làm son, nước hoa, kem dưỡng... nhưng chưa có nhiều hướng dẫn làm đồ trang điểm. Nếu có thêm hướng dẫn làm phấn mắt, mascara, eyeliner, má hồng... có lẽ mình sẽ càng thích hơn. Vì đó cũng là những thứ các chị em phụ nữ vẫn hay sử dụng.
Cuốn sách rất bố ích cho những người muốn tự mình chăm sóc bản thân và những người thân yêu một cách tốt nhất.
4
947030
2015-11-22 10:16:20
--------------------------
337827
5888
Mình biết đến cuốn sách từ một người bạn thân. Đây là một cuốn sách khá hay và hấp dẫn. Đặc biệt là dành cho mình, vì mình cũng là một tín đồ làm mỹ phẩm handmade. Quan trọng là, cuốn sách chỉ dạy rất tỉ mỉ, phương pháp làm ổn và rất thành công. Hơn hết, bìa sách và nội dung bên trong rất hấp dẫn người đọc. Cảm ơn 2 chị nhiều vì đã viết cuốn sách hay như vậy. Hi vọng là em có thể học được các công thức và phương pháp để làm tốt hơn nữa.
5
953110
2015-11-14 20:20:12
--------------------------
336683
5888
Nhắc tới mỹ phẩm, vài người thường nghĩ đó là thú vui tốn kém và có phần xa xỉ, chỉ cần vung tiền ra là có thể làm đẹp, không cần quá nhiều kiến thức. Tuy nhiên, cuốn sách này đã cho mình thấy quan tâm đến mỹ phẩm và tự làm cho mình một món mỹ phẩm vừa ưng ý, vừa an toàn sức khỏe là một thú vui tinh tế, đòi hỏi đam mê, công sức nghiên cứu và óc quan sát nữa. Nhờ cuốn sách mà mình có thể làm những món mỹ phẩm handmade chất lượng đáng tin cậy, tiết kiệm tiền bạc, hiểu được một chút về mỹ phẩm để tránh những lời câu dụ của  shop online trôi nổi. Mình còn rất thích giọng văn của chị Thư, đọc mấy công thức với cảm nghĩ của chị mà ghiền làm mỹ phẩm luôn ý.
5
196061
2015-11-12 20:13:04
--------------------------
335775
5888
Mình biết đến cuốn sách từ một người bạn thân. Đây là một cuốn sách khá hay và hấp dẫn. Đặc biệt là dành cho mình, vì mình cũng là một tín đồ làm mỹ phẩm handmade. Quan trọng là, cuốn sách chỉ dạy rất tỉ mỉ, phương pháp làm ổn và rất thành công. Hơn hết, bìa sách và nội dung bên trong rất hấp dẫn người đọc. Cảm ơn 2 chị nhiều vì đã viết cuốn sách hay như vậy. Hi vọng là em có thể học được các công thức và phương pháp để làm tốt hơn nữa.
4
967917
2015-11-11 15:19:27
--------------------------
334452
5888
Dù có ý định làm hay không làm mỹ phẩm handmade thì cũng nên mua cuốn sách này. Sách viết rõ ràng, cụ thể, chi tiết, lại có thêm hình ảnh minh họa. Mọi chi tiết trong cuốn sách đều được chăm chút đến từng chi tiết.
Đọc xong cuốn sách, ta sẽ biết cách đọc các thành phần trên các loại mỹ phẩm. Cũng sẽ biết cách chọn loại mỹ phẩm phù hợp với da của mình. Nếu có thể tự làm mỹ phẩm, cũng biết cách điều chỉnh tỷ lệ nguyên liệu phù hợp với loại da, phù hợp theo mỗi mùa.
2
825511
2015-11-09 16:40:35
--------------------------
333306
5888
Cuốn sách này hoàn toàn là trang màu , giấy bóng và rất đẹp chưa đọc đã cảm thấy rất thích rồi nó chỉ tận tình liều lượng của từng  sản phẩm , nào là son môi, son nước,.... và chỉ các bạn nhiều loại mỹ phẩm cần thiết hằng ngày cho chị em phụ nữ chúng ta ,.. có nó mình như có thêm được nhiều bí  quyết và cảm thấy iu và tự tin vì mình là con gái mình thật sự rất thích nó còn là chi sẻ tình cảm của tác giả đối với người đọc còn là tình cảm chân thành mà tác giả giành cho người ông quá cố thật là xúc động
5
522480
2015-11-07 19:34:23
--------------------------
333301
5888
Sách hay.. Hình ảnh đẹp ^^ Cách viết chân thật gần gũi, chia sẽ nhiều kinh nghiệm hay trong quá trình làm mỹ phẩm. Hướng dẫn kỹ càng, nguyên liệu lại dễ kiếm... Sách rất phù hợp với mấy bạn muốn làm mỹ phẩm hand made như mình. Đọc xong càng yêu thích làm mỹ phẩm handmade hơn í ^^^^ 
Nói chung là mình đọc đến nhuyễn luôn rồi nhưng vẫn cứ đọc đi đọc lại, rồi nhìn hình ảnh, cứ kích thích sao í... Mong 2 chị ra thêm nhiều sách như vậy... Đặc biệt là một quyển dành riêng cho "SON" :* :* :)
5
767294
2015-11-07 19:28:24
--------------------------
327853
5888
mình đã từng làm mỹ phẩm đơn giản như son dưỡng, son màu và nước hoa khô, rồi sau đó là bán cho người khác dùng một thời gian. Cho đến khi đọc cuốn sách mình đã nhận ra và thừa nhận mình mắc một lỗi sai khá lớn: mình đã cho người khác dùng sản phẩm của mình mà chưa quan sát nó trong 6 tháng, đó là một việc rất nguy hiểm đối với mình và người khác. Cuốn sách này vốn rất quý, truyền cho mình tâm huyết làm mỹ phẩm bằng tâm chứ không phải vì lợi nhuận kinh tế. Hình ảnh sáng, đẹp, trình bày bố cục rõ ràng
5
763272
2015-10-28 16:38:25
--------------------------
327241
5888
Thấy mọi người khen quyển sách này quá cỡ, mình cũng ham hố muốn biết như thế nào.
Trước giờ mình chưa từng chế biến mỹ phẩm mà chỉ mua dùng thôi, hoặc tự làm các loại mặt nạ từ thiên nhiên. Lần này phải cố gắng học hỏi xem sao, biết đâu...
Vừa đọc sách, lại vừa xem clip của Grandpa's Garden, có lẽ mình sẽ học hỏi được nhiều điều. Mình dự định mua 2 quyển để tặng cho nhỏ em 1 quyền luôn, 2 chị em cùng hợp tác có lẽ tốt hơn nhỉ!!! Bởi vì nhỏ em khéo tay hơn mình rất nhiều, em đã tự làm các loại dầu dừa, dầu gấc để kinh doanh rồi, còn mình thì chỉ làm để dùng thôi, không có dám kinh doanh...hii...
5
901979
2015-10-27 13:54:50
--------------------------
307564
5888
Mình theo dõi blog và kênh youtube của chị Anh Thư từ những ngày đầu tập tành tự làm đồ handmade, hướng dẫn rất chi tiết, khoa học và dễ hiểu. Ngóng sách của chị ấy từ lúc mới phôi thai luôn. Cầm em nó trên tay mà m ưng lắm, tất cả đều in màu , cảm giác y chang quyển công thức mình vẫn ghi chép từ những bài chia sẻ của chị ấy luôn. Rất thân quen , không phải là sách nữa, như một quyển nhật ký hơn. Văn phong nhẹ nhàng, gần gũi. Hình ảnh minh họa cũng long lanh nữa. Thực ra các công thức trong sách chị Thư hầu như đã chia sẻ cả rồi, nhưng mình vẫn thích lắm.Mình cũng tự làm son theo công thức trên clip và rất thành công, đang đợi nguyên liệu để làm soap và sữa dưỡng trắng da đây. Hi vọng chị sẽ hướng dẫn thêm phần toner-xịt khoáng nữa ạ. 
À , ngoài lề 1 tí nhưng phong cách làm việc của Tiki rất chuyên nghiệp , giao hàng nhanh mà phí lại rẻ nữa 
4
815839
2015-09-18 10:10:57
--------------------------
303924
5888
Khi đọc cuốn sách này điều mình nhận được không chỉ là những công thức làm mỹ phẩm cực hay ho mà còn như ngọn lửa thổi bùng đam mê của mình trong một lĩnh vực khác. Đam mê không giới hạn và biến đam mê của mình thành hiện thực và có ích. Thật tuyệt.
Chỉ cần tranh thủ một chút các bạn có thể tự làm mỹ phẩm cho bản thân mình, cho người thân, bạn bè như một món quà nhỏ xinh và đặc biệt mà không nơi nào có được, vừa an toàn và làm chúng ta đẹp hơn mỗi ngày. Sách viết chi tiết và cụ thể lắm, rất dễ hiểu nhé!
5
400247
2015-09-16 07:32:04
--------------------------
303477
5888
Đây là một quyển sách hay và mới lạ của hai chị đưa ra, các công thức hướng dẫn cực kỳ kĩ lưỡng, từ lúc mua sách đến giờ mình đã làm ra được nhiều sản phẩm handmade hơn. 
Sách đẹp từ bìa sách, hình ảnh là cả một sự đầu tư rất cao. Từng danh mục sách rất đầy đủ, dù chỉ là một người tay ngang bạn cũng có thể dễ dàng thực hiện theo công thức trong sách.
Hi vọng sẽ còn nhiều sách về mỹ phẩm về làm đẹp mà tiki phân phối để mua hàng được dễ hơn nữa.
4
403665
2015-09-15 21:11:16
--------------------------
299210
5888
Mình đặt mua trên tiki vì được giảm giá và lại giao hàng tận nhà. Mình thích sách này lâu rồi nhưng vì không có thời gian đi mua, khi thấy tiki bán được giảm giá rẻ hơn so với giá gốc mình đã đặt mua ngay, sách hướng dẫn rất nhiều về cách làm 1 số mỹ phẩm đơn giản, và rất dễ hiểu. Hy vọng có cơ hội tìm đọc những loại sách hay như thế này nữa. Mình đang hóng sách dưỡng da trọn gói của chị Thư Đỗ đây.
Cảm ơn tiki đã giao hàng tận nơi cho mình, nhưng cũng hơi lâu vì mình ở Hà Nội, hi vọng lần sau mình sẽ được giao nhanh hơn nữa! 
5
783461
2015-09-13 07:07:15
--------------------------
294733
5888
Khi nhận đc quyển sách này mình rất ngạc nhiên khi thấy tất cả các trang đều đc in = giấy màu và có hình minh họa rất đẹp. Sách ghi rất rõ ràng, còn nói những nguyên liệu mà ta có thể thay thế. Có cả những lỗi sai khi làm son và các đặc tính của từng nguyên liệu. Nói chung từ chất liệu, hình thức cho tới nội dung đều rất tốt, cảm thấy hài lòng về quyển sách này. Tuy nhiên, dù trong sách này tác giả dùng những nguyên liệu cũng khá quen với các bạn mê mỹ phẩm handmade nhưng dù sao những bạn ở các tỉnh thành khác TP.HCM, HN thì cũng khó kiếm đc nguyên liệu và dụng cụ ưng ý. 
5
515730
2015-09-09 17:21:15
--------------------------
288875
5888
Một cuốn sách chỉ dẫn khá chi tiết và đầy đủ cho những tín đồ yêu thích mỹ phẩm hand made. Vơi quyển sách này việc tự tay làm nên một thổi son, hay một lọ dưỡng thể an toàn made by "tui" trở nên dễ dàng hơn. Các bước tiến hành được hướng dẫn rõ ràng, hình ảnh đẹp. Ngoài ra, tác giả còn hướng dẫn cách đọc thành phần có trong các loại mỹ phẩm hand made nói riêng và mỹ phẩm công nghiệp nói chung. Nhờ vậy, mà sau khi đọc xong quyển sách này, mình đã nâng cao nhận thức hơn khi lựa chọn mỹ phẩm.
Còn việc tự tay làm son môi cho bản thân thì mình xin hẹn một dịp khác, vì chưa có điều kiện sắm sửa nguyên liệu và dụng cụ. :D
4
163539
2015-09-03 22:41:58
--------------------------
287231
5888
Mình thấy phần hình thức của sách rất đẹp. Nội dung rất hay. Sách được chia bố cục rất hợp lý, chia ra các phần như son,nước hoa,... Những phần chia sẻ rất chân thành và đủ ý. Đặc biệt nhất là phần hình ảnh được chau truốt rất cẩn thận, tạo cảm hứng cho các cô gái để tự làm những món đồ mỹ phẩm của mình. Công thức rất đảm bảo cho sức khỏe. Cảm giác tự mình được làm ra những món mỹ phẩm thật là thích. Mình rất ưng ý khi mua quyển sách này ^^
5
416008
2015-09-02 14:30:51
--------------------------
279380
5888
Điều đầu tiên khi nhìn thấy cuốn sách này là yêu quá đi thôi, sách được trang trí vô cùng đẹp, từ trang bì tới từng trang mẫu rồi hướng dẫn đều có những hình ảnh, màu sắc ngộ nghĩnh, dễ thương. Đúng là chiều lòng các cô nàng yêu cái đẹp. Không những vậy sách còn thể hiện cái chất rất nghệ sĩ, rất am hiểu và rất tận tâm của 2 tác giả đối với mỹ phẩm handmade, họ thể hiện và truyền cảm hứng cho các cô gái vì mỹ phẩm handmade không chỉ là một thú vui mà còn là cả một nghệ thuật. Vả lại giữa những công thức ồ ạt trên mạng, thì cuốn sách này đưa ra những góc nhìn thật chuyên nghiệp giải đáp những thắc mắc cho các bạn gái muốn bắt đầu mày mò làm mỹ phẩm. Thật là một cuốn sách đầy tâm huyết! 
5
467685
2015-08-26 18:58:40
--------------------------
270585
5888
Quyển sách với hình ảnh đẹp bìa khá bắt mắt cầm trên tay thu hút được người xem hình ảnh minh họa dễ tưởng tượng. Tác giả đã dành khá nhiều tâm huyết cho những người đam mê mỹ phẩm và cái đẹp. Nội dung khá chi tiết có cả những lỗi hay mắc phải và công dụng của từng loại sản phẩm. Một quyển sách tuyệt vời!!
5
748665
2015-08-18 13:33:10
--------------------------
268503
5888
Cuốn sách Tự Làm Mỹ Phẩm  rất hay và rất hữu ích. với cuốn sách này em có thể yên tâm tự làm mỹ phẩm chăm sóc cho bản thân  như son, nước hoa, kem dưỡng, sữa dỡng. kem chống nắng hoàn toàn từ thiên nhiên, rất an toàn, rất tốt cho da. Ngoài ra em có được vốn kiến thức nền tảng để có thể mở rộng kinh doanh các sản phẩm mỹ phẩm handmade. Chân thành cảm ơn tác giả của cuốn sách và Tiki đã mang đến cho em 1 cuốn sách tuyệt vời và những ưu đãi đặc biệt khi mua sách! Nhân viên giao hàng rất đúng hẹn! Em rất yên tâm khi mua sách tại tiki khi không cần đặt cọc mà vẫn có nhân viên giao sách tận tay! 
5
750350
2015-08-16 15:44:10
--------------------------
257077
5888
Mình mua sách này mở ra khá thích vì có hình chụp, cách hướng dẫn đơn giản, ngắn gọn. Nguyên liệu dễ mua, dễ sử dụng. Hơi ít công thức nhưng mới bắt đầu làm như vậy là ok. Mình thấy được tâm huyết của 2 bạn viết sách, chia sẻ rất chân thực không văn hoa mỹ từ, cảm nhận đầu tiên là rất thích cuốn sách này, có điều mình nghĩ không nên tiếp xúc hóa chất quá nhiều sợ ảnh hưởng sức khỏe sau này. Có điều các chất đều thiên nhiên nên chắc không sao.
5
737096
2015-08-07 13:32:50
--------------------------
252774
5888
"Tự làm mỹ phẩm" sản phẩm bổ trợ những kiến thức cơ bản về mỹ phẩm handmade và cách làm mỹ phẩm handmade an toàn, hình ảnh của sách vô cùng đẹp và chân thực. Điều đặc biệt, sách trên tiki lại được giảm đến 20% giá bìa rất rẻ so với giá trên thị trường. Cuốn sách như đã trở thành người bạn đồng hành trên cuộc hành trình theo đuổi niềm đam mê của mình. Cảm ơn tiki đã mang đến cho mình một "kho báu" này! Mong rằng không lâu nữa tiki sẽ cho ra đời thêm nhiều sách hay hơn nữa.
5
291306
2015-08-03 22:05:24
--------------------------
250205
5888
Lúc trước mình đi nhà sách với vợ cô ấy đã thích cuốn sách này nhưng không mua vì cũ và đã tiếc mãi. Mình đã lên mạng kiếm mua cho cô ấy vô tình thấy tiki giảm giá tốt và khuyến mãi thêm phiếu mua hàng 100k + lúc đó mình còn được giảm 10% thêm nữa, thấy rất đáng đồng tiền he he. Và vui nhất vợ mình rất thích, các chàng trai hãy tặng cho cô gái của mình quyển sách nhỏ này rất dễ thương và thú vị. Mình cám ơn tiki mang đến một kênh mua sắm mới cho mình rất vui :)
5
728931
2015-08-01 11:30:56
--------------------------
247985
5888
Chưa kể đến nội dung, nhưng khi cầm lên quyển sách này đập vào mắt bạn trước nhất chính là những hình ảnh vô cùng quyến rũ, bắt mắt và xinh động với đủ màu sắc. Điều đó thu hút cho mỗi con người dù ai có khô khan đến mấy cũng phải động lòng mà kiên nhẫn vừa đọc vừa nghiên cứu. Không những tác giả chia sẽ những công thức của chính bản thân mình mà các chị ấy còn dặn dò chúng ta nhiều lưu ý họ từng thất bại. Tất cả giống như xuất phát từ sự nhiệt tình và tấm lòng. Đối với tôi rất hữu dụng, còn với các bạn thì sao tôi không biết. Nhưng đang rất mong chờ tác giả xuất bản thêm mấy cuốn như thế này lắm, vì có người khác chỉ bảo còn hơn ngồi mò mà kết quả tổn thất khá lớn nữa!
5
722523
2015-07-30 17:04:57
--------------------------
247235
5888
nhận được sách mà cứ hồi hộp bên trong sẽ như thế nào, đã đọc thử vài trang trên tiki, khá tò mò các công thức hướng dẫn làm mỹ phẩm handmade. nội dung khá chi tiết, có nêu rõ nguyên nhân thất bại khi làm son, công dụng của các nguyên liệu như dầu dừa, dầu hướng dương, dầu jojoba , sáp ong,sáp đậu nành, tinh dầu, hương liệu... cách phân biệt tinh dầu và dầu. có hướng dẫn làm những loại tinh dầu đơn giản dễ làm tại nhà. các mỹ phẩm rất có ích cho việc làm đẹp, nếu như bạn đang lăn tăn suy nghĩ và chất lượng mỹ phẩm trên thị trường thì mỹ phẩm handmade là một lưu ý nhỏ bạn nên biết tới về làm đẹp an toàn!
5
421546
2015-07-30 10:02:13
--------------------------
245318
5888
Tôi mua cuốn sách này ban đầu để tặng cho em gái, tuy nhiên sau khi xem qua thì bản thân tôi cũng trở nên hứng thú với những sản phẩm mà tác giả cuốn sách chia sẻ và hướng dẫn. Tự mình làm được một sản phẩm làm đẹp đúng ý bản thân từ công dụng tới màu sắc thật là hấp dẫn. Hơn nữa, tác giả còn liệt kê chi tiết công dụng, lưu ý của từng loại thành phần rất hữu ích. Mặc dù chưa có cơ hội thử nghiệm những công thức này nhưng tôi nghĩ trong tương lai nếu có cơ hội nhất định tôi sẽ thử. 
5
717208
2015-07-28 21:00:55
--------------------------
241584
5888
Trong một lần tình cờ vì tính chất công việc cần tìm hiểu về một vài công thức làm son môi handmade mà tôi đã tìm mua quyển sách này về tham khảo. Kết quả tuyệt vời hơn tôi tưởng, với cuốn sách này các cô gái đừng lo lắng bản thân không khéo léo không thể làm được. Các công thức được chia sẻ một cách tỉ mỉ và dễ hiểu ngoài ra còn những phần chia sẻ lí thú như lời thề của người làm mĩ phẩm, bắt bệnh của sản phẩm... Mọi thứ cơ bản về làm đẹp đã được chia sẻ một cách đầy đủ dễ học dễ thực hiện. Tôi đã thử một vài công thức và kết quả mang lại làm bản thân rất hài lòng. Chân thành cảm ơn 2 tác giả. 
4
211773
2015-07-25 16:49:16
--------------------------
238203
5888
 Mình mua sách này sau khi xem clip của chị Thư Đỗ.
Mình rất kết bìa sách design rất đẹp. Hơn thế là nội dung bên trong lại càng đẹp vs hấp dẫn hơn.
Văn phong của chị Thư rất thân thiện tạo cảm giác thoải mái. Cách chia các phần khá khoa học vd như phần về son sẽ chia làm 3 : Son dưỡng. Son màu vs son bóng... Hướng các bước làm tỉ mỉ. Ảnh minh họa đẹp như mơ. Tuyệt nhất là sau mỗi phần. sách còn có thêm phần Các lỗi thường gặp và cách khắc phục nữa. Điều đó giúp ích rất nhiều cho những ma mới như mình.
" Tự làm mỹ phẩm " đã và đang là 1 trong những quyển sách gối đầu giường của mình. Mình cực kỳ hài lòng khi mua nó ^^
5
256409
2015-07-23 01:14:09
--------------------------
234976
5888
Đến tuổi mười tám đôi mươi, có nhu cầu sử dụng mỹ phẩm sạch, mà điều kiện tài chính hạn hẹp nên mình quyết định tìm đến mỹ phẩm handmade. Cũng khá là thích mày mò nên mình tìm mua quyển sách này. Quyển sách đẹp, bắt mắt, trình bày hấp dẫn, chất lượng tốt. Có rất nhiều công thức hay ho và gần như tóm gọn hầu hết những mỹ phẩm có thể làm handmade. Làm mỹ phẩm cũng giống như làm bánh vậy, cần sự tỉ mỉ và chuẩn xác và mình cũng rất hưởng thụ quá trình làm mỹ phẩm đến khi được sử dụng chúng. Quyển sách này rất phù hợp cho những người muốn dùng mỹ phẩm hoàn toàn sạch và an toàn!
4
84382
2015-07-20 18:30:22
--------------------------
227842
5888
Làm mỹ phẩm tại nhà. Tự chính tay mình tạo ra những thỏi son với nhiều hương vị. màu sắc. những hũ kem dưỡng ẩm cho da vào mùa đông. nước hoa theo phong cách riêng của mình.
Quyển sách mới đầu đọc hơi khó hiểu vì nó giống như là phòng thí nghiệm. nhưng mini.
Giới thiệu các dụng cụ. vật liệu. nguyên liệu. có thể thấy những nguyên liệu tưởng chừng chỉ làm báng. nấu ăn. vậy mà có thể biến thành mỹ phẩm.
Điều mình thấy ấn tượng là Lời thề của người làm mỹ phẩm. phải an toàn. đảm bảo như thế chứ. 
4
105577
2015-07-14 15:43:43
--------------------------
223893
5888
Mình là dân nghiện mỹ phẩm, mua rất nhiều mỹ phẩm và dùng thì cũng rất nhiều luôn, tuy vậy mình cũng rất ngại dùng nhiều hóa chất nên đôi khi cũng lên mạng tìm tòi làm một chút mỹ phẩm handmade. Khi thấy cô nàng này mình quyết định rước nàng về dinh luôn. Một cuốn sách rất tiện lợi cho việc làm mỹ phẩm, cũng giới thiệu hết sức rõ ràng các bước cơ bản để làm mỹ phẩm. Nội dung khá " thâm hậu ", nhiều loại son dễ làm, mà lại không có nhiều hóa chất, dùng rất là yên tâm nhé. Cô nàng được trưng diện mạo cũng rất là đẹp luôn nhé. Mình tặng hẳn năm sao cho nàng này nhé.
5
471002
2015-07-07 19:31:02
--------------------------
223569
5888
Mình rất thích làm đồ hanmade, nhất là mỹ phẩm,và mình đã rinh em này về, một cuốn sách đã đốn ngã mình ngay từ cái nhìn đầu tiên ^^ Bìa sách rất đẹp, chất liệu giấy miễn chê. Nội dung sách  với nhiều công thức được trình bày tỉ mỉ, kèm theo là những hình ảnh minh họa đầy màu sắc, giúp người đọc dễ hiểu và cảm thấy thật hứng thú. Những công thức thật sự có ích,  mà đa số đều là những công thức dễ làm, không quá khó để tạo ra những loại mỹ phẩm handmade vừa xinh, vừa yên tâm về chất lượng. điểm trừ: hơi ít công thức về làm mỹ phẩm , chỉ có làm son, làm kem dưỡng, nhưng nhiêu đó cũng đủ để mình táy máy học theo. một cuốn sách hay và đáng giá. 
5
485995
2015-07-07 11:43:38
--------------------------
222417
5888
Tôi là người hay bị dị ứng, vậy nên, khi dùng bất cứ sản phẩm nào, tôi cũng lo sợ mặt mình sẽ sưng vù lên, kể cả với các loại hay dùng cũng không làm tôi bớt lo lắng. Thế nhưng, với quyển sách này, tôi có thể tự tay làm cho mình các loại mỹ phẩm và không còn lo lắng gì nữa. Cảm ơn các tác giả đã cho ra đời một cuốn sách hay và bổ ích như vậy
5
529182
2015-07-05 15:12:54
--------------------------
220730
5888
Một cuốn sách đã đốn ngã mình ngay từ cái nhìn đầu tiên ^^ Bìa sách rất đẹp, chất liệu giấy miễn chê. Nội dung sách phong phú với nhiều công thức được trình bày tỉ mỉ, kèm theo là những hình ảnh minh họa đầy màu sắc, giúp người đọc dễ hiểu và cảm thấy thật hứng thú. Những công thức thật sự có ích, vừa làm theo mà mình vừa thấy thú vị vô cùng, mà đa số đều là những công thức dễ làm, không quá khó để tạo ra những loại mỹ phẩm handmade vừa xinh xẻo, vừa yên tâm về chất lượng.
5
411723
2015-07-02 22:13:36
--------------------------
207694
5888
mình chưa làm cái gì trong sách bảo hết nhưng khi đọc qua cuốn sách thì thấy nó rất hay. hay ở những lời khuyên hài hước của tác giả và các phương pháp tác giả đưa ra cho người đọc nhìn nhận và thực hiện theo công thức tác giả đưa ra sao cho phù hợp với dòng nguyên liệu mà các bạn đang có hay dễ tìm. thấy cuốn sách còn có nói đến những bí quyết xài kem dưỡng da hay các loại sữa rửa mặt và tác dụng của các loại sữa dê ,... cho làn da. và cách nhìn nhận làn da để bổ sung những gì nữa . mình thấy thú vị và rút ra rất nhiều kinh nghiệm.
5
642439
2015-06-12 22:10:31
--------------------------
194337
5888
Mỹ phẩm là một loại sản phẩm bảo vệ da và làm đẹp không thể thiếu đối với các bạn gái thời hiện đại.Trong thị trường hiện nay có rất nhiều loại mỹ phẩm không đảm bảo chất lượng.Cuốn sách này sẽ giúp bạn có những công thức làm mỹ phẩm tuyệt vời.Ngoài ra nó còn có những lời khuyên hữu ích cho những bạn gái về những bí quyết làm đẹp thật hữu ích.Còn gì tuyệt hơn khi tự mình làm ra những sản phẩm có chất lượng để làm đẹp cho chính mình và đôi khi để tặng nó cho bạn bè và người thân.Hình ảnh ngoài hay trong sản phẩm cũng khá bắt mắt và đẹp.
5
536745
2015-05-10 10:17:12
--------------------------
181210
5888
Cuốn sách mang đến cho mình những kiến mới về mỹ phẩm sử dụng hàng ngày nhưng cũng không kém phần chuyên nghiệp, mang nhiều thông tin bổ ích cùng các thông điệp nho nhỏ dễ thương gửi đến các bạn gái về bí quyết làm đẹp, lời khuyên hóm hỉnh qua các trải nghiệm của tác giả. Quan trọng hơn là các bí quyết ấy do chính tay tụi mình làm ra, thật là "tuyệt cú mèo" đúng không nào ^^. Hình ảnh minh họa trong sách cũng góp phần làm mình thấy yêu đời hơn, nhất là cầm sách lên sau 1 ngày làm việc học tập căng thẳng, hihi LÀ CON GÁI THẬT TUYỆT đấy :)
5
430666
2015-04-11 12:02:50
--------------------------
137231
5888
Như tiêu đề phía trên , mình không có ý định tự làm Mỹ phẩm . Nhưng xem cách Mỹ phẩm được làm ra thoả mãn trí tò mò của mình : tại sao kem dưỡng trông lại giống kem ăn , sữa chua nè , tại sao son lại có dạng khô , dạng nước .....Cuốn sách còn giúp chúng mình xem cách ghi thành phần hoá học trên mỗi cây son , hũ dưỡng da , đặc tính của các thành phần . Hiểu biết không bao giờ là phí cả.  Cuốn sách này mang lại cho mình điều đó .
5
124682
2014-11-25 12:27:33
--------------------------
136191
5888
Quyển sách này thiết kế khá đẹp và bắt mắt,chất giấy khá dày và hơi bóng không hôi và rất dễ đọc,nhưng giấy này thì sách sẽ hơi nặng.Có chỉ dẫn làm son,nước hoa và các loại dưỡng khác,cũng khá chi tiết và nhiều phần chú ý cũng như công dụng của thành phần để bạn tùy thích tạo ra sản phẩm riêng.Hình minh họa rất đẹp,dễ hiểu và bao quát nhưng chưa cụ thể khác.Nói chung khá ổn nhưng nếu sách có thêm hướng dẫn làm phần và các mỹ phẩm khác sẽ tiện hơn rất nhiều.Dù sao thì cuốn sách này cũng khá hữu dụng dù thành phần hơi khó kiếm.
4
383783
2014-11-19 17:26:56
--------------------------
135472
5888
Tự nhận mình khá vụng mấy vụ chế biến này nhưng mình khoái tìm hiểu thông tin về handmade lắm. Cám ơn hai tác giả Anh Thư - Thu Giang và ekip Nhã Nam đã phát hành một ấn bản nói về mỹ phẩm handmade tuyệt như vậy. Điểm cộng đầu tiên là đơn vị phát hành Nhã Nam làm mình yên tâm và trình bày, in ấn đẹp đẽ, bắt mắt nhìn muốn mua ngay. Đến khi đọc, mình cảm nhận được sự nghiêm túc, chân thành qua từng câu chữ, hình ảnh. Mục lời thề đọc xong thấy dễ thương dì đâu. Hai chị đã truyền lửa thành công ít nhất vào em rồi đấy ! Tuần sau em sẽ mua sắm vật liệu để làm mẻ son dưỡng đầu tiên trong đời đây.
5
45199
2014-11-15 19:54:04
--------------------------
130321
5888
mình đã đọc qua cuốn này,hình được chọn lọc nên rất đẹp và nghệ thuật.Có rất nhiều điều mới chưa từng nghe qua đối với người làm mỹ phẩm như mình.Giọng văn của chị Thư rất hay,chị tưởng tượng quá trình làm mỹ phẩm cứ như 1 người nghệ sĩ vậy,rất ngộ nghĩnh.Đối với người có đam mê như mình cầm nó trên tay quả thực là sung sướng.Mình muốn khuyên các bạn nên mua dù là có ý định làm mỹ phẩm hay không,bạn sẽ hiểu và thu nhập được kha khá những điều mới mẻ chưa biết về mỹ phẩm đấy.
5
449111
2014-10-16 21:40:09
--------------------------
