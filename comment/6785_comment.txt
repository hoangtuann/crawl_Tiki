222344
6785
Cuốn sách những hiểu biết về bệnh loãng xương là một cuốn sách có nội dung hữu ích đối với những người lớn tuổi hay những bạn trẻ muốn chăm lo nhiều hơn đối với sức khỏe của ba mẹ như mình. Cuốn sách cung cấp cho chúng ta một cái nhìn tổng quát về cấu tạo cũng như vai trò của xương đối với cơ thể, những nguyên nhân gây ra bệnh loãng xương cũng như cách điều trị và phòng ngừa bệnh này. Thiết kế bìa sách khá đẹp, nội dung trình bày logic, rõ ràng.
5
572238
2015-07-05 13:05:11
--------------------------
