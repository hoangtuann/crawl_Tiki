480971
4691
Truyện có màu sắc và nội dung rất dễ thương. Chỉ từ một cuộn dây mà bé sẽ tưởng tượng ra rất nhiều hình thù sinh động. Phải nói là bộ truyện Khám phá thế giới rất hay và phù hợp với các bé. Mình đọc cho bé rất thích, trước khi ngủ lại đòi mẹ đọc cho nghe một lượt. 
Giấy in khá dày dặn, mình mua lúc giảm giá chỉ còn 8k/quyển. 
Các mẹ có con nhỏ tầm tuổi mầm non nên mua về cho bé. 
Mẹ nào mua lần đầu tại tiki nhớ nhập thêm mã TF-HXF4C để được giảm thêm 5% nhé!
5
1564031
2016-08-22 14:09:22
--------------------------
307590
4691
Truyện “Những tưởng tượng mềm mại” là 1 trong số 12 cuốn trong bộ sách “Khám phá thế giới”. Đợt này mình chỉ đặt được trên Tiki 3 cuốn thôi vì các cuốn còn lại hết hàng rồi. Truyện có tổng cộng 20 trang, dành cho các bé từ 2 đến 6 tuổi, nhưng mình nghĩ các bé từ 1 tuổi trở lên cũng có thể xem được. Truyện kể về một cậu bé, với sợi len màu xanh, cậu đã tưởng tượng ra nhiều đồ vật làm từ sợi len này.Đúng là những tưởng tượng của trẻ con, rất ngộ nghĩnh, dễ thương.
3
15022
2015-09-18 10:25:26
--------------------------
