372924
6375
Tôi đã sưu tầm gần đủ bộ Truyện Cổ Đông Nam Á. Chỉ thiếu mỗi quyển truyện cổ Thái Lan và truyện cổ Mianma. Cám ơn tác giả phó giáo sư tiến sĩ Ngô Văn Doanh đã công phu dành nhiều tâm huyết công sức, biên tập sưu tầm những mẩu chuyện về các nước Đông Nam Á đầy màu sắc này. Đem sản phẩm đến gần người đọc hơn, thông quá đó có sự hiểu biết đa dạng, so sánh các nền văn hóa của các nước lân cận. Bây giờ có quá nhiều tác phẩm của phương Tây mà chúng ta quên đi mất những giá trị gần gũi xung quanh cũng hay ho không kém !
4
854660
2016-01-22 15:48:46
--------------------------
344017
6375
Truyện cổ Lào nằm trong bộ truyện cổ Đông Nam Á là một sản phẩm truyền tải hết sức sống động thông điệp về cuộc sống và tính cách con người, thiên nhiên, cảnh trí, thông qua đó đưa đến cho người đọc những nhận định khái quát về bối cảnh và mục đích ra đời của tác phẩm. Nhà văn dựa vào đặc trưng lịch sử, truyền thống và văn hoá tín ngưỡng để làm nổi bật chất hào hùng, sống động của tác phẩm, thông qua đó đưa đến những nhận định mới mẻ và chân thực hơn về đới sống con người.
4
429966
2015-11-27 16:38:59
--------------------------
