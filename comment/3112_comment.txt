466403
3112
Mình đặt sách này cùng với một số sách nữa, Ban đầu cứ nghĩ sách này chắc dành cho các bé nhưng không chỉ các bé mà cho cả người lớn chúng mình nhé. Các nhân vật Bạch tuyết, Robin, Tarzan, Sherlock Holmes, Chuẩn úy Ripley ...là nhưng câu chuyện đã từng đọc ...nhưng thực sự thì chưa từng nghĩ các nhân vật này hình thành như thế nào? quyển sách này sẽ trả lời cho bạn...từ bối cảnh, đến lịch sử hình thành với các ảnh minh họa dí dỏm rất đáng để đọc. Tiki giao hàng nhanh, đặt hôm trước, ngày sau đã có rồi. Cảm ơn Tiki
5
640160
2016-07-02 10:25:00
--------------------------
456874
3112
Ấn tượng đầu tiên của mình khi cầm trên tay cuốn “Bách Khoa Thư những nhân vật kinh điển” là sự chắc chắn của cuốn sách. Cuốn sách được đóng bìa cứng, từng trang sách khổ to được in màu rất đẹp, chất lượng giấy rất tốt: giấy in dày dặn, giấy màu vàng nhạt, ít lóa, không hại mắt khi nhìn lâu. Hình ảnh Super man ở trang bìa rất nổi bật. Cuốn sách được trình bày với văn phong dí dỏm – pha chút hài hước; hình minh họa trong cuốn sách thì … đem lại tiếng cười rất nhiều cho bạn đọc. Mình tin đây sẽ là một cuốn sách khiến người đọc “cười không nhặt được mồn lên” :D
5
301718
2016-06-23 20:40:15
--------------------------
