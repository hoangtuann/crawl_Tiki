394320
10794
Điểm cộng: bìa đẹp, nội dung hay, kết thúc có hậu
Điểm trừ: sách có nhiều lỗi chính tả và lỗi đánh máy
Câu chuyện thu hút người xem bởi hai nhân vật chính tưởng chừng như rất khác biệt, rất khó hòa hợp cả về tính cách lẫn tướng mạo, cả hai đều tưởng như không thể nào là một đôi hoàn hảo được. Vậy mà, vạn sự tùy duyên, sau những biến cố và rắc rối mà cả hai gây ra cho nhau, Joe và Gabrielle lại tìm đến nhau vì cả hai đều tin rằng họ là một nửa của nhau.
4
669001
2016-03-10 10:27:35
--------------------------
381991
10794
Nội dung hài hước, mang tính giải trí cao nên sau khi đọc xong thì cứ thế trôi tuột đi hết chứ không đọng lại được gì. Nhân vật nữ chính có vẻ quá hiền lành, đến mức độ bị một người lừa bao nhiêu năm trời mà không hay biết. Nhưng cái cách tác giả xử lý cái chỗ nhân vật nữ không bị coi là đồng phạm thì đơn giản quá, không thuyết phục được những người đọc khó tính như tôi đây. Bìa sách dễ thương, chất lượng sách in rõ ràng, không bị lem sang trang sau như nhiều sách khác của chibooks mà tôi đã mua.
3
350180
2016-02-18 13:31:05
--------------------------
379424
10794
Câu truyện chính là mối tình câm lạnh nhạt , thờ ơ , cuốn hút không khiến người xem bỏ quên lãng mạn vương vấn người đi ngang qua lời văn day dứt , khó tả cũng như phần phân bố tách rời những thói quen ngược lại với người xem không chỉ là việc tả đại khái , qua loa những thầm thì mờ ảo dành cho người yêu sâu sắc , vượt xa không kém các câu truyện hiện đại nào , phần kết không làm ngạc nhiên tính khí mạnh mẽ đó của người phương tây .
4
402468
2016-02-11 02:33:55
--------------------------
374002
10794
Đây là một trong những câu chuyện của Rachel Gibson mà mình thích nhất, nhân vật chính rất dễ thương, cách Joe bối rối khi Gabrielle òa vào đời anh thật là đáng yêu hết sức còn Gabrielle cô không bao giờ hết gây bất ngờ cho Joe lẫn độc giả, cô mời anh ở lại ăn cơm và cuối cùng anh phải nấu nướng toàn bộ, cô vẽ tranh khỏa thân của anh với cả những chi tiết nhạy cảm và khi bị phát hiện không hề tỏ ra xấu hổ về chuyện đó. Các nhân vật phụ như gia đình của Joe cũng ấn tượng và dễ thương.
4
306081
2016-01-25 11:09:37
--------------------------
258724
10794
Thật ra mình muốn cho 3 sao rưỡi...mình cảm thấy câu chuyện chưa sâu lắm, tình tiết cười cũng chưa đã nữa, với lại nội dung khá dài dòng, nhất là những đoạn 2 nhân vật chính thân mật với nhau làm mình phát bực vì sự dài dòng của nó, nên nhiều lúc mình bỏ qua tới 1, 2 trang để đọc phần tiếp theo...Nhìn chung câu chuyện không quá mới lạ nhưng cũng khá hấp dẫn và lôi cuốn, bằng chứng là mình đọc 1 mạch hết 1 cuốn trong 1 buổi sáng! Cảm ơn tiki đả giảm cho mình 50%, mình đang mong chờ nhiều đợt giảm giá từ tiki để 1 sinh viên nghèo như mình có cơ hội ôm về thật nhiều tác phẩm văn học hay và ý nghĩa.
4
477940
2015-08-08 20:04:59
--------------------------
240000
10794
Về phần hình thức, bìa sách đơn giản mà đẹp, giấy tốt, chữ in cũng rõ ràng. Về phần nội dung, đọc hơi thất vọng, phần mở đầu cuốn hút nhưng càng về sau thấy tình tiết càng hụt và không có gì nổi bật lên cả, cốt truyện cứ đều đều như vậy dẫn đến nhàm chán khiến mình nhiều lần muốn bỏ dở giửa chừng. Vụ án bán đầu đưa ra khá là hấp dẫn nhưng càng về sau càng thấy không gì hấp dẫn nữa mà thay vào đó lại tập trung quá nhiều vào màn tình cảm của nam nữ chính. Nếu có sự cân bằng giữa vụ án và tình cảm thì đây sẽ là cuốn sách khá hay.
3
471112
2015-07-24 10:52:12
--------------------------
153980
10794
Câu chuyện tình yêu giữa Joe và Gabrielle có phần hấp dẫn khi 2 nhân vật được đặt trong một tình huống rất trớ trêu :) Mình thích cách sắp xếp này của Rachel Gibson, nó có chút gì đó rất thú vị và hài hước, tạo được nhiều kịch tính và tiếng cười nữa :)
Câu chuyện diễn ra khi chàng cảnh sát chìm Joe vướn vào rắc rối tình yêu với cô gái Gabrielle được coi là nghi phạm của một vụ án. Một người nghĩ rằng người kia là nghi phạm còn một người lại nghĩ rằng người kia là tên biến thái chuyên rình mò phụ nữ :) Từ đây mà biết bao tình huống bất ngờ và buồn cười diễn ra. 
Một chuyện tình diễn ra một cách tự nhiên với nhiều tình huống trớ trêu này đã làm mình rung động và thích thú! Trong chuyện tình này mình thích cách miêu tả nội tâm nhân vật, họ cứ dằn vặt và chối bỏ tình yêu nhưng cuối cùng vẫn không thắng được sức mạnh của nó :)
Thiết kế bìa sách khá đơn giản nhưng lại đẹp một cách tinh tế :D
4
303773
2015-01-28 00:00:21
--------------------------
89528
10794
Với một phong cách cũ, Rachel Gibson lại thổi vào nhân vật của mình những câu nói hài hước và tình huống cực kỳ nhạy cảm. Một anh chàng cảm sát chìm phải lòng một cô nàng vốn tin anh là một kẻ biến thái theo đuổi cô, cô từng là một nghi phạm của anh và giờ đây, anh phải làm sao kiểm soát được tình cảm của mình với một cô nàng xinh đep, thông minh và hài hước thế? Ở giữa họ là những tính cách trái ngược nhưng hút lẫn nhau, họ gây gỗ để rồi cuốn hút vào nhau nhiều hơn là họ nghĩ. 
Một câu nếu so với những câu chuyện trước của Rachel Gibson thì hoàn toàn không mới, thậm chí có phần nhạt nhẽo, vì tính cách và tuyến nhân vật do cô xây dựng đều từa tựa như nhau. Nhưng nếu để giải trí và lần đầu tiên biết đến những tác phẩm của Rachel Gibson thì đây là một lựa chọn tuyệt vời.
*Một số nhận xét khác:
- Bìa cuốn sách khá đơn giản nhưng thể hiện được chủ đề được nhắc đến trong câu chuyện.
- Chất lượng giấy và in ấn rất tốt
- Phong cách dịch truyện rất gần gũi, mượt và nắm được cái hài hước của nhân vật trong truyện
Mình cho cuốn sách này 7/10 điểm

3
15919
2013-08-02 15:11:06
--------------------------
81703
10794
Câu chuyện vẫn viết theo lối mòn thông thường của Rachel Gibson, không có gì nổi bật lắm trong cốt truyện.
Tuy nhiên, điều mình thấy khá lôi cuốn trong cuốn này là sự hài hước được tác giả dẫn dắt ngay từ đầu truyện với cảnh đuổi bắt giữa Joe và Gabrielle để rồi sau đó từ những sự giả vờ "như thật" dẫn tới những tình cảm nảy sinh. 
Nếu so với những tác phẩm lãng mạn hiện đại thì văn phong của Rachel Gibson không có nhiều đổi mới và không mấy nổi bật, tuy nhiên cũng không có sự giảm sút trong mỗi tác phẩm, không có tác phẩm nào quá hay hoặc quá dở. 
Có một điều đáng nói là mình không thích chất lượng và mẫu mã bìa in của Chibook, mỗi tác phẩm thường in với cùng 1 kiểu, cùng loại bìa, không có mấy điểm nhấn để tạo nên sự thu hút. 
5
125574
2013-06-18 10:32:55
--------------------------
75793
10794
Sau khi đọc xong cuốn truyện rồi đọc nhận xét của mọi người thì mình không biết có phải mình quá khó tính hay không, nhưng mình hơi hối hận khi mua cuốn này
Thứ nhất, cốt truyện thì cũng khá thú vị- lí do mà mình quyết định đặt mua, tuy nhiên khi đọc thì nội dung lại rườm rà, miên man, dẫn đến nhàm chán. 
Thứ hai, trong khi nên tập trung vào dẫn dắt câu chuyện và tình tiết khám phá vụ án thì chuyện lại quá đi sâu vào các chi tiết nhạy cảm và cảnh gần gũi của hai nhân vật dường như là để câu giờ làm cho mình thấy cực kì phản cảm
Thứ ba, một số đoạn còn dịch quá tối nghĩa, thành ra rốt cục cũng chẳng hiểu mục đích mà đoạn đó muốn nhắm đến.
Phải, khá là thất vọng.
2
101433
2013-05-19 21:05:08
--------------------------
57546
10794
Cuốn sách hài hước nhất của R.Gibson, tôi bị ấn tượng hẳn với  "Hẳn Là Yêu" ngay từ cái bìa sách đơn giản nhưng rất dễ thương và khi đọc nội dung tác phẩm tôi lại càng bị cuốn hút nhiều hơn. Joe & Gabriel gặp nhau trong hoàn cảnh hết sức trớ trêu khi cô là nghi phạm còn anh lại là cảnh sát chìm đang mang nhiệm vụ theo dõi cô nhưng có lẽ đây chính là duyên số của họ, từ những tình huống dở khóc dở cười khi cô dùng bình xịt tóc để tấn công anh cho đến buộc phải chấp nhận anh làm bạn trai giả của mình đã khiến câu chuyện trở nên vô cùng vui nhộn nhưng cũng rất tinh tế và lãng mạn. Đôi lúc cả hai cũng rất vụng về trong tình yêu bởi họ cứ chối bỏ cảm xúc của trái tim mình chỉ vì sợ phải đối mặt với nó nhưng định mệnh là định mệnh, Joe & Gabriel chỉ thật sự hạnh phúc khi được ở bên nhau, họ đã nhận ra được điều đó, đã chiến thắng những trở ngại, khó khăn của chính mình để tìm về bên nhau.
4
41370
2013-01-30 20:55:17
--------------------------
17796
10794
Tác phẩm này của Rachel Gibson - Hẳn là yêu mang một màu sắc mới, có phần tinh nghịch và hài hước hơn những tác phẩm trước đây của bà. Joe Shanahan và Gabrielle Breedlove - hai con người trái ngược nhau hoàn toàn về tính cách - anh phóng túng, cô thanh cao, về sở thích - cô tin vào nhân quả, anh không hề, về hoàn cảnh - cô là nghi phạm, anh là cảnh sát... Cứ thế, vậy mà họ bị hút vào nhau, trước là vì bắt buộc, sau là vì đam mê. Tình yêu nở hoa giữa hai người họ. Vậy mà chỉ mình cô cảm nhận được. Rồi sóng gió đến. Cả hai cùng phải đấu tranh để giành lại tình yêu đích thực của đời mình.
Điều tôi thích nhất ở cuốn tiểu thuyết này là cái cách cô đấu tranh để vươn lên, cái cách anh đấu tranh tư tưởng để chứng minh tình yêu anh dành cho cô. Họ - những người trưởng thành về cơ thể, về nhân cách nhưng chưa trưởng thành trong tình yêu chút nào. Người đọc sẽ được hòa mình vào những trang sách, biến thành Garielle hay Joe, cùng họ đi hết khoảng thời gian quan trọng nhất đời họ, đi qua những khó khăn, vươn lên những chướng ngại để cuối cùng được đến với cái đích xứng đáng với bao công sức đã bỏ ra.
Một câu chuyện đáng để đọc. Đọc để được thả hồn, thoát ra cái thế giới hiện tại khắc nghiệt để hòa mình, để quyện lại với tâm hồn nhân vật và hạnh phúc với những con chữ.
Một tác phẩm được gầy dựng nên bởi ngòi bút tinh sảo của Rachel Gibson
4
19049
2012-01-17 09:09:55
--------------------------
13084
10794
    Không khác với những tác phẩm trước đó, một lần nữa Rachel Gibson lại thổi vào câu chuyện của mình những tình tiết hài hước cũng như những tình huống hiếm có trong tình yêu.
   Lãng mạn nhưng không sáo mòn bởi vì tình tiết câu chuyện được diễn biến một cách hợp lý cũng như những chi tiết thú vị đôi khi khiến độc giả không nén được phải bật cười.
   Ấn tượng nhất có lẽ phải nói đến kết cấu nội dung, một chuyện tình đặc biệt giữa đối tượng tình nghi cực kỳ quyến rũ và tay thám tử khinh khỉnh có cơ bắp căng cứng đầy hấp dẫn. Có lẽ cũng chính bởi vì họ quá đối lập nhau như cực âm và cực dương trái dấu nên lực hút lại càng mãnh liệt, khó chối bỏ.
   Thích nhất là sự thẳng thắn của Gabrielle Breedlove khi bày tỏ tình yêu của mình hay mạnh mẽ tìm cách vượt qua khi bị người mình yêu từ chối.
   Chính cách xây dựng tuyến nhân vật sáng tạo cũng như dẫn dắt các tình huống một cách độc đáo đã một lần nữa làm nên thành công cho Rachel Gibson, truyện của cô đi vào lòng người một cách tự nhiên mà không báo trước.
5
9831
2011-10-19 01:19:10
--------------------------
11525
10794
theo tôi nghĩ, cuốn sách này nóng về mặt: về 2 nhân vật, về các tình tiết và cả về tình yêu... chàng theo chủ trương tự do, tình yêu của chàng đối với phái đẹp chỉ hoá ra lại dành trọn cho Sam - 1 con vẹt.... nàng theo chủ nghĩa hoà bình, tin vào số phận tử vi, thích những người đàn ông trí thức có khả năng "tử vì đạo"... họ va vào nhau như sao chổi va vào trái đất và tạo ra một vụ nổ banh trời với 2 cái thái độ đối nghịch nhau đến nỗi ngay từ đầu nhìn họ đã xác định mình chẳng ưa gì người kia... thế rồi thời gian họ bên nhau đã đưa tình yêu đến với họ lúc nào không biết, nhưng họ vẫn chối bỏ thừa nhận nó, chàng vẫn cố nghĩ nàng bị điên nên sẽ thật điên nếu chàng thích nàng và nàng thì nghĩ chàng quá lỗ mãng với một người "suýt chút nữa thành người ăn chay" như nàng... nhưng số phận là số phận, hành trình họ đến được với nhau là sự đan xen giữa những khoảnh khắc hài hước và những thước phim nóng bỏng, các bạn thử tìm hiểu thêm nhé...
5
6230
2011-09-20 22:35:45
--------------------------
6335
10794
Một chàng trai và một cô gái đến với nhau trong một hoàn cảnh đối lập, cuộc truy tìm bằng chứng buộc tội lại trở thành cuộc khám phá bằng chứng về tình yêu. Để khi hai người đều chìm trong sự quyến rũ của đối phương, cũng là hiểu một điều tưởng như đơn giản nhưng lại cực kỳ phức tạp và khó khăn đó là "hẳn là yêu". Đây chính là điểm thu hút của cuốn sách.
Một cuốn sách chứa đựng một câu chuyện thú vị. Hãy đến với "Hẳn là yêu".
5
6391
2011-06-16 12:04:28
--------------------------
5494
10794
" Hẳn là yêu" cho người đọc một cách nhìn đúng đắn và mới mẻ về tình yêu. Đọc chuyện đọc giả sẽ có một cảm nhận không lẫn vào đâu được, truyện có một cách nhìn khá lạ nhưng không kém phần lãng mạn do hai nhân vật chính của câu chuyện dẫn dắt.
Đọc xong, bạn sẽ thấy thêm yêu cuộc sống và hiểu đúng hơn về tình yêu. Truyện phù hợp với những ai có tâm hồn lãng mạn, muốn hiểu hơn về tình yêu con người trong cuộc sống hiện nay.
Chất lượng sách rất tốt, các bạn nhanh tìm đọc nhé.
5
1452
2011-06-02 10:42:55
--------------------------
5277
10794
- Câu chuyện về tình yêu của chàng cảnh sát chìm Joe Shanahan và cô nàng quyến rũ Gabrielle Breedlove thật khiến người đọc khó rời mắt khỏi trang sách bới những tình tiết hài hước, hấp dẫn và đầy thú vị. Đó là tình yêu, không thể lầm lẫn vào đâu được, không có lý do gì để ta yêu một người, đơn giản chỉ là yêu thì yêu thôi. 
- Bìa sách đẹp, hình vẽ sinh động. Chất lượng giấy in và in ấn tốt. Sách được đóng bìa chắc chắn, khó hư  hao.
- Một cuốn sách đáng để bạn đọc. Mua nhanh đi nhé.
5
3325
2011-05-30 11:21:55
--------------------------
