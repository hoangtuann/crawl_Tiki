455143
5111
Đặt mua quyển này vì muốn đọc thơ Hàn Mạc Tử thôi, mặc dù đã có một quyển và quyển mới này cũng có nhiều bài trùng trong quyển cũ nhưng mà thơ Hàn Mạc Tử thì quá hay nên mình muốn sưu tầm thêm nữa. Nào ngờ nhận được sách thì quá thích hình thức của nó luôn. Sách như một cuốn sổ tay chép thơ của tác giả hay một người yêu thơ nào đó vậy. Trang bìa thứ 2 để trống màu tím nhạt => đẹp, trang giấy in thơ bên trong có những sơ gỗ nhìn như sách cổ vậy => thích, in trên đó là những bài thơ tuyệt đỉnh của Hàn Mạc Tử thì còn gì bằng. Những bài thơ sắc sảo, những câu từ đắt giá nhất mà chỉ Hàn Mạc Tử mới có thể chắt lọc được thôi. Cảm ơn ông vì những áng thơ này, cảm ơn người thiết kế sách.  
5
605606
2016-06-22 13:33:01
--------------------------
397273
5111
Hàn Mặc Tử là một nhà thơ gây cho cá nhân mình rất nhiều sót thương, vài tuần trước  khi theo dõi chương trình chuyển động VTV24 về chuyên mục Việc Tử Tế, thấy những con người mắc căn bệnh Phong quái ác như Hàn Mặc Tử,, thật sự mình cảm thấy rất đau xót. mình đọc khá nhiều cuốn sách viết về cuộc đời nhà thơ, tập thơ bao trọn những cảm xúc của một nhà văn bạc mệnh, ông là thi nhân đoản thọ trong cuộc đời nhưng lại trường thọ trong thơ ca, tập thơ giúp mình hiểu sâu hơn về cuộc đời thi sĩ, và mình cũng rất ấn tượng với bề ngoài tập thơ, rất đẹp..
5
1014806
2016-03-14 18:24:24
--------------------------
370755
5111
Đánh giá chung: 5/5.

Bìa: 5/5 >>> Tuyệt đẹp, tranh vẽ có hồn. 
Nội dung: 5/5 >>> Nội dung tuyệt vời, nhìn khác tất cả cuốn còn lại trong tủ truyện cảu mình.

Ngày xưa đi học rất thích đọc truyện, thơ, và thích nhất thơ của ông Hàn Mạc Tử và thơ Nguyễn Bính. Hàn Mặc Tử có chất thở không lẫn vào đâu được. 
Mỗi lần đọc thơ thấy buồn man mác, có bài thơ mình đọc xong rùn mình, đau buồn cho Hàn Mặc Tử...
Mình trích mấy trang đầu của quyển sách: Thơ là sự ham muốn vô biên những nguồn khoái lạc trong trắng của một cõi trời cách biệt (...).Tôi làm thơ? Nghĩa là tôi nhấn một cung đàn, bấm một đường tơ, rung rinh một làn ánh sáng."_Hàn Mạc Tử 
5
937458
2016-01-18 13:05:52
--------------------------
354250
5111
Chàng thi sĩ trẻ đem vần thơ như có tia nắng ấm rọi vào phía của người lắng nghe và tưởng tượng ra chúng , dòng thơ như là cơn gió nhẹ buồn man mác đến với người thưởng thức như giai điệu cuộc đời , nỗi mong mỏi có người bầu bạn , yêu thương khi không thể có được thôi thúc chàng trai làm nên con đường bao quanh mình , vậy trong cơn bạo bệnh chàng thi sĩ chỉ làm được trút bỏ lòng mình xuống dưới trang giấy đầy nét mực chữ , thơ tập trích từ tập thơ nổi tiếng suốt thời phong kiến xưa .
4
402468
2015-12-17 15:57:04
--------------------------
312397
5111
Trong suốt 12 năm học văn phổ thông thì năm lớp 11 học về văn học trung đại 30 - 45 là hấp dẫn nhất. 
Hàn Mặc Tử là cái tên mà tôi cực kì yêu thích suốt từ những năm học phổ thông ấy. Nếu tôi không lầm thì đây là quyển tổng hợp nhiều bài thơ từ các tập thơ khác nhau của Hàn Mặc Tử. 
Hài lòng với chất lượng in ấn và phong cách thiết kế của Nhã Nam. Cách chọn tác phẩm đưa vào quyển này cũng giúp ta nhận ra cảm quan về cuộc đời của Hàn Mặc Tử từ lúc còn trẻ cho tới lúc sắp lâm chung. 


5
558163
2015-09-21 10:36:16
--------------------------
302929
5111
Lúc vừa nhận được sách Tiki giao, cầm trên tay cuốn Thơ Hàn Mặc Tử là mình đã biết mình không hề lựa chọn sai khi quyết định mua cuốn này. Bìa sách phủ một màu tím nhạt thơ mộng thoáng buồn, chất giấy ngà ngà trắng như lãng đãng màu khói sương, hệt như không khí bao trùm lên thơ Hàn Mặc Tử. Mỗi bài thơ là một tiếng lòng của chàng thi sĩ tài hoa yểu mệnh, dạt dào niềm yêu đời yêu người nhưng cũng đau đáu bên mình niềm mặc cảm và nỗi đau bạo bệnh. Rất hài lòng với hình thức cũng như chất lượng của tập thơ này, luôn hài lòng tuyệt đối với cách trình bày sách của Nhã Nam
5
199482
2015-09-15 15:39:19
--------------------------
299214
5111
Đọc thơ Hàn Mặc Tử lúc nào cũng có cảm giác vừa hư vừa thực. Cảnh rất mộc mạc mà thanh tao. Tình thì rất buồn, nhiều khi không có lấy một chữ 'buồn' nhưng đọc xong bài nào cũng thấy sầu man mác, cũng thấy hình như chưa vẹn toàn. 
Một điểm cộng cho tuyển tập này của Nhã Nam là hình thức làm rất khéo. Giấy in có hoa văn trang nhã, cộng thêm tranh minh họa khiến người cầm cuốn sách có cảm giác như từng câu chữ đều được những người in sách nâng niu đặt vào. Thật sự rất ưng với cách làm này của nhà phát hành. 
5
5768
2015-09-13 07:18:01
--------------------------
291555
5111
Lâu lâu mình ngâm nga :

"Sao anh không về chơi thôn Vĩ
Nhìn nắng hàng cau nắng mới lên
Vườn ai mướt quá xanh như ngọc
Lá trúc che ngang mặt chữ điền"

Cuối tuần rời xa sự hối hả của cuộc sống hàng ngày mà đến với Hàn Mặc Tử thì tuyệt biết bao ! 
Ý thơ ngọt ngào, hình ảnh nên thơ mà đẹp đẽ vô ngần, khiến tâm hồn ai cũng dịu lại, trước cái nắng nóng lẫn cái nóng của tính người. 

Mình yêu thơ của ông không chỉ vì nó miêu tả cảnh đẹp của quê hương mà dưới những vần thơ ấy ẩn giấu tâm trạng buồn miên mang, đôi khi triết lý sâu sắc về cuộc đời, trong đó có tình yêu - như là mạch sống của thơ Mạc Tử. 

"Ngày mai trong đám xuân xanh ấy
Có kẻ theo chồng bỏ cuộc chơi"
5
535536
2015-09-06 13:39:04
--------------------------
226870
5111
Theo thông tin trên báo đọc được thì tập Thơ-Hàn Mạc Tử là 1 trong 3 giai phẩm thơ sắp được xuất bản trở lại. Lưỡng lự lần hồi không biết bao lần để quyết định xem có đặt mua hay không. Bởi vì như nhận xét: Đây là tinh tuyển thi ca của thi sĩ tài hoa bạc mệnh, đồng thời cũng là tác phẩm thơ thuộc loại lạ nhất, đẹp nhất, hay nhất của thi ca Việt Nam giai đoạn 1930-1945. Hơn nữa, luôn cảm giác rằng thơ Hàn Mạc Tử luôn giằng xé, dữ dội, đau đớn đến nghen lòng. Nói về thơ thì mỗi người có một cách cảm nhận riêng. Hàn Mạc Tử lại là 1 tác gia lớn nên tôi sẽ không đưa ra phan tích cá nhân về nội dung tác phẩm. Ai yêu mến thơ nói chung và Hàn Mạc Tử nói riêng thì sẽ tìm được ở đây chút chân trị nơi con người tài hoa này.
Về hình thức thì không cần phải nói gì thêm đối với những sách do Nhã Nam phát hành đặc biệt là bộ tác phẩm Việt Nam danh tác

4
502287
2015-07-13 08:39:08
--------------------------
217307
5111
Những giai phẩm thơ trong bộ Việt Nam danh tác của Nhã Nam được in rất ấn tượng. Có thể nói là không chê vào đâu được! Trong các quyển thơ này còn có các hình ảnh màu minh họa rất đẹp. Giấy in dày, phông chữ mang màu hoài niệm!

Những bài thơ trong tác phẩm này có lẽ đã được độc giả nhiều thế hệ biết đến rộng rãi. Chắc hẳn không cần bàn nhiều về những tác phẩm đã đi vào lòng người này. Chúng trở nên độc đáo, đẹp đẽ hơn khi được viết ra từ một tâm hồn của con người mang bệnh tật, thứ bệnh phong “nan y” thời bấy giờ đã lấy đi của chúng ta một thi sĩ đại tài.

Như lời Chế Lan Viên từng nói: "Mai sau, những cái tầm thường mực thước kia sẽ biến tan đi, và còn lại ở cái thời kỳ này chút gì đáng kể, đó là Hàn Mặc Tử".
5
598102
2015-06-29 09:41:42
--------------------------
199256
5111
Cuốn sách này của Nhã Nam không chê vào đâu được. Thiết kế bìa tao nhã, hình ảnh đẹp, chọn lọc, màu sắc êm dịu. Trang giấy đẹp, trắng phù hợp cho mắt nhìn lâu. Nội dung rất hay đó là các bài tuyệt bút của nhà thơ họ Hàn, liệt kê đầy đủ , sắp xếp bắt mắt. Ở thơ Hàn Mặc Tử ta tìm thấy điều gì đó huyền bí về câu chữ, sự lưỡng lự, e thẹn trong tình yêu, sự điên loạn khi bị cơn bệnh phong hành hạ, sự khát khao giao cảm với trăng.... những điều đó đã tạo nên nét riêng độc đáo cho nhà thơ. Nhưng trên hết là giá cả cũng phải chăng.
5
351857
2015-05-21 20:39:28
--------------------------
192449
5111
TỪ những ngày còn bé mình đã được mẹ đọc cho nghe những Đây thôn Vĩ Dạ hay Mùa xuân chín. Thực sự rất thích, rất yêu, rất đồng cảm với nhà thơ. Đọc thơ Hàn Mặc Tử dễ mà cảm nhận được sâu sắc cái tình yêu cuộc sống thiết tha mà cháy bỏng trong từng vần thơ. Có chút gì đó như nuối tiếc, có chút gì đó như dằn vặt, day dứt,khiến độc giả hiều hơn cái cuộc sống cô độc nơi trại phong, dường như cách biệt với cuộc sống con người.Thực sự tiếc khi nhiều tác phẩm của ông bị mất đi, người sau không thể nào thưởng thức được nữa.
5
606127
2015-05-04 15:41:55
--------------------------
166883
5111
Mình thực sự rất thích bộ Việt Nam danh tác này. Vì sao? Vì mình rất thích đọc những sách đẹp ( he he ). Mà mình lại càng yêu tập thơ của Hàn Mặc Tử. Mình thấy có bạn nói phần bìa sách đơn điệu bên dưới. Mình dám chắc là bạn chưa nhìn bìa sách, cầm sách trên tay hoặc bạn không hiểu thơ Hàn! Bìa sách trơn lỳ, chống thấm nước, vì bìa trơn nên soi dưới đèn nó cứ óng óng lên màu tím rất đẹp, cầm trên tay cực thích, mình cứ ngắm với rờ mãi. Sách vuông vắn, đẹp. Cả tập thơ được phủ một màu tím hoa cà đúng chất thơ mơ màng của Hàn Mặc Tử. Đọc thơ đúng là đỡ nhàm chán thật. Vì chất giấy và thiết kế màu như thế nên giá cả tập thơ có thể hơi mắc. Cũng số tiền này mình có thể mua 2 tập thơ khác ( ko lời bình nhé) của 2 nhà thơ khác cơ (tất nhiên là ko đẹp bằng). Mình lại mua sách này trước khi tiki fix giá xuống 38k nên cứ tiếc hùi hụi. huhu.
5
114793
2015-03-13 19:15:41
--------------------------
159569
5111
Dù đã xem trước hình nhưng khi nhận sách từ Tiki mình vẫn rất bất ngờ về quyển sách này. Vì sách thiết kế đẹp quá: khổ sách rộng, bìa vẽ đẹp, chất liệu giấy cứng rất chắc chắn, cả quyển lại bao trùm tông màu tím mà mình rất thích nữa. Nhưng mình đánh giá cao quyển sách này như thế còn bởi nội dung mà Nhã Nam mang tới rất ý nghĩa, nhiều bài thơ của Hàn Mặc Tử được in lại trên một quyển sách rất đẹp, xen trong đó là một bức tranh trừu tượng tuyệt diệu làm thỏa lòng một người yêu thơ như mình. Những bài quá nổi tiếng như Đây thôn Vĩ Dạ hay Mùa xuân chín mà mình đọc lại trong quyển sách này lại thấy hay hơn, nhận ra nhiều điều ý nghĩa hơn trong lời thơ.
Mình chỉ thấy tiếc một điều là cùng một bộ Việt Nam danh tác mà sao trong các quyển thơ không thấy có một đoạn bình văn ngắn của người biên soạn như các quyển truyện ngắn, nếu Nhã Nam thêm vào thì sẽ hoàn hảo hơn nữa.
5
442739
2015-02-17 18:54:57
--------------------------
158785
5111
Thật vui vì tác phẩm của thi sĩ bạc mệnh và nhiều tác giả nổi tiếng khác được Nhã Nam in lại. Sách thiết kế rất đẹp mắt, từng trang giấy được in rõ ràng kĩ càng thể hiện sự trân trọng với tác giả. Đọc từng bài thơ in trong sách như Mùa xuân chín, Đây thôn Vĩ Dạ... tôi vẫn thấy nuối tiếc, thổn thức. Hàn dường như vẫn hiện hữu qua từng thi phẩm, anh ra đi nhưng từng vần thơ rướm máu, vừa khát vọng vừa tuyệt vọng, vừa trong sáng vừa cuồng loạn vẫn sống mãi. Hi vọng trong tương lai gần sẽ có 1 cuốn sách in đầy đủ các thi phẩm của Hàn ra đời để mọi người có thể hiểu trọn vẹn hồn thơ Hàn Mặc Tử.
4
114409
2015-02-13 22:17:40
--------------------------
155018
5111
Bản thân mình là người thích đọc thơ văn. Chị bạn cho mượn tập thơ Hàn Mặc Tử để đọc, đồng thời có nhã hứng làm một bài thơ mượn ý thơ cuả Hàn Mặc Tử, mình thấy cũng khá hay.

Vĩ Dạ về đây, người chẳng thấy
Mộng Cầm còn tưởng dưới ánh trăng
Mùa xuân đã chín người lỡ bước
Theo chồng cũng đà bỏ cuộc chơi

Thu này ai còn trồng hoa cúc
“Cúc ngó đơn sơ, lắm mặn mòi”
Vườn trăng thu heo may vẫn thổi
Ngắm trăng này ai nỡ bán trăng

Hăm mấy mùa trăng chưa qua hết
Đau thương lăn lộn với cô liêu
Duyên này cũng đành xa cách biệt
Thi nhân lỡ hẹn với gót Kiều.
4
473130
2015-01-30 22:52:57
--------------------------
154591
5111
Dành cho những ai yêu thích thơ của Hàn Mặc Tử .Trong những bài thơ siêu thực của Hàn Mặc Tử, người ta không phân biệt được hư và thực, sắc và không, thế gian và xuất thế gian, cái hữu hình và cái vô hình, nội tâm và ngoại giới, chủ thể và khách thể, thế giới cảm xúc và phi cảm xúc. Mọi giác quan bị trộn lẫn, mọi lôgic bình thường trong tư duy và ngôn ngữ, trong ngữ pháp và thi pháp bị đảo lộn bất ngờ. Nhà thơ đã có những so sánh ví von, những đối chiếu kết hợp lạ kỳ, tạo nên sự độc đáo đầy kinh ngạc và kinh dị đối với người đọc
4
544733
2015-01-29 20:03:09
--------------------------
154554
5111
Hàn Mặc Tử là một trong những cây bút nổi bật của dòng thơ lãng mạn Việt Nam. Ông đã đem đến cho thơ mới một phong cách độc đáo và sáng tạo. Bên cạnh những tác phẩm bình dị, chan chứa tình quê là các tác phẩm đầy những cảm hứng lạ lùng, thậm chí đến điên loạn, phản ảnh trực tiếp một tâm hồn yêu thơ, yêu đời, nhưng lại quằn quại vì cơn bệnh đau đớn dày vò. Ông đã biến nỗi đau của đời mình thành khao khát được sống, được yêu, được cầm bút, được sáng tác....Dù nửa đời người chưa qua hết, nhưng Hàn Mặc Tử đã làm tròn sứ mệnh của mình, để lại cho nền văn học Việt Nam một đời thơ giá trị. Cuốn sách đã tổng hợp một cách chọn lọc những bài thơ mà theo tôi là tuyệt bút của Hàn Mặc Tử, thể hiện rõ nét phong cách nghệ thuật độc đáo của nhà thơ.
5
546314
2015-01-29 18:16:24
--------------------------
150873
5111
Nhắc đến thơ của Hàn Mặc Tử, có lẽ ai cũng sẽ nhớ về một hiện tượng thơ kỳ lạ bậc nhất của phong trào thơ mới. Thơ của ông kỳ lạ, thậm chí điên loạn nhưng vẫn đầy lãng mạn, giàu cảm xúc và những khát khao của con người. Tập thơ "Điên" của ông chứa đầy sự hòa quyện của những hình ảnh mang sắc thái đối lập, dữ dội mà vẫn êm dịu. Trên hết, thơ Hàn Mặc Tử luôn để lại một nỗi buồn man mác, nỗi buồn của một tâm hồn tha thiết với cuộc sống nhưng phải sớm rời khỏi trần thế này...
5
98372
2015-01-17 22:32:04
--------------------------
148775
5111
Về mặt hình thức thì bìa tập thơ này có phần hơi chút đơn điệu.
Về mặt nội dung thì ai yêu thích thơ Hàn Mạc Tử nên tìm đọc tập này, cả nhân tôi thấy tổng hợp các bài thơ trong này thể hiện khá rõ phong cách nghệ thuật trong sách tác thơ văn của Hàn Mạc Tử.
Một tiếng thơ rất riêng, cứ len lỏi ngấm vào người đọc, lúc rạo rực, lúc mơ hồ, lúc man mác, bâng khuâng
Đọc thơ Hàn thi sĩ ta bắt gặp một tâm hồn thiết tha yêu cuộc sống, yêu thiên cảnh, yêu con người đến khát khao, cháy bỏng; một khát vọng sống mãnh liệt đến đau đớn tột cùng
4
496784
2015-01-11 14:40:33
--------------------------
144495
5111
Hàn Mặc Tử - con người tài hoa nhưng bạc mệnh. Thi nhân ra đi khi còn quá trẻ và sự nghiệp văn thơ cũng vừa mới bắt đầu. Tuy nhiên, dù chỉ có hơn 10 năm từ khi chập chững bước vào làng thơ cho đến khi rời xa cõi đời, Hàn Mặc Tử cũng đã kịp cho xuất bản hơn 10 tập thơ với hàng chục bài thơ đặc sắc; không chỉ có vậy, thi nhân để lại trong lòng hàng triệu người hâm mộ trên khắp mọi miền tình cảm mến thương và tiếc nuối khôn nguôi.Thơ ông có những lúc viết trong mơ hồ gọi là thơ điên nhưng cũng đông đầy tình cảm và khác vọng được sống được yêu,đôi khi cũng nhuộm màu siêu thực.
4
448401
2014-12-26 23:12:00
--------------------------
