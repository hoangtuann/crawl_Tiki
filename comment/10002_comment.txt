291987
10002
Từ khi mua quyển sách này về, thích nhất là bé nhà mình cứ đòi mẹ kể chuyện cho nghe. Bé đã biết chăm chú nghe mẹ đọc sách - vì hình ảnh rất đẹp - chứ không còn lơ là mỗi khi mẹ kể chuyện như trước.
Ở tập sách này, sau khi nghe mẹ giới thiệu, bé đã nhìn hình kể được tên các động vật sống ở đại dương. Bé còn học được cách bảo vệ môi trường sống xung quanh. Bé đã nhìn đồng hồ đọc được giờ và vẽ được kim đồng hồ khi cho bé giờ.
Thật tuyệt vời phải không các bạn!
5
496573
2015-09-06 22:07:35
--------------------------
