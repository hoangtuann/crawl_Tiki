490838
5665
Mình đã đắn đo rất nhiều lần trước khi quyết định đặt mua quyển này. Bởi khi đọc qua tựa mình cứ nghĩ nội dung sẽ là những cuộc tranh đấu khốc liệt của các băng nhóm xã hội đen giống như tác phẩm Bố Già vậy đó, nhưng không, khi đi sâu vào nội dung thì đó lại là một khía cạnh khác, quyển sách như mở ra một thế giới khác, giúp mình hiểu thêm nhiều điều ở thế giới ngoài kia. Và đúng như lời giới thiệu, quyển sách như "vén màn thế giới ngầm Nhật Bản", mọi thứ như dần hiện rõ, những con người, những mảnh đời tồn tại ở mặt khác của xã hội, họ tương trợ lẫn nhau khi khó khăn, họ đặt danh dự lên trên hết, họ- dù đói rách, dù cái rét cắt da cắt thịt vẫn không một lời than vãn, họ - thà bỏ mặc mạng sống mình chứ không thể để người đời coi khinh... Và trong khi những con người ở mặt trái xã hội sống có tình có nghĩa thì những con người mang quân hàm với danh nghĩa chiến đấu vì đất nước lại chỉ sống ích kỷ cho bản thân họ.Có lẽ mình không thể nói hết được những gì mình đã rút ra sau khi đọc xong Đời Yakuza. Để cảm nhận sâu sắc hơn mình nghĩ các bạn nên cảm nhận qua từng tranh sách.
3
479081
2016-11-14 22:38:59
--------------------------
480205
5665
Đời Yakuza là một cuốn hồi ký hấp dẫn. Nếu nó được gọi là hồi ký. Đúng hơn thì nó hấp dẫn, dù là thể loại gì đi nữa. Quá hấp dấn. Thú vị. Đặc biệt. Và đời thật.
Eiji Ijichi là một con người đặc biệt. Đúng hơn là thú vị. Có thời tuổi trẻ nông nổi, láo xược. Từng giàu có. Có địa vị xã hội. Từng vào tù ra tội. Đúng. Eiji rất thú vị.
Tại sao lại nói nó đời thật? Không rõ nữa. Nhưng chẳng phải ai trong đời cũng từng gặp những người với mảnh đời và câu chuyện thú vị ư? Chỉ là Eiji, với cuộc đời của mình, gặp được nhiều hơn người khác.
Chỉ có một thắc mắc. Đáng lẽ nhiều hơn, đúng hơn là mong có nhiều hơn khi bắt đầu đọc. Nhưng cuối cùng lại chỉ có một. Tại sao đám tang Eiji lại chỉ có 4 người? Kamezo đã chết, còn những người anh em, những người quen, bang hội,... tại sao lại ít đến thế, với 1 con người thú vị đến thế?
Tại sao?
Phải chăng vì Eiji bình thường?
Confession of a Yakuza - The Gamble's tale
5
162493
2016-08-16 07:03:16
--------------------------
460817
5665
Một cuốn sách có bìa rất ấn tượng, cái tên Yakuza khá xa lạ với tôi, nhưng khi đọc lời tựa giới thiệu trên Tiki tôi đã quyết định lựa chọn cuốn sách này. Các cuốn sách viết về văn hóa, đời sống, xã hội Nhật Bản luôn tạo cho tôi cảm giác thích thú và yêu thích tìm hiểu. Bên cạnh sự phát triển vượt bậc của một cường quốc trên thế giới, chúng ta thật thiếu sót nếu không quan tâm đến cả những mảng tối của một quốc gia đó, Yakuza đối với cá nhân tôi cảm nhận thì đây cũng là một nét văn hóa đặc trưng của Nhật Bản mà bất cứ ai yêu mến đất nước này cũng cần biết đến. 
4
310872
2016-06-27 12:49:33
--------------------------
450851
5665
Mình rất thích đọc những quyển nói về thế giới ngầm nên cuốn này là một lựa chọ tuyệt vời.Yakuza trong những thế kỉ 20 ở nhật được miêu tả một cách sống động,qua đó giúp người đọc hiểu thêm về xã hội nhật bản thời kì đó.Yakuza làm những việc tàn nhẫn nhưng họ luôn luôn có sự trung thành,tình đồng đội anh em ,đọc cái khúc bị giam trong tù qua lời ông thầy bói mới thấy ngay cả người thân mà còn có lúc lạnh lùng không tin nhau,không ngờ những con người xa lạ ở trong tù lại có thể đối xử chân tình với nhau đến thế.Một cuốn sách hay đáng để đọc.
4
526322
2016-06-19 11:19:12
--------------------------
449345
5665
1 tiểu thuyết trinh thám kể về cuộc đời của 1 người đàn ông trong thế giới xã hội đen của Nhật Bản (Yakuza), thích hợp cho các bạn nam hoặc các bạn yêu thích nền văn hoá Nhật. Bạn mình nhờ mình mua cuốn này nên mình chưa có dịp đọc nội dung, tuy nhiên mình cũng xem qua bìa sách, thấy cũng khá thu hút và cuốn sách được rất nhiều đánh giá tốt, lời khen và bình chọn từ các chuyên gia nổi tiếng. Tuy ko phải thể loại mình thích, nhưng nếu bạn nào thích thể loại này thì nên mua và đọc thử :)
5
6118
2016-06-17 12:48:01
--------------------------
432912
5665
Giọng văn bình thản, như đời người đàn ông trong sách. Cái cách mà ông ấy trải qua cuộc đời thật bình thảnh. Vì những biến cố trong cuộc đời ông, những ngã trẻ khiến cho ông trở thành một con người đặc biệt. Cuốn sách cho người đọc hiểu thêm về Yakuza ngày xưa. Lúc họ chỉ kiếm tiền qua cờ bạc chứ không như hiện tại (buôn lậu, thuốc phiện, chính trị...). Nhưng dù là xã hội đen, họ cũng coi trọng phép tắc, lễ nghi, chữ tín. Họ có luật ngầm với chính quyền. Họ đặt tình nghĩa huynh đệ lên hàng đầu. Chứ không như phim ảnh (đánh nhau, chém giết). Điểm cộng là sách đẹp, bìa sách dễ gây chú ý, không có lỗi chính tả, dịch rất có tâm. Màu giấy ngà dễ đọc.
5
1077685
2016-05-20 00:20:54
--------------------------
388888
5665
Đây là một quyển sách hay, câu chữ dễ hiểu mà cuốn hút, phản ánh một mảng tối của xã hội Nhật Bản thời xưa. 
Quyển sách này hay và đáng đọc. Tôi cần nhấn mạnh lại như vậy. Tuy nhiên, dịch giả có vẻ vì muốn gây tò mò với người đọc nên khi dịch qua tên tiếng Việt đã ngoặt nó qua một hướng khác. Bản thân quyển sách là câu chuyện của Eiji, cuộc đời được Eiji kể lại qua lời bác sĩ, từ khi là một cậu thanh thiếu niên cho đến những ngày cuối đời, chứ không chỉ xoáy vào duy nhất quãng đời ông là yakuza như tên sách vốn nói lên như vậy. Tuy vậy, điều quan trọng phải nói ba lần, quyển sách này vẫn được tôi đánh giá cao.
4
62883
2016-02-29 21:35:31
--------------------------
379940
5665
Trước khi đọc tác phẩm này, tôi cứ nghĩ trong những trang sách kia sẽ là những cảnh đâm chém đẫm máu, những toan tính mưu mô tinh xảo kinh hoàng, hay là những lần giao chiến tranh quyền đoạt thế. Yakuza mà tôi mường tượng ra phải là thế lực khiến người ta kinh sợ đồng thời cũng phải kính nể.
Nhưng trái ngược với toàn bộ những gì mong đợi. Yakuza của hồi ký này khiến tôi phải trầm trồ thán phục và kính trọng. Sự trung thành không ai bì kịp, những quyết định dù đúng dù sai vẫn mang hào khí của tuổi trẻ. Đọc mà ước gì mình cũng được có một tuổi trẻ dù sai lầm nhưng vẫn đầy oanh liệt, có thể làm được những gì mình muốn. 
Nó định nghĩa lại cho tôi một lần nữa về khái niệm của sức mạnh và sự thần phục. Tựa như Shank tóc đỏ trong One Piece xem việc bị đổ rượu lên đầu trước bàn dân thiên hạ chả là cái gì, với Yakuza, "cái gì nhịn được là nhịn đến cùng". Tôi nhận ra rằng đao kiếm chỉ làm người ta kinh sợ, còn phẩm chất mới là cái khiến con người kính nể.
Bên cạnh câu chuyện đời, một nước Nhật thời loạn lạc hiện ra khác hẳn một cường quốc như bây giờ. Nó làm người đọc phải tự hỏi làm thế nào mà một nước Nhật như thế là có thể phát triển được như bây giờ.
Đời Yakuza như một cuốn "Nghìn lẻ một đêm" của Yakuza. Quả thật rất đáng xem.
5
822265
2016-02-13 11:10:16
--------------------------
377689
5665
Sách mở ra cho người đọc 1 cách nhìn khác về thế giới ngầm Yakuza của nước Nhật Bản vốn rất nổi tiếng trên thế giới. Hình ảnh thật sự của Yakuza khác khá nhiều so với những gì chúng ta đã được xem trên điện ảnh. Lời văn chân thật, nhẹ nhàng nhưng đều đều quá, không có nhiều điểm nhấn dễ khiến người đọc mất hứng thú đối với sách. Và thực sự là cuộc sống của nhân vật Eiji tuy có rất nhiều biến cố nhưng lại không quá hấp dẫn, đó có lẽ là hậu quả do điện ảnh mang lại.
4
934783
2016-02-03 09:33:27
--------------------------
355033
5665
Cá biệt , mạnh mẽ , phỏng lại đời sống và con người theo chủ nghĩa tự do , quy tắc nghiêm khắc , nhưng mang theo nhiều tình nghĩa , không phân biệt mà sống hòa thuận với  mọi người ,lối ứng xử khác lạ đầy tính văn hóa , thông thạo nhiều sức mới ở hiện đại , làm ta hơi sợ hãi nhưng đắm mình vào thế giới trái ngược tính nết , rõ hơn về băng đảng hoạt động rộng khắp nước nhật bản còn nổi tiếng , thịnh hành , biết đến nhiều nhất .
4
402468
2015-12-19 01:34:31
--------------------------
347427
5665
Năm ngoái mình mua quyển truyện Đời yakuza, đọc được 1 ít rồi vô tình làm mất. Chưa đọc được hết nên phải đi mua lại. Quả thật là quyết định không sai lầm khi mua quyển truyện lần thứ hai. Ấn tượng ban đầu của mình khi quyết định mua quyển sách này là do hình vẽ ở ngoài bìa trông rất "gai góc". Đối với thói quen đọc truyện của mình, mình hay chọn những quyển có nội dung nhẹ nhàng, và lần đầu đến với Đời yakuza là lần đầu mình thử đọc một nội dung khác so với những cuốn khác. Cũng may hôm ấy có thời gian đọc thử, vậy mà đã cuốn mình theo lời dẫn của người bác sĩ về những câu chuyện người bệnh nhân "đặc biệt" của mình, về những trôi nổi, những cái ẩn sâu đằng sau một băng nhóm xã hội đen, tốt có, xấu có, và hơn hết là một thế giới đầy "gai góc" mở ra đối với bạn đọc, ít nhất là đối với mình :) ngoài ra truyện có nhiều chữ, dãn dòng cân đối, không nhiều quá, không ít quá, rất thoải mái và "thỏa mãn" khi đọc.
4
820063
2015-12-04 13:28:13
--------------------------
327861
5665
"Đời Yakuza" là một tác phẩm chiều sâu, được viết bằng lối văn chậm rãi mà sắc sảo, mạnh mẽ; đã lột tả tột cùng những góc khuất trong thế giới ngầm ở Nhật Bản. Nhưng cuốn sách lại không phô bày những u ám hay khắc nghiệt mà thể hiện cái đẹo, cái tình trong giới Yakuza, cho độc giả một góc nhìn khác về họ. Một cuốn sách vừa sâu sắc vừa cảm động, khiến người đọc cảm thấy hấp dẫn ngay từ những chi tiết thực tế, bình dị nhất như miêu tả đường phố, hàng quán. 
4
6502
2015-10-28 16:56:03
--------------------------
292037
5665
Đây là quyển sách kể câu chuyện về một đời của một con người từng trải qua rất nhiều điều và hơn thế là thông qua câu chuyện này, ta biết thêm về một thời kì của một đất nước. Sách hấp dẫn và chi tiết đến ngạc nhiên khi mà từng khung cảnh, từng cái tên đường, tên phố ngày xưa đều được kể ra lần lượt. Thế giới ngầm Nhật Bản được miêu tả trong sách không bao gồm quá nhiều máu và bạo lực mà trên hết là nói lên cái tình nghĩa giữa người với người mà ta cứ lầm tưởng nó không tồn tại giữa những têm xã hội đen hung tợn. Đọc xong sách, thậm chí mình còn tự hỏi không biết còn có quyển sách nào về mafia Ý hay không vì câu chuyện trong quyển sách về thế giới ngầm này đã cuốn hút mình rất nhiều ^_^
Sách in đẹp, giấy tốt, rõ chữ, không lỗi chính tả 
5
123775
2015-09-06 22:41:03
--------------------------
265262
5665
Cuốn sách sẽ cung cấp cho bạn một cái nhìn gần gũi và chân thực hơn về tầng lớp Yakuza - tầng lớp tăm tối và bị khinh miệt nhất tại Nhật Bản. Nó không chỉ đơn thuần là vạch trần bản chất của những tên du côn hay đại ca khét tiếng mà nó viết để nói lên một phần của họ, tìm kiếm sự đồng cảm nơi người đọc, giúp họ hiểu hơn và thấu hiểu hơn cho đời Yakuza. Hơn nữa, sách còn ca ngợi cái tình cái nghĩa giữa con người với con người. Dù mình không biết nội dung cuốn sách là hư cấu hay phi hư cấu nhưng nó rất cuốn hút, khiến mình cứ cầm đọc mãi mà không dứt ra được. Vừa đọc vừa thông cảm và xót xa.
4
464538
2015-08-13 21:05:30
--------------------------
256929
5665
Từng biết đến nền văn hóa Nhật Bản thông qua những truyền thống và hình ảnh đặc trưng mang phong thái nhẹ nhàng và thanh thoát nhưng sau khi đọc xong quyển sách này mình hiểu thêm về một loại "thế giới ngầm" tồn tại lâu đời ở xứ sở hoa anh đào này. 
Quyển sách thật sự rất hay và đầy ý nghĩa về cuộc sống đầy thế lực của xã hội mafia diễn ra ở đất nước này. Sách với nhiều điều thú vị và cho ta thấy được cái tình nghĩa giữa con người với nhau trong đời thông qua Đời Yakuza
5
618799
2015-08-07 11:31:05
--------------------------
234352
5665
   Cuốn sách này dễ dáng trở thành cuốn sách yêu thích của tôi trong nền văn học Nhật Bản. Nó hấp dẫn và đầy đủ chi tiết đến đáng ngạc nhiên về thời Showa. Đây không phải loại truyện xã hội đen khuôn sáo, nó ít máu và bạo lực, cuốn sách uốn khúc lặng lẽ qua cuộc đời của một yakuza như thể một cuốn hồi kí nhiều thông tin và đầy thú vị dù rằng chẳng biết bao nhiêu phần của nó là hư cấu, nhưng nó mở ra một trong những thế giới ngầm khó nắm bắt nhất trên thế giới - một cái nhìn chân thực hơn là trên phim ảnh.
5
529844
2015-07-20 12:04:22
--------------------------
232776
5665
Cuốn sách Đời Yakuza của Saga Junichi chắc hẩn là một lựa chọn không tồi cho những người yêu mến bản sắc văn hóa và tính cách những con Người của xứ sở hoa anh đào - Nhật Bản này. Cuốn sách này đã tạo cho tôi nhiều ấn tượng sâu sắc về tình cảm thiêng liêng giữa những người anh em trong trong bang, sự nghĩa hiệp, hay tình đoàn kết, giúp đỡ lẫn nhau, những cuộc chia tay đầy nước mắt,.. qua dòng ghi chép của Saga Junichi. Về bìa sách khá đẹp mắt, giấy và mực chất lượng. Nhưng cuốn sách này mà có bìa cứng thì khá là tuyệt vời. Dù sao tôi cũng cảm ơn tiki đã đem lại cuốn sách này cho tôi và cũng như nhiều người khác!!!
5
290697
2015-07-19 00:00:28
--------------------------
227066
5665
Quyển sách này quả là một kho kiến thức về những gì không chính thống tồn tại quanh chúng ta. Tôi đọc về Bố Già, về Yakura đều rút ra được rất nhiều điều đáng để học hỏi. Theo như tôi được biết khi xảy ra thảm họa động đất và sóng thần lớn nhất trong lịch sử Nhât Bản ở Tohoku vào năm 2011, 3 tổ chức Yakura lớn nhất nước này là những người đầu tiên gửi hàng cứu trợ đến cho các nạn nhân và sau đó cùng cảnh sát Nhật khắc phục hậu quả. Những điều quá đáng để học hỏi!
4
581502
2015-07-13 13:33:02
--------------------------
204992
5665
Tôi thật sự bất ngờ về những gì được đọc. Một thế giới trong nửa kia của bóng tối, chân thật và sinh động đến khó tả . Cách viết của tác giả lôi cuốn đến mức tôi như thấy mình ở đó, đi theo chân của Eiji qua từng trang cuộc đời ông . 
Dù họ là 1 phần của bóng tối, bị người đời xa lánh, sợ hãi, có khi là căm ghét . Nhưng thế giới của họ là 1 thế giới cũng có trật tự, cũng có tự trọng, và sống vì tín nghĩa .Sẵn sàng dùng cả mạng sống để đền ơn vì 1 lần được giúp đỡ . Họ sống với nhau bằng tình cảm anh em, bằng lòng kính trọng ,hi sinh ,trung thành với thủ lĩnh .
Tôi chỉ hơi tiếc là cuốn sách chỉ có gần 400 trang sách , nếu nó dài hơn một chút chắc sẽ có nhiều điều thú vị nữa .
5
616860
2015-06-05 14:31:02
--------------------------
195757
5665
Mình từng nghe nói đến Yazuka nhưng không hiểu rõ lắm, chỉ nghĩ đó là một tổ chức xã hội đen với đám dân anh chị máu mặt ở Nhật, nhờ đọc cuốn sách này mà mình mới hiểu rõ hơn về họ. Tuy là mafia nhưng họ có quy tắc nghiêm ngặt, có tôn ti trên dưới, họ phải trải qua rất nhiều nỗi đau đớn cả về thể xác và tinh thần, phải nỗ lực rất nhiều mới có thể vươn lên tìm chỗ đứng. Đọc xong, tuy mình không ủng hộ họ nhưng mình lại rất khâm phục họ. Nhờ “Đời Yazuka” mà mình đã có một cái nhìn mới đa chiều hơn, sâu sắc hơn về cả một tầng lớp trong xã hội.
5
449880
2015-05-14 16:25:16
--------------------------
194127
5665
Một câu chuyện ngắn gọn, súc tích, cực kì cuốn hút. Từ xưa đến nay, xã hội đen chúng ta thấy trên phim ảnh, sách báo đều là những người rất đáng sợ. Nhưng với Đời Yakuza, thế giới ấy hiện lên hoàn toàn khác, trang nghiêm, tôn ti trật tự rõ ràng, có trên có dưới, hoàn toàn tôn trọng với cấp trên, đối xử tốt với đồng đội, tình nghĩa và tử tế với những người xung quanh, đặc biệt là chữ Nhẫn. 
Dù xã hội có chà đạp họ ra sao, họ vẫn vươn lên, lời thề đã nói thì nhất định phải giữ, luật đã đặt ra ắt phải tuân theo.
Dù những gì Eiji nhân vật chính trong truyện kể lại chỉ là những đoạn nhỏ trong cả quãng đời nhưng ngần ấy thôi cũng đủ để người ta tôn trọng khí phách của một yakuza, tôn trọng đầu óc nhạy bén để kinh doanh mang lại tiền bạc để nuôi băng đẳng, tinh thần cố gắng chèo chống cho cả băng đẳng khi gặp khó khăn. 
Thực sự rất đáng nể phục theo những nghĩa tích cực nhất.
4
119223
2015-05-09 15:55:22
--------------------------
182840
5665
Yakuza - là từ được dùng để chỉ mafia hay các tổ chức tội phạm truyền thống ở NB, trước đây tôi cứ nghĩ yakuza chuyên bảo kê mà thôi nhưng sau khi đọc cuốn sách này tôi mới biết được rằng để công nhận là yakuza chân chính trải qua đau đớn về thể xác và cả tinh thần nữa mới mong có chỗ đứng trong "thế giới ngầm". Cuốn sách đã tả hết sức chân thực và trần trụi về đời yakuza - hiếu chiến, đẫm máu nhưng học cũng biết yêu, biết đau khổ. Qua cuốn sách này mình hiểu hơn về một phần "đen tối" của xã hội Nhật Bản vài thập niên trước.
4
21730
2015-04-14 22:52:12
--------------------------
175222
5665
Cuốn sách tuy không dày, nhưng lại chứa đựng trong đó tất cả hỉ, nộ, ái, ố của 1 đời người. Có đau đớn thể xác, nỗi khổ tinh thần, nhiệt thành tuổi trẻ, một tình yêu chưa chín, và nỗi hối hận đi theo suốt cuộc đời - tất cả làm nên hình ảnh của một yakuza tiêu biểu trong xã hội đầy nhiễu nhương của Nhật Bản sau thế chiến. Giang hồ thì cũng là người, cũng có đấu tranh, dằn vặt, cũng có người tốt kẻ xấu, cũng có những ước nguyện mãi không thành. Những trải nghiệm được kể ra trong cuốn sách chỉ là kí ức của 1 yakuza đã đến tuổi xế chiều, nhưng cũng như những hình xăm trên người ông, sẽ không bao giờ rời bỏ ngay cả khi ông đã yên nghỉ.
5
68751
2015-03-29 18:24:03
--------------------------
169861
5665
Đời Yakuza mang đến cho người đọc một cái nhìn mới mẻ và chân thật về xã hội, con người Nhật Bản thập niên 20 - 30.
Có người từng nói "Trước khi lên đường đến một nơi nào đó, hãy cố trút bỏ mọi định kiến, mọi hình dung. Hãy lên đường như một tờ giấy trắng với khao khát được phủ kín, lấp đầy." Qua lời kể của nhân vật Eji, rõ ràng ta thấy người Nhật cũng có những góc khuất, cũng là con người vướng đầy bụi trần, hỉ nộ ái ố chứ không hoàn hảo như những gì người ta vẫn tưởng. Họ chỉ thật sự thay đổi và trở nên kỉ luật, có tổ chức hơn sau Thế chiến thứ 2. Đặc biệt, qua lời kể và hành động của Eji, tôi thấy ông là một người đầy nghĩa khí, trí tuệ, và sống rất tình cảm chứ không hẳn chỉ là tầng lớp bị xã hội gán cho cái danh "giang hồ". Đời Yakuza còn giúp tôi hiểu khá nhiều về văn hóa và những giá trị truyền thống của Nhật Bản. Một quyển sách thật sự đáng đọc.
5
135709
2015-03-18 22:50:38
--------------------------
156947
5665
Đời Yakuza mang đến gốc nhìn mới mẽ và đầy chân thật về xã hội Nhật vào những năm thế kỷ 20.
Qua cách kể của Eiji mình nhìn thấy một xã hội Nhật ngoài những điều tốt đẹp cũng còn tồn đọng lại nhiều cái xấu. Thiệt hai từ chiến tranh, từ thiên tai đã đẩy con người vào bước trộm cướp, bốc lột lẫn nhau, sự hà khắc của những kẻ cầm quyền trong xã hội tần nhẫn ấy. 
Song song với đó là hình ảnh của một Yakuza hiện lên với sự sẻ chia chân thành làm mình bất ngờ. Một lớp người vẫn bị xã hội gán cho là kẻ lạnh lùng, tàn nhẫn nhưng vẫn sống với trái tim chân thành. Tình cảnh họ dành cho bạn bè, cho anh em, cho những tên cai ngục làm mình cảm thấy ấm lòng. Bên trong tận cùng trái tim con người luôn ẩn chứa những điều tốt đẹp.
Câu chuyện là một góc nhìn đầy chân thật và tinh tế về con người Nhật thời xưa, qua đó hiện lên hình ảnh Yakuza như một điểm sáng kỳ lạ. Quyển sách làm mình thay đổi cách nhìn nhận về con người rất nhiều. Thực sự có nhiều người "tốt gỗ hơn tốt nước sơn", không thể đánh giá học qua bề ngoài "thô ráp" đó!
4
303773
2015-02-06 22:42:26
--------------------------
155836
5665
Thật sự mình chỉ mới đọc 1/2 quyển sách này, chưa thể cảm thụ dc hết giá trị của nó, nhưng bấy nhiêu cũng đủ để mình phải chiêm nghiệm khá nhiều. Những hồi ức của ông Eiji dc viết qua những dòng ghi chép của tác giả đã giúp mình phần nào hình dung được xã hội Nhật Bản thời hậu chiến. Giữa một nước Nhật bị tàn phá bởi chiến tranh và thiên tai, con người ta dường như trở nên đánh mất chính mình, sẵn sàng lừa dối, phản bội nhau chỉ vì sinh tồn và lợi ích bản thân. Nhưng song song đó cũng có những người Nhật kiên cường, họ thà chịu đói, nằm chờ chết vì không ai cho họ việc làm còn hơn là phải sống nhờ vào miếng ăn của kẻ khác. Qua chân dung Eiji và những mảnh ghép trong cuộc đời sóng gió của ông, mình có được rất nhiều cảm xúc cho cả ông và những nhân vật trong tác phẩm: khinh bỉ, ghê sợ lối sống vô tâm của bọn cớm; cảm thông cho số phận của những cô gái làng chơi và người tử tù Kumazo; kính phục lòng trung thành, kiêu hãnh của Yakuza ngày xưa; và cả tiếc nuối cho những mối tình ngang qua đời Eiji. Đây thực sự là một quyển sách đáng đọc, hòa mình vào dòng xoáy cuộc đời của một ông trùm thế giới ngầm để cảm thụ những cung bậc cảm xúc khác nhau và một cái nhìn sống động về xã hội Nhật Bản xưa - nay.
5
57421
2015-02-02 11:52:42
--------------------------
138335
5665
Quyển sách này là bức tranh sống động về cuộc sống, xã hội Nhật Bản những năm đầu thế kỷ 20 và hình ảnh cuộc đời của các nhân vật được nhìn qua đôi mắt của một ông trùm Yakuza.
Từng người anh, người bạn, những cô gái bán hoa đều được khắc hoạ chi tiết. tất cả những câu chuyện đời ấy đều có cái quá khứ làm tiền đề, cái hiện tại cũng như cái kết thúc được kể chi tiết. Ông đã chọn cách mô tả thế giới xung quanh để kể câu chuyện về đời mình, cảm giác như với ông, đó không phải là cái gì quá đổi vinh quang, nhưng cũng chưa bao giờ ông nhận mình là thấp kém. 
Có lẽ những hình ảnh ông mô tả về cuộc sống sau song sắt là nhiều và đáng nhớ hơn cả. Đặc biệt nhất là cách mà ông cảm thông và đối xử với những cai ngục, sự chân thành, sẻ chia và cố gắng hết mình luôn được đền đáp xứng đáng dù lúc đó bạn ở nơi đâu.
Đọc quyển này để thấy nước Nhật ngày ấy khác bây giờ rất nhiều, cũng có trộm cướp, cũng có áp bức, cũng có bao nhiêu bất công, nhưng sau gần một thế kỷ, mọi thứ đổi thay. Đọc để thấy ngày xưa họ cũng thế, đọc để biết rằng ta cũng có thể làm được.
Không lãng phí cho một ngày cuối tuần với quyển sách này.
4
107173
2014-12-02 09:03:44
--------------------------
133491
5665
Từ bé mình đã có niềm hứng thú đặc biệt với văn hóa Nhật Bản, từ hoa đạo, trà đạo, manga,... - niềm kiêu hãnh của đất nước Mặt Trời Mọc đến những thứ (đôi lúc bị coi là) "xấu xí hơn" như Geisha, Yakuza,... Cuốn sách này đã hé mở cho mình phần nào về một thế giới ngầm hùng mạnh, cho mình một cái nhìn khác, đa chiều và sâu sắc hơn về số phận của những con người "sống trong bóng tối" ấy. Đọc xong, mình hiểu rằng đâu phải tất cả Yakuza sinh ra đã máu lạnh, xấu xa, họ cũng có những quá khứ riêng, những nỗi đau riêng, và hơn hết, họ vẫn có những phẩm chất đáng để người ta khâm phục. Yakuza có lạnh lùng, có tàn nhẫn nhưng thừa đủ lòng trung thành, sẵn sàng hi sinh vì đồng đội, đôi lúc họ bất cần nhưng nhìn chung vẫn luôn biết tuân thủ quy tắc, sống có kỷ luật; họ kiêu hãnh, mạnh mẽ, kiên cường, biết coi trọng chữ "tín",... Có đầy người không phải Yakuza nhưng chẳng làm được như họ. Giữa thời chiến tranh loạn lạc, con người bị cuốn vào vòng xoáy mưu sinh, dối gạt, lợi dụng lẫn nhau, mình hoàn toàn có thể thông cảm cho họ. Và hình ảnh Yamamoto trong mình càng rực sáng, đáng khâm phục và kính nể hơn bao giờ hết.
5
393748
2014-11-05 17:01:45
--------------------------
