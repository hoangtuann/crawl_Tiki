197301
4734
Cuốn sách tập trung phân tích về những nhà kinh doanh có đạo đức và công việc kinh doanh của họ luôn hướng về khách hàng và xã hội
Nhà lãnh đạo là thuyền trưởng của con tàu. Cuốn sách này sẽ cho các nhà lãnh đạo những lời khuyên vô giá qua 7 nguyên tắc giúp doanh nghiệp phát triển thịnh đạt.
Cuốn sách cũng giúp bạn có cái nhìn sâu sắc hơn về thế giới kinh doanh và khơi gợi trong bạn nguồn cảm hứng và động lực mạnh mẽ để tạo ra một doanh nghiệp vừa có lợi nhuận vừa có sự đồng cảm của khách hàng và xã hội.
3
280007
2015-05-17 12:18:01
--------------------------
