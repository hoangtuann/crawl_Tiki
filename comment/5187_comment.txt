217467
5187
Cuốn truyện chú bé người gỗ Pinocchio này giống như là một bản tóm tắt của cuốn truyện nguyên bản vậy. Mặc dù cuốn sách đẹp, dễ đọc nhưng mình cảm thấy không thoải mái lắm vì nó quá ngắn và nhiều chi tiết lướt qua khiến mình cảm thấy nội dung câu truyện thiếu sự gắn kết. Có một vài số chi tiết không giống trong nguyên bản như là nàng tiên vàng (Yellow Fairy trong cuốn sách này) và nàng tiên xanh (Blue Fairy trong truyện nguyên bản). Có vẻ như cuốn sách này được tóm tắt lại cho các em bé đọc thì phải! Nhưng mình vẫn thích bản đầy đủ và có hình đẹp như thế này hơn.
3
536026
2015-06-29 12:59:27
--------------------------
