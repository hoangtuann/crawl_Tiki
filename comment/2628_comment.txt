473868
2628
Mặc dù bé mình mới 4 tuổi nhưng lại rất thích tô vẽ. Vì vậy mình quyết định mua quyển này cho bé tập tô các nét cơ bản, làm quen với việc cầm bút, rèn thói quen cho bé. Bé rất thích thú khi được ngồi vào bàn tô tô vẽ vẽ nhưng lại rất nhanh chán. Quyển tập này giúp bé rèn các nét thẳng, ngang, xiên phải, xiên trái, cong trên, cong dưới, nét khuyết,... và nhiều nét khác. Đồng thời còn có các bức tranh để bé tập tô màu sau khi đã học mệt mỏi.
4
169332
2016-07-10 22:15:53
--------------------------
