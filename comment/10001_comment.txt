291964
10001
Bé nhà mình rất thích quyển sách này. Trước tiên là vì kênh hình nhiều hơn kênh chữ, màu sắc hài hòa, điều này giúp bé dễ dàng tiếp thu các nội dung của sách. Thứ hai là vì sách trang bị cho bé các kiến thức cực kì phong phú gồm nhiều lĩnh vực, không chỉ là kiến thức về tự nhiên, xã hội mà còn dạy bé một số kĩ năng như quan sát, kể chuyện, vẽ hình, phân biệt màu sắc, tư duy logic,... Điều này rất bổ ích cho các bé chuẩn bị bước vào lớp 1.
5
496573
2015-09-06 21:41:04
--------------------------
