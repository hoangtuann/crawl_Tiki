236624
10226
Thấy giới thiệu đây là cuốn sách đc tác giả viết cùng năm đạt giải Nobel nên mua về đọc. Có thể là tác phẩm đc viết khá lâu rồi nên ko có sự thu hút mạnh mẽ đối với tôi. Nội dung thì quả thực vài đoạn gây buồn ngủ nhưng giọng văn và cách hành văn thì quả thực tác giả là bậc thầy. Cuốn sách ko chỉ viết về cuộc phiêu lưu của tác giả cùng chú chó Charley quanh nước Mĩ mà còn nêu lên một số vấn đề vẫn còn tồn tại đến ngày nay tại nước Mĩ hoa lệ. Có một số đoạn tác giả miêu tả quá kĩ càng, mạch truyện chậm nên hơi gây nhàm chán.
3
267843
2015-07-21 21:49:25
--------------------------
220718
10226
Nội dung của tập tiểu thuyết khá ổn. Tôi có đọc nhiều thể loại tương tự như thế này và rất thích chúng. Tuy nhiên, cuốn sách này của Steinbeck khá rời rạc và lan man ở một số đoạn. Ông tuy rất giỏi trong việc ghi chép lại những cuộc phiêu lưu của mình, và đã làm điều đó rất tốt, nhưng có một số trường hợp ông chỉ toàn nói dông dài, trong khi vài trường hợp khác thì chi là ngắn gọn, đặc biệt ở những đoạn quan trọng. 
Khi mua và bắt đầu đọc quyển sách này, tôi đã hy vọng nhiều hơn, nhưng mọi thứ chỉ dừng lại ở đó.
3
1079
2015-07-02 21:50:51
--------------------------
197043
10226
Cuốn sách này khiến tôi nhớ đến "Giấc mơ Mỹ" (American's Dream) của rất nhiều người. Chúng ta từng nhìn nước Mỹ với sự hào nhoáng, văn minh... Những điều chúng ta biết về nước Mỹ không sai, nhưng mới chỉ là một phần của sự thật mà thôi. Qua tác phẩm này, Steinbeck đã cho tôi một cái nhìn hoàn toàn khác về nước Mỹ. Bên cạnh những con người hiếu khách, cảnh đẹp thiên nhiên hùng vĩ thì cũng có những mảng tối như nạn phân biệt chủng tộc, vấn đề ô nhiễm môi trường,... Đọc cuốn sách này khiến tôi hiểu hơn về lịch sử của nước Mỹ và nhận ra mình nên luôn có cái nhìn cân bằng, khách quan khi đánh giá sự việc, con người hay một quốc gia.
5
9242
2015-05-16 20:59:27
--------------------------
160418
10226
Là một quyển sách được viết trên 50 năm nhưng mình thấy nó vẫn phản ánh được nhiều thứ trong xã hội Mỹ ngày nay, hay thậm chí cả xã hội Việt đang dần Tây hóa ở một số nơi: vấn đề môi trường, vấn đề về quan hệ giữa người và người, chỉ mới vài năm thôi mà internet đã thống trị cuốc sống xung quanh ta cũng giống như radio & truyền hình đã thống trị cuộc sống của người Mỹ vào những năm 60. Hơn thế nữa, lối viết của tác giả rất gần gũi, như thể không phải viết để chỉ trích ai, mà chỉ để nói lên những tiếc nuối của mình về quá khứ đã qua. Rất hoài niệm và khắc khoải.

Mình rất thích giọng văn của bản dịch, giữ được phần lớn cách nói chuyện độc thoại tường thuật của tác giả, đôi khi truyền cả được những sự bực bội từ những chuyện trên đường. Nếu có dịp bạn hãy tìm đọc bản audio book của Tuổi Trẻ Mobile, rất truyền cảm và ấn tượng.
5
116202
2015-02-24 11:02:07
--------------------------
141207
10226
Tôi đã đọc cuốn sách này rất lâu, phần vì tôi không hay đọc sách, phần vì mạch truyện cũng không hấp dẫn. Cuốn sách quả có gợi lên một số suy nghĩ, nhưng cũng không có gì mới mẻ. Cuối cùng gấp sách lại tôi không hiểu tại sao nó lại cần dài và dầy như thế. Điểm tôi thích ở cuốn sách này đó là tôi tin nó nói thật. Tôi đã mua sách này sau khi đọc “Nơi dòng sông chảy qua” và “Chết ở Venice” thuộc tủ sách “Cánh cửa mở rộng”, nhưng sau khi đọc xong cảm thấy hơi mất niềm tin, các phần nhận xét – bình luận – giới thiệu trong mỗi cuốn đều rất hay nhưng có lẽ tôi không cảm nhận được hết cái hay giống như những người viết bình luận.
3
107944
2014-12-14 13:30:30
--------------------------
101269
10226
Quyển sách khá dày nhưng không hề gây nhàm chán, trái lại mỗi trang sách đều mang một điều gì đó mới mẻ, một triết lý giản đơn nhưng đáng suy ngẫm, một bài học về cuộc đời qua cái nhìn của con người tài năng, sâu sắc và đầy cá tính. Đã có những lúc ông nghi ngờ tính đúng đắn của cuộc hành trình khi nhận lãnh về mình sự thất vọng, cô đơn trên chính đất nước ông hằng yêu mến. Nhưng cái gì rồi cũng phải đổi thay, nước Mỹ thật sự, nơi ông gọi là nhà đã “không tồn tại nữa, chỉ còn lại trong những xó xỉnh của ký ức mà thôi”. Khó mà nói hết được những điều làm cho cuốn sách này trở thành kinh điển, cũng là do trình độ cảm thụ lẫn khác biệt về sự từng trải mà mình không thể thấy hết cái hay của nó, nhưng thật sự đây là một tuyệt phẩm, mình đã học được rất nhiều điều từ nó, gần như là cái nhìn toàn diện về nước Mỹ những năm 60. Cách tự sự lẫn miêu tả sự vật, hiện tượng của tác giả đều rất linh hoạt, khi thì hài hước, lúc lại trầm lặng, nhiều suy tư.

Tuy vậy có một lỗi nhỏ (có lẽ không phải do người dịch, mình nghĩ là nhầm lẫn trong đánh số các chú thích thôi), không nhớ ở trang nào nữa, từ “Sao Sọc” (Stars and stripes) được chú thích là chỉ cờ Anh, còn trang kế bên từ “Union Jack” lại chú thích là chỉ cờ Mỹ, trong khi theo mình biết thì phải đổi thứ tự với nhau mới đúng, dù phần sau các từ đó vẫn được dùng đúng nghĩa của chúng, nhầm lẫn này không quá nghiêm trọng nhưng sẽ gây ra cách hiểu sai cho những độc giả chưa biết.
5
63189
2013-12-03 14:54:36
--------------------------
75743
10226
Điều làm tôi thích cuốn sách này ngay từ khi chỉ đọc tóm tắt đó chính là bìa và độ dày, tôi thích những cuốn sách dày vừa phải, no sẽ có đầy đủ tình tiết, đầy đủ diễn biến mà không đảm bảo sự nhàm chán dư thừa, và cái bìa sách, tôi không biết có liên quan đến cuốn sách không nhưng nó quá đẹp, không quá màu mè, đơn giản.

Tôi thật sự ngưỡng mộ John Steinbeck, ông có cái đầu quá tài năng, tôi thích cách mà ông nghĩ ra chuyến hành trình của tác giả và Charly - một chú chó. Cuộc hành trình là những cái hoàn toàn mới, đầu tiên là sự can đảm đủ để thực hiện một chuyến hành trình đến những vùng đất mới của Mỹ chưa từng được biết qua, những con người mới, những cảnh trí mới,...

Một cuộc hành trình tuyệt vời bằng giọng văn tự nhiên nghiêng nhiều về nghệ thuật miêu tả, tự sự. Tôi thích cách mà tác giả đi sâ vào để nói về những vừng đất mới, cho thấy tâm trạng của tác giả được khắc họa một cách quá tài tình.

Một cuốn sách đáng để đọc, một cuộc hành trình đáng để ước mơ và thực hiện♥
5
104997
2013-05-19 17:52:16
--------------------------
72092
10226
Mình vốn là một đứa học chuyên anh, bạn bè dần dần đứa nào đứa nấy bay sang Mĩ, sang Anh du học rồi gửi về những tấm hình đẹp mê hồn khiến mình thèm không chịu được. Mình chỉ biết đến nước Mĩ hiện đại, nền giáo dục phát triển, nhà chọc trời  và những nhà khoa học danh tiếng, và trong đầu khi nào cũng có "American dream". Nhưng John Steinbeck đã mang lại cho mình một hình ảnh khác về nước Mĩ, về một nước Mĩ với mọi thứ đều được khử trùng, được đóng gói và hương vị chán chẳng khác gì nhau. Đọc những điều đó mình như bị dính bùa choáng, hết sức ngạc nhiên và tự hỏi mình thích một Việt Nam "tự nhiên" hay một nước Mĩ hào nhoáng? Đoạn cuối truyện là những hình ảnh nhức nhối về nạn phân biệt chủng tộc, những Cheer leaders không biết nên chỉ trích hay ngưỡng mộ như mọi người? Nhưng phải nói là nước Mĩ hiện lên cũng rất đẹp và hùng vĩ, với núi non và sông hồ hết sức yên bình :)
4
58432
2013-04-30 11:21:48
--------------------------
44356
10226
Mới đầu, tôi đã nghĩ rằng đây chỉ đơn giản là những ghi chép trên đường du lịch khắp nước Mỹ của tác giả và người bạn tên là Charley. Nhưng hóa ra, người bạn ấy là một chú chó lông xù có dòng dõi "quý tộc Pháp". Và những ghi chép ấy không chỉ đơn giản là quyển nhật kí hành trình mà là những suy nghĩ, trăn trở về cả một thế hệ người Mỹ mà đến tận bây giờ vẫn chưa hề cũ đi, dù là ở Mỹ hay ở bất cứ quốc gia nào khác. Đó là những trăn trở về sự yếu đuối của con người, những người luôn thèm khát được dịch chuyển nhưng lại quá sợ sự bất ổn định đó mà quyết định ở lại. Là những suy nghĩ về mục đích sống của giới trẻ, những người dường như chỉ còn biết đến sự hưởng thụ. Là những quan sát về tình trạng "quốc gia hóa" những ngôn ngữ địa phương, những mùi vị ẩm thực đặc trưng của mỗi vùng, thay vào đó là thứ ngôn ngữ sản xuất hàng loạt từ tivi, radio và kiểu bữa sáng đóng gói ở hầu như tất cả các trạm dừng chân. Nhưng tất nhiên, quyển sách trên hết vẫn là ghi chép trên đường du lịch. Ta sẽ bắt gặp những cảnh quan tuyệt đẹp mà dưới ngòi bút của tác giả còn trở nên độc đáo hơn bao giờ hết. Sự hài hước, hóm hỉnh, đặc biệt là những đoạn miêu tả Charley khiến cho quyển sách này trở nên thú vị hơn nữa. 

Mặc dù có những ý kiến cho rằng thực tế John Steinbeck đã không thực hiện "hành trình" nước Mỹ như những gì ông mô tả trong quyển sách, nhưng liệu điều đó có quan trọng không so với những giá trị mà nó mang lại?
5
2966
2012-11-02 09:33:30
--------------------------
32223
10226
Với một cuốn tiểu thuyết phiêu lưu tuyệt vời như Tôi, Charly và hành trình nước Mỹ thì bạn cũng có thể đi du lịch ngay tại nhà ! Vâng, John Steinbeck đã tạo nên một chuyến đi tuyệt vời cho mỗi độc giả.

Rong ruổi trên chiếc xe nhỏ có gắn thêm cabin, những nhân vật của chúng ta - tác giả và chú chó Charly -  trên một cuộc hành trình xuyên suốt nước Mỹ. Những vùng đất hoang vu trên lãnh thổ chính đất nước mình mà nhà văn chưa từng đặt chân tới, những phong cảnh hùng vĩ nhưng lại hết sức khắc nghiệt, những con người bản địa chân chất, đọng lại trong lòng độc giả chúng ta một cảm xúc khó tả. Thật tuyệt vời !

Bằng giọng kể "đặc trưng" chỉ có ở John Steinbeck, ông đã kể lại cho chúng ta nghe về một cuộc hành trình thật mà ông đã từng trải qua. Khi đọc tác phẩm này, ta có thể dễ dàng nhận thấy sự điêu luyện của nhà văn trong khả năng miêu tả, ngòi bút sắc sảo khắc họa tâm lý, cuộc đời của chính mình và những người bản địa ông gặp trên chuyến đi.

Có thể nói, đây là tác phẩm tuyệt vời nhất của John Steinbeck.
5
23953
2012-07-03 12:55:31
--------------------------
27587
10226
Mình hiện đang đọc quyển sách này, mặc dù chưa đọc xong nhưng cảm thấy rất thú vị. Không cần tốn tiền đi du lịch nhưng vẫn có được cái nhìn rất độc đáo về đất nước Mỹ thông qua cái nhìn sâu sắc của nhà văn John Steinbeck. Chỉ bằng những mẩu đối thoại nhỏ với những người dân bản địa, tác giả đã cho ta thấy sự đa dạng trong văn hóa cũng như suy nghĩa, quan niệm của người dân Mỹ.
Giọng văn lôi cuốn, ngòi bút miêu tả sinh động, sắc sảo, cảm xúc giản dị chân thực, tất cả làm nên một cuốn hồi kí hay nhất, đẹp nhất, và đáng yêu nhất từ trước đến nay.

5
24250
2012-05-20 08:30:43
--------------------------
26570
10226
"Tôi, Charley Và Hành Trình Nước Mỹ" là một cuốn sách đủ hấp dẫn để giữ rịt bạn trên chiếc giường thân yêu, say sưa và mê mẩn với từng nơi mà John Steinbeck đi đến - những nơi được ông cảm nhận và yêu thương bằng cả trái tim. Đây không phải đơn giản chỉ là một cuộc hành trình, mà thậm chí nó còn là một chuyến phiêu lưu, một sự trải nghiệm để cho bạn biết rằng "Bạn yêu đất nước bạn đến bao nhiêu" :") Đi đến từng vùng đất, có từng trải nghiệm khác nhau, và mỗi lần bánh xe tiếp tục lăn trên con đường trải dài là mỗi lần trái tim John Steinbeck đập lên một nhịp yêu thương với nước Mỹ. Giọng văn lôi cuốn, ngòi bút miêu tả sinh động, sắc sảo, cảm xúc giản dị chân thực, tất cả làm nên một cuốn hồi kí hay nhất, đẹp nhất, và đáng yêu nhất từ trước đến nay. 
5
38398
2012-05-10 13:53:20
--------------------------
24573
10226
Mình hiện đang đọc quyển sách này, mặc dù chưa đọc xong nhưng cảm thấy rất thú vị. Không cần tốn tiền đi du lịch nhưng vẫn có được cái nhìn rất độc đáo về đất nước Mỹ thông qua cái nhìn sâu sắc của nhà văn John Steinbeck. Chỉ bằng những mẩu đối thoại nhỏ với những người dân bản địa, tác giả đã cho ta thấy sự đa dạng trong văn hóa cũng như  suy nghĩa, quan niệm của người dân Mỹ.
Mình đặt biệt thích những câu văn sáng tạo và hấp dẫn của ông. Nhiều chi tiết hóm hỉnh một cách nhẹ nhàng và sâu sắc. Đây đúng là văn của một nhà văn đã từng đoạt giải Nobel. 
Mình khuyến nghị các bạn nên đọc quyển tiểu thuyết này để có thêm nhiều điều lý thú mới về nước Mỹ nha!
5
31897
2012-04-13 17:05:37
--------------------------
