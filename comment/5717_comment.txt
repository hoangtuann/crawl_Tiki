523173
5717
Mình mới nhận sách hôm nay nên chưa kịp đọc hết. Nhưng mình tin tưởng vào bản dịch của Chibooks. Điều làm mình thất vọng là chất liệu giấy. So với phần 5 mình từng đặt, thì sách chất giấy có phần mỏng. Mình đọc cảm thấy bị rối mắt khi chữ trang sau in lên trang trước. 
3
740304
2017-02-11 01:58:05
--------------------------
493870
5717
Quyển sách cho ta biết thêm về tất cả các vị thần trên đỉnh Olympia. Thật là đáng để mua nó.
4
915414
2016-12-03 10:50:41
--------------------------
448448
5717
Một quyển off series đậm chất Rick Riordan!
Không cầu kỳ, không có những trận đánh, chỉ đơn giản là kể lại thần thoại qua lăng kính hài hước và dí dỏm của một cậu bé tuổi teen thời hiện đại. Những câu chuyện tưởng như đã quen thuộc lại trở nên lạ lẫm, mới mẻ và hài hước. Đọc thần thoại Hy Lạp nhiều lúc thấy các thần quá phi lí, nhưng cuốn sách này đã làm cho những điều phi lí trở nên hợp lí, đồng thời khắc họa rõ nét chân dung từng vị thần chỉ Percy Jackson mới có. Dù đã đọc series Percy Jackson hay chưa, bạn đều có thể bị lôi cuốn bởi quyển sách này.
4
1335201
2016-06-15 23:15:35
--------------------------
445227
5717
Bộ truyện Percy Jackson rất hay và mình cảm thấy không tiếc chút nào khi bỏ tiền rs mua nó.Dưới ngòi bút của tác giả, tác giả đã phác họa nên nội dung câu truyện đầy màu sắc hơn. Cứ ngỡ các vị thần Hy Lạp sống thời xa xưa nhưng hiện tại họ vẫn sống và sống rất hợp thời. Những vị thần này cực kì dí dỏm, hài hước, sống cuộc sống như một con người: cũng có xe hơi, nhà cửa, vợ con...Cốt truyện còn xoay quanh những cuộc phiêu lưu kì thú của Percy Jackson nữa
4
900397
2016-06-09 19:50:58
--------------------------
405955
5717
Thực sự vậy. Dưới ngòi bút của tác giả, những câu chuyện bắt nguồn của các các thần, của thế giới.. được diễn tả một cách..buồn cười hơn hẳn. Một số chi tiết cũ, một số khác được thêm vào, thỉnh thoảng lại có nhận xét của Percy Jackson làm cuốn sách trở nên thật thú vị. 

Cách kể chuyện này cũng làm các vị thần trở nên gần gũi hơn. Có thể họ mang trong mình sức mạnh thần thánh thật đấy, nhưng những thói xấu, tính cách của con người thì cũng chả thiếu. Chết..mong các vị thần bỏ qua :3

Về mặt hình thức thì mình không có ý kiến gi chê trách cả.:)
5
475622
2016-03-27 15:07:25
--------------------------
381236
5717
Mình đã đắn đo rất nhiều trước khi mua cuốn sách này, vì cho rằng đây chỉ là phụ lục của Percy Jackson, bộ Percy Jackson như thế đã là đủ. Nhưng nhân dịp Tiki nhập hàng cuốn này về lại mình đặt mua và không hề hối hận.
Rick Riordan vô cùng sáng tạo khi kể lại thần thoại Hy Lạp với ngôn ngữ riêng, giọng điệu của anh chàng Percy Jackson cùng những yếu tố hiện đại, những câu thoại ngộ nghĩnh khiến thần thoại tưởng như khô khan lại trở nên dí dỏm, hài hước. 
Những vị thần Hy Lạp gần gũi hơn và thần thoại Hy Lạp cũng hấp dẫn và dễ tiếp thu hơn nữa.
5
477889
2016-02-16 21:00:39
--------------------------
336779
5717
Nếu ai là fan của series Percy Jackson thì không thể không mua cuốn này. Với những nội dung cũ về thần thoại hi lạp khô khan, khó hiểu thì cuốn sách này lại hoàn toàn ngược lại, với tài năng của mình, bác Rick đã rất khéo léo lồng ghép sự hài hước, dí dỏm vào từng trang sách làm câu chuyện về các vị thần trở nên vô cùng sinh động và thú vị. Chỉ tiếc là bìa sách không đẹp, quá tối so với trên hình. Nhưng đó chỉ là một khuyết điểm nhỏ. Nói chung truyện vẫn tuyệt vời.
5
477880
2015-11-12 22:06:07
--------------------------
298757
5717
Mình rất thích đọc các tác phẩm về thần thoại Hy Lạp. Nghe bạn bè giới thiệu nói cuốn này hoàn toàn khác với các bản Thần thoại Hy Lạp khác. Đó chính là cách hành văn, phong cách viết văn hài hước, lối kể chuyện rất mới và lôi cuốn người đọc của tác giả. Trong vai kể chuyện của cậu bé Percy tinh nghịch, tính cách nhí nhố, tác giả đưa người đọc đến một thế giới của thần thoại Hy Lạp trong một phiên bản mới, khác lạ mà không hề thiếu thông tin hay mất chất của thần thoại. Nhìn chung đây là một cuốn sách thú vị cho các bạn yêu thích và khám phá thêm về Hy Lạp. :D
5
81795
2015-09-12 21:01:47
--------------------------
297313
5717
Trước đây mình có từng đọc một ít trang đầu của Thần thọai Hy lạp nhưng thật tiếc là nó quá dài và mình không biết phải bắt đầu từ đâu cả nên đã bỏ dở. Nhưng thật may rằng trong cách kể chuyện của Rick Riordan, cuốn sách đã mang lại một hiệu ứng hài hước, lôi cuốn để mình hiểu hơn về Thần thoại Hy lạp. Các Vị thần không còn trở nên xa lạ, trừu tượng và thuộc về thế lực siêu nhiên nào đó mà gần gũi với cuộc sống đời thường của chúng ta. Đọc mà có cảm giác một ngày nào đó mình cũng sẽ trở thành thần tiên, thành siêu anh hùng được nữa :)) Rất hay.
5
91802
2015-09-11 19:24:00
--------------------------
287125
5717
Mình đã tình cờ đọc được bộ Percy Jackson và bị hút hồn vào cuộc phiêu lưu của cậu cũng như ngòi bút tươi mới của Rick Rithần Hyordan. Mình cũng gần như là 1 fan của Thần thoại Hy Lạp. Mình đã đọc qua rất nhiều bản kể lại, cả thi ca lẫn bản tóm tắt lại, nhưng phần lớn những bản đó rất nghiêm túc và và mang phong cách mô phạm; người lớn đọc thì được chứ trẻ em đọc sẽ buồn ngủ. Nhưng khi đọc quyển "Các vị thần Hy Lạp của Percy Jackson", mình đã thực sự cười không ngớt với lối kể chuyện rất mới, rất thú vị và hoàn toàn mang đậm dấu ấn ngòi bút của Rick.
Trong tác phẩm, những câu chuyện thần thoại của Hy Lạp vốn rất nghiêm túc trở nên dễ chịu hơn, dễ tiếp cận hơn và dễ tiếp thu hơn. Những câu chuyện được tác giả viết với giọng kể của Percy- cậu bé nổi tiếng với tính cách nhí nhố, bất cần, hơi ngố nhưng đôi khi nghiêm túc và sâu sắc không ngờ. Những nhân vật trong thần thoại cũng được nhân hoá, những đoạn hội thoại có chiều sâu và gần gũi với người hiện đại hơn, và những hành động thì logic và dễ hiểu hơn. Nhưng không vì vậy mà tác phẩm làm mất đi chất thần thoại lẫn những chi tiết trong những bản nguyên gốc. Khi đọc "Các vị thần Hy Lạp của P`ercy Ja  
4
258176
2015-09-02 12:15:22
--------------------------
274529
5717
Với các quốc gia khác nhau đều có những cách lý giải hay những câu truyện thần thoại giải thích cho những sự kiên thiên nhiên hay những bí ẩn mà con người thời xưa không thể khám phá thì thần thoại Hy Lạp là một trong những truyện thần thoại hấp dẫn và có ảnh hưởng lớn hất trên thế giới với sự phổ quát của nó hay các ảnh hưởng của nó.
Với lối viết hấp dẫ tác giả đưa ra đầy đủ những thông tin chi tiết về các vị thần Hy Lạp một cách sống động đầy cuốn hút khi đọc.
Quả thật quyển sách đã mang lại khá nhiều kiến thức về các vị thần Hy Lạp cho mình, nâng cao một phần hiểu biết hơn nữa về thần thoại hay của thế giới.
4
105692
2015-08-21 23:30:29
--------------------------
261385
5717
"Thần thoại Hi Lạp" là một tựa sách mà rất rất nhiều người đã đọc, thậm chí là đọc rất nhiều lần. Những vị thần, những vấn đề về văn hóa, lịch sử quyển sách này đem lại thật sự bổ ích và dường như không thể xóa mờ trong trí người đọc. Nhưng Rick Riordan với tác phẩm  "Các vị thần Hy Lạp của Percy Jackson" mà tôi vẫn thường gọi là "Thần thoại Hi Lạp phiên bản troll" đã mang lại những cảm giác thật mới mẻ với những nội dung mà tôi đã quen thuộc từ trước. Cách hành văn hài hước đầy sáng tạo và đặc biệt ông đã xóa bỏ góc nhìn xưa cũ với các vị thần tối cao mà nhìn với góc nhìn rất thực tế của những con người bình thường với những vị thần không hề mẫu mực. Điều đó đã khiến thần thoại Hi Lạp trở nên gần gũi và hiện đại. Cảm ơn ông!
5
55985
2015-08-11 07:49:26
--------------------------
258072
5717
Không giống như những cuốn sách viết về các vị thần Hi Lạp trước kia khô khan, khó đọc, Rick Riordan đã mang cho chúng ta một cuốn sách rất hài hước, lôi cuốn và dễ hiểu về thần thoại Hi Lạp. Các Vị thần đã được phát họa nên từ cách nhìn rất hóm hỉnh, gần gũi với cuộc sống đời thường của chúng ta, rất con người chứ không nghiêm nghị thần thánh hóa. Thật sự Rick Riordan đã cho chúng ta một cách nhìn mới thú vị hơn về các vị Thần, khác rất nhiều về những gì bạn đã biết trước kia. Một cuốn sách phù hợp cho mọi lứa tuổi.
5
310872
2015-08-08 10:38:33
--------------------------
241429
5717
Bác Rick đã kể lại thần thoại Hy Lạp mà cụ thể là câu chuyện về các vị thần theo lăng kính hiện đại, phán xét theo thời đại chúng ta, thì rất thiếu văn minh. Tuy nhiên bác đã viết theo phong cách hài hước, châm biếm để làm người đọc không cảm thấy ngột ngạt, mà trái lại, còn được vui vẻ và cười sảng khoái. Các vị thần không phải là oai nghi và gương mẫu, trái lại như con người, rất gần gũi và thân thuộc. Mình đọc mà cảm giác đó không còn là các vị thần hô mưa gọi gió nữa, đó là đời sống của chúng ta, với dưới lăng kính hài hước....
5
333860
2015-07-25 14:27:05
--------------------------
230903
5717
Mình rất thích những câu chuyện thần thoại.Đây là phần tiếp theo trong series  “Các anh hùng của đỉnh Olympus”. Thật sự đây là một tác phẩm hay phù hợp với mọi lứa tuổi, đặc biệt là trẻ em. Những câu chuyện thần thoại nổi tiếng, những chiến tích của các anh hùng và của các vị thần trên đỉnh Olympus được tác giả kể lại với phong cách dí dỏm khiến những câu chuyện trở nên thú vị và hấp dẫn hơn.Qua tài năng kể chuyện lôi cuốn của tác giả Rick Riordan các bạn sẽ có những trải nghiệm thú vị và không thể nào quên.
5
483530
2015-07-17 20:18:06
--------------------------
218819
5717
Chỉ đọc bộ Percy Jackson thì cũng chưa thấy hết được khả năng sáng tạo và máu hài hước của bác Rick. Đây là cuốn sách viết về các sự kiện trong thần thoại Hy Lạp và giúp cho chúng ta tiếp cận với thế giới thần thành của dân Hy Lạp trước kia.. Mình đã đọc qua thần thoại Hy Lạp và thật sự cuốn sách Các Vị Thần Hy Lạp Của Percy Jackson hấp dẫn hơn 100 lần!!! Cách viết làm mình cười đau bụng, từ chuyện vợ chồng nhà Gaia đến thói lăng nhăng của Zeus đều được tác giả dùng những liên tưởng với cuộc sống hiện đại  làm mình cứ lo nếu có các vị thần chắc bác Rick đã mắc tội cực kì lớn với họ mất!
4
180112
2015-06-30 21:45:45
--------------------------
217549
5717
Không chỉ mang đến yếu tối phiêu lưu, huyền bí, Các Vị Thần Hy Lạp Của Percy Jackson cho mình biết được nhiều kiến thức về thần thoại hy lạp, hiểu về từng vị thần một, hiểu về tính cách của từng vị thần khiến việc đọc và cảm nhận các tác phẩm thần thoại khác tốt hơn. Bên cạnh đó, yếu tố gây cười mới chính là cái Tuyệt của phần này. Mỗi vị thần qua lời kể của nhân vật như hiện ra trong đầu mình với biết bao là tình huống sốc :v đọc mà chỉ biết ngồi tưởng tượng và cười thôi :)))
Thật sự rất hay, nếu bạn đã đọc các truyện của Rick Riordan  rồi thì phần này không thể không đọc :v 
5
37970
2015-06-29 15:33:58
--------------------------
213678
5717
Có thể nói, bên cạnh J.K.Rowling thì bác Rick là người đem lại cho mình nhiều thời gian thư giãn nhất. Bên cạnh những câu chuyện hồi hộp về chuyến phiêu lưu của Percy, còn có những tình tiết nhẹ nhàng gây cười. Từng câu chuyện về từng vị thần tưởng chừng người đọc sẽ bị ngắt mạch cảm xúc nhưng không, những câu chuyện ấy liên kết với nhau thành một mạch. Quyển sách rất phù hợp với ai muốn biết thêm vthần thoại Hi Lạp nói chung. Mình rất ấn tượng về quyển sách này. Chỉ có một vài lỗi nhỏ sai chính tả thôi, nxb cần khắc phục việc này.
4
377097
2015-06-24 00:51:57
--------------------------
212825
5717
Phải nói là tôi vô cùng yêu thích tác giả Rick Riordan này.Cách kể chuyện vô cùng thú vị. Tôi đã từng đọc các câu chuyện về các vị thần Hy Lạp nhưng qua ngòi bút của bác Rick thì lại càng tuyệt vời hơn nữa. 
Cuốn sách này kể về các vị thần với nhiều tình huống hài hước khó mà có thể tưởng tượng được một vị thần oai nghi trong một tình huống như vậy. haha.
Cách miêu tả nhân vật lại vô cùng khéo léo, dí dỏm nhưng vẫn có phong thái của một vị thần.
Bản thân tôi vốn yêu thích thần biển, bây giờ thì lại càng thích hơn
Tuy nhiên vẫn còn nhiều lỗi chính tả, đôi này đôi lúc gây khó chịu trong khi đọc, một tác phẩm hay như thế mà lại vất phải những điểm khúc. hi vọng khi tái bản sẽ được biên tập tốt hơn. Sao cuối cùng thuộc về lỗi biên tập, thật sự đây là một tác phẩm tuyệt vời, không nên bỏ lỡ
4
288731
2015-06-22 22:21:03
--------------------------
159997
5717
Tôi là fan của bác Rick và cô Row, hai người này luôn có cách kể truyện khiến độc giả không hề thấy nhàm chán. Truyện nào của bác Rick cũng vừa khiến độc giả biết thêm về các thần thoại vừa hài hước.

Có ai lại nghĩ ra được như thế này không? 

"TẠI SAO ZEUS LUÔN LUÔN ĐỨNG ĐẦU? Thực sự, mỗi cuốn sách về các vị thần Hy Lạp đều phải bắt đầu với vị thần này. Chúng ta đang đảo ngược thứ tự bảng chữ cái à? Tôi biết ông ấy là vua của Olympus và tất cả - nhưng hãy tin tôi, cái tôi của vị thần này không cần lớn hơn nữa đâu.
Bạn biết gì không? Quên ông ta đi.
Chúng ta sẽ nói về các vị thần theo thứ tự họ được sinh ra, các quý bà trước. Hãy ngồi ghế sau, Zeus. Chúng ta đang bắt đầu với Hestia."

Thật sự chỉ có bác Rick mới làm được điều ấy. Một tác giả tuyệt vời.

Hài hước, hấp dẫn, khó bỏ qua. Đó là ba tính từ tôi dành cho cuốn sách này.

Còn về hình thức thì tôi có đôi lời gửi nhà xuất bản: Chibooks vẫn mắc lỗi biên tập vì vẫn còn lỗi chính tả (tuy ít hơn so với cuốn dấu hiệu Athena bản in lần đầu), chúng như những hạt sạn trong tô cơm vậy. Còn nữa, tại sao NXB không lấy bìa gốc hoặc design giống loạt bìa Percy tái bản 2014 nhỉ? Chúng đẹp thôi rồi, và như vậy sẽ đồng bộ nữa (giống bộ BNS nhà Kane cũng có mỗi cuốn 1 là lạc loài). Bìa mới của Chibooks thật sự khiến mình lắc đầu ngán ngẩm. Mong Chibooks sẽ xem xét việc tái bản với diện mạo mới cho cuốn sách.
4
89033
2015-02-21 09:31:49
--------------------------
154461
5717
Nếu như bạn muốn có cả bộ truyện "Percy Jackson và các vị thần trên đỉnh Olympus" thì nhất định không được bỏ qua cuốn sách này.
Rick Riordan đã kể lại thần thoại Hy Lạp theo cái cách hài hước nhất mà bạn có thể tưởng tượng ra. Rất nhiều những tình huống hài hước cùng cách miêu tả nhân vật dí dỏm, làm tăng sự hấp dẫn cho cuốn sách, rất dễ đọc, dễ hiểu và tuyệt vời.
Tác giả còn khéo léo thêm thắt vài câu để giới thiệu phần 2 của bộ truyện là " Các Anh Hùng Của Đỉnh Olympus ". Cuốn sách lần này đã được nhà xuất bản biên soạn cẩn thận hơn những cuốn trước nên ít lỗi hơn hẳn.
Tóm lại đây thật sự là một cuốn sách tuyệt vời mà bạn không thẻ bỏ lỡ.
4
418341
2015-01-29 12:00:28
--------------------------
148136
5717
Mình cứ vừa đọc sách vừa phá ra cười :) 
Với nội dung cũ là về Huyền thoại Hy Lạp, thường thì khô khan và khá khó để nhớ được hết tên các nhân vật trong đó, nhưng bác Rick đã kết hợp thêm rất nhiều những tình huống hài hước cùng cách miêu tả nhân vật dí dỏm, làm tăng sự hấp dẫn cho cuốn sách, rất dễ đọc, dễ hiểu và nếu bạn là fan của 2 bộ sách Percy Jackson thì không nên bỏ qua. Dịch giả cuốn này rất có phong cách trong diễn giải  từ ngữ nữa, khá hơn 2 bộ kia rất nhiều.
Nhưng có một vài điều mình muốn lưu ý một chút:
- Mình không nghĩ đây là phần 6 của bộ PJO, vì có một số chi tiết lại nói sang bộ thứ 2 (HoO) mất rồi
- Bìa sách (không biết có phải chỉ có sách của mình không) tối và đậm hơn trên hình nên bìa gần như một màu đen sì vậy
- Ở gần cuối có 1 lỗi bị đánh máy sai hoặc là sai chính tả gì đó. Có thể bỏ qua vì lỗi ít thôi, nhưng trong lúc đọc mình vẫn thấy hơi khó chịu vì đọc đoạn đầu tưởng cuốn này được Chibooks làm khá khẩm hơn mấy cuốn kia được nhiều rồi chứ, thấy hụt hẫng kinh luôn
- Phần làm mình không hài lòng nhất là dịch giả có vẻ như chưa tìm hiểu về 2 bộ còn lại nên khi dịch ở đây về tình huống hay nhân vật trong 2 bộ đó, dịch bị sai mất một số chỗ. VD như "anh bạn Piper", mà đáng nhẽ ra Piper là con gái
Nhưng nói túm lại thì đây vẫn là cuốn sách rất rất rất đáng đọc, cực kì có ích để các bạn thư giãn một chút sau một ngày học tập mệt mỏi :) Mình cho 4 sao vậy...
4
282245
2015-01-09 21:28:03
--------------------------
148106
5717
Ngay lúc cầm trên tay cuốn sách này, tôi đã nghĩ rằng cuốn sách này chắc là không có gì khác so với những thần thoại tôi đã đọc trước đây. Nhưng tôi đã nhầm. Bằng một phong cách dí dỏm quen thuộc, Rick Riordan đã kể lại thần thoại Hy Lạp theo cái cách hài hước nhất mà bạn có thể tưởng tượng ra. Thần Zeus vĩ đại là một người bưng cốc, thích kể chuyện tiếu lâm và nhảy múa ư? Hades đáng sợ lại là một người nhút nhát và giàu tình cảm sao? Bạn sẽ đọc được những mẩu chuyện như vậy trong cuốn sách này. Đừng lo rằng hình ảnh các vị thần không giống như bạn tưởng tượng trước đây, vì cuốn sách này chỉ giúp bạn có thêm cái nhìn khác về họ mà thôi. Cuốn sách cuối cùng thật tuyệt vời.
5
291067
2015-01-09 20:41:13
--------------------------
