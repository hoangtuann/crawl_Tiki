231389
11776
Cuốn sách là một tập hợp các bài giảng, lời khuyên hữu ích, thậm chí cả pháp hành của các đạo sư Mật tông - Tây Tạng - dành cho các Phật tử và những bạn đọc muốn tìm hiểu thêm về Phật pháp. Đặc biệt, phần cuối sách có Những bài kệ kính lễ chư Phật và Bồ Tát viết bằng phiên âm Tạng và tiếng Việt, dễ đọc. 
Tuy nhiên với những người mới bắt đầu thì sẽ khó có được một hệ thống khái niệm và cách thức thực hành tuần tự. Bên cạnh đó, việc đề tên sách là "Đi vào cửa pháp" - Jetsun Milarepa dễ khiến bạn đọc nhầm lẫn rằng các bài viết đều của vị đạo sư nêu tên hoặc đây là tác giả cuốn sách. Thực tế thì đây là tuyển tập giáo huấn của các đạo sư Tây Tạng được Liên Hoa và Thanh Liên biên dịch.
4
44379
2015-07-18 00:22:37
--------------------------
