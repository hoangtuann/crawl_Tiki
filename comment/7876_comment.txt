375008
7876
Tôi mua sách cho con trai hai tuổi. Sách đẹp nhưng có nhược điểm nhìn hơi cũ, không rõ do Tiki bảo quản không tốt hay do nguyên nhân gì khác. Nhận xét chi tiết:
- Sách khổ to, bìa cứng khá đẹp (nhưng nhìn cảm giác hơi cũ, có thể do chất liệu giấy bìa dễ bị mòn)
- Sách nhiều chữ, hình vẽ hơi bị lặp lại những người bạn trong khu rừng (tôi thích hình vẽ có nhiều chi tiết nhỏ để chỉ cho con trai nhưng sách ko đáp ứng đc)
- Sách thiên về nội dung, nhiều chữ. Phù hợp cho các cháu lớn tuổi hơn bé nhà tôi (chắc phải trên 5 tuổi, khi mức độ tập trung tốt hơn)

4
1046240
2016-01-27 16:33:38
--------------------------
