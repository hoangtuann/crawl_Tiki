469802
3355
Chỉ vì sự cả nể của bản thân được mệnh danh là lịch sự tối thiểu của một con người. ( chẳng hạn như bị làm phiền cũng không dám lên tiếng mà đành nín nhịn )
Vì sự chân thành của mỗi chúng ta luôn mang theo bên mình để đem ra giúp đỡ người khác được mệnh danh là lòng tốt. ( người khác đòi hỏi được giúp đỡ cũng không dám từ chối, dù bản thân không thấy mình ở cảm giác tự nguyện mà chỉ thấy bị ép buộc)
Vì sự tôn trọng mỗi người xung quanh ta cũng đang được mệnh danh theo lòng biết ơn. ( dù có bị xúc phạm chúng ta cũng không lên tiếng bảo vệ bản thân, tự chịu dựng)
Nếu bạn đang trong hoàn cảnh như vậy nên đọc cuốn sách này, nó đã đem tới cho tôi thấy giới hạn là gì với những người xung quanh và cả bản thân tôi.
Tôi đã vượt qua cảm giác bị người khác lợi dụng.....
4
420322
2016-07-06 20:37:41
--------------------------
417779
3355
Ngay khi nhận được sách này ,mình rất hài lòng. Mỗi trang sách giúp mình nắm bắt và hiểu tâm lí người khác hơn. Sách dạy mình biết cân bằng các mối quan hệ trong cuộc sống, nhờ đó tâm hồn bạn thấy thoải mái hơn. Nó dẫn dắt mình qua từng trang sách, hấp dẫn từ đầu đến cuối sách. Bìa sách thiết kế đẹp,dày, ấn tượng. Chất liệu giấy tốt, bổ ích cho mọi người nhất là những ai đang đi làm hoặc có gia đình, sẽ giúp họ cân bằng mọi thứ từ công việc đến gia đình, cách giáo dục con cái.
5
619956
2016-04-17 20:23:12
--------------------------
385062
3355
Trong khi tìm kiếm những cuốn sách để rèn luyện tư duy, tôi đã tìm thấy Vạch ranh giới.
Quả thực, nói lời từ chối chưa bao giờ đơn giản, nhất là những người mình tin yêu. Khi tôi đọc câu chuyện phần mở đầu sách, về nhân vật Linda, tôi cảm thấy cô ấy quả thật là ngốc nghếch,, tại sao lại đồng ý giúp hết người này đến người khác cho dù cô ấy không hề có thời gian cho mình và thậm chí không còn thời gian để làm những việc chính ra cô ấy phải làm trước. Nhưng nếu đặt mình vào vị trí của cô ấy, hoặc bản thân tôi khi gặp phải hoàn cảnh bạn bè hay người thân của mình nhờ vả, tôi cũng rất nhiều lúc tặc lưỡi cho qua mà không hề suy xét xem mình có đủ thời gian hay không.
Quyển sách này đã giúp tôi hiểu đâu là ranh giới của mình, và khi nào thì mình nên nói Không.
4
358928
2016-02-23 14:28:32
--------------------------
383719
3355
Vạch ranh giới là quyển sách rất hay. Vạch ranh giới mang đến cho người đọc biết hơn về cách ranh giới giữa xấu và tốt, giữa người lừa đảo và người lương thiện. Sách dễ đọc dễ hiểu. Và rất cuốn hút, lôi cuốn mình từ cái tựa, lời mở đầu, những dòng đầu sách cho đến những dong cuốn của quyển sách. Mình rất thich! Mình cảm ơn tiki rất nhiều với chế độ chăm sóc khách hàng tốt, tiện lợi mua hàng rất dễ dàng. Cảm ơn tác giả đã mang đến cho người đọc quyển sách rất  hay và ý nghĩa
4
853243
2016-02-21 09:16:51
--------------------------
333549
3355
Tôi đã đọc tới trang thứ 300 của quyển sách và chưa bao giờ có suy nghĩ sẽ bỏ nó xuống.
Từng trang, từ chữ của quyển sách là một bài học đáng quý cho cuộc sống của chúng ta khi mà mọi thứ trôi quá nhanh. Cuốn sách dạy ta biết cách cân bằng giữa ranh giới của mình và của người khác có như vậy ta mới tạo ra được sự thoải mái trong tâm hồn và cuộc sống của chúng ta. Biết nói không với những điều không phải  là bổn phận và nghĩa vụ của mình. Cuốn sách này dạy ta làm sao để từ chối mà ta luôn cảm thấy thoải mái với những điều đó.
Một khía cạnh hay nữa của cuốn sách này mà tôi cảm nhận được đó là cuốn sách này rất phù hợp cho những ai đang có con. Đó là cách dạy con tự chịu trách nhiệm về cuộc đời của chính mình, có những việc cha mẹ làm cứ nghĩ sẽ tốt cho con nhưng đó là những điều hại con trong tương lai. Cuốn sách lý giải được tất cả những nỗi lòng của bậc cha mẹ và cho các bậc cha mẹ cách dạy con đúng đắn hơn tạo một nền tảng tốt cho con bước vào đời.
5
345818
2015-11-08 00:24:50
--------------------------
327356
3355
Cuốn sách chứa đựng những điều rất thực tế về ranh giới, vô hình nhưng hữu hình. Đây là một chủ đề không mới với mình nhưng cách tiếp cận vấn đề thì lại khá hấp dẫn. Khi mới đọc thì mình hơi khó chịu vì nói đến kinh thánh nhiều quá, nhưng mình chưa tìm hiểu về kinh thánh bao giờ nên đây là dịp để hiểu biết thêm. Là người theo Phật giáo và mình nghĩ dù bạn theo tôn giáo nào thì cũng nên mở lòng thử đón nhận những quan điểm của tôn giáo khác. Sự song hành của tôn giáo trong tác phẩm cũng là điều đặc biệt nhất của tác phẩm này. Nhân đây mình cũng xin chia sẻ một câu nói mình tự “bịa” từ vài năm trước: “Chính sự giới hạn sẽ phá vỡ những giới hạn”. Hãy vạch ra những ranh giới đúng đắn và kiểm soát cuộc đời bạn, thành công hơn và hạnh phúc hơn.
4
39590
2015-10-27 18:01:55
--------------------------
298382
3355
Khi duyệt sách tìm thấy quyển này, tôi đã nghĩ chắc là một cuốn sách sáo rỗng nữa đây, và tôi đã bỏ qua nhiều lần khi bắt gặp nó ở các trang web cũng như khi đi nhà sách, nhưng vì gặp và bỏ qua quá nhiều lần nên có lẽ tôi đã bị nó ám ảnh, và có một ngày tôi quyết định xem thử cuốn sách này như thế nào?
Tôi chỉ biết thốt lên là có quá nhiều thứ mới mẻ để đọc, trong cuộc sống tôi tưởng chừng việc vạch ranh giới là không cần thiết, bởi vậy cả cuộc đời tôi cứ như vậy trôi qua không có cái gì mang dấu ấn và ý nghĩa, nhiều khi khiến tôi mệt mỏi. Nhưng cuốn sách này đã giúp tôi nhận biết được nguyên nhân dẫn đến cuộc đời đó của tôi. Cuốn sách được viết và phân định rạch ròi rất nhiều trường hợp về gia đình, bạn bè, ... với rất nhiều lời khuyên thiết thực.
5
74132
2015-09-12 17:55:13
--------------------------
230500
3355
Nghe cái tên tựa sách là đã muốn đọc ngay rồi, những nội dung rất thiết thực gần gũi đời thường, vì vậy dễ hiểu. Đọc cuốn vạch ranh giới phần nào hiểu và nhìn thấu tháo hơn về tâm lý con người, nhất là người đối diện, để giúp nhận định ban đầu phân biệt được người tốt và người xấu.
Xã hội ngày nay có quá nhiều kẻ lừa đảo, chuyên dùng các chiêu trò, bán đứng lương tâm để đi lừa gạt những con người lương thiện. Vì vậy, nếu là người lương thiện thì cần phải tìm hiểu thêm về cái chưa lương thiện của cuộc đời này nhằm tránh phải những va vấp không đáng có.

5
471141
2015-07-17 15:14:06
--------------------------
