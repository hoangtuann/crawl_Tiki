445590
11217
Tập truyện này đưa mỗi chúng ta không chỉ về với tuổi thơ gắn bó với Doraemon mà còn gợi cả về những món đồ chơi thú nhồi bông chúng ta đặc biệt mê mẩn nữa. Nét vẽ của tập truyện này có phần hơi khác so với 16 tập truyện trước nên mình dù rất muốn nhưng vẫn chỉ có thể cho được đến tối đa là 4 sao mà thôi à. Nhưng đổi lại, cuộc đấu tranh bảo vệ công lý, hòa bình trong thành phố dây cót rất chân thực, như khắc họa lại một phần hiện thực đời sống ấy. Mình rất thích.
4
1420970
2016-06-10 15:31:46
--------------------------
396499
11217
Mình rất thích bộ truyện tranh "Doraemon" của tác giả người Nhật Bản Fujio Fujiko. Bởi lẽ bộ truyện này dạy cho ta rất nhiều điều bổ ích như: lòng dũng cảm, tình yêu thương con người, tình bạn cao đẹp giữa Nobita và Doraemon quá đỗi ngây thơ, hồn nhiên. Đọc "Doraemon" chúng ta còn rút ra được nhiều bài học đáng suy ngẫm. Thực sự là những chuyến phiêu lưu của Nobita và nhóm bạn của cậu ấy rất cuốn hút mình vì nó rất thú vị và hấp dẫn. Tập này sẽ đưa các bạn đến xứ sở dây cót.
5
1142307
2016-03-13 16:29:31
--------------------------
360129
11217
Tuổi thơ tôi đã từng đọc cuốn sách này. Tôi như nhớ về quá khứ với những ước mơ, những khát khao trước những món đồ chơi nhỏ bé. Đặc biệt khi đến với Doraemon truyện dài "Nobita và cuộc phiêu lưu ở thành phố dây cót" tôi lại càng khao khát nhưng "người bạn" đồ chơi biết cử động ấy. Cảm xúc này vẫn không thay đổi cho dù bao nhiêu năm đã qua và đến lần gần đây nhất đọc lại, tôi vẫn ao ước những điều mà Doraemon mang lại. Xin cám ơn chú mèo máy đáng yêu này.
3
362041
2015-12-28 18:59:44
--------------------------
246243
11217
Thật là hay biết bao khi chúng ta co thể tạo ra một thế giới của riêng mình như Nobita nhỉ? Cậu ấy đã tạo nên một hành tinh thật sự rất xinh đẹp với đầy đủ muôn thú. Điều đặc biệt là các con thú đều hoạt động được nhờ vào bảo bối của Doraemon và chúng biết nói, cũng rất thông minh nữa. Mình thật sự rất thích thú khi đọc tập này, các con thú được vẽ rất dễ thượng. Có thể nói rằng, tác giả đã tạo ra một hành tinh vô cùng đẹp đẽ mà mọi người ai cũng muốn. Cảm ơn, thật sự rất cảm ơn Fujio vì ông đã cho mình một thế giới đáng để mơ ước đến như thế, và cũng cảm ơn vì ông đã mang đến một niềm vui rất lớn trong tuổi thơ mình, đó chính là Doraemon.
4
13723
2015-07-29 17:47:45
--------------------------
183102
11217
Đôrêmon đã trở lại với những tình tiết vô cùng vui nhộn, bất ngờ trong tập truyện dài thứ 17 này. Thú thật mình rất thích thú bông nên quyển sách này hấp dẫn mình kinh khủng. Cuộc phiêu lưu của Đôrêmon và nhóm bạn đến thế giới thú nhồi bông đầy rẫy những nguy hiểm, bắt đầu từ việc xuất hiện tên cướp vượt ngục nhưng cũng đầy những niềm vui, tình cảm lắng đọng trong tâm hồn người đọc về tình cảm gắn bó mà mỗi chúng ta đã từng trải qua với món đồ chơi yêu thích của mình. Đây cũng là một bài hát ngợi ca lòng dũng cảm và sự đoàn kết. Nhìn chung, tập truyện dài này rất đáng tiền mua.
5
558345
2015-04-15 14:26:35
--------------------------
182782
11217
Tuổi thơ chúng ta ít nhiều cũng từng đọc qua Đôrêmon. Những câu chuyện về gia đình, tình bạn thật sự làm tôi rất cảm động dù chỉ nhìn qua lăng kính trẻ thơ. Với Nobita Phiêu Lưu Ở Thành Phố Dây Cót ngoài gia đình, tình bạn ra còn có cả tình yêu thiên nhiên, yêu động vật nữa. Mỗi câu chuyện của Đôrêmon đều truyền tải tới chúng ta một thông điệp sống chứ không nhất thiết phải đọc những cuốn sách về thiên nhiên, môi trường vì trẻ con thì sao hiểu được những cuốn sách ấy.
4
21730
2015-04-14 21:47:37
--------------------------
168219
11217
Mỗi một lúc khi nhớ về tuổi thơ, không thể không nhớ đến chú mèo máy đô rê mon cùng nhóm bạn nobita, xuka, chaien, xêkô. Đó là những kỉ niệm mà mình không bao giờ quên, cho đến bây giờ vẫn vậy. Đô rê mon là người bạn không thể thiếu trong cuộc sống của mình.
Ở truyện dài Nobita Phiêu Lưu Ở Thành Phố Dây Cót, khi mình đọc nó có một cái tên khác. Cũng là một cuộc phiêu lưu kì thú cho nhóm bạn nobita, được quen biết thêm nhiều bạn bè ở thế giới khác.
Câu chuyện nào cũng xuất hiện tình bạn ngây thơ trong sáng, đáng yêu vô cùng.
Đô rê mon, tôi yêu đô rê mon.
5
482828
2015-03-16 10:50:26
--------------------------
