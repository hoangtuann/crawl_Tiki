317130
4730
Khi đọc đến tận những dòng cuối-cùng, mới hiểu vì sao tên cụ Nguyễn Tri-Phương được đặt cho rất nhiều các con đường lớn ở mọi thành-thị. Một danh-tướng lẫy-lừng, cố-thủ Đà-nẵng, trị-an Bắc-hà, lại là một công-thần phụng-sự ba triều, vị-quốc vong-thân, bảo-toàn khí-tiết khi thành Hà-nội thất-thủ, đến cả quan Pháp cũng phải ngưỡng-vọng và vị-nể. Xuyên suốt quyển sách này, độc-giả hầu như thấy sự vẻ-vang nơi công-nghiệp của họ Nguyễn dâng lên quốc-gia. Bên cạnh những chiến-tích lẫy-lừng, cũng không phải là không có những chiến-bại đã ghi vào thanh-sử, như việc thất-thủ đồn Chí-hòa, mất ba tỉnh miền Đông Nam-kỳ... Dầu vậy, phàm là con người tất sẽ không được hoàn-toàn. Lại nữa, quân-khí nước ta thời đó khó có thể so bì với sự tiến-bộ của Tây-phương. Nên chăng điều mất nước là tất-yếu, như cụ đã tiên-đoán. Và cũng như các quyển sách khác về danh-nhân, như Trần Thủ-độ, như Lê Ngọc-Hân, sự công-tâm và khách-quan đã được đặt lên hàng đầu. Thiết-tưởng đây là cuốn sách thực sự hay và có giá-trị cho hết thảy những ai ham mê sử-học, tâm-huyết với quốc-sử và muốn tìm hiểu thấu-đáo công-nghiệp bảo-quốc của tiền-nhơn. 
5
34195
2015-10-02 16:49:54
--------------------------
185846
4730
Cụ Nguyễn Tri Phương là một nhân vật lịch sử đã làm cho tôi suy ngẫm rất nhiều khi đọc xong tác phẩm này. Thật vui vì Alphabooks đã cho ra đời loạt sách về Góc Nhìn Sử Việt. Cụ đã chứng minh cho tất cả thấy rằng chết vinh còn hơn sống nhục của các bậc tiền nhân khi nhịn ăn nhịn uống mà chết chứ không hàng làm cả giặc pháp cũng phải kính nể. Thật thiếu sót cho thế hệ ngày nay khi hầu như rất ít người biết về những anh hùng dân tộc của đất nước thời buổi đầu loạn lạc. Mong rằng Alphabooks sẽ tiếp tục mang tới cho độc giả những quyển sách hay khác trong loạt sách Góc Nhìn Sử Việt.
5
387632
2015-04-20 03:00:54
--------------------------
168048
4730
đây là một sách rất hay cho những ai yêu mến lịch sử. nó thể hiện một góc nhìn khác của hậu thế về Nguyễn Tri Phương - một vị quan tài năng, lỗi lạc, ngoài ra còn có tài văn chương  của nước ta, nhiều lần xung phong chống lại Pháp khi chúng mới sang xâm lược nước ta. khi học lịch sử 11, mình cảm thấy rất khâm phục ông, một người hết lòng nước vì dân, khi bị giặc bắt thà nhịn ăn tới chết chứ không khuất phục. một tinh thần mạnh mẽ, đáng để chúng ta noi gương à học hỏi
5
495463
2015-03-15 21:03:31
--------------------------
