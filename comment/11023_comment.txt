468379
11023
Tôi là một người low-tech, và Dữ liệu lớn đã mang đến cho tôi một cái nhìn toàn diện và dễ hiểu nhất về “Big Data”. Tôi đã không ngờ rằng những dự đoán hằng ngày tôi vẫn nghe, những gợi ý mua hàng trên Amazon,... và còn nhiều nữa, đều dựa trên những phân tích với dữ liệu lớn. Đó là những việc chúng ta không thể làm với dữ liệu nhỏ. Điều đó thật tuyệt vời. Tôi đọc cuốn sách với một cảm xúc khao khát khám phá dâng tràn, cảm giác lâng lâng trong người. Không thể nào dừng lại được.
5
54635
2016-07-04 23:19:08
--------------------------
396594
11023
Tôi tùng rất tò mó về nhan đề cuốn sách Big Dat, thế rồi lời trich dẫn vô cùng mới lạ và hấp dẫn "Màu sơn nào có thể cho bạn biết một chiếc xe đã qua sử dụng vẫn còn trong tình trạng tốt? Làm thế nào các công chức ở thành phố New York có thể xác định các hố ga nguy hiểm nhất trước khi chúng phát nổ? Và làm thế nào những cuộc tìm kiếm của Google dự đoán được sự lây lan của dịch cúm H1N1?". Nhưng khi đọc xong tôi đã tìm được câu trả lời. Theo tôi đây là một cuốn sách bạn nên đọc bởi nó không cần một chuyên ngành nào cả. Đọc nó, bạn sẽ tìm ra được nhiều điều trong cuốn sách này.
5
586122
2016-03-13 19:42:26
--------------------------
298127
11023
Đây là cuốn sách tiếng việt đầu tiên nói về 1 khái niệm hoàn toàn mới trong lĩnh vực công nghệ thông tin , nói đúng hơn là 1 cách tư duy mới trong thời đại bùng nổ thông tin. Bên trong cuốn sách là những case study đáng giá từ những công ty lớn giúp chúng ta có cái nhìn khác về dữ liệu và cách mà những công ty đó biến dữ liệu thô đó thành vàng. Bên cạnh việc nhìn ra giá trị to lớn của dữ liệu cuốn  sách còn cung cấp cho ta 1 cái nhìn khác về việc đe dọa sự riêng tư của người dùng trong thời đại dữ liệu lớn .

5
65620
2015-09-12 14:09:06
--------------------------
259331
11023
Biết đến quyển sách này khi bạn mình nhờ mua, vô tình đọc lướt vài trang nó hấp dẫn mình ngay từ những trang đầu tiên, và quyết định mượn đọc khi bạn ấy đọc xong. Cuốn sách cung cấp nhiều ví dụ từ thực tế  rất hay, đưa ra nhiều quan điểm thú vị và quan trọng của những gì đang xảy ra trong lĩnh vực Big Data, nó đã làm thay đổi nhiều khía cạnh trong cuộc sống và cách chúng ta tư duy. Big Data là một xu hướng và làm thế nào nó có thể giúp một doanh nghiệp hoặc tổ chức thành công. Nếu bạn đang tìm kiếm lý do tại sao mọi người nên thực hiện các kỹ thuật Big Data trong tổ chức của bạn, đây là cuốn sách dành cho bạn. Điều tôi không thích ở cuốn sách là có nhiều ví dụ về các khái niệm tương tự được lặp lại đến hơn ba, bốn lần. Tuy nhiên đây là cuốn sách hay rất đáng đọc. 
4
342197
2015-08-09 12:41:09
--------------------------
221725
11023
Mình đã mua cuốn này đã lâu và giờ mới chỉ đang trong quá trình đọc và tìm hiểu về những nội dung, kiến thức mà Big Data đem đến. Thật sự những điều trong đây quá mới mẻ đối với mình dù xu hướng này đã có từ lâu và đã được những công ty về công nghệ hàng đầu áp dụng như Google, Facebook. 
Đây thật sự là một trong những cuốn nằm trong bộ sách Khoa học khám phá đáng để đọc, chứa đựng nhiều kiến thức nhưng không quá khó đọc và dễ tiếp thu. Một cuốn sách đáng nằm trong tủ sách nhà bạn
4
441722
2015-07-04 11:53:36
--------------------------
140321
11023
- Đây là quyển sách rất cần thiết cho mọi người, đặc biệt là các bạn học ngành công nghệ thông tin, cho các bạn cái nhìn khái quát về khái niệm Big Data, cách thức mà mọi người đã ứng dụng Big Data để phân tích các dữ liệu trong quá khứ và tìm ra các xu hướng chung từ đó dự đoán được những việc sẽ xảy tiếp trong tương lai. Bên cạnh đó, tác giả cũng cung cấp các cách thức mà các ông lớn ngành công nghệ (Facebook, Google,...) đang sử dụng để thu thập và phân tích dữ liệu nhằm cải thiện các dịch vụ của mình, do đó mình nghĩ những kiến thức này rất cần thiết cho các bạn học ngành CNTT đang có ý định khởi nghiệp với các dịch vụ cần ứng dụng Big Data.
5
57354
2014-12-10 20:33:01
--------------------------
112653
11023
Big Data (tạm dịch là dữ liệu lớn) đang trở thành một xu thế hot trong những năm gần đây tại các diễn đàn công nghệ cũng như các trang tin chuyên ngành kinh tế, kỹ thuật. Đi kèm với nó là rất nhiều sách liên quan cũng được xuất bản, nhưng chọn cuốn nào để đọc lại tùy thuộc vào công việc, lĩnh vực của mỗi người.

Tôi chọn cuốn Big Data - A Revolution That Will Transform How We Live, Work, and Think của Viktor Mayer và Schonberger Kenneth Cukier bởi nội dung của cuốn sách bám sát vào các vấn đề thực tiễn như y tế, kinh tế, giao thông, xã hội, phòng chống tội phạm.v.v… Và các vấn đề này được giải quyết dựa trên việc tổng hợp lượng lớn dữ liệu, phân tích rồi đưa ra dự đoán về xu thế.

Xuyên suốt cuốn sách, 2 tác giả luôn nhắc nhở người đọc về cách thức sử dụng dữ liệu lớn với phương châm tìm hiểu “cái gì” chứ không phải “tại sao“. Nghĩa là khai thác dữ liệu từ những mảnh vụn có phần “vô nghĩa”, tái cấu trúc nó, phân tích với những thuật toán và công cụ xác suất thống kê, rồi từ đó để dữ liệu “tự nói” lên thông tin hữu ích.

Sách mở đầu với việc mô tả hiện trạng áp dụng Big Data ở Mỹ và các nước tiên tiến trên thế giới, với sự góp mặt điển hình của các “ông lớn” như Google, Amazon, Walmart, IBM, v.v… Các hãng lớn từ lâu đã và đang thu thập đủ loại dữ liệu về người dùng internet, khách hàng, thời tiết, thiết bị thông tin y tế… với mục đích mang lại các dự đoán có giá trị cho chính bản thân hãng cũng như các đối tác.

Dưới góc nhìn Big Data, ta sẽ hiểu rõ hơn về giá trị của những thông tin tưởng như “bình thường” của Facebook (số lượng like, comments), của Twitter (còn gọi là dữ liệu tư duy, suy nghĩ của hàng triệu người dùng) hay Instagram. Đối với trường hợp Instagram, bấy lâu nay, khi xem lại một bức ảnh cũ, cá nhân tôi thấy Intargram chỉ ghi chung chung rằng bức ảnh này được đăng tải mấy tuần trước (hoặc mấy ngày trước đó) mà không ghi cụ thể, chính xác ngày nào (hoặc đếm chính xác bao nhiêu ngày trước). Tại sao? bởi với dữ liệu lớn, bạn chỉ cần biết xu thế, biết một con số chung “áng chừng” hoặc “trong khoảng” bao nhiêu ngày mà không nên “đòi hỏi” một sự chính xác tuyệt đối. Vì sao ư? Bởi sự chính xác tuyệt đối là giá trị của dữ liệu nhỏ.

Các chương tiếp theo của cuốn sách tập trung vào việc dữ liệu hóa cũng như xác định giá trị của dữ liệu, xác định chuỗi giá trị mà qua đó các doanh nghiệp, cá nhân, tổ chức tham gia vào các phân đoạn của thị trường Big Data. Có một nghịch lý hiện tại trong thị trường Big Data đó là các doanh nghiệp nắm giữ một lượng lớn dữ liệu (thường họ có được do thu thập, lưu trữ qua nhiều năm dựa trên công tác kinh doanh của họ) lại không / hoặc chưa biết cách khai thác lượng dữ liệu lớn này một cách hiệu quả, hữu ích. Nếu như Google khai thác cơ sở dữ liệu tìm kiếm (search query) của hàng triệu người dùng để từ đó bổ trợ cho các công cụ quảng cáo, dịch thuật, hay Amazon khai thác thói quen duyệt web của khách hàng, Walmart khai thác thông tin mua hàng tại cửa hàng dựa theo thời tiết, chu kỳ mua sắm, mật độ giao thông… thì những ngành nghề như quản lý hành chính, cơ quan dân sự của nhà nước, dân số, hàng không, thời tiết, giao thông, quân đội, y tế… lại chưa thực sự “nhảy” vào khai thác mảng dữ liệu màu mỡ mà họ đang có.

Dữ liệu nếu không khai thác, sẽ chỉ là những mảng lưu trữ rời rạc và vô nghĩa. Thậm chí những đơn vị nắm giữ dữ liệu cũng bỏ mặc chúng và ngủ quên trong sự bận rộn của công việc hàng ngày. Nhưng nếu một ngày, một lượng lớn dữ liệu được trao vào tay những chuyên gia (còn gọi là data scientist), hay những công ty phân tích và khai thác dữ liệu chuyên nghiệp, chúng ta sẽ thấy những giá trị to lớn được tìm thấy từ “mỏ vàng” bị bỏ quên.

Mặc dù các doanh nghiệp start-up được lấy làm ví dụ trong cuốn sách phần lớn đã bị thâu tóm (Decide.com bị mua bởi Ebay, Farecast.com bị mua bởi Microsoft…) nhưng phần nào chúng ta cũng thấy được tầm ảnh hưởng của Big Data lên các doanh nghiệp ngày nay, đặc biệt là các doanh nghiệp hoạt động trên Internet. Trong một tương lai không xa, Big Data  chắc chắn sẽ là một nền công nghiệp nổi trội, cũng như thời của Internet, e-Commerce, Social network những năm trước đây, thậm chí, Big Data chính là “đầu ra” mới của các doanh nghiệp cũng như là một hướng đi mới để các đơn vị cải tiến dịch vụ, sản phẩm của mình được tốt hơn.

Sau cùng, các tác giả cũng để cập đến vấn đề quyền riêng tư, cách thức kiểm soát thông tin để sao cho Big Data không bị lạm dụng cũng như không kiểm soát cuộc sống của con người, xã hội và đặc biệt là công lý. Có chăng, nên nhìn nhận Big Data là một công cụ mới của thời đại mới chứ không nên lạm dụng nó hoàn toàn trong việc dự đoán (thậm chí là phỏng đoán) hay đưa ra quyết định, bởi đó là những việc rất cần “lý trí” của con người.

Lưu ý:

- Sách được dịch bởi NXB Trẻ và có hơi trúc trắc khi đọc, nhất là chương 1, 2. Sách dịch nhầm khá nhiều chỗ đối với từ “start-up” nên trong mấy chương đầu phải tham khảo bản tiếng Anh để hiểu đúng nghĩa hơn.

- Sách cũng dành cho đối tượng làm UX, bởi quan sát và thu thập thông tin về người sử dụng (user observation) là việc làm phổ biến của ngành này. Do đó, có thể nói, Big Data cũng là một phần tương lai của UX.
4
9449
2014-05-17 22:25:26
--------------------------
110240
11023
Đây là một quyển sách hay và không đòi hỏi người đọc phải có bất cứ chuyên ngành nào để có thể hiểu được nó. Nói cách khác, quyển sách được coi là sách khoa học thường thức hơn là sách dành cho nghiên cứu. Quyển sách kích thích trí tò mò của người đọc bằng 2 ví dụ làm sao Google có thể dự đoán chính xác hướng lây lan của đại dịch cúm H1N1 trước khi đại dịch thật sự bắt đầu, và làm sao để dự đoán lúc nào nên mua vé máy bay để được hưởng ưu đãi tốt nhất. Quyển sách nêu rõ vấn đề hiện tại của nhân loại là "bội thực thông tin" và cách chúng ta phản ứng với việc đó ra sao. 

Tuy nhiên, đáng tiếc là quyển sách không thật sự nêu được định nghĩa thế nào là dữ liệu lớn, nhưng điều này cũng dễ hiểu, vì hiện tại vẫn còn nhiều tranh cãi xung quanh định nghĩa này.
5
270700
2014-04-14 11:07:03
--------------------------
