437318
4360
Đối với người học luật thì việc đọc luật là vô cùng quan trọng. Là một người học luật, mình rất quna trọng và kỹ tính trong việc lựa chọn sách luật cũng như nhà xuất bản. Việc đặt mua trên tiki là một quyết định sáng suốt của mình. Tiki giao hàng rất tốt, rất nhanh. Sách từ nhà xuất bản rất đẹp. Trình bày rất dễ xem và dễ đọc. Chữ rõ ràng, không nhem nhuốc hay mờ. Nhìn chung, mình rất hài lòng với sản phẩm này của tiki, và hi vọng sẽ mua được nhiều sách luật khác trên tiki nữa. Cảm ơn!
5
309555
2016-05-27 23:53:04
--------------------------
403409
4360
"Luật Sở Hữu Trí Tuệ" là một cuốn sách vô cùng bổ ích. Sách đã cung cấp một lượng tri thức lớn cho người đọc về luật. Đây quả là sự lựa chọn tuyệt vời cho những bạn theo đuổi ngành luật. Sách được trình bày hết sức khoa học, dễ dàng tra cứu, khổ sách in rất vừa vặn, gọn nhẹ, dễ đọc. Các trang giấy in rất trắng, mịn. Mình rất hài lòng về cuốn sách này, quả là đúng đắn khi chọn lựa mua nó. Mình tin rằng mọi bạn đọc đều hài lòng về cuốn "Luật Sở Hữu Trí Tuệ" này.
5
1174527
2016-03-23 19:39:05
--------------------------
