470998
7247
mình rất thích tập truyện này vì có câu chuyện về hai người ông của shin. câu truyện hấp dẫn mình từ những trang đầu tiên của nó, hình vẽ rất sống động chỉ bằng những nét phác thảo đơn giản. Cốt truyện vui vẻ và gần gũi với cuộc sống. Cuốn Shin này không chỉ hợp với các em thiếu nhi mà còn hợp với các bạn học sinh, sinh viên, bậc cha mẹ nữa. Mỗi trang Shin đem lại cho mình thật nhiều tiếng cười. không chỉ mình mà em mình cũng siêu nghiện truyện Shin nữa. Nhớ mãi thằng Shin hột mít vui tính đáng yêu và không kém phần bựa :D
5
1098156
2016-07-08 09:38:14
--------------------------
445339
7247
Từ khi biết đến cậu bé bút chì. Mình thấy câu chuyện rất hay. Mỗi câu truyện là một bài học là một niềm vui đối với mỗi người đọc cũng như mình. Hình ảnh sinh động đẹp. Các họat động trong từng câu truyện khá hấp dẫn và lôi cuốn. Mình có 2 đứa e nữa nó rất thích đọc truyện này. Mình mua rất nhiều tập rồi và cũng cảm thấy rất hay. Hình như mình đang dần nghiện cậu Shin rồi hi hi. Nếu ai chưa đọc hãy thử một lần nhé. Tiki giao hàng gói rất kĩ có cả hóa đơn. Mình rất thích cách giao hàng ở tiki.
4
1227587
2016-06-10 00:49:41
--------------------------
366102
7247
Không biết nó có gì mà cuốn hút mấy đứa nhỏ quá. Đứa em mình cứ đòi mua cho bằng được. Nên mua cho thỏa lòng con trẻ đọc và mình cũng thử đọc ké. Quả thật cuốn sách này cuốn hút trẻ nhỏ là hợp lý. Cậu bé Shin rất tinh nghịch, hài hước nên gây được tiếng cười. Một tiếng cười rất hồn nhiên của trẻ thơ. Thấy em mình đọc xong hỏi nội dung lại nó cũng nhớ nên cũng thắc mắc sao mà nội dung truyện hay những bộ phim nó yêu thích lại được trẻ con nhớ đến vậy trong khi bài học nó khó nhớ hơn. Thậm chí có nhớ cũng chỉ là "nhớ vẹt". Phải chăng tiếng cười giúp trẻ học tốt hơn?.
3
574020
2016-01-09 11:00:59
--------------------------
