540145
9291
Nội dung rất hay. Phải nói là cực kì.Lúc đầu mk cứ tửong 2 ng Thái Hồng & Quý Hòang  sẽ k thành đôi.Nhưng khi đọc gần cuối thì lại khác.Cuộc gặp gỡ thật đẹp.Chỉ có điều Nhân vật LMC quả là ngừoi của xhpk. Thực sự  nhân vật đó có chút vấn đề về tư tưởng suy nghĩ.
4
1887414
2017-03-12 11:41:07
--------------------------
537373
9291
Truyện sẽ đưa bạn đến một xã hội hiện đại. Nơi những phồn hoa, hào nhoáng được con người đeo đuổi. Nơi người ta quên đi ước mơ thật sự của mình. Nơi những con người bần cùng tìm cách gian dối đẻ mưu sinh. Nhưng bên cạnh đó, không thiếu những trái tim non trẻ, mong ước thực hiện được ước mơ. Nơi những hoài bão được ấm ủ và tỏa sáng. Nơi chỉ có tình yêu chân thành nhất bên nhau. Nhưng liệu chỉ cần có tình yêu là đủ? Họ có vượt qua được rào cảng xã hội? Họ có thể bên nhau dù hai bàn tay trắng? Tất cả đều được khắc họa một cách chân thực nhất trong câu chuyện.

5
896426
2017-03-07 21:35:34
--------------------------
512969
9291
Bìa đẹp, sách cực dày nhìn đã mắt. Xét về nội dung thì tạm ổn nhưng nó cũng không làm cho ngừoi đọc cảm thấy đủ hay thấm không có những mạch cảm xúc theo truyện như những cuốn khác nhưng nó là một cuốn sách cũng đáng để thử. Mình cho 4 sao.
4
1448474
2017-01-19 12:06:13
--------------------------
384762
9291
Thành Phố Hoang Vắng không phải là một câu chuyện quá hay đến nỗi người ta ghi nhớ từng câu chữ. Thế những mỗi nhân vật trong đó đều ôm một nỗi đau riêng và dường như những nỗi đau của họ gắn kết với nhau tạo nên cốt truyện rất đỗi riêng biệt.
Thái Hồng vì sống trong sự che chở kĩ càng và cẩn thận của gia đình mà trở nên dựa dẫm, và có phần suy diễn, hoang tưởng. Mình không thích cô gái này lắm vì Thái Hồng có 1 tính xấu khá tai hại, cô nàng rất hay tọc mạch và thích chỉ đạo.
Qúy Hoàng là một thanh niên tốt nhưng anh chàng có cách thể hiện tình cảm hơi quê mùa. Dù sao thì điểm nổi bật của anh là lòng tự trọng cao ngất ngưởng.
Lý Minh Châu vì vấn vương thời huy hoàng của gia tộc nên cố chấp và mưu lợi.Dù ẩn dưới những suy nghĩ, câu chữ, lời nói có phần tàn độc là cả một trái tim yêu thương dành cho đứa con gái nuôi Thái Hồng.
Bản thân mình rất thích Đông Lâm và Tần vị, họ đều mang nét phóng khoáng của thiếu gia nhà giàu, vừa đáng yêu theo cách riêng của họ
Tác phẩm cũng không có gì nổi bật về tính triết lí, câu từ có phần hời hợt, ngòi bút của tác giả hơi yếu nhưng học vấn mà tác giả  mang tới lại vô biên và sâu sắc.
Điểm cộng của tác phầm là bìa đẹp và chất lượng giấy tốt
4
584588
2016-02-22 22:32:35
--------------------------
368814
9291
Thi định nhu có gì rất mang tính hồi ức mà u tối , thoáng buồn hiện lên trên gương mặt nhân vật , sự lắng đọng trong từng cử chỉ hay lối diễn tả bình thường , đều có kỳ lạ ở trong chính những xuyên lạc , mối quan hệ không mấy được hoàn hảo , sự xuất hiện từ chính trong kẽ hở của nhân vật làm điều quá khứ khó khăn , nam chính có cả lạc quan trong suy nghĩ về nghề nghiệp , viết văn và có tâm hồn nhạy cảm và ngay cả sự nghiêm khắc .
4
402468
2016-01-14 18:02:37
--------------------------
284783
9291
Hiếm có cuốn sách nào lại gợi cho tôi nhiều về nội dung bên trong chỉ qua phần trang trí bìa như "Thành phố hoang vắng". Bìa sách rất giản dị, nhưng lại mang nét đơn côi, cô quạnh.
Nội dung của "Thành phố hoang vắng" không quá mới mẻ, nhưng đủ thu hút tôi nhờ giọng văn thoảng nét buồn, mạch lạc và khá thực tế. Đọc cuốn sách này, tôi có thoáng thấy hình ảnh của bản thân trong thành phố lạnh lẽo ấy. Mỗi chúng ta khi sinh ra đều có sứ mệnh riêng, nhưng đều chung một ý chí sinh tồn. Thành phố trong cuốn sách bao vây và gián tiếp đẩy con người vào thế cô độc. Nhưng tương lai khó đoán định, con người vẫn phải tự tìm ra hơi ấm cho mình.
4
48662
2015-08-31 11:27:05
--------------------------
179100
9291
Thấy tên tác giả Thi Định Nhu + bìa truyện siêu đẹp (nhìn như tranh nghệ thuật ấy), mình đọc truyện này không cần đắn đo. Đọc xong tâm trạng trồi sụt mất mấy ngày luôn. Vẫn giọng văn nhẹ nhàng, hờ hững, nhưng trong Thành phố hoang vắng còn phảng phất nét thê lương, ảm đạm. Thi Định Nhu đã miêu tả thành công sự tương phản giữa hai mặt hào nhoáng và tăm tối của cuộc sống chốn thành phố. Cả câu chuyện là bầu không khí bức bối, không lối thoát của các nhân vật. Tác giả có phần tàn nhẫn với Thái Hồng, khi lần lượt tước đi mất những người cô yêu thương, từ người bạn gái thân thiết, người mẹ luôn quan tâm cô, người bạn tri kỷ của cô. Tôi bị ám ảnh rất lâu khi biết Tô Đông Lâm mất tích, chỉ để lại viên nham thạch như đã hứa với Thái Hồng. Những mất mát ấy quá lớn, khiến cho thành phố của Thái Hồng trở nên hoang vắng, và tình yêu của cô với Quý Hoàng không đủ để lấp đầy khoảng trống tỏng tim.
5
57459
2015-04-06 14:41:40
--------------------------
124595
9291
Văn phong của Thi Định Nhu có thể nói là khá đặc biệt, nó không thoải mái, nhẹ nhàng như của Cố Mạn, cũng không quá nặng nề như Phỉ Ngã Tư Tồn nhưng đọc truyên của Thi Định Nhu, mình luôn cảm thấy có cái gì đó rất buồn và ảm đạm.
Tính cách của các nhân vật trong "Thành Phố hoang vắng" được xây dựng rất đa dạng. Mỗi nhân vật đều có cá tính rất riêng và nổi bật. Nữ chính của truyện, Thái Hồng, là cô gái được mẹ bao bọc quá kĩ nên đôi khi cô nhìn cuộc đời dưới ánh mắt quá lãng mạn, tuy nhiên, cô cũng là người có chính kiến và quyết tâm. Nam chính Quý Hòang, một tiến sĩ văn học nghèo, đầy ý chí, và tất nhiên, những người như vậy thì lòng tự tôn cũng rất cao. Lí do hai người yêu nhau, theo mình, đơn giản là vì họ có cùng chung tư duy, chung đam mê văn học. 
Nhân vật thực sự gây ấn tượng mạnh với mình chính là Lý Minh Châu, mẹ của Thái Hồng. Bà là người phụ nữ rất lí trí, vô cùng thực tế, thương con nhưng cũng vô cùng nghiêm khắc và đôi khi rất tàn nhẫn. Bà nhìn nhận sự việc theo nhiều hướng khác nhau và chỉ lựa chọn những phương án giải quyết sự việc tốt nhất, chứ không phải tình nghĩa nhất. Từ đầu đến cuối truyện, ta có thể thấy được những suy nghỉ của bà đều chính xác. Mặc dù đây có thể là nhân vật phản diện trong mối tình của Thái Hồng và Quý Hoàng nhưng bà là người phụ nữ đáng được người khác nể trọng, kính phục.
4
323629
2014-09-08 10:48:22
--------------------------
106573
9291
Tôi đã đọc hầu hết các tá phẩm của chị Nhu.
Và luôn ấn tượng với chất văn bác học của chị. Nhưng truyện chị viết luôn đi liền với rất nhiều thể loại, cùng thuật ngữ chuyên môn, chuyên ngành, nhiều câu chứ khiến ta phải trầm ngâm, suy ngẫm sau khi đọc.
Với " Thành phố hoang vắng" vẫn với lối văn phong cũ, chau chuốt, sâu sắc, và nhiều suy ngẫm về cuộc sống.
Nhưng so với các tác phẩm khác của Thi Định Nhu thì tôi không ấn tượng với tác phẩm này lắm.
Tôi đọc nửa đầu truyện cảm thấy rất thú vị và bị cuốn hút, nhưng nửa sau của truyện lại khiến tôi cảm thấy đuối.
Đoạn đầu tác giả đi vào diễn biến và tình cảm của hai nhận vật chính và các nhân vật phụ sâu bao nhiêu thì về sau càng loãng bấy nhiêu.
Sự xa cách, lý do chia tay rồi hoàn cảnh tái hợp của hai nhân vật chính dù nó rất đời rất thật nhưng lại không làm tôi thỏa mãn.
Cách chị Như viết tác phẩm này có phần gượng ép tính cách và hoàn cảnh diễn biến thiên truyện.
Sự hiểu lầm, chia tay của Quý Hoàng và Thái Hồng có lẽ bởi chính sự phũ phàng của số phận, sự cách biệt về tư tưởng gia đình.
Hai con người cao ngạo, cá tính và đầy tự trọng ấy, đã chọn con lựa đi hai con đường tách xa nhau...
Tôi có thể hiểu và cảm thông cho lý do Quý Hoàng từ bỏ tình yêu của anh dành cho Thái Hồng, nhưng tôi lại không thể cảm thông cho cách Thái Hồng chọn lựa từ bỏ gia đình, người đã chọn nuôi nấng dưỡng dục cô như con gái để suốt bao nhiêu năm bởi sự hiểu lầm không đáng có.
Cô từ bỏ gia đình, từ bỏ nới đã gắn bó với cô suốt thủa thiếu thời, cho đến khi mẹ cô gần đất xa trời mới chịu quay trở lại để giải mối hiểu lầm...Khiến tôi vô cùng thất vọng về hình tượng Thái Hồng mà chị Nhu dày công xây dựng trong tác phẩm thấm đẫm những triết lý rất đời này.
Cô đã tự chọn lựa từ bỏ, chứ không ai gượng ép cô từ bỏ để biến thành phố, gia đình, bạn bè đã gắn bó với cô suốt những tháng ngày niên thiếu cho đến lúc trưởng thành để biến vùng đất đã chứng kiến cô lớn lến, chứng kiến tình yêu của cô, chứng kiến tình bạn của cô trở thành " Thành phố hoang vắng"...
Tôi đã khá hụt hẫng khi đọc đến những trang sách cuối cùng, có lẽ đối với tôi đây là tác phẩm đuối "triết lý", kém " đời" nhất trong các tác của chị Nhu mà tôi đã chọn đọc!
3
174705
2014-02-21 22:41:54
--------------------------
91122
9291
Khác với nhiều tiểu thuyết ngôn tình khác mà tôi đã từng đọc, thành phố hoang vắng đối với tôi giống như câu chuyện mang đậm tính hiện thực, nhưng dù vậy nó cũng đan xen chút lãng mạn, ngọt ngào bên trong đó. Khi đọc cuốn sách này, tôi cảm thấy tác giả viết rất thật, về tình yêu, về cuộc sống.
Tuy rằng truyện là HE, nhưng cuốn sách vẫn mang cho người đọc cảm giác buồn miên man... Đúng với tên của câu truyện "Thành phố hoang vắng". Tôi không hề ghét ai trong câu chuyện cả, vì mỗi người đều có một nỗi niềm riêng, ví như mẹ Thái Hồng, bà không xấu, những gì bà làm đều là vì muốn tốt cho con gái mình, thế nhưng tình cảm ấy biểu lộ không đúng cách và có vẻ hơi thái quá. Vào đoạn cuối sách, tôi có thể cảm thấy bà yêu Thái Hồng như thế nào khi hỏi Quý Hoàng về cuộc sống dành cho Thái Hồng sau này trước lúc lâm chung.
Từng đọc Lịch Xuyên chuyện cũ của tác giả này, nên khi nhìn thấy cuốn sách này xuất bản (lại trong thời gian tiki đang giảm giá) tôi đã mua ngay mà không đắn đo, quả là cuốn sách đã không làm tôi thất vọng cả về chất lượng, nội dung và hình thức.
4
108329
2013-08-16 01:55:26
--------------------------
89709
9291
Đây là quyển sách đầu tiên tôi mua của Amun Đinh Tỵ Books và nhờ nó mà đã dành cho Amun khá nhiều thiện cảm ngay từ lúc ban đầu.

Như nhiều bạn đã nhận xét ở dưới, nội dung của Thành Phố Hoang Vắng không quá mới mẻ hay độc đáo nhưng giọng văn và cách kể chuyện lại mang nét trữ tình hiện thực khá thu hút. Biết đến dịch giả há cảo di động từ lâu nhưng khi mua quyển này lại không hề hay nó là do chị dịch. Đến khi xé lớp màng co ra, mở vào trong, thấy tên người dịch, nửa ngạc nhiên nửa vui mừng, cứ có cảm giác là nhờ người dịch này mà mình sẽ có một câu chuyện đáng đọc đây. Và sự thật đúng là như thế :D

Với tôi, Thành Phố Hoang Vắng là câu chuyện đọng lại được trong lòng người. Trong suốt hơn 500 trang của quyển sách, có những phút giây hài hước, lãng mạn, nhưng cũng không kém phần chua xót và thực tế đến phũ phàng. Dàn nhân vật từ chính đến phụ đều được xây dựng chăm chút và truyền tải được những thông điệp, bài học, giá trị nhất định đến độc giả. Kết thúc truyện có thể gọi là HE nhưng lại không hoàn toàn viên mãn mà để lại cảm giác "hoang vắng" đến ngợp người - đúng như tên gọi của câu chuyện.

Cuối cùng thì, thành phố S nơi mà Thái Hồng đã sinh ra và lớn lên suốt 25 năm, lại trở thành một nơi hoàn toàn xa lạ và chẳng chút quan hệ gì với cô. Chỉ vì một lẽ đơn giản, những người cô từng yêu và từng yêu cô, đều đã đi khỏi thành phố đó, bằng cách này hoặc cách khác.

Sau Thành Phố Hoang Vắng, tôi mong chờ Chuyện Cũ Của Lịch Xuyên của Thi Định Nhu.
4
10596
2013-08-03 20:59:44
--------------------------
79104
9291
Tôi biết đến Thi Định Nhu qua Tam mê hệ liệt và không mấy ấn tượng lắm với tác giả này. Đến khi đọc Thành phố hoang vắng thì tôi hoàn toàn bị tác giả đánh gục. Khoan nói đến cốt truyện hay văn phong, tôi ngưỡng mộ tác giả với kiến thức sâu rộng, cách đưa tri thức vào trang sách lại không quá phô trương, không theo kiểu tác giả khoe khoang kiến thức. Còn cốt truyện thì khá hay, cách viết của tác giả không theo kiểu bi lụy nhưng lại làm cho tôi cảm thấy xót xa. Bởi vì những gì Thi Định Nhu kể, không là câu chuyện tình lãng mạn, ngọt ngào, trong truyện là cả một hiện thực gai góc, đâm chọt vào đáy lòng độc giả. Thái Hồng và Quý Hoàng, một cô gái thành thị, và một chàng trai quê ở nông thôn, cả hai người thực ra chẳng có gì quá đặc biệt như trong các tiểu thuyết ngôn tình khác. Quý Hoàng gia cảnh nghèo, không có vẻ bề ngoài sáng chói, lại còn bị hen suyễn, hoàn toàn khác biệt với hình tượng soái ca trong lòng các thiếu nữ. Thái Hồng nhan sắc bình thường, gia cảnh bình thường, không theo kiểu nữ chính ngây thơ hay cá tính gì gì đấy. Hai người họ yêu nhau, đến với nhau như theo lẽ tự nhiên. Thế nhưng mọi thứ lại không hề suôn sẻ, bởi vì hiện thực cuộc đời, hay đúng hơn là vì Thái Hồng có một người mẹ yêu chủ nghĩa hiện thực. Lý Minh Châu, một người quá thực tế hay đúng hơn là thực dụng, nhưng tôi không trách bà ấy, tôi nghĩ bà ấy cũng là vì con gái mình. Một người phụ nữ sống trong điều kiện khó khăn, hằng ngày phải nai lưng kiếm sống, cố gắng chống lại cuộc đời, hơn ai hết, bà hiểu rõ cuộc đời chông gai cỡ nào, là một người mẹ, bà sợ con gái mình (cho dù là con gái nuôi) khổ sở, bà luôn muốn con gái mình được sống hạnh phúc, điều đó có gì sai. Chỉ có một điều tôi không thích ở Lý Minh Châu là sự ảo tưởng về quá khứ của bà ấy, lúc nào cũng ngày xưa thế này, ngày xưa thế kia, giữa thành phố hiện thực này mà sống như vậy, chẳng tốt chút nào. Bên cạnh hai nhân vật chính, các nhân vật phụ Hàn Thanh, Hạ Phong, Lợi Lợi cũng để lại nhiều cảm xúc. Đọc xong truyện, dư âm để lại trong tôi rất nhiều, tôi không còn nghĩ rằng cuộc đời này màu hồng nữa, có rất nhiều gai trong đời, chúng ta cần phải thật mạnh mẽ, kiên cường để chống chọi lại gai góc cuộc đời.
4
25683
2013-06-05 13:48:14
--------------------------
76949
9291
Mình biết truyện khi được tặng quà sinh nhật, truyện đã để lại trong mình nhiều cảm xúc. Nội dung truyện không quá mới mẻ, cốt truyện mẹ ngăn cản con đến với người mình yêu cũng đã khá quen thuộc, nhưng cái hay ở chỗ tác giả đã làm mới truyện, khiến truyện mới mẻ hơn. Cách kể chuyện của tác giả cũng thật lôi cuốn, hấp dẫn, tình yêu của Qúy Hoàng và Thái Hồng thật ngọt ngào và lãng mạn. Cái đáng nói nhất ở truyện là tính hiện thực, truyện phê phán lối sống thực dụng, đặt tiền lên trên hạnh phúc, đặt biệt là mẹ của nhân vật nữ chính, mình rất ghét nhân vật này.
4
57869
2013-05-25 00:19:10
--------------------------
74304
9291
"Thành phố hoang vắng" - ngay từ tiêu đề đã vẽ nên cho người đọc một cảnh hoang vu, vắng lạ. Nó vắng bởi cuối cùng, kết thúc đôi khi làm độc giả tiếc nuối.

Chuyện tình cảm giữa Quý Hoàng và Thái Hồng không lãng mạn như nhiều ngôn tình khác nhưng khi thấy họ bên nhau, vẫn có chút ấm nóng toả nhẹ trong lòng. Đâu phải là ngôn tình thì phải lãng mạn, phải tình nồng mặn mà. Giữa Thái Hồng và Quý Hoàng ban đầu là đồng nghiệp, sau lại tò mò, thắc mắc và tìm hiểu về nhau, Quý Hoàng làm người ta có cảm giác ngờ ngợ.
Mình thích cách hai bạn giáo viên này bàn luận về văn học. Mặc dù mở đầu chậm chạp và dễ gây nản nhưng khi đã ổn định mạch truyện, bạn sẽ tìm ra nét thu hút trong văn phong của Thi Định Nhu. Dẫn dắt vào cuộc đối thoại về chủ nghĩa Macx đơn giản nhưng hiệu quả. Mình bị điểm này đánh gục

Nhân vật ấn tượng nhất có lẽ là Lý Minh Châu - mẹ Thái Hồng. Một bà mẹ thực dụng và lắm lúc, mình tự hỏi bà ta có nghĩ đến con gái mình không. Nhưng quả thật, mẹ nào cũng mong con hạnh phúc, ấm no; có điều cách làm và ứng xử của bà làm người ta chán ghét.

Đối với những bạn khoái cảm giác mạnh hoặc lạ thì đây chưa hẳn là một quyển sách hay. Vì nó khá là buồn ngủ, theo ý kiến chủ quan của mình.
4
53823
2013-05-12 21:56:04
--------------------------
73067
9291
Lần đầu tiên đọc chuyện của Thi Đinh Nhu nhưnng mình rất thích bởi chuyện mang cái đó rất cuộc sống, rất chân thực. Tình yêu của Thái hồng và Quý Hoàng không lãng mạn như những câu chuyện trong các tiểu thuyết ngôn tình khác nhưng nó vẫn rất hấp dẫn người đọc. Tác giả mang đến cho người đọc thêm những cách nhìn mới về cuộc sống. Mẹ Thái Hồng tưởng chừng như rất thực dụng, rất tàn nhẫn khi luôn chia cắt tình yêu của con gái nhưng mình lại nghĩ khác đó là xuất phát tình yêu của người mẹ. Không có người mẹ nào muốn con gái mình có cuộc sống nghèo khổ cả, cuộc sống mà luôn phải lo cơm áo gạo tiền, bà không muốn Thái Hồng lại đi vào vết xe đổ của bà. Còn về những nhân vật phụ thì thấy kết thúc danhf cho họ hơi buồn nhưng có lẽ không có cuộc đời nào là hoàn mỹ cả. Nhẹ nhang, sâu lắng nhưng cũng hài hước và thú vị. 


4
97873
2013-05-06 10:43:57
--------------------------
66929
9291
Đọc xong câu chuyện, ấn tượng nhất với mình không phải 2 nhân vật chính mà là người mẹ Lý Minh Châu của Thái Hồng.  Trong suốt cả câu chuyện, Lý Minh Châu đóng vai trò là rào cản của tình yêu, là hiện thực phũ phàng. Mình thật sự bị ấn tượng bởi những điều mà bà dạy cho Thái Hồng : Phải thực tế để có cuộc sống tốt hơn. Lý Minh Châu luôn can thiệp vào cuộc sống của Thái Hồng, nhiều lần có những hành vi rất quá đáng, nói cho cùng thì cũng vì tình thương con mà thôi. Nhiều người sẽ nghĩ là, Lý Minh Châu là con người quá thực dụng, nhưng nghĩ lại, cũng chỉ vì cuộc sống của bà quá khắc nghiệt, từ là tiểu thư con nhà giàu trở thành vợ của một người công nhân, cả đời cơm áo gạo tiền. 
Tình yêu của Quý Hoàng và Thái Hồng nhẹ nhàng, mình cũng rất thích. Kết thúc câu chuyện viên mãn cho 2 nhân vật chính, còn những nhân vật phụ mình thấy cũng có thể coi là viên mãn : Lợi Lợi đạt được mục đích, Đông Lâm thì cũng thực hiện được lý tưởng của mình. Riêng Hàn Thanh thì kết cục thật đau lòng. 
Câu chuyện đơn giản, nhưng thể hiện được sự thực tế của tác giả , đây là một câu chuyện hay.
5
52086
2013-04-03 13:59:13
--------------------------
61532
9291
"Thành phố hoàng vắng" chính xác là tác phẩm mình thích nhất của nhà văn Thi Định Nhu. Có gì đó rất chân thực, rất tinh tế và sâu lắng trong tác phẩm này. Từng trang văn hiện lên một cách đơn giản, nhẹ nhàng, với những nét chấm phá sống động nhưng đủ để khắc họa tâm lý nhân vật. Ẩn sau vẻ giản dị của con chữ là những tầng sâu cảm xúc khó nắm bắt, chính điều này đã làm nên sức hút lạ kỳ cho tác phẩm này. Tác giả đã vẽ nên một bức tranh nhiều màu sắc, gần gũi nhưng vẫn đầy ẩn ý về tình yêu, cuộc sống và con người. "Thành phố hoang vắng" thực sự là một tác phẩm không nên bỏ qua.
4
20073
2013-03-02 19:26:23
--------------------------
59902
9291
Đây là một cuốn truyện khá hiện thực, tình yêu giữa Thái Hồng và Quý Hoàng cũng không lãng mạn như trong những cuốn ngôn tình khác, nhưng mình lại cảm thấy nó rất thu hút. Mới đầu nhìn tên truyện và bìa sách cứ nghĩ truyện hơi khô khan, nhưng đọc rồi mới thấy trong truyện vẫn có những chi tiết hài hước, thú vị. Tuy hai nhân vật chính có happyy ending nhưng mình cảm thấy các nhân vật phụ sao mà có số phận thảm quá, dù biết cái kết cho họ như thế là hợp lý với diễn biến của truyện nhưng vẫn thầm mong giá mà tác giả đừng nhẫn tâm quá với họ như vậy. Mình nghĩ cuốn này khá hay và các bạn nên đọc.
4
17740
2013-02-18 22:00:20
--------------------------
