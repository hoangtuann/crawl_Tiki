98181
8624
Trong khi chỉ tóm tắt nội dung mà tác giả viết đã như thế này: "Vẻ rực rỡ và niềm kiêu hãnh của họ trên bậc thang xã hội không khiến nam giới cùng thời lựa chọn họ. Tuổi thanh xuân của họ trôi qua đơn chiếc chỉ vì tính chất phi thực tế của khung tiêu chuẩn chọn chồng mà họ đặt ra, thật khó để đàn ông đáp ứng kỳ vọng của họ." Chỉ bấy nhiêu từ cũng đủ thấy tác giả đưa ra nhận xét phiến diện quá. "không khiến nam giới cùng thời lựa chọn họ" => Chọn ư? Tại sao phụ nữ lại để cho đàn ông chọn như một món hàng hóa? Hôn nhân vốn dĩ là việc của cả 2 người, cảm thấy hợp thì đến với nhau thôi, chọn với lựa cái gì? Ai chọn ai?

"Tuổi thanh xuân của họ trôi qua đơn chiếc chỉ vì tính chất phi thực tế của khung tiêu chuẩn chọn chồng mà họ đặt ra, thật khó để đàn ông đáp ứng kỳ vọng của họ." Tại sao tác giả lại nghĩ rằng những người phụ nữ đến ngưỡng tuổi 30 mà chưa có chồng là do họ đã đặt tiêu chuẩn quá cao? Vậy như thế nào là vừa đủ? Căn bản, hôn nhân là phải có tình yêu, chưa gặp được người mình yêu thích thì làm sao yêu và cưới cho được? Người ngoài đường tuy nhiều, nhưng để tìm được người tâm đầu ý hợp k phải dễ. Chưa kể còn vì nhiều nguyên nhân cá nhân và xã hội khác. 

Phụ nữ 30 sẽ suy nghĩ chín chắn hơn, họ biết mình cần gì và nên làm gì, trễ nhưng chưa hẳn đã là không tốt. Nhiều người phụ nữ lập gia đình sớm và chia tay sớm, và như vậy họ chưa đến 30 mà đã có 1 đời chồng và 1 đứa con. Như vậy liệu có tốt? Tất nhiên ở đây mình không đề cập đến những người lập gia đình sớm và may mắn hạnh phúc.
1
112457
2013-11-01 22:58:22
--------------------------
