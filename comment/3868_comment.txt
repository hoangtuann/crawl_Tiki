201938
3868
Từ xưa tới nay Trung Quốc luôn lăm le muốn biến nước ta thành một quận của chúng. Gần đây nhất, chúng hạ đặt giàn khoan HD 981 xuống lãnh hãi nước ta khiến nhân dân cả nước sôi sục. Để tìm hiểu thêm, mình đã quyết định mua cuốn sách này về đọc. Thực sự đây là một cuốn sách hay, rất bổ ích. Cuốn sách nêu rất chi tiết, đầy đủ các vấn đề liên quan đến tranh chấp chủ quyền trên Biển Đông cũng như hai quần đảo Trường Sa và Hoàng Sa giữa Việt Nam với Trung Quốc. Mọi người nên đọc để hiểu kĩ. Đây thực sự là cuốn sách đáng đọc.
5
606127
2015-05-28 15:44:20
--------------------------
