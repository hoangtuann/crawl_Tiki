283940
7266
Gunter Pauli đã cho thấy sự thay đổi trong mô hình kinh doanh hiện nay, ông kêu gọi sự khôn ngoan của sinh học, môi trường để tạo ra một cơ cấu kinh doanh mới trong ngành công nghiệp, tập trung vào sự liên kết của các hệ thống, phạm vi của các mô hình kinh doanh và nhu cầu không thể thiếu của các hiệp lực giữa môi trường, kinh tế và xã hội. Nếu ai có kế hoạch nghiên cứu khoa học môi trường hay kinh doanh hàng hóa xanh, hoặc làm việc cho một công ty, mua một sản phẩm được sản xuất từ một công ty thì tôi đề cử bạn nên đọc cuốn sách này.
4
167618
2015-08-30 14:37:27
--------------------------
262508
7266
Mình là 1 đứa rất chậm về môn Hoá đấy nhé. Thế mà không hiểu sao vẫn có thể đọc và hiểu về sách này 1 cách trôi chảy như vậy, thậm chí còn mày mò tim hiểu thêm. Cuốn sách cho mình thấy có rất nhiều bước đi mới trong phát triển kinh tế mà không nhất thiết cứ phải đi ngược lại với tự nhiên. "Ít độc hại không có nghĩa là không độc hai". Thế đấy, cho nên dù thế nào kinh tế với môi trường không thể tách rời được. Nó là con đường lâu dài và bền vững nhất. Sách còn có rất nhiều gợi ý có thể tham khảo được, thực sụ rất muốn có tiềm lực để sở hữu 1 doanh nghiệp ngay bây giờ.
5
281150
2015-08-11 22:26:48
--------------------------
211207
7266
Gunter Pauli đã minh họa cho sự thay đổi trong mô hình kinh doanh hiện nay. Thay vì chủ nghĩa tư bản truyền thống, tuyến tính, và nền kinh tế tập trung, Pauli kêu gọi sự khôn ngoan của sinh học, môi trường để thiết kế một cơ cấu kinh doanh mới trong ngành công nghiệp, tập trung vào sự liên kết của các hệ thống, phạm vi của các mô hình kinh doanh và nhu cầu không thể thiếu của các hiệp lực giữa môi trường, kinh tế và xã hội.  Bất cứ ai có kế hoạch nghiên cứu kinh doanh, bắt đầu một công ty, công ty hoạt động, làm việc cho một công ty, hoặc mua một sản phẩm được sản xuất từ một công ty thì nên đọc cuốn sách này :)))))))))
5
519242
2015-06-20 10:42:51
--------------------------
168719
7266
Ô nhiễm môi trường do các công ty hay nhà máy gây ra là một điều không có gì xa lạ trong thế giới ngày nay. Ở nước ta, nếu đi qua những khu công nghiệp hiện đại hay chỉ là những làng mạc sản xuất thủ công, bạn sẽ thấy những ống khói to đùng thải những cột khói cao ngút và đen ngòm lên bầu không khí, cái bầu không khí vẫn nuôi sống chúng ta hằng ngày. Hay tâm điểm là công ty bột ngọt Vedan thải nước thải chưa qua xử lí ra sông Thị Vải và biến nó thành một dòng sông "chết". Tất cả những điều đó khiến mọi người vô tình có ác cảm với giới kinh doanh, chụp mũ giới kinh doanh là những người phá hoại môi trường làm ảnh hưởng không nhỏ tới những người kinh doanh thân thiện với môi trường sinh thái. Tác phẩm Nền Kinh Tế Xanh Lam sẽ cho chúng ta biết môi trường và kinh tế có thể hợp tác với nhau hay không và cách giải quyết vấn đề, thách thức lớn nhất của toàn nhân loại ngày nay - vấn đề môi trường sinh thái.
5
387632
2015-03-17 07:36:10
--------------------------
