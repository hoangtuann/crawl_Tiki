351499
6271
Mình thích cuốn này nhất trong toàn bộ series lão kẹo gôm. Tuy ban đầu đọc hơi khó hiểu nhưng càng về sau sẽ càng lôi cuốn.Quyển sách là sự pha trộn giữa những yếu tố nhân văn để lại những bài học đáng để suy ngẫm cùng với nhưng yếu tố hài hước dí dỏm rất thích hợp cho việc đọc giải trí trong những lúc căng thẳng.Chúng ta sẽ được bước vào một thế giới phiên lưu  đầy vui nhộn của Polly và Mr. Thứ Sáu cùng với những người bạn của họ (chú chó Jake và tỉ phú bánh gừng) để chống lại những âm mưu của lão kẹo gôm độc ác. Thật sự đây là một quyển sách đáng để đọc.
5
264321
2015-12-12 12:25:31
--------------------------
345288
6271
Nằm trong series truyện nổi tiếng Lão Kẹo Gôm dành cho thiếu nhi, phần truyện này khá hay. Trong truyện có những tình tiết vui nhộn mà ẩn sau đó là những bài học về tình bạn, cách đối nhân xử thế. Truyện còn có những hình minh họa vui nhộn, giúp người đọc không nhàm chán. Truyện nay fraats thích hợp cho lứa tuổi thiếu nhi. Thật tiếc nhưng trong truyện có những từ ngữ khá khó hiểu, không mượt mà cho các em nhỏ, đọc khá ngang.
Giấy màu vàng ngà chống lóa, giấy mỏng, nhẹ. Chữ to, rõ ràng, mực nét.
Bìa dày, gập, sách vuông và nhìn tươi sáng, đẹp.
4
800850
2015-11-30 10:26:54
--------------------------
233163
6271
Cuốn sách này gây ấn tượng rất đặc biệt đối với mình. Trí tưởng tượng vô cùng phong phú và hài hước của Andy Stanton cùng với nét vẽ minh họa vô cùng đặc biệt và dí dỏm của David Tazzyman đã tạo nên một tác phẩm xuất sắc, kể về chuyến phiêu lưu vui nhộn của cô bé Polly cùng ông Thứ Sáu O'Leary, Lão Bà Bà, chú chó Jake,..... Nhưng những tập sau đã không giữ được phong độ như vậy, vẫn hài hước đấy nhưng không bằng tập 1. Dù sao cuốn mở đầu cho series của Lão Kẹo Gôm này đã để lại ấn tượng vô cùng sâu sắc trong mình.
4
307134
2015-07-19 15:07:09
--------------------------
145860
6271
Có thể khi mới bắt đầu người đọc sẽ không thể hiểu rõ những nội dung và bài học mà truyện đem lại. Nhưng ẩn sau trong những tình tiết hài hước và vui nhộn đó là những bài học quý giá có thể là về tình bạn, giá trị của những đồng tiền, những kí ức không nên bao giờ đánh đổi,... được ẩn giấu bên trong. Mà chỉ khi người đọc đọc thật kĩ tác phẩm mới có thể thấy được.
Về khuyết điểm thì đối vối tôi, điều đáng tiếc duy nhất chính là bản dịch chưa thật sự sát với nghĩa mà tác giả muốn thể hiện nên đã vô tình góp phần khiến cho tác phẩm trở nên hơi khó hiểu- nhất là đối với những lứa tuổi thiếu nhi.
5
458188
2015-01-01 12:50:09
--------------------------
