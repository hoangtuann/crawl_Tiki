226744
3535
Về hình thức: Sách đẹp, được in bằng giấy phần lan: mỏng, nhẹ. Bìa được in nổi khá bắt mắt và mang đậm nét cổ điển cần có của truyện cổ tích
Về bố cục: Việc chia bộ sách thành 4 quyển và in cỡ vừa là hợp lý, khi cầm quyển truyện dễ đọc, dễ mang theo
Về nội dung: Bản dịch khá tốt, không mắc nhiều lỗi chính tả. Trong cuốn 1 có câu truyện Cô bé bán diêm mà tôi đã yêu thích từ lâu
Tuy nhiên, tôi thấy để thu hút trẻ em đọc nhiều hơn, cần có thêm một số tranh minh họa cho sách.
3
663937
2015-07-12 21:19:30
--------------------------
