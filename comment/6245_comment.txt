465941
6245
Cuốn sách này mình được một người bạn giới thiệu. Trước đây mình chưa từng đọc cuốn sách nào của Dan Brown, nhưng sau khi đọc "Điểm dối lừa", chắc chắn mình sẽ tìm đọc thêm nhiều các tác phẩm khác của ông. Về nội dung, tuyệt vời, không chỉ về khả năng viết, truyền tải của tác giả, mà còn là sự kỳ công tìm hiểu về những công nghệ KH-KT được đề cấp trong tác phẩm của ông. Về công tác dịch thuật, dịch tốt, dễ hiểu những từ khó chuyên ngành, mạch lạc. Tuy nhiên có 1 điểm trừ vô cùng lớn, đó là sách có quá nhiều lỗi chính tả, đặc biệt là tên nhân vật, và khá nhiều chỗ sai chính tả. Có thể là vì hơi cầu toàn, nên lỗi này làm mình khá khó chịu, nhưng nếu bỏ qua lỗi này thì đây quả là một cuốn sách đáng để đọc.
3
368311
2016-07-01 16:20:10
--------------------------
452279
6245
Thực sự thì tôi rất hâm mộ tác giả này nên mua hẳn quyển bìa cứng về đọc và ngắm nghía cho thích thú. Quyển truyện này rất thu hút ánh nhìn người đọc cả về tiêu đề lẫn hình minh họa. Không những thế, vì nó là bìa cứng nên khi nhìn rất đẹp khá bắt mắt. Nội dung ổn, không tệ đối với tôi. Giá cả hợp lí, phải chăng, mua về ngoài để đọc còn có thể trưng bày trên kệ sách trông cũng có tính thẩm mĩ khá cao. Truyện khá dày nhưng không gây cảm giác nhàm chán.
5
292697
2016-06-20 15:49:03
--------------------------
399351
6245
Truyện của Dan Brown thì ... không còn gì để bàn cãi. Hấp dẫn, lôi cuốn, hồi hộp... đủ cả. Phải công nhận Dan Brown có vốn kiến thức quá phong phú, giống một cuốn bách khoa toàn thư sống. Ông rất biết cách tạo hứng khởi cho người đọc, dẫn dắt một cách nhẹ nhàng, khéo léo. Người đọc cứ đi từ bí mật này đến bí mật khác, mỗi một bí ẩn được giải đáp lại có một bí ẩn khác trong nó. Và truyện này ... mình nhớ nhất cái chi tiết cuối cuối cuối cùng, chi tiết " con cá ngựa ". :'')
4
145923
2016-03-17 17:23:32
--------------------------
374464
6245
Đọc xong truyện mình rất đỗi bất ngờ về kết thúc. Tình tiết nhanh nhưng đôi lúc chững lại. Sau 4 ngày đọc mình đã hoàn thành cuốn nay. Tuy nhiên, mình vẫn thích tác phẩm Thiên thần và ác quỷ hơn. Cuốn sách dày, to và bìa cứng có lớp bìa mỏng láng bao bên ngoài nên bảo quản được lâu. Ban đầu mình mở sách nghe mùi kì kì không thích lắm. Nghe mùi đó xong là đóng lại liền chắc do mũi nhạy cảm quá. Để 1 thời gian thì cũng hết. Đây là cuốn sách rất đáng đọc
3
1023964
2016-01-26 12:50:05
--------------------------
367999
6245
Biết đến Dan Brown qua tác phẩm "Thiên Thần và ác quỷ", nhưng đến khi đọc "Điểm dối lừa", tôi chợt nhận ra ông đúng là một nhà văn thiên tài. Mỗi tác phẩm đều được thai nghén nhiều năm, tập hợp những kiến thức sâu rộng và mới lạ. Dù đó là tri thức về các loại mật mã, về thiên văn học, kí hiệu,... nhưng lại được Dan Brown chuyển tải một cách tài ba qua từng trang sách, khiến nó không quá xa lạ đối với bạn đọc mà kích thích trí tò mò và tính hiếu kì của họ. Tác phẩm "Pháo đài số" là một trong những minh chứng cho điều đó. Với sự tài tình trong việc tạo dựng cốt truyện kịch tính và đầy bất ngờ, tác giả đã cuốn người đọc theo từng bước chân của nhân vật; những tính huống li kì, những chi tiết tưởng như nhỏ vụn nhưng là then chốt của cả câu chuyện, nhà văn "Mật mã Da Vinci" đã tạo nên một tác phẩm cực kì xuất sắc.
5
410238
2016-01-13 09:12:15
--------------------------
365435
6245
Từ những chi tiết vụn vặt và có vẻ chẳng liên quan gì nhau, tác giả đã dẫn người đọc vào 1 câu truyện ly kỳ và hồi hộp. Tôi đã không thể rời được quyển sách khi chưa tìm được câu trả lời cuối cùng cho tất cả những câu hỏi mà tác giả đã bắt tôi tìm tòi. Gấp cuốn sách lại sau 1 đêm, tôi cảm thấy mình cần tìm thêm những cuốn khác của Dan Brown, để đọc, để nghiền ngẫm, để yêu mến. Quả thật, tôi sợ đọc sách của ông, nhưng lại kinh ngạc và chìm sâu vào đến nỗi muốn gặp ông để hỏi tại sao lại có thể làm được như vậy. Trong tôi dấy lên sự kính trọng và yêu mến tác giả. Ông đã có thêm 1 fan hâm mộ mất rồi.
4
687129
2016-01-08 05:22:24
--------------------------
363634
6245
Mình đọc những tác phẩm khác của DanBrown trước rồi mới đọc quyển này nên cứ nghĩ là quyển này cũng viết về Robert nên khi đọc có phần bị hớ. Tuy nhiên mình thấy các nhân vật trong tác phẩm này cũng có sức hút riêng. Họ thông minh, tinh tế và biết đưa ra quyết định khôn ngoan trong các tình huống éo le để có thể sống xót đến phút chót. Tác phẩm này không đề cập đến những góc nhìn cao siêu về văn hóa, tôn giáo như trong Biểu tượng thất truyền, Thiên thần và ác quỷ nhưng vẫn thu hút người xem. Những công nghệ, thiết bị tiên tiến luôn là điểm nổi bật của DanBrown. Đọc xong tác phẩm mình có cái nhìn mới về NASA, không phải mọi thứ điều hoàn hảo. Đây là tác phẩm rất đáng để xem!!
5
785604
2016-01-04 18:12:11
--------------------------
362157
6245
Là một độc giả mê sách của Dan Brown và luôn muốn sở hữu những cuốn sách của ông. Tiểu thuyết của ông cực lôi cuốn người đọc, đặc biệt là những người yêu thích tiểu thuyết trinh thám và khoa học giả tưởng. về cuốn Điểm dối lừa, nội dung thì rất hay, bất ngờ đưa người đọc từ suy luận này đến suy luận khác, còn về giấy thì hơi cũ một chút, và trọng lượng của nó cũng nhẹ hơn những cuốn sách khác của Dan Brown.
Hiện tại mình vẫn chờ đợi Tiki mang về cuốn "Mật mã Da Vinci". Hóng lâu lắm rồi.
3
1087297
2016-01-01 13:29:45
--------------------------
359588
6245
Đã từng đọc qua Bí mật Davinci của Dan Brown và mình đã hoàn toàn bị chinh phục bởi những tình tiết gay cấn mà ông dựng nên, những kiến thức phong phú và mới lạ mà ông đã mang đến cho độc giả. Chính vì thế mà mình đã không ngần ngại khi mua tiếp tác phẩm của ông- Điểm Dối Lừa. Phải nói là qua tác phẩm này, Dan Brown đã mở ra và dẫn dắt người đọc đến với những vấn đề khác nhau, những lĩnh vực khác như là hóa học, vũ trụ, chính trị, quân sự......Và đó cũng chính là yếu tố giúp tác phẩm của ông trở thành tác phẩm yêu thích và bán chạy nhất. Quyển sách Điểm Dối Lừa theo minh thấy là khá quyển sách mà không đọc thì đúng là rất phí!!!
5
190618
2015-12-27 14:22:38
--------------------------
316445
6245
Đọc nguyên quyền chỉ trong 2 ngày, quá lôi cuốn . . . . . . . . . . .
Hình thức: Đẹp, bìa cứng, giấy tốt, nhẹ. Bìa bóng không đẹp lắm, thích bìa sần giống Davincy hơn. Chữ khá lớn dẫn đến sách khá dày, gần 800 trang nhưng giấy tốt nên vẫn nhẹ, Theo mình nên giảm cỡ chữ xuống 2 cỡ nữa.
Nội dung: Nhận xét chung là khá hay, lôi cuốn, đáng đọc. Theo ý kiến của riêng mình thì kém lôi cuốn, gây tò mò nếu so sánh với Davincy. Kết thúc cũng không bất ngờ bằng. 

5
85763
2015-09-30 21:23:40
--------------------------
308535
6245
Đối với những ai mới lần đầu đọc tác phẩm của Dan Brown thì cuốn này có lẽ sẽ rất hấp dẫn và cuốn hút. Tuy nhiên, mình lại thấy cuốn này không bằng những cuốn khác viết về Robert Langdon. Mạch truyện tuy vẫn hồi hộp và gay cấn nhưng không bất ngờ bằng những cuốn khác. Thật ra mình đoán được kẻ đứng sau mọi chuyện từ khá sớm nên có giảm đi chút hồi hộp khi đến đoạn kết. Những cảnh rượt đuổi vẫn ngoạn mục và nghẹt thở, lượng kiến thức và những điều mới mẻ vẫn đầy ắp cuốn truyện như những cuốn khác của Dan Brown. Vì vậy nên dù sao thì đây cũng là một cuốn không thể bỏ qua đối với cả fan của Dan Brown và những người yêu thích thể loại hành động, phiêu lưu mới đọc tác phẩm của ông lần đầu.
4
234502
2015-09-18 19:19:19
--------------------------
306468
6245
So với cuốn Hỏa Ngục mua cùng lúc, cuốn Điểm dối lừa có chất lượng in ấn như sách in lậu bán ngoài đường. Chất lượng giấy xấu, thô ráp, dính trang. Lỗi chính tả. Mực in không đều. Nói chung là không hài lòng khi cầm cuốn sách trên tay.
Về nội dung, vẫn lối viết cực kỳ gay cấn, hấp dẫn của Dan Brown. Vẫn cho mình cảm giác hồi hộp từng trang sách như các tác phẩm khác của ông mà mình đã từng đọc. Nội dung khác với Mật mã Da Vinci, Thiên thần Ác quỷ, Hỏa ngục khi không còn giải đố, nhưng vẫn hấp dẫn theo phong cách của Dan Brown.
3
6077
2015-09-17 16:39:48
--------------------------
293269
6245
Mình đã đọc tác phẩm "Điểm dối lừa" trước đây nhưng chỉ là mượn sách của bạn, nên mình vẫn luôn muốn mua riêng cho mình một quyển. Nhờ đọc tác phẩm này mà mình mới "nghiền" Dan Brown và tìm tới các tác phẩm khác của ông như "Mật mã Da Vinci", "Thiên thần và ác quỷ",.... Các câu chuyện của Dan Brown đều mang đến cho người đọc cảm giác phiêu lưu, hồi hộp, mạo hiểm và rất kịch tính. Và cảm ơn tiki đã tạo điều kiện để mình có được những cuốn sách này với giác cả rất phải chăng. 
5
625458
2015-09-08 09:45:50
--------------------------
276435
6245
Woa ! nếu bạn đã đọc " Bộ của giáo sư Langdon " thì bạn sẽ thấy rằng chẳng có bất ngờ gì đối với bộ sách này vì nếu " Mật mã Da Vinci " đã quá thành công xuất sắc quá rực rỡ thì bạn sẽ thấy quyển sách này sẽ sẽ cực kì hơi hơi hụt hẫng một tí tẹo teo, bởi vì nó mất mất một thứ gì đó của màu sắc cực kì hấp hấp dẫn và cuốn hút mà Dan Brown đã mang lại từ bộ của giáo sư Langdon thì câu chuyện này thiếu một chút màu sắc bí ẩn , hay một chút mù sương trong mạch chuyện một chút, nó thiếu đi tính liên kết hay một chút gì đó bật mí hay bí mật trong một câu chuyện thế này, nhưng điểm cộng nó là thì mạch chuyện vẫn lôi cuốn với những bạn nào lần đầu đến với tác giả.  
3
36336
2015-08-23 23:38:23
--------------------------
261177
6245
Vẫn là phong cách quen thuộc của Dan Brown : sự kết hợp giữa những thông tin thực tế ( các tổ chức, kĩ thuật khoa học trong cuốn truyện hoàn toàn có thật) và những tình tiết giả tưởng, cách dẫn truyện tạo ra sự cuốn hút khiến độc giả không thể dừng đọc và 1 cái kết cục không thể bất ngờ hơn :D. Tuy nhiên những kiến thức trong truyện khá khó hiểu và tưởng tượng. 
Nhìn chung, tuy vẫn chưa thể so sánh với những cuốn sách khác của Dan Brown như "Mật mã Da Vinci", "Mật mã thất truyền" nhưng vẫn là 1 cuốn sách đáng đọc đáng mua cho các bạn thích thể loại li kì hấp dẫn :)
4
489443
2015-08-10 22:05:56
--------------------------
258120
6245

Người dịch:		Văn Thị Thanh Bình
NXB:			Văn Hóa Thông Tin
Thể loại:		Trinh thám khoa học
Bìa:			Đẹp – Ấn Tượng
Chất lượng in, giấy: Tốt
Chất lượng dịch:	Tốt
Nhận xét:		Lĩnh vực mới về công nghệ chiến tranh bậc cao. Rất hay cho bạn đọc muốn biết về vũ khí kỹ thuật cao, “bí kíp” quân sự hiện đại, ám sát như là tai nạn… Quá hay và mạnh mẽ. Nếu bạn có một trái tim yếu đuối thì không nên đọc cuốn này vì đơn giản nó quá hồi hộp, kịch tính ngay từ dòng đầu tiên. Tuy nhiên có có những khái niệm khoa học hơi khó nhằn.
Khuyên: 		Rất hay, nên đọc.

5
129879
2015-08-08 11:06:40
--------------------------
251150
6245
Mình rất khâm phục trí óc tưởng tượng của tác giả Dan Brown lần này. Câu chuyện được đặt trên nền tảng quá đỗi bình thường mà có thể phát triển thăng hoa đến vậy. Đọc câu chuyện này mà run lắm luôn á. Dan Brown miêu tả  một cách rất tài tình các vụ án trong cái lạnh lẽo đến thấu xương trên đỉnh núi, những mối nguy hiểm rình rập, âm mưu từng giờ từng giây từng phút đang kề cận đe dọa mạng sống của đoàn người đó. Mình thích hai nhân vật chính, lúc nào cũng đủ bình tĩnh và lí trí.
4
120193
2015-08-02 14:39:25
--------------------------
248497
6245
Lại một tác phẩm xuất sắc nữa của tác giả Dan Brown. "Điểm dối lừa" hé lộ một bí mật khủng khiếp của Nasa. Hàng loạt những câu hỏi, rất nhiều điểm nghi vấn, vô số chi tiết đáng lưu ý. Những vấn đề khúc mắt đan xen lẫn nhau nhưng sẽ lần lượt đươc giải đáp thông qua từng chương. Vẫn như những tác phẩm khác, hàm lượng chất xám vô cùng dồi dào được truyền tải thông qua những trang sách. Chất lượng in đợt này khá tốt nhưng trình bày bìa hơi đơn điệu, mong lần tái bản sau bìa sách sẽ đẹp hơn
4
472338
2015-07-31 10:01:57
--------------------------
245829
6245
Sau khi đọc Mật mã Da Vinci, mình đã tìm đọc hết tất cả những tác phẩm còn lại của Dan Brown. Và không lạ gì khi mình tìm đến "Điểm dối lừa", một trong những tác phẩm đặc sắc của ông. Vẫn phong cách đó, vẫn là một bất ngờ lớn mà Dan Brown đem lại trong mỗi câu chuyện. "Điểm dối lừa", quả đúng như tên gọi, một cú lừa ngoạn mục, vô cùng bất ngờ. Nhưng mình vẫn cảm giác đoạn kết có vẻ hơi hụt, hay ít nhất là không trơn tru như "Thiên thần và ác quỉ" hay "Biểu tương thất truyền". Về phần bìa và giấy, mình khá hài lòng. Bìa cứng và giấy xốp nhẹ, giúp cầm sách rất dễ và bảo quản cũng tốt hơn.
4
172460
2015-07-29 02:10:26
--------------------------
239112
6245
Đây là cuốn sách thứ tư của tác giả Dan Brown mà mình đọc. Nội dung cuốn hút, gay cấn ngay từ những chương đầu tiên, làm mình không sao rời mắt ra được. Nhưng mình vẫn chỉ tặng cho cuốn sách này 4 sao, vì đoạn sau làm mình hơi hụt hẫng. Mình thấy kể cả cái kết của câu chuyện cũng có chút dễ dãi và khá là thiếu tính logic. Nói chung, dù không hay đến mức tuyệt đối, nhưng "Điểm dối lừa" là cuốn sách gối đầu giường lí tưởng nếu bạn đam mê thể loại trinh thám
4
360043
2015-07-23 17:32:49
--------------------------
221364
6245
Một cuốn sách tuyệt vời và hấp dẫn và lại một lần nữa chúng ta lại học được rất nhiều kiến thức từ những tác phẩm của Dan Brown. Mặc dù mình không đánh giá quyển sách này bằng "Pháo đài số" hay là "Mật mã Da Vinci"; nhưng đây là tác phẩm đã cho mọi người thấy "chính trị" nó bí ẩn và hiểm ác như thế nào. Đọc xong sẽ thấy rằng các chính trị gia ít nhiều đều có lợi dụng đất nước để mưu lợi cho mình, có điều ai có tâm thì làm việc "lợi nước lợi mình" thôi.
4
230629
2015-07-03 19:51:48
--------------------------
219258
6245
Mình mua vào ngày 12/12/2014, đầu tiên mình rất thích bìa sách. Bìa sách đẹp, bền, chi tiết độc đáo và ấn tượng.
Thứ hai, nội dung rất ly kỳ, kịch tính, hấp dẫn, có nhiều điểm nhấn, bên cạnh đó Dan còn viết về lịch sử, văn hóa, chính trị sâu sắc, không nhàm chán, bổ trợ tri thức và đọng lại được trong lòng người đọc
Thứ ba, tuy viết theo thể loại trinh thám nhưng nội dung không khô khan, cứng nhắc mà có sự uyển chuyển, dàn trải nội tâm rất đều và phong phú, các tình tiết logic và phần kết cục để lại suy nghĩ cho nhiều người
Nếu bạn đang phân vân về việc mua quyển sách này thì hãy mua ngay đi! Bạn sẽ không hối hận đâu :)
5
508055
2015-07-01 12:07:45
--------------------------
217151
6245
Cũng như bao cuốn sách khác của tác giả Dan Brown, "Điểm Dối Lừa" vẫn là cuốn sách vô cùng hấp dẫn và lôi cuốn, mặc dù không nằm trong seri của Robert Langdon. Đọc xong cuốn sách, ta mới thấy được bộ mặt của "chính trị" thực sự là như thế nào, những mưu đồ, những bí mật ẩn sâu đằng sau những cuộc phát kiến. Cả cuốn sách là liên tiếp những chuỗi sự kiện dồn dập, kịch tính diễn ra vỏn vẹn trong vòng...1 ngày. 
Về hình thức, mình cảm thấy khá hài lòng. Dù vẫn còn lỗi chính tả nhưng ít hơn hẳn so với những quyển đã xuất bản. Sách bìa cứng, lại có thêm 1 bìa rời và bao ni lông bọc ngoài. Giấy màu ngà đọc lâu không bị mỏi mắt. Nhìn có vẻ đồ sộ nhưng khi cầm lên quyển sách cũng khá nhẹ.
Nói chung, mình rất thích quyển sách và sẽ luôn ủng hộ tác giả Dan Brown.
5
178929
2015-06-28 22:14:24
--------------------------
207731
6245
Mặc dù tác phẩm Điểm dối lừa không được hay như những tác phẩm đương thời của Dan Brown như: Pháo đài số, Mật mã Da Vanci, Hỏa ngục,… nhưng đây vẫn là một tác phẩm hay với cốt truyện được Dan Brown xây dựng một cách tài tình và hấp dẫn. Từng tình tiết câu chuyện, từng lời thoại của nhân vật rất lôi cuốn người đọc, cho người đọc thấy được những âm mưu thâm hiểm của những người làm chính trị, đi sâu vào những tình tiết đến nghẹt thở, càng đọc thì chỉ muốn đọc nữa, đọc cho hết cuốn truyện. Đây quả là một cuốn tiểu thuyết khoa học giả tưởng hay nhất mà mình từng đọc.
4
527557
2015-06-13 00:38:51
--------------------------
200458
6245
Với nội dung về cuộc phiêu lưu của nữ chuyên gia phân tích tình báo Rachel Sexton. Điểm dối lừa đặt người đọc vào tầng tầng lớp lớp những mối nghi ngờ giữa những vụ ám sát tưởng chừng vô căn cứ để che đậy một bí mật khổng lồ của chính phủ Mỹ. Tác giả áp dụng những kiến thức khoa học uyên bác nhưng không kém phần thú vị làm nổi bật lên những tình tiết của cốt truyện lôi kéo người đọc qua một chuỗi những phát hiện không ngờ xuyên suốt toàn bộ câu chuyện tưởng chừng rất thường thấy.

Sự lừa dối, đôi khi chúng diễn ra ở mức độ tinh vi với quy mô khó thể tưởng tượng. Điểm Dối Lừa đánh vào tâm lý của người đọc với một câu hỏi khiến nhiều người tranh luận. Con người ta có thể tự chủ được không khi đứng trước lựa chọn giữa lợi ích và vấn đề luân lý?
4
455391
2015-05-25 02:24:23
--------------------------
198480
6245
Tác giả có lối dẫn dắt câu chuyện gợi nên sự tò mò ngay từ đầu câu chuyện, khiến người đọc trở nên bị cuốn hút, rồi khi mọi thứ tưởng chừng như đã rõ ràng thì đó cũng là thời điểm bắt đầu một hành trình mới của câu chuyện. Mọi tình tiết trở nên gay cấn vừa giống với truyện trinh thám, lại vừa giống như một bộ phim hành động, những lập luận logic của những người-làm-khoa-học-đúng-nghĩa với những hiểm nguy mà họ phải đối mặt trong quá trình lần mò ra tất cả sự thật. Rồi cuối cũng mọi thứ đều được sáng tỏ sau những tình tiết bất ngờ và ngỡ ngàng. Có một điều luôn đúng: chính nghĩa luôn đi liền với sự thật.
 Thưởng thức tác phẩm ta còn phải khâm phục tác giả về tầm hiểu biết thật rộng trên nhiều lĩnh vực, các công nghệ về kĩ thuật quân sự quốc phòng, hệ thống chính trị, hàng không vũ trụ, cả hải dương học, băng hà học, địa chất học... Thật khiến cho người đọc phải bị thuyết phục.
  Nhận xét một chút về phần cứng: sách bìa cứng,có bọc một lớp bìa ngoài. giấy dày nhưng đặc biệt nhẹ và màu giấy hơi nâu thích hợp để không bị mỏi mắt. mình thích những loại giấy như vậy.
4
355441
2015-05-20 01:18:07
--------------------------
189085
6245
Càng đọc tác phẩm của Dan Brown mình càng cảm thấy bị thuyết phục thực sự vì đầu óc thông minh cực kỳ của ông và lượng kiến thức khổng lồ của ông, cộng thêm lối hành văn chương rất hấp dẫn rất độc đáo, gay cấn, kịch tính, đầy hồi hộp. Lần này là về đề tài thám hiểm vũ trụ, ông đã biến những điều không tưởng, những điều ta tưởng tượng ra trở nên rất thực rất logic, rất đúng với đời thường. Sức mạnh ngôn ngữ mà Dan Brown có quả thực rất ghê gớm, rất mong những tác phẩm tiếp theo của ông.
5
530790
2015-04-26 14:50:26
--------------------------
166711
6245
Đây là cuốn sách đầu tiên của Dan Brown mà mình đọc, và đã đọc một cách chăm chú cho đến trang cuối cùng. Cốt truyện khoa học viễn tưởng thì thật sự không mới, những công trình nghiên cứu, những phát kiến khoa học cũng không  quá ngạc nhiên. Tuy nhiên, cách đưa tất cả những công trình đó vào truyện của Dan Brown khiến người đọc phải nhìn nó bằng sự thán phục. Những âm mưu chính trị ẩn dấu sau những công trình nghiên cứu khoa học cực kỳ đắt đỏ và khó khăn, những con người hiểu và có thể thay đổi kết quả theo ý mình, có lợi cho con đường chính trị của mình nhất. Khi khoa học được đánh giá và phục vụ cho mục đích chính trị thì sẽ không có giá trị đối với lịch sử và thời đại nữa, nó đơn thuần chỉ là một tác động mang tính thời điểm mà thôi. Tuy nhiên, dù ở đâu hay thời điểm nào, con người ta vẫn luôn tìm được những cá nhân hay một cộng đồng, những người tâm huyết và bảo vệ những gái trị thuần túy!
4
120479
2015-03-13 11:48:33
--------------------------
157682
6245
Trong "Điểm Dối Lừa", ta được tác giả đưa vào một trong những khía cạnh đen tối về chính trị của Mỹ, đó là bầu cử. Thông qua cuộc đua vào nhà Trắng của Thượng Nghị Sĩ Sexton đã dẫn tới một trong những vụ dàn dựng công phu và hao tốn công sức nhất liên quan tới Nasa. Một thiên thạch có chứa những sinh vật bên trong, không chỉ là phát kiến của một quốc gia. Nó sẽ thay đổi toàn bộ lịch sử và sẽ là thứ không bao giờ bị xóa đi. Nhưng đằng sau thiên thạch tưởng chừng như đơn giản và quý báu kia là một âm mưu được đạo diễn bởi một trong những bàn tay đứng đầu Chính phủ Mỹ. Đã có nhiều người phải chết để cho bí mật kia không được phát hiện ra, nhưng cuối cùng nhờ vào bản năng sống và tình yêu mà những nhân vật chính đã vượt qua được cái chết trong gang tấc.Tôi rất thích truyện của Dan Brown không chỉ vì tính giải trí cao mà nó mang lại mà truyện còn phản ánh một cách chân thực và kèm với đó là những kiến thức về mọi mặt trong đời sống khó tìm thấy ở những quyển sách khác.
5
291067
2015-02-09 21:28:20
--------------------------
157106
6245
Các tác phẩm của Dan Brown luôn mang người đọc từ bất ngờ, hồi hộp này đến  bất ngờ, hồi hợp khác. Các tình tiết ly kì và lượng thông tin nhiều vô số khiến người đọc hoài nghi và bán tín bán nghi về tính chân thật của nó. Nói chung, đây là một câu truyện khoa học viễn tưởng vô cùng đặc sắc, tuy là một cuốn truyện giả tưởng nhưng khi đọc mình hoàn toàn bị cuốn vào các tình tiết và diễn biến của truyện đến nỗi mình tin luôn rằng đó là sự thật. Đó là một nét thú vị khi đọc các tác phẩm của Dan Brown. Bên cạnh đó, cuốn sách này làm mình khá "xoắn não" trong cái việc tuyên đoán, đúng và sai cứ lầm lẫn cả lên, đúng là cái "điểm dối lừa".
5
392352
2015-02-07 19:19:42
--------------------------
146371
6245
Vẫn mô tuýp cũ như các tác phẩm trước của Dan Brown, luôn hấp dẫn người đọc bởi rất nhiều tình tiết ly kỳ. Luôn có một lượng thông tin vô cùng lớn nhưng lại không khiến cho người đọc cảm thấy nhàm chán, mà lại còn thấy thích thú.
Tuy nhiên, theo cá nhân mình thì trong tác phẩm này Dan Brown không còn gây bất ngờ tuyệt đối cho mình như 2 tác phẩm trước mình đã đọc là "Hỏa Ngục" và "Thiên thần và ác quỷ", có nhiều chỗ đọc là đoán được luôn :'(
Sách của Dan Brown luôn được đảm bảo và được chứng thực bởi rất nhiều người rồi. Thêm 1 lý do cho những bạn chưa sở hữu quyển sách này có thể quyết định mua mà không phải phân vân đó chính là: Bìa sách cứng, đẹp; chất giấy tốt, in màu đều. Cả quyển chỉ có 1 lỗi duy nhất "giá mà" bị nhầm thành "giá mua" :)))))
4
388681
2015-01-03 15:48:51
--------------------------
142409
6245
Đây là một trong những quyển sách mà mình thấy là xuất sắc nhất của Dan Brown. 
Vẫn là mô tuýp quen thuộc, nhưng không hề nhàm chán, vẫn là một lượng thông tin khổng lồ nhưng được truyền thụ một cách đầy nghệ thuật khiến độc giả có cảm giác những kiến thức chuyên môn ấy " ngon hơn cả chiếc bánh Donut". 
Tôi đã bị shock từ chương này đến chương khác của câu truyện, khi mà bí mật này lại mở ra bí mật khác,cái này ngỡ là giải thích cho cái kia nhưng thực ra không phải. Đến cuối cùng, khi tất cả bí mật phơi bày thì không thể thấy được sự thật mà chỉ thấy một " điểm dối lùa" khủng khiếp. Tôi hoàn toàn bị chinh phục bởi kiến thức, logic và tình tiết của truyện
Ai là fan của trinh thám và đam mê khoa học nên mua cuốn sách này
5
97129
2014-12-18 16:38:05
--------------------------
138436
6245
Xuyên suốt tác phẩm sẽ cho bạn 1 suy nghĩ là đoán được, dù đúng là bạn đoán được nhưng đa số đoán không đúng thôi, mà nếu bạn đoán đúng - thì một vài đoạn nữa lại thấy bạn đoán sai, và cuối cùng khi bạn nghĩ mình sai thì rất có thể bạn đoán đúng.
Một tác phẩm trong danh sách của tác giả Dan Brown thì có lẽ bạn không nên bỏ qua tác phẩm nàym dù sách khá dày và nặng nhưng bạn khó mà quyết định được việc có nên bỏ sách xuống mà nghỉ ngơi hay không, thậm chí nhiều lúc vẫn quên cảm giác mỏi tay hay mắt.
Khuyết điểm duy nhất của các tác phẩm nhà văn Dan Brown có lẽ là GIÁ, khá là đắt và ít khi nằm trong danh sách giảm giá khủng.
5
451219
2014-12-02 20:27:24
--------------------------
131907
6245
Là fan hâm mộ của Dan Brown nên tôi luôn lựa chọn sách bìa cứng cho các tác phẩm của ông. Tuy nhiên nhà phát hành đã không đầu tư kỹ càng về mặt hình thức của tác phẩm. Phần dịch còn quá ít các chú thích cho những ký hiệu, tổ chức, thuật ngữ chuyên ngành. 
Ở tác phẩm này có nhiều cái mới so với lối viết thông thường của tác giả. Khá nhiều câu văn hài hước. Nội dung của tác phẩm tập trung chủ yếu vào sự truy đuổi chết chóc. Chính trong những tình huống như thế này, trí thông minh sắc sảo, nhạy bén của các nhân vật được khắc họa rõ nét. Cuốn sách còn giúp ta có cái nhìn hiện thực hơn về giới chính trị gia, về những cuộc chạy đua bầu cử. Tuy nhiên truyện thiếu đi sự ly kỳ vì chưa đọc đến cuối tác phẩm ta gần như đã biết đâu là chân tướng. Sự hấp dẫn của cuốn sách là ở những tình tiết hết sức éo le tưởng như không thể thoát được cái chết của các nhân vật, và những tình tiết về các thế lực ngầm.
4
90029
2014-10-28 12:57:53
--------------------------
131871
6245
Vẫn tới phong cách truyện lôi cuốn, đầy kịch tính, Dan Brown đã dẫn dắt người đọc vào một cuộc chiến hỗn loạn của khoa học và chính trị. Cuốn tiểu thuyết như một bữa ăn thịnh soạn, bày lên đó hàng tá các lĩnh vực như địa chất học, hải dương học, động vật học, côn trùng học, vật lý nguyên tử, vũ trụ, hóa học, quân sự, chính trị... Mỗi lý thuyết khoa học được đưa ra hoàn toàn khúc chiết mà không hề gây cảm giác nhập nhằng cho người đọc (cảm giác thường thấy khi ta phải nạp một lượng lớn câu chữ lý thuyết khô khan của vài môn học gộp lại với nhau). Kiến thức phong phú luôn là điểm mạnh trong các tác phẩm của Dan Brown.
Về hình thức, bìa sách thật ấn tượng với cách phối màu mang lại cảm giác sang trọng cho sách. Tuy là bìa cứng nhưng sách vẫn tương đối nhẹ, thuận tiện để mang theo trong ba lô, cặp sách.
5
313783
2014-10-27 23:57:35
--------------------------
130631
6245
Đây là quyển sách đầu tiên của Dan Brown mà tôi đọc. Phải nói người dịch rất mượt, đọc không khó chịu về lỗi dịch nào cả.
Về nội dung, thật sự tôi đã dành cả đêm để đọc hết cuốn sách này và thật bất ngờ đến nghẹt thở khi biết đc chân dung người đứng sau tất cả mọi chuyện. Dan Brown là 1 cây bút sáng giá trong thể loại truyện trinh thám và có vẻ như tài năng của ông vẫn luôn đc toả sáng!
5
418880
2014-10-19 09:40:03
--------------------------
122846
6245
Sau khi đọc cuốn ' mật mã Da Vinci' và thú vị không kém là cuốn "Thiên thần và ác quỷ" Tôi nghĩ tôi chẳng thể đi sai khi quyết định mua  "điểm dối lừa" và tôi đã đúng 100%. Dan Brown nhanh chóng trở thành một trong những tác giả yêu thích của tôi .
Dan Brown đã được chứng minh là một trong những nhà văn hàng đầu trong thể loại trinh thám - kinh dị - hồi hộp. Tính độc đáo từ cốt truyện, nghiên cứu chính xác đến mức đáng kinh ngạc, và khả năng thu hút sự quan tâm của người đọc từ khi bắt đầu và giữ nó cho đến từ cuối cùng trong câu cuối cùng của trang cuối cùng, làm cho ông trở thành một tác giả đặc biệt. Thêm vào đó, sau khi hoàn thành mỗi cuốn sách của Dan Brown, người đọc thường bước ra khỏi cuốn sách với nhiều kinh nghiệm đã học được nhiều hơn so với một cốt truyện.
5
400183
2014-08-28 14:11:38
--------------------------
