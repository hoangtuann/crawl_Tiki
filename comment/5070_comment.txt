466738
5070
Âm thanh và cuồng nộ, một cuốn sách quả đúng như lời chính tác giả đã nói, đầy những âm thanh và cuồng nộ.Nếu chỉ đọc qua cuốn sách lần đầu tiên thôi thì sẽ thấy rằng cuốn sách thật rắc rối và khó hiểu. nhưng chỉ cần đọc lại nó lần thứ hai thì sẽ thấy hoàn toàn khác biệt. những góc nhìn của các nhân vật, những sự kiện đan xen với nhau tạo nên một cuốn truyện đầy tính nhân văn. Một cuốn sách không nên bỏ qua
Rất mong Tiki sẽ có nhiều hơn  những cuốn sách hay như thế 
5
556738
2016-07-02 20:34:06
--------------------------
466298
5070
- Bìa sách : xấu òm,  màu trông dễ cũ, trông đúng kiểu bìa sách từ những năm xa lắc xa lơ rồi ấy. 
- Nội dung : Không dành cho những tâm hồn mong manh hay màu hồng. Đoạn độc thoại nội tâm của Benji, Quetin và Jason nhiều khi khiến mình phát điên lên được khi đọc. Mắt đọc không thì không đủ, phải dùng thêm cái đầu tỉnh táo để móc nối, chắp vá các sự kiện ở những thời điểm hiện tại - quá khứ lại với nhau, vì mọi thứ đều có nhân -quả. Từ đó, mới có thể hiểu về nhân vật cũng như bối cảnh của câu chuyện. 3 thế hệ của gia đình quý tộc ấy cũng là một thử thách khiến người đọc phải nhớ tên cũng như thời điểm sống của họ. Khi đọc sách, ức chế là cảm giác thường trực. Nhưng khi mọi thứ rõ ràng hơn, thì mình lại rất thích thú, vì kiểu như chính mình đã khám phá ra được mọi chuyện, mọi bí mật ẩn giấu. 
4
773172
2016-07-02 02:04:36
--------------------------
448119
5070
Sau khi đọc xong thì mình chỉ có thể thốt lên rằng tác giả bị Khùng!! Nhưng cái Khùng ở đây là cả 1 tài năng! Sách vô cùng khó đọc, và mình thề mình sẽ chỉ đọc lại sau vài năm nữa. 
Nhưng đây là tác phẩm rất hay, không hẳn là về cốt truyện nhưng cái hành trình mà tác giả lèo lái là vô cùng độc đáo!
Mặc dù khó khăn để thưởng thức tác phẩm này, nhưng thật lòng mình muốn được đọc nguyên bản, để thưởng thức văn phong từ chính tác giả thì còn có thể khiến mình bùng nổ cỡ nào nữa!!!!!
5
493161
2016-06-15 13:32:57
--------------------------
425244
5070
Nhiều người phàn nàn truyện khó hiểu, nhưng với tôi đây là cuốn tiểu thuyết hay nhất tôi từng đọc. Một cảm giác nôn nao nghèn nghẹn xuyên suốt quá trình đọc tác phẩm, khiến tôi cứ hết một chương lại phải nghỉ 1 ngày rồi mới đọc tiếp. Hàng trang giấy tự sự của nhân vật, thế giới nội tâm quay cuồng đảo điên, đầy những chi tiết buồn. Một khi đã dấn bước vào truyện, tôi bị cuốn đi như ở giữa dòng nước xoáy, để ở đó mà cảm nhận cái hay đến kinh ngạc và choáng váng của kết cấu dòng ý thức. Nội dung chi tiết ai cũng có thể kể ra, nhưng đây không phải cuốn truyện mà bạn đọc chỉ để biết nó tiếp diễn và kết thúc ra sao. Nếu là một người yêu các tác phẩm kinh điển, hãy thử một lần trải nghiệm văn chương của Faulkner, và bạn sẽ hiểu vì sao ông được ca ngợi đến thế.
5
764255
2016-05-04 09:30:17
--------------------------
308083
5070
   Đây có lẽ là một trong những cuốn sách buồn thảm nhất tôi từng đọc: loạn luân, tự tử, phân biệt chủng tộc. coi thường phụ nữ... - cuốn sách có tất cả. Thậm chí ngay từ đầu, một cảm giác buồn nôn như bám riết lấy từng sự kiện và cảm giác này không thể mất đi cho dù sau khi gập sách lai. Đọc cuốn sách giống như đi qua một lớp sương mù dày đặc với hai bàn tay rỗng không và trái tim sợ hãi: bạn không thể nhận biết ai là ai, đàn ông và phụ nữ, đen và trắng, bạn và thù... Nhưng dần dần lớp sương mà bắt đầu tan, theo thời gian tôi ngạc nhiên bởi những gì tôi có thể thu lượm được từ những phần có vẻ khó hiểu trong cuốn sách.
5
529844
2015-09-18 14:40:31
--------------------------
291032
5070
Tôi đã đọc rất nhiều tác phẩm cổ kim, đông tây nhưng chưa có tác phẩm nào vừa khiến tôi say mê, vừa khiến tôi nghẹt thở như "Âm thanh và cuồng nộ" của Faulkner. Nghẹt thở không phải vì tác phẩm quá gay cấn, kịch tính hay có quá nhiều chết chóc mà chính bởi kết cấu dòng ý thức, bởi cách tái hiện những hồi ức qua hình thức độc thoại nội tâm của nhiều nhân vật. Đặc biệt trong các nhân vật độc thoại có cả người đần độn (Benjy), người sắp tự tử (Quentin Compson). Những dòng hồi ức rối loạn, đứt đoạn khiến người đọc như lạc lối trong một mê cung không lối ra nhưng chính điều đó mới tạo nên sự hấp dẫn vượt thời gian của tác phẩm. Đọc Faulkner chưa bao giờ là dễ. Tuy nhiên, một khi đã nắm bắt được cách vận hành của tác phẩm tôi tin không ai dễ dàng buông sách một khi chưa đi đến trang cuối cùng.
5
114409
2015-09-05 23:20:06
--------------------------
289654
5070
"Âm thanh và cuồng nộ" quả đúng là một trong số những tác phẩm khó đọc nhất trong lịch sử. Nhưng ai đã yêu dòng văn học kinh điển thì sao có thể bỏ qua tác phẩm này. Khó đọc thật nhưng vẫn cuốn hút. William Faulkner là bậc thầy của văn học dòng ý thức vậy nên những câu văn trong tác phẩm này có nhiều trường đoạn dài, không có chấm phẩy. Khiến cho người đọc như mình phải ngâm đi ngâm lại khá nhiều lần và rất khó nắm bắt. Dù "Âm thanh và cuồng nộ" là một thách thức lớn nhưng đã yêu thích William Faulkner thì đâu nề hà gì. Cầm nó trên tay và nhâm nhi thì bạn sẽ hiểu vì sao nó lại nổi tiếng đến vậy!
4
474670
2015-09-04 18:38:10
--------------------------
186108
5070
Đúng là sách của nhà văn kinh điển có khác, khó hiểu nhưng vẫn rất hấp dẫn. Cá nhân mình thấy chương tuyệt vời nhất (và cũng khó đọc nhất) trong quyển này là chương của Quentin, từ cách tạo dựng bầu không khí cho đến những triết lí về thời gian (đã trở thành kinh điển) đều hết sức ám ảnh. Còn chương của Benjamin có cách diễn đạt độc đáo, dù lúc đầu hơi "khoai" nhưng nếu đã quen với nhịp độ thì việc nắm bắt sẽ không thành vấn đề. Sau khi qua được hai chương đầu xem như nhẹ gánh, tác giả sẽ dần dần gỡ nút những điểm khó nhằn để người đọc hiểu hơn về câu chuyện. Bên cạnh đó không thể không chú ý đến thủ pháp kể chuyện "dòng ý thức" của W. Faulkner, rất độc đáo, nếu ai không quen sẽ dễ bị "choáng" bởi có khi một chương viết liên tù tì mà chẳng ngắt câu gì, hoặc là ngắt câu đột ngột mà không có dấu câu :D
4
38828
2015-04-20 17:17:23
--------------------------
175876
5070
Văn của William Faulkner lúc nào cũng độc đáo, đến mức hơi khó hiểu và khó cảm nhận cho người lần đầu tiếp xúc. "Âm thanh và cuồng nộ" tiêu biểu cho phong cách "dòng ý thức" của tác giả, với tầng tầng lớp lớp những chương hồi, diễn biến được đặc tả thông qua những dòng độc thoại nội tâm vô cùng phức tạp. Gia đình Compson hiện lên trong truyện với đầy những bi kịch được kể lại dưới góc nhìn của chính những người trong cuộc: những đứa con và người vú nuôi da đen. Từng lời kể chậm rãi, xoáy sâu vào tâm can và trái tim con người, những kẻ đau đớn và vật vã trước bàn tay số phận, và cả phút giây điên loạn trước sự biến động của cuộc sống.
Cuốn sách tăm tối và đầy rẫy bi kịch được kể bằng giọng văn sắc lạnh, từng con chữ chạm đến đáy sâu cảm xúc, nơi có những nỗi sợ mà con người thường né tránh.
5
109067
2015-03-31 10:33:52
--------------------------
165842
5070
Tôi là một con mọt thực thụ của tủ sách văn học kinh điển. Và tất nhiên, tôi sẽ chẳng thể bỏ qua cuốn "Âm thanh và cuồng nộ".
Có thể nói thế giới cuộc sống và con người trong tác phẩm là một thế giới của sự hỗn độn, gào thét, thảm trạng cuộc sống xã hội, hỗn loạn với những mảnh chắp nối giữa hiện tại, quá khứ, tương lai, những mập mờ được tạo dựng nhằm gây sự tò mò, chú ý của người đọc.
Trong tác phẩm: có tình yêu, có sự cô đơn, sự tha hóa, tham lam ích kỉ và tồn sinh...
 Với thủ pháp thời gian huyền thoại, tác phẩm là sự kết hợp độc đáo của những khung bậc thời gian, địa điểm. Một tác phẩm đặc biệt, độc đáo đến kỳ lạ !
4
540359
2015-03-11 11:45:51
--------------------------
