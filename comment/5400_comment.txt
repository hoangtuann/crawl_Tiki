468061
5400
Một cuốn sách thú vị và bổ ích cho mình. Mình thích đọc sách ở nhiều thể loại khác nhau, và có thói quen thấy quyển nào hay là muốn đọc nhưng tốc độ đọc sách của mình không tốt lắm, nên thấy có quyển dạy đọc nhanh này là mua liền, mà còn của Buzan nữa chứ. Sau khi đọc được hơn một nửa cuốn thì mình cũng nắm bắt được một số quy tắc cho việc đọc nhanh và cũng áp dụng liền, thấy hiệu quả lắm. Nhưng nói thật là phải cố gắng và kiên trì mới có thể thay thế cách đọc thông thường qua cách đọc nhanh này vì một số phương pháp hiệu quả thật nhưng khác hẳn với những gì mình vẫn đang làm, nên hơi khó để thay đổi, nhưng thôi cố gắng sự nghiệp đọc nhanh.
4
915258
2016-07-04 17:44:43
--------------------------
448117
5400
Mình biết tác giả từ " Bản Đồ Tư Duy" một công cụ trí não siêu nổi tiếng.
Sách hay, tốc độ đọc của mình cũng tăng lên chút chút, mà các phương pháp thiệt lòng mình thấy khá siêu phàm, nhưng chắc bạn cũng sẽ tìm được 1 cách thôi, đừng lo. quan trọng lá có muốn luyện tập không.
Mình chọn luyện tập trên các sách dễ đọc, hoặc các sách có ý muốn đọc lại. Như vậy vừa tiết kiệm thời gian vừa luyện được kĩ năng và không lo uổng cuốn sách nếu đọc ko hiểu
4
493161
2016-06-15 13:29:12
--------------------------
379689
5400
Quyển sách này nó giúp cho tôi có tốc độ cải thiện hơn rõ rệt, tôi rất thích đọc sách nhưng vì tốc độ đọc chậm nên một quyển sách 300 trang tôi phải mất vài ngày mới đọc xong, rất mất thời gian. Sau khi luyện tập với như những gì tony dạy tôi đã tự tin khi mình có thể đọc cuốn túp lều bác Tôm chỉ trong 1h. Tôi rất cảm ơn tony. Duy chỉ có một điều là người đọc phải tự tìm bài tập về đọc nhanh chứ nếu chỉ mong bài tập trong cuốn sách này thì ít lắm
5
789999
2016-02-12 10:14:50
--------------------------
347996
5400
Đầu tiên tôi nghĩ mình là người có tốc độ đọc khá nhanh (khoảng 322 từ mỗi phút). Tôi không nghĩ là có thể cải thiện tốc độ đọc "tức thì" cho đến khi đọc quyển này của Tony Buzan.

Bài tự kiểm tra số 1: tốc độ đọc 322 từ mỗi phút, mức độ hiểu là 46%
Bài tự kiểm tra số 2: tốc độ đọc 413 từ mỗi phút, mức độ hiểu là 50%
Bài tự kiểm tra số 3: tốc độ đọc 523 từ mỗi phút, mức độ hiểu là 60%

Thật không thể tin được, những bài tập luyện não bao giờ cũng khó khăn lúc đầu. Nhưng chỉ cần một chút cố gắng, mọi chuyện sẽ trở nên dễ dàng :)
5
1019157
2015-12-05 12:59:07
--------------------------
342229
5400
Mình đọc quyển này cách đây 2 năm hồi đó sinh viên không có tiền nên phải photo lại của người quen đọc.
Tốc độ đọc của mình thuộc dạng nhanh rồi tại trước đó cũng được chị tặng một quyển về kĩ năng đọc. 
Nhờ quyển này mình cải thiện được tốc độ đọc và ghi nhớ được lượng kiến thức nhiều hơn. Tiết kiệm được thời gian và nâng cao hiệu quả trong học tập cũng như công việc. 
Tony buzan là tác giả quá nổi tiếng trong các lĩnh vực ghi nhớ bằng sơ đồ tư duy rồi. Thật sự cảm ơn các dịch giả đã giúp mình tiếp cận quyển sách dễ dàng hơn. 
4
679396
2015-11-23 23:35:03
--------------------------
331198
5400
Lượng thông tin và kiến thức ngày nay tuy rất nhiều nhưng đọc nhanh mà không nắm được gì sau khi đọc, đọc hàng trăm quyển sách mà qua một tháng rồi thì trong đầu rỗng tênh, chẳng áp dụng được gì thì thật là uổng phí thời gian, tiền mua sách và công sức mà bạn đã bỏ ra.
Quyển sách này sẽ đánh giá 2 tiêu chí khi đọc sách: 1. Thời gian đọc của bạn; 2. Tỷ lệ % mà bạn nắm bắt được nội dung như thế nào sau khi đọc nhanh. Rèn luyện theo hai tiêu chí trên cùng với những phương pháp mà sách đưa ra hy vọng các bạn vừa có thể cải thiện được tốc độ đọc của mình, vừa có thể nắm bắt được nội dung mà quyển sách truyền đạt.
Về ý kiến cá nhân, mấy quyển sách của NXB Nhân Trí Việt được định giá quá cao so với những NXB khác, và đây là quyển đầu tiên cũng như quyển cuốn cùng mà mình mua của NXB này.
2
333477
2015-11-04 08:46:49
--------------------------
303470
5400
Đọc nhanh là 1 kĩ năng rất cần thiết cho công việc cũng như học tập, nghiên cứu. Cuốn sách này với các bài tập bổ ích và theo các độ khó tăng dẫn sẽ khiến cho bạn không nhàm chán và năng cao đáng kể khả năng đọc nhanh, tư duy nhanh nhạy. Nếu bạn có thể kiên trì thực hành theo các bài tập trong sách thì chỉ 1 thời gian là bạn sẽ nắm bắt được các kĩ năng này. Các cuốn sách của Tony Buzan luôn nằm trong top Best seller, và tất nhiên là giá cũng không hề rẻ chút nào!
4
724204
2015-09-15 21:09:05
--------------------------
295466
5400
Sách do Tony Buzan viết chưa bao giờ làm mình thất vọng. Bìa sách được trang trí khá bắt mắt, nội dung thì khỏi phải chê. Có thể xem cuốn sách như là một bí kiếp hữu ích dành cho mọi người. Tất cả những ai, dù già hay trẻ đều cần phải đọc nhanh để nắm được những thông tin mình quan tâm. Trong khi lượng thông tin trong cuộc sống ngày một gia tăng thì tốc độ đọc của mỗi người cũng phải tăng theo cho phù hợp để không bỏ sót những điều quan trọng. Nếu siêng năng và kiên trì rèn luyện thì bạn hoàn toàn có thể làm chủ khả năng đọc nhanh. Tóm lại, đây là một cuốn sách rất bổ ích.
5
456957
2015-09-10 11:47:21
--------------------------
290992
5400
Quyền sách này 3 sao không phải vì nó không hay mà chỉ vì nó quá cao siêu để tôi có thể áp dụng và thực tiễn hết.Có lẽ được nếu như tôi chịu dành thời gian ra đọc "ngấu nghiến " nó nhưng tôi lại không thích như vậy lắm! Tóm lại đây sẽ là quyển sách hữu dụng đối với những người " kiên gan bền chí" .Tôi cũng sẽ cố gắng áp dụng những kĩ năng trong sách 1 cách tối ưu vì nếu thực sực được như vậy thì tôi sẽ tiết kiệm được rất nhiều thời gian khi đọc .
3
22795
2015-09-05 22:41:39
--------------------------
255769
5400
Sau khi luyện tập mình đã có rất nhiều tiến bộ! Thật sự vô cùng biết ơn tác giả! Lúc đầu khi luyện các kiểu chuyển động chữ S, thòng lọng ... thật sự quá khó nhưng kiên trì một thời gian mình đã thực hiện được. Lúc trước vẫn tin phải đọc kỹ đọc chậm mới hiểu giờ thì mình hoàn toàn tin khác rồi, khi đọc nhanh bạn sẽ cảm thấy vô cùng hứng thú. Quan trọng vẫn là niềm tin, chỉ cần tin tưởng có thể tăng tốc độ đọc lên và cố gắng thì nhất định sẽ làm được. 
5
596865
2015-08-06 12:56:44
--------------------------
251518
5400
Quả thật với lượng thông tin phải xử lý và nghiên cứu trong công việc ngày càng lớn thì một phương pháp thâu tóm thông tin nhanh và chính xác là vô cùng cần thiết. Cuốn sách hướng dẫn người đọc cải thiện tốc độ đọc cùng với hiệu quả tiếp thu của mình trong hầu như tất cả các loại tài liệu, kể cả thơ. Đây là một cuốn sách dạng hướng dẫn thực hành kĩ năng, do đó cần phải kiên nhẫn tập luyện theo hướng dẫn để đạt được kết quả như mong đợi. Nếu như bạn chưa từng nghĩ tới việc cải thiện tốc độ đọc của mình thì bạn lại càng phải đọc cuốn sách này.
4
59771
2015-08-02 21:55:28
--------------------------
247553
5400
Về mặt nội dung của sách tôi rất hài lòng từ cách tác giả sắp xếp các bài tập cho tới việc tác giả thiết kế một hệ thống đi từ cơ bản tới nâng cao giúp cho người học có thể đạt được những tiến bộ nhanh chóng. 
Điều tối quan trọng là bạn cần một chút kiên trì thực hành các bài tập trong sách hướng dẫn. Những chương đầu bạn có thể thấy hơi ngợp và muốn bỏ cuộc nhưng đó chính là những gì khó nhất bạn phải trải qua. Luôn giữ quyết tâm, vì càng về sau tất cả đều dễ dàng hơn.

4
627565
2015-07-30 10:52:45
--------------------------
232611
5400
Mình rất thích sách của Tony Buzan, các quyển sách của ông đều rất hay và bổ ích. Quyển Đọc nhanh này cũng vậy, có cả lí thuyết và bài tập thực hành, sau mỗi bài tập, tốc độ đọc được cải thiện đáng kể. Có rất nhiều cách đọc nhanh khác nhau, nhưng để đạt đến trình độ cao thì cần nhiều thời gian luyện tập chứ không phải chỉ cần đọc xong quyển sách này thì tốc độ đọc của bạn tự nhiên tăng ngay lập tức được đâu. Tóm lại đây là một quyển sách đáng giá.
5
192644
2015-07-18 21:53:26
--------------------------
211657
5400
Mình mới có sở thích đọc sách trong thời gian gần đây thôi và mình cũng rất quan tâm đến cách đọc làm sau nhanh và tớm được nhiều kiến thức nhất .
Tình cờ mình tìm thấy quyển sách này của Tony Buzan và khá hứng thú vì mình đã đọc khá nhiều sách của ông .
Xét về kiến thức thì sách có những phương pháp cụ thể và có bài tập sau mỗi bài học để bạn áp dụng ngay . Mình nghĩ mọi người nên kiên trì và thực hành thường xuyên hơn để có tốc độ đọc nhanh nhất .
Mình chỉ hơi bận tâm về giá tiền khá cao của quyển sach thôi .
Một quyển sách hữu ích . Xin cảm ơn
3
554150
2015-06-20 21:48:31
--------------------------
205916
5400
Xã hội hiện đại, nhiều kiến thức quan trọng mỗi ngày đều tuôn ra ào ạt, những quyển sách mới xuất bản liên tục, những tờ báo với những thông tin nóng hỏi và nhiều những thứ khác nữa nếu ta không cập nhật kịp sẽ bị đi lùi so với thời đại, nghèo nàn về kiến thức vì vậy muốn bắt kịp thông tin chúng ta cần phải có kĩ năng đọc thật tốt. Với một số người việc đọc nhanh, đọc lướt rất dễ dàng những với một số người khác, điển hình là mình, việc đọc nhanh trở nên rất khó khăn. Cuốn sách này đã cung cấp một lượng lớn những kỹ năng dạy đọc nhanh, từ cơ bản đến nâng cao để giúp cải thiện khả năng đọc và mình thấy rằng nếu muốn thực hiện được hết những kĩ năng này thì chúng ta đừng nên hấp tấp và phải chịu khó luyện tập mỗi ngày từng chút một thì hiệu quả sẽ được cải thiện từ từ, không thể luyện tập những kĩ năng này trong thời gian ngắn mà đọc nhanh được. Quyển sách được trình bày khá rõ ràng và có ví dụ, mục tiêu cụ thể để giúp chúng ta cải thiện khả năng đọc của mình.
4
274995
2015-06-08 08:45:12
--------------------------
201273
5400
Nhận xét:
-Nội dung quyển sách không sâu vào trọng tâm của vấn đề đọc nhanh mà chỉ nói chung chung vấn đề.
-Phần quan trọng nhất của quyển sách là cách đọc ngoại vi nhưng mỗi cách chỉ nói sơ lược vài dòng về lợi ích và định nghĩa.
-Đa số là phần trích dẫn thông tin gì đó để đọc và sau đó luyện tập. Nhưng phần quan trọng mà không nêu lý thuyết thì làm sao thực hành được.
-So với số trang sách của quyển sách thì số trang giúp hình thành kĩ năng chiếm tỉ lệ % rất thấp.
-Với một quyển sách như vậy mà giá tiền còn rất cao.
-Tôi khuyên mọi người mua sách nên có mục đích rõ ràng (đọc để hiểu biết hơn và hoàn thiện bản thân hơn) chứ không nên mua sách theo phong trào.
2
440921
2015-05-26 23:43:48
--------------------------
188617
5400
Thứ nhất, về thời gian - Người trung bình dành 2 giờ một ngày đọc những vấn đề liên quan đến công việc. Nếu bạn chỉ đơn giản là có thể tăng gấp đôi tốc độ đọc của bạn, bạn có thể tiết kiệm 260 - 360 giờ trong một năm (tương đương 40 ngày làm việc)

Thứ hai, về tiền bạc - Nếu tổng chi phí của bạn cho công ty của bạn (tiền lương, thuê văn phòng, thiết bị văn phòng, quyền lợi và lợi ích khác) là $100K một năm, và bạn làm việc 40 giờ một tuần, thì chi phí cho công ty của bạn là $48 một giờ. Nếu bạn tiết kiệm 360 giờ một năm trong thời gian đọc, điều này giúp tiết kiệm cho công ty của bạn là $ 17.280 một năm - và điều này là tính trên chỉ số nếu bạn chỉ tăng gấp đôi tốc độ đọc của bạn. Nếu bạn tăng tốc độ đọc của bạn lên đến 3, 4, 5 hoặc thậm chí 6 đến 10 lần, thì các khoản tiết kiệm thời gian và tiền bạc trở nên rất lớn.
5
553306
2015-04-25 18:48:26
--------------------------
155964
5400
Hiện nay con người ai cũng phải đối mặt với khối lượng thông tin đồ sộ đến từ sách báo và internet. Trong núi thông tin khổng lồ đó, ai cũng có thể bị khủng hoảng thông tin, bị nó nuốt chửng nếu chỉ trang bị cho bản thân khả năng đọc chậm rãi được dạy từ mẫu giáo và tiểu học. May mắn thay, mọi người có thể tự tin đối mặt với nó nếu được trang bị những kỹ năng đọc nhanh có trong quyển sách này. Quyển sách cho ta biết lịch sử hình thành của các phương pháp đọc nhanh, các nhà vô địch về đọc nhanh trên thế giới cũng như các phương pháp tăng tốc độ đọc đối với sách báo và cả internet. Tôi nhận thấy cuốn sách rất hữu ích trong thời đại bùng nổ thông tin ngày nay.
5
387632
2015-02-02 18:33:40
--------------------------
