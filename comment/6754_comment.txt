465154
6754
Văn của Văn Thành Lê luôn gây ấn tượng với tôi ngay từ khi biết anh, từ truyện ngắn đến truyện dài. Ngòi bút không quá sắc sảo nhưng hóm hỉnh và vui tươi lạ. "Không biết đâu mà lần" nói về vấn đề giáo dục - một chủ đề nghe có vẻ khô khan nhưng thật sự qua con mắt của tác giả nó nhẹ nhàng và nghịch ngợm rất lạ mà người đọc vẫn nắm bắt được những vấn đề của thời cuộc trong ngành. Nhìn chung đây là 1 quyển sách  khá, đọc giải trí nhưng vẫn có điều để suy ngẫm. Tuy nhiên cái điệp khúc "Không biết đâu mà lần" của tác giả cứ lặp đi lặp lại ở cuối mỗi chương nhiều khi cứ thấy gượng ép và lắm lúc không có duyên.
4
46905
2016-06-30 21:25:31
--------------------------
408329
6754
Tác giả cuốn sách này là một thầy giáo và nội dung của tác phẩm cũng xoay quanh chuyện trường, chuyện dạy học và những mối quan hệ liên quan đến. Có những chuyện dường như ta đã biết hoặc từng nghe thấy. Qua tác phẩm này, tác giả đã phản ánh chúng ở một góc độ trào phúng, vui nhộn hơn nhưng không kém phần trăn trở, nặng lòng. Cách viết "tưng tửng" của tác giả có vẻ không kiêng dè bất cứ ai, bất cứ chuyện gì, từ chuyện thầy hiệu trưởng, đến chuyện "đổi chác" để được thuận đường sự nghiệp. Thật tiếc nếu như những thầy giáo, cô giáo như nhân vật chính trong tác phẩm, có tâm với nghề, không trụ lại được với công việc đào tạo thế hệ mai sau như thế. 
4
113977
2016-03-31 15:29:42
--------------------------
341350
6754
Tác giả có cách viết hơi lập dị, hài hước đúng như tựa sách "không biết đâu mà lần" nhưng mang tính "hiện thực phê phán" khá cao. Đã đọc thử trên Tiki nên quyết định mua quyển này, hơi thất vọng về hình thức một tí, sách khá mỏng và nhỏ với giá 48.000 thì hơi mắc. Lúc đầu mới đọc thấy cũng bình thường, không lôi cuốn mấy. Tuy nhiên đọc hết và suy ngẫm thì thấy nội dung cũng khá hay.
Đọc qua về giới thiệu tác giả thì biết là tác giả khá trẻ, cách viết vừa theo ngôn ngữ của tầng lớp trẻ nhưng không kém phần sâu sắc. 
3
895614
2015-11-21 21:54:33
--------------------------
175095
6754
Sau khi đọc cuốn Châu Luch thứ 7 của anh Văn Thành Lê, mình quyết đinh lên tiki đặt cuốn này. Dù cách viết có hơi khác cách anh viết truyện ngắn, nhưng truyện của anh Lê vẫn có sức hút lạ kì. Lời văn đơn giản, pha chút hài hước. Đặc biệt nhan đề được nhấn đi nhân lại ở mỗi chương như nhấn mạnh với bạn đọc "chả biết đâu mà lầ" về những thứ đã diễn ra, sẽ diễn ra.
Đề tài không thực sự mới, nhưng tác giả đã không làm nó thêm cũ kĩ, mà làm nó có phần mới mẻ hơn.
Sách về giá cả thì khá mềm + chất giấy khá ok!
5
190083
2015-03-29 13:25:06
--------------------------
168340
6754
Câu chuyện được kể bằng một giọng văn rất trẻ, rất hài, đọc vào có hơi vô duyên nhưng lại thấy cuốn hút với ngôn ngữ có vần điệu, cùng với cách chơi chữ, dựng tình huống làm cho người đọc phải cười ra nước mắt, nhưng đó là một sự thật khá đau đối với một phần thế hệ trẻ. Và lối thoát ở đâu vẫn là một dấu hỏi, với nhân vật Anh – một người thầy trong truyện thì có lẽ hướng ngoại cũng là một giải pháp.
Bìa sách đẹp, giấy xốp vàng, chữ in rõ nhưng font nhỏ, không có lỗi đánh máy, có bookmark.
4
289979
2015-03-16 14:38:36
--------------------------
