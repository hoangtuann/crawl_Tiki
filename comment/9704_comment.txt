197807
9704
Những câu chuyện phát triển chỉ số tình cảm EQ” là bộ sách đầu tiên ngoài các truyện cổ tích mà mình cùng đọc, học và chơi cùng con gái. Những câu chuyện đơn giản, dễ hiểu, cùng với hình ảnh đầy màu sắc đã lôi cuốn con gái cùng tham gia một cách hào hứng. Cuối mỗi truyện đều có phần đặt câu hỏi để giúp bé hệ thống hóa câu truyện, giúp bé tập suy nghĩ và trả lời các câu hỏi liên quan. Ngoài các câu hỏi trong sách, bạn cũng có thể đặt các câu hỏi khác để giúp con hiểu rõ hơn. Những câu chuyện đã giúp ích rất nhiều cho kỹ năng sống của bé, ví dụ như biết xếp hàng, không nói lớn chỗ đông người.
5
183312
2015-05-18 15:29:33
--------------------------
