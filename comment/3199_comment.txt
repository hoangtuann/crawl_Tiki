411384
3199
Điều đầu tiên ấn tượng với quyển sách này là nó rất nhỏ gọn như quyển tập 100 trang dành cho học sinh vậy. Có thể dễ dàng mang theo làm bài tập lúc rảnh. Cuốn sách bao quát hết mọi tình huống xảy ra trong khách sạn. Bạn nào dự định hoặc đang làm Front Office, House Keeping có thể mua để tham khảo. Ngoài ra sách còn có những phần Did you know hoặc Language Tip rất bổ ích. Cung cấp những kiến thức chuyên ngành để chúng ta thêm tự tin, tránh những sai sót có thể xảy ra khi phục vụ khách hàng.
5
509425
2016-04-05 22:34:57
--------------------------
392095
3199
Mình là sinh viên ngành Hospitality, hiện tại mình cũng đã đi làm và đặc biệt là muốn trao dồi thêm kỹ năng tiếng anh trong lĩnh vực này, Thật ra thì mình cũng đã mua vài cuốn tương tự nhưng mình vẫn chưa hài lòng 100%.

Nhưng khi tìm kiếm thì thấy bộ sách của Collins là mình mua liền vì mình cũng đã mua trước đó vài cuốn với nội dung Ielts.

Nội dung trong sách khá đầy đủ đặc biệt là từng tình huống mà mình phải giải quyết cho khách hàng, có những đoạn hội thoại cho từng tình huống và cách dùng từ chính xác

Bìa sách đẹp, chất liệu bên trong tốt ( giấy in màu ) cảm thấy thú vị khi học.
chỉ có một điều là từ vựng được thống kê cuối bài thì không có nhưng cũng không sao, chúng ta có thể note lại

Gía cả phù hợp với sản phẩm
4
58391
2016-03-06 10:37:57
--------------------------
366654
3199
Điểm cộng: Tôi đã tìm sách nước ngoài về chuyên ngành Nhà hàng - Khách sạn trên nhiều cấc trang web bán sách online khác như Fahasa, Vinabook nhưng hoặc là không có hoặc là đọc không hay. Khi tìm được quyển này tôi rất ưng ý. Bỏ qua việc PR nhiều thì nội dung mạch lạ, dễ hiểu và giúp tôi rất nhiều không những trong việc học tiếng anh mà còn trong việc học về chuyên ngành của mình. Vẫn chưa đưa hết các phần vào thực tế mà chỉ áp dụng từng phần thấy phù hợp! Đọc qua sách và Áp dụng tốt sách trong tuỳ từng thời điểm sẽ giúp bạn đạt được két quả như mong muốn!
Điểm trừ: Ngoài ra mình có sử dụng bọc sách của Tiki cho sách này: sách có khổ trung bình (15x21cm) và bìa hơi cứng, Tiki bọc như sách khổ to hơn khác, phần yếm cũng ngắn đi, vì vậy mà cứ mở sách ra đọc là phải giữ sách lẫn phần bọc để bìa sách không bị tuột ra khỏi bọc của Tiki
4
380150
2016-01-10 12:17:28
--------------------------
