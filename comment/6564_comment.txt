198774
6564
'Thủy Hử' là một trong tứ đại danh tác của Trung Quốc cùng với  'Tam Quốc Diễn Nghĩa' của La Quán Trung, 'Tây Du Ký 'của Ngô Thừa Ân và 'Hồng Lâu Mộng' của Tào Tuyết Cần. Cốt truyện chính là sự hình thành và phát triển của một nhóm cướp trên núi Lương Sơn thường gọi là 108 anh hùng Lương Sơn Bạc thường cướp của người giàu chia cho người nghèo nên được nhân dân gọi là những hảo hán. sau lớn mạnh lên, họ mở doanh trại chống lại triều đình thối nát và lập được nhiều chiến công lừng lẫy. Với tài năng tạo hình của tác giả, các anh hùng Lương Sơn hiện lên rất sinh động từ cử chỉ đến lời nói, giọng văn mộc mạc, mạnh mẽ. Kết truyện thật buồn vì các anh hùng người thì chết người thì phiêu bạt do Tống Giang-thủ lĩnh Lương Sơn quy phục triều đình, qua đó độc giả cũng cần rút ra bài học cho mình chớ nên dại dột tin tưởng vào cái ác.
5
351857
2015-05-20 19:48:16
--------------------------
