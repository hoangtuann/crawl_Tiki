377794
10653
Chàng trai kỳ lạ , làm việc trượng nghĩa , không thích cái xấu ức hiếp thấy là ra tay giúp đỡ nên được ca ngợi về hành động nhanh trí và gan dạ như người có sức khỏe lớn , tập này nói khá vui về việc anh chàng thủy hử đại náo trong rừng dã trư khiến mọi người hơi khiếp đảm , sợ hãi bởi người có phen khí chất ngời ngời làm những việc khác thường , nể phục anh hơn mỗi khi anh cứu nguy cho người dân lành , là tập bắt đầu chuỗi ngày ghi tên lên danh sách .
4
402468
2016-02-03 13:01:49
--------------------------
264111
10653
Bộ truyện này dù giấy khá lóa mắt, chỉ có một màu xanh vô tận nhưng mình phải ca ngợi họa sĩ và tác giả đã làm nên bộ truyện này . Không chỉ nội dung hấp dẫn mà nét vẽ cũng vô cùng cuốn hút . 
Tác giả của nét vẽ này đã tạo nên những nhân vật rất có hồn , hơn nữa những chàng trai trẻ nhìn cũng rất khôi ngô tuấn tú . Mình là nữ nên lại càng thích hơn . Đọc truyện tranh hay như vậy , mình đang cân nhắc xem có nên mua tiểu thuyết để đọc cho rõ nội tâm nhân vật hay không . Có lẽ đọc tới tập mười mình  sẽ ra quyết định . 
Mình có lời khen cho họa sĩ đã vẽ nên bộ truyện này , hi vọng những bộ truyện nổi tiếng khác cũng được họa sĩ này vẽ minh họa cho . 
4
335856
2015-08-13 00:10:44
--------------------------
169311
10653
Những tập truyện dài Thủy hử thường không phù hợp với trẻ em cho lắm vì quá dài, nhiều chữ, với tốc độ đọc chậm thì bọn trẻ sẽ rất nhanh chán, do vậy truyện tranh là một sự lựa chọn tuyệt vời, vừa khơi gợi niềm yêu thích đọc sách, vừa giúp trẻ dễ hiểu được nội dung của một tác phẩm kinh điển như Thủy Hử. Phần truyện này nói về Lỗ Trí Thâm, một người anh hùng nóng tinh nhưng đầy hiệp nghĩa. Lỗ Trí Thâm có nhiều điểm giống với Lí Quì, tuy nhiên mình thích Lỗ Trí Thâm hơn nhiều, Lỗ là một người hướng thiện và luôn cân nhắc trước một hành động của mình, không như Lí Quì chỉ giỏi chém giết, sai gì nghe nấy, hiếm khi dùng trí óc.
4
472173
2015-03-17 22:59:49
--------------------------
134875
10653
Thủy Hử là một tựa truyện rất nổi tiếng rồi, về phần nội dung thì khỏi bàn. Khi chuyển thể sang truyện tranh thì truyện cũng truyền tải khá tốt cảm xúc nhân vật và diễn tả sâu sắc nội dung cốt truyện qua nét vẽ.

Ưu điểm:
- Nét vẽ, giấy in mực xanh rất đẹp, lạ. (mình rất ưng khoản này :D)
- Chất lượng giấy cầm thích tay.
- Bìa đẹp, mới.
- Giảm giá nhiều. (nên mình mua thử 1 cuốn, sau thì thích nên tìm mua đủ luôn)

Nhược điểm:
- Bạn nào đã quen đọc manga rồi thì chắc mới đầu có lẽ đọc hơi khó hiểu vì kiểu sắp xếp khung tranh hơi rối và nét vẽ thiên về tả thực nên nhiều cảnh nhìn hơi sợ ^^
- Khổ truyện hơi to, cầm không thuận tay lắm.

Kết là nếu thích truyện tranh hay Thủy Hử thì bạn đều nên mua thử 1 cuốn ^^
5
450022
2014-11-11 20:09:51
--------------------------
