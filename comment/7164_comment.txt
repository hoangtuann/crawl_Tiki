348783
7164
Albert Einstein là nhà khoa học mà tôi ngưỡng mộ nhất. Điều đặc biệt nhất của ông chính là người có chỉ số IQ cao nhất trên thế giới. 
Mình thích tất cả những phát minh của Einstein. Mình cũng luôn tìm những cuốn sách viết về cuộc đời và sự nghiệp của ông. 
Những nội dung trong cuốn sách này thì mình đã biết từ trước, nhưng mình vẫn mua vì nó có phần mình thích. Chính là phụ đề song ngữ Anh -  Việt. Mình vốn rất thích đọc sách bằng tiếng Anh. Hi vọng là các bạn cũng sẽ yêu thích cuốn sách giống mình. 
5
754539
2015-12-06 19:46:51
--------------------------
276954
7164
Cuốn này mình đặc biệt ấn tượng với tạo hình của nhà bác học Albert Einstein ngoài bìa, trông có vẻ hài hước và quen thuộc như những hình ảnh thường thấy về nhà bác học này. Tiếc là tranh vẽ bên trong lại khác, tạo hình không đẹp và không đậm nét tính cách bằng. Cũng như các tập khác, cuốn này nêu đủ các thông tin cơ bản về Albert Einstein, nếu bạn nào hứng thú có thể tìm hiểu thêm, lượng thông tin ở đây tuy không chi tiết cụ thể nhưng cũng đủ để cung cấp cho bạn những gì cơ bản nhất.
4
82774
2015-08-24 15:35:48
--------------------------
