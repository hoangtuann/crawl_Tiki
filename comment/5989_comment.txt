242024
5989
Tôi phải công nhận đây là cuốn sách vô cùng quý giá, ngoài việc chất liệu giấy in khá tốt thì nội dung càng khiến tôi yêu thích hơn, tôi đã đọc nhiều sách về lịch sử thời Trần, nhưng đây là cuốn sách đầy đủ và có chiều sâu nhất. Qua cuốn sách này tôi cũng hiểu rõ hơn tại sao nhà Trần lại phát triển lớn mạnh như thế, theo như tôi biết qua sách thì tư tưởng yêu nước là tư tưởng chủ đạo, tư tưởng  “phụ tử chi binh”, “dĩ đoản chế trường”,... của Trần Hưng Đạo là nền tảng để xây dựng đội quân hùng mạnh chống quân ngoại xâm, bên cạnh đó còn rất nhiều tư tưởng khác ảnh hưởng đến sự hưng thịnh của một đất nước.
5
539768
2015-07-25 23:59:31
--------------------------
