532169
5115
Đây là một cuốn sách hay của Haruki Murakami. Hajime trong tác phẩm là một chàng trai luôn cảm thấy sự thiếu hoàn thiện ở chính bản thân anh và trong quá trình trưởng thành anh ta luôn tìm kiếm sự bù lấp, mặc cho chuyến hành trình ấy, anh nhận ra rằng " Một con người có thể làm tổn thương một người khác, duy nhất bởi vì anh ta tồn tại và là chính anh ta". Điều ấy khiến anh ta chật vật không kém nhưng là chuyện không thể nào tránh khỏi. Đôi khi trong cuộc sống, chúng ta củng gặp phải những hoàn cảnh tương tự nhưng vẫn phải tiếp tục tìm kiếm chính mình. 
5
36093
2017-02-27 00:48:30
--------------------------
531269
5115
Văn phong rất tuyệt vời. Lối viết của ông cực kì tinh tế và hấp dẫn. Mình rất thích điểm này. Tuy nhiên ý nghĩa của sách mình k thể nào rút ra được. Mình chỉ cảm thấy đây giống như cuốn sách viết về tâm sinh lí của con trai từ lúc chưa trưởng thành đến lúc trưởng thành. Chắc do mình còn nhỏ chưa có nhiều trải nghiệm. Đặc biệt có nhiều cảnh về tình dục phải gọi là quá trần trụi và k cần thiết. Vì vậy mình nghĩ mình nên cho 4 sao thôi.
4
1722981
2017-02-24 20:32:50
--------------------------
525737
5115
Mình rất ấn tượng với cuốn sách này. Các tình tiết đan xen nhau khó đoán tạo hứng thú, bất ngờ cho người đọc. Cốt truyện cũng có gì đấy rất đặc biệt mà mình chưa từng đọc qua.Ngoài ra cũng có nét giống với ngôn tình nhưng mới lạ, độc đáo hơn. Tên sách cũng rất hay nữa 
5
1484394
2017-02-15 19:16:04
--------------------------
494454
5115
Haruki Murakami viết quyển sách này làm cho ai thích văn phong trước đây của ông sẽ khá ngạc nhiên. Bởi lẽ nó quá đơn giản và nhẹ nhàng so với những câu truyện ông rừng viết trước đây! Nhưng nó không phải đơn giản để hiểu mà phải thật lắng đọng để xảm nhận được nó. Nó làm tôi đau lòng nhưng lại làm tôi thấy thú vị. Văn phong của ông lúc nào cũng thế! Vẫn lôi kéo tôi đến từng trang sách cho dù câu truyện của ông có đau thương hay nhẹ nhàng hay đôi khi khó hỉu đến thế nào chăng nữa!
5
777339
2016-12-06 22:37:16
--------------------------
492579
5115
Một trong những cuốn mình yêu thích nhất của murakami. Vẫn là Ám ảnh về sự cô đơn, thiếu hụt trong tâm hồn nhưng nhẹ nhàng hơn, sâu lắng và đặc biệt là  thực tế hơn những cuốn khác. Thỉnh thoảng mình vẫn tìmnat king cole để nghe cho tâm hồn thoải mái. Pretend youre happy when youre blue. 
5
81385
2016-11-25 11:39:55
--------------------------
457445
5115
Phía Nam biên giới, phía Tây mặt trời , hẳn những ai đã từng trẻ thì cũng có ít nhất một điều day dứt và tiếc nuối. Đơn giản nhưng không hề đơn thuần, Haruki luôn có năng lực đặc biệt lôi kéo độc giả vào vòng xoáy nội tâm nhân vật của mình thật sự xuất sắc, trong bất cứ tình huống truyện, hoàn cảnh nào .Tuy diễn biến cuộc đời của Hajime về sau khá lạ, nhưng biết đâu đấy, cuộc đời luôn có thể biến chuyển theo những hướng mà ta không bao giờ ngờ tới,những chuỗi chi tiết nhỏ nhặt nhưng đầy ẩn ý đã cùng nhau kết hợp thành một cuốn tiểu thuyết có giá trị. 
5
1452634
2016-06-24 11:33:29
--------------------------
450679
5115
- Về nội dung: Đây là một trong những cuốn tiểu thuyết thuộc vào loại "ít đồ sộ nhất" tương quan với các tác phẩm các của Murakami Haruki. Cuốn tiểu thuyết khá dễ đọc, dù nội dung mà nó phản ánh và tầm triết lý mà nó thể hiện khá nhiều. Đọc tác phẩm rồi, cái còn đọng lại trong tôi là câu chuyện cảm động về cuộc đời Hajime và những ý nghĩa về sự trải nghiệm, dấn thân, quy tắc xã hội, những quy luật cuộc sống, ... Tất cả thật sâu sắc mà trữ tình.
- Về hình thức: Sách có bìa đẹp, ấn tượng; giấy chất lượng; chữ rõ, đẹp.
4
60352
2016-06-19 03:03:06
--------------------------
441406
5115
Cách kể truyện của Murakami không cách điệu, không thái quá, ban đầu chưa quen với văn phong, tôi đánh giá cuốn Rừng Na Uy là khá khó hiểu. Với những cuốn sách như vậy, tôi thường bỏ dở giữa chừng. Nhưng đối với Rừng Na Uy, tôi đã đọc đến con chữ cuối cùng dù không thấm thía nhiều lắm. Và đối với Phía Nam Biến Giới, Phía Tây Mặt trời cũng vậy. 
Văn phòng của Murakami rất giản dị, dễ đọc nhưng không dễ thấm, Mặc dù vậy, tôi vẫn bị PNBG, PTMT cuốn hút ngay từ dòng mở đầu. Như được viết ở bìa sách, đây là "Cuốn tiểu thuyết sâu sắc cảm động của ông". Phân đa cuốn sách chính là đời thực của Murakami nên cuốn sách rất chân thật, đọc mà không cảm thấy gượng ép. Không có cao trào, mạch kể đều đều, tuy nhiên tôi không tài nào đặt cuốn cuốn sách xuống khi đã cầm lên và mở ra đọc. Đó là bở nghệ thuật kể truyện tài tình của ông. Những câu đối thoại của các nhân vận đầy ẩn dụ và triết lí, hơn cả, những nhân vật này đều có một lỗ hổng trong tâm hồn ; những chuỗi chi tiết nhỏ nhặt nhưng đầy ẩn ý đã cũng nhau kết hợp thành một cuốn tiểu thuyết có giá trị. 
Đây là một cuốn sách khó hiểu, vì thế, những người đã lớn, nếu chưa trưởng thành cũng đừng nên mua đọc.

4
625507
2016-06-03 10:07:37
--------------------------
440944
5115
Mình rất thích chị zelda nên khi biết chị thích văn phong của Haruki Murakami mình tìm văn của bác và đọc. Cái bìa sách được thiết kế tinh kế, dày dặn,  hình vẽ cuốn hút, bí ẩn. Ngay từ cái bìa đã thấy ám ảnh vô cùng.
Về phần nội dung tác phẩm, phải nói đây là một câu chuyện ám ảnh. Khi đọc xong mình đã loay hoay suy nghĩ về nó rất nhiều những ngày sau đó. Câu chuyện diễn biến nhẹ nhàng, đọng lại những nội dung, triết lí sâu sắc. Ở phần cuối truyện, mình chưa hiểu lắm cái kết. Một cái kết mở, đem cho mỗi người một suy nghĩ.
5
433772
2016-06-02 15:28:49
--------------------------
434681
5115
Mình biết truyện này lâu rồi do có 1 người bạn mình rất thích và cái tên của nó cũng rất ấn tượng nên mình nhớ hoài. Cho tới sau này mình mới có dịp mua sách thì mình lại khá ấn tượng bìa với tông màu trắng, đỏ, đen. Cũng hào hứng cầm lên đọc ngay, nhưng thật sự đọc thì mình cũng k có bắt kịp mạch cảm xúc nhân vật, có lẽ truyện k phù hợp với mình rồi. Chắc có lẽ do mình chưa từng trải nên cũng không hiểu suy nghĩ của tác giả nữa. 
Bù lại được cái giấy in ở chất lượng tốt nên nhìn vẫn có cảm tình.
3
813663
2016-05-23 17:01:52
--------------------------
432705
5115
Đọc xong cuốn sách lại khiến tôi nặng lòng suy nghĩ, một cuốn sách mất một thời gian khá lâu để đọc xong nhưng lại để lại rất nhiều. Đó là câu chuyện đau đớn khiến tôi nhớ mãi. Cứ tưởng đến cuối cùng sẽ lại thuộc về nhau nhưng không! Giọng văn êm êm như ru ngủ, nhẹ nhàng đầy triết lý của một người ít nhiều đã trải đời. Không biết ở phía Tây mặt trời kia có gì, trong đời khó tránh khỏi mắc những sai lầm, nhưng một sự tiếc nuối cứ bao trùm ở Hajime khi đã để đánh mất Shimamoto-san, cứ lạc lối mãi đến 37 tuổi mới vỡ ra mình muốn làm gì, cần gì. Cuốn sách cho ta tĩnh tâm bởi giọng văn êm của tác giả, bình tâm suy nghĩ về những triết lý trong đời.
5
480412
2016-05-19 16:25:15
--------------------------
428828
5115
Mặc dù tựa đề là quyển sách cảm động sâu sắc nhất của ông nhưng mình vẫn chưa thấy cảm động được ở chỗ nào. Tên sách là phía Nam biên giới , phía Tây mặt trời nhưng mà không hiểu nó có liên quan gì tới cả quyển sách, phía Nam biên giới là nói về một bài hát của mexico, phía tây mặt trời là về một căn bệnh của người dân vùng xyria gì đó. Đây là quyển đầu tiên của Haruki Murakami mình đọc mà thấy khó cảm nhận được văn của ông, chắc phải đọc lại lần nữa nếu có thời gian
3
820063
2016-05-11 22:34:23
--------------------------
420910
5115
Truyện vẫn mang đậm phong cách của Murakami, không ồn ào, không vội vã, nhưng đọc từng từ lại càng thấy thấu hiểu. Vẫn lối viết truyện không nội dung, tuy nhiên mỗi tác phẩm là một mảnh đời, một diễn biến tâm lí nhân vật hoàn hảo. Nếu như ở "Kafka bên bờ biển", ta được gặp những hiện tượng thiên nhiên kì lạ như cơn mưa cá, thì ở "Phía Nam biên giới, phía Tây mặt trời", ta sẽ được trải nghiệm cuộc đời của nhân vật chính từ khi anh còn là đứa trẻ, cho đến khi lấy vợ và gặp lại được người bạn thơ ấu nhưng đặc biệt của mình. Giọng văn nhẹ nhàng nhưng lôi cuốn sẽ khiến ta đọc "ngấu nghiến" mà không tài nào rời mắt nổi!
5
292674
2016-04-24 02:38:25
--------------------------
418295
5115
Tôi đang đọc dở quyển sách và phải dừng lại để viết nhận xét này. Quả thực tôi rất tin tưởng thương hiệu tiki và đã mua khá nhiều sách ở đây, nhưng đôi khi tôi tôi nhận được sách với chất lượng bản lậu và cuốn sách này là một trong số đó. Hiện tại tôi đang đọc đến trang 160/290 và thấy rằng trang số 161 hoàn toàn bỏ trắng và không biết liệu các trang còn lại có bị như vậy nữa hay không. Tôi rất mong tiki có những phản hồi thoả đáng cho vấn đề này của tôi.
2
404090
2016-04-18 23:12:16
--------------------------
403082
5115
Tôi mua cuốn này vì yêu thích Rừng Nauy nên muốn đọc thêm một vài tác phẩm của Haruki Murakami. Sau khi đọc hết cả quyển thì tôi rút ra một kết luận rằng tôi không hợp với sách của ông. Lối viết của Haruki Murakami khá khó đọc, khó hiểu và nặng về tình dục. Một số đoạn kể chi li về việc quan hệ giữa nam và nữ tôi nghĩ không cần thiết lắm, hình như nó không có ý nghĩa gì đối với diễn biến của sách cả, tôi cho rằng những đoạn đó đơn giản chỉ để câu khách. Tuy nhiên, bỏ qua những khó chịu đó thì sách của ông vẫn mang lại những giá trị nhất định. Nhân vật trong truyện của ông luôn mang những tính cách, tâm tư rất điển hình trong mỗi người chúng ta, nếu chịu khó cảm nhận, rất dễ nhìn thấy mình trong đó. Nói chung là sách đọc được, nhưng không dễ chịu.
3
465963
2016-03-23 10:22:23
--------------------------
402268
5115
Đúng là sách của Haruki Murakami không phải là những cuốn sách dễ đọc. Với tôi cuốn sách này cũng vậy, với cách dùng từ và miêu tả nội tâm tuyệt vời của ông đã mang lại cho chúng ta một cái nhìn toàn diện về mỗi nhân vật mà ông xây dựng. Cân bằng giữa việc phác họa cho người đọc về ngoại hình nhân vật và đi sâu vào xây dựng nội tâm nhân vật. Để hiểu sâu sắc những dằn vặt mà Hajime đã phải trải qua, hay chính là sự trả giá cho những đau khổ trong tâm hồn mà anh đã gây ra cho những cô gái đi qua cuộc đời mình.
2
903103
2016-03-22 00:03:37
--------------------------
395137
5115
Vẫn là Haruki, vẫn độc, hay và gây nghiện không tưởng. Bằng giọng trần thuật ngôi thứ nhất gần gũi, đây là câu chuyện về "tôi" như bao cái "tôi" khác chúng ta đầy biến động và cuốn hút. Đây không phải là một cuốn tiểu thuyết cảm động sâu sắc gì cả, đây là cuốn tiểu thuyết chậm, thực tế và sắc sảo. Phía Nam biên giới, phía Tây mặt trời, mỗi người sẽ có mỗi cách cảm nhận khác nhau. Nhưng hơn hết, đây là một cuốn sách đáng để đọc
sách đẹp, tiki giao hàng rất hài lòng
4
412371
2016-03-11 14:57:48
--------------------------
394712
5115
Tôi thấy nhiều người ngần ngại khi đứng trước những cuốn sách của Haruki Murakami với nỗi sợ rằng bản thân chưa thể hiểu thấu nó. Phần mình, tôi nghĩ ngoài tác giả ra chẳng ai hiểu trọn vẹn được tác phẩm hơn nữa, thế nên với Haruki, chỉ có thích hay không thích.
Tôi thích cuốn sách này. Ông vẫn đưa những chi tiết vụn vặt ám ảnh vào và vẫn là kiểu câu truyện và lối hành văn ấy. Nhưng sao tôi có thể từ chối được chúng cơ chứ? Lại một mối tình buồn, nhân vật với cuộc đời buồn. Rồi thì cuốn sách này lại ám ảnh tôi, giống như mọi cuốn sách khác của Haruki Murakami thôi.
4
302453
2016-03-10 20:38:41
--------------------------
389268
5115
Đây là cuốn sách đưa mình đến với Haruki Murakami. Nhờ nó, mình đã trở thành fan ruột của ông. Mình mới 17 tuổi và có lẽ mình chưa đủ sâu sắc để hiểu hết những gì ông muốn truyền đạt, nhưng mình nghĩ trong những phần mình có thể hiểu được thì mình thích truyện của ông. Phía Nam biên giới,  phía Tây mặt trời có lối viết rất thực, bí ẩn như những cuốn truyện khác của Haruki. Đọc truyện, người ta cứ mải kiếm tìm cái nơi gọi là phía Nam biên giới, phía Tây mặt trời, và tự hỏi đó có phải là cái nơi hạnh phúc mà Hajime và Shimamoto hằng mơ ước đến không? Thế nhưng sau khi gấp cuốn sách lại, ta lại nhận ra những điều hoàn toàn khác. Đó là những tình cảm cao hơn tất cả, tình yêu, rồi tình bạn, nhưng trên hết là tình cảm gia đình. Con người chúng ta, trong cuộc đời, có thể sẽ đứng trước nhiều ngã rẽ, nhưng cuối cùng, nơi tốt nhất, an tòan và đáng trân trọng  nhất vẫn là mái ấm gia đình. Cái kết truyện đã thể  hiện điều đó, khiến mình hoàn toàn hài lòng khi kết thúc cuốn sách.
4
768677
2016-03-01 19:13:58
--------------------------
384490
5115
Đây có lẽ là tác phẩm dễ đọc nhất của Haruki, và cũng là cuốn sách đưa mình đến với thế giới văn chương của bác mà chưa từng có một tác giả nào khác trước đó cuốn hút đến vậy. Câu chuyện với nòng cốt chân thực và đơn giản, có nhiều mối liên kết với cuộc sống ngoài đời thực của Haruki. Đơn giản nhưng không hề đơn thuần, Haruki luôn có năng lực đặc biệt lôi kéo độc giả vào vòng xoáy nội tâm nhân vật của mình thật sự xuất sắc, trong bất cứ tình huống truyện, hoàn cảnh nào. Có lẽ chỉ có duy nhất một người thực sự thích hợp dành cho mỗi chúng ta trong cuộc đời này, chúng ta say mê, cuồng si trong cái màng lưới đó, sẵn sàng vứt bỏ mọi thứ để được ở bên người mình yêu. Ta biết rằng điều đó là sai nhưng nếu được quay trở lại, ta vẫn tiếp tục mắc sai lầm đó. Để rồi sau cùng, tất cả những gì còn lại chỉ là sa mạc - trống rỗng và vô định.
4
1152842
2016-02-22 14:29:41
--------------------------
380613
5115
" Ngắn gọn mà sâu sắc. Tất cả chúng ta trải qua đều quá suôn sẻ vậy nên chúng ta không cảm nhận được niềm hạnh phúc thực sự: Gia đình, sự nghiệp. Bị cuốn theo 1 ngách nhỏ của cuộc sống_ sâu lắng, day dứt, da diết. Cố gắng tìm ra bản thân mình là ai!. ĐÓ cũng chính là câu hỏi đại bộ phận con người sống hiện nay" - Tôi đã viết như thế trong cuốn sách
Hajime- Một người đơn giản và nhẹ nhàng trong tình yêu với cô gái tên Shimamoto- San. Rồi một ngày 2 người không gặp nhau nữa. Hajime tiếp tục sống và có vợ con với một cuộc sống đuề huề từ người Bố vợ cung cấp. Thế rồi một ngày cô gái Shimamoto- San quay lại trong quán Bar của Hajime, Mọi chuyện rắc rối bắt đầu từ đó. Những dằn vặt của bản thân Hajime. Tất cả sẽ tạo ra một câu chuyện đầy ý nghĩa về sự chung thủy, thành thật trong cuộc sống gia đình .Những vấn đề nổi cộm của xã hội Nhật Bản được đưa vào tác phẩm. 
Tuy không xuất xắc bằng Rừng Nauy, xong nó cũng rất xứng đáng để đọc. Bạn sẽ nhận ra một chất văn của Haruki 
Đánh giá giấy in: Nhã Nam luôn làm tôi hài lòng
Bản dịch dễ hiểu
5
919665
2016-02-15 13:35:31
--------------------------
380022
5115
Cuốn sách lần đầu tiên được đọc vỏn vẹn trong hai ngày là hết, cốt truyện không có gay cấn, cao trào hồi hộp nhưng hoàn toàn lôi cuốn đến kỳ lạ.
Tôi nhớ đâu đó hoặc nhiều đọc giả sẽ nhận xét một câu truyện ngớ ngẫn của người đàn ông sắp bước vào tứ tuần còn muốn bỏ tất cả sự nghiệp đang đi vào ổn định, một gia định êm ấm trong sự phục tùng của người vợ và hai đứa con thơ....để chạy theo cái gọi là tình yêu đầu đời dở hơi. Nhưng vì bạn chưa từng được nếm mùi, được trải qua, được đắm chìm trong thế giới mà chỉ có hai con người tự cảm nhận được đối phương chỉ đơn giản bằng ánh mắt, không cần phải nói thành lời, đó mới chính là lý do để một người sẵn sàn từ bỏ tất cả chạy theo một người như vậy.
4
420322
2016-02-13 16:01:19
--------------------------
377133
5115
Phải nói đây là tác phẩm thứ hai của Haruki Murakami mình đọc và mình không hề thấy uổng phí sức lực và tiền bạc cho nó tí nào cả. Quả là một tác phẩm khiến người đọc suy nghĩ. Có một câu chuyện ở đây, nhưng mình không biết chắc nó sẽ kết thúc thế nào. Và nói rõ ra, mình cũng không quan tâm mấy. Những tác phẩm đọc xong mà dạy ta điều gì đó chỉ có trong tủ sách tư duy. Bạn sẽ biết một cuốn sách hay khi bạn đọc nó và đây là một cuốn sách hay
5
383240
2016-02-01 18:19:11
--------------------------
369646
5115
Phía Nam biên giới, phía Tây mặt trời - 2 phía khác nhau! một phía bên kia chẳng biết là sẽ còn gì còn phía này thì chỉ có mặt trời, nơi mà chúng ta có thể ngắm mặt trời. Phải chăng tác phẩm chính là nổi lòng của Haruki Murakami, nhưng điều đã trả qua, đã tiếc nuối và đã hối hận. Hẳn những ai đã từng trẻ thì cũng có ít nhất một điều day dứt và tiếc nuối. 2 ngày cuối tuần để có thể đọc hết được tác phẩm. Tự nhiên bị ám ảnh ở cái mốc tuổi 37, giống như một vài người tuổi 37 khác họ cũng tương tự như vậy. 
 Hajime đã làm tổn thương những người yêu thương mình và làm tổn thương chính anh. Biết làm sao được khi tuổi trẻ đã qua đi mà ta muốn làm lại những điều trước đó, từ "Nếu" sẽ chẳng bao giờ xảy ra cả. 
Một cái kết khi mà mọi chuyện đều trở về đúng chỗ của nó, Hajime đã hài lòng với những gì mình đang có, khi anh có Yukiko một người vợ quá tuyệt vời.
Sau khi đọc xong, tự hỏi lại mình rằng: " Liệu mình đã bỏ qua giấc mơ hay niềm đam mê nào không? Để sau này đỡ phải dằn vặt và bứt dứt như anh chàng Hajime này." Sẽ quá đau khổ nếu ai cũng phải trải qua những điều đó. Sad
5
309333
2016-01-16 09:16:36
--------------------------
359808
5115
Câu chuyện nhẹ nhàng, tình tiết rất chân thật không khó hiểu như những tác phẩm khác của ông, cảm giác như đang ở trong một câu chuyện thật, một cuộc đời thật. Chúng ta như tìm thấy con người của Haruki Murakami trong tác phẩm của ông.
Mắc dù tác phẩm khá ngắn, tuy nhiên nó thể hiện một cách đầy đủ rõ nét lối hành văn của tác giả. Luôn mang lại một nỗi ám ảnh cho người đọc.
Mỗi người phụ nữ trong tác phẩm có một ý nghĩa nhất định trong cuộc đời Hajime 
Shimamoto-san là giấc mơ, là khát vọng của bản thân tác giả.
Yukiko là cuộc sống gia đình đầm ấm.
Izumi là tình yêu cháy bỏng tuổi học trò.
Chị Izumi là khát khao tính dục.



































5
714111
2015-12-28 02:31:57
--------------------------
339915
5115
Vì cái bìa + cái tiêu đề khiến tò mò nên mua thử. Văn phong giản dị và nhịp nhàng, xoáy sâu vào cái "thực" và "không thực" làm khi đọc đến hết quyển sách để lại vẫn là những mập mờ, những câu hỏi cũng như chính bản chất cuộc sống này cũng vậy. Rất thích hợp cho những người sống nội tâm tự nhìn nhận và tìm ra cho mình 1 hướng suy nghĩ, hướng đi thích hợp.
Túm lại là khá hài lòng, chỉ có 1 số chỗ dịch giả dùng từ hơi thô làm cảm giác khi mình đọc không tốt mấy.
3
416294
2015-11-18 22:12:25
--------------------------
338498
5115
Mình nghe nhiều người nói đây là 1 trong những truyện cảm động nhất của Haruki Murakami. Cũng chuẩn bị tâm lý trước rồi vì trước giờ mình đọc truyện của ông này không có được :(( Không chỉ khó hiểu mà cảm giác nó cứ bức bối sao ấy. Với quyển này thì tuy ngắn nhưng cũng không dễ đọc chút nào. Cảm giác thực và ảo nó cứ đan xen với nhau, từ đầu đến cuối mạch đều đều khiến mình thấy chán.Có lẽ sự trải nghiệm của mình chưa đủ nhiều và sâu sắc để có thể hiểu hết những câu chữ uyên thâm của tác giả.
3
476360
2015-11-16 11:51:21
--------------------------
337539
5115
Trên hết thì đây là một câu chuyện viết về con một, và những đứa trẻ kiểu sheltered child, vốn thuộc về thiểu số trong hoàn cảnh xã hội Nhật Bản thời bấy giờ và cả Việt Nam một vài thập niên về trước. Một điều tất yếu rằng thiểu số lúc nào cũng phải chịu khá nhiều thiệt thòi, dễ cảm thấy lạc lõng và có sự phát triển nội tâm khác hẳn với số đông cũng như những quy chuẩn của cộng đồng xã hội, do đó khó hòa nhập hơn và tương đối vất vả để tìm kiếm được một người giống mình, cùng chia sẻ mọi điều trong cuộc sống. Tôi cũng là con một, sinh trưởng trong giai đoạn gần giống như nhân vật chính Hajime và trải qua những suy nghĩ, triết lý, thậm chí có một mối quan hệ cũng khá tương đồng với anh trong những năm tháng đầu đời, vì vậy đọc câu chuyện này mà tôi cảm thấy một sự đồng cảm sâu sắc và thú vị. Tuy diễn biến cuộc đời của Hajime về sau khá lạ, nhưng biết đâu đấy, cuộc đời luôn có thể biến chuyển theo những hướng mà ta không bao giờ ngờ tới, biết đâu tôi cũng sẽ gặp phải ít nhiều những hoàn cảnh giống như anh.
4
711854
2015-11-14 11:10:00
--------------------------
330908
5115
Người dịch:		Cao Việt Dũng
NXB:			Nhã Nam
Thể loại:		Tâm lý tình cảm
Bìa:			Đẹp
Chất lượng in, giấy: Tốt
Chất lượng dịch:      Tốt
Nhận xét:                 Có lẽ đây phải là cuốn sách các phụ huynh nên có để cho con cháu đọc để con cháu hiểu hơn thế nào là tình yêu và tình yêu sẽ sống như thế nào. Và có lẽ phụ nữ nên đọc cuốn này trước khi lấy chống để hiểu tình yêu là gì? Và cuộc sống sau hôn nhân sẽ là gì? Và có lẽ chính bản thân tôi cũng nên đọc cuốn này sớm hơn để biết tại sao tình yêu có lý lẽ riêng của nó. Murakami có quá nhiều trãi nghiệm về cuộc sống gia đình. 
 Lời khuyên:               Nên có và đọc.

5
129879
2015-11-03 16:23:14
--------------------------
315198
5115
Mình thường có xu hướng thích các nhân vật nữa của Haruki Murakami (như Midori trong "Rừng Na Uy", hay Kasahara May trong "Biên niên ký chim vặn dây cót"). Ở cuốn sách này mình lại không có cảm giác  "thích" bất cứ cô gái nào cả mà không hiểu lý do tại sao. Ngược lại đây là lần đầu tiên mình ghét một nhân vật nào đó trong các tác phẩm của ông. Mình ghét Hajime. Ghét không phải vì cậu ta độc ác hay xấu xa gì đó, cậu ta hoàn toàn không như thế. Mình ghét vì những  cô gái đi qua cuộc đời Hajime đều quá thê thảm. Người thì đau khổ vì bị bỏ rơi mà mất hết bản ngã, người thì biệt tích, xuất hiện trở ại, sau đó lại biệt tích, đến cả vợ của anh ta còn từng  tự vẫn trong quá khứ nhưng không thành sau đó lại còn suýt bị anh ta bỏ rơi. Mình cực "khó chịu" với Hajime.
Đấy chỉ chút cảm xúc riêng tư cá nhân còn về tổng thể khách quan thì "Phía nam biên giới, phía tây mặt trời" là 1 tác phẩm khá ổn cho dù chưa phải là cuốn xuất sắc nhất của Haruki Murakami. Những bạn chưa từng đọc sách của ông thì đây là cuốn rất đáng để  "mở đâu". Nó khá dễ đọc, ít các chi tiết siêu thực và phản ánh 1 câu chuyện mang nhiều nét đời thường.
4
263240
2015-09-28 13:52:49
--------------------------
289995
5115
khi mình đọc tác phẩm này cảm giác của mình rất khó chịu có lẻ theo mình  Hajime là người thiếu trách nhiệm vs cô bạn gái đầu tiên và với Yukiko ! nhưng mình bị cuốn vào mạch cảm xúc của tr,  có quá nhiều câu hỏi được đặt ra ch gì đã và đang xảy ra cho Shimamoto... có quá nhiều câu hỏi không có lời giải làm mình ám ảnh gần cả tuần ! 
Tuy nhiên nghỉ lại có thể cách hành xữ của  Hajime đó là cách duy nhất để sống tiếp, để tiếp tục tiến lên bỏ lại quá khứ sau lưng ! Dù ko quên được  Shimamoto nhưng cuộc sống này đâu có dừng lại thà chọn một cuộc sống khác đi ! Những con người này  Hajime và Shimamoto họ quá cô đơn trong thế giới riêng của mình !
4
390304
2015-09-05 00:06:42
--------------------------
279692
5115
Có thể ví von Murakami là một nhà văn "truyện người lớn". Thật vây, hầu hết truyện ông kể đều khá thoải mái về vấn đề muôn thuở - tình dục của con người. Một con người như Hejime vẫn đâu đó trong xã hội ngoài kia, như một phép ẩn dụ của Murakami về xự xung đột Tình Yêu - Gia Đình - Cái Tôi bản ngã. Một câu chuyện chưa phải thật xuất sắc nhưng với kết cấu giản dị, người đọc có thể cẳm nhận được triết lý của nhà văn. Thông qua nhân vật, ông luôn đặt ra những câu hỏi tuy đơn giản mà hàm ý sâu xa, và chỉ có người đủ trải nghiệm mới có thể tự trả lời - cho câu hỏi của chính mình.
4
621799
2015-08-27 01:04:00
--------------------------
274894
5115
Tất cả cảm xúc sau khi gấp lại cuốn sách chỉ là bực tức và buồn. Một loạt câu hỏi cứ luẩn quẩn trong đầu mình về những việc làm của những-người-trưởng-thành. Mình đọc ngôn tình nhiều, nhưng những-người-trưởng-thành trong ngôn tình hoàn toàn khác ở đây. Khi mình cứ bảo thủ nhìn vào một thế giới, mình sẽ thấy kì lạ và ám ảnh khi nhìn vào thế giới khác. Ngôn tình chưa bao giờ chỉ toàn màu hồng, nhưng ít nhất nó không u ám như này. Mình đã nghĩ, đâu mới thực sự là người-trưởng-thành mà thế giới xung quanh mình đang chất chứa? Là những chàng trai si tình, những cô gái chỉ hiến thân cho duy nhất một người đàn ông? Hay những chàng trai ích kỉ và lạc lối như Hajime và những cô gái chỉ có thể cam chịu và cam chịu như Yukiko? 10 năm nữa, 20 năm nữa, khi mình vô tình hay cố tình đọc lại cuốn sách này, liệu mình có thể cảm thông cho Hajime?
4
724807
2015-08-22 13:07:40
--------------------------
274088
5115
Mình không thích cuốn này lắm nhưng không có nghĩa là nó không hay. Mình chỉ là không thích Hajime giống như kiểu không chấp nhận được loại đàn ông thiếu trách nhiệm đó. Tất nhiên văn phong của Haruki vẫn tuyệt, cái tình yêu đặc biệt của Hajime và Shimamoto vẫn thu hút mình. Nhưng thực sự mình vẫn bị khó hấp thụ cuốn sách và nội dung Haruki muốn truyền tải. Cuốn này mình đọc rất lâu vì nó cứ nhẹ nhàng cứ trôi như đời trôi, như chuyện hiển nhiên. Rất nhiều lúc muốn kết thúc cuốn này thật nhanh nhưng càng đọc thấy đầu óc cứ càng lơ mơ. So với Rừng Na uy thì mình thích Rừng Na Uy hơn.
3
380813
2015-08-21 17:09:57
--------------------------
266148
5115
Mình còn trẻ, mình vẫn đang là sinh viên, và có lẽ mình vẫn chưa đối mặt với cuộc đời nên mình không cảm được những gì ông muốn nói trong truyện. Nói như vậy không có nghĩa là mình không cảm nhận được hết tác phẩm. Từng say mê văn phong của Haruki Murakami thông qua tác phẩm nổi tiếng của ông - "Rừng Na-uy", đó là lí do mình đã tìm đến "Phía Nam biên giới, phía Tây mặt trời", và nó cũng không làm mình thất vọng. Nếu bạn yêu thích văn phong của ông, u buồn, trần trụi và sexy thì chắc chắn bạn sẽ thích quyển này, nhưng ông không hề lối mòn nhé, và đừng lo vì điều đó. Một điều nữa mình muốn nói và, mình rất ghét, thực sự rất ghét nam chính trong hai tác phẩm mình đã đọc, tại sao chứ, Watanabe có thể tha thức được vì hắn ta vẫn chưa có vợ con, còn Hajime, sao ông có thể bỏ mặt lại tất cả chỉ vì một cô gái, khi người ta già, có thực sự tình cảm còn lớn như vậy không, có lẽ mình bị ảnh hưởng bởi anh chàng Ngạn trong "Mắt Biếc" và Chương trong "Còn Chút Gì Để Nhớ", họ quá chung tình đến...tội nghiệp. Nhưng dù sao, không vì lí do ghét Hejime mà mình không để 5 sao cho tác phẩm này, thật tuyệt vời!
5
616945
2015-08-14 13:59:26
--------------------------
258235
5115
Mình đã đặt nhiều hy vọng ở quyển sách này và đã thất vọng. Những tác phẩm văn học Nhật Bản mình đã có dịp tiếp xúc đều rất nhẹ nhàng, sâu lắng, nhiều cảm xúc và rất dễ dàng để cảm nhận. Nhưng, khi đọc xong quyển sách này, mình vẫn không cảm thấy gì cả. Mình không phải nói tác giả hay dịch giả làm không tốt, nhưng cũng không phải là do bản thân kì thị những chuyện ngoại tình của những người đã có gia đình (mãi nửa sau mới có chi tiết này và cao trào ở cuối truyện). Thật xin lỗi tác giả và dịch giả vì mình không cảm thấy hài lòng ở tác phẩm này, nhưng cảm xúc của mỗi độc giả mỗi khác mà, phải không?
2
247339
2015-08-08 12:33:23
--------------------------
217903
5115
Câu chuyện được viết như những lời tự sự của Hajime về cuộc đời mình, đúng hơn về cuộc sống nội tâm và những bóng hồng bước qua đời ông.Tôi tự hỏi, với lời tựa ngoài quyển sách, liệu trong hình hài của nhân vật Hajime, thực sự tác giả đang kể về mình?  
Khi câu chuyện đi qua thời niên thiếu, anh chàng con một Hajime khiến cho cô bạn Izumi vỡ nát vì sự phản bội xác thịt của cậu với người chị họ của cô. Tôi thấy thương cho cô bạn, nhưng vẫn tự nhủ rằng "Tuổi mới lớn, đôi khi chẳng phân biệt nổi đúng sai, chỉ làm theo bản năng sinh học của mình" Tôi thông cảm cho Hajime...
Nhưng khi câu chuyện lên đến cao trào ở lứa tuổi 37, Hajime trong suy nghĩ của tôi là người đàn ông lịch lãm, điềm đạm, mang phong thái của những người đàn ông thành công đã trưởng thành. Ở Hajime 37 tuổi, ông có tất cả những thứ mà người đời mơ ước, không phải là quá lớn lao, nhưng chắc chắn, rất nhiều người phấn đấu cả đời cũng không đạt được những gì ông có.  Những tưởng với sự yên bình vững chãi đó, ông có thể dễ dàng vượt qua những cám dỗ bên ngoài, vậy mà, khi Shimamoto-san xuất hiện, ông quay cuồng lao vào thứ đam mê nghiệt ngã kia, sẵn sàng từ bỏ mọi thứ vì người tình xưa cũ năm 12 tuổi. Tôi cảm thấy kinh tởm những điều người đàn ông này làm, những thứ trắng trợn được đội lốt tình yêu khiến tôi kinh sợ. 
Nhưng cái kết với cuộc trò chuyện nhẹ nhàng giữa Hajime và Yukiko, với những giấc mơ của Yukiko, khiến tôi nhận ra rằng, bất cứ ai cũng có thể mắc sai lầm, bất cứ ai cũng có thể có những giấc mơ còn giang dở, bất cứ ai cũng có thể sa lầy...Nó khiến tôi bình tâm và ngẫm nghĩ sâu sắc, ít những góc nhọn thô ráp hơn về ý nghĩa của cuốn sách. Đây là cuốn sách hay được viết rất nhẹ nhàng, dành cho những con người còn đang quằn quại với cuộc chiến nội tâm của mình! :)
4
671508
2015-06-30 10:36:35
--------------------------
199808
5115
Một câu chuyện về cuộc sống của nhân vật Hajime từ khi còn bé ,đến khi trưởng thành, những bước ngoặc trong cuộc đời anh, những ám ảnh ,day dứt về quá khứ, về những người anh làm tổn thương. 
Trải dài câu chuyện là một lối viết nhẹ nhàng, không quá gây nhiều kịch tính, nhưng cũng khó mà đặt cuốn sách xuống khi chưa đọc xong . Có đôi khi tôi cảm thấy thật giận nhân vật Hajime,bởi những tổn thương mà anh gây ra cho những người phụ nữ cạnh mình, cho Izumi, cho vợ anh Yukiko....
Kết cục câu chuyện có lẽ đối với 1 số người là lấp lững, là còn bỏ ngõ, bởi lẽ nhân vật Shimamoto , nhưng với tôi đó là 1 kết cục cần có . Hajime cần trở về ben những điều là có thật ,đang tồn tại ,thuộc về anh là gia đình, vợ con anh, chứ không phải là 1 bóng hình thoắt ẩn thoắt hiện, của mối tình đầu xa cũ
5
616860
2015-05-23 10:34:38
--------------------------
197381
5115
Ám ảnh có lẽ là một trong những từ chính xác nhất để miêu tả về các tác phẩm của Haruki Murakami. Cũng như những cuốn sách khác "Phía tây biên giới, phía nam mặt trời" để lại trong tôi nhưng khoảng lặng hụt hẫng rất khó diễn giải sau khi đọc xong. "Phía nam biên giới phía tây mặt trời chỉ được nhắc đến một lần trong sách nhưng nó là chủ đề bao trùm cả cuốn sách: nó miêu tả sự mất phương hướng của nhân vật chính, người mà ngay cả khi tưởng như rất thành công thì thực ra vẫn không thể xác định một cách rõ ràng rằng mình là ai, mình muốn gì và mình phải làm gì. Tuy nhiên mừng là cuối cùng anh ta vẫn không phản bội vợ
4
507583
2015-05-17 14:54:00
--------------------------
190750
5115
"Phía Tây mặt trời" nghe thật lạ, ban đầu tôi tự hỏi tác giả đang muốn ẩn dụ cái gì, hay là hư cấu câu chuyện của một hành tinh khác, rồi sau đó câu chuyện của Shimamoto-san đã giải thích cho tôi. Nó đơn giản hơn tôi nghĩ rất nhiều, nhưng cũng ý nghĩa hơn những gì tôi tưởng tượng. Mặt trời ở phía tây ấy, nó vừa là biểu tượng của sự sống, biểu tượng của cái chết. Bạn muốn tìm lí tưởng của cuộc sống và theo đuổi nó, nhưng cứ theo nó tới tận cùng, bạn mới nhận ra là vô ích và bỏ lỡ biết bao điều, đến khi kiệt quệ thì đã muộn. Nhân vật chính có một cuộc sống đầy đủ, hạnh phúc, nhưng Shimamoto, người bạn thưở nhỏ xuất hiện, làm xáo trộn cuộc sống của anh. Với anh, cô chính lí tưởng. Nhưng rồi tất cả biến mất một cách chóng vánh, tất cả chỉ như một giấc mơ, thứ cuối cùng còn lại bên anh là người vợ. Gia đình mãi là chỗ dựa vững chắc cho chúng ta. Câu chuyện này của Haruki Murakami đơn giản, êm ái và thấm dần vào tâm hồn chúng ta một cách tự nhiên nhất.
4
403246
2015-04-30 09:39:45
--------------------------
160643
5115
Haruki Murakami có lẽ là nhà văn viết riêng cho những người trưởng thành,hay ít nhiều đã trải đời,bởi vậy mà khi đọc tác phẩm này,cũng như Rừng Na uy,thật khó để cảm nhận và đọng lại nội dung,triết lý sâu sắc mà nhà văn gửi gắm.
Bìa sách đẹp,bí ẩn.
Nội dung xoay quanh cuộc sống vừa thực vừa ảo của nhân vật Hajime,không có cao trào,cũng như phần lớn các tác phẩm của Murakami,nên khá buồn ngủ.
Đọng lại sau khi đọc sách,với mình,đó là sự day dứt của Hajime khi phải làm tổn thương những người xung quanh,theo một cách nào đó,đôi lúc chúng ta trong cuộc sống cũng phải làm như vậy.
3
477889
2015-02-25 09:27:30
--------------------------
156842
5115
Khép lại "Phía Nam Biên Giới, Phía Tây Mặt Trời" tôi bỗng dưng suy nghĩ: Phải chăng trong cuộc đời mỗi người đều tồn tại một Shimamoto-san. chấp niệm mà ta không thể buông bỏ. Ai từng xem "Great Gasby" hẳn sẽ có đôi chút liên tưởng nhân vật chính trong truyện của Haruki Murakami với Gasby- luôn luôn dành một vị trí quan trọng trong trái tim mình chi người con gái dầu tiên. Nhưng cảm xúc Haruki đem lại cho ta không phải sự bi lụy, tuyệt vọng mà là những lí do để bước về phía trước. Đây là một câu chuyện đáng đọc.
4
475008
2015-02-06 16:56:18
--------------------------
155243
5115
Cuốn sách không phải là tác phẩm tiêu biểu nhất của Murakami nhưng là một tác phẩm đậm chất văn học Nhật Bản. Có cái gì như là một đám mây xám mờ che phủ lên cuộc đời của các nhân vật. Họ như bị nhốt trong phòng kính, trên đầu là mây mờ, họ loay hoay trong căn  phòng chật chội của đời mình, nhiều khi bế tắc, nhiều khi bất lực, muốn buông xuôi. Nhân vật nhìn mà không thấy, mọi thứ chỉ dựa vào những xúc cảm mạnh mẽ tự thân.  Không đi vào đả phá chua cay như văn học Trung Quốc, văn học Nhật mà đại diện là Murakami đem đến một cảm nhận khác biệt khó trộn lẫn với người đọc.
4
534017
2015-01-31 17:10:45
--------------------------
152030
5115
Ở một độ tuổi non nớt thì khó lòng tiếp nhận được "Phía Nam biên giới phía Tây mặt trời". Cuốn sách này thực sự già dặn và chín muồi trong tình cảm của con người ta và Hajime - theo tôi nghĩ đã tròn vành trong cái tình cảm ấy. Tất nhiên chỉ là ý kiến cá nhân thôi, nhưng cuốn sách này mang lại độ dư không cao như các tiểu thuyết trước đó của Murakami. Có chăng chỉ là những câu hỏi để lại và ở lại một cách dài lâu, kích thích trí tò mò trên cơ sở đã được tính toán. 
Đứng trên giác độ của một người trẻ chưa 30, tôi cảm ra mình chưa chạm đến cái tận cùng của tình cảm trong nó. Nó rất khác và rất cá biệt. Và về nhân vật, Hajime làm tôi say đắm, Shimamoto làm tôi thấy bối rối. Yukiko mang cho tôi cảm giác an toàn, Izumi lại bí ẩn. Và rồi thì chỉ còn câu hỏi ở lại, đúng như ý đồ...
4
541549
2015-01-21 21:04:00
--------------------------
147845
5115
Tôi sẽ không đánh giá cuốn sách này quá cao, bởi dường như nó không mang nhiều đột phá đối với văn phong của Haruki. Đọc một lượt cuốn sách này, tôi không nghĩ người ta có thể nhớ đến từng chi tiết hay từng câu văn, có thể nó là đặc trưng của văn học Nhật, cũng có thể cuốn sách này vốn dĩ nói về một điều không thể chi tiết hóa. Mơ hồ và day dứt là hai từ có lẽ dễ nói nhất về cuốn sách này. Mơ hồ không biết Shimamoto sẽ đi đâu, sẽ như thế nào, quá khứ đã xảy ra chuyện gì, hiện giờ cô đang sống ra sao ... Day dứt một đoạn tình cảm đẹp như vậy thế mà cũng không đi đến hồi kết ... Không thể không nói là tò mò, nhưng cuối cùng ai cũng sẽ nhận ra, giống như cuối sách viết: "... đã trở thành quy luật, những câu trả lời thì qua đi, còn câu hỏi thì ở lại".
4
100688
2015-01-08 22:34:13
--------------------------
145759
5115
Tôi không nghĩ có thể dùng từ cảm động sâu sắc để hình dung về cuốn sách này. Khi đọc sách xong tôi chỉ thấy trong lòng mình dâng lên những xúc cảm mông lung, bâng khuâng, mơ hồ, không rõ nét và khó nắm bắt. Truyện kể ngôi thứ 1, phần lớn mang màu sắc cá nhân, ít đối thoại nên mới bắt đầu đọc sẽ thấy nhàm chán và buồn ngủ. Nhưng có lẽ đây cũng là đặc trưng của văn học Nhật. Cuốn sách kể một câu chuyện bình thường, khép lại một cách cũng rất tự nhiên song vẫn đặt ra nhiều câu hỏi về cô gái Shimamoto. Giống những gì đã giới thiệu sau sách, tôi nhận ra không phải câu hỏi nào cũng cần câu trả lời và nhiều khi câu trả lời cũng chẳng còn quan trọng.
3
146361
2014-12-31 21:59:10
--------------------------
