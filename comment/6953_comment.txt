356145
6953
Bộ sách Con là bé ngoan gồm 6 tập do Nhà xuất bản Dân Trí phát hành. Tập 3 trong bộ sách này bao gồm 36 trang, có các hình minh họa rất dễ thương. Nội dung trong tập 3 bao gồm : thói quen hằng ngày (ăn mặc gọn gang, sạch đẹp, thắt dây giày, bảo vệ đôi mắt), ở nhà (đóng mở cửa, ăn cơm, đưa vật nguy hiểm, nhận điện thoại, tiếp đón khách), ở trường (chăm chú nghe giảng, lên xuống cầu thang, tiết kiệm nước), ở nơi công cộng (sử dụng thang máy, đi xe buýt, hỏi đường, chỉ đường). Các quy tắc ứng xử được trình bày theo dạng thơ rất dễ nhớ và sinh động.
3
15022
2015-12-21 13:08:33
--------------------------
346893
6953
Bộ sách "Con là bé ngoan" rất có ích. Đúng với lời giới thiệu của sách "Để bé yêu khôn lớn, phát triển toàn diện và tự tin".
Ở quyển Con là bé ngoan tập 3 này có các mục lớn: Thói quen hàng ngày, Ở nhà, Ở trường, Ở nơi công cộng và Ngày lễ tết. Trong đó có các mục nhỏ khác như: Bảo vệ đôi mắt, Tiếp đón khách, Tiết kiệm nước, Đi siêu thị, Trung thu... 
Cách hướng dẫn cụ thể, hữu ích cho bé. Ví dụ: Mục bảo vệ đôi mắt hướng dẫn cách bé ngồi đọc sách như thế nào cho đúng. Phần sử dụng thang máy học để hạn chế những nguy hiểm có thể xảy ra đối với trẻ. Hay phần nói về quốc khánh nước ta để bé hiểu thêm và tự hào về đất nước mình.
Những phần nhỏ nhưng bổ ích.
5
241211
2015-12-03 15:27:01
--------------------------
260570
6953
Sách đẹp về cả hình thức và nội dung. Hình ảnh và màu sắc đều rất đáng yêu, chất lượng giấy in khá tốt. Sách có năm nội dung chính: Thói quen hằng ngày, Ở nhà, Ở trường, Ở nơi công cộng, và Ngày lễ tết. Mỗi phần có các bài học hữu ích gắn liền với cuộc sống và sinh hoạt của bé. Các bài học không hề khô khang, bé có thể tiếp thu một cách nhẹ nhàng qua các hình ảnh, và bố mẹ có thể tương tác với bé, hỏi bé về các tình huống và lắng nghe cách bé sẽ ứng xử trong tình huống đó như thế nào. Nếu bé có những cách ứng xử chưa đúng lắm, bố mẹ có thể giải thích cho bé hiểu. Mình thấy sách thật sự hữu ích với con mình.
5
476955
2015-08-10 15:19:54
--------------------------
219772
6953
Nếu nói về dòng sách cho bé, đặc biệt là sách cho các bé có độ tuổi từ hai trở lên thì Đông A có chất lượng sách hơn hẳn. Giấy in và màu in rất đẹp mắt, khi cẩm quyển sách trên tay là muốn xem một mạch đến hết. Nội dung bộ sách rất hay và phong phú. Có khá đầy đủ các vấn đề ứng xử sảy ra xung quanh cuộc sống của các bé, sách hướng dẫn các bé làm như thế nào mới đúng  trong các tình huống. Theo mình bộ sách không chê vào đâu được  Cảm ơn các tác giả đã mang đến cho các bé một quyển sách quý.
5
615810
2015-07-01 22:07:25
--------------------------
