463608
4088
Cuốn sách này không phải là một cuốn sách hướng dẫn làm món ăn gồm những nguyên liệu gì. Mà nó là một quyển sách chỉ những thủ thuật khi nấu ăn làm sao để có một món ăn ngon mà tốt cho sức khỏe nhất. Mình thích quyển sách như thế này bởi không quá gò bó vào cách thức làm mà không hiểu mình làm như vậy để làm chi, đốt cháy giai đoạn này sẽ bị vấn đề gì. Ẩm thực phân tử lý giải những nguyên nhân gây đến các hiện tượng đó khi nấu ăn và phải nấu nguyên liệu đó như thế nào để tốt nhất. Chúng ta làm việc gì cũng nên hiểu cơ chế của nó để có thể thiên biến vạn hóa trong nấu ăn. Rất tuyệt vời!
5
74132
2016-06-29 14:21:50
--------------------------
456658
4088
Bí ẩn của cái chảo - quyển sách hấp dẫn một đứa thích nấu ăn và thích đồ ăn ngon như mình ngay từ cái tên. Quyển sách không phải tập hợp những công thức nấu ăn,  như là nấu món này cần những nguyên liệu gì, trọng lượng bao nhiêu, nấu trong vòng bao lâu hay nhiệt độ như thế nào? Quyển sách tiếp cận ẩm thực trên phương diện khoa học như là phản ứng sinh hóa trong quá trình nấu các món luộc, các món chiên, các món nướng, các món xa lát.... diễn ra như thế nào như thế nào? Nhưng cũng chính vì thế mà quyển sách cũng hơi bị khô khan kiểu khoa học
4
368299
2016-06-23 16:39:49
--------------------------
450119
4088
Đầu tiên phải nói rằng tôi là người yêu khoa học và yêu ẩm thực, tôi cũng có chút hiểu biết về hai lĩnh vực đó. Cho nên ngày từ những trang đầu tôi đã có cảm nhận rằng cuốn sách như được viết cho riêng mình. Có thể nói cuốn sách là sự giao thoa vô cùng thú vị, độc đáo của hai lĩnh vực tưởng chừng chẳng liên quan gì nhau. 
Cuốn sách sẽ rất thích hợp với những bạn thích ẩm thực Phương Tây hơn, tuy nhiên như đã nói, chỉ cần bạn yêu thích một trong hai thứ trên đã đủ lý do để bạn mua sách này. 
Rất nhiều thông tin hữu ích bạn sẽ đọc được thông qua cuốn sách này. Ví dụ: tuyệt đối không được cho trẻ sơ sinh ăn xúc xích, thịt sấy khô. Vì cơ thể trẻ không có enzyme để phân hủy và hấp thụ được nitrit (sản phẩm của quá trình bảo quản thịt bằng muối diêm tiêu). Chúng sẽ chuyển hóa hemoglobin (có chức năng vận chuyển oxi) thành methemoglobin (không vận chuyển oxi)
Điểm trừ của cuốn sách là cách trình bày sách và ảnh bìa hơi chán (chắc do đã xuất bản lâu rồi). Tuy nhiên với nội dung có một không hai như thế thì có thể bỏ qua những chi tiết nhỏ nhặt này.
5
283844
2016-06-18 15:07:17
--------------------------
303618
4088
Một cuốn sách thú vị về việc lí giải các bí ẩn của việc bếp núc thông qua các quy tắc sinh hóa học. Nghe thì có vẻ hàn lâm và khó hiểu nhưng với sự tối giản nhất có thể các lời giải thích mang tính học thuật của tác giả, chúng ta đều có thể hình dung tương đối dễ dàng các lí giải này, và có thể áp dụng chúng trong những món ăn gần gũi hàng ngày. Với những ai có tâm hồn ăn uống thì đây quả là một tác phẩm không nên bỏ qua.
4
31303
2015-09-15 22:06:36
--------------------------
288495
4088
Thoạt đầu nhìn cái bìa thiệt là lạ mà lại do ybook dịch, vì sách ybook đa số là sách y học thường thức nên thấy lạ mua ngay mà mình cũng chưa coi giới thiệu gì vì mình nghĩ chắc sẽ rất thú vị. Nhận sách và đọc thì thấy thú vị về sách ẩm thực do giáo sư Hervé This viết đề cập đến các kiến thức khoa học trong quá trình chế biến thức ăn. Chắc là sẽ đọc và nghiền ngẩm đây, mình rất thích mấy sách của ybook hy vọng tương lai có nhiều bản dịch thú vị như thế này nữa.
5
730322
2015-09-03 16:54:57
--------------------------
287116
4088
Nếu bạn thích nấu ăn, bạn muốn nấu ăn ngon hay đơn giản là tóm thóp được vài 'chiêu' nấu ăn ngon của các đầu bếp chuyên nghiệp mà bạn vẫn chưa biết nên làm thế nào thì bạn này tóm ngay lấy người bạn này. Mặc dù là kiến thức đã được phát hành năm 1993 của Pháp nhưng mình thấy nó vẫn hoàn toàn mới cho đến tận 22 năm lịch sử của nó đối với những đứa amatuer mà thích nấu ăn ngon như mình. Đã áp dụng thử kĩ thuật đánh bông lòng trắng trứng, tuy nhiên sách vẫn chưa mô tả kĩ về trọng lượng, thời gian và mức độ đánh nên chỉ đơn thuần là áp dụng thế thôi. Mình thích thử làm socola và muốn tham khảo kĩ thuật trong đây mà cuốn sách này không có nên hơi buồn. Mong trong tương lai sẽ có thêm nhiều cuốn sách nấu ăn khoa học và bổ ích như thế này nữa ở Việt Nam để ngày càng có nhiều người được thưởng thức món ngon hơn.
4
609164
2015-09-02 11:59:26
--------------------------
271672
4088
Cuốn sách này không phải là một loạt các công thức nấu ăn từng chi tiết. Cuốn sách đơn giản là nói về những vấn đề liên quan tới các chế biến đồ ăn theo góc nhìn khoa học. Và nhận xét của mình thì đáng đọc nếu bạn vốn yêu thích về ẩm thực. Dù trong đây nói nhiều về ẩm thực phương Tây hơn, nhưng đọc qua có thể nhận thấy cách nấu ăn của mình vẫn chưa thực "sành điệu" cho lắm. Những bí kíp nho nhỏ nhưng cũng vô cùng hữu ích mà mình nghĩ hoàn toàn có thể áp dụng cho việc bếp núc sau này được.
5
723697
2015-08-19 12:35:57
--------------------------
244261
4088
Cảm nhận của mình với quyển "Bí Ẩn Của Cái Chảo - Ẩm Thực Phân Tử" này là một cuốn sách thật sự thú vị nó đưa ra cho chúng ta một cái nhìn rất khoa học về ẩm thực, nó giải thích cho chúng ta hiểu được các món ăn đã được tạo nên như thế nào, tại sao lại có những màu sắc mùi vị đó từ đó giúp chúng ta hiểu được nguyên lý và có thể tự vận dụng với bản thân, mà cách dẫn chuyện đề cập của sách cũng rất lôi cuốn và thú vị không gây nhàm chán chút gì. Nói  chung mình thấy đây là một quyển sách ẩm thực rất thú vị  
5
153008
2015-07-28 09:03:27
--------------------------
215218
4088
Bị thu hút bởi tựa đề của quyển sách, mình đã mua về và đọc thử. Cuốn sách mang lại một góc độ tiếp cận hoàn toàn mới về cách món ăn - góc độ khoa học của việc chế biến các món ăn. Đây không phải là một quyển sách dạy nấu ăn với các công thức và cách chế biến, nó hoàn toàn thể hiện mặt khoa học của việc giải thích làm thế nào mà màu sắc, mùi vị của món ăn thông dụng được tạo nên. Một góc nhìn mới về ẩm thực mà một người đam mê ẩm thực không thể bỏ qua.
5
183662
2015-06-26 00:43:57
--------------------------
211506
4088
Những cách thức nấu nướng, chế biến món ăn hàng ngày vô cùng gần gũi với mọi người, nhưng chắc không phải ai cũng biết vì sao chúng ta lại làm như vậy! Qua cuốn sách "Bí ẩn của cái chảo", chúng ta có thể thấy được sự thú vị và hấp dẫn của việc nấu nướng dưới cái nhìn của khoa học, liên hệ và khám phá thêm 1 cách vô cùng sống động giữ thực tế và kiến thức lý-hoá-sinh chúng ta đã học qua. Sách đề cập đến nhiều cách thức chế biến thực phẩm và còn cung cấp những mẹo vặt để nấu ngon hay bảo quản được lâu hơn, tất cả đều nhờ những hiểu biết về khoa học!
5
24396
2015-06-20 18:05:01
--------------------------
