352422
9864
Tình yêu là một gia vị không thể thiếu trong cuộc sống, và "Nghe socola kể chuyện tình" thật sự dã vẽ nên một bức tranh khá hoàn thiện về tình yêu. Ngọt ngào, thích thú với socola trắng, với những ngây thơ, đáng yêu của tình yêu tuổi mười chín đôi mươi, rồi lại chát đắng với socola đen, với những cung bậc rất thật của tình yêu trưởng thành. Cao Bảo Vy đã thật sự nhẹ nhàng dắt tay dẫn người đọc bước qua từng cung bậc cảm xúc của tình yêu, nồng nà có, đắng cay có, ghen tuông cũng có... Không hối hận khi mua cuốn sách này, bời quả thật nó rất tuyệt. Tuy nhiên một điểm trừ lớn là chất lượng giấy xầu, khiến người đọc giảm bớt thích thú khi xem sách
3
570187
2015-12-14 14:16:32
--------------------------
160909
9864
Trong khi nhiều tác giả Việt gần đây hay viết truyện tình yêu đọc thấy rất Tàu thì mình bắt gặp truyện của Cao Bảo Vy: Mhững câu chuyện tình yêu rất Việt trong những khung cảnh cũng rất Việt. Nhân vật của Vy đậm nét người thành thị, có lẽ như người ta nói rằng tác giả thường viết về những thứ xung quanh mình và mình tin rằng lối viết của Bảo Vy rất giống với con người đời thường của cô. Giọng văn rất nhẹ nhàng và trẻ trung. Nhiều mẫu truyện mình đã đọc qua trên mạng và thấy rất ấn tượng.
4
116202
2015-02-26 01:05:23
--------------------------
79304
9864
Từng chủ đề như dắt ta đi qua từng câu chuyện khác nhau, mà cảm giác như lại thấy ta ở đâu đó qua từng nhân vật , đã được Bảo Vy khắc họa. Tôi cũng vô tình biết đến truyện này qua một vài người bạn giới thiệu. Cảm giác ban đầu của tôi là trang bìa khá ấn tượng, bắt mắt, khiến tôi phải mua lấy cho bằng được. Tuy nhiên cũng có một vài mẫu truyện có nội dung na ná một số bài mà tôi đã từng đọc. Mặc dù vậy, nhưng tôi vẫn rất thích tác phẩm này .
4
57984
2013-06-06 15:34:54
--------------------------
71690
9864
Đúng như tựa đề của quyển sách. Từng truyện ngắn là từng vị khác nhau của sô cô la mà ta có thể cảm nhận được. Đôi khi đó là thanh sô cô la ngọt dịu, thanh sô cô la thanh tao với các hạt thêm vào hay đơn giản là vị đắng đặc trưng của sô cô la hoặc thanh sô cô la ấy có trộn thêm vị ớt cay xè…
Với tôi tình yêu đã không còn tìm được đường vào trái tim. Khi đọc những câu chuyện và cùng các nhân vật trải qua các trãi nghiệm, từ tình yêu dịu nhẹ, tinh khôi của tuổi học trò, đến những sôi nổi và mãnh liệt của tuổi đôi mươi hay đến sự tan vỡ và tái hợp của sự chín muồi  về suy nghĩ. Để rồi khi đóng sách lại, tôi nhận ra rằng tình yêu sẽ có muôn ngàn lối đi vào trái tim và đi vào lúc ta không ngờ nhất. Hãy tin vào điều ấy!
5
47557
2013-04-28 10:28:46
--------------------------
49874
9864
Một vài truyện trong cuốn sách này có nội dung khá quen thuộc, hơi giông giống một vài tác phẩm nào đó, chính điều này sẽ làm giảm đi phần nào sự hấp dẫn của cuốn sách. Nhưng những truyện còn lại thì khá hay và thú vị, mới lạ cũng như đem đến nhiều trải nghiệm độc đáo cho người đọc. Trong sách, những cung bậc cảm xúc, những giai đoạn của tình yêu, và muôn nghìn trạng thái khác nhau được ngắm nhìn và diễn tả dưới nhiều góc độ, nhiều lăng kính. Thế nên người đọc sẽ thấy hứng thú và đồng cảm với mỗi trang sách nhiều sắc màu và sinh động đến bất ngờ này. Cuốn sách hẳn sẽ là một món quà cho những đôi tình nhân, cũng như tất cả những ai đang có trong tay một hạnh phúc ngọt ngào.
4
20073
2012-12-10 16:41:38
--------------------------
43041
9864
Theo dõi từng trang sách trong "Nghe socola kể chuyện tình", tác giả như đang dẫn dắt chúng ta qua từng cung bậc của tình yêu từ khi mới bắt đầu yêu cho đến khi thành vợ, thành chồng được đánh dấu ở ba mốc : Tập yêu (socola trắng), đang yêu (socola sữa) và cuối cùng là hôn nhân (Socola đen). Bằng lối viết mộc mạc, giản dị, người đọc có thể dễ dàng nhận thấy mình đang ở giai đoạn nào trong "quá trình yêu". 
Qua những câu chuyện trong sách giúp chúng ta nhận thấy : Dù tình yêu chúng ta có gặp nhiều trắc trở, sóng gió hay tan vỡ... nhưng chúng ta nên chấp nhận như một phần của cuộc sống vì qua đó, chúng ta sẽ trưởng thành hơn trong những mối quan hệ sau này.
4
57175
2012-10-17 16:36:26
--------------------------
42967
9864
Bìa sách đẹp cách bố trí khá hấp dẫn người đọc, giá lại không quá đắt. Những câu chuyện của Bảo Vy khá thú vị bao hàm nhiều cung bậc cảm xúc. Tuy nhiên hạn chế trong tác phẩm của cô là nội dung thiếu sự lôi cuốn cảm xúc. Những thứ nhẹ nhàng bao giờ cũng để lại dư âm riêng của nó nhưng những câu chuyện này không đạt được điều đó. Nó làm người đọc cảm thấy hay nhưng không cảm thấy được rằng chính xác là ở hay ở điểm nào. Thế nhưng ở đây tôi đánh giá cao cảm xúc rất thật của những câu chuyện cô viết. Đó có lẽ là điểm đặc biệt đáng suy ngẫm để mang tôi đến quyển sách này. Hy vọng tác phẩm này của cô không làm tôi thất vọng!
3
55514
2012-10-16 17:12:14
--------------------------
