253817
10992
"24 cung bậc trái tim" với 24 câu chuyện, cùng với những cung bậc cảm xúc khác nhau trong những trái tim đang lớn. Cả tôi và người bạn thân đều cảm nhận được một điều gì đó khác trong lòng mình... Cảm xúc bất chợt nảy lên bên trong của những tâm hồn nhạy cảm, đa chiều. Suy nghĩ nông nổi, nhất thời mà đằng sau đó là vô vàn cảm xúc, chúng hỗn loạn, đan xen vào nhau... Rất thú vị với những cảm nhận mới mẻ, đong đầy tình yêu thương với bản thân và với mọi người.
5
526960
2015-08-04 19:50:01
--------------------------
140253
10992
Đằng sau bìa ngoài hấp dẫn là 228 trang sách. Đằng sau 228 trang sách là 24 bậc trái tim. Đằng sau 24 cung bậc trái tim là vô vàn yêu thương gửi gắm. Vâng, "24 cung bậc trái tim" quả thực là cuốn sách vô cùng hấp dẫn và thú vị. Thời thơ ấu, trong cái suy nghĩ còn non dại của mình thì yêu thương là 2 từ gì đó quá xa xỉ, chỉ giành cho tầng lớp văn nghệ sĩ, dỗi hơi đi viết thơ, truyện,... Và lớn lên vẫn vậy,.... Thế nhưng sau khi đọc "24 cung bậc trái tim", mình đã thực sự tìm được yêu thương trong cuộc sống. Yêu thương không chỉ là tình cảm nam nữ, đó còn là tình yêu thương giữa các thành viên trong gia đình, sự chia sẻ, quan tâm của những người bạn, sự đoàn kết, ân cần của những người đồng nghiệp,.. hay sự thương cảm, xót xa với những mảnh đời trong cuộc sống,... Tất cả tình cảm đó, luôn tiềm tàng trong mỗi chúng ta, nhưng nếu không biết giữ gìn và vun đắm, tình cảm đó sẽ phai dần theo năm tháng. Hãy trân trọng những yêu thương quanh mình, vì sống là để yêu thương.
5
414919
2014-12-10 17:06:09
--------------------------
