404103
11279
Cu Shin mê chơi, nghịch ngợm, hay gây rắc rối, tham ăn, rồi lại còn háo sắc nữa chứ. Ấy thế mà ai cũng phải yêu mến cu Shin, có lẽ nhờ cái sự lém lỉnh tinh ranh song đôi lúc lại quá đỗi ngây thơ của cu cậu. Mỗi một mẩu truyện, tức là một câu chuyện xảy ra quanh cuộc sống thường ngày của cu Shin đều mang tới những điều thú vị, những rắc rối nho nhỏ, những bất ngờ và đặc biệt là vô cùng hài hước. Tôi chỉ không thích lắm cái kiểu Shin hay vạch quần ra rồi lắc mông, hành động đó kể cả đối với trẻ con cũng là không hay, nhưng vì phong cách sáng tác của tác giả Yoshito Usui vốn bá đạo như thế nên cũng dễ hiểu. Chỉ là chúng ta cho con trẻ đọc thì nên cân nhắc tập nào phù hợp.
4
386342
2016-03-24 18:19:50
--------------------------
202335
11279
Với những tình huống dở khóc dở cười trong bộ truyện Shin-cậu bé bút chì làm tôi nhớ lại những kỉ niệm thời thơ ấu, cậu bé Shin tinh nghịch, hoạt bát, hiếu động luôn luôn muốn gây tiếng cười cho người khác, đôi khi lại an ủi những người có chuyện buồn một cách thầm lặng cho thấy trong cậu bé cũng có phần chững chạc rất nhiều. Nhìn bề ngoài quyển sách chứa đầy vè ngây thơ trong sáng của cậu bé Shinnosuke với những người bạn, với gia đình, thầy cô,...Ý nghĩa thật sự của quyển sách mà chú Yoshito Usui muốn gửi gắm cho mọi người biết là trẻ con hoạt bát, lạc quan, tuyệt vời như thế nào cho thấy sự yêu quý trẻ con của tác giả. Với nhiều cảnh vui nhộn, thú vị, có khi lại hồi hộp, lo lắng tạo nên những quyển truyện Shin của ngày hôm nay !
5
642752
2015-05-29 11:06:20
--------------------------
175035
11279
Nội dung của cuốn sách thì không cần nói các bạn cũng biết là rất hay, hóm hỉnh và vui tươi, nhiều lúc truyện làm mình cười vỡ bụng luôn. Vẫn là những câu chuyện xoay quanh đời sống của cậu bé shin, những trò nghịch phá đã khiến mọi người xung quanh luôn bực mình vì cậu nhưng lại góp phần giúp cho cuộc sống của mọi người thêm thú vị
mình rất thích truyện tuy lúc đầu mình không thích đọc vì hình vẽ xấu nhưng sau khi đứa em mình rủ mình đọc thì mình thấy chuyện hay khong kém gì truyện connan và doraemon
5
597338
2015-03-29 11:26:28
--------------------------
