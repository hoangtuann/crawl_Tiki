465018
11312
Một chân lí được khẳng định: không có gì là không thể. Một tác phẩm hay đã được làm phim thì chắc chắn tầm cỡ hay và tuyệt như thế nào. Thật ra mình xem phim trước mới đọc sách. Nhưng như thế không có nghĩa là mất đi sự hấp dẫn của sách. Phim đã hay nhưng sách còn tuyệt hơn rất nhiều. Tình yêu trong truyện vừa mới lạ vừa ngọt ngào. Thật sự mua sách ở Tiki rất nhiều cái lợi. Được tặng quà nhiều, rẻ hơn rất nhiều ngay cả khi bạn mua ở nhà sách.
5
1426772
2016-06-30 18:57:44
--------------------------
439328
11312
Đúng như tên gọi của nó. Xác ấm là câu chuyện về tình yêu ấm áp giữa người và zombie. Một anh chàng zombie với thân xác đang dần phân huỷ từng ngày nay lại đang cảm nhận những nhịp đập tình yêu bởi cô gái ấy - cô gái mà anh đã yêu ngay từ lần đầu gặp mặt. Tình yêu nảy nở giữa người và zombie, một kết thúc đầy bất ngờ, một cốt truyện hoàn hảo, một câu chuyện tình yêu lãng mạn, nồng nàn. Anh và cô, cả hai đã cùng nhau giải cứu thế giới khỏi sự diệt vong. 
5
98806
2016-05-31 08:35:30
--------------------------
412250
11312
Cốt truyện rất hay nhưng mạch truyện thì hơi chậm. Lời văn của tác giả cũng rấi hài hước, dí dỏm. Cuốn sách này đọc thấy rất nhẹ nhàng, ít chi tiết hồi hộp nhưng vẫn hấp dẫn người đọc. Câu chuyện kể về R - một xác sống và Julie, tình yêu của họ đúng như những gì tác giả viết: một tình yêu chữa lành cả thế giới, nó khiến cho R không còn là một xác sống nữa mà trở thành người sống, một lần nữa sống cuộc sống của con người. Đây đúng là một ý tưởng hay về truyện giả tưởng.
4
1165894
2016-04-07 13:09:55
--------------------------
406223
11312
Xác ấm khá là lôi cuốn, thay vì Twilight kể về ma cà rồng " chay " còn Xác Ấm thì kể về một chàng zombie không ăn thịt cô gái mà mình thích. Mình nghĩ anh chàng R dùng mọi cách để bảo vệ nữ chính cũng là do có được trí nhớ của người yêu cũ của cô, rồi dần dần tim anh đậm trở lại, rồi máu lại chảy trong cơ thể anh và đồng tử mắt của anh đã có thể co giản bình thường. Cũng chính nhờ tình yêu của 2 người mà những xác sống khác nhịp tim cũng quanh trở lại và họ cùng nhau chống lại lũ xương khô tàn bạo. Kết thúc truyện có hậu quá cơ, trước đến giờ toàn là thấy tình yêu ma cà rồng thôi nhưng đây là một sự đột phá _ tình yêu zombie.
5
408083
2016-03-27 22:15:36
--------------------------
337377
11312
Mình xem cả bản phim và truyện, và theo quan điểm của mình thì truyện hay hơn rất nhiều. Trong phim, để truyền tải hết cốt truyện thì các nhà làm phim chỉ gây ra những yếu tố tự sự và mạch tình cảm. Nhưng cuốn sách này còn hơn cả thế nữa. Không chỉ là tình yêu chữa lành thế giới, mà còn là những ý niệm mới mẻ về cuộc sống , về tình người. 
Mình đắn đo mãi mới mua cuốn này, vì sợ như phim sẽ rất thất vọng. Nhưng không ngờ, cuốn sách để laih ấn tượng vô cùng lớn trong mình. Đừng suy nghĩ gì nữa và cuốn sách này cần cho giá sách của bạn.
4
513868
2015-11-13 23:14:47
--------------------------
335429
11312
Một câu truyện thực sự dễ thương và cảm động. 

Mình biết đến Xác Ấm qua phim rồi mới tìm đến quyển truyện này. Tác phẩm hay hơn bộ phim rất nhiều, khi đọc truyện thì bạn sẽ có rất nhiều suy đoán về nguồn gốc của đại dịch chứ không chỉ đơn giản như bộ phim giải thích. 

Và câu "Một tình yêu chữa lành thế giới" thực sự khiến bản thân mình phải suy nghĩ và cũng rất cảm phục tình yêu của một xác ấm - R và một cô gái người thường Julie. Họ đã thực sự loại bỏ mọi rào cản để đến với nhau và để biến đổi thế giới này, biến nó trở thành một nơi tốt đẹp hơn. 

Bìa của Nhã Nam thì luôn luôn đẹp rồi. Chất lượng giấy cũng rất tốt, in rõ nét và không bị nhiều lỗi chính tả. Một quyển sách đáng đọc. 
5
131866
2015-11-11 01:02:57
--------------------------
291773
11312
Cuốn sách thu hút tôi từ những con chữ đầu tiên, với R - một Xác Sống, cái tên của anh chỉ có một chữ cái bởi khi trở thành Xác Sống anh đã mất hết kí ức, nhớ được chữ cái đầu tiên trong tên mình đã là một may mắn của anh rồi. Trong một lần đi săn, sau khi ăn não của Perry anh như trải qua những kí ức của Perry và đi đến quyết định không ăn Julie mà còn ra sức bảo vệ cô - bạn gái của Perry - một hành động trước nay anh chưa từng có. R đã thay Perry mang tình yêu đến cho Julie, một mối tình kì lạ nhưng cũng đầy cảm động. Có lẽ bạn nên đọc cuốn sách này để một lần nữa cảm nhận tình yêu từ nhiều khía cạnh khác nhau
5
578865
2015-09-06 18:14:03
--------------------------
271183
11312
Tôi xem phim Warm bodies vào dịp lễ tình nhân năm 2013. Một năm sau, tôi đọc quyển sách " Xác ấm ". Mối tình đáng yêu đến lạ kỳ giữa một cô gái và anh chàng thây ma một lần nữa lại tạo cho tôi ấn tượng vô cùng sâu sắc. Phim do giới hạn về thời lượng nên chỉ truyền tải một số nội dung chính, còn đi sâu và khai thác nội tâm nhân vật, chỉ có sách mới làm được một cách trọn vẹn. R, anh chàng zombie điển trai, gặp gỡ Julie trong một chuyến đi săn, để rồi yêu và tìm mọi cách bảo vệ nàng khỏi cộng đồng thây ma đầy nguy hiểm. Julie, trước sự chân thành và đáng yêu của R, cũng dần buông bỏ sự sợ hãi, chấp nhận anh, và cũng dần yêu anh. Tình yêu của hai người chính là phương thuốc kỳ diệu làm đập lại những trái tim tưởng chừng đã chết, và từ đó, sự sống một lần nữa bừng lên, những cơ thể thối rửa bắt đầu cảm thấy ấm áp!
4
109583
2015-08-18 22:36:20
--------------------------
248447
11312
Mình xem phim trước khi đọc sách, thấy phim cũng khá ấn tượ g bì từ trước đến nay mình luôn mặc định xác sống là những sinh vật khô g tình cảm, không nhân tính, sống chỉ để ăn,..... Nên khi đọc Xác Ấm khá ấn tượng vì nó rất mới lạ. Xác sống ở đây được tác giả khắc hoạ rất giống con người, tuy không hoàn chỉnh nhưng cũng có một phần nào đó nhân cách ẩn sau những con người đó. Quyển sách này là một chuyện tình nhẹ nhàng, lãng mạn giữa chàng xác sống và cô nàng Julie. Điều làm mình thích ở cuốn sách này là tuy là sách về thể loại tình cảm nhưng nó không sến sủa, ướt át như những chuyện khác mà rất chân thực, giàu chân lý "Tình yêu thay đổi thế giới". Nếu muốn đọc để giải trí nhẹ nhàng thì Xác Ấm là lựa chọn số một
4
415536
2015-07-31 09:58:51
--------------------------
240284
11312
Mình xem phim trước khi đọc quyển sách này, phim tươi sáng và trong sáng hơn nhưng không truyền tải được ý nghĩa như tác phẩm gốc. Câu chuyện tình giữa cô gái và chàng trai "xác ấm" vừa li kì hấp dẫn, vừa đáng yêu, vừa có khoảng lặng nhẹ nhàng lại vừa có lúc bi thương. Quyển sách có nhiều chi tiết hài hước nhưng vẫn bao trùm không khí hơi u buồn, đặc biệt là vào gần kết thúc, cái kết của bố cô gái có phần ảm đạm. Nhưng nhờ có những trắc trở đau buồn đó mà chuyện tình trong tác phẩm lấp lánh ý nghĩa cuộc sống và tình yêu hơn, gửi đến thông điệp về sự chữa lành thế giới mạnh mẽ hơn đến người đọc. Hài hước ở đây chỉ là yếu tố phụ. Nếu chọn đọc quyển sách này để giải trí - như khi xem bộ phim - thì hoàn toàn sai lầm, vì quyển sách phần nhiều nghiêng về tâm lí. 
4
120276
2015-07-24 14:46:57
--------------------------
236749
11312
Đây là câu chuyện cổ tích bất ngờ giữa ngày tận thế, là phép màu nhiệm trong thế giới tàn lụi và đổ vỡ. Một cô nàng Julie xinh đẹp, căng tràn sức sống, luôn mơ ước ở trong một "ngôi nhà " máy bay để vươn tới sự tự do bên ngoài bức tường thành bê tông. Một chàng thây ma R lang thang trong những ký ức mờ nhạt, tồn tại khác thường và kỳ quặc giữa đồng loại, thích một mình nghe nhạc trong chiếc 747.  Nếu bạn đã từng xem bộ phim Warm bodies thì nhất định sẽ không thể bỏ qua quyển sách này. Bởi cách kể ngôi thứ nhất, vừa là R vừa là Perry, giúp ta hiểu rõ hơn về suy nghĩ, cảm xúc của các nhân vật.Quả thật truyện khá khó hiểu với nhiều ẩn dụ và triết lý, song chính vì thế mà nó đáng được đọc và suy ngẫm. Có lẽ chúng ta đã, đang và muốn sống tiếp vì ta còn có những tình cảm trong trái tim, niềm hi vọng nơi khóe mắt và đặc biệt là những ký ức được ghi chép để nhớ và lưu giữ...
4
271828
2015-07-21 23:20:41
--------------------------
231304
11312
Bạn nhàm chán với những mô-típ chuyện tình cảm đã quá đổi quen thuộc? Hãy đổi gió ngay với Xác ấm (Warm Bodies). Xác sống và Người? Bạn nghĩ là ngớ ngẩn cho một câu chuyện tình yêu? Không đâu, những cảm xúc giữa hai "loài" khác nhau không có nghĩa là nó không chân thật, không lãng mạn. Cùng nhau trải qua hoạn nạn để dẫn đến một sự thay đổi to lớn, chuyện tình giữa anh chàng xác sống R và cô nàng Julie rất nhẹ nhàng và nhiều cung bậc. Cầm sách lên và cảm nhận những điều mới mẻ đi nào
4
457941
2015-07-17 23:15:41
--------------------------
230951
11312
Một đề tại không mới, nhưng cách xây dựng lại hoàn toán mới, lạ và cực kì độc đáo. Truyện kể về một thời kì khủng hoảng của nhân loại với bệnh dịch sản sinh ra Zombie. Hai chiến tuyến đối đầu. Với những chi tiết diễn biến của truyện, 2 nhân vật chính gặp nhau. Tình yêu đã cứu dỗi và mang sự sống lại. Tình yêu là liều thuốc tốt nhất, là cách hữu hiệu nhất để chữa lành bách bệnh!
Một tác phẩm pha lẫn cả kinh dị với lãng mạn, nhưng mọi thứ ở mức đủ để mạng lại những cung bậc cảm xúc thật trong lòng người đọc.
Enjoy!
5
334680
2015-07-17 20:39:10
--------------------------
220043
11312
Xác ấm quả thật là câu chuyện của Romeo và Juliet thời hiện đại, điều đó thể hiện rõ qua từ cái tên của hai hân vật chính: R và Julie cho đến nội dung. Rất ấn tượng với R, một xác sống, nhưng ấm. Biết suy nghĩ, cảm nhận như một con người, và mang trong mình niềm khao khát mãnh liệt được trở lại làm người, không ngừng chứng minh điều đó cho cô gái mình yêu. Julie, bằng cái nhìn đầy tin tưởng, hy vọng, đồng thời cho R thấy được điều dường như không thể của một con người với một xác sống luôn ở bên mình, mở ra một tương lai tươi sáng cho bao xác sống khác. "Một tình yêu chữa lành thế giới", rất ý nghĩa. Tuy nhiên, tình yêu của họ chưa đủ thuyết phục mình. dù sao cũng rất hài lòng với tác phẩm này.
5
372593
2015-07-02 10:39:09
--------------------------
211431
11312
Mình phải công nhận đây là quyển sách mà mình đọc trong vòng một ngày. Câu truyện xoay quanh một chàng zombie tên là R và tình yêu dành cho cô nàng Julie. Phải công nhận nhà văn Isaac Marion rất hay khi vẽ nên một thế giới về zombie khác xa hoàn toàn so với những bộ phim về thây ma trên phim ảnh. Ở đó, mặc dù những zombie vẫn ăn thịt người nhưng thú vị ở chỗ là những zombie  lại biết nói , biết suy nghĩ và trên hết là khao khát được nhận tình yêu thương. Và tình yêu của chàng zombie R và cô nàng Julie đã chứng minh cho điều đó. Tuy nhiên, điều khiến mình khó hiểu nhất là khi R ăn não của bạn trai Julie thì R bắt đầu yêu Julie, vậy nếu như R không ăn não bạn trai của Julie thì liệu R có yêu Julie được hay không. Bỏ qua chi tiết đó thì câu chuyện vẫn rất là hay
Một điểm cộng nữa là bìa sách vừa có phần u ám mà cũng vừa toát lên được ý nghĩa của câu chuyện .
4
300493
2015-06-20 16:15:44
--------------------------
207899
11312
Mình biết đến cuốn sách này trước hết là vì phim Warm bodies. Câu chuyện xoay quanh anh chàng R, từ một zombie đã trở về lại hình dạng con người nhờ tình yêu với cô nàng Julie - một cô gái mạnh mẽ từng được cử đi để chống lại những zombie như anh nhưng rồi lại chính cô nàng được anh chàng R giúp đỡ. Mỗi lần đọc cuốn sách này mình lại có một cảm xúc, suy nghĩ khác nhau. Lần đầu tiên xem xong mình đã nghĩ "Tại sao một zombie mà còn muốn yêu và được yêu, trong khi mình lại trốn chạy điều đó?". Và cả những cảm xúc của anh chàng R khi bị thất tình, khi được tin tưởng, khi tìm lại được tình yêu... đều được miêu tả rất thật. Nói chung đây là một cuốn sách nhẹ nhàng, nhiều ý nghĩa
4
507583
2015-06-13 14:28:51
--------------------------
183129
11312
Truyện không chỉ xoay quanh vấn đề tranh giành, giết chóc hay xác sống có thể trở lại thành người- biết cảm nhận và có tình yêu. Nó còn sáng lên một triết lí: tình yêu thương con người khiến tan chảy mọi rào cản- triết lí chẳng bao giờ sai cả. Tình yêu len lỏi và tác động đến mọi thứ. Đặc biệt là tình yêu không phân biệt vẻ ngoài giai cấp, chủng tộc... Giống như hai nhân vật chính của chúng ta, họ có cùng cảm nhận trong tâm hồn. Tác giả viết rất lôi cuốn và cảm động, truyện không nhiều bất ngờ nhưng vẫn khiến người ta đọc miết đến cuối. Thực sự đây là tác phẩm đáng đọc.
4
475171
2015-04-15 15:14:59
--------------------------
175291
11312
"Xác ấm".. lúc đầu nếu ai chưa xem phim "warm body" thì chắc cũng không biết cuốn sách này đâu. Nhưng đối vối 1 fan loạt truyện viễn tưởng thì chắc chắn không thể không biết cuốn sách này. Tựa đề đã nói lên tất cả.. " xác ấm " với 1 câu chuyện về 1 thế giới khác có 1 căn bệnh giống như virus mà con con người cần phải tránh : thây ma... với nhân vật chính là 1 xác sống tên "Rrr.." :)) trong 1 tập thể zombie bị cách ly trong thành phố hoang tàn. Xuyên suốt là khoảng thời gian "sống" với 1 cô gái mà anh đã bị tiếng sét ái tình.... sau cùng là anh thoát khỏi cơ thể "tim không đập" và sống như 1 con người thực sự.
5
495447
2015-03-29 20:58:13
--------------------------
175243
11312
Đầu tiên, mình cũng biết tới tác phẩm nay qua phim như một số các bạn khác, nhưng khi đọc xong mới thấy có vài điểm khác biệt. Tuy thế, khi đọc truyện, lại có cảm giác sau lắng, có thể để cho trí tưởng tượng bay cao. Bìa sách rất đẹp, nội dung hay, ly kỳ, hấp dẫn. Dường như nhân vật R. với trái tim lấm áo của mình cùng cô nang Julie nữ chính mạnh mẽ, dễ thương đã khiến cho người đọc cảm thấy thích thú vô cùng, quả là một cuộc tình cảm động. Một tình yêu chữa lành thế giới !
5
594111
2015-03-29 19:26:33
--------------------------
165221
11312
Với đề tài Zombie mà tôi yêu thích từ lâu thì đây là tác phẩm tình cảm, lãng mạn có chút hài hước đem lại cảm giác độc đáo, có thể giải trí cho tôi trong những ngày nghỉ. 

R và Julie như Romeo và Juliet hiện đại, một tình yêu vượt qua giới hạn của khoảng cách thân phận lẫn định kiến trong thế giới loạn lạc. Không hiểu sao, tôi thích phần đầu câu chuyện hơn phần sau, có lẽ vì thích cuộc sống của một xác sống, bất thường nhưng lại được miêu tả vô cùng bình thường. Trẻ con cũng được đi học, người lớn có thể hẹn hò, có thể kết hôn hay ngoại tình,... Phần sau có chút u ám nhưng tình yêu của đôi trẻ đã làm "rã đông" thế giới.

Tóm lại, một câu chuyện tình yêu lãng mạn kiểu Mỹ, không quá lạ nhưng lại độc đáo. Nghe các bạn nhận xét phim hay, có lẽ một ngày nào đó tôi sẽ dành thời gian xem.
4
382812
2015-03-10 07:23:36
--------------------------
163263
11312
Không hiểu sao đối với tác phẩm này thì tôi vẫn thích xem phim hơn mặc dù đúng thật là truyện hay hơn nhiều, rõ ràng hơn, chi tiết hơn và cũng sống động hơn nữa. Bộ phim đã gây ấn tượng với tôi ngay từ lần đầu xem và từ đó tôi đã tìm đọc cuốn sách này. Một xác sống khác biệt so với những xác sống khác, R dù không tên tuổi, không ký ức, không mạch đập nhưng dường như anh vẫn còn đó tâm hồn của một con người, anh yêu mến Julie và ra sức bảo vệ cô ngay từ khi mới gặp nhau, một xác sống không thể nói nên lời nhưng qua những cử chỉ có thể giúp độc giả hiểu rõ điều mà anh muốn diễn đạt. 

Xuyên suốt cả tác phẩm là không gian đầy u tối của đấu tranh, của hận thù, của những ký ức đau buồn nên khi đọc tác phẩm này tôi cũng mang cảm giác u ám không kém, tuy nhiên tình yêu của R & Julie đã giúp truyện trở nên sáng sủa hơn. “Tình yêu chữa lành thế giới”, đây là ý nghĩa rất hay của truyện, một câu chuyện vừa mang yếu tố kinh dị nhưng cũng đầy sự yêu thương và cảm thông thật sự mang lại ấn tượng sâu sắc cho độc giả.
3
41370
2015-03-04 16:08:41
--------------------------
161358
11312
Điểm trừ thứ nhất là tên tác phẩm.Mình nghĩ giữ nguyên Warm Bodies sẽ hay hơn là dịch sang "Xác Ấm".
Mình xem phim trước,vì thích phim nên đã mua sách đọc.So với phim thì sách chi tiết hơn nhiều,đi sâu vào nội tâm R,cũng như trọn vẹn hơn ở chi tiết R đã từng có vợ và con.
Truyện có nhiều lỗi chính tả,mình còn bị dính 2 trang sách lại với nhau và phải lấy kéo cắt.
Nhưng tác giả có sự khai thác nội tâm nhân vật,cũng như xây dựng Julie tràn đầy sức sống.Mình chỉ ước tác giả nên làm rõ chi tiết về Perry : là linh hồn hay tâm trí còn sót lại và chỉ dẫn cho R.
3
477889
2015-02-27 16:06:43
--------------------------
160918
11312
Hồi mới thấy truyện ra mắt, mình đã không dám đọc vì sợ. Rồi thấy những hình ảnh của phim thì càng sợ hơn, hay đúng hơn là mình không thích những cái xác như thế. Nhưng Xác ấm đã chinh phục được mình ở tình cảm của R. Mình thật sự thích nhân vật này, anh là một người rât tình cảm và yêu cuộc sống, sống lãng mạn và sao nhỉ? Mình có cảm giác nếu được ở bên anh thì sẽ cảm thấy rất yên bình. Bên cạnh đó, việc miêu tả nội tâm nhân vật, diễn biến tâm lý của tác giả đã giúp mình hiểu hơn về R, về Julie, về Nora hay Perry,... để hiểu thêm rằng Trong thế giới này, ai cũng cần yêu và được yêu.
3
63376
2015-02-26 08:19:52
--------------------------
160456
11312
Mình biết đến "Xác ấm" qua bộ phim "Warm Bodies", có thể nói bộ phim cũng là lý do phần nào mình muốn mua quyển sách này. Sau khi đọc xong càng thấy phim hay và truyện tuyệt vời hơn nữa. Trong truyện tâm lý nhân vật được thể hiện rõ nét hơn đặc biệt là những nhân vật được coi là phụ trong phim như Nora, Perry. Mình đặc biệt thích những suy nghĩ của Perry thông qua nhân vật R. So với truyện thì mình thích kết phim hơn, tuy hơi vô lý nhưng mình thích một cái kết viên mãn hơn cho những gì hai nhân vật chính phải chịu đựng :)
5
83088
2015-02-24 13:30:09
--------------------------
150673
11312
Xem phim mình thật sự cảm động khi R-một xác sống phải vật lộn với bản thân để phát ra từng chữ " keep you safe" khi R ra sức bảo vệ J đang gặp nguy hiểm. Tuy nhiên trong phim và trong truyện cuộc đời hai nhân vật chính trước khi gặp nhau được diễn tả khác nhau nhiều còn về sau thì diễn biến tương tự. Nếu ai xem phim rồi thì sẽ nhận ra sự khác nhau này. Cũng là một chủ đề về xác sống nhưng ẩn chứa trong đó không chỉ là sự đấu tranh sinh tốn của loài người mà là một tình yêu có thể làm thay đổi cả thế giới.
5
324101
2015-01-17 12:58:23
--------------------------
149822
11312
Mình xem phim Warm Body trước khi đọc tiểu thuyết này. Mê mẩn anh R "mặt thộn" lúc nào cũng nói "save you" nên tiểu thuyết xuất bản nhất định là không thể bỏ qua. Một suy nghĩ hoàn toàn khác về anh chàng này. Tiểu thuyết miêu tả đầy đủ, chân thực suy nghĩ của R, những diễn biến tâm lí của anh chàng mà anh ấy không thể nói ra được. Vì anh ấy là một xác sống. Trước khi gặp Julie, R đã có một người vợ và 2 đứa con cũng là xác sống. Mình nhớ nhất câu nói của R khi Julie hỏi anh không thấy đau khi thấy vợ đi với người đàn ông (cũng là xác sống) hay sao "Muốn đau... nhưng ko đau". Xác sống không phải là không có tình cảm, đã từng có, nhưng không nhớ. Những người sống luôn e sợ, tìm cách tiêu diệt xác sống bằng vũ khí, súng đạn nhưng tình yêu thương, tình cảm con người mới là sức mạnh lơn nhất, tìm lại cảm xúc đã đánh mất.
5
507943
2015-01-14 21:10:02
--------------------------
147822
11312
Đây có lẽ là một trong những tình yêu siêu thực nhất mà tôi từng đọc đến. Từng câu từng chữ mà một xác sống kể ra làm tôi đọc ngấu nghiến không thôi. Những chi tiết được miêu tả chân thực đến nỗi tưởng chừng như tác giả đã trải qua những khoảnh khắc mà một xác sống đã trải qua. Một xác sống, nhưng thích lên xuống trên những chiếc thang cuốn trong sân bay bỏ hoang, thích nghe nhạc Sinatra và coi một chiếc máy bay ấm cúng là nhà. Và quan trong hơn nữa, anh đã biết yêu. Không còn gì làm tôi tâm đắc hơn câu nhận xét: "Một tình yêu chữa lành thế giới." Câu chuyện có lẽ không có thật, nhưng trí tưởng tượng siêu phàm của tác giả buộc chúng ta phải tin. Tin vào tình yêu, tình người, sự thương yêu đùm bọc và dám đấu tranh.
4
298061
2015-01-08 21:50:22
--------------------------
142010
11312
Mình thích quyển sách này vì sự nhẹ nhàng trong đó: R là một Xác Sống nhưng "thích lên lên xuống xuống trên những cầu thang cuốn ở sân bay bỏ hoang, thích nghe nhạc Sinatra trong chiếc 747 ấm cúng được anh gọi là nhà, thích sưu tập đồ kỷ niệm từ những tàn tích của nền văn minh". Những chi tiết đơn giản, nhưng gợi nên một mẫu người rất hấp dẫn đối với mình.
Và rồi tình yêu đến, không còn nhẹ nhàng nữa, đến lúc phải vùng lên đấu tranh chữa lành thế giới, vâng, dùng tình yêu để chữa lành thế giới. Nhẹ nhàng mà lại không nhẹ nhàng chút nào :)
4
66322
2014-12-17 13:38:56
--------------------------
122505
11312
Cuốn sách này rất phù hợp với những độc giả yêu thích dòng văn học kỳ ảo kết hợp với những yếu tố như lãng mạn, kinh dị. "Xác ấm" có độ dài vừa phải, ý tưởng khá độc đáo, nhân vật được xây dựng một cách chỉnh chu, tạo nên cảm giác thoải mái khi đọc. Mạch truyện đôi chỗ hơi chậm một chút nhưng nhìn chung không ảnh hưởng tới độ hấp dẫn của tác phẩm.  Tác giả dùng lời kể của R, một xác sống chưa đến quá trình phân hủy đang lang thang vật vờ với những kẻ đồng cảnh ngộ với mình. Qua con mắt của anh, thế giới của những "zomebie" quá khắc nghiệt và đầy khổ đau, những xác sống chẳng thể cười đùa, chẳng nhớ nổi tên mình và lúc nào cũng phải tìm thức ăn là máu. Nhưng bản năng sinh tồn man rợ đó cũng không thắng nổi những tình cảm rất con người bên trong R, chính điều đó đã khiến anh phải bảo vệ Julie bằng mọi giá, bất chấp nguy hiểm. Có thể nói, R không chỉ là một xác sống đáng sợ, mà thực sự còn là một con người với trái tim và tâm hồn ấm áp.
Truyện còn đan xen nhiều ký ức về bạn trai cũ của Julie, những ký ức buồn bã miên man, khiến câu chuyện càng thêm u tối. Nhưng vượt lên trên tất cả những yếu tố kinh dị rùng rợn, đây thực sự là một tác phẩm đậm chất nhân văn cho giới trẻ. Nó thường được so sánh với "Chạng vạng", nhưng mình nghĩ  "Xác ấm" vẫn hay hơn rất nhiều.
4
109067
2014-08-26 12:39:32
--------------------------
119963
11312
Tôi biết được truyện này aqua bộ phim Warm Bodies, một câu chuyện gây cảm động lòng người đọc, tôi không thể ngờ lại có thể đắm đuối trong từng trang sách như vậy với 1 xác sống, 1 mô típ tình yêu có vẻ quen thuộc nhưng lại thật đặc sắc, rất đáng để đọc. Câu chuyện được kể lại đủ đẹp để chẳng thể coi là truyện kinh dị, đủ bi thương để không đơn thuần xem như truyện lãng mạn, đủ hấp dẫn để khiến bạn mê mải đọc liền một mạch, và đủ chân thực để khiến bạn tin vào một tình yêu chữa lành thế giới.
5
385556
2014-08-08 13:53:05
--------------------------
115834
11312
"Xác ấm" có đầy đủ những yếu tố làm nên một cuốn tiểu thuyết thành công. Đọc "Xác ấm", ta có thể cảm nhận đầy đủ tính lãng mạn, gay cấn pha trộn thêm chút triết lý nhân sinh. Một xác sống thích nghe nhạc Sinatra trên chiếc 747 anh coi là nhà, một xác sống trót đem lòng yêu thương một cô gái là người sống. Hành trình kiếm tìm hạnh phúc của R đã vượt qua thứ goi là "giống loài" hay chủng tộc. Anh yêu Julie bởi vì anh muốn thế, và bởi vì anh muốn "sống", muốn cảm nhận rõ ràng nhất cơ thể mình. Một thế giới đã gần đi đến bờ vực của sự tàn lụi, lại bừng sáng bởi một tình yêu tưởng chừng như không thể. R và Julie đã khai quật lại những phần của thế giới bị chôn sâu, những phần mạnh mẽ và đẹp đẽ nhất. Tình yêu ấy đủ thực để khiến tôi tin vào, khiến tôi suy ngẫm.
5
113960
2014-07-02 10:57:43
--------------------------
113918
11312
Mình biết đến quyển sách này qua bộ phim cùng tên, phải nói là cả hai phiên bản này đều thú vị. Isaac Marion sáng tạo ra thế giới của thây ma - những kẻ tàn sát người sống và chết dần chết mòn cho tới khi chỉ còn lại những bộ xương khô chuyển động kẽo kẹt. Họ chưa chết hẳn, nhưng không còn khái niệm gì về cuộc sống nữa. Trong số đó có R, một chàng thây ma khác lạ, thích nghe nhạc Sinatra và lên xuống cầu thang ở sân bay hàng giờ liền. R và những người bạn tàn sát và ăn những người sống khác vì đó là bản năng của họ, không thể cưỡng lại. Nhưng khi gặp Julie, R lại nảy sinh cảm giác muốn bảo vệ cô. Và một tình yêu tưởng như không thể lại bắt đầu. R đã ừm, ăn não của bạn trai Julie – Perry, và từ đó lượm lặt những ký ức và cảm xúc của anh ta, dần dần học cách yêu Julie. Văn phong Isaac  Marion lạ, không cầu kỳ hoa mỹ, nhưng cách dùng từ rất chân thực và gợi tải, truyền tải những bài học và triết lý qua những suy nghĩ và khát khao đơn giản của một thây ma. Đây không đơn thuần là một tác phẩm lãng mạn, người sống này ngã xuống, thây ma kia đứng lên, đây cũng không đơn thuần là một tác phẩm kinh dị, có tình yêu tan biến, tình yêu khác tái sinh. Và một cái kết đã có hậu khép lại câu chuyện giả tưởng hấp dẫn này: thế giới đang tự chữa lành và hồi phục, bằng tình yêu thương. R không còn là thây ma, mà là một người sống, cơ thể anh đang được hồi sinh, và những người khác cũng vậy.  
Tất cả, đều bắt đầu từ một tình yêu.
4
198930
2014-06-06 20:28:26
--------------------------
113188
11312
Mình đến với phim "Xác ấm" trước rồi mới đến truyện, tất nhiên là do thời gian ra mắt. Đây là một câu chuyện cảm động và mang một phép nhiệm màu về tình yêu giữa người sống và xác sống - một thi thể đã chết nhưng vẫn hoạt động được, chuyên đi ăn thịt người. Nạn xác sống đã trở thành toàn cầu, và chỉ còn khoảng 20 nghìn người cầm cự sống sót ở Mỹ. Tưởng chừng như Trái đất đã đến bờ diệt vong nhưng tình yêu giữa R và Julie đã đánh thức phần con người trong mỗi xác sống, khiến họ sống lại, trở lại làm người. Đây là một sự phi lí trong logic, trong hiện thực, người chết không thể sống lại, nhưng lại là có lí trong thế giới viễn tưởng, trong thế giới đề cao tình yêu, tình yêu thương giữa người với người đánh gục mọi rào cản. Đó cũng chính là thông điệp tác giả muốn gửi đến bạn đọc. Về nội dung thì đây là một cuốn sách hay tuyệt. Nhưng có lẽ để khắc hoạ thành công thế giới của xác sống, mạch văn có vẻ rất rời rạc. Theo mình, cuốn sách này có một mạch kể khá rối, dù ý tưởng rất tuyệt, tác giả biết đi sâu vào phân tích tâm lí nhân vật, cũng như có những sáng tạo rất đặc sắc (xác sống không ngửi mùi gì ngoài mùi sự sống, cảm nhận bằng cả cơ thể, nói chuyện ngắt quãng vì không hoàn toàn linh hoạt như người sống...). Nhưng chuyện tình của hai nhân vật chính không hẳn thuyết phục mình. Vẫn có ít ưu tư trăn trở, hay hoài nghi, lo sợ. Dường như đây là một tình yêu rất dễ đến, rất dễ thành, dễ một cách vô lí. Những suy nghĩ của nhân vật cũng rối và lủng củng, không hoàn toàn mượt. Dù sao đây cũng là một cuốn sách đáng đọc, để biết trân trọng tình yêu và tình người. Chất lượng sách tốt, cách dịch ổn, phù hợp cho những ai thích đọc liền một mạch.
4
113650
2014-05-27 17:23:56
--------------------------
