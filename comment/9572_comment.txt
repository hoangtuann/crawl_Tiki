285941
9572
Tôi vốn quen với tác giả Hồ Anh Thái qua những tác phẩm mang tính châm biếm sâu cay những vấn đề nhức nhối trong xã hội của ông. Ở tác phẩm này Hồ Anh Thái mang người đọc đến những chân trời nơi mà trí tưởng tượng và hiện thực đang xen , hoà lẫn nhau một cách li kì, không thể đoán biết được những diễn biến tiếp theo là như thế nào, khiến người đọc không thể gấp quyển sách lại giữa chừng. Nhiều những lắng đọng sâu sắc mà tác giả để lại trong tác phẩm thông qua những tình tiết trớ trêu, cách đối nhân xử thế của con người và con người trong xã hội. văn Hồ Anh Thái thực sự không phải dễ đọc nhưng một khi đã đọc thì rất dễ nghiện !
4
24682
2015-09-01 12:09:05
--------------------------
172444
9572
Hơi khác với những gì chúng ta vẫn nghĩ về Hồ Anh Thái. Không hẳn là một Hồ Anh Thái châm biếm, mỉa mai trong SBC là Săn Bắt Chuột, không phải một niềm đau đớn, buồn tủi như trong những truyện ngắn viết về xứ sở Ấn Độ nhưng vẫn là một cái nhìn đầy chất thế sự, cuộc đời. Dấu về gió xóa là một câu truyện lấy bối cảnh là một quốc đảo đồng thời là nhà tù trá hình của biết bao nhân vật nghe tên ai cũng biết. Cuốn hút vô cùng trong một mạch truyện nhẹ nhàng nhưng đầy bất ngờ. Tích hợp trong một cuốn tiểu thuyết không chỉ những vấn đề mang tính quốc tế, sự giải phóng, những vấn đề chính trị mà cả những số phận con người.
4
473281
2015-03-23 23:32:54
--------------------------
94253
9572
Một cuốn tiểu thuyết hay, ý nghĩa đáng để chúng ta nghiền ngẫm.
Vẫn lối viết đậm chất giễu nhại, Hồ Anh Thái mang người đọc vào câu chuyện hư cấu về một nơi(Đảo Xanh) giam giữ những người nổi tiếng bị mất tích. Họ vẫn còn sống song trong tâm thức người đời, họ đã được công bố là mất tích hay đã chết. Họ cũng không thể quay trở về nữa bởi gió đã xóa đi hết dấu vết
. Cũng như những "đứa con" khác của mình, Hồ Anh Thái đã bôi xóa tên nhân vật, nhân vật không tên hay nói đúng hơn là cái-tên-không-phải-là-tên khiến tiểu thuyết càng thêm phần "gió xóa". Nhân vật chính là Anh (Giáo Sư) cùng những nhân vật không tên khác như: Chàng (Giám Đốc, Người Đứng), Nàng (Cô Chủ), Thằng Bé (Con Trai Cá Ông), cô Mắt Hai Màu, Giáo Sĩ, Hoàng Tử, Hoàng Thân, Nhà Vua... mang người đọc xoay mòng trong thế giới của họ bởi những câu chuyện tưởng như bịa nhưng mang nhiều ý nghĩa sâu xa, thâm thúy. Người đọc sẽ nhận ra "Dấu về gió xóa "đâu phải chỉ là chuyện của Đảo Xanh mà đó là những vẫn đề hằng ngày ta từng bắt gặp. 
4
100555
2013-09-12 01:06:46
--------------------------
