357372
126
Rất hài lòng về cuốn sách dày. Sách dạy nhưng nhẹ, giấy tốt, đủ bộ các câu chuyện luôn, cỡ chữ hơi nhỏ chút (nếu cỡ chữ to hơn có lẽ cuốn sách phải dầy 2000 trang mất) nhưng vẫn rất dễ đọc, độ sáng tối cúa giấy không làm người đọc bị nhức mắt. Cầm cuốn sách mà sướng âm ỉ :)
Mình vẫn thích cuốn có lớp nhung đỏ êm êm hơn, em mình có làm mình ganh tị mãi, nhưng cuốn này thế này là quá hài lòng rồi.
Mình vẫn canh me cuốn này, mà cứ hay bị hết hàng nên mãi bữa nay mới mua được. Vừa rẻ vừa đẹp nên vừa xuất hiện đã hết hàng! 
5
101570
2015-12-23 14:02:00
--------------------------
343894
126
Do cực kỳ bức xúc với việc các bộ sách về Sherlock Holmes ở Việt Nam có quá nhiều lỗi. Có bộ thì lỗi trong phiên âm, có bộ thì lỗi trong lược dịch, có bộ thì lỗi trong biên tập, và rất nhiều lỗi khác. Là một người đọc khó tính, tôi không chấp nhận với các lỗi đó, và thật khó chịu nếu đó lại là một tác phẩm kinh điển của thể loại trinh thám. Trong lúc chờ đợi một bản dịch tốt hơn, tình cờ tôi tìm thấy bản tiếng Anh này. Cực kỳ hài lòng về chất lượng của quyển sách. Một quyển duy nhât chứa tất cả 54 truyện ngắn và 6 truyền dài, kèm minh họa từ tạp chí Strand. Do chỉ là 1 quyển duy nhất, nên nó khá dày, gần 1500 trang. Bù lại, giấy rất mỏng nên không quá nặng. 
5
270700
2015-11-27 13:32:47
--------------------------
