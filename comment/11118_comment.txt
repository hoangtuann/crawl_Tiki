392557
11118
Lãnh đạo vốn là một nghề, một nghệ thuật và quản lý vốn là một kỹ năng quan trọng trong đó. Quyển sách này đã giúp cho tôi, một người lãnh đạo trẻ với nhiều tham vọng có cơ hội nhìn lại và không lặp lại sai lầm. Sai lầm của đa số nhà lãnh đạo bình thường và đặc biệt là người trẻ như tôi thường mắc phải là lao vào thứ 20/80 mà lẻ ra phải là 80/20.
Tôi chân thành khuyên các bạn trẻ khi đến với tác phẩm này, các bạn nên đọc thêm tác phẩm "Con người 80/20".

4
636922
2016-03-07 10:13:35
--------------------------
341356
11118
Tôi mua quyển này cùng với bộ 80/20, phải nói rằng đây là một bộ sách khá hoàn chỉnh để học hỏi và thực hiện ngay nếu bạn muốn cuộc sống của mình thoát khỏi cái vòng công việc luẩn quẩn ngày 8h từ xưa tới nay, tập trung vào 20% cuộc sống và công việc để đạt được 80% thành công, bạn có thể thấy được mục tiêu của cuốn sách ngay từ trang bìa. Nếu bạn muốn phá cách cuộc sống của mình thì tôi khuyên bạn hãy mua quyển này ngay!
Tôi khá hài lòng với cách đóng gói và giao hàng của Tiki. 
5
467203
2015-11-21 22:10:16
--------------------------
266446
11118
Tác giả lại viết một quyển sách nữa về quy tắc 80/20 chính là lý do khiến tôi phải đặt mua quyển sách ”quản lý 80/20” này trên Tiki. Và khi nhận được quyển sách này trong tay, tôi thấy quyển sách này bìa sách đẹp, các trang trình bày cũng đẹp, nội dung trong quyển sách này đề cập đến kỹ năng quản lý. Đã có rất nhiều sách viết về chủ đề kỹ năng quản lý nhưng hầu như đều nói đến các kỹ năng khá chung chung không đăt nặng tính hiệu quả của việc lựa chọn, và quyển sách này đã giải quyết được vấn đề đó. Nhìn chung, theo tôi đây thật sự là một quyển sách rất đáng để chúng ta mua về đọc. 
5
42985
2015-08-14 17:57:31
--------------------------
162372
11118
Chắc hẳn bạn tò mò 80/20 là gì, thế nào là 80/20, nó có gì quan trọng, bản thân mình cũng vậy, chính sự tò mò khiến mình mua cuốn sách này và đọc. Sách có phần khiến mình khó tập trung khi thống kê hàng loạt những con số, bạn đừng nản lòng đó chỉ là những ví dụ và dẫn chứng để chứng minh cho kết luận của tác giả ở ngay phía sau. Cuộc sống này được quyết định phần lớn bằng "thiểu số" chứ không phải "đa số" -20/80. Sách sẽ chỉ cho bạn nhận biết và phân tích đâu là - 20 "thiểu số" đâu là - 80 "đa số" việc còn lại chỉ cần tập trung nguồn lực vào thiểu số để nhận được kết quả tối đa.
5
501817
2015-03-02 10:39:10
--------------------------
