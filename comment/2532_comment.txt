394456
2532
Cuốn này phải nói em đặt hụt hơn 5 lần :v mãi đến hôm qua mới đặt được, và tiki giao hàng rất nhanh, hôm nay đã có hàng dù là đặt giao hàng tiêu chuẩn :3
Lúc giao phát hiện trên cuốn truyện dính 1 đường màu trắng dài làm em sợ hết hồn, cũng may là lau nước thì ra :v Tom Sawyer là 1 tác phẩm vô cùng kinh điển đối với trẻ con, đọc mà thấy mình rất muốn quay về thời ấu thơ. Cuốn truyện đầy nét tinh nghịch, đáng yêu và tình yêu chớm nở 1 cách dại khờ của Beck với Tom. Và cũng rất hồi hộp khi xem đến khúc Tom và Huck xém bị Joe bắt. Đọc truyện cho ta thấy, cho dù trẻ con tinh nghịch đến đâu, nhưng nếu ta luôn yêu thương và quan tâm nó thì nó sẽ luôn là người tốt :)
4
84723
2016-03-10 13:34:37
--------------------------
255475
2532
Những cuộc phiêu lưu của Tom Sawyer là câu chuyện mà mình thích đọc từ khi còn đi học. Dù đã lên mạng đọc truyện này rồi nhưng giờ thấy truyện được chuyển thể sang truyện tranh cũng tò mò muốn đọc thử xem sao. Nhìn chung thì cuốn truyện này thể hiện nhân vật tương đối giống như mình từng tưởng tượng nhưng độ biểu cảm trong truyện thì chưa đạt lắm, mình cảm thấy nó chưa được mượt mà, cũng chưa thể hiện hết được các cảm xúc của nhân vật. Nếu là người chưa đọc tiểu thuyết của truyện thì có thể xem đây là một truyện hay nhưng với người đã đọc qua tiểu thuyết thì mình thấy biểu đạt của truyện tranh chưa chạm tới được cảm xúc mà tiểu thuyết truyền cho người đọc.
Ưu điểm của truyện là được in rất tốt, bìa thiết kế cũng khá đẹp.
Nhược điểm là biểu đạt chưa đạt lắm, khung thoại sắp xếp đôi khi có hơi lộn xộn một chút.
3
96853
2015-08-06 07:51:09
--------------------------
