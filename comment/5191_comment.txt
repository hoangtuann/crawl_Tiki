163095
5191
Cảm nhận về không gian, thời gian, học cách cư xử, nhận biết với những người sống xung quanh, học cách thể hiện tình cảm, tìm hiểu những sự vật ...là những bài học thông qua các câu chuyện mà quyển sách này muốn truyền tải đến người đọc và học. Vẫn theo cách mẹ kể cho con nghe, giảng giải để con hiểu ý nghĩa của bài thơ, câu chuyện con vừa được nghe. Ở độ tuổi này, ngôn ngữ của các bé đã tốt hơn nên các mẹ cũng sẽ dễ dàng hơn khi truyền đạt với con. Nội dung của cuốn sách vẫn tập trung vào việc nhằm nâng cao  khả năng ngôn ngữ, toán học, kiến thức tự nhiên, xã hội và học chơi một số trò chơi đơn giản. Các mẹ nên tranh thủ thời gian "vàng" này để luyện tập cùng bé nhé.
4
534227
2015-03-04 09:03:19
--------------------------
