528821
3625
Truyện vừa lôi cuốn, gây cấn, làm cho mấy bạn độc giả như mình rất thích. Ở tập 1 thì mình thấy phần mở đầu tuy hơi dài, ít hấp dẫn nhưng lại có phần mở đầu câu chuyện rất hay, đặc biệt. Phần 2 quá là tuyệt vời. Đọc xong tập 2 mới thấy ghét nhân vật Olivia Newton 
5
1384891
2017-02-20 22:41:39
--------------------------
447947
3625
Sau những diễn biến nhẹ nhàng của tập thì tập 2 có mạch truyện nhanh, hấp dẫn hơn nhiều. Theo mình đây là cuốn hay nhất, kịch tính và có rất nhiều tình huống thú vị. Sau khi đến Ai Cập, Jackson Rick và cô bé Ai Cập Maruk tìm ra các dấu vết của tấm bản đồ về Kilmore Cove. Bằng trí thông minh tài suy luận và may mắn cả 3 đã tìm ra nó tuy vậy kết quả tấm bản đồ lại rơi vào tay Olivia Newton... Tại sao Olivia lại đến được Ai Cập và truy lùng tấm bản đồ thì phải chờ xem tập 3 thôi
5
938445
2016-06-15 08:17:51
--------------------------
440093
3625
Nhứng tập truyện của Ulysses Moore bao giờ cũng hấp dẫn tôi 

Tôi đã đọc một mạch không ngừng nghỉ, mất một đêm và hôm sau thì mệt phờ. Đây là cuốn sách thích hợp cho tất cả những ai là fan của những cuộc phiêu lưu. Nội dung không có gi phải bàn, tiết tấu hợp lý với nhiều tình tiết hấp dẫn. Tôi sẽ rất hóng những tập truyện tiếp theo của tác giả này

Vể mặt trình bày: bìa sách khá đẹp, in rõ ràng, không bị lỗi. Sách cũng khá nhẹ nên dù hơi dày cũng không gây quá khó chịu khi phải cầm lâu. 

4
869075
2016-06-01 10:17:24
--------------------------
383566
3625
Giống như ở tập 1 mình đã nhận xét, cái hay ở truyện là khi bắt đầu thấy hơi chán thì lại thấy hay dần, và tập 2 này cũng vậy. Tuy nhiên Ở Tiệm Những Tấm Bản Đồ Bị Lãng Quên hay hơn hẳn Cánh Cửa Thời Gian. Hấp dẫn hơn, cuốn hút hơn, phiêu lưu hơn và đặc biệt là có nhiều yếu tố kỳ ảo hơn. Hay ở quá trình giải mã bí ẩn về căn phòng Không Tồn Tại để tìm ra tấm bản đồ. Kết thúc khá là kịch tính và bất ngờ, tạo tiền đề cho tập sau. Mình thích bộ này rồi nên hy vọng Alphabooks xuất bản trọn bộ.
4
386342
2016-02-20 21:54:51
--------------------------
301160
3625
Nối tiếp tập 1, sau khi mở được cánh cửa thời gian, khởi động con thuyền chỉ bằng cách nghĩ đến nơi muốn đến, 3 đứa trẻ đã phiêu lưu tới Ai Cập. Tuy nhiên sự cố xảy ra khiến 3 đứa trẻ lại bị tách nhau ra. Julia trở về lại lâu đài còn Jason và Rick vẫn ở lại Ai Cập khám phá. Trở lại lâu đài Julia cùng với người trông coi phải bảo vệ lâu đài thoát khỏi bàn tay đe dọa của tên lái xe cho Olivia. Còn mụ Olivia lại dùng một cánh cửa khác tới được vùng đất Ai Cập tranh đoạt bản đồ cùng Jason và Rick. Dưới sự giúp đỡ của người bạn Ai Cập, Jason và Rick giải mã bí ẩn để tìm bản đồ. Trong cuộc chạy đua tìm tấm bản đồ Jason và Rick đã thắng, tìm được bản đồ trước mụ. Nhưng phút cuối chúng lại bị Olivia cướp mất. Gay cấn, lôi cuốn, mong chờ các diễn biến trong tập kế tiếp.
5
43129
2015-09-14 15:03:36
--------------------------
