163996
4942
Với những ai từng đi qua chiến tranh,ký ức với họ đầy đau thuơng nhưng cũng thật hào hùng,và câu chuyện họ kể lại như một bản bi hùng ca.Còn với những ai chưa từng chứng kiến cái khốc liệt đó,chiến tranh lại được khắc họa theo lối chiêm nghiệm của mỗi người.Quyển sách là một tập hợp những người viết trẻ - viết về chiến tranh và thời kỳ oanh liệt đó - nên nó mang thần sắc của lớp người sau,suy nghĩ và quan sát những hiện thực của hậu chiến tranh.Những câu chuyện giữa đời thuờng của những người từng xông pha trận tuyến,những mưu sinh nhọc nhằn,những lo toan cuộc sống..vẫn tóat lên khí chất của người bộ đội cụ Hồ.Quyển sách,vì vậy mà có sức hút riêng của nó.
3
551735
2015-03-06 14:08:07
--------------------------
