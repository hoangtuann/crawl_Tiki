398193
8199
Cuốn sách này rất tốt cho những người đang theo học ngoại ngữ, đặc biệt là những người mới bắt đầu. Mỗi từ ngữ được giải thích khá chi tiết. Dịch nghĩa bằng tiếng Anh, sau đó mới sang tiếng Việt. Động từ, tính từ, danh từ cũng được trình bày rất cụ thể, chi tiết với những lưu ý khá cần thiết. Hơn nữa, mỗi từ mới đều có ví dụ minh họa cách dùng bằng cách đặt câu do đó rất dễ nhớ từ. Ngoài ra, việc sử dụng nhiều hình ảnh minh họa trực quan cũng làm cho người học dễ dàng hình dung và nhớ lâu hơn. 
5
896663
2016-03-15 21:30:54
--------------------------
289220
8199
Một cuốn từ điển tiếng anh tốt!Bìa cứng được bao bọc cẩn thận rất đẹp và chắc chắn!Giấy sáng và trắng bên trong nhìn rất rõ!Trình bày rất rõ ràng và kỹ lưỡng,nếu bạn nào muốn mua từ điển hãy mua cuốn này!Đặc biệt bên trong còn co những trang giấy màu láng bóng được trình bày rất đẹp!Tôi nghĩ đây là cuốn từ điển đầy đủ và bổ ích nhất!Các bạn nên mua nó!Tuy nhiên do số từ rất nhiều nên sách rất nặng,mình cầm muốn què tay luôn!thật sự rất nặng và tốt!!Mong các bạn có sự lựa chọn tốt!!
5
413637
2015-09-04 11:20:39
--------------------------
