457907
10411
Về giá trị cũng như nội dung của tác phẩm thì không có gì phải bàn cãi, vì "Bữa sáng ở Tiffany's" được công nhận là một trong các tác phẩm tiêu biểu của văn học thế giới hiện đại. Truman Capote bằng ngòi bút nhẹ nhàng nhưng châm biếm sâu cay đã tạo nên một nhân vật để đời không chỉ trong phạm vi văn học mà còn cả điện ảnh. Điều duy nhất làm tôi rất không hài lòng về cuốn sách này là việc dịch giả đã dịch hẳn tên các nhân vật theo nghĩa Việt. Tên riêng của các nhân vật như Holly Golightly chẳng hạn, tốt nhất nên được giữ đúng theo nguyên tác chứ không nên dịch ra. Đây là điều duy nhất tôi không đồng tình về cuốn sách này.
4
141226
2016-06-24 17:24:51
--------------------------
377098
10411
Mình nghe cụm từ này khá nhiều trong một seires phim mình thích khi xem phim mỹ. Thế nên khi phát hiện cuốn sách, mình đã mua ngay khi biết. Phải nói mình không ghét nhưng cũng chả thích sách được vào những cảnh phim và nhân vật. Không dài cho lắm, được lồng vào trong những tự truyện ngắn của tác giả nhưng chỉ đạt trang sách. Tuy vậy, mình thấy khá hài lòng khi có tác giả cuốn "Giết con chim nhạt" có mặt trong tập ảnh (tuy trắng đen) của sách. Qua thời gian đã cách đây rất lâu nhưng người đẹp Audrey Hepburn vẫn luôn được nhắc tới khi người ta nói đến sắc đẹp
5
383240
2016-02-01 17:15:04
--------------------------
291036
10411
Nhà văn Truman Capote có một lối viết rất nhẹ nhàng, sâu lắng, dù qua bao thời gian, nhân vật Holly Nhẹ Dạ vẫn mãi là một biểu tượng của văn học Mỹ thế kỷ 20. Ba truyện ngắn bao gồm trong quyển sách cũng rất hay và đặc sắc, nhất là truyện Ký ức giáng sinh. Các truyện này khiến độc giả có thể hiểu nhiều hơn về phong cách của Truman Capote. Sách được in rất đẹp, những trang minh hoạ từ phim cũng tô điểm cho câu chuyện. Mình chưa xem bộ phim chuyển thể nổi tiếng của Hollywood, nhưng hình ảnh Audrey Hepburn bới tóc và ngậm tẩu thuốc có thể xem là một hình ảnh trích từ phim ảnh kinh điển của thế kỉ 20
4
3243
2015-09-05 23:22:50
--------------------------
284812
10411
Holy Golightly—cô ấy lạ lùng, xinh đẹp, quyến rũ, thời trang...không ai có thể cưỡng lại sự quyễn rũ của cô ấy. Cô ấy cô đơn, luôn tỏ vẻ như cần một người đàn ông mãnh mẽ để dựa vào. Nhưng tâm hôn cô ấy tự do, sợ hãi khi bị cầm tù. Có ai để ý không ! Fred trong tiểu thuyết mình cảm giác như là một người đồng tính cuốn hút trước sự hào nhoàng của cô, yêu cô như một người bạn (có lẽ hollywood nhầm chăng). Dù fred có là trai thẳng thì chắc chắn cô ko bao đền với anh ấy. Tiểu thuyết như một biểu tượng của vẻ đẹp tự do của một ng con gái, sự xa hoa của chốn thành thị và khao khát tự do của những người trẻ thế hệ trước. Một tác phẩm hoàn hảo; không biết chừng nào việt nam thoát khỏi thời kì tiểu thuyết tản văn, văn chế nhảm mà đạt tới những đỉnh cao về nghệ thuật và tâm lý như thế này. 
5
323456
2015-08-31 11:54:28
--------------------------
273487
10411
Truman Capote là một tên tuổi đặc biệt trong làng văn Hoa Kì. Ông viết như không viết, nên sinh thời văn chương của ông bị qui chụp là rẻ tiền và đại chúng. Nhưng thời gian chứng minh tất cả, cuốn tuyển truyện của Truman Capote này đã đặt chân đến Việt Nam. Nếu bạn cũng như mình, vốn không thể không ngừng mơ về một giấc mơ Mỹ hoa lệ, bạn nên đọc cuốn truyện tiêu biểu đã định hình cho cả văn chương và điện ảnh Mĩ này. Cuốn truyện được lồng ghép rất khéo cùng các tranh ảnh và trang màu, không chỉ để thưởng thức mà còn để sưu tầm và nâng niu
5
292321
2015-08-21 01:33:40
--------------------------
218787
10411
Hình ảnh đọng lại trong mình sau khi gấp sách lại là cảnh Holly Nhẹ Dạ hoảng hốt kiếm tìm con mèo không tên của cô ta trong một khu ổ chuột dưới trời mưa tầm tã, như tìm kiếm một điều gì đó sâu thẳm trong trái tim níu giữ cô lại với cuộc sống mà đối với cô thật phù phiếm và vô chừng này. Và hình ảnh này đã được dựng lên thật sống động và thật đẹp trong tác phẩm điện ảnh được chuyển thể. Hoang dã, ngây thơ và duyên dáng là những gì ta có thể tìm thấy ở Holly vừa đáng thương vừa đáng trách.
4
57402
2015-06-30 21:29:05
--------------------------
182716
10411
Mình mua cuốn sách này  vì tình yêu với bộ phim, nhưng sau khi đọc tiểu thuyết gốc thì mình còn thích tác phẩm văn học hơn cả bộ phim nữa, tác phẩm được viết dưới cái nhìn của chính nhân vật nhà văn, tạo nên một sự khác biệt so với bộ phim, Cái kết cũng khác so với bộ phim, không phải là một happy ending đậm chất Hollywood mà là một cái kết mở đầy suy ngẫm. Ngoài ra cuốn sách này cũng bao gồm 3 tác phẩm khác khá nổi tiếng của Truman Capote nữa, sau khi đọc xong chúng ta sẽ có cái nhìn khá toàn diện về phong cách viết của ông, vừa nhẹ nhàng vừa ấn tượng. Rất thích cuốn sách này.
4
472173
2015-04-14 20:48:55
--------------------------
125458
10411
Bữa sáng ở Tiffany's là 1 tác phẩm nhẹ nhàng nhưng đầy sâu lắng. Holly Nhẹ Dạ là người phụ nữ thực tế nhưng đầy chân thành. Cũng như bao người phụ nữ khác thôi, khi yêu cũng yêu hết mình và vạn lần chung thủy.
Ngoài tác phẩm Bữa sáng ở Tiffany's, cuốn sách còn có 3 truyện ngắn ý nghĩa khác nữa, Nhà hoa, Cây đàn kim cương và Ký ức giáng sinh. Những truyện ngắn ấy cũng rất nhẹ nhàng nhưng lại ý nghĩa vô cùng. Có thể đọc xong cuốn sách ta có thể phần nào hình dung ra được lối hành văn của Truman Capote, với tôi, nó nhẹ nhàng, ý nghĩa, lắng đọng....
4
322130
2014-09-13 13:45:39
--------------------------
115057
10411
Do đã xem phim trước nên tôi thấy câu chuyện đầu tiên không mấy hấp dẫn. Đọc câu chuyện này tôi có cảm giác thiếu mất sự sinh động như trên màn ảnh. Thật may là trong sách có nhiều hình minh họa nên điều này giúp gợi lại trí nhớ của tôi về bộ phim đã xem. Hình được biên tập dễ theo dõi, có bố cục rõ ràng. Có lẽ tôi đã không nên xem phim trước rồi mới đọc kịch bản.
Các câu chuyện tiếp theo thì ngược lại, cuốn hút hơn vì tôi chưa từng xem phim. Đó là những câu chuyện rất đời thường và tôi cảm giác rằng dường như ai cũng có thể tìm thấy mình trong những câu chuyện như thế.
2
40797
2014-06-22 10:45:21
--------------------------
84461
10411
Nếu chọn hai từ để nhận xét về cuốn sách "Bữa sáng ở Tiffany's" thì mình sẽ chọn hai từ "ngắn và "sâu". Các mẩu truyện đều ngắn ở dung lượng, thực sự khá súc tích. Tác giả không diễn giải hay dẫn dắt dài dòng về những câu chuyện của nhân vật Holly mà cứ như chỉ nhấn lướt vào một số điểm nhất định nào đó trong tâm lí, hay ngoại cảnh tác động đến tâm lí nhân vật.  Nhưng chính điều này đã tạo ra điểm "sâu" cho tập truyện. Sâu sắc và sâu lắng, cuốn truyện thực sự dành cho những ai thích đào sâu vào ý nghĩa của cuộc sống và không thích những tiểu thuyết dài dòng mà nhẹ tênh!
5
52262
2013-06-30 17:29:14
--------------------------
81664
10411
Trong tuyệt tác đầy cám dỗ và nuối tiếc này, Truman Capote đã sáng tạo nên một cô gái mà tên cô đã đi vào thành ngữ Mỹ và phong cách cô là một phần của diện mạo văn học. 
Có điều gì đó ở Holly khiến cho những người yêu quý cô và cả bạn đọc cứ mải miết cuốn theo cô như quyện vào một cơn gió hoang thật ngọt ngào nhưng quá đỗi tự do; để rồi khi cơn gió ấy đã bay đến phương trời nào, ta vẫn không thôi vương vấn chút dư vị khó phai. 
Bộ phim Bữa sáng ở Tiffany's đã được Hollywood tạo thành một biểu tượng phù hoa, không chỉ thời trang mà còn một phong cách sống nổi bật nhất của truyền thông thế kỷ 20. Holly Golightly cũng là vai diễn để đời của ngôi sao Audrey Hepburn, đưa bà trở thành huyền thoại trong lẫn ngành diện ảnh và thời trang. 
Phim và truyện cho đến nay, trở thành một cặp biểu tượng thời trang và văn hóa kinh điển.
Câu chữ của Capote quá đẹp, từng hình ảnh toát lên một cái duyên dịu dàng man mác. Các nhân vật của ông cứ như bước ra từ một bài thơ và nằm lại trong tâm trí ta. Văn phong của tác phẩm đã khiến cho nhà văn hóa Norman Mailer gọi Capote là “nhà văn hoàn mỹ nhất của thời đại tôi,” và rằng ông sẽ “không thể thay đổi nổi hai từ của Bữa sáng ở Tiffany’s”. 
Tuyển tập này có thêm 3 trong số những truyện ngắn nổi tiếng nhất của Capote, "Nhà hoa", "Cây đàn guitar kim cương" và "Ký ức Giáng sinh" mà tờ Saturday Review gọi là "một trong những truyện ngắn cảm động nhất bằng tiếng Anh". 

5
3188
2013-06-18 00:14:33
--------------------------
76801
10411
"Bữa sáng ở Tiffany's" là cuốn sách đầu tiên mình được đọc của nhà văn Truman Capote. Ông có cách viết rất hay và lôi cuốn, với những câu từ giàu cảm xúc khắc họa được những góc cạnh sâu kín trong tâm hồn của con người. Mình đặc biệt thích cách tác giả vẽ nên chân dung của nhân vật Holly, một cô gái nhẹ dàng, với những suy nghĩ đôi khi quá ngây thơ và hồn nhiên, luôn chạy theo những ước mơ mà không biết rằng những ước mơ ấy nằm ngoài tầm với của bản thân mình. Tác giả không phán xét nhân vật này, không đưa ra những ý kiến mang tính chủ quan, mà chỉ đơn giản để mỗi độc giả có những cảm nhận của riêng mình.  Cuốn sách khép lại, đem đến không ít băn khoăn, tiếc nuối cho người đọc, nhưng đó là điều làm cuốn sách trở nên kinh điển, sống mãi với thời gian.
4
109067
2013-05-24 14:36:25
--------------------------
48764
10411
Bữa sáng ở Tiffany's có nội dung không gọi gì là mới, một tình yêu "trên trời rơi xuống" nhưng lại mãnh liệt rực cháy để đem lại một "happy ending." Tuy nhiên, điều này không khiến câu truyện trở thành nhàm chán. Ngược lại, chính đâu đó những câu nói nghe có vẻ sáo rỗng lại khiến hồn ta nhẹ bẫng và thanh thản giữa dòng đời xô đẩy của cái kỷ nguyên công nghệ mới này. Chợt nhận ra rằng đôi khi tìm về cái cũ là một việc mà ai cũng phải thực hành trong thế giới đảo điên hiện nay. Để thấy cái đơn điệu hóa ra lại mộc mạc và đáng yêu đến dường nào, như hai nhân vật trong truyện vậy. 

Bên cạnh đó, hình ảnh chú mèo không tên đã để lại một ấn tượng khó phai trong lòng tôi. Một con mèo tên là "mèo" trong một căn hộ không được trang trí cho ra hồn của một người chủ mang giấc mơ tìm được người đàn ông trong mơ của mình. Một buổi sáng với giai điệu Moon River. Vừa đọc, vừa tưởng tượng ra những khung cảnh ấy, đôi khi khiến lòng ta xúc động lạ thường.

Bất chợt nghĩ phải chi ở đây cũng có một tiệm Tiffany's, nơi mà ta sẽ tìm được sự bình yên buổi sớm mai...
4
25797
2012-12-03 06:19:03
--------------------------
38023
10411
Quả thật là Truman Capote rất điêu luyện trong cách hành văn. Ông không những gợi lên các hình ảnh hết sức chi tiết và sống động mà còn biết cách dẫn dắt người đọc theo tâm lý nhân vật. Tuy nhiên, đây không phải là tác phẩm dễ dàng để cảm nhận. Nếu chỉ đọc để giải trí thì bạn sẽ thấy cuốn sách này tẻ nhạt. Còn nếu bạn cảm được nó một cách từ từ thì bạn sẽ thấy nó vô cùng thu hút
2
13732
2012-08-29 14:36:59
--------------------------
31939
10411
Bữa sáng ở Tiffany's thật giống như một làn gió thoảng qua, nhanh chóng như một bữa sáng... nhưng thấm đẫm vị ngọt. Nếu ai đã xem bộ phim được chuyển thể, thì chắc chắn không thể bỏ qua cuốn sách này. Cuốn sách tuy không lãng mạng, không có một cái kết vui như trong phim, nhưng chắc hẳn rằng, nó chân thật hơn rất nhiều.

Câu chuyện hệt như trong đời thường, khiến độc giả cảm thấy được sự giản đơn, gần gũi, chân thật mà sâu lắng. Holly Nhẹ Dạ, một cô gái trong sáng, hồn nhiên, luôn chắc rằng, mọi thứ sẽ tốt đẹp. Sẽ không có điều gì tồi tệ đến với cuộc đời cô. Và Holly Nhẹ Dạ cứ mãi chạy theo những ước mơ của riêng cô, những ước mơ xa vời nhưng trong sáng trong mắt một cô gái hồn nhiên...

Cuốn sách đã cho tôi những phút giây thật tuyệt vời. Mong là bạn cũng sẽ cảm thấy điều đó... một làn gió nhẹ thoảng để hồn mình lắng xuống...
5
23953
2012-06-30 18:36:34
--------------------------
25875
10411
Ai đã từng rung động với hình ảnh nàng Audrey Hepburn kiêu kỳ với chiếc little black dress kinh điển của Givenchy trong "Breakfast at Tiffany's" ắt hẳn không thể bỏ qua cuốn sách này. Câu chuyện về nàng Holly Golightly trong phiên bản gốc của Truman Capote vẫn sẽ làm người xem có cùng những thang bậc cảm xúc như vậy: lòng trìu mến đối với sự ngây thơ trong trẻo của một cô gái quê chuyển sang nỗi bực tức đối với thói đỏng đảnh học đòi lối thành thị; sự thương cảm đối với những ước mơ trưởng giả không bao giờ có thật (điểm tâm tại Tiffany's, trong khi Tiffany's là cửa hàng bán trang sức) cùng những phút hân hoan hiếm hoi trước cái nền của nỗi buồn đọng lại trong cuộc hành trình theo đuổi những ước mơ ấy. Phiên bản gốc không kết thúc lãng mạn và có hậu như phim: Holly vẫn sang Brazil và gửi một bức thư (duy nhất) về cho chàng nhà văn người tình. Kết thúc rất đời thật với một Holly vẫn hồn nhiên chạy đuổi theo ước mơ xa xỉ của chính mình.


4
2312
2012-05-01 11:03:44
--------------------------
22481
10411
Một câu chuyện tuyệt vời, một sự trải nghiệm ngọt ngào có đôi phần chớp nhoáng, giống như việc thưởng thức một bữa sáng nhanh khi đang đi trên đường. Bạn có thể đã vội vàng "ăn" mất bữa sáng ấy mà lại quên thưởng thức - điều đó khiến bạn bỏ qua những hương vị đặc biệt của bữa sáng, và sẽ vô cùng đáng tiếc nếu như bạn không dành thời gian cho "Bữa sáng ở Tiffany's" - một câu chuyện có thể nói đã trở thành một một biểu tưởng phù hoa, không chỉ thời trang mà còn một phong cách sống nổi bật nhất của truyền thông thế kỷ 20. Nó nói về sự cám dỗ nơi thị thành - đương nhiên, và nó thách thức những con người tham vọng, thử nghĩ mà xem, ai có thể cưỡng lại sự cám dỗ ấy cơ chứ? "Bữa sáng ở Tiffany's" chắc chắn khiến bạn hài lòng, và tin tôi đi, bạn sẽ cảm thấy hối hận nếu như bạn không đón đọc cuốn sách tuyệt vời này.
4
26940
2012-03-23 20:45:04
--------------------------
