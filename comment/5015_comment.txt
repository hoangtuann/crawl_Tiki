334744
5015
Mình đã được tiếp xúc với thơ Trần Đăng Khoa từ hồi bé, và cho đến bây giờ đó vẫn là nét văn học đẹp đẽ nhất mình biết. Những vần thơ đôi khi rất ngây ngô, chỉ một vài từ nhưng cũng thật đẹp và đầy cảm xúc. Lúc bé thì chưa biết gì, đọc được ở đâu đó rồi cũng không nhớ, chỉ nhớ được cái cảm giác đọng lại trong mình lúc đấy. Lớn hơn chút thì được học về thơ Trần Đăng Khoa nhưng bây giờ tìm mua được tuyển tập này trên Tiki thật sự rất quí giá. 
Về hình thức thì bìa sách đẹp và chắc, chất giấy nhẹ, rất phù hợp với chất thơ :) có kèm một bookmark rất tiện. Nói chung là rất xứng đôi với nội dung chứa đựng bên trong.
5
662480
2015-11-09 23:12:55
--------------------------
169261
5015
Hạt gạo làng ta 
Có vị phù sa 
Của sông Kinh Thầy 
Có hương sen thơm 
Trong hồ nước đầy 
Có lời mẹ hát 
Ngọt bùi đắng cay... 
Ngày còn bé khi bập bẹ tập nói mẹ đã dạy tôi bài thơ này và sau đó là cô giáo dạy tôi những bì thơ khác của nhà thơ Trần Đăng Khoa. Những bái thơ ấy là tưng lời, từng lời hay lắng đọng trong tâm hồn trẻ con chúng tôi. Và sau này khi lớn lên những câu thơ quen thuộc ấy vẫn mãi theo tôi. Thơ Trần Đăng Khoa hay từ cách gieo vần, đến nội dung, câu chữ, luật bằng trắc. Cảnh vật trong thơ là những gì thực tế  thật tế, gần gũi và bình dị trong của tác giả là cái nhìn của nhà thi sĩ trẻ con về cuộc sống này, về thế giới xung quanh này.
5
557220
2015-03-17 21:40:01
--------------------------
