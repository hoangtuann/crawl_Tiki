201845
5433
Đây là một trong những kĩ năng cần được bời dưỡng cho các em học sinh hiện nay. Với xu thế phát triển như hiện nay, các em đang chịu sự ảnh hưởng rất nhiều từ phim ảnh, game bạo lực...dường như các em luôn có tâm thế "ăn miếng trả miếng" khi có bạn nào xúc phạm mình rồi hành động không suy nghĩ. Do đó, cuốn sách này rất cần thiết trong việc giáo dục các em kiểm soát cảm xúc, kiểm soát cơn giận của mình, ngoài ra còn rất có ích cho các em trong việc học môn GDCD thông qua các bài tập tình huống. Nội dung sách rõ ràng, hình ảnh minh họa đẹp.
5
455308
2015-05-28 13:12:37
--------------------------
