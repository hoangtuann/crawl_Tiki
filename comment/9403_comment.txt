517109
9403
Những câu chuyện bình dị của GS. Trần Văn Khê như tiếp thêm cho chúng ta động lực vượt qua mọi khó khăn trong cuộc sống , một con người không vì danh cũng chẳng màng đến lợi nguyện một lòng truyền bá âm nhạc Việt Nam ra toàn thế giới . Đây là minh chứng cho giấc mơ của chúng ta nếu chúng ta nguyện toàn tâm toàn ý kiên trì theo đuổi một lý tưởng thì chắc chắn một ngày sẽ thành công dù xuất phát điểm của chúng ta có thấp đến đâu nữa . Hãy đọc đi bạn sẽ không nuốt tiếc về nó đâu...
5
475999
2017-02-01 13:27:20
--------------------------
459863
9403
Giáo Sư Trần Văn Khê là một nhạc sỹ, nhà nghiên cứu âm nhạc nổi tiếng cả trong lẫn ngoài nước. Một người có công rất lớn trong việc quảng bá âm nhạc Việt Nam ra thế giới cũng như văn hóa Việt Nam đến với cộng đồng Quốc Tế. Cuốn sách là 12 Câu chuyện dưới giọng văn chia sẻ của Tiến Sỹ chứa đựng rất nhiều bài học quý báu bổ ích cho chúng ta. Đây thật sự là một trong những món quà quà rất ý nghĩa mà Tiến Sĩ đã để lại cho đời.Qua cuốn sách này, tôi thật sự hiểu thêm và khâm phục một con người vĩ địa, chuẩn mực và rất đáng tự hào!
4
310872
2016-06-26 16:29:00
--------------------------
394481
9403
Tôi bị cuốn hút bởi những trang sách đầu tiên, cố GS mô tả cuộc đời mình bằng những ngôn tư bình dị và lạc quan nhất có thể. Ở nhiều phân đoạn tôi buộc phải dừng lại, ngẫm suy mình đã cố gắng như thế nào, mình đạt được gì, mất gì sau những năm tháng trải nghiệm. Qua những ngôn từ của GS tôi hiểu được rằng khí chất của một con người bản lĩnh không chỉ được thể hiện qua sự nỗ lực hết mình vì những mục tiêu cao cả mà còn ở những khía cạnh nhỏ nhất của cuộc sống. xuyên suốt cả những chia sẻ của cố GS tôi khâm phục nhất câu chuyện "đứt dây đờn", có lẽ cả nghiệp cố gắng của tôi mãi mãi chẳng thể nào vươn tới tầm cao của cố GS.
5
167874
2016-03-10 14:32:52
--------------------------
206895
9403
"Những Câu Chuyện Từ Trái Tim" của Giáo sư Trần Văn Khê là 12 câu chuyện, 12 bài học ý nghĩa cho giới trẻ, đọc tới đâu, bạn sẽ thấm tới đó. Một cuốn sách mà theo mình, tất cả các bạn trẻ đều nên (hay đều phải) đọc.

Suốt cuộc đời bôn ba đó, ông vẫn giữ trong mình một tâm hồn rất trẻ, một cái đầu rất mở và ánh nhìn rất sáng. Ông yêu âm nhạc dân tộc Việt Nam, ông trân trí tà áo dài, cái khăn đống, từng câu ca dao, lời thơ, ý nhạc. Ông tin vào thai giáo, vào những triết lý giáo dục nhân văn, khai phóng. Ông tin niềm vui lớn nhất là khi kiến thức được chia sẻ: mọi người Việt Nam cần được học âm nhạc, mỹ thuật, nghệ thuật nói chung một cách bài bản - ông đã dành cả đời nghiên cứu âm nhạc, truyền dạy nó, và đến cuối đời vẫn không ngừng chia sẻ và truyền lại về văn hoá, âm nhạc.

Ông tin rằng người Việt Nam rất giỏi, nhưng ông cũng tin rằng, nghị lực làm nên tất cả, và chính nghị lực là cái mà giới trẻ Việt Nam cần nhất vào lúc này. Trong một thế giới toàn cầu hoá, mọi thứ ta đều có thể tiếp cận được, học được, làm được, nhưng ta cần thêm những người có ý chí, nghị lực theo đuổi những gì mình đam mê, mình tin tưởng - đó mới chính là những người sẽ đi đến cùng, đến bến bờ thành công.

Ông đã giành cả đời để mang Việt Nam ra thế giới, và kì vọng, các thế hệ đi sau sẽ không quên điều đó.

Riêng cho những ai từng học Pétrus Ký - Lê Hồng Phong, có một lần ông nói rằng: "Ông và các bậc cha, chú đi qua đã có rất nhiều người làm rạng danh ngôi trường này, đầu đội trời, chân đạp đất, xoay vầng lịch sử, làm nên đất nước như bây giờ và làm rạng danh Việt Nam trên thế giới. Bây giờ, đến lượt các cháu, mong các cháu tiếp nối truyền thống và làm tốt hơn nữa."
5
413058
2015-06-10 23:56:23
--------------------------
51687
9403
Cuốn tự truyện được viết bở một con người thuộc thế hệ trước, đã trải qua biết bao thăng trầm, biến cố trong cuộc đời. Nên một bài học trong đó đều rất có giá trị, đáng để người đọc hôm nay suy ngẫm. Tác giả đã đưa chúng ta trở lại những năm tháng xa xưa, khám phá cuộc sống và tâm hồn của những con người Việt Nam lúc bấy giờ. Đồng thời, tác giả trải lòng với người đọc, bằng những sẻ chia chân thực nhất về cuộc sống, kể cho chúng ta nghe những câu chuyện về cuộc sống của chính bản thân mình, những kinh nghiệm vượt lên khó khăn, vất vả và tuyệt vọng. Tác phẩm được viết bởi lời văn nhẹ nhàng, giản dị, chắc chắn sẽ tìm được sự đồng cảm của người đọc, đồng thời thắp lên tia hy vọng cho cuộc sống ngày hôm nay. 
4
20073
2012-12-22 10:48:47
--------------------------
