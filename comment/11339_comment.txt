457127
11339
Không chỉ hài lòng về sách của IPM như từ trước tới nay (cách bao kính kỹ lưỡng, bìa rời, khổ nhỏ...) mà còn hài lòng về việc vận chuyển của tiki nữa. Và về nội dung, tôi chắc chắn ai đọc cũng sẽ ngạc nhiên khi thấy cốt truyện giờ đã chuyển tới tận sáu tháng sau các sự kiện diễn ra ở tập trước, cụ thể cả đoàn giờ cùng chung tay làm một bộ phim cho lễ hội văn hóa của trường, một bộ phim mà phải nói rằng, việc tạo được ra nó chứa biết bao "sự thần kì". Cứ đọc đi rồi bạn sẽ hiểu.
5
1021968
2016-06-24 00:59:54
--------------------------
457058
11339
Mình thực lòng hối hận khi chưa nghĩ gì mà mua một lúc sáu tập của Suzumiya Haruhi, truyện quá chán, không hay chút nào hết luôn. Suzumiya Haruhi là một cô gái có tính cách kì lạ, nghe tưởng chừng hấp dẫn nhưng khi đọc theo giọng kể của main chính thì thôi mình bó tay, dở toàn tập, mấy đoạn đối thoại cũng nhảm nhảm. Cảm tưởng như đang đọc tiểu kịch trường ( Mình đọc tập nào cũng phải bỏ dở hết, ai muốn mua lại mình bán sáu tập cho, còn nguyên vẹn chưa kể có bọc plastic, còn mới nguyên xi.
3
936775
2016-06-23 23:17:43
--------------------------
444738
11339
Tiếp tục câu chuyện xoay quanh nhân vật chính của chúng ta là haruhi với những ý tưởng của cô bé tham gia lể hội trường,rồi cô bé gây ra những chuyện gì,những náo loạn gì cho ngôi trường cô đang học.Đó chính là sự kiện làm phim,những sự kiện xoay quanh cô luôn luôn vui nhộn và đầy bất ngờ như ý muốn cô bé bắn ra tia sáng thì ngay sau đó nhân vật chính của bộ phim do cô bé đạo diễn lập tức thành hiện thực,chút nữa là ảnh hưởng đến tính mạng của những người xung quanh.
4
826663
2016-06-08 21:55:10
--------------------------
437993
11339
Dù đã xem anime nhưng đọc Light novel vẫn có cảm giác riêng và không cảm thấy chán gì cả. Tuy nhiên anime bị xáo trộn nên khó hiểu, nhưng light novel theo mạch truyện nên đọc cảm thấy dễ hiểu ngày 
Nội dung thì cũng có thể hơi nhạt, chưa cao trào gì lắm, như gợn sóng nhẹ thôi, mấy tập sau sẽ có điều thú vị hơn. 
Mình đọc tập này cứ cảm thấy chán chường sao ấy, không mấy gì đặc sắc cho lắm, thôi thì trông đợi vào các tập sau. 
Tóm lại nhìn chung cũng được
5
612250
2016-05-29 08:30:27
--------------------------
369918
11339
Ở tập 2 này cô nàng quái chiêu Haruhi bày ra trò quay phim đúng là vui thật tội mỗi anh chàng Kyon lúc nào cũng dính rắc rối. Hình ảnh Mikuru thì siêu dễ thương. Tình tiết truyện thì vẫn làm người đọc đã đọc rồi thì khó mà dừng lại được. Về hình thức thì khỏi bàn, từ bìa, chất lượng giấy hay tranh minh họa trong truyện đều rất ổn, khổ bunko ban đầu thấy hơi lạ lẫm nhưng cầm mãi cũng quen tay lại thấy khổ nhỏ cũng khá là tiện. Bộ này chắc chắn mình sẽ theo từ đầu đến hết luôn. Bây giờ mình đã có đủ 5 tập đã phát hành rồi. Mong là sớm có tập mới 
5
905465
2016-01-16 19:15:26
--------------------------
355921
11339
Nói thật, từ lúc cầm tập một lên mình đã thấy đọc không ra gì. Nhân vật nữ chính thì độc quyền quá, còn nam chính thì lại chẳng ra đâu vào đâu cả, không có chứng kiến mà lại đăm đăm đi làm mấy việc mà Haruhi bảo. Nhưng rồi nghĩ lại thì series này nổi đình nổi đám, có lẽ tập hai sẽ hay hơn. Nhưng đến lúc mua tập hai rồi thì thất vọng toàn tập. Nội dung nhàm, phần hài hước lại không hay, các nhân vật vẫn lấc cấc y như tập một, mình cố lết đến cuối truyện chỉ vì sợ tiếc tiền thôi chứ thật ra mình chẳng còn hứng thú gì nữa. Mình cho năm sao là bốn sao cho dịch vụ chuyển hàng của Tiki và một sao cho nội dung truyện, nói thật chứ đáng lẽ ra là chẳng được nổi một sao.
5
502467
2015-12-20 22:52:44
--------------------------
335636
11339
Mình rất ít khi mua những quyển mà có nhiều tập vì sợ không đủ tiền và diễn biến có dài dòng  nhưng với bộ này thì không cần phải đắn đo! Phải nói là đây là bộ truyện nhiều tập đầu tiên mình mua! Mình đọc tập 1 ở ngoài và phải cấp tốc mua tiếp về vì không thể chờ đợi được nữa! Nếu bạn đang phân vân có nên mua không thì mình khuyên là nên mua! Truyện rất hay và xứng đáng với số tiền bạn bỏ ra! hy vọng tiki sẽ nhập tiếp các tập sau của truyện này !  
4
768112
2015-11-11 11:59:56
--------------------------
323480
11339
So với quyển một thì nhịp điệu quyển hai diễn ra sôi động hơn bởi những trò tinh quái của haruhi khi kéo cả đoàn sos làm phim gây ra nhiều tình tiết vui nhộn trong suốt tập truyện mà khiến người đọc không thể ngừng cười. Và cũng như quyển trước là khi đặt mua vẫn ko có bookmark chắc do lâu rồi nên ko còn nữa nên mỗi tội đọc phải nhớ trang hoặc nguyên một chương. Ngoài ra tiki bao bọc sách rất tốt và cẩn thận có điều giao hàng hơi trễ so vs mấy lần trước. Hi vọng mốt tiki sẽ giao hàng sớm hơn
5
815806
2015-10-18 21:41:08
--------------------------
318659
11339
Nếu mình ko nhầm thì đây là quyển tập 3 trong 11 tổng số quyển của bộ Light Novel Suzumiya Haruhi. Nhìn bề ngòai thì mình thích quyển này nhất do có ảnh bìa khá dễ thương và bắt mắt hơn các quyển khác. Do nhỏ hơn các quyển Light Novel nên độ dày của quyển này vẫn đảm bảo tuy câu chuyện viết hơi ngắn. Light novel này lâu rồi, tuy vậy mỗi tập lại cho chúng ta những trải nhiệm thú vị do các câu chuyện hấp dẫn xoay quanh các nhân vật. Mình vẫn sẽ tiếp tục dõi theo từng tập một cho tới hết.
4
789882
2015-10-06 17:03:30
--------------------------
310054
11339
Thấy light novel này được PR rầm rộ quá trời và nghe đồn truyện này hay còn được dựng thành anime nữa nên mình cũng mua đọc thử coi sao. Đọc tập 1 thấy không hấp dẫn mấy, còn cảm thấy có cái gì đó vô duyên lắm nhưng ngẫm nghĩ bộ gần chục tập, chắc chưa tới tập hay. Nhưng quả thật mình xin chia tay với bộ này sau khi lết xong tập 2. Theo mình mặc dù nội dung khá mới lạ nhưng nhân vật nữ chính cứ như bị hâm hâm sao ấy, còn chảnh nữa, có cảm tưởng như bạn này muốn làm cái rốn của vũ trụ ấy, cực ức chế với Suzumiya. Yếu tố hài hước cũng không có gì nổi trội, chắc mình nên xem anime thử coi thế nào chứ light novel mình nuốt không trôi rồi, có lẽ do không hợp gu. Vớt vát được phần hình thức với trang màu, quà tặng kèm, với lại khổ sách cũng nhỏ gọn.
3
42247
2015-09-19 11:52:40
--------------------------
308494
11339
Hơi tiếc khi mua quyển này một chút, vừa không có bookmark mà nội dung... chẳng đặc sắc. Được cái minh họa đẹp. Tập này thì đoàn trưởng của chúng ta nảy ra ý tưởng đóng phim và Haruhi thì vẫn chứng nào tật nấy, hành hạ những nhân vật khác theo ý mình như remote điều khiển quạt máy. Một đạo diễn như Haruhi thì ngoài đời không hiếm nhưng ít nhất thì truyện cũng nên biến hóa làm sao cho nó nhẹ nhàng chút. Dù sao cũng không nên bỏ sót bất kì tập truyện nào, ngộ nhỡ không nắm được tình tiết sẽ dễ bị bí. Ít nhất cũng có phần cải thiện ở các series sau.
4
534452
2015-09-18 18:45:23
--------------------------
292480
11339
thấy tập 2 trên tiki và rất háo hức muốn đặt mua.không ngoài tưởng tưởng. truyện khá hay và hấp dẫn.Đã xem anime và cuồng Suzumiya Haruhi,đọc light novel xong còn cuồng hơn nữa >~< truyện khá hay. tập này vẫn rất thú vị. Truyện được thiết kế nhỏ gọn màu đẹp đẽ. cứ hôm nào ngồi trên bus là mình lại lôi ra đọc. Con trai nhưng người ta nhìn thế nào thì cứ mặc kệ. chuyện kể về việc làm phim của Đoàn SOS do sáng kiến của Đoàn trưởng Haruhi. Các bạn nên mua ngay đi ạ. xin cảm ơn.
4
96879
2015-09-07 13:30:41
--------------------------
286235
11339
Các bạn mình nói rằng đây là một bộ light novel rất hay, tuyệt phẩm, vâng vâng.. thì mình đã mua thử
Mình khá thất vọng với tập 1, nhưng chưa từ bỏ, nên lội lên đây mua tập 2 về (một phần vì muốn sưu tầm cho đủ bộ ấy mà). Và mình có nhận xét...
NHẠT, NHẠT VÀ NHẠT! 
Mình thật không hiểu vì sao nhiều người lại thích bộ này đến như thế. Tranh minh họa bên trong hoàn toàn không vừa mắt mình (mặc dù màu khá ổn). Nội dung không hẳn là tệ nhưng nhân vật chính làm mình ngứa mắt. Vâng, Suzumiya Haruhi làm mình ngứa mắt lắm luôn đó! Nhiều người cho tính cách đó là đáng yêu, dễ thương, tsundere (ngoài lạnh trong nóng), mình chỉ thấy chảnh; Suzumiya xem mình như nữ hoàng, ra oai ngồi trên ghế đạo diễn làm phim, quát mắng, đày đọa cá nhân vật khác... 
Nói chung là mình ức chế nữ chính. Còn những tình tiết trong sách thì như mình đã nói, hết sức tẻ nhạt! 
Trong khi giá bìa là 54k đó...
Mình rất hối hận khi mua quyển sách này

2
112516
2015-09-01 15:33:52
--------------------------
276937
11339
Tập 2 của serie: Suzumiya Haruhi cũng được mình kỳ vọng như khi mình tình cờ đọc được tập 1. Với những ai là fan của thể loại lightnovel thì không thể không nhắc đến Suzumiya Haruhi. Tập 2 vẫn thú vị và buồn cười như tập 1m khiến mình cười đau cả bụng, các nhân vật vẫn giữ được nét cá tính và độc đáo của riêng mình.
Điểm cộng: Khổ sách bỏ túi, cực kỳ nhỏ gọn có thể dễ dàng mang theo khi đi xe bus, đây cũng là 1 nét văn hóa của người Nhật. Chất lượng giấy của IPM tốt.

4
195704
2015-08-24 15:14:21
--------------------------
275095
11339
Là tập 2 trong series Suzumiya Haruhi, câu chuyện lần này nói về việc làm phim của Đoàn SOS do sáng kiến của không ai khác ngoài Đoàn trưởng Haruhi. Những tình huống hài hước vẫn tiếp diễn, chủ yếu là việc Asahina Mikuru bị Haruhi hành hạ, bắt cosplay những trang phục đáng xấu hổ. Cốt truyện không có gì đặc biệt nhưng vẫn đủ làm người đọc thấy thích thú. Vẫn là kiểu sách nhỏ, gọn có thêm vài bức ảnh minh hoạ, đúng kiểu Light Novel, đặc biệt bìa trông rất bắt mắt. Nói chung là cuốn sách này không nên bỏ qua.
5
503185
2015-08-22 17:05:32
--------------------------
274583
11339
Mình rất thích tập 1 của Haruhi vì độ ảo, bất ngờ và hài hước của nó nên mình đã rất  kì vọng vào tập 2 này. Thế nhưng khi đọc tập 2 thì mình thực sự cảm thấy buồn ngủ, phải nói là tẻ nhạt hơn tập 1 rất nhiều, vẫn là những nhân vật với những cá tính không lẫn vào đâu được ấy nhưng đâu rồi những nút thắt mở đầy bất ngờ ở tập 1 :((
Về hình thức thì IPM vẫn làm tốt như ở tập 1, sách khổ bunko cằm nhỏ gọn rất là thích, giấy hình như là dày hơn tập 1 một chút cũng là một điểm cộng đối với mình.
3
148945
2015-08-22 05:03:21
--------------------------
267452
11339
Mình đang chờ những phần sau của cuốn sách xuất bản từng ngày từng giờ. Vô cùng lôi cuốn và thú vị là những từ mà mình dành cho cuốn sách này. Thích Suzumiya lắm lắm luôn. Cách tác giả xây dựng các nhân vật, kể cả nhân vật chính lẫn nhân vật phụ đều rất độc đáo và có màu sắc riêng biệt. Chờ mong những tác phẩm light novel tiếp theo của tác giả Nagaru Tanigawa.
Về hình thức, khổ sách nhìn rất xinh, cầm vừa tay bỏ vừa túi, hình minh họa cũng rất đẹp. IPM đúng là nhất luôn ấy!
5
209160
2015-08-15 15:37:37
--------------------------
262033
11339
Truyện phải nói là thể hiện trí tưởng tượng phong phú vô cùng của tác giả. Đọc về việc làm phim đến là bá đạo của Đoàn SOS làm mình cũng muốn chui vào tham gia chung. 
Cô nàng chúa, chị gái kẹo ngọt vượt thời gian, sinh vật ngoài vũ trụ không cảm xúc, siêu năng lực gia và anh chàng học sinh cấp ba bình thường của bình thường, ai cũng đều được xây dựng rất độc đáo, nổi bật và chiếm được tình cảm của độc giả.
Cách dẫn dắt câu chuyện với ngôi thứ nhất của tác giả phải nói là thuộc vào hàng cực kì điêu luyện. Tuy nhiên mình vẫn hi vọng những tập sau của series sẽ ngầu và chất hơn nữa.
 Chất lượng sách thì không có gì để phàn nàn, chữ và hình ảnh đều được in ấn rõ ràng và đẹp.
3
342345
2015-08-11 16:33:12
--------------------------
258496
11339
Mình đã rất kì kì vọng vào tập 2 một Suzumiya Haruhi chất và ngầu hơn nữa. Tuy nhiên, haizz .... có lẽ là chưa đến lúc chăng? Tuy vậy vẫn không thể phủ nhận được trong tập 2 này vẫn có những tình tiết hay, giá trị và cả ... khá buồn cười nữa.  Sự ngang tàn đến nỗi mà đã khiến mình phải thốt lên: Trời đất! Ai xông vào xiên nó chết đi cái! của Suzumiya Haruhi vẫn được bù đắp lại bằng những hành động bá đạo đấy chứ! Yahooo! Ủng hộ cho series Suzumiya Haruhi nào!
1
608340
2015-08-08 16:18:12
--------------------------
258175
11339
Tập 2 hấp dẫn hơn tập 1. Kyon thì suốt ngày bị Haruhi sai đi làm việc lặt vặt. Suốt ngày           mài đít ở cái văn phòng SOS bí ẩn để tìm những điều kì bí. Tội Mikuru nhất, luôn phải mặc những bội đồ mà ý tưởng của cô nàng quái dị kia nghĩ ra. Nhưng có lẽ vì thế mà cuộc sống học sinh của các nhân vật đều đầy ắp tiếng cười. Còn cái suy nghĩ đen tối của Kyon nữa chứ, cậu này chắc thích Mikuru dữ lắm và toàn nghĩ vớ vẩn. Hình như thực sự có một thế giới siêu năng lực đang tồn tại song song với không gian bình thường. Khá gay cấn.
5
139133
2015-08-08 11:45:49
--------------------------
248280
11339
Nếu tập 1 cho chúng ta thấy cái bướng, cái ngang của Haruhi và làm chúng ta thích nó, thì tập 2 dường như đẩy chúng ta đi hướng ngược lại. Càng yêu Haruhi bao nhiêu tác giả cố tình cho Haruhi thêm ngang tàng bấy nhiêu ở tập 2.. Quả thật, một người bạn của mình đã phải thốt lên:" Sao không ai vô tát cho con bé Haruhi 1 cái đi". Haruhi là chúa, ai dám tát cô ấy chứ. Tuy nhiên, nhờ đó chúng ta lại có những tràng cười sảng khoái"không thể kiếm đượcc ở bất cứ đâu, bất cứ sách nào" về Haruhi và những gì mà thành viên đoàn SOS xử lý khi phải đối mặt với nó.
4
620273
2015-07-31 09:43:55
--------------------------
237806
11339
Mình mới mua quyển truyện này cách đây 2 ngày-chỉ mua tập 1 và tập 2 và mình đã thấy vô cùng ấn tượng với haruhi suzumiya - tính cách ngang bướng có phần lập dị trong suy nghĩ đây đúng là một mẫu người mà mình rất có hứng thú(không biết ngoài đời có mấy đứa vậy) nhưng cũng có phần hơi ghét do quá đáng đến bá đạo nhưng nói chung là rất cá tính.Tuy nhiên mình thấy tập 2 vẫn chưa xuất xắc bằng tập 1 -hy vọng tập 3 sẽ hay hơn.Hy vọng các bạn đọc và thích nó.YOLO!
5
710652
2015-07-22 19:39:28
--------------------------
237681
11339
Nhận xét chủ quan của mình sau khi xem xong tập này là nó không hay bằng tập 1. Tiếp nối theo câu chuyện  tập 1 về cô nàng Suzumiya mê những điều kỳ lạ và hiện tượng kì bí, sau những phấn khích về những bí mật quanh Suzumiya ở tập 1 thì mình thấy những diễn biến trong tập 2 tẻ nhạt hẳn đi. Vẫn cốt truyện cũ, khi mà bộ ba người ngoài hành tinh, nhà du hành thời gian, người siêu năng lực và nam chính phải cố gắng để cho trí tưởng tượng của Suzumiya thành sự thật, nhưng vì đã biết những điều này từ tập trước rồi nên với mình câu chuyện không còn thú vị như trước nữa. Không có bí mật mới nào được tiết lộ, không có điều mới mẻ nào xảy ra, điều đó làm cho mình cảm thấy chán.
3
123497
2015-07-22 17:07:35
--------------------------
233308
11339
Khi mới mua quyển Haruhi tập 2 này thì mình để ý thấy cái bìa khá moe. Nói thật là mình sắp bị cuốn hút vào những câu chuyện của Haruhi mất rồi, trí tưởng tượng của Haruhi phong phú đến mức mình không biết phải nói gì hơn nữa. Mình đang cố gắng đọc tiếp 2 tập còn lại cũng như chuẩn bị mua quyển 5 tại Tiki vì nghe nói Haruhi tập 5 sẽ sớm ra mắt nên mình rất mong chờ nó. Mong rằng, khi đọc những tập sau mình sẽ thấy được cái gì đó tốt hơn và "hại não" hơn tập này! Rất mong chờ, mong chờ dữ lắm!
5
558720
2015-07-19 15:24:45
--------------------------
211509
11339
Đối với tôi, series Suzumiya Haruhi như đã mở ra cho tôi một thế giới mới vậy. Một thế giới với sức mạnh siêu nhiên, người ngoài hành tinh,... Phải nói là trí tưởng tượng của Haruhi thật đặc biệt khi cô tưởng tượng ra những câu chuyện kì thú kia.
Tôi không xem anime nhưng tôi nghĩ sau khi xem thì phiên bản yêu thích nhất vẫn sẽ là Light Novel. Tuy nhiên, nếu bạn đã xem anime rồi, đọc lại Light Novel sẽ càng để lại ấn tượng mạnh mẽ về truyện hơn cho bạn.
Còn lại, tôi nghĩ Series Suzumiya Haruhi là một bộ truyện bạn không thể không đọc nếu bạn là một tín đồ Nhật Bản.
5
520407
2015-06-20 18:06:13
--------------------------
183213
11339
Mặc dù đúng là bìa vol 2 cũng như mọi người nói đấy xệc xi hơn tập 1 nhưng không sao hay là được rồi.Bìa hình Mikuru nhìn rất moe.Trang màu trông rất đẹp .bìa rời đẹp,mặc dù mặt bìa trong vẫn vậy theo bản Nhật.Nôi dung vẫn rất vui nhộn,hài hước,hay và kì quái.Khổ Mikuru ghê bị haruhi xoay như chong chóng nào thì giúp quảng cáo cho nhiều gian hàng làm nhân vật chính của bộ phim(mặc dù trong anime nó được làm cực kì hãi hùng với hiệu ứng vô cùng...)Mikuru là một Mahou shoujo từ tương lai,Yuki,Koizumi nhân vật phụ,còn Kyon thì vẫn nhọ như xưa vẫn bị sai việc vặt 1 cách đầy đắng lòng.Cách dịch vẫn hay và độc lập,Chất lượng giấy vẫn vậy,không đổi,tuyệt.Mong mọi người hãy chào đón haruhi series để dinh về nhà
5
569687
2015-04-15 18:02:20
--------------------------
171913
11339
Mình biết đến Suzumiya Haruhi lần đầu tiên là qua anime và manga, khi ấy mình đã rất ấn tượng với cô nàng mạnh mẽ với cá tính kì lạ này. Mình không hay đọc sách nên mình không hề biết rằng cô nàng Suzumiya Haruhi về đến Việt Nam với phong cách Light Novel. Cho đến lúc biết được thì IPM đã phát hành đến quyển thứ tư rồi và mình quyết định mua một lúc 4 quyển.
Khi đọc quyển 1 được vài trang, mình cảm thấy khá là nhạt vì nó hơi đặc biệt quá, cho đến khi đọc hết quyển thì mình cảm thấy rất phục tác giả vì cái lối dẫn truyện hài hước và lạ của mình. Mình không thể rời mắt khỏi cuốn sách vì khả năng hấp dẫn của nó. Qủa là 1 tác phẩm hay, cảm ơn tiki đã phân phối 1 tuyệt tác như vậy đến tay mọi người.
5
373909
2015-03-22 22:22:21
--------------------------
131495
11339
Lần đầu tiên mình xem Haruhi là anime. Nói thật cảm giác xem lần đầu thấy đây là một anime khá là nhạt, điểm nhấn duy nhất là hình đẹp, chưa kể anime phát hành bị tráo các eps, vì vậy câu chuyện càng khó hiểu, và rất khó nắm bắt.
Do vậy, mình mới đọc light novel, light novel quả thực rất đặc sắc, chỉ đơn giản qua lời kể của Kyon với những tình huống kỳ lạ cậu gặp phải xung quanh nhân vật Haruhi, bình thường, dị nhưng lại vô cùng logic.
Mình đọc light novel bang tiếng Anh trước rồi sau đó mới đọc bản tiếng Việt này và thấy sách hoàn thiện tốt. Bản dịch rất ok và chuyển tải được hết nội dung. 
Mong các tập tiếp theo sẽ ra trong thời gian sớm!
5
324739
2014-10-25 13:34:21
--------------------------
