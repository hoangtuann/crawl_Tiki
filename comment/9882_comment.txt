79349
9882
Truyện có một cái tên khá lạ, '' Đắm và những truyện ngắn khác ''  nên lúc đầu mình không hiểu lắm, sau mới hiểu Đắm được kấy từ truyện Đắm đuối, nhưng không hiểu nxb cắt ra làm gì.
Truyện khá hay và nhiều ý nghĩa, tuy nhiên có cái gì đó xa xăm làm mình không hiểu hết lắm nội dung của truyện, có một vài chỗ khó hiểu khiến khi đọc xong làm  mình cũng phải suy ngẫm rất nhiều. 
Nói về tác giả, lời văn rất mượt mà, trau truốt, cách dẫn truyện cũng khá thu hút và lôi cuốn, nội dung , những truyện ngắn đều khá hay .
3
57869
2013-06-06 18:43:22
--------------------------
69727
9882
Mình vừa đọc xong Đắm & những truyện ngắn khác của Mai Sơn. Và mình tưởng tượng về tác giả thế này: Có lẽ tác giả thích tìm hiểu về phân tâm học, thích đọc tiểu thuyết của các nhà văn đoạt giải Nobel, thích nghe nhạc thính phòng và thích xem phim nghệ thuật. Vậy đó. Cho nên cuốn sách này có cái đó hơi cao siêu, hơi sang trọng, hơi xa cách… đối với một độc giả bình dân như mình.
Sách gồm một truyện vừa (Đắm đuối) và năm truyện ngắn (Hình dung, Một chỗ chật hẹp, Bữa tiệc, Phức cảm, Tâm cảnh). 
Nhưng hình như mình không cảm được truyện nào cả.
3
47661
2013-04-17 18:44:18
--------------------------
