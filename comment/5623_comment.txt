163689
5623
Mỗi người chúng ta dù đã lớn nhưng trong tâm hồn vẫn có những kí ức về tuổi thơ, có người thì mạnh mẽ, dữ dội có người thì dịu dàng, tĩnh mịch và gắn liền với điều đó phải chăng là người bạn Doraemon. Doraemon là chú mèo máy đem lại nhiều niềm vui cho mọi người, không những thế, bên cạnh là những người bạn sẵn sàng giúp đỡ bất cứ lúc nào. Doraemon không những làm tuổi thơ của tôi thêm rực rỡ mà còn là động lực cho tôi vươn tới tương lai. Dù đã lớn nhưng tôi sẽ không bao giờ quên người bạn này vì hình ảnh đầu tiên trong kí ức là rất khó phai nhòa.
5
13723
2015-03-05 17:53:20
--------------------------
