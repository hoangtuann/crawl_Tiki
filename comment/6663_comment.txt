264614
6663
Mình mua cuốn sách này vì tác giả tony buzan là một tác giả rất nổi tiếng đặc biệt là trong lĩnh vực khai thác tiềm năng trí não con người. Sau khi đọc quyển sách tôi thấy đây là một quyết định đúng đắn. Quyển sách giúp tôi nhận ra các chức năng của bộ não con người mà trước đây mình mới chỉ suy nghĩ qua loa chứ không có một hệ thống các khái niệm và kỹ năng về nó như ghi nhớ, sáng tạo, Bản đồ Tư duy... và những yếu tố quan trọng ảnh hưởng đến sự phát triển trí tuệ. Cuốn sách rất phù hợp với tư tưởng của tôi thì quan điểm của tôi là để cho các bé phát triển một cách tự nhiên nhất.
4
300425
2015-08-13 12:00:20
--------------------------
261424
6663
Tony Buzan là một tác giả quá nổi tiếng về công cuộc chinh phục não bộ. Và dĩ nhiên bộ não trẻ em cũng là một đề tài không thể thiếu. Tôi đọc và đã áp dụng phương pháp Mindmap của Tony Buzan khá thành công và vận dụng được trong nhiều lĩnh vực. Sách khai thác các khía cạnh như trí tuệ cảm xúc, hay sáng tạo và cả trí thông minh đa diện. Ngoài ra sách còn xây dựng một mô hình của cậu bé hạnh phúc để ba mẹ nhìn vào, hơn nữa sách còn hướng dẫn các thực phẩm tốt cho sự phát triển trí óc của bé, rất hữu ích.
5
478786
2015-08-11 08:51:20
--------------------------
246580
6663
Đã từng đọc và áp dụng thành công mindmap trong công việc cũng như trong học tập nhưng đến khi đọc đến quyển sách này thì Tôi mới cảm nhận được hết cái hay của Tony Buzan. Cuốn sách mỏng, viết dể hiểu mang đến cho các bậc làm cha mẹ cái nhìn toàn diện về các giai đoạn phát triển vượt bậc với bộ não của trẻ. Các khía cạnh như trí thông minh đa diện, trí tuệ cảm xúc, óc sáng tạo cũng như các loại thực phầm tốt cho trí não cũng được tác giả mô tả chi tiết để từ đó các bậc phụ huynh có thể có phương pháp phù hợp nhằm tối ưu hóa khả năng của con trẻ.
4
13278
2015-07-29 18:15:46
--------------------------
134288
6663
là một bài học xương máu cho những bậc làm cha làm mẹ, cuốn sách chứa đựng những bài học hết sức bổ ích và lí thú. Nó dạy cho những đứa trẻ cách phát triển tư duy nhạy bén, dạy cho những bậc cha mẹ cách cư xử và nuôi dạy con cái, nó khai mở trí óc ta bằng những phương pháp trí tuệ tuyệt vời như: ghi nhớ, sáng tạo, Bản đồ Tư duy... và những yếu tố quan trọng ảnh hưởng đến sự phát triển trí tuệ. hãy đọc thử, bạn sẽ nhận ra những điều mình học được từ nó là vô giá!
4
311886
2014-11-08 22:33:51
--------------------------
