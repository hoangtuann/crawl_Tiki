399473
9276
"Doraemon bóng chày" – là một bộ truyện tranh nói về môn thể thao khá phổ biến ở Nhật Bản – môn bóng chày của tác giả người Nhật Mugiwara Shintaro & Fujio Fujiko. Bộ truyện này kể về những trận đấu bóng chày của đội Doras – một đội có tinh thần đoàn kết rất cao. Mình rất ấn tượng với nhân vật Chibiemon, cậu thường gây ra rắc rối cho cả đội, nhưng đôi lúc cậu cũng là vị cứu cứu tinh. Trong tập này, các bạn sẽ được thưởng thức trận đấu giữa đội Doras và đội Yamadera, trận đấu này rất gay cấn và hấp dẫn vì không chỉ có Pokoemon mà còn có 2 người anh của cậu ta nữa. Rất cuốn hút.
5
1142307
2016-03-17 20:22:05
--------------------------
253147
9276
Để dành quyền đi tiếp trong giải bóng chày nghiệp dư toàn quốc , Edogawa Doras đã  phải một lần nữa đối đầu với Yamadera Bear - đội bóng đã từng gây rất nhiều khó khăn cho họ . Nhưng lần này Yamadera Bear thi đấu hoàn toàn " fair play " , họ không cần phải bất chấp tất cả mong được chiến thắng để cứu ngọn núi quê hương nữa mà họ chiến đấu vì muốn thắng Kuroemon . Để làm được điều đó , Poko đã mời 2 người anh của mình với những tuyệt kĩ khác nhau về tham gia trận đấu này hứa hẹn sẽ mang đến sự kịch tích cho tập 19 .
5
617834
2015-08-04 10:40:40
--------------------------
220849
9276
Tập này thật gây cấn khi đội Doras phải thi đấu với đội Yamadera trong vòng tứ kết để đi tiếp. Lần này không chỉ có chú chồn núi Pokoemon mà còn có 2 người anh của cậu ta nữa, nhưng cuối cùng Doras vẫn chiến thắng để lọt vào bán kết. Xem tập này mà tức cành hông ông già huấn luyện viên Dora Nosuke, chơi bẩn quá, làm đủ trò để Doras bị loại cho đội Great Doras của ông ta thắng cuộc, xem mà ức chế !!! Tuy bị xử thua cuộc nhưng Kuroemon và đồng đội vẫn muốn đấu 1 trận với Great Doras để xem ai mới thực sự là giỏi hơn, nếu không bị chơi bẩn thì Doras đã thắng rồi .
5
471112
2015-07-03 07:41:47
--------------------------
74951
9276
Mình đọc Doraemon bóng chày lâu rồi nhưng thật sự chưa bao giờ thấy chán. Câu chuyện không chỉ hay mà còn ý nghĩa, nó nhắc ta thấy nhiều thứ. Sự cố gắng, nghị lực vươn lên của bản thân để đạt được thành công. Tình cảm bạn bè trong những lúc khó khăn kể cả khi là đối thủ. Mình rất thích Shiroemon, một chú mèo máy từ bất tài trở thành thiên tài, vượt qua nhiều khó khăn, có nhiều sáng tạo và vươn lên trước con mắt của những đối thủ. Hộ đều ngưỡng mộ và không thể ngăn mình ghen tị với cậu. Cho dù chú mèo máy ấy có kiêu căng hay như thế nào thì mình vẫn cứ yêu. Bởi vì mình biết, đó là Shiroemon và cậu sẽ không bao giờ bỏ cuộc.
5
119038
2013-05-15 22:42:00
--------------------------
