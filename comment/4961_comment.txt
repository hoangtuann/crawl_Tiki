382464
4961
Sách màu rất đẹp, nội dung rõ ràng, mô tả cụ thể các đặc điểm chính của các binh sĩ các thời đại. Thông tin phong phú +hữu ích. phù hợp cho ai thích tìm hiểu về đề tài lịch sử chiến tranh. Xứng đáng để mua đọc và sưu tầm vì phần lớn các sách về đề tài này đều in đen trắng. Mình nhớ ko nhâm đã xem ở hiệu sách còn 1 quyển to như khổ giấy A3 về "Knight" nhưng chỉ xem chứ ko lấy.
Kích thước cuốn này hơi to hơn so với khổ sách trung bình do hình in to hơn nên khó để sắp xếp chung với các khổ sách khác.
Nói chung là nên mua với những ai thích tìm hiểu về nội dung này.
4
1126932
2016-02-19 10:12:08
--------------------------
278689
4961
Mình mua cuốn sách này làm quà cho cháu mình nhưng khi mở nó ra, mình  bị thu hút ngay lập tức vì thế giới hình ảnh trong đó. Hình ảnh trong sách quá sống động, đặc biệt là khi bạn đeo kính 3D vào nữa . Thông tin được trình bày rõ ràng, khoa học. Đây có lẽ là cuốn sách phù hợp với những ai yêu thích lịch sử thế giới( nhất là các đội quân thần thoại) hoặc là với các bạn yêu nghệ thuật, còn nếu bạn thích cả 2 thứ thì đây là một sản phẩm quá tuyệt! 
5
579087
2015-08-26 08:15:36
--------------------------
