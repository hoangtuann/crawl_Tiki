512257
11387
Sách cũng tạm thôi, do ngẫu bút nên khá lan man . Đọc xong dễ thấy buồn, "như cánh chim vô định giữa đêm đông, thân xác tồn tại, còn hồn lạc giữa hư không"
3
2048439
2017-01-17 22:35:50
--------------------------
503793
11387
"Trên đường" không phải là cuốn sách dành cho những ai yêu thích thể loại truyện có nhiều tình tiết kịch tính. Trên đường không có kịch tính gì hết, nó không khiến người đọc tò mò, thấp thỏm lật trước trang cuối để xem nhân vật chính kết thúc câu chuyện như thế nào. Sẽ có lúc bạn chán, sẽ có lúc bạn hoang mang, truyện này nói cái gì, nhưng nếu đ"Trên đường" không phải là cuốn sách dành cho những ai yêu thích thể loại truyện có nhiều tình tiết kịch tính. Trên đường không có kịch tính gì hết, nó không khiến người đọc tò mò, thấp thỏm lật trước trang cuối để xem nhân vật chính kết thúc câu chuyện như thế nào. Sẽ có lúc bạn chán, sẽ có lúc bạn hoang mang, truyện này nói cái gì, nhưng nếu đặt câu chuyện vào bối cảnh ra đời của nó, sẽ thấy trân trọng tác phẩm này hơn, thấy được một bức tranh có phần u ám của 1 thế hệ. Trên đường chỉ mở ra trước mắt người đọc những cung đường rộng thênh thang và tuyệt đẹp của nước Mỹ, Trên đường giúp người đọc hình dung một nước Mỹ rất thật vào những năm 60s qua suy nghĩ, hành động của lớp trẻ bấy giờ. Thuốc phiện, nhậu nhẹt, trai gái, trộm cắp, nhưng trong thăm thẳm, họ là những người rất máu lửa, tất cả chỉ vì trốn tránh thực tại ẩm ương. Câu chuyện thực chất vẫn chưa đến hồi kết, Sal, ở đó, vẫn tiếp tục những cung đường của mìnhặt câu chuyện vào bối cảnh ra đời của nó, sẽ thấy trân trọng tác phẩm này hơn, thấy được một bức tranh có phần u ám của 1 thế hệ. Trên đường chỉ mở ra trước mắt người đọc những cung đường rộng thênh thang và tuyệt đẹp của nước Mỹ, Trên đường giúp người đọc hình dung một nước Mỹ rất thật vào những năm 60s qua suy nghĩ, hành động của lớp trẻ bấy giờ. Thuốc phiện, nhậu nhẹt, trai gái, trộm cắp, nhưng trong thăm thẳm, họ là những người rất máu lửa, tất cả chỉ vì trốn tránh thực tại ẩm ương. Câu chuyện thực chất vẫn chưa đến hồi kết, Sal, ở đó, vẫn tiếp tục những cung đường của mình
4
733941
2017-01-02 07:12:44
--------------------------
451050
11387
Đầu tiên thì, thực sự theo mình nghĩ, cuốn sách này khá là khó hiểu, chắc chỉ nguời lớn hoặc đọc nhiều sách thì có thể hiểu tường tận. Bản thân mình cũng chưa hiểu rõ, văn phong khá giống mấy quyển của Dan Brown.
Tuy nhiên, cuốn sách này thì lại mang ý nghĩa rất sâu sắc và khá hay. bạn sẽ nhìn thấy được bức tranh toàn cảnh về nước Mỹ, câu chuyện như một trải nghiệm thực sự của hai nhân vật.
Câu chuyện là chuyến hành trình của những con nguời tràn đầy khát khao chinh phục miền đất mới, những người liều lĩnh đối đầu với thử thách.
Thiết kế bìa cũng khá độc đáo, 
5
448018
2016-06-19 13:28:56
--------------------------
410740
11387
Dù được mọi người khen ngợi nhiều nhưng có lẽ quyển này không hợp với mình. Mình vốn là fan của thể loại tiểu thuyết kinh điển nhưng không hiểu sao mình không cảm nổi cuốn này. Quả thực, mình rất ngưỡng mộ sự ngông nghênh và nhiệt huyết của Sal, thích cách anh đi khắp nước Mĩ, thích cái tâm hồn tự do ấy. Mình cũng ghen tị với anh vì anh đã có những người bạn tuyệt vời, như Dean chẳng hạn. Có những đoạn nói về cảnh các anh ăn chơi, tán gái, dụ dỗ họ rồi vứt bỏ họ, những cô gái trong đây cũng toàn là những người lẳng lơ và dễ dãi, có lẽ bối cảnh thời đó và tư tưởng phương Tây phóng khoáng như vậy nhưng mình không chấp nhận được. Một phần về cách dịch của dịch giả, nào là "cứt", "làm tình"... nghe thật thô thiển, không biết tác giả viết như thế nào nhưng thiết nghĩ dịch giả nên chỉnh sửa đôi chút. Không phải cứ giữ nguyên xi từ ngữ của bản gốc là giữ được tinh thần của tác phẩm, nên cân nhắc sửa chữa để phù hợp với văn hóa Việt Nam mình. Trên đây là vài dòng nhận xét của mình, hy vọng lần tái bản sau sẽ hoàn thiện hơn.
3
593346
2016-04-04 19:50:47
--------------------------
407716
11387
Khi bắt đầu đọc quyển sách này, mình có hơi bối rối. Vì thực sự hơi khó đọc. Văn phong không giống như mình mong đợi, rời rạc và không có kịch tính. Nó như một cuốn nhật ký ghi chép thì đúng hơn. Tuy nhiên, cuốn nhật ký này rất chân thật, không phải loại sách hoa mỹ, giàu sức tưởng tượng hư cấu, mà nó là những trang nhật ký ghi lại cuộc đời hiện thực, cuộc sống nổi loạn, dám làm những gì mình thích của những thanh niên sống hết mình cho hiện tại, không cần biết về những chuyện ngày sau. Đọc xong cuốn sách này, một nửa mình ngưỡng mộ những con người như vậy, ngưỡng mộ họ dám sống cho mình, nhưng một nửa mình lại cảm thấy không chấp nhận được lối sống có phần hư hỏng và không suy nghĩ cho tương lai của cả thế hệ thanh niên trong thời kỳ này. Những câu chuyện được kể lại của tác giả có phần rời rạc, và hơi khó để đoc. Vì vậy, bạn hãy kiên nhẫn khi cầm cuốn sách này trên tay.
4
375725
2016-03-30 16:55:32
--------------------------
386602
11387
Thêm một cuốn sách về khám phá vùng đất mà tôi tâm đắc. Lối dẫn chuyện của tác giả khiến tôi cảm thấy mình phải thật thận trọng mới tìm thấy được đường đi. Một cuốn sách với những trải nghiệm mà cho dù chỉ đọc qua những trang sashc, nó cũng sẽ khiến bạn khó quên được. Bên cạnh đó, là những bài học, những triết lí đầy tính nhân văn dành cho tuổi trẻ tươi đẹp. Miêu tả cảnh vật của tác giả chân thực đến nỗi tôi như thấy mình đang đứng ở Mỹ, đang ngắm nhìn cảnh vật bằng hai mặt chứ không phải là qua trang sách.
5
211114
2016-02-25 22:08:47
--------------------------
374136
11387
Một cái nhìn đặc trưng rất "Mỹ".
Trên đường cho thấy những hồ hởi tuổi trẻ, những khát vọng và đam mê. Tuy có ngông cuồng, có bấp bênh. Nhưng ở đó chính là tuổi trẻ. Thử hỏi có ai được một lần đi lại con đường tuổi trẻ. Vậy hà cớ gì không một lần thử đứng lên và bước đi?
Tuy nhiên, bên trong đó cũng là một cái nhìn rất hiện thực của Xã hội Mỹ lúc bấy giờ, một Thế giới mới không dễ dàng... nhưng là một Thế giới đầy sức sống, trần tục và liên tiếp những thú vị. 
Không dễ gì mà có được một lần trong đời, trải nghiệm những bất ngờ tuyệt vời đó.
4
876813
2016-01-25 16:34:51
--------------------------
368641
11387
Cuốn sách này thực sự thể hiện rõ được sự điên cuồng của tuổi trẻ, khao khát tự do, khao khát trải nghiệm, và khao khát đi.
Ban đầu cuốn sách đối với mình khá khó đọc, không phải vì cuốn sach dở mà vì cái cảm giác cuồng loạn mà cuốn sách mang lại khi mình đọc. Nhưng rồi, càng đọc mình càng thấy bản thân của mình trong đó, cái nỗi khao khát tự do tuổi trẻ, tự do tung hành, phiêu bạt. 
"Trên Đường" đúng là trên đường, ta không hề biết những gì sẽ đến, những gì bản thân ta sẽ đối mặt, rồi những ngã rẽ sẽ dẫn ta đến đâu. Thê nhưng, những điều đó mới mang lại ý nghĩa thực sự cho đời người. Ngẫm lại, từ khi ra trường, đi phỏng vấn tìm việc, cái mà mình ghét nhất chính là câu hỏi kế hoạch ngắn hạn, dài hạn, 3 năm, 5 năm của những nhà tuyển dụng. Một câu hỏi quá vô nghĩa. Nếu ta thấy được những gì ta sẽ có, đời còn gì để khám phá?
Cuộc sống là một chuỗi những trải nghiệm để ta định hình bản thân, giá trị của chính mình và tạo lập một cuộc đời riêng cho bản thân mình. Nó còn là chuỗi phép thử và sai, những gì bản thân mỗi người trải qua sẽ là những giá trị vĩnh hằng mà bản thân họ có, và không ai giống ai. 
Tương lai ta không hề biết, tận hưởng giá trị món quà của hiện tại, sống với ước muốn, với khao khát của chính bản thân là những gì mà tôi bị cuốn hút qua từng trang sách.
Càng về sau, nhịp sống như dồn dập hơn, mãnh liệt hơn, điên cuồng hơn và tôi thấy được sức trẻ bừng bừng qua mỗi hành trình.Tuy qua cuốn sách, mình cảm nhận một phần của cái gọi là vô định hướng của tuổi trẻ nhưng thực sự cái giá trị ẩn sau đó mà mình thực sự thích chính là cái khao khát đi, khao khát sống, khao khát trải nghiệm.
Mỗi bước ta đi qua chính là cái đã tạo nên con người ta bây giờ, xấu hay tốt chỉ mình bản thân mỗi người biết, nhưng cái điều mà họ thực sự đã có chính là những bài học trên đường đời mình bước. Và đó chính là tài sản vô giá của mỗi người, khi nhìn lại, ta có quyền tự hào về những gì ta đã trải qua để học để lớn, để sống và để đi.
5
106800
2016-01-14 13:11:39
--------------------------
363796
11387
Sống cuộc đời cho đáng. Và cái đáng này với Sal hay Dean là những chuỗi ngày rong chơi, phiêu bạt hết từ miền viễn Tây hoang dã sang tận cả Mexico với những điệu mambo cuồng nhiệt.
Với lối văn rất thực, Jack đã cho ta thấy một bức tranh nhiều màu sắc về tuổi trẻ và những cuộc rong chơi của ông, từ những lần nhảy tàu đến những lần tiết kiệm liều lĩnh, thó luôn đồ ăn trong quán, hay đơn giản chỉ miêu tả những câu nói của Dean kết thúc với ba chấm(...) để ta thấy được một Dean nổi loạn, khùng khùng, không mấy dễ ưa nếu chỉ tiếp xúc lần đầu.
Tuổi trẻ người ta vậy đấy, sung sức, hoan lạc, lầm lỗi. Nhưng quan trọng hơn hết, người ta dám sống với đam mê của mình, sống với con người thực của mình. Chúng ta liệu có dám điên như họ?
Cuốn sách này có lẽ hơi khó đọc với những ai thích kiểu văn mượt mà, trau truốt. Không! nó thực lắm. Vì là thực nên nhiều khi hơi khó hiểu và lộn xộn. Thế nên hãy đọc Trên Đường khi bạn thật sự có thời gian cho nó.
3
346234
2016-01-04 22:31:59
--------------------------
363773
11387
nổi loạn, phóng túng, hoang dã
trầm mặc rồi lại cuồng nhiệt đến điên rồ
lối viết chắp nối rời rạc khá lạ nhưng ẩn chứa trong từng câu chữ là khát vọng mãnh liệt tìm cho mình 1 nơi thuộc về. Làm mình bất ngờ, thích phát điên lên đc, nhớ đến 1 người nữa, ngẩng đầu về phía tây,chỉ cần nhìn thấy những ngôi sao mang hy vọng về miền đất ấy, thế cũng đủ rồi
"cách nhìn mỹ về tự do và hy vọng" :))
"bởi tôi thích qá nhìu thứ và cứ mải mê đuổi theo hết ngôi sao băng náy đến ngôi sao băng khác đến khi kiệt sức. Thế đó, tôi chẳng có gì để trao cho người cái gì ngoài sự rối tinh của chính mình"

4
335409
2016-01-04 21:45:09
--------------------------
350059
11387
Nội dung sách chỉ là hành trình du ký của 2 gã đàn ông trên những chiếc ô tô mua góp hoặc trộm, xăng, thuốc lá, đồ ăn cái nào trộm được thì 2 lão đều trộm. Hai ông này đi dọc nước Mỹ mỗi năm đi 1 đến 2 lần, không có tiền vẫn đi, vừa đi vừa kiếm. Mặc dù Dean đã lấy 2, 3 cô vợ và có đến 4 đứa con ngoài giá thú nhưng mỗi lần Sal đến rủ là bỏ vợ tót đi ngay. Còn Sal cũng vậy dù là nhà văn ở New York nhưng vẫn nuôi chí tang bồng thích đi du hý cùng Dean, thích nghe những điều Dean nói, dù cho cả thế giới có coi Dean là tên khùng vô loại nhưng Sal vẫn tin tưởng ở Dean vẫn nhìn ra được trong Dean đó là một chàng trai ngây thơ yêu tự do không theo bất cứ quy luật nào của cuộc sống, Dean tạo nên ý nghĩa cho cuộc sống của chính gã và cũng cho cả Sal nên cả hai cứ dính lấy nhau.
Mở đầu tác phẩm là hình ảnh Dean kết thúc cũng là hình ảnh của Dean trong tâm trí của Sal dù Sal mới là nhân vật chính. Trong truyện còn rất nhiều chi tiết diễm tình như kiểu Sal sẵn sàng bỏ rơi cô gái mà anh yêu dọc dường để về nhà vì ko có tiền lo cho cuộc sống của cô anh hứa sẽ quay lại nhưng ko bao giờ quay lại và cũng không mang cô theo. Nhưng lúc Dean gặp khó khăn bị vợ đuổi thì Sal sẵn sàng cưu mang Dean và còn vạch kế hoạch để cả hai sang Ý, dù cuối cùng chuyến đi không thành vì Dean gặp gái rồi yêu và lại kết hôn. Và còn nhiều câu thoại rất chi là tình thú nữa.
Nói chung khi đọc quyển này cảm thấy rất hạnh phúc nhưng không phải vì chuyện bromance đâu (chắc cũng có nhưng chỉ chút đỉnh thôi) mà chủ yếu là vì vừa đọc vừa có cảm giác như mình đang cùng một nhóm người hiểu mình tự do không lo không nghĩ trên chiếc ô tô cứ lao về phía trước, tóc thổi trong gió lạnh có, nóng có và phía trước là con đường vô tận. Chỉ cần nhắm mắt và hưởng thụ chờ đợi điểm dừng để lao vào khám phá, đặt lại phía sau tất cả mọi gánh nặng mọi tất bật lo toan của trần thế.
Một câu hỏi rất hay trong truyện : "Em mong đợi gì ở cuộc đời này?" Người nào có thể trả lời được câu hỏi này chắc chắn là người hạnh phúc vì đã biết được điều mình muốn và mình cần.
5
180949
2015-12-09 20:10:33
--------------------------
309880
11387
Phải nói là bìa sách của lần tái bản này quá đẹp, con đường ngoằn nghèo giữa đồng rộng và núi cao, như những con đường ngang dọc nước Mỹ mà Sal và Dean . Khi mới đọc cuốn sách này, mình thấy hơi khó chịu vì mạch văn khá rối, sau nghĩ lại thì đây là kiểu văn xuôi bột phát, được viết ngay khi ý tưởng đến cho nên mới có cảm giác như vậy. Điều nữa khiến mình khó chịu đó chinh là lối sống phóng túng và bất cần của các nhân vật, nhưng đó cũng chính là điều cuốn sách này muốn truyền tải, về một thế hệ Beat khó khăn trong việc định phương hướng. Nhưng mình cũng đã nhìn thấy hướng đi cho thế hệ đó khi đọc tới phần cuối cùng: gia đình. Trên đường là một cuốn sách hay về tuổi trẻ, về một thế hệ đã qua của nước Mỹ.
4
105510
2015-09-19 10:22:43
--------------------------
305442
11387
Giọng văn nồng đượm chất khoáng đãng, tự do, hệt như cuộc hành trình rong ruổi khắp nước Mỹ của chính tác giả vậy. Trong câu chuyện có cả tình bạn lẫn tình yêu, những mối quan hệ có chút "ngông" và đôi lúc hết sức hoang dại. Ban đầu mới đọc, tôi vẫn chưa thể quen được với lối hành văn và nhịp văn nhanh thế này; câu văn đôi chỗ khá lê thê và khó hiểu, nhưng sau dần khi đã nắm bắt được, tôi như cảm thấy mình bị cuốn vào nó từ lúc nào không hay. Cuốn sách đã phần nào đáp ứng được ham muốn phiêu lưu của bản thân tôi. Tôi thật sự thích nó.
5
580478
2015-09-16 22:57:45
--------------------------
296959
11387
Một cuốn sách đáng đọc cho tuổi trẻ, những con người đang tìm kiếm lý tưởng, yêu tự do và đầy khát khao. Và tất nhiên đã là kinh điển thì đôi khi việc khó đọc là điều không thể tránh. Bạn có thể cân nhắc kĩ càng trước khi mua quyển sách này, nếu không quen với văn phong, bạn có thể sẽ hơi hụt. Tuyến nhân vật trong sách là những con người của tự do, phóng khoáng, đôi lúc điên cuồng đến hoang dại. Họ là những người trẻ, sống cháy hết mình và luôn muốn tìm đến những điều mới mẻ hơn cho cuộc sống. Nếu bạn là người thích những cuộc phiêu lưu, trải nghiệm những điều lạ lẫm thì hãy dành thời gian để đọc cuốn sách này!
4
474670
2015-09-11 14:12:05
--------------------------
269571
11387
Nhận xét đầu tiên và hết sức chân thành (đặc biệt dành cho những bạn định mua quyển này): Đây là một quyển sách cực kỳ khó đọc. Văn phong của “Trên đường” khó đọc và lạ lẫm ngay từ trong nguyên tác, ngay từ khi xuất bản đã làm dấy lên nhiều tranh luận trái chiều. “Trên đường” gây dấu ấn với những câu văn rất dài, cấu trúc phức tạp, lộn xộn, gây hoang mang cho người đọc. Các nhân vật trong truyện điên cuồng, nổi loạn, bất hạnh và đáng thương vì sống mà không biết được tương lai sẽ trôi về đâu khi mọi ước mơ và lý tưởng đều vỡ vụn. Cuốn sách có lẽ kén người đọc nhưng vẫn thích hợp với những ai cũng mong muốn lên đường để tìm hiểu bản thân hoặc chí ít để gặm nhấm nỗi bất hạnh và cô đơn của mình.
4
273261
2015-08-17 15:56:11
--------------------------
264062
11387
Trên Đường hoàn toàn không phải là một cuốn nhật kí hành trình hay cẩm nang du lịch bụi dành cho người trẻ hiện đại mà là 1 tác phẩm văn học kinh điển phản ánh một giai đoạn trong lịch sử Mỹ. Đây là điều tất nhiên dễ dàng nhận thấy vì cuốn sách được viết vào thập niên 60. Toàn bộ nhân vật chính không phải là những con người đang vào giai đoạn tuổi thanh xuân, cũng không phải là con người dư dả về vật chất. Chuyến phiêu lưu xuyên nước Mỹ của họ bên cạnh sự phóng túng, bên cạnh sự đi vì khát khao được đi của tuổi trẻ và sự vô trách nhiệm của nhân vật Dean Moriarty khi bỏ lại vợ con chật vật kiếm cái ăn ở nhà, những đứa con rơi rớt khắp nơi, li dị và tiếp tục li dị,.. mà đằng sau đó là những bất mãn trước hệ thống chính trị, sự suy tư về những mối quan hệ, về mục đích sống, là sự am hiểu văn chương, nghệ thuật. Như vậy cuốn sách hoàn toàn không phải chỉ là "những giây phút đam mê và sống trọn vẹn nhất với ước mơ và con người thật của mình, bỏ lại đằng sau những ưu tư, vui vẻ cùng bạn bè xách ba lô và lên đường, sống thật ý nghĩa những năm tháng tuổi xanh không bao giờ quay lại" như mấy nhận xét đã được viết. Nếu chỉ là như thế thì Trên Đường không được đưa vào hàng tác phẩm kinh điển. Điều cuối cùng đọng lại cho người thực sự đọc hết cuốn sách và suy nghĩ là: Sống như thế nào mới gọi là "sống thật ý nghĩa"???
4
38615
2015-08-12 23:32:36
--------------------------
263185
11387
Đọc cuốn sách này giúp mình tìm lại cái thời trẻ trâu, hay thích dong xe rong ruổi với mấy đứa bạn xuống các tỉnh miền Tây để khám phá văn hóa và ẩm thực ở đó.  Cuốn sách không phù hợp với những bạn chỉ thích ngồi ì một chỗ và du lịch qua cái màn hình laptop mà phù hợp với những bạn ưa dịch chuyển, thích khám phá và có máu phiêu lưu mạo hiểm. Nó đưa chúng ta đến những vùng đất mới trên bản đồ cũng như những vùng đất mới trong tâm hồn chưa được khai phá, giúp ta hiểu chính mình hơn, yêu những con đường hơn và sống có ý nghĩa hơn
Đây không nên là quyển sách chỉ đọc 1 lần.
4
464538
2015-08-12 13:40:02
--------------------------
258587
11387
Tiếp cận tác phẩm sau khi đọc nhiều thông tin  giới thiệu Trên đường như là một tác phẩm kinh điển, cũng là lần đầu đọc văn Kerouac, những trang đầu tiên khiến tôi hơi ngỡ ngàng và cũng hơi khó bắt nhịp với giọng văn tưng tửng, đôi ba phần châm biếm cùng với những ý tưởng và lời thoại có phần hài hước của các nhân vật. Nhưng khi đã quen rồi, và dặn lòng "buông thả" một chút, bỗng thấy các chuyến phiêu lưu xuyên Mỹ của Sal (và Dean) ngày một thêm hấp dẫn, mới lạ, và "phiêu" đúng chất của người trẻ, những người đam mê thú vui mà ngày nay ta hay gọi là "phượt". Quả thật, chỉ khi người ta trẻ, ta mới có những giây phút đam mê và sống trọn vẹn nhất với ước mơ và con người thật của mình, bỏ lại đằng sau những ưu tư, vui vẻ cùng bạn bè xách ba lô và lên đường, sống thật ý nghĩa những năm tháng tuổi xanh không bao giờ quay lại. Độc giả từng có một thời tuổi trẻ sôi nổi, chắc chắn sẽ đâu đó tìm thấy mình qua hình ảnh Sal, Dean và những người bạn của các anh, trên đường, trên những chuyến đi ..
3
392686
2015-08-08 17:18:53
--------------------------
258015
11387
"Trên đường" là cuốn sách dành cho những người trẻ ưa thích dịch chuyển, trải nghiệm những điều mới lạ. Họ đi để sống hết mình, tận hưởng những ngày tháng tươi đẹp của tuổi trẻ. Quả thật đây là một cuốn sách rất đáng đọc, nên được đưa vào must-read list của người trẻ, xứng đáng với lời nhận xét là một trong những tiểu thuyết vĩ đại nhất thế kỷ hai mươi của nền văn học Mỹ và thế giới.
Ngoài nội dung tuyệt vời thì bìa sách cực đẹp và rất phù hợp này cũng là một điểm cộng.
4
307488
2015-08-08 10:06:58
--------------------------
251859
11387
"On the road" - Trên Đường không chỉ là quyển sách du ký mà nó còn đại diện cho cả một thời kỳ văn hoá của Mỹ. Dù thế, sau rất nhiều năm, với một nền văn hoá khác là Việt Nam nó vẫn tìm được những nhịp đập chung nơi những người trẻ và nhiệt huyết bừng bừng muốn được ra đi và tận hưởng tuổi trẻ. Tôi rất đồng cảm với bạn Nguyễn Dương rằng hãy đọc cuốn sách này hơn một lần. Hãy bỏ bớt những tiểu tiết mà với văn hoá Việt bạn sẽ thấy chúng quá khác lạ, phóng túng. Hãy bỏ bớt định kiến. Và ôm cuốn sách dọc theo hành trình của bạn, cho tuổi trẻ của mình có những dấu ấn. 
5
390673
2015-08-03 10:39:14
--------------------------
247014
11387
Cá nhân mình đang theo đuổi top 100 tiểu thuyết nên đọc trong đời của BBC bình chọn. Khi nhìn thấy cuốn Trên đường xuất bản, mình đặt mua ngay, phần vì nó nằm trong top 100, phần vì cũng thích kiểu sách đi phượt đây đó của tác phẩm. Đọc xong biết thêm được nhiều vùng đất của Mỹ. Thiết kế bìa khá chân phương, cầm khá chắc tay. Về nội dung thì nhân vật ám ảnh nhất đối với mình cũng như mọi độc giả là Dean Moriaty- tay chơi rách rưới, tay lái thần sầu và cũng là một kẻ bất cần đời. Tôi thích nhân vật này nhưng ghét ở hắn một điểm duy nhất là quá hời hợt và thực dụng!
5
292668
2015-07-30 09:41:54
--------------------------
232412
11387
Quyết định mua cuốn sách này vì lời giới thiệu hứa hẹn về một tác phẩm du ký kinh điển của văn học Mỹ. Mình rất thích đọc những câu chuyện du hành có chiều sâu như thế này, những câu chuyện về những người trẻ lang thang vô định để tìm kiếm lại giá trị đánh mất trong một thế giới quá hỗn loạn này. Nhất là cuốn sách không chỉ bao gồm những trải nghiệm du lịch đơn thuần mà còn mang trong mình nỗi đau đớn day dứt của nhũng người đánh mất lẽ sống trong một xã hội hiện đại nhưng lạnh lùng này. mỗi người có một cách trải nghiệm của riêng mình, khi cuộc đời tôi mắc kẹt với cái góc nhỏ hẹp này, một cuốn sách như thế này giúp tôi dõi mắt theo cuộc hành trình của họ và rút ra được gì đó  cho bản thân mình.
3
419116
2015-07-18 19:46:56
--------------------------
228332
11387
Nếu như được trẻ lại 10 tuổi, hẵng sẽ sống đời The road is home, và quyển sách này, hay Bắt trẻ đồng xanh, sẽ trở thành thánh kính. Nhưng suy cho cùng, vẫn là không thể.
Và dù là không thể sống đời "on the road", thì vẫn có thể yêu thích quyển sách này :))
Đây là một quyển sách ngắn về một câu chuyện có hành trình lên đường rất dài, và nhường như là bất tận. Tuổi trẻ ngắn ngủi, nhiệt huyết cháy bỏng, tương lai chính là những giây phút được đi và ngắn nhìn thế giới trong hiện tại.
Tất cả đều rất phóng khoáng và tự do, từ nhân vật, đến giọng văn. Không có gì có thể trói buộc được những đôi chân luôn khao khát được phiêu lưu.

4
30924
2015-07-15 12:17:00
--------------------------
226031
11387
Nếu mua cuốn sách này mà bạn chỉ đọc một lần thì quả là uổng phí.

Hãy đọc lại lần thứ 2, thứ 3. Nghiền ngẫm nó thật lâu bạn mới thấy mình bỏ quên nhiều tình tiết của câu chuyện. Có những câu nói trong cuốn sách này khiến bạn phải đọc đi đọc lại mới có thể hiểu được. Hay là lý do mà Sal quyết định đi xuyên Mỹ, tạm biệt quê hương nhàm chán của mình. 
Tự do, phóng khoáng đó là tinh thần của người Mỹ. Hãy cứ đi và trải nghiệm, sống hết mình với cuộc đời để sau này không phải hối tiếc nữa
5
408981
2015-07-11 11:54:47
--------------------------
212614
11387
Tôi thích đi, đi khắp nơi,khám phá những địa điểm làm tôi phải ngỡ ngàng.Tôi yêu thích những cuốn sách nói về những chuyến đi vì thế tôi đã mang " Lên đường " về.
Tôi vốn không có niềm yêu thích đặc biệt với nước Mỹ.Có lẽ vì vậy mà tôi không thích cuốn sách này lắm...
Điều cuốn sách này làm được là khắc họa những đam mê tuổi trẻ, tuổi của nhiệt huyết.họ làm bất cứ điều gì họ thích.Phóng khoáng, tự do, lên đường.... Tôi thích tinh thần đó.
Nhưng với tôi , " lên đường' là từ từ chậm rãi, là hưởng thụ,là hạnh phúc" nên tôi không cảm được hết những gì tác giả muốn thể hiện.Là do tôi không hợp với cuốn sách này.Mong cuốn sách này sẽ đến tay những người thực sự cần chúng.Nó sẽ giúp ích rất nhiều.
3
379843
2015-06-22 17:34:43
--------------------------
212185
11387
Dean Moraty một nhân vật mà tác giả luôn bị cuốn hút bởi sức sống mãnh liệt, những suy nghĩ táo bạo hay đam mê điên cuồng của anh. Sal Paradise đã quyết định lần đầu đi xuyên Mỹ để chấm dứt những năm tháng nhàm chán ở quê nhà, để rồi từ đó anh nhiều lần đi bộ, quá giang xuyên Mỹ chỉ để gặp người bạn thân của mình để sống hết mình vì tuổi trẻ, vì đam mê, vì mỗi người chỉ sống một lần thôi mà, phải sống hết mình chứ.
Beat Generation là một thế hệ nổi loạn của Mỹ, nơi con người tìm kiếm sự tự do tuyệt đối về tình yêu, về cuộc sống và cả những nhu cầu tự do nhỏ bé nhất như ăn mặc....Cuốn sách kết thúc trong cảnh tác giả ngồi trong xe nhìn đứa bạn thân đang ngóng theo cứ khuất dần trong màn đêm của New York nói lên một điều răng ta có thể làm bất cứ thứ gì ở tuổi trẻ nhưng trung niên hay có gia đình rồi thì mọi chuyện sẽ khác. Và cũng như những quy luật khác trong cuộc sống, không ai có thể quay về quá khứ, thời gian không bao giờ quay lại được....Nên chúng ta phải sống hết mình, trọn vẹn từng phút giây.
4
174816
2015-06-21 22:20:52
--------------------------
199097
11387
Đây là cuốn sách của tuổi trẻ đầy tính hiện thực. Ở đó có những người trẻ đam mê chủ nghĩa xê dịch. Họ cứ đi mà chẳng có đích đến nào cụ thể, lên chuyến xe gần nhất, dừng chân tại nơi mình cảm thấy thích, cứ đi và chẳng bận tâm lo nghĩ gì đến tương lai. Những chuyến đi cứ ào qua khắp mọi nơi với đầy đủ mọi sự tự do, sôi nổi, phóng khoáng thậm chí là điên cuồng.
Thực sự tôi không có nhiều kinh nghiệm trải đời để nói về tinh thần của cuốn sách. Những khía cạnh u tối như một thế hệ bị đánh cắp, bị tổn thương, những góc khuất trong lòng xứ sở cờ hoa, những rạn nứt sau khi giấc mơ Mỹ tan vỡ...có vẻ quá tầm đối với tôi. Tất cả những thứ mà cuốn sách này mang lại cho tôi là dược làm bạn đồng hành của nó, bạn đồng hành trên những chuyến đi, thỏa mãn khát khao và ước vọng. Những chuyến đi vẫy gọi và ta trả lời bằng cách ở trên đường....
5
263240
2015-05-21 15:11:37
--------------------------
198083
11387
Cuốn sách cho ta thấy vẻ đẹp của những chuyến đi. Ta sẽ gặp những con người mới, những điều thú vị, cả những biến cố nữa. Nếu ai đó chưa từng đến Mỹ, thì đây là cẩm nang dành cho họ với những miêu tả về những con đường từ miền đất này đến miền đất khác. Ngoài ra bạn cũng sẽ thấy được những con người được khắc họa chân thực, với cuộc sống khác nhau,tính cách khác nhau, những rắc rối và vấn đề khác nhau nhưng ai cũng ấp ủ trong mình những ước mơ, khát vọng cao đẹp và cố gắng để đạt được nó. Là cuốn sách mà mỗi bạn trẻ nên đọc để khi gấp những trang sách cuối cùng, ta biết mình phải làm gì tiếp theo.
4
483379
2015-05-19 09:10:48
--------------------------
196536
11387
Mua cuốn sách này vì chủ đề Lên đường của nó. Tuy nhiên quyển sách viết về một thế hệ trẻ của nước Mỹ - Beat Generation mà hai gương mặt quan trọng nhất là Sal và Dean cũng là hai nhân vật chính của tiểu thuyết này cùng với lối văn xuôi bột phát của Jack Kerouac, quyển sách này không phải dễ đọc, đúng như đặc điểm của những tác phẩm kinh điển. Bản thân tôi đọc xong quyển sách này không giống như Anna Hassaghi là muốn ngay lập tức lên đường, chộp lấy tháng ngày hiện tại và sống, sống, sống đến tận cùng mà là thấy thất vọng, hoang mang, vô định cho những con người nổi trôi phiêu bạt vô phương hướng giữa cuộc đời...
3
392722
2015-05-16 00:25:51
--------------------------
179587
11387
Đây là cuốn sách đầu tiên mình mua trên tiki, và có thể nói là cụm "The classic of all time" đã thể hiển hết. Jack đã đưa người đọc đi tới các nẻo đưởng nước Mỹ từ Đông sang Tây, rồi xuôi xuống Mexico, hành trình tưởng chừng như vô ích, chán chường nhưng chứa đựng nhiều điều lớn lao. Nổ lực luôn muốn trên đường, luôn muốn đồng hành của Sal với Dean như nổ lực tìm kiếm điều gì đáng sống cho cuộc đời, tìm kiểu một đích đến trong thời điểm giấc mơ Mỹ tan vỡ, vô vọng. Họ như muốn quậy tưng lên, tiệc tùng, lái xe bạt mạng để thoát khỏi guồng quay của cuộc sống chán chướng trôi qua từng ngày. Họ muốn sống cuộc đời một cách trọn vẹn, không hối tiếc.
Sal quý mến Dean vì ở Dean có cái gì rât khác lạ: sự hài hước, liều lĩnh - cũng chính là điều mà Jack cảm thấy ở Neal Cassady.
Đọc Trên Đường như hít được cái bụi đường, cảm nhận cái nóng ghê người một cách chân thật nhất. Jack đã quá xuất sắc trong ngôn từ chân thật của mình.
Tác phẩm để lại trong mình rất nhiều ấn tượng, bài học. Liên tưởng đến cuộc đời mỗi người thì càng thấy đúng, tất cả chúng ta đều đang "Trên Đường".
5
421650
2015-04-07 16:39:10
--------------------------
171550
11387
Một số người cho rằng những điều viết trong sách là những bài học triết lý viết cho một thế hệ, nhưng tôi cho rằng đây là một cuốn tự truyện của cảm xúc, nghĩ sao viết vậy. Jack Kerouac chỉ viết cho mình, lưu lại những ký ức về những ngày tháng của mình, chứ chẳng cho ai cả. Trong những gò bó của cuộc sống, anh đã bỏ mặc tất cả mà đi tìm chính mình, thử trải nghiệm tất cả, bỏ rơi những giá trị chuẩn mực và chỉ theo đuổi hiện tại mà thôi, dù có thấy lạc lõng thế nào đi chăng nữa. Từng lời văn dạt dào cảm xúc và suy nghĩ, nhiều đến nỗi có khi tôi tự hỏi mình đã từng suy nghĩ nhiều được như thế hay chưa về chính mình.

"Tôi tỉnh dậy khi mặt trời đỏ lựng, Và đây là lần duy nhất trong đời, giây phút lạ lùng nhất, tôi không còn biết đích thực mình là ai nữa - tôi đang ở rất xa nhà mình, bị ám ảnh và mệt nhoài vì chuyến đi, một mình trong một căn phòng trọ tồi tàn chưa từng thấy bao giờ, nghe thấy tiếng đầu máy hơi nước rít lên ngoài kia, tiếng sàn gỗ cọt kẹt, tiếng bước chân ngay trên đầu mình và các thứ tiếng động buồn thảm khác. Tôi nhìn lên cái trần nhà cao nứt nẻ và trong vòng mười lăm giây đồng hồ kỳ lạ thực sự không còn biết mình là ai. Tôi không hoảng sợ, chỉ đơn giản thấy mình là ai khác, một kẻ xa lạ, và cả đời tôi đã bị ma ám, cuộc đời của một bóng ma. Tôi đã đi qua nửa nước Mỹ, trên ranh giới giữa miền Đông của thời thơ ấu tôi và miền Tây của tương lai tôi. Và có lẽ vì điều đó mà chuyện này đã xảy ra ở chính nơi này, trong khoảnh khắc này, trong một buổi chiều đỏ ối."
~ Trích từ Trên Đường, chương 3
5
116202
2015-03-22 05:58:09
--------------------------
169220
11387
Trên Đường không phải là quyển sách chỉ để đọc 1 lần: quá nhiều triết lý nhân sinh và cảm nhận chơi vơi, vỡ nát của tác giả mà chúng ta khó có thể hiểu hết được. Nội dung bình lặng, như những trang nhật ký của Jack qua những cuộc hành trình xuyên dọc nước Mỹ, nhưng là một nước Mỹ trong thời kỳ ảm đạm và tan vỡ. Vì gần như là nhật ký của Jack trên đường tìm kiếm ý nghĩa cuộc sống, nên sẽ không có các câu chuyện cổ tích, có tình yêu trọn đời, mà là những câu chuyện rất thật, rất khốc liệt : Cuộc sống là phải trải qua những gì xấu xí nhất: rượu, tiệc tùng thâu đêm, thuốc lá, cần sa,...
Tôi thích lối sống của Sal, Dean, hay các nhân vật bạn của họ. Tự do, lấy niềm tin thần thánh của mình làm lý tưởng sống, là lời phán xét của Chúa. Họ dường như ko thể ở yên 1 nơi, họ cảm thấy số phận mình thuộc về lòng các con đường bụi mờ và kể cả những người lướt ngang đời họ, dù chỉ một thoáng. 
Đọc xong Trên Đường, tôi bỗng muốn đi đây đó, muốn giống như họ: điên cuồng và cháy hết mình cho từng phút giây hiện tại của tuổi trẻ.
5
526861
2015-03-17 20:51:56
--------------------------
158014
11387
Tôi rất thích cuốn truyện này của Jack Kerouac. Qua giọng văn phóng khoáng, sôi nổi, mạnh mẽ của ông, hành trình của hai chàng trai trẻ Dean và Sal hiện lên vô cùng ấn tượng. Theo chân hai gã cao bồi trẻ tuổi rong ruổi trên mọi nẻo đường đất Mỹ, mọi cảnh vật, con người hiện lên đều vô cùng chân thực và sống động. Mạch truyện vô cùng sôi động, từ đầu tới cuối truyện đều vậy. Tôi cực kỳ ấn tượng với nhân vật Dean Moriaty, anh thật sự rất cá tính, tôi thích sự dũng cảm, dám đi, dám trải nghiệm của anh, cũng thích cái tính mê phiêu lưu mạo hiểm của anh. Đọc sách mà tôi chỉ muốn đi ngay, cháy hết mình, sống hết mình với tuổi trẻ như hai chàng trai ấy thôi.
5
449880
2015-02-11 02:55:49
--------------------------
150714
11387
Họ là một nhóm những người bạn cùng trang lứa, lang thang phiêu bạt khắp những nẻo đường nước Mỹ, ngược xuôi Miền Đông - Miền Tây. Họ đi theo bản năng, sống không nhiều mục đích, không có những dự tính xây dựng tương lai và chỉ tiêu xài tuổi trẻ trên những cung đường. Họ có trộm cắp, có kết hôn - li dị - cặp bồ, họ có những thứ tự do phóng túng không biết chừng mực. Họ cứ thế bị chính tuổi trẻ cuốn đi. Sôi nổi, cuồng nhiệt, bốc đồng rồi chìm vào những vàng tối thẫm không có lối thoát ra. Cả một thế hệ những người Mỹ trẻ tuổi sau chiến tranh phải đối mặt với những vấn đề của xã hội. Giọng văn như miêu tả, tường thuật chi tiết, không ra điều hoa mỹ bay bướm mà có gì ngang tàng, bất cần, kể chỉ để kể mà thôi. Có những điều mà các bạn trẻ sẽ học được từ những cuốn sách, có những điều các bạn trẻ nên tránh. Sự bế tắc của những thanh niên trẻ ấy không đại diện cho tất cả các bạn trẻ chúng ta. Các bạn cần tìm cho mình lẽ sống, và cháy hết mình với những ước mơ, thay vì lang thang bất định trong màn sương mù không tìm đâu lối ra.
3
534017
2015-01-17 15:19:17
--------------------------
142333
11387
Thực sự cái mình ấn tượng nhất trong "on the road" chính là cảm giác tự do, họ có thể bắt bất cứ cái xe nào trên đường và quen được bất cứ người bạn nào thuộc đủ mọi thành phần, yêu bất cứ cô gái nào họ gặp. Nó mang lại cho chúng ta sự khao khát khám phá cuộc sống và đi du lịch, thậm chí muốn ra ngay ngoài đường chính để bắt một chiếc xe nào đó. Nhân vật mình thích nhất chính là Terry, thực sự mình rất ấn tượng với cô ấy và cảm thấy buồn bã khi Sal bỏ đi, phim chuyển thể đã không miêu tả được nhiều về cô ấy.
5
472173
2014-12-18 12:34:30
--------------------------
125077
11387
Là một quyển sách thật sự rất đẹp cho tuổi trẻ. Là một quyển sách nói lên niềm đam mê khám phá mãnh liệt, là tiếng nói từ những tâm hồn, những trái tim tràn đầy nhựa sống, muốn chinh phục, muốn đối đầu với thử thách.
"Trên Đường" thực sự trình ra cho người đọc những khung cảnh mà những người khám phá đất Mỹ đi qua. Những cảnh vật, những tiếng động của xe cộ, con người như thực sự sát bên bạn, bởi tác giả miêu tả nó quá chân thật.
Mạch truyện sôi nổi và hấp dẫn, không hề có cảm giác buồn chán, dù là bắt đầu hay kết thúc câu chuyện.
Là những trải nghiệm để khẳng định bản thân và nhìn nhận lại bản thân. Là một câu chuyện giàu cảm xúc và chứa đựng những triết lý tuyệt vời.
Một quyển sách rất đáng để đọc, đọc cho tuổi trẻ đang qua!
5
303773
2014-09-10 23:30:47
--------------------------
123277
11387
Thật ra, mình đọc cuốn này xong cảm thấy buồn bã hơn là yêu đời. Người ta có thể giới thiệu rằng "Trên đường" sẽ khiến bạn muốn sống, sống hết mình, là khát vọng tuổi trẻ, là khoảnh khắc vĩnh cửu...nhưng mình chỉ thấy hào hứng được mấy đoạn đầu, còn về sau càng ngày càng xuống sắc. Cái màu mè của hiện thực bắt đầu lộ rõ. Không phải chỉ một nhóm người trẻ lang bạt trên đường phố, mà là cả một thế hệ của nước Mĩ đang đổ xô vất vưởng trên những con đường, những ga tàu như binh đoàn thây ma, ngày ngày ôm giấc mộng chẳng thành để hướng về tương lai vô định. Câu chuyện bạc phếch màu bụi đường này chủ yếu vẫn là về Dean Moriaty - một nhân vật khiến người đọc có những tình cảm xáo trộn, không biết nên tức, tội, hay thương xót thay cho anh ta. Còn đối với mình, Dean Moriaty (hay nên gọi là Neal Cassady nhỉ?) là một cá nhân rất đặc biệt, vì ở anh ta toát ra một nguồn năng lượng nội tại vừa luôn muốn bùng nổ như pháo hoa, vừa bi thương thống khổ bởi không được giải tỏa, không được thấu hiểu. Rất đặc biệt. Và một lời khen nữa cho bản dịch của Cao Nhị, đọc rất mượt và bụi phủi. Tóm lại, "Trên đường" của Kerouac dễ dàng trở thành cuốn sách yêu thích của mình, khuyến khích mọi người đọc thử :)
5
38828
2014-08-31 11:52:17
--------------------------
122525
11387
Đây là cuốn sách mà bạn có thể cảm nhận ngay sự sôi sục trong từng câu chữ ngay những trang đầu tiên. Tác giả, và những người bạn của mình có thể nói là những thanh niên nổi loạn, với đôi chút ngông cuồng và điên rồ, dấn thân mình lên phía trước, và cứ thế sống với những gì mình muốn. Hành trình của họ là một hành trình vô định, và chẳng vì một ý nghĩa gì lớn lao cả, khi họ cứ đi hết vùng đất này đến vùng đất khác, tìm kiếm những thú vui, tìm gặp lại những người bạn cũ và gây ra một vài vụ lộn xộn không nhỏ. Cứ thế hành trình xuyên nước Mỹ của họ cũng cuốn độc giả theo. Jack kể lại câu chuyện của ông quá chân thực, đến mức ta cảm tưởng như đang nghe được âm thanh của chiếc xe chạy qua chạy lại, ngửi được mùi hương của những cánh đồng bông, và cảm thấy cả âm nhạc của thế kỷ 20 phát ra đâu đó. Đầy sức sống, trần tục, bất ngờ, đó là những gì mà "Trên đường" mang đến. Đây là bài ca về tuổi trẻ, về tình yêu và khát vọng. Đúng như lời nhận xét in trên bìa, rằng những nhân vật trong sách "Họ không biết mình đang tìm kiếm nơi nào để nương tựa, nhưng họ vẫn không ngừng tìm kiếm", họ chỉ đơn giản sống hết mình cho thực tại để cảm nhận mọi cung bậc của cuộc sống này. Đọc, và bạn sẽ thấy, tuổi trẻ của ngày xưa đó, và của ngày hôm nay đây, cũng không có gì khác nhau cả.
5
109067
2014-08-26 13:47:14
--------------------------
