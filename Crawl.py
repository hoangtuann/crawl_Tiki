#!/usr/bin/python
#-*- coding: utf8 -*-
import re
import urllib
import json
import random
import datetime
from BeautifulSoup import*

#Ham chuyen chuoi int sang time
def GetTime(number):
	value = str(number)
	s = (
		datetime.datetime.fromtimestamp(
			int(value)
		).strftime('%Y-%m-%d %H:%M:%S')
	)
	return s

def GetJsonFile(link,key): #link: review_url
	handle = urllib.urlopen(link)
	html =  handle.read()
	t = random.randint(0,99)
	filename = GetProductID(link)+ '_json'+str(t)+'.txt'
	jsonfile = open(filename,'w')
	jsonfile.write(html)
	jsonfile.write('\n')
	jsonfile.close()
	#print'Finish getting json file',filename
	#Call GetBookInfo function
	return filename

def GetComment(filename,key):
	file = open(filename,'r')
	html=file.read()
	info =json.read(html)
	name = str(key) + '_comment.txt'
	res = open(name,'a+')
	for i in info["data"]:
		res.write (str(i["id"]))
		res.write('\n')
		#res.write (str(i["product_id
		res.write(str(key))
		res.write('\n')
		res.write (i["content"].encode("utf-8"))
		res.write('\n')
		res.write (str(i["rating"]))
		res.write('\n')
		res.write (str(i["customer_id"]))
		res.write('\n')
		res.write (str(GetTime(i["created_at"])))		
		res.write('\n--------------------------\n')
	file.close()
	res.close()
	#print 'File chua binh luan: ',name,'tu jsonfile',filename
	
def GetNumberofPage(jsonfile):
	file = open(jsonfile,'r')
	html=file.read()
	info =json.read(html)
	#print'Number of pages:',info["paging"]["last_page"]
	return info["paging"]["last_page"]

def LoadPage(link,id,jsonfile):
	number = GetNumberofPage(jsonfile)
	val=1
	filename=id+'_sublink.txt'
	file = open(filename,'w')
	if number==0: number=1
	if number == val:
		new_val=str(val)
		newlink = link % (id,new_val)
		file.write(newlink)
		return filename
	else:
		while (val<=number):
			new_val=str(val)
			newlink = link % (id,new_val)
			file.write(newlink)
			file.write('\n')
			val=val+1
	file.close()
	#print 'Sublink file',filename
	return filename
	
def GetProductID(link):#link goc 
	testid = re.findall('p[0-9]+',link)
	global id
	for i in testid:
		id = i[1:]
	return id

def GetInsideInfoAndRemoveSpace(line):
	a = str(line)
	b = a.strip('\n')
	data = b.strip(' ')
	atpos = data.find('>')
	sppos = data.find('<',atpos)
	s=data[atpos+1:sppos]
	s=s.strip()
	return s

def ConvertString(data):
	#data="UTF-8 DATA"
	udata=data.decode("utf-8")
	#asciidata=udata.encode("ascii","ignore")
	return udata

def GetBookInfo(link,key): 
	html = urllib.urlopen(link).read()
	soup=BeautifulSoup(html)
	file = open('bosung.txt','a')
	
	file.write(str(key))
	file.write('\n')
	
	titles= soup.findAll('meta',attrs = {'property' : 'og:title'})
	for title in titles:
		s=title.get('content',None).encode('utf-8')
		file.write(s)
		file.write('\n')
		
	prices=soup.findAll('p',attrs = {'style' : ''})
	for price in prices:
		if price.get('data-value',None) is None:
			continue
		else:
			cost= price.get('data-value',None)
			file.write(cost)
			file.write('\n')
			
	#au = soup.findAll('td',attrs = {'class' : 'last'})
	au = soup.findAll('td')
	author_flag = 0 #tag td that contains "Tac gia" appear before the name of the author
	nxb_flag=0
	i=0
	
	for line in au:
		word= str(line)
		if 'Tác giả' in word:
			author_flag = i
		if 'Nhà xuất bản' in word:
			nxb_flag=i
		if 'Manufacturer' in word:
			nxb_flag=i
		i =i+1
	
	if author_flag == 0:
		file.write('NULL')
		file.write('\n')
	else:
		author = GetInsideInfoAndRemoveSpace(au[author_flag+1])
		file.write(author)
		file.write('\n')
	
	if nxb_flag==0:
		file.write('NULL')
		file.write('\n')
	else:
		nxb = GetInsideInfoAndRemoveSpace(au[nxb_flag+1])
		file.write(nxb)
		file.write('\n')
	
	#author_pattern = re.compile(r'\'brand\': "([^"]+)"')
	#author_matcher = author_pattern.search(html)
	#author_matcher = re.search(r'', html)
	#if author_matcher is not None:
	#	print(author_matcher.group(1))
		
	images = soup.findAll('img', attrs={'id': 'product-magiczoom', 'src': True})
	for image in images:
		if image.get('src',None) is None:
			continue
		else:
			s= image.get('src',None)
			file.write(s)
			file.write('\n')
	file.write('NULL')
	file.write('\n')
	file.write('NULL')
	file.write('\n')
	file.write('------------------')
	file.write('\n')
	file.close()

			
review_url_pattern = 'https://tiki.vn/ajax/reviews?product_id=%s&sort=desc&include=comments&page=1&limit=5'
lacklink = 'https://tiki.vn/ajax/reviews?product_id=%s&sort=desc&include=comments&page=%s&limit=5'

def Crawl(thisurl,key):
	GetBookInfo(thisurl,key)
	id = GetProductID(thisurl)
	review_url = review_url_pattern % id
	jsonname=GetJsonFile(review_url,key)
	subname =LoadPage(lacklink,id,jsonname)
	sub = open(subname,'r')
	for i in sub:
		name = GetJsonFile(i,key)
		GetComment(name,key)
	print 'Crawl: Done,',thisurl

#link='https://tiki.vn/1000-cau-hoi-dap-ve-thang-long-ha-noi-tap-1-p331182.html?ref=c8322.c316.c873.c4968.c5608.c857.c880.c2766.c3452.c4291.c4978.c5273.c5571.c6144.c2288.c4981.c5289.c5572.c8008.c8154.'
#Crawl(link,10894)

