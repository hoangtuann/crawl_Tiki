63032
7
Với bối cảnh nước Úc cuối thế kỉ XVIII- đầu thế kỉ XIV, The Thorn Birds đã ghi lại những bước thăng trầm của dòng họ Cleary nói chung và của Mecghen Cleary nói riêng từ ngày thơ ấu đến lúc trưởng thành như một quyển tự truyện vô cùng chân thực và đặc sắc. Truyện đã thế hiện tình yêu mãnh liệt và nồng cháy của Mecghen và Ralph-người đã khấn trọn đời cho Chúa trời. Có phải tình yêu ngang trái, bị cấm đoán đều gợi sự xót xa và tiếc nuối cho con người hay không mà chuyện tình Romeo và Juliet, và Mecghen-Ralph đều để lại cho độc giả nhiều suy nghĩ, suy nghĩ về những trái ngang của cuộc đời, những chiếc gai dù nhọn nhưng đầy ma lực, những chiếc gai mà con người - những con chim - đều không ngần ngại đâm xuyên qua tim mình để rồi hót lên khúc ca tuyệt diệu cuối cùng. Đây là một quyển classic đáng để tìm đọc
5
166
2013-03-13 11:02:25
--------------------------
10911
7
Không cần dùng những từ ngữ quá trau chuốt nhưng tác phẩm này có một vẻ đẹp hoàn hảo cùng một sức hút mạnh mẽ. 'The Thorn Bird' là một cuốn tiểu thuyết ghi lại một cách chân thật những xung đột của gia đình Cleary qua nhiều thế hệ, đồng thời dẫn dắt người đọc đến với một nước Úc cũng rất thật với những đàn cừu khổng lồ trong trang trại, với những trận bão bụi và đồng cỏ xanh tươi. Chính sự chân thật, không cầu kì của tác phẩm này và cách giải quyết các xung đột của Collen McCullough đã khiến nó trở thành kinh điển và được rất nhiều người đọc qua nhiều thế hệ yêu thích.
5
2851
2011-09-06 08:45:21
--------------------------
