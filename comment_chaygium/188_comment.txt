575505
188
Mình đã đọc cuốn này bản tiếng Việt do Nhã Nam phát hành. Cuốn tiếng anh có khổ nhỏ hơn, nội dung mình đọc tiếng anh có thể cảm nhận theo cách của mình nên thấy sâu sắc hơn... Một cuốn sách đáng đọc.
5
5184678
2017-04-17 13:25:34
--------------------------
563837
188
One of the greatest books i've read. I've read The Alchemist the first time in vietnamese. That's why i decided to buy its english version. The book i bought is light, pretty beautiful and small enough to carry around when i travel somewhere. I think it's suitable with the price of 103k vnd.
5
5126753
2017-04-04 12:57:24
--------------------------
561973
188
Sách tốt, chuyển nhanh. Xin cảm ơn. Cần nhiều sách thế này cho người mua chọn lựa.
5
5128918
2017-04-02 11:55:43
--------------------------
527625
188
Mình đã đọc Nhà giả kim và quyết định mua bản tiếng anh. Nói chung là giấy không đẹp nhưng nhẹ, bìa không đẹp bằng bản tiếng Việt. Nội dung khỏi phải chê!!!
4
1951053
2017-02-18 20:27:33
--------------------------
501441
188
Mình thấy nhiều bạn chê khổ sách nhỏ với chất lượng giấy. Mình thấy như vậy là ok rồi, khổ sách này giúp ta có thể mang theo đọc bất cứ đâu mà không bị vướng, mình thấy khá là tiện lợi. Còn nội dung thì khỏi phải bàn rồi, mình đã đọc bản tiếng Việt, giờ mình đọc bản tiếng Anh để rèn khả năng học ngoại ngữ
4
470308
2016-12-28 14:49:14
--------------------------
493822
188
Mình mua quyển này cũng lâu rồi. Mỗi tội lười đọc. Mình đang học tiếng anh nên quyển này bổ sung cho mình nhiều từ vựng mới. Với lại nhỏ gọn dễ mang theo. Có điều là nhiều lúc mang theo đọc nhưng gặp từ khó là k biết tra kiểu gì. Các bạn nên tìm đọc
4
97062
2016-12-02 21:17:48
--------------------------
492145
188
Mình thấy có một số nhận xét không thích The Alchemist về kích thước sản phẩm và loại giấy, nhưng ý kiến riêng của mình lại rất thích. Mình đã mua 2 quyển tặng cho bạn mình, bạn mình cũng rất thích sự nhỏ gọn và nhẹ của quyển sách, tiện lợi nằm đọc hay mang theo ra ngoài đọc. Nhìn chung với mức giá như trên cho loại sách ngoại văn, nội dung bên trong thì không cần bàn cãi, thì thật đáng giá!
5
476799
2016-11-23 11:47:50
--------------------------
461168
188
Về nội dung, quyển sách này có đôi chút khác bản tiếng Việt (chút xíu thôi). Vì mình chưa quen đọc sách tiếng Anh nên quyển này đọc hơi chậm một chút. Nhưng cách hành văn hay và có nhiều từ mới khá thú vị. Cốt truyện không phải là dạng lôi cuốn thu hút người xem đọc 1 lúc  từ đầu tới cuối, mà là sâu sắc, phải đọc từ từ mới thấm.
Về chất lượng giấy: vì làm từ giấy bảo vệ môi trường nên giấy khá vàng và nhẹ hơn giấy sách ở VN, khổ sách nhỏ, nội dung truyện là 177 trang. Nói chung mình thấy là với giá hơn 160k thì có hơi cao so với chất lượng sách.
4
691313
2016-06-27 16:35:26
--------------------------
449777
188
Mình đã từng đọc cuốn sách này bản tiếng Việt với tựa Nhà giả kim hai lần rồi và mình vô cùng yêu quyển sách này. Mình mình muốn đọc cuốn sách này nhiều lần hơn nữa và vì thế mình đã quyết định mua bản tiếng Anh để có thể vừa đọc vừa học tiếng Anh. Về hình thức của quyển phiên bản tiếng Anh này thì mình cũng hơi thất vọng vì nó nhỏ gần như là một nữa thôi so với bản Việt nhưng thôi không sao vì bài học trong đó lớn lao hơn nhiều. Cám ơn tiki.
5
712474
2016-06-18 00:04:25
--------------------------
397056
188
Mình vừa nhận được cuốn sách này. Mình có vài nhận xét thế này:
- Khi mở bao bì ra mình thấy hơi bất ngờ (thất vọng) vì cuốn sách kích thước nhỏ hơn nhiều so với hình, chất lượng giấy in xấu quá (giấy đen, sần, kiểu cổ hồi xưa). Nếu đây là sách in ấn tại nước ngoài thì hơi thất vọng.
So với cuốn tiếng Việt thì trình bày không bằng (vì mình đặt mua cả 2 cuốn tiếng anh, tiếng việt để so sánh bản dịch).
- Nội dung thì mình chưa đọc, chắc sẽ đọc bản tiếng việt trước (tuy nhiên cuốn này nhiều người đã đọc nói thấy hay và ý nghĩa). Hy vọng sẽ học được lối hành văn từ cuốn tiếng anh này (bù lại cho hình thức bề ngoài).
2
1024392
2016-03-14 13:49:12
--------------------------
