549061
6
Đầu tiên sách dày, in đẹp, dễ học song bên cạnh đó không kèm theo CD hay phần phiên âm để dễ học hơn. Nói chung mình thấy mọi người nên mua 1 quyển để học.
5
1274364
2017-03-21 07:50:06
--------------------------
542265
6
Sách có màu, trang giấy hơi mỏng, nhìn giống như mấy cuốn sách Tiếng Anh mình hay hay học.
5
1501823
2017-03-15 00:02:34
--------------------------
531521
6
Các bạn nào thích tiếng anh thì còn chần chờ gì nữa <3
5
1036308
2017-02-25 09:48:45
--------------------------
519269
6
Đây là 1 cuốn sách khá hay, có khoảng 4000 từ mới, hình ảnh đẹp mắt và sinh động. Sách có 12 phần là : Everyday language, people, housing, food, clothing,health, community, transportation,work,areas of study, plants and animals và recreation. Mỗi phần đều có khoảng 5-10 mục nhỏ. Tuy nhiên mình thấy cuốn sách này giấy khá mỏng, dể bị cong hơn nữa là nếu có CD để phát âm chuẩn hơn, tuy nhiên mình thấy quyển sách khá đáng mua vì nó cung cấp cho người đọc khá nhiều từ mới theo tiếng Anh-Anh.
4
1835316
2017-02-05 12:32:53
--------------------------
491293
6
Ấn tượng ban đầu là cuốn sách khá là to và đẹp mắt. Lần đầu tiên mình cầm cuốn từ điển nào mà có khổ giấy lớn như vậy luôn. Các mục được chia ra như các bài học làm mình gợi nhớ như hồi còn học thêm anh văn ở trung tâm chứ không còn nghĩ nó khô khan như cái từ mà mọi người thường nghĩ ra trong đầu khi nói đến khái niệm này. Các chủ đề được phân ra kèm màu sắc khác nhau xoay quanh đời sống hằng ngày của chúng ta. Rất chi tiết, rất sinh động. Nếu tiki nhập sách bilingual nữa thì hay hơn. Mình muốn học thêm 1 ngoại ngữ nữa. 
5
84972
2016-11-17 13:22:11
--------------------------
491142
6
Sách rất đẹp từ bìa đến trình bày nội dung bên trong
Mình mua được lúc Tiki giảm 40% và mình có kèm coupon giảm thêm 250K
Thanks Tiki
5
1541715
2016-11-16 14:08:15
--------------------------
490911
6
Mình mua được sách đợt giảm giá 50% nên giá rất rẻ, chất lượng giấy tốt, trình bày đẹp, rất hữu ích cho ai học tiếng anh vì có nhiều chủ đề rất gần gũi với đời sống và 4000 từ thông dụng hay dùng. 
Điểm chưa tốt là dịch vụ giao hàng, sách bị nhăn góc bìa làm ảnh hưởng đến thẩm mỹ của cuốn sách do sách bìa mềm. Hiện sách đã hết hàng nên mình cũng không đổi trả.
3
653941
2016-11-15 11:08:14
--------------------------
487251
6
Ưu điểm:
- Nội dung hay, hình ảnh sinh động nên dễ học
- giấy bóng, in màu toàn bộ
- giá rẻ hơn bên nhà sách
Khuyết điểm:
- Cách vẽ tranh hơi tối và khá lạ, có lẽ là văn hoá bên họ như vậy.
- Không có phiên âm nên phải tra thêm từ điển khác.
- Đây là kiểu từ điển Anh-Anh nên hơi khó hiểu.
- Và đây là lí do chính mình cho 3* : mở hộp ra thì góc sách bị cong, nhăn. Lưu ý thêm là đơn hàng mình còn 1 số món hàng khác và sách này không được bọc book care. 
Tuy nhiên, trước đây khi tiki chỉ bán sách thì ngoài book care, tiki còn bọc 1 lớp nilon cho toàn bộ đơn hàng, phải nói là rất kĩ. Nhưng từ khi bán thêm các loại hàng khác thì mình thấy ko còn lớp bọc nilon nữa. Cho là tiki muốn tiết kiệm chi phí đi nhưng với 1 quyển sách giá khoảng 250k (giá lúc mình mua) thì tiki cũng nên bọc 1 lớp nilon cho sách, có thể trừ vào số book care trong tài khoản.
Ngoài ra, một phần lỗi ở mình là mình đã không xem kĩ chính sách đổi trả nên bị quá hạn đổi, gặp trường hợp đó mình trả sách lại ngay thì đã không rước bực tức vào mình.

=> Nói chung quyển này đáng mua vì nội dung hay và giá rẻ hơn ngoài hiệu sách. Các bạn có thể chọn mua nếu muốn tiết kiệm và chấp nhận liều 1 chút (Có thể lúc gói đơn hàng cho mình tiki bận rộn quá nên làm không kĩ).

3
54770
2016-10-21 06:38:13
--------------------------
475868
6
Tuy mắc nhưng rất chất lượng, 
Điểm + : 1. Quyển sách giấy chất lượng khổ lớn
2. Màu sắc cực đẹp và chất lượng
3. Hình ảnh minh họa rất chân thực 
4. Có khoảng 4000 từ rất nhiều  và mình tin có thể trau dồi từ vựng khi nói 
5. Rất nhiều chủ đề( cơ thể, đồ ăn...)
6. Sách có phần nghe câu hỏi và nhiều từ vựng > tuy nhiên không có đĩa 
7. Có ngữ pháp câu hỏi ở phần dưới mỗi trang
ĐIỂM TRỪ: - 1. Không có hệ thống phiên âm IPA( nếu có sẽ dễ học > )
2.  Nếu như có VD = vài câu sẽ hay, nhưng mình không đòi hỏi lắm về khoảng này

-> Nếu ai có nhu cầu nâng cao hoặc bổ sung từ vựng thì tuyệt biết mấy, sách rất chất lượng hình ảnh đều rất dễ hiểu, dày nữa. Với 4000 từ nếu m.n học hết thì tiếng anh chắc đủ để nói good rồi ^ ^
5
753648
2016-07-17 01:23:32
--------------------------
