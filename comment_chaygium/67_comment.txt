392864
67
Chị mình đã mua 1 quyển, đọc rất hay và dễ hiểu. nhờ có nó mà tiếng anh mình cải thiện rõ hơn hẳn. Các bài tập thực hành trong sách được thiết kế tương tự trong bài thi TOEIC giúp mình làm quen, có sự chuẩn bị trước khi làm bài thi TOEIC thật sự.
Theo mình thấy cuốn sách này tốt, nhất định có ích cho bạn nào đang có ý định thi lấy bằng TOEIC. chỉ tiếc là sách không có đĩa CD kèm theo, còn lại thì rất tốt! Rất  đáng tiền bỏ ra để mua!! 
5
1173287
2016-03-07 20:21:44
--------------------------
340341
67
Cuốn sách được viết hoàn toàn bằng tiếng Anh. Tuy nhiên, lời lẽ dễ hiểu, hướng dẫn và giải thích chi tiết. Sách gồm 4 phần. Phần 1 dành trọn cho việc giới thiệu về TOEIC: Mục đích của TOEIC là gì? TOEIC kiểm tra những kỹ năng gì? Ai là người sử dụng TOEIC? Kết cấu của một bài thi TOEIC? Làm thế nào tôi chuẩn bị cho bài thi TOEIC? Làm thế nào để có thể đạt điểm cao trong bài thi TOEIC?... Phần 2 là Listening. Phần 3 Reading. Phần 4 TOEIC Model Tests.
Các bài tập thực hành trong sách được thiết kế tương tự trong bài thi TOEIC giúp mình làm quen, có sự chuẩn bị trước khi làm bài thi TOEIC thật sự.
Theo mình thấy cuốn sách này tốt, nhất định có ích cho bạn nào đang có ý định thi lấy bằng TOEIC.
Bản này không bán kèm CD, bạn nào muốn mua kèm CD thì chọn bản khác (cũng có trên Tiki) với giá cao hơn nhé.
4
85219
2015-11-19 18:52:05
--------------------------
264492
67
Tôi mua quyển sách này khi được một người bạn giới thiệu và quả thật nó không hề làm tôi thất vọng. Quyển sách rất to, tất cả đều bằng tiếng anh nhưng viết tương đối dễ hiểu cho những ai mới bắt đầu học để lấy bằng TOEIC. Phần đầu nêu lên cách học tiếng anh hiệu quả, phần sau bao gồm một số lý thuyết và bài tập gặp khá nhiều trong bài thi Toeic. Nhìn chung, quyển sach rất đáng bỏ một số tiền ra mua, nhưng do hồi đó tôi không mua CD để luyện thêm nên bây giờ có chút hối hận. Hãy mua quyển sách này có kèm CD các bạn nhé !
4
532501
2015-08-13 10:48:58
--------------------------
