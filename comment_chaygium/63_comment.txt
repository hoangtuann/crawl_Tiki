324720
63
I find this book is like a sad haunting song of youth with unforgettable memories of first love and even death of those who suffered too much vulnerable things in life. If you want to find happiness through every single word in a fictional world, don't touch this book. If you used to be hurt, this book would bring your wound back. Once reading it, you will never forget its sad melody as well as the forest that the author draws in your mind. Hurt and dark and silent and isolated did the characters feel will melt into you like a storm and it's quite hard for you to find an escape...
4
374075
2015-10-21 20:03:17
--------------------------
160658
63
Tôi cho rằng Rừng Nauy nên là một tác phẩm đi theo cuộc đời một con người. Rất nhiều, rất nhiều bạn lần đầu tìm đến vì yếu tố 18+ của tác phẩm. Vậy là Rừng Nauy theo bạn ngay khi bạn có những hiếu kỳ đầu tiên về giới tính. Gạt chúng sang một bên, cho dù ấn tượng đầu tiên là gì, hãy đọc lại Rừng Nauy khi bạn tròn đôi mươi. Khắc khoải, cô độc, trống trải và mất phương hướng - bạn sẽ tìm thấy mình trong đó. 
Hãy đọc lại một lần nữa khi bạn đã gần 30, để thấy mình vượt qua khủng hoảng ngoạn mục thế nào. Đôi khi cô đơn sẽ tìm đến bạn, có thể bạn lại một lần nữa tìm thấy chính mình trong tác phẩm. 
Rừng Nauy là một cuốn sách biến hoá. Mỗi một lần đọc cho độc giả một trải nghiệm mới. Bởi vì sao ư? Tuổi trẻ, sự cô độc, nỗi đau, sự mất mát đau thương, tình yêu, hi vọng - chẳng phải chính là những chủ đề chúng ta gần gũi nhất, dễ bắt gặp bản thân mình trong đó nhất sao?
5
565773
2015-02-25 10:08:01
--------------------------
157996
63
Mình luôn rất thích cách kể chuyện của Murakami. Mỗi lần đọc sách của ông, mình có thể chuyển thể nó thành một bộ phim ngay trong đầu mình.
Norwegian Wood cũng không ngoại lệ. Theo dòng suy nghĩ của Toru trở về những năm tháng đại học, mình học được nhiều bài học về tình bạn, tình yêu, sự mất mát và trưởng thành. Trong câu chuyện mà Murakami kể, ông còn nhắn nhủ rất nhiều triết lý nhân sinh rất sâu sắc.
Đây là bản dịch của Jay Rubin từ tiếng Nhật của nhà xuất bản Vintage, và là bìa sách mà mình thích nhất trong các bìa hiện có của Norwegian Wood. 
Khổ sách theo mình rất vừa vặn. Giấy sách là giấy tái chế nên cực nhẹ luôn, có thể xách theo bất cứ đâu mà không sợ nặng hay choán chỗ.
5
11661
2015-02-10 23:41:22
--------------------------
28557
63
The novel is a great one. If you just view them on the base of sex, you have made the biggest mistake in your life, all the sex description is just make the story more real and more meaningful. It is a story about love and loss, about young people who have to struggle with powerful emotions and with evolving ideas about life, freedom, love, responsibility. Never will you find a novel of Haruki, which is easy to understand. A normal love story is not his style, he want to empower the reader, he want you to think, imagine and create with him. Reading the novel, you will know and understand more about a generation of Japanese people as well as understand more about yourself. 
5
17940
2012-05-28 20:29:02
--------------------------
18669
63
lại một tác phẩm không thể bỏ qua của Murakami. cách viết độc đáo, thấm đẫm văn hóa tinh thần rất Nhật ấy vẫn tiếp tục lôi cuốn ta vào từng trang sách. êm đềm nhưng không kém phần gay cấn, cuốn sách gây ra nhiều tranh cãi về nội dung này vẫn thực sự là món ăn tinh thần làm tôi thích thú trong những chiều mưa. café + ghế gỗ + Murakami, thế là đủ. tôi tin rằng vài bạn đọc khi lần đầu thưởng thức sẽ cảm thấy khó chịu đôi chút với phong cách nhấn-nhưng-lướt vào sex của Mukarami, đúng thôi, nhưng đó chính là cái riêng của ông, một cái riêng rất phóng khoáng và thẳng thắn của xứ Mặt Trời Mọc.
Dịu lắng như bản "Norwegian Wood" của John Lennon.
Nhưng cũng dữ dội như chính sự khao khát trong tâm hồn mỗi người, những ai đã, đang, và sẽ đọc "Rừng Na-uy".
5
24326
2012-02-05 10:04:38
--------------------------
15518
63
một tác phẩm kinh điển và đầy tính nhân văn. mình đã đọc đi đọc lại bản dịch tiếng việt rất nhiều lần và nay mình quyết định mua bản dich tiếng anh về vừa để có dịp trải nghiệm tác phẩm kinh điển này thêm 1 lần và đồng thời có dịp nâng cao khả năng ngoại ngữ
5
18691
2011-12-07 09:08:24
--------------------------
